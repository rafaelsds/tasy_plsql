create or replace 
procedure cpoe_gerar_rel_inconsistencias(nm_usuario_p	varchar2) is

ie_tipo_item_w			w_relat_hor_cpoe.ie_tipo_item%type;
nr_seq_item_w			w_relat_hor_cpoe.nr_seq_item%type;
nr_atendimento_w		w_relat_hor_cpoe.nr_atendimento%type;
cd_estabelecimento_w	w_relat_hor_cpoe.cd_estabelecimento%type;

nr_prescricao_w			prescr_medica.nr_prescricao%type;

qt_dia_prim_hor_w		prescr_material.qt_dia_prim_hor%type;

cd_material_w			cpoe_material.cd_material%type;
ie_administracao_w		cpoe_material.ie_administracao%type;
ie_alterou_horario_w	cpoe_material.ie_alterou_horario%type;
ds_horarios_prescr_w	cpoe_material.ds_horarios%type;
ds_horarios_w			cpoe_material.ds_horarios%type;
dt_liberacao_enf_w		cpoe_material.dt_liberacao_enf%type;
dt_liberacao_farm_w		cpoe_material.dt_liberacao_farm%type;

nr_seq_proc_interno_w	cpoe_procedimento.nr_seq_proc_interno%type;

cd_dieta_w				cpoe_dieta.cd_dieta%type;

cd_recomendacao_W		cpoe_recomendacao.cd_recomendacao%type;

cd_intervalo_w			intervalo_prescricao.cd_intervalo%type;
ie_operacao_w			intervalo_prescricao.ie_operacao%type;

dt_prox_geracao_w		date;
dt_prim_horario_w		date;
dt_fim_rel_w			date;
dt_aux_w				date;
qt_hora_intervalo_w		number(15,4);
qt_dias_w				number(10);
qt_prescricoes_prev_w	number(10);
qt_prescricoes_w		number(10);

ds_dias_semana_w		varchar2(30);


/*
1 - Verifica se a c�pia contem um item prescrito e ainda vig�nte que n�o contem data de pr�xima gera��o
2 - Verifica se a c�pia contem um item prescrito e ainda vig�nte que a data de pr�xima gera��o deixou de ser atualizada
3 - Verifica se a quantidade de prescri��es geradas
*/

type cc02 is Ref Cursor;
c02 cc02;

cursor c01 is
	select	decode(nvl(ie_material, 'N'), 'S', 'MAT', decode(ie_controle_tempo, 'S', 'SOL', 'M')) ie_tipo_item,
		a.nr_sequencia nr_seq_item,
		nr_atendimento,
		obter_estab_atendimento(a.nr_atendimento) cd_estabelecimento,
		dt_prox_geracao,
		dt_inicio,
		nvl(ie_administracao, 'P') ie_administracao,
		cd_intervalo,
		dt_liberacao_enf,
		dt_liberacao_farm
	from	cpoe_material a
	where	((decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= sysdate) or (decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
	and	a.dt_liberacao is not null
	and	nvl(a.nr_seq_receita_amb,0) = 0
	and	nvl(a.ie_retrogrado,'N') = 'N'
	and	((((nvl(a.ie_evento_unico,'N') = 'N') and
			(a.nr_seq_adicional is null) and
			(a.nr_seq_ataque is null)) and
			(nvl(a.ie_material,'N') = 'N')) or
			(not exists(select	1
						from	prescr_medica y,
								prescr_material z
						where	y.nr_prescricao = z.nr_prescricao
						and	y.nr_atendimento = a.nr_atendimento
						and	z.nr_seq_mat_cpoe = a.nr_sequencia )))
	and	nvl(a.ie_baixado_por_alta,'N') = 'N'
	and	nvl(a.cd_funcao_origem,2314) = 2314
	and	a.nr_atendimento is not null
	and obter_se_atendimento_alta(a.nr_atendimento) = 'N'
	union all
	select	ie_tipo_dieta ie_tipo_item,
		a.nr_sequencia nr_seq_item,
		nr_atendimento,
		obter_estab_atendimento(a.nr_atendimento) cd_estabelecimento,
		dt_prox_geracao,
		dt_inicio,
		nvl(ie_administracao, 'P') ie_administracao,
		cd_intervalo,
		dt_liberacao_enf,
		dt_liberacao_farm
	from	cpoe_dieta a
	where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate) or (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and	a.dt_liberacao is not null
	and	nvl(a.ie_retrogrado,'N') = 'N'
	and	((nvl(a.ie_evento_unico,'N') = 'N') or
		(not exists (select	1
					from	prescr_medica y
					where	y.nr_atendimento = a.nr_atendimento
					and	exists (select	1
								from	prescr_material z
								where	z.nr_prescricao = y.nr_prescricao
								and		z.nr_seq_dieta_cpoe = a.nr_sequencia
								union
								select	1
								from	prescr_dieta z
								where	z.nr_prescricao = y.nr_prescricao
								and		z.nr_seq_dieta_cpoe = a.nr_sequencia
								union
								select	1
								from	rep_jejum z
								where	z.nr_prescricao = y.nr_prescricao
								and		z.nr_seq_dieta_cpoe = a.nr_sequencia
								union
								select	1
								from	prescr_leite_deriv z
								where	z.nr_prescricao = y.nr_prescricao
								and		z.nr_seq_dieta_cpoe = a.nr_sequencia
								union
								select	1
								from	nut_pac z
								where	z.nr_prescricao = y.nr_prescricao
								and		z.nr_seq_npt_cpoe = a.nr_sequencia))))
	and	nvl(a.ie_dose_unica,'N') = 'N'
	and	nvl(a.ie_baixado_por_alta,'N') = 'N'
	and	nvl(a.cd_funcao_origem,2314) = 2314
	and	a.nr_atendimento is not null
	and obter_se_atendimento_alta(a.nr_atendimento) = 'N'
	and ie_tipo_dieta not in ('P', 'I')
	union all
	select	'R' ie_tipo_item,
		a.nr_sequencia nr_seq_item,
		nr_atendimento,
		obter_estab_atendimento(a.nr_atendimento) cd_estabelecimento,
		dt_prox_geracao,
		dt_inicio,
		nvl(ie_administracao, 'P') ie_administracao,
		cd_intervalo,
		dt_liberacao_enf,
		dt_liberacao_farm
	from	cpoe_recomendacao a
	where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate) or (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and	a.dt_liberacao is not null
	and	nvl(a.ie_retrogrado,'N') = 'N'
	and	((nvl(a.ie_evento_unico,'N') = 'N') or
		(not exists (select	1
					from	prescr_medica y,
						prescr_recomendacao z
					where	y.nr_prescricao = z.nr_prescricao
					and	y.nr_atendimento = a.nr_atendimento
					and	z.nr_seq_rec_cpoe = a.nr_sequencia)))
	and	nvl(a.cd_funcao_origem,2314) = 2314
	and	a.nr_atendimento is not null
	and obter_se_atendimento_alta(a.nr_atendimento) = 'N'
	union all
	select	'G' ie_tipo_item,
		a.nr_sequencia nr_seq_item,
		nr_atendimento,
		obter_estab_atendimento(a.nr_atendimento) cd_estabelecimento,
		dt_prox_geracao,
		dt_inicio,
		nvl(ie_administracao, 'P') ie_administracao,
		cd_intervalo,
		dt_liberacao_enf,
		dt_liberacao_farm
	from	cpoe_gasoterapia a
	where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate) or (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and	a.dt_liberacao is not null
	and	nvl(a.ie_retrogrado,'N') = 'N'
	and	nvl(a.ie_baixado_por_alta,'N') = 'N'
	and	nvl(a.cd_funcao_origem,2314) = 2314
	and	a.nr_atendimento is not null
	and obter_se_atendimento_alta(a.nr_atendimento) = 'N'
	union all
	select	'P' ie_tipo_item,
		a.nr_sequencia nr_seq_item,
		nr_atendimento,
		obter_estab_atendimento(a.nr_atendimento) cd_estabelecimento,
		dt_prox_geracao,
		dt_inicio,
		nvl(ie_administracao, 'P') ie_administracao,
		cd_intervalo,
		dt_liberacao_enf,
		dt_liberacao_farm
	from	cpoe_procedimento a
	where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate) or (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and	a.dt_liberacao is not null
	and	nvl(a.ie_retrogrado,'N') = 'N'
	and	consiste_se_copia_proc_int(a.nr_seq_proc_interno) = 'S'
	and	((nvl(a.ie_evento_unico,'N') = 'N') or
		(not exists(select	1
					from	prescr_medica y,
						prescr_procedimento z
					where	y.nr_prescricao = z.nr_prescricao
					and	y.nr_atendimento = a.nr_atendimento
					and	z.nr_seq_proc_cpoe = a.nr_sequencia)))
	and	nvl(a.ie_baixado_por_alta,'N') = 'N'
	and	nvl(a.cd_funcao_origem,2314) = 2314
	and	a.nr_atendimento is not null
	and obter_se_atendimento_alta(a.nr_atendimento) = 'N'
	and	a.nr_seq_proc_interno is not null
	union all
	select	'DI' ie_tipo_item,
		a.nr_sequencia nr_seq_item,
		nr_atendimento,
		obter_estab_atendimento(a.nr_atendimento) cd_estabelecimento,
		dt_prox_geracao,
		dt_inicio,
		nvl(ie_administracao, 'P') ie_administracao,
		null,
		dt_liberacao_enf,
		dt_liberacao_farm
	from	cpoe_dialise a
	where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= sysdate) or (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
	and	a.dt_liberacao is not null
	and	nvl(a.ie_retrogrado,'N') = 'N'
	and	((nvl(a.ie_evento_unico,'N') = 'N') or
		(not exists (select	1
					from	prescr_medica y,
						hd_prescricao z
					where	y.nr_prescricao = z.nr_prescricao
					and	y.nr_atendimento = a.nr_atendimento
					and	z.nr_seq_dialise_cpoe = a.nr_sequencia)))
	and	nvl(a.ie_baixado_por_alta,'N') = 'N'
	and	nvl(a.cd_funcao_origem,2314) = 2314
	and	a.nr_atendimento is not null
	and obter_se_atendimento_alta(a.nr_atendimento) = 'N'
	order by cd_estabelecimento, nr_atendimento, ie_tipo_item;
		
procedure valores_adicionais is

begin
	ie_alterou_horario_w:= 'N';
	
	if	(ie_tipo_item_w in ('M','MAT','SOL')) then
		select	cd_material,
			ie_alterou_horario,
			ds_horarios
		into	cd_material_w,
			ie_alterou_horario_w,
			ds_horarios_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_item_w;
	elsif (ie_tipo_item_w in ('L', 'E', 'S', 'O')) then
		select	nvl(cd_material, cd_mat_prod1),
			cd_dieta,
			ie_alterou_horario,
			ds_horarios
		into	cd_material_w,
			cd_dieta_w,
			ie_alterou_horario_w,
			ds_horarios_w
		from	cpoe_dieta
		where	nr_sequencia = nr_seq_item_w;
	elsif (ie_tipo_item_w in ('P')) then
		select	nr_seq_proc_interno,
			ds_horarios
		into	nr_seq_proc_interno_w,
			ds_horarios_w
		from	cpoe_procedimento
		where	nr_sequencia = nr_seq_item_w;
	elsif (ie_tipo_item_w in ('R')) then
		select	cd_recomendacao,
			ds_horarios
		into	cd_recomendacao_W,
			ds_horarios_w
		from	cpoe_recomendacao
		where	nr_sequencia = nr_seq_item_w;
	elsif (ie_tipo_item_w in ('G')) then
		select	ds_horarios
		into	ds_horarios_w
		from	cpoe_gasoterapia
		where	nr_sequencia = nr_seq_item_w;
	end if;

end;

procedure insert_w_relat_hor_cpoe(ie_tipo_incons_p	varchar2, ds_incons_aux_p varchar2) is

begin
	insert
	into w_relat_hor_cpoe (cd_estabelecimento, nr_seq_item, dt_atualizacao, nm_usuario, ie_tipo_item, ie_tipo_incons, nr_atendimento, ds_incons_aux) 
	values	(cd_estabelecimento_w, nr_seq_item_w, sysdate, nm_usuario_p, ie_tipo_item_w, ie_tipo_incons_p, nr_atendimento_w, ds_incons_aux_p);
end;

begin

dt_fim_rel_w	:=	sysdate + (18/24); /* Verificar melhor essa data, talvez ser� melhor pegar atrav�s da tabela LOG_CPOE*/

delete from w_relat_hor_cpoe where nm_usuario = nm_usuario_p;

commit;

open c01;
loop
fetch c01 into
		ie_tipo_item_w,
		nr_seq_item_w,
		nr_atendimento_w,
		cd_estabelecimento_w,
		dt_prox_geracao_w,
		dt_prim_horario_w,
		ie_administracao_w,
		cd_intervalo_w,
		dt_liberacao_enf_w,
		dt_liberacao_farm_w; /* (P,N,C)(Conforme 1� Hor�rio,Se necess�rio,A crit�rio m�dico) */
exit when c01%notfound;
	begin
	valores_adicionais();
	
	
	if (cd_intervalo_w is not null) then
		select	max(ie_operacao),
			max(obter_ocorrencia_intervalo(cd_intervalo, 24, 'H'))
		into	ie_operacao_w,
			qt_hora_intervalo_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_w;
	end if;
	
	/* Verifica se a c�pia contem um item prescrito e ainda vig�nte que n�o contem data de pr�xima gera��o */
	if (dt_prox_geracao_w is null) then
		insert_w_relat_hor_cpoe('1', ''); 
		goto proximo_item;
	end if;
	/* Verifica se a c�pia contem um item prescrito e ainda vig�nte que a data de pr�xima gera��o deixou de ser atualizada */
	if (dt_prox_geracao_w < sysdate - 3) then
		insert_w_relat_hor_cpoe('2','');
		goto proximo_item;
	end if;
	/* Verifica se a quantidade de prescri��es geradas */
	if (cd_intervalo_w is not null and ie_operacao_w not in ('D')) then
	
		qt_dias_w := trunc(dt_fim_rel_w - dt_prim_horario_w);
				
		if (qt_hora_intervalo_w between 1 and 12) then
			qt_prescricoes_prev_w := qt_dias_w;
		elsif (qt_hora_intervalo_w between 13 and 24) then
			qt_prescricoes_prev_w := TRUNC(((qt_dias_w * 24) / (qt_hora_intervalo_w * 2)));
		elsif (qt_hora_intervalo_w >= 24) then 
			qt_prescricoes_prev_w := TRUNC(((qt_dias_w * 24) / (qt_hora_intervalo_w)));
		end if;
		
		if	(ie_tipo_item_w in ('M','MAT','SOL')) then
		
			if (ie_tipo_item_w = 'M') then
				select	 decode(nvl(max(ie_domingo), 'S'), 'S', '1', '') ||
					decode(nvl(max(ie_segunda), 'S'), 'S', '2', '') ||
					decode(nvl(max(ie_terca), 'S'), 'S', '3', '') ||
					decode(nvl(max(ie_quarta), 'S'), 'S', '4', '') ||
					decode(nvl(max(ie_quinta), 'S'), 'S', '5', '') ||
					decode(nvl(max(ie_sexta), 'S'), 'S', '6', '') ||
					decode(nvl(max(ie_sabado), 'S'), 'S', '7', '')
				into	ds_dias_semana_w
				from	cpoe_material
				where	nr_sequencia	= nr_seq_item_w;
				
				dt_aux_w := dt_prim_horario_w;
				while (dt_aux_w < dt_fim_rel_w) loop
					if (instr(ds_dias_semana_w, obter_cod_dia_semana(dt_aux_w)) = 0) then
						qt_dias_w := qt_dias_w-1;
					end if;
					dt_aux_w := dt_aux_w +1;
				end loop;
			end if;
			select	count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	prescr_material
			where	nr_seq_mat_cpoe = nr_seq_item_w;
		elsif (ie_tipo_item_w in ('L', 'E', 'S')) then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	prescr_material
			where	nr_seq_dieta_cpoe = nr_seq_item_w;
		elsif (ie_tipo_item_w in ('O')) then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	prescr_dieta
			where	nr_seq_dieta_cpoe = nr_seq_item_w;
		elsif (ie_tipo_item_w in ('J')) then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	rep_jejum
			where	nr_seq_dieta_cpoe = nr_seq_item_w;
		elsif (ie_tipo_item_w in ('P')) then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	prescr_procedimento
			where	nr_seq_proc_cpoe = nr_seq_item_w;
		elsif (ie_tipo_item_w in ('R')) then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	prescr_recomendacao
			where	nr_seq_rec_cpoe = nr_seq_item_w;
		elsif (ie_tipo_item_w in ('G')) then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	prescr_gasoterapia
			where	nr_seq_gas_cpoe = nr_seq_item_w;
		elsif	(ie_tipo_item_w = 'DI') then
			select count(distinct nr_prescricao)
			into	qt_prescricoes_w
			from	hd_prescricao
			where	nr_seq_dialise_cpoe = nr_seq_item_w;
		end if;
		
		if (qt_prescricoes_w < qt_prescricoes_prev_w) then
			insert_w_relat_hor_cpoe('3', '');
			goto proximo_item;
		end if;		
	end if;
	
	/* Poss�vel erro na gera��o do item na prescri��o */	
	if (ie_administracao_w = 'P' and  ie_alterou_horario_w = 'N' and (dt_liberacao_farm_w is not null or dt_liberacao_enf_w is not null)) then
		if	(ie_tipo_item_w in ('M','MAT','SOL') and cd_material_w is not null) then
			open c02 for
			select	ds_horarios,
				nr_prescricao,
				qt_dia_prim_hor
			from	prescr_material 
			where	nr_seq_mat_cpoe = nr_seq_item_w
			and	cd_material = cd_material_w
			and	dt_atualizacao_nrec > nvl(dt_liberacao_farm_w, dt_liberacao_enf_w);
		elsif (ie_tipo_item_w in ('L', 'E', 'S') and cd_material_w is not null) then
			open c02 for
			select	ds_horarios,
				nr_prescricao,
				qt_dia_prim_hor
			from	prescr_material 
			where	nr_seq_dieta_cpoe = nr_seq_item_w
			and	cd_material = cd_material_w
			and	dt_atualizacao_nrec > nvl(dt_liberacao_farm_w, dt_liberacao_enf_w);
		elsif (ie_tipo_item_w in ('O') and cd_dieta_w is not null) then
			open c02 for
			select ds_horarios,
				nr_prescricao,
				0
			from	prescr_dieta
			where	nr_seq_dieta_cpoe = nr_seq_item_w
			and	cd_dieta = cd_dieta_w
			and	dt_atualizacao > nvl(dt_liberacao_farm_w, dt_liberacao_enf_w);
		elsif (ie_tipo_item_w in ('P') and nr_seq_proc_interno_w is not null) then
			open c02 for
			select	ds_horarios,
				nr_prescricao,
				0
			from	prescr_procedimento 
			where	nr_seq_proc_cpoe = nr_seq_item_w
			and	nr_seq_proc_interno = nr_seq_proc_interno_w
			and	dt_atualizacao_nrec > nvl(dt_liberacao_farm_w, dt_liberacao_enf_w);
		elsif (ie_tipo_item_w in ('R')) then
			open c02 for
			select ds_horarios,
				nr_prescricao,
				0
			from	prescr_recomendacao
			where	nr_seq_rec_cpoe = nr_seq_item_w
			and	cd_recomendacao = cd_recomendacao_W
			and	dt_atualizacao > nvl(dt_liberacao_farm_w, dt_liberacao_enf_w);
		elsif (ie_tipo_item_w in ('G')) then
			open c02 for
			select	ds_horarios,
				nr_prescricao,
				0
			from	prescr_gasoterapia
			where	nr_seq_gas_cpoe = nr_seq_item_w
			and	dt_atualizacao_nrec > nvl(dt_liberacao_farm_w, dt_liberacao_enf_w);
		else
			open c02 for
			select	'', '', 0 
			from	dual;
		end if;
		
		loop
		fetch c02 into
			ds_horarios_prescr_w,
			nr_prescricao_w,
			qt_dia_prim_hor_w;
		exit when c02%notfound;		
			if (ds_horarios_prescr_w is not null and nr_prescricao_w is not null) then
				if (instr(ds_horarios_w, 'das') = 0 and instr(ds_horarios_prescr_w, 'das') = 0 and  obter_quantidade_horarios(ds_horarios_w) <> obter_quantidade_horarios(ds_horarios_prescr_w)) then
					insert_w_relat_hor_cpoe('4', 'Prescr: ' || nr_prescricao_w);
					goto proximo_item;
				elsif (qt_dia_prim_hor_w < 0) then
					insert_w_relat_hor_cpoe('5', 'Prescr: ' || nr_prescricao_w);
					goto proximo_item;
				end if;
			end if;
		end loop;
		close c02;
	end if;
	
	<<proximo_item>>
	null;
	end;
end loop;
close c01;

commit;
		
end cpoe_gerar_rel_inconsistencias;
/
