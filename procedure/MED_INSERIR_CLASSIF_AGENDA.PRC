create or replace
procedure Med_Inserir_Classif_Agenda
		(cd_medico_p		varchar2,
		cd_classif_p		varchar2,
		ds_classif_p		varchar2,
		nr_min_duracao_p	number,
		nm_usuario_p		varchar2) is

ie_classif_tasy_w	varchar2(03);
nr_seq_classif_w	number(10,0);

begin

if	(cd_classif_p	= 'CONS') or
	(cd_classif_p	= 'CONS.QT') then
	
	ie_classif_tasy_w	:= 'C';
elsif	(cd_classif_p	= 'RET') or
	(cd_classif_p	= 'RET PRO') or
	(cd_classif_p	= 'RET. DE QT') or
	(cd_classif_p	= 'RET CIRUR') or
	(cd_classif_p	= 'RET COLO') or
	(cd_classif_p	= 'RET EDA') or
	(cd_classif_p	= 'RET PONTOS') or
	(cd_classif_p	= 'RECONSULTA') or
	(cd_classif_p	= 'RET T.E') or
	(cd_classif_p	= 'RECON') then
	ie_classif_tasy_w	:= 'R';
else
	ie_classif_tasy_w	:= 'X';
end if;

select 	med_classif_agenda_seq.nextval
into	nr_seq_classif_w
from 	dual;


insert	into med_classif_agenda
	(nr_sequencia,
	cd_medico,
	ie_classif_agenda,
	ds_classif_agenda,
	dt_atualizacao,
	nm_usuario,
	ds_cor_fonte,
	ds_cor_fundo,
	nr_minuto_duracao,
	ie_ficha_paciente,
	ie_tipo)
values	(nr_seq_classif_w,
	cd_medico_p,
	ie_classif_tasy_w,
	ds_classif_p,
	sysdate,
	nm_usuario_p,
	'clNavy',
	'clWhite',
	nr_min_duracao_p,
	'N', ie_classif_tasy_w);

commit;

end Med_Inserir_Classif_Agenda;
/