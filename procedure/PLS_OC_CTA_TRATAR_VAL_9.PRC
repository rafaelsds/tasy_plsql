create or replace 
procedure pls_oc_cta_tratar_val_9(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de situa��o do prestador conforme tipo de prestador selecionado
------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

i				pls_integer;
dados_tb_sel_w			pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_gera_ocorrencia_w		varchar2(1);
ie_prestador_referencia_w	varchar2(50);
ds_sql_w			varchar2(4000);
dt_exclusao_w			pls_prestador.dt_exclusao%type;
dt_cadastro_w			pls_prestador.dt_cadastro%type;
ie_situacao_w			pls_prestador.ie_situacao%type;
dt_atendimento_w		pls_conta.dt_atendimento_referencia%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
valor_bind_w			sql_pck.t_dado_bind;
cursor_w			sql_pck.t_cursor;

-- Informa��es da valida��o de situa��o inativa do prestador
cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_tipo_prestador,
		nvl(a.ie_forma_inativacao, 'D') ie_forma_inativacao
	from	pls_oc_cta_val_sit_prest a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;
				
cursor C03 (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is 
	/*Serve tanto para importa��o quanto para  consist�ncia, pois o nr_seq_prestador � 
	  populado na pls_imp_equipe_proc
	*/
	select	(select	x.dt_exclusao
		from	pls_prestador x
		where	x.nr_sequencia = a.nr_seq_prestador) dt_exc_prest,
		(select	x.dt_cadastro  
		from	pls_prestador x 
		where	x.nr_sequencia = a.nr_seq_prestador) dt_cad_prest,
		(select	x.ie_situacao  
		from	pls_prestador x 
		where	x.nr_sequencia = a.nr_seq_prestador) ie_situacao_prest,
		c.dt_atendimento,
		c.nr_sequencia nr_seq_conta
	from	pls_proc_participante a,
		pls_conta_proc b,
		pls_conta_ocor_v c
	where	c.nr_sequencia = b.nr_seq_conta
	and	b.nr_sequencia = a.nr_seq_conta_proc
	and	exists (	select	1
				from	pls_selecao_ocor_cta x
				where	x.nr_id_transacao = nr_id_transacao_pc
				and	x.ie_valido = 'S'
				and	x.nr_seq_conta = c.nr_sequencia);
	
	
begin
-- Deve se ter a informa��o da regra para que a valida��o seja aplicada.
if	(dados_regra_p.nr_sequencia is not null) then
	
	pls_tipos_ocor_pck.atualiza_campo_auxiliar('V', nr_id_transacao_p, null, dados_regra_p);
	pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
	i := 0;
	-- Buscar os dados da regra de valida��o conforme montado pelo usu�rio.
	for	r_C01_w in C01 (dados_regra_p.nr_sequencia) loop
		
		-- Caso o prestador referencia n�o seja o Participante
		if	(r_C01_w.ie_tipo_prestador <> 'P') then
			-- Busca o campo do prestador referencia
			if	(dados_regra_p.ie_evento = 'IMP') then
				if	(r_C01_w.ie_tipo_prestador = 'E') then
					ie_prestador_referencia_w := 'b.nr_seq_prestador_exec_imp';
				elsif	(r_C01_w.ie_tipo_prestador = 'S') then
					ie_prestador_referencia_w := 'b.nr_seq_prestador_imp';
				elsif	(r_C01_w.ie_tipo_prestador = 'L') then
					ie_prestador_referencia_w := 'c.nr_seq_prestador_imp';					
				end if;
			else
				if	(r_C01_w.ie_tipo_prestador = 'E') then
					ie_prestador_referencia_w := 'b.nr_seq_prestador_exec';
				elsif	(r_C01_w.ie_tipo_prestador = 'S') then
					ie_prestador_referencia_w := 'b.nr_seq_prestador';
				elsif	(r_C01_w.ie_tipo_prestador = 'L') then
					ie_prestador_referencia_w := 'c.nr_seq_prestador';					
				end if;
			end if;
			-- Monta o SQL din�mico com base no prestador refer�ncia
			ds_sql_w :=	'	select	x.dt_exclusao,							' || pls_tipos_ocor_pck.enter_w ||
					'		x.dt_cadastro,							' || pls_tipos_ocor_pck.enter_w ||
					'		x.ie_situacao,							' || pls_tipos_ocor_pck.enter_w ||
					'		b.dt_atendimento,						' || pls_tipos_ocor_pck.enter_w ||
					'		b.nr_sequencia							' || pls_tipos_ocor_pck.enter_w ||
					'	from 	pls_conta_ocor_v b,						' || pls_tipos_ocor_pck.enter_w ||
					'		pls_protocolo_conta_v c,					' || pls_tipos_ocor_pck.enter_w ||
					'		pls_prestador x							' || pls_tipos_ocor_pck.enter_w ||
					'	where 	c.nr_sequencia = b.nr_seq_protocolo  				' || pls_tipos_ocor_pck.enter_w ||
					'	and	x.nr_sequencia = ' || ie_prestador_referencia_w || pls_tipos_ocor_pck.enter_w ||
					'	and	exists (	select	1					' || pls_tipos_ocor_pck.enter_w ||
					'				from	pls_selecao_ocor_cta a			' || pls_tipos_ocor_pck.enter_w ||
					'				where	a.nr_id_transacao = :nr_id_transacao	' || pls_tipos_ocor_pck.enter_w ||
					'				and	a.ie_valido = ''S''			' || pls_tipos_ocor_pck.enter_w ||
					'				and	a.nr_seq_conta = b.nr_sequencia)	';
			-- Alimenta a bind	
			sql_pck.bind_variable(':nr_id_transacao', nr_id_transacao_p, valor_bind_w);
			-- Monta o cursor
			cursor_w := sql_pck.executa_sql_cursor(ds_sql_w, valor_bind_w);
			
			loop
			
				fetch 	cursor_w 
				into	dt_exclusao_w,
					dt_cadastro_w,
					ie_situacao_w,
					dt_atendimento_w,
					nr_seq_conta_w;
				exit when cursor_w%notfound;
				
				ie_gera_ocorrencia_w := 'N';
				-- Nesse IF � verificado as tr�s possibilidades do campo ie_forma_inativacao sendo
				-- D - Datas
				-- S - Situa��o
				-- DS - Data e situa��o				
				if	((r_C01_w.ie_forma_inativacao = 'D') and
					((dt_atendimento_w < dt_cadastro_w) or
					(dt_atendimento_w > dt_exclusao_w)))
					or
					(r_C01_w.ie_forma_inativacao = 'S' and ie_situacao_w = 'I')
					or
					((r_C01_w.ie_forma_inativacao = 'DS') and
					((dt_atendimento_w < dt_cadastro_w) or
					(dt_atendimento_w > dt_exclusao_w) or
					(ie_situacao_w = 'I'))) then
					ie_gera_ocorrencia_w := 'S';				
				end if;
				
				if 	(ie_gera_ocorrencia_w = 'S') then
					--Passa nr_seq_conta ao inv�z do nr_seq_selecao, pois ser� feito valida��o a n�vel de conta
					dados_tb_sel_w.nr_seq_selecao(i) := nr_seq_conta_w;
					dados_tb_sel_w.ds_observacao(i) := null;
					dados_tb_sel_w.ie_valido(i) := 'S';

					if	( i >= pls_util_pck.qt_registro_transacao_w) then

						pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
												'SEQ_CONTA', dados_tb_sel_w.ds_observacao, dados_tb_sel_w.ie_valido, nm_usuario_p, nr_id_transacao_p);
						pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
						i := 0;
					else
						i := i + 1;
					end if;
				end if;
			end loop;
			close cursor_w;
		else
			for r_C03_w in C03 (nr_id_transacao_p) loop
				
				ie_gera_ocorrencia_w := 'N';
				-- Nesse IF � verificado as tr�s possibilidades do campo ie_forma_inativacao sendo
				-- D - Datas
				-- S - Situa��o
				-- DS - Data e situa��o
				if	((r_C01_w.ie_forma_inativacao = 'D') and
					((r_C03_w.dt_atendimento < r_C03_w.dt_cad_prest) or
					(r_C03_w.dt_atendimento > r_C03_w.dt_exc_prest)))
					or
					(r_C01_w.ie_forma_inativacao = 'S' and r_C03_w.ie_situacao_prest = 'I')
					or
					((r_C01_w.ie_forma_inativacao = 'DS') and
					((r_C03_w.dt_atendimento < r_C03_w.dt_cad_prest) or
					(r_C03_w.dt_atendimento > r_C03_w.dt_exc_prest) or
					(r_C03_w.ie_situacao_prest = 'I'))) then
					ie_gera_ocorrencia_w := 'S';				
				end if;
				
				if 	(ie_gera_ocorrencia_w = 'S') then
					--Passa nr_seq_conta ao inv�z do nr_seq_selecao, pois ser� feito valida��o a n�vel de conta
					dados_tb_sel_w.nr_seq_selecao(i) := r_C03_w.nr_seq_conta;
					dados_tb_sel_w.ds_observacao(i) := null;
					dados_tb_sel_w.ie_valido(i) := 'S';

					if	( i >= pls_util_pck.qt_registro_transacao_w) then

						pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
												'SEQ_CONTA', dados_tb_sel_w.ds_observacao, dados_tb_sel_w.ie_valido, nm_usuario_p, nr_id_transacao_p);
						pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
						i := 0;
					else
						i := i + 1;
					end if;
				end if;
			end loop;
		end if;
	end loop; -- C01
	
	pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
							'SEQ_CONTA', dados_tb_sel_w.ds_observacao, dados_tb_sel_w.ie_valido, nm_usuario_p, nr_id_transacao_p);
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;
end pls_oc_cta_tratar_val_9;
/
