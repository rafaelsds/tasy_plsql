create or replace
procedure gerar_fluxo_caixa_lote_filtro(
					nr_seq_lote_fluxo_p	number,
					cd_banco_p 		number,
					nr_seq_proj_rec_p	number,
					nr_seq_caixa_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

/*--------------------------------------------------------------- ATEN��O ----------------------------------------------------------------*/
/* Cuidado ao realizar altera��es no fluxo de caixa em lote. Toda e qualquer altera��o realizada em qualquer uma */
/* das procedures do fluxo de caixa em lote deve ser cuidadosamente verificada e realizada no fluxo de caixa      */
/* convencional. Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando     */
/* assim que existam diferen�as entre os fluxos de caixa.                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA EM LOTE ALTERAR TAMB�M O FLUXO DE CAIXA ---------------*/

begin

if	(nvl(cd_banco_p,0) <> 0) then
	
	insert into fluxo_caixa_lote_filtro 
		(nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		cd_banco,
		nr_seq_lote_fluxo)
	VALUES	(fluxo_caixa_lote_filtro_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		cd_banco_p,
		nr_seq_lote_fluxo_p);
		
end if;

if	(nvl(nr_seq_proj_rec_p,0) <> 0) then
	
	insert into fluxo_caixa_lote_filtro 
		(nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_proj_rec,
		nr_seq_lote_fluxo)
	VALUES	(fluxo_caixa_lote_filtro_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_proj_rec_p,
		nr_seq_lote_fluxo_p);
		
end if;

if	(nvl(nr_seq_caixa_p,0) <> 0) then
	
	insert into fluxo_caixa_lote_filtro 
		(nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_caixa,
		nr_seq_lote_fluxo)
	VALUES	(fluxo_caixa_lote_filtro_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_caixa_p,
		nr_seq_lote_fluxo_p);
		
end if;

if	(nvl(cd_estabelecimento_p,0) <> 0) then
	
	insert into fluxo_caixa_lote_filtro 
		(nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		cd_estabelecimento,
		nr_seq_lote_fluxo)
	VALUES	(fluxo_caixa_lote_filtro_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		cd_estabelecimento_p,
		nr_seq_lote_fluxo_p);
		
end if;

commit;

end gerar_fluxo_caixa_lote_filtro;
/