create or replace procedure get_ihc_claim(
		nr_interno_conta_p   	number,
		cd_estabelecimento_p	number,
		nm_usuario_p       		varchar2,
		ie_validate_p      		varchar2,
		ie_process_p       		varchar2 default 'S') is 
    
cd_facility_w         	eclipse_parameters.cd_facility%type;
ie_facility_w         	eclipse_parameters.ie_facility%type;
ie_claim_type_w       	eclipse_parameters.ie_claim_type%type;
nr_atendimento_w      	atendimento_paciente.nr_atendimento%type;
nr_seq_conta_origem_w	conta_paciente.nr_seq_conta_origem%type;
nm_contact_w          	ihc_claim.nm_contact%type;
cd_pessoa_fisica_w    	pessoa_fisica.cd_pessoa_fisica%type;
ds_email_w        	compl_pessoa_fisica.ds_email%type;
nr_sequencia_w        	ihc_claim.nr_sequencia%type;
nr_records_w          	number(8,0);
ie_urgency_w          	ihc_claim.ie_urgency%type;
cd_externo_w          	convenio.cd_externo%type;
cd_convenio_w         	convenio.cd_convenio%type;
ie_referral_source_w	Number(10);
cd_discharge_w			Varchar2(10);


begin

BILLING_I18N_PCK.set_validate_eclipse(ie_validate_p);

select  nvl(max(nr_sequencia),0)
into    nr_sequencia_w
from    IHC_CLAIM
where   NR_ACCOUNT =  nr_interno_conta_p;

clear_ihc_claim(nr_sequencia_w);

select  a.nr_atendimento,
  b.nr_seq_conta_origem,
  a.cd_pessoa_fisica,
  obter_nome_pf(a.cd_pessoa_fisica),
  obter_email_pf(a.cd_pessoa_fisica)
into    nr_atendimento_w,
  nr_seq_conta_origem_w,
  cd_pessoa_fisica_w,
  nm_contact_w,
  ds_email_w
from    atendimento_paciente a,
        conta_paciente b
where   a.nr_atendimento = b.nr_atendimento
and     b.nr_interno_conta = nr_interno_conta_p;

select  max(cd_facility),
        max(ie_facility),
        max(ie_hospital_type)
into    cd_facility_w,
        ie_facility_w,
        ie_claim_type_w
from    eclipse_parameters
where   cd_estabelecimento = cd_estabelecimento_p;

-- REMOVE THIS CODE AFTER NOI TESTING - ONLY ADDED SO THAT WE CAN TEST PR AND PU TOGETHER
select 	max(decode(CD_AUTORIZACAO, null, ie_claim_type_w, 'PR', 'PR', 'PU')), 
	max(decode(CD_AUTORIZACAO, 'PR', 2, ie_facility_w))
into 	ie_claim_type_w , ie_facility_w
from 	conta_paciente p
where 	p.nr_interno_conta = nr_interno_conta_p;

if (ds_email_w is not null) then

  if (instr(ds_email_w,'@') = 0) then
    GENERATE_INCO_ECLIPSE(nr_interno_conta_p, 1, obter_desc_expressao(790540), nm_usuario_p);
  end if;

end if;

if (cd_facility_w is null) then
  GENERATE_INCO_ECLIPSE(nr_interno_conta_p, 1, obter_desc_expressao(958723), nm_usuario_p);
end if;
if (ie_facility_w is null) then
  GENERATE_INCO_ECLIPSE(nr_interno_conta_p, 1, obter_desc_expressao(960554), nm_usuario_p);
end if;
if (ie_claim_type_w is null) then
  GENERATE_INCO_ECLIPSE(nr_interno_conta_p, 1, obter_desc_expressao(960556), nm_usuario_p);
end if;

select  Obter_Convenio_Atendimento(nr_atendimento_w)
into    cd_convenio_w
from    dual;
  
if (cd_convenio_w > 0) then
  select  max(CD_EXTERNO)
  into    cd_externo_w
  from    convenio
  where   cd_convenio = cd_convenio_w;
end if;

if (cd_externo_w is null) then
  GENERATE_INCO_ECLIPSE(nr_interno_conta_p, 1, obter_desc_expressao(960562), nm_usuario_p);
end if;

get_eclipse_conversion('IE_CARATER_INTER_SUS', obter_carater_atend(nr_atendimento_w), 'IHC', null, nr_interno_conta_p, ie_urgency_w);
--get_eclipse_conversion('IE_FACILITY', ie_facility_w, 'IHC', null, nr_interno_conta_p, ie_facility_w);

if (nr_sequencia_w = 0) then  
  select  ihc_claim_seq.nextval
  into    nr_sequencia_w
  from  dual;   

  insert into ihc_claim(  nr_account,
      ie_claim_type,
      ie_compensation,
      ie_contiguous,
      cd_facility,
      ie_facility,
      cd_fund_brand,
      ie_previous_claim,
      nr_previous,
      ds_email,
      nm_contact,
      nr_phone,
      vl_charge,
      vl_hospital,
      vl_medical,
      ie_urgency,
      nr_sequencia,
      nm_usuario,
      nm_usuario_nrec,
      dt_atualizacao,
      dt_atualizacao_nrec,
      ie_status_tasy,
      ie_process)
    values(nr_interno_conta_p,
      ie_claim_type_w,
      'U',
      'N',
      cd_facility_w,
      ie_facility_w,
      cd_externo_w,
      decode(nr_seq_conta_origem_w, null,'S','N'),
      nr_seq_conta_origem_w,
      ds_email_w,
      nm_contact_w,
      to_number(substr(obter_compl_pf(cd_pessoa_fisica_w,1,'DIT')||obter_compl_pf(cd_pessoa_fisica_w,1,'DDT')||obter_compl_pf(cd_pessoa_fisica_w,1,'T'),1,20)),
      0,
      0,
      0,
      ie_urgency_w,
      nr_sequencia_w,
      nm_usuario_p,
      nm_usuario_p,
      sysdate,
      sysdate,
      'P',
      ie_process_p);
else 
  update IHC_CLAIM
  set nr_account = nr_interno_conta_p,
      ie_claim_type = ie_claim_type_w,
      ie_compensation = 'U',
      ie_contiguous = 'N',
      cd_facility = cd_facility_w,
      ie_facility = ie_facility_w,
      cd_fund_brand = cd_externo_w,
      ie_previous_claim = decode(nr_seq_conta_origem_w, null,'S','N'),
      nr_previous = nr_seq_conta_origem_w,
      ds_email = ds_email_w,
      nm_contact = nm_contact_w,
      nr_phone = to_number(substr(obter_compl_pf(cd_pessoa_fisica_w,1,'DIT')||obter_compl_pf(cd_pessoa_fisica_w,1,'DDT')||obter_compl_pf(cd_pessoa_fisica_w,1,'T'),1,20)),
      ie_urgency = ie_urgency_w,
      nm_usuario = nm_usuario_p,
      nm_usuario_nrec = nm_usuario_p,
      dt_atualizacao = sysdate,
      dt_atualizacao_nrec = sysdate,
      ie_status_tasy = 'P',
      vl_charge = 0,
      vl_hospital = 0,
      vl_medical = 0
  where nr_sequencia = nr_sequencia_w;  
    
end if;     

get_ihc_pat(nr_sequencia_w,nm_usuario_p);
get_ihc_epd(nr_sequencia_w,nm_usuario_p);
get_ihc_svb(nr_sequencia_w,nm_usuario_p);
get_ihc_anb(nr_sequencia_w,nm_usuario_p);
get_ihc_acs(nr_sequencia_w,nm_usuario_p);

select	max(ie_referral_source),
		max(cd_discharge)
into	ie_referral_source_w,
		cd_discharge_w
from	ihc_claim
where	nr_sequencia	= nr_sequencia_w;

/* TransferCde get_ihc_tfr
Must be set to A if ReferralSourceCode (EPD Segment) is equal to 01
Must be set to S if DischargeTypeCode (EPD Segment) is equal from 01 to 04
*/
if	(ie_referral_source_w = 1) or (cd_discharge_w in ('01','02','03','04')) then
	get_ihc_tfr(nr_sequencia_w,nm_usuario_p);
end if;

get_ihc_psg(nr_sequencia_w,nm_usuario_p); 
get_ihc_dmg(nr_sequencia_w,nm_usuario_p); 
get_ihc_trg(nr_sequencia_w,nm_usuario_p);
get_ihc_mig(nr_sequencia_w,nm_usuario_p);
get_ihc_cer(nr_sequencia_w,nm_usuario_p);
get_ihc_med(nr_sequencia_w,nm_usuario_p);
get_ihc_rem(nr_sequencia_w,nm_usuario_p);

commit;

end get_ihc_claim;
/