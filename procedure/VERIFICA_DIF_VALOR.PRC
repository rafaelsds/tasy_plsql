CREATE or REPLACE 
procedure Verifica_Dif_Valor(	dt_mesano_referencia_p  date,
					cd_material_p		Number) is

cd_estabelecimento_w	number(04,0)	:= 0;
cd_local_estoque_w	number(04,0)	:= 1;
cd_material_w		number(06,0)	:= 0;
cd_material_ref_w		number(06,0)	:= 0;
cd_operacao_estoque_w	number(03,0)	:= 0;
ie_entrada_saida_w		varchar2(1);
vl_estoque_w		number(15,2);
dt_mesano_referencia_w	date;
dt_mesano_anterior_w	date;
vl_entrada_w		number(15,2);
vl_saida_w		number(15,2);
vl_saldo_estoque_w	number(15,2);
vl_saldo_anterior_w		number(15,2);
vl_dif_w			number(15,2);
ds_retorno_w		varchar2(40);
qt_existe_w		Number(5);


cursor c00 is
select	distinct
	cd_estabelecimento,
	cd_material
from	saldo_estoque
where	dt_mesano_referencia 	= dt_mesano_referencia_w
and	nvl(qt_estoque, 0) > 0
union
select	distinct
	cd_estabelecimento,
	cd_material
from	saldo_estoque a
where	a.dt_mesano_referencia 	= dt_mesano_anterior_w
and	nvl(qt_estoque, 0) > 0
and not exists (
	select	1
	from	saldo_estoque b
	where	a.cd_estabelecimento 	= b.cd_estabelecimento
	and	a.cd_material		= b.cd_material
	and	b.dt_mesano_referencia 	= dt_mesano_referencia_w);

cursor c01 is
select	a.vl_estoque,
	a.cd_operacao_estoque,
	b.ie_entrada_saida
from	operacao_estoque b, 
	resumo_movto_estoque a
where	a.dt_mesano_referencia	= dt_mesano_referencia_w
and	a.cd_material		= cd_material_w
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	a.cd_operacao_estoque	= b.cd_operacao_estoque
and	a.ie_consignado		= 'N'
and	b.ie_atualiza_estoque	= 'S';

BEGIN
dt_mesano_referencia_w	:= pkg_date_utils.start_of(dt_mesano_referencia_p,'MONTH',0);
dt_mesano_anterior_w  	:= pkg_date_utils.add_month(dt_mesano_referencia_w, -1,0);
obter_valor_dinamico('Truncate table w_saldo_estoque',ds_retorno_w);
select	min(cd_material)
into	cd_material_ref_w
from 	saldo_Estoque
where	dt_mesano_referencia 	= dt_mesano_referencia_w;

open c00;
loop
	begin
	fetch c00 into
		cd_estabelecimento_w,
		cd_material_w;
	exit when c00%notfound;
        	begin
		vl_entrada_w		:= 0;
		vl_saida_w			:= 0;
		vl_saldo_estoque_w	:= 0;
		vl_saldo_anterior_w	:= 0;
		begin
		select	nvl(sum(vl_estoque),0)
		into	vl_saldo_estoque_w
		from	saldo_estoque
		where	cd_estabelecimento 	= cd_estabelecimento_w
		and	cd_material		= cd_material_w
		and	dt_mesano_referencia	= dt_mesano_referencia_w;
		exception
			when others	 then
				vl_saldo_estoque_w	:= 0;
		end;
		begin
		select	nvl(sum(vl_estoque),0)
		into	vl_saldo_anterior_w
		from	saldo_estoque
		where	cd_estabelecimento 	= cd_estabelecimento_w
		and	cd_material		 = cd_material_w
		and	dt_mesano_referencia	= dt_mesano_anterior_w;
		exception
			when others	 then
				vl_saldo_anterior_w	:= 0;
		end;

		open c01;
		loop
		fetch c01 into
			vl_estoque_w,
			cd_operacao_estoque_w,
			ie_entrada_saida_w;
		exit when c01%notfound;
       		begin
			if	(ie_entrada_saida_w = 'S') then
				vl_saida_w		:= vl_saida_w + vl_estoque_w;
			else
				vl_entrada_w	:= vl_entrada_w + vl_estoque_w;
			end if;
			end;
		end loop;
		close c01;
 		vl_dif_w	:=  	vl_saldo_estoque_w -
					(vl_saldo_anterior_w + vl_entrada_w - vl_saida_w);
		if	(vl_dif_w <> 0) then
			begin
			insert into w_saldo_estoque(
				cd_estabelecimento,
				cd_local_estoque,
				cd_material,
				dt_mesano_referencia,
				qt_estoque_atual,
				qt_estoque_anterior,
				qt_entrada,
				qt_saida,
				qt_diferenca)
			values(
				cd_estabelecimento_w,
				cd_local_estoque_w,
				cd_material_w,
				dt_mesano_referencia_w,
				vl_saldo_estoque_w,
				vl_saldo_anterior_w,
				vl_entrada_w,
				vl_saida_w,
				vl_dif_w);
			if	(cd_material_p = 1555) then
				update	saldo_estoque
				set	vl_estoque 		= vl_estoque - vl_dif_w
				where	cd_estabelecimento 	= cd_estabelecimento_w
				and	cd_material		= cd_material_w
				and	dt_mesano_referencia	= dt_mesano_referencia_w
				and	cd_local_estoque 	= (
								select	min(cd_local_estoque)
								from	saldo_estoque
								where	cd_estabelecimento 	= cd_estabelecimento_w
				  	  			and	cd_material		= cd_material_w
				  	  			and	dt_mesano_referencia	= dt_mesano_referencia_w);
			end if;

			if	(cd_material_p = 1556) then
				select	count(*)
				into	qt_existe_w
				from	saldo_estoque
				where	cd_estabelecimento 	= cd_estabelecimento_w
       	    		and	cd_material         	= cd_material_w
           			and	dt_mesano_referencia	= dt_mesano_referencia_w;
				if	(qt_existe_w = 0) then
					insert into saldo_estoque(
						cd_estabelecimento,
						cd_local_estoque,
						cd_material,
						dt_mesano_referencia,
						qt_estoque,
						vl_estoque,
						qt_reservada_requisicao,
						qt_reservada,
						dt_atualizacao,
						nm_usuario,
						vl_custo_medio,
						vl_preco_ult_compra,
						dt_ult_compra,
						ie_status_valorizacao,
						ie_bloqueio_inventario)
					values(cd_estabelecimento_w,
						cd_local_estoque_w,
						cd_material_w,
						dt_mesano_referencia_w,
						0,
						vl_dif_w,
						0,
						0,
						sysdate,
						'dif_valor',
						0,
						null,
						null,
						'N',
						'N');
				end if;
			end if;
			end;
		end if;
		end;
	end;
end loop;
close c00;
END Verifica_Dif_Valor;
/