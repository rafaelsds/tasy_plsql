create or replace
procedure pls_pacotes_duplica_composicao(
			nr_seq_composicao_p	pls_pacote_composicao.nr_sequencia%type,
			nr_seq_gerada_p	out	pls_pacote_composicao.nr_sequencia%type,
			nm_usuario_p		usuario.nm_usuario%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Permitir ao usuario duplicar a composicao selecionada, gerando assim uma 
	nova composicao com os mesmos procedimentos e materiais da composicao
	selecionada.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicionario [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de atencao:

Alteracoes:
-------------------------------------------------------------------------------------------------------------------
jjung OS 614334 - 26/07/2013 - Criacao da rotina.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_nova_composicao_w	pls_pacote_composicao.nr_sequencia%type;
qt_registro_w			pls_integer;
begin

-- Deve se ter informacao de qual composicao deve ser informada para que seja realizada alguma alteracao.
if	(nr_seq_composicao_p is not null) then
	
	select	count(1)
	into	qt_registro_w
	from	pls_pacote_composicao 
	where	nr_sequencia = nr_seq_composicao_p;
	
	if	(qt_registro_w = 0) then
	
		-- Por favor selecione uma composicao valida!
		wheb_mensagem_pck.Exibir_mensagem_abort(251256);
	else
		select	pls_pacote_composicao_seq.nextval
		into	nr_seq_nova_composicao_w
		from	dual;
		
		insert into pls_pacote_composicao 
				(	ds_composicao, dt_atualizacao, dt_atualizacao_nrec, dt_fim_vigencia,
					dt_inicio_vigencia, dt_liberacao, ie_situacao, nm_usuario, nm_usuario_liberacao,
					nm_usuario_nrec, nr_seq_pacote, nr_sequencia, dt_negociacao, dt_reajuste)
				(select	substr('Copia( '|| nm_usuario_p || ') - ' || ds_composicao,1,120), sysdate, sysdate, dt_fim_vigencia,
					dt_inicio_vigencia, null, 'A', nm_usuario_p, null, 
					nm_usuario_p, nr_seq_pacote, nr_seq_nova_composicao_w, dt_negociacao, dt_reajuste
				from	pls_pacote_composicao 
				where	nr_sequencia = nr_seq_composicao_p);
		
		select	count(1)
		into	qt_registro_w
		from	pls_pacote_composicao
		where	nr_sequencia = nr_seq_nova_composicao_w;
		
		if	(qt_registro_w = 0) then
			
			-- Houve um problema nao identificado ao gerar a nova composicao. Favor verifique se o processo foi executado com sucesso e novamente.
			wheb_mensagem_pck.Exibir_mensagem_abort(251263);
		else
		
			insert into pls_pacote_procedimento
					(	cd_area_procedimento, cd_especialidade, cd_grupo_proc, cd_procedimento,         
						dt_atualizacao, dt_atualizacao_nrec, ie_estrutura, ie_gerar_a1200,          
						ie_origem_proced, ie_situacao, ie_tipo_guia, nm_usuario, nm_usuario_nrec,         
						nr_seq_composicao, nr_seq_pacote, nr_seq_prestador, nr_seq_regra, nr_sequencia,            
						qt_procedimento, vl_negociado, vl_procedimento_regra, ie_contem_hm, ie_gerar_a500,
						ie_procedimento_princ, ie_proc_alternativo, ie_via_acesso, nr_seq_grau_partic_ent,
						cd_medico, nr_seq_cbo_saude, nr_seq_grupo_servico, nr_seq_grau_partic)
					(select	cd_area_procedimento, cd_especialidade, cd_grupo_proc, cd_procedimento,
						sysdate, sysdate, ie_estrutura, ie_gerar_a1200,
						ie_origem_proced, 'A', ie_tipo_guia, nm_usuario_p, nm_usuario_p,
						nr_seq_nova_composicao_w, nr_seq_pacote, nr_seq_prestador, nr_seq_regra, pls_pacote_procedimento_seq.nextval,
						qt_procedimento, vl_negociado, vl_procedimento_regra, ie_contem_hm, ie_gerar_a500,
						ie_procedimento_princ, ie_proc_alternativo, ie_via_acesso, nr_seq_grau_partic_ent,
						cd_medico, nr_seq_cbo_saude, nr_seq_grupo_servico, nr_seq_grau_partic
					from	pls_pacote_procedimento
					where	nr_seq_composicao = nr_seq_composicao_p);
					
			insert into pls_pacote_material
					(	cd_material, cd_unidade_medida, dt_atualizacao, dt_atualizacao_nrec, ie_gerar_a1200,
						ie_situacao, ie_tipo_guia, nm_usuario, nm_usuario_nrec, nr_seq_composicao, nr_seq_material,
						nr_seq_pacote, nr_seq_prestador, nr_seq_regra, nr_sequencia, qt_material, vl_material_regra,       
						vl_negociado, ie_tipo_despesa, ie_gerar_a500, nr_seq_estrutura_mat, cd_materiar_prop, ds_material_proprio, ie_mat_alternativo)
					(select	cd_material, cd_unidade_medida, sysdate, sysdate, ie_gerar_a1200,
						'A', ie_tipo_guia, nm_usuario_p, nm_usuario_p, nr_seq_nova_composicao_w, nr_seq_material,
						nr_seq_pacote, nr_seq_prestador, nr_seq_regra, pls_pacote_material_seq.nextval, qt_material, vl_material_regra,
						vl_negociado, ie_tipo_despesa, ie_gerar_a500, nr_seq_estrutura_mat, cd_materiar_prop, ds_material_proprio, ie_mat_alternativo
					from	pls_pacote_material
					where	nr_seq_composicao = nr_seq_composicao_p);
					
			nr_seq_gerada_p := nr_seq_nova_composicao_w;
			
			commit;
		end if;
	end if;
else
	-- Por favor selecione uma composicao valida!
	wheb_mensagem_pck.Exibir_mensagem_abort(251256);
end if;

end pls_pacotes_duplica_composicao;
/