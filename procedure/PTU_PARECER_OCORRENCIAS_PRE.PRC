create or replace
procedure ptu_parecer_ocorrencias_pre
		(	nr_seq_fatura_p		Number,
			cd_estabelecimento_p	Number,
			nm_usuario_p		Varchar2		) is

/*
Diego 21/07/2011

Rotina que ira atualizar os pareceres  das ocorr�ncias de pr�- an�lise de um arquivo que faltam ser  liberadas.

*/			

nr_seq_analise_w		number(10);
nr_seq_lote_w			number(10);
nr_seq_analise_item_w		number(10);
nr_seq_mot_liberacao_w		number(10);
nr_seq_analise_conta_item_w	Number(10);
nr_seq_conta_proc_w		Number(10);
nr_seq_conta_mat_w		Number(10);
nr_seq_glosa_oc_w		Number(10);
ie_tipo_w			Varchar2(10);
nr_seq_conta_w			Number(10);

ie_status_w			varchar2(1);

Cursor C01 is
	select	b.nr_seq_analise
	from	pls_protocolo_conta a,
		pls_conta b
	where	a.nr_seq_lote_conta	= nr_seq_lote_w
	and	b.nr_seq_protocolo	= a.nr_sequencia
	group by b.nr_seq_analise
	order by 1;
	
Cursor C02 is
	select	a.nr_sequencia,			
		a.nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		a.nr_seq_conta,
		a.nr_seq_analise,
		a.nr_seq_glosa_oc,
		a.ie_tipo,
		a.ie_status
	from	pls_analise_conta_item a		
	where	a.nr_seq_analise = nr_seq_analise_w
	and	a.ie_pre_analise = 'S'
	and	ie_status <> 'I'
	and not exists (select	b.nr_sequencia
			from	pls_analise_parecer_item b
			where	b.nr_seq_item = a.nr_sequencia);
			
begin

select	max(b.nr_seq_lote_conta)
into	nr_seq_lote_w
from	ptu_fatura a,
	pls_protocolo_conta b
where	a.nr_sequencia 	   = nr_seq_fatura_p
and	a.nr_seq_protocolo = b.nr_sequencia;

open C01;
loop
fetch C01 into
	nr_seq_analise_w;
exit when C01%notfound;
	begin
	
	 --askono -- OS 396221 - item 1 -  SE EXISTIREM PEND�NCIAS, ESTAS N�O SER�O  MAIS LIBERADAS NO MOMENTO DE ENVIO PARA A AN�LISE. SER�O ENVIADAS COM PEND�NCIA.
	 -- Os itens de glosa/ocorr�ncia ser�o transferidos para a an�lise da maneira comom est�o. 
	/*
	open C02;
	loop
	fetch C02 into	
		nr_seq_analise_conta_item_w,
		nr_seq_conta_proc_w,
		nr_seq_conta_mat_w,
		nr_seq_conta_w,
		nr_seq_analise_w,
		nr_seq_glosa_oc_w,
		ie_tipo_w,
		ie_status_w;
	exit when C02%notfound;
		begin
		

		select	max(nr_sequencia)
		into	nr_seq_mot_liberacao_w
		from	pls_mot_lib_analise_conta
		where	ie_situacao		= 'A'
		and	nvl(ie_pre_analise,'N')	= 'S';
		
		if	(nr_seq_mot_liberacao_w is not null) then
			pls_conta_liberar_glosa_oc(nr_seq_analise_conta_item_w, nr_seq_conta_proc_w, nr_seq_conta_mat_w,
						   nr_seq_conta_w, nr_seq_analise_w, nr_seq_glosa_oc_w,
						   ie_tipo_w, nr_seq_mot_liberacao_w, '', nm_usuario_p, 
						   cd_estabelecimento_p, null, null,
						   'N', 'N');
		else
			aise_application_error(-20011,'N�o existe motivo libera��o padr�o de ocorr�ncias de pr�-an�lise.(Cadastros Gerais / Plano de Sa�de / OPS - Contas m�dicas / Motivos de libera��o da an�lise da conta / Interc�mbio (Pr�-An�lise)).'||'#@#@');
		end if;
		
		end;
	end loop;
	close C02;
	*/
	update	pls_auditoria_conta_grupo
	set	dt_liberacao	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_seq_analise	= nr_seq_analise_w
	and	ie_pre_analise	= 'S'
	and	dt_liberacao is null;
	
	pls_atualizar_grupo_penden(nr_seq_analise_w, cd_estabelecimento_p, nm_usuario_p);
	
	end;
end loop;
close C01;

commit;

end ptu_parecer_ocorrencias_pre;
/