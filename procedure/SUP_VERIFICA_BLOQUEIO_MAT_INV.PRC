create or replace
procedure sup_verifica_bloqueio_mat_inv(cd_estabelecimento_p	in	number,
					dt_mesano_referencia_p	in	date,
					cd_local_estoque_p	in	number,
					cd_material_estoque_p	in	number,
					cd_fornecedor_p		in	varchar2,
					ie_consignado_p		in	varchar2,
					ie_bloqueio_p		out	varchar2) is 

dt_mesano_consulta_w	date;					
dt_mesano_referencia_w	date;
dt_mesano_vigente_w	date;
qt_existe_w		number(5);

ie_bloqueio_inventario_w	varchar2(1);

begin
dt_mesano_consulta_w := pkg_date_utils.start_of(nvl(dt_mesano_referencia_p,sysdate),'MONTH',0);

select	max(dt_mesano_vigente)
into	dt_mesano_vigente_w
from 	parametro_estoque
where	cd_estabelecimento	= cd_estabelecimento_p;


if	(ie_consignado_p = 'N') then
	begin
	
	begin
	select	nvl(a.ie_bloqueio_inventario,'N')
	into	ie_bloqueio_inventario_w
	from	saldo_estoque a
	where	a.dt_mesano_referencia	= dt_mesano_consulta_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_local_estoque	= cd_local_estoque_p
	and	a.cd_material		= cd_material_estoque_p;
	exception
		when others then
		begin
		ie_bloqueio_inventario_w := 'N';
		end;
	end;
		
	if	(ie_bloqueio_inventario_w = 'S') then
		begin
		select	count(*)
		into	qt_existe_w
		from	inventario a,
			inventario_material b
		where	a.nr_sequencia = b.nr_seq_inventario
		and	a.ie_consignado = 'N'
		and	b.cd_material = cd_material_estoque_p
		and	a.cd_local_estoque = cd_local_estoque_p
		and	a.dt_atualizacao_saldo is null
		and	a.dt_bloqueio is not null;
		
		if	(qt_existe_w = 0) then
			begin
			select	/*+ index (s salesto_i2) */
				nvl(max(s.dt_mesano_referencia), pkg_date_utils.start_of(sysdate, 'MONTH',0))
			into	dt_mesano_referencia_w
			from 	saldo_estoque s
			where	s.ie_bloqueio_inventario	= 'S'
			and	s.cd_estabelecimento	= cd_estabelecimento_p
			and 	s.cd_material		= cd_material_estoque_p
			and	s.cd_local_estoque	= cd_local_estoque_p
			and 	s.dt_mesano_referencia	>= dt_mesano_vigente_w;

			update	saldo_estoque
			set	ie_bloqueio_inventario	= 'N'
			where	cd_estabelecimento	= cd_estabelecimento_p
			and	cd_local_estoque	= cd_local_estoque_p
			and	cd_material		= cd_material_estoque_p
			and	dt_mesano_referencia	<= dt_mesano_referencia_w;
			
			ie_bloqueio_inventario_w := 'N';
			end;
		end if;
		end;
	end if;
	end;
else
	begin
	
	begin
	select	nvl(a.ie_bloqueio_inventario,'N')
	into	ie_bloqueio_inventario_w
	from	fornecedor_mat_consignado a
	where	a.dt_mesano_referencia	= dt_mesano_consulta_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_local_estoque	= cd_local_estoque_p
	and	a.cd_material		= cd_material_estoque_p
	and	a.cd_fornecedor		= cd_fornecedor_p;
	exception
		when others then
		begin
		ie_bloqueio_inventario_w := 'N';
		end;
	end;	
	
		
	if	(ie_bloqueio_inventario_w = 'S') then
		begin
		select	count(*)
		into	qt_existe_w
		from	inventario a,
			inventario_material b
		where	a.nr_sequencia = b.nr_seq_inventario
		and	a.ie_consignado = 'S'
		and	nvl(b.cd_fornecedor, a.cd_fornecedor) = cd_fornecedor_p
		and	b.cd_material = cd_material_estoque_p
		and	a.cd_local_estoque = cd_local_estoque_p
		and	a.dt_atualizacao_saldo is null
		and	a.dt_bloqueio is not null;
		
		if	(qt_existe_w = 0) then
			begin
			select	/*+ index (s salesto_i2) */
				nvl(max(s.dt_mesano_referencia), pkg_date_utils.start_of(sysdate, 'MONTH',0))
			into	dt_mesano_referencia_w
			from 	fornecedor_mat_consignado s
			where	s.ie_bloqueio_inventario	= 'S'
			and	s.cd_estabelecimento	= cd_estabelecimento_p
			and 	s.cd_material		= cd_material_estoque_p
			and	s.cd_local_estoque	= cd_local_estoque_p
			and	s.cd_fornecedor		= cd_fornecedor_p
			and 	s.dt_mesano_referencia	>= dt_mesano_vigente_w;

			update	fornecedor_mat_consignado
			set	ie_bloqueio_inventario	= 'N'
			where	cd_estabelecimento		= cd_estabelecimento_p
			and	cd_local_estoque		= cd_local_estoque_p
			and	cd_fornecedor		= cd_fornecedor_p
			and	cd_material		= cd_material_estoque_p
			and	dt_mesano_referencia	<= dt_mesano_referencia_w;
			
			ie_bloqueio_inventario_w := 'N';
			end;
		end if;
		end;
	end if;	
	end;
end if;

ie_bloqueio_p := ie_bloqueio_inventario_w;
end sup_verifica_bloqueio_mat_inv;
/