create or replace
procedure gerar_relat_eis_evol(nm_usuario_p		Varchar2) is 

ds_erro_w varchar2(255);
begin

delete w_eis_evol_relat;

EXEC_SQL_DINAMICO('TASY', ' insert into w_eis_evol_relat(NR_SEQUENCIA,DT_ATUALIZACAO,DS_DIMENSAO,CD_DIMENSAO,VL_DATA,DT_DATA,DS_MASCARA) (select w_eis_evol_relat_seq.nextval,sysdate,DS_DIMENSAO,CD_DIMENSAO,VL_DATA,DT_DATA,DS_MASCARA from w_eis_evol_'|| elimina_caractere_especial(LOWER(replace(nm_usuario_p, ' ', '')))||')');

commit;

end gerar_relat_eis_evol;
/