create or replace 
procedure pls_alt_vigencia_pagador_seg
			(	nr_seq_seg_pagador_p	number,
				dt_inicio_vigencia_p	date,
				nm_usuario_p		varchar2) is

nr_seq_pagador_antigo_w		number(10);
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
dt_fim_vigencia_antiga_w	date;
dt_inicio_vigencia_antiga_w	date;

begin

select	max(nr_seq_segurado)
into	nr_seq_segurado_w
from	pls_segurado_pagador
where	nr_sequencia	= nr_seq_seg_pagador_p;

-- Pagador anterior, se tiver
select	max(nr_sequencia)
into	nr_seq_pagador_antigo_w
from	pls_segurado_pagador
where	nr_sequencia < nr_seq_seg_pagador_p
and	nr_seq_segurado = nr_seq_segurado_w;

-- Se tiver mais de um registro na tela ...
if	(nr_seq_pagador_antigo_w is not null) then
	
	-- Seleciona a data do fim de vig�ncia do pagador antigo antes de atualiz�-la
	select	dt_fim_vigencia
	into	dt_fim_vigencia_antiga_w
	from	pls_segurado_pagador
	where	nr_sequencia = nr_seq_pagador_antigo_w;
	
	-- Atualiza a data do fim de vig�ncia do contrato anterior
	update	pls_segurado_pagador
	set	dt_fim_vigencia = fim_dia(dt_inicio_vigencia_p - 1)
	where	nr_sequencia = nr_seq_pagador_antigo_w;
	
	insert	into	pls_segurado_pagador_hist (
		nr_sequencia,
		ds_historico,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_historico,
		nm_usuario,
		nm_usuario_historico,
		nm_usuario_nrec,
		nr_seq_seg_pagador)
	values	(
		pls_segurado_pagador_hist_seq.nextval,
		to_char('Alterada a data de final de vig�ncia de ' || dt_fim_vigencia_antiga_w || ' para ' || (dt_inicio_vigencia_p - 1)),
		sysdate,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_pagador_antigo_w);
end if;

-- Seleciona a data de in�cio de vig�ncia do pagador selecionado antes de atualiz�-la
select	dt_inicio_vigencia
into	dt_inicio_vigencia_antiga_w
from	pls_segurado_pagador
where	nr_sequencia = nr_seq_seg_pagador_p;

-- Atualiza a data de in�cio de vig�ncia do contrato que est� sendo alterado
update	pls_segurado_pagador
set	dt_inicio_vigencia = trunc(dt_inicio_vigencia_p,'dd')
where	nr_sequencia = nr_seq_seg_pagador_p;

insert	into	pls_segurado_pagador_hist (
	nr_sequencia,
	ds_historico,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_historico,
	nm_usuario,
	nm_usuario_historico,
	nm_usuario_nrec,
	nr_seq_seg_pagador)
values	(
	pls_segurado_pagador_hist_seq.nextval,
	to_char('Alterada a data de in�cio de vig�ncia de ' || dt_inicio_vigencia_antiga_w || ' para ' || (dt_inicio_vigencia_p)),
	sysdate,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	nm_usuario_p,
	nr_seq_seg_pagador_p);

commit;

end pls_alt_vigencia_pagador_seg;
/