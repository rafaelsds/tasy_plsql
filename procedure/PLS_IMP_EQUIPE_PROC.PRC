create or replace 
procedure pls_imp_equipe_proc(	nr_crm_imp_p		pls_proc_participante.nr_crm_imp%type,
				ie_funcao_medico_imp_p	pls_proc_participante.ie_funcao_medico_imp%type,
				cd_cbo_saude_imp_p	pls_proc_participante.cd_cbo_saude_imp%type,
				uf_crm_imp_p		pls_proc_participante.uf_crm_imp%type,
				sg_conselho_imp_p	pls_proc_participante.sg_conselho_imp%type,
				nm_medico_exec_imp_p	pls_proc_participante.nm_medico_executor_imp%type,
				nr_cpf_imp_p		pls_proc_participante.nr_cpf_imp%type,
				nr_seq_conta_proc_p	pls_proc_participante.nr_seq_conta_proc%type,
				cd_interno_prestador_p	pls_proc_participante.cd_prestador_imp%type,
				cd_grau_partic_imp_p	pls_proc_participante.cd_grau_partic_imp%type,
				nm_usuario_p		usuario.nm_usuario%type) is

nr_seq_prestador_w		pls_proc_participante.nr_seq_prestador%type;
cd_prest_upper_conv_w 		pls_protocolo_conta_imp.cd_prest_upper_conv%type;
cd_prest_number_conv_w	 	pls_protocolo_conta_imp.cd_prest_number_conv%type;
cd_cgc_prestador_conv_w 	pls_protocolo_conta_imp.cd_cgc_prestador_conv%type;
cd_cpf_prestador_conv_w 	pls_protocolo_conta_imp.cd_cpf_prestador_conv%type;
cd_prestador_conv_w	 	pls_protocolo_conta_imp.cd_prestador_conv%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_prestador_participante_w	pls_param_importacao_conta.ie_prestador_participante%type;
nr_cpf_imp_w			pls_proc_participante.nr_cpf_imp%type;
ie_prestador_codigo_w		pls_param_importacao_conta.ie_prestador_codigo%type;
cd_prestador_imp_w		pls_proc_participante.cd_prestador_imp%type;
dt_procedimento_referencia_w	pls_conta_proc.dt_procedimento_referencia%type;
begin

if	(nr_seq_conta_proc_p is not null) then

	select	max(b.cd_estabelecimento),
		max(a.dt_procedimento_referencia)
	into	cd_estabelecimento_w,
		dt_procedimento_referencia_w
	from	pls_conta_proc a,
		pls_conta b
	where	a.nr_sequencia = nr_seq_conta_proc_p
	and	b.nr_sequencia = a.nr_seq_conta;
	
	select	nvl(max(ie_prestador_participante), 'S'),
		nvl(max(ie_prestador_codigo),'N')
	into	ie_prestador_participante_w,
		ie_prestador_codigo_w
	from	pls_param_importacao_conta
	where	cd_estabelecimento = cd_estabelecimento_w;

	-- se for para utilizar apenas a tag codigo prestador na operadora limpa o campo cpf
	if	(ie_prestador_participante_w = 'S') then
		nr_cpf_imp_w := null;
	else
		nr_cpf_imp_w := nr_cpf_imp_p;
	end if;
	cd_prestador_imp_w	:= cd_interno_prestador_p;
	
	if	(ie_prestador_codigo_w	= 'S') then
		if	(length(cd_interno_prestador_p) = 14) or
			(length(cd_interno_prestador_p) = 11) then
			cd_prestador_imp_w	:=  null;
		end if;
	end if;
	
	nr_seq_prestador_w := pls_conv_xml_cta_pck.obter_prestador_tasy(null, cd_prestador_imp_w, 
									null, nr_cpf_imp_w, 
									cd_estabelecimento_w, dt_procedimento_referencia_w,
									cd_prestador_conv_w, cd_prest_upper_conv_w, 
									cd_prest_number_conv_w, cd_cgc_prestador_conv_w, 
									cd_cpf_prestador_conv_w);

	insert into pls_proc_participante (
		nr_sequencia, nm_usuario, dt_atualizacao,
		nm_usuario_nrec, dt_atualizacao_nrec, nr_seq_conta_proc,
		nr_cpf_imp, nm_medico_executor_imp, sg_conselho_imp,
		nr_crm_imp, uf_crm_imp, ie_funcao_medico_imp,
		cd_cbo_saude_imp, cd_prestador_imp, ie_status,
		cd_grau_partic_imp, nr_seq_prestador
	) values (
		pls_proc_participante_seq.nextval, nm_usuario_p, sysdate,
		nm_usuario_p, sysdate, nr_seq_conta_proc_p,
		nr_cpf_imp_p, nm_medico_exec_imp_p, sg_conselho_imp_p,
		nr_crm_imp_p, uf_crm_imp_p, ie_funcao_medico_imp_p,
		cd_cbo_saude_imp_p, cd_interno_prestador_p, 'U',
		cd_grau_partic_imp_p, nr_seq_prestador_w
	);
end if;

commit;

end pls_imp_equipe_proc;
/
