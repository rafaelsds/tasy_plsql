create or replace
procedure reverter_lote_fornec_adm(	nr_seq_horario_p	Number,
									nr_prescricao_p		Number,
									nr_seq_item_p		Number,
									dt_horario_p		Date,	
									nm_usuario_p		Varchar2,
									ie_tipo_item_p		Varchar2 default null) is 

nr_seq_lote_adm_w	administracao_lote_item.nr_seq_lote_adm%type;
nr_seq_horario_w	prescr_mat_hor.nr_sequencia%type;
qt_material_w		administracao_lote_item.qt_material%type;
																
cursor c01 is
select	a.nr_sequencia		
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material	= b.nr_sequencia
and		b.nr_prescricao		= nr_prescricao_p
and		b.nr_sequencia_diluicao	= nr_seq_item_p
and		a.dt_horario		= dt_horario_p
and		b.ie_agrupador		in (3,7,9)
and		nvl(b.ie_suspenso,'N')	<> 'S'
and		nvl(a.ie_situacao,'A')	= 'A'
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
and		ie_tipo_item_p = 'M'
union
select	nr_seq_horario_p
from	dual							
where	ie_tipo_item_p = 'M'
union
select	a.nr_sequencia		
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material	= b.nr_sequencia
and		b.nr_prescricao		= nr_prescricao_p
and		a.dt_horario		= dt_horario_p
and		b.nr_sequencia_proc = nr_seq_item_p
and		b.ie_agrupador		in (5)
and		nvl(b.ie_suspenso,'N')	<> 'S'
and		nvl(a.ie_situacao,'A')	= 'A'
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
and		ie_tipo_item_p in ('IA', 'C', 'G')
union
select	a.nr_sequencia		
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material	= b.nr_sequencia
and		b.nr_prescricao		= nr_prescricao_p
and		a.dt_horario		= dt_horario_p
and		b.nr_seq_gasoterapia = nr_seq_item_p
and		b.ie_agrupador		in (15)
and		nvl(b.ie_suspenso,'N')	<> 'S'
and		nvl(a.ie_situacao,'A')	= 'A'
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
and		ie_tipo_item_p in ('GAS')
union
select	a.nr_sequencia		
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material	= b.nr_sequencia
and		b.nr_prescricao		= nr_prescricao_p
and		a.dt_horario		= dt_horario_p
and		b.nr_sequencia      = nr_seq_item_p
and		b.ie_agrupador		in (8)
and		nvl(b.ie_suspenso,'N')	<> 'S'
and		nvl(a.ie_situacao,'A')	= 'A'
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
and		ie_tipo_item_p in ('SNE');
								
begin

open C01;
loop
fetch C01 into	
	nr_seq_horario_w;	
exit when C01%notfound;
	begin
		
	--Busca a qtde de cada horario 
	select	coalesce(sum(qt_material),0),
			coalesce(max(nr_seq_lote_adm),0)
	into	qt_material_w,
			nr_seq_lote_adm_w
	from	administracao_lote_item
	where	nr_seq_horario = nr_seq_horario_w
	and		ie_situacao = 'S';
	
	if	(qt_material_w > 0) and (nr_seq_lote_adm_w > 0) then
		
		--Atualiza situacao do registro selecionado 
		update	administracao_lote_item
		set		ie_situacao = 'N'
		where	nr_seq_horario = nr_seq_horario_w;
		
		--Decrementa a qtde total do material
		update	administracao_lote_fornec
		set		qt_material = qt_material - qt_material_w
		where	nr_sequencia = nr_seq_lote_adm_w;
		
		
		if	(ie_tipo_item_p = 'M') then
			update 	prescr_mat_alteracao
			set		ie_evento_valido = 'N'
			where	nr_seq_horario = nr_seq_horario_w
			and		ie_alteracao = 58;
		elsif	(ie_tipo_item_p in ('IA', 'C', 'G')) then		
			update 	prescr_mat_alteracao
			set		ie_evento_valido = 'N'
			where	nr_prescricao = nr_prescricao_p
			and		nr_seq_procedimento = nr_seq_item_p
			and		nr_seq_horario_proc = nr_seq_horario_p
			and		ie_alteracao = 58;			
			
		elsif	(ie_tipo_item_p = 'GAS') then		
			update	prescr_gasoterapia_evento
			set		ie_evento_valido = 'N'
			where	nr_seq_horario = nr_seq_horario_p
			and		nr_seq_gasoterapia = nr_seq_item_p
			and 	ie_evento = 'CR';
		end if;
		
		
	end if;
	
	if	(ie_tipo_item_p in ('SNE')) then		
		update 	prescr_solucao_evento
		set		ie_evento_valido = 'N'
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_material = nr_seq_item_p			
		and		ie_alteracao = 54
		and		dt_horario = dt_horario_p
		and		ie_evento_valido = 'S';
	end if;
		
	end;
end loop;
close C01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end reverter_lote_fornec_adm;
/	