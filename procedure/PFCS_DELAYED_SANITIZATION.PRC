create or replace
procedure pfcs_delayed_sanitization( 	nr_seq_indicator_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p	varchar2) is

cursor c01_tasy is
select	b.cd_setor_atendimento cd_department,
	b.ds_setor_atendimento ds_department,
	c.cd_unidade_basica || ' ' || c.cd_unidade_compl ds_location,
	substr(obter_valor_dominio(82, c.ie_status_unidade),1,255) ds_status,
	c.ie_status_unidade ie_status,
	d.ds_servico ds_sanitization_service,
	round((sysdate - a.dt_inicio) * 1440) qt_time_sanitization
from	sl_unid_atend a,
	setor_atendimento b,
	unidade_atendimento c,
	sl_servico d
where	b.cd_setor_atendimento = a.cd_setor_higienizacao
and	b.cd_classif_setor in ('1','3','4','9','11','12')
and	b.ie_ocup_hospitalar <> 'N'
and	b.cd_setor_atendimento = c.cd_setor_atendimento
and	c.nr_seq_interno = a.nr_seq_unidade
and	d.nr_sequencia = a.nr_seq_servico
and	a.dt_fim is null
and	a.dt_cancelamento_servico is null
and	a.dt_aprovacao is null
and	obter_se_ultrapassou_temp_serv(0,a.nr_sequencia,sysdate) = 'S'
and	a.cd_estabelecimento = to_number(cd_estabelecimento_p);

cursor c01_fhir is
select sec.cd_setor_atendimento cd_department,
    sec.ds_setor_atendimento ds_department,
    uni.cd_unidade_basica || ' ' || uni.cd_unidade_compl ds_location,
	substr(obter_valor_dominio(82, uni.ie_status_unidade),1,255) ds_status,
	uni.ie_status_unidade ie_status,
    substr(obter_desc_expressao(489110),1,255) ds_sanitization_service,
    round((sysdate - uni.dt_higienizacao) * 1440) qt_time_sanitization
from
    setor_atendimento sec,
    unidade_atendimento uni
where sec.cd_setor_atendimento = uni.cd_setor_atendimento
    and sec.cd_classif_setor in ('1','3','4','9','11','12')
    and uni.ie_status_unidade = 'H'
    and uni.ie_situacao = 'A'
    and uni.nr_seq_location is not null
    and round((sysdate - uni.dt_higienizacao) * 1440) > (SELECT QT_SANITIZATION_TIME FROM PFCS_GENERAL_RULE)
    and sec.cd_estabelecimento = cd_estabelecimento_p;


qt_total_w				number(15) := 0;
pfcs_panel_detail_seq_w			pfcs_panel_detail.nr_sequencia%type;
pfcs_panel_seq_w			pfcs_panel.nr_sequencia%type;
nr_seq_operational_level_w		pfcs_operational_level.nr_sequencia%type;
pfcs_flag_settings_w            pfcs_general_rule.ie_table_origin%type;

begin

nr_seq_operational_level_w := pfcs_get_structure_level(
		cd_establishment_p => cd_estabelecimento_p,
		ie_level_p => 'O',
		ie_info_p => 'C');
		
select ie_table_origin
into pfcs_flag_settings_w
from pfcs_general_rule;

-- Tasy tables or both
if (pfcs_flag_settings_w = 0 or pfcs_flag_settings_w = 2) then

	for c01_w in c01_tasy loop
		begin

		qt_total_w := qt_total_w + 1;

		select	pfcs_panel_detail_seq.nextval
		into	pfcs_panel_detail_seq_w
		from	dual;

		insert into pfcs_panel_detail(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_situation,
			nr_seq_indicator,
			nr_seq_operational_level)
		values (
			pfcs_panel_detail_seq_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'T',
			nr_seq_indicator_p,
			nr_seq_operational_level_w);

		insert into pfcs_detail_bed(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_detail,
			ds_location,
			cd_department,
			ds_department,
			ds_status,
			ie_status,
			ds_sanitization_service,
			qt_time_sanitization)
		values (
			pfcs_detail_bed_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			pfcs_panel_detail_seq_w,
			c01_w.ds_location,
			c01_w.cd_department,
			c01_w.ds_department,
			c01_w.ds_status,
			c01_w.ie_status,
			c01_w.ds_sanitization_service,
			c01_w.qt_time_sanitization);

		commit;

		end;
	end loop;
end if;

-- Integration tables or both
if (pfcs_flag_settings_w = 1 or pfcs_flag_settings_w = 2) then
	
	for c01_w in c01_fhir loop
		begin

		qt_total_w := qt_total_w + 1;

		select	pfcs_panel_detail_seq.nextval
		into	pfcs_panel_detail_seq_w
		from	dual;

		insert into pfcs_panel_detail(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_situation,
			nr_seq_indicator,
			nr_seq_operational_level)
		values (
			pfcs_panel_detail_seq_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'T',
			nr_seq_indicator_p,
			nr_seq_operational_level_w);

		insert into pfcs_detail_bed(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_detail,
			ds_location,
			cd_department,
			ds_department,
			ds_status,
			ie_status,
			ds_sanitization_service,
			qt_time_sanitization)
		values (
			pfcs_detail_bed_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			pfcs_panel_detail_seq_w,
			c01_w.ds_location,
			c01_w.cd_department,
			c01_w.ds_department,
			c01_w.ds_status,
			c01_w.ie_status,
			c01_w.ds_sanitization_service,
			c01_w.qt_time_sanitization);

		commit;

		end;
	end loop;
end if;

pfcs_pck.pfcs_generate_results(
		vl_indicator_p => qt_total_w,
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => pfcs_panel_seq_w);

pfcs_pck.pfcs_update_detail(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => pfcs_panel_seq_w,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

pfcs_pck.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

end pfcs_delayed_sanitization;
/