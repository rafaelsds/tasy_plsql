create or replace PROCEDURE PFCS_PA_AVG_DISPOSITION(
    cd_estabelecimento_p  number,
    nm_usuario_p          varchar2) is

nr_seq_indicator_w        pfcs_panel.nr_seq_indicator%type := 152;
pfcs_panel_seq_w          pfcs_panel.nr_sequencia%type;

qt_patient_w    number(20) := 0;
qt_time_w       number(20) := 0;

BEGIN

select count(enc.nr_sequencia) nr_patient,
    sum((SYSDATE - psr.dt_authored_on) * 24 * 60 ) total_time
into qt_patient_w,
    qt_time_w
from pfcs_encounter enc,
     pfcs_service_request psr,
    pfcs_patient pat,
    unidade_atendimento uni,
    setor_atendimento sec
where uni.nr_seq_location =  psr.nr_seq_location
    and uni.cd_setor_atendimento = sec.cd_setor_atendimento
    and enc.nr_seq_patient = pat.nr_sequencia
    and enc.nr_sequencia = psr.nr_seq_encounter
    and psr.cd_service = 'E0401'
    and upper(psr.si_intent) = 'PLAN'
    and upper(psr.si_status) = 'ACTIVE'
    and sec.cd_classif_setor in ('3','4','9')
    and upper(enc.cd_admit_source) = 'EMD'
    and upper(enc.si_classif) = 'INPATIENT'
    and enc.si_status in ('PLANNED', 'ARRIVED')
    and pat.ie_active = '1'
    and sec.cd_estabelecimento = cd_estabelecimento_p;


 pfcs_pck.pfcs_generate_results(
        vl_indicator_p => qt_time_w,
        vl_indicator_aux_p => qt_patient_w,
        nr_seq_indicator_p => nr_seq_indicator_w,
        nr_seq_operational_level_p => cd_estabelecimento_p,
        nm_usuario_p => nm_usuario_p,
        nr_seq_panel_p => pfcs_panel_seq_w);

pfcs_pck.pfcs_activate_records(
    nr_seq_indicator_p => nr_seq_indicator_w,
    nr_seq_operational_level_p => cd_estabelecimento_p,
    nm_usuario_p => nm_usuario_p);

END PFCS_PA_AVG_DISPOSITION;
/
