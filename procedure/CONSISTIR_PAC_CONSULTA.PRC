create or replace
procedure consistir_pac_consulta(	cd_agenda_p		number,
					nr_seq_agenda_p		number,
					dt_agenda_p		date,
					nr_seq_consulta_p	out Number) is

nr_seq_agenda_w		Number(10) := 0;

begin

if	(nr_seq_agenda_p is not null) and
	(dt_agenda_p is not null) then
	begin
	
	select	max(nr_sequencia)
	into	nr_seq_agenda_w
	from	agenda_consulta
	where	trunc(dt_agenda,'dd') = trunc(dt_agenda_p)
	and	cd_agenda = cd_agenda_p
	and	ie_status_agenda = 'O'
	and	dt_consulta is not null
	and	nr_atendimento is not null
	and	nr_sequencia <> nr_seq_agenda_p;
	
	end;	
end if;

if	(nr_seq_agenda_w > 0) then
	nr_seq_consulta_p	:= nr_seq_agenda_w;
end if;

end consistir_pac_consulta;
/