create	or	replace	procedure	
ajustar_macro_gestao_doc_emr(NR_SEQ_CONTENT_MACRO_P	in number,	
							NM_USUARIO_P			in varchar2,	
							DS_PARAM_P				in varchar2,	
							IE_OPCAO_AJUST_P		in varchar2)	
is	
begin	
/**
ie_opcao_ajust_p 
D = Delete
I = Insert
**/
	if(	nr_seq_content_macro_p	is	not	null	)	then	
		if(	ie_opcao_ajust_p	=	'D'	)	then	
			delete from db_content_macro_param	
			WHERE  nr_seq_content_macro = nr_seq_content_macro_p; 
		elsif	(	ie_opcao_ajust_p	=	'I'	)	then	
			insert into db_content_macro_param	
						(nr_sequencia,	
						nr_seq_content_macro,	
						dt_atualizacao,	
						nm_usuario,	
						dt_atualizacao_nrec,	
						nm_usuario_nrec,	
						ds_parameter)	
			values (db_content_macro_param_seq.nextval,	
					nr_seq_content_macro_p,	
					sysdate,	
					nm_usuario_p,	
					sysdate,	
					nm_usuario_p,	
					ds_param_p);	
		end	if;	
	end	if;	
end	ajustar_macro_gestao_doc_emr;	
/	