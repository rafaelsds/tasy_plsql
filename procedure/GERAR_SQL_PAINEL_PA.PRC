create or replace
procedure GERAR_SQL_PAINEL_PA(	nr_sequencia_p		number,
								nm_usuario_p		Varchar2) is

ds_sql_w			varchar2(4000);
ds_erro_w			varchar2(4000);
col_cnt     		pls_integer;
campos_w        	dbms_sql.desc_tab;
cursor_w 			INTEGER;
nm_atributo_w		painel_pa_sql_campo.nm_atributo%type;
qt_tamanho_w		painel_pa_sql_campo.qt_tamanho%type;
nr_seq_apres_w		painel_pa_sql_campo.nr_seq_grid%type := 10;
ds_label_w			painel_pa_sql_campo.ds_atributo%type;
qt_reg_w			number(10);

begin

select	ds_sql
into	ds_sql_w
from	PAINEL_PA_SQL
where	nr_sequencia	= nr_sequencia_p;

begin
cursor_w := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(cursor_w, ds_sql_w, dbms_sql.native);
dbms_sql.describe_columns(cursor_w,col_cnt,campos_w);
DBMS_SQL.CLOSE_CURSOR(cursor_w);
exception when others then
	ds_erro_w	:= SQLERRM(SQLCODE);
	exibir_erro_abortar(obter_desc_expressao(347264) || chr(13) || chr(10) || ds_erro_w,null);
end;

for i in 1..col_cnt loop
	begin
	nm_atributo_w		:= substr(campos_w(i).col_name,1,255);
	qt_tamanho_w	   	:= substr(campos_w(i).col_max_len,1,3);
	nr_seq_apres_w		:= substr(nr_seq_apres_w + 5,1,10);
	ds_label_w			:= substr(substr(nm_atributo_w,1,1) || substr(replace(lower(nm_atributo_w),'_',' '),2,length(nm_atributo_w)),1,90);

	select	count(*)
	into	qt_reg_w
	from	PAINEL_PA_SQL_CAMPO
	where	NR_SEQ_SQL = nr_sequencia_p
	and		nm_atributo = nm_atributo_w;
	if	(qt_reg_w = 0) and (upper(nm_atributo_w) <> 'DS_COR') then
		insert into PAINEL_PA_SQL_CAMPO (
				 NR_SEQUENCIA,
				 DT_ATUALIZACAO,
				 NM_USUARIO,
				 DT_ATUALIZACAO_NREC,
				 NM_USUARIO_NREC,
				 NR_SEQ_SQL,
				 NM_ATRIBUTO,
				 DS_ATRIBUTO,
				 NR_SEQ_GRID,
				 QT_TAMANHO)
			values (
				 PAINEL_PA_SQL_CAMPO_seq.nextval,
				 sysdate,
				 nm_usuario_p,
				 sysdate,
				 nm_usuario_p,
				 nr_sequencia_p,
				 nm_atributo_w,
				 ds_label_w,
				 nr_seq_apres_w,
				 qt_tamanho_w);
	end if;

	end;
end loop;

commit;

end GERAR_SQL_PAINEL_PA;
/
