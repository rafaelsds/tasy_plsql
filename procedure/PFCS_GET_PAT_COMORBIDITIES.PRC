create or replace
procedure pfcs_get_pat_comorbidities(
	cd_estabelecimento_p	number,
	nm_usuario_p	varchar2) is

nr_seq_detail_w	number(10);
qt_commit_w		number(10) := 0;
nr_seq_operational_level_w  pfcs_operational_level.nr_sequencia%type;

type doenca is record (qtd_conf number(10), qtd_nconf number(10), nr_seq_painel number(10));
type Vetor is table of doenca index by binary_integer;

comorbidades_w 		vetor;
cd_pessoa_fisica_w 	varchar2(10);
qt_reg_w			number(10);
ind                 integer;

Cursor C01 is
	select 	b.nr_seq_indicator, a.id_patient, a.nr_encounter,
			substr(obter_sexo_pf(a.id_patient,'D'),1,50) ds_sexo,
			substr(obter_leito_atual_pac(a.nr_encounter),1,50) ds_leito,
			pfcs_get_age_range(obter_data_nascto_pf(p.cd_pessoa_fisica)) ds_age,
			obter_setor_pf(a.id_patient) ds_setor
	from	pfcs_panel_detail c, pfcs_panel b, pfcs_detail_patient a, pessoa_fisica p
	where	c.nr_seq_panel = b.nr_sequencia
	and  	a.nr_seq_detail = c.nr_sequencia
	and   	a.id_patient = p.cd_pessoa_fisica
	and		b.nr_seq_indicator in (85,88) --Pacientes positivo e nao confirmado
	and     b.nr_seq_operational_level = (select max(nr_sequencia) from pfcs_operational_level where cd_establishment = cd_estabelecimento_p); 

Cursor C02 is
	select distinct d.IE_COMORBIDADE, d.CD_DOENCA, substr(OBTER_VALOR_DOMINIO(9575, ie_comorbidade),1,255) descricao, substr(obter_desc_cid(a.cd_doenca),1,300) ds_doenca
	from   DIAGNOSTICO_DOENCA a, atendimento_paciente b, parametro_medico c, PFCS_DIAGNOSIS d
	where  a.nr_atendimento = b.nr_atendimento
	and    c.cd_estabelecimento = cd_estabelecimento_p
	and    b.cd_pessoa_fisica = cd_pessoa_fisica_w
	and    replace(d.CD_DOENCA,'.','') = replace(a.CD_DOENCA,'.','')
	and    d.IE_COMORBIDADE is not null
	and    a.DT_FIM is null
	and    (nvl(c.IE_LIB_DIAG_MEDICO,'N') = 'N' or (a.dt_liberacao is not null and a.dt_inativacao is null));

Cursor c03 is
	select ie_comorbidade, substr(OBTER_VALOR_DOMINIO(9575, ie_comorbidade),1,255) descricao
	from   PFCS_DIAGNOSIS
	where  IE_COMORBIDADE is not null
	group by ie_comorbidade;

begin
nr_seq_operational_level_w := pfcs_get_structure_level(
    cd_establishment_p => cd_estabelecimento_p,
    ie_level_p => 'O',
    ie_info_p => 'C');

--inicializar vetor
begin
select max(to_number(vl_dominio))
into   qt_reg_w
from   valor_dominio
where  cd_dominio = 9575;

while	(qt_reg_w > 0) loop
	begin
	comorbidades_w(qt_reg_w).qtd_conf := 0;
	comorbidades_w(qt_reg_w).qtd_nconf := 0;

	select pfcs_panel_seq.nextval
	into   comorbidades_w(qt_reg_w).nr_seq_painel
	from   dual;

	insert into pfcs_panel (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_situation,
		nr_seq_indicator,
		nr_seq_operational_level)
	values (
		comorbidades_w(qt_reg_w).nr_seq_painel,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'T',
		86,
		nr_seq_operational_level_w);

	qt_reg_w := qt_reg_w - 1;
	end;
end loop;
end;

qt_commit_w := 0;

for reg_atendimentos in C01 loop
	begin
	cd_pessoa_fisica_w := reg_atendimentos.id_patient;

	for reg_diagnosticos in C02 loop
	begin
	if (reg_atendimentos.nr_seq_indicator = 85) then
		--Quantidade de pacientes para aquela comorbidade com covid-19 confirmado
		comorbidades_w(to_number(reg_diagnosticos.ie_comorbidade)).qtd_conf := comorbidades_w(to_number(reg_diagnosticos.ie_comorbidade)).qtd_conf + 1;
	elsif (reg_atendimentos.nr_seq_indicator = 88) then
		--Quantidade de pacientes para aquela comorbidade com covid-19 nao confirmado
		comorbidades_w(to_number(reg_diagnosticos.ie_comorbidade)).qtd_nconf := comorbidades_w(to_number(reg_diagnosticos.ie_comorbidade)).qtd_nconf + 1;
	end if;

	--inserir para o detalhe
	begin
	qt_commit_w := qt_commit_w + 1;

	select	pfcs_panel_detail_seq.nextval
	into	nr_seq_detail_w
	from	dual;

	insert into pfcs_panel_detail (
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		ie_situation,
		nr_seq_panel,
		nr_seq_indicator,
		nr_seq_operational_level)
	values (
		nr_seq_detail_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		'T',
		comorbidades_w(to_number(reg_diagnosticos.ie_comorbidade)).nr_seq_painel,
		86,
		nr_seq_operational_level_w);

	insert into pfcs_detail_patient (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		id_patient,
		nr_encounter,
		ds_gender,
		ds_current_location,
		ds_bed_assigned,
		ds_age_range,
		ds_classification,
		cd_classification,
		nr_seq_detail,
		ds_symptoms,
		ie_confirmed)
	values (
		pfcs_detail_patient_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		reg_atendimentos.id_patient,
		reg_atendimentos.nr_encounter,
		reg_atendimentos.ds_sexo,
		reg_atendimentos.ds_setor,
		reg_atendimentos.ds_leito,
		reg_atendimentos.ds_age,
		reg_diagnosticos.descricao,
		reg_diagnosticos.ie_comorbidade,
		nr_seq_detail_w,
		reg_diagnosticos.CD_DOENCA || ' - ' || reg_diagnosticos.ds_doenca,
		decode(reg_atendimentos.nr_seq_indicator,85,'S','N'));

	if (qt_commit_w > 500) then
		begin
		qt_commit_w := 0;
		commit;
		end;
	end if;
	end;

	end;
	end loop;
end;
end loop;

-- Atualizar comorbidades (CARD)
for reg_comorbidade in C03 loop
	begin

	update  pfcs_panel
	set     cd_reference_value = reg_comorbidade.ie_comorbidade,
                ds_reference_value = reg_comorbidade.descricao,
                cd_reference_aux = reg_comorbidade.ie_comorbidade,
                ds_reference_aux = reg_comorbidade.descricao,
                vl_indicator = comorbidades_w(reg_comorbidade.ie_comorbidade).qtd_conf,
                vl_indicator_aux = comorbidades_w(reg_comorbidade.ie_comorbidade).qtd_nconf
	where   nr_sequencia = comorbidades_w(reg_comorbidade.ie_comorbidade).nr_seq_painel;

end;
end loop;


pfcs_pck.pfcs_activate_records(
	nr_seq_indicator_p => 86,
	nr_seq_operational_level_p => nr_seq_operational_level_w,
	nm_usuario_p => nm_usuario_p);

end pfcs_get_pat_comorbidities;
/