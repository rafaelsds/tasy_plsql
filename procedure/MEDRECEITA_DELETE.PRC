create or replace
procedure MEDRECEITA_DELETE( nr_seq_med_receita_p med_receita.nr_sequencia%type)
is

begin
	delete 
	from  med_principio_ativo 
	where nr_sequencia_item in (
		select nr_sequencia
		from  med_receita_item 
		where nr_seq_med_receita = nr_seq_med_receita_p
	);

	delete 
	from  med_receita_item 
	where nr_seq_med_receita = nr_seq_med_receita_p;
	
	delete 
	from  med_receita
	where nr_sequencia = nr_seq_med_receita_p;
	
	commit;

end MEDRECEITA_DELETE;
/