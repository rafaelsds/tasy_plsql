create or replace
procedure HAOC_ctb_gerar_razao_gerencial(	dt_inicial_p		date,
				dt_final_p			date,
				cd_estab_p		number,
 				cd_centro_custo_p		varchar2,
 				cd_conta_contabil_p	varchar2,
				cd_classif_centro_p		varchar2,
				cd_classif_conta_p		varchar2,
				nm_usuario_p		varchar2,
				ie_opcao_p		number,
				ie_tipo_resultado_p		varchar2,
				cd_conta_contabil_codigo_p	varchar2,
				ie_quebra_mes_p		varchar2,
				cd_empresa_p		number,
				ie_consolida_empresa_p	varchar2) is

dt_movimento_w			date;
cd_contrapartida_w			varchar2(040);
cd_conta_contabil_w		varchar2(040);
cd_centro_custo_w			number(08,0);
ds_conta_w			varchar2(255);
cd_classif_conta_w			varchar2(40);
cd_classif_centro_w		varchar2(40); 
ds_historico_w			varchar2(255);
ds_compl_historico_w		varchar2(255);
ie_debito_credito_w			varchar2(1);
cd_historico_w			number(10); 
vl_credito_w			number(15,2);
vl_debito_w			number(15,2);
vl_movimento_w			number(15,2);
vl_saldo_ant_w			number(15,2);
vl_saldo_w			number(15,2);
vl_movto_saldo_w			number(15,2);

ie_gerar_w			varchar2(1);
ie_pos_w				number(15,0);
cd_classif_w			varchar2(255);
cd_classif_ww			varchar2(255);
nr_sequencia_w			number(15,0);
ie_movimento_w			varchar2(01);
ie_deb_cred_w			varchar2(01);

cd_clas_conta_w			varchar2(255);
cd_clas_conta_ww			varchar2(255);
cd_clas_centro_w			varchar2(255);
cd_clas_centro_ww			varchar2(255);
ie_tem_movimento_w		varchar2(001);
nr_lote_contabil_w			number(10,0);
dt_referencia_w			date;
cd_estabelecimento_w		number(04,0);
cd_estab_w			number(04,0);
dt_final_w			date;
dt_inicial_w			date;
nr_seq_movto_w			number(10);


cursor	c001	is
	select	decode(nvl(ie_quebra_mes_p,'N'),'S',r.dt_referencia,to_date(null)) dt_referencia,
		s.cd_conta_contabil,
		substr(c.cd_conta_contabil  || ' - ' || nvl(s.cd_classificacao, c.cd_classificacao) || '  ' || c.ds_conta_contabil,1,255),
		s.cd_centro_custo,
		d.ie_debito_credito,
		sum(abs(vl_movimento)),
		sum(decode(r.dt_referencia,trunc(dt_final_w, 'month'), vl_saldo,0)),
		decode(cd_estab_w,0,null,s.cd_estabelecimento) cd_estabelecimento
	from	ctb_grupo_conta d,
		conta_contabil c,
		ctb_saldo s,
		ctb_mes_ref r
	where	s.nr_seq_mes_ref		= r.nr_sequencia
	and	s.cd_conta_contabil	= c.cd_conta_contabil 
	and	c.cd_grupo		= d.cd_grupo
	and	r.dt_referencia between trunc(dt_inicial_w, 'month')  and trunc(dt_final_w, 'month')
	and	c.ie_tipo		<> 'T'
	and	((d.ie_tipo in ('R','C','D') and nvl(ie_tipo_resultado_p, 'S') = 'S') or (nvl(ie_tipo_resultado_p, 'S') = 'N'))
	and	s.cd_centro_custo is not null
	and	r.cd_empresa	= cd_empresa_p
	and	((s.cd_estabelecimento	= cd_estab_w) or (cd_estab_w = 0))
	group by decode(nvl(ie_quebra_mes_p,'N'),'S',r.dt_referencia,to_date(null)),
		s.cd_conta_contabil,
		substr(c.cd_conta_contabil  || ' - ' || nvl(s.cd_classificacao, c.cd_classificacao) || '  ' || c.ds_conta_contabil,1,255),
		s.cd_centro_custo,
		d.ie_debito_credito,
		decode(cd_estab_w,0,null,s.cd_estabelecimento)
	order by decode(nvl(ie_quebra_mes_p,'N'),'S',r.dt_referencia,to_date(null));

cursor	c002	is
	select	'M',
		a.dt_movimento, 
		a.ds_historico,
		a.ie_debito_credito,
		a.ds_compl_historico,
		a.cd_contrapartida,
		a.cd_historico,
		decode(a.vl_credito,0,0, c.vl_movimento) vl_credito,
		decode(a.vl_debito,0,0, c.vl_movimento) vl_debito,
		c.vl_movimento,
		b.nr_lote_contabil,
		a.nr_sequencia
	from	ctb_movto_centro_custo c,
		lote_contabil b,
		ctb_movimento_v a
	where 	a.dt_movimento	between trunc(dt_inicial_w,'dd')  and trunc(dt_final_w,'dd') + 86399 / 86400
	and	a.cd_conta_contabil	= cd_conta_contabil_w
	and	c.cd_centro_custo	= cd_centro_custo_w
	and	a.nr_lote_contabil	= b.nr_lote_contabil
	and	b.dt_atualizacao_saldo is not null
	and	a.nr_sequencia		= c.nr_seq_movimento
	and	(((trunc(ctb_obter_mes_ref(a.nr_seq_mes_ref),'mm') = trunc(dt_referencia_w,'mm')) and nvl(ie_quebra_mes_p,'N') = 'S') 
		or (nvl(ie_quebra_mes_p,'N') = 'N'))
	union all 
	select	'S',
		sysdate,
		'Saldo Anterior',
		'', '', '', 0, 0, 0, 0, 0, 0
	from	dual
	order by 1 desc, 2;

begin

cd_estab_w	:= cd_estab_p;
dt_inicial_w	:= dt_inicial_p;
dt_final_w	:= nvl(dt_final_p, fim_mes(dt_inicial_w));
vl_saldo_ant_w	:= null;
if 	(nvl(ie_consolida_empresa_p,'N') = 'S') then
	cd_estab_w	:= 0;
end if;
exec_sql_dinamico(nm_usuario_p,'truncate table w_ctb_razao');

open  c001;
loop
	fetch c001 into	
		dt_referencia_w,
		cd_conta_contabil_w,
		ds_conta_w,
		cd_centro_custo_w,
		ie_deb_cred_w,
		vl_movimento_w,
		vl_saldo_w,
		cd_estabelecimento_w;
	exit when c001%notfound;	
	ie_gerar_w	:= 'S';

	if  	(ie_gerar_w = 'S') and
	  	(nvl(cd_centro_custo_p, '0') <> '0') then
		select	ctb_obter_se_centro_contido(cd_centro_custo_w, cd_centro_custo_p)
		into	ie_gerar_w
		from	dual;
	end if; 
	if  	(ie_gerar_w = 'S') and
	  	(nvl(cd_conta_contabil_p,'0')	<> '0') then
		select	ctb_obter_se_conta_contida(cd_conta_contabil_w, cd_conta_contabil_p)
		into	ie_gerar_w
		from	dual;
	end if; 
	if  	(ie_gerar_w = 'S') and
	  	(nvl(cd_conta_contabil_codigo_p,'0')	<> '0') then				
		select	ctb_obter_se_conta_contida(cd_conta_contabil_w, cd_conta_contabil_codigo_p)
		into	ie_gerar_w
		from	dual;
	end if; 

	if  	(ie_gerar_w = 'S') and
	  	(nvl(cd_classif_centro_p,'0') <> '0')  then
		begin 
		ie_gerar_w		:= 'N';
		cd_clas_centro_w	:= cd_classif_centro_p;
		cd_clas_centro_w	:= replace(cd_clas_centro_w,'(','');
		cd_clas_centro_w	:= replace(cd_clas_centro_w,')','');
		cd_clas_centro_w	:= replace(cd_clas_centro_w,' ','');
		while (ie_gerar_w = 'N') and (length(cd_clas_centro_w) > 0)  loop
			begin
			ie_pos_w 	:= instr(cd_clas_centro_w,',');
			if	(ie_pos_w = 0) then
				cd_clas_centro_ww	:= cd_clas_centro_w;
				cd_clas_centro_w	:= '';
			else
				cd_clas_centro_ww	:= substr(cd_clas_centro_w,1, ie_pos_w - 1);
				cd_clas_centro_w	:= substr(cd_clas_centro_w, ie_pos_w + 1, 255);
			end if;
			select ctb_obter_se_centro_sup(cd_centro_custo_w, cd_clas_centro_ww)
			into	ie_gerar_w
			from dual;
			end;
		end loop;	
		end;
	end if; 

	if  	(ie_gerar_w = 'S') and
	  	(nvl(cd_classif_conta_p,'0') <> '0')  then
		begin
		ie_gerar_w	:= 'N';
		cd_clas_conta_w	:= cd_classif_conta_p;
		cd_clas_conta_w	:= replace(cd_clas_conta_w,'(','');
		cd_clas_conta_w	:= replace(cd_clas_conta_w,')','');
		cd_clas_conta_w	:= replace(cd_clas_conta_w,' ','');
		while (ie_gerar_w = 'N') and (length(cd_clas_conta_w) > 0)  loop
			begin
			ie_pos_w 	:= instr(cd_clas_conta_w,',');
			if	(ie_pos_w = 0) then
				cd_clas_conta_ww	:= cd_clas_conta_w;
				cd_clas_conta_w		:= '';
			else
				cd_clas_conta_ww	:= substr(cd_clas_conta_w,1, ie_pos_w - 1);
				cd_clas_conta_w	:= substr(cd_clas_conta_w, ie_pos_w + 1, 255);
			end if;
			select ctb_obter_se_conta_classif_sup(cd_conta_contabil_w, cd_clas_conta_ww)
			into	ie_gerar_w
			from dual;
			end;
		end loop;	
		end;
	end if; 

/*	op��o marcus 16/06/2004
	1 - todas contas
	2 - com movimento
	3 - com saldo
*/
	if 	(ie_gerar_w	= 'S')  then
		if	(ie_opcao_p = 2) and
			(vl_movimento_w = 0) then
			ie_gerar_w	:= 'N';
		elsif	(ie_opcao_p = 3) and
			(vl_saldo_w = 0) then
			ie_gerar_w	:= 'N';
		end if;
	end if;
	
	if 	(ie_gerar_w	= 'S')  then
		begin

		select 	sum(nvl((nvl(s.vl_saldo,0) - nvl(s.vl_movimento,0)),0))
		into	vl_saldo_w
		from	ctb_saldo s,
			ctb_mes_ref r
		where	s.nr_seq_mes_ref	= r.nr_sequencia
		and	r.dt_referencia		= trunc(dt_inicial_w, 'month')
		and	s.cd_conta_contabil	= cd_conta_contabil_w
		and	s.cd_centro_custo		= cd_centro_custo_w; 

		if	(to_char(dt_inicial_w,'dd') = '01') then
			vl_movto_saldo_w	:= 0;
		else
			select	sum(decode(ie_debito_credito, ie_deb_cred_w, vl_movimento, vl_movimento * -1))
			into	vl_movto_saldo_w
			from	lote_contabil b,
				ctb_movimento_v a
			where 	a.dt_movimento	between trunc(dt_inicial_w,'month')  
				and trunc(dt_inicial_w,'dd') - 1 / 86400
			and	a.cd_conta_contabil	= cd_conta_contabil_w
			and	a.nr_lote_contabil		= b.nr_lote_contabil
			and	b.dt_atualizacao_saldo is not null;
		end if;

		vl_saldo_w				:= nvl(vl_saldo_w,0) + nvl(vl_movto_saldo_w,0);
		ie_tem_movimento_w			:= 'N';
		
		open  c002;
		loop
			fetch c002 into	
				ie_movimento_w,
				dt_movimento_w,
				ds_historico_w,
				ie_debito_credito_w,
				ds_compl_historico_w,
				cd_contrapartida_w,
				cd_historico_w,
				vl_credito_w,
				vl_debito_w,
				vl_movimento_w,
				nr_lote_contabil_w,
				nr_seq_movto_w;
			exit when c002%notfound;
			begin

			if	(ie_debito_credito_w <> ie_deb_cred_w) then			
				vl_movimento_w	:= vl_movimento_w * -1;
			end if;

			select w_ctb_razao_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			vl_saldo_w		:= nvl(vl_saldo_w,0) + nvl(vl_movimento_w,0);
			
			if (vl_saldo_w <> 0) then
				vl_saldo_ant_w	:= nvl(vl_saldo_w,0);
			end if;
			
			if ((vl_saldo_w = 0) and (nvl(vl_saldo_ant_w,0) <> 0)) then
				vl_saldo_w := vl_saldo_ant_w;
			end if;			

			if	(ie_movimento_w = 'S') then
				dt_movimento_w	:= null;
			end if;
			
			select	max(cd_classificacao)
			into	cd_classif_centro_w
			from	centro_custo
			where	cd_centro_custo	= cd_centro_custo_w;
			
				insert into w_ctb_razao(
					cd_estabelecimento,
					dt_movimento,
					cd_centro_custo,
					cd_conta_contabil,
					cd_contrapartida,
					cd_classif_conta,
					cd_classif_centro,
					cd_historico,
					ds_historico,
					ie_debito_credito,
					ds_compl_historico,
					ds_conta,
					vl_credito,
					vl_debito,
					vl_movimento,
					vl_saldo,
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_lote_contabil,
					nr_seq_movto,
					dt_referencia)
				values(cd_estabelecimento_w,
					dt_movimento_w,
					cd_centro_custo_w,
					cd_conta_contabil_w,
					cd_contrapartida_w,
					'',
					cd_classif_centro_w,
					cd_historico_w,
					ds_historico_w,
					ie_debito_credito_w,
					ds_compl_historico_w,
					ds_conta_w,
					vl_credito_w,
					vl_debito_w,
					vl_movimento_w,
					vl_saldo_w,
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					nr_lote_contabil_w,
					nr_seq_movto_w,
					dt_referencia_w); 
			
			exception when others then
				dbms_output.put_line('Erro'); 
			ie_tem_movimento_w	:= 'S';
			end;
		end loop;
		close c002;
		end;
	end if;
end loop;
close c001;

commit;
end HAOC_ctb_gerar_razao_gerencial;
/