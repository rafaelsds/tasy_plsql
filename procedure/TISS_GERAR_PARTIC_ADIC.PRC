create or replace
procedure TISS_GERAR_PARTIC_ADIC(	nr_seq_procedimento_p	in	number,
				nm_usuario_p		in	varchar2) is

cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(15,0);
ie_responsavel_credito_w		varchar2(255);
cd_convenio_w			number(15,0);
cd_estabelecimento_w		number(15,0);
cd_grupo_proc_w			number(15,0);
cd_especialidade_w		number(15,0);
cd_area_procedimento_w		number(15,0);
nr_seq_regra_w			number(15,0);
cd_setor_atendimento_w		number(15,0);
nr_interno_conta_w			number(15,0);
nr_atendimento_w			number(15,0);
nm_participante_w			varchar2(255);
cd_interno_w			varchar2(255);
ie_funcao_w			varchar2(255);
ie_tiss_tipO_guia_w			varchar2(255);
ds_proc_tiss_w			varchar2(255);
vl_custo_operacional_w		varchar2(255);
ds_procedimento_w			varchar2(255);
cd_classif_setor_w			varchar2(255);
sg_conselho_W			varchar2(255);
nr_crm_W			varchar2(255);
uf_crm_W			varchar2(255);
nr_prescricao_w		varchar2(255);
nr_cpf_W				varchar2(255);
cd_cbos_w			varchar2(255);
ie_guia_w			varchar2(255);
ie_tipo_protocolo_w			number(15,0);
ie_tipo_guia_atend_w		varchar2(255);
cd_regional_w			varchar2(255);
ie_tipo_atendimento_w		number(15,0);
ie_medico_atend_w		varchar2(10);
nr_seq_interno_w		number(10);

cursor c01 is
select	a.nr_sequencia
from	tiss_regra_partic_adic a
where	a.cd_convenio						= cd_convenio_w 
and	a.cd_estabelecimento					= cd_estabelecimento_w
and	((a.ie_origem_proced					= ie_origem_proced_w) or 
	(a.ie_origem_proced is null))
and	nvl(a.cd_procedimento, nvl(cd_procedimento_w,0))		= nvl(cd_procedimento_w,0)
and	nvl(a.cd_area_procedimento, nvl(cd_area_procedimento_w,0))	= nvl(cd_area_procedimento_w,0)
and	nvl(a.cd_especialidade, nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
and	nvl(a.cd_grupo_proc, nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
and	nvl(a.ie_responsavel_credito, ie_responsavel_credito_w)		= ie_responsavel_credito_w
and	nvl(a.ie_tiss_tipo_guia, ie_tiss_tipo_guia_w)			= ie_tiss_tipo_guia_w
and	vl_custo_operacional_w	<> 0
order 	by nvl(a.ie_tiss_tipo_guia, 0),
	nvl(a.ie_responsavel_credito, 0),
	nvl(a.cd_procedimento, 0),
	nvl(a.cd_grupo_proc, 0),
	nvl(a.cd_especialidade, 0),
	nvl(a.cd_area_procedimento, 0);

cursor c02 is
select	nm_participante,
	cd_interno,
	ie_funcao,		-- Edgar 31/03/2009, n�o aplicar convers�o neste select
	sg_conselho,
	nr_crm,
	uf_crm,
	nr_cpf,
	cd_cbos,
	nvl(ie_medico_atend,'N')
from	tiss_partic_adic
where	nr_seq_regra	= nr_seq_regra_w;

BEGIN

delete	from tiss_conta_partic_adic
where	nr_seq_procedimento	= nr_seq_procedimento_p;

begin
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.ie_responsavel_credito,
	b.cd_convenio_parametro,
	b.cd_estabelecimento,
	a.vl_custo_operacional,
	nvl(a.ie_responsavel_credito, '-1'),
	a.ie_tiss_tipO_guia,
	a.cd_setor_atendimento,
	a.nr_interno_conta,
	a.nr_atendimento,
	c.ie_tipo_atendimento,
	a.nr_prescricao
into	cd_procedimento_w,
	ie_origem_proced_w,
	ie_responsavel_credito_w,
	cd_convenio_w,
	cd_estabelecimento_w,
	vl_custo_operacional_w,
	ie_responsavel_credito_w,
	ie_tiss_tipO_guia_w,
	cd_setor_atendimento_w,
	nr_interno_conta_w,
	nr_atendimento_w,
	ie_tipo_atendimento_w,
	nr_prescricao_w
from	atendimento_paciente c,
	conta_paciente b,
	procedimento_paciente a
where	a.nr_sequencia		= nr_seq_procedimento_p
and	a.nr_interno_conta		= b.nr_interno_conta
and	b.nr_atendimento		= c.nr_atendimento;
exception
when no_data_found then
	nr_interno_conta_w		:= null;
end;

if	(nvl(nr_interno_conta_w,0) > 0) then

	select	max(cd_grupo_proc),
		max(cd_especialidade),
		max(cd_area_procedimento),
		max(ds_procedimento)
	into	cd_grupo_proc_w,
		cd_especialidade_w,
		cd_area_procedimento_w,
		ds_procedimento_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced		= ie_origem_proced_w;

	select	max(cd_classif_setor)
	into	cd_classif_setor_w
	from	setor_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_w;

	select	max(ie_tipo_protocolo)
	into	ie_tipo_protocolo_w
	from	protocolo_convenio a,
		conta_paciente b
	where	a.nr_seq_protocolo	= b.nr_seq_protocolo
	and	b.nr_interno_conta	= nr_interno_conta_w;
	
	begin
	nr_seq_interno_w		:= obter_atecaco_atendimento(nr_atendimento_w);
	exception
	when others then
		nr_seq_interno_w	:= null;
	end;
	
	begin
	select	ie_tipo_guia
	into	ie_tipo_guia_atend_w
	from	atend_categoria_convenio
	where	nr_atendimento		= nr_atendimento_w
	and	nr_seq_interno		= nr_seq_interno_w;
	exception
	when others then
		ie_tipo_guia_atend_w	:= null;
	end;

	cd_regional_w	:= Obter_Valor_Conv_Estab(cd_convenio_w, cd_estabelecimento_w, 'CD_REGIONAL');

	nr_seq_regra_w		:= null;

	open c01;
	loop
	fetch c01 into
		nr_seq_regra_w;
	exit when c01%notfound;
	end loop;
	close c01;

	if	(ie_tiss_tipO_guia_w = 4) then
		ie_guia_w	:= 'SADT';
	elsif	(ie_tiss_tipO_guia_w = 5) then
		ie_guia_w	:= 'RI';
	end if;

	if	(nvl(nr_seq_regra_w, 0) > 0) then
		open c02;
		loop
		fetch c02 into
			nm_participante_w,
			cd_interno_w,
			ie_funcao_w,
			sg_conselho_W,
			nr_crm_W,
			uf_crm_W,
			nr_cpf_W,
			cd_cbos_w,
			ie_medico_atend_w;
		exit when c02%notfound;

			ds_proc_tiss_w	:= tiss_obter_campo_esp(cd_convenio_w,
								cd_estabelecimento_w,
								'DS_PROCEDIMENTO',
								ds_procedimento_w,
								null,
								null,
								cd_classif_setor_w,
								ie_funcao_w,
								null,
								null,
								ie_tipo_protocolo_w,
								ie_tipo_guia_atend_w,
								cd_regional_w,
								ie_guia_w,
								cd_procedimento_w,
								ie_origem_proced_w,
								cd_setor_atendimento_w,
								ie_tipo_atendimento_w,
								null,
								ie_responsavel_credito_w,
								null,
								nr_interno_conta_w, nr_prescricao_w, null, null, null);

			if	(nvl(ie_medico_atend_w,'N') = 'S') then
		
				select	nvl(nm_participante_w,substr(obter_nome_pf_pj(cd_medico_resp,null),1,255)),					
					nvl(sg_conselho_W,OBTER_DADOS_MEDICO(cd_medico_resp,'SGCRM')),
					nvl(nr_crm_W,OBTER_DADOS_MEDICO(cd_medico_resp,'CRM')),
					nvl(uf_crm_W,OBTER_DADOS_MEDICO(cd_medico_resp,'UFCRM')),
					nvl(nr_cpf_W,Obter_Cpf_Pessoa_Fisica(cd_medico_Resp))
				into	nm_participante_w,
					sg_conselho_W,
					nr_crm_W,
					uf_crm_W,
					nr_cpf_W
				from	atendimento_paciente
				where	nr_atendimento	= nr_atendimento_w;
	
			end if;

			insert	into tiss_conta_partic_adic
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_procedimento,
				ds_proc_tiss,
				vl_participante,
				sg_conselho,
				nr_crm,
				uf_crm,
				nr_cpf,
				nm_participante,
				cd_cbos,
				cd_interno,
				ie_funcao)
			select	tiss_conta_partic_adic_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_procedimento_p,
				ds_proc_tiss_w,
				vl_custo_operacional_w,
				sg_conselho_w,
				nr_crm_w,
				uf_crm_w,
				nr_cpf_w,
				nm_participante_w,
				cd_cbos_w,
				cd_interno_w,
				ie_funcao_w
			from	dual;
		end loop;
		close c02;
	end if;
end if;

end TISS_GERAR_PARTIC_ADIC;
/