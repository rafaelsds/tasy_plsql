create or replace
procedure sus_altera_status_laudo(	nr_seq_status_laudo_p	number,
					nr_seq_status_old_p	number,
					nr_seq_laudo_paciente_p	number,
					nr_seq_motivo_alter_p	number,
					nm_usuario_p		varchar2) is

ds_perfil_w			perfil.ds_perfil%type;

Cursor c_regra_comunic_stat_laudo(	nr_seq_status_laudo_pc	sus_status_laudo.nr_sequencia%type) is
	select	nr_seq_status_laudo,
		nm_usuarios_destino,
		cd_perfil_destino,
		ds_texto_comunicacao
	from	sus_regra_comun_stat_laudo
	where	nr_seq_status_laudo = nr_seq_status_laudo_pc;
					
begin

if	(nr_seq_laudo_paciente_p is not null) then

	update	sus_laudo_paciente
	set	nr_seq_status_laudo	= nr_seq_status_laudo_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_seq_interno		= nr_seq_laudo_paciente_p;
	
	insert	into	sus_log_status_laudo(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_status_atual,
						nr_seq_status_anter,
						nr_seq_motivo_alter,
						nr_seq_laudo)
	values				(	sus_log_status_laudo_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_status_laudo_p,
						nr_seq_status_old_p,
						nr_seq_motivo_alter_p,
						nr_seq_laudo_paciente_p);
						
	for r_c_regra_comunic_stat_laudo in c_regra_comunic_stat_laudo(nr_seq_status_laudo_p) loop
		
		begin
		select	ds_perfil
		into	ds_perfil_w
		from	perfil
		where	cd_perfil = r_c_regra_comunic_stat_laudo.cd_perfil_destino;
		exception
		when others then
			ds_perfil_w	:= null;
		end;
		
		Gerar_Comunic_Padrao(	sysdate,
					wheb_mensagem_pck.get_texto(779663),
					r_c_regra_comunic_stat_laudo.ds_texto_comunicacao,
					nm_usuario_p,
					null,
					r_c_regra_comunic_stat_laudo.nm_usuarios_destino,
					'N',
					null,
					ds_perfil_w,
					null,
					null,
					null,
					null,
					null);
	end loop;
	
	commit;
	
end if;

end sus_altera_status_laudo;
/