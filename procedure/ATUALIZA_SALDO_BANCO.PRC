Create or Replace
Procedure ATUALIZA_SALDO_BANCO 	(nr_seq_saldo_banco_p	in	number,
				nr_seq_lote_p		in	number,
				nm_usuario_p		in	varchar2) is 

vl_atualizacao_w		number(15,2);
vl_transacao_w			number(15,2);
ie_banco_w			varchar2(1);
/* Projeto Multimoeda - Variáveis */
vl_transacao_estrang_w		number(15,2);
vl_atualizacao_estrang_w	number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;

cursor c01 is
select	a.vl_transacao,
	a.vl_transacao_estrang,
	b.ie_banco
from 	transacao_financeira b,
	movto_trans_financ a
where	b.nr_sequencia = a.nr_seq_trans_financ
and	a.nr_seq_saldo_banco = nr_seq_saldo_banco_p
and 	a.nr_seq_lote = nr_seq_lote_p;

begin
vl_atualizacao_w := 0;
vl_atualizacao_estrang_w := 0;
open c01;
loop
fetch c01 into
	vl_transacao_w,
	vl_transacao_estrang_w,
	ie_banco_w;
exit when c01%notfound;

if (ie_banco_w = 'C') then
	vl_atualizacao_w := vl_atualizacao_w + vl_transacao_w;
	/* Projeto Multimoeda - Calcula os valores em moeda estrangeira */
	if (nvl(vl_transacao_estrang_w,0) <> 0) then
		vl_atualizacao_estrang_w := vl_atualizacao_estrang_w + vl_transacao_estrang_w;
	end if;
elsif (ie_banco_w = 'D') then
	vl_atualizacao_w := vl_atualizacao_w - vl_transacao_w;
	/* Projeto Multimoeda - Calcula os valores em moeda estrangeira */
	if (nvl(vl_transacao_estrang_w,0) <> 0) then
		vl_atualizacao_estrang_w := vl_atualizacao_estrang_w - vl_transacao_estrang_w;
	end if;
elsif (ie_banco_w = 'T') then
	vl_atualizacao_w := vl_atualizacao_w - 0;
	/* Projeto Multimoeda - Calcula os valores em moeda estrangeira */
	if (nvl(vl_transacao_estrang_w,0) <> 0) then
		vl_atualizacao_estrang_w := vl_atualizacao_estrang_w - 0;
	end if;
end if;

end loop;
close c01;

update	banco_saldo
set	vl_saldo = vl_saldo + vl_atualizacao_w,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_saldo_banco_p;

/* Projeto Multimoeda - Atualiza o saldo em moeda estrangeira */
if (nvl(vl_atualizacao_estrang_w,0) <> 0) then
	update	banco_saldo
	set	vl_saldo_estrang = vl_saldo_estrang + vl_atualizacao_estrang_w
	where	nr_sequencia = nr_seq_saldo_banco_p;
end if;

update	movto_trans_financ
set	dt_fechamento_lote = sysdate,
	nm_usuario = nm_usuario_p
where	nr_seq_lote = nr_seq_lote_p
and	nr_seq_saldo_banco = nr_seq_saldo_banco_p;

commit;

end ATUALIZA_SALDO_BANCO;
/
