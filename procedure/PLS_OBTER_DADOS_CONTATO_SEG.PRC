create or replace
procedure pls_obter_dados_contato_seg(	nr_seq_segurado_p 		pls_segurado.nr_sequencia%type,
					cd_cep_p		out	compl_pessoa_fisica.cd_cep%type,
					ds_endereco_p		out	compl_pessoa_fisica.ds_endereco%type,
					nr_endereco_p		out	compl_pessoa_fisica.nr_endereco%type,
					ds_complemento_p	out	compl_pessoa_fisica.ds_complemento%type,
					ds_bairro_p		out	compl_pessoa_fisica.ds_bairro%type,
					cd_municipio_ibge_p	out	compl_pessoa_fisica.cd_municipio_ibge%type,
					ds_municipio_ibge_p	out	compl_pessoa_fisica.ds_municipio%type,
					sg_estado_p		out	compl_pessoa_fisica.sg_estado%type,				
					ds_email_p		out	compl_pessoa_fisica.ds_email%type) is 		

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter os dados de contato do segurado
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
qt_endereco_residencial_w	number(10);	
nr_seq_compl_pf_w		compl_pessoa_fisica.nr_sequencia%type;

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	count(1)
into	qt_endereco_residencial_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_complemento	= 1;

if (qt_endereco_residencial_w > 0) then
	select	cd_cep,
		ds_endereco,
		nvl(nr_endereco, ds_compl_end) nr_endereco,
		ds_complemento,
		ds_bairro,
		cd_municipio_ibge,
		substr(obter_desc_municipio_ibge(cd_municipio_ibge),1,255) ds_municipio_ibge,
		sg_estado,
		ds_email
	into	cd_cep_p,
		ds_endereco_p,
		nr_endereco_p,
		ds_complemento_p,
		ds_bairro_p,
		cd_municipio_ibge_p,
		ds_municipio_ibge_p,
		sg_estado_p,
		ds_email_p
	from	compl_pessoa_fisica 
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	ie_tipo_complemento	= 1
	and	rownum = 1;
end if;

end pls_obter_dados_contato_seg;
/
