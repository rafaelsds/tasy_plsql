create or replace
procedure gerar_cheque_cr_hist(	nr_seq_cheque_cr_p	number,
				ds_historico_p		varchar2,
				ie_commit_p		varchar2,
				nm_usuario_p		Varchar2) is 

begin

if	(nvl(nr_seq_cheque_cr_p,0) <> 0) then

	insert into cheque_cr_hist(
		nr_sequencia,
		nr_seq_cheque,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_historico,
		ie_tipo)
	values(	cheque_cr_hist_seq.nextval,
		nr_seq_cheque_cr_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		substr(ds_historico_p,1,4000),
		'S');
	
end if;


if	(ie_commit_p = 'S') then
	commit;
end if;

end gerar_cheque_cr_hist;
/