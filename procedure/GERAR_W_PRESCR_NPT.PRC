create or replace
procedure gerar_w_prescr_npt	(	cd_estabelecimento_p	number,
					nr_prescricao_p		number,
					ie_npt_adulta_p		varchar2,
					nm_usuario_p		varchar2) is

nr_sequencia_w		number(5,0)	:= 0;
nr_linha_w		number(5,0);

nr_seq_nut_pac_w	number(10);
ds_material_w		varchar2(80);
qt_volume_w		number(15,4);
cd_unidade_medida_w	varchar2(30);

qt_volume_protocolo_w	number(15,4); /*David OS 99442*/
ie_modificado_w		varchar2(1);

cursor c01 is
	select	rownum nr_linha,
		substr(obter_desc_material(c.cd_material),1,80) ds_material,
		c.qt_volume,
		c.qt_protocolo
	from	nut_pac_elem_mat c
	where	c.nr_seq_nut_pac 	= nr_seq_nut_pac_w;
	/*and	c.qt_volume	> 0;  David OS 99442*/


cursor c02 is
	select	rownum nr_linha,
		a.nr_sequencia,
		substr(Obter_desc_mat_npt(c.nr_seq_elem_mat),1,80) ds_material,
		c.qt_vol_cor,
		c.qt_protocolo
	from	nut_pac_elem_mat c,
		nut_elemento x,
		nut_pac_elemento b,
		nut_pac a
	where	a.nr_prescricao		= nr_prescricao_p
	and	a.nr_sequencia		= b.nr_seq_nut_pac
	and	b.nr_seq_elemento	= x.nr_sequencia
	and	b.nr_sequencia		= c.nr_seq_pac_elem;
	/*and	c.qt_volume	> 0;  David OS 99442*/

cursor c03 is
	select	rownum nr_linha,
		substr(obter_descricao_padrao('NUT_ELEMENTO','DS_ELEMENTO',NR_SEQ_ELEMENTO),1,40) ds_elemento,
		qt_diaria,
		cd_unidade_medida,
		qt_protocolo
	from	nut_pac_elemento
	where	nr_seq_nut_pac = nr_seq_nut_pac_w;	

cursor c04 is	
	select	rownum nr_linha,
		substr(obter_descricao_padrao('NUT_ELEMENTO','DS_ELEMENTO',NR_SEQ_ELEMENTO),1,40) ds_elemento,
		qt_diaria,		/*  alterei para qt_diaria OS89119 */
		cd_unidade_medida
	from	nut_pac_elemento
	where	nr_seq_nut_pac = nr_seq_nut_pac_w;


begin

	EXEC_SQL_DINAMICO_BV('TASY','delete from w_prescr_npt where nm_usuario = :nm_usuario', 'nm_usuario=' || nm_usuario_p);

if	(nr_prescricao_p is not null) then
	
	
	if	(ie_npt_adulta_p = 'S') then
		
		select 	max(nr_sequencia)
		into	nr_seq_nut_pac_w
		from	nut_pac
		where	nr_prescricao = nr_prescricao_p
		and	ie_npt_adulta =	'S';
		
		open c01;
		loop
		fetch c01 into	
			nr_linha_w,
			ds_material_w,
			qt_volume_w,
			qt_volume_protocolo_w;
		exit when c01%notfound;
			begin
			if	(qt_volume_protocolo_w is null) then
				ie_modificado_w := 'I';
			elsif 	(qt_volume_w <> qt_volume_protocolo_w) then
				ie_modificado_w := 'M';
			else
				ie_modificado_w := 'N';
			end if;
				
			if	((nr_linha_w mod 2) <> 0) then
		
				nr_sequencia_w:= nr_sequencia_w + 1;					

				insert into w_prescr_npt (	
					nr_sequencia,
					nr_prescricao,
					nr_seq_npt,
					ds_material_col1,
					qt_volume_col1,
					ie_modificado_col1,
					ds_material_col2,
					qt_volume_col2,
					ie_modificado_col2,
					ie_tipo_item,
					nm_usuario)
				values(	nr_sequencia_w,
					nr_prescricao_p,
					nr_seq_nut_pac_w,
					ds_material_w,
					qt_volume_w,
					ie_modificado_w,
					null,
					null,
					null,
					'P',
					nm_usuario_p);
			else
				update w_prescr_npt set  	
					ds_material_col2	= ds_material_w,
					qt_volume_col2		= qt_volume_w,
					ie_modificado_col2	= ie_modificado_w
				where	nr_sequencia 		= nr_sequencia_w;
			end if;
			end;
		end loop;
		close C01;
		
		open c03;
		loop
		fetch c03 into	
			nr_linha_w,
			ds_material_w,
			qt_volume_w,
			cd_unidade_medida_w,
			qt_volume_protocolo_w;
		exit when c03%notfound;
			begin
			
			if	(qt_volume_protocolo_w is null) then
				ie_modificado_w := 'I';
			elsif 	(qt_volume_w <> qt_volume_protocolo_w) then
				ie_modificado_w := 'M';
			else
				ie_modificado_w := 'N';
			end if;
			
			if	((nr_linha_w mod 2) <> 0) then
		
				nr_sequencia_w:= nr_sequencia_w + 1;					

				insert into w_prescr_npt (	
					nr_sequencia,
					nr_prescricao,
					nr_seq_npt,
					ds_material_col1,
					qt_volume_col1,
					ds_unid_med_col1,
					ie_modificado_col1,
					ds_material_col2,
					qt_volume_col2,		
					ds_unid_med_col2,
					ie_modificado_col2,
					ie_tipo_item,
					nm_usuario)
				values(	nr_sequencia_w,
					nr_prescricao_p,
					nr_seq_nut_pac_w,
					ds_material_w,
					qt_volume_w,
					cd_unidade_medida_w,
					ie_modificado_w,
					null,
					null,
					null,
					null,
					'E',
					nm_usuario_p);
			else
				update w_prescr_npt set  	
					ds_material_col2	= ds_material_w,
					qt_volume_col2		= qt_volume_w,
					ds_unid_med_col2	= cd_unidade_medida_w,
					ie_modificado_col2	= ie_modificado_w
				where	nr_sequencia 		= nr_sequencia_w;
			end if;
			end;
		end loop;
		close C03;
	else		
		open c02;
		loop
		fetch c02 into	
			nr_linha_w,
			nr_seq_nut_pac_w,
			ds_material_w,
			qt_volume_w,
			qt_volume_protocolo_w;
		exit when c02%notfound;
			begin
			if	(qt_volume_protocolo_w is null) then
				ie_modificado_w := 'I';
			elsif 	(qt_volume_w <> qt_volume_protocolo_w) then
				ie_modificado_w := 'M';
			else
				ie_modificado_w := 'N';
			end if;
			
			if	((nr_linha_w mod 2) <> 0) then
		
				nr_sequencia_w:= nr_sequencia_w + 1;					

				insert into w_prescr_npt (	
					nr_sequencia,
					nr_prescricao,
					nr_seq_npt,
					ds_material_col1,
					qt_volume_col1,
					ie_modificado_col1,
					ds_material_col2,
					qt_volume_col2,
					ie_modificado_col2,
					ie_tipo_item,
					nm_usuario)
				values(	nr_sequencia_w,
					nr_prescricao_p,
					nr_seq_nut_pac_w,
					ds_material_w,
					qt_volume_w,
					ie_modificado_w,
					null,
					null,
					null,
					'P',
					nm_usuario_p);
			else
				update w_prescr_npt set  	
					ds_material_col2	= ds_material_w,
					qt_volume_col2		= qt_volume_w,
					ie_modificado_col2	= ie_modificado_w
				where	nr_sequencia 		= nr_sequencia_w;
			end if;
			end;
		end loop;
		close C02;
		
		select 	max(nr_sequencia)
		into	nr_seq_nut_pac_w
		from	nut_pac
		where	nr_prescricao = nr_prescricao_p
		and	ie_npt_adulta =	'N';
		
		open c04;
		loop
		fetch c04 into	
			nr_linha_w,
			ds_material_w,
			qt_volume_w,
			cd_unidade_medida_w;
		exit when c04%notfound;
			begin
		
			if	((nr_linha_w mod 2) <> 0) then
		
				nr_sequencia_w:= nr_sequencia_w + 1;					

				insert into w_prescr_npt (	
					nr_sequencia,
					nr_prescricao,
					nr_seq_npt,
					ds_material_col1,
					qt_volume_col1,
					ds_unid_med_col1,
					ds_material_col2,
					qt_volume_col2,		
					ds_unid_med_col2,
					ie_tipo_item,
					nm_usuario)
				values(	nr_sequencia_w,
					nr_prescricao_p,
					nr_seq_nut_pac_w,
					ds_material_w,
					qt_volume_w,
					cd_unidade_medida_w,
					null,
					null,
					null,
					'E',
					nm_usuario_p);
			else
				update w_prescr_npt set  	
					ds_material_col2	= ds_material_w,
					qt_volume_col2		= qt_volume_w,
					ds_unid_med_col2	= cd_unidade_medida_w
				where	nr_sequencia 		= nr_sequencia_w;
			end if;
			end;
		end loop;
		close C04;
		
	end if;
end if;

commit;

end gerar_w_prescr_npt;
/