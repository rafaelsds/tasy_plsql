create or replace
procedure valida_reserva_funcionario(cd_barras_p	Varchar2,
				     dt_reserva_ini_p	date,
				     dt_reserva_fim_p	date,
				     nr_servico_p	number,
				     ds_erro_p		out varchar2) is 
				     
cd_pessoa_fisica_w	number(10);
nr_seq_reserva_w	number(10);
ie_reserva_w		varchar2(1);

begin
ds_erro_p := '';
select	nvl(max(cd_pessoa_fisica),0) 
into	cd_pessoa_fisica_w
from	usuario
where	cd_barras = cd_barras_p;

if (cd_pessoa_fisica_w <> 0) then
	select 	max(p.nr_sequencia)
	into	nr_seq_reserva_w
	from	nut_reserva_pf p,nut_reserva n
	where	p.nr_seq_nut_reserva = n.nr_sequencia
	and	n.nr_seq_servico = nr_servico_p
	and	trunc(n.dt_servico) between trunc(dt_reserva_ini_p) and trunc(dt_reserva_fim_p)
	and	p.cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	if (nr_seq_reserva_w > 0) then
		select 	decode(count(*),0,'S','N') 
		into	ie_reserva_w
		from 	nut_reserva_pf
		where	nr_sequencia = nr_seq_reserva_w
		and	dt_atend_reserva is null;
		
		if (ie_reserva_w = 'N') then
			update 	nut_reserva_pf set
				dt_atend_reserva = sysdate
			where	nr_sequencia = nr_seq_reserva_w;
		else
			ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279184);
		end if;
	else
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279185);
	end if;
else
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279186);
end if;
	
commit;

end valida_reserva_funcionario;
/