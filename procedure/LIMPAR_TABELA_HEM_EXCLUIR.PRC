CREATE OR REPLACE
PROCEDURE Limpar_Tabela_Hem_Excluir	(ie_tabela_p		Varchar2,
						nr_seq_registro_p	Number) is

BEGIN

if	(ie_tabela_p = 'CONCLUSAO') then
	BEGIN
	update	hem_cat c
	set	c.nr_seq_coronaria			= null,
		c.nr_seq_coronaria_adic		= null,
		c.nr_seq_ponte			= null,
		c.nr_seq_ponte_adic			= null,
		c.nr_seq_ve				= null,
		c.nr_seq_ve_adic			= null,
		c.nr_seq_valvula_aortica		= null,
		c.nr_seq_valvula_aortica_adic	= null,
		c.nr_seq_mitral			= null,
		c.nr_seq_mitral_adic			= null
	where	c.nr_sequencia			= nr_seq_registro_p;
	END;

elsif	(ie_tabela_p = 'USIC_PLACA') then
	BEGIN
	update	hem_usic p
	set	p.ds_observacao_placa		= null,
		p.ie_placa_env_orig_ramo		= null,
		p.ie_placa_trombo			= null,
		p.ie_placa_disseccao			= null,
		p.ie_placa_hematoma			= null,
		p.ie_placa_excentrica		= null,
		p.ie_placa_calcificada		= null,
		p.ie_placa_lipidica			= null,
		p.ie_placa_fibrotica			= null,
		p.ie_placa_mista			= null,
		p.ie_placa_vulneravel		= null
	where	p.nr_sequencia			= nr_seq_registro_p;
	END;

elsif	(ie_tabela_p = 'USIC_STENT') then
	BEGIN
	update	hem_usic s
	set	s.nr_seq_reestenose_stent		= null,
		s.ds_observacao_stent		= null,
		s.ie_stent_pervio			= null,
		s.ie_stent_hipoexpandido		= null,
		s.ie_stent_aposicao_haste		= null,
		s.ie_stent_disseccao_bordo		= null		
	where	s.nr_sequencia			= nr_seq_registro_p;
	END;
end if;

END	Limpar_Tabela_Hem_Excluir;
/