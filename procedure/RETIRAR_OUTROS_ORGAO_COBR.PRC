create or replace
procedure retirar_outros_orgao_cobr(
			nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);
ds_historico_w		varchar2(255);
vl_acobrar_w		cobranca.vl_acobrar%type;
qt_w			number(10);
			
Cursor c01 is
	select	nr_sequencia,
		nr_seq_orgao_cobr,
		nr_seq_cobranca
	from	outros_orgao_cobr
	where	ie_selecionado = 'S'
	and 	dt_liberacao_exclusao is null
	and 	nr_seq_lote is null;

vet01	c01%RowType;
	
begin

select	count(*)
into	qt_w
from	outros_orgao_cobr
where	ie_selecionado = 'S';

if	(qt_w = 0)then
	wheb_mensagem_pck.exibir_mensagem_abort(682121);
end if;

select	count(*)
into	qt_w
from	outros_orgao_cobr
where	ie_selecionado = 'S'
and 	(dt_liberacao_exclusao is not null
or 	nr_seq_lote is not null);		

if	(qt_w > 0)then
	wheb_mensagem_pck.exibir_mensagem_abort(338753);
end if;

open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin
	
	select	b.vl_acobrar
	into	vl_acobrar_w
	from	outros_orgao_cobr a,
		cobranca b
	where	a.nr_seq_cobranca = b.nr_sequencia
	and 	a.nr_sequencia = vet01.nr_sequencia;
	
	update	cobranca_orgao 
	set	dt_retirada = sysdate 
	where	nr_seq_cobranca = vet01.nr_seq_cobranca
	and 	nr_seq_orgao = vet01.nr_seq_orgao_cobr;
	
	ds_historico_w := wheb_mensagem_pck.get_texto(337852, 'NM_USUARIO=' || nm_usuario_p || ';VL_TOTAL=' || vl_acobrar_w || ';DT_ATUALIZACAO=' || to_char(sysdate,'dd/MM/yyyy hh24:mi:ss'));
	gerar_historico_cobr_orgao(null,null,null,ds_historico_w,nm_usuario_p,'P');

	end;
end loop;
close c01;	

delete 
from 	outros_orgao_cobr a
where 	a.ie_selecionado = 'S'
and 	dt_liberacao_exclusao is null
and 	nr_seq_lote is null;

commit;

end retirar_outros_orgao_cobr;
/
