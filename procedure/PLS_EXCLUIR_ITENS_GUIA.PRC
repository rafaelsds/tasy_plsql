create or replace
procedure pls_excluir_itens_guia	
			(	nr_seq_guia_p			number,
				ie_exc_itens_neg_audit_p	varchar2,
				nr_seq_motivo_exclusao_p	number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

ie_status_mat_w			varchar2(2);
ie_status_proc_w		varchar2(2);
nr_seq_guia_proc_w		number(10);
nr_seq_guia_mat_w		number(10);
nr_seq_motivo_lib_w		number(10);

cursor c01 is
	select	nr_sequencia,
		ie_status
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p
	and 	nr_seq_motivo_exc is null;

cursor c02 is
	select	nr_sequencia,
		ie_status
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_p
	and 	nr_seq_motivo_exc is null;

begin
if	(nr_seq_guia_p is not null) and
	(nvl(nr_seq_guia_p,0) <> 0) then
	open c01;
	loop
	fetch c01 into	
		nr_seq_guia_proc_w,
		ie_status_proc_w;
	exit when c01%notfound;
		begin
		if	((ie_status_proc_w = 'N') or
			((ie_status_proc_w = 'M') and (ie_exc_itens_neg_audit_p = 'S'))) then
			update  pls_guia_plano_proc
			set	nr_seq_motivo_exc = nr_seq_motivo_exclusao_p
			where	nr_sequencia	= nr_seq_guia_proc_w;
		end if;
		end;
	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into	
		nr_seq_guia_mat_w,
		ie_status_mat_w;
	exit when c02%notfound;
		begin
		if	((ie_status_mat_w = 'N') or
			((ie_status_mat_w = 'M') and (ie_exc_itens_neg_audit_p = 'S'))) then
			update  pls_guia_plano_mat
			set	nr_seq_motivo_exc = nr_seq_motivo_exclusao_p
			where	nr_sequencia	= nr_seq_guia_mat_w;
		end if;
		end;
	end loop;
	close c02;

	delete	from pls_guia_glosa
	where	nr_seq_guia	= nr_seq_guia_p;

	delete	from pls_guia_glosa
	where	nr_seq_guia is null
	and	nr_seq_guia_proc is null
	and	nr_seq_guia_mat is null;

	delete	from pls_guia_motivo_lib
	where	nr_seq_guia is null
	and	nr_seq_guia_proc is null
	and	nr_seq_guia_mat is null;

	pls_consistir_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);
end if;

commit;

end pls_excluir_itens_guia;
/
