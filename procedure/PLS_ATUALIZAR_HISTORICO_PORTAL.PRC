create or replace
procedure pls_atualizar_historico_portal
			(	nr_seq_atendimento_p	Number,
				nm_usuario_p		Varchar2) is 

dt_envio_mensagem_w	Varchar2(20);
ds_mensagem_w		Varchar2(255);
nm_usuario_envio_w	Varchar2(255);
ds_mensagem_final_w	long;

cursor c01 is
	select	to_char(dt_envio_mensagem, 'dd/mm/yyyy hh24:mi:ss'), 
		nm_usuario_envio, 
		ds_mensagem
	from 	pls_atendimento_chat
	where 	nr_seq_atendimento = nr_seq_atendimento_p
	order by nr_sequencia;
begin
open c01;
loop
fetch c01 into	dt_envio_mensagem_w,
		nm_usuario_envio_w,
		ds_mensagem_w;
exit when c01%notfound;

	if(ds_mensagem_final_w is null) then
		ds_mensagem_final_w := '(' || dt_envio_mensagem_w || ') - ' || nm_usuario_envio_w || chr(10) || ds_mensagem_w;
	else
		ds_mensagem_final_w := ds_mensagem_final_w || chr(10) || '(' || dt_envio_mensagem_w || ') - ' || nm_usuario_envio_w || chr(10) ||ds_mensagem_w;
	end if;
end loop;
close c01;

update	pls_atendimento
set	ds_historico_atend_chat = ds_mensagem_final_w
where	nr_sequencia = nr_seq_atendimento_p;

commit;

end pls_atualizar_historico_portal;
/