create or replace
procedure cheques_desvincular_cr(nr_seq_cheque_p	number,
			nm_usuario_p	varchar2) is 

begin

if	(nr_seq_cheque_p is not null) then
	begin
	update	cheque_cr
	set 	nr_titulo = null, 
		dt_atualizacao = sysdate, 
		nm_usuario = nm_usuario_p
	where 	nr_seq_cheque = nr_seq_cheque_p;
	
	commit;
	end;
end if;

end cheques_desvincular_cr;
/