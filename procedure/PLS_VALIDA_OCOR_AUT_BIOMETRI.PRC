create or replace
procedure pls_valida_ocor_aut_biometri
				(	nr_seq_ocor_combinada_p		pls_ocor_aut_combinada.nr_sequencia%type,
					nr_seq_ocorrencia_p		pls_ocorrencia.nr_sequencia%type,
					nr_seq_motivo_glosa_p		tiss_motivo_glosa.nr_sequencia%type,
					nr_seq_guia_p			pls_guia_plano.nr_sequencia%type,
					nr_seq_requisicao_p		pls_requisicao.nr_sequencia%type,
					nr_seq_execucao_p		pls_execucao_requisicao.nr_sequencia%type,
					ie_utiliza_filtro_p		pls_ocor_aut_combinada.ie_utiliza_filtro%type,
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type ) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Validacao para a informacao da digital do beneficiario.
 Esta validacao se refere a digital nao cadastrada, digital nao informada ou digital invalida.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  x]  Objetos do dicionario [ ] Tasy (Delphi/Java) [   ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ie_gerar_ocorrencia_w		varchar2(2);
ie_gerar_ocorrencia_digital_w	varchar2(2);
ie_tipo_validacao_digital_w	pls_requisicao.ie_tipo_validacao_digital%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;	
ie_tipo_ocorrencia_w		varchar2(2);
ie_regra_w			varchar2(2);	
nr_seq_oc_benef_w		pls_ocorrencia_benef.nr_sequencia%type;	
ie_gerou_ocor_cabecalho_w	Varchar2(2);

Cursor C01 is
	select	ie_digital_invalida,
		ie_digital_obrigatoria,
		ie_digital_cadastrada
	from	pls_validacao_aut_biometri
	where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p
	and	ie_situacao			= 'A';

Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C03 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	nr_seq_material		is null;

Cursor C05 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	cd_procedimento		is null;
	
Cursor C06 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p;

Cursor C07 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_p;
	
begin

for r_C01_w in C01 loop
	begin
		if	(nr_seq_guia_p is not null) then
			begin
				select	ie_tipo_validacao_biometria,
					nr_seq_prestador,
					nr_seq_segurado
				into	ie_tipo_validacao_digital_w,
					nr_seq_prestador_w,
					nr_seq_segurado_w
				from 	pls_guia_plano
				where 	nr_sequencia	= nr_seq_guia_p;
			exception
			when others then
				ie_tipo_validacao_digital_w	:= null;
			end;	
			
			ie_gerar_ocorrencia_digital_w	:= 'S';
			
			if	(nvl(r_C01_w.ie_digital_invalida, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 1) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			if	(nvl(r_C01_w.ie_digital_obrigatoria, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 2) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			if	(nvl(r_C01_w.ie_digital_cadastrada, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 3) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;
						
			for r_C06_w in C06 loop
				begin
					ie_gerar_ocorrencia_w	:= 'S';	
					
					if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
										null, null, null,
										r_C06_w.nr_sequencia, null, null,
										r_C06_w.cd_procedimento, r_C06_w.ie_origem_proced, null,
										null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
										
						if	(ie_regra_w	= 'S') then
							ie_gerar_ocorrencia_w	:= 'S';
						elsif	(ie_regra_w	in ('E','N')) then
							ie_gerar_ocorrencia_w	:= 'N';
						end if;							
					end if;
					
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						ie_gerar_ocorrencia_w	:= ie_gerar_ocorrencia_digital_w;		
											
						if	(ie_gerar_ocorrencia_w	= 'S') then						
											
							pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
										nr_seq_guia_p, null, r_C06_w.nr_sequencia, 
										null, nr_seq_ocor_combinada_p, nm_usuario_p, 
										null, nr_seq_motivo_glosa_p, 1, 
										cd_estabelecimento_p, 'N' ,null,
										nr_seq_oc_benef_w, null,
										null, null, null);
									
							pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
											nr_seq_guia_p, null, null,
											r_C06_w.nr_sequencia, null, null,
											null, null, null,
											nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;
				end;
			end loop;
			for r_C07_w in C07 loop
				begin
					ie_gerar_ocorrencia_w	:= 'S';
					
					if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
										null, null, null,
										null,r_C07_w.nr_sequencia, null,
										null, null, r_C07_w.nr_seq_material,
										null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
										
						if	(ie_regra_w	= 'S') then
							ie_gerar_ocorrencia_w	:= 'S';
						elsif	(ie_regra_w	in ('E','N')) then
							ie_gerar_ocorrencia_w	:= 'N';
						end if;
					end if;
					
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						ie_gerar_ocorrencia_w	:= ie_gerar_ocorrencia_digital_w;
												
						if	(ie_gerar_ocorrencia_w	= 'S') then
							pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
										nr_seq_guia_p, null, null, 
										r_C07_w.nr_sequencia, nr_seq_ocor_combinada_p, nm_usuario_p, 
										null, nr_seq_motivo_glosa_p, 2, 
										cd_estabelecimento_p, 'N' ,null,
										nr_seq_oc_benef_w, null,
										null, null, null);
											
							pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
											nr_seq_guia_p, null, null,
											null, r_C07_w.nr_sequencia, null,
											null, null, null,
											nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;
				end;
			end loop;
		elsif	(nr_seq_requisicao_p is not null) then
			begin
				select	ie_tipo_validacao_digital,
					nr_seq_prestador,
					nr_seq_segurado
				into	ie_tipo_validacao_digital_w,
					nr_seq_prestador_w,
					nr_seq_segurado_w
				from 	pls_requisicao
				where 	nr_sequencia	= nr_seq_requisicao_p;
			exception
			when others then
				ie_tipo_validacao_digital_w	:= null;
			end;	
			
			ie_gerar_ocorrencia_digital_w	:= 'S';
			
			if	(nvl(r_C01_w.ie_digital_invalida, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 1) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			if	(nvl(r_C01_w.ie_digital_obrigatoria, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 2) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			if	(nvl(r_C01_w.ie_digital_cadastrada, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 3) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;
						
			for r_C02_w in C02 loop
				begin
					ie_gerar_ocorrencia_w	:= 'S';	
					
					if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
										null, null, null,
										r_C02_w.nr_sequencia, null, null,
										r_C02_w.cd_procedimento, r_C02_w.ie_origem_proced, null,
										null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
										
						if	(ie_regra_w	= 'S') then
							ie_gerar_ocorrencia_w	:= 'S';
						elsif	(ie_regra_w	in ('E','N')) then
							ie_gerar_ocorrencia_w	:= 'N';
						end if;							
					end if;
					
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						ie_gerar_ocorrencia_w	:= ie_gerar_ocorrencia_digital_w;		
													
						if	(ie_gerar_ocorrencia_w	= 'S') then							
							pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
										null, null, r_C02_w.nr_sequencia, 
										null, nr_seq_ocor_combinada_p, nm_usuario_p, 
										null, nr_seq_motivo_glosa_p, 5, 
										cd_estabelecimento_p, 'N' ,null,
										nr_seq_oc_benef_w, null,
										null, null, null);
									
							pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
											null, nr_seq_requisicao_p, null,
											null, null, r_C02_w.nr_sequencia,
											null, null, null,
											nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;
				end;
			end loop;
			for r_C03_w in C03 loop
				begin
					ie_gerar_ocorrencia_w	:= 'S';
					
					if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
										null, null, null,
										null,r_C03_w.nr_sequencia, null,
										null, null, r_C03_w.nr_seq_material,
										null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
										
						if	(ie_regra_w	= 'S') then
							ie_gerar_ocorrencia_w	:= 'S';
						elsif	(ie_regra_w	in ('E','N')) then
							ie_gerar_ocorrencia_w	:= 'N';
						end if;
					end if;
					
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						ie_gerar_ocorrencia_w	:= ie_gerar_ocorrencia_digital_w;
												
						if	(ie_gerar_ocorrencia_w	= 'S') then
							pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
										null, null, null, 
										r_C03_w.nr_sequencia, nr_seq_ocor_combinada_p, nm_usuario_p, 
										null, nr_seq_motivo_glosa_p, 6, 
										cd_estabelecimento_p, 'N' ,null,
										nr_seq_oc_benef_w, null,
										null, null, null);
										
							pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
											null, nr_seq_requisicao_p, null,
											null, r_C03_w.nr_sequencia, null,
											null, null, null,
											nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;
				end;
			end loop;
		elsif 	(nr_seq_execucao_p is not null) then	
			begin
				select	a.ie_tipo_validacao_digital,	
					a.nr_seq_prestador,
					b.nr_seq_segurado
				into	ie_tipo_validacao_digital_w,
					nr_seq_prestador_w,
					nr_seq_segurado_w
				from 	pls_execucao_requisicao a,
					pls_requisicao b
				where 	a.nr_seq_requisicao	= b.nr_sequencia
				and	a.nr_sequencia		= nr_seq_execucao_p;
			exception
			when others then
				ie_tipo_validacao_digital_w	:= null;
			end;			
			
			ie_gerar_ocorrencia_digital_w	:= 'S';
			
			if	(nvl(r_C01_w.ie_digital_invalida, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 1) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			if	(nvl(r_C01_w.ie_digital_obrigatoria, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 2) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			if	(nvl(r_C01_w.ie_digital_cadastrada, 'N') = 'S') then			
				if	(nvl(ie_tipo_validacao_digital_w, 0)	<> 3) then
					ie_gerar_ocorrencia_digital_w	:= 'N';
				end if;			
			end if;

			for r_C04_w in C04 loop
				begin
					ie_gerar_ocorrencia_w	:= 'S';

					if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
										nr_seq_execucao_p, null, null,
										null, null, r_C04_w.nr_sequencia,
										r_C04_w.cd_procedimento, r_C04_w.ie_origem_proced, null,
										ie_gerou_ocor_cabecalho_w,nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
										
						if	(ie_regra_w	= 'S') then
							ie_gerar_ocorrencia_w	:= 'S';
						elsif	(ie_regra_w	in ('E','N')) then
							ie_gerar_ocorrencia_w	:= 'N';
						end if;
					end if;
					
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						ie_gerar_ocorrencia_w	:= ie_gerar_ocorrencia_digital_w;
														
						if	(ie_gerar_ocorrencia_w	= 'S') then
							pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
										null, null, r_C04_w.nr_sequencia, 
										null, nr_seq_ocor_combinada_p, nm_usuario_p, 
										null, nr_seq_motivo_glosa_p, 10, 
										cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
										nr_seq_oc_benef_w, null,
										null, null, null);
								
							pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
											nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
											null, null, null,
											null, r_C04_w.nr_sequencia, null,
											nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;
				end;
			end loop;
			
			for r_C05_w in C05 loop
				begin
					ie_gerar_ocorrencia_w	:= 'S';

					if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
										nr_seq_execucao_p, null, null,
										null, null, r_C05_w.nr_sequencia,
										null, null, r_C05_w.nr_seq_material,
										ie_gerou_ocor_cabecalho_w,nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
										
						if	(ie_regra_w	= 'S') then
							ie_gerar_ocorrencia_w	:= 'S';
						elsif	(ie_regra_w	in ('E','N')) then
							ie_gerar_ocorrencia_w	:= 'N';
						end if;
					end if;
					
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						ie_gerar_ocorrencia_w	:= ie_gerar_ocorrencia_digital_w;
														
						if	(ie_gerar_ocorrencia_w	= 'S') then
							pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
										null, null, null, 
										r_C05_w.nr_sequencia, nr_seq_ocor_combinada_p, nm_usuario_p, 
										null, nr_seq_motivo_glosa_p, 11, 
										cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
										nr_seq_oc_benef_w, null,
										null, null, null);
								
							pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
											nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
											null, null, null,
											null, r_C05_w.nr_sequencia, null,
											nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;
				end;
			end loop;
		end if;
	end;
end loop;

end pls_valida_ocor_aut_biometri;
/