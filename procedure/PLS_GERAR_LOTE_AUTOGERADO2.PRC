create or replace
procedure pls_gerar_lote_autogerado2(nr_seq_lote_p		number, 
									ie_opcao_p			varchar2 default 'N',
									nm_usuario_p		varchar2) is 

nr_seq_prestador_w 		pls_lote_auto_gerado.nr_seq_prestador%type;
dt_inicio_w				pls_lote_auto_gerado.dt_inicio%type;
dt_fim_w				pls_lote_auto_gerado.dt_fim%type;					
begin

select 	nvl(max(nr_seq_prestador),0),
		max(to_date(dt_inicio,'dd/mm/yyyy hh24:mi:ss')),
		max(to_date(dt_fim,'dd/mm/yyyy hh24:mi:ss'))
into	nr_seq_prestador_w,
		dt_inicio_w,
		dt_fim_w
from 	pls_lote_auto_gerado
where	nr_sequencia = nr_seq_lote_p;

if (ie_opcao_p = 'S')then
	pls_gerencia_autogerado_pck.pls_gerar_lote_autogerado (nr_seq_lote_p, wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario);
else 	
	pls_gerencia_autogerado_pck.pls_desfazer_lote_autogerado (nr_seq_lote_p, wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario);			
end if;

commit;

end  pls_gerar_lote_autogerado2;
/