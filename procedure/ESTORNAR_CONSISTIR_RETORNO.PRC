create or replace
procedure estornar_consistir_retorno(nr_seq_retorno_p	number,
				nm_usuario_p	varchar2) is

nr_seq_lote_audit_w	number(10);

begin

select	max(nr_seq_lote_audit)
into	nr_seq_lote_audit_w
from	convenio_retorno_item c,
	conta_paciente_ret_hist b,
	lote_audit_tit_rec a
where	a.nr_sequencia		= b.nr_seq_lote_audit
and	b.nr_seq_ret_item	= c.nr_sequencia
and	c.nr_seq_retorno	= nr_seq_retorno_p
and	a.dt_baixa is not null;

if	(nr_seq_lote_audit_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265626,'chr(13)='||chr(13)|| 'seq_lote_audit= '||nr_seq_lote_audit_w);
	--N�o � poss�vel estornar a consist�ncia deste retorno.' || chr(13) ||
	--As guias deste retorno j� est�o no lote de t�tulos ' || nr_seq_lote_audit_w || ' da auditoria, que j� foi baixado!
end if;

update	convenio_retorno
set	dt_consistencia	= null,
	ie_status_retorno	= 'R',
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia	= nr_seq_retorno_p;

delete	from	convenio_retorno_venc
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end estornar_consistir_retorno;
/
