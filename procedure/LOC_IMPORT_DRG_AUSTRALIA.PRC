CREATE OR REPLACE PROCEDURE "LOC_IMPORT_DRG_AUSTRALIA"(
    cd_drg_version_p VARCHAR2 )
IS
  ds_locale_w establishment_locale.ds_locale%type;
  ds_procedure_w VARCHAR2(30) ;
BEGIN

  ds_locale_w := pkg_i18n.get_estab_locale();

  ds_procedure_w:='IMPORT_DRG_CATALOG_AUS';

  EXECUTE immediate get_procedure_call(ds_procedure_w, 1, ds_locale_w) USING cd_drg_version_p;
END loc_import_drg_australia;
/