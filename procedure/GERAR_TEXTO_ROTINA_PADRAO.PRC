create or replace
procedure Gerar_Texto_Rotina_Padrao(	NR_ATENDIMENTO_P	NUMBER,
					CD_PACIENTE_P		VARCHAR2,
					CD_PROFISSIONAL_P	VARCHAR2,
					CD_ESTABELECIMENTO_P	NUMBER,
					DS_LISTA_TEXTO_P	VARCHAR2,
					NM_USUARIO_P		VARCHAR2,
					ie_opcao_p		varchar2 default null,
					nr_seq_reg_elemento_p	number default null) IS 

lista_w	dbms_sql.varchar2_table;
nr_seq_texto_w	number(10);
ds_titulo_w	varchar2(255);
nr_seq_ci_w	number(10);
ds_texto_w		varchar2(32764);
ie_opcao_w	varchar2(10);--	:= nvl(ie_opcao_p,'CI');
ie_tipo_evolucao_w	varchar2(3);

begin
lista_w	:= obter_lista_string(ds_lista_texto_p,',');

ie_opcao_w := nvl(ie_opcao_p,'CI');

if	(lista_w.count	>0) then
	
	for i in lista_w.first..lista_w.last loop
		begin
		nr_seq_texto_w	:= lista_w(i);
		
		if	(nr_seq_texto_w	is not null) then
			select	nvl(DS_TITULO,' '),
				ds_texto
			into	DS_TITULO_w,
				ds_texto_w
			from	texto_padrao
			where	nr_sequencia	= nr_seq_texto_w;	
			
			if	(ie_opcao_w	= 'CI') then
			
				select	PEP_PAC_CI_seq.nextval
				into	nr_seq_ci_w
				from	dual;
			
				insert into PEP_PAC_CI ( NR_SEQUENCIA,
							 CD_ESTABELECIMENTO,
							 CD_PESSOA_FISICA,
							 DT_ATUALIZACAO,
							 NM_USUARIO,
							 DT_ATUALIZACAO_NREC,
							 NM_USUARIO_NREC,
							 CD_PROFISSIONAL,
							 DS_TITULO,
							 DS_TEXTO,
							 NR_ATENDIMENTO,
							 NR_SEQ_AGENDA,
							 DT_LIBERACAO,
							 NR_SEQ_AVAL_PRE,
							 IE_SITUACAO,
							 DT_INATIVACAO,
							 NM_USUARIO_INATIVACAO,
							 DS_JUSTIFICATIVA,
							 NR_SEQ_AGE_CONS,
							 CD_PERFIL_ATIVO,
							 NR_SEQ_REG_ELEMENTO,
							 IE_TIPO_CONSENTIMENTO,
							 NR_SEQ_TEXTO,
							 NR_SEQ_PROC_INTERNO,
							 IE_LADO,
							 CD_PROTOCOLO,
							 NR_SEQ_MEDICACAO,
							 NR_CICLO,
							 DS_DIA_CICLO,
							 NR_SEQ_ASSINATURA,
							 NR_SEQ_ASSINAT_INATIVACAO,
							 NR_PRESCRICAO)
						values (
							 nr_seq_ci_w,
							 cd_estabelecimento_p,
							 cd_paciente_p,
							 sysdate,
							 nm_usuario_p,
							 sysdate,
							 nm_usuario_p,
							 CD_PROFISSIONAL_P,
							 DS_TITULO_w,
							 ds_texto_w,
							 NR_ATENDIMENTO_p,
							 null,
							 null,
							 null,
							 'A',
							 null,
							 null,
							 null,
							 null,
							 null,
							 nr_seq_reg_elemento_p,
							 null,
							 nr_seq_texto_w,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null);
							 
				elsif	(ie_opcao_w	= 'E') then
					
					select	max(ie_tipo_evolucao)
					into	ie_tipo_evolucao_w
					from	usuario
					where	nm_usuario = nm_usuario_p;
					
					select	evolucao_paciente_seq.nextval
					into	nr_seq_ci_w
					from	dual;
					
					insert into evolucao_paciente (
						cd_evolucao,
						DT_EVOLUCAO,
						IE_TIPO_EVOLUCAO,
						CD_PESSOA_FISICA,
						DT_ATUALIZACAO,
						NR_ATENDIMENTO,
						DS_EVOLUCAO,
						CD_MEDICO,
						NM_USUARIO,
						IE_SITUACAO,
						ie_evolucao_clinica)
					values (nr_seq_ci_w,
							sysdate,
							ie_tipo_evolucao_w,
							cd_paciente_p,
							sysdate,
							NR_ATENDIMENTO_p,
							ds_texto_w,
							CD_PROFISSIONAL_P,
							nm_usuario_p,
							'A',
							Obter_tipo_evolucao_regra(NR_ATENDIMENTO_P));
							
				elsif	(ie_opcao_w	= 'OA') then
					select	ATENDIMENTO_ALTA_seq.nextval
					into	nr_seq_ci_w
					from	dual;
				
					insert into ATENDIMENTO_ALTA (
						 NR_SEQUENCIA,
						 NR_ATENDIMENTO,
						 IE_TIPO_ORIENTACAO,
						 DT_ATUALIZACAO,
						 NM_USUARIO,
						 DS_ORIENTACAO,
						 IE_DESFECHO,
						 CD_ESPECIALIDADE,
						 CD_MOTIVO_ALTA,
						 CD_MEDICO_DEST,
						 IE_MEDICO_CIENTE,
						 DT_DESFECHO,
						 CD_SETOR_ATENDIMENTO,
						 DT_LIBERACAO,
						 CD_PROCESSO_ALTA,
						 QT_CARACTERES,
						 IE_SITUACAO,
						 DT_INATIVACAO,
						 NM_USUARIO_INATIVACAO,
						 DS_JUSTIFICATIVA,
						 NR_SEQ_PEPO,
						 CD_PERFIL_ATIVO,
						 NR_SEQ_ASSINATURA,
						 NR_CIRURGIA,
						 DT_REGISTRO,
						 NR_SEQ_ASSINAT_INATIVACAO,
						 IE_TIPO_EVOLUCAO,
						 CD_ESPECIALIDADE_MEDICO,
						 QT_PERIODO,
						 IE_PERIODO,
						 CD_CGC,
						 NR_SEQ_MOTIVO,
						 IE_RECEM_NATO,
						 NR_RECEM_NATO,
						 NR_SEQ_AREA_ATUACAO,
						 IE_AVALIADOR_AUX,
						 DT_LIBERACAO_AUX,
						 CD_PROFISSIONAL,
						 NM_USUARIO_NREC,
						 DT_ATUALIZACAO_NREC)
					values (
						 nr_seq_ci_w,
						 NR_ATENDIMENTO_P,
						 'M',
						 sysdate,
						 NM_USUARIO_p,
						 ds_texto_w,
						 'A',
						 null,
						 null,
						 null,
						 'N',
						 sysdate,
						 null,
						 null,
						 null,
						 null,
						 'A',
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 sysdate,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 'N',
						 null,
						 null,
						 'N',
						 null,
						 obter_pessoa_fisica_usuario( nm_usuario_p,'C'),
						 nm_usuario_p,
						 sysdate);

				elsif	(ie_opcao_w	= 'AT') then
				
				select	ATESTADO_PACIENTE_seq.nextval
				into	nr_seq_ci_w
				from	dual;
				
				insert into ATESTADO_PACIENTE(
					NR_SEQUENCIA,
					DT_ATESTADO,
					NR_ATENDIMENTO,
					DT_ATUALIZACAO,
					NM_USUARIO,
					CD_MEDICO,
					CD_PESSOA_FISICA,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					DS_ATESTADO,
					IE_SITUACAO
				) values (
					nr_seq_ci_w,
					sysdate,
					NR_ATENDIMENTO_P,
					sysdate,
					NM_USUARIO_p,
					CD_PROFISSIONAL_P,
					CD_PACIENTE_P,
					sysdate,
					NM_USUARIO_p,
					ds_texto_w,
					'A'
				);
				
				end if;
				
				
				commit;
		substituir_macro_protocolo_pa(nr_seq_ci_w,ds_texto_w,nr_atendimento_p,nm_usuario_p,ie_opcao_w);
		
		end if;
		
		end;
	end loop;
end if;

commit;

end Gerar_Texto_Rotina_Padrao;
/