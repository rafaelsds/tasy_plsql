create or replace
procedure gerar_trib_conf_venc_nf_contr(
			nr_sequencia_nf_p   		number,
			nr_sequencia_regra_p		number,
			vl_nota_p			number,
			qt_item_nf_p			number,
			qt_conversao_p			number) is

cd_tributo_w				number(03,0);
vl_tributo_w				number(15,2);
pr_tributo_w				number(7,4);
cd_beneficiario_w			varchar2(14);
cd_cond_pagto_w				number(5,0);
cd_conta_financ_w			number(10,0);
nr_seq_trans_reg_w			number(10,0);
nr_seq_trans_baixa_w			number(10,0);
ie_corpo_item_w				varchar2(1);
ie_regra_trib_w				varchar2(2);
ie_pagar_receber_w			varchar2(2);
cd_darf_contrato_w			varchar2(10);
cd_conta_financ_ww			number(10,0);
nm_usuario_w				varchar2(15);
cd_estabelecimento_w			number(4,0);
cd_cgc_w				varchar2(20);
cd_pessoa_fisica_w			varchar2(10);
cd_beneficiario_trib_w			varchar2(20);
pr_aliquota_w				number(15,4);
vl_minimo_base_w			number(15,2);
vl_minimo_tributo_w			number(15,2);
ie_acumulativo_w			varchar2(1);
vl_teto_base_w				number(15,2);
vl_desc_dependente_w			number(15,2);
cd_darf_w				varchar2(20);
dt_referencia_w				date;
cd_variacao_w				varchar2(2);
ie_periodicidade_w			varchar2(1);
qt_existe_w				number(3);
tx_tributo_w				number(15,4);
vl_nota_w				number(13,4);
vl_liquido_w				number(15,2);
dt_emissao_w				date;
dt_entrada_saida_w			date;
ie_soma_diminui_w			varchar2(01);
cd_tipo_servico_w				varchar2(100);
ds_irrelavante_w				varchar2(4000);

cursor c01 is 
select	a.cd_tributo,
	a.vl_tributo,
	a.pr_tributo,
	a.cd_beneficiario,
	a.cd_cond_pagto,
	a.cd_conta_financ,
	a.nr_seq_trans_reg,
	a.nr_seq_trans_baixa,
	a.ie_corpo_item,
	a.ie_regra_trib,
	c.ie_pagar_receber,
	a.cd_darf
from	contrato c,
	contrato_regra_pagto_trib a,
	contrato_regra_nf b
where	a.nr_seq_regra_nf	= b.nr_sequencia
and	b.nr_seq_contrato	= c.nr_sequencia
and	b.nr_sequencia		= nr_sequencia_regra_p
order by decode(a.ie_corpo_item, 'C', 0, 1);

begin

if	(nr_sequencia_nf_p > 0) then

	select	cd_estabelecimento,
		cd_cgc,
		cd_pessoa_fisica,
		nm_usuario,
		dt_emissao,
		dt_entrada_saida,
		cd_tipo_servico
	into	cd_estabelecimento_w,
		cd_cgc_w,
		cd_pessoa_fisica_w,
		nm_usuario_w,
		dt_emissao_w,
		dt_entrada_saida_w,
		cd_tipo_servico_w
	from	nota_fiscal
	where	nr_sequencia = nr_sequencia_nf_p;

end if;	
	
if (qt_conversao_p > 0) then
	vl_nota_w := (vl_nota_p * qt_conversao_p);
else
	vl_nota_w := (vl_nota_p * qt_item_nf_p);
end if;	

open c01;
loop
fetch c01 into
	cd_tributo_w,
	vl_tributo_w,
	pr_tributo_w,
	cd_beneficiario_w,
	cd_cond_pagto_w,
	cd_conta_financ_w,
	nr_seq_trans_reg_w,
	nr_seq_trans_baixa_w,
	ie_corpo_item_w,
	ie_regra_trib_w,
	ie_pagar_receber_w,
	cd_darf_contrato_w;
exit when c01%notfound;

	select	nvl(ie_soma_diminui,'N')
	into	ie_soma_diminui_w
	from	tributo
	where	cd_tributo = cd_tributo_w;
	
	atualiza_total_nota_fiscal(nr_sequencia_nf_p, nm_usuario_w);

	cd_conta_financ_ww	:= cd_conta_financ_w;

	obter_dados_trib_tit_pagar (cd_tributo_w,
				cd_estabelecimento_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				cd_beneficiario_trib_w,
				pr_aliquota_w,
				cd_cond_pagto_w,
				cd_conta_financ_w,
				nr_seq_trans_reg_w,
				nr_seq_trans_baixa_w,
				vl_minimo_base_w,
				vl_minimo_tributo_w,
				ie_acumulativo_w,
				vl_teto_base_w,
				vl_desc_dependente_w,
				cd_darf_w,
				dt_referencia_w,
				cd_variacao_w,
				ie_periodicidade_w,
				null,
				null,
				cd_tipo_servico_w,
				null,
				null,
				null,
				ds_irrelavante_w,
				null,
				0,
				ds_irrelavante_w,
				ds_irrelavante_w,
				vl_tributo_w,
				'N',
				null,
				null,
				null,
				null);
	
	if	(ie_regra_trib_w = 'N') then

		if	(nvl(vl_minimo_base_w,0) <= vl_nota_w) then
	
			if	(ie_corpo_item_w = 'V') then
				
				/* Verifica se existe o vencimento antes de gerar tributos */
				select	count(*)
				into	qt_existe_w
				from	nota_fiscal_venc
				where	nr_sequencia = nr_sequencia_nf_p
				and	dt_vencimento = dt_emissao_w
				and	ie_origem = 'N';
				
				if (qt_existe_w > 0) then
				
					select	count(*)
					into	qt_existe_w
					from	nota_fiscal_venc_trib
					where	nr_sequencia	= nr_sequencia_nf_p
					and	cd_tributo	= cd_tributo_w;
				
					if	(qt_existe_w = 0) then

						tx_tributo_w	:= pr_tributo_w;

						if	(vl_tributo_w > 0) then
							tx_tributo_w	:= dividir(vl_tributo_w * 100, vl_nota_w);
						else
							vl_tributo_w	:= dividir((vl_nota_w * tx_tributo_w), 100);
						end if;

						select	nvl(sum(vl_liquido),0)
						into	vl_liquido_w
						from	nota_fiscal_item
						where	nr_sequencia = nr_sequencia_nf_p;
						
						insert	into nota_fiscal_venc_trib
							(nr_sequencia,
							dt_vencimento,
							cd_tributo,
							vl_tributo,
							dt_atualizacao,
							nm_usuario,
							vl_base_calculo,
							tx_tributo,
							vl_desc_base,
							VL_TRIB_NAO_RETIDO,
							VL_BASE_NAO_RETIDO,
							VL_TRIB_ADIC,
							VL_BASE_ADIC,
							vl_reducao,
							cd_darf,
							ie_origem)
						values(	nr_sequencia_nf_p,
							dt_emissao_w,
							cd_tributo_w,
							vl_tributo_w,
							sysdate,
							nm_usuario_w,
							vl_liquido_w,
							tx_tributo_w,
							nvl(vl_desc_dependente_w,0),
							0,
							0,
							0,
							0,
							0,
							cd_darf_contrato_w,
							'N');
					
						update	nota_fiscal_venc
						set	vl_vencimento	= vl_vencimento + (decode(ie_soma_diminui_w, 'S', vl_tributo_w, 'D', vl_tributo_w * -1, 0))
						where	nr_sequencia	= nr_sequencia_nf_p
						and	dt_vencimento	= dt_emissao_w;
					else
						update	nota_fiscal_venc_trib
						set	vl_base_calculo	= vl_base_calculo + vl_nota_w,
							vl_tributo		= vl_tributo + vl_tributo_w,
							tx_tributo		= dividir(vl_tributo * 100, vl_base_calculo)
						where	nr_sequencia	= nr_sequencia_nf_p
						and	cd_tributo		= cd_tributo_w;
					end if;
				
				end if;
			end if;
		end if;
	else
		if	(ie_pagar_receber_w = 'P') then
			gerar_tributos_fornecedor(nr_sequencia_nf_p, cd_tributo_w, NM_USUARIO_W, dt_entrada_saida_w);
		else
			gerar_imposto_nf(nr_sequencia_nf_p, NM_USUARIO_W, null, cd_tributo_w);
		end if;
	end if;
end loop;
close c01;
end gerar_trib_conf_venc_nf_contr;
/