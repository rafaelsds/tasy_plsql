create or replace
procedure	gerar_ordem_compra_consig_nf(
			nr_sequencia_p			number,
			nm_usuario_p			varchar2) is

nr_item_oci_w				number(5);
cd_comprador_padrao_w			varchar2(10);
cd_moeda_padrao_w			number(10);
cd_condicao_pagamento_padrao_w  		number(10);
cd_local_estoque_padrao_w			number(10);
cd_local_consig_w				number(10);
cd_pessoa_solic_padrao_w        		varchar2(10);
qt_dia_prazo_entrega_w			number(10);
qt_material_w				number(13,4);
cd_material_w               			number(6)    	:= 0;
cd_estabelecimento_w        			number(4)    	:= 1;
cd_unidade_medida_compra_w  		varchar2(30)  	:= '';
nr_ordem_compra_w			number(10);
dt_prevista_entrega_w			date;
cd_local_estoque_w			number(5,0)	:= '';
cd_local_entrega_w			number(5,0)	:= '';
cd_centro_custo_w			number(8,0)	:= '';
vl_unitario_material_w			number(17,4)	:= 0;
dt_entrega_w				date;
ie_aprova_auto_w				varchar2(1);
cd_conta_contabil_w			varchar2(20);
nm_pessoa_contato_w			varchar2(255);
tx_desc_ordem_w				number(7,4);
pr_desc_financeiro_w			number(13,4);
ie_solic_comprador_w			varchar2(1);
qt_existe_w				number(10);
vl_total_item_w				number(13,4);
vl_unitario_item_w				number(13,4);
cd_cgc_emitente_w			varchar2(14);

cursor	c01 is
select	cd_material,
	cd_unidade_medida_compra,
	qt_item_nf,
	vl_unitario_item_nf,
	cd_local_estoque,
	cd_centro_custo,
	cd_conta_contabil
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p
and	nr_sequencia_vinc_consig is not null
order by	nr_item_nf;

begin

select	cd_estabelecimento,
	cd_cgc_emitente
into	cd_estabelecimento_w,
	cd_cgc_emitente_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

select	nvl(cd_comprador_consig, cd_comprador_padrao),
      	cd_moeda_padrao,
      	cd_condicao_pagamento_padrao,
      	cd_local_estoque_padrao,
	cd_local_consig,
      	nvl(cd_pessoa_solic_consig,cd_pessoa_solic_padrao),
	ie_aprova_ordem_consig,
	cd_local_entrega_consig,
	nvl(ie_solic_comprador,'N')
into  	cd_comprador_padrao_w,
      	cd_moeda_padrao_w,
      	cd_condicao_pagamento_padrao_w,
      	cd_local_estoque_padrao_w,
	cd_local_consig_w,
      	cd_pessoa_solic_padrao_w,
	ie_aprova_auto_w,
	cd_local_entrega_w,
	ie_solic_comprador_w
from  	parametro_compras
where 	cd_estabelecimento = cd_estabelecimento_w;

if	(cd_pessoa_solic_padrao_w is null) or
	(cd_moeda_padrao_w is null) or
	(cd_local_estoque_padrao_w is null) or
	(cd_comprador_padrao_w is null) then
	--(-20011,'Os parametros de compra est�o incompletos');
	wheb_mensagem_pck.exibir_mensagem_abort(195748);
end if;

select	nvl(obter_dados_pf_pj_estab(cd_estabelecimento_w, null, cd_cgc_emitente_w, 'EPE'),0),
	nvl(obter_dados_pf_pj_estab(cd_estabelecimento_w, null, cd_cgc_emitente_w, 'ECP'), cd_condicao_pagamento_padrao_w),
	substr(obter_dados_pf_pj_estab(cd_estabelecimento_w, null, cd_cgc_emitente_w, 'ENC'),1,255),
	obter_dados_pf_pj_estab(cd_estabelecimento_w, null, cd_cgc_emitente_w, 'TDO'),
	obter_dados_pf_pj_estab(cd_estabelecimento_w, null, cd_cgc_emitente_w, 'TDF')
into	qt_dia_prazo_entrega_w,
	cd_condicao_pagamento_padrao_w,
	nm_pessoa_contato_w,
	tx_desc_ordem_w,
	pr_desc_financeiro_w
from	dual;

if	(ie_solic_comprador_w = 'S') then
	begin

	select	max(nvl(cd_pessoa_fisica,cd_comprador_padrao_w))
	into	cd_comprador_padrao_w
	from	comprador
	where	cd_pessoa_fisica = cd_pessoa_solic_padrao_w;
	
	end;
end if;

dt_entrega_w	:= trunc(sysdate,'dd') + qt_dia_prazo_entrega_w;

select	ordem_compra_seq.nextval
into	nr_ordem_compra_w
from	dual;

insert into ordem_compra(
	nr_ordem_compra,
	cd_estabelecimento,
	cd_cgc_fornecedor,
	cd_condicao_pagamento,
	cd_comprador,
	dt_ordem_compra,
	dt_atualizacao,
	nm_usuario,
	cd_moeda,
	ie_situacao,
	dt_inclusao,
	cd_pessoa_solicitante,
	ie_frete,
	vl_frete,
	pr_desc_pgto_antec,
	pr_juros_negociado,
	cd_local_entrega,
	dt_entrega,
	ie_aviso_chegada, 
	ie_emite_obs,
	ie_urgente,
	ie_somente_pagto,
	ie_tipo_ordem,
	ds_pessoa_contato,
	pr_desconto,
	pr_desc_financeiro,
	cd_centro_custo)
values (
	nr_ordem_compra_w,
	cd_estabelecimento_w,
	cd_cgc_emitente_w,
	cd_condicao_pagamento_padrao_w,
	cd_comprador_padrao_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	cd_moeda_padrao_w,
	'A',
	sysdate,
	cd_pessoa_solic_padrao_w,
	'C',
	0,
	0,
	0,
	cd_local_entrega_w,
	dt_entrega_w,
	'N',
	'S',
	'N',
	'N',
	'G',
	nm_pessoa_contato_w,
	tx_desc_ordem_w,
	pr_desc_financeiro_w,
	cd_centro_custo_w);

open c01;
loop
fetch c01 into
	cd_material_w,
	cd_unidade_medida_compra_w,
	qt_material_w,
	vl_unitario_item_w,
	cd_local_estoque_w,
	cd_centro_custo_w,
	cd_conta_contabil_w;
exit when c01%notfound;
	begin

	select	nvl(max(nr_item_oci),0) + 1
	into	nr_item_oci_w
	from	ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_w;

	vl_total_item_w	:= (vl_unitario_item_w * qt_material_w);

	insert into ordem_compra_item(
		nr_ordem_compra,
		nr_item_oci,
		cd_material,
		cd_unidade_medida_compra,
		qt_material,
		qt_original,
		vl_unitario_material,
		vl_total_item,
		cd_local_estoque,
		cd_centro_custo,
		cd_conta_contabil,
		dt_atualizacao,
		nm_usuario,
		ie_situacao)
	values (
		nr_ordem_compra_w,
		nr_item_oci_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		qt_material_w,
		qt_material_w,
		vl_unitario_item_w,
		vl_total_item_w,
		cd_local_estoque_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		sysdate,
		nm_usuario_p,
		'A');

	select	to_date(sysdate + nvl(qt_dias_entrega_oc_consig,0))
	into	dt_prevista_entrega_w
	from	parametro_compras
	where	cd_estabelecimento = cd_estabelecimento_w;

	insert into ordem_compra_item_entrega(
		nr_sequencia,
		nr_ordem_compra,
		nr_item_oci,
		dt_prevista_entrega,
		dt_real_entrega,
		dt_entrega_original,
		dt_entrega_limite,
		qt_prevista_entrega,
		qt_real_entrega,
		dt_atualizacao,
		nm_usuario,
		ds_observacao)
	values(	ordem_compra_item_entrega_seq.nextval,
		nr_ordem_compra_w,
		nr_item_oci_w,
		dt_prevista_entrega_w,
		null,
		dt_entrega_w,
		dt_entrega_w,
		qt_material_w,
		null,
		sysdate,
		nm_usuario_p,
		null);

	end;
end loop;
close c01;

Calcular_Liquido_Ordem_Compra(nr_ordem_compra_w, nm_usuario_p);

if	(ie_aprova_auto_w = 'S') then
	begin
	select	count(*)
	into	qt_existe_w
	from	ordem_compra
	where	nr_ordem_compra	= nr_ordem_compra_w
	and	dt_aprovacao is null;
	/*Para verificar se a OC j� n�o esta aprovada ou reprovada*/
	if	(qt_existe_w > 0) then
		Gerar_Aprov_Ordem_Compra(nr_ordem_compra_w, null, 'S', nm_usuario_p);
	end if;
	end;
end if;

gerar_ordem_compra_venc(nr_ordem_compra_w, nm_usuario_p);	

end gerar_ordem_compra_consig_nf;
/