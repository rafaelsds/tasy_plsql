create or replace procedure material_imp_interacao_ficha(	nr_seq_ficha_p			number,
					nr_seq_ficha_interacao_p	number,
					ie_severidade_p			varchar2,
					ds_observacao_p			varchar2,
					ds_referencia_p			varchar2,
					nm_usuario_p			varchar2,
					ds_observation_notes_p varchar2 default null,
					ds_precaution_notes_p varchar2 default null) is 
	

qt_reg_w			number(10);
qt_reg_ww			number(10);
material_interacao_medic_w 	material_interacao_medic%rowtype;
isdirtycheck                  NUMBER(1, 0) := 0; 
	
begin

select	count(1)
into	qt_reg_w
from	material_interacao_medic
where	nr_seq_ficha = 	nr_seq_ficha_p
and	nr_seq_ficha_interacao = nr_seq_ficha_interacao_p;
/*
select	count(1)
into	qt_reg_ww
from	material_interacao_medic
where	nr_seq_ficha = 	nr_seq_ficha_interacao_p
and	nr_seq_ficha_interacao = nr_seq_ficha_p;

*/
qt_reg_ww	:= 0; 

if	(qt_reg_w	= 0) and (qt_reg_ww = 0) then
	
	select	material_interacao_medic_seq.nextval
	into	material_interacao_medic_w.nr_sequencia
	from	dual;
	
	material_interacao_medic_w.nm_usuario				:= nm_usuario_p;
	material_interacao_medic_w.nm_usuario_nrec			:= nm_usuario_p;
	material_interacao_medic_w.dt_atualizacao			:= sysdate;
	material_interacao_medic_w.dt_atualizacao_nrec			:= sysdate;
	material_interacao_medic_w.ie_severidade			:= ie_severidade_p;
	material_interacao_medic_w.ds_orientacao			:= substr(ds_observacao_p,1,2000);
	material_interacao_medic_w.nr_seq_ficha				:= nr_seq_ficha_p;
	material_interacao_medic_w.nr_seq_ficha_interacao		:= nr_seq_ficha_interacao_p;
	material_interacao_medic_w.IE_CONSISTIR				:= 'P';
	material_interacao_medic_w.IE_MENSAGEM_CADASTRADA		:= 'B';
	material_interacao_medic_w.DS_REF_BIBLIOGRAFICA			:= substr(ds_referencia_p,1,4000);
	if (ds_observation_notes_p is not null) then 
		material_interacao_medic_w.ds_observation_notes		:= substr(ds_observation_notes_p,1,256);
	end if;
	if (ds_precaution_notes_p is not null) then 
		material_interacao_medic_w.ds_precaution_notes		:= substr(ds_precaution_notes_p,1,4000);
	end if;
	
	insert into material_interacao_medic values material_interacao_medic_w;

--elsif (qt_reg_w	= 1) then
else
	SELECT Count(1) INTO   isdirtycheck FROM (
	select ie_severidade_p as ie_severidade, substr(ds_observacao_p,1,2000) as ds_orientacao, substr(ds_referencia_p,1,4000) as DS_REF_BIBLIOGRAFICA, 
		substr(ds_observation_notes_p,1,256) as DS_OBSERVATION_NOTES, substr(ds_precaution_notes_p,1,4000) as DS_PRECAUTION_NOTES  from dual
	minus
	select ie_severidade, ds_orientacao, DS_REF_BIBLIOGRAFICA, DS_OBSERVATION_NOTES, DS_PRECAUTION_NOTES from material_interacao_medic 
		where	nr_seq_ficha = 	nr_seq_ficha_p and	nr_seq_ficha_interacao = nr_seq_ficha_interacao_p
	);
	
	if(isdirtycheck !=  0) then
		update material_interacao_medic 
			set ie_severidade = ie_severidade_p, 
				ds_orientacao = substr(ds_observacao_p,1,2000),
				DS_REF_BIBLIOGRAFICA = substr(ds_referencia_p,1,4000),
				DS_OBSERVATION_NOTES = substr(ds_observation_notes_p,1,256),
				DS_PRECAUTION_NOTES = substr(ds_precaution_notes_p,1,4000),
				dt_atualizacao = sysdate, nm_usuario = 'MIMSUPDATE'
			where	nr_seq_ficha = 	nr_seq_ficha_p and	nr_seq_ficha_interacao = nr_seq_ficha_interacao_p;
	end if;
	
end if;



end material_imp_interacao_ficha;
/
