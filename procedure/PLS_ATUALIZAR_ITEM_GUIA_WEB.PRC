create or replace
procedure pls_atualizar_item_guia_web
			(	nr_seq_item_p		number,
				qt_autorizada_p		number,
				ie_status_p		varchar2,
				nm_usuario_p		varchar2,
				ie_tipo_item_p		varchar2) is


	
begin

if	(ie_tipo_item_p = 'P') then
	update	pls_guia_plano_proc
	set	ie_status		= decode(ie_status_p,'A','P','N','M'),
		dt_atualizacao		= sysdate,
		dt_liberacao		= sysdate,
		qt_autorizada		= qt_autorizada_p,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_item_p; 
	
elsif	(ie_tipo_item_p = 'M') then
	update	pls_guia_plano_mat
	set	qt_autorizada		= qt_autorizada_p,
		ie_status 		= decode(ie_status_p,'A','P','N','M'),
		dt_atualizacao		= sysdate,
		dt_liberacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia 		= nr_seq_item_p; 
end if;

commit;

end pls_atualizar_item_guia_web;
/