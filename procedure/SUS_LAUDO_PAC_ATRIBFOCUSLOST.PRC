create or replace
procedure sus_laudo_pac_AtribFocusLost(	cd_pessoa_fisica_p		varchar2,
					cd_procedimento_solic_p		varchar2,
					cd_medico_requisitante_p	varchar2,
					ie_origem_proced_p		number,
					cd_estabelecimento_p		number,
					ie_consiste_data_laudo_p	out varchar2,
					ie_permite_proc_p		out varchar2,
					ie_consiste_idade_proc_p	out varchar2) is

begin

ie_consiste_data_laudo_p	:= sus_consiste_data_laudo_APAC(cd_pessoa_fisica_p, cd_procedimento_solic_p);

ie_permite_proc_p		:= sus_obter_se_permite_proc(cd_medico_requisitante_p, ie_origem_proced_p, cd_procedimento_solic_p, 0, cd_estabelecimento_p);

ie_consiste_idade_proc_p	:= sus_consiste_idade_proc(cd_procedimento_solic_p, ie_origem_proced_p, cd_pessoa_fisica_p);

end sus_laudo_pac_AtribFocusLost;
/
