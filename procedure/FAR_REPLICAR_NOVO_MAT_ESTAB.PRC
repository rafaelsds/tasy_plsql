create or replace
procedure	far_replicar_novo_mat_estab(
			cd_material_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

qt_existe_w			number;
cd_estabelecimento_w		number(4);
ie_estoque_lote_w			varchar2(1);
ie_controla_serie_w		varchar2(1);
ie_requisicao_w			varchar2(1);
ie_prescricao_w			varchar2(1);
ie_padronizado_w			varchar2(1);
ie_ressuprimento_w		varchar2(1);
ie_material_estoque_w		varchar2(1);
ie_baixa_estoq_pac_w		varchar2(1);
lista_estab_w			varchar2(255);
cd_unidade_medida_compra_w	varchar2(30);
cd_unidade_medida_venda_w	varchar2(30);
cd_unidade_medida_estoque_w	varchar2(30);
ie_farmacia_com_w		varchar2(1);

cursor	c01 is
select	cd_estabelecimento
from	estabelecimento_v;

begin

select	cd_unidade_medida_compra,
	cd_unidade_medida_estoque,
	cd_unidade_venda,
	ie_farmacia_com
into	cd_unidade_medida_compra_w,
	cd_unidade_medida_estoque_w,
	cd_unidade_medida_venda_w,
	ie_farmacia_com_w
from	material_estab
where	cd_material = cd_material_p;

open c01;
loop
fetch c01 into
	cd_estabelecimento_w;
exit when c01%notfound;
	begin

	select	count(*)
	into	qt_existe_w
	from	material_estab
	where	cd_estabelecimento = cd_estabelecimento_w
	and	cd_material = cd_material_p;

	/*Estes campos n�o podem ter o valor padr�o para os outros estabelecimentos, ou seja:
	Nos outros estabelecimentos n�o podem gerar estoque, ressuprimento, etc, etc*/
	ie_estoque_lote_w		:= 'N';
	ie_controla_serie_w		:= 'N';
	ie_requisicao_w		:= 'S';
	ie_prescricao_w		:= 'S';
	ie_padronizado_w		:= 'S';
	ie_ressuprimento_w		:= 'S';
	ie_material_estoque_w	:= 'S';
	ie_baixa_estoq_pac_w		:= 'S';
	if	(cd_estabelecimento_w <> cd_estabelecimento_p) then
		ie_requisicao_w		:= 'N';
		ie_prescricao_w		:= 'N';
		ie_padronizado_w		:= 'N';
		ie_ressuprimento_w		:= 'N';
		ie_material_estoque_w	:= 'N';
		ie_baixa_estoq_pac_w		:= 'N';
	end if; 

	if	(qt_existe_w = 0) then
		begin

		insert into material_estab(
			nr_sequencia,			cd_estabelecimento,
			cd_material,			dt_atualizacao,
			nm_usuario,			ie_baixa_estoq_pac,
			ie_material_estoque,		qt_dia_interv_ressup,
			qt_dia_ressup_forn,		nr_minimo_cotacao,
			ie_ressuprimento,			dt_atualizacao_nrec,
			nm_usuario_nrec,			ie_classif_custo,
			ie_prescricao,			ie_padronizado,
			ie_estoque_lote,			ie_requisicao,
			ie_controla_serie,			cd_unidade_medida_compra,
			cd_unidade_medida_estoque,	cd_unidade_venda,
			ie_farmacia_com)
		values(material_estab_seq.nextval,		cd_estabelecimento_w,
			cd_material_p,			sysdate,	
			nm_usuario_p,			ie_baixa_estoq_pac_w,
			ie_material_estoque_w,		10,
			3,				1,
			ie_ressuprimento_w,		sysdate,
			nm_usuario_p,			'B',
			ie_prescricao_w,			ie_padronizado_w,
			ie_estoque_lote_w,		ie_requisicao_w,
			ie_controla_serie_w,		cd_unidade_medida_compra_w,
			cd_unidade_medida_estoque_w,	cd_unidade_medida_venda_w,
			ie_farmacia_com_w);

		end;
	end if;

	end;
end loop;
close c01;

commit;

end far_replicar_novo_mat_estab;
/