create or replace
procedure OBTER_CUSTO_MATERIAL_REPASSE
	(cd_estabelecimento_p	in	number,
	 cd_material_p		in	number,
	 vl_custo_p		out	number) is

ie_forma_calculo_w	varchar2(255);
qt_compras_w		number(10,0);
vl_unitario_nf_w	number(15,2);
vl_retorno_w		number(15,2);
cont_w			number(10,0);

cursor c01 is
select	a.ie_forma_calculo,
	a.qt_compras
from	regra_custo_mat_repasse a
where	a.cd_estabelecimento			= cd_estabelecimento_p
order	by nvl(a.cd_material, 0),
	nvl(a.cd_grupo_material, 0),
	nvl(a.cd_subgrupo_material, 0),
	nvl(a.cd_classe_material, 0);

begin

vl_retorno_w		:= 0;

ie_forma_calculo_w	:= null;
qt_compras_w		:= 0;
open c01;
loop
fetch c01 into
	ie_forma_calculo_w,
	qt_compras_w;
exit when c01%notfound;
end loop;
close c01;

if	(ie_forma_calculo_w = '1') then

	if	(qt_compras_w > 0) then
		select	sum(vl_unitario_item_nf),
			count(*)
		into	vl_unitario_nf_w,
			cont_w
		from	(select	distinct decode(a.cd_unidade_medida_compra, a.cd_unidade_medida_estoque, a.vl_unitario_item_nf, dividir_sem_round(a.vl_unitario_item_nf, a.qt_item_estoque)) vl_unitario_item_nf,
				b.dt_entrada_saida
			from	natureza_operacao o,
				nota_fiscal_item a,
				nota_fiscal b
			where	b.nr_sequencia		= a.nr_sequencia
			and	b.ie_acao_nf		= '1'
			and	b.ie_situacao		= '1'
			and	a.cd_estabelecimento	= cd_estabelecimento_p
			and	b.cd_natureza_operacao	= o.cd_natureza_operacao
			and	o.ie_entrada_saida	= 'E'
			and	b.dt_atualizacao_estoque	is not null
			and	a.cd_material		= cd_material_p
			order 	by b.dt_entrada_saida desc)
		where	rownum <= qt_compras_w;
		vl_retorno_w	:= (dividir_sem_round(vl_unitario_nf_w, cont_w));
	end if;

end if;

vl_custo_p		:= vl_retorno_w;

end OBTER_CUSTO_MATERIAL_REPASSE;
/