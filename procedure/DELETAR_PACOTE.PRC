create or replace
procedure deletar_pacote(	nr_seq_pacote_p		number,
			dt_vigencia_p		date,
			nm_usuario_p		varchar2) is 

begin
if	((nr_seq_pacote_p is not null) and
	(dt_vigencia_p is not null) and
	(nm_usuario_p is not null)) then
	begin
	
	delete	from pacote_material
	where	nr_seq_pacote	= nr_seq_pacote_p
	and	nr_seq_pac_acomod in (	select	nr_sequencia
					from	pacote_tipo_acomodacao
					where	dt_vigencia	= dt_vigencia_p
					and	nr_seq_pacote	= nr_seq_pacote_p );
	
	delete	from pacote_procedimento
	where	nr_seq_pacote	= nr_seq_pacote_p
	and	nr_seq_pac_acomod in (	select	nr_sequencia
					from	pacote_tipo_acomodacao
					where	dt_vigencia	= dt_vigencia_p
					and	nr_seq_pacote	= nr_seq_pacote_p );
	
	delete	from pacote_tipo_acomodacao
	where	nr_seq_pacote	= nr_seq_pacote_p
	and	dt_vigencia	= dt_vigencia_p;
	
	end;
end if;
commit;

end deletar_pacote;
/