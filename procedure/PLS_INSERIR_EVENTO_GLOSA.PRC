create or replace
procedure pls_inserir_evento_glosa
			(	nr_seq_motivo_glosa_p		Number,
				ie_evento_p			Varchar,
				nm_usuario_p			Varchar2) is 

begin

insert into pls_glosa_evento
	(nr_sequencia, dt_atualizacao, nm_usuario, 
	dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_motivo_glosa,
	ie_evento, ie_plano)
values(	pls_glosa_evento_seq.nextval, sysdate, nm_usuario_p,
	sysdate, nm_usuario_p, nr_seq_motivo_glosa_p,
	ie_evento_p, 'N');

commit;

end pls_inserir_evento_glosa;
/