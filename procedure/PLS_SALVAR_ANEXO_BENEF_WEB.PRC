create or replace
procedure pls_salvar_anexo_benef_web
			(	nr_seq_inclusao_p		number,	
				ds_caminho_p			varchar2,
				nm_usuario_p			varchar2,
				ie_apresenta_portal_p		varchar2,	
				nr_seq_tipo_documento_p		number) is 

begin
	

if	(ds_caminho_p is not null) then
	insert into pls_inclusao_anexo(nr_sequencia, nr_seq_inclusao, dt_anexo,           
				     ds_anexo,dt_atualizacao, nm_usuario,         
				     dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_tipo_documento, ie_apresenta_portal)     
			      values(pls_inclusao_anexo_seq.nextval, nr_seq_inclusao_p, sysdate,            
				     ds_caminho_p, sysdate, nm_usuario_p,        
				     sysdate, nm_usuario_p,nr_seq_tipo_documento_p, ie_apresenta_portal_p);
end if;

commit;

end pls_salvar_anexo_benef_web;
/
