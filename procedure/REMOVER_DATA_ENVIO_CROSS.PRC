create or replace
procedure remover_data_envio_cross(nr_sequencia_p	number) is 
					
begin

update dados_envio_cross set dt_envio = null where nr_sequencia = nr_sequencia_p;

commit;

end remover_data_envio_cross;
/