create or replace
procedure Ageint_Gerar_Equip_Agenda(
			nr_seq_agenda_p		number,
			nr_seq_item_p		number,
			nm_usuario_p		Varchar2) is 

cd_equipamento_w			number(10);
nr_seq_classif_equip_w		number(10);
ie_utilizar_equip_presc_w	varchar2(1);
			
Cursor C01 is
	select	cd_equipamento,
		nr_seq_classif_equip
	from	ageint_equip_item
	where	nr_seq_agenda_item	= nr_seq_item_p;	

Cursor C02 is
	select	a.cd_equipamento
	from	agenda a,
			agenda_paciente ap
	where	ap.nr_sequencia = nr_seq_agenda_p
	and		ap.cd_agenda = a.cd_agenda;
			
begin

Obter_Param_Usuario(869, 417, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_utilizar_equip_presc_w);

if	(nvl(ie_utilizar_equip_presc_w,'N') = 'N') then
	open C01;
	loop
	fetch C01 into	
		cd_equipamento_w,
		nr_seq_classif_equip_w;
	exit when C01%notfound;
		begin
		insert into agenda_pac_equip
				(nr_sequencia,
				nr_seq_agenda,
				dt_atualizacao,
				nm_usuario,
				cd_equipamento,
				nr_seq_classif_equip,
				ie_origem_inf,--I
				ie_obrigatorio)--S
			values
				(agenda_pac_equip_seq.nextval,
				nr_seq_agenda_p,
				sysdate,
				nm_usuario_p,
				cd_equipamento_w,
				nr_seq_classif_equip_w,
				'I',
				'S');
		end;
	end loop;
	close C01;
else
	open C02;
	loop
	fetch C02 into	
		cd_equipamento_w;
	exit when C02%notfound;
		begin
		insert into agenda_pac_equip
				(nr_sequencia,
				nr_seq_agenda,
				dt_atualizacao,
				nm_usuario,
				cd_equipamento,				
				ie_origem_inf,--I
				ie_obrigatorio)--S
			values
				(agenda_pac_equip_seq.nextval,
				nr_seq_agenda_p,
				sysdate,
				nm_usuario_p,
				cd_equipamento_w,
				'I',
				'S');
		end;
	end loop;
	close C02;
end if;


commit;

end Ageint_Gerar_Equip_Agenda;
/
