create or replace
procedure TISS_OBTER_REGRA_TABELA_MAT
			(ie_origem_preco_p	in 	varchar2,
			cd_material_p		in 	number,
			cd_estabelecimento_p	in 	number,
			cd_convenio_p		in 	number,
			cd_categoria_p		in 	varchar2,
			cd_tab_preco_material_p	in 	number,
			dt_vigencia_p		in 	date,
			cd_setor_atendimento_p	in 	number,
			cd_material_tuss_p	in 	number,
			vl_unitario_mat_p	in	number,
			nr_seq_tiss_tabela_p	out 	number,
			ie_desc_material_p	out	varchar2,
			ie_codigo_material_p	out 	varchar2,
			ie_conversao_externa_p	out	varchar2,
			cd_material_tiss_p	out 	varchar2,
			ds_material_tiss_p	out	varchar2) is

ie_tipo_material_w		varchar2(10);
nr_seq_tiss_tabela_w		number(15,0);
cd_tabela_relat_w			varchar2(10);
cd_tabela_xml_w			varchar2(10);
cd_grupo_material_w		number(15,0);
cd_subgrupo_material_w		number(15,0);
cd_classe_material_w		number(15,0);
ie_origem_preco_mat_w		number(10);
vl_unitario_mat_w			number(17,4) := 0;
ie_desc_material_w		varchar2(10);
ie_codigo_material_w		varchar2(10);
ie_conversao_externa_w		varchar2(10);
nr_seq_familia_w			number(10);
cd_material_tiss_w		varchar(20);
ds_material_tiss_w		varchar(200);

cursor c01 is
select	a.nr_seq_tiss_tabela,
	a.ie_desc_material,
	a.ie_conversao_externa,
	a.ie_cod_material,
	a.cd_material_tiss,
	a.ds_material_tiss
from 	tiss_regra_tabela_mat a
where	a.cd_estabelecimento					= cd_estabelecimento_p
and	a.cd_convenio						= cd_convenio_p
and	nvl(a.cd_categoria, cd_categoria_p)				= cd_categoria_p
and	nvl(a.cd_material, cd_material_p)				= cd_material_p
and	nvl(a.cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material, cd_subgrupo_material_w)		= cd_subgrupo_material_w
and	nvl(a.cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(dt_vigencia_p, sysdate)				between trunc(a.dt_inicio_vigencia, 'dd') and trunc(nvl(a.dt_final_vigencia,nvl(dt_vigencia_p, sysdate)+1)) + 89399/89400
and	nvl(a.ie_situacao,'A') = 'A'
and	((nvl(cd_tab_preco_material, nvl(cd_tab_preco_material_p,'00')) = nvl(cd_tab_preco_material_p,'00')) or
	 ((cd_tab_preco_material_p = '00') and (cd_tab_preco_material is null)))
and	nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_p,0))		= nvl(cd_setor_atendimento_p,0)
and	nvl(a.ie_origem_preco, nvl(ie_origem_preco_mat_w,0))		= nvl(ie_origem_preco_mat_w,0)
and	nvl(a.nr_seq_familia, nvl(nr_seq_familia_w,0))			= nvl(nr_seq_familia_w,0)
and	((nvl(a.ie_material_tuss,'A') = 'A') or
	 (nvl(a.ie_material_tuss,'A') = 'S' and nvl(cd_material_tuss_p,0) <> 0) or 
	 (nvl(a.ie_material_tuss,'A') = 'N' and nvl(cd_material_tuss_p,0) = 0))
and	((nvl(vl_unitario_mat_p,0) >= nvl(a.vl_inicial,0)) and (nvl(vl_unitario_mat_p,0) <= nvl(a.vl_final,999999999)))		 
and 	nvl(a.ie_tipo_material, nvl(ie_tipo_material_w, '0')) = nvl(ie_tipo_material_w, '0')
order	by ie_prioridade desc,
	nvl(cd_material, 0),
	nvl(cd_classe_material, 0),
	nvl(cd_subgrupo_material, 0),
	nvl(cd_grupo_material, 0),
	nvl(cd_tab_preco_material,0),
	nvl(a.cd_setor_atendimento,0),
	nvl(a.ie_origem_preco,0),
	nvl(a.vl_inicial,0),
	a.dt_inicio_vigencia,
	nvl(a.nr_seq_familia,0),
	nvl(a.ie_tipo_material,'0');

begin

select 	b.cd_grupo_material,
	b.cd_subgrupo_material,
	b.cd_classe_material,
	a.ie_tipo_material,
	a.nr_seq_familia
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	ie_tipo_material_w,
	nr_seq_familia_w
from	estrutura_material_v b,
	material a
where 	a.cd_material		= b.cd_material
and	a.cd_material		= cd_material_p;

if	(ie_origem_preco_p not in (1,4)) then
	ie_origem_preco_mat_w	:= 2;			/*2- Tabela de pre�o*/
else
	ie_origem_preco_mat_w	:= to_number(ie_origem_preco_p); 	/*1- Bras�ndice, 4-Simpro*/
end if;

nr_seq_tiss_tabela_w	:= null;
ie_desc_material_w	:= null;
ie_conversao_externa_w	:= null;
ie_codigo_material_w	:= null;

open c01;
loop
fetch c01 into
	nr_seq_tiss_tabela_w,
	ie_desc_material_w,
	ie_conversao_externa_w,
	ie_codigo_material_w,
	cd_material_tiss_w,
	ds_material_tiss_w;
exit when c01%notfound;
end loop;
close c01;

nr_seq_tiss_tabela_p	:= nr_seq_tiss_tabela_w;
ie_desc_material_p	:= ie_desc_material_w;
ie_conversao_externa_p	:= ie_conversao_externa_w;
ie_codigo_material_p	:= ie_codigo_material_w;
cd_material_tiss_p	:= cd_material_tiss_w;
ds_material_tiss_p	:= ds_material_tiss_w;

end TISS_OBTER_REGRA_TABELA_MAT;
/
