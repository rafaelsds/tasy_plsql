create or replace
procedure ptu_obter_se_consiste_mat(	nr_seq_inconsistencia_p	number,
					ie_consiste_p		out varchar2) is 

/*Finalidade: Verificar se deve realizar a consistencia no A400/450
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
*/
					
begin
select	nvl(max(ie_consiste),'N')
into	ie_consiste_p
from	ptu_inconsistencia_mat
where	nr_sequencia = nr_seq_inconsistencia_p;

end ptu_obter_se_consiste_mat;
/