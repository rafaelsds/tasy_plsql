create or replace
procedure gerar_regra_associada(nr_atendimento_p        number,
                                nr_pend_prot_assist_p   number,
                                nr_seq_protocolo_p      number,
                                nm_usuario_p            Varchar2) is 

--Busca as regras para os protocolos assistencias
Cursor C010 is
    select  max(a.nr_seq_regra)                                                             nr_seq_protocolo, 
            rtrim(xmlagg(xmlelement (e, a.nr_seq_regra_mult||',')).extract('//text()'),',') nr_seq_regra_mult,
            c.ie_tipo_pendencia                                                             ie_tipo_pendencia,
            max(nvl(a.qt_horas_retroativa, 0))                                              qt_horas_retroativa
    from    gqa_pendencia_regra_mult a,
            gqa_pendencia_regra      b,
            gqa_pendencia            c
    where   b.nr_sequencia = a.nr_seq_regra_mult
    and     c.nr_sequencia = b.nr_seq_pendencia
    and     nvl(a.ie_situacao,'A') = 'A'
    and     a.nr_seq_regra = nr_seq_protocolo_p 
    and     c.ie_tipo_pendencia <> 6
    and     b.ie_assistencial = 'S'
    group by c.ie_tipo_pendencia
    order by c.ie_tipo_pendencia;


--Percorre a lista como um cursor
C02				SYS_REFCURSOR;

nr_seq_protocolo_w              number(10);
ie_tipo_pendencia_w             number(10);
nr_seq_mentor_w                 number(10);	
nr_seq_prot_item_w              number(10);	
qt_horas_retroativa_w           number(10);	

ConvertSplitStrToCursor         varchar2(2000);
nr_seq_split_regra_mult_w       varchar2(2000);
nr_regras_atendidas_w           varchar2(2000);
nr_seq_regra_mult_w             varchar2(255);
ie_resultado_w                  varchar2(1);

dt_protocolo_assist_w           date;

/*Constante Tipos de regras */
ConstTipoDiagnostico            number(2) := 1;
ConstTipoExames                 number(2) := 2;
ConstTipoSinalVital             number(2) := 3;
ConstTipoEscalaIndice           number(2) := 4;
ConstTipoCurativo               number(2) := 5;
ConstTipoProtAssist             number(2) := 6;
ConstTipoEvento                 number(2) := 7;
ConstClassificacaoRisco         number(2) := 8;
ConstTipoCIAP					number(2) := 9;

begin
    dt_protocolo_assist_w := sysdate;
        
    open C010;
    loop
    fetch C010 into
        nr_seq_protocolo_w,
        nr_seq_split_regra_mult_w,
        ie_tipo_pendencia_w,
        qt_horas_retroativa_w;
    exit when C010%notfound;
        begin

            case ie_tipo_pendencia_w 
            
                when ConstTipoDiagnostico then
                    GQA_Liberacao_diagnostico(  null, --Sequencia diagnosticos
                                                nm_usuario_p,
                                                nr_seq_mentor_w,
                                                nr_regras_atendidas_w,
                                                nr_atendimento_p,
                                                nr_seq_split_regra_mult_w,
                                                qt_horas_retroativa_w);
                
                when ConstTipoExames then                
                    GQA_Aprov_Exame_result_item(  null, --Sequencia Exame
                                                  null, --Sequencia resultado
                                                  nm_usuario_p,
                                                  nr_regras_atendidas_w,
                                                  nr_atendimento_p,
                                                  nr_seq_split_regra_mult_w,
                                                  qt_horas_retroativa_w);
                
                when ConstTipoSinalVital then
                    GQA_Liberacao_Sinal_Vital(  null, -- Sequencia SV
                                                nm_usuario_p, 
                                                nr_regras_atendidas_w,
                                                nr_atendimento_p,
                                                nr_seq_split_regra_mult_w,
                                                qt_horas_retroativa_w);
                when ConstTipoEscalaIndice then
                    GQA_Liberacao_Escala(   nr_atendimento_p,
                                            null, -- Sequencia da escala
                                            nm_usuario_p,
                                            null, -- Tabela da escala
                                            nr_regras_atendidas_w,
                                            null, -- Regra sepse
                                            nr_seq_split_regra_mult_w,
                                            qt_horas_retroativa_w);
                when ConstTipoCurativo then
                    GQA_Liberacao_Curativo( nr_atendimento_p,
                                            null, -- Sequencia do curativo
                                            nm_usuario_p,
                                            nr_regras_atendidas_w,
                                            nr_seq_split_regra_mult_w,
                                            qt_horas_retroativa_w);

                when ConstTipoEvento then
                    GQA_Liberacao_qua_evento(   null, -- Sequencia do Evento)
                                                nm_usuario_p,
                                                nr_regras_atendidas_w,
                                                nr_atendimento_p,
                                                nr_seq_split_regra_mult_w,
                                                qt_horas_retroativa_w);
                
                when ConstClassificacaoRisco then
                    GQA_Classificacao_Risco(    null,  --Sequencia da triagem
                                                nr_atendimento_p,
                                                nm_usuario_p,
                                                nr_regras_atendidas_w,
                                                null, -- Sequencia do discriminador
                                                nr_seq_split_regra_mult_w,
                                                qt_horas_retroativa_w);

                when ConstTipoProtAssist then
                    null;                
				
				when ConstTipoCIAP then
					null;
            end case;
            
            nr_regras_atendidas_w := nvl(nr_regras_atendidas_w, 'XXXXXXX');
            
            nr_seq_split_regra_mult_w := chr(39) || nr_seq_split_regra_mult_w || chr(39);
            ConvertSplitStrToCursor :=  'select '   
                || 'substr( '|| nr_seq_split_regra_mult_w || ', ' 
                ||' decode(level, 1, 1, instr(' || nr_seq_split_regra_mult_w || ', '','', 1, level - 1) + 1), ' 
                ||' decode(instr(' || nr_seq_split_regra_mult_w || ', '','', 1, level), 0, length(' || nr_seq_split_regra_mult_w || '), instr(' || nr_seq_split_regra_mult_w || ', '','', 1, level) -  ' 
                ||'         decode(level, 1, 0, instr(' || nr_seq_split_regra_mult_w || ', '','', 1, level - 1)) - 1) ' 
                ||' ) ' 
                || ' from  dual '
                || ' connect by level <= length(' || nr_seq_split_regra_mult_w || ') - length(replace(' || nr_seq_split_regra_mult_w || ', '','')) + 1';
            
            open C02 for ConvertSplitStrToCursor;
            loop
            fetch C02 into
                nr_seq_regra_mult_w;
            exit when C02%notfound;
                begin
                                  
                    ie_resultado_w := 'N';
                    if instr(nr_regras_atendidas_w, nr_seq_regra_mult_w) > 0 then 
                        ie_resultado_w := 'S';
                    end if;

                    select  protocolo_assist_item_seq.nextval
                    into    nr_seq_prot_item_w
                    from    dual;

                    insert into protocolo_assist_item (nr_sequencia,
                                                       dt_atualizacao,
                                                       nm_usuario,
                                                       nr_seq_regra_gqa,
                                                       ie_resultado, 
                                                       nr_seq_protocolo)
                        values(                        nr_seq_prot_item_w,
                                                       dt_protocolo_assist_w,
                                                       nm_usuario_p,
                                                       nr_seq_regra_mult_w,
                                                       ie_resultado_w,
                                                       nr_pend_prot_assist_p);                                             
            
                end;
            end loop;
            close C02; 
            commit;
        
        end;
    end loop;
    close C010;
    
end gerar_regra_associada;
/
