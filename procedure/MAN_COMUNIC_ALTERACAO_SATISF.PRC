create or replace 
procedure man_comunic_alteracao_satisf (nr_sequencia_p		number,
										nm_usuario_p		varchar2,
										ds_justificativa_p	varchar2,
										ie_opcao_p			varchar2) is

ds_titulo_w				varchar2(255);
ds_comunicado_w			varchar2(4000);
nr_sequencia_w			number(10,0);
nr_seq_classif_w		number(10,0);
nm_usuario_w			varchar2(15);
ds_dano_breve_w			varchar2(80);
ds_grau_satisfacao_w	varchar2(80);
nm_usuario_exec_w		varchar(15);
ds_comunic_exec_w		varchar(2000);
nr_grupo_trabalho_w		number(10,0);

/* Ie_opcao_p
'N' - N�o envia
'S' - Solicitante
'R' - Responsavel setor
'E' - Executor
'T' - Todos
'G' - Respons�vel pelo grupo de trabalho
'A' - Todos os respons�veis (Setor + Grupo de trabalho)
*/


cursor C01 is

select	distinct(nm_usuario) 
from (
	select	nm_usuario_exec nm_usuario
	from	man_ordem_servico_exec
	where	nr_seq_ordem		= nr_sequencia_p
	and	ie_opcao_p		in ('E','T')
	union all
	select	min(b.nm_usuario) nm_usuario
	from	usuario b,
		man_ordem_servico a
	where	a.nr_sequencia		= nr_sequencia_p
	and	b.cd_pessoa_fisica		= a.cd_pessoa_solicitante
	and	ie_opcao_p		in ('S','T')
	union all
	select	substr(obter_usuario_pessoa(a.cd_pessoa_resp),1,60) nm_usuario
	from	setor_atendimento a,
		man_ordem_servico_exec c
	where	a.cd_setor_atendimento = obter_setor_usuario(c.nm_usuario_exec)
	and 	c.nr_seq_ordem		= nr_sequencia_p
	and	ie_opcao_p		in ('R','T','A')
	union all
	select	nm_usuario_param nm_usuario
	from	man_grupo_trab_usuario
	where	nvl(ie_responsavel,'N') = 'S'
	and	nr_seq_grupo_trab	= nr_grupo_trabalho_w
	and	ie_opcao_p		in ('T','G','A'));
	
cursor c02 is
	select	distinct
		nm_usuario_exec
	from	(
		select	nm_usuario_exec
		from	man_ordem_servico_exec
		where	nr_seq_ordem = nr_sequencia_p
		union all
		select	nm_usuario_exec
		from	man_ordem_serv_ativ
		where	nr_seq_ordem_serv = nr_sequencia_p);

begin

select	substr('Dano: ' || ds_dano_breve,1,80),
	substr(obter_desc_expressao(290975)/*'Grau de satisfa��o: '*/ ||': '|| substr(obter_valor_dominio(1197,ie_grau_satisfacao),1,40),1,80),
	nr_grupo_trabalho
into	ds_dano_breve_w,
	ds_grau_satisfacao_w,
	nr_grupo_trabalho_w
from	man_ordem_servico
where	nr_sequencia = nr_sequencia_p;

ds_titulo_w	:= 'Altera��o do grau de satisfa��o da Ordem de servi�o n� ' || nr_sequencia_p;

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

ds_comunicado_w	:= substr(ds_dano_breve_w || chr(13) || chr(10) || ds_grau_satisfacao_w || chr(13) || chr(10) || 
		  		'Justificativa de altera��o do grau de satisfa��o: ' || chr(13) || chr(10) || 
		   		ds_justificativa_p,1,4000);

if	(ie_opcao_p <> 'E') then
	begin
	
	open c02;
	loop
	fetch c02 into
		nm_usuario_exec_w;
	exit when c02%notfound;
		begin
		if	(nm_usuario_exec_w is not null) then
			ds_comunic_exec_w := ds_comunic_exec_w || Obter_Nome_Usuario(nm_usuario_exec_w) || chr(13)||chr(10);
		end if;
		end;
	end loop;
	close c02;
	
	if	(ds_comunic_exec_w is not null) then
		ds_comunicado_w := substr(ds_comunicado_w || chr(13)||chr(10) ||
			chr(13)||chr(10) || 'Executores da OS: ' || chr(13)||chr(10) || ds_comunic_exec_w,1,4000);
	end if;
	
	end;
end if;

open c01;
loop
fetch c01 into
	nm_usuario_w;
exit when c01%notfound;
	begin

	if	(nm_usuario_w is not null) then
		begin
		select comunic_interna_seq.nextval
		into nr_sequencia_w
		from dual;

		insert into comunic_interna(
			dt_comunicado, ds_titulo, ds_comunicado, nm_usuario,
			dt_atualizacao, ie_geral, nm_usuario_destino, nr_sequencia,
			ie_gerencial, nr_seq_classif, dt_liberacao)
		values(
			sysdate, ds_titulo_w, ds_comunicado_w, 
			nm_usuario_p, sysdate, 'N', nm_usuario_w || ',', nr_sequencia_w, 'N',
			nr_seq_classif_w, sysdate);
	
		Insert into man_ordem_serv_envio(	nr_sequencia,
						nr_seq_ordem,
						dt_atualizacao,
						nm_usuario,
						dt_envio,
						ie_tipo_envio,
						ds_destino,
						ds_observacao)
					values(	man_ordem_serv_envio_seq.nextval,
						nr_sequencia_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						'I',
						substr(nm_usuario_w,1,255),
						'Altera��o de satisfa��o');		
		commit;

		end;
	end if;
	end;

end loop;
close c01;

end man_comunic_alteracao_satisf;
/