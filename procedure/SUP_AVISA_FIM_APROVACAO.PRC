create or replace
procedure	sup_avisa_fim_aprovacao(
			nr_solic_compra_p		number,
			nm_usuario_p		varchar2) is

nr_seq_regra_w				number(10);
ds_email_adicional_w			varchar2(2000);
cd_perfil_dispara_w			number(5);
nr_seq_aprovacao_w			number(10);
nr_seq_aprov_lista_w			varchar2(200);
vl_total_aprov_w			number(15,4);
vl_total_aprov_ww			varchar2(100);
nm_usuario_aprov_w			varchar2(15);
ie_aprov_reprov_w			varchar2(1);
nm_aprov_lista_w			varchar2(2000);
nm_solicitante_w			varchar2(255);
ds_centro_custo_w			varchar2(255);
ds_assunto_w				varchar2(255);
ds_mensagem_w				varchar2(2000);
ds_email_pessoa_solic_w			varchar2(255);
ds_lista_email_w			varchar2(2000);
ie_usuario_w				varchar2(1);
ds_email_origem_w			varchar2(255);
ds_email_comprador_w			varchar2(255);
ds_usuario_comprador_w			comprador.nm_guerra%type;
ds_usuario_origem_w			varchar2(255);
ds_aprov_reprov_w			varchar2(15);
cd_centro_custo_w			number(8);
cd_comprador_w				varchar2(10);
cd_estabelecimento_w			number(5);
ie_momento_envio_w			varchar2(1);
ds_forn_sugerido_w			varchar2(255);
ds_tipo_servico_w			varchar2(255);
dt_solicitacao_compra_w			date;
dt_liberacao_w				date;
ds_observacao_w				varchar2(4000);
ds_material_w				varchar2(255);
local_estoque_w				varchar2(255);
ds_email_remetente_w			varchar2(255);
ie_envia_w				varchar2(1);

cursor c00 is
select	nr_sequencia,
	nvl(ds_email_remetente,'X'),
	replace(ds_email_adicional,',',';'),
	cd_perfil_disparar,
	nvl(ie_momento_envio,'I')
from	regra_envio_email_compra
where	ie_tipo_mensagem = 56
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_w;

cursor	c01 is
select	nr_seq_aprovacao
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
group by	nr_seq_aprovacao;

cursor	c02 is
select	nm_usuario_aprov,
	ie_aprov_reprov
from	processo_aprov_compra
where	nr_sequencia in	(select	x.nr_seq_aprovacao
			from	solic_compra_item x
			where	x.nr_solic_compra = nr_solic_compra_p)
group by	nm_usuario_aprov,
	ie_aprov_reprov;
	
cursor c03 is	
select	cd_material||chr(45)||substr(obter_desc_material(cd_material),245) || ','
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

select	nvl(obter_se_envia_email_regra(nr_solic_compra_p, 'SC', 56, cd_estabelecimento_w),'S')
into	ie_envia_w
from	dual;

if	(ie_envia_w = 'S') then

	open C00;
	loop
	fetch C00 into	
		nr_seq_regra_w,
		ds_email_remetente_w,
		ds_email_adicional_w,
		cd_perfil_dispara_w,
		ie_momento_envio_w;
	exit when C00%notfound;
		begin		
		
		if	(nr_seq_regra_w > 0) and
			((cd_perfil_dispara_w is null) or
			((cd_perfil_dispara_w is not null) and (cd_perfil_dispara_w = obter_perfil_ativo))) then
			begin

			select	substr(obter_nome_pf(cd_pessoa_solicitante),1,255),
				substr(obter_desc_centro_custo(cd_centro_custo),1,255),
				cd_centro_custo,
				cd_comprador_resp,
				obter_nome_pf_pj(cd_pessoa_fisica,cd_fornec_sugerido),
				obter_valor_dominio(4200,ie_tipo_servico),
				dt_solicitacao_compra,
				dt_liberacao,
				ds_observacao,			
				SUBSTR(cd_local_estoque||'-'||obter_desc_local_estoque(cd_local_estoque),1,255)	       
			into	nm_solicitante_w,
				ds_centro_custo_w,
				cd_centro_custo_w,
				cd_comprador_w,
				ds_forn_sugerido_w,
				ds_tipo_servico_w,
				dt_solicitacao_compra_w,
				dt_liberacao_w,
				ds_observacao_w,
				local_estoque_w
			from	solic_compra
			where	nr_solic_compra = nr_solic_compra_p;
			
			open C03;
			loop
			fetch C03 into	
				ds_material_w;
			exit when C03%notfound;
				begin
				ds_material_w := substr(ds_material_w || ds_material_w,1,255);
				end;
			end loop;
			close C03;

			open c01;
			loop
			fetch c01 into
				nr_seq_aprovacao_w;
			exit when c01%notfound;
				begin

				nr_seq_aprov_lista_w	:= (nr_seq_aprov_lista_w || to_char(nr_seq_aprovacao_w) || ' ');

				end;
			end loop;
			close c01;

			select	nvl(sum((b.qt_material * nvl(b.vl_unit_previsto,0))),0)
			into	vl_total_aprov_w
			from	solic_compra a,
				solic_compra_item b
			where	a.nr_solic_compra = b.nr_solic_compra
			and	b.dt_reprovacao is null
			and	b.dt_autorizacao is not null
			and	a.nr_solic_compra = nr_solic_compra_p;

			vl_total_aprov_ww		:= substr(campo_mascara_virgula_casas(vl_total_aprov_w,2),1,100);

			open c02;
			loop
			fetch c02 into
				nm_usuario_aprov_w,
				ie_aprov_reprov_w;
			exit when c02%notfound;
				begin

				select	decode(ie_aprov_reprov_w,'A',WHEB_MENSAGEM_PCK.get_texto(306884),'R',WHEB_MENSAGEM_PCK.get_texto(306890))
				into	ds_aprov_reprov_w
				from	dual;

				nm_aprov_lista_w := substr(nm_aprov_lista_w || substr(obter_pessoa_fisica_usuario(nm_usuario_aprov_w,'N'),1,255) || ' (' || ds_aprov_reprov_w || ')' || chr(13),1,1800);

				end;
			end loop;
			close c02;

			select	ds_assunto,
				ds_mensagem_padrao
			into	ds_assunto_w,
				ds_mensagem_w
			from	regra_envio_email_compra
			where	nr_sequencia = nr_seq_regra_w;

			ds_assunto_w	:=	substr(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(ds_assunto_w,
							'@solicitacao', nr_solic_compra_p),
							'@solicitante',nm_solicitante_w),
							'@cd_centro_custo',cd_centro_custo_w),
							'@ds_centro_custo',ds_centro_custo_w),
							'@ds_forn_sugerido',ds_forn_sugerido_w),
							'@ds_tipo_servico',ds_tipo_servico_w),
							'@dt_solicitacao_compra',dt_solicitacao_compra_w),
							'@ds_observacao',ds_observacao_w),
							'@ds_material',ds_material_w),
							'@local_estoque',local_estoque_w),1,255);

			ds_mensagem_w	:=	substr(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(
						replace(ds_mensagem_w,
							'@solicitacao', nr_solic_compra_p),
							'@solicitante',nm_solicitante_w),
							'@cd_centro_custo',cd_centro_custo_w),
							'@ds_centro_custo',ds_centro_custo_w),
							'@vl_total_aprovacao',vl_total_aprov_ww),
							'@seq_aprovacao',nr_seq_aprov_lista_w),
							'@aprovadores_processo',nm_aprov_lista_w),
							'@ds_forn_sugerido',ds_forn_sugerido_w),
							'@ds_tipo_servico',ds_tipo_servico_w),
							'@dt_solicitacao_compra',dt_solicitacao_compra_w),
							'@dt_liberacao',dt_liberacao_w),
							'@ds_observacao',ds_observacao_w),
							'@ds_material',ds_material_w),
							'@local_estoque',local_estoque_w),1,2000);

			select	substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante),'E'),1,255)
			into	ds_lista_email_w
			from	solic_compra
			where	nr_solic_compra = nr_solic_compra_p;
			
			if	(ds_email_adicional_w is not null) then
				if	(ds_lista_email_w is not null) then
					ds_lista_email_w := ds_lista_email_w || '; ' || ds_email_adicional_w;
				else
					ds_lista_email_w := ds_email_adicional_w;
				end if;
			end if;

			select	nvl(max(ie_usuario),'U')
			into	ie_usuario_w
			from	regra_envio_email_compra
			where	nr_sequencia = nr_seq_regra_w;

			if	(ie_usuario_w = 'U') or
				((nvl(cd_comprador_w,'0') <> 0) and (ie_usuario_w = 'O')) then --Usuario
				begin

				select	ds_email,
					nm_usuario
				into	ds_email_origem_w,
					ds_usuario_origem_w
				from	usuario
				where	nm_usuario = nm_usuario_p;

				end;
			elsif	(ie_usuario_w = 'C') then --Setor compras
				begin

				select	ds_email
				into	ds_email_origem_w
				from	parametro_compras
				where	cd_estabelecimento = cd_estabelecimento_w;

				select	nvl(ds_fantasia,ds_razao_social)
				into	ds_usuario_origem_w
				from	estabelecimento_v
				where	cd_estabelecimento = cd_estabelecimento_w;

				end;
			elsif	(ie_usuario_w = 'O') then --Comprador
				begin

				select	max(ds_email),
					max(nm_guerra)
				into	ds_email_comprador_w,
					ds_usuario_comprador_w
				from	comprador
				where	cd_pessoa_fisica = cd_comprador_w
				and	cd_estabelecimento = cd_estabelecimento_w;

				ds_email_origem_w := ds_email_comprador_w;
				ds_usuario_origem_w := ds_usuario_comprador_w;

				end;
			end if;
			
			if	(ds_email_remetente_w <> 'X') then
				ds_email_origem_w	:= ds_email_remetente_w;
			end if;

			if	(ie_momento_envio_w = 'A') then
				begin

				sup_grava_envio_email(
					'SC',
					'56',
					nr_solic_compra_p,
					null,
					null,
					ds_lista_email_w,
					ds_usuario_origem_w,
					ds_email_origem_w,
					ds_assunto_w,
					ds_mensagem_w,
					cd_estabelecimento_w,
					nm_usuario_p);

				end;
			else
				begin
				enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_origem_w,ds_lista_email_w,ds_usuario_origem_w,'M');
				exception
				when others then
					/*gravar__log__tasy(91301,'Falha ao enviar e-mail compras - Evento: 56 - Seq. Regra: ' || nr_seq_regra_w,nm_usuario_p);*/
					
					gerar_hist_solic_sem_commit(
							nr_solic_compra_p,
							WHEB_MENSAGEM_PCK.get_texto(299745),
							WHEB_MENSAGEM_PCK.get_texto(306899,'NR_SEQ_REGRA_W='||nr_seq_regra_w),
							'FGE',
							nm_usuario_p);
				end;
			end if;

			end;
		end if;
		
		
		
		
		
		
		
		
		end;
	end loop;
	close C00;
end if;




end sup_avisa_fim_aprovacao;
/