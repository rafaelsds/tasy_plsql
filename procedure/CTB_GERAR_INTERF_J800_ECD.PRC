create or replace
procedure ctb_gerar_interf_J800_ecd(
			nr_seq_controle_p		number,
			nm_usuario_p			Varchar2,
			cd_estabelecimento_p		number,
			dt_inicio_p			date,
			dt_fim_p			date,
			cd_empresa_p			number,
			qt_linha_p			in out	number,
			nr_sequencia_p			in out	number) is 

			

	
ds_longo_w			long;
cd_instit_resp_cadastro_w	varchar2(4);
cd_instituicao_w		varchar2(21);
ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			varchar2(8000);
ie_gerar_w			varchar2(1)	:= 'S';
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
nr_sequencia_w			number(10);
sep_w				varchar2(1) := '|';
tp_registro_w			varchar2(15) := 'J800';
ds_documento_w			clob;
ie_ult_documento_w		boolean;
cd_versao_w			ctb_regra_sped.cd_versao%type;


cursor c01 is
select	a.nr_sequencia
from	ctb_demo_externo a
where  	a.cd_empresa = cd_empresa_p
and	dt_referencia between dt_inicio_p and dt_fim_p;

begin

select 	nvl(a.cd_versao, '9.0')
into	cd_versao_w
from	ctb_regra_sped a,
	ctb_sped_controle b
where	a.nr_sequencia	= b.nr_seq_regra_sped
and	b.nr_sequencia	= nr_seq_controle_p;


open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin	
	ie_gerar_w	:= 'S';
	ie_ult_documento_w:= false;
	
	select	ds_documento
	into	ds_documento_w
	from	ctb_demo_externo a
	where	a.nr_sequencia	= nr_sequencia_w;
		
	begin
	select	a.ds_demonstrativo
	into	ds_longo_w
	from	ctb_demo_externo a
	where  	a.nr_sequencia	= nr_sequencia_w;
	exception when others then 
		ie_gerar_w	:= 'N';
		
		if	(ds_documento_w is not null) then
			begin
			
			ie_ult_documento_w:= true;
			ie_gerar_w	:= 'S';
			
			end;
		end if;
	end;

		
	if	(ie_gerar_w = 'S')
	and 	(cd_versao_w is null)	then
		
		if	(ie_ult_documento_w) then
			begin
			ds_linha_w	:= substr(	sep_w || tp_registro_w 		|| 
							sep_w || ds_documento_w		|| 
							sep_w || 'J800FIM' 		|| sep_w,1,8000);
			end;
		else	
			begin
			ds_linha_w	:= substr(	sep_w || tp_registro_w 		|| 
							sep_w || ds_longo_w 		|| 
							sep_w || 'J800FIM' 		|| sep_w,1,8000);
			end;
		
		end if;

		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;	
	
		insert into ctb_sped_registro(
			nr_sequencia,                   
			ds_arquivo,                     
			dt_atualizacao,                 
			nm_usuario,                     
			dt_atualizacao_nrec,            
			nm_usuario_nrec,                
			nr_seq_controle_sped,           
			ds_arquivo_compl,               
			cd_registro,
			nr_linha,
			ds_longo)
		values(	nr_seq_registro_w,
			ds_arquivo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_controle_p,
			ds_compl_arquivo_w,
			tp_registro_w,
			nr_linha_w,
			ds_longo_w);
	end if;
	
	end;
end loop;
close C01;

commit;
qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;
end ctb_gerar_interf_J800_ecd;
/
