create or replace
procedure Inserir_Med_Pre_Natal_fator(	nr_seq_pre_natal_p	number,
					nr_seq_fator_p		number,
					nm_usuario_p		Varchar2,
					nr_seq_parto_p		number default null) is 

begin

if	(nr_seq_parto_p is not null) then
	insert into MED_PAC_PRE_NATAL_FATOR	(	nr_sequencia,
							nr_seq_parto,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_fator)
				values		(	MED_PAC_PRE_NATAL_RISCO_seq.nextval,	
							nr_seq_parto_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_fator_p);
else
	insert into MED_PAC_PRE_NATAL_FATOR	(	nr_sequencia,
							nr_seq_pre_natal,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_fator)
				values		(	MED_PAC_PRE_NATAL_RISCO_seq.nextval,	
							nr_seq_pre_natal_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_fator_p);
end if;


commit;
 
end Inserir_Med_Pre_Natal_fator;
/