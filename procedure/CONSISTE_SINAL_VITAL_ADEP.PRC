CREATE OR REPLACE
PROCEDURE consiste_sinal_vital_adep	(nr_sequencia_p		number,
					vl_item_p		number,					
					ds_retorno_p	out	varchar2,
					ie_retorno_p	out	varchar2,					
					nr_atendimento_p	number default null,
					cd_escala_dor_p		varchar2 default null,
					ie_rn_p			varchar2 default null) is

qt_min_aviso_w		number(15,4);
qt_max_aviso_w		number(15,4);
qt_minimo_per_w		number(15,4);
vl_maximo_per_w		number(15,4);
ds_sinal_vital_w	varchar2(60);
ie_sinal_w		varchar2(1);
qt_idade_w		number(15,4);
qt_idade_dia_w		number(15,4);
vl_item_w		number(15,4);
cd_setor_Atendimento_w	number(10):=0;
ds_mensagem_bloqueio_w	varchar2(255);
ds_mensagem_alerta_w	varchar2(255);
cd_pessoa_fisica_w	varchar2(10);
ds_atributo_w		varchar2(30) := '';
nr_seq_item_w		number(5);
cd_perfil_w		varchar2(10);
ie_sexo_w				varchar2(1);

cursor c01 is
select	b.qt_min_aviso,
	b.qt_max_aviso,
	b.qt_minimo,
	b.vl_maximo,
	nvl(substr(obter_desc_expressao(a.CD_EXP_INFORMACAO),1,254),a.ds_sinal_vital),
	b.ds_mensagem_bloqueio,
	b.ds_mensagem_alerta
from	sinal_vital_regra b,
	sinal_vital a
where	a.nr_sequencia	= nr_seq_item_w
and	a.nr_sequencia	= b.nr_seq_sinal
and	((qt_idade_w  between b.qt_idade_min and b.qt_idade_max) or
	(qt_idade_dia_w between b.qt_idade_min_dias and b.qt_idade_max_dias))
and	nvl(b.cd_setor_Atendimento,cd_setor_Atendimento_w)	= cd_setor_Atendimento_w
and	b.cd_escala_dor is null
and	nvl(b.cd_estabelecimento,Wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
and	nvl(b.cd_perfil, cd_perfil_w) = cd_perfil_w
and	nvl(b.ie_sexo,ie_sexo_w) = ie_sexo_w
order by nvl(b.cd_setor_atendimento,0), nvl(b.cd_escala_dor,'0') desc, nvl(b.cd_estabelecimento,0);

BEGIN

cd_perfil_w	:= nvl(obter_perfil_ativo,0);

vl_item_w	:= nvl(vl_item_p,0);

if	(nvl(nr_atendimento_p,0)	> 0) then
	cd_setor_Atendimento_w	:= nvl(obter_setor_atendimento(nr_atendimento_p),0);
end if;

select 	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select	max(ie_sexo)
into	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

if	(nvl(ie_rn_p,'N') = 'S') then

	qt_idade_w	:= null;
	qt_idade_dia_w	:= 0;
	
else
	select	obter_idade(dt_nascimento,sysdate,'A'),
		obter_idade(dt_nascimento,sysdate,'DIA')
	into	qt_idade_w,
		qt_idade_dia_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

end if;

select	max(nm_atributo)
into	ds_atributo_w 
from 	adep_sv 
where   nr_sequencia   = nr_sequencia_p;

if 	(ds_atributo_w = 'QT_PA_SISTOLICA') then
	nr_seq_item_w := 1;
elsif 	(ds_atributo_w = 'QT_PA_DIASTOLICA') then
	nr_seq_item_w := 2;
elsif 	(ds_atributo_w = 'QT_PAM') then
	nr_seq_item_w := 8;
elsif 	(ds_atributo_w = 'QT_FREQ_CARDIACA') then
	nr_seq_item_w := 9;
elsif 	(ds_atributo_w = 'QT_FREQ_RESP') then
	nr_seq_item_w := 3;
elsif 	(ds_atributo_w = 'QT_TEMP') then
	nr_seq_item_w := 4;
elsif 	(ds_atributo_w = 'QT_ESCALA_DOR') then
	nr_seq_item_w := 10;
elsif 	(ds_atributo_w = 'QT_PESO') then
	nr_seq_item_w := 12;
elsif 	(ds_atributo_w = 'QT_ALTURA_CM') then
	nr_seq_item_w := 13;
elsif 	(ds_atributo_w = 'QT_PESO') then
	nr_seq_item_w := 12;
elsif 	(ds_atributo_w = 'QT_IMC') then
	nr_seq_item_w := 14;
elsif 	(ds_atributo_w = 'QT_SUPERF_CORPORIA') then
	nr_seq_item_w := 15;
elsif 	(ds_atributo_w = 'QT_GLICEMIA_CAPILAR') then
	nr_seq_item_w := 7;
elsif 	(ds_atributo_w = 'QT_SATURACAO_O2') then
	nr_seq_item_w := 6;
elsif 	(ds_atributo_w = 'QT_PVC_H2O') then
	nr_seq_item_w := 16;
elsif 	(ds_atributo_w = 'QT_PVC') then
	nr_seq_item_w := 17;
elsif 	(ds_atributo_w = 'QT_PAE') then
	nr_seq_item_w := 18;
elsif 	(ds_atributo_w = 'QT_PRESSAO_INTRA_CRANIO') then
	nr_seq_item_w := 19;
elsif 	(ds_atributo_w = 'QT_PRESSAO_INTRA_ABD') then
	nr_seq_item_w := 20;
elsif 	(ds_atributo_w = 'QT_BCF') then
	nr_seq_item_w := 11;
elsif 	(ds_atributo_w = 'QT_TEMP_INCUBADORA') then
	nr_seq_item_w := 21;
elsif 	(ds_atributo_w = 'QT_PERIMETRO_CEFALICO') then
	nr_seq_item_w := 22;
end if;

open C01;
loop
fetch C01 into	
	qt_min_aviso_w,
	qt_max_aviso_w,
	qt_minimo_per_w,
	vl_maximo_per_w,
	ds_sinal_vital_w,
	ds_mensagem_bloqueio_w,
	ds_mensagem_alerta_w;
exit when C01%notfound;
end loop;
close C01;

if	((nr_seq_item_w is not null) and
	(qt_min_aviso_w is null)    and
	 (qt_max_aviso_w is null)    and
	 (qt_minimo_per_w is null)   and
	 (vl_maximo_per_w is null)   and
	 (ds_sinal_vital_w is null)) then
	begin
	select	nvl(max(a.qt_min_aviso),0),
		nvl(max(a.qt_max_aviso),0),
		nvl(max(a.qt_minimo),0),
		nvl(max(a.vl_maximo),0),
		max(nvl(substr(obter_desc_expressao(a.CD_EXP_INFORMACAO),1,254),a.ds_sinal_vital))
	into	qt_min_aviso_w,
		qt_max_aviso_w,
		qt_minimo_per_w,
		vl_maximo_per_w,
		ds_sinal_vital_w
	from	sinal_vital a
	where	a.nr_sequencia	= nr_seq_item_w;
	end;
end if;
	
if	((qt_min_aviso_w = 0) and
	 (qt_max_aviso_w = 0) and
	 (qt_minimo_per_w = 0) and
	 (vl_maximo_per_w = 0)) or
	(vl_item_w between qt_min_aviso_w and qt_max_aviso_w) then
	ie_sinal_w	:= 'N';
elsif	((qt_minimo_per_w > 0) or (vl_maximo_per_w > 0)) and
	((qt_min_aviso_w > 0) or (qt_max_aviso_w > 0)) and
	(not (vl_item_w between qt_minimo_per_w and vl_maximo_per_w)) then
	ie_sinal_w	:= 'E';
elsif	((qt_minimo_per_w > 0) or (vl_maximo_per_w > 0)) and
	((qt_min_aviso_w > 0) or (qt_max_aviso_w > 0)) and
	(not (vl_item_w between qt_min_aviso_w and qt_max_aviso_w)) then
	ie_sinal_w	:= 'A';
elsif	(qt_minimo_per_w = 0) and
	(vl_maximo_per_w = 0) and
	((qt_min_aviso_w > 0) or (qt_max_aviso_w > 0)) and
	(not (vl_item_w between qt_min_aviso_w and qt_max_aviso_w)) then	
	ie_sinal_w	:= 'A';
elsif	(qt_min_aviso_w = 0) and
	(qt_max_aviso_w = 0) and
	((qt_minimo_per_w > 0) or (vl_maximo_per_w > 0)) and
	(not (vl_item_w between qt_minimo_per_w and vl_maximo_per_w)) then
	ie_sinal_w	:= 'E';
elsif	(not (vl_item_w between qt_minimo_per_w and vl_maximo_per_w)) then
	ie_sinal_w	:= 'E';
else
	ie_sinal_w	:= 'N';
end if;

if	(ie_sinal_w	= 'E') then
	if	(ds_mensagem_bloqueio_w is not null) then
		ds_retorno_p	:= ds_mensagem_bloqueio_w;
	else
		/*ds_retorno_p	:= 'Valor inválido!'||chr(13)||chr(10)||
				'Neste estabelecimento permite-se o registro deste parâmetro conforme a seguir:'||chr(13)||chr(10)||
				'Valores possíveis entre '||to_char(qt_minimo_per_w) || ' e ' || to_char(vl_maximo_per_w)||chr(13)||chr(10)||
				'Parâmetro: '||ds_sinal_vital_w || '  Atualizar na Administração do sistema/Parametros/Sinais Vitais';
		*/
		
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(300525, 'QT_MIN='||to_char(qt_minimo_per_w)||';QT_MAX='||to_char(vl_maximo_per_w)||
								';DS_SINAL='||ds_sinal_vital_w);
		
	end if;
elsif	(ie_sinal_w	= 'A') then
	if	(ds_mensagem_alerta_w is not null) then
		ds_retorno_p	:= ds_mensagem_alerta_w;
	else
		/*
		ds_retorno_p	:= 'Valor fora da faixa usual!'||chr(13)||chr(10)||
				'Neste estabelecimento considera-se usual o registro deste parâmetro conforme a seguir:'||chr(13)||chr(10)||
				'Valores usuais entre '||to_char(qt_min_aviso_w) || ' e ' || to_char(qt_max_aviso_w)||chr(13)||chr(10)||
				'Parâmetro: '||ds_sinal_vital_w  || '  Atualizar na Administração do sistema/Parametros/Sinais Vitais';
		*/
		
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(300535, 'QT_MIN='||to_char(qt_min_aviso_w)||';QT_MAX='||to_char(qt_max_aviso_w)||
								';DS_SINAL='||ds_sinal_vital_w);
		
	end if;
end if;

ie_retorno_p	:= ie_sinal_w;

END consiste_sinal_vital_adep;
/