create or replace
procedure adep_obter_horarios_interv (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_so_proc_setor_usuario_p	varchar2,
		ie_so_interv_setor_usuario_p	varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is
					
dt_horario_w	date;
					
cursor c01 is
select	c.dt_horario
from	pe_prescr_proc x,
	pe_prescr_proc_hor c,
	pe_prescricao a
where	x.nr_seq_prescr = c.nr_seq_pe_prescr
and	x.nr_sequencia = c.nr_seq_pe_proc
and	x.nr_seq_prescr = a.nr_sequencia
and	c.nr_seq_pe_prescr = a.nr_sequencia
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	a.dt_liberacao is not null
and	nvl(x.ie_adep,'N') in ('S','M') 
and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))
and	obter_se_exibir_interv_adep(ie_so_proc_setor_usuario_p, ie_so_interv_setor_usuario_p, x.nr_seq_proc, cd_perfil_p, cd_setor_usuario_p, nm_usuario_p) = 'S'
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_situacao,'A') <> 'I'
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	((nvl(c.ie_horario_especial,'N') = 'N') or (c.dt_fim_horario is not null))
and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))
and	((ie_regra_inclusao_p = 'S') or
	 ((ie_regra_inclusao_p = 'R') and (adep_obter_regra_inclusao(	'SAE', 
																	cd_estabelecimento_p, 
																	cd_setor_usuario_p, 
																	cd_perfil_p, 
																	null, 
																	null, 
																	null, 
																	null,
																	null,
																	null,
																	null,	-- nr_prescricao_p. Passei nulo porque criaram o param na adep_obter_regra_inclusao como default null, e n�o haviam passado nada
																	null) = 'S'))) -- nr_seq_exame_p
and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
group by
	c.dt_horario;
	
begin
open c01;
loop
fetch c01 into dt_horario_w;
exit when c01%notfound;
	begin
	insert into w_adep_horarios_t (
		nm_usuario,
		dt_horario)
	values (
		nm_usuario_p,
		dt_horario_w);
	end;
end loop;
close c01;
end adep_obter_horarios_interv;
/
