CREATE OR REPLACE PROCEDURE
IMP_EXTRATO_ITAU_SEPARADOR_4(	nr_seq_extrato_p	number) is

/* Dados do extrato */
vl_extrato_anterior_w	number(15,2);
vl_total_extrato_w	number(15,2);
vl_extrato_atual_w	number(15,2);
nr_seq_conta_w		number(10);
dt_inicial_w		date;
dt_final_w		date;

/* Registro detalhe */
dt_lancamento_w		date;
vl_lancamento_w		number(15,2);
ie_deb_cred_w		varchar2(1);
ds_historico_w		varchar2(255);
nr_documento_w		varchar2(255);

cursor	c01 is
Select	to_date(substr(obter_valor_campo_separador(a.ds_conteudo,1,';'),1,255),'dd/mm/yyyy') dt_lancto,
	rtrim(substr(obter_valor_campo_separador(a.ds_conteudo,2,';'),1,255)) ds_historico,
	substr(obter_valor_campo_separador(a.ds_conteudo,3,';'),1,255) nr_documento,
	somente_numero(substr(obter_valor_campo_separador(a.ds_conteudo,4,';'),1,255)) / 100 vl_lancto
from	w_interf_concil a
where	a.nr_seq_conta		= nr_seq_conta_W;

begin

select	max(a.nr_seq_conta)
into	nr_seq_conta_w
from	banco_extrato a
where	a.nr_sequencia	= nr_seq_extrato_p;

open	c01;
loop
fetch	c01 into
	dt_lancamento_w,
	ds_historico_w,
	nr_documento_w,
	vl_lancamento_w;
exit	when c01%notfound;

	if	(vl_lancamento_w	< 0) then
		ie_deb_cred_w	:= 'D';
		vl_lancamento_w	:= vl_lancamento_w * -1;
	else
		ie_deb_cred_w	:= 'C';
	end if;

	insert	into banco_extrato_lanc
		(ds_historico,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_movimento,
		ie_conciliacao,
		ie_deb_cred,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_extrato,
		nr_sequencia,
		vl_lancamento,
		nr_documento)
	values	(ds_historico_w,
		sysdate,
		sysdate,
		dt_lancamento_w,
		'N',
		ie_deb_cred_w,
		'Tasy',
		'Tasy',
		nr_seq_extrato_p,
		banco_extrato_lanc_seq.nextval,
		vl_lancamento_w,
		nr_documento_w);

end	loop;
close	c01;

select	nvl(max(a.vl_saldo_final),0)
into	vl_extrato_anterior_w
from	banco_extrato a
where	a.nr_sequencia	=
	(select	max(x.nr_sequencia)
	from	banco_extrato x
	where	x.nr_sequencia	<> nr_seq_extrato_p
	and	x.nr_seq_conta	= nr_seq_conta_w);

select	nvl(sum(decode(a.ie_deb_cred,'D',a.vl_lancamento * -1,a.vl_lancamento)),0)
into	vl_total_extrato_w
from	banco_extrato_lanc a
where	a.nr_seq_extrato	= nr_seq_extrato_p;

vl_extrato_atual_w	:= vl_extrato_anterior_w + vl_total_extrato_w;

select	max(a.dt_movimento),
	min(a.dt_movimento)
into	dt_final_w,
	dt_inicial_w
from	banco_extrato_lanc a
where	a.nr_seq_extrato	= nr_seq_extrato_p;

update	banco_extrato
set	dt_inicio		= dt_inicial_w,
	dt_final		= dt_final_w,
	vl_saldo_inicial	= vl_extrato_anterior_w,
	vl_saldo_final		= vl_extrato_atual_w
where	nr_sequencia		= nr_seq_extrato_p;

delete	from w_interf_concil
where	nr_seq_conta	= nr_seq_conta_W;

commit;

end IMP_EXTRATO_ITAU_SEPARADOR_4;
/