create or replace
procedure ctb_excluir_lancamento(
			nr_seq_lancamento_p		number,
			nr_lote_contabil_p		number) is 

begin

update	w_ctb_lancamento
set		ie_status_origem 	= 'EX'
where	nr_lote_contabil 	=  nr_lote_contabil_p
and		nr_sequencia 		=  nr_seq_lancamento_p;


commit;

end ctb_excluir_lancamento;
/