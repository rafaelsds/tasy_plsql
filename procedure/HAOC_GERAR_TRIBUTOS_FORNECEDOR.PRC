create or replace 
procedure haoc_gerar_tributos_fornecedor (	
				nr_sequencia_p	number,
				nm_usuario_p	varchar2) is

cd_tributo_w			number(10);
pr_imposto_w			number(15,4);
vl_liquido_w			number(15,2);
vl_inss_w			number(15,2);
vl_tributo_w			number(15,2);
cd_estabelecimento_w		number(4);
cd_cond_pagto_w			number(4);
cd_cgc_w			varchar2(20);
cd_cgc_emitente_w		varchar2(20);
cd_pessoa_fisica_w		varchar2(10);
cd_beneficiario_w		varchar2(20);
ie_acumulativo_w		varchar2(1);
cd_conta_financ_w		number(10,0);
nr_seq_trans_reg_w		number(10,0);
nr_seq_trans_baixa_w		number(10,0);
cd_natureza_operacao_w		number(10,0);
vl_minimo_base_calculo_w	number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_tributo_a_reter_w		number(15,2);
vl_base_a_reter_w		number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_base_adic_w			number(15,2);
vl_liquido_original_w		number(15,2);
vl_trib_anterior_w		number(15,2);
vl_base_venc_w			number(15,2);
vl_total_nota_w			number(15,2);
vl_vencimento_w			number(15,2);
dt_emissao_w			date;
vl_teto_base_w			number(15,2);
vl_total_base_w			number(15,2);
ie_tipo_tributo_w		varchar2(15);
ie_irpf_w			varchar2(1);
ie_apuracao_piso_w		varchar2(3);
ie_ordem_w			varchar2(1);
vl_reducao_w			number(15,2);
vl_desc_dependente_w		number(15,2);
qt_dependente_w			number(2,0);
cd_darf_w			varchar2(20);
ie_corpo_item_w			varchar2(5);
ie_soma_diminui_w		varchar2(5);
cont_w				number(7,0);
cd_variacao_w			varchar2(2);
ie_periodicidade_w		varchar2(1);
nr_contrato_w			number(10,0);
cd_condicao_pagamento_w		number(10,0);
ie_forma_pagamento_w		number(02,0);
ie_corpo_item_ant_w		varchar2(20);
cd_cnpj_raiz_w			varchar2(20);
ie_cnpj_w			varchar2(20);
cd_cnpj_emitente_raiz_w		varchar2(20);
nr_ccm_w			number(10)	:= null;
ie_tipo_tributacao_w		varchar2(255);
cd_tipo_servico_w		varchar2(100);
ds_irrelevante_w		varchar2(4000);
nr_seq_regra_w			number(15);
cd_grupo_material_regra_w	number(15);
cd_subgrupo_material_regra_w	number(15);
cd_classe_material_regra_w	number(15);
cd_material_regra_w		number(15);
vl_item_nf_w			number(18,2);
cd_estab_regra_w		number(10,0);
cd_empresa_regra_w		number(10,0);
ie_restringe_estab_w		varchar2(1);
nr_seq_classe_w			number(10);
cd_tipo_baixa_neg_w		number(5);
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;

cursor c01 is
select	decode(a.ie_tipo_tributo,'IR','Z','A') ie_ordem,
	a.cd_tributo,
	a.ie_tipo_tributo,
	a.ie_apuracao_piso,
	a.ie_corpo_item,
	a.ie_soma_diminui,
	a.ie_cnpj,
	a.ie_restringe_estab
from	tributo a
where	cd_tributo	= 12
and	not exists (select 1 from nota_fiscal_trib x where x.nr_sequencia = nr_sequencia_p and a.cd_tributo = x.cd_tributo);

cursor c02 is
select	0 ie_ordem,
	a.cd_estabelecimento,
	a.cd_cgc,
	a.cd_cgc_emitente,
	a.cd_pessoa_fisica,
	a.dt_emissao dt_vencimento,
	0
from	nota_fiscal a
where	a.nr_sequencia	= nr_sequencia_p;

begin
delete	w_haoc_nota_fiscal_trib
where	nm_usuario = nm_usuario_p;
commit;
select	cd_estabelecimento,
	cd_cgc,
	cd_cgc_emitente,
	nvl(cd_pessoa_fisica,null),
	dt_emissao,
	cd_condicao_pagamento,
	obter_cnpj_raiz(cd_cgc),
	obter_cnpj_raiz(cd_cgc_emitente),
	cd_natureza_operacao,
	cd_tipo_servico
into	cd_estabelecimento_w,
	cd_cgc_w,
	cd_cgc_emitente_w,
	cd_pessoa_fisica_w,
	dt_emissao_w,
	cd_condicao_pagamento_w,
	cd_cnpj_raiz_w,
	cd_cnpj_emitente_raiz_w,
	cd_natureza_operacao_w,
	cd_tipo_servico_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

if	(cd_pessoa_fisica_w is not null) then
	select	max(nr_ccm)
	into	nr_ccm_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
end if;

select	ie_forma_pagamento 
into	ie_forma_pagamento_w
from	condicao_pagamento 
where	cd_condicao_pagamento = cd_condicao_pagamento_w;

select	nvl(sum(vl_liquido),0)
into	vl_liquido_original_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p;

select	max(nvl(nr_contrato,0))
into	nr_contrato_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p;

select	max(ie_tipo_tributacao) 
into	ie_tipo_tributacao_w
from	pessoa_juridica
where	cd_cgc	= cd_cgc_w;

open c01;
loop
fetch c01 into
	ie_ordem_w,
	cd_tributo_w,
	ie_tipo_tributo_w,
	ie_apuracao_piso_w,
	ie_corpo_item_w,
	ie_soma_diminui_w,
	ie_cnpj_w,
	ie_restringe_estab_w;
exit when c01%notfound;
	ie_corpo_item_ant_w	:= ie_corpo_item_w;
	open c02;
	loop
	fetch c02 into
		ie_ordem_w,
		cd_estabelecimento_w,
		cd_cgc_w,
		cd_cgc_emitente_w,
		cd_pessoa_fisica_w,
		dt_emissao_w,
		vl_base_venc_w;
	exit when c02%notfound;

		vl_liquido_w				:= vl_liquido_original_w;
		if	(ie_corpo_item_w = 'V') and
			(ie_forma_pagamento_w <> 10) then	/*conforme vencimentos*/
			vl_liquido_w			:= vl_base_venc_w;
		end if;
	
		pr_imposto_w			:= 0;

		obter_dados_trib_tit_pagar(cd_tributo_w,
					cd_estabelecimento_w,
					cd_cgc_w,
					cd_pessoa_fisica_w,
					cd_beneficiario_w,
					pr_imposto_w,
					cd_cond_pagto_w,
					cd_conta_financ_w,
					nr_seq_trans_reg_w,
					nr_seq_trans_baixa_w,
					vl_minimo_base_calculo_w,
					vl_minimo_tributo_w,
					ie_acumulativo_w,
					vl_teto_base_w,
					vl_desc_dependente_w,
					cd_darf_w,
					dt_emissao_w,
					cd_variacao_w,
					ie_periodicidade_w,
					null,
					cd_natureza_operacao_w,
					cd_tipo_servico_w,
					null,
					null,
					null,
					nr_seq_regra_w,
					null,
					0,
					nr_seq_classe_w,
					cd_tipo_baixa_neg_w,
					vl_liquido_w,
					'N',
					null,
					null,
					null,
					null);

		select	max(a.cd_grupo_material),
			max(a.cd_subgrupo_material),
			max(a.cd_classe_material),
			max(a.cd_material),
			max(a.cd_estabelecimento),
			max(a.cd_empresa)
		into	cd_grupo_material_regra_w,
			cd_subgrupo_material_regra_w,
			cd_classe_material_regra_w,
			cd_material_regra_w,
			cd_estab_regra_w,
			cd_empresa_regra_w
		from	tributo_conta_pagar a
		where	a.nr_sequencia	= nr_seq_regra_w;
		
		if	((cd_grupo_material_regra_w 	is not null) or
			(cd_subgrupo_material_regra_w	is not null) or
			(cd_classe_material_regra_w	is not null) or
			(cd_material_regra_w		is not null)) then

			select	sum(a.vl_total_item_nf)
			into	vl_liquido_w
			from	nota_fiscal_item a
			where	a.nr_sequencia		= nr_sequencia_p
			and	obter_se_estrutura_mat(cd_grupo_material_regra_w, cd_subgrupo_material_regra_w, cd_classe_material_regra_w, cd_material_regra_w, a.cd_material, 'S')	= 'S';

		end if;

		vl_tributo_w	:= (vl_liquido_w * pr_imposto_w) / 100;
		
		select	a.vl_mercadoria
		into	vl_liquido_w
		from	nota_fiscal a
		where	a.nr_sequencia		= nr_sequencia_p;
		
		if	(pr_imposto_w > 0) then

			select	/*+ use_concat */ nvl(sum(vl_soma_trib_nao_retido),0),
				nvl(sum(vl_soma_base_nao_retido),0),
				nvl(sum(vl_soma_trib_adic),0),
				nvl(sum(vl_soma_base_adic),0),
				nvl(sum(vl_tributo),0),
				nvl(sum(vl_total_base),0),
				nvl(sum(vl_reducao),0)
			into	vl_soma_trib_nao_retido_w,
				vl_soma_base_nao_retido_w,
				vl_soma_trib_adic_w,
				vl_soma_base_adic_w,
				vl_trib_anterior_w,
				vl_total_base_w,
				vl_reducao_w
			from	valores_tributo_v a
			where	a.cd_tributo			= cd_tributo_w
			and	trunc(a.dt_tributo, 'month')	= trunc(dt_emissao_w, 'month')
			and	(
				  (
				    ((nvl(a.cd_cgc_emitente, 'X') = nvl(cd_cgc_emitente_w, 'X')) and (a.ie_origem_valores = 'NFE')) or 
				    (
				      (ie_cnpj_w = 'Empresa') and 
				      (cd_cnpj_emitente_raiz = cd_cnpj_emitente_raiz_w)
				    )
				  ) or 
				  (
				    (a.cd_cgc_emitente is null) and (a.ie_origem_valores <> 'NFE')
				  )
				)
			and	(
				  (cd_pessoa_fisica_w is not null and a.cd_pessoa_fisica = cd_pessoa_fisica_w) or
				  (
				    (cd_pessoa_fisica_w is null) and 
				    (cd_cgc_w 		is not null) and 
				    (a.cd_pessoa_fisica is null) and 
				    (
				       (a.cd_cgc 		= cd_cgc_w) or 
				       ((ie_cnpj_w = 'Empresa') and (a.cd_cnpj_raiz = cd_cnpj_raiz_w))
				    )
				  )
				)
			and	(a.ie_origem_valores		= 'NFE' or ie_apuracao_piso_w = 'S')
			and	((ie_restringe_estab_w		= 'N') or
				 (a.cd_estabelecimento		= cd_estabelecimento_w) or
				 (a.cd_estab_financeiro		= cd_estabelecimento_w) or
				 (cd_estab_regra_w is null and a.cd_empresa = cd_empresa_regra_w))
			and	(ie_apuracao_piso_w		= 'N' or
				ie_apuracao_piso_w		= ie_base_calculo)
			and	ie_baixa_titulo			= 'N';

			ie_irpf_w			:= 'N';
			if	(ie_tipo_tributo_w = 'IR') and 
				(cd_pessoa_fisica_w is not null) then

				select	nvl(sum(a.vl_tributo),0)
				into	vl_inss_w
				from	tributo b,
					nota_fiscal_trib a
				where	a.cd_tributo		= b.cd_tributo
				and	a.nr_sequencia		= nr_sequencia_p
				and	b.ie_tipo_tributo	= 'INSS';

				select	nvl(qt_dependente,0)
				into	qt_dependente_w
				from	pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

				ie_irpf_w			:= 'S';
				vl_liquido_w			:= vl_liquido_w - vl_inss_w;

			end if;
			
			obter_valores_tributo(	ie_acumulativo_w,
						pr_imposto_w,
						vl_minimo_base_calculo_w,
						vl_minimo_tributo_w,
						vl_soma_trib_nao_retido_w,
						vl_soma_trib_adic_w,
						vl_soma_base_nao_retido_w,
						vl_soma_base_adic_w,
						vl_liquido_w,
						vl_tributo_w,
						vl_trib_nao_retido_w,
						vl_trib_adic_w,
						vl_base_nao_retido_w,
						vl_base_adic_w,
						vl_teto_base_w,
						vl_trib_anterior_w,
						ie_irpf_w,
						vl_total_base_w,
						vl_reducao_w,
						vl_desc_dependente_w,
						qt_dependente_w,
						0,
						null,
						null,
						obter_outras_reducoes_irpf(cd_pessoa_fisica_w, cd_estabelecimento_w, dt_emissao_w),
						sysdate,
						nr_seq_regra_irpf_w);

			if	(vl_tributo_w > vl_liquido_original_w) then
				vl_tributo_w	:= vl_liquido_original_w;
			end if;

			if	(vl_tributo_w < 0) then
				vl_tributo_w	:= 0;
			end if;
			
			if	(cd_tributo_w = 12) then
				
				insert	into w_haoc_nota_fiscal_trib
					(nr_sequencia,
					cd_tributo,
					vl_tributo,
					dt_atualizacao,
					nm_usuario,
					vl_base_calculo,
					tx_tributo,
					vl_reducao_base,
					vl_trib_nao_retido,
					vl_base_nao_retido,
					vl_trib_adic,           
					vl_base_adic,
					vl_reducao,
					cd_darf,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_periodicidade,
					cd_variacao,
					ie_origem_trib)
				values	(nr_sequencia_p,
					cd_tributo_w,
					vl_tributo_w,
					sysdate,
					nm_usuario_p,
					vl_liquido_w,
					pr_imposto_w,
					vl_desc_dependente_w,
					vl_trib_nao_retido_w,
					vl_base_nao_retido_w,
					vl_trib_adic_w,
					vl_base_adic_w,
					vl_reducao_w,
					cd_darf_w,
					sysdate,
					nm_usuario_p,
					ie_periodicidade_w,
					cd_variacao_w,
					'N');

			end if;
		end if;

	end loop;
	close c02;

end loop;
close c01;

commit;

end haoc_gerar_tributos_fornecedor;
/