create or replace
procedure sup_altera_material_nota(
		cd_material_p		number,
		nr_sequencia_p		number,
		nr_item_nf_p		number,
		nm_usuario_p		Varchar2) is 

begin

if	((cd_material_p is not null) and (nr_sequencia_p is not null) and (nr_item_nf_p is not null)) then
	begin

update 	nota_fiscal_item 
set    	cd_material = cd_material_p,
	nm_usuario = nm_usuario_p
where  	nr_sequencia = nr_sequencia_p
and    	nr_item_nf = nr_item_nf_p;
	
	end;
end if;

commit;

end sup_altera_material_nota;
/