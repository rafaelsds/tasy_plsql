create or replace
procedure	cm_atualiza_excluir_item_cont(
			nr_seq_item_cont_p	number,
			qt_item_p		number,
			nr_seq_motivo_exclusao_p	number,
			nm_usuario_p		varchar2) is

qt_item_atual_w			number(9,2);
qt_item_restante_w		number(9,2);
nr_seq_item_novo_w		number(10);
nr_seq_conjunto_w		number(10);
nr_seq_item_w			number(10);
ds_observacao_w			varchar2(255);
ie_status_w			varchar2(15);

begin

select	qt_item
into	qt_item_atual_w
from	cm_item_cont
where	nr_sequencia = nr_seq_item_cont_p;

if	(qt_item_atual_w = qt_item_p) then
	begin

	update	cm_item_cont
	set	ie_excluido = 'S',
		nr_seq_mot_exclusao = nr_seq_motivo_exclusao_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_item_cont_p;

	end;
elsif	(qt_item_atual_w > qt_item_p) and
	(qt_item_p > 0) then
	begin

	select	(qt_item_atual_w - qt_item_p)
	into	qt_item_restante_w
	from	dual;

	select	cm_item_cont_seq.nextval
	into	nr_seq_item_novo_w
	from	dual;

	select	nr_seq_conjunto,
		nr_seq_item,
		ds_observacao,
		ie_status
	into	nr_seq_conjunto_w,
		nr_seq_item_w,
		ds_observacao_w,
		ie_status_w
	from	cm_item_cont
	where	nr_sequencia = nr_seq_item_cont_p;

	insert into cm_item_cont(
		nr_sequencia,
		nr_seq_conjunto,
		nr_seq_item,
		qt_item,
		ie_excluido,
		ie_status,
		dt_atualizacao,
		nm_usuario,
		ds_observacao,
		nr_seq_mot_exclusao) values (
			nr_seq_item_novo_w,
			nr_seq_conjunto_w,
			nr_seq_item_w,
			qt_item_p,
			'S',
			ie_status_w,
			sysdate,
			nm_usuario_p,
			ds_observacao_w,
			nr_seq_motivo_exclusao_p);


	update	cm_item_cont
	set	qt_item = qt_item_restante_w,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_item_cont_p;		

	end;
end if;

commit;

end cm_atualiza_excluir_item_cont;
/