create or replace procedure ctb_gerar_interf_k300_ecd(
					nr_seq_controle_p	number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_fim_p		date,
					cd_empresa_p		number,
					qt_linha_p		in out number,
					nr_sequencia_p		in out number) is

ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			varchar2(8000);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
sep_w				varchar(1) := '|';
tp_registro_w			varchar(15) := 'K300';
ie_apres_conta_ctb_w		ctb_regra_sped.ie_apres_conta_ctb%type;
cd_conta_contabil_ww		ctb_saldo.cd_classificacao%type;
cd_conta_contabil_w		ctb_saldo.cd_classificacao%type;
ie_debito_credito_elim_w	ctb_grupo_conta.ie_debito_credito%type;
ie_debito_credito_saldofinal_w	ctb_grupo_conta.ie_debito_credito%type;
ie_debito_credito_absoluto_w	ctb_grupo_conta.ie_debito_credito%type;
vl_eliminacao_w			ctb_movimento.vl_movimento%type := 0;
vl_eliminacao_ww		ctb_movimento.vl_movimento%type := 0;
vl_saldo_final_absoluto_w	ctb_saldo.vl_saldo%type;
vl_saldo_final_w		ctb_saldo.vl_saldo%type;
--ie_debito_credito_final_w    ctb_grupo_conta.ie_debito_credito%type;

cursor c01 is
select	k300.cd_conta_contabil cd_conta_contabil,
	sum(k300.vl_saldo) vl_saldo
from	(
	select	a.cd_conta_contabil cd_conta_contabil,
		substr(ctb_obter_classif_conta(a.cd_conta_contabil,null,dt_fim_p),1,40) cd_classif_conta,
		a.vl_saldo
	from	ctb_saldo a,
		estabelecimento b,
		conta_contabil c,
		ctb_mes_ref r
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.cd_conta_contabil	= c.cd_conta_contabil
	and	b.cd_empresa		= c.cd_empresa
	and	a.nr_seq_mes_ref	= r.nr_sequencia
	and	r.cd_empresa		= b.cd_empresa
	and	c.ie_tipo		= 'A'
	and	r.dt_referencia		= trunc(dt_fim_p, 'mm')
	and	b.cd_empresa		= cd_empresa_p
	union all
	select	c.cd_conta_referencia cd_conta_contabil,
		substr(ctb_obter_classif_conta(a.cd_conta_contabil,null,dt_fim_p),1,40) cd_classif_conta,
		a.vl_saldo
	from	ctb_saldo a,
		estabelecimento b,
		conta_contabil c,
		ctb_mes_ref r
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.cd_conta_contabil	= c.cd_conta_contabil
	and	b.cd_empresa		= c.cd_empresa
	and	a.nr_seq_mes_ref	= r.nr_sequencia
	and	r.cd_empresa		= b.cd_empresa
	and	c.ie_tipo		= 'A'
	and	r.dt_referencia		= trunc(dt_fim_p, 'mm')
	and	b.cd_empresa in (select	cd_empresa
				from	grupo_empresa_v v
				where	v.nr_seq_grupo_empresa = holding_pck.get_grupo_emp_estrut_vigente(cd_empresa_p)
				and	v.cd_empresa <> cd_empresa_p
				and	v.ie_tipo_estrutura = 2)
	) k300
group by k300.cd_conta_contabil
order by 2;

cursor c02 (cd_conta_contabil_prm varchar2) is
select	'D' ie_debito_credito_elim_w,
	sum(nvl(vl_movimento,0)) vl_eliminacao
from	ctb_movimento m,
	estabelecimento e,
	ctb_mes_ref re
where	 m.cd_estabelecimento = e.cd_estabelecimento
and	m.nr_seq_mes_ref = re.nr_sequencia
and	re.dt_referencia between dt_inicio_p and dt_fim_p
and	e.cd_empresa = cd_empresa_p
and	m.cd_conta_debito = cd_conta_contabil_prm
and	m.cd_conta_debito is not null
and	m.ie_eliminacao_lancto = 'S'
union all
select	'C' ie_debito_credito_elim_w,
	sum(nvl(vl_movimento,0)) vl_eliminacao
from	ctb_movimento m,
	estabelecimento e,
	ctb_mes_ref re
where	m.cd_estabelecimento = e.cd_estabelecimento
and	m.nr_seq_mes_ref = re.nr_sequencia
and	re.dt_referencia between dt_inicio_p and dt_fim_p
and	e.cd_empresa = cd_empresa_p
and	m.cd_conta_credito = cd_conta_contabil_prm
and	m.cd_conta_credito is not null
and	m.ie_eliminacao_lancto = 'S';

begin

select	nvl(max(a.ie_apres_conta_ctb),'CD')
into	ie_apres_conta_ctb_w
from	ctb_regra_sped a,
	ctb_sped_controle b
where	b.nr_seq_regra_sped = a.nr_sequencia
and	b.nr_sequencia = nr_seq_controle_p;

open C01;
loop
fetch C01 into
	cd_conta_contabil_w,
	vl_saldo_final_w;
	exit when C01%notfound;
begin
	vl_eliminacao_w 	:= 0;
	vl_eliminacao_ww 	:= 0;

	open c02(cd_conta_contabil_w);
	loop
	fetch c02 into
		ie_debito_credito_elim_w,
		vl_eliminacao_w;
	exit when c02%notfound;
		vl_eliminacao_ww := nvl(vl_eliminacao_ww,0) + nvl(vl_eliminacao_w,0);
	end loop;
	close c02;

	ie_debito_credito_saldofinal_w := substr(ctb_obter_situacao_saldo(cd_conta_contabil_w,vl_saldo_final_w),1,1);
	ie_debito_credito_absoluto_w := substr(ctb_obter_situacao_saldo(cd_conta_contabil_w,vl_saldo_final_absoluto_w),1,1);
	-- Considerar valor absoluto, conforme manual ECD
	if (ie_debito_credito_saldofinal_w = ie_debito_credito_elim_w) then
		vl_saldo_final_absoluto_w := abs(nvl(vl_saldo_final_w,0)) - abs(nvl(vl_eliminacao_ww,0));
	else
		vl_saldo_final_absoluto_w := nvl(vl_saldo_final_w,0) - nvl(vl_eliminacao_ww,0);
	End if;

	cd_conta_contabil_ww := cd_conta_contabil_w;

	if (ie_apres_conta_ctb_w = 'CL') then
		cd_conta_contabil_w := substr(ctb_obter_classif_conta(cd_conta_contabil_ww,null,dt_fim_p),1,40);
	elsif (ie_apres_conta_ctb_w = 'CP') then
		cd_conta_contabil_w := substr(replace(ctb_obter_classif_conta(cd_conta_contabil_ww, null, dt_fim_p),'.',''),1,40);
	end if;

	ds_linha_w := substr(	sep_w ||
				tp_registro_w										|| sep_w ||
				cd_conta_contabil_w									|| sep_w ||
				replace(replace(campo_mascara_virgula(vl_saldo_final_w),'.',''),'-','')			|| sep_w ||
				ie_debito_credito_saldofinal_w								|| sep_w ||
				replace(replace(campo_mascara_virgula(vl_eliminacao_ww),'.',''),'-','')			|| sep_w ||
				ie_debito_credito_elim_w								|| sep_w ||
				replace(replace(campo_mascara_virgula(vl_saldo_final_absoluto_w),'.',''),'-','')	|| sep_w ||
				ie_debito_credito_absoluto_w								|| sep_w,1,8000);

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into ctb_sped_registro(
		nr_sequencia,
		ds_arquivo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_controle_sped,
		ds_arquivo_compl,
		cd_registro,
		nr_linha)
	values(
		nr_seq_registro_w,
		ds_arquivo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		ds_compl_arquivo_w,
		tp_registro_w,
		nr_linha_w);

	if (nvl(vl_eliminacao_ww,0) > 0) then
	begin
		ctb_gerar_interf_k310_ecd(nr_seq_controle_p,
		nm_usuario_p,
		cd_estabelecimento_p,
		dt_inicio_p,
		dt_fim_p,
		cd_empresa_p,
		cd_conta_contabil_ww,
		nr_linha_w,
		nr_seq_registro_w);
	end;
	end if;
end;

end loop;
close C01;

commit;
qt_linha_p := nr_linha_w;
nr_sequencia_p := nr_seq_registro_w;
end ctb_gerar_interf_k300_ecd;
/