create or replace procedure dar_generate_census is

cursor  c01 is

select
	a.dt_atualizacao,
	a.nm_usuario,
	a.dt_atualizacao_nrec,
    a.nm_usuario_nrec,
	a.cd_pessoa_fisica,
    a.nr_atendimento,
	a.ie_clinica,
	b.dt_entrada_unidade,
	b.dt_saida_unidade,
	a.ie_tipo_atendimento,
	b.cd_motivo_alta_setor,
	b.cd_setor_atendimento,
	a.dt_entrada,
	a.nr_seq_queixa,
	a.cd_estabelecimento
	
from atendimento_paciente a,
     atend_paciente_unidade b,
     pessoa_fisica c
	 
where a.nr_atendimento = b.nr_atendimento
and   c.cd_pessoa_fisica = a.cd_pessoa_fisica   
and	   not exists    (select 1
				 from dar_census x
				 where x.nr_atendimento = a.nr_atendimento);
c01_w	c01%rowtype;

begin
open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;

	begin
		insert into dar_census(
            nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			nr_atendimento,
			ie_clinica,
			dt_entrada_unidade,
			dt_saida_unidade,
			ie_tipo_atendimento,
			cd_motivo_alta_Setor,
			cd_setor_atendimento,	
			dt_entrada,	
			nr_seq_queixa,		
			cd_estabelecimento_atend
			)
		values(
            dar_census_seq.nextval,
			c01_w.dt_atualizacao,
			c01_w.nm_usuario,
			c01_w.dt_atualizacao_nrec,
			c01_w.nm_usuario_nrec,
			c01_w.cd_pessoa_fisica,
			c01_w.nr_atendimento,
			c01_w.ie_clinica,
			c01_w.dt_entrada_unidade,
			c01_w.dt_saida_unidade,
			c01_w.ie_tipo_atendimento,
			c01_w.cd_motivo_alta_Setor,
			c01_w.cd_setor_atendimento,	
			c01_w.dt_entrada,	
			c01_w.nr_seq_queixa,		
			c01_w.cd_estabelecimento
			);
	
	end;
	
end loop;

close c01;

commit;

end dar_generate_census;
/
