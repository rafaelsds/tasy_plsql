CREATE OR REPLACE
PROCEDURE Obter_Preco_CBHPM(
				cd_estabelecimento_p		number,
				dt_referencia_p			date,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				dt_vigencia_edicao_p		date,
				cd_convenio_p			number,
				nr_seq_prestador_p		number,
				ie_video_p			varchar2,
				vl_medico_p		out	number,
				cd_porte_p		out	varchar2,
				tx_porte_p		out	number,
				qt_uco_p		out	number,
				nr_porte_anest_p	out	number,
				nr_auxiliar_p		out	number,
				qt_filme_p		out	number,
				qt_incidencia_p		out	number,
				ie_unid_ra_p		out	varchar2,
				vl_porte_p		out	number,
				dt_vigencia_porte_p	out	date,
				dt_vigencia_preco_p	out	date,
				vl_porte_anestesista_p	out	number,
				cd_categoria_p			varchar2,
				ie_credenciado_p		varchar2,
				nr_seq_congenere_p		Number) is

vl_custo_operacional_w	number(15,2);
vl_medico_w		number(15,2);
vl_anestesista_w	number(15,2);
cd_porte_w		varchar2(10);
tx_porte_w		number(15,4);
tx_porte_orig_w		number(15,4);
tx_porte_convenio_w	number(15,4);
qt_uco_w		number(15,4);
nr_porte_anest_w	number(3);
nr_auxiliar_w		number(3);
qt_filme_w		number(15,4);
qt_incidencia_w		number(15,4);
ie_unid_ra_w		varchar2(1);
vl_porte_w		number(15,2);
dt_vigencia_porte_w	date;
dt_vigencia_preco_w	date;
cd_porte_anestesista_w	varchar2(8);
vl_porte_anestesista_w	number(15,2);
cd_grupo_w		Number(15)	:= 0;
cd_especialidade_w	Number(15)	:= 0;
cd_area_w		Number(15)	:= 0;
IE_DATA_VIG_CBHPM_w	varchar2(01);
tx_porte_anest_w	number(15,4);
IE_DESPREZA_CASA_CBHPM_w 	varchar2(1);
vl_medico_arred_w	number(15,4);
vl_anest_arred_w	number(15,4);
VL_PORTE_NEGOCIADO_w	number(15,2):= 0;
ie_preco_cbhpm_data_w	Varchar2(1)	:= 'N';
ie_preco_cbhpm_data_ww	Varchar2(1)	:= 'N';
qt_ajuste_uco_cbhpm_w	number(10,0);
qt_uco_ajuste_w		number(15,4);
nr_porte_anest_cobranca_w	number(3,0);

CURSOR	C01 IS
	select	nvl(tx_ajuste,1),
		VL_PORTE_NEGOCIADO
	from	conv_ajuste_porte_cbhpm	a
	where	a.cd_convenio	= cd_convenio_p	
	and	a.cd_porte	= substr(cd_porte_w,1,8)
	and	((cd_procedimento 	= cd_procedimento_p 	or cd_procedimento 	is null))
	and	((ie_origem_proced 	= ie_origem_proced_p 	or ie_origem_proced 	is null))
	and	((cd_grupo_proc 	= cd_grupo_w 		or cd_grupo_proc 	is null))
	and	((cd_especialidade 	= cd_especialidade_w 	or cd_especialidade 	is null))
	and	((cd_area_procedimento 	= cd_area_w 		or cd_area_procedimento	is null))
	and 	((nvl(a.ie_tipo_ajuste,'M') = 'M') or (nvl(a.ie_tipo_ajuste,'M') = 'T'))
	and	((ie_video_p is null) or (nvl(ie_video,'A') = 'A') or (nvl(ie_video,'N') = ie_video_p))
	and	((a.cd_categoria is null) or (a.cd_categoria = nvl(cd_categoria_p, a.cd_categoria)))
	and 	((nvl(a.ie_credenciado,'T') = 'T') or (a.ie_credenciado = ie_credenciado_p))
	and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
	and	a.dt_vigencia	=
		(select	max(nvl(x.dt_vigencia,sysdate - 3650))
		from	conv_ajuste_porte_cbhpm	x
		where	x.cd_porte 	= substr(cd_porte_w,1,8)
		and	x.dt_vigencia	<= dt_referencia_p
		and	x.cd_convenio	= cd_convenio_p
		and	nvl(x.cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0))
	and	a.dt_vigencia	=
		(select	max(nvl(x.dt_vigencia,sysdate - 3650))
		from	conv_ajuste_porte_cbhpm	x
		where	x.cd_porte 	= substr(cd_porte_w,1,8)
		and	dt_referencia_p between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(x.dt_vigencia) and nvl(x.dt_vigencia_final, dt_referencia_p)
		and	x.cd_convenio	= cd_convenio_p
		and	nvl(x.cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0))
	order by	nvl(cd_procedimento,0),
			nvl(ie_origem_proced,0),
			nvl(cd_grupo_proc,0),
			nvl(cd_especialidade,0),
			nvl(cd_area_procedimento,0),			
			nvl(cd_categoria,'0'),
			nvl(cd_estabelecimento,0);

CURSOR	C02 IS
	select	nvl(tx_ajuste,1),
		VL_PORTE_NEGOCIADO
	from	conv_ajuste_porte_cbhpm	a
	where	a.cd_convenio	= cd_convenio_p	
	and	a.cd_porte	= substr(cd_porte_anestesista_w,1,8)
	and	((cd_procedimento 	= cd_procedimento_p 	or cd_procedimento 	is null))
	and	((ie_origem_proced 	= ie_origem_proced_p 	or ie_origem_proced 	is null))
	and	((cd_grupo_proc 	= cd_grupo_w 		or cd_grupo_proc 	is null))
	and	((cd_especialidade 	= cd_especialidade_w 	or cd_especialidade 	is null))
	and	((cd_area_procedimento 	= cd_area_w 		or cd_area_procedimento	is null))
	and 	((nvl(a.ie_tipo_ajuste,'M') = 'A') or (nvl(a.ie_tipo_ajuste,'M') = 'T'))
	and	((ie_video_p is null) or (nvl(ie_video,'A') = 'A') or (nvl(ie_video,'N') = ie_video_p))
	and	((a.cd_categoria is null) or (a.cd_categoria = nvl(cd_categoria_p, a.cd_categoria)))
	and 	((nvl(a.ie_credenciado,'T') = 'T') or (a.ie_credenciado = ie_credenciado_p))
	and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
	and	a.dt_vigencia	=
		(select	max(nvl(x.dt_vigencia,sysdate - 3650))
		from	conv_ajuste_porte_cbhpm	x
		where	x.cd_porte 	= substr(cd_porte_anestesista_w,1,8)
		and	x.dt_vigencia	<= dt_referencia_p
		and	x.cd_convenio	= cd_convenio_p
		and	nvl(x.cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0))
	and	a.dt_vigencia	=
		(select	max(nvl(x.dt_vigencia,sysdate - 3650))
		from	conv_ajuste_porte_cbhpm	x
		where	x.cd_porte 	= substr(cd_porte_anestesista_w,1,8)
		and	dt_referencia_p between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(x.dt_vigencia) and nvl(x.dt_vigencia_final, dt_referencia_p)
		and	x.cd_convenio	= cd_convenio_p
		and	nvl(x.cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0))
	order by	nvl(cd_procedimento,0),
			nvl(ie_origem_proced,0),
			nvl(cd_grupo_proc,0),
			nvl(cd_especialidade,0),
			nvl(cd_area_procedimento,0),
			nvl(cd_categoria,'0'),
			nvl(cd_estabelecimento,0);

Cursor C03 is
	select	nvl(a.tx_ajuste,1),
		a.vl_porte_negociado
	from	pls_ajuste_porte_cbhpm	a
	where	((a.nr_seq_prestador is null) or (a.nr_seq_prestador = nr_seq_prestador_p))
	and	a.cd_porte		= substr(cd_porte_w,1,8)
	and	((cd_procedimento 	= cd_procedimento_p 	or cd_procedimento 	is null))
	and	((ie_origem_proced 	= ie_origem_proced_p 	or ie_origem_proced 	is null))
	and	((cd_grupo_proc 	= cd_grupo_w 		or cd_grupo_proc 	is null))
	and	((cd_especialidade 	= cd_especialidade_w 	or cd_especialidade 	is null))
	and	((cd_area_procedimento 	= cd_area_w 		or cd_area_procedimento	is null))
	and 	(nvl(a.ie_tipo_ajuste,'M') = 'M')
	and	a.ie_situacao	= 'A'
	and	a.dt_vigencia	=
			(select	max(nvl(x.dt_vigencia,sysdate - 3650))
			from	pls_ajuste_porte_cbhpm	x
			where	x.cd_porte 	= substr(cd_porte_w,1,8)
			and	x.dt_vigencia	<= dt_referencia_p
			and	x.ie_situacao	= 'A'
			and	((x.nr_seq_prestador is null) or (x.nr_seq_prestador = nr_seq_prestador_p)))
	order by
		nvl(cd_procedimento,0),
		nvl(ie_origem_proced,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0),
		nvl(nr_seq_prestador,0);

Cursor C04 is
	select	nvl(a.tx_ajuste,1),
		a.vl_porte_negociado
	from	pls_ajuste_porte_cbhpm	a
	where	((a.nr_seq_prestador is null) or (a.nr_seq_prestador = nr_seq_prestador_p))
	and	a.cd_porte		= substr(cd_porte_anestesista_w,1,8)
	and	((cd_procedimento 	= cd_procedimento_p 	or cd_procedimento 	is null))
	and	((ie_origem_proced 	= ie_origem_proced_p 	or ie_origem_proced 	is null))
	and	((cd_grupo_proc 	= cd_grupo_w 		or cd_grupo_proc 	is null))
	and	((cd_especialidade 	= cd_especialidade_w 	or cd_especialidade 	is null))
	and	((cd_area_procedimento 	= cd_area_w 		or cd_area_procedimento	is null))
	and 	(nvl(a.ie_tipo_ajuste,'M') = 'A')
	and	a.ie_situacao	= 'A'
	and	a.dt_vigencia	=
			(select	max(nvl(x.dt_vigencia,sysdate - 3650))
			from	pls_ajuste_porte_cbhpm	x
			where	x.cd_porte 	= substr(cd_porte_anestesista_w,1,8)
			and	x.dt_vigencia	<= dt_referencia_p
			and	x.ie_situacao	= 'A'
			and	((x.nr_seq_prestador is null) or (x.nr_seq_prestador = nr_seq_prestador_p)))
	order by
		nvl(cd_procedimento,0),
		nvl(ie_origem_proced,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0),		
		nvl(nr_seq_prestador,0);

Cursor C05 is
	select	qt_uco
	from	conv_ajuste_uco_cbhpm
	where	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = cd_estabelecimento_p
	and	nvl(cd_procedimento, nvl(cd_procedimento_p,0))		= nvl(cd_procedimento_p,0)
	and	nvl(ie_origem_proced, nvl(ie_origem_proced_p,0))	= nvl(ie_origem_proced_p,0)
	and	nvl(cd_grupo_proc, nvl(cd_grupo_w,0))			= nvl(cd_grupo_w,0)
	and	nvl(cd_especialidade, nvl(cd_especialidade_w,0))	= nvl(cd_especialidade_w,0)
	and	nvl(cd_area_procedimento, nvl(cd_area_w,0))		= nvl(cd_area_w,0)
	and	dt_referencia_p between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_vigencia_inicial) and nvl(dt_vigencia_final, dt_referencia_p)
	and	ie_situacao = 'A'
	order by	dt_vigencia_inicial,
		nvl(cd_procedimento,0),
		nvl(ie_origem_proced,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0);
		
		
Cursor C06 is
	select	nr_porte_anest_cobranca
	from	regra_porte_anest_cbhpm
	where	nr_porte_anest = nr_porte_anest_w
	and	nvl(cd_procedimento, nvl(cd_procedimento_p,0))	= nvl(cd_procedimento_p,0)
	and	((cd_procedimento is null) or (nvl(ie_origem_proced, nvl(ie_origem_proced_p,0)) = nvl(ie_origem_proced_p,0)))
	and	nvl(cd_area_procedimento, cd_area_w) = cd_area_w
	and	nvl(cd_especialidade, cd_especialidade_w) = cd_especialidade_w
	and	nvl(cd_grupo_proc, cd_grupo_w) = cd_grupo_w
	order by nr_porte_anest;		
			

BEGIN

select	nvl(max(IE_DATA_VIG_CBHPM), 'N'),
	nvl(max(IE_DESPREZA_CASA_CBHPM), 'N'),
	nvl(max(IE_PRECO_CBHPM_DATA), 'N')
into	IE_DATA_VIG_CBHPM_w,
	IE_DESPREZA_CASA_CBHPM_w,
	ie_preco_cbhpm_data_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_data_vig_cbhpm_w	= 'R') then	
	select	nvl(max(ie_data_vig_cbhpm), 'N')
	into	ie_data_vig_cbhpm_w
	from	convenio_estabelecimento
	where	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento 	= cd_estabelecimento_p;
end if;

/* Obter Estrutura do procedimento */
select	nvl(max(cd_grupo_proc),0),
	nvl(max(cd_especialidade),0),
	nvl(max(cd_area_procedimento),0)
into	cd_grupo_w,
	cd_especialidade_w,
	cd_area_w
from	Estrutura_Procedimento_V
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

/* Felipe - 16/09/2011 - OS 301610 - Se o parametro estiver como Ambos ou Prestador e o calculo for para o Tasy/Hospital ou 
	como Ambos ou Operadora e o calculo for para Tasy/OPS */
if	(((ie_preco_cbhpm_data_w in ('A','P')) and
	(cd_convenio_p is not null)) or
	((ie_preco_cbhpm_data_w in ('A','O')) and
	((nr_seq_prestador_p is not null) or
	( nr_seq_congenere_p is not null)))) then
	ie_preco_cbhpm_data_ww	:= 'S';
end if;

begin

/* Felipe - 16/09/2011 - OS 301610 */
if	(ie_preco_cbhpm_data_ww = 'S') then
	select	a.dt_vigencia,
		nvl(a.cd_porte,'X'),
	 	nvl(a.tx_porte,1),
		nvl(a.qt_uco,0),
		nvl(a.nr_porte_anest,0),
		nvl(a.nr_auxiliar,0),
		nvl(a.qt_filme,0),
		nvl(a.qt_incidencia,0),
		a.ie_unid_ra
	into	dt_vigencia_preco_w,
		cd_porte_w,
		tx_porte_w,
		qt_uco_w,
		nr_porte_anest_w,
		nr_auxiliar_w,
		qt_filme_w,
		qt_incidencia_w,
		ie_unid_ra_w
	from	cbhpm_preco a
	where	a.cd_procedimento	= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p
	and	a.dt_vigencia		=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
			from 	cbhpm_preco x
			where x.cd_procedimento		= cd_procedimento_p
			and	x.ie_origem_proced	= ie_origem_proced_p
			and	x.dt_vigencia		<= dt_referencia_p);
elsif	(IE_DATA_VIG_CBHPM_w	= 'N') then
	select	a.dt_vigencia,
		nvl(a.cd_porte,'X'),
	 	nvl(a.tx_porte,1),
		 nvl(a.qt_uco,0),
		 nvl(a.nr_porte_anest,0),
		 nvl(a.nr_auxiliar,0),
		 nvl(a.qt_filme,0),
		 nvl(a.qt_incidencia,0),
		 a.ie_unid_ra
	into	 dt_vigencia_preco_w,
		 cd_porte_w,
		 tx_porte_w,
		 qt_uco_w,
		 nr_porte_anest_w,
		 nr_auxiliar_w,
		 qt_filme_w,
		 qt_incidencia_w,
		 ie_unid_ra_w
	from	 cbhpm_preco a
	where	 a.cd_procedimento	= cd_procedimento_p
	and	 a.ie_origem_proced	= ie_origem_proced_p
	and	 a.dt_vigencia		=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
			from 	cbhpm_preco x
			where x.cd_procedimento		= cd_procedimento_p
			and	x.ie_origem_proced	= ie_origem_proced_p
			and	x.dt_vigencia		<= dt_referencia_p
			and	x.dt_vigencia		<= nvl(dt_vigencia_edicao_p, dt_referencia_p));
else
	select	a.dt_vigencia,
		nvl(a.cd_porte,'X'),
	 	nvl(a.tx_porte,1),
		 nvl(a.qt_uco,0),
		 nvl(a.nr_porte_anest,0),
		 nvl(a.nr_auxiliar,0),
		 nvl(a.qt_filme,0),
		 nvl(a.qt_incidencia,0),
		 a.ie_unid_ra
	into	 dt_vigencia_preco_w,
		 cd_porte_w,
		 tx_porte_w,
		 qt_uco_w,
		 nr_porte_anest_w,
		 nr_auxiliar_w,
		 qt_filme_w,
		 qt_incidencia_w,
		 ie_unid_ra_w
	from	 cbhpm_preco a
	where	 a.cd_procedimento	= cd_procedimento_p
	and	 a.ie_origem_proced	= ie_origem_proced_p
	and	 a.dt_vigencia		=
		(select min(nvl(x.dt_vigencia,sysdate - 3650))
			from 	cbhpm_preco x
			where x.cd_procedimento		= cd_procedimento_p
			and	x.ie_origem_proced	= ie_origem_proced_p
			and	x.dt_vigencia		<= dt_referencia_p
			and	x.dt_vigencia		<= nvl(dt_vigencia_edicao_p, dt_referencia_p)
			and	x.dt_vigencia		= dt_vigencia_edicao_p);
end if;
exception
     	when others then
	begin
	dt_vigencia_preco_w	:= null;
	cd_porte_w		:= 'X';
	tx_porte_w		:= 1;
	qt_uco_w		:= 0;
	nr_porte_anest_w	:= 0;
	nr_auxiliar_w		:= 0;
	qt_filme_w		:= 0;
	qt_incidencia_w		:= null;
	end;
end;		

if	(tx_porte_w	= 0) then
	tx_porte_w	:= 1;
end if;

tx_porte_orig_w		:= tx_porte_w;

if	(cd_porte_w	<> 'X') then
	begin
	if	(nvl(cd_convenio_p,0) > 0) then
		OPEN C01;
		LOOP
		FETCH C01 into	
			tx_porte_convenio_w,
			VL_PORTE_NEGOCIADO_w;
		exit when c01%notfound;
			/*Andre e Felipe OS71939*/
			if	(tx_porte_convenio_w <> 1) then
				tx_porte_W	:= tx_porte_convenio_w;
			else	
				tx_porte_w	:= tx_porte_orig_w;
			end if;
		
		END LOOP;
		CLOSE C01;
	elsif	(nvl(nr_seq_prestador_p,0) > 0) or
		(nvl(nr_seq_congenere_p,0) > 0) then
		open C03;
		loop
		fetch C03 into	
			tx_porte_convenio_w,
			vl_porte_negociado_w;
		exit when C03%notfound;
			begin			
			if	(tx_porte_convenio_w <> 1) then
				tx_porte_w	:= tx_porte_convenio_w;
			else	
				tx_porte_w	:= tx_porte_orig_w;
			end if;
			end;
		end loop;
		close C03;
	
	end if;

	/* Felipe e Fabricio - OS 194603 - Colocamos a ultima linha do comando abaixo*/
	if	(IE_DATA_VIG_CBHPM_w	= 'N') then
		select	a.vl_porte,
			a.dt_vigencia
		into	vl_porte_w,
			dt_vigencia_porte_w
		from	cbhpm_porte	a
		where	a.cd_porte	= substr(cd_porte_w,1,8)
		and	a.dt_vigencia	=
			(select	max(nvl(x.dt_vigencia,sysdate - 3650))
				from cbhpm_porte x
				where x.cd_porte = substr(cd_porte_w,1,8)
				and	x.dt_vigencia <= dt_referencia_p
				and	x.dt_vigencia <= nvl(dt_vigencia_edicao_p, dt_referencia_p));
	else
		select	a.vl_porte,
			a.dt_vigencia
		into	vl_porte_w,
			dt_vigencia_porte_w
		from	cbhpm_porte	a
		where	a.cd_porte	= substr(cd_porte_w,1,8)
		and	a.dt_vigencia	=
			(select	min(nvl(x.dt_vigencia,sysdate - 3650))
				from cbhpm_porte x
				where x.cd_porte = substr(cd_porte_w,1,8)
				and	x.dt_vigencia 	<= dt_referencia_p
				and	x.dt_vigencia	= dt_vigencia_edicao_p);
	end if;
	exception
     		when others then
		begin
		vl_porte_w		:= 0;
		dt_vigencia_porte_w	:= null;
		end;
	end;
end if;

select	count(*)
into	qt_ajuste_uco_cbhpm_w
from	conv_ajuste_uco_cbhpm
where	cd_convenio = cd_convenio_p
and	cd_estabelecimento = cd_estabelecimento_p;

if	(nvl(qt_ajuste_uco_cbhpm_w,0) > 0) then

	open C05;
	loop
	fetch C05 into	
		qt_uco_ajuste_w;
	exit when C05%notfound;
		begin
		qt_uco_w	:= qt_uco_ajuste_w;
		end;
	end loop;
	close C05;

end if;

/*Define valor do medico */
if	(IE_DESPREZA_CASA_CBHPM_w = 'S') then	

	/*Fabricio OS 189016 e 187019*/
	if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
		vl_medico_arred_w:= VL_PORTE_NEGOCIADO_w * tx_porte_orig_w;
		arredondamento(vl_medico_arred_w, 2, 'D');
		vl_medico_w:= vl_medico_arred_w;
	else
		vl_medico_arred_w:= vl_porte_w * tx_porte_w;
		arredondamento(vl_medico_arred_w, 2, 'D');
		vl_medico_w:= vl_medico_arred_w;
	end if;
	
else
	/*Fabricio OS 189016 e 187019*/
	if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
		vl_medico_w	:= (VL_PORTE_NEGOCIADO_w * tx_porte_orig_w);
	else
		vl_medico_w	:= (vl_porte_w * tx_porte_w);
	end if;		
	
end if;

nr_porte_anest_cobranca_w:= null;
open C06;
loop
fetch C06 into	
	nr_porte_anest_cobranca_w;
exit when C06%notfound;
	begin
	nr_porte_anest_cobranca_w:= nr_porte_anest_cobranca_w;
	end;
end loop;
close C06;

if	(nr_porte_anest_cobranca_w is not null) then
	nr_porte_anest_w:= nr_porte_anest_cobranca_w;
end if;

/*	Define porte do anestesista */
if	(nr_porte_anest_w	= 1) then
	cd_porte_anestesista_w	:= '3A';
elsif (nr_porte_anest_w	= 2) then
	cd_porte_anestesista_w	:= '3C';
elsif (nr_porte_anest_w	= 3) then
	cd_porte_anestesista_w	:= '4C';
elsif (nr_porte_anest_w	= 4) then
	cd_porte_anestesista_w	:= '6B';
elsif (nr_porte_anest_w	= 5) then
	cd_porte_anestesista_w	:= '7C';
elsif (nr_porte_anest_w	= 6) then
	cd_porte_anestesista_w	:= '9B';
elsif (nr_porte_anest_w	= 7) then
	cd_porte_anestesista_w	:= '10C';
elsif (nr_porte_anest_w	= 8) then
	cd_porte_anestesista_w	:= '12A';
else
	cd_porte_anestesista_w	:= 'X';
end if;

vl_porte_anestesista_w	:= 0;
if	(cd_porte_anestesista_w <> 'X') then
	begin
	
	if	(IE_DATA_VIG_CBHPM_w	= 'N') then
		select 	nvl(max(a.vl_porte),0)
		into	vl_porte_anestesista_w
		from	cbhpm_porte a
		where	a.cd_porte		= cd_porte_anestesista_w
		and	a.dt_vigencia	=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
			from cbhpm_porte x
			where x.cd_porte = cd_porte_anestesista_w
			and	x.dt_vigencia <= dt_referencia_p
			and	x.dt_vigencia <= nvl(dt_vigencia_edicao_p, dt_referencia_p));
	else
		select 	nvl(max(a.vl_porte),0)
		into	vl_porte_anestesista_w
		from	cbhpm_porte a
		where	a.cd_porte	= cd_porte_anestesista_w
		and	a.dt_vigencia	=
		(select	min(nvl(x.dt_vigencia,sysdate - 3650))
				from cbhpm_porte x
				where x.cd_porte = cd_porte_anestesista_w
				and	x.dt_vigencia 	<= dt_referencia_p
				and	x.dt_vigencia	= dt_vigencia_edicao_p);
	end if;
	
	tx_porte_anest_w:= 1;
	
	if	(nvl(cd_convenio_p,0) > 0) then
		OPEN C02;
		LOOP
		FETCH C02 into	
			tx_porte_convenio_w,
			VL_PORTE_NEGOCIADO_w;
		exit when c02%notfound;
			begin
			tx_porte_anest_w	:= tx_porte_convenio_w;
			end;		
		END LOOP;
		CLOSE C02;
	elsif	(nvl(nr_seq_prestador_p,0) > 0) or
		(nvl(nr_seq_congenere_p,0) > 0) then
		open C04;
		loop
		fetch C04 into	
			tx_porte_convenio_w,
			vl_porte_negociado_w;
		exit when C04%notfound;
			begin
			tx_porte_anest_w	:= tx_porte_convenio_w;
			end;
		end loop;
		close C04;
	end if;

	/*	Define valor do Anestesista */
	if	(IE_DESPREZA_CASA_CBHPM_w = 'S') then	

		/*Fabricio OS 189016 e 187019*/
		if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
			vl_anest_arred_w:= VL_PORTE_NEGOCIADO_w * 1;
			arredondamento(vl_anest_arred_w, 2, 'D');
			vl_porte_anestesista_w:= vl_anest_arred_w;
		else
			vl_anest_arred_w:= vl_porte_anestesista_w * tx_porte_anest_w;
			arredondamento(vl_anest_arred_w, 2, 'D');
			vl_porte_anestesista_w:= vl_anest_arred_w;
		end if;
	
	else

		/*Fabricio OS 189016 e 187019*/
		if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
			vl_porte_anestesista_w	:= (VL_PORTE_NEGOCIADO_w * 1);
		else
			vl_porte_anestesista_w	:= (vl_porte_anestesista_w * tx_porte_anest_w);
		end if;
		
	end if;

		
	end;
end if;

/*	Define valor custo operacional */
vl_custo_operacional_w	:= 0;


/*	Retorno dos valores */

vl_medico_p		:= vl_medico_w;
cd_porte_p		:= cd_porte_w;
tx_porte_p		:= tx_porte_w;
qt_uco_p		:= qt_uco_w;
nr_porte_anest_p	:= nr_porte_anest_w;
nr_auxiliar_p		:= nr_auxiliar_w;
qt_filme_p		:= qt_filme_w;
qt_incidencia_p		:= qt_incidencia_w;
ie_unid_ra_p		:= ie_unid_ra_w;
vl_porte_p		:= vl_porte_w;
dt_vigencia_porte_p	:= dt_vigencia_porte_w;
dt_vigencia_preco_p	:= dt_vigencia_preco_w;
vl_porte_anestesista_p	:= vl_porte_anestesista_w;

END Obter_Preco_CBHPM;
/
