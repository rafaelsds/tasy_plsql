Create or Replace 
procedure Gerar_Conta_Paciente_Repasse(	nr_interno_conta_p	number,
					nm_usuario_p	varchar2) is

ie_repasse_mat_w		varchar2(1);
ie_repasse_proc_w		varchar2(1);
ie_repasse_mat_conv_w	varchar2(1);
ie_repasse_proc_conv_w	varchar2(1);
nr_seq_procedimento_w	number(10,0);
nr_seq_material_w		number(10,0);
vl_material_w		number(15,2);
cd_estabelecimento_w	Number(04,0);
cd_medico_resp_w		Varchar2(10);
cd_convenio_w		Number(05,0);
cd_edicao_amb_w		Number(06,0);
ie_tipo_atendimento_w	Number(03,0);
cd_categoria_w		Varchar2(10);
dt_entrada_w		date;
ie_tipo_convenio_w	Number(5,0);
vl_repasse_mat_w		number(15,4);
vl_repasse_proc_w		number(15,4);
cont_w			number(10);
cont_rep_item_w		number(10);
nr_seq_item_repasse_w	number(10);
ds_observacao_w		varchar2(4000);
nr_repasse_terceiro_w	number(10);
ie_status_w		varchar2(1);
nr_lote_contabil_w	number(10);
ie_vincula_repasse_w	varchar2(1) := 'S';
dt_aprovacao_w		date;
nr_titulo_w		repasse_terceiro_venc.nr_titulo%type;	

cursor	c01 is
	select	a.nr_sequencia
	from	conta_paciente b,
		procedimento_paciente a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and 	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	(ie_repasse_proc_w	= 'C' or b.ie_cancelamento is not null);	-- Edgar 27/07/2006 OS 36638, estornar repasse de conta cancel


cursor	c02 is
	select	a.nr_sequencia
	from	conta_paciente b,
		material_atend_paciente a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and 	a.cd_motivo_exc_conta	is null
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	(ie_repasse_mat_w	= 'C' or b.ie_cancelamento is not null);	-- Edgar 27/07/2006 OS 36638, estornar repasse de conta cancel
	
cursor	c03 is
	select	a.nr_sequencia,
		a.nr_repasse_terceiro,
		a.nr_lote_contabil
	from	conta_paciente b,
		repasse_terceiro_item a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	b.ie_cancelamento is not null
	and	cont_w		= 0;

begin

select 	nvl(max(c.ie_repasse_mat),'N'),
	nvl(max(c.ie_repasse_proc),'N'),
	max(a.cd_estabelecimento),
	max(b.cd_medico_resp),
	max(ie_tipo_atendimento),
	max(cd_convenio_parametro),
	max(cd_categoria_parametro),
	max(dt_entrada),
	max(OBTER_TIPO_CONVENIO(cd_convenio_parametro))
into	ie_repasse_mat_w,
	ie_repasse_proc_w,
	cd_estabelecimento_w,
	cd_medico_resp_w,
	ie_tipo_atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	dt_entrada_w,
	ie_tipo_convenio_w
from	parametro_faturamento c,
	atendimento_paciente b,
	conta_paciente a
where	a.nr_atendimento		= b.nr_atendimento
and	a.cd_estabelecimento	= c.cd_estabelecimento
and	a.nr_interno_conta		= nr_interno_conta_p;

select	count(*)
into	cont_w
from	REGRA_REPASSE_CONPACI
where	cd_estabelecimento	= cd_estabelecimento_w;

-- Edgar 16/11/2006 OS 42066, utilizar regra de repasse de procedimentos do SUS Andr� 18/07/2008 OS101321
if	(ie_tipo_convenio_w = 3) then
	select	nvl(max(ie_repasse_proc), ie_repasse_proc_w)
	into	ie_repasse_proc_w
	from	sus_parametros_aih
	where	cd_estabelecimento	= cd_estabelecimento_w;
end if;


select   Obter_Valor_Conv_Estab(cd_convenio_w,cd_estabelecimento_w,'IE_REPASSE_PROC'),
	 Obter_Valor_Conv_Estab(cd_convenio_w,cd_estabelecimento_w,'IE_REPASSE_MAT')
into	 ie_repasse_proc_conv_w,
	 ie_repasse_mat_conv_w
from dual;


if (ie_repasse_proc_conv_w is not null) then
	ie_repasse_proc_w := ie_repasse_proc_conv_w;
end if;

if (ie_repasse_mat_conv_w is not null) then
	ie_repasse_mat_w := ie_repasse_mat_conv_w;
end if;


/*      Obter a edicao do convenio  */
/* Ricardo 13/11/2006 -  
select	max(cd_edicao_amb)
into	cd_edicao_amb_w
from	convenio_amb
where	cd_estabelecimento	= cd_estabelecimento_w
  and	cd_convenio		= cd_convenio_w
  and	cd_categoria		= cd_categoria_w
  and	(nvl(ie_situacao,'A')	= 'A')
  and	dt_inicio_vigencia	=
	(select	max(dt_inicio_vigencia)
	from	convenio_amb a
	where	a.cd_estabelecimento  = cd_estabelecimento_w
	and	a.cd_convenio         = cd_convenio_w
	and	a.cd_categoria        = cd_categoria_w
	and	(nvl(a.ie_situacao,'A')= 'A')
	and	a.dt_inicio_vigencia <=  dt_entrada_w);
*/

begin
select	obter_edicao (cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, dt_entrada_w, null)
into	cd_edicao_amb_w
from	dual;
exception
	when others then
		select	max(cd_edicao_amb)
		into	cd_edicao_amb_w
		from	convenio_amb
		where	cd_estabelecimento	= cd_estabelecimento_w
		  and	cd_convenio		= cd_convenio_w
		  and	cd_categoria		= cd_categoria_w
		  and	(nvl(ie_situacao,'A')	= 'A')
		  and	dt_inicio_vigencia	=
			(select	max(dt_inicio_vigencia)
			from	convenio_amb a
			where	a.cd_estabelecimento  = cd_estabelecimento_w
			and	a.cd_convenio         = cd_convenio_w
			and	a.cd_categoria        = cd_categoria_w
			and	(nvl(a.ie_situacao,'A')= 'A')
			and	a.dt_inicio_vigencia <=  dt_entrada_w);
end; 


OPEN C01;
LOOP
FETCH C01 into nr_seq_procedimento_w;
exit when c01%notfound;
	
	gerar_procedimento_repasse(
		nr_seq_procedimento_w, 
		cd_estabelecimento_w,
		cd_medico_resp_w,
		nm_usuario_p,
		cd_edicao_amb_w,
		cd_convenio_w,
		ie_tipo_atendimento_w,
		null,
		null,
		null);
      
END LOOP;
Close C01;


OPEN C02;
LOOP
FETCH C02 into	nr_seq_material_w;
exit when c02%notfound;
	gerar_material_repasse	(
		nr_seq_material_w,
		cd_estabelecimento_w,
		cd_medico_resp_w,
		nm_usuario_p,
		cd_edicao_amb_w,
		cd_convenio_w,
		ie_tipo_atendimento_w,
		null,
		null);
      
END LOOP;
Close C02;

OPEN C03;
LOOP
FETCH C03 into nr_seq_item_repasse_w, nr_repasse_terceiro_w, nr_lote_contabil_w;
exit when c03%notfound;
	
	select	max(ie_status),
		max(dt_aprovacao_terceiro)
	into	ie_status_w,
		dt_aprovacao_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	repasse_terceiro_venc
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
		
	if	(nvl(ie_status_w,'A') = 'A') and
		(nr_lote_contabil_w is null) then
		begin
		delete	from	repasse_terceiro_item
		where	nr_interno_conta	= nr_interno_conta_p;
		end;
	else
		begin
		
		if	(dt_aprovacao_w is not null) or
			(nr_titulo_w is not null) then
			ie_vincula_repasse_w := 'N';
		end if;
		
		ds_observacao_w	:= substr(wheb_mensagem_pck.get_texto(305915,'NR_SEQUENCIA=' || nr_seq_item_repasse_w),1,255);
		insert	into	repasse_terceiro_item(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				vl_repasse,
				nr_seq_terceiro,
				ds_observacao,
				dt_lancamento,
				nr_interno_conta,
				nr_atendimento,
				cd_centro_custo,
				cd_centro_custo_prov,
				cd_conta_contabil,
				cd_conta_contabil_prov,
				cd_conta_financ,
				cd_convenio,
				cd_material,
				cd_medico,
				cd_procedimento,
				dt_contabil,
				dt_liberacao,
				dt_plantao,
				ie_origem_proced,
				ie_partic_tributo,
				nr_adiant_pago,
				nr_lote_contabil,
				nr_lote_contabil_prov,
				nr_repasse_terceiro,
				nr_seq_med_plantao,
				nr_seq_proc_interno,
				nr_seq_regra,
				nr_seq_regra_esp,
				nr_seq_repasse_prod,
				nr_seq_ret_glosa,
				nr_seq_terc_regra_esp,
				nr_seq_terc_regra_item,
				nr_seq_terc_rep,
				nr_seq_tipo,
				nr_seq_tipo_valor,
				nr_seq_trans_fin,
				nr_seq_trans_fin_prov,
				qt_minuto)
			select	repasse_terceiro_item_seq.nextval,
				nm_usuario_p,
				a.dt_atualizacao,
				(a.vl_repasse * -1),
				a.nr_seq_terceiro,
				ds_observacao_w,
				a.dt_lancamento,
				a.nr_interno_conta,
				a.nr_atendimento,
				a.cd_centro_custo,
				a.cd_centro_custo_prov,
				a.cd_conta_contabil,
				a.cd_conta_contabil_prov,
				a.cd_conta_financ,
				a.cd_convenio,
				a.cd_material,
				a.cd_medico,
				a.cd_procedimento,
				a.dt_contabil,
				a.dt_liberacao,
				a.dt_plantao,
				a.ie_origem_proced,
				a.ie_partic_tributo,
				a.nr_adiant_pago,
				a.nr_lote_contabil,
				a.nr_lote_contabil_prov,
				decode(ie_vincula_repasse_w,'S',a.nr_repasse_terceiro,null),
				a.nr_seq_med_plantao,
				a.nr_seq_proc_interno,
				a.nr_seq_regra,
				a.nr_seq_regra_esp,
				a.nr_seq_repasse_prod,
				a.nr_seq_ret_glosa,
				a.nr_seq_terc_regra_esp,
				a.nr_seq_terc_regra_item,
				a.nr_seq_terc_rep,
				a.nr_seq_tipo,
				a.nr_seq_tipo_valor,
				a.nr_seq_trans_fin,
				a.nr_seq_trans_fin_prov,
				a.qt_minuto
			from	repasse_terceiro_item a
			where	nr_interno_conta	= nr_interno_conta_p;
		end;	
	end if;	
END LOOP;
CLOSE C03;


select	sum(vl_repasse)
into	vl_repasse_proc_w
from	procedimento_repasse a,
	procedimento_paciente b
where	a.nr_seq_procedimento	= b.nr_sequencia
and	b.nr_interno_conta		= nr_interno_conta_p;

select	sum(vl_repasse)
into	vl_repasse_mat_w
from	material_repasse a,
	material_atend_paciente b
where	a.nr_seq_material		= b.nr_sequencia
and	b.nr_interno_conta		= nr_interno_conta_p;


update	conta_paciente
set	vl_repasse_conta	= nvl(vl_repasse_mat_w,0) + nvl(vl_repasse_proc_w,0)
where	nr_interno_conta	= nr_interno_conta_p;


if	(nr_interno_conta_p is not null) then

			
	gerar_procmat_repasse_nf(nr_interno_conta_p, nm_usuario_p, 'S');
	
	
end if;	

end Gerar_Conta_Paciente_Repasse;
/