create or replace
procedure Atualizar_plt_controle(	nm_usuario_p		varchar2,
					nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					ie_tipo_item_p		varchar2,
					ie_atualizar_p		varchar2,
					nr_prescricao_p		number) is 

nr_atendimento_w		number(15);
cd_pessoa_fisica_w		varchar2(15);
					
pragma autonomous_transaction;

begin

if	(nr_prescricao_p	is not null) then
	begin
		select	nr_atendimento,
				cd_pessoa_fisica
		into	nr_atendimento_w,
				cd_pessoa_fisica_w
		from	prescr_medica
		where	nr_prescricao	= nr_prescricao_p;
	exception
	when others then
		nr_atendimento_w := nr_atendimento_p;
		cd_pessoa_fisica_w := cd_pessoa_fisica_p;
	end;
else
	nr_atendimento_w	:= nr_atendimento_p;
	cd_pessoa_fisica_w	:= cd_pessoa_fisica_p;

end if;	
	
if	(ie_tipo_item_p	= 'M') then
	update	plt_controle
	set	ie_atualizar_medic	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'MAT') then
	update	plt_controle
	set	ie_atualizar_material	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'CCG') then
	update	plt_controle
	set	ie_atualizar_ccg	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'CIG') then
	update	plt_controle
	set	ie_atualizar_cig	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'DIA') then
	update	plt_controle
	set	ie_atualizar_dialise	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'D') then
	update	plt_controle
	set	ie_atualizar_dieta	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'G') then
	update	plt_controle
	set	ie_atualizar_gas	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'HM') then
	update	plt_controle
	set	ie_atualizar_hemoterap	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'IVC') then
	update	plt_controle
	set	ie_atualizar_ivc	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'J') then
	update	plt_controle
	set	ie_atualizar_jejum	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'L') then
	update	plt_controle
	set	ie_atualizar_lab	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'LD') then
	update	plt_controle
	set	ie_atualizar_leite	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'NPA') then
	update	plt_controle
	set	ie_atualizar_npt_adulta	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'NPN') then
	update	plt_controle
	set	ie_atualizar_npt_neo	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'NPP') then
	update	plt_controle
	set	ie_atualizar_npt_ped	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'P') then
	update	plt_controle
	set	ie_atualizar_proced	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'R') then
	update	plt_controle
	set	ie_atualizar_rec	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'SNE') then
	update	plt_controle
	set	ie_atualizar_sne	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'SOL') then
	update	plt_controle
	set	ie_atualizar_solucao	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
elsif	(ie_tipo_item_p	= 'S') then
	update	plt_controle
	set	ie_atualizar_supl	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	nm_usuario		= nm_usuario_p;	
elsif	(ie_tipo_item_p	= 'TODOS') then
	update	plt_controle
	set	ie_atualizar_dieta     	= ie_atualizar_p,
		ie_atualizar_jejum     	= ie_atualizar_p,
		ie_atualizar_supl      	= ie_atualizar_p,
		ie_atualizar_sne       	= ie_atualizar_p,
		ie_atualizar_npt_adulta	= ie_atualizar_p,
		ie_atualizar_npt_neo   	= ie_atualizar_p,
		ie_atualizar_npt_ped   	= ie_atualizar_p,
		ie_atualizar_leite     	= ie_atualizar_p,
		ie_atualizar_solucao   	= ie_atualizar_p,
		ie_atualizar_medic     	= ie_atualizar_p,
		ie_atualizar_material  	= ie_atualizar_p,
		ie_atualizar_proced    	= ie_atualizar_p,
		ie_atualizar_lab       	= ie_atualizar_p,
		ie_atualizar_ccg       	= ie_atualizar_p,
		ie_atualizar_cig       	= ie_atualizar_p,
		ie_atualizar_ivc       	= ie_atualizar_p,
		ie_atualizar_gas       	= ie_atualizar_p,
		ie_atualizar_hemoterap 	= ie_atualizar_p,
		ie_atualizar_rec       	= ie_atualizar_p,
		ie_atualizar_dialise   	= ie_atualizar_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_atendimento		= nvl(nr_atendimento_w,nr_atendimento)
	and	((nm_usuario		= nm_usuario_p) or
		 (nm_usuario_p		is null));
end if;

commit;

end Atualizar_plt_controle;
/