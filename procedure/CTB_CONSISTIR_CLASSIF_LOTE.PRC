create or replace
procedure ctb_consistir_classif_lote(	nr_lote_contabil_p	number,
					nm_usuario_p		varchar2,
					ds_erro_p	out	varchar2) is 

cd_classificacao_w			varchar2(40);
ds_consistencia_w			varchar2(255);

cursor C01 is
select	nr_sequencia,
	dt_movimento,
	cd_conta_debito,
	cd_conta_credito,
	cd_classif_debito,
	cd_classif_credito
from	ctb_movimento
where	nr_lote_contabil = nr_lote_contabil_p;

vet01	C01%RowType;

begin

/* Consistir a Classifica��o das Contas Cont�beis */

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	ds_consistencia_w	:= '';
	
	/*D�bito*/
	if	(nvl(vet01.cd_conta_debito,'X') <> 'X') then
		begin
		
		if	(nvl(vet01.cd_classif_debito,'X') = 'X') then
			ds_consistencia_w		:= WHEB_MENSAGEM_PCK.get_texto(279062);
			ds_erro_p			:= ds_consistencia_w;
		else
			begin
			cd_classificacao_w	:= ctb_obter_classif_conta(vet01.cd_conta_debito, vet01.cd_classif_debito, vet01.dt_movimento);
			if	(nvl(cd_classificacao_w,'X') <> vet01.cd_classif_debito) then
				ds_consistencia_w	:= WHEB_MENSAGEM_PCK.get_texto(279063);
				ds_erro_p		:= ds_consistencia_w;
			end if;
			end;
		end if;
		
		end;
	end if;
	
	/*Cr�dito*/
	if	(nvl(vet01.cd_conta_credito,'X') <> 'X') then
		begin
		if	(nvl(vet01.cd_classif_credito,'X') = 'X') then
			ds_consistencia_w	:=  WHEB_MENSAGEM_PCK.get_texto(279062);
			ds_erro_p			:= ds_consistencia_w;
		else
			begin
			cd_classificacao_w	:= ctb_obter_classif_conta(vet01.cd_conta_credito, null, vet01.dt_movimento);
			if	(nvl(cd_classificacao_w,'X') <> vet01.cd_classif_credito) then
				ds_consistencia_w	:= WHEB_MENSAGEM_PCK.get_texto(279063);
				ds_erro_p		:= ds_consistencia_w;
			end if;
			end;
		end if;	
		end;
	end if;
	
	/*Atualizar consist�ncia*/
	if	(ds_consistencia_w is not null) then
		update	ctb_movimento
		set	ds_consistencia	= ds_consistencia_w
		where	nr_sequencia	= vet01.nr_sequencia;
	end if;
	
	end;
end loop;
close C01;

end ctb_consistir_classif_lote;
/