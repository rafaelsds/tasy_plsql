CREATE OR REPLACE PROCEDURE COPY_LABTEST_DATA 
(
  DATA_P IN VARCHAR2 
, SESSION_ID_P IN   VARCHAR2 DEFAULT NULL 
, RECORD_TYPE_P IN VARCHAR2 
) AS 
BEGIN

 IF (RECORD_TYPE_P = 'LAB_TEST') THEN
 INSERT INTO clipboard_data (
            nr_sequencia,
            cd_estabelecimento,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            ie_record_type,
            ie_clip_data,
            ie_situacao,
            id_sessao
        ) VALUES (
            clipboard_data_seq.NEXTVAL,
           wheb_usuario_pck.get_cd_estabelecimento,
            sysdate,
            wheb_usuario_pck.get_nm_usuario,
            sysdate,
          wheb_usuario_pck.get_nm_usuario,
            RECORD_TYPE_P,
            DATA_P,
            'A',
            SESSION_ID_P);
 
 END IF;
 commit;
END COPY_LABTEST_DATA;
/
