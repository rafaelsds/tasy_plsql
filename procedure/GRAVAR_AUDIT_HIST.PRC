create or replace
procedure  gravar_audit_hist(	ie_tipo_item_p		varchar2,
				nr_seq_audit_proc_p	number,
				nr_seq_audit_mat_p	number,
				vl_item_p		number,
				nr_conta_origem_p	number,
				nr_seq_audit_origem_p	number,
				nm_usuario_p		Varchar2) is 

ie_Gerar_Pendencia_w		varchar2(1);	
cd_estabelecimento_w		number(4,0);	
nr_seq_auditoria_w		number(10,0);		
nr_seq_pend_w			number(10,0);
nr_atendimento_w		number(10,0);
qt_reg_hist_w			number(10,0);
nr_interno_conta_w		number(10,0);
				
begin

insert into audit_hist_transferencia
		(nr_sequencia, 
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_item,
		nr_seq_audit_mat,
		nr_seq_audit_proc,
		vl_item,
		nr_conta_origem,
		nr_seq_audit_origem)
	values 	(audit_hist_transferencia_seq.nextVal,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_tipo_item_p,
		nr_seq_audit_mat_p,
		nr_seq_audit_proc_p,
		vl_item_p,
		nr_conta_origem_p,
		nr_seq_audit_origem_p);
		

nr_seq_auditoria_w:= 0;
		
if	(ie_tipo_item_p = 'M') then
	select 	nvl(max(nr_seq_auditoria),0)
	into	nr_seq_auditoria_w
	from 	auditoria_matpaci
	where 	nr_sequencia = nr_seq_audit_mat_p;
elsif	(ie_tipo_item_p = 'P') then
	select 	nvl(max(nr_seq_auditoria),0)
	into	nr_seq_auditoria_w
	from 	auditoria_propaci
	where 	nr_sequencia = nr_seq_audit_proc_p;
end if;

select 	max(cd_estabelecimento),
	max(nr_atendimento)
into	cd_estabelecimento_w,
	nr_atendimento_w
from 	conta_paciente
where 	nr_interno_conta = nr_conta_origem_p;

ie_Gerar_Pendencia_w 	:= nvl(obter_valor_param_usuario(1116, 136, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');

if	(ie_Gerar_Pendencia_w = 'V') and (nr_seq_auditoria_w > 0) then
	
	update	auditoria_conta_paciente
	set	vl_auditoria_orig =  Obter_valor_Orig_Audit(nr_sequencia)
	where	nr_sequencia = nr_seq_auditoria_w;
	
	select 	nvl(max(nr_sequencia),0),
		nvl(max(nr_interno_conta),0)
	into	nr_seq_pend_w,
		nr_interno_conta_w
	from 	cta_pendencia
	where 	nr_seq_auditoria = nr_seq_auditoria_w
	and 	nr_atendimento = nr_atendimento_w;
	
	select 	count(*)
	into	qt_reg_hist_w
	from  	cta_pendencia_hist
	where 	nr_seq_pend = nr_seq_pend_w;
	
	if	(nr_seq_pend_w > 0) and (qt_reg_hist_w = 1) then
		
		update	cta_pendencia_hist
		set 	vl_auditoria	 =  nvl(Obter_valor_Orig_Audit(nr_seq_auditoria_w),0),
			vl_conta_estagio =  nvl(obter_valor_conta(nr_interno_conta_w,0),0)
		where	nr_seq_pend = nr_seq_pend_w;
		
	end if;
	
end if;

commit;

end gravar_audit_hist;
/