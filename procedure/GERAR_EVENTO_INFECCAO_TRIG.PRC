create or replace
procedure gerar_evento_infeccao_trig(	nm_usuario_p			varchar2,
					nr_atendimento_p		number,
					cd_estabelecimento_p		number,
					cd_topografia_p			number,
					cd_caso_infeccao_p		number,
					cd_clinica_p			number,
					nr_seq_evento_infeccao_p number) is 

			
nr_seq_evento_w			number(10);
cd_setor_atendimento_w		number(10);
ds_desc_tmp_w			varchar2(255);
mensagem_w			varchar2(1000) := '';

begin

select	max(nr_seq_evento_infeccao)
into	nr_seq_evento_w
from	cih_parametros
where	cd_caso_infeccao = cd_caso_infeccao_p
and	nr_seq_evento_infeccao = nr_seq_evento_infeccao_p
and	(cd_estabelecimento = cd_estabelecimento_p
or	(cd_estabelecimento is null 
and not exists (select	1
		from	cih_parametros
		where	cd_estabelecimento = cd_estabelecimento_p)));

select	obter_setor_atendimento(nr_atendimento_p)
into	cd_setor_atendimento_w
from	dual;

if	(cd_topografia_p is not null) then

	ds_desc_tmp_w := null;

	select	obter_cih_topografia(cd_topografia_p)
	into	ds_desc_tmp_w
	from 	dual;
	
	if	(ds_desc_tmp_w is not null) then
		mensagem_w	:= mensagem_w || chr(13) || wheb_mensagem_pck.get_texto(797021) ||' : ' || ds_desc_tmp_w;
	end if;

end if;

if	(cd_caso_infeccao_p is not null) then

	ds_desc_tmp_w := null;

	select	obter_desc_caso_infeccao(cd_caso_infeccao_p)
	into	ds_desc_tmp_w
	from 	dual;
	
	if	(ds_desc_tmp_w is not null) then
		mensagem_w	:= mensagem_w || chr(13) || wheb_mensagem_pck.get_texto(797022) ||' : ' || ds_desc_tmp_w;
	end if;
	
end if;


if	(cd_clinica_p is not null) then

	ds_desc_tmp_w := null;

	select	obter_nome_clinica(cd_clinica_p)
	into	ds_desc_tmp_w
	from 	dual;
	
	if	(ds_desc_tmp_w is not null) then
		mensagem_w	:= mensagem_w || chr(13) || wheb_mensagem_pck.get_texto(797023) ||' : ' || ds_desc_tmp_w;
	end if;
	
end if;


if	(nr_seq_evento_w is not null) then
insert into qua_evento_paciente
	(
	nr_sequencia,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	nr_atendimento,
	nr_seq_evento,
	dt_evento,
	ds_evento,
	cd_setor_atendimento,
	dt_cadastro,
	nm_usuario_origem,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nm_usuario_reg,
	nr_seq_classif_evento,
	dt_liberacao,
	cd_pessoa_fisica,
	ie_status,
	ie_origem,
	ie_situacao,
	cd_funcao_ativa
	)
values
	(
	qua_evento_paciente_seq.nextval,
	cd_estabelecimento_p,
	sysdate,
	nm_usuario_p,
	nr_atendimento_p,
	nr_seq_evento_w,
	sysdate,
	wheb_mensagem_pck.get_texto(797027) ||' ' || mensagem_w,
	cd_setor_atendimento_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	null,
	sysdate,
	obter_pessoa_atendimento(nr_atendimento_p,'C'),
	'1',
	'S',
	'A',
	obter_funcao_ativa
	);
end if;

end gerar_evento_infeccao_trig;
/
