CREATE OR REPLACE 
procedure agend_executado_list_esp	(nr_seq_agenda_p		number,
									 nm_usuario_p			varchar2,
									 ie_reverte_p			varchar2 default 'N') is

qt_registros_w					number(10);
cnpj_solicitante_origem_w  		agenda_integrada.cnpj_solicitante%type;
nr_seq_lista_espera_origem_w	agenda_integrada_item.nr_seq_lista_espera_origem%type;
nr_sequencia_agen_list_esp_w  	agenda_lista_espera.nr_sequencia%type;
cd_expressao_w					number(10) default 1099049;

begin

	select	count(*)
	into	qt_registros_w
	from	agenda_integrada_item
	where	(nr_seq_agenda_cons = nr_seq_agenda_p and nr_seq_agenda_exame is null)
	or		(nr_seq_agenda_exame = nr_seq_agenda_p and nr_seq_agenda_cons is null);

	if (qt_registros_w > 0) then
		begin

			select	max(ai.cnpj_solicitante),
					max(aii.nr_seq_lista_espera_origem)
			into	cnpj_solicitante_origem_w,
					nr_seq_lista_espera_origem_w
			from	agenda_integrada_item	aii,
					agenda_integrada		ai
			where	((aii.nr_seq_agenda_cons = nr_seq_agenda_p and aii.nr_seq_agenda_exame is null)
			or		(aii.nr_seq_agenda_exame = nr_seq_agenda_p and aii.nr_seq_agenda_cons is null))
			and		ai.nr_sequencia			=	aii.nr_seq_agenda_int;

			if (cnpj_solicitante_origem_w is not null) and (nr_seq_lista_espera_origem_w is not null) then

				select  max(nr_sequencia)
				into	nr_sequencia_agen_list_esp_w
				from    agenda_lista_espera
				where   cnpj_origem = cnpj_solicitante_origem_w
				and     nr_seq_lista_espera_origem = nr_seq_lista_espera_origem_w;	

			else

				select	max(ai.nr_seq_lista_espera)
				into	nr_seq_lista_espera_origem_w
				from	agenda_integrada_item	aii,
						agenda_integrada		ai
				where	((aii.nr_seq_agenda_cons = nr_seq_agenda_p and aii.nr_seq_agenda_exame is null)
				or		(aii.nr_seq_agenda_exame = nr_seq_agenda_p and aii.nr_seq_agenda_cons is null))
				and		ai.nr_sequencia			=	aii.nr_seq_agenda_int;

				if ( nr_seq_lista_espera_origem_w is not null) then

					select  max(nr_sequencia)
					into	nr_sequencia_agen_list_esp_w
					from    agenda_lista_espera
					where   nr_sequencia = nr_seq_lista_espera_origem_w
					and		nvl(ie_integracao,'N') = 'N';

				end if;
			end if;

			if ( nr_sequencia_agen_list_esp_w is not null) then
				begin

					if (ie_reverte_p = 'S') then
						cd_expressao_w		:= 1098955;
					end if;

					update  agenda_lista_espera
					set     dt_realizacao_agenda = decode(ie_reverte_p, 'S', null, sysdate)
					where   nr_sequencia = nr_sequencia_agen_list_esp_w;

					insert into ag_lista_espera_contato(
						nr_sequencia,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario,
						nm_usuario_nrec,
						nr_seq_lista_espera,
						dt_contato,
						ie_origem,
						nm_pessoa_contato,
						nr_seq_situacao_atual,
						ds_observacao,
						dt_liberacao,
						nm_usuario_liberacao,
						ie_contato_realizado
					) values(
						ag_lista_espera_contato_seq.nextval,
						sysdate,
						sysdate,
						nm_usuario_p,
						nm_usuario_p,
						nr_sequencia_agen_list_esp_w,
						sysdate,
						'M',
						obter_pessoa_fisica_usuario(nm_usuario_p, null),
						'',
						obter_expressao_dic_objeto(cd_expressao_w),
						sysdate,
						nm_usuario_p,
						'N'
					);
				end;		
			end if;
		end;
	end if;
end agend_executado_list_esp;
/