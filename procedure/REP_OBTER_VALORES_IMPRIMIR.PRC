create or replace
procedure rep_obter_valores_imprimir(
		nr_prescricao_p			number,
		cd_pessoa_fisica_p		varchar2,
		ie_recebeu_alta_p out		varchar2,
		ie_exibe_proced_APH_p out	varchar2,
		ie_exibe_proced_APC_p out	varchar2,
		ie_exibe_proced_AP_p out	varchar2,
		ie_exibe_proced_BS_p out	varchar2,
		nr_seq_autor_p out		number,
		ds_lista_relat_p out		varchar2,
		ds_relat_regra_exame_p out	varchar2,
		nr_seq_avaliacao_p	out	number) is

ie_recebeu_alta_w	varchar2(1);
ie_exibe_proced_APH_w	varchar2(1);
ie_exibe_proced_APC_w	varchar2(1);
ie_exibe_proced_AP_w	varchar2(1);
ie_exibe_proced_BS_w	varchar2(1);
nr_seq_autor_w		number(10,0);
ds_lista_relat_w	varchar2(255);
ds_relat_regra_exame_w	varchar2(2000);
nr_seq_avaliacao_w	number(10,0);
begin
if	(nr_prescricao_p is not null) and
	(cd_pessoa_fisica_p is not null) then
	begin
	select	decode(count(*),0,'S','N')
	into	ie_recebeu_alta_w
	from	atendimento_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and		dt_alta is null;
	
	SELECT  NVL(MAX(nr_sequencia),0) 
	into	nr_seq_avaliacao_w
	FROM 	med_avaliacao_paciente 
	WHERE 	nr_prescricao = nr_prescricao_p 
	AND 	ie_situacao = 'A';
	
	
	if	(ie_recebeu_alta_w = 'N') then
		begin
		
		select	decode(count(*),0,'N','S')
		into	ie_exibe_proced_APH_w
		from	prescr_procedimento
		where	nr_prescricao	= nr_prescricao_p
		and	obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'APH') = 'S';
		
		select	decode(count(*),0,'N','S')
		into	ie_exibe_proced_APC_w
		from	prescr_procedimento
		where	nr_prescricao	= nr_prescricao_p
		and	obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'APC') = 'S';
		
		select	decode(count(*),0,'N','S')
		into	ie_exibe_proced_AP_w
		from	prescr_procedimento
		where	nr_prescricao	= nr_prescricao_p
		and	obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'AP') = 'S';
		
		select	decode(count(*),0,'N','S')
		into	ie_exibe_proced_BS_w
		from	prescr_procedimento
		where	nr_prescricao	= nr_prescricao_p
		and	(obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'BSHE') = 'S'
		or	obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'BSST') = 'S');
		
		select	max(nr_sequencia)
		into	nr_seq_autor_w
		from	autorizacao_convenio
		where	nr_prescricao	= nr_prescricao_p;
		
		ds_lista_relat_w	:= substr(rep_obter_todos_relat_regra,1,255);
		
		ds_relat_regra_exame_w	:= rep_obter_relatorio_regra_imp(nr_prescricao_p);
		end;
	end if;
	end;
end if;
commit;
ie_recebeu_alta_p	:= ie_recebeu_alta_w;
ie_exibe_proced_APH_p	:= ie_exibe_proced_APH_w;
ie_exibe_proced_APC_p	:= ie_exibe_proced_APC_w;
ie_exibe_proced_AP_p	:= ie_exibe_proced_AP_w;
ie_exibe_proced_BS_p	:= ie_exibe_proced_BS_w;
nr_seq_autor_p		:= nr_seq_autor_w;
ds_lista_relat_p	:= ds_lista_relat_w;
ds_relat_regra_exame_p	:= ds_relat_regra_exame_w;
nr_seq_avaliacao_p	:= nr_seq_avaliacao_w;
end rep_obter_valores_imprimir;
/