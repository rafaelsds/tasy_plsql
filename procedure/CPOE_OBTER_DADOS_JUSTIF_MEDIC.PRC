create or replace
procedure CPOE_Obter_Dados_Justif_Medic(
					nr_atendimento_p			IN 	number,
					cd_material_p				IN	material.cd_material%type,
					cd_setor_prescr_p			IN	number,
					cd_estabelecimento_p		IN	number,
					cd_setor_atendimento_p		IN	setor_atendimento.cd_setor_atendimento%type,
					cd_prescritor_p				IN	number,
					ie_via_aplicacao_p			IN	material_prescr.ie_via_aplicacao%type,
					cd_pessoa_fisica_p			IN	pessoa_fisica.cd_pessoa_fisica%type,
					cd_intervalo_p				IN	varchar2,
					ie_se_necessario_p			IN	varchar2,
					ie_solucao_p				IN 	varchar2,
					ie_considerar_peso_p		IN	varchar2,
					qt_dose						IN	material.qt_limite_pessoa%type,
					ie_bomba_infusao_p		IN 	cpoe_material.ie_bomba_infusao%type,
					ie_exige_justificativa_p	OUT	material_prescr.ie_exige_justificativa%type,
					ie_justificativa_padrao_p	OUT material_prescr.ie_justificativa_padrao%type,
					ds_justificativa_p 			OUT varchar2
				) IS

nr_seq_agrupamento_w		number(10,0);
qt_idade_w					number(10,0);
qt_idade_dia_w				number(15,2);
qt_idade_mes_w				number(15,2);
qt_peso_w					number(6,3);
qt_regra					number;
ie_exige_justificativa_w	material_prescr.ie_exige_justificativa%type;
ie_justificativa_padrao_w	material_prescr.ie_justificativa_padrao%type;

begin

select	nvl(max('S'),'N')
into	ie_exige_justificativa_p
from	regra_prescr_mat_justi
where	cd_material = cd_material_p
and 	nvl(ie_bomba_infusao,nvl(ie_bomba_infusao_p,'XPTO')) = nvl(ie_bomba_infusao_p,'XPTO')
and	nvl(cd_setor_atendimento, cd_setor_atendimento_p) = cd_setor_atendimento_p
and	((obter_sinal_vital(nr_atendimento_p, 'Peso') between nvl(qt_peso_inicial,0) and nvl(qt_peso_final,999)) or ((qt_peso_inicial is null) and (qt_peso_final is null)))
and	nvl(cd_perfil, obter_perfil_ativo) = obter_perfil_ativo
and	nvl(cd_convenio, obter_convenio_atendimento(nr_atendimento_p)) = obter_convenio_atendimento(nr_atendimento_p)
and	ie_situacao = 'A';

select	decode(ie_exige_justificativa_p, 'N', nvl(max(ie_obriga_justificativa), 'N'), ie_exige_justificativa_p),
	nvl(max(ie_justificativa_padrao), 'N')
into	ie_exige_justificativa_p,
	ie_justificativa_padrao_p
from	material
where 	cd_material = cd_material_p;

select
	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DI')),
	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'MM')),
	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'A'))
into
	qt_idade_dia_w,
	qt_idade_mes_w,
	qt_idade_w
from pessoa_fisica b
where b.cd_pessoa_fisica = cd_pessoa_fisica_p;

select	max(obter_sinal_vital(nr_atendimento_p,'Peso'))
into	qt_peso_w
from	dual;

if (ie_exige_justificativa_p = 'N' OR ie_justificativa_padrao_p = 'N') then
	begin

	select	count(*)
	into	qt_regra
	from	material_prescr
	where	cd_material	= cd_material_p;

	if  (qt_regra > 0) then
		begin
		
		select	max(nr_seq_agrupamento)
		into	nr_seq_agrupamento_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_p;

		-- Obtendo o valor da regra
		select	nvl(max(ie_exige_justificativa),'N'), nvl(max(ie_justificativa_padrao), 'N')
		into	ie_exige_justificativa_w, ie_justificativa_padrao_w
		from	material_prescr
		where	nvl(cd_setor_atendimento, nvl( cd_setor_prescr_p,0))	= nvl( cd_setor_prescr_p,0)
		and		cd_material		= cd_material_p
		and		nvl(cd_estabelecimento, cd_estabelecimento_p ) = cd_estabelecimento_p
		and		nvl(cd_setor_atendimento, nvl( cd_setor_atendimento_p ,0)) = nvl( cd_setor_atendimento_p,0)
		and		nvl(ie_via_aplicacao, nvl( ie_via_aplicacao_p,0)) = nvl( ie_via_aplicacao_p,0)
		and		nvl(cd_intervalo_filtro, nvl(cd_intervalo_p,0)) = nvl(cd_intervalo_p,0)
		and		nvl(ie_somente_sn, nvl(ie_se_necessario_p,'N')) = nvl(ie_se_necessario_p,'N')
		and		qt_idade_dia_w between nvl(qt_idade_min_dia,0) and nvl(qt_idade_max_dia,55000)
		and		qt_idade_mes_w between nvl(qt_idade_min_mes,0) and nvl(qt_idade_max_mes,55000)
		and		((nr_seq_agrupamento is null) or (nr_seq_agrupamento = nr_seq_agrupamento_w ))
		and		nvl(qt_idade_w,0) between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
		and		(ie_considerar_peso_p = 'N' 
					or (nvl(qt_peso_w,0) between nvl(qt_peso_min,0) and nvl(qt_peso_max,999)))
		and		Obter_se_setor_regra_prescr( nr_sequencia, cd_setor_atendimento_p ) = 'S'
		and		((cd_especialidade is null) or
				 (obter_se_especialidade_medico( cd_prescritor_p, cd_especialidade ) = 'S'));
		
		if (ie_exige_justificativa_p = 'N') then
			if (ie_exige_justificativa_w = 'N') then
				begin
				
				select nvl(max(ie_obriga_just_dose), 'N')
				into ie_exige_justificativa_p
				from material_prescr
				where cd_material = cd_material_p 
				and nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
				and ie_obriga_just_dose = 'S'
				and qt_limite_pessoa IS NOT NULL
				and qt_limite_pessoa < qt_dose;
				
				end;
			else
				ie_exige_justificativa_p := ie_exige_justificativa_w;
			end if;
		end if;
		
		if (ie_justificativa_padrao_p = 'N') then
			ie_justificativa_padrao_p := ie_justificativa_padrao_w;
		end if;
		
		end;
	end if;
	
	end;
end if;

ds_justificativa_p := Obter_Padrao_Param_Prescr(nr_atendimento_p, cd_material_p, ie_via_aplicacao_p, cd_setor_atendimento_p, cd_pessoa_fisica_p, qt_idade_w, qt_peso_w, ie_se_necessario_p, 'J', cd_intervalo_p, ie_solucao_p);

end CPOE_Obter_Dados_Justif_Medic;
/