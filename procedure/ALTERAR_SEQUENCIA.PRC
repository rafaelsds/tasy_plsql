create or replace
procedure alterar_sequencia(	nr_prescricao_p		number,
				nr_agrupamento_p	number,
				nr_seq_material_p	number,
				ie_item_superior_p	varchar2,
				nm_usuario_p		varchar2,
				nr_agrup_ant_p		number,
				cd_perfil_p		number,
				cd_estabelecimento_p	varchar2) is 

ie_volta_horario_w	varchar2(10) := '';
ie_item_superior_w	varchar2(10) := ie_item_superior_p;
ds_horarios_w		varchar2(50) := '';			
ie_recalcular_hora_w	varchar2(1)  := '';
ds_horarios_ww		varchar2(4000) := '';

				
begin

Obter_Param_Usuario(7010, 106, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_recalcular_hora_w);

if	(nr_prescricao_p is not null) and
	(nr_agrupamento_p is not null) and
	(nr_seq_material_p is not null) then
	begin
	
	select	decode(count(*),0,'S','N')
	into	ie_volta_horario_w
	from    prescr_material a
	where	nr_prescricao	= nr_prescricao_p
	and	nr_agrupamento	= nr_agrupamento_p
	and	obter_classif_material_proced(a.cd_material, null,null) <> 1
	and	a.nr_sequencia_solucao	is null
	and	a.nr_sequencia_proc	is null
	and	a.ie_agrupador	= 1
	and	a.ie_suspenso	<> 'S'
	and	nvl(ie_administrar,'S') = 'S'
	and	a.nr_sequencia_diluicao	is null
	and     nr_sequencia <> nr_seq_material_p;
	
	ajustar_item_superior_agrup(nr_prescricao_p,
				    nr_seq_material_p,
				    nr_agrupamento_p,
				    ie_item_superior_w,
				    'F',
				    '',
				    nm_usuario_p);
				    
	update prescr_material
	set    nr_agrupamento = nr_agrupamento_p
	where  nr_prescricao  = nr_prescricao_p
	and    nr_sequencia   = nr_seq_material_p;
		
	--ie_recalcular_hora_w := 'S';  O par�metro que tem que controlar isto. OS 781604
	
	if (ie_recalcular_hora_w = 'S') then	
	
		atualizar_horarios_agrup_medic( nr_prescricao_p,
						nr_seq_material_p,
						nr_agrupamento_p,
						nr_agrup_ant_p,
						'F',
						'',
						'',
						ds_horarios_w,
						cd_estabelecimento_p,
						ie_volta_horario_w,
						nm_usuario_p);
	end if;
	
	end;
end if;

commit;

end alterar_sequencia;
/
