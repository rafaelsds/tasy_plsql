create or replace
procedure atualizar_item_cotacao_ordem( nr_ordem_compra_p number,
                                        nr_item_oci_p number,
                                        nr_ordem_transf_p number,
                                        nm_usuario_p varchar2) is

nr_cot_compra_w         ordem_compra_item.nr_cot_compra%type;
nr_item_cot_compra_w    ordem_compra_item.nr_item_cot_compra%type;
cd_material_w           ordem_compra_item.cd_material%type;
ds_material_w           material.ds_material%type;
ds_erro_w               varchar2(2000) := '';

begin

select nr_cot_compra,
       nr_item_cot_compra, 
       cd_material
into   nr_cot_compra_w,
       nr_item_cot_compra_w,
       cd_material_w
from   ordem_compra_item
where  nr_ordem_compra = nr_ordem_compra_p
and    nr_item_oci = nr_item_oci_p;

ds_material_w := substr(obter_desc_material(cd_material_w),1,255);

begin

update cot_compra_forn_item 
set    nr_ordem_compra = nr_ordem_transf_p
where  nr_cot_compra = nr_cot_compra_w
and    nr_item_cot_compra = nr_item_cot_compra_w
and    nr_ordem_compra = nr_ordem_compra_p;

insert into cot_compra_hist(
        nr_sequencia,
        nr_cot_compra,
        dt_atualizacao,
        nm_usuario,
        dt_historico,
        ds_titulo,
        ds_historico,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        ie_origem,
        dt_liberacao )
values( cot_compra_hist_seq.nextval,
        nr_cot_compra_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        substr(WHEB_MENSAGEM_PCK.get_texto(1155362),1,80),
        substr(WHEB_MENSAGEM_PCK.get_texto(1155365,'NR_ITEM_COT_COMPRA_W='||nr_item_cot_compra_w||
                                ';CD_MATERIAL_W='||cd_material_w||';DS_MATERIAL_W='||ds_material_w||
                                ';NR_ORDEM_COMPRA_P='||nr_ordem_compra_p||';NR_ORDEM_TRANSF_P='||nr_ordem_transf_p),1,4000),
        sysdate,
        nm_usuario_p,
        'S',
        sysdate );

exception when others then

ds_erro_w := substr(sqlerrm,1,3000);

inserir_historico_ordem_compra( nr_ordem_transf_p,
                                'S',
                                WHEB_MENSAGEM_PCK.get_texto(1155366),
                                substr(WHEB_MENSAGEM_PCK.get_texto(1155368,'DS_ERRO_W='||ds_erro_w||
                                                ';NR_ITEM_COT_COMPRA_W='||nr_item_cot_compra_w||
                                                ';NR__COT_COMPRA_W='||nr_cot_compra_w),1,4000),
                                nm_usuario_p );

end;

commit;

end atualizar_item_cotacao_ordem;
/
