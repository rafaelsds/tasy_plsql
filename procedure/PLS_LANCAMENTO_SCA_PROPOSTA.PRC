create or replace
procedure pls_lancamento_sca_proposta
			(	nr_seq_proposta_benef_p		number,
				nm_usuario_p			Varchar2,
				ie_commit_p			varchar2) is 
			
nr_seq_plano_w			number(10);
nr_seq_lanc_automotico_w	number(10);
nr_seq_regra_item_w		number(10);
nr_seq_sca_w			number(10);
nr_seq_tabela_w			number(10);
qt_registros_w			number(10);
nr_seq_simulacao_w		number(10);
nr_seq_tabela_perfil_w		number(10);
dt_inicio_proposta_w		date;
			
Cursor C01 is
	select	nr_sequencia
	from	pls_regra_lanc_automatico
	where	ie_evento	= 13
	and	((nr_seq_grupo_produto is not null and substr(pls_se_grupo_preco_produto(nr_seq_grupo_produto,nr_seq_plano_w),1,255) = 'S') or
		 (nr_seq_grupo_produto is null))
	and	((nr_seq_plano	is not null and	nr_seq_plano = nr_seq_plano_w) or
		 (nr_seq_plano	is null))
	and	ie_situacao	= 'A'
	and	dt_inicio_proposta_w between nvl(dt_inicio_vigencia,dt_inicio_proposta_w) and nvl(dt_fim_vigencia,dt_inicio_proposta_w);
	
Cursor C02 is
	select	nr_seq_sca,
		nr_seq_tabela_sca
	from	pls_regra_lanc_aut_item
	where	nr_seq_regra	= nr_seq_lanc_automotico_w
	and	nr_seq_sca is not null
	and	ie_situacao	= 'A';

begin

select	b.nr_seq_plano,
	nvl(b.dt_contratacao,a.dt_inicio_proposta)
into	nr_seq_plano_w,
	dt_inicio_proposta_w
from	pls_proposta_beneficiario	b,
	pls_proposta_adesao		a
where	b.nr_seq_proposta	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_proposta_benef_p;

if	(nr_seq_plano_w is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_lanc_automotico_w;
	exit when C01%notfound;
		begin
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_sca_w,
			nr_seq_tabela_w;
		exit when C02%notfound;
			begin
			
			select	count(1)
			into	qt_registros_w
			from	pls_sca_vinculo
			where	NR_SEQ_BENEF_PROPOSTA	= nr_seq_proposta_benef_p
			and	nr_seq_plano		= nr_seq_sca_w;
			
			if	(qt_registros_w = 0) then
				insert into pls_sca_vinculo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					NR_SEQ_BENEF_PROPOSTA,nr_seq_plano,nr_seq_tabela,ds_observacao,DT_INICIO_VIGENCIA)
				values
				(	pls_sca_vinculo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_proposta_benef_p,nr_seq_sca_w,nr_seq_tabela_w,'Gerada a partir do lançamento automático',dt_inicio_proposta_w);
			end if;		
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;
end if;	

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_lancamento_sca_proposta;
/
