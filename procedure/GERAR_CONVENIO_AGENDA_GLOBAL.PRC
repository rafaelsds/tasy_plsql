create or replace
procedure gerar_convenio_agenda_global	(cd_pessoa_fisica_p	varchar2,
						cd_tipo_agenda_p	number,
						nr_seq_agenda_p	number,
						ie_forma_convenio_p	varchar2,
						nm_usuario_p		varchar2) is

cd_convenio_w			number(5,0);
cd_categoria_w		varchar2(10);
cd_usuario_convenio_w	varchar2(30);
dt_validade_w			date;
nr_doc_convenio_w		varchar2(20);
cd_tipo_acomodacao_w		number(4,0);
cd_plano_w			varchar2(10);
ds_observacao_w		varchar2(4000);

begin
if	(cd_pessoa_fisica_p is not null) and
	(cd_tipo_agenda_p is not null) and
	(nr_seq_agenda_p is not null) and
	(ie_forma_convenio_p is not null) and
	(nm_usuario_p is not null) then
	/* obter dados convenio */
	gerar_convenio_agendamento(cd_pessoa_fisica_p, cd_tipo_agenda_p, nr_seq_agenda_p, ie_forma_convenio_p, cd_convenio_w, cd_categoria_w, cd_usuario_convenio_w, dt_validade_w, nr_doc_convenio_w, cd_tipo_acomodacao_w, cd_plano_w, nm_usuario_p, ds_observacao_w);

	/* gerar dados convenio */
	if	(cd_convenio_w is not null) then
		update	agenda_paciente
		set	cd_convenio		= cd_convenio_w,
			cd_categoria		= cd_categoria_w,
			cd_usuario_convenio	= cd_usuario_convenio_w,
			dt_validade_carteira	= dt_validade_w,
			nr_doc_convenio	= nr_doc_convenio_w,
			cd_tipo_acomodacao	= cd_tipo_acomodacao_w,
			cd_plano		= cd_plano_w,
			ds_observacao 	= ds_observacao_w
		where	nr_sequencia		= nr_seq_agenda_p;
	end if;
end if;

commit;

end gerar_convenio_agenda_global;
/