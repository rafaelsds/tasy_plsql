create or replace
procedure	cm_retorna_conj_pend_req(
			nr_requisicao_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

nr_seq_conj_real_w		number(10);

cursor	c01 is
select	a.nr_seq_conj_real
from	cm_requisicao_conj a,
	cm_requisicao_item b
where	a.nr_seq_item_req	= b.nr_sequencia
and	b.nr_seq_requisicao	= nr_requisicao_p
and	a.dt_retorno is null;	

begin

open	c01;
loop
fetch c01 into
	nr_seq_conj_real_w;
exit when c01%notfound;
	begin

	cme_gerar_retorno_item(nr_requisicao_p,nr_seq_conj_real_w,nm_usuario_p,cd_estabelecimento_p);

	end;
end loop;
close c01;

end cm_retorna_conj_pend_req;
/