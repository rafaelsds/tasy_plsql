create or replace
procedure man_aprov_analise_impacto(	nr_seq_impacto_p	number,
				nm_usuario_p	Varchar2) is 
qt_reg_w				number(10);
		
ds_cabecalho_w			long;
ds_rodape_w			long;
	
ds_tabela_w			long;
ds_html_w			long;
ds_ccb_title_w 			varchar2(255);
ds_ccb_status_w			varchar2(255);
nr_seq_os_w			number(10);
nr_seq_tipo_hist_w			man_tipo_hist.nr_sequencia%type;
ds_titulo_impacto_w		man_ordem_serv_impacto.ds_titulo%type;

qt_prs_impacto_w			number(3);

nm_system_analyst_aprov_w 		varchar2(15);
nm_business_analyst_aprov_w 	varchar2(15);

Cursor C01 is
	select 	a.dt_aprovacao,
		a.dt_reprovacao,
		a.ie_status,
		a.nm_usuario_aprov,
		a.nm_usuario_reprov,
		nvl(a.dt_aprovacao,a.dt_reprovacao) dt_registro,
		b.ds_equipe,
		obter_nome_usuario(nvl(a.nm_usuario_aprov,a.nm_usuario_reprov)) nm_pessoa,
		decode(a.ie_status, 'A', obter_desc_expressao_idioma(283709, 'Approved', 5), obter_desc_expressao_idioma(306137, 'Rejected', 5)) ds_status,
		decode(a.ie_status, 'A', null, a.ds_motivo_reprov)ds_motivo_reprov
	from	man_ordem_serv_aprov_ccb a,
		ccb_equipe b
	where	a.nr_seq_impacto = nr_seq_impacto_p
	and	a.nr_seq_equipe	 = b.nr_sequencia;
  
Cursor C02 is
	select 	b.nr_product_requirement nr_seq_pr,
		nvl(a.ie_clinico, 'N') ie_clinico
	from 	reg_product_requirement a,
		man_ordem_serv_imp_pr b
	where 	a.nr_sequencia = b.nr_product_requirement
	and 	b.ie_impacto_requisito in ('A', 'I')
	and 	b.nr_seq_impacto = nr_seq_impacto_p;

begin

select	count(1)
into	qt_reg_w
from	man_ordem_serv_aprov_ccb
where	nr_seq_impacto	= nr_seq_impacto_p
and	ie_status in ('R');

if	(qt_reg_w	= 0) then	
	select	max(a.nm_usuario_aprov)
	into	nm_system_analyst_aprov_w
	from	man_ordem_serv_aprov_ccb a,
		ccb_equipe b
	where	a.nr_seq_impacto = nr_seq_impacto_p
	and	b.nr_sequencia = a.nr_seq_equipe
	and	b.ie_tipo_equipe = 'SA'; -- Systems analyst
	
	if	(nm_system_analyst_aprov_w is not null or nm_business_analyst_aprov_w is not null) then	
		for r_c02 in c02 loop
			if (r_c02.ie_clinico = 'S') then
				reg_toggle_pr_manager_approval(r_c02.nr_seq_pr, nm_business_analyst_aprov_w, 'S');
			else
				reg_toggle_pr_manager_approval(r_c02.nr_seq_pr, nm_system_analyst_aprov_w, 'S');
			end if;
		end loop;
	end if;
	
	update	man_ordem_serv_impacto
	set	dt_aprovacao = sysdate
	where	nr_sequencia = nr_seq_impacto_p;

	man_atualiza_pr_title(nr_seq_impacto_p, nm_usuario_p);

	ds_ccb_status_w	:= obter_desc_expressao_idioma(283709, 'Approved', 5);
	nr_seq_tipo_hist_w := 185; -- CCB Accepted
else
	ds_ccb_status_w	:= obter_desc_expressao_idioma(306137, 'Rejected', 5);
	nr_seq_tipo_hist_w := 184; -- CCB Rejected
end if;

select	nr_seq_ordem_serv,
	ds_titulo
into	nr_seq_os_w,
	ds_titulo_impacto_w
from	man_ordem_serv_impacto
where	nr_sequencia = nr_seq_impacto_p;

select 	decode(count(1), 0, 'Technical Analysis', 'CCB')
into 	ds_ccb_title_w
from 	man_ordem_serv_imp_pr
where 	nr_seq_impacto = nr_seq_impacto_p
and 	ie_impacto_requisito in ('I', 'A', 'E');

ds_cabecalho_w	:= '<!DOCTYPE html>
<html>
<body><h2>' || ds_titulo_impacto_w || '</h2><h3>' || ds_ccb_title_w || ' Status: '||ds_ccb_status_w||
'</h3><br>

<table border="1" cellspacing="0" cellpadding="0" style="width:100%;border: 1px;">
<tbody align="left">
<tr>
<th>Approver</th>
<th>Role</th>
<th>Date</th>
<th>Status</th>
<th>Coments</th>
</tr>';

ds_rodape_w	:= '</tbody></table></body></html>';

for r_c01 in c01 loop
	begin
	ds_tabela_w	:= ds_tabela_w ||'<tr>'||
				 '<td>'|| r_c01.nm_pessoa ||'</td>'||
				 '<td>'|| r_c01.ds_equipe ||'</td>'||
				 '<td>'|| to_char(r_c01.dt_registro,'dd-Mon-yyyy', 'NLS_DATE_LANGUAGE=ENGLISH')  ||'</td>'||
				 '<td>'|| r_c01.ds_status ||'</td>'||
				 '<td>'|| r_c01.ds_motivo_reprov ||'</td>'||
				  '</tr>'||chr(10);
	end;
end loop;

if	(nr_seq_os_w	is not null) then

	ds_html_w	:= ds_cabecalho_w||ds_tabela_w||ds_rodape_w;
	insert into man_ordem_serv_tecnico(	nr_sequencia,
						nr_seq_ordem_serv,
						dt_atualizacao,
						nm_usuario,
						ds_relat_tecnico,
						dt_historico,
						dt_liberacao,
						ie_origem,
						nr_seq_tipo)
						
				values	 (	man_ordem_serv_tecnico_seq.nextval,
						nr_seq_os_w,
						sysdate,
						'Tasy',
						ds_html_w,
						sysdate,
						sysdate,
						'I',
						nr_seq_tipo_hist_w);

end if;
					
commit;

end man_aprov_analise_impacto;
/
