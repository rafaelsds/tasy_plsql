create or replace
procedure 	pda_inserir_itens_adic_nreq(	cd_material_p		Number,
											nr_seq_processo_p	Number,
											cd_barras_p			Varchar2,
											cd_unidade_medida_p	Varchar2,
											nr_seq_lote_p		Number,
											qt_material_p		Number,
											nm_usuario_p		Varchar2) is 

begin

if	(cd_material_p is not null) and
	(nr_seq_processo_p is not null) then
	
	insert into pda_processo_item_nreq( nr_sequencia,
										cd_barras,
										cd_material,
										cd_unidade_medida,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_baixa_estoque,
										ie_lista_material,
										nm_usuario,
										nm_usuario_nrec,
										nr_seq_horario,
										nr_seq_lote,
										nr_seq_material,
										nr_seq_processo,
										qt_material,
										qt_saldo)
								values( pda_processo_item_nreq_seq.nextval,
										cd_barras_p,
										cd_material_p,
										cd_unidade_medida_p,
										sysdate,
										sysdate,
										'S',
										'N',
										nm_usuario_p,
										nm_usuario_p,
										null,
										nr_seq_lote_p,
										null,
										nr_seq_processo_p,
										qt_material_p,
										qt_material_p);
								
	
end if;

commit;

end pda_inserir_itens_adic_nreq;
/