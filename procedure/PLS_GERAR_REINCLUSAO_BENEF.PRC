create or replace
procedure pls_gerar_reinclusao_benef
			(	nr_seq_reinclusao_p	in out	number,
				nr_seq_contrato_p		number,
				nr_seq_rescisao_p		number,
				nr_seq_solic_resc_benef_p	number,
				cd_estabelecimento_p		varchar2,
				cd_cep_p			varchar2,
				cd_municipio_ibge_p		varchar2,
				cd_tipo_logradouro_p		varchar2,
				ds_bairro_p			varchar2,
				ds_complemento_p		varchar2,
				ds_endereco_p			varchar2,
				ds_municipio_p			varchar2,
				ie_inclui_dependentes_p		varchar2,
				nm_pessoa_fisica_p		varchar2,
				nr_ddi_telefone_p		varchar2,
				nr_ddd_telefone_p		varchar2,
				nr_telefone_p			varchar2,
				nr_ddi_celular_p		varchar2,
				nr_ddd_celular_p		varchar2,
				nr_telefone_celular_p		varchar2,
				nr_endereco_p			varchar2,
				sg_estado_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_segurado_p		number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Gerar ou alterar a solicitacao de reinclusao de beneficiario
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  x ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_reinclusao_w		pls_reinclusao_benef.nr_sequencia%type;

begin

if 	(nr_seq_reinclusao_p is null) then
	select	pls_reinclusao_benef_seq.nextval
	into	nr_seq_reinclusao_w
	from	dual;
	
	insert into pls_reinclusao_benef(	nr_sequencia, ie_status, dt_solicitacao,
						nm_usuario_nrec, dt_atualizacao_nrec, nm_usuario,
						dt_atualizacao, cd_estabelecimento, nr_seq_segurado,
						nr_seq_contrato, nr_seq_rescisao, cd_cep,
						cd_municipio_ibge, cd_tipo_logradouro, ds_bairro,
						ds_complemento, ds_municipio, ie_inclui_dependentes,
						nm_pessoa_fisica, nr_ddi_telefone, nr_ddd_telefone,
						nr_telefone, nr_ddi_celular, nr_ddd_celular,
						nr_telefone_celular, nr_endereco_varchar, sg_estado,
						ds_endereco, nr_seq_solic_resc_benef, ie_criar_pagador)
				values	(	nr_seq_reinclusao_w, '2', sysdate,
						nm_usuario_p, sysdate, nm_usuario_p,
						sysdate, cd_estabelecimento_p, nr_seq_segurado_p,
						nr_seq_contrato_p, nr_seq_rescisao_p, cd_cep_p,
						cd_municipio_ibge_p, cd_tipo_logradouro_p, ds_bairro_p,
						ds_complemento_p, ds_municipio_p, nvl(ie_inclui_dependentes_p, 'N'),
						nm_pessoa_fisica_p, nr_ddi_telefone_p, nr_ddd_telefone_p,
						nr_telefone_p, nr_ddi_celular_p, nr_ddd_celular_p,
						nr_telefone_celular_p, nr_endereco_p, sg_estado_p,
						ds_endereco_p, nr_seq_solic_resc_benef_p, 'S');
	
	nr_seq_reinclusao_p	:= nr_seq_reinclusao_w;
else 
	update	pls_reinclusao_benef
	set	nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		cd_cep			= cd_cep_p,
		cd_municipio_ibge	= cd_municipio_ibge_p, 
		cd_tipo_logradouro	= cd_tipo_logradouro_p, 
		ds_bairro		= ds_bairro_p,
		ds_complemento		= ds_complemento_p,
		ds_municipio		= ds_municipio_p, 
		ds_endereco		= ds_endereco_p,
		ie_inclui_dependentes	= nvl(ie_inclui_dependentes_p, 'N'),
		nm_pessoa_fisica	= nm_pessoa_fisica_p, 
		nr_ddi_telefone		= nr_ddi_telefone_p, 
		nr_ddd_telefone		= nr_ddd_telefone_p,
		nr_telefone		= nr_telefone_p, 
		nr_ddi_celular		= nr_ddi_celular_p, 
		nr_ddd_celular		= nr_ddd_celular_p,
		nr_telefone_celular	= nr_telefone_celular_p, 
		nr_endereco_varchar	= nr_endereco_p, 
		sg_estado		= sg_estado_p
	where	nr_sequencia		= nr_seq_reinclusao_p;
	
	--Remove os beneficiarios dependentes cadastrados
	delete	from pls_reinc_dependentes
	where	nr_seq_reinc_benef = nr_seq_reinclusao_p;
end if;

commit;

end pls_gerar_reinclusao_benef;
/