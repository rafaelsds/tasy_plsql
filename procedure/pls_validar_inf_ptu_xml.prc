create or replace
procedure pls_validar_inf_ptu_xml(	nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_tipo_arquivo_p	w_pls_inconsistencia_int.ie_tipo_arquivo%type ) is

vl_total_fat_xml		pls_fat_guia_envio_proc.vl_beneficiario%type;
vl_tot_pls_fatura_w		pls_fatura.vl_fatura%type;
nr_seq_lote_fat_guia_w		pls_lote_fat_guia_envio.nr_sequencia%type;
ds_complemento_w		w_pls_inconsistencia_int.ds_complemento%type;
vl_total_fatura_w		ptu_fatura.vl_total_fatura%type := 0;
nr_seq_lote_fat_w		pls_lote_faturamento.nr_sequencia%type;
qt_conta_fora_xml_w		pls_integer;

-- FATURAS PTU
cursor c01 (	nr_seq_fatura_pc		pls_fatura.nr_sequencia%type) is
	select	a.nr_seq_pls_fatura,
		a.nr_sequencia,
		a.vl_total_fatura,
		a.vl_total_ndc
	from	ptu_fatura	a
	where	a.nr_seq_pls_fatura = nr_seq_fatura_pc;

-- buscar as contas que entraram na fatura XML
cursor c02 is
	select	distinct(b.nr_seq_conta)
	from	pls_fatura_conta	b,
		pls_fatura_evento	a
	where	a.nr_sequencia		= b.nr_seq_fatura_evento
	and	a.nr_seq_fatura		= nr_seq_fatura_p
	and not exists(
			select	1
			from	pls_fat_guia_arquivo 	x,
				pls_fatura_guia_envio 	z
			where	x.nr_sequencia 		= z.nr_seq_guia_arquivo
			and	x.nr_seq_lote 		= nr_seq_lote_fat_guia_w
			and	z.nr_seq_conta		= b.nr_seq_conta)
	and exists(	select	1
			from	pls_lote_fat_guia_envio d
			where	d.nr_seq_fatura		= nr_seq_fatura_p);

-- Cobranca PTU			
cursor c03 (	nr_seq_fatura_pc	ptu_fatura.nr_sequencia%type) is
	select	a.nr_sequencia,
		a.nr_seq_conta
	from	ptu_nota_cobranca	a
	where	a.nr_seq_fatura		= nr_seq_fatura_pc;
	
-- Procedimentos PTU
cursor c04 (	nr_seq_cobranca_pc		ptu_nota_cobranca.nr_sequencia%type,
		nr_seq_conta_pc			pls_conta.nr_sequencia%type) is
	select	t.nr_seq_conta_proc_mat,
		t.union_proc_mat
	from	(	-- PROC
			select	c.nr_seq_conta_proc	nr_seq_conta_proc_mat,
				'P' union_proc_mat
			from	pls_fatura_proc		c,
				pls_fatura_conta	b,
				pls_fatura_evento	a
			where	a.nr_sequencia		= b.nr_seq_fatura_evento
			and	c.nr_seq_fatura_conta	= b.nr_sequencia
			and	a.nr_seq_fatura		= nr_seq_fatura_p
			and	b.nr_seq_conta		= nr_seq_conta_pc
			and not exists(
					select	1
					from	ptu_nota_servico 	x,
						ptu_nota_cobranca 	z
					where	x.nr_seq_nota_cobr 	= z.nr_sequencia
					and	z.nr_sequencia		= nr_seq_cobranca_pc
					and	x.nr_seq_conta_proc	= c.nr_seq_conta_proc
					and	z.nr_seq_conta		= nr_seq_conta_pc)
			and exists(	select	1
					from	ptu_nota_cobranca	d
					where	d.nr_sequencia		= nr_seq_cobranca_pc)
			union all	
			-- MAT
			select	c.nr_seq_conta_mat	nr_seq_conta_proc_mat,
				'M' union_proc_mat
			from	pls_fatura_mat		c,
				pls_fatura_conta	b,
				pls_fatura_evento	a
			where	a.nr_sequencia		= b.nr_seq_fatura_evento
			and	c.nr_seq_fatura_conta	= b.nr_sequencia
			and	a.nr_seq_fatura		= nr_seq_fatura_p
			and not exists(
					select	1
					from	ptu_nota_servico 	x,
						ptu_nota_cobranca 	z
					where	x.nr_seq_nota_cobr 	= z.nr_sequencia
					and	z.nr_sequencia		= nr_seq_cobranca_pc
					and	x.nr_seq_conta_mat	= c.nr_seq_conta_mat
					and	z.nr_seq_conta		= nr_seq_conta_pc)
			and exists(	select	1
					from	ptu_nota_cobranca	d
					where	d.nr_sequencia		= nr_seq_cobranca_pc)) t;
begin

-- Obter valor total da fatura
select	nvl(max(a.vl_fatura), 0) + nvl(max(a.vl_total_ndc), 0)
into	vl_tot_pls_fatura_w
from	pls_fatura		a
where	a.nr_sequencia		= nr_seq_fatura_p;

select	max(a.nr_seq_lote)
into	nr_seq_lote_fat_w
from	pls_fatura	a
where	a.nr_sequencia	= nr_seq_fatura_p;

-- VALIDACOES XML
if	(ie_tipo_arquivo_p = 'TISS') then

	select	max(a.nr_sequencia)
	into	nr_seq_lote_fat_guia_w
	from	pls_lote_fat_guia_envio		a
	where	a.nr_seq_fatura = nr_seq_fatura_p;

	if	(nr_seq_lote_fat_guia_w is not null) then
		select	(select	nvl(sum(b.vl_beneficiario),0)
			from	pls_fat_guia_envio_proc	b,
				pls_fatura_guia_envio	e,
				pls_fat_guia_arquivo	g
			where	b.nr_seq_guia_envio	= e.nr_sequencia
			and	e.nr_seq_guia_arquivo	= g.nr_sequencia
			and	g.nr_seq_lote		= a.nr_sequencia) +
			(select	nvl(sum(b.vl_beneficiario),0)
			from	pls_fat_guia_envio_mat	b,	
				pls_fatura_guia_envio	e,
				pls_fat_guia_arquivo	g
			where	b.nr_seq_guia_envio	= e.nr_sequencia
			and	e.nr_seq_guia_arquivo	= g.nr_sequencia
			and	g.nr_seq_lote		= a.nr_sequencia)
		into	vl_total_fat_xml
		from	pls_lote_fat_guia_envio a
		where	a.nr_sequencia			= nr_seq_lote_fat_guia_w;
	end if;

	-- Se o o valor do XML estiver diferente do valor total da Fatura
	if	(vl_total_fat_xml != vl_tot_pls_fatura_w ) then
		ds_complemento_w	:=	'Total fatura TASY: ' 	|| Campo_Mascara_virgula(vl_tot_pls_fatura_w) 	|| chr(10) ||
						'Total fatura XML: ' 	|| Campo_Mascara_virgula(vl_total_fat_xml) 	|| chr(10) ||
						'Fatura TASY: ' 	|| nr_seq_fatura_p				|| chr(10) ||
						'Fatura XML: ' 		|| nr_seq_lote_fat_guia_w;

		-- Divergencia de valor entre Fatura e Fatura XML
		pls_gerar_ptu_lote_conta_erro(null, null, nm_usuario_p, cd_estabelecimento_p, nr_seq_fatura_p, null,nr_seq_lote_fat_w,183,'TISS', ds_complemento_w);
	end if;

	-- select para verificar se tem conta que nao entrou na fatura XML
	select	count(1)
	into	qt_conta_fora_xml_w
	from	pls_fatura_conta	b,
		pls_fatura_evento	a
	where	a.nr_sequencia		= b.nr_seq_fatura_evento
	and	a.nr_seq_fatura		= nr_seq_fatura_p
	and not exists(
			select	1
			from	pls_fat_guia_arquivo 	x,
				pls_fatura_guia_envio 	z
			where	x.nr_sequencia 		= z.nr_seq_guia_arquivo
			and	x.nr_seq_lote 		= nr_seq_lote_fat_guia_w
			and	z.nr_seq_conta		= b.nr_seq_conta)
	and exists(	select	1
			from	pls_lote_fat_guia_envio d
			where	d.nr_seq_fatura		= nr_seq_fatura_p);

	if	(qt_conta_fora_xml_w > 0) then

		/*ds_complemento_w	:=	'Fatura Tasy: '	|| nr_seq_fatura_p		|| chr(10) ||
						'Fatura XML: ' 	|| nr_seq_lote_fat_guia_w;*/
		for r_c02_w in c02 loop	
		
			ds_complemento_w := substr(ds_complemento_w || chr(10) || 'Conta: ' || r_c02_w.nr_seq_conta,1,4000);
			
			-- Conta nao entrou na Fatura XML
			pls_gerar_ptu_lote_conta_erro(null, null, nm_usuario_p, cd_estabelecimento_p, nr_seq_fatura_p, r_c02_w.nr_seq_conta,nr_seq_lote_fat_w,182,'TISS');
		end loop;
	end if;
end if;

-- VALIDACOES PTU
if	(ie_tipo_arquivo_p = 'PTU') then
	for r_c01_w in c01( nr_seq_fatura_p ) loop
	
		vl_total_fatura_w := (nvl(r_c01_w.vl_total_fatura, 0) + nvl(r_c01_w.vl_total_ndc, 0));

		-- O valor total da fatura TASY esta diferente do valor total da fatura PTU
		if	(vl_tot_pls_fatura_w <> vl_total_fatura_w) then
			ds_complemento_w := 	'Total fatura TASY: ' 	|| Campo_Mascara_virgula(vl_tot_pls_fatura_w) 	|| chr(10) ||
						'Total fatura PTU: ' 	|| Campo_Mascara_virgula(vl_total_fatura_w) 	|| chr(10) || chr(10) ||
						'Fatura TASY: ' 	|| r_c01_w.nr_seq_pls_fatura			|| chr(10) ||
						'Fatura PTU: ' 		|| r_c01_w.nr_sequencia;

			pls_gerar_ptu_lote_conta_erro(null, null, nm_usuario_p, cd_estabelecimento_p, nr_seq_fatura_p, null,nr_seq_lote_fat_w,168,'PTU', ds_complemento_w);
		end if;
		
		-- Verificar se tem procedimentos na fatura que nao entraram no PTU
		for r_c03_w in c03 ( r_c01_w.nr_sequencia ) loop
			for r_c04_w in c04 ( r_c03_w.nr_sequencia, r_c03_w.nr_seq_conta ) loop
				
				if	(r_c04_w.union_proc_mat = 'P') then
					ds_complemento_w	:= '. Conta Proc: '|| r_c04_w.nr_seq_conta_proc_mat;
				elsif	(r_c04_w.union_proc_mat = 'M') then
					ds_complemento_w	:= '. Conta Mat: '|| r_c04_w.nr_seq_conta_proc_mat;
				
				end if;
				
				pls_gerar_ptu_lote_conta_erro(null, null, nm_usuario_p, cd_estabelecimento_p, nr_seq_fatura_p, r_c03_w.nr_seq_conta,nr_seq_lote_fat_w,184,'PTU', ds_complemento_w );
			end loop;
		end loop;
	end loop;
end if;

commit;

end pls_validar_inf_ptu_xml;
/