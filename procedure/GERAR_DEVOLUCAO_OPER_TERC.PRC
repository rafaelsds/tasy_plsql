create or replace
procedure GERAR_DEVOLUCAO_OPER_TERC
			(nr_sequencia_p	in number,
			nm_usuario_p	in varchar2) is
			
cd_material_w			number(10);			
nr_sequencia_w			number(10);
dt_atualizacao_estoque_w	date;

begin

select	max(cd_material),
	max(dt_atualizacao_estoque)
into	cd_material_w,
	dt_atualizacao_estoque_w
from	terceiro_operacao
where	nr_sequencia	= nr_sequencia_p;

select	terceiro_operacao_seq.nextval
into	nr_sequencia_w
from	dual;

insert	into terceiro_operacao
	(nr_sequencia,
	cd_estabelecimento,
	nr_seq_terceiro,
	nr_seq_operacao,
	tx_operacao,
	dt_atualizacao,
	nm_usuario,
	nr_doc,
	nr_seq_doc,
	dt_operacao,
	nr_seq_conta,
	cd_material,
	ie_origem_proced,
	cd_procedimento,
	qt_operacao,
	vl_operacao,
	nr_lote_contabil,
	cd_pessoa_fisica,
	dt_atualizacao_estoque,
	cd_local_estoque,
	nr_seq_trans_fin,
	nr_seq_ordem_servico,
	dt_inclusao,
	nm_usuario_inclusao,
	ie_biometria,
	ie_senha,
	qt_peso,
	cd_cgc_fornecedor,
	nr_seq_oper_origem,
	NR_SEQ_LOTE_FORNEC)
select	nr_sequencia_w,
	cd_estabelecimento,
	nr_seq_terceiro,
	nr_seq_operacao,
	tx_operacao,
	sysdate,
	nm_usuario_p,
	nr_doc,
	nr_seq_doc,
	dt_operacao,
	nr_seq_conta,
	cd_material,
	ie_origem_proced,
	cd_procedimento,
	qt_operacao,
	vl_operacao * -1,
	nr_lote_contabil,
	cd_pessoa_fisica,
	null,
	cd_local_estoque,
	nr_seq_trans_fin,
	nr_seq_ordem_servico,
	dt_inclusao,
	nm_usuario_p,
	ie_biometria,
	ie_senha,
	qt_peso,
	cd_cgc_fornecedor,
	nr_sequencia_p,
	NR_SEQ_LOTE_FORNEC
from	terceiro_operacao
where	nr_sequencia	= nr_sequencia_p;

if	(cd_material_w is not null) and
	(dt_atualizacao_estoque_w is not null) then
	Gerar_movto_estoque_Terc(nr_sequencia_w, '2', nm_usuario_p); --Gerar devolu��o no estoque
end if;

commit;

end GERAR_DEVOLUCAO_OPER_TERC;
/