create or replace 
procedure cirurgia_participan_beforepost (cd_pessoa_fisica_p	varchar2,
					 cd_funcao_medico_p	number,
					 nr_cirurgia_p		number,
					 nm_pessoa_fisica_p	out varchar2,
					 qt_participante_p	out number) is

nm_pessoa_fisica_w	varchar2(100) 	:= '';
qt_funcao_medico_w	number(4)	:= 0;
qt_participante_w	number(4)	:= 0;

begin



if	(cd_funcao_medico_p > 0) then 
	select	count(*) 
	into 	qt_funcao_medico_w
	from	funcao_medico
	where	cd_funcao 	= cd_funcao_medico_p;

	if	(qt_funcao_medico_w = 0) then
		--Fun��o n�o cadastrada!
		Wheb_mensagem_pck.exibir_mensagem_abort(263502);
	end if;
end if;

if	(cd_pessoa_fisica_p > 0) then 
	nm_pessoa_fisica_w	:= substr(obter_nome_pf(cd_pessoa_fisica_p), 1, 100);
	
	if	(nr_cirurgia_p > 0) then 
		select	count(*)
		into 	qt_participante_w
		from 	cirurgia_participante
		where	nr_cirurgia 		= nr_cirurgia_p
		and		dt_inativacao is null
		and	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
	end if;
end if;

nm_pessoa_fisica_p	:= nm_pessoa_fisica_w;
qt_participante_p	:= qt_participante_w;

end  cirurgia_participan_beforepost;
/
