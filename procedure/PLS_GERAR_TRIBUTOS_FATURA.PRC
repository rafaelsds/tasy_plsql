create or replace
procedure pls_gerar_tributos_fatura	(	nr_seq_fatura_p		number,
						ie_opcao_p		varchar2,
						nm_usuario_p		varchar2) is
--	IE_OPCAO_P
--	'F'		Gerar tributos sobre o valor da FATURA
--	'NDC'		Gerar tributos sobre o valor da NOTA DE REEMBOLSO 
--	'NDC+F'		Gerar tributos sobre o valor da NOTA DE REEMBOLSO + FATURA

ie_vago_w			varchar2(255);
ie_tipo_tributo_w		varchar2(15);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
ie_acumulativo_w		varchar2(10);
ie_origem_titulo_w		varchar2(10);
ie_ato_cooperado_w		varchar2(1)	:= 'N';
vl_fatura_w			number(15,2);
vl_fatura_ww			number(15,2);
vl_fatura_orig_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_minimo_base_calculo_w	number(15,2);
vl_tributo_w			number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_base_adic_w			number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_tributo_a_reter_w		number(15,2);
vl_base_a_reter_w		number(15,2);
vl_minimo_base_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_desc_dependente_w		number(15,2);
vl_trib_anterior_w		number(15,2);
vl_total_base_w			number(15,2);
vl_reducao_w			number(15,2);
vl_proc_cooperado_w		number(15,2);
nr_seq_regra_w			number(10);
nr_seq_pagador_w		number(10);
pr_imposto_w			number(7,4);
cd_estabelecimento_w		number(4);
cd_tributo_w			number(3);
dt_emissao_w			date;
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;
ie_saldo_tit_rec_w		varchar2(2)	:= 'N';

cd_darf_w		regra_calculo_imposto.cd_darf%type;

cursor 	C01 is
	select	b.cd_tributo
	from	tributo b
	where	b.ie_corpo_item	= 'C'
	and	b.ie_situacao	= 'A'
	and	b.ie_pf_pj	in ('A',decode(cd_pessoa_fisica_w,null,'PJ','PF'))
	order 	by b.cd_tributo;

begin
select	b.cd_estabelecimento,
	decode(ie_opcao_p, 
		'NDC+F', nvl(a.vl_fatura,0) + nvl(a.vl_total_ndc,0), 
		'NDC', nvl(a.vl_total_ndc,0), 
		'F', nvl(a.vl_fatura,0)),
	nvl(a.dt_emissao, nvl(b.dt_emissao, sysdate)),
	'13',
	a.nr_seq_pagador
into	cd_estabelecimento_w,
	vl_fatura_w,
	dt_emissao_w,
	ie_origem_titulo_w,
	nr_seq_pagador_w
from	pls_lote_faturamento b,
	pls_fatura a
where	a.nr_sequencia		= nr_seq_fatura_p
and	a.nr_seq_lote		= b.nr_sequencia;

select	max(a.cd_pessoa_fisica),
	max(a.cd_cgc)
into	cd_pessoa_fisica_w,
	cd_cgc_w
from	pls_contrato_pagador a
where	a.nr_sequencia = nr_seq_pagador_w;

if	(cd_cgc_w is not null) and
	(cd_pessoa_fisica_w is not null) then
	cd_cgc_w	:= null;
end if;

vl_fatura_orig_w	:=	vl_fatura_w;

open	C01;
loop
fetch	C01 into
	cd_tributo_w;
exit	when C01%notfound;
	begin
	select	ie_tipo_tributo
	into	ie_tipo_tributo_w
	from	tributo
	where	cd_tributo	= cd_tributo_w;

	vl_fatura_w		:= vl_fatura_orig_w;
	ie_ato_cooperado_w	:= 'N';
	
	--if	(ie_opcao_p in ('NDC','NDC+F')) and (ie_tipo_tributo_w in ('IR','IRRF')) then
		select	nvl(sum(x.vl_beneficiario),0)
		into	vl_proc_cooperado_w
		from	pls_conta_proc x
		where	x.nr_sequencia in
			(select	f.nr_sequencia
			from	pls_cooperado			i,
				pls_prestador			h, /* Prestador executor */
				pls_conta_proc			f,
				pls_conta			e,
				pls_fatura_conta		b,
				pls_fatura_evento         	k,
				pls_fatura                      l
			where	l.nr_sequencia			= nr_seq_fatura_p
			and     l.nr_sequencia                  = k.nr_seq_fatura
			and     k.nr_sequencia                  = b.nr_seq_fatura_evento
			and	f.nr_seq_conta			= e.nr_sequencia
			and     e.nr_sequencia                  = b.nr_seq_conta
			and	e.nr_seq_prestador_exec		= h.nr_sequencia
			and	h.cd_pessoa_fisica		= i.cd_pessoa_fisica
			and     f.nr_seq_evento_fat is  null
			and	f.ie_ato_cooperado	= '1'
			and	exists (select 1
					from	pls_conta_medica_resumo	m,
						pls_cooperado		j,
						pls_prestador		g /* Prestador pagto */
					where   m.nr_seq_conta(+)	= e.nr_sequencia
					and	m.nr_seq_prestador_pgto	= g.nr_sequencia(+)
					and	g.cd_pessoa_fisica	= j.cd_pessoa_fisica(+)));

	obter_dados_trib_tit_rec(cd_tributo_w,
				cd_estabelecimento_w,
				null,
				dt_emissao_w,
				ie_ato_cooperado_w,
				pr_imposto_w,
				vl_minimo_base_w,
				vl_minimo_tributo_w,
				ie_acumulativo_w,
				vl_teto_base_w,
				vl_desc_dependente_w,
				cd_pessoa_fisica_w,
				cd_cgc_w,
				ie_vago_w,
				ie_origem_titulo_w,
				0,
				nr_seq_regra_w,
				cd_darf_w);
				
	if	(nr_seq_regra_w is null) then
		if	(ie_ato_cooperado_w = 'N') then
			ie_ato_cooperado_w := 'S';
		else
			ie_ato_cooperado_w := 'N';
		end if;
	
		obter_dados_trib_tit_rec(cd_tributo_w,
					cd_estabelecimento_w,
					null,
					dt_emissao_w,
					ie_ato_cooperado_w,
					pr_imposto_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					ie_acumulativo_w,
					vl_teto_base_w,
					vl_desc_dependente_w,
					cd_pessoa_fisica_w,
					cd_cgc_w,
					ie_vago_w,
					ie_origem_titulo_w,
					0,
					nr_seq_regra_w,
					cd_darf_w);
	end if;
	
	select	nvl(max(ie_ato_cooperado),'N'),
		nvl(max(ie_saldo_tit_rec),'N')
	into	ie_ato_cooperado_w,
		ie_saldo_tit_rec_w
	from	regra_calculo_imposto
	where	nr_sequencia = nr_seq_regra_w;
	
	if	(ie_ato_cooperado_w = 'S') then
		vl_fatura_w	:= vl_proc_cooperado_w;
	end if;

	if	(pr_imposto_w > 0) then
		vl_fatura_ww	:= vl_fatura_w;

		obter_valores_tributo(	ie_acumulativo_w,
					pr_imposto_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					vl_soma_trib_nao_retido_w,
					vl_soma_trib_adic_w,
					vl_soma_base_nao_retido_w,
					vl_soma_base_adic_w,
					vl_fatura_ww,
					vl_tributo_w,
					vl_trib_nao_retido_w,
					vl_trib_adic_w,
					vl_base_nao_retido_w,
					vl_base_adic_w,
					vl_teto_base_w,
					vl_trib_anterior_w,
					'N',
					vl_total_base_w,
					vl_reducao_w,
					vl_desc_dependente_w,
					0,
					0,
					0,
					null,
					0,
					sysdate,
					nr_seq_regra_irpf_w);

		insert	into pls_fatura_trib
			(	nr_sequencia,cd_tributo,tx_tributo,vl_tributo,vl_base_calculo,
				dt_atualizacao,nm_usuario,nr_seq_fatura,vl_trib_nao_retido,vl_base_nao_retido,
				vl_trib_adic,vl_base_adic,ie_tipo_valor_fat,ie_origem_tributo,dt_tributo,
				nr_seq_regra_trib)
		select		titulo_receber_trib_seq.NextVal,cd_tributo_w,pr_imposto_w,vl_tributo_w,vl_fatura_ww,
				sysdate,nm_usuario_p,nr_seq_fatura_p,vl_trib_nao_retido_w,vl_base_nao_retido_w,
				vl_trib_adic_w,vl_base_adic_w,ie_opcao_p,decode(ie_saldo_tit_rec_w,'S','CD','C'),dt_emissao_w,
				nr_seq_regra_w
		from 	dual;
	end if;
	end;
end loop;
close C01;

end pls_gerar_tributos_fatura;
/
