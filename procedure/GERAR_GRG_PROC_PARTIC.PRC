create or replace
procedure GERAR_GRG_PROC_PARTIC	(nr_seq_hist_item_p	in	number,
	 			nm_usuario_p		in	varchar2,
				nr_seq_guia_p		in 	number) is

nr_seq_propaci_w	number(10);
vl_partic_w		number(15,2);
vl_saldo_amenor_w	number(15,2);
vl_saldo_orig_w		number(15,2);
vl_participante_w	number(15,2);
nr_seq_partic_w		number(10);
qt_registro_w		number(10);
vl_adicional_w		number(15,2);
vl_amenor_w		number(15,2);
vl_glosa_w		number(15,2);
vl_pago_w		number(15,2);
vl_amenor_partic_w	number(15,2)	:= 0;
nr_seq_ret_item_w	number(10);
cd_estabelecimento_w	number(10);
ie_valor_amenor_ret_w	varchar2(10) := 'N';
nr_seq_hist_item_w	number(10);

cursor	c01 is
select	a.nr_seq_partic,
	a.vl_participante
from	procedimento_participante a
where	not exists
	(select	1
	from	grg_proc_partic x
	where	x.nr_seq_partic		= a.nr_seq_partic
	and	x.nr_seq_proc		= a.nr_sequencia
	and	x.nr_seq_hist_item	= nr_seq_hist_item_p)
and	a.nr_sequencia	= nr_seq_propaci_w;

cursor 	c02 is
select	a.nr_sequencia
from	lote_audit_hist_item a
where	a.nr_seq_guia		= nr_seq_guia_p
and	a.nr_seq_propaci 	is not null
and	nr_seq_guia_p		is not null
and	nr_seq_hist_item_p	is null
union
select	a.nr_sequencia
from	lote_audit_hist_item a
where	a.nr_sequencia		= nr_seq_hist_item_p
and	a.nr_seq_propaci 	is not null;

begin

obter_param_usuario(69,38,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_valor_amenor_ret_w );

open C02;
loop
fetch C02 into	
	nr_seq_hist_item_w;
exit when C02%notfound;
	begin
	
	select	max(b.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	lote_auditoria b,
		lote_audit_hist a,
		lote_audit_hist_guia d,
		lote_audit_hist_item c
	where	c.nr_sequencia		= nr_seq_hist_item_w
	and	c.nr_seq_guia		= d.nr_sequencia
	and	d.nr_seq_lote_hist	= a.nr_sequencia
	and	a.nr_seq_lote_audit	= b.nr_sequencia;

	select	max(a.nr_seq_propaci),
		nvl(max(a.vl_adicional),0),
		nvl(max(a.vl_amenor),0),
		nvl(max(a.vl_glosa),0),
		nvl(max(a.vl_pago),0),
		max(a.nr_seq_ret_item)
	into	nr_seq_propaci_w,
		vl_adicional_w,
		vl_amenor_w,
		vl_glosa_w,
		vl_pago_w,
		nr_seq_ret_item_w
	from	lote_audit_hist_item a
	where	a.nr_sequencia	= nr_seq_hist_item_w;

	if	(nvl(nr_seq_propaci_w,0) <> 0) then

		select	count(*)	/* verificar se o m�dico executor j� foi gerado para o item */
		into	qt_registro_w
		from	grg_proc_partic a
		where	a.nr_seq_hist_item	= nr_seq_hist_item_w
		and	a.nr_seq_partic		is null
		and	a.nr_seq_proc		= nr_seq_propaci_w;

		if	(nvl(qt_registro_w,0)	= 0) then

			select	nvl(max(a.vl_medico),0)	/* valor original do participante (no caso, o m�dico executor) */
			into	vl_participante_w
			from	procedimento_paciente a
			where	a.nr_sequencia	= nr_seq_propaci_w;
			
			if	(vl_participante_w = 0) then
				select	nvl(max(a.vl_procedimento),0)	/* valor original do participante (no caso, o m�dico executor) */
				into	vl_participante_w
				from	procedimento_paciente a
				where	a.nr_sequencia	= nr_seq_propaci_w;
			end if;

			select	nvl(sum(a.vl_glosa),0) + nvl(sum(a.vl_pago),0) vl_partic	/* valor das an�lises anteriores */
			into	vl_partic_w
			from	grg_proc_partic a
			where	a.nr_seq_hist_item	< nr_seq_hist_item_w
			and	a.nr_seq_partic		is null
			and	a.nr_seq_proc		= nr_seq_propaci_w;

			vl_saldo_orig_w	:= vl_participante_w - vl_partic_w;
			
			if	(nvl(ie_valor_amenor_ret_w,'N') = 'S') and
				(nr_seq_ret_item_w is not null) then
				
				select	nvl(sum(a.vl_glosa),0)
				into	vl_amenor_w
				from	motivo_glosa b,
					convenio_retorno_glosa a
				where	a.cd_motivo_glosa	= b.cd_motivo_glosa
				and	b.ie_acao_glosa		= 'R'
				and	a.nr_seq_ret_item	= nr_seq_ret_item_w
				and	a.nr_seq_propaci	= nr_seq_propaci_w
				and	a.nr_seq_partic 	is null;	
				
			end if;

			/* M�dico executor do procedimento */
			insert	into grg_proc_partic
				(dt_atualizacao,
				nm_usuario,
				nr_seq_hist_item,
				nr_seq_partic,
				nr_seq_proc,
				nr_sequencia,
				vl_adicional,
				vl_amenor,
				vl_glosa,
				vl_pago,
				vl_saldo_orig)
			values	(sysdate,
				nm_usuario_p,
				nr_seq_hist_item_w,
				null,
				nr_seq_propaci_w,
				grg_proc_partic_seq.nextval,
				vl_adicional_w,
				vl_amenor_w,
				vl_glosa_w,
				vl_pago_w,
				vl_saldo_orig_w);

		end if;

		open	c01;
		loop
		fetch	c01 into
			nr_seq_partic_w,
			vl_participante_w;
		exit	when c01%notfound;

			select	nvl(sum(a.vl_glosa),0) + nvl(sum(a.vl_pago),0) vl_partic	/* valor das an�lises anteriores */
			into	vl_partic_w
			from	grg_proc_partic a
			where	a.nr_seq_hist_item	< nr_seq_hist_item_w
			and	a.nr_seq_partic		= nr_seq_partic_w
			and	a.nr_seq_proc		= nr_seq_propaci_w;

			vl_saldo_orig_w	:= vl_participante_w - vl_partic_w;
			
			vl_amenor_partic_w	:= 0;
			
			if	(nvl(ie_valor_amenor_ret_w,'N') = 'S') and
				(nr_seq_ret_item_w is not null) then
				
				select	nvl(sum(a.vl_glosa),0)
				into	vl_amenor_partic_w
				from	motivo_glosa b,
					convenio_retorno_glosa a
				where	a.cd_motivo_glosa	= b.cd_motivo_glosa
				and	b.ie_acao_glosa		= 'R'
				and	a.nr_seq_ret_item	= nr_seq_ret_item_w
				and	a.nr_seq_propaci	= nr_seq_propaci_w
				and	a.nr_seq_partic 	= nr_seq_partic_w;	
				
			end if;

			/* Participante n�o executor do procedimento */
			insert	into grg_proc_partic
				(dt_atualizacao,
				nm_usuario,
				nr_seq_hist_item,
				nr_seq_partic,
				nr_seq_proc,
				nr_sequencia,
				vl_adicional,
				vl_amenor,
				vl_glosa,
				vl_pago,
				vl_saldo_orig)
			values	(sysdate,
				nm_usuario_p,
				nr_seq_hist_item_w,
				nr_seq_partic_w,
				nr_seq_propaci_w,
				grg_proc_partic_seq.nextval,
				0,
				vl_amenor_partic_w,
				0,
				0,
				vl_saldo_orig_w);

		end	loop;
		close	c01;

	end if;
	
	end;
end loop;
close C02;

commit;

end GERAR_GRG_PROC_PARTIC;
/