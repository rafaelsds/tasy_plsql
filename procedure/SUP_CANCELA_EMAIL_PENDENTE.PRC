create or replace
procedure	sup_cancela_email_pendente(
			ie_tipo_mensagem_p		number,
			nr_documento_p			number,
			ie_tipo_documento_p		varchar2,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

nr_sequencia_w			number(10);

cursor	c01 is
select	nr_sequencia
from	envio_email_compra_agrup a
where	cd_estabelecimento = cd_estabelecimento_p
and	ie_tipo_mensagem = nvl(ie_tipo_mensagem_p,ie_tipo_mensagem)
and	nr_documento = nr_documento_p
and	ie_tipo_documento = ie_tipo_documento_p
and	nvl(a.ie_cancelado,'N') = 'N'
and	exists	(select	1
		from	email_compra_agrup_dest x
		where	x.nr_seq_envio = a.nr_sequencia
		and	nvl(x.ie_enviado,'N') = 'N');

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w;
exit when c01%notfound;
	begin

	update	envio_email_compra_agrup
	set	dt_cancelamento = sysdate,
		ie_cancelado = 'S',
		nm_usuario = nm_usuario_p,		
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_w;

	end;	
end loop;
close c01;

end sup_cancela_email_pendente;
/
