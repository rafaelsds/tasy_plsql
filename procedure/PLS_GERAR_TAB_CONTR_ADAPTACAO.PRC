create or replace
procedure pls_gerar_tab_contr_adaptacao
			(	nr_seq_contrato_p		number,
				nr_seq_proposta_p		number,
				nr_seq_plano_p			number,
				nr_seq_tabela_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

dt_contrato_w			date;
nr_seq_tabela_w			number(10);
qt_idade_inicial_w		number(5);
qt_idade_final_w		number(5);
vl_preco_atual_w		number(15,2);
tx_acrescimo_w			number(7,4);
qt_registros_w			number(10);
ie_tabela_contrato_w		varchar2(1);
nm_estipulante_w		varchar2(80);
ie_tipo_contratacao_w		varchar2(3);
vl_preco_nao_subsidiado_w	Number(15,2);
ie_preco_w			Varchar2(2);
nr_seq_contrato_tabela_w	Number(10);
nr_contrato_w			Number(10);
vl_minimo_w			Number(15,2);
ie_grau_titularidade_w		varchar2(2);

qt_vidas_inicial_w		number(10);
qt_vidas_final_w		number(10);
tx_adaptacao_w			Number(7,4);
vl_adaptacao_w			Number(15,2);
dt_inicio_proposta_w		date;

cursor c01 is
	select	qt_idade_inicial,
		qt_idade_final,
		vl_preco_atual,
		tx_acrescimo,
		vl_preco_nao_subsidiado,
		vl_minimo,
		ie_grau_titularidade,
		qt_vidas_inicial,
		qt_vidas_final
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p
	order by qt_idade_inicial;

begin

select	max(ie_preco)
into	ie_preco_w
from	pls_plano
where	nr_sequencia	= nr_seq_plano_p;

if	(ie_preco_w in (1,4)) then	
	select	nr_contrato,
		nvl(dt_contrato,sysdate),
		substr(obter_nome_pf_pj(cd_pf_estipulante,cd_cgc_estipulante),1,80)
	into	nr_contrato_w,
		dt_contrato_w,
		nm_estipulante_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_p;
	
	select	max(nr_contrato)
	into	nr_seq_contrato_tabela_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_p;
	
	select	max(dt_inicio_proposta)
	into	dt_inicio_proposta_w
	from	pls_proposta_adesao
	where	nr_sequencia	= nr_seq_proposta_p;
	
	if	(nr_seq_plano_p is not null) and
		(nr_seq_tabela_p is not null) then
		
		select	max(tx_adaptacao)
		into	tx_adaptacao_w
		from	pls_regra_adaptacao_plano
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	dt_inicio_proposta_w between dt_inicio_vigencia and fim_dia(nvl(dt_fim_vigencia,dt_inicio_proposta_w))
		and	((nr_seq_grupo_produto is null) or
			(nr_seq_grupo_produto is not null) and
			(pls_se_grupo_preco_produto(nr_seq_grupo_produto,nr_seq_plano_p)) = 'S');
			
		
		select	pls_tabela_preco_seq.nextval
		into	nr_seq_tabela_w
		from	dual;
		
		insert into pls_tabela_preco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nm_tabela,
			dt_inicio_vigencia,
			nr_seq_plano,
			dt_liberacao,
			ie_tabela_base,
			nr_contrato,
			nr_seq_tabela_origem,
			ie_preco_vidas_contrato,
			ie_calculo_vidas,
			tx_adaptacao,
			nr_seq_faixa_etaria)
		select	nr_seq_tabela_w,
			sysdate,
			nm_usuario_p,
			'Tabela para o contrato ' || to_char(nr_contrato_w) || ' - ' || nm_estipulante_w,
			dt_contrato_w,
			nr_seq_plano_p,
			sysdate,
			'N',
			nr_seq_contrato_p,
			nr_sequencia,
			ie_preco_vidas_contrato,
			ie_calculo_vidas,
			tx_adaptacao_w,
			nr_seq_faixa_etaria
		from	pls_tabela_preco
		where	nr_sequencia	= nr_seq_tabela_p;
		
		open c01;
		loop
		fetch c01 into
			qt_idade_inicial_w,
			qt_idade_final_w,
			vl_preco_atual_w,
			tx_acrescimo_w,
			vl_preco_nao_subsidiado_w,
			vl_minimo_w,
			ie_grau_titularidade_w,
			qt_vidas_inicial_w,
			qt_vidas_final_w;
		exit when c01%notfound;
		
			if	(tx_adaptacao_w is not null) then
				vl_adaptacao_w	:= dividir((vl_preco_atual_w * tx_adaptacao_w), 100); 
			else
				vl_adaptacao_w	:= 0;
			end if;
			
			insert into pls_plano_preco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tabela,
				qt_idade_inicial,
				qt_idade_final,
				vl_preco_inicial,
				vl_preco_atual,
				tx_acrescimo,
				vl_preco_nao_subsidiado,
				vl_minimo,
				ie_grau_titularidade,
				qt_vidas_inicial,
				qt_vidas_final,
				vl_adaptacao)
			values	(pls_plano_preco_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tabela_w,
				qt_idade_inicial_w,
				qt_idade_final_w,
				vl_preco_atual_w,
				vl_preco_atual_w,
				tx_acrescimo_w,
				vl_preco_nao_subsidiado_w,
				vl_minimo_w,
				ie_grau_titularidade_w,
				qt_vidas_inicial_w,
				qt_vidas_final_w,
				vl_adaptacao_w);
		end loop;
		close c01;
		
		update	pls_contrato_plano
		set	nr_seq_tabela	= nr_seq_tabela_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			nr_seq_tabela_origem = nr_seq_tabela_p
		where	nr_seq_contrato	= nr_seq_contrato_p
		and	nr_seq_plano	= nr_seq_plano_p
		and	nr_seq_tabela	= nr_seq_tabela_p;
	end if;
end if;

end pls_gerar_tab_contr_adaptacao;
/