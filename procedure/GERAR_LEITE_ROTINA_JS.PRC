create or replace
procedure gerar_leite_rotina_js(	nr_prescricao_p			number,
									ds_lista_leites_p		varchar2,
									cd_perfil_p				number,
									nm_usuario_p			varchar2) is

ds_lista_w		varchar2(4000);
ds_lista_aux_w		varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_pos_hifem_w		number(10,0);
cd_leite_w		number(10,0);
nr_ctrl_loop_w		number(3,0) := 0;

begin
if	(nr_prescricao_p is not null) and
	(ds_lista_leites_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ds_lista_w	:= ds_lista_leites_p;
	
	while	(ds_lista_w is not null) and
			(nr_ctrl_loop_w < 100) loop
		begin
		nr_ctrl_loop_w 		:= nr_ctrl_loop_w + 1;		
		nr_pos_virgula_w	:= instr(ds_lista_w,',');
		
		cd_leite_w		:= 0;		
		
		if	(nvl(nr_pos_virgula_w,0) > 0) then
			begin
			ds_lista_aux_w	:= substr(ds_lista_w,0,nr_pos_virgula_w-1);
			ds_lista_w	:= substr(ds_lista_w,nr_pos_virgula_w+1,length(ds_lista_w));
			end;
		else
			begin
			ds_lista_aux_w	:= ds_lista_w;
			ds_lista_w	:= null;
			end;
		end if;
		
		if	(ds_lista_aux_w is not null) then
			begin
			nr_pos_hifem_w	:= instr(ds_lista_aux_w,'-');
			
			if	(nr_pos_hifem_w > 0) then	
				cd_leite_w := substr(ds_lista_aux_w,1,nr_pos_hifem_w-1);
			else
				cd_leite_w := ds_lista_aux_w;
			end if;			
						
			end;
		end if;
		
		if	(cd_leite_w is not null) then
			begin			
			gerar_leite_rotina(	nr_prescricao_p,
								cd_leite_w,
								nm_usuario_p,
								cd_perfil_p);
			end;
		end if;
		
		end;
	end loop;
	end;
end if;
commit;
end gerar_leite_rotina_js;
/
