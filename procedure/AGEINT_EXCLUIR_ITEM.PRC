create or replace
procedure Ageint_Excluir_Item(
			nr_seq_item_p		number,
			nm_usuario_p		Varchar2) is 

nr_seq_agenda_cons_w	number(10);
nr_seq_agenda_exame_w	number(10);
			
begin

select	max(nr_seq_agenda_cons),
	max(nr_seq_agenda_exame)
into	nr_seq_agenda_cons_w,
	nr_seq_agenda_exame_w
from	agenda_integrada_item
where	nr_sequencia	= nr_seq_item_p;

if	(nvl(nr_seq_agenda_cons_w,0) > 0) and
	(nvl(nr_seq_agenda_exame_w,0) > 0) then
	delete	ageint_equip_item
	where	nr_seq_agenda_item	= nr_seq_item_p;

	delete	agenda_integrada_prof_item
	where	nr_seq_agenda_item	= nr_seq_item_p;

	delete	agenda_integrada_item
	where	nr_sequencia	= nr_seq_item_p;
end if;
	
commit;

end Ageint_Excluir_Item;
/