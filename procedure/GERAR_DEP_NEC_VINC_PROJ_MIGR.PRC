create or replace
procedure gerar_dep_nec_vinc_proj_migr (
		nr_seq_os_nec_vinc_p	number,
		nr_seq_ativ_proj_p	number,
		ie_nivel_dependencia_p	varchar2,
		ds_observacao_p		varchar2,
		nm_usuario_p		varchar2) is
		
nr_seq_projeto_w	number(10,0);
ie_dep_reg_w		varchar2(1);
		
begin
if	(nr_seq_os_nec_vinc_p is not null) and
	(ie_nivel_dependencia_p	is not null) and
	(nm_usuario_p is not null) then
	begin
	select	nvl(max(nr_seq_proj),0)
	into	nr_seq_projeto_w
	from	proj_ordem_servico
	where	nr_seq_ordem = nr_seq_os_nec_vinc_p;
	
	if	(nr_seq_projeto_w > 0) then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_dep_reg_w		
		from	proj_dependencia
		where	nr_seq_projeto = nr_seq_projeto_w
		and	nr_seq_ordem_serv = nr_seq_os_nec_vinc_p
		and	nvl(nr_seq_ativ_cron,nvl(nr_seq_ativ_proj_p,0)) = nvl(nr_seq_ativ_proj_p,0);
		
		if	(ie_dep_reg_w = 'N') then
			begin
			insert into proj_dependencia (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nr_seq_projeto,
				nr_seq_ativ_cron,
				nr_seq_ordem_serv,
				nr_seq_proj_dep,
				ie_nivel_dependencia,
				ie_status,
				cd_funcao,
				ds_observacao)
			values (
				proj_dependencia_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_projeto_w,
				nr_seq_ativ_proj_p,
				nr_seq_os_nec_vinc_p,
				null,
				ie_nivel_dependencia_p,
				'N',
				null,
				ds_observacao_p);
			end;
		else
			begin
			wheb_mensagem_pck.exibir_mensagem_abort(281865);
			end;
		end if;
		end;
	end if;
	end;
end if;
commit;
end gerar_dep_nec_vinc_proj_migr;
/