create or replace
procedure gerar_w_analise_pre_fatur(	cd_estabelecimento_p	number,
				dt_referencia_p		date,
				nm_usuario_p		Varchar2) is 

dt_mesano_referencia_w		date;
ie_existe_w			varchar2(1);
ie_status_acerto_w			number(1);
nr_interno_conta_w			number(10);
qt_conta_w			number(10);
vl_pre_fatur_w			number(15,2);
vl_conta_w			number(15,2);
vl_conta_exc_w			number(15,2);
vl_conta_prox_mes_w		number(15,2);
vl_conta_outro_mes_w		number(15,2);
vl_conta_provisoria_w		number(15,2);
					
cursor c01 is
select	a.nr_interno_conta,
	a.vl_conta vl_pre_fatur
from	pre_fatur_conta a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.dt_referencia		= dt_referencia_p
order by 1;

BEGIN

delete	from w_analise_pre_fatur
where	nm_usuario		= nm_usuario_p;
commit;

open C01;
loop
fetch C01 into	
	nr_interno_conta_w,
	vl_pre_fatur_w;
exit when C01%notfound;
	begin
	ie_existe_w		:= 'S';
	vl_conta_w		:= 0;
	vl_conta_exc_w		:= 0;
	vl_conta_provisoria_w	:= 0;
	vl_conta_prox_mes_w	:= 0;
	vl_conta_outro_mes_w	:= 0;
	
	select	count(*)
	into	qt_conta_w
	from	conta_paciente
	where	nr_interno_conta	= nr_interno_conta_w;
	
	if	(qt_conta_w > 0) then
		vl_conta_w	:= obter_valor_conta(nr_interno_conta_w,0);
		select	b.dt_mesano_referencia,
			b.ie_status_acerto
		into	dt_mesano_referencia_w,
			ie_status_acerto_w
		from	conta_paciente b
		where	b.nr_interno_conta	= nr_interno_conta_w;
	else
		ie_existe_w	:= 'N';
		vl_conta_exc_w	:= vl_pre_fatur_w;
	end if;
	
	
	if	(ie_existe_w	= 'S') then
		begin
		if	(ie_status_acerto_w = 1) then
			vl_conta_provisoria_w		:= vl_conta_w;
		elsif	(ie_status_acerto_w = 2) then
			begin
			if	(PKG_DATE_UTILS.start_of(dt_mesano_referencia_w,'month',0) = PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,1,0)) then
				vl_conta_prox_mes_w	:= vl_conta_w;
			elsif	(PKG_DATE_UTILS.start_of(dt_mesano_referencia_w,'month',0) > PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,1,0)) then
				vl_conta_outro_mes_w	:= vl_conta_w;
			end if;
			end;
		end if;
		end;	
	end if;
	insert into w_analise_pre_fatur(
		dt_referencia,
		cd_estabelecimento,
		nr_interno_conta, 
		vl_conta,
		dt_atualizacao,
		nm_usuario,
		vl_atual_conta,
		vl_conta_prox_mes,
		vl_conta_outro_mes,
		vl_conta_provisoria,
		vl_conta_exc)
	values(	dt_referencia_p,
		cd_estabelecimento_p,
		nr_interno_conta_w,
		vl_pre_fatur_w,
		sysdate,
		nm_usuario_p,
		vl_conta_w,
		vl_conta_prox_mes_w,
		vl_conta_outro_mes_w,
		vl_conta_provisoria_w,
		vl_conta_exc_w);
	end;
end loop;
close C01;
commit;

end gerar_w_analise_pre_fatur;
/