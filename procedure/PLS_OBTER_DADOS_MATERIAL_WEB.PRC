create or replace
procedure pls_obter_dados_material_web (
				nr_seq_material_p	number,
				cd_material_ops_p	varchar2,
				ie_opcao_p		varchar2,
				ds_retorno_p	out	varchar2,
				cd_retorno_p	out	varchar) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Obter dados do produto
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

IE_OPCAO_P

NC - Nome comercial
N - Nome	

*/

ds_retorno_w		varchar2(255);			
nr_seq_codigo_w		varchar2(255);			
				
begin

if 	(nr_seq_material_p 	is not null) then

	if	(ie_opcao_p 	= 'NC') then
		select 	max(nvl(ds_nome_comercial,ds_material))
		into	ds_retorno_w
		from	pls_material
		where 	nr_sequencia  = nr_seq_material_p
		and 	ie_situacao = 'A'
		and 	(dt_exclusao is null or dt_exclusao > sysdate);
	elsif	(ie_opcao_p 	= 'N') then
		select 	max(ds_material)
		into	ds_retorno_w
		from	pls_material
		where 	nr_sequencia  = nr_seq_material_p
		and 	ie_situacao = 'A'
		and 	(dt_exclusao is null or dt_exclusao > sysdate);
	end if;
	
	select	max(cd_material_ops)
	into	nr_seq_codigo_w
	from	pls_material
	where	nr_sequencia	= nr_seq_material_p;
	
elsif	(cd_material_ops_p 	is not null) then
	
	select	max(nr_sequencia)
	into	nr_seq_codigo_w
	from	pls_material
	where	cd_material_ops	= cd_material_ops_p;
	
	if	(ie_opcao_p 	= 'NC') then
		select 	max(nvl(ds_nome_comercial,ds_material))
		into	ds_retorno_w
		from	pls_material
		where 	nr_sequencia  = nr_seq_codigo_w
		and 	ie_situacao = 'A'
		and 	(dt_exclusao is null or dt_exclusao > sysdate);
	elsif	(ie_opcao_p 	= 'N') then
		select 	max(ds_material)
		into	ds_retorno_w
		from	pls_material
		where 	nr_sequencia  = nr_seq_codigo_w
		and 	ie_situacao = 'A'
		and 	(dt_exclusao is null or dt_exclusao > sysdate);
	end if;
	
end if;

ds_retorno_p := ds_retorno_w;
cd_retorno_p := nr_seq_codigo_w;

end pls_obter_dados_material_web;
/