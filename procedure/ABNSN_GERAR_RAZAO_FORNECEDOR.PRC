create or replace PROCEDURE ABNSN_Gerar_Razao_Fornecedor(	dt_parametro_p   		DATE,
					nm_usuario_p			varchar2,
					ie_considera_cancel_p		varchar2,
					cd_estabelecimento_p		number,
					dt_inicial_p			date,
					dt_final_p			date,
					ie_somente_repasse_p		varchar2,
					ie_tipo_titulo_p		varchar2,
					nr_seq_conta_financ_p		number,
					ie_tipo_relatorio_p		varchar2,
					ie_data_mes_p			varchar2) IS

dt_inicio_w				date;
dt_final_w				date;
cd_cgc_w					varchar2(14);
cd_pessoa_fisica_w			varchar2(10);
vl_saldo_ant_w				number(15,2);
vl_saldo_atual_w				number(15,2);
ie_tipo_w					integer;
dt_movimento_w				date;
nr_documento_w				varchar2(40);
vl_movimento_w				number(15,2);
vl_credito_w				number(15,2);
vl_debito_w				number(15,2);
nr_sequencia_w				number(10,0);
nr_titulo_ant_w				number(10,0);
cd_conta_contabil_w			varchar2(20);
nr_titulo_w				varchar2(20);
vl_saldo_anterior_w			number(15,2);
cd_tipo_pessoa_w			pessoa_juridica.cd_tipo_pessoa%type;
vl_saldo_movto_w			number(15,2);
ie_ordem_w				number(10,0)	:= 1;

/*Matheus OS 51243 em 02/03/2007 Inclusao de cd_pessoa_fisica */
cursor Fornecedor is
	select	distinct
		cd_cgc,
		cd_pessoa_fisica,
		substr(obter_conta_contab_pf_pj_estab(cd_estabelecimento_p,a.cd_cgc,a.cd_pessoa_fisica,'P',dt_inicial_p),1,100) conta_contabil
	from	titulo_pagar a
	where	dt_contabil < dt_final_w
	and	nvl(dt_liquidacao,sysdate) >= dt_inicio_w
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N')
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null);

cursor	Movimento is
select	ie_tipo,
	dt_movimento,
	ds_movimento,
	vl_movimento,
	nr_titulo
from	(select	1 ie_tipo,
		a.dt_baixa dt_movimento,
		substr(wheb_mensagem_pck.get_texto(302852) || a.nr_titulo || '/' || nvl(b.nr_cheque,substr(obter_dados_nota_fiscal(c.nr_seq_nota_fiscal,'0'),1,100)),1,40) ds_movimento,
		vl_baixa + ((select nvl(sum(x.vl_imposto),0) from titulo_pagar_imposto x where x.nr_titulo = c.nr_titulo and x.ie_pago_prev = 'V' and x.nr_seq_baixa is not null) * sign(vl_baixa)) vl_movimento,
		a.nr_titulo
	from 	bordero_pagamento b,
		titulo_pagar_baixa a,
		titulo_pagar c
	where	a.nr_titulo = c.nr_titulo
	and	a.nr_bordero = b.nr_bordero(+)
	and	((cd_cgc_w is not null and c.cd_cgc = cd_cgc_w) or (cd_pessoa_fisica_w is not null and c.cd_pessoa_fisica = cd_pessoa_fisica_w))
	and	a.dt_baixa between dt_inicio_w and dt_final_w
	and	(c.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((c.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(c.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(c.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	c.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	2 ie_tipo,
		nvl(b.dt_contabil, b.dt_atualizacao) dt_movimento,
		substr(wheb_mensagem_pck.get_texto(302897) || b.nr_titulo || '/' || nr_adiantamento,1,40),
		vl_adiantamento,
		a.nr_titulo
	from	titulo_pagar_adiant b,
		titulo_pagar a
	where	nvl(b.dt_contabil, b.dt_atualizacao) between dt_inicio_w and dt_final_w
	and	a.nr_titulo = b.nr_titulo
	and	(((cd_cgc_w is not null) and (a.cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (a.cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(a.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((a.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(a.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(a.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	3 ie_tipo,
		b.dt_alteracao dt_movimento,
		substr(wheb_mensagem_pck.get_texto(302898) || to_char(b.nr_titulo),1,40),
		abs(vl_anterior - vl_alteracao),
		a.nr_titulo
	from	titulo_pagar_alt_valor b,
		titulo_pagar a
	where	b.dt_alteracao between dt_inicio_w and dt_final_w
	and	(vl_anterior - vl_alteracao) > 0
	and	a.nr_titulo = b.nr_titulo
	and	(a.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	(((cd_cgc_w is not null) and (a.cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (a.cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	((a.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(a.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(a.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	4 ie_tipo,
		b.dt_alteracao dt_movimento,
		substr(wheb_mensagem_pck.get_texto(302899) || to_char(b.nr_titulo),1,40),
		abs(vl_anterior - vl_alteracao),
		a.nr_titulo
	from	titulo_pagar_alt_valor b,
		titulo_pagar a
	where	b.dt_alteracao between dt_inicio_w and dt_final_w
	and	(vl_anterior - vl_alteracao) < 0
	and	a.nr_titulo = b.nr_titulo
	and	(((cd_cgc_w is not null) and (a.cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (a.cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(a.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((a.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(a.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(a.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	5 ie_tipo,
		nvl(dt_contabil, dt_emissao) dt_movimento,
		substr(wheb_mensagem_pck.get_texto(302900) || nr_titulo || '/' || nvl(nr_documento,substr(obter_dados_nota_fiscal(nr_seq_nota_fiscal,'0'),1,100)),1,40),
		/*to_number(obter_dados_tit_pagar(nr_titulo,'VT')),  Francisco - 24/08/11 - Tem que considerar por data, troquei por outra function*/
		obter_valores_tit_pagar(a.nr_titulo,dt_inicio_w,'VOT'),
		a.nr_titulo
	from	titulo_pagar a
	where	nvl(dt_contabil, dt_emissao) between dt_inicio_w and dt_final_w
	and	(((cd_cgc_w is not null) and (cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null))
order by	dt_movimento, ie_tipo;
					  	-- Edgar/Francisco 09/03/2006 OS 30065, titulos desdobrados dao lugar a outros...
						-- Anderson 26/09/2006 OS 39921 - inclui o parametro dos titulos cancelados.

BEGIN

dt_inicio_w	:= nvl(dt_inicial_p,trunc(dt_parametro_p, 'month'));
dt_final_w	:= (trunc(nvl(nvl(dt_final_p,trunc(last_day(dt_parametro_p),'dd')),sysdate),'dd') + 86399/86400);

select	max(nr_titulo)
into	nr_titulo_ant_w
from	titulo_pagar a
where	dt_contabil			< dt_final_w
and	nvl(dt_liquidacao,sysdate)	>= dt_inicio_w
and	cd_estabelecimento		= cd_estabelecimento_p
and		exists (select	1
				from 	titulo_pagar_baixa x
				where	a.nr_titulo = x.nr_titulo
				and	(((x.dt_baixa < trunc(nvl(a.dt_contabil, a.dt_emissao), 'dd')) and ('N' = ie_data_mes_p))
					or ((x.dt_baixa < trunc(nvl(a.dt_contabil, a.dt_emissao), 'mm')) and (trunc(dt_parametro_p,'mm') = trunc(nvl(a.dt_contabil, a.dt_emissao), 'mm')) and ('S' = ie_data_mes_p))));

/*if	(nr_titulo_ant_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(231971,'NR_TITULO='||nr_titulo_ant_w);

end if;*/

delete from t_razao_fornecedor;

open Fornecedor;
loop
	fetch Fornecedor into
			cd_cgc_w,
			cd_pessoa_fisica_w,
			cd_conta_contabil_w;
	exit when Fornecedor%notfound;

	/* obter saldo anterior */

	select	sum(obter_saldo_titulo_pagar(nr_titulo,dt_inicio_w - 1/86400)) vl_credito,
		substr(obter_dados_pf_pj(NULL, max(cd_cgc), 'TP'),1,255) cd_tipo_pessoa
	into	vl_saldo_anterior_w,
		cd_tipo_pessoa_w
	from	titulo_pagar
	where	(	(cd_cgc_w is not null and cd_cgc = cd_cgc_w) or
			(cd_pessoa_fisica_w is not null and cd_pessoa_fisica = cd_pessoa_fisica_w)
		)
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	nvl(dt_contabil, dt_emissao) < dt_inicio_w
	and	(	(ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
			(ie_situacao <> 'D' and 'S' = ie_considera_cancel_p)
		)
	and     (nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null);

	ie_ordem_w	:= ie_ordem_w + 1;

	insert into T_Razao_Fornecedor(
		nr_sequencia,
		cd_cgc,
		cd_pessoa_fisica,
		ie_tipo,
		dt_atualizacao,
		nm_usuario,
		dt_movimento,
		nr_documento,
		vl_credito,
		vl_debito,
		cd_tipo_pessoa,
		CD_CONTA_CONTABIL,
		vl_saldo_atual,
		ie_ordem)
	values	(T_Razao_Fornecedor_seq.nextval,
		cd_cgc_w,
		cd_pessoa_fisica_w,
		0,
		sysdate,
		nm_usuario_p,
		dt_inicio_w,
		wheb_mensagem_pck.get_texto(302901),
		vl_saldo_anterior_w,
		0,
		cd_tipo_pessoa_w,
		cd_conta_contabil_w,
		vl_saldo_anterior_w,
		ie_ordem_w);

	if	(nvl(ie_tipo_relatorio_p,'N') = 'N') then

		vl_saldo_movto_w	:= vl_saldo_anterior_w;

		open Movimento;
		loop
			fetch Movimento into	ie_tipo_w,
						dt_movimento_w,
						nr_documento_w,
						vl_movimento_w,
						nr_titulo_w;
			exit when Movimento%notfound;

			vl_credito_w := 0;
			vl_debito_w := 0;

			if (ie_tipo_w <= 3) then
				vl_debito_w	:= vl_movimento_w;
			else
				vl_credito_w	:= vl_movimento_w;
			end if;

			select  T_Razao_Fornecedor_seq.nextval
			into	nr_sequencia_w
			from	dual;

			if	(cd_conta_contabil_w is null) then
				select 	substr(obter_conta_contab_pf_pj_estab(cd_estabelecimento_p,cd_cgc_w,cd_pessoa_fisica_w,'P',dt_movimento_w),1,100)
				into	cd_conta_contabil_w
				from	dual;
			end if;

			vl_saldo_movto_w	:= nvl(vl_saldo_movto_w,0) + nvl(vl_credito_w,0) - nvl(vl_debito_w,0);

			ie_ordem_w	:= ie_ordem_w + 1;

			begin
			insert into T_Razao_Fornecedor(
				nr_sequencia,
				cd_cgc,
				cd_pessoa_fisica,
				ie_tipo,
				dt_atualizacao,
				nm_usuario,
				dt_movimento,
				nr_documento,
				vl_credito,
				vl_debito,
				cd_tipo_pessoa,
				cd_conta_contabil,
				nr_titulo,
				vl_saldo_atual,
				ie_ordem)
			values (nr_sequencia_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				ie_tipo_w,
				sysdate,
				nm_usuario_p,
				dt_movimento_w,
				nr_documento_w,
				vl_credito_w,
				vl_debito_w,
				obter_dados_pf_pj(NULL, cd_cgc_w, 'TP'),
				cd_conta_contabil_w,
				nr_titulo_w,
				vl_saldo_movto_w,
				ie_ordem_w);
			exception
				when others then
					dbms_output.put_line(vl_credito_w || '|' || vl_debito_w);
			end;
		end loop;
		close Movimento;
	end if;

	/*	Edgar 27/08/2008, retirei este bloco
	select	T_Razao_Fornecedor_seq.nextval
	into	nr_sequencia_w
	from	dual;
	*/
	ie_ordem_w	:= ie_ordem_w + 1;
	/* obter saldo atual */
	insert into T_Razao_Fornecedor(
		nr_sequencia,
		cd_cgc,
		cd_pessoa_fisica,
		ie_tipo,
		dt_atualizacao,
		nm_usuario,
		dt_movimento,
		nr_documento,
		vl_credito,
		vl_debito,
		cd_tipo_pessoa,
		cd_conta_contabil,
		vl_saldo_atual,
		ie_ordem)
	select	T_Razao_Fornecedor_seq.nextval,
		a.*,
		ie_ordem_w
	from
	(select	cd_cgc_w cd_cgc,
		cd_pessoa_fisica_w cd_pessoa_fisica,
		99 ie_tipo,
		sysdate dt_atualizacao,
		nm_usuario_p nm_usuario,
		dt_final_w dt_mocimento,
		wheb_mensagem_pck.get_texto(302902) nr_documento,
		sum(obter_saldo_titulo_pagar(nr_titulo,dt_final_w)) vl_credito,
		0 vl_debito,
		obter_dados_pf_pj(NULL, cd_cgc, 'TP') cd_tipo_pessoa,
		substr(obter_conta_contab_pf_pj_estab(cd_estabelecimento_p,cd_cgc,cd_pessoa_fisica,'P',dt_inicial_p),1,100) CD_CONTA_CONTABIL,
		sum(obter_saldo_titulo_pagar(nr_titulo,dt_final_w)) vl_saldo_atual
	from 	titulo_pagar
	where	(((cd_cgc_w is not null) and (cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	nvl(dt_contabil, dt_emissao) 	<= dt_final_w
	and	((ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	group by obter_dados_pf_pj(NULL, cd_cgc, 'TP'),
		substr(obter_conta_contab_pf_pj_estab(cd_estabelecimento_p,cd_cgc,cd_pessoa_fisica,'P',dt_inicial_p),1,100)) a;

/*	and	nvl(dt_liquidacao,sysdate) >= dt_inicio_w; Fabio 07/03/2005 OS15754 */
end loop;
close Fornecedor;

commit;

END ABNSN_Gerar_Razao_Fornecedor;
/
