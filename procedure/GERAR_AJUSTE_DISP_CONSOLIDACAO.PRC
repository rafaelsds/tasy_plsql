create or replace
procedure gerar_ajuste_disp_consolidacao(nm_usuario_p		varchar2,
										 cd_estabelecimento_p number,
										 ds_lista_sequencia_p varchar2,
										 nr_prescricao_p	number) is 

qt_pos_antes_virgula_w  	number(10,0);
qt_pos_depois_virgula_w  	number(10,0);
ds_lista_repartida_w 		varchar2(255);
nr_sequencia_w				number(10,0);
nr_sequencia_w2				varchar2(255);
i							number(10,0);
										 
begin
if (ds_lista_sequencia_p is not null) then
		ds_lista_repartida_w 		:= ds_lista_sequencia_p;
		for i in 0..length(ds_lista_repartida_w) loop
				if (instr(ds_lista_repartida_w,'#') > 0) then
						qt_pos_antes_virgula_w 		:= (instr(ds_lista_repartida_w,'#') - 1);
						qt_pos_depois_virgula_w 	:= (instr(ds_lista_repartida_w,'#') + 1);
						nr_sequencia_w 				:= to_number(substr(ds_lista_repartida_w,1,qt_pos_antes_virgula_w));
						ds_lista_repartida_w		:= substr(ds_lista_repartida_w,qt_pos_depois_virgula_w,100);
					
						ajustar_dispensacao_total(	nr_sequencia_w,
													nm_usuario_p,
													nr_prescricao_p,
													cd_estabelecimento_p,
													'I');				
				end if;
		end loop;		
end if;

end gerar_ajuste_disp_consolidacao;
/
