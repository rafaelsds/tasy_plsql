create or replace procedure envia_comunic_falta_entrega_oc(	nr_seq_nf_p		number,
						nm_usuario_p		varchar2) is

cd_cgc_emitente_w		varchar2(14);
nr_ordem_compra_w		number(10);
cd_estabelecimento_w		number(4);
ds_razao_social_w		pessoa_juridica.ds_razao_social%type;
ds_email_w			varchar2(255);
ds_pessoa_contato_w		varchar2(255);
nr_ddd_telefone_w		varchar2(3);
nr_telefone_w			varchar2(15);
cd_material_w			number(6);
ds_material_w			varchar2(255);
qt_prevista_entrega_w		number(13,4);
qt_real_entrega_w		number(13,4);
cd_setor_atendimento_w		number(10);
nr_sequencia_w			number(10);
cd_perfil_w			number(10);
ie_ci_lida_w			varchar2(1);
nm_usuario_destino_w		varchar2(255);
ds_titulo_w			varchar2(80);
nr_nota_fiscal_w		varchar2(255);
nr_seq_comunic_w		number(10);
nr_seq_classif_w		number(10);
qt_regra_usuario_w		number(10);
ds_lista_materiais_w		long;
ds_comunic_w			long;
dt_prev_entrega_w		date;
dt_real_entrega_w		date;
/* Se tiver setor na regra, envia CI para os setores */
ds_setor_adicional_w            varchar2(2000) := '';
/* Campos da regra Usuario da Regra */
cd_setor_regra_usuario_w	number(5);

cursor c01 is
select	distinct nr_ordem_compra
from (	select	nr_ordem_compra
	from	nota_fiscal
	where	nr_sequencia = nr_seq_nf_p
	and	nr_ordem_compra is not null
	union
	select	nr_ordem_compra
	from	nota_fiscal_item
	where	nr_sequencia = nr_seq_nf_p
	and	nr_ordem_compra is not null);

cursor c02 is
select	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,255),
	b.qt_prevista_entrega,
	nvl(b.qt_real_entrega,0),
	trunc(b.dt_prevista_entrega,'dd'),	
	trunc(sysdate,'dd')
from	ordem_compra_item a,
	ordem_compra_item_entrega b
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_item_oci = b.nr_item_oci
and	a.nr_ordem_compra = nr_ordem_compra_w
and	b.dt_cancelamento is null
and	((nvl(b.qt_real_entrega,0) < b.qt_prevista_entrega) or 
	(trunc(b.dt_prevista_entrega,'dd') < trunc(sysdate,'dd')))
order by a.nr_item_oci;

Cursor C03 is
select	b.nr_sequencia,
	b.cd_perfil
from	regra_envio_comunic_compra a,
	regra_envio_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 917
and	b.cd_evento = 37
and	b.ie_situacao = 'A'
and	a.cd_estabelecimento = cd_estabelecimento_w
and	(((cd_setor_atendimento_w is not null) and (b.cd_setor_destino = cd_setor_atendimento_w)) or
	((cd_setor_atendimento_w is null) and (b.cd_setor_destino is null)) or (b.cd_setor_destino is null))
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,nr_seq_nf_p,'NF',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';

cursor c05 is
select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
from	regra_envio_comunic_usu a
where	a.nr_seq_evento = nr_sequencia_w;

begin

select	nvl(cd_cgc_emitente,'X'),
	cd_estabelecimento,
	cd_setor_digitacao,
	nr_nota_fiscal
into	cd_cgc_emitente_w,
	cd_estabelecimento_w,
	cd_setor_atendimento_w,
	nr_nota_fiscal_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nf_p;

if	(cd_cgc_emitente_w <> 'X') then
	begin
	select	p.ds_razao_social,
		e.ds_email,
		e.nm_pessoa_contato,
		p.nr_ddd_telefone,
		p.nr_telefone	
	into	ds_razao_social_w,
		ds_email_w,
		ds_pessoa_contato_w,
		nr_ddd_telefone_w,
		nr_telefone_w
	from	pessoa_juridica_estab e,
		pessoa_juridica p
	where	p.cd_cgc = e.cd_cgc
	and	p.cd_cgc = cd_cgc_emitente_w
	and	e.cd_estabelecimento = cd_estabelecimento_w;
	exception when others then
		ds_razao_social_w := '';
		ds_email_w := '';
		ds_pessoa_contato_w := '';
		nr_ddd_telefone_w := '';
		nr_telefone_w := '';
	end;
end if;

open C01;
loop
fetch C01 into
	nr_ordem_compra_w;
exit when C01%notfound;
	begin

	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		ds_material_w,
		qt_prevista_entrega_w,
		qt_real_entrega_w,
		dt_prev_entrega_w,
		dt_real_entrega_w;
	exit when C02%notfound;
		begin	
		ds_lista_materiais_w := substr(ds_lista_materiais_w	|| cd_material_w || ' - ' || ds_material_w	|| 
					' - '||obter_desc_expressao(646487) || ': '	|| campo_mascara_virgula(qt_prevista_entrega_w)	|| 
					' - '||obter_desc_expressao(335685) || ': '	|| campo_mascara_virgula(qt_real_entrega_w)	|| 
					' - '||obter_desc_expressao(645628) || ': '	|| dt_prev_entrega_w	|| 
					' - '||obter_desc_expressao(731615) || ': '	|| dt_real_entrega_w	|| chr(13) || chr(10),1,32000);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

if	(ds_lista_materiais_w is not null) then

	open C03;
	loop
	fetch C03 into	
		nr_sequencia_w,
		cd_perfil_w;
	exit when C03%notfound;
		begin
		
		select	count(*)
		into	qt_regra_usuario_w
		from	regra_envio_comunic_compra a,
			regra_envio_comunic_evento b,
			regra_envio_comunic_usu c
		where	a.nr_sequencia = b.nr_seq_regra
		and	b.nr_sequencia = c.nr_seq_evento
		and	b.nr_sequencia = nr_sequencia_w;

		select	nvl(ie_ci_lida,'N')
		into	ie_ci_lida_w
		from 	regra_envio_comunic_evento
		where 	nr_sequencia = nr_sequencia_w;

		if	(qt_regra_usuario_w > 0) then
			open C05;
			loop
			fetch C05 into	
				cd_setor_regra_usuario_w;
			exit when C05%notfound;
				begin
				if	(cd_setor_regra_usuario_w <> 0) and
					(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
					ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
				end if;
				end;
			end loop;
			close C05;

			nm_usuario_destino_w	:= '';
			nm_usuario_destino_w	:= obter_usuarios_comunic_compras(nr_ordem_compra_w,null,37,nr_sequencia_w,'');
			ds_titulo_w		:= '';
			ds_comunic_w		:= '';	

			select	substr(ds_titulo,1,80),
				ds_comunicacao
			into	ds_titulo_w,
				ds_comunic_w
			from	regra_envio_comunic_evento
			where	nr_sequencia = nr_sequencia_w;

			ds_comunic_w	:= replace(
					replace(
					replace(
					replace(
					replace(
					replace(
					replace(
					replace(
					replace(ds_comunic_w,
						'@nota', nr_nota_fiscal_w),
						'@ordem', nr_ordem_compra_w),
						'@cd_cgc_fornec', cd_cgc_emitente_w),
						'@fornecedor', ds_razao_social_w),
						'@lista_materiais', ds_lista_materiais_w),
						'@e-mail', ds_email_w),
						'@ddd', nr_ddd_telefone_w),
						'@fone', nr_telefone_w),
						'@contato', ds_pessoa_contato_w);

			if	(nm_usuario_destino_w is not null) then

				select	comunic_interna_seq.nextval
				into	nr_seq_comunic_w
				from	dual;

				select	obter_classif_comunic('F')
				into	nr_seq_classif_w
				from	dual;	

				if	(cd_perfil_w is not null) then
					cd_perfil_w := cd_perfil_w ||',';
				end if;

				insert	into comunic_interna(
					cd_estab_destino,
					dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					dt_atualizacao,
					ie_geral,
					nm_usuario_destino,
					nr_sequencia,
					ie_gerencial,
					nr_seq_classif,
					dt_liberacao,
					ds_perfil_adicional,
					ds_setor_adicional)
				values(	cd_estabelecimento_w,
					sysdate,
					ds_titulo_w,
					ds_comunic_w,
					nm_usuario_p,
					sysdate,
					'N',
					nm_usuario_destino_w,
					nr_seq_comunic_w,
					'N',
					nr_seq_classif_w,
					sysdate,
					cd_perfil_w,
					ds_setor_adicional_w);

				if	(ie_ci_lida_w = 'S') then
					insert into comunic_interna_lida(
						nr_sequencia,
						nm_usuario,
						dt_atualizacao)
					values(	nr_seq_comunic_w,
						nm_usuario_p,
						sysdate);
				end if;
			end if;
		end if;
		end;
	end loop;
	close C03;
end if;

end envia_comunic_falta_entrega_oc;
/
