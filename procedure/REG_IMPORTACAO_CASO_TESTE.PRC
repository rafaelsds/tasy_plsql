create or replace procedure reg_importacao_caso_teste(nr_seq_intencao_uso_p in number,
                                                      prs_id_p              in varchar2,
                                                      nm_usuario_p          in varchar2,
                                                      nr_tempo_estimado_p   in number,
                                                      ie_tipo_execucao_p    in varchar2,
                                                      cd_versao_p           in varchar2,
                                                      ds_descricao_p        in varchar2,
                                                      ds_pre_condicao_p     in varchar2,
                                                      cd_ct_id_p            out varchar2,
                                                      nr_seq_test_case_p    out number) is

  nr_seq_product_w   reg_product_requirement.nr_sequencia%type;
  nr_seq_test_case_w reg_caso_teste.nr_sequencia%type;
  cd_ct_id_w         reg_caso_teste.cd_ct_id%type;

begin

  select nr_sequencia
    into nr_seq_product_w
    from reg_product_requirement
   where nr_seq_intencao_uso = nr_seq_intencao_uso_p
     and cd_prs_id = prs_id_p;

  if nr_seq_product_w is null then
    wheb_mensagem_pck.exibir_mensagem_abort(240227);
  end if;

  select reg_caso_teste_seq.nextval into nr_seq_test_case_w from dual;

  insert into reg_caso_teste
    (cd_versao,
     ds_descricao,
     ds_pre_condicao,
     dt_aprovacao,
     dt_atualizacao,
     dt_liberacao_vv,
     ie_tipo_documento,
     ie_tipo_execucao,
     nm_usuario,
     nm_usuario_aprovacao,
     nm_usuario_liberacao,
     nm_usuario_nrec,
     nr_seq_intencao_uso,
     nr_seq_product,
     nr_sequencia,
     nr_tempo_estimado)
  values
    (cd_versao_p,
     ds_descricao_p,
     ds_pre_condicao_p,
     sysdate,
     sysdate,
     sysdate,
     'T',
     ie_tipo_execucao_p,
     nm_usuario_p,
     nm_usuario_p,
     nm_usuario_p,
     nm_usuario_p,
     nr_seq_intencao_uso_p,
     nr_seq_product_w,
     nr_seq_test_case_w,
     nr_tempo_estimado_p);

  commit;

  select cd_ct_id
    into cd_ct_id_w
    from reg_caso_teste
   where nr_sequencia = nr_seq_test_case_w;

  nr_seq_test_case_p := nr_seq_test_case_w;
  cd_ct_id_p         := cd_ct_id_w;

end reg_importacao_caso_teste;
/