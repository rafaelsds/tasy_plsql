create or replace
procedure obter_texto_primeiro_exame(nr_seq_proc_interno_p	number,
				     cd_procedimento_p		number,
				     ie_origem_proced_p		number,
				     cd_estabelecimento_p 	number,
				     nm_usuario_p		varchar2,
				     ie_primeiro_exame_p	varchar2,
				     ds_texto_retorno_p		out varchar2,
				     cd_convenio_p		number,
				     cd_categoria_p		varchar2,
				     cd_plano_p			varchar2,
				     nr_seq_grupo_p		number,
				     ie_resumo_p		varchar2,
				     ie_resumo_long_p		varchar2,
				     cd_pessoa_fisica_p		varchar2) is
			
nr_seq_regra_w		number(10,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
ds_texto_retorno_w	varchar2(2000) := '';
qt_reg_w		number(10,0);
cd_grupo_proced_w	number(15,0);
cd_espec_proced_w	number(15,0);
cd_area_proced_w	number(15,0);
ds_texto_w		varchar2(2000);
ds_enter_w		varchar2(255);
nr_seq_grupo_w		number(10,0);
cd_medico_w		varchar2(10);

			
Cursor C01 is
	select	a.nr_sequencia
	from	ageint_texto_padrao_proc a
	where	((cd_area_procedimento = cd_area_proced_w) or (cd_area_procedimento is null))
	and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))
	and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))
	and	((cd_procedimento = cd_procedimento_p) or (cd_procedimento is null))
	and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_p) or (ie_origem_proced is null)))
	and	((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
	and	(nvl(cd_convenio, cd_convenio_p)	= cd_convenio_p or (cd_convenio_p is null and cd_convenio is null))
	and	(nvl(cd_convenio, cd_categoria_p)	= cd_categoria_p or (cd_categoria_p is null and cd_categoria is null))
	and	(nvl(cd_convenio, cd_plano_p)		= cd_plano_p or (cd_plano_p is null and cd_plano is null))
	and	(nvl(nr_seq_grupo, nr_seq_grupo_w)	= nr_seq_grupo_w)
	and	((ie_primeiro_exame_p = 'S' and ie_somente_pri_exame ='S') or (ie_primeiro_exame_p = 'N' and ie_somente_pri_exame = 'N'))
	and	((nvl(ie_email, 'S')	= 'S' and ie_resumo_p	= 'S') or ie_resumo_p =	'N')
	and	nvl(a.cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and	(nvl(cd_pessoa_fisica, cd_pessoa_fisica_p) = cd_pessoa_fisica_p or (cd_pessoa_fisica_p is null))
	order by a.nr_sequencia;

begin
if	(ie_resumo_long_p	= 'N') then
	ds_enter_w	:= chr(10);
else
	ds_enter_w	:= '\par ';
end if;
select	count(*)
into	qt_reg_w
from	ageint_texto_padrao_proc;
	
nr_seq_grupo_w	:= nvl(nr_seq_grupo_p,0);	
	
if	(qt_reg_w > 0) then	

	select	nvl(max(a.cd_area_procedimento),0),
		nvl(max(a.cd_especialidade),0),
		nvl(max(a.cd_grupo_proc),0)
	into	cd_area_proced_w,
		cd_espec_proced_w,
		cd_grupo_proced_w
	from	estrutura_procedimento_v a
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_w;
	exit when C01%notfound;
		begin
			select	substr(max(ds_texto),1,2000)
			into	ds_texto_w
			from 	ageint_texto_padrao_proc
			where	nr_sequencia = nr_seq_regra_w;
			
			ds_texto_retorno_w	:= substr(ds_texto_retorno_w || ds_enter_w || ds_texto_w,1,2000);
		end;
	end loop;
	close C01;

end if;
ds_texto_retorno_p := ds_texto_retorno_w;
end obter_texto_primeiro_exame;
/