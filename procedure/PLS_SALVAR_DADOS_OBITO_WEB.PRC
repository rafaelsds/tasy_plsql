create or replace
procedure pls_salvar_dados_obito_web(
					nr_seq_conta_p			number,					
					cd_doenca_obito_p		varchar2,
					nr_declaracao_obito_p		varchar2,
					nm_usuario_p			varchar2,
					ie_indicador_dorn_p		varchar2,
					nr_seq_obito_p		in out	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Receber os valores para  inseri-los na tabela respesctiva.
A Nr_Sequencia deve ser null para se adequar a forma ao contexto no qual � usado a procedure.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_obito_w	Number(10,0) := null;

begin
	
if	(nr_seq_obito_p is null and (cd_doenca_obito_p is not null or nr_declaracao_obito_p is not null)) then
	select	pls_diagnost_conta_obito_seq.nextval
	into	nr_seq_obito_w
	from	dual;
	
	insert into	pls_diagnost_conta_obito(
			nr_sequencia,nr_seq_conta,dt_atualizacao,
			nm_usuario,nm_usuario_nrec,nr_declaracao_obito,
			dt_atualizacao_nrec,cd_doenca, ie_indicador_dorn)
		values(	nr_seq_obito_w, nr_seq_conta_p, sysdate,
			nm_usuario_p,nm_usuario_p,nr_declaracao_obito_p,
			sysdate,cd_doenca_obito_p, ie_indicador_dorn_p);
else
	delete from 	pls_diagnost_conta_obito
	where		nr_sequencia = nr_seq_obito_p;	
end if;

nr_seq_obito_p	:= nr_seq_obito_w;

commit;

end pls_salvar_dados_obito_web;
/