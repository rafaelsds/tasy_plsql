CREATE OR REPLACE
PROCEDURE INSERIR_PESSOA_DOMICILIO(     NM_USUARIO_P VARCHAR2,
					CD_PESSOA_FISICA_P       VARCHAR2,
					NR_SEQ_DOMICILIO_P       NUMBER DEFAULT NULL,
					NR_SEQ_GRAU_PARENTESCO_P NUMBER DEFAULT NULL,
					ACAO_P VARCHAR2 DEFAULT 'OUTRA',
					NM_CONTATO_P VARCHAR2 DEFAULT NULL,
					DS_BAIRRO_P  VARCHAR2 DEFAULT NULL,
					DS_COMPLEMENTO_P  VARCHAR2 DEFAULT NULL,
					DS_ENDERECO_P  VARCHAR2 DEFAULT NULL,
					NR_ENDERECO_P  VARCHAR2 DEFAULT NULL,
					DS_MUNICIPIO_P  VARCHAR2 DEFAULT NULL,
					NR_TELEFONE_P  VARCHAR2 DEFAULT NULL,
					CD_CEP_P  VARCHAR2 DEFAULT NULL,
					DS_REFERENCIA_P VARCHAR DEFAULT NULL,
					IE_TIPO_DOMICILIO_P VARCHAR DEFAULT NULL,
					IE_TRATAMENTO_AGUA_P VARCHAR DEFAULT NULL,
					IE_TIPO_AGUA_P VARCHAR DEFAULT NULL,
					IE_DESTINO_LIXO_P VARCHAR DEFAULT NULL,
					IE_ESGOTO_SANITARIO_P VARCHAR DEFAULT NULL,
					IE_TIPO_COMUNICACAO_P VARCHAR DEFAULT NULL,
					IE_TIPO_DOENCA_P VARCHAR DEFAULT NULL,
					IE_TIPO_TRANSPORTE_P VARCHAR DEFAULT NULL,
					IE_ENERGIA_ELETRICA_P VARCHAR DEFAULT NULL,
					DS_REF_RESIDENCIAL_P VARCHAR DEFAULT NULL,
					NR_DDI_RESIDENCIAL_P VARCHAR DEFAULT NULL,
					NR_SEQ_AREA_P NUMBER DEFAULT NULL,
					NR_SEQ_EQUIPE_P NUMBER DEFAULT NULL,
					NR_SEQ_MICROAREA_P NUMBER DEFAULT NULL,
					QT_COMODO_P NUMBER DEFAULT NULL,
					NR_SEQ_TIPO_LOGRADOURO_P NUMBER DEFAULT NULL,
					CD_DOMICILIO_P NUMBER DEFAULT NULL,
					INSERIR_SI_PROPRIO_P VARCHAR2 DEFAULT 'N')IS
                
  

NM_USUARIO_ATIVO_W USUARIO.NM_USUARIO%TYPE;
MEMBROSDOMICILIO   NUMBER;
QT_EXISTE          NUMBER;
NR_SEQ_DOMICILIO_W NUMBER;
NR_SEQ_PESSOA_DOMICILIO_W NUMBER;
NR_SEQUENCIA_COMPL_W NUMBER;

DS_BAIRRO_W VARCHAR(30);
DS_COMPLEMENTO_W VARCHAR2(15);
DS_ENDERECO_W VARCHAR2(50);
NR_ENDERECO_W VARCHAR2(7);
CD_DOMICILIO_MUNICIPIO_W VARCHAR2(48);
NR_TEL_RESIDENCIAL_W VARCHAR2(50);
CD_CEP_W VARCHAR2(15);
NM_PESSOA_RESPONSAVEL_W VARCHAR2(255);
DS_REFERENCIA_W VARCHAR2(50);
IE_TIPO_DOMICILIO_W VARCHAR2(1); 
IE_TRATAMENTO_AGUA_W VARCHAR2(1); 
IE_TIPO_AGUA_W VARCHAR2(1); 
IE_DESTINO_LIXO_W VARCHAR2(1); 
IE_ESGOTO_SANITARIO_W VARCHAR2(1); 
IE_TIPO_COMUNICACAO_W VARCHAR2(1); 
IE_TIPO_DOENCA_W VARCHAR2(1); 
IE_TIPO_TRANSPORTE_W VARCHAR2(1);
IE_ENERGIA_ELETRICA_W VARCHAR2(1); 
DS_REF_RESIDENCIAL_W VARCHAR2(50);
NR_DDI_RESIDENCIAL_W VARCHAR2(3);
NR_SEQ_AREA_W  NUMBER(10);
NR_SEQ_EQUIPE_W  NUMBER(10);
NR_SEQ_MICROAREA_W NUMBER(10);
QT_COMODO_W NUMBER(2);
NR_SEQ_TIPO_LOGRADOURO_W NUMBER(10);
CD_DOMICILIO_W NUMBER;
tag_pais_w	varchar2(15);
  


CURSOR C02 IS /*UTILIZADO PARA PREENCHER DADOS DA MORADIA*/
	SELECT DISTINCT A.CD_PESSOA_FISICA
	FROM DOMICILIO_FAMILIA A
	WHERE CD_DOMICILIO = CD_DOMICILIO_P
	AND CD_PESSOA_FISICA NOT IN(SELECT B.CD_PESSOA_FAMILIA
					FROM PESSOA_DOMICILIO_MORADIA B
					WHERE B.NR_SEQ_DOMICILIO = CD_DOMICILIO_P
					AND B.CD_PESSOA_FAMILIA = A.CD_PESSOA_FISICA); 

              
CURSOR C01 IS /*UTILIZADO PARA PREENCHER DADOS AO VINCULAR DOMICILIOS DE UM PACIENTE PARA OUTRO.*/
SELECT  DS_BAIRRO,
        DS_COMPLEMENTO,
        DS_ENDERECO,
        NR_ENDERECO,
        CD_DOMICILIO_MUNICIPIO,
        NR_TEL_RESIDENCIAL,
        CD_CEP,
        NM_PESSOA_RESPONSAVEL,
        DS_REFERENCIA,
        IE_TIPO_DOMICILIO,
        IE_TRATAMENTO_AGUA,
        IE_TIPO_AGUA,
        IE_DESTINO_LIXO,
        IE_ESGOTO_SANITARIO,
        IE_TIPO_COMUNICACAO,
        IE_TIPO_DOENCA,
        IE_TIPO_TRANSPORTE,
        IE_ENERGIA_ELETRICA,
        DS_REF_RESIDENCIAL,
        NR_DDI_RESIDENCIAL,
        NR_SEQ_AREA,
        NR_SEQ_EQUIPE,
        NR_SEQ_MICROAREA,
        QT_COMODO,
        NR_SEQ_TIPO_LOGRADOURO,
        CD_DOMICILIO
        FROM DOMICILIO_FAMILIA
	WHERE CD_DOMICILIO = CD_DOMICILIO_P
	AND NR_SEQUENCIA = CD_DOMICILIO;

BEGIN

OPEN C01;
	FETCH C01 INTO
	DS_BAIRRO_W,
	DS_COMPLEMENTO_W,
	DS_ENDERECO_W,
	NR_ENDERECO_W,
	CD_DOMICILIO_MUNICIPIO_W,
	NR_TEL_RESIDENCIAL_W,
	CD_CEP_W,
	NM_PESSOA_RESPONSAVEL_W,
	DS_REFERENCIA_W,
	IE_TIPO_DOMICILIO_W,
	IE_TRATAMENTO_AGUA_W,
	IE_TIPO_AGUA_W,
	IE_DESTINO_LIXO_W,
	IE_ESGOTO_SANITARIO_W,
	IE_TIPO_COMUNICACAO_W,
	IE_TIPO_DOENCA_W,
	IE_TIPO_TRANSPORTE_W,
	IE_ENERGIA_ELETRICA_W,
	DS_REF_RESIDENCIAL_W,
	NR_DDI_RESIDENCIAL_W,
	NR_SEQ_AREA_W,
	NR_SEQ_EQUIPE_W,
	NR_SEQ_MICROAREA_W,
	QT_COMODO_W,
	NR_SEQ_TIPO_LOGRADOURO_W,
	CD_DOMICILIO_W;
CLOSE C01;

select	max(ds_locale) 
into	tag_pais_w
from	user_locale 
where	nm_user = NM_USUARIO_P;

IF ACAO_P = 'INSERIR_RESIDENCIA' THEN
	  
	SELECT	NVL(MAX(NR_SEQUENCIA),0) + 1
		INTO	NR_SEQUENCIA_COMPL_W
		FROM	COMPL_PESSOA_FISICA
		WHERE	CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
	
	if(tag_pais_w = 'de_DE')then
		INSERT INTO COMPL_PESSOA_FISICA (NR_SEQUENCIA,
						CD_PESSOA_FISICA,
						IE_TIPO_COMPLEMENTO,
						DT_ATUALIZACAO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO,
						NM_USUARIO_NREC,
						CD_CEP,
						DS_BAIRRO,
						DS_COMPLEMENTO,
						DS_MUNICIPIO,
						DS_COMPL_END,
						DS_ENDERECO)
					 VALUES(NR_SEQUENCIA_COMPL_W,
						CD_PESSOA_FISICA_P,
						'1',
						SYSDATE,
						SYSDATE,
						NM_USUARIO_P,
						NM_USUARIO_P,
						CD_CEP_W,
						DS_BAIRRO_W,
						DS_COMPLEMENTO_W,
						CD_DOMICILIO_MUNICIPIO_W,
						NR_ENDERECO_W,
						DS_ENDERECO_W);
						COMMIT;
	else
		INSERT INTO COMPL_PESSOA_FISICA (NR_SEQUENCIA,
						CD_PESSOA_FISICA,
						IE_TIPO_COMPLEMENTO,
						DT_ATUALIZACAO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO,
						NM_USUARIO_NREC,
						CD_CEP,
						DS_BAIRRO,
						DS_COMPLEMENTO,
						DS_MUNICIPIO,
						NR_ENDERECO,
						DS_ENDERECO)
					 VALUES(NR_SEQUENCIA_COMPL_W,
						CD_PESSOA_FISICA_P,
						'1',
						SYSDATE,
						SYSDATE,
						NM_USUARIO_P,
						NM_USUARIO_P,
						CD_CEP_W,
						DS_BAIRRO_W,
						DS_COMPLEMENTO_W,
						CD_DOMICILIO_MUNICIPIO_W,
						NR_ENDERECO_W,
						DS_ENDERECO_W);
						COMMIT;
	end if;

ELSIF ACAO_P = 'ALTERAR_RESIDENCIA' THEN
	if(tag_pais_w = 'de_DE')then
		UPDATE COMPL_PESSOA_FISICA
			SET  DT_ATUALIZACAO = SYSDATE,
			DT_ATUALIZACAO_NREC = SYSDATE,
			NM_USUARIO = NM_USUARIO_P,
			NM_USUARIO_NREC = NM_USUARIO_P,
			CD_CEP = CD_CEP_W,
			DS_BAIRRO = DS_BAIRRO_W,
			DS_COMPLEMENTO = DS_COMPLEMENTO_W,
			DS_MUNICIPIO = CD_DOMICILIO_MUNICIPIO_W,
			DS_COMPL_END = NR_ENDERECO_W,
			DS_ENDERECO = DS_ENDERECO_W
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P
				AND IE_TIPO_COMPLEMENTO  = 1;
			COMMIT;
	else
		UPDATE COMPL_PESSOA_FISICA
			SET  DT_ATUALIZACAO = SYSDATE,
			DT_ATUALIZACAO_NREC = SYSDATE,
			NM_USUARIO = NM_USUARIO_P,
			NM_USUARIO_NREC = NM_USUARIO_P,
			CD_CEP = CD_CEP_W,
			DS_BAIRRO = DS_BAIRRO_W,
			DS_COMPLEMENTO = DS_COMPLEMENTO_W,
			DS_MUNICIPIO = CD_DOMICILIO_MUNICIPIO_W,
			NR_ENDERECO = NR_ENDERECO_W,
			DS_ENDERECO = DS_ENDERECO_W
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P
				AND IE_TIPO_COMPLEMENTO  = 1;
			COMMIT;
	end if;
  
ELSE

	NM_USUARIO_ATIVO_W     := WHEB_USUARIO_PCK.GET_NM_USUARIO;
	SELECT DOMICILIO_FAMILIA_SEQ.NEXTVAL
		INTO NR_SEQ_DOMICILIO_W 
		FROM DUAL;
		
	SELECT PESSOA_DOMICILIO_MORADIA_SEQ.NEXTVAL
		INTO NR_SEQ_PESSOA_DOMICILIO_W
		FROM DUAL;
    
	IF ACAO_P = 'MUDAR DOMICILIO' OR ACAO_P = 'DELETE MORADIA' THEN
		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  DOMICILIO_FAMILIA
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P AND IE_MUDOU_SE = 'N';
		IF QT_EXISTE >= 1 THEN
			UPDATE DOMICILIO_FAMILIA
				SET IE_MUDOU_SE  =  'S'
				WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
		END IF;
       
		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  PESSOA_DOMICILIO_MORADIA
			WHERE CD_PESSOA_FAMILIA = CD_PESSOA_FISICA_P;
		IF QT_EXISTE >= 1 THEN
			UPDATE PESSOA_DOMICILIO_MORADIA
			SET IE_MUDOU_SE  =  'S'
			WHERE CD_PESSOA_FAMILIA = CD_PESSOA_FISICA_P;
		END IF;
    
    
	ELSIF ACAO_P = 'ATUALIZA CD_DOMICILIO' THEN
		
		UPDATE DOMICILIO_FAMILIA
			SET CD_DOMICILIO = NR_SEQUENCIA
			WHERE CD_DOMICILIO IS NULL;
			COMMIT;
        
	ELSIF ACAO_P = 'CD MORADIA' THEN
    
		UPDATE PESSOA_DOMICILIO_MORADIA
			SET NR_SEQ_DOMICILIO = CD_DOMICILIO_P
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P
			AND NR_SEQ_DOMICILIO IS NULL;
			COMMIT;
                                   
	ELSIF ACAO_P = 'MEMBROS DOMICILIO' OR ACAO_P = 'INSERIR NOVO' THEN -- INSERIR NOVOS MEMBROS AO DOMICILIO.
	
		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  DOMICILIO_FAMILIA
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P AND IE_MUDOU_SE = 'N';
		IF QT_EXISTE >= 1 THEN
			UPDATE DOMICILIO_FAMILIA
			SET IE_MUDOU_SE  =  'S'
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
		END IF;
		  
		IF ACAO_P = 'MEMBROS DOMICILIO' THEN
        
			UPDATE PESSOA_DOMICILIO_MORADIA
				SET NR_SEQ_DOMICILIO = CD_DOMICILIO_P
				WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
				COMMIT;
		END IF;

  
		INSERT
		INTO DOMICILIO_FAMILIA	(NR_SEQUENCIA,
					NM_USUARIO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					DS_BAIRRO,
					DS_COMPLEMENTO,
					DS_ENDERECO,
					NR_ENDERECO,
					CD_DOMICILIO_MUNICIPIO,
					NR_TEL_RESIDENCIAL,
					CD_PESSOA_FISICA,
					CD_CEP,
					NM_PESSOA_RESPONSAVEL,
					IE_MUDOU_SE,
					DT_ATUALIZACAO,
					DS_REFERENCIA,
					IE_TIPO_DOMICILIO,
					IE_TRATAMENTO_AGUA,
					IE_TIPO_AGUA,
					IE_DESTINO_LIXO,
					IE_ESGOTO_SANITARIO,
					IE_TIPO_COMUNICACAO,
					IE_TIPO_DOENCA,
					IE_TIPO_TRANSPORTE,
					IE_ENERGIA_ELETRICA,
					DS_REF_RESIDENCIAL,
					NR_DDI_RESIDENCIAL,
					NR_SEQ_AREA,
					NR_SEQ_EQUIPE,
					NR_SEQ_MICROAREA,
					QT_COMODO,
					NR_SEQ_TIPO_LOGRADOURO,
					CD_DOMICILIO)
				VALUES (NR_SEQ_DOMICILIO_W,
					NM_USUARIO_P,
					SYSDATE,
					NM_USUARIO_P,
					substr(DS_BAIRRO_P,1,30),
					substr(DS_COMPLEMENTO_P,1,15),
					substr(DS_ENDERECO_P,1,50),
					NR_ENDERECO_P,
					DS_MUNICIPIO_P,
					NR_TELEFONE_P,
					CD_PESSOA_FISICA_P,
					CD_CEP_P,
					NM_CONTATO_P,
					'N',
					SYSDATE,
					DS_REFERENCIA_P,
					IE_TIPO_DOMICILIO_P,
					IE_TRATAMENTO_AGUA_P,
					IE_TIPO_AGUA_P,
					IE_DESTINO_LIXO_P,
					IE_ESGOTO_SANITARIO_P,
					IE_TIPO_COMUNICACAO_P,
					IE_TIPO_DOENCA_P,
					IE_TIPO_TRANSPORTE_P,
					IE_ENERGIA_ELETRICA_P,
					DS_REF_RESIDENCIAL_P,
					NR_DDI_RESIDENCIAL_P,
					NR_SEQ_AREA_P,
					NR_SEQ_EQUIPE_P,
					NR_SEQ_MICROAREA_P,
					QT_COMODO_P,
					NR_SEQ_TIPO_LOGRADOURO_P,
					CD_DOMICILIO_P);
					COMMIT;
					
		IF CD_DOMICILIO_P IS NULL THEN
			UPDATE DOMICILIO_FAMILIA
				SET CD_DOMICILIO = (NR_SEQUENCIA)
				WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
				COMMIT;
			END IF;
	     
	ELSIF ACAO_P = 'INSERIR DOMICILIO COMPL' THEN -- INSERIR NOVO DOMICILIO ATRAV�S DE COMPL_PESSOA_FISICA.
	    
		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  DOMICILIO_FAMILIA
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P AND IE_MUDOU_SE = 'N';
		IF QT_EXISTE >= 1 THEN
			UPDATE DOMICILIO_FAMILIA
				SET IE_MUDOU_SE  =  'S'
				WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
		END IF;
		
		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  PESSOA_DOMICILIO_MORADIA
			WHERE CD_PESSOA_FAMILIA = CD_PESSOA_FISICA_P;
			
		IF QT_EXISTE >= 1 THEN
			UPDATE PESSOA_DOMICILIO_MORADIA
				SET IE_MUDOU_SE  =  'S'
				WHERE CD_PESSOA_FAMILIA = CD_PESSOA_FISICA_P;
		END IF;
		
		INSERT
			INTO DOMICILIO_FAMILIA(NR_SEQUENCIA,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			DS_BAIRRO,
			DS_COMPLEMENTO,
			DS_ENDERECO,
			NR_ENDERECO,
			CD_DOMICILIO_MUNICIPIO,
			NR_TEL_RESIDENCIAL,
			CD_PESSOA_FISICA,
			CD_CEP,
			NM_PESSOA_RESPONSAVEL,
			IE_MUDOU_SE,
			DT_ATUALIZACAO)
		VALUES( NR_SEQ_DOMICILIO_W,
			NM_USUARIO_P,
			SYSDATE,
			NM_USUARIO_P,
			substr(DS_BAIRRO_P,1,30),
			substr(DS_COMPLEMENTO_P,1,15),
			substr(DS_ENDERECO_P,1,50),
			NR_ENDERECO_P,
			DS_MUNICIPIO_P,
			NR_TELEFONE_P,
			CD_PESSOA_FISICA_P,
			CD_CEP_P,
			NM_CONTATO_P,
			'N',
			SYSDATE);
		    COMMIT;
		    
		IF CD_DOMICILIO_P IS NULL THEN
			UPDATE DOMICILIO_FAMILIA
				SET CD_DOMICILIO = (NR_SEQUENCIA)
				WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
				COMMIT;
		END IF;
		    
	ELSIF ACAO_P =  'ATUALIZA COMPL' THEN
		UPDATE DOMICILIO_FAMILIA
			SET DS_BAIRRO = substr(DS_BAIRRO_P,1,30),
			DS_COMPLEMENTO = substr(DS_COMPLEMENTO_P,1,15),
			DS_ENDERECO = substr(DS_ENDERECO_P,1,50),
			NR_ENDERECO = NR_ENDERECO_P,
			CD_DOMICILIO_MUNICIPIO = DS_MUNICIPIO_P,
			NR_TEL_RESIDENCIAL = NR_TELEFONE_P,
			CD_CEP = CD_CEP_P,
			NM_PESSOA_RESPONSAVEL = NM_CONTATO_P
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P
			AND IE_MUDOU_SE = 'N';
			COMMIT;

	ELSIF ACAO_P =  'ATUALIZA TODOS COMPL' THEN
		UPDATE DOMICILIO_FAMILIA
			SET DS_BAIRRO = substr(DS_BAIRRO_P,1,30),
			DS_COMPLEMENTO = substr(DS_COMPLEMENTO_P,1,15),
			DS_ENDERECO = substr(DS_ENDERECO_P,1,50),
			NR_ENDERECO = NR_ENDERECO_P,
			CD_DOMICILIO_MUNICIPIO = DS_MUNICIPIO_P,
			NR_TEL_RESIDENCIAL = NR_TELEFONE_P,
			CD_CEP = CD_CEP_P,
			NM_PESSOA_RESPONSAVEL = NM_CONTATO_P
		WHERE CD_DOMICILIO = (  SELECT CD_DOMICILIO
					FROM DOMICILIO_FAMILIA
					WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P
					AND IE_MUDOU_SE = 'N')
					AND IE_MUDOU_SE = 'N';
		COMMIT;

	      
	ELSIF ACAO_P = 'DELETE' THEN
		DELETE FROM PESSOA_DOMICILIO_MORADIA
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P AND NR_SEQ_DOMICILIO = NR_SEQ_DOMICILIO_P;
			COMMIT;
	  
	  ELSIF ACAO_P = 'UPDATE ALL' THEN
		UPDATE DOMICILIO_FAMILIA
			SET  DT_ATUALIZACAO_NREC = SYSDATE,
			NM_USUARIO_NREC = NM_USUARIO_P,
			DS_BAIRRO = substr(DS_BAIRRO_P,1,30),
			DS_COMPLEMENTO = substr(DS_COMPLEMENTO_P,1,15),
			DS_ENDERECO = substr(DS_ENDERECO_P,1,50),
			NR_ENDERECO = NR_ENDERECO_P,
			NR_TEL_RESIDENCIAL = NR_TELEFONE_P,
			CD_CEP = CD_CEP_P,
			NM_PESSOA_RESPONSAVEL = NM_CONTATO_P,
			DT_ATUALIZACAO = SYSDATE,
			CD_DOMICILIO_MUNICIPIO = DS_MUNICIPIO_P,
			DS_REFERENCIA = DS_REFERENCIA_P,
			IE_TIPO_DOMICILIO = IE_TIPO_DOMICILIO_P,
			IE_TRATAMENTO_AGUA = IE_TRATAMENTO_AGUA_P,
			IE_TIPO_AGUA = IE_TIPO_AGUA_P,
			IE_DESTINO_LIXO = IE_DESTINO_LIXO_P,
			IE_ESGOTO_SANITARIO = IE_ESGOTO_SANITARIO_P,
			IE_TIPO_COMUNICACAO = IE_TIPO_COMUNICACAO_P,
			IE_TIPO_DOENCA = IE_TIPO_DOENCA_P,
			IE_TIPO_TRANSPORTE = IE_TIPO_TRANSPORTE_P,
			IE_ENERGIA_ELETRICA = IE_ENERGIA_ELETRICA_P,
			DS_REF_RESIDENCIAL = DS_REF_RESIDENCIAL_P,
			NR_SEQ_AREA = NR_SEQ_AREA_P,
			NR_SEQ_EQUIPE = NR_SEQ_EQUIPE_P,
			NR_SEQ_MICROAREA = NR_SEQ_MICROAREA_P,
			QT_COMODO = QT_COMODO_P,
			NR_SEQ_TIPO_LOGRADOURO = NR_SEQ_TIPO_LOGRADOURO_P
			WHERE CD_DOMICILIO = CD_DOMICILIO_P;
			COMMIT;  
	  
	ELSIF ACAO_P = 'VINCULAR' THEN
		  
		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  PESSOA_DOMICILIO_MORADIA
			WHERE CD_PESSOA_FAMILIA = CD_PESSOA_FISICA_P;
		IF QT_EXISTE >= 1 THEN
			UPDATE PESSOA_DOMICILIO_MORADIA
			SET IE_MUDOU_SE  =  'S'
			WHERE CD_PESSOA_FAMILIA = CD_PESSOA_FISICA_P;
		END IF;

		SELECT COUNT(1)
			INTO QT_EXISTE
			FROM  DOMICILIO_FAMILIA
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P AND IE_MUDOU_SE = 'N';
		IF QT_EXISTE >= 1 THEN
			UPDATE DOMICILIO_FAMILIA
			SET IE_MUDOU_SE  =  'S'
			WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
		END IF;
		    
		    
		FOR REGISTRO IN C02
		LOOP
    IF (CD_PESSOA_FISICA_P <> REGISTRO.CD_PESSOA_FISICA)THEN
		INSERT INTO PESSOA_DOMICILIO_MORADIA (  NR_SEQUENCIA,
							NR_SEQ_DOMICILIO,
							DT_ATUALIZACAO,
							NM_USUARIO,
							DT_ATUALIZACAO_NREC,
							NM_USUARIO_NREC,
							CD_PESSOA_FISICA,
							CD_PESSOA_FAMILIA,
							NR_SEQ_GRAU_PARENT,
							IE_MUDOU_SE)
						VALUES (PESSOA_DOMICILIO_MORADIA_SEQ.NEXTVAL,
							NR_SEQ_DOMICILIO_P,
							SYSDATE,
							NM_USUARIO_P,
							SYSDATE,
							NM_USUARIO_P,
							CD_PESSOA_FISICA_P,
							REGISTRO.CD_PESSOA_FISICA,
							NR_SEQ_GRAU_PARENTESCO_P, 'N');
              END IF;
		END LOOP;

		INSERT
		INTO DOMICILIO_FAMILIA( NR_SEQUENCIA,
					DT_ATUALIZACAO,              
					NM_USUARIO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					DS_BAIRRO,
					DS_COMPLEMENTO,
					DS_ENDERECO,
					NR_ENDERECO,
					CD_DOMICILIO_MUNICIPIO,
					NR_TEL_RESIDENCIAL,
					CD_PESSOA_FISICA,
					CD_CEP,
					NM_PESSOA_RESPONSAVEL,
					IE_MUDOU_SE,
					DS_REFERENCIA,
					IE_TIPO_DOMICILIO,
					IE_TRATAMENTO_AGUA,
					IE_TIPO_AGUA,
					IE_DESTINO_LIXO,
					IE_ESGOTO_SANITARIO,
					IE_TIPO_COMUNICACAO,
					IE_TIPO_DOENCA,
					IE_TIPO_TRANSPORTE,
					IE_ENERGIA_ELETRICA,
					DS_REF_RESIDENCIAL,
					NR_DDI_RESIDENCIAL,
					NR_SEQ_AREA,
					NR_SEQ_EQUIPE,
					NR_SEQ_MICROAREA,
					QT_COMODO,
					NR_SEQ_TIPO_LOGRADOURO,
					CD_DOMICILIO)
				VALUES (NR_SEQ_DOMICILIO_W,
					SYSDATE,
					NM_USUARIO_P,
					SYSDATE,
					NM_USUARIO_P,
					substr(DS_BAIRRO_W,1,30),
					substr(DS_COMPLEMENTO_W,1,15),
					substr(DS_ENDERECO_W,1,50),
					NR_ENDERECO_W,
					CD_DOMICILIO_MUNICIPIO_W,
					NR_TEL_RESIDENCIAL_W,
					CD_PESSOA_FISICA_P,
					CD_CEP_W,
					NM_PESSOA_RESPONSAVEL_W,
					'N',
					DS_REFERENCIA_W,
					IE_TIPO_DOMICILIO_W,
					IE_TRATAMENTO_AGUA_W,
					IE_TIPO_AGUA_W,
					IE_DESTINO_LIXO_W,
					IE_ESGOTO_SANITARIO_W,
					IE_TIPO_COMUNICACAO_W,
					IE_TIPO_DOENCA_W,
					IE_TIPO_TRANSPORTE_W,
					IE_ENERGIA_ELETRICA_W,
					DS_REF_RESIDENCIAL_W,
					NR_DDI_RESIDENCIAL_W,
					NR_SEQ_AREA_W,
					NR_SEQ_EQUIPE_W,
					NR_SEQ_MICROAREA_W,
					QT_COMODO_W,
					NR_SEQ_TIPO_LOGRADOURO_W,
					CD_DOMICILIO_W);
					COMMIT; 
		
    IF CD_DOMICILIO_P IS NULL THEN
			UPDATE DOMICILIO_FAMILIA
				SET CD_DOMICILIO = (NR_SEQUENCIA)
				WHERE CD_PESSOA_FISICA = CD_PESSOA_FISICA_P;
				COMMIT;
		END IF;
	END IF;
		    

	IF INSERIR_SI_PROPRIO_P = 'S' THEN
		INSERT INTO PESSOA_DOMICILIO_MORADIA (  NR_SEQUENCIA,
							NR_SEQ_DOMICILIO,
							DT_ATUALIZACAO,
							NM_USUARIO,
							DT_ATUALIZACAO_NREC,
							NM_USUARIO_NREC,
							CD_PESSOA_FISICA,
							CD_PESSOA_FAMILIA,
							NR_SEQ_GRAU_PARENT,
							IE_MUDOU_SE)
						VALUES (NR_SEQ_PESSOA_DOMICILIO_W,
							NR_SEQ_DOMICILIO_P,
							SYSDATE,
							NM_USUARIO_ATIVO_W,
							SYSDATE,
							NM_USUARIO_ATIVO_W,
							CD_PESSOA_FISICA_P,
							CD_PESSOA_FISICA_P,
							NR_SEQ_GRAU_PARENTESCO_P,
							'N');
							COMMIT;

		  
	IF NR_SEQ_DOMICILIO_P IS NULL THEN
		IF ACAO_P = 'MUDAR DOMICILIO' OR ACAO_P = 'ATUALIZA CD_DOMICILIO' OR ACAO_P = 'INSERIR DOMICILIO COMPL' OR ACAO_P = 'INSERIR NOVO' OR ACAO_P = 'ATUALIZA CD_DOMICILIO' THEN
			UPDATE PESSOA_DOMICILIO_MORADIA
				SET NR_SEQ_DOMICILIO = (SELECT CD_DOMICILIO
							FROM DOMICILIO_FAMILIA
							WHERE NR_SEQUENCIA = (  SELECT MAX(NR_SEQUENCIA)
							FROM DOMICILIO_FAMILIA))
							WHERE NR_SEQUENCIA = (SELECT MAX(NR_SEQUENCIA)
							FROM PESSOA_DOMICILIO_MORADIA);
			COMMIT;
		END IF;
	END IF;
	END IF;
END IF;
END;
/
