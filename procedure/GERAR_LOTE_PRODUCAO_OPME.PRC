create or replace
procedure gerar_lote_producao_opme(	nr_ficha_tecnica_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				cd_local_estoque_p	number,
				nr_lote_producao_p	out number,
				nr_prescricao_p		number) is

cd_material_w			number(6,0);
cd_unidade_medida_consumo_w	varchar2(30);
cd_setor_atendimento_w		number(5,0);
cd_local_estoque_w		number(4,0);
nr_lote_producao_w		number(10,0);

cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_atendimento_w		number(10);
dt_inicio_prescr_w		date;
dt_validade_prescr_w		date;
cd_setor_atend_prescr_w		number(15,0);
dt_prescricao_w			date;
nr_interno_conta_w		number(10);
ie_lancar_conta_w		varchar2(1);


Cursor C01 is
	select a.cd_procedimento,
	       a.ie_origem_proced,
	       b.nr_atendimento,
	       b.dt_inicio_prescr,
	       b.dt_validade_prescr,
	       b.cd_setor_atendimento,
	       dt_prescricao
	from   prescr_opm a,
	       prescr_medica b
	where  a.nr_prescricao		= b.nr_prescricao
	and    b.nr_prescricao 		= nr_prescricao_p	
	order by 1;

				
begin

obter_param_usuario(10025, 15, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, cd_estabelecimento_p, ie_lancar_conta_w);

select	max(cd_material)
into	cd_material_w
from	ficha_tecnica
where	nr_ficha_tecnica = nr_ficha_tecnica_p;

if	(cd_material_w is not null) then
	begin
	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo
	into	cd_unidade_medida_consumo_w
	from	material
	where	cd_material = cd_material_w;

	select	lote_producao_seq.nextval
	into	nr_lote_producao_w
	from	dual;
	
	insert into lote_producao(
		nr_lote_producao,
		cd_material,
		ie_status_lote,
		cd_unidade_medida,
		qt_prevista,
		cd_estabelecimento,
		cd_local_estoque,
		dt_atualizacao,
		nm_usuario,
		ie_etapa,
		cd_farmaceutico,
		ie_tipo_lote,
		dt_inicio,
		dt_geracao,
		nm_usuario_geracao,
		cd_responsavel,
		nr_prescricao)
	values(
		nr_lote_producao_w,
		cd_material_w,
		'P',
		cd_unidade_medida_consumo_w,
		1,
		cd_estabelecimento_p,
		cd_local_estoque_p,
		sysdate,
		nm_usuario_p,
		'G',
		null,
		'A',
		sysdate,
		sysdate,
		nm_usuario_p,
		obter_dados_usuario_opcao(nm_usuario_p,'C'),
		nr_prescricao_p);
	end;
	
	gerar_lote_producao_comp_opme(nr_lote_producao_w, nm_usuario_p);
	
	begin
	gerar_status_prescr_opm('G',nr_prescricao_p,'',cd_estabelecimento_p,nm_usuario_p);
	exception
	when others then
		null;
	end;
	
	if (ie_lancar_conta_w = 'S') then
		open C01;
		loop
		fetch C01 into	
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_atendimento_w,
			dt_inicio_prescr_w,
			dt_validade_prescr_w,
			cd_setor_atend_prescr_w,
			dt_prescricao_w;
		exit when C01%notfound;
			begin
			
			/*select	max(nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente
			where	nr_atendimento		= nr_atendimento_w
			and	ie_status_acerto 	= 1
			and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
			and	dt_inicio_prescr_w	>= dt_periodo_inicial
			and	dt_validade_prescr_w	<= dt_periodo_final;*/
			
			
			Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, nr_atendimento_w,null, null, cd_procedimento_w, ie_origem_proced_w, 1, cd_setor_atend_prescr_w, 1, 
						   dt_prescricao_w, nm_usuario_p, null, null, null, null,null);
			end;
		end loop;
		close C01;
	end if;
	
	nr_lote_producao_p	:= nr_lote_producao_w; 
	
	commit;
end if;

end gerar_lote_producao_opme;
/