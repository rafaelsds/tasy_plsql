create or replace
procedure tws_solic_justificativa_prest(nr_seq_auditoria_p		pls_auditoria.nr_sequencia%type,
					ds_lista_sequencias_itens_p	Varchar2,
					ds_historico_justific_p		pls_guia_plano_historico.ds_observacao%type,
					nm_usuario_p			usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Solicitar justificativa do prestador.
Esta rotina foi criada para unificar o processo de justificativa, conforme relaizado no Tasy.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros: TWS
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ie_gerar_grupo_regra_w		Varchar2(2)	:= 'N';
ds_lista_sequencias_w		varchar2(2000);
nr_pos_virgula_w		number(10);
nr_seq_aud_item_w		pls_auditoria_item.nr_sequencia%type;
ie_solicita_just_prest_w	pls_param_analise_aut.ie_solicita_just_prest%type := 'N';
nr_seq_grupo_auditor_w		pls_auditoria_grupo.nr_sequencia%type;

begin

if	(ds_lista_sequencias_itens_p is not null) and
	(nr_seq_auditoria_p is not null) then	
	pls_encaminha_grupo_acao_regra(	nr_seq_auditoria_p, 1, nm_usuario_p, ie_gerar_grupo_regra_w);

	if 	(ie_gerar_grupo_regra_w = 'N') then	
		ds_lista_sequencias_w	:= ds_lista_sequencias_itens_p;
		
		while (ds_lista_sequencias_w is not null) loop
		begin
			nr_pos_virgula_w	:= instr(ds_lista_sequencias_w,',');
			
			if	(nr_pos_virgula_w > 0) then
				nr_seq_aud_item_w	:= to_number(substr(ds_lista_sequencias_w,0,nr_pos_virgula_w-1));
				ds_lista_sequencias_w	:= substr(ds_lista_sequencias_w,nr_pos_virgula_w+1,length(ds_lista_sequencias_w));
			else
				nr_seq_aud_item_w	:= to_number(ds_lista_sequencias_w);
				ds_lista_sequencias_w	:= null;
			end if;
			
			if	(nr_seq_aud_item_w > 0) then			
				pls_atualizar_status_justifica(	nr_seq_aud_item_w, 'I', null, nm_usuario_p);				
				
			end if;
		end;
		end loop;
		
		pls_atualizar_status_justifica(	nr_seq_auditoria_p, 'A', substr(ds_historico_justific_p,1,4000), nm_usuario_p);
		
	else	
		pls_gravar_justific_auditor(	nr_seq_auditoria_p, substr(ds_historico_justific_p,1,4000), nm_usuario_p);
	end if;
	
	begin
		select	ie_solicita_just_prest
		into	ie_solicita_just_prest_w
		from	pls_param_analise_aut
		where	cd_estabelecimento = (	select	cd_estabelecimento
						from	pls_auditoria
						where	nr_sequencia	= nr_seq_auditoria_p);
	exception
	when others then
		ie_solicita_just_prest_w := 'N';
	end;
	
	if	(ie_solicita_just_prest_w = 'S') then
		nr_seq_grupo_auditor_w	:= pls_obter_grupo_analise_atual(nr_seq_auditoria_p);
		pls_desfazer_assumir_grupo_aud(	nr_seq_grupo_auditor_w, nr_seq_auditoria_p, 'J', nm_usuario_p);
	end if;	
end if;	
	
end tws_solic_justificativa_prest;
/