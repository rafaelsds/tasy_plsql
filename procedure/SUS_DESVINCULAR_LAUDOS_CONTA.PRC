create or replace
procedure sus_desvincular_laudos_conta(	nr_interno_conta_p	number,
					nm_usuario_p		varchar2) is

begin
update	sus_laudo_paciente
set	nr_interno_conta = null,
	nm_usuario	 = nm_usuario_p,
	dt_atualizacao	 = sysdate
where	nr_interno_conta = nr_interno_conta_p;

commit;

end sus_desvincular_laudos_conta;
/
