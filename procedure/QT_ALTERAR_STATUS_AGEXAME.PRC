CREATE OR REPLACE
PROCEDURE Qt_Alterar_Status_Agexame(	cd_agenda_p		varchar2,
					ie_status_agenda_p	varchar2,
					nr_seq_paciente_p	varchar2,
					dt_agenda_p		date,
					nm_usuario_p		varchar2) IS
					
qt_agendamentos_pac_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(10,0);
ds_lista_w		varchar2(1000);
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
cd_agenda_w		number(10);




Cursor C01 is
	select	a.nr_sequencia
	from	agenda_paciente a,
		agenda b
	where	a.cd_agenda					= b.cd_agenda
	and	a.cd_pessoa_fisica				= (	select	max(a.cd_pessoa_fisica)		
									from	paciente_setor a
									where	a.nr_seq_paciente	= nr_seq_paciente_p)
	and	a.ie_status_agenda				not in ('L','C','B','F','I','II')
	and	a.dt_agenda					between	trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
	and	obter_se_contido(a.cd_agenda,cd_agenda_p)	= 'S'
	and	b.cd_tipo_agenda				= 2
	and	b.ie_situacao					= 'A';


begin	
open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin		
	
	if	(nr_sequencia_w 	> 0) and
		(ie_status_agenda_p 	= 'O') then
		
		update 	agenda_paciente a
		set	a.ie_status_agenda	= ie_status_agenda_p,			
			a.dt_atendimento	= sysdate,
			a.nm_usuario		= nm_usuario_p
		where	a.nr_sequencia		= nr_sequencia_w;				
		
	end if;
	
	if	(nr_sequencia_w 	> 0) and
		(ie_status_agenda_p 	= 'E') then
		update 	agenda_paciente a
		set	a.ie_status_agenda	= ie_status_agenda_p,			
			dt_executada		= sysdate,
			a.nm_usuario		= nm_usuario_p
		where	a.nr_sequencia		= nr_sequencia_w;					
	end if;	
	
	
	end;
end loop;
close C01;		

commit;

end Qt_Alterar_Status_Agexame;
/