create or replace
procedure pls_gerar_lote_emissao
			(	nr_seq_carterinha_p	number,
				cd_estabelecimento_p	number,
				ie_commit_p		varchar2,
				nm_usuario_p		Varchar2) is

ie_situacao_w			varchar2(1);
nr_seq_lote_w			number(10);
ie_controle_carteira_w		varchar2(1);
qt_cart_provisorias_w		number(10);
ie_tipo_segurado_w		varchar2(10);
nr_seq_tipo_lote_w		number(10);
ie_agrupar_lote_w		varchar2(10);
nr_seq_segurado_w		number(10);
nr_seq_contrato_w		number(10);
nr_contrato_w			number(10);
ie_usuario_solic_w		varchar2(10);
ie_restringir_tipo_benef_w	varchar2(10);
ie_tipo_contrato_w		varchar2(10);
ie_emissao_cart_repasse_pre_w	varchar2(1);
ie_gerar_emissao_w		varchar2(1);
ie_tipo_repasse_w		varchar2(1);
nr_seq_cart_emis_w		number(10);
nm_usuario_solic_w 		pls_segurado_carteira.nm_usuario_solicitante%type;
dt_solicitacao_w			pls_segurado_carteira.dt_solicitacao%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_lote_carteira
	where	ie_situacao	= 'G'
	and	ie_tipo_lote	= 'E'
	and	cd_estabelecimento = cd_estabelecimento_p
	and	((ie_restringir_tipo_benef_w	= 'S')
		and	((ie_tipo_beneficiario is not null and ie_tipo_beneficiario = ie_tipo_segurado_w) or
			((ie_tipo_beneficiario is not null and ie_tipo_beneficiario = 'R' and ie_tipo_segurado_w = 'R') or
			((ie_tipo_beneficiario is null))))
			and	((ie_tipo_contrato <> 'I')
			or	(ie_tipo_contrato is null)) or
		(ie_restringir_tipo_benef_w	= 'N'))
	and	((ie_agrupar_lote_w = 'N' and nr_seq_tipo_lote = nr_seq_tipo_lote_w and nr_seq_tipo_lote is not null
		and 	(nr_contrato = nr_contrato_w and nr_contrato_w is not null))
		or (ie_agrupar_lote_w = 'S' and nr_seq_tipo_lote is null))
	and	nr_seq_lote_vencimento is null
	and	((nm_usuario_solicitante = nm_usuario_p and nm_usuario_solicitante is not null) or (nm_usuario_solicitante is null))
	and	((nvl(ie_tipo_pessoa,'A') = ie_tipo_contrato_w) or (nvl(ie_tipo_pessoa,'A') = 'A'))
	order by
		nvl(ie_tipo_pessoa, 'A'),
		decode(ie_tipo_beneficiario,null,-1,1);

begin

ie_gerar_emissao_w	:= 'S';
begin
nr_seq_tipo_lote_w	:= nvl(obter_valor_param_usuario(1226, 4, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), null);
exception
when others then
	nr_seq_tipo_lote_w	:= null;
end;

ie_usuario_solic_w		:= nvl(obter_valor_param_usuario(1226, 6, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');
ie_restringir_tipo_benef_w	:= nvl(obter_valor_param_usuario(1202, 106, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'S');

select	max(ie_emissao_cart_repasse_pre)
into	ie_emissao_cart_repasse_pre_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;
ie_emissao_cart_repasse_pre_w	:= nvl(ie_emissao_cart_repasse_pre_w,'N');

select	b.ie_tipo_segurado,
	b.nr_sequencia,
	b.nr_seq_contrato
into	ie_tipo_segurado_w,
	nr_seq_segurado_w,
	nr_seq_contrato_w
from	pls_segurado		b,
	pls_segurado_carteira	a
where	a.nr_seq_segurado	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_carterinha_p;

if	((ie_emissao_cart_repasse_pre_w = 'S') and (ie_tipo_segurado_w = 'R')) then
	select	max(ie_tipo_repasse)
	into	ie_tipo_repasse_w
	from	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_w
	and	dt_liberacao is not null
	and	dt_fim_repasse is null;
	
	if	(ie_tipo_repasse_w = 'P') then
		ie_gerar_emissao_w	:= 'N';
	end if;
end if;

if	(ie_gerar_emissao_w = 'S') then
	select	max(b.nr_sequencia)
	into	nr_seq_lote_w
	from	pls_lote_carteira	b,
		pls_carteira_emissao	a
	where	b.ie_situacao		in ('P','G')
	and	b.ie_tipo_lote		= 'E'
	and	a.nr_seq_lote		= b.nr_sequencia
	and	a.nr_seq_seg_carteira	= nr_seq_carterinha_p
	and	cd_estabelecimento 	= cd_estabelecimento_p;
	
	if	(nr_seq_lote_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(183144,'NR_SEQ_CARTEIRA='||nr_seq_carterinha_p||';'||'NR_SEQ_LOTE='||nr_seq_lote_w);
	end if;
	
	select	count(*)
	into	qt_cart_provisorias_w
	from	pls_segurado_carteira
	where	ie_situacao		= 'D'
	and	nr_sequencia		= nr_seq_carterinha_p;
	
	if	(qt_cart_provisorias_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(183143,'');
	end if;
	
	if	(nr_seq_tipo_lote_w	is not null) then
		select	ie_agrupar_lote
		into	ie_agrupar_lote_w
		from	pls_tipo_lote_carteira
		where	nr_sequencia	= nr_seq_tipo_lote_w
		and	ie_situacao	= 'A';
	else
		ie_agrupar_lote_w	:= 'S';
	end if;
	
	if	(nr_seq_contrato_w is not null) then
		select	nr_contrato,
			decode(cd_cgc_estipulante,null,'PF','PJ')
		into	nr_contrato_w,
			ie_tipo_contrato_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
	end if;

	open C01;
	loop
	fetch C01 into	
		nr_seq_lote_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
	if	(nr_seq_lote_w is null) then
		select	pls_lote_carteira_seq.NextVal
		into	nr_seq_lote_w
		from	dual;
		
		if	(nr_seq_tipo_lote_w is null) then
			nr_contrato_w		:= null;
		end if;
		
		insert	into	pls_lote_carteira 
			(nr_sequencia, cd_estabelecimento, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			dt_referencia_venc, ie_situacao, ie_tipo_lote,
			nr_seq_lote_vencimento,ie_tipo_contrato,ie_tipo_beneficiario,
			nr_seq_tipo_lote,nr_contrato,
			ie_tipo_data,ie_situacao_atend,ie_tipo_pessoa,nm_usuario_solicitante)
		values(	nr_seq_lote_w, cd_estabelecimento_p, sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			null, 'G', 'E',
			null,'O',decode(ie_restringir_tipo_benef_w,'S',ie_tipo_segurado_w,''),
			nr_seq_tipo_lote_w,nr_contrato_w,
			'N','T','A',decode(ie_usuario_solic_w,'N','',nm_usuario_p));
	end if;
	
	select	nvl(max(a.ie_controle_carteira), 'A')
	into	ie_controle_carteira_w
	from	pls_contrato 		a,
		pls_segurado 		b,
		pls_segurado_carteira	c
	where	a.nr_sequencia	= b.nr_seq_contrato
	and	b.nr_sequencia	= c.nr_seq_segurado
	and	c.nr_sequencia	= nr_seq_carterinha_p;
	
	if	(ie_controle_carteira_w in ('A','E')) then
		select 	dt_solicitacao,
				 		nm_usuario_solicitante
		into   	dt_solicitacao_w,
						nm_usuario_solic_w
		from   	pls_segurado_carteira
		where  	nr_sequencia = nr_seq_carterinha_p;

		select	pls_carteira_emissao_seq.nextval
		into	nr_seq_cart_emis_w
		from	dual;
		
		insert	into	pls_carteira_emissao
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_lote,
			nr_seq_seg_carteira, dt_recebimento, ie_situacao, dt_solicitacao, nm_usuario_solic)
		values(	nr_seq_cart_emis_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, nr_seq_lote_w,
			nr_seq_carterinha_p, null, 'P', dt_solicitacao_w, nm_usuario_solic_w);
		
		update 	pls_segurado_carteira
		set	nr_seq_lote_emissao	= nr_seq_lote_w
		where	nr_sequencia		= nr_seq_carterinha_p;
		
		pls_atualizar_campos_cart_emis(nr_seq_cart_emis_w, nr_seq_carterinha_p, nm_usuario_p);
		pls_alterar_estagios_cartao(nr_seq_carterinha_p, sysdate, '2', cd_estabelecimento_p, nm_usuario_p);
	end if;
end if;

if	(ie_commit_p	= 'S') then
	commit;
end if;

end pls_gerar_lote_emissao;
/
