create or replace
procedure pls_gerar_hist_faq_atend
			(	nr_seq_atendimento_p	number,
				nr_seq_pergunta_p	number,
				nr_seq_resposta_p	number,
				nm_usuario_p		varchar2) is

ds_pergunta_w			varchar2(255);
ds_resposta_w			varchar2(3745);
nr_tipo_historico_w		number(10);
qtd_resposta_long_w             number(10);
nr_atendimento_historico_w      number(10);

begin
if	(pls_obter_se_controle_estab('GA') = 'S') then
	select	substr(ds_pergunta,1,255)
	into	ds_pergunta_w
	from	pls_pergunta_atendimento
	where	nr_sequencia = nr_seq_pergunta_p
	and 	(cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento );
else
	select	substr(ds_pergunta,1,255)
	into	ds_pergunta_w
	from	pls_pergunta_atendimento
	where	nr_sequencia = nr_seq_pergunta_p;
end if;

select	substr(ds_resposta,1,3745)
into	ds_resposta_w
from	pls_resp_pergunta_atend
where	nr_sequencia = nr_seq_resposta_p;

select 	count(*) 
into 	qtd_resposta_long_w
from   	pls_resp_pergunta_atend
where 	nr_sequencia = nr_seq_resposta_p
and 	ds_resposta_long is not null;

if	(pls_obter_se_controle_estab('GA') = 'S') then
	select	max(nr_sequencia)
	into	nr_tipo_historico_w
	from	pls_tipo_historico_atend
	where	ie_historico_faq = 'S'
	and 	(cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento );

	if	(nr_tipo_historico_w is null) then
		select	min(nr_sequencia)
		into	nr_tipo_historico_w
		from	pls_tipo_historico_atend
		where 	(cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento );
	end if;
else
	select	max(nr_sequencia)
	into	nr_tipo_historico_w
	from	pls_tipo_historico_atend
	where	ie_historico_faq = 'S';

	if	(nr_tipo_historico_w is null) then
		select	min(nr_sequencia)
		into	nr_tipo_historico_w
		from	pls_tipo_historico_atend;
	end if;
end if;

insert	into pls_atendimento_historico
	(nr_sequencia, nr_seq_atendimento, ds_historico_long,
	dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
	nm_usuario_nrec, nr_seq_tipo_historico, dt_historico,
	ie_origem_historico, ie_gerado_sistema)
values	(pls_atendimento_historico_seq.nextval, nr_seq_atendimento_p, ds_pergunta_w || chr(10) || ds_resposta_w,
	sysdate, nm_usuario_p, sysdate,
	nm_usuario_p, nr_tipo_historico_w, sysdate,
	'', 'N');

if 	(qtd_resposta_long_w > 0) then
	select	pls_atendimento_historico_seq.nextval
	into	nr_atendimento_historico_w
	from	dual;
	
	insert	into pls_atendimento_historico
		(nr_sequencia, nr_seq_atendimento, ds_historico_long,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
		nm_usuario_nrec, nr_seq_tipo_historico, dt_historico,
		ie_origem_historico, ie_gerado_sistema)
	values	(nr_atendimento_historico_w, nr_seq_atendimento_p, '',
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, nr_tipo_historico_w, sysdate,
		'', 'N');
	
	copia_campo_long_de_para('pls_resp_pergunta_atend',
				'ds_resposta_long',
				' where nr_sequencia = :nr_sequencia',
				'nr_sequencia='||nr_seq_resposta_p,
				'pls_atendimento_historico',
				'ds_historico_long',
				' where nr_sequencia = :nr_sequencia',
				'nr_sequencia='||nr_atendimento_historico_w);
end if;
		
commit;

end pls_gerar_hist_faq_atend;
/
