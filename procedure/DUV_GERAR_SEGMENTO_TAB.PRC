create or replace procedure duv_gerar_segmento_tab(nr_seq_mensagem_p  in duv_mensagem.nr_sequencia%type,
                                                   nm_usuario_p       in usuario.nm_usuario%type,
                                                   nr_seq_episodio_p  in episodio_paciente.nr_sequencia%type)is

  cursor c01 is
     select
       substr(dd.ds_diagnostico,1,3000) as ds_diagnostico,
       ddia.ds_diagnostico as ds_diag_outro 
     from diag_doenca_inf_adic ddia,
          diagnostico_doenca   dd,
          atendimento_paciente ap
     where ap.nr_seq_episodio = nr_seq_episodio_p
       and ap.nr_atendimento  = dd.nr_atendimento
       and dd.nr_seq_interno  = ddia.nr_seq_diag_doenca(+);     

  c01_w c01%rowtype;

begin
  c01_w := null;
  open c01;
  fetch c01 into c01_w;
  close c01;
 
  if c01_w.ds_diag_outro is null then
    c01_w.ds_diag_outro := c01_w.ds_diagnostico;
  end if;
  
  insert into duv_tab(nr_sequencia,
                      dt_atualizacao,
                      nm_usuario,
                      dt_atualizacao_nrec,
                      nm_usuario_nrec,
                      nr_seq_mensagem,
                      ds_diag_outro) values (duv_tab_seq.nextval,
                                             sysdate,
                                             nm_usuario_p,
                                             sysdate,
                                             nm_usuario_p,
                                             nr_seq_mensagem_p,
                                             c01_w.ds_diag_outro);  

end duv_gerar_segmento_tab;
/
