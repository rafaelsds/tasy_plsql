create or replace
procedure siscolo_limpar_cito_atipias(	nr_seq_siscolo_p	number,
				nm_usuario_p	varchar2) is 

begin

if	(nr_seq_siscolo_p is not null) then
	begin
	update	siscolo_cito_atipias
	set	ie_atipicas_escamosas = '',
		ie_atipicas_glandulares = '',
		ie_atipicas_indefinidas = '',
		ie_atipias_escamosas = '',
		ie_atipias_gla_adeno = '',
		ie_neo_malignas = 'N',
		ds_neo_malignas = '',
		ie_celula_endometrial = 'N'
	where	nr_seq_siscolo = nr_seq_siscolo_p;
	commit;
	end;
end if;

end siscolo_limpar_cito_atipias;
/