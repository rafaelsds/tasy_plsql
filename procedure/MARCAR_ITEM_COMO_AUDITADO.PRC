create or replace
procedure marcar_item_como_auditado(	ie_tipo_item_p		varchar,
				nr_seq_item_p		number,
				nr_seq_motivo_p		number) is 

begin

if	(ie_tipo_item_p	is not null) and
	(nr_seq_item_p	is not null) then
	begin
	if	(ie_tipo_item_p = 2) then
		begin 
		update	auditoria_matpaci
		set	ie_tipo_auditoria	= 'D',
			nr_seq_motivo	= decode(nvl(nr_seq_motivo_p, 0) , 0, nr_seq_motivo, nr_seq_motivo_p),
			dt_atualizacao	= sysdate
		where 	nr_sequencia 	= nr_seq_item_p;
		end;
	elsif	(ie_tipo_item_p = 1) then
		begin 
		update 	auditoria_propaci
		set	ie_tipo_auditoria	= 'D',
			nr_seq_motivo	= decode(nvl(nr_seq_motivo_p, 0) , 0, nr_seq_motivo, nr_seq_motivo_p),
			dt_atualizacao	= sysdate
		where 	nr_sequencia 	= nr_seq_item_p;
		end;
	end if;
	end;
end if;

commit;

end marcar_item_como_auditado;
/