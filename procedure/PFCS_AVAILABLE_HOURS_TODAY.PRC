create or replace
procedure pfcs_available_hours_today(	nr_seq_indicator_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p		varchar2) is

pfcs_panel_detail_seq_w		pfcs_panel_detail.nr_sequencia%type;
nr_seq_operational_level_w	pfcs_operational_level.nr_sequencia%type;
nr_seq_panel_w			pfcs_panel.nr_sequencia%type; 
qt_total_w			number(10) := 0;

cursor c01 is
select	(select	x.ds_sala_painel
	from	sala_cirurgia x
	where	x.nr_sequencia	= b.nr_seq_sala_cir) ds_room,
	sum(a.nr_minuto_duracao) qt_duration_minutes
from	agenda_paciente	a,
	agenda		b
where	a.cd_agenda		= b.cd_agenda
and	b.cd_tipo_agenda	= 1
and	a.ie_status_agenda	= 'L'
and	trunc(a.dt_agenda)	= trunc(sysdate)
and	b.cd_estabelecimento	= cd_estabelecimento_p
and	b.nr_seq_sala_cir is not null
group by b.nr_seq_sala_cir;

begin

nr_seq_operational_level_w := pfcs_get_structure_level(
		cd_establishment_p => cd_estabelecimento_p,
		ie_level_p => 'O',
		ie_info_p => 'C');

for c01_w in c01 loop
	begin

	qt_total_w	:= qt_total_w + c01_w.qt_duration_minutes;
	
	select	pfcs_panel_detail_seq.nextval
	into	pfcs_panel_detail_seq_w
	from	dual;

	insert into pfcs_panel_detail(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_situation,
		nr_seq_indicator,
		nr_seq_operational_level)
	values (
		pfcs_panel_detail_seq_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'T',
		nr_seq_indicator_p,
		nr_seq_operational_level_w);
	
	insert into pfcs_detail_schedule(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_detail,
		ds_room,
		qt_duration_minutes)
	values (
		pfcs_detail_schedule_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		pfcs_panel_detail_seq_w,
		c01_w.ds_room,
		c01_w.qt_duration_minutes);
	end;
end loop;

commit;

pfcs_pck.pfcs_generate_results(
		vl_indicator_p => round((qt_total_w / 60),0),
		vl_indicator_aux_p => qt_total_w,
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

pfcs_pck.pfcs_update_detail(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

pfcs_pck.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

end pfcs_available_hours_today;
/
