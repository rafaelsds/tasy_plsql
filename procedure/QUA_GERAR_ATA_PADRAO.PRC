create or replace
procedure qua_gerar_ata_padrao(	nr_seq_ata_padrao_p	number,
				nr_seq_ata_p	out	number,
				nm_usuario_p		varchar2) is

ds_pauta_w		varchar2(255);
ds_titulo_w		varchar2(80);
ds_conteudo_w		varchar2(4000);
nr_seq_apresentacao_w	number(10,0);
ds_ata_w			varchar2(255);
nr_seq_classif_w		number(10,0);
cd_pessoa_w		varchar2(10);
cd_estabelecimento_w	number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;


Cursor c01 is
	select	ds_pauta
	from	proj_ata_padrao_pauta
	where	nr_seq_ata = nr_seq_ata_padrao_p;
	
Cursor c02 is
	select	ds_titulo,
		ds_conteudo,
		nr_seq_apresentacao
	from	proj_ata_padrao_conteudo
	where	nr_seq_ata = nr_seq_ata_padrao_p;

begin

if	(nvl(nr_seq_ata_padrao_p,0) > 0) then
	begin
	select	ds_ata,
		nr_seq_classif
	into	ds_ata_w,
		nr_seq_classif_w
	from	proj_ata_padrao
	where	nr_sequencia = nr_seq_ata_padrao_p;

	select	proj_ata_seq.nextval
	into	nr_seq_ata_p
	from	dual;
	
	select	cd_pessoa_fisica
	into	cd_pessoa_w
	from	usuario
	where	nm_usuario = nm_usuario_p;
	
	if	(nvl(cd_pessoa_w,'X') = 'X') then
		/*(-20011,'O usu�rio logado n�o possui pessoa f�sica vinculada');*/
		wheb_mensagem_pck.exibir_mensagem_abort(263176);
	end if;
	
	insert into proj_ata(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_ata,
			cd_estabelecimento,
			cd_consultor,
			ds_ata,
			ie_tipo_ata,
			ie_situacao)
		values(	nr_seq_ata_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_estabelecimento_w,
			cd_pessoa_w,
			ds_ata_w,
			'Q',
			'A');

	open c01;
	loop
	fetch c01 into
		ds_pauta_w;
	exit when c01%notfound;
		begin
		insert into proj_ata_pauta(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_ata,
					ds_pauta)
				values(	proj_ata_pauta_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_ata_p,
					ds_pauta_w);
		
		end;
	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into
		ds_titulo_w,
		ds_conteudo_w,
		nr_seq_apresentacao_w;
	exit when c02%notfound;
		begin	

		insert into proj_ata_conteudo(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_ata,
					ds_conteudo,
					ds_titulo,
					nr_seq_apresentacao)
				values(	proj_ata_conteudo_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_ata_p,
					ds_conteudo_w,
					ds_titulo_w,
					nr_seq_apresentacao_w);
		
		end;
	end loop;
	close c02;

	commit;
	end;
end if;

end qua_gerar_ata_padrao;
/