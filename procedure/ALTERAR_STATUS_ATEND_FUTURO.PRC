create or replace
procedure alterar_status_atend_futuro ( cd_pessoa_fisica_p	Varchar2) is 

nr_seq_status_w		number(10);
nr_seq_atend_futuro_w	number(10);

begin

select	max(nr_sequencia)
into	nr_seq_status_w
from   	status_atend_futuro
where  	ie_status = 5;

select	max(nr_sequencia)
into	nr_seq_atend_futuro_w
from   	atend_pac_futuro
where  	cd_pessoa_fisica = cd_pessoa_fisica_p
and    	nr_atendimento is null;

update	atend_pac_futuro
set	nr_seq_status 	= nr_seq_status_w
where	nr_sequencia 	= nr_seq_atend_futuro_w;

commit;

end alterar_status_atend_futuro;
/