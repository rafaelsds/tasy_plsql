create or replace
procedure lic_aprovar_troca_marca(		nr_sequencia_p		number,
					ds_justificativa_p		varchar2,
					nm_usuario_p		Varchar2) is 

ds_marca_w			varchar2(255);
ds_marca_original_w		varchar2(255);
ds_justificativa_w			varchar2(255);
ds_justificativa_ww			varchar2(255);
nr_seq_reg_compra_item_w		number(10);
ds_historico_w			varchar2(4000);
cd_material_w			number(6);
ds_material_w			varchar2(255);
nr_seq_reg_compra_w		number(10);
nr_seq_marca_w			number(10);
qt_existe_w			number(10);
begin

select	nr_seq_reg_compra_item,
	ds_marca,
	ds_marca_original,
	ds_justificativa
into	nr_seq_reg_compra_item_w,
	ds_marca_w,
	ds_marca_original_w,
	ds_justificativa_w
from	reg_lic_troca_marca
where	nr_sequencia = nr_sequencia_p;

select	nr_seq_reg_compra,
	cd_material,
	substr(obter_desc_material(cd_material),1,255) ds_material
into	nr_seq_reg_compra_w,
	cd_material_w,
	ds_material_w
from	reg_compra_item
where	nr_sequencia	= nr_seq_reg_compra_item_w;

if	(ds_marca_w is not null) then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_marca_w
	from	marca
	where	elimina_acentuacao(upper(ds_marca)) = elimina_acentuacao(upper(ds_marca_w));
	
	if	(nr_seq_marca_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(266170,'DS_MARCA=' || ds_marca_w);
		--'N�o existe nenhuma marca cadastrada no sistema com o nome ' || ds_marca_w || '.' || chr(13) || chr(10) || 'Favor cadastra-la para dar continuidade no processo.');
	else
		select	count(*)
		into	qt_existe_w
		from	material_marca
		where	cd_material = cd_material_w
		and	nr_sequencia = nr_seq_marca_w;

		if	(qt_existe_w = 0) then
		
			insert into material_marca(
				cd_material,
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				qt_prioridade,
				ie_situacao)
			values(	cd_material_w,
				nr_seq_marca_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				1,
				'A');
		end if;	
	end if;	
end if;

ds_justificativa_ww	:= ds_justificativa_w;

if	(ds_justificativa_p is not null) then
	
	if	(ds_justificativa_w is not null) then
		ds_justificativa_ww := substr(ds_justificativa_w || chr(13) || chr(10) || ds_justificativa_p,1,255);
	else
		ds_justificativa_ww := substr(ds_justificativa_p,1,255);
	end if;
end if;

update	reg_compra_item
set	ds_marca		= ds_marca_w
where	nr_sequencia	= nr_seq_reg_compra_item_w;

update	reg_lic_troca_marca
set	dt_aprovacao	= sysdate,
	nm_usuario_aprov	= nm_usuario_p,
	ds_justificativa	= substr(ds_justificativa_ww,1,255)
where	nr_sequencia	= nr_sequencia_p;

ds_historico_w := 	substr(WHEB_MENSAGEM_PCK.get_texto(310370, 'CD_MATERIAL_W=' || cd_material_w || ';DS_MATERIAL_W=' || ds_material_w || ';DS_MARCA_ORIGINAL_W=' || ds_marca_original_w) || ds_marca_w || '.',1,4000); --Aprovado a troca da marca do material ' || cd_material_w || ' - ' || ds_material_w || ', de ' || ds_marca_original_w || ' para 
			
lic_gerar_historico_reg_preco(nr_seq_reg_compra_w, ds_historico_w, 'S', nm_usuario_p);

commit;

end lic_aprovar_troca_marca;
/
