create or replace
procedure REP_copia_indicacao_solic(
			nr_prescricao_orig_p	number,
			nr_prescricao_p		number,
			nr_seq_mat_orig_p	number,
			nr_seq_material_p	number,
			nm_usuario_p		varchar2) is 

qt_dia_prev_util_w	 number(5);
nr_seq_solic_orig_w	 number(10);
nr_seq_indicacao_w	 number(10);
nr_seq_solic_w		 number(10);
nr_seq_solic_indic_w number(10);
ds_result_exame_w	 varchar2(255);
ds_complemento_w	 varchar2(255);

cursor c01 is	
	select	c.nr_sequencia nr_seq_solic_orig,
			c.qt_dia_prev_util
	from	prescr_medica_solic c,
		prescr_material a
	where	a.nr_prescricao = nr_prescricao_orig_p 
	and	a.nr_prescricao	= c.nr_prescricao
	and	a.nr_sequencia	= c.nr_seq_material
	order by 1;

cursor c02 is	
	select	nr_seq_indicacao,
		substr(ds_resultado_exame,1,255) ds_resultado_exame,
		substr(ds_complemento,1,255) ds_complemento   
	from	prescr_medica_indic_solic
	where	nr_seq_solic = nr_seq_solic_orig_w
	order by 1;

begin

open C01;
loop
fetch C01 into	
	nr_seq_solic_orig_w,
	qt_dia_prev_util_w;
exit when C01%notfound;
	begin
	
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_solic_w
	from	prescr_medica_solic;
	
	insert into prescr_medica_solic(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_prescricao,
		nr_seq_material,
		qt_dia_prev_util
	) values (
		nr_seq_solic_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_prescricao_p,
		nr_seq_material_p,
		qt_dia_prev_util_w);
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_indicacao_w,
		ds_result_exame_w,
		ds_complemento_w;
	exit when C02%notfound;
		begin
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_solic_indic_w
		from	prescr_medica_indic_solic;
		
		insert into prescr_medica_indic_solic(
			nr_sequencia,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_solic,
			nr_seq_indicacao,
			ds_resultado_exame,
			ds_complemento
		) values (
			nr_seq_solic_indic_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_solic_w,
			nr_seq_indicacao_w,
			ds_result_exame_w,
			ds_complemento_w
		);
		
		end;
	end loop;
	close C02;
	
	commit;
	end;
end loop;
close C01;

commit;

end REP_copia_indicacao_solic;
/