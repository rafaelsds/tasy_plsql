CREATE OR REPLACE
PROCEDURE APAC_ATUALIZAR_CONTA (
				nr_sequencia_p	number,
				nm_usuario_p		varchar2) is

nr_atendimento_w		number(10);
nr_apac_w			number(13);
cd_medico_resp_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
cd_estabelecimento_w		number(4);
cd_cgc_prestador_w		varchar2(14);

cd_setor_atendimento_w		number(5);
dt_entrada_unidade_w		date;
nr_seq_atepacu_w		number(10);

cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_seq_atecaco_w		number(10);

nr_prescricao_w			number(14);
nr_seq_exame_w			number(10);
cd_material_exame_w		varchar2(20);
nr_seq_item_prescr_w		number(6);
dt_prescricao_w			date;
cd_setor_prescr_w		number(5);
cd_medico_prescr_w		varchar2(10);
cd_medico_proc_apac_w		varchar2(10);

ie_tipo_apac_w			number(2);
cd_motivo_cobranca_w		number(3);
cd_procedimento_w		number(15);
cd_atividade_prof_w		number(3);
qt_procedimento_w		number(11,4);
cd_cgc_fornecedor_w		varchar2(14);
nr_nota_fiscal_w		varchar2(255);
nr_seq_proc_apac_w		number(10);
nr_seq_propaci_w		number(10);

nr_seq_protocolo_w		number(10);
dt_mesano_referencia_w		date;
dt_entrada_w			date;
nr_interno_conta_w		number(10);
ds_erro_w			varchar2(255);
ie_vincular_prot_w		varchar2(1);
ie_tipo_protocolo_w		Number(2);
cd_setor_apac_w			number(5);
ie_fecha_atend_w		Varchar2(1);

ie_consiste_comp_w		varchar2(1);
ie_consiste_apac_prot_w		varchar2(1);
dt_fim_validade_w		date;

cursor C01 is
select	a.nr_atendimento,
	a.cd_setor_atendimento,
	b.nr_sequencia,
	b.cd_procedimento,
	b.cd_atividade_prof,
	b.qt_procedimento,
	b.cd_cgc_fornecedor,
	b.nr_nota_fiscal,
	b.cd_medico_exec
from	sus_apac_proc	b,
	sus_apac_movto	a
where	a.nr_sequencia	= nr_sequencia_p
and	a.nr_sequencia	= b.nr_seq_apac
and	a.ds_inconsistencia 	is null
and	b.nr_seq_propaci	is null
order by	b.nr_sequencia;
	

BEGIN

/*
select	nvl(vl_parametro, vl_parametro_padrao)
into	ie_vincular_prot_w
from	funcao_parametro
where	cd_funcao = 930
and	nr_sequencia = 4;
*/

nr_atendimento_w	:= null;

/* Buscar dados do atendimento */
begin
select	a.nr_atendimento,
	a.nr_apac,
	b.cd_medico_resp,
	b.cd_pessoa_fisica,
	b.cd_estabelecimento,
	c.cd_cgc,
	b.dt_entrada,
	a.ie_tipo_apac,
	a.dt_fim_validade
into	nr_atendimento_w,
	nr_apac_w,
	cd_medico_resp_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	cd_cgc_prestador_w,
	dt_entrada_w,
	ie_tipo_apac_w,
	dt_fim_validade_w
from	estabelecimento c,
	atendimento_paciente b,
	sus_apac_movto a
where	a.nr_atendimento	= b.nr_atendimento
and	a.nr_sequencia	= nr_sequencia_p
and	a.ds_inconsistencia 	is null
and	b.cd_estabelecimento = c.cd_estabelecimento;
exception
	when others then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278971);
end;

obter_param_usuario(930, 4, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_vincular_prot_w);

if	(nr_atendimento_w is null) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278972);
end if;

/*Buscar o tipo de protocolo para APAC */
select	nvl(max(ie_tipo_protocolo),21)
into	ie_tipo_protocolo_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

/* Buscar dados do conv�nio */
select	a.cd_convenio,
	a.cd_categoria,
	a.nr_seq_interno
into	cd_convenio_w,
	cd_categoria_w,
	nr_seq_atecaco_w
from	atend_categoria_convenio a
where	a.nr_atendimento	= nr_atendimento_w
and	a.nr_seq_interno	=
	(select max(x.nr_seq_interno)
		from atend_categoria_convenio x
		where x.nr_atendimento = a.nr_atendimento);

/* Buscar dados da unidade */
begin
select	a.cd_setor_atendimento,
	a.dt_entrada_unidade,
	a.nr_seq_interno
into	cd_setor_atendimento_w,
	dt_entrada_unidade_w,
	nr_seq_atepacu_w
from	atend_paciente_unidade a
where	a.nr_atendimento	= nr_atendimento_w
and	a.dt_entrada_unidade 	=
	(select max(x.dt_entrada_unidade)
		from atend_paciente_unidade x
		where x.nr_atendimento = a.nr_atendimento);
exception
	when others then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278973);
end;

update	sus_apac_proc
set	nr_seq_propaci	= null
where	nr_seq_apac	= nr_sequencia_p;

open C01;
loop
fetch c01 into
	nr_atendimento_w,
	cd_setor_apac_w,
	nr_seq_proc_apac_w,
	cd_procedimento_w,
	cd_atividade_prof_w,
	qt_procedimento_w,
	cd_cgc_fornecedor_w,
	nr_nota_fiscal_w,
	cd_medico_proc_apac_w;
	exit when c01%notfound;
	BEGIN

	/* Buscar procedimento executado manualmente pelo TASY */
	select	nvl(max(a.nr_sequencia),0)
	into	nr_seq_propaci_w
	from	procedimento_paciente a,
		conta_paciente x
	where	x.nr_interno_conta	= a.nr_interno_conta
	and	x.ie_status_acerto	= 1
	and	((x.nr_seq_protocolo	= 0) or (x.nr_seq_protocolo is null))
	and	a.nr_atendimento	= nr_atendimento_w
	and	a.cd_procedimento	= cd_procedimento_w
	and	a.ie_origem_proced	= 3
	and	a.cd_motivo_exc_conta	is null
	and	exists (	select	1
				from	sus_apac_proc b
				where	b.nr_seq_apac 		= nr_sequencia_p
				and	a.cd_procedimento 	= b.cd_procedimento
				and	a.ie_origem_proced 	= b.ie_origem_proced);

	/* Criar procedimento paciente a partir do registro da APAC */
	if	(nr_seq_propaci_w	= 0) then
		BEGIN
		/* Buscar dados da prescri��o m�dica */
		begin
		select	a.nr_prescricao,
			a.nr_sequencia,
			a.nr_seq_exame,
			a.cd_material_exame,
			nvl(a.cd_setor_atendimento,b.cd_setor_atendimento),
			nvl(a.cd_medico_exec,b.cd_medico),	
			b.dt_prescricao
		into	nr_prescricao_w,
			nr_seq_item_prescr_w,
			nr_seq_exame_w,	
			cd_material_exame_w,
			cd_setor_prescr_w,
			cd_medico_prescr_w,
			dt_prescricao_w
		from	prescr_medica b,
			prescr_procedimento a
		where	a.nr_prescricao	= b.nr_prescricao
		and	a.cd_procedimento	= cd_procedimento_w
		and	b.nr_atendimento	= nr_atendimento_w
		and	a.dt_baixa		is null
		and	a.nr_sequencia	=
			(select max(x.nr_sequencia)
				from prescr_procedimento x
				where x.nr_prescricao 	= a.nr_prescricao
				and     x.cd_procedimento 	= a.cd_procedimento);
		exception
			when others then
			nr_prescricao_w := null; 
		end;

		/* Incluir na procedimento_paciente */
		select	procedimento_paciente_seq.nextval
		into	nr_seq_propaci_w
		from	dual;

		begin
		insert into 	procedimento_paciente(
			NR_SEQUENCIA, NR_ATENDIMENTO, DT_ENTRADA_UNIDADE, CD_PROCEDIMENTO,
			DT_PROCEDIMENTO, QT_PROCEDIMENTO, DT_ATUALIZACAO, NM_USUARIO,
			CD_MEDICO, CD_CONVENIO, CD_CATEGORIA, CD_PESSOA_FISICA,
			DT_PRESCRICAO, DS_OBSERVACAO, VL_PROCEDIMENTO, 	VL_MEDICO,
			VL_ANESTESISTA, VL_MATERIAIS, CD_EDICAO_AMB, CD_TABELA_SERVICO, 
			DT_VIGENCIA_PRECO, CD_PROCEDIMENTO_PRINC, DT_PROCEDIMENTO_PRINC, DT_ACERTO_CONTA, 
			DT_ACERTO_CONVENIO, DT_ACERTO_MEDICO, VL_AUXILIARES, VL_CUSTO_OPERACIONAL, 
			TX_MEDICO, TX_ANESTESIA, NR_PRESCRICAO, NR_SEQUENCIA_PRESCRICAO, 
			CD_MOTIVO_EXC_CONTA, DS_COMPL_MOTIVO_EXCON, CD_ACAO, QT_DEVOLVIDA, 
			CD_MOTIVO_DEVOLUCAO, NR_CIRURGIA, NR_DOC_CONVENIO, CD_MEDICO_EXECUTOR, 
			IE_COBRA_PF_PJ, NR_LAUDO, DT_CONTA, CD_SETOR_ATENDIMENTO, 
			CD_CONTA_CONTABIL, CD_PROCEDIMENTO_AIH, IE_ORIGEM_PROCED, NR_AIH, 
			IE_RESPONSAVEL_CREDITO, TX_PROCEDIMENTO, CD_EQUIPAMENTO, IE_VALOR_INFORMADO, 
			CD_ESTABELECIMENTO_CUSTO, CD_TABELA_CUSTO, CD_SITUACAO_GLOSA, NR_LOTE_CONTABIL, 
			CD_PROCEDIMENTO_CONVENIO, NR_SEQ_AUTORIZACAO, 	IE_TIPO_SERVICO_SUS, IE_TIPO_ATO_SUS, 
			CD_CGC_PRESTADOR, NR_NF_PRESTADOR, CD_ATIVIDADE_PROF_BPA, NR_INTERNO_CONTA, 
			NR_SEQ_PROC_PRINC, IE_GUIA_INFORMADA, DT_INICIO_PROCEDIMENTO, IE_EMITE_CONTA, 
			IE_FUNCAO_MEDICO, IE_CLASSIF_SUS, 	CD_ESPECIALIDADE, NM_USUARIO_ORIGINAL, 
			NR_SEQ_PROC_PACOTE, IE_TIPO_PROC_SUS, CD_SETOR_RECEITA, VL_ADIC_PLANT, 
			NR_SEQ_ATEPACU, ie_auditoria)
		values	(nr_seq_propaci_w, nr_atendimento_w,dt_entrada_unidade_w,cd_procedimento_w,
			dt_entrada_unidade_w, qt_procedimento_w, sysdate, nm_usuario_p,
			cd_medico_resp_w, cd_convenio_w,cd_categoria_w, null,
			dt_prescricao_w,null, 0, 0,
			0, 0,null,null,
			null, null, null, null,
			null, null, 0, 0,
			1, 1, nr_prescricao_w, nr_seq_item_prescr_w,
			null, null, null, null,
			null, null, null, nvl(cd_medico_proc_apac_w,nvl(cd_medico_prescr_w,cd_medico_resp_w)),
			null, null, null, nvl(cd_setor_apac_w,nvl(cd_setor_prescr_w,cd_setor_atendimento_w)),
			null, null, 3, null,
			null, null, null, 'N',
			cd_estabelecimento_w, null, null, null,		
			null, null, null, null,
			cd_cgc_prestador_w, null, cd_atividade_prof_w, null,
			null, null, null, null,
			null, null, null, nm_usuario_p,
			null, null, nvl(cd_setor_apac_w,nvl(cd_setor_prescr_w,cd_setor_atendimento_w)), 0,
			nr_seq_atepacu_w,null);		
		exception
			when others then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278974);
		end;
		END;
	else
		update	procedimento_paciente
		set	qt_procedimento	= qt_procedimento_w
		where	nr_sequencia	= nr_seq_propaci_w;
	end if;

	/* Identificar �tem da APAC com procedimento paciente */
	update	sus_apac_proc
	set	nr_seq_propaci 	= nr_seq_propaci_w
	where	nr_sequencia	= nr_seq_proc_apac_w;
	commit;

	/* Atualizar preco da procedimento paciente  */
	Atualiza_Preco_Procedimento(nr_seq_propaci_w,cd_convenio_w,nm_usuario_p);

	END;
end loop;
close C01;


/* Buscar a conta paciente gerada para a APAC */
select	nvl(max(a.nr_interno_conta),0)
into	nr_interno_conta_w
from	sus_apac_proc c,
	procedimento_paciente b,
	conta_paciente a
where	a.nr_atendimento	= nr_atendimento_w
and	a.cd_convenio_parametro	= cd_convenio_w
/*and	a.ie_status_acerto	= 1 Retirado para pegar as contas fechadas na libera��o do laudo (Edilson Adelson 

27/10/04)*/
and	a.nr_seq_protocolo is null
and	a.nr_interno_conta	= b.nr_interno_conta
and	b.nr_sequencia		= c.nr_seq_propaci
and	b.cd_motivo_exc_conta	is null;

if	(nr_interno_conta_w > 0) then
	BEGIN
	/* Identificar movto da APAC com a conta paciente */
	update	sus_apac_movto
	set	nr_interno_conta	= nr_interno_conta_w
	where	nr_sequencia		= nr_sequencia_p
	and	nr_interno_conta	is null;

	Obter_Param_Usuario(930,9,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_fecha_atend_w);

	if	((ie_fecha_atend_w = 'D') and
		(((ie_tipo_apac_w = 3) or (ie_tipo_apac_w = 2)) and 
		(trunc(sysdate,'month') = trunc(dt_fim_validade_w,'month')))) or
		(ie_fecha_atend_w = 'S') then
		/* Alta do atendimento */
		Saida_Setor_Servico(nr_atendimento_w,cd_setor_atendimento_w,null, null, nm_usuario_p);
	else
		/*Fecha s� a conta, sem alta do atendimento*/
		Fechar_Conta_Paciente(nr_interno_conta_w,nr_atendimento_w,2,nm_usuario_p,ds_erro_w);
	end if;

	/* Buscar o protocolo convenio que dever� ser incluida a APAC */
	select	nvl(max(nr_seq_protocolo),0)
	into	nr_seq_protocolo_w
	from	protocolo_convenio
	where	cd_convenio		= cd_convenio_w
	and	ie_tipo_protocolo	= ie_tipo_protocolo_w
	and	ie_status_protocolo	= 1
	and	cd_estabelecimento	= cd_estabelecimento_w; /*Felipe Martini OS77052*/

	if	(nr_seq_protocolo_w	= 0) or
		(nr_seq_protocolo_w is null) then
		begin
		select	nvl(max(dt_competencia_apac),dt_entrada_w)
		into	dt_mesano_referencia_w
		from	sus_parametros
		where	cd_estabelecimento = cd_estabelecimento_w;

		select	protocolo_convenio_seq.nextval
		into	nr_seq_protocolo_w
		from	dual;

		begin
		insert into	protocolo_convenio(
			CD_CONVENIO, NR_PROTOCOLO, IE_STATUS_PROTOCOLO, DT_PERIODO_INICIAL,
			DT_PERIODO_FINAL, DT_ATUALIZACAO, NM_USUARIO, IE_TIPO_PROTOCOLO,
			NR_SEQ_PROTOCOLO, DT_ENVIO, NM_USUARIO_ENVIO, DS_ARQUIVO_ENVIO,
			DT_RETORNO, NM_USUARIO_RETORNO, DS_ARQUIVO_RETORNO, CD_SETOR_ATENDIMENTO,
			CD_ESPECIALIDADE, IE_PERIODO_FINAL, DS_PARAMETRO_ATEND, DT_GERACAO,
			NM_USUARIO_GERACAO, DT_MESANO_REFERENCIA, CD_CLASSIF_SETOR, IE_TIPO_ATEND_BPA,
			DT_CONSISTENCIA, NR_SEQ_ENVIO_CONVENIO, DT_MESANO_REF_PAR, DS_INCONSISTENCIA,
			DT_VENCIMENTO, DT_INTEGRACAO_CR, DT_ENTREGA_CONVENIO, CD_INTERFACE_ENVIO,
			CD_ESTABELECIMENTO,NR_SEQ_LOTE_RECEITA,NR_SEQ_LOTE_REPASSE,NR_SEQ_LOTE_GRAT)
		values
			(cd_convenio_w,to_char(nr_seq_protocolo_w),1,sysdate,
			sysdate,sysdate,nm_usuario_p,ie_tipo_protocolo_w,
			nr_seq_protocolo_w,null,null,null,
			null,null,null,null,
			null,null,null,sysdate,
			nm_usuario_p,dt_mesano_referencia_w,null,null,
			null,null,null,null,
			null,null,null,null,
			cd_estabelecimento_w,0,0,0);
		exception
			when others then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278975);
		end;
		end;
	end if;

	if	((ie_fecha_atend_w = 'D') and
		(((ie_tipo_apac_w = 3) or (ie_tipo_apac_w = 2)) and 
		(trunc(sysdate,'month') = trunc(dt_fim_validade_w,'month')))) or
		(ie_fecha_atend_w = 'S') then
		/* Fechar a conta paciente gerada para a APAC */
		Finalizar_Atendimento(nr_atendimento_w,'S',nm_usuario_p,ds_erro_w);
	end if;

	select	sus_consiste_comp_apac_prot(nr_interno_conta_W, nr_seq_protocolo_w)
	into	ie_consiste_apac_prot_w
	from	dual;
	
	select	Obter_Valor_Param_Usuario(930, 13, Obter_Perfil_Ativo, nm_usuario_p, cd_Estabelecimento_W)
	into	ie_consiste_comp_w
	from	dual;

	/* Inserir a conta no protocolo */
	if	(ie_vincular_prot_w = 'S') then
		if	(ie_consiste_comp_w <> 'S') and
			(ie_consiste_apac_prot_w = 'N')then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278976);			
		else
			update	conta_paciente
			set	nr_seq_protocolo 	= nr_seq_protocolo_w
			where	nr_interno_conta	= nr_interno_conta_w
			and	ie_status_acerto	= 2; 
		end if;
	end if;
	END;
end if;
commit;
END APAC_ATUALIZAR_CONTA;
/