create or replace
procedure RECALCULAR_TITULO_REC_LIQ_CC
	(nr_titulo_p		in	number,
	 nr_seq_baixa_p		in	number,
	 nm_usuario_p		in	varchar2) is

cd_estabelecimento_w	number(10,0);
nr_seq_baixa_w		number(5,0);
nr_titulo_contab_w	number(10);
nr_seq_liq_origem_w	titulo_receber_liq.nr_seq_liq_origem%type;
count_w			number(10);
ie_baixa_estornada_w	varchar2(1);

Cursor C01 is
select	nr_sequencia
from	titulo_receber_liq
where	nr_titulo = nr_titulo_p
and	(nvl(nr_seq_baixa_p,0) = 0 or nr_sequencia = nr_seq_baixa_p); 

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p;

open C01;
loop
fetch C01 into	
	nr_seq_baixa_w;
exit when C01%notfound;
	begin
	
	select	obter_se_baixa_estornada(nr_titulo_p, nr_seq_baixa_w, 'R')
	into	ie_baixa_estornada_w
	from	dual;
	/*OS 1321467 - Somente fazer o processo se n�o for uma baixa estornada.*/
	if (nvl(ie_baixa_estornada_w,'N') = 'N') then
	
		select	max(a.nr_seq_liq_origem)
		into	nr_seq_liq_origem_w
		from	titulo_receber_liq a
		where	a.nr_titulo		= nr_titulo_p
		and		a.nr_sequencia	= nr_seq_baixa_w;
		
		delete	from titulo_rec_liq_cc
		where	nr_titulo	= nr_titulo_p
		and	nr_seq_baixa	= nr_seq_baixa_w;

		gerar_titulo_rec_liq_cc(cd_estabelecimento_w, null, nm_usuario_p, nr_titulo_p, nr_seq_baixa_w);
		pls_gerar_tit_rec_liq_mens(nr_titulo_p,	nr_seq_baixa_w, nm_usuario_p,nr_titulo_contab_w);
		
		select	count(*)
		into	count_w
		from	titulo_rec_liq_cc
		where	nr_titulo 		= nr_titulo_p
		and		nr_seq_baixa 	= nr_seq_baixa_w;
		
		/*se for recalculo de uma baixa de estorno, pegar os dados da baixa que foi estornada e lan�ar negativo, igual ocorre na ESTORNAR_TIT_RECEBER_LIQ */
		if (nr_seq_liq_origem_w is not null) and (count_w = 0) then
		
			insert into titulo_rec_liq_cc
						(NR_SEQUENCIA,
						NR_TITULO,
						NR_SEQ_BAIXA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						CD_CENTRO_CUSTO,
						VL_BAIXA,
						VL_AMAIOR,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO_NREC,
						CD_CTA_CTB_ORIGEM,
						CD_CONTA_CONTABIL,
						VL_RECEBIDO,
						CD_CONTA_DEB_PLS,
						CD_CONTA_REC_PLS,
						NR_SEQ_MENS_SEG_ITEM,
						CD_HISTORICO_PLS,
						NR_SEQ_CONTA_PLS,
						NR_SEQ_PRODUTO,
						cd_conta_antec_pls,
						cd_historico_antec_pls,
						vl_antecipacao_mens,
						vl_pro_rata,
						ie_lote_pro_rata,
						vl_contab_pro_rata,
						cd_conta_rec_antecip,
						cd_conta_deb_antecip,
						cd_historico_rev_antec,
						ie_origem_classif,
						vl_perdas,
						vl_recebido_estrang,
						vl_complemento,
						vl_cotacao,
						cd_moeda,
						vl_desconto,
						vl_juros,
						vl_multa)
					select	titulo_rec_liq_cc_seq.nextval,
						nr_titulo_p,
						nr_seq_baixa_w,
						sysdate,
						nm_usuario_p,
						cd_centro_custo,
						vl_baixa * -1,
						vl_amaior * -1,
						sysdate,
						nm_usuario_p,
						cd_cta_ctb_origem,
						cd_conta_contabil,
						vl_recebido * -1,
						cd_conta_deb_pls,
						cd_conta_rec_pls,
						nr_seq_mens_seg_item,
						cd_historico_pls,
						nr_seq_conta_pls,
						nr_seq_produto,
						cd_conta_antec_pls,
						cd_historico_antec_pls,
						vl_antecipacao_mens * - 1,
						vl_pro_rata * - 1,
						ie_lote_pro_rata,
						vl_contab_pro_rata * - 1,
						cd_conta_rec_antecip,
						cd_conta_deb_antecip,
						cd_historico_rev_antec,
						ie_origem_classif,
						decode(vl_perdas,null,null,(vl_perdas*-1)),
						decode(vl_recebido_estrang,null,null,(vl_recebido_estrang * -1)), --Projeto Multimoeda - grava os valores em moeda estrangeira
						decode(vl_recebido_estrang,null,null,(vl_complemento * -1)),
						decode(vl_recebido_estrang,null,null,vl_cotacao),
						decode(vl_recebido_estrang,null,null,cd_moeda),
						vl_desconto * -1,
						vl_juros * -1,
						vl_multa * -1
					from	titulo_rec_liq_cc
					where	nr_titulo	= nr_titulo_p
					and	nr_seq_baixa	= nr_seq_liq_origem_w;
		
		end if;
	
	end if;
	
	/*if	(nr_titulo_contab_w is not null) then  Retirado essa consistencia devido a ter sido criad a rotina consiste_recalc_classif_tit para consistir sem abortar o recalculo.
		wheb_mensagem_pck.exibir_mensagem_abort(236517, 'NR_TITULO=' || nr_titulo_contab_w);
	end if;*/
		
	end;
end loop;
close C01;
commit;
end RECALCULAR_TITULO_REC_LIQ_CC;
/