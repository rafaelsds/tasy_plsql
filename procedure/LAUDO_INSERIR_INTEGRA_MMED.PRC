create or replace
procedure laudo_inserir_integra_mmed (nr_acesso_dicom_p	  number,					
                                      ds_nome_diretorio_p	varchar2,					
                                      ds_sigla_conselho_p	varchar2,
                                      ds_codigo_prof_p	  varchar2,
                                      uf_conselho_p		    varchar2,
                                      ds_laudo_p		      varchar2) is 
					
nr_prescricao_w		      number(14);
nr_seq_prescricao_w	    number(10);
nr_sequencia_w		      number(10);
nr_seq_imagem_w		      number(10);
nr_laudo_w		          number(10);
nr_atendimento_w	      number(10);
cd_medico_resp_w	      varchar2(10) := null;
dt_entrada_unidade_w	  date;
ds_titulo_laudo_w	      varchar2(255);
nr_seq_propaci_w	      number(15);
dt_procedimento_w	      date;
nr_seq_laudo_w		      number(10);
nr_seq_proc_interno_w	  number(15);
cd_procedimento_w	      number(15);
ie_origem_proced_w	    number(10);
qt_procedimento_w	      number(15);
cd_setor_atendimento_w	number(10);
dt_prev_execucao_w	    date;
cd_medico_exec_w	      varchar2(10);
ie_lado_w		            varchar2(15);
ds_laudo_copia_w	      long;
nr_seq_laudo_ant_w	    number(10);
nr_seq_laudo_atual_w	  number(10);
cd_especialidade_w		  number(5);
ie_status_execucao_w	  varchar2(3);
nr_seq_conselho_w	      number(10);
cd_medico_prescr_w	    varchar2(10);
nr_interno_conta_w	    number(10);
ie_status_acerto_w	    number(1);
nm_usuario_w		        varchar2(15);
cd_convenio_w	          number(10);
ie_cancelar_laudo_w		varchar2(1);	

begin

gravar_log_cdi(987,	'log01'||nr_acesso_dicom_p||'-'||
                    ds_nome_diretorio_p||'-'||
                    ds_sigla_conselho_p||'-'||
                    ds_codigo_prof_p||'-'||
                    uf_conselho_p||'-'||
                    substr(ds_laudo_p,1,3500)
                  , 'tasy');

if (nr_acesso_dicom_p is not null) and
   (ds_nome_diretorio_p is not null) then
  select max(a.nr_prescricao),
         max(a.nr_sequencia),
      	 max(nr_seq_proc_interno),
         max(cd_procedimento),
         max(ie_origem_proced),
         max(qt_procedimento),
         max(cd_setor_atendimento),
         max(dt_prev_execucao),
         max(cd_medico_exec),
         max(nvl(ie_lado,'A'))
	  into nr_prescricao_w,
         nr_seq_prescricao_w,
         nr_seq_proc_interno_w,
         cd_procedimento_w,
         ie_origem_proced_w,
         qt_procedimento_w,
         cd_setor_atendimento_w,
         dt_prev_execucao_w,
         cd_medico_exec_w,
         ie_lado_w
	  from prescr_procedimento a
	 where a.nr_acesso_dicom = nr_acesso_dicom_p;
	
	/*--  In�cio busca o m�dico do conselho informado  --*/
	if ((ds_sigla_conselho_p = 'CRM') or
		  (ds_sigla_conselho_p = 'CRO') or
		  (ds_sigla_conselho_p = 'CRF')) then
    select max(cd_pessoa_fisica)
		  into cd_medico_resp_w
		  from medico
		 where nr_crm = ds_codigo_prof_p
		   and uf_crm = uf_conselho_p;
	end if;
	
	if (cd_medico_resp_w is null) then
		select max(nr_sequencia)
    	into nr_seq_conselho_w
      from conselho_profissional
     where sg_conselho = ds_sigla_conselho_p;
		
		if (nr_seq_conselho_w is not null) then
			select max(cd_pessoa_fisica)
			  into cd_medico_resp_w
			  from pessoa_fisica
			 where nr_seq_conselho = nr_seq_conselho_w
			   and ds_codigo_prof = ds_codigo_prof_p
			   and sg_emissora_ci	= uf_conselho_p;
		end if;
	end if;	
	
	if (cd_medico_resp_w is null) then
		select max(a.cd_medico)
	  	into cd_medico_resp_w
	  	from prescr_medica a
	   where a.nr_prescricao = nr_prescricao_w;
	end if;
	
	/*--  Fim busca o m�dico do conselho informado  --*/		
	select nvl(max(a.nr_laudo),0)+1
	  into nr_laudo_w
	  from laudo_paciente a
	 where a.nr_prescricao = nr_prescricao_w;
	
	/*Executando a prescri��o*/
	select nvl(max(a.nr_sequencia),0),
         max(a.nr_atendimento),
         max(a.dt_entrada_unidade),
         max(a.dt_procedimento)		
    into nr_seq_propaci_w,
         nr_atendimento_w,
         dt_entrada_unidade_w,
         dt_procedimento_w
	  from procedimento_paciente a, conta_paciente b
	 where a.nr_interno_conta = b.nr_interno_conta
     and b.ie_cancelamento is null
     and a.nr_prescricao = nr_prescricao_w
     and a.nr_sequencia_prescricao = nr_seq_prescricao_w;
	
	if (nr_seq_propaci_w = 0) then
		begin
			Gerar_Proc_Pac_item_Prescr(nr_prescricao_w, 
                        				 nr_seq_prescricao_w, 
                                 null, 
                                 null,
                                 nr_seq_proc_interno_w,
                                 cd_procedimento_w, 
                                 ie_origem_proced_w,
                                 qt_procedimento_w, 
                                 cd_setor_atendimento_w,
                                 9, 
                                 dt_prev_execucao_w,
                                 'Multimed', 
                                 nvl(cd_medico_resp_w, cd_medico_exec_w), 
                                 null,
                                 ie_lado_w, 
                                 null);
						
      select max(nr_sequencia),
             max(nr_atendimento),
             max(dt_entrada_unidade),
             max(dt_procedimento),
             max(nr_interno_conta),
             max(nm_usuario)
        into nr_seq_propaci_w,
             nr_atendimento_w,
             dt_entrada_unidade_w,
             dt_procedimento_w,
             nr_interno_conta_w,
             nm_usuario_w
        from procedimento_paciente
       where nr_prescricao = nr_prescricao_w
         and nr_sequencia_prescricao = nr_seq_prescricao_w;
		
      gerar_lancamento_automatico(nr_atendimento_w,
                      						null,
                                  34,
                                  'Multimed',
                                  nr_seq_propaci_w,
                                  null,
                                  null,
                                  null,
                                  null,
                                  nr_interno_conta_w);
		
		end;
	end if;					
	
	select max(substr(obter_desc_prescr_proc_laudo(cd_procedimento, ie_origem_proced, nr_seq_proc_interno, ie_lado,  nr_seq_propaci_w),1,255))
	  into ds_titulo_laudo_w		
	  from prescr_procedimento a
	 where a.nr_prescricao = nr_prescricao_w
	   and a.nr_sequencia	= nr_seq_prescricao_w;

	
	select nvl(max(ie_cancelar_laudo),'N')
    into ie_cancelar_laudo_w
    from PARAMETRO_INTEGRACAO_PACS
    where cd_estabelecimento = (select max(cd_estabelecimento) 
								from prescr_medica 
								where nr_prescricao = nr_prescricao_w);
	
	if (ie_cancelar_laudo_w = 'S') then
		begin
			select  MAX(nr_sequencia)
			into	nr_seq_laudo_w
			from 	laudo_paciente
			where   nr_prescricao = nr_prescricao_w
			and	nr_seq_prescricao = nr_seq_prescricao_w;

			if	(nvl(nr_seq_laudo_w, 0) <> 0) then
				begin
					cancelar_laudo_paciente(nr_seq_laudo_w,'C','Multimed','');
				end;
			end if;
		end;
	end if;
	
	select laudo_paciente_seq.nextval
	into nr_seq_laudo_w
	from dual;
	   
	insert into laudo_paciente (nr_sequencia,
                              nr_atendimento,
                              dt_entrada_unidade,
                              nr_laudo,
                              nm_usuario,
                              dt_atualizacao,
                              cd_medico_resp,
                              ds_titulo_laudo,
                              dt_laudo,
                              nr_prescricao,
                              nr_seq_proc,
                              nr_seq_prescricao,
                              dt_liberacao,
                              qt_imagem,
                              ie_status_laudo,
                              dt_exame,
                              dt_integracao,
                              ds_arquivo,
                              ie_formato,
                              ds_laudo)
                      values (nr_seq_laudo_w,
                              nr_atendimento_w,
                              dt_entrada_unidade_w,
                              nr_laudo_w,
                              'Multimed',
                              sysdate,
                              cd_medico_resp_w,
                              ds_titulo_laudo_w,
                              sysdate,
                              nr_prescricao_w,			
                              nr_seq_propaci_w,
                              nr_seq_prescricao_w,
                              sysdate,
                              0,
                              'LL',
                              dt_procedimento_w,
                              sysdate,
                              ds_nome_diretorio_p,
                              3,
                              ds_laudo_p);

	select max(ie_status_acerto)
	  into ie_status_acerto_w
	  from conta_paciente
	 where nr_interno_conta = nr_interno_conta_w;
	
	if (nvl(ie_status_acerto_w,1) <> 2) then
		select max(cd_medico)
		  into cd_medico_prescr_w
		  from prescr_medica
		 where nr_prescricao = nr_prescricao_w;	

		cd_especialidade_w := to_number(obter_especialidade_medico(nvl(cd_medico_resp_w,cd_medico_exec_w),'C'));
		
		update procedimento_paciente
		   set nr_laudo = nr_seq_laudo_w,
			     cd_medico_req = cd_medico_prescr_w,
           cd_medico_executor = nvl(cd_medico_exec_w, cd_medico_resp_w),
           cd_especialidade = cd_especialidade_w
		 where nr_sequencia = nr_seq_propaci_w;		
	else	
		gravar_log_cdi(987, 'log02 conta '||nr_interno_conta_w||' com status 2, n�o pode ser alterada, nr_seq_proc'|| nr_seq_propaci_w,'tasy');
	end if;
		
	/* *****  Atualiza status execu��o na prescri��o ***** */
	update prescr_procedimento a
	   set a.ie_status_execucao = '40',
		     a.dt_baixa = sysdate,
         a.cd_motivo_baixa = 1,
		     a.nm_usuario = 'Multimed'
	 where a.nr_prescricao = nr_prescricao_w
	   and a.nr_sequencia in (select b.nr_sequencia_prescricao
                              from procedimento_paciente b
                             where b.nr_prescricao = a.nr_prescricao
                               and b.nr_sequencia_prescricao = a.nr_sequencia
                               and b.nr_laudo = nr_seq_prescricao_w);
                               
	select max(ie_status_execucao)
	  into ie_status_execucao_w
	  from prescr_procedimento a
	 where a.nr_prescricao = nr_prescricao_w
	   and a.nr_sequencia = nr_seq_prescricao_w;

	if (ie_status_execucao_w <> '40') then
		update prescr_procedimento a
		   set a.ie_status_execucao = '40',
			     a.dt_baixa = sysdate,
           a.cd_motivo_baixa = 1,
			     a.nm_usuario = 'Multimed'
		 where a.nr_prescricao = nr_prescricao_w
		   and a.nr_sequencia in (select b.nr_sequencia_prescricao
						                    from procedimento_paciente b
						                   where b.nr_prescricao = a.nr_prescricao
						                     and b.nr_prescricao = nr_prescricao_w
						                     and b.nr_sequencia_prescricao = a.nr_sequencia
						                     and b.nr_sequencia_prescricao = nr_seq_prescricao_w);	
	end if;
	
	insert into laudo_paciente_pdf (nr_sequencia,
                                  nr_acesso_dicom,
                                  nr_seq_laudo,
                                  dt_atualizacao,
                                  nm_usuario,
                                  dt_atualizacao_nrec,
                                  nm_usuario_nrec)
                          values (laudo_paciente_pdf_seq.nextval,
                                  nr_acesso_dicom_p,
                                  nr_seq_laudo_w,
                                  sysdate,
                                  'Multimed',
                                  sysdate,
                                  'Multimed');

  select max(obter_convenio_atendimento(nr_atendimento))
    into cd_convenio_w
    from prescr_medica
   where nr_prescricao = nr_prescricao_w;
   
  atualiza_preco_procedimento(nr_seq_propaci_w, cd_convenio_w, 'Multimed');	
end if;

end laudo_inserir_integra_mmed;
/