create or replace
procedure reg_operacoes_terc_w_delete(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		Varchar2 default '0') is 

begin

delete reg_operacoes_terc_w 
where nm_usuario = nm_usuario_p 
and ((nvl(nr_sequencia_p,'0') <> '0' and obter_se_contido(nr_sequencia,nr_sequencia_p) = 'S' ) or (nr_sequencia_p = '0'));

commit;

end reg_operacoes_terc_w_delete;
/
