create or replace
procedure w_inserir_recomendacoes(	ds_lista_recomendacoes_p	varchar2,
									cd_perfil_p					number,
									nm_usuario_p				varchar2,
									nr_prescricao_p				number) is 

cd_intervalo_w				varchar2(7);
nr_seq_classif_w			number(10,0);
ds_lista_recomendacoes_w	varchar2(2000);
nr_pos_virgula_w			number(10,0);
cd_tipo_recomend_w			number(10,0);
begin
if	(ds_lista_recomendacoes_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ds_lista_recomendacoes_w	:= ds_lista_recomendacoes_p;
	
	while (ds_lista_recomendacoes_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(ds_lista_recomendacoes_w,',');
		if	(nr_pos_virgula_w > 0) then
			begin
			cd_tipo_recomend_w	:= substr(ds_lista_recomendacoes_w,0,nr_pos_virgula_w-1);
			ds_lista_recomendacoes_w	:= substr(ds_lista_recomendacoes_w,nr_pos_virgula_w+1,length(ds_lista_recomendacoes_w));			
			end;
		else
			begin
			cd_tipo_recomend_w	:= to_number(ds_lista_recomendacoes_w);
			ds_lista_recomendacoes_w	:= null;
			end;
		end if;	
		
		if	(nvl(cd_tipo_recomend_w,0) > 0) then
			begin
			select	cd_intervalo,
				nr_seq_classif
			into	cd_intervalo_w,
				nr_seq_classif_w
			from	tipo_recomendacao
			where	cd_tipo_recomendacao = cd_tipo_recomend_w;
			
			w_inserir_recomendacao(
				nr_prescricao_p,
				cd_tipo_recomend_w,
				cd_intervalo_w,
				nm_usuario_p,
				cd_perfil_p,
				nr_seq_classif_w);
			end;
		end if;
		end;
	end loop;
	end;	
end if;
commit;
end w_inserir_recomendacoes;
/