create or replace
procedure atend_categ_befpost_abort_eup(	
						ie_resp_nao_medico_p		varchar2,
						dt_validade_carteira_p		date,
						ie_perm_cart_venc_p		varchar2,
						ie_tipo_convenio_p		number,
						ie_exige_guia_tipo_convenio_p	varchar2,
						cd_medico_resp_p		varchar2,
						ie_cons_permi_regra_atend_p	varchar2,
						nr_seq_cobertura_p		number,
						ie_visao_cobertura_p		varchar2,
						ie_convenios_nao_liberados_p	varchar2,
						ie_convenios_liberados_p	varchar2,
						ie_clinica_p			number,
						cd_pessoa_fisica_p		varchar2,
						ie_consiste_cart_convenio_w	varchar2,
						dt_entrada_p			date,
						cd_empresa_p			number,
						cd_categoria_p			varchar2,
						ie_tipo_guia_p			varchar2,
						cd_senha_p			varchar2,
						ie_consiste_guia_senha_p	varchar2,
						ie_atualiza_guia_tipo_guia_p	varchar2,
						ie_alterou_guia_p		varchar2,
						ie_edicao_registro_p		varchar2,
						ie_tipo_atendimento_p		number,
						nr_doc_convenio_old_p		varchar2,
						nr_doc_convenio_p		varchar2,
						nr_atendimento_p		number,
						qt_convenio_atend_p		number,
						ie_novo_registro_p		varchar2,
						cd_usuario_convenio_p		varchar2,
						cd_estabelecimento_p		number,
						ie_exige_conv_estab_p		varchar2,
						cd_convenio_p			number,
						cd_tipo_acomodacao_p		number,
						cd_plano_convenio_p		varchar2,
						nr_seq_visao_p			number,
						nm_usuario_p			Varchar2,
						ds_msg_abort_p		out	varchar2,
						ds_msg_abort_cons_cart_p out	varchar2,
						nr_doc_conv_retorno_p	 out	varchar2,
						ie_focar_no_convenio_p	out	varchar2) is 

ie_lib_acomod_plano_w			varchar2(1);
ie_convenio_estab_w			varchar2(1) := 'N';
ie_conv_exige_letra_cart_w		varchar2(1) := 'N';
ie_letra_carteira_w			varchar2(1);
qt_conv_atend_w				number(10);
nr_doc_convenio_w			varchar2(20);
nr_atend_guia_iguais_w			number(10);
ie_obriga_empresa_w			varchar2(1);
ie_lib_categ_tipo_atend_w		varchar2(1) := 'S';
qt_plano_convenio_p			number(10);
ie_convenio_contido_w			varchar2(1) := 'N';
ie_consiste_cod_conv_duplic_w		varchar2(1);
ds_consist_cd_con_dupl_w		varchar2(1);
ie_exige_cobertura_w			varchar2(1);
qt_medico_w				number(10);
ie_resp_atend_w				varchar2(1);
cd_usuario_convenio_w			varchar2(50);


begin

/* 
PROCEDURE CRIADA PARA REALIZAR TODAS AS VERIFICA��ES DO EVENTO BEFOREPOST QUE RETORNAM MSG COM ABORT
*/

ie_focar_no_convenio_p := 'N';
-- Tratamento para vis�o especifica
if	(nr_seq_visao_p = 18311) and   
	(nvl(cd_tipo_acomodacao_p,0) > 0) and
	(cd_plano_convenio_p is not null) then
	
	ie_lib_acomod_plano_w := Obter_Se_Lib_Acomod_Plano(cd_convenio_p, cd_plano_convenio_p, cd_tipo_acomodacao_p);
	if	(ie_lib_acomod_plano_w = 'N') then
		ds_msg_abort_p := substr(obter_texto_dic_objeto(285145, wheb_usuario_pck.get_nr_seq_idioma, 'DS_ACOMODACAO='||OBTER_DESC_TIPO_ACOMOD(cd_tipo_acomodacao_p) || 
		';DS_PLANO='|| Obter_Desc_Plano(cd_convenio_p,cd_plano_convenio_p)),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

if	(ie_visao_cobertura_p = 'S') then
	select	nvl(max(ie_exige_cobertura),'N') 
	into	ie_exige_cobertura_w
        from	convenio_estabelecimento 
        where	cd_convenio = cd_convenio_p
        and 	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(ie_exige_cobertura_w = 'S') and
		(nvl(nr_seq_cobertura_p,0) = 0) then
		ds_msg_abort_p := substr(obter_texto_tasy(90730, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;

end if;

nr_doc_convenio_w := nr_doc_convenio_p;

if	(ie_exige_conv_estab_p = 'C') then
	ie_convenio_estab_w := Obter_Valor_Conv_Estab(cd_convenio_p, cd_estabelecimento_p, 'IE_EXIGE_CARTEIRA_ATEND');
end if;
if	((ie_exige_conv_estab_p = 'S') or
	(ie_convenio_estab_w = 'S')) and
	(cd_usuario_convenio_p is null) then
	ds_msg_abort_cons_cart_p := substr(obter_texto_tasy(90085, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	if	(ds_msg_abort_cons_cart_p is not null) then
		goto final;
	end if;
end if;

select	Obter_Valor_Conv_Estab(cd_convenio, cd_estabelecimento_p, 'IE_EXIGE_LETRA_CARTEIRA') 
into	ie_conv_exige_letra_cart_w
from   	convenio 
where 	cd_convenio = cd_convenio_p;

if	(ie_conv_exige_letra_cart_w = 'S') then
	ie_letra_carteira_w := obter_se_letra_carteirinha(cd_usuario_convenio_p);
	if	(ie_letra_carteira_w = 'N') then
		ds_msg_abort_p := substr(obter_texto_tasy(296459, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

if	(ie_novo_registro_p = 'S') then
	if	(nvl(qt_convenio_atend_p,0) > 0) then
		select	nvl(count(*),0) 
		into	qt_conv_atend_w			
		from	atend_categoria_convenio   
		where	nr_atendimento = nr_atendimento_p; 
		if	(qt_conv_atend_w = qt_convenio_atend_p) then
			ds_msg_abort_p :=  substr(obter_texto_dic_objeto(90200, wheb_usuario_pck.get_nr_seq_idioma, 'ITEM='||qt_convenio_atend_p),1,255);
			if	(ds_msg_abort_p is not null) then
				goto final;
			end if;
		end if;
	end if;
end if;

if	(nr_doc_convenio_p is null) then
	obter_guia_conv_atend(nr_atendimento_p, cd_convenio_p, cd_categoria_p,ie_tipo_atendimento_p, cd_estabelecimento_p, ie_tipo_guia_p, nr_doc_conv_retorno_p, null, null);
	nr_doc_convenio_w := nr_doc_conv_retorno_p;
end if;

if	(ie_atualiza_guia_tipo_guia_p = 'S') and
	(ie_alterou_guia_p = 'S') and
	(ie_edicao_registro_p = 'S') then
	obter_guia_conv_atend(nr_atendimento_p, cd_convenio_p, cd_categoria_p, ie_tipo_atendimento_p, cd_estabelecimento_p, ie_tipo_guia_p, nr_doc_conv_retorno_p, null, null);
	nr_doc_convenio_w := nr_doc_conv_retorno_p;
end if;

if	(ie_consiste_guia_senha_p <> 'N') and
	(nr_doc_convenio_w is not null) then
	Consiste_Atend_Guia_Senha(nr_atendimento_p, cd_convenio_p, nr_doc_convenio_w, cd_senha_p, ie_consiste_guia_senha_p, nr_atend_guia_iguais_w);
	if	(nvl(nr_atend_guia_iguais_w,0) > 0) then
		ds_msg_abort_p :=  substr(obter_texto_dic_objeto(90285, wheb_usuario_pck.get_nr_seq_idioma, 'ITEM='||nr_atend_guia_iguais_w),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

Verifica_exige_empresa_conv(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, cd_plano_convenio_p, ie_tipo_atendimento_p, cd_empresa_p, obter_perfil_ativo, ie_obriga_empresa_w);
if	(ie_obriga_empresa_w = 'S') then
	ds_msg_abort_p := substr(obter_texto_tasy(90298, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;

ie_lib_categ_tipo_atend_w := Obter_Tipo_Atend_Lib_Categoria(cd_convenio_p, cd_categoria_p, ie_tipo_atendimento_p, cd_estabelecimento_p, dt_entrada_p);
if	(ie_lib_categ_tipo_atend_w = 'N') then
	ds_msg_abort_p := substr(obter_texto_tasy(90368, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;

consistir_acomodacao_categoria(cd_convenio_p, cd_categoria_p, cd_tipo_acomodacao_p, ds_msg_abort_p);
if	(ds_msg_abort_p is not null) then
	goto final;
end if;


/* ESTA ROTINA ESTA AGUARDANDO DEFINI��O DA OS 741875 , PARA EXECUTAR TODA CONSIST�NCIA DO PARAM 93 NA PROCEDURE MESMO */
if	(ie_consiste_cart_convenio_w = 'S') and
	(cd_usuario_convenio_p is not null) then
	cd_usuario_convenio_w := replace(replace(replace(cd_usuario_convenio_p, '.',''),'-', ''),'/','');
	consistir_cart_conve_eup_js(ie_clinica_p, ie_tipo_atendimento_p, dt_entrada_p, cd_pessoa_fisica_p, cd_convenio_p, cd_categoria_p, cd_usuario_convenio_w, nm_usuario_p, ds_msg_abort_p);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;

if	(cd_plano_convenio_p is not null) then
	select	count(*)
	into	qt_plano_convenio_p
	from	convenio_plano
	where	cd_convenio = cd_convenio_p
        and	ie_situacao   = 'A'
        and	Obter_Plano_Lib_Categoria(cd_convenio_p, cd_categoria_p, cd_plano, cd_estabelecimento_p, dt_entrada_p) = 'S'
        and	cd_plano = cd_plano_convenio_p;
	if	(nvl(qt_plano_convenio_p,0) = 0) then
		ds_msg_abort_p := substr(obter_texto_tasy(90545, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

if	(ie_convenios_liberados_p is not null) then
	ie_convenio_contido_w := obter_se_contido(cd_convenio_p,ie_convenios_liberados_p);
	if	(ie_convenio_contido_w = 'N') then
		ds_msg_abort_p := substr(obter_texto_tasy(90615, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		ie_focar_no_convenio_p := 'S';
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;
if	(ie_convenios_nao_liberados_p is not null) then
	ie_convenio_contido_w := obter_se_contido(cd_convenio_p,ie_convenios_nao_liberados_p);
	if	(ie_convenio_contido_w = 'S') then
		ds_msg_abort_p := substr(obter_texto_tasy(90617, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		ie_focar_no_convenio_p := 'S';
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

begin
	select	nvl(ie_permite_cod_convenio_duplic,'N')
	into	ie_consiste_cod_conv_duplic_w
	from	convenio_estabelecimento                       
	where 	cd_convenio = cd_convenio_p             
	and   	cd_estabelecimento = cd_estabelecimento_p;
exception
when others then
	ie_consiste_cod_conv_duplic_w := 'N';
end;

if	(ie_consiste_cod_conv_duplic_w = 'S') then
	Consistir_Guia_duplic_paciente(cd_convenio_p, nr_doc_convenio_p, nr_atendimento_p, ds_consist_cd_con_dupl_w);
	if	(ds_consist_cd_con_dupl_w = 'N') then
		ds_msg_abort_p := substr(obter_texto_tasy(88526, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

if	(ie_cons_permi_regra_atend_p = 'S') then
	
	if	(cd_medico_resp_p is null) or
		(nvl(cd_convenio_p,0) = 0) then
		ds_msg_abort_p := substr(obter_texto_tasy(90734, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end if;
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
	obter_se_med_lib_conv_setor(cd_medico_resp_p, cd_convenio_p, null, cd_pessoa_fisica_p);
	
end if;

if	(ie_exige_guia_tipo_convenio_p = 'S') and
	(ie_tipo_convenio_p = 2) and
	(nvl(nr_doc_conv_retorno_p,nr_doc_convenio_p) is null) then
	ds_msg_abort_p := substr(obter_texto_tasy(90735, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;

if	(ie_perm_cart_venc_p = 'P') and
	(dt_validade_carteira_p is not null) and
	(trunc(dt_validade_carteira_p) < trunc(dt_entrada_p)) then
	ds_msg_abort_p := substr(obter_texto_tasy(88278, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;

Select	count(*) 
into	qt_medico_w
from	medico 
where	cd_pessoa_fisica = cd_medico_resp_p;
	
ie_resp_atend_w:= obter_se_responsavel_atend(obter_perfil_ativo, cd_medico_resp_p, cd_convenio_p, ie_tipo_atendimento_p);

if	(ie_resp_nao_medico_p = 'S') then
	if	(nvl(qt_medico_w,0) = 0) and
		(ie_resp_atend_w = 'N') then
		ds_msg_abort_p := substr(obter_texto_tasy(97922, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		if	(ds_msg_abort_p is not null) then
			goto final;
		end if;
	end if;
end if;

if	((ie_resp_nao_medico_p = 'N') or
	 ((qt_medico_w > 0) and
	  (ie_resp_nao_medico_p = 'S') and
	  (ie_resp_atend_w = 'N'))) and
	(qt_medico_w = 0) then
	
	ds_msg_abort_p := substr(obter_texto_tasy(93530, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;


<<final>>

commit;

end atend_categ_befpost_abort_eup;
/