create or replace
procedure fin_excluir_conv_retorno_imp(nr_seq_retorno_p	number) is 

begin

delete	from convenio_retorno_imp
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end fin_excluir_conv_retorno_imp;
/