create or replace
procedure fis_gerar_regs_efd_icmsipi(nr_seq_controle_p number) is 

-- VARIABLES
qt_total_w			number;
qt_contador_w			number;
ds_proc_longo_w			varchar2(40);
ds_proc_w			varchar2(255);

-- FIS_EFD_ICMSIPI_CONTROLE
nr_seq_lote_w			fis_efd_icmsipi_controle.nr_seq_lote%type;

-- FIS_EFD_ICMSIPI_REG_LOTE
ds_registro_w			fis_efd_icmsipi_reg_lote.ds_registro%type;

-- CURSOR DOS REGISTROS DE MOVIMENTO
cursor c01 is
	select	ds_registro
	from	fis_efd_icmsipi_reg_lote
	where	nr_seq_lote		= nr_seq_lote_w
	and		ie_gerar		= 'S'
	and		substr(ds_registro,2,3) <> '001'
	and		substr(ds_registro,2,3) <> '990'
	and 	nvl(nr_ordem,0) <> 0
	and     nvl(ie_nivel,0) <= 2	
	order by nr_ordem;
	
begin

select	max(nr_seq_lote)
into	nr_seq_lote_w
from	fis_efd_icmsipi_controle
where	nr_sequencia	= nr_seq_controle_p;

select	count(*)
into	qt_total_w
from	fis_efd_icmsipi_reg_lote
where	nr_seq_lote		= nr_seq_lote_w
and		ie_gerar		= 'S'
and		substr(ds_registro,2,3) <> '001'
and		substr(ds_registro,2,3) <> '990';

open c01;
loop
fetch c01 into	
	ds_registro_w;
exit when c01%notfound;
begin
	qt_contador_w 	:= qt_contador_w + 1;
	
	ds_proc_longo_w	:= substr(wheb_mensagem_pck.get_texto(300326),1,40);
	gravar_processo_longo(ds_proc_longo_w || ': ' || ds_registro_w || ' ('|| qt_contador_w ||'/' ||qt_total_w ||')','FIS_GERAR_DADOS_EFD_ICMSIPI', qt_contador_w);
	
	ds_proc_w := 'begin fis_gerar_reg_' || ds_registro_w || '_icmsipi(:1); end;';
	
	execute immediate ds_proc_w using nr_seq_controle_p;
end;
end loop;
close c01;

-- REGISTRO OBRIGATORIO NO ARQUIVO
fis_gerar_reg_1010_icmsipi(nr_seq_controle_p);

commit;

end fis_gerar_regs_efd_icmsipi;
/