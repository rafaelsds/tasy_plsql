create or replace
procedure pls_enviar_autorizacao_web( 	nr_seq_guia_p			number,
					cd_operadora_exec_p		varchar2,
					ie_glosa_biometria_p		varchar2,
					ie_calcula_franquia_p		varchar2,
					ie_autoriza_glosa_web_p		varchar2,
					ie_gerar_senha_web_p		varchar2,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2,
					ie_transacao_p		out	varchar2,
					cd_cooperativa_p	out	varchar2,
					ie_tipo_intercambio_p	out	varchar2,
					ie_status_p		out	varchar2,
					ie_estagio_p		out	number) is 

ie_estagio_w		number(2);	
ie_status_w		varchar2(2);	
ie_tipo_intercambio_w	varchar2(2);
ie_tipo_trans_w		varchar2(4) := null;
ie_estagio_req_w	Number(10);
nm_cooperativa_w	varchar2(255);
nr_seq_prestador_web_w	pls_guia_plano.nr_seq_prestador_web%type;
nr_seq_uni_exec_w	pls_guia_plano.nr_seq_uni_exec%type;
nr_seq_perfil_web_w	pls_usuario_web.nr_seq_perfil_web%type;
param_28_w		varchar2(255);

begin

begin
	select	nr_seq_prestador_web,
		nr_seq_uni_exec
	into	nr_seq_prestador_web_w,
		nr_seq_uni_exec_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;

	if	(nr_seq_uni_exec_w is null) then
		begin
			select	nr_seq_perfil_web
			into	nr_seq_perfil_web_w
			from	pls_usuario_web
			where	nr_sequencia	= nr_seq_prestador_web_w;
		exception
		when others then
			nr_seq_perfil_web_w := null;
		end;

		param_28_w := pls_obter_param_web(1247, 28, cd_estabelecimento_p, nr_seq_prestador_web_w, nr_seq_perfil_web_w, null, null, 'P', null, null);

		if	(param_28_w is not null) and
			(param_28_w <> 'X') then
			update	pls_guia_plano
			set	nr_seq_uni_exec = to_number(pls_obter_seq_cooperativa(param_28_w))
			where	nr_sequencia = nr_seq_guia_p;
		end if;
	end if;
exception
when others then
	nr_seq_prestador_web_w	:= null;
end;

pls_atualizar_guia_web(nr_seq_guia_p, cd_operadora_exec_p , nm_usuario_p);

pls_consistir_guia_web(nr_seq_guia_p, ie_glosa_biometria_p, ie_calcula_franquia_p,
		       cd_estabelecimento_p, nm_usuario_p); 

pls_dados_intercambio_web(  nr_seq_guia_p, null, cd_estabelecimento_p,
			    ie_tipo_trans_w, cd_cooperativa_p, nm_cooperativa_w,
			    ie_tipo_intercambio_w, ie_estagio_req_w );
			    
if	(nvl(ie_tipo_trans_w, 'X') = 'I') then
	ie_tipo_trans_w := 'I';
end if;
			    
begin
	select	ie_status,
		ie_estagio
	into	ie_status_w,
		ie_estagio_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;
exception
when others then
	ie_status_w := null;
	ie_estagio_w := null;
end;

if	( ie_status_w <> '1' and ie_estagio_w <> 9 and ie_estagio_w > 1) then

	if	(ie_tipo_trans_w is null or ie_tipo_trans_w <> 'I' or ( ie_tipo_trans_w = 'I' and ie_tipo_intercambio_w = 'A' )) then 
	
		pls_liberar_guia( nr_seq_guia_p, 0, 0,
				  'S', cd_estabelecimento_p, nm_usuario_p,
				  ie_autoriza_glosa_web_p, ie_gerar_senha_web_p);
				  
		begin
			select	ie_status,
				ie_estagio
			into	ie_status_w,
				ie_estagio_w
			from	pls_guia_plano
			where	nr_sequencia = nr_seq_guia_p;
		exception
		when others then
			ie_status_w := null;
			ie_estagio_w := null;
		end;
		
	end if;
end if;

ie_status_p := ie_status_w;
ie_estagio_p := ie_estagio_w;
ie_transacao_p := ie_tipo_trans_w;
ie_tipo_intercambio_p := ie_tipo_intercambio_w;

commit;

end pls_enviar_autorizacao_web;
/