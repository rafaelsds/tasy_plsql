create or replace
procedure gerar_lancto_automatico_conta(
				nr_atendimento_p		number,
				nr_cirurgia_p		number,
				cd_material_p		number,
				qt_material_p		number,
				cd_local_estoque_p	number,
				nr_seq_proced_princ_p	number,
				cd_procedimento_p		number,
				ie_origem_proced_p	number,
				qt_procedimento_p		number,
				nm_usuario_p		varchar2,
				cd_convenio_p		number,
				cd_categoria_p		varchar2,
				nr_seq_regra_pepo_p number default Null) is 
			
nr_sequencia_w		number(15);
cd_setor_atendimento_w	number(5);
cd_setor_atendimento_ww	number(5);
dt_entrada_unidade_w	date;
cd_unidade_medida_w	varchar2(30);
nr_seq_atepacu_w		number(10);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
nr_doc_convenio_w	varchar2(20);
ie_tipo_guia_w		varchar2(2);
cd_senha_w		varchar2(20);
ie_consignado_w		varchar2(1);
cd_material_estoque_w	number(06,0);
qt_conv_estoque_consumo_w	number(13,4);
cd_unidade_medida_estoque_w	varchar2(30);
cd_unidade_medida_consumo_w	varchar2(30);
qt_estoque_w			number(13,4);
cd_estab_w			number(10);
dt_entrada_unidade_ww		date;
cd_pessoa_fisica_w		varchar2(10);
dt_inicio_real_w			date;
cd_medico_cirurgiao_w		varchar2(10);
cd_medico_anestesista_w		varchar2(10);
nr_seq_interno_w			number(10,0);
ds_erro_w			varchar2(255);
nr_seq_proc_w			number(10);
ie_classificacao_w			varchar2(1);
cd_tipo_procedimento_w		number(3,0);

ie_acao_excesso_w		varchar2(10);
qt_excedida_w			number(10,3);
qt_procedimento_w		number(10,3);
ds_erro_uso_w			varchar2(255);
ie_regra_uso_w			varchar2(1):= 'N';
nr_seq_excedido_w		number(10,0);
nr_conta_w			number(10,0);
ie_doc_convenio_w		varchar2(3);
cd_convenio_excesso_w		number(5,0);
cd_categoria_excesso_w		varchar2(10);
dt_termino_w			date;
ie_data_fim_w			varchar2(1):= 'N';

ie_lanca_med_exec_conta_w varchar2(1);
cd_medico_req_w		        varchar2(10):= null;
cd_medico_executor_w		varchar2(10):= null;
ds_texto_aux_w			varchar2(255);
cd_motivo_exc_conta_w		parametro_faturamento.cd_motivo_exc_conta%type;


begin

Obter_Param_Usuario(872,332,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_regra_uso_w);
Obter_Param_Usuario(872,478,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_data_fim_w);

nr_seq_atepacu_w := obter_unid_setor_cirurgia(nr_cirurgia_p,'N');

if	(nvl(cd_convenio_p,0) = 0) then
	obter_convenio_execucao(nr_atendimento_p, sysdate, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
else
	cd_convenio_w	:= cd_convenio_p;
	cd_categoria_w	:= cd_categoria_p;
	
	select	max(nr_doc_convenio),
			max(ie_tipo_guia),
			max(cd_senha)
		into	nr_doc_convenio_w,
			ie_tipo_guia_w,
			cd_senha_w
		from 	Atend_Categoria_convenio
		where	nr_atendimento	= nr_atendimento_p
		and	nr_seq_interno	=
				(	select	max(nr_seq_interno)
					from	atend_categoria_convenio
					where	nr_atendimento		= nr_atendimento_p
					and	dt_inicio_vigencia	<= sysdate
					and	cd_convenio		= nvl(cd_convenio_w,cd_categoria)
					and	cd_categoria		= nvl(cd_categoria_w,cd_categoria));
					
		select	Obter_Valor_Conv_Estab(cd_convenio, wheb_usuario_pck.get_cd_estabelecimento, 'IE_DOC_CONVENIO')
		into 	ie_doc_convenio_w
		from 	convenio
		where	cd_convenio	= cd_convenio_w;

		if 	(ie_doc_convenio_w = 'S') then
			nr_doc_convenio_w := nvl(cd_senha_w, nr_doc_convenio_w);
		elsif	(ie_doc_convenio_w in ('N','T')) then
			nr_doc_convenio_w := null;
		end if;

		if	(ie_doc_convenio_w	<> 'A') then
			cd_senha_w		:= null;
		end if;
end if;	

select	cd_estabelecimento
into	cd_estab_w
from  	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;


SELECT NVL(MAX(ie_lanca_med_exec_conta),'S')
INTO  ie_lanca_med_exec_conta_w  
FROM  regra_lancamento_pepo
WHERE nr_sequencia = nr_seq_regra_pepo_p;

select	max(cd_pessoa_fisica),
	max(dt_inicio_real),
	max(cd_medico_cirurgiao),
	max(cd_medico_anestesista),
	max(dt_entrada_unidade),
	max(dt_termino)
into	cd_pessoa_fisica_w,
	dt_inicio_real_w,
	cd_medico_cirurgiao_w,
	cd_medico_anestesista_w,
	dt_entrada_unidade_ww,
	dt_termino_w
from	cirurgia
where	nr_cirurgia	=	nr_cirurgia_p;	

if (nr_seq_proced_princ_p is not null) then

	begin
	select	cd_setor_atendimento,
		nr_seq_interno
	into	cd_setor_atendimento_ww,
		nr_seq_interno_w
	from	atend_paciente_unidade
	where	dt_entrada_unidade	 = dt_entrada_unidade_ww
	and	nr_atendimento = nr_atendimento_p;
	exception
	when others then
		-- N�o foi gerado passagem de setor para esta cirurgia!
		Wheb_mensagem_pck.exibir_mensagem_abort(215674);
	end;

	Consiste_Paciente_Proc(nr_seq_proced_princ_p, nr_atendimento_p,cd_pessoa_fisica_w,cd_procedimento_p,ie_origem_proced_p,ds_erro_w);

	if	(ds_erro_w is not null) then
		-- #@DS_ERRO#@
		Wheb_mensagem_pck.exibir_mensagem_abort(215675,'DS_ERRO=' || ds_erro_w);
	end if;	

	select	procedimento_paciente_seq.nextval
	into	nr_seq_proc_w
	from	dual;
	
	qt_procedimento_w := qt_procedimento_p;
	
	if	(ie_regra_uso_w = 'S') then
	
		obter_regra_qtde_proc_exec(nr_atendimento_p, cd_procedimento_p, ie_origem_proced_p, qt_procedimento_p, dt_inicio_real_w, cd_medico_cirurgiao_w,
	     				   ie_acao_excesso_w, qt_excedida_w, ds_erro_uso_w, cd_convenio_excesso_w, cd_categoria_excesso_w, nr_seq_proced_princ_p, cd_categoria_w, NULL, 0,
					   nr_cirurgia_p, null, cd_setor_atendimento_ww,null);
	 

		if 	(ie_acao_excesso_w = 'E') then
			if 	(qt_excedida_w   > 0) then
				
				if 	((qt_procedimento_p - qt_excedida_w) >= 0) then
				
					if (ie_lanca_med_exec_conta_w = 'S') then
						cd_medico_req_w      := cd_medico_cirurgiao_w;
				        cd_medico_executor_w := cd_medico_cirurgiao_w;						
					end if ;
					
					Inserir_procedimento_paciente(cd_procedimento_p, qt_excedida_w, null, nr_seq_proced_princ_p, ie_origem_proced_p, cd_setor_atendimento_ww,
								nr_atendimento_p, cd_estab_w, nm_usuario_p, null, 'S', cd_medico_cirurgiao_w, nr_seq_interno_w, dt_inicio_real_w,
								cd_convenio_w, cd_categoria_w, nr_seq_excedido_w, cd_medico_req_w, cd_medico_executor_w);
					atualiza_preco_procedimento(nr_seq_excedido_w, cd_convenio_w, nm_usuario_p);

					select 	max(nr_interno_conta)
					into	nr_conta_w
					from 	procedimento_paciente
					where 	nr_sequencia = nr_seq_excedido_w;				
					
					--Exclu�do pela regra de uso da fun��o Cadastro de Conv�nios
					ds_texto_aux_w := substr(wheb_mensagem_pck.get_texto(300556),1,255);
					
					select	max(cd_motivo_exc_conta)
					into	cd_motivo_exc_conta_w
					from	parametro_faturamento
					where	cd_estabelecimento = cd_estab_w;
					
					excluir_matproc_conta(nr_seq_excedido_w, nr_conta_w, nvl(cd_motivo_exc_conta_w, 12), ds_texto_aux_w, 'P', nm_usuario_p);				
				end if;
				
				qt_procedimento_w := qt_procedimento_p - qt_excedida_w;
			end if;
		end if;
	end if;		
	
	if	(qt_procedimento_w > 0) then
	
		if (ie_lanca_med_exec_conta_w = 'S') then
			cd_medico_req_w      := cd_medico_cirurgiao_w;
	        cd_medico_executor_w := cd_medico_cirurgiao_w;						
		end if ;
	
		insert into procedimento_paciente(
			nr_sequencia,
			cd_procedimento,
			ie_origem_proced,
			qt_procedimento,
			cd_pessoa_fisica,
			ie_funcao_medico,
			cd_convenio,
			cd_categoria,
			ie_proc_princ_atend,
			ie_video,
			tx_medico,
			tx_anestesia,
			tx_procedimento,
			ie_valor_informado,
			ie_guia_informada,
			cd_situacao_glosa,
			nm_usuario_original,
			nr_atendimento,
			cd_setor_atendimento,
			dt_entrada_unidade,
			nr_seq_atepacu,
			cd_senha,
			ie_auditoria,
			ie_emite_conta,
			cd_cgc_prestador,
			nr_seq_proc_interno,
			dt_procedimento,
			dt_atualizacao,
			nm_usuario,
			nr_cirurgia,
			cd_medico_executor,
			cd_medico_req,
			dt_conta,
			nr_doc_convenio,
			nr_seq_regra_pepo)
		values(
			nr_seq_proc_w,
			cd_procedimento_p,
			ie_origem_proced_p,
			qt_procedimento_w,
			decode(ie_origem_proced_p,7,null,obter_pessoa_fisica_usuario(nm_usuario_p,'C')),
			'1',
			cd_convenio_w,
			cd_categoria_w,	
			'N',
			'N',
			100,
			100,
			100,
			'N',
			'N',
			0,
			nm_usuario_p,
			nr_atendimento_p,
			cd_setor_atendimento_ww,
			dt_entrada_unidade_ww,
			nr_seq_interno_w,
			cd_senha_w,
			'N',
			'N',
			obter_cgc_estabelecimento(cd_estab_w),
			nr_seq_proced_princ_p,
			dt_inicio_real_w,
			sysdate,
			nm_usuario_p,
			nr_cirurgia_p,
			cd_medico_executor_w,
			cd_medico_req_w,
			decode(ie_data_fim_w, 'S', nvl(dt_termino_w,sysdate), sysdate), 
			nr_doc_convenio_w,
			nr_seq_regra_pepo_p);	
	
		select	max(ie_classificacao),
			max(cd_tipo_procedimento)
		into	ie_classificacao_w,
			cd_tipo_procedimento_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p;	



		if	(ie_classificacao_w in (1,8)) then 		
			atualiza_preco_procedimento(nr_seq_proc_w,cd_convenio_w,nm_usuario_p);
			gerar_taxa_sala_porte(nr_atendimento_p,dt_entrada_unidade_ww,sysdate,cd_procedimento_p,nr_seq_proced_princ_p,nm_usuario_p);
		else
			atualiza_preco_servico(nr_seq_proc_w,nm_usuario_p);
		end if;	

		Gerar_Lancamento_automatico(nr_atendimento_p,null,34,nm_usuario_p,nr_seq_proc_w,null,null,null,null,null);

		gerar_autor_regra(nr_atendimento_p,null,nr_seq_proc_w,null,null,nr_seq_proced_princ_p,'EP',nm_usuario_p,null,null,null,null,null,null,'','','');
	
	end if;	
end if;

if (cd_material_p is not null) then

	if (nr_seq_atepacu_w is not null) then

		select	cd_setor_Atendimento,
			dt_entrada_unidade
		into	cd_setor_atendimento_w,
			dt_entrada_unidade_w
		from	atend_paciente_unidade
		where	nr_seq_interno = nr_seq_atepacu_w;

		select	material_atend_paciente_seq.nextval
		into	nr_sequencia_w
		from	dual;

		select	substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo
		into	cd_unidade_medida_w
		from	material
		where	cd_material = cd_material_p;
		/*
		if	(cd_local_estoque_p is not null) and (cd_local_estoque_p > 0) then
			begin
													
			select	nvl(ie_consignado,'0'),
				cd_material_estoque,
				qt_conv_estoque_consumo,
				cd_unidade_medida_estoque,
				cd_unidade_medida_consumo
			into	ie_consignado_w, 
				cd_material_estoque_w,
				qt_conv_estoque_consumo_w,
				cd_unidade_medida_estoque_w,
				cd_unidade_medida_consumo_w
			from	material
			where	cd_material = cd_material_p;
		
			if	(ie_consignado_w in ('0','2'))  then
				qt_estoque_w	:= obter_saldo_disp_estoque(cd_estab_w,cd_material_estoque_w,cd_local_estoque_p,trunc(sysdate,'mm'));
				
				if	(cd_unidade_medida_consumo_w <> cd_unidade_medida_estoque_w) then
					qt_estoque_w := (qt_estoque_w * qt_conv_estoque_consumo_w);
				end if;
				
				if	(qt_estoque_w < qt_material_p) then
					-- N�o existe Saldo de Estoque:  Material=#@CD_MATERIAL#@ Est=#@QT_ESTOQUE#@ quant=#@QT_MATERIAL#@ Local=#@CD_LOCAL_ESTOQUE#@
					Wheb_mensagem_pck.exibir_mensagem_abort(215678,'CD_MATERIAL=' || cd_material_p ||
										';QT_ESTOQUE=' || qt_estoque_w ||
										';QT_MATERIAL=' || qt_material_p ||
										';CD_LOCAL_ESTOQUE=' || cd_local_estoque_p);
				end if;
			end if;
			
			end;
		end if;
		*/
		
		ds_erro_w := null;
		begin
		insert into material_atend_paciente(	
				nr_sequencia,
				cd_material,
				dt_atendimento,
				cd_senha,
				cd_convenio,
				nr_doc_convenio,
				ie_tipo_guia,
				cd_categoria,
				nr_seq_atepacu,
				cd_setor_atendimento,
				cd_material_exec,
				qt_executada,
				vl_unitario,
				qt_ajuste_conta,
				cd_situacao_glosa,
				ie_valor_informado,
				ie_guia_informada,
				ie_auditoria,
				dt_entrada_unidade,
				qt_material,
				cd_local_estoque,
				dt_Atualizacao,
				nm_usuario,
				nm_usuario_original,
				nr_atendimento,
				cd_unidade_medida,
				cd_acao,
				nr_cirurgia,
				nr_seq_regra_pepo)
		values	(	nr_sequencia_w,
				cd_material_p,
				dt_inicio_real_w,
				cd_senha_w,
				cd_convenio_w,
				nr_doc_convenio_w,
				ie_tipo_guia_w,
				cd_categoria_w,
				nr_seq_atepacu_w,
				cd_setor_atendimento_w,
				cd_material_p,
				qt_material_p,
				0,
				0,
				0,
				'N',
				'N',
				'N',
				dt_entrada_unidade_w,
				qt_material_p,
				cd_local_estoque_p,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				nr_atendimento_p,
				cd_unidade_medida_w,
				'1',
				nr_cirurgia_p,
				nr_seq_regra_pepo_p);
		exception
		when others then
				ds_erro_w 	:= substr(SQLERRM(sqlcode),1,255);
		end;
		
		if	(ds_erro_w is null) then					
			atualiza_preco_material(nr_sequencia_w, nm_usuario_p);
		end if;
							
	end if;

	commit;

end if;

end gerar_lancto_automatico_conta;
/