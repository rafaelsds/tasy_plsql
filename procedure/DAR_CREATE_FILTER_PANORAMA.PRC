create or replace procedure DAR_CREATE_FILTER_PANORAMA (nr_seq_dar_app_p number,
														nr_seq_dar_dashboard_p number,
														nr_atendimento_p varchar2,
													    cd_pessoa_fisica_p number,
													    cd_setor_atendimento_p number,
														nm_usuario_p varchar2,
													    cd_estabelecimento_p number
													) as

cursor cFiltros is
select a.nr_seq_app, c.nr_sequencia cd_field , c.ds_campo 
  from dar_app_datamodels a,
       dar_tables_control b,
       dar_tab_control_fields c
 where nvl(a.nr_seq_ligacao,a.nr_seq_table_control) = b.nr_sequencia
   and a.nr_seq_table_control = c.nr_seq_table_control
   and a.nr_seq_app = nr_seq_dar_app_p;			

begin
	delete from dar_dashboard_filter where nr_seq_dashboard = nr_seq_dar_dashboard_p;
	for r1 in cFiltros loop
		if (upper(r1.ds_campo) = 'NR_ATENDIMENTO' and nr_atendimento_p is not null) then
			insert into dar_dashboard_filter (
					nr_sequencia,         
					cd_estabelecimento,   
					dt_atualizacao,       
					nm_usuario,           
					dt_atualizacao_nrec,  
					nm_usuario_nrec,      
					cd_exp_campo,         
					nm_campo,             
					ie_operacao,          
					ds_resultado,         
					nr_seq_dashboard) 
			values (dar_dashboard_filter_seq.nextval,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					r1.cd_field,
					7,
					nr_atendimento_p,
					nr_seq_dar_dashboard_p); 
		
		elsif (upper(r1.ds_campo) = 'CD_PESSOA_FISICA' and cd_pessoa_fisica_p  is not null) then
			insert into dar_dashboard_filter (
					nr_sequencia,         
					cd_estabelecimento,   
					dt_atualizacao,       
					nm_usuario,           
					dt_atualizacao_nrec,  
					nm_usuario_nrec,      
					cd_exp_campo,         
					nm_campo,             
					ie_operacao,          
					ds_resultado,         
					nr_seq_dashboard) 
			values (dar_dashboard_filter_seq.nextval,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					r1.cd_field,
					7,
					cd_pessoa_fisica_p,
					nr_seq_dar_dashboard_p); 
		
		elsif (upper(r1.ds_campo) = 'CD_SETOR_ATENDIMENTO' and cd_setor_atendimento_p is not null) then
						insert into dar_dashboard_filter (
					nr_sequencia,         
					cd_estabelecimento,   
					dt_atualizacao,       
					nm_usuario,           
					dt_atualizacao_nrec,  
					nm_usuario_nrec,      
					cd_exp_campo,         
					nm_campo,             
					ie_operacao,          
					ds_resultado,         
					nr_seq_dashboard) 
			values (dar_dashboard_filter_seq.nextval,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					r1.cd_field,
					7,
					cd_setor_atendimento_p,
					nr_seq_dar_dashboard_p); 
		end if;
	end loop;
commit;
end DAR_CREATE_FILTER_PANORAMA;	
/
