create or replace
procedure Gerar_equipe_atend
		(nr_atendimento_p		number,
		nr_prescricao_p		number,
		nm_usuario_p		varchar2) is
		
nr_seq_equipe_w	 	number(10);
ie_tipo_profissional_w	varchar2(15);
nr_sequencia_w		number(10);
cd_setor_atendimento_w	number(5);
nr_seq_proc_interno_w	number(10);

Cursor C01 is --Procedimentos
	select	a.nr_seq_proc_interno,
		b.cd_setor_atendimento
	from	prescr_procedimento a,
		prescr_medica b
	where	a.nr_prescricao = b.nr_prescricao
	and	a.nr_prescricao = nr_prescricao_p
	and	nvl(a.ie_suspenso,'N') = 'N';
	

Cursor C02 is --Profissionais do procedimento
	select	a.ie_profissional
	from	proc_interno_prof a,
		proc_interno b		
	where	a.nr_seq_proc_interno = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_proc_interno_w;

Cursor C03 is --Regras
	select	a.nr_seq_equipe		
	from	sp_regra_aloc_prof a
	where	nvl(a.cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and	a.ie_tipo_profissional = ie_tipo_profissional_w
	and	not exists (	select	1
				from	sp_equipe_atend b
				where	b.nr_seq_equipe = a.nr_seq_equipe
				and	b.nr_atendimento = nr_atendimento_p)
	order by a.nr_seq_prior;	
		
begin
open C01;
loop
fetch C01 into	
	nr_seq_proc_interno_w,
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		ie_tipo_profissional_w;
	exit when C02%notfound;
		begin
		
		open C03;
		loop
		fetch C03 into	
			nr_seq_equipe_w;
		exit when C03%notfound;
			begin
			
		
			select 	sp_equipe_atend_seq.nextval
			into 	nr_sequencia_w
			from 	dual;
		
			
			insert into	sp_equipe_atend(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_equipe,
							nr_atendimento,
							ie_forma_aloc_prof	
							)
				values			(nr_sequencia_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_equipe_w,
							nr_atendimento_p,
							'EF'		
							);
			
			end;
		end loop;
		close C03;
		
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

commit;

end Gerar_equipe_atend;
/