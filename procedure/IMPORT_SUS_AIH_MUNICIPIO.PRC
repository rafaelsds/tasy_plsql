create or replace
procedure import_sus_aih_municipio(cd_municipio_ibge_p	varchar2,
			ds_municipio_p	varchar2,
			ds_unidade_federacao_p	varchar2,
			ie_novo_p	varchar2,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar dados AIH
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
if	(upper(ie_novo_p) = 'S') then
	if	(cd_municipio_ibge_p is not null) and
		(ds_municipio_p is not null) and
		(ds_unidade_federacao_p is not null) then 
		insert	into sus_municipio 
					(cd_municipio_ibge, 
					ds_municipio, 
					dt_atualizacao, 
					nm_usuario, 
					dt_atualizacao_nrec, 
					nm_usuario_nrec, 
					ds_unidade_federacao,
					IE_IMPORTACAO_SUS) 
		values		(cd_municipio_ibge_p, 
					ds_municipio_p, 
					sysdate, 
					nm_usuario_p, 
					sysdate, 
					nm_usuario_p, 
					ds_unidade_federacao_p,
					'S');
	
	else
		update	sus_municipio 
		set	ie_importacao_sus = 'S' 
		where	cd_municipio_ibge = cd_municipio_ibge_p;
	end if;
end if;
commit;
end import_sus_aih_municipio;
/