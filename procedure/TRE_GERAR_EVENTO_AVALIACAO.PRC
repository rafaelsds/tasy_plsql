create or replace
procedure tre_gerar_evento_avaliacao( 	nr_seq_avaliacao_p	number,
					nr_seq_evento_p		number,
					nm_usuario_p		Varchar2) is 

nr_seq_inscrito_w	number(10);	
nr_sequencia_w		number(10);				
nr_seq_aval_w		number(10);	
nr_seq_item_aval_w	number(10);
nr_sequencia_item_w	number(10);

	
cursor c01 is
select	a.nr_sequencia
from	tre_inscrito a
where	a.nr_seq_evento = nr_seq_evento_p
and	not exists (	select	1 
			from	tre_aval_eve_inscrito b
			where	b.nr_seq_inscrito = a.nr_sequencia);

cursor c02 is
select	nr_sequencia
from	tre_aval_item
where	nr_seq_aval = nr_seq_aval_w
order by nr_seq_apres;			
					
begin
select	max(nr_seq_aval)
into	nr_seq_aval_w
from	tre_modulo_aval
where	nr_sequencia = nr_seq_avaliacao_p;


open C01;
loop
fetch C01 into	
	nr_seq_inscrito_w;
exit when C01%notfound;
	begin
	
	select	tre_aval_eve_inscrito_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into tre_aval_eve_inscrito(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_inscrito,
			nr_seq_aval_mod)
		values(nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_inscrito_w,
			nr_seq_avaliacao_p);
			
			
	open C02;
	loop
	fetch C02 into	
		nr_seq_item_aval_w;
	exit when C02%notfound;
		begin
		
		select	tre_aval_inscrito_item_seq.nextval
		into	nr_sequencia_item_w
		from	dual;

		insert into tre_aval_inscrito_item(
				nr_sequencia,
				nr_seq_aval_inscrito,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_item_aval)
			values(nr_sequencia_item_w,
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,				
				nr_seq_item_aval_w);		
		
		end;
	end loop;
	close C02;		
	
	end;
end loop;
close C01;




commit;

end tre_gerar_evento_avaliacao;
/