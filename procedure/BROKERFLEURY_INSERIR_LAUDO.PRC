create or replace
procedure  brokerfleury_inserir_laudo(	nr_ficha_p			number,
								nr_prescricao_p		number,
								nr_seq_prescr_p		number,
								cd_unidade_p	varchar2,
								nr_seq_laudo_p		out number) is 
					
nr_prescricao_w		number(14);
nr_seq_prescricao_w	number(10);

nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
nr_seq_imagem_w		number(10);
nr_laudo_w		number(10);
nr_atendimento_w	number(10);
cd_medico_resp_w	varchar2(10);
dt_entrada_unidade_w	date;
ds_titulo_laudo_w	varchar2(255);
nr_seq_propaci_w	number(15);
dt_procedimento_w	date;
nr_seq_laudo_w		number(10);
nr_seq_proc_interno_w	number(15);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_procedimento_w	number(15);
cd_setor_atendimento_w	number(10);
dt_prev_execucao_w	date;
cd_medico_exec_w	varchar2(10);
ie_lado_w		varchar2(15);
ds_laudo_copia_w	long;
nr_seq_laudo_ant_w	number(10);
nr_seq_laudo_atual_w	number(10);
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;
ie_agrupa_w				varchar2(1);
ie_existe_param_maq_w	varchar2(1);
ie_fim_conta_w			atendimento_paciente.ie_fim_conta%type;

begin

if (nvl(nr_prescricao_p,0) = 0) then
	/*select	Obter_Prescr_Controle(nr_ficha_p)
	into	nr_prescricao_w
	from	dual;*/
		
	select	fleury_obter_dados_unidade(cd_unidade_p, 'E'),
			fleury_obter_dados_unidade(cd_unidade_p, 'AF')
	into	cd_estabelecimento_w,
			ie_agrupa_w
	from	dual;
	
	if (ie_agrupa_w <> 'N') then
		select	decode(count(*),0,'N','S')
		into	ie_existe_param_maq_w
		from	lab_param_maquina a
		where	a.cd_estabelecimento = cd_estabelecimento_w;
	
		if	(ie_existe_param_maq_w = 'S') then
			select	max(a.nr_prescricao)
			into	nr_prescricao_w
			from	(
					select	a.nr_prescricao,
							substr(lab_obter_parametro(cd_estabelecimento_w, a.nr_prescricao, null, 'UF'),1,255) cd_unidade_fleury
					from	prescr_procedimento a
					where	a.nr_controle_ext = nr_ficha_p
					) a
			where	a.cd_unidade_fleury =  cd_unidade_p;
		else
			select	max(a.nr_prescricao)
			into	nr_prescricao_w
			from	prescr_procedimento a,
					prescr_medica b
			where	a.nr_prescricao = b.nr_prescricao
			and		a.nr_controle_ext = nr_ficha_p
			and		b.cd_estabelecimento = cd_estabelecimento_w;
		end if;
	else
		select	max(a.nr_prescricao)
		into	nr_prescricao_w
		from	prescr_medica a
		where	a.nr_controle = nr_ficha_p
		and		a.cd_estabelecimento = cd_estabelecimento_w;		
	end if;	
else
	nr_prescricao_w	:= nr_prescricao_p;
end if;

nr_seq_prescricao_w	:= nr_seq_prescr_p;

if	(nr_prescricao_w is not null) then
   
	select	max(nr_seq_proc_interno),
		max(cd_procedimento),
		max(ie_origem_proced),
		max(qt_procedimento),
		max(cd_setor_atendimento),
		max(dt_prev_execucao),
		max(cd_medico_exec),
		max(nvl(ie_lado,'A'))
	into	nr_seq_proc_interno_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		cd_setor_atendimento_w,
		dt_prev_execucao_w,
		cd_medico_exec_w,
		ie_lado_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_w
	and	a.nr_sequencia = nr_seq_prescricao_w;
		
	select	max(a.cd_medico)
	into	cd_medico_resp_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_w;
		
	select	nvl(max(a.nr_laudo),0)+1
	into	nr_laudo_w
	from	laudo_paciente a
	where	a.nr_prescricao = nr_prescricao_w;
	
	/*Executando a prescri��o*/
	select	nvl(max(nr_sequencia),0),
		max(nr_atendimento),
		max(dt_entrada_unidade),
		max(dt_procedimento)		
	into	nr_seq_propaci_w,
		nr_atendimento_w,
		dt_entrada_unidade_w,
		dt_procedimento_w
	from	procedimento_paciente
	where	nr_prescricao		= nr_prescricao_w
	and	nr_sequencia_prescricao	= nr_seq_prescricao_w;
	
	if	(nr_seq_propaci_w = 0) then
		begin
		
		Gerar_Proc_Pac_item_Prescr_up(	nr_prescricao_w, 
										nr_seq_prescricao_w, 
										null, 
										null,
										nr_seq_proc_interno_w,
										cd_procedimento_w, 
										ie_origem_proced_w,
										qt_procedimento_w, 
										cd_setor_atendimento_w,
										9, 
										dt_prev_execucao_w,
										'TasyFleuryWS', 
										cd_medico_exec_w, 
										null,
										ie_lado_w, 
										null);
		
		/*Gerar_Proc_Pac_item_Prescr(	nr_prescricao_w, 
						nr_seq_prescricao_w, 
						null, 
						null,
						nr_seq_proc_interno_w,
						cd_procedimento_w, 
						ie_origem_proced_w,
						qt_procedimento_w, 
						cd_setor_atendimento_w,
						9, 
						dt_prev_execucao_w,
						'TasyFleuryWS', 
						cd_medico_exec_w, 
						null,
						ie_lado_w, 
						null);*/
						
		select	max(nr_sequencia),
			max(nr_atendimento),
			max(dt_entrada_unidade),
			max(dt_procedimento)		
		into	nr_seq_propaci_w,
			nr_atendimento_w,
			dt_entrada_unidade_w,
			dt_procedimento_w
		from	procedimento_paciente
		where	nr_prescricao		= nr_prescricao_w
		and	nr_sequencia_prescricao	= nr_seq_prescricao_w;
		
		end;
	end if;

	select  MAX(nr_sequencia)
	into	nr_sequencia_ww
	from 	laudo_paciente
	where   nr_prescricao = nr_prescricao_w
	and	nr_seq_prescricao = nr_seq_prescricao_w;
	
	/* *** Cancela o laudo atual do procedimento (Tratamento para retifica��o do laudo pela TasyFleuryWS. Quando j� existir um laudo para o item, este � cancelado - OS 404183). *** */
	if	(nvl(nr_sequencia_ww,0)<>0) then
		begin
			--cancelar_laudo_paciente(nr_sequencia_ww,'C','TasyFleuryWS','');
			
			UPDATE 	laudo_paciente
			SET 	dt_cancelamento = SYSDATE,
					nm_usuario_cancel = 'TasyFleuryWS',
					nr_seq_motivo_canc = ''
			WHERE 	nr_sequencia  = nr_sequencia_ww;

			SELECT	MAX(ie_fim_conta)
			INTO	ie_fim_conta_w
			FROM	atendimento_paciente
			WHERE 	nr_atendimento = nr_atendimento_w;

			IF	(ie_fim_conta_w = 'F') THEN
				--O atendimento est� fechado, os procedimento n�o poder�o ser executados
				Wheb_mensagem_pck.exibir_mensagem_abort(191437);
			END IF;
		end;
	end if;
	
			
	select	laudo_paciente_seq.nextval
	into	nr_seq_laudo_w
	from	dual;

	
	select	max(substr(obter_desc_prescr_proc_laudo(cd_procedimento, ie_origem_proced, nr_seq_proc_interno, ie_lado,  nr_seq_propaci_w),1,255))
	into	ds_titulo_laudo_w		
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_w
	and		a.nr_sequencia	= nr_seq_prescricao_w;
				
	insert into laudo_paciente(
			nr_sequencia,
			nr_atendimento,
			dt_entrada_unidade,
			nr_laudo,
			nm_usuario,
			dt_atualizacao,
			cd_medico_resp,
			ds_titulo_laudo,
			dt_laudo,
			nr_prescricao,
			nr_seq_proc,
			nr_seq_prescricao,
			dt_liberacao,
			qt_imagem,
			ie_status_laudo,
			dt_exame
			)
		values(	nr_seq_laudo_w,
			nr_atendimento_w,
			dt_entrada_unidade_w,
			nr_laudo_w,
			'TasyFleuryWS',
			sysdate,
			cd_medico_resp_w,
			ds_titulo_laudo_w,
			sysdate,
			nr_prescricao_w,			
			nr_seq_propaci_w,
			nr_seq_prescricao_w,
			sysdate,
			0,
			'LL',
			dt_procedimento_w
			);
					
	update	procedimento_paciente
	set	nr_laudo	= nr_seq_laudo_w
	where	nr_sequencia	= nr_seq_propaci_w;
	
	nr_seq_laudo_p	:= nr_seq_laudo_w;
	
end if;

end brokerfleury_inserir_laudo;
/