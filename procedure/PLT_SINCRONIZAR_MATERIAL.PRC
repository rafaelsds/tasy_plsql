create or replace
procedure plt_sincronizar_material (
					nm_usuario_p			varchar2,
					nr_atendimento_p		number,
					dt_validade_limite_p		date,					
					dt_horario_p			date,
					nr_horario_p			integer,
					ie_prescr_usuario_p		varchar2) is				
				
ds_sep_bv_w			varchar2(50);
nr_prescricao_w			number(14,0);
nr_seq_material_w		number(6,0);
nr_seq_horario_w		number(10,0);
ie_status_horario_w		varchar2(1);
cd_material_w			number(6,0);
ds_material_w			varchar2(255);
ie_acm_sn_w			varchar2(1);
cd_intervalo_w			varchar2(7);
qt_dose_w			number(15,3);
ds_prescricao_w			varchar2(100);
ie_status_w			varchar2(1);
ds_comando_update_w		varchar2(4000);
ie_lib_pend_rep_w		varchar2(1);

cursor c01 is	
select	a.nr_prescricao,
	c.nr_seq_material,
	c.nr_sequencia,
	SUBSTR(PLT_obter_status_hor_material(c.dt_fim_horario,c.dt_suspensao,c.dt_lib_horario),1,1),
	c.cd_material,
	y.ds_material,
	obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) ie_acm_sn,		
	x.cd_intervalo,
	x.qt_dose,
	substr(adep_obter_um_dosagem_prescr(a.nr_prescricao,c.nr_seq_material,x.ie_acm,x.ie_se_necessario),1,100) ds_prescricao,
	nvl(x.ie_suspenso,'N') ie_suspenso,
	substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1)
from	material y,
	prescr_material x,
	prescr_mat_hor c,
	prescr_medica a
where	y.cd_material = x.cd_material
and	x.nr_prescricao = c.nr_prescricao
and	x.nr_sequencia = c.nr_seq_material	
and	x.nr_prescricao = a.nr_prescricao
and	c.nr_prescricao = a.nr_prescricao	
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	x.ie_agrupador = 2
and	x.nr_seq_kit is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'S'
and	c.ie_agrupador = 2
and	nvl(c.ie_dose_especial,'N') = 'N'
and	((nvl(c.ie_horario_especial,'N') = 'N') or (c.dt_fim_horario is not null))
and	c.dt_horario = dt_horario_p	 
and	(((a.dt_liberacao_medico is not null) and
	  (ie_prescr_usuario_p = 'N')) or
	(a.nm_usuario_original = nm_usuario_p))
group by
	a.nr_prescricao,
	c.nr_seq_material,
	c.nr_sequencia,
	c.dt_fim_horario,
	c.dt_suspensao,
	c.cd_material,
	y.ds_material,
	x.ie_acm,
	x.ie_se_necessario,		
	x.cd_intervalo,
	x.qt_dose,
	x.ie_suspenso,
	c.dt_lib_horario,
	a.dt_liberacao_medico,
	a.dt_liberacao,
	a.dt_liberacao_farmacia
order by
	c.dt_suspensao;
	
begin
ds_sep_bv_w	:= obter_separador_bv;

open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_material_w,
		nr_seq_horario_w,
		ie_status_horario_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,		
		cd_intervalo_w,
		qt_dose_w,
		ds_prescricao_w,
		ie_status_w,
		ie_lib_pend_rep_w;
exit when c01%notfound;
	begin
	ds_comando_update_w :=	' update w_rep_t ' ||
				' set hora' || to_char(nr_horario_p) || ' = :vl_hora, ' ||
				' nr_prescricoes = adep_juntar_prescricao(nr_prescricoes,:nr_prescricao) ' ||
				' where nm_usuario = :nm_usuario ' ||
				' and ie_tipo_item = :ie_tipo ' ||
				' and nvl(nr_prescricao,nvl(:nr_prescricao,0)) = nvl(:nr_prescricao,0) ' ||
				' and nvl(nr_seq_item,nvl(:nr_seq_item,0)) = nvl(:nr_seq_item,0) ' ||					
				' and cd_item = :cd_item ' ||
				' and ie_pendente_liberacao = :ie_pendente_liberacao ' ||				
				' and nvl(cd_intervalo,0) = nvl(:cd_intervalo,0) ' ||
				' and nvl(qt_item,0) = nvl(:qt_item,0) ' ||
				' and ((ds_prescricao = :ds_prescricao) or (ds_prescricao is null)) ';
				
	exec_sql_dinamico_bv('PLT', ds_comando_update_w,	'vl_hora=S' || to_char(nr_seq_horario_w) || 'H' || ie_status_horario_w || ds_sep_bv_w ||
								'nr_prescricao=' || to_char(nr_prescricao_w) || ds_sep_bv_w || 
								'nm_usuario=' || nm_usuario_p || ds_sep_bv_w || 
								'ie_tipo=MAT' || ds_sep_bv_w ||
								'nr_seq_item='|| to_char(nr_seq_material_w) || ds_sep_bv_w ||
								'cd_item=' || to_char(cd_material_w) || ds_sep_bv_w ||
								'ie_pendente_liberacao=' || ie_lib_pend_rep_w || ds_sep_bv_w ||								
								'cd_intervalo=' || cd_intervalo_w  || ds_sep_bv_w ||
								'qt_item=' || to_char(qt_dose_w) || ds_sep_bv_w ||
								'ds_prescricao=' || ds_prescricao_w || ds_sep_bv_w );
	end;
end loop;
close c01;
end plt_sincronizar_material;
/
