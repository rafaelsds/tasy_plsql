create or replace 
procedure atualiza_exame_lab_result_item (
		nr_sequencia_p		number,
		nr_seq_resultado_p	number,
		ds_observacao_p		varchar2,
		dt_atualizacao_p	date,
		nm_usuario_p		varchar2,
		ds_resultado_p		varchar2,
		pr_resultado_p		number,
		qt_resultado_p		number) is
		
begin

if	(nr_sequencia_p is not null) and
	(nr_seq_resultado_p is not null) then
	begin
	update	exame_lab_result_item
	set	qt_resultado 	= qt_resultado_p,
		pr_resultado 	= pr_resultado_p,
		ds_resultado 	= ds_resultado_p,
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao 	= dt_atualizacao_p,
		ds_observacao 	= ds_observacao_p
	where 	nr_seq_resultado = nr_seq_resultado_p
	and	nr_sequencia 	= nr_sequencia_p;
	end;
end if;

end atualiza_exame_lab_result_item;
/