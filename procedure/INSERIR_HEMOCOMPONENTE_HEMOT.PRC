create or replace
procedure inserir_hemocomponente_hemot(
		nr_seq_derivado_p	number,
		dt_producao_p	date,
		cd_pf_realizou_p	varchar2,
		dt_vencimento_p	date,
		nr_sangue_p	varchar2,
		nr_seq_emp_ent_p	number,
		ie_tipo_sangue_p	varchar2,
		ie_fator_rh_p	varchar2,
		qt_volume_p	number,
		ie_irradiado_p	varchar2,
		ie_filtrado_p	varchar2,
		ie_lavado_p	varchar2,
		ds_observacao_p	varchar2,
		nm_doador_p	varchar2,
		nr_sec_saude_p	varchar2,
		nm_usuario_p	varchar2) is 

begin

insert	into san_producao
	(nr_sequencia,
	nr_seq_derivado,
	dt_producao,
	cd_pf_realizou,
	dt_atualizacao,
	nm_usuario,
	dt_vencimento,
	nr_sangue,
	nr_seq_emp_ent,
	ie_tipo_sangue,
	ie_fator_rh,
	qt_volume,
	ie_irradiado,
	ie_filtrado,
	ie_lavado,
	ie_aliquotado,
	ds_observacao,
	dt_liberacao,
	nm_doador,
	nr_sec_saude,
	cd_estabelecimento)
values	(san_producao_seq.NextVal,
	nr_seq_derivado_p,
	dt_producao_p,
	cd_pf_realizou_p,
	sysdate,
	nm_usuario_p,
	dt_vencimento_p,
	nr_sangue_p,
	nr_seq_emp_ent_p,
	ie_tipo_sangue_p,
	ie_fator_rh_p,
	qt_volume_p,
	ie_irradiado_p,
	ie_filtrado_p,
	ie_lavado_p,
	'N',
	ds_observacao_p,
	sysdate,
	nm_doador_p,
	nr_sec_saude_p,
	wheb_usuario_pck.get_cd_estabelecimento);

commit;

end inserir_hemocomponente_hemot;
/