Create or Replace
procedure Ajustar_Protocolo_Mat_Medic(
                         	cd_protocolo_p     	NUMBER,
                         	nr_sequencia_p     	NUMBER,
				nr_seq_material_p	Number) IS


nr_seq_material_w	Number(6);	
cd_intervalo_w		Varchar2(7);
ie_acm_w		Varchar2(1);		
ie_se_necessario_w	Varchar2(1);		


cursor C01 is
Select	nvl(nr_seq_material,0)
from	protocolo_medic_material
where	cd_protocolo	= cd_protocolo_p
and	nr_sequencia	= nr_sequencia_p
and	nr_seq_diluicao	= nr_seq_material_p
and	cd_material is not null;

begin


begin
Select	cd_intervalo,
	nvl(ie_se_necessario,'N'),
	nvl(ie_acm,'N')
into	cd_intervalo_w,
	ie_se_necessario_w,
	ie_acm_w
from	protocolo_medic_material
where	cd_protocolo	= cd_protocolo_p
and	nr_sequencia	= nr_sequencia_p
and	nr_seq_material	= nr_seq_material_p;


exception
	when others then
	null;
	
end;


open C01;
loop
	fetch C01 into	
		nr_seq_material_w;
	exit when C01%notfound;	
	
	Update	protocolo_medic_material
	set	ie_acm		= ie_acm_w,
		ie_se_necessario = ie_se_necessario_w
	where	cd_protocolo	= cd_protocolo_p
	and	nr_sequencia	= nr_sequencia_p
	and	nr_seq_diluicao	= nr_seq_material_p
	and	ie_agrupador in (3,7,9);

end loop;
close C01;


commit;

end Ajustar_Protocolo_Mat_Medic;
/
