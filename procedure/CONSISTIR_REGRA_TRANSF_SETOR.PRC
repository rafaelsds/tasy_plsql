create or replace
procedure consistir_regra_transf_setor( ie_tipo_mov_p		Varchar2,
					cd_setor_origem_p	number,
					cd_setor_destino_p	number,
					nr_atendimento_p	number,
					ds_retorno_p		out varchar2) is


cd_estabelecimento_w	number(10) 	:= wheb_usuario_pck.get_cd_estabelecimento;
cd_classif_setor_dest_w number(10) 	:= obter_classif_setor(cd_setor_destino_p);	
cd_classif_setor_orig_w	number(10) 	:= Obter_classif_setor_atend(nr_atendimento_p);
ie_tipo_movimentacao_w	varchar2(1);
nr_sequencia_w		number(10);
ds_setor_origem_w	varchar2(255)	:= obter_nome_setor(cd_setor_origem_p);
ds_setor_destino_w	varchar2(255)	:= obter_nome_setor(cd_setor_destino_p);
ie_tipo_atend_origem_w	number(3);
ds_mensagem_bloqueio_w				regra_transf_setor.ds_mensagem_bloqueio%type;
	
Cursor C01 is

select  nr_sequencia,
		ie_tipo_movimentacao,
		nvl(ds_mensagem_bloqueio,' ')
from	regra_transf_setor
where	nvl(cd_setor_destino, cd_setor_destino_p) 			= cd_setor_destino_p
and	nvl(cd_setor_origem, cd_setor_origem_p)  			= cd_setor_origem_p
and	nvl(cd_classif_setor_destino, cd_classif_setor_dest_w)		= cd_classif_setor_dest_w
and	nvl(cd_classif_setor_origem, cd_classif_setor_orig_w) 		= cd_classif_setor_orig_w
and	nvl(cd_estabelecimento,cd_estabelecimento_w) 			= cd_estabelecimento_w
and	nvl(ie_tipo_atend_origem,ie_tipo_atend_origem_w)		= ie_tipo_atend_origem_w
and	((ie_tipo_movimentacao						= ie_tipo_mov_p) or (ie_tipo_movimentacao = 'A'))
and	nvl(ie_permite,'S') = 'N'
and	ie_situacao = 'A'
and nvl(cd_perfil_ativo,obter_perfil_ativo) = obter_perfil_ativo
order	by cd_setor_destino,
	   cd_setor_origem,
	   cd_classif_setor_destino,
	   cd_classif_setor_origem;

begin

select	max(ie_tipo_atendimento)
into	ie_tipo_atend_origem_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ie_tipo_movimentacao_w,
	ds_mensagem_bloqueio_w;
exit when C01%notfound;
	begin
	nr_sequencia_w := nr_sequencia_w;	
	end;
end loop;
close C01;

if	(nr_sequencia_w > 0) then

	if	((ie_tipo_movimentacao_w = 'P') or
		(ie_tipo_movimentacao_w = 'A') and (ie_tipo_mov_p = 'P')) then
			ds_retorno_p 	:= 	wheb_mensagem_pck.get_texto(403143, 'DS_SETOR_ORIGEM_W=' || ds_setor_origem_w || ';DS_SETOR_DESTINO_W=' || ds_setor_destino_w
								|| ';DS_MOTIVO_BLOQ_W=' || ds_mensagem_bloqueio_w);			
								
			
	elsif	((ie_tipo_movimentacao_w = 'T') or
		(ie_tipo_movimentacao_w = 'A') and (ie_tipo_mov_p = 'T')) then
		ds_retorno_p	:=  wheb_mensagem_pck.get_texto(403171, 'DS_SETOR_ORIGEM_W=' || ds_setor_origem_w || ';DS_SETOR_DESTINO_W=' || ds_setor_destino_w
							|| ';DS_MOTIVO_BLOQ_W=' || ds_mensagem_bloqueio_w);								
	end if;
end if;

commit;

end consistir_regra_transf_setor;
/
