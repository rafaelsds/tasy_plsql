create or replace procedure
qua_inativar_revisao_documento (nr_seq_revisao_p	number,
				nm_usuario_p		varchar2)
is

nr_seq_doc_w	number(10);

begin
	if	(obter_se_base_corp = 'S'
		or	obter_se_base_wheb = 'S') then
		begin
			update	qua_doc_revisao
			set	ie_situacao = 'I',
				nm_usuario_inativacao = nm_usuario_p,
				dt_inativacao = sysdate
			where	nr_sequencia = nr_seq_revisao_p
			and	nvl(ie_situacao, 'A') = 'A';
			
			select	nr_seq_doc
			into	nr_seq_doc_w
			from	qua_doc_revisao
			where	nr_sequencia = nr_seq_revisao_p
			and	nvl(ie_situacao, 'A') = 'A';
			
			if(nr_seq_doc_w is not null) then
				qua_atualizar_status_doc(nr_seq_doc_w, 'D', nm_usuario_p);
			end if;
			commit;
		end;
	end if;
end qua_inativar_revisao_documento;
/
