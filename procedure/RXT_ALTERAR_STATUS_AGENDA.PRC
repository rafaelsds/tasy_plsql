create or replace
procedure rxt_alterar_status_agenda	(nr_seq_agenda_p	number,
				ie_status_p	varchar2,
				nm_usuario_p	varchar2) is

ie_status_w	varchar2(3);
dt_confirmacao_w	date;
nr_seq_equip_w	number(10,0);
dt_agenda_w	date;
qt_hor_cancel_w	number(10,0);
nr_atendimento_w	number(10,0);
integracao_tasy_w varchar2(1) default 'N';
retorno_integracao_w	clob;

begin

if	(nr_seq_agenda_p is not null) and
	(ie_status_p is not null) and
	(nm_usuario_p is not null) then
	begin

	select	nvl(max(ie_status_agenda),ie_status_p),
		max(dt_confirmacao),
		max(nr_seq_equipamento),
		max(dt_agenda)
	into	ie_status_w,
		dt_confirmacao_w,
		nr_seq_equip_w,
		dt_agenda_w
	from	rxt_agenda
	where	nr_sequencia = nr_seq_agenda_p;

	if	(ie_status_p = 'C') then
		/*
		update	rxt_agenda
		set	ie_status_agenda	= decode(ie_status_w, 'C', 'M', ie_status_p),
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_agenda_p;	
		*/		
		
		if	(ie_status_w in ('M','P','V')) then /* Jerusa OS 120512 - Inclui o status = 'P', pois quando a procedure: RXT_GERAR_PLANEJAMENTO � executado ao gerar agenda do tratamento, o status � 'P' de planejamento, assim nunca entrava nesta rotina quando cancelavamos os dias agendados*/
			select	nvl(max(PKG_DATE_UTILS.extract_field('SECOND', dt_agenda)),0)+1
			into	qt_hor_cancel_w
			from	rxt_agenda
			where	nr_seq_equipamento = nr_seq_equip_w
			and	to_date(to_char(dt_agenda,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
			and	ie_status_agenda = 'C';		
		
			update	rxt_agenda
			set	ie_status_agenda	= 'C',
				dt_agenda		= dt_agenda + qt_hor_cancel_w / 86400,
				nm_usuario		= nm_usuario_p,
				dt_cancelamento		= sysdate,
				nm_usuario_cancel	= nm_usuario_p
			where	nr_sequencia		= nr_seq_agenda_p;			
		elsif	(ie_status_w = 'C') then
			update	rxt_agenda
			set	ie_status_agenda	= 'M',
				dt_agenda		= PKG_DATE_UTILS.start_of(dt_agenda, 'mi', 0),
				nm_usuario		= nm_usuario_p,
				dt_cancelamento		= null,
				nm_usuario_cancel	= null
			where	nr_sequencia		= nr_seq_agenda_p;					
		end if;
	elsif	(ie_status_p = 'F') then
		update	rxt_agenda
		set	ie_status_agenda	= decode(ie_status_w, 'F', 'M', ie_status_p),
			nm_usuario	= nm_usuario_p,
			nr_seq_motivo_falta	= decode(ie_status_w, 'M', nr_seq_motivo_falta, null)
		where	nr_sequencia	= nr_seq_agenda_p;
	elsif	(ie_status_p = 'M') then
		update	rxt_agenda
		set	ie_status_agenda	= ie_status_p,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_agenda_p;		
	elsif	(ie_status_p = 'H') then
		update	rxt_agenda
		set	ie_status_agenda	= ie_status_p,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_agenda_p;		
	elsif	(ie_status_p = 'V') then
		begin
		if	(ie_status_w = 'V') then
			update	rxt_agenda
			set	ie_status_agenda	= 'M',
				nm_usuario		= nm_usuario_p,
				dt_confirmacao		= null,
				nm_usuario_confirmacao	= null
			where	nr_sequencia		= nr_seq_agenda_p;
		else
			update	rxt_agenda
			set	ie_status_agenda		= ie_status_p,
				nm_usuario		= nm_usuario_p,
				dt_confirmacao		= sysdate,
				nm_usuario_confirmacao	= nm_usuario_p
			where	nr_sequencia		= nr_seq_agenda_p;
		end if;
		end;
	elsif	(ie_status_p = 'A') then
		begin
		if	(ie_status_w = 'A') then
			update	rxt_agenda
			set	ie_status_agenda		= decode(dt_confirmacao_w, null, 'M', 'V'),
				nm_usuario		= nm_usuario_p,
				dt_chegada		= null,
				nm_usuario_chegada	= null
			where	nr_sequencia		= nr_seq_agenda_p;
		else
			update	rxt_agenda
			set	ie_status_agenda		= ie_status_p,
				nm_usuario		= nm_usuario_p,
				dt_chegada		= sysdate,
				nm_usuario_chegada	= nm_usuario_p
			where	nr_sequencia		= nr_seq_agenda_p;
			
			select 	max(nr_atendimento)
			into	nr_atendimento_w
			from 	rxt_agenda
			where 	nr_sequencia = nr_seq_agenda_p;
		
			if	(nvl(nr_atendimento_w,0) > 0) then
				gerar_lancamento_automatico(nr_atendimento_w, null, 353, nm_usuario_p, null, null, null, null, null, null);
			end if;	
		end if;
		end;
	elsif	(ie_status_p = 'T') then
		begin
		if	(ie_status_w = 'T') then
			update	rxt_agenda
			set	ie_status_agenda		= 'A',
				nm_usuario		= nm_usuario_p,
				dt_tratamento		= null,
				nm_usuario_tratamento	= null
			where	nr_sequencia		= nr_seq_agenda_p;
		else
			update	rxt_agenda
			set	ie_status_agenda		= ie_status_p,
				nm_usuario		= nm_usuario_p,
				dt_tratamento		= sysdate,
				nm_usuario_tratamento	= nm_usuario_p
			where	nr_sequencia		= nr_seq_agenda_p;
		end if;
		end;
	elsif	(ie_status_p = 'E') then
		begin

		if	(ie_status_w = 'E') then
			update	rxt_agenda
			set	ie_status_agenda		= 'T',
				nm_usuario		= nm_usuario_p,
				dt_execucao		= null,
				nm_usuario_execucao	= null
			where	nr_sequencia		= nr_seq_agenda_p;
		else
			update	rxt_agenda
			set	ie_status_agenda		= ie_status_p,
				nm_usuario		= nm_usuario_p,
				dt_execucao		= sysdate,
				nm_usuario_execucao	= nm_usuario_p
			where	nr_sequencia		= nr_seq_agenda_p;
		end if;

		end;
	elsif	(ie_status_p = 'AP') then
		update	rxt_agenda
		set	ie_status_agenda	= ie_status_p,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_agenda_p;
	elsif	(ie_status_p = 'S') then
		begin
		if	(ie_status_w = 'S') then
			update	rxt_agenda
			set	ie_status_agenda	= 'A',
				nm_usuario		= nm_usuario_p,
				dt_simulacao		= null,
				nm_usuario_simulacao	= null
			where	nr_sequencia		= nr_seq_agenda_p;
		else
			update	rxt_agenda
			set	ie_status_agenda	= ie_status_p,
				nm_usuario		= nm_usuario_p,
				dt_simulacao		= sysdate,
				nm_usuario_simulacao	= nm_usuario_p
			where	nr_sequencia		= nr_seq_agenda_p;
		end if;
		end;
	elsif	(ie_status_p = 'SE') then
		begin 
		if	(ie_status_w = 'SE') then
			update	rxt_agenda
			set	ie_status_agenda		= 'S',
				nm_usuario			= nm_usuario_p,
				dt_exec_simulacao		= null,
				nm_usuario_exec_simulacao	= null
			where	nr_sequencia			= nr_seq_agenda_p;
		else
			update	rxt_agenda
			set	ie_status_agenda		= ie_status_p,
				nm_usuario			= nm_usuario_p,
				dt_exec_simulacao		= sysdate,
				nm_usuario_exec_simulacao	= nm_usuario_p
			where	nr_sequencia			= nr_seq_agenda_p;
			
			Rxt_Gerar_Proced_Fat(nr_seq_Agenda_p, null, 'S', nm_usuario_p);
		end if;
		end;
	end if;
	end;
end if;

    select OBTER_PARAM_USUARIO_LOGADO(9041,10)
    into integracao_tasy_w
    from dual;
    
    if (integracao_tasy_w = 'S')then
       retorno_integracao_w := BIFROST.SEND_INTEGRATION_CONTENT('VARIAN_registerPatientArrival',
       obter_siu_14_varian(nr_seq_agenda_p),
        OBTER_USUARIO_ATIVO);
     end if;

commit;

end rxt_alterar_status_agenda;
/