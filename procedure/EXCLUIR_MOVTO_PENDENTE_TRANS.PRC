create or replace
procedure excluir_movto_pendente_trans( nr_movimento_estoque_p	number,
					nm_usuario_p		varchar2) is

begin
update	w_protheus_movto_estoque
set	ie_status = 'X',
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate,
	ds_observacao = ds_observacao || ' ' || wheb_mensagem_pck.get_texto(325649)
where   nr_movimento_estoque = nr_movimento_estoque_p
and	ie_status <> 'X';

commit;

end excluir_movto_pendente_trans;
/