create or replace
procedure pls_vincular_segurado_ans
			(	nr_seq_tasy_ans_p	number,
				nr_seq_segurado_p	number,
				nm_usuario_p		Varchar2,
				ie_opcao_p		Varchar2) is 

cd_usuario_plano_w	varchar2(30);
dt_contratacao_w	date;
ie_tipo_registro_w	number(1);
cd_plano_w		varchar2(20);
cd_usuario_plano_ant_w	varchar2(30);
nr_seq_segurado_w	number(10);
	
begin

if (ie_opcao_p	= 'C') then
	select	a.nr_sequencia,
		b.cd_usuario_plano,
		a.dt_contratacao,
		decode(dt_rescisao,null,1,3),
		c.cd_plano
	into	nr_seq_segurado_w,
		cd_usuario_plano_w,
		dt_contratacao_w,
		ie_tipo_registro_w,
		cd_plano_w
	from	pls_segurado a,
		pls_segurado_carteira b,
		pls_plano c
	where	a.nr_sequencia	= b.nr_seq_segurado
	and	a.nr_seq_plano	= c.nr_sequencia
	and	a.nr_sequencia	= nr_seq_segurado_p;

	update	pls_sib_tasy_ans
	set	ie_tasy_ans	= 'S',
		ie_situacao_benef	= ie_tipo_registro_w,
		dt_contratacao	= dt_contratacao_w,
		cd_plano	= cd_plano_w,
		cd_usuario_plano	= cd_usuario_plano_w,
		nr_seq_segurado	= nr_seq_segurado_p,
		ie_enviar_ans	= 'A',
		ie_status	= 'V'
	where	nr_sequencia	= nr_seq_tasy_ans_p;
	
else
	delete	from pls_retorno_consistencia
	where	nr_seq_tasy_ans = nr_seq_tasy_ans_p;
	
	update	pls_sib_tasy_ans
	set	ie_tasy_ans		= 'N',
		ie_situacao_benef	= null,
		dt_contratacao		= null,
		cd_plano		= null,
		cd_usuario_plano	= null,
		nr_seq_segurado		= null,
		ie_status		= 'I'
	where	nr_sequencia		= nr_seq_tasy_ans_p;
end if;

commit;

end pls_vincular_segurado_ans;
/