create or replace 
procedure pls_gerar_analise_anexo_ws (	nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,	
					nr_seq_requisicao_p	pls_requisicao.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type ) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Rotina utilizada para deixar a autoriza��o como Aguardando anexo guia TISS, esta
rotina � utilizada somente para as Solicita��es de procedimentos via Webservice
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [  x ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
Esta rotina atualiza a an�lise da guia, deve ser usada somente no WebService
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 				
			
cursor C01 (	nr_seq_guia_pc	pls_guia_plano.nr_sequencia%type )	is
	select	a.nr_sequencia
	from	pls_auditoria a,
		pls_guia_plano b
	where	a.nr_seq_guia 		= b.nr_sequencia
	and	a.nr_seq_guia		= nr_seq_guia_pc
	and	a.ie_status		= 'A'
	and	b.ie_aguarda_anexo_guia = 'S';
	
	
cursor C02 (	nr_seq_requisicao_pc	pls_requisicao.nr_sequencia%type )	is
	select	a.nr_sequencia
	from	pls_auditoria a,
		pls_requisicao b
	where	a.nr_seq_requisicao	= b.nr_sequencia
	and	a.nr_seq_requisicao	= nr_seq_requisicao_pc
	and	a.ie_status		= 'A'
	and	b.ie_aguarda_anexo_guia = 'S';	
	
			
begin

--Verifica se a an�lise � da requisi��o primeiramente
if	( nr_seq_requisicao_p is not null ) then

	/* Setar o status da an�lise para 'Aguardando anexo guia TISS' */
	for r_C02_w in C02  ( nr_seq_requisicao_p ) loop
		update	pls_auditoria
		set	ie_status 	= 'AAG',
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p	
		where	nr_sequencia	= r_C02_w.nr_sequencia;	
		
		/* Gerar hist�rico na autoriza��o e na an�lise */
		pls_requisicao_gravar_hist( 	nr_seq_requisicao_p, 'L', 'Alterado status para Aguardando anexo guia TISS, foi enviado itens sem informar o anexo',
						null, nm_usuario_p );	
	end loop;

else
	/* Setar o status da an�lise para 'Aguardando anexo guia TISS' */
	for r_C01_w in C01  ( nr_seq_guia_p ) loop
		update	pls_auditoria
		set	ie_status 	= 'AAG',
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p	
		where	nr_sequencia	= r_C01_w.nr_sequencia;	
		
		/* Gerar hist�rico na autoriza��o e na an�lise */
		pls_guia_gravar_historico( 	nr_seq_guia_p, null, 'Alterado status para Aguardando anexo guia TISS, foi enviado itens sem informar o anexo',
						null, nm_usuario_p );	
	end loop;

end if;

commit;

end pls_gerar_analise_anexo_ws; 
/