create or replace
procedure gerar_projeto_revisao_migracao (
		ie_gerencia_p	number,
		nm_usuario_p	varchar2) is
		
nr_seq_gerencia_w	number(10,0);
cd_gerente_w		varchar2(10);		
nr_seq_projeto_w	number(10,0) := null;
nr_seq_cronograma_w	number(10,0);
nr_seq_atividade_w	number(10,0);
cd_funcao_w		number(5,0);
ds_funcao_w		varchar2(80);
nr_seq_apresent_w	number(15,0) := 1000;
nr_seq_atividade_ww	number(10,0);

cursor c01 is
select	cd_funcao,
	ds_funcao
from	funcao_migracao_v
where	ie_gerencia = ie_gerencia_p
order by
	ds_funcao;	
		
begin
if	(ie_gerencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	if	(ie_gerencia_p = 1) then
		begin
		nr_seq_gerencia_w	:= 7;
		cd_gerente_w		:= '4995';
		end;
	elsif	(ie_gerencia_p = 2) then
		begin
		nr_seq_gerencia_w	:= 4;
		cd_gerente_w		:= '442';
		end;		
	elsif	(ie_gerencia_p = 3) then
		begin
		nr_seq_gerencia_w	:= 3;
		cd_gerente_w		:= '130';
		end;					
	end if;
	
	select	proj_projeto_seq.nextval
	into	nr_seq_projeto_w
	from 	dual;
		
	insert into proj_projeto (
		nr_sequencia,
		dt_projeto,
		nr_seq_gerencia,
		nr_seq_classif,
		ds_titulo,
		cd_coordenador,
		nr_seq_cliente,
		cd_gerente_cliente,
		nr_seq_estagio,
		ie_status,
		cd_funcao,
		nr_seq_ordem_serv,
		nr_seq_prioridade,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_origem)
	values (
		nr_seq_projeto_w,
		sysdate,
		nr_seq_gerencia_w,
		14,
		'Revis�o Projetos Migra��o',
		'4464',
		1800,
		cd_gerente_w,
		1,
		'P',
		null,
		null,
		null,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'D');	
		
	select	proj_cronograma_seq.nextval
	into	nr_seq_cronograma_w
	from	dual;
	
	insert into proj_cronograma (
		nr_seq_proj,
		nr_sequencia,
		ie_tipo_cronograma,
		dt_inicio,
		ds_objetivo,
		cd_empresa,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_cliente,
		ie_estrutura,
		nr_seq_cliente)
	values (
		nr_seq_projeto_w,
		nr_seq_cronograma_w,
		'E',
		sysdate,
		'.',
		1,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_gerente_w,
		'EL',
		1800);		
		
	select	proj_cron_etapa_seq.nextval
	into	nr_seq_atividade_w
	from	dual;
	
	insert into proj_cron_etapa (
		nr_seq_cronograma,
		nr_sequencia,
		ds_atividade,
		ie_fase,
		qt_hora_prev,
		pr_etapa,
		nr_seq_apres,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_modulo,
		ie_tipo_obj_proj_migr,
		nr_seq_superior)
	values (
		nr_seq_cronograma_w,
		nr_seq_atividade_w,
		'Revis�o Projetos Migra��o',
		'S',
		0,
		0,
		nr_seq_apresent_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'N',
		null,
		null);
		
	open c01;
	loop
	fetch c01 into	cd_funcao_w,
			ds_funcao_w;
	exit when c01%notfound;
		begin
		nr_seq_apresent_w := nr_seq_apresent_w + 1000;
		
		select	proj_cron_etapa_seq.nextval
		into	nr_seq_atividade_ww
		from	dual;
		
		insert into proj_cron_etapa (
			nr_seq_cronograma,
			nr_sequencia,
			ds_atividade,
			ie_fase,
			qt_hora_prev,
			pr_etapa,
			nr_seq_apres,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_modulo,
			ie_tipo_obj_proj_migr,
			nr_seq_superior,
			cd_funcao)
		values (
			nr_seq_cronograma_w,
			nr_seq_atividade_ww,
			ds_funcao_w,
			'N',
			0,
			0,
			nr_seq_apresent_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'N',
			null,
			nr_seq_atividade_w,
			cd_funcao_w);		
		end;
	end loop;
	close c01;
	end;
end if;
commit;
end gerar_projeto_revisao_migracao;
/