create or replace
procedure GERAR_RETORNO_PAGTO_BRASIL(	nr_seq_banco_escrit_p	number,
				nm_usuario_p		varchar2) is

/*
interface: 1517 - "Retorno Pagamento em Banco - Procedure"
tabela: w_retorno_banco
procedure: GERAR_RETORNO_PAGTO_BRASIL
240 bytes - Padrao FEBRABAN
*/

ds_dt_liquidacao_w		varchar2(255);
ds_vl_liquidacao_w		varchar2(255);
cd_arquivo_w			varchar2(255);

cd_retorno_liq_w		varchar2(50);

nr_titulo_w			number(10,0);
cd_tipo_baixa_w			number(10,0);
nr_seq_trans_escrit_w		number(10,0);
nr_seq_conta_banco_w		number(10,0);
nr_sequencia_w			number(10,0);
dt_liquidacao_w			date;
vl_liquidacao_w			number(15,2);
qt_reg_w			number(10);
qt_tit_w      number(10);
cd_estabelecimento_w		number(4);
vl_escritural_w			number(15,2);
cd_tipo_baixa_padrao_w		number(5);

cd_ocorrencia1_w		varchar2(255);
cd_ocorrencia2_w		varchar2(255);
cd_ocorrencia3_w		varchar2(255);
cd_ocorrencia4_w		varchar2(255);
cd_ocorrencia5_w		varchar2(255);
ie_movto_bco_pag_escrit_w	parametros_contas_pagar.ie_movto_bco_pag_escrit%type;
ds_vl_desconto_w		varchar2(255);
vl_desconto_w			number(15,2);
qt_baixa_w				number(10);
nr_seq_trans_fin_baixa_w	titulo_pagar.nr_seq_trans_fin_baixa%type;


cursor c01 is
select	somente_numero(substr(ds_string,74,20))	nr_titulo,
	substr(ds_string,94,8)			ds_dt_liquidacao,
	somente_numero(substr(ds_string,120,15))	ds_vl_liquidacao,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5
from	w_retorno_banco
where	substr(ds_string,8,1)		= '3'
and	substr(ds_string,14,1)	= 'A'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p;

cursor c02 is
select	somente_numero(substr(ds_string,183,10))	nr_titulo,
	substr(ds_string,145,8)			ds_dt_liquidacao,
	somente_numero(substr(ds_string,153,15))	ds_vl_liquidacao,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5,
	somente_numero(substr(ds_string,115,15)) ds_vl_desconto_w	
from	w_retorno_banco
where	substr(ds_string,8,1)		= '3'
and	substr(ds_string,14,1)	= 'J'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
union
select	somente_numero(substr(ds_string,123,20)) nr_titulo,
	substr(ds_string,100,8) ds_dt_liquidacao,
	somente_numero(substr(ds_string,108,15)) ds_vl_liquidacao,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5,
	null ds_vl_desconto_w	
from	w_retorno_banco
where	substr(ds_string, 14, 1)	= 'O'
and	substr(ds_string, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p;

begin

begin
select	b.cd_retorno_liq,
	a.cd_estabelecimento,
	a.nr_seq_conta_banco,
	a.nr_seq_trans_financ
into	cd_retorno_liq_w,
	cd_estabelecimento_w,
	nr_seq_conta_banco_w,
	nr_seq_trans_escrit_w
from	banco_retorno_cp b,
	banco_escritural a
where	a.cd_banco		= b.cd_banco
and	a.nr_sequencia		= nr_seq_banco_escrit_p;
exception
	when no_data_found then
	/* Nao foi encontrado o codigo de retorno da liquidacao!
	Verifique o cadastro de "Retorno CP" no cadastro de bancos. */
	wheb_mensagem_pck.exibir_mensagem_abort(200894);
end;

if	(nr_seq_trans_escrit_w	is null) then

	select	max(nr_seq_trans_escrit)
	into	nr_seq_trans_escrit_w
	from	parametro_tesouraria
	where	cd_estabelecimento	= cd_estabelecimento_w;

end if;

if	(nr_Seq_trans_escrit_w	is not null) then
	select	max(cd_tipo_baixa)
	into	cd_tipo_baixa_w
	from	transacao_financeira
	where	nr_sequencia = nr_seq_trans_escrit_w;
end if;

if	(cd_tipo_baixa_w	is null) then
	select	nvl(cd_tipo_baixa_padrao, 1)
	into	cd_tipo_baixa_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= cd_estabelecimento_w;
end if;

select	substr(ds_string,143,1)
into	cd_arquivo_w
from 	w_retorno_banco
where	substr(ds_string,8,1) = '0'
and	substr(ds_string,4,4) = '0000'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

select	max(ie_movto_bco_pag_escrit)
into	ie_movto_bco_pag_escrit_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;


if	(cd_arquivo_w	= '2')	then
	open c01;
	loop
	fetch c01 into
		nr_titulo_w,
		ds_dt_liquidacao_w,
		ds_vl_liquidacao_w,
		cd_ocorrencia1_w,
		cd_ocorrencia2_w,
		cd_ocorrencia3_w,
		cd_ocorrencia4_w,
		cd_ocorrencia5_w;
	exit when c01%notfound;

		dt_liquidacao_w		:= to_date(ds_dt_liquidacao_w);
		vl_liquidacao_w		:= to_number(ds_vl_liquidacao_w);
		vl_liquidacao_w		:= dividir_sem_round(ds_vl_liquidacao_w,100);

		select	count(*)
		into	qt_reg_w
		from	titulo_pagar_escrit
		where	nr_seq_escrit	= nr_seq_banco_escrit_p
		and	nr_titulo			= nr_titulo_w;

		if	(qt_reg_w = 0) then
			gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);

			qt_reg_w	:= 1;
		end if;


		if	(cd_retorno_liq_w = cd_ocorrencia1_w) and (qt_reg_w	> 0) and (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T')	then
			
			select	vl_escritural
			into	vl_escritural_w
			from	titulo_pagar_escrit
			where	nr_seq_escrit	= nr_seq_banco_escrit_p
			and	nr_titulo	= nr_titulo_w;
			/* Colocado este tratamento para nao efetuar baixa com valor maior que o titulo */
			if	(vl_liquidacao_w	> vl_escritural_w) then
				vl_liquidacao_w	:= vl_escritural_w;
			end if;
			
			select	max(nr_seq_trans_fin_baixa)
			into	nr_seq_trans_fin_baixa_w
			from	titulo_pagar
			where	nr_titulo = nr_titulo_w;
		
			qt_baixa_w	:= nvl(qt_baixa_w,0) + 1;
		
			baixa_titulo_pagar
					(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_w,
					vl_liquidacao_w,
					nm_usuario_p,
					nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_escrit_w),
					null,
					nr_seq_banco_escrit_p,
					dt_liquidacao_w,
					nr_seq_conta_banco_w);

			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	titulo_pagar_baixa
			where	nr_titulo	= nr_titulo_w;

			gerar_movto_tit_baixa
					(nr_titulo_w,
					nr_sequencia_w,
					'P',
					nm_usuario_p,
					'N');

			atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
			Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
			
			update	titulo_pagar_escrit
			set	ds_erro			= cd_ocorrencia1_w||cd_ocorrencia2_w||cd_ocorrencia3_w||cd_ocorrencia4_w||cd_ocorrencia5_w
			where	nr_seq_escrit		= nr_seq_banco_escrit_p
			and	nr_titulo		= nr_titulo_w;			
		else
			update	titulo_pagar_escrit
			set	ds_erro			= cd_ocorrencia1_w||cd_ocorrencia2_w||cd_ocorrencia3_w||cd_ocorrencia4_w||cd_ocorrencia5_w
			where	nr_seq_escrit		= nr_seq_banco_escrit_p
			and	nr_titulo		= nr_titulo_w;
		end if;
	
	
	end loop;
	close c01;

	Open c02;
	loop
	fetch c02 into
		nr_titulo_w,
		ds_dt_liquidacao_w,
		ds_vl_liquidacao_w,
		cd_ocorrencia1_w,
		cd_ocorrencia2_w,
		cd_ocorrencia3_w,
		cd_ocorrencia4_w,
		cd_ocorrencia5_w,
		ds_vl_desconto_w;
	exit when c02%notfound;
  
  	select	count(*)
      into	qt_tit_w
      from	titulo_pagar
      where	nr_titulo	= nr_titulo_w;
		
    if (qt_tit_w > 0) then 
    
      dt_liquidacao_w		:= to_date(ds_dt_liquidacao_w);
      vl_liquidacao_w		:= to_number(ds_vl_liquidacao_w);
      vl_liquidacao_w		:= dividir_sem_round(ds_vl_liquidacao_w,100);
      vl_desconto_w		:= to_number(ds_vl_desconto_w);
      vl_desconto_w		:= dividir_sem_round(vl_desconto_w,100);
      
      select	count(*)
      into	qt_reg_w
      from	titulo_pagar_escrit
      where	nr_seq_escrit	= nr_seq_banco_escrit_p
      and	nr_titulo	= nr_titulo_w;
  
      if	((qt_reg_w = 0) and (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T')) or ((qt_reg_w = 0) and (nvl(ie_movto_bco_pag_escrit_w,'T') = 'L') and (cd_retorno_liq_w = cd_ocorrencia1_w)) then			
        gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);
        qt_reg_w := 1;
      end if;
      
      if	(cd_retorno_liq_w = cd_ocorrencia1_w) and (qt_reg_w	> 0) and (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T')	then
  
        select	vl_escritural
        into	vl_escritural_w
        from	titulo_pagar_escrit
        where	nr_seq_escrit	= nr_seq_banco_escrit_p
        and		nr_titulo			= nr_titulo_w;
      
        /*Colocado este tratamento para nao efetuar baixa com valor maior que o titulo - Feltrin OS88049*/
        if	(vl_liquidacao_w	> vl_escritural_w) then
          vl_liquidacao_w	:= vl_escritural_w;
        end if;
        /*Se por acaso ultrapassar o valor escritural, nao considerar o desconto*/
        if ((nvl(vl_liquidacao_w,0) + nvl(vl_desconto_w,0)) >  vl_escritural_w) then
          vl_desconto_w := 0;
        end if;
        
        qt_baixa_w	:= nvl(qt_baixa_w,0) + 1;
  
        baixa_titulo_pagar
            (cd_estabelecimento_w,
            cd_tipo_baixa_w,
            nr_titulo_w,
            vl_liquidacao_w + nvl(vl_desconto_w,0),
            nm_usuario_p,
            nr_seq_trans_escrit_w,
            null,
            nr_seq_banco_escrit_p,
            dt_liquidacao_w,
            nr_seq_conta_banco_w);
  
        select	max(nr_sequencia)
        into	nr_sequencia_w
        from	titulo_pagar_baixa
        where	nr_titulo	= nr_titulo_w;
  
        gerar_movto_tit_baixa
            (nr_titulo_w,
            nr_sequencia_w,
            'P',
            nm_usuario_p,
            'N');
  
        atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
        Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
  
        update	titulo_pagar_escrit
        set	ds_erro			= cd_ocorrencia1_w||cd_ocorrencia2_w||cd_ocorrencia3_w||cd_ocorrencia4_w||cd_ocorrencia5_w
        where	nr_seq_escrit		= nr_seq_banco_escrit_p
        and	nr_titulo		= nr_titulo_w;
      else
        update	titulo_pagar_escrit
        set	ds_erro			= cd_ocorrencia1_w||cd_ocorrencia2_w||cd_ocorrencia3_w||cd_ocorrencia4_w||cd_ocorrencia5_w
        where	nr_seq_escrit		= nr_seq_banco_escrit_p
        and	nr_titulo		= nr_titulo_w;
      end if;
	  

    end if;
	
	end loop;
	close c02;
else
	/* O arquivo que esta sendo importado nao e de retorno, favor verifique! */
	wheb_mensagem_pck.exibir_mensagem_abort(200895);
end if;

if (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T' and nvl(qt_baixa_w,0)	> 0) then
	update	banco_escritural
	set	dt_baixa	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_banco_escrit_p;
end if;

commit;

end GERAR_RETORNO_PAGTO_BRASIL;
/
