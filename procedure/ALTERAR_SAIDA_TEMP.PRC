create or replace
procedure alterar_saida_temp(
			nr_atendimento_p	number,
			ie_opcao_p	varchar2) is
/* ie_opcao_p = S - saida / R - retorno */
-- indentation only
ie_gerar_clinical_notes_w varchar2(1) := 'N';
cd_evolucao_w    evolucao_paciente.cd_evolucao%type;
nr_seq_interno_w number (10);
			
begin

obter_param_usuario(281, 1598, Obter_perfil_ativo, obter_usuario_ativo, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_clinical_notes_w);

if ( ie_gerar_clinical_notes_w = 'S') then
select Max( nr_seq_interno)
into nr_seq_interno_w
from atend_paciente_unidade
where nr_atendimento = nr_atendimento_p;
end if;

if	(nr_atendimento_p is not null) then

	if (ie_opcao_p = 'S') then
		update	atend_paciente_unidade
		set	dt_saida_temporaria = sysdate,
			dt_retorno_saida_temporaria = null,
			nm_usuario = nvl(obter_usuario_ativo,nm_usuario),
			dt_atualizacao = sysdate
		where	nr_atendimento = nr_atendimento_p;


                if (ie_gerar_clinical_notes_w = 'S' and nr_atendimento_p is not null and nr_seq_interno_w is not null) then
                clinical_notes_pck.gerar_soap (nr_atendimento_p,nr_seq_interno_w,'TEMP_LEAVE',null,'P',1,cd_evolucao_w,ie_opcao_p);
                update atend_paciente_unidade
                set cd_evolucao = cd_evolucao_w
                where	nr_atendimento = nr_atendimento_p;
                end if;

	elsif (ie_opcao_p = 'R') then
		update	atend_paciente_unidade
		set	dt_retorno_saida_temporaria = sysdate,
			nm_usuario = nvl(obter_usuario_ativo,nm_usuario),
			dt_atualizacao = sysdate
		where	nr_atendimento = nr_atendimento_p;

		if (ie_gerar_clinical_notes_w = 'S' and nr_atendimento_p is not null and nr_seq_interno_w is not null) then
                        clinical_notes_pck.gerar_soap (nr_atendimento_p,nr_seq_interno_w,'TEMP_LEAVE',null,'P',1,cd_evolucao_w,ie_opcao_p);
                        update atend_paciente_unidade
                        set cd_evolucao = cd_evolucao_w
                        where	nr_atendimento = nr_atendimento_p;
                end if;

                update WOCUPACAO_SAIDA_TEMPORARIA
                set dt_final_saida = sysdate
                where NR_ATENDIMENTO = nr_atendimento_p
                and   DT_INICIO_SAIDA is not null;

	end if;	
	
end if;

commit;

end alterar_saida_temp;
/
