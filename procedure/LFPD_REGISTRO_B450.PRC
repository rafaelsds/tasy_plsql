create or replace
procedure lfpd_registro_B450(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				ds_separador_p		varchar2,
				qt_linha_p		in out number,
				nr_sequencia_p		in out number) is

contador_w		number(10)	:= 0;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10)	:= qt_linha_p;
nr_seq_registro_w	number(10)	:= nr_sequencia_p;
sep_w			varchar2(1)	:= ds_separador_p;
cd_municipio_w		varchar2(10);
cd_cgc_w		varchar2(14);

cursor c01 is
	select	'B450' cd_registro,
		substr(decode(Obter_se_nota_entrada_saida(n.nr_sequencia),'E',0,'S',1),1,1) ie_operacao,
		'5300108' cd_municipio,
		replace(campo_mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',') vl_contabil_nota,
		replace(campo_mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',') vl_base_calculo,
		'0' vl_isento,
		'0' vl_deducoes,
		replace(campo_mascara(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS')),2),'.',',')  vl_issqn_retido,
		replace(campo_mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),2),'.',',') vl_issqn_destacado
	from	nota_fiscal		n,
		operacao_nota		o
	where   n.cd_operacao_nf	= o.cd_operacao_nf
	and 	o.ie_servico		= 'S'
	and	n.ie_situacao		= 1
	and	n.cd_estabelecimento	= cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.vl_total_nota > 0
	and 	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS') > 0
	and nvl(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CDPAIS'),'1058') = '1058'
	and	exists (select	1
		from	nota_fiscal_item x
		where	x.nr_sequencia = n.nr_sequencia
		and 	(((nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E') and x.cd_material is not null) 
			or ((nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S') and x.cd_procedimento is not null))
		and	rownum = 1
		)
	group by substr(decode(Obter_se_nota_entrada_saida(n.nr_sequencia),'E',0,'S',1),1,1);

vet01	c01%RowType;

begin

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin
	contador_w	:= contador_w + 1;

	cd_municipio_w	:= vet01.cd_municipio;

	ds_linha_w	:= substr(	sep_w	|| vet01.cd_registro		||
					sep_w	|| vet01.ie_operacao		||
					sep_w	|| cd_municipio_w		||
					sep_w	|| vet01.vl_contabil_nota	||
					sep_w	|| vet01.vl_base_calculo	||
					sep_w	|| vet01.vl_isento		||
					sep_w	|| vet01.vl_deducoes		||
					sep_w	||vet01.vl_issqn_retido		||
					sep_w	|| vet01.vl_issqn_destacado	||
					sep_w, 1, 8000);

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_lfpd_arquivo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_linha,
		ds_arquivo,
		ds_arquivo_compl,
		cd_registro,
		nr_seq_controle_lfpd)
	values	(fis_lfpd_arquivo_seq.NextVal,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_linha_w,
		ds_arquivo_w,
		ds_arquivo_compl_w,
		vet01.cd_registro,
		nr_seq_controle_p);

	if	(mod(contador_w,100) = 0) then
		commit;
	end if;
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_registro_B450;

/