create or replace
Procedure vincular_oftalmo_prescricao(	nr_prescricao_p		in number,
						nr_seq_consulta_p	in number,
						nm_usuario_p		in varchar) 
						is
						
begin
if	(nvl(nr_seq_consulta_p,0) > 0) and (nvl(nr_prescricao_p,0) > 0) then
	update	prescr_medica
	set	nr_seq_consulta_oft 	= nr_seq_consulta_p
	where	nr_prescricao		= nr_prescricao_p;
	commit;
end if;	
	
end vincular_oftalmo_prescricao;
/