create or replace procedure liberar_contrato(  nr_sequencia_p    number,
        nm_usuario_p    Varchar2 ) is



begin
-- Verifica primeiro se precisa entrar em processo de aprovacao
gerar_processo_aprov_contrato( nr_sequencia_p, nm_usuario_p, 'N');

update  contrato
set  dt_liberacao  = sysdate,
  nm_usuario_lib  = nm_usuario_p,
  dt_aprovacao  = decode(nr_seq_aprovacao,null,sysdate,dt_aprovacao)
where  nr_sequencia  = nr_sequencia_p;

commit;

end liberar_contrato;
/

