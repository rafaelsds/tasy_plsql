create or replace
procedure consiste_lab_convenio_js(	cd_convenio_p		number,
					nr_atendimento_cat_p	number,
					cd_estabelecimento_p	number,
					ie_obter_se_exige_cpf_p	varchar2,
					nr_atendimento_p	number,
					ie_exige_cpf_paciente_p	out varchar2,
					nr_seq_interno_p	out number,
					ds_msg_erro_p		out varchar2) is 

ie_exige_cpf_paciente_w	varchar2(1);
ds_msg_erro_w		varchar2(255);
ie_continua_exec_w	varchar2(1)	:= 'S';
nr_seq_interno_w	number(10,0);
				
begin

if	(cd_convenio_p is not null)and
	(nr_atendimento_cat_p is not null)then
	begin
	
	select 	max(ie_exige_cpf_paciente)
	into	ie_exige_cpf_paciente_w
	from   	convenio_estabelecimento
	where  	cd_convenio = cd_convenio_p
	and    	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(ie_obter_se_exige_cpf_p = 'S')and
		(ie_exige_cpf_paciente_w = 'S')then
		begin
		
		update 	atendimento_paciente 
		set 	ie_tipo_convenio = obter_tipo_convenio(cd_convenio_p)
		where 	nr_atendimento 	= nr_atendimento_cat_p;
		
		ds_msg_erro_w		:= Wheb_mensagem_pck.get_texto(306379); -- 'Este conv�nio exige a informa��o do CPF do paciente ou respons�vel. Par�metro[501].';
		ie_continua_exec_w	:= 'N';
		
		end;
	end if;
	
	end;
end if;

if	(ie_continua_exec_w = 'S')and
	(nr_atendimento_p is not null)then
	begin
	
	select 	nvl(max(nr_seq_interno),0) 
	into	nr_seq_interno_w
	from  	atend_categoria_convenio   
	where 	nr_atendimento = nr_atendimento_p;
	
	if	(nr_seq_interno_w = 0)then
		begin
		
		ds_msg_erro_w		:= Wheb_mensagem_pck.get_texto(306380); -- '� necess�rio a informa��o dos dados do conv�nio!';
		
		end;
	end if;
	
	end;
end if;

ds_msg_erro_p		:= ds_msg_erro_w;
nr_seq_interno_p	:= nr_seq_interno_w;
ie_exige_cpf_paciente_p := ie_exige_cpf_paciente_w;

end consiste_lab_convenio_js;
/
