create or replace
procedure pls_diops_confirmar_envio
		(	nr_seq_periodo_p	Number,
			nm_usuario_p		Varchar2) is 

ds_maquina_w		Varchar2(80);			

begin

select	substr(obter_inf_sessao(0)||' - '||obter_inf_sessao(1),1,80)
into	ds_maquina_w
from	dual;

update	diops_periodo
set	nm_usuario_envio	= nm_usuario_p,
	ds_maquina_envio	= ds_maquina_w,
	dt_envio		= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_periodo_p;

pls_diops_gravar_historico(nr_seq_periodo_p,4,'','',nm_usuario_p);

commit;

end pls_diops_confirmar_envio;
/
