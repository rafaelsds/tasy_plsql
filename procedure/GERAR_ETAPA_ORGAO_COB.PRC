create or replace
procedure GERAR_ETAPA_ORGAO_COB(	nr_seq_cob_hist_p	number,
					nm_usuario_p		varchar2) is
					
nr_seq_etapa_w		number(10);
nr_seq_orgao_w		number(10);
nr_seq_cobranca_w	number(10);
qt_registro_w		number(10);

begin

select	max(b.nr_seq_etapa),
	max(b.nr_seq_orgao),
	max(a.nr_seq_cobranca)
into	nr_seq_etapa_w,
	nr_seq_orgao_w,
	nr_seq_cobranca_w
from	tipo_hist_cob b,
	cobranca_historico a
where	a.nr_sequencia		= nr_seq_cob_hist_p
and	a.nr_seq_historico	= b.nr_sequencia;

if	(nr_seq_etapa_w is not null) then

	select	count(*)
	into	qt_registro_w
	from	cobranca_etapa a
	where	a.nr_seq_cobranca	= nr_seq_cobranca_w
	and	a.nr_seq_etapa		= nr_seq_etapa_w;

	if	(nvl(qt_registro_w,0) = 0) then

		insert	into cobranca_etapa
			(ds_observacao,
			dt_atualizacao,
			dt_etapa,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_etapa,
			nr_sequencia)
		values	(WHEB_MENSAGEM_PCK.get_texto(303592),
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_cobranca_w,
			nr_seq_etapa_w,
			cobranca_etapa_seq.nextval);

	end if;

end if;

if	(nr_seq_orgao_w is not null) then

	select	count(*)
	into	qt_registro_w
	from	cobranca_etapa a
	where	a.nr_seq_cobranca	= nr_seq_cobranca_w
	and	a.nr_seq_etapa		= nr_seq_orgao_w;

	if	(nvl(qt_registro_w,0) = 0) then

		insert	into cobranca_orgao
			(ds_observacao,
			dt_atualizacao,
			dt_inclusao,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_orgao,
			nr_sequencia)
		values	(WHEB_MENSAGEM_PCK.get_texto(303593),
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_cobranca_w,
			nr_seq_orgao_w,
			cobranca_orgao_seq.nextval);

	end if;

end if;

commit;

end GERAR_ETAPA_ORGAO_COB;
/