create or replace
procedure desdobrar_bordero_recebimento	(nr_bordero_p	number,
					 vl_abaixar_p	number) is

nr_bordero_novo_w	number(15);
nr_titulo_w		number(15);
vl_abaixar_w		number(15,4);
/* Projeto Multimoeda - Vari�veis */
vl_abaixar_estrang_w	number(15,2);
vl_cotacao_w		cotacao_moeda.vl_cotacao%type;
cd_moeda_w		number(5);
ie_moeda_estrang_w	varchar2(1) := 'N';

cursor c01 is
select	nr_titulo,
	vl_abaixar_estrang,
	vl_cotacao,
	cd_moeda
from	bordero_tit_rec
where	nr_bordero	= nr_bordero_p;

begin

select	bordero_recebimento_seq.nextval
into	nr_bordero_novo_w
from	dual;

insert into bordero_recebimento(
	cd_cgc_pagamento,
	cd_estabelecimento,
	cd_pessoa_pagamento,
	cd_tipo_recebimento,
	ds_bordero,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_bordero,
	dt_prev_receb,
	dt_recebimento,
	nm_usuario,
	nm_usuario_nrec,
	nr_bordero,
	nr_seq_conta_banco,
	nr_seq_trans_fin,
	cd_moeda,
	vl_cotacao)
select	cd_cgc_pagamento,
	cd_estabelecimento,
	cd_pessoa_pagamento,
	cd_tipo_recebimento,
	wheb_mensagem_pck.get_texto(303606, 'DS_BORDERO=' || substr(ds_bordero,1,67)), -- #@DS_BORDERO#@ - desdobrado
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_bordero,
	dt_prev_receb,
	dt_recebimento,
	nm_usuario,
	nm_usuario_nrec,
	nr_bordero_novo_w,
	nr_seq_conta_banco,
	nr_seq_trans_fin,
	cd_moeda,
	decode(nvl(vl_cotacao,0),0,null,vl_cotacao)
from	bordero_recebimento
where	nr_bordero	= nr_bordero_p;

select	sum(nvl(vl_abaixar,0))
into	vl_abaixar_w
from	bordero_tit_rec
where	nr_bordero	= nr_bordero_p;

open c01;
loop
fetch c01 into
	nr_titulo_w,
	vl_abaixar_estrang_w,
	vl_cotacao_w,
	cd_moeda_w;
exit when c01%notfound;
	
	/* Projeto Multimoeda - Verifica se o bordero � moeda estrangeira para calcular os valores em moeda estrangeira ao gravar os dados.*/
	if (nvl(vl_abaixar_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
		ie_moeda_estrang_w := 'S';
	else
		ie_moeda_estrang_w := 'N';
	end if;
	
	insert into bordero_tit_rec(
		cd_centro_custo_desc,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_bordero,
		nr_seq_motivo_desc,
		nr_titulo,
		vl_abaixar,
		vl_amenor,
		vl_desconto,
		vl_glosa,
		vl_juros,
		vl_multa,
		vl_perdas,
		vl_rec_maior,
		vl_abaixar_estrang,
		vl_cotacao,
		cd_moeda)
	select	cd_centro_custo_desc,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_bordero_novo_w,
		nr_seq_motivo_desc,
		nr_titulo,
		(vl_abaixar - dividir_sem_round((vl_abaixar * vl_abaixar_p), vl_abaixar_w)),
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		decode(nvl(ie_moeda_estrang_w,'N'), 'S', ((vl_abaixar - dividir_sem_round((vl_abaixar * vl_abaixar_p), vl_abaixar_w))/vl_cotacao_w), null),
		decode(nvl(ie_moeda_estrang_w,'N'), 'S', vl_cotacao_w, null),
		decode(nvl(ie_moeda_estrang_w,'N'), 'S', cd_moeda_w, null)
	from	bordero_tit_rec
	where	nr_titulo	= nr_titulo_w
	and		nr_bordero	= nr_bordero_p;
	
	/* Projeto Multimoeda - Quando bordero em moeda estrangeira atualiza o valor do complemento para o t�tulo inserido. */
	if (nvl(ie_moeda_estrang_w,'N') = 'S') then
		update	bordero_tit_rec
		set	vl_complemento = vl_abaixar - vl_abaixar_estrang
		where	nr_bordero = nr_bordero_novo_w
		and	nr_titulo = nr_titulo_w;
	end if;

	update	bordero_tit_rec
	set	vl_abaixar	= dividir_sem_round((vl_abaixar * vl_abaixar_p), vl_abaixar_w),
		vl_abaixar_estrang = decode(nvl(ie_moeda_estrang_w,'N'), 'S', (dividir_sem_round((vl_abaixar * vl_abaixar_p), vl_abaixar_w))/vl_cotacao_w, null)
	where	nr_bordero	= nr_bordero_p
	and	nr_titulo	= nr_titulo_w;
	
	/* Projeto Multimoeda - Quando bordero em moeda estrangeira atualiza o valor do complemento para o t�tulo no bordero antigo */
	if (nvl(ie_moeda_estrang_w,'N') = 'S') then
		update	bordero_tit_rec
		set	vl_complemento = vl_abaixar - vl_abaixar_estrang
		where	nr_bordero = nr_bordero_p
		and	nr_titulo = nr_titulo_w;
	end if;

end loop;
close c01;

commit;

end	desdobrar_bordero_recebimento;
/