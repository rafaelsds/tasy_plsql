create or replace
procedure ptu_gerar_envio_arq_a700_v90(	nr_seq_servico_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p		varchar2) is 

-- Arquivo
ds_conteudo_w			varchar2(4000);
ds_arquivo_w			clob;
ds_hash_w			ptu_servico_pre_pagto.ds_hash%type;

ds_meridiano_w			varchar2(3);
cd_cooperativa_w		varchar2(4);
cd_cgc_w			varchar2(14);
sg_estado_w			pessoa_juridica.sg_estado%type;
nr_seq_registro_w		number(10) := 0;
qt_registro_w			number(10) := 0;
nr_seq_nota_cobranca_w		number(10);
nr_seq_nota_cobranca_ww		number(10);
nr_seq_nota_servico_w		number(10);
qt_tot_r702_w			number(5) := 0;
qt_tot_r703_w			number(5) := 0;
qt_tot_r704_w			number(5) := 0;
qt_tot_r704_ww			number(10) := 0;
qt_tot_r705_w			number(5) := 0;
					
-- 701
cd_unimed_destino_w		varchar2(4);
cd_unimed_origem_w		varchar2(4);
dt_geracao_w			varchar2(8);
nr_seq_geracao_w		varchar2(4);
dt_inicio_pagto_w		varchar2(8);
dt_fim_pagto_w			varchar2(8);
nr_versao_transacao_w		varchar2(2);
cd_operadora_origem_w		pls_congenere.cd_cooperativa%type;

-- 702
nm_beneficiario_w		varchar2(25);
dt_atendimento_w		varchar2(21);
cd_usuario_plano_w		varchar2(13);
nr_nota_w			ptu_nota_cobranca.nr_nota%type;
nr_guia_principal_w		ptu_nota_cobranca.nr_guia_principal%type;
nr_lote_w			varchar2(8);
cd_cid_w			varchar2(6);
cd_unimed_w			varchar2(4);
ie_tipo_atendimento_w		varchar2(2);
cd_excecao_w			varchar2(1);
ie_carater_atendimento_w	varchar2(1);
ie_paciente_w			varchar2(1);
ie_tipo_saida_spdat_w		varchar2(1);
dt_internacao_702_w		varchar2(21);
dt_alta_702_w			varchar2(21);
dt_ultima_autoriz_w		varchar2(8);
tp_nota_w			varchar2(1);
id_nota_principal_w		varchar2(1);
nr_ver_tiss_w			varchar2(7);
nr_guia_tiss_prestador_w	varchar2(20);
nr_guia_tiss_principal_w	varchar2(20);
nr_guia_tiss_operadora_w	varchar2(20);
tp_ind_acidente_w		varchar2(1);
motivo_encerram_w		varchar2(2);
nr_cnpj_cpf_req_w		varchar2(14);
nm_prest_req_w			varchar2(70);
sg_cons_prof_req_702_w		varchar2(12);
nr_cons_prof_req_702_w		varchar2(15);
sg_uf_cons_req_702_w		varchar2(2);
nr_cbo_req_w			varchar2(6);
nr_fatura_glosada_w		varchar2(11);
nr_ndr_glosada_w		varchar2(11);
nr_lote_glosado_w		varchar2(8);
nr_nota_glosada_w		varchar2(11);
dt_protocolo_w			varchar2(8);
id_rn_w				varchar2(1);
dt_atendimento_ww		date;
tp_consulta_w			varchar2(1);
tp_pessoa_w			varchar2(1);
nr_cnpj_cpf_w			varchar2(14);
cd_cnes_cont_exec_w		varchar2(7);
cd_munic_cont_exec_w		varchar2(7);

-- 703
nm_hospital_w			varchar2(70);
dt_internacao_w			varchar2(21);
dt_alta_w			varchar2(21);
nr_declara_obito_w		varchar2(17);
nr_declara_obito_0_w		varchar2(17);
nr_declara_obito_1_w		varchar2(17);
nr_declara_obito_2_w		varchar2(17);
nr_declara_obito_3_w		varchar2(17);
nr_declara_obito_4_w		varchar2(17);
nr_declara_obito_5_w		varchar2(17);
nr_declara_vivo_0_w		varchar2(15);
nr_declara_vivo_1_w		varchar2(15);
nr_declara_vivo_2_w		varchar2(15);
nr_declara_vivo_3_w		varchar2(15);
nr_declara_vivo_4_w		varchar2(15);
nr_declara_vivo_5_w		varchar2(15);
cd_cgc_hospital_w		varchar2(14);
nr_nota_703_w			ptu_nota_hospitalar.nr_nota%type;
nr_lote_703_w			varchar2(8);
cd_hospital_w			varchar2(8);
cd_cid_obito_w			varchar2(6);
cd_cid_obito_0_w		varchar2(6);
cd_cid_obito_1_w		varchar2(6);
cd_cid_obito_2_w		varchar2(6);
cd_cid_obito_3_w		varchar2(6);
cd_cid_obito_4_w		varchar2(6);
cd_cid_obito_5_w		varchar2(6);
cd_unimed_hospital_w		varchar2(4);
tx_mult_amb_w			varchar2(4);
ie_tipo_acomodacao_w		varchar2(2);
cd_motivo_saida_w		varchar2(2);
qt_nasc_vivos_w			varchar2(2);
qt_nasc_mortos_w		varchar2(2);
qt_nasc_vivos_pre_w		varchar2(2);
qt_obito_precoce_w		varchar2(2);
qt_obito_tardio_w		varchar2(2);
ie_ind_acidente_w		varchar2(1);
ie_tipo_internacao_w		varchar2(1);
ie_int_gestacao_w		varchar2(1);
ie_int_aborto_w			varchar2(1);
ie_int_transtorno_w		varchar2(1);
ie_int_puerperio_w		varchar2(1);
ie_int_recem_nascido_w		varchar2(1);
ie_int_neonatal_w		varchar2(1);
ie_int_baixo_peso_w		varchar2(1);
ie_int_parto_cesarea_w		varchar2(1);
ie_int_parto_normal_w		varchar2(1);
ie_faturamento_w		varchar2(1);
ie_obito_mulher_w		varchar2(1);
reg_internacao_w		varchar2(1);

-- 704
ds_servico_w			varchar2(80);
nm_prestador_w			varchar2(70);
nm_profissional_prestador_w	varchar2(70);
nm_prestador_requisitante_w	varchar2(40);
nr_gui_tiss_w			varchar2(20);
nr_cons_prof_prest_w		varchar2(15);
nr_cons_prof_req_w		varchar2(15);
vl_procedimento_w		varchar2(14);
vl_custo_operacional_w		varchar2(14);
vl_filme_w			varchar2(14);
vl_adic_procedimento_w		varchar2(14);
vl_adic_co_w			varchar2(14);
vl_adic_filme_w			varchar2(14);
nr_cgc_cpf_w			varchar2(14);
nr_cgc_cpf_req_w		varchar2(14);
vl_pago_prest_w			varchar2(14);
sg_cons_prof_prest_w		varchar2(12);
sg_cons_prof_req_w		varchar2(12);
nr_nota_704_w			ptu_nota_servico.nr_nota%type;
nr_seq_nota_w			varchar2(11);
nr_autorizacao_w		varchar2(10);
nr_lote_704_w			varchar2(8);
cd_prestador_w			varchar2(8);
dt_procedimento_w		varchar2(8);
cd_procedimento_w		varchar2(8);
qt_procedimento_w		varchar2(8);
cd_prestador_req_w		varchar2(8);
ds_hora_procedimento_w		varchar2(8);
cd_pacote_w			varchar2(8);
cd_cnes_prest_w			varchar2(7);
cd_unimed_prestador_w		varchar2(4);
cd_unimed_autorizadora_w	varchar2(4);
cd_unimed_pre_req_w		varchar2(4);
tx_procedimento_w		varchar2(3);
ie_via_acesso_w			varchar2(2);
cd_especialidade_w		varchar2(2);
ie_tipo_prestador_w		varchar2(2);
sg_uf_cons_prest_w		varchar2(2);
sg_uf_cons_req_w		varchar2(2);
ie_tipo_participacao_w		varchar2(1);
ie_tipo_tabela_w		varchar2(1);
cd_porte_anestesico_w		varchar2(1);
ie_rede_propria_w		varchar2(1);
ie_tipo_pessoa_prestador_w	varchar2(1);
ie_pacote_w			varchar2(1);
cd_ato_w			varchar2(1);
ie_reembolso_w			varchar2(1);
tp_autoriz_w			varchar2(1);
ie_prestador_alto_custo_w	varchar2(1) := ' ';
cd_especialidade_ptu_w		number(2);
hr_final_w			varchar2(8);
id_acres_urg_emer_w		varchar2(1);
nr_cbo_exec_w			varchar2(6);
tec_utilizada_w			varchar2(1);
dt_autoriz_w			varchar2(8);
dt_solicitacao_w		varchar2(8);
unidade_medida_w		varchar2(3);
nr_reg_anvisa_w			varchar2(15);
cd_munic_w			varchar2(7);
cd_ref_material_fab_w		varchar2(60);
dt_pgto_prestador_w		varchar2(8);
nr_cnpj_fornecedor_w		varchar2(14);
qt_nota_servico_w		pls_integer;
cd_rec_prestador_w		varchar2(10);

-- 705
ds_complemento_w		varchar2(100);
nr_nota_705_w			ptu_nota_complemento.nr_nota%type;
nr_lote_705_w			varchar2(8);
ie_tipo_complemento_w		varchar(1);
especif_material_w		varchar(500);

-- 709
vl_tot_serv_w			varchar2(14);
vl_tot_co_w			varchar2(14);
vl_tot_filme_w			varchar2(14);

Cursor C00 is
	select	lpad(nvl(cd_unimed_destino,'0'),4,'0'),
		lpad(nvl(nvl(cd_unimed_origem,cd_operadora_origem_w),'0'),4,'0'),
		to_char(dt_geracao,'yyyymmdd'),
		lpad(nvl(nr_seq_geracao,'0'),4,'0'),
		to_char(dt_inicio_pagto,'yyyymmdd'),
		to_char(dt_fim_pagto,'yyyymmdd'),
		'20'
	from	ptu_servico_pre_pagto
	where	nr_sequencia	= nr_seq_servico_p;
	
Cursor C01 is
	select	lpad(nvl(elimina_caractere_especial(to_char(nr_lote)),'0'),8,'0'),
		lpad(nvl(substr(elimina_caractere_especial(to_char(nvl(pls_obter_dados_conta(nr_nota,'G'),pls_obter_dados_conta(nr_nota,'GR')))),1,20),'0'),20,'0'),
		lpad(nvl(cd_unimed,'0'),4,'0'),
		lpad(nvl(cd_usuario_plano,'0'),13,'0'),
		rpad(nvl(elimina_acentuacao(nm_beneficiario),' '),25,' '),
		rpad(nvl(to_char(dt_atendimento,'yyyy/mm/ddhh:mi:ss') || ds_meridiano_w,' '),21,' '),
		rpad(nvl(to_char(cd_excecao),' '),1,' '),
		rpad(nvl(to_char(ie_carater_atendimento),' '),1,' '),
		rpad(nvl(to_char(cd_cid),' '),6,' '),
		rpad(nvl(ie_paciente,' '),1,' '),
		rpad(nvl(ie_tipo_saida_spdat,' '),1,' '),
		lpad(nvl(ie_tipo_atendimento,'  '),2,'0'),
		rpad(decode(id_nota_principal,'S',' ',nvl(nr_guia_principal,' ')),20,' '),
		nr_sequencia,
		decode(dt_internacao,null,lpad(' ',21,' '),rpad(nvl(to_char(dt_internacao,'yyyy/mm/ddhh:mi:ss') || decode(dt_internacao,null,' ',ds_meridiano_w),' '),21,' ')),
		decode(dt_alta,null,lpad(' ',21,' '),rpad(nvl(to_char(dt_alta,'yyyy/mm/ddhh:mi:ss') || decode(dt_alta,null,' ',ds_meridiano_w),' '),21,' ')),
		rpad(nvl(to_char(dt_ultima_autoriz,'yyyymmdd'),' '),8,' '),
		nvl(tp_nota,'0'),
		rpad(nvl(id_nota_principal,'N'),1,' '),
		rpad(nvl(nr_ver_tiss,' '),7,' '),
		rpad(nvl(nr_guia_tiss_prestador,' '),20,' '),
		rpad(nvl(nr_guia_tiss_principal,' '),20,' '),
		rpad(nvl(nr_guia_tiss_operadora,' '),20,' '),
		rpad(nvl(tp_ind_acidente,'9'),1,'9'),
		rpad(nvl(motivo_encerram,' '),2,' '),
		lpad(nvl(nr_cnpj_cpf_req,'0'),14,'0'),
		rpad(nvl(ptu_somente_caracter_permitido(nm_prest_req,'ANS'),' '),70,' '),
		rpad(nvl(sg_cons_prof_req,' '),12,' '),
		rpad(nvl(nr_cons_prof_req,' '),15,' '),
		rpad(nvl(sg_uf_cons_req,' '),2,' '),
		lpad(nvl(nr_cbo_req,'0'),6,'0'),
		lpad(nvl(nr_fatura_glosada,'0'),11,'0'),
		lpad(nvl(nr_ndr_glosada,'0'),11,'0'),
		lpad(nvl(nr_lote_glosado,'0'),8,'0'),
		rpad(nvl(nr_nota_glosada,' '),11,' '),
		rpad(nvl(to_char(dt_protocolo,'yyyymmdd'),' '),8,' '),
		rpad(nvl(id_rn,'N'),1,' '),
		rpad(nvl(to_char(tp_consulta),' '),1,' '),
		nvl(tp_pessoa,' '),
		lpad(nvl(nr_cnpj_cpf,'0'),14,'0'),
		rpad(nvl(cd_cnes_cont_exec,'9999999'),7,'9'),
		rpad(nvl(cd_munic_cont_exec || substr(calcula_digito('MODULO10', cd_munic_cont_exec),1,1),'0'),7,'0')
	from	ptu_nota_cobranca
	where	nr_seq_serv_pre_pagto	= nr_seq_servico_p;
	
Cursor C02 is
	select	lpad(elimina_caractere_especial(a.nr_lote),8,'0') nr_lote_703,
		lpad(nvl(substr(elimina_caractere_especial(to_char(pls_obter_dados_conta(a.nr_nota,'G'))),1,20),'0'),20,'0') nr_nota_703,
		lpad(nvl(a.cd_unimed_hospital,'0'),4,'0'),
		lpad(nvl(a.cd_hospital,'0'),8,'0'),
		rpad(nvl(ptu_somente_caracter_permitido(a.nm_hospital,'ANS'),' '),70,' '),		
		rpad(nvl(a.ie_tipo_acomodacao,' '),2,' '),
		rpad(nvl(to_char(a.dt_internacao,'yyyy/mm/ddhh:mi:ss') || decode(a.dt_internacao,null,' ',ds_meridiano_w),' '),21,' '),
		rpad(nvl(to_char(a.dt_alta,'yyyy/mm/ddhh:mi:ss') || decode(a.dt_alta,null,' ',ds_meridiano_w),' '),21,' '),
		lpad(replace(replace(campo_mascara(a.tx_mult_amb,2),',',''),'.',''),4,'0'),
		lpad(a.ie_ind_acidente,1,' '),
		rpad(a.cd_motivo_saida,2,' '),
		lpad(nvl(a.cd_cgc_hospital,' '),14,' '),
		nvl(a.ie_tipo_internacao,'0'),
		lpad(nvl(a.qt_nasc_vivos,'0'),2,'0'),
		lpad(nvl(a.qt_nasc_mortos,'0'),2,'0'),
		lpad(nvl(a.qt_nasc_vivos_pre,'0'),2,'0'),
		lpad(nvl(a.qt_obito_precoce,'0'),1,'0'),
		lpad(nvl(a.qt_obito_tardio,'0'),1,'0'),
		lpad(nvl(a.ie_int_gestacao,'N'),1,' '),
		lpad(nvl(a.ie_int_aborto,'N'),1,' '),
		lpad(nvl(a.ie_int_transtorno,'N'),1,' '),
		lpad(nvl(a.ie_int_puerperio,'N'),1,' '),
		lpad(nvl(a.ie_int_recem_nascido,'N'),1,' '),
		lpad(nvl(a.ie_int_neonatal,'N'),1,' '),
		lpad(nvl(a.ie_int_baixo_peso,'N'),1,' '),
		lpad(nvl(a.ie_int_parto_cesarea,'N'),1,' '),
		lpad(nvl(a.ie_int_parto_normal,'N'),1,' '),
		to_char(nvl(a.ie_faturamento,'1')),
		nvl(to_char(a.ie_obito_mulher),'0'),
		rpad(nvl(a.nr_declara_obito,' '),17,' '),
		nvl(a.reg_internacao,'0')
	from	ptu_nota_hospitalar	a,
		ptu_nota_cobranca	c
	where	a.nr_seq_nota_cobr	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_nota_cobranca_w
	and	c.tp_nota 		= 3;
	
Cursor C03 is
	select	lpad(nvl(elimina_caractere_especial(a.nr_lote),'0'),8,'0'),
		decode(	lpad(nvl(substr(elimina_caractere_especial(to_char(pls_obter_dados_conta(a.nr_nota,'G'))),length(to_char(pls_obter_dados_conta(a.nr_nota,'G'))) - 10,length(to_char(pls_obter_dados_conta(a.nr_nota,'G')))),'0'),11,'0'),
			'00000000000',
			lpad(nvl(substr(elimina_caractere_especial(to_char(pls_obter_dados_conta(a.nr_nota,'G'))),1,11),'0'),11,'0'),
			lpad(nvl(substr(elimina_caractere_especial(to_char(pls_obter_dados_conta(a.nr_nota,'G'))),length(to_char(pls_obter_dados_conta(a.nr_nota,'G'))) - 10,length(to_char(pls_obter_dados_conta(a.nr_nota,'G')))),'0'),11,'0')),
		lpad(nvl(a.cd_unimed_prestador,'0'),4,'0'),
		lpad(nvl(a.cd_prestador,'0'),8,'0'),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(a.nm_prestador)),' '),70,' '),
		rpad(nvl(a.ie_tipo_participacao,'0'),1,'0'),
		rpad(nvl(to_char(a.dt_procedimento,'yyyymmdd'),' '),8,' '),
		rpad(nvl(to_char(a.ie_tipo_tabela),' '),1,' '),
		lpad(nvl(a.cd_servico,'0'),8,'0'),
		nvl(a.qt_procedimento,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_procedimento,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_custo_operacional,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_filme,2),',',''),'.',''),'0'),14,'0'),
		rpad(nvl(to_char(a.cd_porte_anestesico),' '),1,' '),
		lpad(nvl(a.cd_unimed_autorizadora,'0'),4,'0'),
		lpad(nvl(a.cd_unimed_pre_req,'0'),4,'0'),
		lpad(nvl(a.cd_prestador_req,'0'),8,'0'),
		lpad(nvl(a.ie_via_acesso,'0'),2,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_adic_procedimento,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_adic_co,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_adic_filme,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(a.cd_especialidade,'0'),2,'0'),
		lpad(nvl(a.ie_tipo_prestador,'0'),2,'0'),
		rpad(nvl(to_char(a.ie_rede_propria),' '),1,' '),
		rpad(nvl(to_char(a.ie_tipo_pessoa_prestador),' '),1,' '),
		lpad(nvl(a.nr_cgc_cpf,'0'),14,'0'),
		rpad(nvl(to_char(a.ie_pacote),' '),1,' '),
		rpad(nvl(to_char(a.cd_ato),' '),1,' '),
		lpad(nvl(a.tx_procedimento,'0'),3,'0'),
		lpad(nvl(a.nr_seq_nota,'0'),11,'0'),
		rpad(nvl(a.ds_hora_procedimento,' '),8,' '),
		rpad(nvl(a.cd_cnes_prest,'9999999'),7,'9'),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(a.nm_profissional_prestador)),' '),70,' '),
		rpad(nvl(a.sg_cons_prof_prest,' '),12,' '),
		rpad(nvl(a.nr_cons_prof_prest,' '),15,' '),
		rpad(nvl(a.sg_uf_cons_prest,' '),2,' '),
		lpad(nvl(a.nr_cgc_cpf_req,'0'),14,'0'),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(a.nm_prestador_requisitante)),' '),40,' '),
		rpad(nvl(a.sg_cons_prof_req,' '),12,' '),
		rpad(nvl(a.nr_cons_prof_req,' '),15,' '),
		rpad(nvl(a.sg_uf_cons_req,' '),2,' '),
		rpad(nvl(to_char(a.ie_reembolso),' '),1,' '),
		lpad(nvl(a.nr_autorizacao,'0'),10,'0'),
		lpad(nvl(replace(replace(campo_mascara(vl_pago_prest,2),',',''),'.',''),'0'),14,'0'),
		a.nr_sequencia,
		lpad(nvl(to_char(cd_pacote),'0'),8,'0'),
		rpad(nvl(nr_guia_tiss,' '),20,' '),
		rpad(nvl(to_char(nvl(tp_autoriz,1)),' '),1,' '),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(ds_servico)),' '),80,' '),
		rpad(nvl(a.hr_final,' '),8,' '),
		nvl(a.id_acres_urg_emer,'N'),
		lpad(nvl(a.nr_cbo_exec,'0'),6,'0'),
		nvl(a.tec_utilizada,'0'),
		rpad(nvl(to_char(a.dt_autoriz,'yyyymmdd'),' '),8,' '),
		rpad(nvl(to_char(a.dt_solicitacao,'yyyymmdd'),' '),8,' '),
		lpad(nvl(a.unidade_medida,'0'),3,'0'),
		rpad(nvl(a.nr_reg_anvisa,' '),15,' '),
		rpad(nvl(a.cd_munic || substr(calcula_digito('MODULO10', a.cd_munic),1,1),'0'),7,'0'),
		rpad(nvl(substr(a.cd_ref_material_fab,1,60),' '),60,' '),
		rpad(nvl(to_char(a.dt_pgto_prestador,'yyyymmdd'),' '),8,' '),
		rpad(nvl(nr_cnpj_fornecedor,'0'),14,'0'),
		b.nr_sequencia,
		rpad(nvl(a.cd_rec_prestador,'0'),10,'0')
	from	ptu_nota_servico	a,
		ptu_nota_cobranca	b
	where	a.nr_seq_nota_cobr	= b.nr_sequencia
	and	nvl(a.qt_procedimento,0) > 0
	and	b.nr_sequencia		= nr_seq_nota_cobranca_w;
	
Cursor C04 is
	select	lpad(nvl(elimina_caractere_especial(a.nr_lote),'0'),8,'0'),
		lpad(nvl(substr(elimina_caractere_especial(to_char(pls_obter_dados_conta(a.nr_nota,'G'))),length(to_char(pls_obter_dados_conta(a.nr_nota,'G'))) - 10,length(to_char(pls_obter_dados_conta(a.nr_nota,'G')))),'0'),11,'0'),
		nvl(to_char(a.ie_tipo_complemento),' '),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(replace(replace(a.ds_complemento,chr(13),''),chr(10),''))),' '),100,' '),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(replace(replace(a.especif_material,chr(13),''),chr(10),''))),' '),500,' ')
	from	ptu_nota_complemento	a,
		ptu_nota_cobranca	b
	where	a.nr_seq_nota_cobr	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_nota_cobranca_w;
	
Cursor C10 is
	select	rpad(nvl(b.nr_declara_vivo,' '),15,' '),
		rpad(nvl(b.cd_cid_obito,' '),6,' '),
		rpad(nvl(b.nr_declara_obito,' '),17,' ')
	from	ptu_nota_hospitalar	a,
		ptu_nota_hosp_compl	b,
		ptu_nota_cobranca	c
	where	a.nr_sequencia		= b.nr_seq_nota_hosp(+)
	and	a.nr_seq_nota_cobr	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_nota_cobranca_w;

begin
delete from  w_ptu_envio_arq where nm_usuario = nm_usuario_p;

-- Padr�o brasileiro � -3, Regi�es Sul, Sudeste e Nordeste, Goi�s, Distrito Federal, Tocantins, Amap� e Par�
ds_meridiano_w := '-03';

select	lpad(cd_unimed_destino,4,'0')
into	cd_cooperativa_w
from	ptu_servico_pre_pagto
where	nr_sequencia	= nr_seq_servico_p;

select	max(cd_cgc)
into	cd_cgc_w
from	pls_congenere
where	lpad(cd_cooperativa,4,'0') = cd_cooperativa_w;

select	max(sg_estado)
into	sg_estado_w
from	pessoa_juridica
where	cd_cgc = cd_cgc_w;

if	(sg_estado_w in ('RN','PB','PE','AL')) then
	ds_meridiano_w := '-02';
end if;

if	(sg_estado_w in ('MS','MT','RO','AC','AM','RR')) then
	ds_meridiano_w := '-04';
end if;

cd_operadora_origem_w	:= pls_obter_unimed_estab(cd_estabelecimento_p);

open C00;
loop
fetch C00 into	
	cd_unimed_destino_w,
	cd_unimed_origem_w,
	dt_geracao_w,
	nr_seq_geracao_w,
	dt_inicio_pagto_w,
	dt_fim_pagto_w,
	nr_versao_transacao_w;	
exit when C00%notfound;
	begin
	nr_seq_registro_w	:= nr_seq_registro_w + 1;

	ds_conteudo_w	:=	lpad(to_char(nr_seq_registro_w),8,'0') || '701' || cd_unimed_destino_w || cd_unimed_origem_w || dt_geracao_w || nr_seq_geracao_w ||
				dt_inicio_pagto_w || dt_fim_pagto_w || nr_versao_transacao_w;
	ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

	insert into w_ptu_envio_arq
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ds_conteudo,
		nr_seq_apres)
	values	(w_ptu_envio_arq_seq.nextval,
		sysdate,
		nm_usuario_p,
		ds_conteudo_w,
		nr_seq_registro_w);
		
	open C01;
	loop
	fetch C01 into
		nr_lote_w,
		nr_nota_w,
		cd_unimed_w,
		cd_usuario_plano_w,
		nm_beneficiario_w,
		dt_atendimento_w,
		cd_excecao_w,
		ie_carater_atendimento_w,
		cd_cid_w,
		ie_paciente_w,
		ie_tipo_saida_spdat_w,
		ie_tipo_atendimento_w,
		nr_guia_principal_w,
		nr_seq_nota_cobranca_w,
		dt_internacao_702_w,
		dt_alta_702_w,
		dt_ultima_autoriz_w,
		tp_nota_w,
		id_nota_principal_w,
		nr_ver_tiss_w,
		nr_guia_tiss_prestador_w,
		nr_guia_tiss_principal_w,
		nr_guia_tiss_operadora_w,
		tp_ind_acidente_w,
		motivo_encerram_w,
		nr_cnpj_cpf_req_w,
		nm_prest_req_w,
		sg_cons_prof_req_702_w,
		nr_cons_prof_req_702_w,
		sg_uf_cons_req_702_w,
		nr_cbo_req_w,
		nr_fatura_glosada_w,
		nr_ndr_glosada_w,
		nr_lote_glosado_w,
		nr_nota_glosada_w,
		dt_protocolo_w,
		id_rn_w,
		tp_consulta_w,
		tp_pessoa_w,
		nr_cnpj_cpf_w,
		cd_cnes_cont_exec_w,
		cd_munic_cont_exec_w;
	exit when C01%notfound;
		begin
		qt_registro_w		:= 0;
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		qt_tot_r702_w		:= qt_tot_r702_w + 1;
		
		select	count(1)
		into	qt_registro_w
		from	ptu_nota_hospitalar	a,
			ptu_nota_hosp_compl	b,
			ptu_nota_cobranca	c
		where	a.nr_sequencia		= b.nr_seq_nota_hosp
		and	a.nr_seq_nota_cobr	= c.nr_sequencia
		and	c.nr_sequencia		= nr_seq_nota_cobranca_w
		and	rownum			<= 1;
		
		begin
		select	dt_atendimento
		into	dt_atendimento_ww
		from	ptu_nota_cobranca
		where	nr_sequencia		= nr_seq_nota_cobranca_w;
		exception
		when others then
			dt_atendimento_ww	:= sysdate;
		end;
		
		/* OS 649133 - Em atendimentos ao TISS o campo tipo de sa�da ( IE_TIPO_SAIDA_SPDAT_W ) deve ser informado em atendimento com data superior a 31/08/2007 quando n�o houver interna��o. */	
		if	(qt_registro_w > 0) and (trunc(dt_atendimento_ww) <= trunc(to_date('31/08/2007', 'dd/mm/yyyy'))) or (somente_numero(nr_ver_tiss_w) >= '30000') then
			ie_tipo_saida_spdat_w := ' ';
		end if;
		
		if	(trim(nr_guia_principal_w) is null) then
			nr_guia_principal_w := lpad('0',20,'0');
		end if;
		
		ds_conteudo_w	:=	lpad(to_char(nr_seq_registro_w),8,'0') || '702' || nr_lote_w || lpad(' ',12,' ') || cd_unimed_destino_w || '   ' || 
					cd_usuario_plano_w || nm_beneficiario_w || dt_atendimento_w || lpad(' ',4,' ') || cd_cid_w || 
					lpad(' ',14,' ') || ie_paciente_w || ie_tipo_saida_spdat_w ||  ie_tipo_atendimento_w || lpad(' ',11,' ') || nr_nota_w ||
					nr_guia_principal_w || dt_internacao_702_w || dt_alta_702_w || tp_nota_w || id_nota_principal_w ||
					nr_ver_tiss_w || nr_guia_tiss_prestador_w || nr_guia_tiss_principal_w || nr_guia_tiss_operadora_w || tp_ind_acidente_w ||
					motivo_encerram_w || nr_cnpj_cpf_req_w || lpad(' ',40,' ') || sg_cons_prof_req_702_w || nr_cons_prof_req_702_w || sg_uf_cons_req_702_w ||
					lpad(nvl(nr_cbo_req_w,'0'),6,'0') || dt_protocolo_w || id_rn_w || ie_carater_atendimento_w || tp_consulta_w || tp_pessoa_w || nr_cnpj_cpf_w || 
					cd_cnes_cont_exec_w || cd_munic_cont_exec_w || nm_prest_req_w;
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo,
			nr_seq_apres)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w,
			nr_seq_registro_w);
			
		open C02;
		loop
		fetch C02 into
			nr_lote_703_w,
			nr_nota_703_w,
			cd_unimed_hospital_w,
			cd_hospital_w,
			nm_hospital_w,
			ie_tipo_acomodacao_w,
			dt_internacao_w,
			dt_alta_w,
			tx_mult_amb_w,
			ie_ind_acidente_w,
			cd_motivo_saida_w,
			cd_cgc_hospital_w,
			ie_tipo_internacao_w,
			qt_nasc_vivos_w,
			qt_nasc_mortos_w,			
			qt_nasc_vivos_pre_w,
			qt_obito_precoce_w,
			qt_obito_tardio_w,
			ie_int_gestacao_w,
			ie_int_aborto_w,
			ie_int_transtorno_w,
			ie_int_puerperio_w,
			ie_int_recem_nascido_w,
			ie_int_neonatal_w,
			ie_int_baixo_peso_w,
			ie_int_parto_cesarea_w,
			ie_int_parto_normal_w,
			ie_faturamento_w,
			ie_obito_mulher_w,
			nr_declara_obito_w,
			reg_internacao_w;
		exit when C02%notfound;
			begin
			nr_declara_vivo_1_w 	:= null;
			nr_declara_vivo_2_w 	:= null;
			nr_declara_vivo_3_w 	:= null;
			nr_declara_vivo_4_w 	:= null;
			nr_declara_vivo_5_w 	:= null;
			cd_cid_obito_w		:= null;			
			cd_cid_obito_1_w 	:= null;
			cd_cid_obito_2_w 	:= null;
			cd_cid_obito_3_w 	:= null;
			cd_cid_obito_4_w 	:= null;
			cd_cid_obito_5_w 	:= null;
			nr_declara_obito_1_w 	:= null;
			nr_declara_obito_2_w 	:= null;
			nr_declara_obito_3_w 	:= null;
			nr_declara_obito_4_w 	:= null;
			nr_declara_obito_5_w 	:= null;
			
			open C10;
			loop
			fetch C10 into	
				nr_declara_vivo_0_w,
				cd_cid_obito_0_w,
				nr_declara_obito_0_w;
			exit when C10%notfound;
				begin
				
				if	(nr_declara_vivo_1_w is null) then
					nr_declara_vivo_1_w := nr_declara_vivo_0_w;
				
				elsif	(nr_declara_vivo_2_w is null) then
					nr_declara_vivo_2_w := nr_declara_vivo_0_w;
					
				elsif	(nr_declara_vivo_3_w is null) then
					nr_declara_vivo_3_w := nr_declara_vivo_0_w;
					
				elsif	(nr_declara_vivo_4_w is null) then
					nr_declara_vivo_4_w := nr_declara_vivo_0_w;
				
				elsif	(nr_declara_vivo_5_w is null) then
					nr_declara_vivo_5_w := nr_declara_vivo_0_w;
				end if;
				
				if	(cd_cid_obito_1_w is null) then
					cd_cid_obito_1_w 	:= cd_cid_obito_0_w;
					cd_cid_obito_w		:= cd_cid_obito_0_w;
				
				elsif	(cd_cid_obito_2_w is null) then
					cd_cid_obito_2_w 	:= cd_cid_obito_0_w;
					cd_cid_obito_w		:= cd_cid_obito_0_w;
					
				elsif	(cd_cid_obito_3_w is null) then
					cd_cid_obito_3_w 	:= cd_cid_obito_0_w;
					cd_cid_obito_w		:= cd_cid_obito_0_w;
					
				elsif	(cd_cid_obito_4_w is null) then
					cd_cid_obito_4_w 	:= cd_cid_obito_0_w;
					cd_cid_obito_w		:= cd_cid_obito_0_w;
				
				elsif	(cd_cid_obito_5_w is null) then
					cd_cid_obito_5_w 	:= cd_cid_obito_0_w;
					cd_cid_obito_w		:= cd_cid_obito_0_w;
				end if;
				
				cd_cid_obito_w	:= rpad(substr(nvl(cd_cid_obito_w,' '),1,6),6,' ');
				
				if	(nr_declara_obito_1_w is null) then
					nr_declara_obito_1_w := nr_declara_obito_0_w;
				
				elsif	(nr_declara_obito_2_w is null) then
					nr_declara_obito_2_w := nr_declara_obito_0_w;
					
				elsif	(nr_declara_obito_3_w is null) then
					nr_declara_obito_3_w := nr_declara_obito_0_w;
					
				elsif	(nr_declara_obito_4_w is null) then
					nr_declara_obito_4_w := nr_declara_obito_0_w;
				
				elsif	(nr_declara_obito_5_w is null) then
					nr_declara_obito_5_w := nr_declara_obito_0_w;
				end if;				
				end;
			end loop;
			close C10;
			
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			qt_tot_r703_w		:= qt_tot_r703_w + 1;
			
			ds_conteudo_w	:= 	lpad(to_char(nr_seq_registro_w),8,'0') || 
						'703' || 
						nr_lote_703_w || 
						lpad(' ',11,' ') || 
						cd_unimed_hospital_w || 
						cd_hospital_w || 
						lpad(' ',25,' ') || 
						ie_tipo_acomodacao_w || 
						lpad(' ',42,' ') || 					
						tx_mult_amb_w || '   ' || 
						cd_cgc_hospital_w || 
						ie_tipo_internacao_w || 
						'  ' || 
						qt_nasc_vivos_w || 
						qt_nasc_mortos_w ||
						qt_nasc_vivos_pre_w || 
						qt_obito_precoce_w || 
						qt_obito_tardio_w || 
						ie_int_gestacao_w || 
						ie_int_aborto_w || 
						ie_int_transtorno_w ||
						ie_int_puerperio_w || 
						ie_int_recem_nascido_w || 
						ie_int_neonatal_w || 
						ie_int_baixo_peso_w || 
						ie_int_parto_cesarea_w || 
						ie_int_parto_normal_w ||
						ie_faturamento_w || 
						cd_cid_obito_w ||
						lpad(' ',7,' ') || 
						rpad(nvl(nr_declara_vivo_1_w,' '),15,' ') || 
						rpad(nvl(nr_declara_vivo_2_w,' '),15,' ') || 
						rpad(nvl(nr_declara_vivo_3_w,' '),15,' ') || 
						rpad(nvl(nr_declara_vivo_4_w,' '),15,' ') || 
						rpad(nvl(nr_declara_vivo_5_w,' '),15,' ') || 
						' ' || 
						nr_declara_obito_w || 
						rpad(nvl(cd_cid_obito_1_w,' '),6,' ') || 
						rpad(nvl(cd_cid_obito_2_w,' '),6,' ') || 
						rpad(nvl(cd_cid_obito_3_w,' '),6,' ') || 
						rpad(nvl(cd_cid_obito_4_w,' '),6,' ') || 
						rpad(nvl(cd_cid_obito_5_w,' '),6,' ') || 
						rpad(nvl(nr_declara_obito_1_w,' '),17,' ') || 
						rpad(nvl(nr_declara_obito_2_w,' '),17,' ') || 
						rpad(nvl(nr_declara_obito_3_w,' '),17,' ') || 
						rpad(nvl(nr_declara_obito_4_w,' '),17,' ') || 
						rpad(nvl(nr_declara_obito_5_w,' '),17,' ') || 
						nr_nota_703_w || 
						reg_internacao_w ||
						nm_hospital_w;
			ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			end;
		end loop;
		close C02;
		
		open C03;
		loop
		fetch C03 into
			nr_lote_704_w,
			nr_nota_704_w,
			cd_unimed_prestador_w,
			cd_prestador_w,
			nm_prestador_w,
			ie_tipo_participacao_w,
			dt_procedimento_w,
			ie_tipo_tabela_w,
			cd_procedimento_w,
			qt_procedimento_w,
			vl_procedimento_w,
			vl_custo_operacional_w,
			vl_filme_w,
			cd_porte_anestesico_w,
			cd_unimed_autorizadora_w,
			cd_unimed_pre_req_w,
			cd_prestador_req_w,
			ie_via_acesso_w,
			vl_adic_procedimento_w,
			vl_adic_co_w,
			vl_adic_filme_w,
			cd_especialidade_w,
			ie_tipo_prestador_w,
			ie_rede_propria_w,
			ie_tipo_pessoa_prestador_w,
			nr_cgc_cpf_w,
			ie_pacote_w,
			cd_ato_w,
			tx_procedimento_w,
			nr_seq_nota_w,
			ds_hora_procedimento_w,
			cd_cnes_prest_w,
			nm_profissional_prestador_w,
			sg_cons_prof_prest_w,
			nr_cons_prof_prest_w,
			sg_uf_cons_prest_w,
			nr_cgc_cpf_req_w,
			nm_prestador_requisitante_w,
			sg_cons_prof_req_w,
			nr_cons_prof_req_w,
			sg_uf_cons_req_w,
			ie_reembolso_w,
			nr_autorizacao_w,
			vl_pago_prest_w,
			nr_seq_nota_servico_w,
			cd_pacote_w,
			nr_gui_tiss_w,
			tp_autoriz_w,
			ds_servico_w,
			hr_final_w,
			id_acres_urg_emer_w,
			nr_cbo_exec_w,
			tec_utilizada_w,
			dt_autoriz_w,
			dt_solicitacao_w,
			unidade_medida_w,
			nr_reg_anvisa_w,
			cd_munic_w,
			cd_ref_material_fab_w,
			dt_pgto_prestador_w,
			nr_cnpj_fornecedor_w,
			nr_seq_nota_cobranca_ww,
			cd_rec_prestador_w;
		exit when C03%notfound;
			begin
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			qt_tot_r704_w		:= qt_tot_r704_w + 1;

			if	(cd_prestador_w is not null) then
				select	max(a.ie_prestador_alto_custo)
				into	ie_prestador_alto_custo_w
				from	pls_prestador a
				where	upper(a.cd_prestador) = upper(cd_prestador_w);
			end if;

			cd_prestador_w := lpad(cd_prestador_w,8,'0');

			-- Utiliza o c�digo de especialidade do PTU
			if	(cd_especialidade_w is not null) then
				select	max(cd_ptu)
				into	cd_especialidade_ptu_w
				from	especialidade_medica
				where	cd_especialidade = cd_especialidade_w;
				
				if	(cd_especialidade_ptu_w is not null) then
					cd_especialidade_w := lpad(nvl(cd_especialidade_ptu_w,'0'),2,'0');
				end if;
			end if;

			-- Mesma informa��o do registro 702
			nr_nota_704_w := nr_nota_w;

			-- O sistema deve seguir o padr�o de 4 casas para de inteiros e 4 para casas decimais, a representa��o da quantidade 1 � a seguinte --> '00010000'
			qt_procedimento_w	:= lpad(somente_numero(trunc(qt_procedimento_w)),4,'0') || rpad(somente_numero((qt_procedimento_w - trunc(qt_procedimento_w))),4,'0');

			if	(ie_carater_atendimento_w = '1') and (trim(hr_final_w) is null) then
				hr_final_w	:= ds_hora_procedimento_w;
			end if;

			-- Verifique a nota 00000000000235678160, lote 15421731. Sempre que houver um registro R703, no registro R704 as notas com participa��o H (hospital) devem ter os campos R704.CD_UNI_PRE+R704.CD_PREST = R703.CD_UNI_HOSP+R703.CD_HOSP
			--
			begin
			select	a.cd_unimed_hospital,
				a.cd_hospital
			into	cd_unimed_hospital_w,
				cd_hospital_w
			from	ptu_nota_hospitalar	a,
				ptu_nota_cobranca	c
			where	a.nr_seq_nota_cobr	= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_nota_cobranca_ww
			and	c.tp_nota 		= 3;
			exception
			when others then
				cd_unimed_hospital_w	:= null;
				cd_hospital_w		:= null;
			end;

			if	(nvl(ie_tipo_participacao_w,'X') = 'H') then
				cd_unimed_prestador_w	:= lpad(nvl(cd_unimed_hospital_w,cd_unimed_prestador_w),4,'0');
				cd_prestador_w		:= lpad(nvl(cd_hospital_w,cd_prestador_w),8,'0');
			end if;

			if	(ie_tipo_tabela_w = '2') or (ie_tipo_participacao_w = ' ') then
				ie_tipo_participacao_w	:= '0';
			end if;
	
			ds_conteudo_w	:=	lpad(to_char(nr_seq_registro_w),8,'0') || '704' || nr_lote_704_w || lpad(' ',11,' ') ||
						cd_unimed_prestador_w || cd_prestador_w || lpad(' ',40,' ') || ie_tipo_participacao_w || dt_procedimento_w ||
						ie_tipo_tabela_w || cd_procedimento_w || qt_procedimento_w || vl_procedimento_w || vl_custo_operacional_w ||
						vl_filme_w || cd_porte_anestesico_w || lpad(' ',13,' ') || cd_unimed_pre_req_w || cd_prestador_req_w || ie_via_acesso_w || 
						lpad(' ',42,' ') || cd_especialidade_w || dt_pgto_prestador_w || ie_tipo_prestador_w || ie_rede_propria_w || 
						ie_tipo_pessoa_prestador_w || nr_cgc_cpf_w || ie_pacote_w || 'S' || cd_ato_w || tx_procedimento_w || nr_seq_nota_w || 
						ds_hora_procedimento_w || lpad(' ',7,' ') || sg_cons_prof_prest_w || nr_cons_prof_prest_w || sg_uf_cons_prest_w || 
						lpad(' ',85,' ') || lpad(' ',40,' ') || ie_reembolso_w || nr_nota_704_w || hr_final_w || id_acres_urg_emer_w ||
						nr_cbo_exec_w || tec_utilizada_w || dt_autoriz_w || dt_solicitacao_w || unidade_medida_w || nr_reg_anvisa_w || 
						lpad(' ',37,' ') || lpad(' ',8,' ') || nr_cnpj_fornecedor_w || cd_rec_prestador_w || cd_ref_material_fab_w || nm_prestador_w ||
						nm_profissional_prestador_w;
			ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

			qt_tot_r704_ww	:= qt_tot_r704_ww + somente_numero(qt_procedimento_w);

			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
				
			update	ptu_nota_servico
			set	nr_seq_a500	= nr_seq_registro_w
			where	nr_sequencia	= nr_seq_nota_servico_w;
			end;
		end loop;
		close C03;
		
		open C04;
		loop
		fetch C04 into	
			nr_lote_705_w,
			nr_nota_705_w,
			ie_tipo_complemento_w,
			ds_complemento_w,
			especif_material_w;
		exit when C04%notfound;
			begin
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			qt_tot_r705_w		:= qt_tot_r705_w + 1;
			
			-- Mesma informa��o do registro 702
			nr_nota_705_w := nr_nota_w;

			ds_conteudo_w	:=	lpad(to_char(nr_seq_registro_w),8,'0') || '705' || nr_lote_705_w || lpad(' ',11,' ') || 
						ie_tipo_complemento_w || ds_complemento_w || nr_nota_705_w;
			ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			end;
		end loop;
		close C04;
		
		end;
	end loop;
	close C01;
			
	end;
end loop;
close C00;

-- R709 � TRAILER (OBRIGAT�RIO)
nr_seq_registro_w	:= nr_seq_registro_w + 1;

select	replace(nvl(ptu_obter_qt_registro_serv(nr_seq_servico_p,'VL_SERV'),'0'),',',''),
	replace(nvl(ptu_obter_qt_registro_serv(nr_seq_servico_p,'VL_CO'),'0'),',',''),
	replace(nvl(ptu_obter_qt_registro_serv(nr_seq_servico_p,'VL_FI'),'0'),',','')
into	vl_tot_serv_w,
	vl_tot_co_w,
	vl_tot_filme_w
from	dual;

ds_conteudo_w	:=	lpad(to_char(nr_seq_registro_w),8,'0') || '709' || lpad(to_char(qt_tot_r702_w),5,'0') || lpad(to_char(qt_tot_r703_w),5,'0') || 
			lpad(to_char(qt_tot_r704_w),5,'0') || lpad(to_char(qt_tot_r705_w),5,'0') || lpad(' ',10,' ') || lpad(to_char(qt_tot_r704_ww),11,'0') ||
			lpad(vl_tot_serv_w,14,'0') || lpad(vl_tot_co_w,14,'0') || lpad(vl_tot_filme_w,14,'0');
ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

insert into w_ptu_envio_arq
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_conteudo,
	nr_seq_apres)
values	(w_ptu_envio_arq_seq.nextval,
	sysdate,
	nm_usuario_p,
	ds_conteudo_w,
	nr_seq_registro_w);
	
-- R998 � Hash (OBRIGAT�RIO)
nr_seq_registro_w	:=	nr_seq_registro_w + 1;
ds_hash_w		:=	pls_hash_ptu_pck.obter_hash_txt(ds_arquivo_w); -- Gerar HASH
ds_conteudo_w		:=	lpad(nr_seq_registro_w,8,'0') || '998' || ds_hash_w;

insert into w_ptu_envio_arq
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_conteudo,
	nr_seq_apres)
values	(w_ptu_envio_arq_seq.nextval,
	sysdate,
	nm_usuario_p,
	ds_conteudo_w,
	nr_seq_registro_w);

commit;

end ptu_gerar_envio_arq_a700_v90;
/