create or replace
procedure sup_desvinc_kit_proc_interno(
			nr_seq_proc_p		Number,
			nm_usuario_p		Varchar2) is 

begin

update 	proc_interno
set 	cd_kit_material = null,
	nm_usuario 	= nm_usuario_p, 
	dt_atualizacao 	= sysdate
where 	nr_sequencia 	= nr_seq_proc_p;


commit;

end sup_desvinc_kit_proc_interno;
/