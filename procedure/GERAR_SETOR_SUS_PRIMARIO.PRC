create or replace procedure GERAR_SETOR_SUS_PRIMARIO(
    nm_usuario_p	varchar2,
    nr_atendimento_p	number,
    cd_unidade_compl_p	varchar2,
    cd_unidade_basica_p	varchar2,
    cd_setor_atendimento_p	number) is

count_w NUMBER(10,0);

BEGIN

SELECT COUNT(*) INTO count_w FROM atend_paciente_unidade WHERE nr_atendimento = nr_atendimento_p;

IF count_w = 0 THEN
	BEGIN
		Insert into atend_paciente_unidade 
		(
			NR_ATENDIMENTO,
			NR_SEQUENCIA,
			CD_SETOR_ATENDIMENTO,
			CD_UNIDADE_BASICA,
			CD_UNIDADE_COMPL,
			DT_ENTRADA_UNIDADE,
			DT_ATUALIZACAO,
			NM_USUARIO,
			NR_SEQ_INTERNO
		) values 
		(
			nr_atendimento_p,
			'1',
			cd_setor_atendimento_p,
			cd_unidade_basica_p,
			cd_unidade_compl_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			atend_paciente_unidade_seq.NextVal
		);
		commit;
	END;
END IF;
end GERAR_SETOR_SUS_PRIMARIO;
/