create or replace
procedure pls_gerar_lote_refaturamento(	nr_seq_lote_disc_p	pls_lote_discussao.nr_sequencia%type,
					dt_vencimento_p		date,
					ie_commit_p		varchar2,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

nr_seq_lote_faturamento_w	pls_lote_faturamento.nr_sequencia%type;
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
vl_fatura_w			pls_conta_pos_estabelecido.vl_beneficiario%type;
nr_seq_fatura_w			pls_fatura.nr_sequencia%type;
nr_seq_fatura_conta_w		pls_fatura_conta.nr_sequencia%type;
nr_seq_pls_fatura_w		pls_lote_contestacao.nr_seq_pls_fatura%type;
nr_seq_regra_fat_w		pls_lote_faturamento.nr_seq_regra_fat%type;
nr_seq_evento_w			pls_fatura_evento.nr_seq_evento%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
vl_beneficiario_w		pls_conta_pos_estabelecido.vl_beneficiario%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
ie_tipo_cobranca_w		pls_fatura_conta.ie_tipo_cobranca%type;
nr_seq_fatura_evento_w		pls_fatura_evento.nr_sequencia%type;
nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_mat.nr_sequencia%type;
nr_seq_fatura_proc_new_w	pls_fatura_proc.nr_sequencia%type;
nr_seq_fatura_mat_new_w		pls_fatura_mat.nr_sequencia%type;
nr_seq_conta_pos_w		pls_conta_pos_estabelecido.nr_sequencia%type;
nr_seq_congenere_w		pls_congenere.nr_sequencia%type;
nr_seq_grupo_prestador_w	pls_fatura.nr_seq_grupo_prestador%type;
ie_tipo_relacao_w		pls_fatura.ie_tipo_relacao%type;
nr_seq_cong_w			pls_segurado.nr_seq_congenere%type;
qt_registro_w			pls_integer;
ie_tipo_taxa_w			pls_conta_pos_estab_taxa.ie_tipo_taxa%type;
nr_seq_conta_pos_contab_w	pls_conta_pos_estab_contab.nr_sequencia%type;
vl_administracao_w		pls_conta_pos_estabelecido.vl_administracao%type;
nr_seq_disc_mat_w		pls_conta_pos_estabelecido.nr_seq_disc_mat%type;
nr_seq_disc_proc_w		pls_conta_pos_estabelecido.nr_seq_disc_proc%type;
nr_seq_fatura_mat_w		pls_fatura_mat.nr_sequencia%type;
nr_seq_fatura_proc_w		pls_fatura_proc.nr_sequencia%type;
ie_tipo_cobranca_ant_w		pls_fatura_conta.ie_tipo_cobranca%type;
nr_seq_pos_estab_taxa_w		pls_conta_pos_estab_taxa.nr_sequencia%type;
nr_seq_pos_taxa_contab_w	pls_conta_pos_taxa_contab.nr_sequencia%type;
ie_tipo_lote_w			pls_lote_faturamento.ie_tipo_lote%type;
	
Cursor C00 is
	select	c.nr_seq_evento
	from	pls_fatura_evento		c,
		pls_conta_pos_estabelecido	b, -- ps-estabelecido novo
		pls_fatura_conta		a
	where	b.nr_seq_conta			= a.nr_seq_conta
	and	a.nr_seq_fatura_evento		= c.nr_sequencia
	and	c.nr_seq_fatura			= nr_seq_pls_fatura_w
	and	b.nr_seq_lote_disc		= nr_seq_lote_disc_p
	and	b.ie_situacao			= 'A'
	and	nvl(b.ie_status_faturamento,'L') = 'L'
	and	exists	(select	1	-- ps-estabelecido original
			from	pls_conta_pos_estabelecido	d
			where	d.nr_seq_evento_fat	= c.nr_seq_evento
			and	d.nr_seq_lote_disc	is null
			and	(d.nr_seq_conta_mat	= b.nr_seq_conta_mat or d.nr_seq_conta_proc	= b.nr_seq_conta_proc))	
	group by c.nr_seq_evento;
	
Cursor C01 is
	select	b.nr_seq_conta,
		sum(b.vl_beneficiario),
		nvl(sum(b.vl_administracao), 0)
	from	pls_fatura_evento		c,
		pls_conta_pos_estabelecido	b,
		pls_fatura_conta		a
	where	b.nr_seq_conta			= a.nr_seq_conta
	and	a.nr_seq_fatura_evento		= c.nr_sequencia
	and	c.nr_seq_fatura			= nr_seq_pls_fatura_w
	and	b.nr_seq_lote_disc		= nr_seq_lote_disc_p
	and	c.nr_seq_evento			= nr_seq_evento_w
	and	nvl(ie_status_faturamento,'L')	= 'L'
	and	b.ie_situacao			= 'A'
	group by b.nr_seq_conta;
	
Cursor C02 is
	select	a.nr_sequencia,
		a.nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		a.vl_beneficiario,
		null ie_tipo_taxa,
		nvl(a.vl_administracao, 0) vl_administracao,
		a.nr_seq_disc_mat,
		a.nr_seq_disc_proc,
		null
	from	pls_conta_pos_estabelecido	a
	where	a.nr_seq_lote_disc		= nr_seq_lote_disc_p
	and	a.nr_seq_conta			= nr_seq_conta_w
	and	a.ie_situacao			= 'A'
	and	nvl(a.ie_status_faturamento,'L') = 'L'
	union
	select	a.nr_sequencia,
		a.nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		b.vl_taxa_manutencao,
		b.ie_tipo_taxa,
		0 vl_administracao,
		null nr_seq_disc_mat,
		null nr_seq_disc_proc,
		b.nr_sequencia
	from	pls_conta_pos_estabelecido	a,
		pls_conta_pos_estab_taxa	b
	where	a.nr_sequencia			= b.nr_seq_conta_pos_estab
	and	a.nr_seq_lote_disc		= nr_seq_lote_disc_p
	and	a.nr_seq_conta			= nr_seq_conta_w
	and	a.ie_situacao			= 'A'
	and	nvl(a.ie_status_faturamento,'L') = 'L'
	order by 1;
					
begin
-- Gerar o lote, evento e fatura para o refaturamento 
if	(nr_seq_lote_disc_p is not null) then
	select	max(a.nr_seq_pls_fatura)
	into	nr_seq_pls_fatura_w	
	from	pls_lote_discussao	b,
		pls_lote_contestacao	a
	where	a.nr_sequencia	= b.nr_seq_lote_contest
	and	b.nr_sequencia	= nr_seq_lote_disc_p;	

	-- Obter dados para o refaturamento
	if	(nr_seq_pls_fatura_w is not null) then
		select	max(c.nr_seq_regra_fat),
			max(a.nr_seq_pagador),
			max(a.nr_seq_congenere),
			max(a.nr_seq_grupo_prestador),
			max(a.ie_tipo_relacao),
			max(c.ie_tipo_lote)
		into	nr_seq_regra_fat_w,
			nr_seq_pagador_w,
			nr_seq_congenere_w,
			nr_seq_grupo_prestador_w,
			ie_tipo_relacao_w,
			ie_tipo_lote_w
		from	pls_lote_faturamento	c,
			pls_fatura_evento	b,
			pls_fatura		a
		where	c.nr_sequencia	= a.nr_seq_lote
		and	a.nr_sequencia	= b.nr_seq_fatura
		and	a.nr_sequencia	= nr_seq_pls_fatura_w;
	end if;
	
	if	(nr_seq_pagador_w is not null) and
		(nr_seq_pls_fatura_w is not null) then
		insert into pls_lote_faturamento
			(nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_mesano_referencia,
			nr_seq_lote_disc,
			nr_seq_regra_fat,
			dt_geracao,
			dt_fechamento,
			dt_vencimento,
			ie_status,
			ie_tipo_lote)
		values	(pls_lote_faturamento_seq.nextval,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_lote_disc_p,
			nr_seq_regra_fat_w,
			sysdate,
			sysdate,
			null,
			1,
			nvl(ie_tipo_lote_w, 'C')) returning nr_sequencia into nr_seq_lote_faturamento_w;
			
		pls_faturamento_pck.inicializa_geracao_lote(nr_seq_lote_faturamento_w, nm_usuario_p);
			
		select	sum(nvl(vl_beneficiario,0))
		into	vl_fatura_w
		from	pls_conta_pos_estabelecido
		where	nr_seq_lote_disc = nr_seq_lote_disc_p
		and	ie_situacao			= 'A'
		and	nvl(ie_status_faturamento,'L')	= 'L';
		
		select	pls_fatura_seq.nextval
		into	nr_seq_fatura_w
		from	dual;
		
		insert into pls_fatura
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_lote,
			dt_vencimento,
			vl_fatura,
			nr_seq_pagador,
			vl_total_ndc,
			nr_seq_congenere,
			nr_seq_grupo_prestador,
			ie_tipo_relacao,
			nr_fatura,
			dt_mes_competencia,
			ie_tipo_fatura)
		values	(nr_seq_fatura_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_lote_faturamento_w,
			dt_vencimento_p,
			vl_fatura_w,
			nr_seq_pagador_w,
			0,
			nr_seq_congenere_w,
			nr_seq_grupo_prestador_w,
			ie_tipo_relacao_w,
			nr_seq_fatura_w,
			sysdate,
			ie_tipo_lote_w);
		
		open C00;
		loop
		fetch C00 into	
			nr_seq_evento_w;
		exit when C00%notfound;
			begin			
			insert into pls_fatura_evento
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_fatura,
				nr_seq_evento,
				vl_evento)
			values	(pls_fatura_evento_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_fatura_w,
				nr_seq_evento_w,
				0) returning nr_sequencia into nr_seq_fatura_evento_w;
				
			-- Gerar a conta para o faturamento
			open C01;
			loop
			fetch C01 into	
				nr_seq_conta_w,
				vl_beneficiario_w,
				vl_administracao_w;
			exit when C01%notfound;
				begin			
				select	max(nr_seq_segurado)
				into	nr_seq_segurado_w
				from	pls_conta
				where	nr_sequencia = nr_seq_conta_w;	

				select	max(ie_tipo_cobranca)
				into	ie_tipo_cobranca_w
				from	pls_fatura_conta
				where	nr_seq_conta = nr_seq_conta_w;
				
				select	max(a.nr_seq_congenere)
				into	nr_seq_cong_w
				from	pls_segurado	a
				where	a.nr_sequencia	= nr_seq_segurado_w;
				
				insert into pls_fatura_conta
					(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_fatura_evento,
						nr_seq_conta,
						vl_faturado,
						nr_seq_segurado,
						ie_tipo_cobranca,
						nr_lote_contabil,
						vl_faturado_ndc,
						ie_tipo_vinculacao,
						nr_seq_congenere)
				values	(	pls_fatura_conta_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_fatura_evento_w,
						nr_seq_conta_w,
						vl_administracao_w,
						nr_seq_segurado_w,
						ie_tipo_cobranca_w,
						0, 
						vl_beneficiario_w, 
						'A', 
						nr_seq_cong_w) returning nr_sequencia into nr_seq_fatura_conta_w;
			
				update	pls_conta_pos_estabelecido
				set	nr_seq_lote_fat			= nr_seq_lote_faturamento_w,
					nr_seq_evento_fat		= nr_seq_evento_w,
					nr_seq_regra_evento_fat		= nr_seq_regra_fat_w,
					dt_atualizacao			= sysdate,
					nm_usuario			= nm_usuario_p
				where	nr_seq_lote_disc		= nr_seq_lote_disc_p
				and	nr_seq_conta			= nr_seq_conta_w
				and	nvl(ie_status_faturamento,'L')	= 'L'
				and	ie_situacao			= 'A';
				
				update	pls_conta_pos_estab_contab x
				set	x.nr_seq_evento_fat		= nr_seq_evento_w,
					x.nr_seq_regra_evento_fat	= nr_seq_regra_fat_w,
					x.nr_seq_lote_fat		= nr_seq_lote_faturamento_w,
					x.dt_atualizacao		= sysdate,
					x.nm_usuario			= nm_usuario_p
				where	x.nr_seq_conta_pos	in (	select	w.nr_sequencia
									from	pls_conta_pos_estabelecido	w
									where	w.nr_seq_lote_disc		= nr_seq_lote_disc_p
									and	w.nr_seq_conta			= nr_seq_conta_w
									and	nvl(ie_status_faturamento,'L')	= 'L'
									and	ie_situacao			= 'A');
				
				-- Gerar os itens para a conta do faturamento
				open C02;
				loop
				fetch C02 into
					nr_seq_conta_pos_w,
					nr_seq_conta_proc_w,
					nr_seq_conta_mat_w,
					vl_beneficiario_w,
					ie_tipo_taxa_w,
					vl_administracao_w,
					nr_seq_disc_mat_w,
					nr_seq_disc_proc_w,
					nr_seq_pos_estab_taxa_w;
				exit when C02%notfound;
					begin
					
					nr_seq_pos_taxa_contab_w := null;
					if	(nr_seq_pos_estab_taxa_w is not null) then
						select	max(nr_sequencia)
						into	nr_seq_pos_taxa_contab_w
						from	pls_conta_pos_taxa_contab
						where	nr_seq_pos_estab_taxa = nr_seq_pos_estab_taxa_w;
					end if;
					
					nr_seq_conta_pos_contab_w	:= null;
					ie_tipo_cobranca_ant_w		:= null;
					
					-- Inserir os procedimento
					if	(nr_seq_conta_proc_w is not null) then
						select	count(1)
						into	qt_registro_w
						from	pls_lote_faturamento d,
							pls_fatura c,
							pls_fatura_evento b,
							pls_fatura_conta a,
							pls_fatura_proc x
						where	d.nr_sequencia		= c.nr_seq_lote
						and	c.nr_sequencia		= b.nr_seq_fatura
						and	b.nr_sequencia		= a.nr_seq_fatura_evento
						and	a.nr_sequencia		= x.nr_seq_fatura_conta
						and	x.nr_seq_conta_pos_estab= nr_seq_conta_pos_w				
						and	d.nr_seq_lote_disc	= nr_seq_lote_disc_p
						and	d.nr_sequencia		= nr_seq_lote_faturamento_w
						and	x.ie_tipo_cobranca not in ('3','4');
						
						select	max(nr_sequencia)
						into	nr_seq_conta_pos_contab_w
						from	pls_conta_pos_estab_contab
						where	nr_seq_conta_pos	= nr_seq_conta_pos_w
						and	nr_seq_conta_proc	= nr_seq_conta_proc_w
						and	nr_seq_conta		= nr_seq_conta_w
						and	nr_seq_lote_fat		= nr_seq_lote_faturamento_w;
						
						if	(nr_seq_disc_proc_w is not null) then
							select	max(nr_seq_fatura_proc)
							into	nr_seq_fatura_proc_w
							from	pls_discussao_proc
							where	nr_sequencia	= nr_seq_disc_proc_w;
							
							if	(nr_seq_fatura_proc_w is not null) then
								select	ie_tipo_cobranca
								into	ie_tipo_cobranca_ant_w
								from	pls_fatura_proc
								where	nr_sequencia = nr_seq_fatura_proc_w;
							end if;
						end if;
					
						if	(qt_registro_w = 0) then						
							insert into pls_fatura_proc
								(	nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_fatura_conta,
									nr_seq_conta_proc,
									vl_faturado,
									nr_lote_contabil,
									ie_tipo_cobranca,
									nr_seq_conta_pos_estab,
									vl_faturado_ndc,
									nr_seq_conta_pos_contab,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_taxa_contab)
							values	(	pls_fatura_proc_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_fatura_conta_w,
									nr_seq_conta_proc_w,
									vl_administracao_w,
									0,
									nvl(ie_tipo_cobranca_ant_w,decode(ie_tipo_taxa_w,null,ie_tipo_cobranca_w,'1','4','2','3')),
									nr_seq_conta_pos_w, 
									vl_beneficiario_w,
									nr_seq_conta_pos_contab_w,
									nr_seq_pos_estab_taxa_w,
									nr_seq_pos_taxa_contab_w) returning nr_sequencia into nr_seq_fatura_proc_new_w;
						end if;
					end if;
					
					-- Inserir os materiais
					if	(nr_seq_conta_mat_w is not null) then
						select	count(1)
						into	qt_registro_w
						from	pls_lote_faturamento d,
							pls_fatura c,
							pls_fatura_evento b,
							pls_fatura_conta a,
							pls_fatura_mat x
						where	d.nr_sequencia		= c.nr_seq_lote
						and	c.nr_sequencia		= b.nr_seq_fatura
						and	b.nr_sequencia		= a.nr_seq_fatura_evento
						and	a.nr_sequencia		= x.nr_seq_fatura_conta
						and	x.nr_seq_conta_pos_estab= nr_seq_conta_pos_w				
						and	d.nr_seq_lote_disc	= nr_seq_lote_disc_p
						and	d.nr_sequencia		= nr_seq_lote_faturamento_w
						and	x.ie_tipo_cobranca not in ('3','4');
						
						select	max(nr_sequencia)
						into	nr_seq_conta_pos_contab_w
						from	pls_conta_pos_estab_contab
						where	nr_seq_conta_pos	= nr_seq_conta_pos_w
						and	nr_seq_conta_mat	= nr_seq_conta_mat_w
						and	nr_seq_conta		= nr_seq_conta_w
						and	nr_seq_lote_fat		= nr_seq_lote_faturamento_w;
						
						if	(nr_seq_disc_mat_w is not null) then
							select	max(nr_seq_fatura_mat)
							into	nr_seq_fatura_mat_w
							from	pls_discussao_mat
							where	nr_sequencia	= nr_seq_disc_mat_w;
							
							if	(nr_seq_fatura_mat_w is not null) then
								select	ie_tipo_cobranca
								into	ie_tipo_cobranca_ant_w
								from	pls_fatura_mat
								where	nr_sequencia = nr_seq_fatura_mat_w;
							end if;
						end if;
						
						if	(qt_registro_w = 0) then
							insert into pls_fatura_mat
								(	nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_fatura_conta,
									nr_seq_conta_mat,
									vl_faturado,
									nr_lote_contabil,
									ie_tipo_cobranca,
									nr_seq_conta_pos_estab,
									vl_faturado_ndc,
									nr_seq_conta_pos_contab,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_taxa_contab)
							values	(	pls_fatura_mat_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_fatura_conta_w,
									nr_seq_conta_mat_w,
									vl_administracao_w,
									0,
									nvl(ie_tipo_cobranca_ant_w,decode(ie_tipo_taxa_w,null,ie_tipo_cobranca_w,'1','4','2','3')),
									nr_seq_conta_pos_w, 
									vl_beneficiario_w,
									nr_seq_conta_pos_contab_w,
									nr_seq_pos_estab_taxa_w,
									nr_seq_pos_taxa_contab_w) returning nr_sequencia into nr_seq_fatura_mat_new_w;
						end if;
					end if;	
					end;
				end loop;
				close C02;
				
				end;
			end loop;
			close C01;
			
			end;
		end loop;
		close C00;
		
		delete	pls_fatura_conta z
		where	(not exists (select 1 from pls_fatura_proc p where p.nr_seq_fatura_conta = z.nr_sequencia)
		and	(not exists (select 1 from pls_fatura_mat m where m.nr_seq_fatura_conta = z.nr_sequencia)))
		and	z.nr_seq_fatura_evento in (	select	b.nr_sequencia
							from	pls_lote_faturamento d,
								pls_fatura c,
								pls_fatura_evento b
							where	d.nr_sequencia		= c.nr_seq_lote
							and	c.nr_sequencia		= b.nr_seq_fatura
							and	d.nr_seq_lote_disc	= nr_seq_lote_disc_p
							and	d.nr_sequencia		= nr_seq_lote_faturamento_w);
							
		pls_limpar_faturas_vazias_lote(nr_seq_lote_faturamento_w,cd_estabelecimento_p,nm_usuario_p);

		/*aaschlote 13/05/2014 - OS - 735275 - Coloquei essa procedure para atualizar o valor do evento*/
		pls_atualizar_vl_lote_fatura(nr_seq_lote_faturamento_w,nm_usuario_p, 'N', 'N');
		
		pls_faturamento_pck.altera_status_gerado_lote(nr_seq_lote_faturamento_w, nm_usuario_p);
	end if;	
end if;

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;


end pls_gerar_lote_refaturamento;
/