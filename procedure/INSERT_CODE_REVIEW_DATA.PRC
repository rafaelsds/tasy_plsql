create or replace procedure INSERT_CODE_REVIEW_DATA(	
			nr_pr_p					number,
			ds_author_p				varchar2,
          	ds_reviewer_p			varchar2,
          	ds_repo_p				varchar2,
			qt_linhas_add_p			number default 0,
			qt_linhas_del_p			number default 0,
			qt_comentarios_p		number,
			ds_branch_destino_p		varchar2,
			ds_branch_origem_p		varchar2,
			dt_code_review_p 		date default sysdate) is

nr_sequencia_w		number(10);	
				
begin


begin 

select 	nr_sequencia
into	nr_sequencia_w
from	COE_CODE_REVIEW_DATA
where	nr_pr = nr_pr_p
and 	ds_repo = ds_repo_p;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
   begin
      NR_SEQUENCIA_W := null;
   end;
end;

if	(nr_sequencia_w is not null) then 

	update 	coe_code_review_data
	set 	DS_PR_AUTHOR = ds_author_p,
			DS_REVIEWER = ds_reviewer_p,
			QT_LINHAS_ADD = qt_linhas_add_p,
			QT_LINHAS_DEL = qt_linhas_del_p,
			qt_comentarios = qt_comentarios_p
	where	NR_SEQUENCIA = NR_SEQUENCIA_W;

else

	insert into COE_CODE_REVIEW_DATA
		(NR_SEQUENCIA,
			NR_PR,
			DS_PR_AUTHOR,
			DS_REVIEWER,
			DS_REPO,
			QT_LINHAS_ADD,  
			QT_LINHAS_DEL,  
			QT_COMENTARIOS,
			DS_BRANCH_ORIGEM,
			DS_BRANCH_DESTINO,			
    		NM_USUARIO,
    		NM_USUARIO_NREC,
    		DT_ATUALIZACAO,
    		DT_ATUALIZACAO_NREC)
	values		
		(COE_CODE_REVIEW_DATA_SEQ.nextval,
			nr_pr_p,
			ds_author_p,
			ds_reviewer_p,
			ds_repo_p,
			qt_linhas_add_p,
			qt_linhas_del_p,
			qt_comentarios_p,
			ds_branch_origem_p,
			ds_branch_destino_p,
 			ds_author_p,
    		ds_author_p,
    		dt_code_review_p,
    		sysdate);

end if;

commit;

end INSERT_CODE_REVIEW_DATA;
/
