create or replace
procedure pls_consist_restr_manual_vend
			(	nr_seq_solicitacao_p	number,
				nr_seq_vendedor_p	number,
				nm_usuario_p		varchar2,
				ds_erro_p	out	varchar2) is

dt_solicitacao_w		date;
ie_origem_solicitacao_w		varchar2(10);
ds_erro_w			varchar2(255)	:= null;
nr_seq_restricao_w		number(10);
nr_seq_regra_restricao_w	number(10);
qt_dias_solic_inicial_w		number(10);
qt_dias_solic_final_w		number(10);
qt_dias_solicitacao_w		number(10);
qt_registros_w			pls_integer;
ie_consiste_w			varchar2(10);

Cursor C01 is
	select	a.nr_sequencia
	from	pls_restricao_vendedor	a
	where	sysdate	between nvl(a.dt_inicio_vigencia,sysdate) and nvl(a.dt_fim_vigencia,sysdate)
	and	a.nr_seq_restricao	= nr_seq_restricao_w
	and	exists	(	select	1
				from	pls_solic_regra_restricao	x
				where	x.nr_seq_regra		= a.nr_sequencia
				and	x.ie_origem_solicitacao	= ie_origem_solicitacao_w)
	order by decode(qt_dias_solic_inicial,null,1,-1),
		decode(qt_dias_solic_final,null,1,-1);

begin

if	(nvl(nr_seq_vendedor_p,0) <> 0) then
	select	dt_solicitacao,
		ie_origem_solicitacao
	into	dt_solicitacao_w,
		ie_origem_solicitacao_w
	from	pls_solicitacao_comercial
	where	nr_sequencia	= nr_seq_solicitacao_p;
	
	select	nr_seq_restricao
	into	nr_seq_restricao_w
	from	pls_vendedor
	where	nr_sequencia	= nr_seq_vendedor_p;
	
	ie_consiste_w	:= 'N';
	
	if	(nr_seq_restricao_w is not null) then
		select	count(1)
		into	qt_registros_w
		from	pls_restricao_vendedor
		where	sysdate	between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);
		
		if	(qt_registros_w > 0) then
			ie_consiste_w	:= 'S';
			
			open C01;
			loop
			fetch C01 into
				nr_seq_regra_restricao_w;
			exit when C01%notfound;
				begin
				
				select	qt_dias_solic_inicial,
					qt_dias_solic_final
				into	qt_dias_solic_inicial_w,
					qt_dias_solic_final_w
				from	pls_restricao_vendedor
				where	nr_sequencia	= nr_seq_regra_restricao_w;
				
				qt_dias_solicitacao_w	:= obter_dias_entre_datas(dt_solicitacao_w,sysdate);
				
				if	(qt_dias_solicitacao_w between nvl(qt_dias_solic_inicial_w,qt_dias_solicitacao_w) and nvl(qt_dias_solic_final_w,qt_dias_solicitacao_w)) then
					ie_consiste_w	:= 'N';
					exit;
				end if;	
				
				end;
			end loop;
			close C01;
		end if;
	end if;
	
	if	(ie_consiste_w = 'S') then
		ds_erro_w	:= wheb_mensagem_pck.get_texto(280714);
	end if;
end if;

ds_erro_p	:= ds_erro_w;

end pls_consist_restr_manual_vend;
/