create or replace
procedure gravar_doc_digitalizado_anexo
                         	(cd_pessoa_fisica_p		varchar2,
							 nr_atendimento_p		number,
							 ds_arquivo_p		varchar2,
							 doc_nome_p			varchar2,
							 nm_usuario_p		varchar2) is
					
begin				

	insert into doc_digitalizado_anexo(
       nr_sequencia,
       dt_atualizacao,
	   nm_usuario,
	   ds_arquivo,
	   doc_nome,
	   nm_usuario_nrec,
	   cd_pessoa_fisica,
	   nr_atendimento,
	   dt_atualizacao_nrec) 
	values(
       doc_digitalizado_anexo_seq.nextval,
       sysdate,
	   nm_usuario_p,
       ds_arquivo_p,
       doc_nome_p,
	   nm_usuario_p,
	   cd_pessoa_fisica_p,
	   nr_atendimento_p,
       sysdate);
	
	commit;
	
end gravar_doc_digitalizado_anexo;
/	
