Create or replace
PROCEDURE inserir_pessoa_chamada_painel(  nr_seq_agenda_p     NUMBER,
					  cd_pessoa_contato_p varchar2)
					IS


BEGIN


if	(nr_seq_agenda_p >  0) and (cd_pessoa_contato_p is not null) THEN
	UPDATE	agenda_paciente
	SET 	cd_pessoa_chamada_setor = cd_pessoa_contato_p 
	WHERE 	nr_sequencia = nr_seq_agenda_p;
	
END IF;

COMMIT;

END inserir_pessoa_chamada_painel;
/