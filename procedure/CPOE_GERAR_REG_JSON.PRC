create or replace procedure cpoe_gerar_reg_json(	nm_usuario_p		Varchar2,
								dt_referencia_p 	date default sysdate-1) is

Cursor C01 is
	select distinct nr_atendimento_w
	from (
		select	nr_atendimento nr_atendimento_w
		from	unidade_atendimento
		where	nr_atendimento is not null
		union
		select	a.nr_atendimento nr_atendimento_w
		from	atend_paciente_unidade a,
				setor_atendimento b,
				atendimento_paciente c
		where	a.cd_setor_atendimento = b.cd_setor_atendimento
		and		a.nr_atendimento = c.nr_atendimento
		and		a.dt_saida_interno = to_date('30/12/2999','dd/mm/yyyy')
		and		c.dt_alta_interno = to_date('30/12/2999','dd/mm/yyyy')
		union
		select	nr_atendimento
		from	atendimento_paciente
		where	dt_alta between trunc(dt_referencia_p) and fim_dia(dt_referencia_p)
		order by nr_atendimento_w) a
	where	(select count(1) 
			from cpoe_itens_json_v b
			where	a.nr_atendimento_w = b.nr_atendimento
			and	b.dt_prox_geracao > dt_referencia_p - 2) > 0
	order by nr_atendimento_w;

nr_hora_w			number(10);

begin
EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_LANGUAGE=''BRAZILIAN PORTUGUESE'''; 
EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_TERRITORY = ''BRAZIL''';
EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YY'''; 

nr_hora_w := to_number(to_char(dt_referencia_p,'hh24'));
if (nr_hora_w <= 12) then
	for c01_w in C01
	loop
		begin
		CPOE_Gerar_Registro_Json_PCK.CPOE_Gerar_Registros_Json(c01_w.nr_atendimento_w,dt_referencia_p,nm_usuario_p);
		exception
		when others then
			gravar_log_cpoe('CPOE_GERAR_REG_JSON - STACK: ' || dbms_utility.format_call_stack || ' ERRO: ' ||to_char(sqlerrm), c01_w.nr_atendimento_w);
		end;
		commit;
	end loop;
end if;

end cpoe_gerar_reg_json;
/
