create or replace
PROCEDURE Ageint_Gerar_necessidade_vaga(
			nr_seq_Agenda_p			Number,
			ie_solicitacao_p		varchar2,
			ie_tipo_vaga_p			varchar2,
			cd_acomodacao_desejada_p	number,
			cd_estabelecimento_p		number,
			nm_usuario_p			Varchar2,
			ie_tipo_agendamento_p		Varchar2,
			ds_retorno_p			out varchar2,
      qt_acompanhante_p Number default null
			) IS

cd_tipo_acomodacao_w	number(4);
cd_convenio_w		number(5);
cd_setor_atendimento_w	number(5);
ie_clinica_w		number(5);
nr_sequencia_w		number(10);
cd_pessoa_fisica_w	varchar2(10):=null;
cd_medico_resp_w	varchar2(10);
cd_estabelecimento_w	number(4);
ie_existe_w		varchar(1);
cd_categoria_w		varchar2(10);
ds_codigo_usuario_w	varchar2(30);
ds_complemento_w	varchar2(30);
ds_validade_w		varchar2(30);
dt_validade_w		date;
cd_plano_w		varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
VarGerarNecVagaSemAtend		varchar2(1):='S';
nr_seq_agenda_w 	number(10,0);
cd_usuario_logado_w	varchar2(255):=null;
VarAtualizaMedRespUsu	varchar2(1):='N';
VarGerarDataPrevista_w	varchar2(1) := 'N';
ie_reserva_vaga_w	varchar2(1)	:= 'N';
ie_consiste_tipo_acom_w varchar2(1);
dt_agenda_w		date;
qt_diarias_w		number(3,0);
ie_tipo_atend_w		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_classif_atend_w	atendimento_paciente.nr_seq_classificacao%type;


BEGIN

BEGIN

nr_seq_Agenda_w  := nr_seq_agenda_p;

if	(ie_tipo_agendamento_p = 'E') then
	select	b.cd_pessoa_fisica,
		b.cd_medico_exec,
		a.cd_estabelecimento,
		b.hr_inicio,
    b.ie_tipo_atendimento,
    (select c.nr_seq_classif_atend from agenda_paciente_auxiliar c where c.nr_seq_agenda = b.nr_sequencia) NR_SEQ_CLASSIF_ATEND
	into	cd_pessoa_fisica_w,
		cd_medico_resp_w,
		cd_estabelecimento_w,
		dt_agenda_w,
		ie_tipo_atend_w,
		nr_seq_classif_atend_w
	from	agenda a,
		agenda_paciente b
	where	b.nr_sequencia	= nr_seq_Agenda_w
	and	a.cd_agenda	= b.cd_agenda;
	
else 
	select	b.cd_pessoa_fisica,
		b.cd_medico,
		a.cd_estabelecimento,
		b.dt_agenda
	into	cd_pessoa_fisica_w,
		cd_medico_resp_w,
		cd_estabelecimento_w,
		dt_agenda_w
	from	agenda a,
		agenda_consulta b
	where	b.nr_sequencia	= nr_seq_Agenda_w
	and	a.cd_agenda	= b.cd_agenda;
end if;

EXCEPTION
WHEN others then
	cd_estabelecimento_w := cd_estabelecimento_p;
END;


-- Ivan em 21/05/2007 OS56547
select	consistir_solicitacao_vaga(cd_pessoa_fisica_w, cd_estabelecimento_p, nm_usuario_p, 0)
into	ie_existe_w
from 	dual;

select	max(qt_diaria_prev)
into	qt_diarias_w
from	agenda_integrada_item
where	nr_seq_agenda_cons = nr_seq_Agenda_w;

ie_consiste_tipo_acom_w := obter_se_acomodacao_desej_disp(cd_acomodacao_desejada_p, dt_agenda_w, nvl(cd_estabelecimento_w,cd_estabelecimento_p));
	
Obter_Param_Usuario(869, 121, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_reserva_vaga_w);	

IF	(ie_existe_w = 'N') then
	BEGIN

	if	(ie_tipo_agendamento_p = 'E') then
		select	cd_convenio,
			cd_categoria,
			cd_plano,
			cd_usuario_convenio,
			dt_validade_carteira
		into	cd_convenio_w,
			cd_categoria_w,
			cd_plano_w,
			ds_codigo_usuario_w,
			ds_validade_w
		from	agenda_paciente
		where	nr_sequencia	= nr_seq_agenda_p;
	else
		select	cd_convenio,
			cd_categoria,
			cd_plano,
			cd_usuario_convenio,
			dt_validade_carteira
		into	cd_convenio_w,
			cd_categoria_w,
			cd_plano_w,
			ds_codigo_usuario_w,
			ds_validade_w
		from	agenda_consulta
		where	nr_sequencia	= nr_seq_agenda_p;
	end if;
		
	
	IF	(ds_validade_w is not null) then
		dt_validade_w	:= to_date(ds_validade_w,'dd/mm/yyyy');
	END IF;


select	nvl(max(obter_valor_param_usuario(935, 92, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),'S'),
	nvl(max(obter_valor_param_usuario(916, 589, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),'N')
into	VarGerarNecVagaSemAtend,
	VarGerarDataPrevista_w
from	dual;

/* Obter param 45 da Gestao de Vagas
Abreiter em 12/02/2010
Se valor = 'S', o campo CD_MEDICO sera atualizado com o codigo de pessoa fisica do usuario logado no sistema
*/
select	nvl(max(obter_valor_param_usuario(1002, 45, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),'N')
into	VarAtualizaMedRespUsu
from	dual;

if upper(VarAtualizaMedRespUsu) = upper('S') then
	select 	max(cd_pessoa_fisica) 
	into	cd_usuario_logado_w
	from   	usuario 
	where  	upper(nm_usuario_p) = upper(nm_usuario);
end if;
	
	if	((ie_consiste_tipo_acom_w = 'S') or (ie_reserva_vaga_w = 'S')) then
		
		select	gestao_vaga_seq.NEXTVAL
		into	nr_sequencia_w
		from	dual;
	
		insert into gestao_vaga(
			nr_sequencia,
			cd_estabelecimento,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			dt_solicitacao,
			ie_solicitacao,
			cd_convenio,
			cd_categoria,
			ie_status,
			ie_tipo_vaga,
			cd_tipo_acomod_atual,
			cd_tipo_acomod_desej,
			cd_medico,
			ds_cod_usuario,
			ds_compl,
			dt_validade,
			cd_plano_convenio,
			dt_prevista,
			nr_seq_agenda,
			qt_dia,
			qt_acompanhante,
			ie_tipo_atendimento,
			nr_seq_classif_atend
			)
			values(
			nr_sequencia_w,
			cd_estabelecimento_w,
			cd_pessoa_fisica_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			ie_solicitacao_p,
			cd_convenio_w,
			cd_categoria_w,
			'A',
			ie_tipo_vaga_p,
			cd_tipo_acomodacao_w,
			cd_acomodacao_desejada_p,
			nvl(cd_usuario_logado_w,cd_medico_resp_w),
			ds_codigo_usuario_w,
			ds_complemento_w,
			dt_validade_w,
			cd_plano_w,
			decode(VarGerarDataPrevista_w,'S',sysdate,dt_agenda_w),
			nr_seq_Agenda_p,
			qt_diarias_w,
			qt_acompanhante_p,
			ie_tipo_atend_w,
			nr_seq_classif_atend_w
			);

		Gerar_Autor_Regra(null, null, null, null, null, null, 'GVE', nm_usuario_p, null,null, nr_sequencia_w,null,null,null,'','','');
	
	end if;

	END;
END IF;

if	((nr_sequencia_w > 0) and (ie_consiste_tipo_acom_w = 'S')) then
	ds_retorno_p := obter_desc_expressao(619451);
	

elsif  ((nr_sequencia_w > 0) and (ie_consiste_tipo_acom_w = 'N') and (ie_reserva_vaga_w = 'S')) then
	ds_retorno_p := wheb_mensagem_pck.get_texto(791085,'CD_TIPO_ACOMODACAO='||obter_desc_tipo_acomodacao(cd_acomodacao_desejada_p));
	
elsif ((ie_consiste_tipo_acom_w = 'N') and ( ie_reserva_vaga_w = 'N')) then
	ds_retorno_p := wheb_mensagem_pck.get_texto(791090,'CD_TIPO_ACOMODACAO='||obter_desc_tipo_acomodacao(cd_acomodacao_desejada_p));
end if;	


END Ageint_Gerar_necessidade_vaga;
/
