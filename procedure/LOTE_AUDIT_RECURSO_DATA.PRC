create or replace
procedure lote_audit_recurso_data(
			dt_envio_p		date,
			nr_seq_lote_p		number,
			nm_usuario_p		Varchar2) is 
begin
update	lote_audit_recurso 
set	dt_envio = dt_envio_p
where	nr_sequencia = nr_seq_lote_p;
commit;
end lote_audit_recurso_data;
/