create or replace
procedure lab_grava_dt_imp_ficha(
			nr_prescricao_p		number,
			nr_seq_prescr_p		number) is 

nr_seq_resultado_w	number(10);
			
begin

if 	(nr_prescricao_p is not null) then

	select	max(nr_seq_resultado)
	into	nr_seq_resultado_w
	from	exame_lab_resultado
	where	nr_prescricao = nr_prescricao_p;

	if	(nvl(nr_seq_prescr_p,0) = 0)	then
	
		update	exame_lab_result_item
		set		dt_impressao = sysdate
		where	nr_seq_resultado = nr_seq_resultado_w;

	else
	
		update	exame_lab_result_item
		set		dt_impressao = sysdate
		where	nr_seq_resultado = nr_seq_resultado_w
		and		nr_seq_prescr = nr_seq_prescr_p;
	
	end if;
		
end if;	

commit;

end lab_grava_dt_imp_ficha;
/