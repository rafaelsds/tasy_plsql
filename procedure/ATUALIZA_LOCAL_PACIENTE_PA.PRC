create or replace 
procedure atualiza_local_paciente_pa(nr_seq_local_p number,
                                     nr_atendimento_p number) is

begin

     if (nr_atendimento_p is not null) and
        (nr_seq_local_p is not null) then
     
     update atendimento_paciente
     set    nr_seq_local_pa  = nr_seq_local_p
     where  nr_atendimento = nr_atendimento_p;
     
	 commit;
	 
     end if;

end atualiza_local_paciente_pa;
/
