create or replace
procedure MUDA_STATUS_CONTA(	ie_status_p		varchar2,
				nr_sequencia_p		varchar2,
				nm_usuario_p		varchar2) is 
			
			
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;                           
ie_ctb_online_w			ctb_param_lote_nf.ie_ctb_online%type := 'N';	
cd_empresa_w            	empresa.cd_empresa%type;			

begin

update terceiro_conta
set ie_status_conta = ie_status_p
where nr_sequencia  = nr_sequencia_p;

commit;

begin
select	a.cd_estabelecimento
into	cd_estabelecimento_w
from	terceiro_conta a
where	a.nr_sequencia  = nr_sequencia_p;

cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_w);	
ie_ctb_online_w := ctb_online_pck.get_modo_lote( 9 , cd_estabelecimento_w, cd_empresa_w);
exception when others then
	ie_ctb_online_w	:= 'N';
end;
	 	 
if (ie_ctb_online_w = 'S') then
    begin
    /* Rotina de Contabilização de Controle de Terceiros */
    ctb_contab_onl_controle_terc( nr_sequencia_p,
			ie_status_p,
			nm_usuario_p);
    end;
end if;

end MUDA_STATUS_CONTA;
/
