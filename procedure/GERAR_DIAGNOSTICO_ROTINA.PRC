CREATE OR REPLACE
PROCEDURE Gerar_diagnostico_rotina(	
				nr_atendimento_p  		Number,
				dt_diagnostico_p		Date,
				nr_seq_cid_p			Number,
				nm_usuario_p			Varchar2,
				ie_liberado_p			varchar2 default null,
				dt_fim_p			Date	 default null,
				ds_observacao_p			varchar2 default null,
				ie_classif_diag_p		varchar2 default null,
				ie_lado_p			varchar2 default null,
				nr_seq_mentor_p	    		out number,
				nr_seq_interno_p			out number,
				NR_SEQ_ATEND_CONS_PEPA_P 	in ATEND_CONSULTA_PEPA.NR_SEQUENCIA%type default null) is

nr_seq_pend_regra_w			number(10);	
nr_atendimento_w			number(10);
nr_sequencia_w				number(10);
cd_pessoa_fisica_w			varchar2(10);
qt_idade_w					number(10);
cd_setor_atendimento_w		number(10);
cd_doenca_w					Varchar2(10);
cd_estabelecimento_w		number(5);
ie_classificacao_doenca_w	varchar2(1);
ie_classificacao_doenca_ww	varchar2(1);
ie_tipo_diagnostico_w		number(10);
ie_tipo_diagnostico_ww		number(10);
cd_medico_w					Varchar2(10);
nr_seq_etiologia_w			number(10);
nr_seq_interno_w			number(10);
nr_seq_mentor_w				number(10);
nr_seq_episodio_w			number(10);
nr_restrigir_regras_w   	varchar2(2000);
ds_comando_w				varchar2(2000);
ds_atrib_visao_w			tabela_visao_atributo.nm_atributo%type;
is_inpatient_w				varchar2(1 char) default 'N';

cursor classificacao_c is
	select 	ie_classificacao_diagnostico
	from 	classificacao_diagnostico
	where 	ie_valor_padrao = 'S'
	and 	ie_classificacao_diagnostico is not null;

BEGIN

if	(nvl(nr_atendimento_p,0)	= 0) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267634);
end if;

nr_seq_mentor_w := 0;

select	max(cd_doenca),
	max(ie_classificacao_doenca),
	max(nr_seq_etiologia),
	max(ie_tipo_diagnostico)
into	cd_doenca_w,
	ie_classificacao_doenca_ww,
	nr_seq_etiologia_w,
	ie_tipo_diagnostico_ww
from	cid_rotina
where	nr_sequencia	= nr_seq_cid_p; 

select	max(cd_medico)
into	cd_medico_w
from	diagnostico_medico
where	nr_atendimento = nr_atendimento_p
and	dt_diagnostico = dt_diagnostico_p;

cd_estabelecimento_w := obter_estabelecimento_ativo;
    
obter_param_usuario(281,259,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_classificacao_doenca_w);
obter_param_usuario(-980,1,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_tipo_diagnostico_w);

select	diagnostico_doenca_seq.nextval
into	nr_seq_interno_w
from	dual;


if(PKG_I18N.get_user_locale() = 'de_DE') then

	select 	max(nr_seq_episodio)
	into	nr_seq_episodio_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_p;

	if (obter_se_adic_diag(	cd_doenca_w,
							nvl(ie_classif_diag_p, nvl(nvl(ie_classificacao_doenca_ww,ie_classificacao_doenca_w),'S')), 
							ie_lado_p, 
							nvl(nvl(ie_tipo_diagnostico_ww,ie_tipo_diagnostico_w),2),
							nr_seq_episodio_w) = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(1061344,'CD_DOENCA_W=' || cd_doenca_w);
	end if;
	
	select	decode(obter_tipo_episodio(nr_seq_episodio_w),1,'S','N')
	into	is_inpatient_w
	from	dual;

end if;


Insert into diagnostico_doenca(
	nr_atendimento, 
	dt_diagnostico, 
	cd_doenca, 
	dt_atualizacao, 
	nm_usuario, 
	ds_diagnostico, 
	ie_classificacao_doenca,
	ie_tipo_diagnostico,
	cd_medico,
	nr_seq_etiologia,
	dt_liberacao,
	dt_fim,			
	ie_lado,
	ie_situacao,
	nr_seq_interno,
	nr_seq_rotina,
	ie_relevante_drg,
	nr_seq_atend_cons_pepa,
	IE_SIST_EXT_ORIGEM)
Values(	nr_atendimento_p,
	dt_diagnostico_p,
	cd_doenca_w,
	sysdate,
	nm_usuario_p,
	substr(ds_observacao_p,1,2000),
	nvl(ie_classif_diag_p, nvl(nvl(ie_classificacao_doenca_ww,ie_classificacao_doenca_w),'S')), -- Hudson Alt
	nvl(nvl(ie_tipo_diagnostico_ww,ie_tipo_diagnostico_w),2),
	cd_medico_w,
	nr_seq_etiologia_w,
	decode(ie_liberado_p,'S',sysdate),
	dt_fim_p,		
	ie_lado_p,
	'A',
	nr_seq_interno_w,
	nr_seq_cid_p,
	is_inpatient_w,
	decode(NR_SEQ_ATEND_CONS_PEPA_P, 0, null, NR_SEQ_ATEND_CONS_PEPA_P),
	'TASY');

nr_seq_interno_p := nr_seq_interno_w;


if(PKG_I18N.get_user_locale() = 'de_DE') then
	for r_conversao_w in classificacao_c loop
		begin
		
		if (r_conversao_w.ie_classificacao_diagnostico = 'DPE') then
				ds_atrib_visao_w := 'IE_DIAG_PRINC_EPISODIO';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'UBE') then
			ds_atrib_visao_w := 'IE_DIAG_REFERENCIA';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'BEH') then
			ds_atrib_visao_w := 'IE_DIAG_TRATAMENTO';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'AUF') then
			ds_atrib_visao_w := 'IE_DIAG_ADMISSAO';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'ENT') then
			ds_atrib_visao_w := 'IE_DIAG_ALTA';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'OPD') then
			ds_atrib_visao_w := 'IE_DIAG_CIRURGIA';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'TOD') then
			ds_atrib_visao_w := 'IE_DIAG_OBITO';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'POD') then
			ds_atrib_visao_w := 'IE_DIAG_PRE_CIR';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'FAC') then
			ds_atrib_visao_w := 'IE_DIAG_PRINC_DEPART';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'BHS') then
			ds_atrib_visao_w := 'IE_DIAG_TRAT_CERT';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'FAL') then
			ds_atrib_visao_w := 'IE_DIAG_CRONICO';
		elsif (r_conversao_w.ie_classificacao_diagnostico = 'ASV') then
			ds_atrib_visao_w := 'IE_DIAG_TRAT_ESPECIAL';		
		end if;
		
		if(ds_atrib_visao_w is not null)then
			ds_comando_w	:= 	'update diagnostico_doenca 
								 set '||ds_atrib_visao_w||' = ''S''
								 where nr_seq_interno = '||nr_seq_interno_w;
								 
			execute immediate ds_comando_w;
		end if;
		end;
	end loop;
end if;

commit;

if (nvl(ie_liberado_p,'N') = 'S') then

	GQA_Liberacao_diagnostico(nr_seq_interno_w,nm_usuario_p, nr_seq_mentor_w, nr_restrigir_regras_w);
	gera_protocolo_assistencial(nr_atendimento_p, nm_usuario_p);
    
	GERAR_PACIENTE_GRUPO (cd_doenca_w,obter_pessoa_atendimento(nr_atendimento_p,'C'),nm_usuario_p);
  
  ATUALIZAR_EV_LINHA_CUIDADO('CD', 'DIAGNOSTICO_DOENCA', nr_seq_interno_w, null, Obter_pessoa_atendimento(nr_atendimento_p,'C'), nm_usuario_p);
end if;

gerar_evento_diagnostico(nr_atendimento_p,nm_usuario_p,cd_doenca_w);

nr_seq_mentor_p := nr_seq_mentor_w;

END Gerar_diagnostico_rotina;
/
