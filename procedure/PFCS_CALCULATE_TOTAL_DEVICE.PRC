create or replace procedure PFCS_CALCULATE_TOTAL_DEVICE(
		nr_seq_indicator_p	number,
		cd_estabelecimento_p	varchar2,
		nm_usuario_p	varchar2) is
	
	qt_total_device					number(15) := 0;
	nr_seq_operational_level_w  	pfcs_operational_level.nr_sequencia%type;
	nr_seq_panel_w					pfcs_panel_detail.nr_seq_panel%type;
	pfcs_flag_settings          	number(15) := 0;
	
	ds_tl_dev_type	 				varchar2(10) := 'TL';
	ds_monitor_dev_type				varchar2(10) := 'Monitor';

begin

	nr_seq_operational_level_w := pfcs_get_structure_level(
		cd_establishment_p => cd_estabelecimento_p,
		ie_level_p => 'O',
		ie_info_p => 'C');

	select ie_table_origin into pfcs_flag_settings
	from pfcs_general_rule;
	
	
	/* Read from Tasy cursor*/
	if (pfcs_flag_settings = 0 or pfcs_flag_settings = 2) 
	then
		qt_total_device := pfcs_get_tele_total_device(ds_tl_dev_type, null, cd_estabelecimento_p);
		pfcs_pck.pfcs_generate_results(
			vl_indicator_p => qt_total_device,
			ds_reference_value_p => '',
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);
	end if;

	/* Read from PFCS Integration cursor*/
	if (pfcs_flag_settings = 1 or pfcs_flag_settings = 2) 
	then
		qt_total_device := pfcs_get_tele_device_from_intg(ds_monitor_dev_type, null, cd_estabelecimento_p);
		pfcs_pck.pfcs_generate_results(
			vl_indicator_p => qt_total_device,
			ds_reference_value_p => '',
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);

	end if;

	pfcs_pck.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

end PFCS_CALCULATE_TOTAL_DEVICE;
/