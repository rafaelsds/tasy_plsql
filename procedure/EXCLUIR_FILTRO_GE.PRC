create or replace
procedure excluir_filtro_ge ( 	nm_filtro_p		varchar2,
				nm_usuario_p		Varchar2) is 

begin

if (nm_filtro_p is not null) and
   (nm_usuario_p is not null) then

	delete from   funcao_filtro 
	where  cd_funcao = 942 
	and    ie_tipo_filtro = 'D' 
	and    nm_usuario_ref = nm_usuario_p
	and    nm_filtro = nm_filtro_p;

	commit;
   
end if;


end excluir_filtro_ge;
/