create or replace
procedure gerar_cnes_compl_pf
			(	cd_pessoa_fisica_p		number,
				ie_tipo_estabelecimento_p	varchar2,
				cd_cnes_p			varchar2,
				nr_seq_compl_pf_tel_adic_p	number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

ie_permite_cnes_duplic_w	varchar2(1)	:= 'N';
nr_seq_cnes_identific_w		number(15);
nr_seq_ident_cnes_w		number(10);
qt_cnes_adic_w			number(10)	:= 0;
qt_cnes_compl_w			number(10)	:= 0;
qt_cnes_pj_w			number(10)	:= 0;
qt_cnes_pj_ident_w		number(10)	:= 0;
qt_cnes_compl_pj_ident_w	number(10)	:= 0;

begin
select	nvl(max(ie_permite_cnes_duplic),'S')
into	ie_permite_cnes_duplic_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(nr_seq_compl_pf_tel_adic_p is not null) then
	select	max(nr_seq_ident_cnes)
	into	nr_seq_ident_cnes_w
	from	compl_pf_tel_adic
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	nr_sequencia		= nr_seq_compl_pf_tel_adic_p;
else
	select	max(nr_seq_ident_cnes)
	into	nr_seq_ident_cnes_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tipo_complemento	= 2;
end if;

if	(ie_permite_cnes_duplic_w = 'N') then
	select	count(*)
	into	qt_cnes_adic_w
	from	compl_pf_tel_adic	b,
		cnes_identificacao	a
	where	b.nr_seq_ident_cnes	= a.nr_sequencia
	and	a.cd_cnes		= to_char(cd_cnes_p);

	select	count(*)
	into	qt_cnes_compl_w
	from	compl_pessoa_fisica	b,
		cnes_identificacao	a
	where	b.nr_seq_ident_cnes	= a.nr_sequencia
	and	a.cd_cnes		= to_char(cd_cnes_p);
	
	select	count(*)
	into	qt_cnes_pj_w
	from	pessoa_juridica
	where	cd_cnes	= to_char(cd_cnes_p);
	
	select	count(*)
	into	qt_cnes_pj_ident_w
	from	pessoa_juridica		b,
		cnes_identificacao	a
	where	b.cd_cgc	= a.cd_cgc
	and	a.cd_cnes	= to_char(cd_cnes_p);
	
	select	count(*)
	into	qt_cnes_compl_pj_ident_w
	from	pessoa_juridica_compl	b,
		cnes_identificacao	a
	where	b.nr_seq_ident_cnes	= a.nr_sequencia
	and	a.cd_cnes		= to_char(cd_cnes_p);
	
	if	(qt_cnes_adic_w > 0) or
		(qt_cnes_compl_w > 0) or
		(qt_cnes_pj_w > 0) or
		(qt_cnes_pj_ident_w > 0) or
		(qt_cnes_compl_pj_ident_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(265399,'');
		--Mensagem: Nao pode ser vinculado o mesmo CNES a mais de uma pessoa!
	end if;
end if;

if	(nr_seq_ident_cnes_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265405,'');
	--Mensagem: Ja existe um CNES vinculado a este endereco!
else
	select	cnes_identificacao_seq.nextval
	into	nr_seq_cnes_identific_w
	from	dual;

	insert into cnes_identificacao
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_cnes,
		cd_pessoa_fisica,
		ie_tipo_estabelecimento)
	values	(nr_seq_cnes_identific_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_cnes_p,
		cd_pessoa_fisica_p,
		ie_tipo_estabelecimento_p);
		
	if	(nr_seq_compl_pf_tel_adic_p is not null) then
		update	compl_pf_tel_adic
		set	nr_seq_ident_cnes	= nr_seq_cnes_identific_w
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	nr_sequencia		= nr_seq_compl_pf_tel_adic_p;
	else
		update	compl_pessoa_fisica
		set	nr_seq_ident_cnes	= nr_seq_cnes_identific_w
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_tipo_complemento	= 2;
	end if;
end if;

commit;

end gerar_cnes_compl_pf;
/
