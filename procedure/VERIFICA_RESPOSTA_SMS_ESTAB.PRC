create or replace
procedure verifica_resposta_sms_estab(
					nm_usuario_p		Varchar2) is 
					
cd_estabelecimento_w	number(4);					

Cursor C01 is		
	select	distinct a.cd_estabelecimento		
	from	regra_envio_sms_agenda a
	where	a.cd_estabelecimento IS NOT NULL;	

begin

open C01;
loop
fetch C01 into		
	cd_estabelecimento_w;
exit when C01%notfound;

	wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_w);
	wheb_sms.verifica_resposta_sms(nm_usuario_p);

end loop;
close C01;

end verifica_resposta_sms_estab;
/