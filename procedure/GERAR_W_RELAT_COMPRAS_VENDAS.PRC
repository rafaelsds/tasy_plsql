create or replace
procedure gerar_w_relat_compras_vendas(	cd_estabelecimento_p			number,
					dt_mesano_referencia_p			date,
					cd_convenio_p				number,
					cd_categoria_p				number,
					nm_usuario_p				varchar2) is 

cd_material_w			number(10);
vl_compra_1_w			number(13,4);
vl_compra_2_w			number(13,4);
vl_compra_3_w			number(13,4);
vl_compra_4_w			number(13,4);
vl_compra_5_w			number(13,4);
vl_compra_6_w			number(13,4);
dt_mes_1_w			date;
dt_mes_2_w			date;
dt_mes_3_w			date;
dt_mes_4_w			date;
dt_mes_5_w			date;
dt_mes_6_w			date;
vl_custo_medio_w			number(13,4);
ds_marca_compra_w		varchar2(255);
cd_cnpj_w			varchar2(14);
vl_preco_venda_w			number(13,4);
vl_brasind_simpro_w		number(13,4);
ds_marca_venda_w		varchar2(255);
ds_versao_w			varchar2(255);
					
cursor c01 is
select	cd_material
from	material
where	ie_situacao = 'A'
and	ie_consignado <> '1';

begin

delete from w_rel_valor_compras_vendas
where nm_usuario = nm_usuario_p;

dt_mes_1_w	:= dt_mesano_referencia_p;
dt_mes_2_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p,-1,0);
dt_mes_3_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p,-2,0);
dt_mes_4_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p,-3,0);
dt_mes_5_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p,-4,0);
dt_mes_6_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p,-5,0);

open C01;
loop
fetch C01 into	
	cd_material_w;
exit when C01%notfound;
	begin
	
	vl_preco_venda_w		:= 0;
	ds_marca_venda_w		:= '';
	ds_versao_w			:= '';
	
	select	nvl(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_1_w, 'VU'),0) vl_compra_1,
		nvl(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_2_w, 'VU'),0) vl_compra_2,
		nvl(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_3_w, 'VU'),0) vl_compra_3,
		nvl(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_4_w, 'VU'),0) vl_compra_4,
		nvl(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_5_w, 'VU'),0) vl_compra_5,
		nvl(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_6_w, 'VU'),0) vl_compra_6,
		obter_desc_marca(obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_1_w, 'MA')) ds_marca_compra,
		obter_dados_maior_compra_mes(cd_estabelecimento_p, cd_material_w, dt_mes_1_w, 'PJ') cd_cnpj,
		obter_custo_medio_mat_mes_ref(cd_estabelecimento_p, 6, dt_mesano_referencia_p, cd_material_w)
	into	vl_compra_1_w,
		vl_compra_2_w,
		vl_compra_3_w,
		vl_compra_4_w,
		vl_compra_5_w,
		vl_compra_6_w,
		ds_marca_compra_w,
		cd_cnpj_w,
		vl_custo_medio_w
	from	dual;
	
	if	(cd_convenio_p > 0) and
		(cd_categoria_p > 0) then
	
		select	obter_preco_material(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, dt_mesano_referencia_p, cd_material_w, 0, 0, 0, null, null, null) vl_brasind_simpro,
			obter_dados_brasindice2(cd_estabelecimento_p, cd_material_w, dt_mesano_referencia_p, cd_convenio_p, 'DLAB', cd_categoria_p, null, null) ds_marca_venda,
			obter_dados_brasindice2(cd_estabelecimento_p, cd_material_w, dt_mesano_referencia_p, cd_convenio_p, 'VER', cd_categoria_p, null, null) ds_versao
		into	vl_preco_venda_w,
			ds_marca_venda_w,
			ds_versao_w
		from	dual;
		
		if	(ds_marca_venda_w is null) then
			select	obter_fabric_mat_simpro_estab2(cd_estabelecimento_p, cd_material_w, dt_mesano_referencia_p)
			into	ds_marca_venda_w
			from	dual;
		end if;
		
		if	(ds_versao_w is null) then
			select	obter_versao_simpro(cd_material_w, cd_convenio_p, cd_estabelecimento_p, dt_mesano_referencia_p)
			into	ds_versao_w
			from	dual;
		end if;			
	end if;

	insert into w_rel_valor_compras_vendas(
		cd_material,
		cd_cnpj,
		ds_marca_compra,
		vl_compra_1,
		vl_compra_2,
		vl_compra_3,
		vl_compra_4,
		vl_compra_5,
		vl_compra_6,
		vl_custo_medio,
		vl_preco_venda,
		ds_versao,
		ds_marca_venda,
		dt_atualizacao,
		nm_usuario)
	values(	cd_material_w,
		cd_cnpj_w,
		ds_marca_compra_w,
		vl_compra_1_w,
		vl_compra_2_w,
		vl_compra_3_w,
		vl_compra_4_w,
		vl_compra_5_w,
		vl_compra_6_w,
		vl_custo_medio_w,
		vl_preco_venda_w,
		ds_versao_w,
		ds_marca_venda_w,
		sysdate,
		nm_usuario_p);
	
	end;
end loop;
close C01;

commit;

end gerar_w_relat_compras_vendas;
/