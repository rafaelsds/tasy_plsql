create or replace procedure atualizar_dt_conf_agenda (nm_usuario_conf_p varchar2,
                                                     ie_alterar_status_p varchar2,
			     nr_seq_agenda_p number) is
begin

  if (ie_alterar_status_p = 'S') then
    update agenda_consulta
    set ie_status_agenda = 'CN',
        dt_confirmacao = sysdate,
        nm_usuario_confirm = nm_usuario_conf_p
    where nr_sequencia = nr_seq_agenda_p;
  else 
    update agenda_consulta
    set dt_confirmacao = sysdate,
        nm_usuario_confirm = nm_usuario_conf_p
    where nr_sequencia = nr_seq_agenda_p;
  end if;

commit;

end atualizar_dt_conf_agenda;
/