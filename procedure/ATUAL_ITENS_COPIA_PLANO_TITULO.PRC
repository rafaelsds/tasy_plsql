create or replace
procedure atual_itens_copia_plano_titulo(
			ie_manter_p		varchar2,
			ie_suspender_p		varchar2,
			ie_estender_p		varchar2,
			ie_pend_p		varchar2,
			nm_usuario_p		Varchar2) is 

begin

if	(nm_usuario_p <> '') then

	update 	w_copia_plano
	set    	ie_check_manter    = ie_manter_p,
		ie_check_suspender = ie_suspender_p,
		ie_check_estender  = ie_estender_p
	where	nm_usuario         = nm_usuario_p
	and	obter_se_item_hor_pendente(nr_prescricao, nr_seq_item, ie_tipo_item) = ie_pend_p
	and     ((ie_estender_p    <> 'S') or
		 (ie_permite       = 'S'));

	commit;
end if;


end atual_itens_copia_plano_titulo;
/