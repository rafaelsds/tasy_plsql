create or replace
procedure San_Atualizar_Qt_Atendida (	nr_seq_reserva_p	Number,
					nr_seq_item_p		Number,
					qt_volume_p		Number,
					nm_usuario_p		Varchar2) is 

qt_volume_w	Number(15,4);
					
begin
if (nr_seq_reserva_p is not null) and (nr_seq_item_p is not null) then

	select	nvl(qt_volume_atend,0) + qt_volume_p
	into	qt_volume_w
	from	san_reserva_item
	where	nr_seq_reserva = nr_seq_reserva_p
	and	nr_seq_item = nr_seq_item_p;

	update	san_reserva_item
	set	qt_volume_atend = qt_volume_w
	where	nr_seq_reserva = nr_seq_reserva_p
	and	nr_seq_item = nr_seq_item_p;
	
end if;

commit;

end San_Atualizar_Qt_Atendida;
/