create or replace
procedure agiq_atualizar_resposta(	nr_seq_ageint_resp_quest_p	number,
					nr_seq_resp_p			number,
					ds_resposta_p			varchar2,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number default null,
					nr_seq_ageint_p			number default null,
					ds_msg_bloqueio_p		out varchar2) is

v_count			number;
nr_seq_item_ant_w	ageint_resp_quest_item.nr_seq_item%type := -1;

begin
	update	ageint_resp_quest
	set	nr_seq_resp	= nr_seq_resp_p,
		ds_resposta	= ds_resposta_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_ageint_resp_quest_p;

	commit;
	
	ds_msg_bloqueio_p := null;
	
	for msg in (	select	distinct b.ds_alerta,
				bi.cd_agenda,
				i.nr_seq_item,
				substr(obter_item_grid_ageint(agi.nr_seq_proc_interno, agi.cd_medico, agi.cd_especialidade,1),1,255) ds_item
			from	ageint_resp_quest_item i,
				ageint_resp_quest q,
				ageint_quest_regra_bloq b,
				ageint_quest_utilizacao aqu,
				ageint_quest_regra_bl_item bi,
				agenda_integrada_item agi
			where	aqu.nr_sequencia = b.nr_seq_quest_utilizacao
			and	b.nr_seq_quest_utilizacao = i.nr_seq_quest_utilizacao
			and	b.nr_seq_estrutura = q.nr_seq_estrutura
			and	i.nr_seq_resp_quest = q.nr_sequencia
			and	b.nr_seq_resposta = q.nr_seq_resp
			and	agi.nr_sequencia = i.nr_seq_item
			and	b.ie_situacao = 'A'
			-- and b.ie_bloquear = 'S'
			and	b.nr_sequencia = bi.nr_seq_regra_bloq(+)
			and	i.nr_seq_resp_quest = nr_seq_ageint_resp_quest_p
			and	b.nr_seq_resposta = nr_seq_resp_p
			and	nvl(b.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
			order by nr_seq_item)

	loop
		if msg.cd_agenda is not null then
			select	count(1) 
			into	v_count
			from	ageint_marcacao_usuario u 
			where	u.nr_seq_ageint = nr_seq_ageint_p
			and	u.nr_seq_ageint_item = msg.nr_seq_item
			and	u.cd_agenda = msg.cd_agenda;
		else
			v_count := 1;
		end if;
		
		if v_count > 0 then     
			if (ds_msg_bloqueio_p is not null) then
				ds_msg_bloqueio_p := ds_msg_bloqueio_p || chr(10) || chr(10);
			end if;
			if (nr_seq_item_ant_w <> msg.nr_seq_item) then
				nr_seq_item_ant_w := msg.nr_seq_item;
				ds_msg_bloqueio_p := ds_msg_bloqueio_p || msg.ds_item || ':' || chr(10);
			end if;
			ds_msg_bloqueio_p := ds_msg_bloqueio_p || msg.ds_alerta;
		end if;    
	end loop;
end agiq_atualizar_resposta;
/
