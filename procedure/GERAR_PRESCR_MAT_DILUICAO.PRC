create or replace 
procedure Gerar_prescr_mat_diluicao(	nr_prescricao_p	number,
					nr_sequencia_p	number,
					nm_usuario_p	varchar2,
					cd_perfil_p	number) is

qt_dose_diferenciada_w		number(18,6);
cd_setor_atendimento_w		number(5,0);
nr_seq_via_acesso_w		number(10,0);
cd_estabelecimento_w		number(10,0);
ds_lista_restricao_w		varchar2(2000);
cd_material_w			number(10,0);
cd_material_sem_apresent_w	number(10,0);
qt_idade_w			Number(15,2);
qt_peso_w			number(6,3);
cd_unidade_medida_dose_w	varchar2(30);
qt_dose_medic_w			number(18,6);
ie_via_aplicacao_w		Varchar2(5);
cd_pessoa_fisica_w		Varchar2(50);
ds_dose_diferenciada_w		Varchar2(50);
ds_hora_w			Varchar2(5);
ds_dose_diferenciada_ww		Varchar2(50);
ds_horarios_padr_w		Varchar2(2000);
nr_seq_restricao_w		number(10,0);
ie_loop_w			number(10,0);
qt_minuto_aplicacao_w		number(10,0);
h				number(10,0);
k				number(10,0);
qt_diluicao_w			number(18,6);
cd_unid_med_diluente_w		varchar2(30);

cursor c01 is
select	nvl(qt_minuto_aplicacao,0),
	qt_diluicao,
	cd_unid_med_diluente
from	material_diluicao
where	((cd_setor_atendimento	= cd_setor_atendimento_w) 
or	(cd_setor_atendimento	is null))
and	((cd_setor_excluir is null) or (cd_setor_excluir <> cd_setor_atendimento_w))
and	obter_se_regra_diluicao_setor(nr_seq_interno, cd_setor_atendimento_w) = 'S'
and	((nr_seq_via_acesso is null) or (nr_seq_via_acesso = nr_seq_via_acesso_w))
and	cd_estabelecimento	= cd_estabelecimento_w
and	((obter_se_contido(nr_seq_restricao, ds_lista_restricao_w) = 'S') or
	 (nr_seq_restricao	is null))
and	ie_reconstituicao	= 'N'
and	cd_material		= nvl(cd_material_sem_apresent_w, cd_material_w)
and	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
and	((cd_unidade_medida is null) or
	 (cd_unidade_medida	= cd_unidade_medida_dose_w))
and	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida_dose_w,qt_dose_diferenciada_w),0) between 
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_min),0) and
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_max),9999999)
and	((ie_via_aplicacao	= ie_via_aplicacao_w) or
	(ie_via_aplicacao is null))
order by 	ie_via_aplicacao desc, 
		cd_setor_atendimento desc,
		qt_idade_min desc,
		qt_idade_max desc,
		nr_seq_prioridade desc,
		nr_seq_restricao desc;
	
cursor c03 is
select	nr_seq_restricao
from	paciente_rep_prescricao a
where	dt_liberacao	is not null
and	cd_pessoa_fisica = cd_pessoa_fisica_w
and	dt_inativacao is  null	
and	((a.dt_fim is null) or (sysdate between nvl(a.dt_inicio,sysdate-1) and a.dt_fim + 86399/86400));
	
begin

select	max(a.cd_setor_atendimento),
	max(a.cd_estabelecimento),
	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DIA')),
	nvl(max(a.qt_peso),0),
	max(b.cd_pessoa_fisica)
into	cd_setor_atendimento_w,
	cd_estabelecimento_w,
	qt_idade_w,
	qt_peso_w,
	cd_pessoa_fisica_w
from	pessoa_fisica b,
	prescr_medica a
where	a.nr_prescricao		= nr_prescricao_p
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

Select	nvl(cd_material,0),
	ie_via_aplicacao,
	cd_mat_sem_apresent,
	ds_dose_diferenciada,
	nr_seq_via_acesso,
	qt_dose,
	cd_unidade_medida_dose,
	ds_horarios
into	cd_material_w,
	ie_via_aplicacao_w,
	cd_material_sem_apresent_w,
	ds_dose_diferenciada_w,
	nr_seq_via_acesso_w,
	qt_dose_medic_w,
	cd_unidade_medida_dose_w,
	ds_horarios_padr_w
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia  = nr_sequencia_p
and	cd_material	is not null;
begin
open c03;
loop
fetch c03 into	
	nr_seq_restricao_w;
exit when c03%notfound;
	ds_lista_restricao_w	:= nr_seq_restricao_w  || ',' || ds_lista_restricao_w;
end loop;
close c03;

delete from prescr_mat_diluicao
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_material	= nr_sequencia_p;

commit;

if	(ds_dose_diferenciada_w is not null) then
	ds_dose_diferenciada_ww	:= ds_dose_diferenciada_w;
	ie_loop_w		:= 0;
	while	(ds_dose_diferenciada_ww is not null) and 
		(ie_loop_w < 101) LOOP
		begin
		
		select	instr(ds_dose_diferenciada_ww,'-')
		into	h
		from	dual;
		
		if	(h > 0) then
			qt_dose_diferenciada_w	:= replace(substr(ds_dose_diferenciada_ww, 1, h-1),'/',',');
			ds_dose_diferenciada_ww	:= substr(ds_dose_diferenciada_ww, h+1, 50);
		else
			qt_dose_diferenciada_w	:= replace(ds_dose_diferenciada_ww,'/',',');
			ds_dose_diferenciada_ww	:= null;
		end if;
		
		
		select	instr(ds_horarios_padr_w, ' ') 
		into	k
		from	dual;

		if	(k > 1) and
			(substr(ds_horarios_padr_w, 1, k -1) is not null) then
			ds_hora_w		:= substr(ds_horarios_padr_w, 1, k-1);
			ds_hora_w		:= replace(ds_hora_w, ' ','');
			ds_horarios_padr_w	:= substr(ds_horarios_padr_w, k + 1, 2000);
		elsif	(ds_horarios_padr_w is not null) then
			ds_hora_w 		:= replace(ds_horarios_padr_w,' ','');
			ds_horarios_padr_w	:= '';
		end if;
		
		if	(length(ds_hora_w) = 2) then
			ds_hora_w	:= ds_hora_w || ':00';
		end if;
		
		if	(nvl(qt_dose_diferenciada_w,0) > 0) then
		
			open c01;
			loop
			fetch c01 into
				qt_minuto_aplicacao_w,
				qt_diluicao_w,
				cd_unid_med_diluente_w;
			exit when c01%notfound;
				qt_minuto_aplicacao_w	:= qt_minuto_aplicacao_w;
				qt_diluicao_w		:= qt_diluicao_w;
				cd_unid_med_diluente_w	:= cd_unid_med_diluente_w;
			end loop;
			close c01;
		
			insert into prescr_mat_diluicao
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_material,
				qt_dose_diferenciada,
				qt_min_aplicacao,
				ds_hora,
				ie_ordem,
				qt_diluicao,
				cd_unid_med_diluente)
			values	(prescr_mat_diluicao_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_sequencia_p,
				nvl(qt_dose_diferenciada_w,0),
				qt_minuto_aplicacao_w,
				ds_hora_w,
				ie_loop_w,
				qt_diluicao_w,
				cd_unid_med_diluente_w);
		end if;
		ie_loop_w	:=  ie_loop_w + 1;
		end;
	end loop;
	
	commit;

end if;

exception
when others then
	null;
end;

end Gerar_prescr_mat_diluicao;
/