create or replace
procedure atualizar_vl_mat_autor_cir(
						cd_opcao_p			number,
						vl_material_p			number,
						nr_seq_mat_autor_cirurgia_p	number,
						ie_origem_preco_p		number,
						nm_usuario_p			varchar2) is 
/*
opcoes
1 -  grava o valor do material, valor unitario do material e origem do preco 
2 - grava o valor do material
*/
begin

if	(nr_seq_mat_autor_cirurgia_p	is not null
	and	vl_material_p		is not null) then

	if	(cd_opcao_p = 1) then
		update	material_autor_cirurgia
		set	vl_material		= nvl(qt_solicitada,0) * vl_material_p, 
			vl_unitario_material	= vl_material_p, 
			ie_origem_preco		= ie_origem_preco_p,
            nm_usuario = nm_usuario_p,
            dt_atualizacao = sysdate
		where	nr_sequencia		= nr_seq_mat_autor_cirurgia_p;
		
	elsif	(cd_opcao_p = 2) then
		update	material_autor_cirurgia
		set	vl_material	= DECODE(NVL(qt_material,0),0,Nvl(qt_solicitada,0),qt_material) * vl_material_p,
            nm_usuario = nm_usuario_p,
            dt_atualizacao = sysdate
		where	nr_sequencia	= nr_seq_mat_autor_cirurgia_p;
	end if;
	
end if;

commit;

end atualizar_vl_mat_autor_cir;
/
