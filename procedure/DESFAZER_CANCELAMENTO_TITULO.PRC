create or replace 
procedure desfazer_cancelamento_titulo (	nr_titulo_p	number,
					nm_usuario_p	varchar2) is

vl_titulo_w			number(15,2);
cd_moeda_w			number(5);
nr_sequencia_w			number(5);
vl_ultima_alteracao_w		number(15,2);
vl_saldo_w			number(15,2);

begin

select	vl_titulo,
	cd_moeda,
	vl_saldo_titulo
into	vl_titulo_w,
	cd_moeda_w,
	vl_saldo_w
from	titulo_receber
where	nr_titulo = nr_titulo_p;

select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	alteracao_valor
where	nr_titulo	= nr_titulo_p;

select	max(a.vl_alteracao)
into	vl_ultima_alteracao_w
from	alteracao_valor a
where	a.nr_sequencia	= nvl(nr_sequencia_w,0) - 2
and	a.nr_titulo	= nr_titulo_p;

insert	into alteracao_valor
	(NR_TITULO,
	DS_OBSERVACAO,
	NR_SEQUENCIA,
	DT_ALTERACAO,
	VL_ANTERIOR,
	VL_ALTERACAO,
	CD_MOTIVO,
	CD_MOEDA,
	IE_AUMENTA_DIMINUI,
	DT_ATUALIZACAO,
	NM_USUARIO,
	NR_LOTE_CONTABIL,
	NR_SEQ_TRANS_FIN)
values	(nr_titulo_p,
	substr(wheb_mensagem_pck.get_texto(303771),1,255),
	nr_sequencia_w,
	sysdate,
	vl_saldo_w,
	nvl(vl_ultima_alteracao_w,vl_titulo_w),
	3,
	cd_moeda_w,
	'D',
	sysdate,
	nm_usuario_p,
	null,
	null);

update	titulo_receber
set	vl_saldo_titulo	= nvl(vl_ultima_alteracao_w,vl_titulo_w),
	dt_liquidacao	= sysdate,
	ie_situacao	= '1',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_titulo		= nr_titulo_p;

Atualizar_Saldo_Tit_Rec(nr_titulo_p, nm_usuario_p);

end desfazer_cancelamento_titulo;
/