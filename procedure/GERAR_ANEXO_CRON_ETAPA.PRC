create or replace
procedure gerar_anexo_cron_etapa(	nr_seq_etapa_cron_p	Number,
									ds_arquivo_p		Varchar2,
									nm_usuario_p		Varchar2) is
begin
if (ds_arquivo_p is not null) then
insert	into proj_cron_etapa_anexo (nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_arquivo,
				nr_seq_etapa_cron)
	values (proj_cron_etapa_anexo_seq.nextval,
		sysdate,
		nm_usuario_p,
		ds_arquivo_p,
		nr_seq_etapa_cron_p);
end if;

commit;

end gerar_anexo_cron_etapa;
/
