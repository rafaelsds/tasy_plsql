create or replace 
procedure fin_abrir_mes
		(cd_estabelecimento_p	in	number,
		 dt_referencia_p		in	date,
		 nm_usuario_p		in	varchar2,
		 ie_cp_cr_p		in	varchar2) is

cont_w			number(10,0);
ie_forma_fechamento_w	varchar2(1)	:= 'M';
ie_copiar_regra_w	varchar2(1)	:= 'N';
nr_seq_mes_w		number(10);
nr_seq_anterior_w	number(10);
nr_dais_permite_w	number(10);

begin

if	(ie_cp_cr_p	= 'CR') then
	select	nvl(max(ie_forma_fechamento),'M')
	into	ie_forma_fechamento_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_p;
elsif	(ie_cp_cr_p	= 'CP') then
	select	nvl(max(ie_forma_fechamento),'M')
	into	ie_forma_fechamento_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= cd_estabelecimento_p;
elsif	(ie_cp_cr_p	= 'A') then
	select	nvl(max(ie_forma_fechamento),'M')
	into	ie_forma_fechamento_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_p;
	
	if	(ie_forma_fechamento_w = 'D') then
		select	nvl(max(ie_forma_fechamento),'M')
		into	ie_forma_fechamento_w
		from	parametros_contas_pagar
		where	cd_estabelecimento	= cd_estabelecimento_p;	
	end if;		
end if;

obter_param_usuario(5511,6,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_dais_permite_w);

if	(ie_forma_fechamento_w	= 'D') then
	select	count(*)
	into	cont_w
	from	fin_mes_ref
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	dt_referencia	= trunc(dt_referencia_p, 'dd')
	and	(ie_cp_cr		= ie_cp_cr_p or ie_cp_cr = 'A' or ie_cp_cr_p = 'A');
	
	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(219028);
		--r.aise_application_error(-20011, 'J� existe um dia de refer�ncia com esta data!');
	end if;

	if	(trunc(sysdate,'dd') < trunc(dt_referencia_p, 'dd') - nr_dais_permite_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(219029,'nr_dais_permite_w='||nr_dais_permite_w);
		--r.aise_application_error(-20011, 'Para abrir um novo dia refer�ncia � necess�rio estar no m�nimo nr_dais_permite_w dias antes do novo dia!');
	end if;	
else

	select	count(*)
	into	cont_w
	from	fin_mes_ref
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_referencia = pkg_date_utils.start_of(dt_referencia_p, 'MONTH', 0)
	and	(ie_cp_cr = ie_cp_cr_p or ie_cp_cr = 'A' or ie_cp_cr_p = 'A');
	
	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(219030);
		--r.aise_application_error(-20011, 'J� existe um m�s de refer�ncia com esta data!');
	end if;

	if	(trunc(sysdate,'dd') < pkg_date_utils.start_of(dt_referencia_p, 'MONTH', 0) - nr_dais_permite_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(219031,'nr_dais_permite_w='||nr_dais_permite_w);
		--r.aise_application_error(-20011, 'Para abrir um novo m�s de refer�ncia � necess�rio estar no m�nimo nr_dais_permite_w dias antes do novo m�s!');
	end if;	
end if;

select	fin_mes_ref_seq.nextval
into	nr_seq_mes_w
from	dual;

insert into fin_mes_ref
	(nr_sequencia,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_referencia,
	dt_abertura,
	nm_usuario_abertura,
	dt_fechamento,
	nm_usuario_fechamento,
	ie_cp_cr)
values (	nr_seq_mes_w,
	cd_estabelecimento_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	decode(ie_forma_fechamento_w,'D',trunc(dt_referencia_p,'dd'), pkg_date_utils.start_of(dt_referencia_p, 'MONTH', 0)),
	sysdate,
	nm_usuario_p,
	null,
	null,
	ie_cp_cr_p);

obter_param_usuario(5511,5,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_copiar_regra_w);

if	(ie_copiar_regra_w = 'S') then

	select	max(a.nr_sequencia)
	into	nr_seq_anterior_w
	from	fin_mes_ref a
	where	a.nr_sequencia		< nr_seq_mes_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p;

	insert	into fin_mes_ref_fora(
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_fin_mes,
		nr_sequencia,
		ie_tipo_titulo_cpa,
		cd_convenio,
		ie_baixa_retorno,
		ie_cancel_conta_pac,
		cd_perfil_lib,
		nm_usuario_lib)
	select	sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_mes_w,
		fin_mes_ref_fora_seq.nextval,
		ie_tipo_titulo_cpa,
		cd_convenio,
		ie_baixa_retorno,
		ie_cancel_conta_pac,
		cd_perfil_lib,
		nm_usuario_lib
	from	fin_mes_ref_fora a
	where	a.nr_seq_fin_mes	= nr_seq_anterior_w;

end if;

commit;

end fin_abrir_mes;
/