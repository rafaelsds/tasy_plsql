create or replace
procedure mprev_gerar_atend_prev_alta(	nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
					nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gera atendimentos Previstos na alta do participante da medicina preventiva se tiver
regra cadastrada na funcao  HDM - Cadastros Gerais\Regra geracao de atendimento previsto na alta.
- Procedure chamada pelas procedures GERAR_ESTORNAR_ALTA e SAIDA_SETOR_SERVICO.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_participante_w		mprev_participante.nr_sequencia%type;
ie_partic_programa_w		varchar2(1);
nr_seq_regra_w			varchar2(1);
nr_seq_programa_w		mprev_programa.nr_sequencia%type;
nr_seq_programa_partic_w	mprev_programa.nr_sequencia%type;
dt_alta_w			date;
nr_seq_classificacao_w		atendimento_paciente.nr_seq_classificacao%type;

/*Variaveis para o sql_pck*/
ds_sql_w	varchar2(1000);
ds_filtro_w	varchar2(500);
bind_sql_w	sql_pck.t_dado_bind;
cursor_w	sql_pck.t_cursor;

/*Busca todas as regras cadastradas para o tipo de atendimento*/
Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_programa,
		(sysdate + a.qt_dias) dt_prevista,
		a.ie_forma_atend_prev,
		a.nr_seq_motivo_agendamento,
		a.ie_gerar_ci,
		a.ie_gerar_email		
	from	mprev_atend_previsto_alta a
	where	a.ie_tipo_atendimento = ie_tipo_atendimento_w
	and		((a.nr_seq_classificacao = nr_seq_classificacao_w) or (a.nr_seq_classificacao is null));

begin

select	a.ie_tipo_atendimento,
	a.cd_pessoa_fisica,
	mprev_obter_participante(a.cd_pessoa_fisica),
	mprev_obter_se_part_programa(a.cd_pessoa_fisica,sysdate),
	a.dt_alta,
	a.nr_seq_classificacao
into	ie_tipo_atendimento_w,
	cd_pessoa_fisica_w,
	nr_seq_participante_w,
	ie_partic_programa_w,
	dt_alta_w,
	nr_seq_classificacao_w
from	atendimento_paciente a
where	a.nr_atendimento = nr_atendimento_p;

/*Se a pessoa fisica for participante da medicina preventiva e estiver em algum programa vai continuar*/
if	(nr_seq_participante_w is not null) and
	(ie_partic_programa_w = 'S') then

	/*Busca o dados da regra cadastrada para o tipo de atendimento*/		
	for r_C01 in C01 loop
		begin
		
		ds_filtro_w	:= null;
		
		if	(r_C01.nr_seq_programa is not null) then
			/*Adiciona a restricao ao sql caso existir programa na regra*/
			ds_filtro_w := ds_filtro_w || ' and b.nr_seq_programa = :nr_seq_programa_pc';
			sql_pck.bind_variable(':nr_seq_programa_pc', r_C01.nr_seq_programa, bind_sql_w);
					
		end if;

		/*Adiciona a restricao do participante para buscar apenas os programas do participante da pessoa fisica do atendimento*/
		ds_filtro_w := ds_filtro_w || ' and a.nr_sequencia = :nr_seq_participante_pc';
		sql_pck.bind_variable(':nr_seq_participante_pc', nr_seq_participante_w, bind_sql_w);

		/*Montagem do sql principal com as restricoes*/
		ds_sql_w :=	'select	b.nr_sequencia ' ||
				'from	mprev_participante a, mprev_programa_partic b ' ||
				'where	a.nr_sequencia = b.nr_seq_participante ' ||
				'and	sysdate between b.dt_inclusao and nvl(b.dt_exclusao, sysdate) ' || ds_filtro_w;

		/*Adiciona no select os valores das Bind e executa */	
		cursor_w := sql_pck.executa_sql_cursor(ds_sql_w, bind_sql_w);
				
		loop
			/*Abre o cursor e gera os ciclos de atendimento para os programas do participante. Se tiver programa cadastrado na regra 
			vai gerar apenas para ele. Caso nao tiver cadastro do programa na regra vai gerar para todos os programas do participante
			que estao ativos (sem data de exclusao ou com data de exclusao posterior a data atual).*/
			fetch 	cursor_w 
			into 	nr_seq_programa_partic_w;
			exit when cursor_w%notfound;
			
			insert into mprev_partic_ciclo_item(	nr_sequencia, ds_observacao, dt_atualizacao,
								dt_atualizacao_nrec, dt_cancelamento, dt_execucao,
								dt_prevista, dt_ultima_tent_contato, dt_ultimo_contato_sucesso,
								ie_cancel_troca_mod, ie_forma_atend_prev, ie_origem_atend_prev,
								ie_status, nm_usuario, nm_usuario_nrec,
								nr_atendimento_ciclo, nr_atend_origem, nr_seq_motivo_agendamento,
								nr_seq_motivo_desvio, nr_seq_part_canc_ativ, nr_seq_partic_ciclo_atend,
								nr_seq_plano_atend_item, nr_seq_programa_partic) 
							values (mprev_partic_ciclo_item_seq.nextval, null, sysdate,
								sysdate, null, null,
								r_C01.dt_prevista, null, null,
								null, r_C01.ie_forma_atend_prev, 'A',
								'P', nm_usuario_p, nm_usuario_p,
								null, nr_atendimento_p, r_C01.nr_seq_motivo_agendamento,
								null, null, null,
								null, nr_seq_programa_partic_w);

			commit;

			/*Envia CI e ou email aos profissionais responsaveis pelo programa*/
			if	(r_C01.ie_gerar_ci = 'S') or
				(r_C01.ie_gerar_email = 'S') then
				
				mprev_gerar_ci_email_alta(nr_seq_participante_w, nr_seq_programa_partic_w, r_C01.ie_gerar_ci,
							r_C01.ie_gerar_email, r_C01.dt_prevista, nr_atendimento_p,
							dt_alta_w, nm_usuario_p);
			
			end if;
			
		end loop;
		close cursor_w;

	end;
	end loop;

end if;

end mprev_gerar_atend_prev_alta;
/