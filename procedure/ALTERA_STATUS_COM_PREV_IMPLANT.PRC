create or replace
procedure altera_status_com_prev_implant(
		nr_sequencia_p	number,
		ie_opcao_p	varchar2,
		nm_usuario_p	varchar2) is

ie_permite_opcao_p	varchar2(1);
begin
if	(nr_sequencia_p is not null) and
	(ie_opcao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	(ie_opcao_p = 'L') then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_permite_opcao_p
		from	com_prev_implant
		where	dt_cancelamento is null
		and	dt_liberacao is null
		and	nr_sequencia 	= nr_sequencia_p;
		
		if	(ie_permite_opcao_p = 'S') then
			begin
			update	com_prev_implant
			set	dt_liberacao		= sysdate,
				nm_usuario_liberacao	= nm_usuario_p,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia 		= nr_sequencia_p;
			end;
		end if;
		end;		
	elsif	(ie_opcao_p = 'C') then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_permite_opcao_p
		from	com_prev_implant
		where	dt_cancelamento is null
		and	nr_sequencia 	= nr_sequencia_p;
		
		if	(ie_permite_opcao_p = 'S') then
			begin
			update	com_prev_implant
			set	dt_cancelamento		= sysdate,
				nm_usuario_cancelamento	= nm_usuario_p,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia 		= nr_sequencia_p;
			end;
		end if;
		end;
	end if;
	end;
end if;
commit;
end altera_status_com_prev_implant;
/