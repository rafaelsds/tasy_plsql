create or replace
procedure atend_inserir_pac_saida_hosp(
							nm_usuario_p		varchar2,	
							nr_atendimento_p	number,
							ds_observacao_p		varchar2,
							dt_saida_p          date,
							dt_chegada_p		date,
							ie_saida_p          varchar2) is 


nr_sequencia_min_w	number(10) := 0;
begin

select nvl(min(nr_sequencia),0)
into nr_sequencia_min_w
from atend_pac_saida_hosp
where nr_atendimento = nr_atendimento_p
and   dt_chegada is null;

if	(ie_saida_p = 'S') then  /*Verifica se � sa�da ou chegada*/
	begin

	if	not(nr_sequencia_min_w	>	0) then
		
		insert into atend_pac_saida_hosp(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_atendimento,
					ds_observacao,
					dt_saida,
					dt_chegada)
			values (atend_pac_saida_hosp_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_atendimento_p,
					substr(ds_observacao_p,1,255),
					dt_saida_p,
					dt_chegada_p);	
	else
		Wheb_mensagem_pck.exibir_mensagem_abort(262506);		
	end if;
				
	end;			
else
	begin
	
	if	(nr_sequencia_min_w	>	0) then

		update atend_pac_saida_hosp
		set dt_chegada = dt_chegada_p
		where	nr_atendimento = nr_atendimento_p
		and		nr_sequencia = nr_sequencia_min_w;
	
	else
		Wheb_mensagem_pck.exibir_mensagem_abort(262507);
   	end if;
	
	end;
end if;

commit;
end atend_inserir_pac_saida_hosp;
/
