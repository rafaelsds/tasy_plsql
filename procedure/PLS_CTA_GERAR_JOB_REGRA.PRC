create or replace
procedure pls_cta_gerar_job_regra(	ie_acao_p		varchar2,
					ds_rotina_p		varchar2,
					ds_execucao_p		varchar2,
					dt_execucao_p		date,
					dt_proxima_exec_p	varchar2,
					ie_situacao_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:		ROTINA CHAMADA DENTRO DE TRIGGER
-------------------------------------------------------------------------------------------------------------------
	ie_acao_p	I - Insert	U - Update	D - Delete
-------------------------------------------------------------------------------------------------------------------*/

jobno 			pls_integer;
nr_mes_w		pls_integer;
ds_rotina_w		varchar2(255);
ds_execucao_w		varchar2(255);

-- transformado o select em um cursor para que o mesmo possa eliminar tudo
cursor c01(	ds_rotina_pc	varchar2) is
	select	job
	from	all_jobs
	where	what like '%'||ds_rotina_pc||'%';

begin
-- inicia as vari�veis e faz o tratamento nos campos
ds_rotina_w := substr(upper(ds_rotina_p), 1, 255);
ds_execucao_w := substr(upper(ds_execucao_p), 1, 255);

-- se for inser��o ou altera��o
if	(ie_acao_p = 'I') or (ie_acao_p = 'U') then

	if	(ie_acao_p = 'U') then

		for r_c01_w in c01(ds_rotina_w) loop

			if	(r_c01_w.job is not null) then
				dbms_job.remove(r_c01_w.job);
			end if;
		end loop;
	end if;
	
	-- n�o retirar este if, o mesmo � verificado na chamada feita em uma regra que nela � informado se 
	-- est� ativa ou inativa, por este motivo n�o deve ser retirado
	if	(ie_situacao_p = 'A') then
		dbms_job.submit( jobno, ds_execucao_w, dt_execucao_p, dt_proxima_exec_p);
	end if;
-- quando for para excluir
elsif	(ie_acao_p = 'D') then
	
	for r_c01_w in c01(ds_rotina_w) loop
		if	(r_c01_w.job is not null) then
			dbms_job.remove(r_c01_w.job);
		end if;
	end loop;
end if;

-- o commit deve ser feito na rotina que est� chamando esta procedure
end pls_cta_gerar_job_regra;
/