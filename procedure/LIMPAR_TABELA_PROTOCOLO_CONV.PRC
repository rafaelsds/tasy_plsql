create or replace
procedure limpar_tabela_protocolo_conv is 

nr_seq_protocolo_w	number(10);
cd_convenio_w		number(5);
nm_usuario_w		varchar2(30);
dt_atualizacao_w	date;



cursor c01 is
	select	a.nr_seq_protocolo,
			a.cd_convenio,
			a.nm_usuario,
			a.dt_atualizacao
	from	protocolo_convenio a
	where	obter_dias_entre_datas	(a.dt_atualizacao,sysdate) > 3
	and		not exists (select 1 from conta_paciente b where b.nr_seq_protocolo = a.nr_seq_protocolo)
	and		ie_status_protocolo = 1;

begin

open C01;
loop
fetch C01 into	
	nr_seq_protocolo_w,
	cd_convenio_w,
	nm_usuario_w,
	dt_atualizacao_w	;
exit when C01%notfound;
	begin	
	begin
	delete	from protocolo_convenio
	where	nr_seq_protocolo = nr_seq_protocolo_w;
	commit;
	
	gravar_log_tasy(7469,	'Protocolo '||nr_seq_protocolo_w||' excluido atrav�s da procedure limpar_tabela_protocolo_conv' || chr(13) || chr(10) ||
							'Conv:' || cd_convenio_w ||'   Usu�rio:'||nm_usuario_w||'   Ultima atualizacao:'||to_char(dt_atualizacao_w,'dd/mm/yyyy hh24:mi:ss'),'tasy');
	exception
	when others then
		gravar_log_tasy(7469,	' Erro ao excluir protocolo '||nr_seq_protocolo_w||' - procedure limpar_tabela_protocolo_conv' || chr(13) || chr(10) || SQLERRM,'tasy');
	end;
	
	end;
end loop;
close C01;

end limpar_tabela_protocolo_conv;
/