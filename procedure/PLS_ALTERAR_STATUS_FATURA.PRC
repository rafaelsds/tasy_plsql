create or replace
procedure PLS_ALTERAR_STATUS_FATURA(ie_impedimento_cobranca_p	Varchar2,
				nr_seq_fatura_p		Varchar2,
				nm_usuario_p		Varchar2) is 

begin
	if	(nm_usuario_p is not null) then
		begin
			update  pls_fatura 
			set	   ie_impedimento_cobranca = ie_impedimento_cobranca_p 
			where  	nr_sequencia = nr_seq_fatura_p;
		end;
	end if;
	

commit;

end PLS_ALTERAR_STATUS_FATURA;
/