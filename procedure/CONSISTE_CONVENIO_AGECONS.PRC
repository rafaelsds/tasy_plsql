create or replace
procedure consiste_convenio_agecons (
		nr_seq_destino_p	number,
		cd_agenda_p		number,
		cd_convenio_p		number,
		dt_agenda_p		date,
		nm_usuario_p		varchar2) is 

ie_regra_w		number(10);
ie_permite_agendar_w	varchar2(1);

begin
if	(cd_agenda_p is not null) and
	(cd_convenio_p is not null) then
	select	max(nr_sequencia)
	into	ie_regra_w
	from	agenda_regra_conv_dia
	where	cd_agenda	= cd_agenda_p
	and	cd_convenio	= cd_convenio_p;

	if	(ie_regra_w > 0) then
		select	decode(count(nr_sequencia), 0, 'N', 'S')
		into	ie_permite_agendar_w
		from	agenda_regra_conv_dia
		where	cd_agenda	= cd_agenda_p
		and	cd_convenio	= cd_convenio_p
		and	trunc(dt_agenda_p)	<= (trunc(sysdate) + qt_dias_regra);
		
		if	(ie_permite_agendar_w = 'N') then
			if	(nr_seq_destino_p is not null) and
				(nr_seq_destino_p <> 0) then
				liberar_horario_agenda_cons(nr_seq_destino_p, nm_usuario_p);
			end if;
			
			wheb_mensagem_pck.exibir_mensagem_abort(237331, 'CD_CONVENIO_P='||obter_desc_convenio(cd_convenio_p));
		end if;
	end if;
end if;

end consiste_convenio_agecons;
/