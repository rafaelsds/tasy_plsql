CREATE OR REPLACE
PROCEDURE Gerar_EIS_Reinternacao_UTI(	
	dt_parametro_P		DATE,
	nm_usuario_p		Varchar2) IS 


nr_sequencia_w		Number(10,0);
cd_estabelecimento_w		NUMBER(4)     := 1;
cd_setor_atendimento_w	NUMBER(5);
nr_atendimento_w		NUMBER(10);
ie_sexo_w			Varchar2(1);
qt_idade_w			Number(03,0);		
qt_cirurgia_eletiva_w	Number(03,0);
qt_cirurgia_Urgencia_w	Number(03,0);
qt_obito_w			Number(03,0);
dt_entrada_w			Date;
dt_saida_w			Date;
qt_min_perm_w			Number(07,0);
qt_apache_w			Number(07,3);
pr_risco_w			Number(07,3);

dt_parametro_w		Date;
dt_parametro_fim_w		Date;
dt_saida_old_w			date;
ie_inserir_w			boolean;
qt_reg_w			number(10);
dt_entrada_atend_w		date;

Cursor C01 is
	select  t.nr_atendimento,
		nvl(p.ie_sexo,'I'),
		obter_idade(p.dt_nascimento, t.dt_alta,'A') qt_idade,
		decode(m.ie_obito,'S',1,0) qt_obito,
		trunc(t.dt_entrada)
	from	Pessoa_fisica p,
		motivo_alta m, 
		Atendimento_paciente t
	where	t.dt_entrada between dt_parametro_w and dt_parametro_fim_w
	and	t.cd_pessoa_fisica	= p.cd_pessoa_fisica
	and	t.cd_motivo_alta	= m.cd_motivo_alta
	and	exists
		(select count(*)
		from    Setor_atendimento b,
			atend_paciente_unidade a
		where   a.nr_atendimento        = t.nr_atendimento
		and   a.cd_setor_atendimento  = b.cd_setor_atendimento
		and		a.ie_passagem_setor = 'N'
		and   b.cd_classif_setor = 4
		having count(*)	>= 2);
 
 
Cursor C02 IS
 	select		dt_entrada_unidade,
				dt_saida_unidade
        from	Setor_atendimento b,
				atend_paciente_unidade a
        where	a.nr_atendimento        = nr_atendimento_w
		and		a.ie_passagem_setor = 'N'
        and		a.cd_setor_atendimento  = b.cd_setor_atendimento
        and		b.cd_classif_setor = 4
		order by dt_entrada_unidade asc;


BEGIN
Gravar_Log_Indicador(806, 'Reinternação UTI', sysdate, trunc(dt_parametro_p), nm_usuario_p, nr_sequencia_w);

dt_parametro_w		:=  trunc(dt_parametro_p,'month');
dt_parametro_fim_w	:= last_day(Trunc(dt_parametro_p,'month')) + 86399/86400;

delete from eis_paciente_uti
where dt_referencia	between dt_parametro_w and dt_parametro_fim_w;
COMMIT;

delete from EIS_REINTERNACAO_UTI
where dt_referencia	between dt_parametro_w and dt_parametro_fim_w;
COMMIT;

OPEN C01;
LOOP
FETCH C01 into 
	nr_atendimento_w,
	ie_sexo_w,
	qt_idade_w,
	qt_obito_w,
	dt_entrada_atend_w;
EXIT WHEN C01%NOTFOUND;
	begin

	ie_inserir_w	:= false;
	dt_saida_old_w	:= null;
	open C02;
	loop
	fetch C02 into	
		dt_entrada_w,
		dt_saida_w;
	exit when C02%notfound;
		begin
		
		if	(dt_saida_old_w	is not null) and
			(dt_saida_old_w > (dt_entrada_w - 1) ) then
			
			
			select	count(*)
			into	qt_reg_w
			from	atend_paciente_unidade a,
					setor_atendimento b
			where	a.nr_atendimento	= nr_atendimento_w
			and		a.cd_setor_atendimento	= b.cd_setor_atendimento
			and		a.ie_passagem_setor = 'N'
			and		b.cd_classif_setor	= '3'
			and		a.DT_ENTRADA_UNIDADE between dt_saida_old_w and dt_entrada_w;
			
			if	(qt_reg_w	> 0) then

				insert into EIS_REINTERNACAO_UTI(	nr_sequencia,
									dt_referencia,
									nm_usuario,
									dt_atualizacao,
									nm_usuario_nrec,
									dt_atualizacao_nrec,
									nr_atendimento)
						values		(	EIS_REINTERNACAO_UTI_seq.nextval,
									dt_entrada_atend_w,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									sysdate,
									nr_atendimento_w);
			end if;
			
		end if;
		dt_saida_old_w	:= dt_saida_w;
		end;
	end loop;
	close C02;
	
	
	end;
END LOOP;
CLOSE C01;

Atualizar_Log_Indicador(sysdate, nr_sequencia_w);

END Gerar_EIS_Reinternacao_UTI;
/
