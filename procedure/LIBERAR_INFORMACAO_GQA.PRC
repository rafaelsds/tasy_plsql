create or replace
procedure liberar_informacao_gqa(		nr_atendimento_p number,
										nm_tabela_p	varchar2,
										qt_chave_p	number,
										nm_usuario_p	varchar2,
										ie_html5_p		varchar2 default 'N') is 

nr_regras_atendidas_w  varchar2(2000);
ie_gera_prot_assistencial   boolean := False;
nr_seq_mentor_w		number(10);

begin

if	(upper(nm_tabela_p)	= 'DIAGNOSTICO_DOENCA') then
	
	if ( ie_html5_p = 'N' ) then -- Ja e realizado no objeto liberar_informacao. Duplicidade de geracaoo.
	
		GQA_LIBERACAO_DIAGNOSTICO(qt_chave_p, nm_usuario_p, nr_seq_mentor_w, nr_regras_atendidas_w);
		ie_gera_prot_assistencial := True;
			
		Gerar_tof_meta_atend(nr_atendimento_p , nm_usuario_p);
	
	End if;

elsif	(upper(nm_tabela_p)	= 'QUA_EVENTO_PACIENTE') then
		
	GQA_LIBERACAO_QUA_EVENTO(qt_chave_p, nm_usuario_p, nr_regras_atendidas_w, nr_atendimento_p);
    ie_gera_prot_assistencial := True;
		
	Gerar_tof_meta_atend(nr_atendimento_p , nm_usuario_p);
	
elsif	(upper(nm_tabela_p)	= 'CUR_CURATIVO') then
		
	GQA_LIBERACAO_CURATIVO(nr_atendimento_p, qt_chave_p, nm_usuario_p, nr_regras_atendidas_w);
    ie_gera_prot_assistencial := True;
	
	Gerar_tof_meta_atend(nr_atendimento_p , nm_usuario_p);

elsif (upper(nm_tabela_p) = 'PROTOCOLO_INT_PAC_EVENTO') then

  GQA_LIBERACAO_PROT_INT_EVENTO(qt_chave_p, nm_usuario_p, nr_atendimento_p, nr_regras_atendidas_w);
	
else

	GQA_Liberacao_Escala(nr_atendimento_p, qt_chave_p, nm_usuario_p, nm_tabela_p, nr_regras_atendidas_w, null);
    ie_gera_prot_assistencial := True;
	
	Gerar_tof_meta_atend(nr_atendimento_p , nm_usuario_p);
end if;	
		
commit;	


if ie_gera_prot_assistencial then 
    gera_protocolo_assistencial(nr_atendimento_p, nm_usuario_p);
end if;

end liberar_informacao_gqa;
/