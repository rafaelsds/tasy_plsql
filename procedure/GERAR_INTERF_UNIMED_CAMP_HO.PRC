create or replace
procedure gerar_interf_unimed_camp_ho(
				nr_seq_protocolo_p    in  number,
				cd_estabelecimento_p  in  number,
				ds_erro_p	      out varchar2) is

cd_local_prestador_w	varchar2(8);
mes_ano_producao_w	date;
nr_seq_grade_w		number(6):= 0;
ds_complemento_w		varchar2(223);
cd_convenio_w		number(5);
dt_mesano_referencia_w	date;
dt_alta_w			date;
nr_seq_int_envio_inicial_w	number(10):= 0;
nr_seq_int_envio_w   	number(10):= 0;
ie_tipo_protocolo_w		number(2);
nr_interno_conta_w		number(10);
nr_interno_conta_ant_w	number(10):= 0;
dt_entrada_w		date;
cd_usuario_convenio_w	varchar2(30);
tp_servico_w		varchar2(1);
cd_servico_w		varchar2(8);
qt_item_w		number(7,2);
vl_item_w			number(9,2);
cd_cid_principal_w		varchar2(4);
cd_cid_secundario_w	varchar2(4);
cd_solicitante_w		varchar2(11);
cd_executante_w		varchar2(11);
ie_internado_w		varchar2(1);
ie_atend_urgencia_w	varchar2(1);
ie_acidente_w		varchar2(1);
nm_paciente_w		varchar2(100);
ie_ordem_w		number(10);
tp_alta_w			varchar2(2);
data_aux_w		date;
linha_w			number(8):= 0;
seq_w			number(8):= 0;
nr_doc_convenio_w	varchar2(20);
ds_campos_w		varchar2(283);
cd_interface_w		number(5) := 0;
nr_atendimento_w	number(10,0);
ds_espaco_w		varchar2(11):= '';
ds_espaco_exec_w	varchar2(11):= '';
cd_cgc_hospital_w	varchar2(14);
ie_tipo_item_w		number(10,0);
qt_mat_w		number(10,0):=0;
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
dt_entrada2_w 		date;


/* cabe�alho */
cursor c01 is
	select	substr(nvl(cd_interno,'UNCPXXXX'),1,8) 	cd_local_prestador,
		dt_inicio_protocolo			mes_ano_producao,		
		substr(nm_hospital,1,40)		ds_complemento
	from 	w_interf_conta_header
	where	nr_seq_protocolo = nr_seq_protocolo_p;

/* movimento */
cursor c02 is
	select	a.nr_interno_conta		nr_interno_conta,
		decode(a.cd_cgc_hospital,'44595700000141', a.dt_alta,a.dt_entrada)			dt_entrada,
		decode(a.cd_cgc_hospital,'44595700000141',lpad(substr(replace(Obter_Guia_Conta(a.nr_interno_conta),'-',''),1,11),11,0),substr(replace(a.nr_doc_convenio,'-',''),1,11))	nr_doc_convenio,
		substr(a.cd_usuario_convenio,1,17) cd_usuario_convenio,
		decode(somente_numero(b.ie_emite_conta),51,4,52,4,53,5,72,6,74,7,75,2,8) tp_servico,
		substr(decode(a.cd_cgc_hospital,
			'61709689000112',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			'44595700000141',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			b.cd_item_convenio),1,8)	cd_servico,
		sum(b.qt_item)			qt_item,
		sum(decode(a.cd_cgc_hospital,'61709689000112',decode(b.ie_responsavel_credito,'M', b.vl_custo_oper, b.vl_total_item), b.vl_total_item)) vl_item,
		substr(a.cd_cid_principal,1,4)	cd_cid_principal,
		substr(a.cd_cid_secundario,1,4)	cd_cid_secundario,
		decode(a.ie_tipo_atendimento,1,'S','N') ie_internado,
		decode(nvl(a.ie_carater_inter,'U'),'U','S','N') ie_atend_urgencia,
		obter_acidente_tiss_atend(a.nr_atendimento) ie_acidente,		
		a.dt_alta			dt_alta,
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', substr(b.cd_medico_exec_conv,1,11) ,'189880') cd_solicitante,   -- 44595700000141 - centro m�dico de campinas  ,  61709689000112 - madre theodora
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', '88595', '189880') cd_executante,		
		nvl(obter_conversao_externa(a.cd_cgc_convenio, 'INTERF_UNIMED_CAMP_HO_V', 'TP_ALTA', a.cd_motivo_alta),'01')
						tp_alta,
		substr(a.nm_paciente,1,100) 	nm_paciente,
		decode(somente_numero(nvl(b.ie_emite_conta,0)),74,1,72,2,75,3,53,4,52,5,51,6,7) ie_ordem,		
		a.nr_atendimento,
		a.cd_cgc_hospital,
		b.ie_tipo_item,
		a.dt_periodo_inicial dt_periodo_inicial,
		a.dt_periodo_final   dt_periodo_final
	from 	w_interf_conta_cab	a,	
		w_interf_conta_item 	b
	where	a.nr_interno_conta = b.nr_interno_conta
	and 	somente_numero(nvl(b.ie_emite_conta,0)) in (51,52,53,72,74,75,77,94,97,98,101)  /* inclu�do o 77 pacotes , 94 ortese e protese*/
	and 	a.nr_seq_protocolo = nr_seq_protocolo_p
	and		((obter_classificacao_proced(cd_item, ie_origem_Proced,'C') <> 2 and b.ie_tipo_item = 1) or (b.ie_tipo_item = 2))
	group by
		a.nr_interno_conta,
		decode(a.cd_cgc_hospital,'44595700000141', a.dt_alta,a.dt_entrada),
		decode(a.cd_cgc_hospital,'44595700000141',lpad(substr(replace(Obter_Guia_Conta(a.nr_interno_conta),'-',''),1,11),11,0),substr(replace(a.nr_doc_convenio,'-',''),1,11)),
		substr(a.cd_usuario_convenio,1,17),
		decode(somente_numero(b.ie_emite_conta),51,4,52,4,53,5,72,6,74,7,75,2,8),
		substr(decode(a.cd_cgc_hospital,
			'61709689000112',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			'44595700000141',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			b.cd_item_convenio),1,8),
		a.cd_cid_principal,
		a.cd_cid_secundario,
		decode(a.ie_tipo_atendimento,1,'S','N'),
		decode(nvl(a.ie_carater_inter,'U'),'U','S','N'),
		obter_acidente_tiss_atend(a.nr_atendimento),
		a.dt_alta,
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', substr(b.cd_medico_exec_conv,1,11) ,'189880'),
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', '88595', '189880'),
		nvl(obter_conversao_externa(a.cd_cgc_convenio, 'INTERF_UNIMED_CAMP_HO_V', 'TP_ALTA', a.cd_motivo_alta),'01'),
		substr(a.nm_paciente,1,100),
		decode(somente_numero(nvl(b.ie_emite_conta,0)),74,1,72,2,75,3,53,4,52,5,51,6,7),
		a.nr_atendimento,
		a.cd_cgc_hospital,
		b.ie_tipo_item,
		a.dt_periodo_inicial,
		a.dt_periodo_final
	having	sum(b.qt_item) > 0
	union all
	select	a.nr_interno_conta		nr_interno_conta,
		decode(a.cd_cgc_hospital,'44595700000141', a.dt_alta,a.dt_entrada) dt_entrada,
		decode(a.cd_cgc_hospital,'44595700000141',lpad(substr(replace(Obter_Guia_Conta(a.nr_interno_conta),'-',''),1,11),11,0),substr(replace(a.nr_doc_convenio,'-',''),1,11))	nr_doc_convenio,
		substr(a.cd_usuario_convenio,1,17) cd_usuario_convenio,
		decode(somente_numero(b.ie_emite_conta),51,4,52,4,53,5,72,6,74,7,75,2,8) tp_servico,
		substr(decode(a.cd_cgc_hospital,
			'61709689000112',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			'44595700000141',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			b.cd_item_convenio),1,8)	cd_servico,
		sum(b.qt_item)			qt_item,
		sum(decode(a.cd_cgc_hospital,'61709689000112',decode(b.ie_responsavel_credito,'M', b.vl_custo_oper, b.vl_total_item), b.vl_total_item)) vl_item,
		substr(a.cd_cid_principal,1,4)	cd_cid_principal,
		substr(a.cd_cid_secundario,1,4)	cd_cid_secundario,
		decode(a.ie_tipo_atendimento,1,'S','N') ie_internado,
		decode(nvl(a.ie_carater_inter,'U'),'U','S','N') ie_atend_urgencia,
		obter_acidente_tiss_atend(a.nr_atendimento) ie_acidente,		
		a.dt_alta			dt_alta,
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', substr(b.cd_medico_exec_conv,1,11) ,'189880') cd_solicitante,   -- 44595700000141 - centro m�dico de campinas  ,  61709689000112 - madre theodora
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', '88595', '189880') cd_executante,		
		nvl(obter_conversao_externa(a.cd_cgc_convenio, 'INTERF_UNIMED_CAMP_HO_V', 'TP_ALTA', a.cd_motivo_alta),'01')
						tp_alta,
		substr(a.nm_paciente,1,100) 	nm_paciente,
		decode(somente_numero(nvl(b.ie_emite_conta,0)),74,1,72,2,75,3,53,4,52,5,51,6,7) ie_ordem,		
		a.nr_atendimento,
		a.cd_cgc_hospital,
		b.ie_tipo_item,
		a.dt_periodo_inicial dt_periodo_inicial,
		a.dt_periodo_final   dt_periodo_final		
	from 	w_interf_conta_cab	a,	
		w_interf_conta_item 	b
	where	a.nr_interno_conta = b.nr_interno_conta
	and 	somente_numero(nvl(b.ie_emite_conta,0)) in (51,52,53,72,74,75,77,94,97,98,101)  /* inclu�do o 77 pacotes , 94 ortese e protese*/
	and 	a.nr_seq_protocolo = nr_seq_protocolo_p
	and		(obter_classificacao_proced(cd_item, ie_origem_Proced,'C') = 2 and b.ie_tipo_item = 1)
	group by
		a.nr_interno_conta,
		b.dt_item,
		decode(a.cd_cgc_hospital,'44595700000141', a.dt_alta,a.dt_entrada),
		decode(a.cd_cgc_hospital,'44595700000141',lpad(substr(replace(Obter_Guia_Conta(a.nr_interno_conta),'-',''),1,11),11,0),substr(replace(a.nr_doc_convenio,'-',''),1,11)),
		substr(a.cd_usuario_convenio,1,17),
		decode(somente_numero(b.ie_emite_conta),51,4,52,4,53,5,72,6,74,7,75,2,8),
		substr(decode(a.cd_cgc_hospital,
			'61709689000112',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			'44595700000141',decode(b.cd_item_convenio,to_char(b.cd_item),nvl(to_char(b.cd_procedimento_tuss),b.cd_item_convenio),b.cd_item_convenio),
			b.cd_item_convenio),1,8),
		a.cd_cid_principal,
		a.cd_cid_secundario,
		decode(a.ie_tipo_atendimento,1,'S','N'),
		decode(nvl(a.ie_carater_inter,'U'),'U','S','N'),
		obter_acidente_tiss_atend(a.nr_atendimento),
		a.dt_alta,
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', substr(b.cd_medico_exec_conv,1,11) ,'189880'),
		decode(a.cd_cgc_hospital,'61709689000112','5825733','44595700000141', '88595', '189880'),
		nvl(obter_conversao_externa(a.cd_cgc_convenio, 'INTERF_UNIMED_CAMP_HO_V', 'TP_ALTA', a.cd_motivo_alta),'01'),
		substr(a.nm_paciente,1,100),
		decode(somente_numero(nvl(b.ie_emite_conta,0)),74,1,72,2,75,3,53,4,52,5,51,6,7),
		a.nr_atendimento,
		a.cd_cgc_hospital,
		b.ie_tipo_item,
		a.dt_periodo_inicial,
		a.dt_periodo_final
	having	sum(b.qt_item) > 0
	order by nr_atendimento, dt_entrada, nr_interno_conta,ie_ordem;
	

begin

delete from w_interf_unimed_camp_ho
where nr_seq_protocolo = nr_seq_protocolo_p;

commit;

begin
gerar_interf_conta(cd_estabelecimento_p, nr_seq_protocolo_p);
exception
	when others then
		ds_erro_p:= WHEB_MENSAGEM_PCK.get_texto(279112);
end;

data_aux_w:= sysdate;
qt_mat_w:= 0;

/* achar o n�mero da grade  ++ inicio ++  */

select 	max(cd_convenio),
	max(dt_mesano_referencia),
	max(ie_tipo_protocolo),
	max(nvl(cd_interface_envio,0))
into	cd_convenio_w,
	dt_mesano_referencia_w,
	ie_tipo_protocolo_w,
	cd_interface_w
from 	protocolo_convenio
where 	nr_seq_protocolo = nr_seq_protocolo_p;

if	(cd_interface_w = 0) then
	select 	max(nvl(cd_interface_envio,0))
	into	cd_interface_w
	from 	convenio
	where 	cd_convenio = cd_convenio_w;
end if;


select 	max(nvl(nr_seq_int_envio_inicial,0))
into	nr_seq_int_envio_inicial_w
from 	protocolo_convenio
where	nr_seq_protocolo = nr_seq_protocolo_p;


if	(nr_seq_int_envio_inicial_w = 0) then
	select 	max(nvl(nr_seq_int_envio,0))
	into	nr_seq_int_envio_w
	from 	protocolo_convenio
	where	cd_convenio = cd_convenio_w
	and	trunc(dt_mesano_referencia, 'month') 		= trunc(dt_mesano_referencia_w, 'month')
	and 	nvl(cd_interface_envio, cd_interface_w)		= cd_interface_w;
	
	if	(nr_seq_int_envio_w = 0) then
		select	max(nvl(nr_seq_envio_prot,0))
		into	nr_seq_grade_w
		from	conv_regra_envio_protocolo
		where	cd_convenio					= cd_convenio_w
		and 	nvl(cd_interface, cd_interface_w)		= cd_interface_w
		and 	nvl(ie_tipo_regra, 'S') = 'I';
	else
		nr_seq_grade_w:= nr_seq_int_envio_w + 1;
	end if;
else
	nr_seq_grade_w:= nr_seq_int_envio_inicial_w;
end if;

/* achar o n�mero da grade  ++ fim ++  */

update	protocolo_convenio
set 	nr_seq_int_envio_inicial = nr_seq_grade_w
where 	nr_seq_protocolo 	 = nr_seq_protocolo_p;

open c01;
loop
fetch c01 into
	cd_local_prestador_w,
	mes_ano_producao_w,	
	ds_complemento_w;
exit when c01%notfound;
	begin
	ds_campos_w:=   'AH    ' || rpad(cd_local_prestador_w,8,' ') || '21' || to_char(mes_ano_producao_w,'mmyyyy') ||
			 '        ' || lpad(nr_seq_grade_w,6,0) || to_char(data_aux_w,'ddmmyyyy') ||
			 lpad(' ',25,' ') || rpad(ds_complemento_w,213,' ');
	insert into w_interf_unimed_camp_ho
			(nr_sequencia,
			 nr_linha,
			 nr_seq_protocolo,
			 ds_campos,
			 nr_seq_grade,
			 nr_interno_conta,
			 cd_usuario_convenio,
			 dt_entrada,
			 qt_item,
			 vl_item,
			 nm_paciente,
			 nr_doc_convenio,
			 cd_servico,
			 cd_solicitante,
			 ie_tipo_registro,
			 nr_atendimento)   /* 1 - cabe�alho,   2  -  movimento */
		values	(w_interf_unimed_camp_seq.nextval,
			seq_w,
			nr_seq_protocolo_p,
			ds_campos_w,
			nr_seq_grade_w,
			0,
			'',
			sysdate,
			0,
			0,
			'',
			'',
			0,
			'',
			1,
			0);
	end;
end loop;
close c01;


open c02;
loop
fetch c02 into
	nr_interno_conta_w,
	dt_entrada_w,
	nr_doc_convenio_w,
	cd_usuario_convenio_w,
	tp_servico_w,
	cd_servico_w,
	qt_item_w,
	vl_item_w,
	cd_cid_principal_w,
	cd_cid_secundario_w,
	ie_internado_w,
	ie_atend_urgencia_w,
	ie_acidente_w,
	dt_alta_w,
	cd_solicitante_w,
	cd_executante_w,
	tp_alta_w,
	nm_paciente_w,
	ie_ordem_w,
	nr_atendimento_w,
	cd_cgc_hospital_w,
	ie_tipo_item_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w;
exit when c02%notfound;
	begin
	dt_entrada2_w	:= dt_entrada_w;
	
	if	(cd_cgc_hospital_w = '44595700000141') then
		dt_entrada_w:= dt_periodo_inicial_w;
		dt_entrada2_w := dt_periodo_final_w;
	end if;
	
	
	if	((cd_cgc_hospital_w = '61709689000112') and 
		(ie_tipo_item_w = 2) and (vl_item_w = 0)) then
		goto proximo;
	end if;
	
	seq_w:=	seq_w + 1;

	if (nr_interno_conta_ant_w <> nr_interno_conta_w) then
		nr_interno_conta_ant_w:= nr_interno_conta_w;
		linha_w:= linha_w + 1;
	end if;		
	
	if	(cd_solicitante_w = '5825733') or 
		(cd_solicitante_w = '189880') then
		
		select 	decode(length(cd_solicitante_w), 6, '     ','    ') 
		into	ds_espaco_w
		from 	dual;	
	
	else
		if	(cd_solicitante_w is null) then
			select 	substr(cd_medico_atendimento,1,11)
			into	cd_solicitante_w
			from	atendimento_paciente
			where 	nr_atendimento = nr_atendimento_w;
			
			select 	obter_medico_convenio(1, cd_solicitante_w, cd_convenio_w, null, null, null, null,dt_entrada_w, null)
			into	cd_solicitante_w
			from 	dual;
		end if;
		
		ds_espaco_w:= '';
		
		select	lpad(cd_solicitante_w, 11, ' ')
		into	cd_solicitante_w
		from 	dual;
	
	end if;	
	
	select 	decode(length(cd_executante_w), 6, '     ', 5, '      ', '    ') 
	into	ds_espaco_exec_w
	from 	dual;
	
	/*SEMPRE que a guia tiver 10 d�gitos e iniciar com 1 deve incluir o 0 antes do 1000*/
	/* OS 157902 */
	if	(cd_cgc_hospital_w = '46030318000116') and
		(length(nr_doc_convenio_w) = 10) and (substr(nr_doc_convenio_w,1,1) = '1') then
		nr_doc_convenio_w:= '0' || nr_doc_convenio_w;
	end if;

	if	(linha_w < 16) then
		ds_campos_w:= 	to_char(dt_entrada2_w,'ddmmyyyyhh24mi') || rpad(nvl(nr_doc_convenio_w,' '),11,' ') || 
				lpad(nvl(cd_usuario_convenio_w,'0'),17,'0') || nvl(tp_servico_w,'8') || rpad(cd_servico_w,8,' ') || 
				lpad(replace(campo_mascara(qt_item_w,2),'.',''),7,'0') || lpad(replace(campo_mascara(vl_item_w,2),'.',''),9,'0') ||
				rpad(nvl(cd_cid_principal_w,' '),4,' ') || rpad(nvl(cd_cid_secundario_w,' '),4,' ') || ds_espaco_w || 
				nvl(cd_solicitante_w,'0') || ds_espaco_exec_w || nvl(cd_executante_w,'0') || '100' || 
				nvl(ie_internado_w,'N') || nvl(ie_atend_urgencia_w,'N') || nvl(ie_acidente_w,'N') || rpad(' ',100,' ') || 
				' 0' || '00000000' || rpad(' ',30,' ') || '0' || rpad(' ',17,' ') || rpad(' ',6,' ') || to_char(dt_entrada_w,'ddmmyyyy') ||
				to_char(nvl(dt_alta_w,sysdate),'ddmmyyyy') || lpad(nvl(tp_alta_w,'01'),2,'0');
				
		insert into w_interf_unimed_camp_ho
			(nr_sequencia,
			 nr_linha,
			 nr_seq_protocolo,
			 ds_campos,
			 nr_seq_grade,
			 nr_interno_conta,
			 cd_usuario_convenio,
			 dt_entrada,
			 qt_item,
			 vl_item,
			 nm_paciente,
			 nr_doc_convenio,
			 cd_servico,
			 cd_solicitante,
			 ie_tipo_registro,
			nr_atendimento)   /* 1 - cabe�alho,   2  -  movimento */
		values	(w_interf_unimed_camp_seq.nextval,
			seq_w,
			nr_seq_protocolo_p,
			ds_campos_w,
			nr_seq_grade_w,
			nr_interno_conta_w,
			cd_usuario_convenio_w,
			dt_entrada_w,
			qt_item_w,
			vl_item_w,
			nm_paciente_w,
			nr_doc_convenio_w,
			cd_servico_w,
			cd_solicitante_w,
			2,
			nr_atendimento_w);

	else
		linha_w:= 1;
		/* cabe�alho */
		nr_seq_grade_w:= nr_seq_grade_w + 1;
		ds_campos_w:=    'AH    ' || rpad(cd_local_prestador_w,8,' ') || '21' || to_char(mes_ano_producao_w,'mmyyyy') ||
				 '        ' || lpad(nr_seq_grade_w,6,0) || to_char(data_aux_w,'ddmmyyyy') ||
			 	 lpad(' ',25,' ') || rpad(ds_complemento_w,213,' ');

		insert into w_interf_unimed_camp_ho
			(nr_sequencia,
			 nr_linha,
			 nr_seq_protocolo,
			 ds_campos,
			 nr_seq_grade,
			 nr_interno_conta,
			 cd_usuario_convenio,
			 dt_entrada,
			 qt_item,
			 vl_item,
			 nm_paciente,
			 nr_doc_convenio,
			 cd_servico,
			 cd_solicitante,
			 ie_tipo_registro,
			 nr_atendimento)   /* 1 - cabe�alho,   2  -  movimento */
		values	(w_interf_unimed_camp_seq.nextval,
			seq_w,
			nr_seq_protocolo_p,
			ds_campos_w,
			nr_seq_grade_w,
			0,
			'',
			sysdate,
			0,
			0,
			'',
			'',
			0,
			'',
			1,
			nr_atendimento_w);

		seq_w:=	seq_w + 1;
		/* movimento */
		ds_campos_w:= 	to_char(dt_entrada2_w,'ddmmyyyyhh24mi') || lpad(nvl(nr_doc_convenio_w,' '),11,' ') || 
				lpad(nvl(cd_usuario_convenio_w,'0'),17,'0') || nvl(tp_servico_w,'8') || rpad(cd_servico_w,8,' ') || 
				lpad(replace(campo_mascara(qt_item_w,2),'.',''),7,'0') || lpad(replace(campo_mascara(vl_item_w,2),'.',''),9,'0') ||
				rpad(nvl(cd_cid_principal_w,' '),4,' ') || rpad(nvl(cd_cid_secundario_w,' '),4,' ') || ds_espaco_w || 
				nvl(cd_solicitante_w,'0') || ds_espaco_exec_w || nvl(cd_executante_w,'0') || '100' || 
				nvl(ie_internado_w,'N') || nvl(ie_atend_urgencia_w,'N') || nvl(ie_acidente_w,'N') || rpad(' ',100,' ') || 
				' 0' || '00000000' || rpad(' ',30,' ') || '0' || rpad(' ',17,' ') || rpad(' ',6,' ') || to_char(dt_entrada_w,'ddmmyyyy') ||
				to_char(nvl(dt_alta_w,sysdate),'ddmmyyyy') || lpad(nvl(tp_alta_w,'01'),2,'0');
		insert into w_interf_unimed_camp_ho
			(nr_sequencia,
			 nr_linha,
			 nr_seq_protocolo,
			 ds_campos,
			 nr_seq_grade,
			 nr_interno_conta,
			 cd_usuario_convenio,
			 dt_entrada,
			 qt_item,
			 vl_item,
			 nm_paciente,
			 nr_doc_convenio,
			 cd_servico,
			 cd_solicitante,
			 ie_tipo_registro,
			 nr_atendimento)   /* 1 - cabe�alho,   2  -  movimento */
		values	(w_interf_unimed_camp_seq.nextval,
			seq_w,
			nr_seq_protocolo_p,
			ds_campos_w,
			nr_seq_grade_w,
			nr_interno_conta_w,
			cd_usuario_convenio_w,
			dt_entrada_w,
			qt_item_w,
			vl_item_w,
			nm_paciente_w,
			nr_doc_convenio_w,
			cd_servico_w,
			cd_solicitante_w,
			2,
			nr_atendimento_w);	
	end if;		
		
	<<proximo>>
		if	((cd_cgc_hospital_w = '61709689000112') and 
			(ie_tipo_item_w = 2) and (vl_item_w = 0)) then
			qt_mat_w:= qt_mat_w + 1;
		end if;	
		
	end;
	
end loop;
close c02;

update	protocolo_convenio
set 	nr_seq_int_envio = nr_seq_grade_w
where 	nr_seq_protocolo = nr_seq_protocolo_p;

commit;

end gerar_interf_unimed_camp_ho;
/
