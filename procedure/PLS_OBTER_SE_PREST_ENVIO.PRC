create or replace
procedure pls_obter_se_prest_envio
			(	nr_seq_prestador_p		number,
				dt_referencia_p			date,
				ie_envio_p		out	varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter se o prestador m�dico se encaixa em uma das regras de envio.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_envio_prest_w		varchar2(255);
ie_envio_med_w			varchar2(255);
ie_envio_w			varchar2(255)	:= null;
nr_seq_regra_cpm_w		number(10);
nr_seq_tipo_prestador_w		number(10);
nr_seq_conselho_w		number(10);

Cursor C01 is
	select	a.nr_sequencia
	from	pls_regra_envio_cpm	a
	where	dt_referencia_p between a.dt_inicio_vigencia and a.dt_fim_vigencia;

begin
ie_envio_p	:= 'S';

select	max(a.nr_seq_tipo_prestador),
	max(b.nr_seq_conselho)
into	nr_seq_tipo_prestador_w,
	nr_seq_conselho_w
from	pessoa_fisica	b,
	pls_prestador	a
where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
and	a.nr_sequencia		= nr_seq_prestador_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_cpm_w;
exit when C01%notfound or (ie_envio_w = 'S');
	begin
	pls_obter_se_regra_prest(nr_seq_tipo_prestador_w,
				nr_seq_regra_cpm_w,
				nr_seq_prestador_p,
				ie_envio_prest_w);
				
	pls_obter_se_regra_med(nr_seq_conselho_w,
				nr_seq_regra_cpm_w,
				nr_seq_prestador_p,
				ie_envio_med_w);
				
	if	(ie_envio_prest_w = 'S') and
		(ie_envio_med_w = 'S') then
		ie_envio_w	:= 'S';
	else
		ie_envio_w	:= 'N';
	end if;
	end;
end loop;
close C01;

if	(C01%ISOPEN) then
	close C01;
end if;	

ie_envio_p	:= nvl(ie_envio_w, ie_envio_p);

end pls_obter_se_prest_envio;
/