CREATE OR REPLACE PROCEDURE SCHEMATIC4TEST_AUTO_SCHEDULE(WEEK_P	NUMBER, TIME_P	VARCHAR2) IS
NR_SEQ_SUITE_W	NUMBER(10,0); 
NR_SEQ_PRESET_W NUMBER(10,0); 
DS_TIME_W1 VARCHAR2(5);
DS_TIME_W2 VARCHAR2(5);
QTD_W NUMBER(10,0); 

--loop suites that was scheduled 
CURSOR C01 IS
  SELECT autosch.NR_SEQ_SUITE NR_SEQ_SUITE, substr(autosch.DS_TIME,0,4) DS_TIME1, substr(autosch.DS_TIME,6,10) DS_TIME2, autosch.NR_SEQ_PRESET NR_SEQ_PRESET
      INTO NR_SEQ_SUITE_W, DS_TIME_W1, DS_TIME_W2, NR_SEQ_PRESET_W
  FROM SCHEM_TEST_AUTO_SCHEDULE autosch WHERE autosch.IE_SWITCH = '1' AND (CASE '2'
                                                                                  WHEN '1' THEN  autosch.IE_DOM
                                                                                  WHEN '2' THEN  autosch.IE_SEG
                                                                                  WHEN '3' THEN  autosch.IE_TER
                                                                                  WHEN '4' THEN  autosch.IE_QUA
                                                                                  WHEN '5' THEN  autosch.IE_QUI
                                                                                  WHEN '6' THEN  autosch.IE_SEX
                                                                                  WHEN '7' THEN  autosch.IE_SAB
                                                                                  
                                                                              END) = '1';
BEGIN
	--procedure that create schedule by suite
	IF(WEEK_P IS NOT NULL) THEN                                                                              
      OPEN C01;
      LOOP
        FETCH C01 INTO	
          NR_SEQ_SUITE_W, 
          DS_TIME_W1, 
          DS_TIME_W2, 
          NR_SEQ_PRESET_W;
      EXIT WHEN C01%NOTFOUND;
      BEGIN	
      
		SELECT COUNT(NR_SEQUENCIA) QTD 
          INTO QTD_W
		FROM SCHEM_TEST_SCHEDULE schedule
			WHERE schedule.NR_SEQ_SUITE = NR_SEQ_SUITE_W AND schedule.IE_STATUS not in ('3','4','5')
			AND schedule.IE_JOBS not in ('1', '2','666');
            
      
		IF ((TIME_P = DS_TIME_W1) OR (TIME_P = DS_TIME_W2)) THEN
          IF (QTD_W = 0) THEN
              IF ((DS_TIME_W1 IS NOT NULL) OR (DS_TIME_W2 IS NOT NULL)) THEN                
                  IF (NR_SEQ_PRESET_W IS NOT NULL) THEN
                      UPDATE SCHEM_TEST_SUITE SET NR_SEQ_PRESET = NR_SEQ_PRESET_W, IE_JOBS = '3' WHERE NR_SEQUENCIA = NR_SEQ_SUITE_W AND NR_SEQ_PRESET <> NR_SEQ_PRESET_W;
                      COMMIT;
                      SCHEMATIC4TEST_CONFTSTSUITPRES();
                END IF;
              END IF;
              
              SCHEMATIC4TEST_SENDTOSCHEDULE(NR_SEQ_SUITE_W,'0');
          END IF;
      END IF;
      END;
      END LOOP;
      CLOSE C01; 
    END IF;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Erro: Data not found');
END SCHEMATIC4TEST_AUTO_SCHEDULE;
/