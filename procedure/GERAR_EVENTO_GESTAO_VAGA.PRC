create or replace
procedure gerar_evento_gestao_vaga (
				cd_pessoa_fisica_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				nr_seq_gv_p 		number,
				nm_paciente_p		varchar2) is

nr_seq_evento_w		number(10);
qt_idade_w		number(10);
cd_convenio_gv_w	number(15);
qt_regra_gv_w		Number(10);
ie_permite_w		varchar2(1) := 'N';

ie_status_w 		varchar2(15);
ie_tipo_vaga_w		Varchar2(15);
ie_solicitacao_w	Varchar2(15);
cd_setor_atual_w	number(15);
cd_tipo_acomod_desej_w	number(15);
cd_setor_desejado_w	number(15);
nr_seq_status_pac_w	number(10);
nr_sequencia_w		number(10);
cursor c01 is
	select	nr_seq_evento,
		nr_sequencia
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_evento_disp		= 'VM'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and 	(obter_se_convenio_rec_alerta(cd_convenio_gv_w, nr_sequencia) = 'S')
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_p),0)) = 'S')
	and	nvl(ie_situacao,'A') = 'A';
	
	
begin

select	nvl(cd_convenio,0),
	nvl(ie_tipo_vaga,'X'),
	nvl(ie_status,'X'),
	nvl(ie_solicitacao,'X'),
	nvl(cd_setor_atual,0),
	nvl(cd_setor_desejado,0),
	nvl(cd_tipo_acomod_desej,0),
	nvl(nr_seq_status_pac,0)
into	cd_convenio_gv_w,
	ie_tipo_vaga_w,
	ie_status_w,
	ie_solicitacao_w,
	cd_setor_atual_w,
	cd_setor_desejado_w,
	cd_tipo_acomod_desej_w,
	nr_seq_status_pac_w
from	gestao_vaga
where	nr_sequencia = nr_seq_gv_p;

if	(cd_pessoa_fisica_p is not null) OR (nm_paciente_p is not null) then
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A'),0);
	open c01;
	loop
	fetch c01 into
		nr_seq_evento_w,
		nr_sequencia_w;
	exit when c01%notfound;
		begin		
		
		select	count(*)
		into	qt_regra_gv_w
		from	regra_envio_sms_gv
		where	nr_seq_regra = nr_sequencia_w;
		
		if (qt_regra_gv_w = 0) and (ie_tipo_vaga_w = 'C') then
		
			gerar_evento_paciente(nr_seq_evento_w,null,cd_pessoa_fisica_p,null,nm_usuario_p,null,null,null,null,null,null,nr_seq_gv_p);
			
		elsif (qt_regra_gv_w > 0) then
			
			select 	decode(count(*),0,'N','S')
			into	ie_permite_w
			from	regra_envio_sms_gv
			where	ie_situacao 						= 'A' 
			and	nvl(ie_status, ie_status_w) 				= ie_status_w 
			and	nvl(ie_solicitacao, ie_solicitacao_w)			= ie_solicitacao_w
			and	nvl(cd_setor_atual, cd_setor_atual_w)			= cd_setor_atual_w
			and	nvl(cd_tipo_acomod_desej, cd_tipo_acomod_desej_w)	= cd_tipo_acomod_desej_w
			and	nvl(cd_setor_desejado, cd_setor_desejado_w)		= cd_setor_desejado_w
			and	nvl(ie_tipo_vaga, ie_tipo_vaga_w)			= ie_tipo_vaga_w
			and	nvl(nr_seq_status_pac, nr_seq_status_pac_w)		= nr_seq_status_pac_w
			and	nr_seq_regra 						= nr_sequencia_w;
			
			if (ie_permite_w = 'S') then				
				
				gerar_evento_paciente(nr_seq_evento_w,null,cd_pessoa_fisica_p,null,nm_usuario_p,null,null,null,null,null,null,nr_seq_gv_p);				
				
			end if;
		end if;
		
		end;
	end loop;
	close c01;
end if;

end gerar_evento_gestao_vaga;
/
