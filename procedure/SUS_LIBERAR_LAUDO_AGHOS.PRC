create or replace
procedure sus_liberar_laudo_aghos(	nr_seq_laudo_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		varchar2) is


qt_inco_laudo_w			number(3)	:= 0;
ie_gerar_apac_laudo_w		varchar2(15)	:= 'N';
ie_classificacao_w			number(3);
nr_atendimento_w			number(10);
cd_estabelecimento_w		number(4);
cd_procedimento_solic_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_apac_unif_w		number(10);
nr_interno_conta_w			number(10);
ie_gerar_proced_w			varchar2(1);
	
cd_medico_requisitante_w		varchar2(10);
cd_cid_principal_w			number(4);
ds_sinal_sintoma_w			varchar2(1000);
ds_condicao_justifica_w		varchar2(1000);
ds_result_prova_w			varchar2(1000);
ie_integra_aghos_w			varchar2(1);
qt_regra_lanc_w			number(10);
cd_estab_usuario_w		number(4);
cd_procedimento_w		number(15);
cd_medico_w			varchar2(10);
ds_sep_bv_w			varchar2(50);
ds_comando_w			varchar2(2000);
nr_seq_soager_w			number(10);
ds_erro_w			varchar2(2000);
ds_sqlerror_w			varchar2(4000);
ie_gerar_aih_laudo_w		varchar2(15) := 'N';

/*
IE_OPCAO:
L - Liberar Laudo
D - Desfazer Libera��o do laudo
*/

Cursor C01 is
	select	nr_sequencia,
		nr_interno_conta
	from	sus_apac_unif
	where	nr_apac 		= 0
	and	nr_atendimento		= nr_atendimento_w
	and	cd_estabelecimento 	= cd_estabelecimento_w
	and	cd_procedimento 	= cd_procedimento_solic_w
	and	ie_origem_proced 	= ie_origem_proced_w
	and	ie_tipo_apac in (1,2);

begin

begin
cd_estab_usuario_w := wheb_usuario_pck.get_cd_estabelecimento;
exception
when others then
	cd_estab_usuario_w := null;
end;

ie_gerar_apac_laudo_w	:= nvl(obter_valor_param_usuario( 281, 1029, obter_perfil_ativo, nm_usuario_p , nvl(cd_estab_usuario_w,0)),'N');/*Geliard 19/09/2011 OS351812*/
ie_gerar_proced_w	:= obter_valor_param_usuario(1124,42, obter_perfil_ativo, nm_usuario_p, nvl(cd_estab_usuario_w,0));
ie_gerar_aih_laudo_w	:= nvl(obter_valor_param_usuario(1123,194, obter_perfil_ativo, nm_usuario_p, nvl(cd_estab_usuario_w,0)),'N');

ie_integra_aghos_w := obter_dados_param_atend(wheb_usuario_pck.get_cd_estabelecimento, 'AG');

if	(ie_opcao_p = 'L') then
	begin
	select	count(*)
	into	qt_inco_laudo_w
	from	sus_inconsistencia_laudo	b,
		sus_inco_reg_laudo		a
	where	a.nr_seq_inconsistencia	= b.nr_sequencia
	and	a.nr_seq_laudo		= nr_seq_laudo_p
	and	b.ie_permite_liberar	= 'N';

	if	(qt_inco_laudo_w	> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(199076);
		--Laudo com inconsist�ncias, n�o � poss�vel libera-lo.
	end if;

	update	sus_laudo_paciente
	set	dt_liberacao	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_seq_interno	= nr_seq_laudo_p;
	
	select	max(nr_atendimento),
		max(ie_classificacao)
	into	nr_atendimento_w,
		ie_classificacao_w
	from	sus_laudo_paciente
	where	nr_seq_interno = nr_seq_laudo_p;
		
	if	(obter_funcao_ativa = 281) and -- PEP
		(ie_integra_aghos_w = 'S') and
		(nvl(ie_classificacao_w, 1) = 1) then 	

		begin
			Aghos_solicitacao_internacao(	nr_atendimento_w,
							nr_seq_laudo_p,  
							nm_usuario_p);
		exception
			when 	others then
			
				ds_sqlerror_w := sqlerrm;
			
				select	solicitacao_aghos_erro_seq.nextval,
					substr(Ajusta_mensagem_erro_aghos(ds_sqlerror_w), 1, 2000)
				into	nr_seq_soager_w,
					ds_erro_w
				from	dual;
			
				insert into solicitacao_aghos_erro(
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_laudo,
					nr_atendimento,
					ds_erro)
				values (nr_seq_soager_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_laudo_p,
					nr_atendimento_w,
					ds_erro_w);
					
				commit;
		end;
	end if;
	
	if	(ie_gerar_apac_laudo_w = 'S') then
		begin		
		select	nvl(max(ie_classificacao),1)
		into	ie_classificacao_w
		from	sus_laudo_paciente
		where	nr_seq_interno = nr_seq_laudo_p;
		
		if	(ie_classificacao_w in (11,12,13)) then			
			sus_gerar_apac_laudo_sem_autor(nr_seq_laudo_p,nm_usuario_p);
		end if;	
		end;
	end if;
	
	select 	count(*)
	into	qt_regra_lanc_w
	from 	regra_lanc_automatico
	where 	nr_seq_evento = 536
	and 	ie_situacao = 'A'
	and 	cd_estabelecimento = nvl(cd_estab_usuario_w,cd_estabelecimento);
	
	if	(qt_regra_lanc_w > 0) and
		(nvl(nr_atendimento_w,0) > 0) then
		begin		
		gerar_lancamento_automatico(nr_atendimento_w,0,536,nm_usuario_p,0,nr_seq_laudo_p,'','','',0);
		end;
	end if;
	
	if	(ie_gerar_aih_laudo_w = 'S') then
		begin		
		if	(ie_classificacao_w = 1) then			
			sus_gerar_aih_liberar_laudo(nr_seq_laudo_p,nm_usuario_p);
		end if;	
		end;
	end if;
	
	end;
end if;
--Desfaz libera��o do laudo
if 	(ie_opcao_p = 'D')then
	begin
	if	(ie_gerar_apac_laudo_w = 'S') then
		begin		
		select	nvl(max(ie_classificacao),1)
		into	ie_classificacao_w
		from	sus_laudo_paciente
		where	nr_seq_interno = nr_seq_laudo_p;
		
		if	(ie_classificacao_w in (11,12,13)) then
			begin
			select	b.nr_atendimento,
				b.cd_estabelecimento,
				a.cd_procedimento_solic,
				a.ie_origem_proced
			into	nr_atendimento_w,
				cd_estabelecimento_w,
				cd_procedimento_solic_w,
				ie_origem_proced_w
			from	atendimento_paciente b,
				sus_laudo_paciente a
			where	b.nr_atendimento = a.nr_atendimento
			and	a.nr_seq_interno = nr_seq_laudo_p;
			
			open C01;
			loop
			fetch C01 into	
				nr_seq_apac_unif_w,
				nr_interno_conta_w;
			exit when C01%notfound;
				begin
				if	(ie_gerar_proced_w = 'S') then
					delete from procedimento_paciente where	nr_interno_conta = nr_interno_conta_w 
						and cd_procedimento = cd_procedimento_solic_w and ie_origem_proced = ie_origem_proced_w;
				end if;
				
				delete from sus_apac_unif where nr_sequencia = nr_seq_apac_unif_w;
				end;
			end loop;
			close C01;
			end;
		end if;	
		end;
	end if;
	
	update	sus_laudo_paciente
	set	dt_liberacao	= null,
		dt_assinatura_laudo = null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_seq_interno	= nr_seq_laudo_p;
	end;
end if;

commit;

end sus_liberar_laudo_aghos;
/
