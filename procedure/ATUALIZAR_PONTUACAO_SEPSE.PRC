create or replace
procedure atualizar_pontuacao_sepse(	nr_atendimento_p	number,
										nr_seq_escala_p		number,
										nr_seq_atributo_p	number,
										nm_usuario_p		Varchar2,
										ie_tipo_sepse_p     varchar2 default 'A') is 

qt_pontuacao_w			number(10) := 0;
qt_reg_w				number(10);
qt_valor_w				number(15,5);
ie_atributo_w			varchar2(60) := '';
ie_regra_w				varchar(1);
ie_nivel_consciencia_w	varchar2(15);
qt_pont_w				number(10);
cd_setor_atendimento_w  number(5);
cd_classif_setor_w		varchar2(2);
qt_regra_HP_w			number(10);
qt_regra_FC_w			number(10);
qt_regra_discrasia_w		number(10);
qt_regra_edema_w		number(10);
ie_versao_sepse_w   varchar2(10);
qt_saturacao_o2_w	number(10);
qt_hora_sv_w		number(3);
ie_alteracao_manual_w	varchar2(1);
qt_nivel_cons_w			number(10) := 0;


begin

obter_dados_escala_sepse(nr_atendimento_p,nr_seq_atributo_p,qt_valor_w,ie_regra_w,qt_pont_w,nm_usuario_p, ie_atributo_w, ie_tipo_sepse_p);

select	count(*)
into	qt_reg_w
from	sepse_atributo_cliente
where	nr_seq_atributo = nr_seq_atributo_p;	

if	(qt_reg_w > 0) and
	(ie_regra_w = 'S') then

	select	max(qt_pontuacao)
	into	qt_pontuacao_w
	from	sepse_atributo_cliente b
	where	b.nr_seq_atributo = nr_seq_atributo_p
	and		qt_valor_w between nvl(qt_valor_min,0) and nvl(qt_valor_max,99999);
end if;

select nvl(max(ie_versao_sepse),'1')
into   ie_versao_sepse_w
from   parametro_medico
where  cd_estabelecimento = nvl(obter_dados_atendimento(nr_atendimento_p,'EST'), obter_estabelecimento_ativo);


if	((ie_tipo_sepse_p = 'P') and (nr_seq_atributo_p in (83,84,85,87,89,90,80))) or
	((ie_versao_sepse_w = '1') and (nr_seq_atributo_p in (4,5,6,7,8,11,17,18,19,30,31,32,33))) or 
	(ie_versao_sepse_w = '2' and (nr_seq_atributo_p in (47,48,49,50,51,59,60,61,41,42,95))) then

	Select 	Obter_Setor_Atendimento(nr_atendimento_p)
	into	cd_setor_atendimento_w
	from 	dual;
	
	if	(nvl(cd_setor_atendimento_w,0) > 0 ) then
	
		Select  max(cd_classif_setor)
		into	cd_classif_setor_w
		from	SETOR_ATENDIMENTO
		where	cd_setor_atendimento = cd_setor_atendimento_w;
		
		if  ( nvl(cd_classif_setor_w,'0') = '2') then
			qt_pont_w := 0; 
			if (ie_tipo_sepse_p = 'P') then
				qt_valor_w := null;
			else
				qt_valor_w := 0;
			end if;					  
		
		end if;
	
	end if;

end if;
if (ie_tipo_sepse_p = 'P') then
	
	if (nr_seq_atributo_p = 87) then --Mudanca aguda do estado neurologico
		begin
		select nvl(max(qt_hora_sv),24)
		into qt_hora_sv_w
		from SEPSE_ATRIBUTO_REGRA
		where NR_SEQ_ATRIBUTO = nr_seq_atributo_p;

		select 	count(*)
		into 	qt_nivel_cons_w
		from	atendimento_sinal_vital
		where	nr_atendimento = nr_atendimento_p
		and 	IE_NIVEL_CONSCIENCIA_PED is not null
		and 	ie_situacao = 'A'
		and 	(dt_atualizacao + (qt_hora_sv_w/24)) >= sysdate;
		
		if (qt_nivel_cons_w = 0) then
			ie_atributo_w := null;
			qt_valor_w := null;
		else 
			select 	max(substr(obter_valor_dominio(8921,ie_nivel),1,60)),
					to_number(max(ie_nivel))
			into  	ie_atributo_w,
					qt_valor_w
			from 	(	select IE_NIVEL_CONSCIENCIA_PED ie_nivel
						from	atendimento_sinal_vital
						where	nr_atendimento = nr_atendimento_p
						and 	ie_situacao = 'A'
						and IE_NIVEL_CONSCIENCIA_PED is not null
						and 	(dt_atualizacao + (qt_hora_sv_w/24)) >= sysdate
						order by NR_SEQUENCIA desc) 
			where rownum = 1
			group by ie_nivel;
		end if;
		end;
	end if;
	
	select ie_alteracao_manual
	into ie_alteracao_manual_w
	from escala_sepse_infantil
	where nr_sequencia = nr_seq_escala_p;

	if (ie_alteracao_manual_w <> 'S' or ie_alteracao_manual_w is null) then
		update	escala_sepse_infantil_item
		set	qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
			vl_atributo = qt_valor_w,
			ie_atributo = ie_atributo_w,
			ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
			ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
		where	nr_seq_escala = nr_seq_escala_p
		and	nr_seq_atributo = nr_seq_atributo_p;		
	end if;
	
else
	if	(nr_seq_atributo_p in (8,51)) then

		if (ie_atributo_w is null) or (ie_atributo_w = '') then
			select	max(substr(obter_valor_dominio(3342,ie_nivel_consciencia),1,60)),
					max(ie_nivel_consciencia)
			into	ie_atributo_w,
					ie_nivel_consciencia_w
			from	atendimento_sinal_vital
			where	nr_atendimento = nr_atendimento_p;
		end if;
		
		update	escala_sepse_item
		set		qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
				ie_atributo = ie_atributo_w,
				vl_atributo = to_number(ie_nivel_consciencia_w),
				ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
				ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
		where	nr_seq_escala = nr_seq_escala_p
		and		nr_seq_atributo = nr_seq_atributo_p;
	
--elsif	(nr_seq_atributo_p in (9,22,26)) thena
	else

		if	(nr_seq_atributo_p in (17,30,31,32, 59,41,42))then
			
				update	escala_sepse_item
				set	qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
					vl_atributo = qt_valor_w,
					ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
					ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
				where	nr_seq_escala = nr_seq_escala_p
				and	nr_seq_atributo = nr_seq_atributo_p;

		elsif	(nr_seq_atributo_p in (6,33,49))then
		
			Select  nvl(sum(qt_pontuacao),0)
			into	qt_regra_FC_w
			from	escala_sepse_item
			where	nr_seq_escala = nr_seq_escala_p
			and	nr_seq_atributo in (6,33);
			
			if	( qt_regra_FC_w = 0) then
			
				update	escala_sepse_item
				set	qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
					vl_atributo = qt_valor_w,
					ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
					ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
				where	nr_seq_escala = nr_seq_escala_p
				and	nr_seq_atributo = nr_seq_atributo_p;
			
			end if;
			
		elsif	(nr_seq_atributo_p in (21,34))then
		
			Select  nvl(sum(qt_pontuacao),0)
			into	qt_regra_discrasia_w
			from	escala_sepse_item
			where	nr_seq_escala = nr_seq_escala_p
			and	nr_seq_atributo in (21,34);
			
			if	( qt_regra_discrasia_w = 0) then
			
				update	escala_sepse_item
				set	qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
					vl_atributo = qt_valor_w,
					ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
					ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
				where	nr_seq_escala = nr_seq_escala_p
				and	nr_seq_atributo = nr_seq_atributo_p;
			
			end if;
			
		elsif	(nr_seq_atributo_p in (9,35))then
		
			Select  nvl(sum(qt_pontuacao),0)
			into	qt_regra_edema_w
			from	escala_sepse_item
			where	nr_seq_escala = nr_seq_escala_p
			and	nr_seq_atributo in (9,35);
			
			if	( qt_regra_edema_w = 0) then
			
				update	escala_sepse_item
				set	qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
					vl_atributo = qt_valor_w,
					ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
					ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
				where	nr_seq_escala = nr_seq_escala_p
				and	nr_seq_atributo = nr_seq_atributo_p;
			
			end if;		
	
		else
		
			update	escala_sepse_item
			set	qt_pontuacao = nvl(qt_pont_w,nvl(qt_pontuacao_w,0)),
				vl_atributo = qt_valor_w,
				ie_resultado = decode(nvl(qt_pont_w,0),0,'N','S'),
				ie_resultado_orig = decode(nvl(qt_pont_w,0),0,'N','S')
			where	nr_seq_escala = nr_seq_escala_p
			and	nr_seq_atributo = nr_seq_atributo_p;
		end if;
	end if;

end if;

commit;

end atualizar_pontuacao_sepse;
/
