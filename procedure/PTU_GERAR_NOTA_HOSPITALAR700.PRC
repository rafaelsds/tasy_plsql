create or replace
procedure ptu_gerar_nota_hospitalar700
			(	nr_seq_cobranca_p	Number,
				cd_interface_p		number,
				nm_usuario_p		Varchar2) is 

nm_hospital_w			varchar2(70);
cd_cgc_w			varchar2(14);
cd_doenca_w			varchar2(10);
cd_guia_w			varchar2(10);
ie_tipo_acomodacao_w		varchar2(2);
ie_obito_w			varchar2(2);
cd_motivo_saida_w		varchar2(2);
ie_complicacao_puerperio_w	varchar2(2);
ie_transtorno_w			varchar2(1);
ie_atend_parto_w		varchar2(1);
ie_compl_neonatal_w		varchar2(1);
ie_baixo_peso_w			varchar2(1);
ie_parto_cesaria_w		varchar2(1);
ie_parto_normal_w		varchar2(1);
ie_ind_acidente_w		varchar2(1);
ie_tipo_faturamento_w		varchar2(1);
ie_tipo_fat_w			Number(1);
ie_gestacao_w			varchar2(1);
nr_nota_fiscal_w		number(11);
nr_seq_protocolo_w		number(10);
nr_seq_clinica_w		number(10);
nr_seq_tipo_acomodacao_w	number(10);
nr_seq_saida_w			number(10);
nr_seq_fatura_w			number(10);
nr_seq_hospitalar_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_segurado_w		number(10);
nr_seq_conta_ww			Number(10);
nr_seq_congenere_w		number(8);
nr_seq_lote_w			number(8);
nr_seq_prestador_w		number(8);
cd_cnes_w			number(8);
cd_cooperativa_w		varchar2(10);
qt_nasc_vivos_pre_w		number(2);
qt_nasc_vivos_w			number(2);
qt_nasc_mortos_w		number(2);
qt_nasc_vivos_pre		number(2);
qt_obito_precoce_w		number(1);
qt_obito_tardio_w		number(1);
ie_tipo_internacao_w		number(1);
ie_obito_mulher_w		number(1);
dt_internacao_w			date;
dt_alta_w			date;
ie_regime_internacao_w		varchar2(1);
nr_nota_w			ptu_nota_hospitalar.nr_nota%type;
cd_hospital_w			number(8);
cd_unimed_local_w		varchar2(10);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_versao_w			number(10);

cursor C01 is
	select	b.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_clinica,
		a.nr_seq_tipo_acomodacao,
		a.ie_indicacao_acidente,
		a.nr_seq_saida_int,
		to_number(a.qt_nasc_vivos_prematuros),
		nvl(a.dt_entrada,sysdate),
		a.ie_tipo_faturamento,
		nvl(a.dt_alta,sysdate),
		to_number(a.qt_nasc_vivos),
		to_number(a.qt_nasc_mortos),
		to_number(a.qt_obito_precoce),
		to_number(a.qt_obito_tardio),
		a.ie_gestacao,
		a.ie_obito,
		a.ie_transtorno,
		a.ie_complicacao_puerperio,
		a.ie_atend_rn_sala_parto,
		a.ie_complicacao_neonatal,
		a.ie_baixo_peso,
		a.ie_parto_cesaria,
		a.ie_parto_normal,
		a.ie_obito_mulher,
		a.nr_sequencia,
		b.nr_seq_segurado,
		a.ie_regime_internacao
	from	pls_protocolo_conta	b,
		pls_conta		a
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	pls_obter_se_internado(a.nr_sequencia,'X') = 'S'
	and	a.nr_sequencia		= nr_seq_conta_ww
	group by b.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_clinica,
		a.nr_seq_tipo_acomodacao,
		a.ie_indicacao_acidente,
		a.nr_seq_saida_int,
		a.qt_nasc_vivos_prematuros,
		a.dt_entrada,
		a.ie_tipo_faturamento,
		a.dt_alta,
		a.qt_nasc_vivos,
		a.qt_nasc_mortos,
		a.qt_obito_precoce,
		a.qt_obito_tardio,
		a.ie_gestacao,
		a.ie_obito,
		a.ie_transtorno,
		a.ie_complicacao_puerperio,
		a.ie_atend_rn_sala_parto,
		a.ie_complicacao_neonatal,
		a.ie_baixo_peso,
		a.ie_parto_cesaria,
		a.ie_parto_normal,
		a.ie_obito_mulher,
		a.nr_sequencia,
		b.nr_seq_segurado,
		a.ie_regime_internacao;

cursor C02 is
	select	a.qt_nasc_vivos,
		a.qt_nasc_mortos,
		b.cd_doenca	
	from	pls_diagnostico_conta	b,
		pls_conta		a
	where	a.nr_sequencia	= b.nr_seq_conta
	and	a.nr_sequencia	= nr_seq_conta_w;

begin
select	max(b.nr_nota),
	max(a.cd_estabelecimento)
into	nr_nota_w,
	cd_estabelecimento_w
from	ptu_nota_cobranca	b,
	ptu_servico_pre_pagto	a
where	b.nr_seq_serv_pre_pagto	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_cobranca_p;

nr_seq_conta_ww := null;

if	(somente_numero(nr_nota_w) > 0) then
	nr_seq_conta_ww := somente_numero(nr_nota_w);
end if;

cd_unimed_local_w	:= pls_obter_unimed_estab(cd_estabelecimento_w);
ie_versao_w		:= somente_numero(ptu_obter_versao_dominio('A700', cd_interface_p));

open C01;
loop
fetch C01 into
	nr_seq_protocolo_w,
	nr_seq_prestador_w,
	nr_seq_clinica_w,
	nr_seq_tipo_acomodacao_w,
	ie_ind_acidente_w,
	nr_seq_saida_w,
	qt_nasc_vivos_pre_w,
	dt_internacao_w,
	ie_tipo_faturamento_w,
	dt_alta_w,
	qt_nasc_vivos_w,
	qt_nasc_mortos_w,
	qt_obito_precoce_w,
	qt_obito_tardio_w,
	ie_gestacao_w,
	ie_obito_w,
	ie_transtorno_w,
	ie_complicacao_puerperio_w,
	ie_atend_parto_w,
	ie_compl_neonatal_w,
	ie_baixo_peso_w,
	ie_parto_cesaria_w,
	ie_parto_normal_w,
	ie_obito_mulher_w,
	nr_seq_conta_w,
	nr_seq_segurado_w,
	ie_regime_internacao_w;
exit when C01%notfound;
	begin
	case	ie_tipo_faturamento_w
		when 'T' then ie_tipo_fat_w	:= 1; -- Total
		when 'P' then ie_tipo_fat_w	:= 2; -- Parcial
		when 'F' then ie_tipo_fat_w	:= 3; -- Final
		when 'C' then ie_tipo_fat_w	:= 4; -- Complementar
		else ie_tipo_fat_w		:= 1;
	end case;
	
	select	max(nr_seq_congenere)
	into	nr_seq_congenere_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	select	nvl(max(cd_doenca),1)
	into	cd_doenca_w
	from	pls_diagnostico_conta
	where	nr_seq_conta	= nr_seq_conta_w
	and	ie_classificacao = 'P';
	begin
	select	substr(upper(elimina_acentos(pls_obter_dados_prestador(nr_sequencia,'N'))),1,70),
		decode(cd_pessoa_fisica,null,substr(obter_dados_pf_pj(null,cd_cgc,'CNES'),1,8),substr(obter_dados_pf(cd_pessoa_fisica,'CNES'),1,8)),
		substr(cd_cgc,1,14),
		somente_numero(substr(cd_prest_a400,1,8))
	into	nm_hospital_w,
		cd_cnes_w,
		cd_cgc_w,
		cd_hospital_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_w;
	exception
	when others then
		nm_hospital_w	:= null;
		cd_cnes_w	:= null;
		cd_cgc_w	:= null;
	end;
	
	cd_hospital_w := nvl(cd_hospital_w,nr_seq_prestador_w);
	
	select	max(cd_ptu)
	into	cd_motivo_saida_w
	from	pls_motivo_saida
	where	nr_sequencia	= nr_seq_saida_w;
	
	select	nvl(max(ie_tipo_acomodacao_ptu),'A')
	into	ie_tipo_acomodacao_w
	from	pls_tipo_acomodacao
	where	nr_sequencia	= nr_seq_tipo_acomodacao_w;
	
	select	nvl(max(cd_ptu),1)
	into	ie_tipo_internacao_w
	from	pls_clinica
	where	nr_sequencia = nr_seq_clinica_w;
	
	-- se for pediatrica
	if	(ie_tipo_internacao_w = 6) then
		ie_tipo_internacao_w	:= 4;
	end if;
	
	-- se for psiquiatrica
	if	(ie_tipo_internacao_w = 7) then
		ie_tipo_internacao_w	:= 5;
	end if;
	
	select	nvl(max(cd_cooperativa),1)
	into	cd_cooperativa_w
	from	pls_congenere
	where	nr_sequencia	= nr_seq_congenere_w;
	
	if	(ie_ind_acidente_w is null) then
		ie_ind_acidente_w	:= '0';
	elsif	(ie_ind_acidente_w = '0') then
		ie_ind_acidente_w	:= '1';
	elsif	(ie_ind_acidente_w = '1') then
		ie_ind_acidente_w	:= '2';
	elsif	(ie_ind_acidente_w = '1') then
		ie_ind_acidente_w	:= '3';
	end if;
	
	nr_seq_lote_w	:= to_number(nr_seq_conta_w + nr_seq_protocolo_w);
	
	select	ptu_nota_hospitalar_seq.nextval
	into	nr_seq_hospitalar_w
	from	dual;	
	
	if	(somente_numero(pls_obter_versao_tiss) >= 30000) then
		qt_nasc_vivos_w			:= 0;
		qt_nasc_mortos_w		:= 0;
		qt_nasc_vivos_pre_w		:= 0;
		qt_obito_precoce_w		:= 0;
		qt_obito_tardio_w		:= 0;
		ie_gestacao_w			:= ' ';		-- ID_INT_OBSTETRICIA_1
		ie_obito_w			:= ' ';		-- ID_INT_OBSTETRICIA_2
		ie_transtorno_w			:= ' ';		-- ID_INT_OBSTETRICIA_3
		ie_complicacao_puerperio_w	:= ' ';		-- ID_INT_OBSTETRICIA_4
		ie_atend_parto_w		:= ' ';		-- ID_INT_OBSTETRICIA_5
		ie_compl_neonatal_w		:= ' ';		-- ID_INT_OBSTETRICIA_6
		ie_baixo_peso_w			:= ' ';		-- ID_INT_OBSTETRICIA_7
		ie_parto_cesaria_w		:= ' ';		-- ID_INT_OBSTETRICIA_8
		ie_parto_normal_w		:= ' ';		-- ID_INT_OBSTETRICIA_9
		ie_obito_mulher_w		:= null;
	end if;
	
	-- Se for versao anterior a versao 9.0
	if	(ie_versao_w < 90) then
		nm_hospital_w := substr(nm_hospital_w,1,25);
	end if;
	
	-- Versao 10.0a ou superior
	if	(ie_versao_w >= 101) and
		(ie_regime_internacao_w = '3') then -- (Domiciliar)
		cd_hospital_w := '0';
		cd_unimed_local_w := '0';
		ie_tipo_acomodacao_w := null;
		cd_cgc_w := '0';
		nm_hospital_w := null;
	end if;
	
	insert into ptu_nota_hospitalar
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec, 
		nr_seq_nota_cobr,
		nr_lote,
		nr_nota,
		cd_unimed_hospital,
		cd_hospital,
		qt_obito_tardio,
		ie_int_gestacao,
		ie_int_aborto, 
		ie_int_transtorno,
		nm_hospital,
		ie_tipo_acomodacao,
		dt_internacao, 
		dt_alta,
		tx_mult_amb,
		ie_ind_acidente,
		cd_motivo_saida,
		cd_cgc_hospital,
		ie_tipo_internacao,
		qt_nasc_vivos,
		qt_nasc_mortos, 
		qt_nasc_vivos_pre,
		qt_obito_precoce,
		ie_int_puerperio,
		ie_int_recem_nascido, 
		ie_int_neonatal,
		ie_int_baixo_peso,
		ie_int_parto_cesarea,
		ie_int_parto_normal,
		ie_faturamento,
		cd_cid_obito,
		ie_obito_mulher,
		nr_declara_obito,
		reg_internacao)
	values	(nr_seq_hospitalar_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p, 
		nr_seq_cobranca_p,
		nr_seq_lote_w,
		nr_seq_conta_w,
		cd_unimed_local_w,
		cd_hospital_w,
		qt_obito_tardio_w,
		ie_gestacao_w,
		ie_obito_w, 
		ie_transtorno_w,
		nm_hospital_w,
		ie_tipo_acomodacao_w,
		dt_internacao_w, 
		dt_alta_w,
		decode(ie_tipo_acomodacao_w,'A','1','2'),
		ie_ind_acidente_w,
		cd_motivo_saida_w,
		cd_cgc_w,
		ie_tipo_internacao_w,
		qt_nasc_vivos_w,
		qt_nasc_mortos_w, 
		qt_nasc_vivos_pre_w,
		qt_obito_precoce_w,
		ie_complicacao_puerperio_w,
		ie_atend_parto_w, 
		ie_compl_neonatal_w,
		ie_baixo_peso_w,
		ie_parto_cesaria_w,
		ie_parto_normal_w,
		ie_tipo_fat_w,
		cd_doenca_w,
		ie_obito_mulher_w,
		'1',
		ie_regime_internacao_w);
		
	open C02;
	loop
	fetch C02 into
		qt_nasc_vivos_w,
		qt_nasc_mortos_w,
		cd_doenca_w;
	exit when C02%notfound;
		begin
		insert into ptu_nota_hosp_compl
			(nr_sequencia,
			nr_seq_nota_hosp,
			dt_atualizacao, 
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec, 
			nr_declara_vivo,
			cd_cid_obito,
			nr_declara_obito)
		values	(ptu_nota_hosp_compl_seq.nextval,
			nr_seq_hospitalar_w,
			sysdate, 
			nm_usuario_p,
			sysdate,
			nm_usuario_p, 
			qt_nasc_vivos_w,
			cd_doenca_w,
			qt_nasc_mortos_w);
		end;
	end loop;
	close C02;

	end;
end loop;
close C01;

end ptu_gerar_nota_hospitalar700;
/