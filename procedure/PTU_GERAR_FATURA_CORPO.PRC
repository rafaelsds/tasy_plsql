create or replace
procedure ptu_gerar_fatura_corpo(nr_seq_fatura_p	number,
				nm_usuario_p		varchar2) is 

ds_linhas_w			varchar2(74);
nr_linha_w			number(2);
nr_seq_evento_w			number(10);
nr_seq_pls_fatura_w		number(10);
vl_evento_w			number(12,2);
nr_seq_fat_corpo_w		number(10);
nr_sequencia_w			number(10);
vl_total_fatura_w		number(15,2) := 0;
vl_total_corpo_w		number(15,2) := 0;
vl_faturado_w			number(15,2) := 0;
vl_faturado_ndc_w		number(15,2) := 0;
nr_titulo_w			ptu_fatura.nr_fatura%type;
nr_titulo_ndc_w			number(10);
ie_documento_w			varchar2(1) := null;

cursor c01 is
	select	nr_seq_evento,
		sum(nvl(vl_evento,0)),
		nr_sequencia
	from	pls_fatura_evento
	where	nr_seq_fatura  = nr_seq_pls_fatura_w	
	group by nr_seq_evento,
		nr_sequencia;
		
begin

select	max(nr_seq_pls_fatura),
	max(nr_fatura),
	to_number(max(nr_nota_credito_debito))
into	nr_seq_pls_fatura_w,
	nr_titulo_w,
	nr_titulo_ndc_w
from	ptu_fatura
where	nr_sequencia = nr_seq_fatura_p;

if	(nr_titulo_ndc_w is not null) and (nr_titulo_w is not null) then
	ie_documento_w := null;
elsif	(nr_titulo_ndc_w is not null) then
	ie_documento_w := '1';	
elsif	(nr_titulo_w is not null) then
	ie_documento_w := '2';
end if;

open c01;
loop
fetch c01 into
	nr_seq_evento_w,
	vl_evento_w,
	nr_sequencia_w;
exit when c01%notfound;
	begin
	select 	substr(upper(elimina_acentos(ds_evento)),1,74)
	into	ds_linhas_w
	from	pls_evento_faturamento
	where	nr_sequencia = nr_seq_evento_w;
	
	select	sum(vl_faturado),
		sum(vl_faturado_ndc)
	into	vl_faturado_w,
		vl_faturado_ndc_w
	from	pls_fatura_conta
	where	nr_seq_fatura_evento = nr_sequencia_w;
	
	if	(nr_titulo_ndc_w is not null) and (nr_titulo_w is not null) then
		if	(vl_faturado_ndc_w > 0) then
			select	nvl(max(nr_linha),0) + 1
			into	nr_linha_w
			from	ptu_fatura_corpo
			where	nr_seq_fatura = nr_seq_fatura_p;
			
			select	ptu_fatura_corpo_seq.nextval
			into	nr_seq_fat_corpo_w
			from	dual;
		
			insert into ptu_fatura_corpo
				(nr_sequencia,
				nr_seq_fatura,
				nr_linha,
				ds_linha,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				vl_item,
				ie_documento)
			values	(nr_seq_fat_corpo_w,
				nr_seq_fatura_p, 
				nr_linha_w,
				ds_linhas_w, 
				sysdate, 
				nm_usuario_p,
				sysdate, 
				nm_usuario_p, 
				vl_faturado_ndc_w,
				nvl(ie_documento_w,'1'));
		end if;

		if	(vl_faturado_w > 0) then
			select	nvl(max(nr_linha),0) + 1
			into	nr_linha_w
			from	ptu_fatura_corpo
			where	nr_seq_fatura = nr_seq_fatura_p;
			
			select	ptu_fatura_corpo_seq.nextval
			into	nr_seq_fat_corpo_w
			from	dual;

			insert into ptu_fatura_corpo
				(nr_sequencia,
				nr_seq_fatura,
				nr_linha,
				ds_linha,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				vl_item,
				ie_documento)
			values	(nr_seq_fat_corpo_w,
				nr_seq_fatura_p, 
				nr_linha_w,
				ds_linhas_w, 
				sysdate, 
				nm_usuario_p,
				sysdate, 
				nm_usuario_p, 
				vl_faturado_w,
				nvl(ie_documento_w,'2'));
		end if;
	else
		select	nvl(max(nr_linha),0) + 1
		into	nr_linha_w
		from	ptu_fatura_corpo
		where	nr_seq_fatura = nr_seq_fatura_p;
		
		select	ptu_fatura_corpo_seq.nextval
		into	nr_seq_fat_corpo_w
		from	dual;
	
		insert into ptu_fatura_corpo
			(nr_sequencia,
			nr_seq_fatura,
			nr_linha,
			ds_linha,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec, 
			nm_usuario_nrec, 
			vl_item,
			ie_documento)
		values	(nr_seq_fat_corpo_w,
			nr_seq_fatura_p, 
			nr_linha_w,
			ds_linhas_w, 
			sysdate, 
			nm_usuario_p,
			sysdate, 
			nm_usuario_p, 
			vl_faturado_ndc_w + vl_faturado_w,
			nvl(ie_documento_w,'1'));
	end if;
	end;
end loop;
close C01;

if	(nr_seq_fat_corpo_w is not null) then	
	select	nvl(sum(vl_fatura),0)
	into	vl_total_fatura_w
	from	pls_fatura
	where	nr_sequencia	= nr_seq_pls_fatura_w;
	
	select	nvl(sum(vl_item),0)
	into	vl_total_corpo_w
	from	ptu_fatura_corpo
	where	nr_seq_fatura	= nr_seq_fatura_p;
	
	if	(vl_total_fatura_w > vl_total_corpo_w) then
		ds_linhas_w := 'VALOR INTERCAMBIO';
	
		select	nvl(max(nr_linha),0) + 1
		into	nr_linha_w
		from	ptu_fatura_corpo
		where	nr_seq_fatura = nr_seq_fatura_p;
		
		select	ptu_fatura_corpo_seq.nextval
		into	nr_seq_fat_corpo_w
		from	dual;
		
		vl_evento_w := nvl(vl_total_fatura_w,0) - nvl(vl_total_corpo_w,0);
	
		insert into ptu_fatura_corpo
			(nr_sequencia, 
			nr_seq_fatura,
			nr_linha,
			ds_linha, 
			dt_atualizacao, 
			nm_usuario,
			dt_atualizacao_nrec, 
			nm_usuario_nrec, 
			vl_item,
			ie_documento)
		values	(nr_seq_fat_corpo_w, 
			nr_seq_fatura_p,
			nr_linha_w,
			ds_linhas_w, 
			sysdate, 
			nm_usuario_p,
			sysdate, 
			nm_usuario_p, 
			vl_evento_w,
			nvl(ie_documento_w,'2'));
	end if;
end if;

end ptu_gerar_fatura_corpo;
/