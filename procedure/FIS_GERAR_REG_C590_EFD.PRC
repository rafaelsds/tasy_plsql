create or replace
procedure fis_gerar_reg_c590_efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				nr_seq_superior_p	number,
				qt_linha_p	in out	number,
				nr_sequencia_p	in out	number) is 

nr_seq_regra_efd_w		number(10);
cd_tributo_pis_w		number(3);
cd_tributo_cofins_w		number(3);
nr_versao_efd_w			varchar2(5);
cd_st_pis_w		varchar(15);
cd_st_cofins_w		varchar(15);
tp_registro_w			varchar2(4);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;
ie_tipo_nota_w		varchar2(1);

cursor c01 is
select	distinct
	'C590' tp_registro,
	substr(nvl(c.ie_origem_mercadoria,'0') || nvl(c.ie_tributacao_icms,'10'),1,3) cd_st_icms,
	substr(Elimina_Caracter(obter_dados_natureza_operacao(a.cd_natureza_operacao, 'CF'), '.'), 1, 4) cd_cfop,
	nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TX') vl_aliquota_icms,
	sum(b.vl_total_item_nf) vl_operacao,
	sum(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'BC')) vl_base_icms,
	sum(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TRIB')) vl_icms,
	sum(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMSST', 'BC')) vl_base_icms_st,
	sum(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMSST', 'TRIB')) vl_icms_st,
	0 vl_red_bc,
	'' cod_obs
from	material_fiscal c,
	nota_fiscal_item b,
	nota_fiscal a
where	a.nr_sequencia		= b.nr_sequencia
and	b.cd_material		= c.cd_material(+)
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.nr_sequencia		= nr_seq_superior_p
group by 	'C590',
		substr(nvl(c.ie_origem_mercadoria,'0') || nvl(c.ie_tributacao_icms,'10'),1,3),
		substr(Elimina_Caracter(obter_dados_natureza_operacao(a.cd_natureza_operacao, 'CF'), '.'), 1, 4),
		nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TX');

vet01	c01%rowtype;

begin

open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin	
	
	ds_linha_w	:= substr(	sep_w || vet01.tp_registro			|| 
					sep_w || vet01.cd_st_icms			||
					sep_w || vet01.cd_cfop				||
					sep_w || vet01.vl_aliquota_icms			||
					sep_w || vet01.vl_operacao			||
					sep_w || vet01.vl_base_icms			||
					sep_w || vet01.vl_icms				||
					sep_w || vet01.vl_base_icms_st			||
					sep_w || vet01.vl_icms_st			||
					sep_w || vet01.vl_red_bc			||
					sep_w || vet01.cod_obs				|| 
					sep_w,1,8000);
	
	ds_arquivo_w			:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w		:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w		:= nr_seq_registro_w + 1;
	nr_linha_w			:= nr_linha_w + 1;

	insert into fis_efd_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_efd,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		'C590',
		ds_arquivo_w,
		ds_arquivo_compl_w);
	end;
end loop;
close c01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_c590_efd;
/
