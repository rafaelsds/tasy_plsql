create or replace 
procedure recalcular_regra_esforco (
			nr_sequencia_p			number,
			nm_usuario_p			varchar2) is		


ie_regra_w				number (4);
ie_gap_w				varchar(1);
qt_reg_w				number(2);
vl_base_w				number(10);
qt_tempo_horas_w		latam_requisito.qt_tempo_horas%type;
qt_total_horas_w		latam_requisito.qt_tempo_horas%type;
nr_sequencia_w			latam_requisito.nr_sequencia%type;
nr_seq_gap_w			latam_requisito.nr_seq_gap%type;
ie_tamanho_w			latam_requisito.ie_esforco_desenv%type;	
ie_complexidade_w		latam_requisito.ie_complexidade%type;
pr_confianca_w			latam_requisito.pr_confianca%type;
nr_seq_prs_w			latam_requisito.nr_seq_prs%type;

Cursor C01 is
	select	nr_sequencia,
			nr_seq_gap,
			nr_seq_prs,
			ie_esforco_desenv,
			ie_complexidade,
			pr_confianca
	from	latam_requisito
	where	nr_seq_gap	= nr_sequencia_p
	and		ie_sob_medida	= 'N'
	union
	select	a.nr_sequencia,
			a.nr_seq_gap,
			a.nr_seq_prs,
			a.ie_esforco_desenv,
			a.ie_complexidade,
			a.pr_confianca
	from	latam_requisito a,
			latam_requisito_prs b,
			latam_requisito_crs c,
			latam_subprocesso d,
			latam_modulo e
	where	a.nr_seq_prs = b.nr_sequencia
	and  	b.nr_seq_requisito_crs = c.nr_sequencia
	and   	c.nr_seq_latam_subprocesso = d.nr_sequencia
	and   	d.nr_seq_modulo = e.nr_sequencia
	and   	e.nr_seq_gap	= nr_sequencia_p
	and		a.ie_sob_medida	= 'N';

begin
qt_tempo_horas_w := 0;
qt_total_horas_w := 0;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nr_seq_gap_w,
	nr_seq_prs_w,
	ie_tamanho_w,
	ie_complexidade_w,
	pr_confianca_w;
exit when C01%notfound;
	begin
			qt_tempo_horas_w := nvl(calcular_esforco_requisito(nr_sequencia_w, nr_seq_gap_w, nr_seq_prs_w, ie_tamanho_w, ie_complexidade_w, pr_confianca_w),0);
			
			update	latam_requisito
			set	qt_tempo_horas = qt_tempo_horas_w
			where 	nr_sequencia = nr_sequencia_w;
			
			qt_total_horas_w := qt_total_horas_w + qt_tempo_horas_w;
											
	end;
end loop;
close C01;

calcular_esforco_gap(nr_sequencia_p, qt_total_horas_w, nm_usuario_p);


commit;
END recalcular_regra_esforco;
/