
create or replace procedure nom_gerar_sumario_paciente(	dt_sumario_p			date,
														ds_oid_identificador_p	varchar2, 
														ds_identificador_p		varchar2,
														cd_curp_paciente_p		varchar2,
														cd_hosp_destino_p		varchar2,
														cd_prof_destino_p		varchar2,
														nm_prof_destino_p		varchar2,
														cd_hosp_origem_p		varchar2,
														nm_hosp_origem_p		varchar2,
														cd_prof_solic_p			varchar2,
														nm_prof_solic_p			varchar2,
														nm_usuario_p			varchar2,
														cd_estabelecimento_p		number,
														ds_erro					out varchar2,
														nr_sequencia_p			out number) is
ie_existe_w					number;
cd_cgc_origem_w				pessoa_juridica.cd_cgc%type;
cd_cgc_destino_w			pessoa_juridica.cd_cgc%type;
cd_pessoa_fisica_solic_w	pessoa_fisica.cd_pessoa_fisica%type;
cd_pessoa_fisica_origem_w	pessoa_fisica.cd_pessoa_fisica%type;
cd_pessoa_fisica_pac_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_sequencia_w				imp_sumario_paciente.nr_sequencia%type;
	
	
begin

	ds_erro	:= null;
	
	select	count(*)
	into	ie_existe_w
	from 	imp_sumario_paciente
	where	ds_oid_identificador = ds_oid_identificador_p
	and		ds_identificador = ds_identificador_p;
	
	if(ie_existe_w > 0 ) then
		ds_erro := obter_desc_expressao(882546) || ';' ||  ds_erro;
	end if;
	
	select	count(*)
	into	ie_existe_w
	from	estabelecimento e, pessoa_juridica pj, cat_clues c
	where	e.cd_estabelecimento = cd_estabelecimento_p
	and		e.cd_cgc = pj.cd_cgc
	and		pj.cd_internacional = c.cd_clues
	and		c.cd_clues = cd_hosp_destino_p;

	if(ie_existe_w = 0 ) then
		ds_erro := obter_desc_expressao(882566) || ';' ||  ds_erro;
	end if;
	
	select	count(*)
	into	ie_existe_w
	from	pessoa_fisica
	where  cd_curp = cd_curp_paciente_p;
	
	if(ie_existe_w = 0) then
		ds_erro := obter_desc_expressao(882568) || ';' ||  ds_erro;	
	end if;
	
	if(ds_erro is null) then
		
		select	max(pj.cd_cgc)
		into	cd_cgc_origem_w
		from	estabelecimento e, 
				pessoa_juridica pj, 
				cat_clues c
		where	e.cd_estabelecimento = cd_estabelecimento_p
		and		e.cd_cgc = pj.cd_cgc
		and		pj.cd_internacional = c.cd_clues
		and		c.cd_clues = cd_hosp_origem_p;
		
		select	max(pj.cd_cgc)
		into	cd_cgc_destino_w
		from	estabelecimento e, 
				pessoa_juridica pj, 
				cat_clues c
		where	e.cd_estabelecimento = cd_estabelecimento_p
		and		e.cd_cgc = pj.cd_cgc
		and		pj.cd_internacional = c.cd_clues
		and		c.cd_clues = cd_hosp_destino_p;
		
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_solic_w
		from	pessoa_fisica
		where	cd_curp = cd_prof_solic_p;
		
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_origem_w
		from	pessoa_fisica
		where	cd_curp = cd_prof_destino_p;
		
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_pac_w
		from	pessoa_fisica
		where	cd_curp = cd_curp_paciente_p;	
		
		select 	imp_sumario_paciente_seq.nextval
		into	nr_sequencia_w
		from 	dual;
	
		insert into imp_sumario_paciente(
										nr_sequencia,
										cd_estabelecimento,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										ie_tipo_sumario,
										cd_pessoa_fisica,
										dt_sumario,
										cd_hosp_origem,
										nm_hosp_origem,
										cd_prof_solic,
										nm_prof_solic,
										cd_hosp_destino,
										cd_prof_destino,
										nm_prof_destino,
										ds_oid_identificador,
										ds_identificador)
								values(
										nr_sequencia_w,
										cd_estabelecimento_p,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										'NOM',
										cd_pessoa_fisica_pac_w,
										dt_sumario_p,
										cd_cgc_origem_w,
										nm_hosp_origem_p,
										cd_pessoa_fisica_solic_w,
										nm_prof_solic_p,
										cd_cgc_destino_w,
										cd_pessoa_fisica_origem_w,
										nm_prof_destino_p,
										ds_oid_identificador_p,
										ds_identificador_p);
										
		nr_sequencia_p	:= nr_sequencia_w;
		
		commit;
		
	end if;	

end nom_gerar_sumario_paciente;
/