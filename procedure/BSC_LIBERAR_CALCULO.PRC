create or replace
procedure bsc_liberar_calculo(	nr_sequencia_p		number,
				ie_operacao_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

				
nr_seq_inf_w			number(10);
ie_lib_indicador_w		varchar2(1);
ie_gerar_nao_conf_w		varchar2(1);
nr_seq_tipo_w			number(10);
qt_meta_w			number(15,2);
qt_real_w			number(15,2);
ie_regra_result_w		varchar2(15);
cd_pf_abertura_w		varchar2(10);
nr_seq_indicador_w		number(10);
nr_seq_calc_w			number(10);
cd_setor_atendimento_w		number(10);

cursor	c01 is
select	a.nr_seq_inf
from	bsc_calc_indicador a
where	a.nr_seq_calc		= nr_sequencia_p;

Cursor C02 is
	select	c.ie_regra_result,
		b.qt_meta,
		b.qt_real,
		c.nr_sequencia,
		a.nr_seq_calc
	from	bsc_indicador c,
		bsc_calc_indicador a,
		bsc_ind_inf b
	where	b.nr_sequencia 		= a.nr_seq_inf
	and	b.nr_seq_indicador	= a.nr_seq_indicador
	and	b.nr_seq_indicador	= c.nr_sequencia
	and	a.nr_seq_calc           = nr_sequencia_p
	and	c.ie_regra_result	in ('MA','ME');	
				
begin

ie_lib_indicador_w	:= nvl(substr(obter_valor_param_usuario(7024, 43, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),1,1), 'N');
obter_param_usuario(7024,96,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_nao_conf_w);
obter_param_usuario(7024,97,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_seq_tipo_w);

if	(ie_operacao_p = 'L') then
	
	update	bsc_calculo
	set	dt_liberacao 	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	
elsif	(ie_operacao_p = 'E') then

	update	bsc_calculo
	set	dt_liberacao 	= null,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	
end if;


if	(ie_lib_indicador_w = 'S') then
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_inf_w;
	exit when C01%notfound;
		begin
		
		update	bsc_ind_inf
		set	ie_fechado	= decode(ie_operacao_p, 'L', 'F', 'A'),
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_inf_w;
		
		end;
	end loop;
	close C01;	

	if	(ie_gerar_nao_conf_w = 'S') and
		(nr_seq_tipo_w is not null) then			
		open C02;
		loop
		fetch C02 into	
			ie_regra_result_w,
			qt_meta_w,
			qt_real_w,
			nr_seq_indicador_w,
			nr_seq_calc_w;
		exit when C02%notfound;
			begin
			if	((ie_regra_result_w = 'MA') and
				(qt_real_w < qt_meta_w)) or
				((ie_regra_result_w = 'ME') and
				(qt_real_w > qt_meta_w)) then
				
				select	max(a.cd_pessoa_fisica)
				into	cd_pf_abertura_w
				from	bsc_ind_resp a
				where	nr_seq_indicador = nr_seq_indicador_w
				and	trunc(DT_INICIO_VIGENCIA) <= trunc(sysdate)
				and	(trunc(DT_FIM_VIGENCIA) >= trunc(sysdate) or DT_FIM_VIGENCIA is null);
				
				if	(cd_pf_abertura_w is null) then
					select	max(cd_pessoa_fisica)
					into	cd_pf_abertura_w
					from	bsc_calculo
					where	nr_sequencia = nr_seq_calc_w;
					
				end if;	
				
				select	max(cd_setor_atendimento)
				into	cd_setor_atendimento_w
				from	usuario
				where	cd_pessoa_fisica = cd_pf_abertura_w;
				
				insert into qua_nao_conformidade (	
					NR_SEQUENCIA,
					NR_SEQ_TIPO,
					DT_ABERTURA,
					CD_PF_ABERTURA,
					CD_SETOR_ATENDIMENTO,
					DS_NAO_CONFORMIDADE,
					IE_STATUS,
					CD_ESTABELECIMENTO,
					DT_ATUALIZACAO,
					NM_USUARIO)
				values(	qua_nao_conformidade_seq.nextVal,
					nr_seq_tipo_w,
					sysdate,
					cd_pf_abertura_w,
					cd_setor_atendimento_w,
					wheb_mensagem_pck.get_texto(800304)||' '||substr(obter_desc_bsc_indicador(nr_seq_indicador_w),1,255)||'.',
					WHEB_MENSAGEM_PCK.get_texto(311868),
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p);				
			end if;
			end;
		end loop;
		close C02;
		
	elsif	(ie_gerar_nao_conf_w = 'S') and
		(nr_seq_tipo_w is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(248186);	
	end if;
end if;

commit;

end bsc_liberar_calculo;
/