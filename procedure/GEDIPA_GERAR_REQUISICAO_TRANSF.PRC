create or replace
procedure gedipa_gerar_requisicao_transf(
			nr_requisicao_p			number,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number,
			cd_material_p			number,
			qt_material_p			number,
			cd_local_estoque_p		number,
			cd_local_destino_p		number,
			cd_operacao_estoque_p		number,
			nr_seq_lote_fornec_p		number,
			cd_motivo_baixa_p		number,
			ie_liberar_p			varchar2,
			nr_sequencia_p     	 in out number,
			ds_erro_p	            out varchar2) is 

nr_sequencia_w			number(10);
cd_unidade_medida_w		varchar2(30);
cd_unidade_medida_estoque_w	varchar2(30);
qt_conv_estoque_cons_w		number(13,4);
qt_estoque_w			number(13,4);
ds_erro_w			varchar2(2000);
nr_seq_erro_w			number(10);
nr_item_requisicao_w			number(10);
qt_atendida_w			number(13,4);
cd_pessoa_requisitante_w	number(10);
qt_existe_requisicao_w		varchar2(1 char);

/*594158*/

cursor c01 is
select	nr_sequencia,
	qt_material_requisitada
from	item_requisicao_material
where	nr_requisicao = nr_requisicao_p;
	
begin

cd_pessoa_requisitante_w := substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10);

if	(nr_requisicao_p is null) or (nr_requisicao_p = 0) then
	begin
	
	select	requisicao_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	nr_sequencia_p := nr_sequencia_w;
	
	insert into requisicao_material(
		nr_requisicao,
		cd_estabelecimento,
		cd_local_estoque,
		dt_solicitacao_requisicao,
		dt_atualizacao,
		nm_usuario,
		cd_operacao_estoque,
		cd_pessoa_requisitante,
		cd_local_estoque_destino,
		nm_usuario_lib,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_geracao,
		ie_urgente)
	values(	nr_sequencia_w,
		cd_estabelecimento_p,
		cd_local_estoque_p,
		sysdate,
		sysdate,
		nm_usuario_p,
		cd_operacao_estoque_p,
		cd_pessoa_requisitante_w,
		cd_local_destino_p,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'I',
		'N');
	
	end;
else
	begin
	
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p;
	
	cd_unidade_medida_w 		:= obter_dados_material_estab(cd_material_p,cd_estabelecimento_p,'UMS');
	cd_unidade_medida_estoque_w 	:= obter_dados_material_estab(cd_material_p,cd_estabelecimento_p,'UME');
	qt_conv_estoque_cons_w 		:= obter_conversao_material(cd_material_p,'CE');
	
	insert into item_requisicao_material(
		nr_requisicao,
		nr_sequencia,
		cd_estabelecimento,
		cd_material,
		qt_material_requisitada,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		cd_unidade_medida,
		qt_estoque,
		cd_unidade_medida_estoque,
		nr_seq_lote_fornec)
	values( nr_requisicao_p,
		nr_sequencia_w,
		cd_estabelecimento_p,
		cd_material_p,
		qt_material_p,
		0,
		sysdate,
		nm_usuario_p,
		cd_unidade_medida_w,
		qt_estoque_w,
		cd_unidade_medida_estoque_w,
		nr_seq_lote_fornec_p);
	
	end;
end if;
	
commit;

if	(ie_liberar_p = 'S') then
	consistir_requisicao(nr_requisicao_p,nm_usuario_p,cd_local_destino_p,null,'N','N','N','N','S','N','N',cd_operacao_estoque_p,ds_erro_p);
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_erro_w
	from	requisicao_mat_consist
	where	nr_requisicao = nr_requisicao_p;
	
	if	(nvl(nr_seq_erro_w,0) <> 0) then
		select	nvl(substr(cd_material || ' - ' || ds_consistencia,1,2000),null)
		into	ds_erro_p
		from	requisicao_mat_consist
		where	nr_sequencia = nr_seq_erro_w;
	end if;
	
	select	coalesce(max('S'), 'N')
	into	qt_existe_requisicao_w
	from	item_requisicao_material a
	where	a.nr_requisicao	= nr_requisicao_p
	and 	rownum = 1;
	
	if (qt_existe_requisicao_w = 'S' and coalesce(cd_motivo_baixa_p, 0) > 0) then
		update	requisicao_material
		set		dt_baixa		= sysdate
		where	nr_requisicao	= nr_requisicao_p;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		nr_item_requisicao_w,
		qt_atendida_w;
	exit when C01%notfound;
		begin
		gerar_movto_estoque_req(nr_requisicao_p,nr_item_requisicao_w,cd_operacao_estoque_p,qt_atendida_w,sysdate,cd_motivo_baixa_p,'1','N',nm_usuario_p,0,ds_erro_p);
		end;
	end loop;
	close C01;
				
end if;

commit;

end gedipa_gerar_requisicao_transf;
/