create or replace
procedure limpar_tabelas_temporarias is
begin

Limpar_tabela('W_CTB_BALANCETE', 'AND DT_ATUALIZACAO < SYSDATE - :qt_dias', 3);
Limpar_tabela('W_CTB_VER_SALDO', 'AND DT_ATUALIZACAO < SYSDATE - :qt_dias', 3);
Limpar_tabela('W_CTB_RAZAO_CONSOL', 'AND DT_ATUALIZACAO < SYSDATE - :qt_dias', 3);
Limpar_tabela('USUARIO_MENSAGEM', 'AND DT_LEITURA < sysdate - :qt_dias', 3);
Limpar_tabela('PROCESSO_ATENDIMENTO', 'AND DT_FIM_REAL < sysdate - :qt_dias', 30);
Limpar_tabela('LOG_USUARIO_CONECTADO', 'and dt_log < sysdate - :qt_dias', 360);
Limpar_tabela('AGENDA_CONTROLE_HORARIO', 'AND DT_AGENDA < sysdate - :qt_dias', 1);
Limpar_tabela('TASY_CONVERSAO_RTF', 'AND DT_ATUALIZACAO < sysdate - :qt_dias', 1);
Limpar_tabela('LOG_PRESCRICAO', 'AND DT_ATUALIZACAO < sysdate - :qt_dias', 20);
Limpar_tabela('LOG_GEDIPA', 'AND DT_LOG < sysdate - :qt_dias', 20);


exec_sql_dinamico('Luis','truncate table W_INTERF_CONTA_AUTOR');
exec_sql_dinamico('Luis','truncate table W_INTERF_CONTA_CAB');
exec_sql_dinamico('Luis','truncate table W_INTERF_CONTA_HEADER');
exec_sql_dinamico('Luis','truncate table W_INTERF_CONTA_ITEM');
exec_sql_dinamico('Luis','truncate table w_interf_conta_item_ipe');
exec_sql_dinamico('Luis','truncate table W_INTERF_CONTA_TOTAL');
exec_sql_dinamico('Luis','truncate table W_INTERF_CONTA_TRAILLER');
exec_sql_dinamico('Luis','truncate table W_CONTA_ABRAMGE_ITEM');
exec_sql_dinamico('Luis','truncate table W_LAB_IMPRESSAO');


dropar_tabela_w;
end limpar_tabelas_temporarias;
/
