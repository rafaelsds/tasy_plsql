create or replace
procedure lab_atual_integr_lwsdeg_vfr(nr_prescricao_p 	number, 
                                      nr_seq_prescr_p number,
                                      cd_barras_p			varchar2,
                                      ie_status_pos_envio_p	number,
                                      ie_status_envio_p	number,
                                      nm_usuario_p		varchar2) is


nr_seq_prescr_w		    number(10);
qt_etapa_amostra_w	  number(3);
qt_total_amostra_w	  number(3);
ie_tipo_atendimento_w	number(10);
cd_estabelecimento_w	number(10);
sigla_equip_w		      varchar2(255);
ds_erro_w			        varchar2(2000);

begin
	update 	prescr_proc_mat_item
	set 	ie_status = ie_status_pos_envio_p,
		dt_integracao = sysdate,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where 	nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_seq_prescr_p
	and	cd_barras = cd_barras_p;

	commit;

	select  count(*)
	into	qt_total_amostra_w
	from	prescr_proc_mat_item
	where	nr_prescricao = nr_prescricao_p
	and 	nr_seq_prescr = nr_seq_prescr_p;

	select count(*)
	into	qt_etapa_amostra_w
	from	prescr_proc_mat_item
	where	nr_prescricao 	= nr_prescricao_p
	and     nr_seq_prescr = nr_seq_prescr_p
	and		dt_integracao is not null
	and		ie_status 	= ie_status_pos_envio_p;

	if (qt_total_amostra_w = qt_etapa_amostra_w) then
		begin
		update prescr_procedimento
		set ie_status_atend = ie_status_pos_envio_p,
		    dt_integracao = sysdate,
		    nm_usuario = nm_usuario_p,
		    dt_atualizacao = sysdate
		where nr_prescricao = nr_prescricao_p
		and dt_integracao is null
		and nr_sequencia = nr_seq_prescr_p
		and ie_status_atend <= ie_status_pos_envio_p;

		exception
		  when others then
			ds_erro_w := substr(sqlerrm,1,2000);
			insert into log_lab (nr_prescricao,   dt_atualizacao,   nm_usuario, cd_log, ds_log)
			             values (nr_prescricao_p,       sysdate,  nm_usuario_p,   2461, substr('sequ�ncia '||nr_seq_prescr_w||' '||ds_erro_w,1,2000) );
		end;
	end if;
commit;
end;
/