create or replace
procedure gravar_transmissao_manifesto(nr_seq_transmissao_p    number,
                                nr_seq_manifesto_p    number,
                                nm_usuario_p            varchar2) is

begin

insert into nfe_transmissao_nf (nr_sequencia,
                                dt_atualizacao,
                                nm_usuario,
                                dt_atualizacao_nrec,
                                nm_usuario_nrec,
                                nr_seq_transmissao,
                                nr_seq_nota_fiscal,
		nr_seq_manifesto)
values                         (nfe_transmissao_nf_seq.nextval,
                                sysdate,
                                nm_usuario_p,
                                sysdate,
                                nm_usuario_p,
                                nr_seq_transmissao_p,
                                null,
		nr_seq_manifesto_p);

commit;

end gravar_transmissao_manifesto;
/