create or replace
procedure dixtal_inserir_pdf (	nr_seq_interno_prescr_p	number,					
				cd_pessoa_fisica_p	varchar2,
				dt_agenda_p		date,
				ds_nome_diretorio_p	varchar2) is 
					
nr_prescricao_w		number(14);
nr_seq_prescricao_w	number(10);

nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
nr_seq_imagem_w		number(10);
nr_laudo_w		number(10);
nr_atendimento_w	number(10);
cd_medico_resp_w	varchar2(10);
dt_entrada_unidade_w	date;
ds_titulo_laudo_w	varchar2(255);
nr_seq_propaci_w	number(15);
dt_procedimento_w	date;
nr_seq_laudo_w		number(10);
nr_seq_proc_interno_w	number(15);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_procedimento_w	number(15);
cd_setor_atendimento_w	number(10);
dt_prev_execucao_w	date;
cd_medico_exec_w	varchar2(10);
ie_lado_w		varchar2(15);
ds_laudo_copia_w	long;
nr_seq_laudo_ant_w	number(10);
nr_seq_laudo_atual_w	number(10);
nr_seq_interno_prescr_w	number(10);
nr_seq_agenda_w		number(10);
ds_nome_diretorio_w	varchar2(300);

ie_status_execucao_w	varchar2(3);

begin

commit;

if	--(nr_seq_interno_prescr_p is not null) and
	(cd_pessoa_fisica_p is not null) and
	(dt_agenda_p is not null) and
	(ds_nome_diretorio_p is not null) then

	ds_nome_diretorio_w	:= replace(ds_nome_diretorio_p,'/','\');
	
	select	max(a.nr_sequencia),
		max(a.cd_procedimento),
		max(a.ie_origem_proced),
		max(a.nr_seq_proc_interno)
	into	nr_seq_agenda_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w
	from	agenda_paciente a,
		proc_interno_integracao b
	where	b.nr_seq_sistema_integ = 46
	and	a.ie_status_agenda not in('C', 'F', 'I', 'S')
	and	((nvl(a.cd_procedimento,-1) = nvl(b.cd_procedimento, -2) and nvl(a.ie_origem_proced,-1) = nvl(b.ie_origem_proced, -2)) or 
		(nvl(a.nr_seq_proc_interno,-1) = nvl(b.nr_seq_proc_interno,-2)))
	and	trunc(dt_agenda) = trunc(dt_agenda_p)
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	select	max(a.nr_prescricao)
	into	nr_prescricao_w
	from	prescr_medica a
	where	a.nr_seq_agenda = nr_seq_agenda_w
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if (cd_procedimento_w is null) then		
		select	max(a.cd_procedimento),
			max(a.ie_origem_proced)
		into	cd_procedimento_w,
			ie_origem_proced_w
		from	proc_interno a
		where	a.nr_sequencia = nr_seq_proc_interno_w;		
	end if;
	
	select	max(a.nr_prescricao),
		max(a.nr_sequencia),
		max(nr_seq_proc_interno),
		max(cd_procedimento),
		max(ie_origem_proced),
		max(qt_procedimento),
		max(cd_setor_atendimento),
		max(dt_prev_execucao),
		max(cd_medico_exec),
		max(nvl(ie_lado,'A'))
	into	nr_prescricao_w,
		nr_seq_prescricao_w,
		nr_seq_proc_interno_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		cd_setor_atendimento_w,
		dt_prev_execucao_w,
		cd_medico_exec_w,
		ie_lado_w
	from	prescr_procedimento a
	--where	a.nr_seq_interno = nr_seq_interno_prescr_w; --nr_seq_interno_prescr_p;
	where	a.nr_prescricao = nr_prescricao_w
	and	a.cd_procedimento = cd_procedimento_w
	and	a.ie_origem_proced = ie_origem_proced_w;
		
	select	max(a.cd_medico)
	into	cd_medico_resp_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_w;
		
	select	nvl(max(a.nr_laudo),0)+1
	into	nr_laudo_w
	from	laudo_paciente a
	where	a.nr_prescricao = nr_prescricao_w;
	
	/*Executando a prescri��o*/
	select	nvl(max(nr_sequencia),0),
		max(nr_atendimento),
		max(dt_entrada_unidade),
		max(dt_procedimento)		
	into	nr_seq_propaci_w,
		nr_atendimento_w,
		dt_entrada_unidade_w,
		dt_procedimento_w
	from	procedimento_paciente
	where	nr_prescricao		= nr_prescricao_w
	and	nr_sequencia_prescricao	= nr_seq_prescricao_w;
		
	if	(nr_seq_propaci_w = 0) then
		begin
		
		Gerar_Proc_Pac_item_Prescr(	nr_prescricao_w, 
						nr_seq_prescricao_w, 
						null, 
						null,
						nr_seq_proc_interno_w,
						cd_procedimento_w, 
						ie_origem_proced_w,
						qt_procedimento_w, 
						cd_setor_atendimento_w,
						9, 
						dt_prev_execucao_w,
						'DIXTALEP12', 
						cd_medico_exec_w, 
						null,
						ie_lado_w, 
						null);
						
		select	max(nr_sequencia),
			max(nr_atendimento),
			max(dt_entrada_unidade),
			max(dt_procedimento)		
		into	nr_seq_propaci_w,
			nr_atendimento_w,
			dt_entrada_unidade_w,
			dt_procedimento_w
		from	procedimento_paciente
		where	nr_prescricao		= nr_prescricao_w
		and	nr_sequencia_prescricao	= nr_seq_prescricao_w;
		
		end;
	end if;

	select  MAX(nr_sequencia)
	into	nr_sequencia_ww
	from 	laudo_paciente
	where   nr_prescricao = nr_prescricao_w
	and	nr_seq_prescricao = nr_seq_prescricao_w;
	
	/* *** Cancela o laudo atual do procedimento (Tratamento para retifica��o do laudo pela DIXTALEP12. Quando j� existir um laudo para o item, este � cancelado - OS 404183). *** */
	if	(nvl(nr_sequencia_ww,0)<>0) then
		begin
			cancelar_laudo_paciente(nr_sequencia_ww,'C','DIXTALEP12','');
		end;
	end if;
	
			
	select	laudo_paciente_seq.nextval
	into	nr_seq_laudo_w
	from	dual;

	
	select	max(substr(obter_desc_prescr_proc_laudo(cd_procedimento, ie_origem_proced, nr_seq_proc_interno, ie_lado,  nr_seq_propaci_w),1,255))
	into	ds_titulo_laudo_w		
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_w
	and	a.nr_sequencia	= nr_seq_prescricao_w;
				
	insert into laudo_paciente(
			nr_sequencia,
			nr_atendimento,
			dt_entrada_unidade,
			nr_laudo,
			nm_usuario,
			dt_atualizacao,
			cd_medico_resp,
			ds_titulo_laudo,
			dt_laudo,
			nr_prescricao,
			nr_seq_proc,
			nr_seq_prescricao,
			dt_liberacao,
			qt_imagem,
			ie_status_laudo,
			dt_exame,
			ds_arquivo,
			ie_formato
			)
		values(	nr_seq_laudo_w,
			nr_atendimento_w,
			dt_entrada_unidade_w,
			nr_laudo_w,
			'DIXTALEP12',
			sysdate,
			cd_medico_resp_w,
			ds_titulo_laudo_w,
			sysdate,
			nr_prescricao_w,			
			nr_seq_propaci_w,
			nr_seq_prescricao_w,
			sysdate,
			0,
			'LL',
			dt_procedimento_w,
			ds_nome_diretorio_w,
			3
			);
					
	update	procedimento_paciente
	set	nr_laudo	= nr_seq_laudo_w
	where	nr_sequencia	= nr_seq_propaci_w;
	
	
	/* *****  Atualiza status execu��o na prescri��o ***** */
	update	prescr_procedimento a
	set	a.ie_status_execucao 	= '40',
		a.nm_usuario 	= 'DIXTALEP12'
	where	a.nr_prescricao = nr_prescricao_w
	and	a.nr_sequencia  in (	select	b.nr_sequencia_prescricao
					from	procedimento_paciente b
					where	b.nr_prescricao 	= a.nr_prescricao
					and	b.nr_sequencia_prescricao = a.nr_sequencia
					and	b.nr_laudo 		= nr_seq_prescricao_w);

	select 	max(ie_status_execucao)
	into	ie_status_execucao_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_w
	and	a.nr_sequencia  = nr_seq_prescricao_w;

	if	(ie_status_execucao_w <> '40') then

		update	prescr_procedimento a
		set	a.ie_status_execucao = '40',
			a.nm_usuario 	= 'DIXTALEP12'
		where	a.nr_prescricao = nr_prescricao_w
		and	a.nr_sequencia  in (	select	b.nr_sequencia_prescricao
						from	procedimento_paciente b
						where	b.nr_prescricao = a.nr_prescricao
						and 	b.nr_prescricao = nr_prescricao_w
						and	b.nr_sequencia_prescricao = a.nr_sequencia
						and	b.nr_sequencia_prescricao = nr_seq_prescricao_w);	
	end if;
	
end if;


commit;

end dixtal_inserir_pdf;
/