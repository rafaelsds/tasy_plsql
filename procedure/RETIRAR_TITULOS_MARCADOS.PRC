create or replace
procedure RETIRAR_TITULOS_MARCADOS(	nr_sequencia_p		number,
									nm_usuario_p		varchar2) is

begin

delete 	
from 	bordero_tit_pagar 
where 	nr_sequencia = nr_sequencia_p;

commit;

end RETIRAR_TITULOS_MARCADOS;
/

