create or replace
procedure alt_status_envio_cot_forn(
			nr_cot_compra_p		number,
			nm_usuario_p		Varchar2) is 

begin

update	cot_compra_forn
set	ie_status_envio_email_lib = 'S',
	nm_usuario = nm_usuario_p
where	nr_cot_compra = nr_cot_compra_p;


commit;

end alt_status_envio_cot_forn;
/
