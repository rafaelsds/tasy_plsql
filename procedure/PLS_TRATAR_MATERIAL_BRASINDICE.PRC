create or replace
procedure pls_tratar_material_brasindice(dt_importacao_p	date,
					ie_atualiza_pls_p	Varchar2,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number) is
					
nr_seq_material_w	number(10);
qt_registro_w		number(10);
cd_tiss_w		varchar2(15);
dt_importacao_w		date;
ie_atualiza_dt_exc_w	varchar2(5) := 'N';

Cursor c01 is
	select	a.nr_sequencia,
		a.cd_tiss_brasindice
	from	pls_material a
	where	a.cd_tiss_brasindice is not null
	and	a.dt_exclusao is null
	and	a.cd_estabelecimento	= cd_estabelecimento_p;

begin
if	(ie_atualiza_pls_p in ('S','SE')) then
	ie_atualiza_dt_exc_w := 'S'; -- Atualizar a data de exclus�o
end if;

dt_importacao_w	:= trunc(nvl(dt_importacao_p,sysdate),'dd');

open c01;
loop
fetch c01 into	
	nr_seq_material_w,
	cd_tiss_w;
exit when c01%notfound;
	begin
	select	count(*)
	into	qt_registro_w
	from	brasindice_preco a
	where	a.cd_tiss	= cd_tiss_w
	and	trunc(a.dt_inicio_vigencia,'dd') = dt_importacao_w;
	
	if	(qt_registro_w = 0) then
		update	pls_material
		set	dt_exclusao	= decode( ie_atualiza_dt_exc_w, 'S', nvl(dt_importacao_p,sysdate), dt_exclusao),
			dt_atualizacao	= sysdate,
			nm_usuario	= 'BRASINDICE'
		where	nr_sequencia	= nr_seq_material_w;
	end if;
	
	end;
end loop;
close c01;

commit;

end pls_tratar_material_brasindice;
/