create or replace
procedure cm_gerar_conj_ester_envio_ext(
				nr_seq_envio_p		number,
				nr_seq_item_envio_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_conjunto_w		number(10);
qt_conjunto_w			number(15);
qt_cont_w			number(15);
nr_seq_embalagem_w		number(10);
qt_ponto_w			number(15,2);
nr_seq_cont_w			number(10);
ds_observacao_w			varchar2(2000);
ds_conj_cont_w			varchar2(255);
nr_controle_w			number(10);
cd_medico_w			varchar2(10);
cd_setor_atendimento_w		number(5);
dt_validade_w			date;
cd_local_estoque_w		number(10);

cursor	C01 is
	select	nr_seq_conjunto,
		qt_envio_item,
		dt_validade,
		ds_observacao
	from	cm_envio_externo_item
	where	nr_sequencia = nr_seq_item_envio_p;

begin

obter_param_usuario(406,233,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,cd_estabelecimento_p, cd_local_estoque_w);

select	nvl(max(cd_setor_atendimento),0)
into	cd_setor_atendimento_w
from	cm_envio_externo
where	nr_sequencia = nr_seq_envio_p;

if	(cd_setor_atendimento_w = 0) then
	cd_setor_atendimento_w := obter_setor_usuario(nm_usuario_p);
end if;

open C01;
loop
fetch C01 into
	nr_seq_conjunto_w,
	qt_conjunto_w,
	dt_validade_w,
	ds_observacao_w;
exit when C01%notfound;
	begin
	
	qt_cont_w	:= 0;
	
	select 	nr_seq_embalagem,
		nvl(qt_ponto,0),
		cd_medico
	into	nr_seq_embalagem_w,
		qt_ponto_w,
		cd_medico_w
	from	cm_conjunto
	where	nr_sequencia = nr_seq_conjunto_w;
	
	while (qt_cont_w < qt_conjunto_w) loop
		begin
	
		select	CM_CONJUNTO_CONT_seq.nextval
		into	nr_seq_cont_w
		from	dual;

		select 	nvl(max(nr_seq_controle),0) + 1 
		into	nr_controle_w
		from 	cm_conjunto_cont;
				
		insert into cm_conjunto_cont(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_conjunto,
			dt_atualizacao,
			nm_usuario,
			ie_status_conjunto,
			dt_origem,
			nm_usuario_origem,
			nr_seq_embalagem,
			dt_receb_ester,
			nm_usuario_ester,
			nr_seq_ciclo,
			dt_validade,
			nr_seq_conj_fisico,
			dt_saida,
			nm_usuario_saida,
			nr_seq_agenda,
			cd_pessoa_fisica,
			cd_cgc,
			cd_local_estoque,
			dt_aloc_agenda,
			nm_usuario_agenda,
			dt_baixa_cirurgia,
			nr_cirurgia,
			cd_setor_atend_orig,
			cd_setor_atend_dest,
			qt_ponto,
			vl_esterilizacao,
			cd_setor_atendimento,
			nr_seq_cliente,
			nr_seq_controle,
			nr_seq_lote,
			ds_observacao,
			cd_pessoa_resp,
			ie_tipo_esterilizacao,
			nr_atendimento,
			dt_lancamento_automatico,
			nr_seq_pepo,
			cd_perfil_ativo,
			nm_usuario_vinc,
			dt_vinculo_conj,
			ie_situacao,
			nr_seq_envio_ext_item)
		values (nr_seq_cont_w,
			cd_estabelecimento_p,
			nr_seq_conjunto_w,
			sysdate,
			nm_usuario_p,
			3,
			sysdate,
			nm_usuario_p,
			nr_seq_embalagem_w,
			sysdate,
			nm_usuario_p,
			null, 
			dt_validade_w,
			null, null, null, null, 
			cd_medico_w, 
			null, cd_local_estoque_w, null, null, null, null,
			cd_setor_atendimento_w,
			null,
			qt_ponto_w,
			0,
			cd_setor_atendimento_w,
			null,
			nr_controle_w,
			null,
			wheb_mensagem_pck.get_texto(311707,'NR_SEQ_ENVIO_P='||nr_seq_envio_p),
			null, null, null, null, null, null, null, null,
			'A',
			nr_seq_item_envio_p);
										
		commit;
		
		if	(ds_conj_cont_w is null) then
			ds_conj_cont_w := substr(nr_seq_cont_w,1,255);
		else
			ds_conj_cont_w := substr(ds_conj_cont_w || ', ' || nr_seq_cont_w,1,255);
		end if;
		
		cme_incluir_itens_controle(nr_seq_cont_w, nm_usuario_p);	
		
		qt_cont_w := qt_cont_w + 1;
		
		end;
	
	end loop;
	end;
end loop;
close C01;

update	cm_envio_externo_item
set	ds_observacao =	substr(ds_observacao || chr(13) || wheb_mensagem_pck.get_texto(311708,'DS_CONJ_CONT_W='||ds_conj_cont_w),1,255)
where	nr_sequencia = nr_seq_item_envio_p;

end cm_gerar_conj_ester_envio_ext;
/