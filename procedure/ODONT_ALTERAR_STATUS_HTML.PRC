create or replace 
procedure odont_alterar_status_html(	nr_sequencia_p		number,
					ie_status_p		varchar2,
					cd_pessoa_fisica_p	varchar2,
					nr_atendimento_p	number,
					nm_usuario_p		varchar2) is

ds_historico_w			varchar2(255);
ds_atendimento_w		varchar2(255);
cd_procedimento_w		number(15);
nr_seq_proc_interno_w		number(10);
ie_origem_proced_w		number(10);
cd_profissional_w		number(10);
cd_especialidade_w		number(10);
qt_procedimento_w		number(8,3);
ie_status_w			varchar2(80);
nr_seq_area_w			varchar2(3);
nr_seq_consulta_w		number(10);
nr_seq_local_w			varchar2(3);
nr_seq_dente_w			varchar2(3);
nr_seq_sextante_w		varchar2(3);
nr_seq_arcada_w			varchar2(3);
nr_seq_tratamento_w		varchar2(3);
nr_seq_face_w			varchar2(3);
cd_caracteristica_w		varchar2(3);
ie_raiz_w			varchar2(1);
ie_profissional_externo_w	varchar2(1);
nr_odont_historico_w		number(10);
nr_procedimento_w		number(10);
ds_observacao_w			varchar2(4000);
nr_sequencia_face_w		number(10);
nr_face_proced_w		number(10);
cd_face_proced_w		varchar2(3);
nr_seq_proc_interno_face_w	number(10);
qt_procedimento_face_w		number(8,3);
cd_profissional_face_w		number(10);
cd_especialidade_face_w		number(10);
ie_prof_externo_face_w		varchar2(1);
ie_face_distal_w		odont_procedimento_proc.ie_face_distal%type;
ie_face_incisal_w		odont_procedimento_proc.ie_face_incisal%type;
ie_face_lingual_w		odont_procedimento_proc.ie_face_lingual%type;
ie_face_mesial_w		odont_procedimento_proc.ie_face_mesial%type;
ie_face_oclusal_w		odont_procedimento_proc.ie_face_oclusal%type;
ie_face_palatina_w		odont_procedimento_proc.ie_face_palatina%type;
ie_face_vestibular_w		odont_procedimento_proc.ie_face_vestibular%type;
cd_outras_caracteristicas_w	odont_procedimento.NR_SEQ_OUTRAS_CARACT%type;
nr_seq_odont_proced_w		odont_procedimento_proc.nr_sequencia%type;

cursor C01 is
	select 	nr_seq_proc_interno,
		qt_procedimento,
		cd_profissional,
		cd_especialidade,
		ie_profissional_externo,
		ie_face_distal,
		ie_face_incisal,
		ie_face_lingual,
		ie_face_mesial,
		ie_face_oclusal,
		ie_face_palatina,
		ie_face_vestibular,
		CD_PROCEDIMENTO,
		IE_ORIGEM_PROCED,
		nr_sequencia
	from 	odont_procedimento_proc
	where 	nr_seq_plano_proc = nr_sequencia_p;

begin

if	(nr_sequencia_p is not null) and (cd_pessoa_fisica_p is not null) then
	select	max(ie_status),
		max(nr_sequencia),
		max(nr_seq_area),
		max(nr_seq_consulta),
		max(nr_seq_local),
		max(nr_seq_dente),
		max(nr_seq_sextante),
		max(nr_seq_arcada),
		max(nr_seq_tratamento),
		max(nr_seq_face),
		max(ie_raiz),
		max(nr_seq_proc_interno),
		max(qt_procedimento),
		max(cd_profissional),
		max(cd_especialidade),
		max(IE_STATUS_DENTE),
		max(NR_SEQ_OUTRAS_CARACT)
	into	ie_status_w,
		nr_procedimento_w,
		nr_seq_area_w,
		nr_seq_consulta_w,
		nr_seq_local_w,
		nr_seq_dente_w,
		nr_seq_sextante_w,
		nr_seq_arcada_w,
		nr_seq_tratamento_w,
		nr_seq_face_w,
		ie_raiz_w,
		nr_seq_proc_interno_w,
		qt_procedimento_w,
		cd_profissional_w,
		cd_especialidade_w,
		cd_caracteristica_w,
		cd_outras_caracteristicas_w
	from	odont_procedimento
	where	nr_sequencia = nr_sequencia_p;

	if (nr_atendimento_p is not null) then
		ds_atendimento_w := wheb_mensagem_pck.get_texto(307420)||': '||nr_atendimento_p;
	else
		ds_atendimento_w := '';
	end if;

	select	max(wheb_mensagem_pck.get_texto(798633)||': '|| substr(obter_valor_dominio(5100,ie_status_p),1,255)||chr(10)||
			wheb_mensagem_pck.get_texto(798635)||': '||nr_dente||chr(10)||
			ds_atendimento_w)
	into	ds_historico_w
	from	odont_procedimento
	where	nr_sequencia = nr_sequencia_p
	and		nr_dente is not null;

	if	(ds_historico_w is null) then
		select	max(wheb_mensagem_pck.get_texto(798633)||': '|| substr(obter_valor_dominio(5100,ie_status_p),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(798641)||': '||substr(obter_valor_dominio(5101,ie_tratamento),1,255)||chr(10)||
				ds_atendimento_w)
		into	ds_historico_w
		from	odont_procedimento
		where	nr_sequencia = nr_sequencia_p
		and	ie_tratamento is not null;
	end if;

	select  wheb_mensagem_pck.get_texto(798633)||': '||substr(obter_valor_dominio(5100,ie_status_p),1,255)
	into	ds_observacao_w
	from 	dual;

	update	odont_procedimento
	set	ie_status = ie_status_p
	where	nr_sequencia = nr_sequencia_p;

	if	(((ie_status_p = 'C') and (ie_status_w = 'R')) or ((ie_status_p = 'R') and (nr_atendimento_p is not null))) then

		select	odont_historico_seq.nextval
		into	nr_odont_historico_w
		from	dual;

		insert into odont_historico(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ds_titulo,
					ds_historico,
					nr_atendimento,
					ie_tipo,
					dt_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_status,
					nr_seq_odont_proced,
					cd_pessoa_fisica,
					ds_observacao,
					nr_seq_area,
					nr_seq_consulta,
					nr_seq_local,
					nr_seq_dente,
					nr_seq_sextante,
					nr_seq_arcada,
					nr_seq_tratamento,
					ie_raiz,
					ie_status_area,
					NR_SEQ_OUTRAS_CARACT)
		values(	nr_odont_historico_w,
				sysdate,
				nm_usuario_p,
				wheb_mensagem_pck.get_texto(798633)||': '||substr(obter_valor_dominio(5100,ie_status_p),1,255),
				nvl(ds_historico_w,wheb_mensagem_pck.get_texto(798633)||': '||substr(obter_valor_dominio(5100,ie_status_p),1,255)),
				nr_atendimento_p,
				'S',
				sysdate,
				sysdate,
				nm_usuario_p,
				ie_status_p,
				nr_sequencia_p,
				cd_pessoa_fisica_p,
				ds_observacao_w,
				nr_seq_area_w,
				nr_seq_consulta_w,
				nr_seq_local_w,
				nr_seq_dente_w,
				nr_seq_sextante_w,
				nr_seq_arcada_w,
				nr_seq_tratamento_w,
				ie_raiz_w,
				cd_caracteristica_w,
				cd_outras_caracteristicas_w);

			open C01;
			loop
			fetch C01 into
				nr_seq_proc_interno_face_w,
				qt_procedimento_face_w,
				cd_profissional_face_w,
				cd_especialidade_face_w,
				ie_prof_externo_face_w,
				ie_face_distal_w,
				ie_face_incisal_w,
				ie_face_lingual_w,
				ie_face_mesial_w,
				ie_face_oclusal_w,
				ie_face_palatina_w,
				ie_face_vestibular_w,
				cd_procedimento_w,
				ie_origem_proced_w,
				nr_seq_odont_proced_w;
			exit when C01%notfound;

			insert	into	odont_historico_proced(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_proc_interno,
							qt_procedimento,
							cd_profissional,
							ie_profissional_externo,
							cd_especialidade,
							ie_face_distal,
							ie_face_incisal,
							ie_face_lingual,
							ie_face_mesial,
							ie_face_oclusal,
							ie_face_palatina,
							ie_face_vestibular,
							cd_procedimento,
							ie_origem_proced,
							NR_SEQ_ODONT_HIST
							)
			values(	odont_historico_proced_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_proc_interno_face_w,
					qt_procedimento_face_w,
					cd_profissional_face_w,
					ie_prof_externo_face_w,
					cd_especialidade_face_w,
					ie_face_distal_w,
					ie_face_incisal_w,
					ie_face_lingual_w,
					ie_face_mesial_w,
					ie_face_oclusal_w,
					ie_face_palatina_w,
					ie_face_vestibular_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					nr_odont_historico_w
					);

				if 	(ie_status_p = 'R') and (nr_atendimento_p is not null) then
					odonto_inserir_proced_conta(nr_atendimento_p, cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_face_w, qt_procedimento_face_w, nr_seq_odont_proced_w, nm_usuario_p, 'S');
				end if;

				if 	(ie_status_p = 'C') and (ie_status_w = 'R') then
					odonto_inserir_proced_conta(nr_atendimento_p, cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_face_w, (qt_procedimento_face_w*-1), nr_seq_odont_proced_w, nm_usuario_p, 'S');
				end if;

			end loop;
			close C01;

	end if;

end if;

commit;

end odont_alterar_status_html;
/