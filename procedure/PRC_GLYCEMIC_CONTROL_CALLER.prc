CREATE OR REPLACE PROCEDURE PRC_GLYCEMIC_CONTROL_CALLER(
  dtinicio_p        DATE,
  dtfim_p           DATE,
  estabelecimento_p NUMBER,
  setor_p           NUMBER,
  ano_p             NUMBER,
  trimestre_p       NUMBER,
  visao_p           NUMBER,
  pessoa_p          VARCHAR2) IS
BEGIN
  INSERT INTO W_GLYCEMIC_CONTROL_LOG(
    nr_sequencia, 
    dt_inicio, 
    dt_fim, 
    cd_estabelecimento, 
    cd_setor_atendimento, 
    nr_year, 
    nr_quarter, 
    ie_type, 
    cd_patient, 
    nm_usuario)
  VALUES(
    w_glycemic_control_log_seq.nextval,
    dtinicio_p,
    dtfim_p,
    NVL(estabelecimento_p, 0),
    NVL(setor_p, 0),
    ano_p,
    trimestre_p,
    visao_p,
    TRIM(pessoa_p),
    wheb_usuario_pck.get_nm_usuario);
  --
  COMMIT;
  --
  IF VISAO_P = 3 AND TRIM(PESSOA_P) IS NULL THEN
    WHEB_MENSAGEM_PCK.exibir_mensagem_abort('The PATIENT CODE parameter is required when BY PATIENT is selected in the REPORT TYPE parameter.');
  ELSE
    PKG_GLYCEMIC_CONTROL.RUN(dtinicio_p, dtfim_p, NVL(estabelecimento_p, 0), NVL(setor_p, 0), ano_p, trimestre_p, visao_p, pessoa_p);
  END IF;
  --
END;
/
