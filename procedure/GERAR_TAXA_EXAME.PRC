CREATE OR REPLACE
PROCEDURE Gerar_Taxa_Exame
                       (CD_ESTABELECIMENTO_P    NUMBER,
                        NR_ATENDIMENTO_P        NUMBER,
                        DT_ENTRADA_UNIDADE_P    DATE,
                        CD_PROCEDIMENTO_P       NUMBER,
                        DT_PROCEDIMENTO_P       DATE,
                        QT_PROCEDIMENTO_P       NUMBER,
                        CD_SETOR_ATENDIMENTO_P  NUMBER,
                        NR_DOC_CONVENIO_P       VARCHAR2,
                        CD_CONVENIO_P           NUMBER,
                        CD_CATEGORIA_P          VARCHAR2) IS
dt_atualizacao_w        date          := SYSDATE;
cd_taxa_exame_w         number(10)     := 0;
ie_flag_ok              varchar2(1)   := 'S';
nr_sequencia_w       	NUMBER(10)     := 0;
tx_procedimento_w	number(7,4)	:= 0;
ie_criterio_taxa_w	number(2)		:= 0;
ie_classificacao_w	varchar2(1);
nr_sequencia_org_w      NUMBER(10)     	:= 0;
vl_procedimento_w      	NUMBER(15,2)   	:= 0;
vl_medico_w      	NUMBER(15,2)   		:= 0;
vl_anestesista_w      	NUMBER(15,2)   	:= 0;
vl_materiais_w      	NUMBER(15,2)   	:= 0;
vl_auxiliares_w      	NUMBER(15,2)   	:= 0;
vl_custo_operacional_w  NUMBER(15,2)   	:= 0;
ie_evento_calculo_w	number(2)		:= 0;
ie_origem_proced_w	number(10);
nr_seq_atepacu_w	Number(10)		:= 0;
nr_sequencia_Orig_w	Number(10)		:= 0;

BEGIN
select nvl(max(nr_seq_interno),0)
into	nr_seq_atepacu_w
from 	atend_paciente_unidade
where nr_atendimento 		= nr_atendimento_p
  and dt_entrada_unidade	= dt_entrada_unidade_p;


select 	nvl(max(nr_sequencia),0)
into	nr_sequencia_Orig_w
from 	Procedimento_Paciente
where 	nr_atendimento		= nr_atendimento_p
  and 	dt_procedimento		= dt_Procedimento_p
  and	cd_procedimento		= cd_procedimento_p;
/* Verificar se deve gerar taxa para o procedimento */
begin
	select 	a.cd_taxa_exame,
			a.tx_procedimento,
			a.ie_criterio_taxa,
			b.ie_classificacao,
			nvl(a.ie_origem_taxa,a.ie_origem_proced)
  	into 		cd_taxa_exame_w,
			tx_procedimento_w,
			ie_criterio_taxa_w,
			ie_classificacao_w,
			ie_origem_proced_w
  	from 		convenio_taxa_exame a,
			procedimento b
	where		a.cd_taxa_exame		= b.cd_procedimento
	and		a.ie_origem_proced	= b.ie_origem_proced
 	and 		a.cd_estabelecimento 	= cd_estabelecimento_p
   	and 		a.cd_convenio        	= cd_convenio_p
   	and 		a.cd_categoria       	= cd_categoria_p
   	and 		a.cd_procedimento    	= cd_procedimento_p
	and		a.ie_evento_calculo	= 1
	and		nvl(a.ie_situacao,'A')  = 'A';
	exception
     			when others then
          		ie_flag_ok := 'N';
end;

/* Gravar taxa do exame */
if   	(ie_flag_ok = 'S') then
	begin
     	select 	Procedimento_Paciente_Seq.nextval
     		into 		nr_sequencia_w
     		from dual;

     	insert  into procedimento_paciente
             (nr_sequencia,nr_atendimento, dt_entrada_unidade, cd_procedimento,
		 dt_procedimento, qt_procedimento,dt_atualizacao, nm_usuario, cd_convenio,
		 cd_categoria, vl_procedimento, vl_medico,vl_anestesista, vl_materiais,
		 dt_acerto_conta, vl_auxiliares, vl_custo_operacional,tx_medico, tx_anestesia,
		 nr_doc_convenio, dt_conta, cd_setor_atendimento, ie_origem_proced, nr_seq_atepacu,
		nr_seq_proc_princ)
     	values
             (nr_sequencia_w,nr_atendimento_p, dt_entrada_unidade_p, cd_taxa_exame_w,
		 dt_procedimento_p, qt_procedimento_p,dt_atualizacao_w, 'Tasy', cd_convenio_p,
		 cd_categoria_p, 0, 0, 0, 0,null, 0, 0, 0, 0, nr_doc_convenio_p,
		 dt_procedimento_p, cd_setor_atendimento_p,ie_origem_proced_w, nr_seq_atepacu_w,
		nr_sequencia_Orig_w);

     	if	ie_classificacao_w = '1' then
		Atualiza_Preco_Procedimento
      	      		(nr_sequencia_w, cd_convenio_p,	'Tasy');
	else
		Atualiza_Preco_Servico
      	      		(nr_sequencia_w, 'Tasy');
	end if;
	exception
        	when others then
               		ie_flag_ok := 'N';
     	end;
end if;
END Gerar_Taxa_Exame;
/
