create or replace
procedure esus_gerar_atend_dom_proc(	cd_profissional_p		varchar2,
					cd_cbo_p		varchar2,
					nr_atendimento_p		number,
					dt_atendimento_p		date,
					nr_seq_ficha_p		number,
					nm_usuario_p		usuario.nm_usuario%type) is 
					
cd_procedimento_w		esus_atend_dom_proced.cd_procedimento%type;
ie_origem_proced_w		esus_atend_dom_proced.ie_origem_proced%type;
nr_interno_conta_w			conta_paciente.nr_interno_conta%type := 0; 
qt_proc_gerado_w			number(10) := 0;

Cursor C01 is
	select	b.cd_procedimento,
		b.ie_origem_proced
	from	procedimento_paciente b
	where	trunc(b.dt_procedimento) = trunc(dt_atendimento_p)
	and	nvl(b.cd_pessoa_fisica,b.cd_medico_executor) = cd_profissional_p
	and	b.cd_cbo = cd_cbo_p
	and	b.nr_atendimento = nr_atendimento_p
	and	((nr_interno_conta_w = 0) or (b.nr_interno_conta = nr_interno_conta_w))
	and	b.cd_motivo_exc_conta is null
	group by	b.cd_procedimento,
		b.ie_origem_proced;				
					
begin

begin
select 	nvl(min(nr_interno_conta),0)
into	nr_interno_conta_w
from	conta_paciente
where 	nr_atendimento = nr_atendimento_p
and	trunc(dt_atendimento_p) between trunc(dt_periodo_inicial) and trunc(dt_periodo_final);
exception
when others then
	nr_interno_conta_w := 0;
end;

open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w;
exit when C01%notfound;
	begin
	
	select	count(1)
	into	qt_proc_gerado_w
	from	esus_atend_dom_proced
	where	nr_seq_atend_dom = nr_seq_ficha_p
	and	cd_procedimento	= cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w;
	
	if	(qt_proc_gerado_w = 0) then
		begin
		
		insert into esus_atend_dom_proced(
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			cd_procedimento,
			ie_origem_proced,
			nr_seq_atend_dom,
			nr_sequencia)
		values(	nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_ficha_p,
			esus_atend_dom_proced_seq.nextval);
			
		end;
	end if;
	
	
	end;
end loop;
close C01;
commit;

end esus_gerar_atend_dom_proc;
/
