create or replace
procedure db_sort_workflow_templ_items(
			nr_sequencia_p	number,
			nm_usuario_p	varchar2) is

nr_seq_ordem_w		db_workflow_template_item.nr_seq_ordem%type := 0;

cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_doc_template,
		a.ds_prefixo_id,
		a.ie_obrigatorio
	from	db_document_template b,
		db_workflow_template_item a
	where	b.nr_sequencia = a.nr_seq_doc_template
	and	a.nr_seq_wf_template = nr_sequencia_p
	and not exists (select	1
			from	db_wf_templ_item_predec x
			where	x.nr_seq_item = a.nr_sequencia)
	order by a.ie_obrigatorio desc,
		b.ds_template;

procedure db_sort_child_items(
			nr_seq_doc_template_p	number) is

cursor c02 is
	select	distinct a.nr_sequencia,
		a.nr_seq_doc_template,
		a.ds_prefixo_id,
		a.ie_obrigatorio,
		b.ds_template
	from	db_wf_templ_item_predec c,
		db_document_template b,
		db_workflow_template_item a
	where	b.nr_sequencia = a.nr_seq_doc_template
	and	a.nr_sequencia = c.nr_seq_item
	and	c.nr_seq_doc_template = nr_seq_doc_template_p
	and	a.nr_seq_wf_template = nr_sequencia_p
	and	a.nr_seq_ordem is null
	and not exists (select	1
			from	db_wf_templ_item_predec y,
				db_workflow_template_item x
			where	y.nr_seq_item = a.nr_sequencia
			and	x.nr_seq_wf_template = a.nr_seq_wf_template
			and	y.nr_seq_doc_template = x.nr_seq_doc_template
			and	x.nr_seq_ordem is null)
	order by a.ie_obrigatorio desc,
		b.ds_template;

begin

for c02_w in c02 loop
	begin
	nr_seq_ordem_w := nr_seq_ordem_w + 1;

	update	db_workflow_template_item
	set	nr_seq_ordem = nr_seq_ordem_w
	where	nr_sequencia = c02_w.nr_sequencia;

	db_sort_child_items(c02_w.nr_seq_doc_template);
	end;
end loop;

end db_sort_child_items;

begin

update	db_workflow_template_item
set	nr_seq_ordem = null
where	nr_seq_wf_template = nr_sequencia_p;

for c01_w in c01 loop
	begin
	nr_seq_ordem_w := nr_seq_ordem_w + 1;
	
	update	db_workflow_template_item
	set	nr_seq_ordem = nr_seq_ordem_w
	where	nr_sequencia = c01_w.nr_sequencia;
	end;
end loop;

for c01_w in c01 loop
	db_sort_child_items(c01_w.nr_seq_doc_template);
end loop;

commit;

end db_sort_workflow_templ_items;
/