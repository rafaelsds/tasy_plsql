create or replace
procedure fa_excluir_item_entrega(nr_seq_item_entr_p		number) is 

begin

delete	 
from	fa_entr_medic_item_receita
where	nr_seq_item_entrega = nr_seq_item_entr_p;


commit;

end fa_excluir_item_entrega;
/