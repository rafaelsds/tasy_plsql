create or replace procedure gerar_procedimento(

          nr_seq_paciente_p   number,
          cd_procedimento_p   number,
          ds_dias_aplicacao_p Varchar2,
          nm_usuario_p        varchar2,
          qt_procedimento_p   Number,
          nr_agrupamento_p    number   default null) is



nr_seq_procedimento_w  number(10);

begin

select  nvl(max(nr_seq_procedimento),0) +1
into  nr_seq_procedimento_w
from  paciente_protocolo_proc
where  nr_seq_paciente  = nr_seq_paciente_p;

insert into PACIENTE_PROTOCOLO_PROC (nr_seq_paciente,
                                     nr_seq_procedimento,
                                     cd_procedimento,
                                     Qt_Procedimento,
                                     ds_dias_aplicacao,
                                     dt_atualizacao,
                                     Nm_Usuario,
                                     Dt_Atualizacao_Nrec,
                                     Nm_Usuario_Nrec,
                                     Nr_Agrupamento)
              values
                                    ( nr_seq_paciente_p,
                                      nr_seq_procedimento_w,
                                      cd_procedimento_p,
                                      qt_Procedimento_p,
                                      ds_dias_aplicacao_p,
                                      sysdate,
                                      nm_usuario_p,
                                      sysdate,
                                      nm_usuario_p,
                                      nr_agrupamento_p);

commit;

end gerar_procedimento;
/