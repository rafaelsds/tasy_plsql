create or replace 
procedure Calcular_Nut_Elem_Pediatria(	nr_sequencia_p		number,
										nm_usuario_p		varchar2,
										ie_alterou_volume_p	varchar2 default 'N') is

qt_peso_w				number(30,16);
nr_sequencia_w			number(10);
nr_seq_elemento_w		nut_pac_elemento.nr_sequencia%type;
qt_elem_kg_dia_w		number(30,16);
qt_diaria_w				number(30,16);
pr_total_w				number(3);
qt_kcal_w				nut_pac_elemento.qt_kcal%type;
ie_tipo_elemento_w		varchar2(3);
qt_conversao_ml_w		number(30,16);
qt_volume_w				number(30,16);
nr_seq_elem_mat_w		number(10,0);
ie_unid_med_w			varchar2(15);
qt_peso_calorico_w		number(15,2);
nr_fator_lipideo_w		number(10,2);
nr_fator_lipideo_ww		number(10,2);
qt_conversao_kcal_w		number(30,16);
ie_arredonda_npt_w		parametro_medico.ie_arredondar_npt%type := 'N';
qt_volume_final_w		nut_pac_elemento.qt_volume_final%type;
ie_glutamina_w			nut_elemento.ie_glutamina%type;

cursor c01 is
select	a.nr_sequencia,
		a.qt_elem_kg_dia,
		b.ie_tipo_elemento,
		a.ie_unid_med,
		a.qt_diaria,
		a.nr_seq_elemento,
		nvl(a.qt_volume_final,0),
		nvl(b.ie_glutamina,'N')
from	Nut_elemento b,
		nut_pac_elemento a
where	nr_seq_nut_pac		= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_npt_pediatrica	= 'S'; 

cursor c02 is
select	a.nr_sequencia,
		qt_conversao_ml		
from	nut_elem_material b,
		nut_pac_elem_mat a
where	a.nr_seq_pac_elem	= nr_sequencia_w
and		a.nr_seq_elem_mat	= b.nr_sequencia
and		nvl(b.ie_tipo,'NPT')	= 'NPT';

begin

Obter_Param_Usuario(924,763,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,nr_fator_lipideo_w);

Select	nvl(max(qt_peso),0),
		nvl(max(qt_peso_calorico),0)
into	qt_peso_w,
		qt_peso_calorico_w
from	nut_pac
where	nr_sequencia = nr_sequencia_p;

select 	nvl(max(ie_arredondar_npt),'N')
into	ie_arredonda_npt_w
from	parametro_medico
where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

open c01;
loop
	fetch c01	into
		nr_sequencia_w,
      	qt_elem_kg_dia_w,
		ie_tipo_elemento_w,
		ie_unid_med_w,
		qt_diaria_w,
		nr_seq_elemento_w,
		qt_volume_final_w,
		ie_glutamina_w;		
exit when c01%notfound;
	begin
	if (nvl(ie_alterou_volume_p,'N') = 'N') or
	   (ie_tipo_elemento_w <> 'C') then
	   
		if	(ie_unid_med_w = 'Kg/d') or
			(ie_unid_med_w is null) then
			qt_diaria_w		:= qt_elem_kg_dia_w * qt_peso_w;
		elsif	(ie_unid_med_w = 'PC/d') then
			qt_diaria_w		:= qt_elem_kg_dia_w * qt_peso_calorico_w;
		elsif	(ie_unid_med_w = 'ml') then
			qt_diaria_w		:= qt_elem_kg_dia_w * qt_peso_w;
		end if;
		
		qt_kcal_w		:= 0;
		
		if	(ie_tipo_elemento_w in ('C','P')) then /* Glicose ou proteina */
			if	(ie_arredonda_npt_w = 'S') and
				(ie_glutamina_w = 'S') and 
				(ie_tipo_elemento_w = 'P') then
				qt_kcal_w	:= qt_volume_final_w * 0.8;
			else
				qt_kcal_w	:= qt_diaria_w * 4;
			end if;
		elsif	(ie_tipo_elemento_w = 'L') then  /* Lipidio */
			
			select 	max(nvl(b.qt_conversao_kcal,0))
			into	qt_conversao_kcal_w
			from   	nut_pac_elem_mat a,
					nut_elem_material b 
			where	a.nr_seq_pac_elem = nr_sequencia_w
			and	   	b.nr_sequencia = a.nr_seq_elem_mat;

			if (qt_conversao_kcal_w > 0) then
				nr_fator_lipideo_ww	:= qt_conversao_kcal_w;			
			else
				nr_fator_lipideo_ww	:= nr_fator_lipideo_w;
			end if;
			
			if (ie_arredonda_npt_w = 'S') then
				qt_kcal_w	:= qt_volume_final_w * nvl(nr_fator_lipideo_ww,9);
			else
				qt_kcal_w	:= qt_diaria_w * nvl(nr_fator_lipideo_ww,9);
			end if;
		end if;

		if	(qt_diaria_w is null) then
			--Verifique se as doses/UM informadas est�o corretas!
			Wheb_mensagem_pck.exibir_mensagem_abort(226991);
		end if;
		
		if (ie_arredonda_npt_w = 'S') then
			qt_diaria_w	:= round(qt_diaria_w, 2);
		end if;
		
		update	nut_pac_elemento
		set		qt_diaria		= qt_diaria_w,
				qt_kcal			= qt_kcal_w
		where	nr_sequencia	= nr_sequencia_w;
	end if;
	
	open c02;
	loop
	fetch c02	into
		nr_seq_elem_mat_w,
      	qt_conversao_ml_w;		
	exit when c02%notfound;
		
		qt_volume_w	:= qt_diaria_w * qt_conversao_ml_w;
		
		if (ie_arredonda_npt_w = 'S') then
			qt_volume_w	:= round(qt_volume_w, 2);
		end if;	
		
		update	nut_pac_elem_mat
		set		qt_volume		= qt_volume_w
		where	nr_sequencia	= nr_seq_elem_mat_w;
		
	end loop;
	close c02;
	end;
end loop;
close c01;

verificar_limite_litro_npt_ped(nr_sequencia_p, nm_usuario_p);

end Calcular_Nut_Elem_Pediatria;
/