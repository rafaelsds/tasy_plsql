create or replace
procedure eme_excluir_chamado(	nr_sequencia_p	in	number,
				nm_usuario_p	in	varchar2,
				ds_erro_p		out	varchar2) is

nr_atendimento_w	number(7);

begin

select	nvl(nr_atendimento,0)
into	nr_atendimento_w
from	eme_chamado
where	nr_sequencia = nr_sequencia_p;

if	(nr_atendimento_w <> 0) then
	begin
	update	eme_chamado
	set	nr_atendimento = null
	where	nr_sequencia = nr_sequencia_p;

	EXCLUIR_ATENDIMENTO(nr_atendimento_w, 'S', nm_usuario_p, null, ds_erro_p);
	if	(ds_erro_p <> '') or (ds_erro_p is not null) then
		update	eme_chamado
		set	nr_atendimento = nr_atendimento_w
		where	nr_sequencia = nr_sequencia_p;
	end if;
	end;
end if;

commit;

end eme_excluir_chamado;
/