create or replace
procedure alterar_dt_validade_usuario(	nm_usuario_param_p	Varchar2,
				dt_validade_p		Date,
				nm_usuario_p		Varchar2) is 

begin

if	(nm_usuario_p is not null) then

	update	usuario
	set	dt_validade_usuario = dt_validade_p,
		nm_usuario_atual = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nm_usuario = nm_usuario_param_p;

end if;

commit;

end alterar_dt_validade_usuario;
/