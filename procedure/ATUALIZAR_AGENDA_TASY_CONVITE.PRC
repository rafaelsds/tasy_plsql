create or replace 
procedure Atualizar_Agenda_Tasy_Convite	(nr_seq_agenda_p	Number,
					dt_agenda_p		Date,
					ie_confirmado_p		Varchar2,
					ds_motivo_ausencia_p	Varchar2,
					nm_usuario_p		Varchar2) is

ds_agenda_w				varchar2(100);
nr_seq_tipo_w				number(10);
nr_minuto_duracao_w			number(10);
ds_local_w				varchar2(50);
ds_observacao_w				varchar2(2000);
nr_seq_agenda_w				number(10,0);
nr_seq_local_w				number(10,0);
nm_usuario_agenda_w			varchar2(15);

begin

select	ds_agenda,
	nr_seq_tipo,
	nr_minuto_duracao,
	ds_local,
	ds_observacao,
	nr_seq_local,
	nm_usuario_agenda
into	ds_agenda_w,
	nr_seq_tipo_w,
	nr_minuto_duracao_w,
	ds_local_w,
	ds_observacao_w,
	nr_seq_local_w,
	nm_usuario_agenda_w
from	agenda_tasy
where	nr_sequencia		= nr_seq_agenda_p;


update	agenda_tasy_convite 
set	ie_confirmado		= ie_confirmado_p,
	ds_motivo_ausencia	= ds_motivo_ausencia_p,
	dt_atualizacao		= sysdate,
	dt_confirmacao		= trunc(sysdate),
	nm_usuario		= nm_usuario_p
where	nr_seq_agenda		= nr_seq_agenda_p
and	nm_usuario_convidado	= nm_usuario_p;



if 	((ie_confirmado_p	= 'S')  and
	(nvl(nm_usuario_agenda_w,'XPTO') <> nvl(nm_usuario_p,'XPTO'))) then
	begin
	delete	from agenda_tasy
	where	nm_usuario_agenda	= nm_usuario_p
	and	dt_agenda		= dt_agenda_p
	and	ie_status		= 'L';

	select	agenda_tasy_seq.nextval
	into	nr_seq_agenda_w
	from	dual;

	insert	into agenda_tasy
		(nr_sequencia,
		ds_agenda,
		dt_agenda,
		nr_seq_tipo,
		nr_minuto_duracao,
		nm_usuario_agenda,
		dt_atualizacao,
		nm_usuario,
		ds_observacao,
		ds_local,
		ie_status,
		nr_seq_local,
		nr_seq_agenda_convite)
	values	(nr_seq_agenda_w,
		ds_agenda_w,
		dt_agenda_p,
		nr_seq_tipo_w,
		nr_minuto_duracao_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_observacao_w,
		ds_local_w,
		'M',
		nr_seq_local_w,
		nr_seq_agenda_p);

	end;
end if;


end Atualizar_Agenda_Tasy_Convite;
/