create or replace
procedure Insert_Cadastro_Dinamico( nm_usuario_p		Varchar2,
				    nr_sequencia_p		Varchar2,
				    data_p			date,
				    nr_seq_tipo_equip_p		varchar2,
				    ie_tipo_update_p		Varchar2,
				    ie_tipo_insert_p		Varchar2,
				    vl_valor_p			varchar2) is 

begin

if (ie_tipo_insert_p = 'D') then
	begin
	insert into man_tipo_equipamento_valor(	nr_sequencia,
						nr_seq_tipo_equip_atrib,
						nr_seq_equipamento,
						dt_atualizacao,
						nm_usuario,
						dt_valor)
					values (man_tipo_equipamento_valor_seq.nextval,
						nr_seq_tipo_equip_p,
						nr_sequencia_p,
						sysdate,
						nm_usuario_p,
						data_p);
	end;
end if;

if(ie_tipo_insert_p = 'C')then
	begin
	insert into man_tipo_equipamento_valor(	nr_sequencia,
						nr_seq_tipo_equip_atrib,
						nr_seq_equipamento,
						dt_atualizacao,
						nm_usuario,
						cd_valor)
					values (man_tipo_equipamento_valor_seq.nextval,
						nr_seq_tipo_equip_p,
						nr_sequencia_p,
						sysdate,
						nm_usuario_p,
						vl_valor_p);
	end;					
end if;

if (ie_tipo_update_p = 'D')then
	begin
		update  man_tipo_equipamento_valor 
		set	dt_valor = data_p
		where	nr_seq_equipamento = nr_sequencia_p
		and	nr_seq_tipo_equip_atrib = nr_seq_tipo_equip_p;
	end;	
end if;

if (ie_tipo_update_p = 'C')then
	begin
		update  man_tipo_equipamento_valor 
		set	cd_valor = vl_valor_p
		where	nr_seq_equipamento = nr_sequencia_p
		and	nr_seq_tipo_equip_atrib = nr_seq_tipo_equip_p;
	end;	
end if;

commit;

end Insert_Cadastro_Dinamico;
/
