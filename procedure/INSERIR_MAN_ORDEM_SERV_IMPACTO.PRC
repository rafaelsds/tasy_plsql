create or replace procedure inserir_man_ordem_serv_impacto (
		nr_seq_ordem_serv_p 	number,
		ds_titulo_p 		varchar2,
		nm_usuario_p 		varchar2
) is

nr_seq_idioma_padrao_w 	CONSTANT idioma.nr_sequencia%type := 5; -- English
qt_reg_w				number;
nr_seq_impacto_w 		man_ordem_serv_impacto.nr_sequencia%type;
nr_seq_imp_pr_w 		man_ordem_serv_imp_pr.nr_sequencia%type;
ie_impacto_requisito_w 	man_ordem_serv_imp_pr.ie_impacto_requisito%type;
ds_new_title_w 			man_ordem_serv_imp_pr.ds_new_title%type;
is_add_answer_w			varchar2(1);
	
cursor c02 is
	select 	distinct nr_product_requirement
	from 	w_man_ordem_serv_imp_pr
	where 	nr_seq_ordem_serv = nr_seq_ordem_serv_p;
	
cursor c03 (nr_product_requirement_w number) is
	select 	distinct cd_funcao
	from 	w_man_ordem_serv_imp_pr
	where 	nr_seq_ordem_serv = nr_seq_ordem_serv_p
	and 	nr_product_requirement = nr_product_requirement_w;


begin

	if (nr_seq_ordem_serv_p is not null) then
	
		select 	man_ordem_serv_impacto_seq.nextval
		into 	nr_seq_impacto_w
		from 	dual;
		
		insert into man_ordem_serv_impacto (
			nr_sequencia,
			nr_seq_ordem_serv,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_liberacao,
			ds_titulo
		) values (
			nr_seq_impacto_w,
			nr_seq_ordem_serv_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ds_titulo_p
		);
		
		for r_c02 in c02 loop begin
		
			select 	max(nvl(
				(	select 	max(b.ie_impacto_requisito) 
					from 	w_man_ordem_serv_imp_pr b
					where 	b.nr_seq_ordem_serv = a.nr_seq_ordem_serv
					and 	b.nr_product_requirement = a.nr_product_requirement
					and 	b.ie_impacto_requisito in ('A', 'I', 'E')
				),
				a.ie_impacto_requisito)),
				max(a.ds_new_title) ds_new_title
			into 	ie_impacto_requisito_w,
				ds_new_title_w
			from 	w_man_ordem_serv_imp_pr a
			where 	a.nr_seq_ordem_serv = nr_seq_ordem_serv_p
			and 	a.nr_product_requirement = r_c02.nr_product_requirement;
		
			select 	man_ordem_serv_imp_pr_seq.nextval
			into 	nr_seq_imp_pr_w
			from 	dual;
			
			insert into man_ordem_serv_imp_pr (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_impacto,
				nr_product_requirement,
				ie_impacto_requisito,
				ds_new_title
			) values (
				nr_seq_imp_pr_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_impacto_w,
				r_c02.nr_product_requirement,
				ie_impacto_requisito_w,
				ds_new_title_w
			);
			
			for r_c03 in c03(r_c02.nr_product_requirement) loop begin
				insert into man_ordem_serv_imp_pr_func (
					nr_sequencia,
					nr_seq_imp_pr,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_funcao
				) values (
					man_ordem_serv_imp_pr_func_seq.nextval,
					nr_seq_imp_pr_w,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					r_c03.cd_funcao
				);
			end; end loop;
			
		end; end loop;
		
		
		select 	nvl(max('S'), 'N')
		into	is_add_answer_w
		from 	w_man_ordem_serv_imp_pr a
		where	a.nr_seq_ordem_serv	= nr_seq_ordem_serv_p
		and 	a.ie_impacto_requisito in ('A', 'I', 'E');
		
		if (is_add_answer_w = 'S') then
			insert_man_ordem_serv_imp_resp(nr_seq_ordem_serv_p, nr_seq_impacto_w, nm_usuario_p);
		end if;
		
		delete from w_ordem_serv_imp_quest_res 	where nr_seq_ordem_serv = nr_seq_ordem_serv_p;
		delete from w_man_ordem_serv_imp_pr 		where nr_seq_ordem_serv = nr_seq_ordem_serv_p;
		
		commit;

		
		select	count(1)
		into	qt_reg_w
		from	man_ordem_serv_imp_resp
		where	nr_seq_impacto	= nr_seq_impacto_w
		and		nvl(ie_resposta, 'N') = 'S'
		and		dt_aprovacao is null;
		
		if (qt_reg_w = 0) then
			man_liberar_analise_impacto(nr_seq_ordem_serv_p, nr_seq_impacto_w, nm_usuario_p);
		else
			avisar_aprovacao_pendente_sme(nr_seq_ordem_serv_p, nr_seq_impacto_w);
		end if;
		
	end if;
	
end inserir_man_ordem_serv_impacto;
/