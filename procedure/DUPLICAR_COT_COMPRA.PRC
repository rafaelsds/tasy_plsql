create or replace
procedure duplicar_cot_compra(
			nr_cot_compra_p			Number,
			nm_usuario_p			varchar2,
			nr_cot_nova_p		out	Number) as

nr_cot_compra_ww		number(10);
nr_item_cot_compra_w	number(10);
nr_sequencia_w		number(10);


cursor c01 is
select	nr_item_cot_compra
from	cot_compra_item
where	nr_cot_compra	= nr_cot_compra_ww;

cursor c02 is
select	nr_sequencia
from	cot_compra_forn
where	nr_cot_compra	= nr_cot_compra_ww;



begin

select	cot_compra_seq.nextval
into	nr_cot_compra_ww
from	dual;


insert into cot_compra(
	nr_cot_compra,
	dt_cot_compra,
	dt_atualizacao,
	cd_comprador,
	nm_usuario,
	ds_observacao,
	cd_pessoa_solicitante,
	cd_estabelecimento,
	dt_retorno_prev,
	dt_entrega,
	ie_finalidade_cotacao)
select	nr_cot_compra_ww,
	sysdate,
	sysdate,
	cd_comprador,
	nm_usuario_p,
	ds_observacao,
	cd_pessoa_solicitante,
	cd_estabelecimento,
	dt_retorno_prev,
	dt_entrega,
	nvl(ie_finalidade_cotacao,'C')
from	cot_compra
where	nr_cot_compra	= nr_cot_compra_p;


insert into cot_compra_item(
	nr_cot_compra,
	nr_item_cot_compra,
	cd_material,
	qt_material,
	cd_unidade_medida_compra,
	dt_atualizacao,
	dt_limite_entrega,
	nm_usuario,
	ie_situacao,
	ds_material_direto_w,
	ie_regra_preco,
	cd_estab_item)
select	nr_cot_compra_ww,
	nr_item_cot_compra,
	cd_material,
	qt_material,
	cd_unidade_medida_compra,
	sysdate,
	dt_limite_entrega,
	nm_usuario_p,
	ie_situacao,
	ds_material_direto_w,
	ie_regra_preco,
	cd_estab_item
from	cot_compra_item
where	nr_cot_compra	= nr_cot_compra_p;



open c01;
loop
fetch c01 into
	nr_item_cot_compra_w;
exit when c01%notfound;
	begin

	gerar_cot_compra_item_entrega(
		nr_cot_compra_ww,
		nr_item_cot_compra_w,
		nm_usuario_p);

	end;
end loop;
close c01;


insert into cot_compra_forn(
	nr_sequencia,
	nr_cot_compra,
	cd_cgc_fornecedor,
	cd_condicao_pagamento,
	cd_moeda,
	ie_frete,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_documento,
	qt_dias_validade,
	qt_dias_entrega,
	pr_desconto,
	pr_desconto_pgto_antec,
	pr_juros_negociado,
	ds_observacao,
	cd_pessoa_fisica,
	vl_despesa_acessoria,
	ie_gerado_bionexo,
	vl_desconto,
	ie_liberada_internet,
	ie_status_envio_email_lib)
select	cot_compra_forn_seq.nextval,
	nr_cot_compra_ww,
	cd_cgc_fornecedor,
	cd_condicao_pagamento,
	cd_moeda,
	ie_frete,
	sysdate,
	nm_usuario_p,
	ie_tipo_documento,
	qt_dias_validade,
	qt_dias_entrega,
	pr_desconto,
	pr_desconto_pgto_antec,
	pr_juros_negociado,
	ds_observacao,
	cd_pessoa_fisica,
	vl_despesa_acessoria,
	ie_gerado_bionexo,
	vl_desconto,
	'N',
	'N'
from	cot_compra_forn
where	nr_cot_compra	= nr_cot_compra_p;



open c02;
loop
fetch c02 into
	nr_sequencia_w;
exit when c02%notfound;
	begin

	gerar_cot_compra_forn_item(
			nr_cot_compra_ww,
			nr_sequencia_w,
			nm_usuario_p,'N');
	end;
end loop;
close c02;



nr_cot_nova_p	:= nr_cot_compra_ww;

end duplicar_cot_compra;
/
