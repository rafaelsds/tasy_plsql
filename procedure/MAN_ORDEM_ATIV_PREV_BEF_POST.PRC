create or replace
procedure man_ordem_ativ_prev_bef_post
			(	nr_seq_ativ_prev_p			man_ordem_ativ_prev.nr_sequencia%type,
				nr_seq_ordem_p				man_ordem_servico.nr_sequencia%type,
				nr_seq_funcao_p				number,
				nr_seq_lp_p				number,
				nm_usuario_prev_p			varchar2,
				dt_prevista_p				date,
				nm_usuario_p				varchar2,
				ie_corp_p				varchar2,
				ie_wheb_p				varchar2,
				nr_seq_motivo_alt_prev_p		number,
				nr_seq_grupo_des_p			number,
				nr_seq_grupo_sup_p			number,
				ie_prioridade_sup_p			number,
				ie_prioridade_des_p			number,
				cd_setor_atendimento_p			number,
				ie_acao_exec_p				number,
				ds_abort			out	varchar2,
				ds_confirma_triagem		out	varchar2) is

nr_seq_proj_w			proj_cronograma.nr_seq_proj%type;
ie_param_w			varchar(2);
ds_proj_w			varchar(255);
ie_acao_w			varchar(2);
cd_estabelecimento_w 		number(4);
dt_prevista_w			date;

begin
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
	
select	max(a.nr_seq_proj)
into 	nr_seq_proj_w
from	proj_cronograma a,
	proj_cron_etapa b,
	man_ordem_servico c
where	b.nr_seq_cronograma = a.nr_sequencia
and	c.nr_seq_proj_cron_etapa = b.nr_sequencia
and	c.nr_sequencia = nr_seq_ordem_p;

obter_param_usuario(297, 83, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_param_w);

ie_acao_w	:= man_obter_se_recurso_alocado(nm_usuario_prev_p,dt_prevista_p,'A',nr_seq_proj_w);

if	(ie_acao_w = 'S') and
	(ie_param_w = 'R' or ie_param_w = 'M') then
	ds_proj_w := man_obter_se_recurso_alocado(dt_prevista_p, nm_usuario_prev_p,'P',nr_seq_proj_w);
	
	if	( ie_param_w = 'R') then
		ds_abort	:= obter_texto_dic_objeto(285145, wheb_usuario_pck.get_nr_seq_idioma,	'NM_RECURSO_P='|| nm_usuario_prev_p || 
													';NM_PROJETO_P='|| ds_proj_w);
		goto final;
	elsif	(ie_param_w = 'M') then
		ds_confirma_triagem := obter_texto_dic_objeto(317179, wheb_usuario_pck.get_nr_seq_idioma,	'NM_RECURSO_P='|| nm_usuario_prev_p || 
														';NM_PROJETO_P='|| ds_proj_w);
	end if;
end if;

if	(ie_corp_p = 'S' or ie_wheb_p = 'S') then
	if	(nvl(nr_seq_grupo_sup_p,0) = 0) and
		(nvl(nr_seq_grupo_des_p,0) <> 0) and
		(nvl(ie_prioridade_des_p,0) <= 0) then
		ds_abort	:= obter_texto_tasy(226139, wheb_usuario_pck.get_nr_seq_idioma);
		
		goto final;
	elsif	(nvl(nr_seq_grupo_sup_p,0) <> 0) and
		(nvl(nr_seq_grupo_des_p,0) = 0 ) and
		(nvl(ie_prioridade_sup_p,0) <= 0) then
		ds_abort	:= obter_texto_tasy(121355, wheb_usuario_pck.get_nr_seq_idioma);
		
		goto final;
	end if;
end if;
	
if	(ie_acao_exec_p = 2) then
	select	dt_prevista  
	into	dt_prevista_w
	from	man_ordem_ativ_prev
	where	nr_sequencia	= nr_seq_ativ_prev_p;
	
	obter_param_usuario(296, 48, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_param_w);
	
	if	(dt_prevista_w is not null) and
		(ie_param_w = 'S') and
		((dt_prevista_p is null) or 
		(dt_prevista_p > dt_prevista_w)) and
		(nr_seq_motivo_alt_prev_p is null) then
		ds_abort	:= obter_texto_tasy(232081, wheb_usuario_pck.get_nr_seq_idioma);	
	end if;
end if;

obter_param_usuario(297, 70, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_param_w);

if	(ie_corp_p = 'S') and
	(ie_param_w = 'S') and
	(nr_seq_funcao_p in(11,193)) and
	(nr_seq_lp_p is null) then
	ds_abort	:= obter_texto_tasy(232239, wheb_usuario_pck.get_nr_seq_idioma);
	
	goto final;
end if;

<<final>>
ds_abort := ds_abort;

end man_ordem_ativ_prev_bef_post;
/