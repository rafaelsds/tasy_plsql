create or replace 
procedure vincular_conta_paciente(
		nr_interno_conta_p	number,
		nr_atendimento_p	number,
		nr_documento_p		number,
		nr_titulo_p		number,
		nm_usuario_p		varchar2) is
		
begin
	
	if	(nr_titulo_p is not null) and
		(nr_interno_conta_p is not null) then
		
		update	titulo_receber
		set	nr_interno_conta	= nr_interno_conta_p,
			nr_atendimento		= nvl(nr_atendimento_p,nr_atendimento),
			nr_documento		= nvl(nr_documento_p,nr_documento),
			nm_usuario		= nm_usuario_p
		where	nr_titulo		= nr_titulo_p;
		
		commit;
	
		end if;

end vincular_conta_paciente;
/