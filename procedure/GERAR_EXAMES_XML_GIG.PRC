create or replace
procedure gerar_exames_xml_gig (  dt_inicial_p  date,
			          dt_final_p    date,
			          ie_grava_log_p	varchar2) is

nr_seq_projeto_w 		number(10);
nr_seq_log_w			number(10);
ds_parametros_w			varchar2(255);
ds_xml_w			long;	
nm_arquivo_w			varchar2(255);
							 
arq_texto_w		utl_file.file_type;

dt_inicial_w			date;
dt_final_w			date;
qt_log_w			number(15);


begin


if 	(nvl(ie_grava_log_p,'S') = 'S') then
	/* Peagr a data inicial e final */

	select	max(ds_log),
		sysdate
	into	dt_inicial_w,
		dt_final_w
	from 	log_tasy
	where 	cd_log = 88903;

	if 	(dt_inicial_w is null ) then

	dt_inicial_w  := (sysdate - 7);
		
	end if;
	
end if;

/*Sequencia do projeto XML*/
nr_seq_projeto_w := 100594; 

/*Parametros do projeto XML*/
if 	(nvl(ie_grava_log_p,'S') = 'N') then	
	ds_parametros_w := 'DT_INICIAL='|| to_char(dt_inicial_p,'dd/mm/yyyy hh24:mi')   || ';' ||'DT_FINAL=' || to_char(dt_final_p,'dd/mm/yyyy hh24:mi')   ||';';
else 	
	ds_parametros_w := 'DT_INICIAL='|| to_char(dt_inicial_w,'dd/mm/yyyy hh24:mi') || ';' ||'DT_FINAL=' || to_char(dt_final_w,'dd/mm/yyyy hh24:mi') ||';';
end if;
	
/*Pegar a sequencia da tabela que ir� gerar o XML*/
select tasy_xml_banco_seq.nextval
into 	nr_seq_log_w
from 	dual;

wheb_exportar_xml(nr_seq_projeto_w,nr_seq_log_w,'',ds_parametros_w);

select	ds_xml
into	ds_xml_w
from	tasy_xml_banco
where	nr_sequencia = nr_seq_log_w;

/*Sequencia do numero gerado Ex. sistemagig'1'*/
select  (count(*) + 1)
into	qt_log_w
from 	log_tasy  
where   cd_log = 88906;

/*Grava o log para a proxima vez que executar a procedure*/
gravar_log_tasy(88906,qt_log_w,'Tasy');

commit;
/* Gera o nome do arquivo*/
nm_arquivo_w	:=	'sistemagig' || qt_log_w || '.xml';

arq_texto_w := utl_file.fopen('/mnt/tasy_xml',nm_arquivo_w,'W');

utl_file.put_line(arq_texto_w,ds_xml_w);
utl_file.fflush(arq_texto_w);

utl_file.fclose(arq_texto_w);


if 	(nvl(ie_grava_log_p,'S') = 'S') then
	/* Gravar o log */	
	gravar_log_tasy(88903,to_date(sysdate, 'dd/mm/yyyy hh24:mi'),'Tasy');
end if;	

end gerar_exames_xml_gig;
/