create or replace
procedure registrar_atend_solicitacao(	nr_sequencia_p		Number,
						nm_usuario_p		Varchar2) is 

begin
--Utilizado na Gest�o de exames SWING
if 	(nvl(nr_sequencia_p,0)  > 0) then

	update laudo_paciente_reg_result                     
	set    dt_atendimento       	= sysdate,          
	       nm_usuario_atend     	= nm_usuario_p      
	where  nr_sequencia       	= nr_sequencia_p;  

end if;

commit;

end registrar_atend_solicitacao;
/