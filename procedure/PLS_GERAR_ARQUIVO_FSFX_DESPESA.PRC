CREATE OR REPLACE PROCEDURE PLS_GERAR_ARQUIVO_FSFX_DESPESA
            (    nr_sequencia_p        number,
                ds_local_p        varchar2,
                nm_arquivo_p         varchar2) is

ds_erro_w            varchar2(255);
ds_conteudo_w            varchar2(4000);
arq_texto_w            utl_file.file_type;
qt_dias_pro_rata_w        number(10);
vazio_w                varchar2(30) := '                ';
ie_liberacao_debito_w        pls_ame_regra_geracao.ie_liberacao_debito%type;
nr_seq_ame_empresa_w        pls_ame_lote_rem_destino.nr_seq_ame_empresa%type;
nr_seq_ame_subsidiaria_w    pls_ame_lote_rem_destino.nr_seq_ame_subsidiaria%type;
dt_ref_mensalidade_w        date;
dt_fim_cobertura_w        date;
dt_inicio_cobertura_w        date;
nr_seq_lancamento_mens_w    number(10);
dt_rescisao_aux_w        varchar2(20);
ie_tipo_w            varchar2(20);

Cursor C01 is
    select    cd_matricula_estipulante,
        nm_pessoa_fisica,
        nr_cpf_dependente,
        ie_tipo,
        dt_contratacao,
        dt_rescisao,
        ie_tipo_plano,
        ie_titularidade,
        vl_detalhe,
        cd_cobranca,
        nr_seq_mensalidade_seg,
        cd_company,
        cd_grupo_unisaude,
        ie_tipo_item,
        dt_rescisao_date
    from    (select    lpad(nvl(c.cd_matricula_estipulante,'0'),8,'0') cd_matricula_estipulante,
            rpad(d.nm_pessoa_fisica,40,' ') nm_pessoa_fisica,
            rpad(decode(c.nr_seq_titular,null,' ',nvl(d.nr_cpf,' ')),11,' ') nr_cpf_dependente,
            decode(c.nr_seq_titular,null,'00', ( select    x.cd_sistema_anterior
                                    from    grau_parentesco    x
                                    where    x.nr_sequencia    = c.nr_seq_parentesco)) ie_tipo,
            decode(c.dt_contratacao,null,'        ',to_char(c.dt_contratacao,'ddmmyyyy')) dt_contratacao,
            decode(c.dt_rescisao,null,'        ',to_char(c.dt_rescisao,'ddmmyyyy')) dt_rescisao,
            lpad(b.cd_tarja_magnetica,2,'0') ie_tipo_plano,
            lpad(nvl(c.ie_titularidade,'0'),2,'00') ie_titularidade,
            sum(nvl(e.vl_detalhe,0)) vl_detalhe,
            e.cd_cobranca,
            a.nr_seq_mensalidade_seg,
            (    select    max(x.cd_codigo)
                from    pls_contrato_codigo x
                where    x.nr_seq_contrato = c.nr_seq_contrato
                and    x.nr_seq_tipo_codigo = 2) cd_company,
            (    select    max(x.cd_codigo)
                from    pls_contrato_codigo x
                where    x.nr_seq_contrato = c.nr_seq_contrato
                and    x.nr_seq_tipo_codigo = 1) cd_grupo_unisaude,
            '' ie_tipo_item,
            c.nr_seq_titular,
            c.nr_sequencia nr_seq_segurado,
            c.dt_rescisao dt_rescisao_date
        from    pessoa_fisica        d,
            pls_segurado        c,
            pls_plano        b,
            pls_ame_lote_rem_valor    a,
            pls_ame_lote_rem_valor_det e
        where    d.cd_pessoa_fisica    = c.cd_pessoa_fisica
        and    a.nr_seq_segurado    = c.nr_sequencia
        and    c.nr_seq_plano        = b.nr_sequencia
        and    e.nr_seq_lote_rem_valor    = a.nr_sequencia
        and    a.nr_seq_lote_rem_arq    = nr_sequencia_p
        and    e.ie_tipo_item        <> '20'
        group by c.cd_matricula_estipulante,d.nm_pessoa_fisica,c.nr_seq_titular,d.nr_cpf,c.dt_contratacao,c.dt_rescisao,
            b.cd_tarja_magnetica,c.ie_titularidade,e.cd_cobranca,c.nr_sequencia,c.nr_seq_parentesco,a.nr_seq_mensalidade_seg,c.nr_seq_contrato
        union all
        select    lpad(nvl(c.cd_matricula_estipulante,'0'),8,'0') cd_matricula_estipulante,
            rpad(d.nm_pessoa_fisica,40,' ') nm_pessoa_fisica,
            rpad(decode(c.nr_seq_titular,null,' ',nvl(d.nr_cpf,' ')),11,' ') nr_cpf_dependente,
            decode(c.nr_seq_titular,null,'00', ( select    x.cd_sistema_anterior
                                    from    grau_parentesco    x
                                    where    x.nr_sequencia    = c.nr_seq_parentesco)) ie_tipo,
            decode(c.dt_contratacao,null,'        ',to_char(c.dt_contratacao,'ddmmyyyy')) dt_contratacao,
            decode(c.dt_rescisao,null,'        ',to_char(c.dt_rescisao,'ddmmyyyy')) dt_rescisao,
            lpad(b.cd_tarja_magnetica,2,'0') ie_tipo_plano,
            lpad(nvl(c.ie_titularidade,'0'),2,'00') ie_titularidade,
            sum(nvl(e.vl_detalhe,0)) vl_detalhe,
            e.cd_cobranca,
            a.nr_seq_mensalidade_seg,
            (    select    max(x.cd_codigo)
                from    pls_contrato_codigo x
                where    x.nr_seq_contrato = c.nr_seq_contrato
                and    x.nr_seq_tipo_codigo = 2) cd_company,
            (    select    max(x.cd_codigo)
                from    pls_contrato_codigo x
                where    x.nr_seq_contrato = c.nr_seq_contrato
                and    x.nr_seq_tipo_codigo = 1) cd_grupo_unisaude,
            '20' ie_tipo_item,
            c.nr_seq_titular,
            c.nr_sequencia nr_seq_segurado,
            c.dt_rescisao dt_rescisao_date
        from    pessoa_fisica        d,
            pls_segurado        c,
            pls_plano        b,
            pls_ame_lote_rem_valor    a,
            pls_ame_lote_rem_valor_det e
        where    d.cd_pessoa_fisica    = c.cd_pessoa_fisica
        and    a.nr_seq_segurado    = c.nr_sequencia
        and    c.nr_seq_plano        = b.nr_sequencia
        and    e.nr_seq_lote_rem_valor    = a.nr_sequencia
        and    a.nr_seq_lote_rem_arq    = nr_sequencia_p
        and    e.ie_tipo_item        = '20'
        group by c.cd_matricula_estipulante,d.nm_pessoa_fisica,c.nr_seq_titular,d.nr_cpf,c.dt_contratacao,c.dt_rescisao,
            b.cd_tarja_magnetica,c.ie_titularidade,e.cd_cobranca,c.nr_sequencia,c.nr_seq_parentesco,a.nr_seq_mensalidade_seg,c.nr_seq_contrato)
    order by  cd_matricula_estipulante,
            cd_cobranca,
            decode(nr_seq_titular,null,nr_seq_segurado,nr_seq_titular),
            decode(nr_seq_titular,null,-1,0);

begin

select    nvl(a.ie_liberacao_debito,'N'),
    c.nr_seq_ame_empresa,
    c.nr_seq_ame_subsidiaria,
    b.dt_ref_mensalidade
into    ie_liberacao_debito_w,
    nr_seq_ame_empresa_w,
    nr_seq_ame_subsidiaria_w,
    dt_ref_mensalidade_w
from    pls_ame_lote_rem_arquivo    d,
    pls_ame_lote_rem_destino    c,
    pls_ame_lote_remessa        b,
    pls_ame_regra_geracao        a
where    d.nr_seq_lote_rem_dest        = c.nr_sequencia
and    c.nr_seq_lote_rem        = b.nr_sequencia
and    b.nr_seq_regra_geracao        = a.nr_sequencia
and    d.nr_sequencia            = nr_sequencia_p;

begin
arq_texto_w := utl_file.fopen(ds_local_p,nm_arquivo_p,'W');
exception
when others then
    if (sqlcode = -29289) then
        ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
    elsif (sqlcode = -29298) then
        ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
    elsif (sqlcode = -29291) then
        ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
    elsif (sqlcode = -29286) then
        ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
    elsif (sqlcode = -29282) then
        ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
    elsif (sqlcode = -29288) then
        ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
    elsif (sqlcode = -29287) then
        ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
    elsif (sqlcode = -29281) then
        ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
    elsif (sqlcode = -29290) then
        ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
    elsif (sqlcode = -29283) then
        ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
    elsif (sqlcode = -29280) then
        ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
    elsif (sqlcode = -29284) then
        ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
    elsif (sqlcode = -29292) then
        ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
    elsif (sqlcode = -29285) then
        ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
    else
        ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
    end if;
end;

if    (ds_erro_w is not null) then
    Raise_application_error(-20011,ds_erro_w);
end if;

for r_c01_w in C01 loop
    begin

    qt_dias_pro_rata_w     := null;

    if    (r_c01_w.nr_seq_mensalidade_seg is null) then
        dt_rescisao_aux_w    := r_c01_w.dt_rescisao;
        ie_tipo_w        := r_c01_w.ie_tipo;
    else
        select    decode(a.dt_rescisao_benef,null,'        ',to_char(a.dt_rescisao_benef,'ddmmyyyy')),
            decode(a.nr_seq_titular,null,'00', ( select    x.cd_sistema_anterior
                                from    grau_parentesco    x
                                where    x.nr_sequencia    = a.nr_seq_parentesco))
        into    dt_rescisao_aux_w,
            ie_tipo_w
        from    pls_mensalidade_segurado a
        where    a.nr_sequencia    = r_c01_w.nr_seq_mensalidade_seg;
    end if;

    if    ((substr(r_c01_w.dt_contratacao,3,6) = to_char(dt_ref_mensalidade_w,'mmyyyy')) or
        (substr(dt_rescisao_aux_w,3,6) = to_char(dt_ref_mensalidade_w,'mmyyyy'))) and
        (r_c01_w.nr_seq_mensalidade_seg is not null) then
        select    dt_fim_cobertura,
            dt_inicio_cobertura
        into    dt_fim_cobertura_w,
            dt_inicio_cobertura_w
        from    pls_mensalidade_segurado
        where    nr_sequencia    = r_c01_w.nr_seq_mensalidade_seg;

        qt_dias_pro_rata_w    := obter_dias_entre_datas(dt_inicio_cobertura_w,dt_fim_cobertura_w) + 1;
    end if;

    if    (r_c01_w.ie_tipo_item = '20') then
        select  max(a.nr_seq_lancamento_mens)
        into    nr_seq_lancamento_mens_w
        from	pls_mensalidade_seg_item    a
        where   a.nr_seq_mensalidade_seg    = r_c01_w.nr_seq_mensalidade_seg
        and    	a.ie_tipo_item            = '20';

        if    (nr_seq_lancamento_mens_w is not null) then
            select    dt_fim_cobertura,
                dt_inicio_cobertura
            into    dt_fim_cobertura_w,
                dt_inicio_cobertura_w
            from    pls_lancamento_mensalidade
            where    nr_sequencia    = nr_seq_lancamento_mens_w;
            qt_dias_pro_rata_w    := obter_dias_entre_datas(dt_inicio_cobertura_w,dt_fim_cobertura_w) + 1;

            vazio_w    := to_char(dt_inicio_cobertura_w,'ddmmyyyy')||to_char(dt_fim_cobertura_w,'ddmmyyyy');
        end if;
    end if;

    if    (r_c01_w.vl_detalhe < 0) then
        r_c01_w.vl_detalhe    := r_c01_w.vl_detalhe * -1;
    end if;

    if    (qt_dias_pro_rata_w is not null) and
        (qt_dias_pro_rata_w > 31) then
        qt_dias_pro_rata_w    := 31;
    end if;

    ds_conteudo_w    := r_c01_w.cd_company|| r_c01_w.cd_matricula_estipulante||rpad(fsfx_elimina_acentos(upper(r_c01_w.nm_pessoa_fisica),'S'),40,' ')||  r_c01_w.nr_cpf_dependente||
                lpad(nvl(ie_tipo_w,'0'),2,'0')||r_c01_w.dt_contratacao||dt_rescisao_aux_w||r_c01_w.cd_cobranca||
                replace(lpad(campo_mascara(r_c01_w.vl_detalhe,2),13,'0'),'.',',')||r_c01_w.ie_tipo_plano||
                lpad(nvl(to_char(qt_dias_pro_rata_w),' '),2,' ') ||ie_liberacao_debito_w||r_c01_w.cd_grupo_unisaude||r_c01_w.ie_titularidade||
                to_char(dt_ref_mensalidade_w,'ddmmyyyy')||vazio_w;

    utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
    utl_file.fflush(arq_texto_w);

    vazio_w    := '                ';

    end;
end loop;

utl_file.fclose(arq_texto_w);

end PLS_GERAR_ARQUIVO_FSFX_DESPESA;
/