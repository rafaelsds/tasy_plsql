create or replace 
procedure Consiste_Agenda_Turno_Serv(nm_usuario_p             varchar2,
									 cd_estabelecimento_p     number,
									 ie_dia_semana_p          number,
									 hr_inicial_p             date,
									 hr_final_p               date,
									 nr_seq_sala_p            number,
									 nr_seq_turno_p           number,
									 ds_erro_p		      out varchar2) is

ie_consiste_sala_turno_w varchar2(1);
cd_agenda_w              agenda_turno.cd_agenda%type;
ds_erro_w                varchar2(255) := '';
ds_agenda_w              agenda.ds_agenda%type;

begin

obter_param_usuario(866, 314, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_sala_turno_w);

if (ie_consiste_sala_turno_w = 'S') and (nr_seq_sala_p is not null) then
	select nvl(max(cd_agenda),0)
	into cd_agenda_w
	from agenda_turno at
	where ((at.nr_sequencia <> nr_seq_turno_p) or (nr_seq_turno_p is null))
	and ((at.ie_dia_semana = ie_dia_semana_p)
		 or ((at.ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
		 or ((ie_dia_semana_p = 9) and (at.ie_dia_semana not in (1,7)))
		 or (ie_dia_semana_p is null))
	and at.nr_seq_sala = nr_seq_sala_p
	and (to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(at.hr_inicial, 'hh24:mi:ss'))
		 or (to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(at.hr_inicial, 'hh24:mi:ss')))
	and (to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(at.hr_final, 'hh24:mi:ss'))
		 or (to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(at.hr_final, 'hh24:mi:ss')))
	and ((trunc(at.dt_inicio_vigencia) is null and trunc(at.dt_final_vigencia) is null)
		 or (trunc(at.dt_inicio_vigencia) <= trunc(sysdate) and trunc(at.dt_final_vigencia) is null)
		 or (trunc(at.dt_inicio_vigencia) is null and trunc(at.dt_final_vigencia) >= trunc(sysdate))
		 or (trunc(at.dt_inicio_vigencia) <= trunc(sysdate) and trunc(at.dt_final_vigencia) >= trunc(sysdate)));
		 
	if (cd_agenda_w > 0) then
		select ds_agenda
		into ds_agenda_w
		from agenda
		where cd_agenda = cd_agenda_w;	
	
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(1096603,'DS_AGENDA='||ds_agenda_w||';CD_AGENDA_OCUP='||cd_agenda_w);
	end if;
end if;

ds_erro_p := ds_erro_w;

end Consiste_Agenda_Turno_Serv;
/
