create or replace
procedure gerar_prescr_patologia_cirur(	nr_seq_agenda_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) IS 

nr_prescricao_w			Number(14,0);
ie_origem_inf_w			Varchar(1);
ie_adep_w		    	varchar2(10);
nr_cirurgia_w			number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_sequencia_w			number(10);
cd_cgc_w			varchar2(14);
cd_setor_atendimento_w		number(5);
nr_seq_proc_interno_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);
cd_medico_w			varchar2(10);
dt_prescricao_w			date;
hr_inicio_w			date;

cursor	c01 is
	select	b.cd_procedimento,
		b.ie_origem_proced,
		b.nr_sequencia,
		a.cd_cgc
	from	agenda_pac_servico a,
		proc_interno b
	where	a.nr_seq_agenda    	= nr_seq_agenda_p
	and	a.nr_seq_proc_servico 	= b.nr_sequencia
	and	a.ie_status 		<> 'C'
	and	b.ie_tipo 		= 'AP'
	and	not exists	(select	1 
				from 	prescr_procedimento x
				where	x.nr_seq_proc_interno 	= b.nr_sequencia
				and	x.nr_prescricao 	= nr_prescricao_w);

BEGIN

begin
select	nvl(a.nr_cirurgia,0),
	nvl(b.cd_setor_exclusivo,0),
	a.cd_pessoa_fisica,
	a.nr_atendimento,
	a.cd_medico,
	to_date(to_char(a.dt_agenda,'dd/mm/yyyy') || ' ' || to_char(a.hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
	a.hr_inicio
into	nr_cirurgia_w,
	cd_setor_atendimento_w,
	cd_pessoa_fisica_w,
	nr_atendimento_w,
	cd_medico_w,
	dt_prescricao_w,
	hr_inicio_w
from	agenda_paciente a,
	Agenda b
where 	a.cd_agenda	= b.cd_agenda
and	a.nr_sequencia	= nr_seq_agenda_p;
exception
when others then
	nr_cirurgia_w := 0;
end;

if	(nr_cirurgia_w > 0) then
	if	(cd_setor_atendimento_w = 0) then
		select	max(cd_setor_atendimento)
		into	cd_setor_atendimento_w
		from 	usuario
		where 	nm_usuario = nm_usuario_p;
	end if;

	select	nvl(max(a.nr_prescricao),0)
	into	nr_prescricao_w
	from	prescr_medica a
	where	a.nr_cirurgia_patologia = nr_cirurgia_w
	and	a.ie_tipo_prescr_cirur	= 3;
	
	select 	nvl(max('1'),'3')
	into	ie_origem_inf_w
	from	Medico b, 
		Usuario a
	where 	a.nm_usuario		= nm_usuario_p
	and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

	if	(nr_prescricao_w = 0) then
		begin
		-- Obter ie_origem_inf se � m�dico ou n�o
		Select 	Prescr_medica_seq.nextval  
		into  	nr_prescricao_w
		from 	dual;

		select	nvl(max(ie_adep),'N')
		into	ie_adep_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;
			
		insert into prescr_medica (
			nr_prescricao,      
			cd_pessoa_fisica,      
			nr_atendimento,      
			cd_medico,      
			dt_prescricao,      
			dt_atualizacao,      
			nm_usuario,
			nm_usuario_original,      
			nr_horas_validade,      
			dt_primeiro_horario,      
			dt_liberacao,      
			cd_setor_atendimento,
			cd_setor_entrega,
			dt_entrega,
			ie_origem_inf,
			nr_seq_agenda,
			ie_recem_nato,
			cd_estabelecimento,
			cd_prescritor,
			ie_adep,
			nr_cirurgia_patologia,
			ie_tipo_prescr_cirur)
		values	(nr_prescricao_w,
			cd_pessoa_fisica_w,
			nr_atendimento_w,
			cd_medico_w,
			dt_prescricao_w,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,		
			24,
			hr_inicio_w,
			null,
			cd_setor_atendimento_w,
			cd_setor_atendimento_w,
			null,
			ie_origem_inf_w,
			nr_seq_agenda_p,
			'N',
			cd_estabelecimento_p,
			obter_dados_usuario_opcao(nm_usuario_p, 'C'),
			ie_adep_w,
			nr_cirurgia_w,
			3);
		end;
	end if;
	
	open c01;
	loop
	fetch c01 into
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		cd_cgc_w;
	exit when c01%notfound;
		select	nvl(max(nr_sequencia),0)+1
		into	nr_sequencia_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_w;
		
		insert	into	prescr_procedimento
				(nr_prescricao,
				nr_sequencia,
				nr_seq_interno,
				cd_procedimento,
				ie_origem_proced,
				nr_seq_proc_interno,
				cd_cgc_laboratorio,
				ie_amostra,
				ie_suspenso,
				qt_procedimento,
				ie_origem_inf,
				nm_usuario,
				dt_atualizacao
				)
		values		(nr_prescricao_w,
				nr_sequencia_w,
				prescr_procedimento_seq.nextval,
				cd_procedimento_w,
				ie_origem_proced_w,
				nr_seq_proc_interno_w,
				cd_cgc_w,
				'N',
				'N',
				1,
				ie_origem_inf_w,
				nm_usuario_p,
				sysdate);
	end loop;
	close c01;	
	commit;
end if;	

END gerar_prescr_patologia_cirur;
/