create or replace
procedure ptu_val_pedcompl_v50_ws( 	nr_seq_pedido_aut_compl_p  		ptu_pedido_compl_aut.nr_sequencia%type,
					ie_possui_pedido_p			varchar2,
					nm_usuario_p				usuario.nm_usuario%type,					
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_resp_pedido_compl_p	out	ptu_resposta_autorizacao.nr_sequencia%type) is				 
				 
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Rotina utilizada para consistir o pedido de complemento de autorização e para popular as
tabelas quentes da Autorização ou Requisição 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relatórios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

ptu_val_scs_ws_pck.ptu_processa_pedido_aut_compl( nr_seq_pedido_aut_compl_p, ie_possui_pedido_p, nm_usuario_p,	cd_estabelecimento_p, nr_seq_resp_pedido_compl_p);

end ptu_val_pedcompl_v50_ws;
/