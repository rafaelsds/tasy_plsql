create or replace
procedure excluir_usuario_filtro_ind(
			nr_sequencia_p		number) is 

begin
		       
if(nr_sequencia_p is not null) then		       
	delete from usuario_filtro_indicador 
	where  nr_sequencia = nr_sequencia_p;
end if;


commit;

end excluir_usuario_filtro_ind;
/