Create or replace 
procedure pls_gravar_motivo_quest
			(	nr_seq_motivo_p		Number,
				nr_seq_conta_proc_p	Number,
				nr_seq_conta_mat_p	Number,
				nm_usuario_p		varchar2) is

begin

insert into ptu_questionamento_item
	(nr_sequencia, nr_seq_motivo, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	nr_seq_conta_proc, nr_seq_conta_mat)
values	(ptu_questionamento_item_seq.nextval, nr_seq_motivo_p, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	nr_seq_conta_proc_p, nr_seq_conta_mat_p); 

end pls_gravar_motivo_quest;
/
