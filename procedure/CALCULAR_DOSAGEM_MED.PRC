CREATE OR REPLACE
PROCEDURE Calcular_Dosagem_Med(
				cd_material_p			Number,
				qt_dose_p    in out		Number,
				cd_unid_med_dose_p		Varchar2,
				qt_dose_diluente_p		Number,
				qt_peso_p			Number,
				qt_dose_manutencao_p	in out	Number,
				ie_unidade_manutencao_p		Varchar2,
				qt_velocidade_p		in out	Number,
				ie_unidade_p			Varchar2) is

qt_vol_total_w			Number(15,4);
qt_concent_med_w		Number(15,6);
qt_concent_med_mg_w		Number(13,4);
qt_concent_med_ml_w		Number(13,4) := 1;
qt_concent_dose_total_w		Number(13,4);
qt_velocidade_w			Number(13,4);
qt_dose_w			Number(18,6);
qt_dose_peso_w			Number(18,6);
qt_dose_mg_w			Number(18,6);
qt_dose_direta_w		Number(18,6);
qt_dose_ml_w			Number(18,6);
cd_unid_med_concetracao_w	Varchar2(30);	
cd_unid_med_base_conc_w		Varchar2(30);

BEGIN

qt_dose_p	:= obter_conversao_ml(cd_material_p,qt_dose_p,cd_unid_med_dose_p);
qt_vol_total_w	:= qt_dose_p + qt_dose_diluente_p;

select	max(nvl(qt_conversao_mg,1)),
	max(upper(cd_unid_med_concetracao)),
	max(upper(cd_unid_med_base_conc))
into	qt_concent_med_w,
	cd_unid_med_concetracao_w,
	cd_unid_med_base_conc_w
from	material
where	cd_material	= cd_material_p;

/*Converter concentração para mg/ml*/

if	(cd_unid_med_concetracao_w = 'KG') then
	qt_concent_med_mg_w	:= qt_concent_med_w * 1000 * 1000;
elsif	(cd_unid_med_concetracao_w = 'G') then
	qt_concent_med_mg_w	:= qt_concent_med_w * 1000;
elsif	(cd_unid_med_concetracao_w = 'MCG') then
	qt_concent_med_mg_w	:= dividir(qt_concent_med_w,1000);
elsif	(cd_unid_med_concetracao_w = 'MG') then
	qt_concent_med_mg_w	:= qt_concent_med_w;
end if;

if	(cd_unid_med_base_conc_w = 'L') then
	qt_concent_med_ml_w	:= qt_concent_med_w * 1000;
end if;

qt_concent_med_w	:= dividir(qt_concent_med_mg_w,qt_concent_med_ml_w) * qt_dose_p;

/*Obter a concentração da dose total com base na unidade selecionada pelo médico */
if	(instr(ie_unidade_manutencao_p,'MCG') > 0) then
	qt_concent_dose_total_w	:= dividir(qt_concent_med_w,qt_vol_total_w) * 1000;
elsif	(instr(ie_unidade_manutencao_p,'MG') > 0) then
	qt_concent_dose_total_w	:= dividir(qt_concent_med_w,qt_vol_total_w);
end if;

if	(qt_dose_manutencao_p <> 0) then
	begin
	qt_dose_direta_w	:= qt_dose_manutencao_p;

	/*Conversão p/ unidade direta do elemento se for por peso*/

	if	(instr(ie_unidade_manutencao_p,'KG') > 0) then
		qt_dose_direta_w	:= qt_peso_p * qt_dose_manutencao_p;
	end if;

	qt_dose_ml_w	:= dividir(qt_dose_direta_w,qt_concent_dose_total_w);
	
	if	(instr(ie_unidade_manutencao_p,'/MIN') > 0) or
		(instr(ie_unidade_manutencao_p,'/H') > 0) then
		begin
		/*Transformar em ml/hora */
		if	(upper(ie_unidade_p) = 'MLH') then
			if	(instr(ie_unidade_manutencao_p,'MIN') > 0) then
				qt_dose_ml_w	:= qt_dose_ml_w * 60;
			end if;
		end if;

		/*Transformar em gotas/minuto */
	
		if	(upper(ie_unidade_p) = 'GTM') then
			qt_dose_ml_w	:= qt_dose_ml_w * 20;
			if	(instr(ie_unidade_manutencao_p,'/H') > 0) then
				qt_dose_ml_w	:= dividir(qt_dose_ml_w,60);
			end if;
		end if;	

		/*Transformar em microgotas/minuto */
		if	(upper(ie_unidade_p) = 'MGM') then
			qt_dose_ml_w	:= qt_dose_ml_w * 60;
			if	(instr(ie_unidade_manutencao_p,'/H') > 0) then
				qt_dose_ml_w	:= dividir(qt_dose_ml_w,60);
			end if;
		end if;
		qt_velocidade_p	:= qt_dose_ml_w;
		end;
	else
		qt_velocidade_p	:= qt_dose_ml_w;
	end if;
	end;
else
	begin
	qt_velocidade_w		:= qt_velocidade_p;
	if	(upper(ie_unidade_p) = 'GTM') then
		qt_velocidade_w	:= dividir(qt_velocidade_p,20) * 60;
	end if;
			
	qt_dose_manutencao_p	:= qt_concent_dose_total_w * qt_velocidade_w;
	if	(instr(ie_unidade_manutencao_p,'KG') > 0) then
		qt_dose_manutencao_p	:= dividir(qt_dose_manutencao_p,qt_peso_p);
	end if;
	if	(instr(ie_unidade_manutencao_p,'MIN') > 0) then
		qt_dose_manutencao_p	:= dividir(qt_dose_manutencao_p,60);
	end if;		
	end;
end if;

END Calcular_Dosagem_Med;
/
