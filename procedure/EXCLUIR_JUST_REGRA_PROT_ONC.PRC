create or replace
procedure excluir_just_regra_prot_onc(
			NR_SEQ_PACIENTE_P	number,
			NR_SEQ_MATERIAL_P		number,
			NR_SEQ_REGRA_p			number) is

qtd_regra_atend_w     NUMBER(10);

begin

select COUNT(obter_just_regra_prot_onc(NR_SEQ_PACIENTE_P, NR_SEQ_MATERIAL_P, NR_SEQ_REGRA_p))
INTO  qtd_regra_atend_w 
FROM DUAL;

IF(qtd_regra_atend_w > 0)THEN
	DELETE FROM paciente_atend_erro_just 
	WHERE nr_seq_paciente = NR_SEQ_PACIENTE_P
	and nr_seq_material = NR_SEQ_MATERIAL_P 
	and nr_seq_regra = NR_SEQ_REGRA_p;
END IF;

commit;

end excluir_just_regra_prot_onc;
/