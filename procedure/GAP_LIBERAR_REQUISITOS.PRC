create or replace PROCEDURE GAP_LIBERAR_REQUISITOS(nr_seq_requisito_p	number,
                                 ie_opcao_p         varchar2,
                                 nm_usuario_p       varchar2) is	
                                 
    ie_liberar_desenv_old_w latam_requisito.ie_liberar_desenv%type;                                
BEGIN

IF (ie_opcao_p = 'A') THEN	
    UPDATE	    LATAM_REQUISITO
    SET         DT_LIBERACAO	    = sysdate,
                NM_USUARIO_LIB	= nm_usuario_p
    WHERE       NR_SEQUENCIA 	    = nr_seq_requisito_p
    RETURNING   IE_LIBERAR_DESENV INTO ie_liberar_desenv_old_w;
ELSE
    UPDATE      LATAM_REQUISITO
    SET         DT_LIBERACAO	= null,
                NM_USUARIO_LIB	= null
    WHERE       NR_SEQUENCIA  = nr_seq_requisito_p
    RETURNING   IE_LIBERAR_DESENV INTO ie_liberar_desenv_old_w;
END IF;

generate_latam_log(nr_seq_requisito_p, ie_liberar_desenv_old_w, CASE WHEN ie_opcao_p = 'A' THEN 717011 ELSE 317804 END);

COMMIT;

END GAP_LIBERAR_REQUISITOS;
/
