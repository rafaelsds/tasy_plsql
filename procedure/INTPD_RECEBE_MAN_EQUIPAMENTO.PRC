create or replace
procedure intpd_recebe_man_equipamento(	nr_sequencia_p	in	number,
					xml_p		in	xmltype) is 

ds_id_origin_w				intpd_eventos_sistema.ds_id_origin%type;
ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;
nr_seq_sistema_w			intpd_eventos_sistema.nr_seq_sistema%type;
nr_seq_projeto_xml_w			intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_regra_conv_w			conversao_meio_externo.nr_seq_regra%type;
ie_sistema_externo_w			varchar2(15);
reg_integracao_w			gerar_int_padrao.reg_integracao_conv;
man_equipamento_w				man_equipamento%rowtype;
ie_erro_w				varchar2(1) := 'N';
nr_man_equipamento_delete_w		man_equipamento.nr_sequencia%type;
qt_registros_w				number(10);
nr_man_equipamento_hosp_w 		man_equipamento.nr_sequencia%type;
qt_reg_equip_hosp_w			number(10);
ds_erro_w				varchar2(2000);
ds_operacao_w				varchar2(255);

cursor c01 is
select	*
from	xmltable('/STRUCTURE/EQUIPMENT' passing xml_p columns	

	IE_ACTION				VARCHAR2(15)	path 'IE_ACTION',
	CD_IMOBILIZADO_EXT			VARCHAR2(40)	path 'CD_IMOBILIZADO_EXT',
	DS_EQUIPAMENTO				VARCHAR2(80)	path 'DS_EQUIPAMENTO',
	IE_CONTROLE_SETOR			VARCHAR2(1)	path 'IE_CONTROLE_SETOR',
	IE_DISPONIBILIDADE			VARCHAR2(1)	path 'IE_DISPONIBILIDADE',
	IE_PARADO				VARCHAR2(1)	path 'IE_PARADO',
	IE_PROPRIEDADE				VARCHAR2(1)	path 'IE_PROPRIEDADE',
	IE_ROTINA_SEGURANCA			VARCHAR2(1)	path 'IE_ROTINA_SEGURANCA',
	IE_SITUACAO				VARCHAR2(1)	path 'IE_SITUACAO',
	NM_USUARIO				VARCHAR2(15)	path 'NM_USUARIO',
	NR_SEQ_LOCAL				NUMBER(10,0)	path 'NR_SEQ_LOCAL',
	NR_SEQ_PLANEJ				NUMBER(10,0)	path 'NR_SEQ_PLANEJ',
	NR_SEQ_TIPO_EQUIP			NUMBER(10,0)	path 'NR_SEQ_TIPO_EQUIP',
	NR_SEQ_TRAB				NUMBER(10,0)	path 'NR_SEQ_TRAB',
	CD_ESTAB_CONTABIL			NUMBER(10,0)	path 'CD_ESTAB_CONTABIL',
	IE_EQUIP_HOSPITALAR			VARCHAR2(15)	path 'IE_EQUIP_HOSPITALAR',
	CD_CONTROLE				VARCHAR2(20)	path 'CD_CONTROLE',
	CD_FORNECEDOR				VARCHAR2(14)	path 'CD_FORNECEDOR',
	CD_IMOBILIZADO				VARCHAR2(20)	path 'CD_IMOBILIZADO',
	CD_IMPACTO				VARCHAR2(1)	path 'CD_IMPACTO',
	CD_LOTE_FABRICACAO			VARCHAR2(20)	path 'CD_LOTE_FABRICACAO',
	CD_MATERIAL				NUMBER(6,0)	path 'CD_MATERIAL',
	CD_MOEDA				NUMBER(3,0)	path 'CD_MOEDA',
	CD_NACIONALIDADE			VARCHAR2(8)	path 'CD_NACIONALIDADE',
	CD_PESSOA_TERCEIRO			VARCHAR2(10)	path 'CD_PESSOA_TERCEIRO',
	DS_COR_EQUIP				VARCHAR2(255)	path 'DS_COR_EQUIP',
	DS_DANO_BREVE				VARCHAR2(80)	path 'DS_DANO_BREVE',
	DS_ENDERECO				VARCHAR2(255)	path 'DS_ENDERECO',
	DS_MARCA				VARCHAR2(50)	path 'DS_MARCA',
	DS_MODELO				VARCHAR2(50)	path 'DS_MODELO',
	DS_OBSERVACAO				VARCHAR2(2000)	path 'DS_OBSERVACAO',
	DS_OBSERVACAO_TENSAO			VARCHAR2(2000)	path 'DS_OBSERVACAO_TENSAO',
	DS_PADRAO_OS				VARCHAR2(2000)	path 'DS_PADRAO_OS',
	DT_ANO_FABRICACAO			NUMBER(4,0)	path 'DT_ANO_FABRICACAO',
	DT_AQUISICAO				VARCHAR2(20)	path 'DT_AQUISICAO',
	DT_FIM_GARANTIA				VARCHAR2(20)	path 'DT_FIM_GARANTIA',
	DT_INICIO_GARANTIA			VARCHAR2(20)	path 'DT_INICIO_GARANTIA',
	DT_INSTALACAO				VARCHAR2(20)	path 'DT_INSTALACAO',
	DT_PROG_PREVENTIVA			VARCHAR2(20)	path 'DT_PROG_PREVENTIVA',
	IE_CALIBRACAO				VARCHAR2(1)	path 'IE_CALIBRACAO',
	IE_CLASSIFICACAO_OS			VARCHAR2(1)	path 'IE_CLASSIFICACAO_OS',
	IE_CONSISTE_OS_DUPLIC			VARCHAR2(15)	path 'IE_CONSISTE_OS_DUPLIC',
	CD_CGC_TERC				VARCHAR2(14)	path 'CD_CGC_TERC',
	IE_EXIGE_FUNCAO_OS			VARCHAR2(15)	path 'IE_EXIGE_FUNCAO_OS',
	IE_PRIORIDADE				VARCHAR2(15)	path 'IE_PRIORIDADE',
	IE_TENSAO_AC				VARCHAR2(20)	path 'IE_TENSAO_AC',
	IE_TENSAO_AC_SAIDA			VARCHAR2(20)	path 'IE_TENSAO_AC_SAIDA',
	IE_TENSAO_DC				VARCHAR2(20)	path 'IE_TENSAO_DC',
	IE_TENSAO_DC_SAIDA			VARCHAR2(20)	path 'IE_TENSAO_DC_SAIDA',
	IE_TIPO_ORDEM				VARCHAR2(15)	path 'IE_TIPO_ORDEM',
	IE_VOLTAGEM				VARCHAR2(15)	path 'IE_VOLTAGEM',
	NR_DOC_GARANTIA				VARCHAR2(20)	path 'NR_DOC_GARANTIA',
	NR_REGISTRO_ANVISA			VARCHAR2(60)	path 'NR_REGISTRO_ANVISA',
	NR_SEQ_BEM				NUMBER(10,0)	path 'NR_SEQ_BEM',
	NR_SEQ_CATEGORIA			NUMBER(10,0)	path 'NR_SEQ_CATEGORIA',
	NR_SEQ_CLASSIF_ELET_EQUIP		NUMBER(10,0)	path 'NR_SEQ_CLASSIF_ELET_EQUIP',
	NR_SEQ_CLASSIF_RISCO			NUMBER(10,0)	path 'NR_SEQ_CLASSIF_RISCO',
	NR_SEQ_CONTADOR				NUMBER(10,0)	path 'NR_SEQ_CONTADOR',
	NR_SEQ_ESTAGIO_INICIO_OS		NUMBER(10,0)	path 'NR_SEQ_ESTAGIO_INICIO_OS',
	NR_SEQ_FABRICANTE			NUMBER(10,0)	path 'NR_SEQ_FABRICANTE',
	NR_SEQ_FAMILIA				NUMBER(10,0)	path 'NR_SEQ_FAMILIA',
	NR_SEQ_ITEM_NF_INTEG			NUMBER(5,0)	path 'NR_SEQ_ITEM_NF_INTEG',
	NR_SEQ_MARCA				NUMBER(10,0)	path 'NR_SEQ_MARCA',
	NR_SEQ_MODELO				NUMBER(10,0)	path 'NR_SEQ_MODELO',
	NR_SEQ_NF_INTEG				NUMBER(10,0)	path 'NR_SEQ_NF_INTEG',
	NR_SEQ_ORIGEM_DANO			NUMBER(10,0)	path 'NR_SEQ_ORIGEM_DANO',
	NR_SEQ_STATUS				NUMBER(10,0)	path 'NR_SEQ_STATUS',
	NR_SEQ_SUPERIOR				NUMBER(10,0)	path 'NR_SEQ_SUPERIOR',
	NR_SEQ_TIPO_ORDEM			NUMBER(10,0)	path 'NR_SEQ_TIPO_ORDEM',
	NR_SERIE				VARCHAR2(30)	path 'NR_SERIE',
	QT_ALTURA				NUMBER(7,2)	path 'QT_ALTURA',
	QT_CARGA_HORARIA			NUMBER(5,2)	path 'QT_CARGA_HORARIA',
	QT_COMPRIMENTO				NUMBER(7,2)	path 'QT_COMPRIMENTO',
	QT_CORRENTE_AC				VARCHAR2(20)	path 'QT_CORRENTE_AC',
	QT_CORRENTE_AC_SAIDA			VARCHAR2(20)	path 'QT_CORRENTE_AC_SAIDA',
	QT_CORRENTE_DC				VARCHAR2(20)	path 'QT_CORRENTE_DC',
	QT_CORRENTE_DC_SAIDA			VARCHAR2(20)	path 'QT_CORRENTE_DC_SAIDA',
	QT_LARGURA				NUMBER(7,2)	path 'QT_LARGURA',
	QT_PESO					NUMBER(9,3)	path 'QT_PESO',
	QT_POTENCIA_AC				VARCHAR2(20)	path 'QT_POTENCIA_AC',
	QT_POTENCIA_AC_SAIDA			VARCHAR2(20)	path 'QT_POTENCIA_AC_SAIDA',
	QT_POTENCIA_DC				VARCHAR2(20)	path 'QT_POTENCIA_DC',
	QT_POTENCIA_DC_SAIDA			VARCHAR2(20)	path 'QT_POTENCIA_DC_SAIDA',
	QT_TEMPO_VIDA_UTIL			NUMBER(10,0)	path 'QT_TEMPO_VIDA_UTIL',
	VL_AQUISICAO				NUMBER(15,2)	path 'VL_AQUISICAO',
	VL_CUSTO_SEM_UTILIZACAO			NUMBER(15,2)	path 'VL_CUSTO_SEM_UTILIZACAO',
	CD_CENTRO_CUSTO				NUMBER(8,0)	path 'CD_CENTRO_CUSTO');
	
c01_w	c01%rowtype;		
		
begin

select	id_origin
into	ds_id_origin_w
from	xmltable('/STRUCTURE' passing xml_p
	columns id_origin	varchar2(10) path 'ID_ORIGIN');

select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv,
	nvl(ds_id_origin,ds_id_origin_w)
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_conv_w,
	ds_id_origin_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w				:=	nr_seq_sistema_w;

reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:=	'R';
reg_integracao_w.ie_sistema_externo		:=	ie_sistema_externo_w;
reg_integracao_w.ie_conversao			:=	ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:=	nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao		:=	nr_seq_regra_conv_w;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin	
	ie_erro_w := 'N';

	if	(c01_w.ie_action = 'E') then
		begin
			select	nvl(max(nr_sequencia),0)
			into	nr_man_equipamento_delete_w
			from	man_equipamento
			where	CD_IMOBILIZADO_EXT = c01_w.CD_IMOBILIZADO_EXT;

			if (nr_man_equipamento_delete_w > 0) then
				ds_operacao_w	:= 'INATIVAR MAN_EQUIPAMENTO';
				update man_equipamento set ie_situacao = 'I' where nr_sequencia = nr_man_equipamento_delete_w;
			end if;
		exception
		when others then
			ie_erro_w := 'S';
			ds_erro_w := substr(ds_operacao_w || ': ' || sqlerrm(sqlcode),1,2000);
			intpd_gravar_log_recebimento(nr_sequencia_p, ds_erro_w, c01_w.nm_usuario);
		end;
	else
		begin
			reg_integracao_w.nm_tabela		:=	'MAN_EQUIPAMENTO';
			reg_integracao_w.nm_elemento		:=	'EQUIPMENT';
			reg_integracao_w.nr_seq_visao		:=	31105;
			
			intpd_processar_atributo(reg_integracao_w, 'CD_IMOBILIZADO_EXT', c01_w.CD_IMOBILIZADO_EXT, 'S', man_equipamento_w.CD_IMOBILIZADO_EXT);
			intpd_processar_atributo(reg_integracao_w, 'DS_EQUIPAMENTO', c01_w.DS_EQUIPAMENTO, 'S', man_equipamento_w.DS_EQUIPAMENTO);
			intpd_processar_atributo(reg_integracao_w, 'IE_CONTROLE_SETOR', c01_w.IE_CONTROLE_SETOR, 'S', man_equipamento_w.IE_CONTROLE_SETOR);
			intpd_processar_atributo(reg_integracao_w, 'IE_DISPONIBILIDADE', c01_w.IE_DISPONIBILIDADE, 'S', man_equipamento_w.IE_DISPONIBILIDADE);
			intpd_processar_atributo(reg_integracao_w, 'IE_PARADO', c01_w.IE_PARADO, 'S', man_equipamento_w.IE_PARADO);
			intpd_processar_atributo(reg_integracao_w, 'IE_PROPRIEDADE', c01_w.IE_PROPRIEDADE, 'S', man_equipamento_w.IE_PROPRIEDADE);
			intpd_processar_atributo(reg_integracao_w, 'IE_ROTINA_SEGURANCA', c01_w.IE_ROTINA_SEGURANCA, 'S', man_equipamento_w.IE_ROTINA_SEGURANCA);
			intpd_processar_atributo(reg_integracao_w, 'IE_SITUACAO', c01_w.IE_SITUACAO, 'S', man_equipamento_w.IE_SITUACAO);
			intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c01_w.NM_USUARIO, 'S', man_equipamento_w.NM_USUARIO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_LOCAL', c01_w.NR_SEQ_LOCAL, 'S', man_equipamento_w.NR_SEQ_LOCAL);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_PLANEJ', c01_w.NR_SEQ_PLANEJ, 'S', man_equipamento_w.NR_SEQ_PLANEJ);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TIPO_EQUIP', c01_w.NR_SEQ_TIPO_EQUIP, 'S', man_equipamento_w.NR_SEQ_TIPO_EQUIP);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TRAB', c01_w.NR_SEQ_TRAB, 'S', man_equipamento_w.NR_SEQ_TRAB);
			intpd_processar_atributo(reg_integracao_w, 'CD_ESTAB_CONTABIL', c01_w.CD_ESTAB_CONTABIL, 'S', man_equipamento_w.CD_ESTAB_CONTABIL);
			intpd_processar_atributo(reg_integracao_w, 'IE_EQUIP_HOSPITALAR', c01_w.IE_EQUIP_HOSPITALAR, 'S', man_equipamento_w.IE_EQUIP_HOSPITALAR);
			intpd_processar_atributo(reg_integracao_w, 'CD_CONTROLE', c01_w.CD_CONTROLE, 'S', man_equipamento_w.CD_CONTROLE);
			intpd_processar_atributo(reg_integracao_w, 'CD_FORNECEDOR', c01_w.CD_FORNECEDOR, 'S', man_equipamento_w.CD_FORNECEDOR);
			intpd_processar_atributo(reg_integracao_w, 'CD_IMOBILIZADO', c01_w.CD_IMOBILIZADO, 'S', man_equipamento_w.CD_IMOBILIZADO);
			intpd_processar_atributo(reg_integracao_w, 'CD_IMPACTO', c01_w.CD_IMPACTO, 'S', man_equipamento_w.CD_IMPACTO);
			intpd_processar_atributo(reg_integracao_w, 'CD_LOTE_FABRICACAO', c01_w.CD_LOTE_FABRICACAO, 'S', man_equipamento_w.CD_LOTE_FABRICACAO);
			intpd_processar_atributo(reg_integracao_w, 'CD_MATERIAL', c01_w.CD_MATERIAL, 'S', man_equipamento_w.CD_MATERIAL);
			intpd_processar_atributo(reg_integracao_w, 'CD_MOEDA', c01_w.CD_MOEDA, 'S', man_equipamento_w.CD_MOEDA);
			intpd_processar_atributo(reg_integracao_w, 'CD_NACIONALIDADE', c01_w.CD_NACIONALIDADE, 'S', man_equipamento_w.CD_NACIONALIDADE);
			intpd_processar_atributo(reg_integracao_w, 'CD_PESSOA_TERCEIRO', c01_w.CD_PESSOA_TERCEIRO, 'S', man_equipamento_w.CD_PESSOA_TERCEIRO);
			intpd_processar_atributo(reg_integracao_w, 'DS_COR_EQUIP', c01_w.DS_COR_EQUIP, 'S', man_equipamento_w.DS_COR_EQUIP);
			intpd_processar_atributo(reg_integracao_w, 'DS_DANO_BREVE', c01_w.DS_DANO_BREVE, 'S', man_equipamento_w.DS_DANO_BREVE);
			intpd_processar_atributo(reg_integracao_w, 'DS_ENDERECO', c01_w.DS_ENDERECO, 'S', man_equipamento_w.DS_ENDERECO);
			intpd_processar_atributo(reg_integracao_w, 'DS_MARCA', c01_w.DS_MARCA, 'S', man_equipamento_w.DS_MARCA);
			intpd_processar_atributo(reg_integracao_w, 'DS_MODELO', c01_w.DS_MODELO, 'S', man_equipamento_w.DS_MODELO);
			intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c01_w.DS_OBSERVACAO, 'S', man_equipamento_w.DS_OBSERVACAO);
			intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO_TENSAO', c01_w.DS_OBSERVACAO_TENSAO, 'S', man_equipamento_w.DS_OBSERVACAO_TENSAO);
			intpd_processar_atributo(reg_integracao_w, 'DS_PADRAO_OS', c01_w.DS_PADRAO_OS, 'S', man_equipamento_w.DS_PADRAO_OS);
			intpd_processar_atributo(reg_integracao_w, 'DT_ANO_FABRICACAO', c01_w.DT_ANO_FABRICACAO, 'S', man_equipamento_w.DT_ANO_FABRICACAO);
			intpd_processar_atributo(reg_integracao_w, 'DT_AQUISICAO', c01_w.DT_AQUISICAO, 'S', man_equipamento_w.DT_AQUISICAO);
			intpd_processar_atributo(reg_integracao_w, 'DT_FIM_GARANTIA', c01_w.DT_FIM_GARANTIA, 'S', man_equipamento_w.DT_FIM_GARANTIA);
			intpd_processar_atributo(reg_integracao_w, 'DT_INICIO_GARANTIA', c01_w.DT_INICIO_GARANTIA, 'S', man_equipamento_w.DT_INICIO_GARANTIA);
			intpd_processar_atributo(reg_integracao_w, 'DT_INSTALACAO', c01_w.DT_INSTALACAO, 'S', man_equipamento_w.DT_INSTALACAO);
			intpd_processar_atributo(reg_integracao_w, 'DT_PROG_PREVENTIVA', c01_w.DT_PROG_PREVENTIVA, 'S', man_equipamento_w.DT_PROG_PREVENTIVA);
			intpd_processar_atributo(reg_integracao_w, 'IE_CALIBRACAO', c01_w.IE_CALIBRACAO, 'S', man_equipamento_w.IE_CALIBRACAO);
			intpd_processar_atributo(reg_integracao_w, 'IE_CLASSIFICACAO_OS', c01_w.IE_CLASSIFICACAO_OS, 'S', man_equipamento_w.IE_CLASSIFICACAO_OS);
			intpd_processar_atributo(reg_integracao_w, 'IE_CONSISTE_OS_DUPLIC', c01_w.IE_CONSISTE_OS_DUPLIC, 'S', man_equipamento_w.IE_CONSISTE_OS_DUPLIC);
			intpd_processar_atributo(reg_integracao_w, 'CD_CGC_TERC', c01_w.CD_CGC_TERC, 'S', man_equipamento_w.CD_CGC_TERC);
			intpd_processar_atributo(reg_integracao_w, 'IE_EXIGE_FUNCAO_OS', c01_w.IE_EXIGE_FUNCAO_OS, 'S', man_equipamento_w.IE_EXIGE_FUNCAO_OS);
			intpd_processar_atributo(reg_integracao_w, 'IE_PRIORIDADE', c01_w.IE_PRIORIDADE, 'S', man_equipamento_w.IE_PRIORIDADE);
			intpd_processar_atributo(reg_integracao_w, 'IE_TENSAO_AC', c01_w.IE_TENSAO_AC, 'S', man_equipamento_w.IE_TENSAO_AC);
			intpd_processar_atributo(reg_integracao_w, 'IE_TENSAO_AC_SAIDA', c01_w.IE_TENSAO_AC_SAIDA, 'S', man_equipamento_w.IE_TENSAO_AC_SAIDA);
			intpd_processar_atributo(reg_integracao_w, 'IE_TENSAO_DC', c01_w.IE_TENSAO_DC, 'S', man_equipamento_w.IE_TENSAO_DC);
			intpd_processar_atributo(reg_integracao_w, 'IE_TENSAO_DC_SAIDA', c01_w.IE_TENSAO_DC_SAIDA, 'S', man_equipamento_w.IE_TENSAO_DC_SAIDA);
			intpd_processar_atributo(reg_integracao_w, 'IE_TIPO_ORDEM', c01_w.IE_TIPO_ORDEM, 'S', man_equipamento_w.IE_TIPO_ORDEM);
			intpd_processar_atributo(reg_integracao_w, 'IE_VOLTAGEM', c01_w.IE_VOLTAGEM, 'S', man_equipamento_w.IE_VOLTAGEM);
			intpd_processar_atributo(reg_integracao_w, 'NR_DOC_GARANTIA', c01_w.NR_DOC_GARANTIA, 'S', man_equipamento_w.NR_DOC_GARANTIA);
			intpd_processar_atributo(reg_integracao_w, 'NR_REGISTRO_ANVISA', c01_w.NR_REGISTRO_ANVISA, 'S', man_equipamento_w.NR_REGISTRO_ANVISA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_BEM', c01_w.NR_SEQ_BEM, 'S', man_equipamento_w.NR_SEQ_BEM);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CATEGORIA', c01_w.NR_SEQ_CATEGORIA, 'S', man_equipamento_w.NR_SEQ_CATEGORIA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CLASSIF_ELET_EQUIP', c01_w.NR_SEQ_CLASSIF_ELET_EQUIP, 'S', man_equipamento_w.NR_SEQ_CLASSIF_ELET_EQUIP);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CLASSIF_RISCO', c01_w.NR_SEQ_CLASSIF_RISCO, 'S', man_equipamento_w.NR_SEQ_CLASSIF_RISCO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CONTADOR', c01_w.NR_SEQ_CONTADOR, 'S', man_equipamento_w.NR_SEQ_CONTADOR);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ESTAGIO_INICIO_OS', c01_w.NR_SEQ_ESTAGIO_INICIO_OS, 'S', man_equipamento_w.NR_SEQ_ESTAGIO_INICIO_OS);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_FABRICANTE', c01_w.NR_SEQ_FABRICANTE, 'S', man_equipamento_w.NR_SEQ_FABRICANTE);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_FAMILIA', c01_w.NR_SEQ_FAMILIA, 'S', man_equipamento_w.NR_SEQ_FAMILIA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ITEM_NF_INTEG', c01_w.NR_SEQ_ITEM_NF_INTEG, 'S', man_equipamento_w.NR_SEQ_ITEM_NF_INTEG);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MARCA', c01_w.NR_SEQ_MARCA, 'S', man_equipamento_w.NR_SEQ_MARCA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MODELO', c01_w.NR_SEQ_MODELO, 'S', man_equipamento_w.NR_SEQ_MODELO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_NF_INTEG', c01_w.NR_SEQ_NF_INTEG, 'S', man_equipamento_w.NR_SEQ_NF_INTEG);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ORIGEM_DANO', c01_w.NR_SEQ_ORIGEM_DANO, 'S', man_equipamento_w.NR_SEQ_ORIGEM_DANO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_STATUS', c01_w.NR_SEQ_STATUS, 'S', man_equipamento_w.NR_SEQ_STATUS);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_SUPERIOR', c01_w.NR_SEQ_SUPERIOR, 'S', man_equipamento_w.NR_SEQ_SUPERIOR);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TIPO_ORDEM', c01_w.NR_SEQ_TIPO_ORDEM, 'S', man_equipamento_w.NR_SEQ_TIPO_ORDEM);
			intpd_processar_atributo(reg_integracao_w, 'NR_SERIE', c01_w.NR_SERIE, 'S', man_equipamento_w.NR_SERIE);
			intpd_processar_atributo(reg_integracao_w, 'QT_ALTURA', c01_w.QT_ALTURA, 'S', man_equipamento_w.QT_ALTURA);
			intpd_processar_atributo(reg_integracao_w, 'QT_CARGA_HORARIA', c01_w.QT_CARGA_HORARIA, 'S', man_equipamento_w.QT_CARGA_HORARIA);
			intpd_processar_atributo(reg_integracao_w, 'QT_COMPRIMENTO', c01_w.QT_COMPRIMENTO, 'S', man_equipamento_w.QT_COMPRIMENTO);
			intpd_processar_atributo(reg_integracao_w, 'QT_CORRENTE_AC', c01_w.QT_CORRENTE_AC, 'S', man_equipamento_w.QT_CORRENTE_AC);
			intpd_processar_atributo(reg_integracao_w, 'QT_CORRENTE_AC_SAIDA', c01_w.QT_CORRENTE_AC_SAIDA, 'S', man_equipamento_w.QT_CORRENTE_AC_SAIDA);
			intpd_processar_atributo(reg_integracao_w, 'QT_CORRENTE_DC', c01_w.QT_CORRENTE_DC, 'S', man_equipamento_w.QT_CORRENTE_DC);
			intpd_processar_atributo(reg_integracao_w, 'QT_CORRENTE_DC_SAIDA', c01_w.QT_CORRENTE_DC_SAIDA, 'S', man_equipamento_w.QT_CORRENTE_DC_SAIDA);
			intpd_processar_atributo(reg_integracao_w, 'QT_LARGURA', c01_w.QT_LARGURA, 'S', man_equipamento_w.QT_LARGURA);
			intpd_processar_atributo(reg_integracao_w, 'QT_PESO', c01_w.QT_PESO, 'S', man_equipamento_w.QT_PESO);
			intpd_processar_atributo(reg_integracao_w, 'QT_POTENCIA_AC', c01_w.QT_POTENCIA_AC, 'S', man_equipamento_w.QT_POTENCIA_AC);
			intpd_processar_atributo(reg_integracao_w, 'QT_POTENCIA_AC_SAIDA', c01_w.QT_POTENCIA_AC_SAIDA, 'S', man_equipamento_w.QT_POTENCIA_AC_SAIDA);
			intpd_processar_atributo(reg_integracao_w, 'QT_POTENCIA_DC', c01_w.QT_POTENCIA_DC, 'S', man_equipamento_w.QT_POTENCIA_DC);
			intpd_processar_atributo(reg_integracao_w, 'QT_POTENCIA_DC_SAIDA', c01_w.QT_POTENCIA_DC_SAIDA, 'S', man_equipamento_w.QT_POTENCIA_DC_SAIDA);
			intpd_processar_atributo(reg_integracao_w, 'QT_TEMPO_VIDA_UTIL', c01_w.QT_TEMPO_VIDA_UTIL, 'S', man_equipamento_w.QT_TEMPO_VIDA_UTIL);
			intpd_processar_atributo(reg_integracao_w, 'VL_AQUISICAO', c01_w.VL_AQUISICAO, 'S', man_equipamento_w.VL_AQUISICAO);
			intpd_processar_atributo(reg_integracao_w, 'VL_CUSTO_SEM_UTILIZACAO', c01_w.VL_CUSTO_SEM_UTILIZACAO, 'S', man_equipamento_w.VL_CUSTO_SEM_UTILIZACAO);
			intpd_processar_atributo(reg_integracao_w, 'CD_CENTRO_CUSTO', c01_w.CD_CENTRO_CUSTO, 'S', man_equipamento_w.CD_CENTRO_CUSTO);
			man_equipamento_w.dt_atualizacao := sysdate;
			man_equipamento_w.dt_atualizacao_nrec := sysdate;
			man_equipamento_w.nm_usuario_nrec := man_equipamento_w.nm_usuario;
			
			
			if	(reg_integracao_w.qt_reg_log = 0) then
				select	nvl(max(nr_sequencia),0)
				into	man_equipamento_w.nr_sequencia
				from	man_equipamento
				where	cd_imobilizado_ext = man_equipamento_w.cd_imobilizado_ext;
				
				if (man_equipamento_w.nr_sequencia > 0) then
					ds_operacao_w	:= 'UPDATE MAN_EQUIPAMENTO';
					update	man_equipamento
					set	row = man_equipamento_w
					where	nr_sequencia = man_equipamento_w.nr_sequencia;
				else
					ds_operacao_w	:= 'INSERT MAN_EQUIPAMENTO';
					select	man_equipamento_seq.nextval
					into	man_equipamento_w.nr_sequencia
					from	dual;
				
					insert into man_equipamento values man_equipamento_w;
					
				end if;
				
				select count(*)
				into qt_reg_equip_hosp_w
				from equipamento 
				where nr_seq_equipamento_man = man_equipamento_w.nr_sequencia;
				
				if (man_equipamento_w.IE_EQUIP_HOSPITALAR = 'S' and qt_reg_equip_hosp_w = 0) then
					ds_operacao_w	:= 'INSERT EQUIPAMENTO (HOSPITALAR)';
					select	nvl(max(nr_sequencia),0)
					into	nr_man_equipamento_hosp_w
					from	man_equipamento
					where	CD_IMOBILIZADO_EXT = man_equipamento_w.CD_IMOBILIZADO_EXT;
				
					man_gerar_equip_hosp(nr_man_equipamento_hosp_w, man_equipamento_w.nm_usuario);
				end if;
				
			end if;
		exception
		when others then
			ds_erro_w := substr(ds_operacao_w || ': ' || sqlerrm(sqlcode),1,2000);
			ie_erro_w := 'S';
			intpd_gravar_log_recebimento(nr_sequencia_p, ds_erro_w, c01_w.nm_usuario);
		end;	
	end if;
		
	end;
end loop;
close C01;

if	((reg_integracao_w.qt_reg_log > 0) or (ie_erro_w = 'S')) then
	begin
	rollback;
 
	update 	intpd_fila_transmissao
	set	ie_status = 'E',
		ie_tipo_erro = 'F'
	where	nr_sequencia = nr_sequencia_p;
 
	for i in 0..reg_integracao_w.qt_reg_log-1 loop		
		INTPD_GRAVAR_LOG_RECEBIMENTO(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;
else
	update	intpd_fila_transmissao
	set	ie_status = 'S',
		nr_seq_documento = nvl(nr_man_equipamento_delete_w, man_equipamento_w.nr_sequencia),
		nr_doc_externo = c01_w.CD_IMOBILIZADO_EXT
	where	nr_sequencia = nr_sequencia_p;	
end if;

commit;

end intpd_recebe_man_equipamento;
/
