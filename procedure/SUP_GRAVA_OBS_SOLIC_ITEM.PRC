create or replace
procedure sup_grava_obs_solic_item(	nr_solic_compra_p	number,
					nr_item_solic_p		number,
					ds_observacao_p		varchar2) is 

begin

update	solic_compra_item
set	ds_observacao = ds_observacao_p
where	nr_solic_compra = nr_solic_compra_p
and	nr_item_solic_compra = nr_item_solic_p;

commit;

end sup_grava_obs_solic_item;
/