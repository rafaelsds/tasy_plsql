create or replace
procedure	cg_liberar_lote_js(
				nr_sequencia_p number
				) is
begin

if	(nr_sequencia_p is not null) then
	begin
	update ehr_sugestao_lote
	set   dt_liberacao = sysdate
	where nr_sequencia = nr_sequencia_p;
	end;
	commit;
end if;
 
end cg_liberar_lote_js;
/