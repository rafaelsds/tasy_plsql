create or replace
procedure Inserir_Proc_Pac_Med_Fatur(	nr_sequencia_p		number,
					nr_seq_atepacu_p	varchar2,
					dt_procedimento_p	Date,
					nm_usuario_p		Varchar2,
					ie_relancamento_p	varchar2) is 

					
nr_atendimento_w		number(10);
dt_faturamento_w		date;
cd_cnpj_exec_w			varchar2(50);
nr_doc_convenio_w		varchar2(20);
ie_funcao_medico_w		varchar2(10);
cd_medico_w			varchar2(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ie_via_acesso_w			varchar2(1);
qt_fatur_w			number(15,2);
dt_inicio_procedimento_w	date;
dt_fim_procedimento_w		date;
cd_convenio_w			number(10);
cd_categoria_w			varchar2(10);
nr_seq_atepacu_w		number(10);
dt_entrada_unidade_w		date;
nr_seq_proc_interno_w		number(10);
cd_setor_atendimento_w		number(10);
nr_sequencia_w			number(10);
CD_PESSOA_FISICA_w		varchar2(10);
qt_reg_w				number(10);
begin
begin
select	a.nr_atendimento,
	NVL(nvl(b.dt_inicio_procedimento,B.DT_PROCEDIMENTO),a.dt_faturamento),
	a.cd_cnpj_exec,
	a.nr_doc_convenio,
	a.ie_funcao_medico,
	a.cd_medico,
	b.cd_procedimento,
	b.ie_origem_proced,
	b.ie_via_acesso,
	b.qt_fatur,
	b.dt_inicio_procedimento,
	b.dt_fim_procedimento,
	b.nr_seq_proc_interno
into	nr_atendimento_w,
	dt_faturamento_w,
	cd_cnpj_exec_w,
	nr_doc_convenio_w,
	ie_funcao_medico_w,
	cd_medico_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_via_acesso_w,
	qt_fatur_w,
	dt_inicio_procedimento_w,
	dt_fim_procedimento_w,
	nr_seq_proc_interno_w
from	pep_med_fatur a,
	pep_med_fatur_proc b
where	a.nr_sequencia	= b.nr_seq_fatur
and	b.nr_sequencia	= nr_sequencia_p;

select	cd_convenio,
	cd_categoria,
	nr_seq_atepacu,
	dt_entrada_unidade,
	cd_setor_atendimento
into	cd_convenio_w,	
	cd_categoria_w,
	nr_seq_atepacu_w,
	dt_entrada_unidade_w,
	cd_setor_atendimento_w
from	atendimento_paciente_v
where	nr_atendimento	= nr_atendimento_w;

exception
	when others then
	cd_procedimento_w	:= null;
end;




if	(nr_seq_atepacu_p	is not null) then
	nr_seq_atepacu_w	:= nr_seq_atepacu_p;
	select	dt_entrada_unidade,
		cd_setor_atendimento
	into	dt_entrada_unidade_w,
		cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno	= nr_seq_atepacu_w;
end if;
select	count(*)
into	qt_reg_w
from	medico
where	cd_pessoa_fisica = cd_medico_w;

if	(qt_reg_w	=0) then
	CD_PESSOA_FISICA_w	:= cd_medico_w;
	cd_medico_w	:= null;
	
end if;

if	(cd_procedimento_w	is not null) and
	(ie_origem_proced_w	is not null) then
	
	select	procedimento_paciente_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into procedimento_paciente	(	nr_sequencia,
							nr_atendimento,
							cd_procedimento,
							ie_origem_proced,
							dt_procedimento,
							qt_procedimento,
							dt_atualizacao,
							nm_usuario,
							cd_medico_executor,
							ie_funcao_medico,
							cd_especialidade,
							cd_convenio,
							cd_categoria,
							cd_setor_atendimento,
							dt_inicio_procedimento,
							dt_final_procedimento,
							cd_cgc_prestador,
							ie_responsavel_credito,
							ie_via_acesso,
							nr_seq_atepacu,
							dt_entrada_unidade,
							nr_seq_proc_interno,
							nr_doc_convenio,
							CD_PESSOA_FISICA)
					values	(	nr_sequencia_w,
							nr_atendimento_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							nvl(dt_procedimento_p,dt_faturamento_w),
							qt_fatur_w,
							sysdate,
							nm_usuario_p,
							cd_medico_w,
							ie_funcao_medico_w,
							obter_especialidade_medico(cd_medico_w,'C'),
							cd_convenio_w,
							cd_categoria_w,
							cd_setor_atendimento_w,
							dt_inicio_procedimento_w,
							dt_fim_procedimento_w,
							cd_cnpj_exec_w,
							null,
							ie_via_acesso_w,
							nr_seq_atepacu_w,
							dt_entrada_unidade_w,
							nr_seq_proc_interno_w,
							nr_doc_convenio_w,
							CD_PESSOA_FISICA_w);
							
	atualiza_preco_procedimento(nr_sequencia_w,cd_convenio_w,nm_usuario_p);
	
	if	(ie_relancamento_p = 'N') then
		update	PEP_MED_FATUR_PROC
		set	DT_ENVIO_CONTA	= sysdate
		where	nr_sequencia = nr_sequencia_p;
	end if;
	
end if;
	


commit;

end Inserir_Proc_Pac_Med_Fatur;
/