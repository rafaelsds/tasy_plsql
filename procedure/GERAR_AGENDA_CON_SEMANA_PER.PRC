create or replace 
procedure gerar_agenda_con_semana_per	(
		nr_seq_agenda_p		number,
		dt_agenda_p		date,
		nm_usuario_p		varchar2,
		ie_diario_p		varchar2,
		ie_final_semana_p	varchar2,
		qt_intervalo_p		number,
		ds_dias_p		varchar2,
		ie_copiar_proced_p		varchar2,
		ie_copiar_proced_adic_p		varchar2,
		ds_erro_p out		varchar2,
		cd_estabelecimento_p	number,
		qt_total_secao_p		number,
		nr_secao_atual_p		number,
		ie_recalculando_p	varchar2 default 'N',
		ds_insert_p out varchar2,
		ie_next_day_p varchar2 default 'N') is
		
cd_agenda_w		number(10);
dt_agenda_w		date;
nr_minuto_duracao_w	number(10);
ie_status_agenda_w	varchar2(2);
ie_classif_agenda_w	varchar2(5);
dt_atualizacao_w		date;
nm_usuario_w		varchar2(15);
cd_convenio_w		number(5);
cd_pessoa_fisica_w  varchar2(10);
nm_pessoa_contato_w           varchar2(50);
ds_observacao_w    varchar2(2000);
ie_status_paciente_w  varchar2(3);
nr_seq_consulta_w    number(3);
nm_paciente_w    varchar2(80);
nr_atendimento_w                  number(10);
dt_confirmacao_w                  date;
ds_confirmacao_w                  varchar2(80);
nr_telefone_w                     varchar2(80);
qt_idade_pac_w                    number(3);
nr_seq_plano_w                    number(10);
nr_seq_classif_med_w            number(10);
nm_usuario_origem_w             varchar2(15);
ie_necessita_contato_w          varchar2(1);
nr_seq_sala_w                     number(10);
cd_categoria_w                    varchar2(10);
cd_tipo_acomodacao_w         number(4);
cd_usuario_convenio_w         varchar2(30);
cd_complemento_w                varchar2(30);
dt_validade_carteira_w            date;
nr_doc_convenio_w               varchar2(20);
cd_senha_w                        varchar2(20);
nr_seq_agepaci_w                  number(10);
ds_senha_w                        varchar2(10);
dt_nascimento_pac_w            date;
cd_turno_w                        varchar2(1);
dt_agendamento_w                date;
cd_medico_w                       varchar2(10);
nr_seq_hora_w                     number(10);
nr_seq_pq_proc_w                  number(10);
cd_motivo_cancelamento_w  agenda_consulta.cd_motivo_cancelamento%type;
cd_procedimento_w                 number(15);
cd_procedimento_ww                number(15);
nr_seq_proc_interno_w             number(10);
nr_seq_proc_interno_ww            number(10);
qt_total_secao_w                  number(3);
nr_secao_w                        number(3);
ie_origem_proced_w                number(10);
ie_origem_proced_ww               number(10);
dt_aguardando_w                   date;
dt_consulta_w                     date;
dt_atendido_w                     date;
cd_medico_solic_w                 varchar2(10);
nr_seq_indicacao_w                number(10);
cd_pessoa_indicacao_w         varchar2(10);
cd_setor_atendimento_w         number(10);
dt_provavel_term_w                date;
ie_encaixe_w                  varchar2(1);
ie_classif_bloqueio_w  varchar2(5);
qt_agenda_bloq_w    number(3,0);
qt_classif_agenda_w  number(10,0);
nr_controle_secao_w  number(10,0);
nr_seq_unidade_w    number(10,0);
cd_procedencia_w    number(5,0);
ie_tipo_atendimento_w  number(3,0);
qt_sessao_controle_w  number(3) := 0;

dt_termino_w      date;
dt_atual_w      date;
dt_dia_semana_w      number(1);
ie_feriado_w      varchar2(  1);
nr_seq_esp_w      number(10);
ie_hor_adic_w      varchar2(1);
ie_valido_w      varchar2(1);
ie_bloqueio_w      varchar2(1);
qt_horario_w      number(10);
qt_horario_livre_w      number(10);
qt_horarios_ocup_med_exec_w  number(10);
nr_sequencia_w      number(10);
ds_erro_w      varchar2(1000);
ds_erro_ww      varchar2(1000);
qt_dia_w        number(3);

nr_secao_periodo_w    number(3);
ie_consiste_w      varchar2(1);

cd_dia_semana_w      varchar2(1);
ds_dias_w      varchar2(255);
nr_seq_agenda_sessao_w    number(10,0);
ie_fim_sab_chec_w      varchar2(1);
ie_fim_dom_chec_w    varchar2(1);
ie_gerar_dia_w      varchar2(1);
cd_estabelecimento_w    number(4);

ie_agenda_feriado_w    varchar2(1);
ie_consiste_medico_turno_w  Varchar2(1)  := 'N';
ie_consiste_classificacao_w  varchar2(1);
ie_copiar_prof_w    varchar2(1)  := 'N';
ie_copiar_atendimento_w    varchar2(1)  := 'N';
ie_copiar_dt_confirm_w    varchar2(1)  := 'S';
qt_total_secao_ww      number(10);
nr_secao_atual_ww      number(10);
cd_especialidade_w    number(10,0);
ie_forma_agendamento_w    number(3,0);
cd_cid_w        varchar2(10);
ie_carater_inter_sus_w    varchar2(2);
qt_procedimento_w      number(5,0);
ie_manter_classif_hor_origem_w  varchar2(1)  := 'S';
ie_classif_agend_origem_w    varchar2(5);
ie_cons_hor_disp_prof_exec_w  varchar2(1);
ie_gerar_nr_guia_seq_agenda_w  varchar2(1);

ie_copiar_senha_w      varchar2(1) := 'S';
ie_copiar_autorizacao_w    varchar2(1) := 'S';
ie_autorizacao_w      varchar2(3);
ie_gerar_solic_pront_w    varchar2(1) := 'N';

nr_seq_hor_livre_w      number(10);
ie_classif_agenda_origem_w    varchar2(5);
qt_bloqueio_w      number(10,0);

ie_cons_hor_medico_w      varchar2(1);
nr_seq_classif_w  agenda_consulta.nr_seq_classif%type;
nr_seq_turno_w    agenda_consulta.nr_seq_turno%type;
nr_seq_proc_interno_adic_w   number(10);
nr_seq_proc_adic_w     number(10);
ie_gerar_autorizacao_w    varchar2(1);
ie_insert_w    varchar2(1);
nr_seq_regra_bloq_w		agenda_bloqueio_geral.nr_sequencia%type;
ie_classif_regra_w		agenda_consulta.ie_classif_agenda%type;

CURSOR c01 IS

SELECT   a.nr_sequencia ,
    a.nr_seq_proc_interno
FROM   agenda_consulta_proc a
WHERE   a.nr_seq_agenda = nr_sequencia_w
ORDER BY 1;

begin

ds_erro_w := '';
ds_dias_w := '';
ds_insert_p := 'N'; 

if  (qt_total_secao_p <> 0)then
	qt_total_secao_ww := qt_total_secao_p;
	if  (nr_secao_atual_p <> 0) then
		nr_secao_atual_ww := nr_secao_atual_p;
	end if;
end if;

if  (ie_recalculando_p = 'S') and (nr_secao_atual_ww > qt_total_secao_ww) then
  nr_secao_atual_ww  := qt_total_secao_ww;
end if;

if  (nr_seq_agenda_p is not null) and
  (dt_agenda_p is not null) and
  (nm_usuario_p is not null) then
  
  /* INICIO - PARAMETROS PARA O TRATAMENTO DA COPIA DE ATRIBUTOS*/
  ie_copiar_prof_w    := obter_valor_param_usuario(866, 146, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_copiar_atendimento_w    := obter_valor_param_usuario(866, 148, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_copiar_dt_confirm_w    := obter_valor_param_usuario(866, 156, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_manter_classif_hor_origem_w  := obter_valor_param_usuario(866, 217, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_cons_hor_disp_prof_exec_w  := obter_valor_param_usuario(866, 231, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_gerar_nr_guia_seq_agenda_w  := obter_valor_param_usuario(866, 89, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_copiar_senha_w    := obter_valor_param_usuario(866, 256, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_copiar_autorizacao_w         := obter_valor_param_usuario(866, 257, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  /* FIM - PARAMETROS PARA O TRATAMENTO DA COPIA DE ATRIBUTOS*/
  ie_cons_hor_medico_w := obter_valor_param_usuario(866, 281, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
  ie_gerar_autorizacao_w := obter_valor_param_usuario(866, 302, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);

  /* obter dados agenda */
  select  nr_sequencia,
    cd_agenda,
    dt_agenda,
    nr_minuto_duracao,
    ie_status_agenda,
    ie_classif_agenda,
    dt_atualizacao,
    nm_usuario,
    cd_convenio,
    cd_pessoa_fisica,
    nm_pessoa_contato,
    ds_observacao,
    ie_status_paciente,
    nr_seq_consulta,
    nm_paciente,
    nr_atendimento,
    dt_confirmacao,
    ds_confirmacao,
    nr_telefone,
    qt_idade_pac,
    nr_seq_plano,
    nr_seq_classif_med,
    nm_usuario_origem,
    ie_necessita_contato,
    nr_seq_sala,
    cd_categoria,
    cd_tipo_acomodacao,
    cd_usuario_convenio,
    cd_complemento,
    dt_validade_carteira,
    nr_doc_convenio,
    cd_senha,
    nr_seq_agepaci,
    ds_senha,
    dt_nascimento_pac,
    cd_turno,
    dt_agendamento,
    cd_medico,
    nr_seq_hora,
    nr_seq_pq_proc,
    cd_motivo_cancelamento,
    cd_procedimento,
    nr_seq_proc_interno,
    qt_total_secao,
    nr_secao,
    ie_origem_proced,
    dt_aguardando,
    dt_consulta,
    dt_atendido,
    cd_medico_solic,
    nr_seq_indicacao,
    cd_pessoa_indicacao,
    cd_setor_atendimento,
    nvl(dt_provavel_term,dt_agenda_p),
    ie_encaixe,
    decode(nr_seq_agenda_sessao, null, nr_sequencia, nr_seq_agenda_sessao),
    nr_controle_secao,
    nr_seq_unidade,
    cd_especialidade,
    ie_forma_agendamento,
    cd_cid,
    ie_carater_inter_sus,
    qt_procedimento,
    cd_procedencia,
    ie_tipo_atendimento,
    ie_autorizacao,
    ie_classif_agenda_origem,
    nr_seq_classif,
    nr_seq_turno
  into  nr_sequencia_w,
    cd_agenda_w,
    dt_agenda_w,
    nr_minuto_duracao_w,
    ie_status_agenda_w,
    ie_classif_agenda_w,
    dt_atualizacao_w,
    nm_usuario_w,
    cd_convenio_w,
    cd_pessoa_fisica_w,
    nm_pessoa_contato_w,
    ds_observacao_w,
    ie_status_paciente_w,
    nr_seq_consulta_w,
    nm_paciente_w,
    nr_atendimento_w,
    dt_confirmacao_w,
    ds_confirmacao_w,
    nr_telefone_w,
    qt_idade_pac_w,
    nr_seq_plano_w,
    nr_seq_classif_med_w,
    nm_usuario_origem_w,
    ie_necessita_contato_w,
    nr_seq_sala_w,
    cd_categoria_w,
    cd_tipo_acomodacao_w,
    cd_usuario_convenio_w,
    cd_complemento_w,
    dt_validade_carteira_w,
    nr_doc_convenio_w,
    cd_senha_w,
    nr_seq_agepaci_w,
    ds_senha_w,
    dt_nascimento_pac_w,
    cd_turno_w,
    dt_agendamento_w,
    cd_medico_w,
    nr_seq_hora_w,
    nr_seq_pq_proc_w,
    cd_motivo_cancelamento_w,
    cd_procedimento_w,
    nr_seq_proc_interno_w,
    qt_total_secao_w,
    nr_secao_w,
    ie_origem_proced_w,
    dt_aguardando_w,
    dt_consulta_w,
    dt_atendido_w,
    cd_medico_solic_w,
    nr_seq_indicacao_w,
    cd_pessoa_indicacao_w,
    cd_setor_atendimento_w,
    dt_provavel_term_w,
    ie_encaixe_w,
    nr_seq_agenda_sessao_w,
    nr_controle_secao_w,
    nr_seq_unidade_w,
    cd_especialidade_w,
    ie_forma_agendamento_w,
    cd_cid_w,
    ie_carater_inter_sus_w,
    qt_procedimento_w,
    cd_procedencia_w,
    ie_tipo_atendimento_w,
    ie_autorizacao_w,
    ie_classif_agenda_origem_w,
    nr_seq_classif_w,
    nr_seq_turno_w
  from  agenda_consulta
  where  nr_sequencia = nr_seq_agenda_p;
  
  update  agenda_consulta
  set  dt_provavel_term = dt_provavel_term_w
  where  nr_sequencia = nr_seq_agenda_p;
  
  if  (cd_agenda_w is not null)then    
    select  nvl(max(ie_gerar_solic_pront), 'S')    
    into  ie_gerar_solic_pront_w    
    from  agenda
    where   cd_agenda  = cd_agenda_w;        
  end if;  

  /* Gerar horarios por semana ou diariamente */
  if  (ie_diario_p = 'S') then
    qt_dia_w:= 1;
  elsif  (ie_diario_p = 'N') and (qt_intervalo_p > 0) then
    qt_dia_w:= qt_intervalo_p;
  elsif  (ie_diario_p = 'N') then
    qt_dia_w:= 7;
  end if;

  /* obter datas */
  dt_termino_w  := ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_agenda_p, nvl(dt_agenda_w, trunc(sysdate)));
  dt_atual_w    := dt_agenda_w + qt_dia_w;
  
  /* validar data */
  if  (dt_agenda_p < dt_agenda_w) then
    Wheb_mensagem_pck.exibir_mensagem_abort(251128);
    
  end if;

  /* Consistir o total de sessoes do agendamento com o total de sessoes no periodo informado */
  nr_secao_periodo_w  := obter_qt_secao_per_agenda(nr_seq_agenda_p,ie_diario_p, ie_final_semana_p, qt_intervalo_p, dt_termino_w, ds_dias_p);
  
  select  nvl(max(Obter_Valor_Param_Usuario(866, 28, obter_perfil_ativo, nm_usuario_p, obter_estab_agenda(cd_agenda_w))), 'N')
  into  ie_consiste_w
  from  dual;
  
  ie_consiste_medico_turno_w   := nvl(Obter_Valor_Param_Usuario(866, 60, obter_perfil_ativo, nm_usuario_p, obter_estab_agenda(cd_agenda_w)), 'N');
  ie_consiste_classificacao_w  := nvl(Obter_Valor_Param_Usuario(866, 112, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S');
  
  /* Se o parametro [28] for diferente de nao consistir */
  if  (ie_recalculando_p <> 'S') and (ie_consiste_w <> 'N') then
    begin
    if  (ie_consiste_w = 'C') and
      (qt_total_secao_w < nr_secao_periodo_w) then
      ds_erro_p := substr(WHEB_MENSAGEM_PCK.get_texto(279326,'NR_SECAO_PERIODO='||nr_secao_periodo_w||';QT_TOTAL_SECAO='||qt_total_secao_w),1,255);
    elsif  (ie_consiste_w = 'B') and
      (qt_total_secao_w < nr_secao_periodo_w) then
      Wheb_mensagem_pck.exibir_mensagem_abort(251130,'NR_SECAO_PERIODO=' || nr_secao_periodo_w || ';QT_TOTAL_SECAO=' ||qt_total_secao_w);

    end if;
    end;  
  end if;
  /* obter dias semana */
  while  (dt_atual_w <= dt_termino_w) loop
    begin
    /* obter dia semana */
    ie_insert_w := 'N';
    select  pkg_date_utils.get_weekday(dt_atual_w)
    into  dt_dia_semana_w
    from  dual;

    ie_gerar_dia_w  := 'S';
    
    if  (ds_dias_p is not null) then
      ds_dias_w := TRIM (ds_dias_p);
      ie_gerar_dia_w  :=  obter_se_contido(dt_dia_semana_w,ds_dias_w);
      
    end if;  
          
    select  obter_se_contido(7,ds_dias_w)
    into  ie_fim_sab_chec_w
    from  dual;
    
    select  obter_se_contido(1,ds_dias_w)
    into  ie_fim_dom_chec_w
    from  dual;

    /* obter se feriado */    
    select  decode(count(*),0,'N','S')
    into  ie_feriado_w
    from   feriado a, 
      agenda b
    where   a.cd_estabelecimento = obter_estab_agenda(cd_agenda_w)
    and  (  dt_atual_w  BETWEEN 
            TIMEZONE_UTILS.startOfDay(a.dt_feriado, TIMEZONE_UTILS.getBaseLineTimeZone()) 
            and TIMEZONE_UTILS.endOfDay(a.dt_feriado, TIMEZONE_UTILS.getBaseLineTimeZone())
         ) 
    and   b.ie_feriado = 'N' 
    and   b.cd_agenda = cd_agenda_w;

    /* obter se horario especial */
    select  nvl(max(nr_sequencia),0),
      nvl(max(ie_horario_adicional),'N')
    into  nr_seq_esp_w,
      ie_hor_adic_w
    from  agenda_turno_esp
    where  cd_agenda = cd_agenda_w
    and  (  dt_agenda_w  BETWEEN 
            TIMEZONE_UTILS.startOfDay(dt_agenda, TIMEZONE_UTILS.getBaseLineTimeZone()) 
            and TIMEZONE_UTILS.endOfDay(dt_agenda, TIMEZONE_UTILS.getBaseLineTimeZone())
         );
    
    cd_estabelecimento_w := obter_estab_agenda(cd_agenda_w);  
    Gerar_Horario_Agenda_Servico(cd_estabelecimento_w, cd_agenda_w, dt_atual_w, nm_usuario_p);
    
    dt_dia_semana_w  := pkg_date_utils.get_weekday(dt_atual_w);
    
    /* validar horario x bloqueio */
    select  decode(count(*),0,'N','S'),
      sum(qt_agenda_bloq),
      max(ie_classif_bloqueio)
    into  ie_bloqueio_w,
      qt_agenda_bloq_w,
      ie_classif_bloqueio_w
    from  agenda_bloqueio
    where  cd_agenda = cd_agenda_w
    and  dt_final >=  ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)
    and  dt_inicial <=  ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)
    and  to_char(hr_final_bloqueio,'hh24:mi') >= to_char(dt_atual_w,'hh24:mi')
    and  to_char(hr_inicio_bloqueio,'hh24:mi') <= to_char(dt_atual_w,'hh24:mi')
    and  ((ie_dia_semana is null) or (ie_dia_semana = dt_dia_semana_w));
    
    select  count(*)
    into  qt_classif_agenda_w
    from  agenda_consulta
    where  cd_agenda = cd_agenda_w
    and  dt_agenda = dt_atual_w
    and  ie_classif_agenda = ie_classif_bloqueio_w;
    
    
    if  (ie_consiste_medico_turno_w  = 'N') then
      /* validar horario x cadastro */
      select  decode(count(*),0,'N','S')
      into  ie_valido_w
      from  agenda_turno
      where  cd_agenda = cd_agenda_w
      and    ((ie_dia_semana = dt_dia_semana_w) or (ie_dia_semana = 9 and dt_dia_semana_w not in (1,7)))
      and    ((nr_seq_esp_w = 0) or (ie_hor_adic_w = 'S'))
      and    ((dt_final_vigencia is null) or (dt_final_vigencia >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)))
      and    ((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)))
      and    To_Date(To_Char(SYSDATE,'dd/mm/yyyy')||' '||To_Char(hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
           < To_Date(To_Char(SYSDATE,'dd/mm/yyyy')||' '||To_Char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      and    to_char(hr_inicial,'hh24:mi') = to_char(dt_atual_w,'hh24:mi')
      and    ((nvl(nr_minuto_intervalo,0) > 0) or (obter_tipo_agenda(cd_agenda) = 5))
      and    ie_feriado_w <> 'S'
      and    ((dt_dia_semana_w not in (1,7)) or (ie_final_semana_p = 'S') or (ie_fim_sab_chec_w = 'S' and dt_dia_semana_w = 7) or (ie_fim_dom_chec_w = 'S' and dt_dia_semana_w = 1))
      and    ((ds_dias_w is null) or
          ((ds_dias_w is not null) and (obter_se_contido(dt_dia_semana_w,ds_dias_w) = 'S')));
    else
      select  decode(count(*),0,'N','S')
      into  ie_valido_w
      from  agenda_turno b,
          agenda_turno_classif a
      where  b.cd_agenda = cd_agenda_w
      and    b.nr_sequencia         = a.nr_seq_turno
      and    ((nvl(a.cd_medico, cd_medico_w)    = cd_medico_w) or (cd_medico_w is null))
      and    ((nvl(a.cd_medico_solic, cd_medico_solic_w)  = cd_medico_solic_w) or (cd_medico_solic_w is null))
      and    ((b.ie_dia_semana = dt_dia_semana_w) or (b.ie_dia_semana = 9 and dt_dia_semana_w not in (1,7)))
      and    ((nr_seq_esp_w = 0) or (ie_hor_adic_w = 'S'))
      and    ((b.dt_final_vigencia is null) or (b.dt_final_vigencia >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)))
      and    ((b.dt_inicio_vigencia is null) or (b.dt_inicio_vigencia <= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)))
      and    To_Date(To_Char(SYSDATE,'dd/mm/yyyy')||' '||To_Char(b.hr_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
           < To_Date(To_Char(SYSDATE,'dd/mm/yyyy')||' '||To_Char(b.hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      and    to_char(b.hr_inicial,'hh24:mi') = to_char(dt_atual_w,'hh24:mi')
      and    ((nvl(b.nr_minuto_intervalo,0) > 0) or (obter_tipo_agenda(b.cd_agenda) = 5))
      and    ie_feriado_w <> 'S'
      and    ((dt_dia_semana_w not in (1,7)) or (ie_final_semana_p = 'S') or (ie_fim_sab_chec_w = 'S' and dt_dia_semana_w = 7) or (ie_fim_dom_chec_w = 'S' and dt_dia_semana_w = 1))
      and    ((ds_dias_w is null) or
          ((ds_dias_w is not null) and (obter_se_contido(dt_dia_semana_w,ds_dias_w) = 'S')));
    end if;
    
    if  (ie_bloqueio_w = 'S') and (qt_agenda_bloq_w >= qt_classif_agenda_w) then
      ie_valido_w := 'N';
    end if;
    
    dt_dia_semana_w  := pkg_date_utils.get_weekday(dt_atual_w);
    
    select  count(*)
    into  qt_bloqueio_w
    from  agenda_bloqueio
    where  cd_agenda = cd_agenda_w
    and  dt_final >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)
    and  dt_inicial <= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_atual_w)    
    and  to_char(nvl(hr_final_bloqueio,dt_atual_w),'hh24:mi') >= to_char(dt_atual_w,'hh24:mi')
    and  to_char(nvl(hr_inicio_bloqueio,dt_atual_w),'hh24:mi') <= to_char(dt_atual_w,'hh24:mi')
    and  ie_classif_bloqueio is null
    and  cd_medico is null
    and  ((ie_dia_semana  = dt_dia_semana_w) OR ((ie_dia_semana = 9) and dt_dia_semana_w not in(1,7)) OR (ie_dia_semana IS NULL));
    
    if  (qt_bloqueio_w > 0) then
      ie_valido_w := 'N';
    end if;    

    /* validar horario gerado */
    select  count(*)
    into  qt_horario_w
    from  agenda_consulta
    where  cd_agenda = cd_agenda_w
    and  dt_agenda = dt_atual_w;
    
    /* validar horario livre */
    
    select   nvl(max(ie_feriado),'N')
    into  ie_agenda_feriado_w
    from  agenda
    where  cd_agenda = cd_agenda_w;
    
    if (ie_consiste_medico_turno_w  = 'H') then
      select  count(*)
      into  qt_horario_livre_w
      from  agenda_consulta
      where  cd_agenda = cd_agenda_w      
      and    dt_agenda = dt_atual_w
      and    ie_status_agenda = 'L'
      and    ((cd_medico = cd_medico_w) or (cd_medico_w is null))
      and    ((ie_classif_agenda  = ie_classif_agenda_w) or (ie_consiste_classificacao_w = 'N'))
      and    ((ie_agenda_feriado_w = 'S') or (obter_se_feriado(obter_estab_agenda(cd_agenda_w), dt_atual_w) = 0));      
    else
      select  count(*)
      into  qt_horario_livre_w
      from  agenda_consulta
      where  cd_agenda = cd_agenda_w
      and    dt_agenda = dt_atual_w
      and    ie_status_agenda = 'L'
      and    ((ie_classif_agenda  = ie_classif_agenda_w) or (ie_consiste_classificacao_w = 'N'))
      and    ((ie_agenda_feriado_w = 'S') or (obter_se_feriado(obter_estab_agenda(cd_agenda_w), dt_atual_w) = 0));
    end if;

    if  (ie_gerar_dia_w = 'S') then

      if  (nvl(qt_horario_livre_w,0) > 0) then
        if (ie_consiste_medico_turno_w = 'H') then
          select  max(nr_sequencia)
          into  nr_seq_hor_livre_w
          from  agenda_consulta
          where  cd_agenda = cd_agenda_w
          and    dt_agenda = dt_atual_w
          and    ie_status_agenda = 'L'
          and    ((ie_classif_agenda = ie_classif_agenda_w) or (ie_consiste_classificacao_w = 'N'))
          and    ((ie_agenda_feriado_w = 'S') or (obter_se_feriado(obter_estab_agenda(cd_agenda_w), dt_atual_w) = 0))
          and    dt_agenda between dt_atual_w and fim_dia(dt_atual_w)
          and    to_char(dt_agenda,'hh24:mi') = to_char(dt_atual_w,'hh24:mi')
          and    ((cd_medico = cd_medico_w) or (cd_medico_w is null));
        else
          select  max(nr_sequencia)
          into  nr_seq_hor_livre_w
          from  agenda_consulta
          where  cd_agenda = cd_agenda_w
          and    dt_agenda = dt_atual_w
          and    ie_status_agenda = 'L'
          and    ((ie_classif_agenda = ie_classif_agenda_w) or (ie_consiste_classificacao_w = 'N'))
          and    ((ie_agenda_feriado_w = 'S') or (obter_se_feriado(obter_estab_agenda(cd_agenda_w), dt_atual_w) = 0))
          and    dt_agenda between dt_atual_w and fim_dia(dt_atual_w)
          and    to_char(dt_agenda,'hh24:mi') = to_char(dt_atual_w,'hh24:mi');
        end if;
        
        if  (nr_seq_hor_livre_w is not null) then
          delete  agenda_consulta
          where  nr_sequencia = nr_seq_hor_livre_w;
        end if;
      end if;        
      
      if  ((ie_valido_w = 'S') and (qt_horario_w = 0)) or
        ((ie_valido_w = 'S') and (qt_horario_livre_w > 0)) then

        /* validar disponibilidade de horarios do medico executor */
        if  (ie_cons_hor_disp_prof_exec_w = 'S')then
          begin      
          
          select  nvl(count(*), 0)    
          into  qt_horarios_ocup_med_exec_w
          from  agenda_consulta
          where  cd_agenda     = cd_agenda_w
          and  dt_agenda     between dt_atual_w and fim_dia(dt_atual_w)
          and  to_char(dt_agenda,'hh24:mi')  = to_char(dt_atual_w,'hh24:mi')
          and  ie_status_agenda     not in ('L', 'E', 'C', 'B', 'F', 'I')
          and  cd_medico    = cd_medico_w;
          
          if  (qt_horarios_ocup_med_exec_w > 0)then
            begin
            ie_gerar_dia_w  := 'N';
            ds_erro_w := substr(ds_erro_w || PKG_DATE_FORMATERS.TO_VARCHAR(dt_atual_w, 'shortDayMonth', cd_estabelecimento_p, nm_usuario_p) || ', ',1,1000);
            end;      
          end if;
          
          end;
        end if;

        /* Consistir de medico executor possui agendamento no mesmo horario em outras agendas */
        if (nvl(ie_cons_hor_medico_w,'N') <> 'N') then
          consistir_horario_med_agenda(nr_seq_agenda_p, cd_medico_w, nr_minuto_duracao_w, 5, ds_erro_ww, dt_atual_w);
          if (ds_erro_ww is not null) then
            ie_gerar_dia_w  := 'N';
            ds_erro_w := substr(ds_erro_w || PKG_DATE_FORMATERS.TO_VARCHAR(dt_atual_w, 'shortDayMonth', cd_estabelecimento_p, nm_usuario_p) || ', ',1,1000);
            ds_erro_ww := null;
          end if;
        end if;  

		if (ie_copiar_proced_p = 'S') then
			nr_seq_proc_interno_ww := nr_seq_proc_interno_w;
			cd_procedimento_ww := cd_procedimento_w;
			ie_origem_proced_ww := ie_origem_proced_w;
		end if;

		if  (ie_manter_classif_hor_origem_w = 'N')then
			select  nvl(max(ie_classif_agenda), ie_classif_agenda_w)
			into  ie_classif_agend_origem_w
			from  agenda_consulta
			where  cd_agenda = cd_agenda_w
			and  dt_agenda = dt_atual_w;
			end if;
		
		if (ie_manter_classif_hor_origem_w = 'S') then
			ie_classif_regra_w := ie_classif_agenda_w;
		else
			ie_classif_regra_w := ie_classif_agend_origem_w;
		end if;

		nr_seq_regra_bloq_w := obter_se_bloq_geral_agenda (cd_estabelecimento_p,
								cd_agenda_w,
								ie_classif_regra_w, 
								null,
								cd_especialidade_w,
								cd_setor_atendimento_w,
								nr_seq_proc_interno_ww,
								cd_procedimento_ww,
								ie_origem_proced_ww,
								cd_medico_w,
								dt_atual_w,
								'N',
								'N');
		if (nr_seq_regra_bloq_w > 0) then
			ie_gerar_dia_w  := 'N';
			ds_erro_w := substr(ds_erro_w || PKG_DATE_FORMATERS.TO_VARCHAR(dt_atual_w, 'shortDayMonth', cd_estabelecimento_p, nm_usuario_p) || ', ',1,1000);
			goto pulardia;
		end if;	
        
        /* obter sequencia */
        select  agenda_consulta_seq.nextval
        into  nr_sequencia_w
        from  dual;

        /* obter sequencia hora */
        select  nvl(max(nr_seq_hora),0)+1
        into  nr_seq_hora_w
        from  agenda_consulta
        where  cd_agenda = cd_agenda_w
        and  dt_agenda = dt_atual_w; 
      
        /* atualizar sessoes */
        if  (qt_total_secao_p <> 0) and
          (nr_secao_atual_p <> 0) then
          nr_secao_w:= nvl(nr_secao_atual_ww,0);
          nr_secao_atual_ww := nr_secao_atual_ww + 1;
        elsif  (nvl(qt_total_secao_w,0) > 0) then
          nr_secao_w:= nvl(nr_secao_w,0) + 1;
        end if;  

        wheb_usuario_pck.set_ie_executar_trigger('N');
        
	ds_insert_p := 'S';
	ie_insert_w := 'S';
		
        /*tbyegmann OS 306310 - consisitr a copia do numero de atendimento de acordo com o valor do parametro 148*/
        insert into agenda_consulta(
            nr_sequencia,
            cd_agenda,
            dt_agenda,
            nr_minuto_duracao,
            ie_status_agenda,
            ie_classif_agenda,
            dt_atualizacao,
            nm_usuario,
            cd_convenio,
            cd_pessoa_fisica,
            nm_pessoa_contato,
            ds_observacao,
            ie_status_paciente,
            nr_seq_consulta,
            nm_paciente,
            nr_atendimento,
            dt_confirmacao,
            ds_confirmacao,
            nr_telefone,
            qt_idade_pac,
            nr_seq_plano,
            nr_seq_classif_med,
            nm_usuario_origem,
            ie_necessita_contato,
            nr_seq_sala,
            cd_categoria,
            cd_tipo_acomodacao,
            cd_usuario_convenio,
            cd_complemento,
            dt_validade_carteira,
            nr_doc_convenio,
            cd_senha,
            nr_seq_agepaci,
            ds_senha,
            dt_nascimento_pac,
            cd_turno,
            dt_agendamento,
            cd_medico,
            nr_seq_hora,
            nr_seq_pq_proc,
            cd_motivo_cancelamento,
            cd_procedimento,
            nr_seq_proc_interno,
            qt_total_secao,
            nr_secao,
            ie_origem_proced,
            cd_medico_solic,
            nr_seq_indicacao,
            cd_pessoa_indicacao,
            cd_setor_atendimento,
            dt_provavel_term,
            ie_encaixe,
            nr_seq_agenda_sessao,  
            nr_controle_secao,
            nr_seq_unidade,
            cd_especialidade,
            ie_forma_agendamento,
            cd_cid,
            ie_carater_inter_sus,
            qt_procedimento,
            cd_procedencia,
            ie_tipo_atendimento,
            ie_autorizacao,
            ie_classif_agenda_origem,
            nr_seq_classif,
            nr_seq_turno,
            ie_sessao_diariamente,
            ie_sessao_final_semana,
            qt_sessao_intervalo,
            ds_sessao_dias_semana,
            ie_sessao_copiar_proced,
            ie_sessao_copiar_proced_adic
            )
          values  (
            nr_sequencia_w,
            cd_agenda_w,
            dt_atual_w,
            nr_minuto_duracao_w,
            'N',
            decode(ie_manter_classif_hor_origem_w, 'S', ie_classif_agenda_w, ie_classif_agend_origem_w),
            sysdate,
            nm_usuario_p,
            cd_convenio_w,
            cd_pessoa_fisica_w,
            nm_pessoa_contato_w,
            ds_observacao_w,
            ie_status_paciente_w,
            nr_seq_consulta_w,
            nm_paciente_w,
            decode(ie_copiar_atendimento_w, 'S', nr_atendimento_w, null),
            decode(ie_copiar_dt_confirm_w,'S',sysdate,'N',null),
            decode(ie_copiar_dt_confirm_w,'S',ds_confirmacao_w,'N',null),
            nr_telefone_w,
            qt_idade_pac_w,
            nr_seq_plano_w,
            nr_seq_classif_med_w,
            nm_usuario_p,
            ie_necessita_contato_w,
            nr_seq_sala_w,
            cd_categoria_w,
            cd_tipo_acomodacao_w,
            cd_usuario_convenio_w,
            cd_complemento_w,
            dt_validade_carteira_w,
            decode(ie_gerar_nr_guia_seq_agenda_w, 'S', nr_sequencia_w, nr_doc_convenio_w),
            decode(ie_copiar_senha_w, 'S' , cd_senha_w, null),
            nr_seq_agepaci_w,
            ds_senha_w,
            dt_nascimento_pac_w,
            cd_turno_w,
            sysdate,
            cd_medico_w,
            nr_seq_hora_w,
            nr_seq_pq_proc_w,
            cd_motivo_cancelamento_w,
            decode(ie_copiar_proced_p, 'S', cd_procedimento_w, null),
            decode(ie_copiar_proced_p, 'S', nr_seq_proc_interno_w, null),
            decode(qt_total_secao_p, 0, qt_total_secao_w, qt_total_secao_ww),
            nr_secao_w,
            decode(ie_copiar_proced_p, 'S', ie_origem_proced_w, null),
            cd_medico_solic_w,
            nr_seq_indicacao_w,
            cd_pessoa_indicacao_w,
            cd_setor_atendimento_w,
            dt_provavel_term_w,
            ie_encaixe_w,
            nr_seq_agenda_sessao_w,  
            nr_controle_secao_w,
            nr_seq_unidade_w,
            cd_especialidade_w,
            ie_forma_agendamento_w,
            cd_cid_w,
            ie_carater_inter_sus_w,
            qt_procedimento_w,
            cd_procedencia_w,
            ie_tipo_atendimento_w,
            decode(ie_copiar_autorizacao_w, 'S', ie_autorizacao_w, null),
            ie_classif_agenda_origem_w,
            nr_seq_classif_w,
            nr_seq_turno_w,
            ie_diario_p,
            ie_final_semana_p,
            qt_intervalo_p,
            ds_dias_p,
            ie_copiar_proced_p,
            ie_copiar_proced_adic_p
            );

        IF ( nr_seq_proc_interno_w != 0  AND ie_gerar_autorizacao_w = 'S' AND ie_copiar_proced_p = 'S') then
          gerar_autor_regra(NULL, NULL, NULL, NULL, NULL, NULL, 'AS', nm_usuario_p, NULL, nr_seq_proc_interno_w, NULL, nr_sequencia_w, NULL, NULL, '', '', '');
        END IF ;
            
        if  (ie_copiar_prof_w = 'S') then
          insert into agenda_consulta_prof(
              nr_sequencia,
              nr_seq_agenda,
              dt_atualizacao,
              nm_usuario,
              dt_atualizacao_nrec,
              nm_usuario_nrec,
              nr_seq_ordem,
              ie_tipo_profissional,
              cd_pessoa_fisica,
              ie_solicita_retorno
              ) 
              select  agenda_consulta_prof_seq.nextval,
                nr_sequencia_w,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                nr_seq_ordem,
                ie_tipo_profissional,
                cd_pessoa_fisica,
                ie_solicita_retorno
              from  agenda_consulta_prof
              where  nr_seq_agenda  = nr_seq_agenda_p;
        end if;
        
        if ( ie_copiar_proced_adic_p = 'S') then
          insert into agenda_consulta_proc(
               nr_sequencia,
               cd_procedimento,
               nr_seq_proc_interno,
               cd_material_exame,
               nr_seq_agenda,
               qt_procedimento,
               ie_origem_proced,
               dt_atualizacao,
               nm_usuario,
               ie_executar_proc,
               nr_seq_exame,
               ie_glosa,
               ie_regra,
               nr_seq_regra,
               dt_atualizacao_nrec,
               nm_usuario_nrec,
               ds_observacao,
               ie_lado
              )          
          select agenda_consulta_proc_seq.nextval,
                 cd_procedimento,
                 nr_seq_proc_interno,
                 cd_material_exame,
                 nr_sequencia_w,
                 qt_procedimento,
                 ie_origem_proced,
                 dt_atualizacao,
                 nm_usuario,
                 ie_executar_proc,
                 nr_seq_exame,
                 ie_glosa,
                 ie_regra,
                 nr_seq_regra,
                 dt_atualizacao_nrec,
                 nm_usuario_nrec,
                 ds_observacao,
                 ie_lado
              from agenda_consulta_proc
              where nr_seq_agenda  = nr_seq_agenda_p;
              
          IF (ie_gerar_autorizacao_w = 'S') then
            open C01;
            loop
            fetch C01 into  
              nr_seq_proc_adic_w ,
              nr_seq_proc_interno_adic_w;
            exit when C01%notfound;
              begin
              if (nr_seq_proc_adic_w != 0) then
                gerar_autor_regra(NULL, NULL, NULL, NULL, NULL, NULL, 'AS', nm_usuario_p, NULL, nr_seq_proc_interno_adic_w, NULL, nr_sequencia_w, NULL, nr_seq_proc_adic_w, '', '', '');
              end if;
              end;
            end loop;
            close C01;
          
          END IF;
        end if;
        
        wheb_usuario_pck.set_ie_executar_trigger('S');
        update agenda_consulta set nr_sequencia = nr_sequencia where nr_sequencia = nr_sequencia_w;
        
        if  (ie_gerar_solic_pront_w = 'S') and
          (ie_status_agenda_w <> 'C') and
          (cd_pessoa_fisica_w is not null)then
          begin
          Gerar_Solic_Pront_Agenda(cd_pessoa_fisica_w, nr_sequencia_w, cd_agenda_w, dt_atual_w, nm_usuario_p);          
          exception
          when others then
            ds_erro_ww := '';
          end;          
        end if;
        
      else
        ds_erro_w := substr(ds_erro_w || PKG_DATE_FORMATERS.TO_VARCHAR(dt_atual_w, 'shortDayMonth', cd_estabelecimento_p, nm_usuario_p) || ', ',1,1000);
      end if;
    end if;
	<<pulardia>>
    if	(ie_next_day_p = 'S') and 
        (ie_insert_w = 'N') then
	dt_atual_w := dt_atual_w + 1;
    else
	dt_atual_w := dt_atual_w + qt_dia_w;
    end if;

    select  pkg_date_utils.get_weekday(dt_atual_w)
    into  cd_dia_semana_w
    from  dual;
    

    /* Verifica se a data e domingo ou sabado */
    if  (cd_dia_semana_w = 1) and
      (ie_final_semana_p = 'N') and
      (ie_fim_sab_chec_w = 'N') then
      dt_atual_w  := dt_atual_w + 1;
    elsif  (cd_dia_semana_w = 7) and
      (ie_final_semana_p = 'N') and
      (ie_fim_dom_chec_w = 'N') then
      dt_atual_w  := dt_atual_w + 2;
    end if;
	
	qt_sessao_controle_w := qt_total_secao_w;
	
	if (qt_total_secao_ww > 0) then
		qt_sessao_controle_w := qt_total_secao_ww;
	end if;
	
	if ((qt_sessao_controle_w > 0) and (nr_secao_w = qt_sessao_controle_w) and (ie_consiste_w = 'N')) then
		exit;
	end if;	
	
    end;
  end loop;
end if;

if  (nvl(length(ds_erro_w),0) > 0) then
  if  (length(ds_erro_w) > 162) then
    ds_erro_p := substr(WHEB_MENSAGEM_PCK.get_texto(277593,'DS_ERRO_W='||substr(ds_erro_w,1,161)),1,255);
  else
    ds_erro_p := substr(WHEB_MENSAGEM_PCK.get_texto(277593,'DS_ERRO_W='||substr(ds_erro_w,1,length(ds_erro_w)-2)),1,255);
  end if;
end if;

commit;

end gerar_agenda_con_semana_per;
/