create or replace
PROCEDURE Gerar_EIS_Pronto_Socorro(dt_parametro_P   DATE,
							       nm_usuario_p     VARCHAR2) IS

dt_atualizacao_w			date          := SYSDATE;
dt_parametro_fim_w			date;
dt_parametro_w				date;
dt_consulta_w				date;
dt_fim_consulta_w			date;
dt_enfermagem_w				date;
dt_receb_senha_w			date;
nr_atendimento_w			number(10);
hr_atendimento_w			number(2);
cd_pessoa_fisica_w			varchar2(10);
cd_medico_w					varchar2(10);
cd_procedencia_w			number(5);
ie_clinica_w				number(5);
ie_tipo_atendimento_w		number(3);
cd_convenio_w				number(5);
cd_setor_atendimento_w		number(5);
cd_motivo_alta_w			number(5)	:= 0;
cd_motivo_alta_pa_w			number(5);
dt_entrada_w				date;
nr_pacientes_w				number(15)	:= 0;
nr_pacientes_obs_w			number(15)	:= 0;
qt_min_espera_enf_w			number(15)	:= 0;
qt_min_espera_med_w			number(15)	:= 0;
qt_min_consulta_w			number(15)	:= 0;
qt_min_espera_recep_w		number(15)	:= 0;
qt_min_abertura_ficha_w 	number(15)	:= 0;
cd_estabelecimento_w		number(4)	:= 0;
qt_atend_tempo_w			number(15,0);
cd_motivo_alta_default_w	varchar2(5)	:= 0;
qt_min_espera_pos_enf_w		number(15)	:= 0;
cd_especialidade_w			number(05,0);
ds_retorno_w				varchar2(40);
ds_erro_w					varchar2(255);
ds_tablespace_w				varchar2(40);
nm_usuario_atend_w			varchar2(15);
cd_cid_doenca_w				varchar2(10);
ds_bairro_w					varchar2(80);
nr_seq_queixa_w				number(10,0);
nr_seq_tipo_acidente_w		number(10,0);
ie_carater_inter_sus_w		varchar2(2);
dt_defecho_w				date;
qt_min_desfecho_w			number(15)	:= 0;
cd_unidade_basica_w			varchar2(10);
cd_unidade_compl_w			varchar2(10);
cd_tipo_ocomodacao_w		number(4,0);
qt_min_ps_w					number(10,0);
qt_min_atend_w				number(10,0);
nr_seq_triagem_w			number(10,0);
qt_min_triagem_w			number(15)	:= 0;
dt_fim_triagem_w			date;
nm_usuario_triagem_w		varchar2(15);
ie_status_w					varchar2(5);
nr_seq_classificacao_w		number(10);
ie_clinica_ww				number(5);
ie_tipo_desfecho_w			varchar2(1);
nr_seq_triagem_prior_w		number(10,0);
nr_sequencia_w				number(10);
dt_geracao_senha_w			date;
qt_min_espera_senha_w		number(10,0);
cd_categoria_w				varchar2(10);
nr_prim_prescricao_w		number(15);
qt_minuto_checagem_w		number(15);
dt_check_prim_prescr_w		date;
qt_min_receb_senha_w		number(10,0);
qt_min_desf_senha_w			number(10);
qt_min_atend_senha_w		number(10);
qt_min_parecer_w			number(10);
dt_internacao_w				date;
dt_desfecho_inter_w			date;
qt_min_desf_inter_w			number(10);
dt_classif_risco_w			date;
qt_min_classif_atend_w		number(15,2);
dt_inicio_triagem_w			date;
dt_fim_triagem_pa_w			date;
qt_min_triagem_inic_cons_w	number(15,2);
qt_min_triagem_atend_w		number(15,2);
dt_inicio_observacao_w		date;
dt_fim_observacao_w			date;
qt_min_observacao_w			number(15,2);
nr_seq_turno_w				number(10,0);
qt_min_atend_prescr_w		number(15,2);
dt_medicacao_w				date;
ie_lib_desfecho_w			varchar2(1);

cursor c01 is
select	a.nr_atendimento,
		a.cd_estabelecimento,
		a.cd_procedencia,
		a.cd_pessoa_fisica,
		a.ie_clinica,
		a.ie_tipo_atendimento,
		a.cd_motivo_alta,
		to_char(a.dt_entrada,'hh24') hr_atendimento,
		a.cd_medico_resp,
		a.dt_entrada,
		b.cd_setor_atendimento,
		obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
		Obter_Categoria_Atendimento(a.nr_atendimento) cd_categoria,
		dt_atend_medico dt_consulta,
		dt_fim_consulta,
		dt_inicio_atendimento,
		a.nm_usuario_atend,
		nvl(nvl(OBTER_CID_PRINC_ATENDIMENTO(a.nr_atendimento),substr(OBTER_CID_ATENDIMENTO(A.NR_ATENDIMENTO,'P'),1,10)),substr(OBTER_CID_ATENDIMENTO(A.NR_ATENDIMENTO,'S'),1,10)),
		null,
		a.nr_seq_queixa,
		a.nr_seq_tipo_acidente,
		a.ie_carater_inter_sus,
		nvl(to_date(obter_dados_desfecho_pa(a.nr_atendimento,'UP'),'dd/mm/yyyy hh24:mi:ss'),nvl(b.dt_saida_unidade,a.dt_alta)),
		b.cd_unidade_basica,
		b.cd_unidade_compl,
		b.cd_tipo_acomodacao,
		((b.dt_saida_unidade - b.dt_entrada_unidade) * 1440 ),
		((nvl(nvl(b.dt_saida_unidade,a.dt_alta), sysdate) - a.dt_entrada) * 1440 ),
		a.nr_seq_triagem,
		a.dt_fim_triagem,
		a.nm_usuario_triagem,
		nvl(a.dt_recebimento_senha,a.dt_entrada),
		substr(obter_status_atend_pa_nova(	a.nr_Atendimento,nvl(b.dt_saida_unidade,a.dt_alta),
							a.dt_medicacao,
							a.dt_lib_medico,
							a.dt_atend_medico,
							a.dt_fim_consulta,
							a.dt_inicio_atendimento	,
							a.dt_fim_triagem,
							a.dt_chamada_paciente,
							a.dt_liberacao_enfermagem,
							a.dt_reavaliacao_medica,
							a.nr_atend_alta,
							a.cd_estabelecimento),1,10),
	nr_seq_classificacao,
	a.nr_seq_triagem_prioridade,
	((nvl(nvl(b.dt_saida_unidade,a.dt_alta), sysdate) - nvl(a.dt_recebimento_senha,nvl(a.dt_entrada,sysdate))) * 1440 ),
	((nvl(nvl(to_date(obter_dados_desfecho_pa(a.nr_atendimento,'UP'),'dd/mm/yyyy hh24:mi:ss'),nvl(b.dt_saida_unidade,a.dt_alta)), sysdate) - nvl(a.dt_recebimento_senha,nvl(a.dt_entrada,sysdate))) * 1440 ),
	(select	max(dt_desfecho)
	 from	atendimento_alta x
	 where	x.nr_atendimento = a.nr_atendimento
	 and	x.ie_desfecho = 'I'
	 and    ((ie_tipo_orientacao <> 'P')
		or  (ie_lib_desfecho_w = 'N')
		or  (dt_liberacao is not null and dt_inativacao is null))),
	to_date(obter_data_internacao(a.nr_atendimento),'dd/mm/yyyy hh24:mi:ss'),
	a.dt_classif_risco,
	a.dt_inicio_observacao,
	a.dt_fim_observacao,
	decode(a.dt_inicio_observacao,null,0,1),
	Obter_Turno_Atendimento(a.dt_entrada,a.cd_estabelecimento,'C')
from 	setor_atendimento s,
	atend_paciente_unidade b,
	atendimento_paciente a
where	a.dt_entrada between  	dt_parametro_w and 	dt_parametro_fim_w
and	a.nr_atendimento 		= b.nr_atendimento
and	b.cd_setor_atendimento 		= s.cd_setor_atendimento
and	DT_ATEND_ORIGINAL is null
AND	B.DT_ENTRADA_UNIDADE 		=
	(SELECT MIN(W.DT_ENTRADA_UNIDADE)
	FROM   SETOR_ATENDIMENTO X,
		ATEND_PACIENTE_UNIDADE W
	WHERE	W.NR_ATENDIMENTO = A.NR_ATENDIMENTO
	AND	W.CD_SETOR_ATENDIMENTO = X.CD_SETOR_ATENDIMENTO
	AND X.CD_CLASSIF_SETOR = 1)
and 	s.cd_classif_setor	= 1;

BEGIN

Gravar_Log_Indicador(12, obter_desc_expressao(293972)/*'Nascimento'*/, sysdate, trunc(dt_parametro_p), nm_usuario_p, nr_sequencia_w);

dt_parametro_fim_w                    :=
		       (to_date((to_char(dt_parametro_p,'dd/mm/yy') ||
			' 23:59:59'),'dd/mm/yy hh24:mi:ss'));
dt_parametro_w                        :=
       		(to_date((to_char(dt_parametro_p,'dd/mm/yy') ||
			' 00:00:00'),'dd/mm/yy hh24:mi:ss'));

select	min(cd_motivo_alta)
into	cd_motivo_alta_default_w
from	motivo_alta
where ie_situacao 		= 'A'
and	ie_censo_diario 	= 'S'
and	ie_obito 		= 'N'
and	ie_tipo_atendimento	= 3;

if	(cd_motivo_alta_default_w is null) then
	select	min(cd_motivo_alta)
	into	cd_motivo_alta_default_w
	from	motivo_alta
	where ie_situacao = 'A'
	and	ie_censo_diario = 'S'
	and	ie_obito = 'N';
end if;

delete from eis_pronto_socorro
where dt_referencia = dt_parametro_w;

select nvl(max(ie_liberar_desfecho),'N')
into   ie_lib_desfecho_w
from   parametro_medico
where  cd_estabelecimento = obter_estabelecimento_ativo;

OPEN C01;
LOOP
FETCH C01 into
	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_procedencia_w,
	cd_pessoa_fisica_w,
	ie_clinica_w,
	ie_tipo_atendimento_w,
	cd_motivo_alta_w,
	hr_atendimento_w,
	cd_medico_w,
	dt_entrada_w,
	cd_setor_atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	dt_consulta_w,
	dt_fim_consulta_w,
	dt_enfermagem_w,
	nm_usuario_atend_w,
	cd_cid_doenca_w,
	ds_bairro_w,
	nr_seq_queixa_w,
	nr_seq_tipo_acidente_w,
	ie_carater_inter_sus_w,
	dt_defecho_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w,
	cd_tipo_ocomodacao_w,
	qt_min_ps_w,
	qt_min_atend_w,
	nr_seq_triagem_w,
	dt_fim_triagem_w,
	nm_usuario_triagem_w,
	dt_receb_senha_w,
	ie_status_w,
	nr_seq_classificacao_w,
	nr_seq_triagem_prior_w,
	qt_min_receb_senha_w,
	qt_min_desf_senha_w,
	dt_desfecho_inter_w,
	dt_internacao_w,
	dt_classif_risco_w,
	dt_inicio_observacao_w,
	dt_fim_observacao_w,
	nr_pacientes_obs_w,
	nr_seq_turno_w;
EXIT WHEN C01%NOTFOUND;
	nr_pacientes_w			:= 1;
	qt_atend_tempo_w		:= 0;
	qt_Min_espera_med_w		:= 0;
	qt_min_triagem_w		:= 0;
	qt_min_espera_enf_w		:= 0;
	qt_min_espera_pos_enf_w	:= 0;
	qt_Min_consulta_w		:= 0;
	qt_min_desfecho_w		:= 0;
	qt_min_espera_recep_w 	:= 0;
	qt_min_abertura_ficha_w := 0;
	qt_Min_consulta_w		:= 0;
	qt_atend_tempo_w		:= 0;
	qt_min_espera_recep_w	:= 0;
	qt_min_abertura_ficha_w := 0;
	qt_min_espera_enf_w		:= 0;
	qt_Min_espera_med_w		:= 0;
	qt_min_espera_enf_w		:= 0;
	qt_min_atend_senha_w	:= 0;
	qt_min_desf_inter_w		:= 0;
	qt_min_classif_atend_w	:= 0;

	select	nvl(nr_seq_queixa_w,max(nr_seq_queixa)),
			max(dt_inicio_triagem),
			max(dt_fim_triagem)
	into	nr_seq_queixa_w,
			dt_inicio_triagem_w,
			dt_fim_triagem_pa_w
	from	triagem_pronto_Atend a
	where	nr_atendimento	= nr_atendimento_w;

	--qt_min_classif_atend_w	:= ((dt_consulta_w - dt_classif_risco_w) * 1440);
	if (dt_inicio_triagem_w is not null) then
		qt_min_classif_atend_w		:= abs(((dt_entrada_w - dt_inicio_triagem_w) * 1440));
	end if;
	qt_min_triagem_atend_w := 0;
	qt_min_triagem_inic_cons_w := 0;
	if (dt_fim_triagem_pa_w is not null) then
		qt_min_triagem_inic_cons_w	:= abs(((dt_fim_triagem_pa_w - dt_consulta_w) * 1440));
		qt_min_triagem_atend_w		:= abs(((dt_fim_triagem_pa_w - dt_entrada_w) * 1440));
	end if;

	if	(dt_consulta_w is not null) and
		(dt_fim_consulta_w is not null) and
		(dt_defecho_w is not null) then

		qt_Min_consulta_w		:= ((dt_fim_consulta_w - dt_consulta_w) * 1440);
		qt_min_desfecho_w		:= ((dt_defecho_w - dt_consulta_w) * 1440);
		qt_min_espera_recep_w 	:= ((dt_consulta_w - dt_entrada_w) * 1440);
		qt_min_abertura_ficha_w := ((dt_entrada_w - dt_receb_senha_w) * 1440);
		qt_min_atend_senha_w	:= ((dt_consulta_w - dt_receb_senha_w) * 1440);
		qt_min_desf_inter_w		:= ((dt_desfecho_inter_w - dt_internacao_w) * 1440);
		qt_atend_tempo_w		:= 1;
	end if;

	if	(dt_enfermagem_w is not null) and
		(dt_fim_consulta_w is not null) and
		(dt_defecho_w is not null) then
		qt_Min_espera_med_w		:= ((dt_consulta_w - dt_enfermagem_w) * 1440);
		qt_min_espera_enf_w		:= ((dt_enfermagem_w - dt_entrada_w) * 1440 );
		
		if	(dt_fim_triagem_w is not null) then
			qt_min_triagem_w		:= ((dt_fim_triagem_w - dt_enfermagem_w) * 1440 );
			qt_min_espera_pos_enf_w		:= (( dt_consulta_w - dt_fim_triagem_w) * 1440 );
		end if;
	end if;
	if	(nvl(qt_Min_consulta_w,-1) < 0) or
		/*(nvl(qt_min_espera_recep_w,-1) < 0) or
		(nvl(qt_min_abertura_ficha_w,-1) < 0) or*/
		(qt_Min_consulta_w > 1440) /*or
		(qt_min_espera_recep_w > 1440) or
		(qt_min_abertura_ficha_w> 1440) */then
		qt_Min_consulta_w		:= 0;
		qt_atend_tempo_w		:= 0;
		qt_min_espera_recep_w	:= 0;
		qt_min_abertura_ficha_w := 0;
	end if;

	if	(nvl(qt_Min_espera_med_w,-1) < 0) or
		(nvl(qt_min_espera_enf_w,-1) < 0)  or
		(qt_min_espera_enf_w > 1440)  then
		qt_min_espera_enf_w		:= 0;
		qt_Min_espera_med_w		:= 0;
		qt_min_espera_enf_w		:= 0;
	end if;

	select 	min(cd_especialidade)
	into 	cd_especialidade_w
	from 	medico_especialidade
	where 	cd_pessoa_fisica = cd_medico_w;

/*	select	max(cd_motivo_alta)
	into	cd_motivo_alta_pa_w
	from	atendimento_alta
	where	nr_atendimento	= nr_atendimento_w;   OS 461129*/

	select	max(((z.dt_liberacao - x.dt_liberacao) * 1440))
	into	qt_min_parecer_w
	from	parecer_medico_req x,
		parecer_medico z
	where	x.nr_parecer = z.nr_parecer(+)
	and	x.nr_atendimento = nr_atendimento_w;

	select	max(ie_desfecho)
	into	ie_tipo_desfecho_w
	from	atendimento_alta a
	where	a.nr_atendimento 	= nr_atendimento_w
	and	ie_desfecho is not null
	and	a.nr_sequencia		= (	select	max(e.nr_sequencia)
						from	atendimento_alta e
						where	e.nr_atendimento = a.nr_atendimento
						and	ie_desfecho is not null
						and    ((ie_tipo_orientacao <> 'P')
							or  (ie_lib_desfecho_w = 'N')
							or  (dt_liberacao is not null and dt_inativacao is null)));

	ie_clinica_ww		:= obter_clinica_pac_alteracao(nr_atendimento_w);

	select	max(a.dt_geracao_senha)
	into	dt_geracao_senha_w
	from	paciente_senha_fila a,
		atendimento_paciente b
	where 	b.nr_seq_pac_senha_fila = a.nr_sequencia
	and	b.nr_atendimento = nr_atendimento_w;

	if	(dt_geracao_senha_w is not null) then

		qt_min_espera_senha_w := ((dt_entrada_w - dt_geracao_senha_w)*1440);
	end if;

	if	(nvl(qt_min_espera_senha_w,-1) < 0) then
		qt_min_espera_senha_w := 0;

	end if;

	qt_minuto_checagem_w := 0;

	select	nvl(min(a.nr_prescricao),0)
	into	nr_prim_prescricao_w
	from	prescr_medica a
	where	exists(	select	1
					from	prescr_proc_hor b
					where	a.nr_prescricao = b.nr_prescricao
					and		b.dt_suspensao is null
					and		nvl(b.ie_situacao,'A') = 'A'
					union
					select	1
					from	prescr_mat_hor b
					where	a.nr_prescricao = b.nr_prescricao
					and		b.ie_agrupador = 1
					and		b.dt_suspensao is null
					and		nvl(b.ie_situacao,'A') = 'A')
	and		nvl(a.dt_liberacao_medico, a.dt_liberacao) is not null
	and		a.dt_suspensao is null
	and		a.nr_atendimento = nr_atendimento_w
	and		a.dt_inicio_prescr between dt_entrada_w - 1 and sysdate + 1;

	if	(nr_prim_prescricao_w > 0) then

		select	min(a.dt_fim_horario)
		into	dt_check_prim_prescr_w
		from	prescr_mat_hor a
		where	a.ie_agrupador = 1
		and		a.dt_fim_horario is not null
		and		a.dt_suspensao is null
		and		nvl(a.ie_situacao,'A') = 'A'
		and		a.nr_prescricao = nr_prim_prescricao_w;

		if	(dt_check_prim_prescr_w is null) then
			select	min(a.dt_fim_horario)
			into	dt_check_prim_prescr_w
			from	prescr_proc_hor a
			where	a.dt_fim_horario is not null
			and		a.dt_suspensao is null
			and		nvl(a.ie_situacao,'A') = 'A'
			and		a.nr_prescricao = nr_prim_prescricao_w;
		end if;

		select	nvl(max(obter_min_entre_datas(nvl(a.dt_liberacao_medico, a.dt_liberacao),nvl(dt_check_prim_prescr_w,sysdate),1)),0)
		into	qt_minuto_checagem_w
		from	prescr_medica a
		where	a.nr_prescricao = nr_prim_prescricao_w;
		
		select min(dt_medicacao)
		into dt_medicacao_w
		from atendimento_paciente 
		where nr_atendimento =  nr_atendimento_w;
		
		select	nvl(max(obter_min_entre_datas(nvl(a.dt_liberacao_medico, a.dt_liberacao),nvl(dt_medicacao_w,sysdate),1)),0)
		into	qt_min_atend_prescr_w
		from	prescr_medica a
		where	a.nr_prescricao = nr_prim_prescricao_w;
		
		
	end if;
	
	if	((dt_inicio_observacao_w is null) or
		 (dt_fim_observacao_w is null)) then
		qt_min_observacao_w := 0;
	else
		qt_min_observacao_w := round((dt_fim_observacao_w - dt_inicio_observacao_w) * 1440);
	end if;

-- Atualizar a tabela -----------------------------------------------
	begin
	update EIS_Pronto_Socorro
	set	nr_pacientes			= nr_pacientes + nr_pacientes_w,
		nr_pacientes_obs			= nr_pacientes_obs + nr_pacientes_obs_w,
		qt_minuto_espera		= qt_minuto_espera + qt_min_espera_enf_w,
		qt_minuto_consulta		= qt_minuto_consulta + qt_min_consulta_w,
		qt_min_espera_med		= qt_min_espera_med	+ qt_min_espera_med_w,
		qt_atend_tempo			= qt_atend_tempo + qt_atend_tempo_w,
		qt_min_desfecho			= qt_min_desfecho + qt_min_desfecho_w,
		qt_min_triagem			= qt_min_triagem + qt_min_triagem_w,
		qt_min_espera_recep 		= qt_min_espera_recep + qt_min_espera_recep_w,
		qt_min_abertura_ficha		= qt_min_abertura_ficha + qt_min_abertura_ficha_w,
		qt_min_fim_enf			= qt_min_fim_enf + qt_min_espera_pos_enf_w,
		nm_usuario_triagem		= nm_usuario_triagem_w,
		ie_status			= ie_status_w,
		nr_seq_classificacao		= nr_seq_classificacao_w,
		nr_seq_triagem_prioridade	= nr_seq_triagem_prior_w,
		qt_min_senha			= qt_min_senha + qt_min_espera_senha_w,
		qt_minuto_checagem		= qt_minuto_checagem_w,
		qt_min_classif_atend	= qt_min_classif_atend_w,
		qt_min_triagem_cons		= qt_min_triagem_inic_cons_w,
		qt_min_triagem_atend 	= qt_min_triagem_atend_w,
		qt_min_observacao		= qt_min_observacao_w,
		qt_min_atend_prescr     = qt_min_atend_prescr_w
	where	cd_estabelecimento		= cd_estabelecimento_w
	and	dt_referencia			= dt_parametro_w
	and	hr_atendimento			= hr_atendimento_w
	and	cd_procedencia			= cd_procedencia_w
	and	cd_medico			= cd_medico_w
	and	cd_motivo_alta			= nvl(cd_motivo_alta_pa_w, cd_motivo_alta_w)
	and	ie_clinica			= nvl(ie_clinica_ww,ie_clinica_w)
	and	cd_convenio			= cd_convenio_w
	and	cd_setor_atendimento		= cd_setor_atendimento_w
	and	cd_especialidade		= cd_especialidade_w
	and	ie_tipo_atendimento		= ie_tipo_atendimento_w
	and	cd_cid_doenca			= cd_cid_doenca_w
	and	cd_pessoa_fisica		= cd_pessoa_fisica_w;
	if	(sql%notfound) then
		insert into EIS_Pronto_Socorro (
			cd_estabelecimento	,
			dt_referencia    	,
			cd_procedencia	,
			hr_atendimento       ,
			cd_medico		,
			cd_motivo_alta	,
			ie_clinica		,
			cd_convenio		,
			nr_pacientes		,
			nr_pacientes_obs,
			qt_minuto_espera	,
			qt_Minuto_consulta	,
			qt_min_espera_med	,
			qt_atend_tempo	,
			cd_setor_atendimento	,
			cd_especialidade	,
			ie_tipo_atendimento,
			nm_usuario_atend,
			cd_cid_doenca,
			ds_bairro,
			cd_pessoa_fisica,
			nr_seq_queixa,
			nr_seq_tipo_acidente,
			ie_carater_inter_sus,
			qt_min_desfecho,
			cd_unidade_basica,
			cd_unidade_compl,
			cd_tipo_acomodacao,
			qt_min_ps,
			qt_min_atend,
			nr_seq_triagem,
			qt_min_triagem,
			nm_usuario_triagem,
			qt_min_espera_recep,
			qt_min_abertura_ficha,
			ie_status,
			nr_seq_classificacao,
			nr_seq_triagem_prioridade,
			nr_atendimento,
			ie_desfecho,
			qt_min_fim_enf,
			qt_min_senha,
			cd_categoria,
			qt_minuto_checagem,
			qt_min_atend_senha,
			qt_min_senha_desfecho,
			qt_min_internacao,
			qt_min_parecer,
			qt_min_senha_cons,
			qt_min_classif_atend,
			qt_min_triagem_cons,
			qt_min_triagem_atend,
			qt_min_observacao,
			nr_seq_turno,
			qt_min_atend_prescr)
		Values(
			cd_estabelecimento_w ,
			dt_Parametro_w    	,
			cd_procedencia_w	,
			hr_atendimento_w     ,
			cd_medico_w		,
			nvl(cd_motivo_alta_pa_w, cd_motivo_alta_w),
			nvl(ie_clinica_ww,ie_clinica_w)		,
			cd_convenio_w		,
			nr_pacientes_w	,
			nr_pacientes_obs_w,
			qt_min_espera_enf_w	,
			qt_Min_consulta_w	,
			qt_min_espera_med_w	,
			qt_atend_tempo_w	,
			cd_setor_atendimento_w,
			cd_especialidade_w	,
			ie_tipo_Atendimento_w,
			nm_usuario_atend_w,
			cd_cid_doenca_w,
			null,
			cd_pessoa_fisica_w,
			nr_seq_queixa_w,
			nr_seq_tipo_acidente_w,
			ie_carater_inter_sus_w,
			qt_min_desfecho_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w,
			cd_tipo_ocomodacao_w,
			qt_min_ps_w,
			qt_min_atend_w,
			nr_seq_triagem_w,
			qt_min_triagem_w,
			nm_usuario_triagem_w,
			qt_min_espera_recep_w,
			qt_min_abertura_ficha_w,
			ie_status_w,
			nr_seq_classificacao_w,
			nr_seq_triagem_prior_w,
			nr_atendimento_w,
			ie_tipo_desfecho_w,
			qt_min_espera_pos_enf_w,
			qt_min_espera_senha_w,
			cd_categoria_w,
			qt_minuto_checagem_w,
			qt_min_receb_senha_w,
			qt_min_desf_senha_w,
			qt_min_desf_inter_w,
			qt_min_parecer_w,
			qt_min_atend_senha_w,
			qt_min_classif_atend_w,
			qt_min_triagem_inic_cons_w,
			qt_min_triagem_atend_w,
			qt_min_observacao_w,
			nr_seq_turno_w,
			qt_min_atend_prescr_w);
	end if;
	exception
	when others then
		ds_erro_w		:= SQLERRM(sqlcode);
		--Nao Atualizou. Erro: ' || ds_erro_w 
		Wheb_mensagem_pck.exibir_mensagem_abort(264650,	'DS_ERRO_W='||DS_ERRO_W);
	end;
END LOOP;
CLOSE C01;
Atualizar_Log_Indicador(sysdate, nr_sequencia_w);
COMMIT;

select	obter_tablespace_index into ds_tablespace_w from dual;
obter_valor_dinamico('Alter index EISPRSO_PK_I' ||
           	' Rebuild Tablespace ' || ds_tablespace_w, ds_retorno_w);

END Gerar_EIS_Pronto_Socorro;
/
