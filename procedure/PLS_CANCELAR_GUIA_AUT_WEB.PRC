create or replace
procedure pls_cancelar_guia_aut_web(	nr_seq_guia_p			Number,
					nr_seq_requisicao_p		Number,
					nr_seq_prestador_web_p		Number,
					nr_seq_perfil_web_p		Number,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		Number,
					nr_seq_motivo_cancel_p		Number,
					ds_retorno_p			out	Varchar2) is

nr_seq_prestador_web_w			Number(10);
ds_retorno_w				Varchar2(255) := '';
ie_guia_importada_w			Varchar2(2);
ie_validade_vencida_w			Varchar2(2);
ie_requisicao_executada_w		Varchar2(2);
ie_requisicao_intercambio_w		Varchar2(2);
nr_seq_segurado_w			Number(10);
cd_guia_w				Varchar2(255);
qt_guia_imp_w				Number(10);
dt_validade_senha_w			Date;
qt_proc_exec_w				Number(10);
qt_mat_exec_w				Number(10);
qt_executado_w				Number(10);
ie_tipo_processo_w			Varchar2(2);
ie_cancela_guia_w			Varchar2(2) := 'N';
ie_cancela_req_w			Varchar2(2) := 'N';
nr_seq_auditoria_w			pls_auditoria.nr_sequencia%type;
nr_seq_conta_compl_w			number(10);
ie_situacao_prot_w			varchar2(3);

ie_cancel_guia_local_atend_w		pls_web_param_geral.ie_cancel_guia_local_atend%type	:= 'N';
ie_cancel_req_local_atend_w		pls_web_param_geral.ie_cancel_req_local_atend%type	:= 'N';
nr_seq_local_prest_req_w		pls_usuario_web.nr_seq_local_atend%type;
nr_seq_local_prest_portal_w		pls_usuario_web.nr_seq_local_atend%type;
nr_seq_auditoria_exec_w			pls_auditoria.nr_sequencia%type;

Cursor C01 is
	select	ie_guia_importada,
		ie_validade_vencida,
		ie_requisicao_executada,
		ie_requisicao_intercambio
	from	(
		select	nvl(ie_guia_importada,'N') ie_guia_importada,
			nvl(ie_validade_vencida,'N') ie_validade_vencida,
			nvl(ie_requisicao_executada,'N') ie_requisicao_executada,
			nvl(ie_requisicao_intercambio,'N') ie_requisicao_intercambio,
			nr_sequencia,
			nr_seq_perfil_web
		from	pls_regra_cancel_aut_req 
		where	ie_situacao	= 'A' 
		and	ie_aplicacao_regra	= 1
		and	((nr_seq_usuario_web = nr_seq_prestador_web_w and  nr_seq_perfil_web is null) 
		or 	(nr_seq_usuario_web is null and nr_seq_perfil_web = nr_seq_perfil_web_p))
		and	((cd_estabelecimento = cd_estabelecimento_p) and (pls_obter_se_controle_estab('RE') = 'S'))
		union all
		select	nvl(ie_guia_importada,'N') ie_guia_importada,
			nvl(ie_validade_vencida,'N') ie_validade_vencida,
			nvl(ie_requisicao_executada,'N') ie_requisicao_executada,
			nvl(ie_requisicao_intercambio,'N') ie_requisicao_intercambio,
			nr_sequencia,
			nr_seq_perfil_web
		from	pls_regra_cancel_aut_req 
		where	ie_situacao	= 'A' 
		and	ie_aplicacao_regra	= 1
		and	((nr_seq_usuario_web = nr_seq_prestador_web_w and  nr_seq_perfil_web is null) 
		or 	(nr_seq_usuario_web is null and nr_seq_perfil_web = nr_seq_perfil_web_p))
		and	(pls_obter_se_controle_estab('RE') = 'N'))
	order by nr_sequencia, 
		nr_seq_perfil_web;

Cursor c02 is
	select	a.nr_sequencia,
		b.ie_situacao
	from	pls_protocolo_conta b,
		pls_conta a
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_seq_segurado	= nr_seq_segurado_w
	and	a.cd_guia		= cd_guia_w
	and	a.ie_origem_conta	= 'C'
	and	a.ie_status <> 'C';

Cursor C03 is
	select	nr_sequencia
	from	pls_execucao_requisicao
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	
begin

if	(nr_seq_guia_p is not null) then
	begin
	select	nvl(nr_seq_prestador_web, 0),
		cd_guia,
		nr_seq_segurado
	into	nr_seq_prestador_web_w,
		cd_guia_w,
		nr_seq_segurado_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;
	exception
	when others then
		nr_seq_prestador_web_w	:= 0;
	end;
	
	begin
		select	nvl(ie_cancel_guia_local_atend,'N')
		into	ie_cancel_guia_local_atend_w
		from	pls_web_param_geral
		where	cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		ie_cancel_guia_local_atend_w	:= 'N';
	end;

	begin
		-- Obter o local de atendimento do prestador da requisi��o
		select	nvl(nr_seq_local_atend, 0)
		into	nr_seq_local_prest_req_w
		from	pls_usuario_web
		where	nr_sequencia	= nr_seq_prestador_web_w;
		
		-- Obter o local de atendimento do prestador do portal
		select	nr_seq_local_atend
		into	nr_seq_local_prest_portal_w
		from	pls_usuario_web
		where	nr_sequencia	= nr_seq_prestador_web_p;
	exception
	when others then
		nr_seq_local_prest_req_w	:= null;
		nr_seq_local_prest_portal_w	:= null;
	end;
	
	if	(nr_seq_prestador_web_w	= nr_seq_prestador_web_p) or
		((nr_seq_prestador_web_w <> nr_seq_prestador_web_p) and 
		 (ie_cancel_guia_local_atend_w	= 'S') and 
		 (nr_seq_local_prest_req_w	= nr_seq_local_prest_portal_w)) then

		open C01;
		loop
		fetch C01 into
			ie_guia_importada_w,
			ie_validade_vencida_w,
			ie_requisicao_executada_w,
			ie_requisicao_intercambio_w;
		exit when C01%notfound;
		end loop;
		close C01;

		/*Obter se a guia foi importada para a conta m�dica*/
		select	count(1)
		into	qt_guia_imp_w
		from	pls_conta a
		where	a.cd_guia	= cd_guia_w
		and	a.nr_seq_segurado= nr_seq_segurado_w
		and	a.ie_status <> 'C';

		/*Obter v�lidade da senha guia*/
		select	dt_validade_senha
		into	dt_validade_senha_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;

		/* Se n�o permitir, tem que verificar a quantidade de contas j� geradas para a guia */
		ie_cancela_guia_w	:= 'S';
		if	(ie_guia_importada_w = 'N') then
			if	(qt_guia_imp_w > 0) then
				ds_retorno_w	:= 'N�o � permitido cancelar uma guia j� importada !';
				ie_cancela_guia_w	:= 'N';
			end if;
		end if;

		/* Francisco - 06/07/2012 - Tratamento para remover do protocolo as contas das guias canceladas */
		if	(ie_cancela_guia_w = 'S') then
			open c02;
			loop
			fetch c02 into
				nr_seq_conta_compl_w,
				ie_situacao_prot_w;
			exit when c02%notfound;
				begin
				/* Se o protocolo j� foi rejeitado, nem precisa mais alterar nem dar mensagem */
				if	(nvl(ie_situacao_prot_w,'I') <> 'RE') then
					if      (nvl(ie_situacao_prot_w,'I')  <> 'I') then
						ds_retorno_w	:= 'O complemento de guia j� foi finalizado, o cancelamento da guia n�o pode ser efetuado.';
						ie_cancela_guia_w	:= 'N';
					else
						/* Criar novo protocolo rejeitado e transferir conta para este novo */
						pls_gerar_protocolo_rejeitado(nr_seq_conta_compl_w,nm_usuario_p);
					end if;
				end if;
				end;
			end loop;
			close c02;
		end if;

		if	(ie_validade_vencida_w	= 'N') and (dt_validade_senha_w	< sysdate) then
			ds_retorno_w	:= 'N�o � permitido cancelar uma guia com data de validade vencida !';
		elsif	(ie_validade_vencida_w	is not null and ie_cancela_guia_w = 'S') then
			ie_cancela_guia_w	:= 'S';	
		end if;
	else
		ds_retorno_w := 'Usu�rio logado diferente do usu�rio requisitante !';
	end if;
elsif	(nr_seq_requisicao_p is not null) then
	begin
	select	nvl(nr_seq_prestador_web, 0)
	into	nr_seq_prestador_web_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_prestador_web_w	:= 0;
	end;

	begin
		select	nvl(ie_cancel_req_local_atend,'N')
		into	ie_cancel_req_local_atend_w
		from	pls_web_param_geral
		where	cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		ie_cancel_req_local_atend_w	:= 'N';
	end;
	
	begin
		-- Obter o local de atendimento do prestador da requisi��o
		select	nr_seq_local_atend
		into	nr_seq_local_prest_req_w
		from	pls_usuario_web
		where	nr_sequencia	= nr_seq_prestador_web_w;
		
		-- Obter o local de atendimento do prestador do portal
		select	nr_seq_local_atend
		into	nr_seq_local_prest_portal_w
		from	pls_usuario_web
		where	nr_sequencia	= nr_seq_prestador_web_p;
	exception
	when others then
		nr_seq_local_prest_req_w	:= null;
		nr_seq_local_prest_portal_w	:= null;
	end;

	if	(nr_seq_prestador_web_w	= nr_seq_prestador_web_p) or
		((nr_seq_prestador_web_w <> nr_seq_prestador_web_p) and 
		 (ie_cancel_req_local_atend_w	= 'S') and 
		 (nr_seq_local_prest_req_w	= nr_seq_local_prest_portal_w))	then
	 
		open C01;
		loop
		fetch C01 into
			ie_guia_importada_w,
			ie_validade_vencida_w,
			ie_requisicao_executada_w,
			ie_requisicao_intercambio_w;
		exit when C01%notfound;
		end loop;
		close C01;

		/*Obter se a requisi��o ja foi executada*/
		select  nvl(max(qt_proc_executado),0)
		into	qt_proc_exec_w 
		from	pls_requisicao_proc
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_guia		is not null;

		select nvl(max(qt_mat_executado),0)
		into	qt_mat_exec_w
		from	pls_requisicao_mat
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_guia		is not null;

		qt_executado_w	:= qt_proc_exec_w + qt_mat_exec_w;

		/*Obter se a requisi��o � de interc�mbio*/
		select	ie_tipo_processo
		into	ie_tipo_processo_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;

		/*Obter v�lidade da senha guia*/
		select	dt_validade_senha
		into	dt_validade_senha_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;

		if	(ie_requisicao_executada_w = 'N') and (qt_executado_w > 0) then
			ds_retorno_w := 'N�o � permitido cancelar uma requisi��o j� executada!';
		elsif	(ie_requisicao_executada_w is not null ) then
			ie_cancela_req_w := 'S';
		end if;

		if	(ie_requisicao_intercambio_w	= 'N') and (ie_tipo_processo_w	= 'I') then
			ds_retorno_w	:= 'N�o � permitido cancelar uma requisi��o de interc�mbio!';
		elsif	(ie_requisicao_intercambio_w is not null and ie_cancela_req_w = 'S') then
			ie_cancela_req_w := 'S';
		end if;

		if	(ie_validade_vencida_w	= 'N') and (dt_validade_senha_w	< sysdate) then
			ds_retorno_w	:= 'N�o � permitido cancelar uma requisi��o com data de validade vencida! ';
		elsif	(ie_validade_vencida_w	is not null and ie_cancela_req_w = 'S') then
			ie_cancela_req_w := 'S';
		end if;
	else
		ds_retorno_w	:= 'Usu�rio logado diferente do usu�rio requisitante!';
	end if;
end if;

if	(ie_cancela_guia_w = 'S' and nr_seq_guia_p is not null) then

	--pls_cancelar_guia_requisicao(nr_seq_guia_p, null, nm_usuario_p); Alterado para chamar direto a pls_cancelar_autorizacao pois o tratamento estava redundante e n�o cancelava a execu��o.
	pls_cancelar_autorizacao(nr_seq_guia_p, nr_seq_motivo_cancel_p, nm_usuario_p, 'R');
	
	update	pls_guia_plano
	set	nr_seq_motivo_cancel = nr_seq_motivo_cancel_p
	where	nr_sequencia	= nr_seq_guia_p;
	
	if ((nr_seq_prestador_web_w <> nr_seq_prestador_web_p) and 
	    (ie_cancel_guia_local_atend_w	= 'S') and 
	    (nr_seq_local_prest_req_w	= nr_seq_local_prest_portal_w)) then
	    
		pls_guia_gravar_historico(nr_seq_guia_p, 7, 'Guia solicitada pelo usu�rio '||nr_seq_prestador_web_w
					  ||' - '||substr(pls_obter_nm_usuario_web(nr_seq_prestador_web_w),1,255)||' foi cancelada pelo usuario '||nr_seq_prestador_web_p
					  ||' - '||substr(pls_obter_nm_usuario_web(nr_seq_prestador_web_p),1,255)||', em '||
					  to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||'.', '', nm_usuario_p);	
	else
		pls_guia_gravar_historico(nr_seq_guia_p, 7, 'Guia cancelada no portal web pelo usu�rio '||nr_seq_prestador_web_p
					  ||' - '||substr(pls_obter_nm_usuario_web(nr_seq_prestador_web_p),1,255)||', em '||
					  to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||'.', '', nm_usuario_p);	
	end if;

elsif	(ie_cancela_req_w = 'S' and nr_seq_requisicao_p is not null) then

	update	pls_requisicao
	set	ie_estagio	= 3,
		ie_status	= 'C',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		nr_seq_motivo_cancel = nr_seq_motivo_cancel_p
	where	nr_sequencia	= nr_seq_requisicao_p;

	update	pls_requisicao_proc
	set	ie_status		= 'C',
		ie_estagio		= 'N',
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

	update	pls_requisicao_mat
	set	ie_status		= 'C',
		ie_estagio		= 'N',
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

	begin
		select	nr_sequencia
		into	nr_seq_auditoria_w
		from	pls_auditoria
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and		ie_status not in('F', 'C');
	exception
	when others then
		nr_seq_auditoria_w	:= 0;
	end;

	if	(nvl(nr_seq_auditoria_w,0)	<> 0) then
		update	pls_auditoria_item
		set	ie_status = 'C',
			ie_status_solicitacao = 'C',
			dt_atualizacao	 = sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_seq_auditoria	= nr_seq_auditoria_w;

		update	pls_auditoria
		set	ie_status		= 'C',
			dt_liberacao		= sysdate,
			nr_seq_proc_interno	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_auditoria_w;
	end if;
	
	--OS 1308565, cancela a an�lise da execu��o
	for C03_w in C03 loop
		begin
		update	pls_execucao_requisicao
		set	ie_situacao	= 5,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= C03_w.nr_sequencia
		and	ie_situacao	= 2;
		
		update	pls_execucao_req_item
		set	ie_situacao	= 'C',
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_seq_execucao	= C03_w.nr_sequencia
		and	ie_situacao	= 'A';
		
		begin
			select 	nr_sequencia
			into	nr_seq_auditoria_exec_w
			from 	pls_auditoria
			where	nr_seq_execucao	= C03_w.nr_sequencia;
		exception
		when others then
			nr_seq_auditoria_exec_w	:= null;
		end;
		
		if (nr_seq_auditoria_exec_w is not null) then
			update	pls_auditoria
			set	ie_status		= 'C',
				dt_liberacao		= sysdate,
				nr_seq_proc_interno	= null,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_auditoria_exec_w;
			
			update	pls_auditoria_item
			set	ie_status		= 'C',
				ie_status_solicitacao	= 'C',
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_auditoria	= nr_seq_auditoria_exec_w;
		end if;
		end;
	end loop;

	if ((nr_seq_prestador_web_w <> nr_seq_prestador_web_p) and 
	    (ie_cancel_req_local_atend_w	= 'S') and 
	    (nr_seq_local_prest_req_w	= nr_seq_local_prest_portal_w)) then
	    
		pls_requisicao_gravar_hist(nr_seq_requisicao_p, 'L', 'Requisi��o solicitada pelo usu�rio '||nr_seq_prestador_web_w||
					' - '||substr(pls_obter_nm_usuario_web(nr_seq_prestador_web_w),1,255)||' foi cancelada pelo usu�rio '||nr_seq_prestador_web_p||
					' - '||substr(pls_obter_nm_usuario_web(nr_seq_prestador_web_p),1,255)||', em '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||'.', '', nm_usuario_p);				   
	else
		pls_requisicao_gravar_hist(nr_seq_requisicao_p, 'L', 'Requisi��o cancelada no portal web pelo usu�rio '||nr_seq_prestador_web_p
				   ||' - '||substr(pls_obter_nm_usuario_web(nr_seq_prestador_web_p),1,255)||', em '||
				   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||'.', '', nm_usuario_p);
	end if;

elsif	((ie_cancela_req_w = 'N' and ie_cancela_guia_w <> 'N') or 
	 (ie_cancela_guia_w = 'N' and ie_cancela_req_w <> 'N') or 
	 (ie_cancela_guia_w = 'N' and ie_cancela_req_w = 'N')) then
		if	(ds_retorno_w is null or ds_retorno_w = '') then
			ds_retorno_w	:= 'Usu�rio sem permiss�o para realizar o cancelamento !';
		end if;
end if;

ds_retorno_p := ds_retorno_w;

commit;

end pls_cancelar_guia_aut_web;
/
