create or replace
procedure ptu_imp_intercambio
			(	ds_conteudo_p		varchar2,
				nm_arquivo_p		varchar2,
				ie_tipo_contrato_p	varchar2,
				nm_usuario_p		Varchar2) is 

dt_mov_inicio_ww		varchar2(8);
dt_mov_fim_ww			varchar2(8);
dt_inclusao_ww			varchar2(8);
dt_exclusao_ww			varchar2(8);
dt_inclusao_plano_ww		varchar2(8);
dt_exclusao_plano_ww		varchar2(8);
dt_nascimento_ww		varchar2(8);
dt_inclusao_benef_ww		varchar2(8);
dt_exclusao_benef_ww		varchar2(8);
dt_validade_carteira_ww		varchar2(8);
dt_repasse_ww			varchar2(8);
dt_fim_repasse_ww		varchar2(8);
dt_inclusao_plano_dest_ww	varchar2(8);
dt_fim_carencia_ww		varchar2(8);
dt_fim_carencia_preex_ww	varchar2(8);
dt_geracao_ww			varchar2(8);

--101 - intercambio
nr_seq_intercambio_w		number(10);
nr_seq_empresa_w		number(10);
nr_seq_plano_w			number(10);
nr_seq_benef_w			number(10);
nr_seq_carencia_w		number(10);
nr_seq_agregado_w		number(10);
nr_seq_compl_w			number(10);
nr_seq_preexistencia_w		number(10);

--102 - intercambio
cd_unimed_destino_w		varchar2(3);
cd_unimed_origem_w		varchar2(3);
dt_geracao_w			date;
ie_tipo_mov_w			varchar2(1);
dt_mov_inicio_w			date;
dt_mov_fim_w			date;
nr_versao_transacao_w		number(2);

--103 - intercambio
cd_filial_w			number(3);
ds_razao_social_w		varchar2(40);
nm_empr_abrev_w			varchar2(18);
ie_tipo_pessoa_w		number(1);
cd_cgc_cpf_w			varchar2(15);
nr_insc_estadual_w		number(20);
ds_endereco_w			varchar2(40);
ds_complemento_w		varchar2(20);
ds_bairro_w			varchar2(30);
cd_cep_w			number(8);
nm_cidade_w			varchar2(30);
sg_uf_w				varchar2(2);
nr_ddd_w			number(4);
nr_telefone_w			number(8);
nr_fax_w			number(8);
dt_inclusao_w			date;
dt_exclusao_w			date;
cd_empresa_origem_w		number(10);
cd_municipio_ibge_w		varchar2(7);
nr_endereco_empresa_w		varchar2(5);

--103 - intercambio
nr_seq_segurado_w		number(10);
cd_plano_origem_w		varchar2(6);
ds_plano_origem_w		varchar2(40);
cd_plano_intercambio_w		varchar2(3);
dt_inclusao_plano_w		date;
dt_exclusao_plano_w		date;
ie_natureza_w			number(1);
ie_abrangencia_w		number(1);
nr_ind_reembolso_w		number(3,2);
nr_seq_plano_ww			number(10);
cd_ope_ans_w			varchar2(10);
cd_prod_ans_w			varchar2(20);
ie_plano_w			varchar2(1);

--104 - intercambio
cd_unimed_w			varchar2(12);
cd_usuario_plano_w		varchar2(19);
cd_familia_w			number(32);
nm_benef_abreviado_w		varchar2(38);
cd_plano_intercambio_benef_w	varchar2(3);
dt_nascimento_w			date;
ie_sexo_w			varchar2(1);
cd_cgc_cpf_benef_w		number(15);
nr_rg_w				varchar2(15);
sg_uf_rg_w			varchar2(2);
ie_estado_civil_w		varchar2(1);
dt_inclusao_benef_w		date;
dt_exclusao_benef_w		date;
ie_repasse_w			varchar2(1);
cd_dependencia_w		number(2);
ie_recem_nascido_w		varchar2(1);
nr_matricula_w			number(20);
dt_validade_carteira_w		date;
cd_local_atendimento_w		number(4);
cd_lotacao_w			varchar2(8);
ds_lotacao_w			varchar2(30);
dt_repasse_w			date;
dt_fim_repasse_w		date;
dt_inclusao_plano_dest_w	date;
cd_plano_origem_benef_w		varchar2(6);
nr_vigencia_origem_w		number(4);
cd_titular_plano_w		varchar2(13);
nm_beneficiario_w		varchar2(120);
nr_cartao_nac_sus_w		varchar2(20);
nm_mae_w			varchar2(70);
nr_pis_pasep_w			varchar2(11);
cd_segmentacao_ptu_w		varchar2(2);	/* -- Atributo adicionado PTU 6.3 - OS: 910470 - lkcoelho */

--105 - intercambio
cd_tipo_cobertura_w		varchar2(3);
dt_fim_carencia_w		date;

--106 - intercambio
cd_tipo_produto_w		varchar2(2);
ds_produto_w			varchar2(20);

--107 - intercambio
ds_endereco_benef_w		varchar2(40);
ds_bairro_benef_w		varchar2(30);
cd_cep_benef_w			varchar2(8);
nm_municipio_w			varchar2(30);
sg_uf_benef_w			varchar2(2);
nr_ddd_benef_w			number(4);
nr_fone_w			number(8);
nr_ramal_w			number(8);
nr_endereco_benef_w		varchar2(5);

--108 - intercambio
cd_cid_w			varchar2(4);
dt_fim_carencia_preex_w		date;

cd_tipo_registro_w		varchar2(3);
ds_erro_w			varchar2(4000);

--109 - interc�mbio
qt_benef_intercambio_w		number(7);

--begin

begin

cd_tipo_registro_w	:= substr(ds_conteudo_p,9,3);

if	(cd_tipo_registro_w	= '101') then
	select	decode(substr(ds_conteudo_p,13,3),0,'',substr(ds_conteudo_p,13,3)),
		decode(substr(ds_conteudo_p,17,3),0,'',substr(ds_conteudo_p,17,3)),
		decode(substr(ds_conteudo_p,45,2),0,'',substr(ds_conteudo_p,45,2))
	into	cd_unimed_destino_w,
		cd_unimed_origem_w,
		nr_versao_transacao_w
	from	dual;


	dt_geracao_ww		:= substr(ds_conteudo_p,26,2)||substr(ds_conteudo_p,24,2)||substr(ds_conteudo_p,20,4);	
	if	(dt_geracao_ww <> '        ') then
		dt_geracao_w		:= to_date(dt_geracao_ww,'dd/mm/yyyy');
	end if;
	ie_tipo_mov_w		:= trim(substr(ds_conteudo_p,28,1));
	dt_mov_inicio_ww	:= substr(ds_conteudo_p,35,2)||substr(ds_conteudo_p,33,2)||substr(ds_conteudo_p,29,4);
	if	(dt_mov_inicio_ww <> '        ') then
		dt_mov_inicio_w		:= to_Date(dt_mov_inicio_ww,'dd/mm/yyyy');
	end if;
	dt_mov_fim_ww		:= substr(ds_conteudo_p,43,2)||substr(ds_conteudo_p,41,2)||substr(ds_conteudo_p,37,4);
	if	(dt_mov_fim_ww  <> '        ') then
		dt_mov_fim_w	:= to_date(dt_mov_fim_ww,'dd/mm/yyyy');
	end if;
	
	select	ptu_intercambio_seq.nextval
	into	nr_seq_intercambio_w
	from	dual;
	
	insert into ptu_intercambio
		(nr_sequencia, cd_unimed_destino, cd_unimed_origem,
		dt_geracao, ie_tipo_mov, dt_mov_inicio,
		dt_mov_fim, nr_versao_transacao, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		ie_operacao, nr_seq_envio, nm_arquivo,
		dt_importacao, ie_tipo_contrato)
	values	(nr_seq_intercambio_w, cd_unimed_destino_w, cd_unimed_origem_w,
		dt_geracao_w, ie_tipo_mov_w, dt_mov_inicio_w,
		dt_mov_fim_w, nr_versao_transacao_w, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		'R', null, nm_arquivo_p,
		sysdate, ie_tipo_contrato_p);
		
elsif	(cd_tipo_registro_w	= '102') then
	select	decode(to_number(substr(ds_conteudo_p,16,3)),0,'',to_number(substr(ds_conteudo_p,16,3))),
		decode(substr(ds_conteudo_p,79,14),0,'',substr(ds_conteudo_p,79,14)),
		decode(to_number(substr(ds_conteudo_p,93,20)),0,'',to_number(substr(ds_conteudo_p,93,20))),
		decode(to_number(substr(ds_conteudo_p,203,8)),0,'',to_number(substr(ds_conteudo_p,203,8))),
		decode(to_number(substr(ds_conteudo_p,243,4)),0,'',to_number(substr(ds_conteudo_p,243,4))),
		decode(to_number(substr(ds_conteudo_p,279,10)),0,'',to_number(substr(ds_conteudo_p,279,10)))
	into	cd_filial_w,
		cd_cgc_cpf_w,
		nr_insc_estadual_w,
		cd_cep_w,
		nr_ddd_w,
		cd_empresa_origem_w
	from	dual;
	
	if	(substr(ds_conteudo_p,247,8) = '        ') then
		nr_telefone_w := trim(substr(ds_conteudo_p,297,8));
	else
		nr_telefone_w := trim(substr(ds_conteudo_p,247,8));
	end if;
	
	if	(substr(ds_conteudo_p,255,8) = '        ') then
		nr_fax_w := trim(substr(ds_conteudo_p,306,8));
	else
		nr_fax_w := trim(substr(ds_conteudo_p,255,8));
	end if;	
	
	ie_tipo_pessoa_w	:= trim(substr(ds_conteudo_p,77,1));
	
	if	((ie_tipo_pessoa_w is null) or
		(ie_tipo_pessoa_w	= 0)) then
		ie_tipo_pessoa_w	:= 1;
	end if;	
	
	ds_razao_social_w	:= trim(substr(ds_conteudo_p,19,40));
	nm_empr_abrev_w		:= trim(substr(ds_conteudo_p,59,18));
	ds_endereco_w		:= trim(substr(ds_conteudo_p,113,40));
	ds_complemento_w	:= trim(substr(ds_conteudo_p,153,20));
	ds_bairro_w		:= trim(substr(ds_conteudo_p,173,30));
	nm_cidade_w		:= trim(substr(ds_conteudo_p,211,30));
	sg_uf_w			:= trim(substr(ds_conteudo_p,241,2));
	nr_endereco_empresa_w	:= trim(substr(ds_conteudo_p,314,5));
	dt_inclusao_ww		:= substr(ds_conteudo_p,269,2)||substr(ds_conteudo_p,267,2)||substr(ds_conteudo_p,263,4);
	if	(dt_inclusao_ww <> '        ') then
		dt_inclusao_w		:= to_date(dt_inclusao_ww,'dd/mm/yyyy');
	end if;
	dt_exclusao_ww		:= substr(ds_conteudo_p,277,2)||substr(ds_conteudo_p,275,2)||substr(ds_conteudo_p,271,4);
	if	(dt_exclusao_ww <> '        ') then
		dt_exclusao_w		:= to_date(dt_exclusao_ww,'dd/mm/yyyy');
	end if;
	cd_municipio_ibge_w	:= trim(substr(ds_conteudo_p,289,7));
	
	select	max(nr_Sequencia)
	into	nr_seq_intercambio_w
	from	ptu_intercambio;

	select	ptu_intercambio_empresa_seq.nextval
	into	nr_seq_empresa_w
	from	dual;

	insert into ptu_intercambio_empresa
		(nr_sequencia, cd_filial, ds_razao_social, 
		nm_empr_abrev, ie_tipo_pessoa, cd_cgc_cpf, 
		nr_insc_estadual, ds_endereco, ds_complemento, 
		ds_bairro, cd_cep, nm_cidade, 
		sg_uf, nr_ddd, nr_telefone, 
		nr_fax, dt_inclusao, dt_exclusao, 
		cd_empresa_origem, cd_municipio_ibge, nm_usuario,
		nm_usuario_nrec,dt_atualizacao_nrec,dt_atualizacao, 
		nr_seq_contrato, nr_seq_intercambio, nr_endereco)
	values	( nr_seq_empresa_w, cd_filial_w, ds_razao_social_w,
		nm_empr_abrev_w, ie_tipo_pessoa_w, nvl(cd_cgc_cpf_w,'00000000000000'),	
		nr_insc_estadual_w, ds_endereco_w, ds_complemento_w,
		ds_bairro_w, cd_cep_w, nm_cidade_w, 
		sg_uf_w, nr_ddd_w, nr_telefone_w, 
		nr_fax_w, dt_inclusao_w, dt_exclusao_w, 
		cd_empresa_origem_w, cd_municipio_ibge_w,nm_usuario_p,
		nm_usuario_p, sysdate, sysdate,
		null, nr_seq_intercambio_w, nr_endereco_empresa_w);
		
elsif	(cd_tipo_registro_w	= '103') then
	select	max(nr_sequencia)
	into	nr_seq_empresa_w
	from	ptu_intercambio_empresa;
	
	select	max(nr_seq_intercambio)
	into	nr_seq_intercambio_w
	from	ptu_intercambio_empresa
	where	nr_sequencia	= nr_seq_empresa_w;
	
	select	max(nr_versao_transacao)
	into	nr_versao_transacao_w
	from	ptu_intercambio
	where	nr_sequencia = nr_seq_intercambio_w;
	
	cd_plano_origem_w	:= trim(substr(ds_conteudo_p,12,6));
	ds_plano_origem_w	:= trim(substr(ds_conteudo_p,18,40));
	cd_ope_ans_w		:= trim(to_number(substr(ds_conteudo_p,83,10)));
	cd_prod_ans_w		:= trim(substr(ds_conteudo_p,93,20));
	
	if	(nr_versao_transacao_w >= 11) then -- PTU 5.0
		cd_plano_intercambio_w	:= trim(substr(ds_conteudo_p,114,3));
	else
		cd_plano_intercambio_w	:= trim(substr(ds_conteudo_p,58,2));
	end if;
	
	dt_inclusao_plano_ww	:= substr(ds_conteudo_p,66,2)||substr(ds_conteudo_p,64,2)||substr(ds_conteudo_p,60,4);
	if	(dt_inclusao_plano_ww <>  '        ') then
		dt_inclusao_plano_w := to_date(dt_inclusao_plano_ww,'dd/mm/yyyy');
	end if;
	
	dt_exclusao_plano_ww	:= substr(ds_conteudo_p,74,2)||substr(ds_conteudo_p,72,2)||substr(ds_conteudo_p,68,4);
	if	(dt_exclusao_plano_ww	<> '        ') then
		dt_exclusao_plano_w	:= to_Date(dt_exclusao_plano_ww,'dd/mm/yyyy');
	end if;
	
	select	decode(to_number(substr(ds_conteudo_p,76,1)),0,'',to_number(substr(ds_conteudo_p,76,1))),
		decode(to_number(substr(ds_conteudo_p,77,1)),0,'',to_number(substr(ds_conteudo_p,77,1))),
		decode(to_number(replace(substr(ds_conteudo_p,78,3),'.',',')),0,'',to_number(replace(substr(ds_conteudo_p,78,3),'.',','))),
		decode(to_number(substr(ds_conteudo_p,83,10)),0,'',to_number(substr(ds_conteudo_p,83,10))),
		decode(substr(ds_conteudo_p,113,1),'0','',substr(ds_conteudo_p,113,1))
	into	ie_natureza_w,
		ie_abrangencia_w,
		nr_ind_reembolso_w,
		nr_seq_plano_ww,
		ie_plano_w
	from	dual;
	
	select	ptu_intercambio_plano_seq.nextval
	into	nr_seq_plano_w
	from	dual;
	
	insert into ptu_intercambio_plano
		(nr_sequencia, nr_seq_empresa, cd_plano_origem,
		cd_plano_intercambio, dt_inclusao, dt_exclusao,
		ie_natureza, ie_abrangencia, nr_ind_reembolso,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
		nm_usuario_nrec, ds_plano_origem, nr_seq_plano,
		cd_ope_ans,cd_prod_ans, ie_plano)
	values	(nr_seq_plano_w , nr_seq_empresa_w, cd_plano_origem_w,
		cd_plano_intercambio_w, dt_inclusao_plano_w, dt_exclusao_plano_w,	
		ie_natureza_w, nvl(ie_abrangencia_w,0), nr_ind_reembolso_w,
		sysdate, nm_usuario_p, sysdate, 
		nm_usuario_p, ds_plano_origem_w, nr_seq_plano_ww,
		cd_ope_ans_w,cd_prod_ans_w, ie_plano_w);
		
elsif	(cd_tipo_registro_w	= '104') then
	select	max(nr_sequencia)
	into	nr_seq_empresa_w
	from	ptu_intercambio_empresa;
	
	select	max(nr_seq_intercambio)
	into	nr_seq_intercambio_w
	from	ptu_intercambio_empresa
	where	nr_sequencia	= nr_seq_empresa_w;
	
	select	max(nr_versao_transacao)
	into	nr_versao_transacao_w
	from	ptu_intercambio
	where	nr_sequencia = nr_seq_intercambio_w;

	select	decode(substr(ds_conteudo_p,13,3),0,'',substr(ds_conteudo_p,13,3)),
		decode(to_number(substr(ds_conteudo_p,32,6)),0,'000000',to_number(substr(ds_conteudo_p,32,6))),
		decode(to_number(substr(ds_conteudo_p,74,15)),0,'',to_number(substr(ds_conteudo_p,74,15))),
		decode(to_number(substr(ds_conteudo_p,124,2)),0,'0',to_number(substr(ds_conteudo_p,124,2))),
		decode(to_number(lpad(substr(ds_conteudo_p,127,20),'')),0,'',to_number(lpad(substr(ds_conteudo_p,127,20),''))),
		decode(to_number(substr(ds_conteudo_p,155,4)),0,'',to_number(substr(ds_conteudo_p,155,4))),
		decode(to_number(substr(ds_conteudo_p,293,4)),0,'',to_number(substr(ds_conteudo_p,293,4)))
	into	cd_unimed_w,
		cd_familia_w,
		cd_cgc_cpf_benef_w,
		cd_dependencia_w,
		nr_matricula_w,
		cd_local_atendimento_w,
		nr_vigencia_origem_w
	from	dual;
	
	cd_usuario_plano_w	:= trim(substr(ds_conteudo_p,19,13));
	cd_titular_plano_w	:= trim(substr(ds_conteudo_p,297,13));
	nm_benef_abreviado_w	:= trim(substr(ds_conteudo_p,38,25));
	
	if	(nr_versao_transacao_w >= 11) then -- PTU 5.0
		cd_plano_intercambio_benef_w	:= trim(substr(ds_conteudo_p,526,3));
	else
		cd_plano_intercambio_benef_w	:= trim(substr(ds_conteudo_p,63,2));
	end if;
	
	dt_nascimento_ww	:= substr(ds_conteudo_p,71,2)||substr(ds_conteudo_p,69,2)||substr(ds_conteudo_p,65,4);
	if	(dt_nascimento_ww  <> '        ') then
		dt_nascimento_w	:= to_date(dt_nascimento_ww,'dd/mm/yyyy');
	end if;
	
	ie_sexo_w		:= trim(substr(ds_conteudo_p,73,1));
	nr_rg_w			:= trim(substr(ds_conteudo_p,89,15));
	sg_uf_rg_w		:= trim(substr(ds_conteudo_p,104,2));
	
	ie_estado_civil_w	:= trim(substr(ds_conteudo_p,106,1));
	dt_inclusao_benef_ww	:= substr(ds_conteudo_p,113,2)||substr(ds_conteudo_p,111,2)||substr(ds_conteudo_p,107,4);
	if	(dt_inclusao_benef_ww  <> '        ') then
		dt_inclusao_benef_w	:= to_Date(dt_inclusao_benef_ww,'dd/mm/yyyy');
	end if;
	
	dt_exclusao_benef_ww	:= substr(ds_conteudo_p,121,2)||substr(ds_conteudo_p,119,2)||substr(ds_conteudo_p,115,4);
	if	(dt_exclusao_benef_ww <> '        ') then
		dt_exclusao_benef_w	:= dt_exclusao_benef_w || to_Date(dt_exclusao_benef_ww,'dd/mm/yyyy');
	end if;
	
	ie_repasse_w		:= trim(substr(ds_conteudo_p,123,1));
	ie_recem_nascido_w	:= trim(substr(ds_conteudo_p,126,1));
	
	dt_validade_carteira_ww	:= substr(ds_conteudo_p,153,2)||substr(ds_conteudo_p,151,2)||substr(ds_conteudo_p,147,4);
	if	(dt_validade_carteira_ww <> '        ') then
		dt_validade_carteira_w	:= to_date(dt_validade_carteira_ww,'dd/mm/yyyy');
	end if;
	
	cd_lotacao_w		:= trim(substr(ds_conteudo_p,159,8));
	ds_lotacao_w		:= trim(substr(ds_conteudo_p,167,30));
	
	dt_repasse_ww		:= substr(ds_conteudo_p,267,2)||substr(ds_conteudo_p,265,2)||substr(ds_conteudo_p,261,4);
	if	(dt_repasse_ww <> '        ') then
		dt_repasse_w	:= to_date(dt_repasse_ww,'dd/mm/yyyy');
	end if;
	
	dt_fim_repasse_ww	:= substr(ds_conteudo_p,275,2)||substr(ds_conteudo_p,273,2)||substr(ds_conteudo_p,269,4);
	if	(dt_fim_repasse_ww <> '        ') then
		dt_fim_repasse_w	:= to_date(dt_fim_repasse_ww,'dd/mm/yyyy');	
	end if;
	
	dt_inclusao_plano_dest_ww	:= substr(ds_conteudo_p,283,2)||substr(ds_conteudo_p,281,2)||substr(ds_conteudo_p,277,4);
	if	(dt_inclusao_plano_dest_ww	  is not null) then
		dt_inclusao_plano_dest_w	:= to_date(dt_inclusao_plano_dest_ww,'dd/mm/yyyy');
	end if;
	
	cd_plano_origem_benef_w	:= trim(substr(ds_conteudo_p,287,6));
	nm_beneficiario_w	:= trim(substr(ds_conteudo_p,310,120));	
	nr_cartao_nac_sus_w	:= trim(substr(ds_conteudo_p,430,15));	
	nm_mae_w		:= trim(substr(ds_conteudo_p,445,70));
	nr_pis_pasep_w		:= trim(substr(ds_conteudo_p,515,11));
	
	if	(nr_versao_transacao_w >= 14) then -- PTU 6.3	
		cd_segmentacao_ptu_w	:= trim(substr(ds_conteudo_p,529,2));
	else
		cd_segmentacao_ptu_w	:= null;
	end if;
	
	select	max(nr_Sequencia)
	into	nr_seq_empresa_w
	from	ptu_intercambio_empresa;

	select	ptu_intercambio_benef_seq.nextval
	into	nr_seq_benef_w
	from	dual;
	
	insert into ptu_intercambio_benef
		(nr_sequencia, nr_seq_empresa, cd_unimed, 
		cd_familia, nm_benef_abreviado, cd_plano_intercambio, 
		dt_nascimento, ie_sexo, ie_estado_civil, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
		nm_usuario_nrec, dt_inclusao, ie_repasse, 
		cd_dependencia, cd_local_atendimento, dt_repasse, 
		dt_inclusao_plano_dest, cd_cgc_cpf, nr_rg, 
		sg_uf_rg, dt_exclusao, ie_recem_nascido, 
		nr_matricula, dt_validade_carteira, cd_lotacao, 
		ds_lotacao, dt_fim_repasse, nr_vigencia_origem, 
		cd_plano_origem, cd_titular_plano, nm_beneficiario, 
		cd_usuario_plano, nr_seq_segurado,ie_status,
		nr_cartao_nac_sus, nm_mae_benef, nr_pis_pasep, cd_segmentacao_ptu)
	values	(nr_seq_benef_w, nr_seq_empresa_w,cd_unimed_w, 
		cd_familia_w, nm_benef_abreviado_w, cd_plano_intercambio_benef_w,
		dt_nascimento_w, ie_sexo_w, ie_estado_civil_w,
		sysdate, nm_usuario_p, sysdate, 
		nm_usuario_p, dt_inclusao_benef_w, ie_repasse_w,
		cd_dependencia_w, cd_local_atendimento_w, dt_repasse_w,
		dt_inclusao_plano_dest_w, cd_cgc_cpf_benef_w, nr_rg_w,
		sg_uf_rg_w, dt_exclusao_benef_w, ie_recem_nascido_w,
		nr_matricula_w, dt_validade_carteira_w, cd_lotacao_w, 
		ds_lotacao_w, dt_fim_repasse_w, nr_vigencia_origem_w,
		cd_plano_origem_benef_w, cd_titular_plano_w, nm_beneficiario_w,
		cd_usuario_plano_w, nr_seq_segurado_w,'I',
		nr_cartao_nac_sus_w, nm_mae_w, nr_pis_pasep_w, cd_segmentacao_ptu_w);
		
	/*aaschlote 05/03/2012 OS 419721 - N�o alterar a data que vem do arquivo, essa data vai ser alterada depois da importa��o, com op��es de bot�o direito
	ptu_atualizar_datas_imp_benef(nr_seq_benef_w,nm_usuario_p);
	*/
	--ptu_definir_benef_imp(nr_seq_benef_w,nm_usuario_p);	
		
elsif	(cd_tipo_registro_w	= '105') then
	select	substr(ds_conteudo_p,12,3)
	into	cd_tipo_cobertura_w
	from	dual;

	dt_fim_carencia_ww	:= substr(ds_conteudo_p,21,2)||substr(ds_conteudo_p,19,2)||substr(ds_conteudo_p,15,4);
	if	(dt_fim_carencia_ww  <> '        ') then
		dt_fim_carencia_w	:= to_date(dt_fim_carencia_ww,'dd/mm/yyyy');
	end if;
	
	select	max(nr_Sequencia)
	into	nr_seq_benef_w
	from	ptu_intercambio_benef;

	select	ptu_beneficiario_carencia_seq.nextval
	into	nr_seq_carencia_w
	from	dual;
	
	insert into ptu_beneficiario_carencia
		( nr_sequencia, nr_seq_beneficiario, cd_tipo_cobertura, 
		dt_fim_carencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec)
	values	(nr_seq_carencia_w, nr_seq_benef_w, cd_tipo_cobertura_w,
		dt_fim_carencia_w, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p);
		
elsif	(cd_tipo_registro_w	= '106') then
	cd_tipo_produto_w	:= trim(substr(ds_conteudo_p,12,2));
	ds_produto_w		:= trim(substr(ds_conteudo_p,14,20));
	
	select	max(nr_Sequencia)
	into	nr_seq_benef_w
	from	ptu_intercambio_benef;

	select	ptu_benef_plano_agregado_seq.nextval
	into	nr_seq_agregado_w
	from	dual;
	
	insert into ptu_benef_plano_agregado
		(nr_sequencia, nr_seq_beneficiario, cd_tipo_produto, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
		nm_usuario_nrec, ds_produto)
	values	(nr_seq_agregado_w, nr_seq_benef_w, cd_tipo_produto_w, 
		sysdate, nm_usuario_p, sysdate, 
		nm_usuario_p,ds_produto_w);

elsif	(cd_tipo_registro_w	= '107') then

	select	decode(to_number(substr(ds_conteudo_p,82,8)),0,'',to_number(substr(ds_conteudo_p,82,8))),
		decode(to_number(substr(ds_conteudo_p,122,4)),0,'',to_number(substr(ds_conteudo_p,122,4))),
		decode(to_number(substr(ds_conteudo_p,134,4)),0,'',to_number(substr(ds_conteudo_p,134,4)))
	into	cd_cep_benef_w,
		nr_ddd_benef_w,
		nr_ramal_w
	from	dual;
	
	ds_endereco_benef_w	:= trim(substr(ds_conteudo_p,12,40));
	ds_bairro_benef_w	:= trim(substr(ds_conteudo_p,52,30));
	nm_municipio_w		:= trim(substr(ds_conteudo_p,90,30));
	sg_uf_benef_w		:= trim(substr(ds_conteudo_p,120,2));
	nr_endereco_benef_w	:= trim(substr(ds_conteudo_p,147,5));
	
	if	(substr(ds_conteudo_p,126,8) = '        ') then
		nr_fone_w := trim(substr(ds_conteudo_p,139,8));
	else
		nr_fone_w := trim(substr(ds_conteudo_p,126,8));
	end if;		
		
	select	max(nr_Sequencia)
	into	nr_seq_benef_w
	from	ptu_intercambio_benef;

	select	ptu_beneficiario_compl_seq.nextval
	into	nr_seq_compl_w
	from	dual;
	
	insert into ptu_beneficiario_compl
		(nr_sequencia, nr_seq_beneficiario, ds_endereco, 
		cd_cep, nm_municipio, sg_uf, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
		nm_usuario_nrec, ds_bairro, nr_ddd, 
		nr_fone, nr_ramal, nr_endereco)
	values	(nr_seq_compl_w, nr_seq_benef_w, ds_endereco_benef_w , 
		cd_cep_benef_w, nm_municipio_w, sg_uf_benef_w,
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, ds_bairro_benef_w, nr_ddd_benef_w,
		nr_fone_w, nr_ramal_w, nr_endereco_benef_w);
elsif	(cd_tipo_registro_w	= '108') then
	cd_cid_w		:= trim(substr(ds_conteudo_p,12,4));

	dt_fim_carencia_preex_ww	:= substr(ds_conteudo_p,22,2)||substr(ds_conteudo_p,20,2)||substr(ds_conteudo_p,16,4);
	if	(dt_fim_carencia_preex_ww <> '        ')	then
		dt_fim_carencia_preex_w	:= to_date(dt_fim_carencia_preex_ww,'dd/mm/yyyy');
	end if;

	select	max(nr_Sequencia)
	into	nr_seq_benef_w
	from	ptu_intercambio_benef;

	select	ptu_benef_preexistencia_seq.nextval
	into	nr_seq_preexistencia_w
	from	dual;
	
	insert into ptu_benef_preexistencia
		( nr_sequencia, nr_seq_beneficiario, cd_cid, 
		dt_fim_carencia, dt_atualizacao, nm_usuario, 
		dt_atualizacao_nrec, nm_usuario_nrec)
	values	(nr_seq_preexistencia_w, nr_seq_benef_w, cd_cid_w,
		dt_fim_carencia_preex_w, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p);

elsif	(cd_tipo_registro_w	= '109') then
	
	qt_benef_intercambio_w	:= to_number(substr(ds_conteudo_p,20,7));
	
	select	max(nr_sequencia)
	into	nr_seq_intercambio_w
	from	ptu_intercambio;
	
	update	ptu_intercambio
	set	qt_registros_lidos	= qt_benef_intercambio_w
	where	nr_sequencia		= nr_seq_intercambio_w;		
end if;

/*exception
when others then
	ds_erro_w := ds_erro_w || cd_tipo_registro_w;
end;*/

commit;

end ptu_imp_intercambio;
/