create or replace 
procedure inativar_atend_perda_ganho
		(nm_usuario_p		varchar2,
		 ds_justificativa_p 	varchar2,
		 nr_sequencia_p		number) is
		 
begin
if	(nr_sequencia_p is not null) then 
	begin
	update	atendimento_perda_ganho
	set	ie_situacao = 'I',
		dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		ds_justificativa = ds_justificativa_p
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;

commit;

end inativar_atend_perda_ganho;
/