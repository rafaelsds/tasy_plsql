create or replace
procedure atualizar_justificativa_avf(nr_seq_nota_item_p     number,   
					ds_justificativa_p     varchar2,
					nr_sequencia_p         number,
					nm_usuario_p		varchar2) is 

begin
update 	avf_resultado_item 
set 	nr_seq_nota_item = nr_seq_nota_item_p, 
	ds_justificativa = ds_justificativa_p 
where 	nr_sequencia = nr_sequencia_p;

commit;

end atualizar_justificativa_avf;
/