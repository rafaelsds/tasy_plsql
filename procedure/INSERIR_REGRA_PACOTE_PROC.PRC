create or replace
procedure inserir_regra_pacote_proc (	nr_seq_secretaria_p	number,
										nm_usuario_p	varchar2,
										nr_seq_pacote_p	number default null) is 

	
nr_seq_pacote_w			lote_ent_secretaria.nr_seq_pacote%type;
nr_seq_ficha_w			lote_ent_sec_ficha.nr_sequencia%type;
nr_seq_exame_lab_w		exame_laboratorio.nr_seq_exame%type;
cd_material_exame_w		material_exame_lab.cd_material_exame%type;
ie_tipo_data_w			lote_ent_tipo_coleta.ie_tipo_data%type;
qt_unidade_w			lote_ent_tipo_coleta.qt_unidade%type;
qt_dif_coleta_w			number(14, 4);
qt_valor_abs_w			number(14, 4);
ie_precoce_w			varchar2(1);
ie_registro_repetido	varchar2(1);

Cursor C01 is
SELECT	nr_sequencia,		
		ROUND(TO_DATE(TO_CHAR(nvl(dt_coleta_ficha_f,sysdate),'dd/mm/yyyy')||' '||TO_CHAR(nvl(hr_coleta_f,sysdate),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') - TO_DATE(TO_CHAR(nvl(dt_nascimento_f,sysdate),'dd/mm/yyyy')||' '||TO_CHAR(nvl(hr_nascimento_f,sysdate),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'), 4)
FROM	lote_ent_sec_ficha
WHERE	nr_seq_lote_sec = nr_seq_secretaria_p;	

Cursor C02 is
	select	substr(lab_obter_cd_mat_exame_lab(b.nr_seq_exame_lab),1,20),		
			b.nr_seq_exame_lab
	from		pacote_procedimento a,
			proc_interno b
	where	a.nr_seq_pacote = nr_seq_pacote_w
	and		a.nr_seq_proc_interno = b.nr_sequencia
	and		b.nr_seq_exame_lab is not null;

begin

if (nr_seq_pacote_p is null) then
	select	max(nr_seq_pacote)
	into	nr_seq_pacote_w
	from	lote_ent_secretaria
	where	nr_sequencia = nr_seq_secretaria_p;
else
	nr_seq_pacote_w := nr_seq_pacote_p;
end if;
	
open c01;
loop
fetch c01 into 	nr_seq_ficha_w, 
				qt_dif_coleta_w;
	exit when c01%notfound;
	begin
			
	open c02;
	loop
	fetch c02 into 	cd_material_exame_w, 
					nr_seq_exame_lab_w;
	exit when c02%notfound;
		begin		
		
		ie_precoce_w := '';
		
		select	max(a.qt_unidade),
				max(a.ie_tipo_data)
		into	qt_unidade_w,
				ie_tipo_data_w		
		from	lote_ent_tipo_coleta a
		where	a.nr_seq_exame = nr_seq_exame_lab_w
		and		ie_tipo_coleta = 'P'
		and		nvl(a.ie_situacao,'A') = 'A';
		
		if	(ie_tipo_data_w is not null) then
			if	(ie_tipo_data_w = 'H') then
				qt_valor_abs_w	:= qt_dif_coleta_w*24;
			else
				qt_valor_abs_w	:= ROUND(qt_dif_coleta_w);
			end if;
			
			if	(qt_valor_abs_w <= qt_unidade_w) then
				ie_precoce_w	:= 'P';
			end if;
		end if;
		
		select	max(a.qt_unidade),
				max(a.ie_tipo_data)
		into	qt_unidade_w,
				ie_tipo_data_w		
		from	lote_ent_tipo_coleta a
		where	a.nr_seq_exame = nr_seq_exame_lab_w
		and		ie_tipo_coleta = 'T'
		and		nvl(a.ie_situacao,'A') = 'A';
		
		if	(ie_tipo_data_w is not null) then
			if	(ie_tipo_data_w = 'H') then
				qt_valor_abs_w	:= qt_dif_coleta_w*24;
			else
				qt_valor_abs_w	:= ROUND(qt_dif_coleta_w);
			end if;
			
			if	(qt_valor_abs_w >= qt_unidade_w) then
				ie_precoce_w	:= 'T';
			end if;
		end if;
		
		select	decode(count(1), 0, 'N', 'S')
		into	ie_registro_repetido
		from	lote_ent_sec_ficha_exam
		where	cd_material_exame	=	cd_material_exame_w and
				nr_seq_exame		=	nr_seq_exame_lab_w and
				nr_seq_ficha		=	nr_seq_ficha_w;

		if (ie_registro_repetido <> 'S') then
			insert into lote_ent_sec_ficha_exam(	nr_sequencia,
								cd_material_exame, 
								dt_atualizacao, 
								dt_atualizacao_nrec, 						
								nm_usuario, 
								nm_usuario_nrec, 
								nr_seq_exame, 
								nr_seq_ficha,
								ie_tipo_coleta)
							values(	lote_ent_sec_ficha_exam_seq.NextVal,
								cd_material_exame_w,
								sysdate,
								sysdate,
								nm_usuario_p,
								nm_usuario_p,
								nr_seq_exame_lab_w,
								nr_seq_ficha_w,
								ie_precoce_w);
		end if;
		end;
	end loop;
	close c02;
	
	end;

end loop;
close c01;

commit;

end inserir_regra_pacote_proc;
/
