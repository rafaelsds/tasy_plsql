create or replace
procedure pat_atualizar_imob_equip(	nr_sequencia_p	number,
			nm_usuario_p		Varchar2) is 
			
cd_imobilizado_w	varchar2(20);
cd_bem_w		varchar2(20);
nr_seq_equipamento_w	varchar2(20);
		
begin

select	max(b.nr_sequencia)
into	nr_seq_equipamento_w
from	man_equipamento b
where	b.nr_seq_bem = nr_sequencia_p;

if	(nr_seq_equipamento_w is not null) then
	
	select	max(a.cd_bem)
	into	cd_bem_w
	from	pat_bem a
	where	a.nr_sequencia	= nr_sequencia_p;
		
	select	max(b.cd_imobilizado)
	into	cd_imobilizado_w
	from	man_equipamento b
	where	b.nr_sequencia = nr_seq_equipamento_w;
	
		
	if	(cd_imobilizado_w <> cd_bem_w) then
		update	man_equipamento
		set	cd_imobilizado	= cd_bem_w,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia 	= nr_seq_equipamento_w;
	end if;	
	
end if;   

commit;

end pat_atualizar_imob_equip;
/