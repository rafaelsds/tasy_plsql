create or replace procedure pfcs_db_tl_req_by_unit_cnt(
    nr_seq_indicator_p   	number,
    cd_estabelecimento_p 	number,
    nm_usuario_p			varchar2) is

nr_seq_panel_w 						pfcs_panel.nr_sequencia%type;

/* Cursor to read from pfcs integration tables */
cursor c01_pfcs is
	select
		sa.ds_setor_atendimento ds_unit,
		count(sr.nr_sequencia) qt_patients_w
	from pfcs_service_request sr,
		pfcs_encounter enc,
		pfcs_patient pat,
		pfcs_encounter_location el,
		unidade_atendimento uni,
        setor_atendimento sa
	where
		sr.si_status = 'ACTIVE'
		and sr.cd_service = 'E0404'
		and sr.nr_seq_encounter = enc.nr_sequencia
		and enc.si_status in ('PLANNED', 'ARRIVED')
		and el.nr_seq_encounter = enc.nr_sequencia
		and enc.nr_seq_patient = pat.nr_sequencia
		and pat.ie_active = '1'
		and pat.nr_sequencia not in
		(
			select dev.nr_seq_patient
			from pfcs_device dev
			where dev.si_status = 'ACTIVE'
				and dev.ds_device_type = 'Monitor'
				and dev.nr_seq_patient is not null
		 )
		and el.nr_seq_encounter = enc.nr_sequencia
        and el.nr_seq_location =  uni.nr_seq_location
        and uni.ie_situacao = 'A'
        and uni.cd_setor_atendimento = sa.cd_setor_atendimento
        and sa.ie_situacao = 'A'
        and sa.cd_classif_setor in ('1','3','4','9','11','12')
        and sa.cd_estabelecimento_base = cd_estabelecimento_p      
	    group by sa.ds_setor_atendimento;

begin
	for c01_w in c01_pfcs loop
	begin
	  pfcs_pck_v2.pfcs_generate_results(
			vl_indicator_p => c01_w.qt_patients_w,
			ds_reference_value_p => c01_w.ds_unit,
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);
	end;
	end loop;
	commit;

	pfcs_pck_v2.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p);

end pfcs_db_tl_req_by_unit_cnt;
/
