create or replace
procedure remover_grupo_list_esp(
	nr_seq_agenda_int_p	number,
	nm_usuario_p	varchar2) is 

nr_seq_age_cons_w	number(10);

Cursor C01 is
	select	a.nr_sequencia
	from	agenda_consulta a
	where	a.nr_seq_agend_coletiva in (	select	b.nr_sequencia
					from	agendamento_coletivo b
					where	b.nr_seq_agenda_int = nr_seq_agenda_int_p);

begin

open C01;
loop
fetch C01 into	
	nr_seq_age_cons_w;
exit when C01%notfound;
	begin
	pep_remover_agenda_list_esp_js(nr_seq_age_cons_w,nm_usuario_p);
	end;
end loop;
close C01;

commit;

end remover_grupo_list_esp;
/