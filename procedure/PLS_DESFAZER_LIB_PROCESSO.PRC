create or replace
procedure pls_desfazer_lib_processo
			(	nr_seq_processo_p	number,
				nm_usuario_p		Varchar2) is 
				
qt_reg_w			number(5);

begin

select	count(*)
into	qt_reg_w
from	pls_req_liminar_judicial 	b,
	pls_requisicao			a
where	a.nr_sequencia		= b.nr_seq_requisicao
and	b.nr_seq_processo	= nr_seq_processo_p;

if	(qt_reg_w = 0) then
	update	processo_judicial_liminar
	set	ie_estagio	= 1,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_liberacao	= null,
		nm_usuario_lib	= null
	where	nr_sequencia	= nr_seq_processo_p;
else
	wheb_mensagem_pck.exibir_mensagem_abort(113991,'');
	--Liminar j� cadastrada para uma requisi��o.
	--N�o � poss�vel desfazer a libera��o!
end if;


commit;

end pls_desfazer_lib_processo;
/
