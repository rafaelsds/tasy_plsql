create or replace
procedure sus_fechar_contas_apac_dt_ocor(nr_apac_p		number,
					nr_sequencia_p		number,
					nr_atendimento_p	number,
					nm_usuario_p		Varchar2) is

nr_interno_conta_w		number(10);

Cursor C01 is
	select	nr_interno_conta
	from	sus_apac_unif
	where	nr_atendimento 	= nr_atendimento_p
	and	nr_apac		= nr_apac_p
	and	nr_sequencia	<> nr_sequencia_p;

begin

open C01;
loop
fetch C01 into
	nr_interno_conta_w;
exit when C01%notfound;
	begin

	update 	conta_paciente
	set	ie_status_acerto	= 2,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_interno_conta	= nr_interno_conta_w
	and	nr_atendimento		= nr_atendimento_p;
	
	recalcular_conta_paciente(nr_interno_conta_w,nm_usuario_p);
	
	end;
end loop;
close C01;

commit;

end sus_fechar_contas_apac_dt_ocor;
/
