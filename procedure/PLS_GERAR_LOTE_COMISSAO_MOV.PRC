create or replace
procedure pls_gerar_lote_comissao_mov
			(	nr_seq_lote_p		number,
				nr_seq_canal_venda_p	number,
				nr_seq_comissao_p	number,				
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is	

dt_referencia_w			date;
qt_meta_w			number(10);
vl_meta_fatur_w			number(15,2);
ie_mes_cobranca_reaj_reg_w	varchar2(1);

/* Campos do cursor 1 */
nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_plano_w			number(10);
ie_tipo_pessoa_w		varchar2(2);
ie_acao_contrato_w		varchar2(2);
dt_contratacao_w		date;
nr_seq_tipo_comercial_w		number(10); 
ie_preco_w			varchar2(2);
nr_seq_proposta_w		number(10);
nr_seq_vendedor_pf_w		pls_comissao_beneficiario.nr_seq_vendedor_pf%type;
nr_proposta_adesao_w		pls_segurado.nr_proposta_adesao%type;
cd_pessoa_fisica_w		pls_segurado.cd_pessoa_fisica%type;
nr_seq_motivo_inclusao_w	pls_regra_adic_mensal.nr_seq_motivo_inclusao%type;

/* Campos do cursor 2 */
pr_incremento_w			number(7,4);
qt_vidas_min_w			number(10);
qt_vidas_max_w			number(10);
vl_incremento_w			number(15,2);
qt_limite_max_vidas_w		number(10);
ie_preco_regra_w		varchar2(2);
pr_meta_maximo_w		number(7,4);
ie_tipo_pessoa_regra_w		varchar2(2);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w		date;
pr_meta_fatur_minimo_w		number(7,4);
pr_meta_fatur_maximo_w		number(7,4);
qt_idade_w			number(10);

/* Campos do cursor 2 e 4 */
nr_seq_regra_w			number(10);
pr_meta_minimo_w		number(7,4);
ie_acao_contrato_regra_w	varchar2(15);
ie_dt_base_ref_comissao_w	varchar2(2);
nr_seq_tipo_benef_comercial_w	number(10);

/* Campos do cursor 3 */
nr_seq_reajuste_w		number(10);
vl_preco_total_w		number(15,2);
qt_dias_contrato_adesao_w	number(10);

/* Campos do cursor 4 */
nr_seq_regra_grupo_w		number(10);
nr_seq_canal_venda_grupo_w	number(10);

ie_gerar_comissao_w		varchar2(1);
qt_vidas_w			number(10);
pr_meta_w			number(10,4);
qt_vidas_incluidas_w		number(10);
vl_comissao_w			number(15,2);
pr_incremento_ww		number(7,4);
qt_tot_vidas_w			number(10);
nr_seq_meta_w			number(10);
vl_comissionamento_w		number(15,2);
vl_total_comissao_w		number(15,2);
pr_total_peso_w			number(7,4);
qt_benef_comissao_w		number(10);
vl_comissao_benef_w		number(15,2);
vl_comissao_total_benef_w	number(15,2);
vl_comissao_total_w		number(15,2);
dt_ref_comissao_w		date;
nr_seq_comissao_benef_w		number(10);
qt_estornos			number(10);
nr_seq_plano_item_w		number(10);
vl_total_item_mens_w		number(15,2);
pr_meta_fatur_w			number(10,4);
vl_reajuste_meta_w		number(15,2);

Cursor C01 is	/* Filtrando benefici�rios por comiss�o */
	select	a.nr_sequencia nr_seq_contrato,
		b.nr_sequencia nr_seq_segurado,	
		c.nr_sequencia nr_seq_plano,
		decode(a.cd_pf_estipulante,null,'PJ','PF'),
		b.ie_acao_contrato,
		b.dt_contratacao,
		b.nr_seq_tipo_comercial,
		c.ie_preco,
		a.nr_seq_proposta,
		b.nr_seq_vendedor_pf,
		b.nr_proposta_adesao,
		b.cd_pessoa_fisica,
		b.nr_seq_motivo_inclusao
	from	pls_plano		c,
		pls_segurado		b,
		pls_contrato		a
	where	a.nr_sequencia 		= b.nr_seq_contrato
	and	b.nr_seq_plano 		= c.nr_sequencia
	and	b.nr_seq_intercambio is null
	and	b.nr_seq_vendedor_canal = nr_seq_canal_venda_p
	and	(b.nr_seq_tipo_comercial = nr_seq_tipo_benef_comercial_w or nvl(nr_seq_tipo_benef_comercial_w,0) = 0)
	and	((trunc(b.dt_contratacao,'month') = trunc(dt_referencia_w,'month')) or
		 (trunc(b.dt_liberacao,'month') = trunc(dt_referencia_w,'month')))	
	union all
	select	a.nr_sequencia nr_seq_contrato,
		b.nr_sequencia nr_seq_segurado,		
		c.nr_sequencia nr_seq_plano,
		decode(a.cd_pf_estipulante,null,'PJ','PF'),
		b.ie_acao_contrato,
		b.dt_contratacao,
		b.nr_seq_tipo_comercial,
		c.ie_preco,
		a.nr_seq_proposta,
		d.nr_seq_vendedor_vinculado,
		b.nr_proposta_adesao,
		b.cd_pessoa_fisica,
		b.nr_seq_motivo_inclusao
	from	pls_segurado_canal_compl	d,
		pls_plano			c,
		pls_segurado			b,
		pls_contrato			a
	where	a.nr_sequencia = b.nr_seq_contrato
	and	b.nr_seq_plano = c.nr_sequencia
	and	b.nr_sequencia = d.nr_seq_segurado
	and	b.nr_seq_intercambio is null
	and	d.nr_seq_vendedor_canal = nr_seq_canal_venda_p
	and	(b.nr_seq_tipo_comercial = nr_seq_tipo_benef_comercial_w or nvl(nr_seq_tipo_benef_comercial_w,0) = 0)
	and	((trunc(b.dt_contratacao,'month') = trunc(dt_referencia_w,'month')) or
		 (trunc(b.dt_liberacao,'month') = trunc(dt_referencia_w,'month')));
		 
Cursor C02 is	/* Filtrando benefici�rios que se encaixam na regra mensal */
	select	a.nr_sequencia,
		pr_incremento,
		qt_vidas_min,
		qt_vidas_max,
		vl_incremento,
		qt_limite_max_vidas,
		ie_preco,
		pr_meta_minimo,
		pr_meta_maximo,
		ie_tipo_pessoa,
		ie_acao_contrato,
		nvl(dt_inicio_vigencia,dt_referencia_w),
		nvl(dt_fim_vigencia,dt_referencia_w),
		decode(ie_dt_base_ref_comissao,null,'A',ie_dt_base_ref_comissao),
		b.nr_seq_tipo_benef_comercial,
		a.pr_meta_fatur_minimo,
		a.pr_meta_fatur_maximo
	from	pls_regra_mensal_benef	b,
		pls_regra_adic_mensal	a
	where	a.nr_sequencia = b.nr_seq_regra_adic(+)
	and	a.nr_seq_vendedor = nr_seq_canal_venda_p
	and	(b.nr_seq_tipo_benef_comercial = nr_seq_tipo_comercial_w or nvl(b.nr_seq_tipo_benef_comercial,0) = 0)
	and	ie_situacao = 'A'
	and	dt_referencia_w	between dt_inicio_vigencia and fim_dia(dt_fim_vigencia)
	and	instr(nvl(ie_acao_contrato,ie_acao_contrato_w),ie_acao_contrato_w) > 0
	and	(nvl(ie_tipo_pessoa,nvl(ie_tipo_pessoa_w,'0')) = nvl(ie_tipo_pessoa_w,'0') or (ie_tipo_pessoa = 'A'))
	and	nvl(ie_preco,nvl(ie_preco_w,'0')) = nvl(ie_preco_w,'0')
	and	ie_movimentacao_benef = 'S'
	and	((a.nr_seq_proposta = nr_seq_proposta_w) or (a.nr_seq_proposta is null))
	and	((a.nr_seq_plano = nr_seq_plano_w) or (nr_seq_plano is null)) 
	and	((a.nr_seq_grupo_produto is not null and nr_seq_plano_w is not null and pls_se_grupo_preco_produto(a.nr_seq_grupo_produto,nr_seq_plano_w) = 'S') 
	or 	(a.nr_seq_grupo_produto is null))
	and	(qt_dias_contrato_adesao_w between nvl(qt_dias_contrato, qt_dias_contrato_adesao_w) and nvl(qt_dias_contrato_fim, qt_dias_contrato_adesao_w))	
	and	qt_idade_w between nvl(a.qt_idade_inicial,qt_idade_w) and nvl(a.qt_idade_final,qt_idade_w)
	and	((a.nr_seq_contrato is null) or (a.nr_seq_contrato = nr_seq_contrato_w))
	and	((a.nr_seq_motivo_inclusao = nr_seq_motivo_inclusao_w) or (a.nr_seq_motivo_inclusao is null))
	order by	nvl(ie_preco,'-1'),
			nvl(ie_tipo_item_mensalidade,'-1'),
			nvl(ie_acao_contrato,'-1');	

Cursor C03 is	/* Filtrando os reajustes */
	select	max(nr_sequencia) nr_seq_reajuste
	from	pls_segurado_preco 
	where	(((trunc(sysdate,'month') >= trunc(dt_reajuste,'month'))
	and	((cd_motivo_reajuste <> 'E') or (ie_mes_cobranca_reaj_reg_w = 'M'))) 
	or	(((cd_motivo_reajuste = 'E') and (trunc(sysdate,'month') >= trunc(add_months(dt_reajuste,1), 'month')))
	and	(ie_mes_cobranca_reaj_reg_w = 'P')))
	and	nr_seq_segurado = nr_seq_segurado_w;	
		
Cursor C04 is 	/* Filtrando vendedores pertencentes ao grupo */
	select	a.nr_sequencia,
		b.nr_seq_canal_venda
	from	pls_canal_regra_mensal	b,
		pls_regra_adic_mensal	a
	where	a.nr_sequencia = b.nr_seq_regra_mensal
	and	a.nr_seq_vendedor = nr_seq_canal_venda_p;
	
begin

pr_incremento_w		:= 0;
qt_vidas_incluidas_w 	:= 0;
qt_tot_vidas_w		:= 0;

select	dt_referencia
into	dt_referencia_w
from	pls_lote_comissao
where	nr_sequencia = nr_seq_lote_p;

begin
select	max(nvl(qt_meta,0)),
	max(nvl(vl_meta_faturamento,0))
into	qt_meta_w,
	vl_meta_fatur_w
from	pls_vendedor_meta
where	trunc(dt_mesano_referencia,'month') = trunc(dt_referencia_w,'month')
and	nr_seq_vendedor	= nr_seq_canal_venda_p;
exception
when others then
	qt_meta_w	:= 0;
	vl_meta_fatur_w	:= 0;
end;

select	nvl(max(ie_mes_cobranca_reaj_reg),'P')
into	ie_mes_cobranca_reaj_reg_w
from	pls_parametros
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

open C01;
loop
fetch C01 into
	nr_seq_contrato_w,
	nr_seq_segurado_w,
	nr_seq_plano_w,
	ie_tipo_pessoa_w,
	ie_acao_contrato_w,
	dt_contratacao_w,
	nr_seq_tipo_comercial_w,
	ie_preco_w,
	nr_seq_proposta_w,
	nr_seq_vendedor_pf_w,
	nr_proposta_adesao_w,
	cd_pessoa_fisica_w,
	nr_seq_motivo_inclusao_w;
exit when C01%notfound;

	nr_seq_regra_w		:= 0;
	ie_gerar_comissao_w	:= 'N';
	
	/*Obter a quantidade de dias entre a data de contrata��o e a data de ades�o do benefici�rio. */
	if	(nr_seq_contrato_w is not null) then
		select	b.dt_contratacao - a.dt_contrato
		into	qt_dias_contrato_adesao_w
		from	pls_segurado	b,
			pls_contrato	a
		where	a.nr_sequencia = b.nr_seq_contrato
		and	b.nr_sequencia = nr_seq_segurado_w;
	end if;	
	
	select	trunc(months_between(dt_referencia_w, b.dt_nascimento) / 12)
	into	qt_idade_w
	from	pls_segurado	a,
		pessoa_fisica	b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_sequencia		= nr_seq_segurado_w;

	open C02;
	loop
	fetch C02 into	
		nr_seq_regra_w,
		pr_incremento_w,
		qt_vidas_min_w,
		qt_vidas_max_w,
		vl_incremento_w,
		qt_limite_max_vidas_w,
		ie_preco_regra_w,
		pr_meta_minimo_w,
		pr_meta_maximo_w,
		ie_tipo_pessoa_regra_w,
		ie_acao_contrato_regra_w,
		dt_inicio_vigencia_w,
		dt_fim_vigencia_w,
		ie_dt_base_ref_comissao_w,
		nr_seq_tipo_benef_comercial_w,
		pr_meta_fatur_minimo_w,
		pr_meta_fatur_maximo_w;
	exit when C02%notfound;
	
		open C03;
		loop
		fetch C03 into	
			nr_seq_reajuste_w;
		exit when C03%notfound;
		
			begin
			select	(vl_preco_atual - nvl(vl_desconto,0)) 
			into	vl_preco_total_w
			from	pls_segurado_preco
			where	nr_sequencia = nr_seq_reajuste_w;
			exception
			when others then
				vl_preco_total_w	:= 0;
			end;
		
			if	(ie_dt_base_ref_comissao_w = 'L') then
				select	dt_liberacao
				into	dt_ref_comissao_w
				from	pls_segurado
				where	nr_sequencia = nr_seq_segurado_w;
			elsif	(ie_dt_base_ref_comissao_w = 'A') then
				select	dt_contratacao
				into	dt_ref_comissao_w
				from	pls_segurado
				where	nr_sequencia = nr_seq_segurado_w;
			elsif	(ie_dt_base_ref_comissao_w = 'AC') then
				select	max(dt_aprovacao)
				into	dt_ref_comissao_w
				from	pls_contrato
				where	nr_sequencia	= nr_seq_contrato_w;
			end if;
			
			if	(trunc(dt_ref_comissao_w,'month') = trunc(dt_referencia_w,'month')) then
				begin
				
				select	sum(qt_vidas),
					sum(vl_reajuste)
				into	qt_vidas_w,
					vl_reajuste_meta_w
				from   (select	count(*) qt_vidas,
						sum(d.vl_preco_atual - nvl(d.vl_desconto,0)) vl_reajuste
					from	pls_segurado_preco	d,
						pls_plano		c,
						pls_segurado		b,		
						pls_contrato		a
					where	a.nr_sequencia = b.nr_seq_contrato
					and	b.nr_seq_plano = c.nr_sequencia
					and	b.nr_sequencia = d.nr_seq_segurado
					and	b.nr_seq_vendedor_canal = nr_seq_canal_venda_p
					and	((ie_dt_base_ref_comissao_w = 'A' and trunc(b.dt_contratacao,'month') = trunc(dt_referencia_w,'month') and (trunc(b.dt_contratacao,'month') between dt_inicio_vigencia_w and dt_fim_vigencia_w)) or
						 (ie_dt_base_ref_comissao_w = 'L' and trunc(b.dt_liberacao,'month') = trunc(dt_referencia_w,'month') and trunc(b.dt_liberacao,'month') between dt_inicio_vigencia_w and dt_fim_vigencia_w) or
						 (ie_dt_base_ref_comissao_w = 'AC' and trunc(a.dt_aprovacao,'month') = trunc(dt_referencia_w,'month') and trunc(a.dt_aprovacao,'month') between dt_inicio_vigencia_w and dt_fim_vigencia_w))
					and	instr(nvl(ie_acao_contrato_regra_w, b.ie_acao_contrato), b.ie_acao_contrato) > 0
					and	((decode(a.cd_pf_estipulante,null,'PJ','PF') = ie_tipo_pessoa_regra_w) or (ie_tipo_pessoa_regra_w = 'A'))
					and	(nvl(nr_seq_tipo_benef_comercial_w,0) = 0 or b.nr_seq_tipo_comercial is not null)
					and	d.nr_sequencia = obter_seq_segurado_preco(b.nr_sequencia)
					union all
					select	count(*) qt_vidas,
						sum(e.vl_preco_atual - nvl(e.vl_desconto,0)) vl_reajuste
					from	pls_segurado_preco		e,
						pls_segurado_canal_compl	d,
						pls_plano			c,
						pls_segurado			b,		
						pls_contrato			a
					where	a.nr_sequencia = b.nr_seq_contrato
					and	b.nr_seq_plano = c.nr_sequencia
					and	b.nr_sequencia = d.nr_seq_segurado
					and	b.nr_sequencia = e.nr_seq_segurado
					and	d.nr_seq_vendedor_canal = nr_seq_canal_venda_p
					and	((ie_dt_base_ref_comissao_w = 'A' and trunc(b.dt_contratacao,'month') = trunc(dt_referencia_w,'month') and (trunc(b.dt_contratacao,'month') between dt_inicio_vigencia_w and dt_fim_vigencia_w)) or
						 (ie_dt_base_ref_comissao_w = 'L' and trunc(b.dt_liberacao,'month') = trunc(dt_referencia_w,'month') and trunc(b.dt_liberacao,'month') between dt_inicio_vigencia_w and dt_fim_vigencia_w) or
						 (ie_dt_base_ref_comissao_w = 'AC' and trunc(a.dt_aprovacao,'month') = trunc(dt_referencia_w,'month') and trunc(a.dt_aprovacao,'month') between dt_inicio_vigencia_w and dt_fim_vigencia_w))
					and	instr(nvl(ie_acao_contrato_regra_w, b.ie_acao_contrato), b.ie_acao_contrato) > 0
					and	((decode(a.cd_pf_estipulante,null,'PJ','PF') = ie_tipo_pessoa_regra_w) or (ie_tipo_pessoa_regra_w = 'A'))
					and	(nvl(nr_seq_tipo_benef_comercial_w,0) = 0 or b.nr_seq_tipo_comercial is not null)
					and	e.nr_sequencia = obter_seq_segurado_preco(b.nr_sequencia));
									
				/* C�lculo pela meta por quantidade de vidas vendidas */
				if	(nvl(qt_meta_w,0) > 0) then
					pr_meta_w := nvl(round((qt_vidas_w * 100) / nvl(qt_meta_w,1),2),0);
				else	
					pr_meta_w := 0;
				end if;
				
				/* C�lculo pela meta por faturamento obtido */
				if	(nvl(vl_meta_fatur_w,0) > 0) then
					pr_meta_fatur_w	:= nvl(round((vl_reajuste_meta_w * 100) / nvl(vl_meta_fatur_w,1),2),0);
				else	
					pr_meta_fatur_w	:= 0;
				end if;	
				
				if	(qt_vidas_w >= nvl(qt_vidas_min_w,qt_vidas_w)) and (qt_vidas_w <= nvl(qt_vidas_max_w,qt_vidas_w)) and
					(pr_meta_w >= nvl(pr_meta_minimo_w,pr_meta_w)) and (pr_meta_w <= nvl(pr_meta_maximo_w,pr_meta_w)) and
					(pr_meta_fatur_w >= nvl(pr_meta_fatur_minimo_w,pr_meta_fatur_w)) and (pr_meta_fatur_w <= nvl(pr_meta_fatur_maximo_w,pr_meta_fatur_w)) then
					ie_gerar_comissao_w	:= 'S';
					
					qt_vidas_incluidas_w := qt_vidas_incluidas_w + 1; /* Realizar a verifica��o de quantas vidas foram inclu�das no repasse para n�o ultrapassar o limite informado no campo "Limite vidas repasse"  */
					pr_incremento_ww     := nvl(pr_incremento_w,0);
					
					if	(qt_vidas_incluidas_w <= nvl(qt_limite_max_vidas_w,qt_vidas_incluidas_w)) then
						vl_comissao_w	:= round(((vl_preco_total_w * nvl(pr_incremento_w,0)) / 100) + nvl(vl_incremento_w,0),2);
					else
						vl_comissao_w	:= 0;
					end if;				
				end if;
				
				/*N�o gerar comiss�o duas vezes para o benefici�rio no mesmo m�s */
				select	count(*)
				into	qt_benef_comissao_w
				from	pls_comissao_beneficiario	c,
					pls_comissao			b,
					pls_lote_comissao		a
				where	a.nr_sequencia = b.nr_seq_lote
				and	b.nr_sequencia = c.nr_seq_comissao
				and	c.nr_seq_segurado = nr_seq_segurado_w
				and	trunc(a.dt_referencia,'month')	= trunc(dt_referencia_w,'month')
				and	nvl(b.ie_cancelado,'N') = 'N';
				
				if	((ie_gerar_comissao_w	= 'S') and (qt_benef_comissao_w = 0)) then
				
					select	pls_comissao_beneficiario_seq.NextVal
					into	nr_seq_comissao_benef_w
					from 	dual;
					
					insert	into pls_comissao_beneficiario
						      ( nr_sequencia, 
							nr_seq_comissao,
							nr_seq_segurado,
							vl_incremento,
							nr_seq_vendedor,
							dt_atualizacao, 
							nm_usuario, 
							dt_atualizacao_nrec, 
							nm_usuario_nrec,
							vl_comissao_benef,
							vl_origem,
							nr_seq_regra_mensal,
							nr_seq_vendedor_pf,
							nr_seq_proposta,
							cd_pessoa_fisica )
						values(	nr_seq_comissao_benef_w,
							nr_seq_comissao_p,
							nr_seq_segurado_w,
							vl_incremento_w,
							nr_seq_canal_venda_p,
							sysdate, 
							nm_usuario_p, 
							sysdate, 
							nm_usuario_p,
							vl_comissao_w,
							vl_comissao_w,
							nr_seq_regra_w,
							nr_seq_vendedor_pf_w,
							nr_proposta_adesao_w,
							cd_pessoa_fisica_w);							
							
					qt_tot_vidas_w	:= qt_tot_vidas_w + 1;
					
				end if;
				
				end;
			end if;
				
		end loop;
		close C03;	
		
	end loop;
	close C02;

end loop;
close C01;

select	sum(vl_comissao_benef)
into	vl_comissao_total_w
from	pls_comissao_beneficiario
where	nr_seq_comissao = nr_seq_comissao_p;

select	count(*) /* Verificando a quantidade de estornos pendentes */
into	qt_estornos
from	pls_estorno_comissao
where	nr_seq_canal_venda = nr_seq_canal_venda_p
and	dt_liberacao is not null
and	nr_seq_comissao_benef is null;

if	(qt_estornos > 0) then
	pls_gerar_comissao_estornada(nr_seq_canal_venda_p, nr_seq_comissao_p, nvl(vl_comissao_total_w,0), nm_usuario_p);
else		
	update	pls_comissao
	set	vl_comissao_canal = nvl(vl_comissao_total_w,0) 
	where	nr_sequencia = nr_seq_comissao_p;
end if;

open C04;
loop
fetch C04 into
	nr_seq_regra_grupo_w,
	nr_seq_canal_venda_grupo_w;
exit when C04%notfound;

	pls_gerar_lote_comis_mov_grupo(nr_seq_lote_p, nr_seq_canal_venda_p, nr_seq_canal_venda_grupo_w, nr_seq_comissao_p, nr_seq_regra_grupo_w, nm_usuario_p, cd_estabelecimento_p);	

end loop;
close C04;	

--commit;

end pls_gerar_lote_comissao_mov;
/