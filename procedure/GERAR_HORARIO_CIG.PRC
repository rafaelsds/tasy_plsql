create or replace
procedure gerar_horario_cig	(nr_seq_glicemia_p	number,
				nr_seq_cig_p		number,
				nm_usuario_p		varchar2) is

nr_prescricao_w	number(14,0);
nr_seq_proced_w	number(6,0);

cd_proced_w		number(15,0);
ie_origem_w		number(10,0);
nr_seq_int_w		number(10,0);
dt_horario_w		date;
nr_seq_hor_w		number(10,0);
ie_liberado_w		varchar2(1);

begin
if	(nr_seq_glicemia_p is not null) and
	(nr_seq_cig_p is not null) and
	(nm_usuario_p is not null) then

	/* consistir prescricao */
	/* OS 83818, gerava hor�rio vinculado a prescri��o antiga e n�o mostrava no grid, substitu�do pelo comando abaixo.
	select	nvl(max(nr_prescricao),0) nr_prescricao,
		nvl(max(nr_seq_procedimento),0) nr_seq_proced
	into	nr_prescricao_w,
		nr_seq_proced_w
	from	atend_glicemia
	where	nr_sequencia = nr_seq_glicemia_p;
	*/
	select	nvl(max(b.nr_prescricao),0) nr_prescricao,
		nvl(max(b.nr_seq_procedimento),0) nr_seq_proced
	into	nr_prescricao_w,
		nr_seq_proced_w
	from	prescr_proc_hor b,
		atendimento_cig a
	where	b.nr_sequencia = a.nr_seq_horario
	and	a.nr_sequencia = nr_seq_cig_p
	and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

	if	(nr_prescricao_w > 0) and
		(nr_seq_proced_w > 0) then

		/* obter dados prescricao */
		select	cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno
		into	cd_proced_w,
			ie_origem_w,
			nr_seq_int_w
		from	prescr_procedimento
		where	nr_prescricao	= nr_prescricao_w
		and	nr_sequencia	= nr_seq_proced_w;

		/* obter dados cig */
		select	dt_proximo_controle
		into	dt_horario_w
		from	atendimento_cig
		where	nr_sequencia = nr_seq_cig_p;
		
		/* Verifica se deve setar a data de libera��o ao novo hor�rio gerado - Hudson OS361201 */
		select	nvl(max('S'),'N')
		into	ie_liberado_w
		from	prescr_proc_hor	
		where	nr_prescricao = nr_prescricao_w
		and	dt_lib_horario is not null;

		/* gerar horario */
		select	prescr_proc_hor_seq.nextval
		into	nr_seq_hor_w
		from	dual;

		insert into prescr_proc_hor	(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_prescricao,
						nr_seq_procedimento,
						cd_procedimento,
						ie_origem_proced,
						nr_seq_proc_interno,
						ds_horario,
						dt_horario,
						nr_ocorrencia,
						ie_urgente,
						dt_fim_horario,
						dt_suspensao,
						ie_horario_especial,
						cd_material_exame,
						nm_usuario_reaprazamento,
						qt_hor_reaprazamento,
						ie_aprazado,
						ie_situacao,
						ie_checagem,
						nm_usuario_checagem,
						ie_dose_especial,
						nm_usuario_dose_esp,
						dt_checagem,
						cd_setor_exec,
						dt_lib_horario
						)
		values				(
						nr_seq_hor_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_prescricao_w,
						nr_seq_proced_w,
						cd_proced_w,
						ie_origem_w,
						nr_seq_int_w,
						to_char(dt_horario_w,'hh24:mi:ss'),
						to_date(to_char(dt_horario_w,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss'),
						1,
						'N',
						null,
						null,
						'N',
						null,
						null,
						null,
						'N',
						'A',
						null,
						null,
						null,
						null,
						null,
						null,						
						DECODE(ie_liberado_w,'N',null,sysdate));

		/* atualizar horario x medicao */
		update	atendimento_cig
		set	nr_seq_proc_hor	= nr_seq_hor_w
		where	nr_sequencia		= nr_seq_cig_p;		

	end if;

end if;

commit;

end gerar_horario_cig;
/