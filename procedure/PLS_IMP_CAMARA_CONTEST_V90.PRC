create or replace
procedure pls_imp_camara_contest_v90
			(	ds_conteudo_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_camara_contest_p		number,
				nr_seq_referencia_p		out number,
				cd_estabelecimento_p		number,
				nr_seq_camara_cont_p	in out	number) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OPS - Controle de Contesta��es
Finalidade: Importar o arquivo A550.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/  
				
-- 551
nr_seq_contestacao_w		Number(10);
dt_geracao_w			date;
cd_unimed_origem_w		number(4);
cd_unimed_destino_w		number(4);
nr_versao_transacao_w		number(2);
nr_id_questionamento_w		number(11);
nr_fatura_head_w		ptu_camara_contestacao.nr_fatura%type;
dt_venc_fatura_w		date;
vl_tot_fatura_w			number(12,2);
vl_tot_contestacao_w		number(12,2);
vl_tot_acordo_w			number(12,2);
ie_tp_arquivo_w			number(2);
vl_total_pago_w			Number(12,2);
cd_unimed_credora_w		number(4);
nr_id_origem_w			number(4);
nr_documento_w			number(11);
dt_venc_doc_w			date;
ie_status_conclusao_w		number(1);
ie_classif_cobranca_a500_w	varchar2(1);
nr_nota_credito_debito_a500_w	ptu_camara_contestacao.nr_nota_credito_debito_a500%type;
dt_vencimento_ndc_a500_w	date;
vl_total_ndc_a500_w		number(15,2);
vl_total_contest_ndc_w		number(15,2);
vl_total_pago_ndc_w		number(15,2);
nr_documento2_w			varchar2(30);
dt_venc_doc2_w			date;
nr_seq_lote_contest_w		number(10);
nr_seq_pls_fatura_w		number(10);
nr_nota_cred_deb_number_w	number(30);
qt_arquivo_w			number(10) := 0;
nr_seq_ptu_fatura_w		number(10);
nm_id_sid_w			ptu_camara_contestacao.ds_sid_processo%type;
nm_id_serial_w			ptu_camara_contestacao.ds_serial_processo%type;
tp_arq_parcial_w		ptu_camara_contestacao.tp_arq_parcial%type;
ie_tipo_arquivo_w		ptu_camara_contestacao.ie_tipo_arquivo%type;

-- 552
nr_seq_questionamento_w		Number(10);
nr_lote_w			Number(8);
nr_nota_w			ptu_questionamento.nr_nota%type;
cd_unimed_w			Number(4);
id_benef_w			varchar2(13);
nm_benef_w			Varchar2(25);
dt_atendimento_w		date; 
ie_tipo_tabela_w		Number(1);
cd_servico_w			Number(8);
vl_cobrado_w			number(12,2);
vl_reconhecido_w		number(12,2);
vl_acordo_w			number(12,2);
dt_acordo_w			date;
ie_tipo_acordo_w		Varchar2(10);
qt_cobrada_w			Number(12,4);
ds_servico_w			Varchar2(255);
nr_seq_a500_w			Number(10);
vl_cobr_co_w			Number(12,2);
vl_reconh_co_w			Number(12,2);
vl_acordo_co_w			Number(12,2);
vl_cobr_filme_w			Number(12,2);
vl_reconh_filme_w		Number(12,2);
vl_acordo_filme_w		Number(12,2);
vl_cobr_adic_serv_w		Number(12,2);
vl_reconh_adic_serv_w		Number(12,2);
vl_acordo_adic_serv_w		Number(12,2);
vl_cobr_adic_co_w		Number(12,2);
vl_reconh_adic_co_w		Number(12,2);
vl_acordo_adic_co_w		Number(12,2);
vl_cobr_adic_filme_w		Number(12,2);
vl_reconh_adic_filme_w		Number(12,2);
vl_acordo_adic_filme_w		Number(12,2);
ie_pacote_w			varchar2(1) := 'N';
cd_pacote_w			varchar2(8);
dt_servico_w			date;
hr_realiz_w			varchar2(8);
qt_acordada_w			number(8);
qt_reconh_w			number(8);
fat_mult_serv_w			number(3,2);
nr_seq_camara_contest_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_nota_servico_w		number(10);
nr_seq_nota_cobranca_w		number(10);

-- 553
nr_seq_ptu_quest_codigo_w	Number(12,2);
cd_motivo_quest_w		Number(4);
ds_motivo_quest_w		Varchar2(500);

nr_seq_arquivo_w		number(8);
nr_seq_conta_proc_w		number(10);
nr_seq_conta_mat_w		number(10);
cd_unimed_operadora_w		varchar2(10);
ie_operacao_w			varchar2(1) := 'E';

-- 557
nr_lote_557_w			ptu_questionamento_rrs.nr_lote%type;
nr_nota_557_w			ptu_questionamento_rrs.nr_nota%type;
cd_unimed_557_w			ptu_questionamento_rrs.cd_unimed%type;
id_benef_557_w			ptu_questionamento_rrs.id_benef%type;
nm_benef_557_w			ptu_questionamento_rrs.nm_beneficiario%type;
dt_reembolso_557_w		ptu_questionamento_rrs.dt_reembolso%type;
nr_cnpj_cpf_557_w		ptu_questionamento_rrs.nr_cnpj_cpf%type;
nm_prestador_557_w		ptu_questionamento_rrs.nm_prestador%type;
nr_seq_quest_rrs_w		ptu_questionamento_rrs.nr_sequencia%type;

-- 558
nr_lote_558_w			ptu_quest_serv_rrs.nr_lote%type;
nr_nota_558_w			ptu_quest_serv_rrs.nr_nota%type;
dt_servico_558_w		ptu_quest_serv_rrs.dt_servico%type;
tp_particip_558_w		ptu_quest_serv_rrs.tp_particip%type;
tp_tabela_558_w			ptu_quest_serv_rrs.tp_tabela%type;
cd_servico_558_w		ptu_quest_serv_rrs.cd_servico%type;
vl_dif_vl_inter_558_w		ptu_quest_serv_rrs.vl_dif_vl_inter%type;
vl_serv_558_w			ptu_quest_serv_rrs.vl_serv%type;
vl_reconh_serv_558_w		ptu_quest_serv_rrs.vl_reconh_serv%type;
vl_acordo_serv_558_w		ptu_quest_serv_rrs.vl_acordo_serv%type;
dt_acordo_558_w			ptu_quest_serv_rrs.dt_acordo%type;
tp_acordo_558_w			ptu_quest_serv_rrs.tp_acordo%type;
qt_cobrada_558_w		ptu_quest_serv_rrs.qt_cobrada%type;
qt_reconh_558_w			ptu_quest_serv_rrs.qt_reconh%type;
qt_acordada_558_w		ptu_quest_serv_rrs.qt_acordada%type;
nm_profissional_558_w		ptu_quest_serv_rrs.nm_profissional%type;
sg_cons_prof_558_w		ptu_quest_serv_rrs.sg_cons_prof%type;
nr_cons_prof_558_w		ptu_quest_serv_rrs.nr_cons_prof%type;
sg_uf_cons_prof_558_w		ptu_quest_serv_rrs.sg_uf_cons_prof%type;
nr_autoriz_558_w		ptu_quest_serv_rrs.nr_autoriz%type;
nr_seq_questionam_reemb_w	ptu_quest_serv_rrs.nr_sequencia%type;
nr_seq_a500_558_w		ptu_quest_serv_rrs.nr_seq_a500%type;
nr_seq_quest_serv_rrs_w		ptu_quest_serv_rrs.nr_sequencia%type;

nr_seq_nota_cobr_rrs_w		ptu_nota_cobranca_rrs.nr_sequencia%type;
nr_seq_nota_serv_rrs_w		ptu_nota_servico_rrs.nr_sequencia%type;

-- HASH
ds_hash_w			ptu_camara_contestacao.ds_hash%type;
ie_novo_imp_a550_w		pls_visible_false.ie_novo_imp_a550%type;

-- 559
qt_tot_r552_w			pls_integer := 0;

qt_arq_fec_compl_w		pls_integer := 0;
nr_seq_lote_disc_w		pls_lote_discussao.nr_sequencia%type;
qt_registro_w			pls_integer := 0;

begin
select	nvl(max(ie_novo_imp_a550),'N')
into	ie_novo_imp_a550_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_p;

-- Se for do jeito antigo
if	(nvl(nr_seq_camara_contest_p,0) > 0) then
	nr_seq_camara_cont_p := nr_seq_camara_contest_p;
end if;

if	(substr(ds_conteudo_p,9,3) = '551') then

	nr_versao_transacao_w	:= to_number(substr(ds_conteudo_p,105,2));
	cd_unimed_destino_w	:= to_number(substr(ds_conteudo_p,12,4));
	cd_unimed_origem_w	:= to_number(substr(ds_conteudo_p,16,4));	-- Prestadora dos servi�os	
	
	begin
	dt_geracao_w		:= to_date(substr(ds_conteudo_p,26,2)||substr(ds_conteudo_p,24,2)||substr(ds_conteudo_p,20,4),'dd/mm/yyyy');
	exception
	when others then
		dt_geracao_w := null;
	end;
	
	cd_unimed_credora_w	:= to_number(substr(ds_conteudo_p,28,4));
	nr_fatura_head_w	:= trim(substr(ds_conteudo_p,222,20));
	
	begin
	dt_venc_fatura_w	:= to_date(substr(ds_conteudo_p,60,2) || substr(ds_conteudo_p,58,2) || substr(ds_conteudo_p,54,4),'dd/mm/yyyy');
	exception
	when others then
		dt_venc_fatura_w := null;
	end;
	
	vl_tot_fatura_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,62,14));
	vl_tot_contestacao_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,76,14));
	
	begin
	vl_tot_acordo_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,90,14));
	exception
	when others then
		vl_tot_acordo_w := 0;
	end;
	
	begin
	ie_classif_cobranca_a500_w := substr(ds_conteudo_p,141,1);
	exception
	when others then
		ie_classif_cobranca_a500_w := null;
	end;
	
	begin
	nr_nota_credito_debito_a500_w := substr(ds_conteudo_p,242,20);
	exception
	when others then
		nr_nota_credito_debito_a500_w := null;
	end;
	
	begin
	nr_nota_cred_deb_number_w := somente_numero(nr_nota_credito_debito_a500_w);
	exception
	when others then
		nr_nota_cred_deb_number_w := null;
	end;
	
	begin
	vl_total_ndc_a500_w := pls_format_valor_imp_ptu(substr(ds_conteudo_p,161,14));
	exception
	when others then
		vl_total_ndc_a500_w := 0;
	end;
	
	begin
	vl_total_contest_ndc_w := pls_format_valor_imp_ptu(substr(ds_conteudo_p,175,14));
	exception
	when others then
		vl_total_contest_ndc_w := 0;
	end;
	
	begin
	vl_total_pago_ndc_w := pls_format_valor_imp_ptu(substr(ds_conteudo_p,189,14));
	exception
	when others then
		vl_total_pago_ndc_w := 0;
	end;
	
	begin
	nr_documento2_w := substr(ds_conteudo_p,203,11);
	exception
	when others then
		nr_documento2_w := null;
	end;
	
	begin
	dt_vencimento_ndc_a500_w := to_date(substr(ds_conteudo_p,159,2)||substr(ds_conteudo_p,157,2)||substr(ds_conteudo_p,153,4),'dd/mm/yyyy');
	exception
	when others then
		dt_vencimento_ndc_a500_w := null;
	end;
	
	begin
	dt_venc_doc2_w := to_date(substr(ds_conteudo_p,220,2)||substr(ds_conteudo_p,218,2)||substr(ds_conteudo_p,214,4),'dd/mm/yyyy');
	exception
	when others then
		dt_venc_doc2_w := null;
	end;
	
	ie_tp_arquivo_w		:= to_number(substr(ds_conteudo_p,104,1));	
	vl_total_pago_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,107,14));
	nr_documento_w		:= to_number(substr(ds_conteudo_p,121,11));	
	
	begin
	dt_venc_doc_w	:= to_date(substr(ds_conteudo_p,145,2)||substr(ds_conteudo_p,143,2)||substr(ds_conteudo_p,139,4),'dd/mm/yyyy');	
	exception
	when others then
		dt_venc_doc_w	:= null;
	end;
	
	ie_status_conclusao_w	:= to_number(substr(ds_conteudo_p,140,1));
	cd_unimed_operadora_w	:= pls_obter_unimed_estab(cd_estabelecimento_p);
	tp_arq_parcial_w	:= substr(ds_conteudo_p,262,1);
	
	if	(lpad(cd_unimed_operadora_w,4,'0') = lpad(cd_unimed_credora_w,4,'0')) and	
		(lpad(cd_unimed_operadora_w,4,'0') <> lpad(cd_unimed_origem_w,4,'0')) then
		ie_operacao_w := 'R'; -- Faturamento
	else
		ie_operacao_w := 'E'; -- Pagamento
	end if;	
	
	if	(nvl(nr_fatura_head_w,'0') <> '0') and
		(nvl(nr_nota_cred_deb_number_w,0) > 0) then -- NDR A500 (Reembolso) e Fatura
		if	(ie_operacao_w = 'R') then -- Faturamento
			select	max(nr_sequencia)
			into	nr_seq_contestacao_w
			from	ptu_camara_contestacao
			where	nr_fatura			= nr_fatura_head_w
			and	nr_nota_credito_debito_a500	= nr_nota_credito_debito_a500_w
			and	lpad(cd_unimed_origem,4,'0')	= lpad(cd_unimed_origem_w,4,'0')
			and	ie_operacao 			= ie_operacao_w;
		else
			select	max(nr_sequencia)
			into	nr_seq_contestacao_w
			from	ptu_camara_contestacao
			where	nr_fatura			= nr_fatura_head_w
			and	nr_nota_credito_debito_a500	= nr_nota_credito_debito_a500_w
			and	lpad(cd_unimed_destino,4,'0')	= lpad(cd_unimed_credora_w,4,'0')
			and	ie_operacao 			= ie_operacao_w;
		end if;
		
		if	(nr_seq_contestacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_pls_fatura_w
			from	pls_fatura	a,
				ptu_fatura	b
			where	a.nr_sequencia		= b.nr_seq_pls_fatura
			and	b.nr_fatura		= nr_fatura_head_w
			and	b.nr_nota_credito_debito= nr_nota_credito_debito_a500_w;
			
			select	max(nr_sequencia)
			into	nr_seq_lote_contest_w
			from	pls_lote_contestacao
			where	nr_seq_pls_fatura = nr_seq_pls_fatura_w;
		end if;
		
	elsif	(nvl(nr_fatura_head_w,'0') <> '0') then -- Fatura
		if	(ie_operacao_w = 'R') then -- Faturamento
			select	max(nr_sequencia)
			into	nr_seq_contestacao_w
			from	ptu_camara_contestacao
			where	nr_fatura			= nr_fatura_head_w
			and	lpad(cd_unimed_origem,4,'0')	= lpad(cd_unimed_origem_w,4,'0')
			and	ie_operacao 			= ie_operacao_w;
		else
			select	max(nr_sequencia)
			into	nr_seq_contestacao_w
			from	ptu_camara_contestacao
			where	nr_fatura			= nr_fatura_head_w
			and	lpad(cd_unimed_destino,4,'0')	= lpad(cd_unimed_credora_w,4,'0')
			and	ie_operacao 			= ie_operacao_w;		
		end if;
		
		if	(nr_seq_contestacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_pls_fatura_w
			from	pls_fatura	a,
				ptu_fatura	b
			where	a.nr_sequencia	= b.nr_seq_pls_fatura
			and	b.nr_fatura	= nr_fatura_head_w;
			
			select	max(nr_sequencia)
			into	nr_seq_lote_contest_w
			from	pls_lote_contestacao
			where	nr_seq_pls_fatura = nr_seq_pls_fatura_w;
		end if;
	
	elsif	(nvl(nr_nota_cred_deb_number_w,0) > 0) then -- NDR A500 (Reembolso)
		if	(ie_operacao_w = 'R') then -- Faturamento
			select	max(nr_sequencia)
			into	nr_seq_contestacao_w
			from	ptu_camara_contestacao
			where	nr_nota_credito_debito_a500	= nr_nota_credito_debito_a500_w
			and	lpad(cd_unimed_origem,4,'0')	= lpad(cd_unimed_origem_w,4,'0')
			and	ie_operacao 			= ie_operacao_w;
		else
			select	max(nr_sequencia)
			into	nr_seq_contestacao_w
			from	ptu_camara_contestacao
			where	nr_nota_credito_debito_a500	= nr_nota_credito_debito_a500_w
			and	lpad(cd_unimed_destino,4,'0')	= lpad(cd_unimed_credora_w,4,'0')
			and	ie_operacao 			= ie_operacao_w;		
		end if;
		
		if	(nr_seq_contestacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_pls_fatura_w
			from	pls_fatura	a,
				ptu_fatura	b
			where	a.nr_sequencia		= b.nr_seq_pls_fatura
			and	b.nr_nota_credito_debito= nr_nota_credito_debito_a500_w;
			
			select	max(nr_sequencia)
			into	nr_seq_lote_contest_w
			from	pls_lote_contestacao
			where	nr_seq_pls_fatura = nr_seq_pls_fatura_w;
		end if;
	end if;	
	
	if	(nr_seq_contestacao_w is not null) and
		(ie_tp_arquivo_w in (1,5,6,7,8)) then
		select	max(nr_seq_lote_contest)
		into	nr_seq_lote_contest_w
		from	ptu_camara_contestacao
		where	nr_sequencia = nr_seq_contestacao_w;
		
		select	max(nr_seq_ptu_fatura)
		into	nr_seq_ptu_fatura_w
		from	pls_lote_contestacao
		where	nr_sequencia = nr_seq_lote_contest_w;
		
		if	(nr_seq_lote_contest_w is not null) then
			select	count(1)
			into	qt_arquivo_w
			from	pls_lote_discussao
			where	nr_seq_lote_contest	= nr_seq_lote_contest_w
			and	ie_tipo_arquivo		= ie_tp_arquivo_w
			and	nvl(ie_status,'A')	<> 'C';
		end if;
		
		if	(qt_arquivo_w > 0) or
			(nr_seq_ptu_fatura_w is not null and ie_tp_arquivo_w = 1) then
			wheb_mensagem_pck.exibir_mensagem_abort(267354,'IE_TIPO_ARQUIVO_W=' || ie_tp_arquivo_w);
		end if;
	end if;
	
	-- Se o tipo de arquivo que est� sendo importado for do tipo 3 ou 4 (Fechamento parcial) ou do tipo 5 ou 6 (Arquivo de fechamento)
	if	(ie_tp_arquivo_w in (3,4,5,6)) then
		-- E se j� tem discuss�es do tipo 7 ou 8 (Fechamento complementar)
		select	count(1)
		into	qt_arq_fec_compl_w
		from	pls_lote_discussao
		where	nr_seq_lote_contest = nr_seq_lote_contest_w
		and	ie_tipo_arquivo in (7,8) -- (Fechamento complementar)
		and	nvl(ie_status,'A') <> 'C';
		
		-- Se tiver discuss�o com o tipo de arquivo complementar exibir mensagem e para o processo
		if	(qt_arq_fec_compl_w > 0) then
			if	(ie_tp_arquivo_w in (3,4)) then
				wheb_mensagem_pck.exibir_mensagem_abort(717186); -- N�o � permitido importar arquivos do tipo fechamento parcial ap�s importar arquivo do tipo fechamento complementar.
			else
				wheb_mensagem_pck.exibir_mensagem_abort(717187); -- N�o � permitido importar arquivos do tipo arquivo de fechamento ap�s importar arquivo do tipo fechamento complementar.
			end if;
		end if;
	end if;
	
	if	(ie_tp_arquivo_w in (7,8)) then
		select	count(1)
		into	qt_arquivo_w
		from	pls_lote_discussao
		where	nr_seq_lote_contest = nr_seq_lote_contest_w
		and	ie_tipo_arquivo in (3,4,9) -- 3 e 4 arquivo parcial, 9 � arquivo parcial que entrou em decurso de prazo
		and	nvl(ie_status,'A') <> 'C';
		
		if	(qt_arquivo_w = 0) then
			--Para importar um arquivo complementar de fechamento deve-se antes ser importado arquivo de fechamento parcial.
			wheb_mensagem_pck.exibir_mensagem_abort(658483);
		end if; 
	end if;
	
	-- Verificar se deve atualizar o campo NR_SEQ_A500 dos servi�os
	-- Este campo � de extrema importancia para identifica��o dos servi�os na rotina "PTU_RECONH_ITENS_CONTEST_A550"
	if	(nr_seq_pls_fatura_w is not null) then
		pls_atualizar_seq_a500( nr_seq_pls_fatura_w, 'N');
	end if;
	
	select	sid,
		serial#
	into	nm_id_sid_w,
		nm_id_serial_w
	from	v$session
	where 	audsid = userenv('SESSIONID');
	
	if	(nr_seq_contestacao_w is null) then	
		select	ptu_camara_contestacao_seq.nextval
		into	nr_seq_contestacao_w
		from	dual;
		
		if	(ie_novo_imp_a550_w = 'S') then
			nr_seq_camara_cont_p := nr_seq_contestacao_w;
			-- Iniciar importa��o A550
			pls_gerar_log_imp_a550( nr_seq_camara_cont_p, 'IA', 'PR', nm_usuario_p, sysdate, null, null, ie_tp_arquivo_w, 'S');
		end if;
		
		insert into ptu_camara_contestacao
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, cd_unimed_destino,
			cd_unimed_origem, dt_geracao, nr_fatura,
			vl_total_fatura, nr_versao_transacao, cd_estabelecimento,
			cd_unimed_credora, dt_venc_fatura, vl_total_contestacao,
			vl_total_acordo, ie_tipo_arquivo, vl_total_pago,
			ie_operacao, nr_documento, dt_venc_doc,		
			ie_conclusao,ie_classif_cobranca_a500,nr_nota_credito_debito_a500,
			dt_vencimento_ndc_a500,vl_total_ndc_a500,vl_total_contest_ndc,
			vl_total_pago_ndc,nr_documento2,dt_venc_doc2,
			nr_seq_lote_contest,ie_status_imp,ds_sid_processo,
			ds_serial_processo, tp_arq_parcial)
		values	(nr_seq_contestacao_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, cd_unimed_destino_w, 
			cd_unimed_origem_w, dt_geracao_w, nr_fatura_head_w,
			vl_tot_fatura_w, nr_versao_transacao_w, cd_estabelecimento_p,
			cd_unimed_credora_w, dt_venc_fatura_w, vl_tot_contestacao_w,
			vl_tot_acordo_w, ie_tp_arquivo_w, vl_total_pago_w,
			'R', nr_documento_w, dt_venc_doc_w,
			ie_status_conclusao_w,ie_classif_cobranca_a500_w,nr_nota_credito_debito_a500_w,
			dt_vencimento_ndc_a500_w,vl_total_ndc_a500_w,vl_total_contest_ndc_w,
			vl_total_pago_ndc_w,nr_documento2_w,dt_venc_doc2_w,
			nr_seq_lote_contest_w,'EI',nm_id_sid_w,
			nm_id_serial_w, tp_arq_parcial_w);
	else
		if	(ie_novo_imp_a550_w = 'S') then
			nr_seq_camara_cont_p := nr_seq_contestacao_w;
			-- Iniciar importa��o A550
			pls_gerar_log_imp_a550( nr_seq_camara_cont_p, 'IA', 'PR', nm_usuario_p, sysdate, null, null, ie_tp_arquivo_w, 'S');
		end if;
		
		-- Caso esteja recebendo novamente
		update	ptu_camara_contestacao
		set	vl_total_acordo		= vl_tot_acordo_w,
			vl_total_pago		= vl_total_pago_w,
			vl_total_contestacao	= vl_tot_contestacao_w,
			ie_tipo_arquivo		= ie_tp_arquivo_w,
			ds_sid_processo		= nm_id_sid_w,
			ds_serial_processo	= nm_id_serial_w,
			tp_arq_parcial		= tp_arq_parcial_w,
			nr_versao_transacao	= nr_versao_transacao_w
		where	nr_sequencia		= nr_seq_contestacao_w;
		
		-- Retorna o lote que foi atualizado
		nr_seq_referencia_p := nr_seq_contestacao_w;
	end if;
		 
elsif	(substr(ds_conteudo_p,9,3) = '552') then
	select	ptu_questionamento_seq.nextval
	into	nr_seq_questionamento_w
	from	dual;
	
	-- Obter sequencia da contestacao da qual o questionamento ira ser vinculado
	if	(ie_novo_imp_a550_w = 'N') then
		select	max(nr_sequencia),
			max(ie_operacao)
		into	nr_seq_contestacao_w,
			ie_operacao_w
		from	ptu_camara_contestacao
		where	nm_usuario_nrec = nm_usuario_p;
	else
		select	max(nr_sequencia),
			max(ie_operacao)
		into	nr_seq_contestacao_w,
			ie_operacao_w
		from	ptu_camara_contestacao
		where	nr_sequencia = nr_seq_camara_cont_p;
	end if;

	nr_lote_w		:= to_number(substr(ds_conteudo_p,12,8));
	nr_nota_w		:= trim(substr(ds_conteudo_p,633,20));
	
	if	(nr_nota_w = '0') then
		nr_nota_w := null;
	end if;
	
	cd_unimed_w		:= to_number(substr(ds_conteudo_p,35,4));
	id_benef_w		:= substr(ds_conteudo_p,39,13);
	nm_benef_w		:= to_char(substr(ds_conteudo_p,52,25));	
	
	begin
	dt_atendimento_w	:= to_date(substr(ds_conteudo_p,85,2)||substr(ds_conteudo_p,82,2)||substr(ds_conteudo_p,77,4)||substr(ds_conteudo_p,87,8),'dd/mm/yyyy hh24:mi:ss');	
	exception
	when others then
		dt_atendimento_w := null;
	end;	
	
	ie_tipo_tabela_w	:= to_number(substr(ds_conteudo_p,222,1));
	cd_servico_w		:= to_number(substr(ds_conteudo_p,223,8));
	vl_cobrado_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,231,14));
	vl_reconhecido_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,245,14));
	vl_acordo_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,259,14));
	ie_tipo_acordo_w	:= to_char(substr(ds_conteudo_p,281,2));
	
	begin
	qt_reconh_w	:= to_number(substr(ds_conteudo_p,589,8));
	exception
	when others then
		qt_reconh_w	:= null;
	end;
	
	ie_pacote_w		:= nvl(trim(substr(ds_conteudo_p,597,1)),'N');
	cd_pacote_w		:= to_char(substr(ds_conteudo_p,598,8));
	
	begin
	dt_servico_w	:= to_date(substr(ds_conteudo_p,612,2)||substr(ds_conteudo_p,610,2)||substr(ds_conteudo_p,606,4),'dd/mm/yyyy');
	exception
	when others then
		dt_servico_w := null;
	end;
	
	hr_realiz_w		:= to_char(substr(ds_conteudo_p,614,8));
	
	begin
	qt_acordada_w	:= to_number(substr(ds_conteudo_p,622,8));
	exception
	when others then
		qt_acordada_w	:= null;
	end;
	
	begin
	fat_mult_serv_w	:= to_number(substr(ds_conteudo_p,630,3))/100;
	exception
	when others then
		fat_mult_serv_w	:= 0;
	end;
	
	begin
	dt_acordo_w	:= to_date(substr(ds_conteudo_p,279,2)||substr(ds_conteudo_p,277,2)||substr(ds_conteudo_p,273,4),'dd/mm/yyyy');
	exception
	when others then
		dt_acordo_w := null;
	end;
	
	qt_cobrada_w		:= to_number(substr(ds_conteudo_p,283,8))/10000;
	ds_servico_w		:= to_char(substr(ds_conteudo_p,291,80));
	nr_seq_a500_w		:= to_number(substr(ds_conteudo_p,371,8));
	vl_cobr_co_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,379,14));
	vl_reconh_co_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,393,14));
	vl_acordo_co_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,407,14));
	vl_cobr_filme_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,421,14));
	vl_reconh_filme_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,435,14));
	vl_acordo_filme_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,449,14));
	vl_cobr_adic_serv_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,463,14));
	vl_reconh_adic_serv_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,477,14));
	vl_acordo_adic_serv_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,491,14));
	vl_cobr_adic_co_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,505,14));
	vl_reconh_adic_co_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,519,14));
	vl_acordo_adic_co_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,533,14));
	vl_cobr_adic_filme_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,547,14));
	vl_reconh_adic_filme_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,561,14));
	vl_acordo_adic_filme_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,575,14));	
	nr_seq_arquivo_w	:= to_number(substr(ds_conteudo_p,1,8));
	nr_seq_conta_proc_w	:= null;
	nr_seq_conta_mat_w	:= null;
	nr_seq_nota_servico_w	:= null;
	nr_seq_conta_w		:= null;
	nr_seq_nota_cobranca_w	:= null;
	
	select	max(nr_sequencia)
	into	nr_seq_camara_contest_w
	from	ptu_questionamento
	where	nr_seq_contestacao 	= nr_seq_camara_cont_p
	and	nr_seq_a500		= nr_seq_a500_w;
	
	if	(nr_seq_camara_contest_w is null) then
		insert into ptu_questionamento
			(nr_sequencia, nr_nota, cd_usuario_plano,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, ie_tipo_tabela, dt_acordo,
			ie_tipo_acordo, vl_acordo, cd_unimed,
			dt_atendimento, cd_servico, vl_cobrado, 
			vl_reconhecido, nm_beneficiario, nr_seq_contestacao, 
			nr_lote, qt_cobrada, ds_servico, nr_seq_a500, 
			vl_cobr_co, vl_reconh_co, vl_acordo_co, 
			vl_cobr_filme, vl_reconh_filme, vl_acordo_filme, 
			vl_cobr_adic_serv, vl_reconh_adic_serv, vl_acordo_adic_serv, 
			vl_cobr_adic_co, vl_reconh_adic_co, vl_acordo_adic_co, 
			vl_cobr_adic_filme, vl_reconh_adic_filme, vl_acordo_adic_filme,
			nr_seq_conta_proc,nr_seq_conta_mat,ie_pacote,
			cd_pacote,dt_servico,hr_realiz,
			qt_acordada,fat_mult_serv,nr_seq_conta,
			nr_seq_nota_servico, nr_seq_nota_cobranca, qt_reconh )
		values	(nr_seq_questionamento_w, nr_nota_w, id_benef_w,
			sysdate, nm_usuario_p, sysdate, 
			nm_usuario_p, ie_tipo_tabela_w, dt_acordo_w,
			ie_tipo_acordo_w, vl_acordo_w, cd_unimed_w,
			dt_atendimento_w, cd_servico_w, vl_cobrado_w, 
			vl_reconhecido_w, nm_benef_w, nr_seq_contestacao_w, 
			nr_lote_w, qt_cobrada_w, ds_servico_w, nr_seq_a500_w,
			vl_cobr_co_w, vl_reconh_co_w,	vl_acordo_co_w, 
			vl_cobr_filme_w, vl_reconh_filme_w, vl_acordo_filme_w, 
			vl_cobr_adic_serv_w, vl_reconh_adic_serv_w, vl_acordo_adic_serv_w, 
			vl_cobr_adic_co_w, vl_reconh_adic_co_w, vl_acordo_adic_co_w, 
			vl_cobr_adic_filme_w, vl_reconh_adic_filme_w, vl_acordo_adic_filme_w,
			nr_seq_conta_proc_w, nr_seq_conta_mat_w,ie_pacote_w,
			cd_pacote_w,dt_servico_w,hr_realiz_w,
			qt_acordada_w,fat_mult_serv_w,nr_seq_conta_w,
			nr_seq_nota_servico_w, nr_seq_nota_cobranca_w, qt_reconh_w );

		if	(ie_operacao_w = 'R') then
			ptu_reconh_itens_contest_a550( nr_seq_questionamento_w, nr_seq_conta_w, nr_seq_conta_proc_w, nr_seq_conta_mat_w, nr_seq_nota_servico_w, nr_seq_nota_cobranca_w);
			
			update	ptu_questionamento
			set	nr_seq_conta		= nr_seq_conta_w,
				nr_seq_conta_proc	= nr_seq_conta_proc_w,
				nr_seq_conta_mat	= nr_seq_conta_mat_w,
				nr_seq_nota_servico	= nr_seq_nota_servico_w,
				nr_seq_nota_cobranca	= nr_seq_nota_cobranca_w
			where 	nr_sequencia		= nr_seq_questionamento_w
			and	nr_seq_a500		= nr_seq_a500_w;
		end if;
	else		
		-- Caso esteja recebendo novamente
		update	ptu_questionamento
		set	vl_acordo		= vl_acordo_w,
			vl_acordo_adic_co	= vl_acordo_adic_co_w,
			vl_acordo_adic_filme	= vl_acordo_adic_filme_w,
			vl_acordo_adic_serv	= vl_acordo_adic_serv_w,
			vl_acordo_co		= vl_acordo_co_w,
			vl_acordo_filme		= vl_acordo_filme_w,
			vl_reconh_adic_co	= vl_reconh_adic_co_w,
			vl_reconh_adic_filme	= vl_reconh_adic_filme_w,
			vl_reconh_adic_serv     = vl_reconh_adic_serv_w,
			vl_reconh_co            = vl_reconh_co_w,
			vl_reconhecido         	= vl_reconhecido_w,
			vl_reconh_filme         = vl_reconh_filme_w,
			dt_acordo		= dt_acordo_w,
			ie_tipo_acordo 		= ie_tipo_acordo_w,
			qt_acordada		= qt_acordada_w,
			qt_reconh		= qt_reconh_w,
			qt_cobrada		= qt_cobrada_w
		where	nr_sequencia		= nr_seq_camara_contest_w
		and	nr_seq_a500		= nr_seq_a500_w;
		
		-- Retorna o lote que foi atualizado
		nr_seq_referencia_p := nr_seq_camara_cont_p;
	end if;
	
	update	ptu_camara_contestacao
	set	ie_tipo_arquivo_cob	= '502'
	where	nr_sequencia		= nr_seq_contestacao_w;
		 
elsif	(substr(ds_conteudo_p,9,3) = '553') then
	-- Obter sequencia da contestacao da qual o questionamento ira ser vinculado
	if	(ie_novo_imp_a550_w = 'N') then
		select	max(nr_sequencia),
			max(ie_operacao)
		into	nr_seq_contestacao_w,
			ie_operacao_w
		from	ptu_camara_contestacao
		where	nm_usuario_nrec = nm_usuario_p;	

	else
		select	max(nr_sequencia),
			max(ie_operacao)
		into	nr_seq_contestacao_w,
			ie_operacao_w
		from	ptu_camara_contestacao
		where	nr_sequencia = nr_seq_camara_cont_p;	
	end if;

	cd_motivo_quest_w	:= pls_obter_seq_cd_quest(to_number(substr(ds_conteudo_p,12,4)));
	ds_motivo_quest_w	:= to_char(substr(ds_conteudo_p,16,500));
	
	if	(ie_novo_imp_a550_w = 'N') then
		select	max(nr_sequencia)
		into	nr_seq_questionamento_w	
		from	ptu_questionamento
		where 	nm_usuario_nrec = nm_usuario_p;
		
		qt_registro_w := 1;
		if	(nvl(nr_seq_camara_contest_p,0) = 0) then
			qt_registro_w := 0;
		end if;	
	else
		select	max(nr_sequencia)
		into	nr_seq_questionamento_w
		from	ptu_questionamento
		where	nr_seq_contestacao 	= nr_seq_contestacao_w;
		
		select	count(1)
		into	qt_registro_w
		from	ptu_questionamento_codigo
		where	nr_seq_registro			= nr_seq_questionamento_w
		and	nr_seq_mot_questionamento	= cd_motivo_quest_w;
	end if;
	
	if	(nr_seq_questionamento_w is not null) and
		(qt_registro_w = 0) then
		insert into ptu_questionamento_codigo
			(nr_sequencia, 
			dt_atualizacao, 
			nm_usuario, 
			dt_atualizacao_nrec, 
			ds_motivo, 
			nm_usuario_nrec, 
			nr_seq_registro, 
			nr_seq_mot_questionamento, 
			ds_parecer_glosa,
			nr_seq_contestacao )
		values	(ptu_questionamento_codigo_seq.nextval, 
			sysdate, 
			nm_usuario_p, 
			sysdate, 
			ds_motivo_quest_w, 
			nm_usuario_p, 
			nr_seq_questionamento_w, 
			cd_motivo_quest_w, 
			ds_motivo_quest_w,
			nr_seq_contestacao_w );
	end if;
	
	nr_seq_referencia_p := nr_seq_camara_cont_p;

elsif	(substr(ds_conteudo_p,9,3) = '557') then
	-- Obter sequencia da contestacao da qual o questionamento ira ser vinculado
	if	(ie_novo_imp_a550_w = 'N') then
		select	max(nr_sequencia),
			max(ie_operacao)
		into	nr_seq_contestacao_w,
			ie_operacao_w
		from	ptu_camara_contestacao
		where	nm_usuario_nrec = nm_usuario_p;
	else
		select	max(nr_sequencia),
			max(ie_operacao)
		into	nr_seq_contestacao_w,
			ie_operacao_w
		from	ptu_camara_contestacao
		where	nr_sequencia = nr_seq_camara_cont_p;
	end if;

	nr_lote_557_w		:= to_number(substr(ds_conteudo_p,12,8));
	nr_nota_557_w		:= trim(substr(ds_conteudo_p,20,20));
	cd_unimed_557_w		:= to_number(substr(ds_conteudo_p,40,4));
	id_benef_557_w		:= substr(ds_conteudo_p,44,13);
	nm_benef_557_w		:= to_char(substr(ds_conteudo_p,57,25));	
	
	begin
	dt_reembolso_557_w	:= to_date(substr(ds_conteudo_p,88,2)||substr(ds_conteudo_p,86,2)||substr(ds_conteudo_p,82,4),'dd/mm/yyyy');
	exception
	when others then
		dt_reembolso_557_w := null;
	end;
	
	nr_cnpj_cpf_557_w	:= trim(substr(ds_conteudo_p,90,14));
	nm_prestador_557_w	:= trim(substr(ds_conteudo_p,104,70));
	
	select	max(nr_sequencia)
	into	nr_seq_quest_rrs_w
	from	ptu_questionamento_rrs
	where	nr_seq_contestacao 	= nr_seq_camara_cont_p
	and	nr_lote			= nr_lote_557_w
	and	nr_nota			= nr_nota_557_w
	and	id_benef		= id_benef_557_w;
	
	if	(nr_seq_quest_rrs_w is null) then
		insert into ptu_questionamento_rrs
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_contestacao,
			cd_unimed,
			id_benef,
			nm_beneficiario,
			dt_reembolso,
			nr_cnpj_cpf,
			nm_prestador,
			nr_seq_conta,
			nr_seq_nota_cobr_rrs,
			nr_lote,
			nr_nota,
			nr_nota_numerico)
		values	(ptu_questionamento_rrs_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_contestacao_w,
			cd_unimed_557_w,
			id_benef_557_w,
			nm_benef_557_w,
			dt_reembolso_557_w,
			nr_cnpj_cpf_557_w,
			nm_prestador_557_w,
			null,
			null,
			nr_lote_557_w,
			nr_nota_557_w,
			null) returning nr_sequencia into nr_seq_quest_rrs_w;
			
		update	ptu_camara_contestacao
		set	ie_tipo_arquivo_cob	= '507'
		where	nr_sequencia		= nr_seq_contestacao_w;
		
		-- Vincular os motivos de questionamento
		update	ptu_questionamento_codigo
		set	nr_seq_registro_rrs	= nr_seq_quest_rrs_w
		where	nr_seq_contestacao	= nr_seq_contestacao_w
		and	nm_usuario_nrec 	= nm_usuario_p
		and	nr_seq_registro 	is null;
	end if;
	
	nr_seq_referencia_p := nr_seq_camara_cont_p;
	
elsif	(substr(ds_conteudo_p,9,3) = '558') then
	nr_lote_558_w		:= to_number(substr(ds_conteudo_p,12,8));
	nr_nota_558_w		:= trim(substr(ds_conteudo_p,20,20));
	nr_seq_a500_558_w	:= to_number(substr(ds_conteudo_p,40,8));	
	
	begin
	dt_servico_558_w	:= to_date(substr(ds_conteudo_p,54,2)||substr(ds_conteudo_p,52,2)||substr(ds_conteudo_p,48,4),'dd/mm/yyyy');
	exception
	when others then
		dt_servico_558_w := null;
	end;

	tp_particip_558_w	:= trim(substr(ds_conteudo_p,56,1));
	tp_tabela_558_w		:= to_number(substr(ds_conteudo_p,57,1));
	cd_servico_558_w	:= to_number(substr(ds_conteudo_p,58,8));
	vl_serv_558_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,66,14));
	vl_dif_vl_inter_558_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,80,14));
	vl_reconh_serv_558_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,94,14));
	vl_acordo_serv_558_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,108,14));
	
	begin
	dt_acordo_558_w		:= to_date(substr(ds_conteudo_p,128,2)||substr(ds_conteudo_p,126,2)||substr(ds_conteudo_p,122,4),'dd/mm/yyyy');
	exception
	when others then
		dt_acordo_558_w := null;
	end;
	
	tp_acordo_558_w		:= to_char(substr(ds_conteudo_p,130,2));
	qt_cobrada_558_w	:= to_number(substr(ds_conteudo_p,132,8))/10000;
	qt_reconh_558_w		:= to_number(substr(ds_conteudo_p,140,8))/10000;
	qt_acordada_558_w	:= to_number(substr(ds_conteudo_p,148,8))/10000;
	sg_cons_prof_558_w	:= trim(substr(ds_conteudo_p,196,12));
	nr_cons_prof_558_w	:= trim(substr(ds_conteudo_p,208,15));
	sg_uf_cons_prof_558_w	:= trim(substr(ds_conteudo_p,223,2));
	nr_autoriz_558_w	:= to_number(substr(ds_conteudo_p,225,10));
	nm_profissional_558_w	:= trim(substr(ds_conteudo_p,235,70));
	
	select	max(nr_sequencia)
	into	nr_seq_quest_rrs_w
	from	ptu_questionamento_rrs
	where	nr_seq_contestacao 	= nr_seq_camara_cont_p
	and	nr_lote			= nr_lote_558_w
	and	nr_nota			= nr_nota_558_w;
	
	select	max(s.nr_sequencia)
	into	nr_seq_quest_serv_rrs_w
	from	ptu_questionamento_rrs	c,
		ptu_quest_serv_rrs	s
	where	c.nr_sequencia		= s.nr_seq_quest_rrs
	and	c.nr_seq_contestacao 	= nr_seq_camara_cont_p
	and	s.nr_seq_a500		= nr_seq_a500_558_w;

	if	(nr_seq_quest_serv_rrs_w is null) then
		insert into ptu_quest_serv_rrs
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_quest_rrs,
			nr_lote,
			nr_nota,
			nr_seq_a500,
			tp_particip,
			tp_tabela,
			cd_servico,
			dt_servico,
			vl_dif_vl_inter,
			vl_serv,
			vl_reconh_serv,
			vl_acordo_serv,
			tp_acordo,
			qt_cobrada,
			qt_reconh,
			qt_acordada,
			nm_profissional,
			dt_acordo,
			sg_cons_prof,
			nr_cons_prof,
			sg_uf_cons_prof,
			nr_autoriz,
			nr_seq_nota_serv_rrs,
			nr_seq_conta_mat,
			nr_seq_conta_proc,
			nr_nota_numerico)
		values	(ptu_quest_serv_rrs_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_quest_rrs_w,
			nr_lote_558_w,
			nr_nota_558_w,
			nr_seq_a500_558_w,
			tp_particip_558_w,
			tp_tabela_558_w,
			cd_servico_558_w,
			dt_servico_558_w,
			vl_dif_vl_inter_558_w,
			vl_serv_558_w,
			vl_reconh_serv_558_w,
			vl_acordo_serv_558_w,
			tp_acordo_558_w,
			qt_cobrada_558_w,
			qt_reconh_558_w,
			qt_acordada_558_w,
			nm_profissional_558_w,
			dt_acordo_558_w,
			sg_cons_prof_558_w,
			nr_cons_prof_558_w,
			sg_uf_cons_prof_558_w,
			nr_autoriz_558_w,
			null,
			null,
			null,
			null) returning nr_sequencia into nr_seq_quest_serv_rrs_w;
			
		if	(ie_operacao_w = 'R') then
			ptu_reconh_contest_reemb_a550( nr_seq_quest_serv_rrs_w, nr_seq_conta_w, nr_seq_conta_proc_w, nr_seq_conta_mat_w, nr_seq_nota_cobr_rrs_w, nr_seq_nota_serv_rrs_w);
			
			update	ptu_quest_serv_rrs
			set	nr_seq_conta_proc	= nr_seq_conta_proc_w,
				nr_seq_conta_mat	= nr_seq_conta_mat_w,
				nr_seq_nota_serv_rrs	= nr_seq_nota_serv_rrs_w
			where	nr_sequencia		= nr_seq_quest_serv_rrs_w
			and	nr_seq_a500		= nr_seq_a500_558_w;
			
			update	ptu_questionamento_rrs
			set	nr_seq_conta		= nr_seq_conta_w,
				nr_seq_nota_cobr_rrs	= nr_seq_nota_cobr_rrs_w
			where	nr_sequencia		= nr_seq_quest_rrs_w;
		end if;
			
	else
		-- Caso esteja recebendo novamente
		update	ptu_quest_serv_rrs
		set	dt_acordo		= dt_acordo_558_w,
			qt_acordada		= qt_acordada_558_w,
			qt_cobrada		= qt_cobrada_558_w,
			qt_reconh		= qt_reconh_558_w,
			tp_acordo		= tp_acordo_558_w,
			vl_acordo_serv		= vl_acordo_serv_558_w,
			vl_dif_vl_inter		= vl_dif_vl_inter_558_w,
			vl_reconh_serv		= vl_reconh_serv_558_w
		where	nr_sequencia		= nr_seq_quest_serv_rrs_w
		and	nr_seq_a500		= nr_seq_a500_558_w;
		
		nr_seq_referencia_p := nr_seq_camara_cont_p;	
	end if;
	
elsif	(substr(ds_conteudo_p,9,3) = '559') then
	if	(ie_novo_imp_a550_w = 'N') then
		select	max(nr_sequencia)
		into	nr_seq_contestacao_w	
		from	ptu_camara_contestacao
		where 	nm_usuario_nrec	= nm_usuario_p
		and	ie_operacao	= 'R'
		and	qt_tot_r552 is null;
	else
		select	max(nr_sequencia)
		into	nr_seq_contestacao_w	
		from	ptu_camara_contestacao
		where 	nr_sequencia	= nr_seq_camara_cont_p
		and	ie_operacao	= 'R'
		and	qt_tot_r552 is null;
	end if;
	
	qt_tot_r552_w := somente_numero(substr(ds_conteudo_p,12,5));
	
	if	(nr_seq_contestacao_w is not null) then
		update	ptu_camara_contestacao
		set	qt_tot_r552	= qt_tot_r552_w
		where	nr_sequencia	= nr_seq_contestacao_w;	
	end if;
		
	nr_seq_referencia_p := nr_seq_camara_cont_p;
	
elsif	(substr(ds_conteudo_p,9,3) = '998') then
	ds_hash_w := trim(substr(ds_conteudo_p,12,32));

	if	(nvl(nr_seq_camara_cont_p,0) > 0) or
		(nr_seq_camara_cont_p is not null) then		
		update	ptu_camara_contestacao
		set	ds_hash		= ds_hash_w
		where	nr_sequencia	= nr_seq_camara_cont_p;
		commit;
		
		select	max(ld.nr_sequencia)
		into	nr_seq_lote_disc_w
		from	ptu_camara_contestacao	cc,
			pls_lote_contestacao	lc,
			pls_lote_discussao	ld
		where	lc.nr_sequencia	= cc.nr_seq_lote_contest
		and	lc.nr_sequencia	= ld.nr_seq_lote_contest
		and	cc.nr_sequencia	= nr_seq_camara_cont_p
		and	ld.ds_hash_a550	= ds_hash_w
		and	ld.ie_status	!= 'C';
		
		if	(nr_seq_lote_disc_w is not null) then
			-- N�o foi poss�vel importar este arquivo, ele j� foi importado gerando o lote de discuss�o #@NR_SEQ_LOTE_DISC#@.
			wheb_mensagem_pck.exibir_mensagem_abort(762476,'NR_SEQ_LOTE_DISC=' || nr_seq_lote_disc_w);
		end if;
	end if;

	nr_seq_referencia_p := nr_seq_camara_cont_p;
end if;

end pls_imp_camara_contest_v90;
/