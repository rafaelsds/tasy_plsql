create or replace
procedure pls_alimenta_ptu_redundancia
			(	nr_seq_conta_p			pls_conta.nr_sequencia%type) is 
			
nr_seqs_w		pls_util_cta_pck.t_number_table;
ie_alto_custo_w		pls_util_cta_pck.t_varchar2_table_1;
nr_seq_prest_inter_w	pls_conta.nr_seq_prest_inter%type;
nr_seq_fatura_w		pls_conta.nr_seq_fatura%type;

cursor C01(nr_seq_conta_pc	pls_conta.nr_sequencia%type) is
	select	a.nr_sequencia,
		nvl(b.ie_alto_custo,'N')
	from	pls_conta_proc a,
		ptu_nota_servico b
	where	b.nr_seq_conta_proc	= a.nr_sequencia
	and	a.nr_seq_conta		= nr_seq_conta_pc
	and	a.ie_alto_custo is null;

cursor C02(nr_seq_conta_pc	pls_conta.nr_sequencia%type) is
	select	a.nr_sequencia,
		nvl(b.ie_alto_custo,'N')
	from	pls_conta_mat a,
		ptu_nota_servico b
	where	b.nr_seq_conta_mat	= a.nr_sequencia
	and	a.nr_seq_conta		= nr_seq_conta_pc
	and	a.ie_alto_custo is null;


begin

--Utiliza o max para garantir que n�o ocorra o no data found()Melhor performance que begin exception
select	max(nr_seq_prest_inter),
	max(nr_seq_fatura) 
into	nr_seq_prest_inter_w,
	nr_seq_fatura_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;

if	(nr_seq_prest_inter_w is null) then
	select	max(nr_seq_prest_inter)
	into	nr_seq_prest_inter_w
	from 	ptu_nota_cobranca
	where	nr_seq_fatura = nr_seq_fatura_w;
	
	update	pls_conta
	set	nr_seq_prest_inter = nr_seq_prest_inter_w
	where	nr_sequencia = nr_seq_conta_p;
	commit;
end if;

open C01(nr_seq_conta_p);
loop
	fetch C01 bulk collect into	nr_seqs_w, ie_alto_custo_w
	limit pls_util_pck.qt_registro_transacao_w;
	exit when nr_seqs_w.count = 0;
	forall i in nr_seqs_w.first..nr_seqs_w.last
		update	pls_conta_proc
		set	ie_alto_custo = ie_alto_custo_w(i)
		where	nr_sequencia = nr_seqs_w(i);		
	commit;	
end loop;
close C01;

nr_seqs_w.delete;
ie_alto_custo_w.delete;	

open C02(nr_seq_conta_p);
loop
	fetch C02 bulk collect into	nr_seqs_w, ie_alto_custo_w
	limit pls_util_pck.qt_registro_transacao_w;
	exit when nr_seqs_w.count = 0;
	forall i in nr_seqs_w.first..nr_seqs_w.last
		update	pls_conta_mat
		set	ie_alto_custo = ie_alto_custo_w(i)
		where	nr_sequencia = nr_seqs_w(i);		
	commit;	
end loop;
close C02;		
	
end pls_alimenta_ptu_redundancia;
/
