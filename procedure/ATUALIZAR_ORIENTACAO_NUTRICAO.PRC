create or replace
procedure atualizar_orientacao_nutricao(
		cd_pessoa_fisica_p	varchar2,
		dt_final_p	date) is 

begin

update	nut_orientacao
set	dt_final		= dt_final_p
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	dt_liberacao	is not null
and	dt_final		is null;

commit;

end atualizar_orientacao_nutricao;
/