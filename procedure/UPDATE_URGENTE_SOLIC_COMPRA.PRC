create or replace
procedure update_urgente_solic_compra(
		nr_solic_compra_p	number,
		nm_usuario_p		Varchar2) is 

begin

update	solic_compra	
set	ie_urgente = 'S'
where	nr_solic_compra = nr_solic_compra_p;

commit;

end update_urgente_solic_compra;
/

