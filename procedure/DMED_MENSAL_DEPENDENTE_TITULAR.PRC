create or replace
procedure dmed_mensal_dependente_titular
			(	nr_seq_dmed_p		number,
				dt_referencia_p		date,
				cd_estabelecimento_p	number,
				ie_cpf_p			varchar2,
				ie_idade_p		varchar2, 
				ie_estrangeiro_p		varchar2)as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
	DMED_MENSAL_TITULAR_PLANO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

vl_total_w			number(15,2);
vl_total_ww			number(15,2);
vl_total_www			number(15,2);
vl_diferenca_w	                number(15,2);
vl_total_dep_tit_w			number(15,2);
vl_recebido_w			number(15,2);
vl_mensalidade_w			number(15,2);
vl_item_w				number(15,2);
cd_pessoa_fisica_benef_w		varchar2(14);
cd_pessoa_fisica_responsavel_w	varchar2(14);
cd_pessoa_fisica_pagador_w		varchar(14);
cd_pessoa_fisica_titular_w 		varchar(14);
dt_liquidacao_w			date;
dt_ano_calendario_w		varchar2(20);
ie_relacao_dependencia_w		varchar2(2);
ie_tipo_item_w			varchar2(2);
nr_sequencia_w			number(12);
nr_sequencia_ww			number(12);
nr_titulo_w			number(10);
contador_w			number(10);
qt_registros_w			number(10);
idade_w				number(10);
ie_estorno_w			varchar2(1)	:= 'N';
nr_titulo_ant_w			number(10)	:= 0;
TemTitular_w			number(10);
nr_seq_mensalidade_seg_w		number(10);
nr_cpf_pagador_w			varchar2(14);
dt_nascimento_pagador_w		date;
nr_seq_titular_w			number(10);
dt_ref_inicial_w			date;
dt_ref_final_w			date;
nr_seq_mensalidade_w		pls_mensalidade.nr_sequencia%type;
qt_classe_w			number(10);
vl_juros_w			titulo_receber_liq.vl_juros%type;
vl_multa_w			titulo_receber_liq.vl_multa%type;
ie_juros_multa_w			dmed_regra_geral.ie_juros_multa%type;
ie_update_w			varchar(1) := 'S';
ie_tipo_contratacao_w		pls_plano.ie_tipo_contratacao%type;
vl_tributo_w			pls_mensalidade_trib.vl_tributo%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
ie_considerar_ca_pj_w		dmed_regra_geral.ie_considerar_ca_pj%type;
ie_permite_contrato_w		boolean;
nr_seq_item_w			pls_mensalidade_seg_item.nr_sequencia%type;
vl_devolucao_mens_w		number(15,2);
nr_seq_baixa_w			titulo_receber_liq.nr_sequencia%type;
nr_titulo_ww			number(10);
cd_cgc_administradora_w    pls_contrato.cd_cgc_administradora%type;
cd_cgc_w      titulo_receber.cd_cgc%type;
nr_seq_contrato_w    pls_contrato_pagador.nr_seq_contrato%type;

cursor c01 is
	select	c.cd_pessoa_fisica cd_pessoa_fisica_benef,
		      f.cd_pessoa_fisica cd_pessoa_fisica_pagador,
		      (select	s.cd_pessoa_fisica
		       from	  pls_segurado	s
		       where	s.nr_sequencia	= a.nr_seq_titular) cd_pessoa_fisica_titular, 
		      a.nr_seq_titular,
		      ((nvl(l.vl_recebido, 0) + nvl(l.vl_rec_maior, 0)) - 
		      	nvl(dmed_obter_desc_tit_nc(l.nr_titulo, l.nr_sequencia, dt_referencia_p), 0)) vl_recebido,
		      nvl(l.vl_juros, 0) vl_juros,		
		      nvl(l.vl_multa, 0) vl_multa,		
		      m.vl_mensalidade,
		      (select	decode(max(g.ie_grau_parentesco), '4', '03', '3', '04', '5', '06', '6', '06', '04')
		       from	  grau_parentesco	g
		       where	g.nr_sequencia	= a.nr_seq_parentesco) ie_relacao,
		      nvl(r.dt_liquidacao, r.dt_emissao) dt_liquidacao,
		      r.nr_titulo,
		      e.nr_sequencia,
		      m.nr_sequencia nr_seq_mensalidade,
		      a.nr_sequencia nr_seq_segurado,
		      f.nr_seq_contrato,
		      r.cd_cgc
	from	  pls_mensalidade_segurado	e,
		      titulo_receber_liq		l,
		      titulo_receber			r,
		      pessoa_fisica			c,
		      pls_segurado			a,
		      pls_contrato_pagador		f, -- pagador
		      pls_mensalidade			m
	where   m.nr_sequencia		= r.nr_seq_mensalidade 
	and     m.nr_sequencia		= e.nr_seq_mensalidade 
	and	    r.nr_titulo		= l.nr_titulo(+)
	and     f.nr_sequencia		= m.nr_seq_pagador
	and	    a.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and     a.nr_sequencia		= e.nr_seq_segurado
	and	    a.nr_seq_titular is not null
	and	    ((nvl(ie_estrangeiro_p, 3) = 3 ) 
          or (ie_estrangeiro_p = 2 and fis_obter_se_brasileiro(c.cd_pessoa_fisica) = 'S')
          or (ie_estrangeiro_p = 1 and fis_obter_se_brasileiro(c.cd_pessoa_fisica) = 'N'))
	and	    ((l.vl_recebido <> 0) or (l.vl_rec_maior <> 0))
	and	    ((nvl(a.ie_tipo_segurado,'B') in ('B','A')) or (nvl(a.ie_tipo_segurado,'B') = 'R' and a.nr_seq_contrato is not null))
	and	    l.dt_recebimento between dt_ref_inicial_w and dt_ref_final_w
	and	    ((r.vl_saldo_titulo < r.vl_titulo) or (r.ie_situacao = 2))
	and     ((a.cd_estabelecimento = nvl(cd_estabelecimento_p ,a.cd_estabelecimento)) or
	   	    ((a.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	and	    exists	(select	1
			            from	  dmed_regra_tipo_tit	w
			            where	  w.ie_tipo_receber		= l.cd_tipo_recebimento
			            and	    nvl(w.ie_prestadora_ops,'P')	= 'O')
	and	    exists	(select	1			
			            from	  dmed_regra_origem_tit	w
			            where	  w.ie_origem_titulo	= r.ie_origem_titulo)
	and	    ((ie_cpf_p = 'AM') or
		        ((ie_cpf_p = 'SC') and 	 (c.nr_cpf is null) 	and 	(pkg_date_utils.add_month(c.dt_nascimento, ie_idade_p * 12,0) <= Fim_Mes(dt_ref_inicial_w))) or
		        ((ie_cpf_p = 'CC') and (((c.nr_cpf is not null) and 	(pkg_date_utils.add_month(c.dt_nascimento, ie_idade_p * 12,0) <= Fim_Mes(dt_ref_inicial_w))) or 
						(pkg_date_utils.add_month(c.dt_nascimento, ie_idade_p * 12,0) >= Fim_Mes(dt_ref_inicial_w)))))
	and	(r.ie_situacao not in ('3','5') or (r.ie_situacao = '5' and 
		exists	(select	1			
			      from	titulo_receber_liq k
			      where	k.nr_titulo = r.nr_titulo
			      and 	vl_recebido > 0	
			      and 	not exists (select 1 from titulo_receber_liq x where x.nr_titulo = k.nr_titulo and x.nr_seq_liq_origem = k.nr_sequencia and rownum = 1))))
	and 	((r.nr_seq_classe is null) or
		      (qt_classe_w = 0) or
		      (exists	(select 1 
			            from 	  dmed_regra_classe_tit t
			            where	  t.nr_seq_classe = r.nr_seq_classe)))
	and	l.nr_sequencia in  (select  k.nr_sequencia
											    from	  titulo_receber_liq k
											    where   k.nr_titulo = r.nr_titulo
											    and 		k.nr_seq_liq_origem is null
											    and   	not exists (select	1 
																			          from 	  titulo_receber_liq x 
																			          where 	x.nr_titulo = k.nr_titulo 
																			          and 		x.nr_seq_liq_origem = k.nr_sequencia 
																			          and 		rownum = 1))
	order by r.nr_titulo;

cursor C02 is
	select	d.vl_item, 
		d.ie_tipo_item ie_tipo_item,
		d.nr_Sequencia nr_Seq_item
	from 	pls_mensalidade_segurado	e,
		pls_mensalidade_seg_item	d,
		dmed_regra_tipo_item		t
	where 	e.nr_sequencia	= nr_seq_mensalidade_seg_w
	and	e.nr_sequencia 	= d.nr_seq_mensalidade_seg
	and	d.ie_tipo_item 	= t.ie_tipo_item
	and  	d.ie_tipo_item in (select t.ie_tipo_item from dmed_regra_tipo_item t)
	and     ((d.nr_seq_tipo_lanc in (select	z.nr_tipo_lanc_adic
					from    dmed_regra_tipo_lanc z
					where   z.nr_seq_dmed_item = 	(select max(t.nr_sequencia)
									from   	dmed_regra_tipo_item t
									where  	t.ie_tipo_item = d.ie_tipo_item))) or
		((d.nr_seq_tipo_lanc is null) and 
		(not exists	(select  1
				from    dmed_regra_tipo_lanc z
				where   z.nr_seq_dmed_item in	(select max(t.nr_sequencia) 
								from    dmed_regra_tipo_item t
								where   t.ie_tipo_item = d.ie_tipo_item)))));

Cursor C03 (	nr_seq_segurado_pc	pls_segurado.nr_sequencia%type,
		dt_referencia_pc	date) is
	select	sum(vl_classificacao) * - 1 vl_devolucao_mens,
		a.nr_titulo nr_titulo,
		a.dt_baixa
	from	titulo_pagar_baixa a,
		titulo_pagar_baixa_cc_ops b
	where	trunc(a.dt_baixa, 'mm') = trunc(dt_referencia_pc, 'mm')
	and	a.nr_titulo = b.nr_titulo
	and	b.nr_seq_segurado = nr_seq_segurado_pc
	and	a.nr_sequencia = b.nr_seq_baixa
	and	 not exists (	select	1
				from	dmed_titulos_mensal x
				where	x.nr_documento = a.nr_titulo)
	group by
		a.nr_titulo,
		dt_baixa;
								
begin
dt_ref_inicial_w	:= pkg_date_utils.start_of(dt_referencia_p, 'MONTH',0);
dt_ref_final_w		:= fim_dia(fim_mes(dt_referencia_p));

select 	nvl(max(nr_idade),16) 
into 	idade_w
from 	dmed_regra_geral;

select	count(1)
into	qt_classe_w
from	dmed_regra_classe_tit
where	rownum	= 1;

select 	nvl(max(ie_juros_multa), 'N'),
	nvl(max(ie_considerar_ca_pj), 'N')
into	ie_juros_multa_w,
	ie_considerar_ca_pj_w
from	dmed_regra_geral;

open c01;
loop
fetch c01 into	
	cd_pessoa_fisica_benef_w,
	cd_pessoa_fisica_pagador_w,
	cd_pessoa_fisica_titular_w,
	nr_seq_titular_w,
	vl_recebido_w,
	vl_juros_w,
	vl_multa_w,
	vl_mensalidade_w,
	ie_relacao_dependencia_w,
	dt_liquidacao_w	,	
	nr_titulo_w,		
	nr_seq_mensalidade_seg_w,
	nr_seq_mensalidade_w,
	nr_seq_segurado_w,
	nr_seq_contrato_w,
	cd_cgc_w;
exit when c01%notfound;
	begin

	select	nvl(max(b.ie_tipo_contratacao), 'CE')
	into	ie_tipo_contratacao_w
	from	pls_plano b
	where	b.nr_sequencia = pls_obter_produto_benef(nr_seq_segurado_w,dt_liquidacao_w);
	
	if ie_considerar_ca_pj_w = 'S' then
		ie_permite_contrato_w := ((ie_tipo_contratacao_w in ('I','CA')) or (ie_tipo_contratacao_w in ('CE') and cd_pessoa_fisica_pagador_w is not null));
    if  ((ie_tipo_contratacao_w = 'CA') and (nr_seq_contrato_w is not null)) then
    
      select  max(a.cd_cgc_administradora)
      into  cd_cgc_administradora_w
      from  pls_contrato a
      where  a.nr_sequencia = nr_seq_contrato_w;
    
      if cd_cgc_w = cd_cgc_administradora_w then
        ie_permite_contrato_w := false;
      end if;
    end if;
	else
		ie_permite_contrato_w := ((ie_tipo_contratacao_w in ('I')) or (ie_tipo_contratacao_w in ('CE','CA') and cd_pessoa_fisica_pagador_w is not null));
	end if;
	
  /*select  nvl(sum(b.vl_tributo),0)
  into  vl_tributo_w
  from     pls_mensalidade_trib b,
    pls_mensalidade_seg_item c,
    pls_mensalidade_Segurado a,
    pls_Mensalidade d
  where    c.nr_sequencia = b.nr_seq_item_mens
  and      d.nr_Sequencia = nr_seq_mensalidade_w
  and      b.ie_retencao = 'D'
  and      a.nr_Sequencia = c.nr_Seq_mensalidade_Seg
  and      d.nr_Sequencia = a.nr_Seq_mensalidade;
  
  vl_mensalidade_w := vl_mensalidade_w - vl_tributo_w;*/
	
	if	ie_permite_contrato_w then
		
		cd_pessoa_fisica_responsavel_w	:= nvl(cd_pessoa_fisica_pagador_w,cd_pessoa_fisica_titular_w);

		select	max(nr_cpf),
			max(dt_nascimento)
		into	nr_cpf_pagador_w,
			dt_nascimento_pagador_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_responsavel_w;
		
		if ie_juros_multa_w = 'S' then
			vl_recebido_w := vl_recebido_w + vl_juros_w + vl_multa_w;
		end if;			

		vl_total_ww := 0;

		/* for r_c03_w in c03 (nr_seq_segurado_w,  dt_referencia_p) loop
			begin
			if	(r_c03_w.vl_devolucao_mens <> 0) then		
				select	dmed_titulos_mensal_Seq.nextval
				into 	nr_sequencia_w
				from 	dual;
			
				insert into dmed_titulos_mensal
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_dmed_mensal,
					nr_documento,
					ie_tipo_documento,
					cd_pessoa_titular,
					cd_pessoa_beneficiario,
					vl_pago,
					dt_liquidacao,
					ie_prestadora_ops)
				values	(nr_sequencia_w,
					sysdate,
					'Tasy6',
					sysdate,
					'Tasy6',
					nr_seq_dmed_p,
					r_c03_w.nr_titulo,
					'1',
					cd_pessoa_fisica_responsavel_w,
					cd_pessoa_fisica_benef_w,
					r_c03_w.vl_devolucao_mens,
					r_c03_w.dt_baixa,
					'O');
			end if;
			end;
		end loop;*/
		
		open C02;
		loop
		fetch C02 into	
			vl_item_w, 
			ie_tipo_item_w,
			nr_seq_item_w;
		exit when C02%notfound;
			begin
			
			vl_tributo_w := 0;
			
			if ie_tipo_contratacao_w = 'CA' then
				
				select	nvl(sum(b.vl_tributo),0)
				into	vl_tributo_w
				from   	pls_mensalidade_trib b,
					pls_mensalidade_seg_item c,
					pls_mensalidade_Segurado a,
					pls_Mensalidade d,
					titulo_receber e
				where  	c.nr_sequencia = b.nr_seq_item_mens
				and    	d.nr_Sequencia = e.nr_seq_mensalidade
				and    	b.ie_retencao = 'D'
				and    	a.nr_Sequencia = c.nr_Seq_mensalidade_Seg
				and    	d.nr_Sequencia = a.nr_Seq_mensalidade
				and    	d.nr_Sequencia = nr_seq_mensalidade_w
				and 	c.nr_Sequencia = nr_seq_item_w;
	
				if vl_recebido_w < 0 then
					vl_tributo_w := vl_tributo_w * -1;
				else
				  vl_tributo_w := 0;
				end if;	
				
			end if;	
			
			-- do responsavel verificar apenas o CPF, a data de dascimento deve ser verificar somente do dependente (
			if 	(((ie_cpf_p = 'CC') and
				(nr_cpf_pagador_w is not null )) or 
				((ie_cpf_p = 'SC') and
				(nr_cpf_pagador_w is null)) or 		 
				(ie_cpf_p = 'AM')) then
				vl_total_w	:= round(((vl_recebido_w / vl_mensalidade_w) * vl_item_w) + vl_tributo_w, 2);
				vl_total_ww	:= vl_total_ww + vl_total_w;
				
				select 	dmed_titulos_mensal_seq.NextVal
				into	nr_sequencia_w
				from 	dual;
				
				insert into dmed_titulos_mensal
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_dmed_mensal,
					nr_documento,
					ie_tipo_documento,
					cd_pessoa_titular,
					cd_pessoa_beneficiario,
					vl_pago,
					dt_liquidacao,
					ie_prestadora_ops)
				values	(nr_sequencia_w,
					sysdate,
					'Tasy6',
					sysdate,
					'Tasy6',
					nr_seq_dmed_p,
					nr_titulo_w,
					ie_tipo_item_w,
					cd_pessoa_fisica_responsavel_w,
					cd_pessoa_fisica_benef_w,
					vl_total_w,
					dt_liquidacao_w,
					'O');
				
				contador_w	:= contador_w + 1;
				
				if	(contador_w mod 100 = 0) then
					commit;
				end if;
			end if;
			end;	
		end loop;
		close C02;
		
		select 	sum(vl_pago)
		into	vl_total_www
		from	dmed_titulos_mensal
		where   nr_documento = nr_titulo_w	
		and	nr_seq_dmed_mensal = nr_seq_dmed_p;	
		
		if (vl_recebido_w <> vl_total_ww) or (vl_recebido_w <> vl_total_www) then

			vl_diferenca_w :=  (vl_recebido_w - vl_total_ww);

			ie_update_w := 'S';
			
			if	(vl_diferenca_w >= -0.02) and
				(vl_diferenca_w <=  0.02) then

				update 	dmed_titulos_mensal
				set    	vl_pago = (vl_pago + vl_diferenca_w)
				where   nr_sequencia = nr_sequencia_w;

				ie_update_w := 'N';
			end if;

			vl_diferenca_w :=  (vl_recebido_w - vl_total_www);

			if	(vl_diferenca_w >= -0.02) and
				(vl_diferenca_w <=  0.02) and (ie_update_w = 'S') then

				update 	dmed_titulos_mensal
				set    	vl_pago = (vl_pago + vl_diferenca_w)
				where   nr_sequencia = nr_sequencia_w;
			end if;

		end if;
		
		nr_titulo_ant_w := nr_titulo_w;
	end if;
	end;
end loop;
close c01;


commit;

end dmed_mensal_dependente_titular;
/
