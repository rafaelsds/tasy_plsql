create or replace
procedure dmed_mensal_reembolso_ops
			(	nr_seq_dmed_p		number,
				dt_referencia_p		date,
				cd_estabelecimento_p	number,
				ie_cpf_p		varchar2,
				ie_idade_p		varchar2, 
				ie_estrangeiro_p	varchar2) as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	no DTOP quando o dependente � o pagador e possui sequencia de titular, 
	o respons�vel deve ser o titular
	como � feito desta forma da dmed_mensal_dependente_titular, 
	aqui precisa ser feito desta forma tbm, porque o RDTOP precisa estar 
	associado ao DTOP, se aqui pegar um respons�vel diferente n�o vai ficar
	associado ao registros DTOP
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	DMED_MENSAL_TITULAR_PLANO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

vl_total_w				number(15,2);
nr_seq_titular_w			number(10);
nr_titulo_w				number(10);
cd_pessoa_fisica_benef_w		varchar(15);
cd_pessoa_fisica_pagador_w		varchar(15);
cd_pessoa_jur_prest_serv_w		varchar(15);
cd_pessoa_fisica_responsavel_w		varchar(15);
cd_pessoa_fis_prest_serv_w		varchar(15);
cd_pessoa_fisica_titular_w		varchar(15);
dt_ano_calendario_w			varchar2(5);
dt_liquidacao_w				date;
cd_cgc_w				varchar2(14);
cd_cpf_w				varchar2(20);
contador_w				number(10);
qt_registros_w				number(10);
qt_registros_ww				number(10);
ie_cpf_w				varchar2(2)	:= ie_cpf_p;
nr_cpf_w				varchar2(11);
idade_w					number(10)	:= 18;
dt_nascimento_w				date;
dt_ref_inicial_w			date;
dt_ref_final_w				date;

cursor c01 is
	--titulares
	select	p.nr_titulo,
		x.vl_reembolso vl_total,
		p.dt_liquidacao,
		a.cd_pessoa_fisica cd_pessoa_fisica_benef, 
		f.cd_pessoa_fisica cd_pessoa_fisica_pagador, --pagador das mensalidades, do titulos a receber
		a.cd_pessoa_fisica cd_pessoa_fisica_titular, 
		c.cd_cgc cd_pessoa_jur_prest_serv,
		c.cd_pessoa_fisica cd_pessoa_fis_prest_serv,
		a.nr_seq_titular
	from	pls_conta		c,
		pls_protocolo_conta	d,
		pls_segurado		a,
		pls_contrato_pagador	f,
		titulo_pagar		p,
		pls_mens_detalhe_ir  	x,	
		pls_mens_pagador_ir 	y,	
		pls_mens_beneficiario_ir 	b			
	where	d.nr_sequencia 		= p.nr_seq_reembolso
	and	a.nr_sequencia		= d.nr_seq_segurado
	and	d.nr_sequencia		= c.nr_seq_protocolo	
	and 	f.nr_sequencia(+)	= a.nr_seq_pagador
	and	a.nr_sequencia   	= b.nr_seq_segurado	
	and 	p.nr_titulo   	= x.nr_titulo_pag
	and 	x.nr_seq_beneficiario_ir = b.nr_sequencia	
	and 	f.nr_sequencia	= y.nr_seq_pagador
	and 	y.nr_sequencia	= b.nr_seq_pagador_ir	
	and	d.ie_tipo_protocolo	= 'R'
	and	a.nr_seq_titular is null
	and	x.vl_reembolso > 0
	and	p.ie_situacao <> 'C'
	and	x.dt_baixa between dt_ref_inicial_w and dt_ref_final_w
	and  	((a.cd_estabelecimento = nvl(cd_estabelecimento_p ,a.cd_estabelecimento)) 
	or   	((a.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	union
	--dependentes
	select	p.nr_titulo,
		x.vl_reembolso vl_total,
		p.dt_liquidacao,
		a.cd_pessoa_fisica cd_pessoa_fisica_benef, 
		f.cd_pessoa_fisica cd_pessoa_fisica_pagador,
		(select	s.cd_pessoa_fisica
		from	pls_segurado	s
		where	s.nr_sequencia	= a.nr_seq_titular) cd_pessoa_fisica_titular,
		c.cd_cgc cd_pessoa_jur_prest_serv,
		c.cd_pessoa_fisica cd_pessoa_fis_prest_serv,
		a.nr_seq_titular
	from	pls_conta		c,
		pls_protocolo_conta	d,
		pls_segurado		a,
		pls_contrato_pagador	f,
		titulo_pagar		p,
		pls_mens_detalhe_ir  	x,	
		pls_mens_pagador_ir 	y,	
		pls_mens_beneficiario_ir 	b			
	where	d.nr_sequencia 		= p.nr_seq_reembolso
	and	a.nr_sequencia		= d.nr_seq_segurado
	and	d.nr_sequencia		= c.nr_seq_protocolo	
	and 	f.nr_sequencia(+)	= a.nr_seq_pagador
	and	a.nr_sequencia   	= b.nr_seq_segurado	
	and 	p.nr_titulo   	= x.nr_titulo_pag
	and 	x.nr_seq_beneficiario_ir = b.nr_sequencia	
	and 	f.nr_sequencia	= y.nr_seq_pagador
	and 	y.nr_sequencia	= b.nr_seq_pagador_ir	
	and	d.ie_tipo_protocolo	= 'R'
	and	a.nr_seq_titular is not null
	and	x.vl_reembolso > 0
	and	p.ie_situacao <> 'C'
	and	x.dt_baixa between dt_ref_inicial_w and dt_ref_final_w
	and  	((a.cd_estabelecimento = nvl(cd_estabelecimento_p ,a.cd_estabelecimento)) 
	or   	((a.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	order by
		3,
		1,
		2;
		
type 		fetch_array is table of C01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_movimentacao_w	vetor;
	
begin
dt_ref_inicial_w	:= trunc(dt_referencia_p, 'mm');
dt_ref_final_w		:= fim_dia(fim_mes(dt_referencia_p));

open C01;
loop
fetch C01 bulk collect into s_array limit 1000;
	vetor_movimentacao_w(i)	:= s_array;
	i			:= i + 1;
exit when C01%notfound;
end loop;
close C01;

for i in 1..vetor_movimentacao_w.count loop
	begin
	s_array := vetor_movimentacao_w(i);
	for z in 1..s_array.count loop
		nr_titulo_w			:= s_array(z).nr_titulo;
		vl_total_w			:= s_array(z).vl_total;
		dt_liquidacao_w			:= s_array(z).dt_liquidacao;
		cd_pessoa_fisica_benef_w	:= s_array(z).cd_pessoa_fisica_benef;
		cd_pessoa_fisica_pagador_w	:= s_array(z).cd_pessoa_fisica_pagador; 
		cd_pessoa_fisica_titular_w	:= s_array(z).cd_pessoa_fisica_titular;
		cd_pessoa_jur_prest_serv_w	:= s_array(z).cd_pessoa_jur_prest_serv;
		cd_pessoa_fis_prest_serv_w	:= s_array(z).cd_pessoa_fis_prest_serv;
		nr_seq_titular_w		:= s_array(z).nr_seq_titular;
	
		begin
		
		cd_pessoa_fisica_responsavel_w	:= nvl(cd_pessoa_fisica_pagador_w, cd_pessoa_fisica_titular_w);

			begin
			select	nr_cpf,
				dt_nascimento
			into	nr_cpf_w,
				dt_nascimento_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_responsavel_w;
			exception when others then
				nr_cpf_w := '';
				dt_nascimento_w := null;
			end;
		
		if	(((ie_cpf_w = 'CC') and ((nr_cpf_w is not null) )) or 
			((ie_cpf_w = 'SC') and (nr_cpf_w is null)) or 		 
			(ie_cpf_w = 'AM')) then
			insert into dmed_titulos_mensal
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_dmed_mensal,
				nr_documento,
				ie_tipo_documento,
				cd_pessoa_titular,
				cd_pessoa_beneficiario,
				cd_cgc,
				cd_pf_prestador,
				vl_pago,
				dt_liquidacao,
				ie_prestadora_ops)
			values	(dmed_titulos_mensal_seq.NextVal,
				sysdate,
				'Tasy44',
				sysdate,
				'Tasy44',
				nr_seq_dmed_p,
				nr_titulo_w,
				'RE',
				cd_pessoa_fisica_responsavel_w,
				cd_pessoa_fisica_benef_w,
				cd_pessoa_jur_prest_serv_w,
				cd_pessoa_fis_prest_serv_w,
				vl_total_w,
				dt_liquidacao_w,
				'O');

			contador_w	:= contador_w + 1;
			
			if	(contador_w mod 100 = 0) then
				commit;
			end if;
		
		end if;
		end;
	end loop;
	end;
end loop;

commit;

end dmed_mensal_reembolso_ops;
/
