create or replace
procedure inserir_proj_ata_envio(	nr_seq_ata_p		number,
				ds_envio_p		varchar2,
				ds_observacao_p		varchar2,
				nm_usuario_p		varchar2) is 

begin

if	(nr_seq_ata_p is not null) and
	(ds_envio_p is not null) and
	(ds_observacao_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	insert	into proj_ata_envio(
			nr_sequencia, 
			nr_seq_ata, 
			dt_atualizacao, 
			nm_usuario, 
			dt_atualizacao_nrec, 
			nm_usuario_nrec,
			dt_envio, 
			ds_destino, 
			ds_observacao) 
		values (proj_ata_envio_seq.nextval, 
			nr_seq_ata_p, 
			sysdate,
			nm_usuario_p, 
			sysdate, 
			nm_usuario_p, 
			sysdate, 
			substr(ds_envio_p,1,255), 
			ds_observacao_p);
	commit;
	
	end;
end if;

end inserir_proj_ata_envio;
/