create or replace procedure fleury_del_log_except_canc_lab( nr_prescricao_p         number,
                                                            nr_seq_item_p           number,
                                                            nr_atendimento_p        number,
                                                            nr_prescricoes_p        varchar2,
                                                            cd_item_p               number,
                                                            qt_item_p               number,
                                                            nr_seq_proc_interno_p   number,
                                                            nm_usuario_p			varchar2,
                                                            cd_estabelecimento_p	number) is

ie_desagrupa_proced_w	varchar2(1);
nr_prescricao_w         prescr_procedimento.nr_prescricao%type;
nr_seq_prescr_w         prescr_procedimento.nr_sequencia%type;

cursor c01 is
select
    b.nr_prescricao,
    b.nr_sequencia
from prescr_procedimento b,
     prescr_medica a
where a.nr_prescricao = b.nr_prescricao and
    (((b.nr_sequencia	= nr_seq_item_p) and
      (b.nr_prescricao	= nr_prescricao_p) and
      (ie_desagrupa_proced_w = 'S')) or
     (ie_desagrupa_proced_w  = 'N'))
and	a.nr_atendimento = nr_atendimento_p
and	obter_se_contido(b.nr_prescricao, '(' || nr_prescricoes_p || ')') = 'S'
and	b.cd_procedimento = cd_item_p
and	nvl(b.qt_procedimento,1) = nvl(qt_item_p,nvl(b.qt_procedimento,1)) 
and nvl(b.nr_seq_proc_interno, 1) = nvl(nr_seq_proc_interno_p, nvl(b.nr_seq_proc_interno,1));

begin

    obter_param_usuario(1113, 463, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_desagrupa_proced_w);

    open c01;
	loop
	fetch c01 into
		nr_prescricao_w,
		nr_seq_prescr_w;
	exit when c01%notfound;
    begin
        begin
            fleury_delete_log_except_canc(nr_prescricao_w, nr_seq_prescr_w);
        exception
        when others then
            gravar_log_lab_pragma(19392, 'Erro ao executar -> fleury_delete_log_except_canc('||nr_prescricao_w||', '||nr_seq_prescr_w||'); sqlerrm -> ' ||
                                        sqlerrm ||
                                        'stack -> ' || dbms_utility.format_call_stack, nm_usuario_p);
        end;
    end;
    end loop;
	close c01;

end fleury_del_log_except_canc_lab;
/