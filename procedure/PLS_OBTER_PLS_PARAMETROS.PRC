create or replace 
procedure pls_obter_pls_parametros
		(cd_estabelecimento_p	in	number,
		 nm_atributo_p		in	varchar2,
		 ds_valor_padrao_p	in	varchar2,
		 ds_retorno_p		out	varchar2) is

ie_agrup_contratos_w		pls_parametros.ie_agrup_contratos%type;
cd_banco_desc_folha_w		NUMBER(15,0); 
cd_centro_custo_w		NUMBER(15,0); 
cd_cgc_ans_w 			VARCHAR2(4000); 
cd_condicao_pagamento_w		NUMBER(15,0); 
cd_conta_financ_w		NUMBER(15,0); 
cd_conta_financ_conta_w		NUMBER(15,0); 
cd_conta_financ_prov_w		NUMBER(15,0);
cd_conta_financ_reembolso_w	NUMBER(15,0); 
cd_conta_financ_ressarc_w	NUMBER(15,0); 
cd_conta_financ_taxa_w		NUMBER(15,0);
cd_estabelecimento_w		NUMBER(15,0);
cd_interface_imp_benef_w	NUMBER(15,0); 
cd_moeda_adiantamento_w		NUMBER(15,0); 
cd_natureza_operacao_w		NUMBER(15,0);
cd_natureza_operacao_desp_w	NUMBER(15,0); 
cd_operacao_desp_nf_w		NUMBER(15,0); 
cd_operacao_nf_w		NUMBER(15,0);
cd_perfil_comunic_integr_w	NUMBER(15,0);
cd_portador_w			NUMBER(15,0); 
cd_serie_desp_nf_w 		VARCHAR2(4000); 
cd_serie_nf_w 			VARCHAR2(4000);
cd_tipo_portador_w		NUMBER(15,0);
cd_tipo_receb_adiantamento_w	NUMBER(15,0);
cd_tipo_recebimento_w		NUMBER(15,0); 
cd_tipo_receb_inadimplencia_w	NUMBER(15,0); 
cd_tipo_taxa_juro_w		NUMBER(15,0); 
cd_tipo_taxa_multa_w		NUMBER(15,0);
ds_mascara_cart_padrao_w 	VARCHAR2(4000); 
dt_base_validade_carteira_w 	VARCHAR2(4000);
dt_base_validade_cart_inter_w 	VARCHAR2(4000);
ie_analise_cm_nova_w 		VARCHAR2(4000); 
ie_arredondar_tps_w 		VARCHAR2(4000);
ie_atualizar_valor_apresent_w 	VARCHAR2(4000);
ie_balancete_ativo_w 		VARCHAR2(4000); 
ie_balancete_despesa_w 		VARCHAR2(4000); 
ie_balancete_passivo_w 		VARCHAR2(4000); 
ie_balancete_receita_w 		VARCHAR2(4000); 
ie_base_venc_lote_pag_w 	VARCHAR2(4000);
ie_bloqueto_deb_auto_w 		VARCHAR2(4000); 
ie_calculo_base_glosa_w 	VARCHAR2(4000);
ie_calculo_coparticipacao_w 	VARCHAR2(4000);
ie_calculo_pacote_w 		VARCHAR2(4000);
ie_calculo_pos_estab_w 		VARCHAR2(4000); 
ie_cancela_lib_matmed_w 	VARCHAR2(4000);
ie_carencia_abrangencia_ant_w 	VARCHAR2(4000);
ie_cobertura_assistencial_w 	VARCHAR2(4000);
ie_cobranca_pos_w 		VARCHAR2(4000);
ie_considerar_dt_liq_cancel_w 	VARCHAR2(4000);
ie_consiste_prest_envio_a500_w 	VARCHAR2(4000); 
ie_contab_prov_copartic_w 	VARCHAR2(4000);
ie_contrap_pecun_w		VARCHAR2(4000);
ie_copartic_grau_partic_w 	VARCHAR2(4000);
ie_corresponsabilidade_w 	VARCHAR2(4000); 
ie_corresp_cedida_w		VARCHAR2(4000);
ie_data_base_proporcional_w 	VARCHAR2(4000);
ie_data_base_prot_reap_w 	VARCHAR2(4000); 
ie_data_referencia_nf_w 	VARCHAR2(4000);
ie_data_ref_reaj_adaptado_w 	VARCHAR2(4000);
ie_define_se_gera_analise_w 	VARCHAR2(4000);
ie_dt_protocolo_a500_w 		VARCHAR2(4000); 
ie_esquema_contabil_w 		VARCHAR2(4000);
ie_eventos_sinistros_w 		VARCHAR2(4000); 
ie_exige_lib_bras_w 		VARCHAR2(4000);
ie_fechar_conta_glosa_w 	VARCHAR2(4000);
ie_fluxo_caixa_w 		VARCHAR2(4000); 
ie_fluxo_contest_w 		VARCHAR2(4000); 
ie_fora_linha_simpro_w 		VARCHAR2(4000); 
ie_geracao_coparticipacao_w 	VARCHAR2(4000);
ie_geracao_nota_titulo_w 	VARCHAR2(4000); 
ie_geracao_nota_titulo_pf_w 	VARCHAR2(4000);
ie_geracao_pos_estabelecido_w 	VARCHAR2(4000);
ie_gerar_coparticipacao_w 	VARCHAR2(4000);
ie_gerar_copartic_zerado_w 	VARCHAR2(4000); 
ie_gerar_glosa_valor_zerado_w 	VARCHAR2(4000);
ie_gerar_usuario_even_rep_w	VARCHAR2(4000);
ie_gerar_usuario_eventual_w 	VARCHAR2(4000);
ie_gerar_validade_cartao_w 	VARCHAR2(4000); 
ie_guia_unica_w 		VARCHAR2(4000);
ie_hash_conta_w 		VARCHAR2(4000);
ie_idade_saldo_w 		VARCHAR2(4000); 
ie_importacao_material_w 	VARCHAR2(4000); 
ie_intercambio_w 		VARCHAR2(4000); 
ie_isentar_carencia_nasc_w 	VARCHAR2(4000); 
ie_lucro_prejuizo_w 		VARCHAR2(4000);
ie_material_intercambio_w 	VARCHAR2(4000);
ie_material_ops_w 		VARCHAR2(4000);
ie_material_rede_propria_w 	VARCHAR2(4000); 
ie_mes_cobranca_reaj_w 		VARCHAR2(4000); 
ie_mes_cobranca_reaj_reg_w 	VARCHAR2(4000); 
ie_movto_pel_w			VARCHAR2(4000); 
ie_obriga_vinc_mat_w 		VARCHAR2(4000); 
ie_origem_tit_pagar_w 		VARCHAR2(4000);
ie_origem_tit_reembolso_w 	VARCHAR2(4000);
ie_origem_tit_taxa_saude_w 	VARCHAR2(4000); 
ie_origem_titulo_w 		VARCHAR2(4000); 
ie_pagador_contrato_princ_w 	VARCHAR2(4000);
ie_permite_cnes_duplic_w 	VARCHAR2(4000); 
ie_pessoa_contrato_w 		VARCHAR2(4000); 
ie_pos_estab_faturamento_w 	VARCHAR2(4000); 
ie_pos_estab_fat_resc_w 	VARCHAR2(4000); 
ie_preco_prestador_partic_w 	VARCHAR2(4000);
ie_pro_rata_dia_w 		VARCHAR2(4000);
ie_quebrar_fatura_ptu_tx_w 	VARCHAR2(4000); 
ie_reajustar_benef_cancelado_w 	VARCHAR2(4000); 
ie_reajuste_w 			VARCHAR2(4000);
ie_reajuste_faixa_etaria_w 	VARCHAR2(4000); 
ie_ref_copartic_mens_w 		VARCHAR2(4000); 
ie_regra_autogerado_w 		VARCHAR2(4000);
ie_renovacao_cartao_dt_atual_w 	VARCHAR2(4000); 
ie_restringe_conjuge_w 		VARCHAR2(4000); 
ie_scpa_contrato_w 		VARCHAR2(4000); 
ie_solvencia_w 			VARCHAR2(4000); 
ie_taxa_grau_partic_w 		VARCHAR2(4000);
ie_tela_guia_cm_w 		VARCHAR2(4000);
ie_tipo_feriado_w 		VARCHAR2(4000);
ie_tipo_geracao_a800_w 		VARCHAR2(4000); 
ie_tipo_lote_baixa_mens_w 	VARCHAR2(4000);
ie_utiliza_material_ops_w 	VARCHAR2(4000);
ie_venc_cart_fim_mes_w 		VARCHAR2(4000); 
ie_via_acesso_regra_w 		VARCHAR2(4000);
nr_seq_agente_motivador_w	NUMBER(15,0); 
nr_seq_classif_fiscal_w		NUMBER(15,0); 
nr_seq_conta_banco_w		NUMBER(15,0);
nr_seq_conta_banco_desc_w	NUMBER(15,0); 
nr_seq_emissor_w		NUMBER(15,0);
nr_seq_emissor_provisorio_w	NUMBER(15,0); 
nr_seq_estrut_mat_rev_w		NUMBER(15,0); 
nr_seq_grupo_pre_analise_w	NUMBER(15,0);
nr_seq_modelo_w			NUMBER(15,0); 
nr_seq_motivo_adaptacao_w	NUMBER(15,0); 
nr_seq_motivo_cancel_agenda_w	NUMBER(15,0); 
nr_seq_motivo_inclusao_w	NUMBER(15,0);
nr_seq_motivo_rescisao_w	NUMBER(15,0);
nr_seq_motivo_rescisao_a100_w	NUMBER(15,0); 
nr_seq_motivo_rescisao_obito_w	NUMBER(15,0);
nr_seq_motivo_via_w		NUMBER(15,0); 
nr_seq_plano_usu_eventual_w	NUMBER(15,0); 
nr_seq_relatorio_w		NUMBER(15,0);
nr_seq_relatorio_cat_w		NUMBER(15,0);
nr_seq_sit_trib_w		NUMBER(15,0); 
nr_seq_sit_trib_desp_w		NUMBER(15,0);
nr_seq_tipo_avaliacao_w		NUMBER(15,0); 
nr_seq_trans_fin_baixa_w	NUMBER(15,0);
nr_seq_trans_fin_baixa_conta_w	NUMBER(15,0);
nr_seq_trans_fin_baixa_mens_w	NUMBER(15,0); 
nr_seq_trans_fin_baixa_prov_w	NUMBER(15,0); 
nr_seq_trans_fin_baixa_reemb_w	NUMBER(15,0);
nr_seq_trans_fin_baixa_taxa_w	NUMBER(15,0); 
nr_seq_trans_fin_baixa_vend_w	NUMBER(15,0); 
nr_seq_trans_fin_con_mens_w	NUMBER(15,0); 
nr_seq_trans_fin_inadi_w	NUMBER(15,0);
nr_sequencia_w			NUMBER(15,0);
pr_juro_padrao_w		NUMBER(15,4);
pr_multa_padrao_w		NUMBER(15,4); 
qt_dias_ativ_canal_com_w	NUMBER(15,0);
qt_dias_protocolo_w		NUMBER(15,0); 
qt_dias_rescisao_mig_w		NUMBER(15,0);
qt_dias_venc_primeira_mens_w	NUMBER(15,0);
qt_idade_limite_w		NUMBER(15,0); 
qt_min_limite_analise_w		NUMBER(15,0); 
qt_tempo_ausente_w		NUMBER(15,0);
qt_tempo_limite_w		NUMBER(15,0); 
ie_interc_eventual_w		pls_parametros.ie_interc_eventual%type;
ie_ativo_garantidor_w		pls_parametros.ie_ativo_garantidor%type;
ie_corresp_diops_w		pls_parametros.ie_corresp_diops%type;
ie_fundos_comuns_w		pls_parametros.ie_fundos_comuns%type;

begin

begin
select	cd_banco_desc_folha,
	cd_centro_custo,
	cd_cgc_ans,
	cd_condicao_pagamento,
	cd_conta_financ,
	cd_conta_financ_conta,
	cd_conta_financ_prov,
	cd_conta_financ_reembolso,
	cd_conta_financ_ressarcimento,
	cd_conta_financ_taxa,
	cd_estabelecimento,
	cd_interface_importacao_benef,
	cd_moeda_adiantamento,
	cd_natureza_operacao,
	cd_natureza_operacao_desp,
	cd_operacao_desp_nf,
	cd_operacao_nf,
	cd_perfil_comunic_integr,
	cd_portador,
	cd_serie_desp_nf,
	cd_serie_nf,
	cd_tipo_portador,
	cd_tipo_receb_adiantamento,
	cd_tipo_recebimento,
	cd_tipo_receb_inadimplencia,
	cd_tipo_taxa_juro,
	cd_tipo_taxa_multa,
	ds_mascara_cart_padrao,
	dt_base_validade_carteira,
	dt_base_validade_cart_inter,
	ie_analise_cm_nova,
	ie_arredondar_tps,
	ie_atualizar_valor_apresent,
	ie_balancete_ativo,
	ie_balancete_despesa,
	ie_balancete_passivo,
	ie_balancete_receita,
	ie_base_venc_lote_pag,
	ie_bloqueto_deb_auto,
	ie_calculo_base_glosa,
	ie_calculo_coparticipacao,
	ie_calculo_pacote,
	ie_calculo_pos_estab,
	ie_cancela_lib_matmed,
	ie_carencia_abrangencia_ant,
	ie_cobertura_assistencial,
	ie_cobranca_pos,
	ie_considerar_dt_liq_cancel,
	ie_consiste_prest_envio_a500,
	ie_contab_prov_copartic,
	ie_copartic_grau_partic,
	ie_corresp_diops,
	ie_corresponsabilidade,
	ie_data_base_proporcional,
	ie_data_base_prot_reap,
	ie_data_referencia_nf,
	ie_data_ref_reaj_adaptado,
	ie_define_se_gera_analise,
	ie_dt_protocolo_a500,
	ie_esquema_contabil,
	ie_eventos_sinistros,
	ie_exige_lib_bras,
	ie_fluxo_caixa,
	ie_fluxo_contest,
	ie_fora_linha_simpro,
	ie_fundos_comuns,
	ie_geracao_coparticipacao,
	ie_geracao_nota_titulo,
	ie_geracao_nota_titulo_pf,
	ie_geracao_pos_estabelecido,
	ie_gerar_coparticipacao,
	ie_gerar_copartic_zerado,
	ie_gerar_glosa_valor_zerado,
	ie_gerar_usuario_even_repasse,
	ie_gerar_usuario_eventual,
	ie_gerar_validade_cartao,
	ie_guia_unica,
	ie_hash_conta,
	ie_idade_saldo,
	ie_importacao_material,
	ie_intercambio,
	ie_isentar_carencia_nasc,
	ie_lucro_prejuizo,
	ie_material_intercambio,
	ie_material_ops,
	ie_material_rede_propria,
	ie_mes_cobranca_reaj,
	ie_mes_cobranca_reaj_reg,
	ie_movto_pel,
	ie_obriga_vinc_mat,
	ie_origem_tit_pagar,
	ie_origem_tit_reembolso,
	ie_origem_tit_taxa_saude,
	ie_origem_titulo,
	ie_pagador_contrato_princ,
	ie_permite_cnes_duplic,
	ie_pessoa_contrato,
	ie_pos_estab_faturamento,
	ie_pos_estab_fat_resc,
	ie_preco_prestador_partic,
	ie_pro_rata_dia,
	ie_quebrar_fatura_ptu_tx,
	ie_reajustar_benef_cancelado,
	ie_reajuste,
	ie_reajuste_faixa_etaria,
	ie_ref_copartic_mens,
	ie_regra_autogerado,
	ie_renovacao_cartao_dt_atual,
	ie_restringe_conjuge,
	ie_scpa_contrato,
	ie_solvencia,
	ie_taxa_grau_partic,
	ie_tela_guia_cm,
	ie_tipo_feriado,
	ie_tipo_geracao_a800,
	ie_tipo_lote_baixa_mens,
	ie_utiliza_material_ops,
	ie_venc_cart_fim_mes,
	ie_via_acesso_regra,
	nr_seq_agente_motivador,
	nr_seq_classif_fiscal,
	nr_seq_conta_banco,
	nr_seq_conta_banco_desc,
	nr_seq_emissor,
	nr_seq_emissor_provisorio,
	nr_seq_estrut_mat_rev,
	nr_seq_grupo_pre_analise,
	nr_seq_modelo,
	nr_seq_motivo_adaptacao,
	nr_seq_motivo_cancel_agenda,
	nr_seq_motivo_inclusao,
	nr_seq_motivo_rescisao,
	nr_seq_motivo_rescisao_a100,
	nr_seq_motivo_rescisao_obito,
	nr_seq_motivo_via,
	nr_seq_plano_usu_eventual,
	nr_seq_relatorio,
	nr_seq_relatorio_cat,
	nr_seq_sit_trib,
	nr_seq_sit_trib_desp,
	nr_seq_tipo_avaliacao,
	nr_seq_trans_fin_baixa,
	nr_seq_trans_fin_baixa_conta,
	nr_seq_trans_fin_baixa_mens,
	nr_seq_trans_fin_baixa_prov,
	nr_seq_trans_fin_baixa_reemb,
	nr_seq_trans_fin_baixa_taxa,
	nr_seq_trans_fin_baixa_vend,
	nr_seq_trans_fin_con_mens,
	nr_seq_trans_fin_inadi,
	nr_sequencia,
	pr_juro_padrao,
	pr_multa_padrao,
	qt_dias_ativ_canal_com,
	qt_dias_protocolo,
	qt_dias_rescisao_mig,
	qt_dias_venc_primeira_mens,
	qt_idade_limite,
	qt_min_limite_analise,
	qt_tempo_ausente,
	qt_tempo_limite,
	ie_agrup_contratos,
	ie_interc_eventual,
	ie_ativo_garantidor,
	ie_contrap_pecun,
	ie_corresp_cedida
into	cd_banco_desc_folha_w,
	cd_centro_custo_w,
	cd_cgc_ans_w,
	cd_condicao_pagamento_w,
	cd_conta_financ_w,
	cd_conta_financ_conta_w,
	cd_conta_financ_prov_w,
	cd_conta_financ_reembolso_w,
	cd_conta_financ_ressarc_w,
	cd_conta_financ_taxa_w,
	cd_estabelecimento_w,
	cd_interface_imp_benef_w,
	cd_moeda_adiantamento_w,
	cd_natureza_operacao_w,
	cd_natureza_operacao_desp_w,
	cd_operacao_desp_nf_w,
	cd_operacao_nf_w,
	cd_perfil_comunic_integr_w,
	cd_portador_w,
	cd_serie_desp_nf_w,
	cd_serie_nf_w,
	cd_tipo_portador_w,
	cd_tipo_receb_adiantamento_w,
	cd_tipo_recebimento_w,
	cd_tipo_receb_inadimplencia_w,
	cd_tipo_taxa_juro_w,
	cd_tipo_taxa_multa_w,
	ds_mascara_cart_padrao_w,
	dt_base_validade_carteira_w,
	dt_base_validade_cart_inter_w,
	ie_analise_cm_nova_w,
	ie_arredondar_tps_w,
	ie_atualizar_valor_apresent_w,
	ie_balancete_ativo_w,
	ie_balancete_despesa_w,
	ie_balancete_passivo_w,
	ie_balancete_receita_w,
	ie_base_venc_lote_pag_w,
	ie_bloqueto_deb_auto_w,
	ie_calculo_base_glosa_w,
	ie_calculo_coparticipacao_w,
	ie_calculo_pacote_w,
	ie_calculo_pos_estab_w,
	ie_cancela_lib_matmed_w,
	ie_carencia_abrangencia_ant_w,
	ie_cobertura_assistencial_w,
	ie_cobranca_pos_w,
	ie_considerar_dt_liq_cancel_w,
	ie_consiste_prest_envio_a500_w,
	ie_contab_prov_copartic_w,
	ie_copartic_grau_partic_w,
	ie_corresp_diops_w,
	ie_corresponsabilidade_w,
	ie_data_base_proporcional_w,
	ie_data_base_prot_reap_w,
	ie_data_referencia_nf_w,
	ie_data_ref_reaj_adaptado_w,
	ie_define_se_gera_analise_w,
	ie_dt_protocolo_a500_w,
	ie_esquema_contabil_w,
	ie_eventos_sinistros_w,
	ie_exige_lib_bras_w,
	ie_fluxo_caixa_w,
	ie_fluxo_contest_w,
	ie_fora_linha_simpro_w,
	ie_fundos_comuns_w,
	ie_geracao_coparticipacao_w,
	ie_geracao_nota_titulo_w,
	ie_geracao_nota_titulo_pf_w,
	ie_geracao_pos_estabelecido_w,
	ie_gerar_coparticipacao_w,
	ie_gerar_copartic_zerado_w,
	ie_gerar_glosa_valor_zerado_w,
	ie_gerar_usuario_even_rep_w,
	ie_gerar_usuario_eventual_w,
	ie_gerar_validade_cartao_w,
	ie_guia_unica_w,
	ie_hash_conta_w,
	ie_idade_saldo_w,
	ie_importacao_material_w,
	ie_intercambio_w,
	ie_isentar_carencia_nasc_w,
	ie_lucro_prejuizo_w,
	ie_material_intercambio_w,
	ie_material_ops_w,
	ie_material_rede_propria_w,
	ie_mes_cobranca_reaj_w,
	ie_mes_cobranca_reaj_reg_w,
	ie_movto_pel_w,
	ie_obriga_vinc_mat_w,
	ie_origem_tit_pagar_w,
	ie_origem_tit_reembolso_w,
	ie_origem_tit_taxa_saude_w,
	ie_origem_titulo_w,
	ie_pagador_contrato_princ_w,
	ie_permite_cnes_duplic_w,
	ie_pessoa_contrato_w,
	ie_pos_estab_faturamento_w,
	ie_pos_estab_fat_resc_w,
	ie_preco_prestador_partic_w,
	ie_pro_rata_dia_w,
	ie_quebrar_fatura_ptu_tx_w,
	ie_reajustar_benef_cancelado_w,
	ie_reajuste_w,
	ie_reajuste_faixa_etaria_w,
	ie_ref_copartic_mens_w,
	ie_regra_autogerado_w,
	ie_renovacao_cartao_dt_atual_w,
	ie_restringe_conjuge_w,
	ie_scpa_contrato_w,
	ie_solvencia_w,
	ie_taxa_grau_partic_w,
	ie_tela_guia_cm_w,
	ie_tipo_feriado_w,
	ie_tipo_geracao_a800_w,
	ie_tipo_lote_baixa_mens_w,
	ie_utiliza_material_ops_w,
	ie_venc_cart_fim_mes_w,
	ie_via_acesso_regra_w,
	nr_seq_agente_motivador_w,
	nr_seq_classif_fiscal_w,
	nr_seq_conta_banco_w,
	nr_seq_conta_banco_desc_w,
	nr_seq_emissor_w,
	nr_seq_emissor_provisorio_w,
	nr_seq_estrut_mat_rev_w,
	nr_seq_grupo_pre_analise_w,
	nr_seq_modelo_w,
	nr_seq_motivo_adaptacao_w,
	nr_seq_motivo_cancel_agenda_w,
	nr_seq_motivo_inclusao_w,
	nr_seq_motivo_rescisao_w,
	nr_seq_motivo_rescisao_a100_w,
	nr_seq_motivo_rescisao_obito_w,
	nr_seq_motivo_via_w,
	nr_seq_plano_usu_eventual_w,
	nr_seq_relatorio_w,
	nr_seq_relatorio_cat_w,
	nr_seq_sit_trib_w,
	nr_seq_sit_trib_desp_w,
	nr_seq_tipo_avaliacao_w,
	nr_seq_trans_fin_baixa_w,
	nr_seq_trans_fin_baixa_conta_w,
	nr_seq_trans_fin_baixa_mens_w,
	nr_seq_trans_fin_baixa_prov_w,
	nr_seq_trans_fin_baixa_reemb_w,
	nr_seq_trans_fin_baixa_taxa_w,
	nr_seq_trans_fin_baixa_vend_w,
	nr_seq_trans_fin_con_mens_w,
	nr_seq_trans_fin_inadi_w,
	nr_sequencia_w,
	pr_juro_padrao_w,
	pr_multa_padrao_w,
	qt_dias_ativ_canal_com_w,
	qt_dias_protocolo_w,
	qt_dias_rescisao_mig_w,
	qt_dias_venc_primeira_mens_w,
	qt_idade_limite_w,
	qt_min_limite_analise_w,
	qt_tempo_ausente_w,
	qt_tempo_limite_w,
	ie_agrup_contratos_w,
	ie_interc_eventual_w,
	ie_ativo_garantidor_w,
	ie_contrap_pecun_w,
	ie_corresp_cedida_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;
exception
when no_data_found then
	wheb_mensagem_pck.exibir_mensagem_abort(216905,'CD_ESTABELECIMENTO=' || cd_estabelecimento_p || ';' || 'NM_ESTABELECIMENTO=' || obter_nome_estabelecimento(cd_estabelecimento_p));
end;

if 	(upper(nm_atributo_p) = 'CD_BANCO_DESC_FOLHA') then 
	ds_retorno_p := 	cd_banco_desc_folha_w;
elsif (upper(nm_atributo_p) = 'CD_CENTRO_CUSTO') then 
	ds_retorno_p :=  cd_centro_custo_w;
elsif (upper(nm_atributo_p) = 'CD_CGC_ANS') then
	ds_retorno_p :=  cd_cgc_ans_w; 
elsif (upper(nm_atributo_p) = 'CD_CONDICAO_PAGAMENTO') then 
	ds_retorno_p :=  cd_condicao_pagamento_w;
elsif (upper(nm_atributo_p) = 'CD_CONTA_FINANC') then 
	ds_retorno_p :=  cd_conta_financ_w;
elsif (upper(nm_atributo_p) = 'CD_CONTA_FINANC_CONTA') then 
	ds_retorno_p :=  cd_conta_financ_conta_w;
elsif (upper(nm_atributo_p) = 'CD_CONTA_FINANC_PROV') then
	ds_retorno_p :=  cd_conta_financ_prov_w; 
elsif (upper(nm_atributo_p) = 'CD_CONTA_FINANC_REEMBOLSO') then 
	ds_retorno_p :=  cd_conta_financ_reembolso_w;
elsif (upper(nm_atributo_p) = 'CD_CONTA_FINANC_RESSARCIMENTO') then 
	ds_retorno_p :=  cd_conta_financ_ressarc_w;
elsif (upper(nm_atributo_p) = 'CD_CONTA_FINANC_TAXA') then
	ds_retorno_p :=  cd_conta_financ_taxa_w; 
elsif (upper(nm_atributo_p) = 'CD_ESTABELECIMENTO') then
	ds_retorno_p :=  cd_estabelecimento_w; 
elsif (upper(nm_atributo_p) = 'CD_INTERFACE_IMPORTACAO_BENEF') then 
	ds_retorno_p :=  cd_interface_imp_benef_w;
elsif (upper(nm_atributo_p) = 'CD_MOEDA_ADIANTAMENTO') then 
	ds_retorno_p :=  cd_moeda_adiantamento_w;
elsif (upper(nm_atributo_p) = 'CD_NATUREZA_OPERACAO') then
	ds_retorno_p :=  cd_natureza_operacao_w; 
elsif (upper(nm_atributo_p) = 'CD_NATUREZA_OPERACAO_DESP') then 
	ds_retorno_p :=  cd_natureza_operacao_desp_w;
elsif (upper(nm_atributo_p) = 'CD_OPERACAO_DESP_NF') then 
	ds_retorno_p :=  cd_operacao_desp_nf_w;
elsif (upper(nm_atributo_p) = 'CD_OPERACAO_NF') then
	ds_retorno_p :=  cd_operacao_nf_w; 
elsif (upper(nm_atributo_p) = 'CD_PERFIL_COMUNIC_INTEGR') then
	ds_retorno_p :=  cd_perfil_comunic_integr_w; 
elsif (upper(nm_atributo_p) = 'CD_PORTADOR') then 
	ds_retorno_p :=  cd_portador_w;
elsif (upper(nm_atributo_p) = 'CD_SERIE_DESP_NF') then
	ds_retorno_p :=  cd_serie_desp_nf_w; 
elsif (upper(nm_atributo_p) = 'CD_SERIE_NF') then 
	ds_retorno_p :=  cd_serie_nf_w;
elsif (upper(nm_atributo_p) = 'CD_TIPO_PORTADOR') then
	ds_retorno_p :=  cd_tipo_portador_w; 
elsif (upper(nm_atributo_p) = 'CD_TIPO_RECEB_ADIANTAMENTO') then
	ds_retorno_p :=  cd_tipo_receb_adiantamento_w; 
elsif (upper(nm_atributo_p) = 'CD_TIPO_RECEBIMENTO') then 
	ds_retorno_p :=  cd_tipo_recebimento_w;
elsif (upper(nm_atributo_p) = 'CD_TIPO_RECEB_INADIMPLENCIA') then 
	ds_retorno_p :=  cd_tipo_receb_inadimplencia_w;
elsif (upper(nm_atributo_p) = 'CD_TIPO_TAXA_JURO') then 
	ds_retorno_p :=  cd_tipo_taxa_juro_w;
elsif (upper(nm_atributo_p) = 'CD_TIPO_TAXA_MULTA') then
	ds_retorno_p :=  cd_tipo_taxa_multa_w; 
elsif (upper(nm_atributo_p) = 'DS_MASCARA_CART_PADRAO') then
	ds_retorno_p :=  ds_mascara_cart_padrao_w; 
elsif (upper(nm_atributo_p) = 'DT_BASE_VALIDADE_CARTEIRA') then 
	ds_retorno_p :=  dt_base_validade_carteira_w;
elsif (upper(nm_atributo_p) = 'DT_BASE_VALIDADE_CART_INTER') then 
	ds_retorno_p :=  dt_base_validade_cart_inter_w;
elsif (upper(nm_atributo_p) = 'IE_AGRUP_CONTRATOS') then 
	ds_retorno_p :=  ie_agrup_contratos_w;	
elsif (upper(nm_atributo_p) = 'IE_ANALISE_CM_NOVA') then
	ds_retorno_p :=  ie_analise_cm_nova_w; 
elsif (upper(nm_atributo_p) = 'IE_ARREDONDAR_TPS') then 
	ds_retorno_p :=  ie_arredondar_tps_w;
elsif (upper(nm_atributo_p) = 'IE_ATIVO_GARANTIDOR') then 
	ds_retorno_p :=  ie_ativo_garantidor_w;
elsif (upper(nm_atributo_p) = 'IE_ATUALIZAR_VALOR_APRESENT') then 
	ds_retorno_p :=  ie_atualizar_valor_apresent_w;
elsif (upper(nm_atributo_p) = 'IE_BALANCETE_ATIVO') then
	ds_retorno_p :=  ie_balancete_ativo_w; 
elsif (upper(nm_atributo_p) = 'IE_BALANCETE_DESPESA') then
	ds_retorno_p :=  ie_balancete_despesa_w; 
elsif (upper(nm_atributo_p) = 'IE_BALANCETE_PASSIVO') then
	ds_retorno_p :=  ie_balancete_passivo_w; 
elsif (upper(nm_atributo_p) = 'IE_BALANCETE_RECEITA') then
	ds_retorno_p :=  ie_balancete_receita_w; 
elsif (upper(nm_atributo_p) = 'IE_BASE_VENC_LOTE_PAG') then 
	ds_retorno_p :=  ie_base_venc_lote_pag_w;
elsif (upper(nm_atributo_p) = 'IE_BLOQUETO_DEB_AUTO') then
	ds_retorno_p :=  ie_bloqueto_deb_auto_w; 
elsif (upper(nm_atributo_p) = 'IE_CALCULO_BASE_GLOSA') then 
	ds_retorno_p :=  ie_calculo_base_glosa_w;
elsif (upper(nm_atributo_p) = 'IE_CALCULO_COPARTICIPACAO') then 
	ds_retorno_p :=  ie_calculo_coparticipacao_w;
elsif (upper(nm_atributo_p) = 'IE_CALCULO_PACOTE') then 
	ds_retorno_p :=  ie_calculo_pacote_w;
elsif (upper(nm_atributo_p) = 'IE_CALCULO_POS_ESTAB') then
	ds_retorno_p :=  ie_calculo_pos_estab_w; 
elsif (upper(nm_atributo_p) = 'IE_CANCELA_LIB_MATMED') then 
	ds_retorno_p :=  ie_cancela_lib_matmed_w;
elsif (upper(nm_atributo_p) = 'IE_CARENCIA_ABRANGENCIA_ANT') then 
	ds_retorno_p :=  ie_carencia_abrangencia_ant_w;
elsif (upper(nm_atributo_p) = 'IE_COBERTURA_ASSISTENCIAL') then 
	ds_retorno_p :=  ie_cobertura_assistencial_w;
elsif (upper(nm_atributo_p) = 'IE_COBRANCA_POS') then 
	ds_retorno_p :=  ie_cobranca_pos_w;
elsif (upper(nm_atributo_p) = 'IE_CONSIDERAR_DT_LIQ_CANCEL') then 
	ds_retorno_p :=  ie_considerar_dt_liq_cancel_w;
elsif (upper(nm_atributo_p) = 'IE_CONSISTE_PREST_ENVIO_A500') then
	ds_retorno_p :=  ie_consiste_prest_envio_a500_w; 
elsif (upper(nm_atributo_p) = 'IE_CONTAB_PROV_COPARTIC') then 
	ds_retorno_p :=  ie_contab_prov_copartic_w;
elsif (upper(nm_atributo_p) = 'IE_CONTRAP_PECUN') then
	ds_retorno_p :=  ie_contrap_pecun_w;
elsif (upper(nm_atributo_p) = 'IE_COPARTIC_GRAU_PARTIC') then 
	ds_retorno_p :=  ie_copartic_grau_partic_w;
elsif (upper(nm_atributo_p) = 'IE_CORRESP_CEDIDA') then
	ds_retorno_p :=  ie_corresp_cedida_w;
elsif (upper(nm_atributo_p) = 'IE_CORRESP_DIOPS') then
	ds_retorno_p :=  ie_corresp_diops_w; 
elsif (upper(nm_atributo_p) = 'IE_CORRESPONSABILIDADE') then
	ds_retorno_p :=  ie_corresponsabilidade_w; 
elsif (upper(nm_atributo_p) = 'IE_DATA_BASE_PROPORCIONAL') then 
	ds_retorno_p :=  ie_data_base_proporcional_w;
elsif (upper(nm_atributo_p) = 'IE_DATA_BASE_PROT_REAP') then
	ds_retorno_p :=  ie_data_base_prot_reap_w; 
elsif (upper(nm_atributo_p) = 'IE_DATA_REFERENCIA_NF') then 
	ds_retorno_p :=  ie_data_referencia_nf_w;
elsif (upper(nm_atributo_p) = 'IE_DATA_REF_REAJ_ADAPTADO') then 
	ds_retorno_p :=  ie_data_ref_reaj_adaptado_w;
elsif (upper(nm_atributo_p) = 'IE_DEFINE_SE_GERA_ANALISE') then 
	ds_retorno_p :=  ie_define_se_gera_analise_w;
elsif (upper(nm_atributo_p) = 'IE_DT_PROTOCOLO_A500') then
	ds_retorno_p :=  ie_dt_protocolo_a500_w; 
elsif (upper(nm_atributo_p) = 'IE_ESQUEMA_CONTABIL') then 
	ds_retorno_p :=  ie_esquema_contabil_w;
elsif (upper(nm_atributo_p) = 'IE_EVENTOS_SINISTROS') then
	ds_retorno_p :=  ie_eventos_sinistros_w; 
elsif (upper(nm_atributo_p) = 'IE_EXIGE_LIB_BRAS') then 
	ds_retorno_p :=  ie_exige_lib_bras_w;
elsif (upper(nm_atributo_p) = 'IE_FLUXO_CAIXA') then
	ds_retorno_p :=  ie_fluxo_caixa_w; 
elsif (upper(nm_atributo_p) = 'IE_FLUXO_CONTEST') then
	ds_retorno_p :=  ie_fluxo_contest_w; 
elsif (upper(nm_atributo_p) = 'IE_FORA_LINHA_SIMPRO') then
	ds_retorno_p :=  ie_fora_linha_simpro_w; 
elsif (upper(nm_atributo_p) = 'IE_FUNDOS_COMUNS') then
	ds_retorno_p :=  ie_fundos_comuns_w; 
elsif (upper(nm_atributo_p) = 'IE_GERACAO_COPARTICIPACAO') then 
	ds_retorno_p :=  ie_geracao_coparticipacao_w;
elsif (upper(nm_atributo_p) = 'IE_GERACAO_NOTA_TITULO') then
	ds_retorno_p :=  ie_geracao_nota_titulo_w; 
elsif (upper(nm_atributo_p) = 'IE_GERACAO_NOTA_TITULO_PF') then 
	ds_retorno_p :=  ie_geracao_nota_titulo_pf_w;
elsif (upper(nm_atributo_p) = 'IE_GERACAO_POS_ESTABELECIDO') then 
	ds_retorno_p :=  ie_geracao_pos_estabelecido_w;
elsif (upper(nm_atributo_p) = 'IE_GERAR_COPARTICIPACAO') then 
	ds_retorno_p :=  ie_gerar_coparticipacao_w;
elsif (upper(nm_atributo_p) = 'IE_GERAR_COPARTIC_ZERADO') then
	ds_retorno_p :=  ie_gerar_copartic_zerado_w; 
elsif (upper(nm_atributo_p) = 'IE_GERAR_GLOSA_VALOR_ZERADO') then 
	ds_retorno_p :=  ie_gerar_glosa_valor_zerado_w;
elsif (upper(nm_atributo_p) = 'IE_GERAR_USUARIO_EVEN_REPASSE') then 
	ds_retorno_p :=  ie_gerar_usuario_even_rep_w;
elsif (upper(nm_atributo_p) = 'IE_GERAR_USUARIO_EVENTUAL') then 
	ds_retorno_p :=  ie_gerar_usuario_eventual_w;
elsif (upper(nm_atributo_p) = 'IE_GERAR_VALIDADE_CARTAO') then
	ds_retorno_p :=  ie_gerar_validade_cartao_w; 
elsif (upper(nm_atributo_p) = 'IE_GUIA_UNICA') then 
	ds_retorno_p :=  ie_guia_unica_w;
elsif (upper(nm_atributo_p) = 'IE_HASH_CONTA') then 
	ds_retorno_p :=  ie_hash_conta_w;
elsif (upper(nm_atributo_p) = 'IE_IDADE_SALDO') then
	ds_retorno_p :=  ie_idade_saldo_w; 
elsif (upper(nm_atributo_p) = 'IE_IMPORTACAO_MATERIAL') then
	ds_retorno_p :=  ie_importacao_material_w;
elsif (upper(nm_atributo_p) = 'IE_INTERC_EVENTUAL') then 
	ds_retorno_p :=  ie_interc_eventual_w;	 
elsif (upper(nm_atributo_p) = 'IE_INTERCAMBIO') then
	ds_retorno_p :=  ie_intercambio_w; 
elsif (upper(nm_atributo_p) = 'IE_ISENTAR_CARENCIA_NASC') then
	ds_retorno_p :=  ie_isentar_carencia_nasc_w; 
elsif (upper(nm_atributo_p) = 'IE_LUCRO_PREJUIZO') then 
	ds_retorno_p :=  ie_lucro_prejuizo_w;
elsif (upper(nm_atributo_p) = 'IE_MATERIAL_INTERCAMBIO') then 
	ds_retorno_p :=  ie_material_intercambio_w;
elsif (upper(nm_atributo_p) = 'IE_MATERIAL_OPS') then 
	ds_retorno_p :=  ie_material_ops_w;
elsif (upper(nm_atributo_p) = 'IE_MATERIAL_REDE_PROPRIA') then
	ds_retorno_p :=  ie_material_rede_propria_w; 
elsif (upper(nm_atributo_p) = 'IE_MES_COBRANCA_REAJ') then
	ds_retorno_p :=  ie_mes_cobranca_reaj_w; 
elsif (upper(nm_atributo_p) = 'IE_MES_COBRANCA_REAJ_REG') then
	ds_retorno_p :=  ie_mes_cobranca_reaj_reg_w;
elsif (upper(nm_atributo_p) = 'IE_MOVTO_PEL') then
	ds_retorno_p :=  ie_movto_pel_w;	
elsif (upper(nm_atributo_p) = 'IE_OBRIGA_VINC_MAT') then
	ds_retorno_p :=  ie_obriga_vinc_mat_w; 
elsif (upper(nm_atributo_p) = 'IE_ORIGEM_TIT_PAGAR') then 
	ds_retorno_p :=  ie_origem_tit_pagar_w;
elsif (upper(nm_atributo_p) = 'IE_ORIGEM_TIT_REEMBOLSO') then 
	ds_retorno_p :=  ie_origem_tit_reembolso_w;
elsif (upper(nm_atributo_p) = 'IE_ORIGEM_TIT_TAXA_SAUDE') then
	ds_retorno_p :=  ie_origem_tit_taxa_saude_w; 
elsif (upper(nm_atributo_p) = 'IE_ORIGEM_TITULO') then
	ds_retorno_p :=  ie_origem_titulo_w; 
elsif (upper(nm_atributo_p) = 'IE_PAGADOR_CONTRATO_PRINC') then 
	ds_retorno_p :=  ie_pagador_contrato_princ_w;
elsif (upper(nm_atributo_p) = 'IE_PERMITE_CNES_DUPLIC') then
	ds_retorno_p :=  ie_permite_cnes_duplic_w; 
elsif (upper(nm_atributo_p) = 'IE_PESSOA_CONTRATO') then
	ds_retorno_p :=  ie_pessoa_contrato_w; 
elsif (upper(nm_atributo_p) = 'IE_POS_ESTAB_FATURAMENTO') then
	ds_retorno_p :=  ie_pos_estab_faturamento_w; 
elsif (upper(nm_atributo_p) = 'IE_POS_ESTAB_FAT_RESC') then
	ds_retorno_p :=  ie_pos_estab_fat_resc_w; 
elsif (upper(nm_atributo_p) = 'IE_PRECO_PRESTADOR_PARTIC') then 
	ds_retorno_p :=  ie_preco_prestador_partic_w;
elsif (upper(nm_atributo_p) = 'IE_PRO_RATA_DIA') then 
	ds_retorno_p :=  ie_pro_rata_dia_w;
elsif (upper(nm_atributo_p) = 'IE_QUEBRAR_FATURA_PTU_TX') then
	ds_retorno_p :=  ie_quebrar_fatura_ptu_tx_w; 
elsif (upper(nm_atributo_p) = 'IE_REAJUSTAR_BENEF_CANCELADO') then
	ds_retorno_p :=  ie_reajustar_benef_cancelado_w; 
elsif (upper(nm_atributo_p) = 'IE_REAJUSTE') then 
	ds_retorno_p :=  ie_reajuste_w;
elsif (upper(nm_atributo_p) = 'IE_REAJUSTE_FAIXA_ETARIA') then
	ds_retorno_p :=  ie_reajuste_faixa_etaria_w; 
elsif (upper(nm_atributo_p) = 'IE_REF_COPARTIC_MENS') then
	ds_retorno_p :=  ie_ref_copartic_mens_w; 
elsif (upper(nm_atributo_p) = 'IE_REGRA_AUTOGERADO') then 
	ds_retorno_p :=  ie_regra_autogerado_w;
elsif (upper(nm_atributo_p) = 'IE_RENOVACAO_CARTAO_DT_ATUAL') then
	ds_retorno_p :=  ie_renovacao_cartao_dt_atual_w; 
elsif (upper(nm_atributo_p) = 'IE_RESTRINGE_CONJUGE') then
	ds_retorno_p :=  ie_restringe_conjuge_w; 
elsif (upper(nm_atributo_p) = 'IE_SCPA_CONTRATO') then
	ds_retorno_p :=  ie_scpa_contrato_w; 
elsif (upper(nm_atributo_p) = 'IE_SOLVENCIA') then
	ds_retorno_p :=  ie_solvencia_w; 
elsif (upper(nm_atributo_p) = 'IE_TAXA_GRAU_PARTIC') then 
	ds_retorno_p :=  ie_taxa_grau_partic_w;
elsif (upper(nm_atributo_p) = 'IE_TELA_GUIA_CM') then 
	ds_retorno_p :=  ie_tela_guia_cm_w;
elsif (upper(nm_atributo_p) = 'IE_TIPO_FERIADO') then 
	ds_retorno_p :=  ie_tipo_feriado_w;
elsif (upper(nm_atributo_p) = 'IE_TIPO_GERACAO_A800') then
	ds_retorno_p :=  ie_tipo_geracao_a800_w; 
elsif (upper(nm_atributo_p) = 'IE_TIPO_LOTE_BAIXA_MENS') then 
	ds_retorno_p :=  ie_tipo_lote_baixa_mens_w;
elsif (upper(nm_atributo_p) = 'IE_UTILIZA_MATERIAL_OPS') then 
	ds_retorno_p :=  ie_utiliza_material_ops_w;
elsif (upper(nm_atributo_p) = 'IE_VENC_CART_FIM_MES') then
	ds_retorno_p :=  ie_venc_cart_fim_mes_w; 
elsif (upper(nm_atributo_p) = 'IE_VIA_ACESSO_REGRA') then 
	ds_retorno_p :=  ie_via_acesso_regra_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_AGENTE_MOTIVADOR') then 
	ds_retorno_p :=  nr_seq_agente_motivador_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_CLASSIF_FISCAL') then 
	ds_retorno_p :=  nr_seq_classif_fiscal_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_CONTA_BANCO') then
	ds_retorno_p :=  nr_seq_conta_banco_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_CONTA_BANCO_DESC') then 
	ds_retorno_p :=  nr_seq_conta_banco_desc_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_EMISSOR') then
	ds_retorno_p :=  nr_seq_emissor_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_EMISSOR_PROVISORIO') then 
	ds_retorno_p :=  nr_seq_emissor_provisorio_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_ESTRUT_MAT_REV') then 
	ds_retorno_p :=  nr_seq_estrut_mat_rev_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_GRUPO_PRE_ANALISE') then
	ds_retorno_p :=  nr_seq_grupo_pre_analise_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_MODELO') then 
	ds_retorno_p :=  nr_seq_modelo_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_ADAPTACAO') then 
	ds_retorno_p :=  nr_seq_motivo_adaptacao_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_CANCEL_AGENDA') then 
	ds_retorno_p :=  nr_seq_motivo_cancel_agenda_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_INCLUSAO') then
	ds_retorno_p :=  nr_seq_motivo_inclusao_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_RESCISAO') then
	ds_retorno_p :=  nr_seq_motivo_rescisao_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_RESCISAO_A100') then 
	ds_retorno_p :=  nr_seq_motivo_rescisao_a100_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_RESCISAO_OBITO') then
	ds_retorno_p :=  nr_seq_motivo_rescisao_obito_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_MOTIVO_VIA') then 
	ds_retorno_p :=  nr_seq_motivo_via_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_PLANO_USU_EVENTUAL') then 
	ds_retorno_p :=  nr_seq_plano_usu_eventual_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_RELATORIO') then
	ds_retorno_p :=  nr_seq_relatorio_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_RELATORIO_CAT') then
	ds_retorno_p :=  nr_seq_relatorio_cat_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_SIT_TRIB') then 
	ds_retorno_p :=  nr_seq_sit_trib_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_SIT_TRIB_DESP') then
	ds_retorno_p :=  nr_seq_sit_trib_desp_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_TIPO_AVALIACAO') then 
	ds_retorno_p :=  nr_seq_tipo_avaliacao_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA') then
	ds_retorno_p :=  nr_seq_trans_fin_baixa_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA_CONTA') then
	ds_retorno_p :=  nr_seq_trans_fin_baixa_conta_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA_MENS') then 
	ds_retorno_p :=  nr_seq_trans_fin_baixa_mens_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA_PROV') then 
	ds_retorno_p :=  nr_seq_trans_fin_baixa_prov_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA_REEMB') then
	ds_retorno_p :=  nr_seq_trans_fin_baixa_reemb_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA_TAXA') then 
	ds_retorno_p :=  nr_seq_trans_fin_baixa_taxa_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_BAIXA_VEND') then 
	ds_retorno_p :=  nr_seq_trans_fin_baixa_vend_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_CON_MENS') then 
	ds_retorno_p :=  nr_seq_trans_fin_con_mens_w;
elsif (upper(nm_atributo_p) = 'NR_SEQ_TRANS_FIN_INADI') then
	ds_retorno_p :=  nr_seq_trans_fin_inadi_w; 
elsif (upper(nm_atributo_p) = 'NR_SEQUENCIA') then
	ds_retorno_p :=  nr_sequencia_w; 
elsif (upper(nm_atributo_p) = 'PR_JURO_PADRAO') then
	ds_retorno_p :=  pr_juro_padrao_w; 
elsif (upper(nm_atributo_p) = 'PR_MULTA_PADRAO') then 
	ds_retorno_p :=  pr_multa_padrao_w;
elsif (upper(nm_atributo_p) = 'QT_DIAS_ATIV_CANAL_COM') then
	ds_retorno_p :=  qt_dias_ativ_canal_com_w; 
elsif (upper(nm_atributo_p) = 'QT_DIAS_PROTOCOLO') then 
	ds_retorno_p :=  qt_dias_protocolo_w;
elsif (upper(nm_atributo_p) = 'QT_DIAS_RESCISAO_MIG') then
	ds_retorno_p :=  qt_dias_rescisao_mig_w; 
elsif (upper(nm_atributo_p) = 'QT_DIAS_VENC_PRIMEIRA_MENS') then
	ds_retorno_p :=  qt_dias_venc_primeira_mens_w; 
elsif (upper(nm_atributo_p) = 'QT_IDADE_LIMITE') then 
	ds_retorno_p :=  qt_idade_limite_w;
elsif (upper(nm_atributo_p) = 'QT_MIN_LIMITE_ANALISE') then 
	ds_retorno_p :=  qt_min_limite_analise_w;
elsif (upper(nm_atributo_p) = 'QT_TEMPO_AUSENTE') then
	ds_retorno_p :=  qt_tempo_ausente_w; 
elsif (upper(nm_atributo_p) = 'QT_TEMPO_LIMITE') then 
	ds_retorno_p :=  qt_tempo_limite_w;
else
	wheb_mensagem_pck.exibir_mensagem_abort(216909,'DS_CAMPO=' || upper(nm_atributo_p));
end if;

ds_retorno_p	:= nvl(ds_retorno_p, ds_valor_padrao_p);

end pls_obter_pls_parametros;
/
