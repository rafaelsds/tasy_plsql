create or replace
procedure IMPORTAR_TABELA_PARAM_CONV
			(nm_usuario_p		in varchar2,
			cd_estabelecimento_p	in number,
			cd_convenio_p		in number,			
			nm_tabela_p		in varchar2,
			nm_atributo_p		in varchar2,
			vl_atributo_p		in varchar2) is

ds_comando_w		varchar2(2000);
ds_parametro_w		varchar2(255);
vl_atributo_w		varchar2(255);
			
begin

if	(nvl(vl_atributo_p,'XYZ') = 'XYZ') then
	vl_atributo_w	:= 'null';
else
	vl_atributo_w	:= chr(39)|| vl_atributo_p ||chr(39);
end if;

ds_comando_w	:= 	'update '|| nm_tabela_p ||' set '|| nm_atributo_p ||' = '|| vl_atributo_w ||' where cd_estabelecimento = :cd_estab and cd_convenio = :cd_conv';

ds_parametro_w	:= 	'cd_estab=' || cd_estabelecimento_p ||';'||'cd_conv=' || cd_convenio_p ||';';
	
begin
	Exec_sql_Dinamico_bv( nm_usuario_p, ds_comando_w, ds_parametro_w);
exception
when others then	
	null;
end;

commit;

end IMPORTAR_TABELA_PARAM_CONV;
/