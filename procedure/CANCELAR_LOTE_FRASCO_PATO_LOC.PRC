create or replace
procedure CANCELAR_LOTE_FRASCO_PATO_LOC(
			nr_seq_lote_atend_p		number,
			nm_usuario_p			varchar2) is 

begin

update	pato_lote_atend_rastreabi
set	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate,
	ie_status	=	'C'
where	nr_sequencia	=	nr_seq_lote_atend_p;

update	amost_lote_atend_rastreabi
set	ie_status 	= 	'C',
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_lote_atend	=	nr_seq_lote_atend_p;


insert into frasco_pato_loc_status (
	nr_sequencia, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec, 
	nm_usuario_nrec, 
	ie_status, 
	nr_seq_frasco_pato_loc,
	cd_setor_atendimento)
select	frasco_pato_loc_status_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	60,
	a.nr_seq_amostra,
	null
from	amost_lote_atend_rastreabi a
where	a.nr_seq_lote_atend	=	nr_seq_lote_atend_p;


update	frasco_pato_loc a
set	a.ie_status 		= 	60,
	a.nm_usuario		=	nm_usuario_p,
	a.dt_atualizacao	=	sysdate
where	exists (select	1
		from 	amost_lote_atend_rastreabi x
		where	x.nr_seq_amostra 	= a.nr_sequencia
		and	x.nr_seq_lote_atend	= nr_seq_lote_atend_p);


commit;

end CANCELAR_LOTE_FRASCO_PATO_LOC;
/
