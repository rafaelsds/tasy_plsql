create or replace
procedure hmc_gerar_relat_tit_rec_pos(	dt_inicial_p			date,
										dt_final_p				date,
										dt_pos_contab_p			date,
										ie_tp_data_p			number,
										cd_estabelecimento_p	varchar2,
										cd_conta_contabil_p		varchar2,
										ie_origem_p				varchar2,
										cd_pessoa_fisica_p		varchar2,
										cd_cgc_p				varchar2,
										nr_seq_classe_p			varchar2,
										cd_classificacao_p		varchar2,
										cd_relat_p				number) is 

cd_estabelecimento_w 		titulo_receber.cd_estabelecimento%type;
nr_titulo_w					titulo_receber.nr_titulo%type;
cd_pessoa_fisica_w			titulo_receber.cd_pessoa_fisica%type;
cd_cgc_w					titulo_receber.cd_cgc%type;
vl_titulo_w					titulo_receber.vl_titulo%type;
dt_emissao_w				titulo_receber.dt_emissao%type;
dt_vencimento_w				titulo_receber.dt_vencimento%type;
dt_pagamento_previsto_w		titulo_receber.dt_pagamento_previsto%type;
dt_contabil_w				titulo_receber.dt_contabil%type;
dt_liquidacao_w				titulo_receber.dt_liquidacao%type;
ie_origem_titulo_w			titulo_receber.ie_origem_titulo%type;
nr_seq_classe_w				titulo_receber.nr_seq_classe%type;
cd_conta_contabil_w			conta_contabil.cd_conta_contabil%type;
ie_tipo_contratacao_w		pls_plano.ie_tipo_contratacao%type;
ie_preco_w					pls_plano.ie_preco%type;
nm_usuario_w				usuario.nm_usuario%type;
dt_atualizacao_w			date;
cd_classificacao_w			conta_contabil.cd_classificacao%type;
cd_relat_w					number(10);
									
Cursor C01 is
	select	a.cd_estabelecimento,
			a.nr_titulo,
			a.cd_pessoa_fisica,
			a.cd_cgc,
			a.vl_titulo,
			a.dt_emissao,
			a.dt_vencimento,
			a.dt_pagamento_previsto,
			a.dt_contabil,
			a.dt_liquidacao,
			a.ie_origem_titulo,
			decode(a.cd_cgc,null, (	select	max(x.cd_conta_contabil)
									from	pessoa_fisica_conta_ctb x
									where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
									and		x.ie_tipo_conta = 'R'
									and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy')),
								  (	select	max(x.cd_conta_contabil)
									from	pessoa_jur_conta_cont x
									where	x.cd_cgc	= a.cd_cgc
									and		x.ie_tipo_conta = 'R'
									and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy')) ) cd_conta_contabil,
			d.ie_tipo_contratacao,
			d.ie_preco,
			a.nr_seq_classe,
			decode(a.cd_cgc,null, (	select max(y.cd_classificacao)
									from	conta_contabil y
									where	y.cd_conta_contabil = (	select	max(x.cd_conta_contabil)
																	from	pessoa_fisica_conta_ctb x
																	where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
																	and		x.ie_tipo_conta = 'R'
																	and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy'))),
								  (	select max(y.cd_classificacao)
									from	conta_contabil y
									where	y.cd_conta_contabil = ( select	max(x.cd_conta_contabil)
																	from	pessoa_jur_conta_cont x
																	where	x.cd_cgc	= a.cd_cgc
																	and		x.ie_tipo_conta = 'R'
																	and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy'))) ) cd_classificacao
			/*obter_saldo_tit_rec_pos_cont(a.nr_titulo, dt_pos_contab_p) vl_saldo*/
	from	titulo_receber a,
			pls_contrato  e,
		    pls_contrato_pagador b,
		    pls_contrato_plano c,
		    pls_plano  d
	where	1 = 1
	and 	e.nr_sequencia = b.nr_seq_contrato
	and     e.nr_sequencia = c.nr_seq_contrato
	and     d.nr_sequencia = c.nr_seq_plano
	and     b.nr_sequencia = a.nr_seq_pagador(+)	
	and 	fim_dia(nvl(a.dt_liquidacao,sysdate + 1000)) >= dt_pos_contab_p
	and 	a.dt_contabil <= dt_pos_contab_p
	and 	(a.dt_liquidacao is null or a.dt_liquidacao > dt_pos_contab_p)
	and		( (ie_tp_data_p = 0) or  
			  (ie_tp_data_p = 1 and a.dt_emissao between inicio_dia(dt_inicial_p) and fim_dia(dt_final_p)) or
			  (ie_tp_data_p = 2 and a.dt_vencimento between inicio_dia(dt_inicial_p) and fim_dia(dt_final_p)) or
			  (ie_tp_data_p = 3 and a.dt_pagamento_previsto between inicio_dia(dt_inicial_p) and fim_dia(dt_final_p)) or
			  (ie_tp_data_p = 4 and a.dt_contabil between inicio_dia(dt_inicial_p) and fim_dia(dt_final_p)) or
			  (ie_tp_data_p = 5 and a.dt_liquidacao between inicio_dia(dt_inicial_p) and fim_dia(dt_final_p))
			)
	and		( (cd_pessoa_fisica_p is null) or (obter_se_contido_char(a.cd_pessoa_fisica, Elimina_Caracter(cd_pessoa_fisica_p,' ')) = 'S') )	
	and		( (cd_cgc_p is null) or (obter_se_contido_char(a.cd_cgc, Elimina_Caracter(cd_cgc_p,' ')) = 'S') )
	and		( (cd_estabelecimento_p is null) or (obter_se_contido_char(a.cd_estabelecimento, Elimina_Caracter(cd_estabelecimento_p,' ')) = 'S') )
	and		( (cd_conta_contabil_p is null)  or (obter_se_contido_char(decode(a.cd_cgc,null, (	select	max(x.cd_conta_contabil)
																								from	pessoa_fisica_conta_ctb x
																								where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
																								and		x.ie_tipo_conta = 'R'
																								and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy')),
																							  (	select	max(x.cd_conta_contabil)
																								from	pessoa_jur_conta_cont x
																								where	x.cd_cgc	= a.cd_cgc
																								and		x.ie_tipo_conta = 'R'
																								and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy')) ), Elimina_Caracter(cd_conta_contabil_p,' ')) = 'S') )
	and		( (ie_origem_p is null)  or (obter_se_contido_char(a.ie_origem_titulo, Elimina_Caracter(ie_origem_p,' ')) = 'S') )
	and		( (nr_seq_classe_p is null)  or (obter_se_contido_char(a.nr_seq_classe, Elimina_Caracter(nr_seq_classe_p,' ')) = 'S') )
	and		( (cd_classificacao_p is null)  or (obter_se_contido_char(decode(a.cd_cgc,null, (	select max(y.cd_classificacao)
																								from	conta_contabil y
																								where	y.cd_conta_contabil = (	select	max(x.cd_conta_contabil)
																																from	pessoa_fisica_conta_ctb x
																																where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
																																and		x.ie_tipo_conta = 'R'
																																and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy'))),
																							  (	select max(y.cd_classificacao)
																								from	conta_contabil y
																								where	y.cd_conta_contabil = ( select	max(x.cd_conta_contabil)
																																from	pessoa_jur_conta_cont x
																																where	x.cd_cgc	= a.cd_cgc
																																and		x.ie_tipo_conta = 'R'
																																and		to_date(sysdate,'dd/mm/yyyy') between to_date(nvl(x.dt_inicio_vigencia,sysdate),'dd/mm/yyyy') and to_date(nvl(x.dt_fim_vigencia,sysdate),'dd/mm/yyyy'))) ), Elimina_Caracter(cd_classificacao_p,' ')) = 'S') )
	order	by nr_titulo asc;
										
begin

nm_usuario_w := obter_usuario_ativo;

if (dt_pos_contab_p is null) then
	Raise_application_error(-20011,'N�o foi informado no filtro a data de posi��o cont�bil! ');
end if;

delete from hmc_relat_tit_rec_pos
where	nm_usuario = nm_usuario_w
and		cd_relat = 1;

open C01;
loop
fetch C01 into	
	cd_estabelecimento_w,
	nr_titulo_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	vl_titulo_w,
	dt_emissao_w,
	dt_vencimento_w,
	dt_pagamento_previsto_w,
	dt_contabil_w,
	dt_liquidacao_w,
	ie_origem_titulo_w,
	cd_conta_contabil_w,
	ie_tipo_contratacao_w,
	ie_preco_w,
	nr_seq_classe_w,
	cd_classificacao_w;
exit when C01%notfound;
	begin
		
		insert into hmc_relat_tit_rec_pos (	cd_cgc,                  
											cd_conta_contabil,       
											cd_estabelecimento,      
											cd_pessoa_fisica,        
											dt_atualizacao,          
											dt_contabil,             
											dt_emissao,              
											dt_liquidacao,          
											dt_pagamento_previsto,
											dt_vencimento,           
											ie_origem_titulo,        
											ie_preco,                
											ie_tipo_contratacao,     
											nm_usuario,              
											nr_seq_classe,           
											nr_titulo,               
											vl_titulo,
											cd_classificacao,
											dt_pos_contab_filtro,
											cd_relat)
							values  	(	cd_cgc_w,
											cd_conta_contabil_w,
											cd_estabelecimento_w,
											cd_pessoa_fisica_w,
											sysdate,
											dt_contabil_w,
											dt_emissao_w,
											dt_liquidacao_w,
											dt_pagamento_previsto_w,
											dt_vencimento_w,
											ie_origem_titulo_w,
											ie_preco_w,
											ie_tipo_contratacao_w,
											nm_usuario_w,
											nr_seq_classe_w,
											nr_titulo_w,
											vl_titulo_w,
											cd_classificacao_w,
											dt_pos_contab_p,
											cd_relat_p);
										
							commit;			
	end;
end loop;
close C01;

end hmc_gerar_relat_tit_rec_pos;
/

/*
create table HMC_RELAT_TIT_REC_POS (
CD_ESTABELECIMENTO    NUMBER(10),
NR_TITULO             NUMBER(10),
CD_PESSOA_FISICA      VARCHAR2(10),
CD_CGC                VARCHAR2(14),
VL_TITULO             NUMBER(15,2),
DT_EMISSAO            DATE,
DT_VENCIMENTO         DATE,
DT_PAGAMENTO_PREVISTO DATE,
DT_CONTABIL           DATE,
DT_LIQUIDACAO         DATE,
IE_ORIGEM_TITULO      VARCHAR2(10),
NR_SEQ_CLASSE         NUMBER(10),
CD_CONTA_CONTABIL     VARCHAR2(10),
IE_TIPO_CONTRATACAO   VARCHAR2(2),
IE_PRECO              VARCHAR2(2),
NM_USUARIO            VARCHAR2(15),
DT_ATUALIZACAO        DATE,
CD_CLASSIFICACAO      VARCHAR2(40),
DT_POS_CONTAB_FILTRO  DATE,
CD_RELAT              NUMBER(10));*/