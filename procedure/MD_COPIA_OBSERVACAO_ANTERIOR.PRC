create or replace
procedure md_copia_observacao_anterior (nr_sequencia_p		number) is

ds_observacao_w		varchar2(2000);
dt_dieta_w		Date;
dt_dieta_ref_w		Date;
cd_refeicao_w		Varchar2(02);
cd_refeicao_ref_w	Varchar2(02);
cd_pessoa_fisica_w	Varchar2(10);
ds_tipo_dieta_w		Varchar2(10);
ie_existe_reg_data_w	varchar2(1);
i			Integer := 1;
qt_count		Integer := 0;
cd_dieta_w		mapa_dieta.cd_dieta%type;
	
begin

ds_tipo_dieta_w	:= 'XRCJTABLMD';

select	max(a.dt_dieta),
	max(cd_pessoa_fisica),
	max(cd_refeicao),
	max(cd_dieta)
into	dt_dieta_w,
	cd_pessoa_fisica_w,
	cd_refeicao_w,
	cd_dieta_w
from	mapa_dieta a
where	a.nr_sequencia = nr_Sequencia_p;

cd_refeicao_ref_w 	:= cd_refeicao_w;
dt_dieta_ref_w		:= dt_dieta_w;

begin
select	'S'
into	ie_existe_reg_data_w
from	mapa_dieta a
where	a.nr_Seq_superior is null
and	a.dt_dieta = dt_dieta_w
and	a.nr_sequencia <> nr_sequencia_p
and	a.cd_refeicao <> cd_refeicao_ref_w
and	a.cD_pessoa_fisica = cd_pessoa_fisica_w
--and	a.cd_refeicao <> cd_refeicao_w
and	a.ds_observacao is not null
and	substr(ds_tipo_dieta_w,instr(ds_tipo_dieta_w,cd_refeicao_ref_w),length(ds_tipo_dieta_w)) like('%'||a.cd_refeicao||'%')
and	rownum = 1;
exception
when others then
	ie_existe_reg_data_w	:= 'N';
end;

if (ie_existe_reg_data_w <> 'S') then
	cd_refeicao_ref_w	:= 'X';		
	select	max(a.dt_dieta)
	into	dt_dieta_ref_w
	from	mapa_dieta a
	where	a.nr_seq_superior is null
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	a.nr_sequencia <> nr_sequencia_p
	and	a.dt_dieta < dt_dieta_w
	and	a.ds_observacao is not null;
		
	ds_tipo_dieta_w	:= substr(ds_tipo_dieta_w,instr(ds_tipo_dieta_w,cd_refeicao_ref_w),length(ds_tipo_dieta_w));
else	
	ds_tipo_dieta_w	:= substr(ds_tipo_dieta_w,instr(ds_tipo_dieta_w,cd_refeicao_ref_w)+1,length(ds_tipo_dieta_w));
end if;

while (i <= length(ds_tipo_dieta_w)) and (ds_observacao_w is null) and (dt_dieta_ref_w is not null) and (qt_count <= 10) loop
	begin
	
	cd_refeicao_ref_w := substr(ds_tipo_dieta_w,i,1);
	
	select	max(a.ds_observacao)
	into	ds_observacao_w
	from	mapa_dieta a
	where	a.dt_dieta = dt_dieta_ref_w
	and	a.nr_sequencia <> nr_sequencia_p
	and	a.cd_refeicao = cd_refeicao_ref_w
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	a.cd_dieta = cd_dieta_w;
	
	if	(ds_observacao_w is null) then
		select	max(a.ds_observacao)
		into	ds_observacao_w
		from	mapa_dieta a
		where	a.dt_dieta = dt_dieta_ref_w
		and	a.nr_sequencia <> nr_sequencia_p
		and	a.cd_refeicao = cd_refeicao_ref_w
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
	end if;
	
	i 		:= i+1;
	qt_count	:= qt_count + 1;
	end;
end loop;

if (ds_observacao_w is not null) then
	update	mapa_dieta
	set	ds_observacao = ds_observacao_w
	where	nr_sequencia = nr_sequencia_p;
end if;

commit;

end md_copia_observacao_anterior;
/