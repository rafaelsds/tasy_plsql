create or replace
procedure HDH_GERAR_RETORNO_BRADESCO(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p	varchar2) is 

nr_titulo_w		number(10);
vl_juros_w		number(15,2);
vl_desconto_w		number(15,2);
vl_abatimento_w		number(15,2);
vl_liquido_w		number(15,2);
dt_liquidacao_w		varchar2(6);
ie_tipo_carteira_w		varchar2(255);
ds_titulo_w		varchar2(255);
vl_cobranca_w		number(15,2);
cd_ocorrencia_w		number(10);
cd_motivo_w		varchar2(10);
nr_seq_ocorrencia_ret_w	number(10);
vl_tarifa_cobranca_w	number(15,2);
NR_SEQ_OCORR_MOTIVO_w	NUMBER(10);
vl_saldo_titulo_w	number(15,2);
cd_banco_w		number(3);
cd_banco_cobr_w		number(5);
cd_centro_custo_desc_w	number(8);
nr_seq_motivo_desc_w	number(10);
nr_seq_tit_rec_cobr_w	number(10);
cd_pessoa_fisica_desc_w	varchar2(10);
cd_cgc_desc_w		varchar2(14);
ie_rejeitado_w		varchar2(1);
cd_estabelecimento_w	number(4);
cd_tipo_recebimento_w	number(5);

cursor c01 is
	select	trim(substr(ds_string,127,19)),
		to_number(substr(ds_string,153,13))/100,
		to_number(substr(ds_string,267,13))/100,
		to_number(substr(ds_string,241,13))/100,
		to_number(substr(ds_string,228,13))/100,
		to_number(substr(ds_string,254,13))/100,
		to_number(substr(ds_string,189,13))/100,
		substr(ds_string,296,6),
		substr(ds_string,109,2),
		substr(ds_string,319,2)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit		= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= '1';
begin


select	to_number(substr(ds_string,77,3))
into	cd_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,1,1)	= '0';

select	max(a.cd_banco),
	max(a.cd_estabelecimento)
into	cd_banco_cobr_w,
	cd_estabelecimento_w
from	cobranca_escritural a
where	a.nr_sequencia	= nr_seq_cobr_escrit_p;

if	(cd_banco_w <> cd_banco_cobr_w) then
	raise_application_error(-20011,	'O banco do arquivo � diferente do banco da cobran�a no sistema!' || chr(10) || chr(13) ||
					'Posi��o do c�digo do banco no arquivo: 77 - 79');
end if;

open C01;
loop
fetch C01 into	
	ds_titulo_w,
	vl_cobranca_w,
	vl_juros_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_tarifa_cobranca_w,
	dt_liquidacao_w,
	cd_ocorrencia_w,
	cd_motivo_w;
exit when C01%notfound;
	

	select	max(nr_titulo),
		nvl(max(vl_saldo_titulo),0)
	into	nr_titulo_w,
		vl_saldo_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	
	/* Se encontrou o t�tulo importa, sen�o grava no log */
	if	(nr_titulo_w is not null) then

		ie_tipo_carteira_w	:= OBTER_TIPO_CARTEIRA_REGRA(nr_titulo_w);

		update	titulo_receber
		set	ie_tipo_carteira	= ie_tipo_carteira_w
		where	nr_titulo		= nr_titulo_w
		and	ie_tipo_carteira	is null;

		select 	nvl(max(a.nr_sequencia),0),
			max(ie_rejeitado_w),
			max((select	max(x.nr_sequencia)
			from	banco_ocorr_motivo_ret x
			where	x.nr_seq_escrit_ret	= a.nr_sequencia)) nr_seq_ocorr_motivo_w
		into	nr_seq_ocorrencia_ret_w,
			ie_rejeitado_w,
			nr_seq_ocorr_motivo_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco	= 237
		and	a.cd_ocorrencia	= cd_ocorrencia_w;


		select	max(a.nr_sequencia)
		into	nr_seq_tit_rec_cobr_w
		from	titulo_receber_cobr a
		where	a.nr_titulo	= nr_titulo_w
		and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

		if	(nr_seq_tit_rec_cobr_w	is null) then	/* inserir o t�tulo se ele ainda n�o estiver na cobran�a */

			select	max(a.cd_centro_custo),
				max(a.nr_seq_motivo_desc),
				max(a.cd_pessoa_fisica),
				max(a.cd_cgc)
			into	cd_centro_custo_desc_w,
				nr_seq_motivo_desc_w,
				cd_pessoa_fisica_desc_w,
				cd_cgc_desc_w
			from	titulo_receber_liq_desc a
			where	a.nr_titulo	= nr_titulo_w
			and	a.nr_bordero	is null
			and	a.nr_seq_liq	is null;

			select	titulo_receber_cobr_seq.nextval
			into	nr_seq_tit_rec_cobr_w
			from	dual;

			insert	into titulo_receber_cobr
				(NR_SEQUENCIA,
				NR_TITULO,
				CD_BANCO,
				VL_COBRANCA,
				VL_DESCONTO,
				VL_ACRESCIMO,
				VL_DESPESA_BANCARIA,
				VL_LIQUIDACAO,
				VL_JUROS,
				DT_LIQUIDACAO,
				DT_ATUALIZACAO,
				NM_USUARIO,
				NR_SEQ_COBRANCA,
				nr_seq_ocorrencia_ret,
				NR_SEQ_OCORR_MOTIVO,
				vl_saldo_inclusao,
				VL_MULTA,
				nr_seq_motivo_desc,
				cd_centro_custo_desc)
			values	(nr_seq_tit_rec_cobr_w,
				nr_titulo_w,
				237,
				nvl(vl_cobranca_w,0),
				nvl(vl_desconto_w,0) + nvl(vl_abatimento_w,0),
				0,
				nvl(vl_tarifa_cobranca_w,0),
				nvl(vl_liquido_w,0),
				nvl(vl_juros_w,0),
				nvl(trim(dt_liquidacao_w),sysdate),
				sysdate,
				nm_usuario_p,
				nr_seq_cobr_escrit_p,
				nr_seq_ocorrencia_ret_w,
				NR_SEQ_OCORR_MOTIVO_w,
				nvl(vl_saldo_titulo_w,0),
				0,
				nr_seq_motivo_desc_w,
				cd_centro_custo_desc_w);

			insert	into titulo_rec_cobr_desc
				(cd_cgc,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tit_rec_cobr,
				nr_sequencia)
			values	(cd_cgc_desc_w,
				cd_pessoa_fisica_desc_w,
				sysdate,
				nm_usuario_p,
				nr_seq_tit_rec_cobr_w,
				titulo_rec_cobr_desc_seq.nextval);

		else
			-- raise_application_error(-20011, 'Data: ' || to_date(nvl(trim(dt_liquidacao_w),sysdate),'dd/mm/yy'));
			
			update	titulo_receber_cobr
			set 	vl_liquidacao		= vl_liquido_w,
				dt_liquidacao		= to_date(nvl(trim(dt_liquidacao_w),sysdate),'dd/mm/yy'),
				nr_seq_ocorrencia_ret	= nr_seq_ocorrencia_ret_w,
				nr_seq_ocorr_motivo	= nr_seq_ocorr_motivo_w
			where	nr_sequencia		= nr_seq_tit_rec_cobr_w;

		end if;

		baixar_titulos_cobranca_escrit(	nr_seq_cobr_escrit_p,
						'I',
						nm_usuario_p,
						sysdate,
						'0',
						'N');

	else
		insert	into log_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');

	end if;

end loop;
close C01;

commit;

end HDH_GERAR_RETORNO_BRADESCO;
/
