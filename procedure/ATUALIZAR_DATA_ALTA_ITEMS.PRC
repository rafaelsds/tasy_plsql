create or replace
procedure Atualizar_data_alta_items(nr_atendimento_p 	number,
					dt_alta_p 	date  ) is 
					
qt_proc_w	number(5);
qt_mat_w	number(5);
nr_sequencia_w 	number(10,0);
nr_sequencia_w2 	number(10,0);
	
Cursor C01 is
	select	x.nr_sequencia
	from 	procedimento_paciente 	x,
		conta_paciente		y
	where 	x.nr_interno_conta = y.nr_interno_conta
	and 	y.nr_atendimento   = nr_atendimento_p
	and	y.ie_status_acerto = 1
	and 	x.dt_conta > dt_alta_p;
	
Cursor C02 is
	select	x.nr_sequencia
	from 	material_atend_paciente 	x,
		conta_paciente			y
	where 	x.nr_interno_conta = y.nr_interno_conta
	and 	y.nr_atendimento   = nr_atendimento_p
	and	y.ie_status_acerto = 1
	and 	x.dt_conta > dt_alta_p;

begin

select	count(*)
into	qt_proc_w
from 	procedimento_paciente 	x,
	conta_paciente		y
where 	x.nr_interno_conta = y.nr_interno_conta
and 	y.nr_atendimento   = nr_atendimento_p
and	y.ie_status_acerto = 1
and 	x.dt_conta > dt_alta_p;


if (qt_proc_w > 0) then

	open C01;
	loop
	fetch C01 into	
		nr_Sequencia_w;
	exit when C01%notfound;
		begin
		
			
			update	procedimento_paciente a
			set	a.dt_conta = dt_alta_p
			where 	a.nr_atendimento = nr_atendimento_p
			and	a.nr_sequencia = nr_sequencia_w;
		end;
	end loop;
	close C01;
	
end if;

select	count(*)
into	qt_mat_w
from 	material_atend_paciente 	x,
	conta_paciente			y
where 	x.nr_interno_conta = y.nr_interno_conta
and 	y.nr_atendimento   = nr_atendimento_p
and	y.ie_status_acerto = 1
and 	x.dt_conta > dt_alta_p;

	
if (qt_mat_w > 0) then

	open C02;
	loop
	fetch C02 into	
		nr_sequencia_w2;
	exit when C02%notfound;
		begin
		
		
			update	material_atend_paciente a
			set	a.dt_conta = dt_alta_p
			where 	a.nr_atendimento = nr_atendimento_p
			and	a.nr_sequencia = nr_sequencia_w2;
		end;
	end loop;
	close C02;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Atualizar_data_alta_items;
/
