create or replace
procedure Ajustar_prescricao_copiada(	nr_prescricao_p		number,
					cd_medico_p		varchar2,
					dt_prescricao_p		date,
					dt_primeiro_horario_p	varchar2,
					nr_horas_validade_p	number,
					cd_perfil_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

dt_primeiro_horario_w		date;
dt_inicio_prescr_w		date;
dt_validade_prescr_w		date;
VarCalculaValidade_w		varchar2(5);
nr_horas_validade_w		number(15,0);
qt_total_dispensar_w		number(18,6);
qt_material_w			number(18,6);
qt_unitaria_w			number(18,6);
nr_atendimento_w		number(15,0);
cd_material_w			number(15,0);
VarEstenderPrescricao_w		varchar2(5);
ie_regra_disp_w			varchar2(50);
IE_VIA_APLICACAO_W		varchar2(50);
CD_UNIDADE_MEDIDA_DOSE_W	varchar2(50);
ie_origem_inf_w			varchar2(50);
hr_inicio_setor_w		varchar2(50);
cd_setor_atendimento_w		number(5,0);
dt_prescricao_w			date;

nr_sequencia_w			number(10,0);
nr_ocorrencia_w			number(10,0);
nr_seq_proc_ant_w		number(10,0);
nr_seq_w			number(15,0);
ie_ctrl_glic_w			varchar(3);
cd_intervalo_proc_w		varchar2(50);
IE_OPERACAO_W			varchar2(50) := 'X';
dt_prev_execucao_w		date;
dt_prev_execucao_ww		date;
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(5,0);
nr_prescricao_anterior_w	number(15,0);
ie_regra_medic_w		varchar2(15); 
ds_horarios_w			varchar2(4000);
ds_dose_diferenciada_w		varchar2(4000);
ds_horarios2_w			varchar2(4000);
DS_ERRO_W			varchar2(4000);
nr_prescricoes_w		varchar2(4000);
ds_horarios_ww			varchar2(4000);
ie_situacao_w			varchar2(4);
nr_seq_exame_w			number(15,0);
qt_hora_intervalo_w		number(15,0);
qt_min_intervalo_w		number(15,0);
ie_altera_dt_proxima_dose_w	varchar2(2);
qt_hora_intervalo_ww		number(15);
dt_proxima_dose_w		date;
ie_se_necessario_w		varchar2(1);
ie_acm_w			varchar2(1);
cd_intervalo_mat_w	intervalo_prescricao.cd_intervalo%type;
ie_intervalo_dif_w varchar2(1);
cd_intervalo_up_w intervalo_prescricao.cd_intervalo%type;


cursor c06 is
select	a.nr_sequencia,
	a.cd_intervalo,
	a.dt_prev_execucao,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.ds_horarios,
	nvl(b.ie_situacao,'A'),
	a.nr_seq_exame,
	a.qt_hora_intervalo,
	a.qt_min_intervalo
from	intervalo_prescricao b,
	Prescr_procedimento a,
	prescr_medica c
where	c.nr_prescricao	= nr_prescricao_p
and	c.nr_prescricao	= a.nr_prescricao
and	a.cd_intervalo	= b.cd_intervalo (+)
and	obter_se_exibe_proced(a.nr_prescricao,a.nr_sequencia,a.ie_tipo_proced,'O') = 'S';

cursor c07 is
select	qt_unitaria,
	ie_via_aplicacao,
	ds_dose_diferenciada,
	cd_unidade_medida_dose,
	nr_sequencia,
	cd_material,
	nvl(ie_se_necessario,'N'),
	nvl(ie_acm,'N'),
	cd_intervalo,
	ie_intervalo_dif
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia_proc	= nr_sequencia_w
and	ie_agrupador	= 5;

Cursor C09 is
select	b.nr_sequencia,
	obter_ocorrencia_intervalo(b.cd_intervalo,24,'H'),
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	b.dt_proxima_dose
from	prescr_material b,
	prescr_medica a
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	b.ie_agrupador	= 1
and	b.dt_suspensao is null
and	b.ie_origem_inf	<> 'K'
and	obter_ocorrencia_intervalo(b.cd_intervalo,24,'H') > 24;

begin

if	(nvl(nr_prescricao_p,0) > 0) then

	Obter_Param_Usuario(924,98,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,VarCalculaValidade_w);
	Obter_Param_Usuario(924,249,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,VarEstenderPrescricao_w);
	Obter_Param_Usuario(924,259,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_regra_medic_w);

	select	max(nr_atendimento),
		max(nr_prescricao_anterior),
		max(cd_setor_atendimento)		
	into	nr_atendimento_w,
		nr_prescricao_anterior_w,
		cd_setor_atendimento_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;
	
	if	(VarCalculaValidade_w	= 'R') then
		VarCalculaValidade_w	:= obter_se_calcula_validade(cd_setor_atendimento_w);
	end if;
	
	if	(VarCalculaValidade_w	<> 'N') then
		/*select	max(nvl(obter_horas_validade_prescr(sysdate,nr_atendimento_w,VarEstenderPrescricao_w,'A',sysdate,nr_prescricao_p),nr_horas_validade_p))
		into	nr_horas_validade_w
		from	dual;*/
		nr_horas_validade_w	:= nr_horas_validade_p;
	end if;

	select	to_date(to_char(dt_primeiro_horario,'dd/mm/yyyy') || dt_primeiro_horario_p || ':00','dd/mm/yyyy hh24:mi:ss'),
		dt_inicio_prescr
	into	dt_primeiro_horario_w,
		dt_inicio_prescr_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;

	dt_validade_prescr_w	:= dt_inicio_prescr_w + nvl(nr_horas_validade_w,24) / 24;
	dt_validade_prescr_w	:= trunc(dt_validade_prescr_w,'hh24') - 1/86400;

	update	prescr_medica
	set	cd_medico		= cd_medico_p,
		dt_prescricao		= dt_prescricao_p,
		dt_primeiro_horario	= dt_primeiro_horario_w,
		nr_horas_validade	= nr_horas_validade_w,
		dt_validade_prescr	= dt_validade_prescr_w
	where	nr_prescricao		= nr_prescricao_p;
	commit;

	select	to_char(nvl(max(hr_inicio_prescricao),sysdate),'hh24:mi')
	into	hr_inicio_setor_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_w;

	begin
	dt_prescricao_w	:= to_date(to_char(dt_prescricao_p,'dd/mm/yyyy ') || hr_inicio_setor_w || ':00','dd/mm/yyyy hh24:mi:ss');
	exception when others then
	dt_prescricao_w	:= sysdate;
	end;

	update	prescr_gasoterapia
	set	dt_prev_execucao = dt_prescricao_w
	where	nr_prescricao = nr_prescricao_p;

	commit;

	open c06;
	loop
	fetch c06 into
		nr_sequencia_w,
		cd_intervalo_proc_w,
		dt_prev_execucao_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		ds_horarios_w,
		ie_situacao_w,
		nr_seq_exame_w,
		qt_hora_intervalo_w,
		qt_min_intervalo_w;
	exit when c06%notfound;
		begin
		nr_ocorrencia_w		:= 0;
		ds_horarios_ww	:= ds_horarios_w;
		
		select	dt_inicio_prescr,
			dt_validade_prescr
		into	dt_inicio_prescr_w,
			dt_validade_prescr_w
		from	prescr_medica
		where	nr_prescricao	= nr_prescricao_p;
		
		select	max(ie_operacao)
		into	ie_operacao_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_proc_w;
		
		if	(ie_operacao_w <> 'F') then
			Calcular_Horario_Prescricao( nr_prescricao_p , cd_intervalo_proc_w, dt_inicio_prescr_w, dt_inicio_prescr_w,nr_horas_validade_w,
						cd_procedimento_w, qt_hora_intervalo_w, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w, ds_horarios2_w,'N', null);
		end if;
		
		if	(nvl(ie_ctrl_glic_w,'KK') <> 'CIG') then
			dt_prev_execucao_w	:= dt_inicio_prescr_w;
			update	prescr_procedimento
			set	ds_horarios	= substr(ds_horarios_w ||ds_horarios2_w,1,2000),
				cd_intervalo	= cd_intervalo_proc_w,
				dt_prev_execucao= dt_prev_execucao_w,
				nr_ocorrencia	= nr_ocorrencia_w
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_sequencia_w;
		
			commit;
			open C07;
			loop
			fetch C07 into	
				qt_unitaria_w,
				ie_via_aplicacao_w,
				ds_dose_diferenciada_w,
				cd_unidade_medida_dose_w,
				nr_seq_w,
				cd_material_w,
				ie_se_necessario_w,
				ie_acm_w,
				cd_intervalo_mat_w,
				ie_intervalo_dif_w;
			exit when C07%notfound;
				Obter_Quant_Dispensar(	cd_estabelecimento_p, cd_material_w, nr_prescricao_p, null, cd_intervalo_proc_w,
						ie_via_aplicacao_w, qt_unitaria_w, 0,nvl(nr_ocorrencia_w,0), ds_dose_diferenciada_w,
						ie_origem_inf_w, cd_unidade_medida_dose_w, 1, qt_material_w, qt_total_dispensar_w, ie_regra_disp_w, ds_erro_w,ie_se_necessario_w,ie_acm_w);
						
					if (nvl(ie_intervalo_dif_w, 'N') <> 'N') then
						cd_intervalo_up_w := cd_intervalo_mat_w;
					else
						cd_intervalo_up_w := cd_intervalo_proc_w;
					end if;
				
				update	prescr_material
				set	cd_intervalo		= cd_intervalo_up_w,
					ds_horarios		= substr(ds_horarios_w ||ds_horarios2_w,1,2000),
					nr_ocorrencia		= nr_ocorrencia_w,
					qt_total_dispensar	= qt_total_dispensar_w,
					qt_material		= qt_material_w
				where	nr_prescricao		= nr_prescricao_p
				and	nr_sequencia		= nr_seq_w
				and nvl(ie_intervalo_dif, 'N') <> 'S';
				
				commit;
			end loop;
			close C07;
			
		elsif	(ie_ctrl_glic_w = 'CIG') then
			dt_prev_execucao_w	:= dt_inicio_prescr_w;
			update	prescr_procedimento --Controle intensivo de glicemia
			set	ds_horarios	= ds_horarios_ww,
				cd_intervalo	= cd_intervalo_proc_w,
				dt_prev_execucao= dt_prev_execucao_w,
				nr_ocorrencia	= nr_ocorrencia_w
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_sequencia_w;
			commit;
			
			open C07;
			loop
			fetch C07 into	
				qt_unitaria_w,
				ie_via_aplicacao_w,
				ds_dose_diferenciada_w,
				cd_unidade_medida_dose_w,
				nr_seq_w,
				cd_material_w,
				ie_se_necessario_w,
				ie_acm_w,
				cd_intervalo_mat_w,
				ie_intervalo_dif_w;
			exit when C07%notfound;
				Obter_Quant_Dispensar(	cd_estabelecimento_p, null, nr_prescricao_p, null, cd_intervalo_proc_w,
						ie_via_aplicacao_w, qt_unitaria_w, 0,nr_ocorrencia_w, ds_dose_diferenciada_w,
						ie_origem_inf_w, cd_unidade_medida_dose_w, 1, qt_material_w, qt_total_dispensar_w, ie_regra_disp_w, ds_erro_w,ie_se_necessario_w,ie_acm_w);
				
					if (nvl(ie_intervalo_dif_w, 'N') <> 'N') then
						cd_intervalo_up_w := cd_intervalo_mat_w;
					else
						cd_intervalo_up_w := cd_intervalo_proc_w;
					end if;
					
				update	prescr_material
				set	cd_intervalo		= cd_intervalo_up_w,
					ds_horarios		= ds_horarios_ww,
					nr_ocorrencia		= nr_ocorrencia_w,
					qt_total_dispensar	= qt_total_dispensar_w,
					qt_material		= qt_material_w
				where	nr_prescricao		= nr_prescricao_p
				and	nr_sequencia		= nr_seq_w
				and nvl(ie_intervalo_dif, 'N') <> 'S';
				commit;
			end loop;
			close C07;
				
		end if;	
		
		end;
	end loop;
	close c06;
	open C09;
	loop
	fetch C09 into
		nr_sequencia_w,
		qt_hora_intervalo_ww,
		dt_inicio_prescr_w,
		dt_validade_prescr_w,
		dt_proxima_dose_w;
	exit when C09%notfound;
		begin
		if	(dt_proxima_dose_w is not null) and
			(((dt_proxima_dose_w >= dt_inicio_prescr_w) and
			  (dt_proxima_dose_w < dt_validade_prescr_w)) or
			 ((ie_altera_dt_proxima_dose_w = 'S') and
			  (dt_proxima_dose_w < dt_inicio_prescr_w))) then
			begin
			update	prescr_material
			set	hr_prim_horario	= to_char(dt_proxima_dose_w,'hh24:mi'),
				ds_horarios	= to_char(dt_proxima_dose_w,'hh24:mi'),
				ie_administrar  = null,
				dt_proxima_dose = null				
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_sequencia_w;
			commit;
			end;
		elsif	(dt_proxima_dose_w is not null) and
			((dt_proxima_dose_w < dt_inicio_prescr_w) or
			 (dt_proxima_dose_w >= dt_validade_prescr_w)) then
			begin
			update	prescr_material
			set	ie_administrar	= 'N',
				hr_prim_horario	= to_char(dt_proxima_dose_w,'hh24:mi'),
				ds_horarios	= to_char(dt_proxima_dose_w,'hh24:mi')
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_sequencia_w;
			commit;
			end;
		end if;

		end;
	end loop;
	close C09;
		
end if;

end Ajustar_prescricao_copiada;
/