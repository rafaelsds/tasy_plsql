CREATE OR REPLACE PROCEDURE PRC_BSI_REPORT_CALLER_ELEM_ADT(
  start_date_p DATE,
  end_date_p DATE,
  cd_setor_p VARCHAR2) IS
BEGIN
  INSERT INTO BSI_REPORT_LOG(
    NR_SEQUENCIA, START_DATE, END_DATE, AGE, CD_SETOR, ELEMENTS, NM_USUARIO) 
  VALUES(
    bsi_report_log_seq.nextval, start_date_p, end_date_p, 18, NVL(cd_setor_p, '0'), 'S', wheb_usuario_pck.get_nm_usuario);
  --
  COMMIT;
  --
  PKG_BSI_REPORT.RUN(start_date_p, end_date_p, 18, NVL(cd_setor_p, '0'), 'S');
  --
END;
/
