create or replace
procedure gerar_analise_proj_migr_ger (
		nr_seq_gerencia_p	number,
		nm_usuario_p	varchar2) is

nr_seq_grupo_w			number(10,0);
qt_projeto_w			number(10,0) := 0;
qt_projeto_desenv_w		number(10,0) := 0;
qt_projeto_desenv_tec_w		number(10,0) := 0;
qt_projeto_teste_w			number(10,0) := 0;
qt_projeto_concluido_w		number(10,0) := 0;
qt_projeto_saldo_w			number(10,0) := 0;
qt_horas_previstas_w		number(15,2) := 0;
qt_horas_realizadas_w		number(15,2) := 0;
qt_horas_saldo_w			number(15,2) := 0;
pr_previsao_w			number(15,2) := 0;
pr_realizacao_w			number(15,2) := 0;
qt_os_projeto_w			number(10,0) := 0;
qt_os_projeto_tec_w		number(10,0) := 0;
qt_os_projeto_desenv_w		number(10,0) := 0;
qt_os_projeto_teste_w		number(10,0) := 0;
qt_os_projeto_conc_w		number(10,0) := 0;

qt_projeto_ww			number(10,0) := 0;
qt_projeto_desenv_ww		number(10,0) := 0;
qt_projeto_desenv_tec_ww		number(10,0) := 0;
qt_projeto_teste_ww		number(10,0) := 0;
qt_projeto_concluido_ww		number(10,0) := 0;
qt_projeto_saldo_ww		number(10,0) := 0;
qt_horas_previstas_ww		number(15,2) := 0;
qt_horas_realizadas_ww		number(15,2) := 0;
qt_horas_saldo_ww			number(15,2) := 0;
pr_previsao_ww			number(15,2) := 0;
pr_realizacao_ww			number(15,2) := 0;
qt_os_projeto_ww			number(10,0) := 0;
qt_os_projeto_tec_ww		number(10,0) := 0;
qt_os_projeto_desenv_ww		number(10,0) := 0;
qt_os_projeto_teste_ww		number(10,0) := 0;
qt_os_projeto_conc_ww		number(10,0) := 0;

qt_hora_sent_proj_w		number(15,2) := 0;
qt_hora_sent_proj_ww		number(15,2) := 0;
qt_hora_sent_proj_www		number(15,2) := 0;

cursor c01 is
select	g.nr_sequencia
from	proj_grupo g
where	g.ie_situacao = 'A'
and	exists (
		select	1
		from	proj_projeto p
		where	p.nr_seq_gerencia = nr_seq_gerencia_p
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = g.nr_sequencia);
		
cursor c02 is
select	nvl(obter_valor_perc(nvl(proj_obter_qt_total_horas_cron(p.nr_sequencia,0,'T','E'),0),
		decode(p.nr_seq_estagio, 1, proj_obter_perc_cron(p.nr_sequencia,'R'), 12, proj_obter_perc_cron(p.nr_sequencia,'R'), 100)),0) qt_hora_sent_proj
from	proj_projeto p
where	p.nr_seq_gerencia = nr_seq_gerencia_p		
and	p.nr_seq_classif = 14
and	p.nr_sequencia not in (1430,1427,1061)
and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
and	p.nr_sequencia not in (1282)
and	p.nr_seq_grupo = nr_seq_grupo_w;

begin
if	(nr_seq_gerencia_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	delete
	from	w_analise_proj_desenv
	where	nr_seq_gerencia = nr_seq_gerencia_p
	and	dt_analise = trunc(sysdate);
	
	open c01;
	loop
	fetch c01 into nr_seq_grupo_w;
	exit when c01%notfound;
		begin
		select	count(*),
			sum(nvl(proj_obter_qt_total_horas_cron(p.nr_sequencia,0,'T','E'),0)),
			sum(nvl(proj_obter_qt_total_horas_cron(p.nr_sequencia,0,'R','E'),0))
		into	qt_projeto_w,
			qt_horas_previstas_w,
			qt_horas_realizadas_w
		from	proj_projeto p
		where	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w;
		
		qt_horas_saldo_w	:= qt_horas_previstas_w - qt_horas_realizadas_w;
		pr_previsao_w		:= obter_perc_valor(qt_horas_realizadas_w,qt_horas_previstas_w);
		
		select	count(*)
		into	qt_projeto_desenv_w
		from	proj_projeto p
		where	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w
		and	p.nr_seq_estagio = 12;	
		
		select	count(*)
		into	qt_projeto_desenv_tec_w
		from	proj_projeto p
		where	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w
		and	p.nr_seq_estagio = 12
		and	exists (
				select	1
				from	man_ordem_servico mos
				where	mos.nr_sequencia = p.nr_seq_ordem_serv
				and	mos.nr_seq_estagio = 961);		

		select	count(*)
		into	qt_projeto_teste_w
		from	proj_projeto p
		where	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w
		and	p.nr_seq_estagio in (13,17);
		
		select	count(*)
		into	qt_projeto_concluido_w
		from	proj_projeto p
		where	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w
		and	p.ie_status = 'F';
		
		qt_projeto_saldo_w := qt_projeto_w - qt_projeto_desenv_w - qt_projeto_teste_w - qt_projeto_concluido_w;
		
		select	count(*)
		into	qt_os_projeto_w
		from	proj_ordem_servico pos,
			proj_projeto p
		where	pos.nr_seq_proj = p.nr_sequencia
		and	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w;
		
		select	count(*)
		into	qt_os_projeto_tec_w
		from	man_ordem_servico mos,
			proj_ordem_servico pos,
			proj_projeto p
		where	mos.nr_seq_estagio in (961,131)
		and	mos.nr_sequencia = pos.nr_seq_ordem
		and	pos.nr_seq_proj = p.nr_sequencia
		and	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w;		
		
		select	count(*)
		into	qt_os_projeto_desenv_w
		from	man_ordem_servico mos,
			proj_ordem_servico pos,
			proj_projeto p
		where	mos.nr_seq_estagio in (4,731,732)
		and	mos.nr_sequencia = pos.nr_seq_ordem
		and	pos.nr_seq_proj = p.nr_sequencia
		and	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w;
		
		select	count(*)
		into	qt_os_projeto_teste_w
		from	man_ordem_servico mos,
			proj_ordem_servico pos,
			proj_projeto p
		where	mos.nr_seq_estagio in (2,1511)
		and	mos.nr_sequencia = pos.nr_seq_ordem
		and	pos.nr_seq_proj = p.nr_sequencia
		and	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w;
		
		select	count(*)
		into	qt_os_projeto_conc_w
		from	man_ordem_servico mos,
			proj_ordem_servico pos,
			proj_projeto p
		where	mos.nr_seq_estagio = 9
		and	mos.nr_sequencia = pos.nr_seq_ordem
		and	pos.nr_seq_proj = p.nr_sequencia
		and	p.nr_seq_gerencia = nr_seq_gerencia_p		
		and	p.nr_seq_classif = 14
		and	p.nr_sequencia not in (1430,1427,1061)
		and	p.nr_sequencia not in (580,581,582,583,584,656,494,654,498)
		and	p.nr_sequencia not in (1282)
		and	p.nr_seq_grupo = nr_seq_grupo_w;
		
		qt_hora_sent_proj_ww := 0;
		
		open c02;
		loop
		fetch c02 into qt_hora_sent_proj_w;
		exit when c02%notfound;
			begin
			qt_hora_sent_proj_ww := qt_hora_sent_proj_ww + qt_hora_sent_proj_w;
			end;
		end loop;
		close c02;
		
		qt_hora_sent_proj_www	:= qt_hora_sent_proj_www + qt_hora_sent_proj_ww;		
		pr_realizacao_w		:= obter_perc_valor(qt_hora_sent_proj_ww,qt_horas_previstas_w);
		
		insert into w_analise_proj_desenv (
			nm_usuario,
			dt_analise,
			nr_seq_gerencia,
			nr_seq_grupo,
			qt_projeto,
			qt_projeto_desenv,
			qt_projeto_desenv_tec,
			qt_projeto_teste,
			qt_projeto_concluido,
			qt_projeto_saldo,
			qt_horas_previstas,
			qt_horas_realizadas,
			qt_horas_saldo,
			pr_previsao,
			pr_realizacao,			
			qt_os_projeto,
			qt_os_projeto_tec,
			qt_os_projeto_desenv,			
			qt_os_projeto_teste,
			qt_os_projeto_conc,
			dt_fim_previsto)
		values (
			nm_usuario_p,
			trunc(sysdate),
			nr_seq_gerencia_p,
			nr_seq_grupo_w,
			qt_projeto_w,
			qt_projeto_desenv_w,
			qt_projeto_desenv_tec_w,
			qt_projeto_teste_w,
			qt_projeto_concluido_w,
			qt_projeto_saldo_w,
			qt_horas_previstas_w,
			qt_horas_realizadas_w,
			qt_horas_saldo_w,
			pr_previsao_w,
			pr_realizacao_w,
			qt_os_projeto_w,
			qt_os_projeto_tec_w,
			qt_os_projeto_desenv_w,
			qt_os_projeto_teste_w,
			qt_os_projeto_conc_w,
			null);
			
		qt_projeto_ww			:= qt_projeto_ww + qt_projeto_w;
		qt_projeto_desenv_ww		:= qt_projeto_desenv_ww + qt_projeto_desenv_w;
		qt_projeto_desenv_tec_ww	:= qt_projeto_desenv_tec_ww + qt_projeto_desenv_tec_w;
		qt_projeto_teste_ww		:= qt_projeto_teste_ww + qt_projeto_teste_w;
		qt_projeto_concluido_ww		:= qt_projeto_concluido_ww + qt_projeto_concluido_w;
		qt_projeto_saldo_ww		:= qt_projeto_saldo_ww + qt_projeto_saldo_w;
		qt_horas_previstas_ww		:= qt_horas_previstas_ww + qt_horas_previstas_w;
		qt_horas_realizadas_ww		:= qt_horas_realizadas_ww + qt_horas_realizadas_w;
		qt_horas_saldo_ww		:= qt_horas_saldo_ww + qt_horas_saldo_w;
		qt_os_projeto_ww		:= qt_os_projeto_ww + qt_os_projeto_w;
		qt_os_projeto_tec_ww		:= qt_os_projeto_tec_ww + qt_os_projeto_tec_w;
		qt_os_projeto_desenv_ww		:= qt_os_projeto_desenv_ww + qt_os_projeto_desenv_w;
		qt_os_projeto_teste_ww		:= qt_os_projeto_teste_ww + qt_os_projeto_teste_w;
		qt_os_projeto_conc_ww		:= qt_os_projeto_conc_ww + qt_os_projeto_conc_w;
		end;
	end loop;
	close c01;
	
	pr_previsao_ww 		:= obter_perc_valor(qt_horas_realizadas_ww,qt_horas_previstas_ww);
	pr_realizacao_ww	:= obter_perc_valor(qt_hora_sent_proj_www,qt_horas_previstas_ww);
	
	insert into w_analise_proj_desenv (
		nm_usuario,
		dt_analise,
		nr_seq_gerencia,
		nr_seq_grupo,
		qt_projeto,
		qt_projeto_desenv,
		qt_projeto_desenv_tec,
		qt_projeto_teste,
		qt_projeto_concluido,
		qt_projeto_saldo,
		qt_horas_previstas,
		qt_horas_realizadas,
		qt_horas_saldo,
		pr_previsao,
		pr_realizacao,
		qt_os_projeto,
		qt_os_projeto_tec,
		qt_os_projeto_desenv,
		qt_os_projeto_teste,
		qt_os_projeto_conc,
		dt_fim_previsto)
	values (
		nm_usuario_p,
		trunc(sysdate),
		nr_seq_gerencia_p,
		null,
		qt_projeto_ww,
		qt_projeto_desenv_ww,
		qt_projeto_desenv_tec_ww,
		qt_projeto_teste_ww,
		qt_projeto_concluido_ww,
		qt_projeto_saldo_ww,
		qt_horas_previstas_ww,
		qt_horas_realizadas_ww,
		qt_horas_saldo_ww,
		pr_previsao_ww,
		pr_realizacao_ww,
		qt_os_projeto_ww,
		qt_os_projeto_tec_ww,
		qt_os_projeto_desenv_ww,		
		qt_os_projeto_teste_ww,
		qt_os_projeto_conc_ww,
		null);
	end;
end if;
commit;
end gerar_analise_proj_migr_ger;
/
