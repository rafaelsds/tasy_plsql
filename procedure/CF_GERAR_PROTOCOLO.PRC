create or replace procedure cf_gerar_protocolo(	cd_convenio_p			number,
				nr_protocolo_p			varchar2,
				ie_tipo_protocolo_p		number,
				cd_estabelecimento_p		number,
				dt_entrega_convenio_p		date,
				nr_seq_protocolo_p		out number,
				nm_usuario_p			varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
dt_mesano_referencia_w				date;
nr_seq_protocolo_w				number(10);


begin

select		obter_data_ref_protocolo(cd_convenio_p, ie_tipo_protocolo_p),
		protocolo_convenio_seq.nextval
into		dt_mesano_referencia_w,
		nr_seq_protocolo_w
from		dual;

insert into protocolo_convenio 	(	cd_convenio,
					nr_protocolo,
					ie_status_protocolo,
					dt_periodo_inicial,
					dt_periodo_final,
					dt_atualizacao,
					nm_usuario,
					ie_tipo_protocolo,
					nr_seq_protocolo,
					dt_mesano_referencia,
					cd_estabelecimento,
					nr_seq_lote_receita,
					nr_seq_lote_repasse,
					nr_seq_lote_grat,
					dt_entrega_convenio)
values				(	cd_convenio_p,
					nr_protocolo_p,
					1,
					trunc(sysdate, 'dd'),
					fim_dia(sysdate),
					sysdate,
					nm_usuario_p,
					ie_tipo_protocolo_p,
					nr_seq_protocolo_w,
					dt_mesano_referencia_w,
					cd_estabelecimento_p,
					0,
					0,
					0,
					dt_entrega_convenio_p);
									
if	(nvl(obter_valor_param_usuario(3013, 6, obter_perfil_ativo, nm_usuario_p, 0), 'N') = 'S') then	
	
	cf_inserir_protocolo_lote(nr_seq_protocolo_w,nm_usuario_p);

end if;								
									
nr_seq_protocolo_p := nr_seq_protocolo_w;

commit;
end cf_gerar_protocolo;
/