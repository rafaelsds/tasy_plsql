create or replace
procedure obter_dados_pf_agenda_consulta (
		cd_funcao_p			number,
		cd_agenda_p			number,
		cd_pessoa_fisica_p			varchar2,
		dt_agenda_p			date,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		ie_forma_apres_p		varchar2,
		cd_categoria_p		out	varchar2,
		cd_convenio_p		out	number,
		cd_plano_p		out	varchar2,
		cd_tipo_acomodacao_p	out	number,
		cd_usuario_convenio_p	out	varchar2,
		cd_usuario_plano_p		out	varchar2,
		ds_agenda_pac_espera_p	out	varchar2,
		dt_validade_p		out	date,
		dt_validade_usuario_p	out	date,
		ie_pac_duplo_p		out	varchar2,
		ie_pac_duplo_agenda_p	out	varchar2,
		nm_pessoa_fisica_p		out	varchar2,
		nr_doc_convenio_p		out	varchar2,
		nr_telefone_p		out	varchar2,
		nr_telefone_pac_p		out	varchar2,
		ds_erro_pac_agenda_dia_p	out	varchar2,
		ds_erro_perm_alterar_p	out	varchar2,
		ds_msg_perm_agendar_p	out	varchar2,
		ds_msg_perm_alterar_p	out	varchar2,
		ds_perg_existe_lista_pac_p	out	varchar2,
		ds_perg_pac_outra_lista_p	out	varchar2,
		qt_faltas_p	out	number) is 

ie_buscar_cod_usuario_w	varchar2(1);
ie_verif_pac_lista_espera_w	varchar2(1);
ie_questiona_exclusao_lista_w	varchar2(1);
ie_outra_lista_w		varchar2(1);
ds_perm_agendar_classif_w	varchar2(80);
ds_agenda_pac_w			varchar2(255);

begin

if	(cd_pessoa_fisica_p <> 0) then
	begin		
	select	substr(obter_se_perm_pf_classif(cd_funcao_p, cd_agenda_p, cd_pessoa_fisica_p, dt_agenda_p, 'DS'), 1,80),
		substr(obter_compl_pf(cd_pessoa_fisica_p, 1,'T'), 1,15),
		substr(obter_nome_pf(cd_pessoa_fisica_p), 1,60),
		substr(obter_fone_pac_agenda(cd_pessoa_fisica_p), 1,80),
		nvl(obter_qt_faltas_pac_agecons(cd_pessoa_fisica_p, cd_estabelecimento_p, cd_agenda_p, ie_forma_apres_p),0) qt_faltas
	into	ds_perm_agendar_classif_w,
		nr_telefone_p,
		nm_pessoa_fisica_p,
		nr_telefone_pac_p,
		qt_faltas_p
	from	dual;

	if	(nr_telefone_p is not null) then
		nr_telefone_p	:= 'Res: ' || nr_telefone_p;
	end if;

	if	(ds_perm_agendar_classif_w is not null) then
		begin
		ds_msg_perm_agendar_p	:= substr(obter_texto_tasy(47595, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		ds_msg_perm_agendar_p	:= replace(ds_msg_perm_agendar_p, '#@IE_PERM_AGENDAR_CLASSIF#@', ds_perm_agendar_classif_w);
		end;
	end if;

	select	decode(count(nr_sequencia), 0, 'N', 'S')
	into	ie_pac_duplo_p
	from	agenda_consulta
	where	cd_agenda			= cd_agenda_p
	and	trunc(dt_agenda, 'dd')		= trunc(dt_agenda_p, 'dd')
	and	ie_status_agenda		<> 'C'
	and	cd_pessoa_fisica		= cd_pessoa_fisica_p;
	
	select	decode(count(a.nr_sequencia), 0, 'N', 'S')
	into	ie_pac_duplo_agenda_p
	from	agenda_consulta a
	where	cd_agenda			= cd_agenda_p
	and	trunc(dt_agenda, 'dd')		= trunc(dt_agenda_p, 'dd')
	and	ie_status_agenda		<> 'C'
	and	cd_pessoa_fisica		= cd_pessoa_fisica_p;

	define_convenio_atend_agenda(
		cd_pessoa_fisica_p,
		nm_usuario_p,
		cd_convenio_p,
		cd_categoria_p,
		dt_validade_p,
		cd_usuario_convenio_p,
		nr_doc_convenio_p,
		cd_tipo_acomodacao_p,
		cd_plano_p);

	-- Agenda de Servi�os - Parametro [101] - Ao informar o paciente, verifica se o mesmo esta em lista de espera (Independente da agenda)
	obter_param_usuario(866, 101, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_verif_pac_lista_espera_w);

	if	(ie_verif_pac_lista_espera_w = 'S') then
		begin
		ds_agenda_pac_w	:= substr(nvl(obter_agenda_cons_lista_espera(cd_pessoa_fisica_p, 1), ''), 1,255);
		
		if	(ds_agenda_pac_w is null) then
			ds_agenda_pac_espera_p := '';
		else
			select	replace(substr(obter_texto_tasy(47721, wheb_usuario_pck.get_nr_seq_idioma),1,255), '#@DS_AGENDA_PAC_ESPERA#@', ds_agenda_pac_w)
			into	ds_agenda_pac_espera_p
			from	dual;
		end if;
		end;
	end if;

	-- Agenda de Servi�os - Parametro [106] - Questionar se deseja excluir o paciente da lista de espera, ao agendar um paciente existente na lista de espera
	obter_param_usuario(866, 106, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_questiona_exclusao_lista_w);

	if	(ie_questiona_exclusao_lista_w = 'S') then
		begin
		select	substr(obter_se_pac_outra_lista(cd_pessoa_fisica_p, cd_agenda_p), 1,1)
		into	ie_outra_lista_w
		from	dual;

		if	(ie_outra_lista_w = 'S') then
			ds_perg_pac_outra_lista_p	:= substr(obter_texto_tasy(47618, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		end if;
		end;
	end if;

	ds_msg_perm_alterar_p	:= substr(obter_texto_tasy(47596, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	ds_erro_perm_alterar_p	:= substr(obter_texto_tasy(47598, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	ds_erro_pac_agenda_dia_p	:= substr(obter_texto_tasy(47616, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	ds_perg_existe_lista_pac_p	:= substr(obter_texto_tasy(47617, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end;
end if;

-- Agenda de Servi�os - Parametro [67] - Ao agendar pacientes da OPS, buscar automaticamente o c�digo do usu�rio e validade da carteira
obter_param_usuario(866, 67, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_buscar_cod_usuario_w);

if	(ie_buscar_cod_usuario_w = 'S') then
	begin
	pls_obter_usuario_plano(
		cd_pessoa_fisica_p,
		dt_agenda_p,
		cd_estabelecimento_p,
		cd_usuario_plano_p,
		dt_validade_usuario_p);
	end;
end if;

end obter_dados_pf_agenda_consulta;
/