create or replace
procedure rep_suspender_dieta_vigente(	nr_atendimento_p	number,
					nr_seq_jejum_p		number,
					nm_usuario_p		varchar2) is

nr_seq_horario_w		number(10);
nr_seq_item_w			number(10);
nr_prescricao_w			number(14);
dt_inicio_w				date;
dt_fim_w				date;
cd_estabelecimento_w  	prescr_medica.cd_estabelecimento%type;
cd_refeicao_w			prescr_dieta.cd_dieta%type;
cd_material_w			prescr_mat_hor.cd_material%type;
nr_horas_validade_w		prescr_medica.nr_horas_validade%type;
dt_horario_w			prescr_mat_hor.dt_horario%type;
dt_horario_ww			prescr_dieta_hor.dt_horario%type;
ie_tipo_item_w			varchar2(5);
cd_evolucao_w			number(10);
cd_intervalo_w			varchar2(7);
dt_atualizacao_w		date := sysdate;
nr_seq_alteracao_w		number(10,0);
ie_mostra_adep_w		varchar2(1);
nr_seq_evento_w			number(10,0);

cursor c01 is
select	a.nr_sequencia,
	a.nr_seq_material,
	a.nr_prescricao,
	a.cd_material,
	a.dt_horario,
	decode(a.ie_agrupador, 12, 'S', 
							8, 'SNE',
						   16 , 'LD')
from	prescr_mat_hor a,
	prescr_medica b
where	a.nr_prescricao	 = b.nr_prescricao
and	b.nr_atendimento = nr_atendimento_p
and	a.dt_horario between dt_inicio_w and (dt_fim_w - 1/1440)
and	a.dt_suspensao is null
and	a.dt_fim_horario is null
and	a.ie_agrupador in (8,12, 16)
and	((b.nm_usuario_original = nm_usuario_p) or
	 (nvl(b.dt_liberacao_medico,b.dt_liberacao) is not null));

cursor c02 is
select	a.nr_sequencia,
	a.nr_seq_dieta,
	a.nr_prescricao,
	b.cd_estabelecimento,
	b.nr_horas_validade,
	a.dt_horario,
	c.cd_dieta
from	prescr_dieta_hor a,
	prescr_medica b,
	prescr_dieta c
where	a.nr_prescricao = b.nr_prescricao
and a.nr_prescricao = c.nr_prescricao
and	a.nr_seq_dieta = c.nr_sequencia
and	b.nr_atendimento = nr_atendimento_p
and	a.dt_horario between dt_inicio_w and (dt_fim_w - 1/1440)
and	a.dt_suspensao is null
and	a.dt_fim_horario is null
and	((b.nm_usuario_original = nm_usuario_p) or
	 (nvl(b.dt_liberacao_medico,b.dt_liberacao) is not null));

begin

select	max(dt_inicio),
	max(dt_fim)
into	dt_inicio_w,
	dt_fim_w
from	rep_jejum
where	nr_sequencia	= nr_seq_jejum_p;

if	(dt_inicio_w	is not null) and
	(dt_fim_w	is not null) then

	open c01;
	loop
	fetch c01 into
		nr_seq_horario_w,
		nr_seq_item_w,
		nr_prescricao_w,
		cd_material_w,
		dt_horario_w,
		ie_tipo_item_w;
	exit when c01%notfound;
		update	prescr_mat_hor
		set	dt_suspensao		= sysdate,
			nm_usuario_susp		= nm_usuario_p,
			ie_suspenso_adep	= 'S',
			nr_seq_jejum_susp	= nr_seq_jejum_p
		where	nr_sequencia		= nr_seq_horario_w;

		update	prescr_mat_hor
			set	dt_suspensao		= sysdate,
				nm_usuario_susp		= nm_usuario_p,
				ie_suspenso_adep	= 'S',
				nr_seq_jejum_susp	= nr_seq_jejum_p
		where dt_horario between dt_inicio_w and (dt_fim_w - 1/1440)
		and	dt_suspensao is null
		and	dt_fim_horario is null
		and nr_prescricao = nr_prescricao_w
		and nr_seq_material in (select nr_sequencia from prescr_material a where nr_prescricao = nr_prescricao_w and nr_seq_kit = nr_seq_item_w);

		atualiza_ie_horario_susp(nr_prescricao_w, 2, 'PRESCR_MATERIAL');
		--  gerar evento adep
			if	(ie_tipo_item_w = 'S') or
				(ie_tipo_item_w = 'LD') then
				gerar_alter_hor_prescr_adep(nr_atendimento_p, ie_tipo_item_w, cd_material_w , nr_prescricao_w, nr_seq_item_w, nr_seq_horario_w,dt_horario_w, 12, '', '', null, nm_usuario_p);				
		   elsif(ie_tipo_item_w	= 'SNE') then
			
				select	prescr_solucao_evento_seq.nextval
						into	nr_seq_evento_w
						from	dual;
				
				insert into prescr_solucao_evento	(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_prescricao,
							nr_seq_solucao,
							nr_seq_material,
							nr_seq_procedimento,
							nr_seq_nut,
							nr_seq_nut_neo,
							ie_tipo_dosagem,
							qt_dosagem,
							qt_vol_infundido,
							qt_vol_desprezado,
							cd_pessoa_fisica,
							ie_alteracao,
							dt_alteracao,
							ie_evento_valido,
							nr_seq_motivo,
							ds_observacao,
							ds_justificativa,
							ie_tipo_solucao,
							dt_horario)
					values	(
							nr_seq_evento_w,
							dt_atualizacao_w,
							nm_usuario_p,
							dt_atualizacao_w,
							nm_usuario_p,
							nr_prescricao_w,
							null,
							nr_seq_item_w,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							obter_dados_usuario_opcao(nm_usuario_p, 'C'),
							12,
							dt_atualizacao_w,
							'S',
							null,
							wheb_mensagem_pck.get_texto(819488),
							'',
							2,
							dt_horario_w);
							
			end if;					
	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into
		nr_seq_horario_w,
		nr_seq_item_w,
		nr_prescricao_w,
		cd_estabelecimento_w,
		nr_horas_validade_w,
		dt_horario_ww,
		cd_refeicao_w;
	exit when c02%notfound;
	
		update	prescr_dieta_hor
		set	dt_suspensao 		= sysdate,
			nm_usuario_susp		= nm_usuario_p,
			nr_seq_jejum_susp	= nr_seq_jejum_p
		where	nr_sequencia 		= nr_seq_horario_w;

		atualiza_ie_horario_susp(nr_prescricao_w, nr_seq_item_w, 'PRESCR_DIETA');
		ie_mostra_adep_w	:= nvl(wheb_assist_pck.obterparametrofuncao(1113,459),'S');
		select	prescr_mat_alteracao_seq.nextval
		into	nr_seq_alteracao_w
		from	dual;		

		insert	into	prescr_mat_alteracao	(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_prescricao,
							nr_seq_horario_dieta,
							dt_alteracao,
							cd_pessoa_fisica,
							ie_alteracao,
							nr_seq_qualidade,
							ds_justificativa,
							ds_observacao,
							ie_tipo_item,
							dt_horario,
							nr_atendimento,
							cd_item,
							qt_dose_adm,
							qt_dose_original,
							cd_medico_solic,
							cd_um_dose_adm,
							ie_mostra_adep,
							nr_seq_assinatura,
							nr_seq_lote,
							cd_funcao
							)
						values	(
							nr_seq_alteracao_w,
							dt_atualizacao_w,
							nm_usuario_p,
							dt_atualizacao_w,
							nm_usuario_p,
							nr_prescricao_w,
							nr_seq_horario_w,
							dt_atualizacao_w,
							obter_dados_usuario_opcao(nm_usuario_p,'C'),
							12,
							null,
							wheb_mensagem_pck.get_texto(819488),
							'',
							'D',
							dt_horario_w,
							nr_atendimento_p,
							cd_refeicao_w,
							null,
							null,
							null,
							null,
							ie_mostra_adep_w,
							null,
							null,
							null);
			

	end loop;
	close c02;

	commit;

end if;

end rep_suspender_dieta_vigente;
/