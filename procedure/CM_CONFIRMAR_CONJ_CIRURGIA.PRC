create or replace
procedure cm_confirmar_conj_cirurgia(nr_cirurgia_p 	 number,
				 nr_seq_pepo_p	 number,
				 cd_barras_p    	 number,
				 nm_usuario_p  	 varchar2,
				 ie_opcao_p	 varchar2) is
qt_conjunto_w number(3);
dt_liberacao_w date;

begin
if (nvl(cd_barras_p,0) > 0) then
	if	(nvl(nr_cirurgia_p,0) > 0) then
		select	count(nr_sequencia),
			max(dt_liberacao)
		into	qt_conjunto_w,
			dt_liberacao_w
		from	cm_conjunto_cont
		where	nr_cirurgia = nr_cirurgia_p
		and 	nr_sequencia = cd_barras_p;


	end if;
   
	if	(nvl(qt_conjunto_w,0) = 0) then
		select	count(nr_sequencia),
			max(dt_liberacao)
		into	qt_conjunto_w,
			dt_liberacao_w
		from	cm_conjunto_cont
		where	nr_seq_pepo = nr_seq_pepo_p
		and 	nr_sequencia = cd_barras_p;


	end if;

	if (nvl(qt_conjunto_w,0) > 0 ) then
		if (dt_liberacao_w is null) then		
			if (ie_opcao_p = 'D') then
				update 	cm_conjunto_cont
				set      	dt_baixa_cirurgia = null,
					nm_usuario_baixa_cirurgia = null
				where   	nr_sequencia = cd_barras_p
				and	dt_liberacao is null;
			else
				update 	 cm_conjunto_cont
				set   	 dt_baixa_cirurgia = sysdate,
					nm_usuario_baixa_cirurgia = nm_usuario_p
				where   	nr_sequencia = cd_barras_p
				and	dt_liberacao is null;	
			end if;			
		else 
			--'Conjunto vinculado a cirurgia est� liberado!
			wheb_mensagem_pck.exibir_mensagem_abort(405961);
		end if;
	else
		--'Conjunto n�o encontrado ou n�o est� vinculado a cirurgia!
		wheb_mensagem_pck.exibir_mensagem_abort(200976);

	end if;
end if;

commit;

end cm_confirmar_conj_cirurgia;

/
