create or replace
procedure pls_alterar_cartao_ident_benef
			(	nr_seq_segurado_p	number,
				dt_alteracao_p		date,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_regra_w			number(10);
nr_seq_apresentacao_w		number(2);
ie_tipo_campo_w			varchar2(5);
ds_mascara_w			varchar2(30);
nr_seq_atual_w			number(10);
cd_segurado_familia_w		varchar2(20);
cd_segurado_contrato_w		varchar2(20);
cd_contrato_w			varchar2(20);
cd_plano_w			varchar2(20);
nr_seq_plano_w			number(10);
nr_seq_contrato_w		number(10);
dt_contratacao_w		date;
nr_seq_regra_carteira_w		number(10);
nr_digito_w			varchar2(2);
cd_carteira_usuario_w		varchar2(30)	:= '';
ds_erro_w			varchar2(2000)	:= '';
qt_registros_w			number(10);
nr_seq_titular_w		number(10);
nr_seq_emissor_w		number(10);
nr_seq_cart_numero_w		number(10);
ie_tipo_segurado_w		Varchar2(3);
nr_seq_seg_contrato_w		Number(10);
cd_cod_segurado_anterior_w	Varchar2(20);
cd_cod_contrato_anterior_w	Varchar2(20);
nr_via_solicitacao_w		number(5);
ds_trilha1_w			pls_segurado_carteira.ds_trilha1%type;
ds_trilha2_w			pls_segurado_carteira.ds_trilha2%type;
ds_trilha3_w			pls_segurado_carteira.ds_trilha3%type;
ds_trilha_qr_code_w		pls_segurado_carteira.ds_trilha_qr_code%type;
nr_seq_segurado_carteira_w	Number(10);
nr_seq_segurado_w		Number(10);
nr_identificacao_titular_w	Number(2)	:= 00;
nr_seq_identific_w		Number(10);
nr_contrato_w			Number(10);
cd_cooperativa_w		varchar2(10);
cd_matricula_familia_w		Number(10);
cd_operadora_empresa_w		Number(10);
cd_estabelecimento_w		number(4);
nr_seq_carteira_w		number(10);
cd_matricula_estipulante_w	varchar2(30);
nr_seq_congenere_w		number(10);
nr_seq_intercambio_w		number(10);
cd_cd_codigo_carteira_w		varchar2(30);
ie_tipo_estipulante_w		varchar2(2) := 'A';
cd_fixo_w			pls_regra_carteira_campo.cd_fixo%type;
nr_seq_proposta_w		number(10);
nr_seq_pessoa_proposta_w	number(10);
nr_seq_plano_ww			number(10);
nr_seq_segurado_obito_w		number(10);
ie_titularidade_w		varchar2(2);
cd_pessoa_fisica_w		number(10);
qt_carteira_pessoa_w		number(10);
nr_seq_grupo_intercambio_w	number(10);
cd_grupo_w			varchar2(30);
cd_usuario_ant_w		varchar2(30);
nr_seq_tabela_w			number(10);
cd_codigo_ant_w			varchar2(20);

cursor c01 is
	select	nr_sequencia,
		nr_seq_apresentacao,
		ie_tipo_campo,
		ds_mascara
	from	pls_regra_carteira_campo
	where	nr_seq_regra_carteira	= nr_seq_regra_carteira_w
	order by nr_seq_apresentacao;

begin

/*Alterar a titularidade do beneficiario*/
pls_gerar_regra_titularidade(nr_seq_segurado_p,cd_estabelecimento_p,nm_usuario_p);

select	nr_seq_plano,
	nr_seq_contrato,
	nvl(dt_contratacao,dt_inclusao_operadora),
	nr_seq_titular,
	nvl(ie_tipo_segurado,'B'),
	nr_seq_seg_contrato,
	cd_cod_anterior,
	nr_seq_congenere,
	nr_seq_intercambio,
	nr_seq_pessoa_proposta,
	nr_seq_segurado_obito,
	ie_titularidade,
	cd_pessoa_fisica,
	nr_seq_tabela
into	nr_seq_plano_w,
	nr_seq_contrato_w,
	dt_contratacao_w,
	nr_seq_titular_w,
	ie_tipo_segurado_w,
	nr_seq_seg_contrato_w,
	cd_cod_segurado_anterior_w,
	nr_seq_congenere_w,
	nr_seq_intercambio_w,
	nr_seq_pessoa_proposta_w,
	nr_seq_segurado_obito_w,
	ie_titularidade_w,
	cd_pessoa_fisica_w,
	nr_seq_tabela_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(ie_tipo_segurado_w in ('A','B')) then
	select	decode(cd_pf_estipulante, null, 'PJ', 'PF'),
		nr_contrato
	into	ie_tipo_estipulante_w,
		nr_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
elsif	(ie_tipo_segurado_w in('T','C')) then
	select	decode(cd_pessoa_fisica, null, 'PJ', 'PF'),
		null
	into	ie_tipo_estipulante_w,
		nr_contrato_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_w;
elsif	(ie_tipo_segurado_w = 'P') then
	if	(nr_seq_pessoa_proposta_w is not null) then
		select	nr_seq_proposta
		into	nr_seq_proposta_w
		from	pls_proposta_beneficiario
		where	nr_sequencia	= nr_seq_pessoa_proposta_w;
		
		select	decode(cd_estipulante, null, 'PJ', 'PF'),
			null
		into	ie_tipo_estipulante_w,
			nr_contrato_w
		from	pls_proposta_adesao
		where	nr_sequencia	= nr_seq_proposta_w;
	elsif	(nr_seq_pessoa_proposta_w is not null) then
		ie_tipo_estipulante_w	:= 'A';
		nr_contrato_w		:= null;
	end if;
elsif	(ie_tipo_segurado_w = 'R') then
	if	(nr_seq_contrato_w is not null) then
		select	decode(cd_pf_estipulante, null, 'PJ', 'PF'),
			nr_contrato
		into	ie_tipo_estipulante_w,
			nr_contrato_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
	elsif	(nr_seq_intercambio_w is not null) then
		select	decode(cd_pessoa_fisica, null, 'PJ', 'PF'),
			null
		into	ie_tipo_estipulante_w,
			nr_contrato_w
		from	pls_intercambio
		where	nr_sequencia	= nr_seq_intercambio_w;
	end if;
end if;

if	(ie_tipo_segurado_w in ('B','A')) then
	select	nvl(nr_seq_emissor,0),
		cd_estabelecimento
	into	nr_seq_emissor_w,
		cd_estabelecimento_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
	
	select	nvl(cd_cod_anterior, nr_contrato)
	into	cd_cod_contrato_anterior_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
elsif	(ie_tipo_segurado_w	= 'P') then
	begin
	select	nvl(max(obter_valor_param_usuario(1235, 1, Obter_Perfil_Ativo, nm_usuario_p, 0)), 0)
	into	nr_seq_emissor_w
	from	dual;
	exception
	when others then
		nr_seq_emissor_w	:= 0;
	end;
	
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
elsif	(ie_tipo_segurado_w	= 'I') then
	select	nr_seq_emissor,
		cd_estabelecimento
	into	nr_seq_emissor_w,
		cd_estabelecimento_w
	from	pls_congenere
	where	nr_sequencia	= nr_seq_congenere_w;
elsif	(ie_tipo_segurado_w	in('T','C') ) then
	select	nr_seq_emissor,
		cd_estabelecimento
	into	nr_seq_emissor_w,
		cd_estabelecimento_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_w;
elsif	(ie_tipo_segurado_w = 'R') then
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
	
	if	(nr_seq_segurado_obito_w is null) then
		begin
		select	max(nr_seq_emissor),
			max(nr_seq_plano)
		into	nr_seq_emissor_w,
			nr_seq_plano_ww
		from	pls_segurado_repasse
		where	nr_seq_segurado	= nr_seq_segurado_p
		and	ie_cartao_provisorio	= 'S';
		exception
		when others then
			select	max(nr_seq_emissor_provisorio)
			into	nr_seq_emissor_w
			from	pls_parametros
			where	cd_estabelecimento	= cd_estabelecimento_w;
			nr_seq_plano_ww		:= null;
		end;
		
		if	(nr_seq_plano_ww is not null) then
			nr_seq_plano_w	:= nr_seq_plano_ww;
		end if;
		nr_seq_emissor_w	:= nvl(nr_seq_emissor_w,0);
	elsif	(nr_seq_segurado_obito_w is not null) then
		if	(nr_seq_contrato_w is not null) then
			select	nvl(nr_seq_emissor,0),
				cd_estabelecimento
			into	nr_seq_emissor_w,
				cd_estabelecimento_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
			
			select	nvl(cd_cod_anterior, nr_contrato)
			into	cd_cod_contrato_anterior_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
		elsif	(nr_seq_intercambio_w is not null) then
			select	nr_seq_emissor,
				cd_estabelecimento
			into	nr_seq_emissor_w,
				cd_estabelecimento_w
			from	pls_intercambio
			where	nr_sequencia	= nr_seq_intercambio_w;
		end if;
	end if;
end if;

if	(nr_seq_emissor_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort( 213841, null);
	/* Falta informar o emissor da carteira do beneficiario para o contrato. Verifique! */
end if;

select	nvl(max(nr_sequencia),0)
into	nr_seq_cart_numero_w
from	pls_emissor_cart_numero
where	nr_seq_emissor	= nr_seq_emissor_w;

select	nvl(max(nr_sequencia),0)
into	nr_seq_regra_carteira_w
from	pls_regra_carteira
where	nr_seq_emissor	= nr_seq_emissor_w
and	((ie_tipo_estipulante = ie_tipo_estipulante_w) or (ie_tipo_estipulante = 'A'))
and	ie_situacao	= 'A';

if	(nr_seq_segurado_p is not null) and
	(nvl(nr_seq_segurado_p,0) <> 0) and
	(nr_seq_regra_carteira_w <> 0) then
	open c01;
	loop
	fetch c01 into	
		nr_seq_regra_w,
		nr_seq_apresentacao_w,
		ie_tipo_campo_w,
		ds_mascara_w;
	exit when c01%notfound;
		begin
		if	(ie_tipo_campo_w = 'V') then /* Digito verificador (Modulo 11 base 2) */
			select	substr(calcula_digito('Modulo11',cd_carteira_usuario_w),1,2)
			into	nr_digito_w
			from	dual;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_digito_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_digito_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'S') then /* Sequencial */
			begin
			select	nvl(nr_atual, nr_inicial) + 1
			into	nr_seq_atual_w
			from	pls_emissor_cart_numero
			where	nr_sequencia	= nr_seq_cart_numero_w;
			exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort( 213842, null);
				/* Para a regra de carteira que utiliza numero sequencial, deve-se informar a sequencia inicial. Cadastro geral "Emissor da carteira do beneficiario" - pasta "Numeracao da carteira". */
			end;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_seq_atual_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_seq_atual_w,0), length(ds_mascara_w), '0');
			end if;
			
			update	pls_emissor_cart_numero
			set	nr_atual	= nr_seq_atual_w,
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_cart_numero_w;
		elsif	(ie_tipo_campo_w = 'A') then /* Sequencia do produto */
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_seq_plano_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_seq_plano_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'E') then /* Sequencia do contrato */
			if	(ie_tipo_segurado_w not in('T','C')) then
				if	(instr(ds_mascara_w,'_') > 0) then
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_seq_contrato_w;
				else
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_seq_contrato_w,0), length(ds_mascara_w), '0');
				end if;
			elsif	(ie_tipo_segurado_w in ('T','C')) then	
				if	(instr(ds_mascara_w,'_') > 0) then
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_seq_intercambio_w;
				else
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_seq_intercambio_w,0), length(ds_mascara_w), '0');
				end if;
			end if;
		elsif	(ie_tipo_campo_w = 'CA') then /* Codigo do contrato no sistema anterior */
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_cod_contrato_anterior_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_cod_contrato_anterior_w,' '), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'BA') then /* Codigo do beneficiario no sistema anterior */
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_cod_segurado_anterior_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_cod_segurado_anterior_w,' '), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'I') then /* Sequencia interna do beneficiario no contrato */
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_seq_seg_contrato_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_seq_seg_contrato_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'T') then /* Sequencia do titular */
			select	nr_sequencia,
				nr_seq_titular,
				pls_obter_identifica_titular(nr_seq_titular,nr_sequencia)
			into	nr_seq_segurado_w,
				nr_seq_titular_w,
				nr_identificacao_titular_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_p;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				if	(nr_seq_titular_w is not null) then
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || substr(nr_seq_titular_w,1,8);
				else
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || substr(nr_seq_segurado_w,1,8);
				end if;
			else
				if	(nr_seq_titular_w is not null) then
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(substr(nr_seq_titular_w,1,8), length(ds_mascara_w), '0');
				else
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(substr(nr_seq_segurado_w,1,8), length(ds_mascara_w), '0');
				end if;
			end if;
		elsif	(ie_tipo_campo_w = 'TI') then /* Titularidade */
			select	nr_seq_titular,
				pls_obter_identifica_titular(nr_seq_titular,nr_sequencia)
			into	nr_seq_titular_w,
				nr_identificacao_titular_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_p;
			
			if	(ie_titularidade_w is not null) then
				nr_identificacao_titular_w	:= ie_titularidade_w;
			end if;
			
			if	(nr_seq_titular_w is null) then
				nr_identificacao_titular_w := 0;
			end if;
			
			cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nr_identificacao_titular_w, 2, '0');
		elsif	(ie_tipo_campo_w = 'NC') then /* Numero do contrato */
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_contrato_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(nr_contrato_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'VD') then /* Digito verificador (Modulo 10) */
			select	substr(calcula_digito('Modulo10',cd_carteira_usuario_w),1,2)
			into	nr_digito_w
			from	dual;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || nr_digito_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nr_digito_w, length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'CE') then /* CE - Codigo de controle de empresa na OPS */
			select	max(cd_operadora_empresa)
			into	cd_operadora_empresa_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_p;
			if	(nvl(cd_operadora_empresa_w,-999) <> -999) then
				if	(instr(ds_mascara_w,'_') > 0) then
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_operadora_empresa_w;
				else
					cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_operadora_empresa_w,0), length(ds_mascara_w), '0');
				end if;
			end if;
		elsif	(ie_tipo_campo_w = 'MA') then /* MA - Codigo da matricula da familia do beneficiario */
			select	cd_matricula_familia
			into	cd_matricula_familia_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_p;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_matricula_familia_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_matricula_familia_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'CM') then /* CM - Codigo da cooperativa medica */
			if	(ie_tipo_segurado_w in ('A','B')) then
				select	max(b.cd_cooperativa)
				into	cd_cooperativa_w
				from	pls_contrato	a,
					pls_congenere	b,
					pls_outorgante	c
				where	c.nr_sequencia	= a.nr_seq_operadora
				and	b.cd_cgc	= c.cd_cgc_outorgante
				and	a.nr_sequencia	= nr_seq_contrato_w;
			elsif	(ie_tipo_segurado_w in('T','C')) then
				select	max(b.cd_cooperativa)
				into	cd_cooperativa_w
				from	pls_congenere	b,
					pls_intercambio	a
				where	nvl(a.nr_seq_congenere,a.nr_seq_congenere) 	= b.nr_sequencia
				and	a.nr_sequencia		= nr_seq_intercambio_w;
			elsif	(ie_tipo_segurado_w in('I','P')) then
				select	max(b.cd_cooperativa)
				into	cd_cooperativa_w
				from	pls_congenere	b,
					pls_outorgante	a
				where	b.cd_cgc		= a.cd_cgc_outorgante
				and	a.cd_estabelecimento	= cd_estabelecimento_w;
			elsif	(ie_tipo_segurado_w	= 'R') then
				if	(nr_seq_contrato_w is not null) then
					select	max(b.cd_cooperativa)
					into	cd_cooperativa_w
					from	pls_contrato	a,
						pls_congenere	b,
						pls_outorgante	c
					where	c.nr_sequencia	= a.nr_seq_operadora
					and	b.cd_cgc	= c.cd_cgc_outorgante
					and	a.nr_sequencia	= nr_seq_contrato_w;
				elsif	(nr_seq_intercambio_w is not null) then
					select	max(b.cd_cooperativa)
					into	cd_cooperativa_w
					from	pls_congenere	b,
						pls_intercambio	a
					where	nvl(a.nr_seq_congenere,a.nr_seq_congenere) 	= b.nr_sequencia
					and	a.nr_sequencia		= nr_seq_intercambio_w;
				end if;
			end if;
			
			if	(length(cd_cooperativa_w) = 4) then
				cd_cooperativa_w	:= substr(cd_cooperativa_w,2,4);
			end if;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_cooperativa_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_cooperativa_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'SC') then /* SC - Codigo do sistema cooperativista*/
			if      (instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w   := cd_carteira_usuario_w || 0;
			else
				cd_carteira_usuario_w   := cd_carteira_usuario_w || lpad(0, length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'ME') then /* ME - Codigo da matricula do estipulante do beneficiario */
			select	max(cd_matricula_estipulante)
			into	cd_matricula_estipulante_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_p;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_matricula_estipulante_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_matricula_estipulante_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'CP') then /* CP - Codigo da carteira no produto */
			select	max(cd_codigo_carteira)
			into	cd_cd_codigo_carteira_w
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano_w;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_cd_codigo_carteira_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_cd_codigo_carteira_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'VF') then /* VF - Valor fixo*/
			select	max(cd_fixo)
			into	cd_fixo_w
			from	pls_regra_carteira_campo
			where	nr_sequencia	= nr_seq_regra_w;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_fixo_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_fixo_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'GI') then /*GI - Grupo de intercambio*/
			select	max(nr_seq_grupo_intercambio)
			into	nr_seq_grupo_intercambio_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_p;
			
			if	(nr_seq_grupo_intercambio_w is not null) then
				select	cd_grupo
				into	cd_grupo_w
				from	pls_regra_grupo_inter
				where	nr_sequencia	= nr_seq_grupo_intercambio_w;
			else
				cd_grupo_w	:= '0';
			end if;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_grupo_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_grupo_w,0), length(ds_mascara_w), '0');
			end if;
		elsif	(ie_tipo_campo_w = 'CT') then
			select	cd_codigo_ant
			into	cd_codigo_ant_w
			from	pls_tabela_preco
			where	nr_sequencia	= nr_seq_tabela_w;
			
			if	(instr(ds_mascara_w,'_') > 0) then
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || cd_codigo_ant_w;
			else
				cd_carteira_usuario_w	:= cd_carteira_usuario_w || lpad(nvl(cd_codigo_ant_w,0), length(ds_mascara_w), '0');
			end if;
		end if;
		end;
	end loop;
	close c01;
	
	if	(cd_carteira_usuario_w = '') then
		wheb_mensagem_pck.exibir_mensagem_abort( 213843, null);
		/* Problemas na geracao da carteira. Verifique o cadastro de regras para o emissor. */
	end if;
	
	select	count(*)
	into	qt_registros_w
	from	pls_segurado_carteira
	where	cd_usuario_plano	= cd_carteira_usuario_w
	and	nr_seq_segurado		<> nr_seq_segurado_p
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 213844, 'CD_CARTEIRA_USUARIO='||cd_carteira_usuario_w );
		/* cd_carteira_usuario_w||' Cartao de identificacao ja pertence a outro beneficiario. Verifique o cadastro de regras para o emissor. */
	end if;
	
	select	count(*)
	into	qt_registros_w
	from	pls_segurado_cart_ant
	where	cd_usuario_ant	= cd_carteira_usuario_w
	and	nr_seq_segurado		<> nr_seq_segurado_p;
	
	if	(qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 213845, 'CD_CARTEIRA_USUARIO='||cd_carteira_usuario_w );
		/* cd_carteira_usuario_w||' Cartao de identificacao pertence e igual a carteira anterior de outro beneficiario. Verifique o cadastro de regras para o emissor. */
	end if;
	
	select	max(nr_sequencia),
		max(cd_usuario_plano)
	into	nr_seq_carteira_w,
		cd_usuario_ant_w
	from	pls_segurado_carteira
	where	nr_seq_segurado	= nr_seq_segurado_p;
	
	if	(cd_usuario_ant_w <> cd_carteira_usuario_w) then
		insert into pls_segurado_cart_ant
			(nr_sequencia, nm_usuario, dt_atualizacao,
			cd_usuario_ant, dt_validade, dt_inicio_vigencia,
			nr_seq_segurado, dt_alteracao, nm_usuario_solicitacao,
			dt_solicitacao, ds_observacao, ds_trilha1, ds_trilha2,
			ds_trilha3,nr_via_anterior, ds_trilha_qr_code,
			dt_desbloqueio, nm_usuario_desbloqueio, ie_tipo_desbloqueio)
		select	pls_segurado_cart_ant_seq.nextval, nm_usuario_p, sysdate,
			cd_usuario_plano, dt_validade_carteira, dt_inicio_vigencia,
			nr_seq_segurado, sysdate, nm_usuario_solicitante,
			dt_solicitacao, ds_observacao, ds_trilha1, ds_trilha2,
			ds_trilha3,nr_via_solicitacao, ds_trilha_qr_code,
			dt_desbloqueio, nm_usuario_desbloqueio, ie_tipo_desbloqueio
		from	pls_segurado_carteira
		where	nr_seq_segurado	= nr_seq_segurado_p;
		
		select	nvl(max(nr_via_solicitacao),0) + 1
		into	nr_via_solicitacao_w
		from	pls_segurado_carteira
		where	nr_seq_segurado	= nr_seq_segurado_p;
		
		update	pls_segurado_carteira
		set	cd_usuario_plano	= cd_carteira_usuario_w,
			nr_via_solicitacao	= nr_via_solicitacao_w,
			nr_seq_lote_emissao	= null,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_situacao		= 'P',
			nm_usuario_solicitante	= nm_usuario_p,
			dt_solicitacao		= sysdate,
			nr_seq_mensalidade_seg	= null,
			ds_observacao		= wheb_mensagem_pck.get_texto(1127390),
			dt_desbloqueio		= null, 
			nm_usuario_desbloqueio	= null,
			ie_tipo_desbloqueio	= null
		where	nr_sequencia		= nr_seq_carteira_w;
		
		pls_desbloquear_auto_cartao(nr_seq_segurado_p, 'V', nm_usuario_p, cd_estabelecimento_p);
		
		pls_obter_trilhas_cartao(nr_seq_segurado_p, ds_trilha1_w, ds_trilha2_w, 
				ds_trilha3_w,ds_trilha_qr_code_w, nm_usuario_p);
		
		update	pls_segurado_carteira
		set	ds_trilha1		= ds_trilha1_w,
			ds_trilha2		= ds_trilha2_w,
			ds_trilha3		= ds_trilha3_w,
			ds_trilha_qr_code	= ds_trilha_qr_code_w
		where	nr_sequencia		= nr_seq_carteira_w;
		
		select	count(*)
		into	qt_carteira_pessoa_w
		from	pls_pessoa_carteira
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	cd_usuario_plano	= cd_carteira_usuario_w;
		
		if	(qt_carteira_pessoa_w = 0) then
			insert	into	pls_pessoa_carteira
				(	nr_sequencia, cd_pessoa_fisica, cd_usuario_plano,
					cd_estabelecimento, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec)
			values	(	pls_pessoa_carteira_seq.nextval, cd_pessoa_fisica_w, cd_carteira_usuario_w,
					cd_estabelecimento_w, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p);
		end if;
		
		/* Gerar historico */
		pls_gerar_segurado_historico(
			nr_seq_segurado_p, '9', sysdate,
			'pls_alterar_cartao_ident_benef', wheb_mensagem_pck.get_texto(1127389, 'CD_CARTEIRA_USUARIO='||cd_carteira_usuario_w), null,
			null, null, null,
			dt_alteracao_p, null, null,
			null, null, null,
			null, nm_usuario_p, 'N');
		
		pls_alterar_estagios_cartao(nr_seq_carteira_w,sysdate,1,cd_estabelecimento_w,nm_usuario_p);
	end if;
end if;

end pls_alterar_cartao_ident_benef;
/
