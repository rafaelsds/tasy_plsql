create or replace
procedure pls_oc_cta_tratar_val_89_imp(	nr_seq_combinada_p	pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p	pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p	pls_oc_cta_selecao_imp.nr_id_transacao%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Validar regime de interna��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------*/
dt_entrada_conta_w		date;
dt_alta_conta_w			date;
qt_horas_w			number(15,4);
ie_orig_conta_localizada_w	varchar2(1);
nr_idx_w			pls_integer := 0;
tb_seq_selecao_w		pls_util_cta_pck.t_number_table;
tb_valido_w			pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w			pls_util_cta_pck.t_varchar2_table_4000;
ie_regime_internacao_w		pls_oc_cta_regra_inter.ie_regime_internacao%type;
qt_minina_w			pls_oc_cta_regra_inter.qt_minina%type;
qt_maxima_w			pls_oc_cta_regra_inter.qt_maxima%type;
ie_regime_internacao_conta_w	pls_conta.ie_regime_internacao%type;
nr_seq_conta_prin_w		pls_conta.nr_sequencia%type;

cursor C01 (	nr_seq_oc_cta_comb_p	pls_oc_cta_combinada.nr_sequencia%type) is
	select	a.ie_valida,
		a.nr_seq_regra_reg_int,
		a.ie_dados_cta_pric
	from	pls_oc_cta_val_inter a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;	
/*
	Na tabela pls_conta_imp os campos dt_inicio faturamento e dt_fom_faturamento d�o 
	origem ao dt_entrada e dt_alta respectivamente na pls_conta.
*/	
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	sel.nr_sequencia nr_seq_selecao,
		conta.ie_regime_internacao,
		conta.dt_inicio_faturamento	dt_entrada,
		conta.dt_fim_faturamento	dt_alta,
		conta.nr_sequencia 	nr_seq_conta
	from	pls_oc_cta_selecao_imp	sel,
		pls_conta_imp		conta
	where	sel.nr_id_transacao	= nr_id_transacao_pc
	and	sel.ie_valido		= 'S'
	and	conta.nr_sequencia	= sel.nr_seq_conta;
					
begin

if	(nr_seq_combinada_p is not null) then

	for	r_C01_w in C01(nr_seq_combinada_p) loop
	
		/*Verifica se � para validar a ocorr�ncia e se foi apontada uma regra para aplica��o da valida��o*/
		if	(r_C01_w.ie_valida = 'S') and (r_C01_w.nr_seq_regra_reg_int is not null) then
		
			-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
			pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', ie_regra_excecao_p, nr_id_transacao_p, null);
			
			-- Incializar as listas para cada regra.
			pls_ocor_imp_pck.limpar_nested_tables( tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
			
			--Busca os dados da regra
			select	a.ie_regime_internacao,
				a.qt_minina,
				nvl(a.qt_maxima,0)
			into	ie_regime_internacao_w,
				qt_minina_w,
				qt_maxima_w
			from	pls_oc_cta_regra_inter a
			where	a.nr_sequencia	= r_C01_w.nr_seq_regra_reg_int;
			
			for r_C02_w in C02(nr_id_transacao_p) loop
				--Caso n�o for buscar da conta principal
				if	(r_C01_w.ie_dados_cta_pric = 'N') then
					
					ie_regime_internacao_conta_w	:= r_C02_w.ie_regime_internacao;
					dt_entrada_conta_w		:= r_C02_w.dt_entrada;
					dt_alta_conta_w			:= r_C02_w.dt_alta;
			
				else
					--Busca da conta principal
					nr_seq_conta_prin_w	:= pls_obter_cta_princ_atend_imp(r_C02_w.nr_seq_conta, null, null, ie_orig_conta_localizada_w);
					
					if	(nr_seq_conta_prin_w is not null) then
					
						if (ie_orig_conta_localizada_w = 'C') then
							select	ie_regime_internacao,
								dt_entrada,
								dt_alta
							into	ie_regime_internacao_conta_w,
								dt_entrada_conta_w,
								dt_alta_conta_w
							from	pls_conta_v
							where	nr_sequencia	= nr_seq_conta_prin_w;
						else
						
							select	ie_regime_internacao,
								dt_inicio_faturamento,
								dt_fim_faturamento
							into	ie_regime_internacao_conta_w,
								dt_entrada_conta_w,
								dt_alta_conta_w
							from	pls_conta_imp
							where	nr_sequencia	= nr_seq_conta_prin_w;
						
						end if;
					end if;
				end if;
				
				--Verifica o tipo de evento, caso for importa��o ent�o busca do campo de importa��o, sen�o busca da regime de interna��o normal
				if	(ie_regime_internacao_w = ie_regime_internacao_conta_w) then
				
					--Calcula a hora de interna��o
					qt_horas_w	:= trunc(((dt_alta_conta_w - dt_entrada_conta_w) * 24),2);
			
					--Caso a quantidade de horas n�o esteja conforme a regra, ent�o lan�a a ocorr�ncia
					if	(( qt_horas_w < qt_minina_w) or
						( qt_maxima_w > 0) and (qt_horas_w > qt_maxima_w)) then
						
						tb_seq_selecao_w(nr_idx_w)	:= 'S';
						tb_valido_w(nr_idx_w)		:= r_C02_w.nr_seq_selecao;
						tb_observacao_w(nr_idx_w)	:= 'A conta tem ' || obter_hora_duracao((dt_alta_conta_w - dt_entrada_conta_w) * 1440) || ' horas de interna��o';
						
						if	(nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
							pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w,
													nr_id_transacao_p,'SEQ');
							nr_idx_w := 0;
							-- Incializar as listas para cada regra.
							pls_ocor_imp_pck.limpar_nested_tables( tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
						else
							nr_idx_w := nr_idx_w + 1;
						end if;
			
					end if;
				end if;
				
			end loop;
			
			/*Lan�a as glosas caso existir registros que n�o foram gerados*/
			if	(nr_idx_w > 0)	then
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w,
										nr_id_transacao_p,'SEQ');
			end if;
			
			-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
			pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', ie_regra_excecao_p, null, nr_id_transacao_p, null);
			
		end if;
	end loop;
end if;

end pls_oc_cta_tratar_val_89_imp;
/