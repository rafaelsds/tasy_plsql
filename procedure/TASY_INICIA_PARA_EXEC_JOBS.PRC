create or replace
procedure Tasy_inicia_para_exec_jobs(
			job_queue_processes_p	number,
			ie_salvar_qtd_process_p	varchar2,
			ie_exec_comado_jobs_p	varchar2,
			qt_processes_p		out number) is 

nr_seq_atualizacao_w	number(10);
vl_job_queue_process_w	number(10);
ie_exec_job_w			varchar2(1);
job_w					number(10);
			
begin

select	max(nr_sequencia)
into	nr_seq_atualizacao_w
from	atualizacao_versao;

select	nvl(ie_exec_job, 'N'),
		nvl(vl_job_queue_process,-1)
into	ie_exec_job_w,
		vl_job_queue_process_w
from	atualizacao_versao
where	nr_sequencia = nr_seq_atualizacao_w;

if	(ie_exec_comado_jobs_p = 'N') then
	begin

		if	(ie_salvar_qtd_process_p = 'S') then
			begin
					begin
						EXECUTE IMMEDIATE ' select	nvl(value,-1) from	v$parameter  where	name = :bind_job' INTO qt_processes_p USING 'job_queue_processes';
					exception
						when others then
							qt_processes_p := -1;
					end;
			end;
		end if;
		
		if	(qt_processes_p > 0) then
			begin
				if (vl_job_queue_process_w < qt_processes_p) then
					begin
						vl_job_queue_process_w := qt_processes_p;
					end;
				end if;
			
				if	(vl_job_queue_process_w >0) then
					begin
						qt_processes_p := vl_job_queue_process_w;
							begin
								execute immediate ' alter system set job_queue_processes='||job_queue_processes_p;
							exception
								when others then
									qt_processes_p := vl_job_queue_process_w;
							end;
					end;
			end if; 
			
			end;
		end if;
		
	end;
else
	begin
	
	begin
		
		tasy_verifica_job_exec('Tasy');
        
		select	distinct(job)
		into	job_w
		from	job_v
		where	comando	like '%Tasy_executar_jobs_versao%';
		
		--DBMS_JOB.run(job_w);
		
		if	(job_queue_processes_p > 0) then
			begin
			execute immediate ' alter system set job_queue_processes='||job_queue_processes_p;
			end;
		end if;
		
		exception
			when others then
				--N�o foi poss�vel executar os comandos das jobs que ficaram pendendtes durante a atualiza��o! 
				Wheb_mensagem_pck.exibir_mensagem_abort(282689);
		end;
		
		if	(ie_exec_job_w = 'S') then
			begin
			
			update	atualizacao_versao
			set		ie_exec_job = 'N'
			where	nr_sequencia = nr_seq_atualizacao_w;
			
			commit;
			
			end;
		end if;	
	
	
	end;
end if;
	
end Tasy_inicia_para_exec_jobs;
/		
