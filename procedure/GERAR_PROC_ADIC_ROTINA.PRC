create or replace
procedure gerar_proc_adic_rotina(	nr_seq_agenda_p		number,
					ds_lista_grupo_item_p	varchar2,
					nm_usuario_p		varchar2) is 

cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);	
nr_seq_exame_w		number(10);
nr_seq_proc_interno_w	number(10);

/* ======================================================================================================================================*/
ds_comando_w			Varchar2(2000);
ds_result_w				varchar2(0001);
ds_possib_w				varchar2(2000);
qt_controle_w			number(10);
qt_pos_separador_w		number(10);
ds_possib_aux_w			varchar2(2000);
ds_resultado_w			varchar2(2000);
/* ======================================================================================================================================*/
					
cursor c01 is
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_exame,
		nr_seq_proc_interno
	from	agenda_cons_grupo_item
	where	Obter_Se_Contido(nr_sequencia,ds_resultado_w) = 'S';
					
begin

/* ======================================================================================================================================*/
ds_possib_w := ds_lista_grupo_item_p||',';

if	(instr(ds_possib_w,'(') > 0 ) and
	(instr(ds_possib_w,')') > 0 ) then
	ds_possib_w		:= substr(ds_lista_grupo_item_p,(instr(ds_lista_grupo_item_p,'(')+1),(instr(ds_lista_grupo_item_p,')')-2));
end if;
qt_controle_w 		:= 0;

qt_pos_separador_w 	:= instr(ds_possib_w,',');

if	( qt_pos_separador_w = 0 )  then
	ds_resultado_w	:= substr(ds_possib_w,1, instr(ds_possib_w,'-') - 1);
else
	while	( qt_pos_separador_w > 0 )  and
		( qt_controle_w < 999 ) loop
		ds_possib_aux_w		:= substr(ds_possib_w,1,qt_pos_separador_w);
		ds_resultado_w		:= ds_resultado_w || substr(ds_possib_aux_w,1,instr(ds_possib_aux_w,'-')-1)||',';
		ds_possib_w		:= substr(ds_possib_w,qt_pos_separador_w+1,length(ds_possib_w));
		qt_pos_separador_w 	:= instr(ds_possib_w,',');
		qt_controle_w		:= qt_controle_w + 1;
	end loop;
ds_resultado_w := substr(ds_resultado_w,1,length(ds_resultado_w)-1);	
end if;
/* ======================================================================================================================================*/



open c01;
loop
fetch c01 into	
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_exame_w,
	nr_seq_proc_interno_w;
exit when c01%notfound;
	begin
	
	
		
	insert	into agenda_consulta_proc 
		(
		nr_sequencia,
		nr_seq_agenda,
		cd_procedimento,
		ie_origem_proced,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_proc_interno,
		ie_executar_proc,
		nr_seq_exame
		)	
		values
		(
		agenda_consulta_proc_seq.NextVal,
		nr_seq_agenda_p,
		cd_procedimento_w,
		ie_origem_proced_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_proc_interno_w,
		'S',
		nr_seq_exame_w
		);
	
		
	end;
end loop;
close c01;

commit;

end gerar_proc_adic_rotina;
/
