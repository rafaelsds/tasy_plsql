create or replace
procedure gerar_regra_agenda_cirurgica(	nr_sequencia_p		number,
					lista_agenda_p		varchar,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) 
					is 
					
lista_agenda_w		varchar2(2000);	
cd_agenda_w		number(10);
ie_contador_w		number(10,0):= 0;
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
ie_rastrear_origem_w	varchar2(1);
				
				
begin

Obter_Param_Usuario(871, 541, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_rastrear_origem_w);

lista_agenda_w	:= lista_agenda_p;

while	lista_agenda_w is not null or
	ie_contador_w > 200 loop
	begin
	tam_lista_w		:= length(lista_agenda_w);
	ie_pos_virgula_w	:= instr(lista_agenda_w,',');
	
	if	(ie_pos_virgula_w <> 0) then
		cd_agenda_w 	:= substr(lista_agenda_w,1,(ie_pos_virgula_w - 1));
		lista_agenda_w	:= substr(lista_agenda_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	if	(nr_sequencia_p is not null) and
		(cd_agenda_w is not null) then
		insert	into	agenda_regra
			(nr_sequencia,
			cd_estabelecimento,
			cd_agenda,
			dt_atualizacao,
			nm_usuario,
			ie_permite,
			cd_convenio,
			cd_area_proc,
			cd_especialidade,
			cd_grupo_proc,
			cd_procedimento,
			ie_origem_proced,
			cd_medico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_proc_interno,
			nr_seq_regra,
			cd_municipio_ibge,
			qt_regra,
			nr_seq_grupo,
			nr_seq_forma_org,
			nr_seq_subgrupo,
			nr_seq_classif_agenda,
			cd_categoria,
			ie_forma_consistencia,
			ie_medico,
			cd_perfil,
			cd_plano_convenio,
			nr_seq_turno,
			dt_inicio_agenda,
			dt_fim_agenda,
			ie_dia_semana,
			ie_turno,
			qt_horas_ant_agenda,
			nr_seq_equipe,
			nm_regra,
			nr_seq_agenda_regra,
			ds_mensagem,
			hr_inicio,
			hr_fim,
			ie_validar_dias_semana,
			ie_agenda)
			select	agenda_regra_seq.nextval,
				cd_estabelecimento,
				cd_agenda_w,
				sysdate,
				nm_usuario_p,
				ie_permite,
				cd_convenio,
				cd_area_proc,
				cd_especialidade,
				cd_grupo_proc,
				cd_procedimento,
				ie_origem_proced,
				cd_medico,
				sysdate,
				nm_usuario_p,
				nr_seq_proc_interno,
				nr_seq_regra,
				cd_municipio_ibge,
				qt_regra,
				nr_seq_grupo,
				nr_seq_forma_org,
				nr_seq_subgrupo,
				nr_seq_classif_agenda,
				cd_categoria,
				ie_forma_consistencia,
				ie_medico,
				cd_perfil,
				cd_plano_convenio,
				nr_seq_turno,
				dt_inicio_agenda,
				dt_fim_agenda,
				ie_dia_semana,
				ie_turno,
				qt_horas_ant_agenda,
				nr_seq_equipe,
				nm_regra,
				decode(ie_rastrear_origem_w,'S',nr_sequencia_p,null),
				ds_mensagem,
				hr_inicio,
				hr_fim,
				ie_validar_dias_semana,
				ie_agenda
			from 	agenda_regra
			where	nr_sequencia = 	nr_sequencia_p;
	commit;			
	end if;	
	ie_contador_w	:= ie_contador_w + 1;
	end;
end loop;

end gerar_regra_agenda_cirurgica;
/
