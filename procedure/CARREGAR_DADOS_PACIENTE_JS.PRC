create or replace
procedure carregar_dados_paciente_js(nr_cirurgia_p		number,
									 nr_sequencia_p		number,
									 nr_atendimento_p  	number,
									 nm_pessoa_fisica_p out	varchar2,
									 qt_idade_p	   		out	varchar2,
									 qt_peso_p	   		out varchar2,
									 qt_altura_p        out varchar2,
									 ds_procedimento_p 	out varchar2,
									 nm_usuario_p		varchar2) is 

cursor c01 is
select substr(converte_pri_letra_maiusculo(obter_desc_procedimento(cd_procedimento_princ,ie_origem_proced)),1,255) ds_procedimento
from	cirurgia
where	(((nr_cirurgia_p > 0) and (nr_cirurgia = nr_cirurgia_p)) or
		 ((nr_sequencia_p > 0) and (nr_seq_pepo = nr_sequencia_p))) 
and		ie_status_cirurgia <> 3
order by nr_cirurgia;
					
qtd_w				number;
ds_procedimento_w 	varchar2(255);

			
begin

if	(nr_cirurgia_p > 0) then
	select	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255), 
		substr(obter_idade(b.dt_nascimento, sysdate, 'A'),1,100) qt_idade
	into	nm_pessoa_fisica_p,
		qt_idade_p
        from	cirurgia a, pessoa_fisica b
        where	a.nr_cirurgia = nr_cirurgia_p
        and 	a.cd_pessoa_fisica = b.cd_pessoa_fisica;
else
	select	b.nm_pessoa_fisica, 
		substr(obter_idade(b.dt_nascimento, sysdate, 'A'),1,100) qt_idade
	into	nm_pessoa_fisica_p,
		qt_idade_p
        from	pepo_cirurgia a, pessoa_fisica b
        where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	a.nr_sequencia = nr_sequencia_p;
end if;



if	(nr_cirurgia_p > 0) then

	select	max(obter_sinal_vital_grafico(nr_atendimento_p,nr_cirurgia_p,'Peso'))
	into	qt_peso_p
	from 	dual;

	select 	max(obter_sinal_vital_grafico(nr_atendimento_p,nr_cirurgia_p,obter_desc_expressao(283402)/*'Altura'*/))
	into	qt_altura_p
	from 	dual;

	select	
		substr(converte_pri_letra_maiusculo(obter_desc_procedimento(c.cd_procedimento_princ,c.ie_origem_proced)),1,255) ds_procedimento
	into 
		ds_procedimento_p
		from cirurgia c
		where c.nr_cirurgia  = nr_cirurgia_p;
else
	select	max(obter_sinal_vital_grafico(nr_atendimento_p,0,'Peso'))
	into	qt_peso_p
	from 	dual;

	select 	max(obter_sinal_vital_grafico(nr_atendimento_p,0,obter_desc_expressao(283402)/*'Altura'*/))
	into	qt_altura_p
	from 	dual;
	select 
		max(substr(converte_pri_letra_maiusculo(obter_desc_procedimento(c.cd_procedimento_princ,c.ie_origem_proced)),1,255)) ds_procedimento 
	into
		ds_procedimento_p
		from cirurgia c , pepo_cirurgia p 
		where  p.nr_cirurgia_principal = c.nr_cirurgia
		and c.nr_seq_pepo = nr_sequencia_p;	
	
	/*OS 858471 - Ajustado para caso o atendimento n�o possua uma cirurgia principal, o sistema busque o procedimento da cirurgia(caso tenha apenas uma cirurgia)*/
	/*OS 1002702 - Ajustado para apresentar o primeiro procedimento da �rvorem, caso n�o tenha principal selecionado e tenha mais de um*/
	if (ds_procedimento_p is null) then		
					
		open c01;
		loop
		fetch c01 into ds_procedimento_w;
		exit when c01%notfound;
			begin
				ds_procedimento_p := ds_procedimento_w;
			end;
		end loop;
		close c01;
	end if;	
end if;


end carregar_dados_paciente_js;
/
