create or replace procedure atualiza_prot_convenio_retorno(	nr_sequencia_p		number,
						nr_seq_protocolo_p	number,
						nm_usuario_p		varchar2,
                        			nr_seq_prot_adicional_p varchar2) is

begin
if	(nr_seq_protocolo_p is not null) and
	(nr_sequencia_p	is not null) then
	begin

    if(nr_seq_prot_adicional_p is null) then
        update	convenio_retorno
        set	nr_seq_protocolo = nr_seq_protocolo_p
        where	nr_sequencia = nr_sequencia_p;
    else
        update	convenio_retorno
        set	nr_seq_protocolo = nr_seq_protocolo_p,
        nr_seq_prot_adic = nvl(nr_seq_prot_adicional_p,'')
        where	nr_sequencia = nr_sequencia_p;
    end if;
    
    end;
end if;
commit;

end atualiza_prot_convenio_retorno;
/
