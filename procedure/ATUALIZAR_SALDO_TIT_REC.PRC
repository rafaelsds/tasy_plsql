create or replace
PROCEDURE Atualizar_Saldo_Tit_Rec
		(nr_titulo_p	number,
		nm_usuario_p	varchar2) is

dt_recebimento_w		date;
vl_recebido_w			number(15,2);
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_desconto_w			number(15,2);
vl_glosa_w			number(15,2);

vl_titulo_w			number(15,2);
vl_alteracao_w			number(15,2);
vl_saldo_w			number(15,2);
vl_saldo_juros_w		number(15,2);
vl_saldo_multa_w		number(15,2);
ie_situacao_w			varchar2(1)	:= '2';
cd_estabelecimento_w		number(4);
ie_liq_perda_w			varchar2(1)	:= 'N';
vl_titulo_original_w		number(15,2);
ie_desdobrado_w			varchar(1);
vl_perdas_w			number(15,2);
vl_tributo_w			number(15,2);
nr_seq_cobranca_w		number(10);
vl_nota_credito_w		number(15,2);
ie_tipo_titulo_w		varchar2(2);
vl_cambial_passivo_w		number(15,2);
ie_dt_liquidacao_w		varchar2(1);
qt_tributo_w			number(10);
ie_atualiza_saldo_juro_multa_w	varchar2(1);
ie_data_mais_atual_w		varchar2(1);
ie_tipo_consistencia_w		number(5);
vl_baixa_w			number(15,2);
vl_rec_maior_w			number(15,2);
dt_liquidacao_w			date;
ie_data_liq_rec_maior_w		varchar2(5) := 'S';
nr_seq_baixa_w			number(5);
ie_consistencia_perda_w		number(10);
ie_passivo_saldo_tit_w		varchar2(1);
nr_seq_alt_valor_w		alteracao_valor.nr_sequencia%type;

nr_seq_tit_trib_w		titulo_receber_trib.nr_sequencia%type;
vl_trib_titulo_w		titulo_receber_trib.vl_tributo%type;
vl_baixa_trib_tit_w		titulo_receber_trib_baixa.vl_baixa%type;
nr_seq_baixa_trib_tit_w		titulo_receber_trib_baixa.nr_sequencia%type;
vl_saldo_tributo_w		titulo_receber_trib.vl_saldo%type;
ie_negociado_w			varchar(1);
ie_titulo_negociado_w		parametro_contas_receber.ie_titulo_negociado%type;
nr_seq_liq_w			titulo_receber_liq.nr_sequencia%type;
ie_baixa_negociacao_w		varchar(1);
ie_unificado_w			varchar(1);
situacao_tit_unificado_w	varchar(1);
ie_transferido_w		varchar(1);
qt_tipo_baixas_w		number(10);
ie_somente_baixas_perdas_w	varchar2(1);
ie_origem_titulo_w		titulo_receber.ie_origem_titulo%type;
nr_seq_solic_resc_fin_w		pls_solic_rescisao_fin.nr_sequencia%type;
nr_seq_contrato_w		contrato.nr_sequencia%type;

Cursor c01 is
	select	nr_sequencia
	from	cobranca
	where	nr_titulo	= nr_titulo_p;

cursor	c02 is
select	a.nr_sequencia,
	a.vl_tributo
from	titulo_receber_trib a
where	a.nr_titulo	= nr_titulo_p;

BEGIN

select	nvl(max(dt_recebimento),sysdate),
	nvl(sum(vl_recebido),0),
	nvl(sum(vl_juros),0),
	nvl(sum(vl_multa),0),
	nvl(sum(vl_descontos),0),
	nvl(sum(vl_glosa),0),
	nvl(sum(vl_perdas),0),
	nvl(sum(vl_nota_credito),0),
	nvl(sum(vl_cambial_passivo),0)
into 	dt_recebimento_w,
	vl_recebido_w,
	vl_juros_w,
	vl_multa_w,
	vl_desconto_w,
	vl_glosa_w,
	vl_perdas_w,
	vl_nota_credito_w,
	vl_cambial_passivo_w
from	titulo_receber_liq
where	nr_titulo = nr_titulo_p
and	nvl(ie_lib_caixa, 'S') = 'S';

select	max(nr_sequencia)
into	nr_seq_baixa_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_p;

if	(nvl(nr_seq_baixa_w,0) > 0) then
	select	ie_tipo_consistencia
	into	ie_consistencia_perda_w
	from	tipo_recebimento b,
		titulo_receber_liq a
	where	a.nr_titulo		= nr_titulo_p
	and	a.nr_sequencia 		= nr_seq_baixa_w
	and	a.cd_tipo_recebimento	= b.cd_tipo_recebimento;
	
	/*OS 1475567 - Tratar o parametro, para so liquidar como perda se o titulo possuir somente baixas com consistencia de perdas. Se tiver outro recebimento no titulo que na oseja perda, nao liquidar  como perda*/
	obter_param_usuario(801,213,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w, ie_somente_baixas_perdas_w);
	
	select	count(distinct b.ie_tipo_consistencia)
	into	qt_tipo_baixas_w
	from	titulo_receber_liq a,
			tipo_recebimento b
	where	a.cd_tipo_recebimento 	= b.cd_tipo_recebimento
	and		a.nr_titulo				= nr_titulo_p
	and not exists (select 1 --Nao considerar a baixa que foi estornada
				    from   titulo_receber_liq x
					where  x.nr_titulo 	  = a.nr_titulo
					and    x.nr_seq_liq_origem is not null
					and	   x.nr_seq_liq_origem = a.nr_sequencia)
	and		a.nr_seq_liq_origem is null; -- Nao considerar baixas geradas no estorno	

	if	(ie_consistencia_perda_w = 9) and
        (((nvl(ie_somente_baixas_perdas_w,'N') = 'S') and (qt_tipo_baixas_w = 1)) or (nvl(ie_somente_baixas_perdas_w,'N') = 'N')) then --Baixa por perdas, dominio 708
		ie_liq_perda_w	:= 'S';
	end if;
end if;

select	nvl(max(vl_titulo),0),
	nvl(max(vl_saldo_titulo),0),
	nvl(max(vl_saldo_juros),0),
	nvl(max(vl_saldo_multa),0),
	max(ie_tipo_titulo),
	max(cd_estabelecimento),
	max(ie_origem_titulo),
	max(nr_seq_contrato)
into	vl_titulo_original_w,
	vl_saldo_w,
	vl_saldo_juros_w,
	vl_saldo_multa_w,
	ie_tipo_titulo_w,
	cd_estabelecimento_w,
	ie_origem_titulo_w,
	nr_seq_contrato_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p;

gerenciamento_contrato_pck.consistir_tit_receber_contrato(nr_seq_contrato_w, nr_titulo_p, vl_titulo_original_w, nm_usuario_p, 'S');

obter_param_usuario(801,102,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_data_mais_atual_w);

if	(nvl(ie_data_mais_atual_w,'N') <> 'S') then

	select	nvl(max(dt_ultima_baixa), sysdate)
	into	dt_recebimento_w
	from	(select	max(dt_recebimento) dt_ultima_baixa
		from	titulo_receber_liq
		where	nr_titulo	= nr_titulo_p
		and	dt_atualizacao	= (select max(dt_atualizacao) from titulo_receber_liq where nr_titulo = nr_titulo_p));

end if;

obter_param_usuario(801,103,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w, situacao_tit_unificado_w );
/*Parametro [103]  - Situacao dos titulos ao gerar novo titulo a partir da unificacao
"T" - Alterar situacao para Transferido
"N" - Cancelar o titulo
"S" - Liquidar o titulo
*/

/* Francisco - 25/01/2008 - OS 80618 - Atualizar quando transferido */
select	decode(count(1),0,'N','S')
into	ie_desdobrado_w
from	titulo_receber_desdob
where	nr_titulo	= nr_titulo_p
and	nr_titulo_dest	is not null;

/*acdalfovo - 04/10/2016 - OS 1231660 - Parametro "Alterar titulo para 'Transferido' ao encerrar negociacao", funcao Parametros do Contas a Receber*/
select	nvl(max(ie_titulo_negociado),'N')
into	ie_titulo_negociado_w
from	parametro_contas_receber
where	cd_estabelecimento = cd_estabelecimento_w;

select	decode(count(1),0,'N','S')
into	ie_negociado_w
from	titulo_rec_negociado a,
	negociacao_cr b
where	a.nr_seq_negociacao = b.nr_sequencia
and	a.nr_titulo	= nr_titulo_p
and	b.ie_status <> 'CA'
and	ie_titulo_negociado_w = 'S';

select 	max(nr_sequencia)
into	nr_seq_liq_w
from 	titulo_receber_liq
where 	nr_titulo = nr_titulo_p;

/*acdalfovo - 04/10/2016 - OS 1231660 - Verificacao da ultima baixa incluida no titulo, que deve ser proveniente de negociacao para situacao "Titulo transferido"*/
select	decode(count(1),0,'N','S')
into	ie_baixa_negociacao_w
from 	titulo_receber_liq 
where 	nr_titulo = nr_titulo_p
and	nr_sequencia = nr_seq_liq_w
and	nr_seq_negociacao_cr is not null;

/*acdalfovo - 04/10/2016 - OS 1231660*/
select	decode(count(1),0,'N','S')
into	ie_unificado_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p
and	nr_titulo_dest is not null --NR_TITULO_DEST: Gerado pelo sistema com o titulo destino para os casos de unificacao
and	nvl(situacao_tit_unificado_w,'N') = 'T'; --opcao "T" : Alterar situacao para Transferido

/*acdalfovo - 04/10/2016 - OS 1231660*/
if	(ie_desdobrado_w = 'S' or (ie_negociado_w = 'S' and ie_baixa_negociacao_w = 'S') or ie_unificado_w = 'S') then
	ie_transferido_w := 'S';
end if;

begin
select	nvl(a.ie_dt_liquidacao,'N'),
	nvl(ie_atualiza_saldo_juro_multa,'N'),
	nvl(ie_data_liq_rec_maior,'S'),
	nvl(ie_passivo_saldo_tit,'S')
into	ie_dt_liquidacao_w,
	ie_atualiza_saldo_juro_multa_w,
	ie_data_liq_rec_maior_w,
	ie_passivo_saldo_tit_w
from	parametro_contas_receber a
where	a.cd_estabelecimento	= cd_estabelecimento_w;
exception
when no_data_found then
	/* Parametros do contas a receber nao encontrados! */
	wheb_mensagem_pck.exibir_mensagem_abort(186551);
end;

if	(ie_passivo_saldo_tit_w = 'N') then
	vl_cambial_passivo_w	:= 0;
end if;

if	(nvl(ie_dt_liquidacao_w,'N') = 'S') then

	select	count(1)
	into	qt_tributo_w
	from	titulo_receber_trib a
	where	a.nr_titulo	= nr_titulo_p;

	if	(qt_tributo_w > 0) then

		select	nvl(max(a.dt_tributo), dt_recebimento_w) 
		into	dt_recebimento_w
		from	titulo_receber_trib a
		where	a.nr_titulo	= nr_titulo_p
		and	a.dt_tributo	>= dt_recebimento_w;

	end if;

end if;

/* Edgar 05/03/2004, inclui este select para calcular a alteracao do valr do titulo OS6751 */
select	nvl(sum(decode(ie_aumenta_diminui, 'A', vl_alteracao, 'D', vl_alteracao * -1)), 0)
into	vl_alteracao_w
from	alteracao_valor
where	nr_titulo	=	nr_titulo_p;

select	nvl(sum(decode(ie_origem_tributo, 'S', VL_TRIBUTO*-1, 'D', VL_TRIBUTO, 'CD', VL_TRIBUTO)), 0)
into	vl_tributo_w
from	titulo_receber_trib
where	nr_titulo	= nr_titulo_p;

vl_titulo_w 	:=  nvl(vl_titulo_original_w,0) + nvl(vl_alteracao_w,0) - (nvl(vl_recebido_w,0) + nvl(vl_glosa_w,0) + nvl(vl_desconto_w,0) + nvl(vl_perdas_w,0) + nvl(vl_nota_credito_w,0)) - nvl(vl_tributo_w,0) - nvl(vl_cambial_passivo_w,0);

/* Francisco - OS 252631 - 28/09/2010  */
if	(ie_atualiza_saldo_juro_multa_w = 'S') then
	vl_saldo_juros_w	:= obter_saldo_juro_multa_tit_rec(nr_titulo_p,'J');
	vl_saldo_multa_w	:= obter_saldo_juro_multa_tit_rec(nr_titulo_p,'M');
end if;

vl_saldo_juros_w := vl_saldo_juros_w - vl_juros_w;

if (vl_saldo_juros_w < 0) then
	vl_saldo_juros_w := 0;
end if;

vl_saldo_multa_w := vl_saldo_multa_w - vl_multa_w;

if (vl_saldo_multa_w < 0) then
	vl_saldo_multa_w := 0;
end if;

if 	((vl_titulo_w < 0) and (ie_tipo_titulo_w <> '9')) then /* Titulo de desconto em folha pode ser feito  a baixa mesmo com valor diferente */
	rollback;
	vl_baixa_w	:= nvl(vl_titulo_original_w,0) + nvl(vl_alteracao_w,0) - nvl(vl_titulo_w,0);

	/* Valor das baixas supera o valor do titulo!
	Titulo: nr_titulo_p
	Saldo: vl_saldo_w
	Baixa: vl_baixa_w */
	wheb_mensagem_pck.exibir_mensagem_abort(186552,	'NR_TITULO_P=' || nr_titulo_p ||
							';VL_SALDO_W=' || Campo_Mascara_virgula(nvl(vl_titulo_original_w,0)) ||
							';VL_BAIXA_W=' || Campo_Mascara_virgula(vl_baixa_w) ||
							';VL_ALTERACAO_W=' || Campo_Mascara_virgula(nvl(vl_alteracao_w,0)));
end if;

/* Francisco - OS 63607 - 24/07/07 - Nao deixar realizar estorno superior ao valor do titulo */
if 	(vl_tributo_w >= 0 and vl_titulo_w > vl_titulo_original_w + vl_alteracao_w) or
	(vl_tributo_w < 0 and vl_titulo_w - (vl_tributo_w*-1) > vl_titulo_original_w + vl_alteracao_w) then
	rollback;
	vl_baixa_w	:= (vl_recebido_w + vl_glosa_w + vl_desconto_w + nvl(vl_cambial_passivo_w,0));
	/* Valor dos estornos supera o valor do titulo!
	Titulo: nr_titulo_p
	Saldo: vl_saldo_w
	Baixa: vl_baixa_w */
	wheb_mensagem_pck.exibir_mensagem_abort(186554,	'NR_TITULO_P=' || nr_titulo_p ||
							';VL_SALDO_W=' || vl_saldo_w ||
							';VL_BAIXA_W=' || vl_baixa_w);
end if;

if	(nvl(ie_data_liq_rec_maior_w,'S') = 'N') then
	select	max(dt_liquidacao)
	into	dt_liquidacao_w
	from	titulo_receber
	where	nr_titulo = nr_titulo_p;

	if	(dt_liquidacao_w is not null) then
		select	nvl(max(vl_rec_maior),0)
		into	vl_rec_maior_w
		from	titulo_receber_liq
		where	nr_titulo = nr_titulo_p
		--and	nvl(ie_lib_caixa,'S') = 'S'	OS 579650 - COMENTADA ESTA RESTRICAO, POIS E USADO NO FECHAR_CAIXA_RECEB
		and	nr_sequencia = (select	max(nr_sequencia)
					from	titulo_receber_liq
					where	nr_titulo = nr_titulo_p);

		-- OS 558318 - Nao alterar DT_LIQUIDACAO nos casos em que o titulo esta com saldo ZERO e o recibento for a maior
		if	(vl_rec_maior_w > 0) then
			dt_recebimento_w := dt_liquidacao_w;
		end if;
	end if;
end if;

select	max(a.nr_sequencia)
into	nr_seq_alt_valor_w
from	alteracao_valor a
where	a.nr_titulo		= nr_titulo_p;

select	nvl(max(a.dt_alteracao),dt_recebimento_w)
into	dt_recebimento_w
from	alteracao_valor a
where	a.ie_aumenta_diminui	= 'D'
and	a.vl_anterior		= a.vl_alteracao
and	a.nr_sequencia		= nr_seq_alt_valor_w
and	a.nr_titulo		= nr_titulo_p;

update	titulo_receber
set	vl_saldo_titulo	= vl_titulo_w,
	vl_saldo_juros	= vl_saldo_juros_w,
	vl_saldo_multa	= vl_saldo_multa_w,
	dt_liquidacao	= decode(vl_titulo_w, 0, dt_recebimento_w, null),
	ie_situacao	= decode(vl_titulo_w, 0, decode(ie_transferido_w,'S','5',decode(ie_liq_perda_w,'S','6','2')), '1'),
	nm_usuario 	= nm_usuario_p,
	dt_atualizacao 	= dt_recebimento_w
where	nr_titulo 	= nr_titulo_p
and	ie_situacao	<> '3';

/* Francisco - 31/10/2010 - Obter tipo de consistencia da ultima baixa
para identificar se foi baixado por inadimplencia */
select	a.ie_situacao
into	ie_situacao_w
from	titulo_receber a
where	a.nr_titulo	= nr_titulo_p;

/* So verificar caso esteja com status liquidado */
if	(ie_situacao_w = '2') then
	select	max(b.ie_tipo_consistencia)
	into	ie_tipo_consistencia_w
	from	tipo_recebimento b,
		titulo_receber_liq a
	where	a.cd_tipo_recebimento	= b.cd_tipo_recebimento
	and	nvl(a.ie_lib_caixa,'S')	= 'S'
	and	a.nr_titulo		= nr_titulo_p
	and	a.dt_recebimento	= 	(select	max(x.dt_recebimento)
						from	titulo_receber_liq x
						where	x.nr_titulo	= a.nr_titulo
						and	nvl(x.ie_lib_caixa,'S') = 'S');

	/* Baixa por inadimplencia */
	if	(ie_tipo_consistencia_w = 15) then
		update	titulo_receber
		set	ie_liq_inadimplencia	= 'S',
			ie_negociado	= 'N'
		where	nr_titulo		= nr_titulo_p;
	end if;

	if	(ie_origem_titulo_w = '19') then
		select	max(nr_seq_solic_resc_fin)
		into	nr_seq_solic_resc_fin_w
		from	pls_solic_resc_fin_venc
		where	nr_titulo = nr_titulo_p;
		
		if	(nr_seq_solic_resc_fin_w is not null) then
			update 	pls_solic_rescisao_fin
			set	ie_status = '5'
			where	nr_sequencia = nr_seq_solic_resc_fin_w;
		end if;
	end if;
end if;

ATUALIZAR_TIT_RECEBER_CLASSIF(nr_titulo_p, 'N', nm_usuario_p);

open c01;
loop
fetch c01 into
	nr_seq_cobranca_w;
exit when c01%notfound;
	atualizar_saldo_cobranca(nr_seq_cobranca_w,nm_usuario_p,'N');
end loop;
close c01;

Atualizar_cobranca(nr_titulo_p, 0, 'N', cd_estabelecimento_w, nm_usuario_p);	-- Jacson OS 47203

/* ajustar a diferenca de arredondamento da baixa dos impostos */
if	(nvl(vl_titulo_w,0)	= 0) then

	open	c02;
	loop
	fetch	c02 into
		nr_seq_tit_trib_w,
		vl_trib_titulo_w;
	exit	when c02%notfound;

		select	sum(a.vl_baixa),
			max(a.nr_sequencia)
		into	vl_baixa_trib_tit_w,
			nr_seq_baixa_trib_tit_w
		from	titulo_receber_trib_baixa a
		where	a.nr_seq_tit_trib	= nr_seq_tit_trib_w
		and	a.nr_titulo		= nr_titulo_p;

		if	(nvl(vl_baixa_trib_tit_w,0) <> 0) and
			(nvl(vl_trib_titulo_w,0) <> nvl(vl_baixa_trib_tit_w,0)) then

			if	(nvl(vl_trib_titulo_w,0) > nvl(vl_baixa_trib_tit_w,0)) then

				update	titulo_receber_trib_baixa
				set	vl_baixa	= nvl(vl_baixa,0) + nvl(vl_trib_titulo_w,0) - nvl(vl_baixa_trib_tit_w,0)
				where	nr_seq_tit_trib	= nr_seq_tit_trib_w
				and	nr_titulo	= nr_titulo_p
				and	nr_sequencia	= nr_seq_baixa_trib_tit_w;

			elsif	(nvl(vl_baixa_trib_tit_w,0) > nvl(vl_trib_titulo_w,0)) then

				update	titulo_receber_trib_baixa
				set	vl_baixa	= nvl(vl_baixa,0) - (nvl(vl_baixa_trib_tit_w,0) - nvl(vl_trib_titulo_w,0))
				where	nr_seq_tit_trib	= nr_seq_tit_trib_w
				and	nr_titulo	= nr_titulo_p
				and	nr_sequencia	= nr_seq_baixa_trib_tit_w;

			end if;

		end if;

		select	sum(a.vl_baixa)
		into	vl_baixa_trib_tit_w
		from	titulo_receber_trib_baixa a
		where	a.nr_seq_tit_trib	= nr_seq_tit_trib_w
		and	a.nr_titulo		= nr_titulo_p;

		update	titulo_receber_trib
		set	vl_saldo 	= nvl(vl_trib_titulo_w,0) - nvl(vl_baixa_trib_tit_w,0)
		where	nr_sequencia 	= nr_seq_tit_trib_w;
		
		/* Verifica o saldo para atualizar a data contabil do tributo */
		select	max(a.vl_saldo)
		into	vl_saldo_tributo_w
		from	titulo_receber_trib a
		where	a.nr_sequencia	= nr_seq_tit_trib_w
		and	a.nr_titulo	= nr_titulo_p;
		
		if (nvl(vl_saldo_tributo_w,0) <= 0) then
			update	titulo_receber_trib
			set	dt_contabil = sysdate
			where	nr_sequencia = nr_seq_tit_trib_w;
		else
			update	titulo_receber_trib
			set	dt_contabil = null
			where	nr_sequencia = nr_seq_tit_trib_w;
		end if;

	end	loop;
	close	c02;

end if;

ATUALIZAR_SALDO_TIT_REC_TRIB(nr_titulo_p, nm_usuario_p);

/* Francisco - OS 194015 - 05/02/2010 */
pls_habilitar_atend_susp(nr_titulo_p,nm_usuario_p);

/* Francisco - 04/06/2010 - Tratar  status negociacao */
atualizar_status_negociacao_cr(nr_titulo_p,nm_usuario_p);

select	max(a.ie_situacao)
into	ie_situacao_w
from	titulo_receber a
where	a.nr_titulo	= nr_titulo_p;

/*OS 1596977 - Adicionei para somente fazer essa chamada qdo o titulo for liquidado*/
if	(vl_titulo_w = 0) and (ie_situacao_w <> '6') then 
	liberar_titulo_orgao_cobr(nr_titulo_p,nm_usuario_p);
end if;

END Atualizar_Saldo_Tit_Rec;
/
