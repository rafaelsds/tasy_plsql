create or replace
procedure atualizar_qt_baixa_nconta(	qt_baixa_nconta_p	number,
					nr_prescricao_p		number,
					nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 
begin
if	(qt_baixa_nconta_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update 	prescr_material
	set    	qt_baixa_nconta = qt_baixa_nconta_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where  	nr_prescricao   = nr_prescricao_p
	and    	nr_sequencia    = nr_sequencia_p;
	
	commit;
	end;	
end if;
end atualizar_qt_baixa_nconta;
/