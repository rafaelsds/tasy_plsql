create or replace
procedure assumir_pac_medico_desfecho_pa (
		ie_desfecho_p		varchar2,
		cd_medico_destino_p	varchar2,
		nr_atendimento_p	number,
		ie_forma_assumir_p	varchar2,
		ie_medico_p		varchar2,
		ie_medico_transf_int_p	varchar2,
		nm_usuario_p		varchar2) is

begin
if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	if	(ie_desfecho_p is not null) then
		begin
		if	(cd_medico_destino_p is not null) then
			begin
			if	(ie_desfecho_p <> 'A') then
				begin
				assumir_paciente(nr_atendimento_p, obter_pessoa_fisica_usuario(nm_usuario_p,'C'), ie_forma_assumir_p, nm_usuario_p);
				end;
			end if;			
			if	(ie_medico_p = 'S') and
				(ie_desfecho_p = 'I') then
				begin
				assumir_paciente(nr_atendimento_p, cd_medico_destino_p, ie_forma_assumir_p, nm_usuario_p);
				end;
			end if;			
			if 	(ie_medico_transf_int_p = 'S') and
				(ie_desfecho_p = 'T') then
				begin
				assumir_paciente(nr_atendimento_p, cd_medico_destino_p, ie_forma_assumir_p, nm_usuario_p);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;
commit;
end assumir_pac_medico_desfecho_pa;
/