create or replace
procedure envia_sms_pendencia_ata_corp(	nm_usuario_p Varchar2, nr_dia_semana_p number) is 


cd_pessoa_resp_w		proj_ata_pendencia.cd_pessoa_resp%type;
ds_pendencia_w		proj_ata_pendencia.ds_pendencia%type;
dt_prev_solucao_w		proj_ata_pendencia.dt_prev_solucao%type;
dt_repactuada_w		proj_ata_pendencia.dt_repactuada%type;
nr_sequencia_w		man_ordem_servico.nr_sequencia%type;
nr_telefone_destino_w	varchar2(50);
nr_telefone_origem_w	varchar2(50) := 'SMS-OS';
ds_mensagem_w		varchar2(160);	--Verificado no site da TWW que podem ser enviados apenas 160 caracteres no SMS
id_sms_w			number(10,0);
ds_titulo_w 	varchar2(20);
nr_dia_semana number(10);

Cursor C01 is
select	distinct
		c.cd_pessoa_resp,
		substr(c.ds_pendencia,1,68) description,
		c.dt_prev_solucao conclusion,
		c.dt_repactuada rescheduled_to,
		s.nr_sequencia service_order,
		decode(nr_seq_classif_reuniao,3,'Business Review','Pending issue')
	from	ata_reuniao a,
		proj_ata b,
		proj_ata_pendencia c,
		proj_ata_classif_pend d,
		proj_ata_pendencia_os o,
		man_ordem_servico s
	where	c.nr_seq_ata		= b.nr_sequencia
	and	b.nr_seq_reuniao	= a.nr_sequencia 
	and	d.nr_sequencia		= c.nr_seq_classif
	and	c.nr_sequencia		= o.nr_seq_pendencia
	and	s.nr_sequencia		= o.nr_seq_ordem
	and	s.ie_status_ordem	<> 3
	and	c.dt_conclusao_real is null
	and substr(obter_se_envia_sms_ata(obter_usuario_pf(c.cd_pessoa_resp)),1,1) = 'S'
	and a.nr_seq_classif_reuniao IN (3, 30,27,31,29,28)
	order by  1; 
	
begin

if nr_dia_semana_p is null then
  return;
end if;

--obtem dia semana
select to_char(sysdate, 'd') 
into   nr_dia_semana
from dual;
	
if nr_dia_semana <> nr_dia_semana_p then
  return;
end if;

-- s� executa para se o dia do sysdate for igual ao dia do parametro
open C01;
loop
fetch C01 into	
	cd_pessoa_resp_w,
	ds_pendencia_w,
	dt_prev_solucao_w,
	dt_repactuada_w,
	nr_sequencia_w,
	ds_titulo_w;	
exit when C01%notfound;
	begin
	
	begin
		execute immediate 'alter session SET NLS_DATE_LANGUAGE = ''ENGLISH'' '; 
		if	(nvl(cd_pessoa_resp_w,'X') <> 'X') then
			select	replace(replace(substr(elimina_caracteres_especiais(b.NR_DDI_CELULAR || b.NR_DDD_CELULAR || b.nr_telefone_celular),1,50),'(',''),')','')
			into	nr_telefone_destino_w
			from	pessoa_fisica b
			where	b.cd_pessoa_fisica = cd_pessoa_resp_w;
		end if;

		if	(nvl(nr_telefone_destino_w,'X') <> 'X') and
			(nvl(nr_telefone_origem_w,'X') <> 'X') then
			begin
			ds_mensagem_w := substr(ds_titulo_w  || chr(10) || chr(13) ||
						'SO '|| nr_sequencia_w || chr(10) || chr(13) ||
						ds_pendencia_w || '...' || chr(10) || chr(13) ||
						'Due date: ' || to_char(dt_prev_solucao_w, 'dd-MON-yyyy'),1,160);

			
			wheb_sms.enviar_sms(nr_telefone_origem_w, nr_telefone_destino_w, ds_mensagem_w, nm_usuario_p, id_sms_w);
			end;
		end if;
	exception
	when others then
		ds_titulo_w := '';	
	end;
	
	end;
end loop;
close C01;

end envia_sms_pendencia_ata_corp;
/ 
