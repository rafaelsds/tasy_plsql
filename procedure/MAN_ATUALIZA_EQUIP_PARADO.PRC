create or replace
procedure	man_atualiza_equip_parado(
			nr_seq_equipamento_p		number,
			ie_parado_p			varchar2,
			nm_usuario_p			varchar2) is

begin

if	(nr_seq_equipamento_p is not null) and
	(ie_parado_p is not null) then

	update	man_equipamento
	set	ie_parado = ie_parado_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_equipamento_p;

	commit;

end if;

end man_atualiza_equip_parado;
/