CREATE OR REPLACE
procedure gerar_ordem_item_entrega(
			nr_ordem_compra_p		number,
			nr_item_oci_p	number,
			nm_usuario_p		varchar2) as

nr_ordem_compra_w	ordem_compra_item.nr_ordem_compra%type;
nr_item_oci_w		ordem_compra_item.nr_item_oci%type;			
qt_prevista_entrega_w	ordem_compra_item.qt_material%type;
dt_entrega_w		ordem_compra.dt_entrega%type;			
			
BEGIN

nr_ordem_compra_w := nr_ordem_compra_p;
nr_item_oci_w := nr_item_oci_p;

select 	qt_material
into 	qt_prevista_entrega_w
from 	ordem_compra_item
where 	nr_ordem_compra = nr_ordem_compra_w
and 	nr_item_oci = nr_item_oci_w;

select 	dt_entrega
into 	dt_entrega_w
from 	ordem_compra
where 	nr_ordem_compra = nr_ordem_compra_w;

insert into ordem_compra_item_entrega(
	nr_sequencia,
	nr_ordem_compra,
	nr_item_oci,
	dt_prevista_entrega,
	dt_real_entrega,
	dt_entrega_original,
	dt_entrega_limite,
	qt_prevista_entrega,
	qt_real_entrega,
	dt_atualizacao,
	nm_usuario,
	ds_observacao)
values(	ordem_compra_item_entrega_seq.nextval,
	nr_ordem_compra_w,
	nr_item_oci_w,
	dt_entrega_w,
	null,
	dt_entrega_w,
	dt_entrega_w,
	qt_prevista_entrega_w,
	null,
	sysdate,
	nm_usuario_p,
	null);	

commit;

END gerar_ordem_item_entrega;
/