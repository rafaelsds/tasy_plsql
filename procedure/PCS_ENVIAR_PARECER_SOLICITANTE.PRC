create or replace
procedure pcs_enviar_parecer_solicitante( nr_seq_parecer_p	number,
										cd_estabelecimento_p	number,
										nm_usuario_p		Varchar2) is 

nm_usuario_solicitante_w	usuario.nm_usuario%type;
ds_email_usuario_w		usuario.ds_email%type;
ds_email_origem_w		usuario.ds_email%type;
ds_assunto_w			regra_envio_email_compra.ds_assunto%type;
ds_mensagem_padrao_w	regra_envio_email_compra.ds_mensagem_padrao%type;
ds_material_w			material.ds_material%type;
nr_registro_w			pcs_reg_analise.nr_sequencia%type;
nr_seq_regra_w   		regra_envio_email_compra.nr_sequencia%type;
ds_email_remetente_w	regra_envio_email_compra.ds_email_remetente%type;
ds_email_adicional_w	regra_envio_email_compra.ds_email_adicional%type;
ds_destinatarios_w		Varchar2(4000);
ds_pessoa_dest_w		pessoa_fisica.nm_pessoa_fisica%type;
ds_resposta_w			pcs_parecer.ds_resposta%type;
ds_origem_w				Varchar2(50);

Cursor C01 is
	select	nvl(nr_sequencia,0),
			nvl(ds_email_remetente,'X'),
			replace(ds_email_adicional,',',';'),
			ds_assunto,
			ds_mensagem_padrao
	from	regra_envio_email_compra
	where	ie_tipo_mensagem = 106
	and		ie_situacao = 'A'
	and		cd_estabelecimento = cd_estabelecimento_p;

begin

select  nm_usuario,
		substr(cd_material || ' - ' || obter_desc_material(cd_material),1,255),
		nr_seq_registro_pcs,
		ds_resposta,
		substr(obter_nome_pf(cd_pessoa_dest),1,60),
		decode(ie_origem, 'R', WHEB_MENSAGEM_PCK.get_texto(799394), 'L', WHEB_MENSAGEM_PCK.get_texto(799396)) ds_origem
into	nm_usuario_solicitante_w,
		ds_material_w,
		nr_registro_w,
		ds_resposta_w,
		ds_pessoa_dest_w,
		ds_origem_w
from	pcs_parecer a
where	a.nr_sequencia = nr_seq_parecer_p;


begin
select	max(ds_email)
into	ds_email_usuario_w
from	usuario 
where	nm_usuario = nm_usuario_solicitante_w
and		ie_situacao = 'A';
exception
when others then
	ds_email_usuario_w := 'X';
end;

begin
select	max(ds_email)
into	ds_email_origem_w
from	usuario 
where	nm_usuario = nm_usuario_p
and		ie_situacao = 'A';
exception
when others then
	ds_email_origem_w := 'X';
end;

if (nvl(ds_email_usuario_w,'X') <> 'X') then
	ds_destinatarios_w := ds_email_usuario_w || ';';
end if;



open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ds_email_remetente_w,
	ds_email_adicional_w,
	ds_assunto_w,
	ds_mensagem_padrao_w;
exit when C01%notfound;
	begin
	
	ds_destinatarios_w := substr(ds_destinatarios_w || ds_email_adicional_w || ';',1,4000);

	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_seq_parecer',to_char(nr_seq_parecer_p)),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_destinatário',ds_pessoa_dest_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_material',ds_material_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_registro',nr_registro_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_resposta',ds_resposta_w),1,255);	
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_origem',ds_origem_w),1,255);	
	
	
	ds_mensagem_padrao_w := substr(replace_macro(ds_mensagem_padrao_w,'@nr_seq_parecer',to_char(nr_seq_parecer_p)),1,2000);
	ds_mensagem_padrao_w := substr(replace_macro(ds_mensagem_padrao_w,'@ds_destinatário',ds_pessoa_dest_w),1,2000);
	ds_mensagem_padrao_w := substr(replace_macro(ds_mensagem_padrao_w,'@ds_material',ds_material_w),1,2000);
	ds_mensagem_padrao_w := substr(replace_macro(ds_mensagem_padrao_w,'@nr_registro',nr_registro_w),1,2000);
	ds_mensagem_padrao_w := substr(replace_macro(ds_mensagem_padrao_w,'@ds_resposta',ds_resposta_w),1,2000);		
	ds_mensagem_padrao_w := substr(replace_macro(ds_mensagem_padrao_w,'@ds_origem',ds_origem_w),1,2000);
	end;
end loop;
close C01;	


if ((ds_email_origem_w is not null) and
	(ds_destinatarios_w is not null) and
	(nm_usuario_p is not null)) then
	begin
	enviar_email(ds_assunto_w,ds_mensagem_padrao_w,ds_email_origem_w,ds_destinatarios_w,nm_usuario_p,'M');

	end;
end if;
	


commit;

end pcs_enviar_parecer_solicitante;
/