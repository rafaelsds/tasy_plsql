create or replace
procedure cancela_recebimento_medic(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	recebimento_frac_med
set	nm_usuario_cancelamento = nm_usuario_p,
	dt_cancelamento		= sysdate
where	nr_sequencia 		= nr_sequencia_p;

commit;

end cancela_recebimento_medic;
/