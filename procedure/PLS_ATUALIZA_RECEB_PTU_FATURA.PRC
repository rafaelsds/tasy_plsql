create or replace
procedure pls_atualiza_receb_ptu_fatura(nr_seq_fatura_p    ptu_fatura.nr_sequencia%type,
						 dt_recebimento_p   Date,
						 nm_usuario_p		usuario.nm_usuario%type) is 

ie_inconsistencia_w 		varchar2(1);
dt_emissao_w    			Date;
ie_emissao_igual_receb_w 	varchar2(1);

begin

pls_consistir_dt_receb_fatura(dt_recebimento_p, ie_inconsistencia_w);

if 'S' = ie_inconsistencia_w then 
	wheb_mensagem_pck.exibir_mensagem_abort(1037212);
end if;

obter_param_usuario(1293, 21, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_emissao_igual_receb_w);
if ('S' = ie_emissao_igual_receb_w) then
	dt_emissao_w := dt_recebimento_p;
end if;

if nvl(nr_seq_fatura_p, 0) > 0 then
	update	ptu_fatura
	set 	dt_recebimento_fatura 	= nvl(dt_recebimento_p,dt_recebimento_fatura),
		dt_emissao_fatura 	= nvl(dt_emissao_w,dt_emissao_fatura)
	where 	nr_sequencia 		= nr_seq_fatura_p;
end if;
commit;

end pls_atualiza_receb_ptu_fatura;
/