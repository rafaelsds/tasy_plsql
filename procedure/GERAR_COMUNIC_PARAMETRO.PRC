create or replace
procedure gerar_comunic_parametro
			(	cd_funcao_p		number,
				nr_seq_parametro_p	number,
				ie_tipo_alteracao_p	varchar2,
				vl_parametro_p		varchar2,
				cd_estab_ref_p		varchar2,
				cd_perfil_p		varchar2,
				nm_usuario_ref_p	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ie_tipo_envio_p		varchar2) is
					
ds_comunicado_w		varchar2(4000)	:= null;
ds_usuarios_destino_w	varchar2(4000)	:= null;
ds_perfis_destino_w	varchar2(4000)	:= null;
ds_usuario_email_w	varchar2(4000)	:= null;
ds_lista_emails_w	varchar2(4000)	:= null;

ds_parametro_w		varchar2(255);
vl_anterior_w		varchar2(255);
vl_novo_w		varchar2(255);
ds_titulo_w		varchar2(255);
ds_usuario_alter_w	varchar2(255);
nr_seq_classif_w	number(10);
tam_lista_w		number(10);
ie_pos_virgula_w	number(3)	:= 0;
nm_usuario_w		varchar2(15);
ds_email_destino_w	varchar2(4000);
i  number(10);

begin
if	(nr_seq_parametro_p is not null) then
	ds_usuarios_destino_w	:= obter_valor_param_usuario(6001,36,null,nm_usuario_p,cd_estabelecimento_p);
	ds_perfis_destino_w	:= obter_valor_param_usuario(6001,37,null,nm_usuario_p,cd_estabelecimento_p);
	ds_usuario_email_w	:= obter_valor_param_usuario(6001,155,null,nm_usuario_p,cd_estabelecimento_p);
	
	if	(nvl(somente_numero(obter_valor_param_usuario(6001,38,null,nm_usuario_p,cd_estabelecimento_p)),0) <> 0) then
		nr_seq_classif_w	:= somente_numero(obter_valor_param_usuario(6001,38,null,nm_usuario_p,cd_estabelecimento_p));
	end if;

	select	max(obter_desc_expressao(cd_exp_parametro, ds_parametro))
	into	ds_parametro_w
	from	funcao_parametro
	where	cd_funcao	= cd_funcao_p
	and	nr_sequencia	= nr_seq_parametro_p;

	if	(ie_tipo_alteracao_p = 'A') then
		ds_titulo_w	:= wheb_mensagem_pck.get_texto(348087);
		--ds_titulo_w	:= 'Altera��o de par�metro do sistema';

		ds_comunicado_w	:= wheb_mensagem_pck.get_texto(348088,	'NR_SEQ_PARAMETRO=' || nr_seq_parametro_p || ';DS_PARAMETRO=' || ds_parametro_w) || chr(13);
		--ds_comunicado_w	:=	'Alterado o par�metro [' || nr_seq_parametro_p || '] - ' || ds_parametro_w  || chr(13);
	elsif	(ie_tipo_alteracao_p = 'E') then
		ds_titulo_w	:= wheb_mensagem_pck.get_texto(348089);
		--ds_titulo_w	:= 'Exclus�o de par�metro do sistema';
		
		ds_comunicado_w	:= wheb_mensagem_pck.get_texto(348090,	'NR_SEQ_PARAMETRO=' || nr_seq_parametro_p || ';DS_PARAMETRO=' || ds_parametro_w) || chr(13);
		--ds_comunicado_w	:=	'Exclu�do o par�metro [' || nr_seq_parametro_p || '] - ' || ds_parametro_w  || chr(13);
	end if;

	if	(nm_usuario_ref_p is not null) then
		select	max(substr(obter_nome_usuario(nm_usuario_ref_p),1,255))
		into	ds_usuario_alter_w
		from	dual;
	end if;

	if	(ie_tipo_envio_p = '1') then
		select	max(vl_parametro)
		into	vl_anterior_w
		from	funcao_param_usuario
		where	cd_funcao	= cd_funcao_p
		and	nr_sequencia	= nr_seq_parametro_p
		and	upper(nm_usuario_param)	= upper(nm_usuario_ref_p);
	elsif (ie_tipo_envio_p = '2')  then
		select	max(vl_parametro)
		into	vl_anterior_w
		from	funcao_param_perfil
		where	cd_funcao	= cd_funcao_p
		and	nr_sequencia	= nr_seq_parametro_p
		and	cd_perfil	= cd_perfil_p;
	elsif (ie_tipo_envio_p = '3') then
		select	max(vl_parametro)
		into	vl_anterior_w
		from	funcao_param_estab
		where	cd_funcao		= cd_funcao_p
		and	nr_seq_param		= nr_seq_parametro_p
		and	cd_estabelecimento	= cd_estab_ref_p;
	else
		select	max(vl_parametro)
		into	vl_anterior_w
		from	funcao_parametro
		where	cd_funcao	= cd_funcao_p
		and	nr_sequencia	= nr_seq_parametro_p;
		commit;		
		
	end if;
	
	ds_comunicado_w	:= wheb_mensagem_pck.get_texto(348091,	'DS_COMUNICADO=' || ds_comunicado_w ||
								';DS_FUNCAO=' || substr(obter_desc_funcao(cd_funcao_p),1,255) ||
								';DS_DADOS=' || '' ||
								';DS_ESTAB=' || substr(obter_nome_estabelecimento(cd_estab_ref_p),1,255) ||
								';DS_PERFIL=' || substr(obter_desc_perfil(cd_perfil_p),1,255) ||
								';DS_USUARIO=' || ds_usuario_alter_w);
	
	/*ds_comunicado_w	:=	ds_comunicado_w ||chr(13) || chr(10) || 
				'Fun��o: ' || substr(obter_desc_funcao(cd_funcao_p),1,255) || chr(13) || chr(10) || chr(13) || chr(10) ||
				'Dados da altera��o: ' || chr(13) || chr(10) || chr(13) || chr(10) ||
				'Estabelecimento:	' || substr(obter_nome_estabelecimento(cd_estab_ref_p),1,255) || chr(13) || chr(10) ||
				'Perfil: 		' || substr(obter_desc_perfil(cd_perfil_p),1,255) || chr(13) || chr(10) ||
				'Usu�rio refer�ncia:	' || ds_usuario_alter_w || chr(13) || chr(10) || chr(13)|| chr(10);*/

	vl_novo_w	:= vl_parametro_p;

	if	(cd_funcao_p = 7002) and
		(nr_seq_parametro_p = 12) then
		vl_anterior_w	:= rpad('*',LENGTH(vl_anterior_w),'*');
		vl_novo_w	:= rpad('*',LENGTH(vl_novo_w),'*');
	else
		vl_anterior_w	:= substr(obter_desc_param_senha(cd_funcao_p,nr_seq_parametro_p,vl_anterior_w),1,255);
		vl_novo_w	:= substr(obter_desc_param_senha(cd_funcao_p,nr_seq_parametro_p,vl_novo_w),1,255);
	end if;

	if	(ie_tipo_alteracao_p = 'A') then
		ds_comunicado_w	:= wheb_mensagem_pck.get_texto(348092,	'DS_COMUNICADO=' || ds_comunicado_w ||
									';VL_ANTERIOR=' || vl_anterior_w||
									';VL_ATUAL=' || vl_novo_w);
	
		/*ds_comunicado_w	:= ds_comunicado_w ||
				'Valor anterior:	' || vl_anterior_w || chr(13) || chr(10) ||
				'Valor atual:	' || vl_novo_w || chr(13)|| chr(10);*/
	end if;

	ds_comunicado_w	:= wheb_mensagem_pck.get_texto(348093,	'DS_COMUNICADO=' || ds_comunicado_w ||
									';DS_USUARIO=' || substr(obter_nome_usuario(nm_usuario_p),1,255));
	/*ds_comunicado_w	:= 	ds_comunicado_w ||
				'Usu�rio altera��o:	' || substr(obter_nome_usuario(nm_usuario_p),1,255); */
end if;

if	(ds_comunicado_w is not null) then
	gerar_comunic_padrao(	sysdate,
				ds_titulo_w,
				ds_comunicado_w,
				nm_usuario_p,
				'N',
				ds_usuarios_destino_w,
				'N',
				nr_seq_classif_w,
				ds_perfis_destino_w,
				null,
				null,
				sysdate,
				null,
				null);

	if	(ds_usuario_email_w is not null) then
		begin	
		ds_lista_emails_w := substr(ds_usuario_email_w,1, length(ds_usuario_email_w));	
		WHILE	(ds_lista_emails_w is not null) LOOP
			begin		
			tam_lista_w	:= length( ds_lista_emails_w );
			ie_pos_virgula_w 	:= instr( ds_lista_emails_w ,',');
			
			if	(ie_pos_virgula_w <> 0) then
				nm_usuario_w	:= substr(ds_lista_emails_w,1,(ie_pos_virgula_w - 1));
				ds_lista_emails_w	:= substr(ds_lista_emails_w,(ie_pos_virgula_w + 1),tam_lista_w);
			else
				nm_usuario_w	:= ds_lista_emails_w;
				ds_lista_emails_w 	:= null;
			end if;	

			select  obter_dados_usuario_opcao(nm_usuario_w,'E') ds_email_pf
			into	ds_email_destino_w
			from   	dual;					
			
			enviar_email(	ds_titulo_w,
					ds_comunicado_w,
					null,
					ds_email_destino_w,
					nm_usuario_p,
					'A');
			end;
		END loop;
		end;
	end if;
end if;
	
commit;

end gerar_comunic_parametro;
/