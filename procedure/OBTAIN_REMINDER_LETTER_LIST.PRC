create or replace
procedure obtain_reminder_letter_list(ds_comando_p varchar2,
					letter_list out dbms_sql.varchar2_table) as


c001		Integer;		
retorno_w	Integer;
ds_valor_w	varchar2(5000);
qt_register_w	number(10);

begin
qt_register_w	:= 1;
	begin
	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ds_comando_p, dbms_sql.Native);
	DBMS_SQL.DEFINE_COLUMN(C001, 1, ds_valor_w,32000);
	retorno_w := DBMS_SQL.execute(c001); 
	while	( DBMS_SQL.FETCH_ROWS(C001) > 0 ) loop
		DBMS_SQL.COLUMN_VALUE(C001, 1, ds_valor_w );
		letter_list(qt_register_w) := ds_valor_w;
		qt_register_w := qt_register_w + 1;
	end loop;
	DBMS_SQL.CLOSE_CURSOR(C001);
	exception
	when others then
		DBMS_SQL.CLOSE_CURSOR(C001);
	end;
end obtain_reminder_letter_list;
/