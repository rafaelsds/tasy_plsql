create or replace
procedure fis_reg_ecf_0930(
				nr_seq_controle_p	varchar2,
				ds_separador_p		varchar2,
				cd_estabelecimento_p	varchar2,
				nm_usuario_p		varchar2,
				cd_empresa_p		varchar2,
				qt_linha_p		in out  number,
				nr_sequencia_p		in out	number,
				ie_scp_p		varchar2) is 

				
nr_seq_registro_w	number(10) := nr_sequencia_p;	
nr_linha_w		number(10) := qt_linha_p;
ds_arquivo_w		varchar2(4000);	
ds_arquivo_compl_w	varchar2(4000);	
ds_linha_w		varchar2(4000);
sep_w			varchar2(2) := ds_separador_p;
tp_registro_w		varchar2(5);

cd_titular_w		estabelecimento.cd_titular%type;
cd_contabilista_w	estabelecimento.cd_contabilista%type;
nr_crc_w		empresa.nr_crc%type;
nm_signatario_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_cpf_w		pessoa_fisica.nr_cpf%type;
cd_assinante_dnrc_w	varchar2(3);
ds_email_w		compl_pessoa_fisica.ds_email%type;
nr_telefone_w		compl_pessoa_fisica.nr_telefone%type; 
nr_ddd_telefone_w	compl_pessoa_fisica.nr_ddd_telefone%type; 
telefone_w		varchar2(20);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;


cursor c_pessoa is
	select	nm_pessoa_fisica nm_signatario,
		substr(nr_cpf,1,14) cd_cpf,
		'203' cd_assinante_dnrc,
		substr(obter_compl_pf(cd_pessoa_fisica, 2, 'M'), 1, 60) ds_email,
		obter_compl_pf(cd_pessoa_fisica, 2, 'DDT')	nr_ddd_telefone,
		substr(obter_compl_pf(cd_pessoa_fisica, 2, 'T'), 1, 15) nr_telefone
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_titular_w
	union
	select	nm_pessoa_fisica nm_signatario,
		substr(nr_cpf,1,14) cd_cpf,
		'900' cd_assinante_dnrc,
		substr(obter_compl_pf(cd_pessoa_fisica, 2, 'M'), 1, 60) ds_email,
		obter_compl_pf(cd_pessoa_fisica, 2, 'DDT')	nr_ddd_telefone,
		substr(obter_compl_pf(cd_pessoa_fisica, 2, 'T'), 1, 15) nr_telefone
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_contabilista_w
	order by 4;

begin

cd_estabelecimento_w:= cd_estabelecimento_p;

if	(ie_scp_p = 'S') then
	begin
	
	select	cd_estab_socio_ost_scp
	into	cd_estabelecimento_w
	from	estabelecimento a
	where	a.cd_estabelecimento = cd_estabelecimento_p;
	
	exception
	when others then
		cd_estabelecimento_w:= null;
	end;
end if;

select	max(cd_contabilista),
	max(cd_titular),
	max(nr_crc)
into	cd_contabilista_w,
	cd_titular_w,
	nr_crc_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_w;

select	nvl(cd_contabilista_w, cd_contabilista),
	nvl(cd_titular_w, cd_titular),
	nvl(nr_crc_w, nr_crc)
into	cd_contabilista_w,
	cd_titular_w,
	nr_crc_w
from	empresa
where	cd_empresa = cd_empresa_p;

open c_pessoa;
loop
fetch c_pessoa into	
	nm_signatario_w,
	cd_cpf_w,
	cd_assinante_dnrc_w,
	ds_email_w,
	nr_ddd_telefone_w,
	nr_telefone_w;
exit when c_pessoa%notfound;
	begin
	tp_registro_w		:= '0930';
	telefone_w		:= nr_ddd_telefone_w || nr_telefone_w;

	ds_linha_w	:= substr(	sep_w || tp_registro_w 				|| -- Registro (1)
					sep_w || nm_signatario_w 			|| -- Nome do Signat�rio (2)
					sep_w || cd_cpf_w 				|| -- CPF/CNPJ (3)
					sep_w || cd_assinante_dnrc_w 		|| -- C�digo de qualifica��o do assinante (4)
					sep_w || nr_crc_w	 			|| -- N�mero de inscri��o do contabilista no Conselho Regional de Contabilidade (5)
					sep_w || ds_email_w	 		|| -- E-mail do signat�rio (6)
					sep_w || substr(telefone_w,1,14)		|| sep_w,1,8000); -- DDD e telefone do signat�rio (7)

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_ecf_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_ecf,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		tp_registro_w,
		ds_arquivo_w,
		ds_arquivo_compl_w);
	
	end;
end loop;
close c_pessoa;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

fis_reg_ecf_0990(nr_seq_controle_p,ds_separador_p,cd_estabelecimento_p,nm_usuario_p,cd_empresa_p,qt_linha_p,nr_sequencia_p);

end fis_reg_ecf_0930;
/