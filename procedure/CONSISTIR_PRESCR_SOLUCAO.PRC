create or replace
procedure Consistir_prescr_solucao(	nr_prescricao_p		number,
					nr_seq_solucao_p	number,
					cd_estabelecimento_p	number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					ds_erro_p		out varchar2) is

ie_urgencia_w					varchar2(1);
ie_acm_w						varchar2(1);
ie_se_necessario_w				varchar2(1);
cd_intervaloAcmSn_w				varchar2(5);
hr_prim_horario_w				varchar2(5);
ds_solucao_w					varchar2(10);
nr_seq_erro_w					number(10,0);
ie_prim_hor_w					varchar2(1);
ie_erro_liberar_prescr_w		varchar2(1);
qt_erro_nao_lib_w				number(10,0);
qt_erro_lib_w					number(10,0);
qt_solucao_w					number(10,0);
qt_antimicrobiano_w				number(10,0);
nr_seq_solucao_w				number(10,0);
IE_ESQUEMA_ALTERNADO_w			varchar2(1);
dt_inicio_prescr_w				date;
dt_suspensao_w					date;
dt_validade_prescr_w			date;
dt_prim_horario_w				date;
ie_setor_regra_w				varchar2(255);
ie_material_prescricao_w		varchar2(10);
ie_consistir_setor_material_w	varchar2(10);
ie_hemodialise_w				varchar2(10);
ie_material_disponivel_w		varchar2(1);
ie_solucao_especial_w			varchar2(1);
ie_solucao_pca_w				varchar2(1);
qt_saldo_disponivel_estoque_w	number(15,4);
cd_estab_prescr_w				number(4,0);
nr_horas_validade_w				number(5,0);
qt_horas_validade_w				number(15,4);
cd_material_w					number(6,0);
nr_seq_dialise_w				number(10);
cd_local_prescr_w				number(4,0);
nr_seq_material_w				number(10);
nr_etapas_w						number(3,0);
qt_hora_fase_w					number(15,4);
qt_tempo_aplicacao_w			number(15,4);
ie_via_aplicacao_w				varchar2(15);
ie_via_medic_w					varchar2(15);
ie_tipo_analgesia_w				varchar2(15);
ie_pca_modo_prog_w				varchar2(15);
qt_vol_infusao_pca_w			number(10,1);
qt_bolus_pca_w					number(10,1);
qt_intervalo_bloqueio_w			number(10);
qt_dose_inicial_pca_w			number(18,6);
qt_limite_quatro_hora_w			number(10,1);
qt_limite_uma_hora_w			number(10,1);
ds_horarios_w					varchar2(2000);
ie_fora_validade_w				varchar2(1);
cd_intervalo_w					varchar2(20);
ie_aberto_w                     varchar2(1);
dt_prescricao_w					prescr_medica.dt_prescricao%type;
ie_um_dose_inicio_pca_w			prescr_solucao.ie_um_dose_inicio_pca%type;
ie_um_limite_pca_w				prescr_solucao.ie_um_limite_pca%type;
ie_um_limite_hora_pca_w			prescr_solucao.ie_um_limite_hora_pca%type;

cursor c01 is
select	substr(obter_se_material_disponivel(b.cd_material),1,1),
	a.cd_estabelecimento,		
	nvl(obter_local_estoque_setor(a.cd_setor_atendimento,a.cd_estabelecimento),0),
	b.cd_material,
	Obter_se_via_adm_medic(b.cd_material, ie_via_aplicacao_w)
from	prescr_medica a,
	prescr_material b
where	b.nr_prescricao = a.nr_prescricao
and	b.nr_prescricao = nr_prescricao_p
and	b.nr_sequencia_solucao = nr_seq_solucao_w
and	b.ie_agrupador = 4;

TYPE cc00 IS REF CURSOR;
c00 cc00;

begin

if	(nr_prescricao_p > 0) then
	
	if	(nr_seq_solucao_p > 0) then
		delete
		from	prescr_medica_erro
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	= nr_seq_solucao_p
		and	nr_seq_medic	is null;
		
		open c00 for
		select	ie_acm,
			ie_urgencia, 
			ie_se_necessario,
			hr_prim_horario,
			ie_esquema_alternado,
			nr_etapas,
			qt_hora_fase,
			qt_tempo_aplicacao,
			nvl(ie_hemodialise,'N'),
			nvl(ie_solucao_especial,'N'),
			nvl(ie_solucao_pca,'N'),
			nr_seq_dialise,
			ie_via_aplicacao,
			ds_horarios,
			substr(ds_solucao,1,10),
			ie_tipo_analgesia,
			ie_pca_modo_prog,
			qt_vol_infusao_pca,
			qt_bolus_pca,
			qt_intervalo_bloqueio,
			qt_dose_inicial_pca,
			qt_limite_quatro_hora,
			qt_limite_uma_hora,
			nr_seq_solucao,
			nvl(cd_intervalo,'XPTO'),
			dt_suspensao,
			ie_aberto,
			ie_um_dose_inicio_pca,
			ie_um_limite_pca,
			ie_um_limite_hora_pca
		from 	prescr_solucao
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao 	= nr_seq_solucao_p;
	else
		delete
		from	prescr_medica_erro
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	is not null
		and	nr_seq_medic	is null;
	
		open c00 for
		select	ie_acm,
			ie_urgencia, 
			ie_se_necessario,
			hr_prim_horario,
			ie_esquema_alternado,
			nr_etapas,
			qt_hora_fase,
			qt_tempo_aplicacao,
			nvl(ie_hemodialise,'N'),
			nvl(ie_solucao_especial,'N'),
			nvl(ie_solucao_pca,'N'),
			nr_seq_dialise,
			ie_via_aplicacao,
			ds_horarios,
			substr(ds_solucao,1,10),
			ie_tipo_analgesia,
			ie_pca_modo_prog,
			qt_vol_infusao_pca,
			qt_bolus_pca,
			qt_intervalo_bloqueio,
			qt_dose_inicial_pca,
			qt_limite_quatro_hora,
			qt_limite_uma_hora,
			nr_seq_solucao,
			nvl(cd_intervalo,'XPTO'),
			dt_suspensao,
			ie_aberto,
			ie_um_dose_inicio_pca,
			ie_um_limite_pca,
			ie_um_limite_hora_pca
		from 	prescr_solucao
		where	nr_prescricao	= nr_prescricao_p;
	end if;
	
	select	max(cd_estabelecimento),
			max(dt_inicio_prescr),
			max(nr_horas_validade),
			max(dt_validade_prescr),
			max(dt_prescricao)
	into	cd_estab_prescr_w,
			dt_inicio_prescr_w,
			nr_horas_validade_w,
			dt_validade_prescr_w,
			dt_prescricao_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;
	
	select	nvl(max(ie_consiste_setor_mat),'N')
	into	ie_consistir_setor_material_w
	from	parametro_medico
	where	cd_estabelecimento	= cd_estabelecimento_p;	
	
	loop
	fetch c00 into
		ie_acm_w,
		ie_urgencia_w,
		ie_se_necessario_w,
		hr_prim_horario_w,
		ie_esquema_alternado_w,
		nr_etapas_w,
		qt_hora_fase_w,
		qt_tempo_aplicacao_w,
		ie_hemodialise_w,
		ie_solucao_especial_w,
		ie_solucao_pca_w,
		nr_seq_dialise_w,
		ie_via_aplicacao_w,
		ds_horarios_w,
		ds_solucao_w,
		ie_tipo_analgesia_w,
		ie_pca_modo_prog_w,
		qt_vol_infusao_pca_w,
		qt_bolus_pca_w,
		qt_intervalo_bloqueio_w,
		qt_dose_inicial_pca_w,
		qt_limite_quatro_hora_w,
		qt_limite_uma_hora_w,
		nr_seq_solucao_w,
		cd_intervalo_w,
		dt_suspensao_w,
		ie_aberto_w,
		ie_um_dose_inicio_pca_w,
		ie_um_limite_pca_w,
		ie_um_limite_hora_pca_w;
	exit when c00%notfound;

		if	(ie_hemodialise_w	not in ('S','P')) and
			(dt_suspensao_w is null) and
			(wheb_assist_pck.Obter_Se_Consiste_REP(89,cd_perfil_p) = 'S') then
			
			ie_prim_hor_w := 'N';
			if	(nvl(ie_acm_w,'N') = 'N') and
				(nvl(ie_urgencia_w,'N') = 'N') and
				(nvl(ie_se_necessario_w,'N') = 'N') and
				((hr_prim_horario_w is null) or
				 (hr_prim_horario_w = '  :  '))then
				ie_prim_hor_w := 'S';
			end if;
			if	(ie_prim_hor_w = 'S') then
				ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(89, cd_perfil_p);		
				gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 89, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
			end if;		
		end if;
		
		if	(nvl(ie_esquema_alternado_w,'N') = 'S') and
			(wheb_assist_pck.Obter_Se_Consiste_REP(93,cd_perfil_p) = 'S') then
			
			select	nvl(count(*),0)
			into	qt_solucao_w
			from	prescr_material
			where	nr_prescricao		= nr_prescricao_p
			and	ie_agrupador		= 4
			and	ds_dose_diferenciada	is null
			and	nr_sequencia_solucao	= nr_seq_solucao_w
			and	rownum = 1;
		
			if	(qt_solucao_w > 0) then
				ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(93, cd_perfil_p);		
				gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 93, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
			end if;
		end if;
		
		if	(nvl(ie_hemodialise_w,'N') = 'N') and
			(ds_solucao_w is null) and
			(wheb_assist_pck.Obter_Se_Consiste_REP(95,cd_perfil_p) = 'S') then
			ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(95, cd_perfil_p);		
			gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 95, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
		end if;
		
		if	(wheb_assist_pck.Obter_Se_Consiste_REP(96, cd_perfil_p) = 'S') then
		
			open C01;
			loop
			fetch C01 into	
				ie_material_disponivel_w,
				cd_estab_prescr_w,
				cd_local_prescr_w,
				cd_material_w,
				ie_via_medic_w;
			exit when C01%notfound;
				begin
		
				ie_material_prescricao_w	:= obter_se_material_prescricao(cd_estab_prescr_w,cd_material_w);
			
				if	(ie_material_prescricao_w = 'N') then
					ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(96, cd_perfil_p);
					gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 96, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
				end if;
			
				end;
			end loop;
			close C01;
			
		end if;	
		
		if	(wheb_assist_pck.Obter_Se_Consiste_REP(102, cd_perfil_p) = 'S') then
		
			select	count(*)
			into	qt_antimicrobiano_w
			from	material b,
				prescr_material a
			where	a.cd_material		= b.cd_material
			and	a.nr_prescricao		= nr_prescricao_p
			and	a.nr_sequencia_solucao	= nr_seq_solucao_w
			and	b.ie_controle_medico	<> 0
			and	((a.ie_indicacao is null) and
				 (a.cd_microorganismo_cih is null) and
				 (a.cd_amostra_cih is null) and
				 (a.ie_origem_infeccao is null) and
				 (a.cd_topografia_cih is null) and
				 (a.ie_objetivo is null))
			and	a.ie_agrupador		= 4
			and	rownum = 1;
		
			if	(qt_antimicrobiano_w > 0) then
				ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(102, cd_perfil_p);
				gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 102, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
			end if;
		end if;	
		
		if	(wheb_assist_pck.Obter_Se_Consiste_REP(20,cd_perfil_p) = 'S') or
			(wheb_assist_pck.Obter_Se_Consiste_REP(45,cd_perfil_p) = 'S') or
			(wheb_assist_pck.Obter_Se_Consiste_REP(221,cd_perfil_p) = 'S') then
		
			open C01;
			loop
			fetch C01 into	
				ie_material_disponivel_w,
				cd_estab_prescr_w,
				cd_local_prescr_w,
				cd_material_w,
				ie_via_medic_w;
			exit when C01%notfound;
				begin
				if 	(ie_material_disponivel_w is not null) and
					(wheb_assist_pck.Obter_Se_Consiste_REP(20,cd_perfil_p) = 'S') and
					(ie_material_disponivel_w <> 'S') then			
					begin
					
					qt_saldo_disponivel_estoque_w	:= 0;
					if	(ie_material_disponivel_w = 'E') then
						qt_saldo_disponivel_estoque_w	:= obter_saldo_disp_estoque(cd_estab_prescr_w, cd_material_w, cd_local_prescr_w, SYSDATE);		
					end if;
					
					if	(qt_saldo_disponivel_estoque_w <= 0) then
						begin			
						ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(20, cd_perfil_p);		
						gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 20, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);			
						end;
					end if;		
					end;
				end if;		
				end;
				
				ie_erro_liberar_prescr_w	:= '';
		
				if	(ie_consistir_setor_material_w <> 'S') and
					(wheb_assist_pck.Obter_Se_Consiste_REP(45,cd_perfil_p) = 'S') then

					consiste_setor_exclusivo_item(cd_estab_prescr_w, nr_prescricao_p, cd_material_w, ie_setor_regra_w);

					if	(ie_setor_regra_w = 'N') and
						(ie_consistir_setor_material_w = 'CA') then
						ie_erro_liberar_prescr_w	:= 'S';
					elsif	(ie_setor_regra_w = 'N') and
						(ie_consistir_setor_material_w = 'CAI') then
						ie_erro_liberar_prescr_w	:= 'N';
					end if;

					gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 45, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);			

				end if;
				
				ie_erro_liberar_prescr_w	:= '';
		
				if	(ie_via_medic_w = 'N') and
					(wheb_assist_pck.Obter_Se_Consiste_REP(221,cd_perfil_p) = 'S') then
					ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(221, cd_perfil_p);
					gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 221, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
				end if;
				
				
			end loop;
			close C01;
		end if;
		
		if	(nvl(ie_acm_w,'N')		= 'N') and
			(nr_seq_dialise_w is null) and 
			(nvl(ie_se_necessario_w,'N')	= 'N') and
			(nvl(ie_urgencia_w,'N')		= 'N') and
			((nr_etapas_w	is null) or
			 (qt_hora_fase_w is null) or
			 (qt_tempo_aplicacao_w is null)) and
			(nvl(ie_aberto_w, 'N') = 'N') and 
			(wheb_assist_pck.Obter_Se_Consiste_REP(117, cd_perfil_p) = 'S') then
			ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(117, cd_perfil_p);
			gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 117, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
		end if;	
		
		if	(ie_solucao_especial_w	= 'N') and
			(ie_solucao_pca_w	= 'N') and
			(nvl(nr_etapas_w,0) = 0) and
			(wheb_assist_pck.Obter_Se_Consiste_REP(196,cd_perfil_p) = 'S') then
			ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(196, cd_perfil_p);
			gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w, 196, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
		end if;
		
		
		if	(ie_solucao_pca_w = 'S') and
			(ie_solucao_pca_w = 'S') and
			((ie_tipo_analgesia_w is null) or 
			((ie_pca_modo_prog_w <> 'B') and (qt_vol_infusao_pca_w is null) or
			 (ie_pca_modo_prog_w is null)) or
			((qt_bolus_pca_w is null) and (ie_pca_modo_prog_w <> 'C')) or
			(qt_intervalo_bloqueio_w is null) or
			(qt_dose_inicial_pca_w is null) or
			(qt_limite_quatro_hora_w is null) or
			(qt_limite_uma_hora_w is null) or
			(ie_um_dose_inicio_pca_w is null) or
			(ie_um_limite_pca_w is null) or
			(ie_um_limite_hora_pca_w is null)) and
			(wheb_assist_pck.Obter_Se_Consiste_REP(228,cd_perfil_p) = 'S') then
			ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(228, cd_perfil_p);
			gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w,228, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
		end if;	
		
		begin
		if	(length(trim(hr_prim_horario_w)) = 5) then
			dt_prim_horario_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w, hr_prim_horario_w),'mi');
		else
			dt_prim_horario_w	:= dt_inicio_prescr_w;
		end if;
		exception when others then
			dt_prim_horario_w	:= dt_inicio_prescr_w;
		end;

		if	(dt_prim_horario_w	< dt_inicio_prescr_w) then
			dt_prim_horario_w	:= dt_prim_horario_w + 1;
		end if;

		if	(nvl(ie_urgencia_w,'N') <> 'S') and
			(hr_prim_horario_w is not null) and
			(hr_prim_horario_w <> '  :  ') and
			(nr_horas_validade_w	< 24) and
			((dt_prim_horario_w	< dt_inicio_prescr_w) or
			 (dt_prim_horario_w	> dt_validade_prescr_w)) and
			(wheb_assist_pck.Obter_Se_Consiste_REP(230,cd_perfil_p) = 'S') then
			ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(230, cd_perfil_p);
			gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w,230, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
		end if;
		
		if	(nvl(ie_urgencia_w,'N') 	<> 'S') and
			(nvl(ie_se_necessario_w,'N')	= 'N') and
			(nvl(ie_acm_w,'N')		= 'N') and
			(ds_horarios_w			is not null) and
			(wheb_assist_pck.Obter_Se_Consiste_REP(232,cd_perfil_p) = 'S') then
			
			if	(qt_tempo_aplicacao_w > nr_horas_validade_w) then
				qt_horas_validade_w	:= (qt_tempo_aplicacao_w/24);
			else
				qt_horas_validade_w 	:= (nr_horas_validade_w/24);
			end if;
			
			select	decode(count(nr_sequencia),0,'N','S')
			into	ie_fora_validade_w
			from	prescr_mat_hor
			where	nr_prescricao	= nr_prescricao_p
			and	nr_seq_solucao	= nr_seq_solucao_w
			and	nvl(ie_horario_especial,'N')	<> 'S'
			and	nvl(ie_situacao,'A')	= 'A'
			and	dt_suspensao	is null
			and	nr_seq_superior is null
			and	(((NVL(ie_etapa_especial,'N') = 'N') and 
				  ((dt_horario	< dt_inicio_prescr_w) or
				   (dt_horario	> dt_inicio_prescr_w + qt_horas_validade_w))) or
				 ((NVL(ie_etapa_especial,'N') = 'S') and
				  ((dt_horario	< dt_prescricao_w) or
				   (dt_horario	> dt_inicio_prescr_w + qt_horas_validade_w))))
			and	rownum = 1;
			
			if	(ie_fora_validade_w	= 'S') then
				ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(232, cd_perfil_p);
				gerar_erro_prescr_solucao(nr_prescricao_p, nr_seq_solucao_w,232, ie_erro_liberar_prescr_w, null, cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
			end if;
			
		end if;	
		
		if	(wheb_assist_pck.Obter_Se_Consiste_REP(271,cd_perfil_p) = 'S') and
			(nvl(cd_intervalo_w,'XPTO') <> 'XPTO') then
	
			cd_intervaloAcmSn_w := obter_se_interv_agora_acm_sn(cd_intervalo_w);

			if	((nvl(ie_acm_w,'N')   = 'N') and 
				 (cd_intervaloAcmSn_w = 'ACM')) or
				((nvl(ie_se_necessario_w,'N') = 'N') and
				 (cd_intervaloAcmSn_w = 'SN')) then
			
				ie_erro_liberar_prescr_w	:= obter_lib_erro_regra_rep(271, cd_perfil_p);
				if	(cd_intervaloAcmSn_w = 'ACM') then
					gerar_erro_prescr_solucao(nr_prescricao_p,  nr_seq_solucao_w, 271, ie_erro_liberar_prescr_w, wheb_mensagem_pck.get_texto(278924), cd_perfil_p, nm_usuario_p, nr_seq_erro_w);
				else
					gerar_erro_prescr_solucao(nr_prescricao_p,  nr_seq_solucao_w, 271, ie_erro_liberar_prescr_w, wheb_mensagem_pck.get_texto(278925), cd_perfil_p, nm_usuario_p, nr_seq_erro_w);	
				end if;
			 
			end if;
		end if;	
		
		select	count(*)
		into	qt_erro_nao_lib_w
		from	prescr_medica_erro
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	= nr_seq_solucao_w
		and	nr_seq_medic	is null
		and	ie_libera	= 'N'
		and	rownum = 1;
		
		select	count(*)
		into	qt_erro_lib_w
		from	prescr_medica_erro
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	= nr_seq_solucao_w
		and	nr_seq_medic	is null
		and	ie_libera	= 'S'
		and	rownum = 1;		
		
		if	(qt_erro_nao_lib_w > 0) then
		
			update	prescr_solucao
			set	ie_erro			= 125,
				ds_cor_erro		= obter_cor_erro_prescr_sol(nr_prescricao_p, nr_seq_solucao_w, 125, cd_estabelecimento_p, cd_perfil_p)
			where	nr_prescricao		= nr_prescricao_p
			and	nr_seq_solucao		= nr_seq_solucao_w;
			
			ds_erro_p	:= '125';
			
		elsif	(qt_erro_lib_w > 0) then
		
			update	prescr_solucao
			set	ie_erro			= 124,
				ds_cor_erro		= obter_cor_erro_prescr_sol(nr_prescricao_p, nr_seq_solucao_w, 124, cd_estabelecimento_p, cd_perfil_p)
			where	nr_prescricao		= nr_prescricao_p
			and	nr_seq_solucao		= nr_seq_solucao_w;
			
		else
		
			update	prescr_solucao
			set	ie_erro			= 0,
				ds_cor_erro		= null
			where	nr_prescricao		= nr_prescricao_p
			and	nr_seq_solucao		= nr_seq_solucao_w;
		end if;
	end loop;
	close c00;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

if	(nr_seq_solucao_p > 0) then
	select	max(ie_erro)
	into	ds_erro_p
	from	prescr_solucao
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p;
end if;

end Consistir_prescr_solucao;
/