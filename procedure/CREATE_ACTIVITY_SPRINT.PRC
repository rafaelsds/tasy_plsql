CREATE OR REPLACE PROCEDURE create_activity_sprint(nr_seq_cronograma_p proj_cron_etapa.nr_seq_cronograma%type default null,
                            nr_seq_sprint_p CADASTRO_SPRINT_ATIVIDADE.NR_SEQ_CADASTRO_SPRINT%type,
                            dt_inicial_p proj_cron_etapa.dt_inicio_prev%type default null,
                            dt_final_p proj_cron_etapa.dt_fim_prev%type default null,
                            nr_seq_proj_cron_etp_p proj_cron_etapa.nr_sequencia%type default 0,
                            nr_seq_proj_p proj_projeto.nr_sequencia%type default null,
			    ie_tipo_p varchar2 default 'N') IS

ds_atividade_w  proj_cron_etapa.ds_atividade%type;

cursor c01 IS
SELECT 	NR_SEQUENCIA, DS_ATIVIDADE
FROM 	proj_cron_etapa
WHERE 	((nr_seq_cronograma_p is null or (nr_seq_cronograma_p is not null AND nr_seq_cronograma = nr_seq_cronograma_p))
AND 	(nr_seq_proj_p is null or (nr_seq_proj_p is not null 
AND 	nr_seq_cronograma IN (SELECT nr_sequencia FROM proj_cronograma where nr_seq_proj = nr_seq_proj_p))))
AND 	(ie_tipo_p = 'S' AND
		((dt_inicial_p is not null
			AND	dt_final_p is not null
			AND 	dt_inicio_prev between dt_inicial_p and dt_final_p
			AND 	dt_fim_prev between dt_inicial_p and dt_final_p)
			OR 	(dt_inicial_p is null AND dt_final_p is null))
			OR 	ie_tipo_p = 'D' AND
		((dt_inicial_p is not null
			AND 	dt_final_p is not null
			AND 	dt_inicio_prev between dt_inicial_p and dt_final_p
			OR 	dt_fim_prev between dt_inicial_p and dt_final_p)
			OR 	(dt_inicial_p is null AND dt_final_p is null))
			OR 	ie_tipo_p = 'N');

BEGIN

  if (nr_seq_proj_cron_etp_p > 0) then
  
      SELECT ds_atividade
      INTO ds_atividade_w
      FROM proj_cron_etapa
      WHERE nr_sequencia = nr_seq_proj_cron_etp_p;

      INSERT into CADASTRO_SPRINT_ATIVIDADE(
        NR_SEQUENCIA,
        NR_SEQ_PROJ_CRON_ETAPA,
        NR_SEQ_CADASTRO_SPRINT,
        DS_ATIVIDADE,
        DT_ATUALIZACAO,
        NM_USUARIO,
        DT_ATUALIZACAO_NREC,
        NM_USUARIO_NREC
      ) VALUES (
        CADASTRO_SPRINT_ATIVIDADE_SEQ.nextval,
        nr_seq_proj_cron_etp_p,
        nr_seq_sprint_p,
        ds_atividade_w,
        sysdate,
        wheb_usuario_pck.get_nm_usuario,
        sysdate,
        wheb_usuario_pck.get_nm_usuario
      );

  else

  for c01_w in c01 loop 
    BEGIN

      INSERT into CADASTRO_SPRINT_ATIVIDADE(
        NR_SEQUENCIA,
        NR_SEQ_PROJ_CRON_ETAPA,
        NR_SEQ_CADASTRO_SPRINT,
        DS_ATIVIDADE,
        DT_ATUALIZACAO,
        NM_USUARIO,
        DT_ATUALIZACAO_NREC,
        NM_USUARIO_NREC
      ) VALUES (
        CADASTRO_SPRINT_ATIVIDADE_SEQ.nextval,
        c01_w.NR_SEQUENCIA,
        nr_seq_sprint_p,
        c01_w.DS_ATIVIDADE,
        sysdate,
        wheb_usuario_pck.get_nm_usuario,
        sysdate,
        wheb_usuario_pck.get_nm_usuario
      );

    END;
    end loop;
  end if;
  
  COMMIT;
END create_activity_sprint;
/
