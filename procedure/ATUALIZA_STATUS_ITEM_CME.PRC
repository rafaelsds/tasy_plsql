create or replace
procedure atualiza_status_item_cme(	nr_sequencia_p		number,
					nr_seq_item_p		number,
					ie_status_item_p 	varchar2,
					ie_unico_p		varchar2,
					ie_alteracao_p		varchar2 ) is

ie_status_item_w	cm_conjunto_item.ie_status_item%type;		
ie_unico_w		cm_item.ie_unico%type;					

begin

if (ie_alteracao_p = 'I') then

	if (ie_unico_p = 'S') then

		select 	max(ie_status_item)
		into	ie_status_item_w
		from	cm_conjunto_item
		where	nr_seq_item = nr_sequencia_p;
		
		if (nvl(ie_status_item_w,'X') <> nvl(ie_status_item_p,'X')) then
		
			update 	cm_conjunto_item
			set	ie_status_item = ie_status_item_p
			where	nr_seq_item = nr_sequencia_p;

		end if;

	end if;

	commit;

elsif (ie_alteracao_p = 'C') then

	select 	nvl(ie_unico,'N'),
		ie_status_item
	into	ie_unico_w,
		ie_status_item_w
	from 	cm_item
	where	nr_sequencia = nr_seq_item_p;
	
	if (ie_unico_w = 'S' and (nvl(ie_status_item_w,'X') <> nvl(ie_status_item_p,'X'))) then
	
		update 	cm_item
		set	ie_status_item = ie_status_item_p
		where	nr_sequencia = nr_seq_item_p;
	
	end if;
	
	commit;

end if;

end atualiza_status_item_cme;
/