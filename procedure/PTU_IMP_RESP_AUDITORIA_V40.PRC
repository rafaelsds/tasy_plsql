create or replace
procedure ptu_imp_resp_auditoria_v40
			(	ds_arquivo_p			Varchar2,
				cd_estabelecimento_p		Number,
				nm_usuario_p			Varchar2,
				nr_seq_execucao_p	out	Number) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importar resposta auditoria_v40

Rotina utilizada nas transa��es ptu via scs homologadas com a unimed brasil.
quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Performance
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */				
cd_transacao_w			Varchar2(5);
ie_tipo_cliente_w		Varchar2(15);
cd_unimed_executora_w		Number(4);
cd_unimed_beneficiario_w	Number(4);
nr_seq_origem_w			Number(10);
ds_conteudo_w			Varchar2(4000);
dt_atendimento_ww		Varchar2(255);
dt_validade_senha_w		Date;
ds_observacao_w			Varchar2(4000);
ie_tipo_tabela_w		Varchar2(3);
cd_servico_w			Number(8);
ds_servico_w			Varchar2(255);
qt_autorizada_w			Number(10);
ie_autorizado_w			Number(3);
cd_mens_espec_1			Number(4);
cd_mens_espec_2			Number(4);
cd_mens_espec_3			Number(4);
cd_mens_espec_4			Number(4);
cd_mens_espec_5			Number(4);
ie_status_w			Varchar2(2);
nr_seq_auditoria_w		Number(10);
ie_origem_proced_w		Number(3);
nr_seq_import_w			Number(2);
nr_seq_transacao_w		Number(10);
ie_tipo_transacao_w		Varchar2(3);
qt_reg_liberado_w		Number(2);
qt_reg_liberado_ww		Number(2);
qt_reg_negado_w			Number(2);
qt_reg_negado_ww		Number(2);
qt_registro_w			Number(2);
qt_registro_ww			Number(2);
nr_seq_procedimento_w		Number(10);
nr_seq_material_w		Number(10);
ie_tipo_resposta_w		Varchar2(2);
ie_status_ww			Number(1);
nr_seq_pedido_compl_w		Number(10);
nr_seq_pedido_aut_w		Number(10);
nr_seq_requisicao_w		Number(10);
nr_seq_guia_w			Number(10);
ie_status_item_w		Varchar2(255);
ds_estagio_req_w		Varchar2(255);
nm_usuario_item_w		Varchar2(255);
dt_atualizacao_item_w		Date;
cd_material_ops_w		Varchar2(255);
cd_procedimento_w		Number(15);
cd_servico_conversao_w		Number(15);
qt_reg_audit_w			Number(10);
ie_tipo_autorizacao_w		varchar2(2);
nr_seq_req_proc_w		Number(10);
nr_seq_guia_proc_w		Number(10);
nr_seq_req_mat_w		Number(10);
nr_seq_guia_mat_w		Number(10);
nr_seq_ped_aut_compl_serv_w	Number(10);
nr_seq_auditoria_item_w		number(10);
nr_seq_proc_origem_w		number(10);
nr_seq_mat_origem_w		number(10);
ie_status_analise_w		varchar2(255);
nr_seq_regra_ret_orig_w		number(10);
qt_regra_senha_w		number(10);

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;
	
Cursor C02 is
	select	nr_sequencia,
		nr_seq_proc_origem,
		nr_seq_mat_origem
	from	pls_auditoria_item
	where	nr_seq_auditoria	in (	select	nr_sequencia
						from	pls_auditoria
						where	nr_seq_requisicao	= nr_seq_requisicao_w)
	and	ie_status	= 'P'
	order by nr_sequencia;
	
Cursor C03 is
	select	nr_sequencia,
		nr_seq_proc_origem,
		nr_seq_mat_origem
	from	pls_auditoria_item
	where	nr_seq_auditoria	in (	select	nr_sequencia
						from	pls_auditoria
						where	nr_seq_guia	= nr_seq_guia_w)
	and	ie_status	= 'P'
	order by nr_sequencia;

begin

open c01;
loop
fetch c01 into
	nr_seq_import_w,
	ds_conteudo_w;
exit when c01%notfound;
	begin

	if	(substr(ds_conteudo_w,1,4)	<> 'FIM$') then
		if	(nr_seq_import_w	= 1) then
			cd_transacao_w			:= substr(ds_conteudo_w,1,5);
			ie_tipo_cliente_w		:= trim(substr(ds_conteudo_w,6,15));
			cd_unimed_executora_w		:= to_number(substr(ds_conteudo_w,21,4));
			cd_unimed_beneficiario_w	:= to_number(substr(ds_conteudo_w,25,4));
			nr_seq_execucao_p		:= to_number(substr(ds_conteudo_w,29,10));
			nr_seq_origem_w			:= to_number(substr(ds_conteudo_w,39,10));
			dt_atendimento_ww		:= trim(substr(ds_conteudo_w,55,2)||substr(ds_conteudo_w,53,2)||substr(ds_conteudo_w,49,4));
			ie_tipo_autorizacao_w		:= trim(substr(ds_conteudo_w,57,1));
      
			if	(nvl(dt_atendimento_ww,'X')   <> 'X') then
				begin
					dt_validade_senha_w	:= to_date(dt_atendimento_ww,'dd/mm/yyyy');
				exception
				when others then
					dt_validade_senha_w	:= sysdate;
				end;
			end if;

			begin
				select	nvl(nr_seq_pedido_compl,0),
					nvl(nr_seq_pedido_aut,0)
				into	nr_seq_pedido_compl_w,
					nr_seq_pedido_aut_w
				from	ptu_controle_execucao
				where	nr_sequencia	= nr_seq_execucao_p;
			exception
			when others then
				nr_seq_pedido_compl_w	:= 0;
				nr_seq_pedido_aut_w	:= 0;
			end;

			if	(nr_seq_pedido_compl_w	<> 0) then
				select	nr_seq_guia,
					nr_seq_requisicao
				into	nr_seq_guia_w,
					nr_seq_requisicao_w
				from	ptu_pedido_compl_aut
				where	nr_sequencia	= nr_seq_pedido_compl_w;

				ie_tipo_resposta_w	:= 'PC';
			elsif	(nr_seq_pedido_aut_w	<> 0) then
				select	nr_seq_guia,
					nr_seq_requisicao
				into	nr_seq_guia_w,
					nr_seq_requisicao_w
				from	ptu_pedido_autorizacao
				where	nr_sequencia	= nr_seq_pedido_aut_w;

				ie_tipo_resposta_w	:= 'PA';
			elsif	(nr_seq_pedido_compl_w	= 0) and (nr_seq_pedido_aut_w	= 0) then
				nr_seq_execucao_p	:= 0;
				goto final;
			end if;

			if	(cd_transacao_w	= '00404') then
				select	ptu_resposta_auditoria_seq.NextVal
				into	nr_seq_auditoria_w
				from	dual;

				if	(ie_tipo_cliente_w	= 'UNIMED') then
					ie_tipo_cliente_w	:= 'U';
				elsif	(ie_tipo_cliente_w	= 'PORTAL') then
					ie_tipo_cliente_w	:= 'P';
				elsif	(ie_tipo_cliente_w	= 'PRESTADOR') then
					ie_tipo_cliente_w	:= 'R';
				end if;


				insert	into ptu_resposta_auditoria
					(nr_sequencia, cd_transacao, ie_tipo_cliente,
					 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
					 nr_seq_origem, dt_atualizacao, nm_usuario,
					 dt_validade, nr_seq_guia, nr_seq_requisicao,
					 ds_observacao, ds_arquivo_pedido, ie_tipo_autorizacao,
					 nm_usuario_nrec, dt_atualizacao_nrec)
				values	(nr_seq_auditoria_w, cd_transacao_w, ie_tipo_cliente_w,
					 cd_unimed_executora_w, cd_unimed_beneficiario_w, nr_seq_execucao_p,
					 nr_seq_origem_w, sysdate, nm_usuario_p,
					 dt_validade_senha_w, nr_seq_guia_w, nr_seq_requisicao_w,
					 ' ', ds_arquivo_p, ie_tipo_autorizacao_w,
					 nm_usuario_p, sysdate);
			end if;
		elsif	(nr_seq_import_w	= 2) then
			select	trim(substr(ds_conteudo_w,1,255))
			into	ds_observacao_w
			from	dual;

			update	ptu_resposta_auditoria
			set	ds_observacao	= ds_observacao_w
			where	nr_sequencia	= nr_seq_auditoria_w;
		elsif	(substr(ds_conteudo_w,1,1) in ('0','1','2','3', '4'))  then
			select  substr(ds_conteudo_w,1,1),
				decode(to_number(substr(ds_conteudo_w,2,8)),0,'',to_number(substr(ds_conteudo_w,2,8))),
				trim(substr(ds_conteudo_w,10,80)),
				decode(to_number(substr(ds_conteudo_w,90,8)),0,'',to_number(substr(ds_conteudo_w,90,8))),
				decode(to_number(substr(ds_conteudo_w,98,1)),0,'',to_number(substr(ds_conteudo_w,98,1))),
				decode(to_number(substr(ds_conteudo_w,99,4)),0,'',to_number(substr(ds_conteudo_w,99,4))),
				decode(to_number(substr(ds_conteudo_w,103,4)),0,'',to_number(substr(ds_conteudo_w,103,4))),
				decode(to_number(substr(ds_conteudo_w,107,4)),0,'',to_number(substr(ds_conteudo_w,107,4))),
				decode(to_number(substr(ds_conteudo_w,111,4)),0,'',to_number(substr(ds_conteudo_w,111,4))),
				decode(to_number(substr(ds_conteudo_w,115,4)),0,'',to_number(substr(ds_conteudo_w,115,4)))
			into	ie_tipo_tabela_w,
				cd_servico_w,
				ds_servico_w,
				qt_autorizada_w,
				ie_autorizado_w,
				cd_mens_espec_1,
				cd_mens_espec_2,
				cd_mens_espec_3,
				cd_mens_espec_4,
				cd_mens_espec_5
			from 	dual;

			if	(nr_seq_guia_w 		is not null)then
				nr_seq_transacao_w 	:= nr_seq_guia_w;
				ie_tipo_transacao_w	:= 'G';
			elsif	(nr_seq_requisicao_w	is not null) then
				nr_seq_transacao_w	:= nr_seq_requisicao_w;
				ie_tipo_transacao_w	:= 'R';
			end if;

			if	(ie_autorizado_w	= 1) then
				ie_status_w	:= 'N';
				ie_status_ww	:= 2;
			elsif	(ie_autorizado_w	= 2) then
				ie_status_w	:= 'S';
				ie_status_ww	:= 1;
			end if;

			if	(ie_tipo_tabela_w	in('0','1','4')) then
				if	(nvl(nr_seq_pedido_aut_w,0)	<> 0) then
					begin
						select	nr_sequencia,
							nr_seq_req_proc,
							nr_seq_guia_proc
						into	nr_seq_ped_aut_compl_serv_w,
							nr_seq_req_proc_w,
							nr_seq_guia_proc_w
						from	ptu_pedido_aut_servico
						where	nr_seq_pedido					= nr_seq_pedido_aut_w
						and	ie_tipo_tabela					= ie_tipo_tabela_w
						and	nvl(cd_servico_consersao,cd_servico)		= cd_servico_w
						and	((ds_opme	is not null) and (trim(upper(elimina_acentuacao(ds_opme)))	= trim(upper(ds_servico_w)))
						or	(ds_opme	is null))
						and	(nr_seq_req_proc	is not null or nr_seq_guia_proc	is not null);
					exception
					when others then
						nr_seq_ped_aut_compl_serv_w	:= null;
						nr_seq_req_proc_w		:= null;
						nr_seq_guia_proc_w		:= null;
					end;
				elsif	(nvl(nr_seq_pedido_compl_w,0)	<> 0) then
					begin
						select	nr_sequencia,
							nr_seq_req_proc,
							nr_seq_guia_proc
						into	nr_seq_ped_aut_compl_serv_w,
							nr_seq_req_proc_w,
							nr_seq_guia_proc_w
						from	ptu_pedido_compl_aut_serv
						where	nr_seq_pedido					= nr_seq_pedido_compl_w
						and	ie_tipo_tabela					= ie_tipo_tabela_w
						and	nvl(cd_servico_conversao,cd_servico)		= cd_servico_w
						and	((ds_opme	is not null) and (trim(upper(elimina_acentuacao(ds_opme)))	= trim(upper(ds_servico_w)))
						or	(ds_opme	is null))
						and	(nr_seq_req_proc	is not null or nr_seq_guia_proc	is not null);
					exception
					when others then
						nr_seq_ped_aut_compl_serv_w	:= null;
						nr_seq_req_proc_w		:= null;
						nr_seq_guia_proc_w		:= null;
					end;
				end if;

				--cd_servico_conversao_w	:= ptu_obter_servico_conversao(cd_servico_w, nr_seq_pedido_aut_w, nr_seq_ped_aut_compl_serv_w, 'RA');
				nr_seq_procedimento_w	:= nvl(nr_seq_guia_proc_w,nr_seq_req_proc_w);

				if	(nr_seq_guia_w	is not null) then
					update	pls_guia_plano_proc
					set	ie_status	= ie_status_w,
						qt_autorizada	= decode(ie_status_w,'N',0,qt_autorizada_w),
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_sequencia	= nr_seq_guia_proc_w;
				elsif	(nr_seq_requisicao_w	is not null) then
					select	a.ie_status,
						obter_valor_dominio(3431, b.ie_estagio),
						a.nm_usuario,
						a.dt_atualizacao,
						a.cd_procedimento
					into	ie_status_item_w,
						ds_estagio_req_w,
						nm_usuario_item_w,
						dt_atualizacao_item_w,
						cd_procedimento_w
					from	pls_requisicao		b,
						pls_requisicao_proc	a
					where	a.nr_sequencia		= nr_seq_req_proc_w
					and	a.nr_seq_requisicao	= b.nr_sequencia;

					if	(ie_status_item_w	in('I','A')) then
						update	pls_requisicao_proc
						set	ie_status	= ie_status_w,
							qt_procedimento	= decode(ie_status_w,'N',0,qt_autorizada_w),
							dt_atualizacao	= sysdate,
							nm_usuario	= nm_usuario_p
						where	nr_sequencia	= nr_seq_req_proc_w;
					else
						pls_requisicao_gravar_hist(	nr_seq_requisicao_w,'L',substr(cd_procedimento_w||' - Status n�o atualizado com a resposta '||
										' de auditoria da operadora de origem, requisi��o j� '||
										ds_estagio_req_w||' internamente pelo usu�rio '||nm_usuario_item_w||
										' em '||to_char(dt_atualizacao_item_w,'dd/mm/yyyy hh24:mi:ss'),1,4000),null,
										nm_usuario_p);
					end if;
				end if;

				if	(cd_mens_espec_1	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_1,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_procedimento_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_2	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_2,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_procedimento_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_3	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_3,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_procedimento_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_4	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_4,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_procedimento_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_5	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_5,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_procedimento_w, null, null, nm_usuario_p);
				end if;

				select	max(ie_origem_proced)
				into	ie_origem_proced_w
				from    procedimento
				where   cd_procedimento		= cd_servico_w
				and	ie_origem_proced	in(1,4,5)
				and	ie_situacao		= 'A';

				insert	into ptu_resp_auditoria_servico
					(nr_sequencia, ie_tipo_tabela, cd_servico,
					 ie_origem_servico, ds_servico, ie_autorizado,
					 dt_atualizacao, nm_usuario, qt_autorizado,
					 nr_seq_auditoria, cd_servico_conversao, nr_seq_guia_proc,
					 nr_seq_req_proc, nm_usuario_nrec, dt_atualizacao_nrec)
				values	(ptu_resp_auditoria_servico_seq.NextVal, ie_tipo_tabela_w, cd_servico_w,
					 ie_origem_proced_w, nvl(ds_servico_w,'N�o encontrado'), ie_autorizado_w,
					 sysdate, nm_usuario_p, qt_autorizada_w,
					 nr_seq_auditoria_w, cd_servico_conversao_w, nr_seq_guia_proc_w,
					 nr_seq_req_proc_w, nm_usuario_p, sysdate);
			elsif	(ie_tipo_tabela_w	in('2','3')) then
				if	(nvl(nr_seq_pedido_aut_w,0)	<> 0) then
					begin
						select	nr_sequencia,
							nr_seq_req_mat,
							nr_seq_guia_mat
						into	nr_seq_ped_aut_compl_serv_w,
							nr_seq_req_mat_w,
							nr_seq_guia_mat_w
						from	ptu_pedido_aut_servico
						where	nr_seq_pedido					= nr_seq_pedido_aut_w
						and	ie_tipo_tabela					= ie_tipo_tabela_w
						and	nvl(cd_servico_consersao,cd_servico)		= cd_servico_w
						and	((ds_opme	is not null) and (trim(upper(elimina_acentuacao(ds_opme)))	= trim(upper(ds_servico_w)))
						or	(ds_opme	is null))
						and	(nr_seq_req_mat	is not null or nr_seq_guia_mat	is not null);
					exception
					when others then
						nr_seq_ped_aut_compl_serv_w	:= null;
						nr_seq_req_mat_w		:= null;
						nr_seq_guia_mat_w		:= null;
					end;
				elsif	(nvl(nr_seq_pedido_compl_w,0)	<> 0) then
					begin
						select	nr_sequencia,
							nr_seq_req_mat,
							nr_seq_guia_mat
						into	nr_seq_ped_aut_compl_serv_w,
							nr_seq_req_mat_w,
							nr_seq_guia_mat_w
						from	PTU_PEDIDO_COMPL_AUT_SERV
						where	nr_seq_pedido					= nr_seq_pedido_compl_w
						and	ie_tipo_tabela					= ie_tipo_tabela_w
						and	nvl(cd_servico_conversao,cd_servico)		= cd_servico_w
						and	((ds_opme	is not null) and (trim(upper(elimina_acentuacao(ds_opme)))	= trim(upper(ds_servico_w)))
						or	(ds_opme	is null))
						and	(nr_seq_req_mat	is not null or nr_seq_guia_mat	is not null);
					exception
					when others then
						nr_seq_ped_aut_compl_serv_w	:= null;
						nr_seq_req_mat_w		:= null;
						nr_seq_guia_mat_w		:= null;
					end;
				end if;

				--cd_servico_conversao_w	:= ptu_obter_servico_conversao(cd_servico_w, nr_seq_pedido_aut_w, nr_seq_ped_aut_compl_serv_w, 'RA');
				nr_seq_material_w	:= nvl(nr_seq_guia_mat_w,nr_seq_req_mat_w);

				if	(nr_seq_guia_w 	is not null) then
					update	pls_guia_plano_mat
					set	ie_status	= ie_status_w,
						qt_autorizada	= decode(ie_status_w,'N',0,qt_autorizada_w),
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_sequencia	= nr_seq_guia_mat_w;
				elsif	(nr_seq_requisicao_w	is not null) then
					select	a.ie_status,
						obter_valor_dominio(3431, b.ie_estagio),
						a.nm_usuario,
						a.dt_atualizacao,
						pls_obter_seq_codigo_material(nr_seq_material,'')
					into	ie_status_item_w,
						ds_estagio_req_w,
						nm_usuario_item_w,
						dt_atualizacao_item_w,
						cd_material_ops_w
					from	pls_requisicao		b,
						pls_requisicao_mat	a
					where	a.nr_sequencia		= nr_seq_req_mat_w
					and	a.nr_seq_requisicao	= b.nr_sequencia;

					if	(ie_status_item_w	in('I','A')) then
						update	pls_requisicao_mat
						set	ie_status	= ie_status_w,
							qt_material	= decode(ie_status_w,'N',0,qt_autorizada_w),
							dt_atualizacao	= sysdate,
							nm_usuario	= nm_usuario_p
						where	nr_sequencia	= nr_seq_req_mat_w;
					else
						pls_requisicao_gravar_hist(	nr_seq_requisicao_w,'L',substr(cd_material_ops_w||' - Status n�o atualizado com a resposta '
										||'de auditoria da operadora de origem, requisi��o j� '||
										ds_estagio_req_w||' internamente pelo usu�rio '||nm_usuario_item_w||
										' em '||to_char(dt_atualizacao_item_w,'dd/mm/yyyy hh24:mi:ss'),1,4000),null,
										nm_usuario_p);
					end if;
				end if;

				if	(cd_mens_espec_1	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_1,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_material_w, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_2	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_2,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_material_w, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_3	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_3,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_material_w, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_4	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_4,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_material_w, null, nm_usuario_p);
				end if;

				if	(cd_mens_espec_5	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_espec_5,'',cd_estabelecimento_p, nr_seq_transacao_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_material_w, null, nm_usuario_p);
				end if;

				insert	into ptu_resp_auditoria_servico
					(nr_sequencia, ie_tipo_tabela, cd_servico,
					 ie_origem_servico, ds_servico, ie_autorizado,
					 dt_atualizacao, nm_usuario, qt_autorizado,
					 nr_seq_auditoria, cd_servico_conversao, nr_seq_guia_mat,
					 nr_seq_req_mat, nm_usuario_nrec, dt_atualizacao_nrec)
				values	(ptu_resp_auditoria_servico_seq.NextVal, ie_tipo_tabela_w, cd_servico_w,
					 ie_origem_proced_w, nvl(ds_servico_w,'N�o encontrado'), ie_autorizado_w,
					 sysdate, nm_usuario_p, qt_autorizada_w,
					 nr_seq_auditoria_w, cd_servico_conversao_w, nr_seq_guia_mat_w,
					 nr_seq_req_mat_w, nm_usuario_p, sysdate);
			end if;
		end if;
	end if;
	end;
end loop;
close c01;

if	(nr_seq_requisicao_w	is not null) then
	pls_verif_reg_retorno_aud_orig(nr_seq_requisicao_w, nm_usuario_p, nr_seq_regra_ret_orig_w);
end if;

if	(ie_tipo_transacao_w	= 'G') then
	select	count(1)
	into	qt_reg_liberado_w
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_w
	and	ie_status	= 'S';

	select	count(1)
	into	qt_reg_liberado_ww
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_w
	and	ie_status	= 'S';

	qt_registro_w	:= qt_reg_liberado_w + qt_reg_liberado_ww;

	select	count(1)
	into	qt_reg_negado_w
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_w
	and	ie_status	= 'N';

	select	count(1)
	into	qt_reg_negado_ww
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_w
	and	ie_status	= 'N';

	qt_registro_ww	:= qt_reg_negado_w + qt_reg_negado_ww;

	if	(qt_registro_w	> 0) and (qt_registro_ww = 0) then
		select	count(1)
		into	qt_regra_senha_w
		from	pls_aut_gera_senha_campo a,
			pls_aut_regra_gera_senha b
		where	a.nr_seq_regra_gera_senha = b.nr_sequencia
		and	a.ie_tipo_campo = '4'
		and	sysdate >= b.dt_inicio_vigencia
		and	(sysdate <= b.dt_fim_vigencia or b.dt_fim_vigencia is null);
		
		update	pls_guia_plano
		set	ie_estagio		= 6,
			ie_status		= 1,
			dt_autorizacao		= sysdate,
			dt_valid_senha_ext	= dt_validade_senha_w,
			cd_senha_externa	= nr_seq_origem_w,
			dt_validade_senha	= dt_validade_senha_w,
			cd_senha		= decode(qt_regra_senha_w,0,'',nr_seq_guia_w),
			nm_usuario_liberacao	= nm_usuario_p
		where	nr_sequencia		= nr_seq_guia_w;
	elsif	(qt_registro_w	> 0) and (qt_registro_ww > 0) then
		select	count(1)
		into	qt_regra_senha_w
		from	pls_aut_gera_senha_campo a,
			pls_aut_regra_gera_senha b
		where	a.nr_seq_regra_gera_senha = b.nr_sequencia
		and	a.ie_tipo_campo = '4'
		and	sysdate >= b.dt_inicio_vigencia
		and	(sysdate <= b.dt_fim_vigencia or b.dt_fim_vigencia is null);
		
		update	pls_guia_plano
		set	ie_estagio		= 10,
			ie_status		= 1,
			dt_autorizacao		= sysdate,
			dt_valid_senha_ext	= dt_validade_senha_w,
			cd_senha_externa	= nr_seq_origem_w,
			dt_validade_senha	= dt_validade_senha_w,
			cd_senha		= decode(qt_regra_senha_w,0,'',nr_seq_guia_w),
			nm_usuario_liberacao	= nm_usuario_p
		where	nr_sequencia		= nr_seq_guia_w;
	elsif	(qt_registro_w	= 0) and (qt_registro_ww > 0) then
		update	pls_guia_plano
		set	ie_estagio		= 4,
			ie_status		= 3,
			dt_autorizacao		= sysdate,
			nm_usuario_liberacao	= nm_usuario_p
		where	nr_sequencia		= nr_seq_guia_w;
	end if;

	select	count(1)
	into	qt_reg_audit_w
	from	pls_auditoria
	where	nr_seq_guia	= nr_seq_guia_w
	and	dt_liberacao	is null;

	if	(qt_reg_audit_w	> 0) then
		begin
			select	nr_sequencia,
				ie_status
			into	nr_seq_auditoria_w,
				ie_status_analise_w
			from	pls_auditoria
			where	nr_seq_guia	= nr_seq_guia_w
			and	dt_liberacao	is null;
		exception
		when others then
			nr_seq_auditoria_w := 0;
		end;
	
		update	pls_auditoria
		set	dt_liberacao	= sysdate,
			ie_status	= 'F',
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_seq_guia	= nr_seq_guia_w
		and	dt_liberacao	is null;
		
		open C03;
		loop
		fetch C03 into	
			nr_seq_auditoria_item_w,
			nr_seq_proc_origem_w,
			nr_seq_mat_origem_w;
		exit when C03%notfound;
			begin			
			if	(nvl(nr_seq_proc_origem_w,0) > 0) then 
				select	ie_status
				into	ie_status_item_w
				from	pls_guia_plano_proc
				where	nr_sequencia = nr_seq_proc_origem_w;
			elsif	(nvl(nr_seq_mat_origem_w,0) > 0) then
				select	ie_status
				into	ie_status_item_w
				from	pls_guia_plano_mat
				where	nr_sequencia = nr_seq_mat_origem_w;
			else
				ie_status_item_w := 'S';
			end if;
		
			if	(ie_status_item_w in ('S','P')) then
				ie_status_w := 'S';
			elsif	(ie_status_item_w in ('N','G','C')) then
				ie_status_w := 'N';
			end if;
			
			update	pls_auditoria_item
			set	ie_status		= decode(ie_status_w,'S','A','N'),
				ie_status_solicitacao	= decode(ie_status_w,'S','A','N'),
				dt_atualizacao		= sysdate,
				qt_ajuste		= decode(ie_status_w,'N',0,qt_autorizada_w),
				nm_usuario		= nm_usuario_p
			where	nr_sequencia 		= nr_seq_auditoria_item_w;
			end;
		end loop;
		close C03;
		
		if	(nr_seq_auditoria_w > 0) then
			pls_gerar_alerta_evento(1,nr_seq_auditoria_w,null,null,nm_usuario_p); 
		end if;
	end if;
	
	pls_guia_gravar_historico(nr_seq_guia_w,2,substr('Recebida e processada a resposta de auditoria da Unimed '||cd_unimed_beneficiario_w||' com a mensagem: '||chr(10)||ds_observacao_w,1,4000),'',nm_usuario_p);
elsif	(ie_tipo_transacao_w	= 'R') and (nr_seq_regra_ret_orig_w	= 0) then
	select	count(1)
	into	qt_reg_liberado_w
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_w
	and	ie_status	= 'S';

	select	count(1)
	into	qt_reg_liberado_ww
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_w
	and	ie_status	= 'S';

	qt_registro_w	:= qt_reg_liberado_w + qt_reg_liberado_ww;

	select	count(1)
	into	qt_reg_negado_w
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_w
	and	ie_status	= 'N';

	select	count(1)
	into	qt_reg_negado_ww
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_w
	and	ie_status	= 'N';

	qt_registro_ww	:= qt_reg_negado_w + qt_reg_negado_ww;

	if	(qt_registro_w > 0) and (qt_registro_ww > 0) then
		update	pls_requisicao
		set	ie_estagio		= 6,
			dt_valid_senha_ext	= dt_validade_senha_w,
			dt_validade_senha	= dt_validade_senha_w,
			cd_senha_externa	= nr_seq_origem_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_requisicao_w;
	elsif	(qt_registro_ww > 0) and (qt_registro_w = 0) then
		update	pls_requisicao
		set	ie_estagio		= 7,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_requisicao_w;
	elsif	(qt_registro_ww = 0) and (qt_registro_w > 0) then
		update	pls_requisicao
		set	ie_estagio		= 2,
			dt_valid_senha_ext	= dt_validade_senha_w,
			dt_validade_senha	= dt_validade_senha_w,
			cd_senha_externa	= nr_seq_origem_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_requisicao_w;
	end if;

	select	count(1)
	into	qt_reg_audit_w
	from	pls_auditoria
	where	nr_seq_requisicao	= nr_seq_requisicao_w
	and	dt_liberacao		is null;

	if	(qt_reg_audit_w	> 0) then
		begin
			select	nr_sequencia,
				ie_status
			into	nr_seq_auditoria_w,
				ie_status_analise_w
			from	pls_auditoria
			where	nr_seq_requisicao	= nr_seq_requisicao_w
			and	dt_liberacao		is null;
		exception
		when others then
			nr_seq_auditoria_w := 0;
		end;
	
		update	pls_auditoria
		set	dt_liberacao		= sysdate,
			ie_status		= 'F',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_requisicao	= nr_seq_requisicao_w
		and	dt_liberacao		is null;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_auditoria_item_w,
			nr_seq_proc_origem_w,
			nr_seq_mat_origem_w;
		exit when C02%notfound;
			begin			
			if	(nvl(nr_seq_proc_origem_w,0) > 0) then 
				select	ie_status
				into	ie_status_item_w
				from	pls_requisicao_proc
				where	nr_sequencia = nr_seq_proc_origem_w;
			elsif	(nvl(nr_seq_mat_origem_w,0) > 0) then
				select	ie_status
				into	ie_status_item_w
				from	pls_requisicao_mat
				where	nr_sequencia = nr_seq_mat_origem_w;
			else
				ie_status_item_w := 'S';
			end if;
		
			if	(ie_status_item_w in ('S','P')) then
				ie_status_w := 'S';
			elsif	(ie_status_item_w in ('N','G','C')) then
				ie_status_w := 'N';
			end if;
			
			update	pls_auditoria_item
			set	ie_status		= decode(ie_status_w,'S','A','N'),
				ie_status_solicitacao	= decode(ie_status_w,'S','A','N'),
				dt_atualizacao		= sysdate,
				qt_ajuste		= decode(ie_status_w,'N',0,qt_autorizada_w),
				nm_usuario		= nm_usuario_p
			where	nr_sequencia 		= nr_seq_auditoria_item_w;
			end;
		end loop;
		close C02;
		
		if	(nr_seq_auditoria_w > 0) then
			pls_gerar_alerta_evento(1,nr_seq_auditoria_w,null,null,nm_usuario_p); 
		end if;
	end if;
	
	if	(ie_status_analise_w	<> 'A') then
		if	(ie_status_analise_w	= 'AN') then
			update	pls_notificacao_atend
			set	dt_fim_solicitacao	= sysdate,
				ie_status		= 'NE',
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_auditoria	= nr_seq_auditoria_w;
		end if;
		pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L',substr('Alterado status para An�lise devido recebimento de resposta de auditoria da operadora de origem',1,4000),null,nm_usuario_p);
	end if;

	pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L',substr('Recebida e processada a resposta de auditoria da Unimed '||cd_unimed_beneficiario_w||' com a mensagem: '||chr(10)||ds_observacao_w,1,4000),null,nm_usuario_p);
elsif	(ie_tipo_transacao_w	= 'R') and (nr_seq_regra_ret_orig_w	> 0) then
	update	pls_requisicao
	set	dt_valid_senha_ext	= dt_validade_senha_w,
		dt_validade_senha	= dt_validade_senha_w,
		cd_senha_externa	= nr_seq_origem_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_requisicao_w;

	pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L',substr('Recebida e processada a resposta de auditoria da Unimed '||cd_unimed_beneficiario_w||' com a mensagem: '||chr(10)||ds_observacao_w,1,4000),null,nm_usuario_p);
end if;

ptu_gestao_envio_confirmacao(nr_seq_execucao_p, cd_estabelecimento_p, 'RA', nm_usuario_p);

<<final>>
commit;

end ptu_imp_resp_auditoria_v40;
/
