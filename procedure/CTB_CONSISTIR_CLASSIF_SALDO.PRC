create or replace
procedure ctb_consistir_classif_saldo(	nr_seq_mes_ref_p	number,
					cd_estab_p		number,
					nm_usuario_p		varchar2,
					ds_erro_p	out	varchar2) is 

cd_classificacao_w			varchar2(40);
cd_classif_sup_w			varchar2(40);
cd_empresa_w				number(10);
ds_consistencia_w			varchar2(2000);
dt_referencia_w				date;
ie_classif_mudou_w			varchar2(1)	:= 'N';
ie_classif_sup_mudou_w			varchar2(1)	:= 'N';
ie_sem_classif_w			varchar2(1)	:= 'N';
ie_sem_regra_w				varchar2(1)	:= 'N';
nr_nivel_conta_w			number(10);
	
cursor C01 is
select	a.cd_conta_contabil,
	a.cd_classificacao,
	a.cd_classif_sup,
	a.nr_nivel_conta
from	ctb_saldo a
where	a.cd_estabelecimento	= nvl(cd_estab_p, cd_estabelecimento)
and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
group by a.cd_conta_contabil,
	a.cd_classificacao,
	a.cd_classif_sup,
	a.nr_nivel_conta;

vet01	C01%RowType;

begin

ds_erro_p	:= '';
/* Consistir a Classificação das Contas Contábeis */

select	dt_referencia,
	cd_empresa
into	dt_referencia_w,
	cd_empresa_w
from	ctb_mes_ref
where	nr_sequencia	= nr_seq_mes_ref_p;

	

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	cd_classificacao_w	:= ctb_obter_classif_conta(vet01.cd_conta_contabil, null, dt_referencia_w);
	cd_classif_sup_w	:= ctb_obter_classif_conta_sup(cd_classificacao_w, dt_referencia_w, cd_empresa_w);
	
	nr_nivel_conta_w	:= nvl(vet01.nr_nivel_conta, ctb_obter_nivel_classif_conta(vet01.cd_classificacao));
		
	if	(vet01.cd_classificacao is null) and
		(ie_sem_classif_w = 'N') then
		ie_sem_classif_w	:= 'S';
	end if;
	
	if	(vet01.cd_classificacao is not null) and
		(nvl(cd_classificacao_w,'X') = 'X') and
		(ie_sem_regra_w = 'N') then
		ie_sem_regra_w		:= 'S';
	end if;
	
	if	(vet01.cd_classificacao is not null) and
		(cd_classificacao_w is not null) and
		(cd_classificacao_w <> vet01.cd_classificacao) and
		(ie_classif_mudou_w = 'N') then
		ie_classif_mudou_w	:= 'S';
	end if;
	
	if	(cd_classif_sup_w <> vet01.cd_classif_sup) and
		(ie_classif_sup_mudou_w = 'N') then
		ie_classif_sup_mudou_w	:= 'S';
	end if;
		
	end;
end loop;
close C01;

ds_consistencia_w	:= '';

if	(ie_sem_classif_w = 'S') then
	ds_consistencia_w	:= WHEB_MENSAGEM_PCK.get_texto(277747);
end if;

if	(ie_sem_regra_w = 'S') then
	ds_consistencia_w	:= ds_consistencia_w || WHEB_MENSAGEM_PCK.get_texto(277748);
end if;

if	(ie_classif_mudou_w = 'S') then
	ds_consistencia_w	:= ds_consistencia_w || WHEB_MENSAGEM_PCK.get_texto(277749);
end if;

if	(ie_classif_sup_mudou_w = 'S') then
	ds_consistencia_w	:= ds_consistencia_w ||  WHEB_MENSAGEM_PCK.get_texto(277750);
end if;

ds_erro_p	:= substr(ds_consistencia_w,1,255);

end ctb_consistir_classif_saldo;
/