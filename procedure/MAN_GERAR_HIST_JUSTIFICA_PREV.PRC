create or replace
procedure man_gerar_hist_justifica_prev(
				nr_sequencia_p		number,
				ds_historico_p		varchar2,
				nr_seq_motivo_desvio_p	number,
				nm_usuario_p		varchar2) is 


ds_comunicado_w		long;

begin

ds_comunicado_w := ds_historico_p;

insert into man_ordem_serv_tecnico(
		nr_sequencia,
		nr_seq_ordem_serv,
		dt_atualizacao,
		nm_usuario,
		ds_relat_tecnico,
		dt_historico,
		ie_origem,
		nr_seq_tipo,
		dt_liberacao,
		nm_usuario_lib,
		nr_seq_motivo_desvio_prev)
	values(	man_ordem_serv_tecnico_seq.nextval,
		nr_sequencia_p,
		sysdate,
		nm_usuario_p,
		ds_comunicado_w,
		sysdate,
		'I',
		61,
		sysdate,
		nm_usuario_p,
		nr_seq_motivo_desvio_p);
commit;

end man_gerar_hist_justifica_prev;
/
