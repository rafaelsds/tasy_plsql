create or replace
procedure gerar_interf_conta_item(	nr_seq_protocolo_p		number,
				nr_seq_envio_convenio_p	number,
				cd_convenio_p		number,
				nr_atendimento_p		number,
				nr_interno_conta_p      	number,
				cd_cgc_hospital_p		varchar2,
				cd_cgc_convenio_p	varchar2,
				cd_interno_p		varchar2,
		 		ie_excluir_hon_med_p	varchar2,
				ie_excluir_hon_terc_p	varchar2) is

nr_sequencia_w             	number(10);
nr_seq_atepacu_w           	number(10);
nr_seq_proc_pacote_w       	number(10);
nr_seq_item_w              	number(10);
ie_tipo_item_w             	number(1);
cd_item_w                  	number(15);
cd_proc_tuss_w		number(15);
ie_origem_proced_w         	number(10);
cd_item_convenio_w         	varchar2(20);
ds_item_w                  	varchar2(240);
qt_item_w                  	number(15,4);
cd_unidade_medida_w        	varchar2(30);
dt_item_w                  	date;
vl_unitario_item_w         	number(15,4);
vl_total_item_w            	number(15,2);
qt_ch_item_w               	number(10);
vl_unitario_ch_w           	number(15,4);
cd_medico_executor_w       	varchar2(10);
cd_medico_exec_conv_w      	varchar2(20);
nm_medico_executor_w       	varchar2(60);
nr_crm_executor_w          	varchar2(20);
uf_crm_executor_w          	varchar2(2);
nr_cpf_executor_w          	varchar2(11);
cd_funcao_executor_w       	number(3);
nr_porte_anestesico_w      	number(2);
pr_funcao_participante_w   	number(7,4);
pr_via_acesso_w            	number(7,4);
pr_hora_extra_w            	number(7,4);
ie_via_acesso_w            	varchar2(1);
cd_classif_setor_w         	varchar2(2);
cd_setor_atendimento_w     	number(5);
cd_grupo_gasto_w           	varchar2(10);
cd_brasindice_w            	varchar2(16);
cd_procedimento_ref_w      	number(15);
cd_proc_ref_conv_w         	varchar2(20);
ie_responsavel_credito_w   	varchar2(5);
ie_emite_conta_hosp_w      	varchar2(3);
ie_emite_conta_w           	varchar2(3);
ie_total_interf_w		number(2);
vl_honorario_w             	number(15,2);
cd_especialidade_w         	number(5);
cd_funcao_espec_med_w		number(8);
cd_cgc_cpf_resp_cred_w     	varchar2(14);
cd_cgc_prestador_w	varchar2(14);
cd_interno_terc_w          	varchar2(15);
dt_prescricao_w		date;

dt_autorizacao_w		date;
nr_doc_convenio_w	varchar2(20);
nr_seq_autorizacao_w	number(10);
dt_entrada_w		date;
dt_alta_w			date;
dt_entrada_unidade_w	date;
dt_saida_unidade_w	date;
vl_filme_w                 	number(15,2);
nr_prescricao_w            	number(14);

cd_medico_req_w            	varchar2(10);
nr_crm_requisitante_w      	varchar2(20);
cd_medico_reqaut_w         	varchar2(10);
nr_atendimento_w		number(10);

vl_custo_oper_w		number(15,2);
qt_filme_w		number(15,4);
ie_video_w		varchar2(1);
ie_tipo_atendimento_w	number(03,0);
cd_simpro_w		number(15) 	:= 0;
ie_agrup_item_interf_w	varchar2(1);
vl_total_matmed_ipe_w	number(15,2);
vl_total_mat_ipe_w		number(15,2);
vl_total_med_ipe_w		number(15,2);
vl_contraste_int_ipe_w	number(15,2);
vl_ch_ipe_w		number(15,4);
qt_ch_ipe_w		number(5);
pr_faturado_w		number(15,4);

cd_tipo_item_interf_w	number(2);
cd_area_proc_w		number(15);
cd_especialidade_proc_w	number(15);
cd_grupo_proc_w		number(15);
cd_grupo_mat_w		number(3);
cd_subgrupo_mat_w	number(3);
cd_classe_mat_w		number(5);
cd_tipo_procedimento_w	number(3);
cd_senha_guia_w		varchar2(20);
cd_estabelecimento_w	number(05,0);
ie_forma_apresentacao_w	number(2);
nr_seq_proc_princ_w	number(10);
ie_via_acesso_inf_w	varchar2(1);
nr_seq_conta_convenio_w	number(15);
vl_matmed_exame_w	number(15,2);
vl_matmed_exame_acum_w	number(15,2);
ie_participante_w		varchar2(1);
vl_original_w		number(15,2):= 0;
ie_view_honorario_w	varchar2(1):= 'S';

cd_laboratorio_w		varchar2(6);
cd_apresentacao_w	varchar2(6);
cd_medicamento_w		varchar2(6);

qt_item_conv_div_w	number(15,4);
cd_cgc_partic_w		varchar2(20);
nr_seq_bras_preco_w		material_atend_paciente.nr_seq_bras_preco%type;
nr_seq_lote_fornec_w		material_lote_fornec.nr_sequencia%type;
nr_seq_marca_w 			material_lote_fornec.nr_seq_marca%type;
ie_conversao_conv_w	varchar2(10)	:= 'N';

nr_seq_cbo_executor_w	varchar2(10);
sg_cons_prof_exec_w	w_interf_conta_item.sg_cons_prof_exec%type;
cd_cgc_estab_executor_w	w_interf_conta_item.cd_cgc_estab_executor%type;
cd_cns_estab_executor_w	w_interf_conta_item.cd_cns_estab_executor%type;
tx_item_w				w_interf_conta_item.tx_item%type;
tx_material_w 			material_atend_paciente.tx_material%type;
nr_seq_proc_crit_hor_w 	varchar2(10);

cursor c01 is
select	a.nr_sequencia,
	a.ie_proc_mat,
	a.cd_item,
	a.ie_origem_proced,
	nvl(a.cd_item_convenio, b.cd_material),
	nvl(b.ds_material,a.ds_item),
	(a.qt_item * decode(nvl(b.tx_conversao_qtde,0),0,1,b.tx_conversao_qtde)),
	nvl(b.cd_unidade_medida, nvl(z.cd_unidade_convenio, a.ds_unidade)),
	nvl(a.dt_conta, a.dt_item) dt_item,
	a.vl_unitario,
	a.vl_item,
	d.cd_classif_setor,
	a.cd_setor_atendimento,
	b.cd_grupo,
	substr(wheb_mensagem_pck.get_texto(307102),1,255),
	null,
	null,
	a.ie_responsavel_credito,
	a.ie_emite_conta,
	a.nr_seq_proc_pacote,
	a.dt_prescricao,
	a.nr_doc_convenio,
	a.nr_prescricao,
	a.nr_atendimento,
	decode(ie_conversao_conv_w,'S',decode(obter_dados_item_conv(a.cd_convenio, a.cd_categoria, 2, a.cd_item, null, 'N', null, null, null, a.nr_atendimento, null, null, 'M'), 1,4,5), 
		decode(a.tp_item,1,4,5)),
	w.cd_grupo_material,
	w.cd_subgrupo_material,
	w.cd_classe_material,
	a.nr_seq_atepacu,
	a.nr_seq_proc_princ,
	a.cd_cgc_prestador,
	dividir(a.qt_item,decode(nvl(b.tx_conversao_qtde,0),0,1,b.tx_conversao_qtde))
from	estrutura_material_v w,
	conversao_unidade_medida z,
	setor_atendimento d,
	mat_atend_pac_convenio b,
	conta_paciente_v a
where	a.nr_interno_conta		= nr_interno_conta_p
and 	a.nr_seq_proc_pacote is null
and	a.cd_motivo_exc_conta is null
and	a.ie_proc_mat			= 2
and	a.nr_sequencia		= b.nr_seq_material(+)
and	a.cd_setor_atendimento	= d.cd_setor_atendimento
and	a.cd_convenio			= z.cd_convenio(+)
and	a.ds_unidade			= z.cd_unidade_medida(+)
and	a.cd_item			= w.cd_material(+)
order by a.nr_sequencia;

cursor c02 is
select	e.nr_sequencia seq,
	1,
	e.cd_procedimento,
	nvl(e.cd_procedimento_tuss, e.cd_procedimento),
	e.ie_origem_proced,
	nvl(a.cd_procedimento,nvl(e.cd_procedimento_convenio, e.cd_procedimento)), /* edgar 21/10/2004, I put the second nvl */
	nvl(a.ds_procedimento,e.ds_procedimento),
	round(e.qt_procedimento *
	decode(nvl(a.tx_conversao_qtde,0),0,1,
	a.tx_conversao_qtde),4),
	nvl(a.cd_unidade_medida,'Un'),
	e.dt_conta dt_item,
	round(decode(e.ie_participante,'S',e.vl_procedimento,
		decode(e.ie_responsavel_credito,'M',e.vl_medico,e.vl_procedimento)) /
		decode(nvl(e.qt_procedimento,1),0,1,e.qt_procedimento),4),
	decode(e.ie_participante,'S',e.vl_procedimento,
		decode(e.ie_responsavel_credito,'M',e.vl_medico,e.vl_procedimento)),
		nvl(e.qt_pto_medico,0),
			round(nvl(e.vl_medico,0) / decode(nvl(e.qt_pto_medico,1),
			0,1,1,1,e.qt_pto_medico),4),
	e.cd_medico,
	nvl(e.cd_medico_convenio,substr(obter_medico_convenio(e.cd_estabelecimento,e.cd_medico,e.cd_convenio, null, null, null,
	e.cd_setor_atendimento,e.dt_procedimento, null, e.ie_funcao_medico, null),1,20)),
	b.nm_pessoa_fisica,
	substr(obter_crm_medico(c.cd_pessoa_fisica), 1, 255) nr_crm,
	substr(c.uf_crm,1,2) uf_crm,
	b.nr_cpf,
	e.ie_funcao_medico,
	e.qt_porte_anestesico,
	e.tx_procedimento,
	e.pr_via_acesso,
	e.tx_hora_extra,
	decode(p.ie_classificacao, 1,
		 decode(e.nr_cirurgia,null,' ',
		 	decode(e.ie_via_acesso, null,
				decode(nvl(e.pr_via_acesso,100),100,'M','D'),ie_via_acesso)), ' '),
	d.cd_classif_setor,
	e.cd_setor_atendimento,
	a.cd_grupo,
	null,
	e.cd_proc_ref,
	e.cd_proc_ref_conv,
	e.ie_responsavel_credito,
	e.ie_emite_conta,
	e.ie_emite_conta_hosp,
	e.vl_medico,
	e.nr_seq_proc_pacote,
	e.cd_especialidade,
	e.cd_funcao_espec_med,
	e.cd_cgc_cpf_resp_cred,
	e.cd_cgc_prestador,
	e.dt_prescricao,
	e.nr_doc_convenio,
	e.nr_seq_autorizacao,
	e.vl_filme,
	e.nr_prescricao,
	e.nr_atendimento,
	e.ie_video,
	e.vl_custo_operacional,
	decode(p.ie_classificacao,'1',1,'2',2,'3',3),
	obter_area_procedimento(p.cd_procedimento,p.ie_origem_proced) cd_area_procedimento,
	obter_especialidade_proced(p.cd_procedimento,p.ie_origem_proced) cd_especialidade,
	obter_grupo_procedimento(p.cd_procedimento,p.ie_origem_proced,'C') cd_grupo_proc,
	p.cd_tipo_procedimento,
	e.nr_seq_atepacu,
	p.ie_forma_apresentacao,
 	e.ie_via_acesso,
	e.ie_participante,
	e.pr_faturado,
	e.nr_seq_proc_princ,
	round(dividir(e.qt_procedimento,
	decode(nvl(a.tx_conversao_qtde,0),0,1,
	a.tx_conversao_qtde)),4),
	e.cd_cgc_partic,
	tiss_obter_cbos_medico(e.cd_medico, e.cd_especialidade, tiss_obter_versao(cd_convenio_p, cd_estabelecimento_w, e.dt_conta), cd_convenio_p) nr_seq_cbo_executor,
	Obter_Conselho_Profissional(b.nr_seq_conselho, 'S') sg_cons_prof_exec,
	obter_cgc_estabelecimento(e.cd_estabelecimento)	cd_cgc_estab_executor,
	obter_cnes_estab_cnpj(obter_cgc_estabelecimento(e.cd_estabelecimento)) cd_cns_estab_executor,
	decode(e.nr_seq_proc_crit_hor,null,'N','S')
from	procedimento p,
	setor_atendimento d,
	medico c,
	pessoa_fisica b,
	proc_paciente_convenio a,
	conta_paciente_honorario_v e
where	e.nr_interno_conta		= nr_interno_conta_p
and	e.nr_sequencia		= a.nr_seq_procedimento(+)
and	e.cd_medico			= b.cd_pessoa_fisica(+)
and	e.cd_medico			= c.cd_pessoa_fisica(+)
and	e.cd_setor_atendimento	= d.cd_setor_atendimento
and	e.cd_procedimento		= p.cd_procedimento
and	e.ie_origem_proced		= p.ie_origem_proced
and	e.cd_motivo_exc_conta is null
and 	nvl(ie_view_honorario_w,'S') = 'S'
and 	((e.nr_seq_proc_pacote is null) or (e.nr_seq_proc_pacote = e.nr_sequencia))
and	((ie_excluir_hon_med_p	= 'N') 		 or
	 (e.ie_responsavel_credito 	<> 'M')		 or
	 (e.ie_responsavel_credito	is null))
and	((ie_excluir_hon_terc_p	= 'N') 	or
	 (e.ie_responsavel_credito 	in ('H','RM','M')) or
	 (e.ie_responsavel_credito	is null))
and	obter_se_exp_funcao_medico(e.ie_funcao_medico,e.cd_convenio,e.cd_estabelecimento) = 'S'	 	 
union all
select	e.nr_sequencia seq,
	1,
	e.cd_procedimento,
	e.cd_procedimento,
	e.ie_origem_proced,
	nvl(a.cd_procedimento,nvl(e.cd_procedimento_convenio, e.cd_procedimento)), /* edgar 21/10/2004, I put the second nvl */
	nvl(a.ds_procedimento,e.ds_procedimento),
	round(e.qt_procedimento *
	decode(nvl(a.tx_conversao_qtde,0),0,1,
	a.tx_conversao_qtde),4),
	nvl(a.cd_unidade_medida,'Un'),
	e.dt_conta dt_item,
	round(decode(e.ie_participante,'S',e.vl_procedimento,
		decode(e.ie_responsavel_credito,'M',e.vl_medico,e.vl_procedimento)) /
		decode(nvl(e.qt_procedimento,1),0,1,e.qt_procedimento),4),
	decode(e.ie_participante,'S',e.vl_procedimento,
		decode(e.ie_responsavel_credito,'M',e.vl_medico,e.vl_procedimento)),
		nvl(e.qt_pto_medico,0),
			round(nvl(e.vl_medico,0) / decode(nvl(e.qt_pto_medico,1),
			0,1,1,1,e.qt_pto_medico),4),
	e.cd_medico,
	nvl(e.cd_medico_convenio,substr(obter_medico_convenio(e.cd_estabelecimento,e.cd_medico,e.cd_convenio, null, null, null,
	e.cd_setor_atendimento,e.dt_procedimento, null, e.ie_funcao_medico, null),1,20)),
	b.nm_pessoa_fisica,
	substr(obter_crm_medico(c.cd_pessoa_fisica), 1, 255) nr_crm,
	substr(c.uf_crm,1,2) uf_crm,
	b.nr_cpf,
	e.ie_funcao_medico,
	e.qt_porte_anestesico,
	e.tx_procedimento,
	e.pr_via_acesso,
	e.tx_hora_extra,
	decode(p.ie_classificacao, 1,
		 decode(e.nr_cirurgia,null,' ',
		 	decode(e.ie_via_acesso, null,
				decode(nvl(e.pr_via_acesso,100),100,'M','D'),ie_via_acesso)), ' '),
	d.cd_classif_setor,
	e.cd_setor_atendimento,
	a.cd_grupo,
	null,
	e.cd_proc_ref,
	e.cd_proc_ref_conv,
	e.ie_responsavel_credito,
	e.ie_emite_conta,
	e.ie_emite_conta_hosp,
	e.vl_medico,
	e.nr_seq_proc_pacote,
	e.cd_especialidade,
	e.cd_funcao_espec_med,
	e.cd_cgc_cpf_resp_cred,
	e.cd_cgc_prestador,
	e.dt_prescricao,
	e.nr_doc_convenio,
	e.nr_seq_autorizacao,
	e.vl_filme,
	e.nr_prescricao,
	e.nr_atendimento,
	e.ie_video,
	e.vl_custo_operacional,
	decode(p.ie_classificacao,'1',1,'2',2,'3',3),
	obter_area_procedimento(p.cd_procedimento,p.ie_origem_proced) cd_area_procedimento,
	obter_especialidade_proced(p.cd_procedimento,p.ie_origem_proced) cd_especialidade,
	obter_grupo_procedimento(p.cd_procedimento,p.ie_origem_proced,'C') cd_grupo_proc,
	p.cd_tipo_procedimento,
	e.nr_seq_atepacu,
	p.ie_forma_apresentacao,
 	e.ie_via_acesso,
	e.ie_participante,
	e.pr_faturado,
	e.nr_seq_proc_princ,
	round(dividir(e.qt_procedimento,
	decode(nvl(a.tx_conversao_qtde,0),0,1,
	a.tx_conversao_qtde)),4),
	null cd_cgc_partic,
	tiss_obter_cbos_medico(e.cd_medico, e.cd_especialidade, tiss_obter_versao(cd_convenio_p, cd_estabelecimento_w, e.dt_conta), cd_convenio_p) nr_seq_cbo_executor,
	Obter_Conselho_Profissional(b.nr_seq_conselho, 'S') sg_cons_prof_exec,
	obter_cgc_estabelecimento(e.cd_estabelecimento)	cd_cgc_estab_executor,
	obter_cnes_estab_cnpj(obter_cgc_estabelecimento(e.cd_estabelecimento)) cd_cns_estab_executor,
	decode(e.nr_seq_proc_crit_hor,null,'N','S')
from	procedimento p,
	setor_atendimento d,
	medico c,
	pessoa_fisica b,
	proc_paciente_convenio a,
	conta_paciente_honorario_v2 e
where	e.nr_interno_conta		= nr_interno_conta_p
and	e.nr_sequencia		= a.nr_seq_procedimento(+)
and	e.cd_medico			= b.cd_pessoa_fisica(+)
and	e.cd_medico			= c.cd_pessoa_fisica(+)
and	e.cd_setor_atendimento	= d.cd_setor_atendimento
and	e.cd_procedimento		= p.cd_procedimento
and	e.ie_origem_proced		= p.ie_origem_proced
and	e.cd_motivo_exc_conta is null
and 	nvl(ie_view_honorario_w,'S') = 'N'
and 	((e.nr_seq_proc_pacote is null) or (e.nr_seq_proc_pacote = e.nr_sequencia))
and	((ie_excluir_hon_med_p	= 'N') 		 or
	 (e.ie_responsavel_credito 	<> 'M')		 or
	 (e.ie_responsavel_credito	is null))
and	((ie_excluir_hon_terc_p	= 'N') 	or
	 (e.ie_responsavel_credito 	in ('H','RM','M')) or
	 (e.ie_responsavel_credito	is null))
and	obter_se_exp_funcao_medico(e.ie_funcao_medico,e.cd_convenio,e.cd_estabelecimento) = 'S'	 
order by seq;


begin

nr_doc_convenio_w	:= null;

/* insurance data */
select	ie_agrup_item_interf
into	ie_agrup_item_interf_w
from	convenio
where	cd_convenio = cd_convenio_p;

cd_medico_req_w	:= null;
cd_medico_reqaut_w	:= null;

select	max(nr_conta_convenio)
into	nr_seq_conta_convenio_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;


/* get the patient's entry date */
begin
select	a.dt_entrada,
	a.ie_tipo_atendimento,
	nvl(a.dt_alta,b.dt_periodo_final),
	a.cd_estabelecimento
into	dt_entrada_w,
	ie_tipo_atendimento_w,
	dt_alta_w,
	cd_estabelecimento_w
from	atendimento_paciente a,
	conta_paciente b
where	b.nr_interno_conta	= nr_interno_conta_p
and	b.nr_atendimento 	= a.nr_atendimento;
end;

-- OS 164800, the field did not put on the screen, because only this customer will use this new view Unimed CHU
-- 'S'  =  Conta_paciente_honorario_v
-- 'N' =  Conta_paciente_honorario_v2
select 	nvl(max(ie_view_honorario),'S')
into	ie_view_honorario_w
from 	parametro_faturamento
where 	cd_estabelecimento = cd_estabelecimento_w;

/* get data of atend_categoria_convenio */
select	max(nvl(cd_senha,nr_doc_convenio))
into	cd_senha_guia_w
from	atend_categoria_convenio
where	nr_atendimento		= nr_atendimento_p
and	dt_inicio_vigencia	=
	 	(select	max(x.dt_inicio_vigencia)
	 	from	atend_categoria_convenio x
	 	where	x.nr_atendimento = nr_atendimento_p);

if	(ie_agrup_item_interf_w in ('E','G'))	and
	(ie_tipo_atendimento_w = 7)	then
	begin
	/* get the value of ch */
	select	nvl(max(vl_ch_honorarios),1)
	into	vl_ch_ipe_w
	from	convenio_amb
	where	cd_convenio = cd_convenio_p;
	end;
end if;

/* OS 1679402 - Remove the fixed CNPJ that was here in the routine */
select	nvl(max(ie_conversao_conv_item),'N')
into	ie_conversao_conv_w
from	convenio_estabelecimento
where	cd_convenio		= cd_convenio_p
and	cd_estabelecimento	= cd_estabelecimento_w;

ie_video_w			:='N';
qt_filme_w			:= 0;
vl_custo_oper_w		:= 0;
vl_total_matmed_ipe_w	:= 0;
vl_total_mat_ipe_w		:= 0;
vl_total_med_ipe_w		:= 0;
vl_contraste_int_ipe_w	:= 0;

/* treat materials  */
open c01;
loop
fetch c01 	into
		nr_seq_item_w,
		ie_tipo_item_w,
		cd_item_w,
		ie_origem_proced_w,
		cd_item_convenio_w,
		ds_item_w,
		qt_item_w,
		cd_unidade_medida_w,
		dt_item_w,
		vl_unitario_item_w,
		vl_total_item_w,
		cd_classif_setor_w,
		cd_setor_atendimento_w,
		cd_grupo_gasto_w,
		cd_brasindice_w,
		cd_procedimento_ref_w,
		cd_proc_ref_conv_w,
		ie_responsavel_credito_w,
		ie_emite_conta_w,
		nr_seq_proc_pacote_w,
		dt_prescricao_w,
		nr_doc_convenio_w,
		nr_prescricao_w,
		nr_atendimento_w,
		cd_tipo_item_interf_w,
		cd_grupo_mat_w,
		cd_subgrupo_mat_w,
		cd_classe_mat_w,
		nr_seq_atepacu_w,
		nr_seq_proc_princ_w,
		cd_cgc_prestador_w,
		qt_item_conv_div_w;
exit when 	c01%notfound;
     		begin
		/* search data of atend_paciente_unidade */
		select	nvl(max(dt_entrada_unidade),dt_entrada_w),
			nvl(max(dt_saida_unidade),dt_alta_w)
		into	dt_entrada_unidade_w,
			dt_saida_unidade_w
		from	atend_paciente_unidade
		where	nr_seq_interno	= nr_seq_atepacu_w;

		/* treat ipe - generic code 8001 of medicines use unite 'Un' */
		if	(ie_agrup_item_interf_w in ('E','G'))	and
			(cd_item_convenio_w	= '8001')	then
			begin
			cd_unidade_medida_w	:= 'Un';
			cd_tipo_item_interf_w	:= 5;
			end;
		end if;

		/* treat ipe - the matmed of exams generate in a single code in ch */
		if	(ie_agrup_item_interf_w in ('E','G'))	and
			(ie_tipo_atendimento_w in (7,3)) 	then
			begin
			if	(ie_tipo_atendimento_w = 7) then
				vl_total_matmed_ipe_w := vl_total_matmed_ipe_w + vl_total_item_w;
			end if;
			if	(ie_tipo_atendimento_w = 3) and
				(cd_tipo_item_interf_w = 4) then
				vl_total_mat_ipe_w := vl_total_mat_ipe_w + vl_total_item_w;
			end if;
			if	(ie_tipo_atendimento_w = 3) and
				(cd_tipo_item_interf_w = 5) then
				vl_total_med_ipe_w := vl_total_med_ipe_w + vl_total_item_w;
			end if;
			end;
		else
			begin
			/* get date of authorization */
			begin
			select	nvl(dt_autorizacao,dt_entrada_w)
			into	dt_autorizacao_w
			from	autorizacao_convenio
			where	nr_atendimento	= nr_atendimento_p
			and	cd_autorizacao	= nr_doc_convenio_w;
			exception
				when others then
			 	dt_autorizacao_w	:= dt_entrada_w;
			end;


			/* get sequence */
			select	w_interf_conta_item_seq.nextval
			into	nr_sequencia_w
			from 	dual;

			/* code brasindice  removed by marcus/edilson in 17/12/2003
			begin
			select	(cd_laboratorio||cd_medicamento||cd_apresentacao)
			into	cd_brasindice_w
			from	material_brasindice
			where	cd_item_w		= cd_material;
			exception
				when others then
				cd_brasindice_w	:= '';
	 		end;
			*/
			select 	max(cd_laboratorio),
				max(cd_apresentacao),
				max(cd_medicamento),
				max(nr_seq_bras_preco),
				max(nr_seq_lote_fornec),
				max(tx_material)
			into	cd_laboratorio_w,
				cd_apresentacao_w,
				cd_medicamento_w,
				nr_seq_bras_preco_w,
				nr_seq_lote_fornec_w,
				tx_material_w
			from 	material_atend_paciente
			where 	nr_sequencia = nr_seq_item_w;
			
			nr_seq_marca_w := 0;
			if	(nvl(nr_seq_lote_fornec_w,0) > 0) then
				select	max(nvl(nr_seq_marca,0))
				into	nr_seq_marca_w
				from	material_lote_fornec
				where	nr_sequencia = nr_seq_lote_fornec_w;
			end if;


			-- Fabricio and Anderson Heckmann in 28/10/2009 OS 163217
			if	((cd_laboratorio_w is null) and
				 (cd_apresentacao_w is null) and
				 (cd_medicamento_w is null)) then
				 
				if	(nr_seq_bras_preco_w is not null) then
				
					select	nvl(obter_dados_brasindice_seq(nr_seq_bras_preco_w, 'CLAB'),'') cd_laboratorio,
						nvl(obter_dados_brasindice_seq(nr_seq_bras_preco_w, 'CMED'),'') cd_medicamento,
						nvl(obter_dados_brasindice_seq(nr_seq_bras_preco_w, 'CAPR'),'') cd_apresentacao
					into	cd_laboratorio_w,
						cd_medicamento_w,
						cd_apresentacao_w
					from	dual;
					
					cd_brasindice_w	:= cd_laboratorio_w || ';' || cd_medicamento_w || ';' || cd_apresentacao_w;
					cd_brasindice_w	:= replace(cd_brasindice_w,';','');
					
				else

					select	obter_codigo_brasindice(cd_estabelecimento_w, cd_item_w, dt_item_w, cd_convenio_p, nr_seq_marca_w)
					into	cd_brasindice_w
					from	dual;
					cd_brasindice_w		:= replace(cd_brasindice_w,';','');
					
				end if;

			else

				cd_brasindice_w	:= cd_laboratorio_w || ';' || cd_medicamento_w || ';' || cd_apresentacao_w;
				cd_brasindice_w	:= replace(cd_brasindice_w,';','');

			end if;

			/* code simpro */
			select	obter_codigo_simpro(cd_item_w)
			into	cd_simpro_w
			from	dual;

			/* indicator interface totals */
			begin
			select	ie_total_interf
			into	ie_total_interf_w
			from	conta_paciente_estrutura
			where	cd_estrutura	= ie_emite_conta_w;
			exception
				when others then
				ie_total_interf_w	:= null;
	 		end;

			/* save materials */
			insert	into w_interf_conta_item(
	 			nr_sequencia,
				cd_tipo_registro,
				nr_seq_registro,
				nr_seq_interface,
				nr_remessa,
	 			nr_atendimento,
				nr_interno_conta,
				nr_seq_item,
				ie_tipo_item,
				cd_item,
	 			ie_origem_proced,
				cd_item_convenio,
				ds_item,
				qt_item,
				cd_unidade_medida,
	 			dt_item,
				vl_unitario_item,
				vl_total_item,
				cd_classif_setor,
				cd_setor_atendimento,
	 			cd_grupo_gasto,
				cd_brasindice,
				cd_procedimento_ref,
				cd_proc_ref_conv,
	 			nr_seq_protocolo,
				cd_convenio,
				cd_cgc_hospital,
				cd_cgc_convenio,
				cd_interno,
				ie_responsavel_credito,
				ie_emite_conta,
				ie_total_interf,
				nr_seq_proc_pacote,
	 			cd_especialidade,
				cd_funcao_espec_med,
				dt_prescricao,
				dt_autorizacao,
				vl_filme,
				nr_doc_convenio,
				nr_prescricao,
				cd_medico_req,
				nr_crm_requisitante,
				cd_simpro,
				ie_via_acesso,
				cd_tipo_item_interf,
				cd_grupo_mat,
				cd_subgrupo_mat,
				cd_classe_mat,
				cd_senha_guia,
				pr_hora_extra,
				dt_entrada_unidade,
				dt_saida_unidade,
				nr_seq_proc_princ,
				nr_seq_conta_convenio,
				cd_cgc_prestador,
				vl_original,
				qt_item_conv_div,
				tx_item)
			values	(
	 			nr_sequencia_w,
				4,
				1,
				1,
				nr_seq_envio_convenio_p,
	 			nr_atendimento_p,
				nr_interno_conta_p,
				nr_seq_item_w,
				ie_tipo_item_w,
				cd_item_w,
				ie_origem_proced_w,
				cd_item_convenio_w,
				substr(ds_item_w,1,50),
				qt_item_w,
				cd_unidade_medida_w,
				dt_item_w,
				vl_unitario_item_w,
				vl_total_item_w,
				cd_classif_setor_w,
				cd_setor_atendimento_w,
				cd_grupo_gasto_w,
				cd_brasindice_w,
				cd_procedimento_ref_w,
				cd_proc_ref_conv_w,
	 			nr_seq_protocolo_p,
				cd_convenio_p,
				cd_cgc_hospital_p,
				cd_cgc_convenio_p,
				cd_interno_p,
				ie_responsavel_credito_w,
				ie_emite_conta_w,
				ie_total_interf_w,
				nr_seq_proc_pacote_w,
				null,
				null,
				dt_prescricao_w,
				dt_autorizacao_w,
				0,
				nr_doc_convenio_w,
				nr_prescricao_w,
				null,
				null,
				cd_simpro_w,
				' ',
				cd_tipo_item_interf_w,
				cd_grupo_mat_w,
				cd_subgrupo_mat_w,
				cd_classe_mat_w,
				cd_senha_guia_w,
				1,
				dt_entrada_unidade_w,dt_saida_unidade_w,nr_seq_proc_princ_w,
				nr_seq_conta_convenio_w,cd_cgc_prestador_w, vl_total_item_w,
				qt_item_conv_div_w,
				tx_material_w);
			end;
		end if;
		end;
end loop;
close c01;

/* treat ipe - the materials of pa generate in a single code 98007963 in reais */
if	(ie_agrup_item_interf_w in ('E','G'))	and
	(ie_tipo_atendimento_w = 3)	and
	(vl_total_mat_ipe_w > 0)	then
	begin
	/* get sequence */
	select w_interf_conta_item_seq.nextval
	into	 nr_sequencia_w
	from 	 dual;

	/* include registration ipe */
	insert into w_interf_conta_item(
		nr_sequencia,
		cd_tipo_registro,
		nr_seq_registro,
		nr_seq_interface,
		nr_remessa,
		nr_atendimento,
		nr_interno_conta,
		nr_seq_item,
		ie_tipo_item,
		cd_item,
		ie_origem_proced,
		cd_item_convenio,
		ds_item,
		qt_item,
		cd_unidade_medida,
	 	dt_item,
		vl_unitario_item,
		vl_total_item,
		cd_classif_setor,
		cd_setor_atendimento,
	 	cd_grupo_gasto,
		cd_brasindice,
		cd_procedimento_ref,
		cd_proc_ref_conv,
	 	nr_seq_protocolo,
		cd_convenio,
		cd_cgc_hospital,
		cd_cgc_convenio,
	 	cd_interno,
		ie_responsavel_credito,
		ie_emite_conta,
		ie_total_interf,
		nr_seq_proc_pacote,
		cd_especialidade,
		cd_funcao_espec_med,
		dt_prescricao,
		dt_autorizacao,
		vl_filme,
		nr_doc_convenio,
		nr_prescricao,
		cd_medico_req,
		nr_crm_requisitante,
		cd_simpro,
		cd_senha_guia,
		dt_entrada_unidade,
		dt_saida_unidade,
		cd_tipo_item_interf,
		nr_seq_proc_princ,
		nr_seq_conta_convenio,
		cd_cgc_prestador,
		vl_original)
	values	(nr_sequencia_w,
		4,
		1,
		1,
		nr_seq_envio_convenio_p,
	 	nr_atendimento_p,
		nr_interno_conta_p,
		1,
		2,
		98007963,
		0,
		'98007963',
		substr(wheb_mensagem_pck.get_texto(307101),1,255),
		1,
		null,
		dt_entrada_w,
		vl_total_mat_ipe_w,
		vl_total_mat_ipe_w,
		null,
		null,
		null,
		null,
		null,
		null,
		nr_seq_protocolo_p,
		cd_convenio_p,
		cd_cgc_hospital_p,
		cd_cgc_convenio_p,
	 	cd_interno_p,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		0,
		nr_doc_convenio_w,
		null,
		null,
		null,
		null,
		cd_senha_guia_w,
		dt_entrada_unidade_w,
		dt_saida_unidade_w,
		4,
		0,
		nr_seq_conta_convenio_w,
		cd_cgc_hospital_p,
		vl_total_mat_ipe_w);
	end;
end if;

/* treat ipe - the medicines of pa generate in a single code 98007530 in reais */
if	(ie_agrup_item_interf_w in ('E','G'))	and
	(ie_tipo_atendimento_w = 3)	and
	(vl_total_med_ipe_w > 0)	then
	begin
	/* get sequence */
	select w_interf_conta_item_seq.nextval
	into	 nr_sequencia_w
	from 	 dual;

	/* include registration ipe */
	insert into w_interf_conta_item(
		nr_sequencia,
		cd_tipo_registro,
		nr_seq_registro,
		nr_seq_interface,
		nr_remessa,
		nr_atendimento,
		nr_interno_conta,
		nr_seq_item,
		ie_tipo_item,
		cd_item,
		ie_origem_proced,
		cd_item_convenio,
		ds_item,
		qt_item,
		cd_unidade_medida,
	 	dt_item,
		vl_unitario_item,
		vl_total_item,
		cd_classif_setor,
		cd_setor_atendimento,
	 	cd_grupo_gasto,
		cd_brasindice,
		cd_procedimento_ref,
		cd_proc_ref_conv,
	 	nr_seq_protocolo,
		cd_convenio,
		cd_cgc_hospital,
		cd_cgc_convenio,
	 	cd_interno,
		ie_responsavel_credito,
		ie_emite_conta,
		ie_total_interf,
		nr_seq_proc_pacote,
		cd_especialidade,
		cd_funcao_espec_med,
		dt_prescricao,
		dt_autorizacao,
		vl_filme,
		nr_doc_convenio,
		nr_prescricao,
		cd_medico_req,
		nr_crm_requisitante,
		cd_simpro,
		cd_senha_guia,
		dt_entrada_unidade,
		dt_saida_unidade,
		cd_tipo_item_interf,
		nr_seq_proc_princ,
		nr_seq_conta_convenio,
		cd_cgc_prestador,
		vl_original)
	values	(nr_sequencia_w,
		4,
		1,
		1,
		nr_seq_envio_convenio_p,
	 	nr_atendimento_p,
		nr_interno_conta_p,
		1,
		2,
		98007530,
		0,
		'98007530',
		substr(wheb_mensagem_pck.get_texto(307094),1,255),
		1,
		null,
		dt_entrada_w,
		vl_total_med_ipe_w,
		vl_total_med_ipe_w,
		null,
		null,
		null,
		null,
		null,
		null,
		nr_seq_protocolo_p,
		cd_convenio_p,
		cd_cgc_hospital_p,
		cd_cgc_convenio_p,
	 	cd_interno_p,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		0,
		nr_doc_convenio_w,
		null,
		null,
		null,
		null,
		cd_senha_guia_w,
		dt_entrada_unidade_w,
		dt_saida_unidade_w,
		5,
		0,
		nr_seq_conta_convenio_w,
		cd_cgc_hospital_p,
		vl_total_med_ipe_w);
	end;
end if;

/* treat procedures */
open c02;
loop
fetch c02 	into
		nr_seq_item_w,
		ie_tipo_item_w,
		cd_item_w,
		cd_proc_tuss_w,
		ie_origem_proced_w,
		cd_item_convenio_w,
		ds_item_w,
		qt_item_w,
		cd_unidade_medida_w,
		dt_item_w,
		vl_unitario_item_w,
		vl_total_item_w,
		qt_ch_item_w,
		vl_unitario_ch_w,
		cd_medico_executor_w,
		cd_medico_exec_conv_w,
		nm_medico_executor_w,
		nr_crm_executor_w,
		uf_crm_executor_w,
		nr_cpf_executor_w,
		cd_funcao_executor_w,
		nr_porte_anestesico_w,
		pr_funcao_participante_w,
		pr_via_acesso_w,
		pr_hora_extra_w,
		ie_via_acesso_w,
		cd_classif_setor_w,
		cd_setor_atendimento_w,
		cd_grupo_gasto_w,
		cd_brasindice_w,
		cd_procedimento_ref_w,
		cd_proc_ref_conv_w,
		ie_responsavel_credito_w,
		ie_emite_conta_w,
		ie_emite_conta_hosp_w,
		vl_honorario_w,
		nr_seq_proc_pacote_w,
	 	cd_especialidade_w,
	 	cd_funcao_espec_med_w,
		cd_cgc_cpf_resp_cred_w,
		cd_cgc_prestador_w,
		dt_prescricao_w,
		nr_doc_convenio_w,
		nr_seq_autorizacao_w,
		vl_filme_w,
		nr_prescricao_w,
		nr_atendimento_w,
		ie_video_w,
		vl_custo_oper_w,
		cd_tipo_item_interf_w,
		cd_area_proc_w,
		cd_especialidade_proc_w,
		cd_grupo_proc_w,
		cd_tipo_procedimento_w,
		nr_seq_atepacu_w,
		ie_forma_apresentacao_w,
		ie_via_acesso_inf_w,
		ie_participante_w,
		pr_faturado_w,
		nr_seq_proc_princ_w,
		qt_item_conv_div_w,
		cd_cgc_partic_w,
		nr_seq_cbo_executor_w,
		sg_cons_prof_exec_w,
		cd_cgc_estab_executor_w,
		cd_cns_estab_executor_w,
		nr_seq_proc_crit_hor_w;
exit when 	c02%notfound;
     		begin
		/* get data of atend_paciente_unidade */
		select	nvl(max(dt_entrada_unidade),dt_entrada_w),
			nvl(max(dt_saida_unidade),dt_alta_w)
		into	dt_entrada_unidade_w,
			dt_saida_unidade_w
		from	atend_paciente_unidade
		where	nr_seq_interno	= nr_seq_atepacu_w;

		/* get date of authorization */

		if (qt_ch_item_w > 99999) then
			qt_ch_item_w := 99999;
		end if;

		begin
		select	nvl(dt_autorizacao,dt_entrada_w),
			cd_medico_solicitante
		into	dt_autorizacao_w,
			cd_medico_reqaut_w
		from	autorizacao_convenio
		where	nr_atendimento	= nr_atendimento_p
		and	cd_autorizacao	= nr_doc_convenio_w;
		exception
			when others then
			dt_autorizacao_w	:= dt_entrada_w;
		end;
		vl_original_w:= vl_total_item_w;
		/* treat participants ipe, even if the rule defines what doctor receives direct, should record value of participants */
		/* adelson 24-02-2006 os30090 */
		if	(ie_agrup_item_interf_w in ('E','G'))	and
			(ie_participante_w = 'S')	then
			begin
			vl_total_item_w	:= vl_honorario_w;
			vl_unitario_item_w	:= vl_honorario_w;
			end;
		end if;

		/* treat ipe - Oxygen services in minutes should generate in qt_ocorrencia_dia */
		/* treat ipe - created the type of service 6 */
		/* treat ipe - form of presentation 2-period minutes 4-quantity and minutes */
		if	(ie_agrup_item_interf_w in ('E','G'))	and
			(ie_forma_apresentacao_w in(2,4))	then
			cd_tipo_item_interf_w	:= 6;
		end if;

		/* get sequence */
		select	w_interf_conta_item_seq.nextval
		into	nr_sequencia_w
		from 	dual;
		/* indicator interface totals */
		begin
		select	ie_total_interf
		into	ie_total_interf_w
		from	conta_paciente_estrutura
		where	cd_estrutura	= ie_emite_conta_hosp_w;
		exception
			when others then
			ie_total_interf_w	:= null;
	 	end;
		if (pr_funcao_participante_w < 1) then
			pr_funcao_participante_w := (pr_funcao_participante_w * 100);
		end if;
		/* get the insurance code for the third-party provider */
		cd_interno_terc_w	:= null;
		if	ie_responsavel_credito_w = 'M' then
			begin
			select	a.cd_medico_convenio
			into	cd_interno_terc_w
			from	medico_convenio a
			where	a.cd_convenio	 = cd_convenio_p
			and	a.cd_pessoa_fisica = cd_medico_executor_w;
			exception
				when others then
				cd_interno_terc_w	:= null;
			end;
		elsif	nvl(ie_responsavel_credito_w,'T') not in('RM','H') then
			begin
			select	a.cd_interno
			into	cd_interno_terc_w
			from	param_interface a
			where	a.cd_convenio	= cd_convenio_p
			and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
			and	nvl(cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w
			and	a.cd_cgc	= cd_cgc_cpf_resp_cred_w
			and	a.nr_sequencia	=
					(select max(x.nr_sequencia) from  param_interface x
					where	x.cd_convenio	= cd_convenio_p
					and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
					and	nvl(cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w
					and	x.cd_cgc	= cd_cgc_cpf_resp_cred_w);
			exception
					when others then
					cd_interno_terc_w	:= null;
			end;
		end if;

		/* get code hemosc hsj-hsjb */
		if	(cd_cgc_prestador_w = '86897113000157') then
			begin
			cd_interno_terc_w := '20000020';
			end;
		end if;


		/* get requesting physician */
		begin

		begin
 		select	a.cd_medico_req
		into	cd_medico_req_w
		from	procedimento_paciente a
		where	a.nr_sequencia	= nr_seq_item_w;
		exception
			when others then
			cd_medico_req_w	:= null;
		end;

		if	(cd_medico_req_w is null) then
			begin
			select	cd_medico
			into	cd_medico_req_w
			from	prescr_medica
			where	nr_prescricao	= nr_prescricao_w;
			exception
				when others then
				cd_medico_req_w	:= null;
			end;
		end if;

		if	(cd_medico_req_w is null) and
			(cd_medico_reqaut_w is not null) then
			cd_medico_req_w	:= cd_medico_reqaut_w;
		end if;
		if	(cd_medico_req_w is null) then
			begin
			select	cd_medico_atendimento
			into	cd_medico_req_w
			from	atendimento_paciente
			where	nr_atendimento	= nr_atendimento_w;
			exception
				when others then
				cd_medico_req_w	:= null;
			end;
		end if;

		if	(cd_medico_req_w is not null) then
			begin
			select	substr(obter_crm_medico(cd_pessoa_fisica), 1, 255)
			into	nr_crm_requisitante_w
			from	medico
			where	cd_pessoa_fisica = cd_medico_req_w;
			exception
				when others then
				nr_crm_requisitante_w:= null;
			end;
		end if;
		end;

		/* new exams routine of ipe */
		vl_matmed_exame_w	:= 0;
		if	(ie_agrup_item_interf_w in ('E','G'))	and
			(ie_tipo_atendimento_w = 7)		then
			begin
			select	sum(x.vl_material)
			into	vl_matmed_exame_w
			from	material_atend_paciente x
			where	x.nr_interno_conta 	= nr_interno_conta_p
			and	x.cd_motivo_exc_conta 	is null
			and	x.nr_seq_proc_princ	= nr_seq_item_w;
			end;
		end if;
		
		begin
		nr_seq_cbo_executor_w := somente_numero(nr_seq_cbo_executor_w);
		exception 
			when others then
			nr_seq_cbo_executor_w := '';
		end;
		
		/* save procedures */
		begin
		insert into w_interf_conta_item(
				nr_sequencia,
				cd_tipo_registro,
				nr_seq_registro,
				nr_seq_interface,
	 			nr_remessa,
				nr_atendimento,
				nr_interno_conta,
				nr_seq_item,
	 			ie_tipo_item,
				cd_item,
				cd_procedimento_tuss,
				ie_origem_proced,
				cd_item_convenio,
	 			ds_item,
				qt_item,
				cd_unidade_medida,
				dt_item,
	 			vl_unitario_item,
				vl_total_item,
				qt_ch_item,
				vl_unitario_ch,
	 			cd_medico_executor,
				cd_medico_exec_conv,
				nm_medico_executor,
				nr_crm_executor,
	 			uf_crm_executor,
				nr_cpf_executor,
				cd_funcao_executor,
				nr_porte_anestesico,
	 			pr_funcao_participante,
				pr_via_acesso,
				pr_hora_extra,
				ie_via_acesso,
	 			cd_classif_setor,
				cd_setor_atendimento,
				cd_grupo_gasto,
				cd_brasindice,
	 			cd_procedimento_ref,
				cd_proc_ref_conv,
				nr_seq_protocolo,
				cd_convenio,
	 			cd_cgc_hospital,
				cd_cgc_convenio,
				cd_interno,
				ie_responsavel_credito,
				ie_emite_conta,
				ie_total_interf,
				vl_honorario,
				nr_seq_proc_pacote,
		 		cd_especialidade,
				cd_funcao_espec_med,
				dt_prescricao,
				dt_autorizacao,
				cd_cgc_cpf_resp_cred,
				cd_cgc_prestador,
				vl_filme,
				nr_doc_convenio,
				nr_prescricao,
				cd_medico_req,
				nr_crm_requisitante,
				vl_custo_oper,
				qt_filme,
				ie_video,
				cd_tipo_item_interf,
				cd_area_proc,
				cd_especialidade_proc,
				cd_grupo_proc,
				cd_tipo_procedimento,
				cd_senha_guia,
				dt_entrada_unidade,
				dt_saida_unidade,
				nr_seq_proc_princ,
				ie_via_acesso_inf,
				nr_seq_conta_convenio,
				vl_matmed_proc_princ,
				pr_faturado,
				vl_original,
				qt_item_conv_div,
				cd_cgc_partic,
				nr_seq_cbo_executor,
				sg_cons_prof_exec,
				cd_cgc_estab_executor,
				cd_cns_estab_executor,
				tx_item)
		values	(
	 			nr_sequencia_w,
				4,
				1,
				1,
	 			nr_seq_envio_convenio_p,
				nr_atendimento_p,
				nr_interno_conta_p,
				nr_seq_item_w,
				ie_tipo_item_w,
				cd_item_w,
				cd_proc_tuss_w,
				ie_origem_proced_w,
				cd_item_convenio_w,
				substr(ds_item_w,1,50),
				qt_item_w,
				cd_unidade_medida_w,
				dt_item_w,
				vl_unitario_item_w,
				vl_total_item_w,
				qt_ch_item_w,
				vl_unitario_ch_w,
				cd_medico_executor_w,
				cd_medico_exec_conv_w,
				nm_medico_executor_w,
				nr_crm_executor_w,
				uf_crm_executor_w,
				nr_cpf_executor_w,
				cd_funcao_executor_w,
				nr_porte_anestesico_w,
				pr_funcao_participante_w,
				pr_via_acesso_w,
				pr_hora_extra_w,
				ie_via_acesso_w,
				cd_classif_setor_w,
				cd_setor_atendimento_w,
				cd_grupo_gasto_w,
				cd_brasindice_w,
				cd_procedimento_ref_w,
				cd_proc_ref_conv_w,
				nr_seq_protocolo_p,
				cd_convenio_p,
	 			cd_cgc_hospital_p,
				cd_cgc_convenio_p,
				nvl(cd_interno_terc_w,cd_interno_p),
				ie_responsavel_credito_w,
				ie_emite_conta_w,
				ie_total_interf_w,
				vl_honorario_w,
				nr_seq_proc_pacote_w,
	 			cd_especialidade_w,
				cd_funcao_espec_med_w,
				dt_prescricao_w,
				dt_autorizacao_w,
				cd_cgc_cpf_resp_cred_w,
				cd_cgc_prestador_w,
				vl_filme_w,
				nr_doc_convenio_w,
				nr_prescricao_w,
				cd_medico_req_w,
				nr_crm_requisitante_w,
				vl_custo_oper_w,
				qt_filme_w,
				ie_video_w,
				cd_tipo_item_interf_w,
				cd_area_proc_w,
				cd_especialidade_proc_w,
				cd_grupo_proc_w,
				cd_tipo_procedimento_w,
				cd_senha_guia_w,
				dt_entrada_unidade_w,
				dt_saida_unidade_w,
				nr_seq_proc_princ_w,
				ie_via_acesso_inf_w,
				nr_seq_conta_convenio_w,
				vl_matmed_exame_w,
				pr_faturado_w,
				vl_original_w,
				qt_item_conv_div_w,
				cd_cgc_partic_w,
				nr_seq_cbo_executor_w,
				sg_cons_prof_exec_w,
				cd_cgc_estab_executor_w,
				cd_cns_estab_executor_w,
				pr_funcao_participante_w);
		end;

		end;
end loop;
close c02;

end gerar_interf_conta_item;
/
