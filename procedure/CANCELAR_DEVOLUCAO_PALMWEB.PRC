create or replace
procedure cancelar_devolucao_palmweb(	nr_devolucao_p	number) is 


qt_existe_w	number(10);

begin

if	(nr_devolucao_p	is not null) then
	
	select	count(1)
	into	qt_existe_w
	from	item_devolucao_material_pac
	where	nr_devolucao = nr_devolucao_p;
	
	if	(qt_existe_w > 0) then
		delete	from item_devolucao_material_pac
		where	nr_devolucao = nr_devolucao_p;
	end if;
	
	delete	from devolucao_material_pac
	where	nr_devolucao = nr_devolucao_p;

end if;

commit;

end cancelar_devolucao_palmweb;
/