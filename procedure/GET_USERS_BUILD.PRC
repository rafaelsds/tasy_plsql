create or replace function GET_USERS_BUILD(build_pendente varchar2 default 'N'
                                          , liberacao_build varchar2  default 'N'
                                          , solicitacao_emer varchar2 default 'N'
                                          , solicitacao_pmo varchar2 default 'N'
                                          , build_fila varchar2 default 'N') return varchar2 is
  l_text  VARCHAR2(32767) := NULL;
  ds_email varchar(2000) := null;
  c010 Integer;
  ds_comando_cursor_w varchar2(4000) := 'SELECT u.nm_usuario FROM usuario u ' ||
						'inner join man_cad_dados_inf_build b on b.cd_pessoa_fisica = u.cd_pessoa_fisica ' ||
						'where B.IE_SITUACAO = ''A''';
  retorno_w number(5);
BEGIN
  if (upper(build_pendente) = 'S') then
    ds_comando_cursor_w := ds_comando_cursor_w || ' AND B.IE_RECEBE_BUILD_PENDENTE = ''S''';
  END IF;
  if (upper(liberacao_build) = 'S') then
    ds_comando_cursor_w := ds_comando_cursor_w || ' AND B.IE_RECEBE_BUILD_LIB = ''S''';
  END IF;
  if (upper(solicitacao_emer) = 'S') then
    ds_comando_cursor_w := ds_comando_cursor_w || ' AND B.IE_RECEBE_SOLIC_EMER = ''S''';
  END IF;
  if (upper(solicitacao_pmo) = 'S') then
    ds_comando_cursor_w := ds_comando_cursor_w || ' AND B.IE_RECEBE_SOLIC_PMO = ''S''';
  END IF;
  if (upper(build_fila) = 'S') then
    ds_comando_cursor_w := ds_comando_cursor_w || ' AND B.IE_RECEBE_BUILD_QUEUE = ''S''';
  END IF;
 
  C010 := DBMS_SQL.OPEN_CURSOR;
  DBMS_SQL.PARSE(C010, ds_comando_cursor_w, dbms_sql.Native);
  DBMS_SQL.DEFINE_COLUMN(C010, 1, ds_email,2000);
  retorno_w := DBMS_SQL.execute(c010);

  while	( DBMS_SQL.FETCH_ROWS(C010) > 0 ) loop
    DBMS_SQL.COLUMN_VALUE(C010, 1, ds_email);
    l_text := l_text || ';' || ds_email;
  end loop;
  DBMS_SQL.CLOSE_CURSOR(C010);
  RETURN LTRIM(l_text, ';');  
END;
/