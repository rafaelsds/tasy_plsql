create or replace
procedure pls_inserir_carencia
			(	nr_seq_tipo_insercao_p	number,
				nr_w_pls_carencia_p	number,
				nr_seq_grupo_carencia_p	number,
				nr_seq_plano_p		number,
				nm_usuario_p		varchar2,
				ie_opcao_p		varchar2) is

/*
C - Contrato
S - Segurado do contrato
P - Beneficiário da proposta
PA - Proposta de adesão
TP - Todos beneficiários da proposta
*/

qt_dias_carencia_w	number(10);
nr_seq_tipo_carencia_w	number(10);
qt_carencia_w		number(10);
nr_seq_carencia_w	number(10);
dt_incio_vigencia_w	date;
ds_observacao_w		varchar2(4000);
qt_dias_manutencao_w	pls_grupo_carencia.qt_dias_manutencao%type;
ds_grupo_w		pls_grupo_carencia.ds_grupo%type;
nr_seq_proposta_benef_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_proposta_beneficiario
	where	nr_seq_proposta	= nr_seq_tipo_insercao_p
	and	nr_seq_plano	= nr_seq_plano_p;

begin

if	(nvl(nr_seq_grupo_carencia_p,0) <> 0) then /* Grupo de carência */
	select	qt_dias_manutencao,
		ds_grupo
	into	qt_dias_manutencao_w,
		ds_grupo_w
	from	pls_grupo_carencia
	where	nr_sequencia	= nr_seq_grupo_carencia_p;
	
	if	(nvl(qt_dias_manutencao_w,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(295607,'DS_GRUPO='||ds_grupo_w);
	end if;
	
	if	(ie_opcao_p = 'C') then /* Contrato */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_grupo_carencia	= nr_seq_grupo_carencia_p
		and	nr_seq_contrato		= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_contrato, nr_seq_grupo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_grupo_carencia_p,
					qt_dias_manutencao_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		end if;
	elsif	(ie_opcao_p = 'S') then /* Beneficiário */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_grupo_carencia	= nr_seq_grupo_carencia_p
		and	nr_seq_segurado		= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_segurado, nr_seq_grupo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_grupo_carencia_p,
					qt_dias_manutencao_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		end if;
	elsif	(ie_opcao_p = 'P') then /* Beneficiário da proposta */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_grupo_carencia	= nr_seq_grupo_carencia_p
		and	nr_seq_pessoa_proposta	= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_pessoa_proposta, nr_seq_grupo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_grupo_carencia_p,
					qt_dias_manutencao_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		end if;
	elsif	(ie_opcao_p = 'PA') then /* Proposta de adesão */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_grupo_carencia	= nr_seq_grupo_carencia_p
		and	nr_seq_proposta		= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_proposta, nr_seq_grupo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_grupo_carencia_p,
					qt_dias_manutencao_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		end if;
	elsif	(ie_opcao_p = 'TP') then /* Todos beneficiários da proposta */
		open C01;
		loop
		fetch C01 into	
			nr_seq_proposta_benef_w;
		exit when C01%notfound;
			begin
			select	count(*)
			into	qt_carencia_w
			from	pls_carencia
			where	nr_seq_grupo_carencia	= nr_seq_grupo_carencia_p
			and	nr_seq_pessoa_proposta	= nr_seq_proposta_benef_w;
			
			if	(qt_carencia_w = 0) then
				insert	into	pls_carencia
					(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, nr_seq_pessoa_proposta, nr_seq_grupo_carencia,
						qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
				values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
						nm_usuario_p, nr_seq_proposta_benef_w, nr_seq_grupo_carencia_p,
						qt_dias_manutencao_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
			end if;
			end;
		end loop;
		close C01;
	end if;
elsif	(nvl(nr_w_pls_carencia_p,0) <> 0) then
	select	nvl(qt_dias_carencia,0),
		nr_seq_tipo_carencia,
		dt_inicio_vigencia,
		ds_observacao
	into	qt_dias_carencia_w,
		nr_seq_tipo_carencia_w,
		dt_incio_vigencia_w,
		ds_observacao_w
	from	w_pls_carencia
	where	nr_sequencia	= nr_w_pls_carencia_p;
	
	if	(ie_opcao_p = 'C') then /* Contrato */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
		and	nr_seq_contrato		= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_contrato, nr_seq_tipo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_tipo_carencia_w,
					qt_dias_carencia_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		else
			begin
			select	nr_sequencia
			into	nr_seq_carencia_w
			from	pls_carencia
			where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
			and	nr_seq_contrato		= nr_seq_tipo_insercao_p;
			exception
			when others then
				nr_seq_carencia_w := 0;
			end;
			
			if	(nr_seq_carencia_w <> 0) then
				update	pls_carencia
				set	qt_dias			= qt_dias_carencia_w,
					dt_inicio_vigencia	= dt_incio_vigencia_w,
					ds_observacao		= decode(ds_observacao_w,'','', ' - ' || ds_observacao_w)
				where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
				and	nr_seq_contrato		= nr_seq_tipo_insercao_p;
			end if;
		end if;
	elsif	(ie_opcao_p = 'S') then /* Beneficiário */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
		and	nr_seq_segurado		= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_segurado, nr_seq_tipo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_tipo_carencia_w,
					qt_dias_carencia_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		else
			begin
			select	nr_sequencia
			into	nr_seq_carencia_w
			from	pls_carencia
			where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
			and	nr_seq_segurado		= nr_seq_tipo_insercao_p;
			exception
			when others then
				nr_seq_carencia_w := 0;
			end;
			
			if	(nr_seq_carencia_w <> 0) then
				update	pls_carencia
				set	qt_dias			= qt_dias_carencia_w,
					dt_inicio_vigencia	= dt_incio_vigencia_w,
					ds_observacao		= decode(ds_observacao_w,'','', ' - ' || ds_observacao_w)
				where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
				and	nr_seq_segurado		= nr_seq_tipo_insercao_p;
			end if;
		end if;
	elsif	(ie_opcao_p = 'P') then /* Beneficiário da proposta */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
		and	nr_seq_pessoa_proposta	= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) and
			(substr(pls_cons_sexo_carencia_prop(nr_seq_tipo_insercao_p, nr_seq_tipo_carencia_w),1,255) = 'S') then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_pessoa_proposta, nr_seq_tipo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_tipo_carencia_w,
					qt_dias_carencia_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		else
			begin
			select	nr_sequencia
			into	nr_seq_carencia_w
			from	pls_carencia
			where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
			and	nr_seq_pessoa_proposta	= nr_seq_tipo_insercao_p;
			exception
			when others then
				nr_seq_carencia_w := 0;
			end;
			
			if	(nr_seq_carencia_w <> 0) then
				update	pls_carencia
				set	qt_dias			= qt_dias_carencia_w,
					dt_inicio_vigencia	= dt_incio_vigencia_w,
					ds_observacao		= decode(ds_observacao_w,'','', ' - ' || ds_observacao_w)
				where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
				and	nr_seq_pessoa_proposta	= nr_seq_tipo_insercao_p;
			end if;
		end if;
	elsif	(ie_opcao_p = 'PA') then /* Proposta de adesão */
		select	count(*)
		into	qt_carencia_w
		from	pls_carencia
		where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
		and	nr_seq_proposta		= nr_seq_tipo_insercao_p;
		
		if	(qt_carencia_w = 0) then
			insert	into	pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_proposta, nr_seq_tipo_carencia,
					qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_insercao_p, nr_seq_tipo_carencia_w,
					qt_dias_carencia_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
		else
			begin
			select	nr_sequencia
			into	nr_seq_carencia_w
			from	pls_carencia
			where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
			and	nr_seq_proposta	= nr_seq_tipo_insercao_p;
			exception
			when others then
				nr_seq_carencia_w := 0;
			end;
			
			if	(nr_seq_carencia_w <> 0) then
				update	pls_carencia
				set	qt_dias			= qt_dias_carencia_w,
					dt_inicio_vigencia	= dt_incio_vigencia_w,
					ds_observacao		= decode(ds_observacao_w,'','', ' - ' || ds_observacao_w)
				where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
				and	nr_seq_proposta		= nr_seq_tipo_insercao_p;
			end if;
		end if;
	elsif	(ie_opcao_p = 'TP') then /* Todos beneficiários da proposta */
		open C01;
		loop
		fetch C01 into	
			nr_seq_proposta_benef_w;
		exit when C01%notfound;
			begin
			select	count(*)
			into	qt_carencia_w
			from	pls_carencia
			where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
			and	nr_seq_pessoa_proposta	= nr_seq_proposta_benef_w;
			
			if	(qt_carencia_w = 0) and
				(substr(pls_cons_sexo_carencia_prop(nr_seq_proposta_benef_w, nr_seq_tipo_carencia_w),1,255) = 'S') then
				insert	into	pls_carencia
					(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, nr_seq_pessoa_proposta, nr_seq_tipo_carencia,
						qt_dias, dt_inicio_vigencia, ds_observacao, ie_origem_carencia_benef)
				values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate,
						nm_usuario_p, nr_seq_proposta_benef_w, nr_seq_tipo_carencia_w,
						qt_dias_carencia_w, dt_incio_vigencia_w, ds_observacao_w, 'P');
			else
				begin
				select	nr_sequencia
				into	nr_seq_carencia_w
				from	pls_carencia
				where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
				and	nr_seq_pessoa_proposta	= nr_seq_proposta_benef_w;
				exception
				when others then
					nr_seq_carencia_w := 0;
				end;
				
				if	(nr_seq_carencia_w <> 0) then
					update	pls_carencia
					set	qt_dias			= qt_dias_carencia_w,
						dt_inicio_vigencia	= dt_incio_vigencia_w,
						ds_observacao		= decode(ds_observacao_w,'','', ' - ' || ds_observacao_w)
					where	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
					and	nr_seq_pessoa_proposta	= nr_seq_proposta_benef_w;
				end if;
			end if;
			end;
		end loop;
		close C01;
	end if;
end if;

commit;

end pls_inserir_carencia;
/