create or replace
procedure nut_definir_presc_nutricao ( 	cd_setor_atendimento_p	number,
					dt_referencia_p 	date,
					nm_usuario_p		Varchar2) is 


					  
nr_atendimento_w	number(10);
nr_prescricao_w		number(14);
nr_seq_servico_w	number(10);
nr_seq_serv_rep_w	number(10);
cd_estab_w		number(10);
dt_servico_w		date;
ie_estabs_w		varchar2(1);
					  
Cursor C01 is
	SELECT  b.nr_atendimento
	FROM    atendimento_paciente b,
		ocupacao_unidade_v a,
		setor_atendimento s
	WHERE   ((cd_setor_atendimento_p = 0) OR (a.cd_setor_atendimento = cd_setor_atendimento_p))
	AND	((ie_estabs_w = 'S' and cd_estab_w = b.cd_estabelecimento) or (ie_estabs_w = 'N'))
	AND     a.cd_setor_atendimento 		= s.cd_setor_atendimento
	AND     a.nr_atendimento IS NOT NULL
	AND	a.nr_atendimento 		= b.nr_atendimento
	ORDER BY b.nr_atendimento;	
 
Cursor C02 is
	SELECT	b.nr_sequencia,
		dt_servico
	FROM  	nut_atend_serv_dia b                     
	WHERE	b.nr_atendimento =  nr_atendimento_w
	and	b.dt_servico BETWEEN TO_DATE(to_char(dt_referencia_p,'dd/mm/yyyy')||' 00:00:00','dd/mm/yyyy hh24:mi:ss') AND TO_DATE(to_char(dt_referencia_p,'dd/mm/yyyy')||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
	AND	b.dt_liberacao IS NULL  
	order by nr_sequencia;
	/*AND	EXISTS( SELECT	1                          
			FROM	prescr_medica a               
			WHERE	b.nr_atendimento = a.nr_atendimento 
			and	a.nr_prescricao	 = nr_prescricao_w
			AND		a.dt_liberacao IS NOT NULL          
			AND		((b.dt_servico BETWEEN a.dt_inicio_prescr AND a.dt_validade_prescr) OR (a.dt_validade_prescr IS NULL))                  
			AND ((EXISTS (SELECT 1 FROM prescr_dieta b WHERE (a.nr_prescricao = b.nr_prescricao)))                                    
			OR   (EXISTS (SELECT 1 FROM rep_jejum c WHERE (a.nr_prescricao = c.nr_prescricao)))                                       
			OR	 (EXISTS (SELECT 1 FROM prescr_material d WHERE (a.nr_prescricao = d.nr_prescricao AND d.ie_agrupador IN (8,12))))   
			OR	 (EXISTS (SELECT 1 FROM nut_pac e WHERE (a.nr_prescricao = e.nr_prescricao)))))	
	order by nr_sequencia;*/
	
	
begin 
obter_param_usuario(1003, 78, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_estabs_w); 
cd_estab_w	:= wheb_usuario_pck.get_cd_estabelecimento;

open C01;
loop
fetch C01 into	
	nr_atendimento_w;
exit when C01%notfound;
	begin
	
			
	open C02;
	loop
	fetch C02 into	
		nr_seq_servico_w,
		dt_servico_w;
	exit when C02%notfound;
		begin
		
		SELECT	nvl(MAX(nr_prescricao),0)  
		into	nr_prescricao_w	
		FROM	prescr_medica a               
		WHERE	a.nr_atendimento = nr_atendimento_w 
		AND	nvl(a.dt_liberacao, a.dt_liberacao_medico) IS NOT NULL          
		AND	((dt_servico_w BETWEEN a.dt_inicio_prescr AND a.dt_validade_prescr) OR (a.dt_validade_prescr IS NULL))                  
		AND ((EXISTS ( 	SELECT 1 
			FROM prescr_dieta b 
			WHERE (a.nr_prescricao = b.nr_prescricao)))                                    
			OR   (EXISTS (SELECT 1 FROM rep_jejum c WHERE (a.nr_prescricao = c.nr_prescricao)))                                       
			OR	 (EXISTS (SELECT 1 FROM prescr_material d WHERE (a.nr_prescricao = d.nr_prescricao AND d.ie_agrupador IN (8,12))))   
			OR	 (EXISTS (SELECT 1 FROM nut_pac e WHERE (a.nr_prescricao = e.nr_prescricao)))
			or	 (EXISTS (SELECT 1 FROM prescr_leite_deriv e WHERE (a.nr_prescricao = e.nr_prescricao))));

		
		
		
		SELECT	NVL(MAX(nr_sequencia),0)                   
		into	nr_seq_serv_rep_w
		FROM	nut_atend_serv_dia_rep           
		WHERE	nr_seq_serv_dia = nr_seq_servico_w
		AND	dt_liberacao IS NULL;
		
		if (nr_prescricao_w <> 0) then
			Definir_Prescricao_Nutricao(nr_prescricao_w, nr_seq_serv_rep_w, nr_seq_servico_w, 'T', nm_usuario_p);   
		end if;

		end;
	end loop;	
	close C02;	
		
	end;
end loop;
close C01;


commit;

end nut_definir_presc_nutricao;
/