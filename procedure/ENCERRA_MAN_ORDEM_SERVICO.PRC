create or replace
procedure encerra_man_ordem_servico(	nr_sequencia_p		number,
					nr_seq_estagio_p	number,
					nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	update	man_ordem_servico
	set	ie_status_ordem = 3,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		 nr_seq_estagio = decode(nr_seq_estagio_p,0,nr_seq_estagio,nr_seq_estagio_p),
		dt_fim_real = sysdate
	where	nr_sequencia = nr_sequencia_p;
	commit;
	
	end;
end if;

end encerra_man_ordem_servico;
/