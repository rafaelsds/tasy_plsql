create or replace 
procedure Consistir_interv_solic_exame(	
					nr_prescricao_p		Number,
					nr_sequencia_p		Number,
					ds_erro_p			Out	Varchar2,
					ie_lib_prescr_p 	out	varchar2,
					qt_horas_p			out	number,
					qt_minutos_p		out	number,
					ie_quantidade_p		out	varchar2,
					ds_mensagem_regra_p	out varchar2,
					ds_mensagem_data_p 	out varchar2,
					dt_ultima_data_p	out date,
					cd_perfil_p			number,
					nm_usuario_p		varchar2 ) is


ds_erro_w						Varchar2(2000) := '';
cd_procedimento_w				Number(15);
ie_origem_proced_w				Number(10);
dt_prev_execucao_w				Date;
dt_ultimo_exec_w				Date;
dt_validade_prescr_w			Date;
dt_inicio_prescr_w				Date;
cd_estabelecimento_w			Number(4);
nr_seq_exame_w					Number(10);
nr_seq_grupo_w					Number(10);
nr_seq_proc_interno_w			Number(10);
qt_hora_w						Number(15,6);
qt_hora_ww						Number(15,6);
qt_min_ref_w					Number(15,6);
qt_minutos_w					number(15,6);
qt_minutos_ww					number(15,6);
nr_atendimento_w				Number(10);
nr_seq_derivado_regra_w			Number(10);
qt_prescricao_cons_w			Number(10);
ds_procedimentos_w				Varchar2(2000) := '';
nr_seq_exame_lab_w				Number(10);
nr_seq_grupo_lab_w				Number(10);	
nr_seq_exame_interno_w			Number(10);
cd_setor_atendimento_w			Number(10);
ie_mes_corrente_w				Varchar2(20);
cd_material_exame_w				Varchar2(20);
ie_recem_nato_w					Varchar2(20);
ie_considera_setor_w			Varchar2(1);
cd_area_procedimento_regra_w	Number(15);
nr_seq_regra_w					Number(15);
cd_especialidade_regra_w		Number(15);
cd_grupo_proc_regra_w			Number(15);
cd_procedimento_regra_w			Number(15);
ie_origem_proced_regra_w		Number(10);
ie_tipo_atendimento_w			Number(3);
cd_area_procedimento_prescr_w	Number(15);
cd_especialidade_prescr_w		Number(15);
cd_grupo_proc_prescr_w			Number(15);
ie_lib_prescr_w					varchar2(1) := 'N';  /* Rafael em 20/12/2007 cfe Marcus x PC */
cd_convenio_w					Number(5);	     /*David em 08/07/2008 OS 99741*/
qt_permitida_w					Number(9);
qt_executada_periodo_w			Number(15) := 0;
dt_limite_minimo_w				date;
cd_pessoa_fisica_w				Varchar2(10);
ie_consiste_lado_w				Varchar2(10);
ie_lado_w						Varchar2(10);
nr_seq_derivado_w				number(10) := 0;
ie_considera_validade_w			varchar2(1);
ie_considerar_pessoa_fisica_w	varchar2(1);
nr_seq_forma_org_sus_w			number(10) := 0;
nr_seq_grupo_sus_w				number(10) := 0;
nr_seq_subgrupo_sus_w			number(10) := 0;
ds_mensagem_regra_w				varchar2(255);


Cursor C01 is
	Select	cd_procedimento,
			ie_origem_proced,
			nr_seq_exame,
			nr_seq_proc_interno,
			dt_prev_execucao,
			cd_material_exame,
			ie_lado,
			cd_material_exame,
			nvl(nr_seq_derivado,0)
	from	prescr_procedimento
	where	nr_prescricao		= nr_prescricao_p
	and		nr_sequencia		= nr_sequencia_p
	and		nvl(ie_suspenso,'N')	<> 'S';

Cursor C02 is
	select	nvl(a.qt_hora,0),
			nvl(a.qt_minutos,0),
			a.nr_seq_exame_lab,
			a.nr_seq_grupo_lab,
			a.nr_seq_exame_interno,
			a.cd_area_procedimento,
			a.cd_especialidade,
			a.cd_grupo_proc,
			a.cd_procedimento,
			a.ie_origem_proced,
			a.ie_lib_prescr,
			nvl(a.qt_permitida,1),
			a.ie_consiste_lado,
			a.nr_sequencia,
			nvl(a.ie_considerar_setor,'N'),
			a.ie_mes_corrente,
			a.nr_seq_derivado,
			nvl(a.ie_considera_validade,'N'),
			nvl(a.ds_mensagem_regra,''),
			nvl(ie_considerar_pessoa_fisica,'N')
	from	regra_interv_sol_exame a
	where	nvl(a.nr_seq_exame_lab, nvl(nr_seq_exame_w,0))			= nvl(nr_seq_exame_w,0)
	and		nvl(a.cd_material_exame, nvl(cd_material_exame_w,'0'))		= nvl(cd_material_exame_w,'0')
	and		nvl(a.nr_seq_grupo_lab, nvl(nr_seq_grupo_w,0))			= nvl(nr_seq_grupo_w,0)
	and		nvl(a.nr_seq_exame_interno, nvl(nr_seq_proc_interno_w,0))	= nvl(nr_seq_proc_interno_w,0)
	and		nvl(a.cd_area_procedimento, nvl(cd_area_procedimento_prescr_w,0))	= nvl(cd_area_procedimento_prescr_w,0)
	and		nvl(a.cd_especialidade, nvl(cd_especialidade_prescr_w,0))	= nvl(cd_especialidade_prescr_w,0)
	and		nvl(a.cd_grupo_proc, nvl(cd_grupo_proc_prescr_w,0))		= nvl(cd_grupo_proc_prescr_w,0)
	and		nvl(a.cd_procedimento, nvl(cd_procedimento_w,0))		= nvl(cd_procedimento_w,0)
	and		nvl(a.ie_origem_proced, nvl(ie_origem_proced_w,0))		= nvl(ie_origem_proced_w,0)
	and		nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0))	= nvl(ie_tipo_atendimento_w,0)
	and		nvl(a.nr_seq_forma_org,nr_seq_forma_org_sus_w)			= nr_seq_forma_org_sus_w
	and		nvl(a.nr_seq_grupo,nr_seq_grupo_sus_w)				= nr_seq_grupo_sus_w
	and		nvl(a.nr_seq_subgrupo,nr_seq_subgrupo_sus_w)			= nr_seq_subgrupo_sus_w
	and		nvl(a.nr_seq_derivado, nr_seq_derivado_w)			= nr_seq_derivado_w
	and		a.cd_estabelecimento						= cd_estabelecimento_w
	and		((a.cd_convenio is null) or (a.cd_convenio = cd_convenio_w))
	and		not exists (	select	*
							from	regra_interv_exame_exc x
							where	nvl(x.cd_grupo_proc,nvl(cd_grupo_proc_prescr_w,0)) 		 = nvl(cd_grupo_proc_prescr_w,0)
							and		nvl(x.cd_especialidade,nvl(cd_especialidade_prescr_w,0)) 	 = nvl(cd_especialidade_prescr_w,0)
							and		nvl(x.cd_area_procedimento,nvl(cd_area_procedimento_prescr_w,0)) = nvl(cd_area_procedimento_prescr_w,0)
							and		nvl(x.cd_procedimento,nvl(cd_procedimento_w,0)) 		 = nvl(cd_procedimento_w,0)				
							and		nvl(x.cd_pessoa_fisica, nvl(Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C'),0)) 		 = nvl(Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C'),0)
							and		nvl(x.cd_perfil_excluir, nvl(cd_perfil_p,0))	=	nvl(cd_perfil_p, 0)
							and		x.nr_seq_regra 							 = a.nr_sequencia)
	and		((exists (	select	nr_sequencia
						from	regra_interv_sol_setor
						where	nr_seq_regra	= a.nr_sequencia
						and		nvl(cd_perfil, nvl(cd_perfil_p,0))	=	nvl(cd_perfil_p, 0)
						and		nvl(cd_setor_atendimento,nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
						and		nvl(cd_pessoa_fisica, nvl(Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C'),0))	=	nvl(Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C'),0)))	or  not exists (	select	nr_sequencia
							from	regra_interv_sol_setor
							where	nr_seq_regra	= a.nr_sequencia))
	order by 
		nvl(nr_seq_exame_lab,0),
		nvl(nr_seq_exame_interno,0),
		nvl(nr_seq_grupo_lab,0),
		nvl(cd_procedimento,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0),
		nvl(nr_seq_forma_org,0),
		nvl(nr_seq_subgrupo,0),
		nvl(nr_seq_grupo,0);

BEGIN
begin

select	cd_estabelecimento,
		nr_atendimento,
		obter_convenio_atendimento(nr_atendimento),
		decode(ie_recem_nato,'S',nvl(CD_RECEM_NATO,cd_pessoa_fisica),cd_pessoa_fisica),
		cd_setor_atendimento,
		nvl(ie_recem_nato,'N'),
		dt_validade_prescr,
		dt_inicio_prescr
into	cd_estabelecimento_w,
		nr_atendimento_w,
		cd_convenio_w,
		cd_pessoa_fisica_w,
		cd_setor_atendimento_w,
		ie_recem_nato_w,
		dt_validade_prescr_w,
		dt_inicio_prescr_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;
exception
	when others then
		cd_estabelecimento_w	:= 1;
end;

begin
select	ie_tipo_atendimento
into	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_w;
exception
	when others then
		ie_tipo_atendimento_w	:= null;
end;

open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_exame_w,
	nr_seq_proc_interno_w,
	dt_prev_execucao_w,
	cd_material_exame_w,
	ie_lado_w,
	cd_material_exame_w,
	nr_seq_derivado_w;
exit when C01%notfound;	

	if	(nr_seq_exame_w is not null) then
		begin
		select	nr_seq_grupo
		into	nr_seq_grupo_w
		from	exame_laboratorio
		where	nr_seq_exame	= nr_seq_exame_w;
		exception
			when others then
				nr_seq_grupo_w	:= null;
		end;
	end if;	
	
	select	cd_grupo_proc,
			cd_especialidade,
			cd_area_procedimento
	into	cd_grupo_proc_prescr_w,
			cd_especialidade_prescr_w,
			cd_area_procedimento_prescr_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_w
	and		ie_origem_proced	= ie_origem_proced_w;
	
	/* obter estrutura do procedimento sus unificado*/
	select	nvl(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, 'C', 'F'),'F'),1,10),0),
			nvl(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, 'C', 'G'),'G'),1,10),0),
			nvl(substr(sus_obter_seq_estrut_proc(sus_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, 'C', 'S'),'S'),1,10),0)
	into	nr_seq_forma_org_sus_w,
			nr_seq_grupo_sus_w,
			nr_seq_subgrupo_sus_w
	from	dual;
	
	OPEN C02;
	Loop
	Fetch c02 into	
		qt_hora_w,
		qt_minutos_w,
		nr_seq_exame_lab_w,
		nr_seq_grupo_lab_w,
		nr_seq_exame_interno_w,
		cd_area_procedimento_regra_w,
		cd_especialidade_regra_w,
		cd_grupo_proc_regra_w,
		cd_procedimento_regra_w,
		ie_origem_proced_regra_w,
		ie_lib_prescr_w,
		qt_permitida_w,
		ie_consiste_lado_w,
		nr_seq_regra_w,
		ie_considera_setor_w,
		ie_mes_corrente_w,
		nr_seq_derivado_regra_w,
		ie_considera_validade_w,
		ds_mensagem_regra_w,
		ie_considerar_pessoa_fisica_w;
	EXIT When c02%notfound;
		qt_hora_w	:= qt_hora_w;
		qt_minutos_w	:= qt_minutos_w;
	END Loop;
	CLOSE C02;
	
	qt_horas_p	:= nvl(qt_hora_w,0);
	qt_minutos_p	:= nvl(qt_minutos_w,0);
	
	qt_hora_ww	:= nvl(qt_hora_w,0);
	qt_minutos_ww	:= nvl(qt_minutos_w,0);
	
	if	(qt_minutos_ww = 0) and
		(qt_hora_ww = 0) then
		qt_minutos_ww	:= 999;
		qt_minutos_p	:= 999;
		qt_minutos_w	:= 999;
	end if;	
	
	if	(qt_minutos_w > 0) then
		qt_minutos_w	:= dividir(qt_minutos_w,1440);
	end if;
	
	if	(qt_hora_w > 0) then
		qt_hora_w := ((qt_hora_w * 60) /1440);
	end if;
	
	begin
		dt_limite_minimo_w 	:= dt_prev_execucao_w - qt_hora_w - qt_minutos_w;
	exception when others then
		dt_limite_minimo_w := sysdate;
	end;
	
	if	(Obter_se_setor_intev_proc(cd_setor_atendimento_w, nr_seq_regra_w) = 'S') then

		if	(nr_seq_exame_w is not null) and
			(nr_seq_exame_lab_w is not null) and
			(nr_seq_exame_w = nr_seq_exame_lab_w) then
			begin

			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		b.cd_material_exame	= cd_material_exame_w
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		b.nr_seq_exame		= nr_seq_exame_w
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR		
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));	

			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;

			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		b.cd_material_exame	= cd_material_exame_w
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
				 	 (b.ie_lado	= ie_lado_w))
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p))
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		b.nr_seq_exame		= nr_seq_exame_w;
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;	
				
		elsif	(nr_seq_proc_interno_w is not null) and
			(nr_seq_exame_interno_w is not null) then
			begin

			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		nvl(b.cd_material_exame,0)	= nvl(cd_material_exame_w,0)
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		b.nr_seq_proc_interno	= nr_seq_proc_interno_w
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			

			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;

			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		nvl(b.cd_material_exame,0)	= nvl(cd_material_exame_w,0)
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p))
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		b.nr_seq_proc_interno	= nr_seq_proc_interno_w;
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;
			
		elsif	(nr_seq_exame_w is not null) and
				(nr_seq_grupo_lab_w is not null) then
			begin

			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	exame_laboratorio c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		nvl(b.cd_material_exame,'XPTO')	= nvl(cd_material_exame_w,'XPTO')
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		b.nr_seq_exame		= c.nr_seq_exame
			and		c.nr_seq_grupo		= nr_seq_grupo_lab_w
			and		b.nr_seq_exame		= nr_seq_exame_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;

			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	exame_laboratorio c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		nvl(b.cd_material_exame,'XPTO')	= nvl(cd_material_exame_w,'XPTO')
			and		b.nr_seq_exame		= c.nr_seq_exame
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		c.nr_seq_grupo		= nr_seq_grupo_lab_w
			and		b.nr_seq_exame		= nr_seq_exame_w
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;
			
		elsif	(cd_procedimento_regra_w is not null) and
			(cd_procedimento_w = cd_procedimento_regra_w) and
			(ie_origem_proced_w = ie_origem_proced_regra_w) then
			begin

			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		b.cd_procedimento	= cd_procedimento_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;

			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;
			
		elsif	(nr_seq_derivado_regra_w is not null) and
			(nr_seq_derivado_w is not null) then
			begin

			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.cd_recem_nato,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		b.nr_seq_derivado	= nr_seq_derivado_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;

			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		b.nr_seq_derivado	= nr_seq_derivado_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;	
		
		elsif	(cd_grupo_proc_regra_w is not null) then
			begin

			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	estrutura_procedimento_v c,
				prescr_procedimento b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			--and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		b.cd_procedimento	= c.cd_procedimento
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		b.ie_origem_proced	= c.ie_origem_proced
			and		c.cd_grupo_proc		= cd_grupo_proc_regra_w
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;
			
			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	estrutura_procedimento_v c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			--and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		b.cd_procedimento	= c.cd_procedimento
			and		b.ie_origem_proced	= c.ie_origem_proced
			and		c.cd_grupo_proc		= cd_grupo_proc_regra_w
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;
			
		elsif	(cd_especialidade_regra_w is not null) then
			begin		
			
			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	estrutura_procedimento_v c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		b.cd_procedimento	= c.cd_procedimento
			and		b.ie_origem_proced	= c.ie_origem_proced
			and		c.cd_especialidade	= cd_especialidade_regra_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
				 	 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;
			
			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	estrutura_procedimento_v c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		b.cd_procedimento	= c.cd_procedimento
			and		b.ie_origem_proced	= c.ie_origem_proced
			and		c.cd_especialidade	= cd_especialidade_regra_w
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;
			
		elsif	(cd_area_procedimento_regra_w is not null) then
			begin
			
			select	max(b.dt_prev_execucao)
			into	dt_ultimo_exec_w
			from	estrutura_procedimento_v c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		b.cd_procedimento	= c.cd_procedimento
			and		b.ie_origem_proced	= c.ie_origem_proced
			and		c.cd_area_procedimento	= cd_area_procedimento_regra_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		a.dt_suspensao is null
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			AND		(((a.dt_liberacao IS NOT NULL) OR
					 (a.dt_liberacao_medico IS NOT NULL)) OR
					 (b.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					dt_ultimo_exec_w	:= null;
			end;
			
			begin
			select	sum(b.qt_procedimento)
			into	qt_executada_periodo_w
			from	estrutura_procedimento_v c,
					prescr_procedimento b,
					prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and		decode(ie_recem_nato,'S',nvl(a.CD_RECEM_NATO,a.cd_pessoa_fisica),a.cd_pessoa_fisica)	= cd_pessoa_fisica_w
			and		(((a.nr_atendimento	= nr_atendimento_w) and 
					  (ie_considerar_pessoa_fisica_w = 'N')) or
					 ((a.cd_pessoa_fisica = cd_pessoa_fisica_w) and
					  (ie_considerar_pessoa_fisica_w = 'S')))
			and		((a.nr_prescricao	<> nr_prescricao_p) or
					 (b.nr_sequencia	<> nr_sequencia_p))
			and		b.cd_procedimento	= cd_procedimento_w
			and		b.ie_origem_proced	= ie_origem_proced_w
			and		b.cd_procedimento	= c.cd_procedimento
			and		b.ie_origem_proced	= c.ie_origem_proced
			and		c.cd_area_procedimento	= cd_area_procedimento_regra_w
			and		((nvl(ie_mes_corrente_w,'N') = 'N') or (PKG_DATE_UTILS.start_of(b.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)))
			and		Obter_se_setor_intev_proc(a.cd_setor_atendimento, nr_seq_regra_w) = 'S'
			and		((nvl(ie_considera_setor_w,'N') = 'N') or
					 (a.cd_setor_atendimento = cd_setor_atendimento_w))
			and		((nvl(ie_consiste_lado_w,'N')	= 'N') or
					 (b.ie_lado	= ie_lado_w))
			and		a.dt_suspensao is null
			and		coalesce(a.ie_recem_nato,'N') = ie_recem_nato_w
			and		(((ie_considera_validade_w = 'S') and (b.dt_prev_execucao > dt_inicio_prescr_w)) or
					 ((ie_considera_validade_w = 'N') and (b.dt_prev_execucao between dt_limite_minimo_w and dt_prev_execucao_w)))
			and		nvl(b.ie_suspenso,'N')	<> 'S'
			and		((a.dt_liberacao is not null) or
					 (a.dt_liberacao_medico is not null) or
					 (a.nr_prescricao = nr_prescricao_p AND b.nr_sequencia <> nr_sequencia_p));
			exception
				when others then
					qt_executada_periodo_w	:= 0;
			end;
			
		end if;
	end if;
	
	if	(qt_hora_ww > 0) then
		qt_hora_w := (qt_hora_ww * 60); 
	end if;
	
	
	
	select	nvl(max(b.qt_procedimento),0)
	into	qt_prescricao_cons_w
	from	prescr_procedimento b
	where	b.nr_prescricao	= nr_prescricao_p
	and	b.nr_sequencia	= nr_sequencia_p;	

	qt_executada_periodo_w	:= nvl(qt_executada_periodo_w,0) + nvl(qt_prescricao_cons_w,0);
	
	qt_min_ref_w	:= dividir((qt_hora_w + qt_minutos_ww),1440);	
	
	if	(nr_seq_derivado_w is not null) and
		(qt_permitida_w is not null) and
		(qt_executada_periodo_w > qt_permitida_w) then
		ie_quantidade_p	:= 'S';
	end if;
	
	if	((dt_ultimo_exec_w is not null) and
		(((dt_prev_execucao_w - dt_ultimo_exec_w)) < qt_min_ref_w)) and
		((qt_permitida_w is null) or
		(qt_executada_periodo_w > qt_permitida_w)) then	
		ds_procedimentos_w	:= WHEB_MENSAGEM_PCK.get_texto(457533,'HOURS='|| qt_hora_w ||';MINUTES='|| qt_minutos_w);
	end if;

end loop;
close C01;

if  (dt_ultimo_exec_w is not null) then
	dt_ultima_data_p := dt_ultimo_exec_w;
end if;

if	(ds_procedimentos_w is not null) then
	ds_erro_w	:= ds_procedimentos_w;
	ds_mensagem_data_p := PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_ultimo_exec_w + ((NVL(qt_hora_w,0) + NVL(qt_minutos_ww,0))/1440), 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone);
else
	ds_mensagem_data_p := PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_prev_execucao_w + ((NVL(qt_horas_p,0) * 60) + NVL(qt_minutos_p,0))/1440, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone);
end if;


ds_erro_p	:= substr(ds_erro_w,1,255);
ie_lib_prescr_p		:= ie_lib_prescr_w;
ds_mensagem_regra_p	:= ds_mensagem_regra_w;

END Consistir_interv_solic_exame;
/
