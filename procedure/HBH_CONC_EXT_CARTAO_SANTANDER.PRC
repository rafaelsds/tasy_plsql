create or replace 
procedure HBH_CONC_EXT_CARTAO_SANTANDER(	nr_seq_extrato_arq_p		number,
					ds_lista_extrato_movto_fin_p	varchar2,
					ds_lista_extrato_movto_cred_p	varchar2,
					ie_opcao_p			varchar2,
					nm_usuario_p			varchar2) is

/*	ie_opcao_p

C	Conciliar
D	Desconciliar

*/

nr_seq_parcela_w	number(10);
qt_nao_concil_w		number(10);
vl_parcela_w		number(15,2);

ds_comprovante_w	varchar2(100);
nr_autorizacao_w	varchar2(40);
nr_cartao_w		number(20);
nr_parcela_w		number(10);
nr_resumo_w		varchar2(20);
nr_seq_bandeira_w	number(10);
nr_seq_movto_w		number(10);
nr_seq_parcela_concil_w	number(10);
nr_documento_w		varchar2(100);
cd_bandeira_w		varchar2(3);
vl_saldo_concil_fin_w	number(15,2);
nr_seq_grupo_w		number(10);
vl_min_indevido_w	number(15,2);
vl_max_indevido_w	number(15,2);

cursor	c01 is
select	b.ds_comprovante,
	b.nr_autorizacao,
	somente_numero(b.nr_cartao),
	b.nr_parcela,
	a.nr_resumo,
	a.nr_seq_bandeira,
	b.nr_seq_parcela,
	b.nr_sequencia,
	b.nr_documento,
	c.cd_bandeira,
	b.vl_saldo_concil_fin
from	bandeira_cartao_cr c,
	extrato_cartao_cr_movto b,
	extrato_cartao_cr_res a
where	a.nr_seq_bandeira	= c.nr_sequencia(+)
and	(ds_lista_extrato_movto_fin_p is null or ' ' || ds_lista_extrato_movto_fin_p || ' ' like '% ' || b.nr_sequencia || ' %')
and	nvl(b.ie_pagto_indevido,'N')	= 'N'
and	a.nr_sequencia		= b.nr_seq_extrato_res
and	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

begin

select	max(a.nr_seq_grupo)
into	nr_seq_grupo_w
from	extrato_cartao_cr a,
	extrato_cartao_cr_arq b
where	b.nr_seq_extrato	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_extrato_arq_p;

select	max(a.vl_min_indevido),
	max(a.vl_max_indevido)
into	vl_min_indevido_w,
	vl_max_indevido_w
from	grupo_band_cr_regra a
where	a.nr_seq_grupo	= nr_seq_grupo_w;

open	c01;
loop
fetch	c01 into
	ds_comprovante_w,
	nr_autorizacao_w,
	nr_cartao_w,
	nr_parcela_w,
	nr_resumo_w,
	nr_seq_bandeira_w,
	nr_seq_parcela_concil_w,
	nr_seq_movto_w,
	nr_documento_w,
	cd_bandeira_w,
	vl_saldo_concil_fin_w;
exit	when c01%notfound;

	if	(ie_opcao_p = 'C') and
		(nr_seq_parcela_concil_w is null) then

		select	max(b.nr_sequencia)
		into	nr_seq_parcela_w
		from	bandeira_cartao_cr c,
			movto_cartao_cr_parcela b,
			movto_cartao_cr a
		where	c.cd_bandeira		= nvl(cd_bandeira_w,c.cd_bandeira)
		and	a.nr_seq_bandeira	= c.nr_sequencia
		and	(ds_lista_extrato_movto_cred_p is null or ' ' || ds_lista_extrato_movto_cred_p || ' ' like '% ' || b.nr_sequencia || ' %')
		and	not exists
			(select	1
			from	extrato_cartao_cr_movto x
			where	x.nr_seq_parcela	= b.nr_sequencia)
		and	(nvl(nr_parcela_w,0) = 0 or obter_numero_parcela_cartao(a.nr_sequencia,b.nr_sequencia) = nr_parcela_w)
		and	(b.nr_resumo is null or b.nr_resumo = nvl(nr_resumo_w,b.nr_resumo))
		and	a.nr_sequencia		= b.nr_seq_movto
		and	a.dt_cancelamento	is null
		and 	b.nr_seq_extrato_parcela is null --A parcela nao pode estar conciliada
		and	(a.nr_cartao is null or nvl(somente_numero(substr(a.nr_cartao, -4)),0) = nvl(substr(nr_cartao_w, -4),nvl(somente_numero(substr(a.nr_cartao, -4)),0)))
		and	(ltrim(trim(a.nr_autorizacao),'0')	= ltrim(trim(nvl(nr_autorizacao_w,a.nr_autorizacao)),'0') or
			(ds_comprovante_w is null and nr_documento_w is null) or
			(nvl(a.ds_comprovante,'X') = nvl(ds_comprovante_w,'X')) or
			(nvl(a.ds_comprovante,'X') = nr_documento_w));
			
		if	(nr_seq_parcela_w	is not null) then

			select	max(a.vl_parcela)
			into	vl_parcela_w
			from	movto_cartao_cr_parcela a
			where	a.nr_sequencia	= nr_seq_parcela_w;

			vl_saldo_concil_fin_w	:= nvl(vl_saldo_concil_fin_w,0) - nvl(vl_parcela_w,0);

			if	(nvl(vl_saldo_concil_fin_w,0)	>= nvl(vl_min_indevido_w,0)) or
				(nvl(vl_saldo_concil_fin_w,0)	<= nvl(vl_max_indevido_w,0)) then

				vl_saldo_concil_fin_w	:= 0;

			end if;

			update	extrato_cartao_cr_movto
			set	nr_seq_parcela		= nr_seq_parcela_w,
				vl_saldo_concil_fin	= vl_saldo_concil_fin_w,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_movto_w;

		end if;

	elsif	(ie_opcao_p = 'D') and
		(nr_seq_parcela_concil_w is not null) then

		select	max(a.vl_parcela)
		into	vl_parcela_w
		from	movto_cartao_cr_parcela a
		where	a.nr_sequencia	= nr_seq_parcela_concil_w;

		update	extrato_cartao_cr_movto
		set	nr_seq_parcela		= null,
			vl_saldo_concil_fin	= nvl(vl_saldo_concil_fin,0) + nvl(vl_parcela_w,0),
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_movto_w;

	end if;

end	loop;
close	c01;

select	count(*)
into	qt_nao_concil_w
from	extrato_cartao_cr_movto b,
	extrato_cartao_cr_res a
where	nvl(b.vl_saldo_concil_fin,0) <> 0
and	a.nr_sequencia		= b.nr_seq_extrato_res
and	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

update	extrato_cartao_cr_arq
set	dt_conciliacao		= decode(qt_nao_concil_w,0,sysdate,null),
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_extrato_arq_p;

commit;

end HBH_CONC_EXT_CARTAO_SANTANDER;
/

