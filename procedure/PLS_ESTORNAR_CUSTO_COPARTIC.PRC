create or replace
procedure pls_estornar_custo_copartic
			(	nr_seq_conta_copartic_p	number,
				ds_motivo_p		varchar2,
				ie_acao_p		varchar2,
				nm_usuario_p		Varchar2,
				ie_opcao_p		varchar2) is 

/*
ie_opcao_p
C - Contas m�dicas
R - Recursos de glosa

ie_acao_p
I - Item
T - Total
A - Apropria��o
R - Recalculo de coparticipa��o

ie_opcao_p determina se a a��o � sobre a conta ou sobre o recurso de glosa
ie_acao_p determina de onde veio a a��o. Item � valor individual, Total � a conta toda. Apropria��o � valores de apropria��o do recurso de glosa.
*/
				
qt_registros_w			number(10);
qt_registros_mensalidade_w 	number(10) := 0;
nr_seq_conta_copartic_w		number(10);
nr_seq_conta_w			number(10);
qt_vago_w			number(10);
ds_obervacao_w			varchar2(1000);
nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_mat.nr_sequencia%type;
nr_seq_copartic_contab_w	pls_conta_copartic_contab.nr_sequencia%type;
qt_cta_monitor_w		pls_integer;
nr_seq_regra_limite_copartic_w	pls_conta_coparticipacao.nr_seq_regra_limite_copartic%type;

Cursor C01 (nr_seq_conta_copartic_pc		pls_conta_coparticipacao.nr_sequencia%type) is
	select	a.cd_classif_cred,
		a.cd_classif_cred_provisao,
		a.cd_classif_deb,
		a.cd_classif_deb_provisao,
		a.cd_conta_cred,
		a.cd_conta_cred_provisao,
		a.cd_conta_deb,
		a.cd_conta_deb_provisao,
		a.cd_historico,
		a.cd_historico_provisao,        
		a.ie_ato_cooperado,     
		a.nr_lote_contabil_estorno,
		a.nr_lote_contabil_prov,
		a.nr_seq_conta,
		a.nr_seq_conta_resumo,
		a.nr_seq_esquema,
		a.nr_seq_esquema_prov,
		a.nr_seq_grupo_ans,
		a.nr_seq_prestador_pgto,               
		a.vl_ato_auxiliar * -1 vl_ato_auxiliar,
		a.vl_ato_cooperado * -1 vl_ato_cooperado,
		a.vl_ato_nao_cooperado * -1 vl_ato_nao_cooperado,
		a.vl_coparticipacao * -1 vl_coparticipacao,
		a.vl_provisao * -1 vl_provisao
	from	pls_conta_copartic_contab a
	where	nr_seq_conta_copartic = nr_seq_conta_copartic_pc;
 
begin
select	max(nr_seq_conta),
	max(nr_seq_regra_limite_copartic)
into	nr_seq_conta_w,
	nr_seq_regra_limite_copartic_w
from	pls_conta_coparticipacao
where	nr_sequencia = nr_seq_conta_copartic_p;

-- Verifica se pode realizar o estorno da coparticipa��o
-- considerando se a conta foi enviada no monitoramento TISS
-- conforme a op��o que est� sendo estornada, conta ou recurso
pls_obter_se_conta_monitor(nr_seq_conta_w, ie_opcao_p);

select	count(*)
into	qt_registros_w
from	pls_conta_coparticipacao
where	nr_sequencia	= nr_seq_conta_copartic_p
and	nr_seq_mensalidade_seg is null;

--Se retornou maior que zero, quer dizer que os registros de coparticipa��o encontrados n�o est�o vinculados a uma mensalidade diretamente
--informado no campo nr_seq_mensalidade_seg. Ainda verificamos abaixo se existe algum registro de mensalidade ligado a essa coparticipa��o
if 	(qt_registros_w > 0) then
	select  count(1)
	into 	qt_registros_mensalidade_w
	from  	pls_mensalidade_item_conta a,
			pls_mensalidade_seg_item b,
			pls_mensalidade_segurado c,
			pls_mensalidade d
	where  	b.nr_sequencia = a.nr_seq_item
	and  	c.nr_sequencia = b.nr_seq_mensalidade_seg
	and  	d.nr_sequencia = c.nr_seq_mensalidade
	and  	a.nr_seq_conta_copartic = nr_seq_conta_copartic_p
	and	d.ie_cancelamento is null;
else
	qt_registros_mensalidade_w	:= 0;
end if;

if	((qt_registros_w > 0) and ((qt_registros_mensalidade_w = 0) and (nr_seq_regra_limite_copartic_w is null))) then
	select	pls_conta_coparticipacao_seq.nextval
	into	nr_seq_conta_copartic_w
	from	dual;
	
	insert	into	pls_conta_coparticipacao
		(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_conta, nr_seq_conta_proc, tx_coparticipacao, vl_coparticipacao, nr_seq_regra,
			vl_base_copartic, vl_coparticipacao_unit, ie_calculo_coparticipacao, ie_cobrar_mensalidade,
			nr_seq_regra_exclusao, nr_seq_conta_mat, qt_liberada_copartic,
			ie_estorno_custo, dt_estorno, vl_provisao, ie_origem_regra,ie_status_coparticipacao,
			cd_centro_custo, cd_classif_cred, cd_classif_cred_ant,
			cd_classif_cred_provisao, cd_classif_deb, cd_classif_deb_ant,
			cd_classif_deb_provisao, cd_conta_antec_baixa, cd_conta_cred,
			cd_conta_cred_antecip, cd_conta_cred_provisao, vl_copartic_mens,
			cd_conta_deb, cd_conta_deb_antecip, cd_conta_deb_provisao,
			cd_historico, cd_historico_baixa, cd_historico_provisao,
			cd_procedimento_internacao, cd_sistema_ant, ds_justificativa_canc,
			dt_competencia_mens, dt_fechamento_discussao, dt_mes_competencia,
			ie_ato_cooperado, ie_gerar_mensalidade, ie_glosa,
			ie_origem_copartic, ie_origem_proced_internacao, ie_preco,
			ie_status_mensalidade, ie_tipo_guia, ie_tipo_prestador_atend,
			ie_tipo_prestador_exec, ie_tipo_protocolo, ie_tipo_segurado,
			nr_lote_contabil_estorno, nr_lote_contabil_prov, nr_seq_conta_rec,
			nr_seq_disc_mat, nr_seq_disc_proc, nr_seq_esquema,
			nr_seq_esquema_prev, nr_seq_grupo_ans, nr_seq_mat_rec, 
			nr_seq_mensalidade_seg, nr_seq_motivo_cancel, nr_seq_pagador,
			nr_seq_prestador_atend, nr_seq_prestador_exec, nr_seq_processo_copartic,
			nr_seq_proc_rec, nr_seq_protocolo, nr_seq_rec_futura,
			nr_seq_regra_copartic, nr_seq_regra_ctb_cred, nr_seq_regra_ctb_deb,
			nr_seq_regra_intercambio, nr_seq_regra_limite_copartic, nr_seq_regra_origem,
			nr_seq_regra_preco_copart, nr_seq_segurado, tx_copartic_mens,
			vl_ato_auxiliar, vl_ato_cooperado, vl_ato_nao_cooperado, ds_observacao)
		(select	nr_seq_conta_copartic_w, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_conta, nr_seq_conta_proc, tx_coparticipacao, vl_coparticipacao*-1, nr_seq_regra,
			vl_base_copartic, vl_coparticipacao_unit, ie_calculo_coparticipacao, 'N',
			nr_seq_regra_exclusao, nr_seq_conta_mat, qt_liberada_copartic,
			'S', sysdate, vl_provisao, ie_origem_regra,ie_status_coparticipacao,
			cd_centro_custo, cd_classif_cred, cd_classif_cred_ant,
			cd_classif_cred_provisao, cd_classif_deb, cd_classif_deb_ant,
			cd_classif_deb_provisao, cd_conta_antec_baixa, cd_conta_cred,
			cd_conta_cred_antecip, cd_conta_cred_provisao, vl_copartic_mens*-1,
			cd_conta_deb, cd_conta_deb_antecip, cd_conta_deb_provisao,
			cd_historico, cd_historico_baixa, cd_historico_provisao,
			cd_procedimento_internacao, cd_sistema_ant, ds_justificativa_canc,
			dt_competencia_mens, dt_fechamento_discussao, dt_mes_competencia,
			ie_ato_cooperado, ie_gerar_mensalidade, ie_glosa,
			ie_origem_copartic, ie_origem_proced_internacao, ie_preco,
			ie_status_mensalidade, ie_tipo_guia, ie_tipo_prestador_atend,
			ie_tipo_prestador_exec, ie_tipo_protocolo, ie_tipo_segurado,
			nr_lote_contabil_estorno, nr_lote_contabil_prov, nr_seq_conta_rec,
			nr_seq_disc_mat, nr_seq_disc_proc, nr_seq_esquema,
			nr_seq_esquema_prev, nr_seq_grupo_ans, nr_seq_mat_rec, 
			nr_seq_mensalidade_seg, nr_seq_motivo_cancel, nr_seq_pagador,
			nr_seq_prestador_atend, nr_seq_prestador_exec, nr_seq_processo_copartic,
			nr_seq_proc_rec, nr_seq_protocolo, nr_seq_rec_futura,
			nr_seq_regra_copartic, nr_seq_regra_ctb_cred, nr_seq_regra_ctb_deb,
			nr_seq_regra_intercambio, nr_seq_regra_limite_copartic, nr_seq_regra_origem,
			nr_seq_regra_preco_copart, nr_seq_segurado, tx_copartic_mens,
			vl_ato_auxiliar*-1, vl_ato_cooperado*-1, vl_ato_nao_cooperado*-1,
			ds_observacao
		from	pls_conta_coparticipacao
		where	nr_sequencia	= nr_seq_conta_copartic_p);
	
	select	nr_seq_conta,
		nr_seq_conta_proc,
		nr_seq_conta_mat
	into	nr_seq_conta_w,
		nr_seq_conta_proc_w,
		nr_seq_conta_mat_w
	from	pls_conta_coparticipacao
	where	nr_sequencia	= nr_seq_conta_copartic_p;
	
	if	(ie_acao_p = 'I') then
		ds_obervacao_w := 	'Estorno realizado atrav�s da funcionalidade ''BD - Estornar custo''';
	elsif	(ie_acao_p = 'T') then
		ds_obervacao_w := 	'Estorno realizado atrav�s da funcionalidade ''BD - Estornar valores coparticipa��o conta''';
	elsif	(ie_acao_p = 'A') then
		ds_obervacao_w := 	'Estorno realizado atrav�s da funcionalidade ''BD - Estornar valores de apropria��o''';
	elsif 	(ie_acao_p = 'R') then
		ds_obervacao_w := 	'Estorno realizado atrav�s da aplica��o do rec�lculo de coparticipa��o';
	end if;
	
	-- Lan�a um registro de log para o item estornado
	insert	into	pls_conta_copartic_log
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_conta_copartic,nr_seq_conta,nr_seq_conta_proc,nr_seq_conta_mat,vl_coparticipacao,
				vl_coparticipacao_old,nr_seq_mensalidade_seg,nr_seq_mensalidade_seg_old,ie_status_mensalidade,ie_status_mensalidade_old,
				ie_status_coparticipacao,ie_status_coparticipacao_old,ie_gerar_mensalidade,ie_gerar_mensalidade_old,
				nm_maquina,ds_log_call,ds_log,ds_motivo)
		values	(	pls_conta_copartic_log_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				nr_seq_conta_copartic_p,nr_seq_conta_w,nr_seq_conta_proc_w,nr_seq_conta_mat_w,null,
				null,null,null,null,null,
				null,null,null,null,
				wheb_usuario_pck.get_machine,null,ds_obervacao_w,ds_motivo_p);
	
	-- Lan�a registro de log para o item de estorno (valor negativo)
	insert	into	pls_conta_copartic_log
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_conta_copartic,nr_seq_conta,nr_seq_conta_proc,nr_seq_conta_mat,vl_coparticipacao,
				vl_coparticipacao_old,nr_seq_mensalidade_seg,nr_seq_mensalidade_seg_old,ie_status_mensalidade,ie_status_mensalidade_old,
				ie_status_coparticipacao,ie_status_coparticipacao_old,ie_gerar_mensalidade,ie_gerar_mensalidade_old,
				nm_maquina,ds_log_call,ds_log,ds_motivo)
		values	(	pls_conta_copartic_log_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				nr_seq_conta_copartic_w,nr_seq_conta_w,nr_seq_conta_proc_w,nr_seq_conta_mat_w,null,
				null,null,null,null,null,
				null,null,null,null,
				wheb_usuario_pck.get_machine,null,ds_obervacao_w,ds_motivo_p);
	
	--Gera estornos paraa apropria��o copartic caso tiver
	insert into pls_conta_copartic_aprop(	nr_sequencia, nr_seq_conta_coparticipacao, nr_seq_centro_apropriacao, 
						vl_apropriacao, dt_atualizacao, dt_atualizacao_nrec, 
						nm_usuario_nrec, nm_usuario)
					(select	pls_conta_copartic_aprop_seq.nextval, nr_seq_conta_copartic_w, nr_seq_centro_apropriacao,
						vl_apropriacao * -1, dt_atualizacao, dt_atualizacao_nrec, nm_usuario_p, nm_usuario_p
					from	pls_conta_copartic_aprop
					where	nr_seq_conta_coparticipacao = nr_seq_conta_copartic_p);
	
	for r_C01_w in C01 (nr_seq_conta_copartic_p) loop
	
		select	pls_conta_copartic_contab_seq.nextval
		into	nr_seq_copartic_contab_w
		from	dual;
	
		insert	into	pls_conta_copartic_contab
				(	cd_classif_cred, cd_classif_cred_provisao, cd_classif_deb,
					cd_classif_deb_provisao, cd_conta_cred, cd_conta_cred_provisao,
					cd_conta_deb, cd_conta_deb_provisao, cd_historico,
					cd_historico_provisao, dt_atualizacao, dt_atualizacao_nrec,
					ie_ato_cooperado, nm_usuario, nm_usuario_nrec,
					nr_lote_contabil_estorno, nr_lote_contabil_prov, nr_seq_conta,
					nr_seq_conta_copartic, nr_seq_conta_resumo, nr_seq_esquema,
					nr_seq_esquema_prov, nr_seq_grupo_ans, nr_seq_prestador_pgto,
					nr_sequencia, vl_ato_auxiliar, vl_ato_cooperado,
					vl_ato_nao_cooperado, vl_coparticipacao, vl_provisao,
					dt_mes_competencia)
			values (	r_C01_w.cd_classif_cred, r_C01_w.cd_classif_cred_provisao, r_C01_w.cd_classif_deb,
					r_C01_w.cd_classif_deb_provisao, r_C01_w.cd_conta_cred, r_C01_w.cd_conta_cred_provisao,
					r_C01_w.cd_conta_deb, r_C01_w.cd_conta_deb_provisao, r_C01_w.cd_historico,
					r_C01_w.cd_historico_provisao, sysdate, sysdate,
					r_C01_w.ie_ato_cooperado, nm_usuario_p, nm_usuario_p,
					r_C01_w.nr_lote_contabil_estorno, r_C01_w.nr_lote_contabil_prov, r_C01_w.nr_seq_conta,
					nr_seq_conta_copartic_w, r_C01_w.nr_seq_conta_resumo, r_C01_w.nr_seq_esquema,
					r_C01_w.nr_seq_esquema_prov, r_C01_w.nr_seq_grupo_ans, r_C01_w.nr_seq_prestador_pgto,
					nr_seq_copartic_contab_w, r_C01_w.vl_ato_auxiliar, r_C01_w.vl_ato_cooperado,
					r_C01_w.vl_ato_nao_cooperado, r_C01_w.vl_coparticipacao, r_C01_w.vl_provisao,
					sysdate); -- Dt_mes_competencia recebe sysdate segundo orienta��o Marcio Ropelato
	end loop;
		
	pls_atualizar_codificacao_pck.pls_atualizar_codificacao(trunc(sysdate,'month'));
	ctb_pls_atualizar_prov_copart(nr_seq_conta_w, nr_seq_conta_copartic_w, null, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, qt_vago_w);
	
	update	pls_conta_coparticipacao
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_cobrar_mensalidade	= 'N',
		dt_estorno		= sysdate,
		ie_estorno_custo	= 'S'
	where	nr_sequencia		= nr_seq_conta_copartic_p;
	
end if;

commit;

end pls_estornar_custo_copartic;
/
