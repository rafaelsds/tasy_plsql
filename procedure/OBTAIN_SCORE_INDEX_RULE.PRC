create or replace procedure obtain_score_index_rule(    nr_atendimento_p    in number,
                    nr_tree_node_p      in number,
                    nr_seq_grupo_p      in number,
                    ie_escala_p     in varchar2,
                    ie_readonly_p       out nocopy varchar2) is
nr_seq_group_scl_w  grupo_escala_indice_item.nr_sequencia%type;
ie_blocked_enc_w            varchar2(1) := 'S';
begin
    ie_blocked_enc_w := Obter_se_bloqueio_atendimento(nr_atendimento_p);
    
    if (ie_blocked_enc_w = 'S') then
        ie_readonly_p := 'N';
    end if;
    if (nr_tree_node_p = 374) then
        select  nvl(max(ie_atualizar),'S')
        into    ie_readonly_p
        from    perfil_escala_indice b
        where   b.nr_seq_grupo  = nr_seq_grupo_p
        and b.cd_perfil = wheb_usuario_pck.get_cd_perfil;
        if (ie_readonly_p = 'S') then
            select  max(nr_sequencia)
            into    nr_seq_group_scl_w
            from    grupo_escala_indice_item
            where   nr_seq_grupo = nr_seq_grupo_p
            and     ie_escala = ie_escala_p;
            Obter_se_regra_item_escala(nr_seq_group_scl_w, ie_readonly_p);

        end if;
    else
        ie_readonly_p := 'S';
    end if;
end obtain_score_index_rule;
/
