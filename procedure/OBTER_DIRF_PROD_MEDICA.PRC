create or replace
procedure obter_dirf_prod_medica(	nr_sequencia_p			number,
					cd_estabelecimento_p 		number,	
					cd_tributo_p			number,
					cd_darf_p			varchar2,
					ie_tipo_data_p			number,
					ie_consistir_retencao_p		varchar2,
					dt_mes_referencia_p		date,
					ie_trib_titulo_p		varchar2,
					cd_empresa_p			number,
					ie_pf_pj_p			varchar2) is 
				
nr_seq_pagamento_w	pls_pagamento_item.nr_seq_pagamento%type;	
nr_titulo_w		titulo_pagar.nr_titulo%type;
vl_rendimento_w		pls_pag_prest_venc_trib.vl_base_calculo%type;
vl_imposto_w		pls_pag_prest_venc_trib.vl_imposto%type;
cd_pessoa_fisica_w	pls_prestador.cd_pessoa_fisica%type;
cd_cgc_w		pls_prestador.cd_cgc%type;
dt_vencimento_w		pls_pag_prest_vencimento.dt_vencimento%type;
dt_mes_competencia_w	pls_lote_pagamento.dt_mes_competencia%type;
ie_lote_pagamento_w	varchar2(5);

ds_union_w varchar2(3);
nr_titulo_fechamento_w varchar(255);

cursor C01 is
	select 	p.nr_sequencia,
		z.cd_pessoa_fisica,
		z.cd_cgc,
		decode(upper(ie_pf_pj_p), 'PF', 
			decode(ie_tipo_data_p, 1, l.dt_mes_competencia, 2, l.dt_mes_competencia, 3, l.dt_venc_lote, 4, l.dt_mes_competencia, 5, l.dt_venc_lote, 6, l.dt_venc_lote), 
			decode(cd_darf_p, '1708', l.dt_mes_competencia, 
			decode(ie_tipo_data_p, 1, l.dt_mes_competencia, 2, l.dt_mes_competencia, 3, l.dt_venc_lote, 4, l.dt_mes_competencia, 5, l.dt_venc_lote, 6, l.dt_venc_lote))) dt_mes_competencia,
		'A' ie_lote_pagamento
	from   	pls_lote_pagamento l,
		pls_pagamento_prestador p,
		pls_prestador z
	where  	l.nr_sequencia	= p.nr_seq_lote 
	and	z.nr_sequencia	= p.nr_seq_prestador 
	and	((l.cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento_p is null))
	and 	p.ie_cancelamento is null
	and 	((	select	max(y.vl_liquido) 
			from	pls_pag_prest_vencimento y 
			where	y.nr_seq_pag_prestador = p.nr_sequencia) <= 0)
	and 	decode(upper(ie_pf_pj_p), 'PF', 
			decode(ie_tipo_data_p, 1, fim_mes(l.dt_mes_competencia), 2, fim_mes(l.dt_mes_competencia), 3, fim_mes(l.dt_venc_lote), 4, fim_mes(l.dt_mes_competencia), 5, fim_mes(l.dt_venc_lote), 6, fim_mes(l.dt_venc_lote)), 
			decode(cd_darf_p, '1708', fim_mes(l.dt_mes_competencia), 
			decode(ie_tipo_data_p, 1, fim_mes(l.dt_mes_competencia), 2, fim_mes(l.dt_mes_competencia), 3, fim_mes(l.dt_venc_lote), 4, fim_mes(l.dt_mes_competencia), 5, fim_mes(l.dt_venc_lote), 6, fim_mes(l.dt_venc_lote)))) = fim_mes(dt_mes_referencia_p)
	and	decode(upper(ie_pf_pj_p), 'PF', z.cd_pessoa_fisica, z.cd_cgc) is not null
	union all
	select 	max(p.nr_sequencia),
		z.cd_pessoa_fisica,
		z.cd_cgc,
		trunc(
		decode(upper(ie_pf_pj_p), 'PF', 
			decode(ie_tipo_data_p, 1, l.dt_mes_competencia, 2, l.dt_mes_competencia, 3, l.dt_vencimento_lote, 4, l.dt_mes_competencia, 5, l.dt_vencimento_lote, 6, l.dt_vencimento_lote), 
			decode(cd_darf_p, '1708', l.dt_mes_competencia, 
			decode(ie_tipo_data_p, 1, l.dt_mes_competencia, 2, l.dt_mes_competencia, 3, l.dt_vencimento_lote, 4, l.dt_mes_competencia, 5, l.dt_vencimento_lote, 6, l.dt_vencimento_lote))), 'month') dt_mes_competencia,
		'N' ie_lote_pagamento
	from   	pls_pp_lote l,
		pls_pp_prestador p,
		pls_prestador z
	where  	l.nr_sequencia	= p.nr_seq_lote 
	and	z.nr_sequencia	= p.nr_seq_prestador 
	and	((l.cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento_p is null))
	and 	p.ie_cancelado	= 'N'
	and	l.dt_fechamento is not null
	and	p.vl_liquido < 0
	and 	decode(upper(ie_pf_pj_p), 'PF', 
			decode(ie_tipo_data_p, 1, fim_mes(l.dt_mes_competencia), 2, fim_mes(l.dt_mes_competencia), 3, fim_mes(l.dt_vencimento_lote), 4, fim_mes(l.dt_mes_competencia), 5, fim_mes(l.dt_vencimento_lote), 6, fim_mes(l.dt_vencimento_lote)), 
			decode(cd_darf_p, '1708', fim_mes(l.dt_mes_competencia), 
			decode(ie_tipo_data_p, 1, fim_mes(l.dt_mes_competencia), 2, fim_mes(l.dt_mes_competencia), 3, fim_mes(l.dt_vencimento_lote), 4, fim_mes(l.dt_mes_competencia), 5, fim_mes(l.dt_vencimento_lote), 6, fim_mes(l.dt_vencimento_lote)))) = fim_mes(dt_mes_referencia_p)
	and	decode(upper(ie_pf_pj_p), 'PF', z.cd_pessoa_fisica, z.cd_cgc) is not null
	and	exists	(select	1
			from	pls_pp_valor_trib_pessoa t
			where	t.nr_seq_lote		= p.nr_seq_lote
			and	t.nr_seq_prestador	= p.nr_seq_prestador)
	group by z.cd_pessoa_fisica,
		z.cd_cgc,
		trunc(	decode(upper(ie_pf_pj_p), 'PF', 
			decode(ie_tipo_data_p, 1, l.dt_mes_competencia, 2, l.dt_mes_competencia, 3, l.dt_vencimento_lote, 4, l.dt_mes_competencia, 5, l.dt_vencimento_lote, 6, l.dt_vencimento_lote), 
			decode(cd_darf_p, '1708', l.dt_mes_competencia, 
			decode(ie_tipo_data_p, 1, l.dt_mes_competencia, 2, l.dt_mes_competencia, 3, l.dt_vencimento_lote, 4, l.dt_mes_competencia, 5, l.dt_vencimento_lote, 6, l.dt_vencimento_lote))), 'month');
			
cursor C02 is	
	select 	obter_ds_titulo_pagar((	select	max(a.nr_sequencia) 
					from 	pls_pag_prest_venc_trib a 
					where 	a.nr_seq_vencimento = v.nr_sequencia
					and 	obter_ds_titulo_pagar(a.nr_sequencia) > 0)) nr_titulo,
		(nvl(t.vl_base_calculo, 0) - nvl(t.vl_base_adic, 0) + (	select 	nvl(sum(p.vl_imposto), 0) vl_imposto 
									from 	pls_pag_prest_vencimento x, 
										pls_pag_prest_venc_trib p, 
										tributo w 
									where 	x.nr_sequencia = p.nr_seq_vencimento 
									and 	x.nr_seq_pag_prestador = nr_seq_pagamento_w
									and 	obter_ds_titulo_pagar(p.nr_sequencia) > 0 
									and 	w.cd_tributo = p.cd_tributo 
									and 	w.ie_tipo_tributo = 'INSS' 
									and 	w.cd_tributo <> t.cd_tributo) + nvl(t.vl_desc_base , 0)) vl_rendimento, 
		nvl(t.vl_imposto, 0) vl_imposto	,
		'S1' ds_union 
	from   	pls_pag_prest_vencimento v,
		pls_pag_prest_venc_trib t
	where  	v.nr_sequencia		= t.nr_seq_vencimento
	and	v.nr_seq_pag_prestador	= nr_seq_pagamento_w
	and	t.cd_tributo		= cd_tributo_p
	and 	v.dt_vencimento		<= fim_ano(dt_mes_referencia_p)
	and 	t.cd_darf		= cd_darf_p
	and 	obter_ds_titulo_pagar(t.nr_sequencia) > 0
	and	ie_lote_pagamento_w	= 'A'
	union all
	select 	nvl(pls_pp_tributacao_pck.obter_tit_pag_trib(v.nr_sequencia), nr_titulo_fechamento_w) nr_titulo,
		(nvl(v.vl_base_calculo, 0) - nvl(v.vl_base_adic, 0) + (	select 	nvl(sum(p.vl_tributo),0) vl_imposto 
									from 	pls_pp_valor_trib_pessoa p
									where 	p.nr_seq_prestador	= t.nr_seq_prestador
									and	p.nr_seq_lote		= t.nr_seq_lote
									and 	pls_pp_tributacao_pck.obter_tit_pag_trib(p.nr_sequencia) > 0 
									and 	p.ie_tipo_tributo = 'INSS' 
									and 	p.cd_tributo <> v.cd_tributo)) vl_rendimento, 
		nvl(v.vl_tributo, 0) vl_imposto,
		'S2' ds_union
	from   	pls_pp_valor_trib_pessoa v,
		pls_pp_prestador t
	where	t.nr_seq_prestador	= v.nr_seq_prestador
	and	t.nr_seq_lote		= v.nr_seq_lote
	and	t.nr_sequencia		= nr_seq_pagamento_w
	and	v.cd_tributo		= cd_tributo_p
	and	t.dt_venc_titulo	<= fim_ano(dt_mes_referencia_p)
	and 	v.cd_darf		= cd_darf_p
	and 	(pls_pp_tributacao_pck.obter_tit_pag_trib(v.nr_sequencia) > 0
	or 	nr_titulo_fechamento_w > 0)
	and	ie_lote_pagamento_w	= 'N';
	
begin
	
open C01;
loop
fetch C01 into	
	nr_seq_pagamento_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	dt_mes_competencia_w,
	ie_lote_pagamento_w;
exit when C01%notfound;
	begin		
	
	select	to_char(nvl(max(b.nr_titulo_pagar), 0)) nr_titulo
	into	nr_titulo_fechamento_w
	from	pls_pp_lr_trib_pessoa b,
			pls_pp_lr_lote a
	where	a.nr_sequencia = b.nr_seq_lote_ret
	and		b.cd_tributo = cd_tributo_p
	and		trunc(a.dt_mes_competencia, 'mm') = trunc(dt_mes_referencia_p, 'mm')
	and 	b.cd_darf = cd_darf_p
	and		nvl(b.nr_titulo_pagar, 0) > 0
	and exists(	select	1
				from   	pls_pp_valor_trib_pessoa v,
					pls_pp_prestador t
				where	t.nr_seq_prestador		= v.nr_seq_prestador
				and	t.nr_seq_lote			= v.nr_seq_lote
				and	t.nr_sequencia			= nr_seq_pagamento_w
				and	v.cd_tributo 			= b.cd_tributo
				and	t.dt_venc_titulo		<= fim_ano(dt_mes_referencia_p)
				and	v.cd_darf			= b.cd_darf
				and 	nvl(pls_pp_tributacao_pck.obter_tit_pag_trib(v.nr_sequencia), 0) = 0
				and	ie_lote_pagamento_w		= 'N'
				and	v.cd_pessoa_fisica 		= b.cd_pessoa_fisica);
	
	open C02;
	loop
	fetch C02 into	
		nr_titulo_w,
		vl_rendimento_w,
		vl_imposto_w,
		ds_union_w;
	exit when C02%notfound;
		begin	
		insert into dirf_titulo_pagar
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_lote_dirf,
			nr_titulo,				vl_rendimento,			vl_imposto,
			cd_tributo,				cd_darf,			dt_base_titulo,
			ie_origem,				cd_pessoa_fisica,		cd_cgc)
		values	(dirf_titulo_pagar_seq.Nextval,		sysdate,			'Tasy3',
			sysdate,				'Tasy3',			nr_sequencia_p,
			nr_titulo_w,				vl_rendimento_w,		vl_imposto_w,
			cd_tributo_p,				cd_darf_p,			dt_mes_competencia_w,
			nvl(ds_union_w, 'SP'),			cd_pessoa_fisica_w,		cd_cgc_w);
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

commit;

end obter_dirf_prod_medica;
/