create or replace
procedure delete_pep_item_pend_orfaos is

begin
BEGIN
	delete	from	pep_item_pendente a 
	WHERE	ie_tipo_registro in ('ADTH', 'ADHE', 'ADMD', 'ADAA')
	AND		ie_tipo_pendencia = 'L' 
	AND		nr_seq_registro NOT IN (SELECT nr_sequencia FROM CIRURGIA_AGENTE_ANEST_OCOR);

	commit;
exception
when others then
	commit;
END;

end delete_pep_item_pend_orfaos;
/