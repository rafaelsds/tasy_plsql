create or replace
procedure	HTML_PARAM_TO_RULE_CONVERTER(
			nr_seq_lote_p		number,
			nm_usuario_p		varchar2) is

ie_tipo_acao_w			html_param_to_rule_action.ie_tipo_acao%type;
nr_seq_acao_w			html_param_to_rule_action.nr_sequencia%type;
nr_seq_parametro_w			funcao_parametro.nr_sequencia%type;
vl_parametro_w			funcao_parametro.vl_parametro%type;
ie_tipo_param_w			valor_dominio.vl_dominio%type;
qt_existe_param_w		number(10);
nr_seq_funcao_lote_w	lote_par_regra_html_funcao.nr_sequencia%type;

cd_funcao_w				funcao.cd_funcao%type;

cursor	c00 is
select	cd_funcao,
		nr_sequencia
from	lote_par_regra_html_funcao
where	nr_seq_lote = nr_seq_lote_p;

cursor	c01 is
select	nr_seq_parametro
from	lote_par_regra_html_item
where	cd_funcao = cd_funcao_w
and		nr_seq_funcao_lote = nr_seq_funcao_lote_w;

cursor	c02 is
select	b.nr_sequencia,
		b.ie_tipo_acao
from	html_param_to_rule_action b,
		html_param_to_rule a
where	b.nr_seq_rule = a.nr_sequencia
and		a.cd_funcao = cd_funcao_w
and		a.nr_seq_parametro = nr_seq_parametro_w
and		a.dt_liberacao is not null
and		a.dt_inativacao is null
order by b.nr_seq_rule;

begin

open c00;
loop
fetch c00 into
	cd_funcao_w,
	nr_seq_funcao_lote_w;
exit when c00%notfound;
	begin
	
	open c01;
	loop
	fetch c01 into
		nr_seq_parametro_w;
	exit when c01%notfound;
		begin
		
		/* A consulta abaixo verifica se existe algum valor cadastrado
		na base do cliente, para o parametro que ser� convertido */

		select	sum(qt)
		into	qt_existe_param_w
		from	(
			select	count(*) qt
			from	funcao_parametro
			where	cd_funcao = cd_funcao_w
			and	nr_sequencia = nr_seq_parametro_w
			and	vl_parametro is not null
			union all
			select	count(*) qt
			from	funcao_param_estab
			where	cd_funcao = cd_funcao_w
			and	nr_seq_param = nr_seq_parametro_w
			and	vl_parametro is not null
			union all
			select	count(*) qt
			from	funcao_param_perfil
			where	cd_funcao = cd_funcao_w
			and	nr_sequencia = nr_seq_parametro_w
			and	vl_parametro is not null
			union all
			select	count(*) qt
			from	funcao_param_usuario
			where	cd_funcao = cd_funcao_w
			and	nr_sequencia = nr_seq_parametro_w
			and	vl_parametro is not null);
			
		if	(qt_existe_param_w > 0) then
			begin
			
			open c02;
			loop
			fetch c02 into
				nr_seq_acao_w,
				ie_tipo_acao_w;
			exit when c02%notfound;
				begin
				
				if	(ie_tipo_acao_w = 'EXTERNO') or
					(ie_tipo_acao_w = 'TAB') then
					/* Par�metro de controle de visibilidade acesso externo e TAB*/
					HTML_CREATE_RULE_OBJ_SCHEM(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);

				elsif	(ie_tipo_acao_w = 'FILTRO') then
					/* Par�metro de controle de filtros */
					HTML_CREATE_RULE_WFILTRO(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);
	
				elsif	(ie_tipo_acao_w = 'BD') then
					/* Par�metro de controle de item popup */
					HTML_CREATE_RULE_FUNC_POPUP(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);
	
				elsif	(ie_tipo_acao_w = 'CRUD') then
					/* Par�metro de CRUD */
					HTML_CREATE_RULE_CRUD_PARAM(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);

				elsif	(ie_tipo_acao_w = 'TAB_INICIO') then
					/* Par�metro de pasta padr�o do navegador */
					HTML_CREATE_RULE_SCHEM_ORDER(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);

				elsif	(ie_tipo_acao_w = 'ATRIB') then
					/* Par�metro de regra de atributo */
					HTML_CREATE_RULE_ATRIB_REGRA(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);

				elsif	(ie_tipo_acao_w = 'ARQUIVO') then
					/* Par�metro de reposit�rio de arquivo */
					HTML_CREATE_RULE_REPOSITORY(nr_seq_lote_p,nr_seq_acao_w,nm_usuario_p);
				end if;
				
				end;
			end loop;
			close c02;
			
			end;
		end if;
		
		end;
	end loop;
	close c01;
	
	end;
end loop;
close c00;

end HTML_PARAM_TO_RULE_CONVERTER;
/