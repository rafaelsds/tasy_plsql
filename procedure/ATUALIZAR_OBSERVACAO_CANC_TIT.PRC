create or replace
procedure atualizar_observacao_canc_tit(
		nr_titulo_p	number,
		ds_observacao_p varchar2) is 
begin
if	(nr_titulo_p is not null) then
	begin
	update   titulo_pagar a
	set      a.ds_observacao_titulo   = a.ds_observacao_titulo || ds_observacao_p
	where    a.nr_titulo       = nr_titulo_p;
	commit;
	end;
end if;
end atualizar_observacao_canc_tit;
/