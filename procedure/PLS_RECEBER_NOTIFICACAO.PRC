create or replace
procedure pls_receber_notificacao
			(	nr_seq_notific_pagador_p	number,
				dt_recebimento_p		date,
				nm_pessoa_recebimento_p		varchar2,
				nr_seq_vinculo_p		number,
				nm_usuario_p			varchar2,
				ie_tipo_notificacao_p		varchar2,
				ds_erro_p		out	varchar2) is

ds_erro_w			varchar2(255)  := '';
dt_envio_lote_w			date;
dt_limite_recebimento_w		date;
ie_status_w			pls_notificacao_pagador.ie_status%type;

begin
select	max(dt_envio),
	max(dt_limite_recebimento)
into	dt_envio_lote_w,
	dt_limite_recebimento_w
from	pls_notificacao_pagador	a,
	pls_notificacao_lote	b
where	b.nr_sequencia	= a.nr_seq_lote
and	a.nr_sequencia	= nr_seq_notific_pagador_p;

if	(dt_limite_recebimento_w is not null) and
	(dt_recebimento_p > dt_limite_recebimento_w) then
	ie_status_w	:= 'F';
else
	ie_status_w	:= null;
end if;

if	(dt_envio_lote_w is null) then
	ds_erro_w  := ds_erro_w || nr_seq_notific_pagador_p;
else
	update  pls_notificacao_pagador
	set	dt_recebimento_notif	= dt_recebimento_p,
		nm_pessoa_notific	= nm_pessoa_recebimento_p,
		nr_seq_vinculo_pagador	= decode(nvl(nr_seq_vinculo_p, 0), 0, null, nr_seq_vinculo_p),
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		ie_status_ant		= ie_status,
		ie_status		= nvl(ie_status_w, decode(ie_status, 'C', 'C', 'P', 'P', 'R')),
		ie_tipo_notificacao	= ie_tipo_notificacao_p
	where	nr_sequencia		= nr_seq_notific_pagador_p;
	
	--Utilizado no HTML5
	pls_excluir_barras_pag_notific(nr_seq_notific_pagador_p, nm_usuario_p);
end if;

ds_erro_p  := ds_erro_w;

commit;

end pls_receber_notificacao;
/
