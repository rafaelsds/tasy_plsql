create or replace
procedure gerar_ordem_resposta_cotacao(	nr_cot_compra_p		number,
					nm_usuario_p		varchar2) is 
					
qt_existe_w				number(10);	
ds_erro_w				varchar2(255);
ie_tipo_integracao_envio_w			varchar2(1);
ie_gerar_oc_integracao_w			varchar2(1);
cd_estabelecimento_w			number(5);
ie_gera_nova_cotacao_w		varchar2(1);
cd_perfil_ativo_w		number(10);
ie_resposta_int_compra_w	parametro_compras.ie_resposta_int_compra%type;
dt_aprov_oc_w			ordem_compra.dt_aprovacao%type;


begin

cd_perfil_ativo_w	:= obter_perfil_ativo;

select	count(*)
into	qt_existe_w
from	cot_compra_forn
where	nr_cot_compra = nr_cot_compra_p;

if	(qt_existe_w > 0) then

	select	nvl(max(ie_tipo_integracao_envio),0),
			nvl(max(cd_estabelecimento),0)
	into	ie_tipo_integracao_envio_w,
			cd_estabelecimento_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_p;
	
	select	NVL(obter_valor_param_usuario(915, 46, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w),'N')
	into	ie_gera_nova_cotacao_w
	from	dual;	

	select	nvl(max(ie_gerar_oc_integracao),'S'),
		nvl(max(ie_resposta_int_compra),'N')		
	into	ie_gerar_oc_integracao_w,
		ie_resposta_int_compra_w
	from	parametro_compras
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	((ie_tipo_integracao_envio_w = 2) and
		(ie_gerar_oc_integracao_w = 'S')) or
		(ie_tipo_integracao_envio_w = 3) then
			
		begin
		calcular_cot_compra_liquida(nr_cot_compra_p, nm_usuario_p);
		gerar_cot_compra_resumo(nr_cot_compra_p, nm_usuario_p);
		gerar_historico_cotacao(
					nr_cot_compra_p,
					Wheb_mensagem_pck.get_Texto(301961), /*'C�lculo da cota��o efetuado com sucesso ao resgatar a resposta via WebService.',*/
					Wheb_mensagem_pck.get_Texto(301962), /*'O c�lculo desta cota��o foi efetuado com sucesso no momento de obter a resposta da plataforma via WebService.',*/
					'S',
					nm_usuario_p);
				
		exception when others then
		ds_erro_w := Wheb_mensagem_pck.get_Texto(301963); /*'Erro ao calcular a cota��o.';*/
		gerar_historico_cotacao(
				nr_cot_compra_p,
				Wheb_mensagem_pck.get_Texto(301964), /*Erro ao calcular a cota��o ao resgatar a resposta via WebService.*/
				Wheb_mensagem_pck.get_Texto(301965), /*'Ocorreu um erro ao calcular a esta cota��o no momento de obter a resposta da plataforma via WebService.',*/
				'S',
				nm_usuario_p);
		end;	
				
		if	(ds_erro_w is null) then
					
			begin
			gerar_OC_agend_integracao(nr_cot_compra_p, nm_usuario_p);
			gerar_historico_cotacao(
				nr_cot_compra_p,
				Wheb_mensagem_pck.get_Texto(301966), /*'Gera��o da ordem efetuada com sucesso ao resgatar a resposta via WebService.',*/
				Wheb_mensagem_pck.get_Texto(301968), /*'A gera��o da ordem de compra desta cota��o foi efetuada com sucesso no momento de obter a resposta da plataforma via WebService.',*/
				'S',
				nm_usuario_p);
			
			update	cot_compra
			set	dt_geracao_ordem_compra = sysdate
			where	nr_cot_compra = nr_cot_compra_p;

			if	(ie_tipo_integracao_envio_w = 2) then
				begin
				/* Resposta integracao apos gerar ordem */
				if (ie_resposta_int_compra_w = 'S') then
					begin
					gravar_agenda_integ_webservice(96,nr_cot_compra_p,'C');
					end;
				/* Resposta integracao apor aprovar ordem */
				elsif (ie_resposta_int_compra_w = 'P') then
					begin
					/* Se alguma OC gerada foi aprovada, faz envio para integracao*/
					select max(a.dt_aprovacao)
					into dt_aprov_oc_w
					from ordem_compra a, 
						ordem_compra_item b 
					where a.nr_ordem_compra = b.nr_ordem_compra 
					and b.nr_cot_compra = nr_cot_compra_p;
					
					if (dt_aprov_oc_w is not null) then
						begin
						gravar_agenda_integ_webservice(96,nr_cot_compra_p,'C');
						end;
					end if;
					
					end;
				end if;
				end;
			end if;
			
			if 	(ie_gera_nova_cotacao_w <> 'N') then				
				sup_exclui_item_nao_cotado(nr_cot_compra_p,'S',nm_usuario_p);
			end if;
			
			exception when others then
			gerar_historico_cotacao(
				nr_cot_compra_p,
				Wheb_mensagem_pck.get_Texto(301969), /*'Erro ao calcular a cota��o ao resgatar a resposta via WebService',*/
				Wheb_mensagem_pck.get_Texto(301970), /*'Ocorreu um erro ao gerar a ordem de compra desta cota��o no momento de obter a resposta da plataforma via WebService',*/
				'S',
				nm_usuario_p);				
			end;
		end if;
	end if;
end if;

commit;

end gerar_ordem_resposta_cotacao;
/