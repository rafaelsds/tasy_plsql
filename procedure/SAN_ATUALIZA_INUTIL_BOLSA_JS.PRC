create or replace
procedure san_atualiza_inutil_bolsa_js (	nr_seq_producao_p	varchar2,
						nr_seq_inutil_p		number,
						cd_estabelecimento_p 	number) is 
nr_sequencia_w		number(10,0);
nr_sequencias_w		varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_seq_producao_w	number(10);

begin

if	( nr_seq_producao_p       is not null ) then
	begin
	nr_sequencias_w	:= nr_seq_producao_p;
	while (nr_sequencias_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(nr_sequencias_w,',');
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_sequencia_w	:= to_number(substr(nr_sequencias_w,1,nr_pos_virgula_w-1));
			nr_sequencias_w	:= substr(nr_sequencias_w,nr_pos_virgula_w+1,length(nr_sequencias_w));
			if	( nr_sequencia_w > 0) then
				begin
				san_atualiza_inutil_bolsa( nr_sequencia_w, nr_seq_inutil_p, cd_estabelecimento_p );
				end;
			end if;
		end;
		end if;
	end;
	end loop;
	
	
end;
end if;


commit;

end san_atualiza_inutil_bolsa_js;
/