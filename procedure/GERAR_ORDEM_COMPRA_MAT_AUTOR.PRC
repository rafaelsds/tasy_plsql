create or replace
procedure gerar_ordem_compra_mat_autor(
				nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 


/*Vari�veis do mateiral/autoriza��o */
nr_sequencia_autor_w		number(10,0);
cd_material_w			number(06,0);
nr_sequencia_w			number(10,0);
qt_autorizada_w			number(15,3);
vl_unitario_w			number(17,4);
cd_cnpj_w			varchar2(14);
cd_unidade_medida_w		varchar2(30);

/*Vari�veis do par�metro de compras*/
cd_comprador_padrao_w		varchar2(10);
cd_cond_pagto_padrao_w		number(10,0);
cd_moeda_padrao_w		number(10,0);
cd_local_estoque_padrao_w	number(10,0);
cd_local_entrega_w		number(10,0);

/*Vari�veis da ordem de compras*/
nr_ordem_compra_w		number(10,0);
cd_condicao_pagamento_w		number(10,0);
cd_pessoa_solicitante_w		varchar2(10);
cd_comprador_w			varchar2(10);
nr_item_w			number(05,0);
nr_atendimento_w			number(10,0);
cd_local_estoque_w		number(4,0);
cd_centro_custo_w			varchar2(20);
/*Variaveis gerais/auxiliares*/
qt_existe_w			number(10,0);
cd_estabelecimento_w		number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;

ie_permite_gerar_vl_zerado_w	varchar2(5) := 'N';

qt_solicitada_w			number(10,0);
ds_observacao_w			varchar2(4000);
nm_paciente_w			varchar2(255);
ds_convenio_w			varchar2(255);
nm_medico_w			varchar2(100);
dt_cirurgia_w			varchar2(25);
nr_atendimento_ww		number(10);
ie_utilizada_w			varchar2(1);
qt_utilizada_w			number(10,0);
qt_regra_w			number(10,0);
qt_dia_prazo_entrega_w		pessoa_juridica_estab.qt_dia_prazo_entrega%type;
tx_desc_antecipacao_w		pessoa_juridica_estab.tx_desc_antecipacao%type;
pr_desc_financeiro_w		pessoa_juridica_estab.pr_desc_financeiro%type;
dt_entrega_w			ordem_compra_item_entrega.dt_prevista_entrega%type;

/*Busca todos os itens que possuem valor e quantidade autorizada da autoriza��o do fornecedor selecionado*/
cursor c01 is
select	nr_sequencia,
	cd_material,
	nvl(qt_autorizada,0),
	nvl(qt_solicitada,0),
	vl_unitario,
	obter_regra_centro_custo_mat(cd_estabelecimento_w,cd_material,'O','V'),
	obter_regra_local_material(cd_estabelecimento_w,cd_material,'O','V'),
	obter_regra_local_entrega(cd_estabelecimento_w,cd_material,'O','V')
from	material_autorizado
where	nr_sequencia_autor 	= nr_sequencia_autor_w
and	cd_cgc_fabricante		= cd_cnpj_w
and	(((ie_permite_gerar_vl_zerado_w = 'N') and 
	  (vl_unitario > 0) and
	  (qt_autorizada > 0)) or
	(ie_permite_gerar_vl_zerado_w = 'S'))
and	nr_ordem_compra is null;

begin

obter_param_usuario(3004,173,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_permite_gerar_vl_zerado_w);
obter_param_usuario(3004,230,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_utilizada_w);

if	(nr_sequencia_p > 0) then
	begin
	select	nr_sequencia_autor,
		cd_cgc_fabricante,
		nr_atendimento
	into	nr_sequencia_autor_w,
		cd_cnpj_w,
		nr_atendimento_w
	from	material_autorizado
	where	nr_sequencia = nr_sequencia_p;

	/*Verifica se tem algum item, se tiver, gera a OC*/
	select	count(*)
	into	qt_existe_w
	from	material_autorizado
	where	nr_sequencia_autor 	= nr_sequencia_autor_w
	and	cd_cgc_fabricante		= cd_cnpj_w	
	and	(((ie_permite_gerar_vl_zerado_w = 'N') and 
		  (vl_unitario > 0) and
		  (qt_autorizada > 0)) or
		(ie_permite_gerar_vl_zerado_w = 'S'))
	and	nr_ordem_compra is null;
	
	if	(qt_existe_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(173299);
	end if;

	/*Busca valores padr�o para os campos obrigat�rios no parametro compras*/
	select	nvl(max(cd_condicao_pagamento_padrao),0),
		nvl(max(cd_moeda_padrao),0),
		nvl(max(cd_comprador_padrao),0),
		nvl(max(cd_local_estoque_padrao),0)
	into	cd_cond_pagto_padrao_w,
		cd_moeda_padrao_w,
		cd_comprador_padrao_w,
		cd_local_estoque_padrao_w
	from	parametro_compras
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	/*Busca condi��o de pagamento padr�o do fornecedor para o estabelecimento logado*/
	select	nvl(max(cd_cond_pagto),cd_cond_pagto_padrao_w),
		max(qt_dia_prazo_entrega),
		nvl(max(tx_desc_antecipacao),0),
		nvl(max(pr_desc_financeiro),0)
	into	cd_condicao_pagamento_w,
		qt_dia_prazo_entrega_w,
		tx_desc_antecipacao_w,
		pr_desc_financeiro_w
	from	pessoa_juridica_estab
	where	cd_cgc = cd_cnpj_w
	and	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(cd_condicao_pagamento_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(173300);
	end if;
	
	if	(cd_moeda_padrao_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(173301);
	end if;
	
	cd_pessoa_solicitante_w := substr(obter_pessoa_fisica_usuario(nm_usuario_p, 'C'),1,10);
	
	begin
	select	cd_pessoa_fisica
	into	cd_comprador_w
	from	comprador
	where	ie_situacao 		= 'A'
	and	cd_pessoa_fisica		= cd_pessoa_solicitante_w
	and	cd_estabelecimento 	= cd_estabelecimento_w
	and	rownum < 2;
	exception
	when others then
		cd_comprador_w := cd_comprador_padrao_w;
	end;
	
	if	(nvl(cd_comprador_w,'X') = 'X') then
		wheb_mensagem_pck.exibir_mensagem_abort(173302);
	end if;

	select	ordem_compra_seq.nextval
	into	nr_ordem_compra_w
	from	dual;

	select	substr(obter_nome_pf_pj(cd_pessoa_fisica,null),1,255),
		substr(obter_nome_medico(cd_medico_solicitante, 'C'),1,100),
		substr(obter_desc_convenio(cd_convenio),1,100),
		nr_atendimento,
		substr(obter_cirurgia_autor_convenio(nr_sequencia,'DT'),1,25)
	into	nm_paciente_w,
		nm_medico_w,
		ds_convenio_w,
		nr_atendimento_ww,
		dt_cirurgia_w
	from	autorizacao_convenio
	where	nr_sequencia = nr_sequencia_autor_w;

	ds_observacao_w :=substr( wheb_mensagem_pck.get_texto(302173, 
				'NR_ATENDIMENTO=' || nr_atendimento_ww ||
				';NM_PACIENTE=' || nm_paciente_w ||
				';NM_MEDICO=' || nm_medico_w ||
				';DS_CONVENIO=' || ds_convenio_w || 
				';DT_CIRURGIA=' || dt_cirurgia_w),1,4000);

	insert into ordem_compra(nr_ordem_compra,
				cd_estabelecimento,
				cd_cgc_fornecedor,
				cd_condicao_pagamento,
				cd_comprador,
				dt_ordem_compra,
				dt_atualizacao,
				nm_usuario,
				cd_moeda,
				ie_situacao,
				dt_inclusao,
				cd_pessoa_solicitante,
				ie_frete,
				dt_entrega,
				ie_aviso_chegada,
				ie_emite_obs,
				ie_urgente,
				ie_tipo_ordem,
				vl_frete,
				pr_juros_negociado,
				vl_desconto,
				pr_desc_pgto_antec,
				nr_atendimento,
				cd_local_entrega,
				ds_observacao)
			values(	nr_ordem_compra_w,
				cd_estabelecimento_w,
				cd_cnpj_w,
				cd_condicao_pagamento_w,
				cd_comprador_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				cd_moeda_padrao_w,
				'A',
				sysdate,
				cd_pessoa_solicitante_w,
				'C',
				sysdate,
				'N',
				'S',
				'N',
				'V',
				0,
				0,
				0,
				0,
				nr_atendimento_w,
				cd_local_estoque_padrao_w,
				ds_observacao_w);
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		cd_material_w,
		qt_autorizada_w,
		qt_solicitada_w,
		vl_unitario_w,
		cd_centro_custo_w,
		cd_local_estoque_w,
		cd_local_entrega_w;
	exit when C01%notfound;
		begin
		select	cd_unidade_medida_compra
		into	cd_unidade_medida_w
		from	material
		where	cd_material = cd_material_w;
		
		select	nvl(max(nr_item_oci),0) + 1
		into	nr_item_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w;
		
		select	obter_qt_utilizada_mat_autor(nr_sequencia_w)
		into	qt_utilizada_w 
		from dual;
		
		select decode(qt_autorizada_w,0,qt_solicitada_w,qt_autorizada_w)
		into qt_regra_w
		from dual;
		
		if	( ie_utilizada_w = 'S') then
			begin
			if	(qt_utilizada_w > qt_autorizada_w) then
				qt_regra_w := qt_autorizada_w;
			elsif	(qt_utilizada_w < qt_autorizada_w) then
				qt_regra_w := qt_utilizada_w;
			end if;
				
			end;
		end if;
		
		
		insert into ordem_compra_item(	nr_ordem_compra,
			nr_item_oci,
			cd_material,
			cd_unidade_medida_compra,
			vl_unitario_material,
			qt_material,
			qt_original,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			cd_pessoa_solicitante,
			pr_descontos,
			pr_desc_financ,
			vl_desconto,
			cd_local_estoque,
			cd_centro_custo,
			vl_total_item)
		values(	nr_ordem_compra_w,
			nr_item_w,
			cd_material_w,
			cd_unidade_medida_w,
			vl_unitario_w,
			qt_regra_w,
			qt_regra_w,
			sysdate,
			nm_usuario_p,
			'A',
			cd_pessoa_solicitante_w,
			tx_desc_antecipacao_w,
			pr_desc_financeiro_w,
			0,
			cd_local_estoque_w,
			cd_centro_custo_w,
			round((qt_regra_w * vl_unitario_w),4));
					

		dt_entrega_w := sysdate;
		if	(qt_dia_prazo_entrega_w > 0) then			
			select	obter_proximo_dia_util(cd_estabelecimento_w, sysdate + qt_dia_prazo_entrega_w)
			into	dt_entrega_w
			from	dual;
		end if;
		
		update	ordem_compra
		set	dt_entrega = dt_entrega_w
		where	nr_ordem_compra = nr_ordem_compra_w;
					
		insert	into ordem_compra_item_entrega(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			qt_prevista_entrega,
			dt_prevista_entrega,
			dt_entrega_original,
			dt_entrega_limite,
			nr_ordem_compra,
			nr_item_oci)
		values(	ordem_compra_item_entrega_seq.NextVal,
			nm_usuario_p,
			sysdate,
			decode(qt_autorizada_w,0,qt_solicitada_w,qt_autorizada_w),
			dt_entrega_w,
			dt_entrega_w,
			dt_entrega_w,
			nr_ordem_compra_w,
			nr_item_w);

					
		
		update	material_autorizado
		set	nr_ordem_compra 	= nr_ordem_compra_w,
			nr_item_oci	= nr_item_w,
			dt_atualizacao 	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_w;
		
		if	(cd_local_entrega_w > 0) then
			update	ordem_compra
			set	cd_local_entrega	= cd_local_entrega_w
			where	nr_ordem_compra		= nr_ordem_compra_w;
		end if;
		
		end;
	end loop;
	close C01;
	
	commit;
	end;
end if;

end gerar_ordem_compra_mat_autor;
/
