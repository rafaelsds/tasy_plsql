create or replace
procedure gedipa_alterar_local_est(	nr_sequencia_p number,
					cd_local_estoque_p number) is
begin
update 	adep_processo
set 	cd_local_estoque = cd_local_estoque_p
where 	nr_sequencia = nr_sequencia_p;
commit;
end gedipa_alterar_local_est;
/