create or replace procedure update_status_tasy_queue_build(
    nr_sequencia_p number, 
    ie_status_p varchar2		
) is

ie_status_build_w varchar2(1);
ie_status_build_atual_w varchar2(1);
dt_inicio_build_w date;
dt_fim_build_w date;

ds_mensagem_w varchar2(4000);

begin
  if (not ie_status_p is null) then
    ie_status_build_w := upper(ie_status_p);
    
		if ( not ie_status_build_w in ('A','F','G','E','C')) then
			wheb_mensagem_pck.exibir_mensagem_abort('Estado nulo ou invalido! [ie_status_p='||ie_status_build_w||'], valores validos [A,F,G,E,C][Aguardando,Fila,Gerando,Erro,Cancelado].');
		end if;
	else
		wheb_mensagem_pck.exibir_mensagem_abort('Estado nulo ou invalido! [ie_status_p='||ie_status_build_w||'], valores validos [A,F,G,E,C][Aguardando,Fila,Gerando,Erro,Cancelado].');
	end if;
	
  select ie_status_build 
    into ie_status_build_atual_w 
    from tasy_queue_build 
   where nr_sequencia = nr_sequencia_p;
   
  dt_inicio_build_w := sysdate;
  dt_fim_build_w := null;
  if (ie_status_build_w <> 'A') then
    dt_inicio_build_w := sysdate;
    dt_fim_build_w := null;
    if (ie_status_build_w in ('F','E','C')) then
      dt_inicio_build_w := null;
      dt_fim_build_w := sysdate;
    end if;
  end if;
  
  if (ie_status_build_w = 'F') then  
      update tasy_queue_build 
          set ie_status_build = upper(ie_status_p),
              dt_atualizacao = sysdate,
              nm_usuario = 'jenkins',
              dt_fim_build = dt_fim_build_w
        where nr_sequencia = nr_sequencia_p;      
  elsif (ie_status_build_w = 'E') then
     update tasy_queue_build 
          set ie_status_build = upper(ie_status_p),
              dt_atualizacao = sysdate,
              nm_usuario = 'jenkins',
              dt_fim_build = dt_fim_build_w
        where nr_sequencia = nr_sequencia_p;
 
      select 
             'VERSAO='||CD_VERSAO||CHR(13)||
             'IE_APLICACAO='||IE_APLICACAO||chr(13)||
             'NM_USUARIO_SOLICITANTE='||NM_USUARIO_SOLICITANTE||chr(13)||
             'DT_INICIO_BUILD='||to_char(DT_INICIO_BUILD,'dd/mm/yyyy hh24:mi:ss')||CHR(13)||
             'DT_FIM_BUILD='||to_char(DT_FIM_BUILD,'dd/mm/yyyy hh24:mi:ss')||chr(13)||
             'NR_SEQ_CALENDARIO='||NR_SEQ_CALENDARIO||CHR(13)||
             'NR_SEQ_MAN_OS_BUILD_EMER='||NR_SEQ_MAN_OS_BUILD_EMER||CHR(13)
        into ds_mensagem_w
        from tasy_queue_build 
       where nr_sequencia = nr_sequencia_p;    
  elsif (ie_status_build_w = 'C') then
    update tasy_queue_build 
        set ie_status_build = ie_status_build_w,
            dt_atualizacao = sysdate,
            nm_usuario = 'jenkins'
      where nr_sequencia = nr_sequencia_p;
  elsif (ie_status_build_w = 'A') then
    update tasy_queue_build 
       set ie_status_build = upper(ie_status_p),
           dt_atualizacao = sysdate,
           nm_usuario = 'jenkins',
           dt_inicio_build = null,
           dt_fim_build = null
     where nr_sequencia = nr_sequencia_p;
  else 
    update tasy_queue_build 
       set ie_status_build = upper(ie_status_p),
           dt_atualizacao = sysdate,
           nm_usuario = 'jenkins',
           dt_inicio_build = dt_inicio_build_w
     where nr_sequencia = nr_sequencia_p;
  end if;
  
  commit;
end;
/
