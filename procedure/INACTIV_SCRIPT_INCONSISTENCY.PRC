create or replace procedure INACTIV_SCRIPT_INCONSISTENCY(nr_seq_script_p in tasy_ajuste_base.cd_ajuste_base%type, nm_usuario_p in usuario.nm_usuario%type, ds_justificativa_p in tasy_ajuste_base.ds_justificativa%type) is
ie_analyst_w varchar2(1 char);
begin

  select obter_cargo_colaborador(nm_usuario_p, 'A')
  into ie_analyst_w
  from dual;

  if (ie_analyst_w = 'S') then
    update tasy_ajuste_base set
      dt_inativacao = sysdate,
      nm_usuario_inativacao = nm_usuario_p,
      ds_justificativa = ds_justificativa_p
    where cd_ajuste_base = nr_seq_script_p;
    commit;
  end if;
end;
/