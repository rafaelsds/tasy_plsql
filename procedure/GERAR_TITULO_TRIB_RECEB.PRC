create or replace
procedure gerar_titulo_trib_receb
			(nr_seq_nfs_p		number,
			 nm_usuario_p		varchar2) is
			 
cd_tributo_w            	nota_fiscal_trib.cd_tributo%type;
vl_tributo_w            	nota_fiscal_trib.vl_tributo%type;     
tx_tributo_w            	nota_fiscal_trib.tx_tributo%type;
vl_base_calculo_w      	 	nota_fiscal_trib.vl_base_calculo%type;
vl_trib_nao_retido_w    	nota_fiscal_trib.vl_trib_nao_retido%type;
vl_base_nao_retido_w   	 	nota_fiscal_trib.vl_base_nao_retido%type;
vl_trib_adic_w          	nota_fiscal_trib.vl_trib_adic%type;
vl_base_adic_w         		nota_fiscal_trib.vl_base_adic%type;
ie_origem_trib_w       		nota_fiscal_trib.ie_origem_trib%type;
nr_seq_regra_trib_w		nota_fiscal_trib.nr_seq_regra_trib%type;
ie_nf_tit_rec_w  		tributo.ie_nf_tit_rec%type;
nr_seq_trans_prov_trib_rec_w	tributo.nr_seq_trans_prov_trib_rec%type;
nr_seq_tit_rec_trib_w   	titulo_receber_trib.nr_sequencia%type;
ie_titulo_pagar_w  		regra_calculo_imposto.ie_titulo_pagar%type;
cd_cgc_beneficiario_w 		regra_calculo_imposto.cd_cgc_beneficiario%type;
nr_seq_trans_fin_baixa_cp_w 	regra_calculo_imposto.nr_seq_tf_baixa_cp%type;
cd_condicao_pagamento_trib_w 	regra_calculo_imposto.cd_condicao_pagamento%type;
dt_emissao_w                    nota_fiscal.dt_emissao%type;
cd_estabelecimento_w            nota_fiscal.cd_estabelecimento%type;
vl_total_nota_w 		nota_fiscal.vl_total_nota%type;
cd_cgc_w                	nota_fiscal.cd_cgc%type;
cd_pessoa_fisica_w      	nota_fiscal.cd_pessoa_fisica%type;
nr_interno_conta_w       	nota_fiscal.nr_interno_conta%type;
nr_seq_protocolo_w       	nota_fiscal.nr_seq_protocolo%type;
cd_condicao_pagamento_w    	nota_fiscal.cd_condicao_pagamento%type;
nr_nota_fiscal_w         	nota_fiscal.nr_nota_fiscal%type;
nr_seq_mensalidade_w      	nota_fiscal.nr_seq_mensalidade%type;
cd_conv_integracao_w      	nota_fiscal.cd_conv_integracao%type;
cd_operacao_nf_w          	nota_fiscal.cd_operacao_nf%type;
pr_juro_padrao_w   		parametros_contas_pagar.pr_juro_padrao%type;   
pr_multa_padrao_w  		parametros_contas_pagar.pr_multa_padrao%type;
cd_tipo_taxa_juro_w   		parametro_contas_receber.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_w  		parametro_contas_receber.cd_tipo_taxa_multa%type;
cd_moeda_padrao_w    		parametro_contas_receber.cd_moeda_padrao%type;
nr_seq_trans_fin_baixa_w 	parametro_contas_receber.nr_seq_trans_fin_baixa%type;
ie_conta_financ_tit_rec_w 	parametro_contas_receber.ie_conta_financ_tit_rec%type;
cd_convenio_w                   convenio.cd_convenio%type;
nr_titulo_w                     titulo_receber.nr_titulo%type;
nr_seq_regra_w			regra_calculo_imposto.nr_sequencia%type;
cd_pessoa_fisica_trib_w         titulo_pagar.cd_pessoa_fisica%type;
nr_titulo_pagar_w               titulo_pagar.nr_titulo%type;
cd_estab_financeiro_w           estabelecimento.cd_estab_financeiro%type;
dt_vencimento_trib_w		date;
qt_venc_w			number(15);
ds_venc_w			varchar2(4000);
pr_aliquota_w			number(7,4);
vl_minimo_base_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
ie_acumulativo_w		varchar2(10);
vl_teto_base_w			number(15,2);
vl_desc_dependente_w		number(15,2); 
ie_tipo_contrato_w		varchar2(2);
cont_w 				number(5);        
 
cd_darf_w			regra_calculo_imposto.cd_darf%type;

cursor C03 is
	select	a.cd_tributo,
		a.vl_tributo,
		a.tx_tributo,
		a.vl_base_calculo,
		a.vl_trib_nao_retido,
		a.vl_base_nao_retido,
		a.vl_trib_adic,
		a.vl_base_adic,
		a.ie_origem_trib,
		a.nr_seq_regra_trib
	from	nota_fiscal_trib a
	where	a.nr_sequencia	= nr_seq_nfs_p
union
	select  cd_tributo,
		sum(vl_tributo),
		tx_tributo, 
		sum(vl_base_calculo),
		sum(vl_trib_nao_retido),
		sum(vl_base_nao_retido),
		sum(vl_trib_adic),
		sum(vl_base_adic),
		ie_origem_trib,
		nr_seq_regra_trib
	from ( select	a.cd_tributo,
		a.vl_tributo,
		a.tx_tributo,
		a.vl_base_calculo,
		a.vl_trib_nao_retido,
		a.vl_base_nao_retido,
		a.vl_trib_adic,
		a.vl_base_adic,
		null ie_origem_trib,
		a.nr_seq_regra_trib
		from	nota_fiscal_item_trib a
		where	a.nr_sequencia	= nr_seq_nfs_p )
		group by    cd_tributo,
			    tx_tributo, 
			    ie_origem_trib,
			    nr_seq_regra_trib;
			    
			    
begin

select  max(nr_titulo)
into    nr_titulo_w
from    titulo_receber
where   nr_seq_nf_saida = nr_seq_nfs_p;	


select	dt_emissao,
	cd_estabelecimento,
	vl_total_nota,
	cd_cgc,
	cd_pessoa_fisica,
	nr_interno_conta,
	nr_seq_protocolo,
	cd_condicao_pagamento,
	vl_total_nota,
	nr_nota_fiscal,
	nr_seq_mensalidade,
	cd_conv_integracao,
	cd_operacao_nf
into	dt_emissao_w,
	cd_estabelecimento_w,
	vl_total_nota_w,
	cd_cgc_w,
	cd_pessoa_fisica_w,
	nr_interno_conta_w,
	nr_seq_protocolo_w,
	cd_condicao_pagamento_w,
	vl_total_nota_w,
	nr_nota_fiscal_w,
	nr_seq_mensalidade_w,
	cd_conv_integracao_w,
	cd_operacao_nf_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nfs_p;	


if	(nvl(nr_seq_protocolo_w,0) > 0) then

	select	max(cd_convenio)
		into	cd_convenio_w
		from	protocolo_convenio
		where	nr_seq_protocolo	= nr_seq_protocolo_w;
	else
		select	max(c.cd_convenio)
		into	cd_convenio_w
		from	lote_protocolo c,
			nota_fiscal d
		where	d.nr_sequencia		= nr_seq_nfs_p
		and	d.nr_seq_lote_prot	= c.nr_sequencia;
end if;	
		
	
	select	max(a.pr_multa_padrao),
		max(a.pr_juro_padrao)
	into	pr_multa_padrao_w,
		pr_juro_padrao_w
	from	parametros_contas_pagar a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;
	
	select	cd_tipo_taxa_juro,
		cd_tipo_taxa_multa,
		cd_moeda_padrao
	into	cd_tipo_taxa_juro_w,
		cd_tipo_taxa_multa_w,
		cd_moeda_padrao_w
	from	parametro_contas_receber
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	select	nvl(cd_estab_financeiro, cd_estabelecimento)
	into	cd_estab_financeiro_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;
	
if (nr_titulo_w > 0 ) then	
	 
			 
open C03;
loop
fetch C03 into
	cd_tributo_w,
	vl_tributo_w,
	tx_tributo_w,
	vl_base_calculo_w,
	vl_trib_nao_retido_w,
	vl_base_nao_retido_w,
	vl_trib_adic_w,
	vl_base_adic_w,
	ie_origem_trib_w,
	nr_seq_regra_trib_w;
exit when C03%notfound;

	select	nvl(max(ie_nf_tit_rec),'N'),
		max(nr_seq_trans_prov_trib_rec)
	into	ie_nf_tit_rec_w,
		nr_seq_trans_prov_trib_rec_w
	from	tributo
	where	cd_tributo = cd_tributo_w;

	select	count(*)
	into	cont_w
	from	titulo_receber_trib
	where	nr_titulo	= nr_titulo_w
	and	cd_tributo	= cd_tributo_w;

	if	((ie_nf_tit_rec_w = 'S') and
		(cont_w = 0)) or (ie_nf_tit_rec_w = 'U') then 
		
		if	(cont_w > 0) then
			delete  from titulo_receber_trib
			where   nr_titulo	= nr_titulo_w
			and 	cd_tributo	= cd_tributo_w;
		end if;
		
		select	titulo_receber_trib_seq.nextval
		into	nr_seq_tit_rec_trib_w
		from	dual;
		
		insert into titulo_receber_trib
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_titulo,
			cd_tributo,
			vl_tributo,
			tx_tributo,
			vl_base_calculo,
			vl_trib_nao_retido,
			vl_base_nao_retido,
			vl_trib_adic,
			vl_base_adic,
			nr_seq_nota_fiscal,
			ie_origem_tributo,
			nr_seq_trans_financ,
			nr_lote_contabil)
		select	nr_seq_tit_rec_trib_w,
			sysdate,
			nm_usuario_p,
			nr_titulo_w,
			cd_tributo_w,
			vl_tributo_w,
			nvl(tx_tributo_w,0),
			nvl(vl_base_calculo_w,0),
			vl_trib_nao_retido_w,
			vl_base_nao_retido_w,
			vl_trib_adic_w,
			vl_base_adic_w,
			nr_seq_nfs_p,
			decode(ie_origem_trib_w, null, 'C', ie_origem_trib_w),
			nr_seq_trans_prov_trib_rec_w,
			0
		from	dual;
		
		
		if	(ie_origem_trib_w = 'CD') then
			Atualizar_Saldo_Tit_Rec(nr_titulo_w, nm_usuario_p);
		end if;
		
		
		obter_dados_trib_tit_rec(cd_tributo_w,
					cd_estabelecimento_w,
					nvl(cd_convenio_w,cd_conv_integracao_w),
					dt_emissao_w,
					'N',
					pr_aliquota_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					ie_acumulativo_w,
					vl_teto_base_w,
					vl_desc_dependente_w,
					cd_pessoa_fisica_w,
					cd_cgc_w,
					ie_tipo_contrato_w,
					1,
					0,
					nr_seq_regra_w,
					cd_darf_w);
		
		select	max(a.ie_titulo_pagar),
			max(a.cd_cgc_beneficiario),
			max(a.nr_seq_tf_baixa_cp),
			max(a.cd_condicao_pagamento)
		into	ie_titulo_pagar_w,
			cd_cgc_beneficiario_w,
			nr_seq_trans_fin_baixa_cp_w,
			cd_condicao_pagamento_trib_w
		from	regra_calculo_imposto a
		where	a.nr_sequencia	= nr_seq_regra_w;
		
		
		if (nr_seq_regra_trib_w is not null) and (ie_titulo_pagar_w is null) then
		   
			select	max(a.ie_titulo_pagar),
				max(a.cd_cgc_beneficiario),
				max(a.nr_seq_tf_baixa_cp),
				max(a.cd_condicao_pagamento)
		into		ie_titulo_pagar_w,
				cd_cgc_beneficiario_w,
				nr_seq_trans_fin_baixa_cp_w,
				cd_condicao_pagamento_trib_w
			from	regra_calculo_imposto a
			where	a.nr_sequencia	= nr_seq_regra_trib_w;
		   
		end if;   
		
		if	(ie_titulo_pagar_w	= 'S') and
			(vl_tributo_w <> 0) then
			if	(cd_cgc_beneficiario_w	is null) then
				cd_pessoa_fisica_trib_w	:= cd_pessoa_fisica_w;
			else
				cd_pessoa_fisica_trib_w	:= null;
			end if;
			
			if (cd_condicao_pagamento_trib_w is not null) then
					dt_vencimento_trib_w := dt_emissao_w;
					
					if	(PKG_DATE_UTILS.get_WeekDay(dt_vencimento_trib_w) = 7) then
						
						if	(PKG_DATE_UTILS.start_of(dt_vencimento_trib_w,'DD',0) = PKG_DATE_UTILS.start_of(dt_vencimento_trib_w + 2, 'MONTH',0)) then
							dt_vencimento_trib_w := dt_vencimento_trib_w + 2;
						end if;
					elsif	(PKG_DATE_UTILS.get_WeekDay(dt_vencimento_trib_w) = 1) then
						
						if	(PKG_DATE_UTILS.start_of(dt_vencimento_trib_w,'DD',0) = PKG_DATE_UTILS.start_of(dt_vencimento_trib_w + 1, 'MONTH',0)) then
							dt_vencimento_trib_w := dt_vencimento_trib_w + 1;
						end if;
					end if;
					
					calcular_vencimento(	cd_estabelecimento_w, cd_condicao_pagamento_trib_w, dt_vencimento_trib_w, qt_venc_w, ds_venc_w);

					if	(qt_venc_w = 1) then	
						dt_vencimento_trib_w	:= to_date(substr(ds_venc_w,1,10),'DD/MM/YYYY');
					end if;

			end if;
			
			select	titulo_pagar_seq.nextval
			into	nr_titulo_pagar_w
			from	dual;

			insert	into titulo_pagar
				(cd_cgc,
				cd_estabelecimento,
				cd_estab_financeiro,
				cd_moeda,
				cd_pessoa_fisica,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				dt_atualizacao,
				dt_emissao,
				dt_vencimento_atual,
				dt_vencimento_original,
				ie_origem_titulo,
				ie_situacao,
				ie_tipo_titulo,
				nm_usuario,
				nr_seq_tit_rec_trib,
				nr_titulo,
				tx_juros,
				tx_multa,
				vl_saldo_juros,
				vl_saldo_multa,
				vl_saldo_titulo,
				vl_titulo,
				cd_tributo,
				nr_seq_trans_fin_baixa)
			values	(nvl(cd_cgc_beneficiario_w,cd_cgc_w),
				cd_estabelecimento_w,
				cd_estab_financeiro_w,
				cd_moeda_padrao_w,
				cd_pessoa_fisica_trib_w,
				cd_tipo_taxa_juro_w,
				cd_tipo_taxa_multa_w,
				sysdate,
				dt_emissao_w,
				nvl(dt_vencimento_trib_w,dt_emissao_w),
				nvl(dt_vencimento_trib_w,dt_emissao_w),
				'4',
				'A',
				'4',
				nm_usuario_p,
				nr_seq_tit_rec_trib_w,
				nr_titulo_pagar_w,
				pr_juro_padrao_w,
				pr_multa_padrao_w,
				0,
				0,
				vl_tributo_w,
				vl_tributo_w,
				cd_tributo_w,
				nr_seq_trans_fin_baixa_cp_w);
			atualizar_inclusao_tit_pagar(nr_titulo_pagar_w, nm_usuario_p);
		end if;
	end if;
end loop;
close c03;
end if;
		
end gerar_titulo_trib_receb;
/
