create or replace
procedure gerar_comunic_etapa is 

nr_seq_etapa_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_gerado_w		varchar2(2000);

cursor c01 is
	select	nr_sequencia,
		nr_seq_contrato
	into	nr_seq_etapa_w,
		nr_seq_contrato_w
	from	contrato_etapa
	where	dt_prevista = trunc(sysdate,'dd') + 1;

begin
	
	open c01;
	loop
	fetch c01 into	
		nr_seq_etapa_w,
		nr_seq_contrato_w;
	exit when c01%notfound;
		begin
		con_enviar_comunic_etapa(nr_seq_etapa_w, nr_seq_contrato_w, '', 'Tasy',nr_seq_gerado_w);
		end;
	end loop;
	close c01;

	commit;

end gerar_comunic_etapa;
/