create or replace
procedure um_gerar_ret_debito_itau_150
			(	nr_seq_cobr_escrit_p	number,
				nm_usuario_p		varchar2) is


ds_titulo_w			varchar2(255);
dt_liquidacao_w			varchar2(8);
cd_ocorrencia_w			varchar2(5);
vl_titulo_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
vl_outras_despesas_w		number(15,2);
vl_cobranca_w			number(15,2);
vl_alterar_w			number(15,2);
nr_titulo_w			number(10);
nr_seq_ocorrencia_ret_w		number(10);
vl_saldo_inclusao_w		titulo_receber.vl_saldo_titulo%type;

nr_carteirinha_w		varchar2(25);
cd_agencia_w			varchar2(4);
nr_conta_w				varchar2(5);
nr_digito_conta_w		varchar2(1);
ie_acao_w				varchar2(1);
ds_debito_w				varchar2(8);
dt_debito_w				date;
nr_seq_conta_debito_w		cobranca_escritural.nr_seq_conta_banco%type;

cursor c01 is
	select	trim(substr(ds_string,70,25)),
		to_number(substr(ds_string,53,15))/100,
		0,
		0,
		0,
		0,
		0,
		substr(ds_string,45,8),
		substr(ds_string,68,2),
		null,
		null,
		null,
		null,
		null,
		null
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= 'F'
	union all  --Union para buscar registros de inclusao e exclus�o de d�bito automatico.
	select	'0',
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			trim(substr(ds_string,2,25)) nr_carteirinha,
			substr(ds_string,27,4) cd_agencia,
			substr(ds_string,39,5) nr_conta,
			substr(ds_string,44,1) nr_digito_conta,
			decode( substr(ds_string,150,1) , '1', 'E', '2', 'I', null) ie_acao,
			substr(ds_string,45,8) dt_debito
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= 'B';	
begin

open C01;
loop
fetch C01 into
	ds_titulo_w,
	vl_liquido_w,
	vl_acrescimo_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_cobranca_w,
	vl_outras_despesas_w,
	dt_liquidacao_w,
	cd_ocorrencia_w,
	nr_carteirinha_w,
	cd_agencia_w,
	nr_conta_w,
	nr_digito_conta_w,
	ie_acao_w,
	ds_debito_w;
exit when C01%notfound;
	begin
	vl_alterar_w	:= 0;

	nr_titulo_w := null;
	
	if	(ds_titulo_w <> '0') then
	
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_titulo	= to_char(somente_numero( ds_titulo_w ));
		
		if	(nr_titulo_w is null) then /* Quando n�o h� t�tulo, busca pelo nosso numero */
			select	max(nr_titulo)
			into	nr_titulo_w
			from	titulo_receber
			where	nr_nosso_numero	= to_char(somente_numero(ds_titulo_w));
		end if;
	
	end if;

	/* Se encontrou o t�tulo importa, sen�o grava no log */
	if	(nr_titulo_w is not null) then
		select	max(vl_titulo),
			max(vl_saldo_titulo)
		into	vl_titulo_w,
			vl_saldo_inclusao_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		select 	max(a.nr_sequencia)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco = 341
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		/* Tratar acrescimos/descontos */
		if	(vl_titulo_w <> vl_liquido_w) then
			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;
		end if;
		
		insert	into titulo_receber_cobr
			(nr_sequencia,
			nr_titulo,
			cd_banco,
			vl_cobranca,
			vl_desconto,
			vl_acrescimo,
			vl_despesa_bancaria,
			vl_liquidacao,
			dt_liquidacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_ocorrencia_ret,
			vl_saldo_inclusao)
		values	(titulo_receber_cobr_seq.nextval,
			nr_titulo_w,
			341,
			vl_titulo_w,
			vl_desconto_w,
			vl_acrescimo_w,
			vl_outras_despesas_w,
			vl_liquido_w,
			nvl(to_date(dt_liquidacao_w,'yyyymmdd'),sysdate),
			sysdate,
			nm_usuario_p,
			nr_seq_cobr_escrit_p,
			nr_seq_ocorrencia_ret_w,
			vl_saldo_inclusao_w);
	else
		
		insert into cobranca_escrit_log
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_cobranca,
			ds_log)
		values	(cobranca_escrit_log_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_cobr_escrit_p,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');
	end if;
	
	/*Inicio OS 1217905*/	
	if (nr_carteirinha_w is not null) then
	
		select 	max(a.nr_seq_conta_banco)
		into	nr_seq_conta_debito_w
		from	cobranca_escritural a
		where 	a.nr_sequencia = nr_seq_cobr_escrit_p;
		
		begin
		
		if (ds_debito_w is not null) then
			dt_debito_w := to_date(ds_debito_w,'dd/mm/yyyy');
		end if;
		
 		exception when others then
			dt_debito_w := sysdate;
		end;
		
		pls_inclusao_debito_auto_pag	(null,
						nr_carteirinha_w,
						dt_debito_w,
						nr_seq_conta_debito_w,
						cd_agencia_w,
						null,
						nr_conta_w,
						nr_digito_conta_w,
						ie_acao_w,
						nm_usuario_p );
									  								 
	end if;
	/*Fim OS 1217905*/
	
	end;
end loop;
close C01;

commit;

end um_gerar_ret_debito_itau_150;
/