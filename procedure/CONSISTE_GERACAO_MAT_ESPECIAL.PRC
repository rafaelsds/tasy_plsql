create or replace
procedure consiste_geracao_mat_especial(nr_sequencia_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					ds_erro_p		out varchar2) is

ie_gerar_dados_tiss_w	varchar2(1) := 'N';
ie_gerar_sem_material_w	varchar2(1) := 'N';
ie_mais_de_uma_autor_esp_w	varchar2(1) := 'S';
qt_existe_w		number(5);
ds_erro_w		varchar2(255);

begin

obter_param_usuario(3004, 189, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_dados_tiss_w);
obter_param_usuario(3004, 191, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_sem_material_w);
obter_param_usuario(3004, 205, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_mais_de_uma_autor_esp_w);


if	(nvl(nr_sequencia_p, 0) > 0) then

	if	(ie_gerar_dados_tiss_w = 'S') then
	
		select	count(1)
		into	qt_existe_w
		from	autorizacao_convenio_tiss
		where	nr_sequencia_autor = nr_sequencia_p;
		
		if	(qt_existe_w = 0) then
			ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(280353);
		else
			select	count(1)
			into	qt_existe_w
			from	autorizacao_convenio_tiss
			where	nr_sequencia_autor = nr_sequencia_p
			and	dt_validade_carteira is null;
			
			if	(qt_existe_w > 0) then
				ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(280354);
			end if;
		end if;
	
	end if;
	
	if	(ie_gerar_sem_material_w = 'S') then
		select	count(1)
		into	qt_existe_w
		from	material_autorizado
		where	nr_sequencia_autor = nr_sequencia_p;
		
		if	(qt_existe_w = 0) then
			ds_erro_w  := WHEB_MENSAGEM_PCK.get_texto(280355);
		end if;
	end if;
	
	if	(nvl(ie_mais_de_uma_autor_esp_w,'S') = 'N') then
		select	count(*)
		into	qt_existe_w
		from	autorizacao_cirurgia
		where	nr_seq_autor_conv	= nr_sequencia_p;
		
		if	(qt_existe_w > 0) then
			ds_erro_w  := WHEB_MENSAGEM_PCK.get_texto(280356);
		end if;		
	end if;

end if;

ds_erro_p := ds_erro_w;

end consiste_geracao_mat_especial;
/