create or replace
procedure consistir_quit_cirurugia_js(		ie_opcao_p		number,
					cd_material_p		number,
					cd_unidade_medida_p	varchar2,
					cd_intervalo_p		number,
					ds_horarios_p		varchar2,
					qt_dose_p		number,
					qt_Unitaria_p		number,
					nr_seq_lote_fornec_p	number,
					ie_consistido_agenda_p	varchar2,
					cd_fornec_consignado_p	varchar2,
					nr_prescricao_p		number,
					ie_origem_inf_p		number,
					ie_medicacao_paciente_p	varchar2,
					ie_utiliza_kit_p		varchar2,
					ie_urgencia_p		varchar2,
					ie_bomba_infusao_p	varchar2,
					ie_suspenso_p		varchar2,
					ie_se_necessario_p		varchar2,
					ie_status_cirurgia_p		varchar2,
					hr_prim_horario_p		varchar2,
					cd_motivo_baixa_p		number,
					nr_ocorrencia_p		number,
					qt_material_p		number,
					qt_total_dispensar_p	number,
					nr_agrupamento_p		number,
					nr_seq_lote_p		number,
					nr_seq_item_prescr_p	number,
					ie_restringe_item_prescr_p	varchar2,
					ie_atualiza_saldo_lote_p	varchar2,
					ds_materiais_p		varchar2,
					nm_usuario_p		varchar2) is 
					
nr_sequencia_w				number(10);
sql_qtde_material_w			varchar2(4000) := '';
sql_w					varchar2(4000) := '';
ds_parametros_w				varchar2(4000) := '';
ds_sep_w				varchar2(255) := '';
nr_seq_lote_fornec_w			number(10);

begin

ds_sep_w	:= substr(obter_separador_bv,1,255);
if (nr_seq_lote_fornec_p <> 0) then	
	nr_seq_lote_fornec_w := nr_seq_lote_fornec_p;
end if;

if	(nvl(ie_opcao_p, -1) = 1) then
		
	delete	prescr_material
	where	nr_prescricao = nr_prescricao_p;		
	
elsif	(nvl(ie_opcao_p, -1) = 2) then

	select	nvl(max(nr_sequencia), 0) + 1
	into	nr_sequencia_w
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p;
	
	--insert da prescricao_material_q(atepac_qm)
	insert into prescr_material(
		nr_prescricao,				nr_sequencia,		ie_origem_inf,
		cd_material,				cd_unidade_medida,	qt_dose,
		qt_unitaria,				qt_material,		dt_atualizacao,
		nm_usuario,				cd_intervalo,		ds_horarios,
		ds_observacao,				ie_via_aplicacao,		nr_agrupamento,
		cd_motivo_baixa,				dt_baixa,			ie_utiliza_kit,
		cd_unidade_medida_dose,			qt_conversao_dose, 	ie_urgencia,
		nr_ocorrencia, 				qt_total_dispensar, 		cd_fornec_consignado,
		nr_sequencia_solucao,			nr_sequencia_proc,		qt_solucao,
		hr_dose_especial,				qt_dose_especial,		ds_dose_diferenciada,
		ie_medicacao_paciente,			nr_sequencia_diluicao, 	hr_prim_horario,
		nr_dia_util,				nr_sequencia_dieta,	ie_agrupador,
		dt_emissao_setor_atend,			ie_suspenso,		ds_justificativa,
		qt_dias_solicitado,				qt_dias_liberado,		nm_usuario_liberacao,
		dt_liberacao,				ie_se_necessario,		qt_min_aplicacao,
		nr_seq_lote_fornec,				ie_status_cirurgia,		ie_bomba_infusao,
		cd_kit_material,				nr_doc_interno,		ie_consistido_agenda)
	values(	nr_prescricao_p,				nr_sequencia_w,		ie_origem_inf_p,
		cd_material_p,				cd_unidade_medida_p,	qt_dose_p,
		qt_unitaria_p,				qt_material_p,		sysdate,
		nm_usuario_p,				cd_intervalo_p,		ds_horarios_p,
		null,					null,			nr_agrupamento_p,
		cd_motivo_baixa_p,				null,			ie_utiliza_kit_p,
		null,					null,			ie_urgencia_p,
		nr_ocorrencia_p,				qt_total_dispensar_p,	cd_fornec_consignado_p,
		null,					null,			null,
		null,					null,			null,
		ie_medicacao_paciente_p,			null,			hr_prim_horario_p,
		null	,				null,			null,
		null,					ie_suspenso_p,		null,
		null,					null,			null,
		null,					ie_se_necessario_p,	null,
		nr_seq_lote_fornec_w,			ie_status_cirurgia_p,	ie_bomba_infusao_p,
		null,					null,			ie_consistido_agenda_p);
		
elsif	(nvl(ie_opcao_p, -1) = 3) then

	if	(nvl(nr_seq_lote_p, 0) <> 0) then
		if	(nvl(ie_restringe_item_prescr_p, 'N') = 'S') then
			update	prescr_material
			set	ie_status_cirurgia	= 'CB',
				nr_seq_lote_fornec	= nr_seq_lote_p,
				NM_USUARIO_CONSIST	= nm_usuario_p,
				DT_ATUALIZACAO_CONSIST	= sysdate,
				ie_consistido_agenda	= ie_consistido_agenda_p
			where	cd_material   		= cd_material_p
			and	nr_sequencia		= nr_seq_item_prescr_p
			and	nr_prescricao		= nr_prescricao_p;
		else
			update	prescr_material
			set	ie_status_cirurgia	= 'CB',
				nr_seq_lote_fornec	= nr_seq_lote_p,
				NM_USUARIO_CONSIST	= nm_usuario_p,
				DT_ATUALIZACAO_CONSIST	= sysdate,
				ie_consistido_agenda	= ie_consistido_agenda_p
			where	cd_material   		= cd_material_p
			and	nr_prescricao		= nr_prescricao_p;
		end if;		
	else
		
		if	(nvl(ie_restringe_item_prescr_p, 'N') = 'S') then
			update	prescr_material
			set	ie_status_cirurgia	= 'CB',
				NM_USUARIO_CONSIST	= nm_usuario_p,
				DT_ATUALIZACAO_CONSIST	= sysdate,
				ie_consistido_agenda	= ie_consistido_agenda_p
			where	cd_material   		= cd_material_p
			and	nr_sequencia		= nr_seq_item_prescr_p
			and	nr_prescricao		= nr_prescricao_p;
		else
			update	prescr_material
			set	ie_status_cirurgia	= 'CB',
				NM_USUARIO_CONSIST	= nm_usuario_p,
				DT_ATUALIZACAO_CONSIST	= sysdate,
				ie_consistido_agenda	= ie_consistido_agenda_p
			where	cd_material   		= cd_material_p
			and	nr_prescricao		= nr_prescricao_p;
		end if;		
		
	end if;
	
elsif	(nvl(ie_opcao_p, -1) = 4) then	
	
	if	(ie_atualiza_saldo_lote_p = 'S') then
	
		sql_qtde_material_w	:=	'     qt_material      		= :qt_material, ' || 
						'     qt_Total_Dispensar	= :qt_material, ' ||
						'     qt_dose			= :qt_material, ';
	end if;
	
	if	(nvl(nr_seq_lote_p, 0) <> 0) then
		if	(nvl(ie_restringe_item_prescr_p, 'N') = 'S') then
	
			sql_w	:=	' update	prescr_material '						||
					' set		ie_status_cirurgia 		= ''CB'','			||
					'		nr_seq_lote_fornec 		= :nr_seq_lote_fornec,'	||
					'		nm_usuario_consist 	= :nm_usuario,'		||
					sql_qtde_material_w						||
					'		dt_atualizacao_consist	= sysdate, '		||
					'		ie_consistido_agenda	= :ie_consistido_agenda'	||
					' where		cd_material		= :cd_material'		||
					' and		nr_sequencia		= :nr_seq_item_prescr'	||
					' and		nr_prescricao		= :nr_prescricao';
		else 
		
			sql_w	:=	' update	prescr_material '						||
					' set		ie_status_cirurgia 		= ''CB'','			||
					'		nr_seq_lote_fornec 		= :nr_seq_lote_fornec,'	||
					'		nm_usuario_consist 		= :nm_usuario,'	||
					sql_qtde_material_w						||
					'		dt_atualizacao_consist		= sysdate, '	||
					'		ie_consistido_agenda		= :ie_consistido_agenda'||
					' where		cd_material			= :cd_material'	||
					' and		nr_prescricao			= :nr_prescricao';
				
		end if;
		
		ds_parametros_w		:=	'NR_SEQ_LOTE_FORNEC='	|| nr_seq_lote_fornec_w		|| ds_sep_w ||
						'NM_USUARIO='		|| nm_usuario_p			|| ds_sep_w ||
						'IE_CONSISTIDO_AGENDA='	|| ie_consistido_agenda_p		|| ds_sep_w ||
						'NR_SEQ_ITEM_PRESCR='	|| nr_seq_item_prescr_p		|| ds_sep_w ||
						'NR_PRESCRICA='		|| nr_prescricao_p			|| ds_sep_w ||
						'QT_MATERIAL='		|| qt_material_p;
		
		exec_sql_dinamico_bv(	'CONSISTIR_QUIT_CIRURUGIA',
					sql_w,
					ds_parametros_w);
		
	else 
		if	(nvl(ie_restringe_item_prescr_p, 'N') = 'S') then
		
			sql_w	:=	' update	prescr_material 		' 				||
					' set		ie_status_cirurgia		= ''CB'','			||
					'     		qt_material        		= :qt_material,'		||
					'     		qt_total_dispensar 		= :qt_material,'		||
					'     		qt_dose            		= :qt_material,'		||
					'     		nm_usuario_consist 		= :nm_usuario,'	||
					sql_qtde_material_w						||
					'     		dt_atualizacao_consist 		= sysdate,'	||
					'     		ie_consistido_agenda 		= :ie_consistido_agenda'||
					' where		cd_material   			= :cd_material'	||
					' and		nr_sequencia			= :nr_seq_item_prescr'	||
					' and		nr_prescricao			= :nr_prescricao';
		else
			sql_w	:=	' update	prescr_material 		' 				||
					' set		ie_status_cirurgia		= ''CB'','			||
					'     		qt_material        		= :qt_material,'		||
					'     		qt_total_dispensar 		= :qt_material,'		||
					'     		qt_dose            		= :qt_material,'		||
					'     		nm_usuario_consist 		= :nm_usuario,'	||
					sql_qtde_material_w						||
					'     		dt_atualizacao_consist	 	= sysdate,'	||
					'     		ie_consistido_agenda 		= :ie_consistido_agenda'||
					' where		cd_material   			= :cd_material'	||
					' and		nr_prescricao			= :nr_prescricao';
		end if;
		
		ds_parametros_w		:=	'NR_SEQ_LOTE_FORNEC='	|| nr_seq_lote_fornec_w		|| ds_sep_w ||
						'NM_USUARIO='		|| nm_usuario_p			|| ds_sep_w ||
						'IE_CONSISTIDO_AGENDA='	|| ie_consistido_agenda_p		|| ds_sep_w ||
						'NR_SEQ_ITEM_PRESCR='	|| nr_seq_item_prescr_p		|| ds_sep_w ||
						'NR_PRESCRICA='		|| nr_prescricao_p			|| ds_sep_w ||
						'QT_MATERIAL='		|| qt_material_p;
		
		exec_sql_dinamico_bv(	'CONSISTIR_QUIT_CIRURUGIA',
					sql_w,
					ds_parametros_w);
			
	end if;
	
elsif	(nvl(ie_opcao_p, -1) = 5) then
	
	delete	prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	substr(obter_se_contido(cd_material, ds_materiais_p),1,1) = 'N';
	

end if;
commit;
end consistir_quit_cirurugia_js;
/
