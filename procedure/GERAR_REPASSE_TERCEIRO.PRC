create or replace
procedure GERAR_REPASSE_TERCEIRO
		(dt_inicial_p			in	date,
		dt_final_p			in	date,
		cd_condicao_pagamento_p		in	number,
		cd_estabelecimento_p		in	number,
		ie_tipo_data_p			in	number,
		ie_tipo_convenio_p			in	number,
		ie_gerar_repasse_p		in	varchar2,
		nm_usuario_p			in	varchar2,
		cd_convenio_p			in	number,
		dt_mesano_ref_p			in	date,
		ie_tipo_pessoa_p			in	varchar2,
		ie_ignorar_periodo_p		in	varchar2,
		dt_base_vencto_p			in	date,
		ie_acao_p			in	number,
		ie_gerar_rep_perc_saldo_p		in	varchar2,
		ie_valor_lib_zerado_p		in 	varchar2,
		ie_terceiro_vl_lib_zerado_p	in 	varchar2,
		nr_seq_protocolo_p		in	number,
		nr_seq_tipo_repasse_p		in 	number,
		nr_repasse_terceiro_p		in 	number,
		ie_copiar_terceiro_regra_p	in	varchar2,
		ie_tipo_atendimento_p		in	number,
		ds_filtro_convenios_p		in 	varchar2,
		ds_filtro_procedimentos_p	in	varchar2,
		ie_ignorar_repasse_aberto_p	in	varchar2,
		ds_observacao_p			in	varchar2,
		ds_filtro_medico_p		in	varchar2,
		ds_filtro_proc_p		in	varchar2,
		cd_area_proc_p			in 	number,
		cd_espec_proc_p			in	number,
		cd_grupo_proc_p			in	number,
		ie_repasse_especial_p		in	varchar2,
		ie_interv_prod_p		in	varchar2) is

/*	ie_acao_p

0	Sem a��o
1	Gerar vencimentos
2	Gerar vencimentos e mudar status
3	Gerar vencimentos, mudar status e aprovar repasse
4	Gerar vencimentos, mudar status, aprovar repasse e gerar t�tulo

*/

nr_seq_terceiro_w	number(10,0);
nr_repasse_terceiro_w	number(10,0);
ie_estornado_w		varchar2(255);
ie_gerar_tributos_w	varchar2(1);
qt_cont_w		number(10);
vl_repasse_w		number(15,2);
vl_liberado_terc_w	number(15,2);
ie_excluir_rep_zerado_w	varchar2(255);
cd_regra_w		number(5);

cursor c01 is
select	a.nr_sequencia
from	terceiro a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	nvl(a.ie_utilizacao, 'A') in ('A','R')
and	((ie_tipo_pessoa_p = 'T') or ((ie_tipo_pessoa_p = 'F') and (a.cd_pessoa_fisica is not null)) or ((ie_tipo_pessoa_p = 'J') and (a.cd_cgc is not null)))
and	a.ie_situacao		= 'A'
and	((nr_seq_tipo_repasse_p is null) or (nvl(a.nr_seq_tipo_rep,nr_seq_tipo_repasse_p) = nr_seq_tipo_repasse_p));

begin

select	nvl(max(ie_excluir_rep_zerado),'N')
into	ie_excluir_rep_zerado_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

open c01;
loop
fetch c01 into
	nr_seq_terceiro_w;
exit when c01%notfound;
	
	select	max(obter_valor_liberado_terceiro(nr_seq_terceiro_w))
	into	vl_liberado_terc_w
	from 	dual;
	
	/*lhalves OS295540 em 29/03/2011 - Gerar apenas para terceiros com valor liberado <> de zero*/
	if	((nvl(ie_terceiro_vl_lib_zerado_p,'N') 	= 'S') and
		 (nvl(vl_liberado_terc_w,0) 		<> 0)) or
		(nvl(ie_terceiro_vl_lib_zerado_p,'N') 	= 'N') then
		
		if	(ie_ignorar_repasse_aberto_p = 'N') then
			if	(ie_ignorar_periodo_p = 'P') then
				select	nvl(max(x.nr_repasse_terceiro),0)
				into	nr_repasse_terceiro_w
				from	repasse_terceiro x
				where	x.nr_seq_terceiro	= nr_seq_terceiro_w
				and	x.ie_status		= 'A';
			else
				select	nvl(max(nr_repasse_terceiro),0)
				into	nr_repasse_terceiro_w
				from	repasse_terceiro x
				where	x.nr_seq_terceiro			= nr_seq_terceiro_w
				and	nvl(x.cd_convenio,0)			= nvl(nvl(cd_convenio_p,x.cd_convenio),0)
				and	nvl(x.ie_tipo_convenio,0)		= nvl(nvl(ie_tipo_convenio_p,x.ie_tipo_convenio),0)
				and	nvl(x.cd_condicao_pagamento,0)		= nvl(nvl(cd_condicao_pagamento_p,x.cd_condicao_pagamento),0)
				and	nvl(x.nr_seq_tipo,0)			= nvl(nvl(nr_seq_tipo_repasse_p,x.nr_seq_tipo),0)
				and	x.ie_status				= 'A'
				and  	x.IE_TIPO_DATA				= ie_tipo_data_p
				and	trunc(nvl(x.dt_base_vencto,sysdate))	= trunc(nvl(nvl(dt_base_vencto_p,x.dt_base_vencto),sysdate))
				and	(dt_mesano_ref_p is null or (trunc(x.dt_mesano_referencia,'month')	= trunc(dt_mesano_ref_p,'month')))
				and	x.dt_periodo_inicial between dt_inicial_p and dt_final_p;
			end if;
		else	
			nr_repasse_terceiro_w	:= 0;
		end if;
		
		if	(((nr_repasse_terceiro_w = 0) and (ie_ignorar_periodo_p = 'N')) or
				(ie_ignorar_periodo_p = 'S') or
			(ie_ignorar_periodo_p = 'A') or
			(ie_ignorar_periodo_p = 'P')) then
				
			if	((ie_ignorar_periodo_p = 'A') or (ie_ignorar_periodo_p = 'P'))  and (nr_repasse_terceiro_w <> 0)  then
				update	repasse_terceiro set
					cd_estabelecimento	= cd_estabelecimento_p,
					dt_mesano_referencia	= nvl(dt_mesano_ref_p, dt_final_p),
					ie_status		= 'A',
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p,
					cd_convenio		= cd_convenio_p,
					ie_tipo_convenio	= ie_tipo_convenio_p,
					dt_periodo_inicial	= dt_inicial_p,
					dt_periodo_final	= dt_final_p,
					ie_tipo_data		= ie_tipo_data_p,
					dt_aprovacao_terceiro	= null,
					nm_usuario_aprov	= null,
					cd_condicao_pagamento	= cd_condicao_pagamento_p,
					dt_base_vencto		= nvl(dt_base_vencto_p,sysdate),
					nr_seq_tipo		= nr_seq_tipo_repasse_p,
					ds_observacao		= ds_observacao_p
				where	nr_seq_terceiro		= nr_seq_terceiro_w
				and	dt_periodo_inicial between dt_inicial_p and dt_final_p
				and	ie_status		= 'A';
				
			elsif	((nvl(ie_terceiro_vl_lib_zerado_p,'N') = 'S' and nvl(vl_liberado_terc_w,0) <> 0) or
				(nvl(ie_terceiro_vl_lib_zerado_p,'N') = 'N')) then
				
				select	repasse_terceiro_seq.nextval
				into	nr_repasse_terceiro_w
				from	dual;

				insert into repasse_terceiro
					(nr_repasse_terceiro,
					cd_estabelecimento,
					nr_seq_terceiro,
					dt_mesano_referencia,
					ie_status,
					dt_atualizacao,
					nm_usuario,
					cd_convenio,
					dt_periodo_inicial,
					dt_periodo_final,
					ie_tipo_data,
					dt_aprovacao_terceiro,
					nm_usuario_aprov,
					cd_condicao_pagamento,
					dt_base_vencto,
					nr_seq_tipo,
					ie_tipo_convenio,
					ds_observacao)
				values	(nr_repasse_terceiro_w,
					cd_estabelecimento_p,
					nr_seq_terceiro_w,
					nvl(dt_mesano_ref_p, dt_final_p),
					'A',
					sysdate,
					nm_usuario_p,
					cd_convenio_p,
					dt_inicial_p,
					dt_final_p,
					ie_tipo_data_p,
					null,
					null,
					cd_condicao_pagamento_p,
					nvl(dt_base_vencto_p,sysdate),
					nr_seq_tipo_repasse_p,
					ie_tipo_convenio_p,
					ds_observacao_p);		
			end if;
		end if;
		
		if	(nvl(ie_copiar_terceiro_regra_p,'N') = 'S') and
			(nvl(nr_repasse_terceiro_p,0) > 0) then
				
			COPIAR_REPASSE_TERCEIRO_REGRA(nr_repasse_terceiro_p,nr_repasse_terceiro_w,nm_usuario_p);
		end if;
		
		if	(nr_seq_tipo_repasse_p is not null) and
			(nvl(nr_repasse_terceiro_w,0) > 0) then
			inserir_regra_tipo_repasse(nr_seq_tipo_repasse_p, nr_repasse_terceiro_w,nm_usuario_p);
		end if;
		
		if	(ie_gerar_repasse_p = 'S') and (nvl(nr_repasse_terceiro_w, 0) > 0) then
				
			obter_param_usuario(89, 30, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_estornado_w);
			Gerar_Conta_Repasse(nr_repasse_terceiro_w, ie_tipo_convenio_p ,nm_usuario_p, nvl(ie_estornado_w, 'N'), ie_gerar_rep_perc_saldo_p,ie_valor_lib_zerado_p, nr_seq_protocolo_p, ie_tipo_atendimento_p,ds_filtro_convenios_p,ds_filtro_procedimentos_p,cd_area_proc_p,cd_espec_proc_p,cd_grupo_proc_p,ds_filtro_medico_p,ds_filtro_proc_p, ds_observacao_p);
		end if;
		
		if	(nvl(ie_repasse_especial_p,'N')	= 'S') and
			(nvl(nr_repasse_terceiro_w, 0) > 0) then
			Gerar_repasse_especial(nr_repasse_terceiro_w,nm_usuario_p,0,null);
		end if;
		
		
		
		if	(ie_gerar_repasse_p = 'N') and
			(ie_excluir_rep_zerado_w = 'S') then
			delete	from repasse_terceiro a
			where	not exists
				(select	1
				from	procedimento_repasse x
				where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro
				union
				select	1
				from	material_repasse x
				where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro
				union
				select	1
				from	repasse_terceiro_item x
				where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro)
			and	a.nr_repasse_terceiro	= nr_repasse_terceiro_w;
		end if;
		
		/* ahoffelder - 11/02/2011 - esse comando � necess�rio pq na procedure GERAR_CONTA_REPASSE o repasse pode ser deletado */
		select	count(*)
		into	qt_cont_w
		from	repasse_terceiro a
		where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
		
		obter_param_usuario(89, 28, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_tributos_w);

		if	(nvl(qt_cont_w,0) > 0) then
			
			if	(nvl(ie_interv_prod_p,'N') = 'S') then
				gerar_repasse_interv_producao(nr_repasse_terceiro_w,nm_usuario_p);
			end if;
			
			select	obter_valor_repasse(a.nr_repasse_terceiro,'L')
			into	vl_repasse_w
			from	repasse_terceiro a
			where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w;

			if	(nvl(vl_repasse_w,0) <> 0) then

				if	(ie_acao_p in (1, 2, 3, 4)) then
					Gerar_Venc_Repasse(nr_repasse_terceiro_w,ie_gerar_tributos_w,nm_usuario_p,'N');
				end if;

				if	(ie_acao_p in (2, 3, 4)) then
					Altera_Status_Repasse_Terceiro(nr_repasse_terceiro_w,nm_usuario_p,'N');
					gerar_eventos_repasse(nr_repasse_terceiro_w,'RF',nm_usuario_p,cd_estabelecimento_p);
				end if;

				if	(ie_acao_p in (3, 4)) then
					if	(cd_condicao_pagamento_p is null) then
						/* 191468 = Antes de aprovar o repasse � necess�rio informar a condi��o de pagamento! */
						wheb_mensagem_pck.exibir_mensagem_abort(191468);
					end if;

					Aprovar_Repasse_Terceiro(nr_repasse_terceiro_w,nm_usuario_p,sysdate,'N');
				end if;

				if	(ie_acao_p = 4) then
					Gerar_Titulo_Pagar_Repasse(nr_repasse_terceiro_w,nm_usuario_p,null);
				end if;
				
				if	(obter_param_usuario_logado(89,135)  <> 'N' and nr_repasse_terceiro_w is not null) then
					VINCULAR_ADIANT_TIT_REPASSE(
						nr_repasse_terceiro_w,
						nm_usuario_p,
						cd_estabelecimento_p );
				end if;

			end if;

		end if;

	end if;

end loop;
close c01;

commit;

end GERAR_REPASSE_TERCEIRO;
/