create or replace
procedure HC_CADASTRAR_PROF_AGENDA (   cd_pessoa_fisica_p		varchar2,
					nr_seq_agenda_p			varchar2,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p	number) is 

ie_existe_w	varchar2(1);									
nr_sequencia_w	number(10);

Cursor C01 is
select 	nr_sequencia cd
from 	AGENDA_HOME_CARE
where 	ie_situacao = 'A'
and 	cd_estabelecimento = cd_estabelecimento_p
and 	obter_se_contido(nr_sequencia,nr_seq_agenda_p) = 'S';
									
begin

if (cd_pessoa_fisica_p is not null) then

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin

	select	decode(count(*),0,'N','S')
	into 	ie_existe_w
	from 	AGENDA_HOME_CARE_EQUIPE
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and		nr_seq_agenda		= nr_sequencia_w; 	
		
	if (ie_existe_w = 'N')	 then
		insert into AGENDA_HOME_CARE_EQUIPE (
						NR_SEQUENCIA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						NR_SEQ_AGENDA,
						CD_PESSOA_FISICA)
		values(
						AGENDA_HOME_CARE_EQUIPE_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_sequencia_w,
						cd_pessoa_fisica_p);
	end if;	
	end;
end loop;
close C01;
end if;

commit;

end HC_CADASTRAR_PROF_AGENDA;
/