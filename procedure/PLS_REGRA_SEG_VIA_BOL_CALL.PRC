create or replace
procedure pls_regra_seg_via_bol_call(	nr_seq_titulo_p		number,
					dt_vencimento_p		date,
					nm_usuario_p		varchar2,
					ds_retorno_p	out	varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar regra de segunda via do boleto no call center
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias: Call center
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
nr_seq_regra_w			pls_regra_emis_seg_via_bol.nr_sequencia%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_cgc_w			pessoa_juridica.cd_cgc%type;
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
cd_estabelecimento_w		titulo_receber.cd_estabelecimento%type;
qt_max_dias_boleto_w		pls_regra_emis_seg_via_bol.qt_max_dias_boleto%type;
qt_max_dias_dt_atual_w		pls_regra_emis_seg_via_bol.qt_max_dias_dt_atual%type;
qt_vezes_permitido_w		pls_regra_emis_seg_via_bol.qt_vezes_permitido%type;
qt_max_dias_titulo_w		pls_regra_emis_seg_via_bol.qt_max_dias_titulo%type;
qt_dias_limite_prorrogacao_w	pls_regra_emis_seg_via_bol.qt_dias_limite_prorrogacao%type;
ds_retorno_w			varchar2(255);
dt_ultimo_venc_w		titulo_receber.dt_vencimento%type;
qt_titulo_vencido_w		number(10);
dt_venc_original_w		date;
dt_vencimento_dia_util_w	date;
ie_permite_w			pls_regra_emis_seg_via_bol.ie_permite%type;
ds_mensagem_w			pls_regra_emis_seg_via_bol.ds_mensagem%type;

begin
ds_retorno_w 			:= 'OK';
dt_vencimento_dia_util_w	:= obter_proximo_dia_util(cd_estabelecimento_w, dt_vencimento_p);

if	(nr_seq_titulo_p is not null) then
	select	cd_pessoa_fisica,
		cd_cgc,
		nr_seq_pagador,
		cd_estabelecimento,
		dt_vencimento
	into	cd_pessoa_fisica_w,
		cd_cgc_w,
		nr_seq_pagador_w,
		cd_estabelecimento_w,
		dt_venc_original_w
	from	titulo_receber
	where	nr_titulo = nr_seq_titulo_p;

	nr_seq_regra_w	:= pls_obter_regra_seg_via_boleto(	cd_pessoa_fisica_w,
								cd_cgc_w,
								nr_seq_pagador_w,
								nr_seq_titulo_p,
								sysdate,
								'CC',
								cd_estabelecimento_w);

	if	(nr_seq_regra_w is not null) then
		select	max(qt_max_dias_boleto),
			max(qt_max_dias_dt_atual),
			max(qt_vezes_permitido),
			max(qt_max_dias_titulo),
			max(qt_dias_limite_prorrogacao),
			max(ie_permite),
			max(ds_mensagem)
		into	qt_max_dias_boleto_w,
			qt_max_dias_dt_atual_w,
			qt_vezes_permitido_w,
			qt_max_dias_titulo_w,
			qt_dias_limite_prorrogacao_w,
			ie_permite_w,
			ds_mensagem_w
		from	pls_regra_emis_seg_via_bol
		where	nr_sequencia = nr_seq_regra_w;
		
		if	(ie_permite_w = 'S') then
			if	(nvl(qt_max_dias_boleto_w,0) > 0) and
				(nr_seq_pagador_w is not null) then
				select	max(a.dt_vencimento)
				into	dt_ultimo_venc_w
				from	titulo_receber		a,
					pls_mensalidade		b,
					pls_lote_Mensalidade 	c
				where	a.nr_seq_mensalidade 	= b.nr_sequencia
				and	b.nr_seq_lote		= c.nr_sequencia
				and	a.nr_seq_pagador	= nr_seq_pagador_w
				and	c.ie_visualizar_portal 	= 'S'
				and	a.ie_situacao 		= '1';
				
				dt_ultimo_venc_w := obter_proximo_dia_util(cd_estabelecimento_w,dt_ultimo_venc_w);

				if	(trunc(dt_vencimento_p,'dd') > (trunc(dt_ultimo_venc_w,'dd') + qt_max_dias_boleto_w)) then
					ds_retorno_w	:= 'A data de vencimento informada n�o pode ser superior a ' || qt_max_dias_boleto_w ||
									' dia(s) do vencimento da �ltima parcela em aberto.';
				elsif	(dt_vencimento_p < trunc(sysdate,'dd')) then
					ds_retorno_w	:= 'A data de vencimento informada n�o pode ser menor que a data atual.';
				end if;
			end if;

			if	(nvl(qt_max_dias_dt_atual_w,0) > 0) then
				if	(trunc(dt_vencimento_p,'dd') > obter_proximo_dia_util(cd_estabelecimento_w, trunc(sysdate,'dd') + qt_max_dias_dt_atual_w)) then
					ds_retorno_w	:= 'A data de vencimento informada n�o pode ser superior a ' || qt_max_dias_dt_atual_w ||
									' dia(s) da data atual.';
				end if;
			end if;
			
			if	(nvl(qt_vezes_permitido_w,0) > 0) then
				select	count(1)
				into	qt_titulo_vencido_w
				from	alteracao_vencimento
				where	nr_titulo = nr_seq_titulo_p;

				if	(qt_vezes_permitido_w <= qt_titulo_vencido_w) then
					ds_retorno_w	:= 'N�o � poss�vel imprimir o boleto com mais de ' || qt_vezes_permitido_w || ' altera��es de vencimento';
				end if;
			end if;

			if	(nvl(qt_max_dias_titulo_w,0) > 0) and
				(trunc(dt_vencimento_p) > (dt_venc_original_w + qt_max_dias_titulo_w)) then
				ds_retorno_w := 'A data de vencimento informada n�o pode ser superior a ' || qt_max_dias_titulo_w || ' dia(s) da data do vencimento original';
			end if;
			
			if 	(nvl(qt_dias_limite_prorrogacao_w,0) > 0) then
				if 	(obter_proximo_dia_util(cd_estabelecimento_w, trunc(sysdate,'dd') + qt_dias_limite_prorrogacao_w) < trunc(dt_vencimento_dia_util_w, 'dd')) then
					ds_retorno_w := 'N�o � poss�vel gerar o boleto para mais de ' || qt_dias_limite_prorrogacao_w || ' dia(s) da data atual!';
				end if;
			end if;
		else
			ds_retorno_w := nvl(ds_mensagem_w, 'N�o � permitido alterar o vencimento, conforme regra ' || nr_seq_regra_w || '.');
		end if;
	end if;
end if;

ds_retorno_p	:= ds_retorno_w;

commit;

end pls_regra_seg_via_bol_call;
/