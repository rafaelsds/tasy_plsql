create or replace procedure db_workflow_add_item(
		nr_sequencia_p		number,
		nr_seq_workflow_p	number,
		ds_justificativa_p 	varchar,
		nm_usuario_p		varchar)
is

begin
	
update 	db_workflow_item
set 	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate,
	ie_status = 'P'
where 	nr_sequencia = nr_sequencia_p
and	nr_seq_workflow = nr_seq_workflow_p;

commit;
	
end db_workflow_add_item;
/
