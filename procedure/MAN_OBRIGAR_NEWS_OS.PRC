create or replace
procedure man_obrigar_news_os(	nr_sequencia_p	number) is 
begin
	if	(nr_sequencia_p is not null) then
		begin
		update	man_ordem_servico
		set	ie_obriga_news		= 'S',
			nm_usuario_lib_news     	= null,
			dt_libera_news		= null,
			ds_just_ret_news		= null
		where	nr_sequencia		= nr_sequencia_p;
		commit;
		end;
	end if;
end man_obrigar_news_os;
/