create or replace
procedure Hd_gerar_dialise (	dt_dialise_p			date,
			cd_pessoa_fisica_p			varchar2,
			nm_usuario_p			varchar2,
			cd_unid_dialise_p			varchar2,
			cd_estabelecimento_p		number,
			nr_atendimento_p			number,
			qt_peso_pre_p			number,
			ie_tipo_dialise_p			varchar2,
			ds_erro_p			out	varchar2) is

nr_dialise_pac_w		number(10,0);
nr_seq_dialise_w		number(10,0);
ie_obriga_atendimento_w		varchar2(1);
nr_seq_dialise_gerada_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
nr_prescricao_emergencia_w	number(10);
nr_prescricao_normal_w		number(10);
nr_prescricao_w			number(10);
qt_dialise_atend_w		number(10);
ie_gerar_atend_w		varchar2(1);
qt_dialise_w			number(10);
qt_dialise_atend_pac_w		number(10);

ie_gqa_gerar_proto_assist_w VARCHAR2(1);
ie_utiliza_regra_sessoes_w	varchar2(1);

begin

/* leitura do par�metro 31 da gest�o de hemodi�lise */
obter_param_usuario(7009, 31, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_obriga_atendimento_w);
obter_param_usuario(7009, 103, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, qt_dialise_atend_w);
obter_param_usuario(7009, 280, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gqa_gerar_proto_assist_w);

select 	nvl(max(nr_sequencia),0)
into	nr_seq_dialise_gerada_w
from	hd_dialise
where	dt_fim_dialise is null
and	dt_cancelamento is null
and	cd_pessoa_fisica = cd_pessoa_fisica_p;


select 	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

select	hd_dialise_seq.nextval
into	nr_seq_dialise_w
from	dual;

ie_gerar_atend_w	:= 'S';


qt_dialise_atend_pac_w	:= Obter_dialise_atend(nr_seq_dialise_w,nr_atendimento_p) + 1;

select	decode(count(*),0,'N','S')
into	ie_utiliza_regra_sessoes_w
from	hd_regra_sessao
where	ie_situacao = 'A';


if	(ie_utiliza_regra_sessoes_w = 'N') then

	if	(qt_dialise_atend_w > 0) then

		select	count(*)
		into	qt_dialise_w
		from	hd_dialise
		where	nr_atendimento = nr_atendimento_p
		and 	dt_cancelamento is null
		and	nr_sequencia <= nr_seq_dialise_w;

		if	(qt_dialise_w >= qt_dialise_atend_w) then
			ie_gerar_atend_w	:= 'N';
		end if;
	end if;
else
	ie_gerar_atend_w := hd_permite_dialise_sessao(nr_atendimento_p,nr_seq_dialise_w);
end if;	
	

if	(ie_gerar_atend_w = 'N') then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277446,null);
elsif	(ie_obriga_atendimento_w = 'S') and
	(nr_atendimento_p is null) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(277449,null);
elsif   (nr_seq_dialise_gerada_w > 0) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(277462,'NR_SEQ_DIALISE_GERADA='|| nr_seq_dialise_gerada_w);
elsif	(cd_pessoa_fisica_w <> cd_pessoa_fisica_p) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(277459,null);
else
	/* pega a pr�xima sequencia para o paciente */
	select	nvl(max(nr_seq_paciente),0)+1
	into	nr_dialise_pac_w
	from 	hd_dialise
	where 	cd_pessoa_fisica	= cd_pessoa_fisica_p;

if (cd_unid_dialise_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(207618);
end if;
	
	/* insere os dados da di�lise */
	insert into hd_dialise (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		dt_dialise,
		nr_seq_paciente,
		nr_seq_unid_dialise,
		nr_atendimento,
		NR_DIALISE_ATEND_PAC,
		qt_peso_pre,
		ie_tipo_dialise,
		ie_paciente_agudo
	) values (
		nr_seq_dialise_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_p,
		dt_dialise_p,
		nr_dialise_pac_w,
		cd_unid_dialise_p,
		nr_atendimento_p,
		qt_dialise_atend_pac_w,
		qt_peso_pre_p,
		nvl(ie_tipo_dialise_p,'N'),
		substr(hd_obter_ie_agudo_pac(cd_pessoa_fisica_p),1,1));

	hd_gerar_assinatura(null, null, nr_seq_dialise_w, null, null, null, null, null, null, 'GH', nm_usuario_p, 'N');
	
	select   max(nr_prescricao)
	into	 nr_prescricao_emergencia_w   
	from	 prescr_medica a
	where	 ie_hemodialise	         = 'E'
	and	 trunc(dt_prescricao)	   = trunc(sysdate)
	and	 cd_pessoa_fisica        = cd_pessoa_fisica_p
	and	 nvl(dt_liberacao_medico, dt_liberacao) is not null
	and	dt_fim_prescricao is null
	and	not exists (select 1 from prescr_mat_hor b where b.nr_prescricao = a.nr_prescricao and nr_seq_dialise is not null);
	
	select  max(nr_prescricao)
	into	nr_prescricao_normal_w
	from	prescr_medica
	where	ie_hemodialise	         = 'S'
	and	cd_pessoa_fisica        = cd_pessoa_fisica_p
	and	nvl(dt_liberacao_medico, dt_liberacao) is not null
	and	dt_fim_prescricao is null;

	nr_prescricao_w	:= nr_prescricao_normal_w;
	
	if	(nr_prescricao_emergencia_w is not null) then
		nr_prescricao_w	:= nr_prescricao_emergencia_w;
	end if;
	
	if	(nr_prescricao_w is not null) then
	
		gerar_prescr_mat_hor_dialise(nr_prescricao_w,nr_seq_dialise_w,OBTER_PERFIL_ATIVO,dt_dialise_p,nm_usuario_p);
	
	end if;

	IF ie_gqa_gerar_proto_assist_w = 'S' THEN
		GQA_GERAR_PROTOCOLO_ASSIST(nr_atendimento_p, 8, nm_usuario_p);
	END IF;
  
commit;

end if;

end hd_gerar_dialise;
/