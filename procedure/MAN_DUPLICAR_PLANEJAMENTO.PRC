create or replace
procedure Man_Duplicar_Planejamento(
			nr_sequencia_p		Number,
			nr_seq_equipamento_p	Number,
			nm_usuario_p		Varchar2) is
			
nr_sequencia_w		number(10,0);

begin
select	man_planej_prev_seq.nextval
into	nr_sequencia_w	
from	dual;

insert into man_planej_prev(
		nr_sequencia,
		ds_planejamento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_tipo_equip,
		nr_seq_tipo_contador,
		nr_seq_frequencia,
		qt_dia_previsto,
		qt_dia_gerar_ordem,
		ie_prioridade,
		cd_pessoa_solicitante,
		ds_dano,
		ie_situacao,
		cd_setor_atendimento,
		dt_inicial,
		ie_impacto,
		ie_contador,
		nr_seq_equip,
		nm_usuario_exec,
		ie_solicitante,
		ie_comunicacao,
		qt_dia_sobreposicao,
		nr_seq_superior,
		cd_estabelecimento,
		cd_perfil,
		ie_data_inicio_geracao,
		ie_dia_ultima_verif,
		ie_grau_satisfacao,
		nr_seq_estagio,
		ie_dia_nao_util,
		ie_dia_util,
		nr_seq_origem_dano,
		nr_seq_causa_dano,
		nr_seq_tipo_solucao,
		nr_seq_complex,
		nr_seq_cs,
		nr_seq_marca,
		nr_seq_modelo,
		nr_seq_grupo_planej,
		nr_seq_grupo_trabalho,
		nr_seq_tipo_os,
		nr_seq_grupo_sup)
	select	nr_sequencia_w,
		ds_planejamento,
		sysdate,
		nm_usuario_p,
		nr_seq_tipo_equip,
		nr_seq_tipo_contador,
		nr_seq_frequencia,
		qt_dia_previsto,
		qt_dia_gerar_ordem,
		ie_prioridade,
		cd_pessoa_solicitante,
		ds_dano,
		ie_situacao,
		cd_setor_atendimento,
		dt_inicial,
		ie_impacto,
		ie_contador,
		nr_seq_equipamento_p,
		nm_usuario_exec,
		ie_solicitante,
		ie_comunicacao,
		qt_dia_sobreposicao,
		nr_seq_superior,
		cd_estabelecimento,
		cd_perfil,
		ie_data_inicio_geracao,
		ie_dia_ultima_verif,
		ie_grau_satisfacao,
		nr_seq_estagio,
		ie_dia_nao_util,
		ie_dia_util,
		nr_seq_origem_dano,
		nr_seq_causa_dano,
		nr_seq_tipo_solucao,
		nr_seq_complex,
		nr_seq_cs,
		nr_seq_marca,
		nr_seq_modelo,
		nr_seq_grupo_planej,
		nr_seq_grupo_trabalho,
		nr_seq_tipo_os,
		nr_seq_grupo_sup
	from	man_planej_prev
	where	nr_sequencia = nr_sequencia_p;
	
	
insert into man_planej_atividade(
		nr_sequencia,
		nr_seq_planej_prev,
		dt_atualizacao,
		nm_usuario,
		ds_atividade,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		qt_min_prev)
	select	man_planej_atividade_seq.nextval,
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		ds_atividade,
		sysdate,
		nm_usuario_p,
		qt_min_prev
	from	man_planej_atividade 	
	where	nr_seq_planej_prev = nr_sequencia_p; 


insert into man_planej_prev_arq(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_planej_prev,
		ds_arquivo,
		ie_anexar_email)
	select	man_planej_prev_arq_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		ds_arquivo,
		ie_anexar_email
	from	man_planej_prev_arq
	where	nr_seq_planej_prev = nr_sequencia_p;	


insert into man_planej_prev_regra(
		nr_sequencia,
		nr_seq_planej_prev,
		dt_atualizacao,
		nm_usuario,
		ds_regra,
		qt_contador,
		nr_seq_planej_contador,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	select	man_planej_prev_regra_seq.nextval,
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		ds_regra,
		qt_contador,
		nr_seq_planej_contador,
		sysdate,
		nm_usuario_p
	from	man_planej_prev_regra
	where	nr_seq_planej_prev = nr_sequencia_p;


insert into man_regra_data_frequencia( 
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_planej_prev,
		dt_geracao,
		ie_data_hora,
		hr_geracao,
		dt_dia_semana)
	select	man_regra_data_frequencia_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		dt_geracao,
		ie_data_hora,
		hr_geracao,
		dt_dia_semana
	from	man_regra_data_frequencia
	where	nr_seq_planej_prev = nr_sequencia_p;

insert 	into	man_planej_prev_equip_excl(
		nr_sequencia,
		nr_seq_planej_prev,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_equipamento)
	select	man_planej_prev_equip_excl_seq.nextval,
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_equipamento
	from	man_planej_prev_equip_excl
	where	nr_seq_planej_prev = nr_sequencia_p;
commit;

end Man_Duplicar_Planejamento;
/