create or replace
procedure pat_consistir_w_imp_pat_bem
			(	cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Consistir os registros de bens importados
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
	Controle de patrimonio - 107982
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_erro_w				varchar2(4000);
ds_linha_w				varchar2(10);
qt_integridade_w			number(10);
qt_bens_w				number(10)	:= 0;
qt_bens_lidos_w				number(10)	:= 0;
qt_inconsistencia_w			number(10)	:= 0;

ds_consistencia_w			w_imp_pat_bem.ds_consistencia%type;
cd_bem_ant_w				w_imp_pat_bem.cd_bem%type;
cd_empresa_w				empresa.cd_empresa%type;

Cursor c_bens is
	select	cd_bem,
		ds_bem,
		cd_estabelecimento,
		nr_seq_tipo,
		nr_seq_local,
		cd_conta_contabil,
		cd_centro_custo,
		cd_cgc,
		nr_nota_fiscal,
		cd_pessoa_fisica,
		nr_seq_marca,
		nr_seq_crit_rateio,
		nr_seq_regra_conta,
		cd_moeda,
		dt_aquisicao,
		vl_original,
		tx_deprec,
		dt_inicio_uso,
		ie_imobilizado,
		ie_situacao,
		ie_tipo_valor,
		cd_bem_superior,
		nr_sequencia
	from	w_imp_pat_bem
    where 	((cd_estabelecimento in 
            (select a.cd_estabelecimento from estabelecimento a
            where a.ie_situacao = 'A'
            and a.cd_empresa in (select b.cd_empresa from grupo_emp_estrutura b
            where b.nr_seq_grupo = (select c.nr_seq_grupo from grupo_emp_estrutura c
            where c.cd_empresa = obter_empresa_estab(cd_estabelecimento_p)
            and c.ie_tipo_estrutura = 1 /*Controlada*/)))) or cd_estabelecimento = cd_estabelecimento_p);

type 		fetch_array is table of c_bens%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
bens_w	vetor;
	
begin
update	w_imp_pat_bem
set	ds_consistencia	= null;

commit;

open c_bens;
loop
fetch c_bens bulk collect into s_array limit 1000;
	bens_w(i)	:= s_array;
	i		:= i + 1;
exit when c_bens%notfound;
end loop;
close c_bens;

qt_bens_lidos_w		:= 0;

cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_p);


begin
for i in 1..bens_w.count loop
	begin
	s_array := bens_w(i);
	for z in 1..s_array.count loop
		begin
		ds_consistencia_w	:= null;
		qt_inconsistencia_w	:= 0;
		ds_linha_w		:= null;
		/*Consistindo bem */
		gravar_processo_longo(wheb_mensagem_pck.get_texto(299456,'DS_BEM=' || s_array(z).ds_bem),'PAT_CONSISTIR_W_IMP_PAT_BEM', qt_bens_lidos_w);

		if	(s_array(z).ds_bem is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281707),1,4000);
		end if;
		
		if	(s_array(z).dt_aquisicao is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281709),1,4000);
		end if;
		
		if	(s_array(z).vl_original is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281710),1,4000);
		end if;
		
		if	(s_array(z).tx_deprec is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281711),1,4000);
		end if;
		
		if	(s_array(z).dt_inicio_uso is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281712),1,4000);
		end if;
		
		if	(s_array(z).ie_imobilizado is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281713),1,4000);
		end if;
		
		if	(s_array(z).ie_situacao is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281714),1,4000);
		end if;
		
		if	(s_array(z).ie_tipo_valor is null) then
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281715),1,4000);
		end if;
		
		if	(s_array(z).cd_centro_custo is not null) then
			select	count(1)
			into	qt_integridade_w
			from	centro_custo
			where	cd_centro_custo		= s_array(z).cd_centro_custo
			and	cd_estabelecimento	= s_array(z).cd_estabelecimento
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
			
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281716),1,4000);
			end if;
		else
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281717),1,4000);
		end if;
		
		if	(s_array(z).cd_conta_contabil is not null) then
			select	count(1)
			into	qt_integridade_w
			from	conta_contabil
			where	cd_conta_contabil	= s_array(z).cd_conta_contabil
			and	cd_empresa		= cd_empresa_w
			and	rownum			= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
			
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281718),1,4000);
			end if;
		else
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281719),1,4000);
		end if;
		
		if	(s_array(z).nr_seq_crit_rateio is not null) then
			select	count(1)
			into	qt_integridade_w
			from	ctb_criterio_rateio
			where	nr_sequencia	= s_array(z).nr_seq_crit_rateio
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
			
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
			
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281720),1,4000);
			end if;
		end if;
			
		if	(s_array(z).cd_moeda is not null) then
			select	count(1)
			into	qt_integridade_w
			from	moeda
			where	cd_moeda	= s_array(z).cd_moeda
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
			
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281723),1,4000);
			end if;
		else
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281724),1,4000);
		end if;
			
		if	(s_array(z).nr_nota_fiscal is not null) then
			select	count(1)
			into	qt_integridade_w
			from	nota_fiscal
			where	nr_nota_fiscal	= s_array(z).nr_nota_fiscal
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281725),1,4000);
			end if;
		end if;
			
		if	(s_array(z).nr_seq_regra_conta is not null) then
			select	count(1)
			into	qt_integridade_w
			from	pat_conta_contabil
			where	nr_sequencia	= s_array(z).nr_seq_regra_conta
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281726),1,4000);
			end if;
		else
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281789),1,4000);
		end if;
			
		if	(s_array(z).nr_seq_local is not null) then
			select	count(1)
			into	qt_integridade_w
			from	pat_local
			where	nr_sequencia	= s_array(z).nr_seq_local
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
			
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281727),1,4000);
			end if;
		else
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281728),1,4000);
		end if;
			
		if	(s_array(z).nr_seq_marca is not null) then
			select	count(1)
			into	qt_integridade_w
			from	pat_marca
			where	nr_sequencia	= s_array(z).nr_seq_marca
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281729),1,4000);
			end if;
		end if;
			
		if	(s_array(z).nr_seq_tipo is not null) then
				
			select 	count(1) 
			into 	qt_integridade_w
			from (
					select 1
					from    pat_grupo_tipo  b,
						pat_tipo_bem    a
					where   b.nr_sequencia  = a.nr_seq_grupo
					and 	b.cd_empresa 	= holding_pck.get_emp_controladora_grupo(null, cd_empresa_w)
					and	nvl(b.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
					and 	a.nr_sequencia  = s_array(z).nr_seq_tipo
					union all
					select 1
					from    pat_grupo_tipo  b,
						pat_tipo_bem    a
					where   b.nr_sequencia  = a.nr_seq_grupo
					and 	b.cd_empresa 	= cd_empresa_w
					and  	nvl(b.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
					and  	a.nr_sequencia  = s_array(z).nr_seq_tipo
				 )
			where 	rownum = 1;

			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
			
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281730),1,4000);
			end if;
		else
			qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
			if	(qt_inconsistencia_w > 1) then
				ds_linha_w	:= chr(13) || chr(10);
			end if;
			
			ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281731),1,4000);
		end if;
			
		if	(s_array(z).cd_pessoa_fisica is not null) then
			select	count(1)
			into	qt_integridade_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= s_array(z).cd_pessoa_fisica
			and	rownum			= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281732),1,4000);
			end if;
		end if;
			
		if	(s_array(z).cd_cgc is not null) then
			select	count(1)
			into	qt_integridade_w
			from	pessoa_juridica
			where	cd_cgc		= s_array(z).cd_cgc
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281733),1,4000);
			end if;
		end if;
		
		if	(s_array(z).cd_bem_superior is not null) then
			select	count(1)
			into	qt_integridade_w
			from	w_imp_pat_bem
			where	cd_bem_externo	= s_array(z).cd_bem_superior
			and	rownum		= 1;
			
			if	(qt_integridade_w = 0) then
				select	count(1)
				into	qt_integridade_w
				from	pat_bem	a
				where	a.cd_bem	= s_array(z).cd_bem_superior
				and	rownum		= 1;
			end if;
			
			if	(qt_integridade_w = 0) then
				qt_inconsistencia_w	:= qt_inconsistencia_w + 1;
		
				if	(qt_inconsistencia_w > 1) then
					ds_linha_w	:= chr(13) || chr(10);
				end if;
				
				ds_consistencia_w	:= substr(ds_consistencia_w || ds_linha_w || wheb_mensagem_pck.get_texto(281745),1,4000);
			end if;
		end if;	
		
		gravar_processo_longo(wheb_mensagem_pck.get_texto(299464,'DS_BEM=' || s_array(z).ds_bem),'PAT_CONSISTIR_W_IMP_PAT_BEM', qt_bens_lidos_w);
		update	w_imp_pat_bem
		set	ds_consistencia	= ds_consistencia_w,
			ie_status_imp	= decode(ds_consistencia_w, null, 'C', 'N')
		where	nr_sequencia	= s_array(z).nr_sequencia;
		
		qt_bens_w	:= qt_bens_w + 1;
		qt_bens_lidos_w	:= qt_bens_lidos_w + 1;
		
		if	(qt_bens_w >= 200) then
			commit;
			
			qt_bens_w	:= 0;
		end if;
		end;
	end loop;
	end;
end loop;
exception
when others then
	update	w_imp_pat_bem
	set	ds_consistencia	= null;
	
	commit;

	ds_erro_w	:= substr(sqlerrm(sqlcode),1,4000);
	
	wheb_mensagem_pck.exibir_mensagem_abort(281735, 'DS_ERRO=' || ds_erro_w);
end;

commit;

end pat_consistir_w_imp_pat_bem;
/