create or replace
procedure liberar_conta_paciente_obs(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) is

begin

update	conta_paciente_observacao
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_liberacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end liberar_conta_paciente_obs;
/