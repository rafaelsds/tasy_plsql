create or replace
procedure pls_obter_cd_cgc_pf_pessoa(
		nr_sequencia_p		number,
		cd_cgc_p		out	varchar2,
		cd_pessoa_fisica_p	out	varchar2) is 

begin

select	cd_cgc,
	cd_pessoa_fisica
into	cd_cgc_p,
	cd_pessoa_fisica_p
from	pls_atendimento
where	nr_sequencia = nr_sequencia_p;

end pls_obter_cd_cgc_pf_pessoa;
/