create or replace
procedure limpa_campos_documentacao(cd_cnpj_p		Varchar2) is 

begin
	
if 	(nvl(cd_cnpj_p,'-1') <> '-1') then
	update     pessoa_juridica                     
	set           cd_pf_resp_tecnico         = '',  
		nr_registro_resp_tecnico   = '',  
		dt_validade_resp_tecnico   = null,  
		nr_alvara_sanitario        = '',  
		dt_validade_alvara_sanit   = null,  
		nr_alvara_sanitario_munic  = '',  
		dt_validade_alvara_munic   = null,  
		nr_certificado_boas_prat   = '',  
		dt_validade_cert_boas_prat = null,  
		nr_autor_func              = '',  
		dt_validade_autor_func     = null,  
		nr_registro_pls            = '',  
		nr_seq_cnae                = null,  
		nr_seq_nat_juridica        = null,  
		ds_orgao_reg_resp_tecnico  = '',
		ds_resp_tecnico		   = '',
		nr_ccm			   = null,
		nr_cei			   = ''
	where   cd_cgc = cd_cnpj_p;	
	
	commit;	
end if;

end limpa_campos_documentacao;
/
