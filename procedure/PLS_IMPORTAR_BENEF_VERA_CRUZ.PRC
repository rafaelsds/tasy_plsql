create or replace
procedure pls_importar_benef_vera_cruz
			(	ds_conteudo_p		Varchar2,
				nm_usuario_p		Varchar2) is 

ds_string_w			varchar2(4000);
ds_campo_w			varchar2(255);
nr_campo_w			number(10)	:= 0;
nr_seq_import_w			number(10);
ie_tipo_linha_w			varchar2(2);
ie_tipo_movimento_w		number(1);
cd_matricula_w			varchar2(30);
ie_tipo_acomodacao_w		number(1);
cd_funcionario_w		number(10);
nm_funcionario_w		varchar2(255);
dt_nascimento_w			date;
nr_identidade_w			varchar2(18);
nr_cpf_w			varchar2(18);
ie_sexo_w			varchar2(1);
dt_aposentado_w			date;
ie_universitario_w		varchar2(1);
ie_deficiente_w			varchar2(1);
cd_parentesco_w			number(5);
dt_admissao_w			date;
ds_departamento_w		varchar2(30);
cd_cep_w			varchar2(8);
ds_logradouro_w			varchar2(50);
nr_endereco_w			number(10);
ds_complemento_w		varchar2(15);
ds_bairro_w			varchar2(30);
ds_cidade_w			varchar2(30);
sg_uf_w				varchar2(2);
nr_ddd_w			varchar2(10);
nr_telefone_w			varchar2(10);
ds_observacao_w			varchar2(255);
cd_plano_w			varchar2(30);
nm_mae_w			varchar2(50);
ie_tipo_segurado_w		number(1);

begin

ds_string_w	:= ds_conteudo_p;

while	(trim(ds_string_w) <> '') loop
	begin
	nr_campo_w	:= nr_campo_w + 1;
	
	select	replace(substr(ds_string_w,1,instr(ds_string_w,',')),'"','')
	into	ds_campo_w
	from	dual;
	
	if	(nr_campo_w = 1) then
		ie_tipo_linha_w	:= ds_campo_w;
	elsif	(nr_campo_w = 2) then
		ie_tipo_movimento_w := ds_campo_w;
	elsif	(nr_campo_w = 3) then
		cd_matricula_w := ds_campo_w;
	elsif	(nr_campo_w = 4) then
		ie_tipo_acomodacao_w := ds_campo_w;
	elsif	(nr_campo_w = 5) then
		cd_funcionario_w := ds_campo_w;
	elsif	(nr_campo_w = 6) then
		nm_funcionario_w := ds_campo_w;
	elsif	(nr_campo_w = 7) then
		dt_nascimento_w := ds_campo_w;
	elsif	(nr_campo_w = 8) then
		nr_identidade_w := ds_campo_w;
	elsif	(nr_campo_w = 9) then
		nr_cpf_w := ds_campo_w;
	elsif	(nr_campo_w = 10) then
		ie_sexo_w := ds_campo_w;
	elsif	(nr_campo_w = 11) then
		dt_aposentado_w := ds_campo_w;
	elsif	(nr_campo_w = 12) then
		ie_universitario_w := ds_campo_w;
	elsif	(nr_campo_w = 13) then
		ie_deficiente_w := ds_campo_w;
	elsif	(nr_campo_w = 14) then
		cd_parentesco_w := ds_campo_w;
	elsif	(nr_campo_w = 15) then
		dt_admissao_w := ds_campo_w;
	elsif	(nr_campo_w = 16) then
		ds_departamento_w := ds_campo_w;
	elsif	(nr_campo_w = 17) then
		cd_cep_w := ds_campo_w;
	elsif	(nr_campo_w = 18) then
		ds_logradouro_w := ds_campo_w;
	elsif	(nr_campo_w = 19) then
		nr_endereco_w := ds_campo_w;
	elsif	(nr_campo_w = 20) then
		ds_complemento_w := ds_campo_w;
	elsif	(nr_campo_w = 21) then
		ds_bairro_w := ds_campo_w;
	elsif	(nr_campo_w = 22) then
		ds_cidade_w := ds_campo_w;
	elsif	(nr_campo_w = 23) then
		sg_uf_w := ds_campo_w;
	elsif	(nr_campo_w = 24) then
		nr_ddd_w := ds_campo_w;
	elsif	(nr_campo_w = 25) then
		nr_telefone_w := ds_campo_w;
	elsif	(nr_campo_w = 26) then
		ds_observacao_w := ds_campo_w;
	elsif	(nr_campo_w = 27) then
		cd_plano_w := ds_campo_w;
	elsif	(nr_campo_w = 28) then
		nm_mae_w := ds_campo_w;
	elsif	(nr_campo_w = 29) then
		ie_tipo_segurado_w := ds_campo_w;
	end if;
	
	select	substr(ds_string_w,instr(ds_string_w,','),length(ds_string_w))
	into	ds_string_w
	from	dual;
	
	end;
end loop;


insert into w_pls_import_benef	(nr_sequencia, ie_tipo_registro, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_linha, ie_tipo_movimento,
				cd_matricula, ie_tipo_acomodacao, cd_funcionario, nm_funcionario,
				dt_nascimento, nr_identidade, nr_cpf, ie_sexo,
				dt_aposentado, ie_universitario, ie_deficiente, cd_parentesco,
				dt_admissao, ds_departamento, cd_cep, ds_logradouro,
				nr_endereco, ds_complemento, ds_bairro, ds_cidade,
				sg_uf, nr_ddd, nr_telefone, ds_observacao,
				cd_plano, nm_mae, ie_tipo_segurado)
			values	(w_pls_import_benef_seq.NextVal, 'R', sysdate, nm_usuario_p,
				sysdate, nm_usuario_p, ie_tipo_linha_w, ie_tipo_movimento_w,
				cd_matricula_w, ie_tipo_acomodacao_w, cd_funcionario_w, nm_funcionario_w,
				dt_nascimento_w, nr_identidade_w, nr_cpf_w, ie_sexo_w,
				dt_aposentado_w, ie_universitario_w, ie_deficiente_w, cd_parentesco_w,
				dt_admissao_w, ds_departamento_w, cd_cep_w, ds_logradouro_w,
				nr_endereco_w, ds_complemento_w, ds_bairro_w, ds_cidade_w,
				sg_uf_w, nr_ddd_w, nr_telefone_w, ds_observacao_w,
				cd_plano_w, nm_mae_w, ie_tipo_segurado_w);
commit;

end pls_importar_benef_vera_cruz;
/