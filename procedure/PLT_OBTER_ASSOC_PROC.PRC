create or replace
procedure plt_obter_assoc_proc (
				cd_estabelecimento_p		number,
				cd_setor_usuario_p		number,
				cd_perfil_p			number,
				nm_usuario_p			varchar2,
				nr_atendimento_p		number,
				dt_inicial_horarios_p		date,
				dt_final_horarios_p		date,					
				dt_validade_limite_p		date,	
				ie_agrupar_acm_sn_p		varchar2,
				ie_prescr_usuario_p		varchar2,
				nr_seq_regra_p			number) is

nr_seq_wadep_w			number(10,0);
nr_prescricao_w			number(14,0);
nr_seq_material_w		number(6,0);
cd_material_w			number(6,0);
ds_material_w			varchar2(255);
ie_acm_sn_w			varchar2(1);
cd_intervalo_w			varchar2(7);
qt_dose_w			number(15,3);
nr_agrupamento_w		number(7,1);
ds_prescricao_w			varchar2(100);
ie_status_w			varchar2(1);
ie_lib_pend_rep_w		varchar2(1);
ie_liberado_w			varchar2(1);
cd_unid_med_qtde_w		varchar2(30);
ie_via_administracao_w		varchar2(5);
ds_interv_prescr_w		varchar2(15);
ie_erro_w			number(5);
ie_copiar_w			varchar2(1);
ds_cor_titulo_w			varchar2(20);
					
cursor c01 is
select	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_seq_material, null), null),	
	cd_material,
	ds_material,
	ie_acm_sn,
	cd_intervalo,
	qt_dose,
	nr_agrupamento,
	ds_prescricao,
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null) ie_status,
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo,
	ie_lib_pend_rep,
	ie_liberado,
	ie_erro
from	(
	select	a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		z.ds_material,
		obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) ie_acm_sn,		
		x.cd_intervalo,
		x.qt_dose,
		decode(obter_se_agrupa_composto(a.nr_prescricao,c.nr_seq_material,x.nr_agrupamento,c.cd_material),'S',x.nr_agrupamento,0) nr_agrupamento,
		substr(adep_obter_um_dosagem_prescr(a.nr_prescricao,c.nr_seq_material,x.ie_acm,x.ie_se_necessario),1,100) ds_prescricao,
		nvl(x.ie_suspenso,'N') ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		substr(obter_desc_intervalo_prescr(x.cd_intervalo),1,15) ds_intervalo,
		substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
		decode(nvl(nvl(a.dt_liberacao,a.dt_liberacao_medico),a.dt_liberacao_farmacia),null,'N','S') ie_liberado,
		x.ie_erro
	from	material z,
		proc_interno w,
		prescr_procedimento y,
		prescr_material x,
		prescr_mat_hor c,
		prescr_medica a
	where	z.cd_material = x.cd_material
	and	z.cd_material = c.cd_material
	and	w.nr_sequencia = y.nr_seq_proc_interno
	and	y.nr_prescricao = x.nr_prescricao
	and	y.nr_sequencia = x.nr_sequencia_proc
	and	y.nr_prescricao = a.nr_prescricao
	and	x.nr_prescricao = c.nr_prescricao
	and	x.nr_sequencia = c.nr_seq_material
	and	x.nr_prescricao = a.nr_prescricao
	and	c.nr_prescricao = y.nr_prescricao
	and	c.nr_prescricao = a.nr_prescricao
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	w.ie_tipo <> 'G'
	and	w.ie_tipo <> 'BS'
	and	nvl(w.ie_ivc,'N') <> 'S'
	and	nvl(w.ie_ctrl_glic,'NC') = 'NC'
	and	y.nr_seq_proc_interno is not null
	and	y.nr_seq_prot_glic is null
	and	y.nr_seq_exame is null
	and	y.nr_seq_solic_sangue is null
	and	y.nr_seq_derivado is null
	and	y.nr_seq_exame_sangue is null
	and	x.nr_seq_kit is null	
	and	x.ie_agrupador = 5
	and	nvl(c.ie_situacao,'A') = 'A'
	and	nvl(c.ie_adep,'S') = 'S'
	and	c.ie_agrupador = 5
	and	nvl(x.ie_checar_adep,'N') = 'S'
	and	(((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 ((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	(((a.dt_liberacao_medico is not null) and
		  (ie_prescr_usuario_p = 'N')) or
		 (a.nm_usuario_original = nm_usuario_p))		 
	group by
		a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		z.ds_material,
		x.ie_acm,
		x.ie_se_necessario,		
		x.cd_intervalo,
		x.qt_dose,
		x.nr_agrupamento,
		x.ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		a.dt_liberacao_medico,
		a.dt_liberacao,
		a.dt_liberacao_farmacia,
		x.ie_erro
	union all
	select	a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		z.ds_material,
		obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) ie_acm_sn,		
		x.cd_intervalo,
		x.qt_dose,
		decode(obter_se_agrupa_composto(a.nr_prescricao,c.nr_seq_material,x.nr_agrupamento,c.cd_material),'S',x.nr_agrupamento,0) nr_agrupamento,
		substr(adep_obter_um_dosagem_prescr(a.nr_prescricao,c.nr_seq_material,x.ie_acm,x.ie_se_necessario),1,100) ds_prescricao,
		nvl(x.ie_suspenso,'N') ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		substr(obter_desc_intervalo_prescr(x.cd_intervalo),1,15) ds_intervalo,
		substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
		decode(nvl(nvl(a.dt_liberacao,a.dt_liberacao_medico),a.dt_liberacao_farmacia),null,'N','S') ie_liberado,
		x.ie_erro
	from	material z,
		prescr_procedimento y,
		prescr_material x,
		prescr_mat_hor c,
		prescr_medica a
	where	z.cd_material = x.cd_material
	and	z.cd_material = c.cd_material
	and	y.nr_prescricao = x.nr_prescricao
	and	y.nr_sequencia = x.nr_sequencia_proc
	and	y.nr_prescricao = a.nr_prescricao
	and	x.nr_prescricao = c.nr_prescricao
	and	x.nr_sequencia = c.nr_seq_material
	and	x.nr_prescricao = a.nr_prescricao
	and	c.nr_prescricao = y.nr_prescricao
	and	c.nr_prescricao = a.nr_prescricao
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	y.nr_seq_proc_interno is null
	and	y.nr_seq_exame is null
	and	y.nr_seq_solic_sangue is null
	and	y.nr_seq_derivado is null
	and	y.nr_seq_exame_sangue is null
	and	x.nr_seq_kit is null	
	and	x.ie_agrupador = 5
	and	nvl(c.ie_situacao,'A') = 'A'
	and	nvl(c.ie_adep,'S') = 'S'
	and	c.ie_agrupador = 5
	and	nvl(x.ie_checar_adep,'N') = 'S'
	and	(((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 ((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	(((a.dt_liberacao_medico is not null) and
		  (ie_prescr_usuario_p = 'N')) or
		 (a.nm_usuario_original = nm_usuario_p))		 
	group by
		a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		z.ds_material,
		x.ie_acm,
		x.ie_se_necessario,		
		x.cd_intervalo,
		x.qt_dose,
		x.nr_agrupamento,
		x.ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		a.dt_liberacao_medico,
		a.dt_liberacao,
		a.dt_liberacao_farmacia,
		x.ie_erro
	)
group by
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_seq_material, null), null),	
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	nr_agrupamento,
	ds_prescricao,
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null),
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo,
	ie_lib_pend_rep,
	ie_liberado,
	ie_erro;

begin

ie_copiar_w 	:= plt_obter_se_item_marcado('IA', nr_seq_regra_p);
ds_cor_titulo_w	:= plt_obter_se_item_marcado('IA', nr_seq_regra_p);

open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_material_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,		
		cd_intervalo_w,
		qt_dose_w,
		nr_agrupamento_w,
		ds_prescricao_w,
		ie_status_w,
		cd_unid_med_qtde_w,
		ie_via_administracao_w,
		ds_interv_prescr_w,
		ie_lib_pend_rep_w,
		ie_liberado_w,
		ie_erro_w;
exit when c01%notfound;
	begin
	select	w_rep_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_rep_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,		
		cd_intervalo,
		qt_item,
		nr_agrupamento,
		ds_prescricao,
		ie_status_item,
		nr_seq_proc_interno,
		cd_unid_med_qtde,
		ie_via_aplicacao,
		ds_interv_prescr,
		ie_pendente_liberacao,
		ie_liberado,
		ie_erro,
		ie_copiar,
		ds_cor_titulo)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'IA',
		nr_prescricao_w,
		nr_seq_material_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,
		cd_intervalo_w,
		qt_dose_w,
		nr_agrupamento_w,
		ds_prescricao_w,
		ie_status_w,
		0,
		cd_unid_med_qtde_w,
		ie_via_administracao_w,
		ds_interv_prescr_w,
		ie_lib_pend_rep_w,
		ie_liberado_w,
		ie_erro_w,
		ie_copiar_w,
		ds_cor_titulo_w);
	end;
end loop;
close c01;
	
end plt_obter_assoc_proc;
/