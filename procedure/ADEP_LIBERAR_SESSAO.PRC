create or replace
procedure adep_liberar_sessao(nm_usuario_p	varchar2) is

begin
exec_sql_dinamico(nm_usuario_p,'truncate table w_adep_t');
exec_sql_dinamico(nm_usuario_p,'truncate table w_adep_horarios_t');
end adep_liberar_sessao;
/