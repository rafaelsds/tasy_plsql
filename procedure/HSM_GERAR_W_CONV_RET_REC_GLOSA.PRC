create or replace
procedure HSM_GERAR_W_CONV_RET_REC_GLOSA
			(nm_usuario_p		varchar2,
			dt_inicial_p		date,
			dt_final_p		date,
			ds_lista_convenio_p	varchar2,
			ds_lista_setor_atend_p	varchar2,
			ds_lista_motivo_glosa_p	varchar2) is

nr_seq_receb_w		number(10);
dt_recebimento_w	date;
vl_recebimento_w	number(15,2);
vl_vinculado_w		number(15,2);	
ie_status_w		varchar2(1);	
vl_imposto_w		number(15,2);
vl_adicional_w		number(15,2);
vl_saldo_w		number(15,2);
vl_glosa_w		number(15,2);
vl_glosa_aceita_w	number(15,2);
vl_recursado_w		number(15,2);
vl_pago_rec_w		number(15,2);
vl_glosa_aceita_proc_w	number(15,2);
vl_glosa_aceita_mat_w	number(15,2);
vl_recursado_proc_w	number(15,2);
vl_pago_rec_proc_w	number(15,2);
vl_recursado_mat_w	number(15,2);
vl_pago_rec_mat_w	number(15,2);
ds_lista_convenio_w	varchar2(4000);
ds_lista_setor_atend_w	varchar2(4000);
ds_lista_motivo_glosa_w	varchar2(4000);
cd_convenio_w		number(10);
qt_filtro_w		number(10);
cd_motivo_glosa_w	number(5);
vl_total_w		number(15,2);
			
cursor 	c01 is
select	a.cd_convenio,
	a.nr_sequencia,
	a.dt_recebimento,
	a.vl_recebimento,
	obter_baixa_conrece(a.nr_sequencia),
	a.ie_status
from	convenio_receb a
where	a.dt_recebimento between dt_inicial_p and fim_dia(dt_final_p)
and	((ds_lista_convenio_w	like '% ' || a.cd_convenio || ' %') or 
	 (ds_lista_convenio_w is null));
	 
cursor	c02 is
select	e.cd_motivo_glosa,
	sum(e.vl_glosa)
from	convenio_retorno_glosa e,
	convenio_retorno_item d,
	convenio_retorno c,
	convenio_ret_receb b
where	b.nr_Seq_retorno	= c.nr_sequencia
and	d.nr_seq_retorno	= c.nr_Sequencia
and	e.nr_Seq_ret_item	= d.nr_sequencia
and	b.nr_seq_receb		= nr_seq_receb_w
and	((ds_lista_setor_atend_w	like '% ' || e.cd_setor_atendimento || ' %') or
	 (ds_lista_setor_atend_w is null))
and	((ds_lista_motivo_glosa_w	like '% ' || e.cd_motivo_glosa || ' %') or
	 (ds_lista_motivo_glosa_w is null))
group by e.cd_motivo_glosa;

begin

delete	from HSM_GERAR_W_CONV_RET_REC_GLOSA
where	nm_usuario = nm_usuario_p;

delete	from HSM_GERAR_W_CONV_RET_REC_GLOSA
where	nm_usuario = nm_usuario_p;

ds_lista_convenio_w			:= replace(' ' || replace(replace(replace(ds_lista_convenio_p, ',', ' '), '(', ' '), ')', ' ') || ' ', '  ',' ');
if	(trim(ds_lista_convenio_w) is null) then
	ds_lista_convenio_w		:= null;
end if;

ds_lista_setor_atend_w			:= replace(' ' || replace(replace(replace(ds_lista_setor_atend_p, ',', ' '), '(', ' '), ')', ' ') || ' ', '  ',' ');
if	(trim(ds_lista_setor_atend_w) is null) then
	ds_lista_setor_atend_w		:= null;
end if;

ds_lista_motivo_glosa_w			:= replace(' ' || replace(replace(replace(ds_lista_motivo_glosa_p, ',', ' '), '(', ' '), ')', ' ') || ' ', '  ',' ');
if	(trim(ds_lista_motivo_glosa_w) is null) then
	ds_lista_motivo_glosa_w		:= null;
end if;

open C01;
loop
fetch C01 into	
	cd_convenio_w,
	nr_seq_receb_w,
	dt_recebimento_w,
	vl_recebimento_w,
	vl_vinculado_w,
	ie_status_w;	
exit when C01%notfound;
	begin
	
	if	(trim(ds_lista_setor_atend_w) is not null) or
		(trim(ds_lista_motivo_glosa_w) is not null) then
		
		select	count(*)
		into	qt_filtro_w
		from 	convenio_retorno_glosa e,
			convenio_retorno_item d,
			convenio_retorno c,
			convenio_ret_receb b
		where	b.nr_Seq_retorno	= c.nr_sequencia
		and	d.nr_seq_retorno	= c.nr_Sequencia
		and	e.nr_Seq_ret_item	= d.nr_sequencia
		and	b.nr_seq_receb		= nr_seq_receb_w
		and	((ds_lista_setor_atend_w	like '% ' || e.cd_setor_atendimento || ' %') or
			 (ds_lista_setor_atend_w is null))
		and	((ds_lista_motivo_glosa_w	like '% ' || e.cd_motivo_glosa || ' %') or
			 (ds_lista_motivo_glosa_w is null));			
	end if;
	
	if	(((trim(ds_lista_setor_atend_w) is not null) or (trim(ds_lista_motivo_glosa_w) is not null)) and 
		 (qt_filtro_w > 0)) or
		(trim(ds_lista_setor_atend_w) is null and trim(ds_lista_motivo_glosa_w) is null) then
	
		vl_saldo_w	:= vl_recebimento_w - vl_vinculado_w;
		
		select	nvl(sum(x.vl_adicional),0)
		into	vl_imposto_w
		from	convenio_receb_adic x
		where	x.nr_seq_receb	= nr_seq_receb_w;
		
		select	nvl(sum(d.vl_adicional),0)
		into	vl_adicional_w
		from 	convenio_retorno_item d,
			convenio_retorno c,
			convenio_ret_receb b
		where	b.nr_Seq_retorno		= c.nr_sequencia
		and	d.nr_seq_retorno		= c.nr_Sequencia
		and	b.nr_seq_receb			= nr_seq_receb_w;	
		
		select	nvl(sum(e.vl_glosa),0) 
		into	vl_glosa_w
		from 	convenio_retorno_glosa e,
			convenio_retorno_item d,
			convenio_retorno c,
			convenio_ret_receb b
		where	b.nr_Seq_retorno	= c.nr_sequencia
		and	d.nr_seq_retorno	= c.nr_Sequencia
		and	e.nr_Seq_ret_item	= d.nr_sequencia
		and	b.nr_seq_receb		= nr_seq_receb_w
		and	((ds_lista_setor_atend_w	like '% ' || e.cd_setor_atendimento || ' %') or 
			(ds_lista_setor_atend_w is null));
		
		select	nvl(sum(e.vl_glosa),0),
			nvl(sum(e.vl_amenor),0),
			nvl(sum(e.vl_pago),0)
		into	vl_glosa_aceita_proc_w,
			vl_recursado_proc_w,
			vl_pago_rec_proc_w
		from 	procedimento_paciente g,
			lote_audit_hist_guia f,
			lote_audit_hist_item e,
			convenio_retorno_item d,
			convenio_retorno c,
			convenio_ret_receb b
		where	b.nr_Seq_retorno	= c.nr_sequencia
		and	d.nr_seq_retorno	= c.nr_Sequencia
		and	g.nr_sequencia		= e.nr_seq_propaci
		and	e.nr_seq_guia		= f.nr_sequencia
		and	f.nr_interno_conta	= d.nr_interno_conta
		and	d.cd_autorizacao	= f.cd_autorizacao
		and	b.nr_seq_receb		= nr_seq_receb_w
		and	((ds_lista_setor_atend_w	like '% ' || g.cd_setor_atendimento || ' %') or 
			(ds_lista_setor_atend_w is null));	
		
		select	nvl(sum(e.vl_glosa),0),
			nvl(sum(e.vl_amenor),0),
			nvl(sum(e.vl_pago),0)
		into	vl_glosa_aceita_mat_w,
			vl_recursado_mat_w,
			vl_pago_rec_mat_w
		from 	material_atend_paciente g,
			lote_audit_hist_guia f,
			lote_audit_hist_item e,
			convenio_retorno_item d,
			convenio_retorno c,
			convenio_ret_receb b
		where	b.nr_Seq_retorno	= c.nr_sequencia
		and	d.nr_seq_retorno	= c.nr_Sequencia
		and	g.nr_sequencia		= e.nr_seq_matpaci
		and	e.nr_seq_guia		= f.nr_sequencia
		and	f.nr_interno_conta	= d.nr_interno_conta
		and	d.cd_autorizacao	= f.cd_autorizacao
		and	b.nr_seq_receb		= nr_seq_receb_w
		and	((ds_lista_setor_atend_w	like '% ' || g.cd_setor_atendimento || ' %') or 
			(ds_lista_setor_atend_w is null));
		
		vl_glosa_aceita_w	:= vl_glosa_aceita_proc_w + vl_glosa_aceita_mat_w;
		vl_recursado_w		:= vl_recursado_proc_w + vl_recursado_mat_w;
		vl_pago_rec_w		:= vl_pago_rec_proc_w + vl_pago_rec_mat_w;	
		
		insert into HSM_W_CONV_RET_REC_GLOSA
			(nm_usuario,
			dt_atualizacao,
			nr_seq_receb,
			dt_recebimento,
			vl_recebimento,
			vl_vinculado,
			vl_imposto,
			vl_adicional,
			vl_saldo,
			vl_glosa,
			vl_glosa_aceita,
			vl_recursado,
			vl_pago_rec,
			ie_status_receb,
			cd_convenio)
		values	(nm_usuario_p,
			sysdate,
			nr_seq_receb_w,
			dt_recebimento_w,
			vl_recebimento_w,
			vl_vinculado_w,
			vl_imposto_w,
			vl_adicional_w,
			vl_saldo_w,
			vl_glosa_w,
			vl_glosa_aceita_w,
			vl_recursado_w,
			vl_pago_rec_w,
			ie_status_w,
			cd_convenio_w);
			
		open C02;
		loop
		fetch C02 into	
			cd_motivo_glosa_w,
			vl_total_w;
		exit when C02%notfound;
			begin
			
			insert	into HSM_W_CONV_RET_REC_GLOSA
				(nm_usuario,
				cd_motivo_glosa,
				vl_total)
			values	(nm_usuario_p,
				cd_motivo_glosa_w,
				vl_total_w);
			
			end;
		end loop;
		close C02;			
			
	end if;
	
	end;
end loop;
close C01;

commit;

end HSM_GERAR_W_CONV_RET_REC_GLOSA;
/