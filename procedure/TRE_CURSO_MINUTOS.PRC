create or replace
procedure tre_curso_minutos(nr_sequencia_p 	number,
			nr_seq_p		number,
			qt_minutos_p		number,
			nm_usuario_p		varchar2) is 

begin

update   tre_curso_modulo 
set      qt_carga_horaria = (qt_minutos_p / 60 )
where    nr_sequencia = nr_sequencia_p  
and      nr_seq_curso = nr_seq_p;

commit;

end tre_curso_minutos;
/







