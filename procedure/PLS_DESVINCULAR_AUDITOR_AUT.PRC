create or replace
procedure pls_desvincular_auditor_aut
			(	nm_usuario_p		varchar2) is

cd_estabelecimento_w		number(10);
qt_min_limite_analise_w		number(10);
nr_seq_grupo_atual_w		number(10);
nr_seq_auditoria_w		number(10);
nm_usuario_exec_w		varchar2(50);
dt_inicio_auditoria_w		date;
qt_grupo_w			number(10);
qt_item_pendente_w		number(10);

Cursor C00 is
	select	cd_estabelecimento
	from	pls_outorgante;

cursor C01 is
	select	nr_sequencia
	from	pls_auditoria
	where	cd_estabelecimento = cd_estabelecimento_w
	and	dt_liberacao is null;

begin
cd_estabelecimento_w := 0;
open C00;
loop
fetch C00 into	
	cd_estabelecimento_w;
exit when C00%notfound;
	begin
	
	begin
		select	nvl(qt_min_limite_analise,0)
		into	qt_min_limite_analise_w
		from	pls_param_analise_aut
		where	cd_estabelecimento = cd_estabelecimento_w;
	exception
	when others then
		qt_min_limite_analise_w := 0;
	end;
	
	if	(qt_min_limite_analise_w > 0) then
		open C01;
		loop
		fetch C01 into	
			nr_seq_auditoria_w;
		exit when C01%notfound;
			begin
			nr_seq_grupo_atual_w := pls_obter_grupo_analise_atual(nr_seq_auditoria_w);		
			
			begin
				select	nm_usuario_exec,
					dt_inicio_auditoria
				into	nm_usuario_exec_w,
					dt_inicio_auditoria_w
				from	pls_auditoria_grupo
				where	nr_sequencia = nr_seq_grupo_atual_w;
			exception
			when others then
				nm_usuario_exec_w := 'X';
			end;

			if	(nvl(nm_usuario_exec_w,'X') <> 'X') and
				((dt_inicio_auditoria_w + (qt_min_limite_analise_w/24/60)) < sysdate)	then
				pls_desfazer_assumir_grupo_aud(nr_seq_grupo_atual_w,nr_seq_auditoria_w,'L',nm_usuario_p);
			end if;
			end;
		end loop;
		close C01;
	end if;
	end;
end loop;
close C00;

commit;

end pls_desvincular_auditor_aut;
/
