create or replace
procedure  far_gerar_itens_venda(	nr_seq_venda_p	in out	number,
					cd_pessoa_fisica_p	varchar2,
					cd_cgc_p		varchar2,	
					nm_usuario_p		varchar2) is


qt_material_w		w_far_venda_item.qt_material%type;
vl_unitario_w		w_far_venda_item.vl_unitario%type;
pr_tributo_w		w_far_venda_item.pr_tributo%type;
vl_total_w		w_far_venda_item.vl_total%type;
cd_material_w		w_far_venda_item.cd_material%type;
nr_seq_lote_w		w_far_venda_item.nr_seq_lote%type;

seq_venda_item_w		far_venda_item.nr_sequencia%type;
seq_far_venda_w		far_venda.nr_sequencia%type;

cd_fornecedor_w		far_venda_item.cd_fornecedor%type;
cd_procedimento_w	far_venda_item.cd_procedimento%type;
ie_origem_proced_w	far_venda_item.ie_origem_proced%type;
ie_consignado_w		material.ie_consignado%type;
cd_local_estoque_w	local_estoque.cd_local_estoque%type;
ie_tipo_saldo_w		varchar2(15);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type := obter_estabelecimento_ativo;
cd_condicao_pagamento_w	far_venda.cd_condicao_pagamento%type;

cd_local_nf_far_venda_w		local_estoque.cd_local_estoque%type;
cd_cond_pagto_far_venda_w 	far_venda.cd_condicao_pagamento%type;

cursor c01 is
	select	qt_material,
		vl_unitario,
		pr_tributo,
		vl_total,
		decode(cd_material,0,null,cd_material),
		decode(nr_seq_lote,0,null,nr_seq_lote),
		decode(cd_fornecedor,0,null, cd_fornecedor),
		decode(cd_procedimento,0,null,cd_procedimento),
		decode(ie_origem_proced,0,null,ie_origem_proced)
	from	w_far_venda_item;

begin

begin
select obter_valor_param_usuario(1608, 5, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),
       obter_valor_param_usuario(1608, 7, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento)
into   cd_local_nf_far_venda_w,
       cd_cond_pagto_far_venda_w
from   dual;
exception
when others then
	cd_local_nf_far_venda_w   := null;
	cd_cond_pagto_far_venda_w := null;
end;

if	(nr_seq_venda_p is null) then
	begin
	
	begin
	select	cd_cond_pagto_far_venda
	into	cd_condicao_pagamento_w
	from	parametro_estoque
	where	cd_estabelecimento = cd_estabelecimento_w
	and	rownum = 1;
	exception
	when others then
		cd_condicao_pagamento_w	:=	null;
	end;
	
	if (cd_cond_pagto_far_venda_w is not null) then
		cd_condicao_pagamento_w := cd_cond_pagto_far_venda_w;
	end if;
	
	select	far_venda_seq.nextval
	into	seq_far_venda_w
	from	dual;

	nr_seq_venda_p := seq_far_venda_w;
	
	insert into far_venda(	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		cd_cgc,
		dt_venda,
		cd_pessoa_vendedor,
		cd_estabelecimento,
		cd_condicao_pagamento)
	values(	seq_far_venda_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_p,
		cd_cgc_p,
		sysdate,
		obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'C'),
		cd_estabelecimento_w,
		cd_condicao_pagamento_w);
	commit;
	
	end;
end if; 

open c01;
loop
fetch c01 into
	qt_material_w,
	vl_unitario_w,
	pr_tributo_w,
	vl_total_w,
	cd_material_w,
	nr_seq_lote_w,
	cd_fornecedor_w,
	cd_procedimento_w,
	ie_origem_proced_w;
exit when c01%notfound;
	begin

	if	(cd_material_w is not null) then
		begin
		if	(cd_fornecedor_w is null) then
			begin
			select	ie_consignado
			into	ie_consignado_w
			from	material
			where	cd_material = cd_material_w;
			
			if	(ie_consignado_w = '1') then
				select	max(cd_cgc_fornec)
				into	cd_fornecedor_w
				from	material_lote_fornec
				where	nr_sequencia = nr_seq_lote_w;
			elsif	(ie_consignado_w = '2') then
				begin
				select	max(cd_local_nf_far_venda)
				into	cd_local_estoque_w
				from	parametro_estoque
				where	cd_estabelecimento = cd_estabelecimento_w;
				
				if (cd_local_nf_far_venda_w is not null) then
					cd_local_estoque_w := cd_local_nf_far_venda_w;
				end if;
				
				obter_fornec_consig_ambos(
					cd_estabelecimento_w,
					cd_material_w,
					nr_seq_lote_w,
					cd_local_estoque_w,
					ie_tipo_saldo_w,
					cd_fornecedor_w);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	
	select	far_venda_item_seq.nextval
	into	seq_venda_item_w
	from	dual;

	insert into far_venda_item (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_venda,
		qt_material,
		vl_unitario,
		vl_desconto,
		cd_material,
		pr_desconto,
		vl_total,
		nr_seq_lote_fornec,
		cd_procedimento,
		ie_origem_proced,
		cd_fornecedor)
	values(	seq_venda_item_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_venda_p,
		qt_material_w,
		vl_unitario_w,
		0,
		cd_material_w,
		0,	
		vl_total_w,
		nr_seq_lote_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_fornecedor_w);

	far_gerar_tributo_item(nr_seq_venda_p,seq_venda_item_w,nm_usuario_p);
	far_atualizar_total_item(seq_venda_item_w, nm_usuario_p);
	end;
end loop;
close c01;
commit;
end far_gerar_itens_venda;
/
