create or replace procedure philips_business_log(cd_log_p in varchar2, ds_log_p in varchar2) is

begin
	if cd_log_p is not null then
		philips_business_log_pck.write_log(cd_log_p, substr(ds_log_p, 1, 4000));
	end if;	
end philips_business_log;
/
