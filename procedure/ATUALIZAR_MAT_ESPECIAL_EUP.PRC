create or replace
procedure Atualizar_Mat_Especial_EUP	(	nr_prescricao_p	number,
					nr_sequencia_p	number,
					ds_material_especial_p	varchar2,
					nm_usuario_p	varchar2) is
begin

update	prescr_procedimento 
set	ds_material_especial	= substr(ds_material_especial_p,1,255),
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p
and	nr_prescricao		= nr_prescricao_p;

commit;

end Atualizar_Mat_Especial_EUP;
/