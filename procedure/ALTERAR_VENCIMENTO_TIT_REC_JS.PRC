create or replace
procedure ALTERAR_VENCIMENTO_TIT_REC_JS(	nr_titulo_p	number,
						dt_vencimento_p	date) is

begin

update	titulo_receber
set	dt_pagamento_previsto	= dt_vencimento_p
where	nr_titulo			= nr_titulo_p;

commit;

end	ALTERAR_VENCIMENTO_TIT_REC_JS;
/