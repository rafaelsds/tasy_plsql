create or replace
procedure item_entrada_terceiro(cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				nm_usuario_p		varchar2) is

contador_w			number(10) := 0;
qt_registros_W			number(10);
ds_arquivo_ww			varchar2(3000);
nr_sequencia_w			varchar2(10);	
cd_modelo_documento_w		varchar2(10);
cd_serie_nf_w			varchar2(10); 
nr_nota_fiscal_w		varchar2(255);
dt_emissao_documento_w		varchar2(8);
cd_participante_w		varchar2(14);
nr_item_nf_w			varchar2(10);
cd_item_servico_w		varchar2(20);
ds_item_servico_w		varchar2(255);
cd_fiscal_operacao_w		varchar2(255);
cd_natureza_operacao_w		varchar2(10);
ds_classificacao_fiscal_w	varchar2(255);
qt_item_nf_w			varchar2(18);
cd_unidade_medida_compra_w	varchar2(30);
vl_unitario_item_nf_w		varchar2(20);
vl_total_item_nf_w		varchar2(20);
vl_desconto_w			varchar2(20);
cd_tributacao_w			varchar2(10);
vl_aliquota_ipi_w		varchar2(20);
vl_base_calculo_ipi_w		varchar2(20);
vl_ipi_w			varchar2(20);
ds_situ_trib_estadual_w		varchar2(255);
cd_indicador_trib_icms_w	varchar2(20);
vl_aliquota_icms_w		varchar2(20);
vl_base_calc_icms_w		varchar2(20);
vl_icms_w			varchar2(20);
vl_base_calc_icmsst_w		varchar2(20);
vl_icmsst_w			varchar2(20);
cd_mov_fisica_w			varchar2(10);  
nr_seq_blu_w			varchar2(10);					

cursor	c01 is
SELECT	n.nr_sequencia														nr_sequencia,
	LPAD(' ',2,' ')  													cd_modelo_documento,
	RPAD(n.cd_serie_nf,5,' ')												nr_serie_nf,
	LPAD(n.nr_nota_fiscal,6,'0')												nr_nota_fiscal,
	LPAD(SUBSTR(TO_CHAR(n.dt_emissao,'ddmmyyyy'),1,8),8,'0')								dt_emissao_documento,
	LPAD(nvl(SUBSTR(DECODE(DECODE(n.ie_tipo_nota,'EF','E','EN','E','EP','E','SD','S','SE','S','SF','S','ST','S'),'E',
	nvl(n.cd_cgc_emitente,obter_cpf_pessoa_fisica(n.cd_pessoa_fisica)),'S',DECODE(n.cd_cgc,NULL,obter_cpf_pessoa_fisica(n.cd_pessoa_fisica),nvl(n.cd_cgc_emitente,obter_cpf_pessoa_fisica(n.cd_pessoa_fisica)))),1,14),' '),14,' ') 	cd_participante,
	LPAD(i.nr_item_nf,3,'0') 												nr_item_nf,
	LPAD(NVL(SUBSTR(DECODE(i.cd_material,NULL,obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'),obter_procedimento_nfse(n.nr_sequencia,'O')),'CD'),i.cd_material),1,20),' '),20,' ') cd_item_servico,
	RPAD(NVL(DECODE(i.cd_material,NULL,obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'),obter_procedimento_nfse(n.nr_sequencia,'O')),'DS'),obter_desc_material(i.cd_material)),' '),45, ' ') ds_item_servico,
	LPAD(SUBSTR(NVL(ELIMINA_CARACTERES_ESPECIAIS(obter_dados_natureza_operacao(n.cd_natureza_operacao,'CF')),' '),1,10),3,' ') cd_fiscal_operacao,
	LPAD(nvl(to_char(n.cd_natureza_operacao),' '),6,' ') 											cd_natureza_operacao,
	LPAD(' ',8,' ')														ds_classificacao_fiscal,
	LPAD(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(i.qt_item_nf,0),'999,999,999,990.999'),',',''),'.',''),' ',''),17,' ') 		qt_item_nf,
	RPAD(NVL(to_char(i.cd_unidade_medida_compra),' '),3,' ') 									cd_unidade_medida_compra,
	LPAD(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(i.vl_unitario_item_nf,0),'999,999,999,990.9999'),',',''),'.',''),' ',''),17,' ')	vl_unitario_item_nf,
	LPAD(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(i.vl_total_item_nf,0),'999,999,999,990.99'),',',''),'.',''),' ',''),17,' ')		vl_total_item_nf,
	LPAD(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(i.vl_desconto,0),'999,999,999,990.99'),',',''),'.',''),' ',''),17,' ')		vl_desconto,
	LPAD(' ',1,' ') 														cd_tributacao,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(nfe_obter_valor_trib_item(n.nr_sequencia, i.nr_item_nf, 'IPI', 'TX'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),5,' ')vl_aliquota_ipi,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(nfe_obter_valor_trib_item(n.nr_sequencia, i.nr_item_nf, 'IPI', 'BC'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),17,' ')	vl_base_calculo_ipi,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(nfe_obter_valor_trib_item(n.nr_sequencia, i.nr_item_nf, 'IPI', 'TRIB'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),17,' ')	vl_ipi,
	LPAD(' ',3,' ') 													ds_situ_trib_estadual,
	LPAD(' ',1,' ') 													cd_indicador_trib_icms,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ICMS'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),5,' ')vl_aliquota_icms,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ICMS'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),17,' ')	vl_base_calc_icms,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ICMS'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),17,' ')	vl_icms,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ICMSST'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),17,' ')	vl_base_calc_icmsst,
	LPAD(nvl(REPLACE(REPLACE(REPLACE(TO_CHAR(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ICMSST'),0),'999,999,999,990.99'),',',''),'.',''),' ',''),' '),17,' ')	vl_icmsst,
	LPAD(' ',1,' ') 													cd_mov_fisica
FROM     nota_fiscal n,
	nota_fiscal_item i,
	operacao_nota o
WHERE    i.nr_sequencia = n.nr_sequencia
and	o.cd_operacao_nf = n.cd_operacao_nf
and	o.ie_servico = 'N'
AND	n.ie_tipo_nota IN ('EF','EN')
and	n.ie_situacao not in ('3','2')
and	n.dt_emissao between to_date(to_char(dt_inicio_p,'dd/mm/yyyy'),'dd/mm/yyyy') and fim_dia(to_date(to_char(dt_fim_p,'dd/mm/yyyy'),'dd/mm/yyyy'))
ORDER BY n.nr_sequencia, i.nr_item_nf;




begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_modelo_documento_w,
	cd_serie_nf_w,
	nr_nota_fiscal_w,
	dt_emissao_documento_w,
	cd_participante_w,
	nr_item_nf_w,
	cd_item_servico_w,
	ds_item_servico_w,
	cd_fiscal_operacao_w,
	cd_natureza_operacao_w,
	ds_classificacao_fiscal_w,
	qt_item_nf_w,
	cd_unidade_medida_compra_w,
	vl_unitario_item_nf_w,
	vl_total_item_nf_w,
	vl_desconto_w, 
	cd_tributacao_w,
	vl_aliquota_ipi_w,
	vl_base_calculo_ipi_w,
	vl_ipi_w,
	ds_situ_trib_estadual_w,
	cd_indicador_trib_icms_w,
	vl_aliquota_icms_w,
	vl_base_calc_icms_w,
	vl_icms_w,
	vl_base_calc_icmsst_w,
	vl_icmsst_w,
	cd_mov_fisica_w;
exit when c01%notfound;
	begin
	
	ds_arquivo_ww	:=	cd_modelo_documento_w		|| cd_serie_nf_w		||
				nr_nota_fiscal_w		|| dt_emissao_documento_w	|| cd_participante_w ||
				nr_item_nf_w			|| cd_item_servico_w		||
				ds_item_servico_w		|| cd_fiscal_operacao_w		||
				cd_natureza_operacao_w		|| ds_classificacao_fiscal_w	||
				qt_item_nf_w			|| cd_unidade_medida_compra_w	||
				vl_unitario_item_nf_w		|| vl_total_item_nf_w		||
				vl_desconto_w			|| cd_tributacao_w		||
				vl_aliquota_ipi_w		|| vl_base_calculo_ipi_w	||
				vl_ipi_w			|| ds_situ_trib_estadual_w	||
				cd_indicador_trib_icms_w	|| vl_aliquota_icms_w		||
				vl_base_calc_icms_w		|| vl_icms_w			||
				vl_base_calc_icmsst_w		|| vl_icmsst_w			||
				cd_mov_fisica_w;	

insert into w_inss_direp_arquivo ( nr_sequencia, 
				cd_estabelecimento, 
				dt_atualizacao, 
				nm_usuario, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				ds_arquivo_w)
		values		(w_inss_direp_arquivo_seq.nextval,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_ww);
				

	end;
end loop;
close c01;

commit;

end item_entrada_terceiro;
/
