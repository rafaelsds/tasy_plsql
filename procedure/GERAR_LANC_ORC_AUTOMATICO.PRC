CREATE OR REPLACE
PROCEDURE GERAR_LANC_ORC_AUTOMATICO (
				NR_ATENDIMENTO_P		Number,
				nr_seq_orcamento_p		number,
				NR_SEQ_EVENTO_P			Number,
				nr_seq_item_p			number,
				NM_USUARIO_P			Varchar2,
				nr_seq_medicacao_p		number default null,
				cd_protocolo_p			number default null,
				ds_dia_ciclo_p			Varchar2 default null) IS

/*
Se evento for diferente de Evolu? M?ca (10)
nr_sequencia_p - sequ?ia do ProPaci;
sen?nr_sequencia_p - sequ?ia da Evolu?;
*/
cd_estabelecimento_w		NUMBER(04,0);
cd_convenio_w			NUMBER(05,0);
cd_categoria_w			VARCHAR2(10 char);
ie_Medico_atendimento_w		VARCHAR2(1 char);
ie_Local_Estoque_w		VARCHAR2(1 char);
cd_cgc_w			Varchar2(14 char);
cd_procedimento_w          		number(15)	:= 0;
ie_origem_proced_w		number(10)	:= 0;
cd_material_w      			number(6)		:= 0;
nr_sequencia_w         		number(10)	:= 0;
nr_seq_regra_w         		number(10)	:= 0;
cd_unid_medida_w			Varchar2(30 char);
cd_medico_executor_w		Varchar2(10 char);
cd_setor_regra_w			Number(05,0)	:= 0;
qt_lancamento_w			Number(09,3)	:= 0;

cd_proc_propaci_w			NUMBER(15);
nr_seq_exame_w			number(10);
cd_area_proc_w			NUMBER(15);
cd_especial_proc_w		NUMBER(15);
cd_grupo_proc_w			NUMBER(15);
dt_execucao_w			date;
cd_edicao_amb_w			Number(06,0);
qt_proc_alta_w			NUMBER(8);
qt_mat_alta_w			NUMBER(8);
qt_procedimento_w			number(9,3) 	:= 1;
ie_tipo_convenio_w			number(2,0);
qt_reg_w				Number(15,0);
ie_funcao_medico_w		Number(3);
ie_origem_proc_w			number(10,0);
qt_material_w			number(05,0);
tx_procedimento_w			number(15,4);
nr_seq_proc_interno_w		number(10,0);
ie_dispara_kit_w			varchar2(01 char);
cd_kit_material_w			number(10,0);
cd_medico_w			varchar2(10 char);
ie_tipo_atendimento_w		number(3,0)	:= 0;
nr_seq_item_w			number(10,0);
nr_seq_proc_int_w			number(10,0);
dt_atual_trunc_w			date		:= trunc(sysdate);

ie_lado_w			varchar2(01 char)	:= 'X';
ie_adic_orcamento_w		varchar2(1 char);

ie_autorizacao_w			varchar2(5 char);
ie_verifica_se_pacote_w		varchar2(1 char);
nr_seq_pacote_w			number(10);
ie_consiste_duplicidade_w		varchar2(1 char);
cd_plano_w			orcamento_paciente.cd_plano%type;
cd_tipo_acomodacao_w		orcamento_paciente.cd_tipo_acomodacao%type;
nr_seq_proc_interno_aux_w		number(10,0);
ds_ret_w				number(10,0);
ds_erro_ww			Varchar2(255 char);
ie_situacao_w   varchar2(1 char);

cd_funcao_w			regra_Lanc_Automatico.cd_funcao%type;
nr_seq_classificacao_w 		classificacao_atendimento.nr_sequencia%type;

cd_setor_atendimento_w  	ORCAMENTO_PACIENTE_PROC.cd_setor_atendimento%type;
cd_setor_orcamento_w 		regra_Lanc_Aut_pac.cd_setor_orcamento%type;
ie_clinica_w			atendimento_paciente.ie_clinica%type;

cd_motivo_alta_w		atendimento_paciente.cd_motivo_alta%type := 0;
ie_carater_atend_w		orcamento_paciente.ie_carater_inter_sus%type;
dia_semana_w			number(1)	:= 0;
dt_orcamento_w			orcamento_paciente.dt_orcamento%type;
ie_dia_feriado_w		varchar2(1 char) := 'N';
cd_procedencia_w		ATENDIMENTO_PACIENTE.cd_procedencia%type := 0;
nr_seq_forma_laudo_w		atendimento_paciente.nr_seq_forma_laudo%type;
cd_municipio_ibge_w		compl_pessoa_fisica.cd_municipio_ibge%type := 'X';
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
qt_reg_ibge_w			number(10);
ie_credenciado_w		varchar2(1 char);
cd_medico_filtro_w		orcamento_paciente.cd_medico%type;
cd_tipo_anestesia_w		orcamento_paciente.ie_tipo_anestesia%type;
ie_regra_w			orcamento_paciente_proc.ie_regra_plano%type;

cursor	c00 ( nr_seq_orcamento_pc number, nr_seq_orc_proc_pc number) is
	select	b.cd_grupo_proc,
		b.cd_especialidade,
		b.cd_area_procedimento,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_seq_exame,
		nvl(a.nr_seq_proc_interno, 0) nr_seq_proc_interno,
		a.nr_sequencia,
		nvl(a.ie_lado,'X') ie_lado,
		a.cd_setor_atendimento,
		a.cd_medico
	from	estrutura_procedimento_v b,
		orcamento_paciente_proc  a
	where	a.nr_sequencia_orcamento	= nr_seq_orcamento_pc
	and	a.cd_procedimento		= b.cd_procedimento
	and	a.ie_origem_proced		= b.ie_origem_proced
	and	a.nr_sequencia	= nvl(nr_seq_orc_proc_pc, a.nr_sequencia);

CURSOR C01 IS
select a.nr_sequencia,
	a.cd_setor_atendimento
from 	regra_Lanc_Automatico a
where	1=1
and     a.cd_estabelecimento				= cd_estabelecimento_w
and	nvl(a.cd_convenio, cd_convenio_w)		= cd_convenio_w
and	nvl(a.ie_tipo_convenio, ie_tipo_convenio_w)	= ie_tipo_convenio_w
and	(a.nr_seq_evento				= nr_seq_evento_p)
and	(nvl(a.cd_procedimento,cd_proc_propaci_w)	= cd_proc_propaci_w or cd_proc_propaci_w is null)
and	(nvl(a.ie_origem_proced,ie_origem_proc_w)	= ie_origem_proc_w or ie_origem_proc_w is null)
and	(nvl(a.cd_area_procedimento,cd_area_proc_w)	= cd_area_proc_w 	or cd_area_proc_w is null)
and	(nvl(a.cd_especialidade_proc,cd_especial_proc_w)= cd_especial_proc_w or cd_especial_proc_w is null)
and	(nvl(a.cd_grupo_proc,cd_grupo_proc_w)		= cd_grupo_proc_w or cd_grupo_proc_w is null)
and	((nvl(a.cd_edicao_amb,cd_edicao_amb_w)		= cd_edicao_amb_w) or (cd_edicao_amb_w is null))
and	nvl(a.nr_seq_exame,nvl(nr_seq_exame_w,0))	= nvl(nr_seq_exame_w,0)
and	nvl(a.nr_seq_classificacao,nvl(nr_seq_classificacao_w,0))	= nvl(nr_seq_classificacao_w,0)
and	nvl(a.cd_categoria, cd_categoria_w)		= cd_categoria_w
and	nvl(a.cd_motivo_alta, nvl(cd_motivo_alta_w,0))		= nvl(cd_motivo_alta_w,0)
and	(nvl(a.nr_seq_proc_interno, nvl(nr_seq_proc_interno_w,0)) 	= nvl(nr_seq_proc_interno_w,0))
and	nvl(cd_plano_convenio, nvl(cd_plano_w,'0'))	= nvl(cd_plano_w,'0')
and 	nvl(ie_clinica, nvl(ie_clinica_w,0)) = nvl(ie_clinica_w,0)
and 	nvl(cd_tipo_acomodacao,nvl(cd_tipo_acomodacao_w,0)) = nvl(cd_tipo_acomodacao_w,0)
and	(nvl(a.ie_carater_inter, nvl(ie_carater_atend_w, 0))	= nvl(ie_carater_atend_w, 0))
and	((nvl(dt_dia_semana, dia_semana_w) = dia_semana_w) or (dt_dia_semana = 9))
and 	(nvl(ie_feriado,'N')	= 'N' or nvl(ie_feriado,'N') = ie_dia_feriado_w)
and	(nvl(cd_procedencia,nvl(cd_procedencia_w,0))	= nvl(cd_procedencia_w,0))
and	(nvl(nr_seq_forma_laudo, nvl(nr_seq_forma_laudo_w,0)) = nvl(nr_seq_forma_laudo_w,0) )
and	(nvl(cd_municipio_ibge,cd_municipio_ibge_w) = cd_municipio_ibge_w)
and 	(nvl(dt_orcamento_w, sysdate) between
			pkg_date_utils.Get_DateTime(NVL(dt_orcamento_w, SYSDATE), NVL(hr_inicial, pkg_date_utils.get_Time(00,00,00)))
					and
			pkg_date_utils.Get_DateTime(NVL(dt_orcamento_w, SYSDATE), NVL(hr_final, pkg_date_utils.get_Time(23,59,59))))
and 	(nvl(a.cd_medico, nvl(cd_medico_w,'0')) =  nvl(cd_medico_w,'0'))
and 	(nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) =  nvl(ie_tipo_atendimento_w,0))
and	((nvl(ie_lado_w, 'X')	= 'X') or (nvl(ie_lado, ie_lado_w) = ie_lado_w))
and	qt_reg_w					= 0
and	ie_situacao					= 'A'
and	dt_atual_trunc_w between nvl(dt_inicio_vigencia,dt_atual_trunc_w) and fim_dia(nvl(dt_final_vigencia,dt_atual_trunc_w))
and	nvl(a.cd_funcao,cd_funcao_w)  = cd_funcao_w
and 	((cd_setor_atendimento_w is null) or (nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)))
and 	((nvl(a.ie_credenciado,'T') = 'T') or (a.ie_credenciado 	= ie_credenciado_w ))
and	((a.cd_tipo_anestesia is null) or (cd_tipo_anestesia_w is null) or (a.cd_tipo_anestesia = cd_tipo_anestesia_w))
and	((obter_se_contido_char(nvl(ds_dia_ciclo,'0'),nvl(ds_dia_ciclo_p, '0')) = 'S')
	or (nr_seq_evento_p <> 192)
	or (obter_se_contido_char(nvl(ds_dia_ciclo,'0'),'0') = 'S'))
and 	(nr_seq_evento_p <> 192 or
	((a.cd_protocolo is null or a.cd_protocolo = nvl(cd_protocolo_p, 0))
	and (a.nr_seq_medicacao is null or a.nr_seq_medicacao = nvl(nr_seq_medicacao_p, 0))))
order by
	nvl(a.cd_medico,0),
	nvl(a.cd_setor_atendimento,0),
	nvl(a.ie_tipo_atendimento,0),
	nvl(a.cd_convenio,0),
	nvl(a.ie_tipo_convenio,0),
	nvl(a.cd_edicao_amb,0),
	nvl(a.nr_Seq_proc_interno,0),
	nvl(a.cd_procedimento,0),
	nvl(a.cd_grupo_proc,0),
	nvl(a.cd_especialidade_proc,0),
	nvl(a.cd_area_procedimento,0),
	nvl(a.nr_seq_exame,0),
	nvl(a.ie_clinica,0),
	nvl(a.ie_lado,' '),
	nvl(a.cd_funcao,0),
	nvl(a.cd_plano_convenio,'0'),
	nvl(cd_procedencia,0);

CURSOR C02 IS
	select 	a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_seq_exame,
		a.cd_material,
		substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
		decode(a.ie_quantidade, 'I', nvl(qt_lancamento,1), nvl(qt_lancamento,1) * qt_procedimento_w),
		nvl(a.ie_medico_atendimento,'N'),
		nvl(a.ie_local_estoque,'N'),
		nvl(a.ie_funcao_medico,0),
		nvl(a.tx_procedimento,100),
		0,
		nvl(a.ie_adic_orcamento,'N'),
		nvl(nvl(a.cd_setor_orcamento,a.cd_setor_item),cd_setor_atendimento_w)
	from 	material b, regra_Lanc_Aut_pac a
	where	a.nr_seq_regra  	= nr_seq_regra_w
	and	nr_seq_regra_w 	<> 0
	and	b.ie_situacao 	= 'A'
	and	a.cd_material 	is not null
	and	a.cd_material 	= b.cd_material
	and	qt_reg_w		= 0
	union all
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_exame,
		cd_material,
		'XX',
		decode(ie_quantidade, 'I', nvl(qt_lancamento,1), nvl(qt_lancamento,1) * qt_procedimento_w),
		nvl(ie_medico_atendimento,'N'),
		nvl(ie_local_estoque,'N'),
		nvl(ie_funcao_medico,0),
		nvl(tx_procedimento,100),
		nr_seq_proc_interno,
		nvl(ie_adic_orcamento,'N'),
		nvl(nvl(cd_setor_orcamento,cd_setor_item),cd_setor_atendimento_w)
	from 	regra_Lanc_Aut_pac
	where	nr_seq_regra  	= nr_seq_regra_w
	and	nr_seq_regra_w <> 0
	and	(((cd_procedimento is not null) and (ie_origem_proced is not null)) or (nr_seq_proc_interno is not null) or (nr_seq_exame is not null))
	and	qt_reg_w		= 0;

BEGIN

nr_seq_regra_w			:= 0;
ie_lado_w			:= 'X';

begin


select 	a.cd_estabelecimento,
	b.cd_cgc,
	a.cd_convenio,
	c.ie_tipo_convenio,
	a.cd_categoria,
	a.cd_medico,
	a.cd_plano,
	a.cd_tipo_acomodacao,
	a.nr_seq_classificacao,
	a.dt_orcamento,
	a.ie_clinica,
	a.ie_carater_inter_sus,
	a.cd_procedencia,
	a.cd_pessoa_fisica,
	a.cd_setor_atendimento,
	a.ie_tipo_atendimento,
	a.ie_tipo_anestesia
into	cd_estabelecimento_w,
	cd_cgc_w,
	cd_convenio_w,
	ie_tipo_convenio_w,
	cd_categoria_w,
	cd_medico_w,
	cd_plano_w,
	cd_tipo_acomodacao_w,
	nr_seq_classificacao_w,
	dt_orcamento_w,
	ie_clinica_w,
	ie_carater_atend_w,
	cd_procedencia_w,
	cd_pessoa_fisica_w,
	cd_setor_atendimento_w,
	ie_tipo_atendimento_w,
	cd_tipo_anestesia_w
from 	estabelecimento b,
	convenio c,
	orcamento_paciente a
where	a.cd_estabelecimento		= b.cd_estabelecimento
and	c.cd_convenio			= a.cd_convenio
and	a.nr_sequencia_orcamento	= nr_seq_orcamento_p;
exception
	when others then
		cd_estabelecimento_w := 9999;
end;

dia_semana_w 	:= pkg_date_utils.get_WeekDay(nvl(dt_orcamento_w, sysdate));

/* obter feriado */
begin
select 	'S'
into 	ie_dia_feriado_w
from 	feriado
where 	cd_estabelecimento 			= cd_estabelecimento_w
and 	to_char(dt_feriado,'dd/mm/yyyy')  	= to_char(nvl(dt_orcamento_w, sysdate),'dd/mm/yyyy');
exception
            when others then
		ie_dia_feriado_w	:= 'N';
end;

if	(nvl(nr_atendimento_p,0) > 0) then
	select 	max(a.cd_motivo_alta),
		max(a.nr_seq_forma_laudo)
	into	cd_motivo_alta_w,
		nr_seq_forma_laudo_w
	from 	atendimento_paciente 	  a
	where 	a.nr_atendimento   = nr_atendimento_p;
end if;


begin
select	count(1)
into	qt_reg_ibge_w
from	regra_lanc_automatico
where	nr_seq_evento = nr_seq_evento_p
and	cd_municipio_ibge is not null
and	rownum = 1;
exception
when others then
	qt_reg_ibge_w := 0;
end;

if	(qt_reg_ibge_w > 0) then
	begin

	begin
	select	nvl(max(c.cd_municipio_ibge),'X')
	into	cd_municipio_ibge_w
	from	pessoa_fisica b,
		compl_pessoa_fisica c
	where	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	b.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	c.ie_tipo_complemento	= 1;
	exception
	when others then
		cd_municipio_ibge_w := 'X';
	end;

	end;
end if;


/*	Entrada do paciente no setor */
qt_reg_w		:= 0;

dt_execucao_w	:= sysdate;

ie_verifica_se_pacote_w := nvl(Obter_Valor_Param_Usuario(106, 79, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');
ie_consiste_duplicidade_w 	:= nvl(obter_valor_param_usuario(106, 85, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');
ie_dispara_kit_w		:= nvl(Obter_Valor_Param_Usuario(106,  9, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');

/*select	nvl(max(Obter_Valor_Param_Usuario(106, 9, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)), 'N')
into	ie_dispara_kit_w
from	dual;*/

/* Ricardo 13/11/2006

select	max(cd_edicao_amb)
into	cd_edicao_amb_w
from	convenio_amb
where	(cd_estabelecimento	= cd_estabelecimento_w)
  and	(cd_convenio		= cd_convenio_w)
  and	(cd_categoria		= cd_categoria_w)
  and	(nvl(ie_situacao,'A')	= 'A')
  and	(dt_inicio_vigencia	=
	(select max(dt_inicio_vigencia)
	from	convenio_amb a
	where	(a.cd_estabelecimento	= cd_estabelecimento_w)
	and	(a.cd_convenio		= cd_convenio_w)
	and	(a.cd_categoria		= cd_categoria_w)
	and	(nvl(a.ie_situacao,'A')	= 'A')
	and	(a.dt_inicio_vigencia	<= dt_execucao_w)));
*/

begin


cd_funcao_w	:=	nvl(Obter_Funcao_Ativa,0);

select	obter_edicao (cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, dt_execucao_w, null)
into	cd_edicao_amb_w
from	dual;
exception
	when others then
		select	max(cd_edicao_amb)
		into	cd_edicao_amb_w
		from	convenio_amb
		where	cd_estabelecimento	= cd_estabelecimento_w
		  and	cd_convenio		= cd_convenio_w
		  and	cd_categoria		= cd_categoria_w
		  and	(nvl(ie_situacao,'A')	= 'A')
		  and	dt_inicio_vigencia	=
			(select	max(a.dt_inicio_vigencia)
			from	convenio_amb a
			where	a.cd_estabelecimento  = cd_estabelecimento_w
			and	a.cd_convenio         = cd_convenio_w
			and	a.cd_categoria        = cd_categoria_w
			and	(nvl(a.ie_situacao,'A')= 'A')
			and	a.dt_inicio_vigencia <=  dt_execucao_w);
end;

if (NR_SEQ_EVENTO_P in (612, 192)) then /*Apos gerar Orcamento*/

	select 	obter_se_medico_credenciado(	cd_estabelecimento_w,
						cd_medico_w,
						cd_convenio_w,
						null,
						null,
						cd_categoria_w,
						cd_setor_atendimento_w,
						cd_plano_w,
						null,
						ie_tipo_atendimento_w,
						null,
						ie_carater_atend_w )
	into	ie_credenciado_w
	from 	dual;


	for RLA in C01 loop
		nr_sequencia_w 	 := RLA.nr_sequencia;
		cd_setor_regra_w := RLA.cd_setor_atendimento;
		nr_seq_regra_w 	 := RLA.nr_sequencia;
	end loop;
else
	cd_setor_orcamento_w 	:= cd_setor_atendimento_w;

	for PROC_ORCAMENTO in c00 (nr_seq_orcamento_p , nr_seq_item_p) loop
		cd_grupo_proc_w 	:= PROC_ORCAMENTO.cd_grupo_proc;
		cd_especial_proc_w      := PROC_ORCAMENTO.cd_especialidade;
		cd_area_proc_w		:= PROC_ORCAMENTO.cd_area_procedimento;
		cd_proc_propaci_w	:= PROC_ORCAMENTO.cd_procedimento;
		ie_origem_proc_w	:= PROC_ORCAMENTO.ie_origem_proced;
		nr_seq_exame_w		:= PROC_ORCAMENTO.nr_seq_exame;
		nr_seq_proc_interno_w	:= PROC_ORCAMENTO.nr_seq_proc_interno;
		nr_seq_item_w		:= PROC_ORCAMENTO.nr_sequencia;
		ie_lado_w		:= PROC_ORCAMENTO.ie_lado;
		cd_setor_atendimento_w	:= PROC_ORCAMENTO.cd_setor_atendimento;


		if (PROC_ORCAMENTO.cd_medico is not null) then
			cd_medico_filtro_w    := PROC_ORCAMENTO.cd_medico ;
		else
			cd_medico_filtro_w    := cd_medico_w ;
		end if;

		select 	obter_se_medico_credenciado(	cd_estabelecimento_w,
							cd_medico_filtro_w,
							cd_convenio_w,
							null,
							PROC_ORCAMENTO.cd_especialidade,
							cd_categoria_w,
							PROC_ORCAMENTO.cd_setor_atendimento,
							cd_plano_w,
							null,
							ie_tipo_atendimento_w,
							null,
							ie_carater_atend_w )
		into	ie_credenciado_w
		from 	dual;

		for RLA in C01 loop
			nr_sequencia_w 	 := RLA.nr_sequencia;
			cd_setor_regra_w := RLA.cd_setor_atendimento;
			nr_seq_regra_w 	 := RLA.nr_sequencia;
		end loop;

	end loop;

	cd_setor_atendimento_w := cd_setor_orcamento_w;
end if;

dbms_output.put_line(nr_seq_regra_w);

OPEN C02;
LOOP
FETCH C02 into
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_exame_w,
		cd_material_w,
		cd_unid_medida_w,
		qt_lancamento_w,
		ie_Medico_atendimento_w,
		ie_Local_Estoque_w,
		ie_funcao_medico_w,
		tx_procedimento_w,
		nr_seq_proc_int_w,
		ie_adic_orcamento_w,
		cd_setor_orcamento_w;
	exit when c02%notfound;
	BEGIN
	ie_regra_w	:= null;

	/* Inicializa medico quando ie_medico_atendimento = 'S' */

	/* Primeiro = medico executor, Segundo = medico do atendimento */
	cd_medico_executor_w		:= '';

	/* Verifica se procedimento de alta ja foi executado    Bola 08072002 */
	BEGIN
	qt_proc_alta_w	:= 0;
	if	(nr_seq_evento_p = 36) then
		begin
		select 	count(*)
		into	qt_proc_alta_w
		from	procedimento_paciente
		where	nr_atendimento	= nr_atendimento_p
		and	cd_procedimento	= cd_procedimento_w;
		exception
			when others then
			qt_proc_alta_w	:= 0;
		end;
	end if;
	END;

	/*tratar para ir para o final caso o proc interno da a? da regra esteja inativo*/
	if(nr_seq_proc_int_w is not null) then
	select	nvl(max(ie_situacao),'A')
			into	ie_situacao_w
			from	proc_interno
			where	nr_sequencia = nr_seq_proc_int_w;

			if	(ie_situacao_w = 'I') then
				goto continue_loop;
			end if;
	end if;

	if	(nr_seq_proc_int_w is not null) then
		Obter_Proc_Tab_Interno_Conv (nr_seq_proc_int_w, cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, cd_plano_w, null, cd_procedimento_w,
		ie_origem_proced_w, null, sysdate, cd_tipo_acomodacao_w, null, null, null, null, null, null, null);
	end if;

	if	(nvl(nr_seq_exame_w,0) > 0) then
		Obter_Exame_Lab_Convenio(nr_seq_exame_w, cd_convenio_w, cd_categoria_w, ie_tipo_atendimento_w, cd_estabelecimento_w, ie_tipo_convenio_w,
				null, null, cd_plano_w, ds_ret_w, cd_procedimento_w, ie_origem_proced_w, ds_erro_ww, nr_seq_proc_interno_aux_w, null);
	end if;

	/*Ap?bter o c?o do procedimento atrav?do proc interno ou exame de laborat? testar novamente para ver se o c?o est?tivo.*/
	if(cd_procedimento_w is not null) then
		select 	nvl(max(ie_situacao),'A')
		into	ie_situacao_w
		from	procedimento
		where	cd_procedimento	 = cd_procedimento_w
		and	ie_origem_proced = ie_origem_proced_w;

		if	(ie_situacao_w = 'I') then
			goto continue_loop;
		end if;
	end if;

	if	(cd_procedimento_w is not null) and
		(qt_proc_alta_w	= 0) 		then
		begin

		if	(ie_verifica_se_pacote_w = 'S') and
			(obter_se_pacote_convenio(	cd_procedimento_w,
							ie_origem_proced_w,
							cd_convenio_w,
							cd_estabelecimento_w) = 'S') then
			select 	max(nr_seq_pacote)
			into	nr_seq_pacote_w
			from 	pacote
			where	cd_proced_pacote = cd_procedimento_w
			and 	ie_origem_proced = ie_origem_proced_w
			and 	cd_convenio	 = cd_convenio_w
			and 	cd_estabelecimento = cd_estabelecimento_w
			and 	ie_situacao = 'A';

			gerar_orc_pacote_lanc_auto( nr_seq_orcamento_p,
						nr_seq_pacote_w,
						0,
						nm_usuario_p,
						ie_adic_orcamento_w,
						nr_seq_item_p);


		else

			consiste_proc_orc( nr_seq_orcamento_p,
					   cd_procedimento_w,
					   ie_origem_proced_w,
					   qt_lancamento_w,
					   cd_estabelecimento_w,
					   nm_usuario_p,
					   ie_autorizacao_w,
					   null,
					   ie_regra_w);


			select	ORCAMENTO_PACIENTE_PROC_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into ORCAMENTO_PACIENTE_PROC (
				NR_SEQUENCIA_ORCAMENTO         	,
				CD_PROCEDIMENTO                	,
				IE_ORIGEM_PROCED               	,
				QT_PROCEDIMENTO                	,
				DT_ATUALIZACAO                 	,
				NM_USUARIO                     	,
				VL_PROCEDIMENTO                	,
				VL_MEDICO                      	,
				VL_ANESTESISTA                 	,
				VL_FILME                       	,
				VL_AUXILIARES                  	,
				VL_CUSTO_OPERACIONAL           	,
				VL_DESCONTO                    	,
				CD_MEDICO                      	,
				IE_PROCEDIMENTO_PRINCIPAL      	,
				QT_DIA_INTERNACAO	       	,
				IE_VALOR_INFORMADO             	,
				NR_SEQ_EXAME                   	,
				NR_SEQUENCIA		       	,
				nr_seq_regra_lanc              	,
				nr_seq_proc_princ		,
				nr_seq_proc_interno		,
				ie_adicional,
				ie_autorizacao,
				cd_setor_atendimento,
				ie_regra_plano)
			values (
				nr_seq_orcamento_p	,
				cd_procedimento_w       ,
				ie_origem_proced_w,
				qt_lancamento_w,
				sysdate                 ,
				nm_usuario_p            ,
				0, 0, 0, 0, 0, 0, 0,
				cd_medico_executor_w    ,
				'N',
				null,
				'N',
				nr_seq_exame_w,
				nr_sequencia_w,
				nr_seq_regra_w,
				nvl(nr_seq_item_p, nr_seq_item_w),
				nr_seq_proc_int_w,
				nvl(ie_adic_orcamento_w,'N'),
				ie_autorizacao_w,
				nvl(cd_setor_atendimento_w, cd_setor_orcamento_w),
				ie_regra_w);

		end if;

		if	(1 = 2) then
			Atualiza_preco_procedimento(
				nr_sequencia_w, cd_convenio_w, nm_usuario_p);
		end if;
		end;
	else
		begin
		/* Verifica se procedimento de alta ja foi executado    Bola 08072002 */
		qt_mat_alta_w	:= 0;

		if	(qt_mat_alta_w	= 0) 		then
			begin


			qt_material_w	:= 0;

			if	(ie_consiste_duplicidade_w = 'S') then

				select	count(*)
				into	qt_material_w
				from	orcamento_paciente_mat
				where	nr_sequencia_orcamento	= nr_seq_orcamento_p
				and	cd_material		= cd_material_w;

			end if;

			consiste_mat_orc(	nr_seq_orcamento_p,
						cd_material_w,
						qt_lancamento_w,
						cd_estabelecimento_w,
						nm_usuario_p,
						ie_autorizacao_w,
						ie_regra_w);

			if	(qt_material_w = 0) then

				insert into orcamento_paciente_mat (
					NR_SEQuencia_ORCAMENTO     	,
					CD_MATERIAL             ,
					QT_MATERIAL             ,
					VL_MATERIAL		,
					DT_ATUALIZACAO		,
					NM_USUARIO		,
					IE_VALOR_INFORMADO,
					nr_sequencia,
					nr_seq_regra_lanc,
					nr_seq_proc_princ,
					ie_adicional,
					ie_autorizacao,
					cd_setor_atendimento,
					ie_regra_plano)
				values (nr_seq_orcamento_p	,
					cd_material_w           ,
					qt_lancamento_w		,
					0,
					sysdate                 ,
					nm_usuario_p            ,
					'N',
					orcamento_paciente_mat_seq.NextVal,
					nr_seq_regra_w,
					nvl(nr_seq_item_p, nr_seq_item_w),
					nvl(ie_adic_orcamento_w,'N'),
					ie_autorizacao_w,
					nvl(cd_setor_atendimento_w, cd_setor_orcamento_w),
					ie_regra_w);

				if	(ie_dispara_kit_w	= 'S') then

					SELECT	nvl(max(cd_kit_material),0)
					into	cd_kit_material_w
					FROM 	MATERIAL_ESTAB
					WHERE 	CD_MATERIAL		= cd_material_w
					AND 	CD_ESTABELECIMENTO	= cd_estabelecimento_w;

					if	(cd_kit_material_w	<> 0) then

						gerar_kit_material_orc
							(nr_seq_orcamento_p, qt_lancamento_w, cd_kit_material_w, nvl(nr_seq_item_p, nr_seq_item_w) ,nm_usuario_p,nvl(ie_adic_orcamento_w,'N'), null, nvl(cd_setor_atendimento_w, cd_setor_orcamento_w));

					end if;
				end if;

			end if;

			end;
		end if;
		end;
	end if;
	END;
	<<continue_loop>>
	ie_situacao_w := null;
END LOOP;
CLOSE C02;
END GERAR_LANC_ORC_AUTOMATICO;
/
