create or replace
procedure bsc_validar_estornar_acao(	nr_sequencia_p	number,
				ie_operacao_p	varchar2,
				nm_usuario_p	varchar2) is

/* IE_OPERACAO_P
V - Validar - preenche a data
E - Estornar - limpa o DT_VALIDACAO_PE
*/
begin

update	man_ordem_servico
set	dt_validacao_pe	= decode(ie_operacao_p,'V',sysdate,'E',null)
where	nr_sequencia	= nr_sequencia_p;

commit;

end bsc_validar_estornar_acao;
/