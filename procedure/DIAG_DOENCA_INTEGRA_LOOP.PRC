create or replace
procedure diag_doenca_integra_loop(cd_tipo_evolucao_p varchar2,
									cd_evolucao_p	number) is 

dt_chamada_procedure_w	date;
qt_segundos_w number(10);
ie_encontrou_w char(1) := 'N';

begin

select	qt_segundos_int
into	qt_segundos_w
from	tipo_evolucao
where 	cd_tipo_evolucao = cd_tipo_evolucao_p
and 	nvl(ie_integrar_diagnostico,'N') = 'S';

dt_chamada_procedure_w := sysdate + qt_segundos_w/86400;

while ((dt_chamada_procedure_w > sysdate) and (ie_encontrou_w = 'N'))
loop

	select nvl(max('S'),'N') 
	into ie_encontrou_w
	from PEP_ITEM_PENDENTE 
	where ie_tipo_pendencia = 'R'
	and cd_evolucao = cd_evolucao_p;

end loop;

end diag_doenca_integra_loop;
/
