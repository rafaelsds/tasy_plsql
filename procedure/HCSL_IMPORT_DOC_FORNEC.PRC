create or replace
procedure hcsl_import_doc_fornec is 

cod__docum_w         	   number;
dias_valid_w               number;
data_revis_w              varchar2(10);
dias_aviso_w              number;
comunicar_w              varchar2(1);
necess_rio_w	   	varchar2(1);
cd_cgc_w		varchar2(14);
nr_sequencia_w		number(10);
qt_registro_w		number(10);

Cursor C00 is

select	cd_cgc
into	cd_cgc_w
from	pessoa_juridica
where	ie_situacao = 'A' 
and 	cd_tipo_pessoa = 20;
 
 
Cursor C01 is 
select  cod__docum,
	dias_valid,
	data_revis,
	dias_aviso,
	comunicar_,
	necess_rio
from	hcsl_doc_fornec	;


begin

open C00;
loop
fetch C00 into
	cd_cgc_w;
	exit when C00%notfound;
		open C01;
		loop
		fetch C01 into	
			cod__docum_w,
			dias_valid_w,
			data_revis_w,
			dias_aviso_w,
			comunicar_w,
			necess_rio_w;
		exit when C01%notfound;
		
			select	pessoa_juridica_doc_seq.nextval 
			into  	nr_sequencia_w
			from 	dual;
			
			select	count(*)
			into	qt_registro_w
			from 	pessoa_juridica_doc
			where	cd_cgc = cd_cgc_w
			and 	nr_seq_tipo_docto = cod__docum_w;
			
			if	(qt_registro_w = 0) then
							
				begin
				insert into pessoa_juridica_doc(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_cgc,
					dt_revisao,
					nr_seq_tipo_docto,
					cd_pessoa_responsavel, 
					qt_tempo_validade,
					ie_comunic_resp_documento,
					ie_comunic_comprador,
					qt_dias_aviso_vencto,
					cd_setor_responsavel,
					ie_consiste_cotacao,
					ie_comunic_todos_comprador)
				values	(nr_sequencia_w,
					sysdate,
					'Importacao',
					sysdate,
					'Importacao',
					cd_cgc_w,
					to_date('30/11/2009', 'dd/mm/yyyy'),
					cod__docum_w,
					null,
					dias_valid_w,
					'N',
					'N',
					dias_aviso_w,
					'',
					necess_rio_w,
					comunicar_w);
			
				end;
			end if;
		end loop;
		close C01;		
end loop;
close C00;		
		

commit;

end hcsl_import_doc_fornec;
/