create or replace
procedure proc_lib_prescr_result_exame(	nr_atendimento_p number,
					cd_grupo_procedimento_p number,
					cd_especialidade_proced_p number,
					cd_area_procedimento_p number,
					cd_procedimento_p number,
					nr_seq_proc_interno_p number,
					ie_libera_resultado_p	out varchar2,
					ds_mensagem_p out varchar2) is

qt_resultado_w				exame_lab_result_item.qt_resultado%type;
ds_resultado_w				exame_lab_result_item.ds_resultado%type;
pr_resultado_w				exame_lab_result_item.pr_resultado%type;
nr_seq_exame_w				exame_lab_result_item.nr_seq_exame%type;
ie_resultado_w				varchar2(1) := 'S';
ie_retorno_w				varchar2(1) := 'S';
ds_mensagem_w				varchar2(2000) := '';

cursor c01 is
	select	distinct b.qt_resultado,
		b.ds_resultado,
		b.pr_resultado,
		b.nr_seq_exame
	from	exame_lab_resultado a,
		exame_lab_result_item b
	where	a.nr_seq_resultado = b.nr_seq_resultado
		and a.nr_atendimento = nr_atendimento_p
		and a.dt_resultado in (	
		select	max(c.dt_resultado)
		from exame_lab_resultado c,
			exame_lab_result_item d
		where	c.nr_seq_resultado = d.nr_seq_resultado
			and c.nr_atendimento = a.nr_atendimento
			and d.nr_seq_exame = b.nr_seq_exame
			and c.dt_resultado > sysdate -7);
begin 

	open c01;
		loop
		fetch c01 into
      			qt_resultado_w,
			ds_resultado_w,
			pr_resultado_w,
			nr_seq_exame_w;
		exit when c01%notfound;

		begin
    
			if (qt_resultado_w is not null) then
				select	nvl(decode(max(nvl(b.ie_permite_justificar,'N')),'S','J',max(nvl(b.ie_permite_justificar,'N'))),'S'),
					max(b.ds_mensagem)
				into	ie_resultado_w,
					ds_mensagem_w
				from	proc_exame_lab_result a,
					reg_proc_exame_result b
				where	a.nr_sequencia = b.nr_seq_proc_result 
					and b.nr_seq_exame = nr_seq_exame_w
					and b.ie_formato_resultado = 'V'
					and nvl(a.cd_grupo_proc,cd_grupo_procedimento_p) = cd_grupo_procedimento_p
					and nvl(a.cd_especialidade,cd_especialidade_proced_p) = cd_especialidade_proced_p
					and nvl(a.cd_area_procedimento,cd_area_procedimento_p) = cd_area_procedimento_p
					and nvl(a.cd_procedimento,cd_procedimento_p) = cd_procedimento_p
					and nvl(a.nr_seq_proc_interno,nr_seq_proc_interno_p) = nr_seq_proc_interno_p
					and qt_resultado_w not between nvl(qt_minima,-9999) and nvl(qt_maxima,9999);

			elsif (pr_resultado_w is not null) then
				select	nvl(decode(max(nvl(b.ie_permite_justificar,'N')),'S','J',
					max(nvl(b.ie_permite_justificar,'N'))),'S'),
					max(b.ds_mensagem)
				into	ie_resultado_w,
					ds_mensagem_w
				from	proc_exame_lab_result a,
					reg_proc_exame_result b
				where	a.nr_sequencia = b.nr_seq_proc_result 
					and b.nr_seq_exame = nr_seq_exame_w
					and b.ie_formato_resultado = 'P'
					and nvl(a.cd_grupo_proc,cd_grupo_procedimento_p) = cd_grupo_procedimento_p
					and nvl(a.cd_especialidade,cd_especialidade_proced_p) = cd_especialidade_proced_p
					and nvl(a.cd_area_procedimento,cd_area_procedimento_p) = cd_area_procedimento_p
					and nvl(a.cd_procedimento,cd_procedimento_p) = cd_procedimento_p
					and nvl(a.nr_seq_proc_interno,nr_seq_proc_interno_p) = nr_seq_proc_interno_p
					and pr_resultado_w not between nvl(qt_percent_min,-9999) and nvl(qt_percent_max,9999);
					
			elsif (ds_resultado_w is not null) then
				select	nvl(decode(max(nvl(b.ie_permite_justificar,'N')),'S','J',
					max(nvl(b.ie_permite_justificar,'N'))),'S'),
					max(b.ds_mensagem)
				into 	ie_resultado_w,
					ds_mensagem_w
				from 	proc_exame_lab_result a,
					reg_proc_exame_result b
				where 	a.nr_sequencia = b.nr_seq_proc_result 
					and nr_seq_exame = nr_seq_exame_w
					and b.ie_formato_resultado = 'D'
					and nvl(a.cd_grupo_proc,cd_grupo_procedimento_p) = cd_grupo_procedimento_p
					and nvl(a.cd_especialidade,cd_especialidade_proced_p) = cd_especialidade_proced_p
					and nvl(a.cd_area_procedimento,cd_area_procedimento_p) = cd_area_procedimento_p
					and nvl(a.cd_procedimento,cd_procedimento_p) = cd_procedimento_p
					and nvl(a.nr_seq_proc_interno,nr_seq_proc_interno_p) = nr_seq_proc_interno_p
					and upper(trim(ds_resultado)) != upper(trim(ds_resultado_w));
			end if;
			
			if (ie_resultado_w = 'J') then
				ie_retorno_w := ie_resultado_w;
				ds_mensagem_p := ds_mensagem_w;
			elsif (ie_resultado_w = 'N') then
				ie_retorno_w := ie_resultado_w;
				ds_mensagem_p := ds_mensagem_w;
				exit;
			end if;

		end;
		end loop;
		close c01;

	ie_libera_resultado_p := nvl(ie_retorno_w,'S');

end proc_lib_prescr_result_exame;
/