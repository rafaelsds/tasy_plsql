create or replace
procedure	atualizar_list_unid_item(	nr_sequencia_p	number,
						ie_resultado_p	varchar2,
						ie_nao_aplica_p	varchar2) is
						
begin

if	(nvl(nr_sequencia_p,0) > 0) then
	
	update	sl_check_list_unid_item
	set	ie_resultado	= ie_resultado_p,
		ie_nao_aplica	= ie_nao_aplica_p
	where	nr_sequencia	= nr_sequencia_p;
	
end if;

commit;

end atualizar_list_unid_item;
/
