CREATE OR REPLACE procedure gravar_log_tasy_prag(
			cd_log_p number,
			ds_log_p varchar2,
			nm_usuario_p VARCHAR2 DEFAULT null) is
dt_fim_geracao_w	Date;
PRAGMA AUTONOMOUS_TRANSACTION;

begin
begin
	select	nvl(DT_FIM_GERACAO, sysdate)
	into	dt_fim_geracao_w
	from	tipo_log_tasy
	where	cd_log = cd_log_p;
exception
	When NO_DATA_FOUND then
		dt_fim_geracao_w := sysdate;
end;

if	Trunc(dt_fim_geracao_w) >= Trunc(sysdate) then
	insert into log_tasy (  dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
		values 	     ( 	sysdate,
				Nvl(nm_usuario_p,Nvl(wheb_usuario_pck.get_nm_usuario,'Tasy')),
				cd_log_p,
				substr(ds_log_p,1,2000));
        COMMIT;
end if;

end  gravar_log_tasy_prag;
/