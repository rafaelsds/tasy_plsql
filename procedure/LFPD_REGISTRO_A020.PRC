create or replace
procedure lfpd_registro_A020(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				ds_separador_p		varchar2,
				qt_linha_p		in out number,
				nr_sequencia_p		in out number) is

contador_w		number(10)	:= 0;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10)	:= qt_linha_p;
nr_seq_registro_w	number(10)	:= nr_sequencia_p;
sep_w			varchar2(1)	:= ds_separador_p;

cd_cgc_w		varchar2(14);
cd_municipio_w		varchar2(10);

nr_seq_regra_w		fis_lfpd_controle.nr_seq_regra_lfpd%type;
ie_gerar_w		fis_regra_lfpd_reg.ie_gerar%type;

cursor c01 is
	select	'A020'																			cd_registro,
		substr(decode(Obter_se_nota_entrada_saida(n.nr_sequencia),'E',0,'S',1),1,1) 											ie_operacao,
		substr(decode(Obter_se_nota_entrada_saida(n.nr_sequencia),'E',1,'S',0),1,1) 											ie_emissao_nf,
		nvl(n.cd_pessoa_fisica, n.cd_cgc)																cd_participante,
		'01'																				cd_modelo_doc, -- nota fiscal
		substr(decode(n.ie_situacao, 1, '00', 2, '02', 3, '02', 9, '02'),1,2) 												cd_situacao,
		n.cd_serie_nf 																	cd_serie_nf,
		n.cd_serie_rps 																	cd_subserie_nf,
		nvl(n.nr_nfe_imp, n.nr_nota_fiscal) 														nr_nota_fiscal,
		substr(to_char(n.dt_emissao, 'ddmmyyyy'),1,8) 															dt_emissao,
		substr(ELIMINA_CARACTERE_ESPECIAL(a.cd_cfps), 1, 4)																			cd_cfps,
		obter_dados_pf_pj('', cd_cgc_w, 'UF')																sg_uf,
		obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')														cd_municipio_ibge,
		substr(ELIMINA_CARACTERE_ESPECIAL(a.cd_cfop), 1, 4) 																			cd_operacao_nf,
		replace(campo_mascara(n.vl_mercadoria,2),'.',',')														vl_total_nota,
		substr(decode(nvl(n.cd_condicao_pagamento,0), 0, 0, decode(obter_dados_nota_fiscal(n.nr_sequencia, '28'), 0, 0, 1)), 1, 1)  					ie_pagto,
		0																				vl_subcontratado,
		replace(campo_mascara(n.vl_descontos,2),'.',',')														vl_descontos,
		replace(campo_mascara(n.vl_mercadoria,2),'.',',')														vl_servicos,
		replace(campo_mascara(0,2),'.',',')																vl_material_prop,
		0																				vl_material_terc,
		replace(campo_mascara(0,2),'.',',')																vl_desp_acessorias,
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS'),2),'.',',')									vl_base_calculo,
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS'),2),'.',',')									vl_tributo,
		replace(Campo_Mascara(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'B', 'ISS'),2),'.',',')		 							vl_base_calculo_ret,
		replace(Campo_Mascara(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS'),2),'.',',')		 							vl_retencao,
		decode(n.ds_observacao,'', '',n.nr_sequencia)															ds_observacao,
		n.nr_sequencia																			nr_seq_nota,
		obter_dados_pf_pj('', cd_cgc_w, 'UF'),
		obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')
	from	nota_fiscal n,				
		operacao_nota o,
		natureza_operacao a,
		modelo_nota_fiscal m
	where	n.cd_operacao_nf = o.cd_operacao_nf	
	and 	n.nr_seq_modelo = m.nr_sequencia(+)
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'	
	and	to_char(n.dt_emissao,'dd/MM/yy') between to_date(dt_inicio_p,'dd/MM/yy') and to_date(dt_fim_p,'dd/MM/yy')
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and 	o.ie_servico = 'S'
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and 	(m.cd_modelo_nf <> '3A' or m.cd_modelo_nf is null)
	and		exists (select 1
					from	nota_fiscal_item x
					where	x.nr_sequencia = n.nr_sequencia
					and		a.cd_natureza_operacao = x.cd_natureza_operacao	
					and		(x.cd_material is not null or x.cd_procedimento is not null)	
					and	rownum = 1)
	union
	select	'A020'																			cd_registro,
		substr(decode(Obter_se_nota_entrada_saida(n.nr_sequencia),'E',0,'S',1),1,1) 											ie_operacao,
		substr(decode(Obter_se_nota_entrada_saida(n.nr_sequencia),'E',1,'S',0),1,1) 											ie_emissao_nf,
		nvl(n.cd_pessoa_fisica, n.cd_cgc)																cd_participante,
		'01'																				cd_modelo_doc, -- nota fiscal
		substr(decode(n.ie_situacao, 1, '00', 2, '02', 3, '02', 9, '02'),1,2) 												cd_situacao,
		n.cd_serie_nf 																	cd_serie_nf,
		n.cd_serie_rps 																	cd_subserie_nf,
		nvl(n.nr_nfe_imp, n.nr_nota_fiscal) 														nr_nota_fiscal,
		substr(to_char(n.dt_emissao, 'ddmmyyyy'),1,8) 															dt_emissao,
		substr(ELIMINA_CARACTERE_ESPECIAL(a.cd_cfps), 1, 4)																			cd_cfps,
		obter_dados_pf_pj('', cd_cgc_w, 'UF')																sg_uf,
		obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')														cd_municipio_ibge,
		substr(ELIMINA_CARACTERE_ESPECIAL(a.cd_cfop), 1, 4) 																			cd_operacao_nf,
		replace(campo_mascara(n.vl_mercadoria,2),'.',',')														vl_total_nota,
		substr(decode(nvl(n.cd_condicao_pagamento,0), 0, 0, decode(obter_dados_nota_fiscal(n.nr_sequencia, '28'), 0, 0, 1)), 1, 1)  					ie_pagto,
		0																				vl_subcontratado,
		replace(campo_mascara(n.vl_descontos,2),'.',',')														vl_descontos,
		replace(campo_mascara(n.vl_mercadoria,2),'.',',')														vl_servicos,
		replace(campo_mascara(0,2),'.',',')																vl_material_prop,
		0																				vl_material_terc,
		replace(campo_mascara(0,2),'.',',')																vl_desp_acessorias,
		'0'																				vl_base_calculo,
		'0' 																				vl_tributo,
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS'),2),'.',',')		 							vl_base_calculo_ret,
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS'),2),'.',',')		 							vl_retencao,
		decode(n.ds_observacao,'', '',n.nr_sequencia)																			ds_observacao,
		n.nr_sequencia																			nr_seq_nota,
		obter_dados_pf_pj('', cd_cgc_w, 'UF'),
		obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')
	from	nota_fiscal n,
		operacao_nota o,
		natureza_operacao a,
		modelo_nota_fiscal m
	where	n.cd_operacao_nf = o.cd_operacao_nf
	and 	n.nr_seq_modelo = m.nr_sequencia(+)
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E'
	and	to_char(n.dt_emissao,'dd/MM/yy') between to_date(dt_inicio_p,'dd/MM/yy') and to_date(dt_fim_p,'dd/MM/yy')
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and 	o.ie_servico = 'S'
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and 	(m.cd_modelo_nf <> '3A' or m.cd_modelo_nf is null)
	and		exists (select 1
			from	nota_fiscal_item x
			where	x.nr_sequencia = n.nr_sequencia
			and		a.cd_natureza_operacao = x.cd_natureza_operacao	
			and		(x.cd_material is not null or x.cd_procedimento is not null)
			and	rownum = 1);

vet01	c01%RowType;

begin

select	cd_cgc
into	cd_cgc_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin


	contador_w := contador_w + 1;

	cd_municipio_w := vet01.cd_municipio_ibge;
	
	if (	vet01.cd_cfps <> '9104')then
		cd_municipio_w := '5300108';
	else
		cd_municipio_w := '';
	end if;	

	/*if(vet01.sg_uf = 'DF') then
		cd_municipio_w := '5300108';
	end if;*/

	ds_linha_w := substr(	sep_w	|| 	vet01.cd_registro		|| sep_w ||
						vet01.ie_operacao		|| sep_w ||
						vet01.ie_emissao_nf		|| sep_w ||
						vet01.cd_participante		|| sep_w ||
						vet01.cd_modelo_doc		|| sep_w ||
						vet01.cd_situacao		|| sep_w ||
						vet01.cd_serie_nf		|| sep_w ||
						vet01.cd_subserie_nf		|| sep_w ||
						vet01.nr_nota_fiscal		|| sep_w ||
						vet01.dt_emissao		|| sep_w ||
						vet01.cd_cfps			|| sep_w ||
						cd_municipio_w			|| sep_w ||
						vet01.cd_operacao_nf		|| sep_w ||
						vet01.vl_total_nota		|| sep_w ||
						vet01.ie_pagto			|| sep_w ||
						vet01.vl_subcontratado		|| sep_w ||
						vet01.vl_descontos		|| sep_w ||
						vet01.vl_servicos		|| sep_w ||
						vet01.vl_material_prop		|| sep_w ||
						vet01.vl_material_terc		|| sep_w ||
						vet01.vl_desp_acessorias	|| sep_w ||
						vet01.vl_base_calculo		|| sep_w ||
						vet01.vl_tributo		|| sep_w ||
						vet01.vl_base_calculo_ret	|| sep_w ||
						vet01.vl_retencao		|| sep_w ||
						vet01.ds_observacao		|| sep_w, 1, 8000);

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_lfpd_arquivo	(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_linha,
						ds_arquivo,
						ds_arquivo_compl,
						cd_registro,
						nr_seq_controle_lfpd)
				values	(	fis_lfpd_arquivo_seq.NextVal,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_linha_w,
						ds_arquivo_w,
						ds_arquivo_compl_w,
						vet01.cd_registro,
						nr_seq_controle_p);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;
	
	select	nvl(max(c.nr_seq_regra_lfpd),0)
	into	nr_seq_regra_w
	from	fis_lfpd_regra r,
		fis_lfpd_controle c
	where	r.nr_sequencia		= c.nr_seq_regra_lfpd
	and	r.cd_estabelecimento	= cd_estabelecimento_p
	and	c.nr_sequencia		= nr_seq_controle_p;
	
	ie_gerar_w := 'S';
	
	if nr_seq_regra_w <> 0 then
		select	nvl(max(ie_gerar), 'S')
		into	ie_gerar_w
		from	fis_regra_lfpd_reg
		where	nr_seq_regra_lfpd	= nr_seq_regra_w
		and 	cd_registro = 'A025'
		order by 	nr_sequencia;	
	end if;
	
	if ie_gerar_w = 'S' then
		lfpd_registro_A025(nr_seq_controle_p, vet01.nr_seq_nota, nm_usuario_p, cd_estabelecimento_p, dt_inicio_p, dt_fim_p, ds_separador_p, nr_linha_w, nr_seq_registro_w);
	end if;
	lfpd_registro_A200(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_p, vet01.nr_seq_nota, dt_inicio_p, dt_fim_p, ds_separador_p, nr_linha_w, nr_seq_registro_w);
	
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_registro_A020;
/
