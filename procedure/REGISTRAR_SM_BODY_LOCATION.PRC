create or replace PROCEDURE REGISTRAR_SM_BODY_LOCATION (nm_usuario_p            IN VARCHAR2,
                                    nr_seq_location_p       IN NUMBER,
                                    ds_body_model_p         IN VARCHAR2,
                                    ie_age_p                IN VARCHAR2,
                                    ie_gender_p             IN VARCHAR2,
                                    ie_weight_p             IN VARCHAR2,
                                    ds_side_p               IN VARCHAR2,
                                    id_area_p               IN VARCHAR2,
                                    ie_operacao_p           IN NUMBER) IS
                                    
existe_body_w VARCHAR2(1); 
nr_seq_body_location_w number;
nr_seq_body_area_w     number;
                                    
BEGIN

    IF ie_operacao_p = 1 THEN
    
        SELECT SM_BODY_LOCATION_SEQ.NEXTVAL 
          INTO nr_seq_body_location_w
          FROM dual;
    
        SELECT nvl(MAX(1),0), nvl(max(body_.nr_sequencia), nr_seq_body_location_w)
          INTO existe_body_w, nr_seq_body_location_w
          FROM sm_body_location body_
         WHERE body_.nr_seq_location = nr_seq_location_p
           AND body_.ie_gender = ie_gender_p
           AND body_.ds_side = ds_side_p;

        IF existe_body_w < 1 THEN
            INSERT INTO SM_BODY_LOCATION (
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_location,
                ds_body_model,
                ie_age,
                ie_gender,
                ie_weight,
                ds_side
            ) VALUES (
                nr_seq_body_location_w,
                TRUNC(SYSDATE),
                nm_usuario_p,
                NULL,
                NULL,
                nr_seq_location_p,
                ds_body_model_p,
                ie_age_p,
                ie_gender_p,
                ie_weight_p,
                ds_side_p);
            COMMIT;
        END IF;
        
        DELETE 
          FROM SM_BODY_AREA
         WHERE nr_seq_body_location = nr_seq_body_location_w;
         COMMIT;
        
        INSERT INTO SM_BODY_AREA (
            nr_sequencia,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            ds_area,
            nr_seq_body_location
        ) VALUES (
            SM_BODY_AREA_SEQ.NEXTVAL,
            TRUNC(SYSDATE),
            nm_usuario_p,
            NULL,
            NULL,
            id_area_p,
            nr_seq_body_location_w);
            COMMIT;
    end IF;
    IF ie_operacao_p = 2 THEN
    
    SELECT area.nr_Sequencia
      INTO nr_seq_body_area_w
      FROM sm_body_location body_, sm_body_area area
     WHERE body_.nr_sequencia = area.nr_Seq_body_location
       and body_.nr_seq_location = nr_seq_location_p 
       and area.ds_area = id_area_p;
    
        DELETE FROM SM_BODY_AREA
         WHERE SM_BODY_AREA.nr_sequencia = nr_Seq_body_area_w;
        COMMIT;
           
    END IF;    
    
END REGISTRAR_SM_BODY_LOCATION;
/
