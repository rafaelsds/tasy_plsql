/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Desfazer os dados do lote do envio de guias XML da mensalidade
----------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_desfazer_lote_guia_mens
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
			
nr_seq_guia_mens_arq_w		number(10);
nr_seq_mens_guia_envio_w	number(10);
			
Cursor C01 is
	select	nr_sequencia
	from	pls_mens_guia_arquivo
	where	nr_seq_lote	= nr_seq_lote_p;
	
Cursor C02 is
	select	nr_sequencia
	from	pls_mensalidade_guia_envio
	where	nr_seq_guia_arquivo = nr_seq_guia_mens_arq_w;

begin

open C01;
loop
fetch C01 into	
	nr_seq_guia_mens_arq_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_mens_guia_envio_w;
	exit when C02%notfound;
		begin
		
		delete	pls_mens_guia_envio_proc
		where	NR_SEQ_GUIA_ENVIO	= nr_seq_mens_guia_envio_w;
		
		delete	pls_mens_guia_envio_mat
		where	NR_SEQ_GUIA_ENVIO	= nr_seq_mens_guia_envio_w;
		
		update	pls_mensalidade_seg_item
		set	nr_seq_item_envio_guia	= null
		where	nr_seq_item_envio_guia	= nr_seq_mens_guia_envio_w;
		
		delete	pls_mensalidade_guia_envio
		where	nr_sequencia	= nr_seq_mens_guia_envio_w;
		
		end;
	end loop;
	close C02;
	
	delete	pls_mens_guia_arquivo
	where	nr_sequencia	= nr_seq_guia_mens_arq_w;
	
	end;
end loop;
close C01;

update	pls_lote_mens_guia_envio
set	dt_geracao_lote	= null
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_desfazer_lote_guia_mens;
/
