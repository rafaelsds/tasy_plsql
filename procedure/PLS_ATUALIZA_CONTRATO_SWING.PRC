create or replace
procedure pls_atualiza_contrato_swing
			(	nr_seq_contrato_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_congenere_w		number(10);
ie_tipo_beneficiario_w		Varchar2(3);
cd_cooperativa_w		varchar2(10);

begin

select	nvl(max(cd_cooperativa),0)
into	cd_cooperativa_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_p;

/*aaschlote 21/08/2010 - OS 242380*/
if	(cd_cooperativa_w > 0) then
	select	max(nr_sequencia)
	into	nr_seq_congenere_w
	from	pls_congenere
	where	cd_cooperativa	= cd_cooperativa_w;
	
	update	pls_contrato
	set	nr_seq_congenere	= nr_seq_congenere_w
	where	nr_sequencia		= nr_seq_contrato_p;
else
	update	pls_contrato
	set	nr_seq_congenere	= null
	where	nr_sequencia		= nr_seq_contrato_p;	
end if;
	
select	nvl(max(nr_seq_congenere),0)
into	nr_seq_congenere_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_p;

if (nr_seq_congenere_w	> 0) then
	ie_tipo_beneficiario_w	:= 'ENB';
elsif (nr_seq_congenere_w = 0) then
	ie_tipo_beneficiario_w	:= 'BE';
end if;

update	pls_contrato
set	ie_tipo_beneficiario	= ie_tipo_beneficiario_w
where	nr_sequencia	= nr_seq_contrato_p;

pls_gerar_regra_renov_contr(nr_seq_contrato_p, 'S', cd_estabelecimento_p, nm_usuario_p);

commit;

end pls_atualiza_contrato_swing;
/