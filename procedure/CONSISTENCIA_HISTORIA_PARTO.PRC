create or replace
procedure consistencia_historia_parto(	nr_atendimento_p		number,
				dt_admissao_p		date,
				ie_parto_normal_p		varchar2,
				ie_parto_cesaria_p		varchar2,
				ie_laqueadura_p		varchar2,
				ie_parto_analgesia_p	varchar2,
				ie_parto_episio_p		varchar2,
				dt_inicio_parto_p		date) is
										
dt_entrada_w	Date;
cd_estabelecimento_w integer;
										
begin

select	max(dt_entrada)
into	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select cd_estabelecimento
into cd_estabelecimento_w
from atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

if	(dt_entrada_w	is not null) then
	begin
	if	(dt_admissao_p is not null) and
		(dt_admissao_p < dt_entrada_w) then
		
		Wheb_mensagem_pck.exibir_mensagem_abort(263516);
	end if;
	if (nvl((pkg_i18n.get_user_locale()), 'pt_BR') <> 'de_AT') then
		if	(nvl(OBTER_UTILIZA_NOM(cd_estabelecimento_w),'N') = 'N') and
			(nvl(ie_parto_normal_p,'N') = 'N') and
			(nvl(ie_parto_cesaria_p,'N') = 'N') then
			
			Wheb_mensagem_pck.exibir_mensagem_abort(263517);
		end if;

		if	(nvl(ie_parto_normal_p,'N') = 'S') and
			(nvl(ie_laqueadura_p,'N') = 'S') then
			
			Wheb_mensagem_pck.exibir_mensagem_abort(263525);
		end if;
		if	(nvl(ie_parto_cesaria_p,'N') = 'S') and
			(nvl(ie_parto_analgesia_p,'N') = 'S') then
			
			Wheb_mensagem_pck.exibir_mensagem_abort(263526);
		end if;
		if	(nvl(ie_parto_cesaria_p,'N') = 'S') and
			(nvl(ie_parto_episio_p,'N') = 'S') then
			
			Wheb_mensagem_pck.exibir_mensagem_abort(263528);
		end if;
	end if;
	if	(dt_inicio_parto_p > sysdate) then
		begin
		
		Wheb_mensagem_pck.exibir_mensagem_abort(263529);
		end;
	end if;

	if	(dt_admissao_p is not null) and
		(dt_admissao_p > sysdate) then
		begin
		
		Wheb_mensagem_pck.exibir_mensagem_abort(263530);
		end;
	end if;

	end;
	end if;
end consistencia_historia_parto;
/
