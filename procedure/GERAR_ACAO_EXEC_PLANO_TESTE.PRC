create or replace
procedure gerar_acao_exec_plano_teste(	
				nr_sequencia_p			number,
				nr_seq_caso_teste_p		number,
				nr_seq_caso_tst_item_p		number,
				nr_seq_caso_tst_it_acao_p		number,
				nm_usuario_p			varchar2) is 

ie_tipo_teste_w		varchar2(15);
nr_seq_caso_teste_w	number(10);
nr_seq_item_w            	number(10);
nr_seq_acao_ant_w       	number(10);
nr_seq_apres_ct_w		number(5);
nr_seq_apres_it_w		number(5);
nr_seq_apres_acao_w	number(5);
cd_perfil_w              	number(5);
ds_caso_teste_w		varchar2(255);
ds_item_w		varchar2(255);
ds_premissas_w           	varchar2(2000);
ds_acao_w                	varchar2(2000);
ds_result_esperado_w     	varchar2(2000); 
ds_observacao_w          	varchar2(2000);

Cursor C01 is
	select	c.nr_sequencia,
		b.nr_sequencia,
		a.nr_sequencia,
		c.nr_seq_apres,
		b.nr_seq_apres,
		a.nr_seq_apres,
		a.cd_perfil,
		c.ds_caso_teste,
		b.ds_item,
		a.ds_premissas, 
		a.ds_acao, 
		a.ds_result_esperado, 
		a.ds_observacao
	from	teste_software_execucao e,
		teste_software d,
		teste_caso_teste c,
		teste_caso_teste_item b,
		teste_caso_teste_it_acao a
	where	b.nr_sequencia = a.nr_seq_item
	and	c.nr_sequencia = b.nr_seq_caso_teste
	and	d.nr_sequencia = c.nr_seq_teste
	and	d.nr_seq_proj_gpi = e.nr_seq_proj_gpi
	and	e.nr_sequencia = nr_sequencia_p
	and	d.ie_tipo_teste = 'P'
	and	d.ie_tipo_teste = nvl(e.ie_tipo_teste,d.ie_tipo_teste)
	and	c.nr_sequencia = nvl(nr_seq_caso_teste_p,c.nr_sequencia)
	and	b.nr_sequencia = nvl(nr_seq_caso_tst_item_p,b.nr_sequencia)
	and	a.nr_sequencia = nvl(nr_seq_caso_tst_it_acao_p,a.nr_sequencia)
	and	c.nr_seq_caso_teste_ant is not null
	and not exists (	select	1
			from	teste_soft_exec_acao x
			where	x.nr_seq_teste_software = e.nr_sequencia
			and	x.nr_seq_acao_ant = a.nr_sequencia)
	union all
	select	c.nr_sequencia,
		b.nr_sequencia,
		a.nr_sequencia,
		c.nr_seq_apres,
		b.nr_seq_apres,
		a.nr_seq_apres,
		a.cd_perfil,
		c.ds_caso_teste,
		b.ds_item,
		a.ds_premissas, 
		a.ds_acao, 
		a.ds_result_esperado, 
		a.ds_observacao
	from	teste_software_execucao e,
		teste_software d,
		teste_caso_teste c,
		teste_caso_teste_item b,
		teste_caso_teste_it_acao a
	where	b.nr_sequencia = a.nr_seq_item
	and	c.nr_sequencia = b.nr_seq_caso_teste
	and	d.nr_sequencia = c.nr_seq_teste
	and	d.ie_tipo_teste = e.ie_tipo_teste
	and	e.nr_sequencia = nr_sequencia_p	
	and	d.ie_tipo_teste <> 'P'
	and	c.nr_sequencia = nvl(nr_seq_caso_teste_p,c.nr_sequencia)
	and	b.nr_sequencia = nvl(nr_seq_caso_tst_item_p,b.nr_sequencia)
	and	a.nr_sequencia = nvl(nr_seq_caso_tst_it_acao_p,a.nr_sequencia)
	and not exists (	select	1
			from	teste_soft_exec_acao x
			where	x.nr_seq_teste_software = e.nr_sequencia
			and	x.nr_seq_acao_ant = a.nr_sequencia);

begin

if	(nvl(nr_sequencia_p,0) <> 0) then
	begin	
	open C01;
	loop
	fetch C01 into	
		nr_seq_caso_teste_w,
		nr_seq_item_w,
		nr_seq_acao_ant_w,
		nr_seq_apres_ct_w,
		nr_seq_apres_it_w,
		nr_seq_apres_acao_w,
		cd_perfil_w,
		ds_caso_teste_w,
		ds_item_w,
		ds_premissas_w,
		ds_acao_w,
		ds_result_esperado_w,
		ds_observacao_w;
	exit when C01%notfound;
		begin
		insert into teste_soft_exec_acao(	
				nr_sequencia,
				nr_seq_teste_software, 
				dt_atualizacao, 
				nm_usuario, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec,
				ds_premissas, 
				ds_acao, 
				ds_result_esperado, 
				ds_observacao, 
				cd_perfil,
				nr_seq_caso_teste,
				nr_seq_item,
				nr_seq_acao_ant,
				nr_seq_apres_ct,
				nr_seq_apres_it,
				nr_seq_apres,
				ds_caso_teste,
				ds_item)
			values(	teste_soft_exec_acao_seq.nextval,
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_premissas_w,
				ds_acao_w,
				ds_result_esperado_w,
				ds_observacao_w,
				cd_perfil_w,
				nr_seq_caso_teste_w,
				nr_seq_item_w,
				nr_seq_acao_ant_w,
				nr_seq_apres_ct_w,
				nr_seq_apres_it_w,
				nr_seq_apres_acao_w,
				ds_caso_teste_w,
				ds_item_w);

		end;
	end loop;
	close C01;

	commit;
	end;
end if;

end gerar_acao_exec_plano_teste;
/