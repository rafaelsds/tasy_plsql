create or replace
PROCEDURE alterar_situacao_grupo_medic(	nr_sequencia_p	NUMBER,
					ie_opcao_p	VARCHAR2) IS

BEGIN

IF	(nr_sequencia_p IS NOT NULL) THEN

	UPDATE	med_grupo_medic
	SET	     ie_situacao	= ie_opcao_p
	WHERE	nr_sequencia	= nr_sequencia_p;

     UPDATE  med_medic_padrao
     SET     ie_situacao        = ie_opcao_p
     WHERE   nr_seq_grupo_medic = nr_sequencia_p;

	COMMIT;
END IF;


END alterar_situacao_grupo_medic;
/