create or replace
procedure SCMO_IMPORTAR_RETORNO_SUS_AIH
				(nr_seq_retorno_p	in number) is

nr_aih_char_w		varchar2(255);
nr_aih_w		number(13);
nr_interno_conta_w	number(10);
vl_pago_w		number(15,2);
vl_cobrado_w		number(15,2);

cursor c01 is
select	obter_valor_campo_separador(ds_conteudo,1,';') nr_aih,
	replace(obter_valor_campo_separador(ds_conteudo,2,';'),'.','') vl_pago
from	w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p
and	obter_valor_campo_separador(ds_conteudo,1,';')	<> 'AIH';
				
begin

open C01;
loop
fetch C01 into	
	nr_aih_char_w,
	vl_pago_w;
exit when C01%notfound;
	begin

	nr_aih_w	:= somente_numero(nr_aih_char_w);
	
	select	max(nr_interno_conta)
	into	nr_interno_conta_w
	from	sus_aih_unif
	where	nr_aih		= nr_aih_w;

	select	max(vl_conta)
	into	vl_cobrado_w
	from	conta_paciente
	where	nr_interno_conta	= nr_interno_conta_w;
	
	insert	into convenio_retorno_movto (
		nr_sequencia,
		nr_seq_retorno,
		dt_atualizacao,
		nm_usuario,
		vl_cobrado,
		vl_pago,		
		nr_conta,
		ds_complemento)
	Values	(convenio_retorno_movto_seq.nextval,
		nr_seq_retorno_p,
		sysdate,
		'Tasy_Imp',				
		vl_cobrado_w,
		vl_pago_w,		
		nr_interno_conta_w,
		nr_aih_w);
	
	end;
end loop;
close C01;

delete 	from w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end SCMO_IMPORTAR_RETORNO_SUS_AIH;
/