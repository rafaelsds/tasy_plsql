CREATE OR REPLACE
PROCEDURE Can_Reaproveitamento_op(	
		nr_ordem_origem_p	number,
		nr_ordem_destino_p	number,
		cd_pessoa_fisica_p	varchar2,
		nm_usuario_p		varchar2) is

nr_sequencia_w		Number(10);
dt_inicio_preparo_w	Date;
nm_usuario_inic_prep_w	varchar2(15);
nr_seq_etapa_prod_w	Number(10);
nr_seq_ordem_w		Number(10);
cd_material_w		number(06);
cd_unidade_medida_w	varchar2(30);
ie_medic_diluente_w	varchar2(01);
nr_seq_lote_fornec_w	number(10);
qt_dose_real_w		number(18,6);
qt_perda_w		number(15,4);
qt_dose_prescr_w	number(18,6);
cd_unidade_medida_real_w	varchar2(30);
cd_motivo_baixa_w	varchar2(1);
nr_seq_item_prescr_w	number(10);
nr_prescricao_w		number(14);

Cursor C01 is
	select	cd_material,
		cd_unidade_medida,
		ie_medic_diluente,
		nr_seq_lote_fornec,
		qt_dose_real,
		qt_perda,
		cd_unidade_medida_real,
		cd_motivo_baixa,
		nr_seq_item_prescr
	from	can_ordem_prod_mat
	where	nr_seq_ordem	= nr_ordem_origem_p;

BEGIN

select	max(dt_inicio_preparo),
	max(nm_usuario_inic_prep),
	max(nr_sequencia)
into	dt_inicio_preparo_w,
	nm_usuario_inic_prep_w,
	nr_seq_ordem_w
from	can_ordem_prod
where	nr_sequencia	= nr_ordem_origem_p;

select	max(nr_seq_etapa_prod),
	max(nr_sequencia)
into	nr_seq_etapa_prod_w,
	nr_seq_ordem_w
from	can_ordem_prod
where	nr_sequencia	= nr_ordem_destino_p;

if	(nr_seq_etapa_prod_w is null) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(206944);
end if;

update	can_ordem_prod
set	dt_inicio_preparo	= dt_inicio_preparo_w,
	nm_usuario_inic_prep	= nm_usuario_inic_prep_w
where	nr_sequencia		= nr_ordem_destino_p;


select	can_reaproveitamento_seq.nextval
into	nr_sequencia_w
from	dual;

if	(nvl(nr_ordem_origem_p,0) > 0) then
	insert into can_reaproveitamento(
		nr_sequencia,           
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ordem_origem,
		nr_seq_ordem_destino,
		cd_pessoa_fisica)
	values(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_ordem_origem_p,
		nr_ordem_destino_p,
		cd_pessoa_fisica_p);
	commit;
end if;

open C01;
loop
fetch C01 into	
	cd_material_w,
	cd_unidade_medida_w,
	ie_medic_diluente_w,
	nr_seq_lote_fornec_w,
	qt_dose_real_w,
	qt_perda_w,	
	cd_unidade_medida_real_w,
	cd_motivo_baixa_w,
	nr_seq_item_prescr_w;
exit when C01%notfound;
	begin
	select 	can_ordem_prod_mat_seq.nextval
	into 	nr_sequencia_w
	from 	dual;

	select 	max(qt_dose),
	 	max(cd_unidade_medida),
	 	--max(nr_seq_prescricao), 	
		max(nr_prescricao),
		max(nr_sequencia)
	into	qt_dose_prescr_w,
		cd_unidade_medida_w,
		--nr_seq_item_prescr_w,
		nr_prescricao_w,	
		nr_seq_item_prescr_w
	from	can_ordem_item_prescr
	where 	nr_seq_ordem = nr_ordem_destino_p 
	and 	obter_ficha_tecnica_medic(cd_material) = obter_ficha_tecnica_medic(cd_material_w);
	
	insert into can_ordem_prod_mat(
		nr_sequencia,           
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		cd_material,
		qt_dose,
		cd_unidade_medida,
		ie_medic_diluente,
		nr_seq_lote_fornec,
		qt_dose_real,
		qt_perda,
		cd_unidade_medida_real,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_motivo_baixa,
		nr_seq_item_prescr,
		nr_seq_ordem_origem,
		ie_reaproveitado)
	values	(nr_sequencia_w,
		nr_ordem_destino_p,
		sysdate,
		nm_usuario_p,
		cd_material_w,
		nvl(qt_dose_prescr_w,1),
		nvl(cd_unidade_medida_w,'un'),
		ie_medic_diluente_w,
		nr_seq_lote_fornec_w,
		qt_dose_real_w,
		qt_perda_w,
		cd_unidade_medida_real_w,
		sysdate,
		nm_usuario_p,
		cd_motivo_baixa_w,
		nr_seq_item_prescr_w,
		nr_ordem_origem_p,
		'S');
	
	commit;
	
	end;
end loop;
close C01;

commit;

END Can_Reaproveitamento_op;
/
