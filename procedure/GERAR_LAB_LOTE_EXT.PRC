create or replace 
procedure gerar_lab_lote_ext(	nr_seq_lote_p		number,
				cd_entidade_p		varchar2,
				dt_prev_retorno_p	date,
				dt_envio_p		date,
				ds_observacao_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

begin

insert into lab_lote_externo(
		nr_sequencia, 
                dt_atualizacao, 
                dt_atualizacao_nrec,
		nm_usuario, 
		nm_usuario_nrec,
		dt_lote,
		cd_cgc,
                dt_retorno_prev,
                ds_observacao,
                dt_envio,
		cd_estabelecimento)
        values(	nr_Seq_lote_p,
		sysdate, 
		sysdate,
		nm_usuario_p, 
                nm_usuario_p, 
		sysdate,
                cd_entidade_p, 
                dt_prev_retorno_p , 
                ds_observacao_p,
                dt_envio_p, 
		cd_estabelecimento_p);
commit;

end;
/