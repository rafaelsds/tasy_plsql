create or replace
procedure cpa_desaprovar_pgto_escri(
		nm_usuario_p		Varchar2,
		nr_seq_banco_escrit_p	number,
		nr_titulo_p		number) 	is

begin

if	(nr_titulo_p <> 0) then
	begin
	update	conta_pagar_lib
	set	dt_liberacao         = null
	where	nr_seq_banco_escrit  = nr_seq_banco_escrit_p
	and	nr_titulo            = nr_titulo_p
	and	nm_usuario_lib       = nm_usuario_p;
	end;
else	
	begin
	update	conta_pagar_lib
	set	dt_liberacao	= null
	where	nr_seq_banco_escrit  = nr_seq_banco_escrit_p
	and	nm_usuario_lib	= nm_usuario_p
	and	nr_titulo	is null;
	end;
end if;

commit;

end cpa_desaprovar_pgto_escri;
/
