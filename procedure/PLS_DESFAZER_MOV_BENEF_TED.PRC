create or replace
procedure pls_desfazer_mov_benef_ted
		(	nr_seq_lote_mov_p	number,
			nm_usuario_p		Varchar2) is 

begin

delete	PLS_MOVIMENTACAO_BENEF
where	nr_seq_lote	= nr_seq_lote_mov_p;

update	PLS_LOTE_MOV_BENEF
set	DT_GERACAO_LOTE	= null
where	nr_sequencia	= nr_seq_lote_mov_p;

commit;

end pls_desfazer_mov_benef_ted;
/
