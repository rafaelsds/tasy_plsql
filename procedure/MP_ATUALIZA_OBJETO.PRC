create or replace
procedure mp_atualiza_objeto(	nr_sequencia_p		number,
				qt_topo_p		number,
				qt_esquerda_p		number,
				qt_altura_p		number,
				qt_largura_p		number,
				ds_lista_pontos_p	varchar2,
				nm_objeto_p		varchar2,
				nm_usuario_p		varchar2) is

ie_tipo_grupo_w		varchar2(255);
nr_seq_processo_ref_w	number(10);

begin

if	(nr_sequencia_p is not null) then

	select	c.ie_tipo_grupo,
		a.nr_seq_processo_ref
	into	ie_tipo_grupo_w,
		nr_seq_processo_ref_w
	from	mp_grupo_objeto c,
		mp_objeto b,
		mp_processo_objeto a
	where	c.nr_sequencia	= b.nr_seq_grupo
	and	b.nr_sequencia	= a.nr_seq_objeto
	and	a.nr_sequencia	= nr_sequencia_p;

	if	(ie_tipo_grupo_w = 'F') then
	
		update	mp_processo_objeto
		set	ds_lista_pontos	= ds_lista_pontos_p
		where	nr_sequencia	= nr_sequencia_p;
	else
		update	mp_processo_objeto
		set	qt_topo		= qt_topo_p,
			qt_esquerda	= qt_esquerda_p,
			qt_altura	= qt_altura_p,
			qt_largura	= qt_largura_p,
			nm_objeto	= nvl(nm_objeto_p,nm_objeto)
		where	nr_sequencia	= nr_sequencia_p;
	end if;

	mp_atualiza_processo_ref(nr_sequencia_p,nm_usuario_p);

	mp_atualizar_unid_org(nr_sequencia_p,nm_usuario_p);
end if;

commit;

end mp_atualiza_objeto;
/