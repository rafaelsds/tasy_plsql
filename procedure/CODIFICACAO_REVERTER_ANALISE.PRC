create or replace
procedure codificacao_reverter_analise ( 	nr_sequencia_p number,
											nm_usuario_p varchar ) is

begin

	if (nr_sequencia_p is not null and nm_usuario_p is not null) then

		update	codificacao_atendimento
		set		dt_fim_analise 		= null,
				dt_liberacao 		= null,				
				dt_atualizacao		= sysdate,
				nm_usuario			= nm_usuario_p,
				ie_status			= 'AI',
				nm_responsavel_atual	= nm_responsavel
		where	nr_sequencia 		= nr_sequencia_p;

	end if;

	commit;

end codificacao_reverter_analise;
/