create or replace
procedure pls_gerar_lanc_comissoes
		(	nr_titulo_p			number,
			nr_seq_repasse_p		number,
			nr_seq_comissao_p		number,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number) is 
			
/* nr_seq_vendedor_p:
	este par�metro s� precisa ser passado quando o repasse do tipo normal estiver sendo recalculado.
*/

vl_lancamento_w			number(15,2) := 0;
vl_juros_multa_w		number(15,2) := 0;
			
begin

vl_juros_multa_w := pls_obter_vl_juros_mult_titulo(nr_titulo_p);

if	(vl_juros_multa_w > 0) then

	insert into pls_repasse_lanc  
		       (	nr_sequencia, nr_seq_repasse, nr_seq_comissao, 
				vl_lancamento, vl_lanc_aplicado, ie_tipo_lancamento,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_regra, nr_seq_lancamento_vend, nr_titulo )
		values (	pls_repasse_lanc_seq.nextVal, nr_seq_repasse_p, nr_seq_comissao_p, 
				vl_juros_multa_w, vl_juros_multa_w, '5', -- ie_tipo_lancamento = 5 = Acr�scimo referente aos juros e multas do t�tulo a receber				
				sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				null, null, nr_titulo_p); 
				
end if;
				
--commit;

end pls_gerar_lanc_comissoes;
/