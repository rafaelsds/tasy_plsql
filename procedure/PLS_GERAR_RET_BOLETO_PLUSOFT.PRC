create or replace
procedure pls_gerar_ret_boleto_plusoft( nr_seq_solicitacao_p 	number,
					nr_seq_lote_p 		number, 
					nr_seq_mensalidade_p	number,
					nr_titulo_p		number,
					nm_arquivo_p		varchar2,
					ds_caminho_p		varchar2,
					ds_mensagem_p		varchar2) is

nr_seq_lote_reg_w 	number;
					
					
begin

	select 	max(nr_sequencia)
	into	nr_seq_lote_reg_w
	from	w_pls_lote_ret_mens
	where	nr_seq_solicitacao = nr_seq_solicitacao_p
	and	nr_seq_lote = nr_seq_lote_p;
	
	if(nr_seq_lote_reg_w is null) then
	
		select 	w_pls_lote_ret_mens_seq.nextval
		into	nr_seq_lote_reg_w
		from 	dual;
	
		insert into	w_pls_lote_ret_mens (nr_sequencia , nr_seq_solicitacao, nr_seq_lote)  
		values		(nr_seq_lote_reg_w, nr_seq_solicitacao_p , nr_seq_lote_p ); 	
	end if;


	insert into	w_pls_retorno_boleto_mens ( nr_sequencia, nr_seq_lote_reg, nr_seq_mensalidade, 
			nr_titulo, nm_arquivo, ds_caminho, ds_mensagem ) 
	values		(w_pls_retorno_boleto_mens_seq.nextval, nr_seq_lote_reg_w, nr_seq_mensalidade_p, 
 			nr_titulo_p, nm_arquivo_p, ds_caminho_p, ds_mensagem_p );	
			
commit;

end pls_gerar_ret_boleto_plusoft;
/
