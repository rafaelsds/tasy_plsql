create or replace
procedure san_fechamento_inutilizacao	(	nr_sequencia_p	Number,
						nm_usuario_p	Varchar2) is 

begin

if (nvl(nr_sequencia_p,0) > 0) then

	update	san_inutilizacao
	set	dt_fechamento	= sysdate, 
		nm_usuario_fechamento = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;
	
end if;

commit;

end san_fechamento_inutilizacao;
/