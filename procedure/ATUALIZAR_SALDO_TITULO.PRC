create or replace
procedure atualizar_saldo_titulo(
		nr_titulo_p	number,
		nm_usuario_p	varchar2) is 
begin
if	(nr_titulo_p is not null) and
	(nm_usuario_p is not null) then
	begin
	atualizar_saldo_tit_pagar(nr_titulo_p, nm_usuario_p);
	gerar_w_tit_pag_imposto(nr_titulo_p, nm_usuario_p);
	commit;
	end;
end if;
end atualizar_saldo_titulo;
/