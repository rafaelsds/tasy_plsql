create or replace 
procedure enviar_email_cobranca (
				nr_sequencia_p		number,
				ds_email_destino_p	varchar2,
				nm_usuario_p		varchar2) is

begin

insert	into cobranca_envio( 
		ds_destino,             
		dt_atualizacao,         
		dt_atualizacao_nrec,    
		dt_envio,               
		nm_usuario,             
		nm_usuario_nrec,        
		nr_seq_cobranca,        
		nr_sequencia) 
values	( 
	ds_email_destino_p,
	sysdate,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	nr_sequencia_p,
	cobranca_envio_seq.nextval );
commit;
								
end enviar_email_cobranca;
/