create or replace
procedure Consistir_sala_agecons(nr_seq_sala_p		number,
				 dt_agenda_p		date,
				 nr_minuto_duracao_p	number,
				 cd_estabelecimento_p	number,
				 nm_usuario_p		Varchar2,
				 nr_Seq_Agenda_p	number) is 
				 
ie_consistencia_w	varchar2(1)	:= 'N';
dt_agenda_w		date;
nr_duracao_w		number(10,0);

Cursor C01 is
	select	dt_agenda,
		nr_minuto_duracao
	from	agenda_consulta
	where	(dt_agenda between dt_agenda_p and dt_agenda_p + (nr_minuto_duracao_p / 1440) - (1/86399) or
		dt_agenda_p between dt_agenda and dt_agenda + (nr_minuto_duracao / 1440) - (1/86399))
	and	nr_seq_sala = nr_seq_sala_p
	and	ie_status_agenda not in ('C','L')
	and	nr_sequencia	<> nr_Seq_Agenda_p
	order by 1;

begin

open C01;
loop
fetch C01 into	
	dt_agenda_w,
	nr_duracao_w;
exit when C01%notfound;
	begin
	
	ie_consistencia_w	:= 'S';
	
	end;
end loop;
close C01;

if	(ie_consistencia_w = 'S') then
	Wheb_mensagem_pck.exibir_mensagem_abort(262408);
end if;


commit;

end Consistir_sala_agecons;
/
