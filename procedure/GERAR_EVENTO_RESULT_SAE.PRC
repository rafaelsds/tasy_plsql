CREATE OR REPLACE
PROCEDURE Gerar_evento_result_sae(	nr_atendimento_p	NUMBER,									
									nr_seq_evento_p		NUMBER,
									cd_setor_atendimento_p	NUMBER,
									ds_evento_p			VARCHAR2,
									nm_usuario_p		VARCHAR2,
									cd_pessoa_fisica_p	Varchar2,
									ie_liberar_p		Varchar2 default 'N',
									nr_seq_sae_p		Number default null,
									nr_seq_item_p		Number default null,	
									nr_seq_result_p		Number default null) IS

cd_estabelecimento_w	Number(14,0);
nr_sequencia_w			Number(10);
cd_profissional_w		Varchar(15);
dt_liberacao_w			Date := null;
ie_liberar_w			Varchar2(5);
ds_evento_w				Varchar2(4000);
ds_item_w				Varchar2(255);
ds_result_w				Varchar2(255);
ds_mensagem_w			Varchar2(4000);

BEGIN

if	(cd_pessoa_fisica_p is not null) and
	(nr_seq_evento_p is not null) and
	(cd_setor_atendimento_p is not null ) and
	(ds_evento_p is not null) then
	
	ie_liberar_w := ie_liberar_p;
	
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	
	if	(ie_liberar_w = 'S') then		
		dt_liberacao_w := Sysdate;
	end if;
	
	cd_profissional_w :=  Obter_Dados_Usuario_Opcao(nm_usuario_p,'C');
	
	ds_mensagem_w := substr(obter_texto_tasy (263704, wheb_usuario_pck.get_nr_seq_idioma),1,4000);
	
	if	(nr_seq_item_p is not null) and
		(nr_seq_result_p is not null) then
		begin			
		ds_item_w := substr(obter_desc_item_examinar(nr_seq_item_p),1,100);
		ds_result_w := substr(obter_desc_item_resultado(nr_seq_result_p),1,100);
		exception
			when others then
				ds_item_w	:= null;
				ds_result_w := null;
			end;	
		
	end if;
	
	If	(ds_item_w is not null) then
		
		ds_mensagem_w := substr((ds_mensagem_w || chr(13) ||ds_item_w),1,4000);
		
		if	(ds_result_w is not null) then
		
			ds_mensagem_w := substr((ds_mensagem_w || ' / ' ||ds_result_w),1,4000);
		
		end if;
		
	end if;
	
	if	(ds_mensagem_w  is not null) then
		ds_evento_w := substr((ds_mensagem_w || chr(13) || chr(13) ||ds_evento_p),1,4000);
	else
		ds_evento_w := substr(ds_evento_p,1,4000);
	end if;
	
		Select  qua_evento_paciente_seq.nextval
		into	nr_sequencia_w
		from 	dual;
	
		insert into qua_evento_paciente (	nr_sequencia,
											nr_seq_evento,
											ds_evento,
											nr_atendimento,
											cd_pessoa_fisica,
											cd_profissional,
											nm_usuario,
											nm_usuario_origem,
											nm_usuario_nrec,
											nm_usuario_reg,
											cd_estabelecimento,
											cd_setor_atendimento,
											dt_atualizacao,
											dt_atualizacao_nrec,
											dt_cadastro,
											dt_evento,
											ie_situacao,
											dt_liberacao,
											nr_seq_sae,
                                            ie_status,
											ie_origem,
											cd_funcao_ativa)
								values 	(	nr_sequencia_w,
											nr_seq_evento_p,
											ds_evento_w,
											nr_atendimento_p,
											cd_pessoa_fisica_p,
											cd_profissional_w,
											nm_usuario_p,
											nm_usuario_p,
											nm_usuario_p,
											nm_usuario_p,
											cd_estabelecimento_w,
											cd_setor_atendimento_p,
											Sysdate,
											Sysdate,
											Sysdate,
											Sysdate,
											'A',
											dt_liberacao_w,
											nr_seq_sae_p,
                                            1,
											'S',
											obter_funcao_ativa);
	

	commit;	
	

	if	(ie_liberar_w = 'S') then	
	
		gerar_ficha_UPP_evento(nr_sequencia_w,nm_usuario_p);
	
	end if;
	
end if;

END Gerar_evento_result_sae;
/