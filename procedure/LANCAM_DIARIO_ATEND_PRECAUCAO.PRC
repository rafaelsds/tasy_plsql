create or replace
procedure lancam_diario_atend_precaucao ( nm_usuario_p varchar2 ) is

cursor c01 is
  select  a.nr_atendimento,
    b.nr_seq_precaucao,
    b.nr_seq_motivo_isol
  from    atendimento_paciente a,
          atendimento_precaucao b
  where   a.nr_atendimento = b.nr_atendimento
  and     a.dt_alta is null
  and     b.dt_liberacao is not null
  and     (b.dt_termino is null or b.dt_termino > sysdate)
  and     b.dt_inativacao is null
  and     b.dt_final_precaucao is null
  and     a.dt_fim_conta is null;

begin

for r_c01_w in c01 loop
  gerar_lancamento_automatico(r_c01_w.nr_atendimento, null, 623, nm_usuario_p, null, r_c01_w.nr_seq_precaucao, r_c01_w.nr_seq_motivo_isol, null, null, null);
end loop;

commit;

end lancam_diario_atend_precaucao;
/
