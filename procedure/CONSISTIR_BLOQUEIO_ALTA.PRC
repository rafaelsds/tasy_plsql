create or replace
procedure consistir_bloqueio_alta	(
						cd_motivo_alta_p	number,
						nr_atendimento_p	number,
						nm_usuario_p		Varchar2) is 
ie_possui_regra_w	number(10);

nr_sequencia_w		number(10);
cd_motivo_alta_w	number(10);
ie_tipo_atendimento_w	number(10);
cd_perfil_w		number(10);
cd_perfil_ww		number(10);
ie_tipo_atendimento_ww	number(10);
ie_abortar_w		varchar2(1) := 'N';

begin
cd_perfil_ww	:= obter_perfil_ativo;

if 	(nvl(cd_motivo_alta_p,0) > 0) and
	(nvl(nr_atendimento_p,0) > 0) then

	select 	count(*)
	into	ie_possui_regra_w
	from	regra_bloqueio_alta
	where	ie_situacao = 'A';	

	if (nvl(ie_possui_regra_w,0) > 0) then
		
		select	max(ie_tipo_atendimento)
		into	ie_tipo_atendimento_ww
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
		
		select 	decode(count(*),0,'N','S')
		into	ie_abortar_w
		from	regra_bloqueio_alta
		where	ie_situacao = 'A'
		and	(ie_tipo_atendimento = ie_tipo_atendimento_ww or ie_tipo_atendimento is null)
		and	(cd_perfil = cd_perfil_ww or cd_perfil is null)
		and	(cd_motivo_alta = cd_motivo_alta_p or cd_motivo_alta is null);
		
	
		if (ie_abortar_w = 'S') then
		/*
		N�o ser� poss�vel gerar a alta para este atendimento.
		Favor verificar a regra "Regra de bloqueio de alta"!
		*/
			Wheb_mensagem_pck.exibir_mensagem_abort(237594);
		end if;
	end if;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end consistir_bloqueio_alta;
/
