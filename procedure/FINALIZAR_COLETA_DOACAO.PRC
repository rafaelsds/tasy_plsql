create or replace
procedure finalizar_coleta_doacao(	nr_seq_doacao_p		number,
					cd_pessoa_coleta_p	varchar2,
					ie_status_p		varchar2,
					qt_coletada_p		number,
					nm_usuario_p		varchar2,
                                        dt_fim_coleta_p         date default null) is 


		
ie_atualiza_min_coleta_w	varchar2(1) := 'N';
dt_fim_coleta_w			date;
qtd_w				number;
nr_seq_doacao_amostra_w		number(10);
ie_amostra_inapta_w		varchar2(1) := 'N';
qt_coletada_w			number(4);
cd_pessoa_coleta_w		varchar2(10);
ie_param_479_w			varchar2(1)  := 'N';
nr_marca_bolsa_w		san_doacao.nr_marca_bolsa%type;
ie_tipo_bolsa_w			san_doacao.ie_tipo_bolsa%type;
dt_producao_w			san_producao.dt_producao%type;
nr_seq_conservante_w		san_producao.nr_seq_conservante%type;
qt_validade_bolsa_w		san_marca_bolsa.qt_validade_bolsa%type;
nr_seq_derivado_w		san_producao.nr_seq_derivado%type;
dt_validade_w			san_producao.dt_vencimento%type;

cursor c01 is
select	dt_producao,
	nr_seq_derivado
from	san_producao
where	nr_seq_doacao = nr_seq_doacao_p;


begin

if ( nr_seq_doacao_p is not null) then

	select 	nvl(dt_fim_coleta_p, nvl(max(dt_fim_coleta), sysdate)),
		nvl(max(nr_seq_doacao_amostra),0),
		nvl(max(qt_coletada),0),
		max(cd_pessoa_coleta),
		max(ie_tipo_bolsa),
		max(nr_marca_bolsa)
	into 	dt_fim_coleta_w,
		nr_seq_doacao_amostra_w,
		qt_coletada_w,
		cd_pessoa_coleta_w,
		ie_tipo_bolsa_w,
		nr_marca_bolsa_w
	from  	san_doacao
	where  	nr_sequencia  = nr_seq_doacao_p;


	update 	san_doacao
	set	dt_fim_coleta 		= dt_fim_coleta_w,
		nm_usuario_fim_coleta 	= nm_usuario_p,
		dt_coleta 		= sysdate,
		cd_pessoa_coleta	= nvl(cd_pessoa_coleta_p,cd_pessoa_coleta_w),
		ie_status 		= ie_status_p,		
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		qt_volume_atual		= nvl(qt_coletada_p, qt_coletada_w),
		qt_coletada		= qt_coletada_p
	where 	nr_sequencia		= nr_seq_doacao_p;

	ie_atualiza_min_coleta_w 	:= obter_valor_param_usuario(450,157,obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
	ie_amostra_inapta_w 		:= obter_valor_param_usuario(450,390,obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
	ie_param_479_w 			:= Obter_Valor_Param_Usuario(450, 479, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

	if (ie_atualiza_min_coleta_w = 'S') then

		update	san_doacao
		set	qt_min_coleta	= round((to_date(to_char(sysdate,'dd/mm/yyyy')||' '||nvl(to_char(dt_fim_coleta,'hh24:mi:ss'), to_char(sysdate,'hh24:mi:ss')),'dd/mm/yyyy hh24:mi:ss') - to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(dt_inicio_coleta,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) * 1440)
		where	nr_sequencia	= nr_seq_doacao_p;
	
	end if;

	select	count(*)
	into	qtd_w
	from	san_doacao_reacao a,
		san_tipo_reacao x
	where	x.nr_sequencia = a.nr_seq_reacao
	and	x.ie_inutilizar = 'S'
	and	a.nr_seq_doacao = nr_seq_doacao_p;
	
	if	(qtd_w > 0) then
		update	san_doacao
		set	dt_inutilizacao = sysdate
		where	nr_sequencia = nr_seq_doacao_p;
	else
		san_gerar_paciente_fim_prod(nr_seq_doacao_p, nm_usuario_p, obter_perfil_ativo, wheb_usuario_pck.get_cd_estabelecimento);
	end if;
	
	if	(ie_amostra_inapta_w = 'S') and (nr_seq_doacao_amostra_w > 0) then

		update	san_doacao
		set	ie_avaliacao_final = 'I'
		where	nr_sequencia = nr_seq_doacao_p;
		
	end if;
	
	if	(ie_param_479_w = 'S') then
		update	san_doacao
		set	cd_pessoa_fim_coleta = obter_pf_usuario(nm_usuario_p, 'C')
		where	nr_sequencia = nr_seq_doacao_p;
	end if;
	
	qtd_w := 0;
	
	select	count(*)
	into 	qtd_w
	from 	san_producao
	where 	nr_seq_doacao = nr_seq_doacao_p;
	
	if	( qtd_w > 0
		and  nr_marca_bolsa_w is not null
		and ie_tipo_bolsa_w is not null) then
		
		select 	max(nr_seq_conservante),
			max(qt_validade_bolsa)
		into 	nr_seq_conservante_w,
			qt_validade_bolsa_w
		from 	san_marca_bolsa
		where 	nr_sequencia = nr_marca_bolsa_w
		and 	ie_tipo_bolsa = ie_tipo_bolsa_w;
		
		
		
		if	( nr_seq_conservante_w > 0 ) then
			open c01;
			loop
				fetch c01 into
					dt_producao_w,
					nr_seq_derivado_w;
				exit when c01%notfound;
				begin
					dt_validade_w := san_calc_vencimento_conserv(dt_producao_w, nr_seq_derivado_w, nr_seq_conservante_w, null, ie_tipo_bolsa_w);
					
					if	( dt_validade_w is not null and dt_validade_w > dt_producao_w ) then

						update	san_producao
						set	dt_vencimento = dt_validade_w,
							nr_seq_conservante = nr_seq_conservante_w
						where 	nr_seq_derivado = nr_seq_derivado_w
						and 	nr_seq_doacao = nr_seq_doacao_p;

					elsif	( qt_validade_bolsa_w > 0 ) then

						update	san_producao
						set	dt_vencimento = (dt_producao_w + qt_validade_bolsa_w),
							nr_seq_conservante = nr_seq_conservante_w
						where 	nr_seq_derivado = nr_seq_derivado_w
						and 	nr_seq_doacao = nr_seq_doacao_p;

					end if;
				end;
			end loop;
			close c01;
		end if;
	end if;

	--SAN EVENTO - CONFIRMAR COLETA
	executa_evento_hemoterapia( nm_usuario_p , nr_seq_doacao_p , 3 );
end if;

commit;

end finalizar_coleta_doacao;
/
