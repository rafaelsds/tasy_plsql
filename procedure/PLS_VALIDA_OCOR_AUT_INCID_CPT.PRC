create or replace
procedure pls_valida_ocor_aut_incid_cpt
				(	nr_seq_ocor_combinada_p		Number,
					nr_seq_ocorrencia_p		Number,
					nr_seq_segurado_p		Number,
					nr_seq_motivo_glosa_p		Number,
					nr_seq_guia_p			Number,
					nr_seq_requisicao_p		Number,
					nr_seq_execucao_p		Number,
					ie_utiliza_filtro_p		Varchar2,
					nr_seq_param1_p			Number,
					nr_seq_param2_p			Number,
					nr_seq_param3_p			Number,
					nr_seq_param4_p			Number,
					nr_seq_param5_p			Number,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		Number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar se o beneficiario possui declaracao de saude liberada. Se tiver declaracao de saude e nao gerou a glsoa 1421, deve gerar analise para verificacao
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_segurado_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
dt_liberacao_w			date;
nr_seq_proc_w			number(10);
cd_procedimento_w		varchar2(10);
ie_origem_proced_w		varchar2(2);
qt_glosa_proced_w		number(10);
nr_seq_motivo_glosa_w		number(10);
cd_motivo_tiss_w		number(10);
ie_gerou_glosa_1421		varchar2(1) := 'N';
ie_tipo_tempo_w			Varchar2(255);	
qt_tempo_contratacao_w		Number(10);
dt_regra_w			Date;
ie_gerou_ocor_cabecalho_w	Varchar2(2);
qt_dias_contrato_w		number(10);
dt_inclusao_operadora_w		date;
ie_gerar_ocorrencia_w		varchar2(2);
ie_tipo_item_w			varchar2(2);
ie_regra_w			Varchar2(2);
nr_seq_oc_benef_w		Number(10);
ie_tipo_ocorrencia_w		Varchar2(2);
nr_seq_contrato_w		number(10);

Cursor C01 is
	select 	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from 	pls_guia_plano_proc 
	where 	nr_seq_guia = nr_seq_guia_p;
	
Cursor C02 is
	select 	nr_seq_motivo_glosa
	from 	pls_guia_glosa 
	where 	nr_seq_guia_proc 	= nr_seq_proc_w;
	
Cursor C03 is
	select	qt_tempo_contratacao,
		ie_tipo_tempo
	from	pls_validacao_aut_decl_cpt
	where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p
	and	ie_situacao 	= 'A';
	
Cursor C11 is
	select 	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from 	pls_requisicao_proc 
	where 	nr_seq_requisicao = nr_seq_requisicao_p;
	
Cursor C12 is
	select 	nr_seq_motivo_glosa
	from 	pls_requisicao_glosa 
	where 	nr_seq_req_proc 	= nr_seq_proc_w;
	
begin
if	(nr_seq_guia_p is not null) then
	begin
		select	nr_seq_segurado
		into	nr_seq_segurado_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		nr_seq_segurado_w		:= null;
	end;

elsif	(nr_seq_requisicao_p is not null) then
	begin
		select	nr_seq_segurado
		into	nr_seq_segurado_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_segurado_w		:= null;
	end;
end if;

begin
	select	cd_pessoa_fisica,
		nr_seq_contrato
	into	cd_pessoa_fisica_w,
		nr_seq_contrato_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
exception
when others then
	cd_pessoa_fisica_w	:= null;
end;

begin
	select	max(dt_liberacao)
	into	dt_liberacao_w
	from	pls_declaracao_segurado
	where	nr_seq_segurado		= nr_seq_segurado_w;
exception
when others then
	dt_liberacao_w		:= null;
end;

if	(dt_liberacao_w	is null) then
	begin
		select	max(dt_liberacao)
		into	dt_liberacao_w
		from	pls_declaracao_segurado
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_seq_contrato		= nr_seq_contrato_w;
	exception
	when others then
		dt_liberacao_w		:= null;
	end;
end if;

if	(dt_liberacao_w is not null) then
	if	(nr_seq_guia_p is not null) then
		open C01;
		loop
		fetch C01 into	
			nr_seq_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w;
		exit when C01%notfound;
			begin	
			ie_gerou_glosa_1421	:= 'N';
			
			select 	count(1)
			into	qt_glosa_proced_w
			from 	pls_guia_glosa 
			where 	nr_seq_guia_proc 	= nr_seq_proc_w;
			
			if	(qt_glosa_proced_w > 0) then				
				open C02;
				loop
				fetch C02 into	
					nr_seq_motivo_glosa_w;
				exit when C02%notfound;
					begin						
						select 	cd_motivo_tiss
						into	cd_motivo_tiss_w
						from 	tiss_motivo_glosa 
						where	nr_sequencia = 	nr_seq_motivo_glosa_w;
						
						if	(cd_motivo_tiss_w	= 1421) then
							ie_gerou_glosa_1421	:= 'S';
							exit;
						end if;							
					end;
				end loop;
				close C02;
			end if;
			
			if	(ie_gerou_glosa_1421	= 'N') then 
				open C03;
				loop
				fetch C03 into	
					qt_tempo_contratacao_w,
					ie_tipo_tempo_w;
				exit when C03%notfound;
					begin
					if	(nr_seq_segurado_w	is not null) then
					begin
						select	dt_inclusao_operadora
						into	dt_inclusao_operadora_w
						from	pls_segurado
						where	nr_sequencia	= nr_seq_segurado_w;
					exception
					when others then
						dt_inclusao_operadora_w	:= null;
					end;
					end if;
					
					if	(ie_tipo_tempo_w	= 'D') then
						dt_regra_w	:= trunc(sysdate - (qt_tempo_contratacao_w));
					elsif	(ie_tipo_tempo_w	= 'M') then
						dt_regra_w	:= (add_months(sysdate, - qt_tempo_contratacao_w));
					elsif	(ie_tipo_tempo_w	= 'A') then
						dt_regra_w	:= (add_months(sysdate, - qt_tempo_contratacao_w * 12)); /* Vezes 12 meses ao ano */
					end if;
					
					if	(dt_inclusao_operadora_w	> dt_regra_w) then
						ie_gerar_ocorrencia_w := 'S';

						if 	(ie_utiliza_filtro_p = 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
										null, nr_seq_proc_w, null,
										null, null, null,
										cd_procedimento_w, ie_origem_proced_w, null,
										ie_gerou_ocor_cabecalho_w, null, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
								exit;
							end if;
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S')	then
							if	(nr_seq_guia_p	is not null) then
								ie_tipo_item_w	:= 1;
							end if;

							pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
										nr_seq_guia_p, null, nr_seq_proc_w,
										null, nr_seq_ocor_combinada_p, nm_usuario_p,
										null, nr_seq_motivo_glosa_p, ie_tipo_item_w,
										cd_estabelecimento_p, 'N' ,null,
										nr_seq_oc_benef_w, null,
										null, null, null);
										
							 pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												nr_seq_guia_p, null, null,
												nr_seq_proc_w, null, null,
												null, null, null,
												nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;	
					end;
				end loop;
				close C03;
			end if;
			end;
		end loop;
		close C01;
		
	elsif	(nr_seq_requisicao_p is not null) then
		open C11;
		loop
		fetch C11 into	
			nr_seq_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w;
		exit when C11%notfound;
			begin	
			ie_gerou_glosa_1421	:= 'N';
			
			select 	count(1)
			into	qt_glosa_proced_w
			from 	pls_requisicao_glosa 
			where 	nr_seq_req_proc 	= nr_seq_proc_w;
			
			if	(qt_glosa_proced_w > 0) then				
				open C12;
				loop
				fetch C12 into	
					nr_seq_motivo_glosa_w;
				exit when C12%notfound;
					begin						
						select 	cd_motivo_tiss
						into	cd_motivo_tiss_w
						from 	tiss_motivo_glosa 
						where	nr_sequencia = 	nr_seq_motivo_glosa_w;
						
						if	(cd_motivo_tiss_w	= 1421) then
							ie_gerou_glosa_1421	:= 'S';
							exit;
						end if;							
					end;
				end loop;
				close C12;
			end if;
			
			if	(ie_gerou_glosa_1421	= 'N') then 
				open C03;
				loop
				fetch C03 into	
					qt_tempo_contratacao_w,
					ie_tipo_tempo_w;
				exit when C03%notfound;
					begin
					if	(nr_seq_segurado_w	is not null) then
					begin
						select	dt_inclusao_operadora
						into	dt_inclusao_operadora_w
						from	pls_segurado
						where	nr_sequencia	= nr_seq_segurado_w;
					exception
					when others then
						dt_inclusao_operadora_w	:= null;
					end;
					end if;
					
					if	(ie_tipo_tempo_w	= 'D') then
						dt_regra_w	:= trunc(sysdate - (qt_tempo_contratacao_w));
					elsif	(ie_tipo_tempo_w	= 'M') then
						dt_regra_w	:= (add_months(sysdate, - qt_tempo_contratacao_w));
					elsif	(ie_tipo_tempo_w	= 'A') then
						dt_regra_w	:= (add_months(sysdate, - qt_tempo_contratacao_w * 12)); /* Vezes 12 meses ao ano */
					end if;
					
					if	(dt_inclusao_operadora_w	> dt_regra_w) then
						ie_gerar_ocorrencia_w := 'S';

						if 	(ie_utiliza_filtro_p = 'S') then
						/* Tratamento para filtros */
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
										null, null, null,
										nr_seq_proc_w, null, null,
										cd_procedimento_w, ie_origem_proced_w, null,
										ie_gerou_ocor_cabecalho_w, null, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
								exit;
							end if;
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S')	then
							if	(nr_seq_requisicao_p	is not null) then
								ie_tipo_item_w	:= 9;
							end if;

							pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
										null, null, nr_seq_proc_w,
										null, nr_seq_ocor_combinada_p, nm_usuario_p,
										null, nr_seq_motivo_glosa_p, ie_tipo_item_w,
										cd_estabelecimento_p, 'N' ,null,
										nr_seq_oc_benef_w, null,
										null, null, null);
										
							 pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												null, nr_seq_requisicao_p, null,
												null, null, nr_seq_proc_w,
												null, null, null,
												nm_usuario_p, cd_estabelecimento_p);
						end if;
					end if;	
					end;
				end loop;
				close C03;
			end if;
			end;
		end loop;
		close C11;
	end if;
end if;

end pls_valida_ocor_aut_incid_cpt;
/
