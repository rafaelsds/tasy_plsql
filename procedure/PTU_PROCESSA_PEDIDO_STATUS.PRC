create or replace
procedure ptu_processa_pedido_status(	nr_seq_pedido_status_p  		ptu_pedido_status.nr_sequencia%type,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_resp_ped_status_p	out	ptu_resp_pedido_status.nr_sequencia%type) is

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_origem_w		ptu_controle_execucao.nr_sequencia%type;

begin

cd_estabelecimento_w := cd_estabelecimento_p;

--Quando as transa��es s�o geradas pelo WebService, n�o existe estabelecimento definido, ent�o � verificado o estabeleicmento do par�metro
if	( cd_estabelecimento_w is null	) then
	cd_estabelecimento_w := ptu_obter_estab_padrao;
end if;

--Rotina utilizada para encontrar e atualizar a sequencia da Autoriza��o e da Requisi��o no pedido de status
ptu_atualizar_pedido_status(nr_seq_pedido_status_p, nm_usuario_p);

--Rotina utilizada para gerar a resposta do pedido de status da transa��o
ptu_gerar_pedido_status(nr_seq_pedido_status_p, cd_estabelecimento_p, nm_usuario_p, nr_seq_origem_w);

--Retorna a sequ�ncia da resposta de confirma��o gerada para o 00311 - Pedido de cancelamento, esta sequ�ncia � utilizada pelo WebService para gerar o XML de resposta do pedido
select	max(a.nr_sequencia)
into	nr_seq_resp_ped_status_p
from	ptu_resp_pedido_status a
where	a.nr_seq_origem = nr_seq_origem_w;

end ptu_processa_pedido_status;
/