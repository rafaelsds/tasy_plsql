create or replace
procedure qua_gerar_nc_nota_fiscal(
				cd_material_p		number,
				nr_seq_tipo_p		number,
				cd_setor_atendimento_p	number,
				cd_estabelecimento_p	number,
				dt_nao_conformidade_p	date default sysdate,
				cd_pf_abertura_p	varchar2,
				ds_nao_conformidade_p	varchar2,
				cd_fornecedor_p		varchar2,
				nm_usuario_p		varchar2,
				cd_procedimento_p	number,
				ie_origem_proced_p	number) as

nr_sequencia_w			number(10);
dt_abertura_w			date;

begin
dt_abertura_w := nvl(dt_nao_conformidade_p, sysdate);

select	qua_nao_conformidade_seq.nextval
into	nr_sequencia_w
from	dual;

insert	into qua_nao_conformidade(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_abertura,
		cd_pf_abertura,
		ds_nao_conformidade,
		cd_setor_atendimento,
		nm_usuario_origem,
		ie_status,
		nr_seq_tipo,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material,
		cd_fornecedor,
		cd_procedimento,
		ie_origem_proced)
values(		nr_sequencia_w,
		cd_estabelecimento_p,
		dt_abertura_w,
		nm_usuario_p,
		dt_abertura_w,
		cd_pf_abertura_p,
		ds_nao_conformidade_p,
		cd_setor_atendimento_p,
		nm_usuario_p,
		WHEB_MENSAGEM_PCK.get_texto(311868),
		nr_seq_tipo_p,
		dt_abertura_w,
		nm_usuario_p,
		cd_material_p,
		cd_fornecedor_p,
		cd_procedimento_p,
		ie_origem_proced_p);
		
commit;

end  qua_gerar_nc_nota_fiscal;
/