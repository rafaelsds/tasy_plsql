create or replace
procedure enviar_email_regra_equipamento( nr_seq_agenda_p		number,
										 ie_momento_p			varchar,
										 nm_usuario_p			varchar,
										 cd_estabelecimento_p	number,
										 cd_equipamento_p 		number) is

ie_paciente_internado_w		varchar2(1);
ie_tipo_convenio_w			number(2);
cd_pessoa_fisica_w			varchar2(10);
nr_atendimento_w			number(10);			
nr_seq_regra_w				number(10);
ds_mensagem_w				varchar2(4000);	 
ds_titulo_w					varchar2(2000);
ds_email_remetente_w		varchar2(255);
ds_seq_regra_email_w		varchar2(255);
ds_lista_regras_w			varchar2(255);
ds_email_destino_w			varchar2(2000);
ie_aborta_email_w			varchar2(1);
ie_clinica_w				number(5);	
dt_agendamento_w			date;	
dt_entrada_w				date;
ie_datas_iguais_w			varchar2(1);
ie_email_por_servico_w 		varchar2(1);
ie_carater_cirurgia_w		varchar2(1);
ie_enviar_sempre_w			varchar2(1);
dt_agenda_w					date;
ds_equipamento_w		equipamento.ds_equipamento%type;

cursor	c01 is
	select	d.nr_sequencia,
			nvl(d.ie_enviar_sempre,'N')
	from   	param_regra_envia_email c,
			regra_envio_email_agenda d
	where	c.nr_seq_regra	= d.nr_sequencia
	and     d.ie_momento = ie_momento_p
	and		((ie_momento_p <> 'SA') 
			or (Obter_Se_Contido(d.nr_sequencia,ds_seq_regra_email_w)= 'N'))
	and		(nvl(c.ie_tipo_convenio,nvl(ie_tipo_convenio_w,0)) = nvl(ie_tipo_convenio_w,0))
	and		(nvl(c.ie_paciente_internado,nvl(ie_paciente_internado_w,'N')) 	= nvl(ie_paciente_internado_w,'N'))
	and		(nvl(c.ie_clinica,ie_clinica_w) = ie_clinica_w)
	and		(nvl(c.ie_carater_cirurgia,nvl(ie_carater_cirurgia_w,'XPTO')) = nvl(ie_carater_cirurgia_w,'XPTO'))
	and		((nvl(ie_datas_iguais,'N') = 'N') or (ie_datas_iguais_w = 'S'))
	and 	(nvl(d.cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0))
	and		((nvl(qt_dias,0) = 0) or (ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda_w) between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agendamento_w) and ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agendamento_w + qt_dias)))
	and		(nvl(c.cd_equipamento,nvl(cd_equipamento_p,0)) = nvl(cd_equipamento_p,0));

begin

obter_param_usuario(871, 20, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ds_email_remetente_w);

select 	max(cd_pessoa_fisica),
		max(nr_atendimento),
		max(nvl(ds_seq_regra_email,'-1')),
		max(dt_agendamento),
		max(ie_carater_cirurgia),
		max(dt_agenda)
into	cd_pessoa_fisica_w,
		nr_atendimento_w,
		ds_seq_regra_email_w,
		dt_agendamento_w,
		ie_carater_cirurgia_w,
		dt_agenda_w
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p;	


if 	(nr_atendimento_w is null) then
	select	nvl(max(a.nr_atendimento),0)
	into	nr_atendimento_w
	from	atendimento_paciente a,
		atend_categoria_convenio b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
end if;	

select 	decode(count(*),0,'N','S')
into	ie_paciente_internado_w
from	atendimento_paciente
where	dt_alta is null
and		ie_tipo_atendimento = 1
and 	nr_atendimento = nr_atendimento_w;

select	nvl(max(ie_tipo_convenio),0),
	nvl(max(ie_clinica),0),
	max(dt_entrada)
into	ie_tipo_convenio_w,
	ie_clinica_w,
	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

if	(ie_tipo_convenio_w = 0) then
	select	max(Obter_Tipo_Convenio(cd_convenio))
	into	ie_tipo_convenio_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
end if;	

ie_datas_iguais_w	:= 'N';
if	(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agendamento_w) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrada_w)) then
	ie_datas_iguais_w := 'S';
end if;

select	max(ds_email)
into	ds_email_destino_w
from	equipamento e,
		terceiro t
where	e.cd_equipamento = cd_equipamento_p
and		t.nr_Sequencia = e.nr_seq_terceiro;


select	max(ds_equipamento)
into	ds_equipamento_w
from	equipamento e
where	e.cd_equipamento = cd_equipamento_p;

if (nvl(ds_email_destino_w, 'XPTO') <> 'XPTO' 
	and (ie_momento_p = 'AE') 
	and nvl(ds_email_remetente_w, 'XPTO') <> 'XPTO') then 
	begin
	open c01;
		loop
		fetch c01 into
			nr_seq_regra_w,
			ie_enviar_sempre_w;
		exit when c01%notfound;	
			begin
			select	obter_conteudo_email_regra(nr_seq_agenda_p,nr_seq_regra_w,'T',nr_atendimento_w),
					obter_conteudo_email_regra(nr_seq_agenda_p,nr_seq_regra_w,'C',nr_atendimento_w)
			into	ds_titulo_w,
					ds_mensagem_w
			from	dual;

			ds_titulo_w		:= replace_macro(ds_titulo_w,'@equipamento_fornecedor',ds_equipamento_w);
			ds_mensagem_w	:= replace_macro(ds_mensagem_w,'@equipamento_fornecedor',ds_equipamento_w);
			
			enviar_email(ds_titulo_w,ds_mensagem_w,ds_email_remetente_w,ds_email_destino_w,nm_usuario_p,'M');
			
			if	(ie_enviar_sempre_w = 'N') then
				if	(ds_lista_regras_w is null) then
					ds_lista_regras_w := nr_seq_regra_w;
				else
					ds_lista_regras_w := ds_lista_regras_w || ',' || nr_seq_regra_w;
				end if;	
			end if;	
			end;
		end loop;
	close c01;
	end;
end if;
	
if	(ds_lista_regras_w is not null) then
	begin
	update	agenda_paciente
	set	ds_seq_regra_email 	= ds_seq_regra_email_w || ',' || ds_lista_regras_w,
		dt_envio_email		= sysdate	
	where	nr_sequencia 		= nr_seq_agenda_p;
	commit;
	end;
end if;	
	
end enviar_email_regra_equipamento;
/