create or replace
procedure pls_consistir_dt_receb_fatura(dt_recebimento_p	date,
					ie_consistencia_p	out	varchar2) is 

dt_recebimento_w	date;
dt_referencia_w		date;

begin
dt_recebimento_w  := trunc(dt_recebimento_p,'dd');
dt_referencia_w	  := trunc(sysdate,'dd');
ie_consistencia_p := 'N';

if	(dt_recebimento_w > dt_referencia_w) then
	ie_consistencia_p := 'S';
end if;

end pls_consistir_dt_receb_fatura;
/