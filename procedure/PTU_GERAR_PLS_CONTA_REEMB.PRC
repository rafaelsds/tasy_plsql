create or replace
procedure ptu_gerar_pls_conta_reemb(	nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
					nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type default null,
					nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
	
nr_seq_conta_w			pls_conta.nr_sequencia%type;
cd_cooperativa_w		ptu_fatura.cd_unimed_destino%type;
cd_usuario_plano_imp_w		pls_conta.cd_usuario_plano_imp%type;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type := '4'; -- SP/SADT
ie_tipo_guia_tiss_w		pls_conta.ie_tipo_guia%type;
vl_material_imp_w		pls_conta_mat.vl_material_imp%type;
vl_procedimento_imp_w		pls_conta_proc.vl_procedimento_imp%type;
nr_versao_transacao_w		ptu_fatura.nr_versao_transacao%type;
ie_carater_atendimento_w	pls_conta.ie_carater_internacao%type;

cursor C01 	(	nr_seq_fatura_pc	ptu_fatura.nr_sequencia%type,
			nr_seq_segurado_pc	pls_segurado.nr_sequencia%type) is
	select	nr_sequencia nr_seq_nota_cobr_rrs, --conta
		nr_lote, --conta
		nr_nota, --conta
		pls_obter_segurado_carteira(lpad(to_char(cd_unimed),4,'0')||id_benef,cd_estabelecimento_p) nr_seq_segurado, --conta
		cd_unimed, --conta
		id_benef, --conta
		nm_beneficiario, --conta
		dt_nasc, --conta
		tp_sexo, --conta
		id_rn, --conta
		dt_reembolso, -- conta
		tp_carater_atend, --conta
		tp_pessoa, --participante
		nr_cnpj_cpf, --participante
		nm_prestador, --participante
		null nr_autoriz, -- conta/servico
		nr_seq_prest_inter -- conta
	from	ptu_nota_cobranca_rrs
	where	nr_seq_fatura	= nr_seq_fatura_pc
	and	pls_obter_segurado_carteira(lpad(to_char(cd_unimed),4,'0')||id_benef,cd_estabelecimento_p) = nr_seq_segurado_pc;

begin

select	cd_unimed_destino,
	nr_versao_transacao
into	cd_cooperativa_w,
	nr_versao_transacao_w
from	ptu_fatura
where	nr_sequencia = nr_seq_fatura_p;

for r_C01_w in C01 ( nr_seq_fatura_p, nr_seq_segurado_p ) loop

	-- Se o PTU for da vers�o igual ou menor de 22, o tratamento ser� o antigo
	if 	(nr_versao_transacao_w <= 22) then
		select 	decode(r_C01_w.tp_carater_atend,1,'U','E') 
		into 	ie_carater_atendimento_w 
		from 	dual;
		
	--Se o PTU for da igual ou maior de 23, dever� ser realizado um novo tratamento para o campo ie_tipo_consulta e tratar o ie_carater_atendimento com seus respectivos valores novos.
	elsif	(nr_versao_transacao_w >= 23) then
		select 	decode(r_C01_w.tp_carater_atend,1,'E',2,'U') 
		into 	ie_carater_atendimento_w 
		from 	dual;
	end if;

	if	(cd_cooperativa_w is not null) then
		cd_usuario_plano_imp_w	:= lpad(cd_cooperativa_w,4,'0') || r_C01_w.id_benef;
	end if;
	
	insert into pls_conta
		(nr_sequencia,			nm_usuario,			dt_atualizacao,
		nm_usuario_nrec,		dt_atualizacao_nrec,		nr_seq_protocolo,
		cd_estabelecimento,		cd_guia,			nr_seq_segurado,
		cd_usuario_plano_imp,		ie_status,			ie_tipo_guia,
		ie_recem_nascido,		ie_carater_internacao,		cd_senha,
		cd_senha_externa,		nm_segurado_imp,		dt_atendimento_referencia,
		nr_seq_nota_cobr_rrs,		nr_seq_fatura,			nr_seq_prest_inter,
		dt_emissao,			dt_documento)
	values	(pls_conta_seq.nextval,		nm_usuario_p,			sysdate,
		nm_usuario_p,			sysdate,			nr_seq_protocolo_p,
		cd_estabelecimento_p,		r_C01_w.nr_nota,		r_C01_w.nr_seq_segurado,
		cd_usuario_plano_imp_w,		'U',				ie_tipo_guia_w,
		r_C01_w.id_rn,			ie_carater_atendimento_w,	r_C01_w.nr_autoriz,
		r_C01_w.nr_autoriz,		r_C01_w.nm_beneficiario,	r_C01_w.dt_reembolso,
		r_C01_w.nr_seq_nota_cobr_rrs,	nr_seq_fatura_p,		r_C01_w.nr_seq_prest_inter,
		r_C01_w.dt_reembolso,		r_C01_w.dt_reembolso		) returning nr_sequencia into nr_seq_conta_w;
	
	ptu_gerar_pls_proced_reemb( nr_seq_conta_w, r_C01_w.nr_seq_nota_cobr_rrs, nm_usuario_p, cd_estabelecimento_p);	

	select	sum(vl_procedimento_imp)
	into	vl_procedimento_imp_w
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_w;
	
	select	sum(vl_material_imp)
	into	vl_material_imp_w
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_w;

	-- Rotina para verificar a regra de qual o tipo da guia que sera a conta
	pls_obter_guia_tiss( nr_seq_conta_w, ie_tipo_guia_tiss_w);
	
	update	pls_conta
	set	vl_materiais_imp	= vl_material_imp_w,
		vl_procedimentos_imp	= vl_procedimento_imp_w,
		vl_total_imp		= nvl(vl_procedimento_imp_w,0) + nvl(vl_material_imp_w,0),
		ie_tipo_guia		= nvl(ie_tipo_guia_tiss_w,ie_tipo_guia)
	where	nr_sequencia		= nr_seq_conta_w;
	
	update	pls_protocolo_conta
	set	nr_seq_segurado	= r_C01_w.nr_seq_segurado
	where	nr_sequencia	= nr_seq_protocolo_p;
	
	pls_atualiza_valor_conta( nr_seq_conta_w, nm_usuario_p);
end loop;

end ptu_gerar_pls_conta_reemb;
/