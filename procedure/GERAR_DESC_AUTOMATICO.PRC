create or replace
procedure Gerar_Desc_Automatico(
			nr_interno_conta_p	number,
			ie_tipo_desconto_p	number,
			nm_usuario_p	Varchar2,
			ds_erro_p		out Varchar2) is 
			
qt_regra_desc_item_w	number(10);
nr_seq_desc_w		number(10);
cd_convenio_w		number(5);
vl_conta_w		number(15,2);
ds_erro_w		varchar2(2000)	:= '';
ie_gerar_desc_w		varchar2(1);

begin

select	max(cd_convenio_parametro)
into	cd_convenio_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

/*select	max(obter_valor_conta(nr_interno_conta,0))
into	vl_conta_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;*/

begin
vl_conta_w	:= obter_valor_conta(nr_interno_conta_p, 0);
exception
when others then
	vl_conta_w	:= 0;
end;
	
select	count(1)
into	qt_regra_desc_item_w
from	conv_regra_desc a,
	conv_regra_desc_item b
where	a.nr_sequencia	= b.nr_seq_regra_desc
and	cd_convenio	= cd_convenio_w
and	trunc(sysdate,'dd') between nvl(trunc(dt_inicio_vigencia,'dd'), trunc(sysdate,'dd')) and nvl(dt_final_vigencia, sysdate)
and	vl_conta_w between vl_inicial and vl_final
and	rownum	= 1;

if	(qt_regra_desc_item_w > 0) then	
	select	conta_paciente_desconto_seq.nextval
	into	nr_seq_desc_w
	from	dual;
	
	ie_gerar_desc_w	:= 'S';
	
	begin
	insert into conta_paciente_desconto
		(nr_sequencia,
		nr_interno_conta,
		ie_tipo_desconto,
		dt_atualizacao,
		nm_usuario,
		vl_conta,
		pr_desconto,
		vl_desconto,
		vl_liquido,
		dt_desconto,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_pacote,
		ie_valor_inf)
	values (nr_seq_desc_w,
		nr_interno_conta_p, 
		ie_tipo_desconto_p,
		sysdate,
		nm_usuario_p,
		vl_conta_w,
		0,
		0,
		vl_conta_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		'A',
		'A');
	exception
		when others then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280692);
		ie_gerar_desc_w	:= 'N';
	end;
	
	
	if	(ie_gerar_desc_w = 'S') then
		commit;
		
		Gerar_ConPaci_Desc_Item(nr_seq_desc_w,nm_usuario_p);
	
		Gerar_Conv_Regra_Desc(nr_seq_desc_w,nm_usuario_p);
		
		--Calcular_ConPaci_Desc_Item(nr_seq_desc_w,nm_usuario_p);
	
		Calcular_Conpaci_Desconto(nr_seq_desc_w,'I',nm_usuario_p);
	
	else
		rollback;
	end if;
	
end if;

ds_erro_p	:= ds_erro_w;

commit;

end Gerar_Desc_Automatico;
/