create or replace
procedure sib_excluir_exclusao_sib
			(	nr_seq_lote_sib_p	Number,
				nr_seq_interf_sib_p	Number,
				nm_usuario_p		Varchar2) is 

nr_seq_exclusao_del_w		number(10);
nr_seq_segurado_w		number(10);

begin

select	a.nr_sequencia,
	a.nr_seq_segurado
into	nr_seq_exclusao_del_w,
	nr_seq_segurado_w
from	pls_interf_sib	a
where	a.nr_sequencia = nr_seq_interf_sib_p
and	a.nr_seq_lote_sib = nr_seq_lote_sib_p;

/* insere os dados da altera��o na tabela sib_log_exclusao que rigistra as altera��es excluidas */
insert	into sib_log_exclusao (	nr_sequencia, nr_seq_lote, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_segurado, nr_seq_motivo_exclusao, dt_registro,
				nm_usuario_registro)
			values(	sib_log_exclusao_seq.nextval, nr_seq_lote_sib_p, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_segurado_w, null, sysdate,
				nm_usuario_p);

delete	from pls_interf_sib
where	nr_sequencia	= nr_seq_interf_sib_p
and	nr_seq_lote_sib	= nr_seq_lote_sib_p
and	ie_tipo_reg	= 7;

sib_ordenar_segurados_envio(nr_seq_lote_sib_p,nm_usuario_p);

commit;

end sib_excluir_exclusao_sib;
/