create or replace
procedure pls_gerar_senha_benef_web( 	nr_seq_usuario_p	Number,
					ds_senha_p		Varchar2,
					ds_senha_hash_p 	Varchar2) is 

ds_salt_w		pls_segurado_web.ds_tec%type;	
ds_senha_sha256_w	pls_segurado_web.ds_senha%type;			
					
begin
	
if	(nr_seq_usuario_p is not null) then
	
	select 	replace(replace(dbms_random.string('P', 15), chr(39), ''), ';', '') 
	into	ds_salt_w
	from 	dual;
	
	ds_senha_sha256_w := obter_sha2(upper(ds_senha_p) || ds_salt_w, 256);

	update	pls_segurado_web
	set	ds_tec		= ds_salt_w,
		ds_senha     	= ds_senha_sha256_w,
		ds_hash	      	= ds_senha_hash_p,
		ie_origem_login = 'AB'	
	where	nr_sequencia = nr_seq_usuario_p;
end if;

commit;

end pls_gerar_senha_benef_web;
/
