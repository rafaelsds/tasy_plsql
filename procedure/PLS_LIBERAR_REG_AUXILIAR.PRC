create or replace 
procedure pls_liberar_reg_auxiliar(nr_seq_registro_p	number,
			           ie_liberar_p		varchar2) is

nm_usuario_w		usuario.nm_usuario%type;
				   
begin

nm_usuario_w	:= wheb_usuario_pck.get_nm_usuario;


if	(nvl(nr_seq_registro_p,0) <> 0) then

	if	(ie_liberar_p = 'S') then
		update	ctb_livro_auxiliar
		set	dt_liberacao	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_w
		where	nr_sequencia	= nr_seq_registro_p;
	end if;

	if	(ie_liberar_p = 'N') then
		update	ctb_livro_auxiliar
		set	dt_liberacao	= null,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_w
		where	nr_sequencia	= nr_seq_registro_p;
	end if;

end if;

commit;

end pls_liberar_reg_auxiliar;
/
