create or replace
procedure hem_inativar_procedimento(	nr_seq_proc_p		Number,
					ds_motivo_inativ_p	Varchar2,
					nm_usuario_p		Varchar2) is 

begin
if	(nvl(nr_seq_proc_p,0) > 0) then
	
	update	hem_proc
	set	dt_inativacao		= sysdate,
		nm_usuario_inativacao	= nm_usuario_p,
		ds_justificativa_inat	= ds_motivo_inativ_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_proc_p;
end if;
commit;

end hem_inativar_procedimento;
/
