CREATE OR REPLACE
PROCEDURE estornar_repasse_recebido(
				nr_seq_protocolo_p		number,
				nr_interno_conta_p		number,
				nm_usuario_p			Varchar2) is

nr_seq_proc_repasse_w	number(10);	
nr_seq_mat_repasse_w	number(10);
				
				
cursor c01 is
select 	a.nr_sequencia
from	procedimento_repasse a,
	procedimento_paciente b
where 	nvl(a.ie_desc_caixa,'N')	= 'N'
and	a.nr_seq_procedimento	= b.nr_sequencia
and	a.ie_status in ('L','R','S')
and	nvl(a.ie_estorno,'N') = 'N'
and	b.nr_interno_conta  	= nr_interno_conta_p;

cursor c02 is
select	a.nr_sequencia
from	material_repasse a,
	material_atend_paciente b
where 	nvl(a.ie_desc_caixa,'N')	= 'N'
and	a.nr_seq_material = b.nr_sequencia
and	a.ie_status in ('L','R','S')
and	nvl(a.ie_estorno,'N') = 'N'
and	b.nr_interno_conta  = nr_interno_conta_p;				
				
begin
open c01;
	loop
	fetch c01 into
		nr_seq_proc_repasse_w;
	exit when c01%notfound;
	
	if	(nvl(nr_seq_proc_repasse_w, 0) > 0) then
		insert into procedimento_repasse(
			nr_sequencia,
			nr_seq_procedimento,
			vl_repasse,
			dt_atualizacao,
			nm_usuario,
			nr_seq_terceiro,
			nr_lote_contabil,
			nr_repasse_terceiro,
			cd_conta_contabil,
			nr_seq_trans_fin,
			vl_liberado,
			nr_seq_item_retorno,
			ie_status,
			nr_seq_origem,
			cd_regra,
			DT_CONTABIL_TITULO,
			DT_CONTABIL,
			cd_medico,
			nr_seq_partic,
			nr_seq_criterio,
			NR_SEQ_TRANS_FIN_REP_MAIOR,
			dt_liberacao,
			nr_seq_motivo_des,
			ds_observacao,
			vl_original_repasse,
			ie_estorno)
		select	Procedimento_repasse_seq.nextval,
			nr_seq_procedimento,
			vl_repasse  * -1,
			sysdate,
			nm_usuario_p,
			nr_seq_terceiro,
			0,
			null,
			cd_conta_contabil,
			nr_seq_trans_fin,
			vl_liberado * -1,
			nr_seq_item_retorno,
			'E',
			nr_sequencia,
			cd_regra,
			to_date('01/01/2999','dd/mm/yyyy'),
			to_date('01/01/2999','dd/mm/yyyy'),
			cd_medico,
			nr_seq_partic,
			nr_seq_criterio,
			NR_SEQ_TRANS_FIN_REP_MAIOR,
			sysdate,
			nr_seq_motivo_des,
			Wheb_mensagem_pck.get_Texto( 305585 ), /*'Este repasse foi estornado pois o recebimento que o liberou foi cancelado.',*/			
			vl_original_repasse,
			'S'
		from	Procedimento_repasse
		where	nr_sequencia	= nr_seq_proc_repasse_w;
		
		update	procedimento_repasse
		set	ie_estorno	= 'S'
		where	nr_sequencia	= nr_seq_proc_repasse_w;
	end if;
end loop;
close c01;
	
open c02;
loop
fetch c02 into
	nr_seq_mat_repasse_w;
exit when c02%notfound;	

	if	(nvl(nr_seq_mat_repasse_w, 0) > 0) then
		insert into Material_repasse(
			nr_sequencia,
			nr_seq_Material,
			vl_repasse,
			dt_atualizacao,
			nm_usuario,
			nr_seq_terceiro,
			nr_lote_contabil,
			nr_repasse_terceiro,
			cd_conta_contabil,
			nr_seq_trans_fin,
			vl_liberado,
			nr_seq_item_retorno,
			ie_status,
			nr_seq_origem,
			cd_regra,
			cd_medico,
			NR_SEQ_TRANS_FIN_REP_MAIOR,
			dt_liberacao,
			nr_seq_motivo_des,
			ds_observacao,
			vl_original_repasse,
			nr_seq_criterio,
			ie_estorno)
		select	Material_repasse_seq.nextval,
			nr_seq_material,
			vl_repasse  * -1,
			sysdate,
			nm_usuario_p,
			nr_seq_terceiro,
			0,
			null,
			cd_conta_contabil,
			nr_seq_trans_fin,
			vl_liberado * -1,
			nr_seq_item_retorno,
			'E',
			nr_sequencia,
			cd_regra,
			cd_medico,
			NR_SEQ_TRANS_FIN_REP_MAIOR,
			sysdate,
			nr_seq_motivo_des,
			Wheb_mensagem_pck.get_Texto( 305585 ), /*'Este repasse foi estornado pois o recebimento que o liberou foi cancelado.',*/
			vl_original_repasse,
			nr_seq_criterio,
			'S'
		from	material_repasse
		where	nr_sequencia	= nr_seq_mat_repasse_w;
		
		update	material_repasse
		set	ie_estorno	= 'S'
		where	nr_sequencia	= nr_seq_mat_repasse_w;
	end if;
end loop;
close c02;

delete	procedimento_repasse a
where 	nvl(a.ie_desc_caixa,'N')	= 'N'
and	a.ie_status = 'A'
and	nvl(a.ie_estorno,'N') = 'N'
and	exists(	select	1
		from	procedimento_paciente b
		where	b.nr_interno_conta  	= nr_interno_conta_p
		and	b.nr_sequencia = a.nr_seq_procedimento);

delete	material_repasse a
where 	nvl(a.ie_desc_caixa,'N')	= 'N'
and	a.ie_status = 'A'
and	nvl(a.ie_estorno,'N') = 'N'
and	exists(	select	1
		from	material_atend_paciente b
		where	b.nr_interno_conta  = nr_interno_conta_p
		and	b.nr_sequencia = a.nr_seq_material);

end estornar_repasse_recebido;
/