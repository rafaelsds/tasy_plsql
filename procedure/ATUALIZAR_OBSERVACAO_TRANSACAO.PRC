create or replace
procedure atualizar_observacao_transacao (
			nr_seq_caixa_rec_p	number,
			ds_observacao_p		varchar2) is 

nr_sequencia_w		number(10);

Cursor C01 is
select	nr_sequencia
from	movto_trans_financ
where	nr_seq_caixa_rec = nr_seq_caixa_rec_p;

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	update	movto_trans_financ
	set	ds_observacao = substr(ds_observacao ||chr(13)||chr(10)|| ds_observacao_p,1,4000)
	where	nr_sequencia = nr_sequencia_w;	
	
	end;
end loop;
close C01;

commit;

end atualizar_observacao_transacao;
/