create or replace
procedure UPDATE_AUTHORIZED_PROC_RET_AUT
				(NR_SEQ_AUT_P 	NUMBER,
				CD_PROCEDURE_P 	NUMBER,
				QT_AUTHORIZED_P VARCHAR2,
				VL_AUTHORIZED_P VARCHAR2,				
				NM_USUARIO_P 	VARCHAR2) is

QT_REQUESTED_W		PROCEDIMENTO_AUTORIZADO.QT_SOLICITADA%type;
NR_SEQUENCE_W		PROCEDIMENTO_AUTORIZADO.NR_SEQUENCIA%type;

cursor	c01 is
select	z.nr_sequencia,
		z.qt_solicitada
from	procedimento_autorizado z,
		autorizacao_convenio x
where	x.nr_sequencia 		= z.nr_sequencia_autor
and	z.cd_procedimento_tuss	= cd_procedure_p
and x.nr_sequencia 			= nr_seq_aut_p;

begin
open c01;
loop
fetch c01 into
	nr_sequence_w,
	qt_requested_w;
exit when c01%notfound;
	if (nr_seq_aut_p is not null) then
		update 	procedimento_autorizado
		set		qt_autorizada		= qt_authorized_p,
				vl_autorizado		= vl_authorized_p,
				ie_enviado_tiss		= 'S',
				nm_usuario			= nm_usuario_p,
				dt_atualizacao		= sysdate
		where 	nr_sequencia = nr_sequence_w;
	end if;
end	loop;
close c01;
commit;

end UPDATE_AUTHORIZED_PROC_RET_AUT;
/
