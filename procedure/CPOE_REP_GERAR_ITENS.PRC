create or replace
procedure CPOE_REP_Gerar_itens(	nr_prescricao_p			prescr_medica.nr_prescricao%type,
								nr_atendimento_p		atendimento_paciente.nr_atendimento%type,
								dt_inicio_prescr_p		date,
								dt_validade_prescr_p	date,
								dt_liberacao_p			date,
								nm_usuario_p			usuario.nm_usuario%type,
								cd_pessoa_fisica_p		cpoe_dieta.cd_pessoa_fisica%type,
								ie_tipo_pessoa_p		varchar2 default 'N',
								ie_oncologia_p 			varchar2 default 'N') is 
	
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;	
cd_setor_atendimento_w	prescr_medica.cd_setor_atendimento%type;
nr_seq_agenda_w			prescr_medica.nr_seq_agenda%type;
cd_funcao_w				funcao.cd_funcao%type;
dt_liberacao_w			date := dt_liberacao_p;
begin

select	max(cd_funcao_origem),
		max(cd_setor_atendimento)
into	cd_funcao_w,
		cd_setor_atendimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

if	(nvl(ie_oncologia_p,'N') = 'S') then
	dt_liberacao_w := null;
end if;

select	max(nr_seq_agenda)
into	nr_seq_agenda_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

if (nr_atendimento_p = 0) then
	nr_atendimento_w := null;
else
	nr_atendimento_w := nr_atendimento_p;
end if;

gravar_processo_longo(obter_desc_expressao(770069),'CPOE_REP_GERAR_ITENS',1);
cpoe_rep_gerar_recomendacao(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_gasoterapia(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_hemodialise(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_hemoterapia(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_npt_adulta(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_funcao_w,cd_setor_atendimento_w);

cpoe_rep_gerar_suplemento(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_leite_deriv(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_jejum(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w,cd_setor_atendimento_w);	
cpoe_rep_gerar_dieta_oral(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_dieta_enteral(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w,cd_setor_atendimento_w);
cpoe_rep_gerar_procedimento(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w,cd_setor_atendimento_w, null, null, nr_seq_agenda_w);
cpoe_rep_generate_material(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w, ie_tipo_pessoa_p,cd_setor_atendimento_w, nr_seq_agenda_w);
cpoe_rep_gerar_medicamento(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w, ie_tipo_pessoa_p,cd_setor_atendimento_w, nr_seq_agenda_w);
cpoe_rep_gerar_solucao(nr_prescricao_p, nr_atendimento_w, dt_inicio_prescr_p, dt_validade_prescr_p, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_p, cd_funcao_w, ie_tipo_pessoa_p,cd_setor_atendimento_w, nr_seq_agenda_w);

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end CPOE_REP_Gerar_itens;
/
