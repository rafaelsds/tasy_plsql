create or replace
procedure tre_insere_falta_curso(nr_seq_inscrito_p	number,
				dt_registro_p		date,	
				ie_status_falta_p	varchar2,
				ds_motivo_p		varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number) is 

qt_possui_cad_w		number(10);				
nr_seq_pac_dia_w	number(10);				
begin
select	count(*)
into	qt_possui_cad_w
from	tre_paciente_dia
where	nr_seq_inscrito = nr_seq_inscrito_p	
and	trunc(dt_registro) = trunc(dt_registro_p);

if	(qt_possui_cad_w = 0) then
	tre_insere_pac_dia(nr_seq_inscrito_p,dt_registro_p,nm_usuario_p,nr_seq_pac_dia_w);
	update	tre_paciente_dia
	set	ie_status_paciente = ie_status_falta_p,
		ds_motivo_status   = ds_motivo_p
	where	nr_Sequencia = nr_seq_pac_dia_w;
else	
	select	max(nr_Sequencia)
	into	nr_seq_pac_dia_w
	from	tre_paciente_dia
	where	nr_seq_inscrito = nr_seq_inscrito_p	
	and	trunc(dt_registro) = trunc(dt_registro_p);
	
	update	tre_paciente_dia
	set	ie_status_paciente = ie_status_falta_p,
		ds_motivo_status   = ds_motivo_p
	where	nr_Sequencia = nr_seq_pac_dia_w;
end if;

if	(ie_status_falta_p = 'I') then
	tre_aplica_regra_falta(dt_registro_p,nr_seq_inscrito_p,nm_usuario_p,cd_estabelecimento_p);
end if;	

commit;

end tre_insere_falta_curso;
/
