create or replace
procedure fis_gerar_reg_1900_efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is

aliquota_pis_w		number(7,4);
aliquota_cofins_w		number(7,4);
aliquota_iss_w		number(7,4);
cd_tributo_pis_w		number(3);
cd_tributo_cofins_w		number(3);
cd_tributo_iss_w		number(3);
nr_seq_regra_efd_w	number(10);
nr_linha_w		number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
sep_w			varchar2(1) := ds_separador_p;
sg_estado_w		compl_pessoa_fisica.sg_estado%type;
cd_cgc_w		varchar2(14);

cursor c01 is
	select	'1900' tp_registro,
		cd_cgc_w cd_cgc,
		'55' cd_modelo_nf,
		'U' cd_serie_nf,
		'' cd_subserie_nf,
		'00' cd_situacao,
		sum(n.vl_mercadoria) vl_rceita,
		count(*) qt_documentos,
		'01' cd_cst_pis,
		'01' cd_cst_cofins,
		decode(obter_dados_pf_pj(n.cd_pessoa_fisica,n.cd_cgc,'UF'),sg_estado_w,'5933','6933') cd_cfop,
		'' ds_info_compl,
		'' cd_conta_contabil
	from	nota_fiscal n,
		operacao_nota o,
		titulo_receber t
	where	n.nr_sequencia = t.nr_seq_nf_saida
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	trunc(t.dt_liquidacao,'mm') between trunc(dt_inicio_p,'mm') and trunc(dt_fim_p,'mm')
	and	o.ie_operacao_fiscal = 'S'
	and 	o.ie_servico = 'S'
	and	n.dt_atualizacao_estoque is not null
	and	n.vl_total_nota > 0
	and	n.cd_estabelecimento	= cd_estabelecimento_p
	group by decode(obter_dados_pf_pj(n.cd_pessoa_fisica,n.cd_cgc,'UF'),sg_estado_w,'5933','6933');

vet01	c01%rowtype;

begin

select	nr_seq_regra_efd
into	nr_seq_regra_efd_w
from	fis_efd_controle
where	nr_sequencia = nr_seq_controle_p;

select	cd_tributo_pis,
		cd_tributo_cofins,
		cd_tributo_iss
into	cd_tributo_pis_w,
		cd_tributo_cofins_w,
		cd_tributo_iss_w
from	fis_regra_efd
where	nr_sequencia = nr_seq_regra_efd_w;

aliquota_pis_w 	:= obter_pr_imposto(cd_tributo_pis_w);
aliquota_cofins_w 	:= obter_pr_imposto(cd_tributo_cofins_w);
aliquota_iss_w 	:= obter_pr_imposto(cd_tributo_iss_w);

select	cd_cgc
into	cd_cgc_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

select	obter_dados_pf_pj('',cd_cgc_w,'UF')
into	sg_estado_w
from	dual;

open c01;
loop
fetch c01 into
	vet01;
exit when c01%notfound;
	begin

	ds_linha_w	:= substr(	sep_w || vet01.tp_registro	 	||
				sep_w || vet01.cd_cgc 		||
				sep_w || vet01.cd_modelo_nf 		||
				sep_w || vet01.cd_serie_nf	 	||
				sep_w || vet01.cd_subserie_nf		||
				sep_w || vet01.cd_situacao		||
				sep_w || vet01.vl_rceita		||
				sep_w || vet01.qt_documentos	||
				sep_w || vet01.cd_cst_pis		||
				sep_w || vet01.cd_cst_cofins		||
				sep_w || vet01.cd_cfop		||
				sep_w || vet01.ds_info_compl		||
				sep_w || vet01.cd_conta_contabil	|| sep_w,1,8000);
		
		ds_arquivo_w := substr(ds_linha_w,1,4000);
		ds_arquivo_compl_w := substr(ds_linha_w,4001,4000);
		nr_seq_registro_w := nr_seq_registro_w + 1;
		nr_linha_w := nr_linha_w + 1;

		insert into fis_efd_arquivo(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_controle_efd,
			nr_linha,
			cd_registro,
			ds_arquivo,
			ds_arquivo_compl)
		values(	nr_seq_registro_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_controle_p,
			nr_linha_w,
			vet01.tp_registro,
			ds_arquivo_w,
			ds_arquivo_compl_w);
		end;
	end loop;
	close c01;
commit;

qt_linha_p := nr_linha_w;
nr_sequencia_p := nr_seq_registro_w;

end fis_gerar_reg_1900_efd;
/