create or replace
procedure mprev_alterar_status_capt(nr_seq_captacao_p number,ie_status_p varchar2,nm_usuario_p varchar2) is 
          
vl_parametro_w		varchar2(1);
ie_status_w			varchar2(1);
cd_pessoa_fisica_w  varchar2(10);
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
finalidade:  alterar o status da capta��o para "triagem".
-------------------------------------------------------------------------------------------------------------------
locais de chamada direta: 
[ ]  objetos do dicion�rio [x] tasy (delphi/java) [  ] portal [  ]  relat�rios [ ] outros:
 ------------------------------------------------------------------------------------------------------------------
pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 	

begin

	if	(nr_seq_captacao_p is not null) and	(ie_status_p is not null) then
	
		select 	max(ie_status),
			max(cd_pessoa_fisica)
		into	ie_status_w,
			cd_pessoa_fisica_w
		from  	mprev_captacao
		where	nr_sequencia = nr_seq_captacao_p; 
		
		obter_param_usuario(10155,10,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,vl_parametro_w);
		
		if(vl_parametro_w  = 'S') and (ie_status_w ='R') then
		
			update	mprev_grupo_cont_pessoa a
			set   	a.dt_exclusao 		= sysdate
			where 	cd_pessoa_fisica_w	= a.cd_pessoa_fisica;	
			
		end if;	  
		
		update	mprev_captacao
		set	ie_status 	  	= ie_status_p,
			nm_usuario 	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where	nr_sequencia  	= nr_seq_captacao_p;
		
		commit;
		
	end if;
	
end mprev_alterar_status_capt;
/