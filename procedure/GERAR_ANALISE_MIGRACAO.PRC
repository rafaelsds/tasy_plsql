create or replace
procedure gerar_analise_migracao (
		nm_usuario_p	varchar2) is

begin
if	(nm_usuario_p is not null) then
	begin
	gerar_analise_migr_grupo_des(nm_usuario_p);
	gerar_analise_migr_area_proj(nm_usuario_p);
	end;
end if;
commit;
end gerar_analise_migracao;
/