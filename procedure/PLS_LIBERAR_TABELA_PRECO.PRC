create or replace
procedure pls_liberar_tabela_preco
			(	nr_seq_tabela_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				ds_erro_p	out	varchar2) is

-- Campos do cursor 1
nr_seq_plano_preco_w		pls_plano_preco.nr_sequencia%type;
qt_idade_inicial_w		pls_plano_preco.qt_idade_inicial%type;
qt_idade_final_w		pls_plano_preco.qt_idade_final%type;
ie_grau_titularidade_w		pls_plano_preco.ie_grau_titularidade%type;
qt_vidas_inicial_w		pls_plano_preco.qt_vidas_inicial%type;
qt_vidas_final_w		pls_plano_preco.qt_vidas_final%type;
vl_preco_inicial_w		pls_plano_preco.vl_preco_inicial%type;
vl_preco_atual_w		pls_plano_preco.vl_preco_atual%type;
vl_preco_nao_subsid_atual_w	pls_plano_preco.vl_preco_nao_subsid_atual%type;
tx_acrescimo_w			pls_plano_preco.tx_acrescimo%type;
tx_acrescimo_erro_w		number(15,4);
vl_preco_ant_w			number(15,2);
qt_idade_inicio_w		number(5);
nr_seq_preco_ant_w		number(10);
nr_seq_faixa_etaria_w		pls_faixa_etaria.nr_sequencia%type;
qt_consiste_fx_w		number(3);
qt_idade_inicial_ww		pls_faixa_etaria_item.qt_idade_inicial%type;
qt_idade_final_ww		pls_faixa_etaria_item.qt_idade_final%type;
ds_erro_w			varchar2(16000) := null;
ds_erro_ww			varchar2(16000) := null;
nr_seq_tabela_w			number(10);
ie_tabela_principal_w		varchar(2);
qt_faixa_etaria_w		number(10);

cursor C01 is
	select	nr_sequencia,
		qt_idade_inicial,
		qt_idade_final,
		ie_grau_titularidade,
		qt_vidas_inicial,
		qt_vidas_final,
		vl_preco_inicial,
		vl_preco_atual,
		vl_preco_nao_subsid_atual,
		tx_acrescimo
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_w
	order by qt_idade_inicial;

Cursor C02 is
	select	qt_idade_inicial,
		qt_idade_final
	from	pls_faixa_etaria_item
	where	nr_seq_faixa_etaria = nr_seq_faixa_etaria_w
	group by
		qt_idade_inicial,
		qt_idade_final;

Cursor C03 is
	select 	nr_sequencia,
		decode(nr_seq_principal,null,'N','S')
	from 	pls_tabela_preco
	where 	nr_sequencia 		= nr_seq_tabela_p
	or 	nr_seq_principal	= nr_seq_tabela_p;

begin

select	count(1)
into	qt_faixa_etaria_w
from	pls_plano_preco
where	nr_seq_tabela = nr_seq_tabela_p;

if	(qt_faixa_etaria_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(821778, null);
end if;

pls_consiste_faixa_tit_tabela(nr_seq_tabela_p);

open C03;
loop
fetch C03 into
	nr_seq_tabela_w,
	ie_tabela_principal_w;
exit when C03%notfound;
	begin
	if	(nvl(nr_seq_tabela_w,0) > 0) then
		begin
		select	max(nr_seq_faixa_etaria)
		into	nr_seq_faixa_etaria_w
		from	pls_tabela_preco
		where	nr_sequencia	= nr_seq_tabela_w;
		exception
		when others then
			nr_seq_faixa_etaria_w := null;
		end;
		
		if	(nr_seq_faixa_etaria_w is not null) then
			open C02;
			loop
			fetch C02 into
				qt_idade_inicial_ww,
				qt_idade_final_ww;
			exit when C02%notfound;
				begin
				
				select	count(1)
				into	qt_consiste_fx_w
				from	pls_plano_preco
				where	nr_seq_tabela = nr_seq_tabela_w
				and	qt_idade_inicial = qt_idade_inicial_ww
				and	qt_idade_final = qt_idade_final_ww;
				
				if	(qt_consiste_fx_w = 0) then
					--N�o foi cadastrada a faixa et�ria de #@QT_IDADE_INICIAL#@ a #@QT_IDADE_FINAL#@ anos.
					wheb_mensagem_pck.exibir_mensagem_abort(392860,'QT_IDADE_INICIAL='||qt_idade_inicial_ww||';'||'QT_IDADE_FINAL='||qt_idade_final_ww);
				end if;
				
				end;
			end loop;
			close C02;
		end if;
		
		open C01;
		loop
		fetch C01 into
			nr_seq_plano_preco_w,
			qt_idade_inicial_w,
			qt_idade_final_w,
			ie_grau_titularidade_w,
			qt_vidas_inicial_w,
			qt_vidas_final_w,
			vl_preco_inicial_w,
			vl_preco_atual_w,
			vl_preco_nao_subsid_atual_w,
			tx_acrescimo_w;
		exit when C01%notfound;
			begin
			select	nvl(min(qt_idade_inicial),0)
			into	qt_idade_inicio_w
			from	pls_plano_preco
			where	nr_seq_tabela	= nr_seq_tabela_w;
			
			if	(qt_idade_inicial_w <> qt_idade_inicio_w) then
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_preco_ant_w
				from	pls_plano_preco
				where	nr_seq_tabela	 = nr_seq_tabela_w
				and	qt_idade_inicial < qt_idade_inicial_w;
				
				if	(nr_seq_preco_ant_w > 0) then
					select	nvl(vl_preco_inicial,0)
					into	vl_preco_ant_w
					from	pls_plano_preco
					where	nr_sequencia = nr_seq_preco_ant_w;
					
					begin
					tx_acrescimo_w := (dividir_sem_round(vl_preco_inicial_w,vl_preco_ant_w) * 100) - 100;
					exception
					when others then
						tx_acrescimo_erro_w := (dividir_sem_round(vl_preco_inicial_w,vl_preco_ant_w) * 100) - 100;
						
						wheb_mensagem_pck.exibir_mensagem_abort(200503,'QT_IDADE='||qt_idade_inicial_w||';'||'TX_ACRESCIMO='||tx_acrescimo_erro_w);
						-- Mensagem: O valor do acr�scimo da faixa et�ria QT_IDADE chegou no limite: TX_ACRESCIMO
					end;
					
					if	(tx_acrescimo_w < 0)	then
						tx_acrescimo_w := 0;
					end if;	
					
					update	pls_plano_preco
					set	tx_acrescimo	= tx_acrescimo_w,
						nm_usuario	= nm_usuario_p,
						dt_atualizacao	= sysdate
					where	nr_sequencia	= nr_seq_plano_preco_w;
				end if;
			end if;
			
			if 	(ie_tabela_principal_w = 'N') then
				pls_consiste_tab_faixa_etaria(	nr_seq_tabela_p, nr_seq_plano_preco_w, qt_idade_inicial_w, qt_idade_final_w, ie_grau_titularidade_w,
								qt_vidas_inicial_w, qt_vidas_final_w, vl_preco_atual_w, vl_preco_nao_subsid_atual_w, tx_acrescimo_w , ds_erro_w);
			end if;
			
			if	(ds_erro_w is not null) then
				ds_erro_ww := ds_erro_ww || 'Faixa et�ria '||qt_idade_inicial_w||'-'||qt_idade_final_w||' anos (' || nr_seq_plano_preco_w || '):' || chr(13)
							 || ds_erro_w || chr(13);
			end if;
			end;
		end loop;
		close C01;
		
		pls_consistir_tabela_rn63(nr_seq_tabela_w, ds_erro_w);
		if	(ds_erro_w is not null) then
			ds_erro_ww	:= ds_erro_ww || ds_erro_w || chr(13);
		end if;
		
		if	(ds_erro_ww is not null) then
			rollback;
			
			if	(length(ds_erro_ww) > 252) then
				ds_erro_p := substr(ds_erro_ww, 1, 252) || '...';
			else
				ds_erro_p := ds_erro_ww;
			end if;
		else
			update	pls_tabela_preco
			set	dt_liberacao	= sysdate,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_tabela_w;
			
			commit;
		end if;
	end if;
	end;
end loop;
close C03;

end pls_liberar_tabela_preco;
/
