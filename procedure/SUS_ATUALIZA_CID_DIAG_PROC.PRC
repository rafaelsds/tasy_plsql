create or replace
procedure sus_atualiza_cid_diag_proc (		nr_interno_conta_p	in number,
					ie_subscreve_cid_p	in varchar2) as

nr_atendimento_w		number(10);
cd_medico_executor_w	varchar2(10);
cd_cid_w			varchar2(4);
nr_sequencia_w		number(10);

cursor c01 is
	select	nr_sequencia,
		nr_atendimento,
		cd_medico_executor
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_p
	and	(((cd_doenca_cid is null) and (ie_subscreve_cid_p = 'N')) or
		(ie_subscreve_cid_p = 'S'));

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_atendimento_w,
	cd_medico_executor_w;
exit when c01%notfound;
	begin

	sus_atualiza_cid_proc(nr_atendimento_w,cd_medico_executor_w,cd_cid_w);
	
	update	procedimento_paciente
	set	cd_doenca_cid 	= cd_cid_w
	where	nr_sequencia	= nr_sequencia_w;
	
	end;
end loop;
close c01;

commit;

end sus_atualiza_cid_diag_proc;
/
