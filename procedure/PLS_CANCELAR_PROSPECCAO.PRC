create or replace
procedure pls_cancelar_prospeccao
			(	nr_seq_cliente_p	Number,
				nr_seq_motivo_canc_p	Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

nr_seq_tipo_atividade_w		Number(10);
ds_status_w			Varchar2(255);	
				
begin

select	max(nr_sequencia)
into	nr_seq_tipo_atividade_w
from	pls_tipo_atividade
where	ie_status	= 'S';

select 	obter_valor_dominio(2664, ie_status)
into	ds_status_w
from	pls_comercial_cliente
where	nr_sequencia	= nr_seq_cliente_p;

update	pls_comercial_cliente
set	ie_status = 'N',
	nr_seq_motivo_cancelamento = nr_seq_motivo_canc_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_cliente_p;

insert into pls_comercial_historico(	nr_sequencia,
					nr_seq_cliente,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_titulo,
					ds_historico,
					dt_liberacao,
					nm_usuario_historico,
					ie_tipo_atividade,
					dt_historico)
				values(	pls_comercial_historico_seq.NextVal,
					nr_seq_cliente_p,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					'Altera��o do status',
					'Alterado o status de "' || ds_status_w || '" para "Prospec��o cancelada".',
					sysdate,
					nm_usuario_p,
					nr_seq_tipo_atividade_w,
					sysdate);

--pls_atualizar_status_cliente(nr_seq_cliente_p,nm_usuario_p);

commit;

end pls_cancelar_prospeccao;
/