create or replace
procedure pls_gerar_cartao_marciocunha
		(	nr_seq_lote_p        number,
			cd_estabelecimento_p number,
			nm_usuario_p         varchar2) is

nr_seq_segurado_w 		pls_segurado.nr_sequencia%type;
cd_usuario_plano_w     		varchar2(32);
nm_beneficiario_w      		varchar2(250);
tp_contratacao_w       		varchar2(50);
dt_nascimento_w        		varchar2(20);
ds_produto_w           		varchar2(50);
ds_acomodacao_w        		varchar2(50);
nm_empresa_w           		varchar2(50);
tp_contrato_w          		varchar2(10);
ds_trilha_1_w          		varchar2(1);
ds_trilha_2_w          		varchar2(20);
ds_trilha_3_w          		varchar2(25);
ds_protocolo_ans_w     		varchar2(100);
dt_contratacao_w       		varchar2(20);
nm_titular_w           		varchar2(255);
ds_estipulante_w       		varchar2(255);
ie_segmentacao_w       		varchar2(10);
ds_segmentacao_w       		varchar2(255);
dt_validade_carteira_w 		varchar2(10);
ds_observacao_w 		pls_segurado_carteira.ds_mensagem_carteira%type;
dt_cpt_w          		varchar2(10);
ds_regiao_w       		varchar2(4000);
nm_usuario_nrec_w 		varchar2(15);
nr_seq_w 			pls_carencia.nr_sequencia%type;
nr_seq_plano_w 			pls_plano.nr_sequencia%type;
ds_localizacao_w		pls_localizacao_benef.ds_localizacao%type;

Cursor C01 is
	select 	b.nr_sequencia
	from 	pls_contrato 		e,
		pls_segurado_carteira 	a,
		pls_segurado 		b,
		pls_carteira_emissao 	c,
		pls_lote_carteira 	d
	where 	a.nr_seq_segurado   	= b.nr_sequencia
	and 	c.nr_seq_seg_carteira 	= a.nr_sequencia
	and 	c.nr_seq_lote         	= d.nr_sequencia
	and 	e.nr_sequencia        	= b.nr_seq_contrato
	and 	d.nr_sequencia        	= nr_seq_lote_p
	order by 	e.ds_contrato,
			pls_obter_dados_localiz_benef(b.nr_seq_localizacao_benef, 'D'),
			pls_obter_dados_segurado(nvl(b.nr_seq_titular, b.nr_sequencia), 'N'),
			b.cd_matricula_familia;

begin

delete 	w_pls_interface_carteira 
where 	nr_seq_lote = nr_seq_lote_p;

open C01;
loop
fetch C01 into
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	select 	rpad(d.nm_fantasia,50) nm_fantasia,
		rpad(pls_convert_masc_cart_usuario(c.cd_usuario_plano,cd_estabelecimento_p),32) cd_usuario_plano,
		rpad(to_char(c.dt_validade_carteira,'dd/mm/yyyy'),10) dt_validade_carteira,
		rpad(a.nm_pessoa_fisica,50) nm_pessoa_fisica,
		rpad(to_char(a.dt_nascimento,'dd/mm/yyyy'),10) dt_nascimento,
		rpad(a.ie_sexo,1) ie_sexo,
		rpad(nvl(t.ds_contrato_carteira,t.ds_contrato),50) nm_estipulante_contrato,
		rpad(substr(pls_obter_dados_segurado(b.nr_seq_titular,'N'),1,255),120) nm_titular,
		rpad(c.ds_mensagem_carteira,130) ds_observacao,
		rpad(a.nr_prontuario,20) nr_prontuario,
		rpad(d.nr_protocolo_ans || d.cd_scpa,9) ds_protocolo_ans,
		d.ie_segmentacao,
		rpad(substr(pls_obter_dados_produto(d.nr_sequencia,'C'),1,255),50) ie_tipo_contratacao,
		rpad(decode(d.ie_coparticipacao, 'S', 'Co-participação', 'N', 'Sem Co-participação'),25) ie_coparticipacao,
		rpad(a.nr_cartao_nac_sus,15) nr_cartao_nac_sus,
		d.nr_sequencia,
		nvl(e.ds_localizacao,' ')
	into 	ds_produto_w,
		cd_usuario_plano_w,
		dt_validade_carteira_w,
		nm_beneficiario_w,
		dt_nascimento_w,
		ds_trilha_1_w,
		ds_estipulante_w,
		nm_titular_w,
		ds_observacao_w,
		ds_trilha_2_w,
		ds_protocolo_ans_w,
		ie_segmentacao_w,
		tp_contratacao_w,
		ds_trilha_3_w,
		nm_usuario_nrec_w,
		nr_seq_plano_w,
		ds_localizacao_w
	from 	pls_localizacao_benef		e,
		pls_plano 			d,
		pls_segurado_carteira 		c,
		pls_segurado 			b,
		pessoa_fisica 			a,
		pls_contrato 			t
	where 	b.cd_pessoa_fisica 	= a.cd_pessoa_fisica
	and 	c.nr_seq_segurado    	= b.nr_sequencia
	and 	b.nr_seq_plano       	= d.nr_sequencia
	and 	t.nr_sequencia       	= b.nr_seq_contrato
	and	e.nr_sequencia(+)	= b.nr_seq_localizacao_benef
	and 	b.nr_sequencia       	= nr_seq_segurado_w;
	
	nr_seq_w := 0;
	
	select 	max(nr_sequencia)
	into 	nr_seq_w
	from 	pls_carencia
	where 	nr_seq_segurado = nr_seq_segurado_w
	and 	ie_cpt = 'S';
	
	dt_cpt_w := null;
		
	if (nvl(nr_seq_w,0) > 0) then
		select 	(rpad(to_char(dt_inicio_vigencia + qt_dias,'dd/mm/yyyy'),10))
		into 	dt_cpt_w
		from 	pls_carencia
		where 	nr_sequencia = nr_seq_w;
	end if;
	
	nr_seq_w := 0;
	
	select 	max(nr_sequencia)
	into 	nr_seq_w
	from 	pls_plano_area
	where 	nr_seq_plano  = nr_seq_plano_w;
	
	ds_regiao_w := null;
	
	if (nvl(nr_seq_w,0) > 0) then
		select 	rpad(substr(obter_desc_municipio_ibge(cd_municipio_ibge),1,255)
			|| sg_estado
			|| substr(obter_descricao_padrao_pk('PLS_REGIAO','DS_REGIAO','NR_SEQUENCIA',nr_seq_regiao),1,255),81) ds_area
		into 	ds_regiao_w
		from 	pls_plano_area
		where 	nr_sequencia = nr_seq_w;
	end if;
	
	nr_seq_w := 0;
	
	select 	max(nr_sequencia)
	into 	nr_seq_w
	from 	pls_plano_acomodacao
	where 	nr_seq_plano = nr_seq_plano_w;
	
	ds_acomodacao_w := null;
	
	if (nvl(nr_seq_w,0) > 0) then
		select 	rpad((substr(obter_descricao_padrao('PLS_CATEGORIA','DS_CATEGORIA',nr_seq_categoria),1,255)
			|| substr(obter_descricao_padrao('PLS_TIPO_ACOMODACAO','DS_TIPO_ACOMODACAO',nr_seq_tipo_acomodacao),1,255)),32) ds_tipo_acomodacao
		into 	ds_acomodacao_w
		from 	pls_plano_acomodacao
		where 	nr_sequencia = nr_seq_w;
	end if;
	
	ds_acomodacao_w        := upper(ds_acomodacao_w);
	
	if (ie_segmentacao_w    = '1') then
		ds_segmentacao_w      := 'AMB';
	elsif (ie_segmentacao_w = '2') then
		ds_segmentacao_w      := 'HOSP+OBST';
	elsif (ie_segmentacao_w = '3') then
		ds_segmentacao_w      := 'HOSP';
	elsif (ie_segmentacao_w = '4') then
		ds_segmentacao_w      := 'ODONT';
	elsif (ie_segmentacao_w = '5') then
		ds_segmentacao_w      := 'REF+AMB+HOSP+OBST';
	elsif (ie_segmentacao_w = '6') then
		ds_segmentacao_w      := 'AMB+HOSP+OBST';
	elsif (ie_segmentacao_w = '7') then
		ds_segmentacao_w      := 'AMB+HOSP';
	elsif (ie_segmentacao_w = '8') then
		ds_segmentacao_w      := 'AMB+ODONT';
	elsif (ie_segmentacao_w = '9') then
		ds_segmentacao_w      := 'HOSP+OBST+ODONT';
	elsif (ie_segmentacao_w = '10') then
		ds_segmentacao_w      := 'HOSP+ODONT';
	elsif (ie_segmentacao_w = '11') then
		ds_segmentacao_w      := 'AMB+HOSP+OBST+ODONT';
	elsif (ie_segmentacao_w = '12') then
		ds_segmentacao_w      := 'AMB+HOSP+ODONT';
	end if;
	
	insert	into	w_pls_interface_carteira
		(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nr_seq_lote, ie_tipo_reg, cd_usuario_plano, nm_beneficiario,
			nm_titular, nm_plano, ds_estipulante, dt_nascimento,
			ds_tipo_contratacao, ds_segmentacao, nr_protocolo_ans,
			ds_acomodacao, nr_seq_segurado, dt_validade_carteira, 
			dt_cpt,ds_regiao,ds_observacao, nm_usuario_nrec,
			ds_trilha_1, ds_trilha_2, ds_trilha_3, ds_logradouro)
	values	(	w_pls_interface_carteira_seq.nextval, sysdate, nm_usuario_p, sysdate,
			nr_seq_lote_p, 2, cd_usuario_plano_w, nm_beneficiario_w,
			nm_titular_w, ds_produto_w, ds_estipulante_w, dt_nascimento_w,
			tp_contratacao_w, ds_segmentacao_w, ds_protocolo_ans_w,
			ds_acomodacao_w, nr_seq_segurado_w, dt_validade_carteira_w, 
			dt_cpt_w,ds_regiao_w,ds_observacao_w, nm_usuario_nrec_w,
			ds_trilha_1_w, ds_trilha_2_w, ds_trilha_3_w, ds_localizacao_w);
	end;
end loop;
close C01;

commit;

end pls_gerar_cartao_marciocunha;
/