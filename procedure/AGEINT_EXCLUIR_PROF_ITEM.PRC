create or replace
procedure ageint_excluir_prof_item(
			nr_seq_item_p		number,
			nm_usuario_p		Varchar2) is 
						
begin

if	(nr_seq_item_p > 0) then
	delete 	agenda_integrada_prof_item
	where	nr_seq_agenda_item     	= nr_seq_item_p;
	
	commit;
end if;

end ageint_excluir_prof_item;
/