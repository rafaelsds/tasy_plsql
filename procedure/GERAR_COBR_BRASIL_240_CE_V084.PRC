create or replace
procedure gerar_cobr_brasil_240_ce_v084
		(	nr_seq_cobr_escrit_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is 

ds_conteudo_w			varchar2(4000);
ds_brancos_205_w		varchar2(205);
ds_brancos_165_w		varchar2(165);
nm_empresa_w			varchar2(80);
nm_pessoa_w			varchar2(80);
ds_brancos_54_w			varchar2(54);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(40);
ds_brancos_40_w			varchar2(40);
ds_banco_w			varchar2(30);
ds_brancos_25_w			varchar2(25);
cd_convenio_banco_w		varchar2(20);
ds_cidade_w			varchar2(20);
ds_nosso_numero_w		varchar2(20);
ds_complemento_w		varchar2(18);
vl_juros_w			varchar2(17);
ds_brancos_17_w			varchar2(17);
nr_conta_corrente_w		varchar2(15);
dt_remessa_retorno_w		varchar2(14);
dt_geracao_arquivo_w		varchar2(14);
nr_inscricao_w			varchar2(14);
vl_liquidacao_w			varchar2(14);
vl_cobranca_w			varchar2(14);
vl_desconto_w			varchar2(14);
vl_mora_w			varchar2(14);
nr_seq_registro_w		varchar2(13);
ds_brancos_10_w			varchar2(10);
qt_reg_lote_w			varchar2(10);
dt_vencimento_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_pagamento_w			varchar2(8);
cd_cep_w			varchar2(8);
nr_seq_lote_w			varchar2(7);
nr_seq_arquivo_w		varchar2(6);
nr_seq_envio_w			varchar2(5);
cd_agencia_bancaria_w		varchar2(5);
ie_tipo_inscricao_w		varchar2(5);
nr_endereco_w			varchar2(5);
cd_banco_w			varchar2(3);
ie_emissao_bloqueto_w		varchar2(3);
nr_digito_agencia_w		varchar2(2);
sg_uf_w				varchar2(2);
ie_protesto_w			varchar2(2);
ie_tipo_registro_w		varchar2(1);
qt_reg_arquivo_w		number(10);
qt_registros_w			number(10)	:= 0;

cursor C01 is
	select	lpad(c.cd_banco,3,'0') cd_banco,
		substr(a.nr_sequencia,1,5) nr_seq_envio,
		'3' ie_tipo_registro,
		rpad(to_char(c.nr_sequencia) ,13,'0') nr_seq_registro,
		lpad(c.vl_liquidacao,14,'0') vl_liqudacao,
		rpad('Banco do Brasil',30,' ') nm_cedente,
		to_char(b.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento,
		lpad(c.vl_cobranca,14,'0') vl_cobranca,
		lpad(c.vl_desconto,14,'0') vl_desconto,
		lpad(nvl(c.vl_juros,0) + nvl(c.vl_multa,0),14,'0') vl_mora,
		to_char(c.dt_liquidacao,'ddmmyyyy') dt_pagamento,
		substr(nvl(d.cd_convenio_banco,e.cd_convenio_banco)||lpad(c.nr_titulo,10,0),1,20) ds_nosso_numero
	from	titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a,
		banco_carteira d,
		banco_estabelecimento e
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= e.nr_sequencia
	and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',17,' '),
	lpad(' ',10,' '),
	lpad(' ',54,' '),
	lpad(' ',40,' '),
	lpad(' ',25,' '),
	lpad(' ',165,' '),
	lpad(' ',205,' ')
into	ds_brancos_17_w,
	ds_brancos_10_w,
	ds_brancos_54_w,
	ds_brancos_40_w,
	ds_brancos_25_w,
	ds_brancos_165_w,
	ds_brancos_205_w
from	dual;

/* Header Arquivo*/
select	'0' ie_tipo_registro,
	lpad(c.cd_banco,3,'0') cd_banco,
	to_char(sysdate,'DDMMYYY HHMISS') dt_geracao_arquivo,
	rpad('BANCO DO BRASIL',30,' ') ds_banco,
	rpad(c.cd_agencia_bancaria,5,0) cd_agencia_bancaria,
	calcula_digito('Modulo11',c.cd_agencia_bancaria) nr_digito_agencia,
	c.cd_conta||C.ie_digito_conta nr_conta_corrente,
	upper(substr(obter_razao_social(b.cd_cgc),1,30)) nm_empresa,
	b.cd_cgc nr_inscricao,
	rpad(nvl(d.cd_convenio_banco,c.cd_convenio_banco),9,0)||'0126'|| '       ' cd_convenio_banco,
	rpad(a.nr_sequencia,6,'0')
into	ie_tipo_registro_w,
	cd_banco_w,
	dt_geracao_arquivo_w,
	ds_banco_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_inscricao_w,
	cd_convenio_banco_w,
	nr_seq_arquivo_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:= cd_banco_w || '0000' || ie_tipo_registro_w || '         ' || '2' || nr_inscricao_w || cd_convenio_banco_w || 
			cd_agencia_bancaria_w || nr_digito_agencia_w || nr_conta_corrente_w || ' ' || nm_empresa_w || ds_banco_w ||
			ds_brancos_10_w || '1' || dt_geracao_arquivo_w || nr_seq_arquivo_w || '040' || '0000' || ds_brancos_54_w ||
			'000' || '00' || '0000000000';

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		1);

qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Header Arquivo */

/* Header Lote */
select	'1' ie_tipo_registro,
	lpad(c.cd_banco,3,'0') cd_banco,
	substr(a.nr_sequencia,1,5) nr_seq_envio,
	rpad(c.cd_agencia_bancaria,5,0) cd_agencia_bancaria,
	calcula_digito('Modulo11',c.cd_agencia_bancaria) nr_digito_agencia,
	c.cd_conta||C.ie_digito_conta nr_conta_corrente,
	upper(substr(obter_razao_social(b.cd_cgc),1,30)) nm_empresa,
	b.cd_cgc nr_inscricao,
	rpad(nvl(d.cd_convenio_banco,c.cd_convenio_banco),9,0)||'0126'|| '       ' cd_convenio_banco,
	rpad(a.nr_sequencia,6,'0'),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'EN'),40,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'NR'),5,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'CM'),18,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'MU'),20,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'CEP'),8,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'UF'),2,' ')
into	ie_tipo_registro_w,
	cd_banco_w,
	nr_seq_envio_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_inscricao_w,
	cd_convenio_banco_w,
	nr_seq_arquivo_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_cidade_w,
	cd_cep_w,
	sg_uf_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:= cd_banco_w || nr_seq_envio_w || ie_tipo_registro_w || 'R' || '98' || '30' || '030' || ' ' || '2' || nr_inscricao_w || 
			cd_convenio_banco_w || cd_agencia_bancaria_w || nr_digito_agencia_w || nr_conta_corrente_w || ' ' || nm_empresa_w || 
			ds_brancos_40_w || ds_endereco_w || nr_endereco_w || ds_complemento_w || ds_cidade_w || cd_cep_w || sg_uf_w || '        ' ||
			'0000000000';

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		2);
qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Header Lote */

/* Segmento J*/
open C01;
loop
fetch C01 into	
	cd_banco_w,
	nr_seq_envio_w,
	ie_tipo_registro_w,
	nr_seq_registro_w,
	vl_liquidacao_w,
	ds_banco_w,
	dt_vencimento_w,
	vl_cobranca_w,
	vl_desconto_w,
	vl_mora_w,
	dt_pagamento_w,
	ds_nosso_numero_w;
exit when C01%notfound;
	begin
	ds_conteudo_w	:= cd_banco_w || nr_seq_envio_w || ie_tipo_registro_w || nr_seq_registro_w || 'J' || '0' || '00' || cd_banco_w || '9' ||
			'1' || vl_liquidacao_w || ds_brancos_25_w || ds_banco_w || dt_vencimento_w || vl_cobranca_w || vl_desconto_w || vl_mora_w ||
			dt_pagamento_w || vl_liquidacao_w || '0000000000' || ds_brancos_10_w || ds_brancos_10_w || ds_nosso_numero_w || '09' ||
			'      ' || '0000000000';
	
	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			3);
				
	qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
	
	qt_registros_w		:= qt_registros_w + 1;
	
	if	(qt_registros_w = 500) then
		qt_registros_w	:= 0;
		commit;
	end if;
	end;
end loop;
close C01;
/* Fim segmento J*/

/* Trailler Lote*/
select	count(*),
	'9' ie_tipo_registro,
	'99999' nr_seq_envio,
	sum(b.vl_liquidacao)
into	qt_reg_lote_w,
	ie_tipo_registro_w,
	nr_seq_envio_w,
	vl_liquidacao_w
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
group by a.nr_sequencia,
	 a.dt_remessa_retorno,
	 a.dt_remessa_retorno,
	 a.cd_banco,
	a.ie_emissao_bloqueto;
	
ds_conteudo_w	:= '001' || nr_seq_envio_w || '5' || '         ' || qt_reg_lote_w || vl_liquidacao_w || '0000000000000' || '000000' || 
			ds_brancos_165_w || '0000000000';
	
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		4);
		
qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Trailler Lote*/

/* Trailler Arquivo*/
select	count(*),
	'9' ie_tipo_registro,
	'99999' nr_seq_envio,
	sum(b.vl_liquidacao)
into	qt_reg_lote_w,
	ie_tipo_registro_w,
	nr_seq_envio_w,
	vl_liquidacao_w
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
group by a.nr_sequencia,
	 a.dt_remessa_retorno,
	 a.dt_remessa_retorno,
	 a.cd_banco,
	a.ie_emissao_bloqueto;

begin
ds_conteudo_w	:= '001' || '9999' || '9' || '         ' || '1' || to_char(qt_reg_arquivo_w + 1)  || '000000' || ds_brancos_205_w;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(186264,'DS_CONTEUDO_W='||length(DS_CONTEUDO_W));
	--length('001' || '9999' || '9' || '         ' || '1' || to_char(qt_reg_arquivo_w + 1)  || '000000' || ds_brancos_205_w) || '#@#@');
end;
	
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		5);
/* Fim Trailler Arquivo*/

commit;

end gerar_cobr_brasil_240_ce_v084;
/
