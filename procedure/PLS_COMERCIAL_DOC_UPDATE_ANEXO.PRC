create or replace
procedure pls_comercial_doc_update_anexo	  
			(	nr_seq_cliente_p		Number,
				nr_sequencia_p			Number) is 
begin

if 	(nr_seq_cliente_p > 0) and ( nr_sequencia_p > 0) then
	update	pls_comercial_documento
	set	ie_anexar = 'N'
	where	nr_seq_cliente = nr_seq_cliente_p
	and     nr_sequencia = nr_sequencia_p;
end if;

commit;

end pls_comercial_doc_update_anexo;
/