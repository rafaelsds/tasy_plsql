create or replace
procedure inserir_pac_atend_visita_js(
					nr_atendimento_p		number,
					cd_pessoa_fisica_p		varchar2,
					nm_usuario_p			varchar2,
					nr_controle_acesso_p	out	number,
					nr_identidade_p		out	varchar2,
					nr_seq_visitante_p	out	number,
					ds_msg_acesso_local_p	out 	varchar2,
					ds_msg_erro_integracao_p out    varchar2,
					ds_msg_erro_lib_acesso_p out 	varchar2,
					nr_seq_atend_visita_p	 out 	number) is 

begin

inserir_paciente_atend_visita(nr_atendimento_p, cd_pessoa_fisica_p, nm_usuario_p, nr_controle_acesso_p, nr_identidade_p, nr_seq_visitante_p);

ds_msg_acesso_local_p := substr(wheb_mensagem_pck.get_texto(223994, null),1,255);
ds_msg_erro_integracao_p := substr(wheb_mensagem_pck.get_texto(224034, null),1,255);
ds_msg_erro_lib_acesso_p := substr(wheb_mensagem_pck.get_texto(224020, null),1,255);

select	nvl(max(nr_sequencia),0)
into	nr_seq_atend_visita_p
from	atendimento_visita
where	nr_controle_acesso = nvl(nr_controle_acesso_p,0);


commit;

end inserir_pac_atend_visita_js;
/