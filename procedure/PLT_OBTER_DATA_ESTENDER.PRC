create or replace
procedure plt_obter_data_estender(
			ie_estender_plano_atual_p	varchar2,
			nr_atendimento_P		number,
			nr_prescricao_p			number,
			cd_estabelecimento_p		number,
			cd_perfil_p			number,
			cd_pessoa_p			varchar2,
			cd_setor_atendimento_p		number,
			dt_quebra_p			date,
			hr_quebra_p			date,
			dt_inicio_estender_p	out	date,
			dt_final_estender_p	out	date,
			nm_usuario_p		Varchar2) is 

dt_inicio_estender_w	date;
dt_final_estender_w	date;

begin
if	(ie_estender_plano_atual_p is not null) and
	(nr_atendimento_P is not null) and
	(nr_prescricao_p is not null) and
	(cd_estabelecimento_p is not null) and
	(cd_perfil_p is not null) and
	(cd_pessoa_p is not null) and
	--(cd_setor_atendimento_p is not null) and
	(dt_quebra_p is not null) and
	(hr_quebra_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	('S' = ie_estender_plano_atual_p) then
		begin
		
		dt_inicio_estender_w	:= converte_char_data(to_char(sysdate,'dd/mm/yyyy'),to_char(plt_obter_prim_hor_plano(nr_atendimento_p, cd_setor_atendimento_p, sysdate, nm_usuario_p),'hh24:mi')||':00',null);

		end;
	else	
		begin
		select	plt_obter_inicio_extensao(cd_pessoa_p, nm_usuario_p, cd_setor_atendimento_p, dt_quebra_p, hr_quebra_p, cd_perfil_p,nr_atendimento_P)
		into	dt_inicio_estender_w
		from 	dual;
		end;
	end if;

	if	(dt_inicio_estender_w is not null) then
		begin
		
		select	plt_obter_final_extensao(to_char(dt_inicio_estender_w, 'dd/mm/yyyy'), to_char(to_date(dt_inicio_estender_w,'dd/mm/yyyy hh24:mi:ss'),'hh24:mi:ss'), nr_prescricao_p ,nr_atendimento_P,cd_estabelecimento_p,cd_perfil_p,nm_usuario_p,cd_setor_atendimento_p)
		into	dt_final_estender_w
		from	prescr_medica
		where	nr_prescricao  = nr_prescricao_p;
	
		end;	
	end if;
	end;
	
	dt_inicio_estender_p	:= dt_inicio_estender_w;
	dt_final_estender_p	:= dt_final_estender_w;
	
end if;

commit;

end plt_obter_data_estender;
/
