create or replace
procedure VINCULAR_BARRAS_TIT_PAGAR	(	nr_seq_banco_escrit_barras_p	number,
						nr_titulo_p			number,
						nr_bloqueto_p			varchar2,
						nr_seq_escrit_p			number,
						nm_usuario_p			varchar2) is

cd_cgc_barras_w		varchar2(14);
cd_cgc_titulo_w		varchar2(14);
cd_pessoa_barras_w	varchar2(10);
cd_pessoa_titulo_w		varchar2(10);
cd_pessoa_externo_w	varchar2(60);
cd_cnpj_raiz_w		pessoa_juridica.cd_cnpj_raiz%type;
cd_cnpj_raiz_barras_w	pessoa_juridica.cd_cnpj_raiz%type;
ie_alt_tit_bloq_barras_w	varchar2(1);
ie_vincular_tit_bloq_barras_w	varchar2(1);

begin

obter_param_usuario(857, 60, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_alt_tit_bloq_barras_w);
obter_param_usuario(857, 66, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_vincular_tit_bloq_barras_w);

if	(nr_seq_banco_escrit_barras_p is not null) and
	((nr_titulo_p is not null) and (nr_titulo_p <> 0)) and
	(nr_bloqueto_p is not null) then
	
	validar_se_barras_vinc_titulo(nr_bloqueto_p,nr_titulo_p);
	
	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica),
		max(a.cd_pessoa_externo),
		max(b.cd_cnpj_raiz)
	into	cd_cgc_barras_w,
		cd_pessoa_barras_w,
		cd_pessoa_externo_w,
		cd_cnpj_raiz_barras_w
	from	pessoa_juridica b,
		banco_escrit_barras a
	where	nvl(a.cd_cgc,a.cd_pessoa_externo)	= b.cd_cgc(+)
	and	a.nr_sequencia				= nr_seq_banco_escrit_barras_p;

	select	max(a.cd_cgc),
		max(a.cd_pessoa_fisica),
		max(b.cd_cnpj_raiz)
	into	cd_cgc_titulo_w,
		cd_pessoa_titulo_w,
		cd_cnpj_raiz_w
	from	pessoa_juridica b,
		titulo_pagar a
	where	a.cd_cgc	= b.cd_cgc(+)
	and	a.nr_titulo	= nr_titulo_p;

	if	((cd_cgc_titulo_w is not null) and (nvl(cd_cgc_barras_w,cd_pessoa_externo_w) <> cd_cgc_titulo_w) and (nvl(cd_cnpj_raiz_barras_w,'X') <> nvl(cd_cnpj_raiz_w,'Y')) and (substr(nvl(cd_cgc_barras_w,cd_pessoa_externo_w),1,8) <> substr(cd_cgc_titulo_w,1,8))) or
		((cd_pessoa_titulo_w is not null) and (nvl(cd_pessoa_barras_w,cd_pessoa_externo_w) <> cd_pessoa_titulo_w)) then

		/* A pessoa do t�tulo � diferente da pessoa do c�digo de barras! */
		wheb_mensagem_pck.exibir_mensagem_abort(251417);

	end if;

	update	banco_escrit_barras
	set	nr_titulo		= nr_titulo_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_banco_escrit_barras_p;
	
	if (nvl(ie_vincular_tit_bloq_barras_w, 'N') = 'S') then 
		update	titulo_pagar                                                                               
		set	nr_bloqueto		= nr_bloqueto_p,                                                                 
			nm_usuario		= nm_usuario_p,                                                                                                                                                
			ie_bloqueto     = 'S'                                                                           
		where	nr_titulo	= nr_titulo_p;    	
	end if;
		
	if (nvl(ie_alt_tit_bloq_barras_w, 'N') = 'S') then                                                 
		update	titulo_pagar                                                                               
		set	ie_tipo_titulo	= '1',
			nm_usuario		= nm_usuario_p
		where	nr_titulo	= nr_titulo_p;                                                                   
	end if;                                                                                            

	if	(nvl(nr_seq_escrit_p,0)	= 0) then
		definir_banco_tit_escritural('CP',nr_titulo_p,null,nm_usuario_p,'N');
	else
		definir_banco_tit_escritural('CP',nr_titulo_p,nr_seq_escrit_p,nm_usuario_p,'N');
	end if;

	commit;

end if;

end	VINCULAR_BARRAS_TIT_PAGAR;
/
