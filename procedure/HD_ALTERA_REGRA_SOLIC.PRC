create or replace
procedure hd_altera_regra_solic is
ie_regra_ok_w		varchar2(1);
ie_regra_solic_w	varchar2(15);
cd_pessoa_fisica_w	varchar2(10);
cd_pessoa_exame_w	varchar2(10);
nr_seq_prot_exame_w	number(10);
ie_diabetico_w		varchar2(1);
ie_regra_solic_novo_w	varchar(15);	
Cursor C01 is
	select	cd_pessoa_fisica
	from	hd_pac_renal_cronico
	where	hd_obter_se_paciente_ativo(cd_pessoa_fisica) = 'S';

Cursor C02 is
	select	ie_regra_solic,
		cd_pessoa_fisica,
		nr_sequencia
	from	hd_pac_prot_exame
	where	cd_pessoa_fisica =  cd_pessoa_fisica_w;		
begin
open C01;
loop
fetch C01 into	
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		ie_regra_solic_w,
		cd_pessoa_exame_w,
		nr_seq_prot_exame_w;
	exit when C02%notfound;
		begin
		if	(ie_regra_solic_w is not null) then			
			ie_regra_ok_w := hd_obter_se_regra_exame(ie_regra_solic_w,cd_pessoa_exame_w,'S');						
			if	(ie_regra_ok_w = 'N') then						
				ie_regra_solic_novo_w := hd_obter_regra_solic(cd_pessoa_exame_w);			
				if	(ie_regra_solic_novo_w is not null) then
					update	hd_pac_prot_exame
					set	ie_regra_solic = ie_regra_solic_novo_w
					where	nr_sequencia = nr_seq_prot_exame_w;
					
					--gravar_log_tasy(65874,'Sequencia='||nr_seq_prot_exame_w||' Regra antiga='||ie_regra_solic_w||' Regra nova='||ie_regra_solic_novo_w,'tasy');
				end if;
			end if;		
		end if;	
		end;
	end loop;
	close C02;	
	
	end;
end loop;
close C01;

commit;

end hd_altera_regra_solic;
/