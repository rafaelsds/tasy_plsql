create or replace
procedure definir_suep_padrao( 	nr_sequencia_suep_p	number,
				ie_acao_p		varchar2,
				nm_usuario_p		varchar2) is
				
			/*
				D - Definir padrao
				L - Limpar padrao
			*/				

ie_existe_regra_w 	varchar2(1);

begin

if 	(nr_sequencia_suep_p is not null) and
	(ie_acao_p = 'D') then
	
	select 	nvl(max('S'),'N') 
	into 	ie_existe_regra_w
	from 	suep_padrao_usuario
	where 	nr_seq_modelo_suep = nr_sequencia_suep_p
	and	nm_usuario_suep = nm_usuario_p;
	
	if (ie_existe_regra_w = 'S') then 	
	
		update	suep_padrao_usuario d
		set	nr_seq_modelo_suep = nr_sequencia_suep_p
		where	nm_usuario_suep = nm_usuario_p;
		
	else
		
		insert into suep_padrao_usuario (nr_sequencia ,          
						dt_atualizacao,         
						nm_usuario,             
						dt_atualizacao_nrec,    
						nm_usuario_nrec,        
						nm_usuario_suep,        
						nr_seq_modelo_suep)
					values	(suep_padrao_usuario_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nm_usuario_p,
						nr_sequencia_suep_p);

	end if;
	
	commit;
	
elsif	(nr_sequencia_suep_p is not null) and
	(ie_acao_p = 'L') then
	
	delete 	from suep_padrao_usuario
	where	nm_usuario_suep = nm_usuario_p;
	
	
	commit;
	
end if;

end definir_suep_padrao;
/