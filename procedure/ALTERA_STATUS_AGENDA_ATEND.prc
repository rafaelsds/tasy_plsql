CREATE OR REPLACE PROCEDURE ALTERA_STATUS_AGENDA_ATEND(NM_USUARIO_P         VARCHAR2,
                                                       CD_ESTABELECIMENTO_P NUMBER,
                                                       NR_SEQ_AGENDA_P      NUMBER,
                                                       IE_AGENDA_P          VARCHAR2,
                                                       IE_STATUS_P         VARCHAR2  DEFAULT 'E') AS
                                                       
  ie_status_atend_w  funcao_param_usuario.vl_parametro%type;
  nr_atend_agenda_w  agenda_paciente.nr_atendimento%type;
  nr_seq_agenda_w    agenda_paciente.nr_sequencia%type;
  dt_agenda_ww       agenda_paciente.dt_agenda%type;
  cd_agenda_ww       agenda_paciente.cd_agenda%type;
  hr_inicio_ww       agenda_paciente.hr_inicio%type;
  ie_possui_agenda_w number(10) := 0;
  ie_status_w        agenda_paciente.ie_status_agenda%type;
  nr_seq_hora_ww     agenda_consulta.nr_seq_hora%type;

  cursor c01 is
    select nr_sequencia
      from agenda_paciente
     where nr_sequencia <> nr_seq_agenda_p
       and nr_atendimento = nr_atend_agenda_w
     order by nr_sequencia;

  Cursor C04 is
    select nr_sequencia
      from agenda_consulta
     where nr_atendimento = nr_atend_agenda_w
     order by nr_sequencia;

  cursor c03 is
    select nr_sequencia
      from agenda_paciente a, agenda b
     where a.nr_sequencia <> nr_seq_agenda_p
       and a.nr_atendimento = nr_atend_agenda_w
       and a.cd_agenda = b.cd_agenda
       and b.cd_tipo_agenda = decode(ie_agenda_p, 'CI', 1, 'E', 2, 0)
     order by nr_sequencia;

  cursor c05 is
    select nr_sequencia
      from agenda_paciente
     where nr_sequencia <> nr_seq_agenda_p
       and nr_atendimento = nr_atend_agenda_w
       and ie_status_agenda in ('CN', 'M', 'N', 'P')
     order by nr_sequencia;

begin
  /* alterar status agendas atendimento */
  ie_status_w := ie_status_p;
  
  select nvl(max(obter_valor_param_usuario(820,
                                           24,
                                           obter_perfil_ativo,
                                           nm_usuario_p,
                                           cd_estabelecimento_p)),
             'N')
    into ie_status_atend_w
    from dual;

  if (ie_status_atend_w = 'S') then
    -- Todos os agendamentos, independente do tipo de agenda
    select nvl(max(nr_atendimento), 0)
      into nr_atend_agenda_w
      from agenda_paciente
     where nr_sequencia = nr_seq_agenda_p;
  
    if (nr_atend_agenda_w > 0) then
      open c01;
      loop
        fetch c01
          into nr_seq_agenda_w;
        exit when c01%notfound;
        begin
        
          select cd_agenda, dt_agenda, hr_inicio
            into cd_agenda_ww, dt_agenda_ww, hr_inicio_ww
            from agenda_paciente
           where nr_sequencia = nr_seq_agenda_w;
        
          --Verificar se ja existe outro registro "Executado" com a mesma data(AGEPACI_UK)
          ie_possui_agenda_w := 0;
          select count(*)
            into ie_possui_agenda_w
            from agenda_paciente
           where cd_agenda = cd_agenda_ww
             and dt_agenda = dt_agenda_ww
             and to_char(hr_inicio, 'hh24:mi') =
                 to_char(hr_inicio_ww, 'hh24:mi')
             and ie_status_agenda = ie_status_w
             and nr_sequencia <> nr_seq_agenda_w;
        
          if (ie_possui_agenda_w = 0) then
            update agenda_paciente
               set ie_status_agenda = ie_status_w
             where nr_sequencia = nr_seq_agenda_w;
          end if;
        
          if (ie_status_w = 'AD') then
          
            update agenda_paciente
               set dt_atendido = sysdate
             where nr_sequencia = nr_seq_agenda_w;
          
          elsif (ie_status_w = 'O') then
            update agenda_paciente
               set dt_atendimento = sysdate
             where nr_sequencia = nr_seq_agenda_w;
          end if;
        
        end;
      end loop;
      close c01;
    
      open C04;
      loop
        fetch C04
          into nr_seq_agenda_w;
        exit when C04%notfound;
        begin
          select cd_agenda, dt_agenda, nr_seq_hora
            into cd_agenda_ww, dt_agenda_ww, nr_seq_hora_ww
            from agenda_consulta
           where nr_sequencia = nr_seq_agenda_w;
        
          --Verificar se ja existe outro registro "Executado" com a mesma data(AGECONS_UK)
          ie_possui_agenda_w := 0;
          select count(*)
            into ie_possui_agenda_w
            from agenda_consulta
           where cd_agenda = cd_agenda_ww
             and dt_agenda = dt_agenda_ww
             and ie_status_agenda = ie_status_w
             and nr_seq_hora = nr_seq_hora_ww
             and nr_sequencia <> nr_seq_agenda_w;
        
          if (ie_possui_agenda_w = 0) then
            update agenda_consulta
               set ie_status_agenda = ie_status_w
             where nr_sequencia = nr_seq_agenda_w;
          end if;
        
          if (ie_status_w = 'AD') then
            update agenda_consulta
               set dt_status_atendido = sysdate
             where nr_sequencia = nr_seq_agenda_w;
          elsif (ie_status_w = 'O') then
            update agenda_consulta
               set dt_consulta = sysdate
             where nr_sequencia = nr_seq_agenda_w;
          end if;
        
        end;
      end loop;
      close C04;
    end if;
  elsif (ie_status_atend_w = 'T') then
    -- Somente para o mesmo tipo de agenda
    select nvl(max(nr_atendimento), 0)
      into nr_atend_agenda_w
      from agenda_paciente
     where nr_sequencia = nr_seq_agenda_p;
  
    if (nr_atend_agenda_w > 0) then
      open c03;
      loop
        fetch c03
          into nr_seq_agenda_w;
        exit when c03%notfound;
        begin
        
          select cd_agenda, dt_agenda, hr_inicio
            into cd_agenda_ww, dt_agenda_ww, hr_inicio_ww
            from agenda_paciente
           where nr_sequencia = nr_seq_agenda_w;
        
          --Verificar se ja existe outro registro "Executado" com a mesma data(AGEPACI_UK)
          ie_possui_agenda_w := 0;
          if (ie_status_w = 'E') then
            select count(*)
              into ie_possui_agenda_w
              from agenda_paciente
             where cd_agenda = cd_agenda_ww
               and dt_agenda = dt_agenda_ww
               and to_char(hr_inicio, 'hh24:mi') =
                   to_char(hr_inicio_ww, 'hh24:mi')
               and ie_status_agenda = ie_status_w
               and nr_sequencia <> nr_seq_agenda_w;
          end if;
        
          if (ie_possui_agenda_w = 0) then
            update agenda_paciente
               set ie_status_agenda = ie_status_w,
               dt_executada = decode(ie_status_w, 'E', sysdate, dt_executada)
             where nr_sequencia = nr_seq_agenda_w;
          end if;
        
          if (ie_status_w = 'AD') then
          
            update agenda_paciente
               set dt_atendido = sysdate
             where nr_sequencia = nr_seq_agenda_w;
          
          elsif (ie_status_w = 'O') then
            update agenda_paciente
               set dt_atendimento = sysdate
             where nr_sequencia = nr_seq_agenda_w;
          end if;
        
        end;
      end loop;
      close c03;
    end if;
  elsif (ie_status_atend_w = 'L') then
    -- Todos os agendamentos com status em aberto, independente do tipo de agenda
    begin
      select nvl(max(nr_atendimento), 0)
        into nr_atend_agenda_w
        from agenda_paciente
       where nr_sequencia = nr_seq_agenda_p;
    
      if (nr_atend_agenda_w > 0) then
        open C05;
        loop
          fetch c05
            into nr_seq_agenda_w;
          exit when c05%notfound;
          begin
          
            select cd_agenda, dt_agenda, hr_inicio
              into cd_agenda_ww, dt_agenda_ww, hr_inicio_ww
              from agenda_paciente
             where nr_sequencia = nr_seq_agenda_w;
          
            --Verificar se ja existe outro registro "Executado" com a mesma data(AGEPACI_UK)
            ie_possui_agenda_w := 0;
            select count(*)
              into ie_possui_agenda_w
              from agenda_paciente
             where cd_agenda = cd_agenda_ww
               and dt_agenda = dt_agenda_ww
               and to_char(hr_inicio, 'hh24:mi') =
                   to_char(hr_inicio_ww, 'hh24:mi')
               and ie_status_agenda = ie_status_w
               and nr_sequencia <> nr_seq_agenda_w;
          
            if (ie_possui_agenda_w = 0) then
              update agenda_paciente
                 set ie_status_agenda = ie_status_w
               where nr_sequencia = nr_seq_agenda_w;
            end if;
          
            if (ie_status_w = 'AD') then
            
              update agenda_paciente
                 set dt_atendido = sysdate
               where nr_sequencia = nr_seq_agenda_w;
            
            elsif (ie_status_w = 'O') then
              update agenda_paciente
                 set dt_atendimento = sysdate
               where nr_sequencia = nr_seq_agenda_w;
            end if;
          
          end;
        end loop;
        close c05;
      
        open C04;
        loop
          fetch C04
            into nr_seq_agenda_w;
          exit when C04%notfound;
          begin
            select cd_agenda, dt_agenda, nr_seq_hora
              into cd_agenda_ww, dt_agenda_ww, nr_seq_hora_ww
              from agenda_consulta
             where nr_sequencia = nr_seq_agenda_w;
          
            --Verificar se ja existe outro registro "Executado" com a mesma data(AGECONS_UK)
            ie_possui_agenda_w := 0;
            select count(*)
              into ie_possui_agenda_w
              from agenda_consulta
             where cd_agenda = cd_agenda_ww
               and dt_agenda = dt_agenda_ww
               and ie_status_agenda = ie_status_w
               and nr_seq_hora = nr_seq_hora_ww
               and nr_sequencia <> nr_seq_agenda_w;
          
            if (ie_possui_agenda_w = 0) then
              update agenda_consulta
                 set ie_status_agenda = ie_status_w
               where nr_sequencia = nr_seq_agenda_w;
            end if;
          
            if (ie_status_w = 'AD') then
              update agenda_consulta
                 set dt_status_atendido = sysdate
               where nr_sequencia = nr_seq_agenda_w;
            elsif (ie_status_w = 'O') then
              update agenda_consulta
                 set dt_consulta = sysdate
               where nr_sequencia = nr_seq_agenda_w;
            end if;
          
          end;
        end loop;
        close C04;
      end if;
    end;
  end if;
end;
/
