create or replace
procedure copiar_man_grupo_trab_estab(	cd_estab_origem_p		number,
					cd_estab_destino_p	number,
					nm_usuario_p		Varchar2) is 
					
nr_sequencia_w				number(10);
cd_empresa_w				number(04,0);

cursor c01 is
select	a.nr_sequencia,
	a.ds_grupo_trabalho,
	a.nr_seq_operacao,
	a.ie_situacao, 
	a.qt_tempo_meta,
	a.ie_periodo_meta,
	a.nr_seq_escala_classif,
	cd_exp_grupo_trabalho
from	man_grupo_trabalho a
where	a.cd_estabelecimento	= cd_estab_origem_p;

vet01	C01%RowType;

cursor c02 is
select	a.nr_sequencia,
	a.ds_causa,
	a.ie_situacao
from	man_causa_dano a
where	a.cd_estabelecimento	= cd_estab_origem_p
and	a.nr_seq_grupo_trab	= vet01.nr_sequencia;

vet02	C02%RowType;

begin
select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estab_destino_p;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	select	man_grupo_trabalho_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into man_grupo_trabalho( 
		nr_sequencia,
		ds_grupo_trabalho,
		cd_estabelecimento, 
		dt_atualizacao,
		nm_usuario, 
		ie_situacao, 
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_operacao,
		qt_tempo_meta,
		ie_periodo_meta,
		nr_seq_escala_classif,
		cd_empresa,
		cd_exp_grupo_trabalho)
	values( nr_sequencia_w,
		vet01.ds_grupo_trabalho,
		cd_estab_destino_p,
		sysdate,
		nm_usuario_p, 
		vet01.ie_situacao, 
		sysdate,
		nm_usuario_p,
		vet01.nr_seq_operacao, 
		vet01.qt_tempo_meta,
		vet01.ie_periodo_meta,
		vet01.nr_seq_escala_classif,
		cd_empresa_w,
		vet01.cd_exp_grupo_trabalho);
	
	/*Usu�rios */
	insert into man_grupo_trab_usuario(
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_grupo_trab,
		nm_usuario_param)
	select	sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		nm_usuario_param
	from	man_grupo_trab_usuario
	where	nr_seq_grupo_trab = vet01.nr_sequencia;
	
	/*Fun��es da manuten��o*/
	insert into man_tipo_funcao(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_grupo_trab,
		cd_cargo,
		ds_funcao,
		ie_situacao)
	select	man_tipo_funcao_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		cd_cargo,
		ds_funcao,
		ie_situacao
	from	man_tipo_funcao
	where	nr_seq_grupo_trab = vet01.nr_sequencia;

	/* Tipo de Solu��o */
	insert into man_tipo_solucao(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nr_seq_grupo_trab,
		ds_solucao,
		ie_situacao)
	select	man_tipo_solucao_seq.nextval,
		nm_usuario_p,
		sysdate,
		nr_sequencia_w,
		ds_solucao,
		ie_situacao
	from	man_tipo_solucao
	where	nr_seq_grupo_trab	= vet01.nr_sequencia;
	
	open C02;
	loop
	fetch C02 into	
		vet02;
	exit when C02%notfound;
		begin
		
		insert into man_causa_dano(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			ds_causa,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_grupo_trab,
			cd_empresa)
		values(	man_causa_dano_seq.nextval,
			cd_estab_destino_p,
			sysdate,
			nm_usuario_p,
			vet02.ds_causa,
			vet02.ie_situacao,
			sysdate,
			nm_usuario_p,
			nr_sequencia_w,
			cd_empresa_w);		
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

/*Copiar as Causas de dano que n�o possuem Grupo de trabalho */
insert into man_causa_dano(
	nr_sequencia,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	ds_causa,
	ie_situacao,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_grupo_trab)
select	man_causa_dano_seq.nextval,
	cd_estab_destino_p,
	sysdate,
	nm_usuario_p,
	ds_causa,
	ie_situacao,
	sysdate,
	nm_usuario_p,
	null
from	man_causa_dano
where	cd_estabelecimento	= cd_estab_origem_p
and	nr_seq_grupo_trab is null;

commit;

end copiar_man_grupo_trab_estab;
/