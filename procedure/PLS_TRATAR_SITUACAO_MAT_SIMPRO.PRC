create or replace
procedure pls_tratar_situacao_mat_simpro(nr_seq_simpro_preco_p	number,	
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is

ie_tipo_alteracao_w	varchar2(3);
dt_vigencia_w		date;
cd_simpro_w		number(15);
ie_atualizar_simpro_w	varchar2(1);					

begin

if	(nr_seq_simpro_preco_p is not null) then
	select	a.cd_simpro,
		a.ie_tipo_alteracao,
		a.dt_vigencia
	into	cd_simpro_w,
		ie_tipo_alteracao_w,
		dt_vigencia_w
	from	simpro_preco a
	where	a.nr_sequencia	= nr_seq_simpro_preco_p;
	
	/* Se o tipo de altera��o foi "Fora de linha" ou "Suspens�o" */
	if	(ie_tipo_alteracao_w in ('L','S')) then
		update	pls_material
		set	dt_exclusao	= dt_vigencia_w,
			nm_usuario	= 'SIMPRO',
			dt_atualizacao	= sysdate
		where	cd_simpro	= cd_simpro_w;
	/* Se for inclus�o, reativar */
	elsif	(ie_tipo_alteracao_w in ('I')) then
		update	pls_material
		set	dt_inclusao	= dt_vigencia_w,
			dt_exclusao	= null,
			nm_usuario	= 'SIMPRO',
			dt_atualizacao	= sysdate
		where	cd_simpro	= cd_simpro_w
		and	dt_exclusao	is not null;
	end if;
end if;

/* n�o pode ter commit */

end pls_tratar_situacao_mat_simpro;
/