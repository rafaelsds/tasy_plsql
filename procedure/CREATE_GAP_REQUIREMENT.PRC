CREATE OR REPLACE 
PROCEDURE CREATE_GAP_REQUIREMENT (
  nr_seq_gap_p              in LATAM_REQUISITO.NR_SEQ_GAP%type,
  nr_seq_modulo_p           in LATAM_REQUISITO.NR_SEQ_MODULO%type,
  cd_requisito_p            in LATAM_REQUISITO.CD_REQUISITO%type,
  ie_regulatorio_p          in LATAM_REQUISITO.IE_REGULATORIO%type,
  ie_requisito_core_p       in LATAM_REQUISITO.IE_REQUISITO_CORE%type,
  ds_nome_requisito_p       in LATAM_REQUISITO.DS_NOME_REQUISITO%type,
  ie_importancia_desenv_p   in LATAM_REQUISITO.IE_IMPORTANCIA_DESENV%type,
  ie_situacao_p             in LATAM_REQUISITO.IE_SITUACAO%type,
  ie_classificacao_p        in LATAM_REQUISITO.IE_CLASSIFICACAO%type,
  ie_importancia_cliente_p  in LATAM_REQUISITO.IE_IMPORTANCIA_CLIENTE%type,
  cd_responsavel_p          in LATAM_REQUISITO.CD_RESPONSAVEL%type,
  cd_consultor_resp_p       in LATAM_REQUISITO.CD_CONSULTOR_RESP%type,
  ds_problema_solucionar_p  in LATAM_REQUISITO.DS_PROBLEMA_SOLUCIONAR%type,
  ds_processo_atual_p       in LATAM_REQUISITO.DS_PROCESSO_ATUAL%type,
  ds_processo_esperado_p    in LATAM_REQUISITO.DS_PROCESSO_ESPERADO%type,
  ds_impacto_n_impl_p       in LATAM_REQUISITO.DS_IMPACTO_N_IMPL%type,
  ds_requisito_p            in LATAM_REQUISITO.DS_REQUISITO%type,  
  ie_importancia_philips_p  in LATAM_REQUISITO.IE_IMPORTANCIA_PHILIPS%type,
  nr_seq_req_p              out LATAM_REQUISITO.NR_SEQUENCIA%type,
  cod_documento_p           in LATAM_REQUISITO.COD_DOCUMENTO%type default null,
  ds_processo_p             in LATAM_REQUISITO.DS_PROCESSO%type default null,
  ds_sub_processo_p         in LATAM_REQUISITO.DS_SUB_PROCESSO%type default null
) IS

dt_actual_w   CONSTANT LATAM_REQUISITO.DT_ATUALIZACAO%type := SYSDATE;
nm_usuario_w  CONSTANT LATAM_REQUISITO.NM_USUARIO%type     := WHEB_USUARIO_PCK.GET_NM_USUARIO;

BEGIN

  SELECT LATAM_REQUISITO_SEQ.NEXTVAL
  INTO nr_seq_req_p
  FROM DUAL;

  INSERT into LATAM_REQUISITO (
    NR_SEQUENCIA,
    DT_ATUALIZACAO,
    NM_USUARIO,
    DT_ATUALIZACAO_NREC,    
    NM_USUARIO_NREC,
    IE_LIBERAR_DESENV,
    IE_TYPE_REQUIREMENT,
    NR_SEQ_GAP,
    NR_SEQ_MODULO,
    CD_REQUISITO,
    IE_REGULATORIO,
    IE_REQUISITO_CORE,
    DS_NOME_REQUISITO,
    IE_IMPORTANCIA_DESENV,
    IE_SITUACAO,
    IE_CLASSIFICACAO,
    IE_IMPORTANCIA_CLIENTE,
    CD_RESPONSAVEL,
    CD_CONSULTOR_RESP,
    DS_PROBLEMA_SOLUCIONAR,
    DS_PROCESSO_ATUAL,
    DS_PROCESSO_ESPERADO,
    DS_IMPACTO_N_IMPL,
    DS_REQUISITO,
    IE_IMPORTANCIA_PHILIPS,
    NR_SEQ_MILESTONE,
    COD_DOCUMENTO,
    DS_PROCESSO,
    DS_SUB_PROCESSO
  ) VALUES (
    nr_seq_req_p,
    dt_actual_w,
    nm_usuario_w,
    dt_actual_w,
    nm_usuario_w,
    'D',
    'BR',
    nr_seq_gap_p,
    nr_seq_modulo_p,
    cd_requisito_p,
    ie_regulatorio_p,
    ie_requisito_core_p,
    ds_nome_requisito_p,
    ie_importancia_desenv_p,
    ie_situacao_p,
    ie_classificacao_p,
    ie_importancia_cliente_p,
    cd_responsavel_p,
    cd_consultor_resp_p,
    ds_problema_solucionar_p,
    ds_processo_atual_p,
    ds_processo_esperado_p,
    ds_impacto_n_impl_p,
    ds_requisito_p,
    ie_importancia_philips_p,
    10,
    cod_documento_p,
    ds_processo_p,
    ds_sub_processo_p
  );

  COMMIT;
END CREATE_GAP_REQUIREMENT;
/
