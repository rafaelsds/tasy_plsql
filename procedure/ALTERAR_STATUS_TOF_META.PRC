create or replace
procedure alterar_status_tof_meta	(nr_seq_meta_hor_p	number,
									 nm_usuario_p		varchar2) is
									
qt_reg_w				number(10);
qt_diaria_ating_w		number(10);
qt_diaria_nao_ating_w	number(10);
qt_diaria_parcial_w		number(10);
ie_tipo_meta_w			varchar2(3);
ie_status_w				varchar2(1);
ie_status_meta_w		varchar2(1);
dt_geracao_w			date;
nr_seq_meta_atend_w		number(10);
qt_porcentagem_w		number(10,4);
nr_sequencia_w          number(10);									
									
begin

nr_sequencia_w := nr_seq_meta_hor_p;
if (nvl(nr_sequencia_w,0) = 0) then
    Select max(nr_sequencia)
    into nr_sequencia_w
    from tof_meta_atend_hor
	where nm_usuario  = nm_usuario_p;
end if;

Select  max(nr_seq_meta_atend),
		 max(ie_status),
		 max(dt_geracao)
into	nr_seq_meta_atend_w,
		ie_status_meta_w,
		dt_geracao_w
from	tof_meta_atend_hor
where	nr_sequencia = nr_sequencia_w;


select  nvl(max(a.ie_tipo_meta),'X')
into	ie_tipo_meta_w
from	tof_meta a,
		tof_meta_atend_hor b,
		tof_meta_atend c
where	a.nr_sequencia = c.nr_seq_meta
and		b.nr_seq_meta_atend = c.nr_sequencia
and		b.nr_sequencia = nr_sequencia_w;

if ( ie_tipo_meta_w = 'U') then

	Select  count(*)
	into	qt_reg_w
	from	tof_meta_atend_hor 
	where	nr_seq_meta_atend = nr_seq_meta_atend_w
	and		ie_status is not null
	and		dt_geracao > dt_geracao_w;

	If (qt_reg_w = 0) then
	
		update 	tof_meta_atend
		set		ie_status 	= ie_status_meta_w,
				nm_usuario  = nm_usuario_p
		where	nr_sequencia = nr_seq_meta_atend_w;
		
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

	end if;


elsif (ie_tipo_meta_w = 'D') then

	Select  count(*)
	into	qt_reg_w
	from	tof_meta_atend_hor 
	where	nr_seq_meta_atend = nr_seq_meta_atend_w;
	
	Select  count(*)
	into	qt_diaria_ating_w
	from	tof_meta_atend_hor 
	where	nr_seq_meta_atend = nr_seq_meta_atend_w
	and		ie_status = 'A';
	
	Select  count(*)
	into	qt_diaria_nao_ating_w
	from	tof_meta_atend_hor 
	where	nr_seq_meta_atend = nr_seq_meta_atend_w
	and		ie_status = 'N';
	
	Select  count(*)
	into	qt_diaria_parcial_w
	from	tof_meta_atend_hor 
	where	nr_seq_meta_atend = nr_seq_meta_atend_w
	and		ie_status = 'P';
	
	if	(qt_diaria_nao_ating_w > 0) and
		(qt_reg_w = qt_diaria_nao_ating_w) then
	
		ie_status_w := 'N';
	
	elsif	(qt_diaria_nao_ating_w > 0) then
	
		qt_porcentagem_w := abs(qt_reg_w/qt_diaria_nao_ating_w);
		
		if	(qt_porcentagem_w < 5) then
		
			ie_status_w := 'P';
		
		else
	
			ie_status_w := 'N';
		
		end if;
		
	elsif	(qt_diaria_parcial_w > 0) then
	
		ie_status_w := 'P';
		
	elsif	(qt_reg_w <=  qt_diaria_ating_w) then
	
		ie_status_w := 'A';
	
	end if;
	
	if	(ie_status_w  is not null) then
	
		update 	tof_meta_atend
		set		ie_status 	= ie_status_w,
				nm_usuario  = nm_usuario_p
		where	nr_sequencia = nr_seq_meta_atend_w;
		
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
				
	
	end if;
	
end if;



end alterar_status_tof_meta;
/									
