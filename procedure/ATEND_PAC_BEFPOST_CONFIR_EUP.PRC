create or replace
procedure atend_pac_befpost_confir_eup(
					cd_pessoa_responsavel_p	varchar2,
					cd_pessoa_fisica_p	varchar2,
					ie_exige_cpf_resp_w	varchar2,
					nm_usuario_p		Varchar2,
					ds_msg_erro_p	out 	varchar2,
					ds_msg_alerta_cpf_resp_p out varchar2) is 

ds_msg_erro_w	varchar(255);	
ds_msg_alerta_cpf_resp_w	varchar2(255);
ie_continuar_execucao_w	varchar2(1) := 'S';
ie_possui_cpf_w		varchar2(1);				
					
begin

if	(ie_continuar_execucao_w = 'S') and
	(ie_exige_cpf_resp_w <> 'N') and
	(cd_pessoa_responsavel_p is null) and
	(cd_pessoa_fisica_p is not null) then
	
	select	decode(count(*),0,'N','S')
	into	ie_possui_cpf_w
	from	pessoa_fisica
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	nr_cpf is not null;
	
	if	(ie_possui_cpf_w = 'N') then
		if	(ie_exige_cpf_resp_w = 'S') then
			ds_msg_erro_w := substr(obter_texto_tasy(196695, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			ie_continuar_execucao_w := 'N';
		else
			ds_msg_alerta_cpf_resp_w := substr(obter_texto_tasy(196695, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			ie_continuar_execucao_w := 'S';
		end if;
	end if;
end if;

ds_msg_alerta_cpf_resp_p := ds_msg_alerta_cpf_resp_w;
ds_msg_erro_p := ds_msg_erro_w;


commit;

end atend_pac_befpost_confir_eup;
/