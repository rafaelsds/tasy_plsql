create or replace
procedure Aghos_solicitacao_internacao(	nr_atendimento_p	number,					
					nr_seq_laudo_p 		number,
					nm_usuario_p		Varchar2,
					cd_medico_p		varchar2 default null,
					cd_cid_principal_p	varchar2 default null,
					cd_procedimento_p	number default null,
					nr_seq_aghos_erro_p	number default null) is


cd_pessoa_fisica_w		varchar2(10);
cd_estabelecimento_w		number(10);

nr_seq_internacao_aghos_w 	number := 0;		
ie_status_w 			varchar2(1) := 'A';
ds_erro_w 			varchar2(500);
dt_integracao_w			date := sysdate;
ds_usuario_w 			varchar2(15);
cd_cnes_solicitante_w 		number(7);
nr_prontuario_w			number(9);
ds_nome_paciente_w 		varchar2(60);
--dt_nascimento_w			date;
dt_nascimento_w			varchar2(255);
ie_sexo_w 			number(2);
ie_estado_civil_w 		number(2);
ds_nome_mae_w 			varchar2(60);
ds_nome_pai_w 			varchar2(60);
cd_cep_w 			number(8);
nr_endereco_w			number(10);
ds_complemento_w		varchar2(100);
cd_ocupacao_cbo_w		varchar2(6);
ie_cor_raca_w 			number(2);
cd_naturalidade_w		number(7);
cd_nacionalidade_w 		number(5);
nr_identidade_w			varchar2(20);
nr_cpf_w			number(11);
nr_cartao_sus_w			number(15);
nr_internacao_w			number(9);
ie_prioridade_w			number(2);
nr_cpf_medico_solicitante_w 	number(11);
cd_procedimento_unificado_w 	number(10);
cd_cid_w			varchar2(4);
ds_laudo_sinais_sintomas_w 	varchar2(1000);
ds_laudo_justificativa_w	varchar2(1000);
ds_laudo_result_diagnostico_w 	varchar2(1000);
ie_uti_w 			number(1);
ds_prontuario_solicitante_w 	varchar2(11);

ds_erro_ww			varchar2(2000);

ds_sep_bv_w			varchar2(50);
ds_comando_w			varchar2(2000);
ie_tipo_atendimento_w		number(3);
nr_atendimento_mae_w		number(10);

nr_internacao_agenda_w		number(9);
nr_seq_solicitacao_w		number(10);
ie_autorizacao_automatica_w	number(1);
ie_situacao_w			varchar2(2);

nr_seq_atepacu_w		number(10);
nr_seq_regra_proc_w		number(10);

pragma autonomous_transaction;		
begin

/*  tabela      GSH_I_INTERN_SOLICI_ENT

IDF_GSH_I_INTERN_SOLICI_ENT	NUMBER			not null 	(PK)
TIP_STATUS 			VARCHAR2(1)		not null	A � Aguardando (registro aguardando ser integrado) / P � Processado (registro j� integrado)   / E � Erro (erro na integra��o)
DES_ERRO 			VARCHAR2(500)		null
DAT_INTEGRACAO 		DATE			not null
DES_USUARIO 			VARCHAR2(15)		not null
COD_CNES_SOLICITANTE 		NUMBER(7)		not null
IDF_PRONTUARIO 		NUMBER(9) 		null
DES_NOME_PACIENTE 		VARCHAR2(60) 		not null
DAT_NASCIMENTO 		DATE 			not null
IDF_SEXO 			NUMBER(2) 		not null     1 - M / 2 - F / 3 - Ambos
IDF_ESTADO_CIVIL 		NUMBER(2) 		null	1 - solteiro / 2 - casado / 3 - divorciado / 4 - vi�va (o)
DES_NOME_MAE 			VARCHAR2(60) 		not null
DES_NOME_PAI 			VARCHAR2(60) 		null
COD_CEP 			NUMBER(8) 		not null
NUM_ENDERECO			NUMBER(10) 		not null
DES_COMPLEMENTO 		VARCHAR2(100) 		null
COD_OCUPACAO 			VARCHAR2(6) 		null
IDF_COR_RACA 			NUMBER(2) 		not null	0 - nulo / 1 - branca / 2 - negra /  3 - amarela / 4 - parda / 5 � ind�gena
COD_NATURALIDADE		NUMBER(7) 		null
COD_NACIONALIDADE 		NUMBER(5) 		null
NUM_IDENTIDADE 		VARCHAR2(20) 		null
NUM_CPF 			NUMBER(11) 		null
NUM_CARTAO_SUS		NUMBER(15) 		null
IDF_INTERNACAO 		NUMBER(9) 		null	retorno do aghos
IDF_PRIORIDADE 			NUMBER(2) 		not null	1 - urg�ncia / 2 - emerg�ncia / 3 - eletiva
NUM_CPF_MEDICO_SOLICITANTE 	NUMBER(11) 		not null
COD_PROCEDIMENTO_UNIFICADO 	NUMBER(10) 		not null
COD_CID 			VARCHAR2(4) 		not null
DES_LAUDO_SINAIS_SINTOMAS 	VARCHAR2(1000) 		not null
DES_LAUDO_JUSTIFICATIVA 	VARCHAR2(1000) 		not null
DES_LAUDO_RESULT_DIAGNOSTICO 	VARCHAR2(1000) 		not null
TIP_UTI 				NUMBER(1) 		not null
DES_PRONTUARIO_SOLICITANTE 	VARCHAR2(11) 		null
*/

select	maX(cd_pessoa_fisica),
	max(cd_estabelecimento),
	max(decode(ie_tipo_atendimento, 3, 1,  	-- Tipo atend. Pronto Socorro = Prioridade "urg�ncia"
					7, 3,  	-- Tipo atend. Externo = Prioridade "eletiva"
					8, 3,  	-- Tipo atend. Ambulatorial = Prioridade "eletiva"
					1, 1)), -- Tipo atend. Internado = Prioridade "urg�ncia"
	max(substr(obter_cnes_estab(cd_estabelecimento), 1, 7)),
	max(ie_tipo_atendimento),
	max(nr_atendimento_mae),
	nvl(max(substr(obter_dados_param_atend(a.cd_estabelecimento, 'US'), 1, 15)), 'TREINA')
into	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	ie_prioridade_w,
	cd_cnes_solicitante_w,
	ie_tipo_atendimento_w,
	nr_atendimento_mae_w,
	ds_usuario_w
from	atendimento_paciente a
where	nr_atendimento = nr_atendimento_p;

select	max(nvl(cd_procedimento_p, cd_procedimento_solic))
into	cd_procedimento_unificado_w
from	sus_laudo_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_interno = nr_seq_laudo_p;

select	max(nr_sequencia)
into	nr_seq_regra_proc_w
from	proc_internacao_aghos
where	cd_procedimento = cd_procedimento_unificado_w;

If	(ie_tipo_atendimento_w <> 1) or 
	(nr_seq_regra_proc_w is not null) or 
	(nr_atendimento_mae_w is not null) then
	
	select	max(substr(obter_cpf_pessoa_fisica(nvl(cd_medico_p, cd_medico_requisitante)), 1, 11)),
		max(nvl(cd_procedimento_p, cd_procedimento_solic)),
		max(nvl(cd_cid_principal_p, cd_cid_principal)),
		max(ds_sinal_sintoma),
		max(ds_condicao_justifica),
		max(ds_result_prova)
	into	nr_cpf_medico_solicitante_w,
		cd_procedimento_unificado_w,
		cd_cid_w,
		ds_laudo_sinais_sintomas_w,	
		ds_laudo_justificativa_w,		
		ds_laudo_result_diagnostico_w
	from	sus_laudo_paciente
	where	nr_atendimento = nr_atendimento_p
	and	nr_seq_interno = nr_seq_laudo_p;
					
	select	max(to_char(dt_nascimento,'dd/mm/yyyy')),
		max(decode(ie_sexo, 'M', 1, 'F', 2, 'I', 3)),
		max(decode(ie_estado_civil, 1, 1,  -- Tasy: Solteiro / Aghos: Solteiro
					2, 2,  -- Tasy: Casado / Aghos: Casado
					3, 3,  -- Tasy: Divorciado / Aghos: Divorciado
					4, 3,  -- Tasy: Desquitado / Aghos: Divorciado
					5, 4,  -- Tasy: Vi�vo / Aghos: Vi�vo
					6, 3,  -- Tasy: Separado / Aghos: Divorciado
					7, 1,  -- Tasy: Concubinado / Aghos: Solteiro
					8, 1)), -- Tasy: Outros / Aghos: Solteiro
		max(nm_pessoa_fisica),
		max(substr(obter_nome_mae_pf(cd_pessoa_fisica), 1, 60)),
		max(substr(obter_nome_pai_mae(cd_pessoa_fisica, 'P'), 1, 60)),
		max(substr(obter_compl_pf(cd_pessoa_fisica, 1, 'CEP'), 1, 8)),
		max(substr(obter_compl_pf(cd_pessoa_fisica, 1, 'NR'), 1, 10)),
		max(substr(obter_compl_pf(cd_pessoa_fisica, 1, 'CO'), 1, 60)),
		max(substr(obter_dados_pf(cd_pessoa_fisica, 'CBOS'), 1, 6)),
		max(substr(decode(sus_obter_cor_pele(cd_pessoa_fisica, 'C'), 99, 0, sus_obter_cor_pele(cd_pessoa_fisica, 'C')), 1, 2)),
		max(substr(obter_compl_pf(cd_pessoa_fisica, 1, 'CDM'), 1, 7)),
		max(substr(obter_compl_pf(cd_pessoa_fisica, 1, 'CBPAIS'), 1, 5)),
		max(substr(obter_dados_pf(cd_pessoa_fisica, 'RG'), 1, 20)),
		max(substr(obter_cpf_pessoa_fisica(cd_pessoa_fisica), 1, 11)),
		max(substr(nr_cartao_nac_sus, 1, 15)),
		max(substr(obter_prontuario_pf(cd_pessoa_fisica, cd_estabelecimento_w), 1, 9))
	into	dt_nascimento_w,
		ie_sexo_w,
		ie_estado_civil_w,
		ds_nome_paciente_w,
		ds_nome_mae_w,
		ds_nome_pai_w,
		cd_cep_w,
		nr_endereco_w,
		ds_complemento_w, 
		cd_ocupacao_cbo_w, 		
		ie_cor_raca_w,			
		cd_naturalidade_w,		
		cd_nacionalidade_w,		
		nr_identidade_w,		
		nr_cpf_w,			
		nr_cartao_sus_w,				
		ds_prontuario_solicitante_w	
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;

	--rollback;
	--commit;
	
	gsh_i_prc_intern_solici_ent@tasy_aghos(	0,
						'A',
						ds_erro_ww, 
						sysdate, 
						ds_usuario_w, 
						cd_cnes_solicitante_w,
						nr_prontuario_w, 
						ds_nome_paciente_w, 
						dt_nascimento_w, 
						ie_sexo_w, 
						ie_estado_civil_w,
						ds_nome_mae_w, 
						ds_nome_pai_w, 
						cd_cep_w, 
						nr_endereco_w,
						ds_complemento_w, 
						cd_ocupacao_cbo_w, 
						ie_cor_raca_w, 
						cd_naturalidade_w, 
						cd_nacionalidade_w, 
						nr_identidade_w,  
						nr_cpf_w,  
						nr_cartao_sus_w, 
						nr_internacao_w, 
						ie_prioridade_w, 
						nr_cpf_medico_solicitante_w,  
						cd_procedimento_unificado_w,
						cd_cid_w,
						ds_laudo_sinais_sintomas_w,
						ds_laudo_justificativa_w,
						ds_laudo_result_diagnostico_w, 
						0,
						ds_prontuario_solicitante_w);

	ds_sep_bv_w  := obter_separador_bv;					
	ds_comando_w :=	'declare '||
		'solici_sai gsh_i_pkg_integra_sai.solici_sai@tasy_aghos; '||
		'ds_erro_ww varchar2(500); '||
		'cd_cgc_w varchar2(14); '||
		'nr_seq_solicitacao_w number(10); '||			
		'begin '||
		' '||	
		'gsh_i_pkg_integra_sai.gsh_i_prc_intern_solici_sai@tasy_aghos(solici_sai); '||
		' '||
		'if (solici_sai.first is not null) then '||
		'for i in solici_sai.first..solici_sai.last loop '||
		'begin '||
		'select nr_sequencia '||
		'into nr_seq_solicitacao_w '||
		'from solicitacao_tasy_aghos '||
		'where nr_internacao = solici_sai(i).idf_internacao '||
		'and rownum = 1; '||
		'exception '||
		'when no_data_found then '||
		'nr_seq_solicitacao_w := 0; '||
		'end; '||
		' '||
		'if (nr_seq_solicitacao_w = 0) and '||
		'nvl(:dt_nascimento_w,to_char(solici_sai(i).dat_nascimento,''dd/mm/yyyy'')) = to_char(solici_sai(i).dat_nascimento,''dd/mm/yyyy'') and '||
		'((solici_sai(i).des_nome_paciente = :nm_paciente_p) or '||
		'(nvl(solici_sai(i).num_cpf, 999) = :nr_cpf_p) or '||
		'(nvl(solici_sai(i).des_prontuario_solicitante, ''WW'') = :nr_prontuario_p) or '||
		'(nvl(solici_sai(i).num_cartao_sus, 999) = :nr_cartao_sus_p)) then '||
		' '||
		'insert 	into solicitacao_tasy_aghos ( '||
		'nr_sequencia, '||
		'dt_atualizacao, '||
		'nm_usuario, '||
		'dt_atualizacao_nrec, '||
		'nm_usuario_nrec, '||  
		'nr_internacao, '||    
		'ie_urgencia, '||   
		'nr_seq_laudo, '||
		'nr_atend_original, '||
		'ie_situacao , '||
		'ie_motivo_rejeicao, '|| 
		'nr_atendimento, '||
		'ie_erro, '||         
		'ds_erro, '||
		'ds_motivo_situacao, '||
		'cd_cgc, '||
		'nm_paciente, '||
		'ie_autorizacao_automatica, '||
		'cd_pessoa_fisica) '||
		'values (solicitacao_tasy_aghos_seq.nextval, '||
		'nvl(solici_sai(i).dat_integracao,sysdate), '||
		':nm_usuario_p, '||
		'nvl(solici_sai(i).dat_integracao,sysdate), '||
		':nm_usuario_p, '||
		'solici_sai(i).idf_internacao, '||
		'solici_sai(i).idf_prioridade, '||
		':nr_seq_laudo_p, '||
		':nr_atendimento_p, '||
		'''A'', '||
		'null, '||
		'null, '||
		'decode(solici_sai(i).des_erro, null, ''N'', ''S''), '||
		'solici_sai(i).des_erro, '||
		'''Solicita��o enviada ao Aghos'', '||
		'null, '||
		'solici_sai(i).des_nome_paciente, '||
		'solici_sai(i).tip_autorizacao_automatica, '||
		':cd_pessoa_fisica_p); '||
		' '||		
		'gsh_i_prc_intern_solici_s_u@tasy_aghos(solici_sai(i).idf_gsh_i_intern_solici_sai,''P'', ds_erro_ww);'||		
		' '||
		'end if; '||
		'end loop; '||
		'end if; '||			
		'end;';

	exec_sql_dinamico_bv('AGHOS', ds_comando_w,	'nr_seq_laudo_p=' 		|| nr_seq_laudo_p 				|| ds_sep_bv_w ||
							'nr_atendimento_p=' 		|| nr_atendimento_p 				|| ds_sep_bv_w ||
							'nm_usuario_p=' 		|| nm_usuario_p 				|| ds_sep_bv_w ||
							--'cd_cnes_solicitante_p='	|| cd_cnes_solicitante_w			|| ds_sep_bv_w ||
							'nm_paciente_p='		|| ds_nome_paciente_w				|| ds_sep_bv_w ||
							'nr_cpf_p='			|| nvl(nr_cpf_w, 0)				|| ds_sep_bv_w ||
							'nr_prontuario_p='		|| nvl(ds_prontuario_solicitante_w, 'X')	|| ds_sep_bv_w ||
							'nr_cartao_sus_p='		|| nvl(nr_cartao_sus_w, 0)			|| ds_sep_bv_w ||
							'dt_nascimento_w='		|| dt_nascimento_w				|| ds_sep_bv_w ||
							'cd_pessoa_fisica_p='		|| cd_pessoa_fisica_w);

	/* Autoriza��o autom�tica */
	
	select	max(nr_sequencia),
		max(ie_autorizacao_automatica),
		max(nr_internacao)
	into	nr_seq_solicitacao_w,
		ie_autorizacao_automatica_w,
		nr_internacao_agenda_w
	from	solicitacao_tasy_aghos
	where	nr_atend_original = nr_atendimento_p;
	
	If	(nr_seq_solicitacao_w is not null) and
		(nvl(ie_autorizacao_automatica_w, 0) > 0) then
		
		If	(ie_prioridade_w = 3) then -- Eletiva

			ds_comando_w :=	'declare '||
					'	autcli_sai gsh_i_pkg_integra_sai.autcli_sai@tasy_aghos; '||
					'	ds_erro_ww	varchar2(500); '||
					'begin '||
					'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_autcli_sai@tasy_aghos(autcli_sai); '||
					' '||
					'	if	(autcli_sai.first is not null) then '||
					'		for i in autcli_sai.first..autcli_sai.last loop '||
					' '||
					'			if	(autcli_sai(i).idf_internacao = :nr_internacao) then '||
					' '||
					'				update	solicitacao_tasy_aghos '||
					'				set	ie_situacao = ''AC'', '||
					'					ds_motivo_situacao = ''Solicita��o autorizada clinicamente no Aghos'' '||
					'				where	nr_internacao = :nr_internacao; '||
					' '||
					'				gsh_i_prc_intern_autcli_s_u@tasy_aghos(	autcli_sai(i).idf_gsh_i_intern_autcli_sai, '||
					'									''P'', '||
					'									ds_erro_ww); '||			
					' '||
					'				commit; '||
					' '||
					'			end if; '||
					'		end loop; '||
					'	end if; '||
					'end;';
						
			exec_sql_dinamico_bv('AGHOS', ds_comando_w, 'nr_internacao='|| nr_internacao_agenda_w);

		End if;		
	End if;	
End if;

If	(nr_seq_aghos_erro_p is not null) then
	update	solicitacao_aghos_erro
	set	ie_gerado = 'N'
	where	nr_sequencia = nr_seq_aghos_erro_p;
end if;

commit;

end Aghos_solicitacao_internacao;
/
