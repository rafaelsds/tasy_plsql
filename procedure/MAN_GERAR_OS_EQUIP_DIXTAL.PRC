create or replace
procedure man_gerar_os_equip_dixtal(	nr_serie_equip_p		in	varchar2,
				nr_id_os_dixtal_p		in	number,
				ds_dano_breve_p		in	varchar2,
				ds_dano_p		in	varchar2,
				ds_versao_soft_p		in	varchar2,
				nr_serie_sec_equip_p	in	varchar2,
				ds_versao_sec_soft_p	in	varchar2,
				nm_usuario_p		in	Varchar2,
				nr_sequencia_p		out	number,
				ds_erro_p			out	varchar2) is 

nr_seq_equipamento_w	number(10,0);
nr_grupo_planejamento_w	number(10,0);
nr_grupo_trabalho_w	number(10,0);
nr_seq_estagio_w		number(10,0);
nr_seq_localizacao_w	number(10,0);
cd_pessoa_solicitante_w	varchar2(10);

begin

if	(nvl(nr_serie_equip_p, '0') <> '0') then
	ds_erro_p := '';
	
	select	max(nr_sequencia)
	into	nr_seq_equipamento_w
	from	man_equipamento
	where	nr_serie = nr_serie_equip_p;
	
	if	(nvl(nr_seq_equipamento_w,0) > 0) then
		select	nr_seq_planej,
			nr_seq_trab,
			nr_seq_estagio_inicio_os,
			nr_seq_local
		into	nr_grupo_planejamento_w,
			nr_grupo_trabalho_w,
			nr_seq_estagio_w,
			nr_seq_localizacao_w
		from	man_equipamento
		where	nr_sequencia = nr_seq_equipamento_w;
	
		begin
		select	cd_pessoa_fisica
		into	cd_pessoa_solicitante_w
		from	usuario
		where	nm_usuario = nm_usuario_p;
		exception
		when others then
			cd_pessoa_solicitante_w := '';
		end;

		if	(nvl(cd_pessoa_solicitante_w,'X') = 'X') then
			ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(277219) || nm_usuario_p;
		end if;
		
		if	(nvl(ds_erro_p,'X') = 'X') then
			begin
			select	man_ordem_servico_seq.nextval
			into	nr_sequencia_p
			from	dual;
			
			insert	into man_ordem_servico(
				nr_sequencia,
				nr_seq_localizacao,
				nr_seq_equipamento,
				cd_pessoa_solicitante,
				dt_ordem_servico,
				ie_prioridade,
				ie_parado,
				ds_dano_breve,
				dt_atualizacao,
				nm_usuario,
				dt_inicio_desejado,
				dt_conclusao_desejada,
				ds_dano,
				dt_inicio_previsto,
				dt_fim_previsto,
				dt_inicio_real,
				dt_fim_real,
				ie_tipo_ordem,
				ie_status_ordem,
				nr_grupo_planej,
				nr_grupo_trabalho,
				nr_seq_estagio,
				ie_classificacao,
				ie_forma_receb,
				nm_usuario_nrec,
				ie_origem_os,
				cd_externo_os)
			values(	nr_sequencia_p,				-- nr_sequencia,
				nr_seq_localizacao_w,			-- nr_seq_localizacao,
				nr_seq_equipamento_w,			-- nr_seq_equipamento,
				cd_pessoa_solicitante_w,			-- cd_pessoa_solicitante,
				sysdate,					-- dt_ordem_servico,
				'M',					-- ie_prioridade,
				'N',					-- ie_parado,
				substr(ds_dano_breve_p,1,80),		-- ds_dano_breve,
				sysdate,					-- dt_atualizacao,
				nm_usuario_p,				-- nm_usuario,
				sysdate,					-- dt_inicio_desejado,
				(sysdate + 15),				-- dt_conclusao_desejada,
				ds_dano_p,				-- ds_dano,
				sysdate,					-- dt_inicio_previsto,
				(sysdate + 15),				-- dt_fim_previsto,
				sysdate,					-- dt_inicio_real,
				null,					-- dt_fim_real,
				'1',					-- ie_tipo_ordem,
				'1',					-- ie_status_ordem,
				nr_grupo_planejamento_w,			-- nr_grupo_planej,
				nr_grupo_trabalho_w,			-- nr_grupo_trabalho,
				nr_seq_estagio_w,				-- nr_seq_estagio,
				'S',					-- ie_classificacao,
				'I',					-- ie_forma_receb,
				nm_usuario_p,				-- nm_usuario_nrec,
				4,					--ie_origem_os
				nr_id_os_dixtal_p);
			commit;
			end;
		end if;
	else
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(277220);
	end if;
	
end if;

end man_gerar_os_equip_dixtal;
/
