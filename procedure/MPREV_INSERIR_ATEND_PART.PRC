create or replace
procedure mprev_inserir_atend_part(	nr_seq_atendimento_p	number,
									cd_profissional_p		varchar2,
									nr_seq_participante_p	number,
									nr_seq_regra_eup_p		number, /* data de execu��o informada no wdlg */
									nm_usuario_p			varchar,
									nr_seq_segurado_p		pls_segurado.nr_sequencia%type) is
						
nr_seq_atend_part_w		mprev_atend_paciente.nr_sequencia%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_atend_eup_w			atendimento_paciente.nr_atendimento%type	:= null;
nr_seq_regra_event_w	mprev_regra_evento_atend.nr_sequencia%type;
nr_seq_tipo_evento_w	mprev_regra_evento_atend.nr_seq_tipo_evento%type;
cd_procedimento_w		mprev_regra_evento_atend.cd_procedimento%type;
ie_origem_proced_w		mprev_regra_evento_atend.ie_origem_proced%type;
cd_profissional_w		mprev_atendimento_evento.cd_profissional%type;
nr_seq_regra_eup_w		number(10);	
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
		
begin

select	mprev_atend_paciente_seq.nextval
into	nr_seq_atend_part_w
from	dual;

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	mprev_participante a
where	a.nr_sequencia	= nr_seq_participante_p;
	
insert into mprev_atend_paciente
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_atendimento,
	nr_seq_participante,
	nr_seq_mprev_regra_atend)
values	(nr_seq_atend_part_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_atendimento_p,
	nr_seq_participante_p,
	nr_seq_regra_eup_p); 

if	(nr_seq_segurado_p is null) then
	nr_seq_segurado_w := mprev_obter_benef_partic(nr_seq_participante_p);
else
	nr_seq_segurado_w := nr_seq_segurado_p;
end if;
	
/*Busca a regra quando o atendimento for gerado pela HDM - Atendimento, nesse caso � individual ou coletivo.*/
if	(nvl(nr_seq_regra_eup_p,0) = 0) then
	nr_seq_regra_eup_w := mprev_obter_regra_ger_atend(nr_seq_atendimento_p, nr_seq_segurado_w);
else
	nr_seq_regra_eup_w := nr_seq_regra_eup_p;
end if;
	
if	(nr_seq_regra_eup_w is not null) then
	mprev_inserir_atend_eup(nr_seq_atendimento_p,nr_seq_regra_eup_w, cd_pessoa_fisica_w, nm_usuario_p, nr_seq_segurado_w, nr_atend_eup_w);
end if;

nr_seq_regra_event_w := mprev_obter_regra_evento_atend(nr_seq_atend_part_w);

if	(nr_seq_regra_event_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(293212);
else
	select	max(nr_seq_tipo_evento),
			max(cd_procedimento),
			max(ie_origem_proced)
	into	nr_seq_tipo_evento_w,
			cd_procedimento_w,
			ie_origem_proced_w
	from	mprev_regra_evento_atend
	where	nr_sequencia = nr_seq_regra_event_w;
	
	if	(cd_profissional_p is not null) then
		cd_profissional_w	:= cd_profissional_p;
	else
		select	max(cd_profissional)
		into	cd_profissional_w
		from	mprev_atendimento a
		where	a.nr_sequencia	= nr_seq_atendimento_p;
	end if;
	
	insert into mprev_atendimento_evento
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_inicio_evento,
		cd_procedimento,
		ie_origem_proced,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		--nr_seq_atend_paciente,
		nr_seq_tipo_evento,
		cd_profissional,
		ie_prof_adic,
		nr_seq_atendimento,
		nr_seq_atend_paciente)
	values	(mprev_atendimento_evento_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_procedimento_w,
		ie_origem_proced_w,
		sysdate,
		nm_usuario_p,
		--nr_seq_atend_part_w,
		nr_seq_tipo_evento_w,
		cd_profissional_w,
		'N',
		nr_seq_atendimento_p,
		nr_seq_atend_part_w);
end if;

update	mprev_atend_paciente
set	nr_atendimento	= nr_atend_eup_w
where	nr_sequencia	= nr_seq_atend_part_w;
	
end mprev_inserir_atend_part;
/