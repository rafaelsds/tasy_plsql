create or replace procedure atualizar_dados_item(nr_seq_item_p in number,
						nr_seq_protocolo_p in number,
						nr_interno_conta_p in number,
						cd_autorizacao_p in varchar2) is
begin

	if (nr_seq_item_p is not null) and (nr_seq_item_p > 0) then
		update	imp_dem_retorno_guia
		set	nr_interno_conta 	= nvl(nr_interno_conta_p, nr_interno_conta),
			cd_autorizacao 		= nvl(cd_autorizacao_p,0)
		where	nr_sequencia 		= 	(select 	b.nr_seq_dem_ret_guia
							from 		imp_dem_ret_item b
							where 		b.nr_sequencia = nr_seq_item_p);
		
		update	imp_dem_retorno_prot
		set	nr_seq_protocolo 	= nvl(nr_seq_protocolo_p, nr_seq_protocolo)
		where	nr_sequencia		=	(select		a.nr_seq_dem_ret_prot
							from		imp_dem_retorno_guia a
							where		a.nr_sequencia	= 	(select	b.nr_seq_dem_ret_guia
												from	imp_dem_ret_item b
												where 	b.nr_sequencia = nr_seq_item_p));
		commit;
	end if;

end atualizar_dados_item;
/