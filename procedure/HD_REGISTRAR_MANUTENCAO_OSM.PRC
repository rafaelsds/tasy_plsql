create or replace
procedure HD_registrar_manutencao_osm(	nr_seq_osmose_p		number,
					dt_manutencao_p			date,
					cd_pessoa_responsavel_p		varchar2,
					nr_seq_motivo_manutencao_p	number,
					ds_manutencao_p			varchar2,
					nm_usuario_p			varchar2) is

cd_setor_atendimento_w	number(5,0);
cd_pessoa_fisica_w	varchar2(10);

begin

if 	(nvl(nr_seq_motivo_manutencao_p,0) = 0) then

	Wheb_mensagem_pck.exibir_mensagem_abort(264167);

elsif 	(nvl(nr_seq_osmose_p,0) > 0) and 
	(cd_pessoa_responsavel_p is not null) then

	insert into hd_osmose_manutencao
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_responsavel,
		dt_registro,
		ds_observacao,
		nr_seq_osmose,
		nr_seq_mot_manutencao)
	values	(hd_osmose_manutencao_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_responsavel_p,
		dt_manutencao_p,
		ds_manutencao_p,
		nr_seq_osmose_p,
		nr_seq_motivo_manutencao_p);


end if;

commit;

end HD_registrar_manutencao_osm;
/
