create or replace procedure add_tc_itens( nr_seq_pendency	number,
			nr_seq_ct_p	number,
			nm_usuario_p	varchar2,
			ie_duplicate_all_p	varchar2) is 

ds_passo_w	varchar2(4000);
ds_resultado_w	varchar2(4000);
qt_count_w	number(10);
nr_seq_item_w	number(10);

dt_inicio_plano_w	reg_plano_teste_controle.dt_inicio_plano%type;
dt_fim_plano_w		reg_plano_teste_controle.dt_fim_plano%type;

nr_seq_ciclo_w	reg_tc_evidence_item.nr_seq_ciclo%type;

cursor c01 is
	select	ds_passo,
		ds_resultado,
		a.nr_sequencia
	from	reg_acao_teste a,
			reg_caso_teste b
	where	b.nr_sequencia = a.nr_seq_caso_teste
	and		nr_seq_caso_teste = nr_seq_ct_p
	and		nvl(a.ie_situacao, 'A') = 'A'
	and	(coalesce(b.ie_situacao, 'A') <> 'I'
		or	b.dt_inativacao between dt_inicio_plano_w and dt_fim_plano_w)
	order by ds_passo;
	
cursor c_test_case_items is
	select	ev.nr_sequencia,
		ev.dt_atualizacao,
		ev.nm_usuario,
		ev.dt_atualizacao_nrec,
		ev.nm_usuario_nrec,
		ev.nr_seq_ect,
		ac.ds_passo ds_item_description,
		ac.ds_resultado ds_result,
		ev.dt_execution,
		ev.nm_user_execution,
		ev.ie_result,		
		ev.nr_seq_ciclo,
		ev.nr_seq_tc_item
	from	reg_tc_evidence_item ev,
			reg_acao_teste ac,
			reg_caso_teste ct
	where	ac.nr_seq_caso_teste = ct.nr_sequencia	
	and		ev.nr_seq_ect = nr_seq_pendency
	and	(coalesce(ct.ie_situacao, 'A') <> 'I'
		or	ct.dt_inativacao between dt_inicio_plano_w and dt_fim_plano_w)
	and		nvl(ac.ie_situacao, 'A') = 'A'	
	and		ev.nr_seq_ciclo = nr_seq_ciclo_w
	and		ev.nr_seq_tc_item = ac.nr_sequencia;

begin
	select	count(nr_sequencia)
	into	qt_count_w
	from	reg_tc_evidence_item
	where	nr_seq_ect = nr_seq_pendency;
	
	
	select	max(rptc.dt_inicio_plano),
		max(coalesce(rptc.dt_fim_plano, sysdate))
	into	dt_inicio_plano_w,
		dt_fim_plano_w
	from	reg_plano_teste_controle rptc,
		reg_tc_pendencies rtp
	where	rptc.nr_sequencia = rtp.nr_seq_controle_plano
	and	rtp.nr_sequencia = nr_seq_pendency;
	
	if (qt_count_w > 0) then
		
		select	count(nr_sequencia)
		into	qt_count_w
		from	reg_tc_evidence_item
		where	nr_seq_ect = nr_seq_pendency
		and	ie_result is null;
		
		if (qt_count_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(836886);
		end if;
		
		select	max(nr_seq_ciclo)
		into	nr_seq_ciclo_w
		from	reg_tc_evidence_item
		where	nr_seq_ect = nr_seq_pendency;
					
		for	tc_item in c_test_case_items
		loop
			if	ie_duplicate_all_p = 'S' or tc_item.ie_result <> 'P' then
				insert into reg_tc_evidence_item (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_ect,
					ds_item_description,
					ds_result,
					nr_seq_ciclo,
					nr_seq_tc_item)
				values(	reg_tc_evidence_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					tc_item.nr_seq_ect,
					tc_item.ds_item_description,
					tc_item.ds_result,
					tc_item.nr_seq_ciclo+1,
					tc_item.nr_seq_tc_item);
			end if;
		end loop;
	else
		open c01;
		loop
		fetch c01 into	
			ds_passo_w,
			ds_resultado_w,
			nr_seq_item_w;
		exit when c01%notfound;
			begin
				insert into reg_tc_evidence_item (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_ect,
					ds_item_description,
					ds_result,
					nr_seq_ciclo,
					nr_seq_tc_item)
				values(	reg_tc_evidence_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_pendency,
					ds_passo_w,
					ds_resultado_w,
					1,
					nr_seq_item_w);
			end;
		end loop;
		close c01;
	end if;

commit;

end add_tc_itens;
/
