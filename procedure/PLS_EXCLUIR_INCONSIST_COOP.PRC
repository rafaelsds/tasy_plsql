create or replace
procedure pls_excluir_inconsist_coop
			(	nr_seq_import_p		number,
				nm_usuario_p		varchar2) is 
				
nr_seq_cad_inconsist_w			number(10);
nr_seq_import_w				number(10);

Cursor C01 is
	select	b.nr_sequencia,
		a.nr_sequencia
	from	pls_cad_unimed_inconsist	b,
		pls_w_import_cad_unimed		a
	where	a.nr_sequencia	= b.nr_seq_cad_unimed
	and	a.nr_sequencia	= nr_seq_import_p
	and	b.ie_tipo_inconsistencia = 3;
begin

open c01;
loop
fetch c01 into	
	nr_seq_cad_inconsist_w,
	nr_seq_import_w;
exit when c01%notfound;
	begin
	delete from pls_cad_unimed_inconsist where nr_sequencia = nr_seq_cad_inconsist_w;
	end;
end loop;
close c01;

update	pls_w_import_cad_unimed
set	dt_atualizacao_cadastro = sysdate,
	ie_tipo_inconsistencia = null
where	nr_sequencia	= nr_seq_import_w;

commit;

end pls_excluir_inconsist_coop;
/