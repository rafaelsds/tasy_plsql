create or replace procedure consistir_status_solucao_adep	(ie_tipo_solucao_p	number,
					nr_prescricao_p		number,
					nr_seq_solucao_p	number,
					ie_consiste_em_adm_p	varchar2,
					ie_consistencia_p out	varchar2,
					cd_funcao_p		number,
                    nr_atendimento_p number default null,
                    nr_seq_item_cpoe_p number default null) is

ie_status_w	varchar2(3);
ie_tem_etapas_w	varchar2(3);
nr_seq_mensagem_w number := null;

/*  IE_CONSISTENCIA_P
SSI - Suspens�o de solu��o iniciada/reiniciada
SA   - Suspens�o de solu��o administrada.
SSP - Solu��o com etapas pendentes e nenhuma em andamento
SSIP - Solu��o com etapa em andamento e etapas pendentes
NC - N�o Consiste a administra��o
*/

begin
if	(ie_tipo_solucao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then

	/* obter status atual solu��o */
	select	substr(obter_status_solucao_prescr(ie_tipo_solucao_p, nr_prescricao_p, nr_seq_solucao_p, 'S'),1,3),
		substr(nvl(obter_se_tem_etapas_prescr(ie_tipo_solucao_p, nr_prescricao_p, nr_seq_solucao_p, nr_seq_item_cpoe_p),'X'),1,3)
	into	ie_status_w,
		ie_tem_etapas_w
	from	dual;

    /* consistir suspensao x stats item adep */
    if (nvl(ie_consiste_em_adm_p,'S') = 'S') then
        begin
		if	((ie_status_w = 'I') or (ie_status_w = 'R') or (ie_status_w = 'ITX')) then           

			if (ie_tem_etapas_w = 'S') then
				ie_consistencia_p := 'SSIP';
				-- Suspender etapas pendentes
				 cpoe_suspender_etapas_prescr(ie_tipo_solucao_p, nr_prescricao_p, nr_seq_solucao_p, nr_seq_item_cpoe_p, nr_atendimento_p);
				-- Informar usu�rio sobre OM
			else
				ie_consistencia_p := 'SSI';
			end if;                   

			if	((cd_funcao_p <> 2314) and 
				((cd_funcao_p <> 950) or 
				 (ie_tipo_solucao_p <> 1))) then
				wheb_mensagem_pck.exibir_mensagem_abort(206429);
			end if;
		
		elsif (ie_tem_etapas_w = 'S' and ie_status_w not in ('I','R','T','X')) then
			ie_consistencia_p := 'SSP';
			-- Suspender etapas pendentes
			cpoe_suspender_etapas_prescr(ie_tipo_solucao_p, nr_prescricao_p, nr_seq_solucao_p, nr_seq_item_cpoe_p, nr_atendimento_p);
		
		elsif	(ie_status_w = 'T') then

			ie_consistencia_p := 'SA';

			if	((cd_funcao_p <> 2314) and 
				((cd_funcao_p <> 950) or 
				 (ie_tipo_solucao_p <> 1))) then
				wheb_mensagem_pck.exibir_mensagem_abort(206430);
			end if;         
		end if;
        end;
    else
        ie_consistencia_p := 'NC';
    end if;
end if;

if ie_consistencia_p is null then
    ie_consistencia_p := nvl(ie_status_w, 'N');
end if;

end consistir_status_solucao_adep;
/