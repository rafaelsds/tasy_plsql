create or replace
procedure import_pessoa_juridica_txt(	cd_cnpj_p	varchar2,
				ds_razao_social_p	varchar2,
				nm_fantasia_p		varchar2,
				cd_cep_p		varchar2,
				ds_endereco_p		varchar2,
				ds_bairro_p		varchar2,
				ds_municipio_p		varchar2,
				sg_estado_p		varchar2,
				nm_usuario_p		varchar2,
				ds_complemento_p	varchar2,
				nr_telefone_p		varchar2,
				nr_endereco_p		varchar2,
				nr_fax_p		varchar2,
				ds_email_p		varchar2,
				nm_pessoa_contato_p	varchar2,
				nr_ramal_contato_p	varchar2,
				nr_inscricao_estadual_p	varchar2,
				cd_tipo_pessoa_p	varchar2,
				cd_conta_contabil_p	varchar2,
				ds_site_internet_p	varchar2,
				cd_cond_pagto_p		varchar2,
				ds_nome_abrev_p		varchar2,
				ie_situacao_p		varchar2,
				cd_sistema_ant_p	varchar2,
				ds_observacao_p		varchar2) is
				
nr_seq_pj_estab_w	number(10,0);
cd_cnpj_w		varchar2(14);
cd_estabelecimento_w	number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;

begin
cd_cnpj_w := substr(elimina_caracteres_especiais(cd_cnpj_p),1,14);


if	(nvl(cd_cnpj_w,'X') <> 'X') then
	begin
	
	begin
	insert into pessoa_juridica (
		cd_cgc,
		ds_razao_social,
		nm_fantasia,
		cd_cep,
		ds_endereco,
		ds_bairro,
		ds_municipio,
		sg_estado,
		dt_atualizacao,
		nm_usuario,
		ds_complemento,
		nr_telefone,
		nr_endereco,
		nr_fax,
		ds_email,
		nm_pessoa_contato,
		nr_ramal_contato,
		nr_inscricao_estadual,
		cd_tipo_pessoa,
		cd_conta_contabil,
		ie_prod_fabric,
		ds_site_internet,
		cd_cond_pagto,
		ds_nome_abrev,
		ie_situacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_sistema_ant,
		ie_transporte,
		ds_observacao,
		ie_fornecedor_opme,
		dt_integracao,
		ie_empreendedor_individual)
	values(	cd_cnpj_w,
		substr(ds_razao_social_p,1,255),
		substr(nvl(nm_fantasia_p,ds_razao_social_p),1,80),
		substr(cd_cep_p,1,15),
		substr(ds_endereco_p,1,40),
		substr(ds_bairro_p,1,40),
		substr(ds_municipio_p,1,40),
		substr(sg_estado_p,1,15),
		sysdate,
		nm_usuario_p,
		substr(ds_complemento_p,1,255),
		substr(nr_telefone_p,1,15),
		substr(nr_endereco_p,1,10),
		substr(nr_fax_p,1,15),
		substr(ds_email_p,1,60),
		substr(nm_pessoa_contato_p,1,255),
		substr(nr_ramal_contato_p,1,5),
		substr(nr_inscricao_estadual_p,1,220),
		substr(cd_tipo_pessoa_p,1,3),
		substr(cd_conta_contabil_p,1,20),
		'N',
		substr(ds_site_internet_p,1,255),
		substr(cd_cond_pagto_p,1,10),
		substr(ds_nome_abrev_p,1,18),
		nvl(substr(ie_situacao_p,1,1),'A'),
		sysdate,
		nm_usuario_p,
		substr(cd_sistema_ant_p,1,20),
		'N',
		substr(ds_observacao_p,1,4000),
		'N',
		sysdate,
		'N');
	exception
	when dup_val_on_index then
		update	pessoa_juridica
		set	ds_razao_social		= substr(ds_razao_social_p,1,255),
			nm_fantasia		= substr(nvl(nm_fantasia_p,ds_razao_social_p),1,80),
			cd_cep			= substr(cd_cep_p,1,15),
			ds_endereco		= substr(ds_endereco_p,1,40),
			ds_bairro			= substr(ds_bairro_p,1,40),
			ds_municipio		= substr(ds_municipio_p,1,40),
			sg_estado		= substr(sg_estado_p,1,15),
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			ds_complemento		= substr(ds_complemento_p,1,255),
			nr_telefone		= substr(nr_telefone_p,1,15),
			nr_endereco		= substr(nr_endereco_p,1,10),
			nr_fax			= substr(nr_fax_p,1,15),
			ds_email			= substr(ds_email_p,1,60),
			nm_pessoa_contato	= substr(nm_pessoa_contato_p,1,255),
			nr_ramal_contato		= substr(nr_ramal_contato_p,1,5),
			nr_inscricao_estadual	= substr(nr_inscricao_estadual_p,1,220),
			cd_tipo_pessoa		= substr(cd_tipo_pessoa_p,1,3),
			cd_conta_contabil		= substr(cd_conta_contabil_p,1,20),
			ds_site_internet		= substr(ds_site_internet_p,1,255),
			cd_cond_pagto		= substr(cd_cond_pagto_p,1,10),
			ds_nome_abrev		= substr(ds_nome_abrev_p,1,18),
			ie_situacao		= nvl(substr(ie_situacao_p,1,1),'A'),
			cd_sistema_ant		= substr(cd_sistema_ant_p,1,20),
			ds_observacao		= substr(ds_observacao_p,1,4000),
			dt_integracao		= sysdate
		where	cd_cgc			= cd_cnpj_w;
	end;
	
	if	(nvl(ds_email_p,'X') <> 'X') or
		(nvl(cd_cond_pagto_p,'X') <> 'X') or
		(nvl(nm_pessoa_contato_p,'X') <> 'X') then
		select	max(nr_sequencia)
		into	nr_seq_pj_estab_w
		from	pessoa_juridica_estab
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_cgc 		= cd_cnpj_w;
		
		if	(nvl(nr_seq_pj_estab_w,0) = 0) then
			insert into pessoa_juridica_estab (
					nr_sequencia,
					cd_cgc,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					nm_pessoa_contato,
					ds_email,
					cd_cond_pagto,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_conta_dif_nf)
				values(	pessoa_juridica_estab_seq.nextval,
					cd_cnpj_w,
					cd_estabelecimento_w,
					sysdate,
					nm_usuario_p,
					substr(nm_pessoa_contato_p,1,255),
					substr(ds_email_p,1,255),
					cd_cond_pagto_p,
					sysdate,
					nm_usuario_p,
					'N');
		else
			update	pessoa_juridica_estab
			set	nm_pessoa_contato	= substr(nm_pessoa_contato_p,1,255),
				ds_email			= substr(ds_email_p,1,255),
				cd_cond_pagto		= cd_cond_pagto_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_pj_estab_w;
		end if;
	end if;
	commit;
	end;
end if;

end import_pessoa_juridica_txt;
/
