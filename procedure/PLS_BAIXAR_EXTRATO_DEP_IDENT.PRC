create or replace
procedure pls_baixar_extrato_dep_ident( nr_seq_lote_extrato_p		number,
					nr_seq_conta_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is 
	
nr_documento_w			varchar2(255);
nr_seq_lote_deposito_w		number(10);
ie_tratar_conciliacao_w		varchar2(1);
vl_lancamento_w			number(15,2);
	
Cursor C01 is
	select	nr_documento,
		vl_lancamento
	from	banco_extrato_lanc
	where	nr_seq_extrato		= nr_seq_lote_extrato_p 
	and	to_number(cd_historico)	= 1
	order by 1;
	
begin
select	nvl(max(ie_tratar_conciliacao),'N')
into	ie_tratar_conciliacao_w
from	parametro_deposito_ident
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_tratar_conciliacao_w = 'S') then
	open c01;
	loop
	fetch c01 into	
		nr_documento_w,
		vl_lancamento_w;
	exit when c01%notfound;
		begin		
		select	max(a.nr_sequencia)
		into	nr_seq_lote_deposito_w
		from	deposito_identificado a
		where	a.nr_seq_conta_banco					= nr_seq_conta_p
		and	to_number(a.nr_identificacao || a.ie_digito_ident) 	= nr_documento_w
		and	a.dt_deposito is null
		and	a.cd_estabelecimento					= cd_estabelecimento_p;
		--and	a.vl_deposito						= vl_lancamento_w;
		
		if	(nr_seq_lote_deposito_w is not null) then
			baixar_titulos_deposito_iden(nr_seq_lote_deposito_w,cd_estabelecimento_p,nm_usuario_p);
		end if;
		end;
	end loop;
	close C01;
end if;

end pls_baixar_extrato_dep_ident;
/