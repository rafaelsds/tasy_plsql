create or replace
procedure Ajustar_Vol_Glic_TREV(
						nr_seq_rep_he_p		number,
						nm_usuario_p		varchar2) is
						
nr_seq_glicose_w				number(10);
nr_seq_ele_rep_glic_w			number(10);
qt_equipo_w						number(15,1);
qt_etapa_w						number(15);
qt_volume_sem_glic_w			number(15,4);
qt_aporte_hidrico_diario_w		number(15,2);
qt_volume_faltante_w			number(15,4);
qt_diaria_glic_w				number(15,4);
nr_seq_glic_menor_conc_w		number(10);
qt_conversao_ml_menor_w			number(15,4) := 0;
qt_conv_unid_cons_menor_w		number(16,5);
nr_seq_glic_maior_conc_w		number(10);
qt_conversao_ml_maior_w			number(15,4) := 9999999;
qt_conv_unid_cons_maior_w		number(16,5);
nr_seq_rep_elem_mat_he_w		number(10);
qt_conversao_ml_w				number(15,4);
qt_conv_unid_cons_w				number(16,5);
pr_menor_concentracao_w			number(15,4);
pr_maior_concentracao_w			number(15,4);
qt_ml_total_menor_conc_w		number(15,4);
qt_volume_elem_mat_men_w		number(15,4);
qt_volume_corr_mat_men_w		number(15,4);
qt_volume_etapa_mat_men_w		number(15,4);
qt_volume_elem_mat_mai_w		number(15,4);
qt_volume_corr_mat_mai_w		number(15,4);
qt_volume_etapa_mat_mai_w		number(15,4);
qt_vol_correcao_equipo_w		number(15,4);
ie_correcao_w					varchar2(1);
qt_vel_inf_glicose_w			number(15,2);

cursor c01 is
select	b.nr_sequencia,
		a.qt_conversao_ml,
		a.qt_conv_unid_cons
from 	nut_elem_material a,
		prescr_rep_he_elem_mat b
where	a.nr_sequencia = b.nr_seq_elem_mat
and		b.nr_seq_ele_rep = nr_seq_ele_rep_glic_w
and		a.ie_situacao	= 'A'
and		a.ie_padrao	= 'S'
and		nvl(a.ie_tipo,'NPT') = 'NPT';
						
begin

select	max(qt_aporte_hidrico_diario),
		max(qt_equipo),
		max(qt_etapa),
		nvl(max(ie_correcao),'N'),
		nvl(max(qt_vel_inf_glicose),0)
into	qt_aporte_hidrico_diario_w,
		qt_equipo_w,
		qt_etapa_w,
		ie_correcao_w,
		qt_vel_inf_glicose_w
from	prescr_rep_he
where	nr_sequencia = nr_seq_rep_he_p;

select	sum(qt_volume)
into	qt_volume_sem_glic_w
from	prescr_rep_he_elem
where	nr_seq_rep_he = nr_seq_rep_he_p
and		obter_tipo_elemento(nr_seq_elemento) <> 'C';

select	max(qt_diaria),
		max(nr_sequencia)
into	qt_diaria_glic_w,
		nr_seq_ele_rep_glic_w
from	prescr_rep_he_elem
where	nr_seq_rep_he = nr_seq_rep_he_p
and		obter_tipo_elemento(nr_seq_elemento) = 'C';
begin 

open c01;
loop
fetch c01 into	nr_seq_rep_elem_mat_he_w,
				qt_conversao_ml_w,
				qt_conv_unid_cons_w;
exit when c01%notfound;
	begin
	if (qt_conversao_ml_w > qt_conversao_ml_menor_w) then
	nr_seq_glic_menor_conc_w := nr_seq_rep_elem_mat_he_w;
	qt_conversao_ml_menor_w := qt_conversao_ml_w;
	qt_conv_unid_cons_menor_w := qt_conv_unid_cons_w;
	end if;
	if (qt_conversao_ml_w < qt_conversao_ml_maior_w) then
	nr_seq_glic_maior_conc_w := nr_seq_rep_elem_mat_he_w;
	qt_conversao_ml_maior_w  := qt_conversao_ml_w;
	qt_conv_unid_cons_maior_w := qt_conv_unid_cons_w;
	end if;
	end;
end loop;
close c01;
 if (qt_vel_inf_glicose_w > 0) then
	pr_menor_concentracao_w  := (qt_conv_unid_cons_maior_w/qt_conversao_ml_menor_w) * 100;
	pr_maior_concentracao_w  := (qt_conv_unid_cons_menor_w/qt_conversao_ml_maior_w) * 100; 
	qt_ml_total_menor_conc_w := (qt_diaria_glic_w * qt_conversao_ml_menor_w);
	qt_vol_correcao_equipo_w := (qt_aporte_hidrico_diario_w + qt_equipo_w) / qt_aporte_hidrico_diario_w;

	qt_volume_elem_mat_mai_w  := (qt_ml_total_menor_conc_w - (qt_aporte_hidrico_diario_w - qt_volume_sem_glic_w)) / ((pr_maior_concentracao_w/pr_menor_concentracao_w) - (pr_menor_concentracao_w/pr_menor_concentracao_w));
	qt_volume_corr_mat_mai_w  := (qt_volume_elem_mat_mai_w * qt_vol_correcao_equipo_w);
	qt_volume_etapa_mat_mai_w := (qt_volume_corr_mat_mai_w / qt_etapa_w);
	qt_volume_elem_mat_men_w  := ((qt_aporte_hidrico_diario_w - qt_volume_sem_glic_w) - qt_volume_elem_mat_mai_w);
	qt_volume_corr_mat_men_w  := (qt_volume_elem_mat_men_w * qt_vol_correcao_equipo_w);
	qt_volume_etapa_mat_men_w := (qt_volume_corr_mat_men_w / qt_etapa_w);
end if;

update	prescr_rep_he_elem_mat
set		qt_volume 		= nvl(qt_volume_elem_mat_mai_w,0),
		qt_vol_cor	 	= nvl(qt_volume_corr_mat_mai_w,0),
		qt_vol_etapa 	= nvl(qt_volume_etapa_mat_mai_w,0)
where	nr_sequencia 	= nr_seq_glic_maior_conc_w
and		nr_seq_ele_rep 	= nr_seq_ele_rep_glic_w;

commit;

update	prescr_rep_he_elem_mat
set		qt_volume 		= nvl(qt_volume_elem_mat_men_w,0),
		qt_vol_cor 		= nvl(qt_volume_corr_mat_men_w,0),
		qt_vol_etapa	= nvl(qt_volume_etapa_mat_men_w,0)
where	nr_sequencia	= nr_seq_glic_menor_conc_w
and		nr_seq_ele_rep 	= nr_seq_ele_rep_glic_w;

commit;

update	prescr_rep_he_elem
set		qt_volume			= nvl(qt_volume_elem_mat_mai_w + qt_volume_elem_mat_men_w,0),
		qt_volume_corrigido	= nvl(qt_volume_corr_mat_mai_w + qt_volume_corr_mat_men_w,0),
		qt_volume_etapa		= nvl(qt_volume_etapa_mat_mai_w + qt_volume_etapa_mat_men_w,0)
where	nr_sequencia 		= nr_seq_ele_rep_glic_w
and		nr_seq_rep_he 		= nr_seq_rep_he_p;

exception when others then
	null;
end;
commit;

end Ajustar_Vol_Glic_TREV;
/