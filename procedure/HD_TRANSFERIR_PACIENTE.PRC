create or replace
procedure HD_Transferir_Paciente (	cd_pessoa_fisica_p	varchar2,
					ie_tipo_p		varchar2,
					nr_seq_unid_origem_p	number,
					nr_seq_unid_destino_p	number,
					nr_seq_escala_p		number,
					nr_seq_turno_p		number,
					dt_prov_volta_p		date,
					nm_pessoa_contato_p	varchar2,
					nm_usuario_p		varchar2,
					ds_dialisador_p		varchar2,
					ds_erro_p		out	varchar2) is

cd_estabelecimento_w		number(4,0);
ds_dialisador_w			varchar2(255);
nr_seq_dialisador_w		number(10,0);
cd_perfil_transf_pac_w		number(5,0);
ds_unidade_destino_w		varchar2(80);
vl_parametro_w			varchar2(1);
nr_seq_escala_w			number(10);

nr_seq_escala_dia_w		number(10);
nr_seq_turno_w			number(10);
ie_dia_semana_w			varchar2(1);
nr_seq_escala_aux_w		number(10);
ie_finaliza_trat_w		varchar2(1);

cursor c01 is
	select	nr_sequencia
	from	hd_dializador
	where	obter_se_contido(nr_sequencia,ds_dialisador_p) = 'S'
	and	cd_pessoa_fisica = cd_pessoa_fisica_p;

Cursor C02 is
	select	nr_seq_turno,
		ie_dia_semana,
		nr_sequencia
	from	hd_escala_dialise_dia
	where	nr_Seq_escala = nr_seq_escala_w
	and	dt_fim_escala_dia is null;	

begin

obter_param_usuario(7009, 169, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
obter_param_usuario(7009, 204, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_finaliza_trat_w);

if	(ie_tipo_p = 'I') then
	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	hd_unidade_dialise
	where	nr_sequencia		= nr_seq_unid_destino_p;
elsif	(ie_tipo_p = 'E') then
	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	hd_unidade_dialise
	where	nr_sequencia		= nr_seq_unid_origem_p;
end if;

if	(nr_seq_unid_origem_p = nr_seq_unid_destino_p) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278354,null);
elsif	(cd_estabelecimento_w is null) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278356,null);
elsif	(nr_seq_unid_destino_p = 0) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278357,null);
elsif	((nr_seq_escala_p = 0) and (vl_parametro_w = 'S')) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278358,null);
elsif	((nr_seq_turno_p	= 0) and (vl_parametro_w = 'S')) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278359,null);
else
	begin
	select	max(nr_sequencia)
	into	nr_seq_escala_w
	from	hd_escala_dialise
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_fim is null;
	
	
	/* Baixa a escala atual */
	update	hd_escala_dialise
	set	dt_fim			= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	dt_fim is null;

	/* Insere a nova escala */
	
	select	hd_escala_dialise_seq.nextval
	into	nr_seq_escala_aux_w
	from 	dual;
	
	--if (vl_parametro_w = 'S') then
	if (ie_tipo_p = 'I') then
	
	insert into hd_escala_dialise (
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		nr_seq_turno,
		nr_seq_escala,
		dt_inicio,
		nr_seq_unid_dialise,
		dt_prov_volta,
		nm_pessoa_contato
	) values (
		nr_seq_escala_aux_w,
		cd_estabelecimento_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_p,
		decode(vl_parametro_w,'S',nr_seq_turno_p,null),
		decode(vl_parametro_w,'S',nr_seq_escala_p,null),
		sysdate,
		nr_seq_unid_destino_p,
		dt_prov_volta_p,
		nm_pessoa_contato_p
		);
	end if;
	--end if;
	/* Tratamento dos dialisadores */	
	--ds_dialisador_w		:= ds_dialisador_p;
	--ds_dialisador_w		:= replace(ds_dialisador_w, ',',' ');
	--ds_dialisador_w		:= replace(ds_dialisador_w, '  ',' ');
	
	hd_transf_prot_exame(cd_pessoa_fisica_p,nr_seq_unid_destino_p,nm_usuario_p);
	
	open c01;
	loop
	fetch c01 into
		nr_seq_dialisador_w;
	exit when c01%notfound;
		begin
		
		update	hd_dializador
		set	ie_status		   = 'T',
			nr_seq_unid_dialise_atual  = nr_seq_unid_destino_p
		where	nr_sequencia		   = nr_seq_dialisador_w;
		
		insert into hd_dialisador_transf (
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_dialisador,
			nr_seq_unid_origem,
			nr_seq_unid_destino,
			dt_transferencia,
			cd_pf_transf,
			ie_tipo
		) values (
			hd_dialisador_transf_seq.nextval,
			cd_estabelecimento_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_dialisador_w,
			nr_seq_unid_origem_p,
			nr_seq_unid_destino_p,
			sysdate,
			substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
			ie_tipo_p
		);
				
		end;
	end loop;
	close c01;
		
	
	if	(vl_parametro_w = 'N') then
	
		open C02;
		loop
		fetch C02 into	
			nr_seq_turno_w,
			ie_dia_semana_w,
			nr_seq_escala_dia_w;
		exit when C02%notfound;
			begin
			
			update	hd_escala_dialise_dia
			set	dt_fim_escala_dia = sysdate
			where	nr_sequencia = nr_seq_escala_dia_w;
			
			if (ie_tipo_p = 'I') then
			
			insert into hd_escala_dialise_dia
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_turno,
				ie_dia_semana,
				dt_inicio_escala_dia,
				nr_seq_escala)
			values
				(hd_escala_dialise_dia_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_turno_w,
				ie_dia_semana_w,
				sysdate,
				nr_seq_escala_aux_w);
			
			end if;
				
			end;
		end loop;
		close C02;
	end if;
	
	/* Enviar comunicação ao receber paciente */
	begin
	select	cd_perfil_transf_pac
	into	cd_perfil_transf_pac_w
	from	hd_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception
		when others then
		cd_perfil_transf_pac_w	:= 0;
	end;

	if 	(cd_perfil_transf_pac_w > 0) then
		if 	(cd_pessoa_fisica_p is not null) then
			begin
			
			ds_unidade_destino_w	:= substr(HD_Obter_Desc_Unid_Dialise(nr_seq_unid_destino_p),1,80);

			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(802540) || ': ' || ds_unidade_destino_w,
				wheb_mensagem_pck.get_texto(791350) || ' ' || cd_pessoa_fisica_p ||' - '|| obter_nome_pf(cd_pessoa_fisica_p) || chr(10) || 
	  			wheb_mensagem_pck.get_texto(795798) || ': ' || SUBSTR(HD_OBTER_DESC_UNID_DIALISE(nr_seq_unid_destino_p),1,90),
				nm_usuario_p,
				sysdate,
				'N',
				'',
				cd_perfil_transf_pac_w ||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_w
			);
			end;
		end if;
	end if;
	
	if	(ie_finaliza_trat_w = 'S') and
		(ie_tipo_p = 'E') then
		hd_finaliza_trat_pac_dialise(cd_pessoa_fisica_p,cd_estabelecimento_w);
	end if;
	
	commit;	
	exception
		when others then
		rollback;
	end;
	
end if;	

end HD_Transferir_Paciente;
/
