create or replace 
procedure cpoe_inserir_recomendacao_ant(
						nr_atendimento_p		prescr_medica.nr_atendimento%type,
						nr_atendimento_ant_p	prescr_medica.nr_atendimento%type,
						nr_sequencia_p			prescr_recomendacao.nr_sequencia%type,
						nr_prescricao_p			prescr_medica.nr_prescricao%type,
						nm_usuario_p			prescr_medica.nm_usuario%type,
						cd_perfil_p				number,
						cd_estabelecimento_p	number,
                        nr_seq_item_gerado_p    out number,
						nr_seq_transcricao_p	number default null,
						ie_item_alta_p			varchar2 default 'N',
						ie_prescritor_aux_p		varchar2 default 'N',
						cd_medico_p				number default null,
						ie_retrogrado_p     	varchar2 default 'N',
						dt_inicio_p         	date default null,
						nr_seq_pepo_p			number default null,
						nr_cirurgia_p			number default null,
						nr_cirurgia_patologia_p	number default null,
						nr_seq_agenda_p			number default null,
						ie_oncologia_p			varchar2 default 'N',
						nr_seq_conclusao_apae_p	number default null,
						ie_futuro_p 	    	varchar2 default 'N',
						nr_seq_cpoe_order_unit_p    cpoe_order_unit.nr_sequencia%type default null) is

cd_recomendacao_w		prescr_recomendacao.cd_recomendacao%type;
ds_recomendacao_w		prescr_recomendacao.ds_recomendacao%type;
nr_seq_topografia_w		prescr_recomendacao.nr_seq_topografia%type;
ie_acm_w				prescr_recomendacao.ie_acm%type;
ie_se_necessario_w		prescr_recomendacao.ie_se_necessario%type;
ds_horarios_w			prescr_recomendacao.ds_horarios%type;
hr_prim_horario_w		prescr_recomendacao.hr_prim_horario%type;
hr_prim_horario_ww		prescr_recomendacao.hr_prim_horario%type;
dt_prim_horario_w		prescr_recomendacao.dt_inicio%type;
cd_intervalo_w			prescr_recomendacao.cd_intervalo%type;
nr_ocorrencia_w			prescr_recomendacao.nr_ocorrencia%type;
cd_perfil_ativo_w		prescr_recomendacao.cd_perfil_ativo%type;

nr_sequencia_w			cpoe_recomendacao.nr_sequencia%type;
ie_administracao_w		cpoe_recomendacao.ie_administracao%type;
ie_urgencia_w			cpoe_recomendacao.ie_urgencia%type:='';
ds_horarios_aux_w		cpoe_recomendacao.ds_horarios%type:='';
ie_duracao_w 			cpoe_recomendacao.ie_duracao%type:='C';

qt_min_intervalo_w 		intervalo_prescricao.qt_min_intervalo%type;

ie_prescr_alta_agora_w	varchar2(1);
dt_fim_w				date := null;

cursor C01 is
select	a.cd_recomendacao,
	a.ds_recomendacao,
	a.nr_seq_topografia,
	nvl(a.ie_acm,'N'),
	nvl(a.ie_se_necessario,'N'),
	a.cd_intervalo,
	a.nr_ocorrencia,
	a.hr_prim_horario,
	a.ds_horarios
from	prescr_recomendacao a,
	prescr_medica b
where	b.nr_prescricao	= a.nr_prescricao
and	b.nr_prescricao	= nr_prescricao_p
and	a.nr_sequencia	= nr_sequencia_p;

begin

obter_param_usuario(924,409, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_prescr_alta_agora_w);

open C01;
loop
fetch C01 into	cd_recomendacao_w,
				ds_recomendacao_w,
				nr_seq_topografia_w,
				ie_acm_w,
				ie_se_necessario_w,
				cd_intervalo_w,
				nr_ocorrencia_w,
				hr_prim_horario_w,
				ds_horarios_w;
exit when C01%notfound;
	begin


	if	(nvl(ie_prescr_alta_agora_w,'N') = 'S') and
		(nvl(ie_item_alta_p,'N') = 'S')	then

		ie_urgencia_w := 0;

		select	max(qt_min_intervalo)
		into	qt_min_intervalo_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_w;

		cpoe_calcular_horario_prescr (nr_atendimento_p, cd_intervalo_w, null, sysdate,
									0, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w, ds_horarios_aux_w,
									nm_usuario_p,  cd_estabelecimento_p, cd_perfil_p, null);

		ds_horarios_w	:= ds_horarios_w || ds_horarios_aux_w;

		hr_prim_horario_w	:= obter_prim_dshorarios(ds_horarios_w);
		dt_prim_horario_w	:= to_date(to_char(sysdate,'dd/mm/yyyy ') || hr_prim_horario_w, 'dd/mm/yyyy hh24:mi');

		if (dt_prim_horario_w < sysdate) then
			dt_prim_horario_w := dt_prim_horario_w + 1;
		end if;
	else
		ie_urgencia_w := '';
		cpoe_atualizar_periodo_vig_ant(ds_horarios_w, hr_prim_horario_w, dt_prim_horario_w);
	end if;

	ie_administracao_w := 'P';
	if (ie_se_necessario_w = 'S') then
		ie_administracao_w := 'N';
		ds_horarios_w := '';
	elsif (ie_acm_w = 'S') then
		ie_administracao_w := 'C';
		ds_horarios_w := '';
	end if;

	if (ie_retrogrado_p = 'S' or ie_futuro_p = 'S') then -- retrograde item
		dt_prim_horario_w := dt_inicio_p;
		dt_fim_w := (dt_prim_horario_w + 1) - 1/1440;

		select	nvl(max(qt_min_intervalo),0)
		into	qt_min_intervalo_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_w;

		ie_duracao_w 		:= 'P';
		nr_ocorrencia_w 	:= 0;
		ie_urgencia_w 		:= null;
		ds_horarios_w		:= '';
		ds_horarios_aux_w	:= '';
		cpoe_calcular_horario_prescr (nr_atendimento_p, cd_intervalo_w, null, dt_prim_horario_w,
									0, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w, ds_horarios_aux_w,
									nm_usuario_p,  cd_estabelecimento_p, cd_perfil_p, null);

		ds_horarios_w		:= ds_horarios_w || ds_horarios_aux_w;
		hr_prim_horario_w	:= obter_prim_dshorarios(ds_horarios_w);
	end if;

	select	cpoe_recomendacao_seq.nextval
	into	nr_sequencia_w
	from 	dual;

	insert into cpoe_recomendacao(
				nr_sequencia,
				nr_atendimento,
				cd_recomendacao,
				ds_recomendacao,
				nr_seq_topografia,
				ie_administracao,
				ie_duracao,
				hr_prim_horario,
				dt_inicio,
				dt_fim,
				cd_intervalo,
				ds_horarios,
				nr_ocorrencia,
				ie_acm,
				ie_se_necessario,
				ie_urgencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				cd_perfil_ativo,
				cd_funcao_origem,
				nr_seq_transcricao,
				ie_item_alta,
				ie_prescritor_aux,
				cd_medico,
				ie_retrogrado,
				nr_seq_pepo,
				nr_cirurgia,
				nr_cirurgia_patologia,
				nr_seq_agenda,
				ie_oncologia,
				nr_seq_conclusao_apae,
				ie_futuro,
				nr_seq_cpoe_order_unit)
			values(
				nr_sequencia_w,
				nr_atendimento_p,
				cd_recomendacao_w,
				ds_recomendacao_w,
				nr_seq_topografia_w,
				ie_administracao_w,
				ie_duracao_w,
				hr_prim_horario_w,
				dt_prim_horario_w,
				dt_fim_w,
				cd_intervalo_w,
				ds_horarios_w,
				nr_ocorrencia_w,
				ie_acm_w,
				ie_se_necessario_w,
				null,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				obter_cd_paciente_prescricao(nr_prescricao_p),
				cd_perfil_p,
				2314,
				nr_seq_transcricao_p,
				ie_item_alta_p,
				ie_prescritor_aux_p,
				cd_medico_p,
				ie_retrogrado_p,
				nr_seq_pepo_p,
				nr_cirurgia_p,
				nr_cirurgia_patologia_p,
				nr_seq_agenda_p,
				ie_oncologia_p,
				nr_seq_conclusao_apae_p,
				ie_futuro_p,
				nr_seq_cpoe_order_unit_p);

    nr_seq_item_gerado_p := nr_sequencia_w;

	end;
end loop;
close C01;
commit;

end cpoe_inserir_recomendacao_ant;
/