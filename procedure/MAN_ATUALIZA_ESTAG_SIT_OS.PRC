create or replace
procedure	man_atualiza_estag_sit_os(
			nr_seq_ordem_p		number,
			nr_seq_estagio_p		number,
			ie_tipo_atualizacao_p	number) is

begin

/* 
Esta procedure � utilizada para realizar os updates correspondentes ao WDLG "estagio_situacao_wdlg",
que no Delphi � realizado com Atualizacao_Padrao_Parametros
*/

if	(nvl(nr_seq_ordem_p,0) > 0) then
	begin

	if	(ie_tipo_atualizacao_p = 0) then
		begin

		update	man_ordem_servico
		set	nr_seq_estagio = nr_seq_estagio_p
		where	nr_sequencia = nr_seq_ordem_p;

		end;
	elsif	(ie_tipo_atualizacao_p = 1) then
		begin

		update	man_ordem_servico
		set	dt_inicio_previsto = null,
			dt_fim_previsto = null,
			ie_status_ordem = 1
		where nr_sequencia = nr_seq_ordem_p;

		end;
	elsif	(ie_tipo_atualizacao_p = 2) then
		begin

		update	man_ordem_servico
		set	dt_fim_real = null,
			ie_status_ordem = 2
		where	nr_sequencia = nr_seq_ordem_p;

		end;
	end if;

	commit;

	end;
end if;

end man_atualiza_estag_sit_os;
/