create or replace
procedure alterar_paciente_triagem(	nr_sequencia_p	Varchar2,
				cd_pessoa_fisica_p Varchar2 )
					is
nr_seq_fila_senha_w	number(10);

begin
if	(nr_sequencia_p is not null) and
	(cd_pessoa_fisica_p is not null) then
	begin
	update	TRIAGEM_PRONTO_ATEND
	set	cd_pessoa_fisica = cd_pessoa_fisica_p
	where	nr_sequencia	= nr_sequencia_p;
	
	select	nr_seq_fila_senha
	into	nr_seq_fila_senha_w
	from	triagem_pronto_atend
	where	nr_sequencia	= nr_sequencia_p;
	
	IF	(nvl(nr_seq_fila_senha_w,0) > 0) then
		update	paciente_senha_fila
		set	cd_pessoa_fisica = cd_pessoa_fisica_p
		where	nr_sequencia = nr_seq_fila_senha_w;
	end if;
	
	end;
end if;
commit;

end alterar_paciente_triagem;
/