create or replace procedure sc_gerar_cobr_santander_240_1
      ( nr_seq_cobr_escrit_p    number,
        cd_estabelecimento_p    number,
        nm_usuario_p      varchar2) is

ds_conteudo_w     varchar2(240);
nr_remessa_w      varchar2(8);
qt_lote_arquivo_w     varchar2(6) := 0;
qt_reg_lote_w     varchar2(6) := 0;
qt_registro_lote_w      varchar2(6) := 0;
nr_seq_arquivo_w      varchar2(6) := 0;
nr_seq_registro_w     varchar2(5) := 0;
nm_empresa_w      varchar2(30);
cd_cgc_w      varchar2(15);
nm_banco_w      varchar2(30);
dt_geracao_w      varchar2(8);
nr_lote_w_w     varchar2(4) := 0;
nr_lote_w       varchar2(4);
ie_tipo_taxa_juro_w     varchar2(1);
ie_tipo_taxa_multa_w    varchar2(1);
cd_multa_w      varchar2(1);
cd_juro_w     varchar2(1);
nr_linha_w varchar2(5) := 0;

/* Segmentos */
nr_nosso_numero_w   varchar2(20);
nr_nosso_num_tit_w    titulo_receber.nr_nosso_numero%type;
dt_vencimento_w     varchar2(8);
vl_titulo_w     varchar2(15);
dt_emissao_w      varchar2(8);
cd_agencia_w      varchar2(4);
ie_digito_agencia_w   varchar2(1);
nr_conta_w      varchar2(9);
ie_digito_conta_w     varchar2(1);
cd_conta_cobr_w     varchar2(9);
ie_conta_cobr_w     varchar2(1);
nr_seu_numero_w     varchar2(15);
ie_tipo_inscricao_w     varchar2(1);
nr_inscricao_w      varchar2(15);
nm_sacado_w     varchar2(40);
ds_endereco_sacado_w    varchar2(40);
ds_bairro_sacado_w    varchar2(15);
cd_cep_sacado_w     varchar2(8);
ds_municipio_sacado_w   varchar2(15);
ds_estado_sacado_w    varchar2(2);
cd_mov_remessa_w    varchar2(2);
cd_transmissao_w      varchar2(15);
nm_cedente_w      varchar2(30);
nr_seq_pagador_w      number(10);
nr_titulo_w     number(10);

ds_conteudo_aux_w   varchar2(200);
ds_mensagem_3_w     varchar2(40);
ds_mensagem_4_w     varchar2(40);
vl_mora_w     varchar2(15);
ie_impressao_w      varchar2(1);
cd_banco_w      banco.cd_banco%type;
ie_judicial_w     varchar2(1);
cd_tipo_cobranca_w    varchar2(1);
cd_forma_cadastro_w   varchar2(1);

nr_seq_mensalidade_w    pls_mensalidade.nr_sequencia%type;
tx_multa_w      titulo_receber.tx_multa%type;
ds_multa_w      varchar2(15);
qt_mensagem_w     number(10);
vl_juros_w      number(10);
cd_especie_titulo_banco_w   especie_titulos_remessa.cd_especie_titulo_banco%type;

nr_vetor_w      number(10);
type t_mensagem is record ( ds_mensagem varchar2(200));

type vetor_mensagem is table of t_mensagem index by binary_integer;
vetor_mensagem_w  vetor_mensagem;
cd_estabelecimento_w	number(4);

/* Segmento P */
cursor C01 is
  select  substr(nvl(c.cd_ocorrencia,'01'),1,2) cd_mov_remessa,
    substr(x.cd_agencia_bancaria,1,4) cd_agencia,
    substr(z.ie_digito,1,1) ie_digito_agencia,
    substr(x.cd_conta,1,9) nr_conta,
    substr(c.ie_digito_conta,1,1) ie_digito_conta,
    substr(x.cd_conta,1,9) cd_conta_cobr,
    substr(x.ie_digito_conta,1,1) ie_conta_cobr,
    nvl(b.nr_nosso_numero,'0') nr_nosso_num_tit,
    nvl(substr(obter_nosso_numero_interf(x.cd_banco,b.nr_titulo),1,13),'0') nr_nosso_numero,
    substr(b.nr_titulo,1,15) nr_seu_numero,
    to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyyyy') dt_vencimento,
    lpad(somente_numero(to_char(nvl(c.vl_cobranca,nvl(b.vl_titulo,0)),'9999999999990.00')),15,'0') vl_titulo,
    to_char(b.dt_emissao,'ddmmyyyy') dt_emissao,

    /* Segmento Q */

    decode(b.cd_cgc, null,'1','2') ie_tipo_inscricao,
    substr(b.cd_cgc_cpf,1,15) nr_inscricao,
    upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))) nm_sacado,
    substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'ECM'),pls_obter_compl_pagador(d.nr_seq_pagador,'EC')),1,40) ds_endereco_sacado,
    substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,15) ds_bairro_sacado,
    substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8) cd_cep_sacado,
    substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15) ds_municipio_sacado,
    substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2) ds_estado_sacado,
    f.nr_sequencia nr_seq_pagador,
    b.nr_titulo nr_titulo,
    nvl(obter_instrucao_boleto_estab(b.nr_titulo,a.cd_banco,1,nvl(cd_estabelecimento_p,a.cd_estabelecimento)), ' ') ds_mensagem_3,
    nvl(obter_instrucao_boleto_estab(b.nr_titulo,a.cd_banco,2,nvl(cd_estabelecimento_p,a.cd_estabelecimento)), ' ') ds_mensagem_4,
    somente_numero(to_char(nvl(nvl(c.vl_juros, obter_juros_multa_titulo(b.nr_titulo, sysdate, 'R', 'J')),0) +
    nvl(nvl(c.vl_multa, obter_juros_multa_titulo(b.nr_titulo, sysdate, 'R', 'M')),0),'9999999999990.00')) vl_mora,
    x.cd_banco,
    d.nr_sequencia,
    b.tx_multa,
   -- obter_juros_multa_titulo(b.nr_titulo,sysdate,'R','J') vl_juros,
    b.tx_juros,
    obter_especie_titulo_banco(a.cd_banco, b.ie_tipo_titulo),
	a.cd_estabelecimento
  from  agencia_bancaria  z,
    banco_estabelecimento x,
    pls_contrato_pagador  f,
    banco_carteira    e,
    pls_mensalidade   d,
    titulo_receber_v    b,
    titulo_receber_cobr   c,
    cobranca_escritural   a
  where a.nr_sequencia    = c.nr_seq_cobranca
  and c.nr_titulo     = b.nr_titulo
  and x.nr_sequencia    = a.nr_seq_conta_banco
  and z.cd_agencia_bancaria = x.cd_agencia_bancaria
  and z.cd_banco    = x.cd_banco
  and d.nr_sequencia(+)   = b.nr_seq_mensalidade
  and e.nr_sequencia(+)   = b.nr_seq_carteira_cobr
  and f.nr_sequencia(+)   = d.nr_seq_pagador
  and a.nr_sequencia    = nr_seq_cobr_escrit_p;

Cursor C02 is
  select  substr(rpad(substr(decode(ie_ordem,1,'Codigo: ' || substr(pls_obter_dados_cart_segurado(nr_seq_seg,'C'),1,50),''),1,40), 40, ' ') ||
    rpad(substr(decode(ie_ordem,1,'Nome: ' || upper(substr(pls_obter_dados_segurado(nr_seq_seg,'N'),1,80)),''),1,40), 40, ' ') ||
    rpad(substr(decode(ie_ordem,1,('Parcela: ' || nr_parcela || ' - Idade: ' || qt_idade || ' - Contrato: ' || nr_seq_contrato),''),1,40), 40, ' ') ||
    rpad(substr(decode(ie_ordem,1,'Produto: ' || (substr(pls_obter_dados_produto(nr_seq_plano, 'PA'), 1, 30) || '   -   ' ||
    substr(pls_obter_dados_segurado(nr_seq_segurado, 'P'), 1, 80)),'Outros'),1,40), 40, ' ') ||
    rpad(substr('Valor: ' || to_char(campo_mascara_virgula(vl_mensalidade)) || ' - Coparticipacao: ' ||
    nvl(campo_mascara_virgula(pls_obter_valor_item_mens(nr_sequencia,'3')),0),1,60), 40, ' '),1,200)
  from  (select 1 ie_ordem,
      e.nr_sequencia nr_seq_seg,
      d.nr_parcela,
      d.qt_idade,
      c.nr_seq_contrato,
      e.nr_seq_plano,
      d.nr_seq_segurado,
      d.nr_sequencia,
      nvl(pls_obter_valor_item_mens(d.nr_sequencia,'1'), 0) vl_mensalidade
    from  pls_segurado e,
      pls_mensalidade_segurado  d,
      pls_mensalidade   c,
      titulo_receber_cobr   b,
      titulo_receber    x,
      cobranca_escritural   a
    where a.nr_sequencia        = b.nr_seq_cobranca
    and b.nr_titulo     = x.nr_titulo
    and c.nr_sequencia    = x.nr_seq_mensalidade
    and d.nr_seq_mensalidade  = c.nr_sequencia
    and e.nr_sequencia          = d.nr_seq_segurado
    and x.nr_titulo     = nr_titulo_w
    and d.nr_sequencia in       (select f.nr_sequencia
            from    pls_mensalidade_segurado f
            where   f.nr_seq_mensalidade = c.nr_sequencia
            and rownum <= 50)
    union all
    select  2 ie_ordem,
      e.nr_sequencia nr_seq_seg,
      d.nr_parcela,
      d.qt_idade,
      c.nr_seq_contrato,
      e.nr_seq_plano,
      d.nr_seq_segurado,
      d.nr_sequencia,
      d.vl_mensalidade
    from  pls_segurado e,
      pls_mensalidade_segurado  d,
      pls_mensalidade   c,
      titulo_receber_cobr   b,
      titulo_receber    x,
      cobranca_escritural   a
    where a.nr_sequencia            = b.nr_seq_cobranca
    and b.nr_titulo     = x.nr_titulo
    and c.nr_sequencia    = x.nr_seq_mensalidade
    and d.nr_seq_mensalidade    = c.nr_sequencia
    and e.nr_sequencia            = d.nr_seq_segurado
    and x.nr_titulo     = nr_titulo_w
    and d.nr_sequencia not in   (select f.nr_sequencia
            from    pls_mensalidade_segurado f
            where   f.nr_seq_mensalidade = c.nr_sequencia
            and rownum <= 50));
	   
Cursor C03 is
    select    	a.*
    from       	table (pls_obter_dados_mens_reaj.obter_dados(nr_titulo_w)) a
	union all
	select  nvl(obter_instrucao_boleto_estab(nr_titulo_w,cd_banco_w,1,nvl(cd_estabelecimento_p,cd_estabelecimento_w)), ' ')
	from dual;

begin
delete  from w_envio_banco
where nm_usuario  = nm_usuario_p;

/* Header Arquivo*/
qt_lote_arquivo_w := qt_lote_arquivo_w + 1;

select  elimina_acentuacao(substr(obter_nome_pf_pj(null,b.cd_cgc),1,30)) nm_empresa,
  upper(substr(obter_nome_banco(c.cd_banco),1,30)) nm_banco,
  b.cd_cgc cd_cgc,
  to_char(a.nr_sequencia) nr_seq_arquivo,
  to_char(sysdate,'ddmmyyyy') dt_geracao,
  substr(nvl(c.cd_transmissao,'0'),1,15) cd_transmissao,
  nvl(substr(e.cd_tipo_cobranca_ext,1,1),'5') cd_tipo_cobranca,
  nvl(substr(e.cd_forma_cadastro_ext,1,1),'1') cd_forma_cadastro
into  nm_empresa_w,
  nm_banco_w,
  cd_cgc_w,
  nr_seq_arquivo_w,
  dt_geracao_w,
  cd_transmissao_w,
  cd_tipo_cobranca_w,
  cd_forma_cadastro_w
from  estabelecimento b,
  banco_estabelecimento c,
  cobranca_escritural a,
  banco_carteira d,
  tipo_cobr_escrit e
where a.cd_estabelecimento  = b.cd_estabelecimento
and a.nr_seq_conta_banco  = c.nr_sequencia
and a.nr_seq_carteira_cobr  = d.nr_sequencia(+)
and a.nr_sequencia    = nr_seq_cobr_escrit_p
and a.nr_seq_tipo   = e.nr_sequencia(+);

ds_conteudo_w :=  '033' ||
      '0000' ||
      '0' ||
      rpad(' ',8,' ') ||
      '2' ||
      lpad(nvl(cd_cgc_w,'0'),15, '0') ||
      lpad(nvl(cd_transmissao_w,'0'),15,'0') ||
      rpad(' ',25,' ') ||
      rpad(nvl(nm_empresa_w,' '),30,' ') ||
      rpad(nvl(nm_banco_w,' '),30,' ') ||
      rpad(' ',10,' ') ||
      '1' ||
      lpad(nvl(dt_geracao_w,'0'),8,'0') ||
      rpad(' ',6,' ') ||
      lpad(nvl(nr_seq_arquivo_w,'0'),6, '0') ||
      '040' ||
      rpad(' ',74,' ');

insert  into w_envio_banco
  (nr_sequencia,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  cd_estabelecimento,
  ds_conteudo,
  nr_seq_apres,
  nr_seq_apres_2)
values  (w_envio_banco_seq.nextval,
  sysdate,
  nm_usuario_p,
  sysdate,
  nm_usuario_p,
  cd_estabelecimento_p,
  ds_conteudo_w,
  1,
  1);

/* Fim Header Arquivo */

/* Header Lote */
nr_lote_w_w := nr_lote_w_w + 1;
nr_lote_w   := lpad(nr_lote_w_w,4,'0');
qt_lote_arquivo_w := qt_lote_arquivo_w + 1;
qt_registro_lote_w  := qt_registro_lote_w + 1;

select  b.cd_cgc cd_cgc,
  elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30)) nm_cedente,
  substr(nvl(a.nr_remessa,a.nr_sequencia),1,8) nr_remessa,
  to_char(sysdate,'ddmmyyyy') dt_geracao,
  substr(nvl(c.cd_transmissao,'0'), 1, 15) cd_transmissao
into  cd_cgc_w,
  nm_cedente_w,
  nr_remessa_w,
  dt_geracao_w,
  cd_transmissao_w
from  estabelecimento   b,
  banco_estabelecimento c,
  cobranca_escritural   a,
  banco_carteira    d
where a.cd_estabelecimento  = b.cd_estabelecimento
and a.nr_seq_conta_banco  = c.nr_sequencia
and a.nr_seq_carteira_cobr  = d.nr_sequencia(+)
and a.nr_sequencia    = nr_seq_cobr_escrit_p;

ds_conteudo_w :=  '033' ||
      lpad(nvl(nr_lote_w,'0'),4, '0') ||
      '1' ||
      'R' ||
      '01' ||
      rpad(' ', 2, ' ') ||
      '030' ||
      rpad(' ', 1, ' ') ||
      '2' ||
      lpad(nvl(cd_cgc_w,'0'),15, '0') ||
      rpad(' ', 20, ' ') ||
      lpad(nvl(cd_transmissao_w,'0'),15,'0') ||
      rpad(' ',5,' ') ||
      rpad(nvl(nm_cedente_w,' '),30, ' ') ||
      rpad(' ', 40, ' ') ||
      rpad(' ', 40, ' ') ||
      lpad(nvl(nr_remessa_w,'0'),8, '0') ||
      lpad(nvl(dt_geracao_w,'0'),8, '0') ||
      rpad(' ', 41, ' ');

insert into w_envio_banco
  (nr_sequencia,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  cd_estabelecimento,
  ds_conteudo,
  nr_seq_apres,
  nr_seq_apres_2)
values  (w_envio_banco_seq.nextval,
  sysdate,
  nm_usuario_p,
  sysdate,
  nm_usuario_p,
  cd_estabelecimento_p,
  ds_conteudo_w,
  2,
  2);

qt_reg_lote_w   := qt_reg_lote_w + 1;
/* Fim Header Lote */

/* Inicio Segmentos */
open C01;
loop
fetch C01 into
  cd_mov_remessa_w,
  cd_agencia_w,
  ie_digito_agencia_w,
  nr_conta_w,
  ie_digito_conta_w,
  cd_conta_cobr_w,
  ie_conta_cobr_w,
  nr_nosso_num_tit_w,
  nr_nosso_numero_w,
  nr_seu_numero_w,
  dt_vencimento_w,
  vl_titulo_w,
  dt_emissao_w,
  ie_tipo_inscricao_w,
  nr_inscricao_w,
  nm_sacado_w,
  ds_endereco_sacado_w,
  ds_bairro_sacado_w,
  cd_cep_sacado_w,
  ds_municipio_sacado_w,
  ds_estado_sacado_w,
  nr_seq_pagador_w,
  nr_titulo_w,
  ds_mensagem_3_w,
  ds_mensagem_4_w,
  vl_mora_w,
  cd_banco_w,
  nr_seq_mensalidade_w,
  tx_multa_w,
  vl_juros_w,
  cd_especie_titulo_banco_w,
  cd_estabelecimento_w;
exit when C01%notfound;
  begin
  /* Segmento P */
  nr_seq_registro_w := nr_seq_registro_w + 1;
  qt_registro_lote_w  := qt_registro_lote_w + 1;
  qt_lote_arquivo_w := qt_lote_arquivo_w + 1;

  select  max(b.ie_tipo_taxa)
  into  ie_tipo_taxa_juro_w
  from  tipo_taxa   b,
    titulo_receber    a
  where a.cd_tipo_taxa_juro   = b.cd_tipo_taxa
  and a.nr_titulo     = nr_titulo_w;

  if  (ie_tipo_taxa_juro_w in ('V','F')) then
    cd_juro_w   := '1';
  elsif (ie_tipo_taxa_juro_w = 'D') then
    cd_juro_w   := '2';
  else
    cd_juro_w   := '3';
  end if;

  select  max(b.ie_tipo_taxa)
  into  ie_tipo_taxa_multa_w
  from  tipo_taxa   b,
    titulo_receber    a
  where a.cd_tipo_taxa_multa  = b.cd_tipo_taxa
  and a.nr_titulo   = nr_titulo_w;

  if  (ie_tipo_taxa_multa_w in ('V','F')) then
    cd_multa_w  := '1';
  else
    cd_multa_w  := '2';
  end if;

  if  (nr_nosso_num_tit_w = '0') and /* Tratamento para gravar o nosso n�mero conforme parametriza��o do t�tulo e para atualizar a sequencia do nosso n�mero na regra do mesmo */
    (nr_nosso_numero_w <> '0') then
    update  titulo_receber
    set nr_nosso_numero     = nr_nosso_numero_w,
      ds_observacao_titulo  = substr(ds_observacao_titulo ||
            ' Nosso n�mero ' || nr_nosso_numero_w || ' atualizado atrav�s da procedure gerar_cobr_santander_240 ao gerar remessa da cobran�a escritural ' || nr_seq_cobr_escrit_p,1,4000)
    where nr_titulo       = nr_titulo_w;

    update  banco_regra_numero
    set nr_atual      = nvl(nr_atual,0) + 1
    where cd_banco    = cd_banco_w
    and cd_estabelecimento    = cd_estabelecimento_p
    and ie_regra      = 'FSD';
  elsif (nr_nosso_num_tit_w <> '0') then
    nr_nosso_numero_w   := lpad(substr(nr_nosso_num_tit_w,1,13),13,'0');
  end if;  

  ds_conteudo_w :=  '033' || --001 - 003
        lpad(nr_lote_w, 4,'0') || --004 - 007
        '3' || --008 - 008
        lpad(nvl(nr_seq_registro_w,'0'), 5, '0') || --009 - 013
        'P' || --014 - 014
        rpad(' ', 1, ' ') || --015 - 015
        lpad(nvl(cd_mov_remessa_w,'0'), 2, '0') || --016 - 017
        lpad(nvl(cd_agencia_w,'0'), 4, '0') || --018 - 021
        lpad(nvl(ie_digito_agencia_w,'0'), 1, '0') || --022 - 022
        lpad(nvl(nr_conta_w,'0'), 9, '0') || --023 - 031
        lpad(nvl(ie_conta_cobr_w,'0'), 1, '0') || --032 - 032
        lpad(nvl(cd_conta_cobr_w,'0'), 9, '0') || --033 - 041
        lpad(nvl(ie_conta_cobr_w,'0'), 1, '0') || --042 - 042
        rpad(' ', 2, ' ') || --043 - 044
        lpad(nvl(nr_nosso_numero_w,'0'), 13, '0') || --045 - 057
        '5' || --058 - 058
        '1' || -- 059 - 059
        '2' || --060 - 060
        rpad(' ', 2, ' ') || --061 - 062
        lpad(nvl(nr_seu_numero_w,'0'), 15, '0') || --063 - 077
        lpad(nvl(dt_vencimento_w,'0'), 8, '0') || --078 - 085
        vl_titulo_w || --086 - 100
        '0000' || --101 - 104
        '0' || --105 - 105
        rpad(' ', 1, ' ') || --106 - 106
        lpad(nvl(cd_especie_titulo_banco_w,'2'), 2, '0') || --107 - 108
        'N' || --109 - 109
        lpad(nvl(dt_emissao_w,'0'), 8, '0') || --110 - 117
        '2' || --118 - 118
        lpad(nvl(dt_vencimento_w,'0'), 8, '0') || --119 - 126
       -- lpad(somente_numero(to_char(tx_multa_w,'9999999999990.00')),15,'0') || --127 - 141       
	lpad(somente_numero(to_char(vl_juros_w,'9999999999990.00000')),15,'0') ||--127 - 141
        '0' || --142 - 142
        '00000000' || --143 - 150
        lpad('0', 15, '0') || --151 - 165
        lpad('0', 15, '0') || --166 - 180
        lpad('0', 15, '0') || --181 - 195
        rpad(' ', 25, ' ') || --196 - 220
        '0' || --221 - 221
        '00' || --222 - 223
        '2' || --224 - 224
        '0' || --225 - 225
        '00' || --226 - 227
        '00' || --228 - 229
        rpad(' ', 11, ' '); --230 - 240

  insert into w_envio_banco
    (nr_sequencia,
    dt_atualizacao,
    nm_usuario,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    cd_estabelecimento,
    ds_conteudo,
    nr_seq_apres,
    nr_seq_apres_2)
  values  (w_envio_banco_seq.nextval,
    sysdate,
    nm_usuario_p,
    sysdate,
    nm_usuario_p,
    cd_estabelecimento_p,
    ds_conteudo_w,
    3,
    nr_seq_registro_w);
  /* Fim segmento P */

  qt_lote_arquivo_w := qt_lote_arquivo_w + 1;
  qt_registro_lote_w  := qt_registro_lote_w + 1;

  /* Segmento Q */
  ds_conteudo_w   := null;
  nr_seq_registro_w := nr_seq_registro_w + 1;

  ds_conteudo_w :=  '033' ||
        lpad(nvl(nr_lote_w,'0'),4,'0') ||
        '3' ||
        lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
        'Q' ||
        rpad(' ',1, ' ') ||
        lpad(nvl(cd_mov_remessa_w,'0'),2,'0') ||
        lpad(nvl(ie_tipo_inscricao_w,'0'),1,'0') ||
        lpad(nvl(nr_inscricao_w,'0'),15,'0') ||
        rpad(nvl(nm_sacado_w,' '),40,' ') ||
        rpad(nvl(ds_endereco_sacado_w,' '),40,' ') ||
        rpad(nvl(ds_bairro_sacado_w,' '),15,' ') ||
        lpad(nvl(cd_cep_sacado_w,'0'),8,'0') ||
        rpad(nvl(ds_municipio_sacado_w,' '),15,' ') ||
        rpad(nvl(ds_estado_sacado_w,' '),2,' ') ||
        lpad('0', 16, '0') ||
        rpad(' ', 40, ' ') ||
        '000' ||
        '000' ||
        '000' ||
        '000' ||
        rpad(' ', 19, ' ');

  insert  into w_envio_banco
    (nr_sequencia,
    dt_atualizacao,
    nm_usuario,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    cd_estabelecimento,
    ds_conteudo,
    nr_seq_apres,
    nr_seq_apres_2)
  values  (w_envio_banco_seq.nextval,
    sysdate,
    nm_usuario_p,
    sysdate,
    nm_usuario_p,
    cd_estabelecimento_p,
    ds_conteudo_w,
    3,
    nr_seq_registro_w);

  /* Fim segmento Q */

  /* Segmento R */
  if  (somente_numero(vl_mora_w) <> 0) then

    ds_conteudo_w   := null;
    nr_seq_registro_w := nr_seq_registro_w + 1;
    qt_registro_lote_w  := qt_registro_lote_w + 1;
    qt_lote_arquivo_w := qt_lote_arquivo_w + 1;

    if  (cd_multa_w = '1') then

      ds_multa_w  := lpad(vl_mora_w, 15, '0');

    else

      ds_multa_w  := lpad(tx_multa_w, 15, '0');

    end if;

    ds_conteudo_w :=  '033' ||
          lpad(nvl(nr_lote_w,'0'), 4, '0') ||
          '3' ||
          lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
          'R' ||
          rpad(' ', 1, ' ') ||
          lpad(nvl(cd_mov_remessa_w,'0'), 2, '0') ||
          '0' ||
          lpad('0', 8, '0') ||
          lpad('0', 15, '0') ||
          rpad(' ', 24, ' ') ||
          lpad(nvl(cd_multa_w,'0'), 1, '0') ||
          lpad(nvl(dt_vencimento_w,'0'), 8, '0') ||
          ds_multa_w ||
          rpad(' ', 10, ' ') ||
          rpad(nvl(ds_mensagem_3_w,' '), 40, ' ') ||
          rpad(nvl(ds_mensagem_4_w,' '), 40, ' ') ||
          rpad(' ', 61, ' ');

    insert  into w_envio_banco
      (nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      cd_estabelecimento,
      ds_conteudo,
      nr_seq_apres,
      nr_seq_apres_2)
    values  (w_envio_banco_seq.nextval,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      cd_estabelecimento_p,
      ds_conteudo_w,
      3,
      nr_seq_registro_w);

  end if;
  /* Fim segmento R */

  /* � obrigat�rio aparecer esse processo judicial no boleto, quando ele existir */
  select  nvl(max('S'),'N')
  into  ie_judicial_w
  from  pls_mensalidade_segurado b,
    processo_judicial_liminar a
  where b.nr_seq_mensalidade  = nr_seq_mensalidade_w
  and a.nr_seq_segurado = b.nr_sequencia
  and a.cd_juridico_liminar = '5830020082380670';

  vetor_mensagem_w.delete;
  nr_vetor_w := 0;

  open C02;
  loop
  fetch C02 into
    ds_conteudo_aux_w;
  exit when C02%notfound;
    begin

    nr_vetor_w        := nvl(nr_vetor_w,0) + 1;
    vetor_mensagem_w(nr_vetor_w).ds_mensagem  := nvl(ds_conteudo_aux_w,'');

    end;
  end loop;
  close C02;

  if  (vetor_mensagem_w.count = 1) then

    /* Segmento S */

    ds_conteudo_w   := null;
    nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
    qt_registro_lote_w  := nvl(qt_registro_lote_w,0) + 1;
    qt_lote_arquivo_w := nvl(qt_lote_arquivo_w,0) + 1;
    nr_linha_w := nr_linha_w + 1;

    ds_conteudo_w :=  substr('033' ||
          lpad(nvl(nr_lote_w,'0'),4,0) ||
          '3' ||
          lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
          'S' ||
          rpad(' ',1,' ') ||
          lpad(nvl(cd_mov_remessa_w,'0'),2,'0') ||
          '1' ||
          lpad(nr_linha_w,2,'0') || -- 019 a 020
          '4' || -- 021 a 021
          substr(nvl(vetor_mensagem_w(nr_vetor_w).ds_mensagem,' '),1,100) ||
          rpad(' ',122,' '),1,240);


    insert  into w_envio_banco
      (nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      cd_estabelecimento,
      ds_conteudo,
      nr_seq_apres,
      nr_seq_apres_2)
    values  (w_envio_banco_seq.nextval,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      cd_estabelecimento_p,
      ds_conteudo_w,
      3,
      nr_seq_registro_w);

    if length(vetor_mensagem_w(nr_vetor_w).ds_mensagem) > 100 then

    ds_conteudo_w   := null;
    nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
    qt_registro_lote_w  := nvl(qt_registro_lote_w,0) + 1;
    qt_lote_arquivo_w := nvl(qt_lote_arquivo_w,0) + 1;
    nr_linha_w := nr_linha_w + 1;

    ds_conteudo_w :=  substr('033' ||
          lpad(nvl(nr_lote_w,'0'),4,0) ||
          '3' ||
          lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
          'S' ||
          rpad(' ',1,' ') ||
          lpad(nvl(cd_mov_remessa_w,'0'),2,'0') ||
          '1' ||
          lpad(nr_linha_w,2,'0') || -- 019 a 020
          '4' || -- 021 a 021
          substr(nvl(vetor_mensagem_w(nr_vetor_w).ds_mensagem,' '),101,100) ||
          rpad(' ',122,' '),1,240);

    end if;

    insert  into w_envio_banco
      (nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      cd_estabelecimento,
      ds_conteudo,
      nr_seq_apres,
      nr_seq_apres_2)
    values  (w_envio_banco_seq.nextval,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      cd_estabelecimento_p,
      ds_conteudo_w,
      3,
      nr_seq_registro_w);

    /* Fim segmento S */

  end if;
   open c03;
    loop
    fetch c03 into
        ds_conteudo_aux_w;
    exit when c03%notfound;
        begin
	
        /* Segmento S */
	ds_conteudo_w   := null;
	nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
	qt_registro_lote_w  := nvl(qt_registro_lote_w,0) + 1;
	qt_lote_arquivo_w := nvl(qt_lote_arquivo_w,0) + 1;
	nr_linha_w := nr_linha_w + 1;

    ds_conteudo_w :=  	'033' ||
			lpad(nvl(nr_lote_w,'0'),4,0) ||
			'3' ||
			lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
			'S' ||
			rpad(' ',1,' ') ||
			lpad(nvl(cd_mov_remessa_w,'0'),2,'0') ||
			'1' ||
			lpad(nr_linha_w,2,'0') || -- 019 a 020
			'4' || -- 021 a 021
			substr(nvl(ds_conteudo_aux_w,' '),1,100) ||
			rpad(' ',119,' ');

            insert into w_envio_banco
                (nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                cd_estabelecimento,
                ds_conteudo,
                nr_seq_apres,
                nr_seq_apres_2)
            values    (w_envio_banco_seq.nextval,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                cd_estabelecimento_p,
                ds_conteudo_w,
                3,
                nr_seq_registro_w);
            /* Fim segmento S */
        end;
    end loop;
    close c03;

  end;
end loop;
close C01;
/*Fim Segmentos */

/* Trailler Lote*/
begin
qt_lote_arquivo_w := qt_lote_arquivo_w + 1;
qt_registro_lote_w  := qt_registro_lote_w + 1;

ds_conteudo_w :=  '033' ||
      lpad(nvl(nr_lote_w,'0'), 4, '0') ||
      '5' ||
      lpad(' ', 9, ' ') ||
      lpad(nvl(qt_registro_lote_w,'0'), 6, '0') ||
      rpad(' ', 217, ' ');

insert  into w_envio_banco
  (nr_sequencia,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  cd_estabelecimento,
  ds_conteudo,
  nr_seq_apres,
  nr_seq_apres_2)
values  (w_envio_banco_seq.nextval,
  sysdate,
  nm_usuario_p,
  sysdate,
  nm_usuario_p,
  cd_estabelecimento_p,
  ds_conteudo_w,
  5,
  5);
end;
/* Fim Trailler Lote*/

/* Trailler Arquivo*/
begin
qt_lote_arquivo_w := qt_lote_arquivo_w + 1;

ds_conteudo_w :=  '033' ||
      '9999' ||
      '9' ||
      lpad(' ', 9, ' ') ||
      lpad(nvl(qt_reg_lote_w,'0'), 6, '0') ||
      lpad(nvl(qt_lote_arquivo_w,'0'), 6, '0') ||
      rpad(' ', 211, ' ');

insert  into w_envio_banco
  (nr_sequencia,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  cd_estabelecimento,
  ds_conteudo,
  nr_seq_apres,
  nr_seq_apres_2)
values  (w_envio_banco_seq.nextval,
  sysdate,
  nm_usuario_p,
  sysdate,
  nm_usuario_p,
  cd_estabelecimento_p,
  ds_conteudo_w,
  6,
  6);
end;
/* Fim Trailler Arquivo*/

commit;

end sc_gerar_cobr_santander_240_1;
/