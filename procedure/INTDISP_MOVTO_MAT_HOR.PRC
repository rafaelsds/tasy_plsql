create or replace
procedure intdisp_movto_mat_hor(	nr_seq_lote_p	ap_lote.nr_sequencia%type,
				ie_status_lote_p	ap_lote.ie_status_lote%type,
				cd_local_estoque_p	local_estoque.cd_local_estoque%type,
				ie_opcao_p		varchar2) is

ds_linha_arquivo_w		varchar2(2000);
nr_seq_lote_w		number(10);
nm_arquivo_w        		varchar2(100);
ds_diretorio_w        		varchar2(255);
ds_diretorio_adep_w    	varchar2(255);
nm_usuario_w        		varchar2(15);
cd_estabelecimento_w    	number(4);    
nr_seq_regra_disp_w    	number(10);
ie_gera_arquivo_w    	varchar2(1);        
ie_imprime_lote_w    	varchar2(1);
ie_gerou_arq_w        	varchar2(1) := 'N';
dt_horario_w        		date;
dt_inicio_prescr_w    	date;
dt_validade_prescr_w    	date;
nr_atendimento_w    	number(10);
qt_dispensar_hor_w    	number(13,4);
qt_dose_w        		number(12,4);
cd_unidade_medida_w    	varchar2(30);
ds_justificativa_w    		varchar2(2000);
ie_forma_integracao_w   	varchar2(1);
nr_sequencia_lote_w    	number(10);
cd_material_w        		number(6);
nr_seq_turno_w        	number(10);
ds_turno_w        		varchar2(80);
ds_classif_urgente_w    	varchar2(80);
dt_dispensacao_w    	date;
hr_dispensacao_w    	date;
cd_setor_atendimento_w  	number(10);
ds_setor_atendimento_w  	varchar2(100);
nm_paciente_w        	varchar2(255);
cd_pessoa_fisica_w    	varchar2(10);
ds_observacao_w        	varchar2(400);
dt_liberacao_w        	date;
ds_leito_w        		varchar2(100);
nr_prescricao_w        	number(14);
dt_inicio_w       		date;
dt_validade_w        		date;
cd_medico_w        		varchar2(10);
ie_classif_urgente_w    	varchar2(3);
ds_intrucao_w        		varchar2(255);
cd_material_generico_w  	number(6);
ds_via_aplicacao_w    	varchar2(255);
dt_lib_horario_w    		date;
cd_crm_w        		varchar2(20);
ds_material_w        		varchar2(255);
ie_dispensar_farm_w    	varchar2(1);
cd_prescritor_w        	varchar2(10);
nr_seq_lote_ind_w    	varchar2(15);
ds_local_estoque_w	varchar2(255);
cd_local_estoque_w	number(6);
nr_seq_estrut_w		number(10);
nr_seq_estrut_int_w		number(10);
ie_mat_estrutura_w		varchar2(1);
cd_local_est_regra_w	number(6);
nr_seq_mat_hor_w		number(10);
cd_local_estoque_hor_w	number(6);
nr_seq_empresa_w	empresa_integracao.nr_sequencia%type;
ie_existe_w			varchar2(1);
ie_swisslog_w		varchar2(1);

nr_seq_superior_w			prescr_material.nr_sequencia%type;

cd_intervalo_w		intervalo_prescricao.cd_intervalo%type;
ds_intervalo_w		intervalo_prescricao.ds_intervalo%type;
nr_seq_material_w	prescr_material.nr_sequencia%type;
/*
ie_opcao_p:
1 - Entrada de material no local de estoque da Pyxis
2 - Saida de material do local de estoque da Pyxis
*/

Cursor C01 is
	select	a.nr_sequencia,
		b.nr_prescricao,
		b.dt_inicio_prescr,
		b.dt_validade_prescr,
		b.nr_atendimento,
		a.cd_material,
		b.cd_medico,
		a.ie_classif_urgente,
		e.cd_material_generico,
		b.cd_estabelecimento,
		a.qt_dose,
		a.cd_unidade_medida_dose,
		obter_desc_via(c.ie_via_aplicacao),
		obter_crm_medico(b.cd_medico),
		a.dt_lib_horario,
		e.ds_material,
		a.nr_seq_lote,
		a.ie_dispensar_farm,
		b.cd_prescritor,
		substr(obter_nome_pf(b.cd_pessoa_fisica),1,60) nm_paciente,
		a.dt_horario,
		c.cd_local_estoque,
		a.cd_local_estoque,
		b.cd_setor_atendimento,
		a.nr_seq_lote || '-' ||ROW_NUMBER() OVER (PARTITION BY a.nr_seq_lote order by a.nr_seq_lote) nr_seq_lote_ind,
		c.cd_intervalo,
		substr(obter_desc_intervalo_prescr(c.cd_intervalo),1,40) ds_intervalo,
		a.nr_seq_material,
		a.nr_seq_superior
	from    prescr_mat_hor a,
		prescr_medica b,
		prescr_material c,
		estrutura_material_v s,
		material e
	where   a.cd_material = s.cd_material
	and     a.nr_prescricao = b.nr_prescricao
	and     c.nr_prescricao = b.nr_prescricao
	and     a.nr_seq_material = c.nr_sequencia
	and     a.cd_material = e.cd_material
	and     a.nr_seq_lote = nr_seq_lote_p
	and     a.nr_seq_lote is not null
	and    	ie_status_lote_p = 'G'
	and     c.ie_suspenso <> 'S'
	and     a.dt_suspensao is null 
	and 	(((ie_opcao_p = 1) and exists (select 1 from local_estoque l where l.cd_local_estoque = cd_local_estoque_p and l.ie_tipo_local = 11)) or
			((ie_opcao_p = 2) and exists (select 1 from int_disp_mat_hor z where z.nr_seq_mat_hor = a.nr_sequencia)))
    	order by a.nr_seq_lote;

begin

if	(ie_opcao_p = '1') then
	begin
	
	open C01;
	loop
	fetch C01 into 
		nr_seq_mat_hor_w,
		nr_prescricao_w,
		dt_inicio_w,
		dt_validade_w,
		nr_atendimento_w,
		cd_material_w,
		cd_medico_w,
		ie_classif_urgente_w,
		cd_material_generico_w,
		cd_estabelecimento_w,
		qt_dose_w,
		cd_unidade_medida_w,
		ds_via_aplicacao_w,
		cd_crm_w,
		dt_lib_horario_w,
		ds_material_w,
		nr_seq_lote_w,
		ie_dispensar_farm_w,
		cd_prescritor_w,
		nm_paciente_w,
		dt_horario_w,
		cd_local_estoque_w,
		cd_local_estoque_hor_w,
		cd_setor_atendimento_w,
		nr_seq_lote_ind_w,
		cd_intervalo_w,
		ds_intervalo_w,
		nr_seq_material_w,
		nr_seq_superior_w;
	exit when C01%notfound;
	begin

	begin
	select	nvl(sum(b.qt_dispensar_hor), 0)
	into	qt_dispensar_hor_w
	from	ap_lote_item a, 
		prescr_mat_hor b
	where	a.nr_seq_mat_hor = b.nr_sequencia
	and	a.nr_seq_lote = nr_seq_lote_p
	and	a.cd_material = cd_material_w
	and	b.nr_seq_material = nr_seq_material_w
	and	a.dt_supensao is null
	and	ie_status_lote_p = 'G'
	and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
	having	sum(a.qt_dispensar) > 0;
	exception
	when others then
		qt_dispensar_hor_w := 0;
	end;

	begin
	select	nr_sequencia
	into	nr_seq_empresa_w
	from	empresa_integracao
	where	upper(nm_empresa) = 'SWISSLOG';
	
	select	'S'
	into	ie_existe_w
	from	far_setores_integracao
	where	nr_seq_empresa_int = nr_seq_empresa_w
	and	cd_setor_atendimento = cd_setor_atendimento_w;
	exception
	when others then
		nr_seq_empresa_w := 0;
		ie_existe_w := 'N';
	end;
	
	ie_swisslog_w := 'N';
	if	(ie_existe_w = 'S') then
		select	nvl(max('S'), 'N')
		into	ie_swisslog_w
		from	prescr_mat_hor a,
				prescr_medica b,
				(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
						cd_estabelecimento,
						cd_local_estoque_req
				from	parametros_farmacia) c
		where	a.nr_seq_lote is not null
		and		a.qt_dispensar_hor > 0
		and		consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,a.cd_material) = 'S'
		and		a.cd_local_estoque = c.cd_local_estoque_req
		and		b.cd_estabelecimento = c.cd_estabelecimento
		and		a.nr_prescricao = b.nr_prescricao
		and		a.nr_sequencia = nr_seq_mat_hor_w;
	end if;
	
	
	if	(qt_dispensar_hor_w > 0) and (ie_swisslog_w = 'N') then
	
		begin
		select	substr(obter_desc_local_estoque(nvl(nvl(cd_local_estoque_p, cd_local_estoque_hor_w),cd_local_estoque_w)),1,100) ds_local_estoque
		into	ds_local_estoque_w
		from	dual;
		exception
		when others then
			ds_local_estoque_w := null;
		end;
		
		if	(nr_seq_lote_w is not null) then
			begin
			insert into int_disp_mat_hor(
				nr_sequencia,
				nr_prescricao,
				dt_inicio_prescr,
				dt_validade_prescr,
				nr_atendimento,
				cd_material,
				cd_medico,
				ds_classif_urgente,
				cd_material_generico,
				cd_estabelecimento,
				qt_dispensar_hor,
				qt_dose,
				cd_unidade_medida_dose,
				ds_via_adm,
				crm_medico,
				dt_lib_horario,
				ds_material,
				nr_seq_lote,
				ie_dispensar_farm,
				nm_prescritor,
				nm_paciente,
				dt_horario,
				nr_seq_lote_ind,
				ie_tipo_movimentacao,
				cd_local_estoque,
				ds_local_estoque,
				nr_seq_mat_hor,
				cd_intervalo,
				ds_intervalo,
				dt_atualizacao,
				nr_seq_material,
				nr_seq_superior)
			values(	int_disp_mat_hor_seq.nextval,
				nr_prescricao_w,
				dt_inicio_w,
				dt_validade_w,
				nr_atendimento_w,
				cd_material_w,
				cd_medico_w,
				ie_classif_urgente_w,
				cd_material_generico_w,
				cd_estabelecimento_w,
				qt_dispensar_hor_w,
				qt_dose_w,
				cd_unidade_medida_w,
				ds_via_aplicacao_w,
				cd_crm_w,
				dt_lib_horario_w,
				ds_material_w,
				nr_seq_lote_p,
				ie_dispensar_farm_w,
				cd_prescritor_w,
				nm_paciente_w,
				dt_horario_w,
				nr_seq_lote_ind_w,
				'EOA',
				nvl(nvl(cd_local_estoque_p, cd_local_estoque_hor_w),cd_local_estoque_w),
				ds_local_estoque_w,
				nr_seq_mat_hor_w,
				cd_intervalo_w,
				ds_intervalo_w,
				sysdate,
				nr_seq_material_w,
				nr_seq_superior_w);
			
			end;
		end if;	
	end if;
	
	end;
	end loop;
	close C01;
	end;
	
elsif	(ie_opcao_p = '2') then

	begin
	open C01;
	loop
	fetch C01 into 
		nr_seq_mat_hor_w,
		nr_prescricao_w,
		dt_inicio_w,
		dt_validade_w,
		nr_atendimento_w,
		cd_material_w,
		cd_medico_w,
		ie_classif_urgente_w,
		cd_material_generico_w,
		cd_estabelecimento_w,
		qt_dose_w,
		cd_unidade_medida_w,
		ds_via_aplicacao_w,
		cd_crm_w,
		dt_lib_horario_w,
		ds_material_w,
		nr_seq_lote_w,
		ie_dispensar_farm_w,
		cd_prescritor_w,
		nm_paciente_w,
		dt_horario_w,
		cd_local_estoque_w,
		cd_local_estoque_hor_w,
		cd_setor_atendimento_w,
		nr_seq_lote_ind_w,
		cd_intervalo_w,
		ds_intervalo_w,
		nr_seq_material_w,
		nr_seq_superior_w;
	exit when C01%notfound;
	begin

	begin
	select	nvl(sum(b.qt_dispensar_hor), 0)
	into	qt_dispensar_hor_w
	from	ap_lote_item a, 
		prescr_mat_hor b
	where	a.nr_seq_mat_hor = b.nr_sequencia
	and	a.nr_seq_lote = nr_seq_lote_p
	and	a.cd_material = cd_material_w
	and	b.nr_seq_material = nr_seq_material_w
	and	a.dt_supensao is null
	and	ie_status_lote_p = 'G'
	and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
	having	sum(a.qt_dispensar) > 0;
	exception
	when others then
		qt_dispensar_hor_w := 0;
	end;

	begin
	
	select	nr_sequencia
	into	nr_seq_empresa_w
	from	empresa_integracao
	where	upper(nm_empresa) = 'SWISSLOG';
	
	select	'S'
	into	ie_existe_w
	from	far_setores_integracao
	where	nr_seq_empresa_int = nr_seq_empresa_w
	and	cd_setor_atendimento = cd_setor_atendimento_w;
	exception
	when others then
		nr_seq_empresa_w := 0;
		ie_existe_w := 'N';
	end;
	
	ie_swisslog_w := 'N';
	if	(ie_existe_w = 'S') then
		select	nvl(max('S'), 'N')
		into	ie_swisslog_w
		from	prescr_mat_hor a,
				prescr_medica b,
				(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
						cd_estabelecimento,
						cd_local_estoque_req
				from	parametros_farmacia) c
		where	a.nr_seq_lote is not null
		and		a.qt_dispensar_hor > 0
		and		consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,a.cd_material) = 'S'
		and		a.cd_local_estoque = c.cd_local_estoque_req
		and		b.cd_estabelecimento = c.cd_estabelecimento
		and		a.nr_prescricao = b.nr_prescricao
		and		a.nr_sequencia = nr_seq_mat_hor_w;
	end if;
	
	
	if	(qt_dispensar_hor_w > 0) and (ie_swisslog_w = 'N') then
	
		select	max(substr(obter_desc_local_estoque(nvl(nvl(cd_local_estoque_p, cd_local_estoque_hor_w),cd_local_estoque_w)),1,100))ds_local_estoque
		into	ds_local_estoque_w
		from	dual;
		
		if	(nr_seq_lote_w is not null) then
			begin
			insert into int_disp_mat_hor(
				nr_sequencia,
				nr_prescricao,
				dt_inicio_prescr,
				dt_validade_prescr,
				nr_atendimento,
				cd_material,
				cd_medico,
				ds_classif_urgente,
				cd_material_generico,
				cd_estabelecimento,
				qt_dispensar_hor,
				qt_dose,
				cd_unidade_medida_dose,
				ds_via_adm,
				crm_medico,
				dt_lib_horario,
				ds_material,
				nr_seq_lote,
				ie_dispensar_farm,
				nm_prescritor,
				nm_paciente,
				dt_horario,
				nr_seq_lote_ind,
				ie_tipo_movimentacao,
				cd_local_estoque,
				ds_local_estoque,
				nr_seq_mat_hor,
				cd_intervalo,
				ds_intervalo,
				dt_atualizacao,
				nr_seq_material,
				nr_seq_superior)
			values(	int_disp_mat_hor_seq.nextval,
				nr_prescricao_w,
				dt_inicio_w,
				dt_validade_w,
				nr_atendimento_w,
				cd_material_w,
				cd_medico_w,
				ie_classif_urgente_w,
				cd_material_generico_w,
				cd_estabelecimento_w,
				qt_dispensar_hor_w,
				qt_dose_w,
				cd_unidade_medida_w,
				ds_via_aplicacao_w,
				cd_crm_w,
				dt_lib_horario_w,
				ds_material_w,
				nr_seq_lote_p,
				ie_dispensar_farm_w,
				cd_prescritor_w,
				nm_paciente_w,
				dt_horario_w,
				nr_seq_lote_ind_w,
				'EOD',
				nvl(nvl(cd_local_estoque_p, cd_local_estoque_hor_w),cd_local_estoque_w),
				ds_local_estoque_w,
				nr_seq_mat_hor_w,
				cd_intervalo_w,
				ds_intervalo_w,
				sysdate,
				nr_seq_material_w,
				nr_seq_superior_w);
			
			end;
		end if;	
	end if;
	
	end;
	end loop;
	close C01;
	end;
end if;

--if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end intdisp_movto_mat_hor;
/
