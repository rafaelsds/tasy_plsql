create or replace
procedure cih_importar_transplantes(	nr_sequencia_p		varchar2,
					nr_ficha_ocorrencia_p	varchar2,
					nm_usuario_p		varchar2) is
				
	nr_seq_orgao_w		number(10);
	ie_retransplante_w	varchar2(1);
	ie_tipo_doador_w	varchar2(15);
	dt_transplante_w	date;
	
begin

if ((nr_sequencia_p is not null) and (nr_ficha_ocorrencia_p is not null)) then
	
	select	max(nr_seq_orgao),
		max(ie_retransplante),
		max(dt_transplante)
	into	nr_seq_orgao_w,
		ie_retransplante_w,
		dt_transplante_w
	from	tx_receptor 
	where	nr_sequencia = nr_sequencia_p;
	
	select	max(d.ie_tipo_doador)
	into	ie_tipo_doador_w
	from	tx_receptor a,
		tx_doador_potencial b,
		tx_doador_orgao c,
		tx_doador d
	where	b.nr_seq_receptor 	= a.nr_sequencia
	and	b.nr_seq_doador_orgao 	= c.nr_sequencia
	and	c.nr_seq_doador		= d.nr_sequencia	
	and	a.nr_sequencia = nr_sequencia_p;

	if	(ie_tipo_doador_w is null) then		
		wheb_mensagem_pck.exibir_mensagem_abort(258782);		
	end if;

	insert into cih_dados_transplante
	       (nr_sequencia,
		nr_ficha_ocorrencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_receptor,
		nr_seq_orgao,
		dt_transplante,
		ie_retransplante,
		ie_tipo_doador)
	values (cih_dados_transplante_seq.nextval,
		nr_ficha_ocorrencia_p,
		SYSDATE,
		nm_usuario_p,
		SYSDATE,
		nm_usuario_p,
		nr_sequencia_p,
		nr_seq_orgao_w,
		dt_transplante_w,
		ie_retransplante_w,
		ie_tipo_doador_w);

end if;

commit;

END cih_importar_transplantes;
/
