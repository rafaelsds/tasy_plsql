create or replace procedure desfazer_desdob_parcial 	(	
							nr_seq_retorno_p 	in convenio_retorno.nr_sequencia%type,
							ie_status_retorno_p	in convenio_retorno.ie_status_retorno%type,
							nm_usuario_p 		in convenio_retorno.nm_usuario%type
							) is

ds_valores_originais_w			convenio_retorno_item.ds_valores_originais%type;
ds_valores_originais_item_w		convenio_retorno_glosa.ds_valores_originais%type;
ds_retorno_estorno_w			varchar2(2000);



ds_lista_valores	varchar2(2000);
ds_lista_valores_aux varchar2(2000);
ds_atributo     varchar2(255);
ds_pos_pontovirgula   number;
ds_pos_igual            number;
tamanho_lista       number;
contador            number;
inicio      number;
i           number;

nr_seq_retorno_w		convenio_retorno.nr_sequencia%type;

nr_seq_retorno_atual_w		convenio_retorno.nr_sequencia%type;

nr_seq_retorno_iterador_w	convenio_retorno.nr_sequencia%type;
nr_seq_ret_orig_iter_w		convenio_retorno.nr_seq_ret_origem%type;
ie_status_ret_iterador_w	convenio_retorno.ie_status_retorno%type;

nr_seq_atual_w			convenio_retorno.nr_sequencia%type;
ie_status_atual_w		convenio_retorno.ie_status_retorno%type;
nr_seq_orig_atual_w		convenio_retorno.nr_seq_ret_origem%type;

cursor 	guias is
select	nr_sequencia,	
	ds_valores_originais
from	convenio_retorno_item
where	nr_seq_retorno = nr_seq_retorno_w
and	ds_valores_originais is not null;

type atributo is record (
	nome varchar2(200),
	valor number(15,2)
);

type	ret_item is record (
        VL_TRIBUTO_GUIA		NUMBER,
        VL_SINISTRO		NUMBER,
        VL_PERDAS		NUMBER,
        VL_PAGO			NUMBER,
        VL_NOTA_CREDITO		NUMBER,
        VL_MULTA_COBR		NUMBER,
        VL_JUROS_COBR		NUMBER,
        VL_JUROS		NUMBER,
        VL_GUIA			NUMBER,
        VL_GLOSADO_POST		NUMBER,
        VL_GLOSADO		NUMBER,
        VL_DESCONTO		NUMBER,
        VL_COPARTICIPACAO	NUMBER,
        VL_CAMBIAL_PASSIVO	NUMBER,
        VL_CAMBIAL_ATIVO	NUMBER,
        VL_AMENOR_POST		NUMBER,
        VL_AMENOR		NUMBER,
        VL_ADICIONAL		NUMBER,
        VL_ADEQUADO		NUMBER
);        

type	ret_item_tb is table of ret_item index by binary_integer;

ret_item_v 	ret_item_tb;

type ret_reg is record (
	nr_sequencia 		convenio_retorno.nr_sequencia%type,
	ie_status 		convenio_retorno.ie_status_retorno%type,
	nr_seq_ret_origem	convenio_retorno.nr_seq_ret_origem%type
);

type ret_reg_tb is table of ret_reg index by binary_integer;

lista_retornos_v	ret_reg_tb;

idx binary_integer;

begin
dbms_application_info.SET_ACTION('DESFAZER_DESDOB_PARCIAL');

if	nvl(nr_seq_retorno_p, 0) > 0 then
	
	nr_seq_retorno_atual_w := nr_seq_retorno_p;	
	idx := 0;
	
	while(idx >= 0) loop
		
		select 	max(nr_sequencia),
			max(ie_status_retorno),
			max(nr_seq_ret_origem)
		into	nr_seq_retorno_iterador_w,
			ie_status_ret_iterador_w,
			nr_seq_ret_orig_iter_w
		from	convenio_retorno
		where	nr_seq_ret_origem = nr_seq_retorno_atual_w;
		
		if nr_seq_retorno_iterador_w is not null then
			lista_retornos_v(idx).nr_sequencia := nr_seq_retorno_iterador_w;
			lista_retornos_v(idx).ie_status := ie_status_ret_iterador_w;
			lista_retornos_v(idx).nr_seq_ret_origem := nr_seq_ret_orig_iter_w;
			nr_seq_retorno_atual_w := nr_seq_retorno_iterador_w;
			idx := idx + 1;
		else
			idx := -1;
		end if;
		
	end loop;
	
	if lista_retornos_v.count > 0 then
	
		idx := 0;
		for idx in reverse lista_retornos_v.first .. lista_retornos_v.last loop
			nr_seq_atual_w 		:= lista_retornos_v(idx).nr_sequencia;
			ie_status_atual_w 	:= lista_retornos_v(idx).ie_status;
			nr_seq_orig_atual_w	:= lista_retornos_v(idx).nr_seq_ret_origem;
			
			dbms_output.put_line('Retorno = ' || nr_seq_atual_w || ' / Status = ' || ie_status_atual_w || ' / Origem = ' || nr_seq_orig_atual_w);
			
		
			if ie_status_atual_w = 'F' then
				dbms_output.put_line('Estornando Retorno = ' || nr_seq_atual_w);
				ESTORNAR_BAIXA_TITULO_CONVENIO(nr_seq_atual_w, null, null, nm_usuario_p, ds_retorno_estorno_w);
				
				/*
					Se estornou, o novo retorno gerado mantem o vinculo com a origem do retorno originalmente estornado.
				*/
				
				select 	max(nr_sequencia),
					max(ie_status_retorno)
				into	nr_seq_atual_w,
					ie_status_atual_w
				from 	convenio_retorno
				where	nr_seq_ret_origem = nr_seq_orig_atual_w;
				
				dbms_output.put_line('Retorno gerado apos o estorno = ' || nr_seq_atual_w);
				
			end if;
			
			if ie_status_atual_w = 'V' then
				dbms_output.put_line('Desvinculando Retorno = ' || nr_seq_atual_w);
				desvincultar_retorno_convenio(nr_seq_atual_w, nm_usuario_p);
				
				select 	ie_status_retorno
				into	ie_status_atual_w
				from 	convenio_retorno
				where	nr_sequencia = nr_seq_atual_w;
			end if;
			
			if ie_status_atual_w = 'C' then
				dbms_output.put_line('Estornando Consistir Retorno = ' || nr_seq_atual_w);
				estornar_consistir_retorno(nr_seq_atual_w, nm_usuario_p);
			end if;
			
			dbms_output.put_line('Deletando itens Retorno = ' || nr_seq_atual_w);
			
			delete from 	convenio_retorno_glosa
			where		nr_seq_ret_item in (select x.nr_sequencia from convenio_retorno_item x where x.nr_seq_retorno = nr_seq_atual_w);
			

			dbms_output.put_line('Deletando guias Retorno = ' || nr_seq_atual_w);
			
			delete from 	convenio_retorno_item
			where		nr_seq_retorno 	= nr_seq_atual_w;
			

			dbms_output.put_line('Deletando movimento Retorno = ' || nr_seq_atual_w);
			
			delete from convenio_retorno_movto
			where	nr_seq_retorno = nr_seq_atual_w;
			

			dbms_output.put_line('Deletando Retorno = ' || nr_seq_atual_w);
			
			delete from convenio_retorno
			where	nr_sequencia = nr_seq_atual_w;

		end loop;
		
	end if;
	
	nr_seq_retorno_w := nr_seq_retorno_p;
	
	if ie_status_retorno_p = 'F' then
	
		dbms_output.put_line('Estornando Proprio retorno ');
		ESTORNAR_BAIXA_TITULO_CONVENIO(nr_seq_retorno_w, null, null, nm_usuario_p, ds_retorno_estorno_w);
		
		select 	max(nr_sequencia)
		into	nr_seq_retorno_w
		from	convenio_retorno
		where	nm_usuario_retorno = nm_usuario_p
		and	ie_status_retorno = 'R';
		
		dbms_output.put_line('Retorno gerado apos retorno proprio = ' || nr_seq_retorno_w);
		
	end if;
	
	for guia in guias loop
		ds_lista_valores := guia.ds_valores_originais;
		tamanho_lista := length(ds_lista_valores);
		inicio := 0;
		i := 0;
		contador := 1;
		ds_atributo := '';
		ds_pos_pontovirgula := 0;
		ds_pos_igual := 0;
		while contador <= tamanho_lista LOOP
			i := i + 1;
			if	instr(ds_lista_valores,';') = i then
				ds_pos_pontovirgula := i;
				ds_atributo := '';
			elsif   instr(ds_lista_valores,'=') = i then
			      ds_pos_igual := i;
			      ds_lista_valores := substr(ds_lista_valores,i + 1,tamanho_lista);
			      FOR j IN 1..length(ds_lista_valores) LOOP
				  if    instr(ds_lista_valores,';') = j then
					if  	(ds_atributo = 'VL_ADEQUADO') then
					    ret_item_v(0).VL_ADEQUADO := substr(ds_lista_valores,1,j-1); 
					elsif (ds_atributo = 'VL_ADICIONAL') then
					    ret_item_v(0).VL_ADICIONAL := substr(ds_lista_valores,1,j-1); 
					elsif (ds_atributo = 'VL_AMENOR') then
					    ret_item_v(0).VL_AMENOR := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_AMENOR_POST') then
					    ret_item_v(0).VL_AMENOR_POST := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_CAMBIAL_ATIVO') then
					    ret_item_v(0).VL_CAMBIAL_ATIVO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_CAMBIAL_PASSIVO') then
					    ret_item_v(0).VL_CAMBIAL_PASSIVO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_COPARTICIPACAO') then
					    ret_item_v(0).VL_COPARTICIPACAO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_DESCONTO') then
					    ret_item_v(0).VL_DESCONTO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_GLOSADO') then
					    ret_item_v(0).VL_GLOSADO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_GLOSADO_POST') then
					    ret_item_v(0).VL_GLOSADO_POST := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_GUIA') then
					    ret_item_v(0).VL_GUIA := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_JUROS') then
					    ret_item_v(0).VL_JUROS := substr(ds_lista_valores,1,j-1);
					 elsif (ds_atributo = 'VL_JUROS_COBR') then
					    ret_item_v(0).VL_JUROS_COBR := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_MULTA_COBR') then
					    ret_item_v(0).VL_MULTA_COBR := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_NOTA_CREDITO') then
					    ret_item_v(0).VL_NOTA_CREDITO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_PAGO') then
					    ret_item_v(0).VL_PAGO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_PERDAS') then
					    ret_item_v(0).VL_PERDAS := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_SINISTRO') then
					    ret_item_v(0).VL_SINISTRO := substr(ds_lista_valores,1,j-1);
					elsif (ds_atributo = 'VL_TRIBUTO_GUIA') then
					    ret_item_v(0).VL_TRIBUTO_GUIA := substr(ds_lista_valores,1,j-1);
					end if;
					ds_lista_valores := substr(ds_lista_valores,j+1,length(ds_lista_valores));
					ds_atributo := '';
					i := 0;
					exit;
				  end if;
			      END LOOP;
			else
			    ds_atributo :=  ds_atributo || substr(ds_lista_valores,i,1);        
			end if;
			contador := contador + 1;
		END LOOP;
		
		update 	convenio_retorno_item
		set	vl_pago = ret_item_v(0).vl_pago,
			vl_adicional = ret_item_v(0).vl_adicional,
			vl_amenor = ret_item_v(0).vl_amenor,
			vl_amenor_post = ret_item_v(0).vl_amenor_post,
			vl_cambial_ativo = ret_item_v(0).vl_cambial_ativo,
			vl_cambial_passivo = ret_item_v(0).vl_cambial_passivo,
			vl_coparticipacao = ret_item_v(0).vl_coparticipacao,
			vl_desconto = ret_item_v(0).vl_desconto,
			vl_glosado = ret_item_v(0).vl_glosado,
			vl_glosado_post = ret_item_v(0).vl_glosado_post,
			vl_guia = ret_item_v(0).vl_guia,
			vl_juros = ret_item_v(0).vl_juros,
			vl_juros_cobr = ret_item_v(0).vl_juros_cobr,
			vl_multa_cobr = ret_item_v(0).vl_multa_cobr,
			vl_nota_credito = ret_item_v(0).vl_nota_credito,
			vl_perdas = ret_item_v(0).vl_perdas,
			vl_sinistro = ret_item_v(0).vl_sinistro,
			vl_tributo_guia = ret_item_v(0).vl_tributo_guia,
			vl_adequado = ret_item_v(0).vl_adequado
		where	nr_sequencia = guia.nr_sequencia;
		
		desfazer_desdob_parcial_item(guia.nr_sequencia);
		
	end loop;	
	
end if;

commit;

dbms_application_info.SET_ACTION('');

end desfazer_desdob_parcial;
/
