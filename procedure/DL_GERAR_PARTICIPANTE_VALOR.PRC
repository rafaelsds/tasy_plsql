create or replace
procedure dl_gerar_participante_valor(
			nr_seq_reuniao_p	number,
			vl_reuniao_p		number,
			nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);	
ie_presente_w		varchar2(255);
nr_seq_motivo_aus_w	number(10);
ie_ausencia_w		varchar2(255);
			
cursor C01 is
	select	a.nr_sequencia,
		a.ie_presente,
		a.nr_seq_motivo_aus
	from	dl_reuniao_socio a
	where	a.nr_seq_reuniao	= nr_seq_reuniao_p
	order by a.nr_sequencia;				
			
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ie_presente_w,
	nr_seq_motivo_aus_w;
exit when C01%notfound;
	begin

	if	(ie_presente_w = 'S') then
		update	dl_reuniao_socio
		set	vl_reuniao	= vl_reuniao_p
		where	nr_sequencia	= nr_sequencia_w
		and	nr_seq_reuniao	= nr_seq_reuniao_p;
	else

		if	nr_seq_motivo_aus_w is not null then
		
			select	ie_ausencia 
			into	ie_ausencia_w
			from 	dl_motivo_ausencia 
			where 	nr_sequencia	= nr_seq_motivo_aus_w;

			if	(ie_ausencia_w = 'J') then
				update	dl_reuniao_socio
				set	vl_reuniao	= vl_reuniao_p
				where	nr_sequencia	= nr_sequencia_w
				and	nr_seq_reuniao	= nr_seq_reuniao_p;	
			else
				update	dl_reuniao_socio
				set	vl_reuniao	= 0
				where	nr_sequencia	= nr_sequencia_w
				and	nr_seq_reuniao	= nr_seq_reuniao_p;			
			end if;
			
		else
			update	dl_reuniao_socio
			set	vl_reuniao	= 0
			where	nr_sequencia	= nr_sequencia_w
			and	nr_seq_reuniao	= nr_seq_reuniao_p;		
		end if;
	end if;	
	
	end;
end loop;
close C01;

commit;

end dl_gerar_participante_valor;
/
