create or replace
procedure ptu_gerar_intercambio_benef
			(	nr_seq_empresa_p	number,
				cd_unimed_destino_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				nr_seq_contrato_p	number) is

nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
cd_cooperativa_w		varchar2(4);
cd_usuario_plano_w		varchar2(15);
nm_beneficiario_w		varchar2(120);
cd_plano_intercambio_w		varchar2(3);
dt_nascimento_w			date;
ie_sexo_w			varchar2(1);
ie_estado_civil_w		varchar2(1);
dt_inclusao_operadora_w		date;
ie_preco_w			varchar2(1);
nr_seq_parentesco_w		number(5);
nr_cpf_w			varchar2(14);
nr_identidade_w			varchar2(15);
sg_emissora_ci_w		varchar2(2);
dt_rescisao_w			date;
ie_nascido_plano_w		varchar2(1);
dt_validade_carteira_w		date;
nr_seq_tabela_origem_w		number(10);
nm_abreviado_w			varchar2(25);
cd_plano_w			varchar2(6);
nr_seq_titular_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_titular_plano_w		varchar2(15);
nr_seq_segurado_ww		number(10);
nr_seq_beneficiario_w		Number(10);
nr_seq_congenere_w		number(10);
cd_grau_parentesco_w		varchar2(2);
cd_cgc_outorgante_w		varchar2(14);
ie_repasse_w			varchar2(1);
dt_repasse_w			date;
dt_fim_repasse_w		date;
dt_liberacao_w			pls_segurado_repasse.dt_liberacao%type;
dt_fim_real_w			date;
nr_seq_tabela_w			number(10);
nr_vigencia_origem_w		number(4);
cd_familia_w			number(6);
nr_matricula_w			varchar2(20);
cd_local_atendimento_w		varchar2(4);
nr_seq_plano_repasse_w		number(10);
nr_seq_plano_w			number(10);
ie_tipo_contrato_w		varchar2(1);
nr_seq_intercambio_w		number(10);
nr_seq_lote_intercambio_w	number(10);
dt_mov_inicial_w		date;
dt_mov_final_w			date;
ie_tipo_movimentacao_w		varchar2(1);
nr_seq_congenere_atend_w	number(10);
qt_registros_w			number(10);
nr_seq_seg_repasse_w		number(10);
ie_tipo_registro_w		varchar2(10);
nr_cartao_nac_sus_w		varchar2(20);
nm_mae_benef_w			varchar2(60);
nr_pis_pasep_w			varchar2(11);
nr_idade_benef_w		number(10);
nr_versao_transacao_w		number(2);
ie_gerar_prod_agregado_w 	ptu_intercambio_lote_envio.ie_gerar_prod_agregado%type;
ie_gerar_liberacao_a100_w	pls_parametros.ie_gerar_liberacao_a100%type;
ie_segmentacao_w		pls_plano.ie_segmentacao%type;
cd_segmentacao_ptu_w		ptu_intercambio_benef.cd_segmentacao_ptu%type;
ie_exclusao_rn412_w		ptu_intercambio_benef.ie_exclusao_rn412%type;
nr_seq_motivo_cancelamento_w	pls_motivo_cancelamento.nr_sequencia%type;
cd_motivo_exclusao_w		pls_motivo_cancelamento.cd_intercambio%type;
ie_tipo_compartilhamento_w	ptu_intercambio_lote_envio.ie_tipo_compartilhamento%type;
nm_social_w			ptu_intercambio_benef.nm_social%type;
nm_social_abreviado_w		ptu_intercambio_benef.nm_social_abreviado%type;
dt_inicio_repasse_w		date;
dt_fim_repasse_ww		date;
cd_genero_w			genero.cd_intercambio%type;
nr_seq_genero_w			pessoa_fisica_aux.nr_seq_genero%type;

Cursor C01 is
	select	nr_seq_segurado,
		max(nr_seq_seg_repasse) nr_seq_seg_repasse,
		dt_repasse,
		dt_fim_repasse
	from	(	--Contrato de plano de saude
			select	a.nr_sequencia nr_seq_segurado,	
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_contrato		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_contrato	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'P'
			and	ie_tipo_movimentacao_w	= 'A'
			and	dt_mov_inicial_w between trunc(b.dt_repasse,'dd') and nvl(trunc(b.dt_fim_repasse,'dd'),fim_mes(dt_mov_inicial_w))
			and	(trunc(b.dt_fim_repasse,'dd') > dt_mov_inicial_w or
				(b.dt_fim_repasse is null))
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_contrato		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_contrato	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'P'
			and	ie_tipo_movimentacao_w	= 'M'
			and	trunc(b.dt_repasse,'dd') <= dt_mov_inicial_w
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_contrato		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_contrato	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'P'
			and	ie_tipo_movimentacao_w	= 'P'
			and 	ie_gerar_liberacao_a100_w = 'N'
			and	((b.dt_repasse	between dt_mov_inicial_w  and dt_mov_final_w) or
				(b.dt_fim_repasse between dt_mov_inicial_w  and dt_mov_final_w))
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_contrato		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_contrato	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'P'
			and	ie_tipo_movimentacao_w	= 'P'
			and 	ie_gerar_liberacao_a100_w = 'S'
			and	((b.dt_liberacao between dt_mov_inicial_w  and dt_mov_final_w) or
				(nvl(b.dt_fim_real, b.dt_fim_repasse) between dt_mov_inicial_w  and dt_mov_final_w))
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			--Contrato de intercambio
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_intercambio		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_intercambio	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'I'
			and	ie_tipo_movimentacao_w	= 'A'
			and	dt_mov_inicial_w between trunc(b.dt_repasse,'dd') and nvl(trunc(b.dt_fim_repasse,'dd'),fim_dia(dt_mov_inicial_w))
			and	(trunc(b.dt_fim_repasse,'dd') > dt_mov_inicial_w or
				(b.dt_fim_repasse is null))
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_intercambio		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_intercambio	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'I'
			and	ie_tipo_movimentacao_w	= 'M'
			and	trunc(b.dt_repasse,'dd') <= dt_mov_inicial_w
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_intercambio		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_intercambio	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'I'
			and	ie_tipo_movimentacao_w	= 'P'
			and 	ie_gerar_liberacao_a100_w = 'N'
			and	((b.dt_repasse between dt_mov_inicial_w  and dt_mov_final_w) or
				(b.dt_fim_repasse between dt_mov_inicial_w  and dt_mov_final_w))
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null))
			union
			select	a.nr_sequencia nr_seq_segurado,
				a.nr_seq_titular,
				b.nr_sequencia nr_seq_seg_repasse,
				b.dt_repasse,
				b.dt_fim_repasse
			from	pls_segurado		a,
				pls_segurado_repasse	b,
				pls_congenere		c,
				pls_intercambio		d
			where	b.nr_seq_segurado	= a.nr_sequencia
			and	b.nr_seq_congenere	= c.nr_sequencia
			and	a.nr_seq_intercambio	= d.nr_sequencia
			and	d.nr_sequencia		= nr_seq_contrato_w
			and	c.nr_sequencia		= cd_unimed_destino_p
			and	ie_tipo_contrato_w	= 'I'
			and	ie_tipo_movimentacao_w	= 'P'
			and 	ie_gerar_liberacao_a100_w = 'S'
			and	((b.dt_liberacao between dt_mov_inicial_w  and dt_mov_final_w) or
				(nvl(b.dt_fim_real, b.dt_fim_repasse) between dt_mov_inicial_w  and dt_mov_final_w))
			and	a.dt_liberacao is not null
			and	b.dt_liberacao is not null
			and	((b.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento_w is null)))
	group by  nr_seq_segurado , dt_repasse, dt_fim_repasse,nr_seq_titular		
	order by nr_seq_titular desc;

begin

select	nr_seq_contrato,
	ie_tipo_contrato,
	nr_seq_intercambio
into	nr_seq_contrato_w,
	ie_tipo_contrato_w,
	nr_seq_intercambio_w
from	ptu_intercambio_empresa
where	nr_sequencia	= nr_seq_empresa_p;

--Este parametro e informado apenas quando ja houver registro para a empresa, e existem contratos diferentes para a mesma.
if	(nr_seq_contrato_p is not null)  then
	nr_seq_contrato_w := nr_seq_contrato_p;
end if;

select	max(cd_cgc_outorgante)
into	cd_cgc_outorgante_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(nr_sequencia)
into	nr_seq_congenere_w
from	pls_congenere
where	cd_cgc	= cd_cgc_outorgante_w
and	ie_tipo_congenere = 'CO';

select	nr_seq_lote_envio,
	trunc(dt_mov_inicio,'dd'),
	fim_dia(dt_mov_fim),
	nr_versao_transacao
into	nr_seq_lote_intercambio_w,
	dt_mov_inicial_w,
	dt_mov_final_w,
	nr_versao_transacao_w
from	ptu_intercambio
where	nr_sequencia	= nr_seq_intercambio_w;

select	ie_tipo_movimento,
	ie_gerar_prod_agregado,
	ie_tipo_compartilhamento
into	ie_tipo_movimentacao_w,
	ie_gerar_prod_agregado_w,
	ie_tipo_compartilhamento_w
from	ptu_intercambio_lote_envio
where	nr_sequencia	= nr_seq_lote_intercambio_w;

select 	nvl(ie_gerar_liberacao_a100,'N')
into	ie_gerar_liberacao_a100_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

open C01;
loop
fetch C01 into
	nr_seq_segurado_w,
	nr_seq_seg_repasse_w,
	dt_inicio_repasse_w,
	dt_fim_repasse_ww;
exit when C01%notfound;
	begin
	if	((dt_fim_repasse_ww is null) or
		(dt_fim_repasse_ww > dt_inicio_repasse_w)) then
		select	max(cd_cooperativa)
		into	cd_cooperativa_w
		from	pls_congenere
		where	nr_sequencia	= nr_seq_congenere_w;
		
		select	cd_cooperativa
		into	cd_local_atendimento_w
		from	pls_congenere
		where	nr_sequencia	= cd_unimed_destino_p;
		
		select	dt_repasse,
			dt_fim_repasse,
			dt_liberacao,
			dt_fim_real,
			ie_tipo_repasse,
			nr_seq_plano,
			nr_seq_congenere_atend
		into	dt_repasse_w,
			dt_fim_repasse_w,
			dt_liberacao_w,
			dt_fim_real_w,
			ie_repasse_w,
			nr_seq_plano_repasse_w,
			nr_seq_congenere_atend_w
		from	pls_segurado_repasse
		where	nr_sequencia	= nr_seq_seg_repasse_w;
		
		if	(nr_seq_congenere_atend_w is not null) then
			select	max(cd_cooperativa)
			into	cd_local_atendimento_w
			from	pls_congenere
			where	nr_sequencia	= nr_seq_congenere_atend_w;
		end if;
		
		select	substr(ptu_somente_caracter_permitido(b.nm_pessoa_fisica,'ANS'),1,120),
			b.cd_pessoa_fisica,
			a.nr_seq_plano,
			b.dt_nascimento,
			b.ie_sexo,
			decode(b.ie_estado_civil,'1','S','3','D','2','M','6','A','5','W','9','U','4','U'),
			a.dt_inclusao_operadora,
			a.nr_seq_parentesco,
			substr(trim(b.nr_cpf),1,14),
			(ptu_somente_caracter_permitido(b.nr_identidade,'AN')),
			b.sg_emissora_ci,
			a.dt_rescisao,
			a.ie_nascido_plano,
			substr(ptu_somente_caracter_permitido(b.nm_abreviado,'ANS'),1,25),
			nvl(a.nr_seq_titular,a.nr_sequencia), --Quando nao tem titular, traz as informacoes do proprio beneficiario
			a.nr_seq_tabela,
			substr(a.cd_matricula_familia,1,6),
			substr(a.cd_matricula_estipulante,1,20),
			b.nr_cartao_nac_sus,
			b.nr_pis_pasep,
			a.nr_seq_motivo_cancelamento,
			substr(ptu_somente_caracter_permitido(b.nm_social,'ANS'),1,70),
			(	select  max(b.cd_intercambio)
				from  	pessoa_fisica_aux y ,
					genero b
				where   y.nr_seq_genero    	= b.nr_sequencia
				and 	y.cd_pessoa_fisica 	= a.cd_pessoa_fisica) cd_genero,
			(	select 	max(x.nr_seq_genero)				
				from   	pessoa_fisica_aux x 
				where 	x.cd_pessoa_fisica	= a.cd_pessoa_fisica) nr_seq_genero
		into	nm_beneficiario_w,
			cd_pessoa_fisica_w,
			nr_seq_plano_w,
			dt_nascimento_w,
			ie_sexo_w,
			ie_estado_civil_w,
			dt_inclusao_operadora_w,
			nr_seq_parentesco_w,
			nr_cpf_w,
			nr_identidade_w,
			sg_emissora_ci_w,
			dt_rescisao_w,
			ie_nascido_plano_w,
			nm_abreviado_w,
			nr_seq_titular_w,
			nr_seq_tabela_w,
			cd_familia_w,
			nr_matricula_w,
			nr_cartao_nac_sus_w,
			nr_pis_pasep_w,
			nr_seq_motivo_cancelamento_w,
			nm_social_w,
			cd_genero_w,
			nr_seq_genero_w
		from	pessoa_fisica		b,
			pls_segurado		a
		where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
		and	a.nr_sequencia		= nr_seq_segurado_w;
		
		if	(dt_rescisao_w is not null) then --Se a data de rescisao for informada, e obrigado a preencher o campo ie_exclusao_rn412, caso contrario, e obrigado a envio vazio
			if	(nr_seq_motivo_cancelamento_w is not null)then
				select	nvl(max(ie_iniciativa_beneficiario),'N'),
					max(cd_intercambio)
				into	ie_exclusao_rn412_w,
					cd_motivo_exclusao_w
				from	pls_motivo_cancelamento
				where	nr_sequencia = nr_seq_motivo_cancelamento_w;
			else
				ie_exclusao_rn412_w	:= 'N';
				cd_motivo_exclusao_w	:= null;
			end if;
		else
			ie_exclusao_rn412_w	:= null;
			cd_motivo_exclusao_w	:= null;
		end if;
		
		select	max(ie_segmentacao)
		into	ie_segmentacao_w
		from	pls_plano
		where	nr_sequencia		= nr_seq_plano_w;
		
		--Atribui a segmentacao de acordo com a tabela V do manual
		if	(ie_segmentacao_w = '1')	then
			cd_segmentacao_ptu_w := '07';	--Ambulatorial
		elsif	(ie_segmentacao_w = '2')	then
			cd_segmentacao_ptu_w := '10';	--Hospitalar com Obstetricia
		elsif	(ie_segmentacao_w = '3')	then
			cd_segmentacao_ptu_w := '11';	--Hospitalar sem Obstetricia
		elsif	(ie_segmentacao_w = '4')	then
			cd_segmentacao_ptu_w := '12';	--Odontologico
		elsif	(ie_segmentacao_w = '5')	then
			cd_segmentacao_ptu_w := '01';	--Referencia (Ambulatorial + Hospitalar com Obstetricia + Enfermaria)
		elsif	(ie_segmentacao_w = '6')	then
			cd_segmentacao_ptu_w := '02';	--Ambulatorial + Hospitalar com Obstetricia
		elsif	(ie_segmentacao_w = '7')	then
			cd_segmentacao_ptu_w := '03';	-- Ambulatorial + Hospitalar sem Obstetricia
		elsif	(ie_segmentacao_w = '8')	then
			cd_segmentacao_ptu_w := '06';	--Ambulatorial + Odontologico
		elsif	(ie_segmentacao_w = '9')	then
			cd_segmentacao_ptu_w := '08';	--Hospitalar com Obstetetricia + Odontologico
		elsif	(ie_segmentacao_w = '10')	then
			cd_segmentacao_ptu_w := '09';	--Hospitalar sem Obstetetricia + Odontologico
		elsif	(ie_segmentacao_w = '11')	then
			cd_segmentacao_ptu_w := '04';	--Ambulatorial + Hospitalar com Obstetricia + Odontologico
		elsif	(ie_segmentacao_w = '12')	then
			cd_segmentacao_ptu_w := '05';	--Ambulatorial + Hospitalar sem Obstetricia + Odontologico
		end if;
		
		nr_idade_benef_w	:= nvl(obter_idade(dt_nascimento_w, dt_mov_final_w, 'A'),0); -- Buscar a idade do beneficiario no mes do lote
		
		nr_matricula_w	:= elimina_caractere_especial(nr_matricula_w);
		
		if	((ie_gerar_liberacao_a100_w = 'N' and (dt_repasse_w between dt_mov_inicial_w  and dt_mov_final_w)) or
			 (ie_gerar_liberacao_a100_w = 'S' and (dt_liberacao_w between dt_mov_inicial_w  and dt_mov_final_w))) then
			ie_tipo_registro_w := 'I';
		elsif	((ie_gerar_liberacao_a100_w = 'N' and (dt_fim_repasse_w between dt_mov_inicial_w  and dt_mov_final_w)) or
			 (ie_gerar_liberacao_a100_w = 'S' and (dt_fim_real_w between dt_mov_inicial_w  and dt_mov_final_w))) then
			ie_tipo_registro_w := 'E';
		else
			ie_tipo_registro_w := 'A';
		end if;
		
		if	(ie_tipo_registro_w = 'E') then
			select	count(1)
			into	qt_registros_w
			from	pls_segurado_repasse
			where	nr_seq_segurado		= nr_seq_segurado_w
			and	nr_sequencia		<> nr_seq_seg_repasse_w
			and	nr_seq_congenere	= cd_unimed_destino_p
			and	dt_repasse between dt_mov_inicial_w  and dt_mov_final_w
			and	dt_liberacao is not null
			and	rownum	<= 1;
			
			if	(qt_registros_w > 0) then
				goto final;
			end if;
		end if;
		
		if	(nr_seq_plano_w is not null) then
			select	substr(cd_plano_intercambio,1,3)
			into	cd_plano_intercambio_w
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano_w;
		end if;
		
		if	(nr_seq_plano_repasse_w is not null) then
			select	substr(cd_plano_intercambio,1,3)
			into	cd_plano_intercambio_w
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano_repasse_w;
			
			cd_plano_w	:= nr_seq_plano_repasse_w;
		elsif	(cd_plano_intercambio_w is not null) and
			(nr_seq_plano_repasse_w is null) then
			select	max(cd_plano_origem)
			into	cd_plano_w
			from	ptu_intercambio_plano
			where	nr_seq_empresa		= nr_seq_empresa_p
			and	cd_plano_intercambio	= cd_plano_intercambio_w;
		end if;
		
		begin
		select	lpad(nvl(cd_ptu,0),2,0)
		into	cd_grau_parentesco_w
		from	grau_parentesco
		where	nr_sequencia	= nr_seq_parentesco_w;
		exception
		when others then
				cd_grau_parentesco_w := 00;
		end;
		
		begin
		select	substr(cd_usuario_plano,5,13),
			nvl(to_char(dt_validade_carteira,'dd/mm/yyyy'),null)
		into	cd_usuario_plano_w,
			dt_validade_carteira_w
		from	pls_segurado_carteira
		where	nr_seq_segurado	= nr_seq_segurado_w;
		exception
		when others then
			cd_usuario_plano_w	:= '0000000000000';
		end;
		
		begin
		select	a.nr_seq_tabela_origem
		into	nr_seq_tabela_origem_w
		from	pls_segurado		b,
			pls_tabela_preco	a
		where	a.nr_sequencia	= b.nr_seq_tabela
		and	b.nr_sequencia	= nr_seq_segurado_w;
		exception
		when others then
			nr_seq_tabela_origem_w	:= nr_seq_tabela_w;
		end;
		
		begin
		select	to_number(to_char(dt_inicio_vigencia,'yymm'))
		into	nr_vigencia_origem_w
		from	pls_tabela_preco
		where	nr_sequencia	= nr_seq_tabela_origem_w;
		exception
		when others then
			nr_vigencia_origem_w	:= null;
		end;
		
		begin
		select	substr(ptu_somente_caracter_permitido(nm_contato,'ANS'),1,255)
		into	nm_mae_benef_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	ie_tipo_complemento	= 5;
		exception
		when others then
			nm_mae_benef_w	:= '';
		end;
		
		if	(nr_seq_titular_w is not null) then
			begin
			select	nvl(substr(cd_usuario_plano,5,13),0)
			into	cd_titular_plano_w
			from	pls_segurado_carteira
			where	nr_seq_segurado =	nr_seq_titular_w
			and	rownum		= 1;
			exception
			when others then
				cd_titular_plano_w := 0;
			end;
		elsif	(nr_seq_titular_w is null) then
			cd_titular_plano_w	:= 0;
		end if;
		
		nr_seq_segurado_ww :=	nr_seq_segurado_w;
		
		if	(nr_seq_titular_w is not null) then
			nr_seq_segurado_ww	:= nr_seq_titular_w;
		end if;
		
		if	(nm_abreviado_w is null) then
			nm_abreviado_w	:= substr(pls_gerar_nome_abreviado(nm_beneficiario_w),1,255);
		end if;
		
		if	(nm_social_w is not null) then
			nm_social_abreviado_w	:= substr(pls_gerar_nome_abreviado(nm_social_w),1,25);
		else
			nm_social_abreviado_w	:= '';
		end if;
		
		if	(cd_plano_w is null) then
			if	(ie_tipo_contrato_w	= 'P') then
				wheb_mensagem_pck.exibir_mensagem_abort(189987,'NM_BENEFICIARIO='||nm_beneficiario_w||';'||'NR_SEQ_CONTRATO='||nr_seq_contrato_w);
			elsif	(ie_tipo_contrato_w	= 'I') then
				wheb_mensagem_pck.exibir_mensagem_abort(189988,'NM_BENEFICIARIO='||nm_beneficiario_w||';'||'NR_SEQ_INTERCAMBIO='||nr_seq_contrato_w);
			end if;
		end if;	
		
		insert into ptu_intercambio_benef
				(	nr_sequencia, nr_seq_empresa, cd_unimed, cd_usuario_plano,
					cd_familia, nm_benef_abreviado, cd_plano_intercambio, dt_nascimento,
					ie_sexo, ie_estado_civil, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, dt_inclusao, ie_repasse,
					cd_dependencia, cd_local_atendimento, dt_repasse, dt_inclusao_plano_dest,
					cd_cgc_cpf, nr_rg, sg_uf_rg, dt_exclusao, ie_recem_nascido,
					nr_matricula, dt_validade_carteira, cd_lotacao, ds_lotacao,
					dt_fim_repasse, nr_vigencia_origem, cd_plano_origem, cd_titular_plano,
					nm_beneficiario, nr_seq_segurado, ie_status, ie_tipo_registro,
					nr_cartao_nac_sus, nr_pis_pasep, nm_mae_benef, cd_segmentacao_ptu, ie_segmentacao,
					ie_exclusao_rn412, dt_comp_risco, nm_social, nm_social_abreviado,
					cd_motivo_exclusao,ie_tipo_genero_social,nr_seq_genero)
		values( 	ptu_intercambio_benef_seq.nextval, nr_seq_empresa_p, cd_cooperativa_w, nvl(cd_usuario_plano_w,'0000000000000'),
					nvl(cd_familia_w,0), nvl(nm_abreviado_w,' '), cd_plano_intercambio_w, dt_nascimento_w,
					nvl(ie_sexo_w,'M'), nvl(ie_estado_civil_w,'U'), sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, dt_inclusao_operadora_w, nvl(ie_repasse_w,'C'),
					cd_grau_parentesco_w, cd_local_atendimento_w, dt_repasse_w, dt_repasse_w,
					nr_cpf_w, nr_identidade_w, sg_emissora_ci_w, dt_rescisao_w, ie_nascido_plano_w,
					nr_matricula_w, dt_validade_carteira_w, null, null,
					dt_fim_repasse_w, nr_vigencia_origem_w, cd_plano_w, cd_titular_plano_w,
					nm_beneficiario_w, nr_seq_segurado_w, 'A', ie_tipo_registro_w,
					nr_cartao_nac_sus_w, nr_pis_pasep_w, nm_mae_benef_w, cd_segmentacao_ptu_w, ie_segmentacao_w,
					ie_exclusao_rn412_w, dt_repasse_w, nm_social_w, nm_social_abreviado_w,
					cd_motivo_exclusao_w,cd_genero_w,nr_seq_genero_w)
			returning nr_sequencia into nr_seq_beneficiario_w;
			
			if	(nr_versao_transacao_w in (12, 13)) then -- PTU 6.0
				if	(nr_idade_benef_w < 18) then
					if	((trim(nr_pis_pasep_w) is null) and (trim(nm_mae_benef_w) is null) and (nr_cpf_w is null)) then
						ptu_gravar_incon_benef_envio(nr_seq_beneficiario_w,null,wheb_mensagem_pck.get_texto(1113963),
									null, cd_estabelecimento_p, nm_usuario_p);
					end if;
				else
					if	(trim(nr_pis_pasep_w) is null) and
						(trim(nm_mae_benef_w) is null) and
						(dt_rescisao_w is null) and
						(dt_fim_repasse_w is null) then
						ptu_gravar_incon_benef_envio(nr_seq_beneficiario_w,null,wheb_mensagem_pck.get_texto(1113979),
									null, cd_estabelecimento_p, nm_usuario_p);
					end if;
				end if;
			elsif	(nr_versao_transacao_w = 11) then -- PTU 5.0
				if	(nr_idade_benef_w < 18) and
					(trim(nr_pis_pasep_w) is null) and
					(trim(nm_mae_benef_w) is null) then
					ptu_gravar_incon_benef_envio(nr_seq_beneficiario_w,null,wheb_mensagem_pck.get_texto(1113980),
									null, cd_estabelecimento_p, nm_usuario_p);
				end if;
			end if;
			ptu_gerar_benef_carencia(nr_seq_segurado_w, nr_seq_beneficiario_w, dt_mov_final_w, nm_usuario_p);
			--Gerar Produtos agregados R106
			if 	(ie_gerar_prod_agregado_w is null or ie_gerar_prod_agregado_w = 'S') then
				ptu_gerar_benef_plano_agregado(nr_seq_segurado_w, nr_seq_beneficiario_w, dt_mov_final_w, nm_usuario_p);
			end if;
			ptu_gerar_benef_preexistencia(nr_seq_segurado_w, nr_seq_beneficiario_w, nm_usuario_p);
			ptu_gerar_benef_compl(nr_seq_segurado_w, nr_seq_beneficiario_w, cd_estabelecimento_p, nm_usuario_p);
			
			select	count(1)
			into	qt_registros_w
			from	ptu_intercambio_consist
			where	nr_seq_inter_benef	= nr_seq_beneficiario_w;
			
			if	(qt_registros_w > 0) then
				update	ptu_intercambio_benef
				set	ie_status	= 'S'
				where	nr_sequencia	= nr_seq_beneficiario_w;
			end if;
		
		<<final>>
		nr_seq_segurado_w	:= nr_seq_segurado_w;
	end if;
	end;
end loop;
close C01;

--Gerar os beneficiarios de alteracao caso o lote seja de movimentacao periodica
if	(ie_tipo_movimentacao_w = 'P') then
	ptu_gerar_alteracao_repasse(cd_unimed_destino_p,nr_seq_empresa_p,cd_estabelecimento_p,nm_usuario_p);
end if;

end ptu_gerar_intercambio_benef;
/
