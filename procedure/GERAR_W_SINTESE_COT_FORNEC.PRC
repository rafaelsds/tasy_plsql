create or replace
procedure gerar_w_sintese_cot_fornec(
			nr_seq_cotacao_p		number,
			cd_fornecedor_p		varchar2,
			cd_condicao_pagamento_p	varchar2,
			qt_prz_minimo_entrega_p	number,
			vl_minimo_pedido_p	varchar2,
			ds_observacao_p		varchar2,
			nm_usuario_p		varchar2) is 

nr_seq_cot_fornec_w	number(10);
			
begin

select	w_sintese_cot_fornec_seq.nextval
into	nr_seq_cot_fornec_w
from	dual;

insert into w_sintese_cot_fornec(	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_cotacao,
	cd_fornecedor,
	cd_condicao_pagamento,
	qt_prz_minimo_entrega,
	vl_minimo_pedido,
	ds_observacao)
values(	nr_seq_cot_fornec_w,           
	sysdate,      
	nm_usuario_p,             
	sysdate,    
	nm_usuario_p,        
	nr_seq_cotacao_p,
	cd_fornecedor_p,
	cd_condicao_pagamento_p,
	qt_prz_minimo_entrega_p,
	vl_minimo_pedido_p,
	ds_observacao_p);

commit;

end gerar_w_sintese_cot_fornec;
/