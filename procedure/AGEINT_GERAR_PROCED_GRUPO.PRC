create or replace
procedure Ageint_Gerar_Proced_Grupo(
			nr_seq_grupo_p		number,
			cd_agenda_p		number,
			nm_usuario_p		Varchar2,
			cd_Estabelecimento_p	number) is 

nr_seq_proc_regra_w	number(10);
nr_seq_proc_interno_w	number(10);
qt_proced_w		number(10);
			
Cursor C01 is
	select	nr_sequencia,
		nr_seq_proc_interno
	from	ageint_proced_regra
	where	nr_seq_grupo	= nr_seq_grupo_p
	and	ie_situacao	= 'A'
	order by 1;
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_proc_regra_w,
	nr_seq_proc_interno_w;
exit when C01%notfound;
	begin
	select	count(*)
	into	qt_proced_w
	from	agenda_regra
	where	nr_seq_proc_interno	= nr_seq_proc_interno_w
	and	cd_agenda		= cd_agenda_p;
	
	if	(qt_proced_w	= 0) then
		insert into agenda_regra (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_proc_interno,
						ie_permite,
						ie_medico,
						ie_forma_consistencia,
						cd_agenda,
						nr_seq_proced_regra,
						cd_estabelecimento,
						IE_AGENDA,
            IE_SITUACAO)
		values (agenda_regra_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_proc_interno_w,
			'S',
			'E',
      'M',
      cd_agenda_p,
      nr_seq_proc_regra_w,
      cd_estabelecimento_p,
      DECODE(OBTER_TIPO_AGENDA(CD_AGENDA_P), 1, 'CI', 2, 'E', 'T'),
      'A'); 
  end if;
  end;
end loop;
close C01;

commit;

end Ageint_Gerar_Proced_Grupo;
/