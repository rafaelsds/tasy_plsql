create or replace
procedure gravar_imagens_pepo(	ds_imagem_p		varchar2,
				nr_cirurgia_p		number,
				nr_seq_pepo_p		number,
				nm_usuario_p		varchar2,
				nr_seq_grafico_p	number)
				is


begin

insert into w_grafico_pepo(
		ds_imagem,
		nr_cirurgia,
		nr_seq_pepo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_grafico)
	values(	ds_imagem_p,
		nr_cirurgia_p,
		nr_seq_pepo_p,
		sysdate,
		nm_usuario_p,
		nr_seq_grafico_p);
commit;
	
end gravar_imagens_pepo;
/