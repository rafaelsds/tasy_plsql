create or replace
procedure med_resultado_exame(	ie_tipo_resultado_p		varchar2,
				nr_seq_exame_p		number,
				nr_seq_cliente_p		number,
				dt_exame_p		date,
				nm_usuario_p		varchar2)is
				
begin

if	(ie_tipo_resultado_p is not null) and
	(nr_seq_exame_p is not null) and
	(nr_seq_cliente_p is not null) and
	(dt_exame_p is not null) then
	begin	
	med_alterar_resultado_exame(ie_tipo_resultado_p, nr_seq_exame_p, nr_seq_cliente_p, dt_exame_p, nm_usuario_p);	
	--med_gerar_resultados_ant(nr_seq_cliente_p, nm_usuario_p);	
	med_excluir_result_exame(nr_seq_cliente_p);
	gerar_med_result_exame(nm_usuario_p, nr_seq_cliente_p);
	end;	
end if;

end med_resultado_exame;
/