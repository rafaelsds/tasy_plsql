create or replace
Procedure Gerar_Item_requisicao_Material(nr_requisicao_p		number,
				cd_material_p		number,
				qt_material_p		number,
				nr_seq_lote_fornec_p	number,
				nm_usuario_p		varchar2,
				ds_erro_p		out	varchar2) is

cd_estab_w			number(5);
cd_oper_estoque_w		number(3);
cd_setor_atend_w			number(5);
cd_local_estoque_w		number(4);
cd_centro_custo_w			number(8);
ie_tipo_conta_w			number(1);
cd_conta_contabil_w		varchar2(20) := '';
nr_sequencia_w			number(10);
cd_cgc_fornec_w			varchar2(14);
cd_unid_med_consumo_w		varchar2(30);
cd_unid_med_estoque_w		varchar2(30);
qt_conv_estoque_consumo_w 	number(13,4);
ie_baixa_estoq_pac_w		varchar2(1);
dt_solicitacao_requisicao_w		date;
ds_consistencia_w			varchar2(255);
ds_erro_w			varchar2(255) := '';
VarConsisteMaterialCCusto_w		varchar2(01);
VarConsisteMaterialLocal_w		varchar2(01);
VarPermiteQtMaiorPadrao_w		varchar2(01);

cursor c01 is
select	ds_consistencia
from	requisicao_mat_consist
where	nr_requisicao = nr_requisicao_p;

begin


select	cd_estabelecimento,
	cd_operacao_estoque,
	cd_setor_atendimento,
	cd_local_estoque,
	cd_centro_custo,
	dt_solicitacao_requisicao
into	cd_estab_w,
	cd_oper_estoque_w,
	cd_setor_atend_w,
	cd_local_estoque_w,
	cd_centro_custo_w,
	dt_solicitacao_requisicao_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

VarConsisteMaterialCCusto_w	:= substr(nvl(obter_valor_param_usuario(919, 4, Obter_perfil_ativo, nm_usuario_p, cd_estab_w),'N'),1,1);
VarConsisteMaterialLocal_w	:= substr(nvl(obter_valor_param_usuario(919, 5, Obter_perfil_ativo, nm_usuario_p, cd_estab_w),'N'),1,1);
VarPermiteQtMaiorPadrao_w	:= substr(nvl(obter_valor_param_usuario(919, 69, Obter_perfil_ativo, nm_usuario_p, cd_estab_w),'N'),1,1);


Consistir_Item_Requisicao(cd_material_p, nr_requisicao_p, VarConsisteMaterialCCusto_w, 'N', VarConsisteMaterialLocal_w, 'N', VarPermiteQtMaiorPadrao_w,'S', nm_usuario_p, 'N', cd_cgc_fornec_w, ds_erro_p);

open C01;
loop
fetch C01 into	
	ds_consistencia_w;
exit when C01%notfound;
	begin
	ds_erro_w := substr(ds_erro_w || ds_consistencia_w || chr(13) || chr(10),1,255);
	end;
end loop;
close C01;

if	(ds_erro_w is null) then

	select	decode(ie_tipo_requisicao,2,2,21,2,3)
	into	ie_tipo_conta_w
	from	operacao_estoque
	where	cd_operacao_estoque = cd_oper_estoque_w;

	define_conta_material(
			cd_estab_w,
			cd_material_p,
			ie_tipo_conta_w, 0,
			cd_setor_atend_w, '0', 0, 0, 0, 0,
			cd_local_estoque_w,
			Null,
			dt_solicitacao_requisicao_w,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null);

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p;

	select	substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo,
		substr(obter_dados_material_estab(cd_material,cd_estab_w,'UME'),1,30) cd_unidade_medida_estoque,
		qt_conv_estoque_consumo,
		/*ie_baixa_estoq_pac Fabio - 23/06/2004 Alterei para buscar da function*/
		substr(obter_material_baixa_estoq_pac(cd_estab_w, 0, cd_material),1,1)
	into	cd_unid_med_consumo_w,
		cd_unid_med_estoque_w,
		qt_conv_estoque_consumo_w,
		ie_baixa_estoq_pac_w
	from	material
	where	cd_material = cd_material_p;
end if;
	
consiste_minimo_multiplo_req(cd_material_p, qt_material_p, ds_erro_p);
	
if	(ds_erro_w is null) then
	

	insert into item_requisicao_material(
		NR_REQUISICAO,
		NR_SEQUENCIA,
		CD_ESTABELECIMENTO,
		CD_MATERIAL,
		QT_MATERIAL_REQUISITADA,
		QT_MATERIAL_ATENDIDA,
		VL_MATERIAL,
		DT_ATUALIZACAO,
		NM_USUARIO,
		CD_UNIDADE_MEDIDA,
		DT_ATENDIMENTO,
		CD_PESSOA_RECEBE,
		CD_PESSOA_ATENDE,
		IE_ACAO,
		CD_MOTIVO_BAIXA,
		QT_ESTOQUE,
		CD_UNIDADE_MEDIDA_ESTOQUE,
		CD_CONTA_CONTABIL,
		CD_MATERIAL_REQ,
		NR_SEQ_LOTE_FORNEC,
		CD_CGC_FORNECEDOR,
		DS_OBSERVACAO,
		IE_GERACAO)
	values (
		nr_requisicao_p,
		nr_sequencia_w,
		cd_estab_w,
		cd_material_p,
		qt_material_p,
		0,
		0,
		sysdate,
		nm_usuario_p,
		cd_unid_med_consumo_w,
		null,
		null,
		null,
		'1',
		0,
		(qt_material_p / qt_conv_estoque_consumo_w),
		cd_unid_med_estoque_w,
		cd_conta_contabil_w,
		cd_material_p,
		decode(nr_seq_lote_fornec_p,0,null),
		cd_cgc_fornec_w,
		null,
		null);
		
	-- WHEB_MENSAGEM_PCK.get_texto(355122) - Inclus�o de item
	-- WHEB_MENSAGEM_PCK.get_texto(355133) - Item [#@CD_MATERIAL#@] - #@DS_MATERIAL#@ incluido atrav�s do Atendimento da Requisi��o pelo usu�rio #@NM_USUARIO#@.
	-- AR - Inclus�o de item via atendimento requisi��o
	gerar_historico_requisicao(
			nr_requisicao_p,
			WHEB_MENSAGEM_PCK.get_texto(355122),
			WHEB_MENSAGEM_PCK.get_texto(355133, 'CD_MATERIAL=' || cd_material_p || ';DS_MATERIAL=' || obter_desc_material(cd_material_p) || ';NM_USUARIO=' || Obter_Desc_Usuario(nm_usuario_p)),
			'AR',
			nm_usuario_p);
	
end if;

ds_erro_p := ds_erro_w;

commit;

end;
/
