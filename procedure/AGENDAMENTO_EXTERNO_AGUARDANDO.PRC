create or replace procedure agendamento_externo_aguardando(nr_seq_lista_espera_p 	number,
														 nm_usuario_p			varchar2) is

nr_seq_regulacao_atend_w		regulacao_atend.nr_sequencia%type;
nr_seq_situacao_atual_w			lista_espera_situacao_cont.nr_sequencia%type;

begin

if (nr_seq_lista_espera_p is not null) then

	update agenda_lista_espera
	set ie_status_espera			= 'A'
	where nr_sequencia				= nr_seq_lista_espera_p;

	select max(nr_seq_regulacao)
	into nr_seq_regulacao_atend_w
	from agenda_lista_espera a
	where nr_sequencia = nr_seq_lista_espera_p;

	if (nr_seq_regulacao_atend_w is not null) then
		alterar_status_regulacao(nr_seq_regulacao_atend_w,'FA', obter_expressao_dic_objeto(1099863),obter_pessoa_fisica_usuario(nm_usuario_p, null));
	end if;

	select	min(nr_sequencia)
	into	nr_seq_situacao_atual_w
	from	lista_espera_situacao_cont
	where	ie_situacao = 'A'
	and		ie_tipo_contato = 'PF';

	if (nr_seq_situacao_atual_w is not null) then
		begin

			insert into ag_lista_espera_contato(
				nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_lista_espera,
				dt_contato,
				ie_origem,
				nm_pessoa_contato,
				nr_seq_situacao_atual,
				ds_observacao,
				dt_liberacao,
				nm_usuario_liberacao,
				ie_contato_realizado
			) values(
				ag_lista_espera_contato_seq.nextval,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_lista_espera_p,
				sysdate,
				'A',
				obter_pessoa_fisica_usuario(nm_usuario_p, null),
				nr_seq_situacao_atual_w,
				obter_expressao_dic_objeto(1099862),
				sysdate,
				nm_usuario_p,
				'N'
			);

		end;
	end if;

end if;
end agendamento_externo_aguardando;
/