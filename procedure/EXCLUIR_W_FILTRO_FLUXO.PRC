create or replace
procedure excluir_w_filtro_fluxo(cd_estabelecimento_p number,
				nm_usuario_p varchar2) is 

begin

delete  from w_filtro_fluxo
where   cd_estabelecimento 	= cd_estabelecimento_p
and	nm_usuario		= nm_usuario_p;

commit;

end excluir_w_filtro_fluxo;
/