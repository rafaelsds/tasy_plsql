create or replace
procedure gerar_protocolo_individual(	nr_interno_conta_p 	conta_paciente.nr_interno_conta%type,
					ie_tipo_protocolo_p	number,
					dt_entrega_convenio_p	date,
					nm_usuario_p  		Varchar2) is 

nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
dt_mesano_referencia_w		conta_paciente.dt_mesano_referencia%type;
cd_medico_resp_w		atendimento_paciente.cd_medico_resp%type;
ie_cobra_pf_pj_w		medico.ie_cobra_pf_pj%type;
nr_seq_protocolo_w		protocolo_convenio.nr_seq_protocolo%type;
nr_protocolo_w			protocolo_convenio.nr_protocolo%type;
cd_convenio_parametro_w		conta_paciente.cd_convenio_parametro%type;
cd_estabelecimento_w		conta_paciente.cd_estabelecimento%type;
cd_procedencia_w		atendimento_paciente.cd_procedencia%type;
cd_cgc_procedencia_w		procedencia.cd_cgc_procedencia%type;

begin

begin
	select	nr_atendimento, 
		dt_mesano_referencia,
		cd_convenio_parametro,
		cd_estabelecimento
	into	nr_atendimento_w,
		dt_mesano_referencia_W,
		cd_convenio_parametro_w,
		cd_estabelecimento_w
	from 	conta_paciente
	where 	nr_interno_conta = nr_interno_conta_p;
exception
	when others then
	nr_atendimento_w:=0;
end;

if	(nr_atendimento_w > 0) then 

	select	nvl(max(cd_medico_resp),'0')
	into	cd_medico_resp_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;

	select	nvl(ie_cobra_pf_pj,'0')
	into	ie_cobra_pf_pj_w
	from	medico
	where	cd_pessoa_fisica = cd_medico_resp_w;

	if (ie_cobra_pf_pj_w = 'F') then

		select	nvl(max(nr_seq_protocolo),0),
			nvl(max(nr_protocolo),'0')
		into 	nr_seq_protocolo_w,
			nr_protocolo_w
		from	protocolo_convenio
		where	cd_medico_protocolo = cd_medico_resp_w
		and	ie_status_protocolo = 1
		and	dt_mesano_referencia = dt_mesano_referencia_w;

	elsif (ie_cobra_pf_pj_w = 'J') then

		select	cd_procedencia
		into	cd_procedencia_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_w;


		select	nvl(max(cd_cgc_procedencia),'0')
		into	cd_cgc_procedencia_w
		from	procedencia
		where	cd_procedencia = cd_procedencia_w;

		select	nvl(max(nr_seq_protocolo),0),
			nvl(max(nr_protocolo),'0')
		into 	nr_seq_protocolo_w,
			nr_protocolo_w
		from	protocolo_convenio
		where	cd_cgc_procedencia = cd_cgc_procedencia_w
		and	ie_status_protocolo = 1
		and	dt_mesano_referencia = dt_mesano_referencia_w;

	end if;

	if 	(nr_seq_protocolo_w > 0 ) then

		update	conta_paciente
		set	dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p, 
			nr_protocolo = nr_protocolo_w, 
			nr_seq_protocolo = nr_seq_protocolo_w
		where	nr_interno_conta = nr_interno_conta_p;

	else	

		select	protocolo_convenio_seq.nextval
		into 	nr_seq_protocolo_w
		from 	dual;
	
		if (ie_cobra_pf_pj_w = 'F') then
			
			nr_protocolo_w := substr(obter_nome_pf(cd_medico_resp_w)||' - '||nr_seq_protocolo_w,1,40);
			
		elsif (ie_cobra_pf_pj_w = 'J') then
		
			select	substr(max(ds_procedencia)||' - '||nr_seq_protocolo_w,1,40)
			into	nr_protocolo_w
			from	procedencia
			where	cd_procedencia = cd_procedencia_w;
			
		end if;
				

		insert into protocolo_convenio(	cd_convenio,
						nr_protocolo,
						ie_status_protocolo,
						dt_periodo_inicial,
						dt_periodo_final,
						dt_atualizacao,
						nm_usuario,
						ie_tipo_protocolo,
						nr_seq_protocolo,
						dt_mesano_referencia,
						cd_estabelecimento,
						nr_seq_lote_receita,
						nr_seq_lote_repasse,
						nr_seq_lote_grat,
						cd_medico_protocolo,
						cd_cgc_procedencia,
						dt_entrega_convenio)
					values( cd_convenio_parametro_w,
						nvl(nr_protocolo_w,nr_seq_protocolo_w),
						1,
						pkg_date_utils.start_of(dt_mesano_referencia_w, 'MONTH', 0),    
						pkg_date_utils.end_of(dt_mesano_referencia_w, 'MONTH', 0),
						sysdate,
						nm_usuario_p,
						ie_tipo_protocolo_p, 
						nr_seq_protocolo_w,
						dt_mesano_referencia_w,
						cd_estabelecimento_w, 
						0,
						0,
						0,
						decode(ie_cobra_pf_pj_w,'F',cd_medico_resp_w,null),
						decode(ie_cobra_pf_pj_w,'J',cd_cgc_procedencia_w,null),
						dt_entrega_convenio_p);

			update	conta_paciente
			set	dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p, 
				nr_protocolo = nr_protocolo_w, 
				nr_seq_protocolo = nr_seq_protocolo_w
			where  	nr_interno_conta = nr_interno_conta_p;

	end if;

	commit;

end if;

end 	gerar_protocolo_individual;
/