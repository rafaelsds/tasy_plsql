create or replace
procedure atualizar_repasse_cheque_comp	(	nr_titulo_p		number,
						dt_compasacao_p		date,
						vl_cheque_p		number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number,
						nr_Seq_caixa_receb_p	number,
						nr_seq_cheque_p		number) is



pr_repasse_w			number(7,6);
vl_total_conta_w		number(15,2);
vl_total_conta_rec_w		number(15,2);
vl_total_repasse_w		number(15,2);
vl_repasse_w			number(15,2);
vl_novo_repasse_w		number(15,2);
nr_seq_repasse_w		number(10);
nr_seq_proc_rep_novo_w		number(10);
nr_seq_mat_rep_novo_w		number(10);
nr_titulo_receb_w		number(10);
ie_lib_adiantamento_w		parametro_repasse.ie_lib_adiantamento%type;

cursor c01 is
select	obter_valor_repasse_item(a.nr_seq_procedimento,'P'),
	a.vl_repasse,
	a.nr_sequencia
from	convenio d,
	procedimento_paciente b,   	    
	procedimento_repasse a,
	titulo_receber c
where	a.nr_repasse_terceiro	is null  
and	a.nr_seq_procedimento	= b.nr_sequencia
and	b.cd_motivo_exc_conta	is null  
and	b.nr_interno_conta	= c.nr_interno_conta  
and	b.cd_convenio		= d.cd_convenio  
and	d.ie_tipo_convenio	= 1
and	c.cd_estabelecimento	= cd_estabelecimento_p
and	c.nr_titulo		= nr_titulo_receb_w
and	a.ie_status		in ('A','U','D');

cursor c02 is
select	obter_valor_repasse_item(a.nr_seq_material,'M'),
	a.vl_repasse,
	a.nr_sequencia
from    convenio d,          
	material_atend_paciente b,
	material_repasse a,
	titulo_receber c
where	a.nr_repasse_terceiro	is null  
and	a.nr_seq_material	= b.nr_sequencia  
and	b.cd_motivo_exc_conta	is null  
and	b.nr_interno_conta	= c.nr_interno_conta  
and	b.cd_convenio		= d.cd_convenio  
and	d.ie_tipo_convenio	= 1
and	c.cd_estabelecimento	= cd_estabelecimento_p
and	c.nr_titulo		= nr_titulo_receb_w
and	a.ie_status		in ('A','U','D');

cursor c03 is
select 	distinct 
	nr_titulo
from 	titulo_receber_liq
where	NR_SEQ_CAIXA_REC = nr_Seq_caixa_receb_p
union
select 	nr_titulo_p
from	dual
where	nr_Seq_caixa_receb_p is null
union
select	distinct
	a.nr_seq_titulo_receber
from	movto_trans_financ a
where	a.nr_seq_caixa_rec is null
and	a.nr_seq_cheque = nr_seq_cheque_p
and	nr_seq_caixa_receb_p is null
and	a.nr_seq_titulo_receber is not null
union
select	distinct
	nr_titulo
from	titulo_receber_liq a,
	adiantamento_cheque_cr b
where	b.nr_adiantamento = a.nr_adiantamento
and	b.nr_seq_cheque	= nr_seq_cheque_p
and	ie_lib_adiantamento_w = 'S';

begin
vl_total_conta_rec_w	:= 0;

begin
select	nvl(ie_lib_adiantamento,'N')
into	ie_lib_adiantamento_w
from	parametro_repasse
where	cd_estabelecimento = cd_estabelecimento_p;
exception
when others then
	ie_lib_adiantamento_w	:= 'N';
end;

open c03;
loop
fetch c03 into
	nr_titulo_receb_w;
exit when c03%notfound;
	
	select	nvl(sum(vl_conta),0)
	into	vl_total_conta_w
	from	titulo_receber c,
		conta_paciente b
	where	b.nr_interno_conta	= c.nr_interno_conta  
	and	c.cd_estabelecimento	= cd_estabelecimento_p
	and	c.nr_titulo		= nr_titulo_receb_w;
	
	vl_total_conta_rec_w := vl_total_conta_rec_w + vl_total_conta_w;

end loop;
close c03;

if 	(vl_cheque_p >= vl_total_conta_rec_w) then
	pr_repasse_w 		:= 1;
else	
	pr_repasse_w 		:= dividir_sem_round(vl_cheque_p,vl_total_conta_rec_w);
end if;

open c03;
loop
fetch c03 into
	nr_titulo_receb_w;
exit when c03%notfound;
	
	open c01;
	loop
	fetch c01 into
		vl_total_repasse_w,
		vl_repasse_w,
		nr_seq_repasse_w;
	exit when c01%notfound;

		vl_novo_repasse_w := vl_total_repasse_w * pr_repasse_w;

		if	(vl_repasse_w > vl_novo_repasse_w) then

			update 	procedimento_repasse
			set	vl_repasse = vl_novo_repasse_w,
				vl_liberado = vl_novo_repasse_w,
				ie_status = 'R',
				dt_liberacao = sysdate,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where nr_sequencia = nr_seq_repasse_w;

			desdobrar_procmat_repasse(nr_seq_repasse_w, null, 'A', (vl_repasse_w - (vl_novo_repasse_w)), nm_usuario_p, nr_seq_proc_rep_novo_w, nr_seq_mat_rep_novo_w);

		else
			update 	procedimento_repasse
			set	vl_liberado = vl_repasse,
				ie_status = 'R',
				dt_liberacao = sysdate,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where nr_sequencia = nr_seq_repasse_w;
			
		end if;
	end loop;
	close c01;


	open c02;
	loop
	fetch c02 into
		vl_total_repasse_w,
		vl_repasse_w,
		nr_seq_repasse_w;
	exit when c02%notfound;

		vl_novo_repasse_w := vl_total_repasse_w * pr_repasse_w;

		if	(vl_repasse_w > vl_novo_repasse_w) then

			update 	material_repasse
			set	vl_repasse = vl_novo_repasse_w,
				vl_liberado = vl_novo_repasse_w,
				ie_status = 'R',
				dt_liberacao = sysdate,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where nr_sequencia = nr_seq_repasse_w;

			desdobrar_procmat_repasse(null, nr_seq_repasse_w, 'A', (vl_repasse_w - (vl_novo_repasse_w)), nm_usuario_p, nr_seq_proc_rep_novo_w, nr_seq_mat_rep_novo_w);

		else
			update 	material_repasse
			set	vl_liberado = vl_repasse,
				ie_status = 'R',
				dt_liberacao = sysdate,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where nr_sequencia = nr_seq_repasse_w;
			
		end if;
	end loop;
	close c02;
end loop;
close c03;
end;
/