create or replace
procedure fin_estornar_receb_lote_tes(	nr_seq_caixa_p	number,
				nr_seq_lote_p	number,
				nm_usuario_p	varchar2) is
				
				
nr_Seq_cheque_w		number(10);
nr_seq_movto_cartao_w	number(10);

Cursor C01 is
select	nr_seq_cheque,
	nr_seq_movto_cartao
from	movto_trans_financ
where	nr_seq_lote_origem	= nr_seq_lote_p
and	nr_seq_caixa	= nr_seq_caixa_p;				
				
begin	
open C01;
loop
fetch C01 into	
	nr_seq_cheque_w,
	nr_seq_movto_cartao_w;
exit when C01%notfound;
	begin
	if	(nr_seq_cheque_w is not null) then
		/* Devolver cheques vinculados */ 
		update	cheque_cr 
		set	dt_devolucao	= sysdate, 
			nm_usuario	= nm_usuario_p, 
			dt_atualizacao	= sysdate 
		where	nr_seq_cheque = nr_seq_cheque_w;
	end if;
	
	if	(nr_seq_movto_cartao_w is not null) then	 
		/* Cancelar cart�es vinculados */ 
		update	movto_cartao_cr 
		set	dt_cancelamento	= sysdate, 
			nm_usuario	= nm_usuario_p, 
			dt_atualizacao	= sysdate 
		where	nr_sequencia = nr_seq_movto_cartao_w
		and	dt_integracao_tef is null;
		end if;
	end;
end loop;
close C01;

commit;
end fin_estornar_receb_lote_tes;
/