create or replace
procedure altera_codigo_retencao(	ie_opcao_p	varchar2,
					cd_retencao_p	varchar2,
					cd_tributo_p	number,
					nr_sequencia_p	number,
					dt_vencimento_p	date) is

begin

if	(ie_opcao_p = 'T') then
	begin
	if	(cd_retencao_p is not null) and
		(cd_tributo_p is not null) and
		(nr_sequencia_p is not null) then
		begin
		
		update	nota_fiscal_trib
		set	cd_darf		= cd_retencao_p
		where	cd_tributo	= cd_tributo_p
		and	nr_sequencia	= nr_sequencia_p;
		
		end;
	end if;	
	end;
elsif	(ie_opcao_p = 'V') then
	begin
	if	(cd_retencao_p is not null) and
		(cd_tributo_p is not null) and
		(nr_sequencia_p is not null) and
		(dt_vencimento_p is not null) then
		begin
		
		update	nota_fiscal_venc_trib
		set	cd_darf		= cd_retencao_p
		where	cd_tributo	= cd_tributo_p
		and	nr_sequencia	= nr_sequencia_p
		and	dt_vencimento	= dt_vencimento_p;
		
		end;
	end if;
	end;
end if;

commit;

end altera_codigo_retencao;
/