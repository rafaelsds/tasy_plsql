create or replace
procedure eup_atualiza_age_cons_js(	
					ds_lista_agenda_cons_p		varchar2,
					ds_lista_agenda_serv_p		varchar2,
					cd_setor_usuario_p		number,
					ie_setor_prescricao_p		varchar2,
					dt_alta_p			date,
					dt_entrada_p			date,
					ie_data_prescricao_p		varchar2,
					ie_gerar_prescr_ag_lista_p	varchar2,
					ie_novo_atend_lista_ag_p	varchar2,
					cd_estabelecimento_p		number,
					cd_tipo_agenda_p		number,
					ie_novo_registro_p		varchar2,
					ds_lista_agenda_exame_p		varchar2,
					nr_atendimento_p		number,
					nr_seq_agenda_p			number,
					nm_usuario_p			Varchar2,
					ie_limpar_variaveis_p	out	varchar2,
					nr_prescricao_p		out	number) is 

ie_evento_w		varchar2(1);	
dt_prescricao_w		date;
nr_prescricao_w		number(14);				
					
begin
if	(nvl(nr_atendimento_p,0) > 0) then
	
	if	(nvl(nr_seq_agenda_p,0) > 0) then

		gerar_classif_tiss_agenda(nr_atendimento_p, nr_seq_agenda_p, 'C'); 
	end if;
	if	(ds_lista_agenda_exame_p is not null)	then
		Atualizar_Lista_Ag_Cons_EUP(nr_atendimento_p, ds_lista_agenda_exame_p, nm_usuario_p); 
	else
		if (ds_lista_agenda_cons_p is not null) then
			Atualizar_Lista_Ag_Cons_EUP(nr_atendimento_p, ds_lista_agenda_cons_p, nm_usuario_p);
		elsif	(nvl(nr_seq_agenda_p,0) > 0) then
			Atualizar_Atend_Ag_Cons_EUP(nr_atendimento_p, nr_seq_agenda_p, nm_usuario_p);
		end if;
	end if;
	
	if	(ie_novo_registro_p = 'S') then
		atualizar_espec_med_eup_agenda(nr_atendimento_p, nr_seq_agenda_p, nm_usuario_p);
	end if;
	
	if	(nvl(cd_tipo_agenda_p,0) in (3,4)) then -- NESTA VEIRIFICAÇÃO RETIRAMOS OS PARÂMETROS DA AGENDA ANTIGA
		ie_evento_w := obter_se_existe_evento_agenda(cd_estabelecimento_p,'GA','C');
		if	(ie_evento_w = 'N') then
			Atualizar_Status_Ag_Cons_EUP(nr_seq_agenda_p, 'O', nm_usuario_p); 
		end if;
	end if;
	
	if	(ie_novo_atend_lista_ag_p = 'N') then
		 gerar_prescr_agenda_consulta(nr_seq_agenda_p, nm_usuario_p);
	elsif	(ie_novo_atend_lista_ag_p = 'S') and
		(ie_gerar_prescr_ag_lista_p = 'S') and
		(nvl(cd_tipo_agenda_p,0) > 0) and
		(nvl(cd_tipo_agenda_p,0) <> 5) then
		
		if (nvl(cd_tipo_agenda_p,0) in (3,4)) then
			gerar_prescr_agenda_consulta(nr_seq_agenda_p, nm_usuario_p);
		end if;
		
		if (nvl(cd_tipo_agenda_p,0) in (1,2)) then
			gerar_prescricao_atend_exame (nr_atendimento_p, dt_prescricao_w, nr_seq_agenda_p, nm_usuario_p, cd_setor_usuario_p, ie_setor_prescricao_p);
		end if;
		if	(ie_data_prescricao_p = 'S') then
			dt_prescricao_w := dt_entrada_p;
		elsif	(ie_data_prescricao_p = 'AE') and
			(dt_alta_p is not null) then
			dt_prescricao_w := dt_alta_p;
		else
			dt_prescricao_w := sysdate;
		end if;
		eup_gerar_prescr_ag_listas_js(ds_lista_agenda_cons_p, ds_lista_agenda_serv_p, cd_setor_usuario_p, ie_setor_prescricao_p, nr_seq_agenda_p, dt_prescricao_w, nr_atendimento_p, ds_lista_agenda_exame_p, nm_usuario_p);
		ie_limpar_variaveis_p := 'S';
	end if;
	select	nvl(max(nr_prescricao),0)  
	into	nr_prescricao_w
	from  	prescr_medica  
	where	nr_seq_agecons = nr_seq_agenda_p;
	
	if	(nvl(nr_prescricao_w,0) > 0) then
		nr_prescricao_p := nr_prescricao_w;
	end if;
end if;

commit;

end eup_atualiza_age_cons_js;
/
