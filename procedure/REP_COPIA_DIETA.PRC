create or replace
procedure REP_copia_dieta(	nr_prescricao_orig_p	number,
							nr_prescricao_nova_p	number,
							dt_prescricao_p			date,
							nr_seq_regra_p			number,
							nm_usuario_p			varchar2) is 

nr_seq_dieta_w				number(10,0);
nr_atendimento_w			number(15,0);
ie_copia_valid_igual_w		varchar2(1);
ie_prim_horario_setor_w		varchar2(10);
hr_setor_w					varchar2(20);
ie_loop_w					varchar2(2);
cd_setor_atendimento_w		number(10);
dt_validade_nova_w			date;
dt_validade_origem_w		date;
dt_inicio_prescr_w			date;
dt_prescricao_www			date;
dt_primeiro_horario_w		date;
cd_estabelecimento_w		number(4,0);
qt_itens_w					number(10,0);
/* Guilherme Pereira 25/10/2012 OS:510787  */ 
cd_dieta_w					number(10);
qt_parametro_w				number(15,4);
ds_horarios_w				varchar2(2000);
ds_horarios2_w				varchar2(2000);
ds_observacao_w				varchar2(4000);
ie_destino_dieta_w			varchar2(2);
ie_refeicao_w				varchar2(3);
ie_via_aplicacao_w			varchar2(50);
cd_intervalo_w				varchar2(50);
nr_seq_anterior_w			number(6);
dt_inicio_item_w			date;
dt_inicio_w					date;
dt_final_w					date;
qt_operacao_w				number(15,4);
ie_operacao_w				varchar2(15);
ie_regra_geral_w			varchar2(15);
ie_copiar_w					varchar2(1);
nr_horas_validade_w			number(15);
nr_seq_regra_copia_w		number(15);
nr_ocorrencia_w				number(15);
ie_manter_intervalo_w		varchar2(1);
hr_prim_horario_w			varchar2(5);
dt_ultimo_horario_w			date;
dt_prim_horario_ant_w		date;

cursor c01 is
select	a.cd_dieta,
		a.qt_parametro,
		a.ds_horarios,
		a.ds_observacao,
		a.ie_destino_dieta,
		a.ie_refeicao,
		a.nr_sequencia,
		a.ie_via_aplicacao,
		a.cd_intervalo,
		a.hr_prim_horario
from	prescr_dieta a,
		dieta b
where	nvl(ie_suspenso,'N') <> 'S'
and		nr_prescricao	= nr_prescricao_orig_p
and		a.cd_dieta = b.cd_dieta
and		nvl(b.ie_situacao,'A') = 'A';

begin

select	nvl(max(ie_copiar), 'S'),
		max(ie_regra_geral),
		max(nr_sequencia),
		nvl(max(ie_manter_intervalo), 'N')
into	ie_copiar_w,
		ie_regra_geral_w,
		nr_seq_regra_copia_w,
		ie_manter_intervalo_w
from	rep_regra_copia_crit
where	ie_tipo_item = 'DO'
and		nr_seq_regra = nr_seq_regra_p;

if	(ie_copiar_w = 'S') then
	begin
	
	select	max(nr_atendimento),
			max(dt_inicio_prescr),
			max(nr_horas_validade),
			max(dt_primeiro_horario)
	into	nr_atendimento_w,
			dt_inicio_prescr_w,
			nr_horas_validade_w,
			dt_primeiro_horario_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_nova_p;
	
	open c01;
	loop
	fetch c01 into cd_dieta_w,
				qt_parametro_w,
				ds_horarios_w,
				ds_observacao_w,
				ie_destino_dieta_w,
				ie_refeicao_w,
				nr_seq_anterior_w,
				ie_via_aplicacao_w,
				cd_intervalo_w,
				hr_prim_horario_w;
		exit when C01%notfound;
		begin
		
		if	(ie_regra_geral_w = 'H') then
			if	(ie_manter_intervalo_w = 'S') and
				(cd_intervalo_w is not null) then
			
				select	max(ie_operacao),
						max(qt_operacao)
				into	ie_operacao_w,
						qt_operacao_w						
				from	intervalo_prescricao
				where	cd_intervalo	= cd_intervalo_w;	
				
				dt_inicio_w			:= dt_inicio_prescr_w - 5;
				dt_final_w			:= sysdate + 5;
						
				qt_operacao_w		:= obter_ocorrencia_intervalo(cd_intervalo_w, 24, 'H');
				dt_inicio_item_w	:= dt_inicio_prescr_w - qt_operacao_w / 24;

				select	max(c.dt_horario)
				into	dt_inicio_item_w
				from	prescr_dieta_hor c,
						prescr_dieta b,
						prescr_medica a
				where	1 = 1
				and		nvl(c.ie_situacao,'A')	= 'A'
				and		c.nr_seq_dieta	= b.nr_sequencia
				and		c.nr_prescricao		= b.nr_prescricao
				and		c.dt_horario		>= dt_inicio_item_w - 220/24
				and		b.dt_suspensao		is null
				and		nvl(a.dt_liberacao_medico, a.dt_liberacao)	is not null
				and		b.cd_intervalo		= nvl(cd_intervalo_w, b.cd_intervalo)
				and 	nvl(b.qt_parametro,0) = nvl(qt_parametro_w, nvl(b.qt_parametro,0))
				and		b.cd_dieta	= cd_dieta_w
				and		b.nr_prescricao		= a.nr_prescricao
				and		a.dt_suspensao		is null
				and		a.dt_inicio_prescr between dt_inicio_w and dt_final_w
				and		a.nr_atendimento	= nr_atendimento_w;
							--and 	nvl(c.ie_horario_especial,'N') <> 'S'
				--and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
				
				
				hr_prim_horario_w	:= trim(nvl(hr_prim_horario_w, obter_Prim_dshorarios(ds_horarios_w)));
				
				if	(dt_inicio_item_w is null) then
					if	(length(hr_prim_horario_w) = 5) then
						if	(dt_inicio_prescr_w > to_date(to_char(dt_inicio_prescr_w, 'dd/mm/yyyy ') || hr_prim_horario_w, 'dd/mm/yyyy hh24:mi')) then
							dt_inicio_item_w	:= to_date(to_char(dt_inicio_prescr_w, 'dd/mm/yyyy ') || hr_prim_horario_w, 'dd/mm/yyyy hh24:mi');
						else
							dt_inicio_item_w	:= to_date(to_char(dt_inicio_prescr_w+1, 'dd/mm/yyyy ') || hr_prim_horario_w, 'dd/mm/yyyy hh24:mi');
						end if;
					else
						hr_prim_horario_w	:= null;
					end if;
				end if;
				
				if	(dt_inicio_item_w is not null) then
					dt_inicio_item_w	:= dt_inicio_item_w + qt_operacao_w / 24;
				end if;

				if	(nvl(qt_operacao_w,0) > 0) and
					(ie_operacao_w in ('H')) and
					(dt_inicio_item_w is not null) then
					dt_inicio_item_w	:= nvl(dt_inicio_item_w, dt_inicio_prescr_w);	
					
					if	(ie_operacao_w in ('H')) and
						(dt_inicio_item_w not between dt_inicio_prescr_w and (dt_inicio_item_w + qt_operacao_w)) then
						
						while (dt_inicio_item_w not between dt_inicio_prescr_w and (dt_inicio_item_w + qt_operacao_w)) loop
							dt_inicio_item_w	:= dt_inicio_item_w + qt_operacao_w/24;
						end loop;
					end if;
					
					hr_prim_horario_w	:= to_char(dt_inicio_item_w, 'hh24:mi');
				else
					dt_inicio_item_w	:= dt_inicio_prescr_w;
				end if;
				
				nr_ocorrencia_w	:= 0;
				Calcular_Horario_Prescricao(	nr_prescricao_nova_p, 
												cd_intervalo_w, 
												dt_inicio_prescr_w, 
												dt_inicio_item_w, 
												nr_horas_validade_w, 
												null, 
												0,
												0,
												nr_ocorrencia_w,
												ds_horarios_w, 
												ds_horarios2_w,
												'N', 
												null);
			
				ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;
				
			elsif (ie_manter_intervalo_w = 'N') then
			
				dt_ultimo_horario_w	:= PLT_obter_ultimo_horario(nr_prescricao_orig_p, nr_seq_anterior_w, 'D', nm_usuario_p);

				if	(dt_ultimo_horario_w is not null) and
					(dt_ultimo_horario_w > (sysdate - 1)) then

					hr_prim_horario_w	:= to_char(dt_ultimo_horario_w + /*nvl(*/ Obter_ocorrencia_intervalo(cd_intervalo_w, nvl(nr_horas_validade_w,24),'H')/ 24,'hh24:mi');
				end if;
					
				if	(hr_prim_horario_w <> '  :  ') and
					(hr_prim_horario_w is not null) then
					ds_horarios_w	:= '';
					
					if	(dt_prim_horario_ant_w is null) then
						dt_prim_horario_ant_w	:= dt_inicio_prescr_w;
					end if;
						
					Calcular_Horario_Prescricao(nr_prescricao_nova_p,
												cd_intervalo_w,
												dt_inicio_prescr_w,
												dt_prim_horario_ant_w,
												nr_horas_validade_w,
												null,
												0,
												0,
												nr_ocorrencia_w,
												ds_horarios_w,
												ds_horarios2_w,
												'N',
												null);
												
					ds_horarios_w := ds_horarios_w||ds_horarios2_w;
				else
					ds_horarios_w	:= '';
				end if;
			end if;
		end if;
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_dieta_w
		from	prescr_dieta
		where	nr_prescricao = nr_prescricao_nova_p;
		
		if	(dt_primeiro_horario_w is not null) then
			hr_prim_horario_w := to_char(dt_primeiro_horario_w,'hh24:mi');
		end if;

		Insert  into prescr_dieta (
						nr_prescricao,
						nr_sequencia,
						cd_dieta,
						dt_atualizacao,
						nm_usuario,
						qt_parametro,
						ds_horarios,
						ds_observacao,
						cd_motivo_baixa,
						dt_baixa,
						ie_destino_dieta,
						ie_refeicao,
						ie_suspenso,
						nr_seq_anterior,
						nr_prescricao_original,
						ie_via_aplicacao,
						cd_intervalo,
						hr_prim_horario
				) values (
						nr_prescricao_nova_p,
						nr_seq_dieta_w,
						cd_dieta_w,
						dt_prescricao_p,
						nm_usuario_p,
						qt_parametro_w,
						substr(ds_horarios_w,1,255),
						ds_observacao_w,
						null,
						null,
						ie_destino_dieta_w,
						ie_refeicao_w,
						'N',
						nr_seq_anterior_w,
						nr_prescricao_orig_p,
						ie_via_aplicacao_w,
						cd_intervalo_w,
						hr_prim_horario_w);

		commit;
		
		end;
	end loop;
	close c01;
	
	end;
end if;

end REP_copia_dieta;
/