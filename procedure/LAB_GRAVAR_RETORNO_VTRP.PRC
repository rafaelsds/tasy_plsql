create or replace procedure lab_gravar_retorno_vtrp(nr_lote_p		number,
													dt_integracao_p	date,
													cd_mensagem_p	number,
													ds_mensagem_p	varchar2,
													ds_carteira_p	varchar2,
													ds_guia_p		varchar2,
													ds_senha_p		varchar2,
													nm_usuario_p	varchar2) is
													
nr_sequencia_w	lab_retorno_integ_vtrp.nr_sequencia%type;													
													
begin
	if (nr_lote_p is not null and cd_mensagem_p is not null) then
		select	lab_retorno_integ_vtrp_seq.nextval
		into	nr_sequencia_w
		from	dual;
		insert	into	lab_retorno_integ_vtrp	(
			nr_sequencia,
			nr_lote,
			nm_usuario_nrec,
			nm_usuario,
			dt_atualizacao_nrec,
			dt_atualizacao,
			dt_integracao,
			cd_mensagem,
			ds_mensagem,
			ds_carteira,
			ds_guia,
			ds_senha)
		values (
			nr_sequencia_w,
			nr_lote_p,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			dt_integracao_p,
			cd_mensagem_p,
			ds_mensagem_p,
			ds_carteira_p,
			ds_guia_p,
			ds_senha_p);
		commit;
	end if;

end;
/