create or replace
procedure lote_ent_rec_insere_locais(
			nr_seq_consulta_p		number,
			nr_seq_tratamento_p		number,
			nr_seq_reconvocado_p	number) is 

begin

if 	(nr_seq_consulta_p is not null) and
	(nr_seq_tratamento_p is not null) and
	(nr_seq_reconvocado_p is not null) then


	update	lote_ent_reconvocado_cont
	set		NR_SEQ_LOCAL_TRAT = nr_seq_tratamento_p,
			NR_SEQ_LOCAL_CONS = nr_seq_consulta_p
	where	NR_SEQ_RECONVOCADO = nr_seq_reconvocado_p;
	
	update	lote_ent_reconvocado
	set		NR_SEQ_LOCAL_TRAT = nr_seq_tratamento_p,
			NR_SEQ_LOCAL_CONS = nr_seq_consulta_p
	where	NR_SEQUENCIA = nr_seq_reconvocado_p;
	
	commit;

end if;

end lote_ent_rec_insere_locais;
/