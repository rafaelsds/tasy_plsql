/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Procedure para gerar o relatório WPLS 2304 - "OPS - Retorno de beneficiários repassados (A200)"
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  X]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure ptu_gerar_relat_benef_inc_a200
			(	cd_unimed_p		varchar2,
				dt_periodo_p		date,
				ie_apresentar_inco_p	varchar2,
				nm_usuario_p		Varchar2) is 
			
nr_seq_retorno_benef_w		number(10);
nr_seq_benef_intercambio_w	number(10);
nr_seq_segurado_w		number(10);
cd_inconsistencia_w		number(10);
cd_usuario_plano_w		varchar2(30);
cd_unimed_destino_w		varchar2(10);
nr_seq_congenere_w		number(10);
nm_beneficiario_w		varchar2(255);
nr_seq_lote_w			number(10);
			
Cursor C01 is
	select	b.nr_sequencia,
		b.NR_SEQ_BENEF_INTERCAMBIO,
		b.CD_USUARIO_PLANO,
		b.CD_INCONSISTENCIA,
		c.CD_UNIMED_DESTINO,
		c.nr_seq_lote_envio
	from	ptu_retorno_mov_benef		b,
		ptu_retorno_movimentacao	a,
		ptu_intercambio			c
	where	b.NR_SEQ_RETORNO_MOV		= a.nr_sequencia
	and	a.NR_SEQ_INTERCAMBIO		= c.nr_sequencia
	and	a.IE_OPERACAO			= 'R'
	and	dt_periodo_p between c.dt_mov_inicio and c.dt_mov_fim
	and	((to_number(c.cd_unimed_destino)	= to_number(cd_unimed_p)) or (cd_unimed_p is null))
	and	((ie_apresentar_inco_p		= 'S' and b.CD_INCONSISTENCIA not in (3201,3202,3203)) or (ie_apresentar_inco_p = 'N'));
	

begin

delete	w_pls_relat_inconsist_a200
where	nm_usuario	= nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_seq_retorno_benef_w,
	NR_SEQ_BENEF_INTERCAMBIO_w,
	CD_USUARIO_PLANO_w,
	CD_INCONSISTENCIA_w,
	CD_UNIMED_DESTINO_w,
	nr_seq_lote_w;
exit when C01%notfound;
	begin
	
	if	(NR_SEQ_BENEF_INTERCAMBIO_w is not null) then
		select	nr_seq_segurado,
			NM_BENEFICIARIO
		into	nr_seq_segurado_w,
			nm_beneficiario_w
		from	PTU_INTERCAMBIO_BENEF
		where	nr_sequencia	= NR_SEQ_BENEF_INTERCAMBIO_w;
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_congenere_w
	from	pls_congenere
	where	to_number(cd_cooperativa)	= to_number(CD_UNIMED_DESTINO_w);
	
	insert into w_pls_relat_inconsist_a200
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			cd_usuario_plano,cd_inconsistencia,nr_seq_segurado,nr_seq_congenere,NR_SEQ_LOTE,
			NM_BENEFICIARIO)
	values	(	w_pls_relat_inconsist_a200_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			cd_usuario_plano_w,cd_inconsistencia_w,nr_seq_segurado_w,nr_seq_congenere_w,nr_seq_lote_w,
			nm_beneficiario_w);
	
	end;
end loop;
close C01;

commit;

end ptu_gerar_relat_benef_inc_a200;
/
