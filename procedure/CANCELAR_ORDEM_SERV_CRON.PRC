create or replace
procedure Cancelar_Ordem_Serv_Cron(
			nr_seq_proj_p	number,
			nr_seq_motivo_cancel_p	number, 
			nr_seq_estagio_cancel_p number,
			nm_usuario_p		Varchar2) is 

nr_seq_proj_cro_w 	number;			
nr_seq_cro_eta_w 	number;
nr_seq_os_w 		number;
ds_his_w			long;
			
cursor c01 is --seleciona todos os cronogramas conforme o projeto
	select	e.nr_sequencia nr_seq_cronograma
	from	proj_cronograma e,
		prp_processo_fase p,
		prp_fase_processo f
	where	p.nr_Sequencia = e.nr_seq_processo_fase
	and	f.nr_sequencia = p.nr_seq_fase_processo
	and	e.nr_seq_proj = nr_seq_proj_p
	and	(obter_acesso_cronograma_npi(nr_seq_proj_p, f.nr_sequencia, nm_usuario_p, 'V') = 'S');
	
cursor c02 is --seleciona as etapas conforme o cronograma
	select	nr_sequencia
	from 	proj_cron_etapa 
	where 	nr_seq_cronograma = nr_seq_proj_cro_w; 
	
cursor c03 is --seleciona as OS's de cada etapa
	select	v.nr_sequencia
	from	man_ordem_servico_v v
	where	nr_seq_proj_cron_etapa = nr_seq_cro_eta_w;
	
begin

	open c01;
	loop
	fetch c01 into 
		nr_seq_proj_cro_w ;
	exit when c01%notfound;
		----INICIO CURSOR C02
		open c02;
		loop
		fetch c02 into 
			nr_seq_cro_eta_w;
		exit when c02%notfound;
			--INICIO CURSOR C03		
			open c03;
			loop
			fetch c03 into 
				nr_seq_os_w;
			exit when c03%notfound;
				
				select ds_historico 
				into ds_his_w
				from com_cliente_hist 
				where nr_seq_projeto = nr_seq_proj_p 
				and nr_seq_tipo = 21;
				
				insert 
					into	man_ordem_serv_tecnico (nr_sequencia, 
							nr_seq_ordem_serv, 
							dt_atualizacao, 
							dt_historico, 
							dt_liberacao,
							nm_usuario, 
							nr_seq_tipo,
							ie_origem,
							ds_relat_tecnico) 
					values ( man_ordem_serv_tecnico_seq.NEXTVAL, 
							 nr_seq_os_w, 
							 sysdate, 
							 sysdate, 
							 sysdate, 
							 nm_usuario_p, 
							 72, 
							 'I',
							 ds_his_w);
				
				man_cancelar_ordem_servico(nr_seq_os_w, nr_seq_motivo_cancel_p, nr_seq_estagio_cancel_p, nm_usuario_p);
			end loop;
			close c03;
			--FIM CURSOR C03
		end loop;
		close c02;
		----FIM CURSOR C02
	end loop;
	close c01;
commit;

end Cancelar_Ordem_Serv_Cron;
/