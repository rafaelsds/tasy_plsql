create or replace
procedure pls_imp_grupo_estoque_unimed( cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

ie_produto_w		varchar2(100);
ds_linha_w		varchar2(4000);
qt_registros_w		number(5);
ds_valor_w		varchar2(2000);
contador_w  		number(5) := 0;			

/* =================== CAMPOS ================== */			
cd_classificacao_w	varchar2(30);
cd_final_w		number(10);
cd_grupo_estoque_w	number(5);
cd_inicial_w		number(10);
cd_pai_w		number(5);
ds_grupo_estoque_w	varchar2(255);
ie_grau_w		varchar2(3);
ie_tipo_w		varchar2(2);
nr_nivel_w		number(5);
ie_layout_simples_w	varchar2(2) := 'N';
			
Cursor C01 is
	select	ds_conteudo
	from	w_import_mat_unimed
	where	elimina_acentuacao(substr(ds_conteudo,1,67)) <>  elimina_acentuacao('Chave;Estrutura;Pai;N�vel;Grau;Nome;Tipo;C�digoInicial;C�digoFinal')
	order by
		nr_sequencia;
			
begin

--Valida��o de arquivo selecionado para a Importa��o
select	count(*)
into	ie_produto_w
from	w_import_mat_unimed
where	elimina_acentuacao(substr(ds_conteudo,1,67)) =  elimina_acentuacao('Chave;Estrutura;Pai;N�vel;Grau;Nome;Tipo;C�digoInicial;C�digoFinal')
and	rownum = 1;

if	(ie_produto_w = 0) then
	select	count(*)
	into	ie_produto_w
	from	w_import_mat_unimed
	where	elimina_acentuacao(substr(ds_conteudo,1,2)) =  elimina_acentuacao('0;')
	and	rownum = 1;
	
	if	(ie_produto_w > 0) then
		ie_layout_simples_w := 'S';
	end if;
end if;

if	(ie_produto_w	= 0 ) then
	wheb_mensagem_pck.exibir_mensagem_abort(265255,'');
	--Mensagem: Este arquivo n�o possui cadastros v�lidos de Grupo de Estoque. Verifique o arquivo.

else
	if	(ie_layout_simples_w = 'N') then
		open C01;
		loop
		fetch C01 into	
			ds_linha_w;
		exit when C01%notfound;
			begin
			
			contador_w	:= 0;
			while	(ds_linha_w is not null) and
				(contador_w < 255) loop
				begin

				if	(instr(ds_linha_w,';') = 0) then
					ds_valor_w	:= ds_linha_w;
					ds_linha_w	:= null;
				else
					ds_valor_w	:= substr(ds_linha_w,1,instr(ds_linha_w,';') - 1);
					ds_linha_w	:= substr(ds_linha_w,instr(ds_linha_w,';') + 1,length(ds_linha_w));
				end if;

				
				if	(contador_w = 0) then
				cd_grupo_estoque_w	:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 1) then
				cd_classificacao_w	:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 2) then
				cd_pai_w		:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 3) then
				nr_nivel_w		:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 4) then
				ie_grau_w		:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 5) then
				ds_grupo_estoque_w	:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 6) then
				ie_tipo_w		:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 7) then
				cd_inicial_w		:= ltrim(ds_valor_w);
				end if;
				if	(contador_w = 8) then
				cd_final_w		:= ltrim(ds_valor_w);
				end if;

				contador_w := contador_w + 1;
				
				end;
			end loop;
			
			-- Verifica se j� existe o registro		
			select	count(*)
			into	qt_registros_w
			from	pls_grupo_est_fed_sc
			where	cd_grupo_estoque	= cd_grupo_estoque_w;
			
			--Se n�o existir, faz Insert
			if	(qt_registros_w	= 0) then
			
				insert into pls_grupo_est_fed_sc
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_estabelecimento,
					cd_classificacao,
					cd_final,
					cd_grupo_estoque,
					cd_inicial,
					cd_pai,
					ds_grupo_estoque,
					ie_grau,
					ie_tipo,
					nr_nivel)
				values	(pls_grupo_est_fed_sc_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_estabelecimento_p,
					cd_classificacao_w,
					cd_final_w,
					cd_grupo_estoque_w,
					cd_inicial_w,
					cd_pai_w,
					ds_grupo_estoque_w,
					ie_grau_w,
					ie_tipo_w,
					nr_nivel_w);
			--Se existir, faz Update
			else

				update	pls_grupo_est_fed_sc
				set	nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					ds_grupo_estoque	= ds_grupo_estoque_w
				where	cd_grupo_estoque	= cd_grupo_estoque_w;
					
			end if;
			end;
		end loop;
		close C01;
	
	elsif	(ie_layout_simples_w = 'S') then
		open C01;
		loop
		fetch C01 into	
			ds_linha_w;
		exit when C01%notfound;
			begin
			
			cd_classificacao_w	:= null;
			cd_final_w		:= null;
			cd_grupo_estoque_w	:= null;
			cd_inicial_w		:= null;
			cd_pai_w		:= null;
			ds_grupo_estoque_w	:= null;
			ie_grau_w		:= null;
			ie_tipo_w		:= null;
			nr_nivel_w		:= null;			
			contador_w		:= 0;
			
			while	(ds_linha_w is not null) and
				(contador_w < 255) loop
				begin

				if	(instr(ds_linha_w,';') = 0) then
					ds_valor_w	:= ds_linha_w;
					ds_linha_w	:= null;
				else
					ds_valor_w	:= substr(ds_linha_w,1,instr(ds_linha_w,';') - 1);
					ds_linha_w	:= substr(ds_linha_w,instr(ds_linha_w,';') + 1,length(ds_linha_w));
				end if;

				
				if	(contador_w = 0) then
					cd_grupo_estoque_w	:= ltrim(ds_valor_w);
				
				elsif	(contador_w = 1) then
					ds_grupo_estoque_w	:= ltrim(ds_valor_w);
				end if;

				contador_w := contador_w + 1;
				
				end;
			end loop;
			
			-- Verifica se j� existe o registro		
			select	count(*)
			into	qt_registros_w
			from	pls_grupo_est_fed_sc
			where	cd_grupo_estoque	= cd_grupo_estoque_w;
			
			--Se n�o existir, faz Insert
			if	(qt_registros_w	= 0) then
			
				insert into pls_grupo_est_fed_sc
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_estabelecimento,
					cd_classificacao,
					cd_final,
					cd_grupo_estoque,
					cd_inicial,
					cd_pai,
					ds_grupo_estoque,
					ie_grau,
					ie_tipo,
					nr_nivel)
				values	(pls_grupo_est_fed_sc_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_estabelecimento_p,
					cd_classificacao_w,
					cd_final_w,
					cd_grupo_estoque_w,
					cd_inicial_w,
					cd_pai_w,
					ds_grupo_estoque_w,
					ie_grau_w,
					ie_tipo_w,
					nr_nivel_w);
			--Se existir, faz Update
			else

				update	pls_grupo_est_fed_sc
				set	nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					ds_grupo_estoque	= ds_grupo_estoque_w
				where	cd_grupo_estoque	= cd_grupo_estoque_w;
					
			end if;
			end;
		end loop;
		close C01;
	end if;
end if;



commit;

end pls_imp_grupo_estoque_unimed;
/