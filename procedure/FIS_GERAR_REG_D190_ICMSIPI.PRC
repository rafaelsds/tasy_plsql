create or replace
procedure fis_gerar_reg_D190_icmsipi(	nr_seq_controle_p	number,
					nr_seq_nota_p		number) is 

/*REGISTRO D190: REGISTRO ANALTICO DO DOCUMENTO (CDIGO 21 E 22).*/

-- VARIABLES
ie_gerou_dados_bloco_w 	varchar2(1) := 'N';
qt_cursor_w 			number(10) := 0;
nr_vetor_w  			number(10) := 0;
vl_total_despesa_w		nota_fiscal.vl_total_nota%type := 0;	

-- NR_SEQ_ICMSIPI_D190_W
nr_seq_icmsipi_D190_w 	fis_efd_icmsipi_D190.nr_sequencia%type;

-- USUARIO
nm_usuario_w 			usuario.nm_usuario%type;

/*Cursor que retorna as informaes para o registro d190*/
cursor c_reg_a_doc is
	select	a.cd_cst_icms,
			a.cd_cfop,
			decode(a.ie_tributacao_icms, '00', a.tx_aliq_icms, '10', a.tx_aliq_icms, '20', a.tx_aliq_icms, '70', a.tx_aliq_icms, 0) tx_aliq_icms,
			sum(a.vl_opr) vl_opr,
			sum(decode(a.ie_tributacao_icms, '00', a.vl_bc_icms, '10', a.vl_bc_icms, '20', a.vl_bc_icms, '70', a.vl_bc_icms, 0)) vl_bc_icms,   
			sum(decode(a.ie_tributacao_icms, '00', a.vl_icms, '10', a.vl_icms, '20', a.vl_icms, '70', a.vl_icms, 0)) vl_icms,       
			sum(decode(substr(a.cd_cst_icms, length(cd_cst_icms) -1, length(cd_cst_icms)), '20', a.vl_red_bc,'70', a.vl_red_bc,'90', a.vl_red_bc, 0)) vl_red_bc,
			max(cd_obs) cd_obs
	from	(
			select	nvl(tax_get_active_cst(b.nr_sequencia, b.nr_item_nf, b.cd_estabelecimento), '010') cd_cst_icms,
					substr(Elimina_Caracter(obter_dados_natureza_operacao(a.cd_natureza_operacao, 'CF'), '.'), 1, 4) cd_cfop,
					nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TX') tx_aliq_icms,
					b.vl_total_item_nf vl_opr,
					nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'BC') vl_bc_icms,   
					nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TRIB') vl_icms,
					nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'RICMS') vl_red_bc,
					c.ie_tributacao_icms,
					(select	d.nr_sequencia
					from	fis_variacao_fiscal v,
						fis_dispositivo_legal d
					where	b.nr_seq_variacao_fiscal = v.nr_sequencia
					and	v.cd_dispositivo_legal = d.nr_sequencia) cd_obs
			from	material_fiscal c,
					nota_fiscal_item b,
					nota_fiscal a
			where	a.nr_sequencia		= b.nr_sequencia
			and		b.cd_material		= c.cd_material(+)
			and 	a.nr_sequencia		= nr_seq_nota_p
			) a
	group by	a.cd_cst_icms,
			a.cd_cfop,
			decode(a.ie_tributacao_icms, '00',a.tx_aliq_icms, '10',a.tx_aliq_icms, '20',a.tx_aliq_icms, '70', a.tx_aliq_icms, 0);
			
/*Criao do array com o tipo sendo do cursor eespecificado - c_sat_ecf*/
type reg_c_reg_a_doc is table of c_reg_a_doc%RowType;
vetRegDoc reg_c_reg_a_doc;

/*Criao do array com o tipo sendo da tabela especificada - FIS_EFD_ICMSIPI_D190 */
type registro is table of fis_efd_icmsipi_D190%rowtype index by pls_integer;
fis_registros_w registro;

begin

/*Obteo do usurio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_reg_a_doc;
loop
fetch c_reg_a_doc bulk collect into vetRegDoc limit 1000;
	for i in 1 .. vetRegDoc.count loop
	
		/*Limpeza de variaveis*/
		vl_total_despesa_w:= 0;
		
		/*Incrementa a variavel para o array*/
		qt_cursor_w:=	qt_cursor_w + 1;

		if (ie_gerou_dados_bloco_w = 'N') then
			ie_gerou_dados_bloco_w := 'S';
		end if;
		
		/*Busca o total de despesas da nota e insere no item com maior valor agrupado por cts, cfop e aliquota do icms
		Como o select est ordenado por valor no primeiro registro deve entrar*/
		if	(qt_cursor_w = 1) then
			begin
				select	nvl((vl_frete + vl_seguro + vl_despesa_acessoria) - vl_descontos,0)
				into	vl_total_despesa_w
				from	nota_fiscal
				where	nr_sequencia = nr_seq_nota_p
				and	rownum = 1;
			exception
			when others then
				vl_total_despesa_w:= 0;
			end;
		end if;			

		/*Busca da sequencia da tabela especificada - fis_efd_icmsipi_D190*/
		select	fis_efd_icmsipi_D190_seq.nextval
		into	nr_seq_icmsipi_D190_w
		from	dual;

		/*Inserindo valores no array para realizao do forall posteriormente*/
		fis_registros_w(qt_cursor_w).nr_sequencia        	:= nr_seq_icmsipi_D190_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao      	:= sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario          	:= nm_usuario_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao_nrec 	:= sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario_nrec     	:= nm_usuario_w;
		fis_registros_w(qt_cursor_w).cd_reg              	:= 'D190';
		fis_registros_w(qt_cursor_w).cd_cst_icms         	:= vetRegDoc(i).cd_cst_icms;
		fis_registros_w(qt_cursor_w).cd_cfop             	:= vetRegDoc(i).cd_cfop;
		fis_registros_w(qt_cursor_w).tx_aliq_icms        	:= vetRegDoc(i).tx_aliq_icms;
		fis_registros_w(qt_cursor_w).vl_opr              	:= vetRegDoc(i).vl_opr + vl_total_despesa_w;
		fis_registros_w(qt_cursor_w).vl_bc_icms          	:= vetRegDoc(i).vl_bc_icms;
		fis_registros_w(qt_cursor_w).vl_icms             	:= vetRegDoc(i).vl_icms;
		fis_registros_w(qt_cursor_w).cd_cfop             	:= vetRegDoc(i).cd_cfop;
		fis_registros_w(qt_cursor_w).vl_red_bc           	:= vetRegDoc(i).vl_red_bc;
		fis_registros_w(qt_cursor_w).cd_obs              	:= vetRegDoc(i).cd_obs;
		fis_registros_w(qt_cursor_w).nr_seq_controle     	:= nr_seq_controle_p;
		fis_registros_w(qt_cursor_w).nr_seq_nota	 		:= nr_seq_nota_p;

		if (nr_vetor_w >= 1000) then
			/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_D190 */
			forall i in fis_registros_w.first .. fis_registros_w.last
				insert into fis_efd_icmsipi_d190 values fis_registros_w (i);

			nr_vetor_w := 0;
			fis_registros_w.delete;

			commit;
		end if;

		/*incrementa variavel para realizar o forall quando chegar no valor limite*/
		nr_vetor_w := nr_vetor_w + 1;
	end loop;
exit when c_reg_a_doc%notfound;
end loop;
close c_reg_a_doc;

if (fis_registros_w.count > 0) then
	/*Inserindo registro que no entraram outro for all devido a quantidade de registros no vetor*/
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_D190 values fis_registros_w (i);

	fis_registros_w.delete;

	commit;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

/*Atualizao informao no controle de gerao de registro para SIM*/
if (ie_gerou_dados_bloco_w = 'S') then
	update 	fis_efd_icmsipi_controle
	set 	ie_mov_D 		= 'S'
	where 	nr_sequencia 	= nr_seq_controle_p;
end if;

end fis_gerar_reg_D190_icmsipi;
/
