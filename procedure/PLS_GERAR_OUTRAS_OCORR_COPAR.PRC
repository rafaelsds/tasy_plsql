create or replace
procedure pls_gerar_outras_ocorr_copar
		(	nr_seq_regra_copart_p	number,
			nr_seq_conta_proc_p	number,
			nr_seq_segurado_p	number,
			ie_outras_ocor_p out	varchar2,
			qt_ocorrencias_p out	number) is

ie_outras_ocorrencias_w		varchar2(1) := 'S';
qt_ocorrencia_grupo_serv_w	number(5);
qt_periodo_ocor_w		number(5);
ie_tipo_periodo_ocor_w		varchar2(5);
nr_seq_grupo_serv_w		number(10);
qt_dias_regra_ocorrencias_w	number(10);
dt_regra_mes_w			date;
qt_eventos_w			number(10);
qt_eventos_cobradas_w		number(10);
qt_total_eventos_w		number(10);
qt_liberados_w			number(10);
qt_ocorrencias_w		number(10);
nr_seq_conta_w			number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ie_ano_calendario_out_ocor_w	varchar2(10);
ie_consid_outra_ocor_regra_w	varchar2(10);
dt_inicio_regra_w		date;
dt_fim_regra_w			date;
dt_item_w			date;
cd_especialidade_w		number(15);
cd_grupo_w			number(15);
cd_area_w			number(15);
ie_origem_w			number(10);
nr_seq_tipo_coparticipacao_w	pls_regra_coparticipacao.nr_seq_tipo_coparticipacao%type;
ie_tipo_incidencia_w		pls_regra_coparticipacao.ie_tipo_incidencia%type;
nr_seq_titular_w		pls_segurado.nr_seq_titular%type;
nr_seq_segurado_w		pls_segurado.nr_seq_titular%type;
count_w				pls_integer;
qt_procedimento_w		pls_conta_proc.qt_procedimento%type;
cd_estabelecimento_w		pls_conta.cd_estabelecimento%type;
ie_considerar_dt_lanc_cop_w	pls_parametros.ie_considerar_dt_lanc_copartc%type;
ie_forma_inicidencia_copart_w	pls_parametros.ie_forma_inicidencia_copart%type;

cursor C01 is
	--Tratamento somente para o benefici�rio
	select	a.cd_procedimento,
			a.ie_origem_proced,
		nvl(a.qt_procedimento,0)
	from	pls_conta_proc		a,
		pls_conta		b,
		pls_segurado		c
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_segurado	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_segurado_w
	and	ie_tipo_incidencia_w	= 'B'
	and	((b.ie_status		in ('S','F'))	or	(a.nr_seq_conta	= nr_seq_conta_w and a.nr_sequencia < nr_seq_conta_proc_p))
	and	a.ie_status		!= 'D'
	and	a.nr_sequencia		<> nr_seq_conta_proc_p
	and	a.ie_tipo_despesa	<> '3'
	and	((a.dt_procedimento_referencia  <= dt_item_w) or (ie_considerar_dt_lanc_cop_w = 'N'))
	and  	nvl(a.dt_procedimento,b.dt_atendimento_referencia) between dt_inicio_regra_w and dt_fim_regra_w
	union all
	--Tratamento para titulares e dependentes
	select	a.cd_procedimento,
			a.ie_origem_proced,
		nvl(a.qt_procedimento,0)
	from	pls_conta_proc		a,
		pls_conta		b,
		pls_segurado		c
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_segurado	= c.nr_sequencia
	and	((c.nr_sequencia	= nr_seq_segurado_w) or (c.nr_seq_titular = nr_seq_titular_w))
	and	ie_tipo_incidencia_w	= 'T'
	and	((b.ie_status		in ('S','F'))	or	(a.nr_seq_conta	= nr_seq_conta_w and a.nr_sequencia < nr_seq_conta_proc_p))
	and	a.ie_status		!= 'D'
	and	a.nr_sequencia		<> nr_seq_conta_proc_p
	and	a.ie_tipo_despesa	<> '3'
	and	((a.dt_procedimento_referencia  <= dt_item_w) or (ie_considerar_dt_lanc_cop_w = 'N'))
	and  	nvl(a.dt_procedimento,b.dt_atendimento_referencia) between dt_inicio_regra_w and dt_fim_regra_w;
	
cursor C02 is
	select	a.cd_procedimento,
			a.ie_origem_proced,
		nvl(a.qt_procedimento,0)
	from	pls_conta_proc		a,
		pls_conta		b,
		pls_segurado		c
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_segurado	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_segurado_p
	and	a.nr_sequencia		= nr_seq_conta_proc_p
	and	a.ie_tipo_despesa	<> '3'
	and  	nvl(a.dt_procedimento,b.dt_atendimento_referencia) between dt_inicio_regra_w and dt_fim_regra_w;

begin

begin
select	qt_ocorrencia_grupo_serv,
	qt_periodo_ocor,
	ie_tipo_periodo_ocor,
	nr_seq_grupo_serv,
	cd_procedimento,
	ie_origem_proced,
	nvl(ie_ano_calendario_outras_ocor,'N'),
	nvl(ie_considera_outra_ocor_regra,'N'),
	nr_seq_tipo_coparticipacao,
	nvl(ie_tipo_incidencia,'B')
into	qt_ocorrencia_grupo_serv_w,
	qt_periodo_ocor_w,
	ie_tipo_periodo_ocor_w,
	nr_seq_grupo_serv_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_ano_calendario_out_ocor_w,
	ie_consid_outra_ocor_regra_w,
	nr_seq_tipo_coparticipacao_w,
	ie_tipo_incidencia_w
from	pls_regra_coparticipacao
where	nr_sequencia	= nr_seq_regra_copart_p;
exception
when others then
	qt_ocorrencia_grupo_serv_w	:= null;
	qt_periodo_ocor_w		:= null;
	ie_tipo_periodo_ocor_w		:= null;
	nr_seq_grupo_serv_w		:= null;
	cd_procedimento_w		:= null;
	ie_origem_proced_w		:= null;
	ie_tipo_incidencia_w		:= 'B';
end;

if	(ie_tipo_incidencia_w not in ('T','B')) then
	ie_tipo_incidencia_w	:= 'B';
end if;

select	a.nr_seq_conta,
	nvl(a.dt_procedimento,b.dt_atendimento_referencia),
	b.cd_estabelecimento
into	nr_seq_conta_w,
	dt_item_w,
	cd_estabelecimento_w
from	pls_conta_proc	a,
	pls_conta	b
where	a.nr_seq_conta	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_conta_proc_p;

--max para evitar no data found(retornar� no m�ximo um registro)
select	max(ie_considerar_dt_lanc_copartc),
	nvl(max(ie_forma_inicidencia_copart),'N') 
into	ie_considerar_dt_lanc_cop_w,
	ie_forma_inicidencia_copart_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_w;

if	(nvl(ie_considerar_dt_lanc_cop_w,'X') not in ('S','N')) then
	ie_considerar_dt_lanc_cop_w	:= 'N';
end if;

/*aaschlote 28/03/2011 os - 294117*/
if	(qt_ocorrencia_grupo_serv_w is not null) and
	((qt_periodo_ocor_w is not null) and
	(ie_tipo_periodo_ocor_w is not null) or
	(ie_ano_calendario_out_ocor_w = 'S')) then
	
	if	(ie_tipo_incidencia_w = 'T') then
		select	nr_seq_titular
		into	nr_seq_titular_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p;
		
		--Caso o usu�rio for dependente, ent�o o segurado assume a variavel do titular
		if	(nr_seq_titular_w is not null) then
			nr_seq_segurado_w	:= nr_seq_titular_w;
		else
			nr_seq_titular_w	:= nr_seq_segurado_p;
			nr_seq_segurado_w	:= nr_seq_segurado_p;
		end if;
	else
		nr_seq_segurado_w	:= nr_seq_segurado_p;
	end if;
	
	if	(ie_ano_calendario_out_ocor_w = 'S') then
		dt_inicio_regra_w	:= trunc(to_date('01/01/'||to_char(dt_item_w,'yyyy')),'dd');
		dt_fim_regra_w		:= fim_dia(to_date('31/12/'||to_char(dt_item_w,'yyyy')));
	else
		dt_fim_regra_w			:= fim_dia(dt_item_w);
		/*montar a quantidade de dias da regra*/
		if	(ie_tipo_periodo_ocor_w = 'D') then
			dt_inicio_regra_w		:= trunc(dt_item_w,'dd') - qt_periodo_ocor_w;
		elsif	(ie_tipo_periodo_ocor_w = 'M') then
			qt_periodo_ocor_w		:= qt_periodo_ocor_w - 1;
			
			if	(qt_periodo_ocor_w < 0) then
				qt_periodo_ocor_w	:= 0;
			end if;
			
			dt_inicio_regra_w		:= add_months(trunc(dt_item_w,'mm'),-qt_periodo_ocor_w);
			dt_fim_regra_w			:= fim_mes(dt_item_w);
		end if;
	end if;
	
	if 	((nr_seq_grupo_serv_w is null) and
		(cd_procedimento_w is null)) then
		qt_eventos_w := 0;
		qt_liberados_w := 0;		
		open C01;
		loop		
		fetch C01 into 
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_procedimento_w;
		exit when C01%notfound;
			begin
			pls_obter_estrut_proc
			(cd_procedimento_w,
			ie_origem_proced_w,
			cd_area_w,
			cd_especialidade_w,
			cd_grupo_w,
			ie_origem_w);
			count_w := 0;
			
			select	count(1)
			into	count_w
			from	pls_coparticipacao_proc
			where	nr_seq_tipo_coparticipacao = nr_seq_tipo_coparticipacao_w
			and 	((cd_procedimento is not null and cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
			and	((cd_area_procedimento is not null and cd_area_procedimento = cd_area_w) or (cd_area_procedimento is null))
			and	((cd_especialidade = cd_especialidade_w and cd_especialidade_w is not null) or (cd_especialidade is null))
			and	((cd_grupo_proc = cd_grupo_w and cd_grupo_w is not null) or (cd_grupo_proc is null))
			and	(((nr_seq_grupo_servico is not null) and (pls_se_grupo_preco_servico(nr_seq_grupo_servico,cd_procedimento_w,ie_origem_proced_w)	= 'S'))
				or	(nr_seq_grupo_servico is null));
			
			if 	(count_w > 0) then
				qt_eventos_w := qt_eventos_w + qt_procedimento_w;
			end if;
			end;
		end loop;
		close C01;
		
		open C02;
		loop		
		fetch C02 into 
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_procedimento_w;
		exit when C02%notfound;
			begin
			pls_obter_estrut_proc
			(cd_procedimento_w,
			ie_origem_proced_w,
			cd_area_w,
			cd_especialidade_w,
			cd_grupo_w,
			ie_origem_w);
			
			count_w := 0;
			
			select	count(1)
			into	count_w
			from	pls_coparticipacao_proc
			where	nr_seq_tipo_coparticipacao = nr_seq_tipo_coparticipacao_w
			and 	((cd_procedimento is not null and cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
			and	((cd_area_procedimento is not null and cd_area_procedimento = cd_area_w) or (cd_area_procedimento is null))
			and	((cd_especialidade = cd_especialidade_w and cd_especialidade_w is not null) or (cd_especialidade is null))
			and	((cd_grupo_proc = cd_grupo_w and cd_grupo_w is not null) or (cd_grupo_proc is null))
			and	(((nr_seq_grupo_servico is not null) and (pls_se_grupo_preco_servico(nr_seq_grupo_servico,cd_procedimento_w,ie_origem_proced_w)	= 'S'))
				or	(nr_seq_grupo_servico is null));
				
			if 	(count_w > 0) then
				qt_liberados_w := qt_liberados_w + qt_procedimento_w;
			end if;
			end;
		end loop;
		close C02;
	else		
		--Tratamento somente para o benefici�rio
		if	(ie_tipo_incidencia_w = 'B') then		
			/*verificar a quantidade de  procedimentos que est�o no grupo de servi�os que j� foram cobradas*/
			select	sum(a.qt_procedimento)
			into	qt_eventos_w
			from	pls_conta_proc		a,
				pls_conta		b,
				pls_segurado		c
			where	a.nr_seq_conta		= b.nr_sequencia
			and	b.nr_seq_segurado	= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_segurado_w
			and	((b.ie_status		in ('S','F'))	or	(a.nr_seq_conta	= nr_seq_conta_w and a.nr_sequencia < nr_seq_conta_proc_p))
			and	a.nr_sequencia		<> nr_seq_conta_proc_p
			and	((a.dt_procedimento_referencia  <= dt_item_w) or (ie_considerar_dt_lanc_cop_w = 'N'))
			and	a.ie_status		!= 'D'
			and	a.ie_tipo_despesa	<> '3'
			and	(((nr_seq_grupo_serv_w is not null) and (pls_se_grupo_preco_servico(nr_seq_grupo_serv_w,a.cd_procedimento,a.ie_origem_proced)	= 'S'))
				or	((nr_seq_grupo_serv_w is null) and (a.cd_procedimento = cd_procedimento_w) and (a.ie_origem_proced = ie_origem_proced_w)))
			and  	nvl(a.dt_procedimento,b.dt_atendimento_referencia) between dt_inicio_regra_w and dt_fim_regra_w;
		--Tratamento para titulares e dependentes
		elsif	(ie_tipo_incidencia_w = 'T') then	
			/*verificar a quantidade de  procedimentos que est�o no grupo de servi�os que j� foram cobradas*/
			select	sum(a.qt_procedimento)
			into	qt_eventos_w
			from	pls_conta_proc		a,
				pls_conta		b,
				pls_segurado		c
			where	a.nr_seq_conta		= b.nr_sequencia
			and	b.nr_seq_segurado	= c.nr_sequencia
			and	((c.nr_sequencia	= nr_seq_segurado_w) or (c.nr_seq_titular = nr_seq_titular_w))
			and	((b.ie_status		in ('S','F'))	or	(a.nr_seq_conta	= nr_seq_conta_w and a.nr_sequencia < nr_seq_conta_proc_p))
			and	a.nr_sequencia		<> nr_seq_conta_proc_p	
			and	((a.dt_procedimento_referencia  <= dt_item_w) or (ie_considerar_dt_lanc_cop_w = 'N'))
			and	a.ie_status		!= 'D'
			and	a.ie_tipo_despesa	<> '3'
			and	(((nr_seq_grupo_serv_w is not null) and (pls_se_grupo_preco_servico(nr_seq_grupo_serv_w,a.cd_procedimento,a.ie_origem_proced)	= 'S'))
				or	((nr_seq_grupo_serv_w is null) and (a.cd_procedimento = cd_procedimento_w) and (a.ie_origem_proced = ie_origem_proced_w)))
			and  	nvl(a.dt_procedimento,b.dt_atendimento_referencia) between dt_inicio_regra_w and dt_fim_regra_w;
		end if;
		
		
		/*verificar a quantidade de procedimentos que est� sendo gerada a coparticipa��o*/
		select	sum(a.qt_procedimento)
		into	qt_liberados_w
		from	pls_conta_proc		a,
			pls_conta		b,
			pls_segurado		c
		where	a.nr_seq_conta		= b.nr_sequencia
		and	b.nr_seq_segurado	= c.nr_sequencia
		and	c.nr_sequencia		= nr_seq_segurado_p
		and	a.nr_sequencia		= nr_seq_conta_proc_p
		and	a.ie_tipo_despesa	<> '3'
		and	(((nr_seq_grupo_serv_w is not null) and (pls_se_grupo_preco_servico(nr_seq_grupo_serv_w,a.cd_procedimento,a.ie_origem_proced)	= 'S'))
			or	((nr_seq_grupo_serv_w is null) and (a.cd_procedimento = cd_procedimento_w) and (a.ie_origem_proced = ie_origem_proced_w)))
		and  	nvl(a.dt_procedimento,b.dt_atendimento_referencia) between dt_inicio_regra_w and dt_fim_regra_w;
	end if;	

	/*caso n�o existir procedimentos naquele grupo, n�o entra na regra*/
	if	((nvl(qt_liberados_w,0)	<> 0) or (ie_consid_outra_ocor_regra_w = 'S')) then
		qt_total_eventos_w	:= nvl(qt_eventos_w,0) + nvl(qt_liberados_w,0);
		
		if	(qt_total_eventos_w	< qt_ocorrencia_grupo_serv_w) then
			ie_outras_ocorrencias_w	:= 'N';
			qt_ocorrencias_w	:= 0;
		else	
			--Se for 'N' dever� gerar a ocorr�ncia ao superar a quantidade apontada na regra de coparticipa��o, caso contr�rio,
			--dever� gerar a ocorr�ncia ao igualar a quantidade(Antes desse ajuste, a ocorr�ncia era gerada ao superar a quantidade, por�m as vezes
			--gerava ao igualar, por outras quest�es na pls_gerar_coparticipa��o, tendo um comportamento que dificultava a defini��o de quando a ocorr�ncia seria gerada)
			--Com esse ajuste, � poss�vel apontar que a ocorr�ncia ser� gerada de fato ao atingir a quantidade especificada na regra.
			if	(ie_forma_inicidencia_copart_w = 'N') then
				qt_ocorrencias_w	:= qt_total_eventos_w - qt_ocorrencia_grupo_serv_w;
			else
				qt_ocorrencias_w	:= 1 + qt_total_eventos_w - qt_ocorrencia_grupo_serv_w;
			end if;
		end if;
	end if;
end if;

ie_outras_ocor_p	:= ie_outras_ocorrencias_w;
qt_ocorrencias_p	:= qt_ocorrencias_w;

end pls_gerar_outras_ocorr_copar;
/
