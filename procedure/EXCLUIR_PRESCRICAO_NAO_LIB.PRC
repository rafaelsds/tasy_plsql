CREATE OR REPLACE
PROCEDURE Excluir_Prescricao_Nao_Lib(
				qt_hora_adicional_p		Number,
				cd_motivo_baixa_p		Number,
				dt_parametro_p		date)	   IS

/* o motivo de baixa deve ser o mesmo utilizado na procedure de baixa das prescricoes */

nr_prescricao_w		Number(14,0);
dt_inicial_w		date;
dt_final_w		date;

Cursor c010 is
	select distinct a.nr_prescricao
	from 	Prescr_medica b,
		prescr_material a
	where a.cd_motivo_baixa in (0, cd_motivo_baixa_p)
        and a.nr_prescricao = b.nr_prescricao
	  and b.dt_prescricao between dt_inicial_w and dt_final_w
	  and b.dt_liberacao is null
	  and b.dt_liberacao_medico is null
	  and not exists
		(select	1
		from 	cirurgia c
		where 	c.nr_prescricao = b.nr_prescricao
		union 
		select 	1
		from 	prescr_medica x
		where 	x.nr_cirurgia is not null 
		and 	nvl(x.ie_tipo_prescr_cirur,0) <> 2
		and 	x.nr_prescricao = b.nr_prescricao)
	union
	select distinct a.nr_prescricao
	from 	Prescr_medica b,
		prescr_Procedimento a
	where a.cd_motivo_baixa in (0, cd_motivo_baixa_p)
        and a.nr_prescricao = b.nr_prescricao
	  and b.dt_prescricao between dt_inicial_w and dt_final_w
	  and b.dt_liberacao is null
	  and b.dt_liberacao_medico is null
	  and not exists
		(select	1
		from 	cirurgia c
		where 	c.nr_prescricao = b.nr_prescricao
		union 
		select 	1
		from 	prescr_medica x
		where 	x.nr_cirurgia is not null 
		and 	nvl(x.ie_tipo_prescr_cirur,0) <> 2
		and 	x.nr_prescricao = b.nr_prescricao)
	Order by 1;

Cursor c020 is
select	a.nr_prescricao
from	prescr_medica a
where	a.dt_prescricao between dt_inicial_w and dt_final_w
and	a.dt_liberacao is null
and	a.dt_liberacao_medico is null
and 	not exists
	(select	1
	from 	cirurgia c
	where 	c.nr_prescricao = a.nr_prescricao
	union 
	select 	1
	from 	prescr_medica x
	where 	x.nr_cirurgia is not null 
	and 	nvl(x.ie_tipo_prescr_cirur,0) <> 2
	and 	x.nr_prescricao = a.nr_prescricao)
and not exists (
	select	1
	from	prescr_material c
	where	c.nr_prescricao = a.nr_prescricao
	and 	rownum <=1)
and not exists (
	select	1
	from	prescr_procedimento d
	where	d.nr_prescricao = a.nr_prescricao
	and 	rownum <=1);
		
BEGIN

dt_final_w	:= (dt_parametro_p - qt_hora_adicional_p / 24);
dt_inicial_w	:= dt_final_w - 7;

OPEN C010;
LOOP
FETCH C010 into	
	nr_prescricao_w;
EXIT WHEN C010%NOTFOUND;
	BEGIN
	
	begin
	delete from prescr_material 
	where nr_prescricao	= nr_prescricao_w
	  and cd_motivo_baixa 	in (0, 10);
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;			
	end;
	
	begin
	delete from prescr_procedimento 
	where nr_prescricao	= nr_prescricao_w
	  and cd_motivo_baixa 	in (0, cd_motivo_baixa_p);
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;			
	end;
	
	begin
	delete from prescr_medica 
	where nr_prescricao	= nr_prescricao_w;
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;
	end;
	begin
	gravar_log_exclusao('PRESCR_MEDICA','JOB_PNL','NR_PRESCRICAO : ' || nr_prescricao_w,'N');
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;
	end;
	END;
END LOOP;
close c010;

commit;

open C020;
loop
fetch C020 into	
	nr_prescricao_w;
exit when C020%notfound;
	begin
	begin
	delete from prescr_medica 
	where nr_prescricao	= nr_prescricao_w;
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;
	end;
	begin
	gravar_log_exclusao('PRESCR_MEDICA','JOB_PNL2','NR_PRESCRICAO : ' || nr_prescricao_w,'N');
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;
	end;
	end;
end loop;
close C020;

END Excluir_Prescricao_Nao_Lib; 
/
