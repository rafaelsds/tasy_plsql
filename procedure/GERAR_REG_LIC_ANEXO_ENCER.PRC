create or replace
procedure gerar_reg_lic_anexo_encer(	nr_seq_licitacao_p			number,
				ds_titulo_p			varchar2,
				ds_arquivo_p			varchar2,
				nr_seq_tipo_anexo_p		number,
				nm_usuario_p			varchar2) is 

nr_Sequencia_w		number(10);				
				
begin

select	nvl(max(nr_sequencia),0)
into	nr_Sequencia_w
from	reg_lic_anexo_encer
where	nr_seq_licitacao	= nr_seq_licitacao_p
and	ds_titulo		= ds_titulo_p;

if	(nr_Sequencia_w = 0) then

	insert into reg_lic_anexo_encer(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_licitacao,
		ds_titulo,
		ds_arquivo,
		nr_seq_tipo_anexo,
		ie_anexa_zip)
	values(	reg_lic_anexo_encer_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_licitacao_p,
		substr(ds_titulo_p,1,255),
		substr(ds_arquivo_p,1,255),
		nr_seq_tipo_anexo_p,
		'S');
else
	update	reg_lic_anexo_encer
	set	ie_anexa_zip = 'S'
	where	nr_sequencia = nr_Sequencia_w;
end if;

commit;

end gerar_reg_lic_anexo_encer;
/