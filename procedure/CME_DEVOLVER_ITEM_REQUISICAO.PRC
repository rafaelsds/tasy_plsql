create or replace
procedure CME_DEVOLVER_ITEM_REQUISICAO(	nr_requisicao_p		number,
						nr_seq_item_req_p		number,
						nr_seq_item_atend_p	number,
						ie_baixa_item_p		varchar,
						nm_usuario_p		varchar2) is

nr_seq_conjunto_w		number(10,0);
qt_conj_item_baixado_w		number(05,0);
qt_conj_nao_atendido_w		number(05,0);
nr_seq_item_req_destino_w	number(10,0);
qt_conj_atual_w			number(15,0);
nr_seq_conj_real_w		number(10,0);
ie_status_conj_devol_req_w	number(01,0);
cd_estabelecimento_w		number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;
ie_manter_conj_atend_w		varchar2(1);
cd_motivo_baixa_w		number(3);
ie_desvincular_cirurgia_w	varchar2(1);

BEGIN

Obter_Param_Usuario(410, 11, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w,ie_status_conj_devol_req_w);
Obter_Param_Usuario(410, 26, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w,ie_manter_conj_atend_w);
Obter_Param_Usuario(406, 159, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w,ie_desvincular_cirurgia_w);

select	max(nr_seq_conjunto)
into	nr_seq_conjunto_w
from	cm_requisicao_item
where	nr_sequencia 		= nr_seq_item_req_p;

select	max(nr_seq_conj_real)
into	nr_seq_conj_real_w
from	cm_requisicao_conj
where	nr_sequencia 		= nr_seq_item_atend_p
and	nr_seq_item_req 		= nr_seq_item_req_p;

update	cm_conjunto_cont
set	cd_setor_atend_dest	= null,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia 		= nr_seq_conj_real_w;

update	cm_requisicao_item
set		dt_devolucao 		= sysdate
where	nr_sequencia 		= nr_seq_item_req_p
and		nr_seq_requisicao 	= nr_requisicao_p
and		nr_seq_conjunto		= nr_seq_conjunto_w;


if	(ie_status_conj_devol_req_w > 0) then
	update	cm_conjunto_cont
	set	ie_status_conjunto	= ie_status_conj_devol_req_w,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia 	= nr_seq_conj_real_w;
end if;

if	(nvl(ie_desvincular_cirurgia_w,'N') = 'S') then
	update	cm_conjunto_cont
	set	nr_cirurgia		= null,
		nr_atendimento		= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia 		= nr_seq_conj_real_w;
end if;	

update	cm_requisicao_conj
set	dt_retorno              = sysdate,
        nm_usuario_retorno	= nm_usuario_p
where	nr_sequencia		= nr_seq_item_atend_p
and	nr_seq_item_req 	= nr_seq_item_req_p;

if	(ie_manter_conj_atend_w = 'N') then
	begin

	update	cm_requisicao
	set	dt_atualizacao 		= sysdate,
		dt_baixa 			= null,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia 		= nr_requisicao_p
	and	dt_baixa is not null;

	end;
end if;

select	count(*)
into	qt_conj_item_baixado_w
from	cm_requisicao_conj
where	nr_seq_item_req = nr_seq_item_req_p;

if	(qt_conj_item_baixado_w = 0) and
	(ie_manter_conj_atend_w = 'N') then

	update	cm_requisicao_item
	set	dt_atualizacao		= sysdate,
		cd_motivo_baixa		= null,
		nm_usuario		= nm_usuario_p,
		nm_usuario_atend = null
	where	nr_sequencia 		= nr_seq_item_req_p
	and	nr_seq_requisicao 		= nr_requisicao_p
	and	nr_seq_conjunto		= nr_seq_conjunto_w;
	
	
	select	count(*),
		nvl(max(nr_sequencia),0)
	into	qt_conj_nao_atendido_w,
		nr_seq_item_req_destino_w
	from	cm_requisicao_item
	where	nr_sequencia <> nr_seq_item_req_p
	and	cd_motivo_baixa is null
	and	nr_seq_conjunto 		= nr_seq_conjunto_w
	and	nr_seq_requisicao 		= nr_requisicao_p;

	if	(qt_conj_nao_atendido_w > 0) and
		(nr_seq_item_req_destino_w > 0) then

		select	nvl(qt_conjunto,0) 
		into	qt_conj_atual_w
		from	cm_requisicao_item 
		where	nr_sequencia 	= nr_seq_item_req_p;

		update	cm_requisicao_item
		set	qt_conjunto 	= qt_conjunto + qt_conj_atual_w
		where	nr_sequencia 	= nr_seq_item_req_destino_w
		and	nr_seq_conjunto	= nr_seq_conjunto_w
		and	nr_seq_requisicao 	= nr_requisicao_p;
		
		delete from cm_requisicao_item
		where	nr_sequencia 	= nr_seq_item_req_p
		and	nr_seq_conjunto	= nr_seq_conjunto_w;
	end if;
	
	if	(ie_baixa_item_p = 'S') then
		
		cd_motivo_baixa_w := nvl(obter_valor_param_usuario(406, 155, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),0);
		
		update	cm_requisicao_item
		set	cd_motivo_baixa = cd_motivo_baixa_w
		where	nr_sequencia = nr_seq_item_req_p
		and	nr_seq_requisicao = nr_requisicao_p
		and	nr_seq_conjunto = nr_seq_conjunto_w;
	end if;

elsif	(qt_conj_item_baixado_w > 0) and
	(ie_manter_conj_atend_w = 'N') then
	
	begin

	update	cm_requisicao_item
	set	qt_conjunto		= qt_conjunto - 1
	where	nr_sequencia 		= nr_seq_item_req_p
	and	nr_seq_requisicao		= nr_requisicao_p
	and	nr_seq_conjunto		= nr_seq_conjunto_w;
	

	select	cm_requisicao_item_seq.nextval
	into	nr_seq_item_req_destino_w
	from	dual;

	insert into cm_requisicao_item(
			nr_sequencia,
			nr_seq_requisicao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_conjunto,
			qt_conjunto)
		values(nr_seq_item_req_destino_w,
			nr_requisicao_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_conjunto_w,
			1);
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_item_req_destino_w
	from	cm_requisicao_item
	where	nr_seq_conjunto 	= nr_seq_conjunto_w
	and	cd_motivo_baixa is null
	and	nr_sequencia <> nr_seq_item_req_p
	and	nr_seq_conjunto	= nr_seq_conjunto_w
	and	nr_seq_requisicao = nr_requisicao_p;

	select	nvl(sum(qt_conjunto),0)
	into	qt_conj_nao_atendido_w
	from	cm_requisicao_item
	where	nr_seq_conjunto = nr_seq_conjunto_w
	and	cd_motivo_baixa is null
	and	nr_sequencia <> nr_seq_item_req_p 
	and	nr_sequencia <> nr_seq_item_req_destino_w
	and	nr_seq_requisicao = nr_requisicao_p;

	if	(qt_conj_nao_atendido_w > 0) and
		(nr_seq_item_req_destino_w > 0) then

		update	cm_requisicao_item
		set	qt_conjunto 		= (qt_conj_nao_atendido_w + 1)
		where	nr_sequencia 		= nr_seq_item_req_destino_w
		and	nr_seq_conjunto		= nr_seq_conjunto_w;
		
	end if;

	delete from cm_requisicao_item
	where	nr_sequencia 	<>  nr_seq_item_req_destino_w
	and	cd_motivo_baixa is null
	and	nr_seq_conjunto	= nr_seq_conjunto_w
	and	nr_seq_requisicao	= nr_requisicao_p;

	end;
end if;

commit;

END CME_DEVOLVER_ITEM_REQUISICAO;
/