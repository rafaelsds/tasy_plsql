create or replace
procedure atualizar_motivo_inativ_doc(	nr_seq_motivo_p	number,
					nr_seq_doc_p	number,
					nm_usuario_p	varchar2) is 

begin

if	(nr_seq_motivo_p is not null) and
	(nr_seq_doc_p is not null) then

	update	qua_documento
	set	nr_seq_motivo_inativacao	= nr_seq_motivo_p
	where	nr_sequencia			= nr_seq_doc_p;

	commit;

end if;

end atualizar_motivo_inativ_doc;
/