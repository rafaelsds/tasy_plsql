create or replace
procedure pls_alterar_qt_autorizada(
			nr_sequencia_p		number,
			qt_autorizada_p		number,
			ie_opcao_p		varchar2,
			nm_usuario_p		varchar2) is

/* ie_opcao_p:
M = materiais
P  = procedimentos
*/			
			
begin

if	(qt_autorizada_p is not null) and	
	(nr_sequencia_p is not null) then
	begin
	
	if	(ie_opcao_p = 'P') then
		update	pls_guia_plano_proc
		set	qt_autorizada	= qt_autorizada_p,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_p;
	elsif	(ie_opcao_p = 'M') then
		update	pls_guia_plano_mat
		set	qt_autorizada	= qt_autorizada_p,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_p;
	end if;
	
	commit;
	
	end;
end if;

end pls_alterar_qt_autorizada;
/