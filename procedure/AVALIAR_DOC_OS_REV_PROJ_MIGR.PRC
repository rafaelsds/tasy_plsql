create or replace
procedure avaliar_doc_os_rev_proj_migr (
		nr_seq_os_p		number,
		ie_avaliacao_p		varchar2,
		ie_analise_p		varchar2,
		ds_analise_p		varchar2,
		ds_pontos_positivos_p	varchar2,
		ds_pontos_negativos_p	varchar2,
		nm_usuario_p		varchar2) is
		
nr_seq_documentacao_w	number(10,0);
		
begin
if	(nr_seq_os_p is not null) and
	(ie_avaliacao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_documentacao_w
	from	w_migracao_ordem_servico
	where	nr_seq_os_migracao = nr_seq_os_p;
	
	if	(nr_seq_documentacao_w > 0) then
		begin
		update	w_migracao_ordem_servico
		set	ie_avaliacao = ie_avaliacao_p,
			ie_analise_avaliacao = ie_analise_p,
			ds_analise_avaliacao = ds_analise_p,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p,
			dt_avaliacao = sysdate,
			ds_pontos_positivos = ds_pontos_positivos_p,
			ds_pontos_negativos = ds_pontos_negativos_p
		where	nr_sequencia = nr_seq_documentacao_w;
		end;
	end if;
	end;
end if;
commit;
end avaliar_doc_os_rev_proj_migr;
/