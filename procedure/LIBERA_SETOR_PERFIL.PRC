create or replace
procedure libera_setor_perfil(cd_setor_p number,cd_perfil_p number,nm_usuario_atual_p varchar2) is 

nm_usuario_w	varchar2(15);

Cursor C01 is
	select	a.nm_usuario
	from	usuario_perfil a
	where	a.cd_perfil	= cd_perfil_p
	and		not exists (select 	1
						from	usuario_setor b
						where	a.nm_usuario = b.nm_usuario_param
						and		b.cd_setor_atendimento = cd_setor_p);

begin

open C01;
loop
fetch C01 into	
	nm_usuario_w;
exit when C01%notfound;
	begin
	insert into	usuario_setor (
		nm_usuario_param,
		cd_setor_atendimento,
		dt_atualizacao,
		nm_usuario)
	values	(
		nm_usuario_w,
		cd_setor_p,
		sysdate,
		nm_usuario_atual_p);	
	end;
end loop;
close C01;

commit;
end libera_setor_perfil;
/