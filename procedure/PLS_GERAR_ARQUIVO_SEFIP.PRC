create or replace
procedure pls_gerar_arquivo_sefip
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

ds_conteudo_w			varchar2(4000);
ie_tipo_registro_w		varchar2(2);
ds_brancos_51_w			varchar2(51);
ds_brancos_18_w			varchar2(18);
ie_final_linha			varchar2(1)	:= '*';
ie_tipo_remessa_w		varchar2(1)	:= '1';
ie_tipo_inscr_w			varchar2(1)	:= '1';
ie_tipo_inscr_resp_w		varchar2(1)	:= '1';
cd_inscr_resp_w			varchar2(14);
ds_razao_social_w		varchar2(40);
ie_competencia_w		varchar2(2);
dt_competencia_w		varchar2(6);
ie_ind_rec_fgts_w		varchar2(1)	:= ' ';
ie_mod_arquivo_w		varchar2(1)	:= '1';
dt_recolhimento_w		varchar2(8);
ie_ind_rec_prev_w		varchar2(1)	:= '1';
dt_rec_prev_soc_w		varchar2(8);
ie_ind_rec_at_prev_w		varchar2(7);
nm_pessoa_contato_w		varchar2(20);
ds_logradouro_w			varchar2(50);
ds_bairro_w			varchar2(20);
cd_cep_w			varchar2(8);
sg_uf_w				varchar2(2);
nr_telefone_w			varchar2(12);
ds_email_w			varchar2(60);
ds_zeros_36_w			varchar2(36);
ds_brancos_4_w			varchar2(4);
ds_cidade_w			varchar2(20);
cd_cnae_w			varchar2(7);
cd_centralizacao_w		varchar2(1)	:= '0';
ie_simples_w			varchar2(1)	:= '1';
ds_zeros_21_w			varchar2(21);
nr_seq_tomador_w		number(10);
nr_seq_prestador_w		number(10);
cd_cgc_estip_w			varchar2(14);
cd_inscr_resp_estip_w		varchar2(14);
ds_razao_social_estip_w		varchar2(40);
ds_logradouro_estip_w		varchar2(50);
ds_bairro_estip_w		varchar2(20);
cd_cep_estip_w			varchar2(8);
sg_uf_estip_w			varchar2(2);
ds_cidade_estip_w		varchar2(20);
ds_zeros_45_w			varchar2(45);
ds_brancos_42_w			varchar2(42);
ds_brancos_98_w			varchar2(98);
ds_brancos_306_w		varchar2(306);
ds_marca_51_w			varchar2(51);
ie_fpas_w			varchar2(3)	:= '515';
cd_outra_entidade_w		varchar2(4)	:= '0000';
cd_pagamento_gps_w		varchar2(4);
pr_isencao_filan_w		varchar2(5)	:= '00000';
vl_salario_familia_w		varchar2(15);
vl_salario_maternidade_w	varchar2(15);
vl_contr_desc_emp_13_w		varchar2(15);
ie_ind_neg_pos_w		varchar2(1)	:= '0';
vl_desc_prev_13_w		varchar2(14)	:= '00000000000000';
cd_banco_w			varchar2(3)	:= '   ';
cd_agencia_w			varchar2(4)	:= '    ';
cd_conta_w			varchar2(9)	:= '         ';
cd_pag_gps_estip_w		varchar2(4)	:= '    ';
vl_salario_fam_estip_w		varchar2(15)	:= '000000000000000';
vl_contr_desc_emp_estip_13_w	varchar2(15)	:= '000000000000000';
ie_ind_neg_pos_w_estip_w	varchar2(1)	:= '0';
vl_dev_prev_13_estip_w		varchar2(14)	:= '00000000000000';
vl_retencao_estip_w		varchar2(15);
vl_faturas_estip_w		varchar2(15);
cd_pessoa_fisica_w		varchar2(10);
nr_pis_pasep_w			varchar2(11);
dt_admissao_w			varchar2(8);
cd_categoria_trabalhador_w	varchar2(2);
nm_pessoa_fisica_w		varchar2(70);
cd_funcionario_w		varchar2(11);
nr_ctps_w			varchar2(7);
nr_serie_ctps_w			varchar2(5);
dt_opcao_w			varchar2(8)	:= '        ';
dt_nascimento_w			varchar2(8);
vl_remuneracao_sem_13_w		varchar2(15);
cd_classe_contrib_w		varchar2(2)	:= '  ';
vl_remuneracao_w		varchar2(15);
vl_remuneracao_13_w		varchar2(15);
vl_desconto_w			varchar2(15);
cd_ocorrencia_w			varchar2(2)	:= '  ';	
vl_base_13_w			varchar2(15);
vl_base_13_mov_w		varchar2(15);
dt_lote_w			date;
cd_pessoa_resp_w		varchar2(10);
cd_pessoa_resp_lote_w           sefip_lote.cd_pessoa_resp%type;
nm_resp_w			varchar2(20);
nr_cpf_w			varchar2(14);
cd_cbo_saude_w			varchar2(4000);
cd_cgc_outorgante_w		varchar2(14);
cd_representante_w		varchar2(10);
pr_imposto_w			number(10)	:= 20;
ie_empreendedor_individual_w	varchar2(15);
nr_seq_resp_cont_w    		number(10);
cd_pessoa_fisica_resp_cont_w 	varchar2(10);
cd_cgc_resp_cont_w           	varchar2(14);
ie_responsavel_contab_w		varchar2(15);

Cursor C01 is
	select	s.nr_sequencia,
		s.cd_cgc,
		lpad(replace(to_char(nvl(s.vl_faturado,0), 'fm00000000000.00'),'.',''),15,'0'),
		lpad(replace(to_char(nvl(s.vl_retencao,0), 'fm00000000000.00'),'.',''),15,'0'),
		decode(nvl(j.ie_empreendedor_individual,'N'),'S','2','1') ie_tipo_inscr
	from	sefip_tomador s,
		pessoa_juridica j
	where	s.cd_cgc	= j.cd_cgc(+)
	and	nr_seq_lote	= nr_seq_lote_p
	order by ie_tipo_inscr,
		cd_cgc;
	
Cursor C02 is
	select	cd_pessoa_fisica,
		nr_pis_pasep,
		decode(cd_cbo_saude,'09999','02231',cd_cbo_saude) cd_cbo_saude,
		lpad(replace(to_char(sum(vl_remuneracao), 'fm00000000000.00'),'.',''),15,'0'),
		lpad(replace(to_char(sum(vl_remuneracao_13), 'fm00000000000.00'),'.',''),15,'0'),
		lpad(replace(to_char(sum(vl_descontado), 'fm00000000000.00'),'.',''),15,'0'),
		lpad(replace(to_char(sum(vl_base_13_movimento), 'fm00000000000.00'),'.',''),15,'0'),
		lpad(replace(to_char(sum(vl_base_13), 'fm00000000000.00'),'.',''),15,'0'),
		cd_ocorrencia
	from	(select	a.nr_seq_tomador,
			b.cd_pessoa_fisica,	
			lpad(nvl(c.nr_pis_pasep,0),11,'0') nr_pis_pasep,
			lpad(substr(obter_codigo_cbo_saude(pls_obter_cbo_medico(c.cd_pessoa_fisica)),1,4),5,'0') cd_cbo_saude,
			nvl(sum(vl_remuneracao),0) vl_remuneracao,
			nvl(sum(vl_remuneracao_13),0) vl_remuneracao_13,
			decode(nvl(sum(vl_descontado),0),abs(nvl(sum(vl_descontado),0)),nvl(sum(vl_descontado),0),0) vl_descontado,
			/*case when
			vl_descontado < 0 then
			0
			else
			vl_descontado
			end
			),0) vl_descontado,*/
			nvl(sum(vl_base_13_movimento),0) vl_base_13_movimento,
			nvl(sum(vl_base_13),0) vl_base_13,
			nvl(max(b.cd_ocorrencia_sefip),'  ')  cd_ocorrencia
		from	pessoa_fisica		c,
			pls_prestador		b,
			sefip_tomador_movimento a
		where	b.nr_sequencia 		= a.nr_seq_prestador
		and	b.cd_pessoa_fisica	= c.cd_pessoa_fisica
		and	a.nr_seq_tomador	= nr_seq_tomador_w
		--and	a.vl_descontado >= 0
		group by
			a.nr_seq_tomador,
			c.nr_pis_pasep,
			b.cd_pessoa_fisica,
			c.nr_seq_cbo_saude,
			c.cd_pessoa_fisica)
	having	sum(vl_remuneracao) > 0
	group by
		nr_seq_tomador,
		nr_pis_pasep,
		cd_pessoa_fisica,
		cd_cbo_saude,
		cd_ocorrencia
	order by
		nr_pis_pasep;

begin
delete from w_interf_sefip where nm_usuario = nm_usuario_p;

select	trunc(dt_lote,'mm'),
	nvl(cd_pagamento_gps,'    '),
	cd_pessoa_resp,
	nvl(ie_responsavel_contab, 'T') 
into	dt_lote_w,
	cd_pagamento_gps_w,
	cd_pessoa_resp_lote_w,
	ie_responsavel_contab_w
from	sefip_lote
where	nr_sequencia	= nr_seq_lote_p;

/*Registro - 00*/
ie_tipo_registro_w	:= '00';
ds_brancos_51_w		:= lpad(' ',51,' ');
ds_brancos_18_w		:= lpad(' ',18,' ');
ds_zeros_36_w		:= lpad('0',36,'0');
ds_brancos_4_w		:= lpad(' ',4,' ');
ds_zeros_45_w		:= lpad('0',45,'0');

select	lpad(nvl(substr(obter_dados_cnae(b.nr_seq_cnae,'CD'),1,7),'0'),7,'0')
into	cd_cnae_w
from	pessoa_juridica	b,
	pls_outorgante	a
where	a.cd_cgc_outorgante	= b.cd_cgc
and	cd_estabelecimento	= cd_estabelecimento_p;

if(ie_responsavel_contab_w = 'T') then
	select 	a.cd_cgc_outorgante,
		rpad(nvl(substr(replace(elimina_caractere_especial(b.ds_razao_social),'  ',' '),1,40),' '),40,' '),
		rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'CTT'),1,20),' '),20,' '),
		rpad(nvl(substr(replace(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'E')),':',''),1,50),' '),50,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'B')),1,20),' '),20,' '),
		rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'CEP'),1,8),' '),8,' '),
		rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'UF'),1,2),' '),2,' '),
		lpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'DDT')||obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'T')),1,12),' '),12,'0'),
		rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'M'),1,60),' '),60,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'CI')),1,20),' '),20,' '),
		cd_pf_resp_tecnico,
		decode(nvl(b.ie_empreendedor_individual,'N'),'S','2','1') ie_tipo_inscr
	into	cd_inscr_resp_w,
		ds_razao_social_w,
		nm_pessoa_contato_w,
		ds_logradouro_w,
		ds_bairro_w,
		cd_cep_w,
		sg_uf_w,
		nr_telefone_w,
		ds_email_w,
		ds_cidade_w,
		cd_pessoa_resp_w,
		ie_tipo_inscr_resp_w
	from	pessoa_juridica	b,
		pls_outorgante	a
	where	a.cd_cgc_outorgante	= b.cd_cgc
	and	cd_estabelecimento	= cd_estabelecimento_p;

elsif(ie_responsavel_contab_w = 'C') then

	select	nr_sequencia,
		nvl(cd_pessoa_fisica, 'X'),
		nvl(cd_cgc, 'X')
	into	nr_seq_resp_cont_w,
		cd_pessoa_fisica_resp_cont_w,
		cd_cgc_resp_cont_w
	from	pls_responsavel_contab
	where   nr_sequencia in	(select max(nr_sequencia)
				from 	pls_responsavel_contab
				where	sysdate between dt_vigencia_inicial and nvl(dt_vigencia_final,sysdate));
  
	if (cd_cgc_resp_cont_w <> 'X') then
		
		select	max(nvl(b.ie_empreendedor_individual,'N'))
		into	ie_empreendedor_individual_w
		from	pessoa_juridica b
		where	cd_cgc = cd_cgc_resp_cont_w;
		
	end if;
  
	if(cd_pessoa_fisica_resp_cont_w <> 'X') then
		ie_tipo_inscr_resp_w := 3;
		cd_inscr_resp_w := obter_dados_pf(cd_pessoa_fisica_resp_cont_w, 'CPF');

	elsif(ie_empreendedor_individual_w = 'S' and cd_cgc_resp_cont_w <> 'X') then
		ie_tipo_inscr_resp_w := 2;
		select  max(rpad(nvl(nr_cei,' '),14,' '))
		into    cd_inscr_resp_w
		from    pessoa_juridica
		where   cd_cgc = cd_cgc_resp_cont_w;
    
	elsif(ie_empreendedor_individual_w = 'N' and cd_cgc_resp_cont_w <> 'X') then
		ie_tipo_inscr_resp_w := 1;
		cd_inscr_resp_w := cd_cgc_resp_cont_w;
    
	end if;  
  
	cd_inscr_resp_w := rpad(cd_inscr_resp_w,14,' ');
  
	select  rpad(nvl(substr(replace(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'N'),'  ',' '),1,40),' '),40,' '),
		rpad(nvl(nvl(substr(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'CTT'),1,20),substr(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'N'),1,20)),' '),20,' '),
		rpad(nvl(substr(replace(elimina_caractere_especial(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'E')),':',''),1,50),' '),50,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'B')),1,20),' '),20,' '),
		rpad(nvl(substr(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'CEP'),1,8),' '),8,' '),
		rpad(nvl(substr(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'UF'),1,2),' '),2,' '),
		lpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'DDT')||obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'T')),1,12),' '),12,'0'),
		rpad(nvl(substr(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'M'),1,60),' '),60,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(a.CD_PESSOA_FISICA, a.CD_CGC, 'CI')),1,20),' '),20,' ')
	into    ds_razao_social_w,
		nm_pessoa_contato_w,
		ds_logradouro_w,
		ds_bairro_w,
		cd_cep_w,
		sg_uf_w,
		nr_telefone_w,
		ds_email_w,
		ds_cidade_w
	from    pls_responsavel_contab a
	where   a.nr_sequencia = nr_seq_resp_cont_w;  
	
	nm_resp_w := nm_pessoa_contato_w;

end if;

if (nvl(cd_pessoa_resp_lote_w, 'X') <> 'X' ) then
	cd_pessoa_resp_w	:= cd_pessoa_resp_lote_w;
end if;

select	rpad(substr(nvl(max(nm_pessoa_fisica),ds_razao_social_w),1,20),20,' ')
into	nm_resp_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_resp_w;

select	nvl(max(ie_competencia),'1')
into	ie_competencia_w
from	sefip_lote
where	nr_sequencia	= nr_seq_lote_p;

select	max(to_char(dt_mes,'yyyymm'))
into	dt_competencia_w
from	mes_v
where	to_char(dt_mes,'mm') = lpad(ie_competencia_w,2,'0')
and	dt_mes >= trunc(dt_lote_w,'yyyy');

select	rpad(' ',8,' ')
into	dt_recolhimento_w
from	dual;

select	rpad(' ',8,' ')
into	dt_rec_prev_soc_w
from	dual;

select	rpad(' ',7,' ')
into	ie_ind_rec_at_prev_w
from	dual;

ds_conteudo_w	:=	ie_tipo_registro_w ||
			ds_brancos_51_w ||
			ie_tipo_remessa_w  ||
			ie_tipo_inscr_resp_w  ||
			cd_inscr_resp_w  ||
			substr(ds_razao_social_w,1,30)  ||
			nm_resp_w ||
			ds_logradouro_w	 ||
			ds_bairro_w  ||
			cd_cep_w  ||
			ds_cidade_w  ||
			sg_uf_w  ||
			nr_telefone_w  ||
			ds_email_w  ||
			dt_competencia_w ||
			'211' ||
			ie_ind_rec_fgts_w  ||
			ie_mod_arquivo_w ||
			dt_recolhimento_w  ||
			ie_ind_rec_prev_w  ||
			dt_rec_prev_soc_w  ||
			ie_ind_rec_at_prev_w  ||
			ie_tipo_inscr_resp_w  ||
			cd_inscr_resp_w  ||
			ds_brancos_18_w  ||
			ie_final_linha;
    
insert into w_interf_sefip
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ds_conteudo)
values	(w_interf_sefip_seq.nextval,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	sysdate,
	ds_conteudo_w);
/*Registro - 00 Fim*/
				
/*Registro - 10*/					
ie_tipo_registro_w	:= '10';

select 	a.cd_cgc_outorgante,
	rpad(nvl(substr(replace(elimina_caractere_especial(b.ds_razao_social),'  ',' '),1,40),' '),40,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'CTT'),1,20),' '),20,' '),
	rpad(nvl(substr(replace(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'E')),':',''),1,50),' '),50,' '),
	rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'B')),1,20),' '),20,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'CEP'),1,8),' '),8,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'UF'),1,2),' '),2,' '),
	lpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'DDT')||obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'T')),1,12),' '),12,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'M'),1,60),' '),60,' '),
	rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, a.cd_cgc_outorgante, 'CI')),1,20),' '),20,' '),
	cd_pf_resp_tecnico,
	decode(nvl(b.ie_empreendedor_individual,'N'),'S','2','1') ie_tipo_inscr
into	cd_inscr_resp_w,
	ds_razao_social_w,
	nm_pessoa_contato_w,
	ds_logradouro_w,
	ds_bairro_w,
	cd_cep_w,
	sg_uf_w,
	nr_telefone_w,
	ds_email_w,
	ds_cidade_w,
	cd_pessoa_resp_w,
	ie_tipo_inscr_resp_w
from	pessoa_juridica	b,
	pls_outorgante	a
where	a.cd_cgc_outorgante	= b.cd_cgc
and	cd_estabelecimento	= cd_estabelecimento_p;

select	lpad('0',15,'0'),
	lpad('0',15,'0'),
	lpad('0',15,'0')
into	vl_salario_familia_w,
	vl_salario_maternidade_w,
	vl_contr_desc_emp_13_w
from	dual;

ds_conteudo_w	:=	ie_tipo_registro_w  ||
			ie_tipo_inscr_resp_w  ||
			cd_inscr_resp_w ||
			lpad('0',36,'0') ||
			ds_razao_social_w  ||
			ds_logradouro_w ||
			ds_bairro_w  ||
			cd_cep_w  ||
			ds_cidade_w  ||
			sg_uf_w  ||
			nr_telefone_w  ||
			'N'  ||
			cd_cnae_w  ||
			'N'  ||
			'00' ||
			cd_centralizacao_w ||
			ie_simples_w ||
			ie_fpas_w ||
			cd_outra_entidade_w ||
			cd_pagamento_gps_w ||
			pr_isencao_filan_w ||
			vl_salario_familia_w ||
			vl_salario_maternidade_w ||
			vl_contr_desc_emp_13_w ||
			ie_ind_neg_pos_w ||
			vl_desc_prev_13_w ||
			cd_banco_w ||
			cd_agencia_w ||
			cd_conta_w ||
			ds_zeros_45_w ||
			ds_brancos_4_w ||
			ie_final_linha;

insert into w_interf_sefip
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ds_conteudo)
values	(w_interf_sefip_seq.nextval,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	sysdate,
	ds_conteudo_w);
/*Registro - 10 Fim*/

/*Registro - 20*/ 
open C01;
loop
fetch C01 into	
	nr_seq_tomador_w,
	cd_cgc_estip_w,
	vl_faturas_estip_w,
	vl_retencao_estip_w,
	ie_tipo_inscr_w;
exit when C01%notfound;
	begin
	ie_tipo_registro_w	:= '20';
	ds_zeros_21_w		:= lpad('0',21,'0');
	ds_brancos_42_w		:= lpad(' ',42,' ');
	
	select	cd_cgc,
		rpad(nvl(substr(replace(elimina_caractere_especial(ds_razao_social),'  ', ' '),1,40),' '),40,' '),
		rpad(nvl(substr(replace(replace(elimina_caractere_especial(obter_dados_pf_pj(null, cd_cgc, 'E')),'  ', ' '),':',''),1,50),' '),50,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, cd_cgc, 'B')),1,20),' '),20,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, cd_cgc, 'CEP')),1,8),' '),8,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, cd_cgc, 'UF')),1,2),' '),2,' '),
		rpad(nvl(substr(elimina_caractere_especial(obter_dados_pf_pj(null, cd_cgc, 'CI')),1,20),' '),20,' '),
		decode(nvl(ie_empreendedor_individual,'N'),'S','2','1') ie_tipo_inscr
	into	cd_inscr_resp_estip_w,
		ds_razao_social_estip_w,
		ds_logradouro_estip_w,
		ds_bairro_estip_w,
		cd_cep_estip_w,
		sg_uf_estip_w,
		ds_cidade_estip_w,
		ie_tipo_inscr_w
	from	pessoa_juridica	
	where	cd_cgc	= cd_cgc_estip_w;
	
	ds_logradouro_estip_w := rpad(replace(replace(ds_logradouro_estip_w,'(',''),')',''),50,' ');
	
	select	max(cd_cgc_outorgante),
		max(cd_representante)
	into	cd_cgc_outorgante_w,
		cd_representante_w
	from	pls_outorgante
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	ds_conteudo_w	:=	ie_tipo_registro_w ||
				ie_tipo_inscr_resp_w  ||
				cd_inscr_resp_w ||
				ie_tipo_inscr_w  ||
				lpad(cd_cgc_estip_w,14,'0') || 
				ds_zeros_21_w  ||
				ds_razao_social_estip_w  ||
				ds_logradouro_estip_w  ||
				ds_bairro_estip_w  ||
				cd_cep_estip_w  ||
				ds_cidade_estip_w  ||
				sg_uf_estip_w ||
				cd_pag_gps_estip_w ||
				vl_salario_fam_estip_w ||
				vl_contr_desc_emp_estip_13_w ||
				ie_ind_neg_pos_w_estip_w ||
				vl_dev_prev_13_estip_w ||
				vl_retencao_estip_w ||
				vl_faturas_estip_w ||
				ds_zeros_45_w ||
				ds_brancos_42_w  ||
				ie_final_linha;

	insert into w_interf_sefip
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ds_conteudo)
	values	(w_interf_sefip_seq.nextval,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		ds_conteudo_w);

	/* Registro - 30 */
	open C02;
	loop
	fetch C02 into	
		cd_pessoa_fisica_w,
		nr_pis_pasep_w,
		cd_cbo_saude_w,
		vl_remuneracao_w,
		vl_remuneracao_13_w,
		vl_desconto_w,
		vl_base_13_mov_w,
		vl_base_13_w,
		cd_ocorrencia_w;
	exit when C02%notfound;
		begin
		ie_tipo_registro_w	:= '30';
		ds_brancos_98_w		:= lpad(' ',98,' ');

		/*select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	pls_prestador
		where	nr_sequencia	= nr_seq_prestador_w;*/
		cd_categoria_trabalhador_w := '17';
		
		if (pr_imposto_w = 20) then
			cd_categoria_trabalhador_w := '24';
		end if;
		
		select	nvl(to_char(dt_primeira_admissao,'ddmmyyyy'),null),
			rpad(elimina_caractere_especial(nm_pessoa_fisica),70,' '),
			lpad(nvl(cd_funcionario,' '),11,' '),
			lpad(decode(nvl(to_number(substr(nr_ctps,1,7)),0),0,' ',to_number(substr(nr_ctps,1,7))),7,' '),
			lpad(decode(nvl(nr_serie_ctps,0),0,' ',nr_serie_ctps),5,' '),
			lpad(decode(nvl(dt_nascimento,null),null,' ',to_char(dt_nascimento,'ddmmyyyy')),8,' '),			
			lpad(decode(nr_cpf,null,'0',nr_cpf),14,'0')
		into	dt_admissao_w,
			nm_pessoa_fisica_w,
			cd_funcionario_w,
			nr_ctps_w,
			nr_serie_ctps_w,
			dt_nascimento_w,
			nr_cpf_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		if	(cd_categoria_trabalhador_w not in ('01', '02', '03', '04', '05', '06', '07', '12', '19', '20', '21', '26')) then
			dt_nascimento_w	:= '        ';
		end if;
		
		if	(cd_categoria_trabalhador_w not in ('01','02','03','04','06','07','26')) then
			nr_ctps_w	:= '       ';
      			nr_serie_ctps_w := '     ';      
		end if;
		
		/*if	(vl_desconto_w != '000000000000000') then
			cd_ocorrencia_w	:= '05';
		else
			cd_ocorrencia_w	:= '  ';
		end if;*/
		
		ds_conteudo_w	:=	ie_tipo_registro_w ||
					ie_tipo_inscr_resp_w  ||
					cd_inscr_resp_w  ||
					ie_tipo_inscr_w  ||
					lpad(cd_cgc_estip_w,14,'0')  ||
					nr_pis_pasep_w  ||
					rpad(nvl(dt_admissao_w,' '),8,' ')  ||
					cd_categoria_trabalhador_w  ||
					nm_pessoa_fisica_w  ||
					cd_funcionario_w  ||
					nr_ctps_w  ||
					nr_serie_ctps_w ||
					dt_opcao_w ||
					dt_nascimento_w ||
					cd_cbo_saude_w ||
					vl_remuneracao_w ||
					vl_remuneracao_13_w ||
					cd_classe_contrib_w ||
					cd_ocorrencia_w ||
					vl_desconto_w ||
					vl_base_13_w ||
					vl_base_13_mov_w ||
					vl_base_13_w ||
					ds_brancos_98_w || 
					ie_final_linha;
		
		insert into w_interf_sefip
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ds_conteudo)
		values	(w_interf_sefip_seq.nextval,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			ds_conteudo_w);
		end;
	end loop;
	close C02;
	/*Registro - 30 Fim*/
	end;
end loop;
close C01;
/*Registro - 20 Fim*/

/* Registro - 90 */
ie_tipo_registro_w	:= '90';
ds_brancos_306_w	:= lpad(' ',306, ' ');
ds_marca_51_w		:= lpad('9',51,'9');

ds_conteudo_w	:=	ie_tipo_registro_w ||
			ds_marca_51_w ||
			ds_brancos_306_w  ||
			ie_final_linha;

insert into w_interf_sefip
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ds_conteudo)
values	(w_interf_sefip_seq.nextval,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	sysdate,
	ds_conteudo_w);
/*Registro - 90 Fim*/

commit;

end pls_gerar_arquivo_sefip;
/