create or replace PROCEDURE Gerar_Retorno_Glosa(	NR_SEQ_RETORNO_P     	NUMBER,
				NM_USUARIO_P         	VARCHAR2,
				ie_forma_agrupar_p		VARCHAR2) IS

qt_movto_w			number(15,0);
nr_doc_convenio_w		varchar2(20);
vl_glosa_item_w			number(15,2);
vl_glosa_movto_w			number(15,2)	:= 0;
qt_glosa_movto_w			number(9,3)	:= 0;
ds_erro_w			varchar2(255);
cd_convenio_w			number(5);
nr_interno_conta_w			number(10);
nr_seq_ret_item_w			number(10);
nr_seq_movto_w			number(10);
nr_seq_movto_old_w		number(10);
cd_item_w			number(15);
qt_glosa_w			number(9,3);
qt_cobrada_w			number(9,3);
vl_cobrado_w			number(15,2);
vl_pago_w			number(15,2);
vl_glosa_w			number(15,2);
vl_amaior_w			number(15,2);
ds_complemento_w    		varchar2(255);  /* descricao do motivo da glosa */
cd_glosa_w			varchar2(40);
cd_setor_w			number(5);
dt_execucao_w			date;
cd_item_old_w			number(15);
qt_glosa_old_w			number(9,3);
vl_glosa_old_w			number(15,2);
cd_glosa_old_w			varchar2(40);
cd_setor_old_w			number(5);
dt_execucao_old_w		date;
nr_interno_conta_old_w		number(10);
nr_sequencia_w			number(10);
cd_material_w			number(6);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_glosa_dif_tab_w			varchar2(255);
cd_setor_responsavel_w		number(5);
cd_resposta_w			number(10);
cd_estabelecimento_w		number(4);
ie_setor_movto_w			varchar2(1);
ie_tipo_glosa_w			varchar2(1);
IE_GLOSA_ADIC_RET_w		varchar2(255);
nr_seq_propaci_w			number(10);
nr_seq_matpaci_w			number(10);
nr_seq_item_conta_w		number(10);
cont_w				number(10);
qt_item_w			number(9,3)	:= 0;
ie_gerar_qtd_w			varchar2(1);
ie_somente_valor_cobrado_w		varchar2(255);
cd_glosa_regra_w			number(5);
ie_glosa_w			varchar2(255);
ie_gerar_glosa_zero_w		varchar2(1);
vl_glosa_final_w			number(15,2);
ie_gerar_complemento_w		varchar2(1);
ds_compl_item_w			varchar2(255);
cd_procedimento_conta_w		number(15);
cd_material_conta_w		number(15);
ie_origem_proced_conta_w		number(15);
ds_observacao_conta_w		varchar2(2000);
cd_item_convenio_conta_w		varchar2(20);
cd_setor_atend_conta_w		number(5);
dt_execucao_conta_w		date;
qt_glosa_conta_w			number(9,3);
vl_glosa_conta_w			number(15,2);
ie_gerar_itens_conta_w		varchar2(1);
vl_amaior_movto_w			number(15,2) := 0;
ie_gerar_glosa_adic_w		varchar2(10);
ie_valor_pago_menor_w		varchar2(10) := 'N';
ie_recalcular_valores_guia_w	varchar2(10) := 'N';
nr_seq_partic_w			convenio_retorno_glosa.nr_seq_partic%type;
ie_funcao_medico_w		convenio_retorno_movto.ie_funcao_medico%type;
cd_proc_conversao_w 		conversao_proc_convenio.cd_proc_convenio%type;
cd_mat_conversao_w		conversao_material_convenio.cd_material_convenio%type;
qt_proc_conversao_w		number(10);
qt_mat_conversao_w		number(10);
cd_item_conversao_w		number(15);
cd_item_convenio_w		convenio_retorno_glosa.cd_item_convenio%type;
cd_autorizacao_w		convenio_retorno_item.cd_autorizacao%type;
ie_acao_glosa_w			convenio_retorno_glosa.ie_acao_glosa%type;
qt_pago_w			convenio_retorno_movto.qt_paga%type;
nr_sequencia_item_w convenio_retorno_movto.nr_sequencia%type;
cursor C01 is
select	b.cd_convenio,
	c.nr_interno_conta,
	a.nr_seq_ret_item,
	a.nr_sequencia,
	trim(a.cd_item),
	a.ds_complemento,
	a.cd_motivo,
	a.cd_setor_atendimento,
	trunc(dt_execucao,'dd'),
	nvl(a.qt_cobrada,0),
	nvl(a.qt_cobrada,0) - nvl(a.qt_paga,0),
	nvl(a.vl_cobrado,0),
	nvl(obter_valor_ret_movto(a.nr_sequencia,'P'),0),
	b.cd_estabelecimento,
	a.nr_seq_item_conta,
	decode(ie_gerar_complemento_w,'S',a.ds_complemento,null) ds_compl_item,
	ie_funcao_medico,
	qt_paga,
  a.nr_sequencia_item
from  	convenio_retorno_movto a,
	convenio_retorno_item c,
	convenio_retorno b
where	c.nr_seq_retorno		= b.nr_sequencia
and	b.nr_sequencia			= nr_seq_retorno_p
and	a.nr_seq_ret_item		= c.nr_sequencia
and	Nvl(a.cd_item,0) 		<> 0
and	nvl(a.ie_situacao_item,'X')	<> 'G'
and	(nvl(a.ie_gera_resumo,'S') = 'S' or a.cd_motivo is not null)
and	((nvl(a.qt_cobrada,0) - nvl(a.qt_paga,0) <> 0) or (nvl(a.vl_cobrado,0) - nvl(obter_valor_ret_movto(a.nr_sequencia,'P'),0) <> 0))
and	((nvl(ie_somente_valor_cobrado_w, 'N') = 'N') or (vl_cobrado is not null))
and	((a.vl_glosa is null or nvl(a.vl_glosa,0) <> 0 or nvl(ie_gerar_glosa_zero_w,'N') = 'S') or (nvl(ie_gerar_glosa_adic_w, 'N') = 'S'))
and	nvl(obter_se_gera_item_glosa(b.cd_convenio, a.cd_motivo),'S') = 'S'
and	(nvl(ie_valor_pago_menor_w,'N') = 'N' or (a.vl_pago < a.vl_cobrado)) /*OS 559918*/
order by	b.cd_convenio,
	c.nr_interno_conta,
	a.nr_seq_ret_item,
	a.cd_item,
	a.cd_motivo,
	a.cd_setor_atendimento,
	trunc(dt_execucao,'dd');

cursor C02 is
select	cd_autorizacao,
	vl_glosado + vl_amenor,
	vl_glosa
from	(select	a.cd_autorizacao,
		a.vl_glosado,
		a.vl_amenor,
		sum(nvl(b.vl_cobrado,0)) - sum(nvl(obter_valor_ret_movto(b.nr_sequencia,'P'),0)) vl_glosa
	from 	convenio_retorno_movto b,
		convenio_retorno_item a,
		convenio_retorno c
	where	c.nr_sequencia	= nr_seq_retorno_p
	and	a.nr_seq_retorno	= c.nr_sequencia
	and	b.nr_seq_ret_item	= a.nr_sequencia
	and	((b.cd_motivo is not null)
		 or (nvl(b.ie_gera_resumo,'S') <> 'U'))
	and	((nvl(b.vl_cobrado,0) - nvl(obter_valor_ret_movto(b.nr_sequencia,'P'),0) > 0)
		 or (nvl(b.qt_cobrada,0) - nvl(b.qt_paga,0)) > 0)
	group by	a.cd_autorizacao,
		a.vl_glosado,
		a.vl_amenor
	having	sum(nvl(b.vl_cobrado,0)) - sum(nvl(obter_valor_ret_movto(b.nr_sequencia,'P'),0)) <> (a.vl_glosado + a.vl_amenor));


cursor c04 is
Select	nr_interno_conta,
	nr_sequencia,
	cd_autorizacao
from	convenio_retorno_item
where	nr_seq_retorno = nr_seq_retorno_p;

/* OS 351840 - Douglas dos Santos (26/08/2011) - Cursor C05 */
cursor c05 is
Select	cd_procedimento,
	null,
	ie_origem_proced,
	ds_observacao,
	cd_procedimento_convenio,
	cd_setor_atendimento,
	dt_procedimento,
	qt_procedimento,
	vl_procedimento
from	procedimento_paciente a
where	nr_interno_conta = nr_interno_conta_w
and	Nvl(a.nr_doc_convenio,cd_autorizacao_w) = cd_autorizacao_w
and	cd_motivo_exc_conta is null
and	not exists (Select	1
		From	convenio_retorno_movto x
		where	x.cd_item	= to_char(a.cd_procedimento)
		and	x.nr_seq_ret_item	= nr_seq_ret_item_w)
union all
select	null,
	cd_material,
	null,
	ds_observacao,
	cd_material_convenio,
	cd_setor_atendimento,
	dt_prescricao,
	qt_material,
	vl_material
from	material_atend_paciente a
where	nr_interno_conta = nr_interno_conta_w
and	Nvl(a.nr_doc_convenio,cd_autorizacao_w) = cd_autorizacao_w
and	cd_motivo_exc_conta is null
and	not exists (Select	1
		from	convenio_retorno_movto x
		where	x.cd_item	= to_char(a.cd_material)
		and	x.nr_seq_ret_item	= nr_seq_ret_item_w);

cursor c06 is
Select	a.nr_sequencia
from	convenio_retorno_item a
where	a.nr_seq_retorno = nr_seq_retorno_p
and	exists
	(select	1
	from	convenio_retorno_glosa b
	where	b.nr_seq_ret_item	= a.nr_sequencia);

BEGIN

begin
nr_seq_partic_w := -1;

dbms_application_info.SET_ACTION('GERAR_RETORNO_GLOSA');


select	somente_numero(vl_parametro)
into	cd_glosa_dif_tab_w
from	funcao_parametro
where	cd_funcao	= 27
and	nr_sequencia	= 6;


select	nvl(c.IE_GLOSA_ADIC_RET, 'N')
into	IE_GLOSA_ADIC_RET_w
from	convenio_estabelecimento c,
	convenio b,
	convenio_retorno a
where	a.nr_sequencia		= nr_seq_retorno_p
and	a.cd_convenio		= b.cd_convenio
and	b.cd_convenio		= c.cd_convenio
and	c.cd_estabelecimento	= a.cd_estabelecimento;

select	count(*)
into	qt_movto_w
from	convenio_retorno_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

select	max(a.cd_estabelecimento)
into	cd_estabelecimento_w
from	convenio_retorno a
where	a.nr_sequencia	= nr_seq_retorno_p;

ie_somente_valor_cobrado_w	:= nvl(Obter_valor_param_usuario(27, 139, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');
ie_valor_pago_menor_w		:= nvl(Obter_valor_param_usuario(27, 253, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');
obter_param_usuario(27,180,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_gerar_complemento_w);

if	(qt_movto_w > 0) then
	select	count(*)
	into	qt_movto_w
	from	convenio_retorno_movto
	where	nr_seq_retorno	= nr_seq_retorno_p
	and	nr_seq_ret_glosa	is not null;

	if	(qt_movto_w > 0) then
		--r.aise_application_error(-20011,'A glosa ja foi lancada !');
		wheb_mensagem_pck.exibir_mensagem_abort(191840);
	end if;


	cd_item_old_w		:= 0;
	qt_glosa_old_w		:= 0;
	vl_glosa_old_w		:= 0;
	cd_glosa_old_w		:= '';
	cd_setor_old_w		:= 0;
	nr_interno_conta_old_w	:= 0;
	dt_execucao_old_w	:= sysdate;

	obter_param_usuario(27, 159, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_glosa_zero_w);
	obter_param_usuario(27, 192, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_glosa_adic_w);

	open C01;
	loop
	fetch C01 into
		cd_convenio_w,
		nr_interno_conta_w,
		nr_seq_ret_item_w,
		nr_seq_movto_w,
		cd_item_w,
		ds_complemento_w, /* descricao do motivo de glosa */
		cd_glosa_w,
		cd_setor_w,
		dt_execucao_w,
		qt_cobrada_w,
		qt_glosa_w,
		vl_cobrado_w,
		vl_pago_w,
		cd_estabelecimento_w,
		nr_seq_item_conta_w,
		ds_compl_item_w,
		ie_funcao_medico_w,
		qt_pago_w,
    nr_sequencia_item_w;
	exit when C01%notfound;

		nr_seq_partic_w := -1;

		vl_amaior_w		:= 0;

		vl_glosa_w := vl_cobrado_w - vl_pago_w;
		if	(vl_pago_w < 0) then
			vl_glosa_w := (vl_pago_w * -1);
		end if;

		if	(qt_glosa_w < 0) then /*lhalves OS 369707 em 21/10/2011 - Se qt cobrada menor que qt paga, nao deve ter qt glosa*/
			qt_glosa_w := 0;
		end if;

		if	(cd_item_old_w = 0) or
			((nr_interno_conta_old_w	<> nr_interno_conta_w) or
			 (cd_item_old_w		<> nvl(cd_item_w,'-1')) or
			 (cd_glosa_old_w		<> cd_glosa_w) or
			 ((cd_setor_old_w		<> cd_setor_w) and (nvl(ie_forma_agrupar_p,'T') in ('S','T'))) or
			 ((nr_seq_movto_old_w	<> nr_seq_movto_w) and (nvl(ie_forma_agrupar_p,'T') in ('N'))) or
			 ((dt_execucao_old_w	<> dt_execucao_w) and (nvl(ie_forma_agrupar_p,'T') in ('D','T')))) then

			if	(cd_item_old_w <> 0) and (nvl(ie_gerar_glosa_zero_w,'N') = 'N') then

				update	convenio_retorno_glosa
				set	vl_glosa		= nvl(vl_glosa_movto_w,0),
				    	qt_glosa		= nvl(qt_glosa_movto_w,0),
					vl_amaior		= nvl(vl_amaior_movto_w,0)
				where	nr_sequencia	= nr_sequencia_w;
			end if;

			vl_glosa_movto_w	:= 0;
			qt_glosa_movto_w	:= 0;
			vl_amaior_movto_w	:= 0;

			select	convenio_retorno_glosa_seq.NextVal
			into	nr_sequencia_w
			from	dual;

			nr_seq_matpaci_w	:= null;
			nr_seq_propaci_w	:= null;
			cd_material_w		:= null;
			cd_procedimento_w	:= null;

			/*lhalves OS 401712 em 24/01/2012 - Se o item nao tem nr_seq_item_conta_w no movimento, busca conforme o codigo*/
			if	(nr_seq_item_conta_w is null) then
				select	max(a.nr_sequencia)
				into	nr_seq_item_conta_w
				from	procedimento_paciente a
				where	nvl(somente_numero(cd_procedimento_convenio),cd_procedimento) = somente_numero(cd_item_w)
				and	a.nr_interno_conta	= nr_interno_conta_w
				and	a.cd_motivo_exc_conta	is null;

				if	(nr_seq_item_conta_w is null) then
					select	max(a.nr_sequencia)
					into	nr_seq_item_conta_w
					from	procedimento_paciente a
					where	a.cd_procedimento	= somente_numero(cd_item_w)
					and	a.nr_interno_conta	= nr_interno_conta_w
					and	a.cd_motivo_exc_conta	is null;

					if	(nr_seq_item_conta_w is null) then
						select	max(a.nr_sequencia)
						into	nr_seq_item_conta_w
						from	procedimento_paciente a
						where	a.cd_procedimento_tuss	= somente_numero(cd_item_w)
						and	a.nr_interno_conta	= nr_interno_conta_w
						and	a.cd_motivo_exc_conta	is null;

						if	(nr_seq_item_conta_w is null) then
							select	max(a.nr_sequencia)
							into	nr_seq_item_conta_w
							from	material_atend_paciente a
							where	nvl(somente_numero(a.cd_material_convenio),a.cd_material) 	= somente_numero(cd_item_w)
							and	a.nr_interno_conta						= nr_interno_conta_w
							and	a.cd_motivo_exc_conta						is null;

							if	(nr_seq_item_conta_w is null) then
								select	max(a.nr_sequencia)
								into	nr_seq_item_conta_w
								from	material_atend_paciente a
								where	a.cd_material		= somente_numero(cd_item_w)
								and	a.nr_interno_conta	= nr_interno_conta_w
								and	a.cd_motivo_exc_conta	is null;

								if	(nr_seq_item_conta_w is null) then
									select	max(a.nr_sequencia)
									into	nr_seq_item_conta_w
									from	material_atend_paciente a
									where	somente_numero(a.cd_material_tiss)	= somente_numero(cd_item_w)
									and	a.nr_interno_conta			= nr_interno_conta_w
									and	a.cd_motivo_exc_conta			is null;
								end if;
							end if;
						end if;
					end if;
				end if;
			end if;
			/*lhalves OS 401712 em 24/01/2012 - Se existe  nr_seq_item_conta_w, busca o nr_seq_propaci e nr_seq_matpaci, do item na conta*/

			if	(nr_seq_item_conta_w is not null) then

				select	max(nr_seq_propaci),
					max(nr_seq_matpaci),
					max(cd_item),
					max(cd_procedimento),
					max(ie_origem_proced),
					max(cd_material)
				into	nr_seq_propaci_w,
					nr_seq_matpaci_w,
					cd_item_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					cd_material_w
				from	(
					select	max(a.nr_sequencia) nr_seq_propaci,
						null nr_seq_matpaci,
						nvl(max(somente_numero(a.cd_procedimento_convenio)), cd_item_w) cd_item,
						nvl(max(cd_procedimento),0) cd_procedimento,
						nvl(max(ie_origem_proced),0) ie_origem_proced,
						null cd_material
					from	procedimento_paciente a
					where	a.nr_sequencia		= nr_seq_item_conta_w
					and	a.nr_interno_conta	= nr_interno_conta_w
					union
					select	null nr_seq_propaci,
						max(a.nr_sequencia) nr_seq_matpaci,
						max(somente_numero(a.cd_material_convenio)) cd_item,
						null cd_procedimento,
						null ie_origem_proced,
						nvl(max(cd_material),0) cd_material
					from	material_atend_paciente a
					where	a.nr_sequencia		= nr_seq_item_conta_w
					and	a.nr_interno_conta	= nr_interno_conta_w);

			else
				cd_item_w := somente_numero(cd_item_w);

				select	count(*)
				into 	qt_proc_conversao_w
				from	conversao_proc_convenio
				where	somente_numero(cd_proc_convenio) = cd_item_w
				and	nvl(ie_situacao,'A') = 'A'
				and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
				and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_vigencia_final,sysdate);

				if	(nvl(qt_proc_conversao_w,0) > 0) then
					select	max(cd_procedimento)
					into	cd_proc_conversao_w
					from	conversao_proc_convenio
					where	somente_numero(cd_proc_convenio) = cd_item_w
					and	cd_convenio = cd_convenio_w
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_vigencia_final,sysdate);

					select	min(somente_numero(cd_proc_convenio))
					into	cd_item_conversao_w
					from	conversao_proc_convenio
					where	cd_procedimento = cd_proc_conversao_w
					and	cd_convenio = cd_convenio_w
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_vigencia_final,sysdate);
				end if;

				select	count(*)
				into	qt_mat_conversao_w
				from	conversao_material_convenio
				where	somente_numero(cd_material_convenio) = cd_item_w
				and	nvl(ie_situacao,'A') = 'A'
				and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
				and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia,sysdate);

				if	(nvl(qt_mat_conversao_w,0) > 0) then
					select	max(cd_material)
					into	cd_mat_conversao_w
					from	conversao_material_convenio
					where	somente_numero(cd_material_convenio) = cd_item_w
					and	cd_convenio = cd_convenio_w
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia,sysdate);

					select	min(somente_numero(cd_material_convenio))
					into	cd_item_conversao_w
					from	conversao_material_convenio
					where	cd_material = cd_mat_conversao_w
					and	cd_convenio = cd_convenio_w
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia,sysdate);
				end if;

				select	nvl(max(cd_procedimento),0),
					nvl(max(ie_origem_proced),0)
				into	cd_procedimento_w,
					ie_origem_proced_w
				from	procedimento_paciente
				where	nr_interno_conta						= nr_interno_conta_w
				and	nvl(somente_numero(cd_procedimento_convenio), cd_procedimento)	in (cd_item_w,cd_item_conversao_w);

				if	(cd_procedimento_w = 0) then
					/* para importar o codigo existente no movimento - 05/06/2003*/
					select	nvl(max(cd_procedimento),0),
						nvl(max(ie_origem_proced),0)
					into	cd_procedimento_w,
						ie_origem_proced_w
					from	procedimento
					where	cd_procedimento		= cd_item_w;

					if	(cd_procedimento_w = 0) then
						cd_procedimento_w	:= null;

						select	nvl(max(cd_material),0)
						into	cd_material_w
						from	material_atend_paciente
						where	nr_interno_conta	= nr_interno_conta_w
						and	(nvl(somente_numero(cd_material_convenio), cd_material) in (cd_item_w,cd_item_conversao_w)
							 or cd_material = cd_item_w);

						if	(nvl(cd_material_w,0) = 0) then
							select	nvl(max(cd_material),0)
							into	cd_material_w
							from	material_atend_paciente
							where	nr_interno_conta			= nr_interno_conta_w
							and	somente_numero(cd_material_tiss)	= cd_item_w
							and	somente_numero(cd_material_tiss)	<> 0;
						end if;

					end if;
				end if;

			end if;


			if	(cd_procedimento_w is null and
				 cd_material_w is null) then

				select	count(*)
				into	qt_mat_conversao_w
				from	conversao_material_convenio
				where	somente_numero(cd_material_convenio) = cd_item_w
				and	nvl(ie_situacao,'A') = 'A'
				and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
				and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia,sysdate);

				if	(nvl(qt_mat_conversao_w,0) > 0) then
					select	max(cd_material)
					into	cd_mat_conversao_w
					from	conversao_material_convenio
					where	somente_numero(cd_material_convenio) = cd_item_w
					and	cd_convenio = cd_convenio_w
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia,sysdate);

					select	min(somente_numero(cd_material_convenio))
					into	cd_item_conversao_w
					from	conversao_material_convenio
					where	cd_material = cd_mat_conversao_w
					and	cd_convenio = cd_convenio_w
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_final_vigencia,sysdate);
				end if;

				select	nvl(max(cd_procedimento),0),
					nvl(max(ie_origem_proced),0)
				into	cd_procedimento_w,
					ie_origem_proced_w
				from	procedimento_paciente
				where	nr_interno_conta						= nr_interno_conta_w
				and	nvl(somente_numero(cd_procedimento_convenio), cd_procedimento)	in (cd_item_w,cd_item_conversao_w);

				if	(cd_procedimento_w = 0) then
					select	nvl(max(cd_procedimento),0),
						nvl(max(ie_origem_proced),0)
					into	cd_procedimento_w,
						ie_origem_proced_w
					from	procedimento
					where	cd_procedimento		= cd_item_w;

					if	(cd_procedimento_w = 0) then
						cd_procedimento_w	:= null;

						select	nvl(max(cd_material),0)
						into	cd_material_w
						from	material_atend_paciente
						where	nr_interno_conta	= nr_interno_conta_w
						and	(nvl(somente_numero(cd_material_convenio), cd_material) in (cd_item_w,cd_item_conversao_w)
							 or cd_material = cd_item_w);

						if	(nvl(cd_material_w,0) = 0) then
							select	nvl(max(cd_material),0)
							into	cd_material_w
							from	material_atend_paciente
							where	nr_interno_conta			= nr_interno_conta_w
							and	somente_numero(cd_material_tiss)	= cd_item_w
							and	somente_numero(cd_material_tiss)	<> 0;
						end if;

					end if;
				end if;

			end if;

			obter_param_usuario(27,138,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_gerar_qtd_w);

			if	(ie_gerar_qtd_w = 'S') then

				select	max(qt_item)
				into	qt_item_w
				from	(select	a.qt_procedimento qt_item
					from	procedimento_paciente a
					where	a.nr_sequencia	= nr_seq_propaci_w
					union
					select	a.qt_material qt_item
					from	material_atend_paciente a
					where	a.nr_sequencia	= nr_seq_matpaci_w);

			end if;

			/* Inicio Elemar 04/08/2003 */
			if	(cd_glosa_w is null) and
				(qt_glosa_w = 0) and
				(vl_glosa_w <> 0) and
				(cd_glosa_dif_tab_w <> '0') then
				cd_glosa_w	:= cd_glosa_dif_tab_w;
			end if;
			/* Fim */


			if	(vl_glosa_w < 0) then
				vl_amaior_w	:= vl_glosa_w * -1;
				vl_glosa_w	:= 0;
			end if;

			Valida_Glosa_Conta(nr_interno_conta_w, vl_cobrado_w - vl_glosa_movto_w , vl_glosa_movto_w + vl_glosa_w, null, nm_usuario_p, ie_glosa_w, cd_glosa_regra_w);

			if	(ie_glosa_w = 'S') and
				(cd_glosa_regra_w is not null) then
				cd_glosa_w	:= to_char(cd_glosa_regra_w);
			end if;


			cd_setor_responsavel_w		:= null;
			cd_resposta_w			:= null;

			if	(nvl(cd_glosa_w,'0') <> '0') then

				select	max(cd_setor_resp),
					max(cd_resposta),
					max(ie_tipo_glosa)
				into	cd_setor_responsavel_w,
					cd_resposta_w,
					ie_tipo_glosa_w
				from	motivo_glosa
				where	to_char(cd_motivo_glosa)	= cd_glosa_w;

				obter_param_usuario(27,97,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_setor_movto_w);

				if	(ie_tipo_glosa_w = 'T' and ie_setor_movto_w = 'S') then
					cd_setor_responsavel_w	:= cd_setor_w;
				end if;

			end if;

			if	(ie_setor_movto_w = 'C') then

				if	(nr_seq_propaci_w is not null) then
					select	max(cd_setor_atendimento)
					into	cd_setor_responsavel_w
					from 	procedimento_paciente
					where	nr_sequencia	= nr_seq_propaci_w;

				elsif	(nr_seq_matpaci_w is not null) then

					select	max(cd_setor_atendimento)
					into	cd_setor_responsavel_w
					from 	material_atend_paciente
					where	nr_sequencia	= nr_seq_matpaci_w;
				end if;
				cd_setor_w		:= nvl(cd_setor_w, cd_setor_responsavel_w);
			end if;


			if	(IE_GLOSA_ADIC_RET_w = 'S') then
				select	sum(decode(nvl(vl_pago,0),0,0,nvl(vl_cobrado,0) - (nvl(vl_total_pago,0) + nvl(vl_filme,0))))
				into	vl_amaior_w
				from	convenio_retorno_movto
				where	nr_sequencia		= nr_seq_movto_w
				and	decode(nvl(vl_pago,0),0,0,nvl(vl_cobrado,0) - (nvl(vl_total_pago,0) + nvl(vl_filme,0))) <= 0;

				vl_amaior_w := vl_amaior_w * -1;
			end if;

			select	count(*)
			into	cont_w
			from	motivo_glosa
			where	to_char(cd_motivo_glosa)	= cd_glosa_w;

			if	(cont_w = 0) and
				(somente_numero(cd_glosa_w) > 0) then
				/*r.aise_application_error(-20011, 'O motivo de glosa ' || cd_glosa_w || ' nao existe no cadastro de motivos de glosa do convenio!' || chr(13) || chr(10) ||
								'ConTa paciente: ' || nr_interno_conta_w);*/
				wheb_mensagem_pck.exibir_mensagem_abort(191841,'cd_glosa_w='||cd_glosa_w||';'||
										'nr_interno_conta_w='||nr_interno_conta_w);
			end if;

			begin

			vl_glosa_final_w	:= 0;

			if	(nvl(ie_gerar_glosa_zero_w,'N') = 'S') then

				select	nvl(sum(vl_item),0)
				into	vl_glosa_final_w
				from	(select	a.vl_procedimento vl_item
					from	procedimento_paciente a
					where	((a.nr_sequencia = nr_seq_propaci_w) or (nvl(nr_seq_propaci_w,0) = 0 and a.cd_procedimento = cd_procedimento_w))
					and	a.nr_interno_conta	= nr_interno_conta_w
					union
					select	a.vl_material
					from	material_atend_paciente a
					where	((a.nr_sequencia = nr_seq_matpaci_w) or (nvl(nr_seq_matpaci_w,0) = 0 and a.cd_material = cd_material_w))
					and	a.nr_interno_conta	= nr_interno_conta_w);

				vl_amaior_w	:= 0;

			end if;



			if	(nvl(ie_funcao_medico_w,0) > 0) then

				select	nvl(max(a.nr_Seq_partic),-1)
				into	nr_seq_partic_w
				from	procedimento_participante a
				where	a.nr_sequencia 	= nr_seq_propaci_w
				and	ie_funcao	= ie_funcao_medico_w
				and	vl_conta	= vl_cobrado_w;

			end if;

			begin
				select  ie_acao_glosa
				into	ie_acao_glosa_w
				from	motivo_glosa
				where	cd_motivo_glosa = cd_glosa_w
				and    	nvl(ie_situacao, 'A') = 'A'
				and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w;
			exception
			when others then
				ie_acao_glosa_w := null;
			end;

			if	(nvl(ie_gerar_glosa_zero_w,'N') = 'N') or
				(vl_glosa_final_w <> 0) then

				if	(nr_seq_partic_w <> -1) then


					insert into convenio_retorno_glosa
						(nr_sequencia,
						nr_seq_ret_item,
						vl_glosa,
						dt_atualizacao,
						nm_usuario,
						cd_procedimento,
						ie_origem_proced,
						cd_material,
						cd_motivo_glosa,
						ie_acao_glosa,
						ds_observacao,
						ie_atualizacao,
						cd_item_convenio,
						qt_glosa,
						cd_setor_atendimento,
						dt_execucao,
						vl_amaior,
						qt_cobrada,
						vl_cobrado,
						cd_setor_responsavel,
						cd_resposta,
						nr_seq_propaci,
						nr_seq_matpaci,
						ds_complemento,
						nr_seq_partic,
						qt_informada,
						vl_informado,
						qt_liberada,
						vl_liberado,
            nr_sequencia_item)
					values (nr_sequencia_w,
						nr_seq_ret_item_w,
						vl_glosa_final_w,
						sysdate,
						nm_usuario_p,
						decode(cd_procedimento_w,0,null,cd_procedimento_w),
						decode(cd_procedimento_w, 0, null, ie_origem_proced_w),
						decode(cd_material_w, 0, null, cd_material_w),
						decode(somente_numero(cd_glosa_w), 0, null, somente_numero(cd_glosa_w)),
						ie_acao_glosa_w,
						null,
						'N',
						cd_item_w,
						nvl(qt_item_w,0),
						cd_setor_w,
						dt_execucao_w,
						vl_amaior_w,
						qt_cobrada_w,
						vl_cobrado_w,
						cd_setor_responsavel_w,
						cd_resposta_w,
						nr_seq_propaci_w,
						nr_seq_matpaci_w,
						ds_compl_item_w,
						nr_seq_partic_w,
						qt_cobrada_w,
						vl_cobrado_w,
						qt_pago_w,
						vl_pago_w,
            nr_sequencia_item_w);
				else


					insert into convenio_retorno_glosa
						(nr_sequencia,
						nr_seq_ret_item,
						vl_glosa,
						dt_atualizacao,
						nm_usuario,
						cd_procedimento,
						ie_origem_proced,
						cd_material,
						cd_motivo_glosa,
						ie_acao_glosa,
						ds_observacao,
						ie_atualizacao,
						cd_item_convenio,
						qt_glosa,
						cd_setor_atendimento,
						dt_execucao,
						vl_amaior,
						qt_cobrada,
						vl_cobrado,
						cd_setor_responsavel,
						cd_resposta,
						nr_seq_propaci,
						nr_seq_matpaci,
						ds_complemento,
						qt_informada,
						vl_informado,
						qt_liberada,
						vl_liberado,
            nr_sequencia_item)
					values (nr_sequencia_w,
						nr_seq_ret_item_w,
						vl_glosa_final_w,
						sysdate,
						nm_usuario_p,
						decode(cd_procedimento_w,0,null,cd_procedimento_w),
						decode(cd_procedimento_w, 0, null, ie_origem_proced_w),
						decode(cd_material_w, 0, null, cd_material_w),
						decode(somente_numero(cd_glosa_w), 0, null, somente_numero(cd_glosa_w)),
						ie_acao_glosa_w,
						null,
						'N',
						cd_item_w,
						nvl(qt_item_w,0),
						cd_setor_w,
						dt_execucao_w,
						vl_amaior_w,
						qt_cobrada_w,
						vl_cobrado_w,
						cd_setor_responsavel_w,
						cd_resposta_w,
						nr_seq_propaci_w,
						nr_seq_matpaci_w,
						ds_compl_item_w,
						qt_cobrada_w,
						vl_cobrado_w,
						qt_pago_w,
						vl_pago_w,
            nr_sequencia_item_w);
				end if;

				update	convenio_retorno_movto
				set	nr_seq_ret_glosa	= nr_sequencia_w
				where	nr_sequencia		= nr_seq_movto_w;

			end if;

			exception
			when others then
				/*r.aise_application_error(-20011, 'Erro ao inserir convenio_retorno_glosa!' 	|| chr(13) ||
							'Conta paciente: '	|| nr_interno_conta_w 	|| chr(13) ||
							'Item Conv: '	|| cd_item_w 		|| chr(13) ||
							'Procedimento: '	|| cd_procedimento_w 	|| chr(13) ||
							'Origem Proc: '	|| ie_origem_proced_w 	|| chr(13) ||
							'Material: '	|| cd_material_w 		|| chr(13) ||
							sqlerrm);*/
				wheb_mensagem_pck.exibir_mensagem_abort(191842,
									'nr_interno_conta_w='||nr_interno_conta_w||';'||
									'cd_item_w='||cd_item_w||';'||
									'cd_procedimento_w='||cd_procedimento_w||';'||
									'ie_origem_proced_w='||ie_origem_proced_w||';'||
									'cd_material_w='||cd_material_w||';'||
									'sqlerrm_w='||sqlerrm);
			end;
		end if;

		nr_interno_conta_old_w	:= nr_interno_conta_w;
		nr_seq_movto_old_w	:= nr_seq_movto_w;
		cd_item_old_w		:= cd_item_w;
		cd_glosa_old_w		:= cd_glosa_w;
		cd_setor_old_w		:= cd_setor_w;
		dt_execucao_old_w	:= dt_execucao_w;
		nr_seq_movto_old_w	:= nr_seq_movto_w;

		if	(vl_glosa_w < 0) then
			vl_amaior_w	:= vl_glosa_w * -1;
			vl_glosa_w	:= 0;
		end if;
		vl_glosa_movto_w	:= vl_glosa_movto_w + vl_glosa_w;
		vl_amaior_movto_w	:= vl_amaior_movto_w + vl_amaior_w;

		if	(nvl(qt_item_w,0) = 0) then
			qt_glosa_movto_w	:= qt_glosa_movto_w + qt_glosa_w;
		else
			qt_glosa_movto_w	:= qt_glosa_movto_w + qt_item_w;
		end if;

	end loop;
	close C01;

	if	(cd_item_old_w <> 0) then

		begin
		update	convenio_retorno_glosa
		set	vl_glosa		= vl_glosa_movto_w,
		    	qt_glosa		= qt_glosa_movto_w
		where	nr_sequencia	= nr_sequencia_w;

		select	max(a.nr_seq_ret_item)
		into	nr_seq_ret_item_w
		from	convenio_retorno_glosa a
		where	a.nr_sequencia	= nr_sequencia_w;

		exception
		when others then
			--r.aise_application_error(-20011, 'Erro ao atualizar convenio_retorno_glosa!' || chr(13) || sqlerrm);
			wheb_mensagem_pck.exibir_mensagem_abort(191843,'sqlerrm_w='||sqlerrm);
		end;
	end if;


end if;

obter_param_usuario(27,170,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_gerar_itens_conta_w);


if	(nvl(ie_gerar_itens_conta_w,'N') = 'S') THEN
	open c04;
	loop
	fetch c04 into
		nr_interno_conta_w,
		nr_seq_ret_item_w,
		cd_autorizacao_w;
	exit when c04% notfound;

		open C05;
		loop
		fetch C05 into
			cd_procedimento_conta_w,
			cd_material_conta_w,
			ie_origem_proced_conta_w,
			ds_observacao_conta_w,
			cd_item_convenio_conta_w,
			cd_setor_atend_conta_w,
			dt_execucao_conta_w,
			qt_glosa_conta_w,
			vl_glosa_conta_w;
		exit when C05%notfound;

			insert into convenio_retorno_glosa(
					nr_sequencia,
					dt_atualizacao,
					ie_atualizacao,
					nm_usuario,
					nr_seq_ret_item,
					qt_glosa,
					vl_glosa,
					cd_procedimento,
					cd_material,
					ie_origem_proced,
					ds_observacao,
					cd_item_convenio,
					cd_setor_atendimento
					)
				values	( convenio_retorno_glosa_seq.Nextval,
					sysdate,
					'N',
					nm_usuario_p,
					nr_seq_ret_item_w,
					qt_glosa_conta_w,
					vl_glosa_Conta_w,
					cd_procedimento_conta_w,
					cd_material_conta_w,
					ie_origem_proced_conta_w,
					ds_observacao_conta_w,
					cd_item_convenio_conta_w,
					cd_setor_atend_conta_w);

		end loop;
		close C05;

end loop;
close c04;

end if;

obter_param_usuario(27,262,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_recalcular_valores_guia_w);

if	(nvl(ie_recalcular_valores_guia_w,'N') = 'S') then
	open C06;
	loop
	fetch C06 into
		nr_seq_ret_item_w;
	exit when C06%notfound;
		begin
		atualizar_valores_ret_item(nr_seq_ret_item_w,nm_usuario_p,'S');
		end;
	end loop;
	close C06;
end if;

dbms_application_info.SET_ACTION('');

exception
	when others then
	dbms_application_info.SET_ACTION('');
	--r.aise_application_error(-20011,sqlerrm);
	wheb_mensagem_pck.exibir_mensagem_abort(191845,'sqlerrm_w='||sqlerrm);
end;

commit;

END Gerar_Retorno_Glosa;
/
