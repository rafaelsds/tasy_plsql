create or replace
procedure ctb_excluir_conta_referencial(	cd_versao_p		number,
						cd_empresa_p		number,
						ie_excluir_vinculo_p	varchar2,
						nm_usuario_p		varchar2) is 

cd_versao_w				conta_contabil_referencial.cd_versao%type;
				
begin


begin
select	decode(cd_versao_p, 0,'X',1,'3.0',2,'3.1')
into	cd_versao_w
from	dual;
exception when others then
	cd_versao_w	:= null;
end;

if	(nvl(ie_excluir_vinculo_p, 'N') = 'S') then
	begin

	delete	conta_contabil_classif_ecd a
	where	nvl(a.cd_versao, 'X') = cd_versao_w
	and	exists (	select	1
					from	conta_contabil_referencial x
					where	nvl(x.cd_versao, 'X') 	= cd_versao_w
					and	x.cd_classificacao	= a.cd_classif_ecd
					and	x.cd_empresa		= cd_empresa_p)
	and	exists(	select	1
			from	conta_contabil y
			where	y.cd_conta_contabil	= a.cd_conta_contabil
			and	y.cd_empresa		= cd_empresa_p);	
	end;
end if;
	
delete	conta_contabil_referencial
where	nvl(cd_versao, 'X') = cd_versao_w
and	cd_empresa	= cd_empresa_p;

commit;

end ctb_excluir_conta_referencial;
/