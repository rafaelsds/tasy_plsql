CREATE OR REPLACE
PROCEDURE Excluir_Prescr_Nao_Lib(
				qt_hora_adicional_p		Number,
				dt_parametro_p			date)	   IS

/* o motivo de baixa deve ser o mesmo utilizado na procedure de baixa das prescrições */

nr_prescricao_w		Number(14,0);

Cursor c010 is
	select	b.nr_prescricao
	from 	prescr_medica b
	where	b.dt_prescricao < (dt_parametro_p - qt_hora_adicional_p / 24)
	and	b.dt_liberacao is null
	and	b.dt_liberacao_medico is null
	and not exists(	select	1
			from	prescr_medica x
			where	b.nr_prescricao	= x.nr_prescricao_anterior)
	and not exists(	select	1
			from	cirurgia c
			where	c.nr_prescricao = b.nr_prescricao);

BEGIN

OPEN C010;
LOOP
FETCH C010 into	
	nr_prescricao_w;
EXIT WHEN C010%NOTFOUND;
	BEGIN
	
	begin
	delete from prescr_medica 
	where nr_prescricao	= nr_prescricao_w;
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;
	end;
	begin
	gravar_log_exclusao('PRESCR_MEDICA','JOB_EPNL','NR_PRESCRICAO : ' || nr_prescricao_w,'N');
	exception
		when others then
			nr_prescricao_w := nr_prescricao_w;
	end;
	END;
END LOOP;
close c010;

commit;
END Excluir_Prescr_Nao_Lib; 
/
