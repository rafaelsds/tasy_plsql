create or replace
procedure COPIAR_DIC_OBJETO_JTREE(
			cd_funcao_p	number,
			nr_seq_objeto_p	number,
			nm_usuario_p	varchar2,
			nr_new_obj_p 	out number) is

nr_seq_objeto_w		number(10,0);
nr_seq_obj_nivel_w	number(10,0);
nr_seq_obj_param_w	number(10,0);

cursor 	c01 is
select	a.nr_sequencia
from	dic_objeto a
where	a.nr_seq_obj_sup = nr_seq_objeto_p;

c01_w	c01%rowtype;

cursor	c02 is
select	a.nr_sequencia
from	dic_objeto a
where	a.nr_seq_obj_sup = c01_w.nr_sequencia;

c02_w	c02%rowtype;

begin
if	(cd_funcao_p is not null) and
	(nr_seq_objeto_p is not null) and
	(nm_usuario_p is not null) then

	select	dic_objeto_seq.nextval
	into	nr_seq_objeto_w
	from	dual;

	insert into dic_objeto (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_objeto,
		nm_objeto,
		cd_funcao,
		cd_exp_texto)
	select	nr_seq_objeto_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_tipo_objeto,
		nm_objeto,
		cd_funcao_p,
		cd_exp_texto
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_p;

	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin

		select	dic_objeto_seq.nextval
		into	nr_seq_obj_nivel_w
		from	dual;

		insert into dic_objeto (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo_objeto,
			nm_objeto,
			cd_funcao,
			nr_seq_obj_sup,
			ds_sql,
			nm_campo_base,
			nm_campo_tela,
			nm_campo_panel,
			ie_momento_sql,
			ie_momento_obj,
			ie_layout_objeto,
			nr_seq_apres,
			ie_recursivo)
		select	nr_seq_obj_nivel_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ie_tipo_objeto,
			nm_objeto,
			cd_funcao_p,
			nr_seq_objeto_w,
			ds_sql,
			nm_campo_base,
			nm_campo_tela,
			nm_campo_panel,
			ie_momento_sql,
			ie_momento_obj,
			ie_layout_objeto,
			nr_seq_apres,
			ie_recursivo
		from	dic_objeto
		where	nr_sequencia = c01_w.nr_sequencia;

		open C02;
		loop
		fetch C02 into
			c02_w;
		exit when C02%notfound;
			begin

			select	dic_objeto_seq.nextval
			into	nr_seq_obj_param_w
			from	dual;

			insert into dic_objeto (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_tipo_objeto,
				nm_objeto,
				cd_funcao,
				nr_seq_obj_sup,
				nm_campo_base,
				nr_seq_apres)
			select	nr_seq_obj_param_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ie_tipo_objeto,
				nm_objeto,
				cd_funcao_p,
				nr_seq_obj_nivel_w,
				nm_campo_base,
				nr_seq_apres
			from	dic_objeto
			where	nr_sequencia = c02_w.nr_sequencia;

			end;
		end loop;
		close C02;

		end;
	end loop;
	close C01;

nr_new_obj_p	:= nr_seq_objeto_w;

end if;

end COPIAR_DIC_OBJETO_JTREE;
/