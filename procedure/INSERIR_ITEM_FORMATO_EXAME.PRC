create or replace
procedure inserir_item_formato_exame(	nr_seq_formato_p	number,
					nr_linha_p		number,
					nr_coluna_p		number,
					nr_seq_exame_p		number,
					nm_usuario_p		varchar2) is 
nr_sequencia_w		number(10);
ie_existe_exame_w	number(10);
					
begin

select	count(*)
into	ie_existe_exame_w
from	exame_lab_format_item
where	nr_seq_formato = nr_seq_formato_p
and	nr_seq_exame = nr_seq_exame_p;

if	(ie_existe_exame_w = 0) then

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	exame_lab_format_item;

	insert into exame_lab_format_item (	
		nr_sequencia,
		nr_seq_formato,      	           
		nr_seq_exame,           
		nr_linha,               
		nr_coluna,              
		dt_atualizacao, 
		nm_usuario)
	values(	nr_sequencia_w,
		nr_seq_formato_p,
		nr_seq_exame_p,
		nr_linha_p,
		nr_coluna_p,
		sysdate,
		nm_usuario_p);

	commit;
end if;
	
end inserir_item_formato_exame;
/