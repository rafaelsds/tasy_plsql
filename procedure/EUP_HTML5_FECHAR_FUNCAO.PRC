create or replace
procedure EUP_HTML5_FECHAR_FUNCAO(cd_convenio_p			number,
					cd_pessoa_fisica_p			varchar2,
					nr_atendimento_p			number,
					nm_usuario_p			varchar2,
					ie_consiste_presc_p  		varchar2,
					ds_msg_abort_p 	      	out 	varchar2,
					ds_msg_urgencia_pa_p  	out	varchar2,
					ds_msg_decl_obito_p	out   	varchar2,
					ds_mensagem_pf_p	  	out   	varchar2,
					ie_atrib_obrig_pf_p	  	out   	varchar2,
					ie_regra_proc_p	  	out	varchar2
) is	

ds_msg_abort_w  		varchar2(255);
ds_msg_urgencia_pa_w 	varchar2(255);
ds_msg_decl_obito_w 	varchar2(255);
ds_mensagem_pf_w 	varchar2(255);
ie_atrib_obrig_pf_w 		varchar2(255);
ie_regra_proc_w 		varchar2(255);
ie_consiste_medico_exec_w 	varchar2(1);
cd_estabelecimento_w 	number(4);

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
Obter_param_Usuario(916, 461, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_consiste_medico_exec_w);

eup_permite_fechar_funcao_js(cd_convenio_p, cd_pessoa_fisica_p, nr_atendimento_p, nm_usuario_p, ds_msg_abort_w, ds_msg_urgencia_pa_w, ds_msg_decl_obito_w, ds_mensagem_pf_w, ie_atrib_obrig_pf_w, ie_regra_proc_w);

ds_msg_abort_p 		:= ds_msg_abort_w;
ds_msg_urgencia_pa_p 	:= ds_msg_urgencia_pa_w;
ds_msg_decl_obito_p 	:= ds_msg_decl_obito_w;
ds_mensagem_pf_p 	:= ds_mensagem_pf_w;
ie_atrib_obrig_pf_p 		:= ie_atrib_obrig_pf_w;
ie_regra_proc_p 		:= ie_regra_proc_w;


if ((ie_consiste_medico_exec_w = 'S') 
  and (ds_msg_abort_w is not null)
  and (instr(ds_msg_abort_w,'[461]') > 0)
  and (ie_consiste_presc_p = 'N')) then	
	ds_msg_abort_p := ''; -- abort only when one prescription have already been viewed. front-end: IE_CONSISTE_PRESC	
end if;

end EUP_HTML5_FECHAR_FUNCAO;
/