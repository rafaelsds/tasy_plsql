create or replace procedure delete_ajust_int_test_pend(nr_seq_intencao_uso_p number,
                                                       ds_version_p          varchar2,
                                                       nm_usuario_p          varchar2) is


  cursor c01 is
    select w.nr_seq_integrated_test, w.nr_sequencia
      from w_test_plan_ignored_int w, reg_integrated_test t
     where w.nr_seq_integrated_test = t.nr_sequencia
       and w.ie_tipo_operacao = 'E'
       and w.nm_usuario = nm_usuario_p
       and w.ds_version = ds_version_p
       and t.nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  cursor c02(nr_seq_int_test_w number) is
    select p.*
      from reg_integrated_test_pend p, reg_integrated_test_cont c
     where p.nr_seq_controle = c.nr_sequencia
       and c.ie_situacao = 'A'
       and c.cd_versao = ds_version_p
       and c.dt_fim is null
       and c.nr_seq_intencao_uso = nr_seq_intencao_uso_p
       and p.nr_seq_integrated_test = nr_seq_int_test_w;

  r01 c01%rowtype;
  r02 c02%rowtype;


begin

  if (nr_seq_intencao_uso_p is not null and ds_version_p is not null and
     nm_usuario_p is not null) then
  
    for r01 in c01 loop
    
      for r02 in c02(r01.nr_seq_integrated_test) loop
      
        delete from reg_integrated_test_so
         where nr_seq_int_test_res in
               (select nr_sequencia
                  from reg_integrated_test_result
                 where nr_seq_pendencia = r02.nr_sequencia);
      
        delete from reg_integrated_test_result
         where nr_seq_pendencia = r02.nr_sequencia;
      
        delete from reg_integrated_test_pend
         where nr_sequencia = r02.nr_sequencia;
      
      end loop;
    
      delete from w_test_plan_ignored_int
       where nr_sequencia = r01.nr_sequencia;
    
    end loop;
  
    commit;
  
  end if;

end delete_ajust_int_test_pend;
/
