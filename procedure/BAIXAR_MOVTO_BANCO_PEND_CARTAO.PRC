create or replace
procedure baixar_movto_banco_pend_cartao
				(nr_seq_movto_pend_p	number,
				dt_baixa_p		date,
				vl_baixa_p		number,				
				nm_usuario_p		varchar2,
				ie_commit_p		varchar2,				
				nr_seq_movto_banco_p	number default null,				
				nr_seq_movto_cartao_p	number default null,
				nr_seq_lote_cartao_p	number default null
				) is 

/* tipo de baixa - ie_tipo_baixa_w
7	Movimenta��o banc�ria
9	Administra��o de Cart�es
10	Administra��o de Cart�es (Baixa em lote)
*/

ie_tipo_baixa_w			number(10);
nr_seq_tipo_baixa_w		number(10);
cd_estabelecimento_w		number(4);
vl_saldo_liquido_w		number(15,2);
nr_seq_parcela_w		number(10);
ie_forma_lanc_lote_cartao_w	varchar2(1);

cursor c01 is
select	a.nr_sequencia,
	a.vl_saldo_liquido
from	movto_cartao_cr_parcela a
where	a.nr_seq_lote	= nr_seq_lote_cartao_p;

begin

if	(nr_seq_movto_pend_p is not null) then

	if	(nr_seq_movto_cartao_p is not null) then
		ie_tipo_baixa_w	:= 9;
		
		select	max(c.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	movto_cartao_cr c,
			movto_cartao_cr_parcela b,
			movto_cartao_cr_baixa a
		where	a.nr_seq_parcela	= b.nr_sequencia
		and	b.nr_seq_movto		= c.nr_sequencia
		and	a.nr_sequencia		= nr_seq_movto_cartao_p;

	elsif	(nr_seq_lote_cartao_p is not null) then	
		ie_tipo_baixa_w	:= 10;
		
		select 	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	lote_baixa_cartao_cr
		where	nr_sequencia	= nr_seq_lote_cartao_p;
		
	end if;
	
	select	max(ie_forma_lanc_lote_cartao)
	into	ie_forma_lanc_lote_cartao_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_w;

	select	max(a.nr_sequencia)
	into	nr_seq_tipo_baixa_w
	from	tipo_baixa_cni a
	where	a.cd_estabelecimento	= nvl(cd_estabelecimento_w,a.cd_estabelecimento)
	and	a.ie_tipo_baixa		= ie_tipo_baixa_w
	and	a.ie_situacao		= 'A';
	
	if	(nr_seq_lote_cartao_p is null) then
	
		insert	into	movto_banco_pend_baixa
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_movto_pend,
			dt_baixa,
			vl_baixa,
			nr_titulo,
			nr_seq_baixa,
			nr_bordero_rec,
			nr_seq_conv_receb,
			nr_adiantamento,
			nr_seq_tipo_baixa,
			nr_seq_cheque,
			nr_seq_movto_trans_fin,
			nr_seq_cartao_baixa,
			nr_seq_lote_cartao,
			nr_seq_parc_cartao)
		values	(movto_banco_pend_baixa_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_movto_pend_p,
			nvl(dt_baixa_p,sysdate),
			vl_baixa_p,
			null,
			null,
			null,
			null,
			null,
			nr_seq_tipo_baixa_w,
			null,
			nr_seq_movto_banco_p,
			nr_seq_movto_cartao_p,
			null,
			null);
	else
		if	(ie_forma_lanc_lote_cartao_w = 'P') then /*Um lan�amento para cada parcela*/		
			open c01;
			loop
			fetch c01 into
				nr_seq_parcela_w,
				vl_saldo_liquido_w;
			exit when c01%notfound;
				insert	into	movto_banco_pend_baixa
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nr_seq_movto_pend,
					dt_baixa,
					vl_baixa,
					nr_titulo,
					nr_seq_baixa,
					nr_bordero_rec,
					nr_seq_conv_receb,
					nr_adiantamento,
					nr_seq_tipo_baixa,
					nr_seq_cheque,
					nr_seq_movto_trans_fin,
					nr_seq_cartao_baixa,
					nr_seq_lote_cartao,
					nr_seq_parc_cartao)
				values	(movto_banco_pend_baixa_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_movto_pend_p,
					nvl(dt_baixa_p,sysdate),
					vl_saldo_liquido_w,
					null,
					null,
					null,
					null,
					null,
					nr_seq_tipo_baixa_w,
					null,
					nr_seq_movto_banco_p,
					null,
					nr_seq_lote_cartao_p,
					nr_seq_parcela_w);
			end loop;
			close c01;
		else		
			insert	into	movto_banco_pend_baixa
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_movto_pend,
				dt_baixa,
				vl_baixa,
				nr_titulo,
				nr_seq_baixa,
				nr_bordero_rec,
				nr_seq_conv_receb,
				nr_adiantamento,
				nr_seq_tipo_baixa,
				nr_seq_cheque,
				nr_seq_movto_trans_fin,
				nr_seq_cartao_baixa,
				nr_seq_lote_cartao,
				nr_seq_parc_cartao)
			values	(movto_banco_pend_baixa_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_movto_pend_p,
				nvl(dt_baixa_p,sysdate),
				vl_baixa_p,
				null,
				null,
				null,
				null,
				null,
				nr_seq_tipo_baixa_w,
				null,
				nr_seq_movto_banco_p,
				null,
				nr_seq_lote_cartao_p,
				null);
		
		end if;

	end if;
		
	atualizar_saldo_movto_bco_pend(nr_seq_movto_pend_p,nm_usuario_p,'N');
end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end baixar_movto_banco_pend_cartao;
/