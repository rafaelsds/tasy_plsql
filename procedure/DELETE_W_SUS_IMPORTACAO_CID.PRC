create or replace
procedure delete_w_sus_importacao_cid (	dt_importacao_p		date,
					nm_usuario_p		varchar2) is 

begin

delete	from w_sus_importacao_cid
where	trunc(dt_importacao) = trunc(dt_importacao_p)
and	nm_usuario = nm_usuario_p;

commit;

end delete_w_sus_importacao_cid;
/
