create or replace
procedure calc_min_dur_agenda_proc_del	(cd_estabelecimento_p	number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					nr_seq_agenda_p		number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_seq_proc_interno_p	number,
					ie_lado_p			varchar2) is

/* variaveis */
cd_agenda_w		number(10,0);
dt_agenda_w		date;
cd_medico_exec_w		varchar2(10);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_minuto_duracao_w	number(10,0);

ie_ajustar_min_w		varchar2(1) 	:= 'N';
ds_ajustar_min_w		varchar2(255)	:= null;
qt_min_proc_w		number(10,0) 	:= 0;
qt_min_agenda_w		number(10,0) 	:= 0;

ie_param_sobreposicao_w	varchar2(1);
ie_sobreposicao_w	varchar2(1);
ie_atualiza_dt_menor_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
ie_lado_w		varchar2(1);
cd_convenio_w		number(5,0);
cd_categoria_w		varchar2(10);
cd_plano_w		varchar2(10);
					
/* obter exame agenda */
cursor c01 is
select	cd_agenda,
	hr_inicio,
	cd_medico_exec,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	nr_minuto_duracao,
	cd_pessoa_fisica,
	ie_lado,
	cd_convenio,
	cd_categoria,
	cd_plano
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p;

/* obter exames adicionais */
cursor c02 is
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.nr_seq_proc_interno,
	a.ie_lado,
	b.cd_convenio,
	b.cd_categoria,
	b.cd_plano
from	agenda_paciente_proc a,
	agenda_paciente b
where	a.nr_sequencia = b.nr_sequencia
and	a.nr_sequencia = nr_seq_agenda_p;
	
begin
if	(cd_estabelecimento_p is not null) and
	(nr_seq_agenda_p is not null) then

	/* obter parametros */
	select	obter_parametro_agenda(cd_estabelecimento_p, 'IE_CONSISTE_DURACAO', 'N')
	into	ie_param_sobreposicao_w
	from	dual;	
	
	obter_param_usuario(820, 129, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_dt_menor_w);

	/* obter duracao exame agenda */
	open c01;
	loop
	fetch c01 into	cd_agenda_w,
			dt_agenda_w,
			cd_medico_exec_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_minuto_duracao_w,
			cd_pessoa_fisica_w,
			ie_lado_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_plano_w;
	exit when c01%notfound;
		begin
		
		select	obter_tempo_duracao_proced(cd_agenda_w, cd_medico_exec_w, cd_procedimento_p, ie_origem_proced_p,cd_pessoa_fisica_w, nr_seq_proc_interno_p, ie_lado_p, cd_convenio_w, cd_categoria_w, cd_plano_w, nr_seq_agenda_p, null)
		into	qt_min_proc_w
		from	dual;
		
		qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
		
		end;
	end loop;
	close c01;
		
	if	(qt_min_agenda_w > 0) and
		((nr_minuto_duracao_w - qt_min_agenda_w) > 0) then

		update	agenda_paciente
		set	nr_minuto_duracao	= nr_minuto_duracao - qt_min_agenda_w
		where	nr_sequencia	= nr_seq_agenda_p;		
		commit;		
		
	end if;
	
	gerar_horario_agenda_exame(cd_estabelecimento_p, cd_agenda_w, dt_agenda_w, nm_usuario_p);
end if;

end calc_min_dur_agenda_proc_del;
/
