create or replace
procedure HSM_GERAR_REPASSE_ESPECIFICO(nr_repasse_terceiro_p	number) is

vl_imposto_w		number(15,2);
nr_sequencia_item_w	number(10);
pr_tributo_w		number(7,4);
vl_item_w		number(15,2);

begin

select	sum(b.vl_imposto) vl_imposto,
	sum(b.pr_tributo) pr_tributo
into	vl_imposto_w,
	pr_tributo_w
from	repasse_terc_venc_trib b,
	repasse_terceiro_venc a
where	b.vl_imposto		> 0
and	a.nr_sequencia		= b.nr_seq_rep_venc
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;

/* c�lculo baseado na f�rmula abaixo, desenvolvida na OS 500493
(base atual + x) - ((base atual + x) * ISS + (base atual + x) * IRRF) = base atual */

vl_item_w	:= nvl(vl_imposto_w,0) / (1 - (nvl(pr_tributo_w,0) / 100));

select	nvl(max(nr_sequencia_item),0) + 1
into	nr_sequencia_item_w
from	repasse_terceiro_item
where	nr_repasse_terceiro	= nr_repasse_terceiro_p;
	
insert	into repasse_terceiro_item
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	vl_repasse,
	nr_repasse_terceiro,
	nr_sequencia_item,
	ds_observacao,
	IE_PARTIC_TRIBUTO)
values	(repasse_terceiro_item_seq.nextval,
	sysdate,
	'Tasy',
	vl_item_w,
	nr_repasse_terceiro_p,
	nr_sequencia_item_w,
	'Repasse gerado pela regra de repasse espec�fico. Procedure: HSM_GERAR_REPASSE_ESPECIFICO',
	'S');

commit;

end	HSM_GERAR_REPASSE_ESPECIFICO;
/
