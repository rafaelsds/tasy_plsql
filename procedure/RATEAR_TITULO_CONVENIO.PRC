create or replace
procedure RATEAR_TITULO_CONVENIO	(nr_seq_retorno_p	in	number,
					nr_titulo_p		in	number,
					nr_seq_cobranca_p	in	number,
					nm_usuario_p		in	varchar2,
					ie_acao_p		in 	varchar2) is
/*	N�O DAR COMMIT NESTA PROCEDURE */

/* ie_acao_p
I - Inclus�o
E - Estorno
*/

vl_desconto_w		number(15,2)	:= 0;
vl_juros_W		number(15,2)	:= 0;
vl_guia_w		number(15,2)	:= 0;
vl_total_w		number(15,2)	:= 0;
vl_total_desc_w		number(15,2)	:= 0;
vl_total_juros_w	number(15,2)	:= 0;
vl_desc_item_w		number(15,2)	:= 0;
vl_juros_item_w		number(15,2)	:= 0;
nr_seq_prot_tit_w	number(10)	:= -1;
nr_interno_conta_tit_w	number(10)	:= -1;
nr_documento_w		number(22)	:= -1;
nr_seq_ret_item_w	number(10)	:= -1;
vl_multa_item_w		number(15,2)	:= 0;
vl_total_multa_w	number(15,2)	:= 0;
vl_multa_w		number(15,2)	:= 0;
vl_nota_credito_w	number(15,2)	:= 0;
vl_nc_item_w		number(15,2)	:= 0;
vl_total_nc_w		number(15,2)	:= 0;
nr_titulo_w		number(10);

/* ahoffelder - 20/05/2010 - t�tulos do retorno -
   coloquei este cursor para o rateio feito pelo Retorno Conv�nio (bt direito Ratear notas de cr�dito para as guias) */
cursor	c01 is
select	distinct
	nvl(a.nr_titulo,to_number(obter_titulo_conta_guia(a.nr_interno_conta,a.cd_autorizacao,a.nr_seq_retorno,null))) nr_titulo
from	convenio_retorno_item a
where	a.nr_seq_retorno	= nr_seq_retorno_p
and	(nr_titulo_p is null or nr_titulo_p =
			nvl(a.nr_titulo,to_number(obter_titulo_conta_guia(a.nr_interno_conta,a.cd_autorizacao,a.nr_seq_retorno,null))));

/* guias do t�tulo */
cursor c02 is
select	nvl(obter_saldo_conpaci(b.nr_interno_conta,a.cd_autorizacao),0),
	a.nr_sequencia
from	conta_paciente_guia b,
	convenio_retorno_item a
where	nvl(a.cd_autorizacao,'N�o Informada') = nvl(b.cd_autorizacao,'N�o Informada')
and	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_seq_retorno	= nr_seq_retorno_p
and	nvl(a.nr_titulo,to_number(obter_titulo_conta_guia(a.nr_interno_conta,a.cd_autorizacao,a.nr_seq_retorno,null)))	= nr_titulo_w
order by	a.vl_guia;

begin

open C01;
loop
fetch C01 into	
	nr_titulo_w;
exit when C01%notfound;

	select	max(a.nr_seq_protocolo),
		max(a.nr_interno_conta),
		max(a.nr_documento)
	into	nr_seq_prot_tit_w,
		nr_interno_conta_tit_w,
		nr_documento_w
	from	titulo_receber a
	where	a.nr_titulo	= nr_titulo_w;

	/* soma dos saldos das guias do t�tulo */
	select	nvl(sum(nvl(vl_guia,0)),0)
	into	vl_total_w
	from	(select	b.cd_autorizacao,
			a.nr_interno_conta,
			nvl(obter_saldo_conpaci(b.nr_interno_conta,a.cd_autorizacao),0) vl_guia
		from	conta_paciente_guia a,
			conta_paciente b
		where	a.nr_interno_conta	= b.nr_interno_conta
		and	b.nr_seq_protocolo	= nr_seq_prot_tit_w
		union
		select	b.cd_autorizacao,
			a.nr_interno_conta,
			nvl(obter_saldo_conpaci(b.nr_interno_conta,a.cd_autorizacao),0) vl_guia
		from	conta_paciente b,
			conta_paciente_guia a
		where	a.nr_interno_conta	= b.nr_interno_conta
		and	a.nr_interno_conta	= nr_interno_conta_tit_w
		and	nvl(nr_seq_prot_tit_w,-1) <> -1
		and	to_char(a.nr_interno_conta)	= nvl(nr_documento_w,to_char(nr_interno_conta_tit_w))
		union
		select	b.cd_autorizacao,
			a.nr_interno_conta,
			nvl(obter_saldo_conpaci(b.nr_interno_conta,a.cd_autorizacao),0) vl_guia
		from	conta_paciente b,
			conta_paciente_guia a
		where	a.nr_interno_conta	= b.nr_interno_conta
		and	a.nr_interno_conta	= nr_interno_conta_tit_w
		and	a.cd_autorizacao	= to_char(nr_documento_w)
		and	nvl(nr_seq_prot_tit_w,-1) <> -1
		and	a.cd_autorizacao	<> to_char(nr_interno_conta_tit_w)
		union
		select	b.cd_autorizacao,
			a.nr_interno_conta,
			nvl(obter_saldo_conpaci(b.nr_interno_conta,a.cd_autorizacao),0) vl_guia
		from	conta_paciente_guia a,
			conta_paciente b
		where	b.nr_interno_conta		= nr_interno_conta_tit_w
		and	b.nr_interno_conta		= a.nr_interno_conta
		and	nvl(nr_seq_prot_tit_w,-1) 	= -1);

	if	(nr_seq_cobranca_p is null) then	/* ahoffelder - OS 267650 - 18/11/2010 */

		select	nvl(to_number(obter_dados_titulo_receber(a.nr_titulo,'VNC')),0)
		into	vl_nota_credito_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_w;

	else	/* desconto e juros da cobran�a */
		select	nvl(max(a.vl_desconto),0) - nvl(to_number(obter_valor_nc_titulo_cobr(max(a.nr_sequencia))),0),
			nvl(max(a.vl_juros),0),
			nvl(max(a.vl_multa),0),
			nvl(to_number(obter_valor_nc_titulo_cobr(max(a.nr_sequencia))),0)
		into	vl_desconto_w,
			vl_juros_w,
			vl_multa_w,
			vl_nota_credito_w
		from	titulo_receber_cobr a
		where	a.nr_seq_cobranca	= nr_seq_cobranca_p
		and	a.nr_titulo		= nr_titulo_w;
	end if;

	if	(ie_acao_p	<> 'I') then
		vl_desconto_w		:= vl_desconto_w * -1;
		vl_juros_w		:= vl_juros_w * -1;
		vl_multa_w		:= vl_multa_w * -1;
		vl_nota_credito_w	:= vl_nota_credito_w * -1;
	end if;

	vl_total_desc_w		:= 0;
	vl_total_juros_w	:= 0;
	vl_total_multa_w	:= 0;
	vl_total_nc_w		:= 0;

	open C02;
	loop
	fetch C02 into	
		vl_guia_w,
		nr_seq_ret_item_w;
	exit when C02%notfound;
		/* para a baixa manual pelo Retorno Conv�nio, atualiza somente o valor da nota de cr�dito */
		if	(nr_seq_cobranca_p is not null) then
			vl_desc_item_w		:= (dividir(vl_desconto_w * vl_guia_w , vl_total_w));
			vl_total_desc_w		:= vl_total_desc_w + vl_desc_item_w;
			vl_juros_item_w		:= (dividir(vl_juros_w * vl_guia_w , vl_total_w));
			vl_total_juros_w	:= vl_total_juros_w + vl_juros_item_w;
			vl_multa_item_w		:= (dividir(vl_multa_w * vl_guia_w , vl_total_w));
			vl_total_multa_w	:= vl_total_multa_w + vl_multa_item_w;

			update	convenio_retorno_item
			set	vl_desconto	= vl_desc_item_w,
				vl_juros_cobr	= vl_juros_item_w,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p,
				vl_pago		= nvl(vl_pago,0) - nvl(vl_desc_item_w,0),
				vl_multa_cobr	= vl_multa_item_w
			where	nr_sequencia	= nr_seq_ret_item_w;
		end if;

		vl_nc_item_w		:= (dividir(vl_nota_credito_w * vl_guia_w , vl_total_w));
		vl_total_nc_w		:= vl_total_nc_w + vl_nc_item_w;

		update	convenio_retorno_item
		set	vl_nota_credito	= vl_nc_item_w,
			vl_pago		= nvl(vl_pago,0) - nvl(vl_nc_item_w,0)
		where	nr_sequencia	= nr_seq_ret_item_w;

	end loop;
	close c02;

	/* adicionar a sobra do arredondamento � �ltima guia do retorno */
	update	convenio_retorno_item
	set	vl_desconto	= nvl(vl_desconto,0) + (nvl(vl_desconto_w,0) - nvl(vl_total_desc_w,0)),
		vl_juros_cobr	= nvl(vl_juros_cobr,0) + (nvl(vl_juros_w,0) - nvl(vl_total_juros_w,0)),
		vl_multa_cobr	= nvl(vl_multa_cobr,0) + (nvl(vl_multa_w,0) - nvl(vl_total_multa_w,0)),
		vl_nota_credito	= nvl(vl_nota_credito,0) + (nvl(vl_nota_credito_w,0) - nvl(vl_total_nc_w,0)),
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		vl_pago		= nvl(vl_pago,0) - (nvl(vl_desconto_w,0) - nvl(vl_total_desc_w,0)) - (nvl(vl_nota_credito_w,0) - nvl(vl_total_nc_w,0))
	where	nr_sequencia	= nr_seq_ret_item_w;

end loop;
close c01;

end RATEAR_TITULO_CONVENIO;
/