create or replace 
procedure gerar_ev_intern_diagnostico(
				nr_atendimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_evento_w			number(10);
cd_estabelecimento_w		number(4);
cd_pessoa_fisica_w		varchar2(10);
qt_idade_w			number(10);
cd_doenca_w			varchar2(20);
ie_tipo_atendimento_W		number(3);
qt_existe_w			number(3);
ie_carater_inter_sus_w		varchar2(10);
cd_medico_resp_w		varchar2(10);
ie_sexo_w			varchar2(5);
ie_clinica_w			number(5);

	
Cursor C01 is	
	select	a.nr_seq_evento
	from	regra_envio_sms a,
		regra_envio_sms_atend b
	where	a.nr_sequencia = b.nr_seq_regra(+)
	and	a.ie_evento_disp = 'ID'
	and	cd_estabelecimento	= cd_estabelecimento_w
	and	qt_idade_w between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,9999)
        and	nvl(a.ie_sexo,ie_sexo_w)  = ie_sexo_w
	and	((cd_doenca is null) or (cd_doenca_w = cd_doenca))
	and	nvl(a.cd_medico,nvl(cd_medico_resp_w,'0')) = nvl(cd_medico_resp_w,'0')
	and	nvl(b.ie_carater_inter_sus,nvl(ie_carater_inter_sus_w,0)) = nvl(ie_carater_inter_sus_w,'0')
	and	nvl(b.ie_tipo_atendimento,nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,'0')
	and	nvl(b.ie_clinica,nvl(ie_clinica_w,0)) = nvl(ie_clinica_w,'0')
	and	nvl(a.ie_situacao,'A') = 'A';		
	

begin


select	cd_pessoa_fisica,
	cd_estabelecimento,
	substr(Obter_cod_Diagnostico_atend(nr_atendimento),1,20),
	ie_tipo_atendimento,
	ie_carater_inter_sus,
	cd_medico_resp,
	Obter_Sexo_PF(cd_pessoa_fisica,'C'),
	ie_clinica
into	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	cd_doenca_w,
	ie_tipo_atendimento_w,
	ie_carater_inter_sus_w,
	cd_medico_resp_w,
	ie_sexo_w,
	ie_clinica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);

select	count(*)
into	qt_existe_w
from 	DIAGNOSTICO_MEDICO
where 	nr_atendimento = nr_atendimento_p;
 
 
  
if	(ie_tipo_atendimento_w = 1) and (qt_existe_w = 1) then
	begin
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_fisica_w,null,nm_usuario_p,null);
		end;
	end loop;
	close C01;
	
	end;
end if;

end gerar_ev_intern_diagnostico;
/
