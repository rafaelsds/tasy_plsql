create or replace
procedure fa_Inserir_Material_Receita (
			nr_atendimento_p		number,
			nr_seq_entrega_p		number,
			nm_usuario_p		varchar2,
			cd_local_estoque_p	number,
			cd_setor_atendimento_p	number,
			nr_seq_lote_fornec_p	number,
			ie_validou_p		out varchar2,
			ie_medic_conta_p	out varchar2 ) is 
			
nr_sequencia_w			number(15);
cd_setor_atendimento_w		number(5);
dt_entrada_unidade_w		date;
cd_unidade_medida_w		varchar2(30);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_seq_atepacu_w			number(10);
dt_cancelamento_w		date;

cd_material_estoque_w		Number(06,0);
qt_conv_estoque_consumo_w	number(13,4);
cd_unidade_medida_estoque_w	varchar2(30);
cd_unidade_medida_consumo_w	varchar2(30);
ie_consignado_w			varchar2(1);
cd_estab_w			number(5);
qt_estoque_w			number(13,4);

cd_material_w			number(10);
cd_material_generico_w	number(10);
qt_material_w			number(13,4);
cd_unidade_baixa_w		varchar2(30);
nr_seq_entrega_item_w		number(10);
nr_atendimento_w			number(10);
ie_status_medicacao_w		varchar2(15);
ie_status_mov_estoque_w		varchar2(2);
qt_material_em_conta_w		number(15,4);
qt_medic_conta_w		number(10);
qt_medic_entrega_w		number(10);
dt_alta_w			date;
ie_atend_com_alta_w		varchar2(1);
nr_seq_lote_fornec_w		number(10);
qt_item_w			number(15,3);
cd_cgc_fornecedor_w		varchar2(14);
cd_material_lote_forn_w		material_lote_fornec.cd_material%type;
ie_regra_lanc_auto_w		varchar2(2);


cursor C01 is
select 	nr_sequencia,
	NVL(cd_material_subst,cd_material) cd_material,
	cd_material_generico,
	NVL(qt_material_subst,qt_dispensar) qt_dispensar,
	nvl(cd_unidade_subst, cd_unidade_baixa) cd_unidade_baixa
from	fa_entrega_medicacao_item
where	nr_seq_fa_entrega = nr_seq_entrega_p
and	dt_cancelamento is null;
	
Cursor C02 is
	select	a.nr_seq_lote,
		a.qt_item,
		b.cd_material
	from	fa_entrega_medic_item_lote a,
		material_lote_fornec b
	where	a.nr_seq_lote = b.nr_sequencia
	and	a.nr_seq_medic_item = nr_seq_entrega_item_w
	and	a.nr_seq_lote is not null;

begin

ie_status_mov_estoque_w	:= Obter_Valor_Param_Usuario(10015, 43, Obter_Perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_atend_com_alta_w	:= Obter_Valor_Param_Usuario(10015, 99, Obter_Perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_regra_lanc_auto_w	:= Obter_Valor_Param_Usuario(10015, 122, Obter_Perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

nr_atendimento_w := nr_atendimento_p;
nr_seq_lote_fornec_w := nr_seq_lote_fornec_p;

if (nr_atendimento_w = 0) then
	SELECT 	MAX(nvl(b.nr_atendimento,0))
	into	nr_atendimento_w
	FROM   	fa_paciente_entrega b,
		fa_entrega_medicacao a
	WHERE  	b.nr_sequencia = a.nr_seq_paciente_entrega
	AND	a.nr_sequencia = nr_seq_entrega_p;
	
end if;


SELECT 	MAX(c.dt_cancelamento),
	max(a.ie_status_medicacao)
INTO	dt_cancelamento_w,
	ie_status_medicacao_w
FROM   	fa_receita_farmacia c,
	fa_paciente_entrega b,
	fa_entrega_medicacao a
WHERE  	b.nr_sequencia = a.nr_seq_paciente_entrega
AND	a.nr_seq_receita_amb = c.nr_sequencia
AND	a.nr_sequencia = nr_seq_entrega_p;


gravar_log_nutricao(85458,'1 - nr_seq_entrega_p = '||nr_seq_entrega_p||' nr_atendimento_w = '||nr_atendimento_w||' cd_local_estoque = '||cd_local_estoque_p||
			' cd_setor_atendimento_p = '||cd_setor_atendimento_p||' ie_status_mov_estoque_w = '||ie_status_mov_estoque_w||
			' ie_status_medicacao_w = '||ie_status_medicacao_w||' nr_seq_atepacu_w = '||OBTER_ATEPACU_PACIENTE(nr_atendimento_w, 'A'),nm_usuario_p);

commit;				
if	(ie_status_medicacao_w = 'EM') and (ie_status_mov_estoque_w = 'EM') then
	-- Esta entrega ja esta acom status Entrega da medicacao. Favor atualizar a tela!
	Wheb_mensagem_pck.exibir_mensagem_abort(181172);
end if;

if	(ie_status_medicacao_w = 'CM') and (ie_status_mov_estoque_w = 'CM') then	
	-- Esta entrega ja esta com status Conferencia da medicacao. Favor atualizar a tela!
	Wheb_mensagem_pck.exibir_mensagem_abort(181173);
end if;

if	(dt_cancelamento_w is not null) then
	-- Esta receita ja foi cancelada.
	Wheb_mensagem_pck.exibir_mensagem_abort(181174);
end if;

if (nr_atendimento_w = 0) then	
	-- Receita sem atendimento vinculado.
	Wheb_mensagem_pck.exibir_mensagem_abort(181175);
end if;

nr_seq_atepacu_w	:= OBTER_ATEPACU_PACIENTE(nr_atendimento_w, 'A');
cd_convenio_w		:= OBTER_CONVENIO_ATENDIMENTO(nr_atendimento_w);
cd_categoria_w		:= Obter_Categoria_Atendimento(nr_atendimento_w);

select 	max(cd_estabelecimento),
	max(dt_alta)
into	cd_estab_w,
	dt_alta_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

if (ie_atend_com_alta_w = 'N') and (dt_alta_w is not null) then
	-- O atendimento ja teve alta. Nao e possivel baixar os itens. Parametro [99].
	Wheb_mensagem_pck.exibir_mensagem_abort(221941);
end if;

if (nr_seq_atepacu_w <> 0) then
	select	cd_setor_Atendimento,
		dt_entrada_unidade
	into	cd_setor_atendimento_w,
		dt_entrada_unidade_w
	from	atend_paciente_unidade
	where	nr_seq_interno	= nr_seq_atepacu_w;
else	
	-- Nao existe uma passagem de setor para o atendimento.
	Wheb_mensagem_pck.exibir_mensagem_abort(181177);
end if;
	
open c01;
loop
fetch c01 into
	nr_seq_entrega_item_w,
	cd_material_w,
	cd_material_generico_w,
	qt_material_w,
	cd_unidade_baixa_w;
exit when c01%notfound;
	begin
	
	nr_seq_lote_fornec_w 	:= null;
	cd_cgc_fornecedor_w	:= null;
	
	select	nvl(sum(qt_material),0)
	into	qt_material_em_conta_w
	from	material_atend_paciente
	where	nr_seq_entrega_medic_fa = nr_seq_entrega_item_w;
	
	gravar_log_nutricao(85458,'2 - nr_seq_entrega_p = '||nr_seq_entrega_p||' nr_atendimento_w = '||nr_atendimento_w||' nr_seq_entrega_item_w = '||nr_seq_entrega_item_w||
			' cd_material_w = '||cd_material_w||' qt_material_w = '||qt_material_w||' qt_material_em_conta_w = '||qt_material_em_conta_w||' cd_local_estoque = '||cd_local_estoque_p||
			' cd_setor_atendimento_p = '||cd_setor_atendimento_p||' ie_status_mov_estoque_w = '||ie_status_mov_estoque_w,nm_usuario_p);
	
	if (qt_material_em_conta_w = 0) then
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_lote_fornec_w,
			qt_item_w,
			cd_material_lote_forn_w;
		exit when C02%notfound;
			begin
			
			select	substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo
			into	cd_unidade_medida_w
			from	material
			where	cd_material	= cd_material_lote_forn_w;

			select	nvl(ie_consignado,'0'),
				cd_material_estoque,
				qt_conv_estoque_consumo,
				substr(obter_dados_material_estab(cd_material,cd_estab_w,'UME'),1,30) cd_unidade_medida_estoque,
				substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo
			into	ie_consignado_w, 
				cd_material_estoque_w,
				qt_conv_estoque_consumo_w,
				cd_unidade_medida_estoque_w,
				cd_unidade_medida_consumo_w
			from	material
			where	cd_material = cd_material_lote_forn_w;
			
			select	material_atend_paciente_seq.nextval
			into	nr_sequencia_w
			from	dual;

			qt_material_w := qt_material_w - qt_item_w;
			
			if (cd_unidade_baixa_w <> cd_unidade_medida_w) then
				qt_item_w := obter_dose_convertida(cd_material_lote_forn_w,qt_item_w,cd_unidade_baixa_w,cd_unidade_medida_w);
			end if;
						
			gravar_log_nutricao(85458,'3 - nr_seq_entrega_p = '||nr_seq_entrega_p||' cd_material_lote_forn_w = '||cd_material_lote_forn_w||' nr_atendimento_w = '||nr_atendimento_w||
						' nr_seq_entrega_item_w = '||nr_seq_entrega_item_w||' qt_estoque_w = '||qt_estoque_w||' cd_local_estoque = '||cd_local_estoque_p||
						' nr_seq_atepacu_w = '||nr_seq_atepacu_w||' cd_setor_atendimento_p = '||cd_setor_atendimento_p||
						' ie_status_mov_estoque_w = '||ie_status_mov_estoque_w||' qt_material_w = '||qt_material_w,nm_usuario_p);
			
			if	(qt_item_w > 0) then
				if	(nvl(cd_local_estoque_p, 0) > 0) then
					qt_estoque_w	:= obter_saldo_disp_estoque(cd_estab_w,cd_material_estoque_w,cd_local_estoque_p,trunc(sysdate,'mm'),nr_seq_lote_fornec_w);
					
					if	(nvl(qt_estoque_w,0) < dividir(qt_item_w, qt_conv_estoque_consumo_w)) then
						ie_validou_p := 'N';
						/*Nao existe Saldo de Estoque:
						Material: #@CD_MATERIAL#@
						Quantidade em Estoque : #@QT_ESTOQUE#@
						Quantidade a baixar: #@QT_MATERIAL#@
						Local de estoque: #@DS_LOCAL_ESTOQUE#@ */
						Wheb_mensagem_pck.exibir_mensagem_abort(181184, 'CD_MATERIAL='||to_char(cd_material_lote_forn_w)||';'||
												'QT_ESTOQUE='||to_char(qt_estoque_w)||';'||
												'QT_MATERIAL='||to_char(qt_item_w)||';'||
												'DS_LOCAL_ESTOQUE='||substr(OBTER_DESC_LOCAL_ESTOQUE(cd_local_estoque_p),1,100));		
					end if;

				end if;
				
				cd_cgc_fornecedor_w := Obter_dados_lote_fornec(nr_seq_lote_fornec_w,'CF');
				
			/*	insert into log_xxxxxxxtasy(cd_log,ds_log,dt_atualizacao,nm_usuario)
				values	(78548, 
					'nr_seq_entrega_p = '||nr_seq_entrega_p||' cd_material_w = '||cd_material_w||' nr_atendimento_w = '||nr_atendimento_w||' nr_seq_entrega_item_w = '||nr_seq_entrega_item_w||' qt_estoque_w = '||qt_estoque_w||' cd_local_estoque = '||cd_local_estoque_p||' nr_seq_atepacu_w = '||nr_seq_atepacu_w||' cd_setor_atendimento_p = '||cd_setor_atendimento_p||' ie_status_mov_estoque_w = '||ie_status_mov_estoque_w,
					sysdate,
					nm_usuario_p);  */
				
				insert into material_atend_paciente(	nr_sequencia,
									cd_material,
									dt_atendimento,
									cd_convenio,
									cd_categoria,
									nr_seq_atepacu,
									cd_setor_atendimento,
									dt_entrada_unidade,
									qt_material,
									cd_local_estoque,
									dt_Atualizacao,
									nm_usuario,
									nr_atendimento,
									cd_unidade_medida,
									cd_acao,
									ie_valor_informado,
									nr_seq_lote_fornec,
									nr_seq_entrega_medic_fa,
									cd_cgc_fornecedor)
							values	(	nr_sequencia_w,
									cd_material_lote_forn_w,
									sysdate,
									cd_convenio_w,
									cd_categoria_w,
									nr_seq_atepacu_w,
									cd_setor_atendimento_w,
									dt_entrada_unidade_w,
									qt_item_w,
									cd_local_estoque_p,
									sysdate,
									nm_usuario_p,
									nr_atendimento_w,
									cd_unidade_medida_w,
									'1',
									'N',
									nr_seq_lote_fornec_w,
									nr_seq_entrega_item_w,
									cd_cgc_fornecedor_w
									);

				atualiza_preco_material(nr_sequencia_w,nm_usuario_p);
				
				if	(ie_regra_lanc_auto_w = 'S') then
					gerar_lanc_automatico_mat(nr_atendimento_w,cd_local_estoque_P,132,nm_usuario_p, nr_sequencia_w,null,null);
				end if;
				
				commit;
			end if;
			end;
		end loop;
		close C02;
		
		nr_seq_lote_fornec_w 	:= null;
		cd_cgc_fornecedor_w	:= null;
		
		select	substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo
		into	cd_unidade_medida_w
		from	material
		where	cd_material	= cd_material_w;

		select	nvl(ie_consignado,'0'),
			cd_material_estoque,
			qt_conv_estoque_consumo,
			substr(obter_dados_material_estab(cd_material,cd_estab_w,'UME'),1,30) cd_unidade_medida_estoque,
			substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo
		into	ie_consignado_w, 
			cd_material_estoque_w,
			qt_conv_estoque_consumo_w,
			cd_unidade_medida_estoque_w,
			cd_unidade_medida_consumo_w
		from	material
		where	cd_material = NVL(cd_material_generico_w,cd_material_w);
            
		if	(qt_material_w > 0) then
			
			select	material_atend_paciente_seq.nextval
			into	nr_sequencia_w
			from	dual;

			if (cd_unidade_baixa_w <> cd_unidade_medida_w) then
				qt_material_w := obter_dose_convertida(cd_material_w,qt_material_w,cd_unidade_baixa_w,cd_unidade_medida_w);
			end if;
						
			if	(nvl(cd_local_estoque_p, 0) > 0) then
				qt_estoque_w	:= obter_saldo_disp_estoque(cd_estab_w,cd_material_estoque_w,cd_local_estoque_p,trunc(sysdate,'mm'));
				
			gravar_log_nutricao(85458,'4 - nr_seq_entrega_p = '||nr_seq_entrega_p||' cd_material_w = '||cd_material_w||' nr_atendimento_w = '||nr_atendimento_w||
						' nr_seq_entrega_item_w = '||nr_seq_entrega_item_w||' qt_estoque_w = '||qt_estoque_w||' cd_local_estoque = '||cd_local_estoque_p||
						' nr_seq_atepacu_w = '||nr_seq_atepacu_w||' cd_setor_atendimento_p = '||cd_setor_atendimento_p||
						' ie_status_mov_estoque_w = '||ie_status_mov_estoque_w||' qt_material_w = '||qt_material_w,nm_usuario_p);
		
				if	(nvl(qt_estoque_w,0) < dividir(qt_material_w, qt_conv_estoque_consumo_w)) then
					ie_validou_p := 'N';
					/*Nao existe Saldo de Estoque:
					Material: #@CD_MATERIAL#@
					Quantidade em Estoque : #@QT_ESTOQUE#@
					Quantidade a baixar: #@QT_MATERIAL#@
					Local de estoque: #@DS_LOCAL_ESTOQUE#@ */
					Wheb_mensagem_pck.exibir_mensagem_abort(181184, 'CD_MATERIAL='||to_char(cd_material_w)||';'||
											'QT_ESTOQUE='||to_char(qt_estoque_w)||';'||
											'QT_MATERIAL='||to_char(qt_material_w)||';'||
											'DS_LOCAL_ESTOQUE='||substr(OBTER_DESC_LOCAL_ESTOQUE(cd_local_estoque_p),1,100));		
				end if;

			end if;		
		
		/*
			insert into log_xxxxtasy(cd_log,ds_log,dt_atualizacao,nm_usuario)
			values	(78548, 
				'nr_seq_entrega_p = '||nr_seq_entrega_p||' cd_material_w = '||cd_material_w||' nr_atendimento_w = '||nr_atendimento_w||' nr_seq_entrega_item_w = '||nr_seq_entrega_item_w||' qt_estoque_w = '||qt_estoque_w||' cd_local_estoque = '||cd_local_estoque_p||' nr_seq_atepacu_w = '||nr_seq_atepacu_w||' cd_setor_atendimento_p = '||cd_setor_atendimento_p||' ie_status_mov_estoque_w = '||ie_status_mov_estoque_w,
				sysdate,
				nm_usuario_p); */
		
			insert into material_atend_paciente(	nr_sequencia,
								cd_material,
								cd_material_exec,
								dt_atendimento,
								cd_convenio,
								cd_categoria,
								nr_seq_atepacu,
								cd_setor_atendimento,
								dt_entrada_unidade,
								qt_material,
								cd_local_estoque,
								dt_Atualizacao,
								nm_usuario,
								nr_atendimento,
								cd_unidade_medida,
								cd_acao,
								ie_valor_informado,
								nr_seq_lote_fornec,
								nr_seq_entrega_medic_fa)
						values	(	nr_sequencia_w,
								cd_material_w,
								cd_material_generico_w,
								sysdate,
								cd_convenio_w,
								cd_categoria_w,
								nr_seq_atepacu_w,
								cd_setor_atendimento_w,
								dt_entrada_unidade_w,
								qt_material_w,
								cd_local_estoque_p,
								sysdate,
								nm_usuario_p,
								nr_atendimento_w,
								cd_unidade_medida_w,
								'1',
								'N',
								nr_seq_lote_fornec_w,
								nr_seq_entrega_item_w
								);

			atualiza_preco_material(nr_sequencia_w,nm_usuario_p);
			
			if	(ie_regra_lanc_auto_w = 'S') then
				gerar_lanc_automatico_mat(nr_atendimento_w,cd_local_estoque_P,132,nm_usuario_p, nr_sequencia_w,null,null);
			end if;
			
			commit;
		end if;
	end if;
	end;
end loop;
close C01;
ie_validou_p := 'S';

commit;

select	sum(a.qt_material)
into	qt_medic_conta_w
from	material_atend_paciente a,
	fa_entrega_medicacao_item b
where	a.nr_seq_entrega_medic_fa = b.nr_sequencia
and	b.nr_seq_fa_entrega = nr_seq_entrega_p
and	dt_cancelamento is null;

select	sum(qt_dispensar)
into	qt_medic_entrega_w
from	fa_entrega_medicacao_item
where	nr_seq_fa_entrega = nr_seq_entrega_p
and	dt_cancelamento is null
and	qt_dispensar > 0;

if	(qt_medic_conta_w <> qt_medic_entrega_w) then
	ie_medic_conta_p := 'N';
else
	ie_medic_conta_p := 'S';
end if;

end fa_Inserir_Material_Receita;
/

