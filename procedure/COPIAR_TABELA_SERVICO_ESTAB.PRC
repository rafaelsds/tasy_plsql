create or replace
procedure copiar_tabela_servico_estab
			(	cd_estab_origem_p	number,
				cd_estab_destino_p	number,
				nm_usuario_p		Varchar2) is 

cd_tabela_ant_w			number(10);

cd_tabela_servico_w		number(4);
cd_procedimento_w		number(15);
dt_inicio_vigencia_w		date;
qt_registro_w			number(10);

Cursor C01 is
	select	cd_tabela_servico
	from	tabela_servico
	where	cd_estabelecimento	= cd_estab_origem_p;
	
Cursor C02 is
	select	cd_tabela_servico,
		cd_procedimento,
		dt_inicio_vigencia
	from	preco_servico
	where	cd_tabela_servico = cd_tabela_ant_w
	and	cd_estabelecimento = cd_estab_origem_p;
	
begin
open C01;
loop
fetch C01 into	
	cd_tabela_ant_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_registro_w
	from	tabela_servico
	where	cd_estabelecimento	= cd_estab_destino_p
	and	cd_tabela_servico	= cd_tabela_ant_w;

	if	(qt_registro_w = 0) then
		insert into  tabela_servico
			(cd_estabelecimento, cd_tabela_servico, ds_tabela_servico, 
			ie_situacao, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tiss_tabela,
			ie_hosp_pls)
		select	cd_estab_destino_p, cd_tabela_ant_w, ds_tabela_servico, 
			ie_situacao, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p,nr_seq_tiss_tabela,
			ie_hosp_pls
		from	tabela_servico
		where	cd_tabela_servico = cd_tabela_ant_w
		and	cd_estabelecimento = cd_estab_origem_p;
		open C02;
		loop
		fetch C02 into	
			cd_tabela_servico_w,
			cd_procedimento_w,
			dt_inicio_vigencia_w;
		exit when C02%notfound;
			begin
			insert into  preco_servico
				(cd_estabelecimento, cd_tabela_servico, cd_procedimento, 
				dt_inicio_vigencia, vl_servico, cd_moeda, 
				dt_atualizacao, nm_usuario, ie_origem_proced, 
				cd_unidade_medida, dt_atualizacao_nrec, nm_usuario_nrec, 
				dt_inativacao, dt_vigencia_final)
			select	cd_estab_destino_p, cd_tabela_ant_w, cd_procedimento, 
				dt_inicio_vigencia, vl_servico, cd_moeda, 
				sysdate, nm_usuario_p, ie_origem_proced, 
				cd_unidade_medida, sysdate, nm_usuario_p, 
				dt_inativacao, dt_vigencia_final
			from	preco_servico
			where	cd_estabelecimento	= cd_estab_origem_p
			and	cd_tabela_servico	= cd_tabela_servico_w
			and	cd_procedimento		= cd_procedimento_w
			and	dt_inicio_vigencia	= dt_inicio_vigencia_w;
			end;
		end loop;
		close C02;	
	end if;
	end;
end loop;
close C01;
commit;

end copiar_tabela_servico_estab;
/
