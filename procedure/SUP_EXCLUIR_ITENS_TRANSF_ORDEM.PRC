create or replace
procedure sup_excluir_itens_transf_ordem(	nr_ordem_compra_p	number,
					nr_seq_ordem_prod_p	number,
					nm_usuario_p		Varchar2) is 
begin

delete	transf_ordem_prod
where	nr_ordem_compra 	= nr_ordem_compra_p
and	nr_seq_ordem_prod	= nr_seq_ordem_prod_p;

delete	ordem_compra_item
where	nr_ordem_compra 	= nr_ordem_compra_p
and	nr_seq_ordem_prod	= nr_seq_ordem_prod_p;

commit;

end sup_excluir_itens_transf_ordem;
/