create or replace
procedure pls_copiar_dados_titular_prop
			(	nr_seq_beneficiario_p	number,
				nr_seq_titular_p	number,
				ie_bonificacao_p	varchar2,
				ie_sca_p		varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_vinculo_sca_w		number(10);
nr_seq_vinculo_bonif_w		number(10);
nr_seq_plano_sca_w		number(10);
nr_seq_tabela_w			number(10);
ds_erro_w			varchar2(255);
qt_sca_w			number(2);
			
Cursor C01 is
	select	nr_sequencia,
		nr_seq_plano,
		nr_seq_tabela
	from	pls_sca_vinculo
	where	nr_seq_benef_proposta	= nr_seq_titular_p
	and	ie_sca_p		= 'S'
	order by nr_sequencia;
	
Cursor C02 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_segurado_prop	= nr_seq_titular_p
	and	ie_bonificacao_p	= 'S'
	order by nr_sequencia;
		
begin

open C01;
loop
fetch C01 into	
	nr_seq_vinculo_sca_w,
	nr_seq_plano_sca_w,
	nr_seq_tabela_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_sca_w
	from	pls_sca_vinculo
	where	nr_seq_benef_proposta = nr_seq_beneficiario_p
	and	nr_seq_plano = nr_seq_plano_sca_w;
	
	ds_erro_w	:= '';
	
	pls_consistir_sca_proposta(nr_seq_beneficiario_p,nr_seq_plano_sca_w,nr_seq_tabela_w,wheb_usuario_pck.get_cd_estabelecimento,ds_erro_w,nm_usuario_p);
	
	if	(qt_sca_w = 0) then
		if	(ds_erro_w is null) then
			insert into pls_sca_vinculo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_benef_proposta,nr_seq_plano,nr_seq_tabela,nr_seq_vendedor_canal,nr_seq_vendedor_pf,
					qt_idade_limite,dt_inicio_vigencia)
				(select	pls_sca_vinculo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_beneficiario_p,nr_seq_plano,nr_seq_tabela,nr_seq_vendedor_canal,nr_seq_vendedor_pf,
					qt_idade_limite,dt_inicio_vigencia
				from	pls_sca_vinculo
				where	nr_sequencia	= nr_seq_vinculo_sca_w);
		end if;
	end if;
	end;
end loop;
close C01;


open C02;
loop
fetch C02 into	
	nr_seq_vinculo_bonif_w;
exit when C02%notfound;
	begin
	
	insert into pls_bonificacao_vinculo
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			nr_seq_segurado_prop,nr_seq_bonificacao,dt_inicio_vigencia,dt_fim_vigencia,tx_bonificacao,
			vl_bonificacao)
		(select	pls_bonificacao_vinculo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			nr_seq_beneficiario_p,nr_seq_bonificacao,dt_inicio_vigencia,dt_fim_vigencia,tx_bonificacao,
			vl_bonificacao
		from	pls_bonificacao_vinculo
		where	nr_sequencia	= nr_seq_vinculo_bonif_w);
	
	end;
end loop;
close C02;

commit;

end pls_copiar_dados_titular_prop;
/