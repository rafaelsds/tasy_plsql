create or replace
procedure enviar_movto_pendente_trans(	nr_movimento_estoque_p	number,
					nm_usuario_p		varchar2) is


cd_sistema_ant_w			varchar2(20);
cd_centro_custo_ww		varchar2(20);
cd_unidade_medida_ww		varchar2(30);
cd_conta_contabil_ww		varchar2(30);
ie_entrada_saida_w			varchar2(1);
cd_setor_entrega_w		number(5);

cd_operacao_estoque_w		movimento_estoque.cd_operacao_estoque%type;
cd_material_w			movimento_estoque.cd_material%type;
ie_origem_documento_w		movimento_estoque.ie_origem_documento%type;
nr_documento_w			movimento_estoque.nr_documento%type;
cd_conta_contabil_w		movimento_estoque.cd_conta_contabil%type;
cd_unidade_medida_estoque_w	movimento_estoque.cd_unidade_medida_estoque%type;
cd_centro_custo_w			movimento_estoque.cd_centro_custo%type;
dt_movimento_estoque_w		movimento_estoque.dt_movimento_estoque%type;
vl_movimento_w			movimento_estoque.vl_movimento%type;
qt_estoque_w			movimento_estoque.qt_estoque%type;
cd_acao_w			movimento_estoque.cd_acao%type;

ds_erro_w			varchar2(2000);

begin

select  cd_operacao_estoque,
        cd_material,
        ie_origem_documento,
        nr_documento,
        cd_conta_contabil,
        cd_unidade_medida_estoque,
        cd_centro_custo,
        dt_movimento_estoque,
        vl_movimento,
        qt_estoque,
	cd_acao
into    cd_operacao_estoque_w,
        cd_material_w,
        ie_origem_documento_w,
        nr_documento_w,
        cd_conta_contabil_w,
        cd_unidade_medida_estoque_w,
        cd_centro_custo_w,
        dt_movimento_estoque_w,
        vl_movimento_w,
        qt_estoque_w,
	cd_acao_w
from    movimento_estoque
where   nr_movimento_estoque = nr_movimento_estoque_p;

select  ie_entrada_saida
into    ie_entrada_saida_w
from    operacao_estoque
where   cd_operacao_estoque = cd_operacao_estoque_w;

if	(cd_acao_w = '2') then
	select	decode(ie_entrada_saida_w,'E','S','S','E')
	into	ie_entrada_saida_w
	from	dual;
end if;

select  nvl(substr(cd_sistema_ant,1,10),cd_material_w)
into    cd_sistema_ant_w
from    material
where   cd_material = cd_material_w;

if      (ie_origem_documento_w = 2) and
        (nr_documento_w > 0) then
        select  max(cd_setor_entrega)
        into    cd_setor_entrega_w
        from    requisicao_material
        where   nr_requisicao = nr_documento_w;
end if;

if      (cd_conta_contabil_w is not null) then
        select  nvl(cd_sistema_contabil,cd_conta_contabil)
        into    cd_conta_contabil_ww
        from    conta_contabil
        where   cd_conta_contabil = cd_conta_contabil_w;
end if;

if      (cd_unidade_medida_estoque_w is not null) then
        select  nvl(cd_sistema_ant,cd_unidade_medida)
        into    cd_unidade_medida_ww
        from    unidade_medida
        where   cd_unidade_medida = cd_unidade_medida_estoque_w;
end if;

if      (cd_centro_custo_w > 0) then
        select  nvl(cd_sistema_contabil,cd_centro_custo)
        into    cd_centro_custo_ww
        from    centro_custo
        where   cd_centro_custo = cd_centro_custo_w;
end if;

insert into w_protheus_movto_estoque(
                nr_sequencia,
                ie_tipo_movimento,
                dt_movimento_estoque,
                cd_material,
                cd_centro_custo,
                vl_movimento,
                qt_movimento,
                nr_movimento_estoque,
                dt_atualizacao,
                dt_atualizacao_nrec,
                nm_usuario,
                nm_usuario_nrec,
                cd_unidade_medida_mov,
                cd_conta_contabil,
                ie_entrada_saida,
                ie_status,
                cd_setor_entrega,
		ds_observacao)
        values( w_protheus_movto_estoque_seq.nextval,
                'R',
                dt_movimento_estoque_w,
                cd_sistema_ant_w,
                cd_centro_custo_ww,
                vl_movimento_w,
                qt_estoque_w,
                nr_movimento_estoque_P,
                sysdate,
                sysdate,
                nm_usuario_p,
                nm_usuario_p,
                cd_unidade_medida_ww,
                cd_conta_contabil_ww,
                ie_entrada_saida_w,
                'P',
                cd_setor_entrega_w,
		wheb_mensagem_pck.get_texto(325648));

exception when others then
        begin
        ds_erro_w := substr(sqlerrm,1,2000);

        insert into w_protheus_movto_estoque(
                nr_sequencia,
                ie_tipo_movimento,
                dt_movimento_estoque,
                cd_material,
                cd_centro_custo,
                vl_movimento,
                qt_movimento,
                nr_movimento_estoque,
                dt_atualizacao,
                dt_atualizacao_nrec,
                nm_usuario,
                nm_usuario_nrec,
                cd_unidade_medida_mov,
                cd_conta_contabil,
                ie_entrada_saida,
                ie_status,
                cd_setor_entrega,
		ds_observacao)
        values( w_protheus_movto_estoque_seq.nextval,
                'R',
                dt_movimento_estoque_w,
                cd_sistema_ant_w,
                cd_centro_custo_ww,
                vl_movimento_w,
                qt_estoque_w,
                nr_movimento_estoque_p,
                sysdate,
                sysdate,
                nm_usuario_p,
                nm_usuario_p,
                cd_unidade_medida_ww,
                cd_conta_contabil_ww,
                ie_entrada_saida_w,
                'E',
                cd_setor_entrega_w,
		wheb_mensagem_pck.get_texto(325648));
        end;

commit;

end enviar_movto_pendente_trans;
/