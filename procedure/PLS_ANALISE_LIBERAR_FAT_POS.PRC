create or replace
procedure pls_analise_liberar_fat_pos
			(	nr_seq_analise_p		number,
				nr_seq_w_item_p			number,
				nr_seq_mot_liberacao_p	number,
				nr_seq_grupo_atual_p	number,
				ds_observacao_p			varchar2,
				nr_id_transacao_p		w_pls_analise_item.nr_id_transacao%type,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p	number
				) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_novo_pos_estab_w		pls_visible_false.ie_novo_pos_estab%type;

Cursor C01 is
	select	decode(a.nr_seq_conta_mat, null, 'P', 'M') 	ie_tipo_item,
		a.nr_sequencia					nr_seq_w_item,					
		a.nr_seq_conta,	                                
		a.nr_seq_conta_proc,                            	
		a.nr_seq_conta_mat,                             	
		a.nr_seq_proc_partic,                           	
		a.ie_selecionado,                               	
		a.nr_seq_conta_pos_estab                      	
	from	w_pls_analise_item	a
	where	a.nr_seq_analise	= nr_seq_analise_p
	and	a.ie_selecionado	= 'S'
	and	a.ie_status_analise	<> 'I'
	and a.nr_id_transacao = nr_id_transacao_p
	union all
	select	decode(a.nr_seq_conta_mat, null, 'P', 'M') 	ie_tipo_item,
		a.nr_sequencia					nr_seq_w_item,
		a.nr_seq_conta,
		a.nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		a.nr_seq_proc_partic,
		a.ie_selecionado,
		a.nr_seq_conta_pos_estab
	from	w_pls_analise_item	a
	where	a.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_sequencia	  = nr_seq_w_item_p
	and a.nr_id_transacao = nr_id_transacao_p
	and	(a.ie_selecionado = 'N' or a.ie_selecionado is null)
	and	a.ie_status_analise	<> 'I'
	order by
		ie_selecionado;

begin

select 	nvl(max(ie_novo_pos_estab),'N')
into	ie_novo_pos_estab_w
from	pls_visible_false;

for r_c01_w in C01 loop
	
	if	(ie_novo_pos_estab_w = 'N') then
		pls_analise_lib_fat_total_pos(nr_seq_analise_p, r_c01_w.nr_seq_conta_pos_estab, r_c01_w.nr_seq_conta_proc, r_c01_w.nr_seq_conta_mat, r_c01_w.nr_seq_proc_partic,
						nr_seq_mot_liberacao_p, ds_observacao_p, cd_estabelecimento_p, nr_seq_grupo_atual_p, nm_usuario_p);
	else
		pls_an_lib_fat_total_pos_estab(nr_seq_analise_p, r_c01_w.nr_seq_conta_pos_estab, r_c01_w.nr_seq_conta_proc, r_c01_w.nr_seq_conta_mat, r_c01_w.nr_seq_proc_partic,
						nr_seq_mot_liberacao_p, ds_observacao_p, cd_estabelecimento_p, nr_seq_grupo_atual_p, r_c01_w.ie_tipo_item, nm_usuario_p);
						
	end if;				
		
	pls_gerar_w_analise_selec_item(nr_seq_analise_p, r_c01_w.nr_seq_w_item, null, 'D', nm_usuario_p, 'N', nr_id_transacao_p);
					
end loop;

commit;

end pls_analise_liberar_fat_pos;
/
