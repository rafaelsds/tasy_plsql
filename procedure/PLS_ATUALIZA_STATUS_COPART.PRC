create or replace
procedure pls_atualiza_status_copart (				
			nr_seq_copart_p		pls_conta_coparticipacao.nr_seq_conta%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualiza o status da conta de coparticipação para a cobrança da mensalidade
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicionário [x] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de atenção:

Alterações:
-------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


begin
if	(nr_seq_copart_p is not null) then
	update	pls_conta_coparticipacao
	set	ie_status_mensalidade = 'L',
		ie_status_coparticipacao = 'S',
		ie_gerar_mensalidade = 'S'
	where	nr_sequencia = nr_seq_copart_p;
	
	commit;
end if;

end pls_atualiza_status_copart;
/