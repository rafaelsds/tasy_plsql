create or replace
procedure gerar_itens_escala_sepse(		nr_seq_escala_p		number,
										nr_atendimento_p	number,
										nm_usuario_p		varchar2,
										ie_atualizar_p		varchar2 default 'S') is 

nr_seq_atrib_w		number(10);
qt_pontuacao_w		number(10);
qt_valor_min_w		number(15,4);
qt_valor_max_w		number(15,4);
qt_reg_w			number(10);
ie_gerar_exame_w	varchar2(1);
nr_seq_exame_w		number(10);
ds_deflagradores_w  varchar2(255);
ie_versao_w			varchar2(10);
ie_tipo_sepse_w     varchar2(1);
qt_idade_w			number(5);
qt_idade_min_w		number(5);
qt_idade_max_w		number(5);
qt_idade_dia_w		number(10);
qt_idade_min_dias_w		number(10);
qt_idade_max_dias_w		number(10);
qt_data_cliente_sepse_w	number(10);

Cursor C01 is
	select	nr_sequencia,
			nvl(qt_pontuacao,0),
			qt_valor_min,
			qt_valor_max
	from	sepse_atributo
	where   nvl(ie_versao,'1') = ie_versao_w
	and     nvl(ie_tipo_sepse,'A') = ie_tipo_sepse_w
	order by 1;	
		
Cursor C03 is
	select a.nr_sequencia,
			nvl(b.qt_pontuacao,0),
			b.qt_valor_min,
			b.qt_valor_max
	from	sepse_atributo a,
			sepse_atributo_cliente b
	where	a.nr_sequencia = b.nr_seq_atributo
	and     nvl(ie_versao,'1') = ie_versao_w
	and     nvl(ie_tipo_sepse,'A') = ie_tipo_sepse_w
	order by 1;
	
	function Obter_se_val_ref_lab(	nr_seq_atrib_p	number,
									nr_atendimento_p	number) 
									return varchar2 is
		
		qt_reg_lab_w	number(10);
		qt_dias_w		number(10);
		qt_horas_w		number(10,2);
		ie_retorno_w	varchar2(1);
		
		cursor c01 is
			select	nr_seq_exame
			into	nr_seq_exame_w
			from	sepse_atributo_regra
			where	nr_seq_atributo = nr_seq_atrib_p;
		
		begin
		select	trunc(max(obter_dias_entre_datas(b.dt_nascimento,sysdate))),
				trunc(max(obter_hora_entre_datas(b.dt_nascimento,sysdate)))
		into	qt_dias_w,
				qt_horas_w
		from	atendimento_paciente a,
				pessoa_fisica b
		where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		and		a.nr_atendimento = nr_atendimento_p;
		
		For exames in C01 
		loop
		
		select	count(*)
		into	qt_reg_lab_w
		from	exame_lab_padrao
		where	nr_seq_exame = exames.nr_seq_exame;
		
		if	(qt_reg_lab_w > 0) then
			select	count(*)
			into	qt_reg_lab_w
			from	exame_lab_padrao
			where	nr_seq_exame = exames.nr_seq_exame
			and		((ie_periodo = 'A' and trunc((qt_dias_w / 365.25),2) between qt_idade_min and qt_idade_max) or
					 (ie_periodo = 'M' and ((qt_dias_w / 365.25) * 12) between qt_idade_min and qt_idade_max) or
					 (ie_periodo = 'D' and qt_dias_w between qt_idade_min and qt_idade_max) or
					 (ie_periodo = 'H' and qt_horas_w between qt_idade_min and qt_idade_max));
		end if;
		
		if	(qt_reg_lab_w > 0) then
			ie_retorno_w := 'S';
			exit;
		else
			ie_retorno_w := 'N';
		end if;
		
		end loop;
		
		return ie_retorno_w;
		
		end;
		
	procedure Gerar_registro_item_sepse(nr_seq_atrib_p	number) is
		begin
		if (ie_tipo_sepse_w = 'P') then
			insert into escala_sepse_infantil_item(	nr_sequencia,
											dt_atualizacao,
											nm_usuario,
											nr_seq_escala,
											nr_seq_atributo,
											qt_pontuacao)
								values	(	escala_sepse_infantil_item_seq.NEXTVAL,
											sysdate,
											nm_usuario_p,
											nr_seq_escala_p,
											nr_seq_atrib_p,
											0);

		else
			insert into escala_sepse_item(	nr_sequencia,
											dt_atualizacao,
											nm_usuario,
											nr_seq_escala,
											nr_seq_atributo,
											qt_pontuacao)
								values	(	escala_sepse_item_seq.NEXTVAL,
											sysdate,
											nm_usuario_p,
											nr_seq_escala_p,
											nr_seq_atrib_p,
											0);
		end if;
		end;

begin

ie_tipo_sepse_w := obter_se_sepse_liberada(nr_atendimento_p);

if (ie_tipo_sepse_w = 'P') then

	select	count(*)
	into	qt_reg_w
	from	sepse_atributo a,
			sepse_atributo_cliente b
	where	a.nr_sequencia = b.nr_seq_atributo
	and		a.ie_tipo_sepse = 'P';

	select 	count (*) -- Checa se h� registro de idade dentro de uma regra de cliente, caso n�o tenha, usa a geral definida.
	into	qt_data_cliente_sepse_w
	from 	sepse_atrib_cliente_regra a,
			sepse_atributo_cliente b
	where	a.nr_seq_atrib_cliente = b.nr_sequencia;

	ie_versao_w := '1';
	ds_deflagradores_w := '81';
	
else
	select	count(*)
	into	qt_reg_w
	from	sepse_atributo a,
	sepse_atributo_cliente b
	where	a.nr_sequencia = b.nr_seq_atributo;
	
	select nvl(max(ie_versao_sepse),'1')
	into   ie_versao_w
	from   parametro_medico
	where  cd_estabelecimento = nvl(obter_dados_atendimento(nr_atendimento_p,'EST'), obter_estabelecimento_ativo);

	if (ie_versao_w = '2') then
		ds_deflagradores_w := '54,55,56,65,67,72,75,77';
	else 
		ds_deflagradores_w := '12,13,14,15,16,20,21,23,24,25';
	end if;
	
end if;

select	max(to_number(obter_idade_pf(obter_pessoa_atendimento(nr_atendimento_p,'C'),sysdate,'A'))),
		trunc(max(to_number(obter_idade_pf(obter_pessoa_atendimento(nr_atendimento_p,'C'),sysdate,'DIA'))))
into	qt_idade_w,
		qt_idade_dia_w
from	dual;


if	(qt_reg_w = 0) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_atrib_w,
		qt_pontuacao_w,
		qt_valor_min_w,
		qt_valor_max_w;
	exit when C01%notfound;
		begin
			if	(obter_se_contido(nr_seq_atrib_w,ds_deflagradores_w) = 'S') then
				if	(Obter_se_val_ref_lab(nr_seq_atrib_w, nr_atendimento_p) = 'S') then
					Gerar_registro_item_sepse(nr_seq_atrib_w);
				end if;
			else
				Gerar_registro_item_sepse(nr_seq_atrib_w);
			end if;	
		end;
	end loop;
	close C01;
else
	if (ie_tipo_sepse_w = 'P') then
	begin
		open C03;
		loop
		fetch C03 into	
			nr_seq_atrib_w,
			qt_pontuacao_w,
			qt_valor_min_w,
			qt_valor_max_w;
		exit when C03%notfound;
			begin
			if (qt_data_cliente_sepse_w > 0 ) then
				select 	max(b.QT_IDADE_MIN_DIAS), max(b.QT_IDADE_MAX_DIAS), max(b.QT_IDADE_MIN), max(b.QT_IDADE_MAX)
				into 	qt_idade_min_dias_w,
						qt_idade_max_dias_w,
						qt_idade_min_w,
						qt_idade_max_w
				from  	sepse_atributo_cliente a,
						sepse_atrib_cliente_regra b
				where 	a.NR_SEQ_ATRIBUTO = nr_seq_atrib_w;
			else
				select 	max(b.qt_dias_min), max(b.qt_dias_max), max(b.QT_IDADE_MIN), max(b.QT_IDADE_MAX)
				into 	qt_idade_min_dias_w,
						qt_idade_max_dias_w,
						qt_idade_min_w,
						qt_idade_max_w
				from  	sepse_atributo_cliente a,
						sepse_regra_liberacao b
				where 	a.NR_SEQ_ATRIBUTO = nr_seq_atrib_w
				and 	b.IE_TIPO_SEPSE = 'P';
			end if;		
			if ((qt_idade_dia_w between qt_idade_min_dias_w and qt_idade_max_dias_w) or (qt_idade_w between qt_idade_min_w and qt_idade_max_w)) then
				if	(obter_se_contido(nr_seq_atrib_w,ds_deflagradores_w) = 'S') then
					if	(Obter_se_val_ref_lab(nr_seq_atrib_w,nr_atendimento_p) = 'S') then
						Gerar_registro_item_sepse(nr_seq_atrib_w);
					end if;
				else
					Gerar_registro_item_sepse(nr_seq_atrib_w);
				end if;	
			end if;	
			end;
		end loop;
		close C03;
	end;	
	else
	begin
		open C03;
		loop
		fetch C03 into	
			nr_seq_atrib_w,
			qt_pontuacao_w,
			qt_valor_min_w,
			qt_valor_max_w;
		exit when C03%notfound;
			begin
			if	(obter_se_contido(nr_seq_atrib_w,ds_deflagradores_w) = 'S') then
				if	(Obter_se_val_ref_lab(nr_seq_atrib_w,nr_atendimento_p) = 'S') then
					Gerar_registro_item_sepse(nr_seq_atrib_w);
				end if;
			else
				Gerar_registro_item_sepse(nr_seq_atrib_w);
			end if;	
			end;
		end loop;
		close C03;
	end;	
	end if;
end if;

nr_seq_atrib_w := 0;

if (ie_atualizar_p = 'S') then
	checar_pontuacao_sepse(nr_atendimento_p, nr_seq_escala_p, nm_usuario_p, ie_tipo_sepse_w);
end if;

commit;

if (ie_atualizar_p = 'S') then
	verificar_se_sepse_aberto(nr_seq_escala_p, nr_atendimento_p, nm_usuario_p);
end if;	

end gerar_itens_escala_sepse;
/
