create or replace
procedure consistir_same_pront_js(	ie_ignorar_data_p	Varchar2,
				cd_pessoa_fisica_p	Varchar2,
				nr_sequencia_p		Number,
				dt_inicial_p		Date,
				dt_final_p		Date,
				nr_seq_local_p		Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

begin

if	(ie_ignorar_data_p <> 'S') then
	Consistir_periodo_envelope(cd_pessoa_fisica_p, nr_sequencia_p, dt_inicial_p, dt_final_p, nm_usuario_p);
end if;

if	(nr_seq_local_p <> 0) then
	Consistir_prontuario_local(nr_sequencia_p, nr_seq_local_p, cd_estabelecimento_p, nm_usuario_p);
end if;

end consistir_same_pront_js;
/