create or replace
Procedure Cadastro_Regras_Estab(
    Cd_Estab_Principal_P Number,
    Cd_Estabelecimento_P Number,
    Nm_Tabela_P Varchar2,
    Nm_Usuario_P varchar2) is

    Valores_Estab_W	varchar2(2000);
    Atributo_Sequencia_W Varchar2(2000);
    Atrib_Tab_W Varchar2(2000);
    Insert_Dinamico_W Varchar2(32000);
    retorno_W Varchar2(32000);
    
    Cursor C01 Is
    Select decode(rownum, 1, '', ',')||Nm_Atributo From Tabela_Atributo
    Where Nm_Tabela = Nm_Tabela_P
    And Nm_Atributo <> Atributo_Sequencia_W
    And Nm_Atributo <> 'CD_ESTABELECIMENTO'
    And Nm_Atributo <> 'DT_ATUALIZACAO'
    And Nm_Atributo <> 'DT_ATUALIZACAO_NREC'
    And Nm_Atributo <> 'NM_USUARIO'
    And Nm_Atributo <> 'NM_USUARIO_NREC'
    and ie_tipo_atributo <> 'FUNCTION';
      
    Begin
        
        Select Nm_Atributo Into Atributo_Sequencia_W From Tabela_Atributo Where Nm_Tabela = Nm_Tabela_P And Ie_Obrigatorio = 'S' And Qt_Seq_Inicio = 1;
        
            If (Cd_Estabelecimento_P Is Not Null) Then
                If(Nm_Tabela_P Is Not Null) Then
              
                    Insert_Dinamico_W := 'Insert into '||Nm_Tabela_P||' (';
                    Valores_Estab_W := '';
                    
                    Open C01;
                    Loop
                    Fetch C01 Into 
                        ATRIB_TAB_W;
                    Exit When C01%Notfound;
                    Begin
                      Insert_Dinamico_W := Insert_Dinamico_W || ATRIB_TAB_W;
                    End;
                    End Loop;
                    close C01;
              
                    Insert_Dinamico_W := Insert_Dinamico_W || ',' || atributo_sequencia_w || ', CD_ESTABELECIMENTO, DT_ATUALIZACAO, DT_ATUALIZACAO_NREC, NM_USUARIO, NM_USUARIO_NREC) select ';
                    
                    Open C01;
                    Loop
                    Fetch C01 Into 
                        ATRIB_TAB_W;
                    Exit When C01%Notfound;
                    Begin
                      Insert_Dinamico_W := Insert_Dinamico_W || ATRIB_TAB_W;
                    End;
                    End Loop;
                    close C01;
                    
                    Insert_Dinamico_W := Insert_Dinamico_W || ',' || Nm_Tabela_P || '_seq.nextval , ' || Cd_Estabelecimento_P || ' , '''|| Sysdate || ''' , '''|| Sysdate || ''' , '''|| Nm_Usuario_P || ''' , '''|| Nm_Usuario_P || 
                    ''' from ' || nm_tabela_p || ' where cd_estabelecimento = ' || Cd_Estab_Principal_P;
                    
                    Execute Immediate Insert_Dinamico_W;
                    commit;
                    
                End If;
            End If;
        
        
      End Cadastro_Regras_Estab;
/