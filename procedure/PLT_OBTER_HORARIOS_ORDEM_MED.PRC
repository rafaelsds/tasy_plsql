create or replace
procedure plt_obter_horarios_ordem_med (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p			number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p			number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p			date,					
		dt_validade_limite_p		date,
		ie_prescr_usuario_p		varchar2) is
					
dt_horario_w	date;
					
cursor c01 is
select	c.dt_horario
from	prescr_ordem_hor c,
	prescr_medica_ordem b,
	prescr_medica a
where	c.nr_seq_ordem = b.nr_sequencia
and	b.nr_prescricao = a.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	nvl(a.ie_adep,'S') = 'S'
and	(((a.dt_liberacao_medico is not null) and
	  (ie_prescr_usuario_p = 'N')) or
	 (a.nm_usuario_original = nm_usuario_p))
group by
	c.dt_horario;

begin
open c01;
loop
fetch c01 into dt_horario_w;
exit when c01%notfound;
	begin
	insert into w_rep_horarios_t (
		nm_usuario,
		dt_horario)
	values (
		nm_usuario_p,
		dt_horario_w);
	end;
end loop;
close c01;
end plt_obter_horarios_ordem_med;
/
