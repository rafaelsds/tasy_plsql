create or replace
procedure pls_consistir_prestador_proc
			(	nr_seq_regra_p		number,
				nr_seq_prestador_p	number,
				cd_area_procedimento_p	number,
				cd_especialidade_p	number,
				cd_grupo_proc_p		number,
				cd_procedimento_p	number,
				ie_origem_proced_p	number,
				dt_inicio_vigencia_p	varchar2,
				dt_fim_vigencia_p	varchar2,
				nr_seq_grupo_serv_p	number,
				nr_seq_prestador_exec_p	number,
				ie_tipo_guia_p		varchar2,
				nm_usuario_p		Varchar2) is
					
nr_seq_regra_w			number(10);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w		date;

begin
dt_inicio_vigencia_w		:= to_date(dt_inicio_vigencia_p);
dt_fim_vigencia_w		:= to_date(dt_fim_vigencia_p);

select	max(a.nr_sequencia) 
into	nr_seq_regra_w
from	pls_prestador_proc a
where	a.nr_seq_prestador		= nr_seq_prestador_p
and	a.nr_seq_prestador_exec		= nr_seq_prestador_exec_p
and	a.nr_sequencia			<> nr_seq_regra_p
and	nvl(a.cd_area_procedimento,0)	= nvl(cd_area_procedimento_p,0)
and	nvl(a.cd_especialidade_proc,0)	= nvl(cd_especialidade_p,0)
and	nvl(a.cd_grupo_proc,0)		= nvl(cd_grupo_proc_p,0)
and	nvl(a.cd_procedimento,0)	= nvl(cd_procedimento_p,0)
and	nvl(a.ie_origem_proced,0)	= nvl(ie_origem_proced_p,0)
and	nvl(nr_seq_grupo_serv,0)	= nvl(nr_seq_grupo_serv_p,0)
and	nvl(ie_tipo_guia, '0')		= nvl(ie_tipo_guia_p, '0')
and	(((nvl(dt_inicio_vigencia,sysdate) between trunc(nvl(dt_inicio_vigencia_w,sysdate),'dd') and trunc(nvl(dt_fim_vigencia_w,sysdate),'dd')) or
	(nvl(dt_fim_vigencia,sysdate) between trunc(nvl(dt_inicio_vigencia_w,sysdate),'dd') and trunc(nvl(dt_fim_vigencia_w,sysdate),'dd'))) or
	((nvl(dt_inicio_vigencia,sysdate) between trunc(nvl(dt_inicio_vigencia_w,sysdate),'dd') and trunc(nvl(dt_fim_vigencia_w,sysdate),'dd')) and
	(nvl(dt_fim_vigencia,sysdate) between trunc(nvl(dt_inicio_vigencia_w,sysdate),'dd') and trunc(nvl(dt_fim_vigencia_w,sysdate),'dd'))));

if	(nr_seq_regra_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(194366);
end if;

end pls_consistir_prestador_proc;
/