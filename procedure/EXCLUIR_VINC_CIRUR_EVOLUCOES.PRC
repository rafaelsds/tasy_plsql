create or replace
procedure excluir_vinc_cirur_evolucoes		(nr_cirurgia_p	number) is 

begin

update evolucao_paciente
set    nr_cirurgia = null
where	nr_cirurgia = nr_cirurgia_p
and    ie_situacao = 'I';

commit;

end excluir_vinc_cirur_evolucoes;
/