create or replace
procedure liberar_inativar_suep(
			nr_sequencia_suep_p	number,
			ds_justificativa_p	varchar2,
			ie_acao_p			varchar2) is
begin

if (nr_sequencia_suep_p is not null 
	and	ie_acao_p is not null ) then
	
	if (ie_acao_p = 'L') then
		update	suep
		set		dt_liberacao = sysdate,
				nm_usuario = wheb_usuario_pck.get_nm_usuario
		where	nr_sequencia = nr_sequencia_suep_p;
	elsif (ie_acao_p = 'I') then
		update	suep
		set		dt_inativacao = sysdate,
				ie_situacao = 'I',
				nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
				ds_justificativa = ds_justificativa_p
		where	nr_sequencia = nr_sequencia_suep_p;
	elsif (ie_acao_p = 'LI') then
		update	suep
		set		dt_liberacao = sysdate,
				dt_inativacao = sysdate,
				ie_situacao = 'I',
				nm_usuario = wheb_usuario_pck.get_nm_usuario,
				nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
				ds_justificativa = obter_desc_expressao(322155)
		where	nr_sequencia = nr_sequencia_suep_p;
	end if;
	commit;
end if;

end liberar_inativar_suep;
/