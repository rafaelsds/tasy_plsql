create or replace
procedure tuss_carga_historico_item(
				nr_seq_historico_p	varchar2,
			  	cd_item_tuss_p		varchar2,
				ds_item_tuss_p		varchar2,
				ds_tipo_acao_hist_p	varchar2,
				ie_terminologia_p	varchar2,
				dt_inicio_vigencia_p	date,
				dt_fim_vigencia_p		date,
				dt_implantacao_p		date,  
				nm_usuario_p		varchar2) is
				
begin

begin
insert into tuss_historico_item
	(nr_sequencia,
	nr_seq_historico,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_item_tuss,
	ds_item_tuss,
	ds_tipo_acao_hist,
	ie_terminologia,
	dt_inicio_vigencia,
	dt_final_vigencia,
	dt_importacao)
values	(tuss_historico_item_seq.NextVal,
	nr_seq_historico_p,
	sysdate,
	substr(nm_usuario_p,1,15),
	sysdate,
	substr(nm_usuario_p,1,15),
	substr(cd_item_tuss_p,1,30),
	substr(rtrim(ltrim(ds_item_tuss_p)),1,255),
	substr(rtrim(ltrim(ds_tipo_acao_hist_p)),1,255),
	substr(rtrim(ltrim(ie_terminologia_p)),1,15),
	dt_inicio_vigencia_p,
	dt_fim_vigencia_p,
	dt_implantacao_p);
end;

commit;

end tuss_carga_historico_item;
/
