create or replace
procedure man_duplicar_controle_presenca(
				nr_sequencia_p		number,
				dt_final_p			date,
				ie_sabado_p		varchar2,
				ie_dominto_p		varchar2,
				ie_feriado_p		varchar2,
				nm_usuario_p		varchar2) is

dt_entrada_w		date;
dt_saida_w		date;
dt_referencia_w		date;
nr_sequencia_w		number(10);
qt_min_interv_w		number(5,0);
qt_min_lanche_w		number(5,0);
ie_dia_reg_w		varchar2(1);
				
begin

begin
select	dt_entrada,
	dt_saida,
	qt_min_intervalo,
	qt_min_lanche
into	dt_entrada_w,
	dt_saida_w,
	qt_min_interv_w,
	qt_min_lanche_w
from	usuario_controle
where	nr_sequencia = nr_sequencia_p;
exception
when others then
	null;
end;

if	(dt_entrada_w is not null) and
	(dt_final_p is not null) and
	(dt_entrada_w < dt_final_p) then
	begin
	dt_entrada_w	:= dt_entrada_w+1;
	dt_saida_w 	:= dt_saida_w+1;
	dt_referencia_w	:= trunc(dt_entrada_w);
	
	while (trunc(dt_entrada_w) <= trunc(dt_final_p)) loop 
		begin
		select	decode(count(*),0,'S','N')
		into	ie_dia_reg_w
		from	usuario_controle
		where	nm_usuario = nm_usuario_p
		and	dt_referencia = dt_referencia_w;
		
		if	(ie_dia_reg_w = 'S') then
			if	(nvl(ie_sabado_p,'N') = 'N') and
				(pkg_date_utils.get_WeekDay(dt_entrada_w) = 7) then /* S�bado */
				ie_dia_reg_w := 'N';
			elsif	(nvl(ie_dominto_p,'N') = 'N')
				and (pkg_date_utils.get_WeekDay(dt_entrada_w) = 1) then /* Domingo */
				ie_dia_reg_w := 'N';
			elsif	(nvl(ie_feriado_p,'N') = 'N') and
				(obter_se_feriado(wheb_usuario_pck.get_cd_estabelecimento,dt_entrada_w) > 0) then /* Reriado */
				ie_dia_reg_w := 'N';
			end if;
		end if;
		
		if	(ie_dia_reg_w = 'S') then
			begin
			select	usuario_controle_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			insert into usuario_controle(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_entrada,
					dt_saida,
					qt_min_intervalo,
					qt_min_os,
					qt_min_atividade,
					qt_min_total,
					qt_min_nreg,
					dt_referencia,
					qt_min_lanche,
					qt_min_normal,
					qt_min_extra)
				values(	nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					dt_entrada_w,
					dt_saida_w,
					qt_min_interv_w,
					0,
					0,
					0,
					0,
					dt_referencia_w,
					qt_min_lanche_w,
					0,
					0);
			if	(dt_saida_w is not null) then
				atualizar_ativ_usuario(nr_sequencia_w);
			end if;
			end;
		end if;
		
		dt_entrada_w	:= dt_entrada_w+1;
		dt_saida_w 	:= dt_saida_w+1;
		dt_referencia_w	:= trunc(dt_entrada_w);
		end;
	end loop;
	
	commit;
	end;
end if;

end man_duplicar_controle_presenca;
/
