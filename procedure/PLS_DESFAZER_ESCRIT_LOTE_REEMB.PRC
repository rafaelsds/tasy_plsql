create or replace
procedure pls_desfazer_escrit_lote_reemb
		(nr_seq_lote_reemb_cred_p	pls_lote_reemb_credito.nr_sequencia%type,
		nm_usuario_p			usuario.nm_usuario%type) is

nr_seq_banco_escrit_w		banco_escritural.nr_sequencia%type;
dt_baixa_w			banco_escritural.dt_baixa%type;
dt_liberacao_w			banco_escritural.dt_liberacao%type;

cursor	c01 (nr_seq_escrit_pc	titulo_pagar_escrit.nr_seq_escrit%type) is
select	a.nr_titulo
from	titulo_pagar_escrit a
where	a.nr_seq_escrit		= nr_seq_escrit_pc;

begin

select	nr_seq_banco_escrit
into	nr_seq_banco_escrit_w
from	pls_lote_reemb_credito a
where	a.nr_sequencia = nr_seq_lote_reemb_cred_p;

select	max(dt_baixa),
	max(dt_liberacao)
into	dt_baixa_w,
	dt_liberacao_w
from	banco_escritural
where	nr_sequencia	= nr_seq_banco_escrit_w;

if	(dt_liberacao_w is not null) or (dt_baixa_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(343668); -- O pagamento escritural vinculado a este lote de agrupamento j� est� baixado ou liberado!
end if;

for r_c01_w in c01(nr_seq_banco_escrit_w) loop
	delete	from titulo_pagar_escrit
	where	nr_titulo		= r_c01_w.nr_titulo
	and	nr_seq_escrit		= nr_seq_banco_escrit_w;
end loop;

update 	pls_lote_reemb_credito
set	nr_seq_banco_escrit 	= null,
	dt_geracao_pag_escrit	= null,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_lote_reemb_cred_p;

commit;

end pls_desfazer_escrit_lote_reemb;
/
