create or replace
procedure consistir_proc_conv_eup_js(	nr_prescricao_p			number,
					nr_seq_prescr_p			number,
					nr_atendimento_p		number,
					cd_convenio_p     		number,
					cd_procedimento_p    		number,
					ie_origem_proced_p   		number,
					dt_procedimento_p    		date,
					qt_procedimento_p    		number,
					ie_tipo_atendimento_p		number,
					cd_plano_p			varchar2,
					cd_setor_atendimento_p		number,
					nr_seq_exame_p			number,
					nr_seq_proc_interno_p		number,
					cd_categoria_p			varchar2,
					cd_setor_entrega_prescr_p	number,
					cd_medico_p			varchar2,
					nr_sequencia_p			number,
					nr_acao_executada_p		number,
					nr_prescr_medica_p		number,
					nm_usuario_p			varchar2,
					cd_funcao_p			number,
					cd_perfil_p			number,
					cd_estabelecimento_p		number,
					ds_msg_aviso_p			out varchar2,
					ie_chamar_funcao_p		out varchar2,
					nr_seq_avaliacao_p		out number) is 

vl_parametro_w		varchar2(1);
nr_seq_avaliacao_w	number(10,0);
ds_erro_w 		varchar2(255);
ie_regra_w		varchar2(2);
nr_seq_regra_w		number(10);
ds_observacao_w		varchar2(255) := '';
ds_msg_aviso_w		varchar2(255);
nr_erro_w		number(10,0);
ie_chamar_funcao_w	varchar2(1)	:= 'N';
cd_pessoa_fisica_w	varchar2(10);
ie_glosa_w		regra_ajuste_proc.ie_glosa%type;
nr_seq_regra_preco_w	regra_ajuste_proc.nr_sequencia%type;			
nr_seq_regra_ajuste_w  number(10);
vl_parametro1209_w	varchar2(1);
begin


select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;


if	(nr_prescricao_p is not null) and
	(nr_prescricao_p <> 0)then
	begin
	
	obter_param_usuario(cd_funcao_p, 338, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);
	
	if	(vl_parametro_w = 'E') and (nvl(nr_acao_executada_p,3) <> 3) and (nvl(nr_prescricao_p,0) > 0)  then
		begin
		
		gerar_avaliacao_proced_exame(nr_prescricao_p, nr_seq_prescr_p, nm_usuario_p, nr_seq_avaliacao_w);
		
		end;
	end if;
	
	if	(cd_procedimento_p is not null) and
		(cd_procedimento_p <> 0)then
		begin
		
		consiste_plano_convenio(nr_atendimento_p, cd_convenio_p, cd_procedimento_p, ie_origem_proced_p, dt_procedimento_p, qt_procedimento_p, ie_tipo_atendimento_p, 
				cd_plano_p, '', ds_erro_w, cd_setor_atendimento_p, nr_seq_exame_p, ie_regra_w, null, nr_seq_regra_w, nr_seq_proc_interno_p,
				cd_categoria_p, cd_estabelecimento_p, cd_setor_entrega_prescr_p, cd_medico_p,cd_pessoa_fisica_w,ie_glosa_w,nr_seq_regra_preco_w);
		
		end;
	end if;
				
	obter_param_usuario(cd_funcao_p, 415, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);
	obter_param_usuario(916, 1209, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro1209_w);
	
	if	(vl_parametro_w = 'S')then
		begin
		
		select 	substr(max(ds_observacao), 1, 255)
		into	ds_observacao_w
		from 	regra_convenio_plano 
		where 	nr_sequencia = nr_seq_regra_w;
		
		end;
	end if;
	
	if	(ie_regra_w in (1,2) or (vl_parametro1209_w = 'S' and ie_regra_w in (5,8))) then
		begin
		
		atualizar_aut_prescr_proc_eup(nr_sequencia_p, nr_prescricao_p, 'B', nm_usuario_p);
		
		end;
	end if;
	
	if	(ie_regra_w in (3,6,7))then
		begin
		
		atualizar_aut_prescr_proc_eup(nr_sequencia_p, nr_prescricao_p, 'PA', nm_usuario_p);
		
		end;
	end if;
	
	if	(ie_regra_w in (1,2) or (vl_parametro1209_w = 'S' and ie_regra_w in (5,8))) then
		begin
		
		ds_msg_aviso_w	:= substr(Wheb_mensagem_pck.get_texto(306561, 'DS_OBSERVACAO=' || ds_observacao_w), 1, 255);
		
		end;
	end if;
	
	obter_param_usuario(cd_funcao_p, 326, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);
	
	if	(vl_parametro_w = 'S')and
		(ie_regra_w in (3,6,7))then
		begin
		
		ds_msg_aviso_w	:= substr(Wheb_mensagem_pck.get_texto(306563, 'DS_OBSERVACAO_W=' || ds_observacao_w), 1, 255);
		
		end;
	end if;
	
	obter_param_usuario(cd_funcao_p, 172, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);
	
	if	(vl_parametro_w = 'S')and
		(nr_acao_executada_p <> 3)then
		begin
		
		consistir_prescr_procedimento(nr_prescr_medica_p, nr_seq_prescr_p, nm_usuario_p, cd_perfil_p, nr_erro_w);
		
		if	(nr_erro_w <> 0)then
			begin
			
			ie_chamar_funcao_w	:= 'S';
			
			end;
		end if;
		
		end;
	end if;
	
	--OS 896978
	obter_param_usuario(cd_funcao_p, 212, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);	
	if	(vl_parametro_w = 'S') then
		begin
		
			consiste_plano_convenio(nr_atendimento_p, 
						cd_convenio_p, 
						cd_procedimento_p, 
						ie_origem_proced_p, 
						dt_procedimento_p, 
						qt_procedimento_p, 
						ie_tipo_atendimento_p, 
						cd_plano_p, 
						'', 
						ds_erro_w, 
						cd_setor_atendimento_p, 
						nr_seq_exame_p, 
						ie_regra_w,  
						0, -- nr_seq_agenda_p
						nr_seq_regra_w, 
						nr_seq_proc_interno_p, 
						cd_categoria_p, 
						cd_estabelecimento_p, 
						cd_setor_entrega_prescr_p,
						cd_medico_p, 
						cd_pessoa_fisica_w, 
						ie_glosa_w, 
						nr_seq_regra_ajuste_w, 
						nr_prescricao_p);
						
			if (ds_erro_w <> '') then 			
			ds_msg_aviso_p := ds_erro_w;
			end if;
		end;
	end if;
	
	end;
end if;

ds_msg_aviso_p		:= ds_msg_aviso_w;
ie_chamar_funcao_p	:= ie_chamar_funcao_w;
nr_seq_avaliacao_p	:= nr_seq_avaliacao_w;

commit;

end consistir_proc_conv_eup_js;
/
