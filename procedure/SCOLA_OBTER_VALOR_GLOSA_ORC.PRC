Create or Replace
Procedure Scola_Obter_Valor_Glosa_Orc(	cd_estabelecimento_p	number,
					cd_convenio_p		number,
					cd_categoria_p		varchar2,
					cd_plano_p		varchar2,
					nr_seq_exame_p		number,
					ie_tipo_atendimento_p	number,
					dt_referencia_p		date,
					nm_usuario_p		varchar2,
					cd_material_exame_p	varchar2,
					cd_erro_p		out varchar2,
					ds_erro_p		out varchar2,
					vl_glosa_p		out number) is


qt_conta_w			number(10);
ds_erro_w			varchar2(2000);
nr_sequencia_w			number(10,0);
vl_procedimento_w		number(15,2);
ie_tipo_convenio_w		number(10,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_setor_w			number(10,0);
nr_seq_proc_interno_aux_w	number(10,0);
ie_classificacao_proc_w		varchar2(1);
ie_regra_plano_w		number(5);

nr_seq_exame_dep_w		number(10,0);
vl_exame_dependente_w		Number(15,2);
vl_total_dependente_w		Number(15,2);

Cursor C01 is
	select	distinct nr_seq_exame_dep
	from	exame_lab_dependente
	where	nr_seq_exame = nr_seq_exame_p
	and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0);

/*Lista de erros
0 - SUCESSO
*/

begin

cd_erro_p	:= 0;
ds_erro_p	:= 'SUCESSO';

vl_glosa_p:= 0;
vl_procedimento_w:= 0;

if 	(nvl(nr_seq_exame_p,0) > 0) then

	select	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio = cd_convenio_p;

	Obter_Exame_Lab_Convenio
		(nr_seq_exame_p, cd_convenio_p, cd_categoria_p, ie_tipo_atendimento_p, cd_estabelecimento_p, ie_tipo_convenio_w, 0, cd_material_exame_p, null, cd_setor_w,
		cd_procedimento_w, ie_origem_proced_w, ds_erro_w, nr_seq_proc_interno_aux_w);

	dbms_output.put_line('cd_procedimento_w= ' || cd_procedimento_w);
	dbms_output.put_line('ie_origem_proced_w= ' || ie_origem_proced_w);


	select	nvl(max(ie_classificacao),'0')
	into	ie_classificacao_proc_w
	from	procedimento
	where	cd_procedimento = cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w;

	dbms_output.put_line('ie_classificacao_proc_w= ' || ie_classificacao_proc_w);

	gerar_consulta_cobertura_plano
		(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p,  nvl(dt_referencia_p,sysdate), cd_procedimento_w, ie_origem_proced_w, 0, ie_tipo_atendimento_p, 0,
		null, 0, 0, nr_seq_exame_p, 0, null, 0, 0, null, 0, 0, null, null, nm_usuario_p,  0, 0, 'N', cd_plano_p);

	--Alteracao solicitada pelo cliente na OS 650452 em 01/10/2013
	select	(decode(c.cd_condicao_pagamento, 2, a.vl_procedimento,a.vl_procedimento - nvl(a.vl_glosa,0))) vl_dif_glosa,
		a.ie_regra_plano
	into	vl_procedimento_w,
		ie_regra_plano_w
	from	convenio_plano b,
		w_consulta_cobertura_plano A,
		convenio c
	where	a.cd_convenio		= b.cd_convenio
	and	a.cd_plano		= b.cd_plano
	and	a.cd_convenio		= cd_convenio_p
	and	a.cd_plano		= cd_plano_p
	and	a.ie_tipo_atendimento 	= ie_tipo_atendimento_p
	and	a.cd_convenio 		= c.cd_convenio;

	if	(ie_regra_plano_w = '6') then
		ds_erro_p:= 'Libera c/ autor. (consiste na conta)';
	end if;

	vl_total_dependente_w:= 0;

	if	(nvl(nr_seq_exame_p,0) > 0) then


		select	nvl(max(ie_tipo_convenio),0)
		into	ie_tipo_convenio_w
		from	convenio
		where	cd_convenio = cd_convenio_p;

		open C01;
		loop
		fetch C01 into
			nr_seq_exame_dep_w;
		exit when C01%notfound;
			begin
			Obter_Exame_Lab_Convenio(nr_seq_exame_dep_w, cd_convenio_p, cd_categoria_p, ie_tipo_atendimento_p, cd_estabelecimento_p, ie_tipo_convenio_w, 0,
						null, null, cd_setor_w, cd_procedimento_w, ie_origem_proced_w, ds_erro_w, nr_seq_proc_interno_aux_w);

			/* Substituido de acordo com a OS 800960*/
			gerar_consulta_cobertura_plano(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p,  nvl(dt_referencia_p,sysdate), cd_procedimento_w, ie_origem_proced_w,
							0, ie_tipo_atendimento_p, 0, null, 0, 0, nr_seq_exame_dep_w, 0, null, 0, 0, null, 0, 0, null, null, nm_usuario_p,  0, 0, 'N', cd_plano_p);

			select	(decode(c.cd_condicao_pagamento, 2, a.vl_procedimento,a.vl_procedimento - nvl(a.vl_glosa,0))) vl_dif_glosa
			into	vl_exame_dependente_w
			from	convenio_plano b,
				w_consulta_cobertura_plano A,
				convenio c
			where	a.cd_convenio		= b.cd_convenio
			and	a.cd_plano		= b.cd_plano
			and	a.cd_convenio		= cd_convenio_p
			and	a.cd_plano		= cd_plano_p
			and	a.ie_tipo_atendimento 	= ie_tipo_atendimento_p
			and	a.cd_convenio		= c.cd_convenio;

			vl_total_dependente_w:= vl_total_dependente_w + vl_exame_dependente_w;

			end;
		end loop;
		close C01;

	end if;

	vl_glosa_p:= vl_procedimento_w + vl_total_dependente_w;
end if;

end Scola_Obter_Valor_Glosa_Orc;
/
