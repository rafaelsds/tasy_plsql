create or replace
procedure Atualiza_volume_hidrico(	nr_prescricao_p		number,
					nr_seq_solucao_p	number,
					qt_volume_hidrico_p	number,
					ie_opcao_p		varchar2,
					nm_usuario_p		Varchar2) is 

nr_sequencia_w			number(10);
nr_seq_ancora_w			number(10);
nr_seq_comp_w			number(10);
qt_dose_ancora_w		number(18,6);
qt_dose_w			number(18,6);
qt_dose_ml_w			number(18,6);
	
cursor c01 is
select	nr_sequencia
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia_solucao = nr_seq_solucao_p
and	ie_agrupador	= 4
and	nr_sequencia	not in (nr_sequencia_w, nr_seq_ancora_w);
	
begin

if	(ie_opcao_p	= 'S') then

	select	nvl(min(b.nr_sequencia),0)
	into	nr_seq_ancora_w
	from	material a,
		prescr_material b
	where	b.cd_material		= a.cd_material
	and	b.nr_prescricao		= nr_prescricao_p
	and	b.nr_sequencia_solucao 	= nr_seq_solucao_p
	and	b.ie_agrupador		= 4
	and	a.ie_ancora_solucao	= 'S';

	select	nvl(min(b.nr_sequencia),0)
	into	nr_sequencia_w
	from	material a,
		prescr_material b
	where	b.cd_material		= a.cd_material
	and	b.nr_prescricao		= nr_prescricao_p
	and	b.nr_sequencia_solucao 	= nr_seq_solucao_p
	and	b.ie_agrupador		= 4
	and	nvl(a.ie_ancora_solucao,'N')	= 'N';
	
elsif	(ie_opcao_p	= 'T') then

	select	nvl(min(nr_sequencia),0)
	into	nr_seq_ancora_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_solucao 	= nr_seq_solucao_p
	and	ie_agrupador		= 4;

	select	nvl(min(nr_sequencia),0)
	into	nr_sequencia_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_solucao 	= nr_seq_solucao_p
	and	ie_agrupador		= 4
	and	nr_sequencia		> nr_seq_ancora_w;	
	
end if;	

if	(nr_seq_ancora_w	> 0) and
	(nr_sequencia_w		> 0) then
	
	select	obter_dose_convertida(cd_material,qt_dose,cd_unidade_medida_dose, obter_unid_med_usua('ml'))
	into	qt_dose_ancora_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_solucao	= nr_seq_solucao_p
	and	nr_sequencia		= nr_seq_ancora_w;
	
	qt_dose_ml_w	:= (qt_volume_hidrico_p - qt_dose_ancora_w);
	
	select obter_dose_convertida(cd_material,qt_dose_ml_w,obter_unid_med_usua('ml'),cd_unidade_medida_dose)
	into	qt_dose_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_solucao	= nr_seq_solucao_p
	and	nr_sequencia		= nr_sequencia_w;
	
	if	(qt_dose_ml_w < 0) then
		qt_dose_w	:= 0;
		qt_dose_ml_w	:= 0;
	end if;
	
	update	prescr_material
	set	qt_dose		= qt_dose_w,
		qt_solucao	= qt_dose_ml_w,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_w;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_comp_w;
	exit when C01%notfound;
		begin
		
		update	prescr_material
		set	qt_dose		= 0,
			qt_solucao	= 0,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_comp_w;
		
		end;
	end loop;
	close C01;
	
end if;

commit;

end Atualiza_volume_hidrico;
/
