create or replace
procedure fis_gerar_arquivo_ecf(
			nr_seq_controle_p	number,
			cd_empresa_p	number,
			nm_usuario_p	varchar2) is 

nr_count_w			number(10);
nr_seq_lote_w			number(10);	
qt_contador_w			number(10);
nr_linha_w			number(10);
qt_linha_w			number(10) := 0;	
ds_separador_w			varchar2(10) := '|';
dt_geracao_w			date;		
ds_registros_ecf_w		fis_reg_apuracao_ecf.ds_registro_ecf%type;	
nr_sequencia_w			fis_ecf_arquivo.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_forma_tributacao_w		fis_regra_ecf_0020.ie_forma_tributacao%type;
ie_forma_apuracao_w		fis_regra_ecf_0020.ie_forma_apuracao%type;
ie_tip_esc_pre_w		varchar2(2);
ie_scp_w			fis_controle_ecf.ie_scp%type;
cd_estabelecimento_scp_w	fis_controle_ecf.cd_estabelecimento_scp%type;
rules_filled  number(10);
			
cursor c_registro is
	select	 ds_registro_ecf
	from	 fis_reg_apuracao_ecf
	where	 nr_seq_lote	= nr_seq_lote_w
	and	 ie_gerar	= 'S'
	order by nr_sequencia;	

begin
select  count(*)
into    rules_filled
from    fis_regra_ecf_0020 a,
	      fis_controle_ecf b
where	  a.nr_seq_lote	= b.nr_seq_lote
and     b.nr_sequencia	= nr_seq_controle_p
and	    a.cd_empresa 	= cd_empresa_p;

if rules_filled = 0 then
  wheb_mensagem_pck.exibir_mensagem_abort(1141265);
end if;

delete from fis_ecf_arquivo
where nr_seq_controle_ecf = nr_seq_controle_p;


select 	max(nr_seq_lote),
	max(dt_geracao),
	nvl(max(cd_estabelecimento_scp), max(cd_estabelecimento)),
	nvl(max(ie_scp), 'N'),
	nvl(max(cd_estabelecimento_scp), 0)
into	nr_seq_lote_w,
	dt_geracao_w,
	cd_estabelecimento_w,
	ie_scp_w,
	cd_estabelecimento_scp_w
from 	fis_controle_ecf
where	nr_sequencia = nr_seq_controle_p;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	fis_ecf_arquivo;

select  max(ie_forma_tributacao),
	max(ie_forma_apuracao),
	max(ie_tipo_escrituracao)
into	ie_forma_tributacao_w,
	ie_forma_apuracao_w,
	ie_tip_esc_pre_w
from	fis_regra_ecf_0020 a,
	fis_controle_ecf b
where	a.nr_seq_lote	= b.nr_seq_lote
and 	b.nr_sequencia	= nr_seq_controle_p
and	a.cd_empresa 	= cd_empresa_p;

open c_registro;
loop
fetch c_registro into	
	ds_registros_ecf_w;
exit when c_registro%notfound;
	begin
	
	qt_linha_w		:= qt_linha_w;
	qt_contador_w 		:= qt_contador_w + 1;
	nr_linha_w		:= nr_linha_w + 1;
		
	if    (ds_registros_ecf_w = '0000') then
		begin
		fis_reg_ecf_0000(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;		
	elsif (ds_registros_ecf_w = '0010') then
		begin		
		fis_reg_ecf_0010(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;
	elsif (ds_registros_ecf_w = '0020') then
		begin
		fis_reg_ecf_0020(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;
	elsif (ds_registros_ecf_w = '0030') then
		begin
		fis_reg_ecf_0030(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;
	elsif (ds_registros_ecf_w = '0035') and (ie_scp_w = 'N') then
		begin
		fis_reg_ecf_0035(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;
	elsif (ds_registros_ecf_w = '0930') then
		begin
		fis_reg_ecf_0930(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;			
	elsif (ds_registros_ecf_w = 'J050') and ((ie_forma_tributacao_w in (1,2,3,4)) or (ie_forma_tributacao_w in (5,7,8,9) and (ie_tip_esc_pre_w = 'C') )) then
		begin
		fis_reg_ecf_J050(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;
	elsif (ds_registros_ecf_w = 'J100') and ((ie_forma_tributacao_w in (1,2,3,4)) or (ie_forma_tributacao_w in (5,7,8,9) and (ie_tip_esc_pre_w = 'C') )) then
		begin
		fis_reg_ecf_J100(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;			
	elsif (ds_registros_ecf_w = 'K030') and ((ie_forma_tributacao_w in (1,2,3,4)) or (ie_forma_tributacao_w in (5,7,8,9) and (ie_tip_esc_pre_w = 'C') )) then
		begin
		fis_reg_ecf_K030(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;			
	elsif (ds_registros_ecf_w = 'L030') and (ie_forma_tributacao_w in (1,2,3,4)) then
		begin
		fis_reg_ecf_L030(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;		
	elsif (ds_registros_ecf_w = 'M010') and (ie_forma_tributacao_w in (1,2,3,4)) then
		begin
		fis_reg_ecf_M010(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;			
	elsif (ds_registros_ecf_w = 'N030') and (ie_forma_tributacao_w in (1,2,3,4)) then
		begin
		fis_reg_ecf_N030(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;				
	elsif (ds_registros_ecf_w = 'P030') and ((ie_forma_tributacao_w in (3,4,5)) /*or (ie_forma_tributacao_w = 7 and (0010.FORMA_TRIB_PER = 'P') )*/) then
		begin
		fis_reg_ecf_P030(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;	
	elsif (ds_registros_ecf_w = 'U030') and ((ie_forma_tributacao_w in (8,9)) and (ie_forma_apuracao_w in ('A', 'T'))) then
		begin
		fis_reg_ecf_U030(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w,ie_scp_w);
		end;
	end if;
	
	end;
end loop;
close c_registro;

fis_reg_ecf_9001(nr_seq_controle_p,ds_separador_w,cd_estabelecimento_w,nm_usuario_p,cd_empresa_p,qt_linha_w,nr_sequencia_w);
	
update	fis_controle_ecf
set	dt_geracao = sysdate
where	nr_sequencia = nr_seq_controle_p;

commit;

insert into fis_ecf_arquivo(
                            nr_sequencia,
                            nm_usuario,
                            dt_atualizacao,
                            nm_usuario_nrec,
                            dt_atualizacao_nrec,
                            nr_seq_controle_ecf,
                            nr_linha,
                            cd_registro,
                            ds_arquivo,
                            ds_arquivo_compl)
                    values(	nr_sequencia_w + 1,
                            nm_usuario_p,
                            sysdate,
                            nm_usuario_p,
                            sysdate,
                            nr_seq_controle_p,
                            qt_linha_w + 1,
                            null,
                            '',
                            '');	
                            
commit;

end fis_gerar_arquivo_ecf;
/