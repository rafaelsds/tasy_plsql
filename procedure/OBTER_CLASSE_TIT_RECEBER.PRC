create or replace
function obter_classe_tit_receber(nr_sequencia_p number)
 		    	return Varchar2 is

retorno_w		varchar2(255);			
			
begin

select max(ds_classe) ds
into 	retorno_w
from classe_titulo_receber
where ie_situacao = 'A'
and 	nr_sequencia = nr_sequencia_p
order by 1;

return	retorno_w;

end obter_classe_tit_receber;
/