create or replace
procedure atualizar_indicador_niss_sitio(	dt_referencia_p		date,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number ) is

cd_setor_atendimento_w	number(5,0);
qt_paciente_w		number(15,0);
qt_svd_w		number(15,0);
qt_respirador_w		number(15,0);
qt_cvc_w		number(15,0);
ie_indicador_w		varchar2(15);
qt_dispositivo_w	number(10,0);
qt_infeccao_w		number(10,0);
ie_neonatal_w		niss_classif_setor.ie_neonatal%type;
ie_classif_peso_w	niss_invasividade.ie_classif_peso%type;

cursor c01 is
	select	a.cd_setor_atendimento,
		nvl(b.ie_neonatal,'N')
	from	niss_classif_setor b,
		setor_atendimento a
	where	a.nr_seq_classif_niss	= b.nr_sequencia
	and	a.nr_seq_classif_niss is not null;

cursor c02 is
	select	vl_dominio
	from	valor_dominio
	where	cd_dominio = 2006
	and	ie_situacao = 'A';

cursor c03 is
	select	sum(qt_paciente),
		sum(qt_svd + nvl(qt_cvu,0)),
		sum(qt_respirador),
		sum(qt_cvc),
		ie_classif_peso
	from	niss_invasividade
	where	cd_setor_atendimento =	cd_setor_atendimento_w
	and	dt_referencia between PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0) and PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0)
	group by ie_classif_peso
	order by ie_classif_peso;

begin

delete	from niss_infeccao_sitio
where	dt_referencia  between PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0) and PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0);
commit;


OPEN C01;
LOOP
FETCH C01 into
	cd_setor_atendimento_w,
	ie_neonatal_w;
exit when c01%notfound;
	begin

	if	(ie_neonatal_w = 'S') then

	OPEN C03;
	LOOP
	FETCH C03 into
		qt_paciente_w,
		qt_svd_w,
		qt_respirador_w,
		qt_cvc_w,
		ie_classif_peso_w;
	exit when c03%notfound;
		begin
		OPEN C02;
		LOOP
		FETCH C02 into
			ie_indicador_w;
		exit when c02%notfound;
			begin
			qt_dispositivo_w := 0;

			select	count(*)
			into	qt_infeccao_w
			from	cih_local_infeccao a,
				niss_sitio_especifico b,
				cih_ficha_ocorrencia c
			where	a.nr_seq_sitio_espec = b.nr_sequencia
			and	c.nr_ficha_ocorrencia = a.nr_ficha_ocorrencia
			and	b.cd_sitio_especifico = ie_indicador_w
			and	a.dt_infeccao between PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0) and PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0)
			and	cih_obter_setor_infeccao_niss(a.nr_ficha_ocorrencia, a.dt_infeccao,'C') = cd_setor_atendimento_w
			and	cih_obter_tipo_infeccao(a.cd_caso_infeccao) not in ('2')
			and	c.dt_liberacao is not null
			and	nvl(ie_contabiliza_niss,'S') = 'S'
			and	nvl(ie_classif_peso_w,0) = obter_classif_peso_nasc(c.nr_atendimento);

			if	(ie_indicador_w in ('LCBI','CSEP')) then
				qt_dispositivo_w :=	qt_cvc_w;
			elsif	(ie_indicador_w in ('PNEU')) then
				qt_dispositivo_w :=	qt_respirador_w;
			elsif	(ie_indicador_w in ('SUTI','ASB')) then
				qt_dispositivo_w :=	qt_svd_w;
			end if;

				insert into niss_infeccao_sitio(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_estabelecimento,
					cd_setor_atendimento,
					dt_referencia,
					ie_indicador,
					qt_pac_dia,
					qt_disp_dia,
					qt_infeccao,
					tx_infeccao,
					tx_indice,
					ie_classif_peso)
				values(
					niss_infeccao_sitio_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_estabelecimento_p,
					cd_setor_atendimento_w,
					PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0),
					ie_indicador_w,
					qt_paciente_w,
					qt_dispositivo_w,
					qt_infeccao_w,
					dividir(qt_infeccao_w,qt_dispositivo_w) * 1000,
					dividir(qt_dispositivo_w,qt_paciente_w),
					nvl(ie_classif_peso_w,0));
				commit;

			end;
			END LOOP;
			CLOSE C02;
		end;
	END LOOP;
	CLOSE C03;
	else

		select	sum(qt_paciente),
			sum(qt_svd + nvl(qt_cvu,0)),
			sum(qt_respirador),
			sum(qt_cvc)
		into	qt_paciente_w,
			qt_svd_w,
			qt_respirador_w,
			qt_cvc_w
		from	niss_invasividade
		where	cd_setor_atendimento	=	cd_setor_atendimento_w
		and	dt_referencia between PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0) and PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0);

		OPEN C02;
		LOOP
		FETCH C02 into
			ie_indicador_w;
		exit when c02%notfound;
			begin
			qt_dispositivo_w := 0;

			select	count(*)
			into	qt_infeccao_w
			from	cih_local_infeccao a,
				niss_sitio_especifico b,
				cih_ficha_ocorrencia c
			where	a.nr_seq_sitio_espec = b.nr_sequencia
			and	c.nr_ficha_ocorrencia = a.nr_ficha_ocorrencia
			and	b.cd_sitio_especifico = ie_indicador_w
			and	a.dt_infeccao between PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0) and PKG_DATE_UTILS.end_of(dt_referencia_p, 'MONTH', 0)
			and	cih_obter_setor_infeccao_niss(a.nr_ficha_ocorrencia, a.dt_infeccao,'C') = cd_setor_atendimento_w
			and	cih_obter_tipo_infeccao(a.cd_caso_infeccao) not in ('2')
			and	c.dt_liberacao is not null
			and	nvl(ie_contabiliza_niss,'S') = 'S';

			if	(ie_indicador_w in ('LCBI','CSEP')) then
				qt_dispositivo_w :=	qt_cvc_w;
			elsif	(ie_indicador_w in ('PNEU')) then
				qt_dispositivo_w :=	qt_respirador_w;
			elsif	(ie_indicador_w in ('SUTI','ASB')) then
				qt_dispositivo_w :=	qt_svd_w;
			end if;

			if	(qt_dispositivo_w > 0) then
				insert into niss_infeccao_sitio(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_estabelecimento,
					cd_setor_atendimento,
					dt_referencia,
					ie_indicador,
					qt_pac_dia,
					qt_disp_dia,
					qt_infeccao,
					tx_infeccao,
					tx_indice)
				values(
					niss_infeccao_sitio_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_estabelecimento_p,
					cd_setor_atendimento_w,
					PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0),
					ie_indicador_w,
					qt_paciente_w,
					qt_dispositivo_w,
					qt_infeccao_w,
					dividir(qt_infeccao_w,qt_dispositivo_w) * 1000,
					dividir(qt_dispositivo_w,qt_paciente_w));
				commit;
			end if;

			end;
			END LOOP;
			CLOSE C02;
	end if;


	end;
END LOOP;
CLOSE C01;

end atualizar_indicador_niss_sitio;
/
