create or replace
procedure man_gerar_hist_os_vinculada(
			nr_seq_hist_p		number,
			nm_usuario_p		Varchar2) is 

nr_seq_ordem_serv_w	number(10,0);
nr_seq_os_vinc_w		number(10,0);
ds_historico_w		varchar2(32000);
ie_origem_w		varchar2(01);
nr_seq_tipo_hist_w		number(10,0);

cursor c01 is
select	x.nr_sequencia
from	man_ordem_servico x
where	x.nr_sequencia in (	select	a.nr_sequencia
			from	man_ordem_servico a,
				man_ordem_serv_vinc b
			where	a.nr_sequencia = b.nr_seq_os_vinculada
			and	a.ie_status_ordem <> 3
			and	a.dt_fim_real is null                 
			and	b.nr_seq_ordem_servico = nr_seq_ordem_serv_w)
and	x.ie_status_ordem <> 3;

begin
if	(nvl(nr_seq_hist_p,0) > 0) then
	select	nr_seq_ordem_serv,
		ds_relat_tecnico,
		ie_origem,
		nr_seq_tipo
	into	nr_seq_ordem_serv_w,
		ds_historico_w,
		ie_origem_w,
		nr_seq_tipo_hist_w
	from	man_ordem_serv_tecnico
	where	nr_sequencia = nr_seq_hist_p;

	if	(nr_seq_ordem_serv_w > 0) then
		open C01;
		loop
		fetch C01 into	
			nr_seq_os_vinc_w;
		exit when C01%notfound;
			begin
			insert into man_ordem_serv_tecnico(	nr_sequencia,
							nr_seq_ordem_serv,
							dt_atualizacao,
							nm_usuario,
							ds_relat_tecnico,
							dt_historico,
							ie_origem,
							nr_seq_tipo,
							dt_liberacao,
							nm_usuario_lib)
						values(	man_ordem_serv_tecnico_seq.nextval,
							nr_seq_os_vinc_w,
							sysdate,
							nm_usuario_p,
							ds_historico_w,
							sysdate,
							ie_origem_w,
							nr_seq_tipo_hist_w,
							sysdate,
							nm_usuario_p);
			end;
		end loop;
		close C01;
	commit;
	end if;
end if;

end man_gerar_hist_os_vinculada;
/