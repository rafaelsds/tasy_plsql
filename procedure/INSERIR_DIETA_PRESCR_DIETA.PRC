create or replace
procedure Inserir_dieta_prescr_dieta(
						cd_dieta_p		number,
						qt_parametro_p		number,
						cd_intervalo_p		varchar2,
						nr_prescricao_p		number,
						nm_usuario_p		Varchar2,
						cd_perfil_p		number,
						cd_estabelecimento_p	number,
						ds_observacao_p		varchar2) is 

nr_sequencia_w			number(10,0);
cd_estabelecimento_w		number(10,0);
ie_permite_dieta_w		varchar2(2);
IE_VIA_APLICACAO_w		varchar2(20);
dt_prescricao_w			date;
qt_dietas_w			number(10);
cont_w				number(10);
ie_prescr_dieta_sem_lib_w	varchar2(30);
dt_primeiro_horario_w		date;
dt_inicio_prescr_w		date;
dt_validade_prescr_w		date;
dt_inicio_medic_w		date;
nr_horas_validade_w		number(5);
nr_intervalo_w			number(15) := 0;
ds_horarios1_w			varchar2(2000);
ds_horarios2_w			varchar2(2000);
hr_primeiro_horario_w	varchar2(5);
ds_horarios_w			varchar2(2000);
ds_erro_w			varchar2(2000);
nr_atendimento_w		number(15);
ie_prescr_atual_w		varchar2(1);
ds_tempo_w			varchar2(50);
cd_funcao_w			number(5);
ie_dose_espec_agora_w		varchar2(1) := 'N';
hr_dose_especial_w		varchar2(6) := '';	
ie_dose_esp_param_w		varchar2(1);
cd_intervalo_w			dieta.cd_intervalo%type;
cd_setor_prescr_w		prescr_medica.cd_setor_atendimento%type;

begin
obter_param_usuario(924, 298, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, qt_dietas_w);
Obter_Param_Usuario(924, 530, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_prescr_dieta_sem_lib_w);
Obter_Param_Usuario(924, 1012, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_prescr_atual_w);
Obter_Param_Usuario(950, 155, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_dose_esp_param_w);
nr_intervalo_w := 0;

if	(nr_prescricao_p is not null) and 
	(cd_dieta_p is not null) then
	
if	(obter_valor_param_usuario(924,708,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p) = 'S') and
	(Obter_se_prescr_pac_jejum(nr_prescricao_p) = 'S') then
	-- O paciente est� em jejum! N�o � permitido prescrever dietas para o mesmo. Par�metro [708].
	Wheb_mensagem_pck.Exibir_mensagem_abort(178043);
end if;
	
	select	nvl(max(nr_sequencia),0)+1
	into	nr_sequencia_w
	from	prescr_dieta
	where	nr_prescricao		= nr_prescricao_p;
	
	
	select	max(cd_estabelecimento), 
		max(dt_prescricao),
		max(dt_primeiro_horario),
		max(dt_inicio_prescr),
		max(nr_horas_validade),
		max(dt_validade_prescr),
		max(nr_atendimento),
		nvl(max(cd_funcao_origem),obter_funcao_ativa)
	into	cd_estabelecimento_w, 
		dt_prescricao_w,
		dt_primeiro_horario_w,
		dt_inicio_prescr_w,
		nr_horas_validade_w,
		dt_validade_prescr_w,
		nr_atendimento_w,
		cd_funcao_w
	from	prescr_medica
	where	nr_prescricao		= nr_prescricao_p;
	
	select 	max(Obter_Se_dieta_valida(cd_dieta_p, cd_estabelecimento_w, dt_prescricao_w))
	into	ie_permite_dieta_w
	from 	dual;

	if	(ie_permite_dieta_w = 'N') then
		rollback;
		Wheb_mensagem_pck.exibir_mensagem_abort(178090, 'NM_DIETA=' || substr(obter_nome_dieta(cd_dieta_p),1,80));
	end if;

	select	count(*)
	into	cont_w
	from	prescr_dieta a,
		prescr_medica b
	where	obter_se_prescr_vig_adep(b.dt_inicio_prescr, b.dt_validade_prescr, dt_inicio_prescr_w, dt_validade_prescr_w) = 'S'
	and	a.nr_prescricao		= b.nr_prescricao
	and	b.dt_suspensao is null
	and	a.dt_suspensao is null
	and	((b.dt_liberacao is not null) or
		 (b.nr_prescricao	= nr_prescricao_p))
	and	b.nr_atendimento	= nr_atendimento_w
	and	((ie_prescr_atual_w = 'N') or
		 ((ie_prescr_atual_w = 'S') and (b.nr_prescricao	= nr_prescricao_p)));	
	
	if	(qt_dietas_w	> 0) and
		((cont_w	= qt_dietas_w) or
		 ((cont_w + 1)	> qt_dietas_w)) then
		if	(cd_funcao_w	= 950) then
			ds_tempo_w	:= lower(wheb_mensagem_pck.get_texto(307506)); -- plano
		elsif	(ie_prescr_atual_w	= 'N') then
			ds_tempo_w	:= lower(wheb_mensagem_pck.get_texto(307505)); -- per�odo de prescri��es
		else
			ds_tempo_w	:= lower(wheb_mensagem_pck.get_texto(307503)); -- prescri��o
		end if;	
		Wheb_mensagem_pck.exibir_mensagem_abort(178044, 'DS_TEMPO='|| ds_tempo_w ||';QT_DIETAS=' || qt_dietas_w);
	end if;	
	
	select 	obter_primeiro_horario(cd_intervalo_p,nr_prescricao_p,null,null)
	into		hr_primeiro_horario_w
	from		dual;
	
	select	converte_char_data(to_char(dt_primeiro_horario_w,'dd/mm/yyyy'),hr_primeiro_horario_w,dt_inicio_prescr_w)
	into		dt_inicio_medic_w
	from		dual;

	select	max(cd_intervalo)
	into 	cd_intervalo_w
	from	dieta
	where	cd_dieta = cd_dieta_p;
	
	if (obter_se_limpa_prim_hor(nvl(cd_intervalo_p, cd_intervalo_w), cd_setor_prescr_w) = 'S') then	
		ds_horarios_w		:= '';
		dt_inicio_medic_w	:= null;			
	end if;
		
	Calcular_Horario_Prescricao
			(nr_prescricao_p,
			nvl(cd_intervalo_p, cd_intervalo_w),
			dt_primeiro_horario_w,
			dt_inicio_medic_w,
			nr_horas_validade_w,
			null,
			null,
			null,
			nr_intervalo_w,
			ds_horarios1_w,
			ds_horarios2_w,
			'N',
			null);
	
	
	if	(dt_inicio_medic_w is not null) then
		ds_horarios_w := substr(ds_horarios1_w||ds_horarios2_w,1,2000); 
	end if;
	
	select	max(IE_VIA_APLICACAO)
	into	IE_VIA_APLICACAO_w
	from	dieta
	where	cd_dieta	= cd_dieta_p;
	
	ie_dose_espec_agora_w := obter_se_marca_hor_esp(nr_atendimento_w,nr_prescricao_p,ie_dose_esp_param_w);
		
	if	(ie_dose_espec_agora_w = 'S') then
		select	to_char(max(a.dt_fim + 1/1440),'hh24:mi') 
		into	hr_dose_especial_w
		from	rep_jejum a,
			prescr_medica b
		where a.nr_prescricao = b.nr_prescricao 
                and	b.dt_suspensao is null 
                and   nvl(a.ie_suspenso,'N') <> 'S'
                and   dt_fim > sysdate       
		and	b.nr_atendimento	= nr_atendimento_w;
		
		if	(hr_dose_especial_w is null) then
			hr_dose_especial_w := to_char(sysdate,'hh24:mi');
		end if;
	end if;



	insert into prescr_dieta (
		nr_prescricao,
		nr_sequencia,
		cd_dieta,
		qt_parametro,
		cd_intervalo,
		dt_atualizacao,
		nm_usuario,
		ie_suspenso,
		IE_VIA_APLICACAO,
		ds_horarios,
		hr_prim_horario,
		ie_dose_espec_agora,
		hr_dose_especial,
		ds_observacao
	) values (
		nr_prescricao_p,
		nr_sequencia_w,
		cd_dieta_p,
		qt_parametro_p,
		nvl(cd_intervalo_p, cd_intervalo_w),
		sysdate,
		nm_usuario_p,
		'N',
		IE_VIA_APLICACAO_w,
		ds_horarios_w,
		hr_primeiro_horario_w,
		ie_dose_espec_agora_w,
		hr_dose_especial_w,
		ds_observacao_p		
	);
	
	
	Consistir_prescr_dieta(nr_prescricao_p,nr_sequencia_w,cd_estabelecimento_p,cd_perfil_p,nm_usuario_p,ds_erro_w);
	gerar_suplementos_dieta(nr_prescricao_p,nr_sequencia_w,nm_usuario_p,cd_perfil_p);
	
	if	(ie_prescr_dieta_sem_lib_w = 'S') then
		Gerar_prescr_dieta_hor_sem_lib(nr_prescricao_p,nr_sequencia_w,cd_perfil_p,'N','','N',nm_usuario_p);
	end if;	
		
	
end if;
commit;

end inserir_dieta_prescr_dieta;
/