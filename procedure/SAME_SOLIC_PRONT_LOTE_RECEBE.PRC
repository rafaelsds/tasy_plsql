create or replace
procedure Same_Solic_Pront_Lote_Recebe (nr_seq_lote_p			number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number) is

cd_pessoa_fisica_w	varchar2(10);
ds_parametro_w		varchar2(10) := obter_valor_param_usuario(941,103,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
	
cursor c01 is
	select	cd_pessoa_fisica
	from	same_solic_pront
	where	nr_seq_lote	= nr_seq_lote_p
	and	((ds_parametro_w = 'N') or (cd_estabelecimento = cd_estabelecimento_p));
begin

open c01;
loop
fetch c01 into	
	cd_pessoa_fisica_w;
exit when c01%notfound;
	begin
	same_solic_pront_recebe(nr_seq_lote_p, cd_pessoa_fisica_w, nm_usuario_p);
	end;
end loop;
close c01;

end Same_Solic_Pront_Lote_Recebe;
/
