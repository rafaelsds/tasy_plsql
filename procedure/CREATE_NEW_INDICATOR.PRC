create or replace procedure create_new_indicator(nr_seq_dashboard_p IN dashboard_base.nr_sequencia%type,
                               nr_seq_indicator_p IN ind_base.nr_sequencia%type,
                               nr_seq_dimension_p IN ind_dimensao.nr_sequencia%type,
                               nr_seq_information_p IN ind_informacao.nr_sequencia%type,
                               nr_seq_data_p IN ind_data.nr_sequencia%type,
			                   nm_usuario_p	IN varchar2,
                               nr_seq_dashboard_indicator_p OUT number) is

begin

select	dashboard_indicator_seq.nextval
into	nr_seq_dashboard_indicator_p
from	dual;

insert into dashboard_indicator (
			nr_sequencia,
			nr_seq_dashboard,
			nr_seq_indicator,
			dt_atualizacao,
			nm_usuario)
values(			nr_seq_dashboard_indicator_p,
			nr_seq_dashboard_p,
			nr_seq_indicator_p,
			sysdate,
			nm_usuario_p);
commit;

if	(nvl(nr_seq_dimension_p, '0') != '0') then
	create_dimension(nr_seq_dashboard_p, nr_seq_dashboard_indicator_p, nr_seq_dimension_p, nm_usuario_p);
end if;

if	(nvl(nr_seq_information_p, '0') != '0') then
	create_information(nr_seq_dashboard_p, nr_seq_dashboard_indicator_p, nr_seq_information_p, nm_usuario_p);
end if;

if	(nvl(nr_seq_data_p, '0') != '0') then
	create_date(nr_seq_dashboard_p, nr_seq_dashboard_indicator_p, nr_seq_data_p, nm_usuario_p);
end if;

commit;

end create_new_indicator;
/