create or replace
procedure pls_gerar_w_tiss_dem_pag_odont(	nr_seq_conta_p 		number,
						nr_seq_prestador_p	number,
						dt_inicio_p		date,
						dt_fim_p		date,
						nm_usuario_p		varchar2) is
				
type 	rc_guia is record	(nr_seq_guia_tiss_w			number(10)); -- Array que armazena o n�mero de sequencia da guia gerado
type 	rc_prot is record	(nr_seq_prot_tiss_w			number(10)); -- Array que armazena os numeros dos protocolos  existentes durante o periodo de parametro
type	rc_dados_prot is record	(vl_valor_w				number(15,2), -- Array que armazena os eventos financeiros dos protocolos
				ds_proc_w				varchar2(255),
				nr_event_w				varchar2(255),
				ie_nature_w				varchar2(10),
				nr_proot_w				number(10));
type 	rc_prot_comp is record	(nr_seq_protocolo_comp_w		number(10)); -- Array que armazena os numeros dos protocolos  existentes durante o periodo de parametro
				
type 	tb_guia 	is table of rc_guia 		index by pls_integer;				
type	tb_prot 	is table of rc_prot		index by pls_integer;
type 	tb_dados_prot 	is table of rc_dados_prot 	index by pls_integer;	
type	tb_prot_comp 	is table of rc_prot_comp	index by pls_integer;			

tb_guia_w		tb_guia ;
tb_prot_w		tb_prot;
tb_dados_prot_w		tb_dados_prot ;
tb_prot_comp_w		tb_prot_comp;

-- dados que ser�o usados no  array acima
cd_edicao_amb_w				varchar2(10);
cd_procedimento_w			varchar2(50);
ds_procedimento_w 			procedimento.ds_procedimento%TYPE;
ds_evento_w				varchar2(250);
cd_dente_w 				varchar2(50);
cd_face_dente_w 			varchar2(50);
dt_procedimento_w 			date;
qt_procedimento_w 			number(17,4);
vl_informado_w				number(15,2);
vl_processado_w				number(15,2);
vl_glosa_fatura_w			number(15,2);
vl_franquia_w 				number(17,4);

vl_apresentado_w 			number(17,4);
vl_procedimento_cont_w			number(17,4);
vl_procedimento_w			number(17,4);
cd_glosa_w				varchar2(50);
vl_total_w			number(15,2);
vl_glosa_prot_w				number(15,2);
vl_total_prot_w				number(15,2);
vl_cop_prot_w				number(15,2);
vl_processo_w				number(15,2);
vl_total_conta_w		number(15,2);
vl_glosa_cont_w				number(15,2);
vl_taxa_adm_pag_w			number(15,2);
vl_cop_cont_w				number(15,2);
vl_processo_cont_w			number(15,2);
vl_item_w				number(15,2);
vl_credito_w				number(15,2)	:= 0;
vl_debito_w				number(15,2)	:= 0;
vl_total_deb_cred_w			number(15,2)	:= 0;

ie_natureza_w				varchar2(10);

--totais por data de pagamento
vl_total_geral_w				number(15,2)	:= 0;
vl_total_geral_processado_w 			number(17,4)	:= 0;
vl_total_geral_glosa_w 				number(17,4)	:= 0;	
vl_total_geral_franquia_w 			number(17,4)	:= 0;
vl_total_geral_cobrado_w 			number(17,4)	:= 0;



-- dados que  v�o ser adicionados do prestador/solicitante/operadora
ds_operadora_w				varchar2(255);
cd_cgc_operadora_w			varchar2(20);
nr_demonstrativo_w			varchar2(50);
cd_prestador_w				varchar2(50);
cd_cgc_prestador_w			varchar2(20);	
nr_cpf_prestador_w			varchar2(50);
ds_prestador_w				varchar2(255);
cd_cnes_prestador_w			varchar2(50);
nr_fatura_w				varchar2(50);
nr_lote_w				number(10);
dt_envio_lote_w				date;
nr_seq_protocolo_w			varchar2(50);
cd_banco_w				varchar2(20);		
cd_agencia_w				varchar2(20);
nr_conta_w				varchar2(50);
nr_seq_evento_w				number(10);
nr_seq_lote_pgto_w			number(10);
nr_seq_proc_w 				number(10);
nr_seq_conta_w 				number(10);
cd_ans_w 				varchar2(20);
cd_guia_w				varchar2(255);
cd_guia_prestador_w			varchar2(255);
nm_beneficiario_w 			varchar2(255);
nr_carteira_w				varchar2(255);
nm_medico_w				varchar2(255);
nr_seq_conta_proc_w			number(10);
dt_pgto_w				date;
ds_observacao_w				varchar2(255);
nr_conta_glosa_w			varchar2(50);
ie_origem_proc_w			varchar2(20);		
cd_proc_w				varchar2(20);



			
-- Contadores
numero_proc_w				number(10);
nr_proc_w				number(10);	
nr_seq_apresentacao_w			number(10);	
qt_pag_guia_w				number(10) 	:= 1;
nr_count_teste_w			number(10) 	:= 1;
nr_med_w				number(10) 	:= 1;
nr_prot_w				number(10) 	:= 1;
qt_evento_w				pls_integer 	:= 0;
nr_guia_w				pls_integer;
nr_count_w				pls_integer;	
nr_protoc_tiss_w			pls_integer;
nr_seq_tiss_guia_w			w_tiss_guia.nr_sequencia%type := null;
nr_count2_w				pls_integer;
eventos_financ_seq			w_tiss_eventos_financ.nr_sequencia%type := null ;

cursor C01 is -- cursor que busca todos os n�meros do protocolo entre estas datas e que o status  seja __ e que possua uma conta no pls_conta_medica_resumo  e que o n�mero de sequencia do prestador do pagamento seja igual ao recebido por parametro.
	select	a.nr_sequencia,
		a.vl_glosa,
		a.vl_total,
		a.vl_coparticipacao,
		a.vl_cobrado,
		(select sum(x.vl_procedimentos) from pls_conta x where x.nr_seq_protocolo = a.nr_sequencia)
	from	pls_protocolo_conta a
	where	a.dt_protocolo	between dt_inicio_p 	and dt_fim_p
	and 	ie_status in (3,4,6)  -- Apenas pode ser gerado demonstrativo em contas que tenham o status do protocolo 3,4 ou 6
	and	exists (select	1
			from	pls_conta b
			where	b.nr_seq_protocolo	= a.nr_sequencia
			and	ie_status		<> 'U'
			and	b.ie_tipo_guia		= '11'
			and	exists (select	1
					from	pls_conta_medica_resumo c
					where	c.nr_seq_conta		= b.nr_sequencia
					and	c.nr_seq_prestador_pgto	= nr_seq_prestador_p
					and	c.nr_seq_lote_pgto is not null))
	order by a.dt_protocolo, a.nr_protocolo_prestador, a.nr_sequencia;

cursor C02 is --  busca todas as contas de um determinado protocolo, este que est� sendo buscado no  cursor 2
	select	b.nr_sequencia,
		b.vl_glosa,
		b.vl_total,
		b.vl_coparticipacao,
		b.vl_cobrado,
		b.vl_procedimentos
	from	pls_conta b
	where	b.nr_seq_protocolo	= nr_seq_protocolo_w
	and	b.ie_status		<> 'U'
	and	b.ie_tipo_guia		= '11'
	and	exists (select	1
			from	pls_conta_medica_resumo c
			where	c.nr_seq_conta		= b.nr_sequencia
			and	c.nr_seq_prestador_pgto	= nr_seq_prestador_p
			and	c.nr_seq_lote_pgto is not null);

cursor c03 is	--dados do procedimento que ser�o usados no array
	select	nvl(a.dt_procedimento,nvl(dt_procedimento_imp,dt_liberacao)) dt_procedimento,
		pls_obter_cod_tabela_tiss(a.cd_procedimento,'',''),
		a.cd_procedimento,
		substr(obter_descricao_procedimento(a.cd_procedimento,a.ie_origem_proced),1,255),
		a.qt_procedimento_imp,
		a.vl_beneficiario,
		a.vl_procedimento_imp,
		nvl(a.vl_coparticipacao,0),
		a.cd_dente cd_dente,
		pls_obter_faces_dente_proc(a.nr_sequencia) cd_face,
		a.cd_historico_glosa,
		nvl(a.vl_glosa,0),
		nvl(a.vl_liberado,0),
		b.cd_guia_prestador,
		b.cd_guia,
		pls_obter_dados_segurado(b.nr_seq_segurado,'C'),
		obter_nome_medico(b.cd_medico_executor,'N'),
		pls_obter_dados_segurado(b.nr_seq_segurado,'N'),
		a.nr_sequencia nr_seq_conta_proc,
		c.nr_seq_lote_pgto,
		e.cd_banco,
		e.cd_agencia,
		e.nr_conta,
		b.ds_observacao,
		c.vl_taxa_adm_pag
	from    pls_conta_proc a,
		pls_conta_medica_resumo c,
		pls_conta b,
		pls_prot_conta_titulo e
	where 	b.nr_seq_prot_conta 	= e.nr_sequencia
	and 	a.nr_seq_conta 		= b.nr_sequencia
	and	a.nr_sequencia 		= c.nr_seq_conta_proc	
	and	b.nr_sequencia 		= nr_seq_conta_w
	and	c.nr_seq_lote_pgto is not null;

Cursor 	c04 is
	select 		nr_seq_evento,
			vl_item,
			c.ds_evento,
			c.ie_natureza
		from	pls_evento c,
			pls_pagamento_item b,
			pls_pagamento_prestador a
		where	a.nr_sequencia	 	= b.nr_seq_pagamento
		and	b.nr_seq_evento		= c.nr_sequencia
		and	c.ie_tipo_evento	= 'F' --  financeiro
		and	c.ie_natureza		= 'P' -- provento
		and	a.nr_seq_prestador	= nr_seq_prestador_p
		and	exists (select	1
				from	pls_conta_medica_resumo x
				where	x.nr_seq_protocolo = nr_seq_protocolo_w
				and	x.nr_seq_lote_pgto = a.nr_seq_lote)
		union all
		select	nr_seq_evento,
			vl_item,
			c.ds_evento,
			c.ie_natureza
		from	pls_evento c,
			pls_pagamento_item b,
			pls_pagamento_prestador a
		where	a.nr_sequencia	 	= b.nr_seq_pagamento
		and	b.nr_seq_evento		= c.nr_sequencia
		and	c.ie_tipo_evento	= 'F' --  financeiro
		and	c.ie_natureza		= 'D' --  descontos
		and	a.nr_seq_prestador	= nr_seq_prestador_p
		and	exists (select	1
				from	pls_conta_medica_resumo x
				where	x.nr_seq_protocolo = nr_seq_protocolo_w	
				and	x.nr_seq_lote_pgto = a.nr_seq_lote);

Cursor 	c05 is
	select 		nr_seq_evento,
			vl_item,
			c.ds_evento,
			c.ie_natureza
		from	pls_evento c,
			pls_pagamento_item b,
			pls_pagamento_prestador a
		where	a.nr_sequencia	 	= b.nr_seq_pagamento
		and	b.nr_seq_evento		= c.nr_sequencia
		and	c.ie_tipo_evento	= 'F' --  financeiro
		and	c.ie_natureza		= 'P' -- provento
		and	a.nr_seq_prestador	= nr_seq_prestador_p
		and	exists (select	1
				from	pls_conta_medica_resumo x
				where	x.nr_seq_protocolo = nr_protoc_tiss_w
				and	x.nr_seq_lote_pgto = a.nr_seq_lote)
		union all
		select	nr_seq_evento,
			vl_item,
			c.ds_evento,
			c.ie_natureza
		from	pls_evento c,
			pls_pagamento_item b,
			pls_pagamento_prestador a
		where	a.nr_sequencia	 	= b.nr_seq_pagamento
		and	b.nr_seq_evento		= c.nr_sequencia
		and	c.ie_tipo_evento	= 'F' --  financeiro
		and	c.ie_natureza		= 'D' --  descontos
		and	a.nr_seq_prestador	= nr_seq_prestador_p
		and	exists (select	1
				from	pls_conta_medica_resumo x
				where	x.nr_seq_protocolo = nr_protoc_tiss_w	
				and	x.nr_seq_lote_pgto = a.nr_seq_lote);


begin
delete	from 	w_tiss_conta_proc
where	nm_usuario	= nm_usuario_p;


delete	from 	w_tiss_guia
where	nm_usuario	= nm_usuario_p;

delete	from 	w_tiss_totais
where	nm_usuario	= nm_usuario_p;

delete	from 	w_tiss_outras_despesas
where	nm_usuario	= nm_usuario_p;

delete	from 	w_tiss_total_demonst
where	nm_usuario	= nm_usuario_p;

delete	from 	w_tiss_opm_exec
where	nm_usuario	= nm_usuario_p;

delete	from 	w_tiss_opm
where	nm_usuario	= nm_usuario_p;

delete	from 	w_tiss_proc_paciente
where	nm_usuario	= nm_usuario_p;

commit;

open C01; 
loop
fetch C01 into
	nr_seq_protocolo_w,
	vl_glosa_prot_w,
	vl_total_w,
	vl_cop_prot_w,
	vl_processo_w,
	vl_procedimento_w ; -- guarda o resultado com o numero do protocolo
exit when C01%notfound;
	begin	
	tb_prot_w( nr_prot_w ).nr_seq_prot_tiss_w	:= nr_seq_protocolo_w;
					nr_prot_w	:= nr_prot_w + 1;	
	vl_total_geral_w		:=	vl_total_geral_w + vl_total_w;
	vl_total_geral_processado_w 	:=	vl_total_geral_processado_w + 	vl_procedimento_w;
	vl_total_geral_glosa_w 		:=	vl_total_geral_glosa_w  + vl_glosa_prot_w;
	vl_total_geral_franquia_w 	:=	vl_total_geral_franquia_w + vl_cop_prot_w;
	vl_total_geral_cobrado_w 	:=	vl_total_geral_cobrado_w + vl_procedimento_w;
	

	
	open C02; -- seleciona  o n�mero da conta  do propocolo do cursor anterior
	loop
	fetch C02 into
		nr_seq_conta_w,
		vl_glosa_cont_w,
		vl_total_conta_w,				
		vl_cop_cont_w,
		vl_processo_cont_w ,
		vl_procedimento_cont_w ;	
		begin --  Select que busca o n�mero de conta da tabela pls_glosa conta e verifica  se � igual a conta utilizada no relat�rio, caso 'sim' ir� retornar 'S';
			select  a.nr_seq_conta
			into    nr_conta_glosa_w
			from    pls_rec_glosa_conta a
			where    a.nr_seq_conta =  nr_seq_conta_w
			and ie_status = '2';
		exception
		when others then
			nr_conta_glosa_w := '0';
		end;
		
		if (nr_conta_glosa_w = nr_seq_conta_w) then
			nr_conta_glosa_w := 'S';
		end if;
		
		if (nr_conta_glosa_w  = '0') then
			nr_conta_glosa_w := 'N';
		end if;	
	
	exit when C02%notfound;
		begin
		if	(nr_seq_conta_w is not null) then
			nr_proc_w := 1;
			nr_seq_apresentacao_w := 1 ;				 
			qt_pag_guia_w := 1;	
			nr_seq_tiss_guia_w := null;
		
			open c03;
			loop
			fetch c03 into	
				dt_procedimento_w,
				cd_edicao_amb_w, 
				cd_procedimento_w, 
				ds_procedimento_w,
				qt_procedimento_w,
				vl_informado_w,
				vl_processado_w, 
				vl_franquia_w,
				cd_dente_w,
				cd_face_dente_w, 
				cd_glosa_w,
				vl_glosa_fatura_w, 
				vl_apresentado_w,
				cd_guia_prestador_w,
				cd_guia_w,
				nr_carteira_w,
				nm_medico_w,
				nm_beneficiario_w,
				nr_seq_conta_proc_w,
				nr_lote_w,
				--dt_pgto_w,
				cd_banco_w,
				cd_agencia_w,
				nr_conta_w,
				ds_observacao_w,
				vl_taxa_adm_pag_w;
			exit when C03%notfound;			
				begin
				
				
				
				if	( qt_pag_guia_w = 1) then			
					select	w_tiss_guia_seq.nextval
					into	nr_seq_tiss_guia_w
					from	dual;
					
					insert into w_tiss_guia 
						(nr_sequencia, 				dt_atualizacao,		 			nm_usuario,
						nr_seq_conta_guia, 			nr_guia_prestador, 				nr_guia_operadora,
						ie_tiss_tipo_guia/*Recurso*/, 		ds_assinatura_exec /*Nome do prof.executante*/, 		ds_especific_mat /*n�mero da carteira*/,
						ds_data_assin_benef/*Nom do benef.*/,	nr_seq_protocolo, 				nr_interno_conta /*N�mero do Lote*/,
						dt_entrada /*data de inicio de processamento*/, 	dt_assinatura_contrat /*data de fim do process.*/,	dt_solicitacao /*data do pagamento*/,
						cd_autorizacao/*banco*/, 			cd_autorizacao_prest /*agencia*/,			cd_autorizacao_princ /*conta*/,
						ds_justificativa, 			nr_atendimento )
					values ( nr_seq_tiss_guia_w,			sysdate, 					nm_usuario_p,
						nr_seq_conta_w, 			cd_guia_prestador_w,				cd_guia_w,
						nr_conta_glosa_w,			nm_medico_w,					nr_carteira_w,
						nm_beneficiario_w,			nr_seq_protocolo_w,				nr_lote_w,
						dt_inicio_p, 				dt_fim_p, 					dt_pgto_w,
						cd_banco_w, 				cd_agencia_w, 					nr_conta_w,
						ds_observacao_w, 			nr_seq_prestador_p );	
						
					tb_guia_w(nr_med_w).nr_seq_guia_tiss_w	:= nr_seq_tiss_guia_w; -- Array que armazena os n�meros de sequencia gerado do  w_tiss_guia
					tb_prot_comp_w(nr_med_w).nr_seq_protocolo_comp_w := nr_seq_protocolo_w ;
					nr_med_w	:= nr_med_w + 1;
	
				end if;
				
				if 	(nr_seq_apresentacao_w > 3) then
					nr_seq_apresentacao_w    :=  1;
				end if;
				
				insert into w_tiss_conta_proc
					(nr_sequencia,			dt_atualizacao,			nm_usuario,
					ie_tiss_tipo_guia,		cd_autorizacao,			nr_seq_proc,
					dt_procedimento,		cd_edicao_amb,			cd_procedimento,
					ds_procedimento,		qt_procedimento,		cd_dente, 
					cd_face_dente,			cd_funcionario_honor,		vl_glosa,
					vl_informado,			vl_processado,			vl_franquia,
					vl_apresentado,			nr_interno_conta ) 
				values	(w_tiss_conta_proc_seq.Nextval,	sysdate,			nm_usuario_p,
					'a',				'a',				nr_seq_tiss_guia_w,
					dt_procedimento_w ,		cd_edicao_amb_w,		cd_procedimento_w,
					ds_procedimento_w,		qt_procedimento_w,		cd_dente_w,
					cd_face_dente_w,		cd_glosa_w,			vl_glosa_fatura_w,
					vl_informado_w,			vl_processado_w,		vl_franquia_w,
					vl_apresentado_w,		null );
				
				
				insert into w_tiss_totais
					(nr_sequencia, 			dt_atualizacao, 	nm_usuario,  
					nr_seq_guia,			
									vl_medicamentos,	vl_diarias,
					vl_materiais,			vl_taxas_alugueis,	vl_gases,
					nr_seq_conta, 	

									vl_glosa,		vl_processado,
					vl_apresentado,			vl_franquia,		vl_informado)

					
				values (w_tiss_totais_seq.nextval,	sysdate,		nm_usuario_p,
					nr_seq_tiss_guia_w, 		vl_glosa_prot_w,	vl_procedimento_w, 
					vl_total_w,			vl_cop_prot_w,		vl_processo_w,
					nr_seq_conta_w,	

									vl_glosa_cont_w,	vl_procedimento_cont_w,
					vl_total_conta_w,       	vl_cop_cont_w,		vl_processo_cont_w  );	
		
		
				nr_seq_apresentacao_w := nr_seq_apresentacao_w + 1;
				
				if	( qt_pag_guia_w = 3) then
					qt_pag_guia_w := 1;	
					nr_seq_tiss_guia_w := null;
				else
					qt_pag_guia_w := qt_pag_guia_w + 1;
				end if;	
				end;
			end loop;
			close c03;
		end if;
		end;
	end loop;
	close c02;
	nr_guia_w	:= 1;
	qt_pag_guia_w 	:= 1;

	
	end;
end loop;
close c01;

	
	
qt_pag_guia_w 	:= 1;
nr_guia_w	:= 1;
numero_proc_w 	:= 1;
for i in 1..tb_prot_w.count  loop -- loop que busca todos os n�meros dos protocolos que foram utilizados  e armazenados no Array
	begin
	nr_protoc_tiss_w	:= tb_prot_w(i).nr_seq_prot_tiss_w;
	open C05; 
	loop
	fetch C05 into
			nr_seq_evento_w,
			vl_item_w,
			ds_evento_w,
			ie_natureza_w;
	exit when C05%notfound;
		begin
			tb_dados_prot_w ( numero_proc_w ).vl_valor_w		:= vl_item_w;
			tb_dados_prot_w( numero_proc_w ).ds_proc_w		:= ds_evento_w;
			tb_dados_prot_w( numero_proc_w ).nr_event_w		:= nr_seq_evento_w;
			tb_dados_prot_w( numero_proc_w ).ie_nature_w		:= ie_natureza_w;
			tb_dados_prot_w( numero_proc_w ).nr_proot_w		:= nr_protoc_tiss_w;
			numero_proc_w := numero_proc_w + 1 ;	
		end;
	end loop;
	close c05;
	end ;
end loop;


qt_pag_guia_w 	:= 1;
nr_guia_w	:= 1;
numero_proc_w 	:= 1;
nr_count_w	:= 1;


for z in 1..tb_dados_prot_w.count loop 


	if	(z > 1) then
		nr_count_w := z - 1 ;
	else	nr_count_w := z  ;
	end if;
	
	if 	(nr_guia_w > 1 ) then
		nr_count2_w := nr_guia_w - 1;
	else	nr_count2_w := nr_guia_w;
	end if;
	
	begin
	if	( qt_pag_guia_w = 1) then
		
		begin
			-- A cada  vez que � gerada uma guia nova no w_tiss_guia, � armazenado o n�mero da sequencia e o n�mero do protocolo em um array. Neste m�todo � comparado  se o n�mero do protocolo que veio do array do Cursor 5 
			-- 	� igual ao protocolo que foi armazenado no momento em que foi gerada a guia, se for, a variavel que recebe o n�mero da sequencia do tiss ir� receber o mesmo n�mero que j� foi gerado , caso n�o  seja igual 
			--	ser� verificado se o protocolo atual �  igual aon�mero do protocolo anterior que veio do array do Cursor 5 , se for, ele recebe o n�mero que foi utilizado anteriormente, caso nenhuma das op��es, � gerada uma sequencia nova.
			if	(tb_dados_prot_w(z).nr_proot_w = tb_prot_comp_w(nr_guia_w).nr_seq_protocolo_comp_w)  then
					nr_seq_tiss_guia_w	:= tb_guia_w( nr_guia_w ).nr_seq_guia_tiss_w;
									nr_guia_w := nr_guia_w + 1;
			elsif	 (tb_dados_prot_w(z).nr_proot_w = tb_dados_prot_w(nr_count2_w).nr_proot_w)then
					nr_seq_tiss_guia_w	:= tb_guia_w(nr_count2_w).nr_seq_guia_tiss_w;	
			end if;			
		exception
		when others then
			select	w_tiss_guia_seq.nextval
			into	nr_seq_tiss_guia_w
			from	dual;	
			
			insert into w_tiss_guia 
				(nr_sequencia, 				dt_atualizacao,		 			nm_usuario,
				nr_guia_prestador, 			nr_guia_operadora,
				ie_tiss_tipo_guia/*Recurso*/, 		ds_assinatura_exec /*Nome do prof.executante*/, 		ds_especific_mat /*n�mero da carteira*/,
				ds_data_assin_benef/*Nom do benef.*/,	nr_seq_protocolo, 				nr_interno_conta /*N�mero do Lote*/,
				dt_entrada /*data de inicio de processamento*/, 	dt_assinatura_contrat /*data de fim do process.*/,	dt_solicitacao /*data do pagamento*/,
				cd_autorizacao/*banco*/, 			cd_autorizacao_prest /*agencia*/,			cd_autorizacao_princ /*conta*/,
				ds_justificativa, 			nr_atendimento )
			values ( nr_seq_tiss_guia_w,			sysdate, 					nm_usuario_p,
				cd_guia_prestador_w,			cd_guia_w,
				nr_conta_glosa_w,			nm_medico_w,					nr_carteira_w,
				nm_beneficiario_w,			tb_dados_prot_w(z).nr_proot_w,			nr_lote_w,
				dt_inicio_p, 				dt_fim_p, 					dt_pgto_w,
				cd_banco_w, 				cd_agencia_w, 					nr_conta_w,
				ds_observacao_w, 			nr_seq_prestador_p );	
				
			insert into w_tiss_totais
					(nr_sequencia, 			dt_atualizacao, 	nm_usuario,  
					nr_seq_guia,			vl_medicamentos,	vl_diarias,
					vl_materiais,			vl_taxas_alugueis,	vl_gases,
					nr_seq_conta, 			vl_glosa,		vl_processado,
					vl_apresentado,			vl_franquia,		vl_informado)	
				values (w_tiss_totais_seq.nextval,	sysdate,		nm_usuario_p,
					nr_seq_tiss_guia_w, 		vl_glosa_prot_w,	vl_total_w, 
					vl_total_w,			vl_cop_prot_w,		vl_processo_w,
					nr_seq_conta_w,			vl_glosa_cont_w,	vl_total_conta_w,
					vl_total_conta_w,       	vl_cop_cont_w,		vl_processo_cont_w  );	
				
			tb_guia_w(nr_med_w).nr_seq_guia_tiss_w	:= nr_seq_tiss_guia_w;
		end;		nr_med_w	:= nr_med_w + 1;					
	end if;
	if 	( tb_dados_prot_w(z).vl_valor_w > 0) then 
		ie_natureza_w 	:= 'C';
		vl_credito_w	:= vl_credito_w + tb_dados_prot_w(z).vl_valor_w;
	else 	ie_natureza_w 	:= 'D';
		vl_debito_w 	:= vl_debito_w + tb_dados_prot_w(z).vl_valor_w;
	end if;
	
	vl_total_deb_cred_w := vl_total_deb_cred_w  + tb_dados_prot_w(z).vl_valor_w; 
	
	insert into w_tiss_total_demonst
		(nr_sequencia, 			dt_atualizacao, 		nm_usuario,  
		nr_seq_guia,			ie_indicacao,			cd_cred_deb,
		ds_descricao,			vl_total)
	values	(w_tiss_totais_seq.nextval,	sysdate,			nm_usuario_p,
		nr_seq_tiss_guia_w, 		ie_natureza_w ,			tb_dados_prot_w(z).nr_event_w,
		tb_dados_prot_w(z).ds_proc_w,	tb_dados_prot_w(z).vl_valor_w );
		
	if 	(qt_pag_guia_w = 2 )then		
		qt_pag_guia_w := 0;
	end if;
	qt_pag_guia_w := qt_pag_guia_w + 1;
	end;
end loop;

qt_pag_guia_w 	:= 1;
nr_guia_w	:= 1;
numero_proc_w 	:= 1;
nr_count_w	:= 1;

for x in 1..tb_dados_prot_w.count loop 
	begin
	if	(qt_pag_guia_w = 1) then
	begin
		nr_seq_tiss_guia_w	:= tb_guia_w( nr_guia_w ).nr_seq_guia_tiss_w;
		nr_guia_w		:= nr_guia_w + 1;
	exception
	when others then
		select	w_tiss_guia_seq.nextval
		into	nr_seq_tiss_guia_w
		from	dual;
		
		insert into w_tiss_guia 
			(nr_sequencia, 				dt_atualizacao,		 			nm_usuario,
			nr_guia_prestador, 			nr_guia_operadora,
			ie_tiss_tipo_guia/*Recurso*/, 		ds_assinatura_exec /*Nome do prof.executante*/, 		ds_especific_mat /*n�mero da carteira*/,
			ds_data_assin_benef/*Nom do benef.*/,	nr_seq_protocolo, 				nr_interno_conta /*N�mero do Lote*/,
			dt_entrada /*data de inicio de processamento*/, 	dt_assinatura_contrat /*data de fim do process.*/,	dt_solicitacao /*data do pagamento*/,
			cd_autorizacao/*banco*/, 			cd_autorizacao_prest /*agencia*/,			cd_autorizacao_princ /*conta*/,
			ds_justificativa, 			nr_atendimento )
		values ( nr_seq_tiss_guia_w,			sysdate, 					nm_usuario_p,
			cd_guia_prestador_w,			cd_guia_w,
			nr_conta_glosa_w,			nm_medico_w,					nr_carteira_w,
			nm_beneficiario_w,			tb_prot_w(x).nr_seq_prot_tiss_w,		nr_lote_w,
			dt_inicio_p, 				dt_fim_p, 					dt_pgto_w,
			cd_banco_w, 				cd_agencia_w, 					nr_conta_w,
			ds_observacao_w, 			nr_seq_prestador_p );	
			
		insert into w_tiss_totais
					(nr_sequencia, 			dt_atualizacao, 	nm_usuario,  
					nr_seq_guia,			vl_medicamentos,	vl_diarias,
					vl_materiais,			vl_taxas_alugueis,	vl_gases,
					nr_seq_conta, 			vl_glosa,	vl_processado,
					vl_apresentado,			vl_franquia,	vl_informado)	
				values (w_tiss_totais_seq.nextval,	sysdate,		nm_usuario_p,
					nr_seq_tiss_guia_w, 		vl_glosa_prot_w,	vl_total_w, 
					vl_total_w,			vl_cop_prot_w,		vl_processo_w,
					nr_seq_conta_w,			vl_glosa_cont_w,	vl_total_conta_w,
					vl_total_conta_w,       	vl_cop_cont_w,		vl_processo_cont_w  );	
			
		tb_guia_w(nr_med_w).nr_seq_guia_tiss_w	:= nr_seq_tiss_guia_w;
				nr_med_w	:= nr_med_w + 1;
	end;
					
	end if;
	if	( tb_dados_prot_w (x).vl_valor_w > 0) then 
		ie_natureza_w 	:= 'C';
	
	else 	ie_natureza_w 	:= 'D';

	end if;
	

	
	insert into w_tiss_eventos_financ
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		nr_seq_protocolo,			ds_evento,				ie_indicacao,
		vl_evento,				nr_seq_guia,				nr_seq_conta)      		
	values	(w_tiss_eventos_financ_seq.nextval,	sysdate, 				nm_usuario_p,
		nr_protoc_tiss_w,			tb_dados_prot_w(x).ds_proc_w,		ie_natureza_w ,
		tb_dados_prot_w(x).vl_valor_w,		nr_seq_tiss_guia_w,			tb_dados_prot_w(x).nr_event_w);
			
	if 	(qt_pag_guia_w = 2) then
		qt_pag_guia_w := 0;
	end if;
	qt_pag_guia_w := qt_pag_guia_w + 1;
	end;
end loop;
for y in 1..tb_guia_w.count loop 
	begin
		
		nr_seq_tiss_guia_w	:= tb_guia_w(y).nr_seq_guia_tiss_w;

	insert into w_tiss_outras_despesas -- tabela que armazena os dados dos valores totais de d�bito e cr�dito
		(nr_sequencia,					dt_atualizacao,				nm_usuario,
		nr_seq_guia,					vl_reducao_acrescimo,			vl_unitario,	
		vl_item)
	values	(w_tiss_outras_despesas_seq.nextval,		sysdate, 				nm_usuario_p,
		nr_seq_tiss_guia_w,				vl_credito_w,				vl_debito_w,			
		(vl_credito_w + vl_debito_w)+vl_total_geral_w);	

	insert into w_tiss_total_demonst
		(nr_sequencia, 			dt_atualizacao, 	nm_usuario,  
		nr_seq_guia,			vl_liberado,		vl_processado,
		vl_glosa, 			vl_franquia,		vl_informado, 	
		nr_seq_conta)	
		
	values (w_tiss_totais_seq.nextval,	sysdate,			nm_usuario_p,
		nr_seq_tiss_guia_w, 		vl_total_geral_w, 		vl_total_geral_processado_w,
		vl_total_geral_glosa_w ,	vl_total_geral_franquia_w,	vl_total_geral_cobrado_w,	
		nr_seq_conta_w);
		
		
	end;
end loop;
commit;

end pls_gerar_w_tiss_dem_pag_odont;
/