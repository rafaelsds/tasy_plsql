create or replace
procedure Consiste_Util_SNE(	nr_prescricao_p				number,
								nr_atendimento_p			number,
								dt_prescricao_p				date,
								cd_material_p				number,
								qt_dias_util_medic_p	out	number) is				

cd_material_generico_w		number(6,0);
ie_dias_util_medic_w		varchar2(1);
ie_controle_medico_w		number(1,0);
qt_dias_util_medic_w		number(10,0);
dt_anterior_w				date;
dt_dia_parametro_w			date;
qt_dias_antibiotico_w		number(3);
cd_estabelecimento_w		number(4);

BEGIN
qt_dias_util_Medic_w		:= 0;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente 
where	nr_atendimento	= NR_ATENDIMENTO_P;

select	nvl(max(qt_dias_antibiotico),1)
into	qt_dias_antibiotico_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(max(cd_material_generico), max(cd_material)),
		nvl(max(ie_dias_util_medic),'N'),
		nvl(max(obter_controle_medic(cd_material,wheb_usuario_pck.get_cd_estabelecimento,ie_controle_medico,null,null,null)),0)
into	cd_material_generico_w,
		ie_dias_util_medic_w,
		ie_controle_medico_w
from	material
where	cd_material	= cd_material_P;

/*    Indica que o sistema var retornar o numero de dias da utilização do med */
if	(ie_dias_util_medic_w = 'S') or
	(ie_dias_util_medic_w = 'O') or
	(ie_controle_medico_w = 2) then
	begin
	dt_anterior_w			:= (trunc(dt_prescricao_p,'dd') - 1) + 86399/86400;
	dt_dia_parametro_w		:= trunc(dt_prescricao_p,'dd') - qt_dias_antibiotico_w;

	select	nvl(max(a.nr_dia_util),0)
	into	qt_dias_util_Medic_w
	from 	prescr_material a,
		prescr_medica b
	where 	a.nr_prescricao	= b.nr_prescricao
	  and 	b.nr_atendimento	= nr_atendimento_p
	  and 	a.ie_suspenso	<> 'S'
	  and	a.ie_agrupador	= 8
	  and 	b.dt_prescricao between dt_dia_parametro_w and dt_anterior_w
	  and 	b.dt_liberacao is not null
	  and 	a.cd_material	in	
		(select	cd_material_generico_w
		from dual
		union
		select	cd_material_p
		from dual
		union
		select	cd_material
		from material
		where cd_material_generico	= cd_material_generico_w);
	exception
		when others then
			qt_dias_util_Medic_w	:= 0;
	end;
	
	qt_dias_util_medic_w			:= qt_dias_util_medic_w + 1; 
end if;

qt_dias_util_Medic_p		:= qt_dias_util_medic_w;

END Consiste_Util_SNE;
/
