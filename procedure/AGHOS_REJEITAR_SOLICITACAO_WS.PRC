create or replace
procedure aghos_rejeitar_solicitacao_ws (
			nm_usuario_p		Varchar2,
			nr_internacao_p		number) is 

begin
	update	solicitacao_tasy_aghos
	set	ie_situacao = 'R',
	ds_motivo_situacao = 'Solicitação rejeitada'
	where	nr_internacao = nr_internacao_p;
commit;

end aghos_rejeitar_solicitacao_ws;
/
