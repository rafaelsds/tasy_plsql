create or replace
procedure REMOVER_NR_SEQ_PROTOCOLO( nr_sequencia_p  number,
        nr_seq_prot_adic_p varchar2,
        nr_seq_protocolo_p  number) is 

begin
        UPDATE CONVENIO_RETORNO SET NR_SEQ_PROTOCOLO = nr_seq_protocolo_p, NR_SEQ_PROT_ADIC = nr_seq_prot_adic_p WHERE NR_SEQUENCIA = nr_sequencia_p;

commit;

end REMOVER_NR_SEQ_PROTOCOLO;
/