CREATE OR REPLACE PROCEDURE davita_import_pf_proc IS
  ie_count_w            	NUMBER(10);
  existe_pf_w             VARCHAR2(1);
  existe_pf_sis_ant_w     NUMBER(1);
  ds_erro_w             	VARCHAR2(4000);
  cd_pessoa_fisica_w    	pessoa_fisica.cd_pessoa_fisica%TYPE;
  nr_prontuario_w    	    pessoa_fisica.nr_prontuario%TYPE;
  nm_usuario_p          	VARCHAR2(15) := 'TASY_IMP';
  ds_observacao_w       	VARCHAR2(4000) := '';
  cont_sis_ant            NUMBER;

  CURSOR C01_PF IS
    SELECT t.ROWID, t.* FROM DAVITA_IMPORT_PF_T t
    WHERE NVL(t.IE_REG_IMP, 'N') = 'N';
    c01_w  C01_PF%ROWTYPE ;

  CURSOR C02_C_PF IS
    SELECT t.ROWID, t.* FROM DAVITA_IMPORT_COMPL_PF_T t
    WHERE NVL(t.IE_REG_IMP, 'N') = 'N';
    c02_w  C02_C_PF%rowtype;

  CURSOR C03_PF_P_E IS
    SELECT t.ROWID, t.* FROM DAVITA_IMPORT_PF_PERF_ECO_T t
    WHERE NVL(t.IE_REG_IMP, 'N') = 'N';
    c03_w  C03_PF_P_E%rowtype;

  FUNCTION existValueComplPessoaFisica(c02_w C02_C_PF%rowtype) RETURN VARCHAR2 IS
    wReturn VARCHAR2(1) := 'S';
  BEGIN
    if 	c02_w.DS_ENDERECO        	    is null and
		    c02_w.NR_ENDERECO        	is null and
		    c02_w.DS_COMPLEMENTO     	is null and
		    c02_w.nm_contato2           is null and
		    c02_w.nm_contato3           is null and
		    c02_w.DS_MUNICIPIO       	is null and
		    c02_w.SG_ESTADO          	is null and
		    c02_w.DS_BAIRRO          	is null and
		    c02_w.CD_CEP             	is null and
		    c02_w.CD_ZONA_PROCEDENCIA	is null and
		    c02_w.NM_CONTATO         	is null and
		    c02_w.NR_IDENTIDADE      	is null and
		    c02_w.NR_CPF             	is null and
		    c02_w.NR_TELEFONE        	is null and
		    c02_w.DS_EMAIL           	is null then
          wReturn := 'N';
     end IF;
    RETURN wReturn;
  END;

  FUNCTION existValuePessoaPerfSocEcon(c03_w C03_PF_P_E%rowtype) RETURN VARCHAR2 IS
    wReturn VARCHAR2(1) := 'S';
  BEGIN
    if c03_w.cd_condicao_hab1 IS NULL and c03_w.cd_classe_economica is null then
          wReturn := 'N';
     end IF;
    RETURN wReturn;
  END;

  FUNCTION getNrProntuario(cd_pessoa_fisica_p VARCHAR2) RETURN VARCHAR2 IS
    wReturn NUMBER(10);
    nr_prontuario_w pessoa_fisica.nr_prontuario%TYPE;
  BEGIN
    IF cd_pessoa_fisica_p IS NULL THEN
      wReturn := PRONTUARIO_SEQ.NEXTVAL;
    ELSE
      SELECT nr_prontuario INTO nr_prontuario_w FROM pessoa_fisica WHERE cd_pessoa_fisica = cd_pessoa_fisica_p;
      wReturn := nr_prontuario_w;
    END IF;
  RETURN wReturn;
  END;

  PROCEDURE pfExiste(c01_w C01_PF%ROWTYPE, cd_pessoa_fisica_w NUMBER, ieOpcao VARCHAR2) IS
  BEGIN
    BEGIN
      /*
      ieOpcao
      U = Dado j� existe na tabela PESSOA_FISICA_SIS_ANT e PESSOA_FISICA. Faz update.
      IU = Insert na PESSOA_FISICA_SIS_ANT e Update na DAVITA_IMPORT_PF_T. Foi importado um cadastro com cd_sistema_ant diferente,
           mas de uma pessoa fisica ja existente ou � chamado ap�s inserir uma nova pessoa fisica.
      */

      IF ieOpcao = 'U' THEN
        UPDATE DAVITA_IMPORT_PF_T
        SET ie_reg_imp = 'S', ds_erro = 'Pessoa f�sica j� existe.'
        WHERE cd_sistema_ant = c01_w.cd_sistema_ant
        AND cd_estabelecimento = c01_w.cd_estabelecimento;
      END IF;
      IF ieOpcao = 'IU' THEN
        INSERT INTO PESSOA_FISICA_SIS_ANT (cd_pessoa_fisica, cd_estabelecimento, cd_sistema_ant, nr_prontuario, dt_atualizacao)
        VALUES( cd_pessoa_fisica_w, c01_w.cd_estabelecimento, c01_w.cd_sistema_ant, c01_w.nr_prontuario, SYSDATE );

        UPDATE DAVITA_IMPORT_PF_T
        SET ie_reg_imp = 'S', ds_erro = 'Pessoa f�sica j� existe.'
        WHERE cd_sistema_ant = c01_w.cd_sistema_ant
        AND cd_estabelecimento = c01_w.cd_estabelecimento;
      END IF;
    END;
  END;

BEGIN

  DECLARE
    aux_if_sisAnt       VARCHAR2(1) := 'N';
    aux_if_cpf           pessoa_fisica.nr_cpf%TYPE := NULL;
    aux_if_identidade    pessoa_fisica.nr_identidade%TYPE := NULL;

  BEGIN
  	IE_COUNT_W := 0 ;

	OPEN C01_PF;
	LOOP FETCH C01_PF INTO c01_w;
	EXIT WHEN C01_PF%NOTFOUND;

    ds_observacao_w := NULL;
    cd_pessoa_fisica_w := NULL;

    IE_COUNT_W := IE_COUNT_W + 1;
    IF IE_COUNT_W = 50 THEN
      COMMIT;
      IE_COUNT_W := 0;
    END IF;

    BEGIN
      SELECT 'S', cd_pessoa_fisica INTO aux_if_sisAnt, cd_pessoa_fisica_w
      FROM PESSOA_FISICA_SIS_ANT
      WHERE cd_sistema_ant = c01_w.cd_sistema_ant
      AND cd_estabelecimento = c01_w.cd_estabelecimento;
    EXCEPTION
      WHEN Others THEN
        aux_if_sisAnt := 'N';
    END;

    IF aux_if_sisAnt = 'S' AND cd_pessoa_fisica_w IS NOT NULL THEN
      pfExiste(c01_w, cd_pessoa_fisica_w, 'U');
      CONTINUE;
    ELSE

      BEGIN
        SELECT t.cd_pessoa_fisica INTO cd_pessoa_fisica_w FROM (
              SELECT cd_pessoa_fisica
              FROM pessoa_fisica
              WHERE nr_cpf = REGEXP_REPLACE(c01_w.nr_cpf, '[^0-9A]', '')
               OR ( REGEXP_REPLACE(nr_identidade, '[^0-9A-Za-z]', '') = REGEXP_REPLACE(c01_w.nr_identidade, '[^0-9A-Za-z]', '') AND Upper(nm_pessoa_fisica) = Upper(c01_w.nm_pessoa_fisica) )) t ;
      EXCEPTION
        WHEN Others THEN
          cd_pessoa_fisica_w := NULL;
      END;
    END IF;

		IF cd_pessoa_fisica_w IS NOT NULL THEN
      pfExiste(c01_w, cd_pessoa_fisica_w, 'IU');
    ELSE
        SELECT pessoa_fisica_seq.NEXTVAL INTO cd_pessoa_fisica_w FROM dual;
        nr_prontuario_w :=  getNrProntuario(NULL);
        ds_observacao_w := c01_w.ds_observacao ||Chr(13)||Chr(10)||'NrProntuario legado: ' || c01_w.NR_PRONTUARIO ;
        BEGIN
          INSERT INTO PESSOA_FISICA (
                   cd_pessoa_fisica
                  ,nm_pessoa_fisica
                  ,nr_prontuario
                  ,ie_sexo
                  ,dt_nascimento
                  ,nr_cpf
                  ,nr_cartao_nac_sus
                  ,nr_titulo_eleitor
                  ,nr_identidade
                  ,dt_emissao_ci
                  ,ds_orgao_emissor_ci
                  ,nr_cep_cidade_nasc
                  ,cd_nacionalidade
                  ,nr_telefone_celular
                  ,nr_cartao_estrangeiro
                  ,ie_estado_civil
                  ,cd_religiao
                  ,ie_grau_instrucao
                  ,cd_cargo
                  ,ds_observacao
                  ,ie_tipo_pessoa
                  ,nm_usuario
                  ,nm_usuario_nrec
                  ,dt_atualizacao
                  ,dt_atualizacao_nrec
                 ) VALUES (
                   cd_pessoa_fisica_w,              --cd_pessoa_fisica
                   c01_w.NM_PESSOA_FISICA,          --nm_pessoa_fisica
                   nr_prontuario_w,                 --nr_prontuario
                   c01_w.IE_SEXO,                   --ie_sexo
                   c01_w.DT_NASCIMENTO,             --dt_nascimento
                    REGEXP_REPLACE(c01_w.nr_cpf, '[^0-9A]', ''), --nr_cpf
                   c01_w.NR_CARTAO_NAC_SUS,         --nr_cartao_nac_sus
                   c01_w.NR_TITULO_ELEITOR,         --nr_titulo_eleitor
                   c01_w.NR_IDENTIDADE,             --nr_identidade
                   c01_w.DT_EMISSAO_CI,             --dt_emissao_ci
                   c01_w.DS_ORGAO_EMISSOR_CI,       --ds_orgao_emissor_ci
                   c01_w.NR_CEP_CIDADE_NASC,        --nr_cep_cidade_nasc
                   c01_w.CD_NACIONALIDADE,          --cd_nacionalidade
                   c01_w.NR_TELEFONE_CELULAR,       --nr_telefone_celular
                   c01_w.NR_CARTAO_ESTRANGEIRO,     --nr_cartao_estrangeiro
                   c01_w.IE_ESTADO_CIVIL,           --ie_estado_civil
                   c01_w.CD_RELIGIAO,               --cd_religiao
                   c01_w.IE_GRAU_INSTRUCAO,         --ie_grau_instrucao
                   c01_w.CD_CARGO,                  --cd_cargo
                   ds_observacao_w,                 --ds_observacao
                   2,                               --ie_tipo_pessoa
                   nm_usuario_p,                    --nm_usuario
                   nm_usuario_p,                    --nm_usuario_nrec
                   SYSDATE,                         --dt_atualizacao
                   SYSDATE                          --dt_atualizacao_nrec
                   );

        pfExiste(c01_w, cd_pessoa_fisica_w, 'IU');
        EXCEPTION
          WHEN Dup_Val_On_Index THEN
            ds_erro_w := '-1 Dup_Val_On_Index: PESSOA_FISICA j� existe: ' || SQLERRM;
            UPDATE DAVITA_IMPORT_PF_T
            SET ie_reg_imp = 'N', ds_erro = ds_erro_w
            WHERE cd_sistema_ant = c01_w.cd_sistema_ant AND cd_estabelecimento = c01_w.cd_estabelecimento;
            CONTINUE;

          WHEN Others THEN
            ds_erro_w := 'ERRO Inserir PESSOA_FISICA | ' || c01_w.cd_sistema_ant || ' | ERRO => ' || SQLERRM;
            UPDATE DAVITA_IMPORT_PF_T
            SET ie_reg_imp = 'N', ds_erro = ds_erro_w
            WHERE cd_sistema_ant = c01_w.cd_sistema_ant
            AND cd_estabelecimento = c01_w.cd_estabelecimento;
            CONTINUE;
        END;
		  END IF;
      END LOOP;
	END;

  DECLARE
    nr_endereco_w         COMPL_PESSOA_FISICA.nr_endereco%TYPE := NULL;
    alpha_w               NUMBER(1);
    ds_tipo_logradouro_w	VARCHAR2(125);
    cd_tipo_logradouro_w	VARCHAR2(3);
    cd_municipio_ibge_w	  VARCHAR2(6);

	BEGIN
    IE_COUNT_W := 0;
    cd_pessoa_fisica_w := NULL;
		OPEN C02_C_PF;
		LOOP FETCH C02_C_PF INTO c02_w;
		EXIT WHEN C02_C_PF%NOTFOUND;

		  BEGIN
		  	SELECT cd_pessoa_fisica INTO cd_pessoa_fisica_w FROM PESSOA_FISICA_SIS_ANT WHERE cd_sistema_ant = c02_w.cd_sistema_ant AND cd_estabelecimento = c02_w.cd_estabelecimento;
		  EXCEPTION
		  	WHEN NO_DATA_FOUND THEN
		  		cd_pessoa_fisica_w := NULL;
		  END;

      IF cd_pessoa_fisica_w IS NULL THEN

        UPDATE DAVITA_IMPORT_COMPL_PF_T
        SET ie_reg_imp = 'N', ds_erro = 'Pessoa f�sica n�o est� cadastrada.'
        WHERE cd_sistema_ant = c02_w.cd_sistema_ant
        AND cd_estabelecimento = c02_w.cd_estabelecimento;
        CONTINUE;
		  ELSE
        IF existValueComplPessoaFisica(c02_w) = 'S' THEN
          ds_observacao_w := 'NmContato2: ' || c02_w.nm_contato2 || Chr(13)||Chr(10) || 'NmContato3: ' || c02_w.nm_contato3;

          SELECT REGEXP_INSTR(To_Char(c02_w.NR_ENDERECO), '[a-z|A-Z]|[^0-9A]') INTO alpha_w FROM dual;

          IF Nvl(alpha_w,1) > 0 THEN
            ds_observacao_w := ds_observacao_w || Chr(13)||Chr(10) || 'NrEndereco: ' || To_Char(c02_w.NR_ENDERECO);
            nr_endereco_w := NULL;
          ELSIF Length(c02_w.NR_ENDERECO) > 5 THEN
            ds_observacao_w := ds_observacao_w || Chr(13)||Chr(10) || 'NrEndereco: ' || c02_w.NR_ENDERECO;
            nr_endereco_w := NULL;
          ELSE
            nr_endereco_w := c02_w.NR_ENDERECO;
          END IF;

          BEGIN
		        cd_municipio_ibge_w := Obter_Cod_Municipio_IBGE(c02_w.CD_CEP);
	        EXCEPTION
	          WHEN OTHERS THEN
			        cd_municipio_ibge_w := null;
	        END;

	        select	max(ds_tipo_logradouro)
	        into	ds_tipo_logradouro_w
	        from	cep_logradouro_v
	        where	cd_cep = c02_w.CD_CEP;

	        select	max(cd_tipo_logradouro)
	        into	cd_tipo_logradouro_w
	        from	sus_tipo_logradouro
	        where	upper(ds_tipo_logradouro) = upper(nvl(ds_tipo_logradouro_w, ' '));

          BEGIN
            INSERT INTO COMPL_PESSOA_FISICA(
                   CD_PESSOA_FISICA
                  ,NR_SEQUENCIA
                  ,IE_TIPO_COMPLEMENTO
                  ,ds_endereco
                  ,nr_endereco
                  ,ds_complemento
                  ,ds_observacao
                  ,ds_municipio
                  ,sg_estado
                  ,ds_bairro
                  ,cd_cep
                  ,cd_zona_procedencia
                  ,nm_contato
                  ,nr_identidade
                  ,nr_cpf
                  ,nr_telefone
                  ,ds_email
                  ,NM_USUARIO
                  ,DT_ATUALIZACAO
                  ,cd_tipo_logradouro
                  ,cd_municipio_ibge
                  )
                  VALUES(
                   cd_pessoa_fisica_w          --CD_PESSOA_FISICA
                  ,1                           --NR_SEQUENCIA
                  ,1                           --IE_TIPO_COMPLEMENTO
                  ,c02_w.DS_ENDERECO           --DS_ENDERECO
                  ,nr_endereco_w               --NR_ENDERECO
                  ,c02_w.DS_COMPLEMENTO        --DS_COMPLEMENTO
                  ,ds_observacao_w             --DS_OBSERVACAO
                  ,c02_w.DS_MUNICIPIO          --DS_MUNICIPIO
                  ,c02_w.SG_ESTADO             --SG_ESTADO
                  ,c02_w.DS_BAIRRO             --DS_BAIRRO
                  ,c02_w.CD_CEP                --CD_CEP
                  ,c02_w.CD_ZONA_PROCEDENCIA   --CD_ZONA_PROCEDENCIA
                  ,c02_w.NM_CONTATO            --NM_CONTATO
                  ,c02_w.NR_IDENTIDADE         --NR_IDENTIDADE
                  ,c02_w.NR_CPF                --NR_CPF
                  ,c02_w.NR_TELEFONE           --NR_TELEFONE
                  ,c02_w.DS_EMAIL              --DS_EMAIL
                  ,NM_USUARIO_P                --NM_USUARIO
                  ,SYSDATE                     --DT_ATUALIZACAO
                  ,cd_tipo_logradouro_w
                  ,cd_municipio_ibge_w);

              UPDATE DAVITA_IMPORT_COMPL_PF_T
              SET ie_reg_imp = 'S'
              WHERE cd_sistema_ant = c02_w.cd_sistema_ant
              AND cd_estabelecimento = c02_w.cd_estabelecimento;

          EXCEPTION
          WHEN Dup_Val_On_Index THEN
            ds_erro_w := '-1 Dup_Val_On_Index: COMPL_PESSOA_FISICA j� existe: ' || SQLERRM;
            UPDATE DAVITA_IMPORT_COMPL_PF_T
            SET ie_reg_imp = 'N', ds_erro = ds_erro_w
            WHERE cd_sistema_ant = c02_w.cd_sistema_ant AND cd_estabelecimento = c02_w.cd_estabelecimento;
            CONTINUE;

            WHEN Others THEN
              ds_erro_w := 'INSERT COMPL_PESSOA_FISICA | ' || c02_w.cd_sistema_ant || ' | ERRO => ' || SQLERRM;
              UPDATE DAVITA_IMPORT_COMPL_PF_T
              SET ie_reg_imp = 'N', ds_erro = ds_erro_w
              WHERE cd_sistema_ant = c02_w.cd_sistema_ant
              AND cd_estabelecimento = c02_w.cd_estabelecimento;
              CONTINUE;
          END;
        END IF;
      END IF;

      IE_COUNT_W := IE_COUNT_W + 1;
      IF IE_COUNT_W = 500 THEN
        COMMIT;
        IE_COUNT_W := 0;
      END IF;

		END LOOP;
	END;

	BEGIN
    cd_pessoa_fisica_w := NULL;
    IE_COUNT_W := 0;

		OPEN C03_PF_P_E;
		LOOP FETCH C03_PF_P_E INTO c03_w;
		EXIT WHEN C03_PF_P_E%NOTFOUND;

		  BEGIN
		  	SELECT cd_pessoa_fisica INTO cd_pessoa_fisica_w FROM PESSOA_FISICA_SIS_ANT WHERE cd_sistema_ant = c03_w.cd_sistema_ant AND cd_estabelecimento = c03_w.cd_estabelecimento;
		  EXCEPTION
		  	WHEN NO_DATA_FOUND THEN
		  		cd_pessoa_fisica_w := NULL;
		  END;

      IF cd_pessoa_fisica_w IS NULL THEN

        UPDATE DAVITA_IMPORT_PF_PERF_ECO_T
        SET ie_reg_imp = 'N', ds_erro = 'Pessoa f�sica n�o est� cadastrada.'
        WHERE cd_sistema_ant = c03_w.cd_sistema_ant
        AND cd_estabelecimento = c03_w.cd_estabelecimento;
        CONTINUE;
		  ELSE
        IF existValuePessoaPerfSocEcon(c03_w) = 'S' THEN
          BEGIN
          INSERT INTO PESSOA_PERF_SOC_ECONOMICO(
              NR_SEQUENCIA
              ,NM_USUARIO
              ,DT_ATUALIZACAO
              ,CD_PESSOA_FISICA
              ,CD_CONDICAO_HAB1
              ,CD_CLASSE_ECONOMICA
            ) VALUES(
              PESSOA_PERF_SOC_ECONOMICO_SEQ.NEXTVAL,--NR_SEQUENCIA
              NM_USUARIO_P,                         --NM_USUARIO
              SYSDATE,                              --DT_ATUALIZACAO
              cd_pessoa_fisica_w,                   --CD_PESSOA_FISICA
              c03_w.CD_CONDICAO_HAB1,               --CD_CONDICAO_HAB1
              c03_w.CD_CLASSE_ECONOMICA             --CD_CLASSE_ECONOMICA
          );

          UPDATE DAVITA_IMPORT_PF_PERF_ECO_T
          SET ie_reg_imp = 'S'
          WHERE cd_sistema_ant = c03_w.cd_sistema_ant
          AND cd_estabelecimento = c03_w.cd_estabelecimento;


          EXCEPTION
          WHEN Dup_Val_On_Index THEN
            ds_erro_w := '-1 Dup_Val_On_Index: PESSOA_PERF_SOC_ECONOMICO j� existe: ' || SQLERRM;

            UPDATE DAVITA_IMPORT_PF_PERF_ECO_T
            SET ie_reg_imp = 'N', ds_erro = ds_erro_w
            WHERE cd_sistema_ant = c03_w.cd_sistema_ant AND cd_estabelecimento = c03_w.cd_estabelecimento;
            CONTINUE;

            WHEN Others THEN
              ds_erro_w := 'INSERT PESSOA_PERF_SOC_ECONOMICO | ' || c03_w.cd_sistema_ant || ' | ERRO => ' || SQLERRM;

              UPDATE DAVITA_IMPORT_PF_PERF_ECO_T
              SET ie_reg_imp = 'N', ds_erro = ds_erro_w
              WHERE cd_sistema_ant = c03_w.cd_sistema_ant
              AND cd_estabelecimento = c03_w.cd_estabelecimento;
              CONTINUE;
          END;
        END IF;
      END IF;

      IE_COUNT_W := IE_COUNT_W + 1;
      IF IE_COUNT_W = 500 THEN
        COMMIT;
        IE_COUNT_W := 0;
      END IF;

		END LOOP;
	END;

  COMMIT;
END DAVITA_IMPORT_PF_PROC;
/