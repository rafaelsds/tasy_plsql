CREATE OR REPLACE
PROCEDURE San_Cancela_Reserva 	(nr_seq_reserva_p NUMBER,
				nm_usuario_p VARCHAR2) IS
				
ie_status_w	VARCHAR2(15);
				
BEGIN

SELECT	MAX(ie_status)
INTO	ie_status_w
FROM	san_reserva
WHERE 	nr_sequencia = nr_seq_reserva_p;

IF	(ie_status_w = 'R') OR 
	(ie_status_w = 'S') OR
	(ie_status_w = 'L') THEN

	UPDATE 	san_reserva
	SET 	ie_status = 'C',
		dt_cancelamento = sysdate,
		nm_usuario_cancelamento = nm_usuario_p
	WHERE 	nr_sequencia = nr_seq_reserva_p;

	UPDATE san_reserva_prod
	SET  ie_status = 'N'
	WHERE  nr_seq_reserva = nr_seq_reserva_p;

END IF;

END;
/