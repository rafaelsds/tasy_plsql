create or replace procedure enviar_pix_copia_cola_sms(ds_mensagem_p varchar2,
													  ds_destinatario_p varchar2,
													  nm_usuario_p varchar2) is

	nr_telefone_origem_w pessoa_fisica.nr_telefone_celular%type;
    id_sms_w number(10);
	
begin
	
	select max(nr_telefone_celular)
    into   nr_telefone_origem_w
    from   pessoa_fisica 
    where  nm_usuario = nm_usuario_p;
    
	wheb_sms.enviar_sms(nr_telefone_origem_w, 
						elimina_caractere_especial(ds_destinatario_p), 
						ds_mensagem_p, 
						nm_usuario_p, 
						id_sms_w);		

	insert into log_envio_sms
		(nr_sequencia, 
		dt_atualizacao,
		nm_usuario,
		dt_envio, 
		nr_telefone, 
		ds_mensagem,
		id_sms)
	values 
		(log_envio_sms_seq.nextval, 
		sysdate,
		nm_usuario_p,
		sysdate,
		elimina_caractere_especial(ds_destinatario_p),
		ds_mensagem_p,
		id_sms_w);						
	
end enviar_pix_copia_cola_sms;
/
