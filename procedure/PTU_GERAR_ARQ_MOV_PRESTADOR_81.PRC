create or replace
procedure ptu_gerar_arq_mov_prestador_81(nr_seq_lote_p		number,
					cd_interface_p		varchar2,
					nm_usuario_p		varchar2,
					ds_local_p		varchar2,
					nm_arquivo_p		varchar2) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar arquivo A400 de acordo com a vers�o PTU 8.1
-----------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

ds_conteudo_w			varchar2(4000);
nm_arquivo_w			varchar2(255);
ds_local_w			varchar2(255) := null;
ds_erro_w			varchar2(255);
arq_texto_w			utl_file.file_type;
nr_seq_registro_w		number(8) := 0;
nr_seq_ptu_prestador_w		number(10);
nr_seq_ptu_endereco_w		number(10);
ds_arquivo_w			clob;
ds_hash_a400_w			ptu_movimento_prestador.ds_hash_a400%type;

-- 401
cd_unimed_destino_w		varchar2(4);
cd_unimed_origem_w		varchar2(4);
dt_geracao_w			varchar2(8);
id_ope_prest_w			varchar2(1);
nr_registro_ans_w		varchar2(6);

-- 402
tp_prest_w			varchar2(2);
cd_prest_w			varchar2(8);
cd_cgc_cpf_w			varchar2(15);
cd_insc_est_w			varchar2(20);
nr_conselho_w			varchar2(15);
cd_uf_conselho_w		varchar2(2);
nm_fantasia_w			varchar2(60);
tp_vinculo_w			varchar2(1);
cd_espec_1_w			varchar2(2);
cd_atua_1_w			varchar2(2);
cd_espec_2_w			varchar2(2);
cd_atua_2_w			varchar2(2);
dt_incl_uni_w			varchar2(8);
dt_excl_uni_w			varchar2(8);
tp_contratualizacao_w		varchar2(1);
tp_class_estabelec_w		varchar2(1);
id_cat_dif_w			varchar2(1);
id_acid_trab_w			varchar2(1);
id_urg_emer_w			varchar2(1);
id_rce_espec_1_w		varchar2(1);
id_rce_atua_1_w			varchar2(1);
id_rce_espec_2_w		varchar2(1);
id_rce_atua_2_w			varchar2(1);
dt_ini_servico_w		varchar2(8);
dt_ini_contrato_w		varchar2(8);
nm_diretor_tecnico_w		varchar2(70);
tp_disponibilidade_w		varchar2(1);
id_tab_propria_w		varchar2(1);
cd_perfil_assist_w		varchar2(2);
id_tp_prod_w			varchar2(1);
id_guia_medico_w		varchar2(1);
sg_cons_diretor_tecnico_w	varchar2(12);
cd_cons_diretor_tecnico_w	varchar2(15);
sg_uf_cons_diretor_tecnico_w	varchar2(2);
tipo_rede_min_w			varchar2(1);
id_guia_medico_espec_1_w	varchar2(1);
id_guia_medico_espec_2_w	varchar2(1);
id_guia_medico_atua_1_w		varchar2(1);
id_guia_medico_atua_2_w		varchar2(1);
prest_acred_w			varchar2(1);
particip_notivisa_w		varchar2(1);
particip_qualiss_ans_w		varchar2(1);
nr_rce_espec_1_w		varchar2(10);
nr_rce_espec_2_w		varchar2(10);
nr_rce_2_espec_1_w		varchar2(10);
nr_rce_2_espec_2_w		varchar2(10);
nr_rce_atua_1_w			varchar2(10);
nr_rce_atua_2_w			varchar2(10);
nr_rce_2_atua_1_w		varchar2(10);
nr_rce_2_atua_2_w		varchar2(10);
indic_pos_grad_w		varchar2(1);
nm_prest_w			varchar2(70);
id_publica_ans_w		varchar2(1);
nr_cbo_w			varchar2(6);
indic_residencia_w		varchar2(1);
indic_resid_espec_1_w		varchar2(1);
indic_resid_area_1_w		varchar2(1);
indic_resid_espec_2_w		varchar2(1);
indic_resid_area_2_w		varchar2(1);
cd_uni_prestadora_w		varchar2(4);
id_login_wsd_tiss_w		varchar2(1);
id_cadu_w			varchar2(1);
id_inativo_w			varchar2(1);
sg_conselho_w			varchar2(12);
ie_sexo_w			varchar2(1);
nr_seq_prestador_w		ptu_prestador.nr_seq_prestador%type;
cd_pessoa_fisica_w		pls_prestador.cd_pessoa_fisica%type;
ie_ind_ginec_obst_w		ptu_prestador.ie_ind_ginec_obst%type;
dt_atualizacao_cad_w		varchar2(8);
cod_titulacao_1_w		varchar2(3);
cod_titulacao_2_w		varchar2(3);
ie_doutorado_pos_w		varchar2(5);
id_iso9001_w			varchar2(1);
indic_mestrado_w		varchar2(1);

-- 403
tp_end_w			varchar2(1);
ds_end_w			varchar2(40);
nr_end_w			varchar2(6);
ds_bairro_w			varchar2(30);
cd_munic_w			varchar2(7);
nr_cep_w			varchar2(8);
nr_ddd_w			varchar2(4);
nr_fone_1_w			varchar2(12);
nr_fone_2_w			varchar2(12);
nr_fax_w			varchar2(12);
ds_email_w			varchar2(80);
ds_endereco_web_w		varchar2(80);
ds_email_sec_w			varchar2(80);
cd_cnes_w			varchar2(7);
nr_leitos_totais_w		varchar2(6);
nr_leitos_contrat_w		varchar2(6);
nr_leitos_psiquiat_w		varchar2(6);
nr_uti_adulto_w			varchar2(6);
nr_uti_neonatal_w		varchar2(6);
nr_uti_pediatria_w		varchar2(6);
nr_leitos_intermed_neo_w	varchar2(6);
id_filial_w			varchar2(1);
ds_compl_w			varchar2(30);
nr_leitos_hosp_dia_w		varchar2(6);
nr_leitos_tot_clinic_w		varchar2(6);
nr_leitos_tot_cirur_w		varchar2(6);
nr_leitos_tot_obstr_w		varchar2(6);
nr_leitos_tot_pediat_w		varchar2(6);
nr_leitos_tot_psiqu_w		varchar2(6);
nr_latitude_w			varchar2(20);
nr_longitude_w			varchar2(20);
nr_leitos_intermed_w		varchar2(6);
nr_referencia_end_w		varchar2(2);

-- 404
cd_gr_serv_w			varchar2(3);

-- 405
cd_rede_w			varchar2(4);
nm_rede_w			varchar2(40);

--406
ie_nivel_exclusao_ptu_w		varchar2(2);
cd_exclusao_rede_w		varchar2(4);
cd_exclusao_pla_prod_w		varchar2(20);
nr_exclusao_end_w		varchar2(2);
dt_termino_prest_w		varchar2(8);
ie_substituicao_w		varchar2(1);
cd_cnpj_cpf_substit_w		varchar2(15);
dt_inicio_prest_w		varchar2(8);
cd_motivo_exclusao_w		varchar2(10);

-- 407 
cd_imposto_w			varchar2(2);
vl_aliquota_w			varchar2(5);

-- 408
inst_acred_w			varchar2(5);
nivel_acred_w			varchar2(1);
nr_referencia_end_qualif_w	varchar2(2);

-- 410
ds_divulga_obs_w		varchar2(250);

-- 499
qt_tot_r402_w			varchar2(7);
qt_tot_r403_w			varchar2(7);
qt_tot_r404_w			varchar2(7);
qt_tot_r405_w			varchar2(7);
qt_tot_r406_w			varchar2(7);
qt_tot_r407_w			varchar2(7);
qt_tot_r408_w			varchar2(7);
qt_tot_r410_w			varchar2(7);
	
cursor c01 is
	select	nr_sequencia,
		lpad(nvl(ie_tipo_prestador,'0'),2,'0'),
		lpad(nvl(cd_prestador,'0'),8,'0'),
		lpad(nvl(cd_cgc_cpf,'0'),15,'0'),
		lpad(nvl(nr_insc_estadual,'0'),20,'0'),
		lpad(nvl(nr_crm,'0'),15,'0'),
		rpad(nvl(uf_crm,' '),2,' '),
		rpad(nvl(nm_fantasia,' '),60,' '),
		nvl(ie_tipo_vinculo,'0'),
		lpad(nvl(cd_espec_1,'0'),2,'0'),
		lpad(nvl(cd_atua_1,'0'),2,'0'),
		lpad(nvl(cd_espec_2,'0'),2,'0'),
		lpad(nvl(cd_atua_2,'0'),2,'0'),
		to_char(dt_inclusao,'YYYYMMDD'),
		rpad(nvl(to_char(dt_exclusao,'YYYYMMDD'),' '),8,' '),
		nvl(ie_tipo_contratualizacao,' '),
		nvl(ie_tipo_classif_estab,' '),
		nvl(ie_categoria_dif,' '),
		nvl(ie_acidente_trabalho,' '),
		nvl(ie_urgencia_emerg,' '),
		nvl(id_rce_espec_1,' '),
		nvl(id_rce_atua_1,' '),
		nvl(id_rce_espec_2,' '),
		nvl(id_rce_atua_2,' '),
		rpad(nvl(to_char(dt_inicio_servico,'YYYYMMDD'),' '),8,' '),
		rpad(nvl(to_char(dt_inicio_contrato,'YYYYMMDD'),' '),8,' '),
		lpad(nvl(to_char(nr_registro_ans),' '),6,' '),
		rpad(nvl(nm_diretor_tecnico,' '),70,' '),
		nvl(ie_tipo_disponibilidade,'0'),
		nvl(ie_tabela_propria,' '),
		lpad(nvl(ie_perfil_assistencial,'0'),2,'0'),
		nvl(ie_tipo_produto,'0'),
		decode(ie_tipo_prestador,1,' ',ie_guia_medico),
		rpad(nvl(substr(obter_conselho_profissional(nr_seq_conselho,'S'),1,255),' '),12,' '),
		rpad(nvl(nr_cons_diretor_tecnico,' '),15,' '),
		lpad(nvl(sg_uf_cons_diretor_tecnico,' '),2,' '),
		nvl(ie_tipo_rede_min,'0'),
		nvl(id_guia_medico_espec_1,' '),
		nvl(id_guia_medico_espec_2,' '),
		nvl(id_guia_medico_atua_1,' '),
		nvl(id_guia_medico_atua_2,' '),
		nvl(ie_notivisa,'N'),
		nvl(ie_particip_qualiss_ans,'N'),
		lpad(nvl(nr_rce_espec_1,'0'),10,'0'),
		lpad(nvl(nr_rce_espec_2,'0'),10,'0'),
		lpad(nvl(nr_rce_2_espec_1,'0'),10,'0'),
		lpad(nvl(nr_rce_2_espec_2,'0'),10,'0'),
		lpad(nvl(nr_rce_atua_1,'0'),10,'0'),
		lpad(nvl(nr_rce_atua_2,'0'),10,'0'),
		lpad(nvl(nr_rce_2_atua_1,'0'),10,'0'),
		lpad(nvl(nr_rce_2_atua_2,'0'),10,'0'),
		nvl(indic_pos_grad,' '),
		rpad(nvl(nm_prestador,' '),70,' '),
		nvl(id_publica_ans,' '),
		lpad(nvl(nr_cbo,'0'),6,'0'),
		nvl(indic_residencia,' '),
		lpad(nvl(cd_uni_prestadora,'0'),4,'0'),
		nvl(id_login_wsd_tiss,' '),
		nvl(id_cadu,' '),
		nvl(id_inativo,' '),
		rpad(nvl(sg_conselho,' '),12,' '),
		nr_seq_prestador,
		ie_ind_ginec_obst,
		ie_sexo,
		to_char(dt_atualizacao_cad,'YYYYMMDD'),
		lpad(nvl(cod_titulacao_1,'0'),3,'0'),
		lpad(nvl(cod_titulacao_2,'0'),3,'0'),
		nvl(indic_resid_espec_1,' '),
		nvl(indic_resid_espec_2,' '),
		nvl(indic_resid_atua_1,' '),
		nvl(indic_resid_atua_2,' '),
		nvl(ie_doutorado_pos,' '),
		nvl(id_iso9001,'N'),
		nvl(indic_mestrado,' ')
	from	ptu_prestador
	where	nr_seq_movimento = nr_seq_lote_p
	order by 1;
	
Cursor C02 is
	select	z.nr_sequencia,
		nvl(z.ie_tipo_endereco,'0'),
		rpad(nvl(z.ds_endereco,' '),40,' '),
		rpad(nvl(z.nr_endereco,' '),6,' '),
		rpad(nvl(z.ds_bairro,' '),30,' '),
		lpad(nvl(z.cd_municipio_ibge,'0'),6,'0') || nvl(calcula_digito('MODULO10',lpad(z.cd_municipio_ibge,6,'0')),'0'),
		lpad(nvl(z.cd_cep,'0'),8,'0'),
		lpad(nvl(z.nr_ddd,'0'),4,'0'),
		lpad(nvl(z.nr_telefone,'0'),12,'0'),
		lpad(nvl(z.nr_telefone_dois,'0'),12,'0'),
		lpad(nvl(z.nr_fax,'0'),12,'0'),
		rpad(nvl(z.ds_email,' '),80,' '),
		rpad(nvl(z.ds_endereco_web,' '),80,' '),
		lpad(nvl(z.cd_cnes,'9999999'),7,'0'),
		lpad(nvl(z.nr_leitos_totais,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_contrat,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_psiquiatria,'0'),6,'0'),
		lpad(nvl(z.nr_uti_adulto,'0'),6,'0'),
		lpad(nvl(z.nr_uti_neonatal,'0'),6,'0'),
		lpad(nvl(z.nr_uti_pediatria,'0'),6,'0'),
		lpad(nvl(z.cd_cgc_cpf,'0'),15,'0'),
		lpad(nvl(z.nr_leitos_intermed_neo,'0'),6,'0'),
		nvl(substr(pls_obter_dados_prestador(x.nr_seq_prestador,'F'),1,1),'N'),
		rpad(nvl(z.ds_complemento,' '),30,' '),
		lpad(nvl(z.nr_leitos_hosp_dia,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_tot_clinic,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_tot_cirurgico,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_tot_obstr,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_tot_pediat,'0'),6,'0'),
		lpad(nvl(z.nr_leitos_tot_psiqu,'0'),6,'0'),
		rpad(nvl(z.nr_latitude,' '),20,' ') nr_latitude,
		rpad(nvl(z.nr_longitude,' '),20,' ') nr_longitude,
		lpad(nvl(z.nr_leitos_intermed,'0'),6,'0'),
		nvl(ie_prest_acred,'N'),
		lpad(nvl(z.nr_referencia_end,'0'),2,'0'),
		rpad(nvl(z.ds_email_sec,' '),80,' ')
	from	ptu_prestador_endereco z,
		ptu_prestador	x		
	where	x.nr_sequencia		= z.nr_seq_prestador
	and	x.nr_sequencia		= nr_seq_ptu_prestador_w
	order by 1;
	
Cursor C03 is
	select	lpad(nvl(cd_grupo_servico,'0'),3,'0')
	from	ptu_prestador_grupo_serv
	where	nr_seq_endereco	= nr_seq_ptu_endereco_w
	order by cd_grupo_servico;

Cursor C04 is
	select	rpad(nvl(cd_rede,' '),4,' '),
		rpad(nvl(nm_rede,' '),40,' ')
	from	ptu_prestador_rede_ref
	where	nr_seq_prestador	= nr_seq_ptu_prestador_w
	order by cd_rede;
	
Cursor C05 is
	select	lpad(nvl(ie_nivel_exclusao_ptu,'0'),2,'0'),
		rpad(nvl(cd_exclusao_rede,' '),4,' '),
		rpad(nvl(cd_exclusao_pla_prod,' '),20,' '),
		lpad(nvl(nr_exclusao_end,'0'),2,'0'),
		lpad(nvl(to_char(dt_termino_prest,'YYYYMMDD'),' '),8,' '),
		nvl(ie_substituicao,'N'),
		lpad(nvl(cd_cnpj_cpf_substit,'0'),15,'0'),
		lpad(nvl(to_char(dt_inicio_prest,'YYYYMMDD'),' '),8,' '),
		lpad(nvl(cd_motivo_exclusao,'0'),2,'0')
	from	ptu_prestador_excl_subst
	where	nr_seq_prestador	= nr_seq_ptu_prestador_w;
	
Cursor C06 is
	select	lpad(nvl(cd_imposto,'0'),2,'0'),
		lpad(replace(replace(campo_mascara(nvl(vl_aliquota,0),2),',',''),'.',''),5,'0')
	from	ptu_prestador_imposto
	where	nr_seq_prestador	= nr_seq_ptu_prestador_w;
	
Cursor C07 is
	select	lpad(nvl(cd_cnpj_cpf,'0'),15,'0'),
		lpad(decode(trim(cd_instituicao_acred),null,	'000',	
					'BSI','001','CBA','002','CBRM','003','CBRR','004','CBRT','005','CBRU','006','CQH','007',
					'DICQ','008','DNS','009','FCAV','010','GL','011','IAHC','012','IPAS','013','IQG','014',
					'JOINT','015','ONA','016','OUT','017','PALC','018','PEM','019','SIG','020','APCE','021',
					'AQSA','022','EGOPQ','023','ISO','000','EASSI','024',trim(cd_instituicao_acred)),3,'0'),
		nvl(ie_nivel_acreditacao,'0'),
		lpad(nvl(nr_referencia_end,'0'),2,'0')
	from	ptu_prestador_qualific
	where	nr_seq_prestador	= nr_seq_ptu_prestador_w;
	
Cursor C08 is
	select	rpad(ds_divulgacao_obs,250,' ') ds_divulgacao_obs
	from	ptu_prestador_observacao
	where	nr_seq_prestador	= nr_seq_ptu_prestador_w;

begin

nm_arquivo_w	:= nm_arquivo_p;
ds_local_w	:= ds_local_p;

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

-- 401 HEADER (OBRIGAT�RIO)
select	lpad(nvl(cd_unimed_destino,'0'),4,'0'),
	lpad(nvl(cd_unimed_origem,'0'),4,'0'),
	to_char(nvl(dt_geracao,sysdate),'yyyymmdd'),
	nvl(id_ope_prest,'2'),
	lpad(nvl(nr_registro_ans,'0'),6,'0')
into	cd_unimed_destino_w,
	cd_unimed_origem_w,
	dt_geracao_w,
	id_ope_prest_w,
	nr_registro_ans_w
from	ptu_movimento_prestador
where	nr_sequencia = nr_seq_lote_p;

nr_seq_registro_w := nr_seq_registro_w + 1;

ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '401' || cd_unimed_destino_w || cd_unimed_origem_w || dt_geracao_w || '26' || id_ope_prest_w || 
			nr_registro_ans_w;
ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);
ds_conteudo_w := null;
nr_registro_ans_w := null;

-- 402 PRESTADOR (OBRIGAT�RIO)
open C01;
loop
fetch C01 into	
	nr_seq_ptu_prestador_w,
	tp_prest_w,
	cd_prest_w,
	cd_cgc_cpf_w,
	cd_insc_est_w,
	nr_conselho_w,
	cd_uf_conselho_w,
	nm_fantasia_w,
	tp_vinculo_w,
	cd_espec_1_w,
	cd_atua_1_w,
	cd_espec_2_w,
	cd_atua_2_w,
	dt_incl_uni_w,
	dt_excl_uni_w,
	tp_contratualizacao_w,
	tp_class_estabelec_w,
	id_cat_dif_w,
	id_acid_trab_w,
	id_urg_emer_w,
	id_rce_espec_1_w,
	id_rce_atua_1_w,
	id_rce_espec_2_w,
	id_rce_atua_2_w,
	dt_ini_servico_w,
	dt_ini_contrato_w,
	nr_registro_ans_w,
	nm_diretor_tecnico_w,
	tp_disponibilidade_w,
	id_tab_propria_w,
	cd_perfil_assist_w,
	id_tp_prod_w,
	id_guia_medico_w,
	sg_cons_diretor_tecnico_w,
	cd_cons_diretor_tecnico_w,
	sg_uf_cons_diretor_tecnico_w,
	tipo_rede_min_w,
	id_guia_medico_espec_1_w,
	id_guia_medico_espec_2_w,
	id_guia_medico_atua_1_w,
	id_guia_medico_atua_2_w,
	particip_notivisa_w,
	particip_qualiss_ans_w,
	nr_rce_espec_1_w,
	nr_rce_espec_2_w,
	nr_rce_2_espec_1_w,
	nr_rce_2_espec_2_w,
	nr_rce_atua_1_w,
	nr_rce_atua_2_w,
	nr_rce_2_atua_1_w,
	nr_rce_2_atua_2_w,
	indic_pos_grad_w,
	nm_prest_w,
	id_publica_ans_w,
	nr_cbo_w,
	indic_residencia_w,
	cd_uni_prestadora_w,
	id_login_wsd_tiss_w,
	id_cadu_w,
	id_inativo_w,
	sg_conselho_w,
	nr_seq_prestador_w,
	ie_ind_ginec_obst_w,
	ie_sexo_w,
	dt_atualizacao_cad_w,
	cod_titulacao_1_w,
	cod_titulacao_2_w,
	indic_resid_espec_1_w,
	indic_resid_espec_2_w,
	indic_resid_area_1_w,
	indic_resid_area_2_w,
	ie_doutorado_pos_w,
	id_iso9001_w,
	indic_mestrado_w;
exit when C01%notfound;
	begin
	nr_seq_registro_w := nr_seq_registro_w + 1;
	
	ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '402' || tp_prest_w || cd_prest_w || cd_cgc_cpf_w || cd_insc_est_w || lpad(' ',8,' ') || 		
				cd_uf_conselho_w || lpad(' ',40,' ') || lpad(' ',40,' ') || tp_vinculo_w || cd_espec_1_w || cd_atua_1_w || cd_espec_2_w || cd_atua_2_w || 
				lpad(' ',12,' ') || dt_incl_uni_w || dt_excl_uni_w || tp_contratualizacao_w || tp_class_estabelec_w || id_cat_dif_w || id_acid_trab_w ||
				' ' || id_urg_emer_w || id_rce_espec_1_w || id_rce_atua_1_w || id_rce_espec_2_w || id_rce_atua_2_w || '   ' || dt_ini_servico_w || 
				dt_ini_contrato_w || nr_registro_ans_w || lpad(' ',40,' ') || lpad(' ',8,' ') || tp_disponibilidade_w || id_tab_propria_w || 
				cd_perfil_assist_w || id_tp_prod_w || id_guia_medico_w || sg_cons_diretor_tecnico_w || cd_cons_diretor_tecnico_w || 
				sg_uf_cons_diretor_tecnico_w || tipo_rede_min_w || id_guia_medico_espec_1_w || id_guia_medico_espec_2_w || id_guia_medico_atua_1_w || 
				id_guia_medico_atua_2_w || ' ' || particip_notivisa_w || particip_qualiss_ans_w || nr_rce_espec_1_w || nr_rce_espec_2_w ||
				nr_rce_2_espec_1_w || nr_rce_2_espec_2_w || nr_rce_atua_1_w || nr_rce_atua_2_w || nr_rce_2_atua_1_w || nr_rce_2_atua_2_w || 
				indic_pos_grad_w || '  ' || nm_prest_w || id_publica_ans_w || nr_cbo_w || ' ' || cd_uni_prestadora_w || 
				id_login_wsd_tiss_w || id_cadu_w || id_inativo_w || sg_conselho_w || nvl(ie_ind_ginec_obst_w, '0') || nvl(ie_sexo_w,' ') ||
				dt_atualizacao_cad_w || cod_titulacao_1_w || cod_titulacao_2_w || indic_resid_espec_1_w || indic_resid_espec_2_w ||
				indic_resid_area_1_w || indic_resid_area_2_w || nm_fantasia_w || ie_doutorado_pos_w || nr_conselho_w || id_iso9001_w || nm_diretor_tecnico_w || indic_mestrado_w;
	pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
	ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
	
	utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
	utl_file.fflush(arq_texto_w);
	ds_conteudo_w := null;
	cd_cgc_cpf_w := null;
	
	-- 403 ENDERE�O (OBRIGAT�RIO)
	open C02;
	loop
	fetch C02 into	
		nr_seq_ptu_endereco_w,
		tp_end_w,
		ds_end_w,
		nr_end_w,
		ds_bairro_w,
		cd_munic_w,
		nr_cep_w,
		nr_ddd_w,
		nr_fone_1_w,
		nr_fone_2_w,
		nr_fax_w,
		ds_email_w,
		ds_endereco_web_w,
		cd_cnes_w,
		nr_leitos_totais_w,
		nr_leitos_contrat_w,
		nr_leitos_psiquiat_w,
		nr_uti_adulto_w,
		nr_uti_neonatal_w,
		nr_uti_pediatria_w,
		cd_cgc_cpf_w,
		nr_leitos_intermed_neo_w,
		id_filial_w,
		ds_compl_w,
		nr_leitos_hosp_dia_w,
		nr_leitos_tot_clinic_w,
		nr_leitos_tot_cirur_w,
		nr_leitos_tot_obstr_w,
		nr_leitos_tot_pediat_w,
		nr_leitos_tot_psiqu_w,
		nr_latitude_w,
		nr_longitude_w,
		nr_leitos_intermed_w, 
		prest_acred_w,
		nr_referencia_end_w,
		ds_email_sec_w;
	exit when C02%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;
		
		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '403' || tp_end_w || ds_end_w || nr_end_w || lpad(' ',15,' ') || ds_bairro_w || cd_munic_w ||
					nr_cep_w || nr_ddd_w || nr_fone_1_w || nr_fone_2_w || nr_fax_w || lpad(' ',90,' ') || cd_cnes_w || 
					nr_leitos_totais_w || nr_leitos_contrat_w || nr_leitos_psiquiat_w || nr_uti_adulto_w || nr_uti_neonatal_w || 
					nr_uti_pediatria_w || cd_cgc_cpf_w || nr_leitos_intermed_neo_w || id_filial_w || ds_compl_w || nr_leitos_hosp_dia_w || 
					nr_leitos_tot_clinic_w || nr_leitos_tot_cirur_w || nr_leitos_tot_obstr_w || nr_leitos_tot_pediat_w || nr_leitos_tot_psiqu_w || 
					nr_latitude_w || nr_longitude_w || nr_leitos_intermed_w || prest_acred_w || nr_referencia_end_w || ds_email_w || ds_email_sec_w ||
					ds_endereco_web_w;
		pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		
		-- 404 GRUPO DE SERVI�O (OBRIGAT�RIO)
		open C03;
		loop
		fetch C03 into	
			cd_gr_serv_w;
		exit when C03%notfound;
			begin
			nr_seq_registro_w := nr_seq_registro_w + 1;
			
			ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '404' || cd_gr_serv_w;
			pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
			ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
			
			utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
			utl_file.fflush(arq_texto_w);
			ds_conteudo_w := null;
			
			end;
		end loop;
		close C03;
		
		end;
	end loop;
	close C02;
	
	-- 405 REDE REFERENCIADA (OBRIGAT�RIO)
	open C04;
	loop
	fetch C04 into	
		cd_rede_w,
		nm_rede_w;
	exit when C04%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;
		
		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '405' || cd_rede_w || ' ' || nm_rede_w;
		pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		
		end;
	end loop;
	close C04;
	
	-- 406 EXCLUS�O / SUBSTITUI��O (OPCIONAL)
	open C05;
	loop
	fetch C05 into	
		ie_nivel_exclusao_ptu_w,
		cd_exclusao_rede_w,
		cd_exclusao_pla_prod_w,
		nr_exclusao_end_w,
		dt_termino_prest_w,
		ie_substituicao_w,
		cd_cnpj_cpf_substit_w,
		dt_inicio_prest_w,
		cd_motivo_exclusao_w;
	exit when C05%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;
		
		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '406' || ie_nivel_exclusao_ptu_w || cd_exclusao_rede_w || cd_exclusao_pla_prod_w || nr_exclusao_end_w ||
					dt_termino_prest_w || ie_substituicao_w || cd_cnpj_cpf_substit_w || dt_inicio_prest_w || cd_motivo_exclusao_w;
		pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		end;
	end loop;
	close C05;
	
	-- 407 IMPOSTOS (OPCIONAL)
	open C06;
	loop
	fetch C06 into	
		cd_imposto_w,
		vl_aliquota_w;
	exit when C06%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;
		
		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '407' || cd_imposto_w || vl_aliquota_w;
		pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		end;
	end loop;
	close C06;
	
	-- 408 QUALIFICA��O DOS PRESTADORES DE SERVI�O (OPCIONAL) (Continua��o)
	open C07;
	loop
	fetch C07 into	
		cd_cgc_cpf_w,
		inst_acred_w,
		nivel_acred_w,
		nr_referencia_end_qualif_w;
	exit when C07%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;
		
		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '408' || cd_cgc_cpf_w || inst_acred_w || '  ' || nivel_acred_w || nr_referencia_end_qualif_w;
		pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		end;
	end loop;
	close C07;
	
	-- 410 OBSERVA��ES (OPCIONAL)
	open C08;
	loop
	fetch C08 into	
		ds_divulga_obs_w;
	exit when C08%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;
		
		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '410' || ds_divulga_obs_w;
		pls_hash_ptu_pck.elimina_carac_linha_clob(ds_conteudo_w);
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		end;
	end loop;
	close C08;
	
	end;
end loop;
close C01;

-- 499 TRAILER (OBRIGAT�RIO)
select	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'402'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'403'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'404'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'405'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'406'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'407'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'408'),1,7),'0'),7,'0'),
	lpad(nvl(substr(obter_qt_registro_ptu_a400(nr_seq_lote_p,'410'),1,7),'0'),7,'0')
into	qt_tot_r402_w,
	qt_tot_r403_w,
	qt_tot_r404_w,
	qt_tot_r405_w,
	qt_tot_r406_w,
	qt_tot_r407_w,
	qt_tot_r408_w,
	qt_tot_r410_w
from	dual;

nr_seq_registro_w := nr_seq_registro_w + 1;

ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '499' || qt_tot_r402_w || qt_tot_r403_w || qt_tot_r404_w || qt_tot_r405_w || qt_tot_r407_w || qt_tot_r408_w ||
			qt_tot_r410_w || qt_tot_r406_w;
ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);
ds_conteudo_w := null;

-- 998 - HASH (OBRIGAT�RIO)
nr_seq_registro_w :=	nr_seq_registro_w + 1;
ds_hash_a400_w	:=	pls_hash_ptu_pck.obter_hash_txt(ds_arquivo_w); -- Gerar HASH
ds_conteudo_w 	:= 	lpad(nr_seq_registro_w,8,'0') ||
			'998' ||
			ds_hash_a400_w;
			

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);
ds_conteudo_w := null;
			
update	ptu_movimento_prestador
set	ds_hash_a400	= ds_hash_a400_w
where	nr_sequencia	= nr_seq_lote_p;

-- A Unimed Origem do arquivo n�o deve gerar o Registro 999. Ele ser� gerado exclusivamente pela Central de Movimenta��es Batch (CMB).
-- 999 REGISTRO GERADO PELA CMB (OBRIGAT�RIO)
/*nr_seq_registro_w := nr_seq_registro_w + 1;

ds_conteudo_w :=	lpad(nr_seq_registro_w,8,'0') || '999' || 

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);
ds_conteudo_w := null;*/

end ptu_gerar_arq_mov_prestador_81;
/