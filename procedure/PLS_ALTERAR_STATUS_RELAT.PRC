create or replace
procedure pls_alterar_status_relat( nr_seq_relatorio_p	number,
				    ie_status_p		varchar2,
				    nm_usuario_p	Varchar2) is 
			
/*
ie_status_p
1 -  Pendente
2 - Em processo
3-  Com erro
4 - Gerado
*/			

begin

update	pls_relatorio
set	ie_status = ie_status_p
where	nr_sequencia = nr_seq_relatorio_p;

commit;

end pls_alterar_status_relat;
/