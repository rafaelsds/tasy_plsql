create or replace
procedure ctb_gerar_livro_aux_event_liq(	cd_empresa_p		varchar2,
						cd_estabelecimento_p	varchar2,
						dt_inicial_p		date,
						dt_final_p		date,	
						nm_usuario_p		Varchar2,
						ie_gerar_arquivo_p	varchar2) is 
						
dt_final_w					date;
nr_titulo_ww					number(10);
nr_contador_w					number(10) := 0;
vl_pagar_w					number(15,2) := 0;
vl_imposto_w					number(15,2);
vl_titulo_w					number(15,2);
vl_pagar_ww					number(15,2);
vl_liberado_ww					number(15,2);
vl_saldo_w					number(15,2);
vl_descontos_w					number(15,2);
cd_conta_contabil_w				conta_contabil.cd_conta_contabil%type;
/*Vari�veis gera��o de arquivo*/
ds_erro_w					varchar2(255);
ds_local_w					varchar2(255);
nm_arquivo_w					varchar2(255);
arq_texto_w					utl_file.file_type;
								
cursor c_linha is
	select	a.nm_prestador						|| ';' ||
		a.ds_tipo_documento					|| ';' ||
		a.nr_documento						|| ';' ||
		a.nr_titulo						|| ';' ||
		a.nr_evento						|| ';' ||
		a.nr_contrato						|| ';' ||
		a.dt_emissao						|| ';' ||
		a.dt_rescisao						|| ';' ||
		obter_valor_dominio(1666,a.ie_tipo_contratacao)		|| ';' ||
		obter_valor_dominio(1665,a.ie_segmentacao)		|| ';' ||
		a.nm_usuario_princ					|| ';' ||
		a.nr_cpf_cnpj						|| ';' ||
		a.nm_usuario_evento					|| ';' ||
		a.dt_ocorrencia						|| ';' ||
		a.dt_conhecimento					|| ';' ||
		a.dt_contabilizacao					|| ';' ||
		a.dt_vencimento						|| ';' ||
		a.dt_pagamento						|| ';' ||
		a.vl_evento						|| ';' ||
		a.vl_glosa						|| ';' ||
		a.vl_pagar						|| ';' ||
		a.cd_conta_contabil					|| ';' ||
		substr(ctb_obter_classif_conta(a.cd_conta_contabil, null, dt_final_w),1,255) ds_linha		
	from	w_ctb_livro_aux_event_liq a
	where	a.nm_usuario	= nm_usuario_p
	and 	a.ie_tipo_livro = 1
	and	dt_referencia = dt_final_w;

cursor c_conta is
	select 	c.cd_conta_contabil
	from 	conta_contabil c
	where	c.ie_tipo = 'A'
	and 	substr(ctb_obter_classif_conta(c.cd_conta_contabil, c.cd_classificacao, c.dt_inicio_vigencia),1,5) = '2.1.1';
	
Cursor c_titulo is
	select	nm_prestador,
		ds_tipo_documento,
		nr_documento,
		nr_titulo,
		nr_evento,
		nr_contrato,
		dt_emissao,
		dt_rescisao,
		ie_tipo_contratacao,
		ie_segmentacao,
		nm_usuario_princ,
		nr_cpf_cnpj,
		nm_usuario_evento,
		dt_ocorrencia,
		dt_conhecimento,
		dt_contabilizacao,
		dt_vencimento,
		ie_tipo_protocolo,
		dt_pagamento,
		nr_seq_pls_pag_prest,
		vl_evento,
		vl_liberado,
		vl_glosa,
		vl_saldo,
		vl_pagamento
	from	(	select 	substr(pls_obter_dados_prestador(d.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
				substr(t.nr_documento,1,255) nr_documento,
				t.nr_titulo nr_titulo,
				r.nr_seq_conta nr_evento,
				w.nr_sequencia nr_contrato,
				t.dt_emissao dt_emissao,
				s.dt_rescisao dt_rescisao,
				y.ie_tipo_contratacao ie_tipo_contratacao,
				y.ie_segmentacao ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF') nr_cpf_cnpj,
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
				(select	x.dt_procedimento
				from	pls_conta_proc	x
				where	x.nr_sequencia	= r.nr_seq_conta_proc
				and	d.nr_sequencia	= x.nr_seq_conta
				union all
				select	y.dt_atendimento
				from	pls_conta_mat	y
				where	y.nr_sequencia	= r.nr_seq_conta_mat
				and	d.nr_sequencia	= y.nr_seq_conta) dt_ocorrencia,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_conhecimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_contabilizacao,
				t.dt_vencimento_atual dt_vencimento,
				e.ie_tipo_protocolo ie_tipo_protocolo,
				t.dt_liquidacao dt_pagamento,
				t.nr_seq_pls_pag_prest nr_seq_pls_pag_prest,
				sum(r.vl_liberado + r.vl_glosa) vl_evento,
				sum(r.vl_liberado) vl_liberado,
				sum(round(r.vl_glosa * cc.vl_titulo / t.vl_titulo,2)) vl_glosa,
				max(round(obter_saldo_titulo_pagar(t.nr_titulo,dt_final_w),2)) vl_saldo,
				max(a.vl_pagamento) vl_pagamento
			from	pls_lote_pagamento b,
				pls_pagamento_prestador a,
				pls_pagamento_item c,
				pls_conta_medica_resumo r,
				pls_pagamento_nota n,
				pls_segurado s,
				pls_contrato_pagador p,
				pls_plano y,
				pls_contrato w,
				pls_conta d,
				pls_protocolo_conta e,
				titulo_pagar t,
				titulo_pagar_classif cc
			where	a.nr_seq_lote 		= b.nr_sequencia
			and	r.nr_seq_lote_pgto 	= b.nr_sequencia
			and	c.nr_seq_pagamento 	= a.nr_sequencia
			and	c.nr_sequencia		= r.nr_seq_pag_item
			and	r.nr_seq_segurado 	= s.nr_sequencia
			and	s.nr_seq_pagador 	= p.nr_sequencia
			and	r.nr_seq_conta 		= d.nr_sequencia
			and	d.nr_seq_protocolo	= e.nr_sequencia
			and	a.nr_seq_prestador 	= r.nr_seq_prestador_pgto
			and	a.nr_sequencia 		= t.nr_seq_pls_pag_prest
			and	t.nr_titulo 		= cc.nr_titulo
			and	a.nr_sequencia		= n.nr_seq_pagamento(+)
			and	d.nr_seq_plano 		= y.nr_sequencia(+)
			and	p.nr_seq_contrato 	= w.nr_sequencia(+)
			and	((t.dt_liquidacao is null)
			or 	(t.dt_liquidacao > dt_final_w))
			and	t.dt_emissao <= dt_final_w
			and	nvl(cd_estabelecimento_p,t.cd_estabelecimento) = t.cd_estabelecimento
			and	obter_empresa_estab(t.cd_estabelecimento) = cd_empresa_p
			and	cc.cd_conta_contabil = cd_conta_contabil_w
			and	t.ie_origem_titulo <> '4'
			and	t.vl_titulo > 0
			and	cc.vl_titulo <> 0
			--and	t.nr_titulo in (680618)
			group by substr(pls_obter_dados_prestador(d.nr_seq_prestador_exec,'N'),1,255),
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
				substr(t.nr_documento,1,255),
				t.nr_titulo,
				r.nr_seq_conta,
				w.nr_sequencia,
				t.dt_emissao,
				s.dt_rescisao,
				y.ie_tipo_contratacao,
				r.nr_seq_conta_proc,
				r.nr_seq_conta_mat,
				y.ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
				d.dt_atendimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				t.dt_vencimento_atual,
				e.ie_tipo_protocolo,
				t.dt_liquidacao,
				t.nr_seq_pls_pag_prest,
				d.nr_sequencia
			union all
			select	substr(obter_nome_pf_pj(d.cd_pessoa_fisica, d.cd_cgc),1,255) nm_prestador,
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
				to_char(t.nr_documento) nr_documento,
				t.nr_titulo nr_titulo,
				r.nr_seq_conta nr_evento,
				w.nr_sequencia nr_contrato,
				t.dt_emissao dt_emissao,
				s.dt_rescisao dt_rescisao,
				y.ie_tipo_contratacao ie_tipo_contratacao,
				y.ie_segmentacao ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF') nr_cpf_cnpj,
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
				r.dt_procedimento dt_ocorrencia,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_conhecimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_contabilizacao,
				t.dt_vencimento_atual dt_vencimento,
				e.ie_tipo_protocolo ie_tipo_protocolo,
				t.dt_liquidacao dt_pagamento,
				t.nr_seq_pls_pag_prest nr_seq_pls_pag_prest,
				sum(r.vl_liberado) vl_evento,
				sum(r.vl_liquido) vl_liberado,
				sum(r.vl_glosa) vl_glosa,
				max(round(obter_saldo_titulo_pagar(t.nr_titulo,dt_final_w),2)) vl_saldo,
				max(t.vl_titulo) vl_pagamento
			from	pls_conta			d,
				pls_protocolo_conta		e,
				pls_conta_proc			r,
				pls_segurado			s,
				pls_contrato			w,
				pls_plano			y,
				titulo_pagar			t
			where	e.nr_sequencia	= d.nr_seq_protocolo
			and	d.nr_sequencia	= r.nr_seq_conta
			and	s.nr_sequencia	= d.nr_seq_segurado
			and	w.nr_sequencia 	= s.nr_seq_contrato
			and	y.nr_sequencia 	= d.nr_seq_plano
			and	e.nr_sequencia 	= t.nr_seq_reembolso
			and	((t.dt_liquidacao is null)
			or 	(t.dt_liquidacao > dt_final_w))
			and	t.dt_emissao <= dt_final_w
			and	nvl(cd_estabelecimento_p,t.cd_estabelecimento) = t.cd_estabelecimento
			and	obter_empresa_estab(t.cd_estabelecimento) = cd_empresa_p
			and	r.cd_conta_cred = cd_conta_contabil_w
			and	t.ie_origem_titulo <> '4'
			and	t.vl_titulo > 0
			--and	t.nr_titulo in (680618)
			having sum(r.vl_liberado) <> 0
			group by substr(obter_nome_pf_pj(d.cd_pessoa_fisica, d.cd_cgc),1,255),
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
				to_char(t.nr_documento),
				t.nr_titulo,
				r.nr_seq_conta,
				w.nr_sequencia,
				t.dt_emissao,
				s.dt_rescisao,
				y.ie_tipo_contratacao,
				y.ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
				r.dt_procedimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				t.dt_vencimento_atual,
				e.ie_tipo_protocolo,
				t.dt_liquidacao,
				t.nr_seq_pls_pag_prest
			union all
			select	substr(obter_nome_pf_pj(d.cd_pessoa_fisica, d.cd_cgc),1,255) nm_prestador,
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
				to_char(t.nr_documento) nr_documento,
				t.nr_titulo nr_titulo,
				r.nr_seq_conta nr_evento,
				w.nr_sequencia nr_contrato,
				t.dt_emissao dt_emissao,
				s.dt_rescisao dt_rescisao,
				y.ie_tipo_contratacao ie_tipo_contratacao,
				y.ie_segmentacao ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF') nr_cpf_cnpj,
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
				r.dt_atendimento dt_ocorrencia,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_conhecimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_contabilizacao,
				t.dt_vencimento_atual dt_vencimento,
				e.ie_tipo_protocolo ie_tipo_protocolo,
				t.dt_liquidacao dt_pagamento,
				t.nr_seq_pls_pag_prest nr_seq_pls_pag_prest,
				sum(r.vl_liberado) vl_evento,
				sum(r.vl_liberado - r.vl_participacao) vl_liberado,
				sum(r.vl_glosa) vl_glosa,
				max(round(obter_saldo_titulo_pagar(t.nr_titulo,dt_final_w),2)) vl_saldo,
				max(t.vl_titulo) vl_pagamento
			from	pls_conta			d,
				pls_protocolo_conta		e,
				pls_conta_mat			r,
				pls_segurado			s,
				pls_contrato			w,
				pls_plano			y,
				titulo_pagar			t
			where	e.nr_sequencia	= d.nr_seq_protocolo
			and	d.nr_sequencia	= r.nr_seq_conta
			and	s.nr_sequencia	= d.nr_seq_segurado
			and	w.nr_sequencia 	= s.nr_seq_contrato
			and	y.nr_sequencia 	= d.nr_seq_plano
			and	e.nr_sequencia 	= t.nr_seq_reembolso
			and	((t.dt_liquidacao is null)
			or 	(t.dt_liquidacao > dt_final_w))
			and	t.dt_emissao <= dt_final_w
			and	nvl(cd_estabelecimento_p,t.cd_estabelecimento) = t.cd_estabelecimento
			and	obter_empresa_estab(t.cd_estabelecimento) = cd_empresa_p
			and	r.cd_conta_cred = cd_conta_contabil_w
			and	t.ie_origem_titulo <> '4'
			and	t.vl_titulo > 0
			--and	t.nr_titulo in (680618)
			having sum(r.vl_liberado) <> 0
			group by substr(obter_nome_pf_pj(d.cd_pessoa_fisica, d.cd_cgc),1,255),
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
				to_char(t.nr_documento),
				t.nr_titulo,
				r.nr_seq_conta,
				w.nr_sequencia,
				t.dt_emissao,
				s.dt_rescisao,
				y.ie_tipo_contratacao,
				y.ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
				r.dt_atendimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				t.dt_vencimento_atual,
				e.ie_tipo_protocolo,
				t.dt_liquidacao,
				t.nr_seq_pls_pag_prest
			union all
			-- Novo
			select 	substr(pls_obter_dados_prestador(d.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
				substr(t.nr_documento,1,255) nr_documento,
				t.nr_titulo nr_titulo,
				r.nr_seq_conta nr_evento,
				w.nr_sequencia nr_contrato,
				t.dt_emissao dt_emissao,
				s.dt_rescisao dt_rescisao,
				y.ie_tipo_contratacao ie_tipo_contratacao,
				y.ie_segmentacao ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF') nr_cpf_cnpj,
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
				z.dt_procedimento dt_ocorrencia,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_conhecimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_contabilizacao,
				t.dt_vencimento_atual dt_vencimento,
				e.ie_tipo_protocolo ie_tipo_protocolo,
				t.dt_liquidacao dt_pagamento,
				t.nr_seq_pls_pag_prest nr_seq_pls_pag_prest,
				sum(r.vl_liberado + r.vl_glosa) vl_evento,
				sum(r.vl_liberado) vl_liberado,
				sum(r.vl_glosa) vl_glosa,
				max(round(obter_saldo_titulo_pagar(t.nr_titulo,dt_final_w),2)) vl_saldo,
				max(c.vl_item) vl_pagamento
			from	pls_lote_pagamento b,
				pls_pagamento_prestador a,
				pls_pagamento_item c,
				pls_conta_medica_resumo r,
				pls_pagamento_nota n,
				pls_segurado s,
				pls_contrato_pagador p,
				pls_plano y,
				pls_contrato w,
				pls_conta d,
				pls_protocolo_conta e,
				titulo_pagar t,
				pls_conta_proc z
			where	a.nr_seq_lote 		= b.nr_sequencia
			and	r.nr_seq_lote_pgto 	= b.nr_sequencia
			and	c.nr_seq_pagamento 	= a.nr_sequencia
			and	c.nr_sequencia		= r.nr_seq_pag_item
			and	r.nr_seq_segurado 	= s.nr_sequencia
			and	s.nr_seq_pagador 	= p.nr_sequencia
			and	r.nr_seq_conta 		= d.nr_sequencia
			and	d.nr_seq_protocolo	= e.nr_sequencia
			and	a.nr_seq_prestador 	= r.nr_seq_prestador_pgto
			and	a.nr_sequencia 		= t.nr_seq_pls_pag_prest
			and	a.nr_sequencia		= n.nr_seq_pagamento(+)
			and	d.nr_seq_plano 		= y.nr_sequencia(+)
			and	p.nr_seq_contrato 	= w.nr_sequencia(+)
			and	z.nr_seq_conta		= d.nr_sequencia
			and	r.nr_seq_conta_proc	= z.nr_sequencia
			and	((t.dt_liquidacao is null)
			or 	(t.dt_liquidacao > dt_final_w))
			and	t.dt_emissao <= dt_final_w
			and	nvl(cd_estabelecimento_p,t.cd_estabelecimento) = t.cd_estabelecimento
			and	obter_empresa_estab(t.cd_estabelecimento) = cd_empresa_p
			and	nvl(z.cd_conta_cred, z.cd_conta_provisao_cred) = cd_conta_contabil_w
			and	t.ie_origem_titulo <> '4'
			and	t.vl_titulo > 0
			--and	t.nr_titulo in (680618)
			and	not exists(	select	1
						from	titulo_pagar_classif cc
						where	cc.nr_titulo = t.nr_titulo
						and	nvl(cc.cd_conta_contabil,'X') <> 'X')
			group by substr(pls_obter_dados_prestador(d.nr_seq_prestador_exec,'N'),1,255),
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
				substr(t.nr_documento,1,255),
				t.nr_titulo,
				r.nr_seq_conta,
				w.nr_sequencia,
				t.dt_emissao,
				s.dt_rescisao,
				y.ie_tipo_contratacao,
				y.ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
				z.dt_procedimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				t.dt_vencimento_atual,
				e.ie_tipo_protocolo,
				t.dt_liquidacao,
				t.nr_seq_pls_pag_prest
			union all
			select 	substr(pls_obter_dados_prestador(d.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
				substr(t.nr_documento,1,255) nr_documento,
				t.nr_titulo nr_titulo,
				r.nr_seq_conta nr_evento,
				w.nr_sequencia nr_contrato,
				t.dt_emissao dt_emissao,
				s.dt_rescisao dt_rescisao,
				y.ie_tipo_contratacao ie_tipo_contratacao,
				y.ie_segmentacao ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF') nr_cpf_cnpj,
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
				z.dt_atendimento dt_ocorrencia,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_conhecimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia) dt_contabilizacao,
				t.dt_vencimento_atual dt_vencimento,
				e.ie_tipo_protocolo ie_tipo_protocolo,
				t.dt_liquidacao dt_pagamento,
				t.nr_seq_pls_pag_prest nr_seq_pls_pag_prest,
				sum(r.vl_liberado + r.vl_glosa) vl_evento,
				sum(r.vl_liberado) vl_liberado,
				sum(r.vl_glosa) vl_glosa,
				max(round(obter_saldo_titulo_pagar(t.nr_titulo,dt_final_w),2)) vl_saldo,
				max(c.vl_item) vl_pagamento
			from	pls_lote_pagamento b,
				pls_pagamento_prestador a,
				pls_pagamento_item c,
				pls_conta_medica_resumo r,
				pls_pagamento_nota n,
				pls_segurado s,
				pls_contrato_pagador p,
				pls_plano y,
				pls_contrato w,
				pls_conta d,
				pls_protocolo_conta e,
				titulo_pagar t,
				pls_conta_mat z
			where	a.nr_seq_lote 		= b.nr_sequencia
			and	r.nr_seq_lote_pgto 	= b.nr_sequencia
			and	c.nr_seq_pagamento 	= a.nr_sequencia
			and	c.nr_sequencia		= r.nr_seq_pag_item
			and	r.nr_seq_segurado 	= s.nr_sequencia
			and	s.nr_seq_pagador 	= p.nr_sequencia
			and	r.nr_seq_conta 		= d.nr_sequencia
			and	d.nr_seq_protocolo	= e.nr_sequencia
			and	a.nr_seq_prestador 	= r.nr_seq_prestador_pgto
			and	a.nr_sequencia 		= t.nr_seq_pls_pag_prest
			and	a.nr_sequencia		= n.nr_seq_pagamento(+)
			and	d.nr_seq_plano 		= y.nr_sequencia(+)
			and	p.nr_seq_contrato 	= w.nr_sequencia(+)
			and	z.nr_seq_conta		= d.nr_sequencia
			and	r.nr_seq_conta_mat	= z.nr_sequencia
			and	((t.dt_liquidacao is null)
			or 	(t.dt_liquidacao > dt_final_w))
			and	t.dt_emissao <= dt_final_w
			and	nvl(cd_estabelecimento_p,t.cd_estabelecimento) = t.cd_estabelecimento
			and	obter_empresa_estab(t.cd_estabelecimento) = cd_empresa_p
			and	nvl(z.cd_conta_cred, z.cd_conta_provisao_cred) = cd_conta_contabil_w
			and	t.ie_origem_titulo <> '4'
			and	t.vl_titulo > 0
			--and	t.nr_titulo in (680618)
			and	not exists(	select	1
						from	titulo_pagar_classif cc
						where	cc.nr_titulo = t.nr_titulo
						and	nvl(cc.cd_conta_contabil,'X') <> 'X')
			group by substr(pls_obter_dados_prestador(d.nr_seq_prestador_exec,'N'),1,255),
				decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
				substr(t.nr_documento,1,255),
				t.nr_titulo,
				r.nr_seq_conta,
				w.nr_sequencia,
				t.dt_emissao,
				s.dt_rescisao,
				y.ie_tipo_contratacao,
				y.ie_segmentacao,
				substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
				pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),
				substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
				z.dt_atendimento,
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				nvl(e.dt_recebimento,e.dt_mes_competencia),
				t.dt_vencimento_atual,
				e.ie_tipo_protocolo,
				t.dt_liquidacao,
				t.nr_seq_pls_pag_prest
		union all
		select	substr(pls_obter_dados_prestador(b.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
			decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
			substr(t.nr_documento,1,255) nr_documento,
			t.nr_titulo nr_titulo,
			b.nr_sequencia nr_evento,
			w.nr_sequencia nr_contrato,
			t.dt_emissao dt_emissao,
			s.dt_rescisao dt_rescisao,
			y.ie_tipo_contratacao ie_tipo_contratacao,
			y.ie_segmentacao ie_segmentacao,
			substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
			pls_obter_dados_pagador(s.nr_seq_pagador,'CPF') nr_cpf_cnpj,
			substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
			(select	x.dt_procedimento
			from	pls_conta_proc	x
			where	x.nr_sequencia	= e.nr_seq_proc_rec
			and	b.nr_sequencia	= x.nr_seq_conta
			union all
			select	y.dt_atendimento
			from	pls_conta_mat	y
			where	y.nr_sequencia	= e.nr_seq_mat_rec
			and	b.nr_sequencia	= y.nr_seq_conta) dt_ocorrencia,
			nvl(a.dt_recebimento,a.dt_mes_competencia) dt_conhecimento,
			nvl(a.dt_recebimento,a.dt_mes_competencia) dt_contabilizacao,
			t.dt_vencimento_atual dt_vencimento,
			a.ie_tipo_protocolo ie_tipo_protocolo,
			t.dt_liquidacao dt_pagamento,
			t.nr_seq_pls_pag_prest nr_seq_pls_pag_prest,
			sum(e.vl_liberado + e.vl_glosa) vl_evento,
			sum(e.vl_liberado) vl_liberado,
			sum(e.vl_glosa) vl_glosa,
			max(round(obter_saldo_titulo_pagar(t.nr_titulo,dt_final_w),2)) vl_saldo,
			max(t.vl_titulo) vl_pagamento
		from	titulo_pagar			t,
			pls_pagamento_prestador		j,
			pls_lote_pagamento		i,
			pls_conta_rec_resumo_item	e,
			pls_rec_glosa_conta 		f,
			pls_protocolo_conta		a,
			pls_conta			b,
			pls_segurado			s,
			pls_plano 			y,
			pls_contrato			w,
			pls_pagamento_item		x
		where	t.nr_seq_pls_pag_prest		= j.nr_sequencia
		and	j.nr_seq_lote			= i.nr_sequencia
		and	e.nr_seq_lote_pgto		= i.nr_sequencia(+)
		and	e.nr_seq_conta_rec		= f.nr_sequencia
		and	f.nr_seq_conta			= b.nr_sequencia
		and	a.nr_sequencia			= b.nr_seq_protocolo
		and	b.nr_seq_segurado		= s.nr_sequencia(+)
		and	b.nr_seq_plano			= y.nr_sequencia(+)
		and	s.nr_seq_contrato		= w.nr_sequencia(+)
		and	x.nr_sequencia			= e.nr_seq_pag_item
		and	j.nr_sequencia			= x.nr_seq_pagamento
		and	((t.dt_liquidacao is null)
		or 	(t.dt_liquidacao > dt_final_w))
		and	t.dt_emissao <= dt_final_w
		and	nvl(cd_estabelecimento_p,t.cd_estabelecimento) = t.cd_estabelecimento
		and	obter_empresa_estab(t.cd_estabelecimento) = cd_empresa_p
		and	t.ie_origem_titulo <> '4'
		and	t.vl_titulo > 0
		--and	t.nr_titulo in (680618)
		and	e.cd_conta_deb = cd_conta_contabil_w
		group by substr(pls_obter_dados_prestador(b.nr_seq_prestador_exec,'N'),1,255),
			decode(nvl(t.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
			substr(t.nr_documento,1,255),
			t.nr_titulo,
			b.nr_sequencia,
			w.nr_sequencia,
			t.dt_emissao,
			s.dt_rescisao,
			y.ie_tipo_contratacao,
			e.nr_seq_proc_rec,
			e.nr_seq_mat_rec,
			y.ie_segmentacao,
			substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
			pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),
			substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
			b.dt_atendimento,
			nvl(a.dt_recebimento,a.dt_mes_competencia),
			nvl(a.dt_recebimento,a.dt_mes_competencia),
			t.dt_vencimento_atual,
			a.ie_tipo_protocolo,
			t.dt_liquidacao,
			t.nr_seq_pls_pag_prest,
			b.nr_sequencia)
	order by nr_titulo asc, vl_evento asc;
				
vet_titulo c_titulo%rowtype;

begin

dt_final_w	:= fim_dia(dt_final_p);

delete
from	w_ctb_livro_aux_event_liq a
where	a.nm_usuario	= nm_usuario_p
and	a.ie_tipo_livro = 1
and 	dt_referencia = dt_final_w;

commit;

nr_titulo_ww	:= 0;
vl_pagar_ww	:= 0;
vl_liberado_ww	:= 0;

open c_conta;
loop
fetch c_conta into	
	cd_conta_contabil_w;
exit when c_conta%notfound;
	begin
	
	open c_titulo;
	loop
	fetch c_titulo into	
		vet_titulo;
	exit when c_titulo%notfound;
		begin
		nr_contador_w := nr_contador_w + 1;
		
		select	sum(vl_imposto)
		into	vl_imposto_w
		from	titulo_pagar_imposto i,
			tributo c
		where	i.cd_tributo = c.cd_tributo
		and	i.nr_titulo = vet_titulo.nr_titulo
		and	i.ie_pago_prev = 'V'
		and	nvl(c.ie_saldo_tit_pagar,'S')	= 'S'
		and	nvl(c.ie_baixa_titulo,'N') = 'N';

		vl_descontos_w := 0;
		
		if (nvl(vet_titulo.nr_seq_pls_pag_prest,0) <> 0) then
			select	sum(abs(i.vl_item))
			into	vl_descontos_w
			from	pls_pagamento_item i,
				pls_pagamento_prestador p,
				pls_evento e
			where	i.nr_seq_pagamento = p.nr_sequencia
			and	i.nr_seq_evento = e.nr_sequencia
			and	e.ie_tipo_evento = 'F'
			and	e.ie_natureza = 'D'
			and	p.nr_sequencia = vet_titulo.nr_seq_pls_pag_prest;		
		end if;
		
		vl_saldo_w 	:= vet_titulo.vl_saldo - nvl(vl_imposto_w,0) - nvl(vl_descontos_w,0);
		vl_pagar_w 	:= round(dividir(vl_saldo_w * vet_titulo.vl_liberado, vet_titulo.vl_pagamento),2);
		
		if (nr_titulo_ww <> 0) then
			if (vet_titulo.nr_titulo <> nr_titulo_ww) then
				nr_titulo_ww := vet_titulo.nr_titulo;
				vl_pagar_ww 	:= vl_pagar_w;
				vl_liberado_ww 	:= vet_titulo.vl_evento;	
			else
				vl_liberado_ww := vl_liberado_ww + vet_titulo.vl_liberado;
				vl_pagar_ww  := vl_pagar_ww + vl_pagar_w;
				
				if (vl_liberado_ww = vet_titulo.vl_pagamento) then

					if (vl_saldo_w > vl_pagar_ww) then
						vl_pagar_w := vl_pagar_w + (vl_saldo_w - vl_pagar_ww);
					else
						vl_pagar_w := vl_pagar_w - (vl_pagar_ww - vl_saldo_w);
					end if;

				end if;
			end if;
		else
			vl_pagar_ww := vl_pagar_ww + vl_pagar_w;
			vl_liberado_ww := vl_liberado_ww + vet_titulo.vl_liberado;
			nr_titulo_ww := vet_titulo.nr_titulo;
		end if;
		
		insert into w_ctb_livro_aux_event_liq(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_prestador,
			ds_tipo_documento,
			nr_documento,
			nr_titulo,
			nr_evento,
			nr_contrato,
			dt_emissao,
			dt_rescisao,
			ie_tipo_contratacao,
			ie_segmentacao,
			nm_usuario_princ,
			nr_cpf_cnpj,
			nm_usuario_evento,
			dt_ocorrencia,
			dt_conhecimento,
			dt_contabilizacao,
			dt_vencimento,
			dt_pagamento,
			vl_evento,
			vl_pagar,
			vl_glosa,
			ie_tipo_livro,
			dt_referencia,
			cd_conta_contabil)
		values(	w_ctb_livro_aux_event_liq_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			vet_titulo.nm_prestador,
			vet_titulo.ds_tipo_documento,
			vet_titulo.nr_documento,
			vet_titulo.nr_titulo,
			vet_titulo.nr_evento,
			vet_titulo.nr_contrato,
			vet_titulo.dt_emissao,
			vet_titulo.dt_rescisao,
			vet_titulo.ie_tipo_contratacao,
			vet_titulo.ie_segmentacao,
		        vet_titulo.nm_usuario_princ,
		        vet_titulo.nr_cpf_cnpj,
			vet_titulo.nm_usuario_evento,
			vet_titulo.dt_ocorrencia,
			vet_titulo.dt_conhecimento,
			vet_titulo.dt_contabilizacao,
			vet_titulo.dt_vencimento,
			vet_titulo.dt_pagamento,
			vet_titulo.vl_evento,
			vl_pagar_w,
			vet_titulo.vl_glosa,
			1,
			dt_final_w,
			cd_conta_contabil_w);
		
		if (mod(nr_contador_w, 1000) = 0) then
			commit;
		end if;
		
		end;
	end loop;
	close c_titulo;

	end;
end loop;
close c_conta;

commit;

if	(ie_gerar_arquivo_p = 'S') then
	begin
	nm_arquivo_w	:= 'LivroEventLiq' || to_char(sysdate,'ddmmyyyyhh24miss') || nm_usuario_p || '.csv';
	
	obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

	arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W'); --arq_texto_w := utl_file.fopen('/srvfs03/FINANCEIRO/TASY/',nm_arquivo_w,'W');

	utl_file.put_line(arq_texto_w, obter_desc_expressao(779832));
	utl_file.fflush(arq_texto_w);
	for vetl in c_linha loop
		begin
		utl_file.put_line(arq_texto_w,vetl.ds_linha);
		utl_file.fflush(arq_texto_w);
		end;
	end loop;
	end;
end if;

end ctb_gerar_livro_aux_event_liq;
/