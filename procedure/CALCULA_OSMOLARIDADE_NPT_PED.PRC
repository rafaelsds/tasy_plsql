create or replace
procedure calcula_osmolaridade_npt_ped(nr_sequencia_p	number) is

qt_potassio_w				number(30,16);
qt_vol_calcio_w				number(30,16);
qt_vol_magnesio_w			number(30,16);
qt_vol_sodio_w				number(30,16);
qt_vol_glicose_w			number(30,16);
qt_vol_lip_ml_w				number(30,16);
qt_vol_prot_w				number(30,16);
qt_fosforo_w				number(30,16);
qt_osmolaridade_total_w		number(30,16);
qt_vol_total_w				number(30,16);
qt_peso_w					number(15,3);
qt_vol_glutamina_w			number(30,16);
ie_arredonda_npt_w			parametro_medico.ie_arredondar_npt%type := 'N';

cursor c01 is
select	qt_diaria
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'L';

begin

select	max(qt_volume_diario),
		max(qt_peso)
into	qt_vol_total_w,
		qt_peso_w
from	nut_pac
where	nr_sequencia = nr_sequencia_p;

select 	nvl(max(ie_arredondar_npt),'N')
into	ie_arredonda_npt_w
from	parametro_medico
where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

--- Lipideos
select	nvl(sum(qt_diaria),0)
into	qt_vol_lip_ml_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'L';

--- Carboidratos
select	nvl(sum(a.qt_diaria),0)
into	qt_vol_glicose_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'C';

--- Prote�nas n�o glutamina
if (ie_arredonda_npt_w = 'S') then
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_prot_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'P'
	and		b.ie_glutamina		= 'N';
else
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_prot_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'P';
end if;
--- Glutamina
select	nvl(sum(a.qt_volume_final),0)
into	qt_vol_glutamina_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'P'
and		b.ie_glutamina 	= 'S';

--- S�dio
select	(nvl(sum(a.qt_diaria),0))
into	qt_vol_sodio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'N'; 

--- Magn�sio
select	nvl(sum(a.qt_diaria),0)
into	qt_vol_magnesio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'M'; 

--- C�lcio
select	nvl(sum(a.qt_diaria),0)
into	qt_vol_calcio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'I';

--- Pot�ssio
select	nvl(sum(qt_diaria),0)
into	qt_potassio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'K';

if (ie_arredonda_npt_w = 'S') then
	qt_osmolaridade_total_w	:= dividir_sem_round(((qt_vol_prot_w * 11) + ((qt_vol_glutamina_w * 0.2) * 11) + (qt_vol_lip_ml_w * 0.3) + (qt_vol_glicose_w * 5.5) + (( qt_vol_sodio_w + qt_potassio_w + qt_vol_magnesio_w + qt_vol_calcio_w) * 2)), qt_vol_total_w) *1000;
else
	qt_osmolaridade_total_w	:= dividir_sem_round(((qt_vol_prot_w * 11) + (qt_vol_lip_ml_w * 0.3) + (qt_vol_glicose_w * 5.5) + (( qt_vol_sodio_w + qt_potassio_w + qt_vol_magnesio_w + qt_vol_calcio_w) * 2)), qt_vol_total_w) *1000;
end if;	

update	nut_pac
set		qt_osmolaridade_total	= round(qt_osmolaridade_total_w,1)
where	nr_sequencia			= nr_sequencia_p;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end calcula_osmolaridade_npt_ped;
/