create or replace procedure Gerar_laudo_synapse_hl7(
                                        nr_seg_reg_p            number,
					nm_medico_solicitante_p	varchar,
					dt_aprovacao_p		date,
					cd_medico_p		varchar2,
                                        nr_seq_laudo_p  out     number) is
nr_prescricao_w         number(14);
nr_seq_prescricao_w     number(10);
nr_seq_propaci_w        number(10);
nr_seq_proc_interno_w   number(15);
cd_procedimento_w       number(15);
ie_origem_proced_w      number(10);
qt_procedimento_w       number(15);
cd_setor_atendimento_w  number(10);
cd_medico_exec_w        varchar2(10);
nr_seq_exame_w          number(15);
ie_lado_w               varchar2(15);
dt_prev_execucao_w      date;
nr_atendimento_w        number(10);
dt_entrada_unidade_w    date;
nr_laudo_w              number(10);
cd_medico_resp_w        pessoa_fisica.cd_pessoa_fisica%type;
qt_existe_medico_w      number(10);
ds_laudo_w              long;
ds_laudo_copia_w        long;
nr_seq_laudo_w          number(10);
--nr_seq_laudo_ant_w      number(10);
--nr_seq_laudo_atual_w    number(10);
nr_seq_copia_w          number(10);

cd_laudo_externo_w      number(10);
cd_medico_laudante_w    varchar2(10);
ds_titulo_laudo_w       varchar2(255);
dt_liberacao_laudo_w    date;
ie_existe_laudo_w       varchar2(1);
ie_tipo_ordem_w         varchar2(5);
nm_usuario_w            varchar2(30);
nr_acesso_dicom_w       varchar2(30);
nr_seq_prescr_w         number(10);
nr_crm_w                varchar2(20);
uf_crm_w                w_integracao_laudo_hl7.uf_crm%type;
ie_insere_medico_solic_w varchar2(1);

dt_procedimento_w       date;
ie_status_execucao_w    varchar2(3);

cd_estabelecimento_w    number(4);
ie_alterar_medico_conta_w       varchar2(2);
ie_alterar_medico_exec_conta_w  varchar2(2);


cursor c01 is
  select nr_sequencia
  from  laudo_paciente
  where nr_prescricao = nr_prescricao_w
  and   nr_seq_prescricao = nr_seq_prescricao_w;
c01_w c01%rowtype;

begin

gravar_log_cdi(16322,'nr_seg_reg_p=' || nr_seg_reg_p,'Tasy');


nr_seq_laudo_p := 0;

select  MAX(vl_parametro)
into    ie_insere_medico_solic_w
from    funcao_parametro
where   cd_funcao = 28
and     nr_sequencia = 78;

begin

        select  nr_acc_number,
                cd_medico_laudante,
                ie_tipo_ordem,
                cd_laudo_externo,
        --      ds_laudo,
                dt_liberacao_laudo,
                nr_crm,
                uf_crm
        into    nr_acesso_dicom_w,
                cd_medico_laudante_w,
                ie_tipo_ordem_w,
                cd_laudo_externo_w,
        --      ds_laudo_w,
                dt_liberacao_laudo_w,
                nr_crm_w,
                uf_crm_w
        from    w_integracao_laudo_hl7
        where   nr_sequencia    = nr_seg_reg_p;

exception
        when no_data_found then
                Wheb_mensagem_pck.exibir_mensagem_abort(192823, 'NR_SEG_REG_P='|| nr_seg_reg_p);
end;

gravar_log_cdi(16322,'nr_seg_reg_p=' || nr_seg_reg_p || ' ' || 'nr_acesso_dicom_w=' || nr_acesso_dicom_w,'Tasy');


if      (nr_acesso_dicom_w is not null) then
        begin

        select  nr_prescricao,
                nr_sequencia,
                nr_seq_proc_interno,
                cd_procedimento,
                ie_origem_proced,
                qt_procedimento,
                cd_setor_atendimento,
                cd_medico_exec,
                nr_seq_exame,
                nvl(ie_lado,'A'),
                dt_prev_execucao
        into    nr_prescricao_w,
                nr_seq_prescricao_w,
                nr_seq_proc_interno_w,
                cd_procedimento_w,
                ie_origem_proced_w,
                qt_procedimento_w,
                cd_setor_atendimento_w,
                cd_medico_exec_w,
                nr_seq_exame_w,
                ie_lado_w,
                dt_prev_execucao_w
        from    prescr_procedimento
        where   nr_acesso_dicom = nr_acesso_dicom_w;

        exception
        when no_data_found then
                Wheb_mensagem_pck.exibir_mensagem_abort(192827,'NR_ACESSO_DICOM_P='|| nr_acesso_dicom_w);
        end;
end if;


gravar_log_cdi(16322,'nr_seg_reg_p=' || nr_seg_reg_p || ' ' ||
		'nr_acesso_dicom_w=' || nr_acesso_dicom_w ||
		' nr_prescricao_w=' || nr_prescricao_w ||
		' nr_seq_prescricao_w=' || nr_seq_prescricao_w,'Tasy');

if      (nvl(nr_prescricao_w,0) > 0) and
        (nvl(nr_seq_prescricao_w,0) > 0) then
        begin

        select  nvl(max(nr_sequencia),0),
                max(nr_atendimento),
                max(dt_entrada_unidade),
                max(dt_procedimento)
        into    nr_seq_propaci_w,
                nr_atendimento_w,
                dt_entrada_unidade_w,
                dt_procedimento_w
        from    procedimento_paciente
        where   nr_prescricao           = nr_prescricao_w
        and     nr_sequencia_prescricao = nr_seq_prescricao_w;

        if      (nr_seq_propaci_w = 0) then
                begin

                Gerar_Proc_Pac_item_Prescr(     nr_prescricao_w,
                                                nr_seq_prescricao_w,
                                                null,
                                                null,
                                                nr_seq_proc_interno_w,
                                                cd_procedimento_w,
                                                ie_origem_proced_w,
                                                qt_procedimento_w,
                                                cd_setor_atendimento_w,
                                                9,
                                                dt_prev_execucao_w,
                                                'Integra��o',
                                                cd_medico_exec_w,
                                                null,
                                                ie_lado_w,
                                                null);

                select  max(nr_sequencia),
                        max(nr_atendimento),
                        max(dt_entrada_unidade),
                        max(dt_procedimento)
                into    nr_seq_propaci_w,
                        nr_atendimento_w,
                        dt_entrada_unidade_w,
                        dt_procedimento_w
                from    procedimento_paciente
                where   nr_prescricao           = nr_prescricao_w
                and     nr_sequencia_prescricao = nr_seq_prescricao_w;

                end;
        end if;


      	gravar_log_cdi(16322,   'nr_seg_reg_p=' || nr_seg_reg_p || ' ' ||
                            		'nr_acesso_dicom_w=' || nr_acesso_dicom_w ||
                            		' nr_seq_propaci_w=' || nr_seq_propaci_w ||
                            		' nr_seq_prescricao_w=' || nr_seq_prescricao_w,'Tasy');

        select  nvl(max(nr_laudo),0) + 1
        into    nr_laudo_w
        from    laudo_paciente
        where   nr_atendimento  = nr_atendimento_w;

        select  laudo_paciente_seq.nextval
        into    nr_seq_laudo_w
        from    dual;

        open c01;
        loop
        fetch c01 into c01_w;
          exit when c01%notfound;
            begin

            GERAR_COPIA_LAUDO_PADRAO( c01_w.nr_sequencia,
                                      nr_seq_laudo_w, null,
                                      'Integra��o',
                                      'Excluido na integra��o');


            update  laudo_paciente_copia
            set     nr_seq_laudo = nr_seq_laudo_w
            where   nr_seq_laudo = c01_w.nr_sequencia;

            update  LAUDO_PACIENTE_MEDICO
            set     nr_seq_laudo = nr_seq_laudo_w
            where   nr_seq_laudo = c01_w.nr_sequencia;

            delete  laudo_paciente_pdf
            where   nr_seq_laudo = c01_w.nr_sequencia;

            delete  laudo_paciente
            where   nr_sequencia = c01_w.nr_sequencia;

            end;
        end loop;
        close c01;

        /*select  nvl(max(nr_sequencia),0)
        into    nr_seq_laudo_ant_w
        from    laudo_paciente
        where   cd_laudo_externo = cd_laudo_externo_w
        and     nr_seq_proc <> nr_seq_propaci_w;

        if      (nr_seq_laudo_ant_w > 0) then

                update  procedimento_paciente
                set     nr_laudo        = nr_seq_laudo_ant_w
                where   nr_sequencia    = nr_seq_propaci_w;

        else*/



        /*select  nvl(max(nr_sequencia),0)
        into    nr_seq_laudo_atual_w
        from    laudo_paciente
        where   cd_laudo_externo = cd_laudo_externo_w
        and     nr_seq_proc = nr_seq_propaci_w;*/


        select  substr(obter_desc_prescr_proc_laudo(p.cd_procedimento,
                        p.ie_origem_proced,
                        p.nr_seq_proc_interno,
                        p.ie_lado,
                        nr_seq_propaci_w),1,255)
        into    ds_titulo_laudo_w
        from    prescr_procedimento p
        where   nr_prescricao   = nr_prescricao_w
        and     nr_sequencia    = nr_seq_prescricao_w;

	select	max(a.cd_pessoa_fisica)
	into	cd_medico_resp_w
	from	pessoa_fisica a
	where	((a.cd_pessoa_fisica = cd_medico_p) or (cd_sistema_ant = cd_medico_p));
	
        insert into laudo_paciente(
                nr_sequencia,
                nr_atendimento,
                dt_entrada_unidade,
                nr_laudo,
                nm_usuario,
                dt_atualizacao,
                cd_medico_resp,
                ds_titulo_laudo,
                dt_laudo,
                nr_prescricao,
        --      ds_laudo,
                nr_seq_proc,
                nr_seq_prescricao,
                dt_liberacao,
                qt_imagem,
                nm_medico_solicitante,
                ie_status_laudo,
                cd_laudo_externo,
                dt_exame,
		dt_aprovacao)
        values( nr_seq_laudo_w,
                nr_atendimento_w,
                dt_entrada_unidade_w,
                nr_laudo_w,
                'Integra��o',
                sysdate,
                cd_medico_resp_w,
                ds_titulo_laudo_w,
                dt_aprovacao_p,
                nr_prescricao_w,
        --      ds_laudo_w,
                nr_seq_propaci_w,
                nr_seq_prescricao_w,
                dt_aprovacao_p,
                0,
                nm_medico_solicitante_p,
                'LL',
                cd_laudo_externo_w,
                dt_procedimento_w,
		dt_aprovacao_p);

        nr_seq_laudo_p := nr_seq_laudo_w;


        select  max(cd_estabelecimento)
        into    cd_estabelecimento_w
        from    prescr_medica
        where   nr_prescricao = nr_prescricao_w;

        ie_alterar_medico_conta_w       := Obter_Valor_Param_Usuario(28 ,101 , obter_perfil_ativo ,null,cd_estabelecimento_w);
        ie_alterar_medico_exec_conta_w  := Obter_Valor_Param_Usuario(28 ,112 , obter_perfil_ativo ,null,cd_estabelecimento_w);


        if      (ie_alterar_medico_conta_w = 'S') then
                Atualizar_Propaci_Medico_Laudo(nr_seq_laudo_w,'EX','Integra��o');
        end if;

        if      (ie_alterar_medico_exec_conta_w = 'S') then
                Atualizar_Propaci_Medico_Laudo(nr_seq_laudo_w,'EXC','Integra��o');
        end if;

        update  procedimento_paciente
        set     nr_laudo        = nr_seq_laudo_w
        where   nr_sequencia    = nr_seq_propaci_w;



        /*if      (nr_seq_laudo_atual_w > 0) then


                GERAR_COPIA_LAUDO_PADRAO(nr_seq_laudo_atual_w, nr_seq_laudo_w, null, 'Integra��o', 'Excluido na integra��o');


                update  laudo_paciente_copia
                set     nr_seq_laudo = nr_seq_laudo_w
                where   nr_seq_laudo = nr_seq_laudo_atual_w;

                update  LAUDO_PACIENTE_MEDICO
                set     nr_seq_laudo = nr_seq_laudo_w
                where   nr_seq_laudo = nr_seq_laudo_atual_w;

                delete  laudo_paciente_pdf
                where   nr_seq_laudo = nr_seq_laudo_atual_w;

                delete  laudo_paciente
                where   nr_sequencia = nr_seq_laudo_atual_w;

        end if;*/


        /* *****  Atualiza status execu��o na prescri��o ***** */
        update  prescr_procedimento a
        set     a.ie_status_execucao    = '40',
                a.nm_usuario    = 'Integra��o'
        where   a.nr_prescricao = nr_prescricao_w
        and     a.nr_sequencia  in (    select  b.nr_sequencia_prescricao
                                        from    procedimento_paciente b
                                        where   b.nr_prescricao         = a.nr_prescricao
                                        and     b.nr_sequencia_prescricao = a.nr_sequencia
                                        and     b.nr_laudo              = nr_seq_prescricao_w);

        select  max(ie_status_execucao)
        into    ie_status_execucao_w
        from    prescr_procedimento a
        where   a.nr_prescricao = nr_prescricao_w
        and     a.nr_sequencia  = nr_seq_prescricao_w;

        if      (ie_status_execucao_w <> '40') then

                update  prescr_procedimento a
                set     a.ie_status_execucao = '40',
                        a.nm_usuario    = 'Integra��o'
                where   a.nr_prescricao = nr_prescricao_w
                and     a.nr_sequencia  in (    select  b.nr_sequencia_prescricao
                                                from    procedimento_paciente b
                                                where   b.nr_prescricao = a.nr_prescricao
                                                and     b.nr_prescricao = nr_prescricao_w
                                                and     b.nr_sequencia_prescricao = a.nr_sequencia
                                                and     b.nr_sequencia_prescricao = nr_seq_prescricao_w);
        end if;

        --end if;

        end;
else
        gravar_log_cdi(88877,'N�o encontrou prescr_procedimento','Tasy');
        Wheb_mensagem_pck.exibir_mensagem_abort(192838);
end if;

END Gerar_laudo_synapse_hl7;
/