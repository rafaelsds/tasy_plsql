CREATE OR REPLACE PROCEDURE log_acesso_pacs(
    nm_usuario_p IN log_acesso_pacs.nm_usuario%TYPE,
    nr_acesso_dicom_p IN log_acesso_pacs.nr_acesso_dicom%TYPE
) is

  nr_seq_proc_interno_w log_acesso_pacs.NR_SEQ_PROC_INTERNO%type;
  nr_sequencia_w log_acesso_pacs.nr_sequencia%TYPE;

begin
    SELECT log_acesso_pacs_seq.NEXTVAL
        INTO nr_sequencia_w
    FROM dual;

    SELECT b.nr_seq_proc_interno
        INTO nr_seq_proc_interno_w
    FROM 
        laudo_paciente_v a, procedimento_paciente b
    WHERE 
        a.nr_sequencia = b.nr_laudo
        AND a.nr_acesso_dicom = nr_acesso_dicom_p;

    INSERT INTO log_acesso_pacs(
        nr_sequencia,
        dt_acesso,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_proc_interno,
        nr_acesso_dicom,
        nm_usuario_acesso
    ) VALUES (
        nr_sequencia_w,
        sysdate,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_seq_proc_interno_w,
        nr_acesso_dicom_p,
        nm_usuario_p
    );
    
    COMMIT;
end log_access_pacs;
/
