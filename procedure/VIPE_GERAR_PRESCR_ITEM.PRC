create or replace
procedure vipe_gerar_prescr_item (	cd_estabelecimento_p	number,
				nr_prescricao_p		number,
				nr_atendimento_p		number,
				cd_material_novo_p	number,
				cd_material_p		number,
				nr_seq_item_p		number,
				cd_intervalo_p		varchar2,
				cd_intervalo_novo_p	varchar2,
				ie_acm_sn_p		varchar2,
				qt_dose_p		number,
				ie_via_aplicacao_p		varchar2,
				nr_prescricoes_p		varchar2,
				dt_primeiro_horario_p	date,
				cd_unidade_med_p		varchar2,
				ds_horarios_p		varchar2,
				ie_susp_anterior_p		varchar2,
				ie_tipo_pessoa_p		varchar2,
				cd_perfil_p		number,
				nm_usuario_p		varchar2,
				ds_dose_dif_p		varchar2,
				qt_dose_esp_p		number,
				ds_justificativa_p		varchar2,
				ds_observacao_subs_p	varchar2,
				hr_dose_especial_p		varchar2,
				ie_dose_especial_p		varchar2,
				ie_se_necessario_p		varchar2,
				ie_acm_p			varchar2,
				ie_urgencia_p		varchar2,
				nr_nova_prescricao_p	out number,
				nr_seq_novo_item_p	out number,
				ds_reconst_diluicao_p	out varchar2) is

ds_reconst_diluicao_w	varchar2(255)	:= '';	
nr_seq_novo_item_w	number(6,0);
nr_nova_prescricao_w	number(16,0);			

begin

vipe_gerar_prescricao_item(cd_estabelecimento_p, nr_prescricao_p, nr_atendimento_p, cd_material_novo_p, cd_material_p, nr_seq_item_p, cd_intervalo_p, cd_intervalo_novo_p, ie_acm_sn_p, qt_dose_p, ie_via_aplicacao_p, nr_prescricoes_p, dt_primeiro_horario_p, cd_unidade_med_p, ds_horarios_p, ie_susp_anterior_p, ie_tipo_pessoa_p, cd_perfil_p, nm_usuario_p, ds_dose_dif_p, qt_dose_esp_p, ds_justificativa_p, ds_observacao_subs_p, hr_dose_especial_p, ie_dose_especial_p, ie_se_necessario_p, ie_acm_p, ie_urgencia_p, 'N', nr_nova_prescricao_w, nr_seq_novo_item_w,null);

if	(nr_seq_novo_item_w is not null) and
	(nr_nova_prescricao_w is not null) then
	ds_reconst_diluicao_w	:= substr(obter_diluicao_medic(nr_seq_novo_item_w, nr_nova_prescricao_w),1,255);
end if;

ds_reconst_diluicao_p	:= ds_reconst_diluicao_w;
nr_seq_novo_item_p	:= nr_seq_novo_item_w;
nr_nova_prescricao_p	:= nr_nova_prescricao_w;

end vipe_gerar_prescr_item;
/