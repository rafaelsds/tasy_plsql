CREATE OR REPLACE 
PROCEDURE GERAR_CHEQUE_BRADESCO_W 
		(nr_interno_conta_p	number) IS

nr_count_w		number := -1;
nr_cheque_w		number := 0;
nr_linha_w		number := 0;
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
qt_procedimento_w	number(9,3);
nr_interno_conta_w	number(10);
nr_interno_conta_ww	number(10);
nr_atendimento_w	number(10);
nr_doc_convenio_w	varchar2(20);
nr_doc_convenio_ww	varchar2(20);
cd_usuario_convenio_w	varchar2(30);
nm_paciente_w		varchar2(60);
dt_entrada_w		date;
nr_identidade_w		varchar2(15);
vl_matmedtx_w		number(15,2);
vl_total_csmh_w		number(15,2);
cd_medico_prescr_w	varchar2(10);
cd_senha_atend_w	varchar2(20);
nr_prescricao_w		number(14,0);
nr_crm_w		varchar2(20);

Cursor C01 is
Select	a.cd_procedimento,
	a.ie_origem_proced,
	a.qt_procedimento,
	b.nr_interno_conta,
	obter_codigo_usuario_conv(c.nr_atendimento) cd_usuario_convenio,
	obter_nome_pf(c.cd_pessoa_fisica) nm_paciente,
	c.nr_atendimento,
	a.nr_doc_convenio,
	c.dt_entrada,
	obter_dados_pf(c.cd_pessoa_fisica,'RG') nr_identidade,
	(obter_valor_conta(b.nr_interno_conta,0) - obter_valor_conta(b.nr_interno_conta,1)) vl_matmedtx,
	obter_valor_conta(b.nr_interno_conta,0) vl_total_csmh,
	a.nr_prescricao
from	atendimento_paciente c,
	conta_paciente b,
	procedimento_paciente a
where	a.nr_interno_conta = b.nr_interno_conta
and	b.nr_atendimento = c.nr_atendimento
and	a.nr_doc_convenio is not null
and	a.nr_interno_conta = nr_interno_conta_p
order by	b.nr_interno_conta,
		a.nr_doc_convenio,
		a.cd_procedimento;

BEGIN

delete from w_cheque_bradesco;
commit;

open C01;
loop
	fetch C01 into
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		nr_interno_conta_w,
		cd_usuario_convenio_w,
		nm_paciente_w,
		nr_atendimento_w,
		nr_doc_convenio_w,
		dt_entrada_w,
		nr_identidade_w,
		vl_matmedtx_w,
		vl_total_csmh_w,
		nr_prescricao_w;
	exit when C01%notfound;
	
	select	max(b.nr_crm)
	into	nr_crm_w
	from	medico b,
		prescr_medica a
	where	a.cd_medico	= b.cd_pessoa_fisica
	and	a.nr_prescricao = nr_prescricao_w;

	select	max(cd_senha)
	into	cd_senha_atend_w
	from	atend_categoria_convenio
	where	nr_atendimento	= nr_atendimento_w;
	

	nr_count_w	:= nr_count_w + 1;
	nr_linha_w	:= nr_linha_w + 1;

	if	(nr_doc_convenio_w <> nr_doc_convenio_ww) then
		nr_cheque_w	:= 0;
		nr_count_w	:= 0;
		/*nr_linha_w	:= 1;*/
	end if;

	if	(nr_interno_conta_w <> nr_interno_conta_ww) then
		nr_cheque_w	:= 0;
		nr_count_w	:= 0;
		nr_linha_w	:= 1;
	end if;

	if	(nr_count_w = 8) then
		nr_count_w	:= 0;
		nr_cheque_w	:= nr_cheque_w + 1;
	end if;
		
	insert into w_cheque_bradesco		(	nr_interno_conta,
							nr_cheque,
							cd_procedimento,
							ie_origem_proced,
							qt_procedimento,
							nr_linha,
							cd_usuario_convenio,
							nm_paciente,
							nr_atendimento,
							nr_doc_convenio,
							dt_entrada,
							nr_identidade,
							vl_matmedtx,
							vl_total_csmh,
							nr_crm_prescr,
							cd_senha
						)
				values		(	nr_interno_conta_w,
							nr_cheque_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							qt_procedimento_w,
							nr_linha_w,
							cd_usuario_convenio_w,
							nm_paciente_w,
							nr_atendimento_w,
							nr_doc_convenio_w,
							dt_entrada_w,
							nr_identidade_w,
							vl_matmedtx_w,
							vl_total_csmh_w,
							nr_crm_w,
							cd_senha_atend_w
						);

	nr_doc_convenio_ww	:= nr_doc_convenio_w;
	nr_interno_conta_ww	:= nr_interno_conta_w;

end loop;
close C01;

commit;

END GERAR_CHEQUE_BRADESCO_W;
/