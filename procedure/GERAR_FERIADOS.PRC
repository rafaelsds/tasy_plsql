create or replace
procedure gerar_feriados (	cd_estabelecimento_p	number,
			  dt_feriado_p	   	date,
			  dt_inicial_p		date,
			  dt_final_p		date,
			  nm_usuario_p		varchar) is 

ie_feriado_w		number(1);
dt_ano_seguinte_w		date;
dt_inicial_w		date;
ds_motivo_feriado_w	varchar2(40);
ie_tipo_feriado_w		varchar2(15);

Begin

select	max(ds_motivo_feriado),
	max(ie_tipo_feriado)
into	ds_motivo_feriado_w,
	ie_tipo_feriado_w
from	feriado
where	cd_estabelecimento = cd_estabelecimento_p
and	dt_feriado = pkg_date_utils.start_of(dt_feriado_p, 'DD', 0);

dt_inicial_w := PKG_DATE_UTILS.start_of(dt_inicial_p,'year',0);

while (dt_inicial_w <= dt_final_p) loop

	select	count(*)
	into	ie_feriado_w
	from	feriado
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_feriado = pkg_date_utils.get_date(pkg_date_utils.extract_field('YEAR',dt_inicial_w,0),pkg_date_utils.extract_field('MONTH', dt_feriado_p,0),pkg_date_utils.extract_field('DAY', dt_feriado_p,0),0);

	if (ie_feriado_w = 0) then
		insert into feriado (cd_estabelecimento,
				     dt_feriado,
				     ds_motivo_feriado,
				     dt_atualizacao,
				     nm_usuario,
				     dt_atualizacao_nrec,
				     nm_usuario_nrec,
				     ie_tipo_feriado)
			     values (cd_estabelecimento_p,
				     pkg_date_utils.get_date(pkg_date_utils.extract_field('YEAR',dt_inicial_w,0),pkg_date_utils.extract_field('MONTH', dt_feriado_p,0),pkg_date_utils.extract_field('DAY', dt_feriado_p,0),0),
				     ds_motivo_feriado_w,
				     sysdate,
				     nm_usuario_p,
				     sysdate,
				     nm_usuario_p,
				     ie_tipo_feriado_w);								
	end if;

	dt_inicial_w := pkg_date_utils.add_month(dt_inicial_w, 12, 0);

end loop;

commit;

END GERAR_FERIADOS;
/