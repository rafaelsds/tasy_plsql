create or replace
procedure pls_gerar_cartao_ativia
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
			
nr_seq_segurado_w		number(10);
ds_carencia_w			varchar2(40);
dt_fim_carencia_w		varchar2(20);
ds_tot_carencia_w		varchar2(4000);
ds_mens_dt_carencia_w		varchar2(10);
ds_mensagem_w			varchar2(4000);
cd_matricula_familia_w		varchar2(20);
cd_usuario_plano_w		varchar2(50);
nm_beneficiario_w		varchar2(255);
dt_contratacao_w		varchar2(20);
dt_nascimento_w			varchar2(20);
ds_segmentacao_w		varchar2(255);
ds_plano_w			varchar2(255);
ds_trilha_1_w			varchar2(255);
nr_via_solicitacao_w		varchar2(255);
nr_seq_plano_w			number(10);
ds_mensagem_aux_w		varchar2(4000);
qt_linha_mensagem_w		number(10);
ie_abrangencia_w		varchar2(10);
ie_tipo_contratacao_w		varchar2(10);
ds_trilha2_w			varchar2(255);
ds_carencia_aux_w		varchar2(255);
ie_preenche_vazio_w		varchar2(255);
qt_espaco_mens_w		number(10);
			
Cursor C01 is
	select	b.nr_sequencia
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_p;
	
Cursor C02 is
	select	substr(ds_carencia,1,27),
		dt_carencia,
		ie_preenche_vazio
	from	(	select	b.ds_carencia,		
				decode(a.qt_dias,'0','CUMPRIDA',to_char(nvl(a.dt_fim_vigencia,nvl(a.dt_inicio_vigencia,dt_contratacao_w)+a.qt_dias),'dd/mm/yyyy')) dt_carencia,
				nvl(b.nr_apresentacao,10) nr_apresentacao,
				'N' ie_preenche_vazio
			from	pls_carencia		a,
				pls_tipo_carencia	b,
				pls_segurado		c
			where	a.nr_seq_tipo_carencia	= b.nr_sequencia
			and	a.nr_seq_segurado	= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_segurado_w
			and	nvl(b.ie_padrao_carteira,'N')	= 'S'
			and	b.ie_cpt	= 'N'
			union all
			select	b.ds_carencia,		
				decode(a.qt_dias,'0','CUMPRIDA',to_char(nvl(a.dt_fim_vigencia,nvl(a.dt_inicio_vigencia,dt_contratacao_w)+a.qt_dias),'dd/mm/yyyy')),
				nvl(b.nr_apresentacao,10),
				'N' ie_preenche_vazio
			from	pls_carencia		a,
				pls_tipo_carencia	b,
				pls_segurado		c,
				pls_contrato		d
			where	a.nr_seq_tipo_carencia	= b.nr_sequencia
			and	c.nr_seq_contrato	= d.nr_sequencia
			and	a.nr_seq_contrato	= d.nr_sequencia
			and	c.nr_sequencia		= nr_seq_segurado_w
			and	nvl(b.ie_padrao_carteira,'N')	= 'S'
			and	b.ie_cpt	= 'N'
			and	(	select	count(*)
					from	pls_carencia		a,
						pls_tipo_carencia	b,
						pls_segurado		c
					where	a.nr_seq_tipo_carencia	= b.nr_sequencia
					and	a.nr_seq_segurado	= c.nr_sequencia
					and	c.nr_sequencia		= nr_seq_segurado_w
					and	b.ie_cpt		='N') = 0
			union all
			select	b.ds_carencia,		
				decode(a.qt_dias,'0','CUMPRIDA',to_char(nvl(a.dt_fim_vigencia,nvl(a.dt_inicio_vigencia,dt_contratacao_w)+a.qt_dias),'dd/mm/yyyy')),
				nvl(b.nr_apresentacao,10),
				'N' ie_preenche_vazio
			from	pls_carencia		a,
				pls_tipo_carencia	b,
				pls_segurado		c,
				pls_plano		d
			where	a.nr_seq_tipo_carencia	= b.nr_sequencia
			and	c.nr_seq_plano		= d.nr_sequencia
			and	a.nr_seq_plano		= d.nr_sequencia
			and	c.nr_sequencia		= nr_seq_segurado_w
			and	nvl(b.ie_padrao_carteira,'N')	= 'S'
			and	b.ie_cpt	= 'N'
			and	(	select	count(*)
					from	pls_carencia		a,
						pls_tipo_carencia	b,
						pls_segurado		c
					where	a.nr_seq_tipo_carencia	= b.nr_sequencia
					and	a.nr_seq_segurado	= c.nr_sequencia
					and	c.nr_sequencia		= nr_seq_segurado_w
					and	b.ie_cpt		= 'N') = 0
			and	(	select	count(*)
					from	pls_carencia		a,
						pls_tipo_carencia	b,
						pls_segurado		c,
						pls_contrato		d
					where	a.nr_seq_tipo_carencia	= b.nr_sequencia
					and	c.nr_seq_contrato	= d.nr_sequencia
					and	a.nr_seq_contrato	= d.nr_sequencia
					and	c.nr_sequencia		= nr_seq_segurado_w
					and	b.ie_cpt		= 'N') = 0
			union all
			select	' ',
				' ',
				99999,
				'S' ie_preenche_vazio
			from	tabela_sistema)
	where	rownum	<= 10
	orDER	BY	nr_apresentacao;
				
Cursor C03 is
	select	ds_mensagem,
		rownum
	from		(	select	ds_mensagem,
					NR_SEQ_ORDEM
				from	pls_plano_mensagem_cart
				where	nr_seq_plano	= nr_seq_plano_w
				and	sysdate between dt_inicio_vigencia and nvl(dt_fim_vigencia,sysdate)
				and	ie_situacao	 = 'A'
				union all
				select	null ds_mensagem,
					9999 NR_SEQ_ORDEM
				from	tabela_sistema
				order by NR_SEQ_ORDEM)
	where	rownum <= 3;

begin

delete	w_pls_interface_carteira
where	nr_seq_lote	= nr_seq_lote_p;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	select	b.cd_matricula_familia,
		c.cd_usuario_plano,
		substr(a.nm_pessoa_fisica,1,50),
		to_char(b.dt_contratacao,'dd/mm/yyyy'),
		to_char(a.dt_nascimento,'dd/mm/yyyy'),
		substr(obter_valor_dominio(1665,d.ie_segmentacao),1,50),
		substr(d.DS_CARTEIRA_ACOMODACAO,1,50),
		substr(ds_trilha1,1,50),
		nr_via_solicitacao,
		d.nr_sequencia,
		lpad(decode(d.ie_abrangencia,'N','1','GE','2','E','3','GM','4','M','5'),1,' '),
		lpad(decode(d.ie_tipo_contratacao,'CA','4','CE','3','I','2'),1,' ')
	into	cd_matricula_familia_w,
		cd_usuario_plano_w,
		nm_beneficiario_w,
		dt_contratacao_w,
		dt_nascimento_w,
		ds_segmentacao_w,
		ds_plano_w,
		ds_trilha_1_w,
		nr_via_solicitacao_w,
		nr_seq_plano_w,
		ie_abrangencia_w,
		ie_tipo_contratacao_w
	from	pls_plano		d,
		pls_segurado_carteira	c,
		pls_segurado		b,
		pessoa_fisica		a
	where	c.nr_seq_segurado	= b.nr_sequencia
	and	b.nr_seq_plano		= d.nr_sequencia
	and	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	b.nr_sequencia		= nr_seq_segurado_w;
	
	ds_tot_carencia_w	:= '';
	ds_mensagem_w		:= '';
	
	nr_via_solicitacao_w	:= lpad(nr_via_solicitacao_w,2,'0');
	
	open c02;
	loop
	fetch c02 into	
		ds_carencia_w,
		dt_fim_carencia_w,
		ie_preenche_vazio_w;
	exit when c02%notfound;
		begin
		
		if	(ie_preenche_vazio_w = 'N') then
			if	(dt_fim_carencia_w <> 'CUMPRIDA') AND
				(to_date(dt_fim_carencia_w,'dd/mm/yyyy') < sysdate) then
				dt_fim_carencia_w	:= 'CUMPRIDA';
			end if;
			
			ds_carencia_aux_w	:= rpad(ds_carencia_w,28,' ');
			dt_fim_carencia_w	:= rpad(dt_fim_carencia_w,10,' ');
		else
			ds_carencia_aux_w	:= rpad(' ',28,' ');
			dt_fim_carencia_w	:= rpad(' ',10,' ');
		end if;
		
		ds_tot_carencia_w	:= ds_tot_carencia_w||ds_carencia_aux_w||' '||dt_fim_carencia_w||',';
		end;
	end loop;
	close c02;
	
	open c03;
	loop
	fetch c03 into	
		ds_mensagem_aux_w,
		qt_linha_mensagem_w;
	exit when c03%notfound;
		begin
		
		if	(qt_linha_mensagem_w = 1) then			
			if	(ds_mensagem_aux_w is not null) then
				ds_mensagem_w		:= ds_mensagem_w || lpad(' ',200,' ');
				qt_espaco_mens_w	:= 40 - length(ds_mensagem_aux_w);
				ds_mensagem_w		:= ds_mensagem_w||ds_mensagem_aux_w||rpad(' ',qt_espaco_mens_w,' ')||',';
			else
				ds_mensagem_w		:= ds_mensagem_w||lpad(' ',240,' ')||',';
			end if;
			
		elsif	(qt_linha_mensagem_w = 2) then
			ds_mensagem_w	:= ds_mensagem_w || rpad(nvl(ds_mensagem_aux_w,'  '),40,' ') ||',';
		elsif	(qt_linha_mensagem_w = 3) then
			ds_mensagem_w	:= ds_mensagem_w || rpad(nvl(ds_mensagem_aux_w,'  '),62,' ') ||',';
		end if;

		
		end;
	end loop;
	close c03;
	
	cd_matricula_familia_w	:= ' '||rpad(cd_matricula_familia_w,7, ' ')||',';
	
	ds_trilha2_w		:= cd_usuario_plano_w || '=' || nr_via_solicitacao_w || '7012' ||
				   '=' || '0001000' ||  ie_abrangencia_w || ie_tipo_contratacao_w || '000';
				   
	cd_usuario_plano_w	:= substr(cd_usuario_plano_w,1,3)||'.' ||substr(cd_usuario_plano_w,4,4)||'.' ||substr(cd_usuario_plano_w,8,6)||'.'||
					substr(cd_usuario_plano_w,14,2)||'-'||substr(cd_usuario_plano_w,16,1)|| ' ' ||nr_via_solicitacao_w;
				
	cd_usuario_plano_w	:= '  '||rpad(cd_usuario_plano_w,24, ' ')|| ',';
	
	nm_beneficiario_w	:= ' '||rpad(nm_beneficiario_w,50, ' ')|| ',';	
	
	dt_contratacao_w	:= ' '||dt_contratacao_w|| ',';
	
	dt_nascimento_w		:= ' '||dt_nascimento_w || ',';
	
	ds_segmentacao_w	:= ' '||rpad(upper(ds_segmentacao_w),50, ' ') || ',';
	
	ds_plano_w		:= ' '||rpad(upper(nvl(ds_plano_w,' ')),50, ' ') || ',';
	
	ds_trilha_1_w		:= replace(replace(ds_trilha_1_w,'%',''),'?','');
	
	ds_trilha_1_w		:= 'b'||ds_trilha_1_w;
	
	ds_trilha_1_w		:= rpad(ds_trilha_1_w,50, ' ')|| ',';
	
	insert into w_pls_interface_carteira
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			ds_carencia,cd_matricula_familiar,cd_usuario_plano,nm_beneficiario,ds_observacao,
			dt_adesao,dt_nascimento,ds_segmentacao,nm_plano,ds_trilha_1,
			ds_trilha_2,nr_seq_lote,nr_seq_segurado)
	values	(	w_pls_interface_carteira_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			ds_tot_carencia_w,cd_matricula_familia_w,cd_usuario_plano_w,nm_beneficiario_w,ds_mensagem_w,
			dt_contratacao_w,dt_nascimento_w,ds_segmentacao_w,ds_plano_w,ds_trilha_1_w,
			ds_trilha2_w,nr_seq_lote_p,nr_seq_segurado_w);
	
	end;
end loop;
close C01;

commit;

end pls_gerar_cartao_ativia;
/
