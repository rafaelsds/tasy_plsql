create or replace procedure get_dbs_claim(
				nr_seq_account_p   		number,
				cd_estabelecimento_p		number,
				nm_usuario_p       		varchar2,
				ie_resubmission_p		varchar default 'N',
				ie_validate_p      		varchar2,
				ie_service_type_p 		varchar2,
        cd_transaction_p    out varchar2)
is

nr_encounter_w			atendimento_paciente.nr_atendimento%type;
dt_return_w			autorizacao_convenio.dt_retorno%type;
nr_ihi_w			person_ihi.nr_ihi%type;
cd_medic_encounter_w		atendimento_paciente.cd_medico_atendimento%type;
cd_medic_resp_w			atendimento_paciente.cd_medico_resp%type;
ie_service_type_w		dbs_claim.ie_service_type%type;
cd_provider_w			dbs_claim.cd_provider%type;
cd_payee_provider_w		dbs_claim.cd_payee_provider%type;
nr_seq_transaction_w		dbs_claim.nr_seq_transaction%type;
cd_claim_w   			dbs_claim.cd_claim%type;
inconsistency_count_w 		number(10);
request_sts              	varchar2(500);
nr_sequencia_w			number(10);
general_procedures_w   		varchar2(400);
specialist_procedures_w  	varchar2(400);
pathology_procedures_w 		varchar2(400);
cd_attribute_w             	varchar2(40);
cd_field_name_w            	varchar2(40);
nr_voucher_count_w  		number;

cursor c03 is
	select 	nm_eclipse_field,
		nm_atributo
	from   	eclipse_attribute
	where  	ie_condition = 'M'
	and	ie_dbs = 'S';

begin
billing_i18n_pck.set_validate_eclipse(ie_validate_p);


-- Clear the prior validation checks
delete	eclipse_inco_account a
where	a.nr_interno_conta = nr_seq_account_p;
commit;

select  nvl(max(nr_sequencia),0)
into    nr_sequencia_w
from    dbs_claim
where   nr_seq_account	=  nr_seq_account_p;

select	generateclaimnumeclipse cd_claim
into 	cd_claim_w
from 	dual ;

select  a.nr_atendimento,
	a.cd_medico_resp
into    nr_encounter_w,
	cd_medic_resp_w

from    atendimento_paciente a,
        conta_paciente b
where   a.nr_atendimento = b.nr_atendimento
and     b.nr_interno_conta = nr_seq_account_p;

select	decode(a.ie_service_type , 6, 'P' , 1, 'O' , 2, 'S' , 'O')
into	ie_service_type_w
from	eclipse_parameters a;

select	max(Lpad(substr(k.nr_provider, 1,8),8,'0'))
into	cd_provider_w
from   	conta_paciente b,
        atendimento_paciente a ,
        medical_provider_number k
where  	b.nr_interno_conta = nr_seq_account_p
and 	b.nr_atendimento = a.nr_atendimento
and     a.cd_medico_resp = k.cd_medico
and 	rownum =1;

select	max(Lpad(substr(k.NR_PROVIDER, 1,8),8,'0'))
into	cd_payee_provider_w
from   	conta_paciente b,
        medical_provider_number k
where  	b.nr_interno_conta = nr_seq_account_p
and     b.CD_RESPONSAVEL = k.cd_medico
and 	rownum =1;

select	max(a.dt_retorno)
into	dt_return_w
from	autorizacao_convenio a
where 	a.nr_atendimento = nr_encounter_w
and 	a.cd_convenio = Obter_Convenio_Atendimento(nr_encounter_w);

get_eclipse_service_types(nr_seq_account_p,general_procedures_w,specialist_procedures_w,pathology_procedures_w );
if(specialist_procedures_w is not null and pathology_procedures_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1118140, ' Specialist items = ' || specialist_procedures_w || ' Pathology items = ' || pathology_procedures_w);
end if;
if	(specialist_procedures_w is not null) then
	ie_service_type_w := 'S';
elsif	(pathology_procedures_w is not null) then
	ie_service_type_w := 'P';
else
	ie_service_type_w := 'O';
end if;

-- logic to validate fields
for r03 in c03 loop
	begin

        cd_attribute_w := r03.nm_atributo;
        cd_field_name_w := r03.nm_eclipse_field;

        if	(upper(cd_field_name_w) = upper('AuthorisationDate')) then
		if (dt_return_w > sysdate or dt_return_w is null ) then
			generate_inco_eclipse(nr_seq_account_p, 3, Wheb_mensagem_pck.get_texto(1104641), nm_usuario_p);
		end if;

        elsif	(upper(cd_field_name_w) = upper('PmsClaimId') ) then
		if(cd_claim_w is null ) then
			generate_inco_eclipse(nr_seq_account_p, 3, 'invalid Claim Id', nm_usuario_p);
		end if;

	elsif	(upper(cd_field_name_w) = upper('ServicingProviderNum')  ) then
		if(cd_provider_w is null) then
			generate_inco_eclipse(nr_seq_account_p, 3, Wheb_mensagem_pck.get_texto(1100198), nm_usuario_p);
		end if;

	elsif	(upper(cd_field_name_w) = upper('ServiceTypeCde')  ) then
		if(ie_service_type_w is null) then
			generate_inco_eclipse(nr_seq_account_p, 3, 'invalid service type cde', nm_usuario_p);
		end if;
        end if;

	end;
end loop;

if( cd_payee_provider_w = cd_provider_w ) then
	generate_inco_eclipse(nr_seq_account_p , 1, Wheb_mensagem_pck.get_texto(1104644), nm_usuario_p);
end if;

if	(ie_resubmission_p = 'N') then
		nr_seq_transaction_w := generateRandomNumber();
	end if;

if (nr_sequencia_w = 0 ) then
	select 	dbs_claim_seq.nextval
	into 	nr_sequencia_w
	from dual;

	insert into dbs_claim(
		nr_sequencia,
		nr_encounter,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_authorisation,
		cd_payee_provider,
		cd_claim,
		cd_provider,
		ie_service_type,
		nr_seq_account,
		cd_establishment,
		nr_seq_transaction)
	values(
		nr_sequencia_w,
		nr_encounter_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nvl(dt_return_w, sysdate),
		cd_payee_provider_w,
		cd_claim_w,
		cd_provider_w,
		ie_service_type_w,
		nr_seq_account_p,
		cd_estabelecimento_p,
		nr_seq_transaction_w);
else
	delete	dbs_item	a
	where	a.nr_seq_voucher in (select b.nr_sequencia from dbs_voucher b where b.nr_seq_claim = nr_sequencia_w);

	delete	dbs_voucher
	where	nr_seq_claim	= nr_sequencia_w;

	update	dbs_claim
	set	nr_encounter		= nr_encounter_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_authorisation	= nvl(dt_return_w, sysdate),
		cd_payee_provider	= cd_payee_provider_w,
		cd_claim		= cd_claim_w,
		cd_provider		= cd_provider_w,
		ie_service_type		= ie_service_type_w,
		cd_establishment	= cd_estabelecimento_p,
		nr_seq_transaction 	= nr_seq_transaction_w
	where	nr_sequencia		= nr_sequencia_w;
end if;


get_dbs_voucher(nr_sequencia_w, nr_seq_account_p, nm_usuario_p, ie_validate_p,ie_service_type_w,nr_seq_transaction_w,cd_claim_w);

select	count(*)
into 	nr_voucher_count_w
from  	dbs_voucher
where 	nr_seq_claim =nr_sequencia_w;

if(nr_voucher_count_w > 80) then
  wheb_mensagem_pck.exibir_mensagem_abort('Too many services are added in one claim - more than 80 vouchers are not allowed in one claim please split the claims');
end if;

select	count(*)
into 	inconsistency_count_w
from  	eclipse_inco_account a
where 	a.nr_interno_conta = nr_seq_account_p;

if(inconsistency_count_w = 0 and ie_validate_p = 'N') then
cd_transaction_p := nr_seq_transaction_w;
else
	rollback;
end if;

end get_dbs_claim;
/