create or replace
procedure pls_alterar_data_ref(	nr_sequencia_p			number,
				dt_mes_competencia_p		date,
				dt_protocolo_p			date,
				nr_seq_prestador_p		number,
				ds_observacao_p			varchar2,
				nr_seq_congenere_p		number,
				dt_base_venc_p			date,
				ie_tipo_guia_p			varchar2,				
				nr_protocolo_prestador_p	varchar2,	
				nr_seq_protocolo_p		number,
				ie_guia_fisica_p		varchar2,
				nr_seq_lote_p			number,
				qt_contas_informadas_p		number,
				ds_erro_p		out	Varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

dt_mes_competencia_w		Date;
dt_recebimento_w		Date;
ds_tipo_erro_w			varchar2(1) := 'A';
nr_seq_prot_referencia_w 	pls_protocolo_conta.nr_seq_prot_referencia%type;

begin

begin -- Obtem data de recebimento do protocolo  
select	dt_recebimento,
	nr_seq_prot_referencia
into	dt_recebimento_w,
	nr_seq_prot_referencia_w
from 	pls_protocolo_conta
where	nr_Sequencia 	= nr_sequencia_p;
exception
when others then
	dt_recebimento_w	:= null;
end;

pls_alterar_dados_conta_medica(nr_sequencia_p, dt_mes_competencia_p, dt_protocolo_p, nr_seq_prestador_p, ds_observacao_p,nr_seq_lote_p,qt_contas_informadas_p,ie_guia_fisica_p, cd_estabelecimento_p, nm_usuario_p,dt_recebimento_w, null, '','N','N','N',nr_seq_prot_referencia_w);
				
pls_consistir_protocolo_conta(nr_seq_prestador_p,nr_seq_congenere_p,dt_base_venc_p,ie_tipo_guia_p,dt_mes_competencia_p,dt_protocolo_p,nr_protocolo_prestador_p,nr_seq_protocolo_p,
				null, nm_usuario_p,cd_estabelecimento_p, sysdate, null, null, dt_mes_competencia_w, ds_tipo_erro_w, ds_erro_p);		
				
commit;

end pls_alterar_data_ref;
/