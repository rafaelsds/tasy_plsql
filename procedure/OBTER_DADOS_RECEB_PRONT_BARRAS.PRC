create or replace
procedure obter_dados_receb_pront_barras(	nr_sequencia_p		Number,
					ie_cont_trans_solic_p	Varchar2,
					nm_usuario_p		Varchar2,
					cd_setor_destino_p out	Number) is 

nr_sequencia_w 	Number(10);
cd_setor_destino_w	Number(5);
			
			
begin

select 	nvl(max(a.nr_sequencia),0)
into	nr_sequencia_w 
from 	transf_prontuario a, 
	transf_prontuario_envelope b 
where 	a.nr_sequencia = b.nr_seq_transf 
and 	b.nr_seq_prontuario = nr_sequencia_p
and 	a.dt_recebimento is null;

if	(ie_cont_trans_solic_p = 'S'
	and nr_sequencia_w = 0 ) then
	--(-20011, obter_texto_tasy(67885, wheb_usuario_pck.get_nr_seq_idioma)||'#@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(67885);
end if;

select 	max(cd_setor_destino)
into	cd_setor_destino_w 
from 	transf_prontuario 
where  	nr_sequencia = nr_sequencia_w;

cd_setor_destino_p := cd_setor_destino_w;

end obter_dados_receb_pront_barras;
/