create or replace
procedure valida_procedimento_bio(nr_prescricao_p		number,	
				  nr_sequencia_p		number,
				  nm_usuario_biometria_p	varchar2,
				  nm_usuario_aux_biometria_p	varchar2,
				  nr_seq_participante_p		number,
				  cd_estabelecimento_p		number,
				  nm_usuario_p			Varchar2) is 


cd_medico_exec_w		varchar2(255);
ie_gerar_somente_medico_w	varchar2(1);
ie_gerar_somente_auxiliar_w	varchar2(1);
ie_validado_w			varchar2(1) := 'S';
nr_cirurgia_w			number(10);
ie_funcao_w			varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
ie_funcao_ww			varchar2(10);
cd_pessoa_fisica_ww		varchar2(10);


cursor	c01 is				  
	select 	cd_pessoa_fisica,
		ie_funcao
	from	cirurgia_participante
	where	nr_seq_interno = nr_seq_participante_p;

begin
Obter_Param_Usuario(900,455,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_somente_medico_w);
Obter_Param_Usuario(900,524,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_somente_auxiliar_w);

if 	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) and
	((nm_usuario_biometria_p is not null) or 
	 (nm_usuario_aux_biometria_p is not null)) then
	if	(nm_usuario_biometria_p is not null) then	
		select 	max(cd_medico_cirurgiao)
		into	cd_medico_exec_w
		from	cirurgia
		where	nr_prescricao = nr_prescricao_p;
		
		if	(nvl(ie_gerar_somente_medico_w,'N') = 'M') and
			(nvl(cd_medico_exec_w,0) <> obter_pessoa_fisica_usuario(nm_usuario_biometria_p,'C')) then 
			wheb_mensagem_pck.exibir_mensagem_abort(204265);
		else
			update	prescr_procedimento
			set	dt_biometria = sysdate,
				nm_usuario_biometria = nm_usuario_biometria_p,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where	nr_sequencia = nr_sequencia_p
			and 	nr_prescricao = nr_prescricao_p;
		end if;
	else	
		if	(ie_gerar_somente_auxiliar_w = 'P') then
			ie_validado_w := 'N';
		
			select	max(nr_cirurgia)
			into	nr_cirurgia_w
			from	cirurgia
			where	nr_prescricao = nr_prescricao_p;
			
			open C01;
			loop
			fetch C01 into	
				cd_pessoa_fisica_w,
				ie_funcao_w;
			exit when C01%notfound;
				begin
				if	(cd_pessoa_fisica_w = obter_pessoa_fisica_usuario(nm_usuario_aux_biometria_p,'C')) then
					ie_validado_w 		:= 'S';
					cd_pessoa_fisica_ww	:= cd_pessoa_fisica_w;
					ie_funcao_ww		:= ie_funcao_w;
				end if;	
				end;
			end loop;
			close C01;
		else
			select 	max(cd_pessoa_fisica),
				max(ie_funcao)
			into	cd_pessoa_fisica_ww,
				ie_funcao_ww
			from	cirurgia_participante
			where	nr_seq_interno = nr_seq_participante_p;
		end if;
		
		if	(ie_validado_w = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(239257);
		else
			/*
			update	prescr_procedimento
			set	dt_biometria_aux	 = sysdate,
				nm_usuario_aux_biometria = nm_usuario_aux_biometria_p,
				nm_usuario 		 = nm_usuario_p,
				dt_atualizacao 		 = sysdate
			where	nr_sequencia 		 = nr_sequencia_p
			and 	nr_prescricao 		 = nr_prescricao_p;
			*/
			insert	into prescr_proced_biometria	(	nr_sequencia, 
									nr_prescricao, 
									ie_funcao, 
									cd_pessoa_fisica, 
									dt_atualizacao, 
									nm_usuario, 
									dt_atualizacao_nrec, 
									nm_usuario_nrec, 
									nr_seq_proc)
			values					(	prescr_proced_biometria_seq.nextval,
									nr_prescricao_p,
									ie_funcao_ww,
									cd_pessoa_fisica_ww,
									sysdate,
									nm_usuario_aux_biometria_p,
									sysdate,
									nm_usuario_p,
									nr_sequencia_p);
			commit;						
		end if;
	end if;	
end if;

commit;

end valida_procedimento_bio;
/