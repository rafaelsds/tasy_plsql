create or replace
procedure definir_itens_protocolo_solic(		nr_sequencia_p			number,
					cd_estabelecimento_p		number,
					ie_tipo_material_p			varchar2,
					cd_grupo_material_p		number,
					cd_subgrupo_material_p		number,
					cd_classe_material_p		number,
					nr_seq_familia_p			number,
					ie_padronizado_p			varchar2,
					ie_mat_estoque_p			varchar2,
					nm_usuario_p			varchar2) is


cd_material_w			material.cd_material%type;				
				

cursor c01 is
select	b.cd_material
from	estrutura_material_v a,
	material b
where	a.cd_material		= b.cd_material
and	b.ie_situacao = 'A'
and	a.cd_classe_material	= nvl(cd_classe_material_p, a.cd_classe_material)
and	a.cd_grupo_material	= nvl(cd_grupo_material_p, a.cd_grupo_material)
and	a.cd_subgrupo_material	= nvl(cd_subgrupo_material_p, a.cd_subgrupo_material)
and	b.ie_tipo_material	= nvl(ie_tipo_material_p, b.ie_tipo_material)
and	((nvl(nr_seq_familia_p, 0) = 0) or (b.nr_seq_familia = nr_seq_familia_p))
and	((ie_mat_estoque_p = '2') or
	((ie_mat_estoque_p = '0') and (obter_se_material_estoque(cd_estabelecimento_p, 0, b.cd_material) = 'S')) or
	((ie_mat_estoque_p = '1') and (obter_se_material_estoque(cd_estabelecimento_p, 0, b.cd_material) = 'N')))
and	((ie_padronizado_p = '2') or
	((ie_padronizado_p = '0') and (obter_se_material_padronizado(cd_estabelecimento_p, b.cd_material) = 'S')) or
	((ie_padronizado_p = '1') and (obter_se_material_padronizado(cd_estabelecimento_p, b.cd_material) = 'N')))
and not exists(
	select	1
	from	prot_solic_compra_item x
	where	x.nr_seq_protocolo = nr_sequencia_p
	and	x.cd_material = b.cd_material);
	

begin

open c01;
loop
fetch c01 into
	cd_material_w;
exit when c01%notfound;
	begin
	insert into prot_solic_compra_item(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_protocolo,
		cd_material,
		qt_material)
	values(	prot_solic_compra_item_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p,
		cd_material_w,
		0);
	
	end;
end loop;
close C01;

commit;

end DEFINIR_ITENS_PROTOCOLO_SOLIC;


/
