create or replace
procedure pls_inserir_preexist_doenca
			(	nr_seq_preexistencia_p	Number,
				cd_doenca_cid_p		Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

nr_seq_preexist_cid_w	Number(10);

begin

select	pls_preexist_cid_seq.nextval
into	nr_seq_preexist_cid_w
from	dual;

insert	into	pls_preexist_cid
	(	nr_sequencia, nr_seq_preexistencia, cd_estabelecimento,
		cd_doenca_cid, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec)
	values
	(	nr_seq_preexist_cid_w, nr_seq_preexistencia_p, cd_estabelecimento_p,
		cd_doenca_cid_p, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p);
commit;

end pls_inserir_preexist_doenca;
/