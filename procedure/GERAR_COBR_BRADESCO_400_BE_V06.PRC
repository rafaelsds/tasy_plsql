create or replace
procedure gerar_cobr_bradesco_400_be_v06
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
/*	Vers�o 06
	Data: 05/06/2009
*/

/* HEader */
ds_conteudo_w			varchar2(400);
nm_empresa_w			varchar2(30);
cd_empresa_w			varchar2(20);
nr_seq_arquivo_w		varchar2(7);
dt_geracao_w			varchar2(6);
nr_seq_registro_w		number(10);
nr_seq_registro2_w		number(10) := 0;

/* Transa��o */
nm_avalista_w			varchar2(60);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
nr_controle_partic_w		varchar2(25);
id_empresa_w			varchar2(17);
nr_inscricao_w			varchar2(14);
vl_titulo_w			varchar2(13);
vl_acrescimo_w			varchar2(13);
vl_desconto_w			varchar2(13);
vl_iof_w			varchar2(13);
vl_abatimento_w			varchar2(13);
nr_nosso_numero_w		varchar2(11);
vl_desconto_dia_w		varchar2(10);
nr_documento_w			varchar2(10);
cd_cep_w			varchar2(8);
cd_conta_w			varchar2(7);
dt_vencimento_w			varchar2(6);
dt_emissao_w			varchar2(6);
dt_desconto_w			varchar2(6);
cd_agencia_debito_w		varchar2(5);
cd_agencia_bancaria_w		varchar2(5);
cd_agencia_deposito_w		varchar2(5);
pr_multa_w			varchar2(4);
cd_banco_w			varchar2(3);
cd_banco_cobranca_w		varchar2(3);
ie_ocorrencia_w			varchar2(2);
ie_especie_w			varchar2(2);
ie_instrucao_1_w		varchar2(2);
ie_instrucao_2_w		varchar2(2);
ie_tipo_inscricao_w		varchar2(2);
ie_digito_agencia_w		varchar2(1);
ie_digito_conta_w		varchar2(1);
ie_multa_w			varchar2(1);
nr_dig_nosso_numero_w		varchar2(1);
cd_condicao_w			varchar2(1);
ie_emite_papeleta_w		varchar2(1);
ie_rateio_w			varchar2(1);
ie_endereco_w			varchar2(1);
ie_aceite_w			varchar2(1);

/* Trailler */
ds_brancos_393_w		varchar2(393);
ds_brancos_277_w		varchar2(277);
ds_brancos_20_w			varchar2(20);
ds_brancos_12_w			varchar2(12);
ds_brancos_10_w			varchar2(10);
ds_brancos_8_w			varchar2(8);
ds_brancos_2_w			varchar2(2);

qt_registros_w			number(10)	:= 0;

Cursor C01 is
	select	lpad(' ',5,' ') cd_agencia_debito,
		lpad(substr(nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),' '),1,1),1,' ') ie_digito_agencia,
		lpad(' ',5,' ') cd_agencia_bancaria,
		lpad(' ',7,' ') cd_conta,
		' ' ie_digito_conta,
		'0' || lpad(substr(nvl(x.cd_carteira,'0'),1,3),3,'0') || lpad(nvl(x.cd_agencia_bancaria,'0'),5,'0') || lpad(nvl(x.cd_conta,'0'),7,'0') || nvl(x.ie_digito_conta,'0') id_empresa,
		lpad(nvl(f.cd_cgc,'0'),25,'0') nr_controle_partic,
		lpad(substr(nvl(a.cd_banco,'0'),1,3),3,'0') cd_banco,
		'0' ie_multa,
		lpad('0',4,'0') pr_multa,
		lpad(to_char(nvl(b.nr_titulo,'0')),11,'0') nr_nosso_numero,
		decode(calcula_digito('MODULO11_BRAD',lpad(substr(x.cd_carteira,2,2),2,'0') || lpad(b.nr_titulo,11,'0')),'-1','P',
			calcula_digito('MODULO11_BRAD',lpad(substr(x.cd_carteira,2,2),2,'0') || lpad(b.nr_titulo,11,0))) nr_dig_nosso_numero,
		lpad('0',10,'0') vl_desconto_dia,
		'2' cd_condicao,
		'N' ie_emite_papeleta,
		'R' ie_rateio,
		'1' ie_endereco,
		lpad(nvl(substr(c.cd_ocorrencia,1,2),'1'),2,'0') ie_ocorrencia,
		lpad('0',10,'0') nr_documento,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
		lpad(nvl(elimina_caracteres_especiais(b.vl_titulo),0),13,'0') vl_titulo,
		lpad('0',3,'0') cd_banco_cobranca,
		lpad('0',5,'0') cd_agencia_deposito,
		'12' ie_especie,
		'N' ie_aceite,
		to_char(b.dt_emissao,'ddmmyy') dt_emissao,
		lpad(substr(nvl(c.cd_instrucao,'0'),1,2),2,'0') ie_instrucao1,
		'00' ie_instrucao2,
		lpad(elimina_caracteres_especiais(nvl(c.vl_acrescimo,'0')),13,'0') vl_acrescimo,
		to_char(sysdate,'ddmmyy') dt_desconto,
		lpad(elimina_caracteres_especiais(nvl(c.vl_desconto,'0')),13,'0') vl_desconto,
		lpad('0',13,'0') vl_iof,
		lpad('0',13,'0') vl_abatimento,
		decode(b.cd_cgc, null,'01','02') ie_tipo_inscricao,
		lpad(b.cd_cgc_cpf,14,' ') nr_inscricao,
		rpad(substr(upper(elimina_acentuacao(nvl(b.nm_pessoa,''))),1,40),40,' ') nm_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'ECM'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'EC')),1,40),40,' ') ds_endereco_sacado,			
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8) cd_cep,
		lpad(' ',60,' ') nm_avalista,
		lpad(to_char(a.nr_sequencia),7,'0') nr_seq_arquivo
	from	banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',393,' '),
	lpad(' ',277,' '),
	lpad(' ',20,' '),
	lpad(' ',12,' '),
	lpad(' ',10,' '),
	lpad(' ',8,' '),
	lpad(' ',2,' ')
into	ds_brancos_393_w,
	ds_brancos_277_w,
	ds_brancos_20_w,
	ds_brancos_12_w,
	ds_brancos_10_w,
	ds_brancos_8_w,
	ds_brancos_2_w
from	dual;

/* Header */
select	lpad(substr(nvl(c.cd_convenio_banco,'0'),1,20),20,'0'),
	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30))),30,' '),
	to_char(nvl(a.dt_remessa_retorno,sysdate),'ddmmyy'),
	lpad(to_char(a.nr_sequencia),7,'0')
into	cd_empresa_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w
from	estabelecimento		b,
	cobranca_escritural	a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

nr_seq_registro2_w := nr_seq_registro2_w + 1;

ds_conteudo_w	:= '01REMESSA01COBRANCA       ' || cd_empresa_w || nm_empresa_w || '237Bradesco       ' || dt_geracao_w ||
			ds_brancos_8_w|| 'MX' || nr_seq_arquivo_w || ds_brancos_277_w || lpad(nr_seq_registro2_w,6,'0');	
		
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_registro2_w);
/* Fim Header */

/* Transa��o */
--begin
open C01;
loop
fetch C01 into	
	cd_agencia_debito_w,
	ie_digito_agencia_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	ie_digito_conta_w,
	id_empresa_w,
	nr_controle_partic_w,
	cd_banco_w,
	ie_multa_w,
	pr_multa_w,
	nr_nosso_numero_w,
	nr_dig_nosso_numero_w,
	vl_desconto_dia_w,
	cd_condicao_w,
	ie_emite_papeleta_w,
	ie_rateio_w,
	ie_endereco_w,
	ie_ocorrencia_w,
	nr_documento_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_banco_cobranca_w,
	cd_agencia_deposito_w,
	ie_especie_w,
	ie_aceite_w,
	dt_emissao_w,
	ie_instrucao_1_w,
	ie_instrucao_2_w,
	vl_acrescimo_w,
	dt_desconto_w,
	vl_desconto_w,
	vl_iof_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	cd_cep_w,
	nm_avalista_w,
	nr_seq_arquivo_w;
exit when C01%notfound;
	begin
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;
	
	nr_seq_registro2_w := nr_seq_registro2_w + 1;
	
	ds_conteudo_w	:= 	'1' || cd_agencia_debito_w || ie_digito_agencia_w || cd_agencia_bancaria_w || cd_conta_w || ie_digito_conta_w || 
				id_empresa_w || nr_controle_partic_w || cd_banco_w || ie_multa_w || pr_multa_w || nr_nosso_numero_w ||
				nr_dig_nosso_numero_w || vl_desconto_dia_w || cd_condicao_w || ie_emite_papeleta_w || ds_brancos_10_w ||
				ie_rateio_w || ie_endereco_w || ds_brancos_2_w || ie_ocorrencia_w || nr_documento_w || dt_vencimento_w || 
				vl_titulo_w || cd_banco_cobranca_w || cd_agencia_deposito_w || ie_especie_w || ie_aceite_w || dt_emissao_w || 
				ie_instrucao_1_w || ie_instrucao_2_w || vl_acrescimo_w || dt_desconto_w || vl_desconto_w || vl_iof_w ||
				vl_abatimento_w || ie_tipo_inscricao_w || nr_inscricao_w || nm_sacado_w || ds_endereco_sacado_w ||
				ds_brancos_12_w || cd_cep_w ||nm_avalista_w || lpad(nr_seq_registro2_w,6,'0');
	
	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	nr_seq_registro_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_registro2_w);
	
	qt_registros_w	:= qt_registros_w + 1;
	
	if	(qt_registros_w = 500) then
		qt_registros_w	:= 0;
		commit;
	end if;
	end;
end loop;
close C01;
/* Fim Transa��o */

/* Trailler */
nr_seq_registro2_w := nr_seq_registro2_w + 1;

ds_conteudo_w	:= '9' || ds_brancos_393_w || lpad(nr_seq_registro2_w,6,'0');	

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;
		
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_registro2_w);
/* Fim Trailler*/

commit;

end gerar_cobr_bradesco_400_be_v06;
/