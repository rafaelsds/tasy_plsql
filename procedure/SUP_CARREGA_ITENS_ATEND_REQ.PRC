create or replace
procedure sup_carrega_itens_atend_req(	nr_requisicao_p			number,                  
					nr_seq_item_p			number,
					cd_unidade_medida_p		varchar2,
					cd_estabelecimento_p		number,
					dt_atendimento_p			date, 
					cd_pessoa_recebe_p		varchar2,
					cd_pessoa_atende_p		varchar2,
					ie_acao_p			varchar2,
					cd_motivo_baixa_p			number,
					qt_estoque_p			number,
					cd_unidade_medida_estoque_p	varchar2,
					cd_conta_contabil_p		varchar2,
					cd_material_p			number,
					cd_material_req_p			number,
					nr_seq_lote_fornec_p		number,
					cd_cgc_fornecedor_p		varchar2,
					qt_material_requisitada_p		number,
					ds_observacao_p			varchar,
					nm_usuario_retirada_p		varchar2,
					ds_justificativa_p			varchar2,
					ds_justificativa_atend_p		varchar2,
					qt_material_atendida_p		number,
					cd_material_lido_p			number,
					ie_geracao_p			varchar2,
					vl_material_p			number,
					nr_documento_externo_p		number,
					nr_item_docto_externo_p		number,
					nm_usuario_aprov_p		varchar2,
					dt_aprovacao_p			date,
					vl_unit_previsto_p			number,
					dt_reprovacao_p			date,
					nr_seq_cor_exec_p			number,
					vl_preco_venda_p			number,
					nr_seq_etapa_gpi_p		number,
					ie_msg_estoque_max_p		varchar2, 
					qt_estoque_superou_p		number,
					nr_seq_aprovacao_p		number,
					cd_kit_material_p			number,
					nm_usuario_p			varchar2) is

nr_sequencia_w			number(10);
qt_existe_w			number(10);
dt_baixa_w			date;
dt_solicitacao_requisicao_w		date;
cd_local_estoque_w		number(4);
cd_local_estoque_destino_w		number(4);
ie_entrada_saida_w		varchar2(1);
qt_itens_w			number(10);
ds_material_w			varchar2(255);
ds_fixo_w			varchar2(255);
ds_local_estoque_destino_w		varchar2(100);
ds_local_estoque_w		varchar2(100);
ie_status_w			varchar2(1);
ie_de_para_unid_med_w			varchar2(15);
ie_de_para_material_w			varchar2(15);
cd_unidade_medida_w			varchar2(30);
cd_unidade_medida_estoque_w		varchar2(30);
cd_material_w				number(6);
cd_material_req_w			number(6);
cd_material_lido_w			number(6);

begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	sup_int_requisicao
where	nr_requisicao = nr_requisicao_p;

if	(nr_sequencia_w = 0) then

	select	sup_int_requisicao_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into sup_int_requisicao(
		nr_sequencia,
		ie_forma_integracao,
		nr_requisicao,
		cd_estabelecimento,
		cd_local_estoque,
		dt_solicitacao_requisicao,
		dt_atualizacao,
		nm_usuario,
		cd_operacao_estoque,
		cd_pessoa_requisitante,
		cd_pessoa_atendente,
		cd_pessoa_solicitante,
		cd_setor_atendimento,
		cd_estabelecimento_destino,
		cd_local_estoque_destino,
		cd_setor_entrega,
		cd_centro_custo,
		dt_baixa,
		dt_lib_requisicao,
		dt_emissao_setor,
		dt_emissao_loc_estoque,
		ds_observacao,
		nm_usuario_lib,
		dt_aprovacao,
		nr_seq_justificativa,
		nr_seq_ordem_serv,
		ie_urgente,
		ie_geracao,
		nr_documento_externo,
		nr_seq_inv_roupa,
		nr_seq_protocolo,
		nr_atendimento,
		nr_adiantamento,
		nm_usuario_aprov,
		nm_usuario_recebedor,
		dt_recebimento,
		nr_seq_proj_gpi)
	select	nr_sequencia_w,
		'E',
		nr_requisicao,		
		cd_estabelecimento,
		cd_local_estoque,
		dt_solicitacao_requisicao,
		sysdate,
		nm_usuario_p,
		cd_operacao_estoque,
		cd_pessoa_requisitante,
		cd_pessoa_atendente,
		cd_pessoa_solicitante,
		cd_setor_atendimento,
		cd_estabelecimento_destino,
		cd_local_estoque_destino,
		cd_setor_entrega,
		cd_centro_custo,
		dt_baixa,
		dt_liberacao,
		dt_emissao_setor,
		dt_emissao_loc_estoque,
		ds_observacao,
		nm_usuario_lib,
		dt_aprovacao,
		nr_seq_justificativa,
		nr_seq_ordem_serv,
		ie_urgente,
		ie_geracao,
		nr_documento_externo,
		nr_seq_inv_roupa,
		nr_seq_protocolo,
		nr_atendimento,
		nr_adiantamento,
		nm_usuario_aprov,
		nm_usuario_recebedor,
		dt_recebimento,
		nr_seq_proj_gpi
	from	requisicao_material
	where	nr_requisicao = nr_requisicao_p;
end if;

select	dt_solicitacao_requisicao,
	cd_local_estoque,
	cd_local_estoque_destino,
	obter_dados_operacao_estoque(cd_operacao_estoque,'E') ie_entrada_saida,	
	obter_desc_local_estoque(cd_local_estoque) ds_local_estoque_destino,
	obter_desc_local_estoque(cd_local_estoque_destino) ds_local_estoque
into	dt_solicitacao_requisicao_w,
	cd_local_estoque_w,
	cd_local_estoque_destino_w,
	ie_entrada_saida_w,
	ds_local_estoque_w,
	ds_local_estoque_destino_w	
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

select	count(*)
into	qt_existe_w
from	sup_int_req_item
where	nr_sequencia	= nr_sequencia_w
and	nr_seq_item	= nr_seq_item_p;

if	(qt_existe_w > 0) then
	delete sup_int_req_item
	where	nr_sequencia	= nr_sequencia_w
	and	nr_seq_item	= nr_seq_item_p;
end if;

select	obter_ie_de_para_sup_integr('RM','E','UNIDADE_MEDIDA'),
	obter_ie_de_para_sup_integr('RM','E','MATERIAL')
into	ie_de_para_unid_med_w,
	ie_de_para_material_w
from	dual;

/*Conversao para unidade de medida*/
cd_unidade_medida_estoque_w	:= cd_unidade_medida_estoque_p;
cd_unidade_medida_w		:= cd_unidade_medida_p;

if	(ie_de_para_unid_med_w = 'C') then
	if	(cd_unidade_medida_w is not null) then
		cd_unidade_medida_w		:= nvl(Obter_Conversao_externa(null,'UNIDADE_MEDIDA','CD_UNIDADE_MEDIDA',cd_unidade_medida_w),cd_unidade_medida_w);
	end if;
	
	if	(cd_unidade_medida_estoque_w is not null) then
		cd_unidade_medida_estoque_w	:= nvl(Obter_Conversao_externa(null,'UNIDADE_MEDIDA','CD_UNIDADE_MEDIDA',cd_unidade_medida_estoque_w),cd_unidade_medida_estoque_w);
	end if;
	
elsif	(ie_de_para_unid_med_w = 'S') then
	if	(cd_unidade_medida_w is not null) then
		cd_unidade_medida_w		:= nvl(obter_dados_unid_medida(cd_unidade_medida_w,'SA'),cd_unidade_medida_w);
	end if;
	
	if	(cd_unidade_medida_estoque_w is not null) then
		cd_unidade_medida_estoque_w	:= nvl(obter_dados_unid_medida(cd_unidade_medida_estoque_w,'SA'),cd_unidade_medida_estoque_w);
	end if;
end if;
/*Fim*/

/*Conversao para material*/
cd_material_w		:= cd_material_p;
cd_material_req_w	:= cd_material_req_p;
cd_material_lido_w	:= cd_material_lido_p;
if	(ie_de_para_material_w = 'C') then
	cd_material_w		:= nvl(Obter_Conversao_externa(null,'MATERIAL','CD_MATERIAL',cd_material_w),cd_material_w);
	if	(cd_material_req_w is not null) then
		cd_material_req_w	:= nvl(Obter_Conversao_externa(null,'MATERIAL','CD_MATERIAL',cd_material_req_w),cd_material_req_w);
	end if;
	
	if	(cd_material_lido_w is not null) then
		cd_material_lido_w	:= nvl(Obter_Conversao_externa(null,'MATERIAL','CD_MATERIAL',cd_material_lido_w),cd_material_lido_w);
	end if;
	
elsif	(ie_de_para_material_w = 'S') then		
	cd_material_w	:= nvl(obter_dados_material_estab(cd_material_w, cd_estabelecimento_p, 'CSA'),cd_material_w);
	
	if	(cd_material_req_w is not null) then
		cd_material_req_w	:= nvl(obter_dados_material_estab(cd_material_req_w, cd_estabelecimento_p, 'CSA'),cd_material_req_w);
	end if;
	
	if	(cd_material_lido_w is not null) then
		cd_material_lido_w	:= nvl(obter_dados_material_estab(cd_material_lido_w, cd_estabelecimento_p, 'CSA'),cd_material_lido_w);
	end if;
end if;
/*Fim*/

ds_material_w := obter_desc_material(cd_material_w);

insert into sup_int_req_item(
	nr_sequencia,
	nr_requisicao,
	nr_seq_item,
	dt_atualizacao,
	nm_usuario,
	cd_unidade_medida,
	dt_atendimento,
	cd_pessoa_recebe,
	cd_pessoa_atende,
	ie_acao,
	cd_motivo_baixa,
	qt_estoque,
	cd_unidade_medida_estoque,
	cd_conta_contabil,
	cd_material,
	cd_material_req,
	nr_seq_lote_fornec,
	cd_cgc_fornecedor,
	qt_material_requisitada,
	ds_observacao,
	nm_usuario_retirada,
	ds_justificativa,
	ds_justificativa_atend,
	qt_material_atendida,
	cd_material_lido,
	ie_geracao,
	vl_material,
	nr_documento_externo,
	nr_item_docto_externo,
	nm_usuario_aprov,
	dt_aprovacao,
	vl_unit_previsto,
	dt_reprovacao,
	nr_seq_cor_exec,
	vl_preco_venda,
	nr_seq_etapa_gpi,
	ie_msg_estoque_max,
	qt_estoque_superou,
	nr_seq_aprovacao,               
	cd_kit_material,
	dt_solicitacao_requisicao,
	cd_local_estoque,
	cd_local_estoque_destino,
	ie_entrada_saida,
	qt_itens,
	ds_material,
	ds_fixo,
	ds_local_estoque_destino,
	ds_local_estoque,
	ie_status)
values(	nr_sequencia_w,
	nr_requisicao_p,
	nr_seq_item_p,
	sysdate,
	nm_usuario_p,
	cd_unidade_medida_w,
	dt_atendimento_p,
	cd_pessoa_recebe_p,
	cd_pessoa_atende_p,
	ie_acao_p,
	cd_motivo_baixa_p,
	qt_estoque_p,
	cd_unidade_medida_estoque_w,
	cd_conta_contabil_p,
	cd_material_w,
	cd_material_req_w,
	nr_seq_lote_fornec_p,
	cd_cgc_fornecedor_p,
	qt_material_requisitada_p,
	ds_observacao_p,
	nm_usuario_retirada_p,
	ds_justificativa_p,
	ds_justificativa_atend_p,
	qt_material_atendida_p,
	cd_material_lido_w,
	ie_geracao_p,
	vl_material_p,
	nr_documento_externo_p,
	nr_item_docto_externo_p,
	nm_usuario_aprov_p,
	dt_aprovacao_p,
	vl_unit_previsto_p,
	dt_reprovacao_p,
	nr_seq_cor_exec_p,
	vl_preco_venda_p,
	nr_seq_etapa_gpi_p,
	ie_msg_estoque_max_p,
	qt_estoque_superou_p,
	nr_seq_aprovacao_p,
	cd_kit_material_p,
	dt_solicitacao_requisicao_w,
	cd_local_estoque_w,
	cd_local_estoque_destino_w,
	ie_entrada_saida_w,
	0, -- passei zero pois passo a quantide no final da procedure.
	ds_material_w,
	'TREAT',
	ds_local_estoque_destino_w,
	ds_local_estoque_w,
	0);

select	dt_baixa
into	dt_baixa_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

if	(dt_baixa_w is not null) then
	update	sup_int_requisicao
	set	dt_liberacao = sysdate
	where	nr_sequencia = nr_sequencia_w;

	update	sup_int_req_item
	set	ie_status = 1
	where	nr_requisicao = nr_requisicao_p;
end if;

select	count(*)
into	qt_itens_w
from	sup_int_req_item
where	nr_requisicao = nr_requisicao_p;

update	sup_int_req_item
set	qt_itens = qt_itens_w
where	nr_requisicao = nr_requisicao_p;

end sup_carrega_itens_atend_req;
/
