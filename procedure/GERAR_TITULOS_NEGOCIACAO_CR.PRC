create or replace
procedure gerar_titulos_negociacao_cr
			(	nr_seq_negociacao_p	number,
				nm_usuario_p		varchar2) is
					

cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
ie_regulamentacao_plano_w	varchar2(2);
ie_liq_inadimplencia_w		varchar2(1)	:= 'N';
ie_pls_w			varchar2(1);
ie_status_pagador_w		varchar2(1);
ie_trans_baixa_tit_neg_w	varchar2(1);
vl_vencimento_w			number(15,2);
vl_negociacao_w			number(15,2);
vl_debito_w			number(15,2);
vl_baixa_w			number(15,2);
vl_total_titulos_w		number(15,2);
vl_negociado_titulo_w		number(15,2);
vl_saldo_negociacao_w		number(15,2);
vl_mensalidade_w		number(15,2);
vl_mensalidade_seg_w		number(15,2);
vl_inserido_mens_w		number(15,2);
vl_total_w			number(15,2);
vl_tesouraria_w			number(15,2);
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_plano_saude_w		number(15,2);
vl_hospital_w			number(15,2);
vl_titulos_origem_w		number(15,2)	:= 0;
vl_preco_pre_segurado_w		number(15,2);
vl_total_preco_pre_w		number(15,2);
vl_lancamento_w			number(15,2);
vl_inserido_w			number(15,2);
vl_hospital_ww			number(15,2)	:= 0;
vl_plano_saude_ww		number(15,2)	:= 0;
vl_total_lancamentos_w		number(15,2);
tx_negociacao_w			number(15,2);
tx_negociacao_prop_w		number(15,2);
vl_multa_neg_w			number(15,2);
vl_juros_neg_w			number(15,2);
vl_outros_acrescimos_w		number(15,2);
vl_descontos_w			number(15,2);
vl_desconto_w			number(15,2);
nr_seq_conta_banco_w		number(15)	:= null;
nr_seq_pagador_fin_w		number(15);
nr_seq_trans_fin_baixa_w	number(15);
nr_seq_boleto_w			number(10);
nr_seq_debito_w			number(10);
nr_titulo_w			number(10);
cd_portador_w			number(10);
cd_tipo_taxa_juro_w		number(10);
cd_tipo_taxa_multa_w		number(10);
nr_negociacao_w			number(10);
qt_registro_w			number(10);
nr_seq_baixa_w			number(10);
nr_seq_negociacao_mens_w	number(10);
nr_seq_pagador_w		number(10);
nr_seq_lancamento_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_motivo_lanc_mens_w	number(10);
qt_segurado_w			number(10);
nr_seq_trans_baixa_tit_neg_w	number(10);
nr_seq_trans_tit_neg_cr_w	number(10);
cd_tipo_taxa_juros_regra_w	number(10);
nr_seq_regra_resultado_w	number(10);
nr_seq_mensalidade_w		number(10);
nr_seq_trans_neg_cr_w		number(10);
nr_seq_trans_neg_cr_ops_w	number(10);
nr_seq_trans_tit_neg_cr_deb_w	number(10);
nr_seq_trans_bx_tit_deb_neg_w	number(10);
nr_seq_trans_bx_tit_bol_neg_w	number(10);
ie_segurado_w			number(10);
qt_segurado_mens_w		number(10);
nr_seq_classe_tit_neg_w		number(10);
nr_seq_lancamento_prog_w	number(10);
nr_seq_regra_w			number(10);
nr_seq_conta_banco_neg_w	number(10)	:= null;
nr_seq_carteira_cobr_neg_w	number(10)	:= null;
nr_seq_conta_banco_dep_w	number(10)	:= null;
nr_seq_conta_banco_dep_ant_w	number(10)	:= null;
nr_seq_deposito_w		number(10)	:= null;
nr_seq_trans_neg_dep_contab_w	number(10);
nr_seq_trans_neg_dep_baixa_w	number(10);
tx_juros_regra_w		number(7,4);
tx_juros_w			number(7,4);
tx_multa_w			number(7,4);
cd_tipo_portador_w		number(5);
cd_moeda_padrao_w		number(5);
cd_tipo_recebimento_w		number(5);
cd_estabelecimento_w		number(4);
dt_fechamento_w			date;
dt_recebimento_w		date	:= null;
dt_vencimento_w			date;
dt_debito_w			date;
dt_negociacao_w			date;
dt_mes_referencia_w		date;
dt_emissao_w			date;
nr_seq_classe_w			number(10);
cd_cgc_estab_w			estabelecimento.cd_cgc%type;
cd_tipo_pessoa_w		pessoa_juridica.cd_tipo_pessoa%type;
ie_comercial_ops_w		tipo_pessoa_juridica.ie_comercial_ops%type;
nr_seq_pagador_negociador_w	negociacao_cr.nr_seq_pagador%type;
cd_estab_financeiro_w	estabelecimento.cd_estab_financeiro%type;	


Cursor C01 is
	select	a.nr_sequencia,
		a.dt_vencimento,
		a.vl_parcela
	from	negociacao_cr_boleto a
	where	a.nr_seq_negociacao	= nr_seq_negociacao_p
	order by
		a.dt_vencimento;

Cursor C02 is
	select	a.nr_sequencia,
		a.dt_debito,
		a.vl_debito
	from	negociacao_cr_deb_cc a
	where	a.nr_seq_negociacao	= nr_seq_negociacao_p
	order by
		a.dt_debito;

Cursor C03 is
	select	b.nr_titulo,
		b.vl_negociado,
		b.vl_juros,
		b.vl_multa,
		a.nr_seq_mensalidade,
		a.ie_pls
	from	titulo_receber		a,
		titulo_rec_negociado	b
	where	a.nr_titulo		= b.nr_titulo
	and	b.nr_seq_negociacao	= nr_seq_negociacao_p;

Cursor C04 is
	select	a.nr_sequencia,
		a.dt_mes_referencia,
		a.vl_mensalidade,
		a.nr_seq_pagador
	from	negociacao_cr_pls_mens a
	where	a.nr_seq_negociacao	= nr_seq_negociacao_p
	order by
		a.dt_mes_referencia;

Cursor C05 is
	select	a.nr_sequencia
	from	pls_segurado a
	where	a.nr_seq_pagador	= nr_seq_pagador_w
	and	a.dt_liberacao is not null
	and	a.dt_rescisao is null;

Cursor C06 is
	select	a.nr_sequencia,
		a.dt_vencimento,
		a.vl_deposito,
		a.nr_seq_conta_banco
	from	negociacao_cr_dep_ident a
	where	a.nr_seq_negociacao	= nr_seq_negociacao_p
	order by
		a.dt_vencimento;

begin
if	(nr_seq_negociacao_p is not null) then
	/* Obter dados da negociacao */
	select	a.dt_negociacao,
		nvl(a.dt_fechamento,sysdate),
		a.cd_estabelecimento,
		a.cd_pessoa_fisica,
		a.cd_cgc,
		a.vl_negociado,
		a.nr_seq_regra_resultado,
		a.tx_negociacao,
		nvl(a.vl_juros,0),
		nvl(a.vl_multa,0),
		a.vl_desconto,
		a.nr_seq_classe,
		a.nr_seq_pagador
	into	dt_negociacao_w,
		dt_fechamento_w,
		cd_estabelecimento_w,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		vl_negociacao_w,
		nr_seq_regra_resultado_w,
		tx_negociacao_w,
		vl_juros_neg_w,
		vl_multa_neg_w,
		vl_desconto_w,
		nr_seq_classe_w,
		nr_seq_pagador_negociador_w
	from	negociacao_cr a
	where	a.nr_sequencia	= nr_seq_negociacao_p;
	
	if (cd_estabelecimento_w is not null) then
		select	obter_estab_financeiro(cd_estabelecimento_w)
		into	cd_estab_financeiro_w
		from 	dual;
	end if;
	
	update	negociacao_cr_parcela
	set	dt_vencimento		= dt_fechamento_w
	where	nr_seq_negociacao	= nr_seq_negociacao_p
	and	dt_vencimento		< dt_fechamento_w /*lhalves OS 609920 em 26/06/2013 - Somente atualizar se a data de vencimento da parcela for menor que a data de fechamento*/
	and	nr_parcela		= 1;

	begin
	select	cd_moeda_padrao,
		cd_tipo_taxa_juro,
		cd_tipo_taxa_multa,
		cd_tipo_portador,
		cd_portador,
		nr_seq_motivo_lanc_mens,
		nr_seq_trans_baixa_tit_neg,
		nr_seq_trans_tit_neg_cr,
		pr_juro_padrao,
		pr_multa_padrao,
		nr_seq_trans_neg_cr,
		nr_seq_trans_tit_neg_cr_deb,
		nr_seq_trans_bx_tit_deb_neg,
		nr_seq_trans_bx_tit_bol_neg,
		cd_tipo_receb_negociacao,
		nr_seq_classe_tit_neg,
		ie_trans_baixa_tit_neg,
		nr_seq_conta_banco_neg,
		nr_seq_carteira_cobr_neg,
		nr_seq_trans_neg_dep_contab,
		nr_seq_trans_neg_dep_baixa		
	into	cd_moeda_padrao_w,
		cd_tipo_taxa_juro_w,
		cd_tipo_taxa_multa_w,
		cd_tipo_portador_w,
		cd_portador_w,
		nr_seq_motivo_lanc_mens_w,
		nr_seq_trans_baixa_tit_neg_w,
		nr_seq_trans_tit_neg_cr_w,
		tx_juros_w,
		tx_multa_w,
		nr_seq_trans_neg_cr_w,
		nr_seq_trans_tit_neg_cr_deb_w,
		nr_seq_trans_bx_tit_deb_neg_w,
		nr_seq_trans_bx_tit_bol_neg_w,
		cd_tipo_recebimento_w,
		nr_seq_classe_tit_neg_w,
		ie_trans_baixa_tit_neg_w,
		nr_seq_conta_banco_neg_w,
		nr_seq_carteira_cobr_neg_w,
		nr_seq_trans_neg_dep_contab_w,
		nr_seq_trans_neg_dep_baixa_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_w;
	exception
	when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(175702);
	end;
	
	if (cd_estabelecimento_w is not null) then
	
		select	max(CD_CGC) /*Buscar CGC do estabelecimento do titulo*/
		into	cd_cgc_estab_w
		from	estabelecimento
		where	cd_Estabelecimento = cd_estabelecimento_w;
		
		if (cd_cgc_estab_w is not null) then
		
			select	max(cd_tipo_pessoa) /*Buscar o tipo de pessoa vinculada a esse CGC*/ 
			into	cd_tipo_pessoa_w
			from	pessoa_juridica
			where	cd_cgc = cd_cgc_estab_w;
			
			if (cd_tipo_pessoa_w is not null) then
			
				select	max(ie_comercial_ops) /*Verificar se o tipo de pessoa e OPS, se for, buscar os dados do OPS.*/
				into	ie_comercial_ops_w	
				from	tipo_pessoa_juridica
				where	cd_tipo_pessoa = cd_tipo_pessoa_w;
							
			end if;
		
		end if;
	
	end if;
	
	if ( nvl(ie_comercial_ops_w,'X') <> 'X')  then   
	
		if (ie_comercial_ops_w = 'O') then /*Somente buscar os parametros caso o tipo de pessoa ddo cgc do estabelecimento for Operadora de plano de saude.*/
			begin
			select	pr_juro_padrao,
					pr_multa_padrao
			into	tx_juros_w,
					tx_multa_w
			from	pls_parametros
			where	cd_estabelecimento	= cd_estabelecimento_w;
			exception
			when no_data_found then
				wheb_mensagem_pck.exibir_mensagem_abort(175703);
			end;
		
		end if;
	end if;
	

	
	select	max(nr_seq_trans_neg_cr)
	into	nr_seq_trans_neg_cr_ops_w
	from	pls_parametros_cr
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(nr_seq_trans_baixa_tit_neg_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(175704);
	end if;
	
	if	(cd_tipo_portador_w is null) or
		(cd_portador_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(175706);
	end if;
	
	select	max(a.nr_sequencia)
	into	nr_seq_pagador_w
	from	pls_contrato_pagador a,
		pls_mensalidade b,
		titulo_receber c,
		titulo_rec_negociado d
	where	d.nr_titulo		= c.nr_titulo
	and	c.nr_seq_mensalidade	= b.nr_sequencia
	and	b.nr_seq_pagador	= a.nr_sequencia
	and	d.nr_seq_negociacao	= nr_seq_negociacao_p;

	obter_conta_neg_boleto(nr_seq_negociacao_p,cd_estabelecimento_w,nr_titulo_w,nr_seq_conta_banco_w);
		
	if	(nr_seq_pagador_w is not null) and
		(nr_seq_conta_banco_w is null) then
		select	pls_obter_dados_pagador_fin(nr_seq_pagador_w,'NS')
		into	nr_seq_conta_banco_w
		from	dual;
	end if;
	
	-- Obter a conta cobranca da pasta Negociacao, da funcao Parametros do Contas a Receber
	if	(nr_seq_conta_banco_neg_w is not null) then
		nr_seq_conta_banco_w := nr_seq_conta_banco_neg_w;
	end if;
	
	if	(nr_seq_regra_resultado_w is not null) then
		select	a.tx_juros,
			a.cd_tipo_taxa_juros
		into	tx_juros_regra_w,
			cd_tipo_taxa_juros_regra_w
		from	regra_resultado_neg_cr a
		where	a.nr_sequencia	= nr_seq_regra_resultado_w;
	end if;	
	
	-- Parametro, funcao Negociacao de Contas a Receber, pasta Regra -> Regra classe
	select	nvl(pls_obter_dados_pagador(nr_seq_pagador_w,'S'),'A')
	into	ie_status_pagador_w
	from	dual;
	
	select	max(y.ie_regulamentacao)
	into	ie_regulamentacao_plano_w
	from	pls_contrato_pagador	w,
		pls_contrato_plano	x,
		pls_contrato		z,
		pls_plano		y
	where	x.nr_seq_contrato	= z.nr_sequencia
	and	x.nr_seq_plano		= y.nr_sequencia
	and	z.nr_sequencia		= w.nr_seq_contrato
	and	w.nr_sequencia		= nr_seq_pagador_w
	and	x.ie_situacao		= 'A'
	and	(x.dt_fim_vigencia is null or x.dt_fim_vigencia > trunc(dt_negociacao_w,'dd'));
		
	pls_obter_regra_tit_rec_negoc(ie_regulamentacao_plano_w,ie_status_pagador_w,dt_negociacao_w,nr_seq_regra_w,nm_usuario_p);      
	
	if	(nr_seq_regra_w is not null) then
		select	nvl(a.nr_seq_classe,nr_seq_classe_tit_neg_w)
		into	nr_seq_classe_tit_neg_w
		from	pls_regra_classe_tit_neg a
		where	a.nr_sequencia = nr_seq_regra_w;
	end if;
	-- Fim parametro, funcao Negociacao de Contas a Receber, pasta Regra -> Regra classe
	
	/* Gerar titulos de boletos */
	open C01;
	loop
	fetch C01 into
		nr_seq_boleto_w,
		dt_vencimento_w,
		vl_vencimento_w;
	exit when C01%notfound;
		begin
		select	titulo_seq.nextval
		into	nr_titulo_w
		from	dual;
		
		if	(dt_vencimento_w > dt_fechamento_w) then
			dt_vencimento_w	:= dt_vencimento_w;
			dt_emissao_w	:= dt_fechamento_w;
		else
			dt_emissao_w	:= dt_fechamento_w;
			dt_vencimento_w	:= dt_fechamento_w;
		end if;
		
		insert into titulo_receber
			(nr_titulo,
			nm_usuario,
			dt_atualizacao,
			cd_estabelecimento,
			cd_tipo_portador,
			cd_portador,
			dt_emissao,
			dt_vencimento,
			dt_pagamento_previsto,
			vl_titulo,
			vl_saldo_titulo,
			vl_saldo_juros,
			vl_saldo_multa,
			tx_juros,
			tx_multa,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			tx_desc_antecipacao,
			ie_tipo_titulo,
			ie_tipo_inclusao,
			ie_origem_titulo,
			cd_moeda,
			ie_situacao,
			cd_pessoa_fisica,
			cd_cgc,
			ie_tipo_emissao_titulo,
			nr_seq_trans_fin_contab,
			nr_seq_conta_banco,
			nr_seq_trans_fin_baixa,
			nr_seq_classe,
			nr_seq_carteira_cobr,
			ds_observacao_titulo,
			nr_seq_pagador,
			cd_estab_financeiro)
		values	(nr_titulo_w,
			nm_usuario_p,
			sysdate,
			cd_estabelecimento_w,
			cd_tipo_portador_w,
			cd_portador_w,
			trunc(dt_emissao_w,'dd'),
			dt_vencimento_w,
			dt_vencimento_w,
			vl_vencimento_w,
			vl_vencimento_w,
			0,
			0,
			nvl(tx_juros_w,tx_juros_regra_w),
			tx_multa_w,
			nvl(cd_tipo_taxa_juro_w, cd_tipo_taxa_juros_regra_w),
			cd_tipo_taxa_multa_w,
			0,
			'1', /* Bloqueto */
			'2',
			'4', /*Negociacao de contas a receber*/
			cd_moeda_padrao_w,
			'1',
			cd_pessoa_fisica_w,
			cd_cgc_w,
			'2' /* Emissao bloqueto origem */,
			nr_seq_trans_tit_neg_cr_w,
			nr_seq_conta_banco_w,
			nr_seq_trans_bx_tit_bol_neg_w,
			nvl(nr_seq_classe_w, nr_seq_classe_tit_neg_w),
			nr_seq_carteira_cobr_neg_w,
			wheb_mensagem_pck.get_texto(304337),
			nr_seq_pagador_negociador_w,
			cd_estab_financeiro_w);
			
		ratear_classif_titulo_neg_cr(nr_seq_negociacao_p,nr_titulo_w,nm_usuario_p);
		
		if	(nr_seq_conta_banco_w is not null) then
			gerar_bloqueto_tit_rec(nr_titulo_w, 'NE');
		end if;
		
		update	negociacao_cr_boleto
		set	nr_titulo	= nr_titulo_w
		where	nr_sequencia	= nr_seq_boleto_w;
		end;
	end loop;
	close C01;
		
	/* Gerar titulos de debito CC */
	open C02;
	loop
	fetch C02 into	
		nr_seq_debito_w,
		dt_debito_w,
		vl_debito_w;
	exit when C02%notfound;
		begin
		select	titulo_seq.nextval
		into	nr_titulo_w
		from	dual;
		
		if	(dt_debito_w > dt_fechamento_w) then
			dt_emissao_w	:= dt_debito_w;
		else
			dt_emissao_w	:= dt_fechamento_w;
		end if;
		
		insert into titulo_receber
			(nr_titulo,
			nm_usuario,
			dt_atualizacao,
			cd_estabelecimento,
			cd_tipo_portador,
			cd_portador,
			dt_emissao,
			dt_vencimento,
			dt_pagamento_previsto,
			vl_titulo,
			vl_saldo_titulo,
			vl_saldo_juros,
			vl_saldo_multa,
			tx_juros,
			tx_multa,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			tx_desc_antecipacao,
			ie_tipo_titulo,
			ie_tipo_inclusao,
			ie_origem_titulo,
			cd_moeda,
			ie_situacao,
			cd_pessoa_fisica,
			cd_cgc,
			ie_tipo_emissao_titulo,
			nr_seq_trans_fin_contab,
			nr_seq_conta_banco,
			nr_seq_trans_fin_baixa,
			nr_seq_classe,
			nr_seq_carteira_cobr,
			ds_observacao_titulo,
			nr_seq_pagador,
			cd_estab_financeiro)
		values	(nr_titulo_w,
			nm_usuario_p,
			sysdate,
			cd_estabelecimento_w,
			cd_tipo_portador_w,
			cd_portador_w,
			trunc(dt_emissao_w,'dd'),
			dt_emissao_w,
			dt_emissao_w,
			vl_debito_w,
			vl_debito_w,
			0,
			0,
			0,
			0,
			cd_tipo_taxa_juro_w,
			cd_tipo_taxa_multa_w,
			0,
			'10', /* Debito automatico */
			'2',
			'4', /* Negociacao de contas a receber */
			cd_moeda_padrao_w,
			'1',
			cd_pessoa_fisica_w,
			cd_cgc_w,
			'1' /* Sem emissao de bloqueto */,
			nr_seq_trans_tit_neg_cr_deb_w,
			nr_seq_conta_banco_w,
			nr_seq_trans_bx_tit_deb_neg_w,
			nvl(nr_seq_classe_w, nr_seq_classe_tit_neg_w),
			nr_seq_carteira_cobr_neg_w,
			wheb_mensagem_pck.get_texto(304338),
			nr_seq_pagador_negociador_w,
			cd_estab_financeiro_w);
			
		ratear_classif_titulo_neg_cr(nr_seq_negociacao_p,nr_titulo_w,nm_usuario_p);
		
		if	(nr_seq_conta_banco_w is not null) then
			Gerar_Bloqueto_Tit_Rec(nr_titulo_w, 'NE');
		end if;
		
		update	negociacao_cr_deb_cc
		set	nr_titulo	= nr_titulo_w
		where	nr_sequencia	= nr_seq_debito_w;
		end;
	end loop;
	close C02;
	
	/* Gerar titulos de deposito identificado */
	open C06;
	loop
	fetch C06 into
		nr_seq_boleto_w,
		dt_vencimento_w,
		vl_vencimento_w,
		nr_seq_conta_banco_dep_w;
	exit when C06%notfound;
		begin
		if	(nr_seq_conta_banco_dep_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(175683);
		end if;
		
		if	(nvl(nr_seq_conta_banco_dep_ant_w,0) <> nr_seq_conta_banco_dep_w) then
			select	deposito_identificado_seq.nextval
			into	nr_seq_deposito_w
			from	dual;
			
			insert into deposito_identificado
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				dt_inclusao,
				ie_status,
				vl_deposito,
				nr_seq_conta_banco,
				cd_estabelecimento,
				cd_pessoa_fisica,
				cd_cgc)
			values	(nr_seq_deposito_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				sysdate,
				'AI',
				0,
				nr_seq_conta_banco_dep_w,
				cd_estabelecimento_w,
				cd_pessoa_fisica_w,
				cd_cgc_w);
		else
			if	(cd_pessoa_fisica_w is not null) then
				select	max(a.nr_sequencia)
				into	nr_seq_deposito_w
				from	deposito_identificado	a
				where	a.nr_seq_conta_banco	= nr_seq_conta_banco_dep_w
				and	a.cd_pessoa_fisica	= cd_pessoa_fisica;
			else
				select	max(a.nr_sequencia)
				into	nr_seq_deposito_w
				from	deposito_identificado	a
				where	a.nr_seq_conta_banco	= nr_seq_conta_banco_dep_w
				and	a.cd_cgc		= cd_cgc_w;
			end if;
		end if;
		
		nr_seq_conta_banco_dep_ant_w	:= nr_seq_conta_banco_dep_w;
		
		select	titulo_seq.nextval
		into	nr_titulo_w
		from	dual;
		
		if	(dt_vencimento_w > dt_fechamento_w) then
			dt_emissao_w	:= dt_vencimento_w;
		else
			dt_emissao_w	:= dt_fechamento_w;
		end if;
		
		insert into titulo_receber
			(nr_titulo,
			nm_usuario,
			dt_atualizacao,
			cd_estabelecimento,
			cd_tipo_portador,
			cd_portador,
			dt_emissao,
			dt_vencimento,
			dt_pagamento_previsto,
			vl_titulo,
			vl_saldo_titulo,
			vl_saldo_juros,
			vl_saldo_multa,
			tx_juros,
			tx_multa,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			tx_desc_antecipacao,
			ie_tipo_titulo,
			ie_tipo_inclusao,
			ie_origem_titulo,
			cd_moeda,
			ie_situacao,
			cd_pessoa_fisica,
			cd_cgc,
			ie_tipo_emissao_titulo,
			nr_seq_trans_fin_contab,
			nr_seq_conta_banco,
			nr_seq_trans_fin_baixa,
			nr_seq_classe,
			nr_seq_carteira_cobr,
			ds_observacao_titulo,
			nr_seq_pagador,
			cd_estab_financeiro)
		values	(nr_titulo_w,
			nm_usuario_p,
			sysdate,
			cd_estabelecimento_w,
			cd_tipo_portador_w,
			cd_portador_w,
			trunc(dt_emissao_w,'dd'),
			dt_emissao_w,
			dt_emissao_w,
			vl_vencimento_w,
			vl_vencimento_w,
			0,
			0,
			nvl(tx_juros_w,tx_juros_regra_w),
			tx_multa_w,
			nvl(cd_tipo_taxa_juro_w, cd_tipo_taxa_juros_regra_w),
			cd_tipo_taxa_multa_w,
			0,
			'1', /* Bloqueto */
			'2',
			'4', /* Negocicao de contas a receber */
			cd_moeda_padrao_w,
			'1',
			cd_pessoa_fisica_w,
			cd_cgc_w,
			'2' /* Emissao bloqueto origem */,
			nr_seq_trans_neg_dep_contab_w,
			nr_seq_conta_banco_dep_w,
			nr_seq_trans_neg_dep_baixa_w,
			nvl(nr_seq_classe_w, nr_seq_classe_tit_neg_w),
			nr_seq_carteira_cobr_neg_w,
			wheb_mensagem_pck.get_texto(304339),
			nr_seq_pagador_negociador_w,
			cd_estab_financeiro_w);
			
		ratear_classif_titulo_neg_cr(nr_seq_negociacao_p,nr_titulo_w,nm_usuario_p);
		
		if	(nr_seq_conta_banco_w is not null) then
			Gerar_Bloqueto_Tit_Rec(nr_titulo_w, 'NE');
		end if;
		
		incluir_titulo_dep_ident(nr_seq_deposito_w,nr_titulo_w,nm_usuario_p,'N');
		
		atualiza_valor_dep_ident(nr_seq_deposito_w, obter_estabelecimento_ativo,nm_usuario_p,'N');
		
		update	negociacao_cr_dep_ident
		set	nr_seq_deposito_ident	= nr_seq_deposito_w
		where	nr_sequencia		= nr_seq_boleto_w;
		end;
	end loop;
	close C06;
	
	/* Gerar lancamentos programados para as mensalidades */
	open C04;
	loop
	fetch C04 into	
		nr_seq_negociacao_mens_w,
		dt_mes_referencia_w,
		vl_mensalidade_w,
		nr_seq_pagador_w;
	exit when c04%notfound;
		begin
		select	count(*)
		into	qt_segurado_mens_w
		from	pls_segurado a
		where	a.nr_seq_pagador	= nr_seq_pagador_w
		and	a.dt_liberacao is not null
		and	a.dt_rescisao is null;
							
		if (qt_segurado_mens_w = 0) then
			--R.aise_application_error(-20011,'O pagador nao esta liberado ou possui data de rescisao. Verifique na funcao OPS - Gestao de Contratos.');
			wheb_mensagem_pck.exibir_mensagem_abort(273852);
		end if;
		
		ie_segurado_w	:= 0;
		open C05;
		loop
		fetch C05 into	
			nr_seq_segurado_w;
		exit when C05%notfound;
			begin
			ie_segurado_w	:= ie_segurado_w + 1;
			
			vl_lancamento_w	:= dividir_sem_round(vl_mensalidade_w,qt_segurado_mens_w);
				
			select	pls_segurado_mensalidade_seq.nextval
			into	nr_seq_lancamento_prog_w
			from	dual;
				
			insert into pls_segurado_mensalidade
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				--nr_seq_pagador,
				dt_referencia,
				vl_item,
				ie_tipo_item,
				ie_situacao,
				nr_seq_negociacao_mens,
				cd_estabelecimento,
				nr_seq_segurado,
				ie_tipo_lanc)
			values	(nr_seq_lancamento_prog_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				--nr_seq_pagador_w,
				dt_mes_referencia_w,
				vl_lancamento_w,
				'18',
				'A',
				nr_seq_negociacao_mens_w,
				cd_estabelecimento_w,
				nr_seq_segurado_w,
				'P');
					
			/* Se for o ultimo fazer ajuste de arredondamento */
			if	(ie_segurado_w = qt_segurado_mens_w) then
				select	sum(nvl(vl_item,0))
				into	vl_total_lancamentos_w
				from	pls_segurado_mensalidade a
				where	a.nr_seq_negociacao_mens = nr_seq_negociacao_mens_w;
				
				if	(vl_total_lancamentos_w <> vl_mensalidade_w) then
					update	pls_segurado_mensalidade
					set	vl_item	= vl_item + (vl_mensalidade_w - vl_total_lancamentos_w)
					where	nr_sequencia	= nr_seq_lancamento_prog_w;
				end if;
			end if;
			end;
		end loop;
		close C05;
		end;
	end loop;
	close C04;
	
	/*Baixar titulos da negociacao proporcionalmente aos novos titulos gerados */
	if	(cd_tipo_recebimento_w is null) then
		select	max(cd_tipo_recebimento)
		into	cd_tipo_recebimento_w
		from	tipo_recebimento
		where	ie_tipo_consistencia	= 0
		and     ((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null));
	end if;
	
	select	sum(nvl(vl_titulo,0))
	into	vl_total_titulos_w
	from	(select	sum(nvl(a.vl_parcela,0)) vl_titulo
		from	negociacao_cr_boleto a
		where	a.nr_seq_negociacao	= nr_seq_negociacao_p
		union all
		select	sum(nvl(a.vl_debito,0)) vl_titulo
		from	negociacao_cr_deb_cc a
		where	a.nr_seq_negociacao	= nr_seq_negociacao_p
		union all
		select	sum(nvl(a.vl_mensalidade,0)) vl_titulo
		from	negociacao_cr_pls_mens a
		where	a.nr_seq_negociacao	= nr_seq_negociacao_p
		union all
		select	nvl(sum(a.vl_deposito),0) vl_titulo
		from	negociacao_cr_dep_ident a
		where	a.nr_seq_negociacao	= nr_seq_negociacao_p);
		
	/* Baixar todos os titulos envolvidos na negociacao */
	open C03;
	loop
	fetch C03 into	
		nr_titulo_w,
		vl_negociado_titulo_w,
		vl_juros_w,
		vl_multa_w,
		nr_seq_mensalidade_w,
		ie_pls_w;
	exit when C03%notfound;
		begin
		select	nvl(a.ie_liq_inadimplencia,'N')
		into	ie_liq_inadimplencia_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_w;
		
		-- Verifica se ha parametro, se houver busca primeiro do titulo a ser baixado, senao continua com o mesmo do Parametros do Contas a Receber
		if	(ie_trans_baixa_tit_neg_w = 'S') then
			select	nvl(nr_seq_trans_fin_baixa,nr_seq_trans_baixa_tit_neg_w)
			into	nr_seq_trans_baixa_tit_neg_w
			from	titulo_receber
			where	nr_titulo = nr_titulo_w;
		end if;
		
		/* So baixar se nao foi liquidado por inadimplencia */
		if	(ie_liq_inadimplencia_w = 'N') then
			vl_baixa_w	:= vl_negociado_titulo_w;

			vl_descontos_w		:= dividir_sem_round(vl_negociado_titulo_w,vl_negociacao_w) * vl_desconto_w;
			tx_negociacao_prop_w	:= dividir_sem_round(vl_negociado_titulo_w,vl_negociacao_w) * tx_negociacao_w;			
			
			baixa_titulo_receber(	cd_estabelecimento_w,
						cd_tipo_recebimento_w,
						nr_titulo_w,
						nr_seq_trans_baixa_tit_neg_w,
						vl_baixa_w,
						trunc(sysdate,'dd'),
						nm_usuario_p,
						0,
						null,
						null,
						0,
						0);
						
			if	(nr_seq_mensalidade_w is not null) or
				(ie_pls_w = 'S') then
				vl_plano_saude_w	:= vl_plano_saude_ww + vl_baixa_w + vl_juros_w + vl_multa_w;
				vl_plano_saude_ww	:= vl_plano_saude_w;
			else
				vl_hospital_w		:= vl_hospital_ww + vl_baixa_w + vl_juros_w + vl_multa_w;
				vl_hospital_ww		:= vl_hospital_w;
			end if;
			
			vl_titulos_origem_w	:= vl_titulos_origem_w + vl_baixa_w + + vl_juros_w +vl_multa_w;
						
			/* Obter ultima baixa */
			select	max(nr_sequencia)
			into	nr_seq_baixa_w
			from	titulo_receber_liq a
			where	a.nr_titulo	= nr_titulo_w;
			
			--tx_negociacao_prop_w	:= dividir_sem_round(vl_negociado_titulo_w,vl_negociacao_w) * tx_negociacao_w;
			--vl_descontos_w		:= dividir_sem_round(vl_negociado_titulo_w,vl_negociacao_w) * vl_desconto_w;
			
			update	titulo_receber_liq
			set	nr_seq_negociacao_cr	= nr_seq_negociacao_p,
				vl_juros		= vl_juros_w,
				vl_multa		= vl_multa_w,
				vl_outros_acrescimos	= tx_negociacao_prop_w,
				vl_descontos		= vl_descontos_w,
				vl_recebido		= vl_recebido - vl_descontos_w
			where	nr_titulo		= nr_titulo_w
			and	nr_sequencia		= nr_seq_baixa_w;
			
			/*AAMFIRMO OS 1117996 em 16/06/2016 - Inclui o delete abaixo antes de gerar novamente para nao deixar classificacao em duplicidade*/
			/*Nao pode chamar a RECALCULAR_TITULO_REC_LIQ_CC direto como tinha feito antes, pq essa tem commit, e essa proc nao pode ter commit;*/
			delete	from titulo_rec_liq_cc
			where	nr_titulo		= nr_titulo_w
			and		nr_seq_baixa	= nr_seq_baixa_w;
			
			/*PMGONCALVES - 13/06/2016 - Incluida a chamada para que passe a considerar os valores de juros, multa e desconto de titulos negociados*/			
			gerar_titulo_rec_liq_cc
					(cd_estabelecimento_w,
					null,
					nm_usuario_p,
					nr_titulo_w,
					nr_seq_baixa_w);
			
			atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);
		else
			update	titulo_receber
			set	ie_negociado	= 'S'
			where	nr_titulo	= nr_titulo_w;
		end if;
		end;
	end loop;
	close C03;
	
	select	sum(nvl(a.vl_outros_acrescimos,0))
	into	vl_outros_acrescimos_w
	from	titulo_receber_liq	a
	where	nr_seq_negociacao_cr	= nr_seq_negociacao_p;
	
	if	(vl_outros_acrescimos_w <> tx_negociacao_w) then
		select	max(a.nr_titulo),
			max(a.nr_sequencia)
		into	nr_titulo_w,
			nr_seq_baixa_w
		from	titulo_receber_liq	a
		where	a.nr_seq_negociacao_cr	= nr_seq_negociacao_p;
		
		update	titulo_receber_liq
		set	vl_outros_acrescimos	= vl_outros_acrescimos  + (tx_negociacao_w - vl_outros_acrescimos_w)
		where	nr_sequencia	= nr_seq_baixa_w
		and	nr_titulo	= nr_titulo_w;
	end if;
	
	select	sum(nvl(a.vl_descontos,0))
	into	vl_descontos_w
	from	titulo_receber_liq	a
	where	nr_seq_negociacao_cr	= nr_seq_negociacao_p;
	
	if	(vl_descontos_w <> vl_desconto_w) then
		select	max(a.nr_titulo),
			max(a.nr_sequencia)
		into	nr_titulo_w,
			nr_seq_baixa_w
		from	titulo_receber_liq	a
		where	a.nr_seq_negociacao_cr	= nr_seq_negociacao_p;
		
		update	titulo_receber_liq
		set	vl_descontos	= vl_descontos  + (vl_desconto_w - vl_descontos_w),
			vl_recebido	= vl_recebido  - (vl_desconto_w - vl_descontos_w)
		where	nr_sequencia	= nr_seq_baixa_w
		and	nr_titulo	= nr_titulo_w;
	end if;
	
	vl_plano_saude_ww	:= 0;
	vl_hospital_ww		:= 0;
	
	/* Gerar novo titulo com o saldo da negociacao */
	vl_total_w	:= obter_valores_negociacao_cr(nr_seq_negociacao_p,'VT');
	vl_tesouraria_w	:= vl_total_w - vl_total_titulos_w;
	
	if	(vl_tesouraria_w > 0) then	
		if	(vl_plano_saude_w > 0) then
			select	titulo_seq.nextval
			into	nr_titulo_w
			from	dual;	
			
			insert into titulo_receber
				(nr_titulo,
				nm_usuario,
				dt_atualizacao,
				cd_estabelecimento,
				cd_tipo_portador,
				cd_portador,
				dt_emissao,
				dt_vencimento,
				dt_pagamento_previsto,
				vl_titulo,
				vl_saldo_titulo,
				vl_saldo_juros,
				vl_saldo_multa,
				tx_juros,
				tx_multa,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				tx_desc_antecipacao,
				ie_tipo_titulo,
				ie_tipo_inclusao,
				ie_origem_titulo,
				cd_moeda,
				ie_situacao,
				cd_pessoa_fisica,
				cd_cgc,
				ie_tipo_emissao_titulo,
				nr_seq_negociacao_origem,
				nr_seq_trans_fin_contab,
				nr_seq_trans_fin_baixa,
				ds_observacao_titulo,
				nr_seq_classe,
				nr_seq_conta_banco,
				nr_seq_carteira_cobr,
				nr_seq_pagador,
				cd_estab_financeiro,
        ie_pls)
			values	(nr_titulo_w,
				nm_usuario_p,
				sysdate,
				cd_estabelecimento_w,
				cd_tipo_portador_w,
				cd_portador_w,
				trunc(dt_fechamento_w,'dd'),
				trunc(dt_fechamento_w,'dd'),
				trunc(dt_fechamento_w,'dd'),
				dividir_sem_round(vl_plano_saude_w,vl_titulos_origem_w) * vl_tesouraria_w,
				dividir_sem_round(vl_plano_saude_w,vl_titulos_origem_w) * vl_tesouraria_w,
				0,
				0,
				0,
				0,
				cd_tipo_taxa_juro_w,
				cd_tipo_taxa_multa_w,
				0,
				'1', /* Bloqueto */
				'2',
				'4', /* Negociacao de contas a receber */
				cd_moeda_padrao_w,
				'1',
				cd_pessoa_fisica_w,
				cd_cgc_w,
				'2' /* Emissao bloqueto origem */,
				nr_seq_negociacao_p,
				nr_seq_trans_tit_neg_cr_w,
				nvl(nr_seq_trans_neg_cr_ops_w,nr_seq_trans_neg_cr_w),
				wheb_mensagem_pck.get_texto(304340),
				nvl(nr_seq_classe_w, nr_seq_classe_tit_neg_w),
				nr_seq_conta_banco_neg_w,
				nr_seq_carteira_cobr_neg_w,
				nr_seq_pagador_negociador_w,
				cd_estab_financeiro_w,
        'S');
				
			ratear_classif_titulo_neg_cr(nr_seq_negociacao_p,nr_titulo_w,nm_usuario_p);
			
			if	(nr_seq_conta_banco_neg_w is not null) then
				Gerar_Bloqueto_Tit_Rec(nr_titulo_w, 'NE');
			end if;
		end if;
		
		if	(vl_hospital_w > 0) then

			select	titulo_seq.nextval
			into	nr_titulo_w
			from	dual;		
			
			insert into titulo_receber
				(nr_titulo,
				nm_usuario,
				dt_atualizacao,
				cd_estabelecimento,
				cd_tipo_portador,
				cd_portador,
				dt_emissao,
				dt_vencimento,
				dt_pagamento_previsto,
				vl_titulo,
				vl_saldo_titulo,
				vl_saldo_juros,
				vl_saldo_multa,
				tx_juros,
				tx_multa,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				tx_desc_antecipacao,
				ie_tipo_titulo,
				ie_tipo_inclusao,
				ie_origem_titulo,
				cd_moeda,
				ie_situacao,
				cd_pessoa_fisica,
				cd_cgc,
				ie_tipo_emissao_titulo,
				nr_seq_negociacao_origem,
				nr_seq_trans_fin_contab,
				nr_seq_trans_fin_baixa,
				ds_observacao_titulo,
				nr_seq_classe,
				nr_seq_conta_banco,
				nr_seq_carteira_cobr,
				cd_estab_financeiro)
			values	(nr_titulo_w,
				nm_usuario_p,
				sysdate,
				cd_estabelecimento_w,
				cd_tipo_portador_w,
				cd_portador_w,
				trunc(dt_fechamento_w,'dd'),
				trunc(dt_fechamento_w,'dd'),
				trunc(dt_fechamento_w,'dd'),
				dividir_sem_round(vl_hospital_w,vl_titulos_origem_w) * vl_tesouraria_w,
				dividir_sem_round(vl_hospital_w,vl_titulos_origem_w) * vl_tesouraria_w,
				0,
				0,
				0,
				0,
				cd_tipo_taxa_juro_w,
				cd_tipo_taxa_multa_w,
				0,
				'1', /* Bloqueto */
				'2',
				'4', /* Negociacao de contas a receber */
				cd_moeda_padrao_w,
				'1',
				cd_pessoa_fisica_w,
				cd_cgc_w,
				'2' /* Emissao bloqueto origem */,
				nr_seq_negociacao_p,
				nr_seq_trans_tit_neg_cr_w,
				nr_seq_trans_neg_cr_w,
				wheb_mensagem_pck.get_texto(304342),
				nvl(nr_seq_classe_w, nr_seq_classe_tit_neg_w),
				nr_seq_conta_banco_neg_w,
				nr_seq_carteira_cobr_neg_w,
				cd_estab_financeiro_w); 
				
			ratear_classif_titulo_neg_cr(nr_seq_negociacao_p,nr_titulo_w,nm_usuario_p);
			
			if	(nr_seq_conta_banco_neg_w is not null) then
				Gerar_Bloqueto_Tit_Rec(nr_titulo_w, 'NE');
			end if;
		end if;
	end if;
end if;

/* Nao pode dar commit */

end gerar_titulos_negociacao_cr;
/