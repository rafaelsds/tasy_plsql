create or replace
procedure consiste_abrir_conta_paciente(	nr_interno_conta_p	in number,
					nm_usuario_p	in varchar2,
					 ds_erro_p	out varchar2) is

ie_ok_w		number(5);
ie_permite_abrir_w	varchar2(10);

begin

ds_erro_p	:= '';

select	count(*) 
into	ie_ok_w
from 	nota_fiscal
where 	nr_interno_conta = nr_interno_conta_p;
if	(ie_ok_w <> 0) then
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(280056);
end if;

ie_permite_abrir_w := nvl(obter_valor_param_usuario(obter_funcao_ativa,244,obter_perfil_ativo,nm_usuario_p,0),'N');

select	count(*)
into	ie_ok_w
from	repasse_terceiro c,
	procedimento_repasse b,
	procedimento_paciente a
where	a.nr_sequencia = b.nr_seq_procedimento
and	b.nr_repasse_terceiro = c.nr_repasse_terceiro
and	a.nr_interno_conta = nr_interno_conta_p
and	c.ie_status = 'F';
if	(ie_ok_w 	<> 0) and
	(ie_permite_abrir_w	= 'N') then
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(280057);
end if;

ds_erro_p	:= ds_erro_p;

end consiste_abrir_conta_paciente;
/