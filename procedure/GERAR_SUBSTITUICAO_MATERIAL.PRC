create or replace
procedure gerar_substituicao_material(	nr_sequencia_matpaci_p		number,
					nr_sequencia_p			number,
					nr_sequencia_auditoria_p	number,
					cd_material_substituto_p 	varchar2,
					cd_motivo_audit_p		varchar2,
					nm_usuario_p		 	varchar2) is

nr_seq_gerada_w				number(10);
nr_seq_motivo_w				number(10);
nr_interno_conta_w			number(10);
qt_material_w				number(10);
					
begin

select	nr_interno_conta,
	qt_material
into	nr_interno_conta_w,
	qt_material_w
from 	material_atend_paciente
where	nr_sequencia = nr_sequencia_matpaci_p;	

if	(nr_sequencia_matpaci_p is not null) then

	update	auditoria_matpaci
	set	qt_ajuste = 0,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		nr_seq_motivo = cd_motivo_audit_p
	where	nr_sequencia = nr_sequencia_p;
	
	duplicar_mat_paciente(nr_sequencia_matpaci_p, nm_usuario_p, 'N', nr_seq_gerada_w);
	
	update 	material_atend_paciente
	set 	cd_material = cd_material_substituto_p,
		cd_material_exec = cd_material_substituto_p,
		cd_material_prescricao = cd_material_substituto_p,
		ie_auditoria = 'N'
	where 	nr_sequencia = nr_seq_gerada_w;
	
	atualiza_preco_material(nr_seq_gerada_w, nm_usuario_p);
	
	update 	material_atend_paciente
	set 	ie_auditoria = 'S'
	where 	nr_sequencia = nr_seq_gerada_w;
	
	Atualizar_Lista_Itens_Audit(nr_interno_conta_w, nr_seq_gerada_w, 1, qt_material_w, nm_usuario_p,
						cd_motivo_audit_p, nr_sequencia_auditoria_p, 'N');
	
end if;

commit;

end gerar_substituicao_material;
/
