create or replace
procedure Atualizar_Agenda_Tasy_Convi_JS(
					nr_seq_agenda_p			Number,
					dt_agenda_p				Date,
					ie_confirmado_p			Varchar2,
					ds_motivo_ausencia_p	Varchar2,
					nm_usuario_p			Varchar2) is

begin
/*
	procedure criada para realizar a chamada na procedure Atualizar_Agenda_Tasy_Convite
	e realizar o commit sem alterar o comportamento do sistema em Delphi.
	Criada pelo HEBERT.
*/
Atualizar_Agenda_Tasy_Convite( nr_seq_agenda_p, dt_agenda_p, ie_confirmado_p, ds_motivo_ausencia_p, nm_usuario_p );

commit;

end Atualizar_Agenda_Tasy_Convi_JS;
/