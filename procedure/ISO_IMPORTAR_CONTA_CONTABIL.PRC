create or replace
procedure iso_importar_conta_contabil is 


cd_empresa_w		number(10);
cd_conta_contabil_w	varchar2(20);
ds_erro_w		varchar2(500);
qt_registro_w		number(10)	:= 0;
qt_commit_w		number(10)	:= 0;
cd_grupo_w		number(10);


cursor c01 is
select	a.cd_classif,
	a.cd_conta,
	a.ds_conta,
	a.ds_grupo,
	a.ie_centro,
	a.ie_tipo
from	w_iso_conta_contabil a
order by cd_classif;

vet01	c01%rowtype;

begin

select	min(cd_empresa)
into	cd_empresa_w
from	empresa;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin

	
	begin
	
	cd_grupo_w	:= substr(vet01.cd_classif, 1, 1);
	
	select	count(*)
	into	qt_registro_w
	from	conta_contabil
	where	cd_conta_contabil	= vet01.cd_conta;	
	
	if	(qt_registro_w > 0) then
			
		select	max(somente_numero(cd_conta_contabil)) + 1
		into	cd_conta_contabil_w
		from	conta_contabil;
		
	else
		cd_conta_contabil_w	:= vet01.cd_conta;
	end if;
	
	
	insert into conta_contabil( 
		cd_conta_contabil,
		ds_conta_contabil,
		ie_situacao, 
		dt_atualizacao,
		nm_usuario,
		ie_centro_custo, 
		ie_tipo,
		cd_classificacao,
		cd_grupo, 
		cd_empresa,
		ie_compensacao)
	values( 	cd_conta_contabil_w,
		vet01.ds_conta,
		'A',
		sysdate,
		'Importacao',
		vet01.ie_centro, 
		vet01.ie_tipo,
		vet01.cd_classif,
		cd_grupo_w, 
		cd_empresa_w,
		'N');
	exception when others then
		ds_erro_w := SQLERRM(sqlcode);
	
		insert 	into log_tasy(
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(	sysdate,
			'Importacao',
			55755,
			'Conta: '|| cd_conta_contabil_w ||' Erro: '|| ds_erro_w);
	end;
	if	(qt_commit_w >= 500) then
		
		qt_commit_w	:= 0;
		commit;
	else
	
		qt_commit_w	:= qt_commit_w +1;
	end if;
	
	end;
end loop;
close C01;

commit;

end iso_importar_conta_contabil;
/