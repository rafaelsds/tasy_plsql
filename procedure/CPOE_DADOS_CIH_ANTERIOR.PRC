create or replace
procedure cpoe_dados_cih_anterior(nr_sequencia_p 	number,
								dt_inicio_cih_p	out date,
								qt_dias_solicitado_p out number,
								qt_dias_liberado_p out number,
								dt_fim_cih_p	out date,
								dt_fim_p	out date,
								nr_dia_utilizacao_p out number) is

begin

if (nr_sequencia_p is not null) then
	select	max(dt_inicio),
		max(qt_dias_solicitado),
		max(qt_dias_liberado),
		max(dt_fim_cih),
		max(nr_dia_util),
		max(dt_fim)
	into	dt_inicio_cih_p,
		qt_dias_solicitado_p,
		qt_dias_liberado_p,
		dt_fim_cih_p,
		nr_dia_utilizacao_p,
		dt_fim_p
	from	cpoe_material
	where	nr_sequencia = nr_sequencia_p;
end if;


end cpoe_dados_cih_anterior;
/