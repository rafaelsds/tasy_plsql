create or replace
procedure proj_alterar_data_etapa(	nr_seq_etapa_p		number,
					dt_anterior_p		date,
					dt_atual_p		date,
					nm_usuario_p		Varchar2) is 

qt_diferenca_w		number(15,10);
nr_seq_cronograma_w	number(10);
nr_seq_etapa_w		number(10);
nr_seq_superior_w	number(10);
nr_seq_etapa_atual_w	number(10);
qt_reg_fim_prev_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	proj_cron_etapa
	where	nr_seq_cronograma	= nr_seq_cronograma_w
	and	nr_sequencia		>= nr_seq_etapa_p
	and	nvl(ie_situacao,'A') 	= 'A'
	order by nr_sequencia;
	
Cursor C02 is
	select	nr_sequencia
	from	proj_cron_etapa
	where	nr_seq_superior		= nr_seq_etapa_atual_w
	and	nvl(ie_situacao,'A') 	= 'A'
	order by nr_sequencia;	

begin

select	(dt_atual_p - dt_anterior_p)
into	qt_diferenca_w
from	dual;

select	nr_seq_cronograma
into	nr_seq_cronograma_w
from	proj_cron_etapa
where	nr_sequencia	= nr_seq_etapa_p;

open C01;
loop
fetch C01 into	
	nr_seq_etapa_w;
exit when C01%notfound;
	begin
	
	update	proj_cron_etapa
	set	dt_inicio_prev = (dt_inicio_prev + qt_diferenca_w)
	where	nr_sequencia = nr_seq_etapa_w;
	
	select	count(*)
	into	qt_reg_fim_prev_w
	from	proj_cron_etapa
	where	nr_sequencia	= nr_seq_etapa_w
	and	dt_fim_prev	is not null;
	
	if (qt_reg_fim_prev_w	> 0) then
		update	proj_cron_etapa
		set	dt_fim_prev = (dt_fim_prev + qt_diferenca_w)
		where	nr_sequencia = nr_seq_etapa_w;
	else
		update	proj_cron_etapa
		set	dt_fim_prev = dt_inicio_prev + (nvl(qt_hora_prev,0) / 24)
		where	nr_sequencia = nr_seq_etapa_w;
	end if;
	
	end;
end loop;
close C01;

select	nvl(max(nr_seq_superior),0)
into	nr_seq_etapa_atual_w
from	proj_cron_etapa
where	nr_sequencia	= nr_seq_etapa_p;

WHILE	(nr_seq_etapa_atual_w <> 0) LOOP
	begin
		
	update	proj_cron_etapa
	set	dt_inicio_prev = (dt_inicio_prev + qt_diferenca_w)
	where	nr_sequencia = nr_seq_etapa_atual_w;
	
	select	count(*)
	into	qt_reg_fim_prev_w
	from	proj_cron_etapa
	where	nr_sequencia	= nr_seq_etapa_atual_w
	and	dt_fim_prev	is not null;
	
	if (qt_reg_fim_prev_w	> 0) then
		update	proj_cron_etapa
		set	dt_fim_prev = (dt_fim_prev + qt_diferenca_w)
		where	nr_sequencia = nr_seq_etapa_atual_w;
	else
		update	proj_cron_etapa
		set	dt_fim_prev = dt_inicio_prev + (nvl(qt_hora_prev,0) / 24)
		where	nr_sequencia = nr_seq_etapa_atual_w;
	end if;

	select	nvl(max(nr_seq_superior),0)
	into	nr_seq_etapa_atual_w
	from	proj_cron_etapa
	where	nr_sequencia	= nr_seq_etapa_atual_w;
		
	end;
END loop;

commit;

end proj_alterar_data_etapa;
/