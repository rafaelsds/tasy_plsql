create or replace procedure CONCLUIR_ATENDIMENTOS_UL (nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Concluir atendimentos Call Center com o statis 'Em atendimento'
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

Cursor C01 is
	select	nr_sequencia
	from	pls_atendimento
	where	ie_status	= 'A'
	and	dt_inicio	< sysdate - 1;

Cursor C02 (nr_seq_atendendimento_pc	number) is
	select	nr_sequencia
	from	pls_atendimento_evento
	where	nr_seq_atendimento = nr_seq_atendendimento_pc;


nr_seq_tipo_historico_w		pls_tipo_historico_atend.nr_sequencia%type;
ie_tipo_pessoa_w		pls_atendimento.ie_tipo_pessoa%type;
	
begin

	select	max(nr_sequencia)
	into	nr_seq_tipo_historico_w
	from	pls_tipo_historico_atend
	where	ie_gerado_sistema  = 'S'
	and	ie_situacao    = 'A';

for r_C01_w in C01 loop
	begin
		begin
			select	ie_tipo_pessoa
			into	ie_tipo_pessoa_w
			from	pls_atendimento
			where	nr_sequencia = r_C01_w.nr_sequencia;
		
		exception
		when others then
			ie_tipo_pessoa_w	:= null;
		end;
		
		if	(ie_tipo_pessoa_w is null) then
			update	pls_atendimento	
			set	ie_tipo_pessoa	= 'N'
			where	nr_sequencia	= r_C01_w.nr_sequencia;
		end if;
		
		for r_C02_w in C02 (r_C01_w.nr_sequencia) loop
			begin
				update	pls_atendimento_evento
				set	ie_status	= 'C',
					dt_fim_evento	= dt_inicio,
					dt_conclusao	= dt_inicio
				where	nr_sequencia	= r_C02_w.nr_sequencia;
			end;
		end loop;

		update	pls_atendimento
		set	ie_status		= 'C',
			dt_fim_atendimento	= dt_inicio,
			dt_conclusao		= dt_inicio,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= r_C01_w.nr_sequencia;
		
		insert	into	pls_atendimento_historico
				(	nr_sequencia, nr_seq_atendimento, ds_historico_long,
					dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_tipo_historico, dt_historico,
					ie_gerado_sistema)	
			values	(	pls_atendimento_historico_seq.nextval, r_C01_w.nr_sequencia,	
					'O atendimento foi finalizado atrav�s do script solicitado na OS 1513379/98451, pelo usu�rio' ||
					nm_usuario_p || ', na data/hora '|| to_char(sysdate, 'MM-DD-YYYY HH24:MI:SS') ||'.',
					sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_tipo_historico_w, sysdate,
					'S');
	end;
end loop;

commit;

end CONCLUIR_ATENDIMENTOS_UL;
/