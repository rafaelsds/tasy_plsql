create or replace 
procedure add_info_medico_externo(
	nm_medico_externo_p			atendimento_medico_externo.nm_medico_externo%type,
	crm_medico_externo_p 		atendimento_medico_externo.crm_medico_externo%type,
	uf_medico_externo_p			atendimento_medico_externo.uf_medico_externo%type,
	ie_tipo_atendimento_p		varchar2,
	nr_atendimento_p			atendimento_medico_externo.nr_atendimento%type,
	nr_prescricao_p				atendimento_medico_externo.nr_prescricao%type,
	nr_seq_prescr_medic_ad_p	atendimento_medico_externo.nr_seq_prescr_medic_ad%type,
	nr_seq_prescr_proced_ad_p	atendimento_medico_externo.nr_seq_prescr_proced_ad%type
	) as
	
	qt_registro_w	number;

begin
	if ie_tipo_atendimento_p = 'A' then
		select count(nr_atendimento)
		into qt_registro_w
		from atendimento_medico_externo
		where nr_atendimento = nr_atendimento_p
		and nr_prescricao is null
		and nr_seq_prescr_medic_ad is null 
		and nr_seq_prescr_proced_ad is null;
		
		if qt_registro_w > 0 then
			update 	atendimento_medico_externo
			set 	dt_atualizacao = sysdate,
					nm_usuario = obter_usuario_ativo(),
					nm_medico_externo = nm_medico_externo_p,
					crm_medico_externo = crm_medico_externo_p,
					uf_medico_externo = uf_medico_externo_p
			where 	nr_atendimento = nr_atendimento_p
			and 	nr_prescricao is null
			and 	nr_seq_prescr_medic_ad is null 
			and 	nr_seq_prescr_proced_ad is null;
			
		else
			insert into atendimento_medico_externo(
				dt_atualizacao,         
				nm_usuario,      
				dt_atualizacao_nrec,
				nm_usuario_nrec,        
				crm_medico_externo,     
				nm_medico_externo,      
				uf_medico_externo,
				nr_atendimento,     
				nr_prescricao,    
				nr_seq_prescr_medic_ad,
				nr_seq_prescr_proced_ad
			) values (
				sysdate,
				obter_usuario_ativo(),
				sysdate,
				obter_usuario_ativo(),
				crm_medico_externo_p,
				nm_medico_externo_p,
				uf_medico_externo_p,
				nr_atendimento_p,
				nr_prescricao_p,
				nr_seq_prescr_medic_ad_p,
				nr_seq_prescr_proced_ad_p
			);
		end if;
		
	elsif ie_tipo_atendimento_p = 'P' then
		select count(nr_atendimento)
		into qt_registro_w
		from atendimento_medico_externo
		where nr_atendimento = nr_atendimento_p
		and nr_prescricao = nr_prescricao_p
		and nr_seq_prescr_medic_ad is null 
		and nr_seq_prescr_proced_ad is null;
		
		if qt_registro_w > 0 then
			update 	atendimento_medico_externo
			set 	dt_atualizacao = sysdate,
					nm_usuario = obter_usuario_ativo(),
					nm_medico_externo = nm_medico_externo_p,
					crm_medico_externo = crm_medico_externo_p,
					uf_medico_externo = uf_medico_externo_p
			where 	nr_atendimento = nr_atendimento_p
			and 	nr_prescricao = nr_prescricao_p
			and 	nr_seq_prescr_medic_ad is null 
			and 	nr_seq_prescr_proced_ad is null;
			
		else
			insert into atendimento_medico_externo(
				dt_atualizacao,         
				nm_usuario,      
				dt_atualizacao_nrec,
				nm_usuario_nrec,        
				crm_medico_externo,     
				nm_medico_externo,      
				uf_medico_externo,
				nr_atendimento,     
				nr_prescricao,    
				nr_seq_prescr_medic_ad,
				nr_seq_prescr_proced_ad
			) values (
				sysdate,
				obter_usuario_ativo(),
				sysdate,
				obter_usuario_ativo(),
				crm_medico_externo_p,
				nm_medico_externo_p,
				uf_medico_externo_p,
				nr_atendimento_p,
				nr_prescricao_p,
				nr_seq_prescr_medic_ad_p,
				nr_seq_prescr_proced_ad_p
			);
		end if;
		
	elsif ie_tipo_atendimento_p = 'PA' then
		select count(nr_atendimento)
		into qt_registro_w
		from atendimento_medico_externo
		where nr_atendimento = nr_atendimento_p
		and nr_prescricao = nr_prescricao_p
		and nr_seq_prescr_medic_ad is null 
		and nr_seq_prescr_proced_ad = nr_seq_prescr_proced_ad_p;
		
		if qt_registro_w > 0 then
			update 	atendimento_medico_externo
			set 	dt_atualizacao = sysdate,
					nm_usuario = obter_usuario_ativo(),
					nm_medico_externo = nm_medico_externo_p,
					crm_medico_externo = crm_medico_externo_p,
					uf_medico_externo = uf_medico_externo_p
			where 	nr_atendimento = nr_atendimento_p
			and 	nr_prescricao = nr_prescricao_p
			and 	nr_seq_prescr_medic_ad is null 
			and 	nr_seq_prescr_proced_ad = nr_seq_prescr_proced_ad_p;
			
		else
			insert into atendimento_medico_externo(
				dt_atualizacao,         
				nm_usuario,      
				dt_atualizacao_nrec,
				nm_usuario_nrec,        
				crm_medico_externo,     
				nm_medico_externo,      
				uf_medico_externo,
				nr_atendimento,     
				nr_prescricao,    
				nr_seq_prescr_medic_ad,
				nr_seq_prescr_proced_ad
			) values (
				sysdate,
				obter_usuario_ativo(),
				sysdate,
				obter_usuario_ativo(),
				crm_medico_externo_p,
				nm_medico_externo_p,
				uf_medico_externo_p,
				nr_atendimento_p,
				nr_prescricao_p,
				nr_seq_prescr_medic_ad_p,
				nr_seq_prescr_proced_ad_p
			);
		end if;
		
	elsif ie_tipo_atendimento_p = 'MA' then
		select count(nr_atendimento)
		into qt_registro_w
		from atendimento_medico_externo
		where nr_atendimento = nr_atendimento_p
		and nr_prescricao = nr_prescricao_p
		and nr_seq_prescr_medic_ad = nr_seq_prescr_medic_ad_p
		and nr_seq_prescr_proced_ad is null;
		
		if qt_registro_w > 0 then
			update 	atendimento_medico_externo
			set 	dt_atualizacao = sysdate,
					nm_usuario = obter_usuario_ativo(),
					nm_medico_externo = nm_medico_externo_p,
					crm_medico_externo = crm_medico_externo_p,
					uf_medico_externo = uf_medico_externo_p
			where 	nr_atendimento = nr_atendimento_p
			and 	nr_prescricao = nr_prescricao_p
			and 	nr_seq_prescr_medic_ad = nr_seq_prescr_medic_ad_p
			and 	nr_seq_prescr_proced_ad is null;
			
		else
			insert into atendimento_medico_externo(
				dt_atualizacao,         
				nm_usuario,      
				dt_atualizacao_nrec,
				nm_usuario_nrec,        
				crm_medico_externo,     
				nm_medico_externo,      
				uf_medico_externo,
				nr_atendimento,     
				nr_prescricao,    
				nr_seq_prescr_medic_ad,
				nr_seq_prescr_proced_ad
			) values (
				sysdate,
				obter_usuario_ativo(),
				sysdate,
				obter_usuario_ativo(),
				crm_medico_externo_p,
				nm_medico_externo_p,
				uf_medico_externo_p,
				nr_atendimento_p,
				nr_prescricao_p,
				nr_seq_prescr_medic_ad_p,
				nr_seq_prescr_proced_ad_p
			);
		end if;
		
	end if;

commit;

end add_info_medico_externo;
/
