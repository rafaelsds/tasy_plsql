create or replace
procedure grava_consiste_disp_mercado(	cd_funcao_p		number,
					cd_material_p		number,
					nm_usuario_p		Varchar2,
					ie_acao_p		varchar2) is

begin

insert into w_consist_disp_mercado(
			cd_funcao,
			cd_material,
			nm_usuario,
			ie_acao_regra)
		values(	cd_funcao_p,
			cd_material_p,
			nm_usuario_p,
			ie_acao_p);

commit;

end grava_consiste_disp_mercado;
/