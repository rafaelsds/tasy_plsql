create or replace
procedure RXT_Gerar_Insercoes_Trat_Prot(nr_sequencia_p		Number,
					cd_estabelecimento_p	Number,
					ie_opcao_p		Varchar2,
					nm_usuario_p		Varchar2) is 

/*
P - Protocolo
T - Tratamento
*/
					
nr_seq_aplicador_w	Number(10);
nr_insercao_w		Number(5);
nr_seq_campo_w		Number(10);
qt_reg_w			number(10);
				
Cursor C01 is
	select	nr_seq_campo
	from	rxt_braq_campo_aplicador
	where	nr_seq_aplicador = nr_seq_aplicador_w;
	
begin
if (nr_sequencia_p is not null) then
	
	if (ie_opcao_p = 'P') then
	
		select	max(nr_seq_aplicador),
			nvl(max(nr_insercao),0)
		into	nr_seq_aplicador_w,
			nr_insercao_w
		from	rxt_braq_aplic_prot
		where	nr_sequencia = nr_sequencia_p;

		delete
		from	rxt_braq_campo_aplic_prot
		where	nr_seq_aplic_prot = nr_sequencia_p
		and		nr_insercao > nr_insercao_w;
		
		while (nr_insercao_w > 0) loop 
			begin
		
			open C01;
			loop
			fetch C01 into	
				nr_seq_campo_w;
			exit when C01%notfound;
				begin

				select	count(1)
				into	qt_reg_w
				from	rxt_braq_campo_aplic_prot
				where	nr_seq_aplic_prot = nr_sequencia_p
				and		nr_seq_campo = nr_seq_campo_w
				and		nr_insercao = nr_insercao_w;
				
				if	(qt_reg_w = 0) then
					insert into rxt_braq_campo_aplic_prot(	nr_sequencia,
										cd_estabelecimento,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_aplic_prot,
										nr_insercao,
										nr_seq_campo,
										ie_situacao)
									values(	rxt_braq_campo_aplic_prot_seq.nextval,
										cd_estabelecimento_p,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_sequencia_p,
										nr_insercao_w,
										nr_seq_campo_w,
										'A');
				end if;
				
				end;
			end loop;
			close C01;
		
			nr_insercao_w := nr_insercao_w - 1;
			end;
		end loop;
	
	elsif (ie_opcao_p = 'T') then
		
		select	max(nr_seq_aplicador),
			nvl(max(nr_insercao),0)
		into	nr_seq_aplicador_w,
			nr_insercao_w
		from	rxt_braq_aplic_trat
		where	nr_sequencia = nr_sequencia_p;

		while (nr_insercao_w > 0) loop 
			begin
		
			open C01;
			loop
			fetch C01 into	
				nr_seq_campo_w;
			exit when C01%notfound;
				begin
				
				select	count(1)
				into	qt_reg_w
				from	rxt_braq_campo_aplic_trat
				where	nr_seq_aplic_trat = nr_sequencia_p
				and		nr_seq_campo = nr_seq_campo_w
				and		nr_insercao = nr_insercao_w;
				
				if	(qt_reg_w = 0) then
					insert into rxt_braq_campo_aplic_trat(	nr_sequencia,
										cd_estabelecimento,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_aplic_trat,
										nr_insercao,
										nr_seq_campo,
										ie_situacao)
									values(	rxt_braq_campo_aplic_trat_seq.nextval,
										cd_estabelecimento_p,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_sequencia_p,
										nr_insercao_w,
										nr_seq_campo_w,
										'A');
				end if;
					
				end;
			end loop;
			close C01;
		
			nr_insercao_w := nr_insercao_w - 1;
			end;
		end loop;		
	end if;
end if;

commit;

end RXT_Gerar_Insercoes_Trat_Prot;
/