create or replace
procedure consiste_plano_mat_proc
			(cd_estabelecimento_p		in number,
			cd_convenio_p			in number,
			cd_categoria_p			in varchar2,
			cd_plano_p			in varchar2,
			cd_material_p			in number,
			cd_procedimento_p			in number,
			ie_origem_proced_p		in number,
			nr_atendimento_p			in number,
			ie_tipo_atendimento_p		in number,
			cd_tipo_acomodacao_p		in number,
			cd_setor_atendimento_p		in number,
			nr_seq_exame_p			in number,
			nr_seq_proc_interno_p		in number,
			ds_retorno_p			out varchar2,
			ie_bloqueia_agenda_p		out varchar2,
			ie_regra_p			out varchar2,
			nr_seq_regra_p			out number) is 

ie_regra_w		number(02,0)	:= 0;
ie_valor_w		varchar2(5);
vl_material_min_w		number(15,4);
vl_material_max_w		number(15,4);
nr_seq_regra_w		number(10);
cd_grupo_w		number(15)	:= 0;
cd_especialidade_w	number(15)	:= 0;
cd_area_w		number(15)	:= 0;
cd_grupo_material_w	number(08,0)	:= 0;
cd_subgrupo_material_w	number(08,0)	:= 0;
cd_classe_material_w	number(08,0)	:= 0;
cd_classif_setor_w		varchar2(10);
nr_seq_forma_org_w	Number(10)	:= 0;
nr_seq_grupo_w		Number(10)	:= 0;
nr_seq_subgrupo_w	Number(10)	:= 0;
ds_erro_w		varchar2(255)	:= null;
ds_retorno_w		varchar2(255)	:= null;
cd_pessoa_fisica_w	varchar2(10);
ie_glosa_w		regra_ajuste_proc.ie_glosa%type;
nr_seq_regra_preco_w	regra_ajuste_proc.nr_sequencia%type;	
--alterada por dsantos e Francisco em 01/10/2009 - OS170119. Esta procedure era totalmente diferente. Verifiacar históricos anteriores.

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

if	(cd_procedimento_p is not null) then

	consiste_plano_convenio(
			nr_atendimento_p,
                  	     	cd_convenio_p,
                        		cd_procedimento_p,
			ie_origem_proced_p,
	      	           	sysdate,
                        		1,
			ie_tipo_atendimento_p,
			cd_plano_p,
			null,
			ds_erro_w,
			cd_setor_atendimento_p,
			nr_seq_exame_p,
			ie_regra_w,
			null,
			nr_seq_regra_w,
			nr_seq_proc_interno_p,
			cd_categoria_p,
			cd_estabelecimento_p,
			null,
			null,
			cd_pessoa_fisica_w,
			ie_glosa_w,
			nr_seq_regra_preco_w);

elsif	(cd_material_p is not null) then

	 consiste_mat_plano_convenio
			(cd_convenio_p,
			cd_plano_p,
			cd_material_p,
			nr_atendimento_p,
			cd_setor_atendimento_p,
			ds_retorno_w,
			ie_bloqueia_agenda_p,
			ie_regra_w,
			nr_seq_regra_w,
			1,
			sysdate,
			null,
			cd_estabelecimento_p,
			cd_categoria_p,
			ie_tipo_atendimento_p,
			cd_tipo_acomodacao_p);

end if;


ie_regra_p	:= ie_regra_w;
nr_seq_regra_p	:= nr_seq_regra_w;

end	consiste_plano_mat_proc;
/
