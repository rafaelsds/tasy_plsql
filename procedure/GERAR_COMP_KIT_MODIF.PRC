create or replace
procedure Gerar_comp_kit_modif(	nr_prescricao_p		number,
				nr_seq_recomendacao_p	number default null,
				cd_recomendacao_p	number,
				cd_kit_p		number,
				nm_usuario_p		Varchar2) is 

ie_gera_w		varchar2(1);
nr_seq_recomendacao_w	number(10);
				
begin

select	nvl(max('N'),'S')
into	ie_gera_w
from	w_kit_modificado
where	nr_prescricao = nr_prescricao_p
and	((nr_seq_recomendacao = nr_seq_recomendacao_p) or (nvl(nr_seq_recomendacao,0) = 0))
and	cd_recomendacao = cd_recomendacao_p
and	cd_kit = cd_kit_p
and	nm_usuario = nm_usuario_p;

if	(ie_gera_w = 'S') then
	insert into w_kit_modificado	(	nr_sequencia,
						cd_kit,
						nr_prescricao,
						nr_seq_recomendacao,
						cd_recomendacao,
						cd_material,
						qt_material,
						ie_excluir,
						dt_atualizacao,
						nm_usuario)
					(select w_kit_modificado_seq.nextval,
						cd_kit_material,
						nr_prescricao_p,
						nr_seq_recomendacao_p,
						cd_recomendacao_p,
						cd_material,
						qt_material,
						'N',
						sysdate,
						nm_usuario_p
					from   componente_kit
					where  cd_kit_material = cd_kit_p);
else
	select	max(nr_seq_recomendacao_w)
	into	nr_seq_recomendacao_w
	from	w_kit_modificado
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_recomendacao = nr_seq_recomendacao_p
	and	cd_recomendacao = cd_recomendacao_p
	and	cd_kit = cd_kit_p
	and	nm_usuario = nm_usuario_p;
	
	if	(nr_seq_recomendacao_w is null) then
		update	w_kit_modificado
		set	nr_seq_recomendacao = nr_seq_recomendacao_p
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_recomendacao is null
		and	cd_recomendacao = cd_recomendacao_p
		and	cd_kit = cd_kit_p
		and	nm_usuario = nm_usuario_p;
			
		commit;
	end if;
end if;

commit;

end Gerar_comp_kit_modif;
/