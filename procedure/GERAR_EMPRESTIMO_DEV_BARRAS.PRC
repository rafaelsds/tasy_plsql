create or replace
procedure gerar_emprestimo_dev_barras(
				nr_emprestimo_p		Number,
				cd_material_p		Number,
				qt_emprestimo_p		Number,
				nr_seq_lote_p		Number,
				nm_usuario_p		Varchar2) as

nr_sequencia_w			Number(5);
nr_seq_dev_w			Number(5);
dt_validade_w			Date;
ds_lote_fornec_w		Varchar2(20);
cd_estabelecimento_w		Number(5);
cd_local_estoque_w		Number(4);
ie_erro_w			Varchar2(255);
ie_estoque_lote_w		varchar2(1);
qt_material_dev_w		number(13,4);
ie_gerar_nf_dev_emp_barras_w 	varchar2(1);
qt_material_w			number(13,4);
qt_emprestimo_w			number(13,4);
qt_devolucao_w			number(13,4);

cursor c01 is
	select	nr_sequencia,
		qt_material
	from	emprestimo_material
	where	nr_emprestimo	= nr_emprestimo_p
	and	cd_material	= cd_material_p
	and	nvl(nr_seq_lote, nvl(nr_seq_lote_p,0)) = nvl(nr_seq_lote_p,0)
	union all
	select	nr_sequencia,
		qt_material
	from	emprestimo_material
	where	nr_emprestimo	= nr_emprestimo_p
	and	obter_controlador_estoque(cd_material) = obter_controlador_estoque(cd_material_p);

BEGIN
/* Par�metro para verificar se gera a nota fiscal ap�s a leitura por barras na devolu��o do empr�stimo */
obter_param_usuario(143,316,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_gerar_nf_dev_emp_barras_w);

select	cd_estabelecimento,
	cd_local_estoque
into	cd_estabelecimento_w,
	cd_local_estoque_w
from	emprestimo
where	nr_emprestimo	= nr_emprestimo_p;

if	(nr_seq_lote_p > 0) then
	select	dt_validade,
		ds_lote_fornec
	into	dt_validade_w,
		ds_lote_fornec_w
	from	material_lote_fornec
	where	nr_sequencia = nr_seq_lote_p;
end if;

qt_emprestimo_w := qt_emprestimo_p;

open c01;
loop
fetch c01 into	
	nr_sequencia_w,
	qt_material_w;
exit when c01%notfound;
	begin
	if	(qt_emprestimo_w > 0) and
		(qt_material_w > 0) then
		if	(qt_emprestimo_w > qt_material_w) then
			qt_emprestimo_w := qt_emprestimo_w - qt_material_w;
			qt_devolucao_w := qt_material_w;
		else
			qt_devolucao_w := qt_emprestimo_w;
			qt_emprestimo_w := 0;
		end if;
		
		select	nvl(max(nr_seq_dev), 0) + 1
		into	nr_seq_dev_w
		from	EMPRESTIMO_MATERIAL_DEV
		where	nr_emprestimo	= nr_emprestimo_p
		and	NR_SEQUENCIA	= nr_sequencia_w;

		insert into emprestimo_material_dev(
			nr_emprestimo,
			nr_sequencia,
			nr_seq_dev,
			cd_material,
			qt_material,
			dt_atualizacao,
			nm_usuario,
			ds_lote_fornec,
			dt_validade,
			nr_seq_lote)
		values(	nr_emprestimo_p,
			nr_sequencia_w,
			nr_seq_dev_w,
			cd_material_p,
			qt_devolucao_w,
			sysdate,
			nm_usuario_p,
			ds_lote_fornec_w,
			dt_validade_w,
			nr_seq_lote_p);

		select	nvl(sum(qt_material),0)
		into	qt_material_dev_w
		from	emprestimo_material_dev
		where	nr_emprestimo	= nr_emprestimo_p	
		and	nr_sequencia	= nr_sequencia_w;
			
		update	emprestimo_material
		set	qt_material	= qt_emprestimo - qt_material_dev_w
		where	nr_emprestimo	= nr_emprestimo_p	
		and	nr_sequencia	= nr_sequencia_w;
		
		select	nvl(sum(qt_material),0)
		into	qt_material_dev_w
		from	emprestimo_material
		where	nr_emprestimo = nr_emprestimo_p;
		
		if	(qt_material_dev_w = 0) then
			update	emprestimo
			set	ie_situacao = 'B'
			where nr_emprestimo = nr_emprestimo_p;
		end if;
	end if;
	end;
end loop;
close c01;

ie_estoque_lote_w := substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_p),1,1);

if	(nvl(ie_gerar_nf_dev_emp_barras_w,'N') = 'N') then

	if	(ie_estoque_lote_w = 'S') then
		atualizar_saldo_lote(
			cd_estabelecimento_w,
			cd_local_estoque_w,
			cd_material_p,
			trunc(sysdate,'mm'),
			nr_seq_lote_p,
			0,
			qt_emprestimo_p,
			2,
			nm_usuario_p,
			ie_erro_w);
end if;

end if;

commit;

END gerar_emprestimo_dev_barras;
/