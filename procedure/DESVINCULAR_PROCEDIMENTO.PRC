create or replace
procedure desvincular_procedimento(	nr_sequencia_p	number,
				nm_usuario_p	varchar2) is
		
begin

if	(nr_sequencia_p is not null) then
	atualizar_desv_Proced_Laudo(nr_sequencia_p,nm_usuario_p);
	atualizar_status_exec(nr_sequencia_p,nm_usuario_p);
end if;

commit;	

end desvincular_procedimento;
/