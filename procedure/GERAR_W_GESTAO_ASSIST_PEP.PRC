create or replace procedure Gerar_W_Gestao_Assist_PEP(	
					dt_inicial_p                 DATE,
    dt_final_p                   DATE,
    nr_atendimento_p             NUMBER,
    cd_pessoa_fisica_p           VARCHAR2,
    ie_somente_internados_p      VARCHAR2,
    ie_tev_p                     VARCHAR2,
    ie_risco_tev_p               VARCHAR2,
    ie_cirurgico_p               VARCHAR2,
    cd_setor_atendimento_p       NUMBER,
    cd_doenca_p                  VARCHAR2,
    palavra_chave_p              VARCHAR2,
    ie_anamnese_p                VARCHAR2,
    ie_auditoria_p               VARCHAR2,
    ie_consentimento_p           VARCHAR2,
    ie_justificativas_p          VARCHAR2,
    ie_atestado_p                VARCHAR2,
    ie_boletim_p                 VARCHAR2,
    ie_evolucoes_p               VARCHAR2,
    ie_receita_p                 VARCHAR2,
    ie_orientacao_g_p            VARCHAR2,
    ie_parecer_p                 VARCHAR2,
    ie_orientacao_a_p            VARCHAR2,
    cd_material_p                NUMBER,
    cd_procedimento_p            NUMBER,
    ie_origem_proced_p           NUMBER,
    nr_seq_proc_interno_p        NUMBER,
    cd_recomendacao_p            NUMBER,
    ie_anamense_dia_p            VARCHAR2,
    ie_somente_anamnese_p        VARCHAR2,
    ie_evolucao_dia_p            VARCHAR2,
    ie_somente_evolucao_p        VARCHAR2,
    ie_resumo_p                  VARCHAR2,
    ie_somente_resumo_p          VARCHAR2,
    ie_somente_atb_prof_p        VARCHAR2,
    ie_alta_real_p               NUMBER,
    ie_prev_alta_p               NUMBER,
    ie_somente_prescr_tev_p      VARCHAR2,
    qt_carac_anamense_p          NUMBER,
    qt_carac_evolucao_p          NUMBER,
    qt_carac_resumo_p            NUMBER,
    cd_proc_cirurgias_p          VARCHAR2,
    nr_seq_dispositivos_p        VARCHAR2,
    nm_usuario_p                 VARCHAR2,
    ie_risco_alto_p              VARCHAR2 DEFAULT NULL,
    ie_risco_baixo_p             VARCHAR2 DEFAULT NULL,
    ie_risco_intermed_p          VARCHAR2 DEFAULT NULL,
    ie_risco_nao_aplica_p        VARCHAR2 DEFAULT NULL,
    qt_horas_ant_tev_p           NUMBER DEFAULT NULL,
    qt_horas_pos_tev_p           NUMBER DEFAULT NULL,
    nr_seq_meta_p                NUMBER,
    ie_tipo_meta_p               VARCHAR2 DEFAULT NULL,
    ie_diagnostico_mentor_p      VARCHAR2 DEFAULT NULL,
    ie_exames_mentor_p           VARCHAR2 DEFAULT NULL,
    ie_sinais_vitais_mentor_p    VARCHAR2 DEFAULT NULL,
    ie_escalas_indices_mentor_p  VARCHAR2 DEFAULT NULL,
    ie_curativos_mentor_p        VARCHAR2 DEFAULT NULL,
    ie_protocolos_assis_mentor_p VARCHAR2 DEFAULT NULL,
    ie_eventos_mentor_p          VARCHAR2 DEFAULT NULL,
    ie_classif_risco_mentor_p    VARCHAR2 DEFAULT NULL,
    cd_tipo_atendimento_p        NUMBER DEFAULT NULL,
    ie_nao_cirurgia_p            VARCHAR2 DEFAULT 'N',
    ie_diagnostico_p             VARCHAR2 DEFAULT NULL,
    ie_eventos_p                 VARCHAR2 DEFAULT NULL,
    ie_avaliacao_p               VARCHAR2 DEFAULT NULL,
    ie_usuario_p                 VARCHAR2 DEFAULT NULL,
	ie_resultado_exames_p		 VARCHAR2 DEFAULT NULL,
    lista_problemas_p			 VARCHAR2 DEFAULT NULL,
	tds_atends_paciente_p		 VARCHAR2 DEFAULT NULL)
IS
  nr_atendimento_w         NUMBER(10);
  nr_atend_registro_w      NUMBER(10);
  dt_entrada_w             DATE;
  dt_alta_w                DATE;
  cd_pessoa_fisica_w       VARCHAR2(10);
  cd_setor_Atendimento_w   NUMBER(10);
  qt_tev_w                 NUMBER(10);
  ie_inserir_w             BOOLEAN := true;
  qt_cirurgia_w            NUMBER(10);
  qt_dias_internacao_w     NUMBER(10) := 0;
  QT_DIAS_POS_OPERATORIO_w NUMBER(10) :=0;
  qt_encontrada_w          NUMBER(10) :=0;
  qt_diagnostico_w         NUMBER(10) :=0;
  qt_recomendacao_w        NUMBER(10) :=0;
  qt_anamnese_dia_w        NUMBER(10) :=0;
  qt_anamnese_caracter_w   NUMBER(10) :=0;
  qt_evolucao_dia_w        NUMBER(10) :=0;
  qt_evolucao_caracter_w   NUMBER(10) :=0;
  qt_resumo_pac_w          NUMBER(10) :=0;
  qt_resumo_pac_carac_w    NUMBER(10) :=0;
  qt_procedimento_w        NUMBER(10) :=0;
  qt_material_w            NUMBER(10) :=0;
  qt_pesquisados_w         NUMBER(10) :=0;
  nr_sequencia_w           NUMBER(10);
  ds_texto_w LONG;
  ds_texto_diag_w              VARCHAR2(255);
  ds_texto_longo_w             VARCHAR2(4000);
  nm_usuario_w                 VARCHAR2(15);
  dt_atualizacao_w             DATE;
  cd_medico_w                  VARCHAR2(10);
  dt_liberacao_w               DATE;
  dt_registro_w                DATE;
  ie_pesquisa_item_w           BOOLEAN;
  qt_registro_w                NUMBER(10);
  qt_prescr_atb_w              NUMBER(10) := 0;
  dt_previsto_alta_w           DATE;
  qt_prev_alta_w               NUMBER(10) := 0;
  qt_proc_tev_w                NUMBER(10) := 0;
  qt_mat_tev_w                 NUMBER(10) := 0;
  qt_recomend_tev_w            NUMBER(10) := 0;
  nr_prescricao_w              NUMBER(14);
  qt_tev_risco_alto_w          NUMBER(10) := 0;
  qt_tev_risco_baixo_w         NUMBER(10) := 0;
  qt_tev_risco_intermediario_w NUMBER(10) := 0;
  qt_tev_risco_nao_aplica_w    NUMBER(10) := 0;
  dt_avaliacao_tev_w           DATE;
  nr_seq_tev_min_w             NUMBER(10);
  nr_seq_dispositivos_w        VARCHAR2(32000);
  cd_proc_cirurgias_w          VARCHAR2(32000);
  qt_meta_atend_w              NUMBER(10) := 0;
  qt_tipo_pendencia_w          NUMBER(10) := 0;
  dt_final_w                   DATE;
  dt_inicial_w                 DATE;
  
  CURSOR C01
  IS
    SELECT a.cd_pessoa_fisica,
      a.nr_atendimento,
      b.cd_setor_atendimento,
      a.dt_entrada,
      a.dt_alta
    FROM atend_paciente_unidade b,
      atendimento_paciente a
    WHERE a.nr_Atendimento = b.nr_atendimento
    AND b.nr_seq_interno   = obter_atepacu_paciente(a.nr_atendimento, 'A')
      --and     a.dt_entrada between dt_inicial_w and dt_final_w
    AND ie_somente_internados_p = 'N'
    AND NVL(nr_atendimento_p,0) = 0
    AND cd_pessoa_fisica_p     IS NULL
  UNION ALL
  SELECT a.cd_pessoa_fisica,
    a.nr_atendimento,
    b.cd_setor_atendimento,
    a.dt_entrada,
    a.dt_alta
  FROM atend_paciente_unidade b,
    atendimento_paciente a
  WHERE a.nr_Atendimento      = b.nr_atendimento
  AND b.nr_seq_interno        = obter_atepacu_paciente(a.nr_atendimento, 'A')
  AND NVL(nr_atendimento_p,0) > 0
  AND a.nr_atendimento        = nr_atendimento_p
  UNION ALL
  SELECT a.cd_pessoa_fisica,
    a.nr_atendimento,
    b.cd_setor_atendimento,
    a.dt_entrada,
    a.dt_alta
  FROM atend_paciente_unidade b,
    atendimento_paciente a
  WHERE a.nr_Atendimento      = b.nr_atendimento
  AND b.nr_seq_interno        = obter_atepacu_paciente(a.nr_atendimento, 'A')
  AND cd_pessoa_fisica_p     IS NOT NULL
  AND NVL(nr_atendimento_p,0) = 0
  AND a.cd_pessoa_fisica      = cd_pessoa_fisica_p
  ORDER BY 1;
  
  CURSOR C02
  IS
    SELECT a.nr_sequencia,
      a.ds_anamnese,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_ananmese,
      a.nr_atendimento
    FROM anamnese_paciente a
    WHERE ((nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N')
    OR (a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N')
	OR (tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p)))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_ananmese BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'ANAMNESE_PACIENTE', 'DS_ANAMNESE', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C03
  IS
    SELECT a.nr_sequencia,
      a.ds_auditoria,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_atualizacao_nrec,
      a.nr_atendimento
    FROM atendimento_audit_medica a
    WHERE (nr_atendimento_w = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento     = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'ATENDIMENTO_AUDIT_MEDICA', 'DS_AUDITORIA', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C04
  IS
    SELECT a.nr_sequencia,
      a.ds_texto,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_profissional,
      a.dt_liberacao,
      a.dt_atualizacao_nrec,
      a.nr_atendimento
    FROM pep_pac_ci a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'PEP_PAC_CI', 'DS_TEXTO', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C05
  IS
    SELECT a.nr_sequencia,
      a.ds_justificativa,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_profissional,
      a.dt_liberacao,
      a.dt_atualizacao_nrec,
      a.nr_atendimento
    FROM paciente_justificativa a
    WHERE (nr_atendimento_w = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento     = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'PACIENTE_JUSTIFICATIVA', 'DS_JUSTIFICATIVA', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C06
  IS
    SELECT a.nr_sequencia,
      a.ds_atestado,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_atestado,
      a.nr_atendimento
    FROM atestado_paciente a
    WHERE (nr_atendimento_w = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento     = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (obter_se_long_contem_texto( 'ATESTADO_PACIENTE', 'DS_ATESTADO', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S')
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C07
  IS
    SELECT a.nr_sequencia,
      a.ds_boletim,
      a.nm_usuario,
      a.dt_atualizacao,
      '',
      a.dt_liberacao,
      a.dt_boletim,
      a.nr_atendimento
    FROM atendimento_boletim a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'ATENDIMENTO_BOLETIM', 'DS_BOLETIM', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C08
  IS
    SELECT a.nr_sequencia,
      a.ds_receita,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_receita,
      a.nr_atendimento
    FROM med_receita a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'MED_RECEITA', 'DS_RECEITA', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C09
  IS
    SELECT a.cd_evolucao,
      a.ds_evolucao,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_evolucao,
      a.nr_atendimento
    FROM evolucao_paciente a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'EVOLUCAO_PACIENTE', 'DS_EVOLUCAO', 'WHERE CD_EVOLUCAO = :CD_EVOLUCAO', 'CD_EVOLUCAO='
      ||a.cd_evolucao, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                  IS NULL
    OR ie_usuario_p                     = a.nm_usuario
    OR ie_usuario_p                     = a.nm_usuario_nrec);
    
  CURSOR C10
  IS
    SELECT a.nr_sequencia,
      a.ds_orientacao_geral,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_profissional,
      a.dt_liberacao,
      a.dt_registro,
      a.nr_atendimento
    FROM pep_orientacao_geral a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'PEP_ORIENTACAO_GERAL', 'DS_ORIENTACAO_GERAL', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C11
  IS
    SELECT a.nr_parecer,
      a.ds_motivo_consulta,
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_atualizacao,
      a.nr_atendimento
    FROM parecer_medico_req a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'PARECER_MEDICO_REQ', 'DS_MOTIVO_CONSULTA', 'WHERE NR_PARECER = :NR_PARECER', 'NR_PARECER='
      ||a.nr_parecer, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                 IS NULL
    OR ie_usuario_p                    = a.nm_usuario
    OR ie_usuario_p                    = a.nm_usuario_nrec);
    
  CURSOR C12
  IS
    SELECT a.nr_sequencia,
      a.ds_orientacao,
      a.nm_usuario,
      a.dt_atualizacao,
      '',
      a.dt_liberacao,
      a.dt_atualizacao,
      a.nr_atendimento
    FROM atendimento_alta a,
      parametro_medico p
    WHERE (nr_atendimento_w          = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento              = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A')       = 'A'
    AND p.cd_estabelecimento         = obter_estabelecimento_ativo
    AND ((a.ie_tipo_orientacao      <> 'P')
    OR (NVL(ie_liberar_desfecho,'N') = 'N')
    OR ((a.dt_liberacao             IS NOT NULL)
    AND (a.dt_inativacao            IS NULL)))
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR (obter_se_long_contem_texto( 'ATENDIMENTO_ALTA', 'DS_ORIENTACAO', 'WHERE NR_SEQUENCIA = :NR_SEQUENCIA', 'NR_SEQUENCIA='
      ||a.nr_sequencia, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  CURSOR C13
  IS
    SELECT a.nr_seq_interno,
      SUBSTR(obter_desc_cid(CD_DOENCA),1,200),
      a.nm_usuario,
      a.dt_atualizacao,
      cd_medico,
      a.dt_liberacao,
      a.dt_atualizacao,
      a.nr_atendimento
    FROM diagnostico_doenca a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p   = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p   = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR upper(obter_desc_cid(CD_DOENCA)) LIKE upper('%'
      ||palavra_chave_p
      ||'%'))
    AND (ie_usuario_p IS NULL
    OR ie_usuario_p    = a.nm_usuario);
    
  CURSOR C14
  IS
    SELECT a.nr_sequencia,
      ds_evento,
      a.nm_usuario,
      a.dt_atualizacao,
      '',
      a.dt_liberacao,
      a.dt_atualizacao,
      a.nr_atendimento
    FROM qua_evento_paciente a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p   = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p   = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR upper(ds_evento) LIKE upper('%'
      ||palavra_chave_p
      ||'%'))
    AND (ie_usuario_p IS NULL
    OR ie_usuario_p    = a.nm_usuario
    OR ie_usuario_p    = a.nm_usuario_nrec);
    
  CURSOR C15
  IS
    SELECT a.nr_sequencia,
      ds_resultado,
      ds_result_long,
      a.nm_usuario,
      a.dt_atualizacao,
      '',
      a.dt_liberacao,
      a.dt_atualizacao,
      a.nr_atendimento
    FROM med_avaliacao_paciente a,
      Med_Avaliacao_Result b
    WHERE a.nr_sequencia       = b.nr_seq_avaliacao
    AND (nr_atendimento_w      = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR upper(ds_resultado) LIKE upper('%'
      ||palavra_chave_p
      ||'%')
    OR (obter_se_long_contem_texto( 'MED_AVALIACAO_RESULT', 'DS_RESULT_LONG', 'WHERE NR_SEQ_AVALIACAO = :NR_SEQUENCIA AND NR_SEQ_ITEM = :NR_SEQ_ITEM ', 'NR_SEQUENCIA='
      ||b.NR_SEQ_AVALIACAO
      ||';NR_SEQ_ITEM='
      ||b.NR_SEQ_ITEM, palavra_chave_p) = 'S'))
    AND (ie_usuario_p                  IS NULL
    OR ie_usuario_p                     = a.nm_usuario
    OR ie_usuario_p                     = a.nm_usuario_nrec);

  CURSOR C16
  IS
    SELECT a.nr_seq_resultado,
      obter_desc_exame(b.nr_seq_exame),
      a.nm_usuario,
      a.dt_atualizacao,
      a.cd_medico,
      a.dt_liberacao,
      a.dt_atualizacao,
      a.nr_atendimento
    FROM EXAME_LAB_RESULTADO a,
    EXAME_LAB_RESULT_ITEM b
    WHERE a.nr_seq_resultado = b.NR_SEQ_RESULTADO
    AND (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))    
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL
    OR upper(obter_desc_exame(b.nr_seq_exame)) LIKE upper('%'
      ||palavra_chave_p
      ||'%'))
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                     = a.nm_usuario);

  CURSOR C17
  IS
    SELECT a.nr_sequencia,
      a.ds_problema,
      a.nm_usuario,
      a.dt_atualizacao,
      '',
      a.dt_liberacao,
      a.dt_atualizacao_nrec,
      a.nr_atendimento
    FROM lista_problema_pac a
    WHERE (nr_atendimento_w    = 0 and tds_atends_paciente_p = 'N'
    OR a.nr_atendimento        = nr_atendimento_w and tds_atends_paciente_p = 'N'
	OR tds_atends_paciente_p   = 'S' and a.nr_atendimento in (select b.nr_atendimento from atendimento_paciente b where cd_pessoa_fisica = cd_pessoa_fisica_p))
    AND NVL(a.ie_situacao,'A') = 'A'
    AND a.dt_liberacao BETWEEN dt_inicial_w AND dt_final_w
    AND (palavra_chave_p IS NULL    
	  OR upper(a.ds_problema) LIKE upper('%'
      ||palavra_chave_p
      ||'%')) 
    AND (ie_usuario_p                   IS NULL
    OR ie_usuario_p                      = a.nm_usuario
    OR ie_usuario_p                      = a.nm_usuario_nrec);
    
  PROCEDURE inserir_registro(
      nr_atend_reg_p NUMBER)
  IS
    qt_reg_w NUMBER(10);
  BEGIN
    SELECT MAX(dt_alta),
      MAX(dt_entrada)
    INTO dt_alta_w,
      dt_entrada_w
    FROM atendimento_paciente
    WHERE nr_atendimento = nr_atend_reg_p;
    SELECT COUNT(*)
    INTO qt_reg_w
    FROM w_gestao_assistencial
    WHERE nr_atendimento      = nr_atend_reg_p
    AND nm_usuario            = nm_usuario_p;
    qt_dias_internacao_w     := (NVL(dt_alta_w,sysdate) - dt_entrada_w) + 1;
    qt_dias_pos_operatorio_w := obter_dias_pos_operatorio(nr_atend_reg_p);
    IF (qt_reg_w              = 0) THEN
      INSERT
      INTO w_gestao_assistencial
        (
          nr_sequencia,
          dt_atualizacao,
          nm_usuario,
          dt_atualizacao_nrec,
          nm_usuario_nrec,
          nr_atendimento,
          cd_setor_Atendimento,
          qt_dias_internacao,
          qt_dias_pos_operatorio
        )
        VALUES
        (
          w_gestao_assistencial_seq.nextval,
          sysdate,
          nm_usuario_p,
          sysdate,
          nm_usuario_p,
          nr_atend_reg_p,
          cd_setor_Atendimento_w,
          qt_dias_internacao_w,
          qt_dias_pos_operatorio_w
        );
      COMMIT;
    END IF;
  END;
BEGIN
  ie_pesquisa_item_w := ((ie_anamnese_p = 'S') 
                        OR (ie_auditoria_p = 'S') 
                        OR (ie_consentimento_p = 'S') 
                        OR (ie_justificativas_p = 'S') 
                        OR (ie_atestado_p = 'S') 
                        OR (ie_boletim_p = 'S') 
                        OR (ie_evolucoes_p = 'S') 
                        OR (ie_receita_p = 'S') 
                        OR (ie_orientacao_g_p = 'S') 
                        OR (ie_parecer_p = 'S') 
                        OR (ie_orientacao_a_p = 'S') 
                        OR (ie_diagnostico_p = 'S') 
                        OR (ie_eventos_p = 'S')
                        OR (ie_avaliacao_p = 'S')
						OR (ie_resultado_exames_p = 'S')
						OR (lista_problemas_p = 'S'));
  DELETE FROM w_gestao_assistencial WHERE nm_usuario = nm_usuario_p;
  DELETE FROM w_pesq_itens_pront WHERE nm_usuario = nm_usuario_p;
  
  dt_inicial_w := inicio_dia(dt_inicial_p);
  dt_final_W   := fim_dia(dt_final_p);
 
  BEGIN
    ie_inserir_w     := true;
    nr_atendimento_w := NVL(nr_atendimento_p,0);
    IF (ie_pesquisa_item_w) THEN
      IF (ie_anamnese_p = 'S') THEN
        OPEN C02;
        LOOP
          FETCH C02
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C02%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(83,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C02;
      END IF;
      
      IF (ie_auditoria_p = 'S') THEN
        OPEN C03;
        LOOP
          FETCH C03
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C03%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(343,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C03;
      END IF;
      
      IF (ie_consentimento_p = 'S') THEN
        OPEN C04;
        LOOP
          FETCH C04
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C04%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(398,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C04;
      END IF;
      
      IF (ie_justificativas_p = 'S') THEN
        OPEN C05;
        LOOP
          FETCH C05
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C05%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(393,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C05;
      END IF;
      
      IF (ie_atestado_p = 'S') THEN
        OPEN C06;
        LOOP
          FETCH C06
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C06%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(74,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C06;
      END IF;
      
      IF (ie_boletim_p = 'S') THEN
        OPEN C07;
        LOOP
          FETCH C07
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C07%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(21,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C07;
      END IF;
      
      IF (ie_receita_p = 'S') THEN
        OPEN C08;
        LOOP
          FETCH C08
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C08%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(73,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C08;
      END IF;
      
      IF (ie_evolucoes_p = 'S') THEN
        OPEN C09;
        LOOP
          FETCH C09
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C09%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(5,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C09;
      END IF;
      
      IF (ie_orientacao_g_p = 'S') THEN
        OPEN C10;
        LOOP
          FETCH C10
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C10%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(416,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C10;
      END IF;
      
      IF (ie_parecer_p = 'S') THEN
        OPEN C11;
        LOOP
          FETCH C11
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C11%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(51,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C11;
      END IF;
      
      IF (ie_orientacao_a_p = 'S') THEN
        OPEN C12;
        LOOP
          FETCH C12
          INTO nr_sequencia_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C12%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(153,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C12;
      END IF;
      
      IF (ie_diagnostico_p = 'S') THEN
        OPEN C13;
        LOOP
          FETCH C13
          INTO nr_sequencia_w,
            ds_texto_diag_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C13%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(1,NULL,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w, ds_texto_diag_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C13;
      END IF;
      
      IF (ie_eventos_p = 'S') THEN -- Eventos
        OPEN C14;
        LOOP
          FETCH C14
          INTO nr_sequencia_w,
            ds_texto_longo_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C14%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            insere_pesq_itens_pront(273,NULL,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w, ds_texto_longo_w);
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C14;
      END IF;
      
      IF (ie_avaliacao_p = 'S') THEN
        OPEN C15;
        LOOP
          FETCH C15
          INTO nr_sequencia_w,
            ds_texto_longo_w,
            ds_texto_w,
            nm_usuario_w,
            dt_atualizacao_w,
            cd_medico_w,
            dt_liberacao_w,
            dt_registro_w,
            nr_atend_registro_w;
          EXIT
        WHEN C15%notfound;
          BEGIN
            SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
            INTO nr_atend_registro_w
            FROM dual;
            IF (trim(ds_texto_longo_w) IS NOT NULL) THEN
              insere_pesq_itens_pront(7,NULL,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w, ds_texto_longo_w);
            ELSE
              insere_pesq_itens_pront(7,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w, NULL);
            END IF;
            qt_pesquisados_w := qt_pesquisados_w + 1;
            inserir_registro(nr_atend_registro_w);
          END;
        END LOOP;
        CLOSE C15;
      END IF;
	  
	 IF (ie_resultado_exames_p = 'S') THEN
		OPEN C16;
		LOOP
		  FETCH C16
		  INTO nr_sequencia_w,
			ds_texto_w,
			nm_usuario_w,
			dt_atualizacao_w,
			cd_medico_w,
			dt_liberacao_w,
			dt_registro_w,
			nr_atend_registro_w;
		  EXIT
		WHEN C16%notfound;
		  BEGIN
			SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
			INTO nr_atend_registro_w
			FROM dual;
			insere_pesq_itens_pront(2,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
			qt_pesquisados_w := qt_pesquisados_w + 1;
			inserir_registro(nr_atend_registro_w);
		  END;
		END LOOP;
		CLOSE C16;
	  END IF;

	 IF (lista_problemas_p = 'S') THEN
		OPEN C17;
		LOOP
		  FETCH C17
		  INTO nr_sequencia_w,
			ds_texto_w,
			nm_usuario_w,
			dt_atualizacao_w,
			cd_medico_w,
			dt_liberacao_w,
			dt_registro_w,
			nr_atend_registro_w;
		  EXIT
		WHEN C17%notfound;
		  BEGIN
			SELECT DECODE(nr_atendimento_w,0,nr_atend_registro_w,nr_atendimento_w)
			INTO nr_atend_registro_w
			FROM dual;
			insere_pesq_itens_pront(1498,ds_texto_w,nm_usuario_p,dt_registro_w,cd_medico_w,dt_liberacao_w, nr_atend_registro_w);
			qt_pesquisados_w := qt_pesquisados_w + 1;
			inserir_registro(nr_atend_registro_w);
		  END;
		END LOOP;
		CLOSE C17;
	  END IF;

      ie_inserir_w    := (qt_pesquisados_w > 0);
      qt_pesquisados_w:=0;
      
      IF (NOT ie_inserir_w) THEN
        GOTO Fim;
      END IF;
    END IF;
    <<Fim>>
    ie_inserir_w := ie_inserir_w;
  END;
  
  COMMIT;

end Gerar_W_Gestao_Assist_PEP;
/
