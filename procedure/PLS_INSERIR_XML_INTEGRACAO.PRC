create or replace
procedure pls_inserir_xml_integracao(	ds_xml_p			pls_xml_integracao.ds_xml%type,
					ie_tipo_xml_p			pls_xml_integracao.ie_tipo_xml%type,
					nr_seq_cotacao_p		pls_xml_integracao.nr_seq_cotacao%type,
					nr_seq_lote_cad_p		pls_xml_integracao.nr_seq_lote_cad%type,
					nr_seq_xml_integracao_p	out	pls_xml_integracao.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir o XML das integracoes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [] Portal []  Relat�rios [] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: ie_tipo_xml_p : 	1 - Gest�o de Protocolo de Interc�mbio
					2 - Inpart
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin


insert into pls_xml_integracao 
	(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
	nm_usuario, nm_usuario_nrec, ds_xml,
	ie_tipo_xml, nr_seq_cotacao, nr_seq_lote_cad)
values (pls_xml_integracao_seq.nextval, sysdate, sysdate,
	'Tasy', 'Tasy', ds_xml_p,
	ie_tipo_xml_p, nr_seq_cotacao_p, nr_seq_lote_cad_p) returning nr_sequencia into nr_seq_xml_integracao_p;
commit;

end pls_inserir_xml_integracao;
/