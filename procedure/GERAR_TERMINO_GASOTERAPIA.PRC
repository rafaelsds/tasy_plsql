create or replace 
procedure gerar_termino_gasoterapia(	nm_usuario_p		varchar2,
				nr_atendimento_p	number,
				dt_alteracao_p		date,
				ie_consistir_dt_p varchar2 default 'N',
				ie_horario_invalido_p out varchar2) is

nr_sequencia_w		prescr_gasoterapia.nr_sequencia%type;
nr_prescricao_w		prescr_gasoterapia.nr_prescricao%type;
nr_seq_gas_cpoe_w	prescr_gasoterapia.nr_seq_gas_cpoe%type;
ds_mensagem_w		varchar2(255);
dt_evento_ref_w		date;
ie_horario_invalido_w	varchar2(2);

cursor c01 is
select	b.nr_sequencia,
	a.nr_prescricao,
	b.nr_seq_gas_cpoe
from	prescr_medica a,
	prescr_gasoterapia b
where	a.nr_prescricao = b.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	obter_se_valor_contido(obter_status_gasoterapia(b.nr_sequencia,'C'), 'P,T,S,N') = 'N';


begin
ie_horario_invalido_w := 'N';
ie_horario_invalido_p := 'N';

open C01;
loop
fetch C01 into
	nr_sequencia_w,
	nr_prescricao_w,
	nr_seq_gas_cpoe_w;
exit when C01%notfound;
	begin
	if (ie_consistir_dt_p = 'S') then
		select	max(dt_evento)
		into	dt_evento_ref_w
		from	prescr_gasoterapia_evento
		where	nr_seq_gasoterapia = nr_sequencia_w
		and     ie_evento_valido = 'S'
		and		ie_evento in ('I', 'V', 'R');
        
		if (dt_evento_ref_w is not null and dt_evento_ref_w >= dt_alteracao_p) then
			ie_horario_invalido_w := 'S';
			ie_horario_invalido_p := 'S';
		else 
			ie_horario_invalido_w := 'N';
		end if;
	end if;
    
	if((ie_consistir_dt_p = 'N' or ie_consistir_dt_p is null) or (ie_consistir_dt_p = 'S' and ie_horario_invalido_w = 'N')) then
		Gerar_Alteracao_Gasoterapia(nr_sequencia_w,'T',null,null,(nvl(dt_alteracao_p,sysdate) - 1/86400),nr_prescricao_w,null,null,nm_usuario_p, null,null,null,null,ds_mensagem_w,nr_seq_gas_cpoe_w);
	end if;
	end;
end loop;
close C01;

commit;
end gerar_termino_gasoterapia;
/
