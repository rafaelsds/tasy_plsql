create or replace
procedure baixa_emprestimo_mat_barras(
				nr_emprestimo_p		Number,
				nr_sequencia_p		Number,
				cd_material_p			Number,
				qt_material_p			Number,
				nr_seq_lote_p			Number,
				nm_usuario_p			Varchar2) is

ie_acao_w			Varchar2(1);
ie_erro_w			Varchar2(255);
nr_seq_dev_w			Number(5);
cd_estabelecimento_w		Number(5);
cd_local_estoque_w		Number(4);
qt_saldo_atual_w		Number(13,4);
ds_lote_fornec_w		Varchar2(20);
ds_local_estoque_w		Varchar2(40);
dt_validade_w			Date;
dt_mesano_referencia_w	Date;
cd_material_estoque_w		number(6);
nr_sequencia_w			number(10);
ie_estoque_lote_w 	material_estab.ie_estoque_lote%type;


begin
ie_acao_w		:= '2';
dt_mesano_referencia_w	:= trunc(sysdate,'mm');

select	cd_estabelecimento,
	cd_local_estoque,
	substr(obter_desc_local_estoque(cd_local_estoque),1,40)
into	cd_estabelecimento_w,
	cd_local_estoque_w,
	ds_local_estoque_w
from	emprestimo
where	nr_emprestimo	= nr_emprestimo_p;

select	ds_lote_fornec,
	dt_validade
into	ds_lote_fornec_w,
	dt_validade_w
from	material_lote_fornec
where	nr_sequencia = nr_seq_lote_p;

select	max(cd_material_estoque)
into	cd_material_estoque_w
from	material
where	cd_material = cd_material_p;

select	substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_p),1,1)
into	ie_estoque_lote_w
from	dual;
if	(ie_estoque_lote_w = 'S') then 
/*Consistir se existe saldo na devolucao do emprestimo de entrada. Lote pode ja ter sido consumido*/
	select	nvl(sum(qt_estoque),0)
	into	qt_saldo_atual_w
	from	saldo_estoque_lote
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	dt_mesano_referencia	= dt_mesano_referencia_w
	and	cd_local_estoque	= cd_local_estoque_w
	and	cd_material		= cd_material_estoque_w
	and	nr_seq_lote		= nr_seq_lote_p;
	if	(qt_material_p > qt_saldo_atual_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(204358,'DS_LOTE=' || ds_lote_fornec_w || ';QT_SALDO=' || qt_saldo_atual_w || ';DS_LOCAL=' || ds_local_estoque_w);
	end if;
end if;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	emprestimo_material
where	nr_emprestimo	= nr_emprestimo_p
and	cd_material	= cd_material_p;

if	(nr_sequencia_w > 0) then
	begin
	if	(ie_estoque_lote_w = 'S') then 
		atualizar_saldo_lote(
				cd_estabelecimento_w,
				cd_local_estoque_w,
				cd_material_p,
				dt_mesano_referencia_w,
				nr_seq_lote_p,
				0,
				qt_material_p,
				ie_acao_w,
				nm_usuario_p,
				ie_erro_w);
	end if;

	/*Faz o registro da baixa tambem vinculando com a sequencia do lote*/
	select	nvl(max(nr_seq_dev),0) + 1
	into	nr_seq_dev_w
	from	emprestimo_material_dev
	where	nr_emprestimo	= nr_emprestimo_p
	and	nr_sequencia	= nr_sequencia_p;
	insert into emprestimo_material_dev(
		nr_emprestimo,
		nr_sequencia,
		nr_seq_dev,
		cd_material,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		ds_lote_fornec,
		dt_validade,
		nr_seq_lote,
		ds_observacao)
	values(nr_emprestimo_p,
		nr_sequencia_p,
		nr_seq_dev_w,
		cd_material_p,
		qt_material_p,
		sysdate,
		nm_usuario_p,
		ds_lote_fornec_w,
		dt_validade_w,
		nr_seq_lote_p,
		wheb_mensagem_pck.get_texto(310228));

	/*Atualiza o saldo do emprestimo*/
	update	emprestimo_material
	set	qt_material	= (qt_material - qt_material_p)
	where	nr_emprestimo	= nr_emprestimo_p
	and	nr_sequencia	= nr_sequencia_p;
	end;
end if;

commit;

end baixa_emprestimo_mat_barras;
/
