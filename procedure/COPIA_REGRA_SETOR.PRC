create or replace 
procedure copia_regra_setor(	cd_setor_origem_p	number,
				cd_setor_destino_p	varchar2,
				ie_tipo_acao_p		varchar2,
				nm_usuario_p		varchar2,
				nm_tabela_p		varchar2) is


cd_Setor_destino_w	Setor_atendimento.cd_setor_atendimento%type;

/*variaveis tabela 'consiste_setor_proc' */
cd_area_proced_w	consiste_setor_proc.cd_area_proced%type;       
cd_cgc_w		consiste_setor_proc.cd_cgc%type;
cd_cgc_laboratorio_w	consiste_setor_proc.cd_cgc_laboratorio%type;
cd_convenio_w		consiste_setor_proc.cd_convenio%type;
cd_especial_proced_w	consiste_setor_proc.cd_especial_proced%type;
cd_estabelecimento_w	consiste_setor_proc.cd_estabelecimento%type;
cd_grupo_proced_w	consiste_setor_proc.cd_grupo_proced%type;
cd_medico_exec_filtro_w	consiste_setor_proc.cd_medico_exec_filtro%type;
cd_medico_executor_w	consiste_setor_proc.cd_medico_executor%type;
cd_pessoa_fisica_w	consiste_setor_proc.cd_pessoa_fisica%type;
cd_procedimento_w	consiste_setor_proc.cd_procedimento%type;
cd_setor_prescricao_w	consiste_setor_proc.cd_setor_prescricao%type;
cd_tipo_procedimento_w	consiste_setor_proc.cd_tipo_procedimento%type;
dt_atualizacao_nrec_w	consiste_setor_proc.dt_atualizacao_nrec%type;
dt_final_vigencia_w	consiste_setor_proc.dt_final_vigencia%type;
dt_inicio_vigencia_w	consiste_setor_proc.dt_inicio_vigencia%type;
hr_final_w		consiste_setor_proc.hr_final%type;
hr_inicial_w		consiste_setor_proc.hr_inicial%type;
ie_credenciado_w	consiste_setor_proc.ie_credenciado%type;
ie_medico_executor_w	consiste_setor_proc.ie_medico_executor%type;
ie_origem_inf_w		consiste_setor_proc.ie_origem_inf%type;
ie_origem_proced_w	consiste_setor_proc.ie_origem_proced%type;
ie_tipo_atendimento_w	consiste_setor_proc.ie_tipo_atendimento%type;
nm_usuario_nrec_w	consiste_setor_proc.nm_usuario_nrec%type;
nr_seq_classificacao_w	consiste_setor_proc.nr_seq_classificacao%type;
nr_seq_equipe_w		consiste_setor_proc.nr_seq_equipe%type;
nr_seq_exame_w		consiste_setor_proc.nr_seq_exame%type;
nr_seq_forma_org_w	consiste_setor_proc.nr_seq_forma_org%type;
nr_seq_grupo_w		consiste_setor_proc.nr_seq_grupo%type;
nr_seq_proc_interno_w	consiste_setor_proc.nr_seq_proc_interno%type;
nr_seq_subgrupo_w	consiste_setor_proc.nr_seq_subgrupo%type;
nr_sequencia_w		consiste_setor_proc.nr_sequencia%type;

/*variaveis tabela 'regra_executa_lib_prescr' */				
cd_area_procedimento_w		regra_executa_lib_prescr.cd_area_procedimento%type;
cd_especialidade_w              regra_executa_lib_prescr.cd_especialidade%type;
cd_grupo_proc_w                 regra_executa_lib_prescr.cd_grupo_proc%type;
cd_medico_exec_w                regra_executa_lib_prescr.cd_medico_exec%type;
cd_perfil_w                     regra_executa_lib_prescr.cd_perfil%type;
cd_proc_excluir_w               regra_executa_lib_prescr.cd_proc_excluir%type;
cd_setor_atendimento_w          regra_executa_lib_prescr.cd_setor_atendimento%type;
cd_setor_entrega_w              regra_executa_lib_prescr.cd_setor_entrega%type;
cd_setor_execucao_w		regra_executa_lib_prescr.cd_setor_execucao%type;
ie_executar_w                   regra_executa_lib_prescr.ie_executar%type;
ie_origem_proc_excluir_w        regra_executa_lib_prescr.ie_origem_proc_excluir%type;
ie_sem_setor_w                  regra_executa_lib_prescr.ie_sem_setor%type;
ie_somente_autorizado_w         regra_executa_lib_prescr.ie_somente_autorizado%type;
ie_tipo_convenio_w              regra_executa_lib_prescr.ie_tipo_convenio%type;
nr_seq_exame_interno_w          regra_executa_lib_prescr.nr_seq_exame_interno%type;
nr_seq_exame_lab_w              regra_executa_lib_prescr.nr_seq_exame_lab%type;
nr_seq_grupo_lab_w              regra_executa_lib_prescr.nr_seq_grupo_lab%type;

/*variaveis tabela 'regra_valor_conta' */	
cd_categoria_w			regra_valor_conta.cd_categoria%type;
cd_classe_material_w		regra_valor_conta.cd_classe_material%type;
cd_especialidade_proc_w		regra_valor_conta.cd_especialidade_proc%type;
cd_grupo_material_w		regra_valor_conta.cd_grupo_material%type;
cd_material_w			regra_valor_conta.cd_material%type;
cd_subgrupo_material_w		regra_valor_conta.cd_subgrupo_material%type;
ds_regra_w			regra_valor_conta.ds_regra%type;
ie_regra_w			regra_valor_conta.ie_regra%type;
ie_responsavel_credito_w	regra_valor_conta.ie_responsavel_credito%type;
ie_tipo_atend_conta_w		regra_valor_conta.ie_tipo_atend_conta%type;
nr_seq_proc_int_ref_w		regra_valor_conta.nr_seq_proc_int_ref%type;


cursor C01 is
	select	cd_setor_atendimento
	from	setor_atendimento
	where	((ie_tipo_acao_p = 1 and obter_se_contido(cd_setor_atendimento, elimina_aspas(cd_setor_destino_p)) = 'S') or
		(ie_tipo_acao_p = 2 and not obter_se_contido(cd_setor_atendimento, elimina_aspas(cd_setor_destino_p)) = 'S'));
		
cursor C02 is 
	select	cd_area_proced,          
		cd_cgc,                  
		cd_cgc_laboratorio,      
		cd_convenio,             
		cd_especial_proced,      
		cd_estabelecimento,     
		cd_grupo_proced,         
		cd_medico_exec_filtro,
		cd_medico_executor,      
		cd_pessoa_fisica,        
		cd_procedimento,         
		cd_setor_prescricao,     
		cd_tipo_procedimento,    
		dt_atualizacao_nrec,     
		dt_final_vigencia,       
		dt_inicio_vigencia,      
		hr_final,                
		hr_inicial,              
		ie_credenciado,          
		ie_medico_executor,      
		ie_origem_inf,           
		ie_origem_proced,        
		ie_tipo_atendimento,     
		nm_usuario_nrec,         
		nr_seq_classificacao,    
		nr_seq_equipe,           
		nr_seq_exame,            
		nr_seq_forma_org,       
		nr_seq_grupo,
		nr_seq_proc_interno,
		nr_seq_subgrupo
	from	consiste_setor_proc
	where	cd_Setor_atendimento = cd_setor_origem_p;
	
Cursor C03 is
	select	cd_area_procedimento,    
		cd_cgc_laboratorio,      
		cd_convenio,             
		cd_especialidade,        
		cd_estabelecimento,      
		cd_grupo_proc,          
		cd_medico_exec,          
		cd_perfil,               
		cd_procedimento,        
		cd_proc_excluir,        
		cd_setor_atendimento,   
		cd_setor_entrega,        
		cd_tipo_procedimento,   
		dt_atualizacao_nrec,     
		hr_final,                
		hr_inicial,              
		ie_executar,            
		ie_origem_proced,       
		ie_origem_proc_excluir,
		ie_sem_setor,            
		ie_somente_autorizado,   
		ie_tipo_atendimento,
		ie_tipo_convenio,
		nm_usuario_nrec,
		nr_seq_exame_interno,
		nr_seq_exame_lab,
		nr_seq_forma_org,
		nr_seq_grupo,
		nr_seq_grupo_lab,
		nr_seq_subgrupo
	from	regra_executa_lib_prescr
	where	cd_setor_execucao = cd_setor_origem_p;

Cursor C04 is
	select	cd_area_procedimento,    
		cd_categoria,            
		cd_classe_material,      
		cd_convenio,             
		cd_especialidade_proc,   
		cd_estabelecimento,      
		cd_grupo_material,       
		cd_grupo_proc,           
		cd_material,             
		cd_procedimento,          
		cd_subgrupo_material,    
		ds_regra,                          
		dt_atualizacao_nrec,     
		ie_origem_proced,       
		ie_regra,                
		ie_responsavel_credito,  
		ie_tipo_atend_conta,     
		ie_tipo_atendimento,      
		nm_usuario_nrec,        
		nr_seq_exame,            
		nr_seq_proc_interno,     
		nr_seq_proc_int_ref
	from	regra_valor_conta
	where	cd_Setor_atendimento = cd_setor_origem_p;	

begin

open C01;
loop 
fetch C01 into
	cd_Setor_destino_w;
exit when C01%notfound;
	begin
	--if(cd_Setor_destino_w > 0) and
	--((ie_tipo_acao_p = '1') and (cd_Setor_destino_w <> cd_setor_origem_p)) or ((ie_tipo_acao_p = '2') and (cd_Setor_destino_w <> cd_setor_origem_p))) then
	
	if	(cd_Setor_destino_w > 0) and
		(cd_Setor_destino_w <> cd_setor_origem_p) then
	
		if(nm_tabela_p = 'CONSISTE_SETOR_PROC') then
			open C02;
			loop
			fetch C02 into
				cd_area_proced_w,          
				cd_cgc_w,
				cd_cgc_laboratorio_w,
				cd_convenio_w,
				cd_especial_proced_w,
				cd_estabelecimento_w,
				cd_grupo_proced_w,
				cd_medico_exec_filtro_w,
				cd_medico_executor_w,
				cd_pessoa_fisica_w,
				cd_procedimento_w,
				cd_setor_prescricao_w,
				cd_tipo_procedimento_w,
				dt_atualizacao_nrec_w,
				dt_final_vigencia_w,
				dt_inicio_vigencia_w,
				hr_final_w,
				hr_inicial_w,
				ie_credenciado_w,
				ie_medico_executor_w,
				ie_origem_inf_w,
				ie_origem_proced_w,
				ie_tipo_atendimento_w,
				nm_usuario_nrec_w,
				nr_seq_classificacao_w,
				nr_seq_equipe_w,
				nr_seq_exame_w,
				nr_seq_forma_org_w,
				nr_seq_grupo_w,
				nr_seq_proc_interno_w,
				nr_seq_subgrupo_w;
			exit when C02%notfound;
				begin
				
				insert into consiste_setor_proc(
					cd_area_proced,          
					cd_cgc,                  
					cd_cgc_laboratorio,      
					cd_convenio,             
					cd_especial_proced,      
					cd_estabelecimento,     
					cd_grupo_proced,         
					cd_medico_exec_filtro,
					cd_medico_executor,      
					cd_pessoa_fisica,        
					cd_procedimento,         
					cd_setor_atendimento,    
					cd_setor_prescricao,     
					cd_tipo_procedimento,    
					dt_atualizacao ,         
					dt_atualizacao_nrec,     
					dt_final_vigencia,       
					dt_inicio_vigencia,      
					hr_final,                
					hr_inicial,              
					ie_credenciado,          
					ie_medico_executor,      
					ie_origem_inf,           
					ie_origem_proced,        
					ie_tipo_atendimento,     
					nm_usuario,              
					nm_usuario_nrec,         
					nr_seq_classificacao,    
					nr_seq_equipe,           
					nr_seq_exame,            
					nr_seq_forma_org,       
					nr_seq_grupo,            
					nr_seq_proc_interno,     
					nr_seq_subgrupo,         
					nr_sequencia)
				values(	cd_area_proced_w,          
					cd_cgc_w,
					cd_cgc_laboratorio_w,
					cd_convenio_w,
					cd_especial_proced_w,
					cd_estabelecimento_w,
					cd_grupo_proced_w,
					cd_medico_exec_filtro_w,
					cd_medico_executor_w,
					cd_pessoa_fisica_w,
					cd_procedimento_w,
					cd_Setor_destino_w,
					cd_setor_prescricao_w,
					cd_tipo_procedimento_w,
					sysdate,
					dt_atualizacao_nrec_w,
					dt_final_vigencia_w,
					dt_inicio_vigencia_w,
					hr_final_w,
					hr_inicial_w,
					ie_credenciado_w,
					ie_medico_executor_w,
					ie_origem_inf_w,
					ie_origem_proced_w,
					ie_tipo_atendimento_w,
					nm_usuario_p,
					nm_usuario_nrec_w,
					nr_seq_classificacao_w,
					nr_seq_equipe_w,
					nr_seq_exame_w,
					nr_seq_forma_org_w,
					nr_seq_grupo_w,
					nr_seq_proc_interno_w,
					nr_seq_subgrupo_w,    
					consiste_setor_proc_seq.nextval);
				end;
			end loop;
			close C02;
		
		elsif	(nm_tabela_p = 'REGRA_EXECUTA_LIB_PRESCR') then
			open C03;
			loop
			fetch C03 into
				cd_area_procedimento_w,    
				cd_cgc_laboratorio_w,      
				cd_convenio_w,             
				cd_especialidade_w,        
				cd_estabelecimento_w,      
				cd_grupo_proc_w,          
				cd_medico_exec_w,          
				cd_perfil_w,               
				cd_procedimento_w,        
				cd_proc_excluir_w,        
				cd_setor_atendimento_w,   
				cd_setor_entrega_w,        
				cd_tipo_procedimento_w,   
				dt_atualizacao_nrec_w,     
				hr_final_w,                
				hr_inicial_w,              
				ie_executar_w,            
				ie_origem_proced_w,       
				ie_origem_proc_excluir_w,
				ie_sem_setor_w,            
				ie_somente_autorizado_w,   
				ie_tipo_atendimento_w,
				ie_tipo_convenio_w,
				nm_usuario_nrec_w,
				nr_seq_exame_interno_w,
				nr_seq_exame_lab_w,
				nr_seq_forma_org_w,
				nr_seq_grupo_w,
				nr_seq_grupo_lab_w,
				nr_seq_subgrupo_w;
			exit when C03%notfound;
				begin
				
				insert into regra_executa_lib_prescr(
					cd_area_procedimento,    
					cd_cgc_laboratorio,      
					cd_convenio,             
					cd_especialidade,        
					cd_estabelecimento,      
					cd_grupo_proc,          
					cd_medico_exec,          
					cd_perfil,               
					cd_procedimento,        
					cd_proc_excluir,        
					cd_setor_atendimento,   
					cd_setor_entrega,        
					cd_setor_execucao,       
					cd_tipo_procedimento,   
					dt_atualizacao,          
					dt_atualizacao_nrec,     
					hr_final,                
					hr_inicial,              
					ie_executar,            
					ie_origem_proced,       
					ie_origem_proc_excluir,
					ie_sem_setor,            
					ie_somente_autorizado,   
					ie_tipo_atendimento,
					ie_tipo_convenio,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_exame_interno,
					nr_seq_exame_lab,
					nr_seq_forma_org,
					nr_seq_grupo,
					nr_seq_grupo_lab,
					nr_seq_subgrupo,
					nr_sequencia)
				values(	cd_area_procedimento_w,    
					cd_cgc_laboratorio_w,      
					cd_convenio_w,             
					cd_especialidade_w,        
					cd_estabelecimento_w,      
					cd_grupo_proc_w,          
					cd_medico_exec_w,          
					cd_perfil_w,               
					cd_procedimento_w,        
					cd_proc_excluir_w,        
					cd_setor_atendimento_w,   
					cd_setor_entrega_w,        
					cd_Setor_destino_w,       
					cd_tipo_procedimento_w,   
					sysdate,
					dt_atualizacao_nrec_w,     
					hr_final_w,                
					hr_inicial_w,              
					ie_executar_w,            
					ie_origem_proced_w,       
					ie_origem_proc_excluir_w,
					ie_sem_setor_w,            
					ie_somente_autorizado_w,   
					ie_tipo_atendimento_w,
					ie_tipo_convenio_w,
					nm_usuario_p,
					nm_usuario_nrec_w,
					nr_seq_exame_interno_w,
					nr_seq_exame_lab_w,
					nr_seq_forma_org_w,
					nr_seq_grupo_w,
					nr_seq_grupo_lab_w,
					nr_seq_subgrupo_w,
					regra_executa_lib_prescr_seq.nextval);
				end;
			end loop;
			close C03;
		
		elsif	(nm_tabela_p = 'REGRA_VALOR_CONTA') then
			open C04;
			loop
			fetch C04 into
				cd_area_procedimento_w,    
				cd_categoria_w,            
				cd_classe_material_w,      
				cd_convenio_w,             
				cd_especialidade_proc_w,   
				cd_estabelecimento_w,      
				cd_grupo_material_w,       
				cd_grupo_proc_w,           
				cd_material_w,             
				cd_procedimento_w,         
				cd_subgrupo_material_w,    
				ds_regra_w,                     
				dt_atualizacao_nrec_w,     
				ie_origem_proced_w,       
				ie_regra_w,                
				ie_responsavel_credito_w,  
				ie_tipo_atend_conta_w,     
				ie_tipo_atendimento_w,                
				nm_usuario_nrec_w,        
				nr_seq_exame_w,            
				nr_seq_proc_interno_w,     
				nr_seq_proc_int_ref_w;
			exit when C04%notfound;
				begin
				
				insert into regra_valor_conta(
					cd_area_procedimento,    
					cd_categoria,            
					cd_classe_material,      
					cd_convenio,             
					cd_especialidade_proc,   
					cd_estabelecimento,      
					cd_grupo_material,       
					cd_grupo_proc,           
					cd_material,             
					cd_procedimento,         
					cd_setor_atendimento,    
					cd_subgrupo_material,    
					ds_regra,                
					dt_atualizacao,          
					dt_atualizacao_nrec,     
					ie_origem_proced,       
					ie_regra,                
					ie_responsavel_credito,  
					ie_tipo_atend_conta,     
					ie_tipo_atendimento,     
					nm_usuario,              
					nm_usuario_nrec,        
					nr_seq_exame,            
					nr_seq_proc_interno,     
					nr_seq_proc_int_ref,     
					nr_sequencia)
				values(	cd_area_procedimento_w,    
					cd_categoria_w,            
					cd_classe_material_w,      
					cd_convenio_w,             
					cd_especialidade_proc_w,   
					cd_estabelecimento_w,      
					cd_grupo_material_w,       
					cd_grupo_proc_w,           
					cd_material_w,             
					cd_procedimento_w,         
					cd_Setor_destino_w,
					cd_subgrupo_material_w,    
					ds_regra_w,                
					sysdate,
					dt_atualizacao_nrec_w,     
					ie_origem_proced_w,       
					ie_regra_w,                
					ie_responsavel_credito_w,  
					ie_tipo_atend_conta_w,     
					ie_tipo_atendimento_w,     
					nm_usuario_p, 
					nm_usuario_nrec_w,        
					nr_seq_exame_w,            
					nr_seq_proc_interno_w,     
					nr_seq_proc_int_ref_w,
					regra_valor_conta_seq.nextval);
				end;
			end loop;
			close C04;
		end if;	
	end if;
	end;
end loop;
close C01;	

end copia_regra_setor;
/