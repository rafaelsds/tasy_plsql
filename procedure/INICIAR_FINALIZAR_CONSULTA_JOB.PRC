create or replace
procedure iniciar_finalizar_consulta_job(	qt_horas_p		number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p    number) is 

nr_sequencia_w		number(10);	

cursor	c01 is
	select	nr_sequencia
	from	oft_consulta
	where	dt_consulta < sysdate - (qt_horas_p/24)
	and	dt_fim_consulta is null
	and	dt_cancelamento is null;

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	iniciar_finalizar_consulta(nr_sequencia_w,'F',nm_usuario_p,cd_estabelecimento_p);
	
	end;
end loop;
close C01;

end iniciar_finalizar_consulta_job;
/