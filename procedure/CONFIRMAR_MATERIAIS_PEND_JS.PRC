create or replace
procedure confirmar_materiais_pend_js(	nr_prescricao_p			number,
					ds_lista_material_p		varchar2,
					ie_status_imp_p			varchar2,
					nm_usuario_p			varchar2,
					ie_consistir_altura_peso_p	varchar2,
					ds_lista_seq_prescr_p 		out varchar2,
					ds_mensagem_p			out varchar2) is 
			
nr_sequencia_w			number(10);	
ds_lista_seq_prescr_w		varchar2(1000);	
ie_exige_w			varchar2(1) := 'N';
ie_exige_peso_coleta_w 		varchar2(1) := 'N';
ds_mensagem_w			varchar2(255) := '';
var_exibe_prot_glic		varchar2(1) :=  'S';
var_gerar_amostra_coleta 	varchar2(1) :=  'S';

cursor c01 is
select 	nr_seq_prescr
from 	exame_lab_resultado a,
	material_exame_lab c,
	exame_lab_result_item b,
	prescr_procedimento d
where 	a.nr_seq_resultado = b.nr_seq_resultado
and 	a.nr_prescricao = d.nr_prescricao
and 	d.nr_sequencia = b.nr_seq_prescr
and 	b.nr_seq_material = c.nr_sequencia
and 	obter_se_contido_char(c.cd_material_exame,ds_lista_material_p) = 'S'
and 	d.ie_status_atend = nvl(ie_status_imp_p, d.ie_status_atend)
and 	a.nr_prescricao = nr_prescricao_p;

cursor c02 is   
select 	nr_sequencia
from 	prescr_procedimento
where 	obter_se_contido_char(cd_material_exame,ds_lista_material_p) = 'S'
and 	ie_status_atend = nvl(ie_status_imp_p, ie_status_atend)
and 	nr_prescricao = nr_prescricao_p;

cursor c03 is
select 	nr_seq_prescr
from 	exame_lab_resultado a,
	material_exame_lab c,
	exame_lab_result_item b,
	prescr_procedimento d
where 	a.nr_seq_resultado = b.nr_seq_resultado
and 	a.nr_prescricao = d.nr_prescricao
and 	d.nr_sequencia = b.nr_seq_prescr
and 	b.nr_seq_material = c.nr_sequencia
and 	obter_se_contido_char(c.cd_material_exame,ds_lista_material_p) = 'S'
and 	d.ie_status_atend = nvl(ie_status_imp_p, d.ie_status_atend)
and 	a.nr_prescricao = nr_prescricao_p
and 	d.nr_seq_prot_glic is null
and 	not exists (select 1 from proc_interno v where v.nr_sequencia = d.nr_seq_proc_interno and v.ie_ctrl_glic = 'CIG');

cursor c04 is   
select 	nr_sequencia
from 	prescr_procedimento d
where 	obter_se_contido_char(cd_material_exame,ds_lista_material_p) = 'S'
and 	ie_status_atend = nvl(ie_status_imp_p, ie_status_atend)
and 	nr_prescricao = nr_prescricao_p
and 	d.nr_seq_prot_glic is null
and 	not exists (select 1 from proc_interno v where v.nr_sequencia = d.nr_seq_proc_interno and v.ie_ctrl_glic = 'CIG');

begin

var_exibe_prot_glic := nvl(Wheb_assist_pck.obterParametroFuncao(722,228), 'S');

if (var_exibe_prot_glic = 'S') then
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		if	(ds_lista_seq_prescr_w is not null) then
			ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || ',',1,1000);
		end if;
		
		ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || nr_sequencia_w,1,1000);
			
		end;
	end loop;
	close C01;
	
	if	(ds_lista_seq_prescr_w is null) then
	
		open C02;
		loop
		fetch C02 into	
			nr_sequencia_w;
		exit when C02%notfound;
			begin
			
			if	(ds_lista_seq_prescr_w is not null) then
				ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || ',',1,1000);
			end if;
			
			ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || nr_sequencia_w,1,1000);
			
			end;
		end loop;
		close C02;
	end if;
else
	open C03;
	loop
	fetch C03 into	
		nr_sequencia_w;
	exit when C03%notfound;
		begin
		if	(ds_lista_seq_prescr_w is not null) then
			ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || ',',1,1000);
		end if;
		
		ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || nr_sequencia_w,1,1000);
			
		end;
	end loop;
	close C03;
	
	if	(ds_lista_seq_prescr_w is null) then
	
		open C04;
		loop
		fetch C04 into	
			nr_sequencia_w;
		exit when C04%notfound;
			begin
			
			if	(ds_lista_seq_prescr_w is not null) then
				ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || ',',1,1000);
			end if;
			
			ds_lista_seq_prescr_w := substr(ds_lista_seq_prescr_w || nr_sequencia_w,1,1000);
			
			end;
		end loop;
		close C04;
	end if;
end if;

ds_lista_seq_prescr_p := ds_lista_seq_prescr_w;

select 	nvl(max(IE_GERA_AMOSTRA_COLETA), 'N')
into 	var_gerar_amostra_coleta
from	lab_parametro
where	cd_estabelecimento = obter_estabelecimento_ativo;

if	(var_gerar_amostra_coleta = 'N') then
	gerar_prescr_proc_seq_lab(nr_prescricao_p, 'C', nm_usuario_p);
end if;

ie_exige_w := lab_obter_se_exige_alt_peso(nr_prescricao_p, ds_lista_seq_prescr_w);
ie_exige_peso_coleta_w := lab_obter_exige_alt_peso_col(nr_prescricao_p,ds_lista_seq_prescr_w);

if	(ie_consistir_altura_peso_p = 'S') and
	((ie_exige_w = 'S') or (ie_exige_peso_coleta_w ='S')) then
	ds_mensagem_w := WHEB_MENSAGEM_PCK.get_texto(312008);
	ds_mensagem_p := ds_mensagem_w;
end if;

end confirmar_materiais_pend_js;
/