create or replace procedure REG_INSERT_URS_TEST_PLAN(nm_usuario_p          varchar2,
                                                     nr_seq_intencao_uso_p number,
                                                     cd_versao_p           varchar2) is

  nr_seq_plan_w number(10);

  cursor c01 is
    select t.nr_sequencia, t.nr_seq_customer
      from reg_caso_teste t, reg_customer_requirement a
     where t.nr_seq_customer = a.nr_sequencia
       and t.ie_tipo_documento = 'V'
       and a.nr_sequencia in
           (select w.nr_seq_customer
              from w_test_plan_pending_urs w
             where w.nm_usuario = nm_usuario_p
               and w.nr_seq_intencao_uso = nr_seq_intencao_uso_p);

  r01 c01%rowtype;

begin

  for r01 in c01 loop
    select nvl(max(nr_sequencia), 0) + 1
      into nr_seq_plan_w
      from reg_validation_pendencies;
  
    insert into reg_validation_pendencies
      (NR_SEQUENCIA,
       DT_ATUALIZACAO,
       NM_USUARIO,
       DT_ATUALIZACAO_NREC,
       NM_USUARIO_NREC,
       NR_SEQ_CR,
       NR_SEQ_TC,
       DS_VERSION)
    values
      (nr_seq_plan_w,
       sysdate,
       nm_usuario_p,
       sysdate,
       nm_usuario_p,
       r01.nr_seq_customer,
       r01.nr_sequencia,
       cd_versao_p);
  
  end loop;

  delete from w_test_plan_pending_urs w
   where w.nm_usuario = nm_usuario_p
     and w.nr_seq_intencao_uso = nr_seq_intencao_uso_p;

  commit;

end reg_insert_urs_test_plan;
/
