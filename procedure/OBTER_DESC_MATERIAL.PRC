create or replace
FUNCTION Obter_Desc_Material(
		cd_material_p	NUMBER)
	RETURN VARCHAR IS

ds_retorno_w		VARCHAR2(255);

BEGIN

IF	( cd_material_p IS NOT NULL ) THEN
	SELECT 	SUBSTR(ds_material,1,255)
	INTO	ds_retorno_w
	FROM 	material
	WHERE 	cd_material = cd_material_p;
END IF;

RETURN ds_retorno_w;

END Obter_Desc_Material;
/
