create or replace
procedure pls_enviar_sms_analise
			(	ds_remetente_p		varchar2,
				ds_destinatario_p	varchar2,
				ds_mensagem_p		varchar2,
				nr_seq_analise_p	number,
				nm_usuario_p		varchar2) is
	
id_sms_w	number(10);	
begin
if	(ds_remetente_p is not null) and
	(ds_destinatario_p is not null) and
	(ds_mensagem_p is not null) and
	(nr_seq_analise_p is not null) and
	(nm_usuario_p is not null) then

	/* enviar sms */
	wheb_sms.enviar_sms(ds_remetente_p, ds_destinatario_p, ds_mensagem_p, nm_usuario_p,id_sms_w);
	
	/*insert into logxxxx_tasy(cd_log, dt_atualizacao, nm_usuario, ds_log)
		values (92222, sysdate, nm_usuario_p, 'Remetente = ' || ds_remetente_p || '  -  ' ||
						      'Destinatário = ' || ds_destinatario_p || '  -  ' ||
						      'Mensagem = ' || substr(ds_mensagem_p,1,1400));*/

	insert into LOG_ENVIO_SMS
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_envio,
		nr_telefone,
		ds_mensagem,
		nr_seq_analise,
		id_sms)
	values	(LOG_ENVIO_SMS_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		ds_destinatario_p,
		ds_mensagem_p,
		nr_seq_analise_p,
		id_sms_w);

end if;
	
commit;

end pls_enviar_sms_analise;
/