create or replace
procedure sup_liberar_reg_kit_usuario(
			nm_usuario_p		Varchar2,
			ie_situacao_p		Varchar2) is 

begin

update	kit_estoque_reg 
set	dt_liberacao 	= sysdate,
nm_usuario_lib = nm_usuario_p
where	ie_situacao 	= ie_situacao
and	dt_liberacao 	is null 
and 	nm_usuario 	= nm_usuario_p;

commit;

end sup_liberar_reg_kit_usuario;
/