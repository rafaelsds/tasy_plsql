create or replace
procedure vincular_guias_receb(
	nr_seq_receb_p		number,
	nr_sequencia_p		number,
	nm_usuario_p		Varchar2) is 

begin


update convenio_retorno_item
set nr_seq_receb = nr_seq_receb_p
where nr_sequencia = nr_sequencia_p;

commit;

end vincular_guias_receb;
/