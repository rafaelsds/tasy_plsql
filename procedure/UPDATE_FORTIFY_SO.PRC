create or replace procedure update_fortify_so(	nr_sequencia_p		number,
												nr_seq_ordem_serv_p	number
											) is

begin

	begin

	update   fortify
	set      nr_seq_service_order   = nr_seq_ordem_serv_p
    where    nr_sequencia   		= nr_sequencia_p;

	end;

commit;

end update_fortify_so;
/  