create or replace
procedure altera_convenio_retorno_glosa(
		nr_sequencia_p		number,
		cd_motivo_glosa_p	number,
		nm_usuario_p		Varchar2) is 

begin

update  convenio_retorno_glosa
set	cd_motivo_glosa = cd_motivo_glosa_p,
	nm_usuario = nm_usuario_p
where   nr_seq_ret_item = nr_sequencia_p;


commit;

end altera_convenio_retorno_glosa;
/