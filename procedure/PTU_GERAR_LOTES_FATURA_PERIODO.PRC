create or replace
procedure ptu_gerar_lotes_fatura_periodo
			(	dt_inicial_p		date,
				dt_final_p		date,
				dt_prev_envio_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

ds_camara_w		varchar2(255);
nr_seq_camara_w		number(10);
nr_seq_periodo_w	number(10);
nr_seq_lote_w		number(10);
dt_limite_a500_w	date;

/* O par�metro dt_prev_envio_p tem de ser varchar2 pois pelo delphi n�o � obrigat�rio pass�-lo*/

/* Cursor das c�maras */
Cursor C01 is
	select	a.nr_sequencia,
		a.ds_camara
	from	pls_camara_compensacao a
	where	a.ie_situacao	= 'A';					

begin
if	(dt_inicial_p is not null) and
	(dt_final_p is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_camara_w,
		ds_camara_w;
	exit when C01%notfound;
		begin
		select	max(a.nr_sequencia)
		into	nr_seq_periodo_w
		from	pls_camara_calendario b,
			pls_camara_calend_periodo a
		where	a.nr_seq_calendario	= b.nr_sequencia
		and	b.nr_seq_camara		= nr_seq_camara_w
		and	a.dt_limite_a500 between dt_inicial_p and dt_final_p;
		
		if	(nr_seq_periodo_w is null) then
			-- N�o foi encontrada uma data limite envio A500 no calend�rio da c�mara #@DS_CAMARA#@ que esteja entre o per�odo inicial e final definido.
			wheb_mensagem_pck.exibir_mensagem_abort(266800, 'DS_CAMARA=' || ds_camara_w);
		else
			select	max(a.dt_limite_a500)
			into	dt_limite_a500_w
			from	pls_camara_calendario b,
				pls_camara_calend_periodo a
			where	a.nr_seq_calendario	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_periodo_w;
		end if;
		
		select	ptu_lote_fatura_envio_seq.nextval
		into	nr_seq_lote_w
		from	dual;
		
		insert into ptu_lote_fatura_envio
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_estabelecimento,
			dt_inicio,
			dt_fim,
			nr_seq_camara,
			nr_seq_periodo,
			ie_sem_camara_comp,
			dt_previsao_envio)
		values	(nr_seq_lote_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_estabelecimento_p,
			dt_inicial_p,
			dt_final_p,
			nr_seq_camara_w,
			nr_seq_periodo_w,
			'N',
			dt_limite_a500_w);
			
		ptu_gerar_lote_fatura(nr_seq_lote_w,cd_estabelecimento_p,nm_usuario_p);
		end;
	end loop;
	close c01;
	
	/* Criar tamb�m um lote para as unimeds sem c�mara */
	select	ptu_lote_fatura_envio_seq.nextval
	into	nr_seq_lote_w
	from	dual;

	insert into ptu_lote_fatura_envio
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_estabelecimento,
		dt_inicio,
		dt_fim,
		nr_seq_camara,
		nr_seq_periodo,
		ie_sem_camara_comp,
		dt_previsao_envio)
	values	(nr_seq_lote_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_estabelecimento_p,
		dt_inicial_p,
		dt_final_p,
		null,
		null,
		'S',
		decode(dt_prev_envio_p,null,null,to_date(dt_prev_envio_p)));
			
	ptu_gerar_lote_fatura(nr_seq_lote_w,cd_estabelecimento_p,nm_usuario_p);
end if;

commit;

end ptu_gerar_lotes_fatura_periodo;
/