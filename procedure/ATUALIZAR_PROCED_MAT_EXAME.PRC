create or replace
procedure Atualizar_proced_mat_exame 
			(nr_seq_exame_p		in 	number,
			cd_convenio_p		in 	number,
			cd_categoria_p		in	varchar2,
			ie_tipo_atendimento_p	in	number,
			cd_estabelecimento_p	in	number,
			ie_tipo_convenio_p	in	number,
			nr_seq_proc_interno_p	in	number,
			cd_material_exame_p	in	varchar2,
			nr_prescricao_p		in	number,
			nr_seq_proced_p		in	number) is

ds_erro_w			varchar2(2000);
nr_seq_proc_interno_aux_w	number(10);
cd_setor_exclusivo_w		number(10);
cd_procedimento_w		number(15);
cd_proced_prescr_w		number(15);
ie_origem_proced_w		number(10);
cd_plano_convenio_w		varchar2(10);
			
begin

if (nr_prescricao_p is not null) then
	select	max(substr(Obter_Plano_Conv_Atend(a.nr_atendimento),1,10))
	into	cd_plano_convenio_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;
end if;

Obter_Exame_Lab_Convenio(nr_seq_exame_p,
			cd_convenio_p,
			cd_categoria_p,
			ie_tipo_atendimento_p,
			cd_estabelecimento_p,
			ie_tipo_convenio_p,
			nr_seq_proc_interno_p,
			cd_material_exame_p,
			cd_plano_convenio_w,
			cd_setor_exclusivo_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			ds_erro_w,
			nr_seq_proc_interno_aux_w);
			
select	max(cd_procedimento)
into	cd_proced_prescr_w
from	prescr_procedimento
where	nr_prescricao		= nr_prescricao_p
and	nr_sequencia		= nr_seq_proced_p;
			
if	(cd_procedimento_w > 0) and
	(cd_procedimento_w <> cd_proced_prescr_w) then
	update	prescr_procedimento
	set	cd_setor_atendimento	= cd_setor_exclusivo_w,
		cd_procedimento		= cd_procedimento_w,
		ie_origem_proced	= ie_origem_proced_w
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		= nr_seq_proced_p;
	
	commit;
	
end if;

end Atualizar_proced_mat_exame;
/