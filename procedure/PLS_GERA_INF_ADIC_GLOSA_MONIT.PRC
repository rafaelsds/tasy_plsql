create or replace
procedure pls_gera_inf_adic_glosa_monit is 

qt_reg_w	pls_integer;

begin

select	count(1)
into	qt_reg_w
from	pls_monitor_tiss_inf_glosa;

if	(qt_reg_w = 0) then
	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (17, 'Motivo: O n�mero do CNES do prestador executor est� incorreto.' || pls_util_pck.enter_w || 
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar o CNES do prestador executor no cadastro de pessoa f�sica ou pessoa jur�dica, conforme prestador executor.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar o cadastro.' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Observa��o: Caso o prestador ainda n�o possua um CNES, dever� ser informado o c�digo ''9999999''.', sysdate, 'aedemuth', sysdate, 'aedemuth', '1202');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (15, 'Motivo: O c�digo do cart�o nacional de sa�de do benefici�rio � inv�lido.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Atualizar o n�mero do cart�o nacional de sa�de no cadastro de pessoa f�sica do benefici�rio.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar o cadastro.' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '1002');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (16, 'Motivo: O plano informado para benefici�rio n�o corresponde a nenhum plano na base da ANS.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar o plano do benefici�rio no cadastro do benefici�rio' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar o cadastro.' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '1024');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (18, 'Motivo: O CPF ou CNPJ forneceido � inv�lido.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Para prestador PJ, � necess�rio ajustar a informa��o no cadastro do prestador, para prestadores PF, � necess�rio ajustar o cadastro no cadastro de PF.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar o cadastro.' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '1206');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (19, 'Motivo: N�o foi encontrado, na base da ANS, um registro com c�digo de guia correspondente ao informado. A cr�tica normalmente ocorre quando � enviado um registro de Altera��o ou Exclus�o, por�m, n�o h� registros com mesma identifica��o na base da ANS.'
	|| pls_util_pck.enter_w || pls_util_pck.enter_w ||
	'Como ajustar: Verificar se houve registro correspondete aceito na ANS.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Desfazer os dados gerados.' || pls_util_pck.enter_w ||
	'- Ajustar o c�digo da guia (caso necess�rio)' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '1307');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (20, 'Motivo: Guia enviada em duplicidade, ou duas contas distintas possuem mesma chave de registro.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Alterar o c�digo da guia prestador da conta glosada.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Na aba "Retorno / Contas glosadas", verificar se a conta que recebeu a glosa possui a informa��o "Seq monitor guia", caso contr�rio, identificar o registro atrav�s da funcionalidade "BD - Selecionar conta para registro n�o identificado".' || pls_util_pck.enter_w ||
	'- Navegar at� a aba "Retorno / Lote", selecionar o lote de retorno que possui a glosa e aplicar a funcionalidade "BD - Alterar n�mero guia prestador para sequencial da conta".' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '1308');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (21, 'Motivo: Data preenchida fora do padr�o TISS.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar a quando campo a glosa est� referenciando e ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1323');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (22, 'Motivo: C�digo de diagn�stico informado n�o corresponde com a tabela CID10.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1509');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (23, 'Motivo: Tipo de atendimento inv�lido (fora do padr�o TISS), ou n�o informado (Registro obrigat�rio para as guias de SP/SADT).' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1602');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (24, 'Motivo: Tipo de consulta fora do padr�o TISS.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1603');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (25, 'Motivo: Ocorre quando houve uma diverg�ncia de valores entre o primeiro e o segundo envio da guia.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar os registros divergentes atrav�s da funcionalidade "BD - Comparar com primeiro envio", dispon�vel na aba "Retorno / Contas glosadas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o divergente' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1705');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (26, 'Motivo: Ocorre quando houve uma diverg�ncia de valores entre o primeiro e o segundo envio da guia.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar os registros divergentes atrav�s da funcionalidade "BD - Comparar com primeiro envio", dispon�vel na aba "Retorno / Contas glosadas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o divergente' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1706');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (27, 'Motivo: Pode ocorrer por alguns motivos:' || pls_util_pck.enter_w ||
	'- Houve alguma altera��o entre o primeiro e o segundo envio de um dos itens da guia.' || pls_util_pck.enter_w ||
	'	Como ajustar: Verificar os registros divergentes atrav�s da funcionalidade "BD - Comparar com primeiro envio", dispon�vel na aba "Retorno / Contas glosadas".' || pls_util_pck.enter_w ||
	'- Item pr�prio da operadora envio fora da tabela refer�ncia "00".' || pls_util_pck.enter_w ||
	'	Como ajustar: Verificar as informa��es de tabela refer�ncia e grupo ANS do item.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o divergente' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1801');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (28, 'Motivo: A quantidade apresentado do item de ser sempre maior que zero.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar se existe algum item com quantidade apresentada igual ou menor que zero.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '1806');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (29, 'Motivo: Item TUSS que, segundo manual ANS, deveria ser enviado de forma consolidada, enviado de forma individualizada.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar as informa��es de tabela refer�ncia e grupo ANS do item.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o divergente' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '2601');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (30, 'Motivo: O arquivo enviado est� fora do padr�o TISS.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Validar o arquivo de envio em um validador TISS, verificar e corrigir a informa��o fora do padr�o.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o divergente' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5001');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (31, 'Motivo: O arquivo enviado para ANS est� com o Hash incorreto.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Gerar um novo lote para a compet�ncia e envi�-lo novamente para ANS, caso a inconsist�ncia persista, favor entrar em contato com o suporte Philips.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5014');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (32, 'Motivo: Data de registro informada fora da compet�ncia informada no lote.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Gerar um novo lote para a compet�ncia e envi�-lo novamente para ANS, caso a inconsist�ncia persista, favor entrar em contato com o suporte Philips.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '5025');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (33, 'Motivo:' || pls_util_pck.enter_w ||
	'- Quando aplicado sobre o campo 041, trata-se de uma altera��o no diagn�stico da conta entre o primeiro e o segundo envio.' || pls_util_pck.enter_w ||
	'- Quando aplicado sobre o campo 071, houve uma altera��o na identifica��o do item (c�digo de grupo ANS e c�digo de tabela refer�ncia) entre o primeiro e o segundo envio.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar os registros divergentes atrav�s da funcionalidade "BD - Comparar com primeiro envio", dispon�vel na aba "Retorno / Contas glosadas" e ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o divergente' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5029');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (34, 'Motivo: O c�digo do munic�pio IBGE do prestador est� incorreto' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Para prestador PJ, � necess�rio ajustar a informa��o no cadastro dPJ, para prestadores PF, � necess�rio ajustar o cadastro no cadastro de PF, complemento comercial.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar o cadastro.' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia.', sysdate, 'aedemuth', sysdate, 'aedemuth', '5030');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (35, 'Motivo: Car�ter de atendimento inv�lido (fora do padr�o TISS).' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5031');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (36, 'Motivo: Motivo de encerramento inv�lido (fora do padr�o TISS).' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Ajustar a informa��o atrav�s da funcionalidade "BD - Ajustar informa��es conta", dispon�vel na aba "Envio / Contas".' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5033');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (37, 'Motivo: O valor total do somat�rio dos tipos de despesa n�o est� de acordo com o valor total pago. O valor total da guia, deve ser a soma dos campos:' || pls_util_pck.enter_w ||
	'- Valor total procedimento' || pls_util_pck.enter_w ||
	'- Valor total di�ria' || pls_util_pck.enter_w ||
	'- Valor total taxa' || pls_util_pck.enter_w ||
	'- Valor total medicamento' || pls_util_pck.enter_w ||
	'- Valor total material' || pls_util_pck.enter_w ||
	'- Valor total OPME' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Verificar se n�o h� um item com tabela refer�ncia n�o correspondete ao tipo de despesa.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Tipo de despesa x tabela refer�ncia:' || pls_util_pck.enter_w ||
	'Materiais e OPME: Tabela 19, 00, 98' || pls_util_pck.enter_w ||
	'Medicamentos: Tabela 19, 20, 00, 98' || pls_util_pck.enter_w ||
	'Gases medicinais: Tabela 18, 00, 98' || pls_util_pck.enter_w ||
	'Taxas e di�rias: Tabela 18, 00, 98' || pls_util_pck.enter_w ||
	'Pacotes: Tabela 00, 98' || pls_util_pck.enter_w ||
	'Procedimentos: Tabela 22, 00, 98' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia'
	, sysdate, 'aedemuth', sysdate, 'aedemuth', '5034');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (38, 'Motivo: C�digo do grupo ANS informado n�o corresponde com o padr�o TISS' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Identificar a forma de gera��o da informa��oa trav�s do campo "Origem grupo"  e ajustar conforme origem.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5036');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (39, 'Motivo: O valor total apresentado est� diferente do somat�rio dos valores apresentado dos itens.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Identificar o item com valor divergente e ajustar.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5042');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (40, 'Motivo: Para este tipo de guia, n�o pode ser informado o valor a quando o campo glosado referencia.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Identificar o valor gerado incorretamente e ajustar.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'A��es necess�rias:' || pls_util_pck.enter_w ||
	'- Ajustar a informa��o indicada' || pls_util_pck.enter_w ||
	'- Gerar um novo lote para a compet�ncia', sysdate, 'aedemuth', sysdate, 'aedemuth', '5050');

	insert into PLS_MONITOR_TISS_INF_GLOSA (NR_SEQUENCIA, DS_OBSERVACAO, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_MOTIVO_TISS)
	values (41, 'Motivo: Item gerado em duplicidade ou n�o agrupado na gera��o do arquivo.' || pls_util_pck.enter_w ||
	pls_util_pck.enter_w ||
	'Como ajustar: Entrar em contato com o suporte Philips.', sysdate, 'aedemuth', sysdate, 'aedemuth', '5053');
	commit;
end if;

end pls_gera_inf_adic_glosa_monit;
/