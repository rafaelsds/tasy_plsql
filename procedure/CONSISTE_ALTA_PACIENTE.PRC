create or replace
procedure consiste_alta_paciente(	nr_seq_lote_p 		number,
									nr_atendimento_p 	number) is

ie_consiste_alta_w	varchar2(1);
ie_alta_w		varchar2(1);
nr_atendimento_w	ap_lote.nr_atendimento%type;
	
begin

select 	max(nr_atendimento),
		max(decode(Obter_data_alta_Atend(nr_atendimento),null,'N','S'))
into	nr_atendimento_w,
		ie_alta_w
from 	ap_lote
where 	nr_sequencia = nr_seq_lote_p;

if	(nr_atendimento_p is not null) then
	select 	max(decode(Obter_data_alta_Atend(nr_atendimento_p),null,'N','S'))
	into	ie_alta_w
	from 	dual;
end if;
if	(ie_alta_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(170906);
end if;

end consiste_alta_paciente;
/
