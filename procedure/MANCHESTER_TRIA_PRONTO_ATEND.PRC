create or replace
procedure manchester_tria_pronto_atend(	nr_atendimento_p		  number,
					                    nm_usuario_p			  varchar2,
					                    cd_pessoa_fisica_p		  varchar2,
					                    ds_queixa_princ_p		  varchar2,
					                    ds_observacao_p			  varchar2,
					                    dt_inicio_triagem_p		  date,
					                    nr_seq_classif_p		  number,
					                    cd_estabelecimento_p	  number,
					                    ie_retriagem_p			  varchar2,
					                    nr_seq_manchester_fluxo_p number,
					                    cd_classificador_p		  varchar2,
					                    nr_seq_discriminador_p	  number,
					                    ie_nao_p			      varchar2,
					                    nr_seq_fluxo_original_p	  number,
                                        ds_detalhe_p              varchar2 default '',
					                    ds_erro_p			      out varchar2,
										nr_sequencia_p            out number) is

nr_regras_atendidas_w varchar2(2000);
cd_setor_atendimento_w varchar2(5);
ie_clinica_w	varchar2(5);
ds_erro_w		varchar2(2000);
nr_seq_fila_w				number(10);
nr_seq_pac_senha_fila_w			number(10);

begin
begin

ds_erro_p := '';

if (nr_atendimento_p is not null) then

	if	(nr_seq_classif_p is not null) then		
		
		update	atendimento_paciente
		set		nr_seq_triagem = nr_seq_classif_p,
				dt_inicio_atendimento = decode(dt_inicio_atendimento, null,dt_inicio_triagem_p,dt_inicio_atendimento),
				dt_fim_triagem = sysdate
		where	nr_atendimento = nr_atendimento_p;
	end if;
	
	select	max(ie_clinica) 
	into	ie_clinica_w
	from	atendimento_paciente  
	where	nr_atendimento = nr_atendimento_p
    and     not exists (select 	1
						from   	triagem_pronto_atend b
						where 	 b.nr_atendimento = nr_atendimento_p);


	select 	max(cd_setor_atendimento)
	into   	cd_setor_atendimento_w
	from   	ATEND_PACIENTE_UNIDADE
	where 	nr_atendimento = nr_atendimento_p
    and   	not exists (select 	1
						from   	triagem_pronto_atend b
						where 	 b.nr_atendimento = nr_atendimento_p);
						
	select triagem_pronto_atend_seq.nextval
	into   nr_sequencia_p
	from   dual;
    
    select max(a.nr_seq_pac_senha_fila)
    into  nr_seq_pac_senha_fila_w
    from	atendimento_paciente a
    where	a.nr_atendimento = nr_atendimento_p;
                
    if	(nr_seq_pac_senha_fila_w is not null) then
        select	max(nvl(nr_seq_fila_senha,nr_seq_fila_senha_origem)) nr_fila
        into	nr_seq_fila_w
        from	paciente_senha_fila
        where	nr_sequencia = nr_seq_pac_senha_fila_w;
    end if;

	insert into triagem_pronto_atend
			(nr_sequencia,
			nm_usuario,
			nm_usuario_nrec,
			nm_usuario_triagem,
			nr_atendimento,
			cd_pessoa_fisica,
			dt_inicio_triagem,
			dt_fim_triagem,
			ds_queixa_princ,
			ds_observacao,
			nr_seq_classif,
			dt_atualizacao,
			cd_estabelecimento,
			ie_retriagem,
			nr_seq_manchester_fluxo,
			cd_classificador,
			dt_atualizacao_nrec,
			nr_seq_discriminador,
			ie_nao,
			nr_seq_fluxo_original,
			ie_status_paciente,
               ds_resumo_manchester,
			   ie_clinica,
			   cd_setor_atendimento,
               nr_seq_fila_senha,
               nr_seq_pac_fila)
		values
			(nr_sequencia_p,
			nm_usuario_p,
			nm_usuario_p,
			nm_usuario_p,
			nr_atendimento_p,
			obter_pessoa_atendimento(nr_atendimento_p,'C'),
			dt_inicio_triagem_p,
			sysdate,
			ds_queixa_princ_p,
			ds_observacao_p,
			nr_seq_classif_p,
			sysdate,
			cd_estabelecimento_p,
			ie_retriagem_p,
			nr_seq_manchester_fluxo_p,
			obter_dados_usuario_opcao(nm_usuario_p,'C'),
			sysdate,
			nr_seq_discriminador_p,
			ie_nao_p,
			nr_seq_fluxo_original_p,
			'T',
               ds_detalhe_p,
			   ie_clinica_w,
			   cd_setor_atendimento_w,
               nr_seq_pac_senha_fila_w,
               nr_seq_fila_w);
			commit;

			if	(nr_atendimento_p > 0) and
				(nr_seq_classif_p > 0)then
				begin
					gqa_classificacao_risco(nr_seq_classif_p,nr_atendimento_p,nm_usuario_p,nr_regras_atendidas_w,nr_seq_discriminador_p);
                    gera_protocolo_assistencial(nr_atendimento_p,nm_usuario_p); 
				exception
				when others then
					ds_erro_w := substr(sqlerrm,2000);
				end;

			end if;

end if;

exception
when others then
	ds_erro_w := substr(sqlerrm,2000);

	gravar_log_tasy(99995,
					SUBSTR('Erro ao inserir TRIAGEM_PRONTO_ATEND  no manchester = ' || ds_erro_w,1,2000),
					wheb_usuario_pck.get_nm_usuario);
					
	ds_erro_p := Substr('Erro ao inserir TRIAGEM_PRONTO_ATEND  no manchester = ' || ds_erro_w,1,2000);
end;

end manchester_tria_pronto_atend;
/
