create or replace procedure gerar_comunic_atend_req(
			nr_requisicao_material_p		Number,
			nm_usuario_p				Varchar2) is
			
cd_requisitante_w			Number(10,0);
dt_aprovacao_w			Date;
dt_liberacao_w			Date;
nm_pessoa_requisitante_w		Varchar2(60);
dt_solic_req_w			Date;
ds_destinatario_w			Varchar2(255);
ds_solicitante_w			Varchar2(255);
ds_comunicacao_w			Varchar2(4000) := '';
ds_titulo_w			Varchar2(80);
cd_material_w			Number(6);
cd_motivo_baixa_w			Number(10);
qt_material_requisitada_w 		Number(13,4);
qt_material_atendida_w		Number(13,4);
ds_quebra_linha_w			Varchar2(10) := ' \par ';
cd_estabelecimento_w		Number(4);
nr_sequencia_w			Number(10,0);
nr_seq_classif_w			Number(10,0);
nr_requisicao_w			Number(10,0);
ds_justificativa_w			Varchar2(255);
nr_seq_justificativa_w		item_requisicao_material.nr_seq_justificativa%type;
ds_justificativa_item_w		motivo_justif_req.ds_motivo%type;

Cursor C01 is
	select	a.cd_material,
		a.cd_motivo_baixa,
		a.qt_material_requisitada,
		a.qt_material_atendida,
		a.ds_justificativa_atend,
		a.nr_seq_justificativa
	from	item_requisicao_material a
	where	a.nr_requisicao = nr_requisicao_material_p
	order by a.cd_material;
begin

select	a.nr_requisicao,
	a.cd_pessoa_requisitante,
	a.dt_aprovacao,
	a.dt_liberacao,
	a.dt_solicitacao_requisicao,
	a.cd_estabelecimento,
	substr(OBTER_USUARIO_PESSOA(a.cd_pessoa_requisitante),1,255),
	substr(obter_nome_pf(a.cd_pessoa_solicitante),1,255)
into	nr_requisicao_w,
	cd_requisitante_w,
	dt_aprovacao_w,
	dt_liberacao_w,
	dt_solic_req_w,
	cd_estabelecimento_w,
	ds_destinatario_w,
	ds_solicitante_w
from	requisicao_material a
where	a.nr_requisicao = nr_requisicao_material_p;
select	comunic_interna_seq.nextval
into	nr_sequencia_w
from	dual;
select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

nm_pessoa_requisitante_w:= substr(obter_nome_pf(cd_requisitante_w),1,80);

ds_titulo_w		:= WHEB_MENSAGEM_PCK.get_texto(301404,'NR_REQUISICAO_W=' || nr_requisicao_w); 
ds_comunicacao_W	:= substr(ds_comunicacao_W || '{\rtf1\ansi\ansicpg1252\deff0{\fonttbl{\f0\fmodern\fprq1\fcharset0 Courier New;}{\f1\fswiss\fcharset0 Arial;}}
{\*\generator Msftedit 5.41.21.2500;}\viewkind4\uc1\pard\lang1046\f0\fs20 ' || WHEB_MENSAGEM_PCK.get_texto(333213) || ' ' || nr_requisicao_w || ds_quebra_linha_w || ds_quebra_linha_w,1,4000);
ds_comunicacao_W	:= substr(ds_comunicacao_W || rpad(WHEB_MENSAGEM_PCK.get_texto(333214),40) || rpad(WHEB_MENSAGEM_PCK.get_texto(333215),11) || rpad(WHEB_MENSAGEM_PCK.get_texto(333216),11) || rpad(WHEB_MENSAGEM_PCK.get_texto(333217),11) || ds_quebra_linha_w,1,4000);
ds_comunicacao_W	:= substr(ds_comunicacao_w || rpad(nm_pessoa_requisitante_w,40) || 
		rpad(nvl(PKG_DATE_FORMATERS.to_varchar(dt_aprovacao_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),' '),11) || 
		rpad(nvl(PKG_DATE_FORMATERS.to_varchar(dt_liberacao_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),' '),11) || 
		rpad(nvl(PKG_DATE_FORMATERS.to_varchar(dt_solic_req_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),' '),11) || ds_quebra_linha_w || ds_quebra_linha_w,1,4000);

if	(nvl(ds_solicitante_w,'X') <> 'X') then
	ds_comunicacao_W	:= substr(ds_comunicacao_W || rpad(WHEB_MENSAGEM_PCK.get_texto(333214),40) || rpad(WHEB_MENSAGEM_PCK.get_texto(333215),11) || rpad(WHEB_MENSAGEM_PCK.get_texto(333216),11) || rpad(WHEB_MENSAGEM_PCK.get_texto(333217),11) || ds_quebra_linha_w,1,4000);
	ds_comunicacao_W	:= substr(ds_comunicacao_w || rpad(ds_solicitante_w,40) || ds_quebra_linha_w || ds_quebra_linha_w,1,4000);
end if;

ds_comunicacao_W	:= substr(ds_comunicacao_w || rpad(WHEB_MENSAGEM_PCK.get_texto(333218),7) || rpad(WHEB_MENSAGEM_PCK.get_texto(333219),36) || rpad(WHEB_MENSAGEM_PCK.get_texto(333220),20) || rpad(WHEB_MENSAGEM_PCK.get_texto(333221),10) || rpad(WHEB_MENSAGEM_PCK.get_texto(333222),10) || ds_quebra_linha_w,1,4000);

open C01;
loop
fetch C01 into
	cd_material_w,
	cd_motivo_baixa_w,
	qt_material_requisitada_w,
	qt_material_atendida_w,
	ds_justificativa_w,
	nr_seq_justificativa_w;
exit when C01%notfound;
	begin
	if	(nr_seq_justificativa_w is not null) then
		select	ds_motivo
		into	ds_justificativa_item_w
		from	motivo_justif_req
		where	nr_sequencia = nr_seq_justificativa_w;
	end if;

	ds_comunicacao_w	:= substr(ds_comunicacao_w || rpad(cd_material_w,7) || rpad(substr(OBTER_DESC_MATERIAL(cd_material_w),1,35),36),1,4000);
	ds_comunicacao_w	:= substr(ds_comunicacao_w || rpad(nvl(OBTER_DESC_MOTIVO_BAIXA_REQ(cd_motivo_baixa_w),' '),20),1,4000);
	ds_comunicacao_w	:= substr(ds_comunicacao_w || ' ' || rpad(nvl(qt_material_requisitada_w,0),10) || rpad(nvl(qt_material_atendida_w,0),10) || ds_quebra_linha_w,1,4000);
	if (ds_justificativa_w is not null) then
    --Obs: = 502224
		ds_comunicacao_w	:=  substr(ds_comunicacao_w || obter_desc_expressao(502224)||' \i ' || ds_justificativa_w || ' \i0 ' || ds_quebra_linha_w || ds_justificativa_item_w || ' \i0 ' || ds_quebra_linha_w,1,4000);
	end if;
	end;
end loop;
close C01;

ds_comunicacao_w	:= substr(ds_comunicacao_w,1,3985) || '\f1\fs20\par}';

insert	into comunic_interna(
			cd_estab_destino,
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			dt_liberacao)
	values(		cd_estabelecimento_w,
			sysdate,
			ds_titulo_w,
			ds_comunicacao_w,
			nm_usuario_p,
			sysdate,
			'N',
			ds_destinatario_w,
			nr_sequencia_w,
			'N',
			nr_seq_classif_w,
			sysdate);
commit;
end gerar_comunic_atend_req;
/