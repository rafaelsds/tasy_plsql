create or replace
procedure con_troca_plano(	nr_sequencia_p		number,
				nm_usuario_p			varchar2) is


qt_consistencia_w		Number(5);
cd_conta_origem_w		varchar2(20);
cd_conta_destino_w		varchar2(20);
cd_empresa_w			Number(4);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w		date;
nr_seq_contrato_w		number(10,0);
nr_sequencia_w			number(10,0);
ie_acao_w			number(3);
ds_erro_w			varchar2(255);
nr_lote_contabil_w		ctb_troca_plano.nr_lote_contabil%Type;
nr_seq_bem_w			pat_bem.nr_sequencia%type;
nr_seq_regra_conta_w	pat_conta_contabil.nr_sequencia%type;

/*ie_acao_w:
0= Troca o plano de contas
1= Estorna a troca do plano de contas
*/


cursor c01 is
select	b.cd_empresa,
	decode(ie_acao_w, 0, a.cd_conta_origem, a.cd_conta_destino) cd_conta_origem_w,
	decode(ie_acao_w, 0, a.cd_conta_destino, a.cd_conta_origem) cd_conta_destino_w
from	ctb_troca_plano b,
	ctb_troca_plano_conta a
where	a.nr_seq_troca = b.nr_sequencia
and	a.nr_seq_troca = nr_sequencia_p;


cursor c02 is
select	nr_sequencia
from	contrato_regra_nf
where	cd_conta_contabil = cd_conta_origem_w;

Cursor C03 is
select	a.nr_sequencia
from	pat_bem a,
	pat_conta_contabil b
where	a.cd_conta_contabil 	= b.cd_conta_contabil
and 	a.nr_seq_regra_conta 	= b.nr_sequencia
and 	a.cd_conta_contabil 	= cd_conta_origem_w
and	a.ie_situacao		= 'A';


BEGIN

ie_acao_w	:= 0;
select	count(*)
into	ie_acao_w
from	ctb_troca_plano
where	dt_troca_real is not null
and	dt_estorno_troca is null
and	nr_sequencia = nr_sequencia_p;

select	count(*)
into	qt_consistencia_w
from	ctb_troca_plano
where	nr_sequencia = nr_sequencia_p
and	dt_consistencia is not null;
if	(qt_consistencia_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(209594);
end if;

select	count(*)
into	qt_consistencia_w
from	ctb_troca_plano
where	nr_sequencia = nr_sequencia_p
and	dt_consistencia is not null;
if	(qt_consistencia_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(209595);
	
end if;

select 	nvl(max(nr_lote_contabil),0)
into 	nr_lote_contabil_w
from 	ctb_troca_plano
where 	nr_sequencia = nr_sequencia_p;

if	(nvl(nr_lote_contabil_w, 0) <> 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(334403,'NR_LOTE='||nr_lote_contabil_w);
end if;


open C01;
loop
fetch C01 into
	cd_empresa_w,
	cd_conta_origem_w,
	cd_conta_destino_w;
exit when C01%notfound;
	begin

	select	dt_inicio_vigencia,
		dt_fim_vigencia
	into	dt_inicio_vigencia_w,
		dt_fim_vigencia_w
	from	conta_contabil
	where	cd_empresa 		= cd_empresa_w
	and	cd_conta_contabil 	= cd_conta_destino_w;


/*SOMENTE FAZ TROCA NAS TABELAS*/

	begin
	update	pessoa_juridica
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209596);

	end;


	/*begin
	update	pat_conta_contabil
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	nvl(cd_estabelecimento,(
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w)) in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w)
		and   nvl(cd_empresa,cd_empresa_w) = cd_empresa_w; 
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209597);
	end;


	begin
	update	pat_conta_contabil
	set	cd_conta_deprec_acum = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_deprec_acum = cd_conta_origem_w
		and	nvl(cd_estabelecimento,(
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w)) in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w)
		and   nvl(cd_empresa,cd_empresa_w) = cd_empresa_w; 
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209598);

		end;


	begin
	update	pat_conta_contabil
	set	cd_conta_deprec_res = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_deprec_res = cd_conta_origem_w
		and	nvl(cd_estabelecimento,(
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w)) in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w)
		and   nvl(cd_empresa,cd_empresa_w) = cd_empresa_w; 
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209598);
	
	end;


	begin
	update	pat_bem
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	cd_estabelecimento in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w);
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209601);

	end;

	begin
	open C03;
	loop
	fetch C03 into	
		nr_seq_bem_w;
	exit when C03%notfound;
		begin
		begin
		
		select 	nr_sequencia
		into 	nr_seq_regra_conta_w
		from 	(	select 	a.nr_sequencia
					from 	pat_conta_contabil a
					where	a.cd_conta_contabil = cd_conta_destino_w
					and	a.ie_situacao		= 'A'
					order by a.dt_vigencia)
		where rownum = 1;
		exception
		when others then
			nr_seq_regra_conta_w := 0;
		end;
		
		if (nr_seq_regra_conta_w <> 0) then
			pat_alterar_conta_contabil(nr_seq_bem_w, cd_conta_destino_w, sysdate, 0,nr_seq_regra_conta_w, nm_usuario_p);
		end if;
		
		end;
	end loop;
	close C03;
	
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209601);
	end;
	*/
	
	begin
	update	convenio
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209621);
	end;


	begin
	update	sim_regra_proc
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209622);
	end;


	begin
	update	sim_regra_mat
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209624);
	end;


	begin
	update	terceiro
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209625);
	end;

	begin
	update	ctb_orc_cen_regra
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	cd_estabelecimento in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w);
	exception
		when others then
		ds_erro_w	:= sqlerrm(sqlCode);
		wheb_mensagem_pck.exibir_mensagem_abort(209636,'ds_erro_w='||ds_erro_w);	


	end;

	begin
	update	ctb_orc_cen_regra
	set	cd_conta_origem = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_origem = cd_conta_origem_w
	and	cd_estabelecimento in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w);
	exception
		when others then
		ds_erro_w	:= sqlerrm(sqlCode);
		wheb_mensagem_pck.exibir_mensagem_abort(209637,'ds_erro_w='||ds_erro_w);

		end;


	begin
	update	ctb_lib_orc_conta
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	cd_empresa = cd_empresa_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209607);
	end;


	begin
	update	ctb_regra_ticket_medio
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	cd_estabelecimento in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w);
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209608);
	end;


	begin
	update	evento_contabil_param_estab
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	cd_estabelecimento in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w);
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209609);
	end;


	begin
	update	caixa
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w
	and	cd_estabelecimento in (
		select	cd_estabelecimento
		from	estabelecimento
		where	cd_empresa = cd_empresa_w);
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209612);
	end;


	begin
	update regra_repasse_terc_item
	set	cd_conta_contabil = cd_conta_destino_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209613);
	end;


/* CRIA NOVO REGISTRO COM A DATA DE VIGENCIA PARA TROCA - Comentado por Anderson a pedido do F�bio

	begin
	insert into pessoa_jur_conta_cont (
						nr_sequencia,
						cd_cgc,
						cd_conta_contabil,
						dt_atualizacao,
						nm_usuario,
						cd_empresa,
						ie_tipo_conta,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_inicio_vigencia,
						dt_fim_vigencia)
					select	pessoa_jur_conta_cont_seq.nextval,
						cd_cgc,
						cd_conta_destino_w,
						sysdate,
						nm_usuario_p,
						cd_empresa_w,
						ie_tipo_conta,
						sysdate,
						nm_usuario_p,
						dt_inicio_vigencia_w,
						dt_fim_vigencia_w
					from	pessoa_jur_conta_cont
					where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209614);
	end;


	begin
	insert into pessoa_fisica_conta_ctb(
						nr_sequencia,
						cd_pessoa_fisica,
						cd_conta_contabil,
						cd_empresa,
						dt_atualizacao,
						nm_usuario,
						ie_tipo_conta,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_inicio_vigencia,
						dt_fim_vigencia)
					select	pessoa_fisica_conta_ctb_seq.nextval,
						cd_pessoa_fisica,
						cd_conta_destino_w,
						cd_empresa_w,
						sysdate,
						nm_usuario_p,
						ie_tipo_conta,
						sysdate,
						nm_usuario_p,
						dt_inicio_vigencia_w,
						dt_fim_vigencia_w
					from	pessoa_fisica_conta_ctb
					where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209615);
	end;


	begin
	insert into ctb_criterio_rateio_item(
						nr_sequencia,
						nr_seq_criterio,
						dt_atualizacao,
						nm_usuario,
						cd_centro_custo,
						cd_conta_contabil,
						cd_conta_financ,
						pr_rateio,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_inicio_vigencia,
						dt_fim_vigencia)
					select	ctb_criterio_rateio_item_seq.nextval,
						nr_seq_criterio,
						sysdate,
						nm_usuario_p,
						cd_centro_custo,
						cd_conta_destino_w,
						cd_conta_financ,
						pr_rateio,
						sysdate,
						nm_usuario_p,
						dt_inicio_vigencia_w,
						dt_fim_vigencia_w
					from	ctb_criterio_rateio_item
					where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209616);
	end;


	begin
	insert into convenio_conta_contabil(
						nr_sequencia,
						cd_convenio,
						cd_conta_contabil,
						dt_atualizacao,
						nm_usuario,
						ie_tipo_atendimento,
						cd_estabelecimento,     
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_tipo_conta,
						dt_inicio_vigencia,
						dt_fim_vigencia)
					select	convenio_conta_contabil_seq.nextval,
						cd_convenio,
						cd_conta_destino_w,
						sysdate,
						nm_usuario_p,
						ie_tipo_atendimento,
						cd_estabelecimento,
						sysdate,
						nm_usuario_p,
						ie_tipo_conta,
						dt_inicio_vigencia_w,
						dt_fim_vigencia_w
					from	convenio_conta_contabil
					where	cd_conta_contabil = cd_conta_origem_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209617);
	end;


	begin

	open c02;
	loop
		fetch c02 into
			nr_seq_contrato_w;
		exit when c02%notfound;

		begin

		select	contrato_regra_nf_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into contrato_regra_nf(
						nr_sequencia,
						nr_seq_contrato,
						dt_atualizacao,
						nm_usuario,
						cd_material,
						cd_conta_contabil,
						cd_centro_custo,
						nr_seq_crit_rateio,
						ds_complemento,
						ds_observacao,
						nr_seq_conta_financ,
						vl_pagto,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_inicio_vigencia,
						dt_fim_vigencia)
					select	nr_sequencia_w,
						nr_seq_contrato,
						sysdate,
						nm_usuario_p,
						cd_material,
						cd_conta_destino_w,
						cd_centro_custo,
						nr_seq_crit_rateio,
						ds_complemento,
						ds_observacao,
						nr_seq_conta_financ,
						vl_pagto,
						sysdate,
						nm_usuario_p,
						dt_inicio_vigencia_w,
						dt_fim_vigencia_w
					from	contrato_regra_nf
					where	nr_sequencia = nr_seq_contrato_w;

		insert into contrato_regra_pagto_trib(
						nr_sequencia,
						nr_seq_regra_nf,
						cd_tributo,
						dt_atualizacao,
						nm_usuario,
						vl_tributo,
						pr_tributo,
						cd_beneficiario,
						cd_cond_pagto,
						cd_conta_financ,
						nr_seq_trans_reg,
						nr_seq_trans_baixa,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_corpo_item,
						ie_regra_trib,
						cd_darf)
					select	contrato_regra_pagto_trib_seq.nextval,
						nr_sequencia_w,
						cd_tributo,
						sysdate,
						nm_usuario_p,
						vl_tributo,
						pr_tributo,
						cd_beneficiario,
						cd_cond_pagto,
						cd_conta_financ,
						nr_seq_trans_reg,
						nr_seq_trans_baixa,
						sysdate,
						nm_usuario_p,
						ie_corpo_item,
						ie_regra_trib,
						cd_darf
					from	contrato_regra_pagto_trib
					where	nr_seq_regra_nf = nr_seq_contrato_w;
		end;
	end loop;
	close c02;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209618);
	end;


	begin
	insert into tributo_conta_contabil(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_tributo,
						cd_cgc,
						cd_conta_contabil,
						dt_inicio_vigencia,
						dt_fim_vigencia)
				select	tributo_conta_contabil_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_tributo,
						cd_cgc,
						cd_conta_destino_w,
						dt_inicio_vigencia_w,
						dt_fim_vigencia_w
					from	tributo_conta_contabil
					where	cd_conta_contabil = cd_conta_origem_w;

	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(209619);
	end;
*/

	end;
end loop;
close c01;

if	(ie_acao_w = 0) then
	update	ctb_troca_plano
	set	dt_troca_real = sysdate,
		dt_estorno_troca = null,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;
elsif	(ie_acao_w = 1) then
	update	ctb_troca_plano
	set	dt_troca_real = null,
		dt_estorno_troca = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;
end if;

commit;

end con_troca_plano;
/