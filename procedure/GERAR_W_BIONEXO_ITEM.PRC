create or replace
procedure gerar_w_bionexo_item(
				cd_produto_p		varchar2,                      
				nr_artigo_p		varchar2,                        
				qt_quantidade_p		varchar2,  
				nr_seq_cabecalho_p	varchar2,             
				nm_usuario_p            varchar2,
				nr_seq_item_p out varchar2) is
				

nr_seq_item_w	number(10);

			
begin
--apapandini OS613017
select	w_bionexo_item_seq.nextval
into	nr_seq_item_w
from	dual;


insert into w_bionexo_item(	
		cd_produto,                      
		nr_artigo,                      
		qt_quantidade,
		nr_seq_cabecalho,  
		nr_sequencia,           
		nm_usuario,              
		nm_usuario_nrec, 
		dt_atualizacao,          
		dt_atualizacao_nrec   )
values(	cd_produto_p,                      
	nr_artigo_p,                      
	qt_quantidade_p,
	nr_seq_cabecalho_p,  
	nr_seq_item_w,           
	nm_usuario_p,              
	nm_usuario_p, 
	sysdate,          
	sysdate);
	
nr_seq_item_p := nr_seq_item_w;

commit;

end gerar_w_bionexo_item;
/
				
