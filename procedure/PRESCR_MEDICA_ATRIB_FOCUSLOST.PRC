create or replace
procedure PRESCR_MEDICA_ATRIB_FOCUSLOST(	nm_usuario_p			Varchar2,
						cd_setor_atendimento_p		number,
						ie_possui_registro_p		Varchar2,
						ie_modo_novo_edicao_p		Varchar2,
						cd_pessoa_fisica_p		varchar,
						vl_item_p			number,
						nr_seq_item_p			number,
						cd_estabelecimento_p		varchar2,
						nm_atributo_P			varchar2,
						ds_valor_P			varchar2,
						ie_situacao_p			out varchar2,
						ds_texto_p			out varchar2,
						ds_retorno_p			out varchar2,
						ie_retorno_p			out varchar2,
						ds_msg_p			out varchar2,
						cd_unidade_medida_p		varchar2 default null,
						nr_atendimento_p		number default null,
						cd_escala_dor_p			varchar2 default null,
						ie_rn_p				varchar2 default null) is 
						
ie_busca_info_convenio_w varchar2(1) ;
begin
obter_param_usuario(916,644, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_busca_info_convenio_w);

if(ie_possui_registro_p = '1'
	and ie_modo_novo_edicao_p = '1')then
	
	select  max(ie_situacao),
		obter_texto_tasy(83265, 1)
	into	ie_situacao_p,
		ds_texto_p
	from  	setor_atendimento 
	where 	cd_setor_atendimento = cd_setor_atendimento_p;
end if;

if(VL_ITEM_P is not null) then
	consiste_sinal_vital(cd_pessoa_fisica_p,vl_item_p,nr_seq_item_p,ds_retorno_p,ie_retorno_p,cd_unidade_medida_p,nr_atendimento_p,cd_escala_dor_p,ie_rn_p);
end if;

if(ie_busca_info_convenio_w = 'S')then
	SELECT 	exibe_msg_atrib_eup(nm_atributo_P,ds_valor_P,'PRESCR_MEDICA')
	into 	ds_msg_p		
	FROM 	dual;	
end if; 

commit;
end PRESCR_MEDICA_ATRIB_FOCUSLOST;
/