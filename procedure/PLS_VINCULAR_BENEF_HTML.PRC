create or replace
procedure pls_vincular_benef_html
			(	nr_seq_mov_benef_p	pls_mov_mens_benef.nr_sequencia%type,
				nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				ie_opcao_p		varchar2) is

/*	ie_opcao_p
	'T'	= Gerar Mensalidade
*/

begin

if	(ie_opcao_p = 'T') then
	pls_mov_mens_receb_pck.vincular_beneficiario(nr_seq_mov_benef_p, nr_seq_segurado_p, nm_usuario_p, cd_estabelecimento_p);
end if;

end pls_vincular_benef_html;
/
