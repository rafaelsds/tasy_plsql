create or replace
procedure pls_alterar_tipo_guia_prest
				(	nr_seq_prestador_p		number,
					nr_seq_tipo_guia_p		number,
					nr_seq_med_partic_p		number,
					ie_opcao_p			varchar2,
					nm_usuario_p			varchar2) is 
/* ie_opcao_p
	I - Incluir
	E - Excluir
*/

begin

if	(ie_opcao_p	= 'I') then
	insert into pls_tipo_guia_med_partic
				(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_tipo_guia,
					nr_seq_prestador,
					ie_entra_guia_medico,
					ie_entra_portal)
			values	(	pls_tipo_guia_med_partic_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_tipo_guia_p,
					nr_seq_prestador_p,
					'S',
					'S');
elsif	(ie_opcao_p	= 'E') then
	delete from pls_tipo_guia_med_partic where nr_sequencia = nr_seq_med_partic_p;
end if;

commit;

end pls_alterar_tipo_guia_prest;
/