create or replace
procedure pls_consistir_pf_sib
			(	cd_pessoa_fisica_p			varchar2,
				nm_mae_benef_p			varchar2,
				nm_beneficiario_p			varchar2,
				dt_nascimento_p			date,
				ie_sexo_p			varchar2,
				nr_cpf_p				varchar2,
				nr_pis_pasep_p			varchar2,
				nr_cartao_nac_sus_p		varchar2,
				cd_declaracao_nasc_vivo_p		varchar2,
				cd_nacionalidade_p		varchar2,
				nr_reg_geral_estrang_p		varchar2,
				nr_identidade_p			varchar2,
				ds_orgao_emissor_ci_p		pessoa_fisica.ds_orgao_emissor_ci%type,
				nr_seq_pais_p			varchar2,
				nr_cep_p				varchar2,
				uf_p				varchar2,
				ds_municipio_p			varchar2,
				ds_logradouro_p			varchar2,
				sg_emissora_ci_p			varchar2,
				is_complemento_p    		varchar2,
				nm_usuario_p			varchar2,
				ds_retorno_p	out		varchar2) is

ds_erro_w      varchar2(4000)  := null;
ds_retorno_w    varchar2(255)  := null;

begin

pls_consistir_pf_sib_pai(cd_pessoa_fisica_p, nm_mae_benef_p, nm_beneficiario_p, dt_nascimento_p, ie_sexo_p, nr_cpf_p, nr_pis_pasep_p, nr_cartao_nac_sus_p, 
              cd_declaracao_nasc_vivo_p, cd_nacionalidade_p, nr_reg_geral_estrang_p, nr_identidade_p, ds_orgao_emissor_ci_p, nr_seq_pais_p, nr_cep_p,
              uf_p, ds_municipio_p, ds_logradouro_p, sg_emissora_ci_p, nm_usuario_p, ds_retorno_w);
ds_erro_w := ds_retorno_w;
if(nvl(is_complemento_p, 'N') = 'S') then
	ds_retorno_w := '';
	pls_consistir_pf_sib_comp(cd_pessoa_fisica_p, nm_mae_benef_p, nm_beneficiario_p, dt_nascimento_p, ie_sexo_p, nr_cpf_p, nr_pis_pasep_p, nr_cartao_nac_sus_p, 
                    cd_declaracao_nasc_vivo_p, cd_nacionalidade_p, nr_reg_geral_estrang_p, nr_identidade_p, ds_orgao_emissor_ci_p, nr_seq_pais_p, nr_cep_p,
                    uf_p, ds_municipio_p, ds_logradouro_p, sg_emissora_ci_p, nm_usuario_p, ds_retorno_w);
	if	(ds_erro_w is null) then
		ds_erro_w := ds_retorno_w;
	else
		ds_erro_w	:= ds_erro_w || chr(13) || chr(13) || chr(10) || chr(10) || ds_retorno_w;
	end if;
end if;

ds_retorno_p := substr(ds_erro_w,1,4000);

end pls_consistir_pf_sib;
/
