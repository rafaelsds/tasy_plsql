create or replace procedure atualizar_funcoes_html5 is
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Atualiza Dashboard HTML5
---------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [X] Outros: Bot�o direito Atualizar Dados no Dashboard HTML5
 --------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin
	
	dashboard_html5_pck.atualizar_dashboard;
	
end atualizar_funcoes_html5;
/
