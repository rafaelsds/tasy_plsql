create or replace
procedure fis_reg_ecf_J100(	nr_seq_controle_p	varchar2,
				ds_separador_p		varchar2,
				cd_estabelecimento_p	varchar2,
				nm_usuario_p		varchar2,
				cd_empresa_p		varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number,
				ie_scp_p		varchar2) is 

				
nr_seq_registro_w	number(10) := nr_sequencia_p;	
nr_linha_w		number(10) := qt_linha_p;
ds_arquivo_w		varchar2(4000);	
ds_arquivo_compl_w	varchar2(4000);	
ds_linha_w		varchar2(4000);
sep_w			varchar2(2) := ds_separador_p;

tp_registro_w		varchar2(5);
dt_inicio_apuracao_w	date;
dt_atualizacao_w	date;
cd_centro_custo_w	number(8);
ds_centro_custo_w	varchar2(80);

cursor c01 is
select	trunc(dt_inicio_apuracao_w,'year') dt_atualizacao,
	a.cd_centro_custo,
	a.ds_centro_custo
from	estabelecimento b,
	centro_custo a
where	a.cd_estabelecimento		= b.cd_estabelecimento
and    (((ie_scp_p = 'S') 
       and (b.cd_estabelecimento 	= cd_estabelecimento_p))
or     (ie_scp_p = 'N') 
       and (b.cd_estabelecimento 	in (	select cd_estabelecimento
						from   estabelecimento
						where  nvl(ie_scp, 'N') = 'N')))
and	b.cd_empresa			= cd_empresa_p;

begin

select  max(dt_inicio_apuracao)
into	dt_inicio_apuracao_w
from	fis_controle_ecf
where	nr_sequencia 		= nr_seq_controle_p;

open C01;
loop
fetch C01 into	
	dt_atualizacao_w,
	cd_centro_custo_w,
	ds_centro_custo_w;	
exit when C01%notfound;
	begin
	tp_registro_w		:= 'J100';
	
	ds_linha_w	:= substr(	sep_w || tp_registro_w 				|| -- Registro (1)
					sep_w || to_char(dt_atualizacao_w,'ddmmyyyy') 	|| -- Data da Alterao (Incluso/Alterao) (2)
					sep_w || cd_centro_custo_w 			|| -- Cdigo do Centro de Custos (3)
					sep_w || ds_centro_custo_w 			|| sep_w,1,8000); -- Nome do Centro de Custos (4)
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;
	
	insert into fis_ecf_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_ecf,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		tp_registro_w,
		ds_arquivo_w,
		ds_arquivo_compl_w);
	
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

fis_reg_ecf_J990(nr_seq_controle_p,ds_separador_p,cd_estabelecimento_p,nm_usuario_p,cd_empresa_p,qt_linha_p,nr_sequencia_p);

end fis_reg_ecf_J100;
/