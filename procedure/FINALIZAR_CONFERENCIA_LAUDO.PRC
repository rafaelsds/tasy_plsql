create or replace
procedure finalizar_conferencia_laudo(	nr_prescricao_p 	number,
					nr_seq_prescr_p		number,
					nm_usuario_p		Varchar2) is 
begin

if (nr_prescricao_p is not null) and	
   (nr_seq_prescr_p is not null) then

	update	prescr_procedimento
	set	ie_status_execucao = 44,
		dt_atualizacao = sysdate,
		nm_usuario     = nm_usuario_p
	where	nr_prescricao  = nr_prescricao_p
	and	nr_sequencia   = nr_seq_prescr_p;

	commit;
	
end if;	

end finalizar_conferencia_laudo;
/