create or replace
procedure com_gravar_modulo_sd_js(	
					ds_lista_modulos_p	varchar2,
					nr_seq_solic_p		number,
					nm_usuario_p		varchar2) is 

nr_seq_modulo_w		number(10);
tam_lista_w		number(10);
ie_pos_virgula_w	number(3) := 0;	
ds_lista_modulos_w	varchar2(800);	
qt_tempo_preparo_w      number(10);
qt_tempo_execucao_w     number(10);	
nr_seq_tipo_abordagem_w number(10);		
					
begin

ds_lista_modulos_w := substr(ds_lista_modulos_p,1,800);

if	(nvl(nr_seq_solic_p,0) > 0) and
	(ds_lista_modulos_w is not null) then
	
	select	max(nr_seq_tipo_abordagem)
	into	nr_seq_tipo_abordagem_w
	from	com_solic_sd
	where	nr_sequencia = nr_seq_solic_p;
	
	WHILE	(ds_lista_modulos_w is not null) LOOP
		begin
		
		tam_lista_w			:=	length(ds_lista_modulos_w);
		ie_pos_virgula_w		:=	instr(ds_lista_modulos_w,',');
		
		if	(ie_pos_virgula_w <> 0) then
			nr_seq_modulo_w		:= substr(ds_lista_modulos_w,1,(ie_pos_virgula_w - 1));
			ds_lista_modulos_w	:= substr(ds_lista_modulos_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;
		
		select	nvl(max(qt_tempo_preparo),0),
			nvl(max(qt_tempo_execucao),0)
		into	qt_tempo_preparo_w,
			qt_tempo_execucao_w
		from	com_tempo_demonstracao
		where	nr_seq_modulo = nr_seq_modulo_w;
		
		insert into com_solic_sd_mod(nr_sequencia,
						nr_seq_mod_impl,
						nr_seq_solic_sd,
						nm_usuario,
						dt_atualizacao,
						dt_atualizacao_nrec,    
						nm_usuario_nrec,
						qt_tempo_preparo,       
						qt_tempo_execucao)
			values(com_solic_sd_mod_seq.nextval,
				nr_seq_modulo_w,
				nr_seq_solic_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				nm_usuario_p,
				qt_tempo_preparo_w,
				qt_tempo_execucao_w);
		end;
	end loop;
end if;

commit;

end com_gravar_modulo_sd_js;
/