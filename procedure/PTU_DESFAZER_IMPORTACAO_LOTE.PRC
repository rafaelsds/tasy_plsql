create or replace
procedure ptu_desfazer_importacao_lote
			(	nr_seq_intercambio_p	number) is 

qt_registro_confirmado_w	number(10);

Cursor C01 is
	select	b.nr_sequencia nr_seq_beneficiario
	from	ptu_intercambio_benef		b,
		ptu_intercambio_empresa		a
	where	a.nr_sequencia = b.nr_seq_empresa
	and	a.nr_seq_intercambio = nr_seq_intercambio_p;

Cursor C02 is
	select	nr_sequencia nr_seq_empresa
	from	ptu_intercambio_empresa
	where	nr_seq_intercambio = nr_seq_intercambio_p;

begin

select 	count(1)
into	qt_registro_confirmado_w
from   	ptu_retorno_movimentacao a,
	ptu_retorno_mov_benef b
where  	a.nr_sequencia = b.nr_seq_retorno_mov
and    	a.nr_seq_intercambio = nr_seq_intercambio_p
and    	a.dt_confirmacao_envio is not null;

if	(qt_registro_confirmado_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1063335);
else
	delete from ptu_retorno_mov_benef
	where	nr_sequencia in (	select	b.nr_sequencia
					from   	ptu_retorno_movimentacao a,
						ptu_retorno_mov_benef b
					where  	a.nr_sequencia = b.nr_seq_retorno_mov
					and    	a.nr_seq_intercambio = nr_seq_intercambio_p);
					
	delete from ptu_retorno_movimentacao
	where	nr_seq_intercambio = nr_seq_intercambio_p;
end if;

delete	from	ptu_intercambio_benef_simp
where	nr_seq_intercambio	= nr_seq_intercambio_p;


for r_c01_w in C01 loop
	begin
	delete	from ptu_beneficiario_carencia
	where	nr_seq_beneficiario = r_c01_w.nr_seq_beneficiario;
	
	delete	from ptu_benef_plano_agregado
	where	nr_seq_beneficiario = r_c01_w.nr_seq_beneficiario;
	
	delete	from ptu_beneficiario_compl
	where	nr_seq_beneficiario = r_c01_w.nr_seq_beneficiario;
	
	delete	from ptu_beneficiario_contato
	where	nr_seq_beneficiario = r_c01_w.nr_seq_beneficiario;
	
	delete	from ptu_benef_preexistencia
	where	nr_seq_beneficiario = r_c01_w.nr_seq_beneficiario;
	
	delete	ptu_intercambio_consist
	where	nr_seq_inter_benef = r_c01_w.nr_seq_beneficiario;
	end;
end loop; --C01

for r_c02_w in C02 loop
	begin
	delete 	from ptu_intercambio_benef
	where	nr_seq_empresa = r_c02_w.nr_seq_empresa;
	
	delete 	from ptu_intercambio_plano
	where	nr_seq_empresa = r_c02_w.nr_seq_empresa;
	end;
end loop; --C02

delete	from ptu_intercambio_empresa
where	nr_seq_intercambio = nr_seq_intercambio_p;
	
delete	from ptu_intercambio
where	nr_sequencia = nr_seq_intercambio_p;

commit;

end ptu_desfazer_importacao_lote;
/