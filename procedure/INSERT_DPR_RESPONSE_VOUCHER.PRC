create or replace
procedure insert_dpr_response_voucher(  nr_seq_response_p number,
					dt_service_p date,
					ie_medicare_card_p varchar2,
					nm_family_p varchar2,
					nm_first_p varchar2,
					cd_medicare_card_p number,
					nr_patient_ref_p number,
					cd_provider_p varchar2,
					nr_voucher_p number, 
					nm_usuario_p varchar2) is

begin

insert 	into 	dpr_response_voucher (
		dt_service,
		ie_medicare_card,
		nm_family,
		nm_first,
		cd_medicare_card, 
		nr_patient_ref,
		cd_provider,
		nr_voucher,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_response)
values (	dt_service_p,         
		ie_medicare_card_p,
		nm_family_p,
		nm_first_p,
		cd_medicare_card_p,
		nr_patient_ref_p,
		cd_provider_p, 
		nr_voucher_p,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		dpr_response_voucher_seq.nextval,
		nr_seq_response_p);

commit;

end insert_dpr_response_voucher;
/