create or replace
procedure pls_gerar_w_analise_pos_part
			(	nr_seq_analise_p		number,
				nr_seq_conta_proc_p		number,
				nr_seq_grupo_p			number,
				ie_minha_analise_p		varchar2,
				ie_pendentes_p			varchar2,
				nm_usuario_p			varchar2,
				nr_id_transacao_p		w_pls_analise_item.nr_id_transacao%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar os participantes - An�lise de P�s-estabelecido
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_item_w			varchar2(255);
nm_medico_w			varchar2(255);
ds_via_acesso_w			varchar2(255);
ds_tipo_guia_w			varchar2(255);
nm_prestador_solic_w		varchar2(255);
nm_prestador_exec_w		varchar2(255);
nm_prestador_pag_w		varchar2(255);
ds_fornecedor_w			varchar2(255);
ds_setor_atend_w		varchar2(255);
ds_item_importacao_w		varchar2(255);
ds_espec_cbo_w			varchar2(255);
ds_medico_executor_w		varchar2(255);
ds_grau_partic_w		varchar2(255);
ds_status_item_w		varchar2(255);
ie_glosa_w			varchar2(255);
nm_medico_executor_imp_w	varchar2(255);
ds_procedimento_w		procedimento.ds_procedimento%TYPE;
ds_ident_nivel_1_w		varchar2(60);
ds_ident_nivel_2_w		varchar2(60);
ds_ident_nivel_3_w		varchar2(60);
ds_identacao_w			varchar2(30);
cd_guia_w			varchar2(30);
cd_guia_conta_w			varchar2(30);
ie_autorizado_w			varchar2(20);
cd_medico_executor_w		varchar2(20);
ie_tipo_guia_conta_w		varchar2(20);
ie_tipo_linha_w			varchar2(10);
ie_tipo_despesa_w		varchar2(10);
ie_tipo_item_w			varchar2(10);
cd_medico_w			varchar2(10);
ie_via_acesso_w			varchar2(10);
cd_porte_anestesico_w		varchar2(10);
ie_status_analise_w		varchar2(3)	:= 'S'; /* Analisado pelo sistema */
ie_pagamento_w			varchar2(3)	:= null;
ie_status_item_w		varchar2(2);
ie_tipo_guia_w			varchar2(2);
ie_expandido_w			varchar2(1)	:= null;
ie_pend_grupo_w			varchar2(1)	:= null;
ie_minha_analise_w		varchar2(1)	:= 'N';
ie_pendentes_w			varchar2(1)	:= 'N';
ie_nao_finalizar_w		varchar2(1)	:= null;
ie_item_nao_encontrado_w	varchar2(1);
ie_proc_ref_w			varchar2(1);
ie_selecionado_w		varchar2(1)	:= 'N';
ie_sem_fluxo_w			varchar2(1)	:= null;
ie_faturamento_w		varchar2(1);
vl_taxa_servico_imp_w		number(15,4);
vl_taxa_co_imp_w		number(15,4);
vl_taxa_material_imp_w		number(15,4);
vl_taxa_servico_w		number(15,4);
vl_taxa_co_w			number(15,4);
vl_taxa_material_w		number(15,4);
vl_procedimento_imp_w		number(15,2);
vl_proc_unitario_w		number(15,2);
vl_procedimento_w		number(15,2);
vl_unitario_imp_w		number(15,2);
vl_glosa_w			number(15,2);
vl_liberado_w			number(15,2);
vl_glosa_material_w		number(15,2);
vl_glosa_hi_w			number(15,2);
vl_glosa_co_w			number(15,2);
vl_custo_operacional_w		number(15,2);
vl_liberado_material_w		number(15,2);
vl_liberado_co_w		number(15,2);
vl_liberado_hi_w		number(15,2);
vl_calculado_material_w		number(15,2);
vl_calculado_co_w		number(15,2);
vl_calculado_hi_w		number(15,2);
vl_taxa_intercambio_w		number(15,2);
vl_taxa_intercambio_imp_w	number(15,2);
vl_unitario_apres_w		number(15,2);
vl_base_w			number(15,2);
vl_proc_pag_calc_w		number(15,2);
vl_proc_pag_apres_w		number(15,2);
vl_proc_pag_lib_w		number(15,2);
vl_proc_pag_glosa_w		number(15,2);
cd_procedimento_w		number(15);
qt_liberado_w			number(12,4);
qt_procedimento_imp_w		number(12,4);
qt_liberar_w			number(12,4);
qt_proc_pag_w			number(12,4);
qt_proc_pag_lib_w		number(12,4);
nr_seq_conta_proc_w		number(10);
nr_seq_proc_partic_w		number(10);
nr_seq_prestador_pgto_w		number(10);
nr_seq_proc_pai_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_guia_w			number(10);
nr_seq_prestador_exec_w		number(10);
nr_seq_protocolo_w		number(10);
nr_seq_prest_fornec_w		number(10);
nr_seq_setor_atend_w		number(10);
nr_auxiliares_w			number(10);
nr_seq_grau_partic_w		number(10);
nr_sequencia_w			number(10)	:= 0;
qt_informativo_w		number(10);
nr_seq_proc_ref_w		number(10);
nr_seq_prestador_honor_w	number(10);
nr_seq_conta_w			number(10);
nr_seq_protocolo_conta_w	number(10);
nr_seq_prestador_solic_w	number(10);
nr_seq_cbo_saude_w		number(10);
qt_selecao_w			number(10);
nr_id_analise_w			number(10);
nr_seq_agrup_analise_w		number(10);
tx_item_w			number(9,3);
tx_reducao_acrescimo_w		number(7,4);
tx_intercambio_w		number(7,4);
tx_intercambio_imp_w		number(7,4);
qt_fluxo_analise_w		number(5);
ie_valor_base_w			number(3);
nr_nivel_w			number(3)	:= 1;
dt_procedimento_w		date;
dt_atend_ref_conta_w		date;
vl_participante_pos_w		number(15,2);
vl_liberado_partic_w		number(15,2);
ie_status_faturamento_w		varchar2(10);
qt_original_w			number(12,4);
qt_item_w			number(12,4);
nr_seq_conta_pos_w		number(10);
nr_seq_disc_proc_w		pls_conta_pos_estabelecido.nr_seq_disc_proc%type;
qt_negada_w			pls_discussao_proc.qt_negada%type;
vl_negado_w			pls_discussao_proc.vl_negado%type;
tx_administracao_w		pls_conta_pos_estabelecido.tx_administracao%type;
vl_liberado_pag_w		pls_proc_participante.vl_participante%type;
ie_aviso_a520_part_w		pls_conta_proc_regra.ie_a520%type;

cursor C03 is
	select	a.nr_sequencia,
		b.nr_seq_conta,
		c.nr_seq_protocolo,
		a.nr_seq_conta_proc,
		'IC', /* Item da conta */
		'R', /* Participante */
		b.ie_tipo_despesa,
		b.qt_procedimento_imp,
		nvl(a.vl_apresentado,0),
		0,
		a.vl_calculado,
		nvl(d.cd_medico, a.cd_medico) cd_medico,
		a.nr_seq_prestador,
		a.nr_seq_conta_proc,
		b.cd_procedimento,
		b.ie_origem_proced,
		null,
		null cd_porte_anestesico,
		b.nr_seq_setor_atend,
		substr(b.cd_procedimento_imp || ' - ' || b.ds_procedimento_imp,1,255),
		0 vl_glosa_material,
		a.vl_glosa vl_glosa_hi,
		0 vl_glosa_co,
		0 vl_custo_operacional,
		0 vl_liberado_material,
		0 vl_liberado_co,
		a.vl_participante vl_liberado_hi,
		0 vl_materiais,
		0 vl_custo_operacional,
		0 vl_total_partic,
		0 tx_intercambio,
		0 tx_intercambio_imp,
		b.qt_procedimento qt_procedimento,
		0 tx_item,
		a.vl_glosa,
		d.vl_participante_pos,
		b.vl_unitario_imp,
		b.ie_valor_base,
		nvl(d.nr_seq_grau_partic, a.nr_seq_grau_partic) nr_seq_grau_partic,
		0 vl_taxa_servico_imp,
		0 vl_taxa_co_imp,
		0 vl_taxa_material_imp,
		0 vl_taxa_servico,
		0 vl_taxa_co,
		0 vl_taxa_material,
		a.nm_medico_executor_imp,
		b.qt_procedimento_imp,
		b.qt_procedimento,
		a.vl_calculado,
		nvl(a.vl_digitado_complemento,a.vl_calculado),
		a.vl_participante,
		a.vl_glosa,
		b.ie_glosa,
		a.nr_id_analise,
		b.nr_seq_agrup_analise,
		d.vl_participante_pos,
		d.vl_liberado,
		e.ie_status_faturamento,
		e.qt_original,
		b.qt_procedimento,
		e.nr_sequencia,
		b.ie_status_pagamento,
		e.nr_seq_disc_proc,
		e.tx_administracao,
		a.vl_participante,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= b.nr_sequencia) ie_a520
	from	pls_conta		c,
		pls_conta_pos_estabelecido e,
		pls_conta_pos_estab_partic d,
		pls_proc_participante	a,
		pls_conta_proc		b
	where	c.nr_sequencia		= b.nr_seq_conta
	and	a.nr_seq_conta_proc	= b.nr_sequencia
	and	d.nr_seq_proc_partic	= a.nr_sequencia
	and	d.nr_seq_conta_pos	= e.nr_sequencia
	and	e.nr_seq_conta_proc	= b.nr_sequencia
	and	a.nr_seq_conta_proc	= e.nr_seq_conta_proc
	and	b.nr_sequencia		= nr_seq_conta_proc_p
	and	e.ie_situacao		= 'A'
	and	not exists	(select	1
				from	pls_conta_proc	x
				where	x.nr_seq_participante_hi	= a.nr_sequencia)
	and	((nr_nivel_w > 2) or (exists	(select	1
						from	w_pls_analise_item_exp	x
						where	x.nr_seq_conta_proc	= b.nr_sequencia)))
	and	e.ie_status_faturamento	!= 'A'
	and	e.ie_cobrar_mensalidade	!= 'A';

begin
ie_minha_analise_w	:= nvl(ie_minha_analise_p,'N');
ie_pendentes_w		:= nvl(ie_pendentes_p,'N');
ds_identacao_w		:= '       ';
ds_ident_nivel_1_w	:= ds_identacao_w;
ds_ident_nivel_2_w	:= ds_identacao_w || ds_identacao_w;
ds_ident_nivel_3_w	:= ds_identacao_w || ds_identacao_w || ds_identacao_w;
nr_nivel_w		:= pls_consulta_analise_pos_pck.get_nr_nivel;
ie_tipo_guia_w		:= pls_consulta_analise_pos_pck.get_ie_tipo_guia;

select	a.ie_status,
	a.ie_via_acesso,
	a.nr_seq_conta
into	ie_status_item_w,
	ie_via_acesso_w,
	nr_seq_conta_w
from	pls_conta_proc	a
where	a.nr_sequencia	=  nr_seq_conta_proc_p;

select	a.nr_seq_guia,
	a.nr_seq_protocolo,
	a.nr_seq_prestador,
	a.nr_seq_prestador_exec,
	a.dt_atendimento_referencia,
	a.nr_seq_cbo_saude,
	a.cd_medico_executor,
	a.cd_guia,
	a.ie_tipo_guia
into	nr_seq_guia_w,
	nr_seq_protocolo_w,
	nr_seq_prestador_solic_w,
	nr_seq_prestador_exec_w,
	dt_atend_ref_conta_w,
	nr_seq_cbo_saude_w,
	cd_medico_executor_w,
	cd_guia_conta_w,
	ie_tipo_guia_conta_w
from	pls_conta a
where	a.nr_sequencia	= nr_seq_conta_w;

nm_prestador_solic_w	:= substr(pls_obter_dados_prestador(nr_seq_prestador_solic_w,'N'),1,255);
nm_prestador_exec_w	:= substr(pls_obter_dados_prestador(nr_seq_prestador_exec_w,'N'),1,255);

if	(nr_seq_guia_w is not null) then
	select	obter_valor_dominio(1746,a.ie_tipo_guia),
		cd_guia,
		decode(ie_status, 1, 'S', 'N')
	into	ds_tipo_guia_w,
		cd_guia_w,
		ie_autorizado_w
	from	pls_guia_plano a
	where	nr_sequencia = nr_seq_guia_w;
else
	cd_guia_w	:= cd_guia_conta_w;
	ds_tipo_guia_w	:= obter_valor_dominio(1746,ie_tipo_guia_conta_w);
end if;

if	(nr_seq_cbo_saude_w is not null) then
	select	max(a.ds_cbo)
	into	ds_espec_cbo_w
	from	cbo_saude	a
	where	a.nr_sequencia	= nr_seq_cbo_saude_w;
end if;

if	(cd_medico_executor_w is not null) then
	select	substr(nm_pessoa_fisica,1,255)
	into	ds_medico_executor_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_medico_executor_w;
end if;

if	(ie_status_item_w is not null) then
	select	ds_valor_dominio
	into	ds_status_item_w
	from	valor_dominio_v
	where	cd_dominio	= 1870
	and	vl_dominio	= ie_status_item_w;
end if;

if	(ie_via_acesso_w is not null) then
	select	ds_valor_dominio
	into	ds_via_acesso_w
	from	valor_dominio_v
	where	cd_dominio	= 1268
	and	vl_dominio	= ie_via_acesso_w;
end if;

open C03;
loop
fetch C03 into
	nr_seq_proc_partic_w,
	nr_seq_conta_w,
	nr_seq_protocolo_w,
	nr_seq_conta_proc_w,
	ie_tipo_linha_w,
	ie_tipo_item_w,
	ie_tipo_despesa_w,
	qt_procedimento_imp_w,
	vl_procedimento_imp_w,
	vl_proc_unitario_w,
	vl_procedimento_w,
	cd_medico_w,
	nr_seq_prestador_pgto_w,
	nr_seq_proc_pai_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_auxiliares_w,
	cd_porte_anestesico_w,
	nr_seq_setor_atend_w,
	ds_item_importacao_w,
	vl_glosa_material_w,
	vl_glosa_hi_w,
	vl_glosa_co_w,
	vl_custo_operacional_w,
	vl_liberado_material_w,
	vl_liberado_co_w,
	vl_liberado_hi_w,
	vl_calculado_material_w,
	vl_calculado_co_w,
	vl_calculado_hi_w,
	tx_intercambio_w,
	tx_intercambio_imp_w,
	qt_liberado_w,
	tx_item_w,
	vl_glosa_w,
	vl_liberado_w,
	vl_unitario_apres_w,
	ie_valor_base_w,
	nr_seq_grau_partic_w,
	vl_taxa_servico_imp_w,
	vl_taxa_co_imp_w,
	vl_taxa_material_imp_w,
	vl_taxa_servico_w,
	vl_taxa_co_w,
	vl_taxa_material_w,
	nm_medico_executor_imp_w,
	qt_proc_pag_w,
	qt_proc_pag_lib_w,
	vl_proc_pag_calc_w,
	vl_proc_pag_apres_w,
	vl_proc_pag_lib_w,
	vl_proc_pag_glosa_w,
	ie_glosa_w,
	nr_id_analise_w,
	nr_seq_agrup_analise_w,
	vl_participante_pos_w,
	vl_liberado_partic_w,
	ie_status_faturamento_w,
	qt_original_w,
	qt_item_w,
	nr_seq_conta_pos_w,
	ie_pagamento_w,
	nr_seq_disc_proc_w,
	tx_administracao_w,
	vl_liberado_pag_w,
	ie_aviso_a520_part_w;
exit when C03%notfound;
	begin
	ds_via_acesso_w		:= null;
	ds_grau_partic_w	:= null;		
	
	ie_pend_grupo_w		:= ie_pend_grupo_w;
	ie_pend_grupo_w		:= null;
	
	if	(((ie_minha_analise_w = 'S') and
		(ie_pend_grupo_w is not null)) or
		(ie_minha_analise_w = 'N')) and
		((ie_pendentes_w = ie_pend_grupo_w) or
		(ie_pendentes_w = 'N')) then
		
		select	count(1)
		into	qt_informativo_w
		from	w_pls_analise_item a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.ie_status_analise	= 'I'
		and	a.nr_seq_conta_proc	= nr_seq_agrup_analise_w
		and	a.nr_id_transacao = nr_id_transacao_p;
		
		/*ds_item_w			:= pls_obter_desc_coluna_analise('R','DS_ITEM',null,null,cd_medico_w);*/
		if	(cd_medico_w is not null) then
			ds_item_w	:= obter_nome_medico(cd_medico_w,'NC');
		else
			if	(nm_medico_executor_imp_w is not null) then
				ds_item_w	:= nm_medico_executor_imp_w;
			else
				ds_item_w	:= 'Sem profissional informado';
			end if;
		end if;
		
		ds_item_w	:= 'Partic: ' || ds_item_w;
		
		if	(qt_informativo_w = 0) then
			ds_item_w			:= ds_ident_nivel_2_w || ds_item_w;
		else
			ds_item_w			:= ds_ident_nivel_3_w || ds_item_w;
		end if;
		
		--ds_status_item_w		:= obter_valor_dominio(1870,ie_status_item_w);
		ds_setor_atend_w		:= pls_obter_ds_setor_atend(nr_seq_setor_atend_w);
		vl_taxa_intercambio_imp_w	:= nvl(vl_taxa_servico_imp_w,0) + nvl(vl_taxa_co_imp_w,0) + nvl(vl_taxa_material_imp_w,0);
		vl_taxa_intercambio_w		:= nvl(vl_taxa_servico_w,0) + nvl(vl_taxa_co_w,0) + nvl(vl_taxa_material_w,0);

		if	(nr_seq_grau_partic_w is not null) then
			select	substr(ds_grau_participacao,1,255)
			into	ds_grau_partic_w
			from	pls_grau_participacao
			where	nr_sequencia	= nr_seq_grau_partic_w;
		end if;

		/* Obter status geral de an�lise do item */
		--ie_status_analise_w	:= pls_obter_status_analise_pos(nr_seq_analise_p,nr_seq_conta_pos_w, ie_item_nao_encontrado_w);
		/* Por enquanto n�o tem ocorr�ncia para participante p�s, vai ser sempre verde */
		ie_status_analise_w	:= 'V';
		
		/* Obter se possui fluxo de an�lise */
		ie_sem_fluxo_w		:= pls_obter_se_item_sem_fluxo(nr_seq_analise_p,null, null, null,nr_seq_proc_partic_w);
		
		/* Verificar status de pagamento */
	
		if	(nr_seq_disc_proc_w	is null) and
			(ie_pagamento_w 	is null) then
			ie_pagamento_w	:= pls_analise_obter_status_pag(ie_status_item_w,qt_proc_pag_w,qt_proc_pag_lib_w,
							vl_proc_pag_apres_w,vl_proc_pag_calc_w,vl_proc_pag_lib_w,vl_proc_pag_glosa_w, ie_glosa_w);
		elsif	(nr_seq_disc_proc_w	is not null) then
			select	max(qt_negada),
				max(vl_negado)
			into	qt_negada_w,
				vl_negado_w
			from	pls_discussao_proc
			where	nr_sequencia	= nr_seq_disc_proc_w;
			
			if	(qt_negada_w	> 0) or
				(vl_negado_w 	> 0) then
				ie_pagamento_w	:= 'P';
			else
				ie_pagamento_w	:= 'L';
			end if;
		end if;
			
		ie_faturamento_w	:= pls_analise_obter_status_fat(ie_status_faturamento_w,qt_original_w,qt_item_w,vl_participante_pos_w,vl_liberado_partic_w);

		nr_sequencia_w	:= pls_consulta_analise_pos_pck.get_nr_seq_item;
		
		qt_liberar_w	:= null;
		
		select	count(1),
			max(qt_liberar)
		into	qt_selecao_w,
			qt_liberar_w
		from	w_pls_analise_selecao_item	a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_w_item		= nr_sequencia_w;
		
		if	(qt_selecao_w > 0) then
			ie_selecionado_w	:= 'S';
		else
			ie_selecionado_w	:= 'N';
		end if;
		
		select	max(ie_faturamento)
		into	ie_faturamento_w
		from	w_pls_analise_item
		where	nr_seq_conta_proc	= nr_seq_proc_pai_w
		and		nr_id_transacao	= nr_id_transacao_p;
		
		insert into w_pls_analise_item
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_analise,
			nr_seq_conta,
			nr_seq_conta_proc,
			nr_seq_proc_partic,
			ds_item,
			ie_tipo_despesa,
			ie_tipo_linha,
			ie_tipo_item,
			qt_apresentada,
			vl_apresentado,
			vl_calculado_unitario,
			vl_calculado,
			nr_seq_proc_pai,
			cd_procedimento,
			ie_origem_proced,
			ie_via_acesso,
			ds_via_acesso,
			nm_prestador_solic,
			nr_auxiliares,
			cd_porte_anestesico,
			ds_tipo_guia,
			nm_prestador_exec,
			nm_prestador_pag,
			ds_espec_cbo,
			nr_seq_protocolo,
			ie_autoriz_previa,
			cd_guia,
			ds_setor_atend,
			ds_item_importacao,
			ds_medico_executor,
			vl_glosa_material,
			vl_glosa_hi,
			vl_glosa_co,
			vl_custo_operacional,
			vl_liberado_material,
			vl_liberado_co,
			vl_liberado_hi,
			vl_calculado_material,
			vl_calculado_co,
			vl_calculado_hi,
			tx_intercambio,
			tx_intercambio_imp,
			vl_taxa_intercambio,
			vl_taxa_intercambio_imp,
			qt_liberado,
			tx_item,
			vl_glosa,
			vl_liberado,
			vl_unitario_apres,
			nr_seq_prestador_exec,
			nr_seq_guia,
			ie_status_analise,
			ie_pagamento,
			ie_valor_base,
			ds_grau_partic,
			ie_status_item,
			ds_status_item,
			ie_pend_grupo,
			ie_item_nao_encontrado,
			ie_selecionado,
			qt_liberar,
			ie_sem_fluxo,
			ie_tipo_guia,
			ie_faturamento,
			nr_identificador,
			nr_seq_conta_pos_estab,
			tx_administracao_pos,
			vl_liberado_pag,
			nr_id_transacao,
			ie_a520)
		values	(nr_sequencia_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_analise_p,
			nr_seq_conta_w,
			nr_seq_conta_proc_w,
			nr_seq_proc_partic_w,
			ds_item_w,
			ie_tipo_despesa_w,
			ie_tipo_linha_w,
			ie_tipo_item_w,
			qt_procedimento_imp_w,
			vl_procedimento_imp_w,
			vl_proc_unitario_w,
			vl_procedimento_w,
			nr_seq_proc_pai_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			ie_via_acesso_w,
			ds_via_acesso_w,
			nm_prestador_solic_w,
			nr_auxiliares_w,
			cd_porte_anestesico_w,
			null,--ds_tipo_guia_w,
			nm_prestador_exec_w,
			nm_prestador_pag_w,
			ds_espec_cbo_w,
			nr_seq_protocolo_w,
			ie_autorizado_w,
			cd_guia_w,
			ds_setor_atend_w,
			ds_item_importacao_w,
			ds_medico_executor_w,
			vl_glosa_material_w,
			vl_glosa_hi_w,
			vl_glosa_co_w,
			vl_custo_operacional_w,
			vl_liberado_material_w,
			vl_liberado_co_w,
			vl_liberado_hi_w,
			vl_calculado_material_w,
			vl_calculado_co_w,
			vl_calculado_hi_w,
			tx_intercambio_w,
			tx_intercambio_imp_w,
			vl_taxa_intercambio_w,
			vl_taxa_intercambio_imp_w,
			qt_liberado_w,
			tx_item_w,
			vl_glosa_w,
			vl_liberado_w,
			vl_unitario_apres_w,
			nr_seq_prestador_exec_w,
			nr_seq_guia_w,
			ie_status_analise_w,
			ie_pagamento_w,
			ie_valor_base_w,
			ds_grau_partic_w,
			ie_status_item_w,
			ds_status_item_w,
			ie_pend_grupo_w,
			ie_item_nao_encontrado_w,
			ie_selecionado_w,
			qt_liberar_w,
			ie_sem_fluxo_w,
			ie_tipo_guia_w,
			ie_faturamento_w,
			nr_id_analise_w,
			nr_seq_conta_pos_w,
			tx_administracao_w,
			vl_liberado_pag_w,
			nr_id_transacao_p,
			ie_aviso_a520_part_w);
			
		pls_consulta_analise_pos_pck.set_nr_seq_item(nr_sequencia_w + 1);
	end if;
	end;
end loop;
close C03;

end pls_gerar_w_analise_pos_part;
/
