create or replace
procedure Atual_status_falta_agecons(dt_parametro_p	date) is 

cd_Agenda_w	number(10);
nr_seq_agenda_w	number(10);

Cursor C01 is
	select	cd_agenda,
		nr_sequencia	
	from	agenda_consulta
	where	trunc(dt_agenda) 	= trunc(dt_parametro_p)
	and	nr_atendimento is null
	and	ie_status_agenda = 'N';

begin

open C01;
loop
fetch C01 into	
	cd_Agenda_w,
	nr_seq_agenda_w;
exit when C01%notfound;
	begin
	Alterar_status_agecons(cd_agenda_w, nr_seq_agenda_w, 'I', null, obter_desc_expressao(774851), 'N', 'TASY', null);
	end;
end loop;
close C01;

commit;

end Atual_status_falta_agecons;
/