create or replace
procedure pls_ajustar_parcela_mens_sca is 

Cursor C01 is
	select	e.nr_sequencia nr_seq_mens_item,
		nvl(e.nr_parcela, 0) nr_parcela_sca,
		trunc(months_between(b.dt_mesano_referencia, trunc(d.dt_inicio_vigencia, 'month'))) + 1 nr_parcela_sca_correta
	  from  pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c,
		pls_sca_vinculo d,
		pls_mensalidade_sca e
	where   c.nr_sequencia = b.nr_seq_mensalidade
	  and	a.nr_sequencia = e.nr_seq_item_mens
	  and   b.nr_sequencia = a.nr_seq_mensalidade_seg
	  and   d.nr_sequencia = e.nr_seq_vinculo_sca
	  and   c.ie_cancelamento is null
	  and   c.dt_referencia <= '01/01/2018'
	  and   d.dt_inicio_vigencia is not null
	  and   a.ie_tipo_item = '15';

begin

for r_c01_w in c01 loop
	begin
	
	if	(r_c01_w.nr_parcela_sca_correta <> r_c01_w.nr_parcela_sca) and
		(r_c01_w.nr_parcela_sca_correta > 0) then
		update	pls_mensalidade_sca
		set	nr_parcela 	= r_c01_w.nr_parcela_sca_correta
		where	nr_sequencia 	= r_c01_w.nr_seq_mens_item;
	end if;
	
	end;
end loop;

commit;

end pls_ajustar_parcela_mens_sca;
/