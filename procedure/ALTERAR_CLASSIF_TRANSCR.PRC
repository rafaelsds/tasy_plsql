create or replace
procedure Alterar_classif_transcr(	nr_sequencia_p		number,
					nr_seq_classif_p	number,
					ds_justificativa_p	varchar2,
					nm_usuario_p		Varchar2) is 

begin

update	transcricao_prescricao
set	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	nr_seq_classificacao	= nr_seq_classif_p
where	nr_sequencia		= nr_sequencia_p;	

commit;

gerar_hist_transcricao(nr_sequencia_p,'A',ds_justificativa_p,nm_usuario_p);

end Alterar_classif_transcr;
/