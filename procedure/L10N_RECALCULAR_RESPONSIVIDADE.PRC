create or replace
procedure l10n_recalcular_responsividade(	nr_seq_visao_p 				number,
											tamanho_panel_p				number default 0,
											ie_chamar_painel_filho_p	varchar2 default 'N',
											nm_usuario_p				varchar2) is 

nm_atributo_w		tabela_visao_atributo.nm_atributo%type;
qt_tam_delphi_w		tabela_visao_atributo.qt_tam_delphi%type;
ds_mascara_w		tabela_visao_atributo.ds_mascara%type;
ie_componente_w		tabela_visao_atributo.ie_componente%type;
cd_mascara_w		tabela_visao_atributo.cd_mascara%type;
cd_funcao_sch_w		number(10);
nr_sequencia_dic_objeto_w	number(10);
ie_chamar_painel_filho_w 	varchar2(10);
nr_seq_visao_ref_w	number(10):= 0;
qt_registro_w	number(10) := 0;

tamanho_panel_w number(10,0);


type v_columns is varray(12) of number(10,0);
type v_panels is varray(6) of v_columns;	

v_column_3 	v_columns; -- Panel of 3 columns
v_column_4 	v_columns; -- Panel of 4 columns
v_column_6 	v_columns; -- Panel of 6 columns
v_column_8 	v_columns; -- Panel of 8 columns
v_column_9 	v_columns; -- Panel of 9 columns
v_column_12 	v_columns; -- Panel of 12 columns
v_sizes		v_panels;  -- Matrix of sizes		 

cursor atributo_visao is
select 	nm_atributo, 
	qt_tam_delphi,
	ds_mascara,
	ie_componente, 
	cd_mascara
from   	tabela_visao_atributo b
where  	b.nr_sequencia = nr_seq_visao_p
and	nr_seq_apresent is not null
and	qt_tam_delphi is not null;	

begin

-- size in pixel of all columns for each panel
v_column_3 := v_columns(50,50,50,50,100,100,150,150,150,200,200,200); 
v_column_4 := v_columns(50,50,50,50,100,150,200,200,250,250,300,300);
v_column_6 := v_columns(50,50,100,150,200,250,300,350,400,450,500,550);
v_column_8 := v_columns(50,100,150,200,300,350,400,450,550,600,650,700);
v_column_9 := v_columns (100,150,250,300,400,450,500,550,650,700,800,850);
v_column_12 := v_columns(100,150,250,350,450,550,650,750,850,950,1050,1150);

v_sizes := v_panels(v_column_3, v_column_4, v_column_6, v_column_8, v_column_9, v_column_12);


update tabela_visao
set ie_responsivo = 'S'
where nr_sequencia = nr_seq_visao_p;


open atributo_visao;
loop
fetch atributo_visao into	
	nm_atributo_w,
	qt_tam_delphi_w,
	ds_mascara_w,
	ie_componente_w,
	cd_mascara_w;
exit when atributo_visao%notfound;
begin
	tamanho_panel_w := tamanho_panel_p;
	ie_chamar_painel_filho_w := ie_chamar_painel_filho_p;
	
	if (tamanho_panel_p  = 0) then
		select max(nr_seq_visao)
		into	nr_seq_visao_ref_w
		from	tabela_visao_pais
		where	nr_seq_visao_ref = nr_seq_visao_p;
		
		if (nr_seq_visao_ref_w = 0) then
			SELECT MAX(cd_funcao) 
			into cd_funcao_sch_w	
			FROM objeto_schematic
			WHERE NR_SEQ_VISAO = nr_seq_visao_p;
			
			select max(nr_sequencia) 
			into nr_sequencia_dic_objeto_w
			from objeto_schematic 
			where nr_seq_visao = nr_seq_visao_p
			and cd_funcao = cd_funcao_sch_w;
		else
			SELECT MAX(cd_funcao) 
			into cd_funcao_sch_w	
			FROM objeto_schematic
			WHERE NR_SEQ_VISAO = nr_seq_visao_ref_w;
			
			select max(nr_sequencia) 
			into nr_sequencia_dic_objeto_w
			from objeto_schematic 
			where nr_seq_visao = nr_seq_visao_ref_w
			and cd_funcao = cd_funcao_sch_w;
		end if;
		
		select count(*)
		into	qt_registro_w
		from objeto_schematic_prop
		where nr_seq_objeto = nr_sequencia_dic_objeto_w;
		
		if (qt_registro_w > 0 ) then
			select ie_chamar_painel_filho
			into ie_chamar_painel_filho_w 
			from objeto_schematic_prop
			where nr_seq_objeto = nr_sequencia_dic_objeto_w;
		end if;

		select obter_tamanho_panel(nr_sequencia_dic_objeto_w) 
		into tamanho_panel_w
		from dual;
	end if;
	

	
	if(tamanho_panel_w > 6 and ie_chamar_painel_filho_w ='S') then
		tamanho_panel_w := 6;
	end if;

	if (tamanho_panel_w = 3) then
	
		if (qt_tam_delphi_w = 1) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(1), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 2) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(2)
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 3) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(3), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 4) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(4), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 5) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(5), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 6) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(6), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 7) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(7), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 8) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(8), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 9) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(9), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 10) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(10), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 11) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(11), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w >= 12) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(1)(12), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		end if;
		
	elsif (tamanho_panel_w = 4) then
	
		if (qt_tam_delphi_w = 1) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(1), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 2) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(2), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 3) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(3), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 4) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(4), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 5) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(5), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 6) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(6), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 7) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(7), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 8) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(8), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 9) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(9), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 10) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(10), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 11) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(11), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w >= 12) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(2)(12), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		end if;
		
	elsif (tamanho_panel_w = 6) then
	
		if (qt_tam_delphi_w = 1) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(1), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 2) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(2), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 3) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(3), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 4) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(4), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 5) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(5), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 6) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(6), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 7) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(7), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 8) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(8), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 9) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(9), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 10) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(10), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 11) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(11), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w >= 12) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(3)(12), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		end if;
		
	elsif (tamanho_panel_w = 8) then
	
		if (qt_tam_delphi_w = 1) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(1), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 2) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(2), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 3) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(3), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 4) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(4), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 5) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(5), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 6) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(6), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 7) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(7), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 8) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(8), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 9) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(9), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 10) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(10), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 11) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(11), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w >= 12) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(4)(12), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		end if;
		
	elsif (tamanho_panel_w = 9) then
	
		if (qt_tam_delphi_w = 1) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(1), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 2) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(2), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 3) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(3), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 4) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(4), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 5) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(5), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 6) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(6), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 7) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(7), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 8) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(8), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 9) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(9), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 10) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(10), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 11) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(11), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w >= 12) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(5)(12), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		end if;
		
	elsif (tamanho_panel_w = 12) then

		if (qt_tam_delphi_w = 1) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(1), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 2) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(2), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 3) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(3), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 4) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(4), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 5) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(5), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 6) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(6), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 7) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(7), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 8) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(8), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 9) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(9), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 10) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(10), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w = 11) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(11), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		elsif (qt_tam_delphi_w >= 12) then 
			update 	tabela_visao_atributo 
			set 	qt_tam_html = v_sizes(6)(12), dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
			where 	nr_sequencia = nr_seq_visao_p
			and	nm_atributo = nm_atributo_w;
		end if;
	
	end if;
	
end;
end loop;
close atributo_visao;

commit;

end l10n_recalcular_responsividade;
/
