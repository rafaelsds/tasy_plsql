create or replace PROCEDURE RESET_RP_MEDICINE_SEQ
									(CD_PESSOA_FISICA_P VARCHAR2,
									 IE_IDENTITY_CODE_P VARCHAR2
									)
IS
NR_SEQ_MAX_W NUMBER := 0;

BEGIN

		execute immediate 'drop sequence RP_MEDICINE_SEQ';
		IF (IE_IDENTITY_CODE_P = 'S') THEN	
				execute immediate 'create sequence RP_MEDICINE_SEQ increment by 1 start with 1 maxvalue 9999999999 cycle cache 10';
		ELSE
				select max(NR_SEQUENCIA) into NR_SEQ_MAX_W from RP_MEDICINE where cd_pessoa_fisica = CD_PESSOA_FISICA_P;
				execute immediate 'create sequence RP_MEDICINE_SEQ increment by 1 start with ' || (NR_SEQ_MAX_W + 1) || ' maxvalue 9999999999 cycle cache 10';
		END IF;
		commit;
			
END RESET_RP_MEDICINE_SEQ;
/
