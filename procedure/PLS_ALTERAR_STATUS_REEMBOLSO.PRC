create or replace
procedure pls_alterar_status_reembolso
			(	nr_seq_reembolso_p	Number,
				nm_usuario_p		Varchar2) is 

qt_proc_reembolso_w		number(10);
qt_mat_reembolso_w		number(10);
qt_liberado_proc_w		number(10);
qt_liberado_mat_w		number(10);
ie_tipo_contratacao_w		varchar2(2);
ie_participacao_w		varchar2(1);
ie_tipo_plano_w			varchar2(3);
ie_tipo_beneficiario_w		varchar2(3);
ie_status_conta_w		Varchar2(5);
cd_estabelecimento_w		Number(10);

begin

select	count(1)
into	qt_proc_reembolso_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_reembolso_p;

select	count(1)
into	qt_liberado_proc_w
from	pls_conta_proc
where	ie_status in ('S','L','C')
and	nr_seq_conta	= nr_seq_reembolso_p;

select	count(1)
into	qt_mat_reembolso_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_reembolso_p;

select	count(1)
into	qt_liberado_mat_w
from	pls_conta_mat
where	ie_status in ('S','L','C')
and	nr_seq_conta	= nr_seq_reembolso_p;

begin
select	c.ie_tipo_contratacao,
	nvl(d.ie_participacao,c.ie_participacao),
	d.ie_tipo_beneficiario,
	a.cd_estabelecimento
into	ie_tipo_contratacao_w,
	ie_participacao_w,
	ie_tipo_beneficiario_w,
	cd_estabelecimento_w
from	pls_contrato	d,
	pls_plano	c,
	pls_segurado	b,
	pls_conta	a
where	a.nr_sequencia		= nr_seq_reembolso_p
and	a.nr_seq_segurado	= b.nr_sequencia
and	b.nr_seq_plano		= c.nr_sequencia
and	b.nr_seq_contrato	= d.nr_sequencia;
exception
	when others then
	ie_tipo_contratacao_w:= null;
	ie_participacao_w:= null;
	ie_tipo_beneficiario_w:= null;
	cd_estabelecimento_w:= null;
--on_error(-20011,'N�o foi poss�vel ler os dados do reembolso ' || nr_seq_reembolso_p || '!#@#@');
end;

/* Obter o tipo do plano do benefici�rio - Dom�nio 2213 */
if	(ie_tipo_contratacao_w	= 'I') then
	ie_tipo_plano_w	:= 'IF';
elsif	(ie_participacao_w	= 'S') then
	ie_tipo_plano_w	:= 'CSP';
elsif	(ie_participacao_w	= 'C') then
	ie_tipo_plano_w	:= 'CCP';
end if;	


ie_status_conta_w	:= 	'L';
if	(qt_liberado_proc_w < qt_proc_reembolso_w) or
	(qt_liberado_mat_w < qt_mat_reembolso_w) then
	ie_status_conta_w	:= 	'P';
end if;

update	pls_conta
set	ie_status		= ie_status_conta_w,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate,
	ie_tipo_plano		= ie_tipo_plano_w,
	ie_tipo_beneficiario	= ie_tipo_beneficiario_w
where	nr_sequencia		= nr_seq_reembolso_p;

commit;

end pls_alterar_status_reembolso;
/
