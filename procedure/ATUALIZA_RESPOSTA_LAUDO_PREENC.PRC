create or replace
procedure atualiza_resposta_laudo_preenc(
			nr_atendimento_p	number,
			ie_laudo_preenchido_p	varchar2) is 

begin

if	(nvl(nr_atendimento_p,0) > 0) then

	update	atendimento_paciente
	set	ie_laudo_preenchido = ie_laudo_preenchido_p 
	where	nr_atendimento = nr_atendimento_p;
end if;

commit;

end atualiza_resposta_laudo_preenc;
/