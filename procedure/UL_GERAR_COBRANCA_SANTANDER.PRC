CREATE OR REPLACE PROCEDURE UL_Gerar_cobranca_Santander	(nr_seq_cobr_escrit_p	NUMBER,
					nm_usuario_p		VARCHAR2) IS


tp_registro_w			number(10);
nr_seq_envio_w 			number(10);
dt_remessa_retorno_w 		date;
dt_credito_w 			date;
cd_banco_w 			varchar2(20);
cd_cgc_w 			varchar2(14);
cd_convenio_banco_w 		varchar2(100);
cd_agencia_bancaria_w		varchar2(20);
nr_digito_agencia_w		varchar2(2);
nr_conta_corrente_w		varchar2(20);
nm_empresa_w			varchar2(100);
cd_movimento_w			varchar2(100);
nr_titulo_w			number(10);
cd_carteira_w			varchar(50);
nr_doc_cobranca_w		number(10);
dt_vencimento_w			date;
dt_emissao_w			date;
dt_desconto_w			date;
dt_juros_mora_w			date;
nr_operacao_credito_w		number(10);
vl_cobranca_w			number(15,2);
vl_juros_mora_w			number(15,2);
vl_desconto_w			number(15,2);
ie_tipo_inscricao_w			number(10);
nr_inscricao_w			varchar2(20);
nm_pessoa_w			varchar2(100);
ds_endereco_w			varchar2(100);
ds_bairro_w			varchar2(80);
cd_cep_w			varchar2(20);
ds_cidade_w			varchar2(50);
ds_uf_w				valor_dominio.vl_dominio%type;
qt_cobranca_simples_w		number(10);
vl_carteira_simples_w		number(15,2);
ie_emissao_bloqueto_w		varchar2(10);
ds_nosso_numero_w		varchar2(25);
qt_cobranca_vinculada_w		number(10);
qt_lote_arquivo_w			number(10);
qt_reg_arquivo_w			number(10);
nm_banco_w			varchar2(100);

nr_seq_reg_lote_w			number(10) := 1;
nr_registro_w			number(10);
ie_digito_conta_w		varchar2(1);

cursor c01 is
select	nr_registro,
	tp_registro,
	nr_seq_envio,
	dt_remessa_retorno,
	dt_credito,
	cd_banco,
	cd_cgc,
	cd_convenio_banco,
	cd_agencia_bancaria,
	nr_digito_agencia,
	nr_conta_corrente,
	nm_empresa,
	cd_movimento,
	nr_titulo,
	cd_carteira,
	nr_doc_cobranca,
	dt_vencimento,
	dt_emissao,
	dt_desconto,
	dt_juros_mora,
	nr_operacao_credito,
	vl_cobranca,
	vl_juros_mora,
	vl_desconto,
	ie_tipo_inscricao,
	nr_inscricao,
	nm_pessoa,
	ds_endereco,
	ds_bairro,
	cd_cep,
	ds_cidade,
	ds_uf,
	qt_cobranca_simples,
	vl_carteira_simples,
	ie_emissao_bloqueto,
	ds_nosso_numero,
	qt_cobranca_vinculada,
	qt_lote_arquivo,
	qt_reg_arquivo,
	nm_banco,
	ie_digito_conta
from	(
select	1 nr_registro,
	1 tp_registro,
	a.nr_sequencia nr_seq_envio,
	a.dt_remessa_retorno,
	a.dt_remessa_retorno dt_credito,
	a.cd_banco,
	b.cd_cgc,
	' ' cd_convenio_banco,
	c.cd_agencia_bancaria,
	(select max(substr(x.ie_digito,1,2)) from agencia_bancaria x  where x.cd_banco = c.cd_banco and x.cd_agencia_bancaria = c.cd_agencia_bancaria) nr_digito_agencia,
	c.cd_conta nr_conta_corrente,
	substr(obter_razao_social(b.cd_cgc),1,100) nm_empresa,
	' ' cd_movimento,
	0 nr_titulo,
	' ' cd_carteira,
	-2 nr_doc_cobranca,
	sysdate dt_vencimento,
	sysdate dt_emissao,
	a.dt_remessa_retorno dt_desconto,
	sysdate dt_juros_mora,
	0 nr_operacao_credito,
	0 vl_cobranca,
	0 vl_juros_mora,
	0 vl_desconto,
	0 ie_tipo_inscricao,
	' ' nr_inscricao,
	' ' nm_pessoa,
	' ' ds_endereco,
	' ' ds_bairro,
	' ' cd_cep,
	' ' ds_cidade,
	' ' ds_uf,
	0 qt_cobranca_simples,
	0 vl_carteira_simples,
	'' ie_emissao_bloqueto,
	'' ds_nosso_numero,
	0 qt_cobranca_vinculada,
	0 qt_lote_arquivo,
	0 qt_reg_arquivo,
	substr(obter_nome_banco(a.cd_banco),1,100) nm_banco,
	substr(c.ie_digito_conta,1,1) ie_digito_conta
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
UNION
select	2 nr_registro,
	2 tp_registro,
	a.nr_sequencia nr_seq_envio,
	a.dt_remessa_retorno,
	a.dt_remessa_retorno dt_credito,
	a.cd_banco,
	b.cd_cgc,
	' ' cd_convenio_banco,
	c.cd_agencia_bancaria,
	(select max(substr(x.ie_digito,1,2)) from agencia_bancaria x  where x.cd_banco = c.cd_banco and x.cd_agencia_bancaria = c.cd_agencia_bancaria) nr_digito_agencia,
	c.cd_conta nr_conta_corrente,
	substr(obter_razao_social(b.cd_cgc),1,100) nm_empresa,
	' ' cd_movimento,
	0 nr_titulo,
	' ' cd_carteira,
	-1 nr_doc_cobranca,
	sysdate dt_vencimento,
	sysdate dt_emissao,
	a.dt_remessa_retorno dt_desconto,
	sysdate dt_juros_mora,
	0 nr_operacao_credito,
	0 vl_cobranca,
	0 vl_juros_mora,
	0 vl_desconto,
	0 ie_tipo_inscricao,
	' ' nr_inscricao,
	' ' nm_pessoa,
	' ' ds_endereco,
	' ' ds_bairro,
	' ' cd_cep,
	' ' ds_cidade,
	' ' ds_uf,
	0 qt_cobranca_simples,
	0 vl_carteira_simples,
	a.ie_emissao_bloqueto,
	'' ds_nosso_numero,
	0 qt_cobranca_vinculada,
	0 qt_lote_arquivo,
	0 qt_reg_arquivo,
	' ' nm_banco,
	substr(c.ie_digito_conta,1,1) ie_digito_conta
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
union
select	3 nr_registro,
	3 tp_registro,
	a.nr_sequencia nr_seq_envio,
	sysdate dt_remessa_retorno,
	sysdate dt_credito,
	a.cd_banco,
	b.cd_cgc_cpf cd_cgc,
	' ' cd_convenio_banco,
	c.cd_agencia_bancaria,
	(select max(substr(x.ie_digito,1,2)) from agencia_bancaria x  where x.cd_banco = c.cd_banco and x.cd_agencia_bancaria = c.cd_agencia_bancaria) nr_digito_agencia,
	c.nr_conta nr_conta_corrente,
	' ' nm_empresa,
	lpad(nvl(substr(c.cd_ocorrencia,1,2),'1'),2,'0') cd_movimento,
	c.nr_titulo,
	'1' cd_carteira,
	c.nr_titulo nr_doc_cobranca,
	b.dt_pagamento_previsto dt_vencimento,
	b.dt_emissao,
	a.dt_remessa_retorno dt_desconto,
	sysdate dt_juros_mora,
	c.nr_titulo nr_operacao_credito,
	c.vl_cobranca,
	0 vl_juros_mora,
	nvl(c.vl_desconto,0) vl_desconto,
	b.ie_tipo_pessoa ie_tipo_inscricao,
	b.cd_cgc_cpf nr_inscricao,
	b.nm_pessoa,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'EC'),1,100) ds_endereco,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),1,80) ds_bairro,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),1,20) cd_cep,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),1,50) ds_cidade,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),1,2) ds_uf,
	0 qt_cobranca_simples,
	0 vl_carteira_simples,
	a.ie_emissao_bloqueto,
	lpad(Obter_Nosso_Numero_Banco(a.cd_banco, b.nr_titulo),13,'0') ds_nosso_numero,
	0 qt_cobranca_vinculada,
	0 qt_lote_arquivo,
	0 qt_reg_arquivo,
	substr(obter_nome_banco(a.cd_banco),1,100) nm_banco,
	substr(c.ie_digito_conta,1,1) ie_digito_conta
from	titulo_receber_v b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo		= b.nr_titulo
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
union
select	3 nr_registro,
	4 tp_registro,
	a.nr_sequencia nr_seq_envio,
	sysdate dt_remessa_retorno,
	sysdate dt_credito,
	a.cd_banco,
	b.cd_cgc_cpf cd_cgc,
	' ' cd_convenio_banco,
	c.cd_agencia_bancaria,
	(select max(substr(x.ie_digito,1,2)) from agencia_bancaria x  where x.cd_banco = c.cd_banco and x.cd_agencia_bancaria = c.cd_agencia_bancaria) nr_digito_agencia,
	c.nr_conta nr_conta_corrente,
	' ' nm_empresa,
	lpad(nvl(substr(c.cd_ocorrencia,1,2),'1'),2,'0') cd_movimento,
	c.nr_titulo,
	'1' cd_carteira,
	c.nr_titulo nr_doc_cobranca,
	b.dt_pagamento_previsto dt_vencimento,
	b.dt_emissao,
	a.dt_remessa_retorno dt_desconto,
	sysdate dt_juros_mora,
	c.nr_titulo nr_operacao_credito,
	c.vl_cobranca,
	0 vl_juros_mora,
	nvl(c.vl_desconto,0) vl_desconto,
	b.ie_tipo_pessoa ie_tipo_inscricao,
	b.cd_cgc_cpf nr_inscricao,
	b.nm_pessoa,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'EC'),1,100) ds_endereco,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),1,80) ds_bairro,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),1,20) cd_cep,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),1,50) ds_cidade,
	substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),1,2) ds_uf,
	0 qt_cobranca_simples,
	0 vl_carteira_simples,
	a.ie_emissao_bloqueto,
	lpad(Obter_Nosso_Numero_Banco(a.cd_banco, b.nr_titulo),13,'0') ds_nosso_numero,
	0 qt_cobranca_vinculada,
	0 qt_lote_arquivo,
	0 qt_reg_arquivo,
	substr(obter_nome_banco(a.cd_banco),1,100) nm_banco,
	substr(c.ie_digito_conta,1,1) ie_digito_conta
from	titulo_receber_v b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo		= b.nr_titulo
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
union
select	4 nr_registro,
	5 tp_registro,
	a.nr_sequencia nr_seq_envio,
	a.dt_remessa_retorno,
	a.dt_remessa_retorno dt_credito,
	a.cd_banco,
	' ' cd_cgc,
	' ' cd_convenio_banco,
	' ' cd_agencia_bancaria,
	'0' nr_digito_agencia,
	' ' nr_conta_corrente,
	' ' nm_empresa,
	' ' cd_movimento,
	0 nr_titulo,
	' ' cd_carteira,
	9999998 nr_doc_cobranca,
	sysdate dt_vencimento,
	sysdate dt_emissao,
	a.dt_remessa_retorno dt_desconto,
	sysdate dt_juros_mora,
	0 nr_operacao_credito,
	0 vl_cobranca,
	0 vl_juros_mora,
	0 vl_desconto,
	0 ie_tipo_inscricao,
	' ' nr_inscricao,
	' ' nm_pessoa,
	' ' ds_endereco,
	' ' ds_bairro,
	' ' cd_cep,
	' ' ds_cidade,
	' ' ds_uf,
	((count(*) * 2) + 2) qt_cobranca_simples,
	sum(b.vl_cobranca) vl_carteira_simples,
	'' ie_emissao_bloqueto,
	'' ds_nosso_numero,
	0 qt_cobranca_vinculada,
	0 qt_lote_arquivo,
	0 qt_reg_arquivo,
	' ' nm_banco,
	'0' ie_digito_conta
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
group by a.nr_sequencia,
 	 a.dt_remessa_retorno,
	 a.dt_remessa_retorno,
	 a.cd_banco
union
select	5 nr_registro,
	9 tp_registro,
	a.nr_sequencia nr_seq_envio,
	a.dt_remessa_retorno,
	a.dt_remessa_retorno dt_credito,
	a.cd_banco,
	' ' cd_cgc,
	' ' cd_convenio_banco,
	' ' cd_agencia_bancaria,
	'0' nr_digito_agencia,
	' ' nr_conta_corrente,
	' ' nm_empresa,
	' ' cd_movimento,
	0 nr_titulo,
	' ' cd_carteira,
	9999999 nr_doc_cobranca,
	sysdate dt_vencimento,
	sysdate dt_emissao,
	a.dt_remessa_retorno dt_desconto,
	sysdate dt_juros_mora,
	0 nr_operacao_credito,
	0 vl_cobranca,
	0 vl_juros_mora,
	0 vl_desconto,
	0 ie_tipo_inscricao,
	' ' nr_inscricao,
	' ' nm_pessoa,
	' ' ds_endereco,
	' ' ds_bairro,
	' ' cd_cep,
	' ' ds_cidade,
	' ' ds_uf,
	count(*) qt_cobranca_simples,
	sum(b.vl_cobranca) vl_carteira_simples,
	'' ie_emissao_bloqueto,
	'' ds_nosso_numero,
	0 qt_cobranca_vinculada,
	000001 qt_lote_arquivo,
	count(*) qt_reg_arquivo,
	' ' nm_banco,
	'0' ie_digito_conta
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
group by a.nr_sequencia,
 	 a.dt_remessa_retorno,
	 a.dt_remessa_retorno,
	 a.cd_banco
)
order by	nr_doc_cobranca,
	tp_registro;

begin

delete	from w_cobranca_banco;
commit;

open c01;
loop
fetch c01 into
	nr_registro_w,
	tp_registro_w,
	nr_seq_envio_w,
	dt_remessa_retorno_w,
	dt_credito_w,
	cd_banco_w,
	cd_cgc_w,
	cd_convenio_banco_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	cd_movimento_w,
	nr_titulo_w,
	cd_carteira_w,
	nr_doc_cobranca_w,
	dt_vencimento_w,
	dt_emissao_w,
	dt_desconto_w,
	dt_juros_mora_w,
	nr_operacao_credito_w,
	vl_cobranca_w,
	vl_juros_mora_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w,
	qt_cobranca_simples_w,
	vl_carteira_simples_w,
	ie_emissao_bloqueto_w,
	ds_nosso_numero_w,
	qt_cobranca_vinculada_w,
	qt_lote_arquivo_w,
	qt_reg_arquivo_w,
	nm_banco_w,
	ie_digito_conta_w;
exit when c01%notfound;

insert	into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		IE_TIPO_REGISTRO,
		nr_seq_envio,
		DT_GERACAO,
		DT_RECEBIMENTO,
		CD_BANCO,
		DS_CGC_CPF,
		CD_AGENCIA_BANCARIA,
		nr_digito_agencia,
		CD_CONTA,
		NM_EMPRESA,
		NR_TITULO,
		DT_VENCIMENTO,
		DT_EMISSAO,
		VL_TITULO,
		VL_JUROS,
		VL_DESCONTO,
		IE_TIPO_PESSOA,
		NM_PAGADOR,
		DS_ENDERECO,
		DS_BAIRRO,
		CD_CEP,
		DS_CIDADE,
		SG_ESTADO,
		qt_reg_lote,
		vl_tot_registros,
		qt_lotes,
		qt_registros,
		ds_banco,
		nr_seq_reg_lote,
		nr_registro,
		nr_nosso_numero,
		ie_digito_conta)
	values	(w_interf_itau_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		tp_registro_w,
		nr_seq_envio_w,
		dt_remessa_retorno_w,
		dt_credito_w,
		cd_banco_w,
		cd_cgc_w,
		cd_agencia_bancaria_w,
		lpad(nr_digito_agencia_w,2,'0'),
		nr_conta_corrente_w,
		nm_empresa_w,
		nr_titulo_w,
		dt_vencimento_w,
		dt_emissao_w,
		vl_cobranca_w,
		vl_juros_mora_w,
		vl_desconto_w,
		ie_tipo_inscricao_w,
		nm_pessoa_w,
		ds_endereco_w,
		substr(ds_bairro_w,1,40),
		cd_cep_w,
		ds_cidade_w,
		ds_uf_w,
		qt_cobranca_simples_w,
		vl_carteira_simples_w,
		qt_lote_arquivo_w,
		qt_reg_arquivo_w,
		nm_banco_w,
		nr_seq_reg_lote_w,
		nr_registro_w,
		ds_nosso_numero_w,
		lpad(ie_digito_conta_w,1,'0'));


if	(tp_registro_w in (3,4)) then
	nr_seq_reg_lote_w := nr_seq_reg_lote_w + 1;
	end if;

end loop;
close c01;

commit;

end UL_Gerar_cobranca_Santander;
/
