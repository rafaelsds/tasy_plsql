create or replace
procedure pls_alimenta_estab_tab_preco is 

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
tb_nr_sequencia_w		pls_util_cta_pck.t_number_table;	
tb_cd_edicao_amb_w		pls_util_cta_pck.t_varchar2_table_20;
tb_dt_inicio_vigencia_w		pls_util_cta_pck.t_date_table;

cursor C01 is
	select	nr_sequencia
	from	cbhpm_preco
	where	cd_estabelecimento is null;
	
cursor C02 is
	select	cd_edicao_amb
	from	edicao_amb
	where	cd_estabelecimento is null;
	
cursor C03 is
	select	nr_sequencia
	from	tuss_porte
	where	cd_estabelecimento is null;
	
cursor C04 is
	select	nr_sequencia
	from	pls_edicao_preco_tuss
	where	cd_estabelecimento is null;
	
cursor C05 is
	select	nr_sequencia
	from	pls_porte_anestesico_tuss
	where	cd_estabelecimento is null;
	
cursor C06 is
	select	nr_sequencia
	from	cbhpm_porte
	where	cd_estabelecimento is null;
	
cursor C07 is
	select	nr_sequencia
	from	cbhpm_edicao
	where	cd_estabelecimento is null;
	
cursor C08 is
	select	cd_laboratorio
	from	brasindice_laboratorio
	where	cd_estabelecimento is null;
	
cursor C09 is
	select	cd_medicamento
	from	brasindice_medicamento
	where	cd_estabelecimento is null;
	
cursor C10 is
	select	cd_apresentacao
	from	brasindice_apresentacao
	where	cd_estabelecimento is null;
	
cursor C11 is
	select	nr_sequencia
	from	brasindice_restrito_hosp
	where	cd_estabelecimento is null;
	
/* Cuidado, este cursor pode afetar o Tasy Prestador
cursor C12 is
	select	nr_sequencia
	from	simpro_preco
	where	cd_estabelecimento is null;*/
	
cursor C13 is
	select	cd_simpro
	from	simpro_cadastro
	where	cd_estabelecimento is null;
	
cursor C14 is
	select	nr_porte_anestesico,
		cd_edicao_amb,
		dt_inicio_vigencia
	from	porte_anestesico
	where	cd_estabelecimento is null;
	
/* Cuidado, este cursor pode afetar o Tasy Prestador
Cursor C15 is
	select	nr_sequencia
	from	brasindice_preco
	where	cd_estabelecimento is null;*/
	
begin
select	pls_obter_cd_estab_ops
into	cd_estabelecimento_w
from	dual;

if	(cd_estabelecimento_w is not null) then

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C01;
	loop
		fetch C01 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	cbhpm_preco set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C02;
	loop
		fetch C02 bulk collect into tb_cd_edicao_amb_w
		limit 500;
		
		exit when tb_cd_edicao_amb_w.count = 0;
		
		forall i in tb_cd_edicao_amb_w.first .. tb_cd_edicao_amb_w.last
			update	edicao_amb set
				cd_estabelecimento = cd_estabelecimento_w
			where	cd_edicao_amb = tb_cd_edicao_amb_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C03;
	loop
		fetch C03 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	tuss_porte set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C04;
	loop
		fetch C04 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	pls_edicao_preco_tuss set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C05;
	loop
		fetch C05 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	pls_porte_anestesico_tuss set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C06;
	loop
		fetch C06 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	cbhpm_porte set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C07;
	loop
		fetch C07 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	cbhpm_edicao set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C08;
	loop
		fetch C08 bulk collect into tb_cd_edicao_amb_w
		limit 500;
		
		exit when tb_cd_edicao_amb_w.count = 0;
		
		forall i in tb_cd_edicao_amb_w.first .. tb_cd_edicao_amb_w.last
			update	brasindice_laboratorio set
				cd_estabelecimento = cd_estabelecimento_w
			where	cd_laboratorio = tb_cd_edicao_amb_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C09;
	loop
		fetch C09 bulk collect into tb_cd_edicao_amb_w
		limit 500;
		
		exit when tb_cd_edicao_amb_w.count = 0;
		
		forall i in tb_cd_edicao_amb_w.first .. tb_cd_edicao_amb_w.last
			update	brasindice_medicamento set
				cd_estabelecimento = cd_estabelecimento_w
			where	cd_medicamento = tb_cd_edicao_amb_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C10;
	loop
		fetch C10 bulk collect into tb_cd_edicao_amb_w
		limit 500;
		
		exit when tb_cd_edicao_amb_w.count = 0;
		
		forall i in tb_cd_edicao_amb_w.first .. tb_cd_edicao_amb_w.last
			update	brasindice_apresentacao set
				cd_estabelecimento = cd_estabelecimento_w
			where	cd_apresentacao = tb_cd_edicao_amb_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C11;
	loop
		fetch C11 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	brasindice_restrito_hosp set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	/*open C12;
	loop
		fetch C12 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	simpro_preco set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;*/

	open C13;
	loop
		fetch C13 bulk collect into tb_cd_edicao_amb_w
		limit 500;
		
		exit when tb_cd_edicao_amb_w.count = 0;
		
		forall i in tb_cd_edicao_amb_w.first .. tb_cd_edicao_amb_w.last
			update	simpro_cadastro set
				cd_estabelecimento = cd_estabelecimento_w
			where	cd_simpro = tb_cd_edicao_amb_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	open C14;
	loop
		fetch C14 bulk collect into tb_nr_sequencia_w, tb_cd_edicao_amb_w, tb_dt_inicio_vigencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	porte_anestesico set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_porte_anestesico = tb_nr_sequencia_w(i)
			and	cd_edicao_amb = tb_cd_edicao_amb_w(i)
			and	dt_inicio_vigencia = tb_dt_inicio_vigencia_w(i);
		commit;
	end loop;

	tb_nr_sequencia_w.delete;	
	tb_cd_edicao_amb_w.delete;
	tb_dt_inicio_vigencia_w.delete;

	/*open C15;
	loop
		fetch C15 bulk collect into tb_nr_sequencia_w
		limit 500;
		
		exit when tb_nr_sequencia_w.count = 0;
		
		forall i in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	brasindice_preco set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(i);
		commit;
	end loop;*/
end if;

end pls_alimenta_estab_tab_preco;
/