create or replace
procedure copiar_hist_padrao_empresa(	cd_empresa_origem_p	number,
				cd_empresa_destino_p	number,
				nm_usuario_p		varchar2) as

cd_historico_w			number(10);
ds_historico_w			varchar2(255);

cursor C01 is
select	ds_historico
from	historico_padrao
where	cd_empresa = cd_empresa_origem_p
and	ie_situacao = 'A';

begin

open C01;
loop
fetch C01 into
	ds_historico_w;
exit when C01%notfound;
	begin

	select	max(cd_historico) + 1
	into	cd_historico_w
	from	historico_padrao;

	insert into historico_padrao(
		cd_historico,
		ds_historico,
		dt_atualizacao,
		nm_usuario,
		cd_empresa,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_situacao)
	values(cd_historico_w,
		ds_historico_w,
		sysdate,
		nm_usuario_p,
		cd_empresa_destino_p,
		sysdate,
		nm_usuario_p,
		'A');
	end;
end loop;
close C01;

commit;

end copiar_hist_padrao_empresa;
/