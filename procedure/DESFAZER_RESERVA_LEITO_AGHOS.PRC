create or replace
procedure Desfazer_reserva_leito_aghos(	nr_internacao_p	number,
					nm_usuario_p	Varchar2) is 

nr_seq_interno_w	number(10);
					
begin

select	max(nr_seq_interno)
into	nr_seq_interno_w
from	unidade_atendimento
where	nr_internacao_aghos = nr_internacao_p;

update	unidade_atendimento
set	cd_paciente_reserva = null,
	nm_usuario_reserva = null,
	nr_internacao_aghos = null
where	nr_seq_interno = nr_seq_interno_w;

update	unidade_atendimento
set	ie_status_unidade = 'L'
where	nr_seq_interno = nr_seq_interno_w;

commit;

end Desfazer_reserva_leito_aghos;
/