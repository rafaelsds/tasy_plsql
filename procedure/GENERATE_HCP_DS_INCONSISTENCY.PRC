create or replace PROCEDURE Generate_hcp_ds_inconsistency ( 
nr_seq_dataset_p   NUMBER, 
ie_type_p          VARCHAR2, 
ds_inconsistency_p VARCHAR2, 
nm_usuario_p       VARCHAR2, 
ie_segment_p       VARCHAR2,
nr_atendimento_p   NUMBER) 
IS 
BEGIN 
    INSERT INTO hcp_inconsistency_ds 
                (nr_sequencia, 
                 dt_atualizacao, 
                 nm_usuario, 
                 dt_atualizacao_nrec, 
                 nm_usuario_nrec, 
                 nr_seq_dataset, 
                 ds_inconsistency, 
                 ie_type, 
                 ie_segment,
                 nr_atendimento) 
    VALUES      (hcp_inconsistency_ds_seq.NEXTVAL, 
                 SYSDATE, 
                 nm_usuario_p, 
                 SYSDATE, 
                 nm_usuario_p, 
                 nr_seq_dataset_p, 
                 ds_inconsistency_p, 
                 ie_type_p, 
                 ie_segment_p,
                 nr_atendimento_p); 
END Generate_hcp_ds_inconsistency;
/