create or replace
procedure pls_conv_hist_solic_resc
			(	nr_seq_historico_p	pls_solicitacao_resc_hist.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type) is
				
nr_seq_rtf_w		tasy_conversao_rtf.nr_sequencia%type;
ds_historico_texto_w	varchar2(4000);
ie_erro_w		varchar2(1);

begin

ie_erro_w	:= 'N';

begin
	converte_rtf_string('select ds_historico from pls_solicitacao_resc_hist where nr_sequencia = :nr_seq_historico ', nr_seq_historico_p, nm_usuario_p, nr_seq_rtf_w);
exception
when others then
	ie_erro_w	:= 'S';
end;

if	(ie_erro_w = 'N') then
	begin
	select	ds_texto
	into	ds_historico_texto_w
	from	tasy_conversao_rtf
	where	nr_sequencia = nr_seq_rtf_w;
	exception
	when others then
		null;
	end;

	update	pls_solicitacao_resc_hist
	set	ds_historico_texto	= ds_historico_texto_w
	where	nr_sequencia		= nr_seq_historico_p;
end if;

commit;

end pls_conv_hist_solic_resc;
/
