create or replace
procedure pls_ptu_imp_prestador
			(	ds_conteudo_p		varchar2,
				nm_usuario_p		Varchar2) is 
/*401*/
nr_seq_movimento_w		number(10);
cd_unimed_destino_w		number(4);
cd_unimed_origem_w		number(4);
dt_geracao_w			date;
nr_versao_transacao_w		number(2);

/*402*/
ie_rce_w			varchar2(1);
cd_especialidade_w		number(4);
cd_prestador_w			number(10);
nr_seq_prestador_ptu_w		number(10);
ie_tipo_prestador_w		varchar2(2);
nr_seq_prestador_w		number(10);
cd_cgc_cpf_w			varchar2(14);
nm_prestador_w			varchar2(40);
ie_tipo_vinculo_w		number(1);
dt_inclusao_w			date;
ie_tipo_classif_estab_w		varchar2(1);
ie_categoria_dif_w		varchar2(1);
ie_acidente_trabalho_w		varchar2(1);
ie_urgencia_emerg_w		Varchar2(1);
dt_inicio_servico_w		date;
dt_inicio_contrato_w		date;
nr_registro_ans_w		number(6);
nm_diretor_tecnico_w		varchar2(40);
nr_insc_estadual_w		number(20);
nr_crm_w			number(8);
uf_crm_w			varchar2(2);
nm_fantasia_w			varchar2(40);
dt_exclusao_w			date;
ie_tipo_contratualizacao_w	varchar2(1);
ie_tabela_propria_w		varchar2(1);
ie_perfil_assistencial_w	number(2);
ie_tipo_produto_w		number(1);
nr_seq_conselho_w		number(10);
nr_cons_diretor_tecnico_w	varchar2(15);
ie_tipo_rede_min_w		number(1);
sg_uf_cons_diretor_tecnico_w	varchar2(2);
ie_tipo_disponibilidade_w	number(1);
ie_guia_medico_w		varchar2(1);

/*403*/
nr_seq_endereco_w		number(10);
ie_tipo_endereco_w		number(1);
ds_endereco_w			varchar2(40);
cd_municipio_ibge_w		varchar2(6);
cd_cep_w			number(8);
nr_telefone_w			number(12);
cd_cnes_w			varchar2(7);
cd_cgc_cpf_ww			varchar2(14);
nr_endereco_w			varchar2(6);
ds_complemento_w		varchar2(15);
ds_bairro_w			varchar2(30);
nr_ddd_w			number(4);
nr_telefone_dois_w		number(12);
nr_fax_w			number(12);
ds_email_w			varchar2(40);
ds_endereco_web_w		varchar2(50);
nr_leitos_totais_w		number(6);
nr_leitos_contrat_w		number(6);
nr_leitos_psiquiatria_w		number(6);
nr_uti_adulto_w			number(6);
nr_uti_neonatal_w		number(6);
nr_uti_pediatria_w		number(6);

/*404*/
cd_grupo_servico_w		number(3);

/*505*/
cd_rede_w			varchar2(5);
nm_rede_w			varchar2(40);


begin

if	(substr(ds_conteudo_p,9,3) = '401') then
	cd_unimed_destino_w	:= to_number(substr(ds_conteudo_p,12,4));
	cd_unimed_origem_w	:= to_number(substr(ds_conteudo_p,16,4));
	dt_geracao_w		:= to_date(substr(ds_conteudo_p,26,2)||substr(ds_conteudo_p,24,2)||substr(ds_conteudo_p,20,4),'dd/mm/yyyy');
	nr_versao_transacao_w	:= to_number(substr(ds_conteudo_p,28,2));

	select	ptu_movimento_prestador_seq.nextval
	into	nr_seq_movimento_w
	from	dual;

	insert into ptu_movimento_prestador
		(nr_sequencia, cd_unimed_destino, cd_unimed_origem, 
		dt_geracao, dt_atualizacao, nm_usuario, 
		dt_atualizacao_nrec, nm_usuario_nrec, nr_versao_transacao)
	values	(nr_seq_movimento_w, cd_unimed_destino_w, cd_unimed_origem_w,
		dt_geracao_w, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, nr_versao_transacao_w);
end if;

if	(substr(ds_conteudo_p,9,3) = '402') then
	ie_tipo_prestador_w		:= substr(ds_conteudo_p,12,2);
	nr_seq_prestador_w		:= to_number(substr(ds_conteudo_p,14,8));
	cd_cgc_cpf_w			:= substr(ds_conteudo_p,23,14);
	nm_prestador_w			:= substr(ds_conteudo_p,67,40);
	ie_tipo_vinculo_w		:= to_number(substr(ds_conteudo_p,147,1));
	dt_inclusao_w			:= to_date(substr(ds_conteudo_p,174,2)||substr(ds_conteudo_p,172,2)||substr(ds_conteudo_p,168,4),'dd/mm/yyyy');
	ie_tipo_classif_estab_w		:= substr(ds_conteudo_p,185,1);
	ie_categoria_dif_w		:= substr(ds_conteudo_p,186,1);
	ie_acidente_trabalho_w		:= substr(ds_conteudo_p,187,1);
	ie_urgencia_emerg_w		:= substr(ds_conteudo_p,189,1);
	dt_inicio_servico_w		:= to_date(substr(ds_conteudo_p,203,2)||substr(ds_conteudo_p,201,2)||substr(ds_conteudo_p,197,4),'dd/mm/yyyy');
	dt_inicio_contrato_w		:= to_date(substr(ds_conteudo_p,211,2)||substr(ds_conteudo_p,209,2)||substr(ds_conteudo_p,205,4),'dd/mm/yyyy');
	nr_registro_ans_w		:= to_number(substr(ds_conteudo_p,213,6));
	nm_diretor_tecnico_w		:= substr(ds_conteudo_p,219,40);
	nr_insc_estadual_w		:= to_number(substr(ds_conteudo_p,37,20));
	nr_crm_w			:= to_number(substr(ds_conteudo_p,57,8));
	uf_crm_w			:= substr(ds_conteudo_p,65,2);
	nm_fantasia_w			:= substr(ds_conteudo_p,107,40);
	dt_exclusao_w			:= to_date(substr(ds_conteudo_p,202,2)||substr(ds_conteudo_p,199,2)||substr(ds_conteudo_p,176,4),'dd/mm/yyyy');
	ie_tipo_contratualizacao_w	:= substr(ds_conteudo_p,184,1);
	ie_tabela_propria_w		:= substr(ds_conteudo_p,268,1);
	ie_perfil_assistencial_w	:= to_number(substr(ds_conteudo_p,269,2));
	ie_tipo_produto_w		:= to_number(substr(ds_conteudo_p,271,1));
	ie_tipo_disponibilidade_w	:= to_number(substr(ds_conteudo_p,267,1));
	ie_guia_medico_w		:= substr(ds_conteudo_p,272,1);
	nr_seq_conselho_w		:= null;
	nr_cons_diretor_tecnico_w	:= null;
	sg_uf_cons_diretor_tecnico_w	:= null;
	ie_tipo_rede_min_w		:= null;

	select	max(nr_sequencia)
	into	nr_seq_movimento_w
	from	ptu_movimento_prestador;
	
	select	ptu_prestador_seq.nextval
	into	nr_seq_prestador_ptu_w
	from	dual;

	insert into ptu_prestador
		(nr_sequencia, nr_seq_movimento, ie_tipo_prestador,
		nr_seq_prestador, cd_cgc_cpf, nm_prestador, 
		ie_tipo_vinculo, dt_inclusao, ie_tipo_classif_estab, 
		ie_categoria_dif, dt_atualizacao, nm_usuario, 
		dt_atualizacao_nrec, nm_usuario_nrec, ie_acidente_trabalho, 
		ie_urgencia_emerg, dt_inicio_servico, dt_inicio_contrato, 
		nr_registro_ans, nm_diretor_tecnico, nr_insc_estadual, 
		nr_crm, uf_crm, nm_fantasia, 
		dt_exclusao, ie_tipo_contratualizacao, ie_tabela_propria, 
		ie_perfil_assistencial, ie_tipo_produto, nr_seq_conselho, 
		nr_cons_diretor_tecnico, sg_uf_cons_diretor_tecnico, ie_tipo_rede_min, 
		ie_tipo_disponibilidade, ie_guia_medico)
	values	(nr_seq_prestador_ptu_w, nr_seq_movimento_w, ie_tipo_prestador_w,
		nr_seq_prestador_w, cd_cgc_cpf_w, nm_prestador_w,
		ie_tipo_vinculo_w, dt_inclusao_w, ie_tipo_classif_estab_w,
		ie_categoria_dif_w, sysdate, nm_usuario_p, 
		sysdate, nm_usuario_p, ie_acidente_trabalho_w,
		ie_urgencia_emerg_w, dt_inicio_servico_w, dt_inicio_contrato_w,
		nr_registro_ans_w, nm_diretor_tecnico_w, nr_insc_estadual_w,
		nr_crm_w, uf_crm_w, nm_fantasia_w, 
		dt_exclusao_w, ie_tipo_contratualizacao_w, ie_tabela_propria_w,
		ie_perfil_assistencial_w, ie_tipo_produto_w, nr_seq_conselho_w,
		nr_cons_diretor_tecnico_w, sg_uf_cons_diretor_tecnico_w, ie_tipo_rede_min_w,
		ie_tipo_disponibilidade_w, ie_guia_medico_w);

	cd_especialidade_w	:= to_number(substr(ds_conteudo_p,148,4));
	ie_rce_w		:= substr(ds_conteudo_p,190,1);
	cd_prestador_w		:= to_number(substr(ds_conteudo_p,14,8));

	if	(cd_especialidade_w	<> 0) then
		insert into ptu_prestador_espec
			(nr_sequencia, nr_seq_prestador, cd_especialidade, 
			ie_rce, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, ie_principal, 
			nr_seq_apres)
		values	(ptu_prestador_espec_seq.nextval, nr_seq_prestador_ptu_w, cd_especialidade_w,
			ie_rce_w, sysdate, nm_usuario_p, 
			sysdate, nm_usuario_p, 'N',
			cd_especialidade_w);
		
	end if;
	cd_especialidade_w	:= to_number(substr(ds_conteudo_p,152,4));
	ie_rce_w		:= substr(ds_conteudo_p,191,1);

	if	(cd_especialidade_w	<> 0) then
		insert into ptu_prestador_espec
			(nr_sequencia, nr_seq_prestador, cd_especialidade, 
			ie_rce, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, ie_principal, 
			nr_seq_apres)
		values	(ptu_prestador_espec_seq.nextval, nr_seq_prestador_ptu_w, cd_especialidade_w,
			ie_rce_w, sysdate, nm_usuario_p, 
			sysdate, nm_usuario_p, 'N',
			cd_especialidade_w);
	end if;

	cd_especialidade_w	:= to_number(substr(ds_conteudo_p,156,4));
	ie_rce_w		:= substr(ds_conteudo_p,192,1);

	if	(cd_especialidade_w	<> 0) then
		insert into ptu_prestador_espec
			(nr_sequencia, nr_seq_prestador, cd_especialidade, 
			ie_rce, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, ie_principal, 
			nr_seq_apres)
		values	(ptu_prestador_espec_seq.nextval, nr_seq_prestador_ptu_w, cd_especialidade_w, 
			ie_rce_w, sysdate, nm_usuario_p, 
			sysdate, nm_usuario_p, 'N',
			cd_especialidade_w);
	end if;

	cd_especialidade_w	:= to_number(substr(ds_conteudo_p,160,4));
	ie_rce_w		:= substr(ds_conteudo_p,193,1);

	if	(cd_especialidade_w	<> 0) then
		insert into ptu_prestador_espec
			(nr_sequencia, nr_seq_prestador, cd_especialidade, 
			ie_rce, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, ie_principal, 
			nr_seq_apres)
		values	(ptu_prestador_espec_seq.nextval, nr_seq_prestador_ptu_w, cd_especialidade_w,
			ie_rce_w, sysdate, nm_usuario_p, 
			sysdate, nm_usuario_p, 'N',
			cd_especialidade_w);
	end if;

	cd_especialidade_w	:= to_number(substr(ds_conteudo_p,164,4));
	ie_rce_w		:= substr(ds_conteudo_p,194,1);

	if	(cd_especialidade_w	<> 0) then
		insert into ptu_prestador_espec
			(nr_sequencia, nr_seq_prestador, cd_especialidade, 
			ie_rce, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, ie_principal, 
			nr_seq_apres)
		values	(ptu_prestador_espec_seq.nextval, nr_seq_prestador_ptu_w, cd_especialidade_w,
			ie_rce_w, sysdate, nm_usuario_p, 
			sysdate, nm_usuario_p, 'N',
			cd_especialidade_w);
	end if;

end if;

if	(substr(ds_conteudo_p,9,3) = '403') then
	ie_tipo_endereco_w	:= to_number(substr(ds_conteudo_p,12,1));
	ds_endereco_w		:= substr(ds_conteudo_p,13,40);
	cd_municipio_ibge_w	:= to_number(substr(ds_conteudo_p,104,7));
	cd_cep_w		:= to_number(substr(ds_conteudo_p,111,8));
	nr_telefone_w		:= to_number(substr(ds_conteudo_p,123,12));
	cd_cnes_w		:= substr(ds_conteudo_p,249,7);
	cd_cgc_cpf_ww		:= substr(ds_conteudo_p,293,14);
	nr_endereco_w		:= substr(ds_conteudo_p,53,6);
	ds_complemento_w	:= substr(ds_conteudo_p,59,15);
	ds_bairro_w		:= substr(ds_conteudo_p,74,30);
	nr_ddd_w		:= to_number(substr(ds_conteudo_p,119,4));
	nr_telefone_dois_w	:= to_number(substr(ds_conteudo_p,135,12));
	nr_fax_w		:= to_number(substr(ds_conteudo_p,147,12));
	ds_email_w		:= substr(ds_conteudo_p,159,40);
	ds_endereco_web_w	:= substr(ds_conteudo_p,199,50);
	nr_leitos_totais_w	:= to_number(substr(ds_conteudo_p,256,6));
	nr_leitos_contrat_w	:= to_number(substr(ds_conteudo_p,262,6));
	nr_leitos_psiquiatria_w	:= to_number(substr(ds_conteudo_p,268,6));
	nr_uti_adulto_w		:= to_number(substr(ds_conteudo_p,274,6));
	nr_uti_neonatal_w	:= to_number(substr(ds_conteudo_p,280,6));
	nr_uti_pediatria_w	:= to_number(substr(ds_conteudo_p,286,6));

	select	max(nr_sequencia)
	into	nr_seq_prestador_ptu_w
	from	ptu_prestador;
	
	select	ptu_prestador_endereco_seq.nextval
	into	nr_seq_endereco_w
	from	dual;
	
	insert into ptu_prestador_endereco
		(nr_sequencia, nr_seq_prestador, ie_tipo_endereco, 
		ds_endereco, cd_municipio_ibge, cd_cep, 
		nr_telefone, cd_cnes, cd_cgc_cpf, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
		nm_usuario_nrec, nr_endereco, ds_complemento, 
		ds_bairro, nr_ddd, nr_telefone_dois, 
		nr_fax, ds_email, ds_endereco_web, 
		nr_leitos_totais, nr_leitos_contrat, nr_leitos_psiquiatria, 
		nr_uti_adulto, nr_uti_neonatal, nr_uti_pediatria)
	values	(nr_seq_endereco_w , nr_seq_prestador_ptu_w, ie_tipo_endereco_w, 
		ds_endereco_w, cd_municipio_ibge_w, cd_cep_w, 
		nr_telefone_w, cd_cnes_w, cd_cgc_cpf_ww, 
		sysdate, nm_usuario_p, sysdate, 
		nm_usuario_p, nr_endereco_w, ds_complemento_w, 
		ds_bairro_w, nr_ddd_w, nr_telefone_dois_w, 
		nr_fax_w, ds_email_w, ds_endereco_web_w, 
		nr_leitos_totais_w, nr_leitos_contrat_w, nr_leitos_psiquiatria_w, 
		nr_uti_adulto_w, nr_uti_neonatal_w, nr_uti_pediatria_w);
	
end if;

if	(substr(ds_conteudo_p,9,3) = '404') then
	
	cd_grupo_servico_w		:= substr(ds_conteudo_p,12,3);
	
	select	max(nr_sequencia)
	into	nr_seq_endereco_w
	from	ptu_prestador_endereco;
	
	insert into ptu_prestador_grupo_serv
		(nr_sequencia, nr_seq_endereco, cd_grupo_servico, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
		nm_usuario_nrec)
	values	(ptu_prestador_grupo_serv_seq.nextval, nr_seq_endereco_w, cd_grupo_servico_w,
		sysdate, nm_usuario_p, sysdate, 
		nm_usuario_p);
end if;

if	(substr(ds_conteudo_p,9,3) = '405') then
	cd_rede_w	:= substr(ds_conteudo_p,12,5);  
	nm_rede_w	:= substr(ds_conteudo_p,17,40); 
	
	select	max(nr_sequencia)
	into	nr_seq_prestador_ptu_w
	from	ptu_prestador;

	insert into ptu_prestador_rede_ref
		(nr_sequencia, nr_seq_prestador, cd_rede, 
		nm_rede, dt_atualizacao, nm_usuario, 
		dt_atualizacao_nrec, nm_usuario_nrec)
	values	(ptu_prestador_rede_ref_seq.nextval, nr_seq_prestador_ptu_w, cd_rede_w,
		nm_rede_w, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p);

end if;

commit;
end pls_ptu_imp_prestador;
/
