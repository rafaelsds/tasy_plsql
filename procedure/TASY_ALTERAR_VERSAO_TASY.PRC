create or replace
procedure tasy_alterar_versao_tasy(cd_versao_p varchar2, nm_usuario_p varchar2) is

jobno	NUMBER(10);

begin
	update aplicacao_tasy set cd_versao_atual = cd_versao_p, dt_atualizacao = sysdate, nm_usuario = nm_usuario_p
	where upper(cd_aplicacao_tasy) in ('TASY', 'TASYGER', 'TASYMED', 'TASYMON', 'TASYREL', 'TASYONC', 'TASYSUSUNI');
	commit;
	
	update	ajuste_versao_base_cliente
	set	ie_compila = 'S';
	commit;
	
	dbms_job.submit(jobno, 'tasy_criar_trigger_log_update;');
	commit;	
	
end tasy_alterar_versao_tasy;
/