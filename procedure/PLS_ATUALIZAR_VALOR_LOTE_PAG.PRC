create or replace
procedure pls_atualizar_valor_lote_pag
			(	nr_seq_pagamento_prest_p	number,
				nm_usuario_p			varchar2) is 

vl_itens_w		number(15,2);
vl_pagamentos_w		number(15,2);
nr_seq_lote_w		number(10);
				
begin			
if	(nr_seq_pagamento_prest_p is not null) then	
	select	nvl(sum(a.vl_item), 0)
	into	vl_itens_w
	from	pls_pagamento_item	a
	where	a.nr_seq_pagamento	= nr_seq_pagamento_prest_p
	and	nvl(a.ie_apropriar_total, 'N') = 'N';	
	
	update	pls_pagamento_prestador
	set	vl_pagamento 	= vl_itens_w,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_pagamento_prest_p;
	
	select	max(nr_seq_lote)
	into	nr_seq_lote_w
	from	pls_pagamento_prestador
	where 	nr_sequencia = nr_seq_pagamento_prest_p;
	
	if	(nr_seq_lote_w is not null) then
		select	sum(nvl(vl_pagamento,0))
		into	vl_pagamentos_w
		from	pls_pagamento_prestador
		where	nr_seq_lote	= nr_seq_lote_w;
			
		update	pls_lote_pagamento
		set	vl_lote		= vl_pagamentos_w
		where	nr_sequencia	= nr_seq_lote_w;
	end if;
end if;

end pls_atualizar_valor_lote_pag;
/