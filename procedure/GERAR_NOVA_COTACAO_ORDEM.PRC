create or replace
procedure gerar_nova_cotacao_ordem(
				nr_ordem_compra_p		number,
				nr_item_oci_p			number,
				nr_sequencia_p			number, /*sequencia da tabela ordem_compra_item_entrega*/
				nm_usuario_p			varchar2,
				nr_cot_compra_p	in 	out	number,
				nr_item_cot_compra_p	out	number) is 

qt_dias_param_w				number(5);
nr_cot_compra_w				number(10);
nr_cot_compra_orig_w			number(10);
nr_solic_compra_orig_w				ordem_compra_item.nr_solic_compra%type;
nr_item_solic_compra_orig_w			ordem_compra_item.nr_item_solic_compra%type;
nr_item_w				number(5);
nr_item_cot_compra_existente_w		number(5);
nr_item_cot_compra_w			number(5);
cd_material_w				number(6);
cd_unidade_medida_compra_w		varchar2(30);
qt_material_w				number(13,4);
nr_item_cot_compra_orig_w		number(5);
dt_limite_entrega_w			date;
cd_cgc_fornecedor_w			varchar2(14);
cd_cgc_fornecedor_ww			varchar2(14);
nr_seq_fornec_orig_w			number(10);
nr_seq_cot_fornec_w			number(10);
nr_seq_cot_forn_item_orig_w		number(10);
nr_seq_cot_forn_item_w			number(10);
dt_retorno_prev_w			date;
cd_estabelecimento_w			number(4);
qt_dias_w				number(10);
qt_prevista_entrega_w			number(13,4);
qt_prevista_entrega_ww			number(13,4);
qt_real_entrega_w			number(13,4);
dt_prevista_entrega_w			date;

cursor C01 is
select	nr_item_cot_compra
from	ordem_compra_item
where	nr_ordem_compra	= nr_ordem_compra_p
and	nr_item_oci = nvl(nr_item_oci_p,nr_item_oci)
and	nr_item_cot_compra is not null;

begin

nr_cot_compra_w	:= nr_cot_compra_p;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

select	nvl(Obter_Valor_Param_Usuario(915, 2, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),3)
into	qt_dias_w
from	dual;

select	nvl(max(nr_cot_compra), 0),
		max(nr_solic_compra),
		max(nr_item_solic_compra)
into	nr_cot_compra_orig_w,
		nr_solic_compra_orig_w,
		nr_item_solic_compra_orig_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nvl(nr_item_oci_p,nr_item_oci);

if	(nr_cot_compra_orig_w > 0) then

	if	(nr_cot_compra_p = 0) then
		select	cot_compra_seq.nextval
		into	nr_cot_compra_w
		from	dual;

		select	obter_dia_hora_util_periodo(cd_estabelecimento_w,sysdate,qt_dias_w)
		into	dt_retorno_prev_w
		from	dual;

		insert into cot_compra(
			nr_cot_compra,
			dt_cot_compra,
			dt_atualizacao,
			cd_comprador,
			nm_usuario,
			ds_observacao,
			cd_pessoa_solicitante,
			cd_estabelecimento,
			dt_retorno_prev,
			dt_entrega,
			nr_classif_interno,     
			nr_seq_subgrupo_compra,
			ie_finalidade_cotacao)
		select	nr_cot_compra_w,
			sysdate,
			sysdate,
			cd_comprador,
			nm_usuario_p,
			decode(ds_observacao,null,Wheb_mensagem_pck.get_Texto(301889, 'NR_ORDEM_COMPRA_P='|| NR_ORDEM_COMPRA_P ),
				substr(ds_observacao,1,3700) || chr(13) || chr(10) || Wheb_mensagem_pck.get_Texto(301889, 'NR_ORDEM_COMPRA_P='|| NR_ORDEM_COMPRA_P)), /*Gerada no cancelamento da Ordem de compra n�mero #@NR_ORDEM_COMPRA_P#@.*/
			cd_pessoa_solicitante,
			cd_estabelecimento,
			dt_retorno_prev_w,
			dt_entrega,
			nr_classif_interno,     
			nr_seq_subgrupo_compra,
			'C'
		from	cot_compra
		where	nr_cot_compra = nr_cot_compra_orig_w;
	else
		nr_cot_compra_w := nr_cot_compra_p;
	end if;

	open C01;
	loop
	fetch C01 into	
		nr_item_cot_compra_orig_w;
	exit when C01%notfound;
		begin

		if	(nr_cot_compra_p > 0) then
			select	nvl(max(nr_item_cot_compra),0) +1
			into	nr_item_cot_compra_existente_w
			from	cot_compra_item
			where	nr_cot_compra = nr_cot_compra_w;
		end if;

		if	(nr_sequencia_p > 0) then

			select	trunc(dt_prevista_entrega)
			into	dt_prevista_entrega_w
			from	ordem_compra_item_entrega
			where	nr_sequencia = nr_sequencia_p;

			select	nvl(sum(qt_prevista_entrega),0),
				nvl(sum(qt_real_entrega),0)
			into	qt_prevista_entrega_w,
				qt_real_entrega_w
			from	ordem_compra_item_entrega
			where	nr_ordem_compra = nr_ordem_compra_p
			and	nr_item_oci = nr_item_oci_p
			and	trunc(dt_prevista_entrega) = dt_prevista_entrega_w;

			qt_prevista_entrega_ww := (qt_prevista_entrega_w - qt_real_entrega_w);
		end if;

		nr_item_cot_compra_w := nvl(nr_item_cot_compra_existente_w, nr_item_cot_compra_orig_w);

		insert into cot_compra_item(
			nr_cot_compra,
			nr_item_cot_compra,
			cd_material,
			qt_material,
			cd_unidade_medida_compra,
			dt_atualizacao,
			dt_limite_entrega,
			nm_usuario,
			ie_situacao,
			ds_material_direto_w,
			nr_solic_compra,
			nr_item_solic_compra,
			cd_estab_item,
			nr_seq_proj_rec)
		select	nr_cot_compra_w,
			nr_item_cot_compra_w,
			cd_material,
			nvl(qt_prevista_entrega_ww,qt_material),
			cd_unidade_medida_compra,
			sysdate,
			dt_limite_entrega,
			nm_usuario_p,
			ie_situacao,
			ds_material_direto_w,
			nr_solic_compra_orig_w,
			nr_item_solic_compra_orig_w,
			cd_estab_item,
			nr_seq_proj_rec
		from	cot_compra_item
		where	nr_cot_compra		= nr_cot_compra_orig_w
		and	nr_item_cot_compra	= nr_item_cot_compra_orig_w;

		-- Para gerar uma nova cotacao a partir de item da OC, deve-se estornar baixa em ambos item e solicitacao de compra.
		-- O numero da nova cotacao deve ser substituido na solicita��o de compras
		update	solic_compra_item
		set	nr_cot_compra		= nr_cot_compra_w,
			nr_item_cot_compra	= nr_item_cot_compra_w,
			cd_motivo_baixa		= null,
			dt_baixa		= null
		where	nr_solic_compra	= nr_solic_compra_orig_w
		and 	nr_item_solic_compra = nr_item_solic_compra_orig_w;
		
		update	solic_compra
		set	cd_motivo_baixa		= null,
			dt_baixa		= null
		where	nr_solic_compra = nr_solic_compra_orig_w;
		
		insert into cot_compra_item_entrega(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_cot_compra,
			nr_item_cot_compra,
			dt_entrega,
			qt_entrega)
		values	(cot_compra_item_entrega_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_cot_compra_w,
			nr_item_cot_compra_w,
			nvl(dt_prevista_entrega_w, (select dt_limite_entrega from cot_compra_item where	nr_cot_compra = nr_cot_compra_w and nr_item_cot_compra = nr_item_cot_compra_w)),
			nvl(qt_prevista_entrega_ww,(select qt_material from cot_compra_item where	nr_cot_compra = nr_cot_compra_w and nr_item_cot_compra = nr_item_cot_compra_w)));
		end;
	end loop;
	close C01;

	gerar_hist_cotacao_sem_commit(
		nr_cot_compra_w,
		Wheb_mensagem_pck.get_Texto(301895), /*'Cota��o gerada pelo cancelamento de uma Ordem de compra',*/
		Wheb_mensagem_pck.get_Texto(301896, 'NR_ORDEM_COMPRA_P='|| NR_ORDEM_COMPRA_P ),/*	'Essa cota��o de compras foi gerada a partir do cancelamento da ordem de compra n�mero: ' || NR_ORDEM_COMPRA_P || '.' || chr(13) || chr(10) ||		'Os itens desta cota��o s�o os mesmo itens da ordem de compra.',*/
		'S',nm_usuario_p);

end if;

nr_cot_compra_p		:= nr_cot_compra_w;
nr_item_cot_compra_p	:= nr_item_cot_compra_w;

commit;

end gerar_nova_cotacao_ordem;
/