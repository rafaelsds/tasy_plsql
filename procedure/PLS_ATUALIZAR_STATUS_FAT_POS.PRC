create or replace
procedure pls_atualizar_status_fat_pos
			(	nr_seq_conta_p		number,
				nr_seq_mat_p		number,
				nr_seq_proc_p		number,
				nr_seq_analise_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is

/*

	ESSA ROTINA DEU ORIGEM  DA PLS_ATUAL_STATUS_FAT_POS_ESTAB DEVIDO A REESTRUTURA��O 
	DE GERA��O DE P�S-ETABELECIDO. ENQUANTO A ANTIGA GERA��O DE P�S-ESTABELECIDO N�O 
	FOR DESCONTINUADA, AS ALTERA��ES EM UMA DAS ROTINAS DEVEM SER REFLETIDAS NA OUTRA.

*/				
				
nr_seq_proc_w		number(10);
nr_seq_mat_w		number(10);
nr_seq_item_analise_w	number(10);
ie_pagamento_w		varchar2(1);
nr_seq_item_princ_w	number(10);
qt_itens_w		number(10);
nr_seq_analise_ref_w	number(10);
ie_faturamento_w	Varchar2(2);
ie_status_faturamento_w	Varchar2(2);
ie_status_w		Varchar2(2);
Cursor C01 is
	select	decode(ie_tipo_item,'P',nr_seq_item,null),
		decode(ie_tipo_item,'M',nr_seq_item,null)
	from	w_pls_resumo_conta
	where	nr_seq_conta	= nr_seq_conta_p
	and	nr_seq_analise	= nr_seq_analise_p;

begin

/*
A rotina de status de faturamento p�s. verifica o status de pagamento e faturamento do item  na an�lise de produ��o m�dica e retorna este
para ser atualizado no campo ie_status fatura na pls_conta_pos_estabelecido.
*/

if	(nvl(nr_seq_mat_p,0) = 0) and (nvl(nr_seq_proc_p,0) = 0) then

	open C01;
	loop
	fetch C01 into
		nr_seq_proc_w,
		nr_seq_mat_w;
	exit when C01%notfound;
		begin
		if	((nvl(nr_seq_proc_w,0) <> 0) or (nvl(nr_seq_mat_w,0) <> 0)) then
			pls_atualizar_status_fat_pos(	nr_seq_conta_p,	nr_seq_mat_w, nr_seq_proc_w,
							nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);
		end if;

		end;
	end loop;
	close C01;
	
else	
	select	max(ie_faturamento),
		max(ie_pagamento),
		max(ie_status)
	into	ie_faturamento_w,
		ie_pagamento_w,
		ie_status_w
	from	w_pls_resumo_conta
	where	nr_seq_conta = nr_seq_conta_p
	and 	(((nr_seq_item = nr_seq_proc_p) and (ie_tipo_item = 'P'))
	or 	 ((nr_seq_item = nr_seq_mat_p) 	and (ie_tipo_item = 'M')))
	and	nr_seq_analise = nr_seq_analise_p;

	if	(ie_status_w = 'P') then
		if 	((ie_faturamento_w <> 'G') and
			(ie_pagamento_w <> 'G' )) then
			ie_status_faturamento_w := 'P';
		else
			ie_status_faturamento_w := 'N';
		end if;
	elsif	(ie_status_w = 'L') then
		if 	((ie_faturamento_w <> 'G') and
			(ie_pagamento_w <> 'G' )) then
			ie_status_faturamento_w := 'L';
		else
			ie_status_faturamento_w := 'N';
		end if;
	else
		ie_status_faturamento_w := 'D';
	end if;
	
	if	(nvl(nr_seq_proc_p,0) > 0) then
		update	pls_conta_pos_estabelecido
		set	ie_status_faturamento  	= ie_status_faturamento_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_conta_proc	= nr_seq_proc_p
		and	nr_seq_conta		= nr_seq_conta_p
		and	nr_seq_lote_fat		is null
		and	((nr_seq_analise	is null) or (nr_seq_analise	= nr_seq_analise_p))
		and	((ie_situacao		= 'A') or (ie_situacao	is null));
	elsif	(nvl(nr_seq_mat_p,0) > 0) then
		update	pls_conta_pos_estabelecido
		set	ie_status_faturamento  	= ie_status_faturamento_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_conta_mat	= nr_seq_mat_p
		and	nr_seq_conta		= nr_seq_conta_p
		and	nr_seq_lote_fat		is null
		and	((nr_seq_analise	is null) or (nr_seq_analise	= nr_seq_analise_p))
		and	((ie_situacao		= 'A') or (ie_situacao	is null));
	end if;
		
end if;

end pls_atualizar_status_fat_pos;
/
