create or replace
procedure ehr_gera_valor_grid_clus_rel_s(		cd_setor_atendimento_p	number,
						dt_inicial_p		date,
						dt_final_p		date) is 
						

nr_seq_template_w		number(10);	
nr_seq_template_custer_w	number(10);	
ds_aux_w			varchar2(255);
nr_atendimento_w		number(10);
nr_seq_reg_template_w		number(10);				
						
Cursor C01 is
	select	NR_SEQ_TEMPLATE_CLUSTER
	from	ehr_template_conteudo
	where	nr_seq_template	= nr_seq_template_w;
	
Cursor C02 is
	SELECT	nr_atendimento
	FROM	atendimento_paciente
	WHERE	obter_setor_atendimento(nr_atendimento) = cd_setor_atendimento_p
	AND	dt_entrada BETWEEN dt_inicial_p AND dt_final_p;
	
Cursor	C03 is 
	SELECT	MAX(b.nr_sequencia)
	FROM	EHR_REGISTRO a,
		ehr_reg_template b
	WHERE  	a.nr_sequencia = b.nr_seq_reg
	AND	nr_atendimento = nr_atendimento_w;
begin

open C02;
loop
fetch C02 into	
	nr_atendimento_w;
exit when C02%notfound;
	begin
	open C03;
	loop
	fetch C03 into	
		nr_seq_reg_template_w;
	exit when C03%notfound;
		begin
		select	max(nr_seq_template)
		into	nr_seq_template_w
		from	ehr_reg_template
		where	nr_sequencia	= nr_seq_reg_template_w;
		open C01;
		loop
		fetch C01 into	
			nr_seq_template_custer_w;
		exit when C01%notfound;
			begin
			begin
			ehr_gerar_tabela_cluster (nr_seq_template_custer_w,null,ds_aux_w,ds_aux_w,ds_aux_w,ds_aux_w,ds_aux_w);
			exception
			when others then
				null;
			end;
			ehr_gerar_valores_grid_cluster(nr_seq_reg_template_w,nr_seq_template_custer_w);
			exception
				when others then
				null;
			end;
		end loop;
		close C01;
		end;
	end loop;
	close C03;
	end;
end loop;
close C02;

commit;

end ehr_gera_valor_grid_clus_rel_s;
/