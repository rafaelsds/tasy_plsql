create or replace procedure cancel_cons_agenda(
	nr_seq_agenda_cons_p	parecer_medico_req.nr_seq_agenda_cons%type) is

	nr_sequencia_w		agenda_consulta.nr_sequencia%type;

begin

	begin
		if (nr_seq_agenda_cons_p is not null) then

			select	nr_sequencia
			into	nr_sequencia_w
			from	agenda_consulta
			where	nr_sequencia = nr_seq_agenda_cons_p;

			if (nvl(nr_sequencia_w,nr_seq_agenda_cons_p) is not null) then
	
				update	agenda_consulta
				set	ie_status_agenda = 'C',
					dt_cancelamento = sysdate, 
					nm_usuario_cancelamento = wheb_usuario_pck.get_nm_usuario,
					dt_atualizacao = sysdate,
					nm_usuario = wheb_usuario_pck.get_nm_usuario
				where	nr_sequencia = nvl(nr_sequencia_w,nr_seq_agenda_cons_p);
				
				commit;
			end if;

		end if;
	end;

end cancel_cons_agenda;
/
