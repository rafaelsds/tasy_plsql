create or replace
procedure	incluir_marca_adic_item_solic(
			nr_solic_compra_p			number,
			nr_item_solic_compra_p		number,
			ds_lista_marca_p			varchar2,
			nm_usuario_p			varchar2) is

begin

update	solic_compra_item
set	ds_marcas_aceitaveis = ds_lista_marca_p,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_solic_compra = nr_solic_compra_p
and	nr_item_solic_compra = nr_item_solic_compra_p;

commit;

end incluir_marca_adic_item_solic;
/