create or replace PROCEDURE SET_STERILE_PREPARATION(
    NR_PRESCRICAO_P prescr_solucao_evento.NR_PRESCRICAO%TYPE,
    NR_SEQ_SOLUCAO_P prescr_solucao_evento.NR_SEQ_SOLUCAO%TYPE,
    NR_SEQ_MATERIAL_P prescr_mat_hor.CD_MATERIAL%TYPE,
    IE_STERILE_PREPARATION_P prescr_solucao_evento.ie_sterile_preparation%TYPE
) AS
    NR_SEQUENCIA_W prescr_solucao_evento.NR_SEQUENCIA%TYPE;
BEGIN
    SELECT 
        MAX(b.NR_SEQUENCIA) NR_SEQUENCIA
    INTO NR_SEQUENCIA_W
    FROM prescr_mat_hor a 
    INNER JOIN prescr_solucao_evento b ON
        a.NR_PRESCRICAO = b.NR_PRESCRICAO AND
        a.NR_SEQ_SOLUCAO = b.NR_SEQ_SOLUCAO AND
        TRUNC(b.dt_atualizacao,'DD') = TRUNC(sysdate,'DD') 
    WHERE 
        a.NR_PRESCRICAO = NR_PRESCRICAO_P AND 
        a.CD_MATERIAL = NR_SEQ_MATERIAL_P AND
        a.NR_SEQ_SOLUCAO = NR_SEQ_SOLUCAO_P AND
        b.ie_alteracao = 35;
    if (NR_SEQUENCIA_W is not null) THEN
        UPDATE prescr_solucao_evento SET 
            IE_STERILE_PREPARATION = IE_STERILE_PREPARATION_P 
        WHERE
            NR_SEQUENCIA = NR_SEQUENCIA_W;
    END IF;
END SET_STERILE_PREPARATION;
/