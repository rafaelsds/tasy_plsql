create or replace
procedure broker_Lab_ins_item_micro_res(	nr_prescricao_p		number,
						nr_seq_prescr_p		number,
						cd_exame_p		varchar2,
						cd_microorganismo_p	varchar2,
						cd_medicamento_p	varchar2,
						qt_microorganismo_p	varchar2,
						qt_mic_p		varchar2,
						ie_result_p		varchar2,
						nm_usuario_p		Varchar2,
						ds_erro_p	out	Varchar2) is 
nr_Seq_resultado_w	number(10);
nr_seq_exame_atual_w	number(10);
nr_seq_superior_w	number(10);
nr_seq_exame_w		number(10);
nr_seq_prescr_nova_w	number(6);
nr_Seq_result_item_w	number(10);
cd_microorganismo_w	number(10);
cd_medicamento_w	number(10);

Cursor C01 is
	select	nr_seq_exame,
		nr_seq_superior
	from	exame_laboratorio
	where nvl(cd_exame_integracao, cd_exame) = cd_exame_p
	order by 1;

begin

open C01;
loop
fetch C01 into	
	nr_seq_exame_atual_w,
	nr_seq_superior_w;
exit when C01%notfound;
	begin

	nr_seq_exame_w	:= nr_seq_exame_atual_w;

	select max(nvl(nr_sequencia, null))
	into	nr_seq_prescr_nova_w
	from	prescr_procedimento
	where	nr_prescricao		= nr_prescricao_p
	  and	nr_seq_exame		= nr_seq_exame_atual_w;

	if	(nr_seq_prescr_nova_w is null) then
	
		select	max(nvl(nr_sequencia, null))
		into	nr_seq_prescr_nova_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		  and	ie_status_atend >= 30
		  and	nr_seq_exame = nr_seq_superior_w;
		  
		  if	(nr_seq_prescr_nova_w is null) then
		
			select	max(nvl(nr_sequencia, null))
			into	nr_seq_prescr_nova_w
			from	prescr_procedimento
			where	nr_prescricao = nr_prescricao_p
			and	nr_seq_exame = nr_seq_superior_w;			
		end if;
	end if;
	
	if	(nr_seq_prescr_nova_w is not null) then
		exit;
	end if;	
	
	end;
end loop;
close C01;

nr_seq_exame_w	:= nr_seq_exame_atual_w;

select 	max(nr_Seq_resultado)
into	nr_Seq_resultado_w
from	exame_lab_resultado
where 	nr_prescricao = nr_prescricao_p;

select 	max(nr_sequencia)
into	nr_Seq_result_item_w
from	exame_lab_result_item
where 	nr_Seq_resultado = nr_Seq_resultado_w
and	nr_seq_prescr 	= nr_seq_prescr_p
and	nr_seq_exame	= nr_seq_exame_w;

if (nr_Seq_result_item_w is null) then
	select 	max(nr_sequencia)
	into	nr_Seq_result_item_w
	from	exame_lab_result_item
	where 	nr_Seq_resultado = nr_Seq_resultado_w
	and	nr_seq_prescr 	= nr_seq_prescr_p;
end if;

select nvl(MAX(cd_microorganismo),0)
into   cd_microorganismo_w
from   cih_microorganismo_int
where  cd_microorganismo_integracao = cd_microorganismo_p;

if	(cd_microorganismo_w = 0) then

	select	max(cd_microorganismo)
	into	cd_microorganismo_w
	from	CIH_MICROORGANISMO
	where	nvl(DS_MICROORGANISMO_INTEGR,CD_MICROORGANISMO) = cd_microorganismo_p;

end if;	

select nvl(MAX(cd_medicamento),0)
into   cd_medicamento_w
from   cih_medicamento_int
where  cd_medicamento_integracao = cd_medicamento_p;

if	(cd_medicamento_w = 0) then
	select	max(cd_medicamento)
	into	cd_medicamento_w
	from	CIH_MEDICAMENTO
	where	nvl(DS_CODIGO_INTEGR,cd_medicamento) = cd_medicamento_p;
end if;
if	(nr_Seq_result_item_w is null) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280066,'CD_EXAME='||cd_exame_p ||';NR_PRESCRICAO='|| nr_prescricao_p||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
elsif	(cd_microorganismo_w is null) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277533,'CD_MICROORGANISMO='||cd_microorganismo_p ||';NR_PRESCRICAO='|| nr_prescricao_p||';NR_SEQ_PRESCR='|| nr_seq_prescr_p);
elsif	(cd_medicamento_w is null) then
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280070,'CD_MEDICAMENTO='||cd_medicamento_p ||';NR_PRESCRICAO='||nr_prescricao_p ||';NR_SEQ_PRESCR='|| nr_seq_prescr_p);
end if;
	
if	(nr_Seq_resultado_w is not null) and
	(cd_microorganismo_w is not null) and
	(cd_medicamento_w is not null) and
	(nr_Seq_result_item_w is not null) then
	
	insert	into exame_lab_result_antib 	(NR_SEQUENCIA,
						NR_SEQ_RESULTADO,
						NR_SEQ_RESULT_ITEM,
						CD_MICROORGANISMO,
						CD_MEDICAMENTO,
						IE_RESULTADO,
						DT_ATUALIZACAO,
						DS_RESULTADO_ANTIB,
						NM_USUARIO,
						QT_MICROORGANISMO)
		values				(exame_lab_result_antib_seq.nextval,
						nr_Seq_resultado_w,
						nr_Seq_result_item_w,
						cd_microorganismo_w,
						cd_medicamento_w,
						ie_result_p,
						sysdate,
						qt_mic_p,
						nm_usuario_p,
						qt_microorganismo_p);		
end if;

end broker_Lab_ins_item_micro_res;
/