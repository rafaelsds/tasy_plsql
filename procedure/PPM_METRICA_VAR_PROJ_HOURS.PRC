create or replace
procedure PPM_METRICA_VAR_PROJ_HOURS(
			nr_seq_metrica_p		number,
			nr_seq_objetivo_metrica_p	number,
			cd_pessoa_fisica_p		varchar2,
			nm_usuario_p			varchar2,
			dt_referencia_p			date) is
			
dt_referencia_w 	date;
resultado_w		number;
qt_hora_prev_w		number;
qt_hora_real_w		number;

begin

dt_referencia_w := pkg_date_utils.start_of(dt_referencia_p,'MONTH');
			
select	sum(qt_hora_prev), 
	sum(qt_hora_real)
into	qt_hora_prev_w,
	qt_hora_real_w
from	(--Etapas do projeto em que a PF � recurso
	select  sum(qt_hora_prev) qt_hora_prev, 
		sum(qt_hora_real) qt_hora_real
	from    proj_projeto a,
		proj_cronograma b,
		proj_cron_etapa c
	where   a.nr_sequencia = b.nr_seq_proj
	and     c.nr_seq_cronograma = b.nr_sequencia
	and     c.ie_fase = 'N'
	and     b.ie_tipo_cronograma = 'I'
	and     pkg_date_utils.start_of(c.dt_fim_real,'MONTH') = dt_referencia_w
	and   	exists (select 1
			from  proj_cron_etapa_equipe x
			where x.nr_seq_etapa_cron = c.nr_sequencia
			and   x.cd_programador = cd_pessoa_fisica_p)
	union	--Ou, etapas de projetos em que a PF faz parte da equipe e o projeto tem apenas 1 ANALISTA DE SISTEMAS
	select  sum(qt_hora_prev) qt_hora_prev, 
		sum(qt_hora_real) qt_hora_real
	from    proj_projeto a,
		proj_cronograma b,
		proj_cron_etapa c
	where   a.nr_sequencia = b.nr_seq_proj
	and     c.nr_seq_cronograma = b.nr_sequencia
	and     c.ie_fase = 'N'
	and     b.ie_tipo_cronograma = 'I'
	and	lower(trim(c.ds_atividade)) like 'analyst' --apenas as etapas de an�lise
	and     pkg_date_utils.start_of(c.dt_fim_real,'MONTH') = dt_referencia_w
	and   	exists	(select	1 
			from	proj_equipe_papel x,
				proj_equipe y
			where	x.nr_seq_equipe = y.nr_sequencia
			and	y.nr_seq_proj = a.nr_sequencia
			and	x.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	x.ie_situacao = 'A'
			and	x.nr_seq_funcao = 43
			and	(select	count(*)
				from	proj_equipe_papel d,
					proj_equipe e
				where	d.nr_seq_equipe = e.nr_sequencia
				and	e.nr_seq_proj = a.nr_sequencia
				and	d.ie_situacao = 'A'
				and	d.nr_seq_funcao in (43,45)) = 1));		
				
resultado_w	:= trunc(dividir((qt_hora_prev_w-qt_hora_real_w) * 100,qt_hora_prev_w));			

PPM_GRAVAR_RESULTADO(nr_seq_objetivo_metrica_p,dt_referencia_p, resultado_w, qt_hora_prev_w, (qt_hora_prev_w-qt_hora_real_w), nm_usuario_p);

end PPM_METRICA_VAR_PROJ_HOURS;
/