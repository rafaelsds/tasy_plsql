create or replace
procedure ptu_imp_hash_resposta_a1100(	nr_seq_cab_resp_p				ptu_cabecalho_a1100.nr_sequencia%type,
					ds_hash_p					ptu_hash_a1100.ds_hash%type,
					nm_usuario_p					usuario.nm_usuario%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar o HASH do arquivo de resposta do A1100
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[]  Objetos do dicion�rio [] Tasy (Delphi/Java) [] Portal []  Relat�rios [X] Outros:Web Service
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

insert into ptu_hash_a1100
	(nr_sequencia, ds_hash, dt_atualizacao,
	dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
	nr_seq_cabecalho)
values	(ptu_hash_a1100_seq.nextval, ds_hash_p, sysdate,
	sysdate, nm_usuario_p, nm_usuario_p,
	nr_seq_cab_resp_p);

commit;

end ptu_imp_hash_resposta_a1100;
/