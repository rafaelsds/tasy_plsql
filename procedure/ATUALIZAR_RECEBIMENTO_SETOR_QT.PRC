create or replace
PROCEDURE atualizar_recebimento_setor_qt(nr_seq_atendimento_p NUMBER,
   nm_usuario_p  VARCHAR2) IS

BEGIN
IF  (nr_seq_atendimento_p IS NOT NULL) AND
 (nm_usuario_p IS NOT NULL) THEN

 UPDATE  paciente_atendimento
        SET  dt_recebimento_medic  =  SYSDATE,
  nm_usuario   =  nm_usuario_p,
  nm_usuario_receb  =   nm_usuario_p
        WHERE  nr_seq_atendimento  =  nr_seq_atendimento_p;
END IF;

COMMIT;

END atualizar_recebimento_setor_qt;
/