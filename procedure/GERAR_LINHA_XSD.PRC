create or replace
procedure gerar_linha_xsd
			(nr_seq_item_xsd_p		in	xsd_item.nr_sequencia%type,
			 ds_linha_p				in	xsd_item_linha.ds_linha%type,
			 nm_usuario_p			in	usuario.nm_usuario%type) is
			 
begin

insert	into xsd_item_linha
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_item_xsd,
		ds_linha)
values	(xsd_item_linha_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_item_xsd_p,
		ds_linha_p);

end gerar_linha_xsd;
/
