create or replace
procedure pls_gerar_login_prestador(	nr_seq_usuario_p		Number,	
					nr_seq_local_atend_p		Number,
					nm_usuario_p			Varchar2,
					ds_email_p			out	Varchar2,
					ds_responsavel_p		out	Varchar2,
					nr_crm_p			out	Varchar2,
					cd_pessoa_fisica_resp_p		out	Varchar2,
					cd_estabelecimento_p		out	Number,
					cd_cooperativa_p		out	Varchar2,
					ie_exige_biometria_p		out	Varchar2,
					nr_seq_perfil_web_p		out	Number,
					ie_tipo_prestador_p		out	Varchar2,
					qt_dias_protocolo_p		out	Number,
					ie_forma_autenticacao_p		out	Varchar2,
					nm_prestador_p			out	Varchar2,
					nr_seq_prestador_p		out	Number,
					cd_pessoa_fisica_p		out	Varchar2,
					qt_prestadores_login_p		out	Number,
					ie_origem_prestador_p		out	Varchar2,
					cd_prestador_p			out	Varchar2,
					nm_local_atend_p		out	Varchar2,
					uf_crm_p			out	Varchar2,
					qt_max_caract_cart_p		out	Number,
					ie_forma_val_cart_benef_p  	out	pls_web_param_geral.ie_forma_val_cart_benef%type,
					cd_versao_tiss_p		out 	pls_versao_tiss.cd_versao_tiss%type,
					ie_nova_importacao_p		out	varchar2,
					ip_socket_scs_p			out	pls_end_webservice_scs.ds_webservice_envio%type,
					dt_ultimo_acesso_p		out	varchar2) is
				

nr_seq_local_atend_w	Number(10);
qt_prestadores_login_w	Number(5);
ds_mensagem_log_w	Varchar2(255);
cd_versao_w		pls_versao_tiss.cd_versao_tiss%type;
				
begin


select 	nvl((	select	max(pls_imp_xml_cta_pck.usar_nova_imp_xml(obter_estabelecimento_ativo))
		from	dual),'N')
into	ie_nova_importacao_p
from 	dual;

select	count(1)
into	qt_prestadores_login_w
from    pls_prestador a,
	pls_prestador_usuario_web b
where   a.nr_sequencia 		= b.nr_seq_prestador
and	a.ie_situacao 		= 'A'
and     b.ie_situacao 		= 'A'  
and     b.nr_seq_usuario 	= nr_seq_usuario_p
order   by a.nr_sequencia asc;

--Caso necessitar de altera��o verificar se precisa ser alterado o script 379351
select	max(ds_webservice_envio) ds_webservice_envio
into	ip_socket_scs_p
from	pls_end_webservice_scs
where	ie_situacao = 'A';

if	(qt_prestadores_login_w > 0) then
	ie_origem_prestador_p := 'LOGIN';

	select  nvl(a.ds_email,'n�o informado') ds_email,
		a.cd_pessoa_fisica_resp,
		nvl(obter_nome_pf(a.cd_pessoa_fisica_resp), 'n�o informado') ds_resp,
		obter_crm_medico(a.cd_pessoa_fisica_resp) nr_crm ,
		NVL(a.cd_estabelecimento, 1) cd_estabelecimento,
		a.ie_exige_biometria, 
		nvl(pls_obter_unimed_estab(a.cd_estabelecimento), '0') cd_cooperativa, 
		pls_obter_dados_prestador_web(a.nr_sequencia, 'PW') nr_seq_perfil_web,                         
		a.nr_seq_local_atend,
		pls_obter_local_atend_medico(a.nr_seq_local_atend),
		pls_obter_uf_crm_medico(a.cd_pessoa_fisica_resp),
		nvl(pls_obter_versao_tiss, '3.02.00')
	into	ds_email_p,
		cd_pessoa_fisica_resp_p,
		ds_responsavel_p,
		nr_crm_p,
		cd_estabelecimento_p,
		ie_exige_biometria_p,
		cd_cooperativa_p,
		nr_seq_perfil_web_p,
		nr_seq_local_atend_w,
		nm_local_atend_p,
		uf_crm_p,
		cd_versao_w
	from    pls_usuario_web a
	where   a.nr_sequencia = nr_seq_usuario_p;
		
	select *	
	into	nm_prestador_p,
		cd_pessoa_fisica_p,
		nr_crm_p,
		nr_seq_prestador_p,
		ie_tipo_prestador_p,
		qt_dias_protocolo_p,
		ie_forma_autenticacao_p	,
		cd_prestador_p,
		uf_crm_p,
		ie_forma_val_cart_benef_p,
		qt_max_caract_cart_p
	from	(select initcap(substr(pls_obter_nm_prest_web(a.nr_sequencia),1,255)) nm_prestador,
		a.cd_pessoa_fisica cd_pessoa_fisica,
		obter_crm_medico(a.cd_pessoa_fisica) nr_crm ,
		a.nr_sequencia nr_seq_prestador,
		substr(pls_obter_dados_prestador(a.nr_sequencia,'TP'),1,255) tipo_prestador,
		pls_obter_dados_prestador(a.nr_sequencia, 'DP') qt_dias_protocolo, 
		pls_obter_dados_prestador(a.nr_sequencia, 'FAB') ie_forma_autenticacao,
		pls_obter_cod_prestador(a.nr_sequencia,null) cd_prestador,
		pls_obter_uf_crm_medico(a.cd_pessoa_fisica) uf_crm,
		pls_parametro_operadora_web('FVC', a.cd_estabelecimento),
		(select nvl(max(x.qt_caracteres_carteira),0)
		from	pls_web_param_geral x
		where	x.cd_estabelecimento = a.cd_estabelecimento)
	from    pls_prestador a,
		pls_prestador_usuario_web b
	where   a.ie_situacao = 'A'
	and     b.ie_situacao = 'A'
	and     a.nr_sequencia = b.nr_seq_prestador
	and     b.nr_seq_usuario = nr_seq_usuario_p		
	order   by nm_prestador asc)
	where rownum = 1;
elsif	(nr_seq_local_atend_p is not null) then
	ie_origem_prestador_p := 'LOCAL';
	
	select  distinct a.ds_email,
		a.cd_pessoa_fisica_resp,
		obter_nome_pf(a.cd_pessoa_fisica_resp) ds_resp,
		obter_crm_medico(a.cd_pessoa_fisica_resp) nr_crm ,
		NVL(a.cd_estabelecimento, 1) cd_estabelecimento,
		a.ie_exige_biometria, 
		nvl(pls_obter_unimed_estab(a.cd_estabelecimento), '0') cd_cooperativa, 
		pls_obter_dados_prestador_web(a.nr_sequencia, 'PW') nr_seq_perfil_web,                         
		a.nr_seq_local_atend,
		pls_obter_local_atend_medico(a.nr_seq_local_atend),
		pls_obter_uf_crm_medico(a.cd_pessoa_fisica_resp)
	into	ds_email_p,
		cd_pessoa_fisica_resp_p,
		ds_responsavel_p,
		nr_crm_p,
		cd_estabelecimento_p,
		ie_exige_biometria_p,
		cd_cooperativa_p,
		nr_seq_perfil_web_p,
		nr_seq_local_atend_w,
		nm_local_atend_p,
		uf_crm_p
	from    pls_usuario_web a
	where   a.nr_sequencia = nr_seq_usuario_p;
	
	select	count(1)
	into	qt_prestadores_login_w
	from	pls_prestador a,
		local_atend_med_prest b
	where   a.nr_sequencia = b.nr_seq_prestador
	and	b.nr_seq_local_atend = nr_seq_local_atend_p
	and	a.ie_situacao = 'A'
	and     (b.dt_fim_vigencia is null or to_date(b.dt_fim_vigencia, 'dd/mm/yyyy') >= to_Date(sysdate,'dd/mm/yyyy'));	
	
	if	(qt_prestadores_login_w > 0) then
	
		 select	initcap(substr(pls_obter_nm_prest_web(a.nr_sequencia),1,255)) nm_prestador,
			a.cd_pessoa_fisica cd_pessoa_fisica,
			obter_crm_medico(a.cd_pessoa_fisica) nr_crm ,
			a.nr_sequencia nr_seq_prestador,
			substr(pls_obter_dados_prestador(a.nr_sequencia,'TP'),1,255) tipo_prestador,
			pls_obter_dados_prestador(a.nr_sequencia, 'DP') qt_dias_protocolo, 
			pls_obter_dados_prestador(a.nr_sequencia, 'FAB') ie_forma_autenticacao,
			pls_obter_cod_prestador(a.nr_sequencia,null) cd_prestador,
			pls_obter_uf_crm_medico(a.cd_pessoa_fisica) uf_crm,
			pls_parametro_operadora_web('FVC', a.cd_estabelecimento),
			(select nvl(max(x.qt_caracteres_carteira),0)
			from	pls_web_param_geral x
			where	x.cd_estabelecimento = a.cd_estabelecimento)
		into	nm_prestador_p,
			cd_pessoa_fisica_p,
			nr_crm_p,
			nr_seq_prestador_p,
			ie_tipo_prestador_p,
			qt_dias_protocolo_p,
			ie_forma_autenticacao_p,
			cd_prestador_p,
			uf_crm_p,
			ie_forma_val_cart_benef_p,
			qt_max_caract_cart_p
		from    pls_prestador a,
			local_atend_med_prest b
		where   a.nr_sequencia = b.nr_seq_prestador
		and	b.nr_seq_local_atend = nr_seq_local_atend_p
		and	a.ie_situacao = 'A'
		and     (b.dt_fim_vigencia is null or to_date(b.dt_fim_vigencia, 'dd/mm/yyyy') >= to_Date(sysdate,'dd/mm/yyyy'))
		and 	rownum = 1
		order   by nm_prestador asc;
		
	end if;
		
end if;		

begin
select	to_char(dt_atualizacao, 'dd/MM/yyyy hh24:mi:ss')
into	dt_ultimo_acesso_p
from	pls_acesso_portal_log
where	nr_sequencia = (select	max(nr_sequencia) 
			from	pls_acesso_portal_log
			where	nr_seq_usu_prestador 	= nr_seq_usuario_p
			and	ie_tipo_acesso		= 'P'
			and	ie_tipo_historico 	= '1');
exception
when others then
	dt_ultimo_acesso_p	:= null;
end;

ds_mensagem_log_w := 'O prestador '|| nm_usuario_p ||' logou no portal.';
pls_gravar_log_acesso_portal(null, null, nr_seq_usuario_p, null, null, null, null, null, '1', ds_mensagem_log_w, 'Function - pls_obter_parametros_funcao', 'S');

qt_prestadores_login_p 	:= qt_prestadores_login_w;
ds_responsavel_p 	:= substr(ds_responsavel_p, 0, 31);
cd_versao_tiss_p	:= pls_obter_versao_tiss;

commit;

end pls_gerar_login_prestador;
/
