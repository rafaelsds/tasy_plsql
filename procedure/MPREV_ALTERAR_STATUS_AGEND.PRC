create or replace
procedure mprev_alterar_status_agend (  nr_seq_status_p	 	number,
				nr_seq_agendamento_p	number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2) is 
begin

if (nr_seq_status_p is not null and nr_seq_agendamento_p is not null) then

	if (upper(ie_opcao_p) = 'P') then
		update mprev_agendamento 
		set nr_seq_status_pac = nr_seq_status_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
		where nr_sequencia = nr_seq_agendamento_p;
	end if;
	
end if;

commit;

end mprev_alterar_status_agend;
/

