create or replace
procedure latam_expressao_sem_uso_wheb is 

nm_tabela_w			varchar2(255);
nm_integridade_referencial_w	varchar2(255);
nm_atributo_w			varchar2(255);
cd_expressao_w			number(10);
ds_sep_bv_w			varchar2(20);
qt_expressao_w			number(15,2) := 0;
total_expressao_w		number(15,2) := 0;	
registro_w			number(5);

Cursor C01 is
Select 	cd_expressao
from	dic_expressao;

Cursor C02 is
SELECT 	a.nm_tabela, b.nm_atributo
FROM	integridade_referencial a,
	integridade_atributo b
where	a.nm_integridade_referencial = b.nm_integridade_referencial
and	nm_tabela_referencia = 'DIC_EXPRESSAO'
and	a.nm_tabela not in ('DIC_EXPRESSAO_TRADUCAO','W_DIC_EXPRESSAO_USO','DIC_EXPRESSAO_HISTORICO','LOTE_TRADUCAO_EXPRESSAO');


begin

-- rodar na base corp e popular a tabela temporária
corp.latam_expressao_sem_uso@whebl01_dbcorp;

delete from w_dic_expressao;
commit;

select	obter_separador_bv
into	ds_sep_bv_w
from	dual;

open C01;
loop
fetch C01 into	
	cd_expressao_w;
exit when C01%notfound;
	begin
		total_expressao_w := 0;
		open C02;
		loop
		fetch C02 into	
			nm_tabela_w,
			nm_atributo_w;
		exit when C02%notfound;
			begin
				qt_expressao_w := 0;
				obter_valor_dinamico_bv('select count(*) from ' || nm_tabela_w || ' where ' || nm_atributo_w || ' = :cd_expressao',
							'cd_expressao='||cd_expressao_w,qt_expressao_w);
				if qt_expressao_w > 0 then
					total_expressao_w := total_expressao_w + qt_expressao_w;
					exit;
				end if;
				
				select	count(1) 
				into registro_w
				from corp.w_dic_expressao@whebl01_dbcorp
				where cd_expressao = cd_expressao_w;
				
				if (registro_w > 0) then
					delete from corp.w_dic_expressao@whebl01_dbcorp where cd_expressao = cd_expressao_w;
					commit;
				end	if;
			end;
		end loop;
		close C02;
	end;
	
	if total_expressao_w = 0 then
		insert into w_dic_expressao (cd_expressao) values (cd_expressao_w);
		commit;
	end if;
	
end loop;
close C01;


commit;

end latam_expressao_sem_uso_wheb;
/