create or replace
procedure pls_analise_glosa_exis_contas
			(	nr_seq_conta_p			number,
				nr_seq_analise_conta_item_p	number,
				nr_seq_mot_liberacao_p		number,
				ds_parecer_p			Varchar2,
				nr_seq_analise_p		number,
				nr_seq_grupo_atual_p		number,				
				ie_comitar_p			Varchar2,
				ie_lib_demais_glosas_p		Varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			Varchar2) is

ds_motivo_liberacao_w		varchar2(255);
ds_tipo_motivo_w		varchar2(50);
ds_observacao_w			varchar2(255);

ie_tipo_glosa_oc_w		varchar2(1);
ie_tipo_historico_w		varchar2(1);
ie_tipo_item_w			varchar2(1);
ie_status_w			varchar2(1) := 'N';
ie_situacao_w			varchar2(1);
ie_tipo_motivo_w		varchar2(1);

qt_apresentado_w		Number(10,2);
qt_glosa_w			Number(10,2) := 0;

nr_seq_mot_liberacao_w		Number(10);
nr_seq_item_glosa_oc_w		Number(10);
nr_seq_conta_w			Number(10);
nr_seq_conta_mat_w		Number(10);
nr_seq_conta_proc_w		Number(10);
nr_seq_partic_proc_w		Number(10);
nr_seq_ocorrencia_w		Number(10);
nr_seq_glosa_w			number(10);
nr_seq_conta_proc_ww		number(10);
nr_seq_conta_mat_ww		number(10);
nr_seq_partic_proc_ww		number(10);
nr_seq_conta_ww			number(10);

vl_total_apres_w		Number(10,2);
vl_glosa_w			Number(10,2) := 0;
ie_origem_analise_w		Number(10);
nr_seq_w_resumo_conta_w		number(10);
qt_liberado_w			number(12,4);
vl_liberado_w			number(15,2);

ie_analise_multipla_w		varchar2(1)	:= 'N';
qt_liberada_w			number(12,4);
ie_valor_base_w			number(15,2);
vl_liberado_material_w		number(15,2);
vl_liberado_hi_w		number(15,2);
vl_liberado_co_w		number(15,2);
vl_unitario_apres_w		number(15,2);
vl_unitario_calc_w		number(15,2);
vl_glosa_hi_w			number(15,2);
vl_glosa_co_w			number(15,2);
vl_glosa_material_w		number(15,2);
ie_pagamento_w			varchar2(1);
ie_pagamento_ww			varchar2(1);
nr_seq_ordem_w			number(10);

qt_proc_conta_w			number(10);
qt_proc_conta_glosado_w		number(10);

Cursor C01 is
	select	ie_tipo,
		nr_sequencia,
		ie_status,
		ie_situacao,
		nr_seq_conta,
		nr_seq_conta_mat,
		nr_seq_conta_proc,
		nr_seq_proc_partic,
		vl_glosa,
		qt_glosa
	from	pls_analise_conta_item
	where	nr_seq_conta = nr_seq_conta_p
	and	(nvl(nr_seq_conta_proc,0) 	= 0)
	and	 (nvl(nr_seq_conta_mat,0)  	= 0)
	and	 (nvl(nr_seq_proc_partic,0)  = 0)
	and	ie_status not in ('I', 'E', 'C')
	order by 1;

begin

-- QUANDO GLOSA A CONTA N�O PODE TER NADA DE VALOR LIBERADO!!!!!!!!!!
update	w_pls_resumo_conta
set	qt_liberado = 0,
	vl_total = 0
where	nr_seq_analise	= nr_seq_analise_p
and	nr_seq_conta	= nr_seq_conta_p;

select	max(nr_seq_w_resumo_conta)
into	nr_seq_w_resumo_conta_w
from	pls_analise_conta_item
where	nr_sequencia	= nr_seq_analise_conta_item_p;

select	nvl(qt_apresentado,0),
	nvl(vl_total_apres,0),
	nvl(qt_liberado,0),
	vl_total,
	nr_seq_conta_proc,
	nr_seq_conta
into	qt_apresentado_w,
	vl_total_apres_w,
	qt_liberado_w,
	vl_liberado_w,
	nr_seq_conta_proc_ww,
	nr_seq_conta_w
from	w_pls_resumo_conta
where	nr_sequencia	= nr_seq_w_resumo_conta_w;

if	(nvl(ie_lib_demais_glosas_p,'N') = 'S') then
	/*Obter o motivo de libera��o padr�o para libera��o manual.*/
	begin
	select	nr_sequencia,
		ie_tipo_motivo,
		ds_motivo_liberacao
	into	nr_seq_mot_liberacao_w,
		ie_tipo_motivo_w,
		ds_motivo_liberacao_w
	from	pls_mot_lib_analise_conta
	where	nr_sequencia =	(select	max(nr_sequencia)
				from	pls_mot_lib_analise_conta
				where	nvl(ie_glosa_manual,'N') = 'S');
	exception
	when others then
		nr_seq_mot_liberacao_w	:= null;
		ie_tipo_motivo_w	:= null;
	end;

	if	(ie_tipo_motivo_w = 'N') then
		ds_tipo_motivo_w	:= 'Libera��o desfavor�vel';
	else
		ds_tipo_motivo_w	:= 'Libera��o favor�vel';
	end if;

	/*Se o motivo de libera��o manual n�o existe, o processo n�o pode continuar. */
	if	(nvl(nr_seq_mot_liberacao_w,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(176126);
		--'N�o existe motivo padr�o de libera��o de glosas/ocorr�ncias para glosa manual. O motivo de libera��o citado, � o campo "Glosa manual", dos "Cadastros gerais / Plano de Sa�de / Contas m�dicas / Motivos de libera��o da an�lise da conta"'
	end if;

	open C01;
	loop
	fetch C01 into
		ie_tipo_glosa_oc_w,
		nr_seq_item_glosa_oc_w,
		ie_status_w,
		ie_situacao_w,
		nr_seq_conta_w,
		nr_seq_conta_mat_w,
		nr_seq_conta_proc_w,
		nr_seq_partic_proc_w,
		vl_glosa_w,
		qt_glosa_w;
	exit when C01%notfound;
		begin
		/*Desfeita qualquer libera��o que pode ter ocorrido pr�viamente*/
		/*Apagar parecer*/
		delete	pls_analise_parecer_item
		where	nr_seq_item	= nr_seq_item_glosa_oc_w;

		/*Atualizar o item para pendente.*/
		update	pls_analise_conta_item
		set	ie_status	= 'P',
			qt_glosa	= decode(nvl(nr_seq_conta_proc,nvl(nr_seq_conta_mat,nvl(nr_seq_proc_partic,0))), 0, 0, nvl(qt_apresentado_w,0)),
			vl_glosa	= decode(nvl(nr_seq_conta_proc,nvl(nr_seq_conta_mat,nvl(nr_seq_proc_partic,0))), 0, 0, nvl(vl_total_apres_w,0)),
			ie_consistencia = 'N',
			ie_situacao	= 'A'
		where	nr_sequencia	= nr_seq_item_glosa_oc_w
		and	ie_status	<> 'I';

		select	nr_seq_ocorrencia,
			nr_seq_glosa
		into	nr_seq_ocorrencia_w,
			nr_seq_glosa_w
		from	pls_analise_conta_item
		where 	nr_sequencia	= nr_seq_item_glosa_oc_w;

		/*Obtem o tipo de hist�rico se � uma libera��o de glosa ou de ocorrencia*/
		select	decode(ie_tipo_glosa_oc_w, 'G', 5, 'O', 6)
		into	ie_tipo_historico_w
		from	dual;

		/*Atualizado a glosa / ocorrencia*/
		update	pls_analise_conta_item
		set	ie_status 	= decode(ie_tipo_motivo_w,'S', 'A', 'N'),
			nm_usuario	= nm_usuario_p,
			dt_atualizacao 	= sysdate,
			ie_consistencia	= 'N',
			ie_principal	= 'N',
			qt_glosa	= decode(nvl(nr_seq_conta_proc,nvl(nr_seq_conta_mat,nvl(nr_seq_proc_partic,0))), 0, 0, nvl(qt_apresentado_w,0)),
			vl_glosa	= decode(nvl(nr_seq_conta_proc,nvl(nr_seq_conta_mat,nvl(nr_seq_proc_partic,0))), 0, 0, nvl(vl_total_apres_w,0)),
			ie_situacao	= decode(ie_tipo_motivo_w, 'N', 'A', 'L', 'A', 'S', 'I')
		where	nr_sequencia	= nr_seq_item_glosa_oc_w
		and	ie_status	<> 'I';

		/*Criado o parecer*/
		insert into pls_analise_parecer_item
			(nr_sequencia,
			nr_seq_item,
			nr_seq_motivo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_parecer,
			ie_tipo_motivo)
		values	(pls_analise_parecer_item_seq.nextval,
			nr_seq_item_glosa_oc_w,
			nr_seq_mot_liberacao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'Parecer criado autom�ticamente pelo sistema na glosa/ocorr�ncia manual.',
			ie_tipo_motivo_w);

		ds_observacao_w := 	'Tipo de libera��o:  '||chr(13)||chr(10)||
					chr(9)||ds_tipo_motivo_w||chr(13)||chr(10)||chr(13)||chr(10)||
					'Motivo de libera��o:'||chr(13)||chr(10)||
					chr(9)||ds_motivo_liberacao_w||chr(13)||chr(10)||chr(13)||chr(10)||
					'Observa��o/Parecer: '||chr(13)||chr(10)||
					chr(9)||'Parecer criado autom�ticamente pelo sistema na glosa/ocorr�ncia manual.';

		/*Adiciona o hist�rico da a��o*/
		pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_p, ie_tipo_historico_w,
								 nvl(nr_seq_ocorrencia_w, nr_seq_glosa_w), ie_tipo_item_w, nr_seq_ocorrencia_w,
								 nr_seq_glosa_w, ds_observacao_w, nr_seq_grupo_atual_p,
								 nm_usuario_p, cd_estabelecimento_p);

		end;
	end loop;
	close C01;
end if;

/*Apagar o parecer de modo se este existir*/
delete	pls_analise_parecer_item
where	nr_seq_item	= nr_seq_analise_conta_item_p;

if	(qt_liberado_w > 0) and
	(qt_liberado_w < qt_apresentado_w) then
	qt_glosa_w	:= qt_apresentado_w - qt_liberado_w;
	ie_status_w	:= 'L';
	
	if	(nr_seq_conta_proc_ww is not null) then
		
		ie_pagamento_ww	:= 'P';
	end if;
	
	ds_observacao_w	:= ds_observacao_w || chr(13) || chr(10) || 'Glosa parcial' || chr(13) || chr(10) || 'Qt. glosada: ' || Campo_Mascara_virgula_casas(qt_glosa_w,4) ||
					chr(13) || chr(10);
elsif	(vl_liberado_w > 0) and
	(vl_liberado_w < vl_total_apres_w) then
	vl_glosa_w	:= vl_total_apres_w - vl_liberado_w;
	ie_status_w	:= 'L';
	
	if	(nr_seq_conta_proc_ww is not null) then
		
		ie_pagamento_ww	:= 'P';
	end if;

	ds_observacao_w	:= ds_observacao_w || chr(13) || chr(10) || 'Glosa parcial' || chr(13) || chr(10) || 'Vl. glosado: ' || Campo_Mascara_virgula_casas(vl_glosa_w,2) || 
		chr(13) || chr(10);
else
	qt_glosa_w	:= qt_apresentado_w - qt_liberado_w;
	vl_glosa_w	:= vl_total_apres_w - vl_liberado_w;	
	ie_status_w	:= 'N';
	
	if	(nr_seq_conta_proc_ww is not null) then
		
		ie_pagamento_ww	:= 'P';
	end if;
	
	ds_observacao_w	:= ds_observacao_w || chr(13) || chr(10) || 'Glosa total' || chr(13) || chr(10);
end if;

if	(qt_liberado_w = 0 and vl_liberado_w = 0) then
	
	ie_pagamento_ww	:= 'G';
	
elsif	(qt_liberado_w <> qt_apresentado_w and vl_liberado_w <> vl_total_apres_w) then

	ie_pagamento_ww	:= 'P';
	
elsif	(qt_liberado_w = qt_apresentado_w and vl_liberado_w = vl_total_apres_w) then

	ie_pagamento_ww	:= 'L';
else
	-- Indefinido
	ie_pagamento_ww	:= 'I';
end if;


/*Atualizado a glosa / ocorrencia*/
update	pls_analise_conta_item
set	ie_status 		= ie_status_w,--'N',
	nm_usuario		= nm_usuario_p,
	dt_atualizacao 		= sysdate,
	ie_consistencia 	= 'N',
	ie_situacao 		= 'A', --Ativada a glosa/ocorrencia
	nr_seq_glosa_manual	= null,
	qt_glosa		= 0,
	vl_glosa		= 0
where	nr_sequencia		= nr_seq_analise_conta_item_p
and	ie_status		<> 'I';

obter_param_usuario(1317,20,null,nm_usuario_p,cd_estabelecimento_p,ie_analise_multipla_w);

if	(nvl(ie_analise_multipla_w,'N') = 'N') then
	if	(nvl(qt_liberado_w,0) > 0) then
		if	(nvl(ie_valor_base_w,'1') = '1') then
			vl_liberado_w	:= round(vl_unitario_apres_w*qt_liberado_w,2);
		else
			vl_liberado_w	:= round(vl_unitario_calc_w*qt_liberado_w,2);
		end if;
			
		qt_liberada_w		:= qt_liberado_w;
		vl_liberado_hi_w 	:= null;
		vl_liberado_co_w 	:= null;
		vl_liberado_material_w 	:= null;			
	elsif	(nvl(vl_liberado_w,0) > 0) then				
		vl_liberado_w		:= vl_liberado_w;
		qt_liberada_w		:= qt_apresentado_w;
	else
		vl_liberado_w		:= 0;
		qt_liberada_w		:= 0;
		vl_liberado_hi_w 	:= 0;
		vl_liberado_co_w 	:= 0;
		vl_liberado_material_w 	:= 0;
	end if;			

	select	max(a.nr_seq_ordem)
	into	nr_seq_ordem_w
	from	pls_tempo_conta_grupo b,
		pls_auditoria_conta_grupo a
	where	a.nr_sequencia 		= b.nr_seq_auditoria
	and	a.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_grupo		= nr_seq_grupo_atual_p
	and	a.dt_liberacao is null
	and	b.dt_inicio_analise is not null
	and	b.dt_final_analise is null;
		
	pls_obter_valores_lib_analise(nr_seq_w_resumo_conta_w, cd_estabelecimento_p, vl_glosa_w, vl_liberado_w, qt_liberada_w, vl_liberado_co_w, vl_liberado_material_w, vl_liberado_hi_w,
					vl_glosa_co_w, vl_glosa_material_w, vl_glosa_hi_w);
	
	if	(nr_seq_ordem_w is not null) then
		if	(nvl(vl_liberado_w,0) = 0) and
			(nvl(qt_liberada_w,0) = 0) then
			ie_pagamento_w	:= 'G';
		elsif	(nvl(vl_glosa_w,0) > 0) and
			(nvl(vl_liberado_w,0) > 0) then
			ie_pagamento_w	:= 'P';	
		else
			ie_pagamento_w	:= 'L';				
		end if;	
	
		insert into pls_analise_fluxo_item
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_analise,
			nr_seq_grupo,
			nr_seq_conta_proc,
			nr_seq_conta_mat,
			nr_seq_proc_partic,
			nr_seq_ordem,
			vl_glosa,
			vl_liberado_hi,
			vl_liberado_co,   
			vl_liberado_material,
			vl_total,
			vl_glosa_co,   
			vl_glosa_hi,
			vl_glosa_material,
			qt_liberada,
			ie_pagamento,
			nr_seq_glosa_item)
		values	(pls_analise_fluxo_item_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_analise_p,
			nr_seq_grupo_atual_p,
			nr_seq_conta_proc_ww,
			nr_seq_conta_mat_ww,
			nr_seq_partic_proc_ww,
			nr_seq_ordem_w,
			vl_glosa_w,
			vl_liberado_hi_w,
			vl_liberado_co_w,   
			vl_liberado_material_w,
			vl_liberado_w,
			vl_glosa_co_w,   
			vl_glosa_hi_w,
			vl_glosa_material_w,
			qt_liberada_w,
			ie_pagamento_w,
			nr_seq_analise_conta_item_p);
	end if;
	
	update	w_pls_resumo_conta
	set	nm_usuario_glosa 	= nm_usuario_p,
		nm_usuario_liberacao 	= '',
		ie_fluxo_com_glosa	= 'S'
	where	nr_sequencia	 	= nr_seq_w_resumo_conta_w;
end if;	

/*Criado o parecer*/
insert into pls_analise_parecer_item
	(nr_sequencia,
	nr_seq_item,
	nr_seq_motivo,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_parecer,
	ie_tipo_motivo)
values	(pls_analise_parecer_item_seq.nextval,
	nr_seq_analise_conta_item_p,
	nr_seq_mot_liberacao_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ds_parecer_p,
	'A');

pls_analise_status_item(nr_seq_conta_p, null, null, nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p, null);

pls_analise_status_pgto(nr_seq_conta_p, null, null, nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p, null, null, null, null);

/*inclu�do a atualiza��o da an�lise, para que os valores de participante sejam atualizados ap�s a pls_an�lise_status_pgto Diogo*/				
select	max(ie_origem_analise)
into	ie_origem_analise_w
from	pls_analise_conta
where	nr_sequencia	= nr_seq_analise_p;

if	(ie_origem_analise_w = 3) then
	pls_atual_w_resumo_conta_ptu(nr_seq_conta_p, null, null, null, nr_seq_analise_p, nm_usuario_p);

else
	pls_atualiza_w_resumo_conta(nr_seq_conta_p, null, null, null, nr_seq_analise_p, nm_usuario_p);
end if;	

update	w_pls_resumo_conta
set	ie_pagamento		= ie_pagamento_ww
where	nr_seq_conta_proc	= nr_seq_conta_proc_ww;
	
if	(nvl(ie_comitar_p,'S') = 'S') then
	commit;
end if;

end pls_analise_glosa_exis_contas;
/