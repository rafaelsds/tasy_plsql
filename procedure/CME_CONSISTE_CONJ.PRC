create or replace
procedure cme_consiste_conj(
			nr_seq_conjunto_p		number,
			nr_seq_item_p 			number 	) is 

qt_conjunto_w	cm_conjunto.qt_conjunto%type;	
qt_registro_w	number(10);
				
begin
	if	(nr_seq_conjunto_p is null and nr_seq_item_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(1041576);
	end if;
	
	if	(nr_seq_conjunto_p is not null) then
		select	nvl(qt_conjunto,0)
		into	qt_conjunto_w
		from	cm_conjunto
		where	nr_sequencia = nr_seq_conjunto_p;
		
		select	count(*)
		into	qt_registro_w
		from	cm_conjunto_cont
		where	ie_status_conjunto not in (10,6)
		and	nr_seq_conjunto = nr_seq_conjunto_p;
		
		if	(qt_registro_w > 0 and qt_conjunto_w = 1) then
			wheb_mensagem_pck.exibir_mensagem_abort(1041576);
		end if;
	end if;
	
	if	(nr_seq_item_p is not null) then
		select	count(*)
		into	qt_registro_w
		from	cm_conjunto_cont
		where	ie_status_conjunto not in (10,6)
		and	nr_seq_item = nr_seq_item_p;
		
		if	(qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1041576);
		end if;
	end if;

commit;

end cme_consiste_conj;
/					