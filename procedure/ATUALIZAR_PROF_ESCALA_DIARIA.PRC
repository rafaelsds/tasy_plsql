create or replace
procedure atualizar_prof_escala_diaria(
			nr_sequencia_p		number,
			cd_pessoa_p		number,
			ie_origem_p		varchar2,
			nm_usuario_p		varchar2) is

begin
if	(nvl(cd_pessoa_p,'0') <> '0') then
	begin
	if	(ie_origem_p = 'S') then
		update	escala_diaria
		set	cd_pessoa_origem = cd_pessoa_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	else
		update	escala_diaria
		set	cd_pessoa_fisica = cd_pessoa_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	end if;

	commit;
	end;
end if;

end atualizar_prof_escala_diaria;
/