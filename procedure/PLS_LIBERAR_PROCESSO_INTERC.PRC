create or replace
procedure pls_liberar_processo_interc(	nr_seq_processo_interc_p	number,
					nm_usuario_p			varchar2) is 

begin
if	(nr_seq_processo_interc_p is not null) then
	update	pls_regra_processo_interc
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_processo_interc_p;	
	
	commit;
end if;

end pls_liberar_processo_interc;
/