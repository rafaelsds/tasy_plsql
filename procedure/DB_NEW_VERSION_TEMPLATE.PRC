create or replace procedure db_new_version_template (
	nr_seq_template_p	number,
	nm_usuario_p		varchar2,
	nr_seq_new_version_p	out number)
is

/*
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Objective: Generate a new version of the template
	------------------------------------------------------------------------
	Direct call: 
	[   ] Dictionary objects
	[ X ] Tasy (Delphi/Java/HTML5)
	[   ] Portal
	[   ] Reports
	[   ] Others:
	 -----------------------------------------------------------------------
	Attention points: N/A
	------------------------------------------------------------------------
	References:
		Tables:	db_content_template
		Objects:
	------------------------------------------------------------------------
	Called in:
		Java: N/A
		HTML5: 
		Object: 
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

	nr_seq_w	db_content_template.nr_sequencia%type;
	nr_version_w	db_content_template.nr_version%type;
	
begin

	select	ct.nr_seq_source
	into	nr_seq_w
	from	db_content_template ct
	where	ct.nr_sequencia = nr_seq_template_p;
	
	--children record
	if	(nr_seq_w is not null) then
		select	max(ct.nr_version)
		into	nr_version_w
		from	db_content_template ct
		where	ct.nr_seq_source = nr_seq_w;
	else
		select	max(ct.nr_version)
		into	nr_version_w
		from	db_content_template ct
		where	ct.nr_seq_source = nr_seq_template_p;
		
		if	(nr_version_w is null) then
			select	max(ct.nr_version)
			into	nr_version_w
			from	db_content_template ct
			where	ct.nr_sequencia = nr_seq_template_p;
		end if;
	end if;

	select	db_content_template_seq.nextval
	into	nr_seq_w
	from	dual;

	insert
	into db_content_template
		(
			ds_content,
			ds_description,
			ds_detail,
			ds_template,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_sequencia,
			nr_seq_source,
			nr_version
		)
	select	ct.ds_content,
		ct.ds_description,
		ct.ds_detail,
		ct.ds_template,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_w,
		nvl(ct.nr_seq_source, nr_seq_template_p),
		nr_version_w + 1
	from	db_content_template ct
	where	nr_sequencia = nr_seq_template_p;

	commit;

	insert
	into db_content_template_item
		(
			nr_seq_template,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_item,
			nr_seq_content_macro
		)
	select	nr_seq_w,
		db_content_template_item_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cti.nr_seq_item,
		cti.nr_seq_content_macro
	from	db_content_template_item cti
	where	cti.nr_seq_template = nr_seq_template_p;	

	commit;
	
	nr_seq_new_version_p := nr_seq_w;

end db_new_version_template;
/
