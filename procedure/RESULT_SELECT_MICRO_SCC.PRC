create or replace procedure result_select_micro_scc(
    nr_seq_interno_p             prescr_procedimento.nr_seq_interno%TYPE,
    ds_observacao_p              exame_lab_result_item.ds_observacao%TYPE)
is
    nr_prescricao_w        	 prescr_medica.nr_prescricao%TYPE;
    nr_seq_resultado_w     	 exame_lab_resultado.nr_seq_resultado%TYPE;
    nr_seq_result_item_w   	 exame_lab_result_item.nr_sequencia%TYPE;
    nr_seq_exame_w          	 exame_laboratorio.nr_seq_exame%TYPE;
    ds_valor_w             	 exame_lab_valor.ds_valor%TYPE;
    ie_existe_antib_w      	 varchar2(1);
    nr_seq_prescr_w        	 number(10);
    nr_resul_integracao_w  	 exame_lab_valor.ie_tipo_valor_integr%TYPE;
    cd_estab_w		         number(5);
    ie_desaprovar_atualizar_w	 lab_parametro.ie_atualizar_result_aprov%TYPE;
    dt_aprovacao_exame_w	 exame_lab_result_item.dt_aprovacao%TYPE;
    ie_status_atend_w	         prescr_procedimento.ie_status_atend%type;
    count_w                    	 number(3);

begin

if (nr_seq_interno_p is null) then
  WHEB_MENSAGEM_PCK.exibir_mensagem_abort(277600);  
end if;

select  count(1)
into    count_w
from    prescr_procedimento c
where   nr_seq_interno = nr_seq_interno_p;

if  (count_w = 0)then
 WHEB_MENSAGEM_PCK.exibir_mensagem_abort(1111467,'NR_SEQ_INTERNO='||nr_seq_interno_p);
end if;

    select
        (nr_prescricao),
        (nr_sequencia),
        (ie_status_atend)
    into
        nr_prescricao_w, 
        nr_seq_prescr_w,
        ie_status_atend_w
    from
        prescr_procedimento
    where
        nr_seq_interno = nr_seq_interno_p;

    if	( nr_prescricao_w is not null ) then
        select
            max(nr_seq_resultado)
        into nr_seq_resultado_w
        from
            exame_lab_resultado
        where
            nr_prescricao = nr_prescricao_w;

        select	max(i.nr_sequencia)
        into nr_seq_result_item_w
        from
            exame_lab_result_item   i,
            exame_laboratorio       e
        where
            i.nr_seq_exame = e.nr_seq_exame
            and i.nr_seq_resultado = nr_seq_resultado_w
            and i.nr_seq_prescr = nr_seq_prescr_w
            and e.ie_formato_resultado IN (
                'SM',
                'SDM'
            );

        select
            DECODE(COUNT(*), 0, 'N', 'S')
        into ie_existe_antib_w
        from
            exame_lab_result_antib a
        where
            a.nr_seq_resultado = nr_seq_resultado_w
            and a.nr_seq_result_item = nr_seq_result_item_w;
            
        select
                max(nr_seq_exame)
            into nr_seq_exame_w
            from
                exame_lab_result_item a
            where
                a.nr_seq_resultado = nr_seq_resultado_w
                and a.nr_sequencia = nr_seq_result_item_w;

        if ( ie_existe_antib_w = 'S' ) then
            nr_resul_integracao_w := 1;
        else
            nr_resul_integracao_w := 2;
        end if;
        
        select
            max(ds_valor)
            into ds_valor_w
            from
              exame_lab_valor a
            where
              a.nr_seq_exame = nr_seq_exame_w
              and a.ie_tipo_valor_integr = nr_resul_integracao_w;

        if ( ds_valor_w is not null ) then
            select	
                cd_estabelecimento
            into	cd_estab_w
            from	
                prescr_medica
            where	
                nr_prescricao = nr_prescricao_w;
            
            begin
                select 
                    ie_atualizar_result_aprov
                into	ie_desaprovar_atualizar_w
                from	
                    lab_parametro
                where	cd_estabelecimento = cd_estab_w;
            exception
                when no_data_found then
                    ie_desaprovar_atualizar_w := 'N';
                when too_many_rows then
                    ie_desaprovar_atualizar_w := 'N';
            end;
            
            begin
                select 
                    dt_aprovacao
                into	dt_aprovacao_exame_w
                from 	
                    exame_lab_result_item
                where	
                    nr_seq_resultado = nr_seq_resultado_w
                    and nr_sequencia = nr_seq_result_item_w;
            exception
                when no_data_found then
                    dt_aprovacao_exame_w := null;
                when too_many_rows then
                    dt_aprovacao_exame_w := null;
            end;
            
            if (ie_desaprovar_atualizar_w = 'S') and ((dt_aprovacao_exame_w is not null) or (ie_status_atend_w >= 35)) then
            
                update	exame_lab_result_item
                set	
                    dt_aprovacao = null
                where 
                    nr_seq_resultado = nr_seq_resultado_w
                    and nr_sequencia = nr_seq_result_item_w;
              
                update  prescr_procedimento
                set
                    ie_status_atend = 30
                where
                    nr_seq_interno = nr_seq_interno_p;
                
            end if;
            
            update exame_lab_result_item
            set
                ds_resultado = ds_valor_w,
                ds_observacao = ds_observacao_p
            where
                nr_seq_resultado = nr_seq_resultado_w
                and nr_sequencia = nr_seq_result_item_w;

            commit;
        end if;
        
    end if;

    commit;
end result_select_micro_scc;
/