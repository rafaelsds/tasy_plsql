create	or replace	
procedure mprev_carregar_w_agenda_ativ (nr_seq_agendamento_p	mprev_agendamento.nr_sequencia%type,
					nr_seq_w_agendamento_p	w_mprev_agendamento.nr_sequencia%type) is

begin

if	(nr_seq_agendamento_p is not null) then
	mprev_agenda_pck.carregar_w_agendamento_ativ(nr_seq_agendamento_p, nr_seq_w_agendamento_p);
end if;


end mprev_carregar_w_agenda_ativ;
/