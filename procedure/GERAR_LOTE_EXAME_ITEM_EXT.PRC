create or replace
procedure gerar_lote_exame_item_ext(nr_lote_ext_p number, 
  ds_erro_p out varchar2) is 
  dt_inicio_w date;
  dt_fim_w date;
  cd_cgc_w varchar2(14);
  nr_prescricao_w number(14);
  nr_sequencia_pp_w number(6);
  qtd_registros_w number;
         
Cursor C01 is      
select distinct pp.nr_prescricao, pp.nr_sequencia
	from prescr_procedimento pp
  join prescr_medica pm on pm.nr_prescricao = pp.nr_prescricao
  join atend_categoria_convenio atc 
    on atc.nr_atendimento=pm.nr_atendimento
   and atc.dt_inicio_vigencia = (select max(w.dt_inicio_vigencia) from atend_categoria_convenio w where w.nr_atendimento  = pm.nr_atendimento)
  join lab_lote_regra_convenio rco
    on rco.cd_convenio=atc.cd_convenio
  join exame_lab_resultado r
    on r.nr_prescricao		= pp.nr_prescricao
  join exame_lab_result_item i
    on r.nr_seq_resultado	= i.nr_seq_resultado
    and	r.nr_prescricao		= pp.nr_prescricao
    and	i.nr_seq_prescr		= pp.nr_sequencia
  where trunc(i.dt_aprovacao) between  dt_inicio_w and dt_fim_w
    and pp.nr_seq_exame is not null
    and lab_obter_regra_estab_ext(1,pm.cd_estabelecimento,0) = 'S'
    and rco.cd_cgc = cd_cgc_w
    and obter_se_contido_entre_virgula(rco.ie_status, pp.ie_status_atend)='S'
    and not exists (select 1 
                    from lab_lote_item li 
                    where li.nr_prescricao =pp.nr_prescricao
                    and li.nr_seq_prescr =pp.nr_sequencia);
                    
                    
                    
begin
 qtd_registros_w :=0;
 ds_erro_p:='';
 
 select dt_inicio_vig, 
        dt_fim_vig, 
        cd_cgc
	 into dt_inicio_w, 
        dt_fim_w, 
        cd_cgc_w
	from lab_lote_externo
	where nr_sequencia = nr_lote_ext_p;
  
  if (nr_lote_ext_p is not null) then
     open C01;
           loop
            fetch C01 into
              nr_prescricao_w,
              nr_sequencia_pp_w;
             exit when C01%notfound;
    
            qtd_registros_w :=1;
            insert into lab_lote_item
               (nr_sequencia ,
                nr_seq_lote_externo  ,
                dt_atualizacao ,
                nm_usuario  ,
                dt_atualizacao_nrec ,    
                nm_usuario_nrec  ,
                nr_prescricao  ,
                nr_seq_prescr
             ) values(
                lab_lote_item_seq.nextval,
                nr_lote_ext_p,
                sysdate,
                obter_usuario_ativo(),
                sysdate,
                obter_usuario_ativo(),
                nr_prescricao_w,
                nr_sequencia_pp_w
            );
            
            update prescr_procedimento pp
              set pp.nr_seq_lote_externo = nr_lote_ext_p,
                  pp.ie_externo = 'S'
              where pp.nr_prescricao = nr_prescricao_w
                and pp.nr_sequencia = nr_sequencia_pp_w
                and pp.nr_seq_lote_externo is null;
          end loop;    
        close c01;
    if (qtd_registros_w=0) then
       ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(1048406); --buscar mensagem do schematics  
    end if;
  end if;
 
end gerar_lote_exame_item_ext;
/
