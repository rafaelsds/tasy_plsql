create or replace
procedure SAN_EXCLUIR_PROCED_PACIENTE (	nr_seq_propaci_p	number,
					nm_usuario_p		varchar2,
					ie_opcao_p		varchar2 default 'N') is

/*
N - Normal - apenas um item de cada vez
T - Total
*/
					
nr_seq_propaci_real_w	number(15);
nr_seq_propaci_prod_w	number(15);
nr_seq_propaci_res_w	number(15);
qt_procedimento_w	number(15);

begin

if	(nr_seq_propaci_p	> 0) then

	select	max(nr_seq_propaci)
	into	nr_seq_propaci_real_w
	from	san_exame_realizado
	where	nr_seq_propaci	= nr_seq_propaci_p;

	select	max(nr_seq_propaci)
	into	nr_seq_propaci_prod_w
	from    san_producao
	where   nr_seq_propaci	= nr_seq_propaci_p;

	select	max(nr_seq_propaci)
	into	nr_seq_propaci_res_w
	from    san_reserva_prod
	where   nr_seq_propaci	= nr_seq_propaci_p;

	select	nvl(max(qt_procedimento),0)
	into	qt_procedimento_w
	from	procedimento_paciente
	where	nr_sequencia	= nr_seq_propaci_p;

	if	(qt_procedimento_w	> 1) and (ie_opcao_p = 'N') then
		update	procedimento_paciente
		set	qt_procedimento = qt_procedimento_w - 1
		where	nr_sequencia	= nr_seq_propaci_p;
		
	elsif	(nvl(nr_seq_propaci_real_w, 0) = 0) and
		(nvl(nr_seq_propaci_prod_w, 0) = 0) and
		(nvl(nr_seq_propaci_res_w, 0)  = 0) then
	
		update	san_exame_realizado
		set	nr_seq_propaci = null
		where	nr_seq_propaci = nr_seq_propaci_p;

		update	san_producao
		set	nr_seq_propaci = null
		where	nr_seq_propaci = nr_seq_propaci_p;

		update	san_reserva_prod
		set	nr_seq_propaci = null
		where	nr_seq_propaci = nr_seq_propaci_p;

		delete	from procedimento_paciente
		where	nr_sequencia = nr_seq_propaci_p;

	elsif	(qt_procedimento_w	= 1) or (ie_opcao_p = 'T') then
		
		update	san_exame_realizado
		set	nr_seq_propaci = null
		where	nr_seq_propaci = nr_seq_propaci_p;

		update	san_producao
		set	nr_seq_propaci = null
		where	nr_seq_propaci = nr_seq_propaci_p;

		update	san_reserva_prod
		set	nr_seq_propaci = null
		where	nr_seq_propaci = nr_seq_propaci_p;

		delete	from procedimento_paciente
		where	nr_sequencia = nr_seq_propaci_p;
	end if;

	commit;
end if;

end SAN_EXCLUIR_PROCED_PACIENTE;
/