create or replace
procedure TISS_OBTER_REGRA_GUIA_DESP (	cd_estabelecimento_p		number,
					cd_convenio_p			number,
					ie_tipo_atendimento_p		number,
					ie_guia_honorario_p	OUT	varchar2) is

ie_guia_honorario_w	varchar2(10);

cursor c01 is
select	nvl(ie_regra, 'N')
from	tiss_regra_desp_guia
where	cd_convenio		= cd_convenio_p
and	cd_estabelecimento	= cd_estabelecimento_p
and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0)) = nvl(ie_tipo_atendimento_p,0);

begin

open c01;
loop
fetch c01 into
	ie_guia_honorario_w;
exit when c01%notfound;

	ie_guia_honorario_w	:= ie_guia_honorario_w;

end loop;
close c01;

ie_guia_honorario_p	:= nvl(ie_guia_honorario_w, 'N');

end TISS_OBTER_REGRA_GUIA_DESP;
/
