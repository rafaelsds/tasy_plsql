create or replace
procedure gerar_dados_diagnostika(	nr_prescricao_p	number,
				nr_seq_interno_p	number,
				ie_tipo_p		number,
				nm_usuario_p	varchar2) is 

nr_seq_projeto_w 		number(10);
nr_seq_log_w		number(10);
ds_parametros_w		varchar2(255);
ds_xml_w			long;
nr_seq_prescr_w		number(10);
pragma autonomous_transaction;
		
begin

/*Sequencia do projeto XML*/
if (ie_tipo_p = 1) then
	nr_seq_projeto_w := 100258;
else
	nr_seq_projeto_w := 100264;
end if;

/*Parametros do projeto XML*/
ds_parametros_w :='NR_SEQ_INTERNO='||nr_seq_interno_p||';';
/*Pegar a sequencia da tabela que ir� gerar o XML*/
select 	tasy_xml_banco_seq.nextval
into 	nr_seq_log_w
from 	dual;

wheb_exportar_xml(nr_seq_projeto_w,nr_seq_log_w,'',ds_parametros_w);

select	ds_xml
into	ds_xml_w
from	tasy_xml_banco
where	nr_sequencia = nr_seq_log_w;

select	max(nr_sequencia)
into	nr_seq_prescr_w
from	prescr_procedimento
where	nr_seq_interno = nr_seq_interno_p;

Gerar_lab_log_interf_imp(nr_prescricao_p,
			nr_seq_prescr_w,
			null,
			null,
			'Projeto: '||nr_seq_projeto_w||' Seq. interno: '||nr_seq_interno_p||' Seq. tasy_xml_banco: '||nr_seq_log_w,
			'XML Diagn�stika',
			'',
			nm_usuario_p,
			'N');
				
insert into 	integracao_diagnostika (nr_sequencia, 
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_interno,
					ds_xml)
			values (	integracao_diagnostika_seq.nextval,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nr_seq_interno_p,
					ds_xml_w);

commit;

end gerar_dados_diagnostika;
/