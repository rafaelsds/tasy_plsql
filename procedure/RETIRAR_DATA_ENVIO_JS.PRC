create or replace
procedure retirar_data_envio_js(	nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p is not null)then
	begin
	
	update 	lab_lote_externo 
	set 	dt_envio = null 
	where 	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end retirar_data_envio_js;
/