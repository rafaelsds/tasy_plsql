create or replace
procedure VINCULAR_ADIANT_RECEB_CONV
			(nr_adiantamento_p	in	number,
		 	nr_seq_receb_p		in	number,
		 	ie_acao_p		in	varchar2,
		 	ie_commit_p		in	varchar2,
		 	nm_usuario_p		in	varchar2,
			vl_vinculado_p		in	number,
			ds_retorno_p		out	varchar2) is

/*
V - Vincular
D - Desvincular
*/

ds_retorno_w		varchar2(4000);
vl_saldo_adiant_w 	adiantamento.vl_saldo%type;
vl_vinculado_w		convenio_receb_adiant.vl_vinculado%type;
nr_adiantamento_w	convenio_receb.nr_adiantamento%type;
qt_vinc_w		convenio_receb_adiant.nr_sequencia%type;
vl_adiantamento_w	adiantamento.vl_adiantamento%type;
vl_saldo_a_vincular_w 	number(15,2);
vl_vincular_w		convenio_receb_adiant.vl_vinculado%type;
vl_recebimento_w  	convenio_receb.vl_recebimento%type;

cursor	c01 is
select	a.nr_adiantamento
from	convenio_receb_adiant a
where	a.nr_seq_receb	= nr_seq_receb_p;

begin

ds_retorno_w	:= 'N';

if	(ie_acao_p = 'V') then

	select	nvl(max(a.vl_saldo),0),
		nvl(max(a.vl_adiantamento),0)
	into	vl_saldo_adiant_w,
		vl_adiantamento_w
	from	adiantamento a
	where	a.nr_adiantamento	= nr_adiantamento_p;

   	select  nvl(max(vl_recebimento), 0)
   	into  vl_recebimento_w
   	from  convenio_receb
   	where nr_sequencia = nr_seq_receb_p;
		
	select  nvl(sum(a.vl_vinculado),0)
  	into  vl_vinculado_w
  	from  convenio_receb_adiant a
  	where a.nr_seq_receb   = nr_seq_receb_p;
	
	vl_saldo_a_vincular_w := (vl_recebimento_w - vl_vinculado_w);

	if	(nvl(vl_vinculado_p,0) <> 0) and
		(vl_vinculado_p <= vl_saldo_adiant_w) and
		(vl_saldo_a_vincular_w > 0) then
		
		select 	count(*)
		into	qt_vinc_w
		from	convenio_receb_adiant
		where	nr_seq_receb 	= nr_seq_receb_p
		and	nr_adiantamento = nr_adiantamento_p
        	and rownum = 1;
		
		if (qt_vinc_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(414819);
		end if;

    		if  (vl_saldo_a_vincular_w < vl_vinculado_p) then
      			vl_vincular_w := vl_saldo_a_vincular_w;
   		 end if;

		insert	into convenio_receb_adiant
			(dt_atualizacao,
			nm_usuario,
			nr_adiantamento,
			nr_seq_receb,
			nr_sequencia,
			vl_vinculado)
		values	(sysdate,
			nm_usuario_p,
			nr_adiantamento_p,
			nr_seq_receb_p,
			convenio_receb_adiant_seq.nextval,
		     	nvl(vl_vincular_w,vl_saldo_adiant_w));

		ATUALIZAR_SALDO_ADIANTAMENTO(nr_adiantamento_p, nm_usuario_p, null);

		ds_retorno_w	:= 'S';
	end if;

elsif	(ie_acao_p = 'D') then

	select	max(a.nr_adiantamento)
	into	nr_adiantamento_w
	from	convenio_receb a
	where	a.nr_sequencia	= nr_seq_receb_p;

	if	(nr_adiantamento_w is not null) then

		update	convenio_receb
		set	nr_adiantamento	= null
		where	nr_sequencia	= nr_seq_receb_p;

		ATUALIZAR_SALDO_ADIANTAMENTO(nr_adiantamento_w, nm_usuario_p, null);

		ds_retorno_w	:= 'S';

	end if;

	open	c01;
	loop
	fetch	c01 into
		nr_adiantamento_w;
	exit	when c01%notfound;

		delete	from convenio_receb_adiant
		where	nr_seq_receb	= nr_seq_receb_p
		and	nr_adiantamento	= nvl(nr_adiantamento_w,nr_adiantamento);

		ATUALIZAR_SALDO_ADIANTAMENTO(nr_adiantamento_w, nm_usuario_p, null);

		ds_retorno_w	:= 'S';

	end	loop;
	close	c01;

end if;

ds_retorno_p	:= nvl(ds_retorno_w,'N');

if	(ie_commit_p = 'S') and (ds_retorno_w = 'S') then
	commit;
end if;

end VINCULAR_ADIANT_RECEB_CONV;
/
