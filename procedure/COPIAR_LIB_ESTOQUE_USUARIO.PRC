create or replace
procedure copiar_lib_estoque_usuario(	nm_usuario_origem_p	Varchar2,
					nm_usuario_destino_p	Varchar2,					   
					nm_usuario_p		Varchar2) is 

cd_estabelecimento_w	number(1);
cd_local_estoque_w	number(4);
ie_evento_w		varchar2(2);
cd_perfil_w		number(5);
cd_cargo_lib_w		number(10);
ie_permissao_w		varchar2(1);	
nr_seq_regra_evento_w	number(10);
nm_usuario_lib_w	varchar2(15);


Cursor C01 is	
	select	c.cd_perfil,
		c.cd_cargo_lib,
		c.ie_permissao,
		c.nr_seq_regra_evento
	from	regra_local_usuario a,
		evento_local_usuario b,
		permissao_local_usuario c
	where	a.nr_sequencia	= b.nr_seq_regra_local
	and	b.nr_sequencia	= c.nr_seq_regra_evento
	and	c.nm_usuario_lib = nm_usuario_origem_p;
						
begin	

open C01;
loop
fetch C01 into
	cd_perfil_w,	
	cd_cargo_lib_w,
	ie_permissao_w,
	nr_seq_regra_evento_w;
exit when C01%notfound;		
		
	insert into permissao_local_usuario (   nr_sequencia,
						nr_seq_regra_evento,
						nm_usuario_nrec,
						nm_usuario_lib,
						nm_usuario,
						ie_permissao,
						dt_atualizacao_nrec,
						dt_atualizacao,
						cd_perfil,
						cd_cargo_lib)	   
				values	(	permissao_local_usuario_seq.nextval,
						nr_seq_regra_evento_w,
						nm_usuario_p,
						nm_usuario_destino_p,
						nm_usuario_p,
						ie_permissao_w,
						sysdate,
						sysdate,
						cd_perfil_w,
						cd_cargo_lib_w);
end loop;
close C01;
				
commit;

end copiar_lib_estoque_usuario;
/

