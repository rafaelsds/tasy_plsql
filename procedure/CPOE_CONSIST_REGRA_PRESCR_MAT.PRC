create or replace 
procedure cpoe_consist_regra_prescr_mat (	nr_atendimento_p         number,
						cd_material_p            number,
						ie_assoc_proc_p          varchar2,
						ie_via_aplicacao_p       varchar2,
						ie_tipo_atendimento_p    number,
						cd_setor_atendimento_p   number,
						cd_perfil_p              number,
						cd_convenio_p            number,
						nm_usuario_p             varchar2,
						cd_estabelecimento_p     number,
						cd_paciente_p            varchar2,
						dt_inicio_p              date,
						ds_erro_p                out varchar2,
						ds_mensagem_p            out varchar2,
						ie_abortar_p             out varchar2,
						cd_intervalo_p           cpoe_material.cd_intervalo%type default null,
						ie_duracao_p             cpoe_material.ie_duracao%type default null) is
	ds_retorno_w   varchar2(4000) := 'S';
begin

select 	nvl(max(ie_forma_consistencia),'X')
into 	ie_abortar_p
from 	regra_consiste_prescr_par
where 	nr_seq_regra   = 168 
and	(( cd_perfil      = cd_perfil_p ) or ( cd_perfil is null ) );

if	( ie_abortar_p <> 'X' ) then
	ds_retorno_w := cpoe_obter_se_medic_lib_med(	nr_atendimento_p,
							cd_material_p,
							ie_assoc_proc_p,
							ie_via_aplicacao_p,
							ie_tipo_atendimento_p ,
							cd_setor_atendimento_p,
							cd_perfil_p,
							cd_convenio_p,
							nm_usuario_p,
							cd_estabelecimento_p,
							cd_paciente_p,
							dt_inicio_p,
							cd_intervalo_p,
							ie_duracao_p);
end if;

if ( ds_retorno_w = 'S' ) then
	ds_erro_p := 'N';
elsif ( ds_retorno_w = 'M' ) then
	ds_erro_p := 'M';
elsif ( ds_retorno_w = 'D' ) then
	ds_erro_p := 'D';
else
	ds_erro_p := 'S';
	if ( ds_retorno_w <> 'N' ) then
		ds_mensagem_p := ds_retorno_w;
	else
		ds_mensagem_p := obter_desc_expressao(554794,'');
	end if;
end if;

end cpoe_consist_regra_prescr_mat;
/