create or replace
procedure consistir_gerar_prescricao_js(	nr_atendimento_p		number,
											ie_momento_p			varchar2,
											ie_prescricao_alta_p	varchar2,
											dt_prescricao_p			Date,
											ds_mensagem_p 		out	varchar2,
											ie_acao_p 			out	varchar2,
											ie_anamnese_p 		out	varchar2,
											ie_evolucao_p 		out	varchar2,
											ie_posicionar_pep_p	out	regra_consiste_inf_rep.ie_posicionar_pep%type,
											cd_setor_atendimento_p	number,
											cd_estabelecimento_p	number,
											cd_perfil_p				number,
											nm_usuario_p			varchar2) is 

ie_acao_w		varchar2(10);
ds_mensagem_w		varchar2(255);		
ds_mensagem_adic_w	varchar2(255);
ie_anamnese_w		varchar2(1);
ie_evolucao_w		varchar2(1);
ie_diagnostico_w		varchar2(1);
ie_recomendacao_w	varchar2(1);
ie_escala_indice_w		varchar2(10);
ie_receita_w		varchar2(1);
ie_parecer_w		varchar2(1);
ie_aval_w		varchar2(1);
ie_peso_w		varchar2(1);
cd_protocolo_w		number(10);
ie_sus_w			varchar2(1);
ie_autorizacao_w		varchar2(1);
ie_item_prontuario_w 	varchar2(5);
ie_posicionar_pep_w 	varchar2(1);

begin

consistir_prescr_apos_alta(nr_atendimento_p,nm_usuario_p);

consiste_regra_inf_rep(
	cd_estabelecimento_p,
	nr_atendimento_p,
	cd_setor_atendimento_p,
	cd_perfil_p,
	ie_momento_p,
	ie_prescricao_alta_p,
	dt_prescricao_p,
	nm_usuario_p,
	ds_mensagem_w,
	ds_mensagem_adic_w,
	ie_acao_w,
	ie_anamnese_w,
	ie_evolucao_w,
	ie_receita_w,
	ie_diagnostico_w,
	ie_escala_indice_w,
	null,
	null,
	null,
	ie_recomendacao_w,
	cd_protocolo_w,
	ie_parecer_w,
	ie_peso_w,
	ie_autorizacao_w,
	ie_sus_w,
	ie_aval_w,
	ie_item_prontuario_w,
	ie_posicionar_pep_w);

commit;
ie_acao_p	:= ie_acao_w;
ds_mensagem_p	:= substr(ds_mensagem_w,1,255);
ie_anamnese_p	:= ie_anamnese_w;
ie_evolucao_p	:= ie_evolucao_w;
ie_posicionar_pep_p := ie_posicionar_pep_w;

end consistir_gerar_prescricao_js;
/
