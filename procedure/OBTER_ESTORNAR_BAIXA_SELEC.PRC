create or replace
procedure obter_estornar_baixa_selec(nr_titulo_p	number,
			nr_seq_baixa_p 		number,
			cd_tipo_baixa_p 		number,
			qt_registros_p		out number,
			ie_estornar_p		out varchar2) is
qt_registros_w		number(10,0);
ie_estornar_w		varchar2(1);
begin
if	(nr_titulo_p is not null) and 
	(nr_seq_baixa_p is not null) then
	begin
	select 	count(*)         
	into	qt_registros_w
	from    	titulo_pagar_baixa a
	where   	a.nr_titulo = nr_titulo_p
	and     	a.nr_seq_baixa_origem = nr_seq_baixa_p;
	end;
end if;	
if 	(cd_tipo_baixa_p is not null) then
	begin	
	select  	nvl(max(a.ie_estornar),'S')
	into	ie_estornar_w
	from   	tipo_baixa_cpa a
	where  	a.cd_tipo_baixa   = cd_tipo_baixa_p;
	end;	
end if;
qt_registros_p 	:= qt_registros_w;
ie_estornar_p	:= ie_estornar_w;
end obter_estornar_baixa_selec;
/