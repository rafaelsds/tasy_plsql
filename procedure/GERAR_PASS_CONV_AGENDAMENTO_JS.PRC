create or replace
procedure gerar_pass_conv_agendamento_js(	cd_tipo_agenda_p		number,
					nr_seq_agenda_p		number,
					ie_gerar_unid_compl_p	varchar2,
					nr_atendimento_p		number,
					dt_entrada_p		date,
					nm_usuario_p		varchar2,
					ds_dados_agenda_p	out number,
					qt_agenda_p		out number) is 

ds_dados_agenda_w	varchar2(255);
qt_agenda_w		number(10,0);
nr_seq_unidade_w	number(10,0);
nr_seq_interno_w	number(10,0);
			
begin

ds_dados_agenda_w	:= obter_dados_agendas(cd_tipo_agenda_p, nr_seq_agenda_p, 'CS');

select 	count(*) 
into	qt_agenda_w
from 	atend_paciente_unidade 
where 	nr_atendimento = nr_atendimento_p;

if	(ie_gerar_unid_compl_p = 'S')and
	(cd_tipo_agenda_p = 5)and
	(to_number(ds_dados_agenda_w) > 0)and
	(qt_agenda_w = 0)then
	begin
	
	select  max(nr_seq_unidade)
	into	nr_seq_unidade_w
	from    agenda_consulta
	where   nr_atendimento = nr_atendimento_p;
	
	ageserv_gerar_pass_setor_atend(nr_atendimento_p, to_number(ds_dados_agenda_w), dt_entrada_p, 'S', to_char(nr_seq_unidade_w), nm_usuario_p);
	
	end;
else
	begin
	
	if	(to_number(ds_dados_agenda_w) > 0)and
		(qt_agenda_w = 0)then
		begin
		
		gerar_passagem_setor_atend(nr_atendimento_p, to_number(ds_dados_agenda_w), dt_entrada_p, 'S', nm_usuario_p);
		
		select	nvl(max(nr_seq_interno),0) 
		into	nr_seq_interno_w
		from 	atend_paciente_unidade 
		where 	nr_atendimento = nr_atendimento_p;
		
		if	(nr_seq_interno_w > 0) then
			atend_paciente_unid_afterpost(nr_seq_interno_w, 'I', nm_usuario_p);
		end if;
		end;
	end if;
	
	end;
end if;

ds_dados_agenda_p	:= to_number(ds_dados_agenda_w);
qt_agenda_p		:= qt_agenda_w;

commit;

end gerar_pass_conv_agendamento_js;
/