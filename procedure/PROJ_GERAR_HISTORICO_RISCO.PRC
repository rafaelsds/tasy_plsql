create or replace
procedure proj_gerar_historico_risco(	nr_seq_risco_p		number,
					ds_historico_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin

insert into proj_risco_hist (nr_sequencia,
			nr_seq_risco,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_historico,
			ds_historico)
		values	(proj_risco_hist_seq.nextval,
			nr_seq_risco_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ds_historico_p);


commit;

end proj_gerar_historico_risco;
/