create or replace
procedure ptu_gerar_incosist_scs
			(	nr_seq_glosa_p		Number,
				nr_seq_transacao_p	Number,
				ie_tipo_transacao_p	Varchar2,
				cd_transacao_p		Varchar2,
				cd_estabelecimento_p	Number,
				nr_seq_procedimento_p	Number,
				nr_seq_material_p	Number,
				nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_glosa_p = 561) then
	ptu_inserir_inconsistencia(	null, null, 2005,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2046
/*elsif	(nr_seq_glosa_p = 566) then
	ptu_inserir_inconsistencia(	null, null, 2046,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					null, null, nm_usuario_p);*/
--------------------------------2008
elsif	(nr_seq_glosa_p	= 486) then
	ptu_inserir_inconsistencia(	null, null, 2008,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2009
elsif	(nr_seq_glosa_p	= 614) then
	ptu_inserir_inconsistencia(	null, null, 2009,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2010
elsif	(nr_seq_glosa_p	= 560) then
	ptu_inserir_inconsistencia(	null, null, 2010,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2011
elsif	(nr_seq_glosa_p	= 615) then
	ptu_inserir_inconsistencia(	null, null, 2011,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2014
elsif	((nr_seq_glosa_p	= 563) or (nr_seq_glosa_p	= 559)) then
	ptu_inserir_inconsistencia(	null, null, 2014,
					'', cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2015
elsif	(nr_seq_glosa_p	= 662) then
	ptu_inserir_inconsistencia(	null, null, 2015,
					'', cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2016
elsif	(nr_seq_glosa_p	= 550) then
	ptu_inserir_inconsistencia(	null, null, 2016,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2017
elsif	(nr_seq_glosa_p	= 487) then
	ptu_inserir_inconsistencia(	null, null, 2017,
					'', cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2022
elsif	(nr_seq_glosa_p	= 579) then
	ptu_inserir_inconsistencia(	null, null, 2022,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2024
elsif	(nr_seq_glosa_p	= 553) then
	ptu_inserir_inconsistencia(	null, null, 2024,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2038
elsif	(nr_seq_glosa_p	= 474) then
	ptu_inserir_inconsistencia(	null, null, 2038,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2044
elsif	(nr_seq_glosa_p	= 558) then
	ptu_inserir_inconsistencia(	null, null, 2044,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2049
elsif	(nr_seq_glosa_p	= 556) then
	ptu_inserir_inconsistencia(	null, null, 2049,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
--------------------------------2053
elsif	(nr_seq_glosa_p	= 672) then
	ptu_inserir_inconsistencia(	null, null, 2053,
					'',cd_estabelecimento_p, nr_seq_transacao_p, 
					ie_tipo_transacao_p, cd_transacao_p, nr_seq_procedimento_p, 
					nr_seq_material_p, null, nm_usuario_p);
end if;

end ptu_gerar_incosist_scs;
/