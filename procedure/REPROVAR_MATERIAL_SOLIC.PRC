create or replace
procedure reprovar_material_solic(	nr_seq_solic_p		number,
				ie_opcao_p		varchar2,
				nr_seq_motivo_reprov_p	number,
				nm_usuario_p		varchar2) is

begin

pls_atualizar_mat_med_solic(nr_seq_solic_p, ie_opcao_p, nm_usuario_p,'S');

pls_insere_motivo_rep_mat_med(nr_seq_solic_p, nr_seq_motivo_reprov_p, nm_usuario_p);

end reprovar_material_solic;
/