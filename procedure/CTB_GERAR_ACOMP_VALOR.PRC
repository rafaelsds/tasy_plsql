create or replace
procedure ctb_gerar_acomp_valor(	cd_estab_p		varchar2,
				nr_seq_cenario_p		number,
				cd_classif_conta_p		varchar2,
				cd_classif_centro_p		varchar2,
				cd_conta_contabil_p	varchar2,
				cd_centro_custo_p		varchar2,
				qt_min_nivel_conta_p	number,
				qt_max_nivel_conta_p	number,
				qt_min_nivel_centro_p	number,
				qt_max_nivel_centro_p	number,
				nm_usuario_p		varchar2,
				ie_ordem_inicio_p		varchar2,
				ie_informacao_p		number,
				nr_seq_mes_contabil_p	number) is


cd_conta_gravar_w			varchar2(20);
cd_centro_gravar_w	 	number(8);
cd_centro_custo_w			number(08,0);
cd_centro_custo_ww		number(08,0)	:= 0;
cd_centro_ww			number(08,0);
cd_conta_contabil_w		varchar2(40);
cd_conta_contabil_ww		varchar2(40)	:= '00000000';
cd_conta_ww			varchar2(40);
cd_classif_w			varchar2(255);
cd_classif_ww			varchar2(255);
cd_classificacao_w			varchar2(40);
cd_classificacao_ww		varchar2(40);
cd_classif_conta_w			varchar2(40);
cd_classif_centro_w		varchar2(40);
cd_empresa_w			number(4);
dt_inicial_cen_w			date;
dt_final_cen_w			date;
dt_mes_ref_w			date;
ds_gerencial_w			varchar2(255);
ds_centro_w			varchar2(80);
ds_conta_w			varchar2(255);
ie_gerar_w			varchar2(1);
ie_informacao_w			number(1);
ie_tipo_conta_w			varchar2(1);
ie_pos_w				number(15,0);
nr_seq_mes_ref_w			number(10);
nr_sequencia_w			number(10);
qt_commit_w			number(10);
qt_nivel_max_w			number(03,0);
qt_nivel_min_w			number(03,0);
qt_nivel_2_w			number(03,0);
qt_registro_w			number(10);
i				integer;
j				integer;
k				integer;
y				integer;
z				integer;
w				integer;
vl_orcado_w			number(15,2);
ie_debito_credito_w			varchar2(01);
vl_contabil_w			number(15,2);
vl_contabil_acum_w		number(15,2);
ie_separador_conta_w		empresa.ie_sep_classif_conta_ctb%type;

cursor c01 is
select	0 ie_informacao,
	a.cd_centro_custo,
	a.cd_conta_contabil,
	c.ie_tipo,
	nvl(sum(a.vl_orcado),0)
from	conta_contabil c,
	ctb_orc_cen_valor a
where	a.cd_conta_contabil	= c.cd_conta_contabil
and	a.nr_seq_cenario		= nr_seq_cenario_p
and	(cd_estab_p is null or substr(ctb_obter_se_elemento_contido(a.cd_estabelecimento, cd_estab_p),1,1) = 'S')
and	c.ie_tipo			= 'A'
and	c.ie_situacao		= 'A'
group by a.cd_centro_custo,
	 a.cd_conta_contabil,
	 c.ie_tipo;

cursor c02 is
select	a.nr_sequencia
from	ctb_mes_ref a
where	a.cd_empresa	= cd_empresa_w
and	a.dt_referencia between dt_inicial_cen_w and dt_final_cen_w
order by 1;

begin

qt_commit_w	:= 0;

ie_separador_conta_w		:= philips_contabil_pck.get_separador_conta;


select	cd_empresa,
	ctb_obter_mes_ref(nr_seq_mes_inicio),
	ctb_obter_mes_ref(nr_seq_mes_fim)
into	cd_empresa_w,
	dt_inicial_cen_w,
	dt_final_cen_w
from	ctb_orc_cenario
where	nr_sequencia	= nr_seq_cenario_p;

delete from w_ctb_acomp_valor
where	nr_seq_cenario	= nr_seq_cenario_p
and	nm_usuario 	= nm_usuario_p;
commit;

if	(nvl(nr_seq_mes_contabil_p,0) <> 0) then
	
	select	dt_referencia
	into	dt_mes_ref_w
	from	ctb_mes_ref
	where	nr_sequencia = nr_seq_mes_contabil_p;

end if;

open  c01;
loop
fetch c01 into 
	ie_informacao_w,
	cd_centro_custo_w,
	cd_conta_contabil_w,
	ie_tipo_conta_w,
	vl_orcado_w;
exit when c01%notfound;
	begin
	ie_gerar_w	:= 'S';

	if  	(nvl(cd_classif_conta_p,'0') <> '0')  then
		begin
		ie_gerar_w	:= 'N';
		cd_classif_w	:= cd_classif_conta_p;
		cd_classif_w	:= replace(cd_classif_w,'(','');
		cd_classif_w	:= replace(cd_classif_w,')','');
		cd_classif_w	:= replace(cd_classif_w,' ','');
		while (ie_gerar_w = 'N') and (length(cd_classif_w) > 0)  loop
			begin
			ie_pos_w 	:= instr(cd_classif_w,',');
			if	(ie_pos_w = 0) then
				cd_classif_ww	:= cd_classif_w;
				cd_classif_w	:= '';
			else
				cd_classif_ww	:= substr(cd_classif_w,1, ie_pos_w - 1);
				cd_classif_w	:= substr(cd_classif_w, ie_pos_w + 1, 255);
			end if;
			
			select	ctb_obter_se_conta_classif_sup(cd_conta_contabil_w, cd_classif_ww)
			into	ie_gerar_w
			from	dual;
			end;
		end loop;
		end;
	end if;

	if 	(ie_gerar_w	= 'S')  and
	  	(nvl(cd_classif_centro_p,'0') <> '0')  then
		begin
		ie_gerar_w	:= 'N';
		cd_classif_w	:= cd_classif_centro_p;
		cd_classif_w	:= replace(cd_classif_w,'(','');
		cd_classif_w	:= replace(cd_classif_w,')','');
		cd_classif_w	:= replace(cd_classif_w,' ','');
		while (ie_gerar_w = 'N') and (length(cd_classif_w) > 0)  loop
			begin
			ie_pos_w 		:= instr(cd_classif_w,',');
			if	(ie_pos_w = 0) then
				cd_classif_ww	:= cd_classif_w;
				cd_classif_w	:= '';
			else
				cd_classif_ww	:= substr(cd_classif_w,1, ie_pos_w - 1);
				cd_classif_w	:= substr(cd_classif_w, ie_pos_w + 1, 255);
			end if;
			select	ctb_obter_se_centro_sup(cd_centro_custo_w, cd_classif_ww)
			into	ie_gerar_w
			from dual;
  		end;
		end loop;
		end;
	end if;

	if	(ie_gerar_w	= 'S') then
		select	ctb_obter_se_centro_usuario(cd_centro_custo_w, cd_empresa_w, nm_usuario_p)
		into	ie_gerar_w
		from	dual;
	end if;

	if 	(ie_gerar_w	= 'S')  and
	  	(nvl(cd_centro_custo_p, '0') <> '0') then
		select	ctb_obter_se_centro_contido(cd_centro_custo_w, cd_centro_custo_p)
		into	ie_gerar_w
		from	dual;
	end if;
	if 	(ie_gerar_w	= 'S')  and
	  	(nvl(cd_conta_contabil_p,'0')	<> '0') then
		select	ctb_obter_se_conta_contida(cd_conta_contabil_w, cd_conta_contabil_p)
		into	ie_gerar_w
		from	dual;
	end if;

	if 	(ie_gerar_w	= 'S')  then
		begin
		if	(cd_conta_contabil_w	<> cd_conta_contabil_ww) then
			select	ctb_obter_conta_nivel(cd_conta_contabil_w, qt_max_nivel_conta_p, dt_inicial_cen_w)
			into	cd_conta_ww
			from	dual;

			select	max(cd_classificacao),
				max(ds_conta_contabil),
				max(b.ie_debito_credito)
			into	cd_classif_conta_w,
				ds_conta_w,
				ie_debito_credito_w
			from	ctb_grupo_conta b,
				conta_contabil a
			where	a.cd_grupo		= b.cd_grupo
			and	a.cd_conta_contabil	= cd_conta_ww;
			cd_conta_contabil_ww		:= cd_conta_contabil_w;
		end if;


		if	(cd_centro_custo_w	<> cd_centro_custo_ww) then
			select	ctb_obter_centro_nivel(cd_centro_custo_w, qt_max_nivel_centro_p)
			into	cd_centro_ww
			from	dual;
			
			select	max(cd_classificacao),
				max(ds_centro_custo)
			into	cd_classif_centro_w,
				ds_centro_w
			from	centro_custo
			where	cd_centro_custo	= cd_centro_ww;
			cd_centro_custo_ww	:= cd_centro_custo_w;
		end if;

		if	(ie_ordem_inicio_p = 'C') then
			cd_classificacao_w	:= cd_classif_conta_w || ie_separador_conta_w || cd_classif_centro_w;
			ds_gerencial_w		:= ds_centro_w;
			qt_nivel_2_w		:= qt_max_nivel_conta_p + qt_min_nivel_centro_p;
			qt_nivel_max_w		:= qt_max_nivel_conta_p;
			qt_nivel_min_w		:= qt_min_nivel_conta_p;
			cd_classificacao_ww	:= cd_classif_conta_w;
		else
			cd_classificacao_w	:= cd_classif_centro_w || ie_separador_conta_w || cd_classif_conta_w;
			ds_gerencial_w		:= ds_conta_w;
			qt_nivel_2_w		:= qt_max_nivel_centro_p + qt_min_nivel_conta_p;
			qt_nivel_max_w		:= qt_max_nivel_centro_p;
			qt_nivel_min_w		:= qt_min_nivel_centro_p;
			cd_classificacao_ww	:= cd_classif_centro_w;
		end if;
		y				:= 1;
		for i in 1..length(cd_classificacao_w) loop
			if	(substr(cd_classificacao_w,i,1) = ie_separador_conta_w) then
				y		:= y + 1;
			end if;
		end loop;
		z				:= y;
		while	(y >= qt_nivel_min_w) loop
			select	/*+ index(wctbval_i2) */
				count(*)
			into	qt_registro_w
			from	w_ctb_acomp_valor
			where	cd_classificacao	= cd_classificacao_w
			and	nr_seq_cenario	= nr_seq_cenario_p
			and 	nm_usuario 	= nm_usuario_p
			and	cd_conta_contabil	= cd_conta_contabil_w;

			if	(qt_registro_w > 0) then
				open c02;
				loop
				fetch c02 into
					nr_seq_mes_ref_w;
				exit when c02%notfound;

					select	/*+ index(wctbval_i1) */
						nvl(max(nr_sequencia),0)
					into	nr_sequencia_w
					from	w_ctb_acomp_valor
					where	cd_classificacao= cd_classificacao_w
					and	nr_seq_cenario	= nr_seq_cenario_p
					and nm_usuario = nm_usuario_p
					and	nr_seq_mes_ref	= nr_seq_mes_ref_w;

					if	(ie_informacao_w = 0) then
						select	/*+ index(a ctborcv_i1) */
							nvl(sum(a.vl_orcado),0)
						into	vl_orcado_w
						from	ctb_orc_cen_valor a
						where	a.nr_seq_cenario		= nr_seq_cenario_p
						and	a.cd_conta_contabil	= cd_conta_contabil_w
						and	a.cd_centro_custo		= cd_centro_custo_w
						and	(cd_estab_p is null or substr(ctb_obter_se_elemento_contido(a.cd_estabelecimento, cd_estab_p),1,1) = 'S')
						and	a.nr_seq_mes_ref		= nr_seq_mes_ref_w;

					end if;	
					update	w_ctb_acomp_valor
					set	vl_orcado		= nvl(vl_orcado,0) + vl_orcado_w
					where	nr_sequencia	= nr_sequencia_w;
					
					qt_commit_w	:= qt_commit_w + 1;
					if	(qt_commit_w >= 3000) then
						commit;
						qt_commit_w	:= 0;
					end if;
				end loop;
				close c02;
				
				else
					begin
					j	:= y - qt_min_nivel_conta_p - qt_min_nivel_centro_p + 1;
					if	(y = z) then
						w		:= 0;
					elsif	(y > qt_nivel_max_w) then
					if	(ie_ordem_inicio_p = 'C') then
						w	:= 10 + y - qt_nivel_max_w;
					else
						w	:= 20 + y - qt_nivel_max_w;
					end if;
				else
					j	:= y - qt_nivel_min_w;
					if	(ie_ordem_inicio_p = 'C') then
						w	:= 20 + y;
					else
						w	:= 10 + y;
					end if;
				end if;

				if	(w > 20) then
					
					select	ctb_obter_conta_nivel(cd_conta_contabil_w, w - 20, dt_inicial_cen_w)
					into	cd_conta_ww
					from	dual;		
					select	max(ds_conta_contabil)
					into	ds_gerencial_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_ww;
					
				elsif	(w > 10) then
					select	ctb_obter_centro_nivel(cd_centro_custo_w, w - 10)
					into	cd_centro_ww
					from	dual;

					select	max(ds_centro_custo)
					into	ds_gerencial_w
					from	centro_custo
					where	cd_centro_custo		= cd_centro_ww;

				end if;

				ds_gerencial_w	:= substr(lpad(' ', j * 2) || ds_gerencial_w,1,255);
				
				select	ie_tipo
				into	ie_tipo_conta_w
				from	conta_contabil
				where	cd_conta_contabil	= nvl(cd_conta_ww,cd_conta_contabil_w);	
				cd_conta_gravar_w	:= substr(cd_conta_contabil_w,1,20);
				cd_centro_gravar_w	:= cd_centro_custo_w;
				if	(w > 0) then
					cd_conta_gravar_w	:= '';
					cd_centro_gravar_w	:= null;
				end if;
				
				open c02;
				loop
				fetch c02 into
					nr_seq_mes_ref_w;
				exit when c02%notfound;
				begin
					select	w_ctb_acomp_valor_seq.nextval
					into	nr_sequencia_w
					from	dual;
					
					if	(ie_informacao_w = 0) then
						select	/*+ index(a ctborcv_i1) */
							nvl(sum(a.vl_orcado),0)
						into	vl_orcado_w
						from	ctb_orc_cen_valor a
						where	a.nr_seq_cenario	= nr_seq_cenario_p
						and	a.cd_conta_contabil	= cd_conta_contabil_w
						and	a.cd_centro_custo	= cd_centro_custo_w
						and	(cd_estab_p is null or substr(ctb_obter_se_elemento_contido(a.cd_estabelecimento, cd_estab_p),1,1) = 'S')
						and	a.nr_seq_mes_ref	= nr_seq_mes_ref_w;
					end if;	
					
					if	(nvl(nr_seq_mes_contabil_p,0) <> 0) then
						

						select	nvl(sum(a.vl_saldo),0)
						into	vl_contabil_acum_w
						from	ctb_mes_ref b,
							ctb_saldo a
						where	a.nr_seq_mes_ref	= b.nr_sequencia
						and	b.cd_empresa		= cd_empresa_w
						and	a.cd_conta_contabil	= nvl(cd_conta_gravar_w, cd_conta_ww)
						and	a.cd_centro_custo	= cd_centro_gravar_w
						and	(cd_estab_p is null or substr(ctb_obter_se_elemento_contido(a.cd_estabelecimento, cd_estab_p),1,1) = 'S')
						and	b.dt_referencia between trunc(dt_mes_ref_w,'year') and dt_mes_ref_w;

						select	nvl(sum(a.vl_saldo),0)
						into	vl_contabil_w
						from	ctb_saldo a
						where	(cd_estab_p is null or substr(ctb_obter_se_elemento_contido(a.cd_estabelecimento, cd_estab_p),1,1) = 'S')
						and	a.cd_conta_contabil	= nvl(cd_conta_gravar_w, cd_conta_ww)
						and	a.cd_centro_custo	= cd_centro_gravar_w
						and	a.nr_seq_mes_ref	= nr_seq_mes_contabil_p;						
					end if;
					
					
					if	(ie_ordem_inicio_p <> 'C' ) and
						(ie_debito_credito_w = 'D') and
						(y = qt_nivel_max_w) then
						vl_orcado_w		:= vl_orcado_w * -1;
					end if;
					
					insert into w_ctb_acomp_valor(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_centro_custo,
						cd_conta_contabil,
						cd_classif_conta,
						cd_classificacao,
						nr_seq_cenario,
						nr_seq_mes_ref,
						ie_informacao,
						ds_gerencial,
						vl_orcado,
						vl_contabil,
						vl_contabil_acum,
						ie_tipo_conta)
					values(nr_sequencia_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_centro_gravar_w,
						nvl(cd_conta_gravar_w,cd_conta_ww),
						cd_classif_conta_w,
						cd_classificacao_w,
						nr_seq_cenario_p,
						nr_seq_mes_ref_w,
						decode(ie_informacao_w,0,'V',1,'T',''),
						ds_gerencial_w,
						vl_orcado_w,
						nvl(vl_contabil_w,0),
						nvl(vl_contabil_acum_w,0),
						ie_tipo_conta_w);
					qt_commit_w	:= qt_commit_w + 1;
					if	(qt_commit_w >= 3000) then
						commit;
						qt_commit_w	:= 0;
					end if;
					
					
				end;
			end loop;
			close c02;
				end;
			end if;
			
			if	(y 	> qt_nivel_max_w) and
				(y 	<= qt_nivel_2_w) then
				cd_classificacao_w	:= cd_classificacao_ww;
			else
				select instr(cd_classificacao_w, ie_separador_conta_w, -1)
				into	k from dual;
				cd_classificacao_w	:= substr(cd_classificacao_w,1,k -1);
			end if;
			y				:= 0;
			if	(cd_classificacao_w is not null) then
				y			:= 1;
				for i in 1..length(cd_classificacao_w) loop
					if	(substr(cd_classificacao_w,i,1) = ie_separador_conta_w) then
						y	:= y + 1;
				end if;
				end loop;
			end if;
			if	(ie_ordem_inicio_p <> 'C' ) and
				(ie_debito_credito_w = 'D' ) and
				(y = qt_nivel_max_w) then
				vl_orcado_w		:= vl_orcado_w * -1;
			end if;
			
		end loop;
		end;
	end if;
	end;
end loop;
close c01;
commit;

end ctb_gerar_acomp_valor;
/