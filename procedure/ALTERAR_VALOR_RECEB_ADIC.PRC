create or replace 
procedure alterar_valor_receb_adic	(nr_seq_receb_p	number,
					nm_usuario_p	varchar2) is


cd_estabelecimento_w		number(4);
nr_lote_contabil_rec_w		number(10);
vl_deposito_w			number(15,2);
nr_seq_tran_financ_w		number(10);
vl_adicional_w			number(15,2);
nr_lote_contabil_adic_w		number(10);
ie_vl_conv_rec_adic_w		varchar2(1);
vl_despesa_bancaria_w		number(15,2);
vl_saldo_w			number(15,2);
ie_status_w			varchar2(1);
cd_convenio_w			number(10);
dt_recebimento_w		date;

begin

select 	nvl(to_number(vl_recebimento - obter_baixa_conrece(nr_sequencia)),0),
	cd_estabelecimento,
	cd_convenio,
	dt_recebimento
into	vl_saldo_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	dt_recebimento_w
from	convenio_receb
where	nr_sequencia = nr_seq_receb_p;

select 	nvl(max(ie_vl_conv_rec_adic),'N')
into	ie_vl_conv_rec_adic_w
from 	parametro_contas_receber
where 	cd_estabelecimento = cd_estabelecimento_w;

ie_vl_conv_rec_adic_w	:= nvl(OBTER_SE_ATUALIZA_JUROS_RECEB(cd_convenio_w,dt_recebimento_w), ie_vl_conv_rec_adic_w);

if	(ie_vl_conv_rec_adic_w = 'S') then

	ie_status_w	:= 'P';
	if (vl_saldo_w = 0) then
		ie_status_w	:= 'T';
	end if;

	update convenio_receb
	set nm_usuario = nm_usuario_p,
	    ie_status = ie_status_w
	where nr_sequencia = nr_seq_receb_p;

end if;

commit;
end;
/