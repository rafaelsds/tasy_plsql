create or replace
procedure importar_w_func_fucs is 

cd_municipio_ibge_w	varchar2(6);
cd_pessoa_fisica_w	varchar2(10);
ds_erro_w		varchar2(1000);
nr_sequencia_w		number(10)	:= 0;
qt_registro_w		number(10)	:= 0;

cursor c01 is
select	nome,
	data_nasc,
	sexo,
	decode(upper(estado_civ),
		'SOLTEIRO','1',
		'CASADO','2',
		'DIVORCIADO','3',
		'DESQUITADO','4',
		'VI�VO','5',
		'SEPARADO','6',
		'CONCUBINADO','7',
		'9') ie_estado_civil,
	cpf,
	rg,
	orgao_exp,
	data_exp,
	rua,
	num,
	complem,
	bairro,
	cep,
	cidade,
	ddd,
	telefone,
	mae,
	pai
from	w_hg_funcionario
order by 1;

vet01 C01%RowType;

cursor c02 is
select	trigger_name
from	user_triggers
where	table_name in('PESSOA_FISICA','COMPL_PESSOA_FISICA');

vet02 C02%rowType;

begin

open C02;
loop
fetch C02 into	
	Vet02;
exit when C02%notfound;
	begin
	exec_sql_dinamico('OS132872', 'alter trigger ' || vet02.trigger_name || ' disable');
	end;
end loop;
close C02;

begin

open C01;
loop
fetch C01 into	
	Vet01;
exit when C01%notfound;
	begin
	
	qt_registro_w		:= qt_registro_w + 1;
	cd_municipio_ibge_w	:= '';
	
	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;
	
	
	begin
	
	insert into pessoa_fisica(
		cd_pessoa_fisica,
		nm_pessoa_fisica,
		ie_sexo,
		ie_estado_civil,
		nr_cpf,
		nr_identidade,
		ds_orgao_emissor_ci,
		dt_emissao_ci,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_pessoa,
		nm_pessoa_fisica_sem_acento,
		ie_funcionario)
	values(	cd_pessoa_fisica_w,
		vet01.nome,
		vet01.sexo,
		vet01.ie_estado_civil,
		vet01.cpf,
		vet01.rg,
		vet01.orgao_exp,
		vet01.data_exp,
		'Importacao',
		sysdate,
		'Importacao',
		sysdate,
		1,
		elimina_acentuacao(vet01.nome),
		'S');
		
	nr_sequencia_w	:= 1;
	
	select	max(cd_municipio_ibge)
	into	cd_municipio_ibge_w
	from	sus_municipio
	where	upper(ds_municipio) = upper(vet01.cidade);
	
	insert into compl_pessoa_fisica(
		cd_pessoa_fisica,
		ie_tipo_complemento,
		nr_sequencia,
		nm_contato,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		ds_bairro,
		cd_cep,
		ds_municipio,
		cd_municipio_ibge,
		nr_ddd_telefone,
		nr_telefone,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec)
	values(	cd_pessoa_fisica_w,
		1,
		nr_sequencia_w,
		'',
		vet01.rua,
		somente_numero(substr(vet01.num,1,5)),
		vet01.complem,
		vet01.bairro,
		substr(vet01.cep,1,8),
		vet01.cidade,
		cd_municipio_ibge_w,
		vet01.ddd,
		vet01.telefone,
		'Importacao',
		sysdate,
		'Importacao',
		sysdate);
	
	if	(vet01.mae is not null) then
		
		nr_sequencia_w	:= nr_sequencia_w + 1;
		
		insert into compl_pessoa_fisica(
			cd_pessoa_fisica,
			ie_tipo_complemento,
			nr_sequencia,
			nm_contato,
			ds_endereco,
			nr_endereco,
			ds_complemento,
			ds_bairro,
			cd_cep,
			ds_municipio,
			cd_municipio_ibge,
			nr_ddd_telefone,
			nr_telefone,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		values(	cd_pessoa_fisica_w,
			5,
			nr_sequencia_w,
			vet01.mae,
			vet01.rua,
			somente_numero(substr(vet01.num,1,5)),
			vet01.complem,
			vet01.bairro,
			substr(vet01.cep,1,8),
			vet01.cidade,
			'',
			vet01.ddd,
			vet01.telefone,
			'Importacao',
			sysdate,
			'Importacao',
			sysdate);
	end if;
	
	if	(vet01.pai is not null) then
	
		nr_sequencia_w	:= nr_sequencia_w + 1;
		
		insert into compl_pessoa_fisica(
			cd_pessoa_fisica,
			ie_tipo_complemento,
			nr_sequencia,
			nm_contato,
			ds_endereco,
			nr_endereco,
			ds_complemento,
			ds_bairro,
			cd_cep,
			ds_municipio,
			cd_municipio_ibge,
			nr_ddd_telefone,
			nr_telefone,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		values(	cd_pessoa_fisica_w,
			4,
			nr_sequencia_w,
			vet01.pai,
			vet01.rua,
			somente_numero(substr(vet01.num,1,5)),
			vet01.complem,
			vet01.bairro,
			substr(vet01.cep,1,8),
			vet01.cidade,
			'',
			vet01.ddd,
			vet01.telefone,
			'Importacao',
			sysdate,
			'Importacao',
			sysdate);
	end if;
	
	exception when others then	
		ds_erro_w	:= sqlerrm(sqlcode);
		insert into log_tasy(
			cd_log,
			ds_log,
			nm_usuario,
			dt_atualizacao)
		values(	1453,
			'Erro ao importar PF: ' || vet01.nome || ' ERRO: ' || ds_erro_w,
			'Tasy',
			sysdate);
	end;
	
	if	(qt_registro_w >= 100) then
		commit;
		qt_registro_w	:= 0;
	end if;
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	Vet02;
exit when C02%notfound;
	begin
	exec_sql_dinamico('OS132872', 'alter trigger ' || vet02.trigger_name || ' enable');
	end;
end loop;
close C02;

exception when others then
	open C02;
	loop
	fetch C02 into	
		Vet02;
	exit when C02%notfound;
		begin
		exec_sql_dinamico('OS132872', 'alter trigger ' || vet02.trigger_name || ' enable');
		end;
	end loop;
	close C02;
end;

commit;

end importar_w_func_fucs;
/
