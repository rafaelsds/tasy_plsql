create or replace
procedure pls_atualiza_status_processo(	nm_usuario_p		usuario.nm_usuario%type) is 

ie_libera_w	varchar2(1);

-- busca todos os que est�o em processamento
cursor C01 is
	select	nr_sequencia
	from	pls_cta_lote_processo
	where	ie_status = '2';
	
begin

for r_C01_w in C01 loop

	ie_libera_w := pls_pode_liberar_nova_geracao(	r_C01_w.nr_sequencia);

	if	(ie_libera_w = 'S') then

		-- se pode liberar atualiza o status
		update	pls_cta_lote_processo
		set	ie_status = '1',
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = r_C01_w.nr_sequencia;

	end if;
end loop;

end pls_atualiza_status_processo;
/