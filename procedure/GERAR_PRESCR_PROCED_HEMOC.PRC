create or replace
procedure gerar_prescr_proced_hemoc(	nr_seq_derivado_p	number,
					nr_seq_solic_sangue_p	number,
					nr_prescricao_p		number,
					ie_irradiado_p		varchar2,
					ie_filtrado_p		varchar2,
					ds_justificativa_p	varchar2,
					nm_usuario_p		varchar2,
					ie_aliquotado_p		varchar2 default null,
					ie_lavado_p			varchar2 default null,
					ie_aferese_p		varchar2 default null,
					ie_pool_p			varchar2 default null) is 

ie_tipo_w			number(10,0);
nr_sequencia_w			number(10,0);
dt_prescricao_w			date;
dt_prev_execucao_w		date;
dt_programada_w			date;
dt_inicio_prescr_w		date;
qt_procedimento_w		number(8,3);
qt_vol_hemocomp_w		number(15,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_intervalo_w			varchar2(7);
ds_derivado_w			varchar2(255);
dt_primeiro_horario_w		date;
nr_horas_validade_w		number(5,0);
ds_horarios_1_w			varchar2(255);
ds_horarios_2_w			varchar2(255);
nr_intervalo_w			number(15,0);
nr_atendimento_w		number(10,0);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
cd_estabelecimento_w		number(4,0);
ie_tipo_convenio_w		number(2,0);
ie_tipo_atendimento_w 		number(3,0);
cd_setor_atendimento_w		number(5,0);
cd_setor_prescricao_w		number(5,0);
nr_seq_proc_interno_w		number(10,0);
ds_erro_w			varchar2(510);
ie_prescr_proc_sem_lib_w	varchar2(30);
ie_via_aplicacao_w		varchar2(5);
ie_irradiado_w			varchar2(1);
ie_filtrado_w			varchar2(1);
ds_erro_ww			varchar(255);
ie_consitir_novo_hemoc_W	varchar2(1);
ie_aliquotado_w			varchar2(1);
ie_lavado_w			varchar2(1);
qt_hora_infusao_w			number(2);
qt_tempo_infusao_w			number(5);
ie_aferese_w			varchar2(1);
ie_pool_w				varchar2(1);
ie_checar_agora_pa_w		varchar2(2 char);
ie_urgencia_presc_w			varchar2(1 char);
ie_marcar_agora_w 			varchar2(2 char);
qt_min_intervalo_w			Number(15);
						
begin

select	ie_irradiado,
	ds_derivado,
	qt_hora_infusao,
	qt_tempo_infusao
into	ie_irradiado_w,
	ds_derivado_w,
	qt_hora_infusao_w,
	qt_tempo_infusao_w
from	san_derivado
where	nr_sequencia = nr_seq_derivado_p;
  
select	dt_inicio_prescr,
	dt_primeiro_horario,
	nr_horas_validade,
	nr_atendimento,
	cd_estabelecimento,
	nvl(cd_setor_atendimento,0)
into 	dt_inicio_prescr_w,
	dt_primeiro_horario_w,
	nr_horas_validade_w,
	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_setor_prescricao_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

Obter_Param_Usuario(924,530,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_prescr_proc_sem_lib_w);   
Obter_Param_Usuario(924,574,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_consitir_novo_hemoc_W);
obter_param_usuario(924,831,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_checar_agora_pa_w);

select 	obter_convenio_atendimento(nr_atendimento_w),
	obter_tipo_convenio(obter_convenio_atendimento(nr_atendimento_w)),
	obter_tipo_atendimento(nr_atendimento_w),
	obter_categoria_atendimento(nr_atendimento_w)	
into	cd_convenio_w,
	ie_tipo_convenio_w,
	ie_tipo_atendimento_w,
	cd_categoria_w
from 	dual;

select	ie_tipo,
	dt_programada
into	ie_tipo_w,
	dt_programada_w
from	prescr_solic_bco_sangue
where	nr_sequencia	=	nr_seq_solic_sangue_p;

if	(ie_tipo_w = 4) then
	dt_prev_execucao_w	:= sysdate;
elsif	(dt_programada_w is null) then
	dt_prev_execucao_w	:= Obter_data_prev_exec(dt_inicio_prescr_w,dt_inicio_prescr_w,0, nr_prescricao_p, 'A');
else
	dt_prev_execucao_w	:= dt_programada_w;
end if;

select	max(cd_intervalo)
into	cd_intervalo_w
from	san_derivado
where	nr_sequencia = nr_seq_derivado_p
and		ie_situacao = 'A';

if (cd_intervalo_w is null) then
	select	max(cd_intervalo)
	into	cd_intervalo_w
	from	intervalo_prescricao
	where	ie_situacao = 'A'
	and		ie_prescricao_dieta = 'B';
end if;

ie_marcar_agora_w	:= Obter_Se_marca_Agora(cd_setor_prescricao_w);

if	((ie_checar_agora_pa_w = 'S')  or 
	 ((ie_checar_agora_pa_w = 'D') and 
	  (ie_marcar_agora_w = 'S'))) and
	(nvl(obter_classif_setor(cd_setor_prescricao_w), 0) = 1) and
	(ie_tipo_w = 4) then
	
	if	(cd_intervalo_w is not null) then
		ie_urgencia_presc_w := obter_se_intervalo_agora(cd_intervalo_w);				
	else				
		ie_urgencia_presc_w := 'S';
	end if;
	
	if	(nvl(ie_urgencia_presc_w,'N') = 'S') then
		if	(cd_intervalo_w is not null) then
			select	coalesce(max(a.qt_min_agora), max(a.qt_min_intervalo), 0)
			into	qt_min_intervalo_w
			from	intervalo_prescricao a
			where	a.cd_intervalo = cd_intervalo_w;
		end if;	
			
		if	(qt_min_intervalo_w > 0) then
			dt_prev_execucao_w	:= sysdate + (qt_min_intervalo_w/1440);
		else
			dt_prev_execucao_w	:= sysdate;
		end if;	
	end if;

end if;

if 	(ie_irradiado_p	= 'S') then
	ie_irradiado_w	:= 'S';
end if;
ie_filtrado_w := 'N';
if	(ie_filtrado_p	= 'S') then
	ie_filtrado_w	:= 'S';
	
end if;
ie_aliquotado_w := 'N';
if	(ie_aliquotado_p	= 'S') then
	ie_aliquotado_w	:= 'S';
end if;	
ie_lavado_w := 'N';
if	(ie_lavado_p	= 'S') then
	ie_lavado_w	:= 'S';
end if;
ie_aferese_w := 'N';
if	(ie_aferese_p	= 'S') then
	ie_aferese_w	:= 'S';
end if;
ie_pool_w := 'N';
if	(ie_pool_p	= 'S') then
	ie_pool_w	:= 'S';
end if;

qt_procedimento_w := 1;
atualizar_volume_hemocomp(nr_seq_derivado_p,qt_vol_hemocomp_w,qt_procedimento_w,'BV');
atualizar_volume_hemocomp(nr_seq_derivado_p,qt_vol_hemocomp_w,qt_procedimento_w,'V');
atualizar_volume_hemocomp(nr_seq_derivado_p,qt_vol_hemocomp_w,qt_procedimento_w,'B');

Calcular_Horario_Prescricao(	nr_prescricao_p,cd_intervalo_w,dt_primeiro_horario_w,dt_prev_execucao_w,nr_horas_validade_w,0,0,0,nr_intervalo_w,
				ds_horarios_1_w,ds_horarios_2_w,'N', null);
				
Obter_Proced_sangue(	0,nr_seq_derivado_p,cd_estabelecimento_w,ie_tipo_atendimento_w,ie_tipo_convenio_w,cd_convenio_w, cd_categoria_w, cd_setor_atendimento_w,
			cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w, 'N','N','N','N', cd_setor_prescricao_w);

select	max(ie_via_aplicacao)
into	ie_via_aplicacao_w
from	san_derivado
where	nr_sequencia = nr_seq_derivado_p
and	obter_se_via_hemoc_setor(ie_via_aplicacao, cd_setor_prescricao_w, nr_sequencia) = 'S';
if 	(nvl(ie_consitir_novo_hemoc_W,'N') <> 'S') then			
	Consiste_solic_hemocomponente(	nr_seq_solic_sangue_p,nr_seq_derivado_p,cd_estabelecimento_w, nm_usuario_p, ds_erro_w);
	if	(ds_erro_w is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(261396, 'ERRO='|| ds_erro_w);
	end if;
end if;
			
select	nvl(max(nr_sequencia), 0) + 1
into	nr_sequencia_w
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p;

if	(cd_procedimento_w is null) then
	--Nao foi vinculado nenhum procedimento ao cadastro do derivado ' ||ds_derivado_w|| '! Verifique o cadastro. #@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(261397, 'DERIVADO='|| ds_derivado_w);
end if;

insert into prescr_procedimento(
	nr_prescricao,
	nr_sequencia,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	qt_procedimento,
	ie_urgencia,
	ie_suspenso,
	dt_prev_execucao,
	ie_status_atend,
	dt_atualizacao,
	nm_usuario,
	ie_origem_inf,
	nr_seq_interno,
	ie_avisar_result,
	cd_motivo_baixa,
	nr_seq_solic_sangue,
	ds_horarios,
	nr_seq_derivado,
	qt_vol_hemocomp,
	cd_intervalo,
	cd_setor_atendimento,
	ie_util_hemocomponente,
	ie_unid_med_hemo,
	ie_status,
	dt_status,
	ie_via_aplicacao,
	ie_irradiado,
	ie_filtrado,
	ie_lavado,
	ie_aliquotado,
	ie_aferese,
	ie_pool,
	ie_acm,
	ie_se_necessario,
	ds_justificativa,
	qt_hora_infusao,
	qt_tempo_infusao)
values	(nr_prescricao_p,
	nr_sequencia_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w,
	qt_procedimento_w, 
	'N', 
	'N',
	dt_prev_execucao_w,
	5, 
	sysdate, 
	nm_usuario_p,
	'1', 
	prescr_procedimento_seq.NextVal,
	'N',
	0,
	nr_seq_solic_sangue_p,
	ds_horarios_1_w || ds_horarios_2_w,
	nr_seq_derivado_p,
	qt_vol_hemocomp_w,
	cd_intervalo_w,
	cd_setor_atendimento_w,
	'C',
	'gpm',
	'P',
	sysdate,
	ie_via_aplicacao_w,
	ie_irradiado_w,
	ie_filtrado_w,
	ie_lavado_w,
	ie_aliquotado_w,
	ie_aferese_w,
	ie_pool_w,
	'N',
	'N',
	ds_justificativa_p,
	qt_hora_infusao_w,
	qt_tempo_infusao_w);
	
Gerar_Solic_testes(nr_seq_derivado_p, nr_prescricao_p, nr_seq_solic_sangue_p, nr_atendimento_w, cd_estabelecimento_w, nm_usuario_p, dt_prev_execucao_w, cd_intervalo_w);
	
if (ie_prescr_proc_sem_lib_w = 'S') then
	Gerar_prescr_proc_sem_dt_lib(nr_prescricao_p,nr_sequencia_w,obter_perfil_ativo,'N',nm_usuario_p);
end if;
if 	(nvl(ie_consitir_novo_hemoc_w,'N') = 'S') then			
	Consistir_Prescr_Procedimento(nr_prescricao_p,nr_sequencia_w,nm_usuario_p,obter_perfil_ativo,ds_erro_ww);
end if;
commit;

end gerar_prescr_proced_hemoc;
/
