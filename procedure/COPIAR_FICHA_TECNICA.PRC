create or replace
procedure copiar_ficha_tecnica(	nr_ficha_origem_p	number,
				nr_ficha_destino_p	number,
				nm_usuario_p	varchar2) is

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
begin

if 	(nr_ficha_origem_p <> nr_ficha_destino_p)then
	delete from ficha_tecnica_componente
	where	nr_ficha_tecnica = nr_ficha_destino_p;


	insert into ficha_tecnica_componente(
		nr_ficha_tecnica,
		nr_seq_componente,
		dt_alteracao,
		cd_estabelecimento,
		nm_usuario,
		dt_atualizacao,
		ie_situacao,
		ie_tipo_componente,
		cd_material,
		cd_procedimento,
		ie_origem_proced,
		cd_centro_controle,
		qt_fixa,
		qt_variavel,
		pr_quebra_variavel,
		nr_seq_exame,
		nr_seq_proc_interno,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	select	nr_ficha_destino_p,
		nr_seq_componente,
		dt_alteracao,
		cd_estabelecimento_w,
		nm_usuario_p,
		sysdate,
		ie_situacao,
		ie_tipo_componente,
		cd_material,
		cd_procedimento,
		ie_origem_proced,
		cd_centro_controle,
		qt_fixa,
		qt_variavel,
		pr_quebra_variavel,
		nr_seq_exame,
		nr_seq_proc_interno,
		sysdate,
		nm_usuario_p
	from	ficha_tecnica_componente
	where	nr_ficha_tecnica = nr_ficha_origem_p
	and	(cd_centro_controle is null or cd_estabelecimento = cd_estabelecimento_w);

end if;

/*Adicionada a restri��o do centro de controle pois 
n�o poder� copiar de outro estabelecimento, s� quando for o estabelecimento logado.*/

commit;
end copiar_ficha_tecnica;
/