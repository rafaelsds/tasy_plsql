create or replace
procedure	man_atualiza_just_grau_satisf(
			nr_seq_justificativa_p	number,
			nr_seq_ordem_p		number) is

begin

update	man_ordem_servico
set	nr_seq_justif_grau_satisf = nr_seq_justificativa_p
where	nr_sequencia = nr_seq_ordem_p;

commit;

end man_atualiza_just_grau_satisf;
/