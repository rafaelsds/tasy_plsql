create or replace
procedure gtt_gerar_inventario ( nr_seq_inventario_p	in out number,
				cd_local_estoque_p	number,
				qt_max_contagem_p 	number,
				ie_consignado_p		varchar2,
				cd_material_p		number,
				ie_contagem_atual_p	varchar2,
				qt_contagem_p		number,
				cd_fornecedor_p 	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ds_erro_p		out varchar2) is


nr_seq_inventario_w		number(10)	:= nr_seq_inventario_p;
cd_local_estoque_w		number(5)  	:= cd_local_estoque_p;
ie_contagem_atual_w		varchar2(15)	:= nvl(ie_contagem_atual_p,'1');
ie_contagem_inventario_w	varchar2(15);
nr_seq_item_inv_w		number(10);
ds_erro_w			varchar2(2000);
qt_existe_item_inv_w		number(10);

begin

if	(nr_seq_inventario_w = 0) then
	begin

	select 	inventario_seq.nextval
	into	nr_seq_inventario_w
	from	dual;

	begin
	insert into inventario
		(nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		cd_local_estoque,
		dt_bloqueio,
		nm_usuario_bloqueio,
		qt_max_contagem,
		ie_novo_item_barra,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		qt_max_definir,
		ie_consignado,
		ie_contagem_atual,
		ie_tipo_contagem,
		dt_inicio)
	values 	(nr_seq_inventario_w,
		cd_estabelecimento_p,
		sysdate,
		nm_usuario_p,
		cd_local_estoque_w,
		sysdate, -- Bloqueio
		nm_usuario_p,
		qt_max_contagem_p,
		'N',
		sysdate,
		nm_usuario_p,
		qt_max_contagem_p,
		ie_consignado_p,
		'1',
		'I',
		sysdate);
	exception when others then
		ds_erro_w := substr(sqlerrm,1,2000);
	end;

	ie_contagem_atual_w := '1';
	end;
end if;

if	(nr_seq_inventario_w > 0) then
	begin

	select	nvl(ie_contagem_atual,'1')
	into	ie_contagem_inventario_w
	from	inventario
	where	nr_sequencia = nr_seq_inventario_w;

	if	(ie_contagem_atual_w <> '1') and
		(ie_contagem_atual_w <> ie_contagem_inventario_w) then
		update	inventario
		set	ie_contagem_atual = ie_contagem_atual_w
		where	nr_sequencia = nr_seq_inventario_w;
	end if;

	end;
end if;

if	(nvl(ds_erro_w,'X') = 'X') then
	begin
	select	count(*)
	into	qt_existe_item_inv_w
	from	inventario_material
	where	nr_seq_inventario = nr_seq_inventario_w
	and	cd_material = cd_material_p;

	if	(qt_existe_item_inv_w = 0) then
		begin
		select	inventario_material_seq.nextval
		into	nr_seq_item_inv_w
		from	dual;

		begin
		insert into inventario_material
			(nr_sequencia,
			nr_seq_inventario,
			dt_atualizacao,
			nm_usuario,
			cd_material,
			qt_contagem,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_fornecedor,
			ie_status_inventario,
			nr_seq_ordem,
			ie_contagem_atual,
			dt_contagem)
		values (nr_seq_item_inv_w,
			nr_seq_inventario_w,
			sysdate,
			nm_usuario_p,
			cd_material_p,
			qt_contagem_p,
			sysdate,
			nm_usuario_p,
			cd_fornecedor_p,
			'P',
			1,
			'1',
			sysdate);
		exception when others then
			ds_erro_w := substr(sqlerrm,1,2000);
		end;

		end;
	elsif	(ie_contagem_atual_w = '2') then
		begin

		begin
		update	inventario_material
		set	qt_recontagem 		= qt_contagem_p,
			nm_usuario_recontagem 	= nm_usuario_p,
			dt_recontagem		= sysdate
		where	nr_seq_inventario	= nr_seq_inventario_w
		and	cd_material		= cd_material_p;
		exception when no_data_found then
			ds_erro_w := substr(sqlerrm,1,2000);
		end;

		end;
	elsif	(ie_contagem_atual_w = '3') then
		begin

		begin
		update	inventario_material
		set	qt_seg_recontagem		= qt_contagem_p,
			nm_usuario_seg_recontagem 	= nm_usuario_p,
			dt_seg_recontagem		= sysdate
		where	nr_seq_inventario		= nr_seq_inventario_w
		and	cd_material			= cd_material_p;
		exception when no_data_found then
			ds_erro_w := substr(sqlerrm,1,2000);
		end;

		end;
	elsif	(ie_contagem_atual_w = '4') then
		begin

		begin
		update	inventario_material
		set	qt_terc_recontagem		= qt_contagem_p,
			nm_usuario_terc_recontagem 	= nm_usuario_p,
			dt_terc_recontagem		= sysdate
		where	nr_seq_inventario		= nr_seq_inventario_w
		and	cd_material			= cd_material_p;
		exception when no_data_found then
			ds_erro_w := substr(sqlerrm,1,2000);
		end;

		end;
	end if;
	end;
end if;

nr_seq_inventario_p	:= nr_seq_inventario_w;
ds_erro_p 		:= ds_erro_w;

commit;

end gtt_gerar_inventario;
/