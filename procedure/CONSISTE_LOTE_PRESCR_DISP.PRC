create or replace
procedure consiste_lote_prescr_disp(	nr_seq_lote_p		ap_lote.nr_sequencia%type,
				nr_prescricao_p		prescr_medica.nr_prescricao%type,
				cd_material_p		material.cd_material%type,
				qt_material_p		number,
				nr_seq_lote_fornec_p	material_lote_fornec.nr_sequencia%type,
				cd_local_estoque_p	local_estoque.cd_local_estoque%type,
				dt_entrada_unidade_p	date,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				cd_setor_atendimento_p	setor_atendimento.cd_setor_atendimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

ie_consignado_w			material.ie_consignado%type;
cd_cgc_fornec_w			pessoa_juridica.cd_cgc%type;
ie_bloqueio_inventario_w		saldo_estoque.ie_bloqueio_inventario%type;
cd_operacao_transf_setor_w		parametro_estoque.cd_operacao_transf_setor%type;
ie_movto_consig_w			varchar2(1) := 'N';
ie_local_valido_w			varchar2(1) := 'N';
cd_tipo_baixa_w			varchar2(10);
ie_atualiza_estoque_w		tipo_baixa_prescricao.ie_atualiza_estoque%type;
ie_estoque_lote_w			material_estab.ie_estoque_lote%type;
ie_material_estoque_w		material_estab.ie_material_estoque%type;
ie_consiste_saldo_w		varchar2(10);
ie_estoque_disp_w			varchar2(1);
ie_baixa_estoque_pac_w		varchar2(1);
ie_saldo_estoque_w		varchar2(1);
ie_consiste_saldo_lote_w		varchar2(1);
qt_saldo_lote_fornec_w		number(13,4);
ds_erro_w			varchar2(255);
ds_material_w			material.ds_material%type;
cd_local_estoque_w		local_estoque.cd_local_estoque%type;

begin

obter_param_usuario(7029, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, cd_tipo_baixa_w);
obter_param_usuario(7029, 58, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_saldo_w);
obter_param_usuario(7029, 59, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_saldo_lote_w);
obter_param_usuario(7030, 79, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, cd_local_estoque_w);

select	nvl(ie_consignado,'0')
into	ie_consignado_w
from	material
where	cd_material = cd_material_p;

select	nvl(max(cd_operacao_transf_setor),0)
into	cd_operacao_transf_setor_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p
and	ie_situacao = 'A';

if	(cd_operacao_transf_setor_w <> '0') then
	ie_movto_consig_w := 'S';
end if;

if	(ie_consignado_w <> '0') then
	begin
	if	(nvl(nr_seq_lote_fornec_p, 0) > 0) then

		select	max(cd_cgc_fornec)
		into	cd_cgc_fornec_w
		from	material_lote_fornec
		where	nr_sequencia = nr_seq_lote_fornec_p;

	else
		cd_cgc_fornec_w	:= obter_fornecedor_regra_consig(
					cd_estabelecimento_p,
					cd_material_p, '1');
	end if;
	end;
end if;

sup_verifica_bloqueio_mat_inv(	cd_estabelecimento_p,
				pkg_date_utils.start_of(dt_entrada_unidade_p, 'MONTH', 0),
				cd_local_estoque_p,
				cd_material_p,
				cd_cgc_fornec_w,
				ie_movto_consig_w,
				ie_bloqueio_inventario_w);

if	(ie_bloqueio_inventario_w = 'S') then
	begin
	select	max(ds_material)
	into	ds_material_w
	from	material
	where	cd_material = cd_material_p;
	
	/*rais-e_application_error(-20011,'Bloqueado p/invent�rio: ' || ds_material_w);*/
	wheb_mensagem_pck.exibir_mensagem_abort(174138,'DS_MATERIAL='||ds_material_w);
	end;
end if;

select	nvl(max(ie_atualiza_estoque),'S')
into	ie_atualiza_estoque_w
from	tipo_baixa_prescricao
where	ie_prescricao_devolucao = 'P'
and	cd_tipo_baixa = cd_tipo_baixa_w
and	ie_situacao = 'A';

select	nvl(max(ie_estoque_lote),'N'),
	nvl(max(ie_material_estoque),'N')
into	ie_estoque_lote_w,
	ie_material_estoque_w
from	material_estab
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

select	nvl(max(cd_local_estoque), cd_local_estoque_w)
into	cd_local_estoque_w
from	setor_local
where	ie_loca_estoque_pac = 'S'
and	cd_setor_atendimento = cd_setor_atendimento_p;

obter_local_valido(cd_estabelecimento_p, cd_local_estoque_w, cd_material_p, '3', ie_local_valido_w);

obter_disp_estoque(cd_material_p, cd_local_estoque_p, cd_estabelecimento_p, 0, qt_material_p, cd_cgc_fornec_w, ie_saldo_estoque_w);

if	(nvl(nr_seq_lote_fornec_p, 0) > 0) and (ie_consiste_saldo_lote_w = 'S') and (ie_material_estoque_w = 'S') and (ie_estoque_lote_w = 'N') then

	qt_saldo_lote_fornec_w	:= obter_saldo_lote_fornec(nr_seq_lote_fornec_p);

	ie_consiste_saldo_lote_w := 'S';
	if	(qt_saldo_lote_fornec_w > 0) and (qt_saldo_lote_fornec_w >= qt_material_p) then
		ie_consiste_saldo_lote_w := 'N';
	end if;
elsif	(nvl(nr_seq_lote_fornec_p, 0) > 0) and (ie_material_estoque_w = 'S') and (ie_estoque_lote_w = 'S') then
	qt_saldo_lote_fornec_w := obter_saldo_lote_fornec_local(nr_seq_lote_fornec_p, cd_local_estoque_p);
	ie_saldo_estoque_w := 'N';
	if	(qt_saldo_lote_fornec_w > 0) and (qt_saldo_lote_fornec_w >= qt_material_p) then
		ie_saldo_estoque_w := 'S';
	end if;
end if;

select	obter_se_baixa_estoque_pac(cd_setor_atendimento_p, cd_material_p,null,0)
into	ie_baixa_estoque_pac_w
from 	dual;

if	((ie_baixa_estoque_pac_w = 'N') or (ie_saldo_estoque_w = 'S') or (nvl(ie_atualiza_estoque_w, 'S') = 'N')) then
	ie_estoque_disp_w := 'S';
else
	ie_estoque_disp_w := 'N';
end if;

if	(nvl(ie_atualiza_estoque_w,'S') = 'S') then
	begin
	if	((ie_local_valido_w <> 'S') and (ie_material_estoque_w = 'S')) then
		ds_erro_w:= Wheb_mensagem_pck.get_texto(306383); -- 'Local n�o � v�lido para a opera��o';
	elsif	((ie_consiste_saldo_w = 'S') and (ie_estoque_disp_w <> 'S') and (ie_material_estoque_w = 'S')) then
		ds_erro_w:= Wheb_mensagem_pck.get_texto(306384); -- 'N�o existe estoque dispon�vel!';
	elsif	(ie_consiste_saldo_lote_w = 'S') and (nvl(nr_seq_lote_fornec_p,0) > 0) and
		(nvl(qt_saldo_lote_fornec_w,0) <= 0) and (ie_material_estoque_w = 'S') and (ie_estoque_lote_w = 'N') then
			    -- 'O saldo deste lote � menor ou igual a zero! '
		ds_erro_w:= Wheb_mensagem_pck.get_texto(306385) || ' ' || chr(13) || chr(10) || Wheb_mensagem_pck.get_texto(306387); -- 'Verifique na Administra��o de estoques, Pasta Lote fornecedor.';
		enviar_comunic_consumo_lote(	nr_seq_lote_fornec_p,
						qt_saldo_lote_fornec_w,
						qt_material_p,
						cd_local_estoque_p,
						nr_seq_lote_p,
						cd_estabelecimento_p,
						nm_usuario_p);
	end if;
	end;
end if;

if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265438,'ds_erro=' || ds_erro_w);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end consiste_lote_prescr_disp;
/