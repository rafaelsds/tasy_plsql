create or replace
procedure gerar_tipo_checkup_pf (	ds_seq_tipo_checkup_p	varchar2,
									nr_seq_pessoa_checkup_p number,
									nm_usuario_p			Varchar2) is 
nr_seq_tipo_checkup_w	number(10);
ds_seq_tipo_checkup_w	varchar2(1000);
tam_lista_w				number(10,0);
ie_pos_virgula_w		number(10,0);
contrato_tipo_checkup_pf_w	number(10);

begin

if  (ds_seq_tipo_checkup_p is not null) and
	(nr_seq_pessoa_checkup_p is not null) then
	begin
	ds_seq_tipo_checkup_w := ds_seq_tipo_checkup_p;
	while ds_seq_tipo_checkup_w is not null loop
		begin
		tam_lista_w := length(ds_seq_tipo_checkup_w);
		ie_pos_virgula_w := instr(ds_seq_tipo_checkup_w, ',');
		if (ie_pos_virgula_w <> 0) then
			nr_seq_tipo_checkup_w := to_number(substr(ds_seq_tipo_checkup_w, 1, (ie_pos_virgula_w - 1)));
			ds_seq_tipo_checkup_w	:= substr(ds_seq_tipo_checkup_w, (ie_pos_virgula_w + 1), tam_lista_w);
		end if;
		
		if (nr_seq_tipo_checkup_w > 0) then
			begin
			select	contrato_tipo_checkup_pf_seq.nextval
			into	contrato_tipo_checkup_pf_w
			from	dual;
			
			insert into contrato_tipo_checkup_pf (
				nr_sequencia,
				nr_seq_tipo_checkup,
				nr_seq_pessoa_checkup,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values (
				contrato_tipo_checkup_pf_w,
				nr_seq_tipo_checkup_w,
				nr_seq_pessoa_checkup_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p);
			end;
		end if;		
		end;
	end loop;	
	end;
end if;

commit;

end GERAR_TIPO_CHECKUP_PF;
/