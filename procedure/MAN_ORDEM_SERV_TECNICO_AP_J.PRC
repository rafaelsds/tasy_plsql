create or replace
procedure man_ordem_serv_tecnico_ap_j(
			NR_SEQ_ORDEM_SERV_P			NUMBER,
			NR_SEQ_ORDEM_SERV_TECNICO_P		NUMBER,
			IE_ACAO_EXEC_P				NUMBER,
			CD_SETOR_ATENDIMENTO_P			NUMBER,
			IE_BASE_P				VARCHAR2,
			NM_USUARIO_P				VARCHAR2,
			IE_AJUSTAR_DELETE_P		OUT	VARCHAR2,
			DS_RET_TESTE_P			OUT	VARCHAR2,
			DS_RET_ATUALIZA_OS_PROCESSO_P	OUT 	VARCHAR2,
			IE_ATUALIZAR_OS_P		OUT	VARCHAR2) IS 
			
ie_status_ordem_w		man_ordem_servico.ie_status_ordem%type;
dt_liberacao_w			man_ordem_serv_tecnico.dt_liberacao%type;
nr_seq_tipo_w			man_ordem_serv_tecnico.nr_seq_tipo%type;
nr_seq_grupo_des_w		man_ordem_servico.nr_seq_grupo_des%type;
ie_resp_teste_reprog_w		man_ordem_servico.ie_resp_teste_reprog%type;
ie_obriga_news_w		man_ordem_servico.ie_obriga_news%type;
ie_exclusiva_w			man_ordem_servico.ie_exclusiva%type;
ie_atu_os_prim_hist_w		man_tipo_hist.ie_atualiza_os_proc_prim_hist%type;
cd_estabelecimento_w 		number(4);
ds_ret_atualiza_os_processo_w	varchar2(255);
ds_ret_teste_w			varchar2(255);
qt_alteracao_w			number(10);
qt_alteracao_n_liberado_w	number(10);
ds_sep_bv_w			varchar2(10);	


begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

select	max(b.ie_status_ordem),
	max(a.dt_liberacao),
	max(a.nr_seq_tipo),
	max(b.nr_seq_grupo_des),
	max(b.ie_resp_teste_reprog),
	max(nvl(b.ie_obriga_news,'S')),
	max(b.ie_exclusiva)
into	ie_status_ordem_w,
	dt_liberacao_w,
	nr_seq_tipo_w,
	nr_seq_grupo_des_w,
	ie_resp_teste_reprog_w,
	ie_obriga_news_w,
	ie_exclusiva_w
from	man_ordem_serv_tecnico a,
	man_ordem_servico b
where	a.nr_sequencia = nr_seq_ordem_serv_tecnico_p
and	b.nr_sequencia = nr_seq_ordem_serv_p;


select	nvl(max(ie_atualiza_os_proc_prim_hist),'N')
into 	ie_atu_os_prim_hist_w
from	man_tipo_hist 
where	nr_sequencia = nr_seq_tipo_w;

if	(dt_liberacao_w is not null)
and	(nr_seq_tipo_w is not null)
and	(ie_status_ordem_w = '1')
and	(ie_atu_os_prim_hist_w = 'S') then
	ie_atualizar_os_p := 'S';
	man_atualiza_os_processo_hist(nr_seq_ordem_serv_tecnico_p, nr_seq_ordem_serv_p, '1', nm_usuario_p, ds_ret_atualiza_os_processo_w);
end if;

/* Removido na OS 1059801
if	(dt_liberacao_w is not null) then
	if	(ie_base_p = 'C')
	and	((nr_seq_grupo_des_w is not null)
	and	(nr_seq_tipo_w = 31)) then
		ds_ret_teste_w := obter_texto_tasy(283720, wheb_usuario_pck.get_nr_seq_idioma);
	end if;
end if;
*/

/*Obter_param_Usuario(297, 40, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_param_w);
	if	(ie_param_w = 'S')
	and	(nr_seq_tipo_w = 1)
	and	(dt_liberacao_w is not null)	then
	end if;*/
	
if	(ie_base_p = 'C' or ie_base_p = 'W')
and	(ie_resp_teste_reprog_w = 'A')	
and	(nr_seq_tipo_w = 31)	then
	ie_atualizar_os_p := 'S';
	man_atualiza_resp_teste_reprog(nr_seq_ordem_serv_p, '', nm_usuario_p);
end if;

ds_sep_bv_w := obter_separador_bv;

Obter_valor_Dinamico_bv('select	count(*) '||
'from	alteracao_tasy@whebl02_orcl '||
'where	nr_seq_ordem_serv = :nr_seq_ordem_serv_p','nr_seq_ordem_serv_p=' || to_char(nr_seq_ordem_serv_p), qt_alteracao_w);

Obter_valor_Dinamico_bv('select	count(*) ' ||
'from	alteracao_tasy@whebl02_orcl ' ||
'where	nr_seq_ordem_serv = :nr_seq_ordem_serv_p ' ||
'and	dt_liberacao is not null','nr_seq_ordem_serv_p=' || to_char(nr_seq_ordem_serv_p), qt_alteracao_n_liberado_w);

if	(ie_acao_exec_p = 3)
and	(((nr_seq_tipo_w in(12,41)) and	(qt_alteracao_w = 0))
or	((nr_seq_tipo_w = 31) and (qt_alteracao_n_liberado_w = 0)))
and	(ie_obriga_news_w = 'S')
and	(ie_exclusiva_w <> 'P')
and	(cd_setor_atendimento_p in(2,7,16))	then
	ie_ajustar_delete_p := 'S';
end if;

ds_ret_teste_p := ds_ret_teste_w;
ds_ret_atualiza_os_processo_p := ds_ret_atualiza_os_processo_w;

commit;

end man_ordem_serv_tecnico_ap_j;
/