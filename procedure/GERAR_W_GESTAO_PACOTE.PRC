create or replace
procedure Gerar_W_Gestao_Pacote
		(cd_estabelecimento_p	number,
		nr_atendimento_p	number,
		dt_inicial_p		date,
		dt_final_p		date,
		nm_usuario_p		varchar2,
		ie_alta_paciente_p	number) is

CD_PROCED_PACOTE_w		number(15,0);
ie_origem_proced_w		number(10,0);
ie_tipo_atendimento_w		number(03,0);
cd_convenio_w			number(10,0);
cd_medico_executor_w		varchar2(10);
cd_setor_atendimento_w		number(10,0);
nr_atendimento_w		number(10,0);
nr_interno_conta_w		number(10,0);
nr_interno_conta_atual_w	number(10,0):= 0;
vl_item_w			number(15,4);
vl_conta_w			number(15,2);
nr_sequencia_w			number(10,0);
cd_especialidade_w		number(10,0);
cd_grupo_procedimento_w		number(15,0);
cd_area_procedimento_w		number(15,0);
cd_especialidade_proced_w	number(15,0);
cd_proced_pacote_atual_w	number(15,0):= 0;
nr_seq_proc_pacote_w		number(10);
vl_original_w			number(15,2);
cd_item_w			number(15,0);
ds_item_w			varchar2(240);
vl_recebido_w			number(15,4):= 0;
ie_proc_mat_w			number(1,0);
qt_item_w			number(15,4);
ie_opcao_data_w			varchar2(1):= 'I';
nr_seq_procedimento_w		number(10,0);
--cd_item_ant_w			number(15,0) :=0;
nr_seq_item_w			number(10,0);

vl_custo_w			conta_paciente_resumo.vl_custo%type;
vl_custo_dir_apoio_w		conta_paciente_resumo.vl_custo_dir_apoio%type;
vl_custo_direto_w		conta_paciente_resumo.vl_custo_direto%type;
vl_custo_hm_w			conta_paciente_resumo.vl_custo_hm%type;
vl_custo_indireto_w		conta_paciente_resumo.vl_custo_indireto%type;
vl_custo_mao_obra_w		conta_paciente_resumo.vl_custo_mao_obra%type;
vl_custo_operacional_w		conta_paciente_resumo.vl_custo_operacional%type;
vl_custo_sadt_w			conta_paciente_resumo.vl_custo_sadt%type;
vl_custo_unitario_w		conta_paciente_resumo.vl_custo_unitario%type;
vl_custo_variavel_w		conta_paciente_resumo.vl_custo_variavel%type;
	

cursor	c10 is	--Contas
	select	a.nr_interno_conta,
		c.nr_seq_procedimento,
		d.cd_proced_pacote,
		d.ie_origem_proced,
		b.ie_tipo_atendimento,
		--obter_dados_propaci('ME',c.nr_seq_procedimento) cd_medico_executor,
		e.cd_medico_executor,
		b.nr_atendimento,
		obter_valor_conta(a.nr_interno_conta,0),
		obter_area_procedimento(d.cd_proced_pacote, d.ie_origem_proced),
		somente_numero(obter_grupo_procedimento(d.cd_proced_pacote, d.ie_origem_proced, 'C')),
		obter_especialidade_proced(d.cd_proced_pacote, d.ie_origem_proced),
		obter_valor_recebido(a.nr_interno_conta)
	from 	pacote			d,
		atendimento_pacote 	c,
		protocolo_convenio	p,
		conta_paciente 		a,
		atendimento_paciente 	b,
		procedimento_paciente	e  -- Adicionado na OS 741219
	where 	a.nr_atendimento = b.nr_atendimento
	and 	b.nr_atendimento = c.nr_atendimento
	and 	c.nr_seq_pacote	 = d.nr_seq_pacote
	and 	p.nr_seq_protocolo = a.nr_seq_protocolo -- Inclu�do a tabela protocolo_convenio OS  238752
	and 	a.nr_seq_protocolo is not null
	and    	b.cd_estabelecimento    = cd_estabelecimento_p
	and 	e.nr_sequencia = c.nr_seq_procedimento   
	and 	(((nr_atendimento_p > 0) and (b.nr_atendimento = nr_atendimento_p)) or (nr_atendimento_p = 0))
	--and     somente_numero(Obter_Dados_ProPaci('IC', c.nr_seq_procedimento)) = a.nr_interno_conta
	and 	e.nr_interno_conta = a.nr_interno_conta
	and	((ie_alta_paciente_p = 0) or  (ie_alta_paciente_p = 1 and b.dt_alta is not null) or (ie_alta_paciente_p = 2 and b.dt_alta is null))
	and     ((ie_opcao_data_w = 'I' and e.dt_procedimento between dt_inicial_p and dt_final_p) or
		 ((ie_opcao_data_w = 'A' and b.dt_entrada between dt_inicial_p and dt_final_p) and
		  (ie_opcao_data_w = 'A' and nvl(b.dt_alta,dt_inicial_p) between dt_inicial_p and dt_final_p)) or
		  (ie_opcao_data_w = 'E' and  nvl(p.dt_entrega_convenio,dt_inicial_p) between dt_inicial_p and dt_final_p) or
		  (ie_opcao_data_w = 'R' and  p.dt_mesano_referencia between dt_inicial_p and dt_final_p))
	order by a.nr_interno_conta, d.cd_proced_pacote;
	
cursor	c20 is --Procedimentos
	select 	cd_convenio,
		cd_setor_atendimento,
		cd_especialidade,
		vl_procedimento,
		nr_seq_proc_pacote,
		nr_sequencia,
		--Obter_custo_conta_resumo_item(nr_interno_conta,cd_procedimento,1, nm_usuario_p),
		--Obter_Custo_Item_Conta(nr_sequencia,1), -- Alterado para a procedure Obter_Custos_Mat_Proc_Conta
		--Obter_preco_proc_mat_orig(nr_sequencia, 1) vl_original,
		Obter_valor_Orig_Proc(nr_sequencia, 0) vl_original,
		cd_procedimento,
		substr(obter_descricao_procedimento(cd_procedimento, ie_origem_proced),1,120),
		1 ie_proc_mat,
		qt_procedimento		
	from 	procedimento_paciente
	where 	nr_interno_conta = nr_interno_conta_w
	and 	nvl(nr_seq_proc_pacote, 0) = nr_seq_procedimento_w
	and 	nvl(nr_seq_proc_pacote,0) <> nr_sequencia --Itens do pacote
	and	cd_motivo_exc_conta is null
	and     (((ie_opcao_data_w = 'I') and (dt_procedimento between dt_inicial_p and dt_final_p)) or (ie_opcao_data_w <> 'I'));
	
cursor	c30 is --Materiais
	select 	cd_convenio,
		cd_setor_atendimento,
		cd_especialidade,
		vl_material,
		nr_seq_proc_pacote,
		nr_sequencia,
		--Obter_custo_conta_resumo_item(nr_interno_conta,cd_material,2, nm_usuario_p),
		--Obter_Custo_Item_Conta(nr_sequencia,2), -- -- Alterado para a procedure Obter_Custos_Mat_Proc_Conta
		--Obter_preco_proc_mat_orig(nr_sequencia, 2) vl_original,
		nvl(vl_tabela_original, vl_material) vl_original,
		cd_material,
		substr(obter_desc_material(cd_material),1,120),
		2 ie_proc_mat,
		qt_material		
	from 	material_atend_paciente
	where 	nr_interno_conta = nr_interno_conta_w
	and 	nvl(nr_seq_proc_pacote, 0) = nr_seq_procedimento_w
	and 	nvl(nr_seq_proc_pacote,0) <> nr_sequencia --Itens do pacote
	and	cd_motivo_exc_conta is null
	and     (((ie_opcao_data_w = 'I') and (dt_atendimento between dt_inicial_p and dt_final_p)) or (ie_opcao_data_w <> 'I'))
	order by cd_material;
	

begin

delete	from W_GESTAO_PACOTE
where nm_usuario = nm_usuario_p;

delete	from W_GESTAO_PACOTE
where	dt_atualizacao < sysdate - 12/24;


ie_opcao_data_w:= nvl(obter_valor_param_usuario(7021, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'I');
if	((ie_opcao_data_w = 'S') or (ie_opcao_data_w = 'N')) then
	ie_opcao_data_w:= 'I';
end if;

open C10;
loop
fetch C10 into	
	nr_interno_conta_w,
	nr_seq_procedimento_w,
	cd_proced_pacote_w,
	ie_origem_proced_w,
	ie_tipo_atendimento_w,
	cd_medico_executor_w,
	nr_atendimento_w,
	vl_conta_w,
	cd_area_procedimento_w,
	cd_grupo_procedimento_w,
	cd_especialidade_proced_w,
	vl_recebido_w;
exit when C10%notfound;
	begin	
	
	-- SOMENTE P / PROCEDIMENTOS
	open C20;
	loop
	fetch C20 into	
		cd_convenio_w,
		cd_setor_atendimento_w,
		cd_especialidade_w,
		vl_item_w,
		nr_seq_proc_pacote_w,
		nr_seq_item_w,
		vl_original_w,
		cd_item_w,
		ds_item_w,
		ie_proc_mat_w,
		qt_item_w;
	exit when C20%notfound;
		begin	

		if 	(nr_interno_conta_atual_w <> nr_interno_conta_w) or
			(cd_proced_pacote_atual_w <> CD_PROCED_PACOTE_w) then
			nr_interno_conta_atual_w:= nr_interno_conta_w;
			cd_proced_pacote_atual_w:= CD_PROCED_PACOTE_w;
		else
			vl_conta_w	:= 0;
			vl_recebido_w	:= 0;
		end if;	
		
		Obter_Custos_Mat_Proc_Conta(nr_seq_item_w, ie_proc_mat_w, vl_custo_w, vl_custo_dir_apoio_w, vl_custo_direto_w, vl_custo_hm_w, vl_custo_indireto_w,
			vl_custo_mao_obra_w, vl_custo_operacional_w, vl_custo_sadt_w, vl_custo_unitario_w, vl_custo_variavel_w);
		
		select	W_GESTAO_PACOTE_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into W_GESTAO_PACOTE
			(NR_SEQUENCIA          ,
			CD_PROCEDIMENTO        ,
			IE_ORIGEM_PROCRED      ,
			DT_ATUALIZACAO         ,
			NM_USUARIO             ,
			DT_ATUALIZACAO_NREC    ,
			NM_USUARIO_NREC        ,
			CD_CONVENIO            ,
			CD_MEDICO_EXECUTOR     ,
			CD_SETOR_ATENDIMENTO   ,
			IE_TIPO_ATENDIMENTO    ,
			VL_PACOTE              ,
			NR_ATENDIMENTO         ,
			NR_INTERNO_CONTA       ,
			cd_especialidade       ,
			vl_conta		,
			cd_area_procedimento	,
			cd_grupo_procedimento	,
			cd_especialidade_proced ,
			nr_seq_proc_pacote	,
			vl_original		,
			cd_item			,
			ds_item			,
			vl_recebido		,
			ie_proc_mat,
			qt_item,
			vl_custo,
			vl_custo_dir_apoio,
			vl_custo_direto,
			vl_custo_hm,
			vl_custo_indireto,
			vl_custo_mao_obra,
			vl_custo_operacional,
			vl_custo_sadt,
			vl_custo_unitario,
			vl_custo_variavel)
		values
			(nr_sequencia_w,
			CD_PROCED_PACOTE_W,
			ie_origem_proced_w,
			SYSDATE,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			CD_CONVENIO_w,
			CD_MEDICO_EXECUTOR_w,
			CD_SETOR_ATENDIMENTO_w,
			IE_TIPO_ATENDIMENTO_w,
			VL_item_w,
			NR_ATENDIMENTO_w,
			NR_INTERNO_CONTA_w,
			cd_especialidade_w,
			vl_conta_w,
			cd_area_procedimento_w,
			cd_grupo_procedimento_w,
			cd_especialidade_proced_w,
			nr_seq_proc_pacote_w,
			vl_original_w,
			cd_item_w,
			ds_item_w,
			vl_recebido_w,
			ie_proc_mat_w,
			qt_item_w,
			vl_custo_w,
			vl_custo_dir_apoio_w,
			vl_custo_direto_w,
			vl_custo_hm_w,
			vl_custo_indireto_w,
			vl_custo_mao_obra_w,
			vl_custo_operacional_w,
			vl_custo_sadt_w,
			vl_custo_unitario_w,
			vl_custo_variavel_w);
		
		end;
	end loop;
	close C20;
	
	--cd_item_ant_w:=0;
	-- SOMENTE P / MATERIAIS
	open C30;
	loop
	fetch C30 into	
		cd_convenio_w,
		cd_setor_atendimento_w,
		cd_especialidade_w,
		vl_item_w,
		nr_seq_proc_pacote_w,
		nr_seq_item_w,
		vl_original_w,
		cd_item_w,
		ds_item_w,
		ie_proc_mat_w,
		qt_item_w;
	exit when C30%notfound;
		begin	

		if 	(nr_interno_conta_atual_w <> nr_interno_conta_w) or
			(cd_proced_pacote_atual_w <> CD_PROCED_PACOTE_w) then
			nr_interno_conta_atual_w:= nr_interno_conta_w;
			cd_proced_pacote_atual_w:= CD_PROCED_PACOTE_w;
		else
			vl_conta_w	:= 0;
			vl_recebido_w	:= 0;
		end if;
		
		/*if	(cd_item_ant_w <> 0) and
			(cd_item_ant_w = cd_item_w) then
			vl_custo_w:= 0;
		end if;	
		cd_item_ant_w:= cd_item_w;*/
		
		Obter_Custos_Mat_Proc_Conta(nr_seq_item_w, ie_proc_mat_w, vl_custo_w, vl_custo_dir_apoio_w, vl_custo_direto_w, vl_custo_hm_w, vl_custo_indireto_w,
			vl_custo_mao_obra_w, vl_custo_operacional_w, vl_custo_sadt_w, vl_custo_unitario_w, vl_custo_variavel_w);		

		select	W_GESTAO_PACOTE_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into W_GESTAO_PACOTE
			(NR_SEQUENCIA          ,
			CD_PROCEDIMENTO        ,
			IE_ORIGEM_PROCRED      ,
			DT_ATUALIZACAO         ,
			NM_USUARIO             ,
			DT_ATUALIZACAO_NREC    ,
			NM_USUARIO_NREC        ,
			CD_CONVENIO            ,
			CD_MEDICO_EXECUTOR     ,
			CD_SETOR_ATENDIMENTO   ,
			IE_TIPO_ATENDIMENTO    ,
			VL_PACOTE              ,
			NR_ATENDIMENTO         ,
			NR_INTERNO_CONTA       ,
			cd_especialidade       ,
			vl_conta		,
			cd_area_procedimento	,
			cd_grupo_procedimento	,
			cd_especialidade_proced ,
			nr_seq_proc_pacote	,
			vl_original		,
			cd_item			,
			ds_item			,
			vl_recebido		,
			ie_proc_mat,
			qt_item,
			vl_custo,
			vl_custo_dir_apoio,
			vl_custo_direto,
			vl_custo_hm,
			vl_custo_indireto,
			vl_custo_mao_obra,
			vl_custo_operacional,
			vl_custo_sadt,
			vl_custo_unitario,
			vl_custo_variavel)
		values
			(nr_sequencia_w,
			CD_PROCED_PACOTE_W,
			ie_origem_proced_w,
			SYSDATE,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			CD_CONVENIO_w,
			CD_MEDICO_EXECUTOR_w,
			CD_SETOR_ATENDIMENTO_w,
			IE_TIPO_ATENDIMENTO_w,
			VL_item_w,
			NR_ATENDIMENTO_w,
			NR_INTERNO_CONTA_w,
			cd_especialidade_w,
			vl_conta_w,
			cd_area_procedimento_w,
			cd_grupo_procedimento_w,
			cd_especialidade_proced_w,
			nr_seq_proc_pacote_w,
			vl_original_w,
			cd_item_w,
			ds_item_w,
			vl_recebido_w,
			ie_proc_mat_w,
			qt_item_w,
			vl_custo_w,
			vl_custo_dir_apoio_w,
			vl_custo_direto_w,
			vl_custo_hm_w,
			vl_custo_indireto_w,
			vl_custo_mao_obra_w,
			vl_custo_operacional_w,
			vl_custo_sadt_w,
			vl_custo_unitario_w,
			vl_custo_variavel_w);
		
		end;
	end loop;
	close C30;
	
	end;
end loop;
close C10;

	
commit;

end Gerar_W_Gestao_Pacote;
/