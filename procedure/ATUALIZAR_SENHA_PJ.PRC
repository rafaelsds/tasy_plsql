create or replace
procedure atualizar_senha_pj (
		cd_cnpj_p	varchar2,
		ds_senha_p	varchar2,
		nm_usuario_p	varchar2) is
		
begin
if	(cd_cnpj_p is not null) and
	(ds_senha_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	pessoa_juridica
	set	ds_senha = ds_senha_p,
		nm_usuario = nm_usuario_p
	where	cd_cgc = cd_cnpj_p;
	end;
end if;
commit;
end atualizar_senha_pj;
/