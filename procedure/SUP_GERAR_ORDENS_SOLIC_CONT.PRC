create or replace
procedure	sup_gerar_ordens_solic_cont(
			nr_solic_compra_p			number,
			cd_comprador_p			varchar2,
			dt_ordem_p			date,
			dt_inclusao_p			date,
			dt_entrega_p			date,
			cd_estabelecimento_p		number,
			cd_setor_atendimento_p		number,
			ie_frete_p				varchar2,
			cd_moeda_p			number,
			ie_aviso_chegada_p		varchar2,
			ie_emite_obs_p			varchar2,
			ie_urgente_p			varchar2,
			nm_usuario_p			varchar2,
			nr_seq_tipo_compra_p		number,
			nr_seq_mod_compra_p		number,
			ie_entrega_solic_p			varchar2,
			vl_frete_p				number default 0, 
			ds_lista_oc_p		out	varchar2) is

cd_cgc_contratado_w		varchar2(14);
cd_pessoa_contratada_w		varchar2(10);
cd_condicao_pagamento_w		number(10,0);
cd_unidade_medida_compra_w	varchar2(30);
cd_material_w			number(06,0);
cd_conta_contabil_w		varchar2(20);
cd_centro_custo_w			number(08,0);
cd_centro_custo_ww			number(08,0);
nr_seq_crit_rateio_w		number(10,0);
nr_seq_conta_financ_w		number(10,0);
qt_material_w			number(13,4);
vl_pagto_w			number(17,4);
nr_ordem_compra_w		number(10,0);
nr_ordem_compra_ant_w		number(10,0);
nr_item_oci_w			number(05,0)	:= 0;
ie_tipo_conta_w			number(5,0)	:= 2;
nr_seq_proj_rec_w			number(10);
cd_pessoa_solicitante_w		varchar2(10);
cd_local_estoque_w		number(4);
nr_item_solic_compra_w		number(5);
nr_seq_regra_contrato_w		number(10);
qt_existe_regra_disp_mer_w		number(10);
ie_acao_disp_mercado_w		varchar2(1) 	:= 'N';
qt_itens_gerados_w			number(10)	:= 0;
nr_seq_contrato_w			number(10);
nr_seq_contrato_ant_w		number(10);
ds_observacao_orig_w		varchar2(255);
ds_observacao_w			varchar2(255);
ds_lista_oc_w			varchar2(4000);
dt_entrega_solicitada_w		date;
ie_estoque_w			varchar2(1);
cd_estabelecimento_w		number(10);
cd_material_contrato_w		number(10);
dt_entrega_w			date;
qt_entrega_solicitada_w		number(13,4);
qt_dias_entrega_w			number(5);
linha_w				number(5);
nr_item_solic_compra_entr_w		number(5);
qt_dias_ajustado_w			number(5);
dt_prog_entrega_w			date;
dt_entrega_atual_w			date;
dt_entrega_ref_w			date;
cd_moeda_w			number(3);
cd_comprador_w			varchar2(10);
dt_aprovacao_w			date;
ie_aprova_auto_w			varchar2(1);
cd_perfil_w			number(10);
vl_total_item_w			ordem_compra_item.vl_total_item%type;
ie_param_147_w			varchar2(15);
ie_param_302_w			varchar2(15);
ds_observacao_solic_w			solic_compra.ds_observacao%type;
ds_observacao_item_w		solic_compra_item.ds_observacao%type;
cd_paciente_soli_compra_w	solic_compra_item.cd_paciente_soli_compra%type;
ie_somente_pagto_w		ordem_compra.ie_somente_pagto%type;
ie_aplica_tributos_oc_w		varchar2(15);
cd_tributo_w			contrato_regra_pagto_trib.cd_tributo%type;
vl_tributo_w			contrato_regra_pagto_trib.vl_tributo%type;
pr_tributo_w			contrato_regra_pagto_trib.pr_tributo%type;
ie_corpo_item_w			contrato_regra_pagto_trib.ie_corpo_item%type;
nr_item_w			number(10);
--ie_centro_custo_oc_contr_w	parametro_compras.ie_centro_custo_oc_contr%type;
ie_centro_custo_oc_contr_w	varchar2(15) := 'SC';
vl_desconto_w			contrato_regra_nf.vl_desconto%type;
pr_desconto_w			contrato_regra_nf.pr_desconto%type;
vl_frete_w				ordem_compra.vl_frete%type;	
qt_material_fixa_w		contrato_regra_nf.qt_material_fixa%type;

cursor	c01 is
select	a.nr_sequencia,
	b.nr_item_solic_compra,
	a.vl_pagto,
	a.cd_material,
	qt_dias_entrega,
	a.vl_desconto,
	a.pr_desconto,
	a.qt_material_fixa
from   	(select	b.nr_sequencia,
		a.cd_material,
		a.vl_pagto,
		a.qt_dias_entrega,
		a.vl_desconto,
		a.pr_desconto,
		a.qt_material_fixa
	from	contrato_regra_nf a,
		contrato b
	where	a.nr_seq_contrato = b.nr_sequencia
	and	((ie_param_302_w = 'S') or (ie_param_302_w = 'N' and ((b.cd_estabelecimento = cd_estabelecimento_p) or 
		exists (select	1
			from	contrato_estab_adic c
			where	b.nr_sequencia = c.nr_seq_contrato
			and	c.cd_estab_adic = cd_estabelecimento_p))))
	and ((ie_param_302_w = 'S') or (ie_param_302_w = 'N' and ((a.cd_estab_regra is null) or (a.cd_estab_regra = cd_estabelecimento_p))))
	and	(b.cd_pessoa_contratada is not null or substr(obter_dados_pf_pj(null,b.cd_cgc_contratado,'S'),1,1) = 'A')
	and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
	and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
	and	cd_pessoa_contratada is null
	and	nvl(b.ie_situacao,'A') = 'A'
	and	((ie_param_147_w = 'T') or (b.ie_classificacao = ie_param_147_w))
	union
	select	b.nr_sequencia,
		a.cd_material,
		a.vl_pagto,
		a.qt_dias_entrega,
		a.vl_desconto,
		a.pr_desconto,
		a.qt_material_fixa
	from	contrato_regra_nf a,
		contrato b
	where	a.nr_seq_contrato = b.nr_sequencia
	and	((ie_param_302_w = 'S') or (ie_param_302_w = 'N' and ((b.cd_estabelecimento = cd_estabelecimento_p) or 
		exists (select	1
			from	contrato_estab_adic c
			where	b.nr_sequencia = c.nr_seq_contrato
			and	c.cd_estab_adic = cd_estabelecimento_p))))
	and ((ie_param_302_w = 'S') or (ie_param_302_w = 'N' and ((a.cd_estab_regra is null) or (a.cd_estab_regra = cd_estabelecimento_p))))
	and	(b.cd_pessoa_contratada is not null or substr(obter_dados_pf_pj(null,b.cd_cgc_contratado,'S'),1,1) = 'A')
	and	((a.dt_inicio_vigencia is null) or (trunc(a.dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
	and	((a.dt_fim_vigencia is null) or (trunc(a.dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
	and	cd_pessoa_contratada is not null
	and	((ie_param_147_w = 'T') or (b.ie_classificacao = ie_param_147_w))
	and	nvl(b.ie_situacao,'A') = 'A') a,
	solic_compra_item b
where	(((ie_estoque_w = 'N') and (a.cd_material = b.cd_material)) or
	((ie_estoque_w = 'S') and (obter_dados_material(a.cd_material,'EST') = obter_dados_material(b.cd_material,'EST'))))
and	b.nr_solic_compra = nr_solic_compra_p
and	((b.nr_contrato = a.nr_sequencia) or (b.nr_contrato is null))
and	not exists	(select	1
			from	ordem_compra_item x
			where	x.nr_solic_compra = b.nr_solic_compra
			and	x.nr_item_solic_compra = b.nr_item_solic_compra)
order by
	b.nr_item_solic_compra,
	a.vl_pagto,	  	
	a.nr_sequencia; /*Jamais mecher nesse order by ... busca pelo mais barato*/

cursor c02 is
select	dt_entrega_solicitada,
	qt_entrega_solicitada
from	solic_compra_item_entrega
where	nr_solic_compra = nr_solic_compra_p
AND	nr_item_solic_compra = nr_item_solic_compra_w
and	ie_entrega_solic_p = 'S';

cursor c03 is
select	rownum linha,
	nr_item_solic_compra_entr,
	dt_entrega
from (	select	nr_item_solic_compra_entr,
		trunc(dt_entrega_solicitada,'dd') dt_entrega
	from	solic_compra_item_entrega
	where	nr_solic_compra = nr_solic_compra_p
	and	nr_item_solic_compra = nr_item_solic_compra_w
	order by	dt_entrega_solicitada);

cursor c04 is
select	cd_tributo,
	vl_tributo,
	pr_tributo,
	ie_corpo_item
from	contrato_regra_pagto_trib
where	nr_seq_regra_nf = nr_seq_regra_contrato_w;
	
begin
if (vl_frete_p = null) or (ie_frete_p <> 'C') then
	vl_frete_w := 0;
else
	vl_frete_w := vl_frete_p;
end if;

cd_comprador_w := cd_comprador_p;

if	(cd_comprador_w is null) then
	cd_comprador_w := substr(obter_dados_parametro_compras(1, 16),1,10);
end if;

select	a.cd_estabelecimento,
	a.cd_centro_custo,
	a.cd_local_estoque,
	a.cd_pessoa_solicitante,
	0,
	ds_observacao
into	cd_estabelecimento_w,
	cd_centro_custo_w,
	cd_local_estoque_w,
	cd_pessoa_solicitante_w,
	nr_seq_contrato_ant_w,
	ds_observacao_solic_w
from	solic_compra a
where	a.nr_solic_compra = nr_solic_compra_p;

cd_perfil_w	:= obter_perfil_ativo;

select	(max(obter_valor_param_usuario(913, 147, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w))),
	(max(obter_valor_param_usuario(913, 185, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w))),
	(max(obter_valor_param_usuario(913, 222, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w))),
	(max(obter_valor_param_usuario(913, 289, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w))),
	(max(obter_valor_param_usuario(913, 302, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w)))
into	ie_param_147_w,
	ie_estoque_w,
	ie_aprova_auto_w,
	ie_aplica_tributos_oc_w,
	ie_param_302_w
from	dual;

select	count(*)
into	qt_existe_regra_disp_mer_w
from	consiste_disp_mercado
where	cd_evento = 'OS';

nr_item_w	:= 0;

ds_lista_oc_w	:= ', ';

open c01;
loop
fetch c01 into
	nr_seq_contrato_w,
	nr_item_solic_compra_w,
	vl_pagto_w,
	cd_material_contrato_w,
	qt_dias_entrega_w,
	vl_desconto_w,
	pr_desconto_w,
	qt_material_fixa_w;
exit when c01%notfound;
	begin
	
	if	(nr_item_w <> nr_item_solic_compra_w) then
		begin
		if	(qt_dias_entrega_w > 0) then
		
			dt_entrega_atual_w	:= trunc(sysdate,'dd') + qt_dias_entrega_w;
			
			update	solic_compra_item
			set	dt_solic_item		= dt_entrega_atual_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_solic_compra 	= nr_solic_compra_p
			and	nr_item_solic_compra	= nr_item_solic_compra_w;
			
			open C03;
			loop
			fetch C03 into	
				linha_w,
				nr_item_solic_compra_entr_w,
				dt_prog_entrega_w;
			exit when C03%notfound;
				begin
				
				if	(linha_w = 1) then
					update	solic_compra_item_entrega
					set	dt_entrega_solicitada		= dt_entrega_atual_w,
						nm_usuario			= nm_usuario_p,
						dt_atualizacao			= sysdate
					where	nr_solic_compra			= nr_solic_compra_p
					and	nr_item_solic_compra		= nr_item_solic_compra_w
					and	nr_item_solic_compra_entr	= nr_item_solic_compra_entr_w;
					
					dt_entrega_ref_w	:= dt_prog_entrega_w;
				
				else
					qt_dias_ajustado_w		:= obter_dias_entre_datas(dt_entrega_ref_w, dt_prog_entrega_w);
					dt_entrega_atual_w		:= trunc((dt_entrega_atual_w + (qt_dias_ajustado_w)),'dd');
					dt_entrega_ref_w		:= dt_prog_entrega_w;
					
					update	solic_compra_item_entrega
					set	dt_entrega_solicitada		= dt_entrega_atual_w,
						nm_usuario			= nm_usuario_p,
						dt_atualizacao			= sysdate
					where	nr_solic_compra			= nr_solic_compra_p
					and	nr_item_solic_compra		= nr_item_solic_compra_w
					and	nr_item_solic_compra_entr	= nr_item_solic_compra_entr_w;
				end if;
				end;
			end loop;
			close C03;			
		end if;
		
		select	cd_cgc_contratado,
			cd_pessoa_contratada,
			cd_condicao_pagamento,
			ie_somente_oc_pagto
		into	cd_cgc_contratado_w,
			cd_pessoa_contratada_w,
			cd_condicao_pagamento_w,
			ie_somente_pagto_w
		from	contrato
		where	nr_sequencia = nr_seq_contrato_w;
		
		select	nvl(max(a.nr_ordem_compra),0)
		into	nr_ordem_compra_w
		from	ordem_compra a,
			ordem_compra_item b
		where	a.nr_ordem_compra = b.nr_ordem_compra
		and	a.cd_estabelecimento = cd_estabelecimento_w
		and	((cd_pessoa_fisica = cd_pessoa_contratada_w) or (cd_cgc_fornecedor = cd_cgc_contratado_w))
		and	b.nr_solic_compra = nr_solic_compra_p
		and	a.dt_liberacao is null;
		
		if	(ie_entrega_solic_p = 'S') then
			dt_entrega_w	:= sysdate;
		else
			dt_entrega_w	:= dt_entrega_p;
		end if;
		
		if	(nr_ordem_compra_w = 0) then		
			begin

			if	(nvl(nr_ordem_compra_ant_w,0) > 0) then /* Atualizar dados da OC anterior */
				begin

				if	(nvl(qt_itens_gerados_w,0) = 0) then
					begin

					delete 	from ordem_compra
					where	nr_ordem_compra = nr_ordem_compra_ant_w;

					end;
				else
					begin

					Gerar_Ordem_Compra_Venc(nr_ordem_compra_ant_w, nm_usuario_p);
					
					/* Verifica se OC ja existe na lista*/
					/* Verifica virgulas no texto para garantir onde o numero de OC comeca e termina no texto */
					if (instr(ds_lista_oc_w,(', ') || nr_ordem_compra_ant_w || ', ') = 0) then
						ds_lista_oc_w	:= ds_lista_oc_w || nr_ordem_compra_ant_w || ', ';
					end if;

					if	(ie_aprova_auto_w = 'S') then
						begin
						select	dt_aprovacao
						into	dt_aprovacao_w
						from	ordem_compra
						where	nr_ordem_compra = nr_ordem_compra_ant_w;
						
						if	(dt_aprovacao_w is null) then
							Calcular_Liquido_Ordem_Compra(nr_ordem_compra_ant_w, nm_usuario_p);
							Gerar_Aprov_Ordem_Compra(nr_ordem_compra_ant_w, null, 'S', nm_usuario_p);
						end if;
						end;
					end if;					
					end;
				end if;
				end;
			end if;

			if	(cd_condicao_pagamento_w is null) then
				/*(-20011,'Nao existe condicao de pagamento informada no contrato seq. ' || nr_seq_contrato_w || '.');*/
				wheb_mensagem_pck.exibir_mensagem_abort(196114,'NR_CONTRATO=' || nr_seq_contrato_w);
			end if;

			if	(cd_cgc_contratado_w is null) and 
				(cd_pessoa_contratada_w is null) then
				/*(-20011,'Nao existe pessoa fisica ou juridica informada no contrato seq. ' || nr_seq_contrato_w || ' para a geracao da ordem de compras.');*/
				wheb_mensagem_pck.exibir_mensagem_abort(196115,'NR_CONTRATO=' || nr_seq_contrato_w);
			end if;			

			cd_moeda_w := cd_moeda_p;
			if	(cd_moeda_p is null) then
				select	max(cd_moeda)
				into	cd_moeda_w
				from	contrato_regra_pagto
				where	nr_seq_contrato = nr_seq_contrato_w;
				
				if	(cd_moeda_w is null) then
					cd_moeda_w := obter_dados_parametro_compras(cd_estabelecimento_p,1);
				end if;
			end if;
			
			select	ordem_compra_seq.nextval
			into	nr_ordem_compra_w
			from	dual;

			insert into ordem_compra (
				nr_ordem_compra,
				cd_estabelecimento,
				cd_cgc_fornecedor,
				cd_condicao_pagamento,
				cd_comprador,
				dt_ordem_compra,
				dt_atualizacao,
				nm_usuario,
				cd_moeda,
				ie_situacao,
				dt_inclusao,
				cd_pessoa_solicitante,
				ie_frete,
				vl_frete,
				pr_desconto,
				pr_desc_pgto_antec,
				pr_juros_negociado,
				dt_entrega,
				ie_aviso_chegada,
				cd_pessoa_fisica,
				ie_emite_obs,
				ie_urgente,
				cd_setor_atendimento,
				pr_desc_financeiro,
				vl_desconto,
				ie_somente_pagto,
				ie_tipo_ordem,
				cd_local_entrega,
				nr_seq_tipo_compra,
				nr_seq_mod_compra,
				ds_observacao)
			values(	nr_ordem_compra_w,
				cd_estabelecimento_p,
				cd_cgc_contratado_w,
				cd_condicao_pagamento_w,
				cd_comprador_w,
				nvl(dt_ordem_p,sysdate),
				sysdate,
				nm_usuario_p,
				cd_moeda_w,
				'A',
				nvl(dt_inclusao_p,sysdate),
				cd_pessoa_solicitante_w,
				nvl(ie_frete_p,sysdate),
				vl_frete_w,
				0,
				0,
				0,
				dt_entrega_w,
				ie_aviso_chegada_p,
				cd_pessoa_contratada_w,
				ie_emite_obs_p,
				ie_urgente_p,
				cd_setor_atendimento_p,
				0,
				0,
				nvl(ie_somente_pagto_w,'N'),
				'C',
				cd_local_estoque_w,
				nr_seq_tipo_compra_p,
				nr_seq_mod_compra_p,
				substr(ds_observacao_solic_w,1,4000));

			nr_item_oci_w		:= 0;
			qt_itens_gerados_w	:= 0;
			
			nr_ordem_compra_ant_w	:= nr_ordem_compra_w;

			end;
		else
			begin
			/* Foi encontrada uma OC valida para esse fornecedor */
			
			if	(nvl(nr_ordem_compra_ant_w,0) > 0) then /* Atualizar dados da OC anterior */
				begin

				if	(nvl(qt_itens_gerados_w,0) = 0) then
					begin

					delete 	from ordem_compra
					where	nr_ordem_compra = nr_ordem_compra_ant_w;

					end;
				else
					begin

					Gerar_Ordem_Compra_Venc(nr_ordem_compra_ant_w, nm_usuario_p);
					
					/* Verifica se OC ja existe na lista*/
					/* Verifica virgulas no texto para garantir onde o numero de OC comeca e termina no texto */
					if (instr(ds_lista_oc_w,(', ') || nr_ordem_compra_ant_w || ', ') = 0) then
						ds_lista_oc_w	:= ds_lista_oc_w || nr_ordem_compra_ant_w || ', ';
					end if;

					if	(ie_aprova_auto_w = 'S') then
						begin
						select	dt_aprovacao
						into	dt_aprovacao_w
						from	ordem_compra
						where	nr_ordem_compra = nr_ordem_compra_ant_w;
						
						if	(dt_aprovacao_w is null) then
							Calcular_Liquido_Ordem_Compra(nr_ordem_compra_ant_w, nm_usuario_p);
							Gerar_Aprov_Ordem_Compra(nr_ordem_compra_ant_w, null, 'S', nm_usuario_p);
						end if;
						end;
					end if;					
					end;
				end if;
				end;
			end if;
			
			nr_ordem_compra_ant_w	:= nr_ordem_compra_w;
			qt_itens_gerados_w	:= 1; /* Encontrou uma OC ja criada, entao ao menos 1 item existe nela vinculado a SC */
			end;
		end if;

		if	(nr_ordem_compra_w > 0) then
			begin

			select	decode(ie_estoque_w,'S',nvl(cd_material_contrato_w,a.cd_material),a.cd_material),
				a.qt_material,
				--a.vl_unit_previsto,
				a.cd_unidade_medida_compra,
				a.nr_item_solic_compra,
				a.nr_seq_proj_rec,
				a.nr_seq_regra_contrato,
				a.ds_observacao,
				a.cd_paciente_soli_compra
			into	cd_material_w,
				qt_material_w,
				--vl_pagto_w,
				cd_unidade_medida_compra_w,
				nr_item_solic_compra_w,
				nr_seq_proj_rec_w,
				nr_seq_regra_contrato_w,
				ds_observacao_item_w,
				cd_paciente_soli_compra_w
			from	solic_compra_item a
			where	a.nr_solic_compra 	= nr_solic_compra_p
			and	a.nr_item_solic_compra = nr_item_solic_compra_w;

			if	(qt_existe_regra_disp_mer_w > 0) then
				begin
				/* Verifica se possui evento na 'Regra para consistencia do item disp. mercado', e se o material esta disponivel no mercado */
				select  substr(obter_acao_regra_disp_mercado('OS', cd_material_w, null, cd_estabelecimento_p),1,1) ie_acao_regra
				into	ie_acao_disp_mercado_w
				from    dual;

				if	(ie_acao_disp_mercado_w <> 'N') then
					begin
					grava_consiste_disp_mercado( 917,
							     cd_material_w,
							     nm_usuario_p,
							     ie_acao_disp_mercado_w);
					end;
				end if;

				end;
			end if;

			if	(ie_acao_disp_mercado_w in ('M', 'N')) then
				begin
			
				select	nvl(max(nr_item_oci),0) + 1
				into	nr_item_oci_w
				from	ordem_compra_item
				where	nr_ordem_compra = nr_ordem_compra_w;
				
				if	(nr_seq_contrato_w > 0) and
					(nvl(nr_seq_regra_contrato_w,0) = 0) then
				
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_regra_contrato_w
					from	contrato_regra_nf
					where	nr_seq_contrato = nr_seq_contrato_w
					and	cd_material = cd_material_w;		
				end if;
			
				if	(nr_seq_regra_contrato_w > 0) then
			
					/*select 	max(nvl(ie_centro_custo_oc_contr,'SC'))
					into 	ie_centro_custo_oc_contr_w		
					from 	parametro_compras 
					where 	cd_estabelecimento = cd_estabelecimento_p;		*/
					if	(ie_centro_custo_oc_contr_w = 'CO') then 
						select	cd_centro_custo
						into	cd_centro_custo_w
						from	contrato_regra_nf
						where	nr_sequencia = nr_seq_regra_contrato_w;
					end if;
			
					select	cd_conta_contabil
					into	cd_conta_contabil_w
					from	contrato_regra_nf
					where	nr_sequencia = nr_seq_regra_contrato_w;
		
				end if;
		
				cd_centro_custo_ww := cd_centro_custo_w;
		
				if	(cd_conta_contabil_w is null) then

					if	(cd_centro_custo_w is null) then
						ie_tipo_conta_w	:= 2;
					else
						ie_tipo_conta_w	:= 3;
					end if;

					define_conta_material(	cd_estabelecimento_p,
								cd_material_w,
								ie_tipo_conta_w,
								null, null, null, null, null, null, null,
								cd_local_estoque_w,
								Null, dt_ordem_p,
								cd_conta_contabil_w,
								cd_centro_custo_w,null);
				end if;	
				
				vl_total_item_w	:= qt_material_w * vl_pagto_w;

				insert into ordem_compra_item(
					nr_ordem_compra,
					nr_item_oci,
					cd_material,
					cd_unidade_medida_compra,
					vl_unitario_material,
					qt_material,
					qt_original,
					dt_atualizacao,
					nm_usuario,
					ie_situacao,
					cd_pessoa_solicitante,
					pr_descontos,
					cd_local_estoque,
					vl_item_liquido,
					cd_centro_custo,
					cd_conta_contabil,
					pr_desc_financ,
					vl_desconto,
					ie_geracao_solic,
					nr_contrato,
					nr_seq_proj_rec,
					nr_solic_compra,
					nr_item_solic_compra,
					vl_total_item,
					ds_observacao,
					nr_seq_regra_contrato,
					cd_paciente_ordem_compra)
				values(	nr_ordem_compra_w,
					nr_item_oci_w,
					cd_material_w,
					cd_unidade_medida_compra_w,
					vl_pagto_w,
					qt_material_w,	
					qt_material_w,
					sysdate,
					nm_usuario_p,
					'A',
					cd_pessoa_solicitante_w,
					pr_desconto_w,
					cd_local_estoque_w,
					vl_pagto_w,
					cd_centro_custo_ww,
					cd_conta_contabil_w,
					0,
					vl_desconto_w,
					'U',
					nr_seq_contrato_w,
					nr_seq_proj_rec_w,
					nr_solic_compra_p,
					nr_item_solic_compra_w,
					vl_total_item_w,
					substr(ds_observacao_item_w,1,255),
					nr_seq_regra_contrato_w,
					cd_paciente_soli_compra_w);
					
				if	(ie_aplica_tributos_oc_w = 'S') and
					(nr_seq_regra_contrato_w > 0) then
			
					open C04;
					loop
					fetch C04 into	
						cd_tributo_w,
						vl_tributo_w,
						pr_tributo_w,
						ie_corpo_item_w;
					exit when C04%notfound;
						begin

						if(nvl(pr_tributo_w,0) = 0) then
							pr_tributo_w :=  dividir(vl_tributo_w , (nvl(qt_material_fixa_w,1) * vl_pagto_w)) * 100; -- gerar percentual
						end if;
						vl_tributo_w :=  vl_total_item_w/100*pr_tributo_w;

						insert into ordem_compra_item_trib(
							nr_ordem_compra,
							nr_item_oci,
							cd_tributo,
							pr_tributo,
							vl_tributo,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ie_corpo_item)
						values(	nr_ordem_compra_w,
							nr_item_oci_w,
							cd_tributo_w,
							nvl(pr_tributo_w,dividir((vl_tributo_w * 100),(vl_total_item_w))),
							nvl(vl_tributo_w,0),
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							ie_corpo_item_w);
						end;
					end loop;
					close C04;
				end if;
					
					
				if 	(ie_entrega_solic_p = 'S')	 then
					begin
					open C02;
					loop
					fetch C02 into	
						dt_entrega_solicitada_w,
						qt_entrega_solicitada_w;
					exit when C02%notfound;
						begin
						
						
						insert into ordem_compra_item_entrega(
							nr_ordem_compra,
							nr_item_oci,
							dt_prevista_entrega,
							qt_prevista_entrega,
							dt_atualizacao,
							nm_usuario,
							nr_sequencia)
						values(	nr_ordem_compra_w,
							nr_item_oci_w,
							dt_entrega_solicitada_w,
							qt_entrega_solicitada_w,
							sysdate,
							nm_usuario_p,
							ordem_compra_item_entrega_seq.nextval);
						end;
					end loop;
					close C02;
					end;
					
				else
					
					begin
						insert into ordem_compra_item_entrega(
							nr_ordem_compra,
							nr_item_oci,
							dt_prevista_entrega,
							qt_prevista_entrega,
							dt_atualizacao,
							nm_usuario,
							nr_sequencia)
						values(	nr_ordem_compra_w,
							nr_item_oci_w,
							dt_entrega_w,
							qt_material_w,
							sysdate,
							nm_usuario_p,
							ordem_compra_item_entrega_seq.nextval);
					end;
				end if;				
				
				select	ds_observacao
				into	ds_observacao_orig_w
				from	solic_compra_item
				where	nr_solic_compra = nr_solic_compra_p
				and	nr_item_solic_compra = nr_item_solic_compra_w;

				ds_observacao_w	:= Wheb_mensagem_pck.get_Texto(307062, 'NR_ORDEM_COMPRA_W='|| NR_ORDEM_COMPRA_W); /*'Gerada ordem de compra ' || nr_ordem_compra_w || ' a partir do item do contrato.';*/

				if	(ds_observacao_orig_w is not null) then
					ds_observacao_w	:=	substr(ds_observacao_orig_w || chr(10) || chr(13) || ds_observacao_w,1,255);
				end if;

				update	solic_compra_item
				set	ds_observacao = ds_observacao_w
				where	nr_solic_compra = nr_solic_compra_p
				and	nr_item_solic_compra = nr_item_solic_compra_w;
		
				qt_itens_gerados_w := nvl(qt_itens_gerados_w,0) + 1;		
				end;
			end if;
			end;
			
			if	(ie_entrega_solic_p = 'S') then
				begin
			
				select	min(dt_prevista_entrega)
				into	dt_entrega_w
				from	ordem_compra_item_entrega
				where	nr_ordem_compra = nr_ordem_compra_w;
			
				update	ordem_compra
				set	dt_entrega = dt_entrega_w
				where	nr_ordem_compra = nr_ordem_compra_w;
			
				end;
			end if;
			
		end if;				
		
		nr_item_w := nr_item_solic_compra_w;
		nr_seq_contrato_ant_w	:= nr_seq_contrato_w;
		end;
	end if;		
	end;
end loop;
close c01;

if	(nvl(qt_itens_gerados_w,0) = 0) then
	begin

	delete 	from ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_ant_w;

	end;
else
	begin

	if	(nr_ordem_compra_ant_w > 0) then
		Gerar_Ordem_Compra_Venc(nr_ordem_compra_ant_w, nm_usuario_p);
		
		/* Verifica se OC ja existe na lista*/
		/* Verifica virgulas no texto para garantir onde o numero de OC comeca e termina no texto */
		if (instr(ds_lista_oc_w,(', ') || nr_ordem_compra_ant_w || ', ') = 0) then
			ds_lista_oc_w	:= ds_lista_oc_w || nr_ordem_compra_ant_w || ', ';
		end if;
		
		if	(ie_aprova_auto_w = 'S') then
			begin
			select	dt_aprovacao
			into	dt_aprovacao_w
			from	ordem_compra
			where	nr_ordem_compra = nr_ordem_compra_ant_w;
			
			if	(dt_aprovacao_w is null) then
				Calcular_Liquido_Ordem_Compra(nr_ordem_compra_ant_w, nm_usuario_p);
				Gerar_Aprov_Ordem_Compra(nr_ordem_compra_ant_w, null, 'S', nm_usuario_p);
			end if;
			end;
		end if;
	end if;
	end;
end if;

if	(ds_lista_oc_w is not null) then
	begin
	commit;
	ds_lista_oc_w	:= substr(ds_lista_oc_w,3,length(ds_lista_oc_w) -4);
	end;
end if;

ds_lista_oc_p	:= ds_lista_oc_w;

end sup_gerar_ordens_solic_cont;
/
