create or replace
procedure Gerar_dispositivo_protocolo(
			cd_protocolo_p		number,
			nr_Atendimento_p	number,
			nr_seq_procolo_p	number,
			nm_usuario_p		Varchar2) is 

Ie_opcao_w				varchar2(3);
nr_seq_dispositivo_w	number(10);
nr_seq_topografia_w		number(10);
ie_lado_w				varchar2(1);			
ds_titulo_w				varchar2(100);	
qt_hora_permanencia_w	number(15);
qt_hora_perm_curat_w	number(4);	
ds_observacao_w			varchar2(255);
ie_acao_w				varchar2(15);
QT_DISP_UTILIZADOS_w	number(10);
ie_retira_alta_w		varchar2(1);
nr_seq_disp_atend_w	number(15); 
nr_seq_qua_evento_w		number(15);
			
Cursor C01 is
	select 	a.nr_seq_dispositivo,
		a.nr_seq_topografia,
		a.ie_lado,
		a.ds_titulo,
		a.qt_hora_permanencia,
		a.qt_hora_perm_curat,
		ds_observacao,
		substr(a.ie_acao,2,length(a.ie_acao)),
		QT_DISP_UTILIZADOS,
		decode(substr(a.ie_acao,1,1),'S', 'SAE','P')
	from	protocolo_dipositivo a
	where	a.cd_protocolo = cd_protocolo_p
	and	a.NR_SEQUENCIA = nr_seq_procolo_p;

begin

If	(cd_protocolo_p is not null) and
	(nr_seq_procolo_p is not null) then
	obter_param_usuario(1113, 232, Wheb_usuario_pck.get_cd_perfil, nm_usuario_p, 0, ie_retira_alta_w);
	open C01;
	loop
	fetch C01 into	
		nr_seq_dispositivo_w,
		nr_seq_topografia_w,
		ie_lado_w,
		ds_titulo_w,
		qt_hora_permanencia_w,
		qt_hora_perm_curat_w,
		ds_observacao_w,
		ie_acao_w,
		QT_DISP_UTILIZADOS_w,
		Ie_opcao_w;
	exit when C01%notfound;
		begin
		registrar_dipositivo(sysdate, 
							0, 
							nr_seq_dispositivo_w, 
							ie_acao_w, 
							0, 
							0, 
							0, 
							0, 
							null, 
							nr_atendimento_p, 
							nr_seq_topografia_w, 
							ie_lado_w, 
							ds_titulo_w, 
							nm_usuario_p, 
							'N', 
							null, 
							null, 
							qt_hora_permanencia_w, 
							null, 
							obter_pessoa_fisica_usuario(nm_usuario_p, 'C'),
							null,
							null,
							qt_hora_perm_curat_w,  
							null, 
							null,
							qt_disp_utilizados_w, 
							'N', 
							ie_retira_alta_w, 
							ie_opcao_w,
							nr_seq_disp_atend_w,
							null,
							null,
							null,
							'N',
							'N',
							'N',
							'S',
							null,
							null,
							nr_seq_qua_evento_w);
		end;
	end loop;
	close C01;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_dispositivo_protocolo;
/
