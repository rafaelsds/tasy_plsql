create or replace
procedure pls_copiar_diops_ans_solvencia
			(	nr_seq_diops_p		number,
				nr_seq_diops_ant_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_diops_conta_w		number(10);
nr_seq_diops_conta_ant_w	number(10);
qt_registro_w			number(10);

Cursor C01 is
	select	nr_sequencia
	from	diops_conta_ans_solvencia
	where	nr_seq_transacao	= nr_seq_diops_ant_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_diops_conta_ant_w;
exit when C01%notfound;
	begin
	select	diops_conta_ans_solvencia_seq.nextval
	into	nr_seq_diops_conta_w
	from	dual;
	
	select	count(*)
	into	qt_registro_w
	from	diops_conta_ans_solvencia
	where	nr_seq_anterior		= nr_seq_diops_conta_ant_w;
	
	if	(qt_registro_w	= 0) then
		insert into  diops_conta_ans_solvencia
			( nr_sequencia, cd_solvencia, ds_solvencia, 
			ie_tipo_solvencia, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_transacao,
			nr_seq_anterior)
		select	nr_seq_diops_conta_w, cd_solvencia, ds_solvencia, 
			ie_tipo_solvencia, sysdate, nm_usuario_p, 
			sysdate, nm_usuario_p, nr_seq_diops_p,
			nr_seq_diops_conta_ant_w
		from	diops_conta_ans_solvencia
		where	nr_sequencia	= nr_seq_diops_conta_ant_w;

		pls_copiar_conta_solv(nr_seq_diops_conta_w, nr_seq_diops_conta_ant_w, nm_usuario_p);
	end if;

	end;
end loop;
close C01;

commit;

end pls_copiar_diops_ans_solvencia;
/
