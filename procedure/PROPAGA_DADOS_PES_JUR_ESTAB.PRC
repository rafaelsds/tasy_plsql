create or replace
procedure propaga_dados_pes_jur_estab(	cd_cnpj_p		varchar2,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p		Varchar2) is 

/*Essa procedure copia os dados do estabelecimento logado e faz update em todos os outros estabelecimentos
SEMPRE QUE CRIAR UM NOVO CAMPO NA MATERIAL_ESTAB, DEVE SER COLOCADO NESTA PROCEDURE */

nm_pessoa_contato_w				varchar2(255);
nr_ramal_contato_w				number(5);
ds_email_w					varchar2(255);
cd_cond_pagto_w					number(10);
qt_dia_prazo_entrega_w				number(3);
vl_minimo_nf_w					number(15,2);
ie_qualidade_w					varchar2(6);
cd_referencia_fornec_w				varchar2(20);
ie_forma_pagto_w					varchar2(15);
ie_conta_dif_nf_w					varchar2(1);
cd_interno_lab_w					varchar2(20);
cd_interf_lab_lote_envio_w				number(10);
cd_interf_lab_lote_ret_w				number(10);
nr_seq_xml_lab_envio_w				number(10);
nr_seq_xml_lab_ret_w				number(10);
cd_portador_w					number(10);
cd_tipo_portador_w					number(5);
cd_tipo_baixa_w					number(5);
nr_codigo_serv_prest_w				varchar2(10);
tx_desc_antecipacao_w				number(7,4);
cd_interf_pls_mensalidade_w				number(10);
ie_rateio_adiant_w					varchar2(2);
ie_retem_iss_w					varchar2(1);
tx_desc_ordem_w					number(7,4);
ie_endereco_correspondencia_w			number(2);
pr_desc_financeiro_w				number(13,4);
cd_estabelecimento_w				number(4);
qt_existe_w					number(10);

Cursor c01 is
select	cd_estabelecimento
from	estabelecimento
where	ie_situacao = 'A'
and	cd_estabelecimento <> cd_estabelecimento_p;

begin

select	nm_pessoa_contato,
	nr_ramal_contato,
	ds_email,
	cd_cond_pagto,
	qt_dia_prazo_entrega,
	vl_minimo_nf,
	ie_qualidade,
	cd_referencia_fornec,
	ie_forma_pagto,
	ie_conta_dif_nf,
	cd_interno_lab,
	cd_interf_lab_lote_envio,
	cd_interf_lab_lote_ret,
	nr_seq_xml_lab_envio,
	nr_seq_xml_lab_ret,
	cd_portador,
	cd_tipo_portador,
	cd_tipo_baixa,
	nr_codigo_serv_prest,
	tx_desc_antecipacao,
	cd_interf_pls_mensalidade,
	ie_rateio_adiant,
	ie_retem_iss,
	tx_desc_ordem,
	ie_endereco_correspondencia,
	pr_desc_financeiro
into	nm_pessoa_contato_w,
	nr_ramal_contato_w,
	ds_email_w,
	cd_cond_pagto_w,
	qt_dia_prazo_entrega_w,
	vl_minimo_nf_w,
	ie_qualidade_w,
	cd_referencia_fornec_w,
	ie_forma_pagto_w,
	ie_conta_dif_nf_w,
	cd_interno_lab_w,
	cd_interf_lab_lote_envio_w,
	cd_interf_lab_lote_ret_w,
	nr_seq_xml_lab_envio_w,
	nr_seq_xml_lab_ret_w,
	cd_portador_w,
	cd_tipo_portador_w,
	cd_tipo_baixa_w,
	nr_codigo_serv_prest_w,
	tx_desc_antecipacao_w,
	cd_interf_pls_mensalidade_w,
	ie_rateio_adiant_w,
	ie_retem_iss_w,
	tx_desc_ordem_w,
	ie_endereco_correspondencia_w,
	pr_desc_financeiro_w
from	pessoa_juridica_estab
where	cd_cgc = cd_cnpj_p
and	cd_estabelecimento = cd_estabelecimento_p;

open C01;
loop
fetch C01 into	
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	select	count(*)
	into	qt_existe_w
	from	pessoa_juridica_estab
	where	cd_estabelecimento = cd_Estabelecimento_w
	and	cd_cgc = cd_cnpj_p;
	
	if	(qt_existe_w > 0) then
	
		update	pessoa_juridica_estab
		set	dt_atualizacao				= sysdate,
			nm_usuario				= nm_usuario_p,
			nm_pessoa_contato			= nm_pessoa_contato_w,
			nr_ramal_contato				= nr_ramal_contato_w,
			ds_email					= ds_email_w,
			cd_cond_pagto				= cd_cond_pagto_w,
			qt_dia_prazo_entrega			= qt_dia_prazo_entrega_w,
			vl_minimo_nf				= vl_minimo_nf_w,
			ie_qualidade				= ie_qualidade_w,
			cd_referencia_fornec			= cd_referencia_fornec_w,	
			ie_forma_pagto				= ie_forma_pagto_w,
			ie_conta_dif_nf				= ie_conta_dif_nf_w,
			cd_interno_lab				= cd_interno_lab_w,
			cd_interf_lab_lote_envio			= cd_interf_lab_lote_envio_w,
			cd_interf_lab_lote_ret			= cd_interf_lab_lote_ret_w,
			nr_seq_xml_lab_envio			= nr_seq_xml_lab_envio_w,
			nr_seq_xml_lab_ret				= nr_seq_xml_lab_ret_w,
			cd_portador				= cd_portador_w,
			cd_tipo_portador				= cd_tipo_portador_w,
			cd_tipo_baixa				= cd_tipo_baixa_w,
			nr_codigo_serv_prest			= nr_codigo_serv_prest_w,
			tx_desc_antecipacao			= tx_desc_antecipacao_w,
			cd_interf_pls_mensalidade			= cd_interf_pls_mensalidade_w,
			ie_rateio_adiant				= ie_rateio_adiant_w,
			ie_retem_iss				= ie_retem_iss_w,
			tx_desc_ordem				= tx_desc_ordem_w,
			ie_endereco_correspondencia		= ie_endereco_correspondencia_w,
			pr_desc_financeiro				= pr_desc_financeiro_w
		where	cd_cgc					= cd_cnpj_p
		and	cd_estabelecimento 			<> cd_estabelecimento_w;
	else
		insert into pessoa_juridica_estab(
			nr_sequencia,
			cd_cgc,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_pessoa_contato,
			nr_ramal_contato,
			ds_email,
			cd_cond_pagto,
			qt_dia_prazo_entrega,
			vl_minimo_nf,
			ie_qualidade,
			cd_referencia_fornec,
			ie_forma_pagto,
			ie_conta_dif_nf,
			cd_interno_lab,
			cd_interf_lab_lote_envio,
			cd_interf_lab_lote_ret,
			nr_seq_xml_lab_envio,
			nr_seq_xml_lab_ret,
			cd_portador,
			cd_tipo_portador,
			cd_tipo_baixa,
			nr_codigo_serv_prest,
			tx_desc_antecipacao,
			cd_interf_pls_mensalidade,
			ie_rateio_adiant,
			ie_retem_iss,
			tx_desc_ordem,
			ie_endereco_correspondencia,
			pr_desc_financeiro)
		values(	pessoa_juridica_estab_seq.nextval,
			cd_cnpj_p,
			cd_estabelecimento_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nm_pessoa_contato_w,
			nr_ramal_contato_w,
			ds_email_w,
			cd_cond_pagto_w,
			qt_dia_prazo_entrega_w,
			vl_minimo_nf_w,
			ie_qualidade_w,
			cd_referencia_fornec_w,
			ie_forma_pagto_w,
			ie_conta_dif_nf_w,
			cd_interno_lab_w,
			cd_interf_lab_lote_envio_w,
			cd_interf_lab_lote_ret_w,
			nr_seq_xml_lab_envio_w,
			nr_seq_xml_lab_ret_w,
			cd_portador_w,
			cd_tipo_portador_w,
			cd_tipo_baixa_w,
			nr_codigo_serv_prest_w,
			tx_desc_antecipacao_w,
			cd_interf_pls_mensalidade_w,
			ie_rateio_adiant_w,
			ie_retem_iss_w,
			tx_desc_ordem_w,
			ie_endereco_correspondencia_w,
			pr_desc_financeiro_w);
	end if;
	end;
end loop;
close C01;

commit;

end propaga_dados_pes_jur_estab;
/
