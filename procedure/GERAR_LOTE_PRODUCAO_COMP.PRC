CREATE OR REPLACE 
PROCEDURE Gerar_Lote_Producao_Comp(	nr_lote_producao_p     	number,
					cd_estabelecimento_p	number,
                              				nm_usuario_p            	varchar2) IS

cd_material_w				number(6,0);
qt_prevista_w				number(15,4);
qt_lote_padrao_w				Number(15,4);

qt_fixa_w					Number(15,4);
qt_variavel_w				Number(15,4);
qt_conv_Est_consumo_w			Number(15,4);
qt_prevista_cons_w				Number(15,4);
qt_prevista_etq_w				Number(15,4);
nr_sequencia_w				Number(10,0);
cd_material_comp_w			Number(06,0);
cd_unidade_medida_w			Varchar2(30);
cd_unidade_medida_estoque_w		Varchar2(30);
cd_local_estoque_w			Number(4,0);
qt_conv_est_Cons_Lote_w			Number(15,4);
cd_estabelecimento_w			number(4);
nr_ficha_tecnica_w				number(10);

/* Servi�os  */
ie_permite_vincular_servico_w		varchar2(1) := 'N';
nr_seq_proc_interno_w			number(10);
qt_item_w				number(15,2);
vl_item_w					number(15,2);
nr_sequencia_serv_w			number(10);

qt_disp_estoque_atual_w			number(15,4);
nr_seq_componente_w			lote_producao_comp.nr_sequencia%type;

CURSOR C01 IS
select	nvl(b.qt_fixa,0),
	nvl(b.qt_variavel,0),
	a.qt_lote_padrao,
	m.qt_conv_estoque_consumo,
	b.cd_material,
	substr(obter_dados_material_estab(m.cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
	substr(obter_dados_material_estab(m.cd_material,cd_estabelecimento_w,'UME'),1,30) cd_unidade_medida_estoque,
	b.nr_seq_componente
from	material m,
    	ficha_tecnica_componente b,
    	ficha_tecnica a
where 	a.nr_ficha_tecnica   = b.nr_ficha_tecnica
and 	b.ie_situacao          = 'A'
and 	b.ie_tipo_componente   = 1
and 	b.cd_material          = m.cd_material
and 	a.cd_material          = cd_material_w
and	a.cd_estabelecimento  = cd_estabelecimento_w;

/* Servi�os */
CURSOR C02 IS
select	nvl(b.qt_fixa,0),
	nvl(b.qt_variavel,0),
	a.qt_lote_padrao,
	b.nr_seq_proc_interno,
	b.nr_seq_componente,
	a.nr_ficha_tecnica
from	ficha_tecnica_componente b,
    	ficha_tecnica a
where 	a.nr_ficha_tecnica   = b.nr_ficha_tecnica
and 	b.ie_situacao          = 'A'
and 	b.ie_tipo_componente   = 2
and		a.cd_estabelecimento = cd_estabelecimento_w	
and 	a.cd_material          = cd_material_w
and	b.nr_seq_proc_interno is not null
and	not exists (	select	1
			from	lote_producao_serv x
			where	x.nr_lote_producao = nr_lote_producao_p);

BEGIN

select	a.cd_material,
	a.qt_prevista,
	a.cd_local_estoque,
	decode(a.cd_unidade_medida, substr(obter_dados_material_estab(a.cd_material,a.cd_estabelecimento,'UMS'),1,30), 1, m.qt_conv_estoque_consumo),
	a.cd_estabelecimento
INTO	cd_material_w,
	qt_prevista_w,
	cd_local_estoque_w,
	qt_conv_est_Cons_Lote_w,
	cd_estabelecimento_w
from	material m,
	lote_producao a
where 	nr_lote_producao = nr_lote_producao_p
and 	a.cd_material = m.cd_material;

delete	consist_comp_lote_prod
where	nr_lote_producao = nr_lote_producao_p;
commit;

/*Pega o tempo para controle do item do invent�rio conforme par�metro [199] do PalmWeb ( Usu�rio que est� realizando a contagem )*/
obter_param_usuario(143, 307, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_permite_vincular_servico_w);

qt_prevista_w  	 := qt_prevista_w * qt_conv_est_Cons_Lote_w;

OPEN C01;
LOOP
	FETCH C01 into
		qt_fixa_w,
		qt_variavel_w,
		qt_lote_padrao_w,
		qt_conv_Est_consumo_w,
		cd_material_comp_w,
		cd_unidade_medida_w,
		cd_unidade_medida_estoque_w,
		nr_sequencia_w;
      EXIT WHEN C01%NOTFOUND;
            begin

		select	max(nr_sequencia)
		into	nr_seq_componente_w
		from	lote_producao_comp
		where	nr_lote_producao = nr_lote_producao_p
		and	nr_sequencia = nr_sequencia_w;

		if	(nr_seq_componente_w is not null) then
			delete
			from 	lote_producao_comp
			where	nr_lote_producao = nr_lote_producao_p
			and	nr_sequencia = nr_seq_componente_w;
		end if;

		qt_prevista_cons_w := (qt_variavel_w * qt_prevista_w ) +
				      (qt_fixa_w * qt_prevista_w / qt_lote_padrao_w );

	 	qt_prevista_etq_w  := qt_prevista_Cons_w / qt_conv_Est_consumo_w;

		select	obter_saldo_disp_estoque(cd_estabelecimento_p, cd_material_comp_w, cd_local_estoque_w, sysdate)
		into	qt_disp_estoque_atual_w
		from	dual;

		if	(qt_disp_estoque_atual_w < qt_prevista_etq_w) then
			gerar_consist_comp_lote_prod(	nr_lote_producao_p,
							cd_material_comp_w,
							'S',
							wheb_mensagem_pck.get_texto(313578), -- 313578 - Sem saldo para gera��o do lote
							nm_usuario_p,
							qt_disp_estoque_atual_w);
		end if;

		insert into lote_producao_comp
			(nr_lote_producao,
			 nr_sequencia,
			 cd_material,
			 cd_unidade_medida,
			 cd_unid_med_etq,
			 cd_local_estoque,
			 dt_atualizacao,
			 nm_usuario,
			 qt_prevista_etq,
			 qt_prevista,
			 qt_real,
			 qt_estoque,
			 qt_perda,
			 qt_perda_etq)
		values
			(nr_lote_producao_p,
			 nr_sequencia_w,
			 cd_material_comp_w,
			 cd_unidade_medida_w,
	    		 cd_unidade_medida_estoque_w,
			 cd_local_estoque_w,
			 sysdate,
			 nm_usuario_p,
			 nvl(qt_prevista_etq_w,0),
			 nvl(qt_prevista_cons_w,0),
			 0, 0, 0, 0);
		end;
end loop;
close C01;

if	(nvl(ie_permite_vincular_servico_w,'N') = 'S') then
	begin
	OPEN C02;
	LOOP
		FETCH C02 into
			qt_fixa_w,
			qt_variavel_w,
			qt_lote_padrao_w,
			nr_seq_proc_interno_w,
			nr_sequencia_w,
			nr_ficha_tecnica_w;
	      EXIT WHEN C02%NOTFOUND;
			begin
			
			/* Obter  a quantidade do procedimento */
			qt_item_w := cus_obter_qt_servico_prod(nr_ficha_tecnica_w, cd_estabelecimento_w, nr_sequencia_w, sysdate, qt_fixa_w) * qt_prevista_w;

			/* Obter o valor padr�o do procedimento */
			vl_item_w := (qt_item_w * qt_prevista_w * cus_obter_preco_padrao_proc(cd_estabelecimento_w, sysdate, nr_seq_proc_interno_w));

			select	lote_producao_serv_seq.nextval
			into	nr_sequencia_serv_w
			from	dual;

			insert into lote_producao_serv
				(nr_lote_producao,
				 nr_sequencia,
				 nr_seq_proc_interno,
				 dt_atualizacao,
				 nm_usuario,
				 qt_item,
				 vl_item)
			values
				(nr_lote_producao_p,
				 nr_sequencia_serv_w,
				 nr_seq_proc_interno_w,
				 sysdate,
				 nm_usuario_p,
				 nvl(qt_item_w,0),
				 vl_item_w);
			end;
	END LOOP;
	close C02;
	end;
end if;

COMMIT;
END Gerar_Lote_Producao_Comp;
/
