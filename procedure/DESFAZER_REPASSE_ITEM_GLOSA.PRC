create or replace
procedure DESFAZER_REPASSE_ITEM_GLOSA (nr_seq_ret_glosa_p	number,
					nm_usuario_p		varchar2) is


nr_repasse_terceiro_w	number(10,0);

begin

select	max(b.nr_repasse_terceiro)
into	nr_repasse_terceiro_w
from	repasse_terceiro b,
	repasse_terceiro_item a
where	b.ie_status		= 'F'
and	a.nr_repasse_terceiro	= b.nr_repasse_terceiro
and	a.nr_seq_ret_glosa		= nr_seq_ret_glosa_p;

if	(nr_repasse_terceiro_w is null) then
	delete 	from repasse_terceiro_item
	where	nr_seq_ret_glosa	= nr_seq_ret_glosa_p;
else
	wheb_mensagem_pck.exibir_mensagem_abort(265467,'nr_repasse_terceiro= ' || nr_repasse_terceiro_w);
	--Mensagem: O repasse || nr_repasse_terceiro_w || j� est� fechado!

end if;

commit;

end DESFAZER_REPASSE_ITEM_GLOSA;
/
