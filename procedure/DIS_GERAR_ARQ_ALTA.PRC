create or replace
procedure dis_gerar_arq_alta(	nr_atendimento_p		number,
				dt_alta_p			date) is 

arq_texto_w		utl_file.file_type;
ds_linha_arquivo_w		varchar2(2000);
nm_arquivo_w		varchar2(100);
ds_diretorio_w		varchar2(255);
qt_itens_w		number(5);
dt_alta_w			date;
nr_atendimento_w		number(10);
ds_setor_atendimento_w	varchar2(100);
cd_estabelecimento_w	number(4);
nm_usuario_w		varchar2(15);
ie_forma_integracao_w	varchar2(1);

Cursor C01 is
SELECT  'EPD' || 
        '|' || 
        TO_CHAR(dt_alta_p, 'MMDDYYYYHHMMSS') ||
        '|' ||
        nr_atendimento_p ||
        '||||' ||
        TO_CHAR(dt_alta_p, 'MMDDYYYYHHMMSS') ||
        '|' ||
        obter_ds_descricao_setor(obter_setor_atendimento(nr_atendimento_p)) ||
        '|SHS|'
from 	dual
WHERE   dt_alta_p IS NOT NULL
AND     obter_setor_atendimento(nr_atendimento_p) in (select cd_setor_atendimento from dis_regra_setor); 

cursor C02 is
	select  dt_alta_p,
		nr_atendimento_p,
		obter_ds_descricao_setor(obter_setor_atendimento(nr_atendimento_p))
	from 	dual
	where   dt_alta_p is not null
	and     obter_setor_atendimento(nr_atendimento_p) in (select cd_setor_atendimento from dis_regra_setor);

begin

select	cd_estabelecimento,
	nvl(nm_usuario_alta, nm_usuario) 
into	cd_estabelecimento_w,
	nm_usuario_w	
from	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

select  nvl(max(ie_forma_integracao),'A')
into	ie_forma_integracao_w
from	dis_parametros_int
where	cd_estabelecimento = cd_estabelecimento_w;

select	substr(obter_valor_param_usuario(3111, 74, Obter_perfil_ativo, '', ''),1,255)
into	ds_diretorio_w
from	dual;

select	count(*)
into	qt_itens_w
from 	dual
where   dt_alta_p is not null
and     obter_setor_atendimento(nr_atendimento_p) in (select cd_setor_atendimento from dis_regra_setor);

if	(ds_diretorio_w is not null) and (ie_forma_integracao_w = 'A') then
	nm_arquivo_w	:= to_char(sysdate,'MMDDYYYYHHMMSS') || '.pyx';

	if	(qt_itens_w > 0) then
		arq_texto_w 	:= utl_file.fopen(ds_diretorio_w,nm_arquivo_w,'W');

		open C01;
		loop
		fetch C01 into	
			ds_linha_arquivo_w;
		exit when C01%notfound;
			--gera uma nova linha no arquivo		
			utl_file.put_line(arq_texto_w,ds_linha_arquivo_w);
			utl_file.fflush(arq_texto_w);
		end loop;
		close C01;
		
		--fecha e libera o arquivo
		utl_file.fclose(arq_texto_w);
	end if;
end if;

if	(ie_forma_integracao_w = 'B') then
	begin
	open C02;
	loop
	fetch C02 into	
		dt_alta_w,
		nr_atendimento_w,
		ds_setor_atendimento_w;	
	exit when C02%notfound;
		begin
		insert into dis_alta_paciente(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_alta,
			nr_atendimento,
			ds_setor_atendimento,
			ie_status)
		values	(dis_alta_paciente_seq.nextval,
			sysdate,
			nm_usuario_w,
			sysdate,
			nm_usuario_w,
			dt_alta_w,
			nr_atendimento_w,
			ds_setor_atendimento_w,
			'0');

		end;
	end loop;
	close C02;

	end;
end if;

end dis_gerar_arq_alta;
/