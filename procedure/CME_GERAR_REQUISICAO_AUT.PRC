create or replace
procedure cme_gerar_requisicao_aut(	dt_agenda_p		date,
				nm_usuario_p		varchar2) is

nr_seq_agenda_w		number(10,0);
nr_seq_requisicao_w	number(10);
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nm_usuario_orig_w	usuario.nm_usuario%type;
dt_agenda_w		agenda_paciente.dt_agenda%type;

cursor c01 is
	select	nr_sequencia,
		substr(obter_pf_usuario(nm_usuario_orig, 'C'),1,80),
		obter_estab_agenda_paciente(nr_sequencia),
		nm_usuario_orig,
		dt_agenda
	from	agenda_paciente
	where	trunc(dt_agenda) = trunc(dt_agenda_p - 1)
	and     ie_status_agenda not in ('L','I','C','B','F')
	and	nm_usuario_orig is not null;

begin

open c01;
loop
fetch c01 into
	nr_seq_agenda_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	nm_usuario_orig_w,
	dt_agenda_w;
exit when c01%notfound;
	begin
	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	usuario
	where	nm_usuario = nm_usuario_orig_w;
	
	if	(cd_setor_atendimento_w is not null) then
		cme_gerar_requisicao(nr_seq_agenda_w, cd_pessoa_fisica_w, cd_setor_atendimento_w, cd_estabelecimento_w, nm_usuario_p, nr_seq_requisicao_w);
		
		update	cm_requisicao
		set	dt_liberacao = dt_agenda_w,
			dt_requisicao = dt_agenda_w
		where	nr_sequencia = nr_seq_requisicao_w;
		
	end if;
	end;
end loop;
close c01;

commit;

end cme_gerar_requisicao_aut;
/