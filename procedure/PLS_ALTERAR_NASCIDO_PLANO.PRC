create or replace
procedure pls_alterar_nascido_plano
		(	nr_seq_segurado_p	number,
			nm_usuario_p		Varchar2) is 
			
ie_nascido_plano_w	varchar2(10);
ds_alteracao_nascido_w	varchar2(100);

begin

select	nvl(ie_nascido_plano,'N')
into	ie_nascido_plano_w
from	pls_segurado	
where	nr_sequencia		= nr_seq_segurado_p;

if	(ie_nascido_plano_w	= 'N') then
	ds_alteracao_nascido_w	:= 'Altera��o do benefici�rio de "n�o nascido pelo plano" para "nascido pelo plano"';
elsif	(ie_nascido_plano_w	= 'S') then
	ds_alteracao_nascido_w	:= 'Altera��o do benefici�rio de "nascido pelo plano" para "n�o nascido pelo plano"';
end if;	

update	pls_segurado
set	ie_nascido_plano	= decode(ie_nascido_plano_w,'S','N','N','S'),
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_segurado_p;
		
pls_gerar_segurado_historico(	nr_seq_segurado_p, '35', sysdate, ds_alteracao_nascido_w,
				'pls_alterar_nascido_plano', null, null, null,
				null, null, null, null,
				null, null, null, null,
				nm_usuario_p, 'N');

commit;

end pls_alterar_nascido_plano;
/
