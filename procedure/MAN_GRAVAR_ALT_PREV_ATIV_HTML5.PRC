CREATE OR REPLACE PROCEDURE man_gravar_alt_prev_ativ_html5(
    nm_usuario_p           VARCHAR2,
    nr_seq_ordem_p         NUMBER,
    nr_seq_justificativa_p NUMBER,
    ie_tipo_acao_p         VARCHAR2,
    nr_seq_ativ_prev_p     NUMBER,
    nm_executor_prev_p     VARCHAR2,
    nr_seq_funcao_p        NUMBER,
    dt_prevista_p          DATE)
IS
  nr_seq_estagio_w NUMBER(10);
  dt_prevista_w    DATE;
BEGIN
  SELECT NVL(MAX(a.nr_seq_estagio),0)
  INTO nr_seq_estagio_w
  FROM man_ordem_servico a
  WHERE a.nr_sequencia      = nr_seq_ordem_p;
  INSERT
  INTO man_ativ_prev_log
    (
      nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      nr_seq_ordem,
      nr_seq_justificativa,
      nr_seq_estagio,
      ie_tipo_acao,
      nr_seq_ativ_prev,
      nm_executor_prev,
      nr_seq_funcao,
      dt_previsao
    )
    VALUES
    (
      man_ativ_prev_log_seq.nextval,
      sysdate,
      nm_usuario_p,
      nr_seq_ordem_p,
      nr_seq_justificativa_p,
      nr_seq_estagio_w,
      ie_tipo_acao_p,
      nr_seq_ativ_prev_p,
      nm_executor_prev_p,
      nr_seq_funcao_p,
      dt_prevista_w
    );
  COMMIT;
END man_gravar_alt_prev_ativ_html5; 
/