create or replace
procedure pls_atualiza_result_contrato
		(	nr_seq_contrato_p	Number,
			dt_periodo_inicial_p	Date,
			dt_periodo_final_p	Date,
			pr_desp_expediente_p	Number,
			nm_usuario_p		Varchar2) is 
			
dt_mes_referencia_w	Date;
vl_mensalidade_w	Number(15,2)	:= 0;
vl_despesa_w		Number(15,2)	:= 0;
			
Cursor C01 is
	select	trunc(dt_mes_referencia, 'month'),
		sum(nvl(decode(ie_tipo_valor,1,vl_resultado),0)) vl_mensalidade,
		sum(nvl(decode(ie_tipo_valor,1,0,vl_resultado),0)) vl_despesa
	from	pls_resultado
	where	trunc(dt_mes_referencia, 'month') between trunc(dt_periodo_inicial_p, 'month') and trunc(dt_periodo_final_p, 'month')
	group by trunc(dt_mes_referencia, 'month');

begin

open C01;
loop
fetch C01 into	
	dt_mes_referencia_w,
	vl_mensalidade_w,
	vl_despesa_w;
exit when C01%notfound;
	begin
	update	pls_resultado
	set	pr_desp_expediente			= decode(pr_desp_expediente_p,0,pr_desp_expediente,pr_desp_expediente_p)
	where	trunc(dt_mes_referencia, 'month')	= dt_mes_referencia_w
	and	nr_seq_contrato				= nr_seq_contrato_p;
	end;
end loop;
close C01;

commit;

end pls_atualiza_result_contrato;
/