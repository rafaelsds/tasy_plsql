create or replace procedure proj_atual_pr_previsto_etapa (	nr_seq_etapa_p			number,
					nm_usuario_p			varchar2	) is

qt_total_horas_w		number(15,2) := 0;
qt_horas_util_w			number(15,2) := 0;
ie_etapa_fase_w			varchar2(1);
pr_previsto_w			number(15,2) := 0;
nr_seq_superior_w		number(10);
qt_horas_prev_fase_w	number(15,2) := 0;
pr_prev_dia_w			number(15,4);
pr_por_dia_w			number(15,4);
qt_dias_trab_w			number(10,0);
dt_inicio_prev_w		proj_cron_etapa.dt_inicio_prev%type;
dt_fim_prev_w			proj_cron_etapa.dt_fim_prev%type;
qt_dias_uteis_w			proj_cron_etapa.qt_dias_uteis%type;
nr_seq_sub_proj_w		proj_cron_etapa.nr_seq_sub_proj%type;
nr_seq_cronograma_w		proj_cron_etapa.nr_seq_cronograma%type;

cursor c01 is
select	pr_previsto,
		qt_hora_prev
from	proj_cron_etapa
where	nr_seq_superior = nr_seq_etapa_p;

cursor c02 is
select	nr_sequencia
from	proj_cronograma
where	nr_seq_proj = nr_seq_sub_proj_w
and 	ie_situacao = 'A'
and 	dt_aprovacao is not null;

cursor c03 is
select	a.nr_sequencia
from	proj_cron_etapa a
where	a.nr_seq_cronograma = nr_seq_cronograma_w
and	not exists (	select	1
			from	proj_cron_etapa b
			where	b.nr_seq_superior = a.nr_sequencia
		)
order by	a.nr_sequencia;

begin
select	decode(count(*), 0, 'N', 'S')
into	ie_etapa_fase_w
from	proj_cron_etapa
where	nr_seq_superior = nr_seq_etapa_p;

if	(ie_etapa_fase_w = 'S') then
	begin
		select	QT_HORA_PREV
		into	qt_horas_prev_fase_w
		from	proj_cron_etapa
		where	nr_sequencia = nr_seq_etapa_p;

		if (qt_horas_prev_fase_w <> 0) then
			for r_c01 in c01 loop
				-- C�lculo do % previsto das atividades fase, devem considerar o peso das atividade filhas em rela��o a ela, e n�o simplesmente a quantidade de atividades filhas
				pr_previsto_w := pr_previsto_w +  ((r_c01.qt_hora_prev / qt_horas_prev_fase_w) * r_c01.pr_previsto);
			end loop;
		end if;
	end;
else
	select	dt_inicio_prev,
			qt_dias_uteis,
			nr_seq_sub_proj
	into	dt_inicio_prev_w,
			qt_dias_uteis_w,
			nr_seq_sub_proj_w
	from	proj_cron_etapa
	where	nr_sequencia = nr_seq_etapa_p;

	if (nr_seq_sub_proj_w is not null) then
		for r_c02 in c02 loop
			nr_seq_cronograma_w := r_c02.nr_sequencia;
			for r_c03 in c03 loop
				proj_atual_pr_previsto_etapa(r_c03.nr_sequencia, nm_usuario_p);
			end loop;
		end loop;
		
		select obter_proj_atual_pr_previsto(nr_seq_sub_proj_w)
		into pr_previsto_w
		from dual;
	else
		pr_por_dia_w := dividir(100, qt_dias_uteis_w);
		qt_dias_trab_w := obter_dias_uteis_periodo(nvl(dt_inicio_prev_w, sysdate), sysdate, 1);
		pr_prev_dia_w := (pr_por_dia_w * nvl(qt_dias_trab_w,1));

		pr_previsto_w := pr_prev_dia_w;
	end if;
end if;

if	(pr_previsto_w < 0) then
	pr_previsto_w := 0;
elsif	(pr_previsto_w > 100) then
	pr_previsto_w := 100;
end if;

select 	dt_fim_prev
into 	dt_fim_prev_w
from 	proj_cron_etapa
where 	nr_sequencia = nr_seq_etapa_p;

if (sysdate > dt_fim_prev_w) then
	pr_previsto_w := 100;
end if;

update	proj_cron_etapa
set	pr_previsto = pr_previsto_w,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_seq_etapa_p;

select	nr_seq_superior
into	nr_seq_superior_w
from	proj_cron_etapa
where	nr_sequencia = nr_seq_etapa_p;

if	(nr_seq_superior_w is not null) then
	proj_atual_pr_previsto_etapa(nr_seq_superior_w, nm_usuario_p);
end if;

commit;

end proj_atual_pr_previsto_etapa;
/