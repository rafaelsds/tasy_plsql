create or replace
procedure cancelar_reg_lic_compra_item(	nr_sequencia_p		number,
					nr_seq_motivo_cancel_p	number,
					ds_justificativa_p		varchar2,
					nm_usuario_p		varchar2) is 

nr_solic_compra_w			number(10);					
nr_item_solic_compra_w		number(5);
ds_motivo_cancel_w		varchar2(255);
dt_baixa_w			date;
cd_material_w			number(6);
ds_material_w			varchar2(255);
ds_justificativa_w			varchar2(4000);

begin

select	nr_solic_compra,
	nr_item_solic_compra
into	nr_solic_compra_w,
	nr_item_solic_compra_w
from	reg_lic_compra_item
where	nr_sequencia		= nr_sequencia_p;

select	ds_motivo
into	ds_motivo_cancel_w
from	motivo_cancel_sc_oc
where	nr_sequencia		= nr_seq_motivo_cancel_p;

update	reg_lic_compra_item
set	dt_cancelamento		= sysdate,
	nm_usuario_cancel		= nm_usuario_p,
	nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p,
	ds_justif_cancel		= ds_justificativa_p
where	nr_sequencia		= nr_sequencia_p;


if	(nr_solic_compra_w > 0) then

	update	solic_compra_item
	set	dt_baixa			= sysdate,
		cd_motivo_baixa 		= 2,
		nm_usuario		= nm_usuario_p,
		ds_observacao		= substr(decode(ds_observacao,null,Wheb_mensagem_pck.get_Texto(298434) || ds_motivo_cancel_w,
						ds_observacao || chr(10) || chr(13) || Wheb_mensagem_pck.get_Texto(298434) || ds_motivo_cancel_w),1,255)
	where	nr_solic_compra 		= nr_solic_compra_w
	and	nr_item_solic_compra	= nr_item_solic_compra_w;

	update	solic_compra
	set	dt_baixa			= sysdate,
		cd_motivo_baixa		= 2,
		nm_usuario		= nm_usuario_p
	where	nr_solic_compra 		= nr_solic_compra_w
	and	not exists
			(select 1
			from	solic_compra_item
			where	nr_solic_compra = nr_solic_compra_w
			and	dt_baixa is null);

	update	processo_aprov_compra a
	set	a.ie_aprov_reprov = 'B',
		a.ds_observacao = substr(a.ds_observacao || Wheb_mensagem_pck.get_Texto(298435) || nr_solic_compra_w,1,2000)
	where	a.nr_sequencia in(
		select	distinct(nr_seq_aprovacao)
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_w)
	and	ie_aprov_reprov = 'P'
	and	not exists(
		select	1
		from	solic_compra_item x
		where	x.nr_seq_aprovacao = a.nr_sequencia
		and	dt_baixa is null);
	
	if	(nr_item_solic_compra_w > 0) then
	
		select	cd_material,
			substr(obter_desc_material(cd_material),1,255)
		into	cd_material_w,
			ds_material_w
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_w
		and	nr_item_solic_compra = nr_item_solic_compra_w;
	
		if	(ds_justificativa_p is not null) then
			ds_justificativa_w	:= chr(13) || chr(10) || Wheb_mensagem_pck.get_Texto(298436) || ds_justificativa_p;
		end if;
			
		gerar_historico_solic_compra(	nr_solic_compra_w,
				Wheb_mensagem_pck.get_Texto(298437),
				WHEB_MENSAGEM_PCK.get_texto(298438,
					'CD_MATERIAL_W='||cd_material_w||
					';DS_MATERIAL_W='||ds_material_w||
					';DS_MOTIVO_CANCEL_W='||ds_motivo_cancel_w||
					';DS_JUSTIFICATIVA_W='||ds_justificativa_w),				
				'B',
				nm_usuario_p);
	end if;
end if;

commit;

end cancelar_reg_lic_compra_item;
/
