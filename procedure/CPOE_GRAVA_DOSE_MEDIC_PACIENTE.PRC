create or replace
procedure cpoe_grava_dose_medic_paciente(	nr_prescricao_p		prescr_medica.nr_prescricao%type,
											cd_material_p		material.cd_material%type,
											cd_unidade_medida_p	unidade_medida.cd_unidade_medida%type,
											qt_dose_p			number,
											nm_usuario_p		usuario.nm_usuario%type) is

nr_atendimento_w				prescr_medica.nr_atendimento%type;
cd_pessoa_fisica_w				prescr_medica.cd_pessoa_fisica%type;

qt_dose_w						prescr_material.qt_dose%type;

cd_unidade_medida_consumo_w		material.cd_unidade_medida_consumo%type;

nr_sequencia_w					cpoe_pac_dose_material.nr_sequencia%type;
					
begin

select	max(nr_atendimento),
		max(cd_pessoa_fisica)
into	nr_atendimento_w,
		cd_pessoa_fisica_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	max(cd_unidade_medida_consumo)
into	cd_unidade_medida_consumo_w
from	material
where	cd_material = cd_material_p;

qt_dose_w := obter_dose_convertida( cd_material_p, qt_dose_p, cd_unidade_medida_p, cd_unidade_medida_consumo_w);

update	cpoe_pac_dose_material
set		qt_dose 			= nvl(qt_dose,0) + qt_dose_w,
		nm_usuario 			= nm_usuario_p,
		dt_atualizacao 		= sysdate,
		cd_pessoa_fisica	= nvl(cd_pessoa_fisica,cd_pessoa_fisica_w)
where	nvl(cd_pessoa_fisica,cd_pessoa_fisica_w) = cd_pessoa_fisica_w
and		((nr_atendimento = nr_atendimento_w) or
		 ((nr_atendimento is null) and (nr_atendimento_w is null)))
and		cd_material 			= cd_material_p;

if (sql%notfound) then
	insert into	cpoe_pac_dose_material(
				nr_sequencia,
				nr_atendimento,
				cd_pessoa_fisica,
				cd_material,
				qt_dose,
				cd_unidade_medida,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec)
			values(
				cpoe_pac_dose_material_seq.nextval,
				nr_atendimento_w,
				cd_pessoa_fisica_w,
				cd_material_p,
				qt_dose_w,
				cd_unidade_medida_consumo_w,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate);
end if;

commit;

end cpoe_grava_dose_medic_paciente;
/