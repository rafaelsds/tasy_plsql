create or replace procedure encore_gerar_fluxo_passado(dt_inicio_p	date,
							dt_final_p	date,
							nm_usuario_p	varchar2,
							ie_periodo_p	varchar2 default 'M',
							estabelecimento_p	varchar2) is

cursor c01 is
	SELECT	b.cd_empresa,
		a.cd_estabelecimento
FROM	estabelecimento a,
		empresa b
WHERE	a.cd_empresa = b.cd_empresa
AND a.cd_estabelecimento = estabelecimento_p;

vet01_w		c01%rowtype;

begin

	open c01;
	loop
	fetch c01 into
		vet01_w;
	exit when c01%notfound;
		begin
			/* Ir� gerar o fluxo realizado para cada empresa da base, utilizando um dos estabelecimentos da empresa como base */
			/* N�o deve gerar com restri��o de estabelecimento, uma vez que deve gerar todos os dados da empresa */
			gerar_fluxo_caixa_passado(vet01_w.cd_estabelecimento,
						dt_inicio_p,
						dt_final_p,
						nm_usuario_p,
						vet01_w.cd_empresa,
						'N', --ie_restringe_estab
						0, --vl_saldo_anterior
						ie_periodo_p,
						'S', --ie_dia_util_p
						'N', --ie_todas_contas
						'A', --ie_operacao_p
						'S', --ie_somente_ativa_p
						null, --nr_seq_regra_p
						null); --cd_moeda_p
						
		end;
	end loop;
	close c01;
	
	/* Cria a view que ser� utilizada pelo relat�rio. */
	gerar_view_fluxo_caixa_encore(dt_inicio_p,
					dt_final_p,
					null,
					ie_periodo_p,
					'P',
					4,
					'N');
	
	gerar_banda_relat_fluxo_encore(nm_usuario_p);
	
	commit;
end ENCORE_GERAR_FLUXO_PASSADO;
/
