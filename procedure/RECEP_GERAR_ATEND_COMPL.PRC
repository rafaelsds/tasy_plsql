create or replace
procedure recep_gerar_atend_compl(
				nm_usuario_p		Varchar2,
				nr_atendimento_p	number,
				nr_prescricao_p		number,
				nr_seq_prescr_p		number,
				ie_tipo_pessoa_p	varchar2,
				cd_perfil_p		number,
				cd_estabelecimento_p	number,
				nr_atendimento_gerado_p out number,
				ds_erro_p               out varchar2) is 
				
nr_atendimento_w 	number(10);
nr_seq_atecaco_w	number(10);
nr_prescricao_w		number(14);
nr_seq_interno_w	number(10);
nr_seq_prescr_proc_w 	number(6);
qt_gerou_atend_w	number(6);
nr_seq_solic_compl_w	number(10);
				
begin

if	(nvl(nr_atendimento_p,0) > 0) and
	(nvl(nr_prescricao_p,0) > 0) and
	(nvl(nr_seq_prescr_p,0) > 0) then
	
	
	select	nr_seq_atecaco
	into	nr_seq_atecaco_w
	from	prescr_medica
	where 	nr_prescricao = nr_prescricao_p;
	
	select	nr_sequencia
	into	nr_seq_solic_compl_w
	from	solicitacao_exame_compl
	where	nr_atendimento = nr_atendimento_p
	and	nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_seq_prescr_p;

	select	atendimento_paciente_seq.nextval
	into	nr_atendimento_w
	from 	dual;
		
	insert into atendimento_paciente(nr_atendimento,
					ie_permite_visita,
					dt_entrada,
					ie_tipo_atendimento,
					cd_procedencia,
					cd_medico_resp,
					cd_pessoa_fisica,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					nm_usuario_atend)
				select nr_atendimento_w,
					ie_permite_visita,
					sysdate,
					ie_tipo_atendimento,
					cd_procedencia,
					cd_medico_resp,
					cd_pessoa_fisica,
					cd_estabelecimento,
					sysdate,
					nm_usuario_p,
					nm_usuario_p
				from atendimento_paciente
				where nr_atendimento = nr_atendimento_p;
					
	select	atend_categoria_convenio_seq.nextval
	into	nr_seq_interno_w
	from	dual;				
					
	insert into atend_categoria_convenio(cd_convenio,
						cd_categoria,
						dt_inicio_vigencia,
						nr_seq_interno,
						nr_atendimento,
						dt_atualizacao,
						nm_usuario,
						nr_doc_convenio,
						cd_senha,
						cd_plano_convenio)
					select cd_convenio,
						cd_categoria,
						sysdate,
						nr_seq_interno_w,
						nr_atendimento_w,
						sysdate,
						nm_usuario_p,
						nr_doc_convenio,
						cd_senha,
						cd_plano_convenio
					from	atend_categoria_convenio
					where 	nr_seq_interno = nr_seq_atecaco_w;
					
	select	prescr_medica_seq.nextval
	into	nr_prescricao_w
	from	dual;
					
	insert into prescr_medica(nr_prescricao,
				cd_pessoa_fisica,
				dt_prescricao,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_doc_conv,
				nr_seq_atecaco,
				cd_medico,
				cd_senha,
				nr_horas_validade,
				dt_entrega,
				ie_origem_inf,
				dt_primeiro_horario,
				nm_usuario_original,
				cd_estabelecimento,
				cd_setor_atendimento)
			select	nr_prescricao_w,
				cd_pessoa_fisica,
				sysdate,
				nr_atendimento_w,
				sysdate,
				nm_usuario_p,
				nr_doc_conv,
				nr_seq_interno_w,
				cd_medico,
				cd_senha,
				nr_horas_validade,
				sysdate,
				ie_origem_inf,
				dt_primeiro_horario,
				nm_usuario_p,
				cd_estabelecimento_p,
				cd_setor_atendimento
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_p;
		
	select	nvl(max(nr_sequencia), 0) + 1
	into	nr_seq_prescr_proc_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_w;	
		
		
	insert into prescr_procedimento(cd_procedimento,
					qt_procedimento,
					nr_prescricao,
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ie_origem_inf,
					ie_origem_proced,
					nr_seq_proc_interno,
					nr_doc_convenio,
					cd_senha,
					nr_seq_solic_compl,
					cd_equipamento,
					cd_medico_exec,
					cd_setor_atendimento,
					ie_lado,
					cd_motivo_baixa,
					ie_suspenso)		
				select 	cd_procedimento,
					qt_procedimento,
					nr_prescricao_w,
					nr_seq_prescr_proc_w,
					sysdate,
					nm_usuario_p,
					ie_origem_inf,
					ie_origem_proced,
					nr_seq_proc_interno,
					nr_doc_convenio,
					cd_senha,
					nr_seq_solic_compl_w,
					cd_equipamento,
					cd_medico_exec,
					cd_setor_atendimento,
					ie_lado,
					0,
					'N'
				from	prescr_procedimento
				where	nr_prescricao = nr_prescricao_p	
				and	nr_sequencia = nr_seq_prescr_p;

	select	count(*)
	into	qt_gerou_atend_w
	from	atendimento_paciente a,
		atend_categoria_convenio b,
		prescr_medica c
	where   a.nr_atendimento = nr_atendimento_w	
	and	a.nr_atendimento = b.nr_atendimento
	and	a.nr_atendimento = c.nr_atendimento;
	
	if	(qt_gerou_atend_w > 0) then
	
		nr_atendimento_gerado_p := nr_atendimento_w;
		
		update	solicitacao_exame_compl
		set	cd_status = 30,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			dt_recepcionado = sysdate
		where	nr_sequencia = nr_seq_solic_compl_w;  
		
		if	(nr_prescricao_w > 0) then

			liberar_prescricao(nr_prescricao_w,nr_atendimento_w,ie_tipo_pessoa_p,cd_perfil_p,nm_usuario_p,'N',ds_erro_p);
	
		end if;
	end if;
end if;

commit;

end recep_gerar_atend_compl;
/