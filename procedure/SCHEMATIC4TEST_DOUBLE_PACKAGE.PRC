create or replace PROCEDURE schematic4test_double_package(nr_package_p NUMBER, nm_usuario_p VARCHAR2) IS
nr_seq_executionw NUMBER(10,0);
nr_seq_suitew NUMBER(10,0);
nr_seq_urlw NUMBER(10,0);
nr_seq_browserw NUMBER(10,0);
nr_seq_servicew NUMBER(10,0);
nr_seq_snapshotw NUMBER(10,0);
NEWSEQUENCEPACKAGE NUMBER(10,0);
ie_switchitemw VARCHAR2(255);
ie_jobsitemw VARCHAR2(255);
nm_packagew VARCHAR2(255);
ie_jobsw VARCHAR2(255);
ie_chainw VARCHAR2(255);
ie_situacaow VARCHAR2(255);
NM_PACKAGEW2 VARCHAR2(255);

CURSOR c01 IS --loop suites in package
SELECT item.nr_seq_execution,item.nr_seq_suite,item.nr_seq_url,item.nr_seq_browser,item.nr_seq_service,item.ie_switch,item.ie_jobs
  INTO nr_seq_executionw,nr_seq_suitew,nr_seq_urlw,nr_seq_browserw,nr_seq_servicew,ie_switchitemw,ie_jobsitemw
FROM schem_test_package_item item 
WHERE item.nr_seq_package = nr_package_p;

BEGIN
  --procedure that duplicate package
  IF (nr_package_p IS NOT NULL) THEN  
    SELECT nm_package, ie_jobs, ie_chain, nr_seq_snapshot, ie_situacao 
      INTO nm_packagew, ie_jobsw, ie_chainw, nr_seq_snapshotw, ie_situacaow
    FROM schem_test_package
    WHERE nr_sequencia = nr_package_p;
    
    NM_PACKAGEW2 := 'COPY OF '||NM_PACKAGEW;

    SELECT schem_test_package_seq.NEXTVAL SEQUENCIA 
        INTO NEWSEQUENCEPACKAGE
      FROM DUAL;   
      
    INSERT INTO schem_test_package (nr_sequencia,nm_package,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_jobs,ie_chain,nm_owner,nr_seq_snapshot,ie_situacao)
        VALUES (NEWSEQUENCEPACKAGE,NM_PACKAGEW2,sysdate,nm_usuario_p,sysdate,nm_usuario_p,ie_jobsw,ie_chainw,nm_usuario_p,nr_seq_snapshotw, ie_situacaow);
    COMMIT;
    
    OPEN C01;
    LOOP
    FETCH C01 INTO
      nr_seq_executionw,
      nr_seq_suitew,
      nr_seq_urlw,
      nr_seq_browserw,
      nr_seq_servicew,
      ie_switchitemw,
      ie_jobsitemw;
    EXIT WHEN C01%notfound;
    BEGIN        
        INSERT INTO schem_test_package_item (nr_sequencia,nr_seq_execution,nr_seq_suite,nr_seq_url,nr_seq_browser,nr_seq_service,dt_atualizacao_nrec,nr_seq_package,nm_usuario_nrec,ie_switch,ie_jobs, DT_ATUALIZACAO, NM_USUARIO)
          VALUES (schem_test_package_item_seq.NEXTVAL,nr_seq_executionw,nr_seq_suitew,nr_seq_urlw,nr_seq_browserw,nr_seq_servicew,sysdate,NEWSEQUENCEPACKAGE,nm_usuario_p,ie_switchitemw,ie_jobsitemw, SYSDATE, nm_usuario_p);
        COMMIT;
    END;
    END LOOP;
    CLOSE C01;
	
  END IF;
  EXCEPTION
  WHEN no_data_found THEN
	dbms_output.put_line('Erro: Data not found');
END schematic4test_double_package;
/