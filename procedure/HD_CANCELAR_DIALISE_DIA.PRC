create or replace
procedure hd_cancelar_dialise_dia is 
			
nr_seq_motivo_fim_w	number(10);
nr_seq_dialise_w	number(10);	
ds_erro_w		varchar2(255);
ds_retorno_w		varchar2(255);
	
Cursor C01 is
	select	nr_sequencia		
	from	hd_dialise
	where	dt_inicio_dialise is null
	and	dt_cancelamento is null;
begin
select	max(nr_sequencia)
into	nr_seq_motivo_fim_w
from	motivo_fim
where	ie_motivo_fim = 'H'
and	ie_situacao = 'A';

if	(nr_seq_motivo_fim_w is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_dialise_w;
	exit when C01%notfound;
		begin
		HD_Cancelar_Dialise(nr_seq_dialise_w,sysdate,obter_desc_expressao(777072), nr_seq_motivo_fim_w,'Tasy', ds_erro_w, ds_retorno_w);
		end;
	end loop;
	close C01;
end if;
commit;

end hd_cancelar_dialise_dia;
/