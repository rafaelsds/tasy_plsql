create or replace
procedure Transferir_item_pacote_conta( nr_sequencia_p		number,
					nr_seq_proc_pacote_p	number,
					nm_usuario_p		Varchar2) is
					
nr_interno_conta_origem_w	number(10,0);
nr_interno_conta_pacote_w	number(10,0);
nr_atendimento_w		number(10,0);
cd_convenio_w			number(10,0);
cd_categoria_w			varchar2(10);
nr_seq_pacote_w			number(10,0);
nr_seq_proc_interno_w		number(15,0);
cd_procedimento_w		number(10,0);
vl_negociado_w			number(15,2);
ie_ratear_item_w		varchar2(1);

Cursor C01 is
	select	vl_negociado,
		nvl(ie_ratear_item,'S')
	from	pacote_procedimento
	where	nr_seq_pacote = nr_seq_pacote_w
	and 	nvl(cd_procedimento, nvl(cd_procedimento_w,0)) = nvl(cd_procedimento_w,0)
	and 	nvl(nr_seq_proc_interno, nvl(nr_seq_proc_interno_w,0)) = nvl(nr_seq_proc_interno_w,0)
	and 	((nr_seq_proc_interno is not null) or (cd_procedimento is not null))
	and 	nvl(IE_INCLUI_EXCLUI,'I') = 'I'
	order by 1;
					
begin

select 	max(nr_atendimento),
	max(nr_interno_conta),
	max(cd_procedimento),
	max(nr_seq_proc_interno)
into	nr_atendimento_w,
	nr_interno_conta_origem_w,
	cd_procedimento_w,
	nr_seq_proc_interno_w
from 	procedimento_paciente
where 	nr_sequencia = nr_sequencia_p;

select 	max(nr_seq_pacote)
into	nr_seq_pacote_w
from 	atendimento_pacote
where 	nr_atendimento = nr_atendimento_w
and 	nr_seq_procedimento = nr_seq_proc_pacote_p;

select  max(nr_interno_conta)
into	nr_interno_conta_pacote_w
from    procedimento_paciente
where   nr_atendimento  = nr_atendimento_w
and 	nr_sequencia 	= nr_seq_proc_pacote_p;

select	max(cd_convenio_parametro),
	max(cd_categoria_parametro)
into	cd_convenio_w,
	cd_categoria_w
from 	conta_paciente
where 	nr_interno_conta = nr_interno_conta_pacote_w;

vl_negociado_w:= 0;

open C01;
loop
fetch C01 into	
	vl_negociado_w,
	ie_ratear_item_w;
exit when C01%notfound;
	begin
	vl_negociado_w:= vl_negociado_w;
	end;
end loop;
close C01;

update procedimento_paciente
set	cd_convenio     	= cd_convenio_w,
      	cd_categoria    	= cd_categoria_w,
	nr_interno_conta	= nr_interno_conta_pacote_w,
      	vl_procedimento 	= vl_negociado_w,
	vl_medico       	= 0,
	vl_anestesista  	= 0,
	vl_materiais    	= 0,
	vl_auxiliares   	= 0,
	vl_custo_operacional 	= 0,
	ie_valor_informado 	= 'S',
	nr_seq_proc_pacote	= nr_seq_proc_pacote_p,
	ie_ratear_item		= ie_ratear_item_w
where 	nr_sequencia  		= nr_sequencia_p;

if	(philips_param_pck.get_cd_pais = 2) then
	delete	from propaci_imposto
	where	nr_seq_propaci = nr_sequencia_p;
end if;

commit;

end Transferir_item_pacote_conta;
/
