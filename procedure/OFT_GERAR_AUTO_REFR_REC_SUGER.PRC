create or replace
procedure oft_gerar_auto_refr_rec_suger	(	nr_seq_consulta_p	number,
								nm_usuario_p		varchar2) is 

cd_profissional_w	varchar2(10);
qt_oft_refracao_w	number(10,0);
				
begin

select	obter_pessoa_fisica_usuario(nm_usuario_p,'C')
into	cd_profissional_w
from	dual;

select	count(*)
into	qt_oft_refracao_w
from	oft_auto_refracao
where	nr_seq_consulta = nr_seq_consulta_p
and 	((ie_refracao_sugerida = 'N') or (ie_refracao_sugerida IS NULL));

if	(qt_oft_refracao_w > 0) then
	begin
	insert into oft_auto_refracao(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_consulta,
		vl_od_ard_esf,
		vl_od_ard_cil,
		vl_od_ard_eixo,
		qt_reflexo_od,
		qt_distancia_np,
		vl_oe_ard_esf,
		vl_oe_ard_cil,
		vl_oe_ard_eixo,
		qt_reflexo_oe,
		vl_od_are_esf,
		vl_od_are_cil,
		vl_od_are_eixo,
		qt_reflexo_are_od,
		vl_oe_are_esf,
		vl_oe_are_cil,
		vl_oe_are_eixo,
		qt_reflexo_are_oe,
		vl_od_lenso_esf,
		vl_od_lenso_cil,
		vl_od_lenso_eixo,
		vl_adicao,
		vl_oe_lenso_esf,
		vl_oe_lenso_cil,
		vl_oe_lenso_eixo,
		cd_profissional,
		dt_exame,
		ds_observacao,
		ie_refracao_sugerida,
		ie_situacao)
	select	oft_auto_refracao_seq.nextval,   
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_consulta_p,
		decode(ie_receita_dinamica,'S',vl_od_ard_esf,null),
		decode(ie_receita_dinamica,'S',vl_od_ard_cil,null),
		decode(ie_receita_dinamica,'S',vl_od_ard_eixo,null),
		decode(ie_receita_dinamica,'S',qt_reflexo_od,null),
		decode(ie_receita_dinamica,'S',qt_distancia_np,null),
		decode(ie_receita_dinamica,'S',vl_oe_ard_esf,null),
		decode(ie_receita_dinamica,'S',vl_oe_ard_cil,null),
		decode(ie_receita_dinamica,'S',vl_oe_ard_eixo,null),
		decode(ie_receita_dinamica,'S',qt_reflexo_oe,null),
		decode(ie_receita_estatica,'S',vl_od_are_esf,null),
		decode(ie_receita_estatica,'S',vl_od_are_cil,null),
		decode(ie_receita_estatica,'S',vl_od_are_eixo,null),
		decode(ie_receita_estatica,'S',qt_reflexo_are_od,null),
		decode(ie_receita_estatica,'S',vl_oe_are_esf,null),
		decode(ie_receita_estatica,'S',vl_oe_are_cil,null),
		decode(ie_receita_estatica,'S',vl_oe_are_eixo,null),
		decode(ie_receita_estatica,'S',qt_reflexo_are_oe,null),
		vl_od_lenso_esf,
		vl_od_lenso_cil,
		vl_od_lenso_eixo,
		vl_adicao,
		vl_oe_lenso_esf,
		vl_oe_lenso_cil,
		vl_oe_lenso_eixo,
		cd_profissional_w,
		sysdate,
		ds_observacao,
		'S',
		'A'
	from	oft_auto_refracao
	where	nr_seq_consulta = nr_seq_consulta_p
	and	ie_situacao = 'A'
	and 	((ie_refracao_sugerida = 'N') or (ie_refracao_sugerida IS NULL));
	commit;
	end;
end if;

end oft_gerar_auto_refr_rec_suger;
/