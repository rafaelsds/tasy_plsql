create or replace
procedure pls_incluir_sca_cont_exist_js(
		nr_seq_proposta_p			number,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		nr_seq_contrato_p			number,
		nr_contrato_p		out	number) is 

begin

pls_incluir_sca_cont_exist(
	nr_seq_proposta_p,
	nm_usuario_p,
	cd_estabelecimento_p);

nr_contrato_p	:= pls_obter_seq_contrato(nr_seq_contrato_p);

end pls_incluir_sca_cont_exist_js;
/