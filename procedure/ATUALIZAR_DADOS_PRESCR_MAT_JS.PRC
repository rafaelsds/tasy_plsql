create or replace
procedure atualizar_dados_prescr_mat_js (	cd_motivo_baixa_p		number,
											nr_seq_prescr_mat_p		number,
											nr_seq_item_p			number,
											cd_material_baixa_p		number,
											ie_opcao_p				varchar2,
											nm_usuario_p			varchar2) is 

/* ####   IE_OPCAO_P   #### 			*/
/*ADCD - Atualizar data de baixa e motivo da baixa	*/
/*ACB - Atualizar o c�digo de baixa do material 		*/
/*					*/						
						
begin

if	(nr_seq_prescr_mat_p is not null) and
	(cd_motivo_baixa_p is not null) and
	(nr_seq_item_p is not null)	 then

	update	prescr_material
	set		dt_baixa		= decode(ie_opcao_p,'ADCD',sysdate,'ACB',null),
			cd_motivo_baixa = cd_motivo_baixa_p
	where	nr_sequencia 	= nr_seq_item_p
	and		nr_prescricao 	= nr_seq_prescr_mat_p;
		
end if;

commit;

end atualizar_dados_prescr_mat_js;
/
