create or replace
procedure pls_inativar_registro_preco(	nr_seq_seg_preco_p	pls_segurado_preco.nr_sequencia%type,
					nm_usuario_p		Varchar2) is 

begin

update	pls_segurado_preco
set	ie_situacao = 'I',
	ds_observacao = ds_observacao || ' - Registro inativado manualmente pelo usu�rio ' || nm_usuario_p || '.',
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_seq_seg_preco_p;

commit;

end pls_inativar_registro_preco;
/
