create or replace 
procedure far_gerar_meta_resumo_js (
		nr_seq_meta_p		number,
		ds_menssagem_p	out	varchar2,
		nm_usuario_p		varchar2) is 

begin
	far_gerar_meta_resumo(nr_seq_meta_p, nm_usuario_p);
	ds_menssagem_p := obter_texto_tasy(103890, wheb_usuario_pck.get_nr_seq_idioma);
	
end far_gerar_meta_resumo_js;
/