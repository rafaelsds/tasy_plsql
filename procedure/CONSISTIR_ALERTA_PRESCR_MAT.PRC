create or replace
procedure consistir_alerta_prescr_mat(	nr_prescricao_p		number,
										nr_sequencia_p		number,
										ds_mensagem_p		out varchar2) is 

cd_material_w		number(10);
cd_unidade_medida_w	varchar2(30);
cd_Setor_atendimento_w number(5);
qt_idade_w			number(10);
ie_via_aplicacao_w	varchar2(30);
ds_mensagem_w		varchar2(2000);
cd_convenio_w		number(5);



Cursor C01 is
	select	ds_mensagem
	from	regra_alerta_material
	where	1 = 1
	and		((nr_seq_estrutura is null)	or (consistir_se_mat_estrutura(nr_seq_estrutura,cd_material_w)	= 'S'))
	and		qt_idade_w between nvl(qt_idade_min, 0) and nvl(qt_idade_max, 999)
	and		nvl(ie_via_aplicacao,ie_via_aplicacao_w) = ie_via_aplicacao_w
	and		nvl(cd_unidade_medida,cd_unidade_medida_w) = cd_unidade_medida_w
	and		nvl(cd_convenio, cd_convenio_w) = cd_convenio_w
	and		nvl(cd_Setor_atendimento,cd_Setor_atendimento_w) = cd_Setor_atendimento_w
	and		nvl(cd_material,cd_material_w) = cd_material_w
	and	((cd_perfil is null) or (cd_perfil = obter_perfil_ativo))
	and		nvl(ie_atend_prescr,'N') = 'N'
	and		ie_situacao	= 'A'
	order by nvl(qt_idade_min, 0),
		 nvl(cd_material,0),
		 nvl(nr_seq_estrutura,0),
		 nvl(cd_Setor_atendimento,0),
		 nvl(ie_via_aplicacao,'0'),
		 nvl(cd_unidade_medida,'0');

			
begin
--if	(nr_prescricao_p is not null) and
--	(nr_sequencia_p is not null) then
	begin

	select	a.cd_material,
			nvl(a.ie_via_aplicacao,'0'),
			nvl(a.cd_unidade_medida_dose,'0'),
			nvl(b.cd_setor_atendimento,0),
			nvl(obter_idade_pf(b.cd_pessoa_fisica,sysdate,'A'),0),
			nvl(obter_convenio_atendimento(b.nr_atendimento),0)
	into	cd_material_w,
			ie_via_aplicacao_w,
			cd_unidade_medida_w,
			cd_Setor_atendimento_w,
			qt_idade_w,
			cd_convenio_w
	from	prescr_material a,
			prescr_medica b
	where	a.nr_prescricao		= b.nr_prescricao
	and		a.nr_sequencia		= nr_sequencia_p
	and		a.nr_prescricao		= nr_prescricao_p
	and		b.nr_prescricao		= nr_prescricao_p;


	open C01;
	loop
	fetch C01 into	
		ds_mensagem_w;
	exit when C01%notfound;
	end loop;
	close C01;
		
	exception 
		when others then
		null;
		end;
--end if;

ds_mensagem_p	:= substr(ds_mensagem_w,1,255);	
	
end consistir_alerta_prescr_mat;
/
