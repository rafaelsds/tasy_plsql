create or replace
procedure lab_soro_rack_ger_etiq(nr_sequencia_p	number,
                                 nm_usuario_p   varchar2,
                                 nr_etiqueta_p  out number) is
begin
  select LAB_SORO_RACK_ETIQ_SEQ.nextval 
    into nr_etiqueta_p      
    from dual; 
  
    update lab_soro_rack set nr_etiqueta = nr_etiqueta_p,
                             nm_usuario = nm_usuario_p,
                             dt_atualizacao = sysdate
    where nr_sequencia = nr_sequencia_p;                        							  
    commit;       
end lab_soro_rack_ger_etiq;
/