create or replace
procedure bft_request_vital_signs_job(
			nm_usuario_p		varchar2) is 

Cursor C01 is
	select	b.nr_atendimento, a.nm_leito_integracao
	from	unidade_atendimento a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.ie_leito_monitorado = 'S' 
	and	a.nm_leito_integracao is not null
	and	a.ie_situacao = 'A';			
			
begin

for r_c01 in c01 loop
	begin
		bft_request_vital_signs( r_c01.nr_atendimento, nm_usuario_p);
	exception
	when others then
		null;
	end;
end loop;


end bft_request_vital_signs_job;
/