create or replace procedure gerar_carga_fluxo_caixa(dt_referencia_p	date,
						nr_seq_agend_p		number,
						ie_origem_info_p	varchar2,
						nm_usuario_p		varchar2,
						ds_erro_p		out varchar2) is

/* */

ds_log_w		fluxo_caixa_agendamento.ds_log%type;
ds_log_ww		fluxo_caixa_agendamento.ds_log%type;

cursor c01 is
	select	distinct a.nr_titulo nr_documento,
		a.nr_sequencia nr_docto_compl,
		'TRB' ie_origem
	from	titulo_receber_liq a
	where	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_titulo_receber = a.nr_titulo
				and	nr_seq_tit_rec_baixa = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p)
	union all
	select	distinct a.nr_titulo nr_documento,
		null nr_docto_compl,
		'TR' ie_origem
	from	titulo_receber a
	where	not exists 	(select	1
				from	titulo_receber_liq b
				where	b.nr_titulo = a.nr_titulo)
	and	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_titulo_receber = a.nr_titulo
				and	nr_seq_tit_rec_baixa is null
				and	dt_atualizacao >= dt_referencia_p)
	union all
	select	distinct a.nr_titulo nr_documento,
		null nr_docto_compl,
		'TRT' ie_origem
	from	titulo_receber_trib a
	where	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_titulo_receber = a.nr_titulo
				and	nr_seq_tit_rec_baixa is null
				and	ie_integracao = 'CP'
				and	dt_atualizacao >= dt_referencia_p);

cursor c02 is
	select	distinct a.nr_titulo nr_documento,
		a.nr_sequencia nr_docto_compl,
		'TPBA' ie_origem
	from	titulo_pagar_adiant a
	where	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_titulo_pagar = a.nr_titulo
				and	nr_seq_tit_pag_baixa = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p)
	union all
	select	distinct a.nr_titulo nr_documento,
		a.nr_sequencia nr_docto_compl,
		'TPB' ie_origem
	from	titulo_pagar_baixa a
	where	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_titulo_pagar = a.nr_titulo
				and	nr_seq_tit_pag_baixa = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p)
	union all
	select	distinct a.nr_titulo nr_documento,
		null nr_docto_compl,
		'TP' ie_origem
	from	titulo_pagar a
	where	not exists 	(select	1
				from	titulo_pagar_baixa b
				where	b.nr_titulo = a.nr_titulo)
	and	not exists 	(select	1
				from	titulo_pagar_adiant c
				where	c.nr_titulo = a.nr_titulo)			
	and	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_titulo_pagar = a.nr_titulo
				and	nr_seq_tit_pag_baixa is null
				and	dt_atualizacao >= dt_referencia_p);

cursor c03 is
	select	distinct a.nr_seq_cheque nr_documento,
		'CR' ie_origem
	from	cheque_cr a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_cheque_cr = a.nr_seq_cheque
				and	dt_atualizacao >= dt_referencia_p);

cursor c04 is
	select	distinct a.nr_sequencia nr_documento,
		'CP' ie_origem
	from	cheque a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_cheque_cp = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p);

cursor c05 is
	select	distinct a.nr_seq_movto nr_documento,
		a.nr_seq_parcela nr_docto_compl,
		'CCB' ie_origem
	from	movto_cartao_cr_baixa a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_movto_cartao = a.nr_seq_movto
				and	nr_seq_movto_parcela = a.nr_seq_parcela
				and	dt_atualizacao >= dt_referencia_p)
	union all
	select	distinct a.nr_seq_movto nr_documento,
		a.nr_sequencia nr_docto_compl,
		'CC' ie_origem
	from	movto_cartao_cr_parcela a
	where	not exists	(select	1
				from	movto_cartao_cr_baixa
				where	nr_seq_movto = a.nr_seq_movto
				and	nr_seq_parcela = a.nr_sequencia)
	and	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_movto_cartao = a.nr_seq_movto
				and	nr_seq_movto_parcela = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p);

cursor c06 is
	select	distinct a.nr_sequencia nr_documento,
		'RC' ie_origem
	from	convenio_receb a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_conv_receb = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p);

cursor c07 is
	select	distinct a.nr_sequencia nr_documento,
		'CBT' ie_origem
	from	movto_trans_financ a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_movto_trans = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p);

cursor c08 is
	select	distinct a.nr_sequencia nr_documento,
		'PR' ie_origem
	from	projeto_recurso a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_seq_proj_recurso = a.nr_sequencia
				and	dt_atualizacao >= dt_referencia_p);

cursor c09 is
	select	distinct a.nr_seq_protocolo nr_documento,
		'PC' ie_origem
	from	protocolo_convenio a
	where	not exists	(select	1
				from	fluxo_caixa_docto b
				where	b.nr_seq_protocolo = a.nr_seq_protocolo
				and	b.dt_atualizacao >= dt_referencia_p);

cursor c10 is
	select	distinct a.nr_seq_contrato nr_documento,
		a.nr_sequencia nr_docto_compl,
		'GEF' ie_origem
	from	emprest_financ_parc a
	where	not exists	(select	1
				from	fluxo_caixa_docto b
				where	b.nr_seq_contrato = a.nr_seq_contrato
				and	b.nr_seq_emprest_parc = a.nr_sequencia
				and	b.dt_atualizacao >= dt_referencia_p);
				
cursor c11 is
	select	distinct a.nr_ordem_compra nr_documento,
		a.NR_ITEM_OC_VENC  nr_docto_compl,
		'OC' ie_origem
	from	ORDEM_COMPRA_VENC a
	where	not exists	(select	1
				from	fluxo_caixa_docto
				where	nr_ordem_compra = a.nr_ordem_compra
				and 	nr_seq_oc_vencimento = a.nr_item_oc_venc
				and	dt_atualizacao >= dt_referencia_p)
	union all
	select	distinct a.nr_ordem_compra nr_documento,
		null nr_docto_compl,
		'OC' ie_origem
	from	ordem_compra_item_entrega a
	where	not exists 	(select	1
				from	fluxo_caixa_docto
				where	nr_ordem_compra = a.nr_ordem_compra
				and 	nr_seq_oc_vencimento = a.nr_sequencia
				and	ie_integracao = 'OC'
				and	dt_atualizacao >= dt_referencia_p);					
				
vet01		c01%rowtype;
vet02		c02%rowtype;
vet03		c03%rowtype;
vet04		c04%rowtype;
vet05		c05%rowtype;
vet06		c06%rowtype;
vet07		c07%rowtype;
vet08		c08%rowtype;
vet09		c09%rowtype;
vet10		c10%rowtype;
vet11		c11%rowtype;

begin
	if (ie_origem_info_p = 'XFLCX') then
		open c01;
		loop
		fetch c01 into
			vet01;
		exit when c01%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet01.ie_origem in ('TR','TRB','TRT')) then
					gerar_fluxo_caixa_tit_receb(vet01.nr_documento,
								vet01.nr_docto_compl,
								vet01.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet01.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c01;
		
		open c02;
		loop
		fetch c02 into
			vet02;
		exit when c02%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet02.ie_origem in ('TP','TPB','TPBA')) then
					gerar_fluxo_caixa_tit_pagar(vet02.nr_documento,
								vet02.nr_docto_compl,
								vet02.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet02.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c02;
		
		open c03;
		loop
		fetch c03 into
			vet03;
		exit when c03%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet03.ie_origem in ('CR')) then
					gerar_fluxo_caixa_cheque_cr(vet03.nr_documento,
								vet03.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet03.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c03;
		
		open c04;
		loop
		fetch c04 into
			vet04;
		exit when c04%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet04.ie_origem in ('CP')) then
					gerar_fluxo_caixa_cheque_cp(vet04.nr_documento,
								vet04.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet04.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c04;
		
		open c05;
		loop
		fetch c05 into
			vet05;
		exit when c05%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet05.ie_origem in ('CC','CCB')) then
					gerar_fluxo_caixa_cartao_cr(vet05.nr_documento,
								vet05.nr_docto_compl,
								vet05.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet05.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c05;
		
		open c06;
		loop
		fetch c06 into
			vet06;
		exit when c06%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet06.ie_origem in ('RC')) then
					gerar_fluxo_caixa_receb_conv(vet06.nr_documento,
								vet06.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet06.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c06;
		
		open c07;
		loop
		fetch c07 into
			vet07;
		exit when c07%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet07.ie_origem in ('CBT')) then
					gerar_fluxo_caixa_movto_trans(vet07.nr_documento,
								null,
								vet07.ie_origem,
								dt_referencia_p,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet07.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c07;
		
		open c08;
		loop
		fetch c08 into
			vet08;
		exit when c08%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet08.ie_origem in ('PR')) then
					gerar_fluxo_caixa_proj_rec(vet08.nr_documento,
								vet08.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet08.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c08;
		
		open c09;
		loop
		fetch c09 into
			vet09;
		exit when c09%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet09.ie_origem in ('PC')) then
					gerar_fluxo_caixa_prot_conv(vet09.nr_documento,
								vet09.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet09.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c09;
		
		open c10;
		loop
		fetch c10 into
			vet10;
		exit when c10%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet10.ie_origem in ('GEF')) then
					gerar_fluxo_caixa_emprest_fin(vet10.nr_documento,
								vet10.nr_docto_compl,
								vet10.ie_origem,
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet10.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c10;
		
		
		open c11;
		loop
		fetch c11 into
			vet11;
		exit when c11%notfound;
			begin
				
				/* Inicia o processamento dos documentos conforme sua origem */
				if (vet11.ie_origem in ('OC')) then
					GERAR_FLUXO_CAIXA_DOCTO_OC(vet11.nr_documento,
								vet11.nr_docto_compl,
								vet11.ie_origem,
								'S',
								ds_log_w);
				end if;
				
				ds_log_ww := substr(ds_log_ww || ' ' || ds_log_w,0,4000);
			exception when others then
				ds_log_ww := substr(ds_log_ww || ' <' ||vet11.ie_origem||'> '|| sqlerrm,0,4000);
			end;
		end loop;
		close c11;		
		
		
		/* Atualiza a fila ap�s o processamento do registro */
		if (nvl(ds_log_ww,'') <> '') then
			update	fluxo_caixa_agendamento
			set	ie_status_agend = 'E',
				ds_log = ds_log_ww,
				dt_processamento = sysdate
			where	nr_sequencia = nr_seq_agend_p;
		else
			update	fluxo_caixa_agendamento
			set	ie_status_agend = 'P',
				dt_processamento = sysdate
			where	nr_sequencia = nr_seq_agend_p;
		end if;
		
		gerar_comunic_padrao(sysdate,
					substr(wheb_mensagem_pck.get_texto(990571),1,100),
					substr(wheb_mensagem_pck.get_texto(990572),1,100) ||chr(10)||chr(13)||ds_log_ww,
					'Tasy',
					'N',
					nm_usuario_p,
					'N',
					null,
					null,
					null,
					null,
					sysdate,
					null,
					null);
		commit;
	end if;
	
	ds_erro_p := ds_log_ww;
	
end gerar_carga_fluxo_caixa;
/
