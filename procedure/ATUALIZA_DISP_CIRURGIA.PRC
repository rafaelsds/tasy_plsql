create or replace 
procedure atualiza_disp_cirurgia(nr_cirurgia_p		number) 
				is

qt_material_w			number(15,4);
qt_disp_pepo_w			number(15,4);
nr_prescricao_w			number(15);
nr_sequencia_w			number(10);
qt_dispensacao_w		number(15,4);
qt_diferenca_w			number(15,4);
ie_consolidacao_generico_w	varchar2(1);
cd_unidade_medida_w		varchar2(30);
cd_material_w			number(6);



cursor	c01 is

	select	nvl(b.qt_material,0),
		nvl(b.qt_disp_pepo,0),
		b.nr_prescricao,
		b.nr_sequencia
	from	cirurgia a,
		prescr_material b
	where	a.nr_prescricao 	= b.nr_prescricao
	and	b.cd_material		= cd_material_w
	and	b.cd_unidade_medida	= cd_unidade_medida_w
	and	a.nr_cirurgia		= nr_cirurgia_p
	order by b.qt_material desc,nr_sequencia; 

	
cursor	c02 is	
	select	nvl(a.qt_dispensacao,0) qt_dispensacao,
		a.cd_unidade_medida,
		a.cd_material
	from	cirurgia_agente_disp a,
		material b
	where	a.cd_material			= b.cd_material_estoque
	and	a.ie_operacao			= 'D'
	and	a.nr_cirurgia			= nr_cirurgia_p
	and	ie_consolidacao_generico_w 	= 'S'
	union all
	select	nvl(a.qt_dispensacao,0) qt_dispensacao,
		a.cd_unidade_medida,
		a.cd_material
	from	cirurgia_agente_disp a
	where	a.nr_cirurgia			= nr_cirurgia_p
	and	a.ie_operacao			= 'D'
	and	ie_consolidacao_generico_w 	= 'N'
	order by cd_material,qt_dispensacao;

begin

update	prescr_material
set	qt_disp_pepo 	= 0
where	nr_prescricao 	= (select max(nr_prescricao) from cirurgia where nr_cirurgia = nr_cirurgia_p);

commit;

obter_param_usuario(872, 152, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_consolidacao_generico_w);

open c02;
loop
fetch c02 into	
	qt_dispensacao_w,
	cd_unidade_medida_w,
	cd_material_w;
exit when c02%notfound;
	begin
	open c01;
	loop
	fetch c01 into	
		qt_material_w,
		qt_disp_pepo_w,
		nr_prescricao_w,
		nr_sequencia_w;
	exit when c01%notfound;
		begin
		if	(qt_dispensacao_w > 0) and (qt_disp_pepo_w < qt_material_w) then
			if	(qt_dispensacao_w = 1) then
				update	prescr_material
				set	qt_disp_pepo	= nvl(qt_disp_pepo,0) + 1
				where	nr_prescricao	= nr_prescricao_w
				and	nr_sequencia	= nr_sequencia_w;
				qt_dispensacao_w 	:= 0;
			else
				qt_diferenca_w := qt_material_w	- qt_disp_pepo_w;
				if	(qt_diferenca_w >= qt_dispensacao_w) then
					update	prescr_material
					set	qt_disp_pepo	= nvl(qt_disp_pepo,0) + qt_dispensacao_w
					where	nr_prescricao	= nr_prescricao_w
					and	nr_sequencia	= nr_sequencia_w;
					qt_dispensacao_w 	:= 0;
				else
					qt_dispensacao_w	:= qt_dispensacao_w - qt_diferenca_w;
					update	prescr_material
					set	qt_disp_pepo	= qt_material
					where	nr_prescricao	= nr_prescricao_w
					and	nr_sequencia	= nr_sequencia_w;
				end if;
			end if;
		end if;	
		end;
		end loop;
		close c01;
	if	(qt_dispensacao_w > 0) then
		update	prescr_material
		set	qt_disp_pepo	= qt_disp_pepo + qt_dispensacao_w
		where	nr_prescricao	= nr_prescricao_w
		and	nr_sequencia	= nr_sequencia_w;
	end if;	

	begin
	commit;
	exception
		when others then
		nr_sequencia_w := null;
	end;
	end;
end loop;
close c02;

end atualiza_disp_cirurgia;
/