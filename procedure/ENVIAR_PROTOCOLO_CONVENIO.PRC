create or replace 
procedure enviar_protocolo_convenio(	ds_arquivo_p		varchar2, 
				nr_seq_protocolo_p		number, 
				nm_usuario_p		varchar2) is 

begin 
if (nr_seq_protocolo_p is not null) and
 	(ds_arquivo_p is not null) then
	begin
		update 	protocolo_convenio 
		set	dt_envio = sysdate, 
			nm_usuario_envio = nm_usuario_p, 
			ds_arquivo_envio = ds_arquivo_p 
		where 	nr_seq_protocolo = nr_seq_protocolo_p;
		commit;
	end; 
end if;	


end enviar_protocolo_convenio;
/
