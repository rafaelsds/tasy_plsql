create or replace procedure hd_exame_mes_anter_relat7	(	DT_INICIO_P		Date,
						NR_SEQ_UNIDADE_P	Number,
						DS_ITENS_P		Number,
						nm_usuario_p		Varchar2
						) is

begin
delete from HD_GRID_CONTROLE_DIALISE where nm_usuario = nm_usuario_p;
hd_gerar_contr_dial_rel_7(
					0,
					DS_ITENS_P,
					nm_usuario_P,
					NR_SEQ_UNIDADE_P,
					0,
					null,
					null,
					'S',
					'N',
					'S',
					(trunc(DT_INICIO_P,'MM') - 5),
					'N',
					'N',
					null,
					null,
					null,
					0,
					0);
-------------------------------------------------------------------------------
hd_gerar_contr_dial_rel_7(
					0,
					DS_ITENS_P,
					nm_usuario_P,
					NR_SEQ_UNIDADE_P,
					0,
					null,
					null,
					'S',
					'N',
					'S',
					DT_INICIO_P,
					'N',
					'N',
					null,
					null,
					null,
					0,
					0);

end;
/