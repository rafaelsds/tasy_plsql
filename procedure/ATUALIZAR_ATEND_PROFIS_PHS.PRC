create or replace procedure atualizar_atend_profis_phs(   nr_atendimento_p        number,
                                                                cd_pessoa_fisica_p      varchar2,
                                                                ie_profissional_p       varchar2,
                                                                nm_usuario_p            varchar2,
                                                                nr_seq_nursing_team_p   number default null,
                                                                nr_seq_phs_p            atend_enfermagem_aux.nr_seq_phs%type) is

cd_pessoa_fisica_w        varchar2(10);

cursor c01 is
    select  cd_pessoa_fisica
    from    atend_profissional
    where   nr_atendimento  = nr_atendimento_p
    and     ie_profissional = ie_profissional_p
    order by
            dt_inicio_vigencia;

begin
	open C01;
        loop
            fetch C01 into
                cd_pessoa_fisica_w;
            exit when c01%notfound;
        end loop;
	close c01;

    if  (cd_pessoa_fisica_w is null) or
        (cd_pessoa_fisica_p <> cd_pessoa_fisica_w) then
        insert into atend_profissional (
            nr_sequencia,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            nr_atendimento,
            cd_pessoa_fisica,
            ie_profissional,
            dt_inicio_vigencia,
            ds_observacao,
            nr_seq_nursing_team,
            nr_seq_phs) 
        values (
            atend_profissional_seq.nextval,
            sysdate,
            nm_usuario_p,
            sysdate,
            nm_usuario_p,
            nr_atendimento_p,
            cd_pessoa_fisica_p,
            ie_profissional_p,
            sysdate,
            null,
            nr_seq_nursing_team_p,
            nr_seq_phs_p);    
    end if;

    if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then
        commit;
    end if;
end ATUALIZAR_ATEND_PROFIS_PHS;
/