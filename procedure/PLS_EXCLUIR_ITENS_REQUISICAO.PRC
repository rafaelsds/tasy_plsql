create or replace
procedure pls_excluir_itens_requisicao
				(	nr_seq_req_proc_p	Number,
					nr_seq_req_mat_p	Number,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_req_proc_p is not null) and
	(nvl(nr_seq_req_proc_p,0)	<> 0)then
	
	delete  from pls_ocorrencia_benef
	where	nr_seq_proc = nr_seq_req_proc_p
	and	nr_seq_requisicao is not null;
	
	delete	from pls_requisicao_glosa
	where	nr_seq_req_proc		= nr_seq_req_proc_p;
	
elsif	(nr_seq_req_mat_p is not null) and
	(nvl(nr_seq_req_mat_p,0)	<> 0)then
	
	delete  from pls_ocorrencia_benef
	where	nr_seq_mat = nr_seq_req_mat_p
	and	nr_seq_requisicao is not null;
	
	delete	from pls_requisicao_glosa
	where	nr_seq_req_mat		= nr_seq_req_mat_p;
	
end if;

commit;

end pls_excluir_itens_requisicao;
/
