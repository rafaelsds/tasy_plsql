create or replace
procedure rop_inicia_contagem_inv_ajuste(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	rop_inv_ajuste_setor
set	dt_inicio_contagem	= sysdate,
	nm_usuario_inicio_cont	= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;


commit;

end rop_inicia_contagem_inv_ajuste;
/