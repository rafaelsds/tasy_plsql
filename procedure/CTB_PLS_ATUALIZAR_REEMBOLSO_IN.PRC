create or replace
procedure ctb_pls_atualizar_reembolso_in
			(	nr_seq_protocolo_p		number,
				nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nr_seq_atualizacao_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				qt_movimento_p		in out	number) is

cd_classificacao_credito_w	varchar2(255);
cd_classificacao_debito_w	varchar2(255);
nr_seq_grupo_ans_w		number(10);
ie_preco_w			varchar2(2);
ie_tipo_ato_w			varchar2(255)	:= '1';
ie_regulamentacao_w		varchar2(2);
ie_tipo_contratacao_w		varchar2(2);
ie_segmentacao_w		varchar2(2);
nr_seq_grupo_superior_w		number(10);
nr_seq_grupo_ans_ww		number(10);
ie_classif_grupo_w		varchar2(5);
ie_classif_grupo_ww		varchar2(5);
dt_referencia_w			date;
nr_seq_plano_w			number(10);
nr_seq_esquema_w		number(10);
cd_conta_credito_w		varchar2(20);
cd_conta_debito_w		varchar2(20);
cd_historico_padrao_w		number(10);
cd_historico_estorno_w		number(10);
cd_historico_baixa_w		number(10);
nr_seq_conta_w			number(10);
ie_tipo_contrato_w		varchar2(2);
ie_tipo_segurado_w		Varchar2(3);
nr_seq_conta_item_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ie_tipo_despesa_w		varchar2(1);
ds_erro_w			varchar2(4000);
ie_tipo_guia_w			varchar2(2);
nr_seq_tipo_atendimento_w	Number(10);
cd_medico_executor_w		Varchar2(10);
ie_regime_internacao_w		Varchar2(1);
nr_seq_conselho_w		Number(10);
ie_ato_cooperado_w		Varchar2(1);
ie_codificacao_w		varchar2(2);
vl_fixo_w			varchar2(30);
cd_conta_contabil_w		varchar2(20);
ie_debito_credito_w		varchar2(1);
ie_conta_glosa_w		varchar2(2);
ds_mascara_w			varchar2(40);
cd_conta_glosa_cred_w		varchar2(20);
cd_conta_glosa_deb_w		varchar2(20);
cd_conta_cred_w			varchar2(20);
cd_conta_deb_w			varchar2(20);
cd_classificacao_cred_w		varchar2(255);
cd_classificacao_deb_w		varchar2(255);
cd_classif_glosa_cred_w		varchar2(255);
cd_classif_glosa_deb_w		varchar2(255);
ie_tipo_vinculo_operadora_w	varchar2(2);
nr_seq_sca_cobertura_w		number(10);
nr_seq_contrato_w		number(10);
cd_classificacao_item_w		varchar2(30);
qt_movimento_w			number(10);
nr_seq_mot_reembolso_w		pls_protocolo_conta.nr_seq_mot_reembolso%type;
ie_tipo_repasse_w               varchar2(1);
ie_tipo_compartilhamento_w      pls_esquema_contabil.ie_tipo_compartilhamento%type;
nr_seq_segurado_w               pls_segurado.nr_sequencia%type;
ie_benef_remido_w		pls_esquema_contabil.ie_benef_remido%type;
nr_seq_congenere_w		pls_protocolo_conta.nr_seq_congenere%type;
dt_ref_repasse_w		date;
dt_repasse_w			date;
dt_fim_repasse_w		date;

Cursor c_itens is
	select	a.nr_sequencia,
		d.ie_tipo_contratacao,
		d.ie_preco,
		d.ie_segmentacao,
		d.ie_regulamentacao,
		decode(f.cd_pf_estipulante, null, 'PJ', 'PF'),
		trunc(b.dt_mes_competencia,'month'),
		nvl(c.ie_tipo_segurado,'B'),
		d.nr_sequencia,
		c.ie_tipo_vinculo_operadora,
		c.nr_seq_contrato,
		a.ie_tipo_guia,
		b.nr_seq_mot_reembolso,
                c.nr_sequencia,
		pls_obter_se_benef_remido(c.nr_sequencia,b.dt_mes_competencia) ie_benef_remido,
		b.nr_seq_congenere
	from	pls_conta 		a,
		pls_protocolo_conta	b,
		pls_segurado		c,
		pls_plano 		d,
		pls_contrato		f
	where	b.nr_sequencia	= a.nr_seq_protocolo
	and	c.nr_sequencia	= a.nr_seq_segurado
	and	d.nr_sequencia	= a.nr_seq_plano
	and	c.nr_seq_contrato = f.nr_sequencia(+)
	and	b.nr_sequencia	= nr_seq_protocolo_p
	and	nr_seq_conta_p is null
	union all
	select	a.nr_sequencia,
		d.ie_tipo_contratacao,
		d.ie_preco,
		d.ie_segmentacao,
		d.ie_regulamentacao,
		decode(f.cd_pf_estipulante, null, 'PJ', 'PF'),
		trunc(b.dt_mes_competencia,'month'),
		nvl(c.ie_tipo_segurado,'B'),
		d.nr_sequencia,
		c.ie_tipo_vinculo_operadora,
		c.nr_seq_contrato,
		a.ie_tipo_guia,
		b.nr_seq_mot_reembolso,
                c.nr_sequencia,
		pls_obter_se_benef_remido(c.nr_sequencia,b.dt_mes_competencia) ie_benef_remido,
		b.nr_seq_congenere
	from	pls_conta 		a,
		pls_protocolo_conta	b,
		pls_segurado		c,
		pls_plano 		d,
		pls_contrato		f
	where	b.nr_sequencia	= a.nr_seq_protocolo
	and	c.nr_sequencia	= a.nr_seq_segurado
	and	d.nr_sequencia	= a.nr_seq_plano
	and	c.nr_seq_contrato = f.nr_sequencia(+)
	and	a.nr_sequencia	= nr_seq_conta_p
	and	nr_seq_protocolo_p is null;

/* ie_conta_glosa_w
conforme dom�nio 3976
11 - Conta m�dica - Participa��o dos Benefici�rios em eventos/sinistros indenizados
12 - Conta m�dica - Recupera��o/Ressarcimento de Eventos
*/
Cursor c_tipo is
	select	11, --ie_conta_glosa_w decode(ie_cancelamento_w,'C',13,11)
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		null,
		nr_seq_grupo_ans,
		ie_ato_cooperado,
		nr_seq_sca_cobertura,
		nvl(dt_procedimento, dt_referencia_w) dt_ref_repasse
	from	pls_conta_proc
	where	nr_seq_conta = nr_seq_conta_w
	and	((nr_sequencia = nr_seq_conta_proc_p) or (nr_seq_conta_proc_p is null))
	union all
	select	11,
		nr_sequencia,
		null,
		null,
		ie_tipo_despesa,
		nr_seq_grupo_ans,
		ie_ato_cooperado,
		nr_seq_sca_cobertura,
		nvl(dt_atendimento, dt_referencia_w) dt_ref_repasse
	from	pls_conta_mat
	where	nr_seq_conta = nr_seq_conta_w
	and	((nr_sequencia = nr_seq_conta_mat_p) or (nr_seq_conta_mat_p is null))
	union all /* GLOSAS */
	select	12, --ie_conta_glosa_w decode(ie_cancelamento_w,'C',13,11)
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		null,
		nr_seq_grupo_ans,
		ie_ato_cooperado,
		nr_seq_sca_cobertura,
		nvl(dt_procedimento, dt_referencia_w) dt_ref_repasse
	from	pls_conta_proc
	where	nr_seq_conta = nr_seq_conta_w
	and	((nr_sequencia = nr_seq_conta_proc_p) or (nr_seq_conta_proc_p is null))
	union all
	select	12,
		nr_sequencia,
		null,
		null,
		ie_tipo_despesa,
		nr_seq_grupo_ans,
		ie_ato_cooperado,
		nr_seq_sca_cobertura,
		nvl(dt_atendimento, dt_referencia_w) dt_ref_repasse
	from	pls_conta_mat
	where	nr_seq_conta = nr_seq_conta_w
	and	((nr_sequencia = nr_seq_conta_mat_p) or (nr_seq_conta_mat_p is null));

Cursor c_esquema is
	select	nr_sequencia,
		cd_historico_padrao,
		cd_historico_estorno,
		cd_historico_baixa
	from	pls_esquema_contabil
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_regra		= 'RM'
	and	dt_referencia_w between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_referencia_w)
	and	((nr_seq_plano = nr_seq_plano_w) or (nr_seq_plano is null))
	and	((ie_tipo_segurado = ie_tipo_segurado_w) or (ie_tipo_segurado is null))
	and	ie_tipo_movimentacao = ie_conta_glosa_w
	and	((ie_tipo_vinculo_operadora = ie_tipo_vinculo_operadora_w) or (ie_tipo_vinculo_operadora is null))
	and	((nr_seq_contrato = nr_seq_contrato_w) or (nr_seq_contrato is null))
	and 	((ie_tipo_guia = ie_tipo_guia_w) or (nvl(ie_tipo_guia,0) = 0))
	and	((nr_seq_mot_reembolso = nr_seq_mot_reembolso_w) or (nr_seq_mot_reembolso is null))
        and     ((ie_tipo_repasse = ie_tipo_repasse_w) or (ie_tipo_repasse is null))
	and     ((ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento is null))
	and	((ie_benef_remido	= ie_benef_remido_w) or (ie_benef_remido is null))
        order by
		nvl(nr_seq_contrato,0),
		nvl(nr_seq_plano,0),
		nvl(ie_tipo_segurado,' '),
		nvl(ie_tipo_vinculo_operadora,' '),
		nvl(nr_seq_mot_reembolso,0),
		nvl(ie_tipo_compartilhamento,0),
		nvl(ie_tipo_repasse,' '),
		nvl(ie_benef_remido,' '),
		nvl(ie_tipo_guia,' '),
		nvl(dt_inicio_vigencia,sysdate);

Cursor c_segmentacao is
	select	ie_codificacao,
		vl_fixo,
		cd_conta_contabil,
		ie_debito_credito,
		ds_mascara
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema = nr_seq_esquema_w
	order by
		ie_debito_credito,
		nr_seq_apresentacao;

begin
qt_movimento_w	:= qt_movimento_p;

open c_itens;
loop
fetch c_itens into
	nr_seq_conta_w,
	ie_tipo_contratacao_w,
	ie_preco_w,
	ie_segmentacao_w,
	ie_regulamentacao_w,
	ie_tipo_contrato_w,
	dt_referencia_w,
	ie_tipo_segurado_w,
	nr_seq_plano_w,
	ie_tipo_vinculo_operadora_w,
	nr_seq_contrato_w,
	ie_tipo_guia_w,
	nr_seq_mot_reembolso_w,
        nr_seq_segurado_w,
	ie_benef_remido_w,
	nr_seq_congenere_w;
exit when c_itens%notfound;
	begin
	
	open c_tipo;
	loop
	fetch c_tipo into
		ie_conta_glosa_w,
		nr_seq_conta_item_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		ie_tipo_despesa_w,
		nr_seq_grupo_ans_w,
		ie_ato_cooperado_w,
		nr_seq_sca_cobertura_w,
		dt_ref_repasse_w;
	exit when c_tipo%notfound;
		begin
		/* GRUPO ANS */
		/*
		if	(nvl(nr_seq_grupo_ans_w,0) = 0) then
			select	nr_seq_tipo_atendimento,
				cd_medico_executor,
				ie_regime_internacao
			into	nr_seq_tipo_atendimento_w,
				cd_medico_executor_w,
				ie_regime_internacao_w
			from	pls_conta
			where	nr_sequencia = nr_seq_conta_w;
			
			select	max(nr_seq_conselho)
			into	nr_seq_conselho_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_medico_executor_w;
			
			select	pls_obter_grupo_ans(cd_procedimento_w, ie_origem_proced_w, nr_seq_conselho_w, 
				nr_seq_tipo_atendimento_w, ie_tipo_guia_w, ie_regime_internacao_w, 
				ie_tipo_despesa_w, 'G', nvl(cd_estabelecimento_p,0))
			into	nr_seq_grupo_ans_w
			from	dual;
		end if;
		*/
	
		pls_obter_dados_repasse(	dt_ref_repasse_w,
						nr_seq_segurado_w,
						nr_seq_congenere_w,
						ie_tipo_repasse_w,
						ie_tipo_compartilhamento_w,
						dt_repasse_w,
						dt_fim_repasse_w);
		
		open c_esquema;
		loop
		fetch c_esquema into
			nr_seq_esquema_w,
			cd_historico_padrao_w,
			cd_historico_estorno_w,
			cd_historico_baixa_w;
		exit when c_esquema%notfound;
		end loop;
		close c_esquema;

		cd_classificacao_credito_w	:= null;
		cd_classificacao_debito_w	:= null;
		cd_conta_credito_w		:= null;
		cd_conta_debito_w		:= null;
		
		
		/* 5 - Grupo ANS com base nos valores do ITAMED */
		if	(nvl(nr_seq_grupo_ans_w,0) > 0) then
			select	max(nr_seq_grupo_superior)
			into	nr_seq_grupo_superior_w
			from	ans_grupo_despesa
			where	nr_sequencia	= nr_seq_grupo_ans_w;
		end if;
		
		if	(nvl(nr_seq_grupo_superior_w,0) = 0) then
			nr_seq_grupo_ans_ww	:= nr_seq_grupo_ans_w;
		else
			nr_seq_grupo_ans_ww	:= nr_seq_grupo_superior_w;
		end if;
		
		select	max(ie_tipo_grupo_ans)
		into	nr_seq_grupo_ans_ww
		from	ans_grupo_despesa
		where	nr_sequencia	= nr_seq_grupo_ans_ww;
		
		if	(nr_seq_grupo_ans_ww = 10) then /* 1 - Consultas */
			ie_classif_grupo_w	:= '1';
			ie_classif_grupo_ww	:= '0';
		elsif	(nr_seq_grupo_ans_ww = 20) then /* 49 - Exames */
			ie_classif_grupo_w	:= '2';
			ie_classif_grupo_ww	:= '0';
		elsif	(nr_seq_grupo_ans_ww = 30) then /* 51 - Terapias */
			ie_classif_grupo_w	:= '3';
			ie_classif_grupo_ww	:= '0';
		elsif	(nr_seq_grupo_ans_ww = 41) then /* 7 - Interna��o - Honor�rio m�dico */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '1';
		elsif	(nr_seq_grupo_ans_ww = 42) then /* 8 - Interna��o - Exames */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '2';
		elsif	(nr_seq_grupo_ans_ww = 43) then /* 9 - Interna��o - Terapias*/
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '3';
		elsif	(nr_seq_grupo_ans_ww = 44) then /* 10 - Interna��o - Materiais m�dicos */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '4';
		elsif	(nr_seq_grupo_ans_ww = 45) then /* 11 - Interna��o - Medicamentos */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '5';
		elsif	(nr_seq_grupo_ans_ww = 49) then /* 12 - Interna��o - Outras despesas */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '9';
		elsif	(nr_seq_grupo_ans_ww = 50) then /* 6 - Outros atendimentos - Ambulatoriais */
			ie_classif_grupo_w	:= '5';
			ie_classif_grupo_ww	:= '0';
		elsif	(nr_seq_grupo_ans_ww = 60) then /* 16 - Demais despesas assistenciais */
			ie_classif_grupo_w	:= '6';
			ie_classif_grupo_ww	:= '0';
		end if;
		/* FIM GRUPO ANS */
		
		
		open c_segmentacao;
		loop
		fetch c_segmentacao into	
			ie_codificacao_w,
			vl_fixo_w,
			cd_conta_contabil_w,
			ie_debito_credito_w,
			ds_mascara_w;
		exit when c_segmentacao%notfound;
			begin
			cd_classificacao_item_w	:= null;
			
			if	(ie_debito_credito_w = 'C') then /* Classifica��o CR�DITO */
				if	(ie_codificacao_w = 'CR') then /* C�digo reduzido */
					select	max(cd_classificacao_atual)
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
					
					cd_conta_credito_w	:= cd_conta_contabil_w;
				elsif	(ie_codificacao_w = 'FX') then /* Fixo */
					cd_classificacao_item_w	:= vl_fixo_w;
				elsif	(ie_codificacao_w = 'FP') then /* Forma��o de Pre�o */
					if	(ie_preco_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_formacao_preco(ie_preco_w);
					else
						cd_classificacao_item_w	:= 'FP';
					end if;
				elsif	(ie_codificacao_w = 'R') then /* Regulamenta��o */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(ie_regulamentacao_w);
				elsif	(ie_codificacao_w = 'TC') then /* Tipo de contrata��o */
					if	(ie_tipo_contratacao_w in ('I','CE','CA')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_contratacao(ie_tipo_contratacao_w);
					else
						cd_classificacao_item_w	:= 'TC';
					end if;
				elsif	(ie_codificacao_w = 'TP') then /* Tipo de Contrato (Pessoa f�sica ou J�r�dica) */
					if	(ie_tipo_contrato_w in ('PF','PJ')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
					else
						cd_classificacao_item_w	:= 'TP';
					end if;
				elsif	(ie_codificacao_w = 'S') then /* Segmenta��o */
					cd_classificacao_item_w	:= lpad(ie_segmentacao_w,2,'0');
				elsif	(ie_codificacao_w = 'TA') then /* Tipo de ato cooperado */
					if	(ie_ato_cooperado_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_ato_cooperado(ie_ato_cooperado_w);
					else
						cd_classificacao_item_w	:= 'TA';
					end if;
				elsif	(ie_codificacao_w = 'GA') then /* Grupo ANS */
					if	(ie_classif_grupo_w is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_w;
					else
						cd_classificacao_item_w	:= 'GA';
					end if;
				elsif	(ie_codificacao_w = 'CG') then /* Complemento grupo ANS */
					if	(ie_classif_grupo_ww is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_ww;
					else
						cd_classificacao_item_w	:= 'CG';
					end if;
				elsif	(ie_codificacao_w = 'RC') then /* Tipo de contrata��o / Regulamenta��o */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(ie_tipo_contratacao_w,ie_regulamentacao_w);
				end if;
				
				if	(cd_classificacao_item_w is not null) then
					if	(ds_mascara_w = '00') then
						cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
					elsif	(ds_mascara_w = '0.0') then
						cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
					elsif	(ds_mascara_w = '0_') then
						cd_classificacao_item_w	:= cd_classificacao_item_w;
					else
						cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
					end if;
					
					cd_classificacao_credito_w	:= cd_classificacao_credito_w || cd_classificacao_item_w;
				end if;
			elsif	(ie_debito_credito_w = 'D') then /* Classifica��o D�BITO */
				if	(ie_codificacao_w = 'CR') then /* C�digo reduzido */
					select	max(cd_classificacao_atual)
					into	cd_classificacao_debito_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
					
					cd_conta_debito_w	:= cd_conta_contabil_w;
				elsif	(ie_codificacao_w = 'FX') then /* Fixo */
					cd_classificacao_item_w	:= vl_fixo_w;
				elsif	(ie_codificacao_w = 'FP') then /* Forma��o de Pre�o */
					if	(ie_preco_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_formacao_preco(ie_preco_w);
					else
						cd_classificacao_item_w	:= 'FP';
					end if;
				elsif	(ie_codificacao_w = 'R') then /* Regulamenta��o */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(ie_regulamentacao_w);
				elsif	(ie_codificacao_w = 'TC') then /* Tipo de contrata��o */
					if	(ie_tipo_contratacao_w in ('I','CE','CA')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_contratacao(ie_tipo_contratacao_w);
					else
						cd_classificacao_item_w	:= 'TC';
					end if;
				elsif	(ie_codificacao_w = 'S') then /* Segmenta��o */
					cd_classificacao_item_w	:= lpad(ie_segmentacao_w,2,'0');
				elsif	(ie_codificacao_w = 'TP') then /* Tipo de Contrato (Pessoa f�sica ou J�r�dica) */
					if	(ie_tipo_contrato_w in ('PF','PJ')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
					else
						cd_classificacao_item_w	:= 'TP';
					end if;	
				elsif	(ie_codificacao_w = 'TA') then /* Tipo de ato cooperado */
					if	(ie_ato_cooperado_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_ato_cooperado(ie_ato_cooperado_w);
					else
						cd_classificacao_item_w	:= 'TA';
					end if;
				elsif	(ie_codificacao_w = 'GA') then /* Grupo ANS */
					if	(ie_classif_grupo_w is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_w;
					else
						cd_classificacao_item_w	:= 'GA';
					end if;
				elsif	(ie_codificacao_w = 'CG') then /* Complemento grupo ANS */
					if	(ie_classif_grupo_ww is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_ww;
					else
						cd_classificacao_item_w	:= 'CG';
					end if;
				elsif	(ie_codificacao_w = 'RC') then /* Tipo de contrata��o / Regulamenta��o */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(ie_tipo_contratacao_w,ie_regulamentacao_w);
				end if;
				
				if	(cd_classificacao_item_w is not null) then
					if	(ds_mascara_w = '00') then
						cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
					elsif	(ds_mascara_w = '0.0') then
						cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
					elsif	(ds_mascara_w = '0_') then
						cd_classificacao_item_w	:= cd_classificacao_item_w;
					else
						cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
					end if;
					
					cd_classificacao_debito_w	:= cd_classificacao_debito_w || cd_classificacao_item_w;
				end if;
			end if;
			end;
		end loop;
		close c_segmentacao;
		
		/* Remover o �ltimo ponto da classifica��o */
		if	(substr(cd_classificacao_credito_w,length(cd_classificacao_credito_w),length(cd_classificacao_credito_w)) = '.') then
			cd_classificacao_credito_w	:= substr(cd_classificacao_credito_w,1,length(cd_classificacao_credito_w)-1);
		end if;
		
		if	(substr(cd_classificacao_debito_w,length(cd_classificacao_debito_w),length(cd_classificacao_debito_w)) = '.') then
			cd_classificacao_debito_w	:= substr(cd_classificacao_debito_w,1,length(cd_classificacao_debito_w)-1);
		end if;

		if	(cd_conta_credito_w is null) then
			cd_conta_credito_w	:= ctb_obter_conta_classif(cd_classificacao_credito_w,dt_referencia_w,cd_estabelecimento_p);
		end if;

		if	(cd_conta_debito_w is null) then
			cd_conta_debito_w	:= ctb_obter_conta_classif(cd_classificacao_debito_w,dt_referencia_w,cd_estabelecimento_p);
		end if;
		
		if	(ie_conta_glosa_w = 11) then /* DESPESA */
			if 	((cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
				if	(nr_seq_atualizacao_p is not null) then
					if	(nr_seq_esquema_w is null) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 1, null, nr_seq_conta_item_w, null, 'C', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					elsif	((cd_conta_credito_w is null) or (cd_conta_debito_w is null)) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 2, null, nr_seq_conta_item_w, null, 'C', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					end if;
				end if;
				
				update	pls_conta_proc
				set	cd_conta_cred		= cd_conta_credito_w,
					cd_conta_deb		= cd_conta_debito_w,
					cd_classif_cred		= cd_classificacao_credito_w,
					cd_classif_deb		= cd_classificacao_debito_w,
					nr_seq_esquema		= nr_seq_esquema_w,
					cd_historico		= cd_historico_padrao_w
				where	nr_sequencia		= nr_seq_conta_item_w;
				
				qt_movimento_w	:= qt_movimento_w + 1;
			elsif 	(ie_tipo_despesa_w is not null) then
				if	(nr_seq_atualizacao_p is not null) then
					if	(nr_seq_esquema_w is null) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 1, null, null, nr_seq_conta_item_w, 'C', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					elsif	((cd_conta_credito_w is null) or (cd_conta_debito_w is null)) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 2, null, null, nr_seq_conta_item_w, 'C', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					end if;
				end if;
				
				update	pls_conta_mat
				set	cd_conta_cred		= cd_conta_credito_w,
					cd_conta_deb		= cd_conta_debito_w,
					cd_classif_cred		= cd_classificacao_credito_w,
					cd_classif_deb		= cd_classificacao_debito_w,
					nr_seq_esquema		= nr_seq_esquema_w,
					cd_historico		= cd_historico_padrao_w
				where	nr_sequencia		= nr_seq_conta_item_w;
				
				qt_movimento_w	:= qt_movimento_w + 1;
			end if;
		elsif	(ie_conta_glosa_w = 12) then /* GLOSA */
			if 	((cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
				if	(nr_seq_atualizacao_p is not null) then
					if	(nr_seq_esquema_w is null) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 1, null, nr_seq_conta_item_w, null, 'G', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					elsif	((cd_conta_credito_w is null) or (cd_conta_debito_w is null)) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 2, null, nr_seq_conta_item_w, null, 'G', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					end if;
				end if;
				
				update	pls_conta_proc
				set	cd_conta_glosa_cred	= cd_conta_credito_w,
					cd_conta_glosa_deb	= cd_conta_debito_w,
					cd_classif_glosa_cred	= cd_classificacao_credito_w,
					cd_classif_glosa_deb	= cd_classificacao_debito_w,
					nr_seq_esquema_glosa	= nr_seq_esquema_w,
					cd_historico_glosa	= cd_historico_padrao_w
				where	nr_sequencia		= nr_seq_conta_item_w;
				
				qt_movimento_w	:= qt_movimento_w + 1;
			elsif 	(ie_tipo_despesa_w is not null) then
				if	(nr_seq_atualizacao_p is not null) then
					if	(nr_seq_esquema_w is null) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 1, null, null, nr_seq_conta_item_w, 'G', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					elsif	((cd_conta_credito_w is null) or (cd_conta_debito_w is null)) then
						pls_gravar_mov_contabil(nr_seq_atualizacao_p, 2, null, null, nr_seq_conta_item_w, 'G', null, null, null, null, null, null, null, null,null,
						null, null, nm_usuario_p, nr_seq_esquema_w);
					end if;
				end if;
				
				update	pls_conta_mat
				set	cd_conta_glosa_cred	= cd_conta_credito_w,
					cd_conta_glosa_deb	= cd_conta_debito_w,
					cd_classif_glosa_cred	= cd_classificacao_credito_w,
					cd_classif_glosa_deb	= cd_classificacao_debito_w,
					nr_seq_esquema_glosa	= nr_seq_esquema_w,
					cd_historico_glosa	= cd_historico_padrao_w
				where	nr_sequencia		= nr_seq_conta_item_w;
				
				qt_movimento_w	:= qt_movimento_w + 1;
			end if;
		end if;
		end;
	end loop;
	close c_tipo;
	end;
end loop;
close c_itens;

qt_movimento_p	:= qt_movimento_w;

--commit;

end ctb_pls_atualizar_reembolso_in;
/
