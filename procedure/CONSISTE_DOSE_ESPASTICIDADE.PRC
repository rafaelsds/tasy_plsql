create or replace
procedure consiste_dose_espasticidade(	cd_pessoa_fisica_p 			varchar2,
										qt_dose_p					number,
										qt_dose_prescr_p 			number,
										qt_pontos_p					number,
										nr_seq_art_mov_musculo_p 	number,
										nr_seq_toxina_p				number default null,
										ie_consistir_forma_p		varchar2 default 'N',
										nr_seq_item_p				number,
										ds_mensagem_p				out varchar2,
										ds_questiona_p				out varchar2
					) is

qt_dose_min_w		number(18,6);
qt_dose_max_w		number(18,6);
qt_peso_min_w		number(15,4);
qt_peso_max_w		number(15,4);
qt_total_dose_w		number(1);
qt_total_peso_w		number(1);
ds_mensagem_w		varchar2(255) := '';
qt_idade_w			number(5);
qt_peso_w			number(10,3);
qt_multiplo_w		number(10,2);
ie_forma_calculo_w	varchar2(3);
ie_regra_w			varchar2(1)   := 'N';
qt_dose_prescr_w 	number(10,4);

Cursor C01 is
	select	b.qt_multiplo,
		b.ie_forma_calculo,
		b.qt_dose_min,
		b.qt_dose_max,
		b.qt_pontos_min,
		b.qt_pontos_max
	from	artic_mov_musculo a,
		artic_mov_musculo_regra b
	where	a.nr_sequencia = b.nr_seq_art_mov_musculo
	and	a.nr_sequencia = nr_seq_art_mov_musculo_p
	and	qt_idade_w between b.qt_idade_min and b.qt_idade_max
	and 	qt_peso_w between nvl(b.qt_peso_min,0) and nvl(b.qt_peso_max,999)
	and	nvl(b.nr_seq_toxina,nr_seq_toxina_p) = nr_seq_toxina_p
	order by nvl(b.nr_seq_toxina,0);

begin

select	max(obter_idade(dt_nascimento,sysdate,'A'))
into	qt_idade_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

select	nvl(max(qt_peso),0)
into	qt_peso_w
from	atendimento_sinal_vital
where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
			from	atendimento_sinal_vital
			where	qt_peso is not null
			and	cd_paciente	= cd_pessoa_fisica_p
			and	ie_situacao = 'A'
			and	nvl(IE_RN,'N')	= 'N');

if	(qt_peso_w > 0) then
	begin

	open C01;
	loop
	fetch C01 into
		qt_multiplo_w,
		ie_forma_calculo_w,
		qt_dose_min_w,
		qt_dose_max_w,
		qt_peso_min_w,
		qt_peso_max_w;
	exit when C01%notfound;
		begin

		if	(qt_multiplo_w is not null) and (mod(qt_dose_p,qt_multiplo_w) > 0) then
			ds_mensagem_p := Wheb_mensagem_pck.get_texto(306299, 'QT_MULTIPLO_W='||qt_multiplo_w)||chr(13); --'Dose refer�ncia deve ser m�ltiplo de ' || qt_multiplo_w ||'.';
		end if;

		if	(qt_multiplo_w is not null) and (mod(qt_dose_prescr_p,qt_multiplo_w) > 0) then
			ds_mensagem_p := Wheb_mensagem_pck.get_texto(306299, 'QT_MULTIPLO_W='||qt_multiplo_w)||chr(13); --'Dose refer�ncia deve ser m�ltiplo de ' || qt_multiplo_w ||'.';
		end if;

		if 	(upper(ie_forma_calculo_w) = 'KG') and (ie_consistir_forma_p = 'S')then

			qt_dose_min_w	:= round(qt_peso_w)* qt_dose_min_w;
			qt_dose_max_w	:= round(qt_peso_w)* qt_dose_max_w;

		end if;

		if	((qt_dose_p < qt_dose_min_w) or (qt_dose_p > qt_dose_max_w )) and
			(qt_dose_min_w > 0) and
			(qt_dose_max_w > 0) then
			begin
			ds_mensagem_p := Wheb_mensagem_pck.get_texto(306301, 'QT_DOSE_MIN_W='||qt_dose_min_w||';QT_DOSE_MAX_W='||qt_dose_max_w) || chr(13);
			end;
		end if;	
		
		if	((nvl(qt_dose_prescr_w,0) > 0 and ((qt_dose_prescr_w < qt_dose_min_w) or (qt_dose_prescr_w > qt_dose_max_w ))) or
			((qt_dose_prescr_p < qt_dose_min_w) or (qt_dose_prescr_p > qt_dose_max_w ))) and
			(qt_dose_min_w > 0) and
			(qt_dose_max_w > 0) then
			begin
				ds_questiona_p := Wheb_mensagem_pck.get_texto(333845, 'QT_DOSE_MIN_W='||qt_dose_min_w||';QT_DOSE_MAX_W='||qt_dose_max_w) || chr(13);
			end;
		end if;
		if	((qt_pontos_p < qt_peso_min_w) or (qt_pontos_p > qt_peso_max_w )) and
			(qt_peso_min_w > 0) and
			(qt_peso_max_w > 0) then
			begin
			ds_mensagem_p := ds_mensagem_p || Wheb_mensagem_pck.get_texto(306358, 'QT_PESO_MIN_W='||qt_peso_min_w||';QT_PESO_MAX_W='||qt_peso_max_w);
			end;
		end if;
		end;
		ie_regra_w := 'S';
	end loop;
	close C01;

	end;
end if;

If 	(ie_regra_w = 'N') then
	ds_mensagem_p := ds_mensagem_p || Wheb_mensagem_pck.get_texto(306361); -- 'Nao existe regra cadastrada nessa toxina para o paciente.';
end if;

end consiste_dose_espasticidade;
/