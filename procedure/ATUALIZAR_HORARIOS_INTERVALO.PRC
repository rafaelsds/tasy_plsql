create or replace
procedure ATUALIZAR_HORARIOS_INTERVALO(
			cd_intervalo_p VARCHAR2,
			nr_sequencia_p NUMBER,
			nr_seq_prescr_p NUMBER,
			dt_hora_inicio_p date) is 
			
ie_agora_w			varchar2(10);
ie_acm_w			varchar2(10);	
ie_se_necessario_w 	varchar2(10);
hr_prim_horario_w	DATE;
dt_Hora_Inicio_w		date;
param_735_w			varchar2(10);
param_839_w			varchar2(10);
qt_horas_validade_w	NUMBER(10);
nr_intervalo_w		number(5,2); 
ds_horarios_w			varchar2(2000);
ie_necessario_w		Varchar2(1);
resul_horario_w			varchar2(2000);

begin

dt_Hora_Inicio_w  := nvl(dt_Hora_Inicio_p, to_date('30/12/1899','dd/mm/yyyy'));

obter_param_usuario(281, 735, obter_perfil_ativo, WHEB_USUARIO_PCK.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, param_735_w);
obter_param_usuario(924, 839, obter_perfil_ativo, WHEB_USUARIO_PCK.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, param_839_w);

if  (param_735_w <> 'N') then
	select	to_date(to_char(sysdate,'dd/mm/yyyy') ||':'|| Obter_primeiro_horario_sae(cd_intervalo_p, nr_seq_prescr_p),'dd/mm/yyyy hh24:mi:ss') hr_prim_horario,
		ie_se_necessario
		into hr_prim_horario_w,		
		ie_se_necessario_w
	from	INTERVALO_PRESCRICAO
	where	cd_intervalo = cd_intervalo_p;
	
	select	max(qt_horas_validade)
	into qt_horas_validade_w
	from	PE_PRESCRICAO
	where	nr_sequencia = nr_seq_prescr_p;
	
	select max(ie_acm)
	into ie_acm_w
	from PE_PRESCR_PROC
	where	nr_sequencia = nr_sequencia_p; 
	
	select max(ie_agora)
	into ie_agora_w
	from PE_PRESCR_PROC
	where	nr_sequencia = nr_sequencia_p; 

	resul_horario_w := to_char(hr_prim_horario_w, 'hh24:mi');
	calcular_horario_intervencao(cd_intervalo_p, dt_Hora_Inicio_w, NVL(qt_horas_validade_w,24),  nr_intervalo_w, ds_horarios_w, resul_horario_w , ie_necessario_w,nr_seq_prescr_p);
		
		
	UPDATE PE_PRESCR_PROC
	SET DS_HORARIOS = ds_horarios_w,
		DT_PRIMEIRO_HORARIO = hr_prim_horario_w,
		HR_PRIM_HORARIO = to_char(hr_prim_horario_w, 'hh24:mi')
	where	nr_sequencia = nr_sequencia_p; 
	
	if (ie_agora_w = 'S') then
		UPDATE PE_PRESCR_PROC
		SET IE_AGORA = 'N'
		where	nr_sequencia_p = nr_sequencia_p; 
			end if;
		
end if;	

if (param_839_w = 'S') and (ie_acm_w <> 'S') or (ie_agora_w <> 'S') and (ie_se_necessario_w = 'S')  then
	update PE_PRESCR_PROC
	set IE_SE_NECESSARIO = ie_se_necessario_w,
		IE_ACM = 'N',
		IE_AGORA = 'N'
	where	nr_sequencia = nr_sequencia_p; 
	
		

end if;

atualizar_prescr_item_sae(nr_seq_prescr_p, nr_sequencia_p, WHEB_USUARIO_PCK.get_nm_usuario);
	
end ATUALIZAR_HORARIOS_INTERVALO;
/
