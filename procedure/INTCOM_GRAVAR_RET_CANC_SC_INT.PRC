create or replace
procedure intcom_gravar_ret_canc_sc_int(	nr_seq_registro_p		number,
					ie_status_p		varchar2,
					ie_sistema_origem_p	varchar2,
					ds_erro_p			varchar2,
					ds_retorno_p		varchar2,
					ds_xml_p			long,
					nm_usuario_p		Varchar2) is 
qt_existe_w		number(10);
ie_tipo_operacao_w	varchar2(30);
nr_solic_compra_w		number(10);
nr_ordem_compra_w		number(10);

begin

select	count(*)
into	qt_existe_w
from	registro_integr_com_xml
where	nr_seq_registro	= nr_seq_registro_p
and	ie_operacao	= 'E'
and	ie_status		= 'NP';

if	(qt_existe_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(173194);
end if;

select	nvl(max(nr_solic_compra),0)
into	nr_solic_compra_w
from	registro_integr_compras
where	nr_sequencia = nr_seq_registro_p;

select	nvl(max(nr_ordem_compra),0)
into	nr_ordem_compra_w
from	registro_integr_compras
where	nr_sequencia = nr_seq_registro_p;

select	nvl(max(ie_tipo_operacao),'')
into	ie_tipo_operacao_w
from	registro_integr_compras
where	nr_sequencia = nr_seq_registro_p;

insert into registro_integr_com_xml(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_registro,
	ie_status,
	ie_operacao,
	ie_sistema_origem,
	ds_erro,
	ds_retorno,
	ds_xml,
	ie_tipo_operacao)
values(	registro_integr_com_xml_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_registro_p,
	ie_status_p,
	'R',
	ie_sistema_origem_p,
	ds_erro_p,
	ds_retorno_p,
	ds_xml_p,
	ie_tipo_operacao_w);
	
if	(ie_status_p = 'P') then	
		
	if	(nr_ordem_compra_w > 0) then
	
		insert into ordem_compra_hist(	
			nr_sequencia,
			nr_ordem_compra,
			dt_atualizacao,
			nm_usuario,
			dt_historico,
			ds_titulo,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo,
			dt_liberacao)
		values(	ordem_compra_hist_seq.nextval,
			nr_ordem_compra_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			WHEB_MENSAGEM_PCK.get_texto(280917),
			WHEB_MENSAGEM_PCK.get_texto(280919),
			sysdate,
			nm_usuario_p,
			'S',
			sysdate);
	elsif	(nr_solic_compra_w > 0) then
		insert into solic_compra_hist(	
			nr_sequencia,
			nr_solic_compra,
			dt_atualizacao,
			nm_usuario,
			dt_historico,
			ds_titulo,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo,
			cd_evento,
			dt_liberacao)
		values(	solic_compra_hist_seq.nextval,
			nr_solic_compra_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			WHEB_MENSAGEM_PCK.get_texto(280920),
			WHEB_MENSAGEM_PCK.get_texto(280921),
			sysdate,
			nm_usuario_p,
			'S',
			'V',
			sysdate);
	end if;
	
elsif	(ie_status_p = 'E') then

	if	(nr_ordem_compra_w > 0) then	
		insert into ordem_compra_hist(	
			nr_sequencia,
			nr_ordem_compra,
			dt_atualizacao,
			nm_usuario,
			dt_historico,
			ds_titulo,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo,
			dt_liberacao)
		values(	ordem_compra_hist_seq.nextval,
			nr_ordem_compra_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			WHEB_MENSAGEM_PCK.get_texto(280923),
			WHEB_MENSAGEM_PCK.get_texto(280924) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(280925) || ds_erro_p,
			sysdate,
			nm_usuario_p,
			'S',
			sysdate);
	elsif	(nr_solic_compra_w > 0) then
		insert into solic_compra_hist(	
			nr_sequencia,
			nr_solic_compra,
			dt_atualizacao,
			nm_usuario,
			dt_historico,
			ds_titulo,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo,
			cd_evento,
			dt_liberacao)
		values(	solic_compra_hist_seq.nextval,
			nr_solic_compra_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			WHEB_MENSAGEM_PCK.get_texto(280926),
			WHEB_MENSAGEM_PCK.get_texto(280927) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(280925) || ds_erro_p,
			sysdate,
			nm_usuario_p,
			'S',
			'V',
			sysdate);
	
	end if;
end if;

update	registro_integr_com_xml
set	ie_status	= ie_status_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_seq_registro = nr_seq_registro_p
and	ie_operacao	= 'E';

commit;

end intcom_gravar_ret_canc_sc_int;
/
