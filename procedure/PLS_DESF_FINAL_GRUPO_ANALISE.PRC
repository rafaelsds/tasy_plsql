create or replace
procedure pls_desf_final_grupo_analise(	nr_seq_analise_p		pls_conta.nr_seq_analise%type,
					nr_seq_grupo_p			pls_auditoria_conta_grupo.nr_seq_grupo%type,
					nr_seq_aud_conta_grupo_p	pls_auditoria_conta_grupo.nr_sequencia%type,
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_permite_em_auditoria_p	varchar2) is

nm_auditor_atual_w		varchar2(255);
ie_status_w			varchar2(1);
nr_seq_conta_w			number(10);
nr_seq_analise_w		number(10);
nr_seq_aud_conta_grupo_w	number(10);
nr_seq_ordem_w			number(10);
nr_seq_grupo_w			number(10);
nr_seq_protocolo_w		number(10);
nr_seq_fatura_w			number(10);
cont_w				pls_integer;
qt_coparticipacao_mens_w	pls_integer;
qt_conta_medica_resumo_w	pls_integer;
nr_lote_contabil_w		number(10);
nr_lote_contabil_prov_w		number(10);
nr_lote_contab_pag_w		number(10);
nr_lote_prov_copartic_w		number(10);
qt_lote_fat_w			pls_integer;
nr_seq_lote_recalculo_w		number(10);
nr_seq_conta_rec_w		pls_rec_glosa_conta.nr_sequencia%type;
qt_conta_enviada_ans_w		pls_integer;

/* IE_PERMITE_AUDITORIA_P
Criado esse par�metro para permitir reabrir um grupo no fluxo de forma automatizada
Ou seja, n�o ir� impedir se tem grupos com fluxo maior j� finalizado ou se houver grupos
j� auditando
*/

Cursor C01(	nr_seq_analise_pc	pls_conta.nr_seq_analise%type) is
	select	nr_sequencia,
		ie_status
	from	pls_conta
	where	nr_seq_analise = nr_seq_analise_pc
	order by 1;
	
Cursor C02(	nr_seq_analise_pc	pls_conta.nr_seq_analise%type) is
	select	nr_sequencia
	from	pls_conta
	where	nr_seq_analise = nr_seq_analise_pc
	and	ie_status = 'F';

begin
/*Cursor para verificar se existe algo que imposs�bilite reabrir a auditoria para evitar que contas com resumo gerados sejam alterados ap�s a gera��o destes. OS 480597 Diogo*/
if	(nr_seq_aud_conta_grupo_p is not null) then

	select	nr_seq_analise,
		nr_seq_grupo,
		nr_seq_ordem
	into	nr_seq_analise_w,
		nr_seq_grupo_w,
		nr_seq_ordem_w
	from	pls_auditoria_conta_grupo
	where	nr_sequencia = nr_seq_aud_conta_grupo_p;
end if;

for r_c02_w in C02(nvl(nr_seq_analise_p,nr_seq_analise_w)) loop
	begin
	pls_desfazer_fechamento_conta(r_c02_w.nr_sequencia,cd_estabelecimento_p,nm_usuario_p,
					'A');
	end;
end loop;

open C01(nvl(nr_seq_analise_p,nr_seq_analise_w));
loop
fetch C01 into	
	nr_seq_conta_w,
	ie_status_w;
exit when C01%notfound;
	begin
	nr_seq_protocolo_w		:= null;
	nr_seq_fatura_w			:= null;
	nr_lote_contabil_w		:= null;
	nr_lote_contabil_prov_w		:= null;
	nr_lote_contab_pag_w		:= null;
	nr_lote_prov_copartic_w		:= null;
	
	/*Se houver uma conta fechada o auditor j� n�o pode reabrir o processo*/
	if	(ie_status_w = 'F') then
		--Esta an�lise possui contas fechadas. Processo abortado.
		wheb_mensagem_pck.exibir_mensagem_abort(183534);
	end if;
	
	select	count(1)
	into	qt_conta_enviada_ans_w
	from	sip_nv_dados a
	where	a.ie_conta_enviada_ans = 'S'
	and	a.nr_seq_conta = nr_seq_conta_w
	and	exists(	select	1
			from	pls_lote_sip b
			where	b.nr_sequencia = a.nr_seq_lote_sip
			and	b.dt_envio is not null);
	
	if	(qt_conta_enviada_ans_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(337915);
	end if;
	
	select	max(nr_seq_protocolo),
		max(nr_seq_fatura)
	into	nr_seq_protocolo_w,
		nr_seq_fatura_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_w;

	select	count(1)
	into	cont_w
	from	pls_prot_conta_titulo	a,
		pls_protocolo_conta	b,
		pls_conta		c
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	b.nr_sequencia		= c.nr_seq_protocolo
	and	c.nr_sequencia		= nr_seq_conta_w;
	
	select	count(1)
	into	qt_coparticipacao_mens_w
	from	pls_conta_coparticipacao
	where	nr_seq_conta = nr_seq_conta_w
	and	nr_seq_mensalidade_seg	is not null;
	
	/*Caso tiver pagamento de produ��o para a conta n�o pode desfazer o fechamento*/
	select	sum(qt)
	into	qt_conta_medica_resumo_w
	from	(
		select	count(1) qt
		from	pls_conta_medica_resumo
		where	nr_seq_conta = nr_seq_conta_w
		and	nr_seq_lote_pgto is not null
		and	((ie_situacao != 'I') or (ie_situacao is null))
		union all
		select	count(1) qt
		from	pls_conta_medica_resumo
		where	nr_seq_conta = nr_seq_conta_w
		and	nr_seq_pp_lote is not null
		and	ie_situacao = 'A'
	);

	select	max(nr_lote_contabil),
		max(nr_lote_contabil_prov),
		max(nr_lote_contab_pag),
		max(nr_lote_prov_copartic)
	into	nr_lote_contabil_w,
		nr_lote_contabil_prov_w,
		nr_lote_contab_pag_w,
		nr_lote_prov_copartic_w
	from	pls_protocolo_conta
	where	nr_sequencia	= nr_seq_protocolo_w;
		
	select	count(1)	
	into	qt_lote_fat_w
	from	pls_conta_pos_estabelecido 
	where	nr_seq_conta	= nr_seq_conta_w
	and	nr_seq_lote_fat is not null
	and	((ie_situacao	= 'A') or (ie_situacao	is null));
		
	if	(qt_lote_fat_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190837);
	end if;
	
	if	(qt_conta_medica_resumo_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190838);
	end if;

	if	(qt_coparticipacao_mens_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190839);
	end if;

	if 	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190841);
	end if;

	/* Lepinski - OSs 389180 e 381715 - Conforme conversa com Adriano, n�o permitir desfazer o fechamento da conta, caso a mesma j� esteja em lote de contabiliza��o */
	if	(nvl(nr_lote_contabil_w,0) <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190842,'NR_LOTE_CONTABIL_W='||nr_lote_contabil_w);
	end if;
	if	(nvl(nr_lote_contab_pag_w,0) <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190845,'NR_LOTE_CONTABIL_W='||nr_lote_contab_pag_w);
	end if;
	/* Edgar 28/11/2013, OS 664025, cfme conversado com Adriano, n�o h� necessidade de consistir o lote de provis�o de produ��o m�dica
	if	(nvl(nr_lote_contabil_prov_w,0) <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190847,'NR_LOTE_CONTABIL_W='||nr_lote_contabil_prov_w);
	end if;
	*/
	if	(nvl(nr_lote_prov_copartic_w,0) <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190849,'NR_LOTE_CONTABIL_W='||nr_lote_prov_copartic_w);
	end if;

	/* Felipe - 28/07/2011 - OS 338196 - N�o permitir alterar protocolos que estejam em lote de recalculo n�o liberado*/
	select	nvl(max(b.nr_sequencia),0)
	into	nr_seq_lote_recalculo_w
	from	pls_lote_recalculo	b,
		pls_conta_recalculo	a
	where	a.nr_seq_lote		= b.nr_sequencia
	and	a.nr_seq_protocolo	= nr_seq_protocolo_w
	and	b.dt_geracao_lote is not null
	and	b.dt_aplicacao is null;

	if 	(nr_seq_lote_recalculo_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(190850,'NR_SEQ_LOTE_RECALCULO_W='||nr_seq_lote_recalculo_w);
	end if;
	end;
end loop;
close C01;

if	(nr_seq_analise_w is null) or
	(nr_seq_grupo_w is null) then
	select	nr_seq_analise,
		nr_seq_grupo
	into	nr_seq_analise_w,
		nr_seq_grupo_w
	from	pls_auditoria_conta_grupo
	where	(nr_seq_grupo 		= nr_seq_grupo_p and nr_seq_analise = nr_seq_analise_p)
	or	(nr_sequencia		= nr_seq_aud_conta_grupo_p);
end if;

if	(nr_seq_ordem_w is null) then
	select	max(nr_seq_ordem)
	into	nr_seq_ordem_w
	from	pls_auditoria_conta_grupo
	where	nr_sequencia = nr_seq_aud_conta_grupo_p;
end if;

select	max(nr_sequencia)
into	nr_seq_aud_conta_grupo_w
from	pls_auditoria_conta_grupo
where	nr_seq_ordem   > nr_seq_ordem_w
and	nr_seq_analise = nr_seq_analise_w
and	dt_liberacao is not null;

if	(nvl(nr_seq_aud_conta_grupo_w,0) > 0) and
	(nvl(ie_permite_em_auditoria_p,'N') = 'N') then
	--N�o � poss�vel desfazer a finaliza��o da an�lise deste grupo enquanto houver grupos superiores liberados.
	wheb_mensagem_pck.exibir_mensagem_abort(183535);	
end if;

select	max(nm_auditor_atual)
into	nm_auditor_atual_w
from	pls_auditoria_conta_grupo
where	nr_seq_ordem   > nr_seq_ordem_w
and	nr_seq_analise = nr_seq_analise_w
and	dt_liberacao is null
and	nvl(nm_auditor_atual,'X') <> 'X';

if	(nvl(nm_auditor_atual_w,'X') <> 'X') and
	(nvl(ie_permite_em_auditoria_p,'N') = 'N') then	
	--N�o � poss�vel desfazer a finaliza��o da an�lise deste grupo pois o grupo atual esta em processo de auditoria.	
	wheb_mensagem_pck.exibir_mensagem_abort(183597);
end if;

update	pls_auditoria_conta_grupo
set	dt_liberacao		= null,
	nm_auditor_atual	= null,
	dt_inicio_auditor	= null,
	dt_atualizacao		= sysdate,
	dt_final_analise	= null, /* OS 372956  - 26/01/2012*/
	nm_usuario		= nm_usuario_p
where	(nr_seq_grupo 		= nr_seq_grupo_p and nr_seq_analise = nr_seq_analise_p)
or	(nr_sequencia		= nr_seq_aud_conta_grupo_p);

pls_inserir_hist_analise(null, nvl(nr_seq_analise_p, nr_seq_analise_w), 24,
			null, 'A', null,
			null, 'Desfeita a finaliza��o da an�lise do grupo ' || pls_obter_nome_grupo_auditor(nr_seq_grupo_w) || ' (Ordem: ' || nr_seq_ordem_w || ') pelo usu�rio ' ||
			pls_Obter_Nome_Usuario(nm_usuario_p) || '.', null,
			nm_usuario_p, cd_estabelecimento_p);

if	(nr_seq_fatura_w is not null) then

	begin
		select	  g.ie_status
		into	  ie_status_w
		from      ptu_fatura g
		where     g.nr_sequencia = nr_seq_fatura_w;                
	exception
	when others then
		ie_status_w := '';
	end;

	if	(ie_status_w = 'CA') then	
		--A fatura da an�lise #@NR_SEQ_ANALISE#@ est� cancelada. N�o � possivel desfazer a finaliza��o dos grupos.
		wheb_mensagem_pck.exibir_mensagem_abort(183602,  'NR_SEQ_ANALISE='||nr_seq_analise_p);	
	elsif	(ie_status_w = 'E') then	
		--A fatura da an�lise #@NR_SEQ_ANALISE#@ est� encerrada. N�o � possivel desfazer a finaliza��o dos grupos.	
		wheb_mensagem_pck.exibir_mensagem_abort(183603, 'NR_SEQ_ANALISE='||nr_seq_analise_p);
	end if;

	ptu_atualizar_status_fatura(nr_seq_fatura_w, 'A', null, nm_usuario_p);
end if;
		
/*Realizado o update para que caso a an�lise estive-se concluida*/
update	pls_analise_conta
set	dt_liberacao_analise	= null,
	dt_final_analise	= null,	
	ie_status		= 'A',
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia 		= nvl(nr_seq_analise_p, nr_seq_analise_w);

pls_atualizar_grupo_penden(nvl(nr_seq_analise_p,nr_seq_analise_w), cd_estabelecimento_p, nm_usuario_p);

-- Atualizar status da an�lise de recurso de glosa
if	(nr_seq_analise_p is not null) or
	(nr_seq_analise_w is not null) then
	select	max(nr_sequencia)
	into	nr_seq_conta_rec_w
	from	pls_rec_glosa_conta
	where	nr_seq_analise	= nr_seq_analise_p;
	
	if	(nr_seq_conta_rec_w is null) then
		select	max(nr_sequencia)
		into	nr_seq_conta_rec_w
		from	pls_rec_glosa_conta
		where	nr_seq_analise	= nr_seq_analise_w;
	end if;
	
	if	(nr_seq_conta_rec_w is not null) then
		pls_reabrir_rec_glosa_conta( nr_seq_conta_rec_w, null, nm_usuario_p);
	end if;
end if;

/*commit;*/

end pls_desf_final_grupo_analise;
/