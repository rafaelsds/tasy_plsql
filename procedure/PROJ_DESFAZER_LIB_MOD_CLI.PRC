create or replace
procedure proj_desfazer_lib_mod_cli(nr_sequencia_p		number) is 

begin				

update	proj_cron_etapa
set	dt_liberacao_etapa = null
where	nr_sequencia = nr_sequencia_p;

commit;

end proj_desfazer_lib_mod_cli;
/
