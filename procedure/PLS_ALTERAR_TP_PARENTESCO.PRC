create or replace
procedure pls_alterar_tp_parentesco
			(	nr_seq_segurado_p	number,
				ie_tipo_parentesco_p	varchar2,
				nm_usuario_p		varchar2) is

ie_tipo_parentesco_antec_w	varchar2(3);
cd_estabelecimento_w		pls_segurado.cd_estabelecimento%type;

begin

if	(ie_tipo_parentesco_p is not null) then
	select	ie_tipo_parentesco,
		cd_estabelecimento
	into	ie_tipo_parentesco_antec_w,
		cd_estabelecimento_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
	
	if	(nvl(ie_tipo_parentesco_antec_w,'X') <> ie_tipo_parentesco_p) then
		update	pls_segurado
		set	ie_tipo_parentesco	= ie_tipo_parentesco_p,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_segurado_p;
		
		pls_preco_beneficiario_pck.gravar_preco_benef(nr_seq_segurado_p, 'T', 'S', sysdate, 'N', null, nm_usuario_p, cd_estabelecimento_w);
		
		pls_preco_beneficiario_pck.atualizar_preco_beneficiarios(nr_seq_segurado_p, null, null, null, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_w);
				
		pls_gerar_segurado_historico(	nr_seq_segurado_p, '69', sysdate, 'De: ' || obter_valor_dominio(1885, ie_tipo_parentesco_antec_w)  || ' Para: ' || obter_valor_dominio(1885, ie_tipo_parentesco_p),
						'pls_alterar_tp_parentesco', null, null, null,
						null, sysdate, null, null,
						null, null, null, null,
						nm_usuario_p, 'N');				
	end if;
end if;

commit;

end pls_alterar_tp_parentesco;
/