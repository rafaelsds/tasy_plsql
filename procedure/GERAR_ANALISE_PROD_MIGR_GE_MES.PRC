create or replace
procedure gerar_analise_prod_migr_ge_mes is

nr_seq_analise_mes_w	number(10,0);

qt_os_colab_inic_w	number(10,0);
qt_os_colab_proj_inic_w	number(10,0);
qt_os_colab_rev_inic_w	number(10,0);
qt_os_colab_val_inic_w	number(10,0);

qt_os_colab_w		number(10,0);
qt_os_colab_proj_w	number(10,0);
qt_os_colab_rev_w	number(10,0);
qt_os_colab_val_w	number(10,0);

qt_os_colab_60_inic_w	number(10,0);
qt_os_colab_45_inic_w	number(10,0);
qt_os_colab_30_inic_w	number(10,0);
qt_os_colab_15_inic_w	number(10,0);
qt_os_colab_07_inic_w	number(10,0);
qt_os_colab_sem_inic_w	number(10,0);

qt_os_colab_60_w	number(10,0);
qt_os_colab_45_w	number(10,0);
qt_os_colab_30_w	number(10,0);
qt_os_colab_15_w	number(10,0);
qt_os_colab_07_w	number(10,0);
qt_os_colab_sem_w	number(10,0);

qt_hora_colab_w		number(15,4);

qt_hora_os_w		number(15,4);
qt_hora_os_proj_w	number(15,4);
qt_hora_os_proj_terc_w	number(15,4);
qt_hora_os_rev_w	number(15,4);
qt_hora_os_rev_terc_w	number(15,4);
qt_hora_os_nec_w	number(15,4);
qt_hora_os_nec_terc_w	number(15,4);
qt_hora_os_dep_w	number(15,4);
qt_hora_os_dep_terc_w	number(15,4);

begin
if	(1 = 1) then
	begin
	/* obter an�lise m�s / geral - caso existir */
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_analise_mes_w
	from	w_analise_prod_migr_colab
	where	trunc(dt_analise,'month') = trunc(sysdate,'month')
	and	ie_analise = 'EM';
	
	/* gerar an�lise m�s / geral - caso n�o existir */
	if	(nr_seq_analise_mes_w = 0) then
		begin
		select	w.qt_os_colab_inic,
			w.qt_os_colab_proj_inic,
			w.qt_os_colab_rev_inic,
			w.qt_os_colab_val_inic,			
			w.qt_os_colab_60_inic,
			w.qt_os_colab_45_inic,
			w.qt_os_colab_30_inic,
			w.qt_os_colab_15_inic,
			w.qt_os_colab_07_inic,
			w.qt_os_colab_sem_inic
		into	qt_os_colab_inic_w,
			qt_os_colab_proj_inic_w,
			qt_os_colab_rev_inic_w,
			qt_os_colab_val_inic_w,			
			qt_os_colab_60_inic_w,
			qt_os_colab_45_inic_w,
			qt_os_colab_30_inic_w,
			qt_os_colab_15_inic_w,
			qt_os_colab_07_inic_w,
			qt_os_colab_sem_inic_w
		from	w_analise_prod_migr_colab w
		where	w.nr_sequencia = (
				select	min(x.nr_sequencia)
				from	w_analise_prod_migr_colab x
				where	trunc(x.dt_analise,'month') = trunc(sysdate,'month')
				and	x.ie_analise = 'E');
				
		select	w.qt_os_colab,
			w.qt_os_colab_proj,
			w.qt_os_colab_rev,
			w.qt_os_colab_val,
			w.qt_os_colab_60,
			w.qt_os_colab_45,
			w.qt_os_colab_30,
			w.qt_os_colab_15,
			w.qt_os_colab_07,
			w.qt_os_colab_sem
		into	qt_os_colab_w,
			qt_os_colab_proj_w,
			qt_os_colab_rev_w,
			qt_os_colab_val_w,
			qt_os_colab_60_w,
			qt_os_colab_45_w,
			qt_os_colab_30_w,
			qt_os_colab_15_w,
			qt_os_colab_07_w,
			qt_os_colab_sem_w
		from	w_analise_prod_migr_colab w
		where	w.nr_sequencia = (
				select	max(x.nr_sequencia)
				from	w_analise_prod_migr_colab x
				where	trunc(x.dt_analise,'month') = trunc(sysdate,'month')
				and	x.ie_analise = 'E');

		select	sum(w.qt_os_colab_sem) qt_os_colab_sem,
			sum(w.qt_hora_colab) qt_hora_colab,
			sum(w.qt_hora_os) qt_hora_os,
			sum(w.qt_hora_os_proj) qt_hora_os_proj,
			sum(w.qt_hora_os_proj_terc) qt_hora_os_proj_terc,
			sum(w.qt_hora_os_rev) qt_hora_os_rev,
			sum(w.qt_hora_os_rev_terc) qt_hora_os_rev_terc,
			sum(w.qt_hora_os_nec) qt_hora_os_nec,
			sum(w.qt_hora_os_nec_terc) qt_hora_os_nec_terc,
			sum(w.qt_hora_os_dep) qt_hora_os_dep,
			sum(w.qt_hora_os_dep_terc) qt_hora_os_dep_terc
		into	qt_os_colab_sem_w,
			qt_hora_colab_w,
			qt_hora_os_w,
			qt_hora_os_proj_w,
			qt_hora_os_proj_terc_w,
			qt_hora_os_rev_w,
			qt_hora_os_rev_terc_w,
			qt_hora_os_nec_w,
			qt_hora_os_nec_terc_w,
			qt_hora_os_dep_w,
			qt_hora_os_dep_terc_w
		from	w_analise_prod_migr_colab w
		where	trunc(w.dt_analise,'month') = trunc(sysdate,'month')
		and	w.ie_analise = 'E';
		
		insert into w_analise_prod_migr_colab (
			nr_sequencia,
			dt_analise,
			ie_analise,
			nm_usuario_analise,
			nr_seq_grupo_des,
			qt_os_colab_inic,
			qt_os_colab_proj_inic,
			qt_os_colab_rev_inic,
			qt_os_colab_val_inic,			
			qt_os_colab,
			qt_os_colab_proj,
			qt_os_colab_rev,
			qt_os_colab_val,			
			qt_os_colab_60_inic,
			qt_os_colab_45_inic,
			qt_os_colab_30_inic,
			qt_os_colab_15_inic,
			qt_os_colab_07_inic,
			qt_os_colab_sem_inic,
			qt_os_colab_60,
			qt_os_colab_45,
			qt_os_colab_30,
			qt_os_colab_15,
			qt_os_colab_07,
			qt_os_colab_sem,
			qt_hora_colab,
			qt_hora_os,
			qt_hora_os_proj,
			qt_hora_os_proj_terc,
			qt_hora_os_rev,
			qt_hora_os_rev_terc,
			qt_hora_os_nec,
			qt_hora_os_nec_terc,
			qt_hora_os_dep,
			qt_hora_os_dep_terc)
		values (
			w_analise_prod_migr_colab_seq.nextval,
			trunc(sysdate,'month'),
			'EM',
			'Rafael',
			57,
			qt_os_colab_inic_w,
			qt_os_colab_proj_inic_w,
			qt_os_colab_rev_inic_w,
			qt_os_colab_val_inic_w,
			qt_os_colab_w,
			qt_os_colab_proj_w,
			qt_os_colab_rev_w,
			qt_os_colab_val_w,
			qt_os_colab_60_inic_w,
			qt_os_colab_45_inic_w,
			qt_os_colab_30_inic_w,
			qt_os_colab_15_inic_w,
			qt_os_colab_07_inic_w,
			qt_os_colab_sem_inic_w,
			qt_os_colab_60_w,
			qt_os_colab_45_w,
			qt_os_colab_30_w,
			qt_os_colab_15_w,
			qt_os_colab_07_w,
			qt_os_colab_sem_w,			
			qt_hora_colab_w,
			qt_hora_os_w,
			qt_hora_os_proj_w,
			qt_hora_os_proj_terc_w,
			qt_hora_os_rev_w,
			qt_hora_os_rev_terc_w,
			qt_hora_os_nec_w,
			qt_hora_os_nec_terc_w,
			qt_hora_os_dep_w,
			qt_hora_os_dep_terc_w);
		end;
	else
		begin
		select	w.qt_os_colab,
			w.qt_os_colab_proj,
			w.qt_os_colab_rev,
			w.qt_os_colab_val,
			w.qt_os_colab_60,
			w.qt_os_colab_45,
			w.qt_os_colab_30,
			w.qt_os_colab_15,
			w.qt_os_colab_07,
			w.qt_os_colab_sem
		into	qt_os_colab_w,
			qt_os_colab_proj_w,
			qt_os_colab_rev_w,
			qt_os_colab_val_w,
			qt_os_colab_60_w,
			qt_os_colab_45_w,
			qt_os_colab_30_w,
			qt_os_colab_15_w,
			qt_os_colab_07_w,
			qt_os_colab_sem_w
		from	w_analise_prod_migr_colab w
		where	w.nr_sequencia = (
				select	max(x.nr_sequencia)
				from	w_analise_prod_migr_colab x
				where	trunc(x.dt_analise,'month') = trunc(sysdate,'month')
				and	x.ie_analise = 'E');

		select	sum(w.qt_os_colab_sem) qt_os_colab_sem,
			sum(w.qt_hora_colab) qt_hora_colab,
			sum(w.qt_hora_os) qt_hora_os,
			sum(w.qt_hora_os_proj) qt_hora_os_proj,
			sum(w.qt_hora_os_proj_terc) qt_hora_os_proj_terc,
			sum(w.qt_hora_os_rev) qt_hora_os_rev,
			sum(w.qt_hora_os_rev_terc) qt_hora_os_rev_terc,
			sum(w.qt_hora_os_nec) qt_hora_os_nec,
			sum(w.qt_hora_os_nec_terc) qt_hora_os_nec_terc,
			sum(w.qt_hora_os_dep) qt_hora_os_dep,
			sum(w.qt_hora_os_dep_terc) qt_hora_os_dep_terc
		into	qt_os_colab_sem_w,
			qt_hora_colab_w,
			qt_hora_os_w,
			qt_hora_os_proj_w,
			qt_hora_os_proj_terc_w,
			qt_hora_os_rev_w,
			qt_hora_os_rev_terc_w,
			qt_hora_os_nec_w,
			qt_hora_os_nec_terc_w,
			qt_hora_os_dep_w,
			qt_hora_os_dep_terc_w
		from	w_analise_prod_migr_colab w
		where	trunc(w.dt_analise,'month') = trunc(sysdate,'month')
		and	w.ie_analise = 'E';
		
		update	w_analise_prod_migr_colab
		set	qt_os_colab = qt_os_colab_w,
			qt_os_colab_proj = qt_os_colab_proj_w,
			qt_os_colab_rev = qt_os_colab_rev_w,
			qt_os_colab_val = qt_os_colab_val,
			qt_os_colab_60 = qt_os_colab_60_w,
			qt_os_colab_45 = qt_os_colab_45_w,
			qt_os_colab_30 = qt_os_colab_30_w,
			qt_os_colab_15 = qt_os_colab_15_w,
			qt_os_colab_07 = qt_os_colab_07_w,
			qt_os_colab_sem = qt_os_colab_sem_w,
			qt_hora_colab = qt_hora_colab_w,
			qt_hora_os = qt_hora_os_w,
			qt_hora_os_proj = qt_hora_os_proj_w,
			qt_hora_os_proj_terc = qt_hora_os_proj_terc_w,
			qt_hora_os_rev = qt_hora_os_rev_w,
			qt_hora_os_rev_terc = qt_hora_os_rev_terc_w,
			qt_hora_os_nec = qt_hora_os_nec_w,
			qt_hora_os_nec_terc = qt_hora_os_nec_terc_w,
			qt_hora_os_dep = qt_hora_os_dep_w,
			qt_hora_os_dep_terc = qt_hora_os_dep_terc_w
		where	nr_sequencia = nr_seq_analise_mes_w;
		end;
	end if;	
	end;
end if;
commit;
end gerar_analise_prod_migr_ge_mes;
/