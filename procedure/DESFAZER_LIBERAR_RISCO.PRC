create or replace
procedure desfazer_liberar_risco(
			nr_sequencia_p		number) is 

begin
update	proj_risco_implantacao 
set	dt_liberacao   = null
where	nr_sequencia   = nr_sequencia_p;

commit;

end desfazer_liberar_risco;
/