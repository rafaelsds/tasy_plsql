create or replace
procedure liberar_orcamento_historico ( nm_usuario_p varchar2,
					nr_sequencia_p number ) is 
begin

if	(nr_sequencia_p is not null) then
	begin
	update	orcamento_historico     
	set     dt_liberacao = sysdate,
		nm_usuario = nm_usuario_p, 
		dt_atualizacao = sysdate
	where   nr_sequencia  = nr_sequencia_p;
	commit;
	end;
end if;

end liberar_orcamento_historico;
/
