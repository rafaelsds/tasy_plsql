create or replace procedure silab_integra_titulo_receber is

cd_portador_w			parametro_contas_receber.cd_portador%type;
cd_tipo_portador_w		parametro_contas_receber.cd_tipo_portador%type;
pr_juro_padrao_w		parametro_contas_receber.pr_juro_padrao%type;
pr_multa_padrao_w		parametro_contas_receber.pr_multa_padrao%type;
cd_tipo_taxa_juro_w		parametro_contas_receber.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_w	parametro_contas_receber.cd_tipo_taxa_multa%type;
cd_moeda_padrao_w		parametro_contas_receber.cd_moeda_padrao%type;
nr_titulo_novo_w		titulo_receber.nr_titulo%type;
ie_gerar_classif_w		varchar2(1);
dt_emissao_w			date;

e_erro					exception;
ds_erro_w				varchar2(2000);
ds_msg_err_w			varchar2(255);

Cursor C01 is
	select	nr_sequencia,
		cd_estabelecimento,      
		cd_pessoa_fisica,        
		cd_portador,             
		cd_tipo_portador,        
		cd_tipo_taxa_juro,       
		cd_tipo_taxa_multa,      
		dt_emissao,              
		dt_vencimento,          
		dt_vencimento_prorrog,
		ie_origem_titulo,        
		ie_tipo_titulo,          
		nm_usuario,              
		nr_seq_nf_saida,         
		nr_titulo,               
		tx_juros,                
		tx_multa,                
		vl_juros,                
		vl_multa,                
		vl_titulo,
		vl_saldo_titulo,
		dt_atualizacao		
	from	silab_titulo_receber
	where	ie_status_integracao <> 'S'
	and		ie_origem_integracao <> 'Tasy';

vet01	c01%rowtype;		
	
begin

begin
	ie_gerar_classif_w := nvl(obter_valor_param_usuario(801,87, obter_perfil_ativo, 'Silab', 0),'S');

	begin
		select 	cd_portador,
			cd_tipo_portador,
			pr_juro_padrao,
			pr_multa_padrao,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			cd_moeda_padrao
		into	cd_portador_w,
			cd_tipo_portador_w,
			pr_juro_padrao_w,
			pr_multa_padrao_w,
			cd_tipo_taxa_juro_w,
			cd_tipo_taxa_multa_w,
			cd_moeda_padrao_w
		from 	parametro_contas_receber 
		where 	cd_estabelecimento = vet01.cd_estabelecimento;
	exception
		when others then
			ds_erro_w := obter_desc_expressao(215576,'');
			raise e_erro;
	end;

	open c01;
	loop
	fetch c01 into	
		vet01;
	exit when c01%notfound;
		begin

		if (vet01.dt_vencimento < vet01.dt_emissao) then		
			dt_emissao_w := vet01.dt_vencimento;
		else
			dt_emissao_w := vet01.dt_emissao;
		end if;		
		
		select 	titulo_seq.nextval
		into	nr_titulo_novo_w
		from	dual;
		
		insert into titulo_receber 
			(
			cd_estabelecimento,     
			cd_moeda,               
			cd_pessoa_fisica,       
			cd_portador,            
			cd_tipo_portador,       
			cd_tipo_taxa_juro,      
			cd_tipo_taxa_multa,     
			ds_observacao_titulo,   
			dt_atualizacao,         
			dt_emissao,             
			dt_inclusao,            
			dt_integracao_externa,  
			dt_pagamento_previsto,  
			dt_vencimento,          
			ie_origem_titulo,       
			ie_situacao,            
			ie_tipo_emissao_titulo, 
			ie_tipo_inclusao,       
			ie_tipo_titulo,         
			nm_usuario,             
			nr_seq_nf_saida,        
			nr_titulo,              
			nr_titulo_externo,      
			tx_desc_antecipacao,    
			tx_juros,               
			tx_multa,               
			vl_saldo_juros,         
			vl_saldo_multa,         
			vl_saldo_titulo,        
			vl_titulo              			
			)
		values
			(
			vet01.cd_estabelecimento,
			cd_moeda_padrao_w,
			vet01.cd_pessoa_fisica,
			nvl(vet01.cd_portador,cd_portador_w),
			nvl(vet01.cd_tipo_portador,cd_tipo_portador_w),
			nvl(vet01.cd_tipo_taxa_juro,cd_tipo_taxa_juro_w),
			nvl(vet01.cd_tipo_taxa_multa,cd_tipo_taxa_multa_w),
			'T�tulo gerado via integra��o com sistema Silab',
			sysdate,
			dt_emissao_w,
			sysdate,
			vet01.dt_atualizacao,
			vet01.dt_vencimento_prorrog,
			vet01.dt_vencimento,
			vet01.ie_origem_titulo,
			'1',
			'1',
			'2', -- inclus�o autom�tica
			vet01.ie_tipo_titulo,
			'Silab',
			vet01.nr_seq_nf_saida,
			nr_titulo_novo_w,
			vet01.nr_titulo,
			0,
			pr_juro_padrao_w,
			pr_multa_padrao_w,
			0,
			0,
			vet01.vl_saldo_titulo,
			vet01.vl_titulo
			);
			
			if (ie_gerar_classif_w = 'S') then
				gerar_tit_receber_classif(nr_titulo_novo_w, 'Silab');
			end if;
			
			update	silab_titulo_receber
			set	ie_status_integracao = 'S',
				ds_erro = null,
				dt_integracao = sysdate,
				nr_titulo_tasy = nr_titulo_novo_w
			where	nr_sequencia = vet01.nr_sequencia;
			
			commit;			
		end;
	end loop;
	close c01;

exception
	when e_erro then
		rollback;
		
		update	silab_titulo_receber
		set	ie_status_integracao = 'E',
			ds_erro = ds_erro_w
		where	nr_sequencia = vet01.nr_sequencia;
		
		commit;
		
	when others then
		rollback;
		
		ds_msg_err_w	:= substr(sqlerrm,1,255);
		
		update	silab_titulo_receber
		set	ie_status_integracao = 'E',
			ds_erro = ds_msg_err_w
		where	nr_sequencia = vet01.nr_sequencia;
		
		commit;
end;

end silab_integra_titulo_receber;
/