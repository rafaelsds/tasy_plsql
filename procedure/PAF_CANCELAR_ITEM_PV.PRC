create or replace
procedure paf_cancelar_item_pv(nr_prevenda_p	number,
                            		nr_seq_prevenda_p		number,
			nm_usuario_p		Varchar2,
			nr_seq_item_retorno_p out Number) is 

qt_item_cupom_w	number(10);

qt_cancelada_w	number(15);
vl_cancelada_w	number(15,2);
nr_sequencia_w	number(10);
ie_situacao_w	Varchar2(1);
nr_seq_apresentacao_w	number(15):= 0;

begin

if	(nvl(nr_prevenda_p,0) <> 0) then

	select	count(*)
	into	qt_item_cupom_w
	from	paf_pre_venda_item
	where	nr_sequencia = nr_prevenda_p
	and	ie_situacao <> 'C';

	if	(qt_item_cupom_w > 0) then

		nr_sequencia_w:= nvl(nr_prevenda_p,0);
		
	end if;
	
end if;

if	(nvl(nr_prevenda_p,0) = 0) then

	select	count(*)
	into	qt_item_cupom_w
	from	paf_pre_venda_item
	where	nr_seq_pre_venda = nr_seq_prevenda_p;

	if	(qt_item_cupom_w > 0) then

		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	paf_pre_venda_item
		where	nr_seq_pre_venda = nr_seq_prevenda_p;
		
		select	ie_situacao
		into	ie_situacao_w
		from	paf_pre_venda_item
		where	nr_sequencia = nr_sequencia_w;
	
		if	(ie_situacao_w = 'C') then
				
			nr_sequencia_w:= 0;
				
		end if;		
	
	end if;

end if;

if	(nvl(nr_sequencia_w,0) <> 0) then
	
	select	nvl(nr_seq_apresentacao,0)
	into		nr_seq_apresentacao_w
	from		paf_pre_venda_item
	where		nr_sequencia = nr_sequencia_w;
	
	nr_seq_item_retorno_p:= nr_seq_apresentacao_w;
	
	update	paf_pre_venda_item
	set	ie_situacao = 'C'
	where	nr_sequencia = nr_sequencia_w;
		
end if;
		
commit;

end paf_cancelar_item_pv;
/
