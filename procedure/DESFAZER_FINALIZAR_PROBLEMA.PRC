create or replace
procedure desfazer_finalizar_problema(	nr_sequencia_p		number,
										ds_observacao_p		varchar2, 	 
										nm_usuario_p	varchar2) is 

										
ie_status_w varchar2(1);									
begin
	select	max(ie_status)	
	into	ie_status_w
	from	lista_problema_status
	where	nr_seq_problema = nr_sequencia_p;
	
	update	lista_problema_pac
	set	ds_observacao = ds_observacao_p,
		ie_status = ie_status_w,
		dt_fim = null,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where nr_sequencia = nr_sequencia_p;
					
commit;

end desfazer_finalizar_problema;
/
