create or replace
procedure Qt_Transf_Mapa(
			nr_seq_local_p		number,
			nr_seq_agenda_qui_p	number,
			nm_usuario_p		Varchar2,
			ds_erro_p	out	varchar2) is 

dt_Agenda_w		date;
qt_tempo_medic_w	number(10);
nr_seq_prof_w		number(10);
nr_seq_atendimento_w	number(10);
qt_ocup_local_w		number(10);
nr_seq_pend_agenda_w	number(10);
			
begin

select	count(*)
into	qt_ocup_local_w
from	agenda_quimio
where	ie_status_Agenda	= 'Q'
and	nr_seq_local		= nr_seq_local_p;

select	dt_agenda,
	nr_minuto_duracao,
	nr_Seq_prof,
	nr_seq_atendimento
into	dt_Agenda_w,
	qt_tempo_medic_w,
	nr_seq_prof_w,
	nr_seq_atendimento_w
from	agenda_quimio
where	nr_sequencia	= nr_seq_agenda_qui_p;

select	nr_seq_pend_agenda
into	nr_seq_pend_agenda_w
from	paciente_atendimento
where	nr_seq_atendimento	= nr_seq_atendimento_w;

if	(qt_ocup_local_w	= 0) then
	insert into agenda_quimio_marcacao
				(nr_sequencia,
				dt_agenda,
				nm_usuario,
				nr_seq_local,
				nr_duracao,
				nr_seq_pend_agenda,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_gerado,
				ie_transferencia,
				nr_seq_prof)
			values
				(agenda_quimio_marcacao_seq.nextval,
				dt_agenda_w,
				nm_usuario_p,
				nr_seq_local_p,
				qt_tempo_medic_w,
				nr_seq_pend_agenda_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				'N',
				'S',
				nr_seq_prof_w);
				
	Qt_Confirmar_Transf(nr_seq_pend_agenda_w, nm_usuario_p);
else
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277334,null);
end if;

commit;

end Qt_Transf_Mapa;
/