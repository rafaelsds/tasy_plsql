create or replace
procedure limpar_tabela_lab_tipo_log is

cd_log_tasy_w		number(10,0);
qt_dia_log_tasy_w	number(10,0);
s_array 		LT_DATE;
i			Integer := 1;
type Vetor is table of LT_DATE index by binary_integer;
Vetor_c01_w		Vetor;
dt_inicial_w		date := to_Date('01/01/1900','dd/mm/yyyy');
dt_final_w		date;

cursor c01 is
	select	tlt.cd_log,
		tlt.qt_dias_base	
	from	lab_tipo_log tlt
	where	tlt.qt_dias_base is not null;

Cursor C02 is
	select *
	from	(
		select	a.dt_atualizacao
		from	log_lab a
		where	a.dt_atualizacao between dt_inicial_w and dt_final_w
		and	a.cd_log = cd_log_tasy_w
	)
	where rownum < 100000;
	
begin

gravar_processo_longo('Vetor Principal', 'LIMPAR_TABELA_LAB_TIPO_LOG', -1);

open c01;
loop
fetch c01 into 
	cd_log_tasy_w,
	qt_dia_log_tasy_w;	
exit when c01%notfound;
	begin
	dt_final_w := sysdate - qt_dia_log_tasy_w;
	gravar_processo_longo('LOG_TASY','LIMPAR_TABELA_LAB_TIPO_LOG',-1);
	
	/*TRATAMENTO ESPEC�FICO DEVIDO AO VOLUME DE DADOS*/
	exec_sql_dinamico('Tasy','ALTER TABLE LOG_LAB NOLOGGING');
	open c02;
	loop
	FETCH C02 BULK COLLECT INTO s_array LIMIT 100;
		Vetor_c01_w(i) := s_array;
		i := i + 1;
	EXIT WHEN C02%NOTFOUND;
	END LOOP;
	CLOSE C02;
			
	for i in 1..Vetor_c01_w.COUNT loop
		s_array := Vetor_c01_w(i);
	
		gravar_processo_longo('Removendo LOG_LAB -> ' || i || '/' || Vetor_c01_w.COUNT,'LIMPAR_TABELA_LAB_TIPO_LOG',-1);
		
		delete	from	log_lab
		where	dt_atualizacao in ( select column_value from table(cast(s_array as LT_DATE)))
		and	cd_log = cd_log_tasy_w;

		commit;
	end LOOP;
	
	exec_sql_dinamico('Tasy','ALTER TABLE LOG_LAB LOGGING');
	
	exception
	when others then
		exec_sql_dinamico('Tasy','ALTER TABLE LOG_LAB LOGGING');
	end;	
end loop;
close c01;

end limpar_tabela_lab_tipo_log;
/