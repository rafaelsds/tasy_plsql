create or replace PROCEDURE Validate_segment_qhapdc(nr_seq_segment_p NUMBER, 
                                                    nr_seq_dataset_p NUMBER, 
                                                    ie_segmento_p    VARCHAR2, 
                                                    nm_usuario_p     VARCHAR2,
                                                    nr_atendimento_p NUMBER) 
IS 
  nr_seq_estrut_arq_w NUMBER(10); 
  vl_retorno_w        VARCHAR2(255); 
  vl_resultado_w      VARCHAR2(255); 
  ds_sql_w            VARCHAR2(500); 
  c001                INTEGER; 
  CURSOR ccampos IS 
    SELECT nm_table, 
           nm_attribute, 
           ds_field 
    FROM   qhapdc_field_mapping 
    WHERE  nr_seq_segment = nr_seq_segment_p 
           AND ie_mandatory = 'S'; 
  ccampos_w           ccampos%ROWTYPE; 
BEGIN 
    --select  max(a.nr_seq_estrut_arq) 
    -- 
    --into  nr_seq_estrut_arq_w 
    -- 
    --from  QHAPDC_FILE a,  
    -- 
    --  QHAPDC_DATASET_SEND b 
    -- 
    --where  b.nr_seq_arquivo   = a.nr_sequencia 
    -- 
    --and  b.nr_sequencia    = nr_seq_dataset_p; 
    OPEN ccampos; 

    LOOP 
        FETCH ccampos INTO ccampos_w; 

        exit WHEN ccampos%NOTFOUND; 

        ds_sql_w := ' select to_char(' 
                    || ccampos_w.nm_attribute 
                    ||') from ' 
                    ||ccampos_w.nm_table 
                    || ' where nr_dataset = ' 
                    || nr_seq_dataset_p; 
 
        c001 := dbms_sql.open_cursor; 

        dbms_sql.Parse(c001, ds_sql_w, dbms_sql.native); 

        dbms_sql.Define_column(c001, 1, vl_retorno_w, 255); 

        vl_resultado_w := dbms_sql.EXECUTE(c001); 

        vl_resultado_w := dbms_sql.Fetch_rows(c001); 

        dbms_sql.Column_value(c001, 1, vl_retorno_w); 

        dbms_sql.Close_cursor(c001); 

        IF ( Trim(vl_retorno_w) IS NULL ) THEN 
          -- Field # @ DS_FIELD # @, was not informed for segment # @ IE_SEGMENT # @ of this dataset. 
          Generate_dataset_inconsistency(nr_seq_dataset_p, 'E', 
          wheb_mensagem_pck.Get_texto(996262, 'DS_CAMPO=' 
                                              ||ccampos_w.ds_field 
                                              ||';IE_SEGMENTO=' 
                                              ||ie_segmento_p 
                                              ||';'), nm_usuario_p, 
          ie_segmento_p, nr_atendimento_p); 
        END IF; 
    END LOOP; 

    CLOSE ccampos; 
END validate_segment_qhapdc;
/