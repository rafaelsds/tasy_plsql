create or replace
procedure Bl_registrar_coleta(	nr_seq_doacao_p		Number,
				nr_seq_agenda_p		Number,
				dt_coleta_p		Date,
				cd_frasco_p		Varchar2,
				qt_volume_p		Number,
				ie_classif_coleta_p	Varchar2,
				ie_procedencia_p	Varchar2,
				ds_observacao_p		Varchar2,
				nm_usuario_p		Varchar2,
				dt_inicio_coleta_p	date,
				dt_fim_coleta_p		date) is 

				
nr_seq_coleta_w		Number(10);
nr_seq_status_leite_w	Number(10);
nr_seq_frasco_w		number(10);
				
begin
if	(nr_seq_doacao_p is not null) and (dt_coleta_p is not null) and (ie_procedencia_p is not null) then

	select	bl_coleta_seq.nextval
	into	nr_seq_coleta_w
	from	dual;

	insert into bl_coleta (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_doacao,
		nr_seq_agenda,
		dt_coleta,
		ds_observacao,
		ie_procedencia,
		ie_classif_leite,
		dt_inicio_coleta,
		nm_usuario_inicio,
		dt_fim_coleta,
		nm_usuario_fim)
	values(	nr_seq_coleta_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_doacao_p,
		nr_seq_agenda_p,
		dt_coleta_p,
		ds_observacao_p,
		ie_procedencia_p,
		ie_classif_coleta_p,
		dt_inicio_coleta_p,
		nm_usuario_p,
		dt_fim_coleta_p,
		nm_usuario_p);
		
	if	(nr_seq_coleta_w is not null) then
	
		select	max(nr_sequencia)
		into	nr_seq_status_leite_w
		from	bl_status_leite
		where	ie_status = 'C'
		and	nvl(ie_situacao,'A') = 'A';
		
		select	bl_frasco_seq.nextval
		into	nr_seq_frasco_w
		from	dual;
	
		insert into bl_frasco (	
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_coleta,
			dt_frasco,
			qt_volume,
			ds_observacao,
			qt_volume_inicial,
			cd_frasco,
			nr_seq_status_leite,
			ie_situacao_processa)
		values(	nr_seq_frasco_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_coleta_w,
			dt_coleta_p,
			nvl(qt_volume_p,0),
			ds_observacao_p,
			nvl(qt_volume_p,0),			
			cd_frasco_p,
			nr_seq_status_leite_w,
			'N');
			
			BL_gravar_codigo_frasco(nr_seq_frasco_w, 0, nm_usuario_p);
	
	end if;	
end if;
		
commit;

end Bl_registrar_coleta;
/