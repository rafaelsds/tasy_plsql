create or replace
procedure gerar_comunic_solic_compra(
		nr_solic_compra_p 			number,
		nm_usuario_p			varchar2) is


cd_estabelecimento_w		Number(04,0);
ds_comunicacao_w			Varchar2(2000);
ds_titulo_w			Varchar2(80);
ds_destinatario_w			Varchar2(255);
nr_seq_classif_w			Number(10,0);
nr_sequencia_w			Number(10,0);
qt_regra_usuario_w			Number(10,0);
nr_seq_regra_w			number(10,0);
cd_setor_atendimento_w		number(5);
ie_urgente_w			varchar2(1);
cd_evento_w			number(3);
cd_perfil_w			varchar2(10);
/* Se tiver setor na regra, envia CI para os setores */
ds_setor_adicional_w                    	varchar2(2000) := '';
/* Campos da regra Usu�rio da Regra */
cd_setor_regra_usuario_w		number(5);

cursor c05 is
select	b.nr_sequencia,
	b.cd_evento,
	b.cd_perfil
from	regra_envio_comunic_evento b,
	regra_envio_comunic_compra a
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_estabelecimento = cd_estabelecimento_w
and	b.cd_evento = 9	
and	b.ie_situacao = 'A'
and	(((cd_setor_atendimento_w is not null) and (b.cd_setor_destino = cd_setor_atendimento_w)) or
	((cd_setor_atendimento_w is null) and (b.cd_setor_destino is null)) or (b.cd_setor_destino is null))
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,nr_solic_compra_p,'SC',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S'
union
select	b.nr_sequencia,
	b.cd_evento,
	b.cd_perfil
from	regra_envio_comunic_evento b,
	regra_envio_comunic_compra a
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_estabelecimento = cd_estabelecimento_w
and	b.cd_evento = 15
and	b.ie_situacao = 'A'
and	ie_urgente_w = 'S'
and	(((cd_setor_atendimento_w is not null) and (b.cd_setor_destino = cd_setor_atendimento_w)) or
	((cd_setor_atendimento_w is null) and (b.cd_setor_destino is null)) or (b.cd_setor_destino is null))
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,nr_solic_compra_p,'SC',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';

Cursor c06 is
select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
from	regra_envio_comunic_usu a
where	a.nr_seq_evento = nr_seq_regra_w;

begin

select	cd_setor_atendimento,
	cd_estabelecimento,
	ie_urgente
into	cd_setor_atendimento_w,	
	cd_estabelecimento_w,	
	ie_urgente_w
from 	solic_compra
where 	nr_solic_compra = nr_solic_compra_p;

open C05;
loop
fetch C05 into	
	nr_seq_regra_w,
	cd_evento_w,
	cd_perfil_w;
exit when C05%notfound;
	begin
	
	select	count(*)
	into	qt_regra_usuario_w
	from	regra_envio_comunic_compra a,
		regra_envio_comunic_evento b,
		regra_envio_comunic_usu c
	where	a.nr_sequencia = b.nr_seq_regra
	and	b.nr_sequencia = c.nr_seq_evento
	and	b.nr_sequencia = nr_seq_regra_w;
	
	if	(nr_seq_regra_w > 0) then

		open C06;
		loop
		fetch C06 into	
			cd_setor_regra_usuario_w;
		exit when C06%notfound;
			begin
			if	(cd_setor_regra_usuario_w <> 0) and
				(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
				ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
			end if;
			end;
		end loop;
		close C06;

		ds_destinatario_w := obter_usuarios_comunic_compras(nr_solic_compra_p,null,cd_evento_w,nr_seq_regra_w,'');

		select	substr(max(obter_dados_regra_com_compra(nr_solic_compra, null, 913, cd_evento_w, nr_seq_regra_w, 'M')),1,2000) ds_comunicacao,
			substr(max(obter_dados_regra_com_compra(nr_solic_compra, null, 913, cd_evento_w, nr_seq_regra_w,'T')),1,80) ds_titulo
		into	ds_comunicacao_w,
			ds_titulo_w
		from	solic_compra
		where	nr_solic_compra = nr_solic_compra_p;

		select	obter_classif_comunic('F')
		into	nr_seq_classif_w
		from	dual;

		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		if	(cd_perfil_w is not null) then
			cd_perfil_w := cd_perfil_w ||',';
		end if;

		insert	into comunic_interna(
				cd_estab_destino,
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
 				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				nr_sequencia,
				ie_gerencial,
 				nr_seq_classif,
				dt_liberacao,
				ds_perfil_adicional,
				ds_setor_adicional)
		values(		cd_estabelecimento_w,
				sysdate,
 				ds_titulo_w,
				ds_comunicacao_w,
				Wheb_mensagem_pck.get_Texto(301313),
				sysdate,
				'N',
				ds_destinatario_w,
				nr_sequencia_w,
				'N',
				nr_seq_classif_w,
				sysdate,
				cd_perfil_w,
				ds_setor_adicional_w);
	end if;
	end;
end loop;
close C05;

commit;	
	
end gerar_comunic_solic_compra;
/
