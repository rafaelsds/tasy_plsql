create or replace
procedure deletar_pac_protocolo_medic(
		nr_seq_paciente_p	number) is
begin
if	(nr_seq_paciente_p is not null) then
	begin	
	
	delete 	from paciente_protocolo_medic
	where	nr_seq_paciente = nr_seq_paciente_p;
	end;
end if;
commit;
end deletar_pac_protocolo_medic;
/