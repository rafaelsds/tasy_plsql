create or replace procedure ESTORNAR_ABONO_SLA (
					nr_seq_ordem_servico_p 	number
						) is
begin	
	
	delete
	from man_ordem_serv_sla_abonada
	where nr_seq_ordem_servico = nr_seq_ordem_servico_p;
	commit;
	
end ESTORNAR_ABONO_SLA;
/
