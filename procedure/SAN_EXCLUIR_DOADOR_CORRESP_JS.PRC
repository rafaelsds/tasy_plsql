create or replace
procedure san_excluir_doador_corresp_js(nr_seq_corresp	varchar2) is 
		
ds_lista_exclusao_w	varchar2(2000);
nr_pos_virgula_w	number(10,0);	
nr_seq_sequencia_w	number(10);

begin

if	(nr_seq_corresp is not null) then
	begin
	 ds_lista_exclusao_w	:=	nr_seq_corresp;
	 
	 while  (ds_lista_exclusao_w is not null)	loop 
	 	begin
	 	nr_pos_virgula_w := instr(ds_lista_exclusao_w,',');
		
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_seq_sequencia_w	:= to_number(substr(ds_lista_exclusao_w,0,nr_pos_virgula_w-1));
			ds_lista_exclusao_w	:= substr(ds_lista_exclusao_w,nr_pos_virgula_w+1,length(ds_lista_exclusao_w));			
			end;
		else
			begin
			nr_seq_sequencia_w	:= to_number(ds_lista_exclusao_w);
			ds_lista_exclusao_w	:= null;
			end;
		end if;
				
		if	(nr_seq_sequencia_w > 0)then
			begin
			san_excluir_doadores_corresp(nr_seq_sequencia_w);
			end;
		end if;
		
		
	 	end;
	 end loop;
	
	end;
end if;


commit;

end san_excluir_doador_corresp_js;
/