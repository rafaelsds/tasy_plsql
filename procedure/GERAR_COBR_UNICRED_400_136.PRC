create or replace
procedure gerar_cobr_unicred_400_136
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

/* Header */
ds_conteudo_w			varchar2(400);
cd_empresa_w			banco_estabelecimento.cd_convenio_banco%type;
nm_empresa_w			pessoa_juridica.ds_razao_social%type;
dt_geracao_w			varchar2(6);
ds_zero_13_w			number(13);
ds_branco_7_w			varchar2(7);
ds_brancos_277_w		varchar2(277);
nr_seq_arquivo_w		varchar2(7);
nr_seq_registro_w		varchar2(10);

/* Transacao */
ie_digito_agencia_w		varchar2(1);
cd_agencia_debito_w		varchar2(8);
cd_conta_w			    varchar2(12);
ie_digito_conta_w		varchar2(1);
id_empresa_w			varchar2(17);
nr_controle_partic_w	varchar2(25);
ie_multa_w			    varchar2(1);
pr_multa_w			    varchar2(10);
nr_dig_nosso_numero_w	varchar2(2);
vl_desconto_dia_w		varchar2(10);
cd_condicao_w			varchar2(1);
ie_endereco_w			varchar2(1);
ds_brancos_2_w			varchar2(2);
ie_ocorrencia_w			varchar2(2);
nr_documento_w			varchar2(10);
dt_vencimento_w			varchar2(6);
vl_titulo_w			    varchar2(13);
cd_banco_cobranca_w		varchar2(3);
cd_agencia_deposito_w	varchar2(5);
dt_emissao_w			varchar2(6);
ie_instrucao_1_w		varchar2(2);
ie_instrucao_2_w		varchar2(2);
vl_acrescimo_w			varchar2(13);
dt_desconto_w			varchar2(6);
vl_desconto_w			varchar2(13);
vl_abatimento_w			varchar2(13);
ie_tipo_inscricao_w		varchar2(2);
nr_inscricao_w			varchar2(14);
nm_sacado_w			    varchar2(40);
ds_endereco_sacado_w	varchar2(40);
ds_bairro_sacado_w		varchar2(40);
ds_estado_sacado_w		varchar2(40);
ds_cidade_sacado_w		varchar2(40);
nm_avalista_w			varchar2(60);
cd_cep_w			    varchar2(8);
nr_nosso_numero_w		varchar2(11);
ds_brancos_10_w			varchar2(10);
cd_carteira_w			varchar2(3);
ie_desconto_w           varchar(2);
nr_seq_reg_arquivo_w	number(10)	:= 1;
vl_juros_diario_w		varchar2(13);
nr_titulo_w			    number(10);
ie_tipo_pagamento_w     number(10);
vl_tx_juros_w           varchar2(13);
cd_tipo_taxa_juro_w     TITULO_RECEBER_V.CD_TIPO_TAXA_JURO%type;
ie_tipo_taxa_w          TIPO_TAXA.ie_tipo_taxa%type;

/* Trailler */
ds_brancos_393_w		varchar2(393);

/*Contador*/
nr_seq_apres_w			number(10)	:= 1;
ie_gerar_cob_esc_prim_mens_w	varchar(1) 	:= null;

cd_convenio_banco_w		varchar2(20);


Cursor C01 is
	select	lpad(nvl(y.cd_agencia_bancaria ,0),5,'0') cd_agencia_debito,
		lpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),1,1), y.ie_digito),1,' ') ie_digito_agencia,
		lpad(nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'C'),nvl(x.cd_conta, '0')),12,'0') cd_conta,		
		substr(nvl(nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DC'),x.ie_digito_conta),'0'),1,1) ie_digito_conta,
		'0' || lpad(nvl(x.cd_carteira,'0'),3,'0') || lpad(nvl(x.cd_agencia_bancaria,'0'),5,'0') || 
			lpad(nvl(x.cd_conta,'0'),7,'0') || nvl(x.ie_digito_conta,'0') id_empresa,
		lpad(nvl(substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(d.nr_seq_pagador),'C'),1,25),' '),25,' ') nr_controle_partic,
		decode(nvl(b.tx_multa,0),0,'0','2') ie_multa,
		replace(to_char(b.tx_multa, 'fm00000000.00'),'.','') pr_multa,
		lpad(substr(nvl(b.nr_nosso_numero,'0'),1,11),11,'0') nr_nosso_numero,
		null,
		--decode(b.nr_nosso_numero, null, '0', calcula_digito('MODULO11', lpad(b.nr_nosso_numero,11,'0'))) nr_dig_nosso_numero,
		lpad('0',10,'0') vl_desconto_dia,
		nvl(a.ie_emissao_bloqueto,'1') cd_condicao,
		lpad(nvl(c.cd_ocorrencia,'1'),2,'0') ie_ocorrencia,
		lpad(nvl(b.nr_titulo,'0'),10,'0') nr_documento,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
		replace(to_char(b.vl_saldo_titulo, 'fm00000000000.00'),'.','') vl_titulo,
		lpad('0',3,'0') cd_banco_cobranca,
		lpad('0',5,'0') cd_agencia_deposito,
		to_char(b.dt_emissao,'ddmmyy') dt_emissao,
		'00' ie_instrucao1,
		'00' ie_instrucao2,
		replace(to_char(nvl(c.vl_acrescimo,0), 'fm00000000000.00'),'.','') vl_acrescimo,
		to_char(nvl(b.dt_limite_desconto,b.dt_pagamento_previsto),'ddmmyy') dt_desconto,
		replace(to_char(nvl(c.vl_desconto,0), 'fm00000000000.00'),'.','') vl_desconto,
		lpad('0',13,'0') vl_abatimento,
		decode(b.cd_cgc, null,'01','02') ie_tipo_inscricao,
		lpad(nvl(b.cd_cgc_cpf,'0'),14, '0') nr_inscricao,
		rpad(upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))),40, ' ') nm_sacado,
		rpad(upper(elimina_acentuacao(substr(
		nvl((select	max(x.ds_endereco)
		from	pessoa_juridica_compl x
		where	x.ie_tipo_complemento	= 5
		and	x.cd_cgc		= b.cd_cgc),
		decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'EN'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'E'))) || ' ' ||
		decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'NR'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'NR')) || ' ' ||
		decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CO'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CO')),1,40))),40, ' ') ds_endereco_sacado,
		lpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8),8,'0') cd_cep,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,12),12,' ') ds_bairro,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,20),20,' ') ds_cidade,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),2,' ') ds_estado,
		lpad(' ',38,' ') nm_avalista,
		lpad(substr(nvl(a.nr_sequencia,''),1,6),6,'0') nr_seq_arquivo,
		lpad(nvl(e.cd_carteira, substr(somente_numero(Obter_carteira_tit_rec(b.nr_seq_carteira_cobr)),1,3)),3,'0'),
		replace(to_char(nvl(obter_vl_juros_diario_tit(null,b.nr_titulo),0), 'fm00000000000.00'),'.', '') vl_juros_diario,
		b.nr_titulo,
        replace(to_char(nvl(b.tx_juros,0), 'fm00000000000.00'),'.', '') vl_tx_juros,
        nvl(b.cd_tipo_taxa_juro, 0) tipo_tx_juro
	from	pls_lote_mensalidade z,
		titulo_receber_mensagem g,
		banco_estabelecimento x,
		AGENCIA_BANCARIA y,
		pls_contrato_pagador f,
		banco_carteira e,
		pls_mensalidade d,
		titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo			= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	AND	x.cd_agencia_bancaria = y.cd_agencia_bancaria
	and	x.cd_banco = y.cd_banco
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	a.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador		= f.nr_sequencia(+)
	and	c.nr_titulo			= g.nr_titulo(+)
	and	d.nr_seq_lote		= z.nr_sequencia(+)
	and	((z.ie_primeira_mensalidade is null or z.ie_primeira_mensalidade = 'N') 
	or	ie_gerar_cob_esc_prim_mens_w = 'S')
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin

delete from w_envio_banco where nm_usuario = nm_usuario_p;

/* Pega o parametro para ver se considera os titulos gerados por lotes de primera mensalidade */
select	nvl(max(ie_gerar_cob_esc_prim_mens),'S')
into	ie_gerar_cob_esc_prim_mens_w
from	pls_parametros_cr
where	cd_estabelecimento = cd_estabelecimento_p;

select	lpad(' ',277,' '),
	lpad(' ',2,' '),
	lpad(' ',393,' '),
	lpad(' ',10,' '),
	lpad('0',13,'0'),
	lpad(' ',7,' ')
into	ds_brancos_277_w,
	ds_brancos_2_w,
	ds_brancos_393_w,
	ds_brancos_10_w,
	ds_zero_13_w,
	ds_branco_7_w
from	dual;

/* Header */

select	lpad(substr(nvl(c.cd_conta,'0'),1,12) || substr(nvl(c.ie_digito_conta,'0'),1,1),20,'0'),
	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30))),30, ' '),
	to_char(a.dt_remessa_retorno,'ddmmyy'),
	lpad(nvl(to_char(substr(nvl(a.nr_remessa,a.nr_sequencia),1,7)),'0'),7,'0'),
	lpad(substr(nvl(cd_convenio_banco,'0'),1,20),20,'0')
into	cd_empresa_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w,
	cd_convenio_banco_w
from	estabelecimento		b,
	cobranca_escritural	a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

ds_conteudo_w	:= 	'01' ||
			'REMESSA' ||
			'01' ||
			rpad('COBRANCA',15,' ') || --012 026
			cd_convenio_banco_w || --27 a 46
			nm_empresa_w || --47 76
			'136' || --77 a 79
			rpad('UNICRED',15,' ') || --080 a 094
			dt_geracao_w || --095 a 100
			ds_branco_7_w || --101 a 107
			'000' || --108 a 110
			nr_seq_arquivo_w || --111 a 117
			ds_brancos_277_w || --118 a 394
			lpad(nr_seq_reg_arquivo_w,6,'0'); --395 a 400

nr_seq_apres_w := nr_seq_apres_w + 1;

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(nr_seq_registro_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w,
	0);

/* Fim Header */

/* Transacao */
--begin
open C01;
loop
fetch C01 into	
	cd_agencia_debito_w,
	ie_digito_agencia_w,
	cd_conta_w,
	ie_digito_conta_w,
	id_empresa_w,
	nr_controle_partic_w,
	ie_multa_w,
	pr_multa_w,
	nr_nosso_numero_w,
	nr_dig_nosso_numero_w,
	vl_desconto_dia_w,
	cd_condicao_w,
	ie_ocorrencia_w,
	nr_documento_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_banco_cobranca_w,
	cd_agencia_deposito_w,
	dt_emissao_w,
	ie_instrucao_1_w,
	ie_instrucao_2_w,
	vl_acrescimo_w,
	dt_desconto_w,
	vl_desconto_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	cd_cep_w,
	ds_bairro_sacado_w,
	ds_cidade_sacado_w,
	ds_estado_sacado_w,
	nm_avalista_w,
	nr_seq_arquivo_w,
	cd_carteira_w,
	vl_juros_diario_w,
	nr_titulo_w,
    vl_tx_juros_w,
    cd_tipo_taxa_juro_w;
exit when C01%notfound;
	begin
	/*Transacao Tipo 1*/
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;

    select nvl(ie_tipo_taxa, 'NULL') 
    into ie_tipo_taxa_w
    from TIPO_TAXA
    where cd_tipo_taxa = cd_tipo_taxa_juro_w;

    if (ie_tipo_taxa_w = 'M') then
        ie_tipo_pagamento_w := 2;
    elsif (ie_tipo_taxa_w = 'D') then
        ie_tipo_pagamento_w := 4;
    else
        ie_tipo_pagamento_w := 5;
        vl_tx_juros_w := replace(to_char(nvl(0,0), 'fm00000000000.00'),'.', '');
    end if;

    if (nvl(vl_desconto_w, 0) > 0) then
        ie_desconto_w := '1';
    else
        ie_desconto_w := '0';
        dt_desconto_w := '000000';
    end if;

	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

	ds_conteudo_w	:=	'1' || --001 a 001
				cd_agencia_debito_w || --002 a 006
				ie_digito_agencia_w || --007 a 007
				cd_conta_w || --008 a 019
				ie_digito_conta_w || --020 a 020
				'0' || --021 A 021
				cd_carteira_w || --022 a 024
				'0000000000000' || --025 a 037 
				nr_controle_partic_w || --038 a 062
				'136' || --063 a 065
				'00' || --066 a 067
				lpad('0',25,'0') || --068 a 092 
				'0' || -- 093 a 093
				'2' || --094 a 094
				pr_multa_w || --095 a 104
				ie_tipo_pagamento_w || --105 a 105 
				'N' || --106 a 106
				'  ' || --107 a 108
				ie_ocorrencia_w || --109 a 110
				nr_documento_w || --111 a 120 nr do titulo
				dt_vencimento_w || --121 a 126
				vl_titulo_w || --127 a 139
				'0000000000' || --140 a 149
				ie_desconto_w || --150 a 150
				dt_emissao_w || --151 a 156 
				'0' || --157 
				'3' || --158
				'00' || --159 a 160 
				vl_tx_juros_w || --161 a 173
				dt_desconto_w || --174 a 179 
				vl_desconto_w || --180 a 192 
				--lpad(nr_nosso_numero_w,10,'0') || lpad(nr_dig_nosso_numero_w,1,'0') || -- 193 a 203 Nosso Numero na UNICRED
				nr_nosso_numero_w || /*193 e 203 - NN e digito, ja com lpad a esquerda com 0*/
				'00' || --204 a 205
				vl_abatimento_w || --206 a 218 (?)
				ie_tipo_inscricao_w || --219 a 220
				nr_inscricao_w || --221 a 234
				elimina_caractere_esp_asc(nm_sacado_w, 'S', 'S') || --235 a 274
				elimina_caractere_esp_asc(ds_endereco_sacado_w, 'S', 'S') || --275 a 314
				elimina_caractere_esp_asc(ds_bairro_sacado_w, 'S', 'S') || --315 a 326
				substr(cd_cep_w,1,5) || --327 a 331
				substr(cd_cep_w,6,3) ||--332 a 334
				elimina_caractere_esp_asc(ds_cidade_sacado_w, 'S', 'S') || --335 a 354
				elimina_caractere_esp_asc(ds_estado_sacado_w, 'S', 'S') || --355 a 356
				elimina_caractere_esp_asc(nm_avalista_w, 'S', 'S') || --357 a 394
				lpad(nr_seq_reg_arquivo_w,6,'0');

	nr_seq_apres_w := nr_seq_apres_w + 1;

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_titulo)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w,
		0,
		nr_titulo_w);
	/*Fim Transacao Tipo 1*/

	end;
end loop;
close C01;

/* Fim Transacao */

/* Trailler */
nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

ds_conteudo_w	:= '9' ||
		   ds_brancos_393_w || 
		   lpad(nr_seq_reg_arquivo_w,6,'0');

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

nr_seq_apres_w := nr_seq_apres_w + 1;
insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(nr_seq_registro_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w,
	0);


/* Fim Trailler*/

commit;

end gerar_cobr_unicred_400_136;
/