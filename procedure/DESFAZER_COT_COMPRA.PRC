Create or Replace
procedure Desfazer_Cot_Compra(	nr_cot_compra_p		in	Number,
				nm_usuario_p		in	Varchar2,
				ie_situacao_p		in	varchar2) is

dt_geracao_ordem_compra_w		Date;
nr_ordem_compra_w			number(10);
dt_aprovacao_w				date;
dt_liberacao_w				date;
nr_item_cot_compra_w			number(5);
dt_limite_entrega_w				date;
qt_material_w				number(13,4);
ds_lista_ordem_w			varchar2(2000);
ds_hist_solic_compra_w			solic_compra_hist.ds_historico%type;
ds_titulo_w				solic_compra_hist.ds_titulo%type;
nr_solic_compra_w			number(10);
nr_item_solic_compra_w			number(10);

cursor c01 is
	select	nr_ordem_compra
	from	cot_compra_forn_item
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_ordem_compra is not null;

cursor c02 is
select	distinct
	a.dt_limite_entrega,
	a.qt_material,
	a.nr_item_cot_compra
from	cot_compra b,
	cot_compra_item a
where	b.nr_cot_compra = a.nr_cot_compra
and	b.nr_cot_compra = nr_cot_compra_p
and	not exists(select	1
		from	cot_compra_item_entrega x
		where	x.nr_cot_compra = a.nr_cot_compra
		and	x.nr_item_cot_compra = a.nr_item_cot_compra);
		
cursor c03 is		
select	distinct nr_ordem_compra
from	ordem_compra a
where exists(	select	b.nr_ordem_compra
		from	ordem_compra_item b
		where	a.nr_ordem_compra = b.nr_ordem_compra
		and	b.nr_cot_compra = nr_cot_compra_p);
		
cursor c04 is
select	distinct nr_solic_compra
from	solic_compra_item_agrup_v
where	nr_cot_compra = nr_cot_compra_p;

cursor c05 is
select	nr_item_solic_compra
from	solic_compra_item_agrup_v
where	nr_solic_compra = nr_solic_compra_w
and	nr_cot_compra	= nr_cot_compra_p;

BEGIN

if	(ie_situacao_p in ('L','A')) then
	
	open 	c01;
	loop	
	fetch c01 into 
		nr_ordem_compra_w;
	exit when c01%notfound;
		
		select	obter_data_ordem_compra(nr_ordem_compra_w,'L'),
			obter_data_ordem_compra(nr_ordem_compra_w,'A')
		into	dt_liberacao_w,
			dt_aprovacao_w
		from	dual;

		if	((ie_situacao_p = 'L') and (dt_liberacao_w is not null)) then
			/*(-20011,'Sem permiss�o para desfazer a cota��o. A ordem de compra n�mero ' || NR_ORDEM_COMPRA_W || ' j� est� liberada.' || CHR(13) || 'Verifique o par�metro [21].');*/
			wheb_mensagem_pck.exibir_mensagem_abort(185687,'NR_ORDEM_COMPRA_W='||NR_ORDEM_COMPRA_W);
			
		elsif	((ie_situacao_p = 'A') and (dt_aprovacao_w is not null)) then
			/*(-20011,'Sem permiss�o para desfazer a cota��o. A ordem de compra n�mero ' || nr_ordem_compra_w || ' j� est� aprovada.' || CHR(13) || 'Verifique o par�metro [21].');*/
			wheb_mensagem_pck.exibir_mensagem_abort(185688,'NR_ORDEM_COMPRA_W='||NR_ORDEM_COMPRA_W);
		end if;
	end loop;
	close c01;
end if;

open C03;
loop
fetch C03 into	
	nr_ordem_compra_w;
exit when C03%notfound;
	begin
	
	ds_lista_ordem_w := substr(ds_lista_ordem_w || nr_ordem_compra_w || ', ',1,2000);
	
	end;
end loop;
close C03;

ds_lista_ordem_w := substr(ds_lista_ordem_w,1,length(ds_lista_ordem_w)-2);

select	dt_geracao_ordem_compra
into 	dt_geracao_ordem_compra_w
from	cot_compra
where	nr_cot_compra	 = nr_cot_compra_p;

if	(dt_geracao_ordem_compra_w is not null) then
	begin	
	update	cot_compra
	set	dt_geracao_ordem_compra = null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_cot_compra	= nr_cot_compra_p;

	update	cot_compra_forn_item
	set	nr_ordem_compra	= null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where 	nr_cot_compra	= nr_cot_compra_p;

	update	cot_compra_solic_agrup
	set	nr_ordem_compra	= null
	where	nr_cot_compra	= nr_cot_compra_p;

	update	processo_aprov_compra
	set	ie_aprov_reprov = 'B',
		ds_observacao = substr(ds_observacao || WHEB_MENSAGEM_PCK.get_texto(299460),1,2000)
	where	nr_sequencia in (
		select	distinct(nr_seq_aprovacao)
		from	ordem_compra_item
		where	nr_ordem_compra in(
			select	nr_ordem_compra
			from	ordem_compra_item
			where	nr_cot_compra = nr_cot_compra_p));
	
	delete from registro_integr_compras a
	where a.nr_ordem_compra in (	select	b.nr_ordem_compra
					from	ordem_compra_item b
					where	a.nr_ordem_compra = b.nr_ordem_compra
					and	b.nr_cot_compra = nr_cot_compra_p);

	compras_pck.set_is_oci_delete('N');				
	begin				
	delete ordem_compra a
	where exists(
		select	b.nr_ordem_compra
		from	ordem_compra_item b
		where	a.nr_ordem_compra = b.nr_ordem_compra
		and	b.nr_cot_compra = nr_cot_compra_p);
	end;
	exception
		when others then
		compras_pck.set_is_oci_delete('S');
		wheb_mensagem_pck.exibir_mensagem_abort(266185, 'DS_ERRO=' || substr(sqlerrm,1,3000)); 
	end;
	compras_pck.set_is_oci_delete('S');
	
	gerar_historico_cotacao(nr_cot_compra_p,
				WHEB_MENSAGEM_PCK.get_texto(299463),
				substr(WHEB_MENSAGEM_PCK.get_texto(299470,'NM_USUARIO='||nm_usuario_p||
							';DS_DATA='||sysdate||
							';DS_LISTA='||ds_lista_ordem_w),1,2000),
				'S',
				nm_usuario_p);
	
	
end if;
	
update	cot_compra_item
set	nr_seq_cot_item_forn	= null,
	nr_cot_venc_sis		= null,
	nr_item_cot_venc_sis	= null,
	cd_cgc_fornecedor_venc_sis	= null,
	cd_cgc_fornecedor_venc_alt	= null,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_cot_compra		= nr_cot_compra_p;

update	cot_compra_forn_item
set 	vl_preco_liquido		= null,
	vl_total_liquido_item		= null,
	vl_presente		= null,
	nr_ordem_compra		= null,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_cot_compra		= nr_cot_compra_p;

OPEN C02;
LOOP
FETCH C02 INTO
	dt_limite_entrega_w,
	qt_material_w,
	nr_item_cot_compra_w;
	exit when c02%notfound;
	begin
	insert into cot_compra_item_entrega(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_cot_compra,
			nr_item_cot_compra,
			dt_entrega,
			qt_entrega,
			ds_observacao)
		values(cot_compra_item_entrega_seq.nextval,
			sysdate,
			'Tasy',
			sysdate,
			'Tasy',
			nr_cot_compra_p,
			nr_item_cot_compra_w,
			dt_limite_entrega_w,
			qt_material_w,
			null);
	end;

END LOOP;
CLOSE C02;

open c04;
loop
fetch c04 into
	nr_solic_compra_w;
exit when c04%notfound;
	begin
	
	update	solic_compra
	set	cd_motivo_baixa	= null,
		dt_baixa	= null
	where	nr_solic_compra	= nr_solic_compra_w;
	
	ds_titulo_w		:= substr(wheb_mensagem_pck.get_texto(336232),1,80);
	ds_hist_solic_compra_w	:= substr(wheb_mensagem_pck.get_texto(336313, 'NR_COT_COMPRA='||to_char(nr_cot_compra_p)),1,2000);
	
	insert into solic_compra_hist(
			nr_sequencia,
			nr_solic_compra,
			ds_historico,
			ds_titulo,
			dt_atualizacao,
			dt_historico,
			nm_usuario)
		values	(solic_compra_hist_seq.nextval,
			nr_solic_compra_w,
			ds_hist_solic_compra_w,
			ds_titulo_w,
			sysdate,
			sysdate,
			nm_usuario_p);
			
	
	open c05;
	loop
	fetch c05 into
		nr_item_solic_compra_w;
	exit when c05%notfound;
		begin
		
		update	solic_compra_item
		set	cd_motivo_baixa		= null,
			dt_baixa		= null
		where	nr_solic_compra		= nr_solic_compra_w
		and	nr_item_solic_compra	= nr_item_solic_compra_w;
		
		end;
	end loop;
	close c05;
	end;
end loop;
close c04;

commit;

END desfazer_cot_compra;
/