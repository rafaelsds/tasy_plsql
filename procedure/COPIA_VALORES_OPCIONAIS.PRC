Create or Replace
Procedure Copia_valores_opcionais(	nr_seq_exame_p		number,
					cd_exame_p		varchar2,
					nm_usuario_p		varchar2) is

nr_seq_exame_w		number(10);

begin


select	max(nr_seq_exame)
into	nr_seq_exame_w
from	exame_laboratorio
where	upper(cd_exame) = upper(cd_exame_p);

insert into exame_lab_valor
	(nr_seq_exame,
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_valor)
select	nr_seq_exame_p,
	nr_sequencia,
	sysdate,
	nm_usuario_p,
	ds_valor
from	exame_lab_valor
where	nr_seq_exame	= nr_seq_exame_w;

commit;
end Copia_valores_opcionais;
/