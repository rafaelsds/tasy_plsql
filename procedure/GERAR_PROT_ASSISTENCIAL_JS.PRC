create or replace
procedure gerar_prot_assistencial_js(   nr_atendimento_p	number,
                                        nm_usuario_p		Varchar2,
                                        nr_seq_resultado_p	number,
                                        nr_sequencia_p		number) as
										
jobno	number;		
joblog varchar2(255);
		
begin

    if nr_seq_resultado_p > 0 then	
    
		joblog := '  GQA_Aprov_Exame_result_item_js('''|| nr_seq_resultado_p || ''',''' ||nr_sequencia_p ||''','''|| nm_usuario_p ||''');';
		
		dbms_job.submit(jobno, joblog);
        
    end if;
    
    if nr_atendimento_p > 0 then 
    
        joblog := ' gera_protocolo_assistencial('''|| nr_atendimento_p || ''',''' || nm_usuario_p ||''');';

        dbms_job.submit(jobno, joblog);
        
    end if;    
   
end gerar_prot_assistencial_js;
/
