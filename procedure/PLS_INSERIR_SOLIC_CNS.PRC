create or replace
procedure pls_inserir_solic_cns(	cd_estabelecimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2) is 

begin

insert into pls_solicitacao_cns (	nr_sequencia,
					cd_pessoa_fisica,
					cd_estabelecimento,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					ie_status	)
			values(		pls_solicitacao_cns_seq.nextval,
					cd_pessoa_fisica_p,
					cd_estabelecimento_p,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					'0');
commit;

end pls_inserir_solic_cns;
/