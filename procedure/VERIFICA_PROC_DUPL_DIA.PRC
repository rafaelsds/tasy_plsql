create or replace
procedure verifica_proc_dupl_dia(	cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_seq_proc_interno_p	number,
					nr_prescricao_p		number,
					ds_retorno_p	out	Varchar2) is
					
cd_pessoa_fisica_w	varchar2(10);
dt_prescricao_w		date;
nr_prescricao_w		number(14);
nm_medico_w		varchar2(60);
ie_suspenso_w		Varchar(10);

Cursor C01 is
	select	r.nr_prescricao,
		substr(obter_nome_medico(r.cd_medico,'N'),1,60),
		decode(p.ie_suspenso,'S','SUSPENSO','')
	from    	prescr_procedimento p,
		prescr_medica r
	where   	p.ie_origem_proced	= ie_origem_proced_p
	and	p.cd_procedimento 	= cd_procedimento_p
	and     	p.nr_prescricao 	= r.nr_prescricao
	and     	r.nr_prescricao 	<> nr_prescricao_p
	and     	trunc(r.dt_prescricao) 	= dt_prescricao_w
	and     	r.cd_pessoa_fisica 	= cd_pessoa_fisica_w
	order by 	r.dt_prescricao;
	
Cursor C02 is
	select  	r.nr_prescricao,
		substr(obter_nome_medico(r.cd_medico,'N'),1,60),
		decode(p.ie_suspenso,'S','SUSPENSO','')
	from    	prescr_procedimento p,
		prescr_medica r
	where   	p.nr_seq_proc_interno	= nr_seq_proc_interno_p
	and     	p.nr_prescricao 	= r.nr_prescricao
	and     	r.nr_prescricao 	<> nr_prescricao_p
	and     	trunc(r.dt_prescricao) 	= dt_prescricao_w
	and     	r.cd_pessoa_fisica 	= cd_pessoa_fisica_w
	order by 	r.dt_prescricao;

begin

ds_retorno_p 	:= '';

select	trunc(dt_prescricao),
	cd_pessoa_fisica
into	dt_prescricao_w,
	cd_pessoa_fisica_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

if 	(nr_seq_proc_interno_p is not null)then
	begin
	open C02;
	loop
	fetch C02 into	
		nr_prescricao_w,
		nm_medico_w,
		ie_suspenso_w;
	exit when C02%notfound;
				ds_retorno_p:= ds_retorno_p 	|| ie_suspenso_w || ' ' 
								|| nr_prescricao_w || ' ' 
								|| nm_medico_w || chr(10);
	end loop;
	close C02;
	end;
else
	begin
	open C01;
	loop
	fetch C01 into	
		nr_prescricao_w,
		nm_medico_w,
		ie_suspenso_w;
	exit when C01%notfound;
				ds_retorno_p:= ds_retorno_p 	|| ie_suspenso_w || ' ' 
								|| nr_prescricao_w || ' ' 
								|| nm_medico_w || chr(10);
	end loop;
	close C01;
	end;
end if;

end verifica_proc_dupl_dia;
/