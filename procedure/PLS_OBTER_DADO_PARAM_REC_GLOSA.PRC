create or replace
function pls_obter_dado_param_rec_glosa (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						ie_opcao_p		varchar2) return varchar2 is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Retorna os parametros em Gest�o de operadoras, para o recurso de glosa
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  X] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:

Op��es:
	II	- Integra ao importar o XML
	AP	- Agrupar protocolo importado via Uploado de XML, no lote mais recente e pendente do usuario
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


ds_retorno_w	varchar2(500) := '';
begin

if	(ie_opcao_p = 'II') then

	select	nvl(max(a.ie_integrar_imp), 'S')
	into	ds_retorno_w
	from	pls_parametros_rec_glosa	a
	where	a.cd_estabelecimento		= cd_estabelecimento_p;

elsif	(ie_opcao_p = 'AP') then

	select	nvl(max(a.ie_agrupar_lote_imp), 'N')
	into	ds_retorno_w
	from	pls_parametros_rec_glosa	a
	where	a.cd_estabelecimento		= cd_estabelecimento_p;

end if;

return ds_retorno_w;

end pls_obter_dado_param_rec_glosa;
/
