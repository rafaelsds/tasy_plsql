create or replace
procedure USC_GERAR_COBR_SANTANDER_400	(nr_seq_cobr_escrit_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p		varchar2) is 
/*Vers�o 2.1 � Outubro de 2014*/
ds_conteudo_w			varchar2(4000);
ds_zeros_16_w			varchar2(16);
ds_mensagem_275_w		varchar2(275);
nr_seq_reg_arq_w		number(6) := 0;
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(15);
dt_geracao_w			varchar2(6);
cd_banco_san_w			varchar2(3);
cd_transmissao_w		varchar2(20);
ds_zeros_374_w			varchar2(374);
ds_brancos_6_w			varchar2(6);
ds_brancos_4_w			varchar2(4);
cd_cgc_w			varchar2(14);
cd_agencia_ced_w		varchar2(4);
cd_conta_movto_w		varchar2(8);
cd_conta_cobr_w			varchar2(8);
nr_controle_w			varchar2(25);
nr_nosso_numero_w		varchar2(8);
dt_seg_desconto_w		varchar2(6);
ie_multa_w			varchar2(1);
pr_multa_w			varchar2(4);
vl_moeda_w			varchar2(2);
vl_tit_unidade_w		varchar2(13);
dt_cobr_multa_w			varchar2(6);
cd_carteira_w			varchar2(1);
cd_ocorrencia_w			varchar2(2);
nr_seu_numero_w			varchar2(10);
dt_vencimento_w			varchar2(6);
ds_vl_titulo_w			varchar2(13);
vl_titulo_w			number(15,2);
cd_banco_w			varchar2(3);
cd_agencia_w			varchar2(5);
ie_documento_w			varchar2(2);
ie_aceite_w			varchar2(1);
dt_emissao_w			varchar2(6);
ie_primeira_instr_w		varchar2(2);
ie_segunda_intr_w		varchar2(2);
vl_juros_diario_w		varchar2(13);
dt_desconto_w			varchar2(6);
vl_desconto_dia_w		varchar2(13);
vl_iof_w			varchar2(13);
vl_abatimento_w			varchar2(13);
ie_tipo_inscricao_w		varchar2(2);
nr_inscricao_w			varchar2(14);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
ds_bairro_sacado_w		varchar2(12);
cd_cep_sacado_w			varchar2(8);
ds_municipio_sacado_w		varchar2(15);
ds_estado_sacado_w		varchar2(2);
nm_sacador_w			varchar2(30);
ie_complemento_w		varchar2(1);
ds_complemento_w		varchar2(2);
nr_dias_baixa_w			varchar2(2);
vl_total_w			number(13,2) := 0;
nr_sequencia_w			number(15);
qt_registro_w			number(6) :=0;
nr_seq_apres_w			number(10) := 1;
ds_mensagem_w			varchar2(50);
nr_titulo_w			titulo_receber.nr_titulo%type;
cd_banco_titulo_w		banco_estabelecimento.cd_banco%type;
cd_agencia_cobradora_w		banco_estabelecimento.cd_agencia_bancaria%type;

Cursor C01 is
select	lpad(nvl(to_char(c.cd_agencia_bancaria),'0'),4,'0') cd_agencia,
	lpad(substr(nvl(d.cd_conta,nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'C'),'0')),1,8),8,'0') cd_conta_movto,
	lpad(substr(nvl(x.cd_conta,'0'),1,8),8,'0') cd_conta_cobr,
	lpad('0',25,'0')  nr_controle,
	nvl(Substr(b.NR_NOSSO_NUMERO,Length(b.NR_NOSSO_NUMERO)-7,8),'00000000') nr_nosso_numero,
	'000000' dt_seg_desconto,
	'4' ie_multa,
	replace(to_char(nvl(b.tx_multa,0), 'fm00.00'),'.','') pr_multa,
	'00' vl_moeda,
	replace(to_char(0, 'fm00000000000.00'),'.','') vl_tit_unidade,
	'000000' dt_cobr_multa,
	'5' cd_carteira,
	'01' cd_ocorrencia,
	lpad(nvl(substr(b.nr_titulo,1,10),'0'),10,'0') nr_seu_numero, -- nr_titulo
	to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
	nvl(b.vl_titulo,0) vl_titulo,
	lpad(nvl(substr(x.cd_banco,1,3),'0'),3,'0') cd_banco,
	lpad(nvl(to_char(c.cd_agencia_bancaria),'0'),5,'0') cd_agencia,
	lpad(nvl(to_char(x.cd_tipo_cobranca), '1'),2,'0') ie_documento,
	'N' ie_aceite,
	lpad(nvl(to_char(b.dt_emissao,'ddmmyy'),' '),6,' ') dt_emissao,
	'00' ie_primeira_instr,
	'00' ie_segunda_intr,   
	lpad(replace(to_char((b.vl_titulo * b.tx_juros/30/100),'fm00000000000.00'),'.',''),13,'0') vl_juros_diario,
	to_char(sysdate,'ddmmyy') dt_desconto,
	lpad('0',13,'0') vl_desconto_dia,
	lpad('0',13,'0') vl_iof,
	lpad('0',13,'0') vl_abatimento,
	decode(b.cd_cgc,null,'01','02') ie_tipo_inscricao, 
	lpad(nvl(b.cd_cgc_cpf,'0'),14,'0') nr_inscricao,
	rpad(upper(elimina_acentuacao(substr(nvl(b.nm_pessoa,' '),1,40))),40,' ') nm_sacado,
	rpad(nvl(substr(decode(d.nr_sequencia,null,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
		pls_obter_compl_pagador(d.nr_seq_pagador,'E')),1,40),
		substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),' '),1,40)),40,' ') ds_endereco_sacado,
	rpad(nvl(substr(decode(d.nr_sequencia,null,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),
		pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,12),
		substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),' '),1,12)),12,' ') ds_bairro_sacado,
	lpad(nvl(substr(decode(d.nr_sequencia,null,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
		pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8),
		substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),'0'),1,8)),8,'0') cd_cep_sacado,
	rpad(nvl(substr(decode(d.nr_sequencia,null,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),
		pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15),
		substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'), ' '),1,15)),15,' ') ds_municipio_sacado,
	rpad(nvl(substr(decode(d.nr_sequencia,null,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
		pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),
		substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),' '),1,2)),2,' ') ds_estado_sacado,
	lpad(' ',30,' ') nm_sacador,
	'I' ie_complemento,
	substr(lpad(substr(nvl(x.cd_conta,'0'),1,8),8,'0'),8,8) || substr(nvl(x.ie_digito_conta,'0'),1,1) ds_complemento,--'38'
	'00' nr_dias_baixa,
	b.nr_titulo,
	x.cd_banco
from	banco_estabelecimento	x,
	pls_contrato_pagador	f,
	banco_carteira		e,
	pls_mensalidade		d,
	titulo_receber_v	b,
	titulo_receber_cobr	c,
	cobranca_escritural	a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo		= b.nr_titulo
and	a.nr_seq_conta_banco	= x.nr_sequencia
and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
and	d.nr_seq_pagador	= f.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
			
			
begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	rpad('0',16,'0'),
	rpad(' ',275,' '),
	rpad('0',374,'0'),
	rpad(' ',6,' '),
	rpad(' ',4,' ')
into	ds_zeros_16_w,
	ds_mensagem_275_w,
	ds_zeros_374_w,
	ds_brancos_6_w,
	ds_brancos_4_w
from	dual;

/* Header */
select	rpad(substr(obter_nome_pf_pj(null,b.cd_cgc),1,30),30,' ') nm_empresa,
	rpad(substr(obter_nome_banco(c.cd_banco),1,15),15,' ') nm_banco,
	to_char(sysdate, 'ddmmyy') dt_geracao,
	lpad(substr(c.cd_banco,1,3),3,'0') cd_banco,
	rpad(nvl(substr(c.cd_transmissao,1,20),' '),20,' ') cd_transmissao,
	lpad(substr(nvl(c.cd_cgc_cedente,b.cd_cgc),1,14),14,'0') cd_cgc,
	lpad(substr(c.cd_agencia_bancaria,1,5),5,0) cd_agencia_bancaria
into	nm_empresa_w,
	nm_banco_w,
	dt_geracao_w,
	cd_banco_san_w,
	cd_transmissao_w,
	cd_cgc_w,
	cd_agencia_cobradora_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

nr_seq_reg_arq_w := nr_seq_reg_arq_w + 1;

ds_conteudo_w	:=	'01REMESSA01COBRAN�A       '	|| 
			cd_transmissao_w		|| 
			nm_empresa_w 			||
			cd_banco_san_w 			|| 
			nm_banco_w			|| 
			dt_geracao_w			|| 
			ds_zeros_16_w			|| 
			ds_mensagem_275_w		||
			'000'				||
			lpad(nr_seq_reg_arq_w,6,'0');

select	w_envio_banco_seq.nextval
into	nr_sequencia_w
from	dual;
			
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);

/* Fim Header  */

/* Registro */
open C01;
loop
fetch C01 into	
	cd_agencia_ced_w,
	cd_conta_movto_w,
	cd_conta_cobr_w,
	nr_controle_w,
	nr_nosso_numero_w,
	dt_seg_desconto_w,
	ie_multa_w,
	pr_multa_w,
	vl_moeda_w,
	vl_tit_unidade_w,
	dt_cobr_multa_w,
	cd_carteira_w,
	cd_ocorrencia_w,
	nr_seu_numero_w,
	dt_vencimento_w,
	vl_titulo_w,	
	cd_banco_w,
	cd_agencia_w,
	ie_documento_w,
	ie_aceite_w,
	dt_emissao_w,
	ie_primeira_instr_w,
	ie_segunda_intr_w,
	vl_juros_diario_w,
	dt_desconto_w,
	vl_desconto_dia_w,
	vl_iof_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w, 
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	ds_bairro_sacado_w,
	cd_cep_sacado_w,
	ds_municipio_sacado_w,
	ds_estado_sacado_w,
	nm_sacador_w,
	ie_complemento_w,
	ds_complemento_w,
	nr_dias_baixa_w,
	nr_titulo_w,
	cd_banco_titulo_w;
exit when C01%notfound;
	begin
	
	ds_vl_titulo_w	:= lpad(replace(to_char(vl_titulo_w,'fm00000000000.00'),'.',''),13,'0');
	
	nr_seq_reg_arq_w := nr_seq_reg_arq_w + 1;
	
	ds_conteudo_w	:=	'102' 			||
				cd_cgc_w		||
				cd_agencia_ced_w 	|| --18 21
				cd_conta_movto_w 	|| --22 29
				cd_conta_cobr_w 	|| --30 37
				nr_controle_w		|| --38 62
				nr_nosso_numero_w	|| --63 70
				dt_seg_desconto_w	|| --71 76
				' '			|| --77 77
				ie_multa_w		|| -- 78 78
				pr_multa_w		||
				vl_moeda_w		||
				vl_tit_unidade_w	||
				ds_brancos_4_w		|| --98
				dt_cobr_multa_w		|| --102
				cd_carteira_w		||
				cd_ocorrencia_w		||
				nr_seu_numero_w		||
				dt_vencimento_w		||
				ds_vl_titulo_w		||
				cd_banco_w		||
				cd_agencia_cobradora_w	|| --143 at� 147
				ie_documento_w		||
				ie_aceite_w		|| --150
				dt_emissao_w		|| --151 156
				ie_primeira_instr_w	||
				ie_segunda_intr_w	||
				vl_juros_diario_w	||
				dt_desconto_w		||
				vl_desconto_dia_w	||
				vl_iof_w		||
				vl_abatimento_w		||
				ie_tipo_inscricao_w	||
				nr_inscricao_w		||
				nm_sacado_w		||
				ds_endereco_sacado_w	||
				ds_bairro_sacado_w	||
				cd_cep_sacado_w		||
				ds_municipio_sacado_w	||
				ds_estado_sacado_w	||
				nm_sacador_w		||
				' '			|| 
				ie_complemento_w	|| --383 383
				ds_complemento_w	||
				ds_brancos_6_w		||
				nr_dias_baixa_w		||
				' '			||
				lpad(nr_seq_reg_arq_w,6,'0');
	
	select	w_envio_banco_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	nr_seq_apres_w := nr_seq_apres_w + 1;
	
	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);
	end;

	vl_total_w := nvl(vl_total_w,0) + nvl(vl_titulo_w,0);
	qt_registro_w := qt_registro_w + 1;
	
end loop;
close C01;
/* Fim Registro */

/* Trailler */
begin
nr_seq_reg_arq_w := nr_seq_reg_arq_w + 1;

ds_conteudo_w	:=	'9' 				|| 
			lpad(nvl(qt_registro_w,0) + 2,6,'0')	||
			lpad(replace(to_char(vl_total_w,'fm000000000.99'),'.',''),13,'0')		||
			ds_zeros_374_w			||
			lpad(nr_seq_reg_arq_w,6,'0');
	
select	w_envio_banco_seq.nextval
into	nr_sequencia_w
from	dual;

nr_seq_apres_w := nr_seq_apres_w + 1;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
		
end;
/* Fim Trailler */

commit;

end USC_GERAR_COBR_SANTANDER_400;
/