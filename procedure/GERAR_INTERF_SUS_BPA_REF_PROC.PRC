create or replace
procedure gerar_interf_sus_bpa_ref_proc
		(	nr_seq_protocolo_p		number,
			nr_folha_bpa_p		number,
			nr_folha_bpi_p		number,
			nm_usuario_p		varchar2) is


nr_sequencia_w		number(10);
nr_interno_conta_w		number(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_pessoa_fisica_ww	varchar2(10);
dt_mesano_referencia_w	date;
dt_mesano_referencia_ww	date;
dt_referencia_proc_w	date;
qt_linhas_w		number(10)	:= 1;
qt_folha_bpa_w		number(6)	:= 0;
nm_orgao_responsavel_w	varchar2(30);
cd_orgao_responsavel_w	varchar2(6);
cd_cgc_responsavel_w	varchar2(14);
nm_orgao_destino_w	varchar2(40);
ie_orgao_destino_w		varchar2(1);
cd_cnes_hospital_w	number(7);
cd_registro_w		number(2);
dt_procedimento_w		date;
cd_cns_medico_exec_w	varchar2(15);
cd_cns_medico_exec_ww	varchar2(15)	:= '0';
cd_cbo_w		varchar2(6);
cd_cbo_ww		varchar2(6)	:= '0';
dt_atend_proc_w		date;
nr_folha_bpa_w		number(10)	:= 0;
nr_folha_bpa_con_w	number(5)	:= 0;
nr_folha_bpa_ind_w	number(5)	:= 0;
nr_linha_folha_w	number(10)	:= 0;
nr_linha_folha_con_w	number(3)	:= 0;
nr_linha_folha_ind_w	number(3)	:= 0;
cd_procedimento_w	number(10);
ie_origem_proced_w	number(10);
cd_cns_paciente_w	varchar2(15);
ie_sexo_pac_w		varchar2(1);
cd_municipio_ibge_w	varchar2(6);
cd_cid_proc_w		varchar2(4);
nr_idade_pac_w		number(3);
qt_procedimento_w		number(10);
cd_carater_atend_w	varchar2(2);
ds_autorizacao_w		varchar2(13);
nm_paciente_w		varchar2(30);
dt_nascimento_w		varchar2(10);
ie_tipo_bpa_w		varchar2(1);
qt_proc_consolidado_w	number(10)	:= 0;
qt_proc_individua_w	number(10)	:= 0;
cd_dominio_w		number(10)	:= 0;
cd_medico_executor_w	varchar2(10);
cd_raca_cor_w		varchar2(2)	:= 99;
cd_cnes_hosp_proc_w	number(7);
cd_estabelecimento_w	number(4);
cd_exec_exp_w		varchar2(10);
cd_cbo_exec_exp_w	varchar2(6);
cd_cgc_prestador_w	varchar2(14);
cd_cnes_prestador_w	number(7);
ie_cnes_prestador_w	varchar2(1);
cd_cgc_prestador_exp_w	varchar2(14);
cd_etnia_w		varchar2(4) 	:= '0000';
cd_nacionalidade_w	varchar2(8) 	:= '   ';
ie_exclui_procedimento_w	varchar2(1)	:= 'N';
qt_existe_proc_w		NUMBER(10)	:= 0;
cd_servico_w		varchar2(3)	:= '0';
cd_serv_classif_w	varchar2(3)	:= '0';
cd_equipe_w		varchar2(12);

tp_logradouro_w		varchar2(3);
cd_cep_w		varchar2(8);
ds_logradouro_w		varchar2(100);
ds_compl_logradouro_w	varchar2(10);
nr_logradouro_w		varchar2(5);
ds_bairro_w		varchar2(30);
ddd_fone_pac_w		varchar2(2);
nr_fone_pac_w		varchar2(9);
email_w			varchar2(40);
cd_cnpj_fornecedor_w	sus_bpa_opme.cd_cnpj_fornecedor%type;
ie_agrupa_bpai_w	varchar2(1) := 'N';
ie_remove_tplogr_end_w	sus_parametros_bpa.ie_remove_tplogr_end%type := 'N';
ds_tipo_logradouro_w	sus_tipo_logradouro.ds_tipo_logradouro%type;
ie_considerar_cns_ant_w	parametro_faturamento.ie_considerar_cns_ant%type;
nr_cartao_nac_sus_ant_w cnes_profissional.nr_cartao_nac_sus_ant%type;
qt_reg_w		number(10) := 0;
qt_reg_commit_w		number(10) := 1000;	
ie_control_num_folha_bpi_w	 sus_parametros_bpa.ie_control_num_folha_bpi%type;	
nr_folha_bpa_cont_w 		 sus_control_folha_bpa_comp.nr_folha_bpi%type;	 
nr_seq_control_folha_w		 sus_control_folha_bpa_comp.nr_sequencia%type;	

/* Obter dados do detalhe */
cursor c01 is
	select	sus_obter_tiporeg_proc(c.cd_procedimento, c.ie_origem_proced, 'C', 1),
		0 nr_interno_conta,
		0 nr_atendimento,
		' ' cd_pessoa_fisica,
		c.cd_pessoa_fisica cd_pessoa_fisica_c,
		' ' cd_cns_medico_exec,
		trunc(c.dt_procedimento,'mm') dt_atendimento,
		c.cd_procedimento,
		c.ie_origem_proced,
		' ' cd_cns_paciente,
		' ' ie_sexo_pac,
		' ' cd_municipio_ibge,
		'' cd_nacionalidade,
		' ' cd_cid_proc,
		s.nr_idade_pac_proc nr_idade_pac,
		sum(c.qt_procedimento) qt_procedimento,
		' ' cd_carater_atendimento,
		' ' ds_autorizacao,
		' ' nm_paciente,
		'' dt_nascimento,
		'C' ie_tipo_bpa,
		' ' cd_profissional,
		' ' cd_raca_cor_w,
		'' cd_etnia_w,
		s.cd_cbo_exp cd_cbo,
		sus_obter_substituir_cnes(cd_cnes_hospital_w, c.cd_setor_atendimento, b.cd_estabelecimento,b.ie_tipo_atendimento),
		cd_cgc_prestador,
		'' cd_servico,
		null cd_serv_classif,
		'' cd_equipe,
		'' tp_logradouro,
		'' cd_cep,
		'' ds_logradouro,
		'' ds_compl_logradouro,
		'' nr_logradouro,
		'' ds_bairro,
		'' ddd_fone_pac,
		'' nr_fone_pac,
		'' email,
		'' cd_cnpj_fornecedor
	from	procedimento_paciente	c,
		atendimento_paciente	b,
		conta_paciente		a,
		sus_valor_proc_paciente s
	where	a.nr_seq_protocolo	= nr_seq_protocolo_p
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_interno_conta	= c.nr_interno_conta
	and	c.ie_origem_proced	= 7
	and	c.cd_motivo_exc_conta	is null
	and	c.nr_sequencia = s.nr_sequencia
	and     sus_obter_tiporeg_proc(c.cd_procedimento, c.ie_origem_proced, 'C', 11) = 1
	and	sus_consiste_proc_comp(b.nr_atendimento, a.nr_interno_conta, c.cd_procedimento) = 'S'
	and	nvl(s.ie_exp_bpa_doa_org, 'S')	= 'S'
	group by	sus_obter_tiporeg_proc(c.cd_procedimento, c.ie_origem_proced, 'C', 1),
		0, 0, ' ', c.cd_pessoa_fisica, ' ', 
		trunc(c.dt_procedimento,'mm'), 
		c.cd_procedimento,
		c.ie_origem_proced, 
		' ', ' ', ' ', '', ' ', 
		s.nr_idade_pac_proc,
		' ', ' ', ' ', '', 'C', ' ', ' ', '',
		s.cd_cbo_exp,
		sus_obter_substituir_cnes(cd_cnes_hospital_w, c.cd_setor_atendimento, b.cd_estabelecimento,b.ie_tipo_atendimento), 
		cd_cgc_prestador,'',null,'','','','','','','','','',''
	union all
	select	sus_obter_tiporeg_proc(c.cd_procedimento, c.ie_origem_proced, 'C', 1),
		a.nr_interno_conta,
		b.nr_atendimento,
		b.cd_pessoa_fisica,
		c.cd_pessoa_fisica cd_pessoa_fisica_c,
		--substr(obter_dados_pf(nvl(c.cd_medico_executor,c.cd_pessoa_fisica),'CNS'),1,15) cd_cns_medico_exec,
		s.cd_cns_medico_exec cd_cns_medico_exec,
		c.dt_procedimento dt_atendimento,
		c.cd_procedimento,
		c.ie_origem_proced,
		substr(obter_dados_pf(b.cd_pessoa_fisica,'CNS'),1,15) cd_cns_paciente,
		substr(obter_sexo_pf(b.cd_pessoa_fisica, 'C'),1,1) ie_sexo_pac,
		substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'CDM'),1,6) cd_municipio_ibge,
		lpad(substr(obter_dados_pf(b.cd_pessoa_fisica,'NC'),1,3),3,'0') cd_nacionalidade,
		c.cd_doenca_cid cd_cid_proc,
		s.nr_idade_pac_proc nr_idade_pac,
		c.qt_procedimento,
		nvl(d.cd_carater_atendimento,ie_carater_inter_sus) ie_carater_inter_sus,
		substr(nvl(nvl(d.nr_bpa,c.nr_doc_convenio),' '),1,13) ds_autorizacao,
		sus_elimina_caracteres_esp(substr(obter_nome_pf(b.cd_pessoa_fisica),1,30)) nm_paciente,
		substr(obter_dados_pf(b.cd_pessoa_fisica,'DN'),1,15) dt_nascimento,
		'I' ie_tipo_bpa,
		--nvl(c.cd_medico_executor, c.cd_pessoa_fisica),
		s.cd_medico_exec_exp cd_medico_executor,
		lpad(sus_obter_cor_pele(b.cd_pessoa_fisica, 'C'),2,'0') cd_raca_cor_w,
		lpad(decode(sus_obter_cor_pele(b.cd_pessoa_fisica, 'C'),'5',sus_obter_etnia(b.cd_pessoa_fisica, 'C'),''),4,'0') cd_etnia_w,
		--nvl(c.cd_cbo, sus_obter_cbo_medico(nvl(c.cd_medico_executor, c.cd_pessoa_fisica),c.cd_procedimento,c.dt_procedimento,0)) cd_cbo,
		s.cd_cbo_exp cd_cbo,
		sus_obter_substituir_cnes(cd_cnes_hospital_w, c.cd_setor_atendimento, b.cd_estabelecimento,b.ie_tipo_atendimento),
		cd_cgc_prestador,
		nvl(to_char((select nvl(cd_servico,0) from sus_servico where nr_sequencia = c.nr_seq_servico)),'0') cd_servico,
		nvl(to_char((select nvl(cd_servico_classif,0) from sus_servico_classif where nr_sequencia = c.nr_seq_servico_classif)),'0') cd_serv_classif,
		'' cd_equipe,
		substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'TLS'),1,3) tp_logradouro,
		substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'CEP'),1,8) cd_cep,
		sus_elimina_caracteres_esp(replace(elimina_acentos(upper(substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'EN'),1,100))),upper(wheb_mensagem_pck.get_texto(1106015)),'C')) ds_logradouro,
		sus_elimina_caracteres_esp(replace(elimina_acentos(upper(substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'CO'),1,10))),upper(wheb_mensagem_pck.get_texto(1106015)),'C')) ds_compl_logradouro,
		upper(substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'NR'),1,5)) nr_logradouro,
		sus_elimina_caracteres_esp(replace(elimina_acentos(upper(substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'B'),1,30))),upper(wheb_mensagem_pck.get_texto(1106015)),'C')) ds_bairro,
		substr(sus_obter_telefone_export(b.cd_pessoa_fisica),1,2) ddd_fone_pac,
		substr(sus_obter_telefone_export(b.cd_pessoa_fisica),3,9) nr_fone_pac,
		substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'M'),1,40) email,
		e.cd_cnpj_fornecedor
	from	sus_bpa_opme		e,
		procedimento_paciente	c,
		atendimento_paciente	b,
		conta_paciente		a,
		sus_bpa_unif		d,
		sus_valor_proc_paciente s
	where	a.nr_seq_protocolo	= nr_seq_protocolo_p
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_interno_conta	= c.nr_interno_conta
	and	a.nr_interno_conta	= d.nr_interno_conta(+)
	and	c.nr_sequencia		= e.nr_seq_procedimento(+)
	and	c.nr_sequencia 		= s.nr_sequencia
	and	c.ie_origem_proced	= 7
	and	c.cd_motivo_exc_conta	is null
	and     sus_obter_tiporeg_proc(c.cd_procedimento, c.ie_origem_proced, 'C', 12) = 2
	and	sus_consiste_proc_comp(b.nr_atendimento, a.nr_interno_conta, c.cd_procedimento) = 'S'
	and	nvl(s.ie_exp_bpa_doa_org, 'S')	= 'S'
	order by	cd_cns_medico_exec,
		dt_atendimento,
		cd_procedimento, 
		cd_cbo, 
		nr_idade_pac;

begin

ie_agrupa_bpai_w	:= nvl(obter_valor_param_usuario(1125,162,obter_perfil_ativo,nm_usuario_p,0),'N');
begin
qt_reg_commit_w		:= nvl(to_number(obter_valor_param_usuario(1125,167,obter_perfil_ativo,nm_usuario_p,0)),1000);
exception
when others then
	qt_reg_commit_w := 1000;
end;

/* Limpar tabela */
delete	from w_susbpa_interf
where	nr_seq_protocolo	= nr_seq_protocolo_p;

/* Obter dados do cabecalho */
select	a.dt_mesano_referencia,
	substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30),
	b.cd_orgao_responsavel,
	substr(obter_cgc_estabelecimento(b.cd_estabelecimento),1,14),
	b.nm_orgao_destino,
	b.ie_orgao_destino,
	sus_obter_substituir_cnes(b.cd_cnes_hospital, a.cd_setor_atendimento, b.cd_estabelecimento,0),
	b.cd_estabelecimento,
	b.ie_cnes_prestador,
	nvl(b.ie_remove_tplogr_end,'N')
into	dt_mesano_referencia_w,
	nm_orgao_responsavel_w,
	cd_orgao_responsavel_w,
	cd_cgc_responsavel_w,
	nm_orgao_destino_w,
	ie_orgao_destino_w,
	cd_cnes_hospital_w,
	cd_estabelecimento_w,
	ie_cnes_prestador_w,
	ie_remove_tplogr_end_w
from	sus_parametros_bpa	b,
	protocolo_convenio	a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.cd_estabelecimento	= b.cd_estabelecimento;

delete  from sus_control_folha_bpa_comp
where	trunc(dt_mesano_referencia,'month') = trunc(dt_mesano_referencia_w,'month')
and     trunc(dt_atualizacao)   <> trunc(sysdate)
and     nr_seq_protocolo	= nr_seq_protocolo_p;

/* Obter o valor do campo DOMINIO, o calculo e feito pelo MOD da soma dos codigos dos procedimentos com suas respectivas quantidades mais 1111 */
select	mod((sum(c.cd_procedimento) + sum(c.qt_procedimento)),1111) + 1111
into	cd_dominio_w
from	procedimento_paciente	c,
	atendimento_paciente	b,
	conta_paciente		a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= c.nr_interno_conta
and	c.ie_origem_proced	= 7;

select	w_susbpa_interf_seq.nextval
into	nr_sequencia_w
from	dual;

select max(nvl(ie_control_num_folha_bpi,'N'))
into ie_control_num_folha_bpi_w
from sus_parametros_bpa
where cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

/* Inserir o registro do cabecalho */
insert into w_susbpa_interf(
		nr_sequencia,
		nr_seq_protocolo,
		nr_interno_conta,
		nr_atendimento,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_registro,
		dt_mesano_referencia,
		qt_linhas,
		qt_folha_bpa,
		nm_orgao_responsavel,
		cd_orgao_responsavel,
		cd_cgc_responsavel,
		nm_orgao_destino,
		ie_orgao_destino,
		ds_versao,
		ie_cabecalho,
		cd_dominio)
	values(	nr_sequencia_w,
		nr_seq_protocolo_p,
		nr_interno_conta_w,
		nr_atendimento_w,
		cd_pessoa_fisica_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'*BPA*',
		dt_mesano_referencia_w,
		qt_linhas_w,
		qt_folha_bpa_w,
		nm_orgao_responsavel_w,
		cd_orgao_responsavel_w,
		cd_cgc_responsavel_w,
		nm_orgao_destino_w,
		ie_orgao_destino_w,
		'*BPABeta4*',
		'C',
		substr(cd_dominio_w,1,4));

commit;

/* Contador de procedimentos consolidados */
select	count(b.cd_procedimento)
into	qt_proc_consolidado_w
from	sus_procedimento_registro	c,
	procedimento_paciente		b,
	conta_paciente 			a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.nr_interno_conta 	= b.nr_interno_conta
and	b.cd_procedimento	= c.cd_procedimento
and	b.ie_origem_proced	= c.ie_origem_proced
and	c.cd_registro		= 1
and	nvl(sus_obter_reg_proc_bpa(c.cd_procedimento,c.ie_origem_proced),c.cd_registro) = 1;

if	(qt_proc_consolidado_w	> 0) then
	nr_folha_bpa_con_w	:= 1;
end if;

if	(nr_folha_bpa_p > 0) then
	nr_folha_bpa_con_w	:= nr_folha_bpa_p + 1;
end if;
if	(nr_folha_bpi_p > 0) then
	nr_folha_bpa_ind_w 	:= nr_folha_bpi_p;
end if;

open c01;
loop
fetch c01 into
	cd_registro_w,
	nr_interno_conta_w,
	nr_atendimento_w,
	cd_pessoa_fisica_w,
	cd_pessoa_fisica_ww,
	cd_cns_medico_exec_w,
	dt_atend_proc_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_cns_paciente_w,
	ie_sexo_pac_w,
	cd_municipio_ibge_w,
	cd_nacionalidade_w,
	cd_cid_proc_w,
	nr_idade_pac_w,
	qt_procedimento_w,
	cd_carater_atend_w,
	ds_autorizacao_w,
	nm_paciente_w,
	dt_nascimento_w,
	ie_tipo_bpa_w,
	cd_medico_executor_w,
	cd_raca_cor_w,
	cd_etnia_w,
	cd_cbo_w,
	cd_cnes_hosp_proc_w,
	cd_cgc_prestador_w,
	cd_servico_w,
	cd_serv_classif_w,
	cd_equipe_w,
	tp_logradouro_w,
	cd_cep_w,
	ds_logradouro_w,
	ds_compl_logradouro_w,
	nr_logradouro_w,
	ds_bairro_w,
	ddd_fone_pac_w,
	nr_fone_pac_w,
	email_w,
	cd_cnpj_fornecedor_w;
exit when c01%notfound;
	begin
	
	qt_reg_w := qt_reg_w + 1;
	
	begin
	select	nvl(ie_considerar_cns_ant, 'N')
	into	ie_considerar_cns_ant_w
	from	parametro_faturamento
	where	cd_estabelecimento=wheb_usuario_pck.get_cd_estabelecimento;
	exception
		when others then
		ie_considerar_cns_ant_w :='N';
	end;
	
	if (ie_considerar_cns_ant_w = 'S') then
		begin
		select	nvl(b.nr_cartao_nac_sus_ant, ' ')
		into	nr_cartao_nac_sus_ant_w
		from 	cnes_profissional 	b
		where	b.cd_pessoa_fisica = nvl(cd_medico_executor_w,cd_pessoa_fisica_ww)
		and 	rownum = 1;
		exception
		when others then
			nr_cartao_nac_sus_ant_w := ' ';
		end;
		if (nr_cartao_nac_sus_ant_w <> ' ') then
			cd_cns_medico_exec_w := nr_cartao_nac_sus_ant_w;
		end if;
		
		begin
		select	nvl(b.nr_cartao_nac_sus_ant, ' ')
		into	nr_cartao_nac_sus_ant_w
		from 	cnes_profissional 	b
		where	b.cd_pessoa_fisica = cd_pessoa_fisica_w
		and 	rownum = 1;
		exception
		when others then
			nr_cartao_nac_sus_ant_w := ' ';
		end;
		if (nr_cartao_nac_sus_ant_w <> ' ') then
			cd_cns_paciente_w := nr_cartao_nac_sus_ant_w;
		end if;
	end if;
	
	/* Obter dados da BPA Unificada */
	if	(ie_tipo_bpa_w	= 'I') then
		begin
		select	nvl(nr_bpa, ''),
			nvl(cd_carater_atendimento, cd_carater_atend_w)
		into	ds_autorizacao_w,
			cd_carater_atend_w
		from	sus_bpa_unif
		where	nr_interno_conta	= nr_interno_conta_w;
		exception
			when others then
			ds_autorizacao_w	:= ds_autorizacao_w;
			cd_carater_atend_w	:= cd_carater_atend_w;
		end;

		cd_carater_atend_w	:= lpad(cd_carater_atend_w,2,'0');
	end if;

	--sus_obter_exec_exp_aih(cd_procedimento_w, 7, cd_medico_executor_w, cd_cbo_w, cd_estabelecimento_w, 'N', 'S', 'N', cd_exec_exp_w, cd_cbo_exec_exp_w);

	qt_existe_proc_w := 0;

	--OS Angelina Caron, problema para agrupar consolidados com mesmo cbo da regra de exportacao. OS 316031

        select 	count(1)
        into	qt_existe_proc_w
        from	w_susbpa_interf
        where	cd_procedimento = cd_procedimento_w
        and	ie_tipo_bpa	= 'C'
        and	cd_cbo		= cd_cbo_w
        and	cd_ups		= cd_cnes_hosp_proc_w
        and	nr_idade_pac	= nr_idade_pac_w
        and	nr_seq_protocolo = nr_seq_protocolo_p
        and	nm_usuario_nrec	= nm_usuario_p
        and	dt_competencia	= dt_atend_proc_w
        and	rownum = 1;

        if	(qt_existe_proc_w > 0) then
                begin
                update	w_susbpa_interf
                set	qt_procedimento = qt_procedimento + qt_procedimento_w
                where	cd_procedimento = cd_procedimento_w
                and	cd_cbo		= cd_cbo_w
                and	cd_ups		= cd_cnes_hosp_proc_w
                and	nr_idade_pac	= nr_idade_pac_w
                and	ie_tipo_bpa	= 'C'
                and	nr_seq_protocolo = nr_seq_protocolo_p
                and	nm_usuario_nrec	= nm_usuario_p
                and	dt_competencia	= dt_atend_proc_w;
                
                if	(qt_reg_w = qt_reg_commit_w) then
                        qt_reg_w := 0;
                        commit;
                end if;
                
                end;
        end if;
	
	sus_obter_prest_exp(cd_procedimento_w, ie_origem_proced_w,cd_cgc_prestador_w,cd_estabelecimento_w,'N','S','N',cd_cgc_prestador_exp_w);

	if	(cd_cgc_prestador_exp_w	is not null) then
		cd_cgc_prestador_w	:= cd_cgc_prestador_exp_w;
	end if;
	
	if	(nvl(ie_agrupa_bpai_w,'N') = 'S') and
		(ie_tipo_bpa_w = 'I') then
		begin
		
		select 	count(1)
		into	qt_existe_proc_w
		from	w_susbpa_interf
		where	cd_procedimento		= cd_procedimento_w
		and	ie_tipo_bpa		= 'I'
		and	cd_cns_medico_exec	= cd_cns_medico_exec_w
		and	cd_cbo			= cd_cbo_w
		and	cd_ups			= cd_cnes_hosp_proc_w
		and	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nvl(ds_autorizacao,'X') = nvl(ds_autorizacao_w,'X')
		and	nr_seq_protocolo 	= nr_seq_protocolo_p
		and	nm_usuario_nrec	 	= nm_usuario_p
		and	rownum = 1;
		
		if	(qt_existe_proc_w > 0) then
			begin
			update	w_susbpa_interf
			set	qt_procedimento = qt_procedimento + qt_procedimento_w
			where	cd_procedimento		= cd_procedimento_w
			and	ie_tipo_bpa		= 'I'
			and	cd_cns_medico_exec	= cd_cns_medico_exec_w
			and	cd_cbo			= cd_cbo_w
			and	cd_ups			= cd_cnes_hosp_proc_w
			and	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	nvl(ds_autorizacao,'X') = nvl(ds_autorizacao_w,'X')
			and	nr_seq_protocolo 	= nr_seq_protocolo_p
			and	nm_usuario_nrec	 	= nm_usuario_p;	

			if	(qt_reg_w = qt_reg_commit_w) then
				qt_reg_w := 0;
				commit;
			end if;
			
			end;
		end if;
		
		end;
	end if;
	
	if	(qt_existe_proc_w = 0) then
		begin
		dt_referencia_proc_w := dt_mesano_referencia_w;
		if	(ie_tipo_bpa_w	= 'C') then
			begin
			cd_medico_executor_w	:= ' ';
			dt_referencia_proc_w := dt_atend_proc_w;
			end;
		end if;

		/* Cada folha possui 20 linhas, se atingir esse limite ou ser outro profissional BPI deve ser criada uma nova folha */
		if	(ie_tipo_bpa_w	= 'C') then
			if	(trunc(dt_referencia_proc_w,'mm') <> trunc(dt_mesano_referencia_ww,'mm')) or
				(nr_linha_folha_w	= 20)  then
				nr_linha_folha_con_w	:= 0;
				nr_folha_bpa_con_w	:= nr_folha_bpa_con_w + 1;
			end if;
		elsif	(ie_tipo_bpa_w	= 'I') then
			if      (nvl(ie_control_num_folha_bpi_w,'N') = 'S') then													
				begin												
				
				if      (cd_cns_medico_exec_w <> cd_cns_medico_exec_ww)  or
					(cd_cbo_w <> cd_cbo_ww) then
					nr_folha_bpa_cont_w := 0;							
					
					if (nvl(nr_folha_bpa_ind_w,0) > 0) then
						sus_gravar_con_fol_bpa_comp(nr_seq_protocolo_p, dt_mesano_referencia_w, cd_cns_medico_exec_ww, cd_cbo_ww, nr_folha_bpa_ind_w, nr_linha_folha_ind_w, nr_seq_control_folha_w);
					end if;
				end if;
			
				if      (nvl(nr_folha_bpa_cont_w,0) = 0) then
					begin
                                        select 	max(nvl(nr_folha_bpi,0)),
                                                        max(nvl(nr_linha_folha_bpi,0)),
                                                        max(nvl(nr_sequencia,0))
                                        into    nr_folha_bpa_cont_w,
                                                        nr_linha_folha_ind_w,
                                                        nr_seq_control_folha_w
                                        from	sus_control_folha_bpa_comp
                                        where	cd_cbo_medico_exec = cd_cbo_w
                                        and		cd_cns_medico_exec = cd_cns_medico_exec_w
                                        and 	trunc(dt_mesano_referencia,'month') = trunc(dt_mesano_referencia_w,'month');
					
					nr_folha_bpa_ind_w := nr_folha_bpa_cont_w;
                                        end;
				end if;							
			
				if      (nvl(nr_seq_control_folha_w,0) = 0) then
					sus_gravar_con_fol_bpa_comp(nr_seq_protocolo_p, dt_mesano_referencia_w, cd_cns_medico_exec_w, cd_cbo_w, null, null, nr_seq_control_folha_w);								
					nr_folha_bpa_ind_w	:= 1;
					nr_linha_folha_ind_w	:= 0;
				end if;
				
				if      (nr_linha_folha_ind_w	= 99) then
					nr_linha_folha_ind_w	:= 0;
					if	(nr_folha_bpa_ind_w < 98901) then
						nr_folha_bpa_ind_w	:= nr_folha_bpa_ind_w + 1;
					else
						wheb_mensagem_pck.exibir_mensagem_abort(279833);				
					end if;	
				end if;							
				end; 							
			elsif	(nr_linha_folha_w	= 99) or
				(cd_cns_medico_exec_w	<> cd_cns_medico_exec_ww) or
				(cd_cbo_w 		<> cd_cbo_ww) then
				nr_linha_folha_ind_w	:= 0;
				if	(nr_folha_bpa_ind_w < 98901) then
					nr_folha_bpa_ind_w	:= nr_folha_bpa_ind_w + 1;
				else
					wheb_mensagem_pck.exibir_mensagem_abort(279833);
				end if;
			end if;
		end if;

		/* Contador de linhas do arquivo */
		qt_linhas_w		:= qt_linhas_w + 1; /* Numero de linhas do BPA gravadas */

		/* Se o procedimento for consolidado entao sera inserida as folhas e o numero da linha do consolidado se nao do Individualizado */
		if	(ie_tipo_bpa_w	= 'C') then
			nr_linha_folha_con_w	:= nr_linha_folha_con_w + 1;
			nr_linha_folha_w	:= nr_linha_folha_con_w;
			nr_folha_bpa_w		:= nr_folha_bpa_con_w;
		elsif	(ie_tipo_bpa_w	= 'I') then
			nr_linha_folha_ind_w	:= nr_linha_folha_ind_w + 1;
			nr_linha_folha_w	:= nr_linha_folha_ind_w;
			nr_folha_bpa_w		:= nr_folha_bpa_ind_w;
		end if;

		/* Felipe - 28/02/2008 - OS 80604 - Esse codigo e feito de 7 em 7 pacientes */
		if	(cd_procedimento_w	= 0301080160) then
			qt_procedimento_w	:= trunc(dividir(qt_procedimento_w,7));
		end if;

		if	(ie_cnes_prestador_w		= 'S') and
			(cd_cgc_prestador_w		<> cd_cgc_responsavel_w) then
			begin
			select	nvl(max(cd_cnes),0)
			into	cd_cnes_prestador_w
			from	pessoa_juridica
			where	cd_cgc	= cd_cgc_prestador_w;
			exception
			when others then
				cd_cnes_prestador_w := 0;
			end;
			if	(nvl(cd_cnes_prestador_w,0) > 0) then
				cd_cnes_hosp_proc_w	:= cd_cnes_prestador_w;
			end if;
		end if;
		
		if	(ie_remove_tplogr_end_w = 'S') and
			(length(ds_logradouro_w) > 30) then
			begin
			ds_tipo_logradouro_w	:= substr(sus_obter_desc_tipo_logr(tp_logradouro_w),1,40);
			
			if	(upper(substr(ds_logradouro_w,1,length(ds_tipo_logradouro_w)+1)) = upper(ds_tipo_logradouro_w)||' ') then
				ds_logradouro_w	:= substr(substr(ds_logradouro_w,length(ds_tipo_logradouro_w)+2,length(ds_logradouro_w)),1,30);
			end if;					
			end;
		elsif	(ie_remove_tplogr_end_w = 'T') and
			(tp_logradouro_w is not null) then
			begin
			ds_tipo_logradouro_w	:= substr(sus_obter_desc_tipo_logr(tp_logradouro_w),1,40);
			
			if	(upper(substr(ds_logradouro_w,1,length(ds_tipo_logradouro_w)+1)) = upper(ds_tipo_logradouro_w)||' ') then
				ds_logradouro_w	:= substr(substr(ds_logradouro_w,length(ds_tipo_logradouro_w)+2,length(ds_logradouro_w)),1,30);
			end if;					
			end;
		else	
			ds_logradouro_w := substr(ds_logradouro_w,1,30);
		end if;

		select	w_susbpa_interf_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into w_susbpa_interf(
				nr_sequencia,
				nr_seq_protocolo,
				nr_interno_conta,
				nr_atendimento,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_registro,
				dt_mesano_referencia,
				qt_linhas,
				qt_folha_bpa,
				nm_orgao_responsavel,
				cd_orgao_responsavel,
				cd_cgc_responsavel,
				nm_orgao_destino,
				ie_orgao_destino,
				ds_versao,
				cd_ups,
				dt_competencia,
				cd_cns_medico_exec,
				cd_cbo,
				dt_procedimento,
				nr_folha_bpa,
				nr_linha_folha,
				cd_procedimento,
				cd_cns_paciente,
				ie_sexo_pac,
				cd_municipio_ibge,
				cd_cid_proc,
				nr_idade_pac,
				qt_procedimento,
				cd_carater_atendimento,
				ds_autorizacao,
				ds_origem_informacao,
				nm_paciente,
				dt_nascimento,
				ie_tipo_bpa,
				ie_cabecalho,
				cd_dominio,
				cd_raca_cor,
				cd_etnia,
				cd_nacionalidade,
				cd_servico,
				cd_servico_classif,
				cd_equipe,
				tp_logradouro,
				cd_cep,
				ds_logradouro,
				ds_compl_logradouro,
				nr_logradouro,
				ds_bairro,
				ddd_fone_pac,
				nr_fone_pac,
				ds_email,
				cd_cnpj_fornecedor)				
		values(		nr_sequencia_w,
				nr_seq_protocolo_p,
				nr_interno_conta_w,
				nr_atendimento_w,
				cd_pessoa_fisica_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				'*BPA*',
				dt_mesano_referencia_w,
				qt_linhas_w,
				qt_folha_bpa_w,
				nm_orgao_responsavel_w,
				cd_orgao_responsavel_w,
				cd_cgc_responsavel_w,
				nm_orgao_destino_w,
				ie_orgao_destino_w,
				'*BPABeta4*',
				nvl(cd_cnes_hosp_proc_w, cd_cnes_hospital_w),
				dt_referencia_proc_w,
				cd_cns_medico_exec_w,
				cd_cbo_w,
				dt_atend_proc_w,
				nr_folha_bpa_w,
				nr_linha_folha_w,
				cd_procedimento_w,
				cd_cns_paciente_w,
				ie_sexo_pac_w,
				cd_municipio_ibge_w,
				cd_cid_proc_w,
				to_number(nr_idade_pac_w),
				qt_procedimento_w,
				cd_carater_atend_w,
				ds_autorizacao_w,
				'BPA',
				nm_paciente_w,
				to_date(dt_nascimento_w,'dd/mm/yyyy'),
				ie_tipo_bpa_w,
				'P',
				substr(cd_dominio_w,1,4),
				cd_raca_cor_w,
				cd_etnia_w,
				cd_nacionalidade_w,
				cd_servico_w,
				cd_serv_classif_w,
				cd_equipe_w,
				tp_logradouro_w,
				cd_cep_w,
				ds_logradouro_w,
				ds_compl_logradouro_w,
				nr_logradouro_w,
				ds_bairro_w,
				ddd_fone_pac_w,
				nr_fone_pac_w,
				email_w,
				cd_cnpj_fornecedor_w);

		cd_cns_medico_exec_ww	:= cd_cns_medico_exec_w;
		cd_cbo_ww		:= cd_cbo_w;
		dt_mesano_referencia_ww := dt_referencia_proc_w;

		if	(qt_reg_w = qt_reg_commit_w) then
			qt_reg_w := 0;
			commit;
		end if;
		
		end;
	end if;
	end;
end loop;
close c01;

if (nvl(ie_control_num_folha_bpi_w,'N') = 'S') then													
	sus_gravar_con_fol_bpa_comp(nr_seq_protocolo_p, dt_mesano_referencia_w, cd_cns_medico_exec_w, cd_cbo_w, nr_folha_bpa_ind_w, nr_linha_folha_ind_w, nr_seq_control_folha_w);																		
end if;	

/* Somatorio da quantidade total de folhas do arquivo */
qt_folha_bpa_w	:= nr_folha_bpa_con_w + nr_folha_bpa_ind_w;

/* Atualizar na tabela temporaria a quantidade linhas e de folhas do arquivo */
update	w_susbpa_interf
set	qt_linhas		= qt_linhas_w,
	qt_folha_bpa		= qt_folha_bpa_w
where	nr_seq_protocolo	= nr_seq_protocolo_p;

commit;

end gerar_interf_sus_bpa_ref_proc;
/
