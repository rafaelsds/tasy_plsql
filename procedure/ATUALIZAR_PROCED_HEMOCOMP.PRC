create or replace
procedure atualizar_proced_hemocomp( 	nr_atendimento_p 		number,
					nr_sequencia_p			number,
					nr_seq_solic_bco_p		number,
					cd_estabelecimento_p		number,
					ie_pasta_p			varchar2,
					ie_irradiado_p			varchar2,
					ie_lavado_p			varchar2,
					ie_filtrado_p			varchar2,
					ie_aliquotado_p			varchar2,
					nm_usuario_p			varchar2,
					cd_convenio_p		out	number, 
					cd_setor_p		in out	number,
					cd_procedimento_p 	out	number, 
					ie_origem_proced_p 	out	number, 
					nr_seq_proc_interno_p 	out 	number
				     ) is 

-- ie_pasta_p ( H- Hemocomponente  e S - Solic Testes )

ie_derivado_exame_w 	number(10,0);
ds_erro_w		varchar2(510);
ie_tipo_atend_w		number(3,0);
ie_tipo_convenio_w	number(2);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
cd_setor_w		number(5);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10);
ie_nova_consiste_w	varchar2(1);


begin

if ( 'H' = ie_pasta_p) then
	ie_derivado_exame_w 	:= 0;
	ie_nova_consiste_w := obter_valor_param_usuario(924, 574, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
	if ( ie_nova_consiste_w = 'N') then
	begin
		-- Consiste_solic_hemocomponente
		Consiste_solic_hemocomponente(nr_seq_solic_bco_p, nr_sequencia_p, cd_estabelecimento_p, nm_usuario_p, ds_erro_w);
		if ( ds_erro_w is not null) then
			-- #@DS_ERRO#@
			Wheb_mensagem_pck.exibir_mensagem_abort( 193870 , 'DS_ERRO=' || ds_erro_w );
		end if;
	end;
	end if;
elsif ('S' = ie_pasta_p) then
	ie_derivado_exame_w 	:= 1;
end if;

cd_convenio_w		:= obter_convenio_atendimento(nr_atendimento_p);
ie_tipo_atend_w		:= obter_tipo_atendimento(nr_atendimento_p);
ie_tipo_convenio_w	:= obter_tipo_convenio(cd_convenio_w);
cd_categoria_w		:= obter_categoria_atendimento(nr_atendimento_p);

cd_setor_w:= cd_setor_p;

Obter_Proced_sangue(ie_derivado_exame_w, nr_sequencia_p, cd_estabelecimento_p, ie_tipo_atend_w, ie_tipo_convenio_w, cd_convenio_w, cd_categoria_w, cd_setor_w, cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w,ie_irradiado_p,ie_lavado_p,ie_filtrado_p,ie_aliquotado_p);

cd_convenio_p		:= cd_convenio_w;
cd_setor_p		:= cd_setor_w;
cd_procedimento_p 	:= cd_procedimento_w;
ie_origem_proced_p	:= ie_origem_proced_w; 	
nr_seq_proc_interno_p	:= nr_seq_proc_interno_w;

commit;

end atualizar_proced_hemocomp;
/