create or replace
procedure pls_oc_cta_aplic_val_sexo_proc(
					dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de Sexo exclusivo do procedimento em cima das contas ou itens
	que foram filtrados pelos filtros da regra e verificar se os mesmos continuam v�lidos
	ou n�o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
 ------------------------------------------------------------------------------------------------------------------
 jjung OS 601977 10/06/2013 - Cria��o da procedure.
 ------------------------------------------------------------------------------------------------------------------
 jjung OS 602057 - 18/06/2013
 
 Altera��o:	Retirado o campo dados_filtro_w.ieececao e substituido pelo ie_gera_ocorrencia.
 
 Motivo:	Foi identificado que a l�gica do campo ie_excecao era muito confusa e poderia
	trazer problemas.
 ------------------------------------------------------------------------------------------------------------------
 jjung 29/06/2013 

Altera��o:	Adicionado parametro nos m�todos de atualiza��o dos campos IE_VALIDO e IE_VALIDO_TEMP 
	da PLS_TIPOS_OCOR_PCK
	
Motivo:	Se tornou necess�rio diferenciar os filtros das valida��es na hora de realizar esta opera��o
	para que os filtros de exce��o funcionem corretamente.
------------------------------------------------------------------------------------------------------------------
 jjung 25/07/2013 

Altera��o:	Alterado forma de buscar os procedimentos que est�o na tabela de sele��o. Tamb�m foi alterado 
	a forma de verifica��o do sexo do procedimento para que n�o considere procedimento nem
	benefici�rio com sexo indeterminado.
	
Motivo:	Foi visto que a ocorr�ncia estava sendo gerada quando o benefici�rio ou o procedimento
	tinham o sexo como Indeterminado o que se entende que n�o � o certo.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
null;
end pls_oc_cta_aplic_val_sexo_proc;
/