CREATE OR REPLACE
PROCEDURE Gerar_View_Fluxo_Caixa_Relat (
				cd_estabelecimento_p	Number,
				Dt_inicio_p		Date,
				dt_final_p     		Date,    
				dt_referencia_p		date,
				ie_periodo_p		Varchar2,
				ie_classif_p		Varchar2,
				ie_nivel_p		Number,
				cd_empresa_p		number,
				ie_restringe_estab_p	varchar2) IS

/*ie_classif_p

'PGRM' - Realizado para rel de fluxo do mes - vem da procedure GERAR_FLUXO_CAIXA_MENSAL (todos os dias)
'RGRM' - A realizar para rel de fluxo do mes - vem da procedure GERAR_FLUXO_CAIXA_MENSAL (todos os dias)
'PRGMRM' - Ralizado/Ralizar para rel de fluxo do mes - vem da procedure GERAR_FLUXO_CAIXA_MENSAL (todos os dias)
*/

ds_comando_w		Varchar2(32000) := '';
vl_fluxo_w		Varchar2(32000) := '';
dt_fluxo_w		Date;
Virgula_w		Varchar2(0001)  := '';
ds_nivel_w		Varchar2(255)   := '';
qt_nivel_w		number(2,0);
cd_conta_saldo_w	number(10,0);
cd_conta_saldo_nt_w	number(10,0);
contador_w		number(10,0);
ds_data_w		varchar2(40);
ie_passado_real_w	varchar2(40);
ds_sql_where_w		varchar2(4000);
cd_conta_saldo_ant_w 	number(10,0);
ds_alias_w		varchar2(40);
qt_coluna_w		number(10)	:= 1;
vl_fluxo_aux_w		varchar2(32000) := '';
ds_semana_w		varchar2(10000)	:= '';
qt_semana_w		number(10);
ie_periodo_w		varchar2(1);
ds_titulo_semana_aux_w	varchar2(40);
ds_titulo_semana_w	varchar2(10000)	:= '';
ds_semana_dia_w		varchar2(32000)	:= '';
qt_dia_w		number(10);
dt_ultima_semana_w	date;

BEGIN

if	(ie_periodo_p = 'S') then
	ie_periodo_w	:= 'D';
else
	ie_periodo_w	:= ie_periodo_p;
end if;

/* Obter a conta de Saldo */
select	cd_conta_financ_saldo,
	cd_conta_financ_sant,
	ie_passado_real
into 	cd_conta_saldo_w,
	cd_conta_saldo_ant_w,
	ie_passado_real_w
from 	Parametro_Fluxo_caixa
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_periodo_w	= 'D') and
	((dt_final_p - dt_inicio_p) > 366) then
	--Periodo Superior a 366 dias.
	Wheb_mensagem_pck.exibir_mensagem_abort(235399);
end if;
if	(ie_nivel_p	> 6) or
	(ie_nivel_p = 0) then
	--O Nivel deve ser de 1 a 6.
	Wheb_mensagem_pck.exibir_mensagem_abort(235400);
end if;

Exec_sql_Dinamico('Tasy', 'drop view Fluxo_Caixa_v');

if	(nvl(ie_classif_p, '-1')	<> 'C') then

	if	(ie_periodo_w	= 'M') then
		dt_fluxo_w		:= last_Day(trunc(dt_inicio_p, 'dd'));
	else	
		dt_fluxo_w		:= trunc(dt_inicio_p, 'dd');
	end if;

	contador_w	:= 1;
	qt_semana_w	:= 1;
	qt_dia_w	:= 1;

	while dt_fluxo_w <= dt_final_p LOOP
		begin

		if	(nvl(ie_classif_p, '-1') not in ('PRM','RRM','RM','RPR')) or 	 /* for�ar somente dias uteis qdo RM   */
			(trunc(obter_proximo_dia_util(cd_estabelecimento_p, dt_fluxo_w), 'dd') = trunc(dt_fluxo_w, 'dd')) then

			if	(nvl(ie_classif_p,'-1') in ('PRM','RRM','RM','RPR')) then /* Se a gera��o � de relat�rio mensal */
				ds_data_w		:= to_char(contador_w);
				contador_w		:= contador_w + 1;
				if	(ie_passado_real_w = 'S') and
					(ie_periodo_w = 'D') and
					(nvl(ie_classif_p, '-1') = 'RPR') and
					(trunc(dt_fluxo_w, 'dd') = trunc(dt_referencia_p, 'dd')) then /* ahoffelder - OS 310509 - 13/04/2011 */

					vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
							'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
							to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
							'dd/mm/yyyy' || chr(39) || '), decode(ie_classif_fluxo, ' || chr(39) ||  'P' || chr(39) || 
							', vl_fluxo, 0),0)) ' || 
							'D' || ds_data_w;
					virgula_w	:= ',';
					ds_data_w	:= to_char(contador_w);
					contador_w	:= contador_w + 1;
					vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
							'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
							to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
							'dd/mm/yyyy' || chr(39) || '), decode(ie_classif_fluxo, ' || chr(39) ||  'R' || chr(39) || 
							', vl_fluxo, 0),0)) ' || 
							'D' || ds_data_w;
				else
					vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
							'sum(decode(dt_referencia, to_date(' || chr(39) || 
							to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
							'dd/mm/yyyy' || chr(39) || ')' || ',vl_fluxo,0)) ' || 
							'D' || ds_data_w;
				end if;
			else
				if	(nvl(ie_classif_p, '-1') in ('PGRM','RGRM','PRGMRM')) then
					if	(ie_periodo_w = 'D') then
						ds_data_w	:= substr(to_char(qt_coluna_w,'00'),2,2);
						qt_coluna_w	:= qt_coluna_w + 1;
					elsif	(ie_periodo_w = 'M') then
						ds_data_w	:= to_char(dt_fluxo_w,'ddmm');
					end if;
				else
					ds_data_w		:= to_char(dt_fluxo_w,'ddmmyyyy');
				end if;

				-- Edgar 04/12/2007, OS 74953
				if	(ie_passado_real_w = 'S') and
					(((trunc(dt_fluxo_w, 'dd') = trunc(dt_referencia_p, 'dd')) or
					 ((ie_periodo_w	= 'M') and (trunc(dt_fluxo_w, 'month') = trunc(dt_referencia_p, 'month'))))) then 

					vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
							'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
							to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
							'dd/mm/yyyy' || chr(39) || '), decode(ie_classif_fluxo, ' || chr(39) ||  'P' || chr(39) || 
							', vl_fluxo, 0),0)) ' || 
							'D' || ds_data_w;
					
					if	(ie_periodo_p = 'S') then
						if	(vl_fluxo_aux_w is not null) then
							vl_fluxo_aux_w	:= vl_fluxo_aux_w || ' + ';
						end if;
						vl_fluxo_aux_w	:=  vl_fluxo_aux_w ||
								'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
								to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
								'dd/mm/yyyy' || chr(39) || '), decode(ie_classif_fluxo, ' || chr(39) ||  'P' || chr(39) || 
								', vl_fluxo, 0),0)) ';
					end if;
					
				else
					if	(nvl(ie_classif_p,'1')	<> 'POM') then
						vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
								'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
								to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
								'dd/mm/yyyy' || chr(39) || ')' || ',vl_fluxo,0)) ' || 
								'D' || ds_data_w;
								
						if	(ie_periodo_p = 'S') then
							if	(vl_fluxo_aux_w is not null) then
								vl_fluxo_aux_w	:= vl_fluxo_aux_w || ' + ';
							end if;
							vl_fluxo_aux_w	:=  vl_fluxo_aux_w ||
									'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
									to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
									'dd/mm/yyyy' || chr(39) || ')' || ',vl_fluxo,0)) ';
						end if;
					else
						vl_fluxo_w	:=  	vl_fluxo_w || virgula_w ||
									'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
									to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
									'dd/mm/yyyy' || chr(39) || ')' || ',vl_fluxo,0)) ' || 
									'P' || ds_data_w || ', ' ||
									' max(OBTER_VALOR_FLUXO_ORCADO( 	' || cd_estabelecimento_p || ', ' || 
									' a.cd_conta_financ,' ||
									' add_months(to_date(' || chr(39) || to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || '),-1), ' ||
									' to_date(' || chr(39) || to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || '),' || chr(39) || cd_empresa_p || chr(39) || ', ' || chr(39) || ie_restringe_estab_p || chr(39) || ')) O' || ds_data_w;
					end if;
					
				end if;
			end if;
			virgula_w	:= ',';
		end if;
		
		if	(ie_periodo_p = 'S') and
			(qt_dia_w = 7) then
			
			ds_titulo_semana_aux_w	:= 1 ||'T' || to_char(dt_fluxo_w - 6,'ddmm') || to_char(dt_fluxo_w,'ddmm');
			
			if	(qt_semana_w = 1) then
				ds_semana_w		:= vl_fluxo_aux_w || 'S' || qt_semana_w;
				ds_titulo_semana_w	:= ds_titulo_semana_aux_w;
			else
				ds_semana_w		:= ds_semana_w || ', ' || vl_fluxo_aux_w || 'S' || qt_semana_w;
				ds_titulo_semana_w	:= ds_titulo_semana_w || ', ' || ds_titulo_semana_aux_w;
			end if;
			
			vl_fluxo_aux_w		:= '';
			qt_dia_w		:= 1;
			qt_semana_w		:= qt_semana_w + 1;
			dt_ultima_semana_w	:= dt_fluxo_w + 1;
			
		elsif	(dt_fluxo_w = dt_final_p) then
			ds_titulo_semana_aux_w	:= 1 ||'T' || to_char(dt_ultima_semana_w,'ddmm') || to_char(dt_fluxo_w,'ddmm');
			
			if	(qt_semana_w = 1) then
				ds_semana_w		:= vl_fluxo_aux_w || 'S' || qt_semana_w;
				ds_titulo_semana_w	:= ds_titulo_semana_aux_w;
			else
				ds_semana_w		:= ds_semana_w || ', ' || vl_fluxo_aux_w || 'S' || qt_semana_w;
				ds_titulo_semana_w	:= ds_titulo_semana_w || ', ' || ds_titulo_semana_aux_w;
			end if;
		else
			qt_dia_w := qt_dia_w + 1;
		end if;
		
		if	(ie_periodo_w	= 'M') then
			begin
			dt_fluxo_w		:= add_months(dt_fluxo_w, 1);
			dt_fluxo_w		:= last_Day(trunc(dt_fluxo_w, 'dd'));
			end;
		else	
			dt_fluxo_w		:= dt_fluxo_w + 1;
		end if;
		
		end;
	END LOOP;

	if	(nvl(ie_classif_p, '-1') in ('PGRM','RGRM','PRGMRM')) then

		if	(ie_periodo_w = 'D') then
			ds_alias_w	:= 'D' || substr(to_char(qt_coluna_w,'00'),2,2);
		else
			ds_alias_w	:= obter_desc_expressao(711564);--'Total';
		end if;

		-- Edgar 30/11/2007, OS 75728, tratar todais das CFs de saldo
		vl_fluxo_w	:=  vl_fluxo_w || virgula_w || 
					'SUM(decode(	cd_conta_financ, ' || 
								cd_conta_saldo_ant_w || ', ' ||
									' decode(dt_referencia, to_date(' || chr(39) || to_char(Dt_inicio_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') , vl_fluxo, 0), ' ||
								cd_conta_saldo_w || ', ' ||
									' decode(dt_referencia, to_date(' || chr(39) || to_char(Dt_final_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') , vl_fluxo, 0), ' ||
						' decode(CD_CONTA_FINANC, ' || cd_conta_saldo_w || ', 0, ' 
							|| cd_conta_saldo_ant_w || ', 0, vl_fluxo))) ' || ds_alias_w;
	else 
		vl_fluxo_w	:=  vl_fluxo_w || virgula_w || 
						' decode(obter_se_totaliza_cf(cd_conta_financ), ' || chr(39) || 'S' || chr(39) || ', decode(CD_CONTA_FINANC, ' || 
								cd_conta_saldo_w || ', ' ||
										'obter_valor_fluxo_saldo(' || cd_estabelecimento_p || ', ' || 
													chr(39) || ie_classif_p || chr(39) || ', ' || 
													chr(39) || ie_periodo_w || chr(39) || ', ' || 
													'to_date(' || chr(39) || to_char(Dt_inicio_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' || ', ' || 
													'to_date(' || chr(39) || to_char(Dt_final_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' || ', ' || 
													'to_date(' || chr(39) || to_char(dt_referencia_p, 'dd/mm/yyyy') || chr(39) ||  ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ' ), ' || 
													'2), ' || 
								cd_conta_saldo_ant_w || ', ' || 
										'obter_valor_fluxo_saldo(' || cd_estabelecimento_p || ', ' || 
													chr(39) || ie_classif_p || chr(39) || ', ' || 
													chr(39) || ie_periodo_w || chr(39) || ', ' || 
													'to_date(' || chr(39) || to_char(Dt_inicio_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' || ', ' || 
													'to_date(' || chr(39) || to_char(Dt_final_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' || ', ' || 
													'to_date(' || chr(39) || to_char(dt_referencia_p, 'dd/mm/yyyy') || chr(39) ||  ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ' ), ' || 
													'1), ' || 
								'sum(vl_fluxo)),  0) Total';
	end if;

	/* Edgar 02/08/2004 - OS 9996, tratamento do 4� n�vel da conta */
	select	nvl(max(length(CD_CONTA_APRES)), 9)
	into	qt_nivel_w
	from	conta_financeira
	where	cd_empresa	= cd_empresa_p;

	if	(qt_nivel_w = 9) then
		if	(ie_nivel_p = 3) then
			ds_nivel_w	:= '';
		else
			ds_nivel_w	:= ' and cd_conta_financ in (select y.cd_conta_financ from conta_financeira y ';
			if	(ie_nivel_p = 2) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,7,3) = ' ||
						chr(39) || '000' || chr(39) || ')';
			else
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,4,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			end if;
		end if;
	elsif	(qt_nivel_w = 15) then
		if	(ie_nivel_p = 5) then
			ds_nivel_w	:= '';
		else
			ds_nivel_w	:= ' and cd_conta_financ in (select y.cd_conta_financ from conta_financeira y ';
			if	(ie_nivel_p = 4) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,13,3) = ' ||
						chr(39) || '000' || chr(39) || ')';
			elsif	(ie_nivel_p = 3) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,10,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			elsif	(ie_nivel_p = 2) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,7,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			else
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,4,2) = ' ||
					chr(39) || '00' || chr(39) || ')';
			end if;
		end if;
	else
		if	(ie_nivel_p = 4) then
			ds_nivel_w	:= '';
		else
			ds_nivel_w	:= ' and cd_conta_financ in (select y.cd_conta_financ from conta_financeira y ';
			if	(ie_nivel_p = 3) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,10,3) = ' ||
						chr(39) || '000' || chr(39) || ')';
			elsif	(ie_nivel_p = 2) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,7,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			else
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,4,2) = ' ||
					chr(39) || '00' || chr(39) || ')';
			end if;
		end if;
	end if;

	/* Edgar 12/07/2005, tratamento do valor orcado no final do fluxo */
	vl_fluxo_w	:=  vl_fluxo_w || virgula_w || 	
			' OBTER_VALOR_FLUXO_ORCADO( 	' || cd_estabelecimento_p || ', ' || 
							' a.cd_conta_financ,' ||
							' to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || '), ' ||
							' to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || '),' || cd_empresa_p || ', ' || chr(39) ||ie_restringe_estab_p || chr(39) || ') vl_orcado ';

	/* Edgar 28/11/2006, tratamento do or�ado - realizado */
	if	(ie_classif_p = 'P') then
		vl_fluxo_w	:=	vl_fluxo_w || virgula_w || ' (OBTER_VALOR_FLUXO_ORCADO(' || cd_estabelecimento_p || ', ' || 
							' a.cd_conta_financ,' ||
							' to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || '), ' ||
							' to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || '),' || cd_empresa_p || ', ' || chr(39) || ie_restringe_estab_p || chr(39) || ') - ' ||
							' decode(CD_CONTA_FINANC, ' || cd_conta_saldo_w || ', 0, ' || 
							cd_conta_saldo_ant_w || ', 0, sum(vl_fluxo))) * (decode(substr(obter_descricao_padrao(' || chr(39) || 'CONTA_FINANCEIRA' || chr(39) || ',' ||
							chr(39) || 'IE_OPER_FLUXO' || chr(39) || ',a.cd_conta_financ),1,1),' || chr(39) || 'S' || chr(39) || ',-1,' || chr(39) || 'D' || chr(39) || ',1,decode(substr(obter_descricao_padrao(' || chr(39) || 'CONTA_FINANCEIRA' || chr(39) || ',' ||
							chr(39) || 'IE_VARIACAO' || chr(39) || ',a.cd_conta_financ),1,1),' || chr(39) || 'S' || chr(39) || ',-1,' || chr(39) || 'D' || chr(39) || ',1,0))) vl_orcado_passado ';
	end if; -- Paulo - OS 45404 - 12/12/2006 - Alterei o comando acima para, se for entrada multiplicar por -1.

	-- Edgar 20/09/20005, OS 22253, Fiz um tratamento para trazer realizado at� dt_referencia e a realizar ap�s esta data
	if	(dt_referencia_p is null) then
		if	(ie_periodo_p = 'S') then
			ds_comando_w	:= 'create or replace view fluxo_caixa_v as ' ||
					' select cd_conta_financ, ' || vl_fluxo_w || ', ' || ds_semana_w || ', ' || ds_titulo_semana_w ||
					' from fluxo_caixa a ' ||
					' where ie_classif_fluxo = ' || chr(39) || substr(ie_classif_p,1,1) || chr(39) ||
					'   and ie_periodo = ' || chr(39) || ie_periodo_w || chr(39) ||
					'   and cd_estabelecimento = ' || cd_estabelecimento_p ||
					' and ((obter_se_filtro_fluxo(a.nr_seq_conta_banco, null, null, wheb_usuario_pck.get_nm_usuario, null) =' || chr(39) || 'S' || chr(39) || ') or (ie_origem<>' || chr(39) || 'D' || chr(39) || '))' ||					
					'   and dt_referencia between ' || 
 					'	to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')' ||
 					' and	to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' ||
					ds_nivel_w || ' Group By cd_conta_financ';
		else
			ds_comando_w	:= 'create or replace view fluxo_caixa_v as ' ||
					' select cd_conta_financ, ' || vl_fluxo_w ||
					' from fluxo_caixa a ' ||
					' where ie_classif_fluxo = ' || chr(39) || substr(ie_classif_p,1,1) || chr(39) ||
					'   and ie_periodo = ' || chr(39) || ie_periodo_w || chr(39) ||
					'   and cd_estabelecimento = ' || cd_estabelecimento_p ||
					' and ((obter_se_filtro_fluxo(a.nr_seq_conta_banco, null, null, wheb_usuario_pck.get_nm_usuario, null) =' || chr(39) || 'S' || chr(39) || ') or (ie_origem<>' || chr(39) || 'D' || chr(39) || '))' ||					
					'   and dt_referencia between ' || 
 					'	to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')' ||
 					' and	to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' ||
					ds_nivel_w || ' Group By cd_conta_financ';
		end if;
	else
		-- Edgar 04/12/2007, OS 74953
		ds_sql_where_w	:= ' where ie_classif_fluxo = ' ||
				   ' decode(obter_data_maior(dt_referencia, to_date(' || chr(39) || to_char(dt_referencia_p, 'dd/mm/yyyy') || chr(39) ||  ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ' )) , dt_referencia, ' || chr(39) || 'R' || chr(39) || ',' || 
				   chr(39) || 'P' || chr(39) || ')';
		if	(ie_passado_real_w = 'S') and (ie_periodo_w = 'M') then

			ds_sql_where_w	:= ' where (((ie_classif_fluxo in (' || chr(39) || 'P' || chr(39) || ', ' || chr(39) || 'R' || chr(39) || ')) and ' ||
						'   (dt_referencia = to_date(' || chr(39) || to_char(fim_mes(dt_referencia_p), 'dd/mm/yyyy') || chr(39) ||  ', '  ||  chr(39) || 'dd/mm/yyyy' || chr(39) || ' ))) or ' ||
						'   ((ie_classif_fluxo = ' ||
					   	' decode(obter_data_maior(dt_referencia, to_date(' || chr(39) || to_char(fim_mes(dt_referencia_p), 'dd/mm/yyyy') 
					   	|| chr(39) ||  ', '  ||
					   	chr(39) || 'dd/mm/yyyy' || chr(39) || ' )) , dt_referencia, ' || 
					   	chr(39) || 'R' || chr(39) || ',' || chr(39) || 'P' || chr(39) || '))))';

		elsif	((ie_passado_real_w = 'S') and (ie_periodo_w <> 'M')) or (nvl(ie_classif_p,'-1') = 'RPR') then

			ds_sql_where_w	:= ' where ((ie_classif_fluxo = ' ||
					   ' decode(obter_data_maior(dt_referencia, to_date(' || chr(39) || to_char(dt_referencia_p, 'dd/mm/yyyy') 
					   || chr(39) ||  ', '  ||
					   chr(39) || 'dd/mm/yyyy' || chr(39) || ' )) , dt_referencia, ' || 
					   chr(39) || 'R' || chr(39) || ',' || chr(39) || 'P' || chr(39) || ')) or ' || 
					   ' (dt_referencia = to_date(' || chr(39) || to_char(dt_referencia_p, 'dd/mm/yyyy') || chr(39) ||  ', '  || 
					   chr(39) || 'dd/mm/yyyy' || chr(39) || ' )))';

		end if;

		if	(ie_periodo_p = 'S') then
			ds_comando_w	:= 	'create or replace view fluxo_caixa_v as ' ||
						' select cd_conta_financ, ' || vl_fluxo_w || ', ' || ds_semana_w || ', ' || ds_titulo_semana_w ||
						' from fluxo_caixa a ' ||
						ds_sql_where_w ||
						'   and ie_periodo = ' || chr(39) || ie_periodo_w || chr(39) ||
						'   and cd_estabelecimento = ' || cd_estabelecimento_p ||
						' and ((obter_se_filtro_fluxo(a.nr_seq_conta_banco, null, null, wheb_usuario_pck.get_nm_usuario, null) =' || chr(39) || 'S' || chr(39) || ') or (ie_origem<>' || chr(39) || 'D' || chr(39) || '))' ||										
						'   and dt_referencia between ' || 
						'	to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || 
							chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')' ||
						' and	to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || 
							chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' ||
						ds_nivel_w || ' Group By cd_conta_financ';
		else
			ds_comando_w	:= 	'create or replace view fluxo_caixa_v as ' ||
						' select cd_conta_financ, ' || vl_fluxo_w ||
						' from fluxo_caixa a ' ||
						ds_sql_where_w ||
						'   and ie_periodo = ' || chr(39) || ie_periodo_w || chr(39) ||
						'   and cd_estabelecimento = ' || cd_estabelecimento_p ||
						' and ((obter_se_filtro_fluxo(a.nr_seq_conta_banco, null, null, wheb_usuario_pck.get_nm_usuario, null) =' || chr(39) || 'S' || chr(39) || ') or (ie_origem<>' || chr(39) || 'D' || chr(39) || '))' ||										
						'   and dt_referencia between ' || 
						'	to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || 
							chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')' ||
						' and	to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || 
							chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' ||
						ds_nivel_w || ' Group By cd_conta_financ';
		end if;
	end if;
else
if	(ie_periodo_w	= 'D') then
	dt_fluxo_w	:= trunc(dt_inicio_p, 'dd');

	contador_w	:= 1;

	while dt_fluxo_w <= dt_final_p LOOP
		begin
		vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
					'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
					to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
					'dd/mm/yyyy' || chr(39) || ')' || ',decode(ie_classif_fluxo,' || chr(39) ||
					'P' || chr(39) || ', vl_fluxo, 0),0)) ' || 
					'P' || contador_w;

		virgula_w	:= ',';

		vl_fluxo_w	:=  vl_fluxo_w || virgula_w ||
					'sum(decode(dt_referencia,' || 'to_date(' || chr(39) || 
					to_char(dt_fluxo_w,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 
					'dd/mm/yyyy' || chr(39) || ')' || ',decode(ie_classif_fluxo,' || chr(39) ||
					'R' || chr(39) || ', vl_fluxo, 0),0)) ' || 
					'R' || contador_w;
	
		dt_fluxo_w	:= dt_fluxo_w + 1;
		contador_w	:= contador_w + 1;
		
		end;
	END LOOP;

	select	nvl(max(length(CD_CONTA_APRES)), 9)
	into	qt_nivel_w
	from	conta_financeira;

	if	(qt_nivel_w = 9) then
		if	(ie_nivel_p = 3) then
			ds_nivel_w	:= '';
		else
			ds_nivel_w	:= ' and cd_conta_financ in (select y.cd_conta_financ from conta_financeira y ';
			if	(ie_nivel_p = 2) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,7,3) = ' ||
						chr(39) || '000' || chr(39) || ')';
			else
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,4,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			end if;
		end if;
	elsif	(qt_nivel_w = 15) then
		if	(ie_nivel_p = 5) then
			ds_nivel_w	:= '';
		else
			ds_nivel_w	:= ' and cd_conta_financ in (select y.cd_conta_financ from conta_financeira y ';
			if	(ie_nivel_p = 4) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,13,3) = ' ||
						chr(39) || '000' || chr(39) || ')';
			elsif	(ie_nivel_p = 3) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,10,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			elsif	(ie_nivel_p = 2) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,7,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			else
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,4,2) = ' ||
					chr(39) || '00' || chr(39) || ')';
			end if;
		end if;
	else
		if	(ie_nivel_p = 4) then
			ds_nivel_w	:= '';
		else
			ds_nivel_w	:= ' and cd_conta_financ in (select y.cd_conta_financ from conta_financeira y ';
			if	(ie_nivel_p = 3) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,10,3) = ' ||
						chr(39) || '000' || chr(39) || ')';
			elsif	(ie_nivel_p = 2) then
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,7,2) = ' ||
						chr(39) || '00' || chr(39) || ')';
			else
				ds_nivel_w	:= ds_nivel_w || 'where substr(y.cd_conta_apres,4,2) = ' ||
					chr(39) || '00' || chr(39) || ')';
			end if;
		end if;
	end if;

	if	(ie_periodo_p = 'S') then
		ds_comando_w	:= 	' create or replace view fluxo_caixa_v as ' ||
					' select cd_conta_financ, ' || vl_fluxo_w || ', ' || ds_semana_w || ', ' || ds_titulo_semana_w ||
					' from fluxo_caixa a ' ||
					' where ie_classif_fluxo in (' || chr(39) || 'P' || chr(39) || ',' || chr(39) || 'R' || chr(39) || ') ' ||
					' and ie_periodo = ' || chr(39) || ie_periodo_w || chr(39) ||
					' and cd_estabelecimento = ' || cd_estabelecimento_p ||
					' and ((obter_se_filtro_fluxo(a.nr_seq_conta_banco, null, null, wheb_usuario_pck.get_nm_usuario, null) =' || chr(39) || 'S' || chr(39) || ') or (ie_origem<>' || chr(39) || 'D' || chr(39) || '))' ||									
					' and dt_referencia between ' || 
					'	to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')' ||
					' and	to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' ||
					ds_nivel_w || ' Group By cd_conta_financ';
	else
		ds_comando_w	:= 	' create or replace view fluxo_caixa_v as ' ||
					' select cd_conta_financ, ' || vl_fluxo_w ||
					' from fluxo_caixa a ' ||
					' where ie_classif_fluxo in (' || chr(39) || 'P' || chr(39) || ',' || chr(39) || 'R' || chr(39) || ') ' ||
					' and ie_periodo = ' || chr(39) || ie_periodo_w || chr(39) ||
					' and cd_estabelecimento = ' || cd_estabelecimento_p ||
					' and ((obter_se_filtro_fluxo(a.nr_seq_conta_banco, null, null, wheb_usuario_pck.get_nm_usuario, null) =' || chr(39) || 'S' || chr(39) || ') or (ie_origem<>' || chr(39) || 'D' || chr(39) || '))' ||									
					' and dt_referencia between ' || 
					'	to_date(' || chr(39) || to_char(dt_inicio_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')' ||
					' and	to_date(' || chr(39) || to_char(dt_final_p,'dd/mm/yyyy') || 
						chr(39) || ', ' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') ' ||
					ds_nivel_w || ' Group By cd_conta_financ';
	end if;
	end if;
end if;

commit;

Exec_sql_Dinamico('Tasy', ds_comando_w);

END Gerar_View_Fluxo_Caixa_Relat;
/
