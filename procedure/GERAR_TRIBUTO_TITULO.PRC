create or replace procedure gerar_tributo_titulo(	nr_titulo_p		number,
				nm_usuario_p		varchar2,
				ie_baixa_titulo_p	varchar2,
				nr_bordero_p		number,
				dt_baixa_bordero_p	date,
				nr_seq_escrit_p		number,
				dt_baixa_escrit_p	date,
				dt_baixa_p		date,
				cd_estabelecimento_p	number,
				vl_pago_tit_p		number) is

cd_estabelecimento_w		number(5,0);
vl_tributo_w			number(15,2);
vl_titulo_w			number(15,2);
vl_diferenca_tit_nf_w		number(15,2);
vl_outros_vencimentos_w		number(15,2);
cd_tributo_w			number(10,0);
nr_seq_imposto_w		number(10,0);
dt_vencimento_w			date;
dt_venc_titulo_w		date;
dt_venc_tributo_w		date;
dt_contabil_w			date;
pr_aliquota_w			number(07,4);
cd_beneficiario_w		varchar2(14);
cd_conta_contabil_w		varchar2(20);
ie_tipo_tributacao_w		varchar2(255);
cd_cond_pagto_w			number(10,0);
vl_inss_ww			number(15,2);
ds_erro_w			varchar2(255);
ie_gerar_titulo_w		varchar2(01);
cd_cgc_titulo_w			varchar2(14);
cd_pf_titulo_w			varchar2(10);
qt_venc_w			number(05,0);
ds_venc_w			varchar2(2000);
ie_acumulativo_w		varchar2(1);
vl_trib_acum_w			number(15,2);
nr_seq_trans_reg_w		number(10,0);
nr_seq_trans_baixa_w		number(10,0);
cd_conta_financ_w		number(10,0);
ie_vencimento_w			varchar2(01);
ie_tipo_tributo_w		varchar2(15);
vl_minimo_base_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_base_adic_w			number(15,2);
vl_tributo_a_reter_w		number(15,2);
vl_base_a_reter_w		number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_inss_w			number(15,2);
vl_titulo_original_w		number(15,2);
vl_base_tributo_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_trib_anterior_w		number(15,2);
vl_pago_w			number(15,2);
vl_total_base_w			number(15,2);
ie_irpf_w			varchar2(3);
ie_apuracao_piso_w		varchar2(3);
vl_reducao_w			number(15,2);
vl_base_calculo_paga_w		number(15,2);
vl_desc_dependente_w		number(15,2);
qt_dependente_w			number(15,2);
cd_darf_w			varchar2(20);
dt_tributo_w			date;
cd_variacao_w			varchar2(2);
ie_periodicidade_w		varchar2(1);
ie_baixa_titulo_w		varchar2(10);
cd_cnpj_raiz_w			varchar2(50);
ie_cnpj_w			varchar2(50);
vl_saldo_titulo_w		number(15,2);
vl_mercadoria_w			number(15,2);
vl_base_trib_calculado_w	number(15,2);
nr_seq_nota_fiscal_w		number(10,0);
nr_titulo_original_w		number(10,0);
nr_parcelas_w			number(10,0);
nr_total_parcelas_w		number(10,0);
vl_soma_trib_nao_retido_temp_w	number(15,2);
vl_soma_base_nao_retido_temp_w	number(15,2);
vl_soma_trib_adic_temp_w	number(15,2);
vl_soma_base_adic_temp_w	number(15,2);
vl_trib_anterior_temp_w		number(15,2);
vl_total_base_temp_w		number(15,2);
vl_base_retido_outro_w		number(15,2);
vl_reducao_temp_w		number(15,2);
vl_desdobrado_w			number(15,2);
vl_base_pago_adic_base_w	number(15,2);
vl_adiantamento_w		number(15,2);
vl_pago_outros_w		number(15,2);
vl_base_calc_paga_outros_w	number(15,2);
ie_tipo_titulo_w		varchar2(100);
dt_emissao_w			date;
cd_estab_titulo_w		number(4);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w		date;
cd_tributo_pf_w			varchar2(10);
vl_base_tributo_pf_w		number(15,2);
vl_tributo_pf_w			number(15,2);
cd_pessoa_fisica_pf_w		varchar2(10);
qt_registro_w			number(5);
ie_vencimento_pf_w		varchar2(01);
ds_emp_retencao_w		varchar2(255);
ie_pago_prev_tit_pagar_w	varchar2(255);
ie_tipo_data_w			varchar2(5);
ie_acumular_trib_liq_w		varchar2(1)	:= 'S';
vl_saldo_tit_w			number(15,2);
nr_ccm_w			number(10)	:= null;
cd_tipo_servico_w		varchar2(100);
ds_irrelevante_w		varchar2(4000);
cd_natureza_operacao_w		number(10);
cd_operacao_nf_w		number(4);
ie_restringe_estab_w		varchar2(1);
qt_pago_outros_w		number(15);
ie_trib_atualiza_saldo_w	varchar2(3);
nr_seq_classe_w			number(10);
cd_empresa_w			number(4);
dt_venc_inicio_w		date;
dt_venc_fim_w			date;
nr_seq_regra_w			number(15);
pr_reducao_base_w		number(15,2);
ie_valor_base_titulo_nf_w	varchar2(1);
vl_base_trib_nf_w		number(15,2);
qt_registros_w          number(10);
ie_data_irpf_w			tributo.ie_data_irpf%type;
dt_competencia_w		date;
ie_origem_titulo_w		titulo_pagar.ie_origem_titulo%type;
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;
nr_seq_repasse_w		repasse_terceiro.nr_repasse_terceiro%type;
nr_tipo_repasse_w		tipo_repasse.nr_sequencia%type;
vl_imposto_unifi_w		titulo_pagar_imposto.vl_imposto%type;
vl_base_calculo_unifi_w titulo_pagar_imposto.vl_base_calculo%type;
ie_gerar_trib_unif_w	varchar2(1) := 'N';
ie_permite_trib_tit_adiant_w	varchar2(1);

cursor c01 is
	select	a.cd_tributo,
		a.ie_gerar_titulo_pagar,
		a.ie_vencimento,
		a.ie_tipo_tributo,
		a.ie_apuracao_piso,
		nvl(a.ie_baixa_titulo, 'N'),
		a.ie_cnpj,
		nvl(a.ie_acumular_trib_liq,'S'),
		a.ie_restringe_estab,
		nvl(a.ie_valor_base_titulo_nf, 'N'),
		nvl(a.ie_data_irpf,'A')
	from	tributo a
	where	a.ie_conta_pagar	= 'S'
	and	a.ie_situacao		= 'A'
	and	(nr_ccm_w is null or nvl(a.ie_ccm,'S') = 'S')
	and	(not exists
			(select	1
			from	titulo_pagar_imposto x
			where	x.nr_titulo	= nr_titulo_p
			and	x.cd_tributo	= a.cd_tributo
			and	x.ie_pago_prev	= 'V') or
		nvl(a.ie_baixa_titulo, 'N')	= 'S')
	and	((nvl(a.ie_pf_pj,'A') 	= 'A') or
		 ((a.ie_pf_pj = 'PF') and (cd_pf_titulo_w is not null)) or
		 ((a.ie_pf_pj = 'PJ') and (cd_cgc_titulo_w is not null)))
	and	nvl(a.ie_baixa_titulo, 'N')	= nvl(ie_baixa_titulo_p, 'N')
	and	( (ie_tipo_titulo_w		not in ('4','5','27')) or
	      ((nvl(ie_permite_trib_tit_adiant_w,'N') = 'S') and (ie_tipo_titulo_w		not in ('4','27')))
		)
	and	(nvl(nr_seq_nota_fiscal_w,0) > 0 or nvl(a.ie_somente_nf,'N') = 'N')
	and	((nvl(ie_tipo_tributacao_w, 'X') <> '0') or (nvl(ie_super_simples, 'S') = 'S'))
	and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	order	by decode(a.ie_tipo_tributo, 'INSS',1,2);

cursor c02 is
	select 	b.cd_tributo,
		b.vl_base_calculo,
		b.vl_tributo,
		b.dt_inicio_vigencia,
		b.dt_fim_vigencia,
		b.cd_pessoa_fisica,
		a.ie_vencimento,
		b.ds_emp_retencao,
		b.ie_tipo_data,
		nvl(b.ie_pago_prev_tit_pagar, 'R')
	from	pessoa_fisica_trib	b,
		tributo			a
	where	b.cd_tributo 	= a.cd_tributo
	and	cd_pessoa_fisica 	= cd_pf_titulo_w
	and	((b.cd_estabelecimento = cd_estabelecimento_p) or (b.cd_estabelecimento is null))
	and	decode(b.ie_tipo_data,'E',dt_emissao_w,'V',dt_venc_titulo_w) between b.dt_inicio_vigencia and dt_fim_vigencia
	and	(nr_ccm_w is null or nvl(a.ie_ccm,'S') = 'S');


begin
vl_adiantamento_w		:= 0;

if	(nvl(nr_bordero_p, 0) > 0) and
	(nvl(ie_baixa_titulo_p,'N') = 'S') then
	begin
	select	a.cd_estabelecimento,
		dt_baixa_bordero_p,
		a.cd_cgc,
		a.cd_pessoa_fisica,
		a.vl_bordero,
		obter_cnpj_raiz(a.cd_cgc),
		b.nr_repasse_terceiro,
		b.ie_origem_titulo
	into	cd_estab_titulo_w,
		dt_venc_titulo_w,
		cd_cgc_titulo_w,
		cd_pf_titulo_w,
		vl_base_tributo_w,
		cd_cnpj_raiz_w,
		nr_seq_repasse_w,
		ie_origem_titulo_w
	from	titulo_pagar_bordero_v a,
		titulo_pagar b
	where	a.nr_titulo		= nr_titulo_p
	and	a.nr_bordero	= nr_bordero_p
	and	a.nr_titulo 	= b.nr_titulo;

	select  max(nr_seq_tipo)
	into	nr_tipo_repasse_w
	from	repasse_terceiro
	where	nr_repasse_terceiro = nr_seq_repasse_w;

	exception
	when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(175622,'DS_ERRO=' || sqlerrm || ';' ||'NR_TIT=' || nr_titulo_p || ';' || 'NR_BORD=' || nr_bordero_p);
	end;

/* francisco - os80633 - 25/02/2008 */
elsif	(nvl(nr_seq_escrit_p,0) > 0) and
	(nvl(ie_baixa_titulo_p,'N') = 'S') then
	select	b.cd_estabelecimento,
		b.dt_remessa_retorno,
		c.cd_cgc,
		c.cd_pessoa_fisica,
		/* se o pagamento for de retorno, os tributos ja deduziram o saldo na remessa */
		decode(b.ie_remessa_retorno,'I',to_number(obter_dados_tit_pagar(a.nr_titulo,'V')),nvl(a.vl_escritural,0)),
		obter_cnpj_raiz(c.cd_cgc),
		c.nr_seq_classe,
		c.ie_origem_titulo
	into	cd_estab_titulo_w,
		dt_venc_titulo_w,
		cd_cgc_titulo_w,
		cd_pf_titulo_w,
		vl_base_tributo_w,
		cd_cnpj_raiz_w,
		nr_seq_classe_w,
		ie_origem_titulo_w
	from	titulo_pagar c,
		banco_escritural b,
		titulo_pagar_escrit a
	where	a.nr_seq_escrit	= b.nr_sequencia
	and	a.nr_titulo		= c.nr_titulo
	and	a.nr_titulo		= nr_titulo_p
	and	a.nr_seq_escrit	= nr_seq_escrit_p;
else
    select count(*)
	into qt_registros_w
	from titulo_pagar_escrit
	where nr_titulo = nr_titulo_p;

	if (qt_registros_w > 0) then

	    wheb_mensagem_pck.exibir_mensagem_abort(352356);

	end if;

	select	cd_estabelecimento,
		nvl(dt_baixa_p,dt_vencimento_atual),
		cd_cgc,
		cd_pessoa_fisica,
		nvl(vl_pago_tit_p,0),
		obter_cnpj_raiz(cd_cgc),
		dt_emissao,
		dt_contabil,
		vl_saldo_titulo,
		nr_seq_classe,
		ie_origem_titulo
	into	cd_estab_titulo_w,
		dt_venc_titulo_w,
		cd_cgc_titulo_w,
		cd_pf_titulo_w,
		vl_base_tributo_w,
		cd_cnpj_raiz_w,
		dt_emissao_w,
		dt_contabil_w,
		vl_saldo_tit_w,
		nr_seq_classe_w,
		ie_origem_titulo_w
	from	titulo_pagar
	where	nr_titulo	= nr_titulo_p;

	/* edgar os 92908 */
	select	nvl(sum(vl_adiantamento),0)
	into	vl_adiantamento_w
	from	titulo_pagar_adiant
	where	nr_titulo		= nr_titulo_p;

	vl_base_tributo_w		:= vl_base_tributo_w + vl_adiantamento_w;

	if	(vl_base_tributo_w = 0) then
		vl_base_tributo_w	:= vl_saldo_tit_w;
	end if;
end if;

if	(cd_pf_titulo_w is not null) then
	select	max(nr_ccm)
	into	nr_ccm_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pf_titulo_w;
end if;

select	max(ie_tipo_tributacao)
into	ie_tipo_tributacao_w
from	pessoa_juridica
where	cd_cgc	= cd_cgc_titulo_w;


open c02;
loop
fetch c02 into
	cd_tributo_pf_w,
	vl_base_tributo_pf_w,
	vl_tributo_pf_w,
	dt_inicio_vigencia_w,
	dt_fim_vigencia_w,
	cd_pessoa_fisica_pf_w,
	ie_vencimento_pf_w,
	ds_emp_retencao_w,
	ie_tipo_data_w,
	ie_pago_prev_tit_pagar_w;
exit when c02%notfound;
	begin

	select	count(*)
	into	qt_registro_w
	from	titulo_pagar_imposto	a,
		titulo_pagar		b
	where	a.nr_titulo			= b.nr_titulo
	and	a.cd_tributo			= cd_tributo_pf_w
	and	b.cd_pessoa_fisica		= cd_pessoa_fisica_pf_w
	and 	a.ie_pago_prev <> 'V'
	and nvl(a.ds_emp_retencao,'X') = nvl(ds_emp_retencao_w,'X')  
	and	PKG_DATE_UTILS.start_of(a.dt_imposto,'month',0)	= PKG_DATE_UTILS.start_of(decode(ie_tipo_data_w,'E',dt_emissao_w,'V',dt_venc_titulo_w),'month',0);

	if	(qt_registro_w = 0) then
		select	titulo_pagar_imposto_seq.nextval
		into	nr_seq_imposto_w
		from	dual;


		insert into titulo_pagar_imposto(
			nr_sequencia,
			nr_titulo,
			cd_tributo,
			ie_pago_prev,
			dt_atualizacao,
			nm_usuario,
			dt_imposto,
			vl_base_calculo,
			vl_imposto,
			vl_nao_retido,
			ie_vencimento,
			vl_base_nao_retido,
			vl_trib_adic,
			vl_base_adic,
			ds_emp_retencao)
		values(	nr_seq_imposto_w,
			nr_titulo_p,
			cd_tributo_pf_w,
			ie_pago_prev_tit_pagar_w,
			sysdate,
			nm_usuario_p,
			PKG_DATE_UTILS.start_of(decode(ie_tipo_data_w,'E',dt_emissao_w,'V',dt_venc_titulo_w),'dd',0),
			vl_base_tributo_pf_w,
			trunc(vl_tributo_pf_w, 2),
			0,
			ie_vencimento_pf_w,
			0,
			0,
			0,
			ds_emp_retencao_w);


	end if;
	end;
end loop;
close c02;

cd_estabelecimento_w	:= nvl(cd_estabelecimento_p,cd_estab_titulo_w);

select	a.vl_saldo_titulo,
	a.nr_seq_nota_fiscal,
	a.vl_titulo,
	a.ie_tipo_titulo,
	b.cd_tipo_servico,
	b.cd_operacao_nf,
	a.nr_seq_classe
into	vl_saldo_titulo_w,
	nr_seq_nota_fiscal_w,
	vl_titulo_w,
	ie_tipo_titulo_w,
	cd_tipo_servico_w,
	cd_operacao_nf_w,
	nr_seq_classe_w
from	nota_fiscal b,
	titulo_pagar a
where	a.nr_seq_nota_fiscal	= b.nr_sequencia(+)
and	a.nr_titulo		= nr_titulo_p;

select	a.cd_empresa
into	cd_empresa_w
from	estabelecimento a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

vl_trib_acum_w	:= 0;
vl_titulo_original_w	:= vl_base_tributo_w;
dt_tributo_w	:= dt_venc_titulo_w;

delete	from w_titulo_pagar_imposto
where	nr_titulo	= nr_titulo_p;

obter_param_usuario(851,260,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_permite_trib_tit_adiant_w);

open c01;
loop
fetch c01 into
	cd_tributo_w,
	ie_gerar_titulo_w,
	ie_vencimento_w,
	ie_tipo_tributo_w,
	ie_apuracao_piso_w,
	ie_baixa_titulo_w,
	ie_cnpj_w,
	ie_acumular_trib_liq_w,
	ie_restringe_estab_w,
	ie_valor_base_titulo_nf_w,
	ie_data_irpf_w;
exit when c01%notfound;

	pr_aliquota_w			:= 0;
	select	max(a.nr_seq_nota_fiscal)
	into	nr_seq_nota_fiscal_w
	from	titulo_pagar a
	where	a.nr_titulo		= nr_titulo_p;

	if	(nr_seq_nota_fiscal_w is not null) then

		select	max(cd_natureza_operacao)
		into	cd_natureza_operacao_w
		from	nota_fiscal
		where	nr_sequencia	= nr_seq_nota_fiscal_w;

	end if;

    select	count(*)
	into	qt_pago_outros_w
	from	titulo_pagar_imposto
	where	nr_titulo		= nr_titulo_p
	and	cd_tributo	= cd_tributo_w
	and	ie_pago_prev	= 'P';



	obter_dados_trib_tit_pagar(
		cd_tributo_w,
		cd_estabelecimento_w,
		cd_cgc_titulo_w,
		cd_pf_titulo_w,
		cd_beneficiario_w,
		pr_aliquota_w,
		cd_cond_pagto_w,
		cd_conta_financ_w,
		nr_seq_trans_reg_w,
		nr_seq_trans_baixa_w,
		vl_minimo_base_w,
		vl_minimo_tributo_w,
		ie_acumulativo_w,
		vl_teto_base_w,
		vl_desc_dependente_w,
		cd_darf_w,
		dt_tributo_w,
		cd_variacao_w,
		ie_periodicidade_w,
		'M',
		cd_natureza_operacao_w,
		cd_tipo_servico_w,
		null,
		null,
		nr_tipo_repasse_w,
		nr_seq_regra_w,
		cd_operacao_nf_w,
		qt_pago_outros_w,
		ds_irrelevante_w,
		ds_irrelevante_w,
		vl_base_tributo_w,
		'N',
		nr_seq_classe_w,
		ie_origem_titulo_w,
		null,
    null);

	vl_base_tributo_w	:= vl_titulo_original_w;

	ie_irpf_w		:= 'N';

	if	(nr_seq_regra_w is not null ) then /*AAMFIRMO OS 560487 18/04/2013 */

		select	nvl(max(PR_REDUCAO_BASE),0)
		into	PR_REDUCAO_BASE_W
		from 	tributo_conta_pagar
		where	nr_sequencia	= nr_seq_regra_w;

		if	(nvl(PR_REDUCAO_BASE_W,0) > 0) then
			vl_base_tributo_w	:= (vl_base_tributo_w - ((PR_REDUCAO_BASE_W/100) * vl_base_tributo_w ));
		end if;
	end if;

	if	(nvl(pr_aliquota_w,0) > 0) then
		-- calcular reducao base irpf e saldo menos inss
		if	(ie_tipo_tributo_w = 'IR') and
			(cd_pf_titulo_w is not null) then
			ie_irpf_w	:= 'S';

			select	nvl(qt_dependente,0)
			into	qt_dependente_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pf_titulo_w;

			if (nvl(nr_seq_nota_fiscal_w,0) = 0) then
				select	nvl(max(ie_trib_atualiza_saldo),'S')
				into	ie_trib_atualiza_saldo_w
				from	parametros_contas_pagar a
				where	a.cd_estabelecimento = cd_estabelecimento_p;

				select	nvl(sum(a.vl_imposto),0)
				into	vl_inss_w
				from	tributo b,
					titulo_pagar_imposto a
				where	a.cd_tributo		= b.cd_tributo
				and	b.ie_tipo_tributo	= 'INSS'
				and	a.nr_titulo		= nr_titulo_p
				and	a.ie_pago_prev		= 'V'
				and	(b.ie_baixa_titulo	= 'N' or
					(nvl(b.ie_saldo_tit_pagar,'S') = 'N' or ie_trib_atualiza_saldo_w = 'N'));

				select	nvl(sum(a.vl_imposto),0)
				into	vl_inss_ww
				from	tributo b,
					w_titulo_pagar_imposto a
				where	a.cd_tributo		= b.cd_tributo
				and	b.ie_tipo_tributo	= 'INSS'
				and	a.nr_titulo		= nr_titulo_p
				and	ie_baixa_titulo_w	<> 'N';

				vl_inss_w	:= vl_inss_w + vl_inss_ww;

				/* A base de calculo do IRPF e o valor do titulo deduzido do INSS */
				vl_base_tributo_w	:= to_number(obter_dados_tit_pagar(nr_titulo_p,'V')) - vl_inss_w;
			end if;
		elsif (ie_tipo_tributo_w = 'ISRDOM' and cd_pf_titulo_w is not null) then 
			select	nvl(qt_dependente,0)
			into	qt_dependente_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pf_titulo_w;
			
			ie_irpf_w	:= 'S';
			if (nvl(nr_seq_nota_fiscal_w,0) = 0) then
				vl_base_tributo_w	:= to_number(obter_dados_tit_pagar(nr_titulo_p,'V'));
			end if;
		else
			vl_base_tributo_w	:= vl_titulo_original_w;
			if ( nvl(PR_REDUCAO_BASE_W,0) > 0 ) then /*AAMFIRMO OS 560487 18/04/2013 */
				vl_base_tributo_w := ( vl_base_tributo_w - ( (PR_REDUCAO_BASE_W/100) * vl_base_tributo_w ) );
			end if;
		end if;

		/* francisco - os 86089 - 13/03/2008 */
		if	(ie_vencimento_w = 'R') and
			(dt_emissao_w is not null) then
			dt_vencimento_w		:= dt_emissao_w;
			dt_venc_tributo_w	:= dt_emissao_w;
		elsif	(ie_vencimento_w = 'C') then
			dt_vencimento_w		:= nvl(nvl(dt_contabil_w,dt_emissao_w),sysdate);
			dt_venc_tributo_w	:= nvl(nvl(dt_contabil_w,dt_emissao_w),sysdate);
		else
			dt_vencimento_w		:= nvl(dt_venc_titulo_w,sysdate);
			dt_venc_tributo_w	:= nvl(dt_venc_titulo_w,sysdate);
		end if;
		/* fim francisco - os 86089 - 13/03/2008 */

		if	(cd_cond_pagto_w is not null) then
			calcular_vencimento(cd_estabelecimento_w, cd_cond_pagto_w,dt_vencimento_w, qt_venc_w, ds_venc_w);

			if	(qt_venc_w = 1) then
				dt_vencimento_w	:= to_date(substr(ds_venc_w,1,10),'dd/mm/yyyy');
			end if;
		end if;

		dt_venc_inicio_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_venc_tributo_w);
		dt_venc_fim_w	:= ESTABLISHMENT_TIMEZONE_UTILS.endOfMonth(dt_venc_tributo_w);

		select	nvl(sum(vl_soma_trib_nao_retido),0),
			nvl(sum(vl_soma_base_nao_retido),0),
			nvl(sum(vl_soma_trib_adic),0),
			nvl(sum(vl_soma_base_adic),0),
			nvl(sum(vl_tributo),0),
			nvl(sum(vl_total_base),0),
			nvl(sum(vl_reducao),0)
		into	vl_soma_trib_nao_retido_w,
			vl_soma_base_nao_retido_w,
			vl_soma_trib_adic_w,
			vl_soma_base_adic_w,
			vl_trib_anterior_w,
			vl_total_base_w,
			vl_reducao_w
		from	valores_tributo_v
		where	nvl(cd_empresa, cd_empresa_w)	= cd_empresa_w
		and	cd_tributo			= cd_tributo_w
		and	(
			  (
			    (cd_cgc		= cd_cgc_titulo_w) or
			    ((cd_cnpj_raiz		= cd_cnpj_raiz_w) and (ie_cnpj_w = WHEB_MENSAGEM_PCK.get_texto(803917)))
			  )
			  or
			  (cd_pessoa_fisica		= cd_pf_titulo_w)
			)
		and	dt_tributo between dt_venc_inicio_w and dt_venc_fim_w
		and	(
			  (ie_origem_valores 	= 'TP') or
			  (ie_apuracao_piso_w 	= 'S') or
			  (
			    (ie_baixa_titulo_w	= 'S') and
			    (ie_baixa_titulo = 'S')
			  )
			)
		and	((ie_restringe_estab_w	= 'N') or
			 (cd_estabelecimento	= cd_estabelecimento_w) or
			 (cd_estab_financeiro	= cd_estabelecimento_w))
		and	ie_baixa_titulo		= ie_baixa_titulo_w
		and	(
			  (
			    (ie_apuracao_piso_w = 'N') or
			    (ie_apuracao_piso_w = ie_base_calculo)
			  ) or
			  (
			    (ie_baixa_titulo_w	= 'S') and
			    (ie_baixa_titulo = 'S')
			  )
			);

		if	(ie_baixa_titulo_w = 'S') then
			-- edgar 03/03/2008, os 80633, qdo a baixa for total devera adicionar o valor da baixa calculada devera adicionar o valor ref e nf
			if	(vl_saldo_titulo_w + vl_adiantamento_w = vl_base_tributo_w) then
				-- edgar 31/03/2008, os 87455, tratar titulo desdobrado
				select	nvl(nr_titulo_original, 0),
					nvl(nr_parcelas,1),
					nvl(nr_total_parcelas,1)
				into	nr_titulo_original_w,
					nr_parcelas_w,
					nr_total_parcelas_w
				from	titulo_pagar
				where	nr_titulo	= nr_titulo_p;

				vl_desdobrado_w		:= 0;
				if	(nr_titulo_original_w > 0) and
					(ie_acumular_trib_liq_w = 'N') then	-- edgar 06/05/2009, os 138996, so somar o valor desdobrado caso esteja parametrizado para nao acumular somente na ultima baixa
					select	nvl(sum(vl_titulo),0)
					into	vl_desdobrado_w
					from	titulo_pagar
					where	nr_titulo_original	= nr_titulo_original_w
					and	nr_titulo		<> nr_titulo_p
					and	ie_tipo_titulo		<> '4';	-- imposto
				end if;

				if	(nvl(nr_seq_nota_fiscal_w,0) > 0) and
					(nr_parcelas_w = nr_total_parcelas_w) and
					(nr_total_parcelas_w > 1) then /*francisco - 10/06/2009 - so fazer esse trat quando mais de uma parcela */
					/* edgar 31/03/2008, os 87455, so gerar trib no ultimo desdobramento */
					select	nvl(sum(vl_titulo),0)
					into	vl_outros_vencimentos_w
					from	titulo_pagar
					where	nr_seq_nota_fiscal	= nr_seq_nota_fiscal_w
					and	nr_titulo		<> nr_titulo_p
					and	ie_tipo_titulo		<> '4'
					and	ie_situacao		not in ('D','C','T');

					select	a.vl_mercadoria
					into	vl_mercadoria_w
					from	nota_fiscal a
					where	a.nr_sequencia		= nr_seq_nota_fiscal_w;

					if	(ie_acumular_trib_liq_w = 'N') then
						select	nvl(sum(vl_base_calculo),0)
						into	vl_base_trib_calculado_w
						from	titulo_pagar_imposto
						where	nr_titulo	= nr_titulo_p
						and	cd_tributo	= cd_tributo_w;

						vl_base_tributo_w	:= vl_mercadoria_w - vl_titulo_w + vl_base_tributo_w - vl_desdobrado_w - vl_outros_vencimentos_w +
									   (vl_titulo_w - vl_saldo_titulo_w) - vl_base_trib_calculado_w;
					else
						vl_base_tributo_w	:= vl_mercadoria_w - vl_titulo_w + vl_base_tributo_w - vl_desdobrado_w - vl_outros_vencimentos_w +
									   (vl_titulo_w - vl_saldo_titulo_w); -- edgar 23/01/2009, os 124531, incluido este calculo para adicionar o que ja foi baixado anteriormente
					end if;
				else
					select	nvl(sum(vl_base_calculo),0)
					into	vl_base_trib_calculado_w
					from	titulo_pagar_imposto
					where	nr_titulo	= nr_titulo_p
					and	cd_tributo	= cd_tributo_w;

					vl_diferenca_tit_nf_w		:= 0;

					if	(nvl(nr_seq_nota_fiscal_w,0) > 0) and
						(nr_total_parcelas_w = 1) then
						select	a.vl_mercadoria - vl_titulo_w
						into	vl_diferenca_tit_nf_w
						from	nota_fiscal a
						where	a.nr_sequencia		= nr_seq_nota_fiscal_w;
					end if;

					-- edgar / diether 30/03/2012, os 386764, obter novamente o valor do inss, pois a base do tributo esta sendo recalculada
					select	nvl(sum(a.vl_imposto),0)
					into	vl_inss_w
					from	tributo b,
						titulo_pagar_imposto a
					where	a.cd_tributo		= b.cd_tributo
					and	b.ie_tipo_tributo	= 'INSS'
					and	a.nr_titulo		= nr_titulo_p
					and	a.ie_pago_prev		= 'V'
					and	(ie_tipo_tributo_w = 'IR') and (cd_pf_titulo_w is not null);

					/* edgar 14/05/2008, os troquei o vl_titulo_original_w pelo vl_titulo_w */
					vl_base_tributo_w		:= vl_diferenca_tit_nf_w + to_number(obter_dados_tit_pagar(nr_titulo_p, 'V')) -
									(vl_saldo_titulo_w + vl_adiantamento_w) +
									   vl_base_tributo_w - 	vl_base_trib_calculado_w - vl_inss_w;
				end if;
			end if;

			/*lhalves OS594282 em 21/06/2013 - Se parametrizado busca o valo base calculo da nota fiscal de origem */
			if	(nvl(ie_valor_base_titulo_nf_w,'N') = 'S') and
				(nvl(nr_seq_nota_fiscal_w,0) > 0) then

				select	max(vl_base_calculo)
				into	vl_base_trib_nf_w
				from	nota_fiscal_trib
				where	nr_sequencia	= nr_seq_nota_fiscal_w;

				if	(nvl(vl_base_trib_nf_w,0) <> 0) then
					vl_base_tributo_w	:= vl_base_trib_nf_w;
				end if;
			elsif	(nvl(ie_valor_base_titulo_nf_w,'N') = 'M') and
				(nvl(nr_seq_nota_fiscal_w,0) > 0) then
				begin
				select	max(vl_mercadoria)
				into	vl_mercadoria_w
				from	nota_fiscal
				where	nr_sequencia = nr_seq_nota_fiscal_w;
				if	(nvl(vl_mercadoria_w,0) <> 0) then
					vl_base_tributo_w	:= vl_mercadoria_w;
				end if;
				end;
			elsif 	(nvl(ie_valor_base_titulo_nf_w,'N') = 'V') and
				(nvl(nr_seq_nota_fiscal_w,0) > 0) then
				select sum(VL_BASE_VENC)
				into vl_mercadoria_w
				from nota_fiscal_venc
				where nr_sequencia = nr_seq_nota_fiscal_w
				and NR_TITULO_PAGAR = nr_titulo_p;
				if 	(nvl(vl_mercadoria_w,0) <> 0) then
					vl_base_tributo_w := vl_mercadoria_w;
				end if;
			end if;

			select	nvl(sum(a.vl_nao_retido),0) vl_soma_trib_nao_retido,
				nvl(sum(a.vl_base_nao_retido),0) vl_soma_base_nao_retido,
				nvl(sum(a.vl_trib_adic),0) vl_soma_trib_adic,
				nvl(sum(a.vl_base_adic),0) vl_soma_base_adic,
				nvl(sum(vl_imposto),0),
				nvl(sum(a.vl_base_calculo),0),
				nvl(sum(nvl(a.vl_reducao,0)),0)
			into	vl_soma_trib_nao_retido_temp_w,
				vl_soma_base_nao_retido_temp_w,
				vl_soma_trib_adic_temp_w,
				vl_soma_base_adic_temp_w,
				vl_trib_anterior_temp_w,
				vl_total_base_temp_w,
				vl_reducao_temp_w
			from	estabelecimento d,
				tributo c,
				titulo_pagar b,
				w_titulo_pagar_imposto a
			where	b.ie_situacao			<> 'C'
			and	nvl(cd_empresa,nvl(cd_empresa_w,0)) = nvl(cd_empresa_w,0)
			and	b.cd_estabelecimento	= d.cd_estabelecimento
			and	a.nr_titulo			= b.nr_titulo
			and	a.cd_tributo		= c.cd_tributo
			and	c.cd_tributo		= cd_tributo_w
			and	(
				  (
				    (b.cd_cgc			= cd_cgc_titulo_w) or
				    ((substr(obter_cnpj_raiz(b.cd_cgc),1,20)		= cd_cnpj_raiz_w) and (ie_cnpj_w = WHEB_MENSAGEM_PCK.get_texto(803917)))
				  )
				  or
				  (b.cd_pessoa_fisica		= cd_pf_titulo_w)
				)
			and	(a.nr_seq_escrit = nr_seq_escrit_p or
				PKG_DATE_UTILS.start_of(decode(ie_vencimento_w,	'V', b.dt_vencimento_atual,
								'C', nvl(b.dt_contabil, b.dt_emissao),
								'B', dt_venc_titulo_w,
								'S', decode(nr_seq_escrit_p,null,decode(nr_bordero_p,null,b.dt_vencimento_atual,dt_venc_titulo_w),dt_venc_titulo_w),
								b.dt_emissao
				), 'month', 0) 	= PKG_DATE_UTILS.start_of(dt_venc_tributo_w, 'month', 0))
			and	(ie_restringe_estab_w		= 'N' or
				b.cd_estabelecimento		= cd_estabelecimento_w or
				b.cd_estab_financeiro		= cd_estabelecimento_w)
			and	c.ie_baixa_titulo		= 'S'
			and	a.nr_seq_baixa	 is null
			and	(nr_seq_escrit_p is null or a.nr_seq_escrit is not null)
			and	(nr_bordero_p is null or a.nr_bordero = nr_bordero_p)
			and	(a.nr_titulo <> nr_titulo_p or vl_pago_tit_p <> 0);

			vl_soma_trib_nao_retido_w	:= vl_soma_trib_nao_retido_w + vl_soma_trib_nao_retido_temp_w;
			vl_soma_base_nao_retido_w	:= vl_soma_base_nao_retido_w + vl_soma_base_nao_retido_temp_w;
			vl_soma_trib_adic_w	:= vl_soma_trib_adic_w + vl_soma_trib_adic_temp_w;
			vl_soma_base_adic_w	:= vl_soma_base_adic_w + vl_soma_base_adic_temp_w;
			vl_trib_anterior_w		:= vl_trib_anterior_w + vl_trib_anterior_temp_w;
			vl_total_base_w		:= vl_total_base_w + vl_total_base_temp_w;
			vl_reducao_w		:= vl_reducao_w + vl_reducao_temp_w;
		end if;

		select	nvl(sum(b.vl_imposto),0),
			nvl(sum(b.vl_base_calculo),0)
		into	vl_pago_outros_w,
			vl_base_calc_paga_outros_w
		from	titulo_pagar_imposto b,
			titulo_pagar a
		where	a.nr_titulo			= b.nr_titulo
		and	a.nr_titulo			<> nr_titulo_p
		and	((a.cd_cgc		= cd_cgc_titulo_w) or
			 (a.cd_pessoa_fisica 	= cd_pf_titulo_w))
		and	b.cd_tributo		= cd_tributo_w
		and	PKG_DATE_UTILS.start_of(b.dt_imposto, 'month', 0) = PKG_DATE_UTILS.start_of(dt_venc_tributo_w, 'month', 0)
		and	b.ie_pago_prev		= 'P'
		and	a.ie_situacao		in ('A','L');

		select	nvl(sum(vl_imposto),0) + vl_pago_outros_w,
			nvl(sum(vl_base_calculo),0) + vl_base_calc_paga_outros_w
		into	vl_pago_w,
			vl_base_calculo_paga_w
		from	titulo_pagar_imposto
		where	nr_titulo	= nr_titulo_p
		and	cd_tributo	= cd_tributo_w
		and	ie_pago_prev	= 'P';

		select	nvl(sum(vl_base_calculo),0)
		into	vl_base_pago_adic_base_w
		from	titulo_pagar_imposto
		where	nr_titulo	= nr_titulo_p
		and	cd_tributo	= cd_tributo_w
		and	ie_pago_prev	= 'S';


		SELECT	NVL(SUM(vl_base_calculo),0)
		into	vl_base_retido_outro_w
		FROM	titulo_pagar_imposto a,
			titulo_pagar b
		WHERE	a.nr_titulo			= b.nr_titulo
		AND	ie_pago_prev	= 'R'
		AND	a.cd_tributo		= cd_tributo_w
		AND ((b.nr_titulo		= nr_titulo_p  AND NVL(vl_base_calculo,0) <> 0)
			OR (a.nr_titulo			<> nr_titulo_p
			AND	((nvl(b.cd_cgc,0)		= nvl(cd_cgc_titulo_w,0)) and
			 	nvl(b.cd_pessoa_fisica,0) 	= nvl(cd_pf_titulo_w,0))
			AND	PKG_DATE_UTILS.start_of(a.dt_imposto,'month',0)	= PKG_DATE_UTILS.start_of(DECODE(ie_tipo_data_w,'E',dt_emissao_w,'V',dt_venc_titulo_w),'month',0)));


		if	(nvl(ie_data_irpf_w,'A')	= 'B') then

			dt_competencia_w	:= nvl(dt_baixa_p,sysdate);

		else

			dt_competencia_w	:= sysdate;

		end if;

		/*os 1740271 - INICIO */
		if	(ie_tipo_tributo_w = 'INSS') and
			(vl_base_retido_outro_w > 0) and
			(vl_total_base_w > vl_base_retido_outro_w )then

			select	nvl(sum(b.vl_titulo),0) vl_total_base
			into	vl_total_base_w
			from	pessoa_juridica e,
				tributo c,
				titulo_pagar b,
				TITULO_PAGAR_IMPOSTO a
			where	a.nr_titulo		= b.nr_titulo
			and	b.ie_situacao	in 	('A','L')
			and	a.ie_pago_prev	= 'V'
			and	a.cd_tributo		= c.cd_tributo
			and	nvl(c.ie_baixa_titulo, 'N')	= 'N'
			and	b.cd_cgc		= e.cd_cgc(+)
			--
			and	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento) = cd_empresa_w
			and	a.cd_tributo			= cd_tributo_w
			and	(
					(
						(b.cd_cgc		= cd_cgc_titulo_w) or
						((e.cd_cnpj_raiz		= cd_cnpj_raiz_w) and (ie_cnpj_w = WHEB_MENSAGEM_PCK.get_texto(803917)))
					)
					or
					(b.cd_pessoa_fisica		= cd_pf_titulo_w)
				)
			and	decode(c.ie_vencimento, 'V', b.dt_vencimento_atual, 'C', nvl(b.dt_contabil, b.dt_emissao), b.dt_emissao) between dt_venc_inicio_w and dt_venc_fim_w
			and	((ie_restringe_estab_w	= 'N') or
			(b.cd_estabelecimento	= cd_estabelecimento_w) or
			(b.cd_estab_financeiro	= cd_estabelecimento_w))
			and	nvl(c.ie_baixa_titulo, 'N')	= ie_baixa_titulo_w
			and	(
					(
						(ie_apuracao_piso_w = 'N') or
						(ie_apuracao_piso_w = substr(OBTER_SE_BASE_CALCULO(a.nr_titulo, 'TP'),1,1))
					)
					or
					(
						(ie_baixa_titulo_w	= 'S') and
						(nvl(c.ie_baixa_titulo, 'N') = 'S')
					)
				);

		end if;
		/*os 1740271 - INICIO */

		obter_valores_tributo(	ie_acumulativo_w,
					pr_aliquota_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					vl_soma_trib_nao_retido_w,
					vl_soma_trib_adic_w,
					vl_soma_base_nao_retido_w,
					vl_soma_base_adic_w,
					vl_base_tributo_w,
					vl_tributo_w,
					vl_trib_nao_retido_w,
					vl_trib_adic_w,
					vl_base_nao_retido_w,
					vl_base_adic_w,
					vl_teto_base_w,
					vl_trib_anterior_w,
					ie_irpf_w,
					vl_total_base_w,
					vl_reducao_w,
					vl_desc_dependente_w,
					qt_dependente_w,
					vl_base_calculo_paga_w,
					vl_base_pago_adic_base_w,
					vl_base_retido_outro_w,
					obter_outras_reducoes_irpf(cd_pf_titulo_w, cd_estabelecimento_w, dt_venc_tributo_w),
					dt_competencia_w,
					nr_seq_regra_irpf_w,
					cd_tributo_w);

		if	(vl_pago_w < vl_tributo_w) then
			vl_tributo_w	:= vl_tributo_w - vl_pago_w;
		elsif	(vl_pago_w > vl_tributo_w) then
			goto proximo;
		end if;

		-- edgar 02/02/2010, os 192586, nao gerar tributo nao retido negativo
		if	(vl_trib_nao_retido_w < 0) then
			vl_trib_nao_retido_w	:= 0;
		end if;

		if (vl_tributo_w > vl_base_tributo_w) then
			vl_tributo_w := vl_base_tributo_w;
		end if;

		if	(ie_baixa_titulo_w = 'N') then

			if (nvl(obter_se_tit_pagar_unificado(nr_titulo_p),'N') = 'S') and (nvl(philips_param_pck.get_cd_pais,1) = 2) then --Mexico

				select nvl(sum(a.vl_base_calculo),0),
					nvl(sum(a.vl_imposto),0)
				into vl_base_calculo_unifi_w,
					vl_imposto_unifi_w
				from titulo_pagar_imposto a,
					titulo_pagar b
				where a.nr_titulo = b.nr_titulo
				and b.nr_titulo_dest = nr_titulo_p
				and a.cd_tributo = cd_tributo_w;

				if (vl_base_calculo_unifi_w > 0) and (vl_imposto_unifi_w > 0) then
					vl_base_tributo_w := vl_base_calculo_unifi_w;
					vl_tributo_w := vl_imposto_unifi_w;

					ie_gerar_trib_unif_w := 'S';
				else
					ie_gerar_trib_unif_w := 'N';
				end if;

			end if;

			/*AAMFIRMO OS 1093998 - Somente gerar imposto no novo titulo unificado, caso os titulos que foram uniciados possuirem imposto gerado. Antes estava sendo unificado titulos sem imposto, e o novo titulo gerado da unificacao acabava gerando o imposto indevidamente.*/
			    /*Se for mexico, se for um titulo do processo de unificacao e se tiver valor de tributo nos titulos unificados, deve entrar e gerar o imposto*/
			if ((nvl(philips_param_pck.get_cd_pais,1) = 2) and (nvl(obter_se_tit_pagar_unificado(nr_titulo_p),'N') = 'S') and (nvl(ie_gerar_trib_unif_w,'N') = 'S')) or
				/*se for mexico, se nao for processo de unificacao, deve gerar o imposto conforme ocorria antes dessa tratativa. */
			   ((nvl(philips_param_pck.get_cd_pais,1) = 2) and (nvl(obter_se_tit_pagar_unificado(nr_titulo_p),'N') = 'N')) or
			   /*se nao for mexico, gera normal tambem conforme ocorria antes dessa tratativa.*/
			   (nvl(philips_param_pck.get_cd_pais,1) <> 2) then

				select	titulo_pagar_imposto_seq.nextval
				into	nr_seq_imposto_w
				from	dual;

				insert into titulo_pagar_imposto(
					nr_sequencia,
					nr_titulo,
					cd_tributo,
					ie_pago_prev,
					dt_atualizacao,
					nm_usuario,
					dt_imposto,
					vl_base_calculo,
					vl_imposto,
					ds_emp_retencao,
					pr_imposto,
					cd_beneficiario,
					cd_conta_financ,
					nr_seq_trans_reg,
					nr_seq_trans_baixa,
					vl_nao_retido,
					ie_vencimento,
					vl_base_nao_retido,
					vl_trib_adic,
					vl_base_adic,
					vl_reducao,
					vl_desc_base,
					cd_darf,/* rafael. 15/01/07. os48099 */
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_variacao,
					ie_periodicidade,
					cd_cond_pagto,/* Jean . 12/02/19  os 1829349*/
					nr_seq_trib_cp)
				values(	nr_seq_imposto_w,
					nr_titulo_p,
					cd_tributo_w,
					'V',
					sysdate,
					nm_usuario_p,
					obter_proximo_dia_util(cd_estabelecimento_w, dt_vencimento_w),
					vl_base_tributo_w,
					trunc(vl_tributo_w, 2),
					null,
					pr_aliquota_w,
					cd_beneficiario_w,
					cd_conta_financ_w,
					nr_seq_trans_reg_w,
					nr_seq_trans_baixa_w,
					vl_trib_nao_retido_w,
					ie_vencimento_w,
					vl_base_nao_retido_w,
					vl_trib_adic_w,
					vl_base_adic_w,
					vl_reducao_w,
					vl_desc_dependente_w,
					cd_darf_w,
					sysdate,
					nm_usuario_p,
					cd_variacao_w,
					ie_periodicidade_w,
					cd_cond_pagto_w,
					nr_seq_regra_w);

				if	(ie_gerar_titulo_w = 'S') then
					gerar_titulo_tributo(nr_seq_imposto_w, nm_usuario_p);
				end if;

				vl_trib_acum_w	:= vl_trib_acum_w + trunc(vl_tributo_w, 2);

			end if;

		else
			insert into w_titulo_pagar_imposto
				(nr_sequencia,
				nr_titulo,
				nr_seq_baixa,
				cd_tributo,
				dt_atualizacao,
				nm_usuario,
				dt_imposto,
				vl_base_calculo,
				vl_imposto,
				pr_imposto,
				cd_beneficiario,
				cd_conta_financ,
				nr_seq_trans_reg,
				nr_seq_trans_baixa,
				vl_nao_retido,
				ie_vencimento,
				vl_base_nao_retido,
				vl_trib_adic,
				vl_base_adic,
				vl_reducao,
				vl_desc_base,
				cd_darf,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_variacao,
				ie_periodicidade,
				nr_bordero,
				cd_cond_pagto,/* Jean . 12/02/19  os 1829349*/
				nr_seq_escrit)
			values(	w_titulo_pagar_imposto_seq.nextval,
				nr_titulo_p,
				null,
				cd_tributo_w,
				sysdate,
				nm_usuario_p,
				obter_proximo_dia_util(cd_estabelecimento_w, dt_vencimento_w),
				vl_base_tributo_w,
				trunc(vl_tributo_w, 2),
				pr_aliquota_w,
				cd_beneficiario_w,
				cd_conta_financ_w,
				nr_seq_trans_reg_w,
				nr_seq_trans_baixa_w,
				vl_trib_nao_retido_w,
				ie_vencimento_w,
				vl_base_nao_retido_w,
				vl_trib_adic_w,
				vl_base_adic_w,
				vl_reducao_w,
				vl_desc_dependente_w,
				cd_darf_w,
				sysdate,
				nm_usuario_p,
				cd_variacao_w,
				ie_periodicidade_w,
				nr_bordero_p,
				cd_cond_pagto_w,/* Jean . 12/02/19  os 1829349*/
				nr_seq_escrit_p);
		end if;

	end if;
	<<proximo>>
	null;
end loop;
close c01;

atualizar_tit_pagar_classif(nr_titulo_p, 'N', nm_usuario_p);

atualizar_saldo_tit_pagar(nr_titulo_p, nm_usuario_p);

end gerar_tributo_titulo;
/
