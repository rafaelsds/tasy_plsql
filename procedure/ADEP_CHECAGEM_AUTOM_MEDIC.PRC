create or replace
procedure ADEP_CHECAGEM_AUTOM_MEDIC(nr_prescricao_p		number,
									nm_usuario_p		Varchar2) is 

cd_material_w			number(15,0);
cd_grupo_material_w		number(15,0);
cd_subgrupo_material_w	number(15,0);
cd_classe_material_w	number(15,0);
ie_via_aplicacao_w		varchar2(100);
ie_checar_w				varchar2(1) := 'N';
nr_sequencia_w			number(15,0);
nr_seq_horario_w		number(15);
nr_atendimento_w		number(15);
cd_setor_Atendimento_w	number(15);
dt_horario_w			date;
ds_texto_aux_w			varchar2(255);
dt_prescricao_w			date;
ie_tipo_item_w			varchar2(10);

cursor c01 is
select	cd_material,
		nr_sequencia,
		decode(ie_agrupador,2,'MAT','M')
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		ie_agrupador	in (1,2);
					
cursor c02 is
select	nr_sequencia,
		dt_horario
from	prescr_mat_hor
where	nr_prescricao	= nr_prescricao_p
and		nr_seq_material	= nr_sequencia_w
and		cd_material		= cd_material_w;

begin

select	max(nr_atendimento),
		max(cd_setor_Atendimento),
		max(dt_prescricao)
into	nr_atendimento_w,
		cd_setor_Atendimento_w,
		dt_prescricao_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

ds_texto_aux_w	:= wheb_mensagem_pck.get_texto(330416);

gravar_Log_tasy(90302,
	' nr_prescricao_p = ' || nr_prescricao_p || chr(10) ||
	' nr_atendimento_w = ' || nr_atendimento_w || chr(10) ||
	' cd_setor_Atendimento_w = ' || cd_setor_Atendimento_w || chr(10) ||
	' dt_prescricao_w = ' || dt_prescricao_w || chr(10) ||
	' Abertura C01 ADEP_CHECAGEM_AUTOM_MEDIC',
	nm_usuario_p);

open C01;
loop
fetch C01 into	
	cd_material_w,
	nr_sequencia_w,
	ie_tipo_item_w;
exit when C01%notfound;

	gravar_Log_tasy(90302,
		' nr_prescricao_p = ' || nr_prescricao_p || chr(10) ||
		' cd_material_w = ' || cd_material_w || chr(10) ||
		' nr_sequencia_w = ' || nr_sequencia_w || chr(10) ||
		' ie_tipo_item_w = ' || ie_tipo_item_w || chr(10) ||
		' Abertura C02 ADEP_CHECAGEM_AUTOM_MEDIC',
		nm_usuario_p);

	open C02;
	loop
	fetch C02 into	
		nr_seq_horario_w,
		dt_horario_w;
	exit when C02%notfound;

		gravar_Log_tasy(90302,
			' nr_prescricao_p = ' || nr_prescricao_p || chr(10) ||
			' nr_seq_horario_w = ' || nr_seq_horario_w || chr(10) ||
			' dt_horario_w = ' || dt_horario_w || chr(10) ||
			' Valores C02 ADEP_CHECAGEM_AUTOM_MEDIC',
			nm_usuario_p);
		
		update	prescr_mat_hor
		set		dt_fim_horario 	= dt_prescricao_w,
				nm_usuario		= nm_usuario_p,
				nm_usuario_adm	= nm_usuario_p,			
				dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_horario_w
		and		dt_fim_horario	is null
		and		dt_suspensao 	is null;
		
		gerar_alter_hor_prescr_adep(nr_atendimento_w, ie_tipo_item_w, cd_material_w, nr_prescricao_p, nr_sequencia_w, nr_seq_horario_w, dt_horario_w, 3, null, ds_texto_aux_w, null, nm_usuario_p, dt_prescricao_w);
		
	end loop;
	close C02;
	
end loop;
close C01;	

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end ADEP_CHECAGEM_AUTOM_MEDIC;
/