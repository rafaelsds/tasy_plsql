CREATE OR REPLACE
PROCEDURE BAIXAR_PROCESSOS
			(nr_atendimento_p	number,
			cd_processo_p		number,
			nr_sequencia_p		number) is

begin

update	processo_atendimento
set	dt_fim_real	= sysdate,
	ie_status	= 'F'
where	nr_atendimento	= nr_atendimento_p
and	cd_processo	= cd_processo_p
and	nr_sequencia	= nr_sequencia_p;


update	atendimento_paciente
set	ie_fim_conta	= 'F',
	dt_fim_conta	= sysdate
where	nr_atendimento	= nr_atendimento_p
and	ie_fim_conta	= 'P'
and 	dt_alta 	is not null
and 	not exists 	(select nr_atendimento
	from 		processo_atendimento
	where 		nr_atendimento	= nr_atendimento_p
        and 		dt_fim_real is null);

end BAIXAR_PROCESSOS;
/