create or replace
procedure pls_alterar_status_analise_cta(	nr_seq_analise_p	pls_analise_conta.nr_sequencia%type,
						ie_status_p		varchar2,
						ds_rotina_p		varchar2,
						nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is

ie_analise_cm_nova_w	pls_parametros.ie_analise_cm_nova%type;

begin

update	pls_analise_conta
set	ie_status	= ie_status_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	nm_proc_status	= substr(ds_rotina_p,1,30)
where	nr_sequencia 	= nr_seq_analise_p;

if	(ie_status_p = 'L') then

	select	nvl(max(ie_analise_cm_nova),'N')
	into	ie_analise_cm_nova_w
	from	pls_parametros a
	where	a.cd_estabelecimento = cd_estabelecimento_p;
	
	if	(ie_analise_cm_nova_w = 'S') then

		update	pls_auditoria_conta_grupo
		set	dt_liberacao 		= sysdate,
			dt_final_analise	= sysdate,
			nm_auditor_atual	= decode(nm_auditor_atual, null, 'Tasy', nm_auditor_atual),
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_analise		= nr_seq_analise_p
		and	dt_liberacao is null;
	end if;
end if;

end pls_alterar_status_analise_cta;
/