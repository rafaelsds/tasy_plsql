create or replace 
procedure ATUALIZAR_GUIA_CONTA_AUTOR(nr_atendimento_p		in number,
					cd_convenio_p		in number,
				   	cd_autorizacao_p	in varchar2) is

qt_proc_conta_w	number(15);
qt_mat_conta_w	number(15);

begin

select 	count(*)
into 	qt_proc_conta_w
from 	procedimento_paciente a
where	a.nr_atendimento	= nr_atendimento_p
and 	a.cd_convenio 		= cd_convenio_p;

select 	count(*)
into 	qt_mat_conta_w
from 	material_atend_paciente a
where	a.nr_atendimento	= nr_atendimento_p
and 	a.cd_convenio 		= cd_convenio_p;

if	(qt_proc_conta_w > 0) then
	update	procedimento_paciente x
	set 	x.nr_doc_convenio	= nvl(cd_autorizacao_p, x.nr_doc_convenio)		
	where	x.nr_atendimento	= nr_atendimento_p
	and 	x.nr_sequencia		in (select	a.nr_sequencia
					   from 	procedimento_paciente a
					   where	a.nr_atendimento	= nr_atendimento_p
					   and		a.cd_convenio		= cd_convenio_p);	
end if;

if	(qt_mat_conta_w > 0) then
	update	material_atend_paciente x
	set	x.nr_doc_convenio	= nvl(cd_autorizacao_p, x.nr_doc_convenio)
	where	x.nr_atendimento	= nr_atendimento_p
	and 	x.nr_sequencia		in (select	a.nr_sequencia
					    from 	material_atend_paciente a
					    where	a.nr_atendimento 	= nr_atendimento_p
					    and		a.cd_convenio		= cd_convenio_p);
end if;

commit;

end ATUALIZAR_GUIA_CONTA_AUTOR;
/