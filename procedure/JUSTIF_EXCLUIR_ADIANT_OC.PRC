create or replace
procedure	JUSTIF_EXCLUIR_ADIANT_OC(	ds_historico_p	  varchar2,
						nm_usuario_p	  varchar2,
						nr_seq_ordem_p	  number,
						nr_adiantamento_p number) is

nr_seq_ordem_compra_hist_w	ordem_compra_hist.nr_sequencia%type;
nr_adiantamento_w		ordem_compra_adiant_pago.nr_ordem_compra%type;
vl_vinculado_w			ordem_compra_adiant_pago.vl_vinculado%type;
ds_retorno_w			ordem_compra_hist.ds_historico%type;

BEGIN

select	ordem_compra_hist_seq.nextval
into 	nr_seq_ordem_compra_hist_w
from 	dual;

select	nr_adiantamento,
	nvl(vl_vinculado,0)
into	nr_adiantamento_w,
	vl_vinculado_w
from	ordem_compra_adiant_pago
where	nr_ordem_compra = nr_seq_ordem_p
  and   nr_adiantamento = nr_adiantamento_p;

ds_retorno_w	:= WHEB_MENSAGEM_PCK.get_texto(284379,	'NR_ADIANTAMENTO_P='	|| nr_adiantamento_w 	|| ';' ||
							'VL_VINCULADO_P='	|| campo_mascara_virgula_casas(vl_vinculado_w,4) || ';') ||
							ds_historico_p;

insert into ORDEM_COMPRA_HIST
	(	nr_sequencia,
		ds_historico,
		ds_titulo,
		dt_atualizacao,
		dt_historico,
		nm_usuario,
		nr_ordem_compra)
	values(	nr_seq_ordem_compra_hist_w,
		ds_retorno_w,
		WHEB_MENSAGEM_PCK.get_texto(284380),
		sysdate,
		sysdate,
		nm_usuario_p,
		nr_seq_ordem_p);
		
END 	JUSTIF_EXCLUIR_ADIANT_OC;
/