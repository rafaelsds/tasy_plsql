CREATE OR REPLACE
PROCEDURE EHR_Gerar_especialidade(
				nr_seq_elemento_p	Number,
				ds_lista_p		Varchar2,
				nm_usuario_p		Varchar2) IS 


lista_informacao_w		Varchar2(800);
ie_contador_w			Number(10,0)	:= 0;
tam_lista_w			Number(10,0);
ie_pos_virgula_w		Number(3,0);
nr_sequencia_w			number(10);
nr_seq_especialidade_w		number(10);


BEGIN

lista_informacao_w	:= ds_lista_p;

while	lista_informacao_w is not null or
	ie_contador_w > 200 loop
	begin
	tam_lista_w		:= length(lista_informacao_w);
	ie_pos_virgula_w	:= instr(lista_informacao_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_especialidade_w	:= to_number(substr(lista_informacao_w,1,(ie_pos_virgula_w - 1)));
		lista_informacao_w	:= substr(lista_informacao_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;

	select	ehr_elemento_especialidade_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into ehr_elemento_especialidade(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_elemento,
		nr_seq_especialidade)
	values(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,	
		nr_seq_elemento_p,
		nr_seq_especialidade_w);	

	ie_contador_w	:= ie_contador_w + 1;
	end;
end loop;

commit;

END EHR_Gerar_especialidade;
/
