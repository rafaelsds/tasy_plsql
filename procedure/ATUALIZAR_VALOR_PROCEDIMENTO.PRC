create or replace
procedure atualizar_valor_procedimento(	nr_seq_nf_p		number,
					cd_convenio_p		number,
					cd_categoria_p		varchar2,
					cd_estabelecimento_p	number) is

cd_procedimento_w	number(15);
nr_item_nf_w		number(5);
qt_procedimento_w	number(13,2);
vl_preco_w		number(15,4);
vl_desconto_w		number(13,2);
vl_total_item_nf_w	number(13,2);


cursor c01 is
select	a.nr_item_nf,
	a.cd_procedimento,
	qt_item_nf,
	vl_desconto,
	nvl(Obter_preco_procedimento(	cd_estabelecimento_p,
					cd_convenio_p,
					cd_categoria_p,
					b.dt_entrada_saida,
					a.cd_procedimento,
					a.ie_origem_proced,
					0,0,0,null,null,null,null,0,0,'P'),0)
from	nota_fiscal_item a,
	nota_fiscal b
where	a.nr_sequencia = nr_seq_nf_p
and	a.cd_procedimento is not null
and	a.nr_sequencia = b.nr_sequencia;

begin

open c01;
loop
fetch c01 into	
	nr_item_nf_w,
	cd_procedimento_w,
	qt_procedimento_w,
	vl_desconto_w,
	vl_preco_w;
exit when c01%notfound;
	begin

	vl_total_item_nf_w := (vl_preco_w * qt_procedimento_w);

	update	nota_fiscal_item
	set	vl_unitario_item_nf = vl_total_item_nf_w / qt_procedimento_w,
		vl_liquido = vl_total_item_nf_w - vl_desconto_w,
		vl_total_item_nf = vl_total_item_nf_w
	where	nr_item_nf = nr_item_nf_w
	and	nr_sequencia = nr_seq_nf_p;
	commit;
	end;
end loop;
close c01;

end atualizar_valor_procedimento;
/