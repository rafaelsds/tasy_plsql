create or replace
procedure atualiza_reducao_ciclo(
			nr_seq_paciente_p		number,
			pr_reducao_p			number,
			nr_ciclo_p			number,
			ds_dia_ciclo_p			varchar2,
			nm_usuario_p			Varchar2) is 

begin

update 	paciente_atendimento
set 	pr_reducao	= pr_reducao_p
where 	nr_seq_paciente = nr_seq_paciente_p
and	nr_prescricao 	is null
and	somente_numero(ds_dia_ciclo) >= somente_numero(ds_dia_ciclo_p);
commit;

recalcula_dose_onc_ciclo(nr_seq_paciente_p,nm_usuario_p,0);


end atualiza_reducao_ciclo;
/