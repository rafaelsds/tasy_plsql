create or replace
procedure obter_oid_nom(cd_estabelecimento_p	number,
					cd_cgc_p	varchar2,
					ie_tipo_oid_p	varchar2,
					ds_oid_p	out varchar2,
					nm_oid_p	out varchar2)
 		    	is
			
ds_oid_w	cadastro_oid.ds_oid%type;			
nm_oid_w	cadastro_oid.nm_oid%type;




Cursor c01 is
select	ds_oid,
	nm_oid
from	cadastro_oid
where	(cd_estabelecimento = cd_estabelecimento_p or cd_cgc_p is not null)
and	(cd_cgc = cd_cgc_p or cd_cgc_p is null)
and	ie_tipo_oid	= ie_tipo_oid_p
order by
	nr_sequencia;

begin

for r_c01 in c01 loop
	begin
	ds_oid_p := r_c01.ds_oid; 
	nm_oid_p := r_c01.nm_oid; 
	end;
end loop;


end obter_oid_nom;
/
