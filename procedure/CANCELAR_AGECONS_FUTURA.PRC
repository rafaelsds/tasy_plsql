create or replace
procedure Cancelar_AgeCons_Futura
		(cd_agenda_p		number,
		dt_agenda_p		date,		
		cd_pessoa_fisica_p	varchar2,
		cd_motivo_p		varchar2,
		ds_observacao_p	varchar2,
		nm_usuario_p		varchar2,
		cd_profissional_p varchar2 default null) is

dt_cancelamento_w		date;
dt_atual_w				date;
dt_canc_nova_w			date;
nr_seq_agenda_w			number(10,0);
cd_tipo_agenda_w		number(10,0);
qt_solic_pront_cip_w	number(10);
ds_erro_w				varchar2(255);
qt_segundos_ajuste_w	number(10) := 0;
dt_cancelamento_aux_w 	date;
hr_agenda_w             varchar2(5);
cancela_hor_prof_w      varchar2(10);
cd_medico_w             varchar2(10);
dt_agenda_w				date;

cursor	c01 is
	select	nr_sequencia,cd_medico, to_char(dt_agenda,'hh24:mi') hr_agenda, dt_agenda
	from	agenda_consulta
	where	cd_agenda		= cd_agenda_p
	and	dt_agenda		>= dt_agenda_p
	and	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	

function calculaDataCancelamento ( dt_agenda_ww		date)
	return date is
begin
	--busca a maior data cancelada do minuto + 1 segundo
	select	/*+ INDEX(AGENDA_CONSULTA AGECONS_I4) */ 
			nvl(max(dt_agenda), dt_agenda_ww) + (1/86400)
	into	dt_cancelamento_aux_w
	from	agenda_consulta
	where	trunc(dt_agenda, 'mi') = trunc(dt_agenda_ww, 'mi')
	and		dt_agenda between trunc(dt_agenda_ww) and trunc(dt_agenda_ww) + (86399/86400);
	
	--caso a data obtida tenha ultrapassado o minuto, calcula a data para este novo minuto
	if (trunc(dt_agenda_ww,'mi') <> trunc(dt_cancelamento_aux_w,'mi')) then
		dt_cancelamento_aux_w := calculaDataCancelamento(dt_cancelamento_aux_w);
	end if;
	
	return dt_cancelamento_aux_w;

end calculaDataCancelamento;


begin
Obter_Param_Usuario(866,309,obter_perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo,cancela_hor_prof_w);
select	max(cd_tipo_agenda)
into	cd_tipo_agenda_w
from	agenda
where	cd_agenda	= cd_agenda_p;

open	c01;
loop
fetch	c01 into
	nr_seq_agenda_w,cd_medico_w,hr_agenda_w,dt_agenda_w;
exit	when c01%notfound;
	begin
	qt_solic_pront_cip_w	:= 0;
	
	select	dt_agenda
	into	dt_atual_w
	from 	agenda_consulta
	where	nr_sequencia	= nr_seq_agenda_w;
	
	select	/*+ INDEX(AGENDA_CONSULTA AGECONS_I4) */ 
			nvl(max(dt_agenda), dt_atual_w)
	into	dt_cancelamento_w
	from	agenda_consulta
	where	trunc(dt_agenda, 'mi') = trunc(dt_atual_w, 'mi')
	and		dt_agenda between trunc(dt_atual_w) and trunc(dt_atual_w) + (86399/86400);
	
	--Verificar se j� existe outro registro com a mesma data(AGECONS_UK)
	select	count(*)
	into	qt_segundos_ajuste_w
	from	agenda_consulta
	where	cd_agenda = cd_agenda_p
	and		to_char(dt_agenda, 'dd/mm/yyyy hh24:mi')	= to_char(dt_cancelamento_w, 'dd/mm/yyyy hh24:mi')
	and		dt_agenda between trunc(dt_cancelamento_w) and trunc(dt_cancelamento_w) + (86399/86400)
	and		ie_status_agenda	= 'C';
	
	select	dt_cancelamento_w + (qt_segundos_ajuste_w + 1)/86400
	into	dt_canc_nova_w
	from	dual;
	
	--Validar caso passar de 1 minuto para o pr�ximo agendamento
	select	count(*)
	into	qt_segundos_ajuste_w
	from	agenda_consulta
	where	cd_agenda = cd_agenda_p
	and		to_char(dt_agenda, 'dd/mm/yyyy hh24:mi')	= to_char(dt_canc_nova_w, 'dd/mm/yyyy hh24:mi')
	and		dt_agenda between trunc(dt_canc_nova_w) and trunc(dt_canc_nova_w) + (86399/86400)
	and		ie_status_agenda	= 'C';

	if (cancela_hor_prof_w = 'N' or ( cancela_hor_prof_w = 'S' and hr_agenda_w = to_char(dt_agenda_p,'hh24:mi') and cd_profissional_p = cd_medico_w and obter_cod_dia_semana(dt_agenda_p) = obter_cod_dia_semana(dt_agenda_w) ) or Obter_Funcao_Ativa <> 866) then
	
	begin
		update	agenda_consulta
		set	ie_status_agenda		= 'C',
			dt_agenda			= dt_canc_nova_w + (qt_segundos_ajuste_w + 1)/86400,
			nm_usuario			= nm_usuario_p,
			dt_atualizacao		= sysdate,
			cd_motivo_cancelamento	= cd_motivo_p,
			ds_observacao		= decode(cd_tipo_agenda_w,3,null,ds_observacao_p),
			ds_motivo_status	= decode(cd_tipo_agenda_w,3,ds_observacao_p,null),
			dt_status		= decode(cd_tipo_agenda_w,3,sysdate,null),
			nm_usuario_status	= decode(cd_tipo_agenda_w,3,nm_usuario_p,null)
		where	nr_sequencia			= nr_seq_agenda_w;
	
		exception
		when others then
			begin
				update	agenda_consulta
				set	ie_status_agenda		= 'C',
					dt_agenda			= dt_canc_nova_w + (1/1440) + (qt_segundos_ajuste_w + 1)/86400,
					nm_usuario			= nm_usuario_p,
					dt_atualizacao		= sysdate,
					cd_motivo_cancelamento	= cd_motivo_p,
					ds_observacao		= decode(cd_tipo_agenda_w,3,null,ds_observacao_p),
					ds_motivo_status	= decode(cd_tipo_agenda_w,3,ds_observacao_p,null),
					dt_status		= decode(cd_tipo_agenda_w,3,sysdate,null),
					nm_usuario_status	= decode(cd_tipo_agenda_w,3,nm_usuario_p,null)
				where	nr_sequencia			= nr_seq_agenda_w;
			exception
			when others then
				dt_canc_nova_w := calculaDataCancelamento(dt_cancelamento_w);
				
				update	agenda_consulta
				set	ie_status_agenda		= 'C',
					dt_agenda			= dt_canc_nova_w,
					nm_usuario			= nm_usuario_p,
					dt_atualizacao		= sysdate,
					cd_motivo_cancelamento	= cd_motivo_p,
					ds_observacao		= decode(cd_tipo_agenda_w,3,null,ds_observacao_p),
					ds_motivo_status	= decode(cd_tipo_agenda_w,3,ds_observacao_p,null),
					dt_status		= decode(cd_tipo_agenda_w,3,sysdate,null),
					nm_usuario_status	= decode(cd_tipo_agenda_w,3,nm_usuario_p,null)
				where	nr_sequencia			= nr_seq_agenda_w;
			end;
	end;
	
	end if;
	--Cancelar solicita��es de prontu�rio futuras
	select	count(*)
	into	qt_solic_pront_cip_w
	from	same_cpi_solic
	where	nr_seq_agenda = nr_seq_agenda_w;
	
	if (cancela_hor_prof_w = 'N' or ( cancela_hor_prof_w = 'S' and hr_agenda_w = to_char(dt_agenda_p,'hh24:mi') and cd_profissional_p = cd_medico_w and obter_cod_dia_semana(dt_agenda_p) = obter_cod_dia_semana(dt_agenda_w) ) or Obter_Funcao_Ativa <> 866) then
	if	(qt_solic_pront_cip_w > 0)then
		begin
		cancelar_solic_pront_agenda(nr_seq_agenda_w, nm_usuario_p);
		cancelar_solic_pront_agenda_gp(nr_seq_agenda_w, nm_usuario_p);
		
		exception
		when others then
			ds_erro_w := substr(sqlerrm,1,255);
		end;
	end if;
	end if;

	end;
end loop;
close c01;

commit;

end Cancelar_AgeCons_Futura;
/