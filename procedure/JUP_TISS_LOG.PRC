create or replace 
procedure JUP_TISS_LOG(	nr_sequencia_p		number,
			ie_status_p		varchar,
			nm_usuario_p		varchar2) is
begin

update	tiss_log
set	ie_status 	= nvl(ie_status_p, ie_status)
where	nr_sequencia	= nr_sequencia_p;

commit;

end JUP_TISS_LOG;
/
