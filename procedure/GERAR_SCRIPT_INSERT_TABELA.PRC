create or replace
procedure gerar_script_insert_tabela (
		nm_tabela_p	varchar2,
		nm_usuario_p	varchar2) is

nm_atributo_w			varchar2(50);
ie_tipo_atributo_w		varchar2(10);
qt_tamanho_w			number(10,0);
qt_decimais_w			number(10,0);
ie_obrigatorio_w		varchar2(1);

ds_declaracao_variavel_w	varchar2(2000);
ds_variaveis_w			long;

ds_atributo_insert_w		varchar2(2000);
ds_clausula_insert_w		long;

ds_atributo_values_w		varchar2(2000);
ds_clausula_values_w		long;

tab_w				varchar2(4) := '	';

ds_script_insert_w		long;

qt_tab_w			number(10,0);
nr_tab_w			number(10,0);

ie_primeiro_loop_w		varchar2(1) := 'S';
ie_atrib_ant_obrig_w		varchar2(1);

cursor c01 is
select	nm_atributo,
	ie_tipo_atributo,
	qt_tamanho,
	qt_decimais,
	ie_obrigatorio
from	tabela_atributo
where	lower(nm_tabela) = lower(nm_tabela_p)
and	ie_tipo_atributo not in ('FUNCTION','VISUAL')
order by
	nr_sequencia_criacao;
		
begin
if	(nm_tabela_p is not null) and
	(nm_usuario_p is not null) then
	begin
	exec_sql_dinamico(nm_usuario_p, 'truncate table w_implementacao_wjpum');
	
	ie_primeiro_loop_w	:= 'S';
	ds_variaveis_w		:= null;
	ds_clausula_insert_w	:= 'insert into ' || lower(nm_tabela_p) || ' (' || chr(10);
	ds_clausula_values_w	:= chr(10) || ' values (' || chr(10);
	
	open c01;
	loop
	fetch c01 into	nm_atributo_w,
			ie_tipo_atributo_w,
			qt_tamanho_w,
			qt_decimais_w,
			ie_obrigatorio_w;
	exit when c01%notfound;
		begin
		-- Montar declara��o vari�veis
		ds_declaracao_variavel_w := lower(nm_atributo_w) || '_w';
		
		while (length(ds_declaracao_variavel_w) < 50) loop
			begin
			ds_declaracao_variavel_w := ds_declaracao_variavel_w || ' ';
			end;
		end loop;
		
		ds_declaracao_variavel_w := ds_declaracao_variavel_w || tab_w;
		
		if	(ie_tipo_atributo_w = 'NUMBER') then
			begin
			ds_declaracao_variavel_w := ds_declaracao_variavel_w || lower(ie_tipo_atributo_w) || '(' || qt_tamanho_w || ',' || qt_decimais_w || ');';
			end;
		elsif	(ie_tipo_atributo_w = 'VARCHAR2') then
			begin
			ds_declaracao_variavel_w := ds_declaracao_variavel_w || lower(ie_tipo_atributo_w) || '(' || qt_tamanho_w || ');';
			end;
		else
			begin
			ds_declaracao_variavel_w := ds_declaracao_variavel_w || lower(ie_tipo_atributo_w) || ';';
			end;
		end if;
		
		ds_declaracao_variavel_w := ds_declaracao_variavel_w || chr(10);
		
		ds_variaveis_w := ds_variaveis_w || ds_declaracao_variavel_w;
		
		-- Montar cl�usula insert
		if	(ie_primeiro_loop_w = 'S') then
			begin
			ds_clausula_insert_w := ds_clausula_insert_w || tab_w || lower(nm_atributo_w);
			end;
		else
			begin
			ds_clausula_insert_w := ds_clausula_insert_w || ',' || chr(10) || tab_w || lower(nm_atributo_w);
			end;
		end if;
		
		-- Montar cl�usula values
		if	(ie_primeiro_loop_w = 'S') then
			begin
			ds_clausula_values_w := ds_clausula_values_w || tab_w || lower(nm_atributo_w) || '_w';
			end;
		else
			begin
			ds_clausula_values_w := ds_clausula_values_w || ',';
			
			if	(ie_atrib_ant_obrig_w = 'S') then
				begin
				ds_clausula_values_w := ds_clausula_values_w || ' -- >>> ATRIBUTO OBRIGAT�RIO !!! <<<';
				end;
			end if;
			
			ds_clausula_values_w := ds_clausula_values_w || chr(10) || tab_w || lower(nm_atributo_w) || '_w';
			end;
		end if;
		
		ie_primeiro_loop_w := 'N';		
		ie_atrib_ant_obrig_w := ie_obrigatorio_w;
		end;
	end loop;
	close c01;
	
	-- Montar script
	ds_clausula_insert_w := ds_clausula_insert_w || ')';
	ds_clausula_values_w := ds_clausula_values_w || ');';
	
	ie_atrib_ant_obrig_w := 'S';
	-- Tratar obrigatoriedade �ltimo atributo loop
	if	(ie_atrib_ant_obrig_w = 'S') then
		begin
		ds_clausula_values_w := ds_clausula_values_w || ' -- >>> ATRIBUTO OBRIGAT�RIO !!! <<<';
		end;
	end if;
	
	ds_script_insert_w := ds_variaveis_w || chr(10) || ds_clausula_insert_w || ds_clausula_values_w;
	
	insert into w_implementacao_wjpum (
		ds_implementacao,
		id)
	values (
		ds_script_insert_w,
		'T');
	end;
end if;
commit;
end gerar_script_insert_tabela;
/