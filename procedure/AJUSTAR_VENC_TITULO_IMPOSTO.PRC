create or replace
procedure ajustar_venc_titulo_imposto(
			nr_titulo_p	number,
			nm_usuario_p	varchar2) is


-- atecao !!! nao dar commit nesta procedure

cd_estabelecimento_w	number(04,0);
dt_imposto_w		date;
nr_seq_imposto_w		number(10,0);
dt_emissao_w		date;
dt_vencimento_w		date;
dt_liquidacao_w		date;
nr_titulo_w		number(10,0);
cd_cond_pagto_w		number(10,0);
qt_venc_w		number(05,0);
ds_venc_w		varchar2(2000);
nr_seq_alt_w		number(10);
ie_dt_contab_tit_trib_w	varchar2(2);

cursor c01 is
select	b.cd_estabelecimento,
	b.nr_titulo,
	a.cd_cond_pagto,
	a.dt_imposto,
	a.nr_sequencia
from	tributo c,
	titulo_pagar b,
	titulo_pagar_imposto a
where	a.nr_titulo			= nr_titulo_p
and	a.nr_sequencia		= b.nr_seq_tributo
and	dt_liquidacao_w		is not null
and	nvl(a.ie_vencimento,'V')	= 'V'
and	c.cd_tributo		= a.cd_tributo
and	nvl(c.ie_atualizar_venc_trib, 'S') = 'S';

cursor c02 is
select	b.nr_titulo
from	tributo c,
	titulo_pagar b,
	titulo_pagar_imposto a
where	a.nr_titulo			= nr_titulo_p
and	a.nr_sequencia		= b.nr_seq_tributo
and	dt_liquidacao_w		is not null
and	nvl(c.ie_dt_contab_tit_trib,'X')	in ('V','L')
and	c.cd_tributo		= a.cd_tributo
and	nvl(b.nr_lote_contabil,0) 	= 0
and	c.ie_atualizar_trib_liq 	= 'S'
and	b.dt_liquidacao is null;

begin

select	dt_liquidacao
into	dt_liquidacao_w
from	titulo_pagar
where 	nr_titulo	= nr_titulo_p;


open c01;
loop
fetch c01 into 
	cd_estabelecimento_w,
	nr_titulo_w,
	cd_cond_pagto_w,
	dt_imposto_w,
	nr_seq_imposto_w;
exit when c01%notfound;

	begin

	dt_vencimento_w	:= null;
	
	if	(cd_cond_pagto_w is not null) then
		calcular_vencimento(cd_estabelecimento_w, cd_cond_pagto_w,
				dt_liquidacao_w, qt_venc_w, ds_venc_w);


		if	(qt_venc_w = 1) then	
			dt_vencimento_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(to_date(substr(ds_venc_w,1,10),'dd/mm/yyyy'));
		end if;
	end if;

	if	(dt_vencimento_w is not null) then
		begin

		update	titulo_pagar_imposto
		set	dt_imposto	= dt_vencimento_w
		where	nr_sequencia	= nr_seq_imposto_w;

		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_alt_w
		from	titulo_pagar_alt_venc 
		where	nr_titulo = nr_titulo_w;
	
		insert into titulo_pagar_alt_venc (		/* rafael. os48885. 29/01/2007. */
			nr_titulo,
			nr_sequencia,
			dt_anterior,
			dt_vencimento,
			dt_atualizacao,
			nm_usuario,
			dt_alteracao)
		values(	nr_titulo_w, 
			nr_seq_alt_w, 
			dt_vencimento_w,
			dt_vencimento_w,
			sysdate,
			nm_usuario_p,
			pkg_date_utils.get_time(SYSDATE,0,0,0));

		update	titulo_pagar
		set	dt_vencimento_atual	= dt_vencimento_w
		where	nr_titulo			= nr_titulo_w;

		end;
	end if;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into	
	nr_titulo_w;
exit when c02%notfound;
	begin
	update	titulo_pagar
	set	dt_contabil	= dt_liquidacao_w,
		dt_emissao 	= dt_liquidacao_w
	where	nr_titulo		= nr_titulo_w;
	end;
end loop;
close c02;

end ajustar_venc_titulo_imposto;
/
