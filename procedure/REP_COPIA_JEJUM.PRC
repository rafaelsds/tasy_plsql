create or replace
procedure REP_copia_jejum(nr_prescricao_orig_p	number,
			nr_prescricao_nova_p	number,
			nr_seq_regra_p		number,
			nm_usuario_p		Varchar2) is 

ie_copia_valid_igual_w		varchar2(1);
dt_validade_origem_w		date;
dt_validade_nova_w		date;
cd_setor_atendimento_w		number(10);
cd_estabelecimento_w		Number(4,0);
nr_atendimento_w		number(15,0);
dt_inicio_prescr_w		date;
dt_primeiro_horario_w		date;
dt_prescricao_www		date;
nr_horas_validade_w		number(5);
ie_prim_horario_setor_w		varchar2(10);
hr_setor_w			varchar2(20);

begin

insert into rep_jejum
	(nr_sequencia,
	nr_prescricao,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_tipo,
	nr_seq_objetivo,
	ie_inicio,
	dt_inicio,
	qt_min_ant,
	qt_min_depois,
	dt_evento,
	ds_evento,
	ds_observacao,
	dt_fim,
	qt_hora_ant,
	qt_hora_depois,
	ie_suspenso,
	ie_repete_copia,
	nr_seq_anterior,
	nr_prescricao_original)
select  rep_jejum_seq.nextval,
	nr_prescricao_nova_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_tipo,
	nr_seq_objetivo,
	ie_inicio,
	dt_inicio,
	qt_min_ant,
	qt_min_depois,
	dt_evento,
	ds_evento,
	ds_observacao,
	dt_fim,
	qt_hora_ant,
	qt_hora_depois,
	ie_suspenso,
	ie_repete_copia,
	nr_sequencia,
	nr_prescricao_orig_p
from    rep_jejum
where   nr_prescricao   = nr_prescricao_orig_p
and	ie_repete_copia	= 'S'
and     ie_suspenso     <> 'S';

commit;

end REP_copia_jejum;
/
