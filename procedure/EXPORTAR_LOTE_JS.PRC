create or replace
procedure exportar_lote_js(	nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p is not null)then
	begin
	
	update 	lab_lote_result_externo 
	set 	dt_envio     = sysdate 
	where 	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end exportar_lote_js;
/