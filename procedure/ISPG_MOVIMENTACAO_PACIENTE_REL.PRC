create or replace
procedure ISPG_movimentacao_paciente_rel(
					dt_inicial_p		date,
					dt_final_p		date,
					qt_remanescente_p	number) is 

qt_remanesc_w		number(10);
qt_existentes_w		number(10);
qt_internacoes_w	number(10);
dt_internacoes_w	varchar2(2);
dt_dia_w		varchar2(2);	
qt_alta_w		number(10);
qt_remas_exist_w	number(10);
ie_primeiro_dia_w	varchar2(1);
total_rema_w		number(10);
total_inter_w		number(10);
total_alta_w		number(10);
total_exis_w		number(10);



Cursor C01 is
	select	to_char(dt_dia,'dd')
	from	dia_v 
	where	dt_dia between dt_inicial_p and dt_final_p;
						
begin

 /*CREATE TABLE ISPG_movimentacao_paciente(dia VARCHAR2(10), campo1 number(10), campo2 NUMBER(10), campo3 NUMBER(10), campo4 NUMBER(10))
 */

delete	ISPG_movimentacao_paciente;

ie_primeiro_dia_w := 'S';

total_rema_w	:= 0;
total_inter_w	:= 0;
total_alta_w    := 0;
total_exis_w	:= 0;

open C01;
loop
fetch C01 into	
	dt_dia_w;
exit when C01%notfound;
	begin
	
	--remanescentes
	
	if	(ie_primeiro_dia_w = 'S') then
		qt_remanesc_w := qt_remanescente_p;
	else
		
		qt_remanesc_w := qt_remas_exist_w;
		
	end if;

	select	count(*)
	into	qt_alta_w
	from 	alta_hospitalar_v
	where 	dt_alta_interno between dt_inicial_p and fim_dia(dt_final_p)
	and	to_char(dt_alta_interno,'dd') = dt_dia_w
	and     obter_classif_setor(cd_setor_atendimento) not in (4);

	
	--internações

	select	count(*)
	into	qt_internacoes_w
	from	admissao_hospitalar_v
	where	dt_entrada between dt_inicial_p and fim_dia(dt_final_p)
	and	to_char(dt_entrada,'dd') = dt_dia_w
	and     obter_classif_setor(cd_setor_atendimento) not in (4);
	
	
	qt_existentes_w := ((qt_remanesc_w + qt_internacoes_w) - qt_alta_w);
	
	qt_remas_exist_w := qt_existentes_w;
	
	insert into ISPG_movimentacao_paciente( dia,
					campo1,
					campo2,
					campo3,
					campo4)
				values( dt_dia_w,
					qt_remanesc_w,
					qt_internacoes_w,
					qt_alta_w,
					nvl(qt_existentes_w,0));
	
	ie_primeiro_dia_w := 'N';

	total_rema_w	:= total_rema_w + qt_remanesc_w;
	total_inter_w	:= total_inter_w + qt_internacoes_w;
	total_alta_w    := total_alta_w + qt_alta_w;
	total_exis_w	:= total_exis_w + nvl(qt_existentes_w,0);		

	end;
end loop;
close C01;

insert into ISPG_movimentacao_paciente( dia,
					campo1,
					campo2,
					campo3,
					campo4)
				values( 'Total',
					substr(nvl(total_rema_w,0),1,10),
					substr(nvl(total_inter_w,0),1,10),
					substr(nvl(total_alta_w,0),1,10),
					substr(nvl(total_exis_w,0),1,10));
	


commit;

end ISPG_movimentacao_paciente_rel;
/