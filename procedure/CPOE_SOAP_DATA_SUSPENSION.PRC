create or replace PROCEDURE cpoe_soap_data_suspension(nr_atendimento_p number,
									lista_itens_p varchar2,
									nm_usuario_p varchar2) is
ds_items_suspend_w          varchar2(2000);
ie_tipo_item_w      	    varchar2(255);
nr_seq_item_w               number(10);
ie_se_liberado_w            varchar2(1);
nr_cpoe_item_type_w         number (1);
cd_evolucao_w               evolucao_paciente.cd_evolucao%type;
ds_error_w                  varchar2(1000);

BEGIN
ds_items_suspend_w :=   substr(lista_itens_p,2,length(lista_itens_p) - 2 );

WHILE ds_items_suspend_w IS NOT NULL LOOP

    nr_seq_item_w := substr( ds_items_suspend_w,1,instr( ds_items_suspend_w,';') - 1);--254658 = sequencia do registro
    ds_items_suspend_w := substr( ds_items_suspend_w,instr( ds_items_suspend_w,';') + 1,length( ds_items_suspend_w ) - 1);--  ;M;S,  456786;D;N,  15648;P;S]

    ie_tipo_item_w := substr( ds_items_suspend_w,0,instr( ds_items_suspend_w,';') - 1);--'M' = Tipo item
    ds_items_suspend_w := substr( ds_items_suspend_w,instr( ds_items_suspend_w,';'),length( ds_items_suspend_w ));--;S,  456786;D;N,  15648;P;S]r

    ie_se_liberado_w := substr( ds_items_suspend_w,instr( ds_items_suspend_w,';') + 1,instr( ds_items_suspend_w,',') - 2);--'S' = Se liberado
    ds_items_suspend_w := substr( ds_items_suspend_w,instr( ds_items_suspend_w,',') + 1,length( ds_items_suspend_w ));-- 456786;D;N,  15648;P;S]

    if  (ie_se_liberado_w = 'S') then

        select decode (ie_tipo_item_w, 'N',0,'M',1,'P',2,'G',3,'R',4,'H',5,'D',6,'DI',6,'DP',6,'MA',8,'AP',9,null)
        into nr_cpoe_item_type_w
		from dual;

        select max(cd_evolucao)
        into cd_evolucao_w
        from clinical_note_soap_data
        where nr_seq_med_item = nr_seq_item_w
        and ie_med_rec_type ='CPOE_ITEMS'
        and ie_stage = 1
        and ie_soap_type = 'P'
        and  ie_cpoe_item_type = to_char(nr_cpoe_item_type_w);

	if(cd_evolucao_w is not null) then
        delete from clinical_note_soap_data
        where cd_evolucao = cd_evolucao_w
        and ie_med_rec_type ='CPOE_ITEMS'
        and ie_stage = 1
        and ie_soap_type = 'P'
        and nr_seq_med_item = nr_seq_item_w
        and ie_cpoe_item_type = to_char(nr_cpoe_item_type_w);
	end if;

   end if;
END LOOP;

clinical_notes_pck.soap_data_after_delete(cd_evolucao_w);
exception
WHEN VALUE_ERROR THEN
ds_error_w := substr(sqlerrm,1,1000);
	gravar_log_cpoe('CPOE_SOAP_DATA_SUSPENSION exception: '||ds_error_w||
			' nr_atendimento_p: '||nr_atendimento_p||
			' lista_itens_p: '||lista_itens_p||
			' nm_usuario_p: '||nm_usuario_p,nr_atendimento_p);
when others then
	rollback;
	ds_error_w := substr(sqlerrm,1,1000);
	gravar_log_cpoe('CPOE_SOAP_DATA_SUSPENSION exception: '||ds_error_w||
			' nr_atendimento_p: '||nr_atendimento_p||
			' lista_itens_p: '||lista_itens_p||
			' nm_usuario_p: '||nm_usuario_p,nr_atendimento_p);

commit;
END CPOE_SOAP_DATA_SUSPENSION;
/
