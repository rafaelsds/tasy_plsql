create or replace
procedure Gerar_Exame_Externo_Protocolo
		(nr_seq_pedido_p	number,
		nr_seq_protocolo_p	number,
		nm_usuario_p		varchar2) is

ds_dados_clinicos_w		varchar2(255);
ds_exame_ant_w			varchar2(255);
qt_exame_w			number(5);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_sequencia_w			number(10);
nr_seq_exame_w			number(10);
nr_seq_exame_lab_w		number(10);
nr_atendimento_w		number(10);
cd_convenio_w		number(10);
cd_categoria_w		varchar2(10);
ie_tipo_atendimento_w	number(3);	
ds_erro_w		varchar2(4000);	
cd_setor_atendimento_w	number(10);
ds_justificativa_w 	varchar2(255);
cd_procedimento_ww	number(15);
ie_origem_proced_ww	number(15);
nr_seq_proc_interno_w	number(10);
nr_seq_proc_interno_aux_w number(10);
cd_plano_convenio_w	varchar2(10);

Cursor C01 is
	select	c.nr_sequencia,
		nvl(c.qt_exame,1) qt_exame,
		b.nr_seq_apresent,
		b.ie_lado,
		c.ds_justificativa,
		c.cd_procedimento,
		c.ie_origem_proced,
		c.nr_seq_exame,
		c.nr_proc_interno,
		c.cd_material_exame
	from	med_exame_padrao c,
		med_exame_protocolo b,
		med_protocolo_exame a
	where	a.nr_sequencia	= nr_seq_protocolo_p
	and	a.nr_sequencia	= b.nr_seq_protocolo
	and	b.nr_seq_exame	= c.nr_sequencia
	union all
	select	null nr_sequencia,
		1 qt_exame,
		b.nr_seq_apresent,
		b.ie_lado,
		null ds_justificativa,
		b.cd_procedimento,
		b.ie_origem_proced,
		b.nr_seq_exame_lab nr_seq_exame,
		b.nr_proc_interno,
		NULL cd_material_exame
	from	med_exame_protocolo b,
		med_protocolo_exame a
	where	a.nr_sequencia	= nr_seq_protocolo_p
	and	a.nr_sequencia	= b.nr_seq_protocolo
	and	b.nr_seq_exame is null;
	
c01_w	c01%rowtype;

begin

select	max(ds_dados_clinicos),
	max(ds_exame_anterior),
	max(ds_justificativa)
into	ds_dados_clinicos_w,
	ds_exame_ant_w,
	ds_justificativa_w
from	med_protocolo_exame
where	nr_sequencia		= nr_seq_protocolo_p;

update	pedido_exame_externo
set	ds_dados_clinicos 	= substr(ds_dados_clinicos ||' '||ds_dados_clinicos_w,1,255),
	ds_exame_ant		= substr(ds_exame_ant ||' '||ds_exame_ant_w,1,255),
	ds_justificativa	= substr(ds_justificativa ||' '||ds_justificativa_w,1,255)
where	nr_sequencia		= nr_seq_pedido_p;

select	max(nr_atendimento)
into	nr_atendimento_w
from	pedido_exame_externo
where	nr_sequencia	= nr_seq_pedido_p;


/*
insert into pedido_exame_externo_item(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_pedido,
	nr_seq_exame,
	qt_exame,
	nr_seq_apresent,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_lado,
	ds_justificativa,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_exame_lab,
	nr_proc_interno)
select	pedido_exame_externo_item_seq.nextval,
	sysdate,
	nm_usuario_p,
	nr_seq_pedido_p,
	c.nr_sequencia,
	nvl(c.qt_exame,1),
	b.nr_seq_apresent,
	sysdate,
	nm_usuario_p,
	c.ie_lado,
	c.ds_justificativa,
	c.cd_procedimento,
	c.ie_origem_proced,
	c.nr_seq_exame,
	c.nr_proc_interno
from	med_exame_padrao c,
	med_exame_protocolo b,
	med_protocolo_exame a
where	a.nr_sequencia	= nr_seq_protocolo_p
and	a.nr_sequencia	= b.nr_seq_protocolo
and	b.nr_seq_exame	= c.nr_sequencia;

*/
open C01;
loop
fetch C01 into	


	c01_w;
exit when C01%notfound;
	begin

	nr_seq_exame_w		:= c01_w.nr_seq_exame;
	cd_procedimento_w	:= c01_w.cd_procedimento;
	ie_origem_proced_w	:= c01_w.ie_origem_proced;
	nr_seq_proc_interno_w	:= c01_w.nr_proc_interno;
	nr_sequencia_w		:= c01_w.nr_sequencia;

	IF	(nr_sequencia_w IS NOT NULL) THEN
		IF	(nr_seq_exame_w	IS NOT NULL) THEN

			SELECT	MAX(cd_convenio),
				MAX(cd_categoria),
				MAX(ie_tipo_Atendimento),
				MAX(cd_plano_convenio)
			INTO	cd_convenio_w,
				cd_categoria_w,
				ie_tipo_atendimento_w,
				cd_plano_convenio_w
			FROM	atendimento_paciente_v
			WHERE	nr_atendimento	= nr_atendimento_w;

			Obter_Exame_Lab_Convenio(	nr_seq_exame_w,
							cd_convenio_w,
							cd_categoria_w,
							ie_tipo_atendimento_w,
							wheb_usuario_pck.get_cd_estabelecimento,
							NULL,
							NULL,
							NULL,
							cd_plano_convenio_w,
							cd_setor_atendimento_w,
							cd_procedimento_ww,
							ie_origem_proced_ww,
							ds_erro_w,
							nr_seq_proc_interno_aux_w);

			if	(nr_seq_proc_interno_aux_w > 0) then
				nr_seq_proc_interno_w 	:= nr_seq_proc_interno_aux_w;
			end if;
			if	(nvl(cd_procedimento_ww,0)	<> 0) then
				cd_procedimento_w	:= cd_procedimento_ww;
				ie_origem_proced_w	:= ie_origem_proced_ww;
			end if;	
		elsif	(nr_seq_proc_interno_w	is not null) then
			Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,null,nr_atendimento_w,null,cd_procedimento_w,ie_origem_proced_w,null,null);	
		end if;
	end if;
	
	insert into pedido_exame_externo_item(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_pedido,
						nr_seq_exame,
						qt_exame,
						nr_seq_apresent,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_lado,
						ds_justificativa,
						cd_procedimento,
						ie_origem_proced,
						nr_seq_exame_lab,
						nr_proc_interno,
						NR_SEQ_PROTOCOLO,
						cd_material_exame)
values		(	pedido_exame_externo_item_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_pedido_p,
						c01_w.nr_sequencia,
						nvl(c01_w.qt_exame,1),
						c01_w.nr_seq_apresent,
						sysdate,
						nm_usuario_p,
						c01_w.ie_lado,
						c01_w.ds_justificativa,
						cd_procedimento_w,
						ie_origem_proced_w,
						nr_seq_exame_w,
						c01_w.nr_proc_interno,
						nr_seq_protocolo_p,
						c01_w.cd_material_exame);
	end;
end loop;
close C01;
	
commit;

end Gerar_Exame_Externo_Protocolo;
/
