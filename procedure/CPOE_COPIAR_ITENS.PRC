create or replace procedure cpoe_copiar_itens(
						nr_atendimento_p			atendimento_paciente.nr_atendimento%type,
						dt_referencia_p				date,
						nm_usuario_p				usuario.nm_usuario%type,
						cd_perfil_p					perfil.cd_perfil%type,
						cd_setor_atendimento_p		setor_atendimento.cd_setor_atendimento%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						cd_pessoa_fisica_p			pessoa_fisica.cd_pessoa_fisica%type default null,
						ie_tipo_item_copia_fut_p	varchar2 default 'N',
						ie_adep_p					varchar2 default 'S',
						dt_inicio_copia_p			date default null) is

ds_item_prescricao_w			varchar2(4000);
nm_usuario_lib_enf_w			usuario.nm_usuario%type;
cd_farmac_lib_w					cpoe_material.cd_farmac_lib%type;
nr_horas_copia_w				number(15) := 0;
ie_inconsistencia_out_w			varchar2(2000);
nr_prescricao_w					prescr_medica.nr_prescricao%type;
ie_susp_prescricoes_alta_w		varchar2(1);
dt_prim_horario_w				date;
dt_referencia_w					date;
ie_copia_sem_lib_enf_farm_w		varchar2(1) := 'N';
cd_setor_atendimento_w			cpoe_material.cd_setor_atendimento%type;
ie_motivo_prescricao_w			prescr_medica.ie_motivo_prescricao%type;
ds_exception_w					varchar2(2000);
ds_prescricoes_geradas_w 		varchar2(4000);
qt_hors_before_w				number(3);
dt_inicio_regra_w				date;
dt_fim_regra_w					date;
is_antecipated_batch_w			varchar2(1);
dt_prox_geracao_w				date;
is_antecipated_item_w			varchar2(1);
sql_w               varchar2(300);
dt_ref_copia_w				date;

/*LISTAGG e usada para concatenar colunas e linhas em uma unica coluna/linha, alteramos para esse metodo para simplificar a forma antiga. OS 1741732*/
/*NAO DEVE SER UTILIZADO O LISTAGG DEVIDO A LIMITACAO DO ORACLE 10g. CRIADA PACKAGE CPOE_LISTAGG_PCK COMO ALTERNATIVA */
cursor c01 is
select cpoe_listagg_pck.tab_to_string(cast(collect(ie_tipo_item || ',' || nr_sequencia) as strarray)) ds_itens,
      nm_usuario_lib_enf,
      cd_farmac_lib,
      min(dt_prim_horario) dt_prim_horario,
      cd_setor_atendimento,
      ie_motivo_prescricao,
	  dt_prox_geracao,
	  is_antecipated_item
from	(
		select	'M' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_w, a.hr_prim_horario,'M',nr_horas_copia_w, dt_inicio_copia_p, a.cd_intervalo) dt_prim_horario,
				nvl(a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				null dt_prox_geracao,
				null is_antecipated_item
		from	cpoe_material a
		where	((decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.nm_usuario_nrec = nm_usuario_p
		and		a.dt_prox_geracao between dt_referencia_p - 3 and dt_referencia_p - 1/86400
		and   	nvl(a.si_dispensation_criteria, 'D') <> 'S'
		and		a.nr_atendimento = nr_atendimento_p
		and		nvl(a.cd_perfil_ativo, cd_perfil_p) = cd_perfil_p
		and		a.dt_liberacao is not null
		and		nvl(a.nr_seq_receita_amb,0) = 0
		and		ie_tipo_item_copia_fut_p = 'N'
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and		((((nvl(a.ie_evento_unico,'N') = 'N') and
				   (a.nr_seq_adicional is null) and
				   (a.nr_seq_ataque is null)) and
				  (nvl(a.ie_material,'N') = 'N')) or
				 (not exists(	select	1
								from	prescr_medica y,
										prescr_material z
								where	y.nr_prescricao = z.nr_prescricao
								and		y.nr_atendimento = nr_atendimento_p
								and		z.nr_seq_mat_cpoe = a.nr_sequencia )))		
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
		union all
		select	'M' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_w, a.hr_prim_horario,'M',nr_horas_copia_w, dt_inicio_copia_p, a.cd_intervalo) dt_prim_horario,
				nvl(a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				a.dt_prox_geracao,
				'S' is_antecipated_item
		from	cpoe_material a
		where	((decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.dt_prox_geracao between dt_inicio_regra_w - (nr_horas_copia_w/24)  and dt_fim_regra_w - (nr_horas_copia_w/24) - 1/86400
		and		dt_referencia_p > (dt_inicio_regra_w - 1)
		and		a.nr_atendimento = nr_atendimento_p
		and 	nvl(is_antecipated_batch_w, 'N') = 'Y'
		and		a.dt_liberacao is not null
		and		nvl(a.nr_seq_receita_amb,0) = 0
		and		ie_tipo_item_copia_fut_p = 'N'
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and		((((nvl(a.ie_evento_unico,'N') = 'N') and
				   (a.nr_seq_adicional is null) and
				   (a.nr_seq_ataque is null)) and
				  (nvl(a.ie_material,'N') = 'N')) or
				 (not exists(	select	1
								from	prescr_medica y,
										prescr_material z
								where	y.nr_prescricao = z.nr_prescricao
								and		y.nr_atendimento = nr_atendimento_p
								and		z.nr_seq_mat_cpoe = a.nr_sequencia )))		
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
		union all
		select	'N' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_w, a.hr_prim_horario,'N',nr_horas_copia_w, dt_inicio_copia_p, null) dt_prim_horario,
				nvl(a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				null dt_prox_geracao,
				null is_antecipated_item
		from	cpoe_dieta a
		where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.nm_usuario_nrec = nm_usuario_p
		and		a.dt_prox_geracao between dt_referencia_p - 3 and dt_referencia_p - 1/86400
		and		a.nr_atendimento = nr_atendimento_p
		and		nvl(a.cd_perfil_ativo, cd_perfil_p) = cd_perfil_p
		and		((ie_tipo_item_copia_fut_p = 'N') or (a.ie_tipo_dieta = 'O'))
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		a.dt_liberacao is not null
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and		((nvl(a.ie_evento_unico,'N') = 'N') or
				 (not exists (	select	1
								from	prescr_medica y
								where	y.nr_atendimento = nr_atendimento_p
								and		exists (	select	1
													from	prescr_material z
													where	z.nr_prescricao = y.nr_prescricao
													and		z.nr_seq_dieta_cpoe = a.nr_sequencia
													union
													select	1
													from	prescr_dieta z
													where	z.nr_prescricao = y.nr_prescricao
													and		z.nr_seq_dieta_cpoe = a.nr_sequencia
													union
													select	1
													from	rep_jejum z
													where	z.nr_prescricao = y.nr_prescricao
													and		z.nr_seq_dieta_cpoe = a.nr_sequencia
													union
													select	1
													from	prescr_leite_deriv z
													where	z.nr_prescricao = y.nr_prescricao
													and		z.nr_seq_dieta_cpoe = a.nr_sequencia
													union
													select	1
													from	nut_pac z
													where	z.nr_prescricao = y.nr_prescricao
													and		z.nr_seq_npt_cpoe = a.nr_sequencia))))
		and		nvl(a.ie_dose_unica,'N') = 'N'
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
		union all
		select	'R' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_w, a.hr_prim_horario,'R',nr_horas_copia_w, dt_inicio_copia_p, null) dt_prim_horario,
				nvl(a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				null dt_prox_geracao,
				null is_antecipated_item
		from	cpoe_recomendacao a
		where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.nm_usuario_nrec = nm_usuario_p
		and		a.dt_prox_geracao between dt_referencia_p - 3 and dt_referencia_p - 1/86400
		and		a.nr_atendimento = nr_atendimento_p
		and		nvl(a.cd_perfil_ativo, cd_perfil_p) = cd_perfil_p
		and		ie_tipo_item_copia_fut_p = 'N'
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		a.dt_liberacao is not null
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and		((nvl(a.ie_evento_unico,'N') = 'N') or
				 (not exists (	select	1
								from	prescr_medica y,
										prescr_recomendacao z
								where	y.nr_prescricao = z.nr_prescricao
								and		y.nr_atendimento = nr_atendimento_p
								and		z.nr_seq_rec_cpoe = a.nr_sequencia)))
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
		union all
		select	'G' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_w, a.hr_prim_horario,'G',nr_horas_copia_w, dt_inicio_copia_p, null) dt_prim_horario,
				nvl(a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				null dt_prox_geracao,
				null is_antecipated_item
		from	cpoe_gasoterapia a
		where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.nm_usuario_nrec = nm_usuario_p
		and		a.dt_prox_geracao between dt_referencia_p - 3 and dt_referencia_p - 1/86400
		and		a.nr_atendimento = nr_atendimento_p
		and		nvl(a.cd_perfil_ativo, cd_perfil_p) = cd_perfil_p
		and		ie_tipo_item_copia_fut_p = 'N'
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		a.dt_liberacao is not null
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
		union all
		select	'P' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_p - (qt_hors_before_w/24), a.hr_prim_horario,'P',nr_horas_copia_w, dt_inicio_copia_p, null) dt_prim_horario,
				coalesce(a.cd_setor_liberacao, a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				null dt_prox_geracao,
				null is_antecipated_item
		from	cpoe_procedimento a
		where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.nm_usuario_nrec = nm_usuario_p
		and		a.dt_prox_geracao between dt_referencia_p - 3 and dt_referencia_p - 1/86400
		and		a.nr_atendimento = nr_atendimento_p
		and		nvl(a.cd_perfil_ativo, cd_perfil_p) = cd_perfil_p
		and		ie_tipo_item_copia_fut_p = 'N'
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		a.dt_liberacao is not null
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and		((nvl(a.ie_evento_unico,'N') = 'N' and consiste_se_copia_proc_int(a.nr_seq_proc_interno) = 'S') or
				 (not exists(	select	1
								from	prescr_medica y,
										prescr_procedimento z
								where	y.nr_prescricao = z.nr_prescricao
								and		y.nr_atendimento = nr_atendimento_p
								and		z.nr_seq_proc_cpoe = a.nr_sequencia)))
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
		union all
		select	'D' ie_tipo_item,
				a.nr_sequencia,
				a.nm_usuario_lib_enf,
				a.cd_farmac_lib,
				get_concatenated_date(dt_referencia_w, to_char(a.dt_inicio,'hh24:mi'),'D',nr_horas_copia_w, dt_inicio_copia_p, null) dt_prim_horario,
				nvl(a.cd_setor_atendimento, cpoe_obter_setor_atend_prescr(a.nr_atendimento, cd_estabelecimento_p, a.cd_perfil_ativo, a.nm_usuario_nrec)) cd_setor_atendimento,
				a.ie_motivo_prescricao,
				null dt_prox_geracao,
				null is_antecipated_item
		from	cpoe_dialise a
		where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (nr_horas_copia_w/24))) or
				 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
		and		a.nm_usuario_nrec = nm_usuario_p
		and		a.dt_prox_geracao between dt_referencia_p - 3 and dt_referencia_p - 1/86400
		and		a.nr_atendimento = nr_atendimento_p
		and		nvl(a.cd_perfil_ativo, cd_perfil_p) = cd_perfil_p
		and		ie_tipo_item_copia_fut_p = 'N'
		and		nvl(ie_adep,'S') = nvl(ie_adep_p,'S')
		and		a.dt_liberacao is not null
		and		nvl(a.ie_retrogrado,'N') = 'N'
		and		((nvl(a.ie_evento_unico,'N') = 'N') or
				 (not exists (	select	1
								from	prescr_medica y,
										hd_prescricao z
								where	y.nr_prescricao = z.nr_prescricao
								and		y.nr_atendimento = nr_atendimento_p
								and		z.nr_seq_dialise_cpoe = a.nr_sequencia)))
		and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (nr_horas_copia_w/24)))))
		and		nvl(a.cd_funcao_origem,2314) = 2314
		and		((cpoe_obter_se_copia_item_term(a.ds_horarios, null, a.dt_prox_geracao + (nr_horas_copia_w/24), decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C')))
group by nm_usuario_lib_enf, cd_farmac_lib, cd_setor_atendimento, ie_motivo_prescricao, dt_prox_geracao, is_antecipated_item
order by is_antecipated_item desc, cd_setor_atendimento, nm_usuario_lib_enf, cd_farmac_lib, dt_prim_horario, ie_motivo_prescricao;


procedure adjust_properties_nls is
	begin
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_LANGUAGE=''BRAZILIAN PORTUGUESE''';
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_TERRITORY = ''BRAZIL''';
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';
	end;
	
procedure copia_verif_intera_med is
	ds_item_prescricao_ww			varchar2(4000);
	nr_sequencia_w					number(10);
	begin
	ds_item_prescricao_ww := replace(ds_item_prescricao_w, ' ', '');
	
	for i in 1..length(ds_item_prescricao_ww) loop
		nr_sequencia_w := substr(ds_item_prescricao_ww, instr(ds_item_prescricao_ww,',') + 1, instr(ds_item_prescricao_ww,';') - 3);		
		ds_item_prescricao_ww := substr(ds_item_prescricao_ww, instr(ds_item_prescricao_ww , ';') + 1, length(ds_item_prescricao_ww));
		cpoe_copia_verif_intera_med(nr_sequencia_w,nr_atendimento_p,cd_perfil_p,cd_estabelecimento_p,nm_usuario_p);
	end loop;
		
	exception when others then	
		ds_exception_w := substr(to_char(sqlerrm),1,2000);
		
		gravar_log_cpoe(substr('CPOE_COPIA_VERIF_INTERA_MED EXCEPTION: ' || ds_exception_w || 
				' ds_item_prescricao_ww: ' || ds_item_prescricao_ww ||
				' nr_sequencia_w: ' || nr_sequencia_w ||
				' cd_perfil_p: ' || cd_perfil_p,1,2000),
				nr_atendimento_p);
	end;
		
begin

gravar_log_cpoe('CPOE_COPIAR_ITENS - DT_REFERENCIA_P: '||to_char(dt_referencia_p,'dd/mm/yyyy hh24:mi:ss'), nr_atendimento_p);

adjust_properties_nls;
Obter_param_Usuario(3111, 162, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_susp_prescricoes_alta_w);

  begin
      sql_w := 'CALL GET_QT_HOURS_AFTER_COPY_CPOE(:1, :2, :3) INTO :nr_horas_copia_w';
      EXECUTE IMMEDIATE sql_w USING IN cd_perfil_p,
                                    IN nm_usuario_p,
                                    IN cd_estabelecimento_p,
                                    OUT nr_horas_copia_w;

  exception
    when others then
      nr_horas_copia_w := null;
  end;
  
  begin
      sql_w := 'CALL GET_QT_HOURS_BEFORE_COPY(:1, :2, :3) INTO :qt_hors_before_w';
      EXECUTE IMMEDIATE sql_w USING IN cd_perfil_p,
                                    IN nm_usuario_p,
                                    IN cd_estabelecimento_p,
                                    OUT qt_hors_before_w;
       
  exception
    when others then
      qt_hors_before_w := null;
  end;
  
  begin
      sql_w := 'CALL OBTER_DATA_REF_COPIA_CPOE_MD(:1, :2, :3) INTO :dt_referencia_w';
      EXECUTE IMMEDIATE sql_w USING IN dt_referencia_p,
                                    IN qt_hors_before_w,
                                    IN nr_horas_copia_w,
                                    OUT dt_referencia_w;
       
  exception
    when others then
      dt_referencia_w := null;
  end;
  
  
  begin
      sql_w := 'CALL OBTER_HORA_REF_COPIA_CPOE_MD(:1, :2) INTO :dt_ref_copia_w';
	  EXECUTE IMMEDIATE sql_w USING IN dt_referencia_w,	 
                                    IN qt_hors_before_w,
                                    OUT dt_ref_copia_w;
       
  exception
    when others then
      dt_ref_copia_w := null;
  end;
  

cpoe_get_antecipated_batch(dt_ref_copia_w, cd_estabelecimento_p, cd_setor_atendimento_p, is_antecipated_batch_w, dt_inicio_regra_w, dt_fim_regra_w);


open c01;
loop
fetch c01 into
	ds_item_prescricao_w,
	nm_usuario_lib_enf_w,
	cd_farmac_lib_w,
	dt_prim_horario_w,
	cd_setor_atendimento_w,
	ie_motivo_prescricao_w,
	dt_prox_geracao_w,
	is_antecipated_item_w;
exit when c01%notfound;
	adjust_properties_nls;

	if (ds_item_prescricao_w is not null) then		
		copia_verif_intera_med();
		if (nm_usuario_lib_enf_w is null and cd_farmac_lib_w is null) then
			ie_copia_sem_lib_enf_farm_w := 'S';
		end if;
			
		if (nvl(is_antecipated_item_w, 'N') = 'S') then
			gerar_prescr_antecip(
						cd_setor_atendimento_w, 
						dt_prox_geracao_w, 
						dt_inicio_regra_w, 
						dt_fim_regra_w,
						dt_prox_geracao_w + nr_horas_copia_w/24,
						nr_atendimento_p, 
						cd_estabelecimento_p, 
						cd_perfil_p, 
						nm_usuario_p, 
						ds_item_prescricao_w, 
						ie_inconsistencia_out_w,
						cd_pessoa_fisica_p,
						'S', 
						'N', 
						ie_motivo_prescricao_w, 
						'N', 
						nr_prescricao_w, 
						null, 
						null, 
						null, 
						ie_copia_sem_lib_enf_farm_w, 
						ie_adep_p, 
						null, 
						ds_prescricoes_geradas_w, 
						nm_usuario_lib_enf_w, 
						cd_farmac_lib_w);
		else
			cpoe_gerar_prescricao(
							nr_atendimento_p, 
							dt_prim_horario_w, 
							cd_estabelecimento_p, 
							cd_perfil_p, 
							cd_setor_atendimento_w, 
							nm_usuario_p, 
							ds_item_prescricao_w, 
							IE_INCONSISTENCIA_OUT_w, 
							cd_pessoa_fisica_p, 
							'S',
							'N',
							ie_motivo_prescricao_w,
							'N',
							nr_prescricao_w, 
							null, 
							null, 
							null, 
							ie_copia_sem_lib_enf_farm_w, 
							ie_adep_p, 
							null, 
							ds_prescricoes_geradas_w, 
							nm_usuario_lib_enf_w, 
							cd_farmac_lib_w);
			
			ie_copia_sem_lib_enf_farm_w := 'N';
			
			 
		end if;
		
	end if;
	
end loop;

end cpoe_copiar_itens;
/