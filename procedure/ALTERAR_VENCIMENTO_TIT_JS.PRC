create or replace
procedure alterar_vencimento_tit_js(	nr_titulo_p	in	number,
				dt_vencimento_p	in	date,
				cd_motivo_p	in	number,
				ds_observacao_p	in	varchar2,
				nm_usuario_p	in	varchar2) is

begin

alterar_vencimento_tit_rec(nr_titulo_p, dt_vencimento_p, cd_motivo_p, ds_observacao_p, nm_usuario_p);

gerar_bloqueto_tit_rec(nr_titulo_p, 'MTR');

end alterar_vencimento_tit_js;
/