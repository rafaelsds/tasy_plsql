create or replace
procedure pls_atualizar_eventos_fatura(	nr_seq_fatura_p		number,
					ie_atualiza_vl_p	varchar2,
					ie_commit_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_lote_w			number(10);
nr_seq_regra_fat_w		number(10);
nr_seq_fatura_evento_w		number(10);
nr_seq_fatura_conta_w		number(10);
nr_seq_fatura_item_w		number(10);
nr_seq_conta_proc_w		number(10);
nr_seq_conta_mat_w		number(10);
dt_item_w			date;
nr_seq_conta_w			number(10);
nr_seq_conta_pos_estab_w	number(10);
nr_seq_evento_fat_w		number(10);
nr_seq_segurado_w		number(10);
ie_tipo_cobranca_w		pls_fatura_proc.ie_tipo_cobranca%type;

nr_seq_evento_novo_w		number(10);
nr_seq_regra_event_w		number(10);
ie_tipo_vinculacao_w		varchar2(1);
vl_faturado_w			number(15,2);
vl_faturado_ndc_w		number(15,2);
qt_contestacao_w		number(10) := 0;
nr_seq_cong_w			number(10);
nr_seq_conta_pos_contab_w	pls_fatura_proc.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		nr_seq_evento
	from	pls_fatura_evento
	where	nr_seq_fatura = nr_seq_fatura_p;
	
Cursor C02 is
	select	nr_sequencia,
		nr_seq_conta,
		nr_seq_segurado,
		ie_tipo_vinculacao
	from	pls_fatura_conta
	where	nr_seq_fatura_evento	= nr_seq_fatura_evento_w;

Cursor C03 is
	select	nr_sequencia,
		nr_seq_conta_pos_estab,
		nr_seq_conta_proc,
		null,
		vl_faturado,
		vl_faturado_ndc,
		nr_seq_conta_pos_contab,
		ie_tipo_cobranca
	from	pls_fatura_proc
	where	nr_seq_fatura_conta = nr_seq_fatura_conta_w
	union all
	select	nr_sequencia,
		nr_seq_conta_pos_estab,
		null,
		nr_seq_conta_mat,
		vl_faturado,
		vl_faturado_ndc,
		nr_seq_conta_pos_contab,
		ie_tipo_cobranca
	from	pls_fatura_mat
	where	nr_seq_fatura_conta = nr_seq_fatura_conta_w;
	
Cursor C04 is
	select	nr_seq_conta_pos_estab,
		nr_seq_evento,
		nr_seq_conta,
		nr_seq_conta_proc,
		nr_seq_conta_mat,
		nr_seq_segurado,
		ie_tipo_vinculacao_conta,
		nvl(vl_faturado,0),
		nvl(vl_faturado_ndc,0),
		nr_seq_conta_pos_contab,
		ie_tipo_cobranca
	from	w_pls_fat_proc_mat
	where	nr_seq_fatura = nr_seq_fatura_p
	order by nr_seq_evento,
		nr_seq_conta;
	
begin
delete	w_pls_fat_proc_mat
where	nr_seq_fatura	= nr_seq_fatura_p
and	nm_usuario	= nm_usuario_p;

select	max(c.nr_seq_regra_fat),
	max(c.nr_sequencia)
into	nr_seq_regra_fat_w,
	nr_seq_lote_w
from	pls_lote_faturamento	c,
	pls_fatura		a
where	a.nr_seq_lote		= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_fatura_p;

-- EVENTOS
open C01;
loop
fetch C01 into	
	nr_seq_fatura_evento_w,
	nr_seq_evento_fat_w;
exit when C01%notfound;
	begin
	
	-- CONTAS
	open C02;
	loop
	fetch C02 into	
		nr_seq_fatura_conta_w,
		nr_seq_conta_w,
		nr_seq_segurado_w,
		ie_tipo_vinculacao_w;
	exit when C02%notfound;
		begin
		-- PROCEDIMENTOS / MATERIAIS
		open C03;
		loop
		fetch C03 into	
			nr_seq_fatura_item_w,
			nr_seq_conta_pos_estab_w,
			nr_seq_conta_proc_w,
			nr_seq_conta_mat_w,
			vl_faturado_w,
			vl_faturado_ndc_w,
			nr_seq_conta_pos_contab_w,
			ie_tipo_cobranca_w;
		exit when C03%notfound;
			begin	
			qt_contestacao_w := 0;
			
			if	(nr_seq_conta_proc_w is not null) then
				select	count(1)
				into	qt_contestacao_w
				from	pls_contestacao_proc
				where	nr_seq_fatura_proc = nr_seq_fatura_item_w;
			
				select	max(a.dt_procedimento)
				into	dt_item_w
				from	pls_conta_proc_v a
				where	a.nr_sequencia	= nr_seq_conta_proc_w;
				
			elsif	(nr_seq_conta_mat_w is not null) then
				select	count(1)
				into	qt_contestacao_w
				from	pls_contestacao_mat
				where	nr_seq_fatura_mat = nr_seq_fatura_item_w;
			
				select	max(a.dt_atendimento)
				into	dt_item_w
				from	pls_conta_mat_v a
				where	a.nr_sequencia	= nr_seq_conta_mat_w;
			end if;
			
			if	(qt_contestacao_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(274548);
			end if;
			
			pls_obter_evento_faturamento( nr_seq_conta_proc_w, nr_seq_conta_mat_w, dt_item_w, nr_seq_evento_novo_w, nr_seq_regra_fat_w, nr_seq_conta_pos_contab_w, nr_seq_conta_pos_estab_w);	

			select	nvl(max(a.nr_seq_regra), 0)
			into	nr_seq_regra_event_w
			from	pls_regra_fat_evento	a
			where	a.nr_seq_evento = nr_seq_evento_novo_w
			and 	a.nr_seq_regra	= nr_seq_regra_fat_w;		

			if	(nr_seq_regra_event_w = nr_seq_regra_fat_w) then
				nr_seq_evento_fat_w := nr_seq_evento_novo_w;
			end if;
			
			insert into w_pls_fat_proc_mat
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_conta_pos_estab,
				nr_seq_evento,
				nr_seq_fatura,
				nr_seq_conta,
				nr_seq_conta_proc,
				nr_seq_conta_mat,
				nr_seq_segurado,
				ie_tipo_vinculacao_conta,
				vl_faturado,
				vl_faturado_ndc,
				nr_seq_conta_pos_contab,
				ie_tipo_cobranca)
			values	(w_pls_fat_proc_mat_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_conta_pos_estab_w,
				nr_seq_evento_fat_w,
				nr_seq_fatura_p,
				nr_seq_conta_w,
				nr_seq_conta_proc_w,
				nr_seq_conta_mat_w,
				nr_seq_segurado_w,
				ie_tipo_vinculacao_w,
				vl_faturado_w,
				vl_faturado_ndc_w,
				nr_seq_conta_pos_contab_w,
				ie_tipo_cobranca_w);
				
			if	(nr_seq_conta_proc_w is not null) then
				delete	pls_fatura_proc
				where	nr_sequencia = nr_seq_fatura_item_w;
				
			elsif	(nr_seq_conta_mat_w is not null) then
				delete	pls_fatura_mat
				where	nr_sequencia = nr_seq_fatura_item_w;
			end if;			
			end;
		end loop;
		close C03;
		
		delete	pls_fatura_conta
		where	nr_sequencia = nr_seq_fatura_conta_w;
				
		end;
	end loop;
	close C02;
	
	delete	pls_fatura_evento
	where	nr_sequencia = nr_seq_fatura_evento_w;
	
	end;
end loop;
close C01;

nr_seq_conta_pos_estab_w := null;
nr_seq_evento_fat_w := null;
nr_seq_conta_w := null;
nr_seq_conta_proc_w := null;
nr_seq_conta_mat_w := null;
dt_item_w := null;
nr_seq_fatura_evento_w := null;
nr_seq_fatura_conta_w := null;
nr_seq_segurado_w := null;
ie_tipo_vinculacao_w := null;

-- ATUALIZAR AS INFORMA��ES
open C04;
loop
fetch C04 into	
	nr_seq_conta_pos_estab_w,
	nr_seq_evento_fat_w,
	nr_seq_conta_w,
	nr_seq_conta_proc_w,
	nr_seq_conta_mat_w,
	nr_seq_segurado_w,
	ie_tipo_vinculacao_w,
	vl_faturado_w,
	vl_faturado_ndc_w,
	nr_seq_conta_pos_contab_w,
	ie_tipo_cobranca_w;
exit when C04%notfound;
	begin
	select	max(nr_sequencia)
	into	nr_seq_fatura_evento_w
	from	pls_fatura_evento
	where	nr_seq_evento	= nr_seq_evento_fat_w
	and	nr_seq_fatura	= nr_seq_fatura_p;
	
	if	(nr_seq_fatura_evento_w is null) then
		select	pls_fatura_evento_seq.nextval
		into	nr_seq_fatura_evento_w
		from	dual;
	
		insert into pls_fatura_evento
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_fatura,
			nr_seq_evento,
			vl_evento)
		values	(nr_seq_fatura_evento_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_fatura_p,
			nr_seq_evento_fat_w,
			0);
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_fatura_conta_w
	from	pls_fatura_conta
	where	nr_seq_fatura_evento	= nr_seq_fatura_evento_w
	and	nr_seq_conta		= nr_seq_conta_w;
	
	if	(nr_seq_fatura_conta_w is null) then
		select	pls_fatura_conta_seq.nextval
		into	nr_seq_fatura_conta_w
		from	dual;
		
		select	max(a.nr_seq_congenere)
		into	nr_seq_cong_w
		from	pls_segurado	a
		where	a.nr_sequencia	= nr_seq_segurado_w;
	
		insert into pls_fatura_conta
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_fatura_evento,
			nr_seq_conta,
			vl_faturado,
			nr_seq_segurado,
			ie_tipo_cobranca,
			cd_conta_debito,
			cd_conta_credito,
			nr_lote_contabil,
			vl_faturado_ndc,
			ie_tipo_vinculacao,
			nr_seq_congenere)
		values(	nr_seq_fatura_conta_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_fatura_evento_w,
			nr_seq_conta_w,
			0,
			nr_seq_segurado_w,			
			null,
			null,
			null,
			0,
			0,
			ie_tipo_vinculacao_w,
			nr_seq_cong_w);
	end if;
	
	if	(nr_seq_conta_proc_w is not null) then
		insert into pls_fatura_proc (	
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_fatura_conta,
			nr_seq_conta_proc,
			vl_faturado,
			cd_conta_debito,
			cd_conta_credito,
			nr_lote_contabil,
			cd_historico,
			nr_seq_esquema,
			cd_classif_cred,
			cd_classif_deb,
			cd_historico_baixa,
			cd_historico_estorno,
			ie_tipo_cobranca,
			ie_liberado,
			nr_seq_conta_pos_estab,
			nr_seq_fat_proc_cancel,
			vl_faturado_ndc,
			cd_conta_debito_ndc,
			cd_conta_credito_ndc,
			cd_classif_cred_ndc,
			cd_classif_deb_ndc,
			nr_seq_esquema_ndc,
			cd_historico_ndc,
			nr_seq_conta_pos_contab)
		values(	pls_fatura_proc_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_fatura_conta_w,
			nr_seq_conta_proc_w,
			vl_faturado_w,
			null,
			null,
			0,
			null,
			null,
			null,
			null,
			null,
			null,
			ie_tipo_cobranca_w,
			null,
			nr_seq_conta_pos_estab_w,
			null,
			vl_faturado_ndc_w,
			null,
			null,
			null,
			null,
			null,
			null,
			nr_seq_conta_pos_contab_w);
	
	elsif	(nr_seq_conta_mat_w is not null) then
		insert into pls_fatura_mat (	
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_fatura_conta,
			nr_seq_conta_mat,
			vl_faturado,
			cd_conta_debito,
			cd_conta_credito,
			nr_lote_contabil,
			cd_historico,
			nr_seq_esquema,
			cd_classif_cred,
			cd_classif_deb,
			cd_historico_baixa,
			cd_historico_estorno,
			ie_tipo_cobranca,
			ie_liberado,
			nr_seq_conta_pos_estab,
			nr_seq_fat_mat_cancel,
			vl_faturado_ndc,
			cd_conta_debito_ndc,
			cd_conta_credito_ndc,
			cd_classif_cred_ndc,
			cd_classif_deb_ndc,
			nr_seq_esquema_ndc,
			cd_historico_ndc,
			nr_seq_conta_pos_contab)
		values(	pls_fatura_mat_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_fatura_conta_w,
			nr_seq_conta_mat_w,
			vl_faturado_w,
			null,
			null,
			0,
			null,
			null,
			null,
			null,
			null,
			null,
			ie_tipo_cobranca_w,
			null,
			nr_seq_conta_pos_estab_w,
			null,
			vl_faturado_ndc_w,
			null,
			null,
			null,
			null,
			null,
			null,
			nr_seq_conta_pos_contab_w);

	end if;
	
	end;
end loop;
close C04;

-- LIMPAR FATURAS E EVENTOS VAZIOS
pls_limpar_faturas_vazias_lote( nr_seq_lote_w, cd_estabelecimento_p, nm_usuario_p);

-- ATUALIZAR VALORES
if	(nvl(ie_atualiza_vl_p,'N') = 'S') then
	pls_atualizar_vl_lote_fatura( nr_seq_lote_w, nm_usuario_p, 'N', 'N');
end if;

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;

end pls_atualizar_eventos_fatura;
/