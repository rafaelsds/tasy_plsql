create or replace
procedure pls_salvar_anex_alt_benef_web
			(	cd_pessoa_fisica_p		varchar2,	
				nr_seq_tipo_documento_p		number,
				ds_arquivo_p			varchar2,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number) is 
				
nr_seq_pls_tipo_doc_w		number(10);
nr_seq_segurado_w		number(10);

Cursor C01 is
	select  nr_sequencia
	from	pls_segurado 
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
				
begin
if	(cd_pessoa_fisica_p is not null and nr_seq_tipo_documento_p is not null) then

	select	pls_tipo_documento_pf_seq.nextval
	into	nr_seq_pls_tipo_doc_w
	from	dual;

	insert into pls_tipo_documento_pf(nr_sequencia, dt_atualizacao, nm_usuario,           
				          dt_atualizacao_nrec,nm_usuario_nrec,dt_registro, 
					  cd_pessoa_fisica, cd_estabelecimento, nr_seq_tipo_documento)     
				   values(nr_seq_pls_tipo_doc_w, sysdate, nm_usuario_p,            
					  sysdate, nm_usuario_p, sysdate,cd_pessoa_fisica_p, 
					  cd_estabelecimento_p, nr_seq_tipo_documento_p);    
	
	insert into pls_documento_pf(nr_sequencia, dt_atualizacao, nm_usuario,           
				          dt_atualizacao_nrec,nm_usuario_nrec,cd_pessoa_fisica, 
					  nr_seq_tipo_documento, ds_arquivo, nr_seq_tipo_doc_pf)     
				   values(pls_documento_pf_seq.nextval, sysdate, nm_usuario_p,            
					  sysdate, nm_usuario_p, cd_pessoa_fisica_p,        
					  nr_seq_tipo_documento_p, ds_arquivo_p, nr_seq_pls_tipo_doc_w);
					  
	
	open C01;
	loop
	fetch C01 into
		nr_seq_segurado_w;
	exit when C01%notfound;
		begin
			insert into pls_segurado_doc_arq
				(nr_sequencia, nr_seq_segurado, cd_estabelecimento, dt_atualizacao,
				 nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ds_arquivo,
				 ie_origem_anexo, nr_seq_tipo_documento, ie_apresenta_portal
				)
			values	(pls_segurado_doc_arq_seq.nextval, nr_seq_segurado_w, cd_estabelecimento_p, sysdate,
				 nm_usuario_p, sysdate, nm_usuario_p, ds_arquivo_p,
				 'P', null, 'S'
				);
		end;
	end loop;
	close C01;
					  
	commit;				  				  
end if;

end pls_salvar_anex_alt_benef_web;
/
