create or replace
procedure SMA_GERAR_COBRANCA_ITAU_400(nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is 
	
ds_conteudo_w		varchar2(400)	:= null;
nr_seq_reg_arquivo_w	number(10)	:= 1;
cd_conta_cobr_w		varchar2(5);
cd_agencia_cobr_w	varchar2(4);
nm_empresa_w		varchar2(30);
dt_geracao_w		varchar2(6);
ie_digito_conta_cobr_w	varchar2(1);
cd_banco_w		number(10);
nr_seq_carteira_cobr_w	number(10);
nr_carteira_w		varchar2(3);

/* detalhe */
ie_tipo_inscricao_w	varchar2(2);
nr_inscricao_empresa_w	varchar2(14);
cd_agencia_bancaria_w	varchar2(4);
cd_conta_w		varchar2(6);
nr_nosso_numero_w	varchar2(8);
vl_multa_w		number(15,2);
nr_titulo_w		number(10);
dt_vencimento_w		date;
vl_titulo_w		number(15,2);
dt_emissao_w		date;
vl_juros_w		number(15,2);
vl_desconto_w		number(15,2);
nm_pessoa_w		varchar2(30);
ds_endereco_w		varchar2(40);
ds_bairro_w		varchar2(12);
cd_cep_w		varchar2(8);
ds_cidade_w		varchar2(15);
sg_estado_w		varchar2(15);
tx_juros_w		number(7,4);
tx_multa_w		number(7,4);
ds_tipo_juros_w		varchar2(255);
ds_tipo_multa_w		varchar2(255);
ie_digito_nosso_num_w	varchar2(1);
ds_instrucao_um_w	varchar2(128);
ds_instrucao_dois_w	varchar2(128);
ds_instrucao_tres_w	varchar2(127);
ds_instrucao_quatro_w	varchar2(128);
ds_instrucao_cinco_w	varchar2(128);
nr_telefone_w		varchar2(15);

cursor	c01 is
SELECT	DECODE(b.cd_pessoa_fisica,NULL,'02','01') ie_tipo_inscricao,
	NVL(c.nr_cpf,b.cd_cgc) nr_inscricao_empresa,
	LPAD(a.cd_agencia_bancaria,4,'0') cd_agencia_bancaria,
	LPAD(SUBSTR(a.nr_conta,1,5) || SUBSTR(a.ie_digito_conta,1,1),6,'0') cd_conta,
	LPAD(SUBSTR(NVL(b.nr_nosso_numero,'0'),1,8),8,'0') nr_nosso_numero,
	DECODE(NVL(a.vl_multa,0),0,obter_juros_multa_titulo(b.nr_titulo,SYSDATE,'R','M'),a.vl_multa),
	a.nr_titulo,
	b.dt_pagamento_previsto,
	b.vl_saldo_titulo,
	b.dt_emissao,
	DECODE(NVL(a.vl_juros,0),0,obter_juros_multa_titulo(b.nr_titulo,SYSDATE,'R','J'),a.vl_juros),
	a.vl_desconto,
	SUBSTR(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,30) nm_pessoa,
	--rpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'R'),1,40),' '),40,' ') ds_endereco,
	DECODE(b.cd_cgc, NULL, 
			 RPAD(NVL(DECODE(  UPPER(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'EN'),1,4)),
													UPPER('AVEN'),SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'EN'),1,2)||'.'|| SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'EN'),8,19)||','||NVL(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'NR'),1,4),' ')||','||NVL(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'CO'),1,12),' '),
												        UPPER('RUA '), SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'EN'),1,1)||'.'|| SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'EN'),5,19)||','||NVL(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'NR'),1,4),' ')||','||NVL(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'CO'),1,12),' '),
													SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'EN'),1,28)||','||NVL(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'NR'),1,4),' ')||','||NVL(SUBSTR(obter_compl_pf(b.cd_pessoa_fisica,1,'CO'),1,6),' ')),' '),40,' '), 
			 RPAD(NVL(SUBSTR(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'R'),1,40),' ')||','||NVL(SUBSTR(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'NR'),1,40),' '),40,' ') 
	        ) ds_endereco,
	RPAD(NVL(SUBSTR(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),1,12),' '),12,' ') ds_bairro,
	LPAD(NVL(SUBSTR(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CEP'),1,8),'0'),8,'0') cd_cep,
	RPAD(NVL(SUBSTR(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CI'),1,15),' '),15,' ') ds_cidade,
	RPAD(NVL(SUBSTR(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'UF'),1,2),' '),2,' ') sg_estado,
	b.tx_juros,
	b.tx_multa,
	SUBSTR(obter_valor_dominio(707,b.cd_tipo_taxa_juro),1,255) ds_tipo_juros,
	SUBSTR(obter_valor_dominio(707,b.cd_tipo_taxa_multa),1,255) ds_tipo_multa
FROM	pessoa_fisica c,
	titulo_receber b,
	titulo_receber_cobr a
WHERE	b.cd_pessoa_fisica	= c.cd_pessoa_fisica(+)
AND	a.nr_titulo		= b.nr_titulo
and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

begin

delete	w_envio_banco
where	nm_usuario = nm_usuario_p;

/* header */
select	lpad(nvl(b.cd_conta,'0'),5,'0'),
	lpad(nvl(b.cd_agencia_bancaria,'0'),4,'0'),
	substr(nvl(b.ie_digito_conta,'0'),1,1),
	rpad(substr(obter_nome_estabelecimento(cd_estabelecimento_p),1,30),30,' ') nm_empresa,
	to_char(sysdate,'DDMMYY'),
	a.nr_seq_carteira_cobr,
	b.cd_banco
into	cd_conta_cobr_w,
	cd_agencia_cobr_w,
	ie_digito_conta_cobr_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_carteira_cobr_w,
	cd_banco_w
from	banco_estabelecimento b,
	cobranca_escritural a
where	a.nr_seq_conta_banco	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	lpad(nvl(substr(max(a.cd_carteira),1,3),'0'),3,'0') nr_carteira
into	nr_carteira_w
from	banco_carteira a
where	a.nr_sequencia 	= nr_seq_carteira_cobr_w
and	a.cd_banco	= cd_banco_w;

select	max(b.nr_telefone)
into	nr_telefone_w
from	pessoa_juridica b,
	estabelecimento a
where	a.cd_cgc		= b.cd_cgc
and	a.cd_estabelecimento	= cd_estabelecimento_p;

ds_conteudo_w	:= 	'0' ||
			'1' ||
			'REMESSA' ||
			'01' ||
			rpad('COBRANCA',15,' ') ||
			cd_agencia_cobr_w ||
			'00' ||
			cd_conta_cobr_w ||
			ie_digito_conta_cobr_w ||
			rpad(' ',8,' ') ||
			nm_empresa_w ||
			lpad(cd_banco_w,3,'0') ||
			rpad('BANCO ITAU SA',15,' ') ||
			dt_geracao_w ||
			rpad(' ',294,' ') ||
			lpad(nr_seq_reg_arquivo_w,6,'0');

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_reg_arquivo_w);

/* detalhe */
open	C01;
loop
fetch	C01 into	
	ie_tipo_inscricao_w,
	nr_inscricao_empresa_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	nr_nosso_numero_w,
	vl_multa_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_w,
	vl_desconto_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	sg_estado_w,
	tx_juros_w,
	tx_multa_w,
	ds_tipo_juros_w,
	ds_tipo_multa_w;
exit	when C01%notfound;

	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

	/* Registro Detalhe (obrigatório) */
	ds_conteudo_w	:=	'1' ||
				ie_tipo_inscricao_w ||
				lpad(nvl(nr_inscricao_empresa_w,'0'),14,'0') ||
				cd_agencia_bancaria_w ||
				'00' ||
				cd_conta_w ||
				rpad(' ',4,' ') ||
				'0000' ||
				rpad(nr_titulo_w,25,' ') ||
				nr_nosso_numero_w ||
				'0000000000000' ||
				nr_carteira_w ||
				rpad(' ',21,' ') ||
				'I' ||
				'01' ||
				rpad(' ',10,' ') ||
				to_char(dt_vencimento_w,'ddmmyy') ||
				lpad(somente_numero(to_char(nvl(vl_titulo_w,0),'99999999990.00')),13,'0') ||
				lpad(cd_banco_w,3,'0') ||
				'00000' ||
				'99' ||
				'A' ||
				to_char(dt_emissao_w,'ddmmyy') ||
				'0505' ||
				lpad(somente_numero(to_char(nvl(vl_juros_w,0),'99999999990.00')),13,'0') ||
				to_char(dt_vencimento_w,'ddmmyy') ||
				lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'99999999990.00')),13,'0') ||
				lpad('0',26,'0') ||
				ie_tipo_inscricao_w ||
				lpad(nvl(nr_inscricao_empresa_w,'0'),14,'0') ||
				rpad(nm_pessoa_w,40,' ') ||
				ds_endereco_w ||
				ds_bairro_w ||
				cd_cep_w ||
				ds_cidade_w ||
				substr(sg_estado_w,1,2) ||
				rpad(nm_empresa_w,30,' ') ||
				rpad(' ',4,' ') ||
				'000000' ||
				'00' ||
				' ' ||
				lpad(nr_seq_reg_arquivo_w,6,'0');

	insert	into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_reg_arquivo_w);

	select	substr(obter_instrucao_boleto(nr_titulo_w,cd_banco_w,1),1,255) ds_instrucao_um,
		substr(obter_instrucao_boleto(nr_titulo_w,cd_banco_w,2),1,255) ds_instrucao_dois,
		substr(obter_instrucao_boleto(nr_titulo_w,cd_banco_w,3),1,255) ds_instrucao_tres,
		substr(obter_instrucao_boleto(nr_titulo_w,cd_banco_w,4),1,255) ds_instrucao_quatro,
		substr(obter_instrucao_boleto(nr_titulo_w,cd_banco_w,5),1,255) ds_instrucao_cinco
	into	ds_instrucao_um_w,
		ds_instrucao_dois_w,
		ds_instrucao_tres_w,
		ds_instrucao_quatro_w,
		ds_instrucao_cinco_w
	from	dual;

	/* Registro mensagem frente (obrigatório) */
	if	(ds_instrucao_um_w	is not null) or
		(ds_instrucao_dois_w	is not null) or
		(ds_instrucao_tres_w	is not null) then

		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

		ds_conteudo_w	:=	'7' ||
					'LWZ' ||
					'01' ||
					rpad(nvl(ds_instrucao_um_w,' '),128,' ') ||
					'02' ||
					rpad(nvl(ds_instrucao_dois_w,' '),128,' ') ||
					'03' ||
					rpad(nvl(ds_instrucao_tres_w,' '),127,' ') ||
					'0' ||
					lpad(nr_seq_reg_arquivo_w,6,'0');

		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);

		if	(ds_instrucao_quatro_w	is not null) or
			(ds_instrucao_cinco_w	is not null) then

			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

			ds_conteudo_w	:=	'7' ||
						'LWZ' ||
						'04' ||
						rpad(nvl(ds_instrucao_quatro_w,' '),128,' ') ||
						'05' ||
						rpad(nvl(ds_instrucao_cinco_w,' '),128,' ') ||
						'06' ||
						rpad(' ',127,' ') ||
						'0' ||
						lpad(nr_seq_reg_arquivo_w,6,'0');

			insert	into w_envio_banco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
			values	(w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_reg_arquivo_w);

		end if;

	end if;

end	loop;
close	C01;

/* trailer */
nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

ds_conteudo_w	:= 	'9' ||
			rpad(' ',393,' ') ||
			lpad(nr_seq_reg_arquivo_w,6,'0');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_reg_arquivo_w);

commit;

end SMA_GERAR_COBRANCA_ITAU_400;
/