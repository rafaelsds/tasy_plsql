create or replace
procedure pls_devolver_notificacao
			(	nr_seq_notificacao_p	number,
				nr_seq_motivo_p		number,
				dt_entrega_p		date,
				nm_entregador_p		varchar2,
				ie_tipo_notificacao_p	varchar2,
				ie_atualiza_notif_pag_p varchar2,
				nm_usuario_p		varchar2) is

ie_status_w			pls_notificacao_pagador.ie_status%type;

begin
insert into pls_notificacao_devolucao
	(nr_sequencia,
	nr_seq_notificacao,
	dt_devolucao,
	dt_entrega,
	nr_seq_tipo_devolucao,
	nm_entregador,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_tipo_notificacao)
values	(pls_notificacao_devolucao_seq.nextval,
	nr_seq_notificacao_p,
	sysdate,
	dt_entrega_p,
	nr_seq_motivo_p,
	nm_entregador_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ie_tipo_notificacao_p);

select	ie_status
into 	ie_status_w
from 	pls_notificacao_pagador
where	nr_sequencia	= nr_seq_notificacao_p;

if	(ie_atualiza_notif_pag_p = 'S') and
	(ie_status_w <> 'D') then
	update	pls_notificacao_pagador
	set	ie_status_ant 	= ie_status,
		ie_status	= 'D',
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_notificacao_p;
end if;

--Utilizado no HTML5
pls_excluir_barras_pag_notific(nr_seq_notificacao_p,nm_usuario_p);

commit;

end pls_devolver_notificacao;
/
