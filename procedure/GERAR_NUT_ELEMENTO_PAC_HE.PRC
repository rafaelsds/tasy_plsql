create or replace
procedure Gerar_Nut_Elemento_Pac_HE(	
					nr_sequencia_p     	number,
					nm_usuario_p        varchar2) is


nr_seq_elemento_w		number(10,0);
nr_sequencia_w			number(10,0);
nr_sequencia_ww			number(10,0);
nr_sequencia_www		number(10,0);
cd_unidade_medida_w		varchar2(30);
qt_fase_npt_w			number(15,0);
nr_seq_glicose_w		number(15,0);
nr_seq_padrao_w			number(15,0);
qt_dose_padrao_w		number(18,6);
qt_fase_w				number(15,1);
ie_processo_hidrico_w	varchar2(15);
ie_tipo_elemento_w		varchar2(1);

cursor C01 is
	select	nr_sequencia,
			cd_unidade_medida,
			ie_tipo_elemento
	from 	nut_elemento
	where	ie_situacao	= 'A'
	and		ie_rep_he	= 'S'
	and		cd_unidade_medida is not null
	order by nr_seq_apresent, ds_elemento;

cursor C02 is
	select	nr_sequencia
	from 	nut_elem_material
	where	ie_situacao	= 'A'
	and		ie_padrao	= 'S'
	and		nr_seq_elemento	= nr_seq_elemento_w
	and		nvl(ie_tipo,'NPT') = 'NPT'
	order by cd_material;

cursor c04 is
	select	nr_sequencia,
			qt_dose_padrao
	from	nut_elemento
	where	ie_rep_he 	= 'S'
	and		ie_situacao	= 'A'
	and		nr_sequencia	= nr_seq_elemento_w
	and		nvl(qt_dose_padrao,0) > 0;	

begin

select	nvl(max(ie_processo_hidrico),'N')
into	ie_processo_hidrico_w
from	prescr_rep_he
where	nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01 into
		nr_seq_elemento_w,
		cd_unidade_medida_w,
		ie_tipo_elemento_w;
exit when C01%notfound;
	begin
	
	if ((ie_tipo_elemento_w = 'A') and
	    (ie_processo_hidrico_w = 'S')) or
	   (ie_tipo_elemento_w <> 'A') then
		select	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from	prescr_rep_he_elem
		where	nr_seq_rep_he	= nr_sequencia_p
		and		nr_seq_elemento	= nr_seq_elemento_w;

		if	(nr_sequencia_w = 0) then
			begin
			select	prescr_rep_he_elem_seq.nextval
			into	nr_sequencia_w
			from 	dual;

			insert into prescr_rep_he_elem(
				nr_sequencia,
				nr_seq_rep_he,
				nr_seq_elemento,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				qt_elem_kg_dia,
				qt_diaria,
				qt_volume,
				qt_volume_corrigido,
				qt_volume_etapa,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(
				nr_sequencia_w,
				nr_sequencia_p,
				nr_seq_elemento_w,
				sysdate,
				nm_usuario_p,
				cd_unidade_medida_w,
				0,
				0,
				0,
				0,
				0,
				sysdate,
				nm_usuario_p);

			open C02;
			loop
			fetch C02 into
					nr_sequencia_www;
			exit when C02%notfound;
				select	prescr_rep_he_elem_mat_seq.nextVal
				into	nr_sequencia_ww
				from	dual;

				insert into prescr_rep_he_elem_mat(
					nr_sequencia,
					nr_seq_ele_rep,
					dt_atualizacao,
					nm_usuario,
					nr_seq_elem_mat,
					qt_volume,
					qt_vol_cor,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(	nr_sequencia_ww,
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					nr_sequencia_www,
					null,
					null,
					sysdate,
					nm_usuario_p);
			end loop;
			close C02;
			end;	
			
			if (ie_tipo_elemento_w = 'C') then
				Atualizar_rep_he_elem_mat(nr_sequencia_w,nm_usuario_p,ie_processo_hidrico_w);
			end if;
			
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_glicose_w
			from	nut_elemento
			where	ie_tipo_elemento = 'C';

			open C04;
			loop
			fetch C04 into	
					nr_seq_padrao_w,
					qt_dose_padrao_w;
			exit when C04%notfound;
				update	prescr_rep_he_elem a
				set		qt_elem_kg_dia		= qt_dose_padrao_w
				where	nr_seq_rep_he		= nr_sequencia_p
				and		nr_seq_elemento		<> nr_seq_glicose_w
				and		nr_seq_elemento		= nr_seq_padrao_w
				and		qt_elem_kg_dia		= 0;

			end loop;
			close C04;
		end if;
	end if;
	end;

end Loop;
close C01;
commit;

end Gerar_Nut_Elemento_Pac_HE;
/