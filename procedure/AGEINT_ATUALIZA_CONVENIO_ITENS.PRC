create or replace
procedure Ageint_atualiza_convenio_itens (cd_convenio_p		number,
					 cd_categoria_p		varchar2,
					 cd_plano_p		varchar2,
					 nr_seq_agenda_int_p	number,
					 nm_usuario_p		Varchar2,
					 cd_estabelecimento_p	number) is 

nr_seq_agenda_exame_w	number(10,0);
nr_seq_agenda_cons_w	number(10,0);
					 
Cursor C02 is
	select	a.nr_seq_agenda_exame
	from 	agenda_integrada_item a
	where	a.nr_seq_agenda_exame is not null
	and		a.nr_seq_agenda_int	= nr_seq_agenda_int_p
	and	not exists (select 1 from agenda_integrada_conv_item b where b.nr_seq_agenda_item	= a.nr_sequencia);
	
Cursor C03 is
	select	a.nr_seq_agenda_cons
	from 	agenda_integrada_item a
	where	a.nr_seq_agenda_cons is not null
	and		a.nr_seq_agenda_int	= nr_seq_agenda_int_p
	and	not exists (select 1 from agenda_integrada_conv_item b where b.nr_seq_agenda_item	= a.nr_sequencia);
					 
begin

open C02;
loop
fetch C02 into	
	nr_seq_agenda_exame_w;
exit when C02%notfound;
	begin		
	update	agenda_paciente
	set	cd_convenio	= cd_convenio_p,
		cd_categoria	= cd_categoria_p,
		cd_plano	= cd_plano_p
	where	nr_sequencia	= nr_seq_agenda_exame_w;		
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_agenda_cons_w;
exit when C03%notfound;
	begin
	update	agenda_consulta
	set	cd_convenio	= cd_convenio_p,
		cd_categoria	= cd_categoria_p,
		cd_plano	= cd_plano_p
	where	nr_sequencia	= nr_seq_agenda_cons_w;		
	end;
end loop;
close C03;

commit;

end Ageint_atualiza_convenio_itens;
/