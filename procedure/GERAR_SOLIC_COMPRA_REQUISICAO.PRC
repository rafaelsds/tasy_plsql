create or replace
procedure gerar_solic_compra_requisicao(
	nr_requisicao_p	in number,
	nm_usuario_p in varchar2,
	cd_estabelecimento_p number,
	nr_solic_compra_p out number) is

cd_estabelecimento_w	requisicao_material.cd_estabelecimento%type;
cd_pessoa_requisitante_w	requisicao_material.cd_pessoa_requisitante%type;
cd_setor_entrega_w	requisicao_material.cd_setor_entrega%type;
ds_observacao_w		requisicao_material.ds_observacao%type;
dt_aprovacao_w		date;
dt_atualizacao_w		date;
dt_baixa_w		date;
dt_liberacao_w		date;
dt_solicitacao_requisicao_w	date;
nm_usuario_req_mat_w	usuario.nm_usuario%type;

cd_setor_atendimento_w	solic_compra.cd_setor_atendimento%type;
cd_centro_custo_w		solic_compra.cd_centro_custo%type;
nr_solic_compra_w		solic_compra.nr_solic_compra%type;

cd_material_w		item_requisicao_material.cd_material%type;
cd_motivo_baixa_w		item_requisicao_material.cd_motivo_baixa%type;
cd_unidade_medida_w	item_requisicao_material.cd_unidade_medida%type;
dt_atendimento_w		date;
dt_atuali_item_req_mat_w	date;
nm_usuario_item_req_mat_w	usuario.nm_usuario%type;
nr_seq_item_requisicao_w	item_requisicao_material.nr_sequencia%type;
qt_material_requisitada_w	item_requisicao_material.qt_material_requisitada%type;

nr_item_solic_compra_w	solic_compra_item.nr_item_solic_compra%type;
qt_dias_obter_compra_w	number(10);
qt_conv_compra_est_orig_w	solic_compra_item.qt_conv_compra_est_orig%type;
cd_local_estoque_w	requisicao_material.cd_local_estoque%type;
qt_regra_w		number(5);
cd_motivo_baixa_ww	sup_motivo_baixa_req.nr_sequencia%type;
qt_pendentes_w		number(5);

cursor c01 is
select	b.cd_material,
	b.cd_motivo_baixa,
	b.cd_unidade_medida,
	b.nr_sequencia,
	b.qt_material_requisitada
from	requisicao_material a,
	item_requisicao_material b
where	a.nr_requisicao = b.nr_requisicao
and	substr(sup_obter_se_gera_solic_req(a.cd_estabelecimento, b.cd_material),1,1) = 'S'
and	a.nr_requisicao = nr_requisicao_p;

begin
begin
select	CD_MOTIVO_BAIXA_REQ_SOLIC
into	cd_motivo_baixa_ww
from	PARAMETRO_COMPRAS
where	cd_estabelecimento = cd_estabelecimento_p
and	CD_MOTIVO_BAIXA_REQ_SOLIC is not null;
exception
when others then
	begin
	select	min(nr_sequencia)
	into	cd_motivo_baixa_ww
	from	sup_motivo_baixa_req
	where	cd_motivo_baixa  = 5
	and	ie_situacao = 'A';
	end;
end;

if	(cd_motivo_baixa_ww is null) then
	/*
	Deve ser informado o motivo de baixa padr�o para os itens gerados na solicita��o de compras.
	Verificar o cadastro dos Par�metros de Compras!
	*/
	wheb_mensagem_pck.exibir_mensagem_abort(289792);
end if;

open c01;
loop
fetch c01 into
	cd_material_w,
	cd_motivo_baixa_w,
	cd_unidade_medida_w,
	nr_seq_item_requisicao_w,
	qt_material_requisitada_w;
exit when c01%notfound;
	begin
	if	(nr_solic_compra_w is null) then
		begin
		obter_param_usuario(913, 11,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, qt_dias_obter_compra_w);

		select	cd_estabelecimento,
			cd_pessoa_requisitante,
			cd_setor_entrega,
			ds_observacao,
			dt_aprovacao,
			dt_atualizacao,
			dt_baixa,
			dt_liberacao,
			dt_solicitacao_requisicao,
			nm_usuario,
			cd_local_estoque,
			cd_centro_custo
		into	cd_estabelecimento_w,
			cd_pessoa_requisitante_w,
			cd_setor_entrega_w,
			ds_observacao_w,
			dt_aprovacao_w,
			dt_atualizacao_w,
			dt_baixa_w,
			dt_liberacao_w,
			dt_solicitacao_requisicao_w,
			nm_usuario_req_mat_w,
			cd_local_estoque_w,
			cd_centro_custo_w
		from	requisicao_material
		where	nr_requisicao = nr_requisicao_p;

		select	solic_compra_seq.nextval
		into	nr_solic_compra_w
		from	dual;

		insert into solic_compra (
			nr_solic_compra,
			cd_centro_custo,
			cd_estabelecimento,
			cd_pessoa_solicitante,
			cd_setor_atendimento,
			cd_setor_entrega,
			ds_observacao,
			dt_autorizacao,
			dt_atualizacao,
			dt_baixa,
			dt_liberacao,
			dt_solicitacao_compra,
			nm_usuario,
			ie_aviso_aprov_oc,
			ie_situacao,
			ie_urgente,
			ie_aviso_chegada,
			ie_tipo_solicitacao,
			ie_comodato,
			ie_semanal,
			cd_local_estoque,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		values(	nr_solic_compra_w,
			cd_centro_custo_w,
			cd_estabelecimento_w,
			cd_pessoa_requisitante_w,
			cd_setor_atendimento_w,
			cd_setor_entrega_w,
			ds_observacao_w,
			dt_aprovacao_w,
			dt_atualizacao_w,
			dt_baixa_w,
			dt_liberacao_w,
			dt_solicitacao_requisicao_w,
			nm_usuario_req_mat_w,
			'N',
			'A',
			'N',
			'N',
			'4',
			'N',
			'N',
			cd_local_estoque_w,
			nm_usuario_p,
			sysdate);
		end;
	end if;
	
	select	nvl(max(nr_item_solic_compra),0) + 1
	into	nr_item_solic_compra_w
	from	solic_compra_item
	where	nr_solic_compra = nr_solic_compra_w;

	qt_conv_compra_est_orig_w :=	obter_dados_material(cd_material_w,'QCE');

	insert into solic_compra_item (
		nr_solic_compra,
		nr_item_solic_compra,
		cd_material,
		cd_unidade_medida_compra,
		qt_material,
		cd_motivo_baixa,
		dt_atualizacao,
		ie_geracao,
		ie_situacao,
		nm_usuario,
		nr_requisicao,
		nr_seq_item_requisicao,
		qt_conv_compra_est_orig,
		dt_solic_item)
	values(	nr_solic_compra_w,
		nr_item_solic_compra_w,
		cd_material_w,
		cd_unidade_medida_w,
		qt_material_requisitada_w,
		cd_motivo_baixa_w,
		sysdate,
		'S',
		'A',
		nm_usuario_p,
		nr_requisicao_p,
		nr_seq_item_requisicao_w,
		qt_conv_compra_est_orig_w,
		sysdate);
		
	gerar_solic_item_entrega(nr_solic_compra_w, nr_item_solic_compra_w, nm_usuario_p);
	
	baixar_item_req_motivo(nm_usuario_p, nr_requisicao_p, nr_seq_item_requisicao_w, cd_motivo_baixa_ww, qt_material_requisitada_w);
	end;
end loop;
close c01;
	
select	count(1)
into	qt_pendentes_w
from	item_requisicao_material
where	nr_requisicao = nr_requisicao_p
and	dt_atendimento is null;

if	(qt_pendentes_w = 0) then
	update	requisicao_material
	set	dt_baixa = sysdate
	where	nr_requisicao = nr_requisicao_p;
end if;

end gerar_solic_compra_requisicao;
/
