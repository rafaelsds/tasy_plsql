create or replace procedure GERAR_TISS_SITUACAO_PROT_GLOSA
		(NR_SEQUENCIA_p			  in	number,
		NM_USUARIO_p			  in	varchar2,
		NR_SEQ_PROT_GUIA_p		  in	number,
		NR_SEQ_PROT_GUIA_ITEM_p	  in	number,
		NR_SEQ_MOTIVO_GLOSA_p	  in	number,
		DS_MOTIVO_GLOSA_p		  in	varchar2,
        	CD_MOTIVO_GLOSA_TISS_p    in varchar2 default 0,
        	DS_TIPO_GLOSA_p           in varchar2 default 0,
        	NR_SEQ_PROTOC_ENVIO_RET_p in number default '') is

begin

insert into TISS_SITUACAO_PROT_GLOSA
	(NR_SEQUENCIA,
	DT_ATUALIZACAO,
	NM_USUARIO,
	DT_ATUALIZACAO_NREC,
	NM_USUARIO_NREC,
	NR_SEQ_PROT_GUIA,
	NR_SEQ_PROT_GUIA_ITEM,
	NR_SEQ_MOTIVO_GLOSA,
	DS_MOTIVO_GLOSA,
    	CD_MOTIVO_GLOSA_TISS,
    	DS_TIPO_GLOSA,
   	NR_SEQ_PROTOC_ENVIO_RET)
values	(NR_SEQUENCIA_p,
	sysdate,
	NM_USUARIO_p,
	sysdate,
	NM_USUARIO_p,
	NR_SEQ_PROT_GUIA_p,
	NR_SEQ_PROT_GUIA_ITEM_p,
	NR_SEQ_MOTIVO_GLOSA_p,
	DS_MOTIVO_GLOSA_p,
    	CD_MOTIVO_GLOSA_TISS_p,
   	DS_TIPO_GLOSA_p,
   	NR_SEQ_PROTOC_ENVIO_RET_p);

commit;

end GERAR_TISS_SITUACAO_PROT_GLOSA;
/
