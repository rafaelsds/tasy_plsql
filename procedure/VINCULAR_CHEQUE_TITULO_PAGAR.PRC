create or replace Procedure VINCULAR_CHEQUE_TITULO_PAGAR (nr_seq_cheque_p			number,
					nr_cheque_p			varchar2,
					cd_banco_p			varchar2,
					cd_agencia_bancaria_p 		varchar2,
					nr_titulo_p			number,
					ie_gera_baixa_p			varchar2,
					nm_usuario_p			varchar2,
					nr_seq_trans_fin_baixa_p	number,
					ie_titulo_maior_valor_p		varchar2,
					ie_acao_p			varchar2) is

/* ie_acao_p

V	Vincular
D	Desvincular

*/

vl_saldo_titulo_w	number(15,2);
vl_cheque_w		number(15,2);
cd_tipo_baixa_w		number(5);
cd_estabelecimento_w	number(4);
vl_baixa_w		number(15,2);
vl_titulos_w		number(15,2);
vl_titulo_w		number(15,2);
ie_titulo_menor_valor_w	varchar2(1);
vl_cheques_w		number(15,2);
ie_soma_cheque_maior_w	varchar2(1);
ie_valida_vl_titulo_w	varchar2(1);
vl_saldo_tit_cheque_w	titulo_pagar.vl_saldo_titulo%type;
/* Projeto Multimoeda - Variaveis */
vl_cheque_estrang_w	number(15,2);
nr_seq_cheque_tit_w	number(10);
vl_cotacao_w		cotacao_moeda.vl_cotacao%type;

cd_moeda_estab_w			parametros_contas_pagar.cd_moeda_padrao%type;
cd_moeda_cheque_w			cheque.cd_moeda%type;
vl_titulo_estrang_w			titulo_pagar.vl_titulo_estrang%type;
vl_titulos_estrang_vinc_w	titulo_pagar.vl_titulo_estrang%type;
vl_saldo_tit_estrang_w		titulo_pagar_baixa.vl_baixa_estrang%type;
cd_moeda_tit_w				titulo_pagar.cd_moeda%type;

begin

select	nvl(Min(cd_tipo_baixa),0)
into	cd_tipo_baixa_w
from	tipo_baixa_cpa
where	ie_tipo_consistencia	= 2;

if	(cd_tipo_baixa_w = 0) then
	--r.aise_application_error(-20011,'Nao foi encontrada uma baixa de tipo de consistencia "Cheques"!');
	wheb_mensagem_pck.exibir_mensagem_abort(256629);
end if;

select	vl_saldo_titulo,
	cd_estabelecimento,
	vl_titulo,
	vl_titulo_estrang,
	cd_moeda
into	vl_saldo_titulo_w,
	cd_estabelecimento_w,
	vl_titulo_w,
	vl_titulo_estrang_w,
	cd_moeda_tit_w
from 	titulo_pagar
where	nr_titulo		= nr_titulo_p;

obter_param_usuario(127,37,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_titulo_menor_valor_w);
obter_param_usuario(127,38,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_soma_cheque_maior_w);
obter_param_usuario(127,50,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_valida_vl_titulo_w);

select	vl_cheque,
	vl_cheque_estrang,
	vl_cotacao,
	cd_moeda
into	vl_cheque_w,
	vl_cheque_estrang_w,
	vl_cotacao_w,
	cd_moeda_cheque_w
from	cheque
where	nr_sequencia	= nr_seq_cheque_p;

vl_baixa_w	:= vl_cheque_w;

if	(nvl(ie_acao_p,'V')	= 'V') then

	if	(ie_titulo_menor_valor_w = 'N') and
		(vl_titulo_w < vl_cheque_w) then
		--r.aise_application_error(-20011,'O titulo ' || nr_titulo_p || ' nao pode ser vinculado porque seu valor e menor do que o valor do cheque! Parametro [37]');
		wheb_mensagem_pck.exibir_mensagem_abort(256630,'nr_titulo_p='||nr_titulo_p);
	end if;

	if	(ie_soma_cheque_maior_w	= 'N') then

		select	nvl(sum(c.vl_cheque),0)
		into	vl_cheques_w
		from	cheque c,
			cheque_bordero_titulo b
		where	c.nr_sequencia 	= b.nr_seq_cheque
		and	b.nr_titulo	= nr_titulo_p;

		/* Valida pelo saldo do titulo quando indicado no parametro 50 da digitacao de cheques */
		if (nvl(ie_valida_vl_titulo_w,'T') = 'S') then
			if	((vl_cheques_w + nvl(vl_cheque_w,0)) > vl_saldo_titulo_w) then
				--r.aise_application_error(-20011,'A soma dos cheques vinculados ao titulo ' || nr_titulo_p || ' supera o saldo do titulo! Parametro [38]');
				wheb_mensagem_pck.exibir_mensagem_abort(361924,'nr_titulo_p='||nr_titulo_p);
			end if;
		else
			if	((vl_cheques_w + nvl(vl_cheque_w,0)) > vl_titulo_w) then
				--r.aise_application_error(-20011,'A soma dos cheques vinculados ao titulo ' || nr_titulo_p || ' supera o valor do titulo! Parametro [38]');
				wheb_mensagem_pck.exibir_mensagem_abort(256631,'nr_titulo_p='||nr_titulo_p);
			end if;
		end if;

	end if;

	select	cheque_bordero_titulo_seq.nextval
	into	nr_seq_cheque_tit_w
	from 	dual;

	Insert	into cheque_bordero_titulo
		(nr_sequencia,
		nr_titulo,
		nr_seq_cheque,
		nr_cheque,
		nm_usuario,
		dt_atualizacao,
		cd_banco,
		cd_agencia_bancaria)
	values	(nr_seq_cheque_tit_w,
		nr_titulo_p,
		nr_seq_cheque_p,
		nr_cheque_p,
		nm_usuario_p,
		sysdate,
		cd_banco_p,
		cd_agencia_bancaria_p);

	select	sum(a.vl_titulo),
			sum(a.vl_saldo_titulo),
			sum(a.vl_titulo_estrang),
			sum((select sum(x.vl_baixa_estrang) from titulo_pagar_baixa x where x.nr_titulo = a.nr_titulo) - a.vl_titulo_estrang) vl_saldo_tit_estrang
	into	vl_titulos_w,
			vl_saldo_tit_cheque_w,
			vl_titulos_estrang_vinc_w,
			vl_saldo_tit_estrang_w
	from	cheque c,
		cheque_bordero_titulo b,
		titulo_pagar a
	where	c.nr_sequencia 	= b.nr_seq_cheque
	and	b.nr_titulo	= a.nr_titulo
	and	c.nr_sequencia	= nr_seq_cheque_p;

	select	Obter_Moeda_Padrao(obter_estabelecimento_ativo,'P')
	into	cd_moeda_estab_w
	from	dual;

	/*Se a moeda do cheque for diferente da moeda do estalecimento logado, indica q o cheque e em moeda estrangeira, e devemos comparar o vl estrageiro do cheque com o valor estrangeiro do documento*/
	if (cd_moeda_cheque_w <> cd_moeda_estab_w) then

		if (cd_moeda_cheque_w = cd_moeda_tit_w) then
			vl_titulos_w 			:=  vl_titulos_estrang_vinc_w;
			vl_saldo_tit_cheque_w	:=	vl_saldo_tit_estrang_w;
			vl_cheque_w				:= 	vl_cheque_estrang_w;
		end if;

	end if;

	if (nvl(ie_valida_vl_titulo_w,'T') = 'T') then

		if 	(vl_titulos_w > vl_cheque_w) and
			(nvl(ie_titulo_maior_valor_p,'N') = 'N') then
			--r.aise_application_error(-20011, 'O valor dos titulos ultrapassaram o valor do cheque!');
			wheb_mensagem_pck.exibir_mensagem_abort(256632);
		end if;

	elsif (nvl(ie_valida_vl_titulo_w,'T') = 'S') then

		if 	(vl_saldo_tit_cheque_w > vl_cheque_w) and
			(nvl(ie_titulo_maior_valor_p,'N') = 'N') then
			--r.aise_application_error(-20011, 'O valor dos titulos ultrapassaram o valor do cheque!');
			wheb_mensagem_pck.exibir_mensagem_abort(256632);
		end if;

	end if;

	if	(vl_cheque_w > vl_saldo_titulo_w) then
		vl_baixa_w	:= vl_saldo_titulo_w;
	end if;


	if	(ie_gera_baixa_p = 'S') then
		/* Projeto Multimoeda - Verifica se o cheque e em moeda estrangeira, caso positivo passa o parametro para realizar a baixa em moeda estrangeira*/
		if (nvl(vl_cheque_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			Baixa_Titulo_Pagar(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_p,
					vl_baixa_w,
					nm_usuario_p,
					nr_seq_trans_fin_baixa_p,
					'',
					'',
					'',
					'',
					null,
					null,
					nr_seq_cheque_tit_w);
		else
			Baixa_Titulo_Pagar(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_p,
					vl_baixa_w,
					nm_usuario_p,
					nr_seq_trans_fin_baixa_p,
					'',
					'',
					'',
					'');
		end if;

		Atualizar_Saldo_Tit_Pagar (nr_titulo_p	, nm_usuario_p);
		Gerar_W_Tit_Pag_imposto(nr_titulo_p, nm_usuario_p);
	end if;

elsif	(ie_acao_p	= 'D') then

	if	(vl_cheque_w > vl_titulo_w) then
		vl_baixa_w	:= vl_titulo_w;
	end if;

	vl_baixa_w	:= nvl(vl_baixa_w,0) * -1;
	/* Projeto Multimoeda - Realizar o estorno da baixa antes de excluir o vinculo, para que seja possivel identificar o cheque em moeda estrangeira. */
	select	max(nr_sequencia)
	into	nr_seq_cheque_tit_w
	from 	cheque_bordero_titulo
	where	nr_titulo	= nr_titulo_p
	  and	nr_seq_cheque	= nr_seq_cheque_p
	  and	nr_bordero	is null;

	if	(ie_gera_baixa_p = 'S') then
		/* Projeto Multimoeda - Verifica se o cheque e em moeda estrangeira, caso positivo passa o parametro para realizar a baixa em moeda estrangeira*/
		if (nvl(vl_cheque_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			Baixa_Titulo_Pagar(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_p,
					vl_baixa_w,
					nm_usuario_p,
					nr_seq_trans_fin_baixa_p,
					'',
					'',
					'',
					'',
					null,
					null,
					nr_seq_cheque_tit_w);
		else
			Baixa_Titulo_Pagar(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_p,
					vl_baixa_w,
					nm_usuario_p,
					nr_seq_trans_fin_baixa_p,
					'',
					'',
					'',
					'');
		end if;

		Atualizar_Saldo_Tit_Pagar (nr_titulo_p	, nm_usuario_p);
		Gerar_W_Tit_Pag_imposto(nr_titulo_p, nm_usuario_p);
	end if;

	delete	from cheque_bordero_titulo
	where	nr_titulo		= nr_titulo_p
	and	nr_seq_cheque	= nr_seq_cheque_p
	and	nr_bordero	is null;

end if;

commit;

end VINCULAR_CHEQUE_TITULO_PAGAR;
/
