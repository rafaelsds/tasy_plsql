create or replace
procedure alterar_status_agecons_dia(
					nr_seq_agenda_p	number,
					dt_agenda_p		date,
					ie_status_p		varchar2,
					cd_pessoa_fisica_p	varchar2,
					ds_informacoes_p	varchar2,
					nm_usuario_p		varchar2
					) is

ie_altera_status_confirmada_w parametro_agenda.ie_altera_status_confirmada%type;
					
begin
if	(nr_seq_agenda_p is not null) and
	(dt_agenda_p is not null) and
	(ie_status_p is not null) and
	(cd_pessoa_fisica_p is not null) then

	if	(ie_status_p = 'A') then

		update	agenda_consulta
		set	ie_status_agenda		= ie_status_p,
			dt_aguardando			= sysdate,
			nm_usuario			= nm_usuario_p
		where ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda_p)
		and	ie_status_agenda		= 'N'
		and	dt_aguardando			is null
		and	cd_pessoa_fisica		= cd_pessoa_fisica_p
		and	nr_sequencia			<> nr_seq_agenda_p;

	elsif	(ie_status_p = 'O') then

		update	agenda_consulta
		set	ie_status_agenda		= ie_status_p,
			dt_consulta			= sysdate,
			nm_usuario			= nm_usuario_p
		where	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda_p)
		and	ie_status_agenda		= 'A'
		and	dt_consulta			is null
		and	cd_pessoa_fisica		= cd_pessoa_fisica_p
		and	nr_sequencia			<> nr_seq_agenda_p;

	elsif	(ie_status_p = 'AC') then
		select 	nvl(max(ie_altera_status_confirmada),'N')
		into 	ie_altera_status_confirmada_w
		from 	parametro_agenda
		where	cd_estabelecimento = obter_estabelecimento_ativo;
		
		update	agenda_consulta
		set	dt_confirmacao		= sysdate,
			nm_usuario_confirm	= nm_usuario_p,
			nm_usuario			= nm_usuario_p,
			ds_confirmacao		= nvl(ds_informacoes_p, ds_confirmacao),
			ie_status_agenda	= decode(ie_altera_status_confirmada_w,'S','CN',ie_status_agenda)
		where	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda_p)
		and	ie_status_agenda	= 'N'
		and	dt_confirmacao		is null
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	nr_sequencia		<> nr_seq_agenda_p;
	elsif	((ie_status_p = 'F') or (ie_status_p = 'I')) then
		update	agenda_consulta
		set	ie_status_agenda		= ie_status_p,
			ds_motivo_status	=	ds_informacoes_p,	
			dt_status				= sysdate,
			nm_usuario			= nm_usuario_p
		where	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda_p)
		and	ie_status_agenda		= 'N'
		and	cd_pessoa_fisica		= cd_pessoa_fisica_p
		and	nr_sequencia			<> nr_seq_agenda_p;
	end if;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end alterar_status_agecons_dia;
/
