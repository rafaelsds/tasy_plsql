create or replace
procedure regerar_cron_proj_migr_funcao (
		nr_seq_cronograma_p	number,
		cd_funcao_p		number,
		nm_usuario_p		varchar2) is
		
begin
if	(nr_seq_cronograma_p is not null) and
	(cd_funcao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	delete
	from	proj_cron_etapa
	where	nr_seq_cronograma = nr_seq_cronograma_p;
	
	update	proj_cronograma
	set	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_cronograma_p;
	
	gerar_ativ_cron_proj_migr(nr_seq_cronograma_p, cd_funcao_p, nm_usuario_p);
	end;
end if;
commit;
end regerar_cron_proj_migr_funcao;
/