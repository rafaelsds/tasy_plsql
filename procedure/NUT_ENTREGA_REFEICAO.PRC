create or replace
procedure nut_entrega_refeicao(nr_sequencia_p		number,
								cd_pessoa_fisica_p	number,
								nm_usuario_p		Varchar2) is 

begin

	update 	nut_atend_serv_dia
	set 	dt_atend_servico = sysdate,
		cd_copeira = cd_pessoa_fisica_p,
		nm_usuario_atend = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

commit;

end nut_entrega_refeicao;
/