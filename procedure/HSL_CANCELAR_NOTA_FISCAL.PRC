create or replace procedure hsl_cancelar_nota_fiscal
			(nr_documento_p		in varchar2,
			nm_usuario_p		in varchar2,
			nr_nota_fiscal_p		in varchar2,
			nr_titulo_p		in number,
			ds_erro_p			out varchar2)
			is

nr_seq_nf_w			number(10);
nr_titulo_w			number(10);
ds_retorno_w			varchar2(4000);
dt_atualizacao_estoque_w		date;
ie_situacao_w			varchar2(1);

begin

select	max(nr_seq_nf_saida)
into	nr_seq_nf_w
from	titulo_receber
where	nr_titulo		= nr_titulo_p;

select	max(nr_titulo)
into	nr_titulo_w
from	titulo_receber
where	nr_titulo		= nr_titulo_p;

if	(nr_seq_nf_w is null) and
	(nvl(nr_nota_fiscal_p,0) > 0) then
	ds_erro_p	:= 'Nota fiscal n�o encontrada no Tasy (NF Externa: '||nr_nota_fiscal_p||')';
end if;
if	(nr_titulo_w is null) and
	(nvl(nr_titulo_p,0) > 0) then
	ds_erro_p	:= 'T�tulo a receber n�o encontrada no Tasy (TR Externo: '||nr_titulo_p||')';
end if;
	
if	(ds_erro_p is null) then

	if	(nr_seq_nf_w is not null) then

		select	max(dt_atualizacao_estoque)
		into	dt_atualizacao_estoque_w	
		from	nota_fiscal
		where	nr_sequencia	= nr_seq_nf_w;
		
		if	(dt_atualizacao_estoque_w is not null) then
			consiste_estornar_nota_fiscal(nr_seq_nf_w,'S','S',ds_retorno_w,nm_usuario_p,'N');
			
			if	(ds_retorno_w is null) then
				hsl_estornar_nota_fiscal(nr_seq_nf_w,nm_usuario_p);
			else
				ds_erro_p	:= ds_retorno_w;
			end if;
			
		else
			update	nota_fiscal
			set	ie_situacao 	= 9,		
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia 	= nr_seq_nf_w;				
			commit;
		end if;
	end if;
	
	select	max(ie_situacao)
	into	ie_situacao_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;	
	
	if	(nr_titulo_w is not null) and
		(nvl(ie_situacao_w,'1') <> '3') then
		cancelar_titulo_receber(nr_titulo_w,nm_usuario_p,'N',sysdate);
		commit;
	end if;

end if;

end;
 
/