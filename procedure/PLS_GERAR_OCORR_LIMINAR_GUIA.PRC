create or replace
procedure pls_gerar_ocorr_liminar_guia
			(	nr_seq_segurado_p	Number,
				nr_seq_processo_p	Number,
				nr_seq_guia_p		Number,
				nm_usuario_p		Varchar2,
				nr_seq_regra_p	out	Number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar as liminares cadastradas para o benefici�rio na fun��o OPS - Processo Judicial e, caso exista ocorr�ncia para liminar judicial na fun��o OPS - Glosas e Ocorr�ncias, gera ocorr�ncia na guia.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
1 - Performance.
2 - Muita aten��o na altera��o desta rotina, ela n�o pode parar de funcionar.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_area_w			Number(15);
cd_especialidade_w		Number(15);
cd_grupo_w			Number(15);
cd_procedimento_w		Number(15);

ie_origem_proced_w		Number(10);
ie_origem_proc_w		Number(10);
ie_estagio_w			Number(4);
ie_gerou_ocorr_w		Varchar2(2)	:= 'N';

nr_seq_auditoria_w		Number(10);
nr_seq_guia_w			Number(10);
nr_seq_material_w		Number(10);
nr_seq_ocorrencia_w		Number(10);
nr_seq_regra_w			Number(10)	:= null;
nr_seq_prestador_w		Number(10);
nr_seq_processo_w		Number(10);
nr_seq_guia_proc_w		Number(10);
nr_seq_guia_mat_w		Number(10);
nr_seq_contrato_w		Number(10);

qt_reg_proc_w			Number(10);
qt_reg_mat_w			Number(10);
qt_reg_prest_w			Number(10);
qt_reg_seg_w			Number(10);
qt_ocorr_liminar_antiga_w	Number(10);

ie_tipo_processo_w		pls_guia_plano.ie_tipo_processo%type;
ie_tipo_intercambio_w		pls_guia_plano.ie_tipo_intercambio%type;
ie_liminar_intercambio_w	Varchar2(2) := 'N';
qt_ped_w			pls_integer := 0;

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

Cursor C00 is
	select	nr_sequencia
	from	processo_judicial_liminar
	where	(((nvl(ie_considera_codigo_pf, 'N') = 'N'	and nr_seq_segurado	= nr_seq_segurado_p)
	or	(nvl(ie_considera_codigo_pf, 'N') = 'S'		and pls_obter_dados_segurado(nr_seq_segurado, 'PF')	= cd_pessoa_fisica_w))
	or	nr_seq_contrato		= nr_seq_contrato_w)
	and	ie_estagio		= 2
	and	ie_impacto_autorizacao	= 'S'
	and	sysdate	between (dt_inicio_validade) and (nvl(dt_fim_validade,sysdate));

Cursor C01 is
	select	nr_sequencia,
		nr_seq_guia
	from	pls_auditoria
	where	dt_liberacao		is null
	and	nr_seq_guia		is not null
	and	(nr_seq_segurado	= nr_seq_segurado_p
	or	pls_obter_dados_segurado(nr_seq_segurado,'NC') = nr_seq_contrato_w);
	
Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_w
	and	ie_status		<> 'S';
	 
Cursor C03 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_w
	and	ie_status		<> 'S';
	
Cursor C05 is
	select	a.nr_sequencia,
		b.nr_sequencia
	from	pls_ocorrencia		a,
		pls_ocorrencia_regra	b
	where	a.nr_sequencia		= b.nr_seq_ocorrencia
	and	a.ie_situacao		= 'A'
	and	a.ie_auditoria		= 'S'
	and	b.ie_aplicacao_regra	= 'A'
	and	b.ie_possui_liminar	= 'S'
	and	b.ie_situacao		= 'A'
	and	sysdate between b.dt_inicio_vigencia and nvl(b.dt_fim_vigencia,sysdate);
	
Cursor C06 is
	select	a.nr_sequencia,
		b.nr_sequencia
	from	pls_ocorrencia			a,
		pls_ocor_aut_combinada		b,
		pls_validacao_aut_liminar 	c
	where	b.nr_seq_ocorrencia		= a.nr_sequencia
	and	c.nr_seq_ocor_aut_combinada	= b.nr_sequencia
	and	a.ie_situacao			= 'A'
	and	a.ie_auditoria			= 'S'
	and	sysdate between b.dt_inicio_vigencia and nvl(b.dt_fim_vigencia,sysdate)
	and	b.ie_aplicacao_regra		= 30
	and	c.ie_valida_liminar		= 'S'
	and	c.ie_situacao			= 'A';

begin

if	(nr_seq_guia_p	is not null) then
	select	nvl(nr_seq_prestador,0),
		ie_estagio,
		nvl(ie_tipo_processo, 'X'),
		nvl(ie_tipo_intercambio, 'X')
	into	nr_seq_prestador_w,
		ie_estagio_w,
		ie_tipo_processo_w,
		ie_tipo_intercambio_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;
	
	begin
		select	nr_seq_contrato,
			cd_pessoa_fisica
		into	nr_seq_contrato_w,
			cd_pessoa_fisica_w
		from	pls_segurado
		where	nr_sequencia = nr_seq_segurado_p;
	exception
	when others then
		nr_seq_contrato_w := 0;
	end;

	if	(ie_estagio_w	in (1,2,4,10,12)) then		
		nr_seq_guia_w	:= nr_seq_guia_p;
		open C00;
		loop
		fetch C00 into	
			nr_seq_processo_w;
		exit when C00%notfound;
			begin
			
			select	count(1)
			into	qt_ocorr_liminar_antiga_w
			from	pls_ocorrencia		a,
				pls_ocorrencia_regra	b
			where	a.nr_sequencia		= b.nr_seq_ocorrencia
			and	a.ie_situacao		= 'A'
			and	a.ie_auditoria		= 'S'
			and	b.ie_aplicacao_regra	= 'A'
			and	b.ie_possui_liminar	= 'S';
			
			if	(qt_ocorr_liminar_antiga_w > 0) then
			
				open C05;
				loop
				fetch C05 into	
					nr_seq_ocorrencia_w,
					nr_seq_regra_w;
				exit when C05%notfound;
					begin
					select	count(1)
					into	qt_reg_proc_w
					from	processo_judicial_proc
					where	nr_seq_processo	= nr_seq_processo_w;
					
					select	count(1)
					into	qt_reg_mat_w
					from	processo_judicial_mat
					where	nr_seq_processo	= nr_seq_processo_w;

					select	count(1)
					into	qt_reg_prest_w
					from	processo_judicial_prest
					where	nr_seq_processo	= nr_seq_processo_w;

					if	((qt_reg_proc_w	> 0) or
						(qt_reg_mat_w	> 0) or
						(qt_reg_prest_w	> 0))then
						open C02;
						loop
						fetch C02 into
							nr_seq_guia_proc_w,
							cd_procedimento_w,
							ie_origem_proced_w;
						exit when C02%notfound;
							begin
							pls_obter_estrut_proc(	cd_procedimento_w, ie_origem_proced_w, cd_area_w,
										cd_especialidade_w, cd_grupo_w, ie_origem_proc_w);
							select	count(1)
							into	qt_reg_proc_w
							from	processo_judicial_proc		a,
								processo_judicial_liminar	b
							where	b.nr_sequencia					= nr_seq_processo_w
							and	a.nr_seq_processo				= b.nr_sequencia
							and	nvl(a.cd_procedimento,cd_procedimento_w)	= cd_procedimento_w
							and	nvl(a.ie_origem_proced,ie_origem_proc_w) 	= ie_origem_proc_w
							and	nvl(a.cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
							and	nvl(a.cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
							and	nvl(a.cd_area_procedimento, cd_area_w) 		= cd_area_w
							and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
						
							if	(qt_reg_proc_w	> 0) then
								if	(ie_tipo_processo_w = 'I') and
									(ie_tipo_intercambio_w = 'I') then
									ie_liminar_intercambio_w := 'S';
								else
									pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													nr_seq_guia_proc_w, null, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
								end if;
							end if;
							end;
						end loop;
						close C02;
						open C03;
						loop
						fetch C03 into
							nr_seq_guia_mat_w,
							nr_seq_material_w;
						exit when C03%notfound;
							begin
							select	count(1)
							into	qt_reg_mat_w
							from	processo_judicial_mat		a,
								processo_judicial_liminar	b
							where	b.nr_sequencia					= nr_seq_processo_w
							and	a.nr_seq_processo				= b.nr_sequencia
							and	(a.nr_seq_material	is null or 	a.nr_seq_material 	= nr_seq_material_w)
							and	(a.nr_seq_estrut_mat	is null or 	
								pls_obter_se_estruturas_mat(a.nr_seq_estrut_mat, nr_seq_material_w)  = 'S')
							and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
							if	(qt_reg_mat_w	> 0) then
								if	(ie_tipo_processo_w = 'I') and
									(ie_tipo_intercambio_w = 'I') then
									ie_liminar_intercambio_w := 'S';
								else
									pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													null, nr_seq_guia_mat_w, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
								end if;
							end if;
							end;
						end loop;
						close C03;
						
						if	(nr_seq_prestador_w	<> 0) then
							select	count(1)
							into	qt_reg_prest_w
							from	processo_judicial_prest		a,
								processo_judicial_liminar	b
							where	b.nr_sequencia		= nr_seq_processo_w
							and	a.nr_seq_processo	= b.nr_sequencia
							and	a.nr_seq_prestador	= nr_seq_prestador_w
							and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
							
							if	(qt_reg_prest_w	> 0) then
								if	(ie_tipo_processo_w = 'I') and
									(ie_tipo_intercambio_w = 'I') then
									ie_liminar_intercambio_w := 'S';
								else
									pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													null, null, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
								end if;
							end if;
						end if;
					else
						if	(ie_tipo_processo_w = 'I') and
							(ie_tipo_intercambio_w = 'I') then
							ie_liminar_intercambio_w := 'S';
						else
							pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													null, null, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
						end if;
					end if;
					end;
				end loop;
				close C05;
			
			else
			
				open C06;
				loop
				fetch C06 into	
					nr_seq_ocorrencia_w,
					nr_seq_regra_w;
				exit when C06%notfound;
					begin
					select	count(1)
					into	qt_reg_proc_w
					from	processo_judicial_proc
					where	nr_seq_processo	= nr_seq_processo_w;
					
					select	count(1)
					into	qt_reg_mat_w
					from	processo_judicial_mat
					where	nr_seq_processo	= nr_seq_processo_w;

					select	count(1)
					into	qt_reg_prest_w
					from	processo_judicial_prest
					where	nr_seq_processo	= nr_seq_processo_w;

					if	((qt_reg_proc_w	> 0) or
						(qt_reg_mat_w	> 0) or
						(qt_reg_prest_w	> 0))then
						open C02;
						loop
						fetch C02 into
							nr_seq_guia_proc_w,
							cd_procedimento_w,
							ie_origem_proced_w;
						exit when C02%notfound;
							begin
							pls_obter_estrut_proc(	cd_procedimento_w, ie_origem_proced_w, cd_area_w,
										cd_especialidade_w, cd_grupo_w, ie_origem_proc_w);
							select	count(1)
							into	qt_reg_proc_w
							from	processo_judicial_proc		a,
								processo_judicial_liminar	b
							where	b.nr_sequencia					= nr_seq_processo_w
							and	a.nr_seq_processo				= b.nr_sequencia
							and	nvl(a.cd_procedimento,cd_procedimento_w)	= cd_procedimento_w
							and	nvl(a.ie_origem_proced,ie_origem_proc_w) 	= ie_origem_proc_w
							and	nvl(a.cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
							and	nvl(a.cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
							and	nvl(a.cd_area_procedimento, cd_area_w) 		= cd_area_w
							and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
						
							if	(qt_reg_proc_w	> 0) then
								if	(ie_tipo_processo_w = 'I') and
									(ie_tipo_intercambio_w = 'I') then
									ie_liminar_intercambio_w := 'S';
								else
									pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													nr_seq_guia_proc_w, null, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
								end if;
							end if;
							end;
						end loop;
						close C02;
						open C03;
						loop
						fetch C03 into
							nr_seq_guia_mat_w,
							nr_seq_material_w;
						exit when C03%notfound;
							begin
							select	count(1)
							into	qt_reg_mat_w
							from	processo_judicial_mat		a,
								processo_judicial_liminar	b
							where	b.nr_sequencia					= nr_seq_processo_w
							and	a.nr_seq_processo				= b.nr_sequencia
							and	(a.nr_seq_material	is null or 	a.nr_seq_material 	= nr_seq_material_w)
							and	(a.nr_seq_estrut_mat	is null or 	
								pls_obter_se_estruturas_mat(a.nr_seq_estrut_mat, nr_seq_material_w)  = 'S')
							and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
							if	(qt_reg_mat_w	> 0) then
								if	(ie_tipo_processo_w = 'I') and
									(ie_tipo_intercambio_w = 'I') then
									ie_liminar_intercambio_w := 'S';
								else
									pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													null, nr_seq_guia_mat_w, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
								end if;
							end if;
							end;
						end loop;
						close C03;
						
						if	(nr_seq_prestador_w	<> 0) then
							select	count(1)
							into	qt_reg_prest_w
							from	processo_judicial_prest		a,
								processo_judicial_liminar	b
							where	b.nr_sequencia		= nr_seq_processo_w
							and	a.nr_seq_processo	= b.nr_sequencia
							and	a.nr_seq_prestador	= nr_seq_prestador_w
							and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
							
							if	(qt_reg_prest_w	> 0) then
								if	(ie_tipo_processo_w = 'I') and
									(ie_tipo_intercambio_w = 'I') then
									ie_liminar_intercambio_w := 'S';
								else
									pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													null, null, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
								end if;
							end if;
						end if;
					else
						if	(ie_tipo_processo_w = 'I') and
							(ie_tipo_intercambio_w = 'I') then
							ie_liminar_intercambio_w := 'S';
						else
							pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
													null, null, nr_seq_segurado_p, 
													nr_seq_auditoria_w, nm_usuario_p);
						end if;
					end if;
					end;
				end loop;
				close C06;
			
			end if;
			end;
		end loop;
		close C00;
		
		if	(ie_tipo_processo_w = 'I') and
			(ie_tipo_intercambio_w = 'I') and
			(ie_liminar_intercambio_w = 'S') then
			
			select	count(1)
			into	qt_ped_w
			from	ptu_pedido_autorizacao
			where	nr_seq_guia = nr_seq_guia_w;
			
			if	(qt_ped_w > 0) then
				
				update	ptu_pedido_autorizacao
				set	ie_liminar = 'S'
				where	nr_seq_guia = nr_seq_guia_w;
			
				pls_guia_gravar_historico(nr_seq_guia_w, 2, 'A guia teve o campo "Liminar" marcado, devido a liminar n�: ' || nr_seq_processo_w, null, nm_usuario_p);
			else
				select	count(1)
				into	qt_ped_w
				from	ptu_pedido_compl_aut
				where	nr_seq_guia = nr_seq_guia_w;
				
				if	(qt_ped_w > 0) then
				
					update	ptu_pedido_compl_aut
					set	ie_liminar = 'S'
					where	nr_seq_guia = nr_seq_guia_w;
				
					pls_guia_gravar_historico(nr_seq_guia_w, 2, 'A guia teve o campo "Liminar" marcado, devido a liminar n�: ' || nr_seq_processo_w, null, nm_usuario_p);
				end if;
			end if;
		end if;
	end if;
else	
	select	nvl(nr_seq_contrato,0)
	into	nr_seq_contrato_w
	from	processo_judicial_liminar
	where	nr_sequencia = nr_seq_processo_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_auditoria_w,
		nr_seq_guia_w;
	exit when C01%notfound;
		begin
		select	nvl(nr_seq_prestador,0)
		into	nr_seq_prestador_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_w;
		
		open C05;
		loop
		fetch C05 into	
			nr_seq_ocorrencia_w,
			nr_seq_regra_w;
		exit when C05%notfound;
			begin
			select	count(1)
			into	qt_reg_proc_w
			from	processo_judicial_proc
			where	nr_seq_processo	= nr_seq_processo_w;
			
			select	count(1)
			into	qt_reg_mat_w
			from	processo_judicial_mat
			where	nr_seq_processo	= nr_seq_processo_w;
			
			select	count(1)
			into	qt_reg_prest_w
			from	processo_judicial_prest
			where	nr_seq_processo	= nr_seq_processo_w;
			
			if	((qt_reg_proc_w	> 0) or
				(qt_reg_mat_w	> 0) or
				(qt_reg_prest_w	> 0))then
				open C02;
				loop
				fetch C02 into
					nr_seq_guia_proc_w,
					cd_procedimento_w,
					ie_origem_proced_w;
				exit when C02%notfound;
					begin
					pls_obter_estrut_proc(	cd_procedimento_w, ie_origem_proced_w, cd_area_w,
								cd_especialidade_w, cd_grupo_w, ie_origem_proc_w);
					select	count(1)
					into	qt_reg_proc_w
					from	processo_judicial_proc		a,
						processo_judicial_liminar	b
					where	b.nr_sequencia					= nr_seq_processo_p
					and	a.nr_seq_processo				= b.nr_sequencia
					and	nvl(a.cd_procedimento,cd_procedimento_w)	= cd_procedimento_w
					and	nvl(a.ie_origem_proced,ie_origem_proc_w) 	= ie_origem_proc_w
					and	nvl(a.cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
					and	nvl(a.cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
					and	nvl(a.cd_area_procedimento, cd_area_w) 		= cd_area_w
					and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
					
					if	(qt_reg_proc_w	> 0) then
						pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
										nr_seq_guia_proc_w, null, nr_seq_segurado_p, 
										nr_seq_auditoria_w, nm_usuario_p);
					end if;
					end;
				end loop;
				close C02;
				
				open C03;
				loop
				fetch C03 into
					nr_seq_guia_mat_w,
					nr_seq_material_w;
				exit when C03%notfound;
					begin
					select	count(1)
					into	qt_reg_mat_w
					from	processo_judicial_mat		a,
						processo_judicial_liminar	b
					where	b.nr_sequencia					= nr_seq_processo_p
					and	a.nr_seq_processo				= b.nr_sequencia
					and	nvl(a.nr_seq_material,nr_seq_material_w)	= nr_seq_material_w
					and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
					
					if	(qt_reg_mat_w	> 0) then
						pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
										null, nr_seq_guia_mat_w, nr_seq_segurado_p, 
										nr_seq_auditoria_w, nm_usuario_p);
					end if;
					end;
				end loop;
				close C03;
				
				if	(nr_seq_prestador_w	<> 0) then
					select	count(1)
					into	qt_reg_prest_w
					from	processo_judicial_prest		a,
						processo_judicial_liminar	b
					where	b.nr_sequencia		= nr_seq_processo_p
					and	a.nr_seq_processo	= b.nr_sequencia
					and	a.nr_seq_prestador	= nr_seq_prestador_w
					and	sysdate	between (b.dt_inicio_validade) and (fim_dia(nvl(b.dt_fim_validade,sysdate)));
					
					if	(qt_reg_prest_w	> 0) then
						pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
										null, null, nr_seq_segurado_p, 
										nr_seq_auditoria_w, nm_usuario_p);
					end if;
				end if;
			else
				pls_inserir_ocorr_guia_benef(	nr_seq_ocorrencia_w, nr_seq_regra_w, nr_seq_guia_w,
											null, null, nr_seq_segurado_p, 
											nr_seq_auditoria_w, nm_usuario_p);
			end if;
			end;
		end loop;
		close C05;
		end;
	end loop;
	close C01;

	update	processo_judicial_liminar
	set	ie_estagio	= 2,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_liberacao	= sysdate,
		nm_usuario_lib	= nm_usuario_p
	where	nr_sequencia	= nr_seq_processo_p;
end if;
	
nr_seq_regra_p	:= nr_seq_regra_w;
	
commit;

end pls_gerar_ocorr_liminar_guia;
/