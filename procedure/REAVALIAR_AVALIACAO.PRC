create or replace
PROCEDURE Reavaliar_avaliacao(
		ie_tipo_p		VARCHAR2,
		nr_seq_avaliacao_p	NUMBER,
		nm_usuario_p		VARCHAR2) IS 
		
nr_sequencia_avaliacao_w	number(10,0);
cd_pessoa_fisica_w		number(10,0);
nr_atendimento_w		number(10,0);
nr_seq_tipo_avaliacao_w		number(10,0);
nr_seq_atend_checklist_w	number(10,0);
nr_seq_checklist_item_w		number(10,0);	
cd_medico_w			number(10,0);
	
CURSOR C01 IS	--Busca dependencia do item atual reavaliado;
	SELECT	max(b.nr_seq_avaliacao),
		max(b.nr_sequencia)		
	FROM	checklist_proc_item_depend a,
		checklist_processo_item b
	WHERE	b.nr_sequencia = a.nr_seq_item_dependencia
	AND	a.nr_seq_check_proc_item = nr_seq_checklist_item_w;

	
BEGIN
if (nvl(nr_seq_avaliacao_p,0) > 0) then
	
	SELECT	MAX(a.cd_pessoa_fisica),
		MAX(a.nr_atendimento),
		MAX(a.nr_seq_tipo_avaliacao),
		MAX(a.nr_seq_atend_checklist),
		MAX(a.nr_seq_checklist_item),
		MAX(a.cd_medico)
	INTO	cd_pessoa_fisica_w,
		nr_atendimento_w,
		nr_seq_tipo_avaliacao_w,
		nr_seq_atend_checklist_w,
		nr_seq_checklist_item_w,
		cd_medico_w
	FROM	med_avaliacao_paciente a
	WHERE	a.nr_sequencia = nr_seq_avaliacao_p;
	
	if (nvl(nr_seq_checklist_item_w,0) > 0) THEN
	
		SELECT  med_avaliacao_paciente_seq.NEXTVAL
		INTO 	nr_sequencia_avaliacao_w
		FROM  	dual;		
		
		INSERT INTO med_avaliacao_paciente
			(nr_sequencia,			
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_avaliacao,
			cd_pessoa_fisica,
			cd_medico,
			nr_atendimento,			
			nr_seq_tipo_avaliacao,
			nr_seq_atend_checklist,
			nr_seq_checklist_item,
			ie_atualiza_macro)
		VALUES (
			nr_sequencia_avaliacao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_pessoa_fisica_w,
			cd_medico_w,
			nr_atendimento_w,
			nr_seq_tipo_avaliacao_w,
			nr_seq_atend_checklist_w,
			nr_Seq_checklist_item_w,
			'S');				
	END IF;
		
	IF ((ie_tipo_p = 'D') AND (NVL(nr_seq_checklist_item_w,0) > 0)) THEN  --criar registros de reavaliação para as avaliações dependentes
		
		OPEN C01;
		LOOP
		FETCH C01 INTO
			nr_seq_tipo_avaliacao_w,
			nr_seq_checklist_item_w;
		EXIT WHEN C01%NOTFOUND;
			BEGIN	
			if (nvl(nr_seq_tipo_avaliacao_w, 0) > 0) and (nvl(nr_seq_checklist_item_w, 0) > 0) then
			
				SELECT  med_avaliacao_paciente_seq.NEXTVAL
				INTO 	nr_sequencia_avaliacao_w
				FROM  	dual;		
			
				INSERT INTO med_avaliacao_paciente
					(nr_sequencia,			
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_avaliacao,
					cd_pessoa_fisica,
					cd_medico,
					nr_atendimento,				
					nr_seq_tipo_avaliacao,
					nr_seq_atend_checklist,
					nr_seq_checklist_item,
					ie_atualiza_macro)
				VALUES (
					nr_sequencia_avaliacao_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_pessoa_fisica_w,
					cd_medico_w,
					nr_atendimento_w,
					nr_seq_tipo_avaliacao_w,
					nr_seq_atend_checklist_w,
					nr_Seq_checklist_item_w,
					'S');									
			END IF;
			END;
		END LOOP;
		CLOSE C01;				
		END IF;	
	END IF;
COMMIT;

END Reavaliar_avaliacao;
/
