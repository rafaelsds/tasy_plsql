create or replace
procedure pls_consistir_data_quota_parc(nr_seq_quota_parc_p	number,
					dt_vencimento_p		date,
					ie_inconsistencia_p	out varchar2,
					nm_usuario_p		varchar2) is 
				
nr_seq_escrituracao_w	number(10);
nr_parcela_w		number(5);
dt_vencimento_ant_w	date;
dt_vencimento_pro_w	date;

begin
ie_inconsistencia_p := 'N';

if	(nr_seq_quota_parc_p is not null) then
	select	max(nr_parcela),
		max(nr_seq_escrituracao)
	into	nr_parcela_w,
		nr_seq_escrituracao_w
	from	pls_escrit_quota_parcela
	where	nr_sequencia = nr_seq_quota_parc_p;
	
	select	max(dt_vencimento)
	into	dt_vencimento_pro_w
	from	pls_escrit_quota_parcela
	where	nr_seq_escrituracao	= nr_seq_escrituracao_w
	and	nr_parcela		= nr_parcela_w + 1;
	
	if	(trunc(dt_vencimento_p,'dd') > trunc(dt_vencimento_pro_w,'dd')) then
		ie_inconsistencia_p := 'S';
	end if;	
	
	if	(nr_parcela_w > 1) then
		select	max(dt_vencimento)	
		into	dt_vencimento_ant_w
		from	pls_escrit_quota_parcela
		where	nr_seq_escrituracao	= nr_seq_escrituracao_w
		and	nr_parcela		= nr_parcela_w - 1;	

		if	(trunc(dt_vencimento_p,'dd') < trunc(dt_vencimento_ant_w,'dd')) then
			ie_inconsistencia_p := 'S';
		end if;	
	end if;
end if;

end pls_consistir_data_quota_parc;
/