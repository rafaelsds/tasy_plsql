create or replace
procedure gerar_w_painel_pa	(cd_setor_atendimento_p	number)
				is

/* vetor */
type colunas is record (nm_coluna_w varchar2(255));
type vetor is table of colunas index by binary_integer;

/* globais */
vetor_w					vetor;
nr_seq_grupo_pa_w			number(10);
i					number(5);
nr_seq_apres_w				number(10);
cd_estabelecimento_w			number(10);

cursor c01 is
	select  nr_sequencia
	from	pa_grupo_local
	where	ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_w
	order by 1;

cursor c02 is
	Select  'SEQ=' || b.nr_sequencia || ';' ||
		'DESC=' || ds_abrev  || ';' ||
  		'HINT=' || a.nr_atendimento || '-' || substr(obter_nome_pf(cd_pessoa_fisica),1,59) ||' - '||wheb_mensagem_pck.get_texto(802307)||': ' || to_char(obter_dt_entrada_setor(a.nr_atendimento),'dd/mm/yyyy hh24:mi:ss')||' - '||wheb_mensagem_pck.get_texto(796926)||': '||substr(obter_desc_status_pa(obter_status_pa_atendimento(a.nr_atendimento)),1,80)|| ';' ||
  		'STATUS=' || nvl(b.ie_status,substr(obter_status_painel_pa(b.nr_sequencia, a.nr_atendimento),1,5))|| ';' ||
  		'INICOR=' ||  SUBSTR(Obter_Dados_Classif_Espec(a.NR_SEQ_CLASSIF_ESP,'C'),1,10)	||';'||
		'ATEND=' || a.nr_atendimento || ';' ||
		'INI=' || substr(obter_iniciais_nome(a.cd_pessoa_fisica,null),1,3)
			nm_inicial,

		nvl(b.nr_seq_apres,999) nr_seq_apres
	from	atendimento_paciente a,
		pa_local b
	where	dt_alta is null
	and	a.nr_seq_local_pa = b.nr_sequencia
	and 	b.nr_seq_grupo_pa = nr_seq_grupo_pa_w
	and	((cd_setor_atendimento_p = b.cd_setor_atendimento) or (cd_setor_atendimento_p = 0) or (b.cd_setor_atendimento is null))
	union all
	Select  'SEQ=' || b.nr_sequencia || ';' ||
		'DESC=' || ds_abrev  || ';' ||
  		'HINT=' || ds_abrev  || ';' ||
  		'STATUS=' || nvl(b.ie_status,substr(obter_status_painel_pa(b.nr_sequencia, null),1,5)) || ';' ||
  		'INICOR='|| null||';'||
		'ATEND=' || null || ';' ||
		'INI=' || null,

		nvl(b.nr_seq_apres,999) nr_seq_apres
	from	pa_local b
	where 	b.nr_seq_grupo_pa = nr_seq_grupo_pa_w
	and	((cd_setor_atendimento_p = b.cd_setor_atendimento) or (cd_setor_atendimento_p = 0) or (b.cd_setor_atendimento is null))
	and not exists (select 1
			from atendimento_paciente x
			where x.nr_seq_local_pa = b.nr_sequencia
			and dt_alta is null)
	order by nr_seq_apres;

begin


cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

exec_sql_dinamico('','truncate table w_painel');

OPEN c01;
LOOP
FETCH c01 INTO
	nr_seq_grupo_pa_w;
	EXIT WHEN c01%NOTFOUND;

	for i in 1..40 loop
		begin
			vetor_w(i).nm_coluna_w := null;
		end;
	end loop;

	i := 1;
	OPEN c02;
	LOOP
	FETCH c02 INTO
		vetor_w(i).nm_coluna_w,
		nr_seq_apres_w;
	EXIT WHEN c02%NOTFOUND;
		i := i + 1;
	END LOOP;
	CLOSE c02;

	insert into w_painel (
		CD_ITEM,
		DS_ITEM1,
		DS_ITEM2,
		DS_ITEM3,
		DS_ITEM4,
		DS_ITEM5,
		DS_ITEM6,
		DS_ITEM7,
		DS_ITEM8,
		DS_ITEM9,
		DS_ITEM10,
		DS_ITEM11,
		DS_ITEM12,
		DS_ITEM13,
		DS_ITEM14,
		DS_ITEM15,
		DS_ITEM16,
		DS_ITEM17,
		DS_ITEM18,
		DS_ITEM19,
		DS_ITEM20,
		DS_ITEM21,
		DS_ITEM22,
		DS_ITEM23,
		DS_ITEM24,
		DS_ITEM25,
		DS_ITEM26,
		DS_ITEM27,
		DS_ITEM28,
		DS_ITEM29,
		DS_ITEM30,
		DS_ITEM31,
		DS_ITEM32,
		DS_ITEM33,
		DS_ITEM34,
		DS_ITEM35,
		DS_ITEM36,
		DS_ITEM37,
		DS_ITEM38,
		DS_ITEM39,
		DS_ITEM40
	) values (
		nr_seq_grupo_pa_w,
		vetor_w(1).nm_coluna_w,
		vetor_w(2).nm_coluna_w,
		vetor_w(3).nm_coluna_w,
		vetor_w(4).nm_coluna_w,
		vetor_w(5).nm_coluna_w,
		vetor_w(6).nm_coluna_w,
		vetor_w(7).nm_coluna_w,
		vetor_w(8).nm_coluna_w,
		vetor_w(9).nm_coluna_w,
		vetor_w(10).nm_coluna_w,
		vetor_w(11).nm_coluna_w,
		vetor_w(12).nm_coluna_w,
		vetor_w(13).nm_coluna_w,
		vetor_w(14).nm_coluna_w,
		vetor_w(15).nm_coluna_w,
		vetor_w(16).nm_coluna_w,
		vetor_w(17).nm_coluna_w,
		vetor_w(18).nm_coluna_w,
		vetor_w(19).nm_coluna_w,
		vetor_w(20).nm_coluna_w,
		vetor_w(21).nm_coluna_w,
		vetor_w(22).nm_coluna_w,
		vetor_w(23).nm_coluna_w,
		vetor_w(24).nm_coluna_w,
		vetor_w(25).nm_coluna_w,
		vetor_w(26).nm_coluna_w,
		vetor_w(27).nm_coluna_w,
		vetor_w(28).nm_coluna_w,
		vetor_w(29).nm_coluna_w,
		vetor_w(30).nm_coluna_w,
		vetor_w(31).nm_coluna_w,
		vetor_w(32).nm_coluna_w,
		vetor_w(33).nm_coluna_w,
		vetor_w(34).nm_coluna_w,
		vetor_w(35).nm_coluna_w,
		vetor_w(36).nm_coluna_w,
		vetor_w(37).nm_coluna_w,
		vetor_w(38).nm_coluna_w,
		vetor_w(39).nm_coluna_w,
		vetor_w(40).nm_coluna_w
		);



END LOOP;
CLOSE c01;

delete	from w_painel
where DS_ITEM1 is  null
and DS_ITEM2 is  null
and DS_ITEM3 is  null
and DS_ITEM4 is  null
and DS_ITEM5 is  null
and DS_ITEM6 is  null
and DS_ITEM7 is  null
and DS_ITEM8 is  null
and DS_ITEM9 is  null
and DS_ITEM10 is  null
and DS_ITEM11 is  null
and DS_ITEM12 is  null
and DS_ITEM13 is  null
and DS_ITEM14 is  null
and DS_ITEM15 is  null
and DS_ITEM16 is  null
and DS_ITEM17 is  null
and DS_ITEM18 is  null
and DS_ITEM19 is  null
and DS_ITEM20 is  null
and DS_ITEM21 is  null
and DS_ITEM22 is  null
and DS_ITEM23 is  null
and DS_ITEM24 is  null
and DS_ITEM25 is  null
and DS_ITEM26 is  null
and DS_ITEM27 is  null
and DS_ITEM28 is  null
and DS_ITEM29 is  null
and DS_ITEM30 is  null
and DS_ITEM31 is  null
and DS_ITEM32 is  null
and DS_ITEM33 is  null
and DS_ITEM34 is  null
and DS_ITEM35 is  null
and DS_ITEM36 is  null
and DS_ITEM37 is  null
and DS_ITEM38 is  null
and DS_ITEM39 is  null
and DS_ITEM40 is null;

commit;

end;
/

