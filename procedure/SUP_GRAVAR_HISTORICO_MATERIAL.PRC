CREATE OR REPLACE
procedure sup_gravar_historico_material(cd_estab_p	number,
				cd_material_p	number,
				ds_titulo_p	varchar2,
				ds_historico_p	varchar2,
				ie_tipo_p		varchar2,
				nm_usuario_p	varchar2) is

ds_historico_w				long;
nr_sequencia_w				number(10);

/* Caso Exista Formatação Negrito */
qt_existe_Format_w				number(10);
qt_existe_w		number(10);

begin

ds_historico_w	:= substr(ds_historico_p,1,4000);

select	instr(ds_historico_w,'{\ul{\b')
into	qt_existe_Format_w
from	dual;

if	(qt_existe_Format_w > 0) then
	ds_historico_w	:= substr('{\rtf1\ansi\deff0{\fonttbl ' ||
				'{\f0\fnil\fcharset0 Verdana;}{\f1\fnil Verdana;}}' ||
				'{\colortbl ;\red0\green0\blue128;}'||
				'\viewkind4\uc1\pard\cf1\lang1046\f0\fs20' ||
				ds_historico_w || '\b0 }',1,4000);
end if;

select	count(*)
into	qt_existe_w 
from 	material 
where 	cd_material = cd_material_p;

if	(qt_existe_w > 0) then
	
	select	material_historico_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into material_historico(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_historico,
		ds_historico_curto,
		cd_material,
		ds_titulo,
		cd_estabelecimento,
		ie_tipo)
	values(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_historico_w,
		substr(ds_historico_w,1,4000),
		cd_material_p,
		ds_titulo_p,
		decode(cd_estab_p,0,null,cd_estab_p),
		ie_tipo_p);
end if;
commit;

end sup_gravar_historico_material;
/