create or replace
procedure pls_criar_contas_proc_list
			(	ds_procedimentos_p	Varchar2,
				qt_procedimentos_p	Varchar2,
				ie_origem_proced_p	Varchar2,
				dt_procedimento_p	Varchar2,
				dt_hora_inicio_p	Varchar2,
				dt_hora_fim_p		Varchar2,
				ie_utiliza_data_p	Varchar2,
				nr_seq_conta_p		Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is
		
ds_procedimentos_w	Varchar2(255);		
cd_procedimento_w	Number(10);
ds_quantidade_w		Varchar2(255);
qt_procedimento_w	Number(10);
ds_origem_proced_w	Varchar2(255);
ie_origem_proced_w	Number(10);	
ie_permite_inserir_w	varchar2(1);
dt_procedimento_w	Date;
dt_hora_inicio_w	Date;
dt_hora_fim_w		Date;
dt_procedimento_ww	Varchar2(255);
dt_hora_inicio_ww	Varchar2(255);
dt_hora_fim_ww		Varchar2(255);
ds_utiliza_data_w	Varchar2(255);
ie_utiliza_data_w	Varchar2(2);
dt_inicio_aux_w			Varchar2(255);
dt_fim_aux_w		Varchar2(255);
dt_procedimento_aux_w	Varchar2(255);
	
/*
 A PRINCIPIO, ESTA ROTINA � CHAMADA APENAS DA DIGITA��O DE SP/SADT E DA DIGITA��O DE INTERNAC�ES.
*/
		
begin

ds_procedimentos_w 	:= ds_procedimentos_p;
ds_quantidade_w  	:= qt_procedimentos_p;
ds_origem_proced_w 	:= ie_origem_proced_p;
dt_procedimento_ww	:= dt_procedimento_p;
dt_hora_inicio_ww	:= dt_hora_inicio_p;
dt_hora_fim_ww		:= dt_hora_fim_p;
ds_utiliza_data_w	:= ie_utiliza_data_p;
				
while	(instr(ds_procedimentos_w,',') <> 0) loop
	begin		
	
	--obtem-se a primeira regra
	cd_procedimento_w 	:= substr(ds_procedimentos_w,1,instr(ds_procedimentos_w,',')-1);
	--obtem-se a quantidade do procedimento.	
	qt_procedimento_w 	:= substr(ds_quantidade_w,1,instr(ds_quantidade_w,',')-1);	
	--obtem-se a origem do procedimento
	ie_origem_proced_w 	:= substr(ds_origem_proced_w,1,instr(ds_origem_proced_w,',')-1);		
/*
	dt_hora_inicio_w  	:= to_date(substr(dt_hora_inicio_ww,1,instr(dt_hora_inicio_ww,',')-1),'hh:mi:ss');
	
	dt_hora_fim_w	  	:= to_date(substr(dt_hora_fim_ww,1,instr(dt_hora_fim_ww,',')-1),'hh:mi:ss');*/
	dt_procedimento_aux_w 	:= substr(dt_procedimento_ww,1,instr(dt_procedimento_ww,',')-1);
	
	dt_inicio_aux_w	  	:= substr(dt_hora_inicio_ww,1,instr(dt_hora_inicio_ww,',')-1);
	
	dt_fim_aux_w	:= substr(dt_hora_fim_ww,1,instr(dt_hora_fim_ww,',')-1);
	
	if	(nvl(trim(replace(dt_procedimento_aux_w,'/','')),'0')	<> '0') then
		begin
		dt_procedimento_w:= to_date(dt_procedimento_aux_w);
		exception
		when others then
			dt_procedimento_w := null;
		end;
	else
		dt_procedimento_w := null;
	end if;
	
	if	(nvl(trim(replace(dt_inicio_aux_w,':','')),'0')	 <> '0') then
		begin
		dt_hora_inicio_w:= to_date(dt_inicio_aux_w,'hh24:mi:ss');
		exception
		when others then
			dt_hora_inicio_w := null;
		end;
	else
		dt_hora_inicio_w := null;
	end if;
	
	if	(nvl(trim(replace(dt_fim_aux_w,':','')),'0')	 <> '0') then
		begin
		dt_hora_fim_w := to_date(dt_fim_aux_w,'hh24:mi:ss');
		exception
		when others then
			dt_hora_fim_w := null;
		end;
	else
		dt_hora_fim_w := null;
	end if;
	
	ie_utiliza_data_w  	:= substr(ds_utiliza_data_w,1,instr(ds_utiliza_data_w,',')-1);
	--remove-se o procedimento do conjunto
	ds_procedimentos_w 	:= substr(ds_procedimentos_w,instr(ds_procedimentos_w,',')+1,255);
	--remove-se a quantidade do conjunto
	ds_quantidade_w	  	:= substr(ds_quantidade_w,instr(ds_quantidade_w,',')+1,255);	
	--remove-se a origem do proced do conjunto
	ds_origem_proced_w   	:= substr(ds_origem_proced_w,instr(ds_origem_proced_w,',')+1,255);	

	dt_procedimento_ww   	:= substr(dt_procedimento_ww,instr(dt_procedimento_ww,',')+1,255);	
	
	dt_hora_inicio_ww   	:= substr(dt_hora_inicio_ww,instr(dt_hora_inicio_ww,',')+1,255);	
	
	dt_hora_fim_ww   	:= substr(dt_hora_fim_ww,instr(dt_hora_fim_ww,',')+1,255);	
	
	ds_utiliza_data_w 	:= substr(ds_utiliza_data_w,instr(ds_utiliza_data_w,',')+1,255);

	if	(nvl(ie_utiliza_data_w,'N') = 'N') then
		dt_procedimento_w 	:= null;
		dt_hora_inicio_w	:= null;
		dt_hora_fim_w		:= null;
	end if;
	pls_verifica_proc_existente(	nm_usuario_p,cd_estabelecimento_p,nr_seq_conta_p,
					cd_procedimento_w, ie_origem_proced_w,qt_procedimento_w,
					dt_procedimento_w,dt_hora_inicio_w, dt_hora_fim_w,
					ie_permite_inserir_w );
	/* SE NAO EXISTIR REGISTRO DE PROCEDIMENTO  COM MESMA QUANTIDADE , ENTAO INSERE O PROCEDIMENTO QUE ESTA INFORMADO NO FiLTRO DAS DIGITA��ES*/
	if	(ie_permite_inserir_w = 'S')  then		
		pls_gerar_conta_proc_consulta(	nr_seq_conta_p, cd_procedimento_w, ie_origem_proced_w,
						qt_procedimento_w, nm_usuario_p, cd_estabelecimento_p,
						dt_procedimento_w,dt_hora_inicio_w, dt_hora_fim_w);
	end if;
	
	end;	
end loop;

end pls_criar_contas_proc_list;
/
