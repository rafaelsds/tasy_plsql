create or replace PROCEDURE GERAR_DIAGNOSTICO_ATEND_RET(	nr_atendimento_p			number,
									cd_doenca_p					varchar2,
									cd_medico_p					varchar2,
									nm_usuario_p				varchar2,
									ie_tipo_diagnostico_p		number default 2,
									ie_classificacao_doenca_p	varchar2 default 'S',
									dt_liberacao_p				date default null,
                                    ie_tipo_doenca_p            varchar2 default null,
                                    ie_lado_p                   varchar2 default null,
                                    ds_diagnostico_p            varchar2 default null,
									nr_seq_interno_p			out number) is

qt_tempo_w          number(15);
ie_tipo_doenca_w    varchar2(2);
ie_unidade_tempo_w  varchar2(2);
dt_diagnostico_w	date;
nr_atendimento_ww   number(10);
cd_doenca_ww        varchar2(10);
ie_nivel_atencao_w  varchar2(1);
nr_seq_episodio_w	number(10);

begin

ie_nivel_atencao_w := wheb_assist_pck.get_nivel_atencao_perfil;

select	max(b.nr_atendimento),
		max(b.cd_doenca),
		max(qt_tempo),
		max(ie_tipo_doenca),
		max(ie_unidade_tempo)
into	nr_atendimento_ww,
		cd_doenca_ww,
		qt_tempo_w,
		ie_tipo_doenca_w,
		ie_unidade_tempo_w
from	diagnostico_doenca b,
		atendimento_paciente a
where	a.cd_pessoa_fisica = (	select	cd_pessoa_fisica
								from	atendimento_paciente
								where	rownum = 1
								and		nr_atendimento = nr_atendimento_p)
and		a.nr_atendimento = b.nr_atendimento
and		cd_doenca = cd_doenca_p;

select	max(dm.dt_diagnostico)
into	dt_diagnostico_w
from	diagnostico_medico dm
where	pkg_date_utils.start_of(dm.dt_diagnostico,'DD',0)	= pkg_date_utils.start_of(sysdate,'DD',0)
and		dm.nr_atendimento		= nr_atendimento_p
and		dm.cd_medico		= cd_medico_p
and		not exists (select	1
					from	diagnostico_doenca dd
					where	dd.dt_diagnostico = dm.dt_diagnostico
					and		dd.nr_atendimento = nr_atendimento_p
					and		dd.cd_doenca = cd_doenca_p
					and		dd.ie_situacao = 'I'
					and		dd.dt_inativacao is not null);

if	(dt_diagnostico_w is null or nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') and
	(cd_doenca_p is not null) then
	dt_diagnostico_w	:= sysdate;
	insert into diagnostico_medico(
					nr_atendimento,
					dt_diagnostico,
					ie_tipo_diagnostico,
					cd_medico,
					dt_atualizacao,
					nm_usuario,
					ds_diagnostico,
					ie_nivel_atencao
					) values (
					nr_atendimento_p,
					dt_diagnostico_w,
					nvl(ie_tipo_diagnostico_p,2),
					cd_medico_p,
					sysdate,
					nm_usuario_p,
					null,
					ie_nivel_atencao_w);
end if;

if	(cd_doenca_p is not null) then

	select 	max(nr_seq_episodio)
	into	nr_seq_episodio_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_p;

	select 	diagnostico_doenca_seq.nextval
	into	nr_seq_interno_p
	from 	dual;

	insert into diagnostico_doenca(
					nr_atendimento,
					dt_diagnostico,
					cd_doenca,
					dt_atualizacao,
					nm_usuario,
					ds_diagnostico,
					ie_classificacao_doenca,
					ie_tipo_diagnostico,
					dt_liberacao,
					ie_situacao,
					qt_tempo,
					ie_tipo_doenca,
					ie_unidade_tempo,
					ie_nivel_atencao,
					ie_relevante_drg,
					cd_medico,IE_DIAG_ADMISSAO,
                    ie_lado,
					nr_seq_interno)
	select	nr_atendimento_p,
			dt_diagnostico_w,
			cd_doenca_p,
			sysdate,
			nm_usuario_p,
			ds_diagnostico_p,
			nvl(ie_classificacao_doenca_p,'S'),
			nvl(ie_tipo_diagnostico_p,2),
			dt_liberacao_p,
			'A',
			qt_tempo_w,
			nvl(ie_tipo_doenca_p, ie_tipo_doenca_w),
			ie_unidade_tempo_w,
			ie_nivel_atencao_w,
			decode(obter_tipo_episodio(nr_seq_episodio_w),1,'S','N'),
			cd_medico_p, 'S',
            ie_lado_p,
			nr_seq_interno_p
	from	dual
	where	not exists(	select	1
						from	diagnostico_doenca
						where	rownum = 1
						and		nr_atendimento	= nr_atendimento_p
						and		cd_doenca	= cd_doenca_p
						and		dt_diagnostico	= dt_diagnostico_w);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_diagnostico_atend_ret;
/
