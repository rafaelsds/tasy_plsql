create or replace
procedure pls_excluir_beneficiarios_lote
			(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is

begin

delete from pls_segurado_preco
where	nr_seq_lote	= nr_sequencia_p;

delete from pls_segurado_preco_origem
where	nr_seq_lote_reajuste	= nr_sequencia_p;
 
update	pls_lote_reaj_segurado 
set	ie_status	= '1',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end pls_excluir_beneficiarios_lote;
/