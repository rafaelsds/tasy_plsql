create or replace
procedure pls_obter_valores_impug(	nr_seq_impugnacao_p	pls_impugnacao.nr_sequencia%type,
					ie_tipo_pedido_p	pls_proc_conta_pedido.ie_tipo_pedido%type,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_commit_p		varchar2 default 'N',
					ie_deferimento_p	varchar2,
					vl_ressarcir_p 		out number,
					vl_deferido_p		out number) is 

nr_seq_defesa_w		pls_impugnacao_defesa.nr_sequencia%type;
vl_deferido_w		pls_impugnacao.vl_deferido%type 	:= 0;
vl_ressarcir_w		pls_impugnacao.vl_ressarcir%type	:= 0;
nr_seq_proc_conta_w	pls_processo_conta.nr_sequencia%type;
ie_tipo_impugnacao_w	pls_processo_conta.ie_tipo_impugnacao%type;
ie_status_defesa_w	pls_impugnacao_defesa.ie_status%type;
vl_procedimentos_w	pls_processo_procedimento.vl_procedimento%type;
vl_conta_w		number(15,2);
vl_pedidos_w		pls_proc_conta_pedido.vl_pedido%type;
ie_tipo_pedido_w	pls_proc_conta_pedido.ie_tipo_pedido%type;

qt_defesas_deferidas_w		integer;
qt_defesas_indeferidas_w	integer;
qt_recursos_abertos_w		integer;
begin

nr_seq_defesa_w		:= pls_obter_ultima_impug_defesa(nr_seq_impugnacao_p);

if	(nvl(nr_seq_defesa_w,0) > 0) then

	select	nr_seq_conta
	into	nr_seq_proc_conta_w
	from	pls_impugnacao
	where	nr_sequencia	= nr_seq_impugnacao_p;
	
	select 	nvl(ie_tipo_impugnacao, 'C')
	into	ie_tipo_impugnacao_w
	from	pls_processo_conta
	where	nr_sequencia	= nr_seq_proc_conta_w;
	
	select	sum(vl_pedido),
		max(ie_tipo_pedido)
	into	vl_pedidos_w,
		ie_tipo_pedido_w
	from	pls_proc_conta_pedido
	where	nr_seq_proc_conta = nr_seq_proc_conta_w;
	
	vl_conta_w := pls_conta_processo_obter_valor(nr_seq_proc_conta_w);
	
	if	(ie_tipo_impugnacao_w = 'C') then
	
		-- verifica quantas defesas validas e deferidas a impugnacao tem, e quantos recursos em aberto
		select	sum(case when a.ie_status = 'D' then 1 else 0 end), -- qt deferidas
			sum(case when a.ie_status = 'I' then 1 else 0 end), -- qt indeferidas
			sum(case when ((a.ie_tipo_defesa = 'R') and (a.ie_status not in ('D', 'I'))) then 1 else 0 end) -- qt recurso aberto
		into	qt_defesas_deferidas_w,
			qt_defesas_indeferidas_w,
			qt_recursos_abertos_w			
		from	pls_impugnacao_defesa	a,
			pls_impugnacao		b
		where	a.nr_seq_impugnacao	= b.nr_sequencia
		and	b.nr_sequencia		= nr_seq_impugnacao_p
		and	a.dt_cancelamento is null;
			
		--	se possui alguma defesa valida deferida e n�o possui nenhum recurso pendente
		if	(qt_defesas_deferidas_w > 0) and (qt_recursos_abertos_w = 0)then
		
			-- � verificado se o pedido � referente a todo o atendimento ou ao valor informado no campo
			-- ser� necess�rio tratar a op��o 'x' pois nesta pode ser ambos os casos dependendo do retorno do SUS
			-- se for pedido de anula��o e foi deferido ent�o o valor a ressarcir � zero e o valor deferido � o total
			if	(ie_tipo_pedido_w = 'A') then
			
				vl_deferido_w	:= vl_conta_w;
				vl_ressarcir_w	:= 0;
				
			-- se for pedido Retifica��o e foi deferido ent�o o valor a ressarcir � o valor do pedido
			-- e o valor deferido � a diferen�a entre o pedido e o valor total
			elsif	(ie_tipo_pedido_w = 'R') then
			
				vl_deferido_w := vl_conta_w - vl_pedidos_w;
				vl_ressarcir_w := vl_pedidos_w;
				
			-- quando o tipo do pedido for RA ent�o o usu�rio informa no delphi qual foi a op��o deferida
			else
				if	(ie_tipo_pedido_p = 'A') then
				
					vl_deferido_w	:= vl_conta_w;
					vl_ressarcir_w	:= 0;
				elsif	(ie_tipo_pedido_p = 'R') then 
				
					vl_deferido_w := vl_conta_w - vl_pedidos_w;
					vl_ressarcir_w := vl_pedidos_w;
				end if;
			end if;

		--	se N�O possui alguma defesa valida deferida, mas possui defesa valida indeferida, e n�o possui nenhum recurso pendente
		--		o valor deferido � zerado
		--		o valor a ressarcir � o valor total da conta
		elsif	((qt_defesas_deferidas_w = 0) and 
		        (qt_defesas_indeferidas_w > 0)) and 
			(qt_recursos_abertos_w = 0) then

			vl_deferido_w	:= 0;
			vl_ressarcir_w	:= vl_conta_w;

		--	se POSSUI algum recurso pendente na defesa, ou ent�o n�o possui nenhuma defesa v�lida deferida/indeferida
		--		o valor deferido � zerado
		--		o valor a ressarcir � zerado
		elsif	(qt_recursos_abertos_w > 0) or 
		        ((qt_defesas_indeferidas_w = 0) and
			 (qt_defesas_deferidas_w = 0))then

			vl_deferido_w	:= 0;
			vl_ressarcir_w	:= 0;
		end if;
	elsif	(ie_tipo_impugnacao_w = 'P') then
	
		select	nvl(sum(nvl(a.vl_liberado,0)),0),
			nvl(sum(nvl(b.vl_procedimento,0)),0)
		into	vl_ressarcir_w,
			vl_procedimentos_w
		from	pls_processo_procedimento b,
			pls_impugnacao_proc a
		where	a.nr_seq_processo_proc	= b.nr_sequencia
		and	a.nr_seq_impugnacao	= nr_seq_impugnacao_p;
	
		if	(ie_deferimento_p = 'D') then
			vl_deferido_w	:= vl_procedimentos_w - vl_ressarcir_w;
			vl_ressarcir_w	:= vl_ressarcir_w;
		elsif	(ie_deferimento_p = 'I') then
			vl_deferido_w	:= 0;
			vl_ressarcir_w	:= vl_procedimentos_w;
		end if;
	
	end if;

end if;

-- Quando n�o tem nenhuma defesa, ou todas est�o canceladas
if	(nvl(nr_seq_defesa_w,0) = 0) then
	-- o valor do ressarcimento deve ser zerado
	-- o valor deferido � zerado
	vl_ressarcir_w	:= 0;
	vl_deferido_w	:= 0;	
end if;

vl_deferido_p 	:= vl_deferido_w;
vl_ressarcir_p 	:= vl_ressarcir_w;

end pls_obter_valores_impug;
/