create or replace
procedure ehro_valid_dispensation (	nr_cirurgia_p			in	number,
				cd_material_p			in	number,
				qt_dispensacao_p			in	number,
				ie_origem_gasto_bipada_p		in	varchar2,
				ds_questiona_alergia_p		out 	varchar2,
		                                ds_questiona_origem_gasto_p	out 	varchar2,
				ie_origem_padrao_p		out 	varchar2,
				ds_aviso_qtd_disp_p		out 	varchar2,
				ds_questiona_cavidade_p		out 	varchar2,
				ds_questiona_qtd_alta_p		out 	varchar2) is


cd_perfil_w				number(5);
nm_usuario_w				varchar2(15);
cd_estabelecimento_w			number(5);
ie_ajusta_origem_padrao_w			varchar2(15);
ie_consiste_qte_w				varchar2(15);
ie_perm_disp_nao_prescr_w			varchar2(15);
ie_disp_pepo_w				varchar2(15);
ds_material_w				varchar2(255);
ds_origem_gasto_bipada_w			varchar2(255);
ie_exibe_qt_min_w				varchar2(15);
nr_seq_proc_interno_w			number(10);
cd_pessoa_fisica_w			varchar2(10);
cd_material_w				number(6);
ds_item_w				varchar2(2000);
ie_retorno_w				varchar2(15);
ie_possui_regra_w				varchar2(15);
ie_permite_alterar_w			varchar2(15);
ds_origem_gasto_padrao_w			varchar2(255);
ie_origem_padrao_w			varchar2(15);
ds_inconsistencia_w			varchar2(2000);
ie_controla_cavidade_w			varchar2(15);
nr_seq_item_controle_w			number(10);

begin
cd_perfil_w			:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w			:= wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w		:= wheb_usuario_pck.get_cd_estabelecimento;


obter_param_usuario(872, 209, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_ajusta_origem_padrao_w);
obter_param_usuario(872, 429, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_consiste_qte_w);
obter_param_usuario(872, 510, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_perm_disp_nao_prescr_w);

cd_material_w	:= cd_material_p;

select		max(obter_se_material_disp_pepo(cd_material)),
		max(ds_material),
	         	max(obter_valor_dominio(1815,ie_origem_gasto_bipada_p))			
into		ie_disp_pepo_w,
		ds_material_w,
		ds_origem_gasto_bipada_w
from		material
where		cd_material = cd_material_w;

select		max(nr_seq_proc_interno),
		max(cd_pessoa_fisica)
into		nr_seq_proc_interno_w,
		cd_pessoa_fisica_w
from		cirurgia
where		nr_cirurgia = nr_cirurgia_p;

if	(ie_disp_pepo_w = 'N') then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(122797,DS_MATERIAL_W);
end if;

Verifica_Paciente_Alergico(cd_pessoa_fisica_w,cd_material_w,ds_item_w,ie_retorno_w);
if	(ie_retorno_w = 'S') and (ds_item_w is not null) then
	ds_questiona_alergia_p := obter_texto_dic_objeto(29405,philips_param_pck.get_nr_seq_idioma,'DS_ITEM_P=' || substr(ds_item_w,1,255));
end if;

Obter_Se_Regra_Disp_Pepo(cd_material_w,ie_origem_gasto_bipada_p,ie_possui_regra_w,ie_permite_alterar_w,ds_origem_gasto_padrao_w,ie_origem_padrao_w);
if	(ie_possui_regra_w = 'S') then
	if	(ie_permite_alterar_w = 'S') then
		ds_questiona_origem_gasto_p := obter_texto_dic_objeto(216894,philips_param_pck.get_nr_seq_idioma,'DS_ORIGEM=' ||ds_origem_gasto_padrao_w|| ';DS_RESP='||ds_origem_gasto_bipada_w);
   elsif	(ie_possui_regra_w = 'S') then
      if	(ie_ajusta_origem_padrao_w = 'N') then
         wheb_mensagem_pck.Exibir_Mensagem_Abort(obter_texto_dic_objeto(216902,philips_param_pck.get_nr_seq_idioma,'DS_ORIGEM=' ||ds_origem_gasto_padrao_w|| ';DS_RESP='||ds_origem_gasto_bipada_w));
      else
         ie_origem_padrao_p := ie_origem_padrao_w;
      end if;
   end if;
end if;

if	(ie_consiste_qte_w <> 'N') then
   consistir_qtde_dispensada(nr_cirurgia_p,cd_material_w,qt_dispensacao_p,ds_inconsistencia_w);
   if	(ds_inconsistencia_w is not null) then
      if	(ie_consiste_qte_w = 'S') then
         wheb_mensagem_pck.Exibir_Mensagem_Abort(ds_inconsistencia_w);
      elsif	(ie_consiste_qte_w = 'A')	then
         ds_aviso_qtd_disp_p	:= ds_inconsistencia_w;
      end if;
   end if;	
end if;

if	(ie_perm_disp_nao_prescr_w = 'N') then
	consistir_se_mat_prescr_cir(nr_cirurgia_p,cd_material_w,ds_inconsistencia_w);
end if;


select 	nvl(max(ie_controla_cavidade),'S')
into		ie_controla_cavidade_w
from   	proc_interno
where  	nr_sequencia = nr_seq_proc_interno_w;
	
if	(ie_controla_cavidade_w = 'S') then	
	select  	max(a.nr_seq_item)
	into	nr_seq_item_controle_w
	from    	material_item_controle a,
		pepo_item_controle b
	where   	a.nr_seq_item = b.nr_sequencia
	and     	nvl(b.ie_situacao,'A') = 'A'
	and 	((b.cd_estabelecimento = cd_estabelecimento_w) or (b.cd_estabelecimento is null))
	and     	a.cd_material = cd_material_w;

	if	(nr_seq_item_controle_w is not null) then
		ds_questiona_cavidade_p	:= obter_texto_dic_objeto(28096,philips_param_pck.get_nr_seq_idioma,null);
	end if;
end if;

if	(nvl(qt_dispensacao_p,1) > 10000) then
	ds_questiona_qtd_alta_p := obter_texto_dic_objeto(28237,philips_param_pck.get_nr_seq_idioma,null);
end if;

end ehro_valid_dispensation;
/
