Create or Replace
procedure Obter_Proced_sangue
		(ie_derivado_exame_p		in	number,
		nr_sequencia_p			in 	number,
		cd_estabelecimento_p		in	number,
		ie_tipo_atendimento_p		in	number,
		ie_tipo_convenio_p		in	number,
		cd_convenio_p			in 	number,
		cd_categoria_p			in	number,
		cd_setor_p			in out	number,
		cd_procedimento_p		in out	number,
		ie_origem_proced_p		in out	number,
		nr_seq_proc_interno_p		in out	number,
		ie_irradiado_p			in	varchar2 default 'N',
		ie_lavado_p			in	varchar2 default 'N',
		ie_filtrado_p			in	varchar2 default 'N',
		ie_aliquotado_p			in	varchar2 default 'N',
		cd_setor_prescricao_p	in  number default null) is

cd_procedimento_w		number(15);
cd_setor_exclusivo_w		number(05,0);
ie_origem_proced_w		number(10,0);
nr_seq_proc_interno_w		number(10);
cd_setor_atendimento_w		Number(5);
nr_seq_proc_interno_regra_w	number(10);
qt_exames_conta_w		number(5);

Cursor C01 is
select	a.cd_setor_atendimento
from	setor_atendimento b,
	proc_interno_setor a
where	a.nr_seq_proc_interno	= nr_seq_proc_interno_w
and	a.cd_setor_atendimento	= b.cd_setor_atendimento
and	nvl(a.cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
and 	nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0)) = nvl(ie_tipo_atendimento_p,0)
and	nvl(cd_perfil,obter_perfil_ativo) = obter_perfil_ativo
and	((cd_setor_origem	= cd_setor_prescricao_p) or (cd_setor_prescricao_p is null) or (cd_setor_origem is null))
order by a.ie_banco_sangue_rep,
	 nr_prioridade desc;


Cursor C02 is
	select	a.cd_setor_atendimento
	from	setor_atendimento b,
		procedimento_setor_atend a
	where	a.cd_setor_atendimento	= b.cd_setor_atendimento
	and	b.cd_estabelecimento_base = cd_estabelecimento_p
	and	a.cd_procedimento	= nvl(cd_procedimento_p,cd_procedimento_w)
	and	a.ie_origem_proced	= nvl(ie_origem_proced_p,ie_origem_proced_w)
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	order by a.ie_banco_sangue_rep, 
		ie_prioridade desc;
BEGIN




nr_seq_proc_interno_w		:= null;

if	(ie_derivado_exame_p = 0) then
	begin
	select	a.cd_procedimento,
		a.ie_origem_proced,
--		b.cd_setor_exclusivo,
		a.nr_seq_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
--		cd_setor_exclusivo_w,
		nr_seq_proc_interno_w
	from	san_derivado a
--		procedimento b
	where	a.nr_sequencia		= nr_sequencia_p;
--	and	a.cd_procedimento	= b.cd_procedimento
--	and	a.ie_origem_proced	= b.ie_origem_proced; -- Bruna OS49001

	select	max(b.cd_setor_exclusivo)
	into	cd_setor_exclusivo_w
	from	procedimento b,
		san_exame a
	where	a.nr_sequencia		= nr_sequencia_p
	and	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced;
	
	if	(cd_setor_exclusivo_w is null) then		
		select	max(c.cd_setor_exclusivo)
		into	cd_setor_exclusivo_w
		from	proc_interno b,
			san_exame a,
			procedimento c
		where	a.nr_sequencia		= nr_sequencia_p
		and	a.nr_seq_proc_interno	= b.nr_sequencia
		and	c.cd_procedimento	= b.cd_procedimento 
		and	c.ie_origem_proced	= b.ie_origem_proced;		
	end if;

	exception
	when others then
		cd_procedimento_w	:= null;
		ie_origem_proced_w	:= null;
		cd_setor_exclusivo_w	:= null;
	end;
	
	if	(cd_setor_exclusivo_w is null) then
		select	max(Obter_setor_Atend_proc(cd_estabelecimento_p, a.cd_procedimento, a.ie_origem_proced, null, null, null, a.nr_seq_proc_interno,null))
		into	cd_setor_exclusivo_w
		from	procedimento b,
			san_exame a
		where	a.nr_sequencia		= nr_sequencia_p
		and	a.cd_procedimento	= b.cd_procedimento
		and	a.ie_origem_proced	= b.ie_origem_proced;
	end if;
	
else
	begin
	
	select	max(a.cd_procedimento),
		max(a.ie_origem_proced),
		max(b.cd_setor_exclusivo)
	into	cd_procedimento_w,
		ie_origem_proced_w,
		cd_setor_exclusivo_w
	from	procedimento b,
		san_exame a
	where	a.nr_sequencia		= nr_sequencia_p
	and	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced;
	
	if 	(cd_procedimento_w is null) and 
		(ie_origem_proced_w is null) and
		(cd_setor_exclusivo_w is null) then
		
		select	max(b.cd_procedimento),
			max(b.ie_origem_proced),
			max(c.cd_setor_exclusivo),
			max(a.nr_seq_proc_interno)
		into	cd_procedimento_w,
			ie_origem_proced_w,
			cd_setor_exclusivo_w,
			nr_seq_proc_interno_w
		from	proc_interno b,
			san_exame a,
			procedimento c
		where	a.nr_sequencia		= nr_sequencia_p
		and	a.nr_seq_proc_interno	= b.nr_sequencia
		and	c.cd_procedimento	= b.cd_procedimento 
		and	c.ie_origem_proced	= b.ie_origem_proced ;
		
	end if;
	
	exception
	when others then
		cd_procedimento_w	:= null;
		ie_origem_proced_w	:= null;
		cd_setor_exclusivo_w	:= null;
	end;
end if;



Obter_San_Proced_Convenio(ie_derivado_exame_p, nr_sequencia_p,cd_estabelecimento_p, ie_tipo_atendimento_p,
			 ie_tipo_convenio_p, cd_convenio_p, cd_categoria_p, cd_setor_exclusivo_w, cd_procedimento_w, 
			 ie_origem_proced_w, 0, nr_seq_proc_interno_w,sysdate,nr_seq_proc_interno_regra_w,qt_exames_conta_w,
			 ie_irradiado_p,ie_lavado_p,ie_filtrado_p,ie_aliquotado_p,'N','N');

	 
			 
if	(nr_seq_proc_interno_w > 0) then
	begin
	open C01;
	loop
		fetch C01 into
		cd_setor_atendimento_w;
	exit when C01%notfound;
		cd_setor_atendimento_w	:= cd_setor_atendimento_w;
	end loop;
	close C01;
	end;
else
	begin
	open C02;
	loop
		fetch C02 into
		cd_setor_atendimento_w;
	exit when C02%notfound;
		cd_setor_atendimento_w	:= cd_setor_atendimento_w;
	end loop;
	close C02;
	end;
end if;
if	(cd_setor_atendimento_w > 0) then
	cd_setor_exclusivo_w	:= cd_setor_atendimento_w;
end if;


cd_setor_p		:= nvl(cd_setor_exclusivo_w, cd_setor_p);
cd_procedimento_p	:= nvl(cd_procedimento_w, cd_procedimento_p);
ie_origem_proced_p	:= nvl(ie_origem_proced_w, ie_origem_proced_p);
nr_seq_proc_interno_p	:= nvl(nr_seq_proc_interno_regra_w,nr_seq_proc_interno_w);

end Obter_Proced_sangue;
/
