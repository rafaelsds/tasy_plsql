create or replace
procedure deletar_resultados_aval_autor (
											nr_seq_autor_conv_aval_p	number
										) is 

begin

if (nvl(nr_seq_autor_conv_aval_p,0) > 0) then

	delete from autorizacao_conv_av_result
	where  nr_seq_autor_conv_aval  = nr_seq_autor_conv_aval_p;

	commit;

end if;

end deletar_resultados_aval_autor;
/