create or replace
procedure SRHOPPE_TESTE (
	reg_integracao_p 		in out	gerar_int_padrao.reg_integracao_conv,
	nm_atributo_p		in	varchar2,
	ds_valor_p		in	varchar2,
	ie_conversao_p		in	varchar2,
	ds_valor_retorno_p		in out	varchar2)  is

/*'
NR_SEQ_FILA_TRANSMISSAO	INTPD_FILA_TRANSMISSAO.NR_SEQUENCIA%TYPE,
IE_ENVIO_RECEBE		VARCHAR2(1),
IE_SISTEMA_EXTERNO		VARCHAR2(15),
NM_TABELA			VARCHAR2(30),
IE_CONVERSAO			VARCHAR2(1),
NR_SEQ_VISAO			TABELA_VISAO.NR_SEQUENCIA%TYPE,
NR_SEQ_PROJETO_XML		XML_PROJETO.NR_SEQUENCIA%TYPE,
NR_SEQ_REGRA_CONVERSAO	NUMBER(10),
NM_ELEMENTO			XML_ELEMENTO.NM_ELEMENTO%TYPE,
QT_REG_LOG			NUMBER(10),
INTPD_LOG_RECEB		REG_INTPD_LOG_RECEB
'*/

nr_sequencia_w		intpd_log_recebimento.nr_sequencia%type;
ie_obrigatorio_w		xml_atributo.ie_obrigatorio%type	:=	'N';
nm_tabela_referencia_w	integridade_referencial.nm_tabela_referencia%type;
nm_atributo_w		indice_atributo.nm_atributo%type;
cd_dominio_w		dominio.cd_dominio%type;
qt_registros_w		number(10);
ie_consistir_fk_w		varchar2(1)	:=	'N';
ds_sql_w			varchar2(2000);
ds_mascara_w		xml_atributo.ds_mascara%type;

nm_elemento_w		xml_elemento.nm_elemento%type;
nm_atributo_xml_w		xml_atributo.nm_atributo_xml%type;
ie_tipo_atributo_w		VARCHAR2(255);
ie_erro_w			varchar2(1);
ds_erro_w		varchar2(4000);
ds_ret_w			varchar2(4000);

ds_valor_w		varchar2(4000);
ds_valor_retorno_w		varchar2(4000);

ie_sequencia_criacao_w	integridade_atributo.ie_sequencia_criacao%type;

type r_vet_err is record (
	ie_tipo			varchar2(1),
	ds_erro			varchar2(4000),
	cd_default_message	varchar(4));

type t_vet_err is table of r_vet_err index by binary_integer;

vet_err_w	t_vet_err;
i 			integer;
j 			integer;

cursor c01 is
select	a.nr_sequencia,
	a.nm_elemento,
	a.nm_atributo,
	a.ds_valor,
	a.ie_situacao,
	a.ie_vazio,
	a.ie_conversao,
	a.ie_invalido,
	a.ie_manter_valor
from	intpd_eventos_valores a,
	intpd_fila_transmissao b
where	b.nr_sequencia = reg_integracao_p.nr_seq_fila_transmissao
and	b.nr_seq_evento_sistema = a.nr_seq_evento_sistema
and	nvl(a.nm_elemento,reg_integracao_p.nm_tabela)	= reg_integracao_p.nm_tabela
and	nvl(a.nm_atributo,nm_atributo_p)	= nm_atributo_p
and	a.ie_situacao = 'A'
and	((a.ie_vazio = 'S' and vet_err_w(j).ie_tipo = 'V') or
	(a.ie_invalido = 'S' and vet_err_w(j).ie_tipo = 'I') or
	(a.ie_conversao = 'S' and vet_err_w(j).ie_tipo = 'C') or
	(a.ie_manter_valor = 'S'))
order by a.nm_atributo asc, a.nm_elemento asc;

c01_w	c01%rowtype;

cursor c02 is
select	b.nr_sequencia,
	b.ds_valor_new
from	intpd_reg_alter_fila a,
	intpd_reg_alter_campo b,
	intpd_reg_alteracao c
where	a.nr_seq_reg_alter = b.nr_sequencia
and	a.nr_seq_reg_alter  = c.nr_sequencia
and	a.nr_seq_fila = reg_integracao_p.nr_seq_fila_transmissao
and	b.nm_atributo = nm_atributo_p
and	c.nm_tabela = reg_integracao_p.nm_tabela;

c02_w	c02%rowtype;

procedure gerar_log(nr_seq_fila_p	in	number,
		ds_log_p		in	varchar2) is

pragma autonomous_transaction;
begin
intpd_gravar_log_recebimento( nr_seq_fila_p,ds_log_p, 'Tasy');
commit;
end gerar_log;

procedure gerar_log_vetor(
		ie_tipo_p			in	varchar2,
		cd_default_message_p	in	varchar2,
		ds_erro_p			in	varchar2,
		vet_err_p			in out	t_vet_err) is

i	integer;

begin
i				:=	vet_err_p.count;
vet_err_p(i).ie_tipo 			:=	ie_tipo_p;
vet_err_p(i).cd_default_message	:=	cd_default_message_p;
vet_err_p(i).ds_erro 			:=	substr(ds_erro_p, 1, 4000);
end gerar_log_vetor;


begin
/*'Limpa o vetor de erros'*/
vet_err_w.delete;

ds_ret_w			:= 	ds_valor_retorno_p;
ds_valor_retorno_w		:=	ds_valor_retorno_p;
ds_valor_w		:=	ds_valor_p;

if	(reg_integracao_p.qt_reg_log is null) then
	reg_integracao_p.qt_reg_log	:=	0;
end if;

/*'Busca definicoes do atributo do XML'*/
if	(reg_integracao_p.nr_seq_projeto_xml > 0) then
	begin
	select	nvl(a.ds_grupo, a.nm_elemento),
		nvl(b.ie_obrigatorio,'N'),
		nvl(b.nm_atributo_xml,'X'),
		b.ds_mascara,
		b.ie_tipo_atributo
	into	nm_elemento_w,
		ie_obrigatorio_w,
		nm_atributo_xml_w,
		ds_mascara_w,
		ie_tipo_atributo_w
	from	xml_elemento a,
		xml_atributo b
	where	a.nr_sequencia = b.nr_seq_elemento
	and	a.nr_seq_projeto = reg_integracao_p.nr_seq_projeto_xml
	and	(upper(a.nm_elemento) like upper(reg_integracao_p.nm_elemento) or upper(a.ds_grupo) like upper(reg_integracao_p.nm_elemento))
	and	(upper(b.nm_atributo) like upper(nm_atributo_p) or upper(b.nm_atributo_xml) like upper(nm_atributo_p))
	and	rownum = 1;
	exception
	when others then
		begin
		select	nvl(a.ds_grupo, a.nm_elemento),
			nvl(b.ie_obrigatorio,'N'),
			nvl(b.nm_atributo_xml,'X'),
			b.ds_mascara,
			b.ie_tipo_atributo
		into	nm_elemento_w,
			ie_obrigatorio_w,
			nm_atributo_xml_w,
			ds_mascara_w,
			ie_tipo_atributo_w
		from	xml_elemento a,
			xml_atributo b
		where	a.nr_sequencia = b.nr_seq_elemento
		and	a.nr_seq_projeto = reg_integracao_p.nr_seq_projeto_xml
		and	(upper(a.nm_elemento) like upper(reg_integracao_p.nm_elemento) or upper(a.ds_grupo) like upper(reg_integracao_p.nm_elemento))
		and	(upper(b.nm_atributo) like upper(reg_integracao_p.nm_atributo_xml) or upper(b.nm_atributo_xml) like upper(reg_integracao_p.nm_atributo_xml))
		and	rownum = 1;
		exception
		when others then
			begin
			if	(obter_se_integracao_ish(reg_integracao_p.nr_seq_projeto_xml) = 'S') then
				select	nvl(a.ds_grupo, a.nm_elemento),
					nvl(b.ie_obrigatorio,'N'),
					nvl(b.nm_atributo_xml,'X'),
					b.ds_mascara,
					b.ie_tipo_atributo
				into	nm_elemento_w,
					ie_obrigatorio_w,
					nm_atributo_xml_w,
					ds_mascara_w,
					ie_tipo_atributo_w
				from	xml_elemento a,
					xml_atributo b
				where	a.nr_sequencia = b.nr_seq_elemento
				and	a.nr_seq_projeto = reg_integracao_p.nr_seq_projeto_xml
				and	(upper(replace(a.nm_elemento,'_',null)) like upper(replace(reg_integracao_p.nm_elemento,'_',null)) or
						upper(replace(a.ds_grupo,'_',null)) like upper(replace(reg_integracao_p.nm_elemento,'_',null)))
				and	(upper(replace(b.nm_atributo,'_',null)) like upper(replace(reg_integracao_p.nm_atributo_xml,'_',null)) or
						upper(replace(b.nm_atributo_xml,'_',null)) like upper(replace(reg_integracao_p.nm_atributo_xml,'_',null)))
				and	rownum = 1;
			end if;
			exception
			when others then
				null;
			end;
		end;
	end;
elsif	(reg_integracao_p.cd_interface > 0) then
	begin
	nm_elemento_w		:=	reg_integracao_p.nm_elemento;
	nm_atributo_xml_w		:=	nm_atributo_p;
	end;
end if;

if	((reg_integracao_p.ie_obrigatoriedade_tabela = 'S') and (ie_obrigatorio_w = 'N') and (reg_integracao_p.ie_envio_recebe = 'R')) then
	begin
	select	nvl(ie_obrigatorio,'N')
	into	ie_obrigatorio_w
	from	tabela_atributo
	where	nm_tabela = reg_integracao_p.nm_tabela
	and	nm_atributo = nm_atributo_p;
	exception
	when others then
		ie_obrigatorio_w	:=	'N';
	end;
end if;

nm_elemento_w		:=	nvl(nm_elemento_w, reg_integracao_p.nm_tabela);
nm_atributo_xml_w		:=	nvl(nm_atributo_xml_w, nm_atributo_p);

/*'Busca definicao da constraint para validar o valor da FK'*/
if	(ie_conversao_p <> 'N') then
	begin
	ie_consistir_fk_w		:=	'N';
	nm_tabela_referencia_w	:=	reg_integracao_p.nm_tabela;
	nm_atributo_w		:=	nm_atributo_p;

	loop
		begin
		begin
		/*'Busca o nome da tabela_referencia'*/
		select	a.nm_tabela_referencia,
			b.ie_sequencia_criacao
		into	nm_tabela_referencia_w,
			ie_sequencia_criacao_w
		from	integridade_referencial a,
			integridade_atributo b
		where	a.nm_tabela = b.nm_tabela
		and	a.nm_integridade_referencial = b.nm_integridade_referencial
		and	a.nm_tabela = nm_tabela_referencia_w
		and	b.nm_atributo = nm_atributo_w
		and	rownum = 1;
		exception
		when others then
			exit;
		end;

		/*'Busca o nome do atributo que e PK da tabela referencia'*/
		select	b.nm_atributo
		into	nm_atributo_w
		from	indice a,
			indice_atributo b
		where	a.nm_tabela = b.nm_tabela
		and	a.nm_indice = b.nm_indice
		and	a.nm_tabela = nm_tabela_referencia_w
		and	b.nr_sequencia = ie_sequencia_criacao_w
		and	a.ie_tipo = 'PK';

		ie_consistir_fk_w	:=	'S';
		end;
	end loop;

	if	(ie_consistir_fk_w = 'N') then
		begin
		nm_tabela_referencia_w	:=	reg_integracao_p.nm_tabela;
		nm_atributo_w		:=	nm_atributo_p;
		end;
	end if;
	end;
end if;

if	(reg_integracao_p.ie_envio_recebe = 'R') then
	begin
	if	(nm_atributo_p like 'DT%') and
		(nvl(ds_mascara_w,'X') <> 'X') then
		begin
		ds_mascara_w	:=	upper(ds_mascara_w);
		ds_mascara_w	:=	replace(ds_mascara_w,'HH:MI:SS','HH24:MI:SS');
		ds_mascara_w	:=	replace(ds_mascara_w,'HHMISS','HH24MISS');
		ds_valor_w	:=	to_char(to_date(ds_valor_w,ds_mascara_w));
		exception
		when others then
			ds_valor_w	:=	ds_valor_w;
		end;
	elsif	(ie_tipo_atributo_w = 'NUMBER') and
		(nvl(ds_mascara_w,'X') <> 'X') then
		begin
		ds_mascara_w	:=	replace(ds_mascara_w,'#','9');
		ds_valor_w	:=	to_number(ds_valor_w, ds_mascara_w);
		exception
		when others then
			ds_valor_w	:=	ds_valor_w;
		end;
	end if;

	/*'Efetua conversao de dados'*/
	if	(nvl(ds_valor_w,'NULL') <> 'NULL') and (ie_conversao_p <> 'N') and (reg_integracao_p.ie_conversao = 'I') then
		begin
		ds_valor_retorno_w	:=	bkf_obter_conv_interna(null, nm_tabela_referencia_w, nm_atributo_w, ds_valor_w, null, reg_integracao_p.nr_seq_regra_conversao);
		/*'Caso nao encontrar conversao no meio externo e a referencia for Pessoa Fisica, efetua conversao pela pf_codigo_externo'*/

		if	(nvl(ds_valor_retorno_w,'NULL') = 'NULL') and
			((nm_atributo_w like 'CD_MEDICO%') or (nm_atributo_w like 'CD_PESSOA%')) then
			if	(ie_conversao_p in ('ISH','ISHMED')) then
				begin
				select	cd_pessoa_fisica
				into	ds_valor_retorno_w
				from	pf_codigo_externo
				where	cd_pessoa_fisica_externo 	= ds_valor_w
                                and     nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
				and	ie_tipo_codigo_externo 	= ie_conversao_p;
				exception
				when others then
					ds_valor_retorno_w	:=	null;
				end;
			end if;

			if	(nvl(ds_valor_retorno_w,'NULL') = 'NULL') then
				begin
				select	cd_pessoa_fisica
				into	ds_valor_retorno_w
				from	pf_codigo_externo
				where	cd_pessoa_fisica_externo = ds_valor_w
                                and     nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
				exception
				when others then
					ds_valor_retorno_w	:=	null;
				end;
			end if;
		end if;

		/*'Quarda o log caso nao encontrar a conversao para o valor recebido'*/
		if	(nvl(ds_valor_retorno_w,'NULL') = 'NULL') then
			begin
			reg_integracao_p.ie_usou_valor_padrao	:= 'S';
			gerar_log_vetor('C', '0001',  substr(	wheb_mensagem_pck.get_texto(736986,
							'DS_ELEMENTO='||nm_elemento_w ||
							';DS_ATRIBUTO='||nm_atributo_xml_w ||
							';NR_SEQ_REGRA='||reg_integracao_p.nr_seq_regra_conversao||
							';NM_TABELA='||nm_tabela_referencia_w||
							';NM_ATRIBUTO='||nm_atributo_w||
							';DS_VALOR='||ds_valor_w),1,4000),
							vet_err_w);
			end;
		end if;
		end;
	elsif	(nvl(ds_valor_w,'NULL') <> 'NULL') then
		ds_valor_retorno_w	:=	ds_valor_w;
	elsif	(nvl(ds_valor_w,'NULL') = 'NULL') and (nvl(ds_valor_retorno_w,'NULL') <> 'NULL') then
		ds_valor_retorno_w	:=	ds_valor_retorno_w;
	elsif	(nvl(ds_valor_w,'NULL') = 'NULL') then
		begin
		begin
		select	max(vl_padrao)
		into	ds_valor_retorno_w
		from	tabela_visao_atributo
		where	nr_sequencia = reg_integracao_p.nr_seq_visao
		and	nm_atributo = nm_atributo_p
		and	vl_padrao not like '@%';
		exception
		when others then
			ds_valor_retorno_w := null;
		end;

		if	(lower(ds_valor_retorno_w) = 'sysdate') then
			ds_valor_retorno_w	:=	to_char(sysdate);
		end if;

		if	(nvl(ds_valor_w,'NULL') = 'NULL') and
			(ie_obrigatorio_w = 'S') then
			begin
			gerar_log_vetor('V', '0002',  substr(	wheb_mensagem_pck.get_texto(736987,
							'DS_ELEMENTO='||nm_elemento_w ||
							';DS_ATRIBUTO='||nm_atributo_xml_w),1,4000),
							vet_err_w);
			end;
		end if;
		end;
	end if;

	if	(nvl(ds_valor_retorno_w,'NULL') <> 'NULL') then
		begin
		select	nvl(max(cd_dominio),0)
		into	cd_dominio_w
		from	tabela_visao_atributo
		where	nr_sequencia = reg_integracao_p.nr_seq_visao
		and	nm_atributo = nm_atributo_p;

		if	(cd_dominio_w > 0) then
			begin
			select	count(*)
			into	qt_registros_w
			from	intpd_valor_dominio_v
			where	cd_dominio = cd_dominio_w
			and	vl_dominio = ds_valor_retorno_w;

			if	(qt_registros_w = 0) then
				begin
				gerar_log_vetor('I', '0003',  substr(	wheb_mensagem_pck.get_texto(736988,
								'DS_ELEMENTO='||nm_elemento_w ||
								';DS_ATRIBUTO='||nm_atributo_xml_w ||
								';CD_DOMINIO='||cd_dominio_w||
								';DS_VALOR='||ds_valor_retorno_w),1,4000),
								vet_err_w);
				end;
			end if;
			end;
		end if;

		if	(ie_consistir_fk_w = 'S') then
			begin
			begin
			ds_sql_w := 'select count(1) qt_reg from ' || nm_tabela_referencia_w || ' where ' || nm_atributo_w || ' = :ds_valor ';
			execute immediate ds_sql_w into qt_registros_w using ds_valor_retorno_w;
			exception
			when others then
				qt_registros_w	:=	0;
			end;

			if	(qt_registros_w = 0) then
				begin
				gerar_log_vetor('R', '0004',  substr(	wheb_mensagem_pck.get_texto(736989,
								'DS_ELEMENTO='||nm_elemento_w ||
								';DS_ATRIBUTO='||nm_atributo_xml_w ||
								';NM_TABELA_REFERENCIA='||nm_tabela_referencia_w||
								';DS_VALOR='||ds_valor_retorno_w),1,4000),
								vet_err_w);
				end;
			end if;
			end;
		end if;
		end;
	end if;

	begin
	ds_valor_retorno_p	:=	ds_valor_retorno_w;
	exception
	when others then
		gerar_log_vetor('I', '0005',  substr(	wheb_mensagem_pck.get_texto(1059154,
						'DS_ELEMENTO='||nm_elemento_w ||
						';DS_ATRIBUTO='||nm_atributo_xml_w ||
						';NM_TABELA='||nm_tabela_referencia_w||
						';NM_ATRIBUTO='||nm_atributo_w||
						';DS_VALOR='||ds_valor_retorno_w),1,4000),
						vet_err_w);
	end;

	if 	(vet_err_w.count > 0) then
		for i in 0..vet_err_w.count-1 loop
			begin
			if 	(nvl(vet_err_w(i).ie_tipo,'NULL') <> 'NULL') and (nvl(vet_err_w(i).ds_erro,'NULL') <> 'NULL') then
				begin
				j := i;

				if	(reg_integracao_p.ie_regra_valor_default = 'S') then
					open c01;
					loop
					fetch c01 into
						c01_w;
					exit when c01%notfound;
						begin
						if	(nvl(c01_w.ie_manter_valor,'N') = 'S') then
							ds_valor_retorno_p 	:=	ds_ret_w;
						else
							begin
							ds_valor_retorno_p	:=	c01_w.ds_valor;

							if	(nvl(c01_w.ds_valor,'NULL') <> 'NULL') then
								begin
								select	nvl(max(cd_dominio),0)
								into	cd_dominio_w
								from	tabela_visao_atributo
								where	nr_sequencia = reg_integracao_p.nr_seq_visao
								and	nm_atributo = nm_atributo_p;

								if	(cd_dominio_w > 0) then
									begin
									select	count(*)
									into	qt_registros_w
									from	intpd_valor_dominio_v
									where	cd_dominio = cd_dominio_w
									and	vl_dominio = c01_w.ds_valor;

									if	(qt_registros_w = 0) then
										begin

										gerar_int_padrao.incluir_log(
											reg_integracao_p,
											substr(	wheb_mensagem_pck.get_texto(736988,
												'DS_ELEMENTO='||nm_elemento_w ||
												';DS_ATRIBUTO='||nm_atributo_xml_w ||
												';CD_DOMINIO='||cd_dominio_w||
												';DS_VALOR='||c01_w.ds_valor),1,4000),
											'0003',
											'E');
										end;
									end if;
									end;
								end if;

								if	(ie_consistir_fk_w = 'S') then
									begin
									begin
									ds_sql_w := 'select count(1) qt_reg from ' || nm_tabela_referencia_w || ' where ' || nm_atributo_w || ' = :ds_valor ';
									execute immediate ds_sql_w into qt_registros_w using c01_w.ds_valor;
									exception
									when others then
										qt_registros_w	:=	0;
									end;

									if	(qt_registros_w = 0) then
										begin
										gerar_int_padrao.incluir_log(
											reg_integracao_p,
											substr(	wheb_mensagem_pck.get_texto(736989,
												'DS_ELEMENTO='||nm_elemento_w ||
												';DS_ATRIBUTO='||nm_atributo_xml_w ||
												';NM_TABELA_REFERENCIA='||nm_tabela_referencia_w||
												';DS_VALOR='||c01_w.ds_valor),1,4000),
											'0004',
											'E');
										end;
									end if;
									end;
								end if;
								end;
							end if;
							end;
						end if;

						gerar_int_padrao.incluir_log(	reg_integracao_p,
										substr(wheb_mensagem_pck.get_texto(1059105,
										'DS_ATRIBUTO='||nm_atributo_xml_w ||
										';DS_ELEMENTO='||nm_elemento_w ||
										';DS_VALOR_ORIGINAL='||ds_ret_w||
										';DS_VALOR_REGRA='||c01_w.ds_valor||
										';NR_SEQ_REGRA='||c01_w.nr_sequencia),1,4000),
										null,
										'W');
						exit;
						end;
					end loop;
					close c01;
				end if;

				if	((reg_integracao_p.ie_regra_valor_default = 'N') or (c01_w.nr_sequencia is null)) then
					begin
					gerar_int_padrao.incluir_log(	reg_integracao_p,
									vet_err_w(i).ds_erro,
									vet_err_w(i).cd_default_message,
									'E');
					end;
				end if;
				end;
			end if;
			end;
		end loop;
	end if;
	end;
elsif	(reg_integracao_p.ie_envio_recebe = 'E') then
	begin
	if	(nvl(reg_integracao_p.ie_somente_alterados,'X') = 'S') then
		begin
		select	max(ie_tipo_atributo)
		into	ie_tipo_atributo_w
		from	tabela_atributo
		where	nm_tabela = reg_integracao_p.nm_tabela
		and	nm_atributo = nm_atributo_p;

		open c02;
		loop
		fetch c02 into
			c02_w;
		exit when c02%notfound;
		end loop;
		close c02;

		if	(reg_integracao_p.ie_campo_verificador = 'N') then
			begin
			if	(ie_tipo_atributo_w = 'DATE') then
				ds_valor_w	:=	to_char(to_date(C02_w.ds_valor_new,'yyyy/mm/dd hh24:mi:ss'));
			else
				ds_valor_w	:=	to_char(C02_w.ds_valor_new);
			end if;

			if	((nvl(ds_valor_w,'NULL') = 'NULL') and (ie_obrigatorio_w = 'S')) then
				ds_valor_w	:=	ds_valor_p;
			end if;
			end;
		elsif	(reg_integracao_p.ie_campo_verificador = 'S') then
			if	((c02_w.nr_sequencia is not null) or (ie_obrigatorio_w = 'S')) then
				ds_valor_w	:=	reg_integracao_p.ie_marca_verif;
			else
				ds_valor_w	:=	null;
			end if;
		end if;
		end;
	else
		ds_valor_w	:=	ds_valor_p;
	end if;

	if	((nvl(ds_valor_w,'NULL') <> 'NULL') and (nvl(ie_conversao_p,'X') <> 'N') and (nvl(reg_integracao_p.ie_campo_verificador,'X') <> 'S')) then
		begin
		ds_valor_retorno_w	:=	intpd_conv(nm_tabela_referencia_w, nm_atributo_w, ds_valor_w, reg_integracao_p.nr_seq_regra_conversao, reg_integracao_p.ie_conversao, ie_conversao_p);

		if	(nvl(ds_valor_retorno_w,'NULL') = 'NULL') and
			((nm_atributo_w like 'CD_MEDICO%') or (nm_atributo_w like 'CD_PESSOA%')) then
			if	(ie_conversao_p in ('ISH','ISHMED')) then
				begin
				select	cd_pessoa_fisica_externo
				into	ds_valor_retorno_w
				from	pf_codigo_externo
				where	cd_pessoa_fisica 		= ds_valor_w
                                and     nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
				and	ie_tipo_codigo_externo 	= ie_conversao_p;
				exception
				when others then
					ds_valor_retorno_w	:=	null;
				end;
			end if;

			if	(nvl(ds_valor_retorno_w,'NULL') = 'NULL') then
				begin
				select	cd_pessoa_fisica_externo
				into	ds_valor_retorno_w
				from	pf_codigo_externo
				where	cd_pessoa_fisica = ds_valor_w
                                and     nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
				exception
				when others then
					ds_valor_retorno_w	:=	null;
				end;
			end if;
		end if;

		/*'Quarda o log caso nao encontrar a conversao para o valor recebido'*/
		if	(nvl(ds_valor_retorno_w,'NULL') = 'NULL') then
			begin
			gerar_int_padrao.incluir_log(
				reg_integracao_p,
				substr(	wheb_mensagem_pck.get_texto(736986,
					'DS_ELEMENTO='||nm_elemento_w ||
					';DS_ATRIBUTO='||nm_atributo_xml_w ||
					';NR_SEQ_REGRA='||reg_integracao_p.nr_seq_regra_conversao||
					';NM_TABELA='||nm_tabela_referencia_w||
					';NM_ATRIBUTO='||nm_atributo_w||
					';DS_VALOR='||ds_valor_w),1,4000),
				'0001',
				'E');
			end;
		end if;
		end;
	else
		ds_valor_retorno_w	:=	ds_valor_w;
	end if;

	if	((nvl(ds_valor_retorno_w,'NULL') = 'NULL') and (ie_obrigatorio_w = 'S')) then
		gerar_int_padrao.incluir_log(
			reg_integracao_p,
			substr(	wheb_mensagem_pck.get_texto(736987,
				'DS_ELEMENTO='||nm_elemento_w ||
				';DS_ATRIBUTO='||nm_atributo_xml_w),1,4000),
			null,
			'E');
	end if;

	begin
	ds_valor_retorno_p	:=	ds_valor_retorno_w;
	exception
	when others then
		begin
		ds_valor_retorno_p	:=	null;

		gerar_int_padrao.incluir_log(
			reg_integracao_p,
			substr(	wheb_mensagem_pck.get_texto(1059154,
						'DS_ELEMENTO='||nm_elemento_w ||
						';DS_ATRIBUTO='||nm_atributo_xml_w ||
						';NM_TABELA='||nm_tabela_referencia_w||
						';NM_ATRIBUTO='||nm_atributo_w||
						';DS_VALOR='||ds_valor_retorno_w),1,4000),
			'0005',
			'E');
		end;
	end;
	end;
end if;

end SRHOPPE_TESTE;
/
