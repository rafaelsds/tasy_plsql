create or replace
procedure gerar_proced_coparticipacao(	nr_seq_autorizacao_p	number,
					vl_coparticipacao_p	varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

nr_atendimento_w	number(10);	
cd_convenio_w		number(5);	
cd_categoria_w		varchar2(20);
cd_procedimento_w	number(10);	
ie_origem_proced_w	number(5);
nr_seq_atepacu_w	number(10);
cd_setor_atendimento_w	number(10);
nr_sequencia_w		number(10);
vl_coparticipacao_w	number(10,2);
ie_tipo_atendimento_w	number(10);

vl_inteiro_w		number(10);
vl_decimais_w		number(10,2);

ie_medico_executor_w	varchar2(20);
cd_cgc_regra_w 		varchar2(20);
cd_medico_regra_w 	varchar2(20);
cd_profissional_w	varchar2(20);
		
begin


vl_coparticipacao_w 	:= somente_numero(vl_coparticipacao_p);
vl_inteiro_w		:= to_number(substr(vl_coparticipacao_w,1,(length(vl_coparticipacao_w)-2)));
vl_decimais_w		:= to_number(substr(vl_coparticipacao_w,(length(vl_coparticipacao_w)-1),length(vl_coparticipacao_w)));
vl_decimais_w		:= dividir(vl_decimais_w,100);
vl_coparticipacao_w 	:= vl_inteiro_w + vl_decimais_w;

begin
 
select 	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from	autorizacao_convenio
where	nr_sequencia = nr_seq_autorizacao_p;
exception
when others then
	nr_atendimento_w:=0;
end;

if	(nr_atendimento_w > 0) and
	(vl_coparticipacao_w > 0) then

	nr_seq_atepacu_w := Obter_Atepacu_paciente(nr_atendimento_W,'A');
	
	
	select	nvl(max(ie_tipo_atendimento),0)
	into	ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
		
	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno = nr_seq_atepacu_w;
	

	begin
	select	nvl(max(cd_convenio),0),
		nvl(max(cd_categoria),'0')
	into	cd_convenio_w,
		cd_categoria_w
	from 	atend_categoria_convenio a
	where 	a.nr_atendimento		= nr_atendimento_w
	and	a.dt_inicio_vigencia	= (	select	max(dt_inicio_vigencia)
						from	atend_categoria_convenio b
						where 	nr_atendimento	= nr_atendimento_w);	
	exception
	when others then
		cd_convenio_w := 0;
		cd_categoria_w:= '0';
	end;	
			
			
	if	(cd_convenio_w	> 0) and
		(cd_categoria_w	<> '0') and
		(cd_setor_atendimento_w	> 0) then				
		
		select	nvl(max(cd_procedimento),0),
			nvl(max(ie_origem_proced),0)
		into	cd_procedimento_w,
			ie_origem_proced_w			
		from	conv_taxa_copartic
		where	cd_convenio = cd_convenio_w;
		
		consiste_medico_executor(cd_estabelecimento_p, cd_convenio_w, cd_setor_atendimento_w, 
					cd_procedimento_w, ie_origem_proced_w, ie_tipo_atendimento_w, 
					null, null, ie_medico_executor_w, 
					cd_cgc_regra_w, cd_medico_regra_w, cd_profissional_w,
					null ,sysdate, null, 'N', null, null);
		
		
		if	(ie_medico_executor_w = 'A') then
			select	max(cd_medico_resp)
			into	cd_medico_regra_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_w;
		end if;
					
		
		if	(cd_procedimento_w > 0) and
			(ie_origem_proced_w > 0) then
			
			inserir_proc_pac(	cd_procedimento_w,		
						1,	
						null,
						null,
						ie_origem_proced_w,
						cd_setor_atendimento_w,
						nr_atendimento_w,
						cd_estabelecimento_p,
						nm_usuario_p,
						null,
						'S',
						cd_medico_regra_w,
						nr_seq_atepacu_w,
						sysdate,
						cd_convenio_w,
						cd_categoria_w,
						nr_sequencia_w,
						vl_coparticipacao_w);	
		end if;
	end if;				
end if;



commit;

end gerar_proced_coparticipacao;
/
