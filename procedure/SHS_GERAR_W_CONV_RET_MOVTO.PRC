create or replace
procedure SHS_GERAR_W_CONV_RET_MOVTO(nr_seq_retorno_p	number) is 

/* CAMPOS DO ARQUIVO */
dt_mesano_pagamento_w	date;
dt_mesano_ant_w		date;
nr_conta_convenio_w	varchar2(255);
dt_execucao_w		date;
cd_usuario_convenio_w	varchar2(255);
cd_item_w		number(15);
vl_total_pago_w		number(17,4);
vl_glosa_w		number(17,4);
nr_doc_convenio_w	varchar2(255);
ds_vl_total_pago_w	varchar2(255);
ds_vl_glosa_w		varchar2(255);
ds_centavos_w		varchar2(255);

/* OUTROS CAMPOS */
nm_usuario_w		varchar2(15);

cursor	c01 is
select	to_date(trim(substr(obter_valor_campo_separador(a.ds_conteudo,4,';'),1,255)),'dd/mm/yyyy hh24:mi') dt_mesano_pagamento,
	to_date(trim(substr(obter_valor_campo_separador(a.ds_conteudo,5,';'),1,255)),'dd/mm/yyyy hh24:mi') dt_mesano_ant,
	substr(obter_valor_campo_separador(a.ds_conteudo,8,';'),1,255) nr_conta_convenio,
	to_date(trim(substr(obter_valor_campo_separador(a.ds_conteudo,11,';'),1,255)),'dd/mm/yyyy hh24:mi') dt_execucao,
	substr(obter_valor_campo_separador(a.ds_conteudo,13,';'),1,255) cd_usuario_convenio,
	somente_numero(substr(obter_valor_campo_separador(a.ds_conteudo,14,';'),1,255)) cd_item,
	substr(obter_valor_campo_separador(a.ds_conteudo,16,';'),1,255) ds_vl_total_pago,
	substr(obter_valor_campo_separador(a.ds_conteudo,18,';'),1,255) ds_vl_glosa,
	substr(obter_valor_campo_separador(a.ds_conteudo,22,';'),1,255) nr_doc_convenio
from	w_conv_ret_movto a
where	a.ds_conteudo		is not null
and	a.nr_seq_retorno	= nr_seq_retorno_p;

begin

select	max(a.nm_usuario)
into	nm_usuario_w
from	convenio_retorno a
where	a.nr_sequencia	= nr_seq_retorno_p;

open c01;
loop
fetch c01 into
	dt_mesano_pagamento_w,
	dt_mesano_ant_w,
	nr_conta_convenio_w,
	dt_execucao_w,
	cd_usuario_convenio_w,
	cd_item_w,
	ds_vl_total_pago_w,
	ds_vl_glosa_w,
	nr_doc_convenio_w;
exit when c01%notfound;

	if	(instr(ds_vl_total_pago_w,',') > 0) then
		ds_centavos_w	:= rpad(substr(ds_vl_total_pago_w,instr(ds_vl_total_pago_w,',') +1,length(ds_vl_total_pago_w)),2,'0');
	else
		ds_centavos_w	:= '00';
	end if;

	vl_total_pago_w	:= somente_numero(substr(ds_vl_total_pago_w,1,instr(ds_vl_total_pago_w,',') -1) || ds_centavos_w) / 100;

	if	(instr(ds_vl_glosa_w,',') > 0) then
		ds_centavos_w	:= rpad(substr(ds_vl_glosa_w,instr(ds_vl_glosa_w,',') +1,length(ds_vl_glosa_w)),2,'0');
	else
		ds_centavos_w	:= '00';
	end if;

	vl_glosa_w	:= somente_numero(substr(ds_vl_glosa_w,1,instr(ds_vl_glosa_w,',') -1) || ds_centavos_w) / 100;

	if	(instr(ds_vl_total_pago_w,'(') > 0) or
		(instr(ds_vl_total_pago_w,'-') > 0) then
		vl_total_pago_w	:= vl_total_pago_w * -1;
	end if;

	if	(instr(ds_vl_glosa_w,'(') > 0) or
		(instr(ds_vl_glosa_w,'-') > 0) then
		vl_glosa_w	:= vl_glosa_w * -1;
	end if;

	insert	into convenio_retorno_movto
		(cd_item,
		cd_usuario_convenio,
		dt_atualizacao,
		dt_execucao,
		dt_mesano_ant,
		dt_mesano_pagamento,
		nm_usuario,
		nr_conta_convenio,
		nr_doc_convenio,
		nr_seq_retorno,
		nr_sequencia,
		vl_glosa,
		vl_total_pago)
	values	(cd_item_w,
		cd_usuario_convenio_w,
		sysdate,
		dt_execucao_w,
		dt_mesano_ant_w,
		dt_mesano_pagamento_w,
		nm_usuario_w,
		nr_conta_convenio_w,
		nr_doc_convenio_w,
		nr_seq_retorno_p,
		convenio_retorno_movto_seq.nextval,
		vl_glosa_w,
		vl_total_pago_w);

end loop;
close c01;

delete	from w_conv_ret_movto 
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end	SHS_GERAR_W_CONV_RET_MOVTO;
/