create or replace
procedure gerar_titulo_receber_instr (  nr_titulo_p			number,
										vl_cobranca_p		number,
										nm_usuario_p		Varchar2,
										cd_moeda_p			number,
										vl_desconto_p		number,
										vl_desc_previsto_p	number,
										vl_saldo_inclusao_p	number,
										ie_tipo_ocorrencia_p number,
										cd_pessoa_fisica_p	varchar2,
										cd_cgc_p			varchar2,
										nr_seq_conta_banco_p	number,
										nm_objeto_p	varchar2 default null) is 
										
										
ie_desc_previsto_w		varchar2(1);										
ie_juros_multa_w		varchar2(1);
nr_seq_motivo_desc_w	titulo_receber_liq_desc.nr_seq_motivo_desc%type;		
cd_centro_custo_desc_w	titulo_receber_liq_desc.cd_centro_custo%type;
ie_retorno_w			number(10);
cd_banco_w				banco_estabelecimento.cd_banco%type;			
cd_agencia_bancaria_w	banco_estabelecimento.cd_agencia_bancaria%type;
nr_conta_w				banco_estabelecimento.cd_conta%type;
ie_digito_conta_w		banco_estabelecimento.ie_digito_conta%type;
cd_camara_compensacao_w	pessoa_fisica_conta.cd_camara_compensacao%type;
ie_gerar_juros_multa_w	varchar2(1) := 'S';

cursor c01 is
	select	cd_banco,
			cd_agencia_bancaria,
			nr_conta,
			nr_digito_conta,
			cd_camara_compensacao
	from 	pessoa_fisica_conta
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p
	union
	select	cd_banco,
			cd_agencia_bancaria,
			nr_conta,
			nr_digito_conta,
			cd_camara_compensacao
	from 	pessoa_juridica_conta
	where	cd_cgc 	= cd_pessoa_fisica_p;
begin

if ( nvl(nr_titulo_p,0) <> 0) then

	begin
	select	a.cd_banco,
			a.cd_agencia_bancaria,
			a.cd_conta,
			a.ie_digito_conta
	into	cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			ie_digito_conta_w
	from	banco_estabelecimento a
	where	a.nr_sequencia = nr_seq_conta_banco_p;
	exception when others then
			ie_retorno_w	:= 0;
	end;
	
	if	(ie_retorno_w = 0) then
		open c01;
		loop
		fetch c01 into
			cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			ie_digito_conta_w,
			cd_camara_compensacao_w;
		exit when c01%notfound;
			cd_banco_w	:= cd_banco_w;
		end loop;
		close c01;
	end if;

	obter_param_usuario(815, 9, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_desc_previsto_w);
	obter_param_usuario(815, 16, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_juros_multa_w);
	
	select	max(a.cd_centro_custo),
			max(a.nr_seq_motivo_desc)
	into	cd_centro_custo_desc_w,
			nr_seq_motivo_desc_w
	from	titulo_receber_liq_desc a
	where	a.nr_titulo	=  nr_titulo_p
	and		a.nr_bordero	is null
	and		a.nr_seq_liq	is null;
	
	if (upper(nm_objeto_p) = 'TITULO_RECEBER_UPDATE') then
		/*N�o pode utizar a obter_juros_multa_titulo qdo essa proc for chamada atraves da trigger TITULO_RECEBER_UPDATE, pq a function que calcula os juros faz select da TITULO_RECEBER, ocasionando mutante.
		Com isso, na WDLG de titulos para a instrucao, o campo de juros e multa sera nulo, mas qdo for gerado o lote de cobran�a escritural, o juros e multa vai ser gerado, se tiver com o parametro 16 na cobrala escritural p sim.*/
		ie_gerar_juros_multa_w := 'N';
	else
		ie_gerar_juros_multa_w := 'S';
	end if;

	insert into titulo_receber_instr (	nr_seq_cobranca,
										nr_titulo,
										vl_cobranca,
										dt_atualizacao,
										nm_usuario,
										cd_banco,
										cd_agencia_bancaria,
										nr_conta,
										cd_moeda,
										ie_digito_conta,
										cd_camara_compensacao,
										vl_desconto,
										vl_desc_previsto,
										vl_acrescimo,
										nr_sequencia,
										vl_despesa_bancaria,
										vl_saldo_inclusao,
										qt_dias_instrucao,
										cd_ocorrencia,
										ie_instrucao_enviada,
										nr_seq_motivo_desc,
										cd_centro_custo_desc,
										vl_juros,
										vl_multa,
										ie_selecionado,
										nr_seq_conta_banco_antiga)
							values	(	null,
										nr_titulo_p,
										vl_cobranca_p,
										sysdate,
										nm_usuario_p,
										cd_banco_w,
										cd_agencia_bancaria_w,
										nr_conta_w,
										cd_moeda_p,
										ie_digito_conta_w,
										cd_camara_compensacao_w,
										decode(ie_desc_previsto_w,'S', nvl(vl_desconto_p,0)) + nvl(to_number(obter_dados_titulo_receber(nr_titulo_p,'VNC')),0),
										decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_p,0),0),
										0,
										titulo_receber_instr_seq.nextval,
										0,
										vl_saldo_inclusao_p,
										null,
										substr(obter_ocorrencia_envio_cre(ie_tipo_ocorrencia_p,cd_banco_w),1,3),
										'N',
										nr_seq_motivo_desc_w,
										cd_centro_custo_desc_w,
										decode(ie_juros_multa_w,'S', decode(nvl(ie_gerar_juros_multa_w,'S'),'S', to_number(obter_juros_multa_titulo(nr_titulo_p,sysdate,'R','J')),null) ,null),
										decode(ie_juros_multa_w,'S', decode(nvl(ie_gerar_juros_multa_w,'S'),'S', to_number(obter_juros_multa_titulo(nr_titulo_p,sysdate,'R','M')),null), null), 
										'N',
										nr_seq_conta_banco_p);	

							
end if;
--Nao pode ter commit aqui
--commit;

end gerar_titulo_receber_instr;
/