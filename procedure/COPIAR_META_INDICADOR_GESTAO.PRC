create or replace
procedure copiar_meta_indicador_gestao (
			nr_sequencia_p	number,
			nm_tabela_p	varchar2,
			dt_referencia_p	date,
			nm_usuario_p	varchar2) is
			
column_name_w		user_tab_columns.column_name%type;
ds_atributos_w		varchar2(2000);
ds_valores_w		varchar2(2000);
			
Cursor C01 is
	select	column_name
	from	user_tab_columns
	where	table_name = nm_tabela_p;

begin

open C01;
loop
fetch C01 into	
	column_name_w;
exit when C01%notfound;
	begin
	if	(nvl(ds_atributos_w,'0') <> '0') then
		ds_atributos_w := ds_atributos_w || ', ';
	end if;
	ds_atributos_w := ds_atributos_w || column_name_w;
	
	if	(nvl(ds_valores_w,'0') <> '0') then
		ds_valores_w := ds_valores_w || ', ';
	end if;
	
	if	((column_name_w = 'NM_USUARIO') or
		(column_name_w = 'NM_USUARIO_NREC')) then
		ds_valores_w := ds_valores_w || '''' || nm_usuario_p || '''';
	elsif	((column_name_w = 'DT_ATUALIZACAO') or
		(column_name_w = 'DT_ATUALIZACAO_NREC')) then
		ds_valores_w := ds_valores_w || 'sysdate';
	elsif	(column_name_w = 'NR_SEQUENCIA') then
		ds_valores_w := ds_valores_w || nm_tabela_p || '_seq.nextval';
	elsif	(column_name_w = 'DT_REFERENCIA') then
		ds_valores_w := ds_valores_w || 'to_date(''' || to_char(dt_referencia_p,'dd/mm/yyyy') || ''',''dd/mm/yyyy'')';
	else
		ds_valores_w := ds_valores_w || column_name_w;
	end if;
	end;
end loop;
close C01;

if	(nvl(ds_atributos_w,'0') <> '0') and
	(nvl(ds_valores_w,'0') <> '0') then
	begin
	exec_sql_dinamico(nm_usuario_p,	'insert into ' || nm_tabela_p || ' (' || ds_atributos_w || ') ' ||
					'select ' || ds_valores_w || ' from ' || nm_tabela_p || ' where nr_sequencia = ' || nr_sequencia_p);
	end;
end if;

commit;

end copiar_meta_indicador_gestao;
/