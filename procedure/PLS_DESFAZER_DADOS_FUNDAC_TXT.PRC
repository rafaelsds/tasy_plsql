create or replace
procedure pls_desfazer_dados_fundac_txt(	nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
						nr_seq_lote_fat_p	pls_lote_faturamento.nr_sequencia%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type) is 

begin
if	(nr_seq_fatura_p is not null) then
	delete	pls_movto_arq_fundacao_txt
	where	nr_seq_lote in (select	nr_sequencia
				from	pls_lote_arq_fundacao_txt
				where	nr_seq_pls_fatura = nr_seq_fatura_p);
				
	delete	pls_lote_arq_fundacao_txt
	where	nr_seq_pls_fatura	= nr_seq_fatura_p;

elsif	(nr_seq_lote_fat_p is not null) then
	delete	pls_movto_arq_fundacao_txt
	where	nr_seq_lote in (select	nr_sequencia
				from	pls_lote_arq_fundacao_txt
				where	nr_seq_lote_fat = nr_seq_lote_fat_p);
				
	delete	pls_lote_arq_fundacao_txt
	where	nr_seq_lote_fat	= nr_seq_lote_fat_p;
end if;

commit;

end pls_desfazer_dados_fundac_txt;
/