create or replace
procedure ctb_orc_regras_ticket(	nr_seq_cenario_p	in	number,	
				nm_usuario_p	in	varchar2) is

nr_sequencia_w			number(15,0);
nr_seq_grupo_conta_w		number(10);
nr_seq_grupo_centro_w		number(10);
nr_seq_metrica_w			number(15,0);
cd_centro_custo_w			number(15,0);
cd_conta_contabil_w		varchar2(20);
nr_seq_mes_ref_w			number(10);
dt_mes_inic_w			date;
dt_mes_fim_w			date;
ie_regra_w			varchar2(20);
pr_aplicar_w			number(15,4);
ie_sobrepor_w			varchar2(20);
vl_fixo_w				number(15,2);
cd_estabelecimento_w		number(15,0);
cd_empresa_w			number(15,0);
ds_erro_w			varchar2(500);

cursor c01 is
select	nr_sequencia,
	cd_estabelecimento,
	cd_centro_custo,
	cd_conta_contabil,
	nr_seq_metrica,
	nr_seq_mes_ref,
	dt_mes_inic,
	dt_mes_fim,
	ie_regra,
	pr_aplicar,
	ie_sobrepor,
	nr_seq_grupo_conta,
	nr_seq_grupo_centro,
	vl_fixo
from	ctb_regra_ticket_medio
where	nr_seq_cenario	= nr_seq_cenario_p
order	by nr_seq_regra, ie_padrao_ajuste desc;

begin
/*limpar o ticket medio antes de gerar novamente*/
delete from ctb_cen_ticket_medio
where	nr_seq_cenario = nr_seq_cenario_p;

select	cd_empresa
into	cd_empresa_w
from	ctb_orc_cenario
where	nr_sequencia			= nr_seq_cenario_p;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_estabelecimento_w,
	cd_centro_custo_w,
	cd_conta_contabil_w,
	nr_seq_metrica_w,
	nr_seq_mes_ref_w,
	dt_mes_inic_w,
	dt_mes_fim_w,
	ie_regra_w,
	pr_aplicar_w,
	ie_sobrepor_w,
	nr_seq_grupo_conta_w,
	nr_seq_grupo_centro_w,
	vl_fixo_w;	
exit when c01%notfound;
	begin
	if	(ie_regra_w = 'VF') then
		begin
		ctb_aplicar_regra_ticket_fixo(nr_seq_cenario_p,
				nr_sequencia_w,
				cd_centro_custo_w,
				cd_conta_contabil_w,
				nr_seq_metrica_w,
				dt_mes_inic_w,
				dt_mes_fim_w,
				ie_sobrepor_w,
				nr_seq_grupo_centro_w,
				nr_seq_grupo_conta_w,
				vl_fixo_w,
				cd_empresa_w,
				cd_estabelecimento_w,
				nm_usuario_p);
		end;
	elsif	(ie_regra_w = 'PG') then
		begin
		ctb_aplicar_regra_ticket_grupo(	nr_seq_cenario_p,
						cd_estabelecimento_w,
						cd_empresa_w,
						nr_sequencia_w,
						nm_usuario_p,
						nr_seq_grupo_centro_w,
						ie_sobrepor_w,
						cd_centro_custo_w);
		end;
	else
		begin
		ctb_aplicar_regra_ticket(
				nr_seq_cenario_p,
				nr_sequencia_w,
				cd_centro_custo_w,
				cd_conta_contabil_w,
				nr_seq_mes_ref_w,
				nr_seq_metrica_w,
				dt_mes_inic_w,
				dt_mes_fim_w,
				ie_regra_w,
				pr_aplicar_w,
				ie_sobrepor_w,
				nr_seq_grupo_centro_w,
				nr_seq_grupo_conta_w,
				vl_fixo_w,
				cd_empresa_w,
				cd_estabelecimento_w,
				nm_usuario_p);
		end;
	end if;
	exception when others then
		ds_erro_w	:= SQLERRM(SQLCODE);
		wheb_mensagem_pck.exibir_mensagem_abort(266623,'NR_SEQUENCIA_W=' || nr_sequencia_w || ';' ||
							'DS_ERRO_W=' || ds_erro_w);
	end;
end loop;
close c01;

commit;

end ctb_orc_regras_ticket;
/
