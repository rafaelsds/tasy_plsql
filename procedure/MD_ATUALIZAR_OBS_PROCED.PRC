create or replace
procedure md_atualizar_obs_proced(nr_prescricao_p		number,
					nr_seq_mapa_p		number,
					nr_atendimento_p	number,
					qt_hora_p		number,
					dt_final_p		date,
					dt_dieta_p		date,
					cd_refeicao_p		Varchar2,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2,
					ie_opcao_p		varchar2) is 

nr_prescricao_ww		Number(10,0);
ds_observacao_proced_w		varchar2(2000);
ds_observacao_proced_ww		varchar2(2000);
ie_prescr_liberadas_enferm_w	varchar2(1);
varPrescrRN_w			varchar2(1);
nr_seq_mapa_w			number(10,0);
dt_dieta_x			date;

Cursor C01 is
	select	substr(a.ds_observacao,1,2000) || ' | '
	from	prescr_procedimento a,
		mapa_regra_proced b
	where	a.nr_prescricao = nr_prescricao_ww
	and	a.ie_origem_proced = b.ie_origem_proced
	and	a.cd_procedimento = b.cd_procedimento
	and	a.ds_observacao is not null;

begin

obter_param_usuario(1000, 114, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_prescr_liberadas_enferm_w);
obter_param_usuario(1000, 19, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, varPrescrRN_w);

if (ie_opcao_p = 'P') then
	begin
	
	if (nvl(nr_prescricao_p,0) <> 0) then
		nr_prescricao_ww := nr_prescricao_p;
	else
		select	nvl(max(a.nr_prescricao),0) 
		into	nr_prescricao_ww
		from	prescr_medica a
		where 	a.nr_atendimento	= nr_atendimento_p
		and	((nvl(a.dt_liberacao,a.dt_liberacao_medico) is not null and ie_prescr_liberadas_enferm_w = 'N')
		or	a.dt_liberacao is not null)
		and 	a.dt_prescricao	> sysdate - (qt_hora_p / 24)
		and	a.dt_prescricao <= dt_final_p
		and	((nvl(a.ie_recem_nato,'N') = 'N') or (a.ie_recem_nato = varPrescrRN_w));
	end if;
	
	nr_seq_mapa_w := nr_seq_mapa_p;

	end;
elsif (ie_opcao_p = 'T') then
	begin
	
	if (nr_prescricao_p <> 0) then
		nr_prescricao_ww := nr_prescricao_p;
	else
		select	nvl(max(a.nr_prescricao),0)
		into	nr_prescricao_ww
		from 	prescr_medica a
		where 	a.nr_atendimento	= nr_atendimento_p
		and	((nvl(a.dt_liberacao,a.dt_liberacao_medico) is not null and ie_prescr_liberadas_enferm_w = 'N')
		or	a.dt_liberacao is not null)
		and 	a.dt_prescricao	> sysdate - (qt_hora_p / 24)
		and	a.dt_prescricao <= dt_final_p	  
		and	((nvl(a.ie_recem_nato,'N') = 'N') or (a.ie_recem_nato = varPrescrRN_w))
		and	not exists (	select 	1 
					from 	mapa_dieta z
					where	z.cd_pessoa_fisica		= a.cd_pessoa_fisica
					and 	z.dt_dieta			= dt_dieta_p
					and	z.cd_refeicao			= cd_refeicao_p
					and	z.dt_liberacao is not null);
	end if;
	
	if (nvl(nr_seq_mapa_p,0) <> 0) then
		nr_seq_mapa_w := nr_seq_mapa_p;
	else
		select max(nr_sequencia)
		into	nr_seq_mapa_w
		from 	mapa_dieta
		where 	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and 	dt_dieta		= trunc(dt_dieta_p,'dd')
		and 	cd_refeicao		= cd_refeicao_p
		and 	ie_destino_dieta	= 'P'
		and	nr_seq_superior is null;
	end if;
	
	end;
	
end if;

ds_observacao_proced_w := '';

open C01;
loop
fetch C01 into
	ds_observacao_proced_ww;
exit when c01%notfound;
	begin

	ds_observacao_proced_w := ds_observacao_proced_w || ds_observacao_proced_ww;
	
	end;
end loop;
close c01;
		
ds_observacao_proced_w := substr(ds_observacao_proced_w,1,length(ds_observacao_proced_w)-3);

if (nvl(ds_observacao_proced_w,' ') <> ' ') then

	update 	mapa_dieta
	set	ds_observacao_proced = ds_observacao_proced_w
	where 	nr_sequencia 	= nr_seq_mapa_w;
	
end if;

commit;

end md_atualizar_obs_proced;
/