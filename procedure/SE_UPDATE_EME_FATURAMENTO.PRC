create or replace
procedure se_update_eme_faturamento(
		nr_sequencia_p		number,	
		nm_usuario_p		varchar2) is

begin
if 	(nr_sequencia_p is not null) then	
	begin
	update	eme_faturamento
	set	dt_emissao_bloqueto 	= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia 		= nr_sequencia_p;
	end;
	end if;	
commit;
end se_update_eme_faturamento;
/