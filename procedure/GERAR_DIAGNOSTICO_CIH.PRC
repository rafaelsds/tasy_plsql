create or replace 
procedure Gerar_Diagnostico_CIH(nr_atendimento_p	number,
			dt_alta_p		date,
			cd_medico_p		varchar2,
			cd_cid_primario_p	varchar2,
			cd_cid_secundario_p	varchar2,
			ie_tipo_atendimento_p	number,
			nm_usuario_p		varchar2) is

nr_atendimento_w	number(10);
dt_diagnostico_w	date;
ie_ignora_tipo_diag_w	varchar2(1);
ie_liberar_auto_cih_w	varchar2(1);
cd_estab_w		number(10);
dt_diagnostico_ww	date;	
begin
cd_estab_w := nvl(wheb_usuario_pck.get_cd_estabelecimento, 0);
ie_ignora_tipo_diag_w := nvl(Obter_Valor_Param_Usuario(916, 711, Obter_perfil_Ativo, nm_usuario_p, cd_estab_w),'N');
ie_liberar_auto_cih_w := Obter_Valor_Param_Usuario(916, 1150, Obter_perfil_Ativo, nm_usuario_p, cd_estab_w);

dt_diagnostico_w	:= sysdate;

select	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from	diagnostico_medico
where 	nr_atendimento = nr_atendimento_p
and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_p) = ie_tipo_atendimento_p
and 	ie_tipo_diagnostico = 2;

if	(nr_atendimento_w <> 0)
and	(ie_ignora_tipo_diag_w = 'S') then
	dt_diagnostico_ww := dt_diagnostico_w;
else	
	dt_diagnostico_ww := nvl(dt_alta_p,dt_diagnostico_w);
end if;

if	(nr_atendimento_w = 0)
or	(ie_ignora_tipo_diag_w = 'S') then	
	begin
	insert into diagnostico_medico (nr_atendimento, dt_diagnostico, ie_tipo_diagnostico, cd_medico,
					dt_atualizacao, nm_usuario, ds_diagnostico)
	values (nr_atendimento_p, dt_diagnostico_ww, 2, cd_medico_p,
		sysdate, nm_usuario_p, null);

	if	(length(cd_cid_primario_p) > 0) then
		insert into diagnostico_doenca (nr_atendimento, dt_diagnostico, cd_doenca, 
						dt_atualizacao, nm_usuario, ds_diagnostico,
						ie_classificacao_doenca, dt_liberacao, ie_situacao)
		values (nr_atendimento_p, dt_diagnostico_ww, cd_cid_primario_p, 
			sysdate, nm_usuario_p, null, 'P', decode(ie_liberar_auto_cih_w, 'S',sysdate, null), 'A');
	end if;

	if	(length(cd_cid_secundario_p) > 0) and
		(cd_cid_secundario_p <> cd_cid_primario_p) then /* Rafael em 30/05/2007 OS58180 */
		insert into diagnostico_doenca (nr_atendimento, dt_diagnostico, cd_doenca, 
						dt_atualizacao, nm_usuario, ds_diagnostico,
						ie_classificacao_doenca, dt_liberacao, ie_situacao)
		values (nr_atendimento_p, dt_diagnostico_ww, cd_cid_secundario_p, 
			sysdate, nm_usuario_p, null, 'S', decode(ie_liberar_auto_cih_w, 'S',sysdate, null), 'A');
	end if;
	commit;
	end;
end if;

END Gerar_Diagnostico_CIH;
/
