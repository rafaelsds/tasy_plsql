create or replace
procedure calcular_min_dur_pedido	(cd_estabelecimento_p	number,
					nr_seq_pedido_p		number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2) is

/* PROC CRIADA BASEADA NA PROC   ---- CALCULAR_MIN_DUR_AGENDA_CIR ----*/				
					
/* variaveis */
cd_agenda_w		number(10,0);
cd_medico_exec_w		varchar2(10);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_minuto_duracao_w	number(10,0);

qt_min_proc_w		number(10,0) 	:= 0;
qt_min_agenda_w		number(10,0) 	:= 0;

ie_param_sobreposicao_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_proc_adic_w	varchar2(10);
ie_atualiza_tempo_agenda_w		varchar2(1);
ie_atualiza_tempo_proc_w		varchar2(1);
ie_atual_maior_tempo_proc_w		varchar2(1); 
cd_estab_agenda_w	number(4);
ie_estab_agenda_w	varchar2(1) := 'S';
ds_lista_status_w varchar2(255)	:= null;
 		
/* obter exame agenda */
cursor c01 is
select
	cd_medico_exec,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	nr_minuto_duracao,
	cd_pessoa_fisica
from	pedido_age_cirurgia
where	nr_sequencia = nr_seq_pedido_p	
and	nr_seq_proc_interno is not null;

/* obter exames adicionais */
cursor c02 is
select	cd_medico,
		nr_seq_proc_interno,
		qt_min_proc
from	pedido_age_cirurgia_PROC
where	nr_seq_pedido = nr_seq_pedido_p	;

begin

obter_param_usuario(871, 162, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_agenda_w);
obter_param_usuario(871, 163, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_proc_w); 	
obter_param_usuario(871, 642, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atual_maior_tempo_proc_w);
	
if	(cd_estabelecimento_p is not null) and
	(nr_seq_pedido_p is not null) then
	
	/* obter duracao exame */
	open c01;
	loop
	fetch c01 into
			cd_medico_exec_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_minuto_duracao_w,
			cd_pessoa_fisica_w;
	exit when c01%notfound;
		begin
    
		if	(ie_atualiza_tempo_agenda_w in ('S','Q')) then		

			select	obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w, 0)
			into	qt_min_proc_w
			from	dual;
			
			qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
    
		end if;
			
		end;
	end loop;
	close c01;

	/* obter duracao exames adicionais */
	if	(ie_atualiza_tempo_proc_w = 'S') then
		
		open c02;
		loop
		fetch c02 into	cd_medico_proc_adic_w,
				nr_seq_proc_interno_w,
				qt_min_proc_w;
		exit when c02%notfound;
			begin
      
			if	(nvl(qt_min_proc_w,0) = 0) then
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,0);
			end if;
      
			if (ie_atual_maior_tempo_proc_w = 'S') then
				if (qt_min_proc_w > qt_min_agenda_w) then
					 qt_min_agenda_w := qt_min_proc_w;
				end if;
			else
				qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
			end if;		
			end;
      
		end loop;
		close c02;
		
		if	(qt_min_agenda_w > nr_minuto_duracao_w) then
			update	pedido_age_cirurgia
			set		nr_minuto_duracao	= qt_min_agenda_w
			where	nr_sequencia		= nr_seq_pedido_p;		
			commit;
		end if;
	
	end if;
	
end if;

end calcular_min_dur_pedido;
/
