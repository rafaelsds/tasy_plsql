create or replace
procedure OBTER_PERMISSAO_ESTAB(nm_usuario_p		in	varchar2,
				cd_perfil_p		in	number,
				cd_estabelecimento_p	in	number,
				cd_empresa_p		out	number,
				ds_erro_p		out	varchar2)  is

/* Cuidado ao alterar esta procedure, ela � usada no componente paciente */

cont_w		number(5,0);
ds_erro_w	varchar2(254);
cd_empresa_w	number(5,0);

begin
cd_empresa_w	:= null;
ds_erro_w	:= '';

select	count(*)
into	cont_w
from	(
	select	1
	from	usuario_estabelecimento
	where	nm_usuario_param	= nm_usuario_p
	and	cd_estabelecimento	= cd_estabelecimento_p
	union
	select	1
	from	usuario
	where	nm_usuario	= nm_usuario_p
	and	cd_estabelecimento	= cd_estabelecimento_p);

if	(cont_w > 0) then
	select	count(*)
	into	cont_w
	from	perfil
	where	cd_perfil		= cd_perfil_p
	and	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p;
	if	(cont_w > 0) then
		select	max(cd_empresa)
		into	cd_empresa_w
		from	Usuario_estabelecimento_v
		where	nm_usuario 		= nm_usuario_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
	else
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279415,null);
	end if;
else
	ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279418,null);
end if;

cd_empresa_p	:= cd_empresa_w;
ds_erro_p	:= ds_erro_w;

end OBTER_PERMISSAO_ESTAB;
/
