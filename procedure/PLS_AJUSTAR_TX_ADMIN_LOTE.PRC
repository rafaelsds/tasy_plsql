create or replace 
procedure pls_ajustar_tx_admin_lote (   nr_seq_lote_p	pls_lote_faturamento.nr_sequencia%type,
					nm_usuario_p	usuario.nm_usuario%type) is
					
tx_adm_considerar_w    	    	pls_conta_pos_estabelecido.tx_administracao%type;
vl_administracao_evento_w 	pls_fatura_evento.vl_evento%type :=0;
vl_adm_todos_itens_w    	number(15,2):= 0;
vl_dif_adm_w      		number(15,2):= 0;
vl_maior_taxa_w      		number(15,2) := 0;
nr_seq_conta_pos_atualizar_w  	pls_conta_pos_estabelecido.nr_sequencia%type;
vl_lib_taxa_co_w    		pls_conta_pos_estabelecido.vl_lib_taxa_co%type;
vl_lib_taxa_material_w          pls_conta_pos_estabelecido.vl_lib_taxa_material%type;
vl_lib_taxa_servico_w           pls_conta_pos_estabelecido.vl_lib_taxa_servico%type;
vl_materiais_w                  pls_conta_pos_estabelecido.vl_materiais%type;
vl_medico_w                     pls_conta_pos_estabelecido.vl_medico%type;
vl_custo_operacional_w          pls_conta_pos_estabelecido.vl_custo_operacional%type;
vl_teste_w      		number(15,4):= 0;
qt_dif_taxas_no_evento_w	pls_integer := 0;
vl_evento_w			pls_fatura_evento.vl_evento%type;

Cursor C01 is
	select  a.nr_sequencia nr_seq_evento,
		b.nr_sequencia nr_seq_fatura,
	       (select	nvl(sum(nvl(w.vl_faturado_ndc,0)),0)
		from	pls_fatura_conta	w
		where	w.nr_seq_fatura_evento	= a.nr_sequencia)vl_evento
	from   	pls_fatura_evento a,
		pls_fatura b
	where  	a.nr_seq_fatura		= b.nr_sequencia
	and     b.nr_seq_lote   	= nr_seq_lote_p
	and	(b.ie_fatura_taxa	= 'N' or b.ie_fatura_taxa is null);
	
procedure ajusta_tx_pos(  nr_seq_conta_pos_p  pls_conta_pos_estabelecido.nr_sequencia%type,
        vl_diferenca_p    number) is
	
--Os valores de ajuste sao necessarios ter 4 casas decimais, pois sao proporcoes do valor de ajuste e precisam ser tratados para 
--que no update para o campo com 2 casas, nao se perca valor
vl_ajustar_tx_co_w    		number(15,4);
vl_ajustar_tx_servico_w    	number(15,4);
vl_ajustar_tx_materiais_w  	number(15,4);
vl_lib_taxa_co_w    		pls_conta_pos_estabelecido.vl_lib_taxa_co%type;
vl_lib_taxa_material_w          pls_conta_pos_estabelecido.vl_lib_taxa_material%type;
vl_lib_taxa_servico_w           pls_conta_pos_estabelecido.vl_lib_taxa_servico%type;
vl_taxa_co_w                    pls_conta_pos_estabelecido.vl_taxa_co%type;
vl_taxa_material_w              pls_conta_pos_estabelecido.vl_taxa_material%type;
vl_taxa_servico_w               pls_conta_pos_estabelecido.vl_taxa_servico%type;
vl_materiais_w                  pls_conta_pos_estabelecido.vl_materiais%type;
vl_medico_w                     pls_conta_pos_estabelecido.vl_medico%type;
vl_custo_operacional_w          pls_conta_pos_estabelecido.vl_custo_operacional%type;
vl_administracao_w              pls_conta_pos_estabelecido.vl_administracao%type;
pr_tx_co_w      		number(10,4);
pr_tx_material_w    		number(10,4);
pr_tx_servico_w      		number(10,4);
vl_divergencia_w    		number(10,4);
nr_seq_conta_w			pls_conta.nr_sequencia%type;

begin

--Obtem os valores das taxas do pos-estab a ser atualizado e o percentual que cada sub-taxa representa em relacao ao valor
--total da taxa administrativa para que o valor de ajuste seja rateado de maneira proporcional
select  vl_lib_taxa_co,
	vl_lib_taxa_material,
	vl_lib_taxa_servico,
	(dividir(vl_lib_taxa_co, vl_administracao ))pr_tx_co,
	(dividir(vl_lib_taxa_material, vl_administracao ))pr_tx_material,
	(dividir(vl_lib_taxa_servico, vl_administracao ))pr_tx_servico
into  	vl_lib_taxa_co_w,
        vl_lib_taxa_material_w,
        vl_lib_taxa_servico_w,
        pr_tx_co_w,
        pr_tx_material_w,
	pr_tx_servico_w
from  	pls_conta_pos_estabelecido
where 	nr_sequencia = nr_seq_conta_pos_p;

	/*
	Aqui e feito o Round para duas casas pois os campos na tabela que receberao esse ajuste tem duas casas, entao preciso garantir que a divisao
	dos valores nao gere fracao de centavo, pois isso viria gerar perda que pode chegar ate 0,99 centavos em cada uma das 3 taxas. Com o round, 
	pode haver  pequena perda ou ganho nesse momento, porem abaixo eu faco um ajuste  e jogo a divergencia(sem fracao) para a maior das taxas.
	*/
	vl_ajustar_tx_co_w         := round((vl_diferenca_p * pr_tx_co_w),2);
	vl_ajustar_tx_servico_w    := round((vl_diferenca_p * pr_tx_servico_w),2);
	vl_ajustar_tx_materiais_w  := round((vl_diferenca_p * pr_tx_material_w),2);
	
	--Ao distribuir a diferenca de valor entre as 3 taxas, pode ocorrer problema com sobra devido a proporcionalidade do valor, entao atribuo essa sobra ao maior dos valores
	vl_divergencia_w := (vl_ajustar_tx_co_w + vl_ajustar_tx_servico_w + vl_ajustar_tx_materiais_w) - vl_diferenca_p;
	if  ( vl_divergencia_w <> 0) then
	
		--Priorizo a taxa servico para receber essa diferenca, se ela for maior ou pelo menos igual as demais, ela recebera o valor. Se alguma das outras duas taxas for mais, entao
		--continuo verificando quem deve receber a diferenca.
		--A divergencia entre o valor a ser ajustado e o valor distribuido proporcionalmente entre as 3 sub-taxas pode gerar uma divergencia que pode 
		--interferir no valor. Abaixo eu subtraio ela das taxas pois o vl_divergencia sera negativo caso reste uma fracao de centavo a ser distribuida entre os valores de ajuste, entao
		--a mesma acabara "somando" devido a subtracao do valor negativo.
		if  ( vl_ajustar_tx_servico_w >= vl_ajustar_tx_co_w and vl_ajustar_tx_servico_w >= vl_ajustar_tx_materiais_w) then
			vl_ajustar_tx_servico_w := vl_ajustar_tx_servico_w - vl_divergencia_w;
			
		elsif  ( vl_ajustar_tx_co_w >= vl_ajustar_tx_materiais_w ) then
			vl_ajustar_tx_co_w := vl_ajustar_tx_co_w - vl_divergencia_w;
			
		else
			vl_ajustar_tx_materiais_w := vl_ajustar_tx_materiais_w - vl_divergencia_w;
		end if;
	end if;
	
	--Nesse ponto os valores de ajuste estarao definidos corretamente.
	update  pls_conta_pos_estabelecido
	set  	vl_lib_taxa_co       	= vl_lib_taxa_co - vl_ajustar_tx_co_w,
		vl_lib_taxa_material    = vl_lib_taxa_material  - vl_ajustar_tx_materiais_w,
		vl_lib_taxa_servico    	= vl_lib_taxa_servico - vl_ajustar_tx_servico_w,
		vl_liberado_co_fat    	= vl_liberado_co_fat - vl_ajustar_tx_co_w,
		vl_liberado_hi_fat    	= vl_liberado_hi_fat - vl_ajustar_tx_servico_w,
		vl_liberado_material_fat = vl_liberado_material_fat - vl_ajustar_tx_materiais_w,
		vl_administracao    	= vl_lib_taxa_co + vl_lib_taxa_material + vl_lib_taxa_servico - ( vl_ajustar_tx_co_w + vl_ajustar_tx_materiais_w + vl_ajustar_tx_servico_w),
		vl_calculado      	= vl_calculado - ( vl_ajustar_tx_co_w + vl_ajustar_tx_materiais_w + vl_ajustar_tx_servico_w ),
		vl_beneficiario     	= vl_beneficiario - ( vl_ajustar_tx_co_w + vl_ajustar_tx_materiais_w + vl_ajustar_tx_servico_w ),
		vl_provisao      	= vl_provisao - ( vl_ajustar_tx_co_w + vl_ajustar_tx_materiais_w + vl_ajustar_tx_servico_w ),
		vl_ajuste		= ( vl_ajustar_tx_co_w + vl_ajustar_tx_materiais_w + vl_ajustar_tx_servico_w ) --Para permitir retornar o ajuste ao desfazer lote de faturamento
	where  	nr_sequencia 		= nr_seq_conta_pos_p
	return 	nr_seq_conta into nr_seq_conta_w;
	
	--Ajuste para que os valores detalhados e o valor de beneficiario estejam corretos
	update	pls_conta_pos_estabelecido
	set	vl_beneficiario	= (nvl(vl_medico,0) + nvl(vl_custo_operacional,0) + nvl(vl_materiais,0) + nvl(vl_administracao,0))
	where	nr_sequencia	= nr_seq_conta_pos_p;
	
	--Necessario gerar novamente os valores contabies de pos-estabelecido devido a alteracao do registro
	pls_gerar_contab_val_adic (	nr_seq_conta_w,	null, null, null, null, null,
					null, 'P', 'N', nm_usuario_p);
					
end;

begin
	for r_c01_w in C01 loop
		--levanta a tx_adminstra utilizada para verificar arredondamento das tx_adm nos itens de pos-estab
		--se retornar zero ou nulo, entao define valor padrao 100 para nao necessitar muitas verificacoes posteriormente
		select decode(max(c.tx_administracao), null, 100, 0, 100, max(c.tx_administracao))
		into   	tx_adm_considerar_w
		from   	pls_fatura_conta a,
			pls_fatura_evento b,
			pls_conta_pos_estabelecido c
		where  	b.nr_sequencia		= a.nr_seq_fatura_evento
		and    	c.nr_seq_conta		= a.nr_seq_conta
		and    	b.nr_sequencia		= r_c01_w.nr_seq_evento
		and	c.nr_seq_lote_fat	= nr_seq_lote_p;
		
		--Se no evento em questao existirem itens cuja taxa administrativa forem diferentes, nao sera possivel a realizacao do arredondamento
		--por essa razao, aqui e levantado se apenas 1 pecentual de taxa e definido para todos os pos-estab que "entram" nesse evento de faturamento.
		select  count(distinct tx)
		into   	qt_dif_taxas_no_evento_w
		from
			(select  distinct c.tx_administracao tx
			from   	pls_fatura_proc x,
				pls_fatura_conta a,
				pls_fatura_evento b,
				pls_conta_pos_estabelecido c
			where  	b.nr_sequencia		= a.nr_seq_fatura_evento
			and	a.nr_sequencia		= x.nr_seq_fatura_conta
			and    	c.nr_sequencia		= x.nr_seq_conta_pos_estab
			and    	b.nr_sequencia		= r_c01_w.nr_seq_evento
			and	c.nr_seq_lote_fat	= nr_seq_lote_p
			union all
			select  distinct c.tx_administracao tx
			from   	pls_fatura_mat x,
				pls_fatura_conta a,
				pls_fatura_evento b,
				pls_conta_pos_estabelecido c
			where  	b.nr_sequencia		= a.nr_seq_fatura_evento
			and	a.nr_sequencia		= x.nr_seq_fatura_conta
			and    	c.nr_sequencia		= x.nr_seq_conta_pos_estab
			and    	b.nr_sequencia		= r_c01_w.nr_seq_evento
			and	c.nr_seq_lote_fat	= nr_seq_lote_p
			);
			
		--Obtem o valor de tx administrativa caso o mesmo fosse aplicado diretamente no valor do evento
		vl_administracao_evento_w :=  dividir((tx_adm_considerar_w * r_c01_w.vl_evento), 100);
		
		--Obtem a soma do valor das taxas adm. de todos os itens
		select  sum(vl_faturado)
			into  vl_adm_todos_itens_w
		from  (
			select  sum(a.vl_faturado) vl_faturado
			from  	pls_fatura_proc a,
				pls_fatura_conta b
			where  	a.nr_seq_fatura_conta = b.nr_sequencia
			and  	b.nr_seq_fatura_evento = r_c01_w.nr_seq_evento
			union all
			select  sum(a.vl_faturado) vl_faturado
			from  	pls_fatura_mat a,
				pls_fatura_conta b
			where  	a.nr_seq_fatura_conta = b.nr_sequencia
			and  	b.nr_seq_fatura_evento = r_c01_w.nr_seq_evento);
			
		--Verifica se ha diferenca entre o valor de taxa adm gerada para todos os itens no evento e o valor da taxa considerando o percentual sobre o
		--valor total do evento. Caso sim, sera necessario passar essa diferenca para as taxas presentes nos registros de pos-estab. Foi definido que o
		--diferenca sera passada para o registro de pos-estab que possuir o maior valor de taxa.
		if	((vl_adm_todos_itens_w != 0) and (vl_administracao_evento_w <> vl_adm_todos_itens_w ) and (qt_dif_taxas_no_evento_w = 1) and (tx_adm_considerar_w!=100)) then
			vl_dif_adm_w := vl_adm_todos_itens_w - vl_administracao_evento_w ;
			
			--Maior valor de tx_administrativa entre os itens contidos no evento
			select  max(vl_faturado)
			into  	vl_maior_taxa_w
			from  (
				select  max(a.vl_faturado) vl_faturado
				from  	pls_fatura_proc a,
					pls_fatura_conta b
				where  	a.nr_seq_fatura_conta 	= b.nr_sequencia
				and  	b.nr_seq_fatura_evento 	= r_c01_w.nr_seq_evento
				and   	a.nr_seq_fat_proc_cancel is null
				union all
				select  max(a.vl_faturado)
				from  	pls_fatura_mat a,
					pls_fatura_conta b
				where  	a.nr_seq_fatura_conta 	= b.nr_sequencia
				and  	b.nr_seq_fatura_evento 	= r_c01_w.nr_seq_evento
				and   	a.nr_seq_fat_mat_cancel is null);
				
			select  max(nr_seq_conta_pos_estab)
			into  	nr_seq_conta_pos_atualizar_w
			from
			     (  select 	max(nr_seq_conta_pos_estab) nr_seq_conta_pos_estab
				from  	pls_fatura_proc a,
					pls_fatura_conta b
				where  	a.nr_seq_fatura_conta 	= b.nr_sequencia
				and  	b.nr_seq_fatura_evento	= r_c01_w.nr_seq_evento
				and  	a.vl_faturado 		= vl_maior_taxa_w
				and   	a.nr_seq_fat_proc_cancel is null
				union
				select 	max(nr_seq_conta_pos_estab)
				from  	pls_fatura_mat a,
					pls_fatura_conta b
				where  	a.nr_seq_fatura_conta 	= b.nr_sequencia
				and  	b.nr_seq_fatura_evento 	= r_c01_w.nr_seq_evento
				and  	a.vl_faturado 		= vl_maior_taxa_w
				and  	a.nr_seq_fat_mat_cancel is null);
				
			--Aqui e chamada rotina para aplicar a diferenca sobre o registro de pos-estabelecido encontrado.
			ajusta_tx_pos( nr_seq_conta_pos_atualizar_w, vl_dif_adm_w );
			
			--Busca valores de pos alterado e atualiza os valores da fatura_proc ou pls_fatura_mat
			select  vl_lib_taxa_co,
				vl_lib_taxa_material,
				vl_lib_taxa_servico,
				vl_materiais,
				vl_medico,
				vl_custo_operacional
			into  	vl_lib_taxa_co_w,
				vl_lib_taxa_material_w,
				vl_lib_taxa_servico_w,
				vl_materiais_w,
				vl_medico_w,
				vl_custo_operacional_w
			from  	pls_conta_pos_estabelecido
			where  	nr_sequencia = nr_seq_conta_pos_atualizar_w;
			
			update  pls_fatura_proc
			set  	vl_faturado     	=  (vl_lib_taxa_co_w + vl_lib_taxa_material_w + vl_lib_taxa_servico_w),
				vl_lib_taxa_co    	= vl_lib_taxa_co_w,
				vl_lib_taxa_material  	= vl_lib_taxa_material_w,
				vl_lib_taxa_servico  	= vl_lib_taxa_servico_w
			where  	nr_seq_conta_pos_estab  = nr_seq_conta_pos_atualizar_w
			and  	nr_seq_fat_proc_cancel is null;
			
			update  pls_fatura_mat
			set  	vl_faturado     	=  ( vl_lib_taxa_co_w + vl_lib_taxa_material_w + vl_lib_taxa_servico_w ),
				vl_lib_taxa_co    	= vl_lib_taxa_co_w,
				vl_lib_taxa_material  	= vl_lib_taxa_material_w,
				vl_lib_taxa_servico  	= vl_lib_taxa_servico_w
			where  	nr_seq_conta_pos_estab  = nr_seq_conta_pos_atualizar_w
			and  	nr_seq_fat_mat_cancel is null;
			
			--Atualiza valor da taxa na respectiva pls_fatura_conta
			update  pls_fatura_conta
			set  	vl_faturado   = vl_faturado - vl_dif_adm_w
			where  	nr_sequencia   = (  	select  max(nr_seq_fatura_conta)
							from    (  select   nr_seq_fatura_conta
							from	pls_fatura_proc
							where  	nr_seq_conta_pos_estab = nr_seq_conta_pos_atualizar_w
							union
							select  nr_seq_fatura_conta
							from  	pls_fatura_mat
							where  	nr_seq_conta_pos_estab = nr_seq_conta_pos_atualizar_w));
							
			update  pls_fatura
			set  	vl_fatura = vl_fatura - vl_dif_adm_w
			where  	nr_sequencia = r_c01_w.nr_seq_fatura;
			
			select	sum(vl_faturado) + sum(vl_faturado_ndc)
			into	vl_evento_w
			from	pls_fatura_conta
			where	nr_seq_fatura_evento = r_c01_w.nr_seq_evento;
			
			update  pls_fatura_evento
			set  	vl_evento 	= vl_evento_w
			where  	nr_sequencia 	= r_c01_w.nr_seq_evento;
		end if;
	end loop;

commit;

end pls_ajustar_tx_admin_lote;
/