create or replace
procedure com_gerar_modulo_estim_prop (
		nr_seq_proposta_p	number,
		nr_seq_estimativa_p	number,
		ds_lista_modulos_p	varchar2,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2) is

ds_lista_modulos_w	varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_seq_modulo_w		number(10,0);

begin
if	(nr_seq_proposta_p is not null) and
	(nr_seq_estimativa_p is not null) and
	(ds_lista_modulos_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ds_lista_modulos_w	:= ds_lista_modulos_p;
	while (ds_lista_modulos_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(ds_lista_modulos_w,',');
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_seq_modulo_w		:= substr(ds_lista_modulos_w,0,nr_pos_virgula_w-1);
			ds_lista_modulos_w	:= substr(ds_lista_modulos_w,nr_pos_virgula_w+1,length(ds_lista_modulos_w));			
			end;
		else
			begin
			nr_seq_modulo_w		:= to_number(ds_lista_modulos_w);
			ds_lista_modulos_w	:= null;
			end;
		end if;	
		
		if	(nvl(nr_seq_modulo_w,0) > 0) then
			begin
			com_gerar_estim_prop(nr_seq_proposta_p,
				nr_seq_estimativa_p,
				nr_seq_modulo_w,
				nm_usuario_p,
				cd_estabelecimento_p);
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;
end com_gerar_modulo_estim_prop;
/