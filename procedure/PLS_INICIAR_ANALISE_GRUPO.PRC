create or replace
procedure pls_iniciar_analise_grupo
			(	nr_seq_analise_p	in	number,
				nr_seq_grupo_p		in	number,
				nm_usuario_p		in	varchar2,
				ds_retorno_p		out	varchar2,
				ie_tipo_retorno_p	out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Tratar in�cio da an�lise do grupo auditor na nova An�lise de Contas M�dicas
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nm_grupo_anterior_w		varchar2(255);
nm_grupo_pedido_w		varchar2(255);
nr_seq_ordem_w			number(10);
qt_pedido_parc_w		number(10);
nr_seq_grupo_atual_w		number(10);
dt_inicio_analise_w		date;
dt_fim_analise_w		date;

begin
/* Consist�ncias */

/* Obter dados do grupo na an�lise */
select	a.nr_seq_ordem,
	a.dt_inicio_grupo,
	a.dt_fim_grupo
into	nr_seq_ordem_w,
	dt_inicio_analise_w,
	dt_fim_analise_w
from	pls_auditoria_conta_grupo a
where	a.nr_seq_analise	= nr_seq_analise_p
and	a.nr_seq_grupo		= nr_seq_grupo_p
and	a.nr_sequencia		=	(select	min(x.nr_sequencia)
					from	pls_auditoria_conta_grupo	x
					where	x.nr_seq_analise	= nr_seq_analise_p
					and	x.nr_seq_grupo		= nr_seq_grupo_p
					and	x.nr_seq_ordem		= (	select	min(y.nr_seq_ordem)
										from	pls_auditoria_conta_grupo	y
										where	y.nr_seq_analise	= nr_seq_analise_p
										and	y.nr_seq_grupo		= nr_seq_grupo_p));


/* Verificar se tem algum grupo que precisa fazer a an�lise primeiro */
select	max(b.nm_grupo_auditor)
into	nm_grupo_anterior_w
from	pls_grupo_auditor b,
	pls_auditoria_conta_grupo a
where	a.nr_seq_grupo		= b.nr_sequencia
and	a.nr_seq_analise	= nr_seq_analise_p
and	a.nr_seq_grupo		<> nr_seq_grupo_p
and	a.dt_liberacao is null
and	a.nr_seq_ordem		< nr_seq_ordem_w;

if	(nm_grupo_anterior_w is not null) then
	ds_retorno_p		:= 'O seu grupo de an�lise s� pode iniciar ap�s o grupo "' || nm_grupo_anterior_w || '" ter conclu�do a an�lise';
	ie_tipo_retorno_p	:= 'E';
end if;

if	(ds_retorno_p is null) then
	pls_atualizar_auditor_grupo(nr_seq_grupo_p,nr_seq_analise_p,nm_usuario_p);
	
	select	count(1),
		max(nr_seq_grupo_atual)
	into	qt_pedido_parc_w,
		nr_seq_grupo_atual_w
	from	pls_analise_pedido_parecer
	where	nr_seq_grupo_parecer	= nr_seq_grupo_p
	and	nr_seq_analise		= nr_seq_analise_p
	and	dt_finalizacao is null
	and	rownum = 1;
	
	if	(qt_pedido_parc_w > 0) then
		select	max(nm_grupo_auditor)
		into	nm_grupo_pedido_w
		from	pls_grupo_auditor
		where	nr_sequencia	= nr_seq_grupo_atual_w;
		
		ds_retorno_p		:= 'O seu parecer foi solicitado novamente pelo grupo ' || nm_grupo_pedido_w;
		ie_tipo_retorno_p	:= 'I';
	end if;
	
	commit;
end if;

end pls_iniciar_analise_grupo;
/