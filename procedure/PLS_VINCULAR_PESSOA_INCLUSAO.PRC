create or replace
procedure pls_vincular_pessoa_inclusao
			(	nr_seq_solicitacao_p	number,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Comparar as informacoes da solicitacao de inclussao com as da pessoa fisica, se
forem diferentes e gerarda a solicitacao de alteracao.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
	OPS - Proposta de Adesao Eletronica
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_intercambio_w		number(10);
cd_estabelecimento_w		number(4);
ie_gerar_solic_alt_pf_w		varchar2(1);
ie_formatacao_nome_pf_w		varchar2(255);
nm_pessoa_fisica_w		varchar2(255);
dt_nascimento_w			date;
cd_nacionalidade_w		varchar2(8);
nr_cpf_w			varchar2(11);
ie_estado_civil_w		varchar2(2);
ie_sexo_w			varchar2(1);
ds_email_w			varchar2(255);
nr_identidade_w			varchar2(15);
dt_emissao_ci_w			date;
sg_emissora_ci_w		valor_dominio.vl_dominio%type;
nr_seq_pais_w			number(10);
ds_orgao_emissor_ci_w		pessoa_fisica.ds_orgao_emissor_ci%type;
nr_reg_geral_estrang_w		varchar2(30);
cd_cep_w			varchar2(15);
cd_cep_ww			number(15);
ds_endereco_w			varchar2(100);
nr_endereco_w			varchar2(5);
ds_complemento_w		varchar2(40);
ds_bairro_w			varchar2(80);
cd_municipio_ibge_w		varchar2(6);
sg_estado_w			valor_dominio.vl_dominio%type;
nr_ddd_telefone_w		varchar2(3);
nr_ddi_telefone_w		varchar2(3);
nr_telefone_w			varchar2(15);
nr_telefone_celular_w		varchar2(40);
nr_ctps_w			pls_inclusao_beneficiario.nr_ctps%type;
nr_serie_ctps_w			pls_inclusao_beneficiario.nr_serie_ctps%type;
uf_emissora_ctps_w		valor_dominio.vl_dominio%type;
ds_municipio_w			varchar2(255);
nr_pis_pasep_w			varchar2(15);
nr_ddd_fax_w			varchar2(3);
nr_fax_w			varchar2(80);
cd_declaracao_nasc_vivo_w	pessoa_fisica.cd_declaracao_nasc_vivo%type;
nr_cartao_nac_sus_w		varchar2(20);
nr_ddd_celular_w		varchar2(3);
nr_cert_casamento_w		pls_inclusao_beneficiario.nr_cert_casamento%type;
dt_emissao_cert_casamento_w	date;
nr_titulo_eleitor_w		varchar2(20);
ds_tipo_logradouro_w		varchar2(125);
cd_tipo_logradouro_w		varchar2(3);
nm_abreviado_w			varchar2(255);
nm_pessoa_fisica_old_w		varchar2(255);
dt_nascimento_old_w		date;
cd_nacionalidade_old_w		varchar2(8);
nr_cpf_old_w			varchar2(11);
ie_estado_civil_old_w		varchar2(2);
ie_sexo_old_w			varchar2(1);
ds_email_old_w			varchar2(255);
nr_identidade_old_w		varchar2(15);
dt_emissao_ci_old_w		date;
sg_emissora_ci_old_w		valor_dominio.vl_dominio%type;
nr_seq_pais_old_w		number(10);
ds_orgao_emissor_ci_old_w	pessoa_fisica.ds_orgao_emissor_ci%type;
nr_reg_geral_estrang_old_w	varchar2(30);
cd_cep_old_w			varchar2(15);
ds_endereco_old_w		varchar2(40);
nr_endereco_old_w		varchar2(5);
ds_complemento_old_w		varchar2(40);
ds_bairro_old_w			varchar2(80);
cd_municipio_ibge_old_w		varchar2(6);
sg_estado_old_w			valor_dominio.vl_dominio%type;
nr_ddd_telefone_old_w		varchar2(3);
nr_ddi_telefone_old_w		varchar2(3);
nr_telefone_old_w		varchar2(15);
nr_telefone_celular_old_w	varchar2(40);
nr_ctps_old_w			pessoa_fisica.nr_ctps%type;
nr_serie_ctps_old_w		pessoa_fisica.nr_serie_ctps%type;
uf_emissora_ctps_old_w		valor_dominio.vl_dominio%type;
ds_municipio_old_w		varchar2(255);
nr_pis_pasep_old_w		varchar2(15);
nr_ddd_fax_old_w		varchar2(3);
nr_fax_old_w			varchar2(80);
cd_declaracao_nasc_vivo_old_w	number(30);
nr_cartao_nac_sus_old_w		varchar2(20);
nr_ddd_celular_old_w		varchar2(3);
nr_cert_casamento_old_w		varchar2(20);
dt_emissao_cert_casament_old_w	date;
nr_titulo_eleitor_old_w		varchar2(20);
ds_tipo_logradouro_old_w	varchar2(125);
cd_tipo_logradouro_old_w	varchar2(3);
nm_abreviado_old_w		varchar2(255);
nm_mae_old_w			varchar2(255);
nm_mae_w			varchar2(255);
nm_pai_old_w			varchar2(255);
nm_pai_w			varchar2(255);
dt_adocao_old_w			date;
dt_adocao_w			date;
ie_consistir_sib_w		varchar2(1);
cd_municipio_ibge_nasc_old_w	pessoa_fisica.cd_municipio_ibge%type;
cd_municipio_ibge_nasc_w	pls_inclusao_beneficiario.cd_municipio_ibge_nasc%type;
sg_estado_nasc_old_w		pessoa_fisica.sg_estado_nasc%type;
sg_estado_nasc_w		pls_inclusao_beneficiario.sg_estado_nasc%type;
nm_social_old_w			pessoa_fisica.nm_social%type;
nm_social_w			pls_inclusao_beneficiario.nm_social%type;
cd_cbo_old_w			pessoa_fisica.cd_cbo_sus%type;
cd_cbo_w			pls_inclusao_beneficiario.cd_cbo%type;
qt_complemento_w		pls_integer;
nr_seq_complemento_pf_w		compl_pessoa_fisica.nr_sequencia%type;

tb_ie_acao_w			pls_util_cta_pck.t_varchar2_table_1;

cursor c01 is
	select	nr_sequencia,
		decode(ie_gerar_solic_alt_pf_w, 'R', ie_acao, ie_gerar_solic_alt_pf_w) ie_acao
	from	pls_regra_atrib_inc_benef;

begin
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
ie_gerar_solic_alt_pf_w	:= nvl(obter_valor_param_usuario(1232, 91, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w),'S');
ie_consistir_sib_w	:= nvl(obter_valor_param_usuario(1232, 65, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w),'S');

select	nm_pessoa_fisica,
	dt_nascimento,
	cd_nacionalidade,
	nr_cpf,
	ie_estado_civil,
	ie_sexo,
	ds_email,
	nr_identidade,
	dt_emissao_ci,
	sg_emissora_ci,
	nr_seq_pais,
	ds_orgao_emissor_ci,
	nr_reg_geral_estrang,
	cd_cep,
	ds_endereco,
	nr_endereco,
	ds_complemento,
	ds_bairro,
	cd_municipio_ibge,
	sg_estado,
	nr_ddd_telefone,
	nr_ddi_telefone,
	nr_telefone,
	nr_telefone_celular,
	nr_ctps,
	nr_serie_ctps,
	uf_emissora_ctps,
	ds_municipio,
	nr_pis_pasep,
	nr_ddd_fax,
	nr_fax,
	cd_declaracao_nasc_vivo,
	nr_cartao_nac_sus,
	nr_ddd_celular,
	nr_cert_casamento,
	dt_emissao_cert_casamento,
	nr_titulo_eleitor,
	cd_tipo_logradouro,
	nm_mae,
	nm_pai,
	dt_adocao,
	cd_municipio_ibge_nasc,
	nr_seq_intercambio,
	sg_estado_nasc,
	nm_social,
	cd_cbo
into	nm_pessoa_fisica_w,
	dt_nascimento_w,
	cd_nacionalidade_w,
	nr_cpf_w,
	ie_estado_civil_w,
	ie_sexo_w,
	ds_email_w,
	nr_identidade_w,
	dt_emissao_ci_w,
	sg_emissora_ci_w,
	nr_seq_pais_w,
	ds_orgao_emissor_ci_w,
	nr_reg_geral_estrang_w,
	cd_cep_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_bairro_w,
	cd_municipio_ibge_w,
	sg_estado_w,
	nr_ddd_telefone_w,
	nr_ddi_telefone_w,
	nr_telefone_w,
	nr_telefone_celular_w,
	nr_ctps_w,
	nr_serie_ctps_w,
	uf_emissora_ctps_w,
	ds_municipio_w,
	nr_pis_pasep_w,
	nr_ddd_fax_w,
	nr_fax_w,
	cd_declaracao_nasc_vivo_w,
	nr_cartao_nac_sus_w,
	nr_ddd_celular_w,
	nr_cert_casamento_w,
	dt_emissao_cert_casamento_w,
	nr_titulo_eleitor_w,
	cd_tipo_logradouro_w,
	nm_mae_w,
	nm_pai_w,
	dt_adocao_w,
	cd_municipio_ibge_nasc_w,
	nr_seq_intercambio_w,
	sg_estado_nasc_w,
	nm_social_w,
	cd_cbo_w
from	pls_inclusao_beneficiario
where	nr_sequencia	= nr_seq_solicitacao_p;

tb_ie_acao_w.delete;

for r_c01_w in c01 loop
	begin
	tb_ie_acao_w(r_c01_w.nr_sequencia)	:= r_c01_w.ie_acao;
	end;
end loop;
	
if	(ie_gerar_solic_alt_pf_w in ('S', 'R')) then
	select	nm_pessoa_fisica,
		dt_nascimento,
		cd_nacionalidade,
		nr_cpf,
		ie_estado_civil,
		ie_sexo,
		nr_identidade,
		dt_emissao_ci,
		sg_emissora_ci,
		nr_seq_pais,
		ds_orgao_emissor_ci,
		nr_reg_geral_estrang,
		nr_telefone_celular,
		nr_ctps,
		nr_serie_ctps,
		uf_emissora_ctps,
		nr_pis_pasep,
		cd_declaracao_nasc_vivo,
		nr_cartao_nac_sus,
		nr_ddd_celular,
		nr_cert_casamento,
		dt_emissao_cert_casamento,
		nr_titulo_eleitor,
		nm_abreviado,
		dt_adocao,
		cd_municipio_ibge,
		sg_estado_nasc,
		cd_cbo_sus,
		nm_social
	into	nm_pessoa_fisica_old_w,
		dt_nascimento_old_w,
		cd_nacionalidade_old_w,
		nr_cpf_old_w,
		ie_estado_civil_old_w,
		ie_sexo_old_w,
		nr_identidade_old_w,
		dt_emissao_ci_old_w,
		sg_emissora_ci_old_w,
		nr_seq_pais_old_w,
		ds_orgao_emissor_ci_old_w,
		nr_reg_geral_estrang_old_w,
		nr_telefone_celular_old_w,
		nr_ctps_old_w,
		nr_serie_ctps_old_w,
		uf_emissora_ctps_old_w,
		nr_pis_pasep_old_w,
		cd_declaracao_nasc_vivo_old_w,
		nr_cartao_nac_sus_old_w,
		nr_ddd_celular_old_w,
		nr_cert_casamento_old_w,
		dt_emissao_cert_casament_old_w,
		nr_titulo_eleitor_old_w,
		nm_abreviado_old_w,
		dt_adocao_old_w,
		cd_municipio_ibge_nasc_old_w,
		sg_estado_nasc_old_w,
		cd_cbo_old_w,
		nm_social_old_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
	begin
	select	ds_fax,
		nr_ddd_fax,
		ds_municipio,
		nr_ddd_telefone,
		nr_ddi_telefone,
		nr_telefone,
		ds_bairro,
		cd_municipio_ibge,
		sg_estado,
		cd_cep,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		ds_email,
		cd_tipo_logradouro
	into	nr_fax_old_w,
		nr_ddd_fax_old_w,
		ds_municipio_old_w,
		nr_ddd_telefone_old_w,
		nr_ddi_telefone_old_w,
		nr_telefone_old_w,
		ds_bairro_old_w,
		cd_municipio_ibge_old_w,
		sg_estado_old_w,
		cd_cep_old_w,
		ds_endereco_old_w,
		nr_endereco_old_w,
		ds_complemento_old_w,
		ds_email_old_w,
		cd_tipo_logradouro_old_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tipo_complemento	= 1;
	exception
	when others then
		nr_fax_old_w		:= null;
		nr_ddd_fax_old_w	:= null;
		ds_municipio_old_w	:= null;
		nr_ddd_telefone_old_w	:= null;
		nr_ddi_telefone_old_w	:= null;
		nr_telefone_old_w	:= null;
		ds_bairro_old_w		:= null;
		cd_municipio_ibge_old_w	:= null;
		sg_estado_old_w		:= null;
		cd_cep_old_w		:= null;
		ds_endereco_old_w	:= null;
		nr_endereco_old_w	:= null;
		ds_complemento_old_w	:= null;
		ds_email_old_w		:= null;
		cd_tipo_logradouro_old_w:= null;
	end;
	
	begin
	select	nm_contato
	into	nm_mae_old_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tipo_complemento	= 5;
	exception
	when others then
		nm_mae_old_w	:= null;
	end;
	
	begin
	select	nm_contato
	into	nm_pai_old_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tipo_complemento	= 4;
	exception
	when others then
		nm_pai_old_w	:= null;
	end;
	
	if	(cd_municipio_ibge_w is null) and
		(cd_cep_w is not null) then
		cd_cep_ww	:= Somente_Numero(cd_cep_w);
		
		select	max(cd_municipio_ibge)
		into	cd_municipio_ibge_w
		from	cep_municipio
		where	cd_cep	= cd_cep_ww;
	end if;
	
	ie_formatacao_nome_pf_w	:= nvl(obter_valor_param_usuario(1232, 87, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w),'S');
	
	if	(ie_formatacao_nome_pf_w = 'MA') then --Letras maiusculas e sem acentuacao
		nm_pessoa_fisica_w	:= Elimina_Acentuacao(upper(nm_pessoa_fisica_w));
	end if;
	
	if	(trim(cd_cep_w) is not null) then
		begin
		select	max(ds_tipo_logradouro)
		into	ds_tipo_logradouro_w
		from	cep_logradouro_v
		where	cd_cep	= cd_cep_w;
		exception
		when others then
			ds_tipo_logradouro_w	:= '';
		end;

	end if;
	
	/*Retornar os dados do endereco pelo CEP*/
	pls_obter_enderec_homonimo_alt(	cd_cep_w,ds_endereco_w,ds_bairro_w,cd_municipio_ibge_w,ds_municipio_w,sg_estado_w,
						ds_endereco_w,ds_bairro_w,cd_municipio_ibge_w,ds_municipio_w,sg_estado_w,
						cd_estabelecimento_w,nm_usuario_p);
	
	nm_abreviado_w	:= pls_gerar_nome_abreviado(nm_pessoa_fisica_w);
	
	--PESSOA
	if	(tb_ie_acao_w(1) = 'S') and
		(nm_pessoa_fisica_w is not null) and
		(upper(nm_pessoa_fisica_w) <> upper(nm_pessoa_fisica_old_w) or nm_pessoa_fisica_old_w is null) then
		pls_gerar_solicitacao_alt(nm_pessoa_fisica_old_w, nm_pessoa_fisica_w, 'NM_PESSOA_FISICA', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(35) = 'S') and
		(sg_estado_nasc_w is not null) and
		(upper(sg_estado_nasc_w) <> upper(sg_estado_nasc_old_w) or sg_estado_nasc_old_w is null) then
		pls_gerar_solicitacao_alt(sg_estado_nasc_old_w, sg_estado_nasc_w, 'SG_ESTADO_NASC', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;

	if	(tb_ie_acao_w(37) = 'S') and
		(nm_social_w is not null) and
		(upper(nm_social_w) <> upper(nm_social_old_w) or nm_social_old_w is null) then
		pls_gerar_solicitacao_alt(nm_social_old_w, nm_social_w, 'NM_SOCIAL', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(24) = 'S') and
		(cd_cbo_w is not null) and
		(upper(cd_cbo_w) <> upper(cd_cbo_old_w) or cd_cbo_old_w is null) then
		pls_gerar_solicitacao_alt(cd_cbo_old_w, cd_cbo_w, 'CD_CBO_SUS', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(34) = 'S') and
		(cd_municipio_ibge_nasc_w is not null) and
		(upper(cd_municipio_ibge_nasc_w) <> upper(cd_municipio_ibge_nasc_old_w) or cd_municipio_ibge_nasc_old_w is null) then
		pls_gerar_solicitacao_alt(cd_municipio_ibge_nasc_old_w, cd_municipio_ibge_nasc_w, 'CD_MUNICIPIO_IBGE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(2) = 'S') and
		(dt_nascimento_w is not null) and
		(dt_nascimento_w <> dt_nascimento_old_w or dt_nascimento_old_w is null) then
		pls_gerar_solicitacao_alt(dt_nascimento_old_w, dt_nascimento_w, 'DT_NASCIMENTO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(4) = 'S') and
		(cd_nacionalidade_w is not null) and
		(cd_nacionalidade_w <> cd_nacionalidade_old_w or cd_nacionalidade_old_w is null) then
		pls_gerar_solicitacao_alt(cd_nacionalidade_old_w, cd_nacionalidade_w, 'CD_NACIONALIDADE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(7) = 'S') and
		(nr_cpf_w is not null) and
		(nr_cpf_w <> nr_cpf_old_w or nr_cpf_old_w is null) then
		pls_gerar_solicitacao_alt(nr_cpf_old_w, nr_cpf_w, 'NR_CPF', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(3) = 'S') and
		(ie_estado_civil_w is not null) and
		(ie_estado_civil_w <> ie_estado_civil_old_w or ie_estado_civil_old_w is null) then
		pls_gerar_solicitacao_alt(ie_estado_civil_old_w, ie_estado_civil_w, 'IE_ESTADO_CIVIL', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(5) = 'S') and
		(ie_sexo_w is not null) and
		(ie_sexo_w <> ie_sexo_old_w or ie_sexo_old_w is null) then
		pls_gerar_solicitacao_alt(ie_sexo_old_w, ie_sexo_w, 'IE_SEXO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(8) = 'S') and
		(nr_identidade_w is not null) and
		(nr_identidade_w <> nr_identidade_old_w or nr_identidade_old_w is null) then
		pls_gerar_solicitacao_alt(nr_identidade_old_w, nr_identidade_w, 'NR_IDENTIDADE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(9) = 'S') and
		(dt_emissao_ci_w is not null) and
		(dt_emissao_ci_w <> dt_emissao_ci_old_w or dt_emissao_ci_old_w is null) then
		pls_gerar_solicitacao_alt( dt_emissao_ci_old_w, dt_emissao_ci_w, 'DT_EMISSAO_CI', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(10) = 'S') and
		(sg_emissora_ci_w is not null) and
		(sg_emissora_ci_w <> sg_emissora_ci_old_w or sg_emissora_ci_old_w is null) then
		pls_gerar_solicitacao_alt(sg_emissora_ci_old_w, sg_emissora_ci_w, 'SG_EMISSORA_CI', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(12) = 'S') and
		(nr_seq_pais_w is not null) and
		(nr_seq_pais_w <> nr_seq_pais_old_w or nr_seq_pais_old_w is null) then
		pls_gerar_solicitacao_alt(nr_seq_pais_old_w, nr_seq_pais_w, 'NR_SEQ_PAIS', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(11) = 'S') and
		(ds_orgao_emissor_ci_w is not null) and
		(upper(ds_orgao_emissor_ci_w) <> upper(ds_orgao_emissor_ci_old_w) or ds_orgao_emissor_ci_old_w is null) then
		pls_gerar_solicitacao_alt(ds_orgao_emissor_ci_old_w, ds_orgao_emissor_ci_w, 'DS_ORGAO_EMISSOR_CI', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(13) = 'S') and
		(nr_reg_geral_estrang_w is not null) and
		(nr_reg_geral_estrang_w <> nr_reg_geral_estrang_old_w or nr_reg_geral_estrang_old_w is null) then
		pls_gerar_solicitacao_alt(nr_reg_geral_estrang_old_w, nr_reg_geral_estrang_w, 'NR_REG_GERAL_ESTRANG', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(23) = 'S') and
		(nr_telefone_celular_w is not null) and
		(nr_telefone_celular_w <> nr_telefone_celular_old_w or nr_telefone_celular_old_w is null) then
		pls_gerar_solicitacao_alt(nr_telefone_celular_old_w, nr_telefone_celular_w, 'NR_TELEFONE_CELULAR', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(25) = 'S') and
		(nr_ctps_w is not null) and
		(nr_ctps_w <> nr_ctps_old_w or nr_ctps_old_w is null) then
		pls_gerar_solicitacao_alt(nr_ctps_old_w, nr_ctps_w, 'NR_CTPS', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(26) = 'S') and
		(nr_serie_ctps_w is not null) and
		(nr_serie_ctps_w <> nr_serie_ctps_old_w or nr_serie_ctps_old_w is null) then
		pls_gerar_solicitacao_alt(nr_serie_ctps_old_w, nr_serie_ctps_w, 'NR_SERIE_CTPS', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(27) = 'S') and
		(uf_emissora_ctps_w is not null) and
		(uf_emissora_ctps_w <> uf_emissora_ctps_old_w or uf_emissora_ctps_old_w is null) then
		pls_gerar_solicitacao_alt(uf_emissora_ctps_old_w, uf_emissora_ctps_w, 'UF_EMISSORA_CTPS', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(30) = 'S') and
		(nr_pis_pasep_w is not null) and
		(nr_pis_pasep_w <> nr_pis_pasep_old_w or nr_pis_pasep_old_w is null) then
		pls_gerar_solicitacao_alt(nr_pis_pasep_old_w, nr_pis_pasep_w, 'NR_PIS_PASEP', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(29) = 'S') and
		(cd_declaracao_nasc_vivo_w is not null) and
		(cd_declaracao_nasc_vivo_w <> cd_declaracao_nasc_vivo_old_w or cd_declaracao_nasc_vivo_old_w is null) then
		pls_gerar_solicitacao_alt(cd_declaracao_nasc_vivo_old_w, cd_declaracao_nasc_vivo_w, 'CD_DECLARACAO_NASC_VIVO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(28) = 'S') and
		(nr_cartao_nac_sus_w is not null) and
		(nr_cartao_nac_sus_w <> nr_cartao_nac_sus_old_w or nr_cartao_nac_sus_old_w is null) then
		pls_gerar_solicitacao_alt(nr_cartao_nac_sus_old_w, nr_cartao_nac_sus_w, 'NR_CARTAO_NAC_SUS', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(23) = 'S') and
		(nr_ddd_celular_w is not null) and
		(nr_ddd_celular_w <> nr_ddd_celular_old_w or nr_ddd_celular_old_w is null) then
		pls_gerar_solicitacao_alt(nr_ddd_celular_old_w, nr_ddd_celular_w, 'NR_DDD_CELULAR', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(39) = 'S') and
		(nr_cert_casamento_w is not null) and
		(nr_cert_casamento_w <> nr_cert_casamento_old_w or nr_cert_casamento_old_w is null) then
		pls_gerar_solicitacao_alt(nr_cert_casamento_old_w, nr_cert_casamento_w, 'NR_CERT_CASAMENTO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(38) = 'S') and
		(dt_emissao_cert_casamento_w is not null) and
		(dt_emissao_cert_casamento_w <> dt_emissao_cert_casament_old_w or dt_emissao_cert_casament_old_w is null) then
		pls_gerar_solicitacao_alt(dt_emissao_cert_casament_old_w, dt_emissao_cert_casamento_w, 'DT_EMISSAO_CERT_CASAMENTO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(31) = 'S') and
		(nr_titulo_eleitor_w is not null) and
		(nr_titulo_eleitor_w <> nr_titulo_eleitor_old_w or nr_titulo_eleitor_old_w is null) then
		pls_gerar_solicitacao_alt(nr_titulo_eleitor_old_w, nr_titulo_eleitor_w, 'NR_TITULO_ELEITOR', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(1) = 'S') and
		(nm_abreviado_w is not null) and
		(upper(nm_abreviado_w) <> upper(nm_abreviado_old_w) or nm_abreviado_old_w is null) then
		pls_gerar_solicitacao_alt(nm_abreviado_old_w, nm_abreviado_w, 'NM_ABREVIADO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(36) = 'S') and
		(dt_adocao_w is not null) and
		(dt_adocao_w <> dt_adocao_old_w or dt_adocao_old_w is null) then
		pls_gerar_solicitacao_alt(dt_adocao_old_w, dt_adocao_w, 'DT_ADOCAO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;	
	
	pls_tasy_gerar_solicitacao(cd_pessoa_fisica_p,'W');
	
	--COMPLEMENTO
	if	(tb_ie_acao_w(22) = 'S') and
		(nr_fax_w is not null) and
		(nr_fax_w <> nr_fax_old_w or nr_fax_old_w is null) then
		pls_gerar_solicitacao_alt(nr_fax_old_w, nr_fax_w, 'DS_FAX', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(22) = 'S') and
		(nr_ddd_fax_w is not null) and
		(nr_ddd_fax_w <> nr_ddd_fax_old_w or nr_ddd_fax_old_w is null) then
		pls_gerar_solicitacao_alt(nr_ddd_fax_old_w, nr_ddd_fax_w, 'NR_DDD_FAX', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(20) = 'S') and
		(ds_municipio_w is not null) and
		(upper(ds_municipio_w) <> upper(ds_municipio_old_w) or ds_municipio_old_w is null) then
		pls_gerar_solicitacao_alt(ds_municipio_old_w, ds_municipio_w, 'DS_MUNICIPIO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(22) = 'S') and
		(nr_ddd_telefone_w is not null) and
		(nr_ddd_telefone_w <> nr_ddd_telefone_old_w or nr_ddd_telefone_old_w is null) then
		pls_gerar_solicitacao_alt(nr_ddd_telefone_old_w, nr_ddd_telefone_w, 'NR_DDD_TELEFONE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(22) = 'S') and
		(nr_ddi_telefone_w is not null) and
		(nr_ddi_telefone_w <> nr_ddi_telefone_old_w or nr_ddi_telefone_old_w is null) then
		pls_gerar_solicitacao_alt(nr_ddi_telefone_old_w, nr_ddi_telefone_w, 'NR_DDI_TELEFONE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(22) = 'S') and
		(nr_telefone_w is not null) and
		(nr_telefone_w <> nr_telefone_old_w or nr_telefone_old_w is null) then
		pls_gerar_solicitacao_alt(nr_telefone_old_w, nr_telefone_w, 'NR_TELEFONE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(19) = 'S') and
		(ds_bairro_w is not null) and
		(upper(ds_bairro_w) <> upper(ds_bairro_old_w) or ds_bairro_old_w is null) then
		pls_gerar_solicitacao_alt(ds_bairro_old_w, ds_bairro_w, 'DS_BAIRRO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(20) = 'S') and
		(cd_municipio_ibge_w is not null) and
		(cd_municipio_ibge_w <> cd_municipio_ibge_old_w or cd_municipio_ibge_old_w is null) then
		pls_gerar_solicitacao_alt(cd_municipio_ibge_old_w, cd_municipio_ibge_w, 'CD_MUNICIPIO_IBGE', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(21) = 'S') and
		(sg_estado_w is not null) and
		(sg_estado_w <> sg_estado_old_w or sg_estado_old_w is null) then
		pls_gerar_solicitacao_alt(sg_estado_old_w, sg_estado_w, 'SG_ESTADO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(15) = 'S') and
		(cd_cep_w is not null) and
		(cd_cep_w <> cd_cep_old_w or cd_cep_old_w is null) then
		pls_gerar_solicitacao_alt(cd_cep_old_w, cd_cep_w, 'CD_CEP', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(16) = 'S') and
		(ds_endereco_w is not null) and
		(upper(ds_endereco_w) <> upper(ds_endereco_old_w) or ds_endereco_old_w is null) then
		pls_gerar_solicitacao_alt( ds_endereco_old_w, ds_endereco_w, 'DS_ENDERECO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(17) = 'S') and
		(nr_endereco_w is not null) and
		(nr_endereco_w <> nr_endereco_old_w or nr_endereco_old_w is null) then
		pls_gerar_solicitacao_alt(nr_endereco_old_w, nr_endereco_w, 'NR_ENDERECO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(18) = 'S') and
		(ds_complemento_w is not null) and
		(upper(ds_complemento_w) <> upper(ds_complemento_old_w) or ds_complemento_old_w is null) then
		pls_gerar_solicitacao_alt(ds_complemento_old_w, ds_complemento_w, 'DS_COMPLEMENTO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(14) = 'S') and
		(ds_email_w is not null) and
		(upper(ds_email_w) <> upper(ds_email_old_w) or ds_email_old_w is null) then
		pls_gerar_solicitacao_alt(ds_email_old_w, ds_email_w, 'DS_EMAIL', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(tb_ie_acao_w(32) = 'S') and
		(cd_tipo_logradouro_w is not null) and
		(cd_tipo_logradouro_w <> cd_tipo_logradouro_old_w or cd_tipo_logradouro_old_w is null) then
		pls_gerar_solicitacao_alt(cd_tipo_logradouro_old_w, cd_tipo_logradouro_w, 'CD_TIPO_LOGRADOURO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	pls_tasy_gerar_solic_compl(cd_pessoa_fisica_p, 1,'W','PLS_VINCULAR_PESSOA_INCLUSAO');
	
	--Endereco da MAE
	if	(tb_ie_acao_w(6) = 'S') and
		(nm_mae_w is not null) and
		(upper(nm_mae_w) <> upper(nm_mae_old_w) or nm_mae_old_w is null) then
		pls_gerar_solicitacao_alt(nm_mae_old_w, nm_mae_w, 'NM_CONTATO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;	
	pls_tasy_gerar_solic_compl(cd_pessoa_fisica_p, 5,'W','PLS_VINCULAR_PESSOA_INCLUSAO');
	
	--Endereco da Pai
	if	(tb_ie_acao_w(33) = 'S') and
		(nm_pai_w is not null) and
		(upper(nm_pai_w) <> upper(nm_pai_old_w) or nm_pai_old_w is null) then
		pls_gerar_solicitacao_alt(nm_pai_old_w, nm_pai_w, 'NM_CONTATO', cd_pessoa_fisica_p, cd_estabelecimento_w, nm_usuario_p);
	end if;
	pls_tasy_gerar_solic_compl(cd_pessoa_fisica_p, 4,'W','PLS_VINCULAR_PESSOA_INCLUSAO');
end if;
	
if	(ie_gerar_solic_alt_pf_w in ('A','R')) then
	update 	pessoa_fisica 
	set 	nm_pessoa_fisica 		= decode(tb_ie_acao_w(1), 'A', nvl(nm_pessoa_fisica_w,nm_pessoa_fisica), nm_pessoa_fisica),
		dt_nascimento 			= decode(tb_ie_acao_w(2), 'A', nvl(dt_nascimento_w,dt_nascimento), dt_nascimento),
		cd_nacionalidade		= decode(tb_ie_acao_w(4), 'A', nvl(cd_nacionalidade_w,cd_nacionalidade), cd_nacionalidade),
		nr_cpf				= decode(tb_ie_acao_w(7), 'A', nvl(nr_cpf_w,nr_cpf), nr_cpf),
		ie_estado_civil			= decode(tb_ie_acao_w(3), 'A', nvl(ie_estado_civil_w,ie_estado_civil), ie_estado_civil),
		ie_sexo				= decode(tb_ie_acao_w(5), 'A', nvl(ie_sexo_w,ie_sexo), ie_sexo),
		nr_identidade			= decode(tb_ie_acao_w(8), 'A', nvl(nr_identidade_w,nr_identidade), nr_identidade),
		dt_emissao_ci			= decode(tb_ie_acao_w(9), 'A', nvl(dt_emissao_ci_w,dt_emissao_ci), dt_emissao_ci),
		sg_emissora_ci			= decode(tb_ie_acao_w(10), 'A', nvl(sg_emissora_ci_w,sg_emissora_ci), sg_emissora_ci),
		nr_seq_pais			= decode(tb_ie_acao_w(12), 'A', nvl(nr_seq_pais_w,nr_seq_pais), nr_seq_pais),
		ds_orgao_emissor_ci		= decode(tb_ie_acao_w(11), 'A', nvl(ds_orgao_emissor_ci_w,ds_orgao_emissor_ci), ds_orgao_emissor_ci),
		nr_reg_geral_estrang		= decode(tb_ie_acao_w(13), 'A', nvl(nr_reg_geral_estrang_w,nr_reg_geral_estrang), nr_reg_geral_estrang),
		nr_telefone_celular		= decode(tb_ie_acao_w(23), 'A', nvl(nr_telefone_celular_w,nr_telefone_celular), nr_telefone_celular),
		nr_ctps				= decode(tb_ie_acao_w(25), 'A', nvl(nr_ctps_w,nr_ctps), nr_ctps),
		nr_serie_ctps			= decode(tb_ie_acao_w(26), 'A', nvl(nr_serie_ctps_w,nr_serie_ctps), nr_serie_ctps),
		uf_emissora_ctps		= decode(tb_ie_acao_w(27), 'A', nvl(uf_emissora_ctps_w,uf_emissora_ctps), uf_emissora_ctps),
		nr_pis_pasep			= decode(tb_ie_acao_w(30), 'A', nvl(nr_pis_pasep_w,nr_pis_pasep), nr_pis_pasep),
		cd_declaracao_nasc_vivo		= decode(tb_ie_acao_w(29), 'A', nvl(cd_declaracao_nasc_vivo_w,cd_declaracao_nasc_vivo), cd_declaracao_nasc_vivo),
		nr_cartao_nac_sus		= decode(tb_ie_acao_w(28), 'A', nvl(nr_cartao_nac_sus_w,nr_cartao_nac_sus), nr_cartao_nac_sus),
		nr_ddd_celular			= decode(tb_ie_acao_w(23), 'A', nvl(nr_ddd_celular_w,nr_ddd_celular), nr_ddd_celular),
		nr_cert_casamento		= decode(tb_ie_acao_w(39), 'A', nvl(nr_cert_casamento_w,nr_cert_casamento), nr_cert_casamento),
		dt_emissao_cert_casamento	= decode(tb_ie_acao_w(38), 'A', nvl(dt_emissao_cert_casamento_w,dt_emissao_cert_casamento), dt_emissao_cert_casamento),
		nr_titulo_eleitor		= decode(tb_ie_acao_w(31), 'A', nvl(nr_titulo_eleitor_w,nr_titulo_eleitor), nr_titulo_eleitor),
		nm_abreviado			= decode(tb_ie_acao_w(1), 'A', nvl(nm_abreviado_w,nm_abreviado), nm_abreviado),
		dt_adocao			= decode(tb_ie_acao_w(36), 'A', nvl(dt_adocao_w,dt_adocao), dt_adocao),
		cd_municipio_ibge		= decode(tb_ie_acao_w(34), 'A', nvl(cd_municipio_ibge_nasc_w,cd_municipio_ibge), cd_municipio_ibge),
		sg_estado_nasc			= decode(tb_ie_acao_w(35), 'A', nvl(sg_estado_nasc_w,sg_estado_nasc), sg_estado_nasc),
		cd_cbo_sus			= decode(tb_ie_acao_w(24), 'A', nvl(cd_cbo_w,cd_cbo_sus), cd_cbo_sus),
		nm_social			= decode(tb_ie_acao_w(37), 'A', nvl(nm_social_w,nm_social), nm_social),
		dt_atualizacao			= sysdate,
		nm_usuario			= nm_usuario_p
	where	cd_pessoa_fisica		= cd_pessoa_fisica_p;
	
	update	compl_pessoa_fisica
	set	ds_fax 				= decode(tb_ie_acao_w(22), 'A', nvl(nr_fax_w,ds_fax), ds_fax),
		nr_ddd_fax 			= decode(tb_ie_acao_w(22), 'A', nvl(nr_ddd_fax_w,nr_ddd_fax), nr_ddd_fax),
		ds_municipio 			= decode(tb_ie_acao_w(20), 'A', nvl(ds_municipio_w,ds_municipio), ds_municipio),
		nr_ddd_telefone			= decode(tb_ie_acao_w(22), 'A', nvl(nr_ddd_telefone_w,nr_ddd_telefone), nr_ddd_telefone),
		nr_ddi_telefone			= decode(tb_ie_acao_w(22), 'A', nvl(nr_ddi_telefone_w,nr_ddi_telefone), nr_ddi_telefone),
		nr_telefone			= decode(tb_ie_acao_w(22), 'A', nvl(nr_telefone_w,nr_telefone), nr_telefone),
		ds_bairro			= decode(tb_ie_acao_w(19), 'A', nvl(ds_bairro_w,ds_bairro), ds_bairro),
		cd_municipio_ibge		= decode(tb_ie_acao_w(22), 'A', nvl(cd_municipio_ibge_w,cd_municipio_ibge), cd_municipio_ibge),
		sg_estado			= decode(tb_ie_acao_w(21), 'A', nvl(sg_estado_w,sg_estado), sg_estado),
		cd_cep				= decode(tb_ie_acao_w(15), 'A', nvl(cd_cep_w,cd_cep), cd_cep),
		ds_endereco			= decode(tb_ie_acao_w(16), 'A', nvl(ds_endereco_w,ds_endereco), ds_endereco),
		nr_endereco			= decode(tb_ie_acao_w(17), 'A', nvl(nr_endereco_w,nr_endereco), nr_endereco),
		ds_complemento			= decode(tb_ie_acao_w(18), 'A', nvl(ds_complemento_w,ds_complemento), ds_complemento),
		ds_email			= decode(tb_ie_acao_w(14), 'A', nvl(ds_email_w,ds_email), ds_email),
		cd_tipo_logradouro		= decode(tb_ie_acao_w(32), 'A', nvl(cd_tipo_logradouro_w,cd_tipo_logradouro), cd_tipo_logradouro),
		dt_atualizacao			= sysdate,
		nm_usuario			= nm_usuario_p
	where	cd_pessoa_fisica		= cd_pessoa_fisica_p
	and	ie_tipo_complemento		= 1;	
	
	select	nvl(max(nr_sequencia), 0) + 1
	into	nr_seq_complemento_pf_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if	((nm_mae_w is not null) and (tb_ie_acao_w(6) = 'A')) then
		select	count(1)
		into	qt_complemento_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_tipo_complemento	= 5;
		
		if	(qt_complemento_w > 0) then
			update	compl_pessoa_fisica
			set	nm_contato		= decode(tb_ie_acao_w(6), 'A', nvl(nm_mae_w,nm_contato), nm_contato)
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	ie_tipo_complemento	= 5;
		else
			insert into compl_pessoa_fisica
				(cd_pessoa_fisica, nr_sequencia, ie_tipo_complemento,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
				nm_usuario_nrec, nm_contato)
			values(	cd_pessoa_fisica_p, nr_seq_complemento_pf_w, 5,
				sysdate, nm_usuario_p, sysdate,
				nm_usuario_p, nm_mae_w);
				
			nr_seq_complemento_pf_w	:= nr_seq_complemento_pf_w + 1;
		end if;
	end if;
	
	if	((nm_pai_w is not null) and (tb_ie_acao_w(33) = 'A')) then
		select	count(1)
		into	qt_complemento_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_tipo_complemento	= 4;
		
		if	(qt_complemento_w > 0) then
			update	compl_pessoa_fisica
			set	nm_contato			= decode(tb_ie_acao_w(33), 'A', nvl(nm_pai_w,nm_contato), nm_contato)
			where	cd_pessoa_fisica		= cd_pessoa_fisica_p
			and	ie_tipo_complemento		= 4;
		else
			insert into compl_pessoa_fisica
				(cd_pessoa_fisica, nr_sequencia, ie_tipo_complemento,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
				nm_usuario_nrec, nm_contato)
			values(	cd_pessoa_fisica_p, nr_seq_complemento_pf_w, 4,
				sysdate, nm_usuario_p, sysdate,
				nm_usuario_p, nm_pai_w);
		end if;
	end if;
end if;

--Quando for intercambio alterar a etapa de solicitacao de intercambio
update	pls_inclusao_beneficiario
set	cd_pessoa_fisica		= cd_pessoa_fisica_p,
	nm_usuario			= nm_usuario_p,
	ie_etapa_solicitacao		= decode(nr_seq_intercambio_w, null, 'CV', ie_etapa_solicitacao),
	ie_etapa_solic_intercambio	= decode(nr_seq_intercambio_w, null, ie_etapa_solic_intercambio, 'CV'),
	dt_atualizacao			= sysdate
where	nr_sequencia			= nr_seq_solicitacao_p;

--Apos vincular a pessoa deve consistir novamente a inclusao, pois existe a consistencia [5] - Ja existe beneficiario cadastrado no contrato com a pessoa fisica vinculada
pls_consistir_inclusao_benef(null, nr_seq_solicitacao_p, ie_consistir_sib_w, 'S', null, nm_usuario_p, 'T');

commit;

end pls_vincular_pessoa_inclusao;
/