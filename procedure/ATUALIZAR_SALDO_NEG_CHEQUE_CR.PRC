create or replace
procedure atualizar_saldo_neg_cheque_cr(nr_seq_cheque_p		number,
					nm_usuario_p		varchar2) is

-- EDGAR 25/09/2008, N�O DAR COMMIT

vl_cheque_w		number(15,2);
vl_saldo_negociado_w	number(15,2);
vl_negociado_w		number(15,2);
vl_novo_saldo_w		number(15,2);
cont_w			number(10,0);
vl_transacao_w		number(15,2);
vl_perda_w		number(15,2);
vl_transacao_perda_w	number(15,2);
qt_perda_w		number(10);
qt_movto_perda_w	number(10);
vl_baixa_perdas_canc_w		perda_contas_receb_baixa.vl_baixa%type;

begin

select	max(vl_cheque),
	max(vl_saldo_negociado)
into	vl_cheque_w,
	vl_saldo_negociado_w
from	cheque_cr
where	nr_seq_cheque	= nr_seq_cheque_p;

select	count(*),
	nvl(sum(nvl(vl_negociado,0) + nvl(vl_desconto,0)),0)
into	cont_w,
	vl_negociado_w
from	cheque_cr_negociado
where	nr_seq_cheque	= nr_seq_cheque_p;

select	nvl(sum(a.vl_transacao),0)
into	vl_transacao_w
from	caixa_receb c,
	transacao_financeira b,
	movto_trans_financ a
where	nvl(c.ie_tipo_receb,'R')	<> 'C'
and	a.nr_seq_caixa_rec		= c.nr_sequencia(+)
and	b.ie_acao			= 30
and	a.nr_seq_trans_financ		= b.nr_sequencia
and	a.nr_seq_cheque			= nr_seq_cheque_p;

select	count(*),
	nvl(sum(a.vl_perda),0)
into	qt_perda_w,
	vl_perda_w
from	caixa_receb b,
	cheque_cr_perda a
where	b.dt_cancelamento		is null
and	b.dt_fechamento			is null
and	nvl(b.ie_tipo_receb,'R')	= 'P'
and	a.nr_seq_caixa_rec		= b.nr_sequencia
and	a.nr_seq_cheque			= nr_seq_cheque_p;

select	count(*),
	nvl(sum(a.vl_transacao),0)
into	qt_movto_perda_w,
	vl_transacao_perda_w
from	transacao_financeira b,
	movto_trans_financ a
where	b.ie_acao			= 33
and	a.nr_seq_trans_financ		= b.nr_sequencia
and	a.nr_seq_cheque			= nr_seq_cheque_p;

/*OS 1799459 Se o cheque tiver baixa por perda, verificar se a perda foi cancelada, para devolver o saldo negociado ao cheque e poder ser negociado novamente*/
if (vl_transacao_perda_w > 0) then
	
	select	sum(nvl(b.vl_baixa,0))
	into	vl_baixa_perdas_canc_w
	from	perda_contas_receber a,
			perda_contas_receb_baixa b,
			fin_tipo_baixa_perda c
	where	a.nr_seq_cheque			= nr_seq_cheque_p  
	and		a.nr_sequencia			= b.nr_seq_perda
	and		b.nr_seq_tipo_baixa 	= c.nr_sequencia
	and		c.ie_tipo_consistencia 	= '4'; -- Tipo de cancelamento com consist�ncia de cancelamento, onde devolve o saldo a perda

	if (nvl(vl_baixa_perdas_canc_w,0) <= nvl(vl_transacao_perda_w,0)) then
		vl_transacao_perda_w := vl_transacao_perda_w - nvl(vl_baixa_perdas_canc_w,0);
	end if;

end if;

if	(cont_w > 0) or (vl_transacao_w > 0) or (qt_perda_w > 0) or (qt_movto_perda_w > 0) then

	if	(vl_negociado_w > 0) or (vl_transacao_w > 0) or (qt_perda_w > 0) or (qt_movto_perda_w > 0) then

		/*vl_novo_saldo_w	:= nvl(vl_saldo_negociado_w,vl_cheque_w) - vl_negociado_w;
		Francisco - OS 171678 - 06/11/2009  - Troquei pela linha abaixo */
		vl_novo_saldo_w	:= nvl(vl_cheque_w,0) - nvl(vl_negociado_w,0) - nvl(vl_transacao_w,0) - nvl(vl_perda_w,0) - nvl(vl_transacao_perda_w,0);
	else
		/* Posse hospital */
		if	(obter_status_cheque(nr_seq_cheque_p) in (1,3,5,10)) then
			vl_novo_saldo_w	:= vl_cheque_w;
		/* Fora do hospital, sacado ou vendido p/terceiros */
		else
			vl_novo_saldo_w	:= 0; 
		end if;
	end if;
else
	vl_novo_saldo_w	:= vl_cheque_w;
end if;

update	cheque_cr
set	vl_saldo_negociado	= vl_novo_saldo_w
where	nr_seq_cheque		= nr_seq_cheque_p;

--commit;

end;
/