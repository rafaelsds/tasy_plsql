create or replace
procedure alterar_status_diretriz(	nr_seq_diretriz_p number,
					ie_status_p	varchar2,
					ds_observacao_p	varchar2 default null,
					nm_usuario_p	varchar2) is 
					
ie_status_w		diretriz_atendimento.ie_status%type;	
nr_atendimento_w	diretriz_atendimento.nr_atendimento%type;	
					
begin				

if (nr_seq_diretriz_p is not null) then
	
	select 	ie_status,
		nr_atendimento
	into 	ie_status_w,
		nr_atendimento_w
	from 	diretriz_atendimento
	where 	nr_sequencia = nr_seq_diretriz_p;
	
	if (ie_status_p = 'C') then 
		
		update 	diretriz_atendimento
		set 	ie_status = ie_status_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_diretriz_p;
		
	elsif (ie_status_p = 'F') then
	
		update 	diretriz_atendimento
		set 	ie_status = ie_status_p,
			dt_final = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_diretriz_p;		
		
	elsif (ie_status_p = 'P') then		
	
		update 	diretriz_atendimento
		set 	ie_status = ie_status_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_diretriz_p
		and	ie_status = 'C';
		
	end if;
	
	if(ie_status_w <> ie_status_p) then
		insert into diretriz_atendimento_log(
		nr_sequencia,
		nr_seq_diretriz,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_status_old,
		ie_status,
		ds_observacao,
		nr_atendimento)
	values (
		diretriz_atendimento_log_seq.nextval,
		nr_seq_diretriz_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_status_w,
		ie_status_p,
		ds_observacao_p,
		nr_atendimento_w);
	end if;
end if;
				
end alterar_status_diretriz;
/
