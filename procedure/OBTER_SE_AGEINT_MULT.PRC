create or replace
function obter_se_ageint_mult(nr_seq_agenda_cons_p number,
			      nr_seq_agenda_exame_p number)
	return varchar is

ie_possui_w		varchar2(255) := 'N';

begin

if	(nr_seq_agenda_cons_p is not null) or
	(nr_seq_agenda_exame_p is not null) then
	select 	nvl(max('S'),'N')
	into	ie_possui_w
	from 	agenda_integrada_item
	where 	ie_origem_agendamento in ('CP','MS')
	and    ((nr_seq_agenda_cons = nr_seq_agenda_cons_p) or
	           (nr_seq_agenda_exame = nr_seq_agenda_exame_p));
end if;

return ie_possui_w;

end obter_se_ageint_mult;
/