create or replace
procedure pls_obter_valores_agravo
			(	nr_seq_agravo_p		in	Number,
				ie_tipo_agravo_p	in	Varchar2,
				qt_parcelas_p		out	Number,
				vl_agravo_total_p	out	Number,
				cd_estabelecimento_p	in	Number,
				nm_usuario_p		in	Varchar2) is 

qt_parcelas_w			Number(10);
vl_agravo_total_w		Number(15,2);
qt_regras_w			Number(10);

begin

select	count(*)
into	qt_regras_w
from	pls_agravo_lote_valor	a,
	pls_agravo_valor	b
where	b.nr_seq_lote		= a.nr_sequencia
and	a.nr_seq_agravo		= nr_seq_agravo_p
and	((a.ie_classificacao	= ie_tipo_agravo_p) or (a.ie_classificacao is null))
and	sysdate between nvl(a.dt_inicio_vigencia,sysdate) and fim_dia(nvl(a.dt_fim_vigencia,sysdate));

if	(qt_regras_w = 1) then
	select	max(b.qt_parcelas),
		max(b.vl_agravo)
	into	qt_parcelas_w,
		vl_agravo_total_w
	from	pls_agravo_lote_valor	a,
		pls_agravo_valor	b
	where	b.nr_seq_lote		= a.nr_sequencia
	and	a.nr_seq_agravo		= nr_seq_agravo_p
	and	((a.ie_classificacao	= ie_tipo_agravo_p) or (a.ie_classificacao is null))
	and	sysdate between nvl(a.dt_inicio_vigencia,sysdate) and fim_dia(nvl(a.dt_fim_vigencia,sysdate));
end if;

qt_parcelas_p		:= qt_parcelas_w;
vl_agravo_total_p	:= vl_agravo_total_w;

commit;

end pls_obter_valores_agravo;
/