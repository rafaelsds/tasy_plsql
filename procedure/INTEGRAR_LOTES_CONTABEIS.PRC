create or replace
procedure integrar_lotes_contabeis(
	nm_usuario_p usuario.nm_usuario%type,
	nr_lote_contabil_p lote_contabil.nr_lote_contabil%type
	) is 
reg_integracao_p		gerar_int_padrao.reg_integracao;
begin

reg_integracao_p.ie_operacao :=	'I';
reg_integracao_p.ds_id_origin := ''; -- Verificar o que deve ser colocado

-- 407 = o Evento que criamos no dom�nio
gerar_int_padrao.gravar_integracao('407', nr_lote_contabil_p,nm_usuario_p, reg_integracao_p);

end integrar_lotes_contabeis;
/