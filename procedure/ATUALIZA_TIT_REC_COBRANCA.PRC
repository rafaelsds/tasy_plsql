create or replace
procedure atualiza_tit_rec_cobranca(	nr_seq_cobranca_p	number,
					nr_titulo_p		number,
					ie_atualiza_p		varchar2,
					nm_usuario_p		varchar2) is

nr_seq_conta_banco_w	number(10);
cd_banco_w		number(3);
cd_agencia_bancaria_w	varchar2(8);
cd_conta_w		banco_estabelecimento.cd_conta%type;
ie_digito_conta_w	varchar2(2);
nr_seq_conta_titulo_w	number(10);
nr_seq_carteira_cobr_w	number(10);
nr_seq_carteira_tit_w	number(10);
cd_estabelecimento_w	number(4);
ie_alterar_banco_w	varchar2(1);
nr_titulo_w		number(10);

cursor	c01 is
select	a.nr_seq_carteira_cobr,
	a.nr_seq_conta_banco,
	a.nr_titulo
from	titulo_receber a
where	a.nr_titulo		= nr_titulo_p
and	ie_atualiza_p		= 'S'
union
select	b.nr_seq_carteira_cobr,
	b.nr_seq_conta_banco,
	b.nr_titulo
from	titulo_receber b,
	titulo_receber_cobr a
where	a.nr_titulo		= b.nr_titulo
and	a.nr_seq_cobranca	= nr_seq_cobranca_p
and	ie_atualiza_p		= 'N';


begin

select	max(a.nr_seq_conta_banco),
	max(a.nr_seq_carteira_cobr),
	max(a.cd_estabelecimento)
into	nr_seq_conta_banco_w,
	nr_seq_carteira_cobr_w,
	cd_estabelecimento_w
from	cobranca_escritural a
where	a.nr_sequencia	= nr_seq_cobranca_p;

select	max(a.cd_banco),
	max(a.cd_agencia_bancaria),
	max(a.cd_conta),
	nvl(max(a.ie_digito_conta),'0')
into	cd_banco_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	ie_digito_conta_w
from	banco_estabelecimento a
where	a.nr_sequencia	= nr_seq_conta_banco_w;

obter_param_usuario(815,4,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_alterar_banco_w);

update	titulo_receber_cobr
set	cd_banco		= cd_banco_w,
	cd_agencia_bancaria	= cd_agencia_bancaria_w,
	nr_conta		= cd_conta_w,
	ie_digito_conta		= ie_digito_conta_w,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	(cd_banco is null or nr_conta is null or cd_agencia_bancaria is null)
and	nr_seq_cobranca		= nr_seq_cobranca_p;

if	(ie_atualiza_p	= 'N') and (nvl(ie_alterar_banco_w,'N') <> 'N') then

	if	(cd_conta_w		is not null) then

		update	titulo_receber_cobr
		set	nr_conta		= cd_conta_w,
			ie_digito_conta		= ie_digito_conta_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_seq_cobranca		= nr_seq_cobranca_p;

	end if;

	if	(cd_agencia_bancaria_w	is not null) then

		update	titulo_receber_cobr
		set	cd_agencia_bancaria	= cd_agencia_bancaria_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_seq_cobranca		= nr_seq_cobranca_p;

	end if;

end if;

open	c01;
loop
fetch	c01 into
	nr_seq_carteira_tit_w,
	nr_seq_conta_titulo_w,
	nr_titulo_w;
exit	when c01%notfound;

	if	((nr_seq_carteira_tit_w is null) and (nr_seq_conta_titulo_w is null)) or
		(ie_atualiza_p = 'N' and nvl(ie_alterar_banco_w,'N') in ('S','R','C')) then
		
		if	(ie_alterar_banco_w = 'C') then --Somente conta banc�ria
		
			update	titulo_receber
			set	nr_seq_conta_banco	= nr_seq_conta_banco_w,				
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_titulo		= nr_titulo_w;
			
		elsif	(ie_alterar_banco_w = 'R') then --Somente carteiroa
		
			update	titulo_receber
			set	nr_seq_carteira_cobr	= nr_seq_carteira_cobr_w,		
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_titulo		= nr_titulo_w;
			
		else	--Ambos
		
			update	titulo_receber
			set	nr_seq_conta_banco	= nr_seq_conta_banco_w,
				nr_seq_carteira_cobr	= nr_seq_carteira_cobr_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_titulo		= nr_titulo_w;
		end if;

	end if;

end	loop;
close	c01;

commit;

end atualiza_tit_rec_cobranca;
/
