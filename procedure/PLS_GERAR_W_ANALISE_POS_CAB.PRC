create or replace
procedure pls_gerar_w_analise_pos_cab
			(	nr_seq_analise_p	number,
				nr_seq_grupo_p		number,
				nm_usuario_p		varchar2,
				nr_id_transacao_p	in out w_pls_analise_item.nr_id_transacao%type) is

nm_beneficiario_w		varchar2(255);
cd_usuario_plano_w		varchar2(255);
ds_idade_benef_w		varchar2(255);
ds_tipo_guia_analise_w		varchar2(255);
nm_prestador_solic_w		varchar2(255);
ds_senha_w			varchar2(255);
ds_mes_ref_lote_w		varchar2(255);
ds_tipo_acomodacao_benef_w	varchar2(255);
nm_profissional_solic_w		varchar2(255);
ds_motivo_saida_w		varchar2(255);
ds_origem_conta_w		varchar2(255);
nm_profissional_w		varchar2(255);
ds_formacao_preco_w		varchar2(255);
ds_tipo_apres_guia_w		varchar2(255);
nm_prestador_atend_w		varchar2(255);
ds_carater_solicitacao_w	varchar2(255);
ds_tipo_internacao_w		varchar2(255);
ds_tipo_acomodacao_atend_w	varchar2(255);
ds_tipo_saida_w			varchar2(255);
ds_tipo_atendimento_w		varchar2(255);
nm_operadora_interc_w		varchar2(255);
ds_origem_analise_w		varchar2(255);
cd_operadora_interc_w		varchar2(30);
nr_lote_prestador_w		varchar2(30);
ds_cid_principal_w		varchar2(30);
ds_data_atend_w			varchar2(30);
ds_criacao_analise_w		varchar2(30);
ds_entrada_internacao_w		varchar2(30);
ds_saida_internacao_w		varchar2(30);
nr_fatura_ptu_w			ptu_fatura.nr_fatura%type;
cd_guia_principal_w		varchar2(30);
cd_guia_w			varchar2(20);
ie_tipo_guia_w			varchar2(2);
ie_pre_analise_w		varchar2(1);
nr_seq_prestador_exec_w		number(10);
nr_seq_prestador_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_conta_w			number(10);
ie_origem_analise_w		number(10);
nr_seq_grupo_atual_w		number(10);
cd_estabelecimento_w		number(10);
cd_usuario_plano_aux_w		varchar2(255);
qt_conta_cabecalho_w		pls_integer;
nr_seq_pos_cabecalho_w		pls_conta_pos_cabecalho.nr_sequencia%type;
cd_guia_pos_estab_w		pls_conta.cd_guia_pos_estab%type;
nr_seq_conta_ref_pos_w		pls_conta.nr_sequencia%type;
ie_tipo_conta_w			pls_conta.ie_tipo_conta%type;
nr_seq_guia_w			pls_conta.nr_seq_guia%type;
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a visualização (tabela temporária) do cabeçalho da análise de pós-estabelecido
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */  

begin

if	(nvl(nr_id_transacao_p,0) = 0) then
	select 	pls_id_analise_seq.nextval
	into	nr_id_transacao_p 
	from 	dual;
else
	delete	from w_pls_analise_cabecalho
	where 	nr_id_transacao	= nr_id_transacao_p;
	
	delete 	w_pls_analise_total
	where	nr_id_transacao	= nr_id_transacao_p;
	
	delete 	w_pls_analise_glosa_ocor
	where	nr_id_transacao	= nr_id_transacao_p;
	
end if;

select	pls_obter_conta_principal(a.cd_guia, a.nr_sequencia, a.nr_seq_segurado, a.nr_seq_prestador),
	a.ie_origem_analise,
	substr(obter_valor_dominio(4572, nvl(a.ie_origem_analise,1)),1,255),
	nvl(ie_pre_analise,'N'),
	a.cd_estabelecimento
into	nr_seq_conta_w,
	ie_origem_analise_w,
	ds_origem_analise_w,
	ie_pre_analise_w,
	cd_estabelecimento_w
from	pls_analise_conta	a
where	a.nr_sequencia	= nr_seq_analise_p;

select	max(a.ie_tipo_guia)
into	ie_tipo_guia_w
from	pls_conta	a
where	a.nr_sequencia	= nr_seq_conta_w;

select	count(1)
into	qt_conta_cabecalho_w
from	pls_conta_pos_cabecalho
where	nr_seq_conta	= nr_seq_conta_w;

if	(qt_conta_cabecalho_w = 0) then
	/* Francisco - 21/06/2013 - OS 609623 */
	pls_gerar_conta_pos_cabecalho(nr_seq_conta_w, null, nm_usuario_p);
end if;

--Verifica tem referencia a outra a guia de pos
select 	max(cd_guia_pos_estab)
into	cd_guia_pos_estab_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_w;

if	(cd_guia_pos_estab_w is not null) then
	
	select	max(nr_sequencia)
	into	nr_seq_conta_ref_pos_w
	from	pls_conta
	where	cd_guia_ok = cd_guia_pos_estab_w;
	
	if	(nr_seq_conta_ref_pos_w is not null) then
		nr_seq_conta_w := nr_seq_conta_ref_pos_w;
	end if;
end if;

select	max(c.nr_sequencia),
	max(a.ie_tipo_conta),
	max(a.nr_seq_guia)
into	nr_seq_pos_cabecalho_w,
	ie_tipo_conta_w,
	nr_seq_guia_w
from	pls_conta		a,
	pls_conta_pos_cabecalho c
where	a.nr_sequencia		= nr_seq_conta_w
and	a.nr_sequencia		= c.nr_seq_conta;
	
if	(ie_tipo_guia_w = '3') then /* Consulta */

	select	substr('(' || decode(pls_obter_dados_segurado(a.nr_seq_segurado,'C'), 0, cd_usuario_plano_imp, pls_obter_dados_segurado(a.nr_seq_segurado,'C')) || ') ',1,255) ,
		substr(decode(nvl(pls_obter_dados_segurado(a.nr_seq_segurado,'N'),'X'), 'X', pls_desc_benef_intercambio(nr_seq_nota_cobranca), 
		pls_obter_dados_segurado(a.nr_seq_segurado,'N')),1,255),
		substr(pls_obter_idade_segurado(a.nr_seq_segurado, sysdate,'D'),1,30),
		substr('(' || trim(pls_obter_dados_segurado(a.nr_seq_segurado, 'CON')) || ')  - ' ,1,30),
		substr(obter_valor_dominio(1746,a.ie_tipo_guia),1,255), 
		substr(pls_obter_cod_prestador(a.nr_seq_prestador, null) || ' - ' || pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,255),
		substr(a.cd_senha || ' / ' || a.cd_senha_externa,1,30),
		to_char(dt_mes_competencia,'mm/yyyy'),
		substr(pls_obter_acomodacoes_plano(pls_obter_dados_segurado(a.nr_seq_segurado,'NRP')),1,255),
		substr(cd_medico_solicitante || ' - ' || obter_nome_medico(cd_medico_solicitante,'N'),1,255),  
		substr(a.nr_protocolo_prestador,1,30),
		(	select	substr(nvl(max(x.ds_motivo_saida),''),1,255)
			from	pls_motivo_saida_consulta	x
			where	x.nr_sequencia	= a.nr_seq_saida_consulta),
		(	select	substr(max(nvl(x.cd_doenca, x.cd_doenca_imp)),1,30)
			from	pls_diagnostico_conta	x
			where 	x.nr_seq_conta		= a.nr_seq_conta
			and	x.ie_classificacao	= 'P'),
		substr(obter_valor_dominio(3470, ie_origem_conta),1,255),
		to_char(a.dt_atendimento,'dd/mm/yyyy'),
		substr(a.cd_medico_executor || ' - ' || obter_nome_medico(a.cd_medico_executor,'N'),1,255),
		substr(pls_obter_formacao_plano_seg(a.nr_seq_segurado),1,255),
		to_char(dt_protocolo,'dd/mm/yy') dt_atualizacao,
		substr(pls_obter_se_conta_reapre(a.nr_seq_conta),1,255),
		substr(nvl(a.cd_guia_referencia,a.cd_guia),1,30),
		substr(pls_obter_cod_prestador(a.nr_seq_prestador_exec, null) || ' - ' || pls_obter_dados_prestador(a.nr_seq_prestador_exec,'N'),1,255),
		substr(decode(a.nr_seq_congenere, null,
			decode(a.ie_tipo_segurado,
			'C',
			(select x.nr_seq_intercambio from pls_segurado x where x.nr_sequencia = a.nr_seq_segurado)|| ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES'),
			pls_obter_dados_segurado(a.nr_seq_segurado,'CC') || ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES')), 
			pls_obter_seq_codigo_coop(a.nr_seq_congenere,'')||' - '||pls_obter_nome_congenere(a.nr_seq_congenere)),1,255),
		substr(pls_obter_dados_ptu_fatura(a.nr_seq_fatura,'F'),1,255),
		a.nr_seq_segurado
	into	cd_usuario_plano_w,
		nm_beneficiario_w,
		ds_idade_benef_w,
		cd_operadora_interc_w,
		ds_tipo_guia_analise_w,
		nm_prestador_solic_w,
		ds_senha_w,
		ds_mes_ref_lote_w,
		ds_tipo_acomodacao_benef_w,
		nm_profissional_solic_w,
		nr_lote_prestador_w,
		ds_motivo_saida_w,
		ds_cid_principal_w,
		ds_origem_conta_w,
		ds_data_atend_w,
		nm_profissional_w,
		ds_formacao_preco_w,
		ds_criacao_analise_w,
		ds_tipo_apres_guia_w,
		cd_guia_principal_w,
		nm_prestador_atend_w,
		nm_operadora_interc_w,
		nr_fatura_ptu_w,
		nr_seq_segurado_w
	from	pls_conta_pos_cabecalho	a
	where  	a.nr_sequencia	= nr_seq_pos_cabecalho_w;
elsif	(ie_tipo_guia_w = '4') then /* SP/SADT */
	
	select	substr('(' || decode(pls_obter_dados_segurado(a.nr_seq_segurado,'C'),'0', cd_usuario_plano_imp, pls_obter_dados_segurado(a.nr_seq_segurado,'C')) || ') ',1,255) ,
		substr(decode(nvl(pls_obter_dados_segurado(a.nr_seq_segurado,'N'),'X'), 'X', pls_desc_benef_intercambio(nr_seq_nota_cobranca), 
		pls_obter_dados_segurado(a.nr_seq_segurado,'N')),1,255),
		substr(pls_obter_idade_segurado(a.nr_seq_segurado, sysdate,'D'),1,30),
		substr(trim(pls_obter_dados_segurado(a.nr_seq_segurado, 'CON')),1,30),    
		substr(obter_valor_dominio(1746,a.ie_tipo_guia),1,255), 
		substr(pls_obter_cod_prestador(a.nr_seq_prestador, null)||' - '||pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,255) nm_prestador,
		substr(nvl(a.cd_guia_referencia,a.cd_guia),1,30) nr_guia, 
		substr(obter_valor_dominio(2819, ie_carater_internacao),1,255) ds_carater_internacao, 
		substr(a.cd_senha ||' / '|| a.cd_senha_externa,1,30) cd_senha_externa,      
		to_char(dt_mes_competencia,'mm/yyyy'),
		substr(pls_obter_acomodac_plano_ptu(pls_obter_dados_segurado(a.nr_seq_segurado,'NRP')),1,255),
		decode(ie_tipo_conta_w, 'I', decode(a.ie_tipo_acomodacao_ptu,null,ptu_obter_ds_acomo_nota_hos(a.nr_seq_nota_cobranca),'A','Enfermaria Intercâmbio - Plano A','B','Apartamento Intercâmbio - Plano B'),
			decode(substr(pls_obter_desc_tipo_acomodacao(a.nr_seq_tipo_acomodacao),1,255),'',substr(pls_obter_tp_acomodacao_guia(nr_seq_guia_w),1,50),substr(pls_obter_desc_tipo_acomodacao(a.nr_seq_tipo_acomodacao),1,255))),
		substr(pls_obter_desc_tipo_atend(a.nr_seq_tipo_atendimento),1,255),
		substr(pls_obter_cod_prestador(a.nr_seq_prestador_exec, null)||' - '||pls_obter_dados_prestador(a.nr_seq_prestador_exec,'N'),1,255),
		substr(cd_medico_solicitante||' - '||obter_nome_medico(cd_medico_solicitante,'N'),1,255), 
		substr(a.nr_protocolo_prestador,1,30),
		substr(pls_obter_desc_motivo_saida(a.nr_seq_saida_spsadt),1,255),
		substr(pls_obter_cid_conta(a.nr_seq_conta),1,30),
		substr(pls_obter_desc_tipo_internacao(a.nr_seq_clinica),1,255),
		substr(obter_valor_dominio(3470, ie_origem_conta),1,255),
		a.dt_atendimento,
		substr(decode(a.nr_seq_congenere, null,
			decode(a.ie_tipo_segurado,
			'C',
			(select x.nr_seq_intercambio from pls_segurado x where x.nr_sequencia = a.nr_seq_segurado)|| ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES'),
			pls_obter_dados_segurado(a.nr_seq_segurado,'CC') || ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES')), 
			pls_obter_seq_codigo_coop(a.nr_seq_congenere,'')||' - '||pls_obter_nome_congenere(a.nr_seq_congenere)),1,255),
		substr(pls_obter_dados_ptu_fatura(a.nr_seq_fatura,'F'),1,30),
		substr(a.cd_medico_executor||' - '||obter_nome_medico(a.cd_medico_executor,'N'),1,255),
		substr(pls_obter_formacao_plano_seg(a.nr_seq_segurado),1,255),
		to_char(a.dt_protocolo, 'dd/mm/yyyy'),
		substr(pls_obter_se_conta_reapre(a.nr_seq_conta),1,255),
		a.nr_seq_segurado
	into	cd_usuario_plano_w,
		nm_beneficiario_w,
		ds_idade_benef_w,
		cd_operadora_interc_w,
		ds_tipo_guia_analise_w,
		nm_prestador_solic_w,
		cd_guia_principal_w,
		ds_carater_solicitacao_w,
		ds_senha_w,
		ds_mes_ref_lote_w,
		ds_tipo_acomodacao_benef_w,
		ds_tipo_acomodacao_atend_w,
		ds_tipo_atendimento_w,
		nm_prestador_atend_w,
		nm_profissional_solic_w,
		nr_lote_prestador_w,
		ds_motivo_saida_w,
		ds_cid_principal_w,
		ds_tipo_internacao_w,
		ds_origem_conta_w,
		ds_data_atend_w,
		nm_operadora_interc_w,
		nr_fatura_ptu_w,
		nm_profissional_w,
		ds_formacao_preco_w,
		ds_criacao_analise_w,
		ds_tipo_apres_guia_w,
		nr_seq_segurado_w
	from	pls_conta_pos_cabecalho		a
	where  	a.nr_sequencia	= nr_seq_pos_cabecalho_w;
	null;
elsif	(ie_tipo_guia_w = '6') then /* HI */
	select	substr('('||decode(pls_obter_dados_segurado(a.nr_seq_segurado,'C'),'0', cd_usuario_plano_imp, pls_obter_dados_segurado(a.nr_seq_segurado,'C'))||') ',1,255),
		substr(decode(nvl(pls_obter_dados_segurado(a.nr_seq_segurado,'N'),'X'), 'X', substr(pls_desc_benef_intercambio(nr_seq_nota_cobranca),1,255), pls_obter_dados_segurado(a.nr_seq_segurado,'N')),1,255),
		substr(pls_obter_idade_segurado(a.nr_seq_segurado, sysdate,'D'),1,30),
		substr(trim(pls_obter_dados_segurado(a.nr_seq_segurado, 'CON')),1,30),        
		to_char(a.dt_entrada,'dd/mm/yyyy hh24:mi:ss'),
		to_char(a.dt_alta,'dd/mm/yyyy hh24:mi:ss'),
		substr(obter_valor_dominio(1746,a.ie_tipo_guia),1,255),
		substr(pls_obter_cod_prestador(a.nr_seq_prestador, null)||' - '||pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,255) nm_prestador,       
		substr(nvl(a.cd_guia_referencia,a.cd_guia),1,30),  
		substr(obter_valor_dominio(2819, ie_carater_internacao),1,255),
		substr(a.cd_senha||' / '||a.cd_senha_externa,1,30),
		to_char(dt_mes_competencia,'mm/yyyy'),
		substr(pls_obter_acomodac_plano_ptu(pls_obter_dados_segurado(a.nr_seq_segurado,'NRP')),1,255),
		decode(ie_tipo_conta_w, 'I', decode(a.ie_tipo_acomodacao_ptu,null,ptu_obter_ds_acomo_nota_hos(a.nr_seq_nota_cobranca),'A','Enfermaria Intercâmbio - Plano A','B','Apartamento Intercâmbio - Plano B'),
			decode(substr(pls_obter_desc_tipo_acomodacao(a.nr_seq_tipo_acomodacao),1,255),'',substr(pls_obter_tp_acomodacao_guia(nr_seq_guia_w),1,50),substr(pls_obter_desc_tipo_acomodacao(a.nr_seq_tipo_acomodacao),1,255))),
		substr(pls_obter_desc_tipo_atend(a.nr_seq_tipo_atendimento),1,255),
		substr(pls_obter_cod_prestador(a.nr_seq_prestador_exec, null)||' - '||pls_obter_dados_prestador(a.nr_seq_prestador_exec,'N'),1,255),  
		substr(cd_medico_solicitante||' - '||obter_nome_medico(cd_medico_solicitante,'N'),1,255),  
		substr(a.nr_protocolo_prestador,1,30),
		substr(pls_obter_desc_mot_saida_int(a.nr_seq_saida_int),1,255),
		substr(pls_obter_cid_conta(a.nr_seq_conta),1,30),
		substr(pls_obter_desc_tipo_internacao(a.nr_seq_clinica),1,255),
		substr(obter_valor_dominio(3470, ie_origem_conta),1,255),
		to_char(a.dt_atendimento,'dd/mm/yyyy') dt_emissao,
		substr(decode(a.nr_seq_congenere, null,
			decode(a.ie_tipo_segurado,
			'C',
			(select x.nr_seq_intercambio from pls_segurado x where x.nr_sequencia = a.nr_seq_segurado)|| ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES'),
			pls_obter_dados_segurado(a.nr_seq_segurado,'CC') || ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES')), 
			pls_obter_seq_codigo_coop(a.nr_seq_congenere,'')||' - '||pls_obter_nome_congenere(a.nr_seq_congenere)),1,255),
		substr(pls_obter_dados_ptu_fatura(a.nr_seq_fatura,'F'),1,255),
		substr(pls_obter_formacao_plano_seg(a.nr_seq_segurado),1,255),
		to_char(dt_protocolo) dt_atualizacao,
		pls_obter_se_conta_reapre(a.nr_seq_conta),
		a.nr_seq_segurado
	into	cd_usuario_plano_w,
		nm_beneficiario_w,
		ds_idade_benef_w,
		cd_operadora_interc_w,
		ds_entrada_internacao_w,
		ds_saida_internacao_w,
		ds_tipo_guia_analise_w,
		nm_prestador_solic_w,
		cd_guia_principal_w,
		ds_carater_solicitacao_w,
		ds_senha_w,
		ds_mes_ref_lote_w,
		ds_tipo_acomodacao_benef_w,
		ds_tipo_acomodacao_atend_w,
		ds_tipo_atendimento_w,
		nm_prestador_atend_w,
		nm_profissional_solic_w,
		nr_lote_prestador_w,
		ds_motivo_saida_w,
		ds_cid_principal_w,
		ds_tipo_internacao_w,
		ds_origem_conta_w,
		ds_data_atend_w,
		nm_operadora_interc_w,
		nr_fatura_ptu_w,
		ds_formacao_preco_w,
		ds_criacao_analise_w,
		ds_tipo_apres_guia_w,
		nr_seq_segurado_w
	from	pls_conta_pos_cabecalho	a
	where  	a.nr_sequencia	= nr_seq_pos_cabecalho_w;
	
	if	(ds_data_atend_w	is not null) then
		begin
			ds_data_atend_w	:= to_date(ds_data_atend_w,'dd/mm/yyyy');
		exception
		when others then
			ds_data_atend_w := null;
		end;
	end if;
elsif	(ie_tipo_guia_w = '5') then /* Internação */
		 
	select	substr('('||decode(pls_obter_dados_segurado(a.nr_seq_segurado,'C'),'0', cd_usuario_plano_imp, pls_obter_dados_segurado(a.nr_seq_segurado,'C'))||') ',1,255),
		substr(decode(nvl(pls_obter_dados_segurado(a.nr_seq_segurado,'N'),'X'), 'X', substr(pls_desc_benef_intercambio(nr_seq_nota_cobranca),1,255), pls_obter_dados_segurado(a.nr_seq_segurado,'N')),1,255),
		substr(pls_obter_idade_segurado(a.nr_seq_segurado, sysdate,'D'),1,30),
		substr(trim(pls_obter_dados_segurado(a.nr_seq_segurado, 'CON')),1,30),        
		to_char(a.dt_entrada,'dd/mm/yyyy hh24:mi:ss'),
		to_char(a.dt_alta,'dd/mm/yyyy hh24:mi:ss'),
		substr(obter_valor_dominio(1746,a.ie_tipo_guia),1,255),
		substr(pls_obter_cod_prestador(a.nr_seq_prestador, null)||' - '||pls_obter_dados_prestador(a.nr_seq_prestador,'N'),1,255) nm_prestador,       
		substr(nvl(a.cd_guia_referencia,a.cd_guia),1,30),  
		substr(obter_valor_dominio(2819, ie_carater_internacao),1,255),
		substr(a.cd_senha||' / '||a.cd_senha_externa,1,30),
		to_char(dt_mes_competencia,'mm/yyyy'),
		substr(pls_obter_acomodac_plano_ptu(pls_obter_dados_segurado(a.nr_seq_segurado,'NRP')),1,255),
		decode(ie_tipo_conta_w, 'I', decode(a.ie_tipo_acomodacao_ptu,null,ptu_obter_ds_acomo_nota_hos(a.nr_seq_nota_cobranca),'A','Enfermaria Intercâmbio - Plano A','B','Apartamento Intercâmbio - Plano B'),
			decode(substr(pls_obter_desc_tipo_acomodacao(a.nr_seq_tipo_acomodacao),1,255),'',substr(pls_obter_tp_acomodacao_guia(nr_seq_guia_w),1,50),substr(pls_obter_desc_tipo_acomodacao(a.nr_seq_tipo_acomodacao),1,255))),
		substr(pls_obter_desc_tipo_atend(a.nr_seq_tipo_atendimento),1,255),		
		substr(pls_obter_cod_prestador(a.nr_seq_prestador_exec, null)||' - '||pls_obter_dados_prestador(a.nr_seq_prestador_exec,'N'),1,255),  
		substr(cd_medico_solicitante||' - '||obter_nome_medico(cd_medico_solicitante,'N'),1,255),  
		substr(a.nr_protocolo_prestador,1,30), 
		substr(pls_obter_desc_mot_saida_int(a.nr_seq_saida_int),1,255),
		substr(pls_obter_cid_conta(a.nr_seq_conta),1,30),
		substr(pls_obter_desc_tipo_internacao(a.nr_seq_clinica),1,255),
		substr(obter_valor_dominio(3470, ie_origem_conta),1,255),
		to_char(a.dt_atendimento,'dd/mm/yyyy') dt_emissao,
		substr(decode(a.nr_seq_congenere, null,
			decode(a.ie_tipo_segurado,
			'C',
			(select x.nr_seq_intercambio from pls_segurado x where x.nr_sequencia = a.nr_seq_segurado)|| ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES'),
			pls_obter_dados_segurado(a.nr_seq_segurado,'CC') || ' - ' || pls_obter_dados_segurado(a.nr_seq_segurado,'ES')), 
			pls_obter_seq_codigo_coop(a.nr_seq_congenere,'')||' - '||pls_obter_nome_congenere(a.nr_seq_congenere)),1,255),
		substr(pls_obter_dados_ptu_fatura(a.nr_seq_fatura,'F'),1,255),
		substr(pls_obter_formacao_plano_seg(a.nr_seq_segurado),1,255),
		to_char(dt_protocolo) dt_atualizacao,
		pls_obter_se_conta_reapre(a.nr_seq_conta), 
		a.nr_seq_segurado
	into	cd_usuario_plano_w,
		nm_beneficiario_w,
		ds_idade_benef_w,
		cd_operadora_interc_w,
		ds_entrada_internacao_w,
		ds_saida_internacao_w,
		ds_tipo_guia_analise_w,
		nm_prestador_solic_w,
		cd_guia_principal_w,
		ds_carater_solicitacao_w,
		ds_senha_w,
		ds_mes_ref_lote_w,
		ds_tipo_acomodacao_benef_w,
		ds_tipo_acomodacao_atend_w,
		ds_tipo_atendimento_w,
		nm_prestador_atend_w,
		nm_profissional_solic_w,
		nr_lote_prestador_w,
		ds_motivo_saida_w,
		ds_cid_principal_w,
		ds_tipo_internacao_w,
		ds_origem_conta_w,
		ds_data_atend_w,
		nm_operadora_interc_w,
		nr_fatura_ptu_w,
		ds_formacao_preco_w,
		ds_criacao_analise_w,
		ds_tipo_apres_guia_w,
		nr_seq_segurado_w
	from	pls_conta_pos_cabecalho	a
	where  	a.nr_sequencia	= nr_seq_pos_cabecalho_w;

end if;

nr_seq_grupo_atual_w	:= nr_seq_grupo_p;

/*aaschlote 21/11/2012 OS - 510018*/
cd_usuario_plano_aux_w	:= replace(replace(cd_usuario_plano_w,')',''),'(','');
cd_usuario_plano_w	:= '(' || substr(pls_convert_masc_cart_usuario(cd_usuario_plano_aux_w,cd_estabelecimento_w),1,255) || ') ';

insert into w_pls_analise_cabecalho
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_analise,
	nr_lote_prestador,
	nm_prestador_solic,
	nm_prestador_atend,
	ds_carater_solicitacao,
	ds_mes_ref_lote,
	ds_tipo_internacao,
	ds_cid_principal,
	ds_formacao_preco,
	ds_tipo_apres_guia,
	ds_origem_conta,
	ds_entrada_internacao,
	ds_saida_internacao,
	ds_tipo_acomodacao_benef,
	ie_tipo_cabecalho,
	ds_tipo_acomodacao_atend,
	ds_tipo_saida,
	ds_senha,
	ds_criacao_analise,
	nm_profissional_solic,
	nm_profissional,
	ds_data_atend,
	ds_motivo_saida,
	nm_beneficiario,
	cd_usuario_plano,
	ds_idade_benef,
	ds_tipo_atendimento,
	cd_operadora_interc,
	nm_operadora_interc,
	nr_fatura_ptu,
	ie_origem_analise,
	ie_tipo_guia,
	ds_origem_analise,
	ds_tipo_guia_analise,
	cd_guia_principal,
	nr_seq_grupo_aud_atual,
	nr_seq_segurado,
	nr_id_transacao)
values	(w_pls_analise_cabecalho_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_analise_p,
	nr_lote_prestador_w,
	nm_prestador_solic_w,
	nm_prestador_atend_w,
	ds_carater_solicitacao_w,
	ds_mes_ref_lote_w,
	ds_tipo_internacao_w,
	ds_cid_principal_w,
	ds_formacao_preco_w,
	ds_tipo_apres_guia_w,
	ds_origem_conta_w,
	ds_entrada_internacao_w,
	ds_saida_internacao_w,
	ds_tipo_acomodacao_benef_w,
	decode(nr_fatura_ptu_w,null,decode(ie_tipo_guia_w,3,'C',4,'S',5,'I',6,'HI'),'IN'),
	ds_tipo_acomodacao_atend_w,
	ds_tipo_saida_w,
	ds_senha_w,
	ds_criacao_analise_w,
	nm_profissional_solic_w,
	nm_profissional_w,
	ds_data_atend_w,
	ds_motivo_saida_w,
	nm_beneficiario_w,
	cd_usuario_plano_w,
	ds_idade_benef_w,
	ds_tipo_atendimento_w,
	cd_operadora_interc_w,
	nm_operadora_interc_w,
	nr_fatura_ptu_w,
	ie_origem_analise_w,
	ie_tipo_guia_w,
	ds_origem_analise_w,
	ds_tipo_guia_analise_w,
	cd_guia_principal_w,
	nr_seq_grupo_atual_w,
	nr_seq_segurado_w,
	nr_id_transacao_p);

end pls_gerar_w_analise_pos_cab;
/