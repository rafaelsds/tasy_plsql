create or replace
procedure cpoe_adjust_rp_sequence(nr_seq_order_unit_p number) is

counter_w number := 1;

cursor 		c_sequence is 
select  	nr_sequencia,
            nr_sequencia_rp
from    	cpoe_rp
where   	nr_seq_cpoe_order_unit = nr_seq_order_unit_p
order by 	nr_sequencia;

begin

for r_sequence in c_sequence loop
    if ((r_sequence.nr_sequencia_rp - counter_w) > 0) then
        update cpoe_rp
        set nr_sequencia_rp = counter_w
        where nr_sequencia = r_sequence.nr_sequencia;
	end if;
	counter_w := counter_w + 1;
end loop;

end cpoe_adjust_rp_sequence;
/
