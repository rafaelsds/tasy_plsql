create or replace
procedure man_gerar_ativ_prev_philips(
			nr_seq_ordem_serv_p	number,
			nm_usuario_previsto_p	varchar2,
			ds_atividade_prevista_p	varchar2,
			dt_prevista_p		date,
			ds_setor_usuario_p		varchar2,
			ds_estagio_wheb_p		varchar2,
			nm_analista_resp_p		varchar2,
			nm_gerente_resp_p		varchar2,
			nr_ramal_gerente_resp_p	varchar2,
			ds_explic_estagio_philips_p	varchar2,
			ie_remover_ativ_ant_p	varchar2,
			nm_usuario_p		varchar2) is

begin

if	(nvl(nr_seq_ordem_serv_p,0) > 0) then
	begin
	if	(nvl(ie_remover_ativ_ant_p,'N') = 'S') then
		delete from man_os_ativ_prev_wheb where nr_seq_ordem_serv = nr_seq_ordem_serv_p;
	end if;

	if	(nvl(nm_usuario_previsto_p,'0') <> '0') and
		(nvl(ds_atividade_prevista_p,'0') <> '0') and
		(nvl(ds_setor_usuario_p,'0') <> '0') and
		(nvl(nm_usuario_p,'0') <> '0') then
		begin
		insert into man_os_ativ_prev_wheb (
				nr_sequencia,
				nr_seq_ordem_serv,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_usuario_previsto,
				ds_atividade_prevista,
				dt_prevista,
				ds_setor_usuario,
				ds_estagio_wheb,
				nm_analista_resp,
				nm_gerente_resp,
				nr_ramal_gerente_resp,
				ds_explic_estagio_philips)
			values (	man_os_ativ_prev_wheb_seq.nextval,
				nr_seq_ordem_serv_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nm_usuario_previsto_p,
				ds_atividade_prevista_p,
				dt_prevista_p,
				ds_setor_usuario_p,
				ds_estagio_wheb_p,
				nm_analista_resp_p,
				nm_gerente_resp_p,
				nr_ramal_gerente_resp_p,
				ds_explic_estagio_philips_p);
		end;
	end if;

	commit;
	end;
end if;

end man_gerar_ativ_prev_philips;
/