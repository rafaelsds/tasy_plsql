CREATE OR REPLACE PROCEDURE update_status_waiting_list(nr_seq_lista_p     agenda_lista_espera.nr_sequencia%TYPE,
                                                        ie_status_espera_p agenda_lista_espera.ie_status_espera%TYPE,
                                                        nm_usuario_p       VARCHAR2) IS


BEGIN
	IF (nr_seq_lista_p IS NOT NULL) THEN
		UPDATE agenda_lista_espera
		SET    ie_status_espera = ie_status_espera_p,
					 dt_atualizacao   = SYSDATE,
					 nm_usuario       = nm_usuario_p
		WHERE  nr_sequencia = nr_seq_lista_p;
	END IF;

	COMMIT;
END update_status_waiting_list;
/
