create or replace
procedure cpoe_gerar_anatomia_patologica(	nr_atendimento_p		number,
											nr_prescricao_p			number,
											nr_sequencia_p			number,
											cd_estabelecimento_p	number,
											cd_perfil_p				number,
											nm_usuario_p			varchar2,
											cd_setor_atendimento_p	number,
											ie_origem_inf_p			varchar2,
											dt_inicio_prescr_p		date,
											dt_inicio_ret_p		out	date,
											cd_pessoa_fisica_p		varchar2) is

dt_inicio_base_w				date;
dt_inicio_w						date;
ie_guia_w						varchar2(3);
ds_horarios_w					varchar2(4000);

cd_pessoa_fisica_w				pessoa_fisica.cd_pessoa_fisica%type;

ie_se_necessario_w				cpoe_procedimento.ie_se_necessario%type;
ie_acm_w						cpoe_procedimento.ie_acm%type;
ds_justificativa_w				cpoe_procedimento.ds_justificativa%type;

nr_seq_exame_w					prescr_procedimento.nr_seq_exame%type;
dt_entrega_w					prescr_procedimento.dt_resultado%type;
dt_prevista_retorno_w			prescr_procedimento.dt_prev_execucao%type;

dt_atualizacao_w				cpoe_anatomia_patologica.dt_atualizacao%type;
nm_usuario_w					cpoe_anatomia_patologica.nm_usuario%type;
dt_atualizacao_nrec_w			cpoe_anatomia_patologica.dt_atualizacao_nrec%type;
nm_usuario_nrec_w				cpoe_anatomia_patologica.nm_usuario_nrec%type;
nr_sequencia_cpoe_w				cpoe_anatomia_patologica.nr_sequencia%type;
cd_cgc_laboratorio_w			cpoe_anatomia_patologica.cd_cgc_laboratorio%type;
cd_material_exame_w				cpoe_anatomia_patologica.cd_material_exame%type;
cd_pessoa_coleta_w				cpoe_anatomia_patologica.cd_pessoa_coleta%type;
cd_setor_exec_w					cpoe_anatomia_patologica.cd_setor_exec%type;
ds_dado_clinico_w				cpoe_anatomia_patologica.ds_dado_clinico%type;
ds_diag_provavel_ap_w			cpoe_anatomia_patologica.ds_diag_provavel_ap%type;      
ds_exame_anterior_ap_w			cpoe_anatomia_patologica.ds_exame_anterior_ap%type; 
ds_localizacao_lesao_w			cpoe_anatomia_patologica.ds_localizacao_lesao%type;
ds_observacao_w					cpoe_anatomia_patologica.ds_observacao%type;
ds_qualidade_peca_ap_w			cpoe_anatomia_patologica.ds_qualidade_peca_ap%type;
ds_tempo_doenca_w				cpoe_anatomia_patologica.ds_tempo_doenca%type;
dt_coleta_w						cpoe_anatomia_patologica.dt_coleta%type;
dt_fim_w						cpoe_anatomia_patologica.dt_fim%type;
dt_prev_execucao_w				cpoe_anatomia_patologica.dt_prev_execucao%type;
dt_prim_horario_w				cpoe_anatomia_patologica.dt_prim_horario%type;
hr_prim_horario_w				cpoe_anatomia_patologica.hr_prim_horario%type;
ie_administracao_w				cpoe_anatomia_patologica.ie_administracao%type;
ie_amostra_w					cpoe_anatomia_patologica.ie_amostra%type; 
ie_autorizacao_w				cpoe_anatomia_patologica.ie_autorizacao%type;
ie_coleta_externa_w				cpoe_anatomia_patologica.ie_coleta_externa%type;
ie_duracao_w					cpoe_anatomia_patologica.ie_duracao%type;
ie_executar_leito_w				cpoe_anatomia_patologica.ie_executar_leito%type; 
ie_forma_exame_w				cpoe_anatomia_patologica.ie_forma_exame%type;
ie_urgencia_w					cpoe_anatomia_patologica.ie_urgencia%type;
nr_seq_amostra_princ_w			cpoe_anatomia_patologica.nr_seq_amostra_princ%type;
nr_seq_proc_int_cirur_w			cpoe_anatomia_patologica.nr_seq_proc_int_cirur%type;
nr_seq_proc_interno_w			cpoe_anatomia_patologica.nr_seq_proc_interno%type;
qt_frasco_env_w					cpoe_anatomia_patologica.qt_frasco_env%type;
qt_peca_ap_w					cpoe_anatomia_patologica.qt_peca_ap%type;
ie_suspenso_w					cpoe_anatomia_patologica.ie_suspenso%type;

cd_procedimento_w				prescr_procedimento.cd_procedimento%type;     
ie_origem_proced_w				prescr_procedimento.ie_origem_proced%type;
nr_doc_convenio_w				prescr_procedimento.nr_doc_convenio%type;
qt_procedimento_w				prescr_procedimento.qt_procedimento%type;
nr_sequencia_w					prescr_procedimento.nr_sequencia%type;
nr_seq_peca_w					prescr_proc_peca.nr_sequencia%type;
nr_seq_citopalogia_w			prescr_citopatologico.nr_sequencia%type;
nr_seq_histopatol_w				prescr_histopatologico.nr_sequencia%type;	


Cursor C01 is
	select	nr_sequencia,		
			nr_seq_proc_interno,
			nr_seq_proc_int_cirur,
			cd_pessoa_coleta,
			qt_peca_ap,
			nr_seq_amostra_princ,
			ds_qualidade_peca_ap,
			ds_dado_clinico,
			ie_forma_exame,
			ds_diag_provavel_ap,
			ds_exame_anterior_ap,
			ds_localizacao_lesao,
			ds_tempo_doenca,
			ds_observacao,
			cd_setor_exec,
			qt_frasco_env,
			cd_material_exame,
			cd_cgc_laboratorio,
			qt_procedimento,
			nvl(ie_administracao,'P'),
			nvl(ie_amostra,'N'),
			nvl(ie_suspenso,'N'),
			nvl(ie_urgencia,'N'),
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nm_usuario,
			dt_atualizacao,
			dt_inicio,
			dt_prev_execucao
	from	cpoe_anatomia_patologica 
	where	nr_sequencia = nr_sequencia_p
	and		((decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim)) >= sysdate) or 
			 (decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao, dt_fim)) is null) or 
			 (NVL(ie_retrogrado,'N') = 'S' AND dt_inicio >= dt_inicio_prescr_p));
			 
	cursor c02 is
	select	nr_sequencia        ,
			cd_topografia       ,
			cd_morfologia       ,
			dt_atualizacao      ,
			nm_usuario          ,
			dt_atualizacao_nrec ,
			nm_usuario_nrec,
			nr_seq_laudo,
			cd_doenca_cid,
			nr_seq_peca,
			nr_seq_pato_exame,
			ie_status,
			nr_seq_tipo,
			nr_seq_apresent,
			nr_controle,
			ds_amostra_principal,
			nr_seq_exame_complementar,
			nr_seq_caso_congelacao,     
			nr_seq_amostra_princ,    
			nr_sequencia_princ,   
			nr_revisao,  
			nr_fragmentos, 
			ds_designacao,
			ds_observacao,
			ie_tipo_designacao,
			ie_exame_compl,
			nr_seq_designacao,
			ie_situacao,
			ie_peca_principal,
			dt_inativacao,
			nr_seq_cpoe_proc,
			ie_lado,
      NR_SEQ_MORF_DESC_ADIC
	from	cpoe_prescr_proc_peca
	where	nr_seq_cpoe_proc	= nr_sequencia_cpoe_w;

	c02_w	c02%rowtype;
	
	cursor c03 is
	select	nr_sequencia,
			ie_exame_preventivo,
			dt_ultimo_exame,
			ie_usa_diu,
			ie_gravida,
			ie_pilula,
			ie_hormonio,
			ie_radioterapia,
			ie_ultima_menstruacao,
			dt_ultima_menstruacao,
			ie_sangramento,
			ie_sangr_apos_menop,
			ie_inspecao_colo,
			ie_dst
	from	cpoe_prescr_citopatologico
	where	nr_seq_proc_anatomia = nr_sequencia_cpoe_w;

	c03_w	c03%rowtype;
	
	cursor c04 is
	select	nr_sequencia,
			ie_result_hpv,
			ie_result_car_esc_inv,
			ie_result_ascus,
			ie_result_aden_in_sito,
			ie_result_nici,
			ie_result_aden_invasivo,
			ie_result_nicii,
			ie_result_nao_fornec,
			ie_result_niciii,
			ie_result_outros,
			ds_result_exame_outro,
			ie_colposcopia,
			ie_colposcopia_anormal,
			ie_procedimento,
			ds_inf_adic,
			ie_caf
	from	cpoe_prescr_histopatol
	where	nr_seq_proc_anatomia = nr_sequencia_cpoe_w;

	c04_w	c04%rowtype;	
	
begin

dt_inicio_base_w := trunc(dt_inicio_prescr_p,'mi');

if	(nr_atendimento_p is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
end if;

open C01;
loop
fetch C01 into	
		nr_sequencia_cpoe_w,
		nr_seq_proc_interno_w,
		nr_seq_proc_int_cirur_w,
		cd_pessoa_coleta_w,
		qt_peca_ap_w, 
		nr_seq_amostra_princ_w, 
		ds_qualidade_peca_ap_w, 
		ds_dado_clinico_w,
		ie_forma_exame_w,
		ds_diag_provavel_ap_w, 
		ds_exame_anterior_ap_w, 
		ds_localizacao_lesao_w, 
		ds_tempo_doenca_w,
		ds_observacao_w,
		cd_setor_exec_w, 
		qt_frasco_env_w, 
		cd_material_exame_w,
		cd_cgc_laboratorio_w,
		qt_procedimento_w,
		ie_administracao_w,
		ie_amostra_w,
		ie_suspenso_w,
		ie_urgencia_w,
		nm_usuario_nrec_w,
		dt_atualizacao_nrec_w,
		nm_usuario_w,
		dt_atualizacao_w,
		dt_inicio_w,
		dt_prev_execucao_w;		
exit when C01%notfound;
	begin
	
	dt_coleta_w := dt_inicio_w;
	
	ie_se_necessario_w	:= 'N';
	ie_acm_w			:= 'N';

	if 	(ie_administracao_w = 'N') then
		ie_se_necessario_w	:= 'S';
		ie_acm_w			:= 'N';
		hr_prim_horario_w	:= null;
		ds_horarios_w		:= 'SN';
		ie_urgencia_w		:= 'N';
	elsif	(ie_administracao_w = 'C') then
		ie_se_necessario_w	:= 'N';
		ie_acm_w			:= 'S';
		hr_prim_horario_w	:= null;
		ds_horarios_w		:= 'ACM';
		ie_urgencia_w		:= 'N';
	end if;

	if(ie_urgencia_w is null) then
        ie_urgencia_w  := 'N';
    end if;
	
	Obter_Proc_Tab_Interno(nr_seq_proc_interno_w, nr_prescricao_p, nr_atendimento_p, 0, cd_procedimento_w, ie_origem_proced_w);
	
	select 	max(nr_seq_exame_lab)
	into	nr_seq_exame_w
	from  	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_w;

	begin
		if (nr_seq_exame_w is null) then
			Obter_Entrega_Laudo(	nr_prescricao_p, dt_coleta_w, nr_seq_proc_interno_w, cd_procedimento_w,
									ie_origem_proced_w, cd_setor_atendimento_p, nm_usuario_p, dt_entrega_w );
		else
			Atualiza_dt_realizacao_exame(	nr_seq_exame_w, nr_prescricao_p, dt_coleta_w, dt_prevista_retorno_w );

			if (to_char(dt_prevista_retorno_w,'hh24:mi:ss') <> '00:00:00')  then
			   dt_coleta_w	:= dt_prevista_retorno_w;
			elsif	(ie_acm_w = 'N') and
					(ie_se_necessario_w = 'N')	then
			   dt_coleta_w	:= dt_prevista_retorno_w;
			end if;
		end if;

	exception when others then
		gravar_log_cpoe(substr('CPOE_GERAR_ANATOMIA_PATOLOGICA EXCEPTION ATUALIZA_DT_REALIZACAO_EXAME:'||substr(to_char(sqlerrm),1,2000)
			||' nr_sequencia_p'	|| nr_sequencia_p
			||' nr_prescricao_p'	|| nr_prescricao_p
			||' nr_seq_exame_w:' ||nr_seq_exame_w
			||' dt_prevista_retorno_w:'|| to_char(dt_prevista_retorno_w,'dd/mm/yyyy hh24:mi:ss'),1,2000),
			nr_atendimento_p);
	end;
	
	-- Atualizar data de inicio com a data atual, para itens que a data de inicio ja tenha sido ultrapassada.
		if	(dt_coleta_w < dt_inicio_base_w) then
			-- Assumir data da prescricao para o item
			dt_coleta_w		:= to_date(to_char(dt_inicio_base_w,'dd/mm/yyyy ') || to_char(dt_coleta_w,'hh24:mi'), 'dd/mm/yyyy hh24:mi');
			-- Adicionar um dia, caso o inicio do item, nao caia na data de hoje
			if	(dt_coleta_w < dt_inicio_base_w) then
				dt_coleta_w	:= dt_coleta_w+1;
			end if;
		end if;
	
	-- Retornar a maior data de inicio dos itens.
	if	((dt_inicio_ret_p is null) or
		 (dt_coleta_w > dt_inicio_ret_p)) then
		dt_inicio_ret_p	:= dt_coleta_w;
	end if;
	
	begin
		tiss_obter_guia(	'4', null, nr_doc_convenio_w, 'N',
							null, null, null, null,
							null, ie_guia_w, null, nr_prescricao_p,
							null, cd_setor_atendimento_p, null, nr_prescricao_p,
							null, null, null, null,
							nr_seq_proc_interno_w, null, cd_procedimento_w, ie_origem_proced_w,
							null, null );
	exception when others then
		gravar_log_cpoe(substr('CPOE_GERAR_ANATOMIA_PATOLOGICA EXCEPTION TISS_OBTER_GUIA:'||substr(to_char(sqlerrm),1,2000)
			||' nr_sequencia_p:'	     || nr_sequencia_p
			||' nr_prescricao_p :'	 || nr_prescricao_p 
			||' nr_seq_proc_interno_w:'|| nr_seq_proc_interno_w
			||' cd_procedimento_w:'	 || cd_procedimento_w
			||' ie_origem_proced_w'	 || ie_origem_proced_w	
			||' nr_doc_convenio_w:' 	 || nr_doc_convenio_w
			||' cd_setor_atendimento_p:'|| cd_setor_atendimento_p,1,2000),
			nr_atendimento_p);
	end;

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from 	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p;

	ds_justificativa_w := cpoe_obter_justificativa_item(nr_sequencia_p, nr_seq_proc_interno_w, null);
	
	begin		
		insert into prescr_procedimento(
				nr_prescricao,
				nr_seq_proc_cpoe,
				ie_origem_proced,
				nr_sequencia,
				cd_procedimento,
				qt_procedimento,
				ie_origem_inf,
				ds_horarios,
				ds_observacao,
				ds_justificativa,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_urgencia,				
				dt_coleta,
				cd_material_exame,				
				ie_se_necessario,
				ie_acm,
				nr_seq_proc_interno,				
				nr_seq_exame,
				cd_setor_atendimento,				
				cd_motivo_baixa,	
				ie_orientar_paciente,
				ie_util_hemocomponente,
				ie_unid_med_hemo,
				ie_autorizacao,				
				ie_amostra,
				ie_coleta_externa,
				ie_executar_leito,				
				ie_suspenso,
				ie_externo,
				ie_avisar_result,
				ie_exame_bloqueado,
				ie_aliquotado,
				ie_filtrado,
				ie_lavado,
				ie_irradiado,
				ie_proced_bloqueado,
				ie_exige_liberacao,
				ie_imagem_pacs,
				ie_baixa_dep_lab,
				ie_descricao_cirurgica,
				ie_modificado,
				ie_pendente_amostra,
				ie_email_aprovacao,
				ie_prev_execucao_alt,
				ie_horario_susp,
				ie_anestesia,
				ie_result_lab_gerado,
				ie_em_protocolo_vanco,				
				ie_tipo_proced,
				ie_emite_mapa,
				ie_cobra_paciente,
				ie_mostrar_web,
				ie_plano,
				ie_alterar_horario,
				ie_aprovacao_execucao,
				ie_lote_ent_exibe_result,				
				nr_ocorrencia,
				dt_inicio,
				dt_fim,
				ie_administrar,
				dt_proxima_dose,
				cd_pessoa_coleta,
				QT_PECA_AP,
				nr_seq_amostra_princ,
				ds_qualidade_peca_ap,
				ds_dado_clinico,
				ie_forma_exame,
				ds_diag_provavel_ap,
				ds_exame_anterior_ap,
				ds_localizacao_lesao,
				ds_tempo_doenca,
				qt_frasco_env,
				cd_cgc_laboratorio,
				nr_seq_proc_int_cirur,
				dt_prev_execucao)
			values(
				nr_prescricao_p,
				nr_sequencia_cpoe_w,
				ie_origem_proced_w,
				nr_sequencia_w,
				cd_procedimento_w,
				nvl(qt_procedimento_w,1),
				ie_origem_inf_p,
				ds_horarios_w,
				ds_observacao_w,
				ds_justificativa_w,
				dt_atualizacao_w,
				nm_usuario_w,
				dt_atualizacao_nrec_w,
				nm_usuario_nrec_w,
				ie_urgencia_w,
				dt_coleta_w,
				cd_material_exame_w,				
				ie_se_necessario_w,
				ie_acm_w,
				nr_seq_proc_interno_w,							
				nr_seq_exame_w,
				cd_setor_exec_w,				
				0,
				'N',
				'C',
				'gpm',
				'L',
				ie_amostra_w,
				'N',
				'N',
				ie_suspenso_w,
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				nvl(Obter_tipo_proc_interno(nr_seq_proc_interno_w),'AP'),
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				1,
				dt_inicio_base_w,
				dt_inicio_base_w,
				'S',
				null,
				cd_pessoa_coleta_w,
				qt_peca_ap_w,
				nr_seq_amostra_princ_w,
				ds_qualidade_peca_ap_w,
				ds_dado_clinico_w,
				ie_forma_exame_w,
				ds_diag_provavel_ap_w,
				ds_exame_anterior_ap_w,
				ds_localizacao_lesao_w,
				ds_tempo_doenca_w,
				qt_frasco_env_w,
				cd_cgc_laboratorio_w,
				nr_seq_proc_int_cirur_w,
				dt_prev_execucao_w);
				
	exception when others then
		gravar_log_cpoe(substr('CPOE_GERAR_ANATOMIA_PATOLOGICA EXCEPTION INSERT PRESCR_PROCEDIMENTO:'||substr(to_char(sqlerrm),1,2000)
			||' nr_sequencia_p:' || nr_sequencia_p
			||' nr_prescricao_p :' || nr_prescricao_p 
			||' nr_sequencia_cpoe_w:' || nr_sequencia_cpoe_w
			||' cd_procedimento_w:'	|| cd_procedimento_w,1,2000),
			nr_atendimento_p);
	end;
	
	begin
		reordenar_procedimento(nr_prescricao_p);	
	exception when others then	
		gravar_log_cpoe(substr('CPOE_GERAR_ANATOMIA_PATOLOGICA EXCEPTION REORDENAR_PROCEDIMENTO:'||substr(to_char(sqlerrm),1,2000)
			||' nr_sequencia_p:'|| nr_sequencia_p
			||' nr_prescricao_p :' || nr_prescricao_p,1,2000),
			nr_atendimento_p);
	end;	
	
	commit;
	
	open C02;
	loop
	fetch C02 into	
		c02_w;
	exit when C02%notfound;
	
		select	prescr_proc_peca_seq.nextval
		into	nr_seq_peca_w
		from	dual;
	
		insert into prescr_proc_peca (	
					nr_sequencia                   ,
					nr_prescricao                  ,
					nr_seq_prescr                  ,
					cd_topografia                  ,
					cd_morfologia                  ,
					dt_atualizacao                 ,
					nm_usuario                     ,
					nr_seq_laudo                   ,
					cd_doenca_cid                  ,
					dt_atualizacao_nrec            ,
					nm_usuario_nrec                ,
					nr_seq_peca                    ,
					ie_status                      ,
					nr_seq_tipo                    ,
					nr_controle                    ,
					nr_revisao                     ,
					nr_fragmentos                  ,
					ds_designacao                  ,
					ds_observacao                  ,
					nr_seq_apresent                ,
					ds_amostra_principal           ,
					nr_seq_amostra_princ           ,
					ie_tipo_designacao             ,
					nr_seq_pato_exame              ,
					nr_seq_exame_complementar      ,
					ie_exame_compl                 ,
					nr_seq_designacao              ,
					nr_seq_caso_congelacao         ,
					nr_sequencia_princ             ,
					nr_seq_proc_paciente           ,
					ie_peca_principal              ,
					ie_situacao                    ,
					nm_usuario_inativacao          ,
					dt_inativacao                  ,
					ie_lado,
          NR_SEQ_MORF_DESC_ADIC)
		values(		nr_seq_peca_w,
					nr_prescricao_p,
					nr_sequencia_w,
					c02_w.cd_topografia                  ,
					c02_w.cd_morfologia                  ,
					sysdate,
					nm_usuario_p,
					c02_w.nr_seq_laudo                   ,
					c02_w.cd_doenca_cid                  ,
					sysdate,
					nm_usuario_p,
					c02_w.nr_seq_peca                    ,
					c02_w.ie_status                      ,
					c02_w.nr_seq_tipo                    ,
					c02_w.nr_controle                    ,
					c02_w.nr_revisao                     ,
					c02_w.nr_fragmentos                  ,
					c02_w.ds_designacao                  ,
					c02_w.ds_observacao                  ,
					c02_w.nr_seq_apresent                ,
					c02_w.ds_amostra_principal           ,
					c02_w.nr_seq_amostra_princ           ,
					c02_w.ie_tipo_designacao             ,
					c02_w.nr_seq_pato_exame              ,
					c02_w.nr_seq_exame_complementar      ,
					c02_w.ie_exame_compl                 ,
					c02_w.nr_seq_designacao              ,
					c02_w.nr_seq_caso_congelacao         ,
					c02_w.nr_sequencia_princ             ,
					null           ,
					c02_w.ie_peca_principal              ,
					c02_w.ie_situacao                    ,
					null          ,
					null                  ,
					c02_w.ie_lado,
          c02_w.NR_SEQ_MORF_DESC_ADIC);
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		c03_w;
	exit when C03%notfound;
	
		select	prescr_citopatologico_seq.nextval
		into	nr_seq_citopalogia_w
		from	dual;
	
		insert into prescr_citopatologico (	
					nr_sequencia,
					dt_atualizacao,
					dt_atualizacao_nrec,
					dt_ultima_menstruacao,
					dt_ultimo_exame,
					ie_dst,
					ie_exame_preventivo,
					ie_gravida,
					ie_hormonio,
					ie_inspecao_colo,
					ie_pilula,
					ie_radioterapia,
					ie_sangramento,
					ie_sangr_apos_menop,
					ie_ultima_menstruacao,
					ie_usa_diu,
					nm_usuario,
					nm_usuario_nrec,
					nr_prescricao)
		values(		nr_seq_citopalogia_w,
					sysdate,
					sysdate,
					c03_w.dt_ultima_menstruacao,
					c03_w.dt_ultimo_exame,
					c03_w.ie_dst,
					c03_w.ie_exame_preventivo,
					c03_w.ie_gravida,
					c03_w.ie_hormonio,
					c03_w.ie_inspecao_colo,
					c03_w.ie_pilula,
					c03_w.ie_radioterapia,
					c03_w.ie_sangramento,
					c03_w.ie_sangr_apos_menop,
					c03_w.ie_ultima_menstruacao,
					c03_w.ie_usa_diu,
					nm_usuario_p,
					nm_usuario_p,
					nr_prescricao_p);
		
	end loop;
	close C03;
	
	open C04;
	loop
	fetch C04 into	
		c04_w;
	exit when C04%notfound;
	
		select	prescr_histopatologico_seq.nextval
		into	nr_seq_histopatol_w
		from	dual;
	
		insert into prescr_histopatologico (
					nr_sequencia,
					ds_inf_adic ,
					ds_result_exame_outro,
					dt_atualizacao,
					dt_atualizacao_nrec,
					ie_caf,
					ie_colposcopia,
					ie_colposcopia_anormal,
					ie_procedimento,
					ie_result_aden_in_sito,
					ie_result_aden_invasivo,
					ie_result_ascus,
					ie_result_car_esc_inv,
					ie_result_hpv,
					ie_result_nao_fornec,
					ie_result_nici,
					ie_result_nicii,
					ie_result_niciii,
					ie_result_outros,
					nm_usuario,
					nm_usuario_nrec,
					nr_prescricao)
		values(		nr_seq_histopatol_w,
					c04_w.ds_inf_adic ,
					c04_w.ds_result_exame_outro,
					sysdate,
					sysdate,
					c04_w.ie_caf,
					c04_w.ie_colposcopia,
					c04_w.ie_colposcopia_anormal,
					c04_w.ie_procedimento,
					c04_w.ie_result_aden_in_sito,
					c04_w.ie_result_aden_invasivo,
					c04_w.ie_result_ascus,
					c04_w.ie_result_car_esc_inv,
					c04_w.ie_result_hpv,
					c04_w.ie_result_nao_fornec,
					c04_w.ie_result_nici,
					c04_w.ie_result_nicii,
					c04_w.ie_result_niciii,
					c04_w.ie_result_outros,
					nm_usuario_p,
					nm_usuario_p,
					nr_prescricao_p);
		
	end loop;
	close C04;	
	
	end;
end loop;
close C01;

commit;

exception when others then
	gravar_log_cpoe(substr('CPOE_GERAR_ANATOMIA_PATOLOGICA EXCEPTION:'||substr(to_char(sqlerrm),1,2000)
		||' nr_atendimento_p:'||nr_atendimento_p
		||' nr_prescricao_p:'||nr_prescricao_p
		||' nr_sequencia_p:'||nr_sequencia_p
		||' cd_estabelecimento_p:'||cd_estabelecimento_p
		||' cd_perfil_p:'||cd_perfil_p
		||' nm_usuario_p:'||nm_usuario_p
		||' cd_setor_atendimento_p:'||cd_setor_atendimento_p
		||' ie_origem_inf_p:'||ie_origem_inf_p
		||' dt_inicio_prescr_p:'|| to_char(dt_inicio_prescr_p,'dd/mm/yyyy hh24:mi:ss')
		||' dt_inicio_ret_p:'|| to_char(dt_inicio_ret_p,'dd/mm/yyyy hh24:mi:ss')
		||' cd_pessoa_fisica_p:'||cd_pessoa_fisica_p,1,2000),
		nr_atendimento_p);
		
	commit;
end cpoe_gerar_anatomia_patologica;
/