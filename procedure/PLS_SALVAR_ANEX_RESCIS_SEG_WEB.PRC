create or replace
procedure pls_salvar_anex_rescis_seg_web
			(	nr_seq_segurado_p		varchar2,	
				nr_seq_solicitacao_p		varchar2,	
				nr_seq_tipo_documento_p		number,
				ds_arquivo_p			varchar2,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				ie_apresenta_portal_p		varchar2) is 				
		
				
begin

if	(nr_seq_segurado_p is not null and nr_seq_tipo_documento_p is not null) then
	/* Caso seja gerado pela nova rescis�o, nr_seq_rescis�o, sen�o � vinculado ao segurado  */
	if(nr_seq_solicitacao_p is not null) then
		insert into pls_solicitacao_resc_anexo (nr_sequencia, nr_seq_solicitacao, ds_arquivo,
							dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
							nm_usuario_nrec, nr_seq_segurado, dt_anexo,
							ie_apresenta_portal)
						values (pls_solicitacao_resc_anexo_seq.nextval, nr_seq_solicitacao_p, ds_arquivo_p,
							sysdate, nm_usuario_p, sysdate,
							nm_usuario_p, nr_seq_segurado_p, sysdate,
							ie_apresenta_portal_p);
	end if;	

	insert into pls_segurado_doc_arq(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_segurado,
					cd_estabelecimento,
					nr_seq_tipo_documento,
					ds_arquivo,
					ie_origem_anexo,
					ie_apresenta_portal)
				values(	pls_segurado_doc_arq_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_segurado_p,
					cd_estabelecimento_p,
					nr_seq_tipo_documento_p,
					ds_arquivo_p,
					'P',
					ie_apresenta_portal_p);	
	
	commit;	
end if;

end pls_salvar_anex_rescis_seg_web;
/
