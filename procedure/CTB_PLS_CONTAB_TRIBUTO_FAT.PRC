create or replace
procedure ctb_pls_contab_tributo_fat(
				nr_lote_contabil_p		number,
				nm_usuario_p			Varchar2,
				ie_exclusao_p			varchar2,
				ds_retorno_p		in out	varchar2) is
				
dt_inicial_w			date;
dt_final_w			date;
dt_referencia_w			date;
dt_contabil_w			date;
dt_emissao_w			date;
dt_emissao_titulo_w		date;
dt_referencia_fatura_w		date;
dt_geracao_titulos_w		date;
nr_seq_info_ctb_w		number(2);
cd_estabelecimento_w		number(4);
cd_centro_custo_w		number(8);
cd_centro_custo_fixo_w		number(8);
nr_lote_contabil_w		number(10);
nr_seq_agrupamento_w		number(10);
nr_seq_regra_cc_w		number(10);
nr_seq_w_movto_cont_w		number(10);
nr_seq_prot_conta_w		number(10);
nr_nota_fiscal_w		number(20);
ie_centro_custo_w		varchar2(1);
ie_compl_hist_w			varchar2(2);
cd_conta_contabil_w		varchar2(20);
ie_debito_credito_w		varchar2(20);
ds_compl_historico_w		varchar2(255);
ds_compl_historico_ww		varchar2(255);
nm_agrupador_w			varchar2(255);
nm_pagador_fat_w		varchar2(255);
nm_prestador_w			varchar2(255);
ds_conteudo_w			varchar2(4000);
vl_retorno_w			varchar2(2000);

nr_seq_prestador_w		pls_prestador.nr_sequencia%type;	
dt_mes_competencia_w		pls_protocolo_conta.dt_mes_competencia%type;
cd_cgc_pag_w			pls_contrato_pagador.cd_cgc%type;
cd_pf_pag_w			pls_contrato_pagador.cd_pessoa_fisica%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
cd_cgc_prestador_w		pls_prestador.cd_cgc%type;
cd_cpf_prestador_w		pessoa_fisica.nr_cpf%type;
nr_seq_lote_fat_w		pls_lote_faturamento.nr_sequencia%type;
nr_titulo_fat_w			titulo_receber.nr_titulo%type;
nr_nota_fiscal_fat_w		nota_fiscal.nr_nota_fiscal%type;
nm_tabela_w			w_movimento_contabil.nm_tabela%type;
nr_seq_plano_w			pls_plano.nr_sequencia%type;
ie_regulamentacao_w		pls_plano.ie_regulamentacao%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nm_atributo_w			w_movimento_contabil.nm_atributo%type;
cd_tipo_lote_contabil_w		lote_contabil.cd_tipo_lote_contabil%type;
nr_seq_fatura_w			pls_fatura.nr_sequencia%type;
nr_seq_pagador_w		pls_fatura.nr_seq_pagador%type;
nr_seq_trib_item_w		pls_fatura_item_trib.nr_sequencia%type;
cd_historico_w			pls_fatura_item_trib.cd_historico%type;
vl_contabil_w			pls_fatura_item_trib.vl_evento%type;
vl_evento_origem_w		pls_fatura_item_trib.vl_evento_origem%type;
cd_tributo_w			pls_fatura_trib.cd_tributo%type;
ie_data_rec_faturamento_w	pls_parametro_contabil.ie_data_rec_faturamento%type;
ie_cancelamento_w		pls_fatura.ie_cancelamento%type;

Cursor c_tributos is
	select	i.nr_sequencia nr_seq_trib_item,
		f.nr_sequencia nr_seq_fatura,
		i.cd_conta_prov_cred cd_conta_credito,
		i.cd_conta_prov_deb cd_conta_debito,
		i.cd_historico_prov cd_historico,
		i.vl_evento,
		i.vl_evento_origem,
		'PLS_FATURA_ITEM_TRIB' nm_tabela,
		'VL_EVENTO_NDR' nm_atributo,
		f.dt_mes_competencia,
		t.cd_tributo,
		f.nr_seq_pagador,
		f.nr_seq_lote nr_seq_lote_fat,
		30 nr_seq_info_ctb,
		f.dt_emissao,
		r.dt_emissao dt_emissao_titulo,
		f.nr_titulo nr_titulo_fat,
		f.ie_cancelamento,
		l.dt_geracao_titulos
	from	pls_lote_faturamento 	l,
		pls_fatura		f,
		pls_fatura_trib		t,
		pls_fatura_item_trib	i,
		titulo_receber 		r
	where	l.nr_sequencia 	= f.nr_seq_lote 
	and	f.nr_sequencia	= t.nr_seq_fatura
	and	t.nr_sequencia	= i.nr_seq_fatura_trib
	and	f.nr_titulo 	= r.nr_titulo(+)
	and	i.nr_lote_contabil = nr_lote_contabil_p
	and	i.ie_tipo_valor_fat = 'NDC'
	union all
	select	i.nr_sequencia nr_seq_trib_item,
		f.nr_sequencia nr_seq_fatura,
		i.cd_conta_cred,
		i.cd_conta_deb,
		i.cd_historico,
		i.vl_evento,
		i.vl_evento_origem,
		'PLS_FATURA_ITEM_TRIB' nm_tabela,
		'VL_EVENTO' nm_atributo,
		f.dt_mes_competencia,
		t.cd_tributo,
		f.nr_seq_pagador,
		f.nr_seq_lote nr_seq_lote_fat,
		30 nr_seq_info_ctb,
		f.dt_emissao,
		r.dt_emissao dt_emissao_titulo,
		f.nr_titulo nr_titulo_fat,
		f.ie_cancelamento,
		l.dt_geracao_titulos
	from	pls_lote_faturamento 	l,
		pls_fatura		f,
		pls_fatura_trib		t,
		pls_fatura_item_trib	i,
		titulo_receber 		r
	where	l.nr_sequencia 	= f.nr_seq_lote 
	and	f.nr_sequencia	= t.nr_seq_fatura
	and	t.nr_sequencia	= i.nr_seq_fatura_trib
	and	f.nr_titulo 	= r.nr_titulo(+)
	and	i.nr_lote_contabil = nr_lote_contabil_p
	and	i.ie_tipo_valor_fat = 'F';

Cursor c_debito_credito is
	select	'C'
	from	dual
	union all
	select	'D'
	from	dual;
	
type 		fetch_array is table of c_tributos%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
c_tributos_w	vetor;

cursor c_movimento is
	select	rowid
	from	movimento_contabil
	where	nr_lote_contabil	= nr_lote_contabil_p;

begin

nr_seq_w_movto_cont_w	:= 0;

select	dt_referencia,
	cd_estabelecimento,
	nr_lote_contabil,
	cd_tipo_lote_contabil
into	dt_referencia_w,
	cd_estabelecimento_w,
	nr_lote_contabil_w,
	cd_tipo_lote_contabil_w
from	lote_contabil
where	nr_lote_contabil	= nr_lote_contabil_p;

dt_inicial_w		:= trunc(dt_referencia_w,'month');
dt_final_w		:= fim_dia(fim_mes(dt_inicial_w));

select	max(a.ie_data_rec_faturamento)
into	ie_data_rec_faturamento_w
from	pls_parametro_contabil	a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_exclusao_p = 'S') then
	wheb_usuario_pck.set_ie_lote_contabil('S');

	for reg in c_movimento
		loop
		delete	from movimento_contabil
		where   rowid	= reg.rowid;
		end loop;

	update	pls_fatura_item_trib
	set	nr_lote_contabil 		= null,
		dt_atualizacao			= sysdate,
		nm_usuario			= nm_usuario_p
	where	nr_lote_contabil		= nr_lote_contabil_p;

	update	lote_contabil
	set	vl_credito	= 0,
		vl_debito	= 0,
		dt_geracao_lote	= null
	where	nr_lote_contabil	= nr_lote_contabil_p;

	wheb_usuario_pck.set_ie_lote_contabil('N');
else
	wheb_usuario_pck.set_ie_lote_contabil('S');
		
	update	pls_fatura_item_trib	a
	set	a.nr_lote_contabil	= nr_lote_contabil_p
	where	nvl(a.nr_lote_contabil,0)		= 0
	and	exists	(select	1
			from	pls_fatura_trib	b,
				pls_fatura	f
			where	f.nr_sequencia	= b.nr_seq_fatura
			and	b.nr_sequencia	= a.nr_seq_fatura_trib
			and	f.nr_lote_contabil = nr_lote_contabil_p);

	wheb_usuario_pck.set_ie_lote_contabil('N');

	nm_agrupador_w	:= nvl(trim(obter_agrupador_contabil(cd_tipo_lote_contabil_w)), 'NR_SEQ_FATURA');

	begin
	ie_compl_hist_w	:= obter_se_compl_tipo_lote(cd_estabelecimento_w, cd_tipo_lote_contabil_w);
	exception
	when others then
		ie_compl_hist_w	:= 'N';
	end;
		
	open c_tributos;
	loop
	fetch c_tributos bulk collect into s_array limit 10000;
		c_tributos_w(i)		:= s_array;
		i			:= i + 1;
	exit when c_tributos%notfound;
	end loop;
	close c_tributos;

	for i in 1..c_tributos_w.count loop
		begin
		s_array	:= c_tributos_w(i);

		for z in 1..s_array.count loop
			begin

			open c_debito_credito;
			loop
			fetch c_debito_credito into	
				ie_debito_credito_w;
			exit when c_debito_credito%notfound;
				begin
				
				if	(ie_debito_credito_w = 'C') then
					cd_conta_contabil_w	:= s_array(z).cd_conta_credito;
				elsif	(ie_debito_credito_w = 'D') then
					cd_conta_contabil_w	:= s_array(z).cd_conta_debito;
				end if;
				
				nr_seq_trib_item_w		:= s_array(z).nr_seq_trib_item;
				nr_seq_fatura_w			:= s_array(z).nr_seq_fatura;
				cd_historico_w			:= s_array(z).cd_historico;
				vl_contabil_w			:= s_array(z).vl_evento;
				vl_evento_origem_w		:= s_array(z).vl_evento_origem;
				nm_tabela_w			:= s_array(z).nm_tabela;
				nm_atributo_w			:= s_array(z).nm_atributo;
				dt_referencia_fatura_w		:= s_array(z).dt_mes_competencia;
				cd_tributo_w			:= s_array(z).cd_tributo;
				nr_seq_pagador_w		:= s_array(z).nr_seq_pagador;
				nr_seq_lote_fat_w		:= s_array(z).nr_seq_lote_fat;
				nr_seq_info_ctb_w		:= s_array(z).nr_seq_info_ctb;
				dt_emissao_w			:= s_array(z).dt_emissao;
				dt_emissao_titulo_w		:= s_array(z).dt_emissao_titulo;
				nr_titulo_fat_w			:= s_array(z).nr_titulo_fat;
				ie_cancelamento_w		:= s_array(z).ie_cancelamento;
				dt_geracao_titulos_w		:= s_array(z).dt_geracao_titulos;
								
				dt_contabil_w		:= dt_referencia_fatura_w;
				
				if	(ie_data_rec_faturamento_w = 'EF') then
					dt_contabil_w	:= dt_emissao_w;
				elsif	(ie_data_rec_faturamento_w = 'TI') then
					dt_contabil_w	:= dt_emissao_titulo_w;
					if (nvl(nr_titulo_fat_w,0) = 0) and (ie_cancelamento_w = 'C') then
						dt_contabil_w	:= dt_geracao_titulos_w;
					end if;
				end if;

				if	(ie_cancelamento_w = 'E') then
					dt_contabil_w		:= dt_referencia_fatura_w;
				end if;
				
				if	(cd_conta_contabil_w is null) then	
					insert into w_pls_movimento_sem_conta
						(nr_sequencia,
						cd_item,
						ds_item,
						ie_tipo_item,
						dt_atualizacao,
						nm_usuario,
						vl_item,
						dt_referencia,
						nr_lote_contabil,
						ie_proc_mat_item,
						ie_deb_cred,
						ds_observacao)
					values	(w_pls_movimento_sem_conta_seq.nextval,
						null,
						null,
						null,
						sysdate,
						nm_usuario_p,
						vl_contabil_w,
						dt_referencia_w,
						nr_lote_contabil_w,
						null,
						ie_debito_credito_w,
						--'Impostos do faturamento'
						obter_desc_expressao(967739));
				else
					if	(nm_agrupador_w = 'NR_SEQ_FATURA') then
						nr_seq_agrupamento_w	:= nr_seq_fatura_w;
					elsif	(nm_agrupador_w = 'NR_SEQ_LOTE_FATUR') then
						nr_seq_agrupamento_w	:= nr_seq_lote_fat_w;
					end if;
										
					select	max(nr_seq_plano),
						max(ie_regulamentacao),
						max(nr_seq_segurado),
						max(nr_seq_prestador),
						max(nr_seq_protocolo),
						max(nr_seq_conta),
						max(nr_seq_prot_conta),
						max(nr_titulo_fat)
					into	nr_seq_plano_w,
						ie_regulamentacao_w,
						nr_seq_segurado_w,
						nr_seq_prestador_w,
						nr_seq_protocolo_w,
						nr_seq_conta_w,
						nr_seq_prot_conta_w,
						nr_titulo_fat_w
					from	(
						select	l.nr_sequencia nr_seq_plano,
							l.ie_regulamentacao,
							s.nr_sequencia nr_seq_segurado,
							t.nr_seq_prestador_exec nr_seq_prestador,
							t.nr_seq_protocolo,
							t.nr_sequencia nr_seq_conta,
							t.nr_seq_prot_conta,
							f.nr_titulo nr_titulo_fat
						from	pls_conta 			t,
							pls_conta_pos_estabelecido 	a,
							pls_conta_pos_estab_contab 	e,
							pls_fatura 			f,
							pls_fatura_proc 		p,
							pls_fatura_conta 		c,
							pls_fatura_evento 		d,
							pls_plano			l,
							pls_segurado			s
						where	t.nr_sequencia 		= a.nr_seq_conta
						and	a.nr_sequencia 		= e.nr_seq_conta_pos
						and	a.nr_sequencia 		= p.nr_seq_conta_pos_estab
						and	c.nr_sequencia 		= p.nr_seq_fatura_conta
						and	d.nr_sequencia 		= c.nr_seq_fatura_evento
						and	f.nr_sequencia 		= d.nr_seq_fatura
						and	s.nr_sequencia		= t.nr_seq_segurado
						and	l.nr_sequencia		= t.nr_seq_plano
						and	f.nr_sequencia		= nr_seq_fatura_w
						union all
						select	l.nr_sequencia,
							l.ie_regulamentacao,
							s.nr_sequencia,
							t.nr_seq_prestador_exec,
							t.nr_seq_protocolo,
							t.nr_sequencia,
							t.nr_seq_prot_conta,
							f.nr_titulo
						from	pls_conta 			t,
							pls_conta_pos_estabelecido 	a,
							pls_conta_pos_estab_contab 	e,
							pls_fatura 			f,
							pls_fatura_proc 		p,
							pls_fatura_conta 		c,
							pls_fatura_evento 		d,
							pls_plano			l,
							pls_segurado			s
						where	t.nr_sequencia 		= a.nr_seq_conta
						and	a.nr_sequencia 		= e.nr_seq_conta_pos
						and	a.nr_sequencia 		= p.nr_seq_conta_pos_estab
						and	c.nr_sequencia 		= p.nr_seq_fatura_conta
						and	d.nr_sequencia 		= c.nr_seq_fatura_evento
						and	f.nr_sequencia 		= d.nr_seq_fatura
						and	s.nr_sequencia		= t.nr_seq_segurado
						and	l.nr_sequencia		= t.nr_seq_plano
						and	f.nr_sequencia		= nr_seq_fatura_w);
									
					select	ie_centro_custo,
						cd_centro_custo
					into	ie_centro_custo_w,
						cd_centro_custo_fixo_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
									
					cd_centro_custo_w := null;
					if	(ie_centro_custo_w = 'S') then
						if	(cd_centro_custo_fixo_w is not null) then
							cd_centro_custo_w	:= cd_centro_custo_fixo_w;
						else
							pls_obter_centro_custo(	'D',
										nr_seq_plano_w,
										cd_estabelecimento_w,
										'',
										'',
										ie_regulamentacao_w,
										nr_seq_segurado_w,
										15,
										cd_centro_custo_w,
										nr_seq_regra_cc_w);
						end if;
					end if;
					
					if	(ie_compl_hist_w = 'S') then
						if	(nvl(nr_seq_prestador_w,0) > 0) then
							select	a.cd_pessoa_fisica,
								a.cd_cgc
							into	cd_pessoa_fisica_w,
								cd_cgc_prestador_w
							from	pls_prestador	a
							where	a.nr_sequencia	= nr_seq_prestador_w;
							
							begin
							if	(cd_cgc_prestador_w is not null) then
								select	substr(a.ds_razao_social,1,80)
								into	nm_prestador_w
								from	pessoa_juridica	a
								where	a.cd_cgc	= cd_cgc_prestador_w;
							else
								select	substr(a.nm_pessoa_fisica,1,80)
								into	nm_prestador_w
								from	pessoa_fisica	a
								where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
							end if;
							exception
							when others then
								nm_prestador_w	:= null;
							end;
							
							cd_cpf_prestador_w	:= null;
								
							if	(cd_pessoa_fisica_w is not null) then
								select	nr_cpf
								into	cd_cpf_prestador_w
								from	pessoa_fisica
								where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
							end if;
							
						end if;
						
						if	(nr_seq_pagador_w is not null) then
							select	a.cd_pessoa_fisica,
								a.cd_cgc
							into	cd_pf_pag_w,
								cd_cgc_pag_w
							from	pls_contrato_pagador	a
							where	a.nr_sequencia		= nr_seq_pagador_w;
							
							begin
							if	(cd_cgc_pag_w is not null) then
								select	substr(a.nm_fantasia,1,80)
								into	nm_pagador_fat_w
								from	pessoa_juridica	a
								where	a.cd_cgc	= cd_cgc_pag_w;
							else
								select	substr(a.nm_pessoa_fisica,1,80)
								into	nm_pagador_fat_w
								from	pessoa_fisica	a
								where	a.cd_pessoa_fisica	= cd_pf_pag_w;
							end if;
							exception
							when others then
								nm_pagador_fat_w	:= null;
							end;
						end if;
						
						if 	(nvl(nr_seq_protocolo_w,0) > 0) then
							select	dt_mes_competencia
							into	dt_mes_competencia_w
							from	pls_protocolo_conta
							where	nr_sequencia = nr_seq_protocolo_w;
						end if;
						
						select	max(nr_nota_fiscal)
						into	nr_nota_fiscal_w
						from	pls_prot_conta_titulo
						where	nr_sequencia	= nr_seq_prot_conta_w;
						
						begin
						select	a.nr_nota_fiscal
						into	nr_nota_fiscal_fat_w
						from	nota_fiscal	a
						where	a.nr_seq_fatura	= nr_seq_fatura_w;
						exception
						when others then
							nr_nota_fiscal_fat_w	:= null;
						end;
					
						ds_conteudo_w	:= substr(	nr_seq_prestador_w	||'#@'||
										nm_prestador_w		||'#@'||
										nr_seq_protocolo_w	||'#@'||
										nr_seq_conta_w		||'#@'||
										cd_cgc_prestador_w	||'#@'||
										cd_cpf_prestador_w	||'#@'||
										nr_nota_fiscal_w	||'#@'||
										nr_seq_lote_fat_w	||'#@'||
										dt_contabil_w		||'#@'||
										null			||'#@'||
										nm_pagador_fat_w	||'#@'||
										nr_titulo_fat_w		||'#@'||
										nr_nota_fiscal_fat_w	||'#@'||
										dt_mes_competencia_w,1,4000);
						
						begin
						ds_compl_historico_ww	:= substr(obter_compl_historico(cd_tipo_lote_contabil_w, cd_historico_w, ds_conteudo_w),1,255);
						exception
						when others then
							ds_compl_historico_ww	:= null;
						end;
						ds_compl_historico_w	:= substr(nvl(ds_compl_historico_ww, ds_compl_historico_w),1,255);					
					end if;
					
					nr_seq_w_movto_cont_w	:= nr_seq_w_movto_cont_w + 1;
												
					insert into w_movimento_contabil
						(nr_lote_contabil,
						nr_sequencia,
						cd_conta_contabil,
						ie_debito_credito,
						cd_historico,
						dt_movimento,
						vl_movimento,
						cd_estabelecimento,
						cd_centro_custo,
						ds_compl_historico,
						nr_seq_agrupamento,
						nr_seq_tab_orig,
						nr_seq_info,
						nm_tabela,
						nm_atributo)
					values	(nr_lote_contabil_p,
						nr_seq_w_movto_cont_w,
						cd_conta_contabil_w,
						ie_debito_credito_w,
						cd_historico_w,
						dt_contabil_w,
						vl_contabil_w,
						cd_estabelecimento_w,
						cd_centro_custo_w,
						ds_compl_historico_w,
						nr_seq_agrupamento_w,
						nr_seq_trib_item_w,
						nr_seq_info_ctb_w,
						nm_tabela_w,
						nm_atributo_w);
				end if;
				end;
			end loop;
			close c_debito_credito;
			
			end;
		end loop;
		
		end;
	end loop;
end if;

commit;

end ctb_pls_contab_tributo_fat;
/