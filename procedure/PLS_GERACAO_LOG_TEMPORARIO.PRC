create or replace
procedure pls_geracao_log_temporario(	nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,
					ds_observacao_p		pls_log_temporario.ds_observacao%type,
					nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:

Rotina criada especificamente para gera��o de logs, n�o deve ter commit dentro da rotina
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin



insert into pls_log_temporario(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec,ds_observacao,
		nr_seq_guia)
	values(	pls_log_temporario_seq.nextval, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, ds_observacao_p,
		nr_seq_guia_p);


end pls_geracao_log_temporario;
/