create or replace
procedure del_perda_ganho_nao_liberado(	nr_cirurgia_p		number,
					nr_seq_pepo_p		number,
					nm_usuario_p		varchar2)
					is

begin

delete 	atendimento_perda_ganho
where	((nr_cirurgia = nr_cirurgia_p) or (nr_seq_pepo = nr_seq_pepo_p))
and	dt_liberacao is null
and	ie_origem	= 'S';

commit;

end del_perda_ganho_nao_liberado;
/