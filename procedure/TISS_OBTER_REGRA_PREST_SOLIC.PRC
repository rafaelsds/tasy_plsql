create or replace
procedure TISS_OBTER_REGRA_PREST_SOLIC (	cd_convenio_p		number,
						cd_estabelecimento_p	number,
						cd_setor_entrada_p	number,
						ie_clinica_p		number,
						cd_procedencia_p		varchar2,
						ie_tipo_atendimento_p	number,
						ie_autorizacao_p		varchar2,
						ie_solicitante_p 		out varchar2,
						cd_cgc_prest_solic_p	out varchar2,
						cd_medico_atendimento_p	varchar2,						
						cd_medico_prestador_p	out varchar2,
						cd_interno_p		out varchar2,
						ds_prestador_tiss_p	out varchar2,
						cd_medico_referido_p	in varchar2,
						cd_categoria_p		in varchar2,
						dt_referencia_p		in date,
						ie_tipo_protocolo_p	in number,
						cd_medico_prescr_p	in varchar2 default null,
						cd_plano_convenio_p	in varchar2 default null,
						nr_seq_regra_p		out number) is

ie_solicitante_w		varchar2(255);
cd_cgc_prestador_w	varchar2(255);
cd_medico_prestador_w	varchar2(255);
cd_interno_w		varchar2(60);
ds_prestador_tiss_w	varchar2(255);
ie_vinculo_medico_w	number(10);
nr_seq_regra_w		TISS_REGRA_PRESTADOR_SOLIC.nr_sequencia%type;
cursor c01 is
select	a.ie_solicitante,
	a.cd_cgc_prestador,
	a.cd_medico_prest,
	a.cd_interno,
	a.ds_prestador_tiss,
	a.nr_sequencia
from	TISS_REGRA_PRESTADOR_SOLIC a
where	a.cd_convenio							= cd_convenio_p
and	a.cd_estabelecimento						= cd_estabelecimento_p
and	nvl(a.cd_setor_entrada, nvl(cd_setor_entrada_p,0))			= nvl(cd_setor_entrada_p,0)
and	nvl(a.ie_clinica, nvl(ie_clinica_p, 0))				= nvl(ie_clinica_p, 0)
and	nvl(to_char(a.cd_procedencia), nvl(cd_procedencia_p,'0'))		= nvl(cd_procedencia_p,'0')
and	((nvl(a.ie_autorizacao, nvl(ie_autorizacao_p, 'N'))			= nvl(ie_autorizacao_p, 'N')) or
	 (nvl(a.ie_autorizacao, 'N') = 'N'))
and	nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0))		= nvl(ie_tipo_atendimento_p,0)
and	nvl(a.cd_medico_atendimento, nvl(cd_medico_atendimento_p, 0))		= nvl(cd_medico_atendimento_p, 0)
and	nvl(a.cd_medico_referido, nvl(cd_medico_referido_p,0))		= nvl(cd_medico_referido_p,0)
and	nvl(a.cd_categoria, nvl(cd_categoria_p,0))				= nvl(cd_categoria_p,0)
and	nvl(a.ie_tipo_protocolo, nvl(ie_tipo_protocolo_p,0))			= nvl(ie_tipo_protocolo_p,0)
and	nvl(a.ie_vinculo_medico, nvl(ie_vinculo_medico_w,0))			= nvl(ie_vinculo_medico_w,0)
and	nvl(a.cd_medico_prescr, nvl(cd_medico_prescr_p,0))			= nvl(cd_medico_prescr_p,0)
and	nvl(a.nr_seq_plano, nvl(cd_plano_convenio_p,0))			= nvl(cd_plano_convenio_p,0)
and	nvl(dt_referencia_p,sysdate)	between nvl(a.dt_inicio_vigencia,to_date('01/01/1990','dd/mm/yyyy')) and ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(nvl(a.dt_fim_vigencia,sysdate + 1)) 
order by
	a.cd_convenio,
	a.cd_estabelecimento,
	nvl(a.cd_setor_entrada,0),
	nvl(a.ie_clinica,0),
	nvl(a.cd_procedencia,0),
	nvl(a.ie_tipo_atendimento,0),
	nvl(a.cd_medico_atendimento,0),
	nvl(a.cd_medico_referido,0),
	nvl(a.cd_categoria,0),
	nvl(a.dt_inicio_vigencia,to_date('01/01/1990','dd/mm/yyyy')),
	nvl(a.ie_tipo_protocolo,0),
	nvl(a.ie_vinculo_medico,0),
	nvl(a.cd_medico_prescr,0),
	nvl(a.nr_seq_plano,0);	

begin

if	(cd_medico_atendimento_p is not null) then
	
	select	max(ie_vinculo_medico)
	into	ie_vinculo_medico_w
	from	medico
	where	cd_pessoa_fisica	= cd_medico_atendimento_p;
	
end if;

open c01;
loop
fetch c01 into
	ie_solicitante_w,
	cd_cgc_prestador_w,
	cd_medico_prestador_w,
	cd_interno_w,
	ds_prestador_tiss_w,
	nr_seq_regra_w;
exit when c01%notfound;

	ie_solicitante_p	:= ie_solicitante_w;
	cd_cgc_prest_solic_p	:= cd_cgc_prestador_w;
	cd_medico_prestador_p	:= cd_medico_prestador_w;
	cd_interno_p		:= cd_interno_w;
	ds_prestador_tiss_p	:= ds_prestador_tiss_w;
	nr_seq_regra_p		:= nr_seq_regra_w;
end loop;
close c01;

end TISS_OBTER_REGRA_PREST_SOLIC;
/