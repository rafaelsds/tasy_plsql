create or replace
procedure Qt_Gerar_Horario_Sim(
			nr_seq_pendencia_p	number,
			nr_seq_agenda_p		number,
			dt_agenda_p		date,
			nm_usuario_p		Varchar2,
			nr_min_duracao_p	number,
			cd_pessoa_fisica_p	varchar2,
			nr_seq_grupo_quimio_p	number default null,
			cd_estabelecimento_p	number default null,
			nr_seq_pac_sim_p	number) is 

nr_seq_local_w		number(10);
hr_atual_w		date;
hr_final_dia_w		date;
hr_inicial_dia_w	date;
hr_inicial_truncada_w	date;
ie_status_w		varchar2(15);
qt_marcado_w		number(10);
ie_dia_Semana_w		number(5);
nr_seq_pend_maracada_w	number(10);
nr_Seq_pend_agenda_w	number(10);
nr_seq_atend_marcado_w	number(10);
nr_seq_pend_agenda_marcado_w	number(10);
nr_minuto_aval_w	number(10);
nr_seq_agequi_w		number(10);
qt_pac_horario_w	number(10);
ie_horario_bloq_w	varchar2(1);
nr_seq_agenda_w		number(10);
nr_seq_agenda_marcado_w	number(10);
qt_turno_w		number(10);
dt_agenda_w		date;
nr_duracao_W		number(10);
qt_tempo_entre_hor_W	number(10);
ie_indice_w		number(5);
ie_ind_w		number(5);
nr_seq_prof_w		number(10);
dt_min_agendamento_w	date;
dt_max_agendamento_w	date;
hr_inicio_turno_w	date;
hr_fim_turno_w		date;
ie_possui_turno_w	varchar2(1)	:= 'N';
qt_marcacao_w		number(10);
ie_feriado_w		varchar2(1);
cd_estabelecimento_w	number(4);
nr_seq_atendimento_w	number(10);
ie_regra_bloq_medic_w	varchar2(1);
cd_estab_pend_w		number(4,0);
nr_min_apres_local_w	number(2,0);
dt_vigencia_inicial_w	date;
dt_vigencia_final_w	date;
ie_permite_w		varchar2(1);
/*
status
L - Livre
I - Invisivel (utilizado para montar corretamente os panels em caso de inicio em hor�rios "quebrados")
EA - Em agendamento
M - Marcado
*/

/*sera parametro*/
--nr_min_duracao_p	number(10)	:= 15;


/*cursor dos locais*/
Cursor C01 is
	select	nr_sequencia,
		cd_estabelecimento,
		nr_min_apres
	from	qt_local
	where	ie_situacao	= 'A'
	and	((nr_seq_grupo_quimio	= nr_seq_grupo_quimio_p) or (nr_seq_grupo_quimio_p is null))
	and	((cd_estabelecimento	= cd_estabelecimento_p) or (cd_estabelecimento_p is null))
	order by 1;
	
/*cursor dos turnos dos locais*/	
Cursor C02 is
	select	hr_inicial,
		hr_final,
		dt_vigencia_inicial,
		dt_vigencia_final
	from	qt_local_turno
	where	nr_seq_local	= nr_seq_local_w
	and	((dt_dia_semana = ie_dia_semana_w) or ((dt_dia_semana = 9) and (ie_dia_Semana_w not in (7,1))))	
	and	((ie_feriado_w	<> 'S' and nvl(ie_feriado,'N') = 'N') or (nvl(ie_feriado, 'N') = 'S' and ie_feriado_w = 'S'))
	/*and	((dt_vigencia_inicial is null) or (dt_vigencia_inicial <= sysdate))
	and	((dt_vigencia_final is null) or (dt_vigencia_final   >= sysdate))*/
	order by 1;
	
cursor C03 is
	select	dt_Agenda,
		nr_seq_paciente,
		nr_Seq_agenda,
		nr_duracao
	from	agenda_quimio_marcacao
	where	nr_seq_local		= nr_Seq_local_w
	and	trunc(dt_agenda)	= trunc(dt_agenda_p);
			
begin

delete 	w_agenda_quimio
where	nm_usuario		= nm_usuario_p;

select	obter_cod_dia_semana(dt_agenda_p)
into	ie_dia_semana_w
from	dual;

ie_regra_bloq_medic_w	:= nvl(Obter_Valor_Param_Usuario(865, 26, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S');

open C01;
loop
fetch C01 into	
	nr_seq_local_w,
	cd_estabelecimento_w,
	nr_min_apres_local_w;
exit when C01%notfound;
	begin
	
	select	decode(count(*),0,'N','S')
	into	ie_feriado_w
	from 	feriado a
	where 	a.cd_estabelecimento 	= nvl(cd_estabelecimento_p, cd_estabelecimento_w)
	and	a.dt_feriado between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400;
	
	ie_possui_turno_W	:= 'N'; 
	select	count(*)
	into	qt_turno_w
	from	qt_local_turno
	where	nr_seq_local	= nr_seq_local_w
	and	((dt_dia_semana = ie_dia_semana_w) or ((dt_dia_semana = 9) and (ie_dia_Semana_w not in (7,1))));
	/*and	((dt_vigencia_inicial is null) or (dt_vigencia_inicial <= sysdate))
	and	((dt_vigencia_final is null) or (dt_vigencia_final   >= sysdate));*/
	
	
	if	(qt_turno_w	= 0) then
		open C03;
		loop
		fetch C03 into	
			dt_Agenda_w,
			nr_seq_pend_agenda_w,
			nr_seq_agenda_w,
			nr_duracao_W;
		exit when C03%notfound;
			begin
			qt_tempo_entre_hor_W	:= OBTER_MIN_ENTRE_DATAS( dt_agenda_w, dt_Agenda_W + (nr_duracao_W - 1) / 1440,1);
			
			ie_indice_w	:= trunc(qt_tempo_entre_hor_W / nvl(nr_min_apres_local_w,nr_min_duracao_p));
		
			ie_ind_w	:= 0;
			while ie_ind_w	<= ie_indice_w loop
				begin
				insert into w_agenda_quimio
					(--nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_horario,
					ie_status,
					nr_seq_local,
					nr_seq_paciente,
					--nr_seq_pend_agenda,
					nr_seq_agequi)
				values
					(--w_agenda_quimio_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p, 
					dt_Agenda_w,
					'EA',--ie_status_w, 
					nr_seq_local_w,
					nr_seq_pend_agenda_w,
					--nvl(nr_seq_pend_agenda_w, null),
					nr_seq_agenda_w);
					
				dt_agenda_w	:= dt_agenda_w + nvl(nr_min_apres_local_w,nr_min_duracao_p) / 1440;
				ie_ind_w	:= ie_ind_w + 1;
				end;
			end loop;
			end;
		end loop;
		close C03;
	end if;
	
	open C02;
	loop
	fetch C02 into	
		hr_inicial_dia_w,
		hr_final_dia_w,
		dt_vigencia_inicial_w,
		dt_vigencia_final_w;
	exit when C02%notfound;
		begin
		hr_inicial_dia_w	:= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_dia_w, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		hr_final_dia_w		:= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_final_dia_w, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		
		hr_inicio_turno_w	:= hr_inicial_dia_w;
		hr_fim_turno_w		:= hr_final_dia_w;
		
		select	max(dt_agenda + nr_duracao / 1440) 
		into	dt_max_agendamento_w
		from 	agenda_quimio_marcacao 
		where 	trunc(dt_Agenda) = trunc(dt_agenda_p);

		if	(dt_max_agendamento_w is not null) and
			(dt_max_agendamento_w > hr_final_dia_w) then
			hr_final_dia_w	:= dt_max_agendamento_w;
		end if;
		
		select 	min(dt_agenda)
		into	dt_min_agendamento_w
		from 	agenda_quimio_marcacao 
		where 	trunc(dt_Agenda) = trunc(dt_agenda_p);		
		
		if	(dt_min_agendamento_w is not null) and
			(dt_min_agendamento_w < hr_inicial_dia_w) then
			hr_inicial_dia_w	:= dt_min_agendamento_w;
		end if;
		
		hr_inicial_truncada_w	:= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_dia_w, 'hh24') || ':00:00','dd/mm/yyyy hh24:mi:ss');
		if	(hr_inicial_truncada_w	<> hr_inicial_dia_w) then
			hr_atual_w	:= hr_inicial_truncada_w;
		else
			hr_atual_w	:= hr_inicial_dia_w;
		end if;
		
		while	(hr_atual_w < hr_final_dia_w) loop 
			begin
			ie_possui_turno_w	:= 'S';
				nr_seq_pend_agenda_w	:= nr_seq_pendencia_p;
				if	(hr_atual_w	< hr_inicio_turno_w) or
					(hr_atual_w + nvl(nr_min_apres_local_w,nr_min_duracao_p) / 1440	> hr_fim_turno_w) then
					ie_status_w	:= 'I';
					select	max(nr_seq_paciente),
						max(nr_seq_agenda)
					into	nr_seq_pend_agenda_marcado_w,
						nr_seq_agenda_marcado_w
					from	agenda_quimio_marcacao
					where	hr_atual_w between dt_agenda and dt_agenda + (nr_duracao -  1) / 1440
					and	nr_seq_local	= nr_seq_local_w
					and	nvl(ie_gerado, 'N')	= 'N';
					if	(nr_seq_pend_agenda_marcado_w	= nr_seq_pac_sim_p) or
						(nr_seq_agenda_marcado_w		= nr_seq_Agenda_p) then
						ie_status_w	:= 'EA';
						nr_seq_agenda_w		:= nr_seq_agenda_p;
					end if;
					
				else	
					ie_status_w	:= 'L';
				end if;
				if	(ie_status_w	<> 'I') then

					
					
					select	nvl(obter_agend_quimio_bloq(dt_agenda_p, hr_atual_w, (hr_atual_w + (nvl(nr_min_apres_local_w,nr_min_duracao_p) / 1440)), nr_seq_local_w),'N')
					into	ie_horario_bloq_w
					from	dual;
					
					
					
					if	(ie_horario_bloq_w = 'N') and 
						(cd_pessoa_fisica_p is not null) then
						
						
						
						select	max(nr_seq_prof)
						into	nr_seq_prof_w
						from	qt_paciente_prof
						where	cd_pessoa_fisica = cd_pessoa_fisica_p;
						
						if	(ie_regra_bloq_medic_w	= 'S') then						
							select 	obter_agend_quimio_bloq_prof(dt_agenda_p, hr_atual_w, (hr_atual_w + (nvl(nr_min_apres_local_w,nr_min_duracao_p) / 1440)),nr_seq_prof_w)
							into	ie_horario_bloq_w
							from	dual;
							
							select	max(nr_sequencia),
								max(cd_estabelecimento)
							into	nr_seq_atendimento_w,
								cd_estab_pend_w
							from	paciente_Atendimento_sim
							where	nr_seq_paciente		= nr_seq_pac_sim_p
							and	trunc(dt_prevista)	= trunc(hr_atual_w);
							
							ie_horario_bloq_w	:= Qt_Consistir_Bloq_Medic_Sim(hr_atual_w, nr_seq_atendimento_w, nm_usuario_p, cd_estabelecimento_w);
						end if;
					end if;
					
				
					if	(ie_horario_bloq_w = 'S') then
						
						ie_status_w := 'B';
						
					else
						-- trunc(nvl(dt_agenda,sysdate),'dd') + 86399/86400
						
						select	count(*)
						into	qt_pac_horario_w
						from	agenda_quimio
						where	hr_atual_w between dt_agenda and dt_agenda + (nvl(nr_minuto_duracao, trunc(OBTER_MIN_ENTRE_DATAS(dt_agenda, trunc(nvl(dt_agenda,sysdate),'dd') + 86399/86400 , 1))) - 1) / 1440
						and	nr_seq_local	= nr_seq_local_w
						and	nvl(ie_status_agenda,'N') <> 'C';
							
						if	(qt_pac_horario_w	< 2) then
							select	max(nr_seq_atendimento),
								max(nr_sequencia)
							into	nr_seq_atend_marcado_w,
								nr_seq_agequi_w
							from	agenda_quimio
							where	hr_atual_w between dt_agenda and dt_agenda + (nvl(nr_minuto_duracao, trunc(OBTER_MIN_ENTRE_DATAS(dt_agenda, trunc(nvl(dt_agenda,sysdate),'dd') + 86399/86400, 1))) - 1) / 1440
							and	nr_seq_local	= nr_seq_local_w
							and	nvl(ie_status_agenda,'N') <> 'C';
							
							/*select	max(nr_seq_pend_agenda)
							into	nr_seq_pend_maracada_w
							from	paciente_atendimento_sim
							where	nr_seq_atendimento	= nr_seq_atend_marcado_w;*/
							
							
							select	max(nr_seq_paciente),
								max(nr_seq_agenda)
							into	nr_seq_pend_agenda_marcado_w,
								nr_seq_agenda_marcado_w
							from	agenda_quimio_marcacao
							where	hr_atual_w between dt_agenda and dt_agenda + (nr_duracao - 1) / 1440
							and	nr_seq_local	= nr_seq_local_w
							and	nvl(ie_gerado, 'N')	= 'N';
							
							if	(nr_seq_pend_agenda_marcado_w	= nr_seq_pac_sim_p) or
								(nr_seq_agenda_marcado_w		= nr_seq_Agenda_p) then
								ie_status_w	:= 'EA';
								nr_seq_agenda_w		:= nr_seq_agenda_p;
							end if;
			 				
							
							if	(ie_status_w	<> 'EA') then
								if	(nr_seq_pend_maracada_w	> 0) then
									nr_Seq_pend_agenda_w	:= nr_seq_pend_maracada_w;
									ie_status_w	:= 'M';
								elsif	(nr_seq_agequi_w	> 0) then
									nr_seq_pend_agenda_marcado_w	:= null;
									nr_seq_pend_agenda_w		:= null;
									ie_status_w	:= 'M';
								else
									select	max(nr_seq_paciente),
										max(nr_seq_agenda)
									into	nr_seq_pend_agenda_marcado_w,
										nr_seq_agenda_marcado_w
									from	agenda_quimio_marcacao
									where	hr_atual_w between dt_agenda and dt_agenda + (nr_duracao - 1) / 1440
									and	nr_seq_local	= nr_seq_local_w
									and	nvl(ie_gerado, 'N')	= 'N';
									if	(nr_seq_pend_agenda_marcado_w	> 0) or
										(nr_seq_agenda_marcado_w	> 0)  then
										ie_status_w	:= 'EA';
										nr_seq_agenda_w		:= nr_seq_agenda_p;
									end if;
								end if;
							end if;
						else
							nr_seq_pend_agenda_marcado_w	:= null;
							nr_seq_pend_agenda_w		:= null;
							ie_Status_w			:= 'D';
							
						end if;
					
					
					end if;
					
					
						
				end if;
				
				select	decode(nr_seq_pend_agenda_w, 0, null, nr_seq_pend_agenda_w)
				into	nr_seq_pend_agenda_w
				from 	dual;
				
				select	decode(count(*),0,'N','S')
				into	ie_permite_w
				from	dual
				where	((hr_atual_w between nvl(dt_vigencia_inicial_w, trunc(dt_agenda_p)) and  nvl(dt_vigencia_final_w, fim_dia(dt_agenda_p))) or
					 (dt_vigencia_inicial_w is null and dt_vigencia_final_w is null)
					);
				
				if (ie_permite_w = 'S') then
					insert into w_agenda_quimio
						(--nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_horario,
						ie_status,
						nr_seq_local,
						nr_seq_paciente,
						--nr_seq_pend_agenda,
						nr_seq_agequi)
					values
						(--w_agenda_quimio_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p, 
						hr_atual_w,
						ie_status_w, 
						nr_seq_local_w,
						nr_seq_pend_agenda_w,
						--nvl(nvl(nr_seq_pend_agenda_marcado_w, nr_seq_pend_agenda_w),null),
						nr_seq_agenda_w);
				end if;	
			nr_seq_pend_agenda_marcado_w	:= null;
			nr_seq_agequi_w			:= null;
			nr_seq_agenda_w			:= null;
			hr_atual_w	:= hr_atual_w + nvl(nr_min_apres_local_w,nr_min_duracao_p) / 1440;
			end;
		end loop;
		end;
	end loop;
	close C02;
	if	(ie_possui_turno_W	= 'N') then
	
		select  min(b.hr_inicial)
		into	hr_inicial_dia_w
		from    qt_local a, 
			qt_local_turno b 
		where   b.nr_seq_local = a.nr_sequencia 
		and     a.cd_estabelecimento = cd_estabelecimento_p;
		/*and	((b.dt_vigencia_inicial is null) or (b.dt_vigencia_inicial <= sysdate))
		and	((b.dt_vigencia_final is null) or (b.dt_vigencia_final   >= sysdate));*/
		
		select  max(b.hr_final) 
		into	hr_final_dia_w
		from    qt_local a, 
			qt_local_turno b 
		where   b.nr_seq_local = a.nr_sequencia 
		and     a.cd_estabelecimento = cd_estabelecimento_p;
		/*and	((b.dt_vigencia_inicial is null) or (b.dt_vigencia_inicial <= sysdate))
		and	((b.dt_vigencia_final is null) or (b.dt_vigencia_final   >= sysdate));*/
	
		hr_inicial_dia_w	:= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_dia_w, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		hr_final_dia_w		:= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_final_dia_w, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	
		hr_inicial_truncada_w	:= to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_dia_w, 'hh24') || ':00:00','dd/mm/yyyy hh24:mi:ss');
	
		if	(hr_inicial_truncada_w	<> hr_inicial_dia_w) then
			hr_atual_w	:= hr_inicial_truncada_w;
		else
			hr_atual_w	:= hr_inicial_dia_w;
		end if;
		
		while	(hr_atual_w < hr_final_dia_w) loop 
			begin
			
			select	count(*)
			into	qt_marcacao_w
			from	agenda_quimio_marcacao
			where	hr_atual_w between dt_agenda and dt_agenda + (nr_duracao - 1) / 1440
			and	nr_seq_local	= nr_seq_local_w;
			--and	nvl(ie_gerado, 'N')	= 'N';
		
			if	(qt_marcacao_w	= 0) then
				insert into w_agenda_quimio
					(--nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_horario,
					ie_status,
					nr_seq_local,
					nr_seq_pend_agenda,
					nr_seq_agequi)
				values
					(--w_agenda_quimio_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p, 
					hr_atual_w,
					'I', 
					nr_seq_local_w,
					nvl(nvl(nr_seq_pend_agenda_marcado_w, nr_seq_pend_agenda_w),null),
					nvl(nr_seq_agenda_w, nr_seq_agenda_p));
			end if;
			hr_atual_w	:= hr_atual_w + nvl(nr_min_apres_local_w,nr_min_duracao_p) / 1440;
			end;
		end loop;
			
	end if;
	end;
end loop;
close C01; 

commit;

end Qt_Gerar_Horario_Sim;
/