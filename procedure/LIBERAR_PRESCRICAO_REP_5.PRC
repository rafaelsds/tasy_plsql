create or replace
procedure liberar_prescricao_rep_5 (
		nr_prescricao_p		number,
		cd_perfil_p		number,
		nr_seq_prescricao_p	number,
		ds_pasta_exp_xml_p out	varchar2,
		ds_nome_arquivo_p out	varchar2,
		ie_integrar_p 	  out	number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2) is 

nr_seq_sistema_integr_w	number(3);
ie_integrar_w		varchar2(1) := 'N';
ds_pasta_exp_xml_w	varchar2(255);
ds_nome_arquivo_w	varchar2(255);
ie_duplic_proced_lado_w varchar2(1) := 'N';
ie_gqa_pend_regra_w	varchar2(1);

cursor c01 is
select
    a.nr_atendimento,
	b.nr_prescricao,
	b.cd_material,
	b.cd_protocolo,
	b.nr_seq_protocolo,
	c.cd_classe_material,
	c.nr_seq_familia
from
	prescr_medica a,
	prescr_material b,
    estrutura_material_v c
where	a.nr_prescricao = nr_prescricao_p
and		b.nr_prescricao = a.nr_prescricao
and		c.cd_material = b.cd_material;

begin
if	(nr_prescricao_p is not null) and
	(cd_perfil_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	alterar_dt_entrega_prescr(
		nr_prescricao_p,
		cd_perfil_p,
		nm_usuario_p);

	ie_integrar_w := verifica_se_integra_sinapse(nr_prescricao_p);
	
	if	(ie_integrar_w = 'S') then
		begin
		select	MAX(nr_seq_empresa_integr)
		into	nr_seq_sistema_integr_w
		from	empresa_integr_dados
		where	(nr_seq_empresa_integr = 31 or nr_seq_empresa_integr = 41 or nr_seq_empresa_integr = 106 or nr_seq_empresa_integr = 232);
		
		if	(nr_seq_sistema_integr_w is not null) then
			begin
			select	max(a.ds_caminho_saida)
			into	ds_pasta_exp_xml_w
			from	empresa_integr_dados a,
				empresa_integracao b,
				sistema_integracao c
			where	a.nr_seq_empresa_integr	= b.nr_sequencia
			and	b.nr_sequencia		= c.nr_seq_empresa
			and	b.nr_sequencia		= nr_seq_sistema_integr_w;
			
			select max(a.nr_prescricao||'_'||TO_CHAR(b.dt_prescricao,'yyyymmdd')||'_'||TO_CHAR(b.dt_prescricao,'hhmiss')|| nr_seq_prescricao_p ||'.xml') ds
			into	ds_nome_arquivo_w
			from 	prescr_procedimento a,
				prescr_medica b
			where	a.nr_prescricao = nr_prescricao_p
			and	a.nr_prescricao = b.nr_prescricao;
			end;
		end if;
		end;
	end if;
	
	/*	
	obter_param_usuario(924, 734, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_duplic_proced_lado_w);
	
	if	(ie_duplic_proced_lado_w = 'S') then
		begin
		duplicar_lado_procedimento(nr_prescricao_p);
		end;
	end if;
	*/	
		select	nvl(max('S'), 'N')
		into	ie_gqa_pend_regra_w
		from	gqa_pendencia_regra a
		where	a.ie_evento = '7'
		and		a.ie_situacao = 'A';
		
		if (ie_gqa_pend_regra_w = 'S') then
			for r_c01_w in c01
			loop
				gqa_gerar_protocolo_assist(
					ie_evento_p			 => '7', -- Liberar prescricao
					nm_usuario_p		 => nm_usuario_p,
					nr_atendimento_p	 => r_c01_w.nr_atendimento,					
					nr_prescricao_p		 => r_c01_w.nr_prescricao,
					cd_material_p        => r_c01_w.cd_material,
					cd_protocolo_p       => r_c01_w.cd_protocolo,
					nr_seq_protocolo_p   => r_c01_w.nr_seq_protocolo,
					cd_classe_material_p => r_c01_w.cd_classe_material,
					nr_seq_familia_p     => r_c01_w.nr_seq_familia);
			end loop;
		end if;
	end;
end if;
commit;
ds_pasta_exp_xml_p	:= ds_pasta_exp_xml_w;
ds_nome_arquivo_p	:= ds_nome_arquivo_w;
ie_integrar_p		:= nr_seq_sistema_integr_w;
end liberar_prescricao_rep_5;
/
