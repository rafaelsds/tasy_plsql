create or replace
procedure desvincular_protocolo(
		nr_titulo_p	number) is 
begin
if	(nr_titulo_p is not null) then
	begin
	update 	titulo_receber
	set    	nr_seq_protocolo  = null,
		nr_documento = null
	where  	nr_titulo = nr_titulo_p;
	commit;
	end;
end if;
end desvincular_protocolo;
/