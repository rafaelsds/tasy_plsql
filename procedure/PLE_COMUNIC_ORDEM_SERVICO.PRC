create or replace
procedure	ple_comunic_ordem_servico(	nr_sequencia_p			number,
										ie_acao_p				varchar2,
										ie_executores_prev_p	varchar2,
										ie_resp_meta_p			varchar2,
										ds_observacao_p			varchar2,
										nr_seq_historico_p		number,
										nm_usuario_p			varchar2) is

nm_usuario_exec_w			varchar2(15);
ds_comunicado_w				varchar2(4000);
ds_titulo_w					varchar2(255);
nr_sequencia_w				number(10);
nr_seq_classif_w			number(10);
ds_dano_breve_w				varchar2(80);
ds_pessoa_solicitante_w		varchar2(100);
ds_status_w					varchar2(15);
ds_prioridade_w				varchar2(30);
ds_enter_w					varchar2(50);
nr_seq_meta_pe_w			number(10);
nm_resp_meta_w				varchar2(60);
nm_usuario_w				varchar2(4000);
ds_relat_tecnico_w			varchar2(4000);

cursor c01 is
select	nm_usuario_exec
from	man_ordem_servico_exec
where	nr_seq_ordem = nr_sequencia_p;

begin
ds_enter_w := (chr(13) || chr(10));
select	a.ds_dano_breve,
	substr(obter_nome_pf(a.cd_pessoa_solicitante),1,100),
	decode(a.ie_status_ordem,1,WHEB_MENSAGEM_PCK.get_texto(808177)/*'Aberta'*/,2,WHEB_MENSAGEM_PCK.get_texto(808178)/*'Processo'*/,3,WHEB_MENSAGEM_PCK.get_texto(808179)/*'Encerrado'*/),
	substr(obter_valor_dominio(1046,a.ie_prioridade),1,30),
	a.nr_seq_meta_pe
into	ds_dano_breve_w,
	ds_pessoa_solicitante_w,
	ds_status_w,
	ds_prioridade_w,
	nr_seq_meta_pe_w
from	man_ordem_servico a
where	a.nr_sequencia = nr_sequencia_p;

if	(ie_acao_p = 'A') then
	ds_titulo_w	:= WHEB_MENSAGEM_PCK.get_texto(818811); /*'Abertura da a��o no Planejamento Estrat�gico'*/
elsif	(ie_acao_p = 'E') then
	ds_titulo_w	:= WHEB_MENSAGEM_PCK.get_texto(818812); /*'Encerramento da a��o no Planejamento Estrat�gico'*/
end if;

if	(ie_acao_p = 'H') then
	begin
	ds_titulo_w	:= WHEB_MENSAGEM_PCK.get_texto(818813, 'NR_SEQUENCIA_P=' || nr_sequencia_p); /* 'Hist�rico da a��o n� #@NR_SEQUENCIA_P#@ no Planejamento Estrat�gico' */ 

	select	ds_relat_tecnico
	into	ds_relat_tecnico_w
	from	man_ordem_serv_tecnico
	where	nr_sequencia = nr_seq_historico_p;

	ds_comunicado_w	:= substr(ds_relat_tecnico_w,1,4000);
	end;
else
	ds_comunicado_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(818814, 'NR_SEQUENCIA_P=' || nr_sequencia_p || 
													';DS_DANO_BREVE_W=' || ds_dano_breve_w || 
													';DS_STATUS_W=' || ds_status_w || 
													':DS_OBSERVACAO_P=' || ds_observacao_p),1,4000);

end if;

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

if	(ie_executores_prev_p = 'S') then
	open c01;
	loop
	fetch c01 into
		nm_usuario_exec_w;
	exit when c01%notfound;
		begin	
		if	(nm_usuario_exec_w is not null) then
			nm_usuario_w	:= nm_usuario_exec_w || ', ' || nm_usuario_w;
		end if;
		end;
	end loop;
	close c01;
end if;

if	(ie_resp_meta_p = 'S') then
	begin
	select	substr(obter_usuario_pessoa(nvl(max(a.cd_pf_resp),'')),1,60)
	into	nm_resp_meta_w
	from	ple_meta a
	where	a.nr_sequencia = nr_seq_meta_pe_w;

	if	(nm_resp_meta_w is not null) then
		nm_usuario_w	:= nm_resp_meta_w || ', ' || nm_usuario_w;
	end if;
	end;
end if;

insert into comunic_interna(
	nr_sequencia,
	dt_comunicado,
	ds_titulo,
	ds_comunicado,
	nm_usuario,
	dt_atualizacao,
	ie_geral,
	nm_usuario_destino,
	ie_gerencial,
	nr_seq_classif,
	dt_liberacao)
values(comunic_interna_seq.nextval,
	sysdate,
	ds_titulo_w,
	ds_comunicado_w,
	nm_usuario_p,
	sysdate,
	'N',
	nm_usuario_w,
	'N',
	nr_seq_classif_w,
	sysdate);

commit;

end	ple_comunic_ordem_servico;
/