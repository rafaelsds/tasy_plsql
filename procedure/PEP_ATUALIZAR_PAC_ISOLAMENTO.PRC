create or replace
procedure pep_atualizar_pac_isolamento(
		nr_atendimento_p	number,
		nm_usuario_p		Varchar2) is 

begin
if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	update	atendimento_paciente
	set	ie_paciente_isolado = 'N',
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_atendimento = nr_atendimento_p;

	end;
end if;

commit;

end pep_atualizar_pac_isolamento;
/

