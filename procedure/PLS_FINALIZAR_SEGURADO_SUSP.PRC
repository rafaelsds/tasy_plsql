create or replace 
procedure	pls_finalizar_segurado_susp(	nr_sequencia_p	number,
					ds_motivo_p	varchar2,
					nm_usuario_p	varchar2) is

begin

if	(nvl(nr_sequencia_p,0) > 0) then

	update	pls_segurado_suspensao
	set	ds_motivo_fim_susp = ds_motivo_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
	
	pls_atualizar_susp_atend(nr_sequencia_p, null, 'D', null,'',nm_usuario_p,'S','N',null);

end if;

commit;

end pls_finalizar_segurado_susp;
/