create or replace
procedure pls_inserir_parecer_auditor
			(	nr_seq_item_p		number,
				nr_seq_motivo_lib_p	number,
				ds_parecer_p		varchar2,
				ie_tipo_historico_p	varchar2,
				ie_status_analise_p	varchar2,
				qt_ajuste_p		number,
				vl_item_p		varchar2,
				nm_usuario_p		Varchar2) is 
			
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Inserir o parecer do auditor, 
* Rotina utilizada no Portal web, na an�lise realizada pelo estipulante.
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/	

ds_observacao_w			pls_requisicao_historico.ds_historico%type;
ie_utiliza_nivel_w		pls_param_analise_aut.ie_utiliza_nivel%type;
nr_seq_grupo_w			pls_grupo_auditor.nr_sequencia%type;
vl_item_w			pls_auditoria_item.vl_item%type;
qt_ajuste_w			pls_auditoria_item.qt_ajuste%type;
qt_original_w			pls_auditoria_item.qt_original%type;
cd_procedimento_w		procedimento.cd_procedimento%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;
nr_seq_auditoria_w		pls_auditoria.nr_sequencia%type;    	
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
nr_seq_execucao_w		pls_execucao_requisicao.nr_sequencia%type;
ds_parecer_alterado_w		varchar2(4000);	
ds_item_w			varchar2(255);
cd_nr_material_w		number(10);
ie_acao_w			varchar2(1);
ie_alterou_qtde_w		varchar2(1)	:= 'N';
ie_alterou_valor_w		varchar2(1)	:= 'N';
vl_item_param_w       		pls_auditoria_item.vl_item%type;
nr_seq_motivo_glosa_w		tiss_motivo_glosa.nr_sequencia%type;
nr_seq_mat_origem_w		pls_auditoria_item.nr_seq_mat_origem%type;				
nr_seq_proc_origem_w		pls_auditoria_item.nr_seq_proc_origem%type;
qt_glosa_w			number(10)	:= 0;
ie_glosar_item_guia_w		pls_membro_grupo_aud.ie_glosar_item_guia%type;
ds_separador_w			varchar2(10) := chr(10) || chr(10);

begin
select	b.nr_seq_segurado,
	b.nr_seq_requisicao,
	b.nr_seq_guia,
	b.nr_seq_execucao,
	b.nr_sequencia,
	a.cd_procedimento,
	nvl(substr(pls_obter_seq_codigo_material(nr_seq_material, ''), 1, 10), a.nr_seq_material),
	b.cd_estabelecimento,
	a.qt_ajuste,
	substr(nvl(Obter_Descricao_Procedimento(cd_procedimento,ie_origem_proced),pls_obter_desc_material(nr_seq_material)),1,255),
	a.qt_original,
	a.vl_item,	
	a.nr_seq_mat_origem,
	a.nr_seq_proc_origem
into	nr_seq_segurado_w,
	nr_seq_requisicao_w,
	nr_seq_guia_w,
	nr_seq_execucao_w,
	nr_seq_auditoria_w,
	cd_procedimento_w,
	cd_nr_material_w,
	cd_estabelecimento_w,
	qt_ajuste_w,
	ds_item_w,
	qt_original_w,
	vl_item_w,
	nr_seq_mat_origem_w,
	nr_seq_proc_origem_w
from	pls_auditoria		b,
	pls_auditoria_item 	a
where	a.nr_seq_auditoria 	= b.nr_sequencia
and	a.nr_sequencia 		= nr_seq_item_p;

if	(nvl(nr_seq_execucao_w, 0) > 0) then
	select	nr_seq_requisicao
	into	nr_seq_requisicao_w
	from	pls_execucao_requisicao
	where	nr_sequencia = nr_seq_execucao_w;
end if;

select	nr_seq_grupo
into	nr_seq_grupo_w
from	pls_auditoria_grupo
where	nr_sequencia	= pls_obter_grupo_analise_atual(nr_seq_auditoria_w);

pls_gerar_analise_macro(substr(ds_parecer_p, 1, 4000), nr_seq_item_p, nm_usuario_p, ds_parecer_alterado_w);

if	(qt_ajuste_w <> nvl(qt_ajuste_p, qt_ajuste_w)) then
	ds_observacao_w	:= substr('O auditor ' || nm_usuario_p ||' alterou a quantidade autorizada do item ' || nvl(cd_procedimento_w,cd_nr_material_w) || ' - ' || 
					ds_item_w || ' de ' || qt_ajuste_w || ' para ' || qt_ajuste_p || ' ' || ds_parecer_alterado_w,1,4000);
					
	ie_alterou_qtde_w	:= 'S';
end if;

begin
	vl_item_param_w := to_number(vl_item_p);
exception
when others then
	vl_item_param_w := to_number(replace(vl_item_p, ',', '.'));
end;

if	(vl_item_w <> nvl(vl_item_param_w, vl_item_w)) then

	if (ie_alterou_qtde_w = 'S') then
		ds_observacao_w	:= ds_observacao_w || '<br><br>';
	end if;
	
	ds_observacao_w	:= ds_observacao_w || substr('O auditor ' || nm_usuario_p ||' alterou o valor do item ' || nvl(cd_procedimento_w,cd_nr_material_w) || ' - ' || 
					ds_item_w || ' de ' || vl_item_w || ' para ' || vl_item_param_w || ' ' || ds_parecer_alterado_w,1,4000);
	
	ie_alterou_valor_w	:= 'S';
end if;

if	(ie_alterou_qtde_w = 'N') and (ie_alterou_valor_w = 'N') then
	ds_observacao_w	:= nvl(cd_procedimento_w, cd_nr_material_w) || ' - ' || substr(ds_parecer_alterado_w, 1, 3980);
end if;	

if	(nvl(nr_seq_requisicao_w, 0) > 0) or
	(nvl(nr_seq_execucao_w, 0) > 0) then
	insert into pls_requisicao_historico
		(nr_sequencia,
		nr_seq_requisicao,
		ie_tipo_historico,
		ds_historico,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_segurado,
		dt_historico,
		ie_origem_historico,
		nr_seq_item,
		ie_status_analise,
		nr_seq_grupo,
		nr_seq_motivo_lib,
		nr_seq_execucao)
	values	(pls_requisicao_historico_seq.nextval,
		nr_seq_requisicao_w,
		ie_tipo_historico_p,
		ds_observacao_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_segurado_w,
		sysdate,
		'M',
		nr_seq_item_p,
		ie_status_analise_p,
		nr_seq_grupo_w,
		decode(nr_seq_motivo_lib_p,0,null,nr_seq_motivo_lib_p),
		nr_seq_execucao_w);
elsif	(nvl(nr_seq_guia_w, 0) > 0) then
	insert into pls_guia_plano_historico 
		(nr_sequencia,
		nr_seq_guia,
		ie_tipo_log,
		dt_historico,
		dt_atualizacao,
		nm_usuario,
		ds_observacao,
		ie_origem_historico,
		ie_tipo_historico,
		nr_seq_item,
		ie_status_analise,
		nr_seq_grupo,
		nr_seq_motivo_lib)
	values	(pls_guia_plano_historico_seq.nextval,
		nr_seq_guia_w,
		null,
		sysdate,
		sysdate,
		nm_usuario_p,
		ds_observacao_w,
		'M',
		ie_tipo_historico_p,
		nr_seq_item_p,
		ie_status_analise_p,
		nr_seq_grupo_w,
		decode(nr_seq_motivo_lib_p,0,null,nr_seq_motivo_lib_p));
end if;

begin
select	nvl(a.ie_glosar_item_guia,'N')
into	ie_glosar_item_guia_w
from	pls_membro_grupo_aud a,
	pls_auditoria_grupo b
where	a.nm_usuario_exec	= nm_usuario_p
and	a.nr_seq_grupo		= b.nr_seq_grupo
and	b.nr_sequencia		= pls_obter_grupo_analise_atual(nr_seq_auditoria_w);
exception
when others then
	ie_glosar_item_guia_w	:= null;
end;

if	(ie_glosar_item_guia_w = 'N') and (ie_status_analise_p = 'N') then
	select	max(nr_seq_motivo_glosa)
	into	nr_seq_motivo_glosa_w
	from	pls_motivo_lib_analise_aut
	where	nr_sequencia	= nr_seq_motivo_lib_p;
		
	if 	(nr_seq_motivo_glosa_w is not null) then
		if 	(nr_seq_guia_w is not null) then		
			select	sum(qt_glosa)
			into	qt_glosa_w
			from	(	select	count(1) qt_glosa
					from	pls_guia_glosa	a
					where	nr_seq_guia_proc 	= nr_seq_proc_origem_w
					and	nr_seq_guia		is null
					and	nr_seq_guia_mat 	is null
					and	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w
					union	all			
					select	count(1) qt_glosa
					from	pls_guia_glosa	a
					where	nr_seq_guia_mat		= nr_seq_mat_origem_w
					and	nr_seq_guia		is null
					and	nr_seq_guia_proc 	is null
					and	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w);		
		elsif	(nr_seq_execucao_w is not null) then
			nr_seq_requisicao_w	:= null; 
			
			select	sum(qt_glosa)
			into	qt_glosa_w
			from	(	select	count(1) qt_glosa
					from	pls_requisicao_glosa
					where	nr_seq_exec_proc	= nr_seq_proc_origem_w					
					and	nr_seq_exec_mat		is null
					and	nr_seq_execucao		= nr_seq_execucao_w
					and	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w
					union	all
					select	count(1) qt_glosa
					from	pls_requisicao_glosa
					where	nr_seq_exec_mat		= nr_seq_mat_origem_w					
					and	nr_seq_exec_proc	is null
					and	nr_seq_execucao		= nr_seq_execucao_w
					and	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w);
		elsif	(nr_seq_requisicao_w is not null) then			
			select	sum(qt_glosa)
			into	qt_glosa_w
			from	(	select	count(1) qt_glosa
					from	pls_requisicao_glosa
					where	nr_seq_req_proc		= nr_seq_proc_origem_w
					and	nr_seq_requisicao 	is null
					and	nr_seq_req_mat		is null
					and	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w
					union	all
					select	count(1) qt_glosa
					from	pls_requisicao_glosa
					where	nr_seq_req_mat		= nr_seq_mat_origem_w
					and	nr_seq_requisicao 	is null
					and	nr_seq_req_proc		is null
					and	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w);
		end if;		
		
		if	(qt_glosa_w = 0) then			
			pls_inserir_motivo_glosa_aud(	nr_seq_guia_w, nr_seq_requisicao_w, nr_seq_execucao_w,
							nr_seq_proc_origem_w, nr_seq_mat_origem_w, nr_seq_proc_origem_w,
							nr_seq_mat_origem_w, nr_seq_proc_origem_w, nr_seq_mat_origem_w,
							nr_seq_motivo_glosa_w, nm_usuario_p); 
		end if;	
	end if;
end if;

ie_utiliza_nivel_w	:= pls_obter_se_uti_nivel_lib_aut(cd_estabelecimento_w); 

if	(ie_utiliza_nivel_w = 'S') then
	select	decode(ie_status_analise_p, 'A', 'L', ie_status_analise_p)
	into	ie_acao_w
	from 	dual;
	
	pls_atualizar_status_item_aud(	nr_seq_item_p,
					nr_seq_grupo_w,
					ie_acao_w,
					'I',
					nm_usuario_p);
else 
	update	pls_auditoria_item
	set	ie_status	= ie_status_analise_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		qt_ajuste	= decode(qt_ajuste_p, null, qt_ajuste, qt_ajuste_p),
		vl_item		= decode(vl_item_param_w, null, vl_item, vl_item_param_w)
	where	nr_sequencia	= nr_seq_item_p;
end if;

update	pls_auditoria_item
set	qt_ajuste	= decode(qt_ajuste_p, null, qt_ajuste, qt_ajuste_p),
	vl_item		= decode(vl_item_param_w, null, vl_item, vl_item_param_w)
where	nr_sequencia	= nr_seq_item_p;

commit;

end pls_inserir_parecer_auditor; 
/
