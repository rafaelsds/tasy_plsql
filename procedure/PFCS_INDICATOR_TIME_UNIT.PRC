create or replace procedure pfcs_indicator_time_unit(	nr_seq_indicator_p			number,
														cd_establishment_p			number,
														cd_department_p				varchar2,
														ds_first_time_p				out nocopy varchar2,
														ds_second_time_p			out nocopy varchar2,
														ds_first_unit_p				out nocopy varchar2,
														ds_second_unit_p			out nocopy varchar2,
														ds_color_p                  out nocopy varchar2) is

	qt_indicator_time_w				number(15) := 0;
begin
	if (nr_seq_indicator_p = 41) then
		qt_indicator_time_w := pfcs_telemetry_config_pck.get_average_waiting_time(cd_establishment_p, cd_department_p);
        ds_color_p := Pfcs_get_indicator_rule(nr_seq_indicator_p, qt_indicator_time_w, cd_establishment_p, 'COLOR');
	end if;

	get_time_units(	qt_indicator_time_w,
					ds_first_time_p,
					ds_second_time_p,
					ds_first_unit_p,
					ds_second_unit_p);

end pfcs_indicator_time_unit;
/
