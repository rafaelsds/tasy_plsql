create or replace
PROCEDURE gerar_quimio_melhor_apresent(		nr_seq_atendimento_p	NUMBER,
											cd_estabelecimento_p	NUMBER,
											nr_atendimento_p		NUMBER,
											nr_seq_ordem_p			NUMBER,
											nm_usuario_p			VARCHAR2) IS
/* vetor */
TYPE colunas IS RECORD (cd_material_w	NUMBER(6,0),
			cd_unidade_medida_w 		VARCHAR2(30),
			qt_dose_w					NUMBER(18,6),
			nr_seq_fabric_w				NUMBER(10,0),
			qt_frasco_disp_w			NUMBER(18,6),
			qt_perda_fornec_w			NUMBER(18,6));
TYPE vetor IS TABLE OF colunas INDEX BY BINARY_INTEGER;

cd_unid_medida_disp_w		varchar2(30);
qt_dose_prescricao_ww		NUMBER(18,6);
qt_sobra_w			NUMBER(18,6);
cont_mat_menor_disper_w		NUMBER(10);
cd_mat_menor_disp_w		number(10);
qt_frasco_w			NUMBER(10);
qt_frasco_tot_w			NUMBER(10) := 0;
cont_ww				NUMBER(10);
vetor_ww			vetor;
vetor_w				vetor;
vetor_aux_w			vetor;
cd_convenio_w			NUMBER(10);
vetor_fornecedor_w		vetor;
dt_atualizacao_w    	   	date := sysdate;
dt_referencia_w			date;
nr_prescricao_w			NUMBER(10);
nr_prescricao_ww		NUMBER(10);
nr_prescricao_www		NUMBER(10);
cd_material_w              	NUMBER(6,0);
cd_material_regra_w		NUMBER(6,0);
ie_gerar_solucao_w		VARCHAR2(3);
cd_unid_med_prescr_w       	VARCHAR2(30);
qt_dose_prescricao_w       	NUMBER(18,6);
qt_unitaria_w   		NUMBER(18,6);
qt_total_dispensar_w		NUMBER(18,6);
qt_conversao_dose_w		NUMBER(18,6);
ie_via_aplicacao_w         	VARCHAR2(5);
ie_gerar_diluicao_w		VARCHAR2(5);
ie_define_disp_w		VARCHAR2(5);
nr_seq_material_w		NUMBER(10,0);
nr_seq_diluicao_w		NUMBER(10,0);
nr_seq_material_rec_w		NUMBER(10,0);
nr_seq_fabric_w			NUMBER(10,0);
nr_agrupamento_ant_w		NUMBER(07,1) := -1;
cd_unid_med_consumo_w		VARCHAR2(30);
cd_intervalo_w             	VARCHAR2(7);
qt_minimo_multiplo_solic_w	NUMBER(18,6);
ie_origem_inf_w			VARCHAR2(1);
nr_ocorrencia_w			NUMBER(18,6);
nr_ocorrencia_ww		NUMBER(18,6);
ie_gerar_horarios   		VARCHAR2(1);
ds_horarios_w			VARCHAR2(2000);
ds_horarios2_w			VARCHAR2(2000);
cd_protocolo_w			NUMBER(10);
nr_seq_medicacao_w		NUMBER(6);
nr_seq_dieta_w			NUMBER(6);
nr_seq_dieta_novo_w		NUMBER(6);
nr_seq_dieta_prot_w		NUMBER(6);
cd_dieta_w			NUMBER(10);
ie_destino_dieta_w		VARCHAR2(2);
ie_refeicao_w			VARCHAR2(3);
ds_horarios_dieta_w		VARCHAR2(2000);
dt_primeiro_horario_w		date;
dt_prescricao_w			date;
dt_inicio_prescr_w		date;
dt_prescricao_ww		date;
dt_entrada_w		date;
cd_pessoa_fisica_ww	VARCHAR2(10);
cd_pessoa_usuario_w	VARCHAR2(10);
nr_sequencia_w		NUMBER(6);
qt_material_w		NUMBER(18,6);
ds_dose_diferenciada_w	VARCHAR2(50);
cd_unidade_medida_dose_w	VARCHAR2(30);
ds_erro_w		VARCHAR2(255);
ie_regra_disp_w		VARCHAR2(1);
cd_fornec_consignado_w	VARCHAR2(14);
ie_consignado_w		VARCHAR2(1);
cd_fornecedor_w		VARCHAR2(14);
qt_estoque_consig_w	VARCHAR2(14);
ie_tipo_material_w		VARCHAR2(14);
nr_seq_ficha_tecnica_w	NUMBER(15,0);
qt_dose_prescr_aux_w	NUMBER(18,6);
qt_dose_total_prescr_w	NUMBER(18,6);
qt_dose_regra_w		NUMBER(18,6);
qt_dose_total_aux_w	NUMBER(18,6);
qt_dose_total_aux_ww	NUMBER(18,6);
qt_dose_total_aux_www	NUMBER(18,6);
cont_w			NUMBER(10,0);
qt_registros_vetor_w	NUMBER(10,0);
qt_controle_w		NUMBER(15,0) := 0;
qt_controle_ww		NUMBER(15,0) := 0;
qt_controle_www		NUMBER(15,0) := 0;
qt_controle_wwww		NUMBER(15,0) := 0;
qt_perda_w		NUMBER(18,6);
qt_perda_2w		NUMBER(18,6);
qt_desconto_w		NUMBER(18,6);
qt_a_lancar_w		NUMBER(18,6);
qt_perda_final_w		NUMBER(18,6);
qt_perda_final_2w		NUMBER(18,6);
pr_desconto_w		NUMBER(18,6);
qt_mat_vetor_w		NUMBER(18,6);
x			NUMBER(15,0) := 0;
i			NUMBER(15,0) := 0;
k			NUMBER(15,0) := 0;
y			NUMBER(15,0) := 0;
r			NUMBER(15,0) := 0;
cd_mat_vetor_w		NUMBER(15,0);
ie_reg_w			NUMBER(15,0);
nr_seq_medic_w		NUMBER(15,0);
nr_seq_proc_w		NUMBER(15,0);
cd_unidade_medida_consumo_w	VARCHAR2(30);
ds_prim_horario_w		VARCHAR2(5);
ie_fabricante_w		VARCHAR2(5);
nr_seq_fabric_melhor_w	NUMBER(10,0);
qt_fornec_w		NUMBER(10,0);
qt_frasco_disp_w		NUMBER(10,0);
ie_agrupamento_w	NUMBER(10);
ie_agrupador_oncologia_w	NUMBER(2);
nr_atendimento_www	NUMBER(10);
qt_dose_w		NUMBER(18,6);
cd_grupo_material_w	NUMBER(3);
cd_subgrupo_material_w	NUMBER(3);
nr_seq_prescricao_w	NUMBER(6);
qt_sobra_overfill_w	NUMBER(10,0);
ie_arred_dose_w		VARCHAR2(1);

CURSOR c01 IS
	SELECT	a.cd_material,
		a.cd_unidade_medida,
		a.qt_dose,
		d.ie_via_aplicacao,
		substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
		b.qt_minimo_multiplo_solic,
		a.nr_sequencia_diluente,
		b.ie_tipo_material,
		a.qt_dose,
		b.nr_seq_ficha_tecnica,
		a.ie_agrupador,
		a.nr_prescricao,
		a.nr_seq_prescricao
	FROM   	material b,
		can_ordem_prod d,
		can_ordem_item_prescr a
	WHERE  	a.cd_material 		  = b.cd_material
	AND    	a.nr_seq_ordem 		  = d.nr_sequencia
	AND    	a.nr_seq_ordem 		  = nr_seq_ordem_p;

CURSOR c02 IS
SELECT		b.cd_material,
		obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_prescr_w) qt_dose,
		substr(obter_dados_material_estab(b.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo
FROM		material_estab c,
		material b,
		medic_ficha_tecnica a
WHERE		b.nr_seq_ficha_tecnica	= a.nr_sequencia
AND		c.cd_material		= b.cd_material
AND		c.cd_estabelecimento	= cd_estabelecimento_p
AND		a.nr_sequencia		= nr_seq_ficha_tecnica_w
AND		Obter_se_via_adm(b.cd_material, ie_via_aplicacao_w) = 'S'
AND		NVL(obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_prescr_w),0) > 0
AND		b.ie_situacao		= 'A'
AND		c.ie_prescricao		= 'S'
and 		b.ie_tipo_material 	<> '6'
ORDER BY qt_dose desc;

BEGIN

delete from dispensacao_quimioterapia where nr_seq_ordem = nr_seq_ordem_p;

select	count(*)
into	qt_sobra_overfill_w
from	can_ordem_prod_mat
where 	nr_seq_ordem	=	nr_seq_ordem_p
and		ie_sobra_overfill in ('S','O');

Obter_param_Usuario(3130, 486, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_arred_dose_w);

OPEN c01;
LOOP
FETCH c01 INTO
	cd_material_w,
	cd_unid_med_prescr_w,
	qt_dose_prescricao_w,
	ie_via_aplicacao_w,
	cd_unid_med_consumo_w,
	qt_minimo_multiplo_solic_w,
	nr_seq_diluicao_w,
	ie_tipo_material_w,
	qt_dose_total_prescr_w,
	nr_seq_ficha_tecnica_w,
	ie_agrupador_oncologia_w,
	nr_prescricao_www,
	nr_seq_prescricao_w;
EXIT WHEN c01%NOTFOUND;
	BEGIN
	ie_agrupamento_w	:= 1;

	select	nvl(max(pr_desconto),0),
			nvl(max(ie_fabricante),'N')
	into	pr_desconto_w,
			ie_fabricante_w
	from	material_sem_apresentacao
	where	cd_material	= cd_material_w;

	select	max(cd_grupo_material),
		max(cd_subgrupo_material)
	into	cd_grupo_material_w,
		cd_subgrupo_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_w;

	SELECT	max(b.cd_pessoa_fisica)
	INTO	cd_pessoa_fisica_ww
	FROM	paciente_setor b,
		paciente_atendimento a
	WHERE	a.nr_seq_atendimento = nr_seq_atendimento_p
	AND	a.nr_seq_paciente = b.nr_seq_paciente;

	select 	max(nr_Atendimento)
	into	nr_atendimento_www
	from 	atendimento_paciente
	where 	cd_pessoa_fisica = cd_pessoa_fisica_ww
	and 	dt_alta is Null;

	select 	obter_convenio_atendimento(nr_atendimento_p)
	into 	cd_convenio_w
	from 	dual;


	if	(nr_seq_diluicao_w	is not null) then
		ie_agrupamento_w	:= 3;
	elsif	(ie_gerar_solucao_w	= 'S') then
		ie_agrupamento_w	:= 4;
	end if;

	if	(ie_agrupador_oncologia_w = 9) then
		ie_agrupamento_w	:= 9;
	end if;

	if	(ie_tipo_material_w = '6') Then
		ie_agrupamento_w	:= 1;
	end if;

	SELECT	NVL(COUNT(*),0)
	INTO	cont_w
	FROM	material b
	WHERE	b.nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w
	and	b.ie_tipo_material <> 6;

	IF	(cont_w		> 0) THEN

			cont_w	:= 0;
			vetor_w.DELETE;
			OPEN c02;
			LOOP
			FETCH c02 INTO
				cd_material_regra_w,
				qt_dose_regra_w,
				cd_unidade_medida_consumo_w;
			EXIT WHEN c02%NOTFOUND;
				vetor_w(cont_w).cd_material_w	:= cd_material_regra_w;
				vetor_w(cont_w).qt_dose_w	:= qt_dose_regra_w;
				vetor_w(cont_w).cd_unidade_medida_w := cd_unidade_medida_consumo_w;
				cont_w := cont_w + 1;
			END LOOP;
			CLOSE c02;

			qt_dose_total_aux_w	:= qt_dose_total_prescr_w;
			qt_desconto_w		:= ((pr_desconto_w * qt_dose_total_aux_w) / 100);
			ie_reg_w		:= 0;

			WHILE	(qt_dose_total_aux_w	> 0) AND
				(qt_controle_w		< 1000) LOOP

				qt_registros_vetor_w	:= vetor_w.COUNT - 1;
				qt_controle_ww		:= 0;
				x			:= -1;

				WHILE	(x < qt_registros_vetor_w) AND
					(qt_dose_total_aux_w	> 0) AND
					(qt_controle_ww		< 1000) LOOP

					x := x + 1;
					qt_controle_ww	:= qt_controle_ww + 1;

					IF	((qt_dose_total_aux_w - vetor_w(x).qt_dose_w) >= 0) THEN
						qt_dose_total_aux_www := qt_dose_total_aux_w;
						qt_dose_total_aux_w	:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
						vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(x).cd_material_w;
						vetor_aux_w(ie_reg_w).qt_dose_w		:= vetor_w(x).qt_dose_w;
						vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(x).cd_unidade_medida_w;
						r := x + 1;
						
						/*if 	(r < cont_w) and
							(qt_dose_total_aux_www - (vetor_w(x).qt_dose_w + vetor_w(x).qt_dose_w) < 0) and
							((qt_dose_total_aux_www - vetor_w(r).qt_dose_w) > 0) then
								qt_dose_total_aux_w	:= (qt_dose_total_aux_www - vetor_w(r).qt_dose_w);
								vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(r).cd_material_w;
								vetor_aux_w(ie_reg_w).qt_dose_w		:= vetor_w(r).qt_dose_w;
								vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(r).cd_unidade_medida_w;
							
						end if;*/
						x := -1;
						ie_reg_w	:= ie_reg_w + 1;
					ELSIF	(qt_dose_total_aux_w <= qt_desconto_w) THEN
						qt_dose_total_aux_w	:= 0;
					ELSIF	(qt_registros_vetor_w = x) THEN

						qt_dose_total_aux_w	:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
						vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(x).cd_material_w;
						vetor_aux_w(ie_reg_w).qt_dose_w	:= vetor_w(x).qt_dose_w;
						vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(x).cd_unidade_medida_w;
						ie_reg_w	:= ie_reg_w + 1;

					ELSIF	((qt_dose_total_aux_w - vetor_w(x).qt_dose_w) < 0) THEN
						qt_perda_w		:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
						qt_dose_total_aux_ww	:= qt_dose_total_aux_w;
						qt_perda_final_w	:= 0;
						qt_controle_wwww	:= 0;
						WHILE	(qt_dose_total_aux_ww	> 0) AND
							(qt_controle_wwww	< 1000) LOOP

							qt_controle_www := 0;
							i 		:= -1;
							WHILE	(i < qt_registros_vetor_w) AND
								(qt_dose_total_aux_ww	> 0) AND
								(qt_controle_www	< 1000) LOOP

								i	:= i + 1;
								qt_controle_www	:= qt_controle_www + 1;
								IF	((qt_dose_total_aux_ww - vetor_w(i).qt_dose_w) >= 0) THEN
									qt_perda_final_w	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
									qt_dose_total_aux_ww	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
									i := -1;
								ELSIF	(qt_registros_vetor_w = i) THEN
									qt_perda_final_w	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
									qt_dose_total_aux_ww	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
								END IF;
							END LOOP;

							qt_controle_wwww := qt_controle_wwww + 1;

						END LOOP;

						qt_perda_2w		:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
						qt_dose_total_aux_ww	:= qt_dose_total_aux_w;
						qt_perda_final_2w	:= 0;
						qt_controle_wwww	:= 0;
						WHILE	(qt_dose_total_aux_ww	> 0) AND
							(qt_controle_wwww	< 1000) LOOP

							qt_controle_www := 0;
							i 		:= -1;
							WHILE	(i < qt_registros_vetor_w) AND
								(qt_dose_total_aux_ww	> 0) AND
								(qt_controle_www	< 1000) LOOP

								i	:= i + 1;
								qt_controle_www	:= qt_controle_www + 1;
								IF	((qt_dose_total_aux_ww - vetor_w(i).qt_dose_w) >= 0) THEN
									qt_perda_final_2w	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
									qt_dose_total_aux_ww	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
									i := -1;
								END IF;
							END LOOP;
							qt_controle_wwww := qt_controle_wwww + 1;
						END LOOP;

						IF	(qt_dose_total_aux_ww < qt_desconto_w) THEN
							NULL;
						ELSIF	(qt_perda_w >= qt_perda_final_w) THEN
							vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(x).cd_material_w;
							vetor_aux_w(ie_reg_w).qt_dose_w		:= vetor_w(x).qt_dose_w;
							vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(x).cd_unidade_medida_w;
							ie_reg_w		:= ie_reg_w + 1;
							qt_dose_total_aux_w	:= 0;
						END IF;
					END IF;
				END LOOP;

				qt_controle_w	:= qt_controle_w + 1;
			END LOOP;

			k		:= -1;
			cd_mat_vetor_w	:= 0;
			qt_mat_vetor_w	:= 0;
			qt_a_lancar_w	:= qt_dose_total_prescr_w;
			ie_gerar_diluicao_w := 'S';

			WHILE	(k < vetor_aux_w.COUNT - 1) LOOP
				k	:= k + 1;

				IF	((cd_mat_vetor_w = vetor_aux_w(k).cd_material_w) OR
					 (cd_mat_vetor_w = 0)) AND
					(vetor_aux_w.COUNT > 1) THEN
					qt_mat_vetor_w			:= qt_mat_vetor_w + vetor_aux_w(k).qt_dose_w;
					qt_unitaria_w			:= vetor_aux_w(k).qt_dose_w;
					cd_mat_vetor_w			:= vetor_aux_w(k).cd_material_w;
					qt_conversao_dose_w		:= vetor_aux_w(k).qt_dose_w;
					cd_unidade_medida_consumo_w 	:= vetor_aux_w(k).cd_unidade_medida_w;
				ELSE
					qt_a_lancar_w	:= (qt_a_lancar_w - qt_mat_vetor_w);

					IF	(qt_a_lancar_w >= 0) THEN
						qt_total_dispensar_w := qt_a_lancar_w;
					ELSE
						qt_mat_vetor_w := (qt_mat_vetor_w - ABS(qt_a_lancar_w));
					END IF;

					qt_unitaria_w	:= dividir(qt_mat_vetor_w, qt_unitaria_w);

					SELECT	NVL(MAX(nr_sequencia),0) +1
					INTO	nr_seq_medic_w
					FROM	prescr_material
					WHERE	nr_prescricao	= nr_prescricao_w;

					SELECT	NVL(MAX(ie_consignado), 'X')
					INTO	ie_consignado_w
					FROM  	material
					WHERE  	cd_material = cd_mat_vetor_w;

					IF	(ie_consignado_w = '1') THEN
						SELECT  NVL(MAX(a.cd_fornecedor),'X')
						INTO	cd_fornecedor_w
						FROM  	fornecedor_mat_consignado a,
							material m
						WHERE 	a.cd_material	= m.cd_material_estoque
						AND   	m.cd_material   = cd_mat_vetor_w
						AND   	m.ie_situacao 	= 'A'
						AND	a.dt_mesano_referencia = TRUNC(dt_prescricao_w,'mm')
						AND qt_estoque = (	SELECT MAX(obter_saldo_estoque_consig(cd_estabelecimento_p, a.cd_fornecedor, a.cd_material, a.cd_local_estoque))
									FROM  	fornecedor_mat_consignado a,
										material m
									WHERE 	a.cd_material	= m.cd_material_estoque
									AND   	m.cd_material   = cd_mat_vetor_w
									AND   	m.ie_situacao 	= 'A'
									AND		a.dt_mesano_referencia = TRUNC(dt_prescricao_w,'mm'));

						IF	(cd_fornecedor_w = 'X') THEN
							cd_fornecedor_w	:= NULL;
						END IF;
					END IF;

					IF	(NVL(cd_mat_vetor_w,0) = '0') THEN
						cd_mat_vetor_w	:= vetor_aux_w(k).cd_material_w;
					END IF;

					IF	(qt_mat_vetor_w > 0) THEN
						IF	(ie_arred_dose_w = 'S') THEN
						Insert 	into dispensacao_quimioterapia(
							nr_sequencia,
							cd_material,
							cd_unidade_medida_dose,
							dt_atualizacao,
							dt_atualizacao_nrec,
							nm_usuario,
							nm_usuario_nrec,
							qt_dose,
							cd_unidade_conta,
							qt_material_conta,
							nr_seq_ordem,
							nr_prescricao,
							nr_seq_prescricao,
							cd_material_exec,
							ie_via_aplicacao)
						select 	dispensacao_quimioterapia_seq.nextval,
							cd_mat_vetor_w,
							cd_unid_med_prescr_w,
							sysdate,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							qt_mat_vetor_w,
							substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30),
							ceil(obter_dose_convertida(cd_mat_vetor_w,qt_mat_vetor_w,cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30))),
							nr_seq_ordem_p,
							nr_prescricao_www,
							nr_seq_prescricao_w,
							cd_mat_vetor_w,
							ie_via_aplicacao_w
						from 	dual;
						ELSE
						Insert 	into dispensacao_quimioterapia(
							nr_sequencia,
							cd_material,
							cd_unidade_medida_dose,
							dt_atualizacao,
							dt_atualizacao_nrec,
							nm_usuario,
							nm_usuario_nrec,
							qt_dose,
							cd_unidade_conta,
							qt_material_conta,
							nr_seq_ordem,
							nr_prescricao,
							nr_seq_prescricao,
							cd_material_exec,
							ie_via_aplicacao)
						select 	dispensacao_quimioterapia_seq.nextval,
							cd_mat_vetor_w,
							cd_unid_med_prescr_w,
							sysdate,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							qt_mat_vetor_w,
							substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30),
							obter_dose_convertida(cd_mat_vetor_w,qt_mat_vetor_w,cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30)),
							nr_seq_ordem_p,
							nr_prescricao_www,
							nr_seq_prescricao_w,
							cd_mat_vetor_w,
							ie_via_aplicacao_w
						from 	dual;
						END IF;

					commit;
					END IF;

					ie_gerar_diluicao_w		:= 'N';
					cd_mat_vetor_w			:= vetor_aux_w(k).cd_material_w;
					qt_mat_vetor_w			:= vetor_aux_w(k).qt_dose_w;
		 			qt_unitaria_w			:= vetor_aux_w(k).qt_dose_w;
					qt_conversao_dose_w		:= vetor_aux_w(k).qt_dose_w;
					cd_unidade_medida_consumo_w 	:= vetor_aux_w(k).cd_unidade_medida_w;
					COMMIT;
				END IF;
			END LOOP;

			vetor_aux_w.DELETE;

			IF	(NVL(cd_mat_vetor_w,0) <> '0') THEN

				qt_a_lancar_w	:= (qt_a_lancar_w - qt_mat_vetor_w);
				IF	(qt_a_lancar_w >= 0) THEN
					qt_total_dispensar_w := qt_a_lancar_w;
				ELSE
					qt_mat_vetor_w := (qt_mat_vetor_w - ABS(qt_a_lancar_w));
				END IF;

				qt_unitaria_w	:= dividir(qt_mat_vetor_w, qt_unitaria_w);

				SELECT	NVL(MAX(nr_sequencia),0) +1
				INTO	nr_seq_medic_w
				FROM	prescr_material
				WHERE	nr_prescricao	= nr_prescricao_w;

				SELECT	NVL(MAX(ie_consignado), 'X'),
					MAX(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30))
				INTO	ie_consignado_w,
					cd_unidade_medida_consumo_w
				FROM  	material
				WHERE  	cd_material = cd_mat_vetor_w;

				IF	(ie_consignado_w = '1') THEN
					SELECT  NVL(MAX(a.cd_fornecedor),'X')
					INTO	cd_fornecedor_w
					FROM  	fornecedor_mat_consignado a,
						material m
					WHERE 	a.cd_material	= m.cd_material_estoque
					AND   	m.cd_material   = cd_mat_vetor_w
					AND   	m.ie_situacao 	= 'A'
					AND	a.dt_mesano_referencia = TRUNC(dt_prescricao_w,'mm')
					AND	qt_estoque = (	SELECT MAX(obter_saldo_estoque_consig(cd_estabelecimento_p, a.cd_fornecedor, a.cd_material, a.cd_local_estoque))
								FROM  	fornecedor_mat_consignado a,
									material m
								WHERE 	a.cd_material	= m.cd_material_estoque
								AND   	m.cd_material   = cd_mat_vetor_w
								AND   	m.ie_situacao 	= 'A'
								AND		a.dt_mesano_referencia = TRUNC(dt_prescricao_w,'mm'));

					IF	(cd_fornecedor_w = 'X') THEN
						cd_fornecedor_w	:= NULL;
					END IF;
				END IF;

					IF	(ie_arred_dose_w = 'S') THEN
					Insert 	into dispensacao_quimioterapia(
						nr_sequencia,
						cd_material,
						cd_unidade_medida_dose,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario,
						nm_usuario_nrec,
						qt_dose,
						cd_unidade_conta,
						qt_material_conta,
						nr_seq_ordem,
						nr_prescricao,
						nr_seq_prescricao,
						cd_material_exec,
						ie_via_aplicacao)
					select 	dispensacao_quimioterapia_seq.nextval,
						cd_mat_vetor_w,
						cd_unid_med_prescr_w,
						sysdate,
						sysdate,
						nm_usuario_p,
						nm_usuario_p,
						qt_mat_vetor_w,
						substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30),
						ceil(obter_dose_convertida(cd_mat_vetor_w,qt_mat_vetor_w,cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30))),
						nr_seq_ordem_p,
						nr_prescricao_www,
						nr_seq_prescricao_w,
						cd_mat_vetor_w,
						ie_via_aplicacao_w
					from 	dual;
					ELSE
					Insert 	into dispensacao_quimioterapia(
						nr_sequencia,
						cd_material,
						cd_unidade_medida_dose,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario,
						nm_usuario_nrec,
						qt_dose,
						cd_unidade_conta,
						qt_material_conta,
						nr_seq_ordem,
						nr_prescricao,
						nr_seq_prescricao,
						cd_material_exec,
						ie_via_aplicacao)
					select 	dispensacao_quimioterapia_seq.nextval,
						cd_mat_vetor_w,
						cd_unid_med_prescr_w,
						sysdate,
						sysdate,
						nm_usuario_p,
						nm_usuario_p,
						qt_mat_vetor_w,
						substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30),
						obter_dose_convertida(cd_mat_vetor_w,qt_mat_vetor_w,cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30)),
						nr_seq_ordem_p,
						nr_prescricao_www,
						nr_seq_prescricao_w,
						cd_mat_vetor_w,
						ie_via_aplicacao_w
					from 	dual;
					END IF;

					commit;

			END IF;
		cont_ww := 0;
		open c02;
		loop
		fetch c02 into 
			cd_material_regra_w,
			qt_dose_regra_w,
			cd_unidade_medida_consumo_w;
		exit when c02%notfound;
			vetor_ww(cont_ww).cd_material_w	:= cd_material_regra_w;
			vetor_ww(cont_ww).qt_dose_w	:= qt_dose_regra_w;
			vetor_ww(cont_ww).cd_unidade_medida_w := cd_unidade_medida_consumo_w;
			cont_ww := cont_ww + 1;
		end loop;
		close c02;
		qt_sobra_w := -9999999999;
		cont_ww := cont_ww - 1;
		while (cont_ww <> 0) loop 
			qt_dose_prescricao_ww := nvl(qt_dose_prescricao_w,0);
			qt_frasco_w := 0;
			while (qt_dose_prescricao_ww > 0) loop
				qt_dose_prescricao_ww := qt_dose_prescricao_ww - vetor_ww(cont_ww).qt_dose_w;
				qt_frasco_w := qt_frasco_w + 1;
			end loop;
		if (qt_dose_prescricao_ww > qt_sobra_w) then
			qt_sobra_w := qt_dose_prescricao_ww;
			cont_mat_menor_disper_w := cont_ww;
			qt_frasco_tot_w :=  qt_frasco_w;
			cd_mat_menor_disp_w := vetor_ww(cont_ww).cd_material_w;
			cd_unid_medida_disp_w := vetor_ww(cont_ww).cd_unidade_medida_w;
		end if;
		cont_ww := cont_ww - 1;
		end loop;	
		
		if (qt_sobra_w > qt_perda_final_w) or
			((qt_sobra_w = qt_perda_final_w) and (qt_frasco_tot_w < vetor_w.COUNT)) then
			
			delete from dispensacao_quimioterapia 
			where 	nr_seq_ordem = nr_seq_ordem_p 
			and 	nr_seq_prescricao = nr_seq_prescricao_w;
			
			IF	(ie_arred_dose_w = 'S') THEN
			Insert 	into dispensacao_quimioterapia(
				nr_sequencia,
				cd_material,
				cd_unidade_medida_dose,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				qt_dose,
				cd_unidade_conta,
				qt_material_conta,
				nr_seq_ordem,
				nr_prescricao,
				nr_seq_prescricao,
				cd_material_exec,
				ie_via_aplicacao)
			select 	dispensacao_quimioterapia_seq.nextval,
				cd_mat_menor_disp_w,
				cd_unid_med_prescr_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				qt_dose_prescricao_w,
				substr(Obter_Dados_Material(cd_mat_menor_disp_w,'UMC'),1,30),
				ceil(obter_dose_convertida(cd_mat_menor_disp_w,qt_frasco_tot_w,cd_unid_medida_disp_w,substr(Obter_Dados_Material(cd_mat_menor_disp_w,'UMC'),1,30))),
				nr_seq_ordem_p,
				nr_prescricao_www,
				nr_seq_prescricao_w,
				cd_mat_menor_disp_w,
				ie_via_aplicacao_w
			from 	dual;
			ELSE
			Insert 	into dispensacao_quimioterapia(
				nr_sequencia,
				cd_material,
				cd_unidade_medida_dose,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				qt_dose,
				cd_unidade_conta,
				qt_material_conta,
				nr_seq_ordem,
				nr_prescricao,
				nr_seq_prescricao,
				cd_material_exec,
				ie_via_aplicacao)
			select 	dispensacao_quimioterapia_seq.nextval,
				cd_mat_menor_disp_w,
				cd_unid_med_prescr_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				qt_dose_prescricao_w,
				substr(Obter_Dados_Material(cd_mat_menor_disp_w,'UMC'),1,30),
				obter_dose_convertida(cd_mat_menor_disp_w,qt_frasco_tot_w,cd_unid_medida_disp_w,substr(Obter_Dados_Material(cd_mat_menor_disp_w,'UMC'),1,30)),
				nr_seq_ordem_p,
				nr_prescricao_www,
				nr_seq_prescricao_w,
				cd_mat_menor_disp_w,
				ie_via_aplicacao_w
			from 	dual;
			END IF;
			commit;
			
		end if;
			
	ELSE
		SELECT	NVL(MAX(nr_sequencia),0) +1
		INTO	nr_seq_medic_w
		FROM	prescr_material
		WHERE	nr_prescricao	= nr_prescricao_w;

			IF	(ie_arred_dose_w = 'S') THEN
			Insert 	into dispensacao_quimioterapia(
				nr_sequencia,
				cd_material,
				cd_unidade_medida_dose,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				qt_dose,
				cd_unidade_conta,
				qt_material_conta,
				nr_seq_ordem,
				nr_prescricao,
				nr_seq_prescricao,
				cd_material_exec,
				ie_via_aplicacao)
			select 	dispensacao_quimioterapia_seq.nextval,
				cd_material_w,
				cd_unid_med_prescr_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p ,
				qt_dose_prescricao_w,
				substr(Obter_Dados_Material(cd_material_w,'UMC'),1,30),
				ceil(obter_dose_convertida(cd_material_w,qt_dose_prescricao_w,cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_material_w,'UMC'),1,30))),
				nr_seq_ordem_p,
				nr_prescricao_www,
				nr_seq_prescricao_w,
				cd_mat_vetor_w,
				ie_via_aplicacao_w
			from 	dual;
			ELSE
			Insert 	into dispensacao_quimioterapia(
				nr_sequencia,
				cd_material,
				cd_unidade_medida_dose,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				qt_dose,
				cd_unidade_conta,
				qt_material_conta,
				nr_seq_ordem,
				nr_prescricao,
				nr_seq_prescricao,
				cd_material_exec,
				ie_via_aplicacao)
			select 	dispensacao_quimioterapia_seq.nextval,
				cd_material_w,
				cd_unid_med_prescr_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p ,
				qt_dose_prescricao_w,
				substr(Obter_Dados_Material(cd_material_w,'UMC'),1,30),
				obter_dose_convertida(cd_material_w,qt_dose_prescricao_w,cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_material_w,'UMC'),1,30)),
				nr_seq_ordem_p,
				nr_prescricao_www,
				nr_seq_prescricao_w,
				cd_mat_vetor_w,
				ie_via_aplicacao_w
			from 	dual;
			END IF;

			commit;
		END IF;
	END;
END LOOP;
CLOSE c01;

COMMIT;

END gerar_quimio_melhor_apresent;
/