create or replace
procedure pls_desfazer_lote_pos_estab
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Desfazer o lote de p�s-estabelecido
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	OPS - Controle de Coparticipa��es e P�s-estabelecidos
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

delete	from	pls_lib_pos_estabelecido
where	nr_seq_lote	= nr_seq_lote_p;

update	pls_lote_pos_estabelecido
set	dt_geracao		= null,
	nm_usuario_geracao	= null,
	ie_status		= 'P'
where	nr_sequencia		= nr_seq_lote_p;

pls_gravar_hist_lote_pos_estab(	nr_seq_lote_p,
				'Desfazer lote',
				'N',
				nm_usuario_p,
				cd_estabelecimento_p);

commit;

end pls_desfazer_lote_pos_estab;
/