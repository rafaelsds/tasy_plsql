CREATE OR REPLACE
PROCEDURE Baixar_Titulos_Cobranca_Escrit
			(	nr_seq_cobranca_p		number,
				ie_acao_p			varchar2,
				nm_usuario_p			varchar2,
				dt_baixa_p			date,
				ie_tipo_carteira_p		varchar2,
				ie_commit_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	ie_tipo_carteira_p
		0 - T�tulos
		1 - Particular
		2 - Conv�nio
	ie_acao_p
		I - Inclus�o
		E - Estorno
		
		
	cursor c_titulos_consistencia:
		Se tiver inconsist�ncia, n�o deve baixar t�tulo algum(rollback), e deve
		gerar as inconsist�ncias.
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	Cobran�a Escritural
	
	HDH_GERAR_RETORNO_BRADESCO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

				
ds_observacao_w				varchar2(4000);
ds_erro_w				varchar2(255);
ds_observacao_titulo_w			varchar2(255);
ie_cons_saldo_w				varchar2(255);
ie_cons_reg_w				varchar2(255);
ie_gerar_critica_w			varchar2(255);
nr_documento_w				varchar2(20);
cd_autorizacao_w			varchar2(20);
ie_dt_liq_cobr_w			varchar2(1);
ie_internacional_w			varchar2(1);
ie_somente_liquidacao_w			varchar2(1);
ie_somente_recebimento_w		varchar2(1);
ie_despesa_bancaria_w			varchar2(1);
ie_gerou_inconsist_w			varchar2(1)	:= 'N';
vl_baixa_w				number(15,2);
vl_desconto_w				number(15,2);
vl_acrescimo_w				number(15,2);
vl_recebimento_w			number(15,2);
vl_conta_w				number(15,2);
vl_total_titulos_w			number(15,2);
vl_despesa_bancaria_w			number(15,2);
vl_juros_w				number(15,2);
vl_multa_w				number(15,2);
vl_liquidacao_w				number(15,2);
vl_guia_positivo_w			number(15,2);
vl_nota_credito_w			number(15,2);
vl_credito_w				number(15,2);
vl_total_guias_w			number(15,2);
vl_total_pago_w				number(15,2);
vl_credito_positivo_w			number(15,2);
nr_titulo_w				number(10);
cd_estabelecimento_w			number(10);
nr_seq_conta_banco_w			number(10);
nr_seq_conv_receb_w			number(10);
nr_seq_retorno_w			number(10);
nr_seq_prot_tit_w			number(10);
nr_interno_conta_tit_w			number(10);
nr_interno_conta_w			number(10);
nr_seq_trans_fin_conv_receb_w		number(10);
nr_seq_tf_cobr_acres_w			number(10);
nr_seq_tf_cobr_desc_w			number(10);
nr_seq_tf_cobr_desp_w			number(10);
nr_seq_tf_cobr_juros_w			number(10);
nr_seq_tf_cobr_multa_w			number(10);
nr_seq_ret_item_w			number(10);
nr_seq_motivo_desc_w			number(10);
nr_cobranca_w				number(10);
nr_seq_hist_cob_w			number(10);
nr_seq_empresa_w			number(10);
nr_mes_ant_w				number(10);
qt_baixas_w				number(10);
nr_vetor_w				number(10);
i					number(10);
qt_inconsistencia_w			number(10);
cd_centro_custo_desc_w			number(8);
cd_convenio_w				number(5);
cd_banco_w				number(3);
ie_impar_w				number(2);
qt_tit_cobranca_w			number(2);
dt_liquidacao_w				date;

vl_cobranca_w				titulo_receber_cobr.vl_cobranca%type;
nr_seq_tit_rec_cobr_w			titulo_receber_cobr.nr_sequencia%type;

Cursor C02 is
	select	nvl(b.cd_convenio_conta, obter_convenio_tit_rec(b.nr_titulo)),
		sum(a.vl_cobranca),
		sum(a.vl_acrescimo),
		sum(a.vl_desconto),
		sum(a.vl_despesa_bancaria),
		sum(a.vl_juros),
		sum(a.vl_multa),
		max(a.dt_liquidacao),
		nvl(sum(a.vl_liquidacao),0) + decode(nvl(ie_despesa_bancaria_w,'N'),'S',nvl(sum(a.vl_despesa_bancaria),0),0)
	from	parametro_contas_receber	e,
		cobranca_escritural		d,
		banco_ocorr_escrit_ret		c,
		titulo_receber			b,
		titulo_receber_cobr		a
	where	d.cd_estabelecimento	= e.cd_estabelecimento(+)
	and	a.nr_seq_cobranca	= d.nr_sequencia
	and	a.nr_titulo		= b.nr_titulo
	and	a.nr_seq_cobranca	= nr_seq_cobranca_p
	and	obter_tipo_carteira_tit_rec(a.nr_titulo) = '2'
	and	a.vl_liquidacao		> 0
	and	(nvl(ie_acao_p,'I') <> 'I' or b.vl_saldo_titulo > 0)
	and	(nvl(a.vl_liquidacao,0) + nvl(a.vl_despesa_bancaria,0) - nvl(a.vl_acrescimo,0)) > 0
	and	(ie_dt_liq_cobr_w = 'N' or a.dt_liquidacao is not null)
	and	a.nr_seq_ocorrencia_ret	= c.nr_sequencia(+)
	and	(ie_somente_liquidacao_w = 'N' or c.ie_rejeitado = 'L')
	and	nvl(a.nr_seq_trans_financ, nvl(e.nr_seq_trans_fin_conv_receb, 0)) = nvl(nr_seq_trans_fin_conv_receb_w, 0)
	group by
		nvl(b.cd_convenio_conta, obter_convenio_tit_rec(b.nr_titulo));	

Cursor C03 is
	select	a.nr_titulo,
		a.vl_cobranca,
		b.nr_seq_protocolo,
		b.nr_interno_conta,
		b.nr_documento,
		nvl(b.vl_saldo_juros,0) + nvl(a.vl_juros,0) + nvl(a.vl_acrescimo,0),
		nvl(a.vl_desconto,0) - nvl(to_number(obter_valor_nc_titulo_cobr(a.nr_sequencia)),0),
		nvl(to_number(obter_valor_nc_titulo_cobr(a.nr_sequencia)), 0),
		nvl(a.vl_liquidacao, 0) + decode(nvl(ie_despesa_bancaria_w, 'N'), 'S', nvl(a.vl_despesa_bancaria, 0),0)
	from	parametro_contas_receber	e,
		cobranca_escritural		d,
		banco_ocorr_escrit_ret		c,
		titulo_receber			b,
		titulo_receber_cobr		a
	where	d.cd_estabelecimento	= e.cd_estabelecimento(+)
	and	a.nr_seq_cobranca	= d.nr_sequencia
	and	a.nr_titulo		= b.nr_titulo
	and	a.nr_seq_cobranca	= nr_seq_cobranca_p
	and	nvl(b.cd_convenio_conta,obter_convenio_tit_rec(b.nr_titulo))		= cd_convenio_w
	and	b.ie_tipo_carteira 	= '2'
	and	a.vl_liquidacao		> 0
	and	(nvl(ie_acao_p,'I') <> 'I' or b.vl_saldo_titulo	> 0)
	and	(nvl(a.vl_liquidacao,0) + nvl(a.vl_despesa_bancaria,0) - nvl(a.vl_acrescimo,0)) > 0
	and	(ie_dt_liq_cobr_w = 'N' or a.dt_liquidacao is not null)
	and	a.nr_seq_ocorrencia_ret	= c.nr_sequencia(+)
	and	(ie_somente_liquidacao_w = 'N' or c.ie_rejeitado = 'L')
	and	nvl(a.nr_seq_trans_financ, nvl(e.nr_seq_trans_fin_conv_receb, 0))	= nvl(nr_seq_trans_fin_conv_receb_w, 0)
	order by
		a.vl_cobranca;

cursor C04 is
	select	nr_interno_conta,
		cd_autorizacao,
		sum(vl_saldo_guia) vl_saldo_guia,
		sum(vl_guia) vl_guia
	from	(select	b.nr_interno_conta, 
			b.cd_autorizacao, 
			obter_saldo_conpaci(b.nr_interno_conta,b.cd_autorizacao) vl_saldo_guia,
			b.vl_guia
		from	conta_paciente_guia	b, 
			conta_paciente		a
		where	nvl(b.dt_convenio,sysdate)	>= sysdate - nvl(nr_mes_ant_w,3) * 30
		and	a.nr_interno_conta	= b.nr_interno_conta
		and	a.ie_status_acerto	= 2
		and	a.ie_cancelamento	is null
		and	(a.cd_estabelecimento	= cd_estabelecimento_w or
			OBTER_ESTAB_FINANCEIRO(a.cd_estabelecimento) = cd_estabelecimento_w)
		and	(a.nr_seq_protocolo = nr_seq_prot_tit_w or a.nr_interno_conta = nr_interno_conta_tit_w)) w
	where	(vl_saldo_guia > 0 or (vl_guia < 0 and (nvl(vl_guia,0) - nvl(vl_saldo_guia,0)) = 0))
	and	nvl(ie_acao_p,'I')	= 'I'
	group by
		nr_interno_conta,
		cd_autorizacao
	union all
	select	y.nr_interno_conta,
		y.cd_autorizacao,
		sum(y.vl_pago),
		sum(z.vl_guia)
	from	conta_paciente_guia	z,
		convenio_retorno_item	y,
		convenio_retorno	x
	where	nvl(y.cd_autorizacao, 'N�o Informada') = nvl(z.cd_autorizacao, 'N�o Informada') 
	and	y.nr_interno_conta	= z.nr_interno_conta
	and	y.nr_titulo		= nr_titulo_w
	and	x.nr_sequencia		= y.nr_seq_retorno
	and	x.nr_seq_cobranca	= nr_seq_cobranca_p
	and	nvl(ie_acao_p,'I') <> 'I'
	group by
		y.nr_interno_conta,
		y.cd_autorizacao;

cursor C05 is
	select	a.nr_titulo,
		b.nr_seq_hist_cobr,
		to_char(sysdate,'dd/mm/yyyy') || ' ' || b.cd_ocorrencia || ' - ' || b.ds_ocorrencia ||
			' - ' || WHEB_MENSAGEM_PCK.get_texto(818039,'NR_SEQ_COBRANCA_P=' || to_char(nr_seq_cobranca_p)),
		d.nr_sequencia
	from	cobranca d,
		titulo_receber		c,
		banco_ocorr_escrit_ret	b,
		titulo_receber_cobr	a
	where	a.nr_seq_cobranca	= nr_seq_cobranca_p
	and	(nvl(ie_tipo_carteira_p,'0') = '0' or nvl(obter_tipo_carteira_tit_rec(a.nr_titulo),'0') = nvl(ie_tipo_carteira_p,'0'))
	and	a.nr_titulo		= c.nr_titulo
	and	a.nr_seq_ocorrencia_ret	= b.nr_sequencia
	and	d.nr_titulo		= c.nr_titulo
	and	ie_acao_p		= 'I'
	and	b.nr_seq_hist_cobr is not null;

Cursor C06 is
	select	distinct
		nvl(a.nr_seq_trans_financ,nvl(e.nr_seq_trans_fin_conv_receb,0)) nr_seq_trans_fin_conv_receb
	from	parametro_contas_receber	e,
		cobranca_escritural		d,
		banco_ocorr_escrit_ret		c,
		titulo_receber			b,
		titulo_receber_cobr		a
	where	d.cd_estabelecimento	= e.cd_estabelecimento(+)
	and	a.nr_seq_cobranca	= d.nr_sequencia
	and	a.nr_titulo		= b.nr_titulo
	and	a.nr_seq_cobranca	= nr_seq_cobranca_p
	and	b.ie_tipo_carteira 	= '2'
	and	a.vl_liquidacao		> 0
	and	(nvl(ie_acao_p,'I') <> 'I' or b.vl_saldo_titulo	> 0)
	and	(nvl(a.vl_liquidacao,0) + nvl(a.vl_despesa_bancaria,0) - nvl(a.vl_acrescimo,0)) > 0
	and	(ie_dt_liq_cobr_w = 'N' or a.dt_liquidacao is not null)
	and	a.nr_seq_ocorrencia_ret	= c.nr_sequencia(+)
	and	(ie_somente_liquidacao_w = 'N' or c.ie_rejeitado = 'L');
	
cursor c_titulos_consistencia is
	select	b.nr_titulo,
		b.vl_titulo,
		b.vl_saldo_titulo
	from	titulo_receber		b,
		titulo_receber_liq	a
	where	b.nr_titulo		= a.nr_titulo
	and	a.nr_seq_cobranca	= nr_seq_cobranca_p
	and	a.nr_sequencia		=	(select	max(x.nr_sequencia)
						from	titulo_receber_liq	x
						where	x.nr_seq_cobranca	= nr_seq_cobranca_p
						and	x.nr_titulo		= a.nr_titulo);

vetor_titulos_consist c_titulos_consistencia%rowtype;


type titulos_inconsistentes is record (	nr_titulo_escrit	number(10),
					ie_cons_saldo		varchar2(255),
					ie_cons_reg		varchar2(255));
					
type vetor_tit_inconsist is table of titulos_inconsistentes index by binary_integer;
vetor_tit_inconsist_w	vetor_tit_inconsist;

begin
if	(nr_seq_cobranca_p is not null) then
	begin
	
	begin
	select	a.cd_estabelecimento,
		b.cd_banco,
		b.nr_sequencia,
		a.nr_seq_empresa
	into	cd_estabelecimento_w,
		cd_banco_w,
		nr_seq_conta_banco_w,
		nr_seq_empresa_w
	from	banco_estabelecimento b,
		cobranca_escritural a
	where	a.nr_seq_conta_banco	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_cobranca_p;
	exception when others then
		wheb_mensagem_pck.exibir_mensagem_abort(238658);
	end;

	select	nvl(max(ie_dt_liq_cobr), 'N')
	into	ie_dt_liq_cobr_w
	from	banco
	where	cd_banco	= nvl(cd_banco_w, -1)
	and	ie_situacao	= 'A';
	
	begin
	select	nr_seq_tf_cobr_acres,
		nr_seq_tf_cobr_desc,
		nr_seq_tf_cobr_desp,
		nr_seq_tf_cobr_juros,
		nr_seq_tf_cobr_multa
	into	nr_seq_tf_cobr_acres_w,
		nr_seq_tf_cobr_desc_w,
		nr_seq_tf_cobr_desp_w,
		nr_seq_tf_cobr_juros_w,
		nr_seq_tf_cobr_multa_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_w;
	exception when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(238662);
	end;

	if	(nvl(ie_tipo_carteira_p,'0') in ('0','1')) then /* Francisco - 09/12/2009 - OS 180749 - Se a baixa n�o for por tipo de carteira ou se for de particular */
		Baixar_Titulos_Cobranca_Partic(nr_seq_cobranca_p,null,ie_tipo_carteira_p,dt_baixa_p,nm_usuario_p,ie_acao_p);
	elsif	(ie_tipo_carteira_p = '2') then /* Se for de conv�nio */
		obter_param_usuario(815, 11, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_somente_liquidacao_w);
		obter_param_usuario(815, 27, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_somente_recebimento_w);
		obter_param_usuario(27, 9, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, nr_mes_ant_w);
		obter_param_usuario(815, 35, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_despesa_bancaria_w);

		/* gerar uma movimenta��o para cada transa��o financeira dos t�tulos */
		open C06;
		loop
		fetch C06 into
			nr_seq_trans_fin_conv_receb_w;
		exit when C06%notfound;

			/* Separar por conv�nio */
			open C02;
			loop
			fetch C02 into
				cd_convenio_w,
				vl_recebimento_w,
				vl_acrescimo_w,
				vl_desconto_w,
				vl_despesa_bancaria_w,
				vl_juros_w,
				vl_multa_w,
				dt_liquidacao_w,
				vl_liquidacao_w;
			exit when C02%notfound;

				select	nvl(max(c.ie_internacional),'N')
				into	ie_internacional_w
				from	tipo_pessoa_juridica	c,
					pessoa_juridica		b,
					convenio		a
				where	a.cd_convenio		= cd_convenio_w
				and	a.cd_cgc		= b.cd_cgc
				and	b.cd_tipo_pessoa	= c.cd_tipo_pessoa;

				/* se for conv�nio internacional baixa da mesma forma que o particular - ahoffelder - OS 208411 - 31/05/2010 */
				if	(ie_internacional_w = 'S') then
					Baixar_Titulos_Cobranca_Partic(	nr_seq_cobranca_p,
									cd_convenio_w,
									ie_tipo_carteira_p,
									dt_baixa_p,
									nm_usuario_p,
									ie_acao_p);
				else
					if	(nvl(nr_seq_trans_fin_conv_receb_w, 0) = 0) then /* Gerar recebimento de conv�nio */
						wheb_mensagem_pck.exibir_mensagem_abort(188373);
					end if;

					select	convenio_receb_seq.nextval
					into	nr_seq_conv_receb_w
					from	dual;

					insert into convenio_receb
						(nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						cd_convenio,
						dt_recebimento,
						vl_recebimento,
						ie_status,
						ie_tipo_glosa,
						cd_estabelecimento,
						ie_integrar_cb_fluxo,
						nr_seq_cobranca,
						nr_seq_conta_banco,
						nr_seq_trans_fin,
						vl_deposito)
					values	(nr_seq_conv_receb_w,
						nm_usuario_p,
						sysdate,
						cd_convenio_w,
						nvl(dt_liquidacao_w,sysdate),
						decode(nvl(ie_acao_p,'I'),'I',vl_recebimento_w,vl_recebimento_w * -1),
						'N',
						'N',
						cd_estabelecimento_w,
						'S',
						nr_seq_cobranca_p,
						nr_seq_conta_banco_w,
						nr_seq_trans_fin_conv_receb_w,
						decode(nvl(ie_acao_p,'I'),'I',vl_liquidacao_w, vl_liquidacao_w * -1));

					if	(ie_acao_p <> 'I') then
						vl_acrescimo_w		:= vl_acrescimo_w * -1;
						vl_desconto_w		:= vl_desconto_w * -1;
						vl_despesa_bancaria_w	:= vl_despesa_bancaria_w * -1;
						vl_juros_w		:= vl_juros_w * -1;
						vl_multa_w		:= vl_multa_w * -1;
					end if;

					gerar_conv_receb_adic(nr_seq_conv_receb_w,vl_acrescimo_w,nr_seq_tf_cobr_acres_w,nm_usuario_p);
					gerar_conv_receb_adic(nr_seq_conv_receb_w,vl_desconto_w,nr_seq_tf_cobr_desc_w,nm_usuario_p);
					gerar_conv_receb_adic(nr_seq_conv_receb_w,vl_juros_w,nr_seq_tf_cobr_juros_w,nm_usuario_p);
					gerar_conv_receb_adic(nr_seq_conv_receb_w,vl_multa_w,nr_seq_tf_cobr_multa_w,nm_usuario_p);
					gerar_conv_receb_adic(nr_seq_conv_receb_w,vl_despesa_bancaria_w,nr_seq_tf_cobr_desp_w,nm_usuario_p);

					gerar_movto_receb_convenio(cd_estabelecimento_w, nr_seq_conv_receb_w, nm_usuario_p,'N');

					if	(nvl(ie_somente_recebimento_w, 'N') = 'N') then
						if	(nvl(ie_acao_p,'I') <> 'I') then
							ds_observacao_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(818040, 'NR_SEQ_COBRANCA_P=' || nr_seq_cobranca_p), 1, 4000); /*Retorno gerado a partir do estorno da baixa da cobran�a*/
										
						else
							ds_observacao_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(818041, 'NR_SEQ_COBRANCA_P=' || nr_seq_cobranca_p), 1, 4000); /*'Retorno gerado a partir da baixa da cobran�a*/
										
						end if;

						/* Gerar retorno conv�nio */
						select	convenio_retorno_seq.nextval
						into	nr_seq_retorno_w
						from	dual;

						insert into convenio_retorno
							(nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							cd_estabelecimento,
							cd_convenio,
							dt_retorno,
							ie_status_retorno,
							nm_usuario_retorno,
							ie_tipo_glosa,
							dt_baixa_cr,
							dt_recebimento,
							ie_baixa_unica_ret,
							nr_seq_cobranca,
							ds_observacao)
						values	(nr_seq_retorno_w,
							nm_usuario_p,
							sysdate,
							cd_estabelecimento_w,
							cd_convenio_w,
							sysdate,
							'R',
							nm_usuario_p,
							'N',
							nvl(dt_baixa_p,sysdate),
							sysdate,
							'N',
							nr_seq_cobranca_p,
							ds_observacao_w);

						/* Gerar guias do retorno */
						vl_total_titulos_w	:= 0;

						open C03;
						loop
						fetch C03 into
							nr_titulo_w,
							vl_baixa_w,
							nr_seq_prot_tit_w,
							nr_interno_conta_tit_w,
							nr_documento_w,
							vl_juros_w,
							vl_desconto_w,
							vl_nota_credito_w,
							vl_credito_w;
						exit when C03%notfound;

							vl_credito_positivo_w	:= vl_credito_w;

							if	(nvl(ie_acao_p,'I') <> 'I') then
								vl_credito_w	:= vl_credito_w * -1;
							end if;

							select	max(a.nr_seq_motivo_desc),
								max(a.cd_centro_custo)
							into	nr_seq_motivo_desc_w,
								cd_centro_custo_desc_w
							from	titulo_receber_liq_desc	a
							where	a.nr_titulo	= nr_titulo_w
							and	a.nr_seq_liq	is null
							and	a.nr_bordero	is null;

							select	sum(vl_saldo_guia)
							into	vl_total_guias_w
							from	(select	sum(vl_saldo_guia) vl_saldo_guia
								from	(select	obter_saldo_conpaci(b.nr_interno_conta,b.cd_autorizacao) vl_saldo_guia,
										b.vl_guia
									from	conta_paciente_guia b, 
										conta_paciente a
									where	nvl(b.dt_convenio,sysdate)	>= sysdate - nvl(nr_mes_ant_w,3) * 30
									and	a.nr_interno_conta	= b.nr_interno_conta
									and	a.ie_status_acerto	= 2
									and	a.ie_cancelamento	is null
									and	a.cd_estabelecimento	= cd_estabelecimento_w
									and	(a.nr_seq_protocolo = nr_seq_prot_tit_w or a.nr_interno_conta = nr_interno_conta_tit_w)) w
								where	(vl_saldo_guia > 0 or (vl_guia < 0 and (nvl(vl_guia,0) - nvl(vl_saldo_guia,0)) = 0))
								and	nvl(ie_acao_p,'I')	= 'I'
								union all
								select	y.vl_pago vl_saldo_guia
								from	conta_paciente_guia z,
									convenio_retorno_item y,
									convenio_retorno x
								where	nvl(y.cd_autorizacao,'N�o Informada') = nvl(z.cd_autorizacao,'N�o Informada')
								and	y.nr_interno_conta	= z.nr_interno_conta
								and	y.nr_titulo		= nr_titulo_w
								and	x.nr_sequencia		= y.nr_seq_retorno
								and	x.nr_seq_cobranca	= nr_seq_cobranca_p
								and	nvl(ie_acao_p,'I')	<> 'I');

							vl_total_pago_w	:= 0;

							/* Guias do t�tulo */
							open C04;
							loop
							fetch C04 into
								nr_interno_conta_w,
								cd_autorizacao_w,
								vl_conta_w,
								vl_guia_positivo_w;
							exit when C04%notfound;

								vl_conta_w	:= nvl(vl_conta_w,0);

								/* ahoffelder - OS 433342 - 02/05/2012 - fiz proporcional porque o t�tulo pode ser proveniente de desdobramento
								de forma que v�rias guias possuam v�rios t�tulos vinculados (N para N) */
								--vl_conta_w	:= dividir(nvl(vl_conta_w,0),(nvl(vl_total_guias_w,0) * nvl(vl_credito_positivo_w,0)));

								select	convenio_retorno_item_seq.nextval
								into	nr_seq_ret_item_w
								from	dual;

								if	(nvl(ie_acao_p,'I') <> 'I') then
									vl_conta_w		:= vl_conta_w * -1;
									vl_desconto_w		:= vl_desconto_w * -1;
									vl_nota_credito_w	:= vl_nota_credito_w * -1;
								end if;
								
								insert into convenio_retorno_item
									(nr_sequencia,
									nr_seq_retorno,
									nm_usuario,
									dt_atualizacao,
									ie_glosa,
									vl_pago,
									vl_glosado,
									vl_amenor,
									vl_adequado,
									vl_adicional,
									vl_desconto,
									ie_analisada,
									nr_interno_conta,
									cd_autorizacao,
									vl_guia,
									ie_libera_repasse,
									nr_titulo,
									cd_centro_custo_desc,
									nr_seq_motivo_desc,
									vl_nota_credito)
								values	(nr_seq_ret_item_w,
									nr_seq_retorno_w,
									nm_usuario_p,
									sysdate,
									'P',
									vl_conta_w,
									0,
									0,
									0,
									0,
									vl_desconto_w,
									'N',
									nr_interno_conta_w,
									cd_autorizacao_w,
									vl_guia_positivo_w,
									'S',
									nr_titulo_w,
									cd_centro_custo_desc_w,
									nr_seq_motivo_desc_w,
									vl_nota_credito_w);

								insert	into convenio_ret_item_desc
									(NR_SEQUENCIA,
									DT_ATUALIZACAO,
									NM_USUARIO,
									DT_ATUALIZACAO_NREC,
									NM_USUARIO_NREC,
									NR_SEQ_RET_ITEM,
									CD_PESSOA_FISICA,
									CD_CGC)
								(select	convenio_ret_item_desc_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_ret_item_w,
									cd_pessoa_fisica,
									cd_cgc
								from	titulo_receber_liq_desc
								where	nr_titulo	= nr_titulo_w
								and	nr_seq_liq	is null
								and	nr_bordero	is null);

								vl_total_pago_w	:= nvl(vl_total_pago_w,0) + nvl(vl_conta_w,0);

							end loop;
							close C04;

							if	(nvl(ie_acao_p, 'I') = 'I') and
								(nvl(vl_total_pago_w, 0) <> nvl(vl_credito_w, 0)) then
								update	convenio_retorno_item
								set	vl_pago		= nvl(vl_pago,0) + nvl(vl_credito_w,0) - nvl(vl_total_pago_w,0)
								where	nr_sequencia	= nr_seq_ret_item_w
								and	nr_titulo	= nr_titulo_w;
							end if;

							ratear_titulo_convenio(nr_seq_retorno_w, nr_titulo_w, nr_seq_cobranca_p, nm_usuario_p, ie_acao_p);

							vl_total_titulos_w	:= nvl(vl_total_titulos_w,0) + nvl(vl_baixa_w,0);
						end loop;
						close C03;

						/* Consistir */
						if	(nvl(ie_acao_p, 'I') = 'I') then /* N�o consiste retorno negativo */
							consistir_Retorno_Convenio(	nr_seq_retorno_w,
											nm_usuario_p,
											'N',
											'N');
						else
							update	convenio_retorno
							set	ie_status_retorno	= 'C'
							where	nr_sequencia		= nr_seq_retorno_w;
						end if;

						/* Vincular */
						if	(vl_total_titulos_w <> vl_recebimento_w) then
							wheb_mensagem_pck.exibir_mensagem_abort(188374,	'VL_TOTAL_TITULOS_W=' || vl_total_titulos_w ||
													';VL_RECEBIMENTO_W=' || vl_recebimento_w);
						else
							insert into convenio_ret_receb
								(nr_seq_receb,
								nm_usuario,
								dt_atualizacao,
								nr_seq_retorno,
								vl_vinculacao)
							values	(nr_seq_conv_receb_w,
								nm_usuario_p,
								sysdate,
								nr_seq_retorno_w,
								decode(nvl(ie_acao_p, 'I'), 'I', vl_recebimento_w, vl_recebimento_w * -1));	

							update	convenio_receb
							set	ie_status	= 'T'
							where	nr_sequencia	= nr_seq_conv_receb_w;

							update	convenio_retorno
							set	dt_vinculacao		= sysdate,
								ie_status_retorno	= 'V'
							where	nr_sequencia		= nr_seq_retorno_w;
						end if;

						/* Baixar */
						baixa_titulo_convenio(	nr_seq_retorno_w,
									nm_usuario_p,
									nr_seq_conta_banco_w,
									ds_erro_w,
									null,
									null,
									'N',
									null,
									null);

					end if;

				end if;

			end loop;
			close C02;

		end loop;
		close C06;

		if	(ie_acao_p = 'I') then
			begin
			insert into cobr_escrit_baixa
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_cobranca,
				ie_tipo_carteira,
				dt_baixa)
			values	(cobr_escrit_baixa_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_cobranca_p,
				ie_tipo_carteira_p,
				nvl(dt_baixa_p,sysdate));
			exception
			when dup_val_on_index then
				wheb_mensagem_pck.exibir_mensagem_abort(188377,	'DS_TIPO_CARTEIRA_W=' || obter_valor_dominio(3132,ie_tipo_carteira_p));
			end;
		else
			delete	from cobr_escrit_baixa
			where	nr_seq_cobranca		= nr_seq_cobranca_p
			and	ie_tipo_carteira	= ie_tipo_carteira_p;

			update	cobranca_escritural
			set	nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_cobranca_p;
		end if;
	end if;
	
	if	(ie_acao_p = 'I') then
		obter_param_usuario(815, 40, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_critica_w);
		
		if	(ie_gerar_critica_w = 'S') then
			nr_vetor_w		:= 0;
			ie_gerou_inconsist_w	:= 'N';
			
			open c_titulos_consistencia;
			loop
			fetch c_titulos_consistencia into	
				vetor_titulos_consist;
			exit when c_titulos_consistencia%notfound;
				begin
				select	count(1)
				into	qt_baixas_w
				from	titulo_receber_liq	a
				where	a.nr_titulo		= vetor_titulos_consist.nr_titulo
				and	a.nr_seq_cobranca	= nr_seq_cobranca_p;
				
				if	((mod(qt_baixas_w,2)) <> 0) then
					select	count(1)
					into	qt_tit_cobranca_w
					from	cobranca_escritural	b,
						titulo_receber_cobr	a
					where	a.nr_titulo		= vetor_titulos_consist.nr_titulo
					and	b.nr_sequencia		= a.nr_seq_cobranca
					and	b.ie_remessa_retorno	= 'R'
					and	rownum			= 1;

					select	nvl(a.vl_cobranca, 0),
						a.nr_sequencia
					into	vl_cobranca_w,
						nr_seq_tit_rec_cobr_w
					from	titulo_receber_cobr	a
					where	a.nr_titulo		= vetor_titulos_consist.nr_titulo
					and	a.nr_seq_cobranca	= nr_seq_cobranca_p
					and obter_dados_bco_ocorr_escr_ret(a.nr_seq_ocorrencia_ret,'I') = 'L';
					
					ie_cons_saldo_w	:= 'N';
					ie_cons_reg_w	:= 'N';
					
					if	(vetor_titulos_consist.vl_saldo_titulo <> 0) then
						select	count(1)
						into	qt_inconsistencia_w
						from	inconsistencia_tit_escrit	a
						where	a.nr_seq_titulo_escrit	= nr_seq_tit_rec_cobr_w
						and	nr_seq_inconsistencia	= 11
						and	a.dt_liberacao is not null
						and	rownum			= 1;
						
						if	(qt_inconsistencia_w = 0) then
							ie_cons_saldo_w		:= 'S';
							ie_gerou_inconsist_w	:= 'S';
						end if;
					end if;
					
					if	(qt_tit_cobranca_w = 0) and
						(vl_cobranca_w <> vetor_titulos_consist.vl_titulo) then
						select	count(1)
						into	qt_inconsistencia_w
						from	inconsistencia_tit_escrit	a
						where	a.nr_seq_titulo_escrit	= nr_seq_tit_rec_cobr_w
						and	nr_seq_inconsistencia	= 12
						and	a.dt_liberacao is not null
						and	rownum			= 1;
						
						if	(qt_inconsistencia_w = 0) then
							ie_cons_reg_w		:= 'S';
							ie_gerou_inconsist_w	:= 'S';
						end if;
					end if;
					
					if	(ie_cons_saldo_w = 'S') or
						(ie_cons_reg_w = 'S') then
						nr_vetor_w						:= nr_vetor_w + 1;
						
						vetor_tit_inconsist_w(nr_vetor_w).nr_titulo_escrit	:= nr_seq_tit_rec_cobr_w;
						vetor_tit_inconsist_w(nr_vetor_w).ie_cons_saldo		:= ie_cons_saldo_w;
						vetor_tit_inconsist_w(nr_vetor_w).ie_cons_reg		:= ie_cons_reg_w;
					end if;
				end if;
				end;
			end loop;
			close c_titulos_consistencia;
			
			if	(ie_gerou_inconsist_w = 'S') then
				rollback;
			end if;
			
			i	:= 0;
			
			for i in 1..vetor_tit_inconsist_w.count loop
				begin
				if	(vetor_tit_inconsist_w(i).ie_cons_saldo = 'S') then
					gerar_inconsist_tit_escrit(	vetor_tit_inconsist_w(i).nr_titulo_escrit,
									11,
									cd_estabelecimento_w,
									nm_usuario_p);
				end if;
				
				if	(vetor_tit_inconsist_w(i).ie_cons_reg = 'S') then
					gerar_inconsist_tit_escrit(	vetor_tit_inconsist_w(i).nr_titulo_escrit,
									12,
									cd_estabelecimento_w,
									nm_usuario_p);
				end if;
				end;
			end loop;
		
		end if;
	end if;

	/* altera observa��o e insere o hist�rico na cobran�a */
	open C05;
	loop
	fetch C05 into
		nr_titulo_w,
		nr_seq_hist_cob_w,
		ds_observacao_titulo_w,
		nr_cobranca_w;
	exit when C05%notfound;

		update	titulo_receber
		set	ds_observacao_titulo	= ds_observacao_titulo_w
		where	nr_titulo		= nr_titulo_w;

		insert into cobranca_historico
			(ds_historico,
			dt_atualizacao,
			dt_historico,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_historico,
			nr_sequencia)
		values	(ds_observacao_titulo_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_cobranca_w,
			nr_seq_hist_cob_w,
			cobranca_historico_seq.nextval);

	end loop;
	close C05;
	
	envia_comunic_baixa_cobr(nr_seq_cobranca_p,nm_usuario_p,'N');
	end;
end if;

if	(ie_acao_p = 'I') then
	-- OS 509083, deixar esta rotina sempre por �ltimo
	baixar_tit_cobr_amortizacao(	nr_seq_cobranca_p,
					'N',
					cd_estabelecimento_w,
					nm_usuario_p);
end if;

if	(nvl(ie_commit_p, 'S') = 'S') then
	commit;
end if;

end Baixar_Titulos_Cobranca_Escrit;
/
