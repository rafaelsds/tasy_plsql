create or replace
procedure pls_atualizar_coparticipacoes
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_lib_copartic_w		pls_lib_coparticipacao.nr_sequencia%type;
nr_seq_conta_coparticipacao_w	pls_lib_coparticipacao.nr_seq_conta_coparticipacao%type;
nr_seq_conta_proc_w		pls_conta_coparticipacao.nr_seq_conta_proc%type;
nr_seq_conta_mat_w		pls_conta_coparticipacao.nr_seq_conta_mat%type;

vl_coparticipacao_w		pls_conta_coparticipacao.vl_coparticipacao%type;
vl_coparticipacao_unit_w	pls_conta_coparticipacao.vl_coparticipacao_unit%type;
vl_base_copartic_w		pls_conta_coparticipacao.vl_base_copartic%type;
qt_liberada_copartic_w		pls_conta_coparticipacao.qt_liberada_copartic%type;
tx_coparticipacao_w		pls_conta_coparticipacao.tx_coparticipacao%type;
vl_liberado_w			pls_conta_proc.vl_liberado%type;
cd_procedimento_w		pls_conta_proc.cd_procedimento%type;
ie_origem_proced_w		pls_conta_proc.ie_origem_proced%type;
nr_seq_material_w		pls_conta_mat.nr_seq_material%type;
nr_seq_prestador_exec_w		pls_conta.nr_seq_prestador_exec%type;
nr_seq_prestador_atend_w	pls_conta.nr_seq_prestador%type;
nr_seq_segurado_w		pls_conta.nr_seq_segurado%type;
dt_atendimento_referencia_w			pls_conta.dt_atendimento_referencia%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		nr_seq_conta_coparticipacao
	from	pls_lib_coparticipacao
	where	nr_seq_lote	= nr_seq_lote_p;

begin

open C01;
loop
fetch C01 into
	nr_seq_lib_copartic_w,
	nr_seq_conta_coparticipacao_w;
exit when C01%notfound;
	begin
	select	nr_seq_conta_proc,
		nr_seq_conta_mat
	into	nr_seq_conta_proc_w,
		nr_seq_conta_mat_w
	from	pls_conta_coparticipacao
	where	nr_sequencia	= nr_seq_conta_coparticipacao_w;
	
	if	(nr_seq_conta_proc_w is not null) then
		select	a.vl_coparticipacao,
			a.vl_coparticipacao_unit,
			a.vl_base_copartic,
			a.qt_liberada_copartic,
			nvl(c.vl_liberado,0),
			c.cd_procedimento,
			b.nr_seq_prestador_exec,
			b.nr_seq_prestador,
			b.nr_seq_segurado,
			c.ie_origem_proced,
			a.tx_coparticipacao,
			b.dt_atendimento_referencia,
			b.nr_sequencia
		into	vl_coparticipacao_w,
			vl_coparticipacao_unit_w,
			vl_base_copartic_w,
			qt_liberada_copartic_w,
			vl_liberado_w,
			cd_procedimento_w,
			nr_seq_prestador_exec_w,
			nr_seq_prestador_atend_w,
			nr_seq_segurado_w,
			ie_origem_proced_w,
			tx_coparticipacao_w,
			dt_atendimento_referencia_w,
			nr_seq_conta_w
		from	pls_conta_proc			c,
			pls_conta			b,
			pls_conta_coparticipacao	a
		where	c.nr_sequencia	= a.nr_seq_conta_proc
		and	b.nr_sequencia	= c.nr_seq_conta
		and	a.nr_sequencia	= nr_seq_conta_coparticipacao_w;
	elsif	(nr_seq_conta_mat_w is not null) then
		select	a.vl_coparticipacao,
			a.vl_coparticipacao_unit,
			a.vl_base_copartic,
			a.qt_liberada_copartic,
			nvl(c.vl_liberado,0),
			b.nr_seq_prestador_exec,
			b.nr_seq_prestador,
			b.nr_seq_segurado,
			c.nr_seq_material,
			a.tx_coparticipacao,
			b.dt_atendimento_referencia,
			b.nr_sequencia
		into	vl_coparticipacao_w,
			vl_coparticipacao_unit_w,
			vl_base_copartic_w,
			qt_liberada_copartic_w,
			vl_liberado_w,
			nr_seq_prestador_exec_w,
			nr_seq_prestador_atend_w,
			nr_seq_segurado_w,
			nr_seq_material_w,
			tx_coparticipacao_w,
			dt_atendimento_referencia_w,
			nr_seq_conta_w
		from	pls_conta_mat			c,
			pls_conta			b,
			pls_conta_coparticipacao	a
		where	c.nr_sequencia	= a.nr_seq_conta_mat
		and	b.nr_sequencia	= c.nr_seq_conta
		and	a.nr_sequencia	= nr_seq_conta_coparticipacao_w;
	end if;
	
	delete	from	pls_coparticipacao_critica
	where	nr_seq_lib_copartic	= nr_seq_lib_copartic_w;
	
	update	pls_lib_coparticipacao
	set	ie_critica_pendente		= 'N',
		vl_coparticipacao		= vl_coparticipacao_w,
		vl_coparticipacao_unit		= vl_coparticipacao_unit_w,
		vl_base_copartic		= vl_base_copartic_w,
		qt_liberada_copartic		= qt_liberada_copartic_w,
		tx_coparticipacao		= tx_coparticipacao_w,
		vl_liberado			= vl_liberado_w,
		cd_procedimento			= cd_procedimento_w,
		ie_origem_proced		= ie_origem_proced_w,
		nr_seq_material			= nr_seq_material_w,
		nr_seq_prestador_exec		= nr_seq_prestador_exec_w,
		nr_seq_prestador_atend		= nr_seq_prestador_atend_w,
		nr_seq_segurado			= nr_seq_segurado_w,
		dt_emissao			= dt_atendimento_referencia_w,
		nr_seq_conta			= nr_seq_conta_w
	where	nr_sequencia	= nr_seq_lib_copartic_w;
	
	pls_criticar_coparticipacao(nr_seq_lib_copartic_w, nm_usuario_p, cd_estabelecimento_p);
	end;
end loop;
close C01;

commit;

end pls_atualizar_coparticipacoes;
/