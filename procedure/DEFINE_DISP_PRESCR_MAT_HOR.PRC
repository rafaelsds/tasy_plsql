create or replace procedure  DEFINE_DISP_PRESCR_MAT_HOR(	nr_seq_horario_p	number,
					nr_prescricao_p		number,
					nr_seq_material_p	number,
					cd_perfil_p		number,
					nm_usuario_p 		varchar2,
					ie_setor_atendimento_p	varchar2 default 'N',
					ie_setor_lote_p		varchar2 default 'N') is

dt_horario_w			date;
nr_regras_w			Number(5);
cd_material_w			Number(6);
nr_sequencia_w			Number(6);
cd_local_estoque_w		Number(5);
cd_local_estoque_prescr_w	Number(5);
cd_loc_estoque_w		Number(5);
cd_grupo_material_w		number(3);
cd_subgrupo_w			number(3);
cd_classe_material_w		number(5);
cd_setor_atendimento_w		number(5);
cd_estabelecimento_w		number(4);
nr_seq_classif_w		number(14);
nr_seq_regra_disp_w		number(10);
ie_dia_semana_w			varchar2(01);
ie_setor_atual_w		varchar2(01);
ie_via_aplicacao_w		varchar2(10);
dt_liberacao_w			Date;
nr_seq_familia_w		number(10,0);
ie_urgencia_W			Varchar2(1);
ie_motivo_prescricao_w		Varchar2(10);
qt_total_dispensar_w		Number(18,6);
ie_padronizado_w		varchar2(1);
nr_atendimento_w		number(10);
cd_convenio_w			number(5)	:= 0;
ie_tipo_convenio_w		number(2)	:= 0;
ie_feriado_w			varchar2(01);
qt_feriado_w			number(10,0) 	:= 0;
ie_acm_sn_w			varchar2(1);
ie_controlado_w			varchar2(1);
ie_acm_w			varchar2(1);
ie_se_necessario_w		varchar2(1);
cd_unidade_medida_w		varchar2(30);
ie_local_estoque_kit_w		varchar2(1);
ie_local_estoque_dil_w		varchar2(1);
nr_seq_forma_chegada_w		number(10);
ie_tipo_hemodialise_w		varchar2(60);
ie_tipo_atendimento_w		number(3);
ie_dose_especial_w		varchar2(1);
cd_material_generico_w		number(6);
nr_regra_local_disp_w		number(10);
cd_material_comercial_w		material.cd_material%type;
qt_dose_w			prescr_material.qt_dose%type;
qt_volume_w			varchar2(60);
nr_seq_estrut_int_w		regra_local_dispensacao.nr_seq_estrut_int%type;
ie_existe_regra_w		varchar2(15);
cd_intervalo_w			intervalo_prescricao.cd_intervalo%type;
ie_intervalo_hor_w		varchar2(1);
ie_gerar_lote_manipulacao_w	material_diluicao.ie_gerar_lote_manipulacao%type;
nr_seq_material_w		prescr_material.nr_sequencia%type;
ie_classif_urgente_w		classif_lote_disp_far.ie_classif_urgente%type;
ie_so_com_estoque_w 		regra_local_dispensacao.ie_so_com_estoque%type;
cd_local_estoque_ww		regra_local_dispensacao.cd_local_estoque%type;
nr_regra_local_disp_ww		regra_local_dispensacao.nr_sequencia%type;
nr_seq_regra_disp_ww		regra_local_dispensacao.nr_seq_regra_disp%type;
ie_atualiza_regra_local_kit_w	varchar2(1);
nr_seq_kit_w				prescr_material.nr_seq_kit%type;
nr_sequencia_diluicao_w		prescr_material.nr_sequencia_diluicao%type;
nr_seq_recomendacao_w		prescr_material.nr_seq_recomendacao%type;
nr_agrupamento_w		prescr_material.nr_agrupamento%type;
ie_item_superior_w		prescr_material.ie_item_superior%type;
ie_emergencia_w prescr_medica.ie_emergencia%type;
ie_saldo_generico_w		varchar2(1);
ie_type_of_prescription_w   regra_local_dispensacao.ie_type_of_prescription%type := '##'; --JRS712
qt_conv_estoque_consumo_w	material.qt_conv_estoque_consumo%type;
qt_dispensar_w				number(18,6);
qt_total_disp_item_w		number(18,6);
nr_seq_local_pa_w           atendimento_paciente.nr_seq_local_pa%type;
ie_termolabil_w				material.ie_termolabil%type;

cursor C01 IS
	select	c.nr_seq_superior,
		a.cd_material,
		nvl(a.ie_urgencia,'N'),
		dividir(a.qt_total_dispensar,b.qt_conv_estoque_consumo),
		b.qt_conv_estoque_consumo,
		substr(obter_se_material_padronizado(k.cd_estabelecimento,a.cd_material),1,1),
		substr(obter_se_medic_controlado(a.cd_material),1,1),
		nvl(a.ie_acm, 'N'),
		nvl(a.ie_se_necessario, 'N'),
		a.cd_unidade_medida_dose,
		a.ie_via_aplicacao,
		c.nr_seq_classif,
		nvl(C.ie_dose_especial,'N'),
		b.cd_material_generico,
		a.qt_dose,
		obter_volume_dil_regra_local(a.nr_prescricao, a.nr_sequencia),
		a.cd_intervalo,
		c.nr_seq_material,
		a.nr_seq_kit,
		a.nr_sequencia_diluicao,
		a.nr_seq_recomendacao,
		a.nr_agrupamento,
		a.ie_item_superior,
		b.ie_termolabil
	from	material b,
		prescr_material a,
		prescr_mat_hor c,
		prescr_medica k
	where	k.nr_prescricao		= nr_prescricao_p
	and	a.nr_sequencia 		= c.nr_seq_material
	and	a.nr_prescricao 	= c.nr_prescricao
	and	c.nr_sequencia 		= nr_seq_horario_p
	and	k.nr_prescricao		= a.nr_prescricao
	and	a.cd_material		= b.cd_material;

cursor C02 IS
	select	cd_local_estoque,
		nr_sequencia,
		nr_seq_regra_disp,
		nvl(ie_so_com_estoque,'N'),
		nvl(ie_saldo_generico,'N')
	from	regra_local_dispensacao
	where	cd_estabelecimento					= cd_estabelecimento_w
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0))		= nvl(cd_setor_atendimento_w,0)
	and	nvl(cd_grupo_material, nvl(cd_grupo_material_w,0))				= nvl(cd_grupo_material_w,0)
	and	nvl(cd_subgrupo_material, nvl(cd_subgrupo_w,0))					= nvl(cd_subgrupo_w,0)
	and	nvl(cd_classe_material, nvl(cd_classe_material_w,0))			= nvl(cd_classe_material_w,0)
	and	nvl(cd_material, nvl(cd_material_w,0))							= nvl(cd_material_w,0)
	and	nvl(cd_perfil, cd_perfil_p)				= cd_perfil_p
	and	nvl(cd_convenio, cd_convenio_w)									= nvl(cd_convenio_w,0)
	and	nvl(nr_seq_classif, nvl(nr_seq_classif_w,0))		= nvl(nr_seq_classif_w,0)
	and	nvl(ie_motivo_prescricao, nvl(ie_motivo_prescricao_w,'XPT'))	= nvl(ie_motivo_prescricao_w,'XPT')
	and	((ie_motivo_prescr_exc is null) or (ie_motivo_prescr_exc <> ie_motivo_prescricao_w))
	and	((nvl(ie_dose_especial,'N') = 'N') or (nvl(ie_dose_especial,'N') = 'S' and (nvl(ie_dose_especial_w,'N') = 'S')))
	and	nvl(ie_tipo_convenio, ie_tipo_convenio_w)		= ie_tipo_convenio_w
	and (nvl(ie_type_of_prescription, ie_type_of_prescription_w) = ie_type_of_prescription_w OR nvl(pkg_i18n.get_user_locale, 'pt_BR') <> 'ja_JP')  --JRS712
	and	nvl(nr_seq_forma_chegada, nvl(nr_seq_forma_chegada_w,0))		= nvl(nr_seq_forma_chegada_w,0)
	and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0))			= nvl(ie_tipo_atendimento_w,0)
	and	((ie_via_aplicacao is null) or (ie_via_aplicacao = ie_via_aplicacao_w))
	and	((nvl(ie_agora,'N') = ie_urgencia_w) or (nvl(ie_agora,'N') = 'N'))
	and	((nvl(ie_nao_padronizado,'N') = 'N') or (nvl(ie_padronizado_w,'S') = 'N'))
	and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') = ie_controlado_w))
	and	nvl(cd_unidade_medida, cd_unidade_medida_w)	= cd_unidade_medida_w
  and	((nvl(ie_considerar_emergencia,'S') = 'S') or (nvl(ie_emergencia_w,'N') = 'N'))
	--and	((nvl(ie_so_com_estoque,'N') = 'N')  or (obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_w,cd_local_estoque,sysdate) >= qt_total_dispensar_w))
	and	((nvl(nr_seq_familia, nr_seq_familia_w)			= nr_seq_familia_w) or
		 (nr_seq_familia is null))
	and	((ie_dia_semana is null) or
		 (ie_dia_semana = ie_dia_semana_w or ie_dia_semana = 9))
	and	obter_se_lib_prescr_horario(dt_hora_inicio, dt_hora_fim, dt_horario_w) = 'S'
	and	(((nvl(ie_feriado,'N') = ie_feriado_w) and (nvl(ie_feriado,'N') = 'S')) or
		((nvl(ie_feriado,'N') = 'N') and (qt_feriado_w = 0)))
	and	(((nvl(ie_somente_acmsn,'N') = 'G') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S'))) or
		((nvl(ie_somente_acmsn,'N') = 'S') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S'))) or
		((nvl(ie_somente_acmsn,'N') = 'A') and (ie_acm_w = 'S') and (ie_se_necessario_w = 'N')) or
		((nvl(ie_somente_acmsn,'N') = 'C') and (ie_acm_w = 'N') and (ie_se_necessario_w = 'S')) or
		((nvl(ie_somente_acmsn,'N') = 'I') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S')) and (nvl(ie_intervalo_hor_w,'S') = 'S') and (ie_classif_urgente_w <> 'A')) or
		((nvl(ie_somente_acmsn,'N') = 'H') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S')) and (ie_classif_urgente_w = 'A')) or
		(nvl(ie_somente_acmsn,'N') = 'N'))
	and	((nvl(nr_seq_estrut_int, 0) = 0) or ((nvl(nr_seq_estrut_int,0) > 0) and consistir_se_mat_contr_estrut(nr_seq_estrut_int,cd_material_w) = 'S'))
	and	((ie_tipo_hemodialise is null) or (ie_tipo_hemodialise = ie_tipo_hemodialise_w))
	and	(( nvl(ie_considerar_padrao_est,'N') = 'N' or (obter_se_mat_existe_padrao_loc(cd_material_w,cd_local_estoque) = 'S' or
		(nvl(ie_considerar_controlador,'N') = 'S' and obter_se_mat_existe_padrao_loc((select x.cd_material_estoque from material x where x.cd_material = cd_material_w), cd_local_estoque) = 'S') or
		(nvl(ie_considerar_generico,'N') = 'S'
		and ((nvl(ie_considerar_generico_assosc,'N') = 'S' and obter_associados_generico(cd_material_generico_w, cd_local_estoque) = 'S') or
			(nvl(ie_considerar_generico_assosc,'N') = 'N' and obter_se_mat_existe_padrao_loc(cd_material_generico_w, cd_local_estoque) = 'S'))))))
	and 	((nvl(ie_medic_alto_custo,'N') = 'N') or obter_se_mat_alto_custo(cd_material_w, cd_estabelecimento_w) = 'S')
	and	((qt_dose is null) or (qt_dose = qt_dose_w))
	and	((qt_volume is null) or (to_char(qt_volume) = qt_volume_w))
	and	((nvl(ie_gerar_lote_manipulacao, 'A') = 'A') or (nvl(ie_gerar_lote_manipulacao,'S') = nvl(ie_gerar_lote_manipulacao_w, 'S')))
	and  ((nr_seq_localizacao is null) or (nr_seq_localizacao = nr_seq_local_pa_w))
	and  (nvl(ie_medic_termolabil,'N') = 'N' or (nvl(ie_medic_termolabil,'N') = 'S' and nvl(ie_termolabil_w,'N') = 'S'))
	order by nvl(NR_SEQ_PRIORIDADE,999) desc,
		nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(nr_seq_familia,0),
		nvl(ie_via_aplicacao,obter_via_usuario('A')),
		nvl(cd_setor_atendimento,0),
		nvl(ie_agora,'N'),
		nvl(ie_nao_padronizado,'N'),
		nvl(ie_feriado,'N'),
		nvl(ie_dose_especial,'N');

cursor c03 is
	select	cd_material
	from	material
	where	ie_situacao = 'A'
	and	cd_material <> cd_material_w
	and	cd_material_generico = cd_material_w
	union
	select	cd_material
	from	material
	where	ie_situacao = 'A'
	and	cd_material <> obter_material_generico(cd_material_w)
	and	cd_material_generico = obter_material_generico(cd_material_w);

-- Busca regras somente onde possui a estrutura interna informada.
cursor c04 is
	select	cd_local_estoque,
		nr_sequencia,
		nr_seq_regra_disp,
		nvl(ie_so_com_estoque,'N'),
		nvl(ie_saldo_generico,'N')
	from	regra_local_dispensacao
	where	cd_estabelecimento					= cd_estabelecimento_w
	and	nvl(cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w
	and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
	and	nvl(cd_subgrupo_material, cd_subgrupo_w)		= cd_subgrupo_w
	and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
	and	nvl(cd_material, cd_material_comercial_w)		= cd_material_comercial_w
	and	nvl(cd_perfil, cd_perfil_p)				= cd_perfil_p
	and	nvl(cd_convenio, cd_convenio_w)				= cd_convenio_w
	and	nvl(nr_seq_classif, nvl(nr_seq_classif_w,0))		= nvl(nr_seq_classif_w,0)
	and	nvl(ie_motivo_prescricao, nvl(ie_motivo_prescricao_w,'XPT'))	= nvl(ie_motivo_prescricao_w,'XPT')
	and	((ie_motivo_prescr_exc is null) or (ie_motivo_prescr_exc <> ie_motivo_prescricao_w))
	and	((nvl(ie_dose_especial,'N') = 'N') or (nvl(ie_dose_especial,'N') = 'S' and (nvl(ie_dose_especial_w,'N') = 'S')))
	and	nvl(ie_tipo_convenio, ie_tipo_convenio_w)		= ie_tipo_convenio_w
	and (nvl(ie_type_of_prescription, ie_type_of_prescription_w) = ie_type_of_prescription_w OR nvl(pkg_i18n.get_user_locale, 'pt_BR') <> 'ja_JP')  --JRS712
	and	nvl(nr_seq_forma_chegada, nr_seq_forma_chegada_w)	= nr_seq_forma_chegada_w
	and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w)			= ie_tipo_atendimento_w
	and	((ie_via_aplicacao is null) or (ie_via_aplicacao = ie_via_aplicacao_w))
	and	((nvl(ie_agora,'N') = ie_urgencia_w) or (nvl(ie_agora,'N') = 'N'))
	and	((nvl(ie_nao_padronizado,'N') = 'N') or (nvl(ie_padronizado_w,'S') = 'N'))
	and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') = ie_controlado_w))
	and	nvl(cd_unidade_medida, cd_unidade_medida_w)	= cd_unidade_medida_w
  and	((nvl(ie_considerar_emergencia,'S') = 'S') or (nvl(ie_emergencia_w,'N') = 'N'))
	--and	((nvl(ie_so_com_estoque,'N') = 'N')  or (obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_comercial_w,cd_local_estoque,sysdate) >= qt_total_dispensar_w))
	and	((nvl(nr_seq_familia, nr_seq_familia_w) = nr_seq_familia_w) or (nr_seq_familia is null))
	and	((ie_dia_semana is null) or (ie_dia_semana = ie_dia_semana_w or ie_dia_semana = 9))
	and	obter_se_lib_prescr_horario(dt_hora_inicio, dt_hora_fim, dt_horario_w) = 'S'
	and	(((nvl(ie_feriado,'N') = ie_feriado_w) and (nvl(ie_feriado,'N') = 'S')) or
		((nvl(ie_feriado,'N') = 'N') and (qt_feriado_w = 0)))
	and	(((nvl(ie_somente_acmsn,'N') = 'G') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S'))) or
		((nvl(ie_somente_acmsn,'N') = 'S') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S'))) or
		((nvl(ie_somente_acmsn,'N') = 'A') and (ie_acm_w = 'S') and (ie_se_necessario_w = 'N')) or
		((nvl(ie_somente_acmsn,'N') = 'C') and (ie_acm_w = 'N') and (ie_se_necessario_w = 'S')) or
		((nvl(ie_somente_acmsn,'N') = 'I') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S')) and (nvl(ie_intervalo_hor_w,'S') = 'S') and (ie_classif_urgente_w <> 'A')) or
		((nvl(ie_somente_acmsn,'N') = 'H') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S')) and (ie_classif_urgente_w = 'A')) or
		(nvl(ie_somente_acmsn,'N') = 'N'))
	and	((nvl(nr_seq_estrut_int,0) >0) and
		(consistir_se_mat_contr_estrut(nr_seq_estrut_int,cd_material_comercial_w) = 'S'))
	and	((ie_tipo_hemodialise is null) or (ie_tipo_hemodialise = ie_tipo_hemodialise_w))
	and	(( nvl(ie_considerar_padrao_est,'N') = 'N' or (obter_se_mat_existe_padrao_loc(cd_material_w,cd_local_estoque) = 'S' or
		(nvl(ie_considerar_controlador,'N') = 'S' and obter_se_mat_existe_padrao_loc((select x.cd_material_estoque from material x where x.cd_material = cd_material_w), cd_local_estoque) = 'S') or
		(nvl(ie_considerar_generico,'N') = 'S'
		and ((nvl(ie_considerar_generico_assosc,'N') = 'S' and obter_associados_generico(cd_material_generico_w, cd_local_estoque) = 'S') or
		(nvl(ie_considerar_generico_assosc,'N') = 'N' and obter_se_mat_existe_padrao_loc(cd_material_generico_w, cd_local_estoque) = 'S'))))))
	and 	((nvl(ie_medic_alto_custo,'N') = 'N') or obter_se_mat_alto_custo(cd_material_w, cd_estabelecimento_w) = 'S')
	and	((qt_dose is null) or (qt_dose = qt_dose_w))
	and	((qt_volume is null) or (to_char(qt_volume) = qt_volume_w))
	and	((nvl(ie_gerar_lote_manipulacao, 'A') = 'A') or (nvl(ie_gerar_lote_manipulacao,'S') = nvl(ie_gerar_lote_manipulacao_w, 'S')))
	and  ((nr_seq_localizacao is null) or (nr_seq_localizacao = nr_seq_local_pa_w))
	and  (nvl(ie_medic_termolabil,'N') = 'N' or (nvl(ie_medic_termolabil,'N') = 'S' and nvl(ie_termolabil_w,'N') = 'S'))
	order by nvl(NR_SEQ_PRIORIDADE,999) desc,
		nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(nr_seq_familia,0),
		nvl(ie_via_aplicacao,obter_via_usuario('A')),
		nvl(cd_setor_atendimento,0),
		nvl(ie_agora,'N'),
		nvl(ie_nao_padronizado,'N'),
		nvl(ie_feriado,'N'),
		nvl(ie_dose_especial,'N');

cursor c99 is
	select	distinct nr_seq_estrut_int
	from	regra_local_dispensacao
	where	nvl(nr_seq_estrut_int,0) > 0
	and	cd_estabelecimento = cd_estabelecimento_w;

begin

---
/*IF upper(nm_usuario_p) = upper('lraju') Then
   ie_type_of_prescription_w := 'INPAT';
end if;*/
---

obter_Param_Usuario(924,799,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_setor_atual_w);

select	max(dt_horario)
into	dt_horario_w
from	prescr_mat_hor
where	nr_sequencia = nr_seq_horario_p;

select	max(cd_setor_atendimento),
	max(nvl(cd_estabelecimento, 1)),
	max(nvl(nvl(dt_liberacao_medico, dt_liberacao), sysdate)),
	max(nvl(cd_local_estoque, 0)),
	max(nvl(nr_atendimento, 0)),
	max(ie_motivo_prescricao),
  nvl(max(ie_emergencia), 'N')
into	cd_setor_atendimento_w,
	cd_estabelecimento_w,
	dt_liberacao_w,
	cd_local_estoque_prescr_w,
	nr_atendimento_w,
	ie_motivo_prescricao_w,
  ie_emergencia_w
from	prescr_medica
where	nr_prescricao		= nr_prescricao_p;

select  max(ie_tipo_hemodialise)
into	ie_tipo_hemodialise_w
from	prescr_solucao a,
	prescr_material b,
	hd_prescricao c
where 	b.nr_sequencia_solucao		= a.nr_seq_solucao
and	a.nr_prescricao		   	= b.nr_prescricao
and	a.nr_seq_dialise	   	= c.nr_sequencia
and	b.nr_prescricao 	   	= nr_prescricao_p
and 	nvl(ie_hemodialise,'N')		= 'S';

if	(ie_setor_atual_w = 'S') then
	cd_local_estoque_prescr_w	:= null;
	cd_setor_atendimento_w		:= obter_setor_atendimento(nr_atendimento_w);
elsif	(ie_setor_atual_w = 'R') then
	cd_local_estoque_prescr_w	:= null;
	cd_setor_atendimento_w		:= Obter_Setor_Atendimento_lib(nr_atendimento_w);
elsif	(ie_setor_atual_w = 'I') then
	cd_local_estoque_prescr_w := null;
	cd_setor_atendimento_w := obter_setor_atepacu(obter_atepacu_paciente(nr_atendimento_w,'IA'),0);
elsif	(ie_setor_atual_w = 'N') and
	(ie_setor_atendimento_p = 'S') and
	(ie_setor_lote_p = 'S') and
	(nr_atendimento_w > 0) then
	cd_local_estoque_prescr_w	:= null;
	cd_setor_atendimento_w		:= obter_setor_atendimento(nr_atendimento_w);
end if;

if	(nr_atendimento_w > 0) then

	select	nvl(max(ie_tipo_convenio), 0),
		nvl(max(nr_seq_forma_chegada), 0),
		nvl(max(ie_tipo_atendimento),0),
		nvl(max(nr_seq_local_pa),0)
	into	ie_tipo_convenio_w,
		nr_seq_forma_chegada_w,
		ie_tipo_atendimento_w,
		nr_seq_local_pa_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;

	cd_convenio_w	:= nvl(obter_convenio_atendimento(nr_atendimento_w),0);

end if;

if	(cd_local_estoque_prescr_w > 0) then
	update	prescr_material
	set	cd_local_estoque	= cd_local_estoque_prescr_w,
		nm_usuario		= nm_usuario_p
	where	nr_prescricao		= nr_prescricao_p
	and	nvl(cd_motivo_baixa,0)	= 0
	and	((nvl(nr_seq_material_p,0) = 0) or
		 (nr_sequencia	   	= nr_seq_material_p) or
		 (nr_sequencia_diluicao = nr_seq_material_p)or
		 (nr_seq_kit  		= nr_seq_material_p));

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
else
	begin
	select	max(pkg_date_utils.get_WeekDay(dt_horario_w))
	into	ie_dia_semana_w
	from	dual;

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

	select	max(nvl(decode(obter_se_feriado(cd_estabelecimento_w, dt_horario_w),0,'N','S'),'N'))
	into	ie_feriado_w
	from	dual;

	begin
	select	max(cd_local_estoque)
	into	cd_loc_estoque_w
	from	setor_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_w;
	exception
	when others then
		cd_loc_estoque_w	:= null;
	end;

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

	select	count(*)
	into	nr_regras_w
	from	regra_local_dispensacao
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	(nr_regras_w > 0) then
		begin
		open c01;
		loop
		fetch c01 into
			nr_sequencia_w,
			cd_material_w,
			ie_urgencia_W,
			qt_total_dispensar_w,
			qt_conv_estoque_consumo_w,
			ie_padronizado_w,
			ie_controlado_w,
			ie_acm_w,
			ie_se_necessario_w,
			cd_unidade_medida_w,
			ie_via_aplicacao_w,
			nr_seq_classif_w,
			ie_dose_especial_w,
			cd_material_generico_w,
			qt_dose_w,
			qt_volume_w,
			cd_intervalo_w,
			nr_seq_material_w,
			nr_seq_kit_w,
			nr_sequencia_diluicao_w,
			nr_seq_recomendacao_w,
			nr_agrupamento_w,
			ie_item_superior_w,
			ie_termolabil_w;
		exit when c01%notfound;
        --JRS712 starts
        ie_type_of_prescription_w := '##';
        begin
          select nvl(c.SI_TYPE_OF_PRESCRIPTION,'##') into ie_type_of_prescription_w 
          from prescr_material a, cpoe_material b, cpoe_order_unit c 
            where b.NR_SEQ_CPOE_ORDER_UNIT = c.nr_sequencia 
              and b.nr_sequencia = a.nr_seq_mat_cpoe
              and a.nr_sequencia = nr_sequencia_w
              and a.nr_prescricao = nr_prescricao_p;
        exception
          when others then
            ie_type_of_prescription_w := '##';
        end;
        --JRS712 ends
			begin

			select	max(ie_gerar_lote_manipulacao)
			into	ie_gerar_lote_manipulacao_w
			from	material_diluicao b
			where  	b.cd_material = cd_material_w
			and	exists(
				select	1
				from	prescr_material x
				where	x.nr_prescricao = nr_prescricao_p
				and	x.nr_sequencia_diluicao = nr_seq_material_w
				and	x.nr_seq_mat_diluicao = b.nr_sequencia);

			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

			cd_local_estoque_w := 0;

			select	max(cd_grupo_material),
				max(cd_subgrupo_material),
				max(cd_classe_material),
				max(nr_seq_familia)
			into	cd_grupo_material_w,
				cd_subgrupo_w,
				cd_classe_material_w,
				nr_seq_familia_w
			from	estrutura_material_v
			where 	cd_material	= cd_material_w;

			select	nvl(max('S'),'N')
			into	ie_intervalo_hor_w
			from	intervalo_prescricao
			where	cd_intervalo = cd_intervalo_w
			and	ie_acm <> 'S'
			and	ie_se_necessario <> 'S';

			select	max(ie_classif_urgente)
			into	ie_classif_urgente_w
			from	classif_lote_disp_far
			where	nr_sequencia = nr_seq_classif_w;
			
			qt_dispensar_w := qt_total_dispensar_w;	
	
			begin
				select	nvl(dividir(sum(a.qt_dispensar_hor),qt_conv_estoque_consumo_w),qt_total_dispensar_w)
				into	qt_total_disp_item_w
				from	prescr_mat_hor a
				where 	a.nr_prescricao = nr_prescricao_p
				and 	a.nr_seq_material = nr_seq_material_w;
			exception
			when others then
				qt_dispensar_w := qt_total_dispensar_w;	
			end;
			
			if	(qt_total_disp_item_w <= qt_total_dispensar_w) then
				qt_dispensar_w := qt_total_disp_item_w;
			end if;

			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

			if	(ie_feriado_w = 'S') then
				select	count(*)
				into	qt_feriado_w
				from	regra_local_dispensacao
				where	cd_estabelecimento					= cd_estabelecimento_w
				and	nvl(cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w
				and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
				and	nvl(cd_subgrupo_material, cd_subgrupo_w)		= cd_subgrupo_w
				and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
				and	nvl(cd_material, cd_material_w)				= cd_material_w
				and	nvl(cd_perfil, cd_perfil_p)				= cd_perfil_p
				and	nvl(cd_convenio, cd_convenio_w)				= cd_convenio_w
				and	nvl(nr_seq_classif, nvl(nr_seq_classif_w,0))		= nvl(nr_seq_classif_w,0)
				and	((ie_via_aplicacao is null) or (ie_via_aplicacao 	= ie_via_aplicacao_w))
				and (nvl(ie_type_of_prescription, ie_type_of_prescription_w) = ie_type_of_prescription_w OR nvl(pkg_i18n.get_user_locale, 'pt_BR') <> 'ja_JP')  --JRS712
				and	nvl(ie_tipo_convenio, ie_tipo_convenio_w)		= ie_tipo_convenio_w
				and	nvl(nr_seq_forma_chegada, nr_seq_forma_chegada_w)	= nr_seq_forma_chegada_w
				and	((nvl(ie_agora,'N') = ie_urgencia_w) or (nvl(ie_agora,'N') = 'N'))
				and	nvl(ie_motivo_prescricao, nvl(ie_motivo_prescricao_w,'XPT'))	= nvl(ie_motivo_prescricao_w,'XPT')
				and	((ie_motivo_prescr_exc is null) or (ie_motivo_prescr_exc <> ie_motivo_prescricao_w))
				and	((nvl(ie_nao_padronizado,'N') = 'N') or (nvl(ie_padronizado_w,'S') = 'N'))
				and	((nvl(ie_controlado,'A') = 'A') or (nvl(ie_controlado, 'A') = ie_controlado_w))
        and	((nvl(ie_considerar_emergencia,'S') = 'S') or (nvl(ie_emergencia_w,'N') = 'N'))
				and	((nvl(ie_so_com_estoque,'N') = 'N')  or (obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_w,cd_local_estoque,sysdate) >= qt_dispensar_w))
				and	((nvl(nr_seq_familia, nr_seq_familia_w)			= nr_seq_familia_w) or
					 (nr_seq_familia is null))
				and	((ie_dia_semana is null) or
					 (ie_dia_semana = ie_dia_semana_w or ie_dia_semana = 9))
				and	obter_se_lib_prescr_horario(dt_hora_inicio, dt_hora_fim, dt_horario_w) = 'S'
				and	nvl(ie_feriado,'N') = 'S'
				and	(((nvl(ie_somente_acmsn,'N') = 'G') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S'))) or
					((nvl(ie_somente_acmsn,'N') = 'S') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S'))) or
					((nvl(ie_somente_acmsn,'N') = 'A') and (ie_acm_w = 'S') and (ie_se_necessario_w = 'N')) or
					((nvl(ie_somente_acmsn,'N') = 'C') and (ie_acm_w = 'N') and (ie_se_necessario_w = 'S')) or
					((nvl(ie_somente_acmsn,'N') = 'I') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S')) and (nvl(ie_intervalo_hor_w,'S') = 'S') and (ie_classif_urgente_w <> 'A')) or
					((nvl(ie_somente_acmsn,'N') = 'H') and ((ie_acm_w = 'S') or (ie_se_necessario_w = 'S') or (ie_urgencia_w = 'S')) and (ie_classif_urgente_w = 'A')) or
					(nvl(ie_somente_acmsn,'N') = 'N'))
				and	((nr_seq_estrut_int is null) or
					(nr_seq_estrut_int is not null and consistir_se_mat_contr_estrut(nr_seq_estrut_int,cd_material_w) = 'S'))
				and	((ie_tipo_hemodialise is null) or (ie_tipo_hemodialise = ie_tipo_hemodialise_w))
				and	(nvl(ie_considerar_generico,'N') = 'N' or obter_se_mat_existe_padrao_loc(cd_material_generico_w, cd_local_estoque) = 'S')
				and	((qt_dose is null) or (qt_dose = qt_dose_w))
				and	((qt_volume is null) or (to_char(qt_volume) = qt_volume_w))
				and  (nvl(ie_medic_termolabil,'N') = 'N' or (nvl(ie_medic_termolabil,'N') = 'S' and nvl(ie_termolabil_w,'N') = 'S'));
			end if;

			/*  Verifica se existe regra que omaterial se encaixe para a estrutura informada. Rotina desenvolvida para o Sirio na utilizacao de regras onde possui a estrutura interna informada.
			Exemplo: na estrutura esta cadastrado o medicamento generico e na prescricao tambem. Porem o hospital somente tem saldonos medicamentos comerciais.
			Assim quando for localizar a regra, tem que visualizar os medicamentos comerciais. */
			ie_existe_regra_w := 'N';
			open c99;
			loop
			fetch c99 into nr_seq_estrut_int_w;
			exit when c99%notfound;
				begin
				ie_existe_regra_w := consistir_se_mat_contr_estrut(nr_seq_estrut_int_w,cd_material_w);
				if (ie_existe_regra_w = 'S') then
					exit;
				end if;
				end;
			end loop;
			close c99;

			if	(ie_existe_regra_w = 'S') then
				--  Verificar saldo dos materiais comercias
				open c03;
				loop
				fetch c03 into
					cd_material_comercial_w;
				exit when c03%notfound or cd_local_estoque_w > 0;
					begin
					select	max(cd_grupo_material),
						max(cd_subgrupo_material),
						max(cd_classe_material),
						max(nr_seq_familia)
					into	cd_grupo_material_w,
						cd_subgrupo_w,
						cd_classe_material_w,
						nr_seq_familia_w
					from	estrutura_material_v
					where 	cd_material = cd_material_comercial_w;

					open c04;
					loop
					fetch c04 into
						cd_local_estoque_ww,
						nr_regra_local_disp_ww,
						nr_seq_regra_disp_ww,
						ie_so_com_estoque_w,
						ie_saldo_generico_w;
					exit when c04%notfound;
						begin
						if	(ie_so_com_estoque_w = 'N') then
							cd_local_estoque_w := cd_local_estoque_ww;
							nr_seq_regra_disp_w := nr_seq_regra_disp_ww;
							nr_regra_local_disp_w := nr_regra_local_disp_ww;

						elsif	(ie_so_com_estoque_w = 'S') and
								(obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_w,cd_local_estoque_ww,sysdate) >= qt_dispensar_w) then
							cd_local_estoque_w := cd_local_estoque_ww;
							nr_seq_regra_disp_w := nr_seq_regra_disp_ww;
							nr_regra_local_disp_w := nr_regra_local_disp_ww;

						elsif	(ie_saldo_generico_w = 'S') and
								(obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_generico_w,cd_local_estoque_ww,sysdate) >= qt_dispensar_w) then
							cd_local_estoque_w := cd_local_estoque_ww;
							nr_seq_regra_disp_w := nr_seq_regra_disp_ww;
							nr_regra_local_disp_w := nr_regra_local_disp_ww;
						end if;

						end;
					end loop;
					close c04;
					end;
				end loop;
				close c03;
			end if;

			if	(nvl(cd_local_estoque_w,0) = 0) then

				select	max(cd_grupo_material),
					max(cd_subgrupo_material),
					max(cd_classe_material),
					max(nr_seq_familia)
				into	cd_grupo_material_w,
					cd_subgrupo_w,
					cd_classe_material_w,
					nr_seq_familia_w
				from	estrutura_material_v
				where 	cd_material = cd_material_w;

				open c02;
				loop
				fetch c02 into
					cd_local_estoque_ww,
					nr_regra_local_disp_ww,
					nr_seq_regra_disp_ww,
					ie_so_com_estoque_w,
					ie_saldo_generico_w;
				exit when c02%notfound;
					begin
					if	(ie_so_com_estoque_w = 'N') then
						cd_local_estoque_w := cd_local_estoque_ww;
						nr_seq_regra_disp_w := nr_seq_regra_disp_ww;
						nr_regra_local_disp_w := nr_regra_local_disp_ww;

					elsif	(ie_so_com_estoque_w = 'S') and
							(obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_w,cd_local_estoque_ww,sysdate) >= qt_dispensar_w) then
						cd_local_estoque_w := cd_local_estoque_ww;
						nr_seq_regra_disp_w := nr_seq_regra_disp_ww;
						nr_regra_local_disp_w := nr_regra_local_disp_ww;

					elsif	(ie_saldo_generico_w = 'S') and
							(obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_generico_w,cd_local_estoque_ww,sysdate) >= qt_dispensar_w) then
						cd_local_estoque_w := cd_local_estoque_ww;
						nr_seq_regra_disp_w := nr_seq_regra_disp_ww;
						nr_regra_local_disp_w := nr_regra_local_disp_ww;
					end if;

					end;
				end loop;
				close 	c02;
			end if;

			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

			if	(nvl(cd_local_estoque_w, 0) = 0) then
				cd_local_estoque_w := cd_loc_estoque_w;
			end if;

			if	(cd_local_estoque_w > 0) then

				Obter_Param_Usuario(924,673,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_local_estoque_kit_w);
				Obter_Param_Usuario(924,741,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_local_estoque_dil_w);

				--ajuste para pegar a regra local do material superior.
				select	nvl(max(ie_atualiza_regra_local_kit),'N')
				into	ie_atualiza_regra_local_kit_w
				from	parametros_farmacia
				where	cd_estabelecimento	= cd_estabelecimento_w;

				if	(nvl(ie_atualiza_regra_local_kit_w,'N') = 'N') then
					select	nvl(max(nr_regra_local_disp), nr_regra_local_disp_w),
							nvl(max(nr_seq_regra_disp), nr_seq_regra_disp_w)
					into	nr_regra_local_disp_w,
							nr_seq_regra_disp_w
					from	prescr_mat_hor
					where	nr_prescricao	= nr_prescricao_p
					and		nr_seq_material	= nr_sequencia_w
					and		nr_sequencia	<> nr_seq_horario_p
					and		nr_seq_superior	is null;
				end if;

				--Atualiza horarios sem kit e diluicao vinculado de acordo com a regra local.
				update	prescr_mat_hor a
				set	a.cd_local_estoque = cd_local_estoque_w,
					a.nr_regra_local_disp = nr_regra_local_disp_w,
					a.nr_seq_regra_disp = nr_seq_regra_disp_w,
					a.nm_usuario	= nm_usuario_p
				where	a.nr_sequencia	= nr_seq_horario_p
				and	a.nr_prescricao	= nr_prescricao_p
				and	nvl(a.cd_motivo_baixa,0)	= 0
				and	((nr_seq_recomendacao_w is not null) or (ie_local_estoque_kit_w = 'N') or (nr_seq_kit_w is null))
				and	((nr_seq_recomendacao_w is not null) or (ie_local_estoque_dil_w = 'N') or (nr_sequencia_diluicao_w is null));

				--Se o item possuir kit, sistema buscara o local de estoque do item principal.
				if	(nvl(ie_local_estoque_kit_w,'N') = 'S') then
					if	(nvl(nr_seq_kit_w,0) > 0) and
						(nvl(nr_seq_recomendacao_w,0) = 0) then

						select	nvl(max(cd_local_estoque),cd_local_estoque_w)
						into	cd_local_estoque_w
						from	prescr_mat_hor
						where	nr_prescricao	= nr_prescricao_p
						and	nr_sequencia	<> nr_seq_horario_p
						and	nr_seq_material	= nr_seq_kit_w
						and	dt_horario	= dt_horario_w
						and	nr_seq_superior	is null;

						update	prescr_mat_hor a
						set	a.cd_local_estoque = cd_local_estoque_w,
							a.nr_regra_local_disp = nr_regra_local_disp_w,
							a.nr_seq_regra_disp = nr_seq_regra_disp_w,
							a.nm_usuario	= nm_usuario_p
						where	a.nr_sequencia	= nr_seq_horario_p
						and	a.nr_prescricao	= nr_prescricao_p
						and	nvl(a.cd_motivo_baixa,0)	= 0
						and 	nr_seq_kit_w is not null
						and 	nr_sequencia_diluicao_w is null;

					elsif	(nvl(nr_agrupamento_w,0) > 0) and
						(nvl(ie_item_superior_w,'N') = 'N') then

						select	nvl(max(a.cd_local_estoque),cd_local_estoque_w)
						into	cd_local_estoque_w
						from	prescr_mat_hor a,
							prescr_material b
						where	a.nr_prescricao = b.nr_prescricao
						and 	a.nr_seq_material = b.nr_sequencia
						and 	a.nr_prescricao	= nr_prescricao_p
						and	a.nr_sequencia	<> nr_seq_horario_p
						and	nvl(b.ie_item_superior,'N') = 'S'
						and	b.nr_agrupamento = nr_agrupamento_w
						and	a.dt_horario	= dt_horario_w
						and	a.nr_seq_superior is null;

						update	prescr_mat_hor a
						set	a.cd_local_estoque = cd_local_estoque_w,
							a.nr_regra_local_disp = nr_regra_local_disp_w,
							a.nr_seq_regra_disp = nr_seq_regra_disp_w,
							a.nm_usuario	= nm_usuario_p
						where	a.nr_sequencia	= nr_seq_horario_p
						and	a.nr_prescricao	= nr_prescricao_p
						and	nvl(a.cd_motivo_baixa,0)	= 0
						and 	nr_agrupamento_w is not null
						and 	nr_seq_kit_w is null
						and 	nr_sequencia_diluicao_w is null;
					end if;

					/*SO 2091690  kit gerado antes do principal*/
					if	(nvl(cd_local_estoque_w,0) > 0) then
						update	prescr_mat_hor a
						set	a.cd_local_estoque = cd_local_estoque_w,
							a.nr_regra_local_disp = nr_regra_local_disp_w,
							a.nr_seq_regra_disp = nr_seq_regra_disp_w,
							a.nm_usuario	= nm_usuario_p
						where	a.nr_prescricao	= nr_prescricao_p
						and	nr_seq_superior	= nr_seq_material_w
						and	dt_horario	= dt_horario_w
						and 	nvl(cd_local_estoque,0) <> cd_local_estoque_w
						and 	nr_sequencia_diluicao_w is null
						and	nvl(a.cd_motivo_baixa,0) = 0;

						if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
						update	prescr_mat_hor a
						set	nr_seq_turno	= nvl(Obter_turno_horario_prescr(cd_estabelecimento_w, cd_setor_atendimento_w, to_char(dt_horario_w,'hh24:mi'), cd_local_estoque_w),nr_seq_turno)
						where	a.nr_prescricao	= nr_prescricao_p
						and	nr_seq_superior	= nr_seq_material_w
						and	dt_horario	= dt_horario_w
						and 	nr_sequencia_diluicao_w is null
						and	nvl(a.cd_motivo_baixa,0) = 0;

					end if;
				end if;

				--Se item for uma diluiacao, sistema buscara o local de estoque do item principal.
				if	(nvl(ie_local_estoque_dil_w,'N') = 'S') and
					(nvl(nr_sequencia_diluicao_w,0) > 0) then

					select	nvl(max(cd_local_estoque),cd_local_estoque_w)
					into	cd_local_estoque_w
					from	prescr_mat_hor
					where	nr_prescricao	= nr_prescricao_p
					and		nr_sequencia	<> nr_seq_horario_p
					and		nr_seq_material	= nr_sequencia_diluicao_w
					and		dt_horario		= dt_horario_w
					and		nr_seq_superior	is null;

					update	prescr_mat_hor a
					set		a.cd_local_estoque = cd_local_estoque_w,
							a.nr_regra_local_disp = nr_regra_local_disp_w,
							a.nr_seq_regra_disp = nr_seq_regra_disp_w,
							a.nm_usuario	= nm_usuario_p
					where	a.nr_sequencia	= nr_seq_horario_p
					and		a.nr_prescricao	= nr_prescricao_p
					and		nvl(a.cd_motivo_baixa,0)	= 0
					and 	nr_sequencia_diluicao_w is not null;
				end if;

				if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
			end if;
			end;
		end loop;
		close 	c01;
		end;
	else
		begin
			if	(cd_loc_estoque_w > 0) then
				update	prescr_mat_hor
				set	cd_local_estoque = cd_loc_estoque_w,
					nm_usuario	= nm_usuario_p
				where	nr_sequencia	= nr_seq_horario_p
				and	nvl(cd_motivo_baixa,0)	= 0;

				if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
			end if;
		end;
	end if;
	end;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end define_disp_prescr_mat_hor;
/
