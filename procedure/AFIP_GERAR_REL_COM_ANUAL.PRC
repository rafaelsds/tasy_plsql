/*create table w_aval_rel(
nr_ordem number(10),
ds_titulo varchar2(255),
ds_conclusao varchar2(255),
ds_conclusao_ant varchar2(255),
ds_orientacao varchar2(255),
dt_registro	date,
nm_usuario varchar2(20));
*/

/*   ANTES DE RODAR, VERIFICAR SE EXISTE A TABELA w_aval_rel NO BANCO*/

create or replace
procedure AFIP_gerar_rel_com_anual(	nr_atendimento_p	number,
				nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);
ds_conclusao_w		varchar2(255);
ds_orientacao_w		varchar2(255);
ds_conclusao_ant_w	varchar2(255);
ds_titulo_w		varchar2(255);
dt_avaliacao_w		date;
nr_seq_ant_w		number(10);
nr_atend_ant_w		number(10);

	procedure gravar_texto_aval(	nr_ordem_P	number,
					ds_titulo_p	varchar2,
					ds_conclusao_p	varchar2,
					ds_orientacao_p	varchar2,
					ds_conclusao_ant_p	varchar2,
					dt_registro_p	date,
					nm_usuario_aval_p	varchar2) is
		begin
		
		insert into w_aval_rel(	nr_ordem,
					ds_titulo,
					ds_conclusao,
					ds_orientacao,
					ds_conclusao_ant,
					dt_registro,
					nm_usuario)
			values(		nr_ordem_p,
					ds_titulo_p,
					ds_conclusao_p,
					ds_orientacao_p,
					ds_conclusao_ant_p,
					dt_registro_p,
					nm_usuario_aval_p);
		
		commit;
		
		end;



begin
delete from w_aval_rel 
where nm_usuario = nm_usuario_p;
commit;

select	max(nr_atendimento)
into	nr_atend_ant_w
from	atendimento_paciente
where	cd_pessoa_fisica = obter_pessoa_atendimento(nr_atendimento_p,'C')
and	nr_atendimento <> nr_atendimento_p;

-- Avalia��o Cl�nica
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 176 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,959),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,961),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;

	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 176 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,959),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(10,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Cardio
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 164 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,12760),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,12773),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 164 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,12760),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(20,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Otorrino
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 178 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,14292),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,14270),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 178 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,14292),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(30,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Oftalmo
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 185 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,1980),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,1981),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 185 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,1980),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(40,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Derma
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 155 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,1760),1,255)) ds_conclusao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 155 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) then
		select	max(substr(aval(nr_sequencia,1760),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(50,'',ds_conclusao_w,'',ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Genico
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 205 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,2663),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,2665),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 205 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,2663),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(60,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Uro
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 188 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,2351),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,2352),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 188 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,2351),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(70,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- Avalia��o Procto (Gastro-Proctol�gica)
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 194 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	select	max(substr(aval(nr_sequencia,2493),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,2499),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 194 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,2493),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(80,'',ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;

-- SINOPSE
select	max(nr_sequencia)
into	nr_sequencia_w
from	med_avaliacao_paciente
where	nr_atendimento = nr_atendimento_p
and	nr_seq_tipo_avaliacao = 241 -- Seq da avalia��o
and	dt_liberacao is not null
and	dt_inativacao is null;

if	(nvl(nr_sequencia_w,0) > 0) then
	if	(nvl(nr_atend_ant_w,0) > 0) then
		select	max(nr_sequencia)
		into	nr_seq_ant_w
		from	med_avaliacao_paciente
		where	nr_atendimento = nr_atend_ant_w
		and	nr_seq_tipo_avaliacao = 241 -- Seq da avalia��o
		and	dt_liberacao is not null
		and	dt_inativacao is null;
	end if;

	-- 1
	select	max(substr(aval(nr_sequencia,17348),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17393),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17395),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17393),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(111,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 2
	select	max(substr(aval(nr_sequencia,17350),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17402),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17442),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17402),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(112,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 3
	select	max(substr(aval(nr_sequencia,17352),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17443),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17400),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17443),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(113,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 4
	select	max(substr(aval(nr_sequencia,17354),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17431),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17467),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17431),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(114,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 5
	select	max(substr(aval(nr_sequencia,17404),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17468),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17433),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17468),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(115,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 6
	select	max(substr(aval(nr_sequencia,17332),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17469),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17470),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17469),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(116,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 7
	select	max(substr(aval(nr_sequencia,17334),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17430),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17471),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17430),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(117,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 8
	select	max(substr(aval(nr_sequencia,17336),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17436),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17472),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17436),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(118,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 9
	select	max(substr(aval(nr_sequencia,17341),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17360),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17473),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17360),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(119,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 10
	select	max(substr(aval(nr_sequencia,17343),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17331),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17361),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17331),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(120,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 11
	select	max(substr(aval(nr_sequencia,17345),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17401),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17466),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17401),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(121,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 12
	select	max(substr(aval(nr_sequencia,17347),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17438),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17440),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17438),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(122,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 13
	select	max(substr(aval(nr_sequencia,17338),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17444),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17445),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17444),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(123,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 14
	select	max(substr(aval(nr_sequencia,17447),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17416),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17419),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17416),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(124,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;

	-- 15
	select	max(substr(aval(nr_sequencia,17449),1,255)) ds_titulo,
		max(substr(aval(nr_sequencia,17423),1,255)) ds_conclusao,
		max(substr(aval(nr_sequencia,17425),1,255)) ds_orientacao,
		max(dt_avaliacao)
	into	ds_titulo_w,
		ds_conclusao_w,
		ds_orientacao_w,
		dt_avaliacao_w
	from	med_avaliacao_paciente
	where	nr_sequencia = nr_sequencia_w;
	
	if	(trim(ds_conclusao_w) is not null) or
		(trim(ds_orientacao_w) is not null) then
		select	max(substr(aval(nr_sequencia,17423),1,255)) ds_conclusao
		into	ds_conclusao_ant_w
		from	med_avaliacao_paciente
		where	nr_sequencia = nr_seq_ant_w;
		
		gravar_texto_aval(125,ds_titulo_w,ds_conclusao_w,ds_orientacao_w,ds_conclusao_ant_w,dt_avaliacao_w,nm_usuario_p);
	end if;
end if;


end AFIP_gerar_rel_com_anual;
/