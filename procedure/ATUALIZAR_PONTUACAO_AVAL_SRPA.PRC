create or replace
procedure atualizar_pontuacao_aval_srpa(nr_sequencia_p		number,
										nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_pontuacao_w	number(15,2);

begin

if	(nr_sequencia_p > 0) then
	select	nvl(sum(qt_result),0)
	into	qt_pontuacao_w
	from	srpa_aval_item
	where	nr_seq_avaliacao = nr_sequencia_p;
	

	update	ATEND_AVAL_SRPA
	set		qt_pontuacao = qt_pontuacao_w,
			nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;

	commit;
end if;

end atualizar_pontuacao_aval_srpa;
/