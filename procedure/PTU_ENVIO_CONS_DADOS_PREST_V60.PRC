create or replace
procedure ptu_envio_cons_dados_prest_v60(	cd_unimed_benef_p		ptu_consulta_prestador.cd_unimed_beneficiario%type,
						nm_prestador_p			ptu_consulta_prestador.nm_prestador%type,
						cd_cgc_cpf_p			ptu_consulta_prestador.cd_cgc_cpf%type,
						sg_cons_prof_p			ptu_consulta_prestador.sg_cons_profissional%type,
						nr_cons_prof_p			ptu_consulta_prestador.nr_cons_profissional%type,
						uf_cons_prof_p			ptu_consulta_prestador.uf_cons_profissional%type,
						nr_versao_ptu_p			ptu_consulta_prestador.nr_versao%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						nm_usuario_p			usuario.nm_usuario%type,
						nr_seq_cons_prest_p	out	ptu_consulta_prestador.nr_sequencia%type) is 
			
			
			
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Preparar os dados para enviar o pedido de Consulta Dados Prestador a Unimed Destino
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_cooperativa_w		varchar2(10);

begin

cd_cooperativa_w	:= pls_obter_unimed_estab(cd_estabelecimento_p);

select	ptu_consulta_prestador_seq.NextVal
into	nr_seq_cons_prest_p
from	dual;

insert	into ptu_consulta_prestador
	(nr_sequencia, cd_transacao, ie_tipo_cliente,
	 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
	 nm_prestador, dt_atualizacao, nm_usuario,
	 cd_cgc_cpf, sg_cons_profissional, nr_cons_profissional,
	 uf_cons_profissional,  nm_usuario_nrec, dt_atualizacao_nrec, 
	 nr_versao)
values	( nr_seq_cons_prest_p, '00418', 'U',
	 cd_cooperativa_w, cd_unimed_benef_p, nr_seq_cons_prest_p,
	 nm_prestador_p, sysdate, nm_usuario_p,
	 cd_cgc_cpf_p, sg_cons_prof_p, nr_cons_prof_p,
	 uf_cons_prof_p, nm_usuario_p, sysdate, 
	 nr_versao_ptu_p);	

commit;

end ptu_envio_cons_dados_prest_v60;
/