create or replace
procedure cpoe_gerar_proc_glic( nr_seq_procedimento_p  number,
                nr_seq_protocolo_p    number,
                nm_usuario_p      varchar2,
                nr_atendimento_p    number,
                cd_pessoa_fisica_p    varchar2,
                ie_comitar_p      varchar2 default 'S',
                nr_seq_proc_interno_p number default null
                ) is

nr_sequencia_w          cpoe_proc_glic.nr_sequencia%type;      
qt_glic_inic_w            prescr_proc_glic.qt_glic_inic%type;
qt_glic_fim_w             prescr_proc_glic.qt_glic_fim%type;   
qt_ui_insulina_w          prescr_proc_glic.qt_ui_insulina%type;  
qt_glicose_w              prescr_proc_glic.qt_glicose%type;  
ds_sugestao_w             prescr_proc_glic.ds_sugestao%type; 
qt_minutos_medicao_w      prescr_proc_glic.qt_minutos_medicao%type;   
qt_ui_insulina_int_w      prescr_proc_glic.qt_ui_insulina_int%type;        
      
/* obter niveis protocolo */
cursor c01 is
select  qt_glic_inic,
  qt_glic_fim,
  qt_ui_insulina,
  qt_ui_insulina_int,
  qt_glicose,
  ds_sugestao,
  qt_minutos_medicao
from  pep_prot_glic_nivel
where  nr_seq_protocolo = nr_seq_protocolo_p
and	nvl((select max(IE_TIPO) 
          from PEP_PROTOCOLO_GLICEMIA 
         where NR_SEQUENCIA = nr_seq_protocolo_p),'C') = (select decode(max(ie_ctrl_glic),'CIG','I','C') 
                                                            from	proc_interno 
                                                           where nr_sequencia = nr_seq_proc_interno_p)
order by
  nr_sequencia;      

begin

open C01;
loop
fetch C01 into  
      qt_glic_inic_w,
      qt_glic_fim_w,
      qt_ui_insulina_w,
      qt_ui_insulina_int_w,
      qt_glicose_w,
      ds_sugestao_w,
      qt_minutos_medicao_w;
exit when C01%notfound;
  begin
  
  select  cpoe_proc_glic_seq.nextval
  into  nr_sequencia_w
  from  dual;

  insert into cpoe_proc_glic(
          nr_sequencia,
          nr_seq_procedimento,
          nr_seq_protocolo,
          qt_glic_inic,
          qt_glic_fim,
          qt_ui_insulina,
          qt_glicose,
          ds_sugestao,
          qt_minutos_medicao,
          qt_ui_insulina_int,
          nr_seq_proc_edit,
          nr_atendimento,
          nm_usuario,
          nm_usuario_nrec,
          dt_atualizacao,
          dt_atualizacao_nrec,
          cd_pessoa_fisica)
        values(
          nr_sequencia_w,
          nr_seq_procedimento_p,
          nr_seq_protocolo_p,
          qt_glic_inic_w,
          qt_glic_fim_w,
          qt_ui_insulina_w,
          qt_glicose_w,
          ds_sugestao_w,
          qt_minutos_medicao_w,
          qt_ui_insulina_int_w,
          nr_seq_procedimento_p,
          nr_atendimento_p,
          nm_usuario_p,
          nm_usuario_p,
          sysdate,
          sysdate,
          cd_pessoa_fisica_p);
  
  end;
end loop;
close C01;

  
if  (nvl(ie_comitar_p,'S') =  'S') then
  commit;  
end if;

end cpoe_gerar_proc_glic;
/
