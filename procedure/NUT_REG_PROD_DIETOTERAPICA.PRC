create or replace
procedure nut_reg_prod_dietoterapica(	nr_locate_p	Varchar2,
					ie_agrupador_p Varchar2	default null) is 
			
nr_sequencia_w		nut_producao_conduta_item.nr_sequencia%type;
			
Cursor C01 is
select	distinct a.nr_sequencia
from 	nut_producao_conduta_item a,
	nut_atend_serv_dia_dieta b
where	a.nr_seq_dieta = b.nr_sequencia 
and	substr(nr_seq_prod_cond||decode(a.dt_producao,'','N','S')||a.cd_material||b.cd_unidade_medida_dosa||fu_obter_itens_adic_prod(a.nr_sequencia,'NR'),1,250) =  nr_locate_p
and	((ie_agrupador_p is null) or (a.ie_agrupador = ie_agrupador_p) or (a.ie_agrupador is null));

begin

if (nr_locate_p is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		
		update	nut_producao_conduta_item
		set	dt_producao		= sysdate,
			nm_usuario_producao	= wheb_usuario_pck.get_nm_usuario
		where	nr_sequencia		= nr_sequencia_w;
		
		end;
	end loop;
	close C01;
	
end if;

commit;

end nut_reg_prod_dietoterapica;
/

