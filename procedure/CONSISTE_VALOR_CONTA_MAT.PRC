CREATE OR REPLACE
PROCEDURE Consiste_Valor_Conta_mat(
			NR_INTERNO_CONTA_P	IN	NUMBER,
			DS_ERRO_P		OUT	VARCHAR2) IS


ds_erro_w		varchar2(80) 	:= '';
cd_convenio_w		number(5);
ie_tipo_atend_w	Number(03,0);
cd_estab_w		Number(05,0);
ie_regra_w		Varchar2(01);

cd_area_w		Number(15);
cd_especialidade_w	Number(15);
cd_Grupo_w		Number(15);
cd_material_w		Number(06,0);
cd_classe_w		Number(06,0);
cd_subgrupo_w		Number(06,0);
cd_categoria_w		varchar2(10);
cd_setor_atendimento_w	number(5,0);
ie_tipo_atend_conta_w	number(03,0);
nr_seq_proc_pacote_w	number(10,0);
nr_sequencia_w		number(10,0);

Cursor C01 is
select	cd_material,
	cd_setor_atendimento,
	nr_seq_proc_pacote,
	nr_sequencia
from	material_atend_paciente
where	nr_interno_conta 	= nr_interno_conta_p
  and	cd_motivo_exc_conta is null
  and	nvl(vl_material,0) = 0;

CURSOR C02 IS
select	ie_regra
from 	regra_valor_conta
where	cd_estabelecimento      			= cd_estab_w
and	nvl(cd_material, cd_material_w)		= cd_material_w
and	nvl(cd_grupo_material, cd_grupo_w)		= cd_grupo_w	
and	nvl(cd_subgrupo_material, cd_subgrupo_w)	= cd_subgrupo_w	
and	nvl(cd_classe_material, cd_classe_w)	= cd_classe_w	
and	nvl(cd_convenio, cd_convenio_w)		= cd_convenio_w
and 	nvl(ie_tipo_atendimento,ie_tipo_atend_w)	= ie_tipo_atend_w
and 	nvl(ie_tipo_atend_conta,ie_tipo_atend_conta_w)	= ie_tipo_atend_conta_w
and	nvl(cd_categoria, cd_categoria_w)	= cd_categoria_w
and 	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
and	((cd_material is not null) or 
	(cd_classe_material is not null) or
	(cd_subgrupo_material is not null) or
	(cd_grupo_material is not null))
order by
	nvl(cd_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_grupo_material,0),
	nvl(ie_tipo_atendimento,0),
	nvl(ie_tipo_atend_conta,0),
	nvl(cd_convenio,0),
	nvl(cd_setor_atendimento,0);

BEGIN

ds_erro_w		:= '';
select	b.ie_tipo_atendimento,
	a.cd_convenio_parametro,
	b.cd_estabelecimento,
	a.cd_categoria_parametro,
	nvl(a.ie_tipo_atend_conta,0)
into	ie_tipo_atend_w,
	cd_convenio_w,
	cd_estab_w,
	cd_categoria_w,
	ie_tipo_atend_conta_w
from	Atendimento_paciente b,
	conta_paciente a
where	a.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento;

OPEN C01;
LOOP
FETCH C01 	into
		cd_material_w,
		cd_setor_atendimento_w,
		nr_seq_proc_pacote_w,
		nr_sequencia_w;
EXIT when c01%notfound;
	begin

	ie_regra_w		:= 'I';
	if	(cd_material_w > 0) then
		select	cd_grupo_material,
			cd_subgrupo_material,
			cd_classe_material
		into	cd_grupo_w,
			cd_subgrupo_w,
			cd_classe_w
		from	estrutura_material_v
		where	cd_material	= cd_material_w;
		OPEN C02;
		LOOP
		FETCH C02 into ie_regra_w;
		EXIT when c02%notfound;
			ie_regra_w	:= ie_regra_w;
		END LOOP;
		CLOSE C02;
		
		if	(ie_regra_w = 'P') and (nvl(nr_seq_proc_pacote_w,0) > 0) then
			ie_regra_w:= 'Z';  --Itens do Pacote pode ter valor zerado
		elsif	(ie_regra_w = 'V') and (nvl(nr_seq_proc_pacote_w,0) > 0) then
			ie_regra_w:= 'I';  --Itens do Pacote n�o podem ter valor zerado		
		elsif	(ie_regra_w = 'D') and (Obter_Desconto_item_conta(nr_sequencia_w ,2) <> 0) then
			ie_regra_w:= 'Z';  --Itens com desconto pode ter valor zerado
		end if;
	end if;

	if	(ie_regra_w <> 'Z') then
		ds_erro_w	:= '3000 ';
	end if;

	end;
END LOOP;
CLOSE C01;

ds_erro_p		:= ds_erro_w;

END Consiste_Valor_Conta_mat;
/
