create or replace
procedure consiste_calcular_depreciacao(
		cd_estabelecimento_p		number,
		dt_mes_ref_p			date,
		ie_consiste_ultimo_mes_p	varchar2,
		ie_consiste_mes_anterior_p	varchar2) is
begin
philips_patrimonio_pck.consistir_desfazer_depreciacao(wheb_usuario_pck.get_nm_usuario,cd_estabelecimento_p,dt_mes_ref_p);
end consiste_calcular_depreciacao;
/
