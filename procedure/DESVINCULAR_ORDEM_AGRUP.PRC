create or replace
procedure desvincular_ordem_agrup(		nr_ordem_compra_p		number,
					nr_ordem_agrup_p			number,
					ie_permite_vincular_liberada_p	varchar2,
					ie_voltar_ordem_pend_p		varchar2,
					nm_usuario_p			Varchar2) is 

begin

update	ordem_compra
set	nr_ordem_agrup	= null
where	nr_ordem_compra = nr_ordem_agrup_p
and	((ie_voltar_ordem_pend_p = 'S') or 
	((ie_voltar_ordem_pend_p = 'N') and (dt_baixa is null)))
and	((ie_permite_vincular_liberada_p = 'S') or 
	((ie_permite_vincular_liberada_p = 'N') and (dt_aprovacao is null) and (dt_liberacao is null)));
	
if	(ie_voltar_ordem_pend_p = 'S') then

	delete	from ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_p
	and	nr_ordem_agrup	= nr_ordem_agrup_p;
	
	update	ordem_compra
	set	dt_baixa = null
	where	nr_ordem_compra = nr_ordem_agrup_p;
end if;

commit;

end desvincular_ordem_agrup;
/