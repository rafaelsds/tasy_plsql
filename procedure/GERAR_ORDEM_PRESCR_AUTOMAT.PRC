create or replace
procedure Gerar_ordem_prescr_automat(
          nr_prescricao_p     Number,
          cd_estabelecimento_p    Number,          
          nm_usuario_p      Varchar2) IS

nr_seq_atendimento_w	paciente_atendimento.nr_seq_atendimento%type;
nr_seq_paciente_w	paciente_atendimento.nr_seq_paciente%type;
cd_medico_resp_w	paciente_setor.cd_medico_resp%type;
cd_pessoa_fisica_w	paciente_setor.cd_pessoa_fisica%type;
nr_prescricao_w		prescr_medica.nr_prescricao%type;
dt_prevista_w		Date;
nr_sequencia_w		can_ordem_prod.nr_sequencia%type;
nr_ordem_prod_w		can_ordem_prod.nr_sequencia%type;
cd_estabelecimento_w	paciente_setor.cd_estabelecimento%type;
cd_estabelecimento_ww	paciente_atendimento.cd_estabelecimento%type;
cd_material_w     Number(6);
nr_seq_prescr_w		prescr_material.nr_sequencia%type;
ie_via_aplicacao_w	prescr_material.ie_via_aplicacao%type;
cd_farmaceutico_ww    Varchar2(10);
cd_farmaceutico_w	Varchar2(10);
ds_precaucao_ordem_w	Varchar2(255);
ds_recomendacao_w	Varchar2(255); 
cd_setor_atendimento_w	paciente_setor.cd_setor_atendimento%type;
qt_min_aplicacao_w    Number(4,0);
qt_hora_aplicacao_w   Number(3,0);
ie_tipo_dosagem_w	Varchar2(3);
qt_dose_diluente_w	Number(15,4);
vl_gotejo_w     	Number(10,1);
nr_agrupamento_w    prescr_material.nr_agrupamento%type;
nr_agrup_ant_w      prescr_material.nr_agrupamento%type := 0;
cd_material_medic_w   Number(10);
nr_sequencia_medic_w   prescr_material.nr_sequencia%type;
qt_dose_medic_w     prescr_material.qt_dose%type;
cd_unidade_medida_dose_w  prescr_material.cd_unidade_medida_dose%type;
nr_seq_item_prescr_w	can_ordem_item_prescr.nr_sequencia%type;
nr_ocorrencia_w		number(15,4);
nm_paciente_w		varchar2(60);
cd_forma_armaz_w	varchar2(255);
cd_estagio_armaz_w	varchar2(255);
qt_estabilidade_w	material_armazenamento.qt_estabilidade%type;
ie_tempo_estab_w	material_armazenamento.ie_tempo_estab%type;
qt_hora_validade_w	material_armazenamento.qt_estabilidade%type;
qt_min_validade_w	material_armazenamento.qt_estabilidade%type;
qt_dias_util_w		prescr_material.qt_dias_util%type;
ie_exige_liberacao_w	paciente_atendimento.ie_exige_liberacao%type;
ie_generico_w		varchar2(1);
ie_gerar_reconstituinte_w varchar2(1);
qt_peso_w		paciente_setor.qt_peso%type;
qt_superf_corporea_w    number(7,3);
nr_seq_ordem_prod_w  prescr_material.nr_seq_ordem_prod%type;
ie_usuario_manipulador_w  varchar2(1);
ie_pre_medicacao_w    prescr_material.ie_pre_medicacao%type;
nr_sequencia_log_w    log_gera_ordem.nr_sequencia%type;
nr_sequencia_inc_w    w_inconsistencias_onco.nr_sequencia%type;
ie_agrupador_w		prescr_material.ie_agrupador%type;
ie_agrup_ant_w		prescr_material.ie_agrupador%type;
ie_grava_aspirado_w	varchar2(1);
qt_volume_aspirado_w	number(15,2);
ie_permite_acm_w	varchar2(1);
ie_bomba_infusao_w	prescr_material.ie_bomba_infusao%type;
nr_sequencia_solucao_w	prescr_material.nr_sequencia_solucao%type;
ds_retorno_ww     varchar2(4000);
dt_chagada_w		paciente_atendimento.dt_chegada%type;
qt_mat_regra_w		number(10);
ds_erro_w     varchar2(255);
qt_dose_w     number(18,6);
nr_atendimento_w	paciente_atendimento.nr_atendimento%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;
ie_via_aplicacao_got_w  prescr_material.ie_via_aplicacao%type;
ie_aplica_gotejamento_w		via_aplicacao.ie_gotejamento%type;
ie_medic_paciente_w		prescr_material.ie_medicacao_paciente%type;
nr_seq_mat_hor_w	prescr_mat_hor.nr_sequencia%type;
qt_regras_estab_central_w	number(10);


Cursor C01 is
  Select  a.nr_seq_atendimento,
		a.nr_seq_paciente,
		b.nr_prescricao,
		nvl(a.dt_real,a.dt_prevista),
		a.ie_exige_liberacao,
		a.cd_estabelecimento,
		a.dt_chegada,
		a.nr_atendimento
from  	paciente_atendimento a,
		prescr_medica b
where 	b.nr_prescricao = a.nr_prescricao
and		b.nr_prescricao = nr_prescricao_p
and		(a.cd_estabelecimento = cd_estabelecimento_p)
and   	a.dt_suspensao is null
and  (obter_se_paciente_obito(obter_pessoa_atendimento(a.nr_atendimento,'C')) <> 'A')
and (not exists (select 1 from w_inconsistencias_onco d  where d.nr_seq_atendimento = a.nr_seq_atendimento))
and a.dt_cancelamento is null
and	b.dt_liberacao_farmacia is not null;


Cursor C02 is
  select  decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
    a.nr_sequencia,
    a.ie_via_aplicacao,
    substr(b.ds_precaucao_ordem,1,255),
    substr(a.ds_observacao,1,255), 
    decode(nvl(a.qt_min_aplicacao,0),0,decode(a.ie_agrupador,4,nvl(Obter_tempo_aplic_sol(nr_prescricao_w,a.nr_sequencia_solucao,'M'),0),0),nvl(a.qt_min_aplicacao,0)),
    decode(nvl(a.qt_hora_aplicacao,0),0,decode(a.ie_agrupador,4,nvl(Obter_tempo_aplic_sol(nr_prescricao_w,a.nr_sequencia_solucao,'H'),0),0),nvl(a.qt_hora_aplicacao,0)),
    decode(obter_um_rep_dispositivo(a.ie_bomba_infusao,a.ie_agrupador),null,decode(a.ie_bomba_infusao,'S','mlh','gtm'),obter_um_rep_dispositivo(a.ie_bomba_infusao,a.ie_agrupador)),
    a.nr_agrupamento,
    DECODE(a.ie_agrupador,4,NVL(obter_etapa_soluc_quimio(nr_prescricao_w,a.nr_sequencia_solucao),1), a.nr_ocorrencia),
    a.qt_dias_util,
    a.ie_pre_medicacao,
    a.ie_agrupador,
    a.ie_bomba_infusao,
    a.nr_sequencia_solucao,
    nvl(a.ie_medicacao_paciente, 'N')	
  from  material b,
    prescr_material a
  where a.nr_prescricao = nr_prescricao_w
  and a.cd_material = b.cd_material
  and a.nr_seq_ordem_prod is null
  and a.ie_via_aplicacao is not null
  and nvl(a.ie_suspenso,'N')  = 'N'
  and   a.dt_suspensao is null
  and ( ((b.ie_mistura = 'S') and (nvl(a.ie_medicacao_paciente, 'N') <> 'S')) or
        ((b.ie_mistura = 'R') and (obter_se_gera_ordem(a.ie_via_aplicacao,a.cd_intervalo, a.qt_dose, a.cd_material, a.cd_unidade_medida,'T', nvl(a.ie_medicacao_paciente, 'N'),nr_atendimento_w) = 'S')) or
		((b.ie_mistura = 'I') and (ie_tipo_atendimento_w = 1) and (nvl(a.ie_medicacao_paciente, 'N') <> 'S'))
	  )
  and   a.ie_agrupador in (1,4)
  and   ((nvl(IE_ACM,'N') = 'N') or (nvl(ie_permite_acm_w,'S') = 'S'))
  and   (obter_se_setor_ordem(ie_acm,obter_setor_atendimento(obter_atendimento_prescr(nr_prescricao))) = 'S')
  and   ( ((qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'S') and (dt_chagada_w is not null)) or
		  (qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'N')
		)
  order by a.nr_agrupamento, a.nr_sequencia;
     

Cursor C03 is
  select  decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),decode(ie_generico_w,'C',obter_medic_regra_onc(a.cd_material,b.cd_material_generico,nr_atendimento_w,ie_tipo_material,b.nr_seq_ficha_tecnica),a.cd_material)),
    a.nr_sequencia,
    a.qt_dose,
    a.cd_unidade_medida_dose
  from  material b,
    prescr_material a
  where a.nr_prescricao   = nr_prescricao_w
  and a.nr_agrupamento  = nr_agrupamento_w
  and   a.ie_agrupador    = ie_agrupador_w
  and a.cd_material   = b.cd_material
  and   a.dt_suspensao is null
  and a.ie_agrupador    in (1,4)
  and   (obter_se_setor_ordem(ie_acm,obter_setor_atendimento(obter_atendimento_prescr(nr_prescricao))) = 'S')
  and   ((nvl(IE_ACM,'N') = 'N') or (nvl(ie_permite_acm_w,'S') = 'S'))
  and a.cd_unidade_medida_dose is not null
  and   (((qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'S') and (dt_chagada_w is not null))
      or (qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'N'));


begin

select log_gera_ordem_seq.nextval
into  nr_sequencia_log_w
from dual;

insert into log_gera_ordem (
  nr_sequencia,
  nm_usuario,
  dt_inicio)
values  (nr_sequencia_log_w,
  nm_usuario_p,
  sysdate);
  
commit; 
  
delete from w_inconsistencias_onco;

commit;


Obter_Param_Usuario(3130,2,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,cd_farmaceutico_w);
Obter_Param_Usuario(3130,22,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_generico_w);
Obter_Param_Usuario(3130,40,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_reconstituinte_w);
Obter_Param_Usuario(3130,142,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_usuario_manipulador_w); 
Obter_Param_Usuario(3130,176,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_grava_aspirado_w);
Obter_Param_Usuario(3130,261,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_permite_acm_w);


if  (cd_farmaceutico_w is not null) then
  select  max(cd_pessoa_fisica)
  into  cd_farmaceutico_ww
  from  pessoa_fisica
  where   cd_pessoa_fisica = cd_farmaceutico_w;

  if  (nvl(cd_farmaceutico_ww,0) = 0) then
    wheb_mensagem_pck.Exibir_Mensagem_Abort(192283);
  end if;
end if;

select	count(*)
into	qt_regras_estab_central_w
from	regra_estab_central_quimio
where	cd_estab_regra = cd_estabelecimento_p;


if (qt_regras_estab_central_w > 0) then
begin

  open C01;
  loop
  fetch C01 into
    nr_seq_atendimento_w,
    nr_seq_paciente_w,
    nr_prescricao_w,
    dt_prevista_w,
    ie_exige_liberacao_w,
    cd_estabelecimento_ww,
    dt_chagada_w,
    nr_atendimento_w;
  exit when C01%notfound;
    begin
    
    select  cd_medico_resp,
      cd_estabelecimento,
      substr(obter_nome_pf(cd_pessoa_fisica),1,60)
    into  cd_medico_resp_w,
      cd_estabelecimento_w,
      nm_paciente_w
    from  paciente_setor
    where nr_seq_paciente = nr_seq_paciente_w;

    if  (cd_medico_resp_w is null) then
      
        select w_inconsistencias_onco_seq.nextval
        into  nr_sequencia_inc_w
        from dual;

        insert into w_inconsistencias_onco (
          nr_seq_atendimento,
          nr_sequencia,
          nm_usuario,
          nr_atendimento,
          ds_informacao)
        values  (nr_seq_atendimento_w,
          nr_sequencia_inc_w,
          nm_usuario_p,
          nr_atendimento_w,
          substr(wheb_mensagem_pck.get_texto (184696, 'NM_PACIENTE_W='||nm_paciente_w),1,2000));
        commit; 
    end if;
    end;    
  end loop;
  Close C01;

  select  nvl(vl_parametro, vl_parametro_padrao)
  into  cd_estagio_armaz_w
  from  funcao_parametro
  where cd_funcao = 3130
  and nr_sequencia  = 10;

  select  nvl(vl_parametro, vl_parametro_padrao)
  into  cd_forma_armaz_w
  from  funcao_parametro
  where cd_funcao = 3130
  and nr_sequencia  = 11;

  open C01;
  loop
  fetch C01 into
    nr_seq_atendimento_w,
    nr_seq_paciente_w,
    nr_prescricao_w,
    dt_prevista_w,
    ie_exige_liberacao_w,
    cd_estabelecimento_ww,
    dt_chagada_w,
    nr_atendimento_w;
  exit when C01%notfound;
    begin

    select  max(cd_pessoa_fisica),
      max(cd_medico_resp),
      max(cd_setor_atendimento),
      max(qt_peso),
      max(round(obter_superficie_corp_red_ped(qt_peso, qt_altura, qt_redutor_sc, cd_pessoa_fisica, nm_usuario),2))
    into  cd_pessoa_fisica_w,
      cd_medico_resp_w,
      cd_setor_atendimento_w,
      qt_peso_w,
      qt_superf_corporea_w
    from  paciente_setor
    where nr_seq_paciente = nr_seq_paciente_w;


    nr_agrup_ant_w  := 0;
    ie_agrup_ant_w  := 0;
    
    select  max(ie_tipo_atendimento)
    into  ie_tipo_atendimento_w
    from  atendimento_paciente
    where nr_atendimento  = nr_atendimento_w;

    OPEN C02;
    LOOP
    FETCH C02 into
      cd_material_w,
      nr_seq_prescr_w,
      ie_via_aplicacao_w,
      ds_precaucao_ordem_w,
      ds_recomendacao_w,
      qt_min_aplicacao_w,
      qt_hora_aplicacao_w,
      ie_tipo_dosagem_w,
      nr_agrupamento_w,
      nr_ocorrencia_w,
      qt_dias_util_w,
      ie_pre_medicacao_w,
      ie_agrupador_w,
      ie_bomba_infusao_w,
      nr_sequencia_solucao_w,
      ie_medic_paciente_w;
    exit when C02%notfound;
      begin
      commit;
      if  (nr_agrup_ant_w <> nr_agrupamento_w) or
        ((nr_agrup_ant_w = nr_agrupamento_w) and
         (ie_agrup_ant_w <> ie_agrupador_w)) then
        begin   

        select  nvl(min(qt_estabilidade),0),
          nvl(min(ie_tempo_estab),'I')
        into  qt_estabilidade_w,
          ie_tempo_estab_w
        from  material_armazenamento
        where cd_material = cd_material_w
        and nr_seq_estagio  = cd_estagio_armaz_w
        and nr_seq_forma  = cd_forma_armaz_w
        and nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0);

        qt_hora_validade_w  := 0;
        qt_min_validade_w := 0;

        if  (ie_tempo_estab_w = 'H') then
          qt_hora_validade_w  := qt_estabilidade_w;
        elsif (ie_tempo_estab_w = 'D') then
          qt_hora_validade_w  := qt_estabilidade_w * 24;
        elsif (ie_tempo_estab_w = 'M') then
          qt_hora_validade_w  := qt_estabilidade_w * 30 * 24;
        elsif (ie_tempo_estab_w = 'A') then
          qt_hora_validade_w  := qt_estabilidade_w * 365 * 24;
        end if;

        nr_agrup_ant_w  := nr_agrupamento_w;
        ie_agrup_ant_w  := ie_agrupador_w;

        select  nvl(max(nr_seq_ordem_prod),0)
        into  nr_seq_ordem_prod_w
        from  prescr_material
        where nr_prescricao = nr_prescricao_w
        and nr_sequencia  = nr_seq_prescr_w;

        while (nr_ocorrencia_w > 0) and
          (nr_seq_ordem_prod_w = 0) LOOP
          begin
          
          nr_ocorrencia_w := nr_ocorrencia_w - 1;

          select  can_ordem_prod_seq.nextval
          into  nr_sequencia_w
          from  dual;

          nr_ordem_prod_w := nr_sequencia_w;

          if (ie_agrupador_w = 4) then
            select  nvl(max(ie_via_aplicacao),ie_via_aplicacao_w)
            into  ie_via_aplicacao_w
            from  prescr_solucao
            where nr_prescricao = nr_prescricao_w
            and nr_seq_solucao = nr_sequencia_solucao_w;

          end if;

          insert into can_ordem_prod(
            nr_sequencia,
            cd_estabelecimento,
            dt_atualizacao,
            nm_usuario,
            cd_pessoa_fisica,
            dt_prevista,
            ie_via_aplicacao,
            qt_hora_validade,
            qt_min_validade,
            qt_hora_aplic,
            qt_min_aplic,
            cd_medico_resp,
            cd_farmaceutico,
            cd_manipulador,
            cd_auxiliar,
            dt_inicio_preparo,
            dt_entrega_setor,
            dt_administracao,
            nr_seq_atendimento,
            nr_prescricao,
            dt_fim_preparo,
            ie_tipo_dosagem,
            ds_precaucao,
            ie_cancelada,
            ds_orientacao_adm,
            cd_setor_paciente,
            ie_conferido,
            qt_dias_util,
            ie_suspensa,
            qt_peso,
            qt_superf_corporea,
            dt_geracao_ordem,
            ie_pre_medicacao,
            ie_bomba_infusao,
            ie_medic_paciente)
          Values(
            nr_sequencia_w,
            nvl(cd_estabelecimento_ww,cd_estabelecimento_p),
            sysdate,
            nm_usuario_p,
            cd_pessoa_fisica_w,
            dt_prevista_w,
            ie_via_aplicacao_w,
            nvl(qt_hora_validade_w,0),
            nvl(qt_min_validade_w,0),
            nvl(qt_hora_aplicacao_w,0),
            nvl(qt_min_aplicacao_w,0),
            cd_medico_resp_w,
            cd_farmaceutico_w,
            decode(ie_usuario_manipulador_w,'S',Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),null),
            null,
            null,
            null,
            null,
            nr_seq_atendimento_w,
            nr_prescricao_w,
            null,
            ie_tipo_dosagem_w,
            ds_precaucao_ordem_w,
            'N',
            ds_recomendacao_w,
            cd_setor_atendimento_w,
            'N',
            qt_dias_util_w,
            ie_exige_liberacao_w,
            qt_peso_w,
            qt_superf_corporea_w,
            sysdate,
            ie_pre_medicacao_w,
            ie_bomba_infusao_w,
            ie_medic_paciente_w);

          commit;

          update  prescr_material 
          set nr_seq_ordem_prod = nr_ordem_prod_w
          where nr_prescricao   = nr_prescricao_w
          and nr_agrupamento    = nr_agrupamento_w
          and   ie_agrupador    = ie_agrupador_w;

          commit;

          OPEN C03;
          LOOP
          FETCH C03 into
            cd_material_medic_w,
            nr_sequencia_medic_w,
            qt_dose_medic_w,
            cd_unidade_medida_dose_w;
          exit when C03%notfound;
            begin
            select  can_ordem_item_prescr_seq.nextval
            into  nr_seq_item_prescr_w
            from  dual;

            select  count(*)
            into  qt_mat_regra_w
            from  regra_geracao_ordem_tot
            where cd_material = cd_material_medic_w;

            if  (qt_mat_regra_w > 0) then
              select  qt_material
              into  qt_dose_medic_w
              from  prescr_material
              where nr_sequencia  = nr_sequencia_medic_w
              and   nr_prescricao = nr_prescricao_w;
            end if;
            
            gravar_log_tasy(0207, 'Gerar_ordem_prescr_automat. '||chr(13)||  
						'nr_sequencia_medic_w: '||nr_sequencia_medic_w||chr(13)||
                        'nr_prescricao_w: '||nr_prescricao_w||chr(13)||
                        'cd_material_medic_w: '||cd_material_medic_w, 'log');
            
            select min  (a.nr_sequencia)
            into 	nr_seq_mat_hor_w
            from    prescr_mat_hor a
            where   a.nr_seq_material = nr_sequencia_medic_w
			and     a.dt_lib_horario is not null
            and     a.nr_prescricao = nr_prescricao_w
            and   a.cd_material = cd_material_medic_w
            and     not exists (select 1 from can_ordem_item_prescr b 
            where   b.nr_seq_mat_hor = a.nr_sequencia);
            
        
            
            insert into can_ordem_item_prescr(
              nr_sequencia,
              dt_atualizacao,
              nm_usuario,
              nr_seq_ordem,
              cd_material,
              qt_dose,
              cd_unidade_medida,
              nr_prescricao,
              nr_seq_prescricao,
              nr_sequencia_diluente,
              nm_usuario_nrec,
              dt_atualizacao_nrec, 
              nr_seq_mat_hor)
            values( nr_seq_item_prescr_w,
              sysdate,
              nm_usuario_p,
              nr_sequencia_w,
              cd_material_medic_w,
              nvl(qt_dose_medic_w,0),
              cd_unidade_medida_dose_w,
              nr_prescricao_w,
              nr_sequencia_medic_w,
              null,
              nm_usuario_p,
              sysdate, 
              nr_seq_mat_hor_w);

            insert into can_ordem_item_prescr(
              nr_sequencia,
              dt_atualizacao,
              nm_usuario,
              nr_seq_ordem,
              cd_material,
              qt_dose,
              cd_unidade_medida,
              nr_prescricao,
              nr_seq_prescricao,
              nr_sequencia_diluente,
              ie_agrupador,
              nm_usuario_nrec,
              dt_atualizacao_nrec,
			  nr_seq_mat_hor)
            select  can_ordem_item_prescr_seq.nextval,
              sysdate,
              nm_usuario_p,
              nr_sequencia_w,
              decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
              nvl(a.qt_dose,0),
              a.cd_unidade_medida_dose,
              nr_prescricao_w,
              a.nr_sequencia,
              nr_seq_item_prescr_w,
              nvl(ie_agrupador_quimio,ie_agrupador),
              nm_usuario_p,
              sysdate,
			  nr_seq_mat_hor_w
            from  material b,
              prescr_material a
            where a.nr_prescricao   = nr_prescricao_w
            and a.nr_sequencia_diluicao = nr_sequencia_medic_w
            and a.cd_material   = b.cd_material
            and a.ie_agrupador    in (3)
            and   a.dt_suspensao    is null
            and a.cd_unidade_medida_dose is not null;

            if  (ie_gerar_reconstituinte_w = 'S') then
              insert into can_ordem_item_prescr(
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                nr_seq_ordem,
                cd_material,
                qt_dose,
                cd_unidade_medida,
                nr_prescricao,
                nr_seq_prescricao,
                nr_sequencia_diluente,
                ie_agrupador,
                nm_usuario_nrec,
                dt_atualizacao_nrec,
				nr_seq_mat_hor)
                select  can_ordem_item_prescr_seq.nextval,
                sysdate,
                nm_usuario_p,
                nr_sequencia_w,
                decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
                nvl(a.qt_dose,0),
                a.cd_unidade_medida_dose,
                nr_prescricao_w,
                a.nr_sequencia,
                nr_seq_item_prescr_w,
                ie_agrupador,
                nm_usuario_p,
                sysdate,
				nr_seq_mat_hor_w
              from  material b,
                prescr_material a
              where a.nr_prescricao   = nr_prescricao_w
              and a.nr_sequencia_diluicao = nr_sequencia_medic_w
              and a.cd_material   = b.cd_material
              and a.ie_agrupador    in (9)
              and a.cd_unidade_medida_dose is not null;

            end if;

            begin
            select  decode(upper(cd_unidade_medida_dose),upper(obter_unid_med_usua('ML')),qt_dose, qt_dose * obter_conversao_unid_med(cd_material,upper(obter_unid_med_usua('ML'))))
            into  qt_dose_diluente_w
            from  prescr_material
            where nr_sequencia_diluicao = nr_seq_prescr_w
            and   nr_prescricao   = nr_prescricao_w
            and   ie_agrupador    = 3;
            exception
            when others then
              qt_dose_diluente_w  := 0;
              ds_erro_w := substr(SQLERRM(sqlcode),1,255);
            end;
            
            select  nvl(max(obter_conversao_ml(cd_material, qt_dose, cd_unidade_medida_dose)),0),
                max(ie_via_aplicacao)
            into  qt_dose_w,
                ie_via_aplicacao_got_w
            from  prescr_material
            where nr_prescricao   = nr_prescricao_w
            and   nr_sequencia  = nr_seq_prescr_w;
            
            ie_aplica_gotejamento_w := 'S';
            
            if (ie_via_aplicacao_got_w is not null) then
              select nvl(ie_gotejamento,'S')
              into   ie_aplica_gotejamento_w
              from   via_aplicacao
              where  ie_via_aplicacao = ie_via_aplicacao_got_w;
            end if;
            
            if (ie_aplica_gotejamento_w = 'S') then
              Calcular_gotejamento_ordem(ie_tipo_dosagem_w,dividir(qt_min_aplicacao_w,60) + qt_hora_aplicacao_w,qt_dose_diluente_w ,vl_gotejo_w);
            else
              vl_gotejo_w := null;
              ie_tipo_dosagem_w := null;
            end if;

            qt_volume_aspirado_w := null;

            if  (ie_grava_aspirado_w = 'S') then
              qt_volume_aspirado_w := obter_volume_aspirado(nr_sequencia_w);
            end if;

            update  can_ordem_prod
            set qt_dosagem          = vl_gotejo_w,
              qt_volume_aspirado  = qt_volume_aspirado_w,
              ie_tipo_dosagem   = ie_tipo_dosagem_w
            where nr_sequencia    = nr_sequencia_w;

            end;
          end loop;
          Close C03;
          end;
        end loop;

        commit;
        end;
      end if;
      end;
      
    end loop;
    Close C02;
    end;
  
  end loop;
  Close C01;

  update  log_gera_ordem
  set   dt_final = sysdate
  where   nr_sequencia = nr_sequencia_log_w;
  commit;
exception
when others then
  update  log_gera_ordem
  set   dt_final = sysdate
  where   nr_sequencia = nr_sequencia_log_w;
  commit;
  
  wheb_mensagem_pck.Exibir_Mensagem_Abort(184695, 'ERRO='||sqlerrm);
end;
end if;

commit;
end Gerar_ordem_prescr_automat;
/
