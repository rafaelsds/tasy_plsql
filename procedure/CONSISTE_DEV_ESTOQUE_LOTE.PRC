create or replace
procedure consiste_dev_estoque_lote(
			nr_devolucao_p		number,
			nr_seq_item_p		number,
			nr_lote_fornec_p	number,
			qt_material_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			ds_retorno_p		out varchar2) is

cd_material_w		number(6);
nr_prescricao_w		number(10);
nr_atendimento_w	number(10);
ie_estoque_lote_w	varchar2(1);
qt_material_w		number(15,4);
ds_retorno_w		varchar2(2000);
ie_valida_lote_w	varchar2(1);

begin

obter_param_usuario(42,78, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_valida_lote_w);

if	(nvl(ie_valida_lote_w,'N') = 'S') then
	begin

	select	b.cd_material,
		b.nr_prescricao,
		a.nr_atendimento
	into	cd_material_w,
		nr_prescricao_w,
		nr_atendimento_w
	from	devolucao_material_pac a,
		item_devolucao_material_pac b
	where	a.nr_devolucao = b.nr_devolucao
	and	a.nr_devolucao = nr_devolucao_p
	and	b.nr_sequencia = nr_seq_item_p;

	exception
		when others then

		cd_material_w		:= 0;
		nr_prescricao_w		:= 0;
		nr_atendimento_w	:= 0;

	end;

	select	nvl(max(ie_estoque_lote),'N')
	into	ie_estoque_lote_w
	from	material_estab
	where	cd_material = cd_material_w
	and	cd_estabelecimento = cd_estabelecimento_p;

	if	(ie_estoque_lote_w = 'S') then

		begin

		select	sum(qt_material)
		into	qt_material_w
		from	material_atend_paciente
		where	nr_seq_lote_fornec = nr_lote_fornec_p
		and	(nr_prescricao = nvl(nr_prescricao_w,0) or nvl(nr_prescricao_w,0) = 0)
		and	nr_atendimento = nr_atendimento_w
		having sum(qt_material) > 0;
		
		exception
			when others then
			
			qt_material_w := 0;
			
		end;
		
		if	(qt_material_p > qt_material_w) then
			ds_retorno_w := wheb_mensagem_pck.get_texto(261256);
								--N�o � poss�vel devolver quantidade maior que a atendida
		end if;
		
	end if;
end if;
ds_retorno_p := ds_retorno_w;

end;
/