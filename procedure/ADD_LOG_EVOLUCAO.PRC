create or replace
procedure add_log_evolucao(cd_evolucao_p     number,
                           nr_atendimento_p  number,
                           nm_usuario_p      varchar2,
						   ds_evolucao_p     varchar2,
						   ds_ctr_v_p        varchar2) is
						   
begin

if (cd_evolucao_p > 0) then 

	insert into log_evolucao_w values (
		cd_evolucao_p,
		nr_atendimento_p,
		nm_usuario_p,
		substr(ds_evolucao_p,1,4000),
		substr(ds_ctr_v_p,1,4000),
		sysdate
	);
	
	commit;

end if;

end;
/