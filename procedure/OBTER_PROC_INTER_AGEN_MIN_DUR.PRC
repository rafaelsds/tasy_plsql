create or replace
procedure obter_proc_inter_agen_min_dur(
		cd_convenio_p		number,
		nr_seq_agenda_p		number,
		nr_seq_interno_p	number,
		cd_estabelecimento_p	number,
		cd_medico_p		varchar2,
		cd_categoria_p		varchar2,				
		qt_min_duracao_p	out number,
		cd_procedimento_p	out number,		
		ie_origem_proced_p	out number,		
		ds_procedimento_p	out varchar2) is
begin

cd_procedimento_p := '';
ie_origem_proced_p := '';
ds_procedimento_p := '';

obter_proc_tab_inter_agenda_js(
	nr_seq_interno_p,
	nr_seq_agenda_p,
	cd_convenio_p,
	cd_categoria_p,
	cd_estabelecimento_p,
	null,
	cd_procedimento_p,
	ie_origem_proced_p,
	ds_procedimento_p);

qt_min_duracao_p := obter_tempo_total_proced(
			nr_seq_interno_p,
			cd_medico_p);

end obter_proc_inter_agen_min_dur;
/
