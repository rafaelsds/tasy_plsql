create or replace
procedure atualizar_horario_escala(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2,
					dt_inicial_p		date,
					dt_final_p		date) is 

begin
update	escala_horario
set	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	hr_inicial	= dt_inicial_p,
	hr_final	= dt_final_p
where	nr_sequencia	= nr_sequencia_p;

commit;

end atualizar_horario_escala;
/