create or replace
procedure gerar_equipe_medica_cirurgia(	nr_cirurgia_p		number,
					ie_tipo_p		number,
					nr_sequencia_p		number,
					nm_usuario_p		varchar,
					cd_estabelecimento_p	number) 
					is
					
cd_pessoa_fisica_w	varchar2(15):=null;
ie_funcao_w		varchar2(15):=null;
nr_sequencia_w		number(15);
												
begin

if	(ie_tipo_p = 1) then
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pf_equipe_partic
	where	nr_sequencia = nr_sequencia_p;
elsif	(ie_tipo_p = 2) then
	select	cd_pessoa_fisica,
		ie_funcao
	into	cd_pessoa_fisica_w,
		ie_funcao_w
	from	cirurgia_participante
	where	nr_seq_interno = nr_sequencia_p;
elsif	(ie_tipo_p = 3) then
	select	cd_anestesista
	into	cd_pessoa_fisica_w
	from	agenda_paciente
	where	nr_sequencia = nr_sequencia_p;
elsif	(ie_tipo_p = 4) then
	cd_pessoa_fisica_w := nr_sequencia_p;
elsif	(ie_tipo_p = 5) then
	select	cd_pessoa_fisica,
		ie_funcao
	into	cd_pessoa_fisica_w,
		ie_funcao_w
	from	cirurgia_participante
	where	nr_seq_interno = nr_sequencia_p;
end if;

if	(ie_funcao_w is null) then
	select	max(ie_funcao)
	into	ie_funcao_w
	from	cirurgia_participante
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	if	(ie_funcao_w is null) then
		select	max(cd_funcao) 
		into	ie_funcao_w
		from	funcao_medico
		where	ie_medico = 'S';
	end if;
end if;	

if	(cd_pessoa_fisica_w is not null) and
	(ie_funcao_w is not null) and
	(nr_cirurgia_p	is not null) then
	
	select	nvl(max(nr_sequencia),0)+1
	into	nr_sequencia_w
	from	cirurgia_participante
	where	nr_cirurgia = nr_cirurgia_p;
	
	insert	into 	cirurgia_participante(	
			nr_cirurgia,
			nr_sequencia,
			nr_seq_interno,
			nm_usuario,
			dt_atualizacao,
			cd_pessoa_fisica,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_funcao)
			
	select		nr_cirurgia_p,
			nr_sequencia_w,
			cirurgia_participante_seq.nextval,
			nm_usuario_p,
			sysdate,
			cd_pessoa_fisica_w,
			'A',
			sysdate,
			nm_usuario_p,
			ie_funcao_w
	from		dual;
	commit;
end if;	

end gerar_equipe_medica_cirurgia;
/