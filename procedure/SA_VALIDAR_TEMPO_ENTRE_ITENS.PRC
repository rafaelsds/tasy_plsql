create or replace
procedure sa_validar_tempo_entre_itens(	nr_seq_proc_adic_p number,
                                        nr_seq_proc_interno_p number,
                                        nr_seq_ageint_item_p number,
                                        nm_usuario_p		Varchar2) is 

qt_regra_tempo_exame_adic_w number(10,0);
nr_seq_proc_interno_w       ageint_tempo_entre_exames.cd_exame_pri%type;
nr_seq_proc_adic_w          ageint_tempo_entre_exames.cd_exame_seg%type;

Cursor C04 is
  select  nr_seq_proc_interno
  from  ageint_exame_adic_item
  where  nr_seq_item = nr_seq_ageint_item_p; 

Cursor C05 is
  select  decode(cd_exame_pri, nr_seq_proc_interno_w, cd_exame_seg, cd_exame_pri) cd_exame
  from	ageint_tempo_entre_exames
  where	((coalesce(ie_regra_exclusiva, 'N') = 'N' and cd_exame_pri  = nr_seq_proc_adic_w)
  or		(cd_exame_seg  = nr_seq_proc_adic_w))
  and		ie_situacao  = 'A';
  
begin

	nr_seq_proc_interno_w := nr_seq_proc_interno_p;
	nr_seq_proc_adic_w := nr_seq_proc_adic_p;

	for r_c05_w in C05
	loop
		select	count(1) 
		into	qt_regra_tempo_exame_adic_w
		from  	ageint_exame_adic_item
		where 	nr_seq_item = nr_seq_ageint_item_p
		and		nr_seq_proc_interno = r_c05_w.cd_exame;
		
		if	(qt_regra_tempo_exame_adic_w > 0) then
			  wheb_mensagem_pck.exibir_mensagem_abort(229800);
		end if;

	end loop;
	
	for r_c04_w in C04
	loop

		nr_seq_proc_interno_w := r_c04_w.nr_seq_proc_interno;
		nr_seq_proc_adic_w := r_c04_w.nr_seq_proc_interno;

		for r_c05_ww in C05
		loop
		
			select	count(1) 
			into	qt_regra_tempo_exame_adic_w
			from  	ageint_exame_adic_item
			where 	nr_seq_item = nr_seq_ageint_item_p
			and		nr_seq_proc_interno = r_c05_ww.cd_exame;
			
			if	(qt_regra_tempo_exame_adic_w > 0) then
				  wheb_mensagem_pck.exibir_mensagem_abort(229800);
			end if;

		end loop;

	end loop;

end sa_validar_tempo_entre_itens;
/