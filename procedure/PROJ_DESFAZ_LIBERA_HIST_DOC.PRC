create or replace
procedure proj_desfaz_libera_hist_doc(	nr_seq_doc_p		number,
									nr_seq_projeto_p	number,
									nm_usuario_p		varchar2) is 

begin
update	com_cliente_hist
set		dt_liberacao	= null,
		dt_atualizacao	= sysdate,
		nm_usuario		= nm_usuario_p,
		nm_usuario_lib	= nm_usuario_p
where	nr_seq_projeto	= nr_seq_projeto_p
and		nr_sequencia	= nr_seq_doc_p;

commit;

end proj_desfaz_libera_hist_doc;
/