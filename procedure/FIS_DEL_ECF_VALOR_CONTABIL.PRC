create or replace 
procedure fis_del_ecf_valor_contabil(	cd_estabelecimento_p 	number,
										nr_seq_lote_p			number) is
begin
	
	delete from fis_ecf_valor_contabil_w 
	where cd_estabelecimento = cd_estabelecimento_p
	and nr_seq_lote = nr_seq_lote_p;
					
	commit;
							
end fis_del_ecf_valor_contabil;
/