create or replace
procedure Atualizar_classif_retorno_enc(ie_classif_agenda_p	varchar2,
					nr_seq_agenda_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	agenda_consulta
set	ie_classif_agenda = ie_classif_agenda_p
where	nr_sequencia = nr_seq_agenda_p;

commit;

end Atualizar_classif_retorno_enc;
/