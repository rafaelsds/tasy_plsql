create or replace
procedure cpoe_atualizar_cadastro_exame ( nm_usuario_p 	varchar2 ) is

	nr_seq_proc_interno_w	proc_interno.nr_sequencia%type;

	Cursor C01 is
		select	nr_sequencia,
			nr_seq_exame		
		from	exame_lab_rotina
		where nr_seq_exame is not null
		AND nvl(ie_situacao,'A') = 'A'
		order by 1;
		
	c01_w c01%rowtype;			
	
begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	
	select	max(nr_sequencia)
	into	nr_seq_proc_interno_w
	from	proc_interno
	where	nr_seq_exame_lab = c01_w.nr_seq_exame;
	
	if	(nvl(nr_seq_proc_interno_w,0) > 0) then
		
		update	exame_lab_rotina
		set nr_seq_exame_interno = nr_seq_proc_interno_w,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where nr_sequencia = c01_w.nr_sequencia
		and nr_seq_exame = c01_w.nr_seq_exame;				
	end if;
	
end loop;
close C01;

commit;

end cpoe_atualizar_cadastro_exame;
/

