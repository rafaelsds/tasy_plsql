create or replace
procedure pls_liberar_desfazer_prog_reaj
		(	nr_seq_prog_reajuste_p	number,
			ie_opcao_p		varchar2,
			nm_usuario_p		Varchar2) is 
			
/*
ie_opcao_p
L - Liberar
D - Desfazer
*/			

begin

if	(ie_opcao_p	= 'L') then
	update	pls_prog_reajuste
	set	dt_liberacao		= sysdate,
		nm_usuario_liberacao	= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_prog_reajuste_p;
elsif	(ie_opcao_p	= 'D') then
	update	pls_prog_reajuste
	set	dt_liberacao		= null,
		nm_usuario_liberacao	= null,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_prog_reajuste_p;
end if;

commit;

end pls_liberar_desfazer_prog_reaj;
/
