create or replace
procedure SCHEDULE_UPDATE_PRE_BILLING(	dt_referencia_p	date,
					nm_usuario_p	varchar2) is

qtd_pf_atualizando_w		number(10,0);
nr_sequencia_w			pre_fatur_atualizacao.nr_sequencia%type;
dt_agendamento_w		pre_fatur_atualizacao.dt_agendamento%type;
cd_estabelecimento_w		pre_fatur_atualizacao.cd_estabelecimento%type;
ds_erro_w			pre_fatur_atualizacao.ds_erro%type;

cursor c01 is
select		a.nr_sequencia,
		a.dt_agendamento,
		a.cd_estabelecimento
from		pre_fatur_atualizacao a
where		a.dt_inicio_real is null
and		a.dt_agendamento <= dt_referencia_p
order by	a.dt_agendamento,
		a.dt_atualizacao;

begin
select	count(1)
into	qtd_pf_atualizando_w
from	pre_fatur_atualizacao
where	dt_inicio_real is not null
and	dt_fim_real is null;

if	(qtd_pf_atualizando_w > 0) then
	--Nao foi possivel atualizar o pre faturamento.
	wheb_mensagem_pck.exibir_mensagem_abort(1174957);
else
	open c01;
	loop
	fetch c01 into
		nr_sequencia_w,
		dt_agendamento_w,
		cd_estabelecimento_w;
	exit when (c01%rowcount = 1) OR (c01%notfound);
	end loop;
	close c01;
	begin
		update	pre_fatur_atualizacao
		set	dt_inicio_real	= sysdate
		where	nr_sequencia	= nr_sequencia_w;
		commit;

		begin
		gerar_pre_faturamento(	dt_agendamento_w,
					nm_usuario_p,
					cd_estabelecimento_w);
		exception
			when others then
				ds_erro_w := substr(sqlerrm,1,2000);
				update	pre_fatur_atualizacao
				set	ds_erro		= ds_erro_w
				where	nr_sequencia	= nr_sequencia_w;
		end;

		update	pre_fatur_atualizacao
		set	dt_fim_real	= sysdate
		where	nr_sequencia	= nr_sequencia_w;
		commit;
	end;
end if;
end SCHEDULE_UPDATE_PRE_BILLING;
/
