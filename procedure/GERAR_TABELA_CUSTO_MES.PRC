create or replace
procedure gerar_tabela_custo_mes(	cd_estabelecimento_p	number,
				dt_mes_referencia_p 		date,
				ie_orcado_real_p		varchar2,
				ds_tabela_custo_p		varchar2,
				nm_usuario_p			varchar2) is

qt_tabela_w			number(5) := 0;
dt_mes_referencia_w		date;
cd_tabela_custo_w			tabela_custo.cd_tabela_custo%type;
ds_orcado_real_w			varchar2(50);
ds_tabela_custo_w			varchar2(255);
nr_sequencia_w			tabela_custo.nr_sequencia%type;
cd_empresa_w			tabela_custo.cd_empresa%type;
ie_custo_multi_estab_w		parametro_custo.ie_custo_multi_estab%type := 'N';
nr_seq_grupo_emp_w		tabela_custo.nr_seq_grupo_emp%type;
nm_grupo_empresa_w		grupo_empresa.nm_grupo_empresa%type;

cursor C01 is
select	a.cd_tipo_tabela_custo,
	substr(cus_obter_desc_tipo_tabela(a.cd_tipo_tabela_custo),1,50) ds_tipo_tabela_custo
from	tipo_tabela_custo a
order by 1;

c01_w			C01%rowtype;

Cursor C02 is
select	a.cd_estabelecimento
from	estabelecimento a
where	a.cd_empresa = cd_empresa_w
and	a.ie_situacao = 'A'
and	(a.cd_estabelecimento	= cd_estabelecimento_p or ie_custo_multi_estab_w = 'S')
union all
select	0 cd_estabelecimento
from	dual
where	ie_custo_multi_estab_w = 'S'
and	1 = 1
order by 1;

c02_w			c02%rowtype;

cursor c03 is
	select	b.nr_seq_tabela,
		b.nr_seq_parametro,
		d.cd_tabela_custo,
		d.nr_sequencia
	from	tabela_custo d,
		tipo_tabela_custo_param c,
		tabela_parametro b,
		tabela_custo a
	where	a.nr_sequencia		= b.nr_seq_tabela
	and	b.nr_seq_parametro		= c.nr_seq_parametro
	and	a.cd_tipo_tabela_custo	= c.cd_tipo_tabela_custo
	and	c.cd_tipo_tabela_parametro	= d.cd_tipo_tabela_custo
	and	a.ie_orcado_real		= ie_orcado_real_p
	and	d.ie_orcado_real		= ie_orcado_real_p
	and	c.ie_tipo_parametro		= 'T'
	and	a.cd_empresa		= cd_empresa_w
	and	d.cd_empresa		= cd_empresa_w
	and	a.dt_mes_referencia	= dt_mes_referencia_w
	and	d.dt_mes_referencia	= dt_mes_referencia_w
	and	nvl(a.cd_estabelecimento,0)	= nvl(d.cd_estabelecimento,0)
	and	nvl(a.cd_estabelecimento,0) = c02_w.cd_estabelecimento
	union all
	select	b.nr_seq_tabela,
		b.nr_seq_parametro,
		d.cd_tabela_custo,
		d.nr_sequencia
	from	tabela_custo d,
		tipo_tabela_custo_param c,
		tabela_parametro b,
		tabela_custo a
	where	a.nr_sequencia		= b.nr_seq_tabela
	and	b.nr_seq_parametro		= c.nr_seq_parametro
	and	a.cd_tipo_tabela_custo	= c.cd_tipo_tabela_custo
	and	c.cd_tipo_tabela_parametro	= d.cd_tipo_tabela_custo
	and	a.ie_orcado_real		= ie_orcado_real_p
	and	d.ie_orcado_real		= ie_orcado_real_p
	and	c.ie_tipo_parametro		= 'T'
	and	a.cd_empresa		= cd_empresa_w
	and	d.cd_empresa		= cd_empresa_w
	and	a.dt_mes_referencia	= dt_mes_referencia_w
	and	d.dt_mes_referencia	= dt_mes_referencia_w
	and	a.cd_estabelecimento = c02_w.cd_estabelecimento
	and	d.cd_estabelecimento is null
	and	ie_custo_multi_estab_w = 'S'
	union all
	select	b.nr_seq_tabela,
		b.nr_seq_parametro,
		d.cd_tabela_custo,
		d.nr_sequencia
	from	tabela_custo d,
		tipo_tabela_custo_param c,
		tabela_parametro b,
		tabela_custo a
	where	a.nr_sequencia		= b.nr_seq_tabela
	and	b.nr_seq_parametro		= c.nr_seq_parametro
	and	a.cd_tipo_tabela_custo	= c.cd_tipo_tabela_custo
	and	c.cd_tipo_tabela_parametro	= d.cd_tipo_tabela_custo
	and	a.ie_orcado_real		= ie_orcado_real_p
	and	d.ie_orcado_real		= ie_orcado_real_p
	and	c.ie_tipo_parametro		= 'T'
	and	a.cd_empresa		= cd_empresa_w
	and	d.cd_empresa		= cd_empresa_w
	and	a.dt_mes_referencia	= dt_mes_referencia_w
	and	d.dt_mes_referencia	= dt_mes_referencia_w
	and	a.cd_estabelecimento	is null
	and	d.cd_estabelecimento = cd_estabelecimento_P
	and	ie_custo_multi_estab_w = 'S'
	union all
	select	b.nr_seq_tabela,
		b.nr_seq_parametro,
		d.cd_tabela_custo,
		d.nr_sequencia
	from	tabela_custo d,
		tipo_tabela_custo_param c,
		tabela_parametro b,
		tabela_custo a
	where	a.nr_sequencia			= b.nr_seq_tabela
	and	b.nr_seq_parametro		= c.nr_seq_parametro
	and	a.cd_tipo_tabela_custo		= c.cd_tipo_tabela_custo
	and	c.cd_tipo_tabela_parametro	= d.cd_tipo_tabela_custo
	and	a.ie_orcado_real		= ie_orcado_real_p
	and	d.ie_orcado_real		= ie_orcado_real_p
	and	c.ie_tipo_parametro		= 'T'
	and	a.cd_empresa			= d.cd_empresa
	and	d.cd_empresa			= cd_empresa_w
	and	a.dt_mes_referencia		= dt_mes_referencia_w
	and	d.dt_mes_referencia		= dt_mes_referencia_w
	and	a.cd_estabelecimento is null
	and	d.nr_seq_grupo_emp is not null
	and	a.nr_seq_grupo_emp is not null
	and	ie_custo_multi_estab_w = 'E'
	order by nr_seq_tabela,nr_seq_parametro,nr_sequencia;

c03_w	c03%RowType;

begin

cd_empresa_w		:= Obter_Empresa_Estab(cd_estabelecimento_p);
dt_mes_referencia_w	:= trunc(dt_mes_referencia_p,'mm');

if	(ie_orcado_real_p = 'O') then
	ds_orcado_real_w	:= wheb_mensagem_pck.get_texto(798614);
elsif	(ie_orcado_real_p = 'R') then
	ds_orcado_real_w	:= wheb_mensagem_pck.get_texto(798615);
elsif	(ie_orcado_real_p = 'S') then
	ds_orcado_real_w	:= wheb_mensagem_pck.get_texto(798616);
end if;

select	nvl(max(ie_custo_multi_estab),'N')
into	ie_custo_multi_estab_w
from	parametro_custo
where	cd_estabelecimento	= cd_estabelecimento_p;

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
	begin
	ds_tabela_custo_w		:= substr(ds_tabela_custo_p,1,255);
	if	(ds_tabela_custo_p is not null) then
		ds_tabela_custo_w	:= substr(replace(ds_tabela_custo_w,'@DS_TIPO_TABELA',c01_w.ds_tipo_tabela_custo),1,255);
		ds_tabela_custo_w	:= substr(replace(ds_tabela_custo_w,'@DT_MES_1',substr(to_char(dt_mes_referencia_w,'dd/mm/yy'),4,8)),1,255);
		ds_tabela_custo_w	:= substr(replace(ds_tabela_custo_w,'@DT_MES_2',substr(to_char(dt_mes_referencia_w,'dd/mm/yy'),4,8)),1,255);
		ds_tabela_custo_w	:= substr(replace(ds_tabela_custo_w,'@IE_TIPO',ie_orcado_real_p),1,255);
		ds_tabela_custo_w	:= substr(replace(ds_tabela_custo_w,'@DS_TIPO',ds_orcado_real_w),1,255);
	else
		ds_tabela_custo_w	:= substr(c01_w.ds_tipo_tabela_custo || ' ' || substr(to_char(dt_mes_referencia_w),4,8),1,50);
	end if;

	ds_tabela_custo_w	:= substr(ds_tabela_custo_w,1,100);
	-- N�o utiliza multi-estabelecimento
	if	(ie_custo_multi_estab_w = 'N') then
		begin

		begin
		select 1
		into	qt_tabela_w
		from	tabela_custo
		where	dt_mes_referencia	= dt_mes_referencia_w
		and	ie_orcado_real		= ie_orcado_real_p
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_tipo_tabela_custo	= c01_w.cd_tipo_tabela_custo
		and	rownum < 2;
		exception when others then
			qt_tabela_w := 0;
		end;

		if	(qt_tabela_w = 0) then

			select	nvl(max(cd_tabela_custo),0)
			into	cd_tabela_custo_w
			from	tabela_custo
			where	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
			and	cd_empresa = cd_empresa_w;

			cd_tabela_custo_w	:= cd_tabela_custo_w + 1;

			select	tabela_custo_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into tabela_custo(
				nr_sequencia,
				cd_estabelecimento,
				cd_tabela_custo,
				ds_tabela_custo,
				nm_usuario,
				dt_atualizacao,
				cd_tipo_tabela_custo,
				ie_situacao_tabela,
				dt_vigencia_inicial,
				dt_vigencia_final,
				dt_inicio_uso,
				dt_final_uso,
				dt_mes_referencia,
				ie_orcado_real,
				cd_empresa,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_situacao)
			values(	nr_sequencia_w,
				cd_estabelecimento_p,
				cd_tabela_custo_w,
				ds_tabela_custo_w,
				nm_usuario_p,
				sysdate,
				c01_w.cd_tipo_tabela_custo,
				'1',
				dt_mes_referencia_w,
				last_day(dt_mes_referencia_w),
				null,
				null,
				dt_mes_referencia_w,
				ie_orcado_real_p,
				cd_empresa_w,
				sysdate,
				nm_usuario_p,
				'A');

			begin
			insert into Tabela_parametro(
				cd_estabelecimento,
				cd_tabela_custo,
				nr_seq_parametro,
				nm_usuario,
				dt_atualizacao,
				cd_tabela_parametro,
				cd_comp_parametro,
				dt_parametro,
				qt_parametro,
				nr_seq_tabela,
				nr_seq_tabela_param,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			select	cd_estabelecimento_p,
				cd_tabela_custo_w,
				nr_seq_parametro,
				nm_usuario_p,
				sysdate,
				null,
				null,
				null,
				null,
				nr_sequencia_w,
				null,
				sysdate,
				nm_usuario_p
			from	tipo_tabela_custo_param
			where	cd_tipo_tabela_custo	= c01_w.cd_tipo_tabela_custo;
			exception when others then
				Wheb_mensagem_pck.exibir_mensagem_abort(212892);
			end;
		end if;
		end;
	elsif	(ie_custo_multi_estab_w = 'S') then
		begin
			if	(c01_w.cd_tipo_tabela_custo in (1,3,4,8,9)) then
				-- Caso utilizar multi estabelecimento e for as tabelas do tipo 1, 3, 4, 8 ou 9 criar uma tabela para cada estabelecimento
				open C02;
				loop
				fetch C02 into
					c02_w;
				exit when C02%notfound;
					begin

					begin
					select	1
					into	qt_tabela_w
					from	tabela_custo
					where	dt_mes_referencia	= dt_mes_referencia_w
					and	cd_empresa		= cd_empresa_w
					and	ie_orcado_real		= ie_orcado_real_p
					and	cd_tipo_tabela_custo	= c01_w.cd_tipo_tabela_custo
					and	cd_estabelecimento	= c02_w.cd_estabelecimento
					and	rownum < 2;

					exception when others then
						qt_tabela_w := 0;
					end;

					if	(qt_tabela_w = 0) and
						(c02_w.cd_estabelecimento <> 0) then
						begin
						select	nvl(max(cd_tabela_custo),0)
						into	cd_tabela_custo_w
						from	tabela_custo
						where	nvl(cd_estabelecimento, c02_w.cd_estabelecimento) = c02_w.cd_estabelecimento
						and	cd_empresa = cd_empresa_w;

						cd_tabela_custo_w	:= cd_tabela_custo_w + 1;

						select	tabela_custo_seq.nextval
						into	nr_sequencia_w
						from	dual;

						insert into tabela_custo(
							nr_sequencia,
							cd_estabelecimento,
							cd_tabela_custo,
							ds_tabela_custo,
							nm_usuario,
							dt_atualizacao,
							cd_tipo_tabela_custo,
							ie_situacao_tabela,
							dt_vigencia_inicial,
							dt_vigencia_final,
							dt_inicio_uso,
							dt_final_uso,
							dt_mes_referencia,
							ie_orcado_real,
							cd_empresa,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ie_situacao)
						values(	nr_sequencia_w,
							c02_w.cd_estabelecimento,
							cd_tabela_custo_w,
							ds_tabela_custo_w,
							nm_usuario_p,
							sysdate,
							c01_w.cd_tipo_tabela_custo,
							'1',
							dt_mes_referencia_w,
							last_day(dt_mes_referencia_w),
							null,
							null,
							dt_mes_referencia_w,
							ie_orcado_real_p,
							cd_empresa_w,
							sysdate,
							nm_usuario_p,
							'A');

						begin
						insert into Tabela_parametro(
							cd_estabelecimento,
							cd_tabela_custo,
							nr_seq_parametro,
							nm_usuario,
							dt_atualizacao,
							cd_tabela_parametro,
							cd_comp_parametro,
							dt_parametro,
							qt_parametro,
							nr_seq_tabela,
							nr_seq_tabela_param,
							dt_atualizacao_nrec,
							nm_usuario_nrec)
						select	c02_w.cd_estabelecimento,
							cd_tabela_custo_w,
							nr_seq_parametro,
							nm_usuario_p,
							sysdate,
							null,
							null,
							null,
							null,
							nr_sequencia_w,
							null,
							sysdate,
							nm_usuario_p
						from	tipo_tabela_custo_param
						where	cd_tipo_tabela_custo	= c01_w.cd_tipo_tabela_custo;
						exception when others then
							Wheb_mensagem_pck.exibir_mensagem_abort(212892);
						end;
						end;
					end if;
					end;
				end loop;
				close C02;
			else
				begin
				begin
				select	1
				into	qt_tabela_w
				from	tabela_custo
				where	dt_mes_referencia		= dt_mes_referencia_w
				and	cd_empresa			= cd_empresa_w
				and	ie_orcado_real			= ie_orcado_real_p
				and	cd_tipo_tabela_custo		= c01_w.cd_tipo_tabela_custo
				and	rownum < 2;

				exception when others then
					qt_tabela_w := 0;
				end;

				if	(qt_tabela_w = 0) then
					begin
					-- Caso utilizar multi-estabelecimento e n�o for as tabelas do tipo 1, 3, 4, 8 ou 9 criar uma tabela sem estabelecimento informado, que ser� utilizada por todos
					ds_tabela_custo_w := substr(ds_tabela_custo_w || ' ' || wheb_mensagem_pck.get_texto(798617),1,100);

					select	nvl(max(cd_tabela_custo),0)
					into	cd_tabela_custo_w
					from	tabela_custo
					where	nvl(cd_empresa,cd_empresa_w) = cd_empresa_w;


					cd_tabela_custo_w	:= cd_tabela_custo_w + 1;

					select	tabela_custo_seq.nextval
					into	nr_sequencia_w
					from	dual;

					insert into tabela_custo(
						nr_sequencia,
						cd_estabelecimento,
						cd_tabela_custo,
						ds_tabela_custo,
						nm_usuario,
						dt_atualizacao,
						cd_tipo_tabela_custo,
						ie_situacao_tabela,
						dt_vigencia_inicial,
						dt_vigencia_final,
						dt_inicio_uso,
						dt_final_uso,
						dt_mes_referencia,
						ie_orcado_real,
						cd_empresa,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_situacao)
					values(	nr_sequencia_w,
						null,
						cd_tabela_custo_w,
						ds_tabela_custo_w,
						nm_usuario_p,
						sysdate,
						c01_w.cd_tipo_tabela_custo,
						'1',
						dt_mes_referencia_w,
						last_day(dt_mes_referencia_w),
						null,
						null,
						dt_mes_referencia_w,
						ie_orcado_real_p,
						cd_empresa_w,
						sysdate,
						nm_usuario_p,
						'A');

					begin
					insert into Tabela_parametro(
						cd_estabelecimento,
						cd_tabela_custo,
						nr_seq_parametro,
						nm_usuario,
						dt_atualizacao,
						cd_tabela_parametro,
						cd_comp_parametro,
						dt_parametro,
						qt_parametro,
						nr_seq_tabela,
						nr_seq_tabela_param,
						dt_atualizacao_nrec,
						nm_usuario_nrec)
					select	cd_estabelecimento_p,
						cd_tabela_custo_w,
						nr_seq_parametro,
						nm_usuario_p,
						sysdate,
						null,
						null,
						null,
						null,
						nr_sequencia_w,
						null,
						sysdate,
						nm_usuario_p
					from	tipo_tabela_custo_param
					where	cd_tipo_tabela_custo	= c01_w.cd_tipo_tabela_custo;

					exception when others then
						Wheb_mensagem_pck.exibir_mensagem_abort(212892);
					end;
					end;
				end if;
				end;
			end if;
		end;
	else

		begin
		select	holding_pck.get_grupo_emp_usuario(cd_empresa_w),
			holding_pck.get_nm_grupo_empresa(null, cd_empresa_w)
		into	nr_seq_grupo_emp_w,
			nm_grupo_empresa_w
		from	dual;

			begin
				select	1
				into	qt_tabela_w
				from	tabela_custo
				where	dt_mes_referencia		= dt_mes_referencia_w
				and	cd_empresa			= cd_empresa_w
				and	ie_orcado_real			= ie_orcado_real_p
				and	cd_tipo_tabela_custo		= c01_w.cd_tipo_tabela_custo
				and	rownum < 2;
			exception when others then
				qt_tabela_w := 0;
			end;

		if	(qt_tabela_w = 0) then
			begin
			ds_tabela_custo_w := substr(ds_tabela_custo_w || ' ' || nm_grupo_empresa_w ,1,100);

			select	nvl(max(cd_tabela_custo),0)
			into	cd_tabela_custo_w
			from	tabela_custo
			where	nvl(cd_empresa,cd_empresa_w) = cd_empresa_w;


			cd_tabela_custo_w	:= cd_tabela_custo_w + 1;

			select	tabela_custo_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into tabela_custo(
				nr_sequencia,
				cd_estabelecimento,
				cd_tabela_custo,
				ds_tabela_custo,
				nm_usuario,
				dt_atualizacao,
				cd_tipo_tabela_custo,
				ie_situacao_tabela,
				dt_vigencia_inicial,
				dt_vigencia_final,
				dt_inicio_uso,
				dt_final_uso,
				dt_mes_referencia,
				ie_orcado_real,
				cd_empresa,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_situacao,
				nr_seq_grupo_emp)
			values(	nr_sequencia_w,
				null,
				cd_tabela_custo_w,
				ds_tabela_custo_w,
				nm_usuario_p,
				sysdate,
				c01_w.cd_tipo_tabela_custo,
				'1',
				dt_mes_referencia_w,
				last_day(dt_mes_referencia_w),
				null,
				null,
				dt_mes_referencia_w,
				ie_orcado_real_p,
				cd_empresa_w,
				sysdate,
				nm_usuario_p,
				'A',
				nr_seq_grupo_emp_w);

			begin
			insert into Tabela_parametro(
				cd_estabelecimento,
				cd_tabela_custo,
				nr_seq_parametro,
				nm_usuario,
				dt_atualizacao,
				cd_tabela_parametro,
				cd_comp_parametro,
				dt_parametro,
				qt_parametro,
				nr_seq_tabela,
				nr_seq_tabela_param,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			select	cd_estabelecimento_p,
				cd_tabela_custo_w,
				nr_seq_parametro,
				nm_usuario_p,
				sysdate,
				null,
				null,
				null,
				null,
				nr_sequencia_w,
				null,
				sysdate,
				nm_usuario_p
			from	tipo_tabela_custo_param
			where	cd_tipo_tabela_custo	= c01_w.cd_tipo_tabela_custo;

			exception when others then
				Wheb_mensagem_pck.exibir_mensagem_abort(212892);
			end;
			end;
		end if;
		end;

	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	c02_w;
exit when C02%notfound;
	begin

	open c03;
	loop
	fetch c03 into
		c03_w;
	exit when c03%notfound;
		null;

		update	tabela_parametro
		set	cd_tabela_parametro	= c03_w.cd_tabela_custo,
			nr_seq_tabela_param	= c03_w.nr_sequencia
		where	nr_seq_tabela		= c03_w.nr_seq_tabela
		and	nr_seq_parametro	= c03_w.nr_seq_parametro;

	end loop;
	close c03;
	end;
end loop;
close C02;

commit;

end gerar_tabela_custo_mes;
/