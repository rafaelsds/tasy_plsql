create or replace
procedure PLT_copia_suplemento(	nr_prescricao_p			number,
				nr_seq_regra_p			number,
				ds_lista_p			varchar2,
				ie_modificar_p			varchar2,
				nm_usuario_p			Varchar2,
				cd_perfil_p			number,
				cd_estabelecimento_p		number,
				ie_estende_inc_p		varchar2,
				ie_copia_agora_p		varchar2) is 

nr_agrup_acum_w			number(10,0);
ds_lista_w			varchar2(4000);
tam_lista_w			number(10,0);
tam_prescricao_w		number(10,0);
ie_pos_virgula_w		number(3,0);
ie_pos_espaco_w			number(3,0);
nr_prescr_lista_w		number(14);
nr_prescr_ant_w			number(14);
qt_w				number(14);
nr_seq_lista_w			number(6);
ds_prescricao_w			varchar2(255);
nr_sequencia_w			number(6);
ie_origem_inf_w			varchar2(1);
cd_material_w			number(6);
cd_unidade_medida_w		varchar2(30);
qt_dose_w			number(15,3);
qt_unitaria_w			number(18,6);
qt_material_w			number(15,3);
cd_intervalo_w			varchar2(7);
ds_horarios_w			varchar2(2000);
ds_observacao_w			varchar2(4000);
ds_observacao_enf_w		varchar2(2000);
ie_via_aplicacao_w		varchar2(5);
nr_agrupamento_w		number(7,1);
ie_cobra_paciente_w		varchar2(1);
cd_motivo_baixa_w		number(3);	
dt_baixa_w			date;
ie_utiliza_kit_w		varchar2(1);	
cd_unidade_medida_dose_w	varchar2(30);	
qt_conversao_dose_w		number(15,4);
ie_urgencia_w			varchar2(1);
nr_ocorrencia_w			number(15,4);
qt_total_dispensar_w		number(18,6);
cd_fornec_consignado_w		varchar2(14);
nr_sequencia_solucao_w		number(6);
nr_sequencia_proc_w		number(6);
qt_solucao_w			number(15,4);
hr_dose_especial_w		varchar2(5);	
qt_dose_especial_w		number(15,4);
ds_dose_diferenciada_w		varchar2(50);
ie_medicacao_paciente_w		varchar2(1);
nr_sequencia_diluicao_w		number(6);
hr_prim_horario_w		varchar2(5);
nr_sequencia_dieta_w		number(6);
ie_agrupador_w			number(2);
nr_dia_util_w			number(10);
ie_suspenso_w			varchar2(1);
ie_se_necessario_w		varchar2(1);
qt_min_aplicacao_w		number(4);
ie_bomba_infusao_w		varchar2(1);
ie_aplic_bolus_w		varchar2(1);
ie_aplic_lenta_w		varchar2(1);
ie_acm_w			varchar2(1);
ie_objetivo_w			varchar2(1);
cd_topografia_cih_w		number(10);
ie_origem_infeccao_w		varchar2(1);
cd_amostra_cih_w		number(10);
cd_microorganismo_cih_w		number(10);
ie_uso_antimicrobiano_w		varchar2(1);
cd_protocolo_w			number(10);
nr_seq_protocolo_w		number(6);
nr_seq_mat_protocolo_w		number(6);
qt_hora_aplicacao_w		number(3);
ie_recons_diluente_fixo_w	varchar2(1);
qt_vel_infusao_w		number(15,4);
ds_justificativa_w		varchar2(2000);
ie_sem_aprazamento_w		varchar2(1);
ie_indicacao_w			varchar2(1);
dt_proxima_dose_w		date;
qt_total_dias_lib_w		number(5);
nr_seq_substituto_w		number(6);
ie_lado_w			varchar2(1);
dt_inicio_medic_w		date;
qt_dia_prim_hor_w		number(10);
ie_regra_disp_w			varchar2(1);
qt_vol_adic_reconst_w		number(15,4);
qt_hora_intervalo_w		number(2);
qt_min_intervalo_w		number(5);
dt_inicio_prescr_w		date;
dt_validade_prescr_w		date;
nr_prescr_estendido_w		number(14);
cd_pessoa_fisica_w		varchar2(10);
dt_suspensao_progr_w		date;
nr_prescricao_original_w	number(14);
nr_seq_anterior_w		number(6);
qt_inconsistencia_w		number(10);
dt_prim_horario_int_w		date;
nr_horas_validade_w		number(5);
dt_prescricao_w			date;
dt_primeiro_horario_w		date;
ie_regra_geral_w		varchar2(10);
ds_horarios2_w			varchar2(2000);
ie_operacao_w			intervalo_prescricao.ie_operacao%type;
cd_estabelecimento_w	Number(4,0);
ds_erro_w				Varchar2(255);
ds_horarios_ww			prescr_material.ds_horarios%type;
ie_manter_intervalo_w	rep_regra_copia_crit.ie_manter_intervalo%type;
dt_ultimo_horario_w		date;
hr_prim_horario_aux_w	varchar2(5);
nr_ocorrencia_aux_w		Number(15,4);
dt_prim_hor_aux_w		date;


cursor c01 is
	select	nvl(ie_regra_geral,'H'),
			nvl(ie_manter_intervalo,'N')
	from	rep_regra_copia_crit
	where	nr_seq_regra	= nr_seq_regra_p
	and	ie_tipo_item	= 'SUP'
	and 	(nvl(ie_se_necessario,'S') = 'S' 	or nvl(ie_se_necessario_w,'N') <> 'S')
	and 	(nvl(ie_acm,'S') = 'S' 			or nvl(ie_acm_w,'N') <> 'S')
	and 	(nvl(ie_agora,'S') = 'S' 		or nvl(ie_urgencia_w,'N') <> 'S')
	and	ie_copiar = 'S'
	order by nr_seq_apres;

begin

ds_lista_w	:= ds_lista_p;

select	nvl(max(nr_agrupamento),0) + 1
into	nr_agrup_acum_w
from	prescr_material
where	nr_prescricao = nr_prescricao_p;

select	max(dt_inicio_prescr),
	max(dt_validade_prescr),
	max(dt_prescricao),
	max(nr_horas_validade),
	max(dt_primeiro_horario),
	nvl(max(cd_estabelecimento),1)
into	dt_inicio_prescr_w,
	dt_validade_prescr_w,
	dt_prescricao_w,
	nr_horas_validade_w,
	dt_primeiro_horario_w,
	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p
and		rownum = 1;


while ds_lista_w is not null loop
	begin
	tam_lista_w		:= length(ds_lista_w);
	ie_pos_virgula_w	:= instr(ds_lista_w,',');
	ds_prescricao_w		:= substr(ds_lista_w,1,(ie_pos_virgula_w - 1));
	ie_pos_espaco_w		:= instr(ds_prescricao_w,' ');
	tam_prescricao_w	:= length(ds_prescricao_w);
	nr_prescr_lista_w	:= to_number(substr(ds_prescricao_w,1,(ie_pos_espaco_w - 1)));
	nr_seq_lista_w		:= to_number(substr(ds_prescricao_w,(ie_pos_espaco_w + 1), tam_prescricao_w));
	
	PLT_consiste_extensao_item(dt_inicio_prescr_w, dt_validade_prescr_w, nr_prescr_lista_w, nr_seq_lista_w, 'S', nr_seq_regra_p, nm_usuario_p, cd_perfil_p, cd_estabelecimento_p);

	select	count(nr_sequencia)
	into	qt_inconsistencia_w
	from	w_copia_plano
	where	nr_prescricao	= nr_prescr_lista_w
	and	nr_seq_item	= nr_seq_lista_w
	and	ie_tipo_item	= 'S'
	and	nm_usuario	= nm_usuario_p
	and	((ie_permite	= 'N') or
		 (ie_estende_inc_p = 'N'))
	and	rownum = 1;
	
	if	((qt_inconsistencia_w	= 0) or
		 (ie_modificar_p 	= 'S')) then

		if	(nr_prescr_lista_w <> nr_prescr_ant_w) and
			(nr_prescr_ant_w is not null) then
			select	max(nr_agrupamento) + 1
			into	nr_agrup_acum_w
			from	prescr_material
			where	nr_prescricao	= nr_prescricao_p;
		end if;

		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p;
		
		select  a.ie_origem_inf,
			a.cd_material,
			a.cd_unidade_medida,
			a.qt_dose,
			a.qt_unitaria,
			a.qt_material,
			a.cd_intervalo,
			a.ds_horarios,
			a.ds_observacao,
			a.ds_observacao_enf,
			a.ie_via_aplicacao,
			a.nr_agrupamento,
			nvl(a.ie_cobra_paciente,'S'),
			decode(nvl(a.ie_regra_disp,'X'), 'D', a.cd_motivo_baixa, decode(nvl(a.ie_cobra_paciente,'S'), 'S', 0, a.cd_motivo_baixa)) cd_motivo_baixa,
			decode(nvl(a.ie_regra_disp,'X'), 'D', sysdate, decode(nvl(a.ie_cobra_paciente,'S'), 'S', null, sysdate)) dt_baixa,
			a.ie_utiliza_kit,
			a.cd_unidade_medida_dose,
			a.qt_conversao_dose,
			'N' ie_urgencia,
			a.nr_ocorrencia,
			a.qt_total_dispensar,
			a.cd_fornec_consignado,
			a.nr_sequencia_solucao,
			a.nr_sequencia_proc,
			a.qt_solucao,
			null hr_dose_especial,
			null qt_dose_especial,
			a.ds_dose_diferenciada,
			a.ie_medicacao_paciente,
			a.nr_sequencia_diluicao,
			decode(a.ie_se_necessario, 'S', null, a.hr_prim_horario) hr_prim_horario,
			decode(a.nr_sequencia_dieta, null,null,a.nr_sequencia_dieta) nr_sequencia_dieta, 
			a.ie_agrupador,
			a.nr_dia_util,
			decode(b.ie_situacao, 'A', 'N', 'S')  ie_suspenso,
			a.ie_se_necessario,
			a.qt_min_aplicacao,
			a.ie_bomba_infusao,
			nvl(a.ie_aplic_bolus,'N') ie_aplic_bolus,
			nvl(a.ie_aplic_lenta,'N') ie_aplic_lenta,
			nvl(a.ie_acm,'N') ie_acm,
			a.ie_objetivo,
			a.cd_topografia_cih,
			a.ie_origem_infeccao,
			a.cd_amostra_cih,
			a.cd_microorganismo_cih,
			nvl(a.ie_uso_antimicrobiano,'N') ie_uso_antimicrobiano,
			a.cd_protocolo,
			a.nr_seq_protocolo,
			a.nr_seq_mat_protocolo,
			a.qt_hora_aplicacao,
			'N' ie_recons_diluente_fixo,
			a.qt_vel_infusao,
			a.ds_justificativa,
			a.ie_sem_aprazamento,
			a.ie_indicacao,
			a.dt_proxima_dose,
			a.qt_total_dias_lib,
			a.nr_seq_substituto,
			a.ie_lado,
			a.dt_inicio_medic,
			a.qt_dia_prim_hor,
			decode(nvl(a.ie_regra_disp,'X'), 'D', a.ie_regra_disp, null) ie_regra_disp,
			a.qt_vol_adic_reconst,
			a.qt_hora_intervalo,
			a.qt_min_intervalo,
			a.dt_suspensao_progr,
			nvl(a.nr_prescricao_original,a.nr_prescricao),
			nvl(a.nr_seq_anterior,a.nr_sequencia)
		into	ie_origem_inf_w,
			cd_material_w,
			cd_unidade_medida_w,
			qt_dose_w,
			qt_unitaria_w,
			qt_material_w,
			cd_intervalo_w,
			ds_horarios_w,
			ds_observacao_w,
			ds_observacao_enf_w,
			ie_via_aplicacao_w,
			nr_agrupamento_w,
			ie_cobra_paciente_w,
			cd_motivo_baixa_w,
			dt_baixa_w,
			ie_utiliza_kit_w,
			cd_unidade_medida_dose_w,
			qt_conversao_dose_w,
			ie_urgencia_w,
			nr_ocorrencia_w,
			qt_total_dispensar_w,
			cd_fornec_consignado_w,
			nr_sequencia_solucao_w,
			nr_sequencia_proc_w,
			qt_solucao_w,
			hr_dose_especial_w,
			qt_dose_especial_w,
			ds_dose_diferenciada_w,
			ie_medicacao_paciente_w,
			nr_sequencia_diluicao_w,
			hr_prim_horario_w,
			nr_sequencia_dieta_w,
			ie_agrupador_w,
			nr_dia_util_w,
			ie_suspenso_w,
			ie_se_necessario_w,
			qt_min_aplicacao_w,
			ie_bomba_infusao_w,
			ie_aplic_bolus_w,
			ie_aplic_lenta_w,
			ie_acm_w,
			ie_objetivo_w,
			cd_topografia_cih_w,
			ie_origem_infeccao_w,
			cd_amostra_cih_w,
			cd_microorganismo_cih_w,
			ie_uso_antimicrobiano_w,
			cd_protocolo_w,
			nr_seq_protocolo_w,
			nr_seq_mat_protocolo_w,
			qt_hora_aplicacao_w,
			ie_recons_diluente_fixo_w,
			qt_vel_infusao_w,
			ds_justificativa_w,
			ie_sem_aprazamento_w,
			ie_indicacao_w,
			dt_proxima_dose_w,
			qt_total_dias_lib_w,
			nr_seq_substituto_w,
			ie_lado_w,
			dt_inicio_medic_w,
			qt_dia_prim_hor_w,
			ie_regra_disp_w,
			qt_vol_adic_reconst_w,
			qt_hora_intervalo_w,
			qt_min_intervalo_w,
			dt_suspensao_progr_w,
			nr_prescricao_original_w,
			nr_seq_anterior_w
		From	Material b,
			Prescr_Material a
		where	a.nr_prescricao = nr_prescr_lista_w
		and	a.nr_sequencia	= nr_seq_lista_w
		and	a.cd_material 	= b.cd_material;
		
		select	count(*)
		into	qt_w
		From	Material b,
			Prescr_Material a
		where	a.nr_prescricao = nr_prescr_lista_w
		and	a.nr_sequencia	= nr_seq_lista_w
		and	a.cd_material 	= b.cd_material
		and	((nvl(ie_copia_agora_p,'S')	= 'S') or
			((ie_copia_agora_p = 'N') and (nvl(a.ie_urgencia,'N') = 'N')))
		and	rownum = 1;
		
		select	max(ie_operacao)
		into	ie_operacao_w
		from 	intervalo_prescricao
		where 	cd_intervalo = cd_intervalo_w;
		
		if	(qt_w > 0) then
		
			open C01;
			loop
			fetch C01 into	
				ie_regra_geral_w,
				ie_manter_intervalo_w;
			exit when C01%notfound;
				begin
				ie_regra_geral_w := ie_regra_geral_w;
				ie_manter_intervalo_w := ie_manter_intervalo_w;
				end;
			end loop;
			close C01;
			
			ds_horarios_ww := ds_horarios_w;
			
			if	(ie_regra_geral_w = 'I') and
				(obter_se_intervalo_agora(cd_intervalo_w) = 'N') then
				hr_prim_horario_w := null;
				ds_horarios_w := reordenar_horarios(dt_inicio_prescr_w, ds_horarios_w);
			
				hr_prim_horario_w	:= nvl(Obter_prim_DsHorarios(ds_horarios_w),to_char(dt_inicio_prescr_w,'hh24:mi'));
			else
				nr_ocorrencia_w		:= 0;
				
				if	(ie_operacao_w in ('F','V')) then
					hr_prim_horario_w := obter_primeiro_horario(cd_intervalo_w,nr_prescricao_p,cd_material_w,ie_via_aplicacao_w);
				elsif (ie_manter_intervalo_w = 'N') then
				
					dt_ultimo_horario_w	:= PLT_obter_ultimo_horario(nr_prescr_lista_w, nr_seq_lista_w, 'S', nm_usuario_p);

					if	(dt_ultimo_horario_w is not null) and
						(dt_ultimo_horario_w > (sysdate - 1)) then
						nr_ocorrencia_aux_w	:= Obter_ocorrencia_intervalo(cd_intervalo_w, nvl(nr_horas_validade_w,24),'H')/ 24;
						dt_prim_hor_aux_w	:= dt_ultimo_horario_w + nr_ocorrencia_aux_w;
						if	(dt_prim_hor_aux_w	< dt_inicio_prescr_w) and (dt_inicio_prescr_w	< (sysdate + 1)) then
							WHILE dt_prim_hor_aux_w < dt_inicio_prescr_w
								LOOP
								   dt_prim_hor_aux_w	:= dt_prim_hor_aux_w + nr_ocorrencia_aux_w;
								END LOOP;
						end if;
						hr_prim_horario_aux_w	:= to_char(dt_prim_hor_aux_w,'hh24:mi');
						if	(hr_prim_horario_aux_w <> '  :  ') and
							(hr_prim_horario_aux_w is not null) then
							hr_prim_horario_w	:= hr_prim_horario_aux_w;
						end if;							
					end if;
				end if;

				dt_prim_horario_int_w	:= converte_char_data(to_char(dt_primeiro_horario_w,'dd/mm/yyyy'),hr_prim_horario_w ||':00',dt_primeiro_horario_w);
				
				calcular_horario_prescricao(nr_prescricao_p, cd_intervalo_w, dt_prim_horario_int_w, nvl(dt_prim_horario_int_w, dt_primeiro_horario_w), nr_horas_validade_w, cd_material_w,
								qt_hora_intervalo_w, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w,ds_horarios2_w,'N', ds_dose_diferenciada_w);
								
				ds_horarios_w := ds_horarios_w || ds_horarios2_w;
			end if;
			
			ds_horarios_w := Eliminar_horarios_vigencia(ds_horarios_w, cd_intervalo_w, dt_inicio_prescr_w, dt_inicio_prescr_w, qt_hora_intervalo_w, qt_min_intervalo_w, nr_prescricao_p);
			
			if (ds_horarios_w is null) and 
			   (ds_horarios_ww is not null) and
			   (nvl(ie_regra_geral_w,'XPTO') <> 'I') and 
			   (ie_operacao_w not in ('F','V')) then
			   
			    nr_ocorrencia_w		:= 0;
				
				calcular_horario_prescricao(nr_prescricao_p, cd_intervalo_w, dt_primeiro_horario_w, dt_primeiro_horario_w, nr_horas_validade_w, cd_material_w,
								qt_hora_intervalo_w, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w,ds_horarios2_w,'N', ds_dose_diferenciada_w);
								
				ds_horarios_w := ds_horarios_w || ds_horarios2_w;
				
				ds_horarios_w := Eliminar_horarios_vigencia(ds_horarios_w, cd_intervalo_w, dt_inicio_prescr_w, dt_inicio_prescr_w, qt_hora_intervalo_w, qt_min_intervalo_w, nr_prescricao_p);
			
				if (ds_horarios_w is not null) then
					hr_prim_horario_w	:= nvl(Obter_prim_DsHorarios(ds_horarios_w),to_char(dt_inicio_prescr_w,'hh24:mi'));
				end if;
			end if;
			
			if	(hr_prim_horario_w <> '')	then
				dt_prim_horario_int_w 	:= converte_char_data(to_char(dt_prescricao_w,'dd/mm/yyyy'),hr_prim_horario_w ||':00',null);
			end if;
			
			insert  into prescr_material (
					nr_prescricao,
					nr_sequencia,
					ie_origem_inf,
					cd_material,
					cd_unidade_medida,
					qt_dose,
					qt_unitaria,
					qt_material,
					dt_atualizacao,
					nm_usuario,
					cd_intervalo,
					ds_horarios,
					ds_observacao,
					ds_observacao_enf,
					ie_via_aplicacao,
					nr_agrupamento,
					ie_cobra_paciente,
					cd_motivo_baixa,
					dt_baixa,
					ie_utiliza_kit,
					cd_unidade_medida_dose,
					qt_conversao_dose,
					ie_urgencia,
					nr_ocorrencia,
					qt_total_dispensar,
					cd_fornec_consignado,
					nr_sequencia_solucao,
					nr_sequencia_proc,
					qt_solucao,
					hr_dose_especial,
					qt_dose_especial,
					ds_dose_diferenciada,
					ie_medicacao_paciente,
					nr_sequencia_diluicao,
					hr_prim_horario,
					nr_sequencia_dieta,
					ie_agrupador,
					nr_dia_util,
					ie_suspenso,
					ie_se_necessario,
					qt_min_aplicacao,
					ie_bomba_infusao,
					ie_aplic_bolus,
					ie_aplic_lenta,
					ie_acm,
					ie_objetivo,
					cd_topografia_cih,
					ie_origem_infeccao,
					cd_amostra_cih,
					cd_microorganismo_cih,
					ie_uso_antimicrobiano,
					cd_protocolo,
					nr_seq_protocolo,
					nr_seq_mat_protocolo,
					qt_hora_aplicacao,
					ie_recons_diluente_fixo,
					qt_vel_infusao,
					ds_justificativa,
					ie_sem_aprazamento,
					ie_indicacao,
					dt_proxima_dose,
					qt_total_dias_lib,
					nr_seq_substituto,
					ie_lado,
					dt_inicio_medic,
					qt_dia_prim_hor,
					ie_regra_disp,
					qt_vol_adic_reconst,
					qt_hora_intervalo,
					qt_min_intervalo,
					dt_suspensao_progr,
					nr_prescricao_original,
					nr_seq_anterior)
			values(		nr_prescricao_p,
					nr_sequencia_w,
					ie_origem_inf_w,
					cd_material_w,
					cd_unidade_medida_w,
					qt_dose_w,
					qt_unitaria_w,
					qt_material_w,
					sysdate,
					nm_usuario_p,
					cd_intervalo_w,
					ds_horarios_w,
					ds_observacao_w,
					ds_observacao_enf_w,
					ie_via_aplicacao_w,
					nr_agrupamento_w + nr_agrup_acum_w,
					ie_cobra_paciente_w,
					cd_motivo_baixa_w,
					dt_baixa_w,
					ie_utiliza_kit_w,
					cd_unidade_medida_dose_w,
					qt_conversao_dose_w,
					ie_urgencia_w,
					nr_ocorrencia_w,
					qt_total_dispensar_w,
					cd_fornec_consignado_w,
					nr_sequencia_solucao_w,
					nr_sequencia_proc_w,
					qt_solucao_w,
					hr_dose_especial_w,
					qt_dose_especial_w,
					ds_dose_diferenciada_w,
					ie_medicacao_paciente_w,
					nr_sequencia_diluicao_w,
					hr_prim_horario_w,
					nr_sequencia_dieta_w,
					ie_agrupador_w,
					nr_dia_util_w,
					ie_suspenso_w,
					ie_se_necessario_w,
					qt_min_aplicacao_w,
					ie_bomba_infusao_w,
					ie_aplic_bolus_w,
					ie_aplic_lenta_w,
					ie_acm_w,
					ie_objetivo_w,
					cd_topografia_cih_w,
					ie_origem_infeccao_w,
					cd_amostra_cih_w,
					cd_microorganismo_cih_w,
					ie_uso_antimicrobiano_w,
					cd_protocolo_w,
					nr_seq_protocolo_w,
					nr_seq_mat_protocolo_w,
					qt_hora_aplicacao_w,
					ie_recons_diluente_fixo_w,
					qt_vel_infusao_w,
					ds_justificativa_w,
					ie_sem_aprazamento_w,
					ie_indicacao_w,
					dt_proxima_dose_w,
					qt_total_dias_lib_w,
					nr_seq_substituto_w,
					ie_lado_w,
					dt_inicio_medic_w,
					qt_dia_prim_hor_w,
					ie_regra_disp_w,
					qt_vol_adic_reconst_w,
					qt_hora_intervalo_w,
					qt_min_intervalo_w,
					dt_suspensao_progr_w,
					nr_prescricao_original_w,
					nr_seq_anterior_w);
					
					Obter_Quant_Dispensar(cd_estabelecimento_w, cd_material_w, nr_prescricao_p, nr_sequencia_w, cd_intervalo_w, ie_via_aplicacao_w, qt_unitaria_w, 0, nr_ocorrencia_w, ds_dose_diferenciada_w,
											ie_origem_inf_w, cd_unidade_medida_dose_w, 1, qt_material_w, qt_total_dispensar_w, ie_regra_disp_w, ds_erro_w, ie_se_necessario_w, ie_acm_w);

					update	prescr_material
					set 	nr_ocorrencia	= nr_ocorrencia_w,
							qt_total_dispensar	= qt_total_dispensar_w,
							qt_material		= nvl(qt_material_w,1),		
							ie_regra_disp	= decode(nvl(ie_regra_disp,'X'), 'D', ie_regra_disp, ie_regra_disp_w)
					where	nr_prescricao 	= nr_prescricao_p
					and	nr_sequencia		= nr_sequencia_w;
					
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;	

		end if;
	
	end if;
	
	nr_prescr_ant_w			:= nr_prescr_lista_w;
	ds_lista_w			:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);	
	end;
end loop;	

end PLT_copia_suplemento;
/