create or replace
procedure pls_obter_dados_local_cep(    cd_cep_p                in     number,
                                        ie_cep_internet_p       in     varchar2, 
                                        nm_logradouro_p         out    varchar2,
                                        nm_localidade_p         out    varchar2,
                                        nm_bairro_p             out    varchar2,
                                        ds_uf_p                 out    varchar2,
                                        cd_cep_retorno_p        out    varchar2,
                                        nr_seq_regiao_p         out    varchar2,
                                        cd_municipio_ibge_p     out    varchar2,
                                        cd_tipo_logradouro_p    out    varchar2     ) is 
                                        
/* ie_cep_internet_p = 'S' ou 'N'*/ 


nm_logradouro_w             varchar2(500);
nm_localidade_w             varchar2(500);
nm_bairro_w                 varchar2(500);
ds_uf_w                     valor_dominio.vl_dominio%type;
cd_cep_retorno_w            varchar2(500);
nr_seq_regiao_w             varchar2(500);
cd_municipio_ibge_w         varchar2(500);
ds_tipo_logradouro_w        varchar2(2000);
cd_tipo_logradouro_w        varchar2(500);                            

begin

    if    (ie_cep_internet_p = 'S') then
        
            select  a.nm_logradouro, 
                    a.nm_localidade, 
                    a.nm_bairro, 
                    a.cd_unidade_federacao, 
                    a.nr_logradouro, 
                    null
            into    nm_logradouro_w,
                    nm_localidade_w,
                    nm_bairro_w,
                    ds_uf_w,
                    cd_cep_retorno_w,
                    nr_seq_regiao_w        
            from    cep_logradouro a  
            where   a.cd_logradouro = cd_cep_p;
            
    elsif    (ie_cep_internet_p = 'N') then
    
            select  a.nm_logradouro,  
                    b.nm_localidade,  
                    c.ds_bairro,   
                    a.ds_uf,           
                    a.cd_cep,          
                    c.nr_seq_regiao
            into    nm_logradouro_w,
                    nm_localidade_w,
                    nm_bairro_w,
                    ds_uf_w,
                    cd_cep_retorno_w,
                    nr_seq_regiao_w  
            from    cep_loc b,              
                    cep_bairro c,           
                    cep_log a               
            where   b.nr_sequencia      = c.nr_seq_loc  
            and     a.cd_bairro_inicial = c.nr_sequencia  
            and     b.nr_sequencia      = a.nr_seq_loc  
            and     a.cd_cep            = cd_cep_p;
            
            if (cd_cep_retorno_p is null) then
            
                select  null log,  
                        b.nm_localidade loc,  
                        null bairro,  
                        b.ds_uf uf,       
                        b.cd_cep cep,      
                        null reg
                into    nm_logradouro_w,
                        nm_localidade_w,
                        nm_bairro_w,
                        ds_uf_w,
                        cd_cep_retorno_w,
                        nr_seq_regiao_w          
                from    cep_loc b         
                where   b.cd_cep       = cd_cep_p;
            
            end if;
            
    end if;
    
    if(ie_cep_internet_p is not null) then
    
            select  max(cd_municipio_ibge) cd_municipio_ibge
            into    cd_municipio_ibge_w
            from    (   select  cd_municipio_ibge
                        from    cep_municipio
                        where   cd_cep = cd_cep_p
                        union
                        select  cd_municipio_ibge
                        from    sus_cep
                        where   cd_cep = cd_cep_p   );
                        
            select  ds_tipo_logradouro
            into    ds_tipo_logradouro_w 
            from    cep_logradouro_v 
            where   cd_cep = cd_cep_p;
            
            select  cd_tipo_logradouro
            into    cd_tipo_logradouro_w 
            from    sus_tipo_logradouro
            where   upper(ds_tipo_logradouro) = upper(ds_tipo_logradouro_w);
    
    end if;
    
    nm_logradouro_p         := '';
    nm_localidade_p         := '';
    nm_bairro_p             := '';
    ds_uf_p                 := '';
    cd_cep_retorno_p        := '';
    nr_seq_regiao_p         := '';
    cd_municipio_ibge_p     := '';
    cd_tipo_logradouro_p    := '';
    
    if( nm_logradouro_w is not null) then
        nm_logradouro_p := nm_logradouro_w;    
    end if;
                    
    if( nm_localidade_w is not null) then
        nm_localidade_p := nm_localidade_w;    
    end if;
    
    if( nm_bairro_w is not null) then
        nm_bairro_p := nm_bairro_w;    
    end if;
    
    if( ds_uf_w is not null) then
        ds_uf_p := ds_uf_w;    
    end if;              
    
    if( cd_cep_retorno_w is not null) then
        cd_cep_retorno_p := cd_cep_retorno_w;    
    end if;
    
    if( nr_seq_regiao_w is not null) then
        nr_seq_regiao_p := nr_seq_regiao_w;    
    end if;
    
    if( cd_municipio_ibge_w is not null) then
        cd_municipio_ibge_p := cd_municipio_ibge_w;    
    end if;
    
    if( cd_tipo_logradouro_w is not null) then
        cd_tipo_logradouro_p := cd_tipo_logradouro_w;    
    end if;                             

end pls_obter_dados_local_cep;
/