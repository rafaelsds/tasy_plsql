create or replace
procedure Inserir_indic_clinica_convenio(
			nr_sequencia_p		number,
         ds_indicacao_p    varchar2) is 

begin

if (nr_sequencia_p is not null) then
   
   update autorizacao_convenio
   set    ds_indicacao = ds_indicacao_p
   where  nr_Sequencia = nr_Sequencia_p;
   
 end if;
commit;

end Inserir_indic_clinica_convenio;
/