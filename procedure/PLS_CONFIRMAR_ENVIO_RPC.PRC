create or replace
procedure pls_confirmar_envio_rpc
			(	nr_seq_lote_p	number,
				ie_desfazer_p	varchar2,
				nm_usuario_p	varchar2) is

begin

if	(ie_desfazer_p = 'S') then
	update	pls_rpc_lote
	set	dt_envio	= null,
		nm_usuario_envio = null
	where	nr_sequencia	= nr_seq_lote_p;
else
	update	pls_rpc_lote
	set	dt_envio	= sysdate,
		nm_usuario_envio = nm_usuario_p
	where	nr_sequencia	= nr_seq_lote_p;
end if;

commit;

end pls_confirmar_envio_rpc;
/