create or replace
procedure sus_renumerar_laudos_lote
		(nr_seq_lote_p		number,
		nr_inicial_p		number) is

nr_seq_ordem_w			number(10,0);
nr_seq_interno_w		sus_laudo_paciente.nr_seq_interno%type;
qt_laudo_w			number(10,0) := 0;
nr_seq_int_laudo_w		sus_laudo_paciente.nr_seq_interno%type := 0;
nr_seq_lote_w			sus_laudo_paciente.nr_seq_interno%type := 0;
ie_tipo_laudo_sus_w		sus_laudo_paciente.ie_tipo_laudo_sus%type;
ie_consiste_seq_laudo_w		varchar2(1);
ie_consiste_conta_laudo_w	varchar(1);
nr_interno_conta_w		sus_laudo_paciente.nr_interno_conta%type;
qt_contas_existentes_w		number(10) := 0;
dt_emissao_w			sus_laudo_paciente.dt_emissao%type;

cursor	c01 is
	select	nr_seq_interno,
		ie_tipo_laudo_sus,
		nr_interno_conta
	from	sus_laudo_paciente
	where	nr_seq_lote	= nr_seq_lote_p
	order by nr_atendimento desc, nr_interno_conta, ie_tipo_laudo_sus, nr_seq_interno;
	
type 		fetch_array is table of c01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c01_w			vetor;

begin
ie_consiste_seq_laudo_w := obter_valor_param_usuario(1006, 21, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
ie_consiste_conta_laudo_w := obter_valor_param_usuario(1006, 59, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

nr_seq_ordem_w	:= nr_inicial_p - 1;

update 	sus_lote_autor
set	nr_inicial_ordenacao = nr_inicial_p
where	nr_sequencia	= nr_seq_lote_p;

open c01;
loop
fetch c01 bulk collect into s_array limit 100000;
	vetor_c01_w(i) := s_array;
	i := i + 1;
exit when c01%notfound;
end loop;
close c01;

for i in 1..vetor_c01_w.count loop
	begin
	
	s_array := vetor_c01_w(i);
	
	for z in 1..s_array.count loop
		begin

		nr_seq_interno_w	:= s_array(z).nr_seq_interno;
		ie_tipo_laudo_sus_w	:= s_array(z).ie_tipo_laudo_sus;
		nr_interno_conta_w	:= s_array(z).nr_interno_conta;


		if 	(ie_tipo_laudo_sus_w = 0) then
			nr_seq_ordem_w	:= nr_seq_ordem_w + 1;
		end if;
	
		if	(nvl(ie_consiste_seq_laudo_w,'N') = 'S') then
			begin
			begin
			select	nr_seq_interno,
				nr_seq_lote,
				dt_emissao
			into	nr_seq_int_laudo_w,
				nr_seq_lote_w,
				dt_emissao_w
			from	sus_laudo_paciente
			where	nr_ordem_laudo	= nr_seq_ordem_w
			and	nr_seq_lote	<> nr_seq_lote_p
			and	ie_tipo_laudo_sus = 0
			and	trunc(dt_emissao,'yyyy') >= trunc(sysdate,'yyyy')
			and	rownum = 1;		
			exception
			when others then
--			qt_laudo_w		:= 0;
				nr_seq_int_laudo_w	:= 0;
				nr_seq_lote_w		:= 0;
			end;
		
			end;
		end if;
	
		if	(nvl(ie_consiste_conta_laudo_w,'N') = 'S') then
			begin
			
			begin
			select	count(nr_seq_interno)
			into	qt_contas_existentes_w
			from	sus_laudo_paciente
			where	nr_ordem_laudo	= nr_seq_ordem_w
			and	nr_interno_conta <> nr_interno_conta_w
			and	nvl(nr_interno_conta,0) <> 0
			and	ie_tipo_laudo_sus = 0
			and	trunc(dt_emissao,'yyyy') >= trunc(sysdate,'yyyy');		
			exception
			when others then
				qt_contas_existentes_w := 0;
			end;
			
			end;
		end if;

		if	((nvl(nr_seq_int_laudo_w,0) = 0) and (nvl(qt_contas_existentes_w,0) = 0)) then
			begin
			
			update	sus_laudo_paciente
			set	nr_ordem_laudo		= nr_seq_ordem_w
			where	nr_seq_interno		= nr_seq_interno_w;
			
			commit;
			
			end;		
		else
			if	(nvl(nr_seq_int_laudo_w,0) > 0) then
				begin
				wheb_mensagem_pck.exibir_mensagem_abort(250270, 'NR_SEQ_INTERNO='|| nr_seq_interno_w ||
										';NR_INTERNO_CONTA='|| nr_interno_conta_w ||
										';NR_SEQ_INT_LAUDO=' || nr_seq_int_laudo_w ||
										';NR_SEQ_LOTE=' || nr_seq_lote_w ||
										';NR_SEQ_ORDEM=' || nr_seq_ordem_w ||
										';DT_EMISSAO='|| dt_emissao_w );				
				end;
			else
				begin
				wheb_mensagem_pck.exibir_mensagem_abort(270823, 'QT_CONTA_P=' || qt_contas_existentes_w);
				end;
			end if;
		end if;
	
		end;	
	end loop;
	
	end;
end loop;

end sus_renumerar_laudos_lote;
/
