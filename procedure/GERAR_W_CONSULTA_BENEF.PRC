create or replace
procedure Gerar_W_Consulta_Benef(
			ie_resultado_p		varchar2,
			ds_mensagem_p		varchar2,
			qt_beneficiarios_p	varchar2,
			cd_modalidade_p		varchar2,
			ds_modalidade_p		varchar2,
			cd_plano_p		varchar2,
			ds_plano_p		varchar2,
			cd_tipo_plano_p		varchar2,
			ds_tipo_plano_p		varchar2,
			nm_plano_p		varchar2,
			cd_usuario_convenio_p	varchar2,
			nm_pessoa_fisica_p	varchar2,
			nr_cpf_p		varchar2,
			nr_identidade_p		varchar2,
			ds_orgao_emissor_p	pessoa_fisica.ds_orgao_emissor_ci%type,
			sg_uf_orgao_emissor_p	varchar2,
			ds_pais_rg_p		varchar2,
			ie_sexo_p		varchar2,
			dt_nascimento_p		date,
			nm_mae_p		varchar2,
			cd_cep_p		varchar2,
			cd_cidade_p		varchar2,
			ds_cidade_p		varchar2,
			sg_uf_p			varchar2,
			ds_bairro_p		varchar2,
			ds_logradouro_p		varchar2,
			nr_endereco_p		varchar2,
			ds_complemento_p	varchar2,
			nr_telefone_1_p		varchar2,
			nr_telefone_2_p		varchar2,
			ds_email_p		varchar2,
			ie_status_p		varchar2,
			nm_usuario_p		Varchar2) is 
		
cd_pessoa_fisica_w	varchar2(10)	:= '';

begin

begin
select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	pessoa_fisica
where	nr_cpf = nr_cpf_p;
exception
	when others then
	cd_pessoa_fisica_w	:= '';
end;

insert into w_consulta_beneficiario (       
	cd_usuario_convenio,     
	dt_atualizacao,          
	dt_atualizacao_nrec,     
	dt_nascimento,           
	nm_pessoa_fisica,        
	nm_usuario,              
	nm_usuario_nrec,         
	nr_cpf,	
	ie_resultado,		
	ds_mensagem,		
	qt_beneficiarios,	
	cd_modalidade,		
	ds_modalidade,		
	cd_plano,		
	ds_plano,		
	cd_tipo_plano,		
	ds_tipo_plano,		
	nm_plano,		
	nr_identidade,		
	ds_orgao_emissor,	
	sg_uf_orgao_emissor,	
	ds_pais_rg,		
	ie_sexo,			
	nm_mae,			
	cd_cep,			
	cd_cidade,		
	ds_cidade,		
	sg_uf,			
	ds_bairro,		
	ds_logradouro,		
	nr_endereco,		
	ds_complemento,		
	nr_telefone_1,		
	nr_telefone_2,		
	ds_email,		
	ie_status,
	cd_pessoa_fisica)
values	(
	cd_usuario_convenio_p,
	sysdate,
	sysdate,
	dt_nascimento_p,
	nm_pessoa_fisica_p,
	nm_usuario_p,
	nm_usuario_p,
	nr_cpf_p,	
	ie_resultado_p,		
	ds_mensagem_p,		
	qt_beneficiarios_p,	
	cd_modalidade_p,		
	ds_modalidade_p,		
	cd_plano_p,		
	ds_plano_p,		
	cd_tipo_plano_p,		
	ds_tipo_plano_p,		
	nm_plano_p,		
	nr_identidade_p,		
	ds_orgao_emissor_p,	
	sg_uf_orgao_emissor_p,	
	ds_pais_rg_p,		
	ie_sexo_p,			
	nm_mae_p,			
	cd_cep_p,			
	cd_cidade_p,		
	ds_cidade_p,		
	sg_uf_p,			
	ds_bairro_p,		
	ds_logradouro_p,		
	nr_endereco_p,		
	ds_complemento_p,		
	nr_telefone_1_p,		
	nr_telefone_2_p,		
	ds_email_p,		
	ie_status_p,
	cd_pessoa_fisica_w);

commit;

end Gerar_W_Consulta_Benef;
/
