CREATE OR REPLACE
PROCEDURE CTB_Atualizar_todos_lotes(	nr_seq_mes_ref_p		number,
					cd_estabelecimento_p	Number,
					nm_usuario_p		varchar2,
					ds_erro_p		out	Varchar2) is

dt_abertura_w				Date;
dt_fechamento_w				Date;
nr_lote_contabil_w				Number(15,0);
dt_consistencia_w				Date;
ds_erro_w				Varchar2(2000);
ds_lotes_w				Varchar2(2000);

Cursor C01 is
Select	nr_lote_contabil,
	dt_consistencia
from	lote_contabil
where	nr_seq_mes_ref		= nr_seq_mes_ref_p
and	cd_estabelecimento		= cd_estabelecimento_p
and	dt_atualizacao_saldo	is null;

BEGIN

select dt_abertura,
	dt_fechamento
into 	dt_abertura_w,
	dt_fechamento_w
from	ctb_mes_ref
where	nr_sequencia	= nr_seq_mes_ref_p;

if	(dt_abertura_w is null)  then
	--(-20011,'O m�s n�o est� aberto');
	Wheb_mensagem_pck.exibir_mensagem_abort(269640);
end if;

if	(dt_fechamento_w is not null) then
	--(-20011,'O m�s est� fechado');
	Wheb_mensagem_pck.exibir_mensagem_abort(269641);
end if;

ds_lotes_w	:= '';

OPEN C01;
LOOP
FETCH C01 into
	nr_lote_contabil_w,
	dt_consistencia_w;
exit when c01%notfound;
	ds_erro_w	:= '';
	if	(dt_consistencia_w is null) then
		ctb_consistir_lote(nr_lote_contabil_w, ds_erro_w,nm_usuario_p);
		if	(ds_erro_w is not null) then
			ds_lotes_w		:= ds_lotes_w || nr_lote_contabil_w || ',';
		else
			dt_consistencia_w	:= sysdate;
		end if;
	end if;
	if	(dt_consistencia_w is not null) then		
		begin
		ctb_atualizar_saldo(nr_lote_contabil_w, 'N', nm_usuario_p,'N', ds_erro_w,'S');
		exception
		when others then
			ds_lotes_w	:= ds_lotes_w || nr_lote_contabil_w || ',';
		end;
	end if;
END LOOP;
CLOSE C01;

ctb_atualizar_dt_processo(	cd_estabelecimento_p, nr_seq_mes_ref_p, null, nm_usuario_p, null);

CTB_Acumular_Saldo(nr_seq_mes_ref_p, cd_estabelecimento_p, nm_usuario_p, ds_erro_w);

ds_erro_p := '';
if	(ds_lotes_w is not null) then
	ds_erro_p		:= substr(ds_lotes_w || chr(10) || chr(13) || ds_erro_w, 1, 254);
end if;

commit;

END CTB_Atualizar_todos_lotes;
/
