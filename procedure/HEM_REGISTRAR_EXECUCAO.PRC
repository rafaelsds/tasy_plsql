create or replace
procedure hem_registrar_execucao(	nr_seq_proc_p		Number,
									nr_seq_plan_p		Number,
									ds_observacao_rel_p	Long,
									nm_usuario_p		Varchar2) is 

begin
if	(nvl(nr_seq_proc_p,0) > 0) then

	insert into hem_plan_etapa (	
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_proc,
				nr_seq_plan,
				ds_observacao_rel,
				nm_usuario_rel,
				dt_realizada)
	values(		hem_plan_etapa_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_proc_p,
				nr_seq_plan_p,
				ds_observacao_rel_p,
				nm_usuario_p,
				sysdate);
	
end if;
commit;

end hem_registrar_execucao;
/