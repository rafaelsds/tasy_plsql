create or replace
procedure qr_altera_status_param(nr_sequencia_p	number,
								cd_funcao_p		number,
								nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) then
insert into qr_parametro_result (	
			nr_sequencia,
			nm_usuario_finalizacao,
			dt_finalizacao,
			ie_finalizado,
			nr_seq_param,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_resultado,
			ie_tipo_resultado,
			cd_funcao)
		values(	qr_parametro_result_seq.nextVal,
			nm_usuario_p,
			sysdate,
			'S',
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			obter_desc_expressao(801946)||' '||nm_usuario_p||'.',
			'S',
			cd_funcao_p);
end if;

commit;

end qr_altera_status_param;
/