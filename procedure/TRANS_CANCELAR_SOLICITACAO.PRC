CREATE OR REPLACE
PROCEDURE Trans_cancelar_solicitacao(	nr_sequencia_p 		NUMBER,
				nr_seq_motivo_cancel_p	NUMBER DEFAULT NULL,
				ds_motivo_cancel_p	VARCHAR2 DEFAULT NULL,
				nm_usuario_p		VARCHAR2) IS 						
BEGIN
IF ((obter_valor_param_usuario(10050, 7, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento)) = 'S') THEN
 UPDATE transportador
 SET ie_status_recurso = 1
 WHERE cd_pessoa_fisica = (SELECT s.cd_transportador FROM trans_solicitacao s
						 WHERE s.cd_transportador IS NOT NULL
 					     AND s.nr_sequencia = nr_sequencia_p);
END IF;

UPDATE	trans_solicitacao
SET	dt_cancelamento = SYSDATE,
	cd_motivo_cancelamento = nr_seq_motivo_cancel_p,
	ds_obs_cancelamento = ds_motivo_cancel_p,
	ie_status = 9,
	dt_atualizacao = SYSDATE,
	nm_usuario = nm_usuario_p
WHERE	nr_sequencia = nr_sequencia_p;

COMMIT;

END Trans_cancelar_solicitacao;
/