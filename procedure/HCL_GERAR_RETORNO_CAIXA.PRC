create or replace
procedure hcl_gerar_retorno_caixa(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p	varchar2) is 

nr_seq_reg_t_w				number(10);
nr_seq_reg_u_w				number(10);
nr_titulo_w				number(10);
vl_titulo_w				number(15,2);
vl_acrescimo_w				number(15,2);
vl_desconto_w				number(15,2);
vl_abatimento_w				number(15,2);
vl_liquido_w				number(15,2);
vl_outras_despesas_w			number(15,2);
dt_liquidacao_w				date;
ds_titulo_w				varchar2(255);
vl_cobranca_w				number(15,2);
vl_alterar_w				number(15,2);
cd_ocorrencia_w				banco_ocorr_escrit_ret.cd_ocorrencia%type;
nr_seq_ocorrencia_ret_w			number(10);
vl_saldo_inclusao_w			number(15,2);
ds_observacao_w				varchar2(255);
ds_titulo_hcl_w				varchar2(255);
ds_nosso_num_w				varchar2(255);
vl_tarifa_banco_w				number(15,2);

	/* objeto customizado de acordo com as necessidades do hospital do c�ncer de londrina, os 998650 */

cursor c01 is
	select	nr_sequencia,
		trim(substr(ds_string,59,11)),
		substr(ds_string,47,10),
		substr(ds_string,40,17),
		to_number(substr(ds_string,82,15))/100,
		to_number(substr(ds_string,199,15))/100
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,8,1)	= '3'
	and	substr(ds_string,14,1)	= 'T'
	and	substr(ds_string,16,2)	<> '28';
begin

open c01;
loop
fetch c01 into	
		nr_seq_reg_t_w,
		ds_titulo_w,
		ds_titulo_hcl_w,
		ds_nosso_num_w,
		vl_cobranca_w,
		vl_tarifa_banco_w;
exit when c01%notfound;
	begin

	vl_alterar_w	:= 0;

	/* encontrar pelo t�tulo externo */
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo_externo = ds_titulo_w
	and	NR_SEQ_CLASSE = 21;

	/* se n�o econtrou, procura pelo t�tulo no tasy */
	if	(nr_titulo_w is null) then
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_titulo	= somente_numero(ds_titulo_w)
		and	NR_SEQ_CLASSE = 21;
	end if;
	
	/* 	conforme verificado durante conex�o (os 998650), 
		o n�mero do t�tulo deve ser buscado a partir da posi��o 47 (10 posi��es anteriores � posi��o final do nosso n�mero), 
		j� que os arquivos de retorno trazem as posi��es referentes ao seu n�mero zeradas (posi��o 59 at� 69, de acordo com o layout) .  */
	if	(nr_titulo_w is null) then
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_titulo	= somente_numero(ds_titulo_hcl_w)
		and	NR_SEQ_CLASSE = 21;
	end if;
	
	/* 	conforme verificado durante conex�o (os 998650), 
		caso n�o encontre comparando ao nr_titulo no tasy, deve-se procurar pelo nr_nosso_numero.  */
	if	(nr_titulo_w is null) then
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_nosso_numero	= ds_nosso_num_w
		and	NR_SEQ_CLASSE = 21;
	end if;

	/* se encontrou o t�tulo importa, sen�o grava no log */

	if	(nr_titulo_w is not null) then

		select	max(vl_titulo),
				max(vl_saldo_titulo)
		into	vl_titulo_w,
				vl_saldo_inclusao_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		nr_seq_reg_u_w := nr_seq_reg_t_w + 1;

		select	nvl(to_number(substr(ds_string,18,15))/100,0),
				nvl(to_number(substr(ds_string,33,15))/100,0),
				nvl(to_number(substr(ds_string,48,15))/100,0),
				nvl(to_number(substr(ds_string,93,15))/100,0),
				nvl(to_number(substr(ds_string,108,15))/100,0),
				to_date(decode(substr(ds_string,146,8),'00000000',null,substr(ds_string,146,8)),'dd/mm/yyyy'),
				substr(ds_string,16,2)
		into	vl_acrescimo_w,
				vl_desconto_w,
				vl_abatimento_w,
				vl_liquido_w,
				vl_outras_despesas_w,
				dt_liquidacao_w,
				cd_ocorrencia_w
		from	w_retorno_banco
		where	nr_sequencia	= nr_seq_reg_u_w;

		if	(cd_ocorrencia_w is not null) then
			select 	max(a.nr_sequencia)
			into	nr_seq_ocorrencia_ret_w
			from	banco_ocorr_escrit_ret a
			where	a.cd_banco	= 104
			and		a.cd_ocorrencia = cd_ocorrencia_w;
		end if;
		
		/* tratar acrescimos/descontos */
		if	(vl_titulo_w <> vl_liquido_w) then
			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;	
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;
		end if;

		/*	conforme verificado durante conex�o (os 998650), 
			Valida��o dos valores referentes �s Despesas Banc�rias.
		*/
		insert	into titulo_receber_cobr (
					nr_sequencia,
					nr_titulo,
					cd_banco,
					vl_cobranca,
					vl_desconto,
					vl_acrescimo,
					vl_despesa_bancaria,
					vl_liquidacao,
					dt_liquidacao,
					dt_atualizacao,
					nm_usuario,
					nr_seq_cobranca,
					nr_seq_ocorrencia_ret,
					vl_saldo_inclusao)
		values	(	titulo_receber_cobr_seq.nextval,
					nr_titulo_w,
					104,
					vl_titulo_w,
					vl_desconto_w,
					vl_acrescimo_w,
					vl_outras_despesas_w + vl_tarifa_banco_w,
					vl_liquido_w,
					dt_liquidacao_w,
					sysdate,
					nm_usuario_p,
					nr_seq_cobr_escrit_p,
					nr_seq_ocorrencia_ret_w,
					vl_saldo_inclusao_w);
					
	else
		ds_observacao_w := substr(wheb_mensagem_pck.get_texto(302700,'ds_titulo_w=' || somente_numero(ds_titulo_hcl_w)),1,4000);
		
		insert into cobranca_escrit_log
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nr_seq_cobranca,
					ds_log,
					vl_saldo_titulo,
					nr_seq_ocorrencia_ret,
					nr_nosso_numero,
					nr_titulo)
		values	(	cobranca_escrit_log_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_cobr_escrit_p,
					ds_observacao_w,
					vl_cobranca_w,
					nr_seq_ocorrencia_ret_w,
					ds_nosso_num_w,
					ds_titulo_hcl_w);

	end if;
	
	end;
end loop;
close c01;

commit;

end hcl_gerar_retorno_caixa;
/