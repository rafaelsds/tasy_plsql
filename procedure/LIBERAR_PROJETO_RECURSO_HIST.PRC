create or replace
procedure liberar_projeto_recurso_hist(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

begin
update 	projeto_recurso_hist
set	dt_liberacao	= sysdate,
	dt_atualizacao	= sysdate,
	nm_usuario 	= nm_usuario_p	
where	nr_sequencia 	= nr_sequencia_p;

commit;

end liberar_projeto_recurso_hist;
/