create or replace
procedure Duplicar_Imagem_Ocup_Estab(
			nr_seq_imagem_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 

ds_imagem_w	varchar2(10000);			
qt_imagem_w	number(10);	
ie_status_w	varchar2(15);
	
begin

select	ds_imagem,
	ie_status
into	ds_imagem_w,
	ie_status_w
from	ocup_hosp_status
where	nr_sequencia	= nr_seq_imagem_p;

select	count(*)
into	qt_imagem_w
from	ocup_hosp_status
where	ie_status		= ie_status_w
and	cd_estabelecimento	= cd_estabelecimento_p;

if	(qt_imagem_w	= 0) then
	insert into ocup_hosp_status
		(nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_status,
		ds_imagem,
		nr_seq_apresent,
		ds_status_mapa,
		nr_status_ocup)
	select	ocup_hosp_status_seq.nextval,
		cd_estabelecimento_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_status,
		ds_imagem_w,
		nr_seq_apresent,
		ds_status_mapa,
		nr_status_ocup
	from	ocup_hosp_status
	where	nr_sequencia	= nr_seq_imagem_p;
end if;

commit;

end Duplicar_Imagem_Ocup_Estab;
/