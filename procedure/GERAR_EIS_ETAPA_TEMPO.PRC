create or replace
procedure gerar_eis_etapa_tempo(cd_estabelecimento_p		number,
				dt_mes_referencia_p		date,
				nm_usuario_p			varchar2) is

dt_inicial_w			date;
dt_final_w			date;
nr_interno_conta_w		number(10,0);
nr_seq_etapa_w			number(10,0);
cd_pessoa_fisica_w		varchar2(10);
nr_sequencia_w			number(10,0);
qt_dias_w			number(10,0);
qt_dias_alta_w			number(10,0);
ds_etapa_w			varchar2(120);
nm_pessoa_fisica_w		varchar2(80);
nr_seq_motivo_dev_w		number(10,0);
cd_convenio_w			number(5);
nr_atendimento_w		number(10,0);
dt_alta_w			date;

cursor 	c01 is
	select 	a.nr_interno_conta,
		b.nr_atendimento,
		x.dt_alta dt_alta,
		a.nr_seq_etapa,
		a.cd_pessoa_fisica,
		a.nr_seq_motivo_dev,
		b.cd_convenio_parametro cd_convenio,
		obter_dias_entre_datas(dt_etapa, nvl(dt_fim_etapa,sysdate)) qt_dias,
		obter_dias_entre_datas(nvl(x.dt_alta,nvl(dt_fim_etapa,sysdate)),dt_etapa) qt_dias_alta
	from 	atendimento_paciente x,
		conta_paciente_etapa a,
		conta_paciente b
	where 	a.nr_interno_conta 	= b.nr_interno_conta
	and 	x.nr_atendimento	= b.nr_atendimento
	and	b.cd_estabelecimento 	= cd_estabelecimento_p
	and	b.dt_mesano_referencia between dt_inicial_w and dt_final_w
	and 	a.nr_sequencia = (select max(c.nr_sequencia)
				  from	 atendimento_paciente y,
					 conta_paciente_etapa c,
					 conta_paciente d
				  where  c.nr_interno_conta 	= d.nr_interno_conta
				  and 	 y.nr_atendimento   	= d.nr_atendimento
				  and	 d.cd_estabelecimento 	= cd_estabelecimento_p
				  and    d.nr_interno_conta 	= b.nr_interno_conta
				  and	 d.dt_mesano_referencia between dt_inicial_w and dt_final_w
				  and 	 a.nr_seq_etapa = c.nr_seq_etapa);

begin

dt_inicial_w		:= trunc(dt_mes_referencia_p, 'month');
dt_final_w		:= fim_dia(last_day(dt_mes_referencia_p));

delete	from eis_etapa_tempo
where	cd_estabelecimento = cd_estabelecimento_p
and	dt_referencia = dt_inicial_w;

open c01;
loop
fetch 	c01 into
	nr_interno_conta_w,
	nr_atendimento_w,
	dt_alta_w,
	nr_seq_etapa_w,	
	cd_pessoa_fisica_w,
	nr_seq_motivo_dev_w,
	cd_convenio_w,
	qt_dias_w,
	qt_dias_alta_w;
exit when c01%notfound;
	begin	

	select	eis_etapa_tempo_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	select 	max(substr(nm_pessoa_fisica,1,80))
	into	nm_pessoa_fisica_w
	from 	pessoa_fisica
	where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	select 	max(substr(ds_etapa,1,120))
	into	ds_etapa_w
	from 	fatur_etapa
	where 	nr_sequencia = nr_seq_etapa_w;	
	
	insert into eis_etapa_tempo
		(nr_sequencia,
		cd_estabelecimento,
		dt_referencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_interno_conta,
		nr_seq_etapa,
		cd_pessoa_fisica,
		qt_dias,
		nm_pessoa_fisica,
		ds_etapa,
		nr_seq_motivo_dev,
		cd_convenio,
		nr_atendimento,
		dt_alta,
		qt_dias_alta)
	values(	nr_sequencia_w,
		cd_estabelecimento_p,
		dt_inicial_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_interno_conta_w,
		nr_seq_etapa_w,
		cd_pessoa_fisica_w,
		qt_dias_w,
		nm_pessoa_fisica_w,
		ds_etapa_w,
		nr_seq_motivo_dev_w,
		cd_convenio_w,
		nr_atendimento_w,
		dt_alta_w,
		qt_dias_alta_w);
	end;
end loop;
close c01;

commit;

end gerar_eis_etapa_tempo;
/