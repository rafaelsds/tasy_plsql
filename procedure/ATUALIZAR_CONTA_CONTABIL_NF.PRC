create or replace
procedure atualizar_conta_contabil_nf(	cd_estabelecimento_p			number,
					nr_sequencia_p				number,
					cd_grupo_desconsidera_p			number,
					cd_subgrupo_desconsidera_p		number,
					cd_classe_desconsidera_p		number,
					cd_material_desconsidera_p		number,
					nm_usuario_p				varchar2) is

cd_centro_custo_w		number(10);
cd_conta_contabil_w		varchar2(20);
ds_erro_w			varchar2(500);
dt_entrada_saida_w		date;
cd_operacao_nf_w		nota_fiscal.cd_operacao_nf%type;
vl_regra_w			number(15,4);
cd_natureza_operacao_w		nota_fiscal.cd_natureza_operacao%type;

cursor c01 is
select	a.nr_nota_fiscal,
	a.nr_item_nf,
	a.cd_material,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_local_estoque,
	a.cd_centro_custo,
	a.cd_conta_contabil
from	nota_fiscal_item a,
	estrutura_material_v e
where	a.cd_material = e.cd_material
and	((cd_grupo_desconsidera_p = 0) or
	((cd_grupo_desconsidera_p > 0) and (e.cd_grupo_material <> cd_grupo_desconsidera_p)))
and	((cd_subgrupo_desconsidera_p = 0) or
	((cd_subgrupo_desconsidera_p > 0) and (e.cd_subgrupo_material <> cd_subgrupo_desconsidera_p)))	
and	((cd_classe_desconsidera_p = 0) or
	((cd_classe_desconsidera_p > 0) and (e.cd_classe_material <> cd_classe_desconsidera_p)))
and	((cd_material_desconsidera_p = 0) or
	((cd_material_desconsidera_p > 0) and (e.cd_material <> cd_material_desconsidera_p)))
and	a.nr_sequencia	= nr_sequencia_p
union
select	a.nr_nota_fiscal,
	a.nr_item_nf,
	a.cd_material,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_local_estoque,
	a.cd_centro_custo,
	a.cd_conta_contabil
from	nota_fiscal_item a
where	a.nr_sequencia	= nr_sequencia_p
and	a.cd_procedimento is not null
and	a.cd_material is null;


Vet01	C01%RowType;

BEGIN
/* Dados da nota fiscal */
select	a.dt_entrada_saida,
	a.cd_operacao_nf,
	a.cd_natureza_operacao
into	dt_entrada_saida_w,
	cd_operacao_nf_w,
	cd_natureza_operacao_w
from	nota_fiscal a
where	a.nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01 into
	Vet01;
exit when C01%notfound;
	begin
	cd_centro_custo_w	:= Vet01.cd_centro_custo;
	
	begin
	vl_regra_w := obter_valor_item_regra(nr_sequencia_p, Vet01.nr_item_nf);
	define_conta_contabil(	2,
				cd_estabelecimento_p,
				'',
				'',
				null,
				null,
				Vet01.cd_material,
				Vet01.cd_procedimento,
				Vet01.ie_origem_proced,
				Vet01.cd_local_estoque,
				cd_conta_contabil_w,
				cd_centro_custo_w,
				'',
				dt_entrada_saida_w,
				null,
				null,
				cd_operacao_nf_w,
				cd_natureza_operacao_w,
				null,
				null,
				vl_regra_w);
	exception when others then
			ds_erro_w	:= SQLERRM(SQLCODE);
			wheb_mensagem_pck.exibir_mensagem_abort(265567,'NR_NOTA_FISCAL=' || Vet01.nr_nota_fiscal || ';DS_ERRO=' || ds_erro_w);
			--'Erro ao obter a conta contabil da nota fiscal: ' || Vet01.nr_nota_fiscal || chr(13) || ds_erro_w || '#@#@');
	end;			
	if	(cd_conta_contabil_w <> nvl(Vet01.cd_conta_contabil,'0')) or
		(cd_centro_custo_w <> nvl(Vet01.cd_centro_custo,0)) then
	
		update	nota_fiscal_item
		set	cd_conta_contabil	= cd_conta_contabil_w,
			cd_centro_custo	= cd_centro_custo_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_p
		and	nr_item_nf	= Vet01.nr_item_nf;

		gerar_historico_nota_fiscal(
			nr_sequencia_p,
			nm_usuario_p,
			'30',
			wheb_mensagem_pck.get_texto(310981,'CD_CONTA_CONTABIL_W=' || cd_conta_contabil_w || ';NM_USUARIO_W=' || nm_usuario_p));

	end if;
	end;
end loop;
close C01;
commit;
END atualizar_conta_contabil_nf;
/
