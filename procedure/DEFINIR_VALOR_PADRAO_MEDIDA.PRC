create or replace
procedure definir_valor_padrao_medida(
			nm_usuario_p		varchar2,
			vl_padrao_p		varchar2,
			nr_sequencia_p		number) is 

begin

update	medida_exame_laudo
set		vl_padrao = vl_padrao_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end definir_valor_padrao_medida;
/