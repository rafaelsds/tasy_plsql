create or replace
procedure duplicar_orcamento(	nm_usuario_p			varchar2,
				cd_classificacao_p		varchar2,
				dt_mes_ini_p			varchar2,
				dt_mes_fim_p			varchar2,
				pr_acrescimo_p			number,
				ie_sobrepor_p			varchar2,
				cd_centro_custo_p		number,
				nr_seq_mes_ref_p		varchar2,
				nr_seq_mes_orig_final_p		varchar2,
				cd_empresa_p			number,
				cd_conta_contabil_p		varchar2,
				cd_conta_contabil_dest_p	varchar2,
				cd_centro_custo_dest_p		number,
				ie_tipo_duplicacao_p		number,
				ie_valor_zerado_p		varchar2,
				ie_orcado_real_p		varchar2,
				ie_media_real_p			varchar2,
				cd_estab_p			number) is

/*
IE_TIPO_DUPLICACAO_P
0 - duplicar como valor da conta + percentual
1 - duplicar como valor da percentual da conta

IE_ORCADO_REAL_P
'O' - or�ado
'R' - real
'P' - real para or�ado
*/

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_conta_contabil_dest_w	conta_contabil.cd_conta_contabil%type;
cd_conta_contabil_w		conta_contabil.cd_conta_contabil%type;
cd_centro_custo_dest_w		centro_custo.cd_centro_custo%type;
cd_centro_custo_w		centro_custo.cd_centro_custo%type;
ds_observacao_w			ctb_orcamento.ds_observacao%type;
vl_realizado_w			ctb_orcamento.vl_realizado%type;
vl_original_w			ctb_orcamento.vl_original%type;
nr_seq_mes_ref_w		ctb_mes_ref.nr_sequencia%type;
vl_orcado_w			ctb_orcamento.vl_orcado%type;
ds_erro_w			varchar2(2000);
ds_macro_w			varchar2(255);
vl_orcado_adic_w		number(15,2);
pr_acrescimo_w			number(10,2);
ie_media_real_w			varchar2(1);
ie_vigente_w			varchar2(1);
cont_w				number(5);
dt_mes_origem_inicial_w		date;
dt_mes_origem_final_w		date;
dt_referencia_dest_w		date;
dt_liberacao_w			date;

cursor c01 is
	select	a.cd_estabelecimento,
		a.cd_conta_contabil,
		a.cd_centro_custo,
		a.vl_original,
		a.vl_orcado,
		a.vl_realizado,
		a.nr_seq_mes_ref,
		a.ds_observacao
	from	estabelecimento b,
		ctb_orcamento a
	where	a.cd_estabelecimento = b.cd_estabelecimento
	and	b.cd_empresa = cd_empresa_p
	and	a.cd_centro_custo = nvl(cd_centro_custo_p, a.cd_centro_custo)
	and	ctb_obter_se_centro_usuario(a.cd_centro_custo, cd_empresa_p, nm_usuario_p) = 'S'
	and	a.nr_seq_mes_ref in (
		select	nr_sequencia
		from	ctb_mes_ref
		where	dt_referencia between to_date(dt_mes_ini_p,'dd/mm/yyyy') and to_date(dt_mes_fim_p,'dd/mm/yyyy')
		and	cd_empresa = cd_empresa_p
		union
		select	to_number(nr_seq_mes_ref_p)
		from	dual)
	and	(cd_classificacao_p is null or
		ctb_obter_se_conta_classif_sup(a.cd_conta_contabil, cd_classificacao_p) = 'S')
	and	a.cd_conta_contabil = nvl(cd_conta_contabil_p, a.cd_conta_contabil)
	and	a.cd_estabelecimento = nvl(cd_estab_p, a.cd_estabelecimento);

cursor c02 is
	select	nr_sequencia,
		dt_referencia
	from	ctb_mes_ref
	where	cd_empresa = cd_empresa_p
	and	dt_referencia between to_date(dt_mes_ini_p,'dd/mm/yyyy') and to_date(dt_mes_fim_p,'dd/mm/yyyy');

cursor c03 is
	select	a.cd_estabelecimento,
		a.cd_conta_contabil,
		a.cd_centro_custo,
		a.vl_original,
		a.vl_orcado,
		a.vl_realizado,
		a.ds_observacao
	from	estabelecimento b,
		ctb_orcamento a
	where	a.cd_estabelecimento = b.cd_estabelecimento
	and	b.cd_empresa = cd_empresa_p
	and	a.cd_centro_custo = nvl(cd_centro_custo_p, a.cd_centro_custo)
	and	ctb_obter_se_centro_usuario(a.cd_centro_custo, cd_empresa_p, nm_usuario_p) = 'S'
	and	a.nr_seq_mes_ref = nvl(nr_seq_mes_ref_p, a.nr_seq_mes_ref)
	and	(cd_classificacao_p is null or ctb_obter_se_conta_classif_sup(a.cd_conta_contabil, cd_classificacao_p) = 'S')
	and	a.cd_conta_contabil = nvl(cd_conta_contabil_p, a.cd_conta_contabil)
	and	a.cd_estabelecimento	= nvl(cd_estab_p,a.cd_estabelecimento);

begin
pr_acrescimo_w := nvl(pr_acrescimo_p,0);

if	(nvl(nr_seq_mes_ref_p,0) <> 0) then
	begin
	dt_mes_origem_inicial_w	:= ctb_obter_mes_ref(nr_seq_mes_ref_p);
	end;
end if;

if	(nvl(nr_seq_mes_orig_final_p,0) <> 0) then
	begin
	dt_mes_origem_final_w	:= ctb_obter_mes_ref(nr_seq_mes_orig_final_p);
	end;
end if;

dt_mes_origem_final_w		:= nvl(dt_mes_origem_final_w, dt_mes_origem_inicial_w);

ie_media_real_w	:= nvl(ie_media_real_p,'N');

cd_conta_contabil_dest_w := nvl(cd_conta_contabil_dest_p,'X');

if	(nr_seq_mes_ref_p is null) and
	(cd_conta_contabil_dest_w <> 'X') then
	begin
	open c01;
	loop
	fetch c01 into
		cd_estabelecimento_w,
		cd_conta_contabil_w,
		cd_centro_custo_w,
		vl_original_w,
		vl_orcado_w,
		vl_realizado_w,
		nr_seq_mes_ref_w,
		ds_observacao_w;
	exit when c01%notfound;
		begin
		cd_centro_custo_dest_w := nvl(cd_centro_custo_dest_p, cd_centro_custo_w);

		if	(ie_orcado_real_p = 'O') then
			begin
			if	(ie_tipo_duplicacao_p = 0) then
				begin
				vl_orcado_w := vl_orcado_w + ((vl_orcado_w * pr_acrescimo_w) / 100);
				end;
			elsif	(ie_tipo_duplicacao_p = 1) then
				begin
				vl_orcado_w := vl_orcado_w * (pr_acrescimo_w / 100);
				end;
			end if;
			end;
		else
			begin
			if	(ie_tipo_duplicacao_p = 0) then
				begin
				vl_realizado_w	:= vl_realizado_w + ((vl_realizado_w * pr_acrescimo_w) / 100);
				end;
			elsif	(ie_tipo_duplicacao_p = 1) then
				begin
				vl_realizado_w	:= vl_realizado_w * (pr_acrescimo_w / 100);
				end;
			end if;
			end;
		end if;

		select  count(*)
		into	cont_w
		from 	ctb_orcamento
		where 	nr_seq_mes_ref		= nr_seq_mes_ref_w
		and	cd_conta_contabil	= cd_conta_contabil_dest_w
		and	cd_centro_custo		= cd_centro_custo_dest_w
		and	cd_estabelecimento	= cd_estabelecimento_w;

		if	(cont_w = 0)	then
			begin
			if	(ie_valor_zerado_p = 'S') then
				begin
				vl_original_w := 0;
				end;
			end if;

			insert into ctb_orcamento(
				nr_sequencia,
				nr_seq_mes_ref,
				dt_atualizacao,
				nm_usuario,
				cd_estabelecimento,
				cd_conta_contabil,
				cd_centro_custo,
				vl_original,
				vl_orcado,
				vl_realizado,
				ds_observacao,
				ie_cenario,
				ie_origem_orc)
			(select ctb_orcamento_seq.nextval,
				nr_seq_mes_ref_w,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_w,
				cd_conta_contabil_dest_w,
				cd_centro_custo_dest_w,
				vl_original_w,
				decode(ie_orcado_real_p,'P', vl_realizado_w, vl_orcado_w),
				decode(ie_orcado_real_p,'R', vl_realizado_w, 0),
				ds_observacao_w,
				'N',
				'SIS'
			from	dual);
			end;
		else
			begin
			if	(ie_sobrepor_p in ('S','A')) 	then
				begin
				select	max(dt_liberacao)
				into	dt_liberacao_w
				from	ctb_orcamento
				where 	nr_seq_mes_ref		= nr_seq_mes_ref_w
				and	cd_conta_contabil	= cd_conta_contabil_dest_w
				and	cd_centro_custo		= cd_centro_custo_dest_w
				and	cd_estabelecimento	= cd_estabelecimento_w;

				if	(dt_liberacao_w is null) then
					begin
					update	ctb_orcamento set
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p,
						vl_orcado		= decode(ie_sobrepor_p,'A',vl_orcado,0) + decode(ie_orcado_real_p, 'P', vl_realizado_w, vl_orcado_w),
						vl_realizado		= decode(ie_sobrepor_p,'A',vl_realizado,0) + decode(ie_orcado_real_p, 'R', vl_realizado_w, 0)
					where 	nr_seq_mes_ref		= nr_seq_mes_ref_w
					and	cd_conta_contabil 	= cd_conta_contabil_dest_w
					and	cd_centro_custo 	= cd_centro_custo_dest_w
					and	cd_estabelecimento	= cd_estabelecimento_w;
					end;
				end if;
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	close c01;
	end;
else
	begin
	open c03;
	loop
	fetch c03 into
		cd_estabelecimento_w,
		cd_conta_contabil_w,
		cd_centro_custo_w,
		vl_original_w,
		vl_orcado_w,
		vl_realizado_w,
		ds_observacao_w;
	exit when c03%notfound;
		begin
		if	(ie_media_real_w = 'S') and
			(dt_mes_origem_final_w is not null) then
			begin
			select	avg(vl_realizado)
			into	vl_realizado_w
			from	ctb_mes_ref a,
				ctb_orcamento b
			where	a.nr_sequencia		= b.nr_seq_mes_ref
			and	a.cd_empresa		= cd_empresa_p
			and	b.cd_estabelecimento	= cd_estabelecimento_w
			and	b.cd_centro_custo	= cd_centro_custo_w
			and	b.cd_conta_contabil	= cd_conta_contabil_w
			and	a.dt_referencia between dt_mes_origem_inicial_w and dt_mes_origem_final_w;
			end;
		end if;

		if	(ie_orcado_real_p = 'O') then
			begin
			if	(ie_tipo_duplicacao_p = 0) then
				begin
				vl_orcado_w := vl_orcado_w + ((vl_orcado_w * pr_acrescimo_w) / 100);
				end;
			elsif	(ie_tipo_duplicacao_p = 1) then
				begin
				vl_orcado_w := vl_orcado_w * (pr_acrescimo_w / 100);
				end;
			end if;
			end;
		else
			begin
			if	(ie_tipo_duplicacao_p = 0) then
				begin
				vl_realizado_w	:= vl_realizado_w + ((vl_realizado_w * pr_acrescimo_w) / 100);
				end;
			elsif	(ie_tipo_duplicacao_p = 1) then
				begin
				vl_realizado_w	:= vl_realizado_w * (pr_acrescimo_w / 100);
				end;
			end if;
			end;
		end if;

		cd_conta_contabil_dest_w	:= nvl(cd_conta_contabil_dest_p, cd_conta_contabil_w);
		cd_centro_custo_dest_w		:= nvl(cd_centro_custo_dest_p, cd_centro_custo_w);

		open c02;
		loop
		fetch c02 into
			nr_seq_mes_ref_w,
			dt_referencia_dest_w;
		exit when c02%notfound;
			begin
			select  count(*)
			into	cont_w
			from 	ctb_orcamento
			where 	nr_seq_mes_ref		= nr_seq_mes_ref_w
			and	cd_conta_contabil 	= cd_conta_contabil_dest_w
			and	cd_centro_custo 	= cd_centro_custo_dest_w
			and	cd_estabelecimento	= cd_estabelecimento_w;

			ie_vigente_w	:= substr(nvl(obter_se_conta_vigente(cd_conta_contabil_dest_w, dt_referencia_dest_w),'N'),1,1);

			if	(cont_w = 0) and
				(ie_vigente_w = 'S') then
				begin
				if	(ie_valor_zerado_p = 'S') then
					begin
					vl_original_w := 0;
					end;
				end if;

				begin
				insert into ctb_orcamento(
					nr_sequencia,
					nr_seq_mes_ref,
					dt_atualizacao,
					nm_usuario,
					cd_estabelecimento,
					cd_conta_contabil,
					cd_centro_custo,
					vl_original,
					vl_orcado,
					vl_realizado,
					ds_observacao,
					ie_cenario,
					ie_origem_orc)
				(select ctb_orcamento_seq.nextval,
					nr_seq_mes_ref_w,
					sysdate,
					nm_usuario_p,
					cd_estabelecimento_w,
					cd_conta_contabil_dest_w,
					cd_centro_custo_dest_w,
					vl_original_w,
					decode(ie_orcado_real_p,'P',vl_realizado_w,vl_orcado_w),
					decode(ie_orcado_real_p,'R',vl_realizado_w,0),
					ds_observacao_w,
					'N',
					'SIS'
				from	dual);
				exception when others then
					ds_erro_w	:= substr(sqlerrm, 1, 255);
					ds_macro_w	:= substr('CD_CONTA_CONTABIL=' || cd_conta_contabil_dest_w || ';' ||
									'CD_CENTRO_CUSTO=' || cd_centro_custo_dest_w || ';' ||
									'DS_ERRO=' || ds_erro_w,1,255);
					wheb_mensagem_pck.exibir_mensagem_abort(186186,ds_macro_w);
				end;
				end;
			else
				begin
				if	(ie_sobrepor_p in ('S','A')) and
					(ie_vigente_w = 'S') then
					begin
					select	max(dt_liberacao)
					into	dt_liberacao_w
					from	ctb_orcamento
					where	nr_seq_mes_ref		= nr_seq_mes_ref_w
					and	cd_conta_contabil	= cd_conta_contabil_dest_w
					and	cd_centro_custo		= cd_centro_custo_dest_w
					and	cd_estabelecimento	= cd_estabelecimento_w;

					if	(dt_liberacao_w is null) then
						begin
						update	ctb_orcamento set
							dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p,
							vl_orcado		= decode(ie_sobrepor_p,'A',vl_orcado,0) + decode(ie_orcado_real_p, 'P', vl_realizado_w, vl_orcado_w),
							vl_realizado		= decode(ie_sobrepor_p,'A',vl_realizado,0) + decode(ie_orcado_real_p, 'R', vl_realizado_w, 0)
						where	nr_seq_mes_ref		= nr_seq_mes_ref_w
						and	cd_conta_contabil 	= cd_conta_contabil_dest_w
						and	cd_centro_custo 	= cd_centro_custo_dest_w
						and	cd_estabelecimento	= cd_estabelecimento_w;
						end;
					end if;
					end;
				end if;
				end;
			end if;
			end;
		end loop;
		close c02;
		end;
	end loop;
	close c03;
	end;
end if;
commit;

end duplicar_orcamento;
/
