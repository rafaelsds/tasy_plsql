create or replace
procedure fis_gerar_reg_B025_icmsipi(	nr_seq_controle_p	number,
					nr_seq_nota_p		number) is 

-- VARIABLES
ie_gerou_dados_bloco_w 	varchar2(1)	:= 'N';
nr_vetor_w		number(10)	:= 0;
qt_cursor_w		number(10)	:= 0;
vl_total_despesa_w	number(10)	:= 0;	

-- FIS_EFD_ICMSIPI_B025
nr_seq_icmsipi_B025_w	fis_efd_icmsipi_B025.nr_sequencia%type;
			
-- USUARIO
nm_usuario_w			usuario.nm_usuario%type;

/*Cursor que retorna as informa��es para o registro B025 restringindo pela sequencia da nota fiscal e sendo agrupado pelos campos de C�digo da Situa��o Tribut�ria, C�digo Fiscal de Opera��o e Presta��o e  Al�quota do ICMS*/
cursor c_movimentos_B025 is
	select	Sum(b.vl_total_nota) vl_cont_p,
		sum(Obter_Valor_tipo_Tributo_Nota(b.nr_sequencia,'B','ISS')) vl_bc_iss_p,
		Obter_Valor_tipo_Tributo_Nota(b.nr_sequencia,'X','ISS') tx_aliq_iss,
		sum(Obter_Valor_tipo_Tributo_Nota(b.nr_sequencia,'V','ISS')) vl_iss_p,
		sum(0) vl_isnt_iss_p,
		nvl(obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(b.nr_sequencia,'P'),   obter_procedimento_nfse(b.nr_sequencia,'O'),b.cd_estabelecimento), 'GR'), obter_item_servico_proced_mat(b.nr_sequencia)) cd_serv
	from	nota_fiscal 		b
	where	b.nr_sequencia 	= nr_seq_nota_p
	group by	Obter_Valor_tipo_Tributo_Nota(b.nr_sequencia,'X','ISS'),
				nvl(obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(b.nr_sequencia,'P'),   obter_procedimento_nfse(b.nr_sequencia,'O'),b.cd_estabelecimento), 'GR'), obter_item_servico_proced_mat(b.nr_sequencia))
	order by tx_aliq_iss desc;
	
/*Cria��o do array com o tipo sendo do cursor eespecificado - C_MOVIMENTOS_B025 */	
type reg_c_movimentos_B025 is table of c_movimentos_B025%RowType;
vetmovimentos_B025	reg_c_movimentos_B025;

/*Cria��o do array com o tipo sendo da tabela eespecificada - FIS_EFD_ICMSIPI_B025 */
type registro is table of fis_efd_icmsipi_B025%rowtype index by pls_integer;
fis_registros_w		registro;
		
begin
/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_movimentos_B025;
loop
fetch c_movimentos_B025 bulk collect into vetmovimentos_B025 limit 1000;
	for i in 1..vetmovimentos_B025.Count loop
		begin
		
		/*Incrementa a variavel para o array*/
		qt_cursor_w:=	qt_cursor_w + 1;
				
		if	(ie_gerou_dados_bloco_w = 'N') then
			ie_gerou_dados_bloco_w:=	'S';
		end if;
		
		/*Busca da sequencia da tabela especificada - fis_efd_icmsipi_B025 */
		select	fis_efd_icmsipi_B025_seq.nextval
		into	nr_seq_icmsipi_B025_w
		from	dual;
		
		/*Inserindo valores no array para realiza��o do forall posteriormente*/
		fis_registros_w(qt_cursor_w).nr_sequencia		:= nr_seq_icmsipi_B025_w;          
		fis_registros_w(qt_cursor_w).dt_atualizacao		:= sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario			:= nm_usuario_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao_nrec	:= sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario_nrec		:= nm_usuario_w;
		fis_registros_w(qt_cursor_w).cd_reg             	:= 'B025';
		fis_registros_w(qt_cursor_w).vl_cont_p        		:= vetmovimentos_B025(i).vl_cont_p;
		fis_registros_w(qt_cursor_w).vl_bc_iss_p            	:= vetmovimentos_B025(i).vl_bc_iss_p;
		fis_registros_w(qt_cursor_w).tx_aliq_iss		:= vetmovimentos_B025(i).tx_aliq_iss;
		fis_registros_w(qt_cursor_w).vl_iss_p             	:= vetmovimentos_B025(i).vl_iss_p;
		fis_registros_w(qt_cursor_w).vl_isnt_iss_p         	:= vetmovimentos_B025(i).vl_isnt_iss_p;
		fis_registros_w(qt_cursor_w).cd_serv            	:= replace(vetmovimentos_B025(i).cd_serv, '.', '');
		fis_registros_w(qt_cursor_w).nr_seq_controle    	:= nr_seq_controle_p;
		fis_registros_w(qt_cursor_w).nr_seq_nota		:= nr_seq_nota_p;   
		
		if	(nr_vetor_w >= 1000) then
			begin
			/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_B025 */
			forall j in fis_registros_w.first..fis_registros_w.last
				insert into fis_efd_icmsipi_B025 values fis_registros_w(j);

			nr_vetor_w	:= 0;
			fis_registros_w.delete;

			commit;

			end;
		end if;
		
		/*incrementa variavel para realizar o forall quando chegar no valor limite*/
		nr_vetor_w	:= nr_vetor_w 	+ 1;
		
		end;
	end loop;
exit when c_movimentos_B025%notfound;
end loop;
close c_movimentos_B025;

if	(fis_registros_w.count > 0) then
	begin
	/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
	forall l in fis_registros_w.first..fis_registros_w.last
		insert into fis_efd_icmsipi_B025 values fis_registros_w(l);
		
	fis_registros_w.delete;

	commit;
	
	end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

/*Atualiza��o informa��o no controle de gera��o de registro para SIM*/
if (ie_gerou_dados_bloco_w = 'S') then
	update 	fis_efd_icmsipi_controle
	set	ie_mov_B 	= 'S'
	where 	nr_sequencia 	= nr_seq_controle_p;
end if;

end fis_gerar_reg_B025_icmsipi;
/