create or replace
procedure pls_desfazer_liberacao_defesa(	nr_seq_formulario_p	pls_formulario.nr_sequencia%type,
						nr_seq_defesa_p		pls_formulario_defesa.nr_sequencia%type,
						nm_usuario_p		usuario.nm_usuario%type) is 

nr_seq_conta_w			pls_formulario.nr_seq_conta%type;
ie_tipo_peticao_w		pls_formulario.ie_tipo_peticao%type;
						
begin
select	max(a.nr_seq_conta),
	max(a.ie_tipo_peticao)
into	nr_seq_conta_w,
	ie_tipo_peticao_w
from	pls_formulario		a,
	pls_formulario_defesa	b
where	a.nr_sequencia	= b.nr_seq_formulario
and	b.nr_sequencia	= nr_seq_defesa_p;

update 	pls_formulario_defesa 
set 	dt_liberacao 	= null,
	ie_status	= 'E',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where 	nr_sequencia 	= nr_seq_defesa_p;

update	pls_formulario
set	ie_status_impugnacao 	= 'A',
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia 		= nr_seq_formulario_p; 

if	(ie_tipo_peticao_w = '2') then
	update	pls_formulario
	set	ie_status_impugnacao	= 'R'
	where	ie_tipo_peticao 	= '1'
	and	nr_seq_conta		= nr_seq_conta_w;
end if;

-- Apenas se for impugnação, volta o status da conta para "Recebido"
if	(ie_tipo_peticao_w = '1') then

	update	pls_processo_conta
	set	ie_status_conta	= 'R'
	where	nr_sequencia	= nr_seq_conta_w;
end if;

commit;

end pls_desfazer_liberacao_defesa;
/
