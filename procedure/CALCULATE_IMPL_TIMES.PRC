CREATE OR REPLACE PROCEDURE calculate_impl_times (
    nr_sequencia_p NUMBER
) AS
    nr_count_w   NUMBER(5);
    nm_unit_w    NUMBER(5);
BEGIN
    SELECT
        COUNT(*)
    INTO nr_count_w
    FROM
        rp_implemen_item_reab
    WHERE
        nr_seq_impl_reab = nr_sequencia_p
        AND ds_excl_unit = 'I';

    SELECT
        nvl(MAX(nr_temp_unid), 20)
    INTO nm_unit_w
    FROM
        (
            SELECT
                nr_temp_unid
            FROM
                rp_parametros
            ORDER BY
                dt_atualizacao DESC
        )
    WHERE
        ROWNUM = 1;

    IF ( nr_count_w > 0 ) THEN
        UPDATE rp_implementation_reab
        SET
            dt_end_time = dt_start_time + ( 1 / 1440 * ( nr_count_w * nm_unit_w ) ),
            dt_total_time = pkg_date_utils.start_of(dt_total_time, 'DD', 0) + ( 1 / 1440 * ( nr_count_w * nm_unit_w ) )
        WHERE
            nr_sequencia = nr_sequencia_p;

    END IF;

    COMMIT;
END calculate_impl_times;
/