create or replace
procedure tasy_ms_envia_int_pendente(
		ie_tipo_p		in	varchar2,
		nr_chave_p	in	varchar2,
		ie_situacao_p	out	varchar2,
		nr_sequencia_p	out	number) as


nr_sequencia_w		number(10);
ie_situacao_w		varchar2(1);

begin

nr_sequencia_w	:= 0;
ie_situacao_w	:= '0';

/*Exemplo de como devera ser a implementacao*/
/*Para buscar �ltima integra��o que foi lida com este tipo*/
select	nvl(max(nr_sequencia), 0)
into	nr_sequencia_w
from	tasy_ms_integracao
where	ie_tipo		= ie_tipo_p
and	ie_status		= 'L';

/*Se encontrou faz o update para que seja identificado como Enviado*/
/*E retorna a (sequencia e situacao) para que no Java possa ser buscado o .xml*/
if	(nr_sequencia_w > 0) then
	update	tasy_ms_integracao
	set	ie_status	= 'E'
	where	nr_sequencia	= nr_sequencia_w;

	select	ie_situacao
	into	ie_situacao_w
	from	tasy_ms_integracao
	where	nr_sequencia	= nr_sequencia_w;
end if;

nr_sequencia_p	:= nr_sequencia_w;
ie_situacao_p	:= ie_situacao_w;

end tasy_ms_envia_int_pendente;
/