create or replace
procedure pls_alterar_item_conta_mat
				(	nr_sequencia_p			number,
					nr_nota_fiscal_p		number,
					dt_emissao_nf_p			varchar2,
					dt_recebimento_nf_p		varchar2,
					dt_atendimento_p		varchar2,
					nr_seq_prest_fornec_p		number,
					cd_estabelecimento_p		number,
					nr_seq_setor_atend_p		number,
					vl_material_imp_p		Number,
					qt_material_imp_p		Number,
					nm_usuario_p			varchar2) is

dt_recebimento_nf_ww		date;
dt_emissao_nf_ww		date;
dt_recebimento_nf_w		date;
dt_emissao_nf_w			date;
dt_atendimento_w		date;
dt_atendimento_ww		date;

nr_nota_fiscal_w		number(10);
nr_seq_conta_w			number(10);
nr_identificador_w		number(10);
nr_seq_material_w		number(10);
nr_seq_prest_fornec_w		number(10);
nr_seq_setor_atend_w		number(10);		
ds_observacao_w			varchar2(4000);
ie_parametro_w			Varchar2(2);
vl_material_imp_w		Number(15,2);
qt_material_imp_w		Number(15,4);
ie_valor_informado_w		Varchar2(1);
ie_origem_conta_w		Varchar2(2);
ie_vl_apresentado_sistema_w	Varchar2(1);

begin

if	(dt_recebimento_nf_p = '  /  /    ') then
	dt_recebimento_nf_w := null;
else
	dt_recebimento_nf_w := to_date(dt_recebimento_nf_p,'dd/mm/yyyy');
end if;

if	(dt_emissao_nf_p = '  /  /    ') then
	dt_emissao_nf_w := null;
else
	dt_emissao_nf_w := to_date(dt_emissao_nf_p,'dd/mm/yyyy');	
end if;

if	(dt_atendimento_p = '  /  /    ') then
	dt_atendimento_w := null;
else
	dt_atendimento_w := to_date(dt_atendimento_p,'dd/mm/yyyy');	
end if;

select	dt_recebimento_nf,
	nr_nota_fiscal,
	dt_emissao_nf,
	nr_seq_conta,
	nr_seq_material,
	dt_atendimento,
	nr_seq_prest_fornec,
	nr_seq_setor_atend,
	vl_material_imp,
	qt_material_imp,
	ie_valor_informado,
	ie_vl_apresentado_sistema
into	dt_recebimento_nf_ww,
	nr_nota_fiscal_w,
	dt_emissao_nf_ww,
	nr_seq_conta_w,
	nr_seq_material_w,
	dt_atendimento_ww,
	nr_seq_prest_fornec_w,
	nr_seq_setor_atend_w,
	vl_material_imp_w,
	qt_material_imp_w,
	ie_valor_informado_w,
	ie_vl_apresentado_sistema_w
from	pls_conta_mat
where	nr_sequencia = nr_sequencia_p;

select	ie_origem_conta
into	ie_origem_conta_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_w;
/*Retirado a consist�ncia abaixo OS 569120 dgkorz
/*
if	(ie_origem_conta_w in ('A','E')) then
/*
	if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(191454);
		/*N�o � permitico alterar o valor apresentado de arquivo importado*/
/*	end if;

	if	(nvl(qt_material_imp_w,0) <> nvl(qt_material_imp_p,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(191455);
		/*N�o � permitico alterar a quantidade apresentado de arquivo importado*/
/*	end if;
end if;
*/
if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0))or
	(nvl(qt_material_imp_w,0) <> nvl(qt_material_imp_p,0)) then
	ie_valor_informado_w := 'S';
end if;
if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0)) then
	ie_vl_apresentado_sistema_w := 'N';
end if;

update	pls_conta_mat
set	dt_recebimento_nf	=	dt_recebimento_nf_w,
	dt_emissao_nf		=	dt_emissao_nf_w,
	nr_nota_fiscal		=	nr_nota_fiscal_p,
	dt_atendimento		=	dt_atendimento_w,
	nr_seq_prest_fornec	=	nr_seq_prest_fornec_p,
	nr_seq_setor_atend	= 	nr_seq_setor_atend_p,
	vl_material_imp		=	vl_material_imp_p,
	qt_material_imp		= 	qt_material_imp_p,
	ie_valor_informado	= 	ie_valor_informado_w,
	ie_vl_apresentado_sistema = ie_vl_apresentado_sistema_w
where	nr_sequencia 		= 	nr_sequencia_p;

if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0)) then
ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Valor apresentado: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||vl_material_imp_w||' - Modificada: '||vl_material_imp_p||chr(13)||chr(10);
end if;

if	(nvl(qt_material_imp_w,0) <> nvl(qt_material_imp_p,0)) then
ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Quantidade apresentada: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||qt_material_imp_w||' - Modificada: '||qt_material_imp_p||chr(13)||chr(10);
end if;
	
if	(nvl(to_char(dt_recebimento_nf_w),'X') <> nvl(to_char(dt_recebimento_nf_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. recebimento NF: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_recebimento_nf_ww, 'dd/mm/yyyy')||' - Modificada: '||to_char(dt_recebimento_nf_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_emissao_nf_w),'X') <> nvl(to_char(dt_emissao_nf_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. emiss�o NF: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_emissao_nf_ww, 'dd/mm/yyyy')||' - Modificada: '||to_char(dt_emissao_nf_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_atendimento_w),'X') <> nvl(to_char(dt_atendimento_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. material: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_atendimento_ww, 'dd/mm/yyyy')||' - Modificada: '||to_char(dt_atendimento_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(nr_nota_fiscal_p,0) <> nvl(nr_nota_fiscal_w,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Nota fiscal: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||nr_nota_fiscal_w||' - Modificada: '||nr_nota_fiscal_p||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_prest_fornec_p,0) <> nvl(nr_seq_prest_fornec_w,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Fornecedor: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||pls_obter_cod_prestador(nr_seq_prest_fornec_w, null)||' - '||pls_obter_dados_prestador(nr_seq_prest_fornec_w,'N')||' - Modificada: '||pls_obter_cod_prestador(nr_seq_prest_fornec_p, null)||' - '||pls_obter_dados_prestador(nr_seq_prest_fornec_p,'N')||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_setor_atend_w,0) <> nvl(nr_seq_setor_atend_p,0)) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Setor atendimento: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||pls_obter_dados_setor_atend(nr_seq_setor_atend_w,'DS')||' - Modificada: '||pls_obter_dados_setor_atend(nr_seq_setor_atend_p,'DS')||chr(13)||chr(10);
end if;

if	(nvl(ds_observacao_w,'X') <> 'X') then
	insert into	pls_conta_log	(	nr_sequencia,dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
						nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
						dt_alteracao, ds_alteracao)
			values		(	pls_conta_log_seq.nextval, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, nr_seq_conta_w,
						null,  nr_sequencia_p, nm_usuario_p,
						sysdate, ds_observacao_w);
end if;

pls_atualiza_valor_conta(nr_seq_conta_w, nm_usuario_p);

commit;

end pls_alterar_item_conta_mat;
/