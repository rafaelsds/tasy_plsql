create or replace
procedure pls_gravar_log_conta(	nr_seq_conta_p		pls_conta.nr_sequencia%type,
				nr_seq_conta_proc_p	pls_conta_proc.nr_sequencia%type,
				nr_seq_conta_mat_p	pls_conta_mat.nr_sequencia%type,
				ds_parecer_p		Varchar2,
				nm_usuario_p		Varchar2) is 
--rotina responsável por gravar o log de alteração da conta criada a mesma com o intuito de centralizar a alteração

nr_seq_analise_w	pls_conta.nr_seq_analise%type;
ie_tipo_item_w		Varchar2(1);
nr_seq_item_w		pls_conta_proc.nr_sequencia%type;
ie_tipo_historico_w	pls_hist_analise_conta.ie_tipo_historico%type	:= '2';
cd_estabelecimento_w	pls_conta.cd_estabelecimento%type;

begin

select	max(cd_estabelecimento),
	max(nr_seq_analise)
into	cd_estabelecimento_w,
	nr_seq_analise_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_p;

insert into pls_conta_log
	(nr_sequencia,	nm_usuario, dt_atualizacao,
	nm_usuario_nrec, dt_atualizacao_nrec, nm_usuario_alteracao,
	dt_alteracao, nr_seq_conta, nr_seq_conta_mat,
	nr_seq_conta_proc, ds_alteracao)
values  (pls_conta_log_seq.nextval, nm_usuario_p, sysdate,
	nm_usuario_p,sysdate,nm_usuario_p,
	sysdate,nr_seq_conta_p, nr_seq_conta_mat_p,
	nr_seq_conta_proc_p, ds_parecer_p);

if	(nr_seq_analise_w	is not null) then
	
	if	(nr_seq_conta_proc_p	is not null) then
		ie_tipo_item_w	:= 'P';
		nr_seq_item_w	:= nr_seq_conta_proc_p;
		ie_tipo_historico_w := 19;
	elsif	(nr_seq_conta_mat_p	is not null) then
		ie_tipo_item_w	:= 'M';
		nr_seq_item_w	:= nr_seq_conta_mat_p;
		ie_tipo_historico_w := 20;
	else
		nr_seq_item_w	:= null;
		ie_tipo_item_w	:= 'C';
	end if;
	
	pls_inserir_hist_analise(nr_seq_conta_p, nr_seq_analise_w, ie_tipo_historico_w,
				 nr_seq_item_w, ie_tipo_item_w, null,
				 null, ds_parecer_p, null,
				 nm_usuario_p, cd_estabelecimento_w);

end if;

end pls_gravar_log_conta;
/