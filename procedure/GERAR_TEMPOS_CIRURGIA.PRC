create or replace
procedure gerar_tempos_cirurgia (nr_cirurgia_p		number,
			         nm_usuario_p		varchar) is 


cursor	c01 is
	SELECT vl_dominio
	FROM   VALOR_DOMINIo
	WHERE  cd_dominio = 1017
	AND    ie_situacao = 'A';
	
ie_tipo_tempo_w		varchar2(3);
nr_sequencia_w		number(5);	
	
			      
begin
if (nr_cirurgia_p is not null) then
	open c01;
	loop
	fetch c01 into
		ie_tipo_tempo_w	;
	exit when c01%notfound;
		begin
		select	nvl(max(nr_sequencia),0)+1
		into	nr_sequencia_w
		from	cirurgia_tempo
		where	nr_cirurgia = nr_cirurgia_p;
		
		insert into cirurgia_tempo
				(nr_sequencia, 
				nr_cirurgia,
				ie_tipo_tempo,
				dt_atualizacao,
				nm_usuario,
				dt_inicial,
				dt_final)
		values		(nr_sequencia_w,
				nr_cirurgia_p,
				ie_tipo_tempo_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				sysdate);
		end;
	end loop;
	close c01;
	commit;	
end if;

end gerar_tempos_cirurgia;
/