create or replace
procedure hd_consistir_pend_pacientes(	ie_exige_acesso_p		varchar2,
				ie_exige_escala_p		varchar2,
				ie_exige_classificacao_p	varchar2,
				ie_exige_profissional_p	varchar2,
				ie_exige_tratamento_p	varchar2,
				ds_erro_p			out varchar2,
				ie_abortar_p		out varchar2) is 

ie_acesso_w 		varchar2(1);
ie_escala_w 		varchar2(1);
ie_profissional_w 		varchar2(1);
ie_classificacao_w 		varchar2(1);
ie_tratamento 		varchar2(1);
ie_abortar_w		varchar2(1);
ds_erro_w		varchar2(255);

begin

ie_acesso_w 		:= HD_obter_se_existe_cadastro(1);
ie_escala_w 		:= HD_obter_se_existe_cadastro(2);
ie_profissional_w 		:= HD_obter_se_existe_cadastro(3);
ie_classificacao_w 		:= HD_obter_se_existe_cadastro(4);
ie_tratamento 		:= HD_obter_se_existe_cadastro(5);
ie_abortar_w		:= 'N';
ds_erro_w		:= '';

if	(ie_exige_acesso_p = 'S') and
	(ie_acesso_w = 'S') then
	ie_abortar_w := 'S';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(282065,null);

elsif	(ie_exige_escala_p = 'S') and
	(ie_escala_w = 'S') then
	ie_abortar_w := 'S';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(282066,null);

elsif 	(ie_exige_classificacao_p = 'S') and
	(ie_profissional_w = 'S') then
	ie_abortar_w := 'S';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(282067,null);

elsif	(ie_exige_profissional_p = 'S') and
	(ie_classificacao_w = 'S') then
	ie_abortar_w := 'S';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(282068,null);

elsif	(ie_exige_tratamento_p = 'S') and
	(ie_tratamento = 'S') then
	ie_abortar_w := 'S';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(282069,null);
end if;

ds_erro_p 	:= ds_erro_w;
ie_abortar_p	:= ie_abortar_w;

end hd_consistir_pend_pacientes;
/