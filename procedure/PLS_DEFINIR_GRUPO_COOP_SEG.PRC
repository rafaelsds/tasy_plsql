create or replace
procedure pls_definir_grupo_coop_seg(	nr_seq_segurado_p	number,
					cd_usuario_plano_p	varchar2,
					nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Definir o grupo de cooperativa do benefici�rio, atrav�s de seu cart�o de identifica��o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 	

cd_empresa_seg_w		varchar2(4000);
cd_empresa_w			varchar2(4000);
ie_segurado_pea_w		varchar2(4000);
qt_posicao_inicial_w		number(15);
qt_posicao_final_w		number(15);
nr_seq_grupo_w			number(15);
nr_seq_coop_grupo_w		number(15);
nr_seq_grupo_pea_w		number(15);
nr_seq_congenere_w		pls_congenere.nr_sequencia%type;

Cursor C01 is
	select	a.qt_posicao_inicial,
		a.qt_posicao_final,
		a.nr_seq_grupo,
		a.nr_sequencia
	from	pls_cooperativa_grupo a,
		pls_grupo_cooperativa b
	where	b.nr_sequencia 		= a.nr_seq_grupo
	and	a.nr_seq_congenere	= nvl(nr_seq_congenere_w,a.nr_seq_congenere)
	and	b.ie_situacao		= 'A'
	and	a.qt_posicao_inicial is not null
	order by a.qt_posicao_inicial,
		a.qt_posicao_final;	
	
Cursor C02 is
	select	a.cd_empresa
	from	pls_coop_grupo_event_faixa a
	where	a.nr_seq_coop_grupo 	= nr_seq_coop_grupo_w;
	
begin
if	(nr_seq_segurado_p is not null) then	
	begin
	select	nvl(nr_seq_ops_congenere,nr_seq_congenere)
	into	nr_seq_congenere_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	exception
	when others then
		nr_seq_congenere_w := null;
	end;
	
	open C01;
	loop
	fetch C01 into	
		qt_posicao_inicial_w,
		qt_posicao_final_w,
		nr_seq_grupo_w,
		nr_seq_coop_grupo_w;
	exit when C01%notfound;
		begin
		if	(qt_posicao_inicial_w is not null) and
			(qt_posicao_final_w is not null) then		
			cd_empresa_seg_w := substr(cd_usuario_plano_p,qt_posicao_inicial_w,(qt_posicao_final_w - qt_posicao_inicial_w) + 1);
			
			open C02;
			loop
			fetch C02 into	
				cd_empresa_w;
			exit when C02%notfound;
				begin				
				if	(cd_empresa_w = cd_empresa_seg_w) then
					
					nr_seq_grupo_pea_w 	:= nr_seq_grupo_w;
					exit;
				end if;
				end;
			end loop;
			close C02;
			
			if	(nr_seq_grupo_pea_w is not null) then
				exit;
			end if;
		end if;
		end;
	end loop;
	close C01;

	update	pls_segurado
	set	nr_seq_grupo_coop 	= nr_seq_grupo_pea_w,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia 		= nr_seq_segurado_p;
end if;

end pls_definir_grupo_coop_seg;
/