create or replace
procedure atualizar_evolucao_prescr_diag(
		nr_seq_evolucao_diag_p	number,
		nr_prescricao_p		number,
		nr_seq_diag_p		number,
		nm_usuario_p	varchar2,
		ds_observacao_p varchar2 DEFAULT NULL) is

begin
if	(nr_seq_evolucao_diag_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_diag_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	pe_prescr_diag
	set	nr_seq_evolucao_diag	= nr_seq_evolucao_diag_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ds_observacao		= ds_observacao_p
	where	nr_seq_prescr		= nr_prescricao_p
	and	nr_seq_diag		= nr_seq_diag_p;
	end;
end if;
commit;
end atualizar_evolucao_prescr_diag;
/