create or replace
procedure pls_gerar_proposta_adapatacao
			(	nr_seq_proposta_p	number,
				nr_seq_contrato_p	number,
				nr_seq_tabela_p		number,
				nr_seq_plano_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_proposta_benef_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_plano_w			number(10);
nr_seq_tabela_w			number(10);
nr_seq_tabela_ant_w		number(10);
nr_seq_plano_ant_w		number(10);
nm_tabela_ant_w			varchar2(255);
ds_plano_ant_w			varchar2(255);
dt_inicio_proposta_w		date;
nr_seq_contrato_plano_w		number(10);
nr_seq_motivo_adaptacao_w	number(10);
nm_plano_novo_w			varchar2(255);
ie_gerar_alerta_w		varchar2(10);
ie_regulamentacao_ant_w		varchar2(10);
ie_regulamentacao_novo_w	varchar2(10);
qt_proposta_plano_w		number(15);
nr_seq_vendedor_canal_w		number(10);
nr_seq_vendedor_pf_w		number(10);
nr_seq_vendedor_canal_seg_w	number(10);
ie_permite_tab_dif_w		varchar2(1);
ie_manter_tabela_benef_adap_w	varchar2(1);
nr_segurado_w			number(15);
nr_seq_alt_plano_w		pls_segurado_alt_plano.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_proposta_beneficiario
	where	nr_seq_proposta	= nr_seq_proposta_p
	and	nr_seq_plano	= nr_seq_plano_p
	and	((nr_seq_tabela	= nr_seq_tabela_w) or (nr_seq_tabela_w is null))
	and	dt_cancelamento is null
	order by nvl(nr_seq_titular,0);

Cursor C02 is
	select	c.nr_sequencia
	from	pls_contrato_plano	c,
		pls_contrato		b,
		pls_plano		a
	where	c.nr_seq_contrato	= b.nr_sequencia
	and	c.nr_seq_plano		= a.nr_sequencia
	and	b.nr_sequencia		= nr_seq_contrato_p
	and	a.ie_regulamentacao	= 'R'
	and	c.ie_situacao		= 'A';

begin

select	dt_inicio_proposta,
	nr_seq_vendedor_canal,
	nr_seq_vendedor_pf
into	dt_inicio_proposta_w,
	nr_seq_vendedor_canal_w,
	nr_seq_vendedor_pf_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

select	max(nr_seq_motivo_adaptacao),
	nvl(max(ie_manter_tabela_benef_adap), 'N')
into	nr_seq_motivo_adaptacao_w,
	ie_manter_tabela_benef_adap_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(nr_seq_motivo_adaptacao_w is null) then
	select	max(nr_sequencia)
	into	nr_seq_motivo_adaptacao_w
	from	pls_motivo_alteracao_plano
	where	ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_p;
end if;

select	count(1)
into	qt_proposta_plano_w
from	pls_proposta_plano
where	nr_seq_proposta	= nr_seq_proposta_p
and	nr_seq_plano	= nr_seq_plano_p
and	rownum <= 2;

if	(qt_proposta_plano_w > 1) then
	nr_seq_tabela_w := nr_seq_tabela_p;
end if;

ie_gerar_alerta_w	:= nvl(obter_valor_param_usuario(1202, 121, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_permite_tab_dif_w	:= nvl(obter_valor_param_usuario(1202, 9, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

open C01;
loop
fetch C01 into
	nr_seq_proposta_benef_w;
exit when C01%notfound;
	begin
	
	select	nr_seq_beneficiario,
		nr_seq_plano
	into	nr_seq_segurado_w,
		nr_seq_plano_w
	from	pls_proposta_beneficiario
	where	nr_sequencia	= nr_seq_proposta_benef_w;
	
	if	(nr_seq_segurado_w is not null) then
		select	nr_seq_tabela,
			nr_seq_plano,
			nr_seq_vendedor_canal
		into	nr_seq_tabela_ant_w,
			nr_seq_plano_ant_w,
			nr_seq_vendedor_canal_seg_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		select	max(nr_seq_tabela)
		into	nr_seq_tabela_w
		from	pls_contrato_plano
		where	nr_seq_contrato		= nr_seq_contrato_p
		and	nr_seq_plano		= nr_seq_plano_w
		and	nr_seq_tabela_origem	= nr_seq_tabela_p;
		
		if	(nr_seq_tabela_ant_w is not null) and
			(ie_manter_tabela_benef_adap_w = 'S') then
			select	a.nr_segurado
			into	nr_segurado_w
			from	pls_tabela_preco a
			where	a.nr_sequencia = nr_seq_tabela_ant_w;
			
			if 	(nr_segurado_w is not null) then
				select	pls_tabela_preco_seq.nextval
				into	nr_seq_tabela_w
				from	dual;
				
				pls_gerar_tabela_seg_adaptacao(nr_seq_segurado_w, nr_seq_tabela_ant_w, nr_seq_tabela_w, nr_seq_plano_w, nr_seq_proposta_p, nm_usuario_p);				
			end if;
		end if;
		
		if	(nr_seq_tabela_w is not null) then
			update	pls_segurado
			set	nm_usuario			= nm_usuario_p,
				dt_atualizacao			= sysdate,
				nr_seq_plano			= nr_seq_plano_w,
				nr_seq_tabela			= nr_seq_tabela_w,
				nr_seq_benef_prop_adesao	= nr_seq_proposta_benef_w,
				ie_acao_contrato		= 'D'
			where	nr_sequencia	= nr_seq_segurado_w;
			
			pls_gerar_valor_segurado
				(null, nr_seq_segurado_w, 'D',
				cd_estabelecimento_p, nm_usuario_p, 'S',
				dt_inicio_proposta_w, ie_permite_tab_dif_w, 'S',
				'N', 'N');
			
			ie_regulamentacao_novo_w	:= substr(pls_obter_dados_produto(nr_seq_plano_w,'R'),1,10);
			ie_regulamentacao_ant_w		:= substr(pls_obter_dados_produto(nr_seq_plano_ant_w,'R'),1,10);
			
			select	pls_segurado_alt_plano_seq.nextval
			into	nr_seq_alt_plano_w
			from	dual;
			
			insert into pls_segurado_alt_plano
				(nr_sequencia, nm_usuario, dt_atualizacao,
				nr_seq_segurado, nr_seq_plano_ant, nr_seq_plano_atual,
				nr_seq_tabela_ant, nr_seq_tabela_atual, dt_alteracao,
				nr_seq_motivo_alt, ds_observacao,
				ie_regulamentacao_ant,ie_regulamentacao_atual, ie_situacao)
			values	(nr_seq_alt_plano_w, nm_usuario_p, sysdate,
				nr_seq_segurado_w, nr_seq_plano_ant_w, nr_seq_plano_w,
				nr_seq_tabela_ant_w, nr_seq_tabela_w, dt_inicio_proposta_w,
				nr_seq_motivo_adaptacao_w, wheb_mensagem_pck.get_texto(1108510),
				ie_regulamentacao_ant_w,ie_regulamentacao_novo_w, 'A');
			
			pls_inativar_alteracao_produto(nr_seq_segurado_w, nr_seq_alt_plano_w, dt_inicio_proposta_w, 'N', nm_usuario_p);
			
			pls_gerar_segurado_historico(nr_seq_segurado_w, '4', sysdate,
							wheb_mensagem_pck.get_texto(1108499),
							wheb_mensagem_pck.get_texto(1108497, 'NR_SEQ_PLANO_ANT='||nr_seq_plano_ant_w||';'||'NR_SEQ_PLANO='||nr_seq_plano_w),
							null, null, null,
							null, dt_inicio_proposta_w, null,
							null, null, null,
							null, null, nm_usuario_p, 'N');
			
			pls_gerar_lanc_automatico(nr_seq_segurado_w, 1, 'A',
							'S', '', nm_usuario_p,
							cd_estabelecimento_p);

			pls_isentar_carencias_adapt(nr_seq_segurado_w,cd_estabelecimento_p,nm_usuario_p);
			
			pls_gravar_histor_prop_benef(nr_seq_proposta_benef_w,sysdate,'13',wheb_mensagem_pck.get_texto(1108510),nm_usuario_p);
			
			if	(nr_seq_vendedor_canal_w <> nr_seq_vendedor_canal_seg_w) then
				pls_alterar_vendedor_benef(	nr_seq_segurado_w, nr_seq_vendedor_canal_w, nr_seq_vendedor_pf_w,
								nm_usuario_p, cd_estabelecimento_p);
			end if;

			if	(ie_gerar_alerta_w = 'S') and
				(dt_inicio_proposta_w > sysdate) then
				
				nm_plano_novo_w	:= substr(pls_obter_dados_produto(nr_seq_plano_w,'N'),1,255);
				ds_plano_ant_w	:= substr(pls_obter_dados_produto(nr_seq_plano_ant_w,'N'),1,255);
				
				pls_gerar_alerta_alt_prod(nr_seq_segurado_w,ds_plano_ant_w,nm_plano_novo_w,dt_inicio_proposta_w,nm_usuario_p);
			end if;
			
			pls_gerar_devolucao_alt_plano(nr_seq_alt_plano_w, 'N', nm_usuario_p, cd_estabelecimento_p);
		end if;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	nr_seq_contrato_plano_w;
exit when C02%notfound;
	begin
	update	pls_contrato_plano
	set	ie_situacao	= 'I',
		dt_inativacao	= dt_inicio_proposta_w,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_contrato_plano_w;
	end;
end loop;
close C02;

end pls_gerar_proposta_adapatacao;
/
