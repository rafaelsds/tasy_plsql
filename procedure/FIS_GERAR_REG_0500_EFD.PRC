create or replace
procedure fis_gerar_reg_0500_efd
			(	nr_seq_controle_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				dt_inicio_p			date,
				dt_fim_p			date,
				cd_empresa_p			number,
				ds_separador_p			varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
tp_registro_w			varchar2(4);
cd_municipio_ibge_w		varchar2(7);
nr_linha_w			number(10)	:= qt_linha_p;
nr_seq_registro_w		number(10)	:= nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;

cursor c01 is
	select	'0500' tp_registro,
		to_char(a.dt_atualizacao,'ddmmyyyy') dt_alt,
		decode(b.ie_tipo, 'A', '01', 'P', '02', '09') cod_nat_cc,
		decode(a.ie_tipo,'A','A','T','S') ind_cta,
		ctb_obter_nivel_conta(a.cd_conta_contabil) nivel,
		a.cd_conta_contabil cod_cta,
		substr(replace(a.ds_conta_contabil,'|',''),1,60) nome_cta,
		'' cod_cta_ref,
		'' cnpj_est
	from	conta_contabil	a,
		ctb_grupo_conta	b,
		fis_efd_conta_contabil c
	where	a.cd_grupo	= b.cd_grupo
	and     a.cd_empresa	= cd_empresa_p
	and	c.cd_conta_contabil = a.cd_conta_contabil
	and	nvl(NR_SEQ_TIPO_CT,0) > 0
	group by decode(b.ie_tipo, 'A', '01', 'P', '02', '09'), decode(a.ie_tipo,'A','A','T','S'), a.dt_atualizacao, a.cd_conta_contabil, a.ds_conta_contabil;

vet01	C01%RowType;

begin
open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	ds_linha_w	:= substr(	sep_w || vet01.tp_registro 	|| 
					sep_w || vet01.dt_alt		||
					sep_w || vet01.cod_nat_cc	||
					sep_w || vet01.ind_cta		||
					sep_w || vet01.nivel		||
					sep_w || vet01.cod_cta		||
					sep_w || vet01.nome_cta		||
					sep_w || vet01.cod_cta_ref 	||
					sep_w || vet01.cnpj_est		||
					sep_w,1,8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_efd_arquivo
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_efd,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values	(nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		vet01.tp_registro,
		ds_arquivo_w,
		ds_arquivo_compl_w);
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_0500_efd;
/