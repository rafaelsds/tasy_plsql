create or replace PROCEDURE CREATE_NURSING_IMPORT_FILE (
    NR_SEQ_IMPORT_P IN nursing_care_import_files.nr_seq_import%TYPE,
    DS_ARQUIVO_P IN nursing_care_import_files.ds_arquivo%TYPE,
    DS_NAME_FILE_P IN nursing_care_import_files.ds_name_file%TYPE,
    NR_SEQUENCIA_P OUT nursing_care_import_files.nr_sequencia%TYPE
    
) AS
    NM_USUARIO_W nursing_care_import_items.nm_usuario%TYPE;
BEGIN
    NM_USUARIO_W := wheb_usuario_pck.get_nm_usuario();
    SELECT NURSING_CARE_IMPORT_FILES_SEQ.nextval INTO NR_SEQUENCIA_P FROM DUAL;

    INSERT INTO NURSING_CARE_IMPORT_FILES (
        NR_SEQUENCIA,
        NR_SEQ_IMPORT,
        DS_ARQUIVO,
        DS_NAME_FILE,
        NM_USUARIO,
        NM_USUARIO_NREC,
        DT_ATUALIZACAO,
        DT_ATUALIZACAO_NREC
    ) VALUES (
        NR_SEQUENCIA_P,
        NR_SEQ_IMPORT_P,
        DS_ARQUIVO_P,
        DS_NAME_FILE_P,
        NM_USUARIO_W,
        NM_USUARIO_W,
        sysdate,
        sysdate
    );
    commit;
END CREATE_NURSING_IMPORT_FILE;
/