create or replace
procedure PLT_estender_prescricao(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					ie_tipo_item_p		varchar2,
					nr_atendimento_p	number,
					nr_seq_regra_p		number,
					cd_medico_p		varchar2,
					dt_prescricao_p		date,
					cd_perfil_p		integer,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number,
					ie_estende_inc_p	varchar2,
					nr_nova_prescricao_p	in out number,
					ds_erro_p		out varchar2,
					nr_horas_validade_p	number) is 

nr_nova_prescr_w	number(15);
nr_sequencia_w		number(20);
ie_tipo_item_w		varchar2(5);
ds_lista_dieta_w	varchar2(1000);
ds_lista_supl_w		varchar2(1000);
ds_lista_sne_w		varchar2(1000);
ds_lista_medic_w	varchar2(1000);
ds_lista_proced_w	varchar2(1000);
cd_item_w		varchar2(255);
qt_item_w		number(15);
cd_intervalo_w		varchar2(10);
ie_vazia_w		varchar2(1);
ds_erro_w		varchar2(2000) := '';
dt_inicio_copia_w	date := sysdate;
nr_seq_leite_deriv_w	number(10);
ie_respiracao_w			prescr_gasoterapia.ie_respiracao%type;
ie_disp_resp_esp_w		prescr_gasoterapia.ie_disp_resp_esp%type;
ie_modo_adm_w			prescr_gasoterapia.ie_modo_adm%type;

cursor c01 is
select	nr_sequencia,
	'J' ie_tipo_item,
	to_char(nr_seq_tipo) cd_item,
	null,
	null,
	null,
	null,
	null
from	rep_jejum
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'J')	= 'J'
union
select	nr_sequencia,
	'D',
	to_char(cd_dieta),
	cd_intervalo,
	0,
	null,
	null,
	null
from	prescr_dieta
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'D')	= 'D'
union
select	nr_sequencia,
	'S',
	to_char(cd_material),
	cd_intervalo,
	null,
	null,
	null,
	null
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	ie_agrupador	= 12
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'S')	= 'S'
union
select	nr_sequencia,
	'LD',
	to_char(cd_material),
	cd_intervalo,
	qt_dose,
	null,
	null,
	null
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	ie_agrupador	= 16
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'LD')	= 'LD'
union
select	nr_sequencia,
	'SNE',
	to_char(cd_material),
	cd_intervalo,
	null,
	null,
	null,
	null
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	ie_agrupador	= 8
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'SNE')	= 'SNE'
union
select	nr_seq_solucao,
	'SOL',
	null,
	cd_intervalo,
	qt_solucao_total,
	null,
	null,
	null
from	prescr_solucao
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_solucao	= nvl(nr_sequencia_p, nr_seq_solucao)
and	nvl(ie_tipo_item_p, 'SOL')	= 'SOL'
union
select	nr_sequencia,
	'HM',
	to_char(cd_procedimento),
	cd_intervalo,
	null,
	null,
	null,
	null
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_solic_sangue is not null
and	nr_seq_derivado	is not null
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'HM')	= 'HM'
union
select	nr_sequencia,
	'M',
	to_char(cd_material),
	cd_intervalo,
	null,
	null,
	null,
	null
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	ie_agrupador	in (1,2)
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'M')	= 'M'
union
select	nr_sequencia,
	'P',
	to_char(cd_procedimento),
	cd_intervalo,
	null,
	null,
	null,
	null
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_solic_sangue is null
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'P')	= 'P'
union
select	nr_sequencia,
	'O',
	to_char(nr_seq_gas),
	cd_intervalo,
	qt_gasoterapia,
	ie_respiracao,
	ie_disp_resp_esp,
	ie_modo_adm
from	prescr_gasoterapia
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'O')	= 'O'
union
select	nr_sequencia,
	'R',
	nvl(to_char(cd_recomendacao),ds_recomendacao),
	cd_intervalo,
	qt_recomendacao,
	null,
	null,
	null
from	prescr_recomendacao
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'R')	= 'R'
union
select	nr_sequencia,
	'NAN',
	to_char(nr_sequencia),
	cd_intervalo,
	0,
	null,
	null,
	null
from	nut_pac
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'NAN')	= 'NAN'
and	ie_npt_adulta	= 'S'
union
select	nr_sequencia,
	'NPN',
	to_char(nr_sequencia),
	cd_intervalo,
	0,
	null,
	null,
	null
from	nut_pac
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nvl(nr_sequencia_p, nr_sequencia)
and	nvl(ie_tipo_item_p, 'NPN')	= 'NPN'
and	nvl(ie_npt_adulta,'N')	<> 'S';


begin

if	(nvl(nr_nova_prescricao_p,0)	= 0) then
	plt_copia_prescricao_regra(nr_prescricao_p,nr_atendimento_p,nr_seq_regra_p,nm_usuario_p,cd_medico_p,dt_prescricao_p,'',substr(to_char(dt_prescricao_p,'dd/mm/yyyy hh24:mi:ss'),12,5),cd_perfil_p,null,nr_horas_validade_p,nr_nova_prescr_w,null,null, null);
else
	nr_nova_prescr_w	:= nr_nova_prescricao_p;
end if;

open C01;
loop
fetch c01 into	
	nr_sequencia_w,
	ie_tipo_item_w,
	cd_item_w,
	cd_intervalo_w,
	qt_item_w,
	ie_respiracao_w,
	ie_disp_resp_esp_w,
	ie_modo_adm_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_item_w	= 'J') then
		plt_copia_jejum(nr_prescricao_p,nr_nova_prescr_w,nr_seq_regra_p,nm_usuario_p,'N',nr_sequencia_w, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p);
	elsif	(ie_tipo_item_w	= 'D') then
		if	(ds_lista_dieta_w is null) then
			ds_lista_dieta_w := nr_prescricao_p || ' ' || nr_sequencia_w || ';' || cd_item_w || ',';
		else
			ds_lista_dieta_w := ds_lista_dieta_w || nr_prescricao_p || ' ' || nr_sequencia_w || ';' || cd_item_w || ',';
		end if;
	elsif	(ie_tipo_item_w	= 'S') then
		if	(ds_lista_supl_w is null) then
			ds_lista_supl_w := nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		else
			ds_lista_supl_w := ds_lista_supl_w || nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		end if;
	elsif	(ie_tipo_item_w	= 'LD') then
	
		select	nvl(max(nr_seq_leite_deriv),0)
		into	nr_seq_leite_deriv_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p
		and	nvl(qt_dose,0) = nvl(qt_item_w,nvl(qt_dose,0))
		and	cd_material = cd_item_w
		and	nvl(cd_intervalo,'XPTO') = nvl(cd_intervalo_w,'XPTO');
	
		plt_copia_leite_deriv(nr_nova_prescr_w, nr_prescricao_p,nr_sequencia_w, nr_seq_leite_deriv_w, nr_seq_regra_p,'N', nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p);
	elsif	(ie_tipo_item_w	= 'SNE') then
		if	(ds_lista_sne_w is null) then
			ds_lista_sne_w := nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		else
			ds_lista_sne_w := ds_lista_sne_w || nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		end if;		
	elsif	(ie_tipo_item_w	= 'SOL') then
		plt_copia_solucao(nr_prescricao_p,nr_nova_prescr_w,nr_seq_regra_p,dt_prescricao_p,nr_sequencia_w,'N',nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p, null);
	elsif	(ie_tipo_item_w	= 'HM') then
		plt_copia_banco_sangue(nr_prescricao_p,nr_nova_prescr_w,nr_sequencia_w,nr_seq_regra_p,'N',nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p);
	elsif	(ie_tipo_item_w	= 'M') then
		if	(ds_lista_medic_w is null) then
			ds_lista_medic_w := nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		else
			ds_lista_medic_w := ds_lista_medic_w || nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		end if;				
	elsif	(ie_tipo_item_w	= 'P') then
		if	(ds_lista_proced_w is null) then
			ds_lista_proced_w := nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		else
			ds_lista_proced_w := ds_lista_proced_w || nr_prescricao_p || ' ' || nr_sequencia_w || ',';
		end if;						
	elsif	(ie_tipo_item_w	= 'O') then
		plt_copia_gasoterapia(nr_prescricao_p,nr_nova_prescr_w,dt_prescricao_p,nr_seq_regra_p,cd_item_w,cd_intervalo_w,qt_item_w,'N',nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p,ie_respiracao_w,ie_disp_resp_esp_w,ie_modo_adm_w);
	elsif	(ie_tipo_item_w	= 'R') then
		plt_copia_recomendacao(nr_prescricao_p,nr_nova_prescr_w,nr_seq_regra_p,dt_prescricao_p,cd_intervalo_w,cd_item_w,qt_item_w,'N',nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p, null);
	elsif	(ie_tipo_item_w	= 'NPN') then
		plt_copia_npt(nr_sequencia_w,nr_nova_prescr_w,nr_Seq_regra_p,nm_usuario_p,ie_tipo_item_w,'N', cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p);
	elsif	(ie_tipo_item_w	= 'NAN') then
		plt_copia_npt_adul_prot(nr_sequencia_w,nr_nova_prescr_w,nr_Seq_regra_p,nm_usuario_p,ie_tipo_item_w,'N', cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p);
	end if;
	end;
end loop;
close C01;

if	(ds_lista_medic_w	is not null) then
	plt_copia_medicamento(nr_nova_prescr_w,nr_Seq_regra_p,dt_prescricao_p,substr(to_char(dt_prescricao_p,'dd/mm/yyyy hh24:mi:ss'),12,5),cd_perfil_p,'N',ds_lista_medic_w,'N','N',nm_usuario_p, cd_estabelecimento_p,ie_estende_inc_p, null,null);
end if;

if	(ds_lista_supl_w	is not null) then
	plt_copia_suplemento(nr_nova_prescr_w,nr_Seq_regra_p,ds_lista_supl_w,'N',nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p, null);
end if;

if	(ds_lista_sne_w		is not null) then
	plt_copia_suporte_nut(nr_nova_prescr_w,nr_seq_regra_p,ds_lista_sne_w,'N',nm_usuario_p, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p, null);
end if;

if	(ds_lista_proced_w	is not null) then
	plt_copia_procedimento(nr_nova_prescr_w,nr_seq_regra_p,dt_prescricao_p,dt_prescricao_p,null,cd_perfil_p,cd_medico_p,'S',ds_lista_proced_w,'N',nm_usuario_p, cd_estabelecimento_p,ie_estende_inc_p,null);
end if;

if	(ds_lista_dieta_w	is not null) then
	plt_copia_dieta(nr_nova_prescr_w,nr_seq_regra_p,nm_usuario_p,'N',ds_lista_dieta_w, cd_perfil_p, cd_estabelecimento_p,ie_estende_inc_p);
end if;

ie_vazia_w	:= obter_se_prescr_vazia(nr_nova_prescr_w);

if	(ie_vazia_w	= 'S') then
	excluir_prescr_copia(nr_nova_prescr_w);
	nr_nova_prescricao_p	:= 0;
else
	consistir_itens_prescricao(nr_nova_prescr_w,nm_usuario_p,'A');
	plt_gerar_horarios_sem_lib(nr_nova_prescr_w,'N',nm_usuario_p);
	nr_nova_prescricao_p	:= nr_nova_prescr_w;
end if;

select	max(obter_valor_dominio(4138,ie_motivo))
into	ds_erro_w
from	w_copia_plano
where	nm_usuario	= nm_usuario_p
and	ie_tipo_item	= ie_tipo_item_w
and	dt_atualizacao	>= dt_inicio_copia_w
and	nr_prescricao	= nvl(nr_prescricao_p, nr_prescricao)
and	nr_seq_item	= nvl(nr_sequencia_p, nr_seq_item)
and	ie_permite	= 'N';

ds_erro_p	:= ds_erro_w;

end PLT_estender_prescricao;
/