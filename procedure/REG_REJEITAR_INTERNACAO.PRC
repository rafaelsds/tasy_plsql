create or replace
procedure reg_rejeitar_internacao(	nr_sequencia_p			number,
					ie_motivo_rejeicao_p	varchar2,
					nm_usuario_p		Varchar2) is 

ds_motivo_rejeicao_w	Varchar2(255);
begin
if	(nvl(nr_sequencia_p,0) > 0) then

	reg_update_motivo_rejeicao(ie_motivo_rejeicao_p, nr_sequencia_p, nm_usuario_p);
	
	gravar_integracao_regulacao(487, 'nr_sequencia='||NVL(nr_sequencia_p, 0)||';'); 
	
	update	regulacao_atendimento
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_status_regulacao	= '6',
		ie_motivo_cancelamento	= ie_motivo_rejeicao_p
	where	nr_sequencia		= nr_sequencia_p;
end if;

commit;

end reg_rejeitar_internacao;
/
