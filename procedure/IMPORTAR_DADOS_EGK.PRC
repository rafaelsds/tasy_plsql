create or replace procedure IMPORTAR_DADOS_EGK
			(ds_xml_pessoa_p	in varchar2,
			ds_xml_convenio_p	in varchar2,
			nm_usuario_p		in varchar2,
			nr_sequencia_p		out number,
			ds_conteudo_kvk_p	in varchar2 default null) is

pessoa_fisica_egk_w	pessoa_fisica_egk%rowtype;
cd_cnes_parcial_w   	pessoa_fisica_egk.abrechnender_kennung%type;
ds_rua_e_num_w      	varchar2(4000);
nr_espacos_w        	number;
ds_ultimo_elem_w    	varchar2(255);
ds_penultimo_elem_w 	varchar2(255);
ds_num_comp_casa_w  	varchar2(255);
ds_xml_pessoa_w		varchar2(4000);
ds_xml_aux_w		varchar2(4000);
ds_xml_convenio_w	varchar2(4000);
ds_xml_convenio_2_w	varchar2(4000);
ds_name_space_w		varchar2(255);
ie_check_gkv_w      varchar2(2) := 'N';
xml_pessoa_w		xmltype;
xml_convenio_w		xmltype;
kvk_cnes_prefix varchar2(2) := '%';

cursor c01 is
select	a.*
from	xmltable('Versicherter' passing xml_pessoa_w columns
		versicherten_id			varchar2(10) 	path 	'Versicherten_ID',
		geburtsdatum			varchar2(8) 	path 	'Person/Geburtsdatum',
		vorname				varchar2(45)	path 	'Person/Vorname',
		nachname      			varchar2(45)	path 	'Person/Nachname',
		geschlecht    			varchar2(1)	path 	'Person/Geschlecht',
		vorsatzwort   			varchar2(20)	path 	'Person/Vorsatzwort',
		namenszusatz  			varchar2(20)	path 	'Person/Namenszusatz',
		titel         			varchar2(20)	path 	'Person/Titel',
		postleitzahl_sa         	varchar2(10)	path	'Person/StrassenAdresse/Postleitzahl',
		ort_sa                  	varchar2(40)	path 	'Person/StrassenAdresse/Ort',
		strasse_sa              	varchar2(46)	path 	'Person/StrassenAdresse/Strasse',
		hausnummer_sa           	varchar2(9) 	path 	'Person/StrassenAdresse/Hausnummer',
		anschriftenzusatz_sa    	varchar2(40)	path 	'Person/StrassenAdresse/Anschriftenzusatz',
		wohnsitzlaendercode_sa  	varchar2(3) 	path 	'Person/StrassenAdresse/Land/Wohnsitzlaendercode',
		postleitzahl_pa         	varchar2(10)	path 	'Person/PostfachAdresse/Postleitzahl',
		ort_pa                  	varchar2(40)	path 	'Person/PostfachAdresse/Ort',
		postfach_pa             	varchar2(8) 	path 	'Person/PostfachAdresse/Postfach',
		wohnsitzlaendercode_pa  	varchar2(8) 	path 	'Person/PostfachAdresse/Land/Wohnsitzlaendercode') a;

c01_w	c01%rowtype;

cursor c02 is
select	a.*
from	xmltable('Versicherter' passing xml_convenio_w columns
		versicherungsschutz_beginn  	varchar2(8)	path	'Versicherungsschutz/Beginn',
		versicherungsschutz_ende    	varchar2(8)	path	'Versicherungsschutz/Ende',
		kostentraeger_kennung       	number(10)	path	'Versicherungsschutz/Kostentraeger/Kostentraegerkennung',
		kostentraeger_laendercode   	varchar2(3)	path	'Versicherungsschutz/Kostentraeger/Kostentraegerlaendercode',
		kostentraeger_name          	varchar2(45)	path	'Versicherungsschutz/Kostentraeger/Name',
		abrechnender_kennung        	number(10)	path	'Versicherungsschutz/Kostentraeger/AbrechnenderKostentraeger/Kostentraegerkennung',
		abrechnender_laendercode    	varchar2(3)	path    'Versicherungsschutz/Kostentraeger/AbrechnenderKostentraeger/Kostentraegerlaendercode',
		abrechnender_name           	varchar2(45)	path    'Versicherungsschutz/Kostentraeger/AbrechnenderKostentraeger/Name',
		besondere_personengruppe    	varchar2(2)	path	'Zusatzinfos/ZusatzinfosGKV/BesonderePersonengruppe',
		dmp_kennzeichnung           	varchar2(2)	path	'Zusatzinfos/ZusatzinfosGKV/DMP_Kennzeichnung',
		versichertenart             	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Versichertenart',
		zuzahlungsstatus            	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zuzahlungsstatus/Status',
		gueltig_bis                 	varchar2(8)	path	'Zusatzinfos/ZusatzinfosGKV/Zuzahlungsstatus/Gueltig_bis',
		zusatzinfos_wop             	varchar2(5)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/WOP',
		ruhen_beginn               	varchar2(8)	path	'Zusatzinfos/ZusatzinfosGKV/RuhenderLeistungsanspruch/Beginn',
		ruhen_ende                 	varchar2(8)	path	'Zusatzinfos/ZusatzinfosGKV/RuhenderLeistungsanspruch/Ende',
		ruhen_artdesruhens         	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/RuhenderLeistungsanspruch/ArtDesRuhens',
		selektiv_aerztlich         	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Selektivvertraege/Aerztlich',
		selektiv_zahnaerztlich     	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Selektivvertraege/Zahnaerztlich',
		selektiv_art               	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Selektivvertraege/Art',
		aerztlicheversorgung		varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Kostenerstattung/Selektivvertraege/AerztlicheVersorgung',
		stationaererbereich        	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Kostenerstattung/Selektivvertraege/StationaererBereich',
		zahnaerztlicheversorgung   	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Kostenerstattung/Selektivvertraege/ZahnaerztlicheVersorgung',
		veranlassteleistungen      	varchar2(1)	path	'Zusatzinfos/ZusatzinfosGKV/Zusatzinfos_Abrechnung_GKV/Kostenerstattung/Selektivvertraege/VeranlassteLeistungen') a;

c02_w	c02%rowtype;

cursor c03 is
select	a.*
from	xmltable('UC_GeschuetzteVersichertendatenXML' passing xml_convenio_w columns
		zuzahlungsstatus            	varchar2(1)	path	'Zuzahlungsstatus/Status',
		gueltig_bis                 	varchar2(8)	path	'Zuzahlungsstatus/Gueltig_bis',
		besondere_personengruppe    	varchar2(2)	path	'BesonderePersonengruppe',
		dmp_kennzeichnung           	varchar2(2)	path	'DMP_Kennzeichnung',
		selektiv_aerztlich         	varchar2(1)	path	'Selektivvertraege/Aerztlich',
		selektiv_zahnaerztlich     	varchar2(1)	path	'Selektivvertraege/Zahnaerztlich',
		selektiv_art               	varchar2(1)	path	'Selektivvertraege/Art',
		aerztlicheversorgung		varchar2(1)	path	'Selektivvertraege/AerztlicheVersorgung') a;

c03_w	c03%rowtype;

	function get_date(  data_w varchar2,
                        hour_w integer default 0,
                        minute_w integer default 0,
                        second_w integer default 0)
		return date is
	dt_retorno_w date;
	begin

	begin
		dt_retorno_w 	:= to_date(data_w,'yyyymmdd');
        if(dt_retorno_w is not null)then
        dt_retorno_w := pkg_date_utils.get_Time(dt_retorno_w, hour_w, minute_w, second_w);
        end if;
	exception
	when others then
		dt_retorno_w	:= null;
	end;

	return	 dt_retorno_w;

	end get_date;
	--################################################
	function get_hexa_as_string
			(nr_id_p	varchar2)
			return varchar2 is

	ds_string_hexa_w	varchar2(4000);
	lista_w			dbms_sql.varchar2_table;
	i 			integer;
	pos_inicial_w		number(10);
	qt_tam_w		number(10);
	ds_retorno_w		varchar(255);
	ds_charset_kvk_w    nls_database_parameters.value%type:= 'D7DEC'; -- DIN 66303
	ds_charset_base_w   nls_database_parameters.value%type;
	kvk_data_w          varchar2(4000);
	kvk_data_length_w   number;
	kvk_header_w        number;

	begin

	begin
        select  value
        into    ds_charset_base_w
        from    nls_database_parameters
        where   parameter='NLS_CHARACTERSET';

        kvk_data_w := replace(ds_conteudo_kvk_p,' ','');
        kvk_data_length_w := to_number(substr(kvk_data_w,3,2),'xx');
        kvk_header_w := 2;

        if kvk_data_length_w > 127 then
           kvk_header_w := 3;
        end if;

        ds_string_hexa_w := convert(utl_raw.cast_to_varchar2(replace(ds_conteudo_kvk_p,' ','')), ds_charset_base_w, ds_charset_kvk_w);

        lista_w := obter_lista_string(ds_conteudo_kvk_p, ' ');

        for i in lista_w.first..lista_w.last loop
        begin
            if (i > kvk_header_w) then --Ignora o Header
				if	(lista_w(i) = nr_id_p) then
					pos_inicial_w := i + 2;
					qt_tam_w := to_number(lista_w(i + 1),'xx');
					exit;
				end if;
			end if;
			end;
		end loop;

		if	(pos_inicial_w > 0) then
			select	substr(ds_string_hexa_w, pos_inicial_w, qt_tam_w)
			into	ds_retorno_w
			from	dual;
		end if;
	exception
	when others then
		ds_retorno_w := null;
	end;

	return	ds_retorno_w;

	end get_hexa_as_string;
	--################################################

	procedure proc_address(
			ds_endereco_orig_p	in varchar2,
			ds_endereco_p	out varchar2,
			nr_endereco_p	out varchar2) is

	endereco_vet_w	dbms_sql.varchar2_table;
	ds_endereco_w	varchar2(255);
	nr_endereco_w	varchar2(255);
	i   integer;

	begin
	endereco_vet_w := obter_lista_string(ds_endereco_orig_p, ' ');

	for i in 1..(endereco_vet_w.count) loop
		begin
		if	(i > 1) and
			(endereco_vet_w.count-1 = i) and
			(somente_numero(endereco_vet_w(i)) > 0) and
			((somente_numero(endereco_vet_w(i+1)) = 0) or
			((somente_numero(endereco_vet_w(i+1)) > 0) and
			(length(somente_numero(endereco_vet_w(i+1))) <> length(endereco_vet_w(i+1))))) then
			nr_endereco_w   :=  endereco_vet_w(i) || ' ' || endereco_vet_w(i+1);
			exit;
		elsif	(i > 1) and
			(endereco_vet_w.count = i) and
			(somente_numero(endereco_vet_w(i)) > 0) then
			nr_endereco_w   :=  endereco_vet_w(i);
		else
			ds_endereco_w   :=  trim(ds_endereco_w || ' ' || endereco_vet_w(i));
		end if;
		end;
	end loop;

	ds_endereco_p	:=	ds_endereco_w;
	nr_endereco_p	:=	nr_endereco_w;
	end proc_address;

begin

begin
select	pessoa_fisica_egk_seq.nextval
into	pessoa_fisica_egk_w.nr_sequencia
from	dual;

pessoa_fisica_egk_w.dt_atualizacao			:= sysdate;
pessoa_fisica_egk_w.nm_usuario				:= nm_usuario_p;
pessoa_fisica_egk_w.dt_atualizacao_nrec			:= sysdate;
pessoa_fisica_egk_w.nm_usuario_nrec			:= nm_usuario_p;

if	(ds_conteudo_kvk_p is not null) then
    
    pessoa_fisica_egk_w.ie_tipo_cartao := 2;

	pessoa_fisica_egk_w.versicherten_id		:= get_hexa_as_string('82');
	pessoa_fisica_egk_w.geburtsdatum		:= to_date(get_hexa_as_string('88'),'ddmmyyyy');
	pessoa_fisica_egk_w.vorname			:= get_hexa_as_string('85');
	pessoa_fisica_egk_w.nachname			:= get_hexa_as_string('87');
	pessoa_fisica_egk_w.titel			:= get_hexa_as_string('84');
	pessoa_fisica_egk_w.postleitzahl_sa		:= get_hexa_as_string('8B');
	pessoa_fisica_egk_w.ort_sa			:= get_hexa_as_string('8C');
	pessoa_fisica_egk_w.versichertenstatus_kvk	:= get_hexa_as_string('83');
	pessoa_fisica_egk_w.statuserganzung_kvk		:= get_hexa_as_string('90');

	-- Tratar nome da rua e numero da casa. Exemplo: "An der hohen Weide 12 D", Rua: "An der hohen Weide", Numero: "12 D"

	proc_address(get_hexa_as_string('89'), pessoa_fisica_egk_w.strasse_sa, pessoa_fisica_egk_w.hausnummer_sa);

	pessoa_fisica_egk_w.wohnsitzlaendercode_sa	:= nvl(get_hexa_as_string('8A'),'D');
	pessoa_fisica_egk_w.zusatzinfos_wop		:= ltrim(get_hexa_as_string('8F'),'0');
	pessoa_fisica_egk_w.versichertenart		:= substr(get_hexa_as_string('83'),1,1);
	pessoa_fisica_egk_w.versicherungsschutz_beginn := to_date(get_hexa_as_string('88'),'ddmmyyyy');

	begin
		pessoa_fisica_egk_w.gueltig_bis := last_day(to_date(get_hexa_as_string('8D'),'mmyy'));
	exception
	when others then
		pessoa_fisica_egk_w.gueltig_bis := null;
	end;
	pessoa_fisica_egk_w.versicherungsschutz_ende	:= pessoa_fisica_egk_w.gueltig_bis;

	if	(pessoa_fisica_egk_w.versichertenart = '0') then
		pessoa_fisica_egk_w.versichertenart := null;
	end if;
    
	if	(nvl(get_hexa_as_string('81'),'0000000') <> '0000000') then
		pessoa_fisica_egk_w.kostentraeger_kennung	:= get_hexa_as_string('81');
		pessoa_fisica_egk_w.kostentraeger_name		:= get_hexa_as_string('80');
		pessoa_fisica_egk_w.abrechnender_kennung	:= pessoa_fisica_egk_w.kostentraeger_kennung;
		pessoa_fisica_egk_w.abrechnender_name		:= pessoa_fisica_egk_w.kostentraeger_name;

		ie_check_gkv_w := 'S';

	elsif	(somente_numero(get_hexa_as_string('80')) > 0) then --Em alguns casos o codigo do convenio vem juntamente com a descricao, igual exemplo "Deutscher Ring K 4013293"
		pessoa_fisica_egk_w.kostentraeger_kennung	:= somente_numero(get_hexa_as_string('80'));
		pessoa_fisica_egk_w.kostentraeger_name		:= replace(get_hexa_as_string('80'), pessoa_fisica_egk_w.kostentraeger_kennung, null);
		pessoa_fisica_egk_w.abrechnender_kennung	:= pessoa_fisica_egk_w.kostentraeger_kennung;
		pessoa_fisica_egk_w.abrechnender_name		:= pessoa_fisica_egk_w.kostentraeger_name;
	end if;

	begin
        select  substr(pessoa_fisica_egk_w.kostentraeger_kennung, 0, 4)
        into    cd_cnes_parcial_w
        from    dual;
        
        kvk_cnes_prefix := '16';
    
       if(ie_check_gkv_w = 'S')then
            kvk_cnes_prefix := '';
            cd_cnes_parcial_w := pessoa_fisica_egk_w.kostentraeger_kennung;
        end if;

        if(cd_cnes_parcial_w is not null)then
            select  max(pj.cd_cnes)
            into    pessoa_fisica_egk_w.kostentraeger_kennung -- esse valor e sobrescrevido no select 1069872 - verificar uma possivel refatoracao
            from    convenio a,
                    pessoa_juridica pj
            where   a.cd_cgc in (	select  b.cd_cgc
                                    from    (select x.cd_cgc,
                                            length(x.ds_razao_social)
                                        from    pessoa_juridica x,
                                            convenio y
                                        where   x.cd_cgc                    = y.cd_cgc
										and 	x.ie_situacao				= 'A'
                                        and     y.ie_situacao               = 'A'
                                        and     x.cd_cnes like (kvk_cnes_prefix ||'%'|| cd_cnes_parcial_w ||'%')  
                                        order by   length(x.ds_razao_social) asc) b
                                    where   rownum = 1)
            and pj.cd_cgc = a.cd_cgc
			and pj.ie_situacao = 'A';

            if(pessoa_fisica_egk_w.kostentraeger_kennung is null)then
                pessoa_fisica_egk_w.kostentraeger_kennung := get_hexa_as_string('81');
                pessoa_fisica_egk_w.abrechnender_kennung := pessoa_fisica_egk_w.kostentraeger_kennung;
            else
                pessoa_fisica_egk_w.abrechnender_kennung := pessoa_fisica_egk_w.kostentraeger_kennung;
            end if;

        end if;
    end;
    
	/*pessoa_fisica_egk_w.versicherungsschutz_beginn	:= get_date(c02_w.versicherungsschutz_beginn, 0, 0, 1);
	pessoa_fisica_egk_w.versicherungsschutz_ende	:= get_date(c02_w.versicherungsschutz_ende, 23, 59, 59);
	pessoa_fisica_egk_w.abrechnender_laendercode	:= c02_w.abrechnender_laendercode;
	pessoa_fisica_egk_w.kostentraeger_laendercode	:= c02_w.kostentraeger_laendercode;
	pessoa_fisica_egk_w.zuzahlungsstatus		:= c02_w.zuzahlungsstatus;
	pessoa_fisica_egk_w.gueltig_bis			:= c02_w.gueltig_bis;
	pessoa_fisica_egk_w.geschlecht			:= c01_w.geschlecht;
	pessoa_fisica_egk_w.vorsatzwort			:= c01_w.vorsatzwort;
	pessoa_fisica_egk_w.namenszusatz		:= c01_w.namenszusatz;
	pessoa_fisica_egk_w.anschriftenzusatz_sa	:= c01_w.anschriftenzusatz_sa;
	pessoa_fisica_egk_w.postleitzahl_pa		:= c01_w.postleitzahl_pa;
	pessoa_fisica_egk_w.ort_pa			:= c01_w.ort_pa;
	pessoa_fisica_egk_w.postfach_pa			:= c01_w.postfach_pa;
	pessoa_fisica_egk_w.wohnsitzlaendercode_pa	:= c01_w.wohnsitzlaendercode_pa;
	pessoa_fisica_egk_w.besondere_personengruppe	:= c02_w.besondere_personengruppe;
	pessoa_fisica_egk_w.dmp_kennzeichnung		:= c02_w.dmp_kennzeichnung;
	pessoa_fisica_egk_w.ruhen_beginn		:= get_date(c02_w.ruhen_beginn);
	pessoa_fisica_egk_w.ruhen_ende			:= get_date(c02_w.ruhen_ende);
	pessoa_fisica_egk_w.ruhen_artdesruhens		:= c02_w.ruhen_artdesruhens;
	pessoa_fisica_egk_w.selektiv_aerztlich		:= c02_w.selektiv_aerztlich;
	pessoa_fisica_egk_w.selektiv_zahnaerztlich	:= c02_w.selektiv_zahnaerztlich;
	pessoa_fisica_egk_w.selektiv_art		:= c02_w.selektiv_art;
	pessoa_fisica_egk_w.stationaererbereich		:= c02_w.stationaererbereich;
	pessoa_fisica_egk_w.zahnaerztlicheversorgung	:= c02_w.zahnaerztlicheversorgung;
	pessoa_fisica_egk_w.veranlassteleistungen	:= c02_w.veranlassteleistungen;
	pessoa_fisica_egk_w.aerztlicheversorgung	:= c02_w.aerztlicheversorgung*/

end if;

if	(ds_xml_pessoa_p is not null) then

    pessoa_fisica_egk_w.ie_tipo_cartao := 1;
    
	--Tratamento necessario para conseguir abrir o XML
	ds_name_space_w	:= substr(substr(ds_xml_pessoa_p,instr(ds_xml_pessoa_p,'xmlns:',1)+6,4000),1,instr(substr(ds_xml_pessoa_p,instr(ds_xml_pessoa_p,'xmlns:',1)+6,4000),'=')-1) || ':';

	ds_xml_pessoa_w	:= replace(ds_xml_pessoa_p,ds_name_space_w,''); --Remove namespace
	ds_xml_pessoa_w	:= substr(ds_xml_pessoa_w,instr(ds_xml_pessoa_w,'<Versicherter>'),length(ds_xml_pessoa_w)); --Remove cabecalho
	ds_xml_pessoa_w	:= replace(ds_xml_pessoa_w,'</UC_PersoenlicheVersichertendatenXML>','');--Remove rodape

	xml_pessoa_w 	:= xmltype.createxml(to_clob(ds_xml_pessoa_w));

	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;

		pessoa_fisica_egk_w.versicherten_id			:= c01_w.versicherten_id;
		pessoa_fisica_egk_w.geburtsdatum			:= get_date(c01_w.geburtsdatum);
		pessoa_fisica_egk_w.vorname				:= c01_w.vorname;
		pessoa_fisica_egk_w.nachname				:= c01_w.nachname;
		pessoa_fisica_egk_w.geschlecht				:= c01_w.geschlecht;
		pessoa_fisica_egk_w.vorsatzwort				:= c01_w.vorsatzwort;
		pessoa_fisica_egk_w.namenszusatz			:= c01_w.namenszusatz;
		pessoa_fisica_egk_w.titel				:= c01_w.titel;
		pessoa_fisica_egk_w.postleitzahl_sa			:= c01_w.postleitzahl_sa;
		pessoa_fisica_egk_w.ort_sa				:= c01_w.ort_sa;
		pessoa_fisica_egk_w.strasse_sa				:= c01_w.strasse_sa;
		pessoa_fisica_egk_w.hausnummer_sa			:= c01_w.hausnummer_sa;
		pessoa_fisica_egk_w.anschriftenzusatz_sa		:= c01_w.anschriftenzusatz_sa;
		pessoa_fisica_egk_w.wohnsitzlaendercode_sa		:= c01_w.wohnsitzlaendercode_sa;
		pessoa_fisica_egk_w.postleitzahl_pa			:= c01_w.postleitzahl_pa;
		pessoa_fisica_egk_w.ort_pa				:= c01_w.ort_pa;
		pessoa_fisica_egk_w.postfach_pa				:= c01_w.postfach_pa;
		pessoa_fisica_egk_w.wohnsitzlaendercode_pa		:= c01_w.wohnsitzlaendercode_pa;

	end loop;
	close C01;
end if;

if	(ds_xml_convenio_p is not null) then
	pessoa_fisica_egk_w.ie_tipo_cartao := 1;

	--remove namespace
	ds_xml_convenio_w	:= replace(ds_xml_convenio_p, chr(92) || chr(34),chr(34));
	ds_xml_convenio_2_w	:= ds_xml_convenio_w;
	ds_xml_convenio_w	:= replace(ds_xml_convenio_w,'vsda:',''); --Remove namespace
	ds_xml_convenio_w	:= substr(ds_xml_convenio_w,instr(ds_xml_convenio_w,'<UC_AllgemeineVersicherungsdatenXML'),4000);
	ds_xml_convenio_w	:= substr(ds_xml_convenio_w,1,instr(ds_xml_convenio_w,'</UC_AllgemeineVersicherungsdatenXML>') + 36);
	ds_xml_aux_w 		:= substr(ds_xml_convenio_w,instr(ds_xml_convenio_w,'<UC_AllgemeineVersicherungsdatenXML')+35,4000);
	ds_xml_convenio_w	:= replace(ds_xml_convenio_w, substr(ds_xml_aux_w,1, instr(ds_xml_aux_w,'>')-1), '');

	--Tratamento para conseguir ler o XML
	ds_xml_convenio_w	:= substr(ds_xml_convenio_w,instr(ds_xml_convenio_w,'<Versicherter>'),length(ds_xml_convenio_w)); --Remove cabecalho
	ds_xml_convenio_w	:= replace(ds_xml_convenio_w,'</UC_AllgemeineVersicherungsdatenXML>','');--Remove rodape

	xml_convenio_w 	:= xmltype.createxml(to_clob(ds_xml_convenio_w));

	open C02;
	loop
	fetch C02 into
		c02_w;
	exit when C02%notfound;

		pessoa_fisica_egk_w.versicherungsschutz_beginn	:= get_date(c02_w.versicherungsschutz_beginn, 0, 0, 1);
		pessoa_fisica_egk_w.versicherungsschutz_ende	:= get_date(c02_w.versicherungsschutz_ende, 23, 59, 59);
		pessoa_fisica_egk_w.kostentraeger_kennung	:= c02_w.kostentraeger_kennung;
		pessoa_fisica_egk_w.kostentraeger_laendercode	:= c02_w.kostentraeger_laendercode;
		pessoa_fisica_egk_w.kostentraeger_name		:= c02_w.kostentraeger_name;
		pessoa_fisica_egk_w.abrechnender_kennung	:= c02_w.abrechnender_kennung;
		pessoa_fisica_egk_w.abrechnender_laendercode	:= c02_w.abrechnender_laendercode;
		pessoa_fisica_egk_w.abrechnender_name		:= c02_w.abrechnender_name;
		pessoa_fisica_egk_w.besondere_personengruppe	:= c02_w.besondere_personengruppe;
		pessoa_fisica_egk_w.dmp_kennzeichnung		:= c02_w.dmp_kennzeichnung;
		pessoa_fisica_egk_w.versichertenart		:= c02_w.versichertenart;
		pessoa_fisica_egk_w.zuzahlungsstatus		:= c02_w.zuzahlungsstatus;
		pessoa_fisica_egk_w.gueltig_bis			:= c02_w.gueltig_bis;
		pessoa_fisica_egk_w.zusatzinfos_wop		:= c02_w.zusatzinfos_wop;
		pessoa_fisica_egk_w.ruhen_beginn		:= get_date(c02_w.ruhen_beginn);
		pessoa_fisica_egk_w.ruhen_ende			:= get_date(c02_w.ruhen_ende);
		pessoa_fisica_egk_w.ruhen_artdesruhens		:= c02_w.ruhen_artdesruhens;
		pessoa_fisica_egk_w.selektiv_aerztlich		:= c02_w.selektiv_aerztlich;
		pessoa_fisica_egk_w.selektiv_zahnaerztlich	:= c02_w.selektiv_zahnaerztlich;
		pessoa_fisica_egk_w.selektiv_art		:= c02_w.selektiv_art;
		pessoa_fisica_egk_w.stationaererbereich		:= c02_w.stationaererbereich;
		pessoa_fisica_egk_w.zahnaerztlicheversorgung	:= c02_w.zahnaerztlicheversorgung;
		pessoa_fisica_egk_w.veranlassteleistungen	:= c02_w.veranlassteleistungen;
		pessoa_fisica_egk_w.aerztlicheversorgung	:= c02_w.aerztlicheversorgung;

	end loop;
	close C02;

	if	(instr(ds_xml_convenio_2_w,'UC_GeschuetzteVersichertendatenXML') > 0) then

		--Tratamento para conseguir ler o XML
		ds_xml_convenio_2_w	:= replace(ds_xml_convenio_2_w,'vsdg:',''); --Remove namespace
		ds_xml_convenio_2_w	:= substr(ds_xml_convenio_2_w,instr(ds_xml_convenio_2_w,'<UC_GeschuetzteVersichertendatenXML'),instr(ds_xml_convenio_2_w,'</UC_GeschuetzteVersichertendatenXML>') + 37);

		ds_xml_aux_w := substr(ds_xml_convenio_2_w,instr(ds_xml_convenio_2_w,'<UC_GeschuetzteVersichertendatenXML')+35,4000);
		ds_xml_convenio_2_w	:= replace(ds_xml_convenio_2_w, substr(ds_xml_aux_w,1, instr(ds_xml_aux_w,'>')-1), '');
		xml_convenio_w 	:= xmltype.createxml(to_clob(ds_xml_convenio_2_w));

		open C03;
		loop
		fetch C03 into
			c03_w;
		exit when C03%notfound;

			pessoa_fisica_egk_w.zuzahlungsstatus		:= c03_w.zuzahlungsstatus;
			pessoa_fisica_egk_w.gueltig_bis			:= get_date(c03_w.gueltig_bis);
			pessoa_fisica_egk_w.besondere_personengruppe	:= c03_w.besondere_personengruppe;
			pessoa_fisica_egk_w.dmp_kennzeichnung		:= c03_w.dmp_kennzeichnung;
			pessoa_fisica_egk_w.selektiv_aerztlich		:= c03_w.selektiv_aerztlich;
			pessoa_fisica_egk_w.selektiv_zahnaerztlich	:= c03_w.selektiv_zahnaerztlich;
			pessoa_fisica_egk_w.selektiv_art		:= c03_w.selektiv_art;
			pessoa_fisica_egk_w.aerztlicheversorgung	:= c03_w.aerztlicheversorgung;

		end loop;
		close C03;
	end if;

end if;

--Exclui os registros ja lidos da mesma pessoa, que ainda nao esta vinculado a uma PF do Tasy
delete 	from pessoa_fisica_egk
where	cd_pessoa_fisica 	is null
and	nm_usuario		= nm_usuario_p
and	versicherten_id		= pessoa_fisica_egk_w.versicherten_id
and	geburtsdatum		= pessoa_fisica_egk_w.geburtsdatum
and	geschlecht		= pessoa_fisica_egk_w.geschlecht;

pessoa_fisica_egk_w.ds_xml_pessoa	:= ds_xml_pessoa_p;
pessoa_fisica_egk_w.ds_xml_convenio	:= ds_xml_convenio_p;
pessoa_fisica_egk_w.ds_conteudo_kvk	:= ds_conteudo_kvk_p;

insert into pessoa_fisica_egk values pessoa_fisica_egk_w;

nr_sequencia_p	:= pessoa_fisica_egk_w.nr_sequencia;

commit;

exception
when others then
	nr_sequencia_p	:= null;
end;

end IMPORTAR_DADOS_EGK;
/
