create or replace
procedure baixar_procedimento_ge (
		ie_status_execucao_p	varchar2,
		nr_seq_propaci_p	number,
		nr_prescricao_p		number,
		nr_seq_procedimento_p	number,
		nr_interno_conta_p	number,
		ie_recalcular_conta_p	varchar2,
		cd_motivo_baixa_p	number,
		ie_atualizar_adep_p	varchar2,
		nr_atendimento_p	number,
		ie_informa_justif_p	varchar2,
		nr_seq_motivo_just_p	number,
		nm_usuario_p		varchar2) is

ie_cancela_w varchar2(1) := 'N';
ie_considera_baixa_conta_w varchar2(1) := 'N';

--Usado na Gestap de exames (Swing)		
begin

ie_considera_baixa_conta_w := nvl(obter_valor_param_usuario(942,360,
	obter_perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo),'N');
	

if	(ie_status_execucao_p = '20') or 
	(((ie_status_execucao_p = '10') or 
	  (ie_status_execucao_p = '15')) and
	   ie_considera_baixa_conta_w  = 'S') then 
	   
	if  (nr_seq_propaci_p is not null) then
	
		delete
		from	procedimento_paciente
		where	nr_sequencia = nr_seq_propaci_p;
	else
		delete
		from	procedimento_paciente
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia_prescricao = nr_seq_procedimento_p;	
	end if;
end if;

if	(nr_interno_conta_p is not null) and
	(ie_recalcular_conta_p = 'S') then
	begin
	recalcular_conta_paciente(nr_interno_conta_p,nm_usuario_p);
	end;
end if;

update 	prescr_procedimento 
set 	cd_motivo_baixa = cd_motivo_baixa_p,
	dt_baixa = sysdate,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate ,
	ie_status_execucao = 'BE',
	nm_usuario_baixa_esp = nm_usuario_p
where 	nr_prescricao = nr_prescricao_p
and 	nr_sequencia = nr_seq_procedimento_p;

if	(ie_informa_justif_p = 'S') then
	update 	prescr_procedimento 
	set	nr_seq_motivo_just = nr_seq_motivo_just_p
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia = nr_seq_procedimento_p;
end if;

if 	(nvl(ie_atualizar_adep_p,'N') = 'S') then
	Baixar_horarios_proc_esp(nr_atendimento_p, nr_prescricao_p, nr_seq_procedimento_p, nm_usuario_p, '1');
end if;

select  nvl(max(ie_cancelar_autor),'N') ie_cancela
into	ie_cancela_w 
from    tipo_baixa_prescricao 
where   ie_prescricao_devolucao = 'P'
and     cd_tipo_baixa           = cd_motivo_baixa_p;

if 	(ie_cancela_w = 'S') then
	suspender_autor_convenio(nr_prescricao_p, nr_seq_procedimento_p, null, nm_usuario_p);
end if;

commit;

end baixar_procedimento_ge;
/
