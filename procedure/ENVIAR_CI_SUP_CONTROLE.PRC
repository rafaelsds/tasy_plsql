create or replace
procedure enviar_ci_sup_controle is 

nm_usuario_exec_w		man_ordem_servico.nm_usuario_exec%type;
nr_seq_ordem_w			man_ordem_servico.nr_sequencia%type;
ds_estagio_w			MAN_ESTAGIO_PROCESSO.ds_estagio%type;
cd_setor_usuario_w		usuario.cd_setor_atendimento%type;
qt_dias_estagio_w		varchar2(10);
ds_mensagem_w			varchar2(4000);
ds_ordens_w			varchar2(4000);
ds_ordens_tec_w			varchar2(4000);
ds_erro_w			varchar2(4000);
nm_usuario_adic_w		usuario.nm_usuario%type;
nm_usuario_dest_w		varchar2(255);
ds_mensagem_princ_w		varchar2(255);
ds_mensagem_tec_w		varchar2(8000);
nr_seq_grupo_w			grupo_suporte.nr_sequencia%type;
nm_usuario_tec_w		usuario.nm_usuario%type;
qt_ordens_w				number(10) := 0;
qt_total_os_grupo_w		number(10) := 0;
ie_erro_w				varchar2(1);
ds_classif_origem_w		varchar2(30);
i number(10);
l number(10);

/* Busca grupos do suporte */
Cursor c00 is
select	nr_sequencia
from	grupo_suporte
where	ie_situacao = 'A';

/* Busca os usu�rios ativos do grupo do suporte */
Cursor	C01 is
select	a.nm_usuario
from	usuario a,
		usuario_grupo_sup b
where	a.nm_usuario = b.nm_usuario_grupo
and		b.nr_seq_grupo = nr_seq_grupo_w;

/* Busca as OS's do usu�rio do grupo de suporte */
/* para enviar o comunicado */
Cursor C02 is
select 	a.nr_sequencia,
		b.ds_estagio,
		round(man_obter_tempo_estagio(c.nr_sequencia,c.nr_seq_ordem,'M') / 1440),
		substr(obter_valor_dominio(1149,a.ie_classificacao_cliente),1,30)
from	man_ordem_servico a,
		man_estagio_processo b,
		man_ordem_serv_estagio c
where	a.nr_seq_estagio 	= b.nr_sequencia
and		c.nr_seq_ordem		= a.nr_sequencia
and		b.nr_sequencia 		= c.nr_seq_estagio
and		c.nr_sequencia 		= (
				select	max(x.nr_sequencia)
				from 	man_ordem_serv_estagio x
				where	x.nr_seq_ordem = a.nr_sequencia
				and		x.nr_seq_estagio = b.nr_sequencia)
and		((b.ie_desenv = 'S') or (ie_tecnologia = 'S'))
and		a.ie_classificacao	= 'D'
and		a.nm_usuario_exec	= nm_usuario_exec_w
and		a.nr_seq_grupo_sup	= nr_seq_grupo_w
and		a.dt_fim_real is null;


/* Buscar as OS's do grupo que n�o possuem */
/* nenhum executor que perten�a ao mesmo */
Cursor C03 is
select 	a.nr_sequencia,
		b.ds_estagio,
		round(man_obter_tempo_estagio(c.nr_sequencia,c.nr_seq_ordem,'M') / 1440)
from	man_ordem_servico a,
		man_estagio_processo b,
		man_ordem_serv_estagio c
where	a.nr_seq_estagio 	= b.nr_sequencia
and		c.nr_seq_ordem		= a.nr_sequencia
and		b.nr_sequencia 		= c.nr_seq_estagio
and		c.nr_sequencia 		= (
				select	max(x.nr_sequencia)
				from 	man_ordem_serv_estagio x
				where	x.nr_seq_ordem = a.nr_sequencia
				and		x.nr_seq_estagio = b.nr_sequencia)
and		((b.ie_desenv = 'S') or (ie_tecnologia = 'S'))
and		a.ie_classificacao	= 'D'
and		a.nr_seq_grupo_sup	= nr_seq_grupo_w
and		a.dt_fim_real is null
and		not exists (
				select	1
				from	usuario_grupo_sup x
				where	x.nr_seq_grupo = a.nr_seq_grupo_sup
				and		x.nm_usuario_grupo = a.nm_usuario_exec)
and		not exists (
				select	1
				from	man_ordem_servico_exec x
				where	x.nr_seq_ordem = a.nr_sequencia
				and		not exists (
								select	1
								from	usuario_grupo_sup y
								where	y.nr_seq_grupo = a.nr_seq_grupo_sup
								and		y.nm_usuario_grupo = x.nm_usuario_exec));
								
cursor	c04 is
select	a.nm_usuario_grupo
from	grupo_suporte b,
		usuario_grupo_sup a
where	a.nr_seq_grupo = b.nr_sequencia
and		b.nr_sequencia = nr_seq_grupo_w
and		b.nr_sequencia <> 81 -- Desconsiderar grupo de plant�o
and		a.ie_funcao_usuario = 'S';

begin

ie_erro_w	:= 'N';
ds_mensagem_princ_w := 'As ordens de servi�o abaixo est�o com o Desenvolvimento a respectivos dias, favor verificar pois OS de d�vida s�o responsabilidade do suporte!';

open c00;
loop
fetch c00 into
	nr_seq_grupo_w;
exit when c00%notfound;
	begin

	open C01;
	loop
	fetch C01 into	
		nm_usuario_exec_w;
	exit when C01%notfound;
		begin
		if	(nm_usuario_exec_w is not null) then
		
			ds_ordens_w		:= '';
			ds_mensagem_w	:= '';
			qt_ordens_w		:= 0;
			
			open C02;
			loop
			fetch C02 into	
				nr_seq_ordem_w,
				ds_estagio_w,		
				qt_dias_estagio_w,
				ds_classif_origem_w;
			exit when C02%notfound;
				begin
						
				if	(nr_seq_ordem_w > 0) then
					ds_ordens_w	:= ds_ordens_w || chr(10) || chr(13) || nr_seq_ordem_w || ' - ' || qt_dias_estagio_w || ' dia(s) - ' || ds_estagio_w || ' (Origem: ' || ds_classif_origem_w || ')' || chr(10) || chr(13);
					qt_ordens_w	:= qt_ordens_w + 1;
				end if;
				
				end;
			end loop;
			close C02;
							
			if	(ds_ordens_w is not null) then				
				ds_mensagem_w := substr(ds_mensagem_princ_w || chr(10) || chr(13) || ds_ordens_w,1,4000);				
					begin
					
					Gerar_Comunic_Padrao(
							sysdate,
							'Alerta de ordem de servi�o no Desenvolvimento',
							ds_mensagem_w,
							'Tasy',
							'N',
							nm_usuario_exec_w,
							'N',
							null,
							null,
							null,
							null,
							sysdate,
							null,
							null);
				
					exception when others then
						ds_mensagem_w := '';
						ds_ordens_w	:= '';
						ds_erro_w	:= sqlerrm;
						ie_erro_w	:= 'S';
						gravar_log_tasy(28069,substr('Usu�rio: ' || nm_usuario_exec_w || ' Erro: ' || ds_erro_w,1,1400), 'TasyJob'); 
					end;
			end if;
			
			if	(ds_ordens_w is not null) then
				begin
				
				ds_mensagem_tec_w	:=	substr(ds_mensagem_tec_w || chr(10) || chr(13) || 'Executor: ' || nm_usuario_exec_w || ' (' || qt_ordens_w || ')' || chr(10) || chr(13) || ds_ordens_w || chr(10) || chr(13),1,7999);
				qt_total_os_grupo_w	:= qt_total_os_grupo_w + qt_ordens_w;
				
				end;
			end if;

		end if;
		end;
	end loop;
	close C01;
	
	ds_mensagem_tec_w	:= substr(ds_mensagem_princ_w || chr(10) || chr(13) || chr(10) || chr(13) || 
									ds_mensagem_tec_w || chr(10) || chr(13) || chr(10) || chr(13) || 
									'Total Grupo: ' || to_char(qt_total_os_grupo_w),1,8000);
	
	open c04;
	loop
	fetch c04 into
		nm_usuario_tec_w;
	exit when c04%notfound;
		begin
	
		if	(ds_mensagem_tec_w is not null) then			
			begin
				
			Gerar_Comunic_Padrao(
							sysdate,
							'Alerta de ordem de servi�o no Desenvolvimento (' || to_char(qt_total_os_grupo_w) || ')',
							ds_mensagem_tec_w,
							'Tasy',
							'N',
							nm_usuario_tec_w,
							'N',
							null,
							null,
							null,
							null,
							sysdate,
							null,
							null);			
			exception when others then
						ds_mensagem_w := '';
						ds_ordens_w	:= '';
						ds_erro_w	:= sqlerrm;
						ie_erro_w	:= 'S';
						gravar_log_tasy(28069,substr('Usu�rio: ' || nm_usuario_tec_w || ' Erro: ' || ds_erro_w,1,1400), 'TasyJob'); 
			end;
		end if;
		
		end;
	end loop;
	close c04;
	
	/* Busca OS's do grupo sem executor do mesmo e envia apenas para o t�cnico/analista */
	ds_ordens_w	:= '';
	qt_ordens_w := 0;
	ds_mensagem_tec_w	:= '';
	open C03;
	loop
	fetch C03 into	
		nr_seq_ordem_w,
		ds_estagio_w,		
		qt_dias_estagio_w;
	exit when C03%notfound;
		begin

		if	(nr_seq_ordem_w > 0) then
			ds_ordens_w	:= ds_ordens_w || chr(10) || chr(13) || nr_seq_ordem_w || ' - ' || qt_dias_estagio_w || ' dias' || chr(10) || chr(13);
			qt_ordens_w	:= qt_ordens_w + 1;
		end if;
		
		end;
	end loop;
	close C03;
	
	if	(ds_ordens_w is not null) then
		ds_mensagem_w	:= 'As ordens de servi�o listadas abaixo est�o no seu grupo de suporte, por�m, sem executor inicial do mesmo: ';		
		ds_mensagem_tec_w	:= substr(ds_mensagem_w || chr(10) || chr(13) || ds_ordens_w || chr(10) || chr(13) || 'Total Grupo: ' || to_char(qt_ordens_w),1,4000);
			begin
			
			open c04;
			loop
			fetch c04 into
				nm_usuario_tec_w;
			exit when c04%notfound;
				begin
			
				Gerar_Comunic_Padrao(
							sysdate,
							'Alerta de ordem de servi�o no Desenvolvimento sem executor do grupo',
							ds_mensagem_tec_w,
							'Tasy',
							'N',
							nm_usuario_tec_w,
							'N',
							null,
							null,
							null,
							null,
							sysdate,
							null,
							null);

				end;
			end loop;
			close c04;
							
			exception when others then
						ds_mensagem_w := '';
						ds_ordens_w	:= '';
						ds_erro_w	:= sqlerrm;
						ie_erro_w	:= 'S';
						gravar_log_tasy(28069,substr('Usu�rio: ' || nm_usuario_tec_w || ' Erro: ' || ds_erro_w,1,1400), 'TasyJob'); 
			end;
	end if;
	
	ds_ordens_w			:= '';
	ds_mensagem_w		:= '';
	ds_mensagem_tec_w	:= '';
	nm_usuario_tec_w	:= '';
	qt_total_os_grupo_w := 0;
	qt_ordens_w			:= 0;
		
	end;
end loop;
close c00;

if	(ie_erro_w = 'S') then
	begin
	ds_mensagem_w	:= 'Problema na execu��o da JOB da rotina ENVIAR_CI_SUP_CONTROLE. Verifique o LOG 28069 da tabela LOG_TASY';
	Gerar_Comunic_Padrao(
				sysdate,
				'Erro na execu��o da rotina ENVIAR_CI_SUP_CONTROLE',
				ds_mensagem_w,
				'Tasy',
				'N',
				'rfoliveira',
				'N',
				null,
				null,
				null,
				null,
				sysdate,
				null,
				null);
	end;
end if;

commit;

end enviar_ci_sup_controle;
/