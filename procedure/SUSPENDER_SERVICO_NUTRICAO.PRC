create or replace
procedure suspender_servico_nutricao (	nr_prescricao_p		number,
					nm_usuario_p		Varchar2) is 
					
nr_seq_serv_dia_w	number(10);

Cursor C01 is
	select	a.nr_sequencia
	from	nut_atend_serv_dia a,
		nut_atend_serv_dia_rep b
	where	a.nr_sequencia = b.nr_seq_serv_dia
	and	a.dt_liberacao is null
	and	((nr_prescr_oral  = nr_prescricao_p)
	or	(nr_prescr_jejum = nr_prescricao_p)
	or	(nr_prescr_compl = nr_prescricao_p)
	or	(nr_prescr_enteral = nr_prescricao_p)
	or	(nr_prescr_npt_adulta = nr_prescricao_p)
	or	(nr_prescr_npt_neo = nr_prescricao_p)
	or	(nr_prescr_npt_ped = nr_prescricao_p));
					
begin

open c01;
loop
fetch c01 into nr_seq_serv_dia_w;
exit when c01%notfound;

	update	nut_atend_serv_dia
	set	dt_suspensao = sysdate,
		nm_usuario_susp = nm_usuario_p
	where	nr_sequencia = nr_seq_serv_dia_w;
end loop;
close c01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end suspender_servico_nutricao;
/
