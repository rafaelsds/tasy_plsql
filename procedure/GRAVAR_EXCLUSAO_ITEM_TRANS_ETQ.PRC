create or replace
procedure gravar_exclusao_item_trans_etq(
			nr_ordem_compra_p	number,
			nr_item_oci_p		number,
			nm_usuario_p		Varchar2) is 

cd_material_w	number(6);
ds_material_w	varchar2(255);
ds_historico_w	varchar2(255);

begin
select	cd_material,
	substr(obter_desc_material(cd_material),1,255)
into	cd_material_w,
	ds_material_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p;

ds_historico_w := substr(WHEB_MENSAGEM_PCK.get_texto(302836,'NR_ITEM_OCI_P='|| NR_ITEM_OCI_P ||';CD_MATERIAL_W='|| CD_MATERIAL_W ||';DS_MATERIAL_W='|| DS_MATERIAL_W),1,255);
		/*Foi realizada a exclus�o do item #@NR_ITEM_OCI_P#@ [#@CD_MATERIAL_W#@] - #@DS_MATERIAL_W#@*/

inserir_historico_ordem_compra(
	nr_ordem_compra_p,
	'S',
	Wheb_mensagem_pck.get_Texto(302837), /*'Exclus�o de item ap�s estorno da libera��o',*/
	ds_historico_w,
	nm_usuario_p);
commit;
end gravar_exclusao_item_trans_etq;
/
