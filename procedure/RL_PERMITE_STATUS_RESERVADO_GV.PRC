create or replace
procedure rl_permite_status_reservado_gv(
					cd_paciente_reserva_p	varchar2,
					nr_seq_unidade_p	number,
					ds_motivo_reserva_p	varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2
					) is

ie_reserva_paciente_w		varchar(1) := 'N';
ie_consiste_sexo_unid_w 	varchar(1) := 'N';
ie_permite_status_rese_gv_w	varchar(1) := 'N';
					
begin

obter_param_usuario(44, 180, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_permite_status_rese_gv_w);
obter_param_usuario(44, 188, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_sexo_unid_w);

if	(ie_permite_status_rese_gv_w = 'S') and
	(cd_paciente_reserva_p is not null) then
	begin
	
	ie_reserva_paciente_w := verifica_reserva_paciente(cd_paciente_reserva_p);
	
	if	(ie_reserva_paciente_w = 'S') then
		begin
		
		altera_status_gv_aguardando(cd_paciente_reserva_p,nm_usuario_p);
		
		commit;
		end;
	end if;
	end;
end if;

if	(ie_consiste_sexo_unid_w = 'J') then
	begin
	
	atualiza_justif_res_sexo_dif(nr_seq_unidade_p,nm_usuario_p,ds_motivo_reserva_p);
	
	end;
end if;



end rl_permite_status_reservado_gv;
/