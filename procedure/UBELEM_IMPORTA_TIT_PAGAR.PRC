create or replace procedure UBELEM_IMPORTA_TIT_PAGAR(cd_estabelecimento_p	number,
							nm_usuario_p		varchar2) is
							
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_cgc_w			pessoa_juridica.cd_cgc%type;
nr_titulo_w			titulo_pagar.nr_titulo%type;
dt_emissao_w			titulo_pagar.dt_emissao%type;
dt_vencimento_w			titulo_pagar.dt_vencimento_original%type;
vl_titulo_w			titulo_pagar.vl_titulo%type;
cd_moeda_w			titulo_pagar.cd_moeda%type;
tx_juros_w			titulo_pagar.tx_juros%type;
tx_multa_w			titulo_pagar.tx_multa%type;
cd_tipo_multa_w			titulo_pagar.cd_tipo_taxa_multa%type;
cd_tipo_juros_w			titulo_pagar.cd_tipo_taxa_juro%type;
ie_origem_w			titulo_pagar.ie_origem_titulo%type;
ie_tipo_titulo_w		titulo_pagar.ie_tipo_titulo%type;
cd_estab_financeiro_w		titulo_pagar.cd_estab_financeiro%type;
ie_data_contabil_w		varchar2(20);
dt_contabil_venc_w		date;
dt_contabil_w			date;
dt_contabil_mes_ant_w		date;
nr_documento_sg_w		number(10);
cd_pessoa_si_w			varchar2(20);
cd_empresa_sg_w			number(3);
cd_cliente_sg_w			number(15);
cd_cpf_w			varchar2(11);
cd_cnpj_w			varchar2(14);
cd_cei_w			varchar2(14);
cd_operacao_sg_w		varchar2(1);
cd_tipo_pessoa_sg_w		varchar2(1);
dt_prorrogacao_sg_w		date;
dt_cancelamento_sg_w		date;
ds_observacao_w			varchar2(200);
ie_existe_pessoa_w		varchar2(1);
cd_tipo_doc_sg_w		number(2);
ds_erro_w			varchar2(500);
cd_documento_si_w		varchar2(20);
dt_vencimento_tasy_w		date;
dt_prorrogacao_tasy_w		date;
dt_emissao_tasy_w		date;
cd_status_doc_sg_w		varchar2(1);
ie_tabela_origem_sg_w		varchar2(1);

cursor c01 is 	select	a."AutoId",
			a."Dat_Emissao",
			a."Dat_Vencimento",
			a."Val_Documento",
			a."Dat_Prorrogacao",
			a."Dat_Cancelamento",
			a."Cod_Oper_Registro",
			a."Dsc_Observacao",
			'C' tabela_origem,
			b."Cod_Cliente_Si",
			b."Cod_Empresa_Sg",
			b."Cod_Cliente_Sg",
			b."Cod_Tipo_Pessoa",
			substr(decode(b."Cod_Tipo_Documento",1,b."Num_Documento",null),0,11) cpf,
			decode(b."Cod_Tipo_Documento",2,b."Num_Documento",null) cnpj,
			decode(b."Cod_Tipo_Documento",3,b."Num_Documento",null) cei,
			a."Cod_TipoDoc_Sg",
			a."Cod_Documento_Si",
			a."Cod_Status_Doc_Sg"
		from	ti_documento_con@MSQLCARDIOHML a,
			ti_empresa_cliente@MSQLCARDIOHML b
		where	a."Num_AutoId_Cliente" = b."AutoId"
		and	((a."Cod_Status_Reg" = 'NP')
		or	(a."Cod_Status_Reg" <> 'NP' and a."Cod_Documento_Si" is null))
		union all
		select	a."AutoId",
			a."Dat_Emissao",
			a."Dat_Vencimento",
			a."Val_Documento",
			a."Dat_Prorrogacao",
			a."Dat_Cancelamento",
			a."Cod_Oper_Registro",
			a."Dsc_Observacao",
			'F' tabela_origem,
			b."Cod_Fornecedor_Si",
			b."Cod_Empresa_Sg",
			b."Cod_Fornecedor_Sg",
			b."Cod_Tipo_Pessoa",
			substr(decode(b."Cod_Tipo_Documento",1,b."Num_Documento",null),0,11) cpf,
			decode(b."Cod_Tipo_Documento",2,b."Num_Documento",null) cnpj,
			decode(b."Cod_Tipo_Documento",3,b."Num_Documento",null) cei,
			a."Cod_TipoDoc_Sg",
			a."Cod_Documento_Si",
			a."Cod_Status_Doc_Sg"
		from	ti_documento_con@MSQLCARDIOHML a,
			ti_emp_fornecedor@MSQLCARDIOHML b
		where	a."Num_AutoId_Fornec" = b."AutoId"
		and	((a."Cod_Status_Reg" = 'NP')
		or	(a."Cod_Status_Reg" <> 'NP' and a."Cod_Documento_Si" is null));

BEGIN
obter_param_usuario(1200, 57, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_data_contabil_w);
ie_origem_w := '0';

select	nvl(cd_estab_financeiro, cd_estabelecimento)
into	cd_estab_financeiro_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	nvl(max(ie_tipo_titulo_pad),'1'),
	max(cd_moeda_padrao),
	max(cd_tipo_taxa_juro),
	max(cd_tipo_taxa_multa),
	max(pr_juro_padrao),
	max(pr_multa_padrao)
into	ie_tipo_titulo_w,
	cd_moeda_w,
	cd_tipo_juros_w,
	cd_tipo_multa_w,
	tx_juros_w,
	tx_multa_w
from	PARAMETROS_CONTAS_PAGAR
where	cd_estabelecimento	= cd_estabelecimento_p;

if (cd_moeda_w is not null and cd_tipo_juros_w is not null and cd_tipo_multa_w is not null) then
	open c01;
	loop
	fetch c01 into
		nr_documento_sg_w,
		dt_emissao_w,
		dt_vencimento_w,
		vl_titulo_w,
		dt_prorrogacao_sg_w,
		dt_cancelamento_sg_w,
		cd_operacao_sg_w,
		ds_observacao_w,
		ie_tabela_origem_sg_w,
		cd_pessoa_si_w,
		cd_empresa_sg_w,
		cd_cliente_sg_w,
		cd_tipo_pessoa_sg_w,
		cd_cpf_w,
		cd_cnpj_w,
		cd_cei_w,
		cd_tipo_doc_sg_w,
		cd_documento_si_w,
		cd_status_doc_sg_w;
	exit when c01%notfound;
		begin
			-- Insere novos documentos no Tasy
			if ((cd_operacao_sg_w = 'I') or (cd_operacao_sg_w = 'A' and cd_documento_si_w is null)) then
				ie_existe_pessoa_w := 'N';
				if (cd_tipo_pessoa_sg_w = 'F') then
					if (cd_pessoa_si_w is not null) then
						-- Verifica se existe a pessoa cadastrada no Tasy
						select	nvl(max('S'),'N')
						into	ie_existe_pessoa_w
						from	pessoa_fisica
						where	cd_pessoa_fisica = cd_pessoa_si_w;
						if (ie_existe_pessoa_w = 'S') then
							cd_pessoa_fisica_w := cd_pessoa_si_w;
						else
							begin
								select	max(cd_pessoa_fisica)
								into	cd_pessoa_fisica_w
								from	pessoa_fisica
								where	upper(ltrim(rtrim(nr_cpf))) = upper(ltrim(rtrim(cd_cpf_w)))
								or	ltrim(rtrim(cd_sistema_ant)) = (to_char(cd_empresa_sg_w)||to_char(cd_cliente_sg_w));
								-- Atualiza o cadastro na integra��o com o c�digo correto
								if (cd_pessoa_fisica_w is not null) then
									ie_existe_pessoa_w := 'S';
									if (ie_tabela_origem_sg_w = 'C') then
										update	ti_empresa_cliente@MSQLCARDIOHML
										set	"Cod_Cliente_Si" = cd_pessoa_fisica_w,
											"Cod_Status_Reg" = 'P'
										where	"Cod_Empresa_Sg" = cd_empresa_sg_w
										and	"Cod_Cliente_Sg" = cd_cliente_sg_w;
									elsif (ie_tabela_origem_sg_w = 'F') then
										update	ti_emp_fornecedor@MSQLCARDIOHML
										set	"Cod_Fornecedor_Si" = cd_pessoa_fisica_w,
											"Cod_Status_Reg" = 'P'
										where	"Cod_Empresa_Sg" = cd_empresa_sg_w
										and	"Cod_Fornecedor_Sg" = cd_cliente_sg_w;
									end if;
								end if;
							exception when others then
								null;
							end;
						end if;
					else
						begin
							select	max(cd_pessoa_fisica)
							into	cd_pessoa_fisica_w
							from	pessoa_fisica
							where	upper(ltrim(rtrim(nr_cpf))) = upper(ltrim(rtrim(cd_cpf_w)))
							or	ltrim(rtrim(cd_sistema_ant)) = (to_char(cd_empresa_sg_w)||to_char(cd_cliente_sg_w));
							-- Atualiza o cadastro na integra��o com o c�digo correto
							if (cd_pessoa_fisica_w is not null) then
								ie_existe_pessoa_w := 'S';
								if (ie_tabela_origem_sg_w = 'C') then
									update	ti_empresa_cliente@MSQLCARDIOHML
									set	"Cod_Cliente_Si" = cd_pessoa_fisica_w,
										"Cod_Status_Reg" = 'P'
									where	"Cod_Empresa_Sg" = cd_empresa_sg_w
									and	"Cod_Cliente_Sg" = cd_cliente_sg_w;
								elsif (ie_tabela_origem_sg_w = 'F') then
									update	ti_emp_fornecedor@MSQLCARDIOHML
									set	"Cod_Fornecedor_Si" = cd_pessoa_fisica_w,
										"Cod_Status_Reg" = 'P'
									where	"Cod_Empresa_Sg" = cd_empresa_sg_w
									and	"Cod_Fornecedor_Sg" = cd_cliente_sg_w;
								end if;
							else
								ie_existe_pessoa_w := 'N';
							end if;
						exception when others then
							null;
						end;
					end if;
				elsif (cd_tipo_pessoa_sg_w = 'J') then
					if (cd_pessoa_si_w is not null) then
						-- Verifica se existe a pessoa cadastrada no Tasy
						select	nvl(max('S'),'N')
						into	ie_existe_pessoa_w
						from	pessoa_juridica
						where	cd_cgc = cd_pessoa_si_w;
						if (ie_existe_pessoa_w = 'S') then
							cd_cgc_w := cd_pessoa_si_w;
						else
							begin
								select	max(cd_cgc)
								into	cd_cgc_w
								from	pessoa_juridica
								where	upper(ltrim(rtrim(cd_cgc))) = upper(ltrim(rtrim(nvl(cd_cnpj_w,cd_cei_w))))
								or	ltrim(rtrim(cd_sistema_ant)) = (to_char(cd_empresa_sg_w)||to_char(cd_cliente_sg_w));
								-- Atualiza o cadastro na integra��o com o c�digo correto
								if (cd_cgc_w is not null) then
									ie_existe_pessoa_w := 'S';
									if (ie_tabela_origem_sg_w = 'C') then
										update	ti_empresa_cliente@MSQLCARDIOHML
										set	"Cod_Cliente_Si" = cd_cgc_w,
											"Cod_Status_Reg" = 'P'
										where	"Cod_Empresa_Sg" = cd_empresa_sg_w
										and	"Cod_Cliente_Sg" = cd_cliente_sg_w;
									elsif (ie_tabela_origem_sg_w = 'F') then
										update	ti_emp_fornecedor@MSQLCARDIOHML
										set	"Cod_Fornecedor_Si" = cd_cgc_w,
											"Cod_Status_Reg" = 'P'
										where	"Cod_Empresa_Sg" = cd_empresa_sg_w
										and	"Cod_Fornecedor_Sg" = cd_cliente_sg_w;
									end if;
								end if;
							exception when others then
								null;
							end;
						end if;
					else
						begin
							select	max(cd_cgc)
							into	cd_cgc_w
							from	pessoa_juridica
							where	upper(ltrim(rtrim(cd_cgc))) = upper(ltrim(rtrim(nvl(cd_cnpj_w,cd_cei_w))))
							or	ltrim(rtrim(cd_sistema_ant)) = (to_char(cd_empresa_sg_w)||to_char(cd_cliente_sg_w));
							-- Atualiza o cadastro na integra��o com o c�digo correto
							if (cd_cgc_w is not null) then
								ie_existe_pessoa_w := 'S';
								if (ie_tabela_origem_sg_w = 'C') then
									update	ti_empresa_cliente@MSQLCARDIOHML
									set	"Cod_Cliente_Si" = cd_cgc_w,
										"Cod_Status_Reg" = 'P'
									where	"Cod_Empresa_Sg" = cd_empresa_sg_w
									and	"Cod_Cliente_Sg" = cd_cliente_sg_w;
								elsif (ie_tabela_origem_sg_w = 'F') then
									update	ti_emp_fornecedor@MSQLCARDIOHML
									set	"Cod_Fornecedor_Si" = cd_cgc_w,
										"Cod_Status_Reg" = 'P'
									where	"Cod_Empresa_Sg" = cd_empresa_sg_w
									and	"Cod_Fornecedor_Sg" = cd_cliente_sg_w;
								end if;
							else
								ie_existe_pessoa_w := 'N';
							end if;
						exception when others then
							null;
						end;
					end if;
				end if;
				-- Se existir a pessoa cadastrada no Tasy, faz a inser��o do t�tulo
				if (ie_existe_pessoa_w = 'S' and (cd_pessoa_fisica_w is not null or cd_cgc_w is not null)) then
					begin
						dt_contabil_w			:= trunc(last_day(dt_vencimento_w),'dd');
						dt_contabil_venc_w		:= trunc(dt_vencimento_w,'dd');
						dt_contabil_mes_ant_w		:= trunc(last_day(add_months(dt_vencimento_w,-1)),'dd');
						
						select	titulo_pagar_seq.nextval
						into	nr_titulo_w
						from	dual;
						
						insert into titulo_pagar(nr_titulo,
									cd_estabelecimento,
									dt_atualizacao,
									nm_usuario,
									dt_contabil,
									dt_emissao,
									dt_vencimento_original,
									dt_vencimento_atual,
									vl_titulo,
									vl_saldo_titulo,
									vl_saldo_juros,
									vl_saldo_multa,
									cd_moeda,
									tx_juros,
									tx_multa,
									cd_tipo_taxa_juro,
									cd_tipo_taxa_multa,
									tx_desc_antecipacao,
									ie_situacao,
									ie_origem_titulo,
									ie_tipo_titulo,
									cd_pessoa_fisica,
									cd_cgc,
									cd_estab_financeiro,
									nr_seq_trans_fin_contab,
									nm_usuario_orig,
									dt_inclusao,
									nr_seq_trans_fin_baixa,
									ds_observacao_titulo,
									nr_seq_classe,
									ie_integra_unimed)
						values(	nr_titulo_w,
							cd_estabelecimento_p,
							sysdate,
							nm_usuario_p,
							decode(ie_data_contabil_w,
								'S',dt_contabil_venc_w,
								'A',dt_contabil_mes_ant_w,dt_contabil_w),
							sysdate,
							dt_vencimento_w,
							dt_vencimento_w,
							vl_titulo_w,
							vl_titulo_w,
							0,
							0,
							cd_moeda_w,
							tx_juros_w,
							tx_multa_w,
							cd_tipo_juros_w,
							cd_tipo_multa_w,
							null,
							'A',
							ie_origem_w,
							ie_tipo_titulo_w,
							cd_pessoa_fisica_w,
							cd_cgc_w,
							cd_estab_financeiro_w,
							null,
							nm_usuario_p,
							sysdate,
							null,
							ds_observacao_w,
							null,
							'N');
						
						ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_w, nm_usuario_p);
						
						gerar_tit_pagar_classif(nr_titulo_w,nm_usuario_p);
						
						update	ti_documento_con@MSQLCARDIOHML
						set	"Cod_Documento_Si" = nr_titulo_w,
							"Cod_Status_Reg" = 'P'
						where	"AutoId" = nr_documento_sg_w;
						
						cd_documento_si_w := to_char(nr_titulo_w);
						
						update	titulo_pagar
						set	ie_integra_unimed = 'S'
						where	nr_titulo = nr_titulo_w;
						
					exception when others then
						ds_erro_w := substr(sqlerrm(sqlcode),0,499);
						
						update	ti_documento_con@MSQLCARDIOHML
						set	"Dsc_Erro_Registro" = ds_erro_w,
							"Cod_Status_Reg" = 'ER'
						where	"AutoId" = nr_documento_sg_w;
					end;
				else
					begin
						update	ti_documento_con@MSQLCARDIOHML
						set	"Dsc_Erro_Registro" = 'Cadastro da pessoa do documento n�o encontrado.',
							"Cod_Status_Reg" = 'ER'
						where	"AutoId" = nr_documento_sg_w;
					end;
				end if;
			end if;
			-- Altera documentos j� inseridos no Tasy
			if (cd_operacao_sg_w = 'A' and cd_documento_si_w is not null) then
				begin
					select	dt_emissao,
						dt_vencimento_original,
						dt_vencimento_atual,
						nr_titulo
					into	dt_emissao_tasy_w,
						dt_vencimento_tasy_w,
						dt_prorrogacao_tasy_w,
						nr_titulo_w
					from	titulo_pagar
					where	nr_titulo = cd_documento_si_w;
					
					update	titulo_pagar
					set	ie_integra_unimed = 'N'
					where	nr_titulo = nr_titulo_w;
					
					-- Altera a data de emiss�o do t�tulo
					if (trunc(dt_emissao_w,'dd') <> trunc(dt_emissao_tasy_w,'dd')) then
						begin
							update	titulo_pagar
							set	dt_emissao = dt_emissao_w,
								dt_atualizacao = sysdate,
								nm_usuario = nm_usuario_p
							where	nr_titulo = nr_titulo_w;
							
							update	ti_documento_con@MSQLCARDIOHML
							set	"Cod_Documento_Si" = nr_titulo_w,
								"Cod_Status_Reg" = 'P'
							where	"AutoId" = nr_documento_sg_w;
						exception when others then
							update	ti_documento_con@MSQLCARDIOHML
							set	"Dsc_Erro_Registro" = 'Data de emiss�o n�o atualizada',
								"Cod_Status_Reg" = 'ER'
							where	"AutoId" = nr_documento_sg_w;
						end;
					end if;
					-- Cancela o t�tulo no Tasy
					if (cd_status_doc_sg_w = 'C' or dt_cancelamento_sg_w is not null) then
						begin
							cancelar_titulo_pagar(nr_titulo_w,
										nm_usuario_p,
										nvl(dt_cancelamento_sg_w,sysdate));
							
							update	ti_documento_con@MSQLCARDIOHML
							set	"Cod_Documento_Si" = nr_titulo_w,
								"Cod_Status_Reg" = 'P'
							where	"AutoId" = nr_documento_sg_w;
						exception when others then
							ds_erro_w := substr(sqlerrm(sqlcode),0,499);
							
							update	ti_documento_con@MSQLCARDIOHML
							set	"Dsc_Erro_Registro" = ds_erro_w,
								"Cod_Status_Reg" = 'ER'
							where	"AutoId" = nr_documento_sg_w;
						end;
					end if;
					-- Prorroga o vencimento do t�tulo no Tasy
					if ((trunc(dt_vencimento_w,'dd') <> trunc(dt_vencimento_tasy_w,'dd'))
						or (trunc(dt_prorrogacao_sg_w,'dd') <> trunc(dt_prorrogacao_tasy_w,'dd'))) then
						begin
							if (dt_prorrogacao_sg_w is not null and trunc(dt_prorrogacao_sg_w,'dd') <> trunc(dt_vencimento_w,'dd')) then
								alterar_venc_titulo(nr_titulo_w,
										dt_prorrogacao_sg_w,
										'CP',
										null,
										null,
										null,
										cd_estabelecimento_p,
										null,
										null,
										nm_usuario_p,
										'Prorroga��o feita pelo SG');
							else
								alterar_venc_titulo(nr_titulo_w,
										dt_vencimento_w,
										'CP',
										null,
										null,
										null,
										cd_estabelecimento_p,
										null,
										null,
										nm_usuario_p,
										'Prorroga��o feita pelo SG');
							end if;
							
							update	ti_documento_con@MSQLCARDIOHML
							set	"Cod_Documento_Si" = nr_titulo_w,
								"Cod_Status_Reg" = 'P'
							where	"AutoId" = nr_documento_sg_w;
						exception when others then
							ds_erro_w := substr(sqlerrm(sqlcode),0,499);
							
							update	ti_documento_con@MSQLCARDIOHML
							set	"Dsc_Erro_Registro" = ds_erro_w,
								"Cod_Status_Reg" = 'ER'
							where	"AutoId" = nr_documento_sg_w;
						end;
					end if;
					
					update	titulo_pagar
					set	ie_integra_unimed = 'S'
					where	nr_titulo = nr_titulo_w;
					
				exception when others then
					update	ti_documento_con@MSQLCARDIOHML
					set	"Dsc_Erro_Registro" = 'Documento n�o importado para o Tasy',
						"Cod_Status_Reg" = 'ER',
						"Cod_Documento_Si" = null
					where	"AutoId" = nr_documento_sg_w;
				end;
			end if;
			-- Cancela documentos j� inseridos no Tasy
			if (cd_operacao_sg_w = 'E' and cd_documento_si_w is not null) then
				begin
					select	nr_titulo
					into	nr_titulo_w
					from	titulo_pagar
					where	nr_titulo = cd_documento_si_w;
					
					update	titulo_pagar
					set	ie_integra_unimed = 'N'
					where	nr_titulo = nr_titulo_w;
					
					begin
						cancelar_titulo_pagar(nr_titulo_w,
									nm_usuario_p,
									nvl(dt_cancelamento_sg_w,sysdate));
						
						update	ti_documento_con@MSQLCARDIOHML
						set	"Cod_Documento_Si" = nr_titulo_w,
							"Cod_Status_Reg" = 'P'
						where	"AutoId" = nr_documento_sg_w;
					exception when others then
						ds_erro_w := substr(sqlerrm(sqlcode),0,499);
						
						update	ti_documento_con@MSQLCARDIOHML
						set	"Dsc_Erro_Registro" = ds_erro_w,
							"Cod_Status_Reg" = 'ER'
						where	"AutoId" = nr_documento_sg_w;
					end;
					
					update	titulo_pagar
					set	ie_integra_unimed = 'S'
					where	nr_titulo = nr_titulo_w;
					
				exception when others then
					update	ti_documento_con@MSQLCARDIOHML
					set	"Dsc_Erro_Registro" = 'Documento n�o importado para o Tasy',
						"Cod_Status_Reg" = 'ER',
						"Cod_Documento_Si" = null
					where	"AutoId" = nr_documento_sg_w;
				end;
			end if;
			
			if (nr_titulo_w is not null) then
				atualizar_saldo_tit_pagar(nr_titulo_w,nm_usuario_p);
			end if;
			
			commit;
			
			cd_pessoa_fisica_w := null;
			cd_cgc_w := null;
			ie_existe_pessoa_w := 'N';
			nr_titulo_w := null;
		end;
	end loop;
	close c01;
end if;

END UBELEM_IMPORTA_TIT_PAGAR;
/
