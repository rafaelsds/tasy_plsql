create or replace procedure GQA_GERAR_REAVALIACAO (nr_seq_pendencia_p     IN gqa_pendencia.nr_sequencia%type,
                                                   nr_seq_protocolo_pac_p IN gqa_protocolo_pac.nr_sequencia%type,
                                                   nr_seq_regra_p         IN gqa_pendencia_regra.nr_sequencia%type,
                                                   nr_seq_etapa_p         IN gqa_protocolo_etapa_pac.nr_sequencia%type DEFAULT NULL) is

nm_usuario_w              usuario.nm_usuario%type;
nr_seq_acao_w             gqa_acao.nr_sequencia%type;
nr_seq_w                  gqa_protocolo_pac.nr_sequencia%type;
ie_finalizar_w            gqa_acao.ie_finaliza_protocolo%type;
ie_duplicado_w            gqa_protocolo_etapa_pac.ie_duplicado%type;
dt_prevista_fim_w         gqa_protocolo_etapa_pac.dt_prevista_fim%type;
dt_prevista_inicio_w      gqa_protocolo_etapa_pac.dt_prevista_inicio%type;

cursor c01 is
 SELECT *
   FROM gqa_pendencia_regra a
  WHERE a.nr_seq_pendencia = nr_seq_pendencia_p
    AND a.ie_situacao = 'A'
    AND a.ie_evento = '9'
  START WITH a.nr_sequencia = nr_seq_regra_p
CONNECT BY NOCYCLE PRIOR a.nr_sequencia = a.nr_seq_regra_sup
  ORDER BY a.nr_seq_regra_sup ASC NULLS FIRST;

begin
  select obter_usuario_ativo into nm_usuario_w from dual;

  for regra in c01 loop

    begin
      select max(nr_sequencia) into nr_seq_acao_w from gqa_acao where nr_seq_pend_regra = regra.nr_sequencia;
    exception when no_data_found then
      nr_seq_acao_w := null;
    end;
    
    if(nr_seq_acao_w is not null) then

      select gqa_protocolo_etapa_pac_seq.nextval into nr_seq_w from dual;

      select nvl(ie_finaliza_protocolo, 'N') 
      into ie_finalizar_w 
      from gqa_acao 
      where nr_sequencia = nr_seq_acao_w;
      
      if regra.nr_sequencia = nr_seq_regra_p then         
        ie_duplicado_w := 'S';
      else
        ie_duplicado_w := 'N';
      end if;
      
      dt_prevista_inicio_w := sysdate;
      
      if regra.qt_tempo_execucao is not null then
        dt_prevista_fim_w := dt_prevista_inicio_w + (regra.qt_tempo_execucao / (24 * 60));
      else
        dt_prevista_fim_w := dt_prevista_inicio_w;
      end if;
    
      insert into gqa_protocolo_etapa_pac (
        ie_finaliza_protocolo,
        ds_justificativa_cancelar,
        ds_nome_etapa,
        dt_atualizacao,
        dt_atualizacao_nrec,
        dt_cancelar_usuario,
        dt_fim,
        dt_inicio,
        dt_prevista_fim,
        dt_prevista_inicio,
        nm_usuario,
        nm_usuario_cancelar,
        nm_usuario_executor,
        nm_usuario_nrec,
        nr_seq_acao,
        nr_seq_etapa,
        nr_seq_etapa_sup,
        nr_seq_etapa_prot_sup,
        nr_seq_motivo_cancelar,
        nr_seq_prot_pac,
        nr_sequencia,
        qt_resultado,
        qt_ponto_min_etapa,
        qt_ponto_max_etapa,
        qt_tempo_execucao,
        qt_tempo_exec_resposta,
        qt_tempo_previsto,
        qt_tempo_prev_resposta,
        ds_tag,
        ds_cor_html,
        ie_permite_duplicar,
        ds_pergunta_duplicar,
        nr_seq_etapa_dup_ant,
        ie_duplicado
      ) values (
        ie_finalizar_w,
        null,
        regra.ds_regra,
        sysdate,
        sysdate,
        null,
        null,
        null,
        dt_prevista_fim_w,
        dt_prevista_inicio_w,
        nm_usuario_w,
        null,
        null,
        nm_usuario_w,
        nr_seq_acao_w,
        regra.nr_sequencia,
        regra.nr_seq_regra_sup,
        null,
        null,
        nr_seq_protocolo_pac_p,
        nr_seq_w,
        null,
        regra.qt_ponto_min_etapa,
        regra.qt_ponto_max_etapa,
        null,
        null,
        regra.qt_tempo_execucao,
        regra.qt_tempo_resposta,
        regra.ds_tag,
        regra.ds_cor_html,
        regra.ie_permite_duplicar,
        regra.ds_pergunta_duplicar,
        nr_seq_etapa_p,
        ie_duplicado_w
      );
    end if;
  end loop;
  
  UPDATE gqa_protocolo_etapa_pac
     SET dt_cancelar_usuario       = SYSDATE
        ,ds_justificativa_cancelar = 'REAVALIACAO'
   WHERE nr_sequencia IN (SELECT e.nr_sequencia
                            FROM gqa_protocolo_etapa_pac e
                                ,(SELECT *
                                    FROM gqa_pendencia_regra a
                                   WHERE a.nr_seq_pendencia = nr_seq_pendencia_p
                                     AND a.ie_situacao = 'A'
                                     AND a.ie_evento = '9'
                                   START WITH a.nr_sequencia = nr_seq_regra_p
                                  CONNECT BY nocycle
                                   PRIOR a.nr_sequencia = a.nr_seq_regra_sup) p
                           WHERE e.nr_seq_etapa = p.nr_sequencia
                             AND e.nr_seq_prot_pac = nr_seq_protocolo_pac_p
                             AND e.dt_cancelar_usuario IS NULL
                             AND (e.nr_seq_etapa_dup_ant IS NULL OR
                                 e.nr_seq_etapa_dup_ant <> nr_seq_etapa_p));

update gqa_protocolo_etapa_pac a set nr_seq_etapa_prot_sup = (select max(b.nr_sequencia) from gqa_protocolo_etapa_pac b where b.nr_seq_prot_pac = nr_seq_protocolo_pac_p and b.nr_seq_etapa = a.nr_seq_etapa_sup) 
where a.nr_seq_prot_pac = nr_seq_protocolo_pac_p;

end GQA_GERAR_REAVALIACAO;
/
