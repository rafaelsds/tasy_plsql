create or replace procedure GRAVA_AUDITORIA_SITUACAO
(
	nr_prescricao_p number,
	nr_seq_prescricao_p number,
	nm_usuario_p varchar2,
	nr_seq_situacao_p varchar2	
)   is
	
nr_sequencia_w number;	

begin

select SITUACAO_PROCEDIMENTO_LOG_seq.nextval 
into nr_sequencia_w
from dual;
insert into SITUACAO_PROCEDIMENTO_LOG
(
	NR_SEQUENCIA,
	DT_ATUALIZACAO,
	DT_ATUALIZACAO_NREC,
	NM_USUARIO,
	NM_USUARIO_NREC,
	NR_PRESCRICAO,
	NR_SEQ_PRESCRICAO,
	NR_SEQ_SITUACAO
)
values 
(
	SITUACAO_PROCEDIMENTO_LOG_seq.nextval,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	nr_prescricao_p,
	nr_seq_prescricao_p,
	nr_seq_situacao_p
);	
commit;

end GRAVA_AUDITORIA_SITUACAO;
/
