create or replace
procedure confirmar_diag_pend(	nr_sequencia_p		varchar2,	
				nr_atendimento_p		number,
				cd_doenca_p		varchar2,
				cd_medico_p		varchar2,
				nm_usuario_p		varchar2,
				nr_seq_doen_prev_p			varchar2,
				ie_tipo_diagnostico_p	number,
				ie_classificacao_doenca_p	varchar2) is
				
				
cursor c01 is
	select regexp_substr(nr_sequencia_p,'[^,]+', 1, level) as seq,
	 regexp_substr(cd_doenca_p,'[^,]+', 1, level) as doenca,
	regexp_substr(nr_seq_doen_prev_p,'[^,]+', 1, level) as doenca_prev
	from dual 
	connect BY regexp_substr(nr_sequencia_p, '[^,]+', 1, level) is not null;
	
c01_w	c01%rowtype;	
	
begin

open c01;
loop
fetch c01 into 
	c01_w;
exit when c01%notfound;
begin
	if	(nr_atendimento_p is not null) and
	(c01_w.seq is not null) then
	begin
	  if(c01_w.doenca is not null or c01_w.doenca != '') then
		gerar_diag_pend_atend(nr_atendimento_p,c01_w.doenca,cd_medico_p,nm_usuario_p,ie_tipo_diagnostico_p,ie_classificacao_doenca_p,c01_w.doenca_prev);
	  end if;
	  atualizar_data_geracao(c01_w.seq,nm_usuario_p);
	end;
end if;
end;
end loop;
close c01;


commit;

end confirmar_diag_pend;
/
