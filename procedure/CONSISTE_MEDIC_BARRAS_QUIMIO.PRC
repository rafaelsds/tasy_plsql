create or replace
procedure consiste_medic_barras_quimio(
		ie_lancar_barras_p			varchar2,
		cd_mat_lista_p			varchar2,
		cd_mat_barra_p			varchar2,
		nr_seq_ordem_p			number,
		qt_dose_p				number,
		qt_dose_real_p			number,
		qt_dose_unid_p			number,
		qt_dose_medic_ml_p			number,
		qt_dose_ml_p			number,
		nr_seq_item_prescr_p		number,
		cd_unidade_medida_p			varchar2,
		cd_unidade_medida_real_p 		varchar2,
		nr_seq_lote_fornec_p		number,
		ie_origem_inf_p			varchar2,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		cd_material_p		out	number,
		ie_limpar_p		out	varchar2,
		ds_erro_p			out	varchar2) is 

ie_consiste_estoque_barras_w	varchar2(1);
ie_consiste_pordose_w	varchar2(1);
nr_seq_lote_w		number(10);
ds_material_w		varchar2(255);
nr_seq_cabine_w		number(10);
qt_estoque_w		number(15,4);
ds_lixo_w			varchar2(255);

begin

ie_limpar_p	:= 'N';

converte_codigo_barras(
	cd_mat_barra_p,
	cd_estabelecimento_p,
	null,
	null,
	cd_material_p,
	ds_lixo_w,
	nr_seq_lote_w,
	ds_lixo_w,
	ds_lixo_w,
	ds_lixo_w,
	ds_lixo_w,
	ds_lixo_w,
	ds_lixo_w,
	ds_lixo_w);

if	(nvl(cd_material_p, 0) = 0) then
	begin
	ie_limpar_p	:= 'S';
	ds_erro_p		:= substr(obter_texto_tasy(64331, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end;

elsif	(nvl(nr_seq_lote_w, 0) = 0) then
	begin
	ie_limpar_p	:= 'S';
	ds_erro_p		:= substr(obter_texto_tasy(64332, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end;

elsif	(instr(cd_mat_lista_p, cd_material_p) < 1) then
	begin
	
	grava_log_erro_quimio(cd_material_p, nr_seq_lote_w, nr_seq_ordem_p, nm_usuario_p);
	
	ie_limpar_p	:= 'S';
	ds_material_w	:= substr(obter_desc_material(cd_material_p), 1,255);
	ds_erro_p		:= substr(obter_texto_dic_objeto(64354, wheb_usuario_pck.get_nr_seq_idioma, 'CD_MATERIAL=' || cd_material_p || ';DS_MATERIAL=' || ds_material_w),1,255);
	end;

elsif	(ie_lancar_barras_p = 'N') and
	(qt_dose_unid_p = qt_dose_p) and
	(qt_dose_medic_ml_p = qt_dose_ml_p) then
	begin
	ie_limpar_p	:= 'S';
	ds_erro_p		:= substr(obter_texto_tasy(64451, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end;

else
	begin
	select	b.nr_seq_cabine
	into	nr_seq_cabine_w
	from	far_etapa_producao b,
		can_ordem_prod a
	where	b.nr_sequencia	= a.nr_seq_etapa_prod
	and	a.nr_sequencia	= nr_seq_ordem_p;

	if	(ie_lancar_barras_p = 'S') then
		begin
		obter_consistencias_quimio(
			nr_seq_cabine_w,
			cd_material_p,
			nr_seq_lote_w,
			ds_erro_p);
			
		if	(ds_erro_p is null)	 then
			inserir_mat_med_ordem(
				nr_seq_ordem_p,
				cd_material_p,
				qt_dose_p,
				qt_dose_real_p,
				nr_seq_item_prescr_p,
				cd_unidade_medida_p,
				cd_unidade_medida_real_p,
				nr_seq_lote_fornec_p,
				ie_origem_inf_p,
				nm_usuario_p);
		end if;
		
		end;
	else
		begin

		select	nvl(max(qt_estoque),0)
		into	qt_estoque_w
		from	far_estoque_cabine
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	nr_seq_cabine	= nr_seq_cabine_w
		and	cd_material	= cd_material_p
		and	nr_seq_lote_fornec	= nr_seq_lote_w;
		
		if	((qt_estoque_w * obter_conversao_unid_med(cd_material_p, cd_unidade_medida_p)) < qt_dose_p) then
			begin
			/* Quimioterapia - Par�metro [187] - Ao bipar medicamentos na manipula��o por barras, consistir estoque da cabine */
			obter_param_usuario(3130, 187, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_estoque_barras_w);

			if	(ie_consiste_estoque_barras_w = 'S') then
				begin
				ie_limpar_p	:= 'S';
				ds_erro_p		:= substr(obter_texto_dic_objeto(64453, wheb_usuario_pck.get_nr_seq_idioma, 'QT_DOSE_UNID=' || qt_dose_unid_p),1,255);
				end;
			end if;
			end;
		end if;

		if	(ds_erro_p is null) and
			(qt_dose_unid_p < qt_dose_p) then
			begin
			/* Quimioterapia - Par�metro [143] - Na op��o Medicamento\Diluente Barras, ao bipar consistir dose do medicamento */
			obter_param_usuario(3130, 143, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_pordose_w);

			if	(ie_consiste_pordose_w = 'S') then
				ds_erro_p	:= substr(obter_texto_tasy(64457, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;

end consiste_medic_barras_quimio;
/