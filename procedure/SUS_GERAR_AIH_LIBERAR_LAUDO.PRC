create or replace
PROCEDURE sus_gerar_aih_liberar_laudo(	nr_seq_interno_p	NUMBER,
				nm_usuario_p		VARCHAR2) IS


nr_seq_interno_w			NUMBER(10);
nr_atendimento_w			NUMBER(10);
cd_orgao_emissor_w		VARCHAR2(10);
cd_medico_resp_w			VARCHAR2(10);
cd_medico_resp_ww		VARCHAR2(10) := '';
dt_emissao_w			DATE;
dt_emissao_laudo_w			DATE;
dt_retorno_secr_w			DATE;
cd_motivo_alta_w			NUMBER(3);
cd_cid_principal_w			VARCHAR2(10);
ie_carater_inter_sus_w		VARCHAR2(2);
nr_sequencia_w			NUMBER(10);
ie_origem_proced_w		NUMBER(10);
cd_procedimento_solic_w		NUMBER(15);
ds_retorno_w			VARCHAR(80);
cd_estabelecimento_w		NUMBER(10);
nr_interno_conta_w			NUMBER(15);
cd_medico_solic_w			VARCHAR2(15);
cd_modalidade_w			NUMBER(3);
cd_especialidade_aih_w		NUMBER(2);
cd_especialidade_aih_ww		NUMBER(2);
ie_mudanca_procedimento_w		VARCHAR(1);
ie_medico_resp_w			VARCHAR2(5);
ie_vincular_conta_w		VARCHAR2(10);
cd_cid_secundario_laudo_w		VARCHAR2(10);
cd_cid_causa_assoc_laudo_w	VARCHAR2(10);
cd_pessoa_fisica_w		VARCHAR2(10);
ie_modalidade_hosp_w		VARCHAR2(15) := 'N';
ie_motivo_alta_atend_w		VARCHAR2(15) := 'N';
cd_estab_usuario_w		NUMBER(5) := 0;
ie_lanca_proc_aih_w		VARCHAR2(15) := 'N';
ie_cria_nova_conta_w		VARCHAR2(15) := 'N';
ds_erro_w			VARCHAR2(255);
nr_aih_w				sus_aih_unif.nr_aih%TYPE;
ds_macros_w			varchar2(1000);
cd_proc_solic_aux_w		sus_aih_unif.cd_procedimento_solic%TYPE;
--nr_atend_original_w		atendimento_paciente.nr_atend_original%type;
ie_tipo_laudo_sus_w		sus_laudo_paciente.ie_tipo_laudo_sus%type;
cd_proc_original_w		sus_aih_unif.cd_procedimento_solic%type;
ie_existe_registro_w            NUMBER(5) := 0;

CURSOR c_contas_atend(	nr_atendimento_pc	atendimento_paciente.nr_atendimento%type,
			dt_emissao_laudo_pc	sus_laudo_paciente.dt_emissao%type) IS
	SELECT	c.nr_interno_conta
	FROM	atendimento_paciente	a,
		conta_paciente	c
	WHERE	a.nr_atendimento	=	c.nr_atendimento
	AND	a.nr_atendimento	=	nr_atendimento_pc
	AND	c.ie_status_acerto	=	1
	AND	dt_emissao_laudo_pc	between	c.dt_periodo_inicial and c.dt_periodo_final;

BEGIN

BEGIN
cd_estab_usuario_w := NVL(wheb_usuario_pck.get_cd_estabelecimento,0);
EXCEPTION
WHEN OTHERS THEN
	cd_estab_usuario_w := 0;
END;


ie_vincular_conta_w	:= NVL(Obter_Valor_Param_Usuario(1006,9,Obter_Perfil_Ativo,nm_usuario_p,cd_estab_usuario_w),'N');
ie_medico_resp_w	:= Obter_Valor_Param_Usuario(1123,10,Obter_Perfil_Ativo,Nm_Usuario_p,cd_estab_usuario_w);
ie_modalidade_hosp_w	:= NVL(Obter_Valor_Param_Usuario(1123,72,Obter_Perfil_Ativo,Nm_Usuario_p,cd_estab_usuario_w),'N');
ie_motivo_alta_atend_w	:= NVL(Obter_Valor_Param_Usuario(1123,116,Obter_Perfil_Ativo,Nm_Usuario_p,cd_estab_usuario_w),'N');
ie_lanca_proc_aih_w	:= NVL(Obter_Valor_Param_Usuario(1123,164,Obter_Perfil_Ativo,Nm_Usuario_p,cd_estab_usuario_w),'N');
ie_cria_nova_conta_w	:= NVL(Obter_Valor_Param_Usuario(1123,246,Obter_Perfil_Ativo,Nm_Usuario_p,cd_estab_usuario_w),'N');

SELECT	p.nr_seq_interno,
	b.nr_atendimento,
	b.dt_entrada,
	b.ie_carater_inter_sus,
	p.dt_retorno_secr,
	cd_procedimento_solic,
	ie_origem_proced,
	p.nr_interno_conta,
	p.cd_medico_requisitante,
	p.cd_medico_responsavel,
	b.cd_estabelecimento,
	DECODE(p.ie_tipo_laudo_sus,1,'S','N'),
	p.cd_cid_principal,
	p.cd_cid_secundario,
	p.cd_cid_causa_assoc,
	p.dt_emissao,
	b.cd_pessoa_fisica,
	p.nr_aih,
	p.ie_tipo_laudo_sus
INTO	nr_seq_interno_w,
	nr_atendimento_w,
	dt_emissao_w,
	ie_carater_inter_sus_w,
	dt_retorno_secr_w,
	cd_procedimento_solic_w,
	ie_origem_proced_w,
	nr_interno_conta_w,
	cd_medico_solic_w,
	cd_medico_resp_w,
	cd_estabelecimento_w,
	ie_mudanca_procedimento_w,
	cd_cid_principal_w,
	cd_cid_secundario_laudo_w,
	cd_cid_causa_assoc_laudo_w,
	dt_emissao_laudo_w,
	cd_pessoa_fisica_w,
	nr_aih_w,
	ie_tipo_laudo_sus_w
FROM	atendimento_paciente	b,
	sus_laudo_paciente 	p
WHERE	b.nr_atendimento	=	p.nr_atendimento
AND	p.nr_seq_interno	=	nr_seq_interno_p;

IF	(sus_obter_tiporeg_proc(cd_procedimento_solic_w,ie_origem_proced_w,'C',13) = '3') THEN
	BEGIN	
	
	IF	(ie_cria_nova_conta_w = 'S' and ie_tipo_laudo_sus_w = 0) THEN
		
		dt_emissao_w := dt_emissao_laudo_w;
	
		FOR r_c_contas_atend in c_contas_atend(nr_atendimento_w, dt_emissao_laudo_w) LOOP
		
			UPDATE	conta_paciente
			SET	dt_periodo_final = (dt_emissao_laudo_w - 1/24/60)
			WHERE	nr_interno_conta = r_c_contas_atend.nr_interno_conta;
		
		END LOOP;
		
	END IF;

	IF (NVL(nr_aih_w,0) > 0 OR nr_interno_conta_w is not null) THEN
		BEGIN
		
		if (NVL(nr_aih_w,0) > 0) then
			begin
			
			UPDATE 	sus_aih_unif
			SET		nr_atendimento = nr_Atendimento_w,
					dt_atualizacao = SYSDATE,
					nm_usuario		= nm_usuario_p
			WHERE	nr_aih			= nr_aih_w
			AND		nr_interno_conta IS NULL;
			
			SELECT	NVL(MAX(nr_sequencia),0),
				nvl(max(cd_procedimento_solic),0)
			INTO	nr_sequencia_w,
				cd_proc_original_w
			FROM	sus_aih_unif
			WHERE	nr_aih = nr_aih_w
			AND		nr_Atendimento = nr_Atendimento_w;
			
			if	(ie_mudanca_procedimento_w = 'S') and
				(cd_procedimento_solic_w <> cd_proc_original_w) then
				
				update 	sus_aih_unif
				set	cd_procedimento_real = cd_procedimento_solic_w,
					ie_mudanca_proc = 'S'
				where	nr_aih	= nr_aih_w
				and	nr_Atendimento = nr_Atendimento_w;
				
			end if;
			
			if	(ie_lanca_proc_aih_w = 'S') THEN
				begin
				sus_atualiza_proced_aih(NVL(nr_aih_w, nr_atendimento_w),nr_sequencia_w,nm_usuario_p,ds_erro_w,NULL);
				end;
			end if;
			
			end;
		else 
			begin
			
			update 	sus_aih_unif
			set	nr_atendimento = nr_Atendimento_w,
				dt_atualizacao = SYSDATE,
				nm_usuario		= nm_usuario_p
			where	nr_interno_conta	= nr_interno_conta_w
			and	nr_interno_conta IS NULL;
			
			select	NVL(MAX(nr_sequencia),0),
				nvl(max(cd_procedimento_solic),0)
			into	nr_sequencia_w,
				cd_proc_original_w
			from	sus_aih_unif
			where	nr_interno_conta = nr_interno_conta_w
			and	nr_Atendimento = nr_Atendimento_w;
			
			if	(ie_mudanca_procedimento_w = 'S') and
				(cd_procedimento_solic_w <> cd_proc_original_w) then
				
				update 	sus_aih_unif
				set	cd_procedimento_real = cd_procedimento_solic_w,
					ie_mudanca_proc = 'S'
				where	nr_interno_conta = nr_interno_conta_w
				and	nr_Atendimento = nr_Atendimento_w;
				
			end if;
			
			if	(ie_lanca_proc_aih_w = 'S') THEN
				begin
				
				select	max(nr_aih)
				into	nr_aih_w
				from	sus_aih_unif
				where	nr_interno_conta = nr_interno_conta_w
				and	nr_atendimento = nr_Atendimento_w;
				
				sus_atualiza_proced_aih(nr_aih_w,nr_sequencia_w,nm_usuario_p,ds_erro_w,NULL);
				end;
			end if;
			
			end;
		end if;

		IF	(ie_vincular_conta_w	= 'S') AND
			(nr_interno_conta_w IS NULL) THEN
			BEGIN

			SELECT	MAX(nr_interno_conta)
			INTO	nr_interno_conta_w
			FROM	conta_paciente a
			WHERE	nr_atendimento	= nr_atendimento_w
			AND	ie_status_acerto = 1
			AND	ie_cancelamento IS NULL
			--and	dt_emissao_laudo_w between dt_periodo_inicial and dt_periodo_final
			AND	((ie_tipo_atend_conta IS NULL) OR (ie_tipo_atend_conta = 1))
			AND	NOT EXISTS	(	SELECT	1
							FROM	sus_aih_unif b
							WHERE	b.nr_atendimento	= nr_atendimento_w
							AND	b.nr_interno_conta	= a.nr_interno_conta);

			UPDATE 	sus_laudo_paciente
			SET 	nr_interno_conta = nr_interno_conta_w
			WHERE 	nr_seq_interno = nr_seq_interno_p
			AND	nr_interno_conta IS NULL;

			UPDATE 	sus_aih_unif
			SET 	nr_interno_conta = nr_interno_conta_w
			WHERE 	nr_aih = nr_aih_w
			AND	nr_sequencia =	nr_sequencia_w
			AND	nr_interno_conta IS NULL;

			END;
		ELSIF	(nr_interno_conta_w IS NOT NULL) THEN
			UPDATE 	sus_laudo_paciente
			SET 	nr_interno_conta = nr_interno_conta_w
				WHERE 	nr_seq_interno = nr_seq_interno_p
			AND	nr_interno_conta IS NULL;
		END IF;

		END;
	ELSE
		BEGIN
		Atualizar_Orgao_Emissor_Aih(nr_atendimento_w,cd_orgao_emissor_w,cd_medico_resp_w);

		SELECT	NVL(MAX(nr_sequencia)+1,0)
		INTO	nr_sequencia_w
		FROM 	sus_aih_unif
		WHERE 	nr_aih	=	nr_atendimento_w;

		IF	(ie_modalidade_hosp_w = 'S') AND
			(sus_obter_se_mod_hospitalar(cd_procedimento_solic_w,ie_origem_proced_w) = 1 )THEN
			cd_modalidade_w := 2;
		ELSE
			BEGIN
			SELECT	MAX(cd_modalidade)
			INTO	cd_modalidade_w
			FROM	sus_procedimento_modal
			WHERE	cd_procedimento		= cd_procedimento_solic_w
			AND	ie_origem_proced	= ie_origem_proced_w;
			END;
		END IF;

		SELECT	NVL(MIN(b.cd_espec_leito),1)
		INTO	cd_especialidade_aih_w
		FROM	sus_espec_leito         b,
			sus_procedimento_leito  a
		WHERE    a.nr_seq_espec_leito    = b.nr_sequencia
		AND      a.cd_procedimento       = cd_procedimento_solic_w
		AND      a.ie_origem_proced      = ie_origem_proced_w
		AND	EXISTS	(	SELECT	1
					FROM	sus_especialidade x
					WHERE	b.cd_espec_leito	= x.cd_especialidade);

		SELECT	NVL(sus_obter_especialidade_aih(cd_procedimento_solic_w, ie_origem_proced_w, cd_pessoa_fisica_w, cd_estabelecimento_w),'0')
		INTO	cd_especialidade_aih_ww
		FROM	dual;

		IF	(cd_especialidade_aih_ww <> '0') THEN
			BEGIN
			cd_especialidade_aih_w := cd_especialidade_aih_ww;
			END;
		END IF;

		cd_motivo_alta_w := NULL;

		IF	(ie_motivo_alta_atend_w = 'S') THEN
			BEGIN
			BEGIN
			SELECT	MAX(b.cd_motivo_alta_sus)
			INTO	cd_motivo_alta_w
			FROM	atendimento_paciente a,
				motivo_alta b
			WHERE	a.nr_atendimento = nr_Atendimento_w
			AND	a.cd_motivo_alta = b.cd_motivo_alta;
			EXCEPTION
				WHEN OTHERS THEN
				cd_motivo_alta_w	:= NULL;
				END;
			END;
		END IF;

		cd_medico_resp_ww	:= NVL(sus_obter_tipomedico_aih(nr_Atendimento_w,ie_medico_resp_w),'');

		IF	(NVL(cd_cid_principal_w,'X') = 'X') THEN
			Sus_Buscar_Dados_Aih(nr_atendimento_w, cd_motivo_alta_w, cd_cid_principal_w, cd_cid_secundario_laudo_w);
		END IF;

		select  count(*)
		into    ie_existe_registro_w
		from    sus_aih_unif 
		where   nr_atendimento = nr_atendimento_w
		and     cd_procedimento_solic = cd_procedimento_solic_w
		and     cd_medico_solic = cd_medico_solic_w
		and     trunc(dt_emissao) = trunc(dt_emissao_w)
		and     nm_usuario = nm_usuario_p
		and     trunc(dt_atualizacao) = trunc(sysdate);

		IF	(NVL(nr_atendimento_w,0) > 0) AND
			(NVL(cd_cid_principal_w,'X') <> 'X') AND
                        (NVL(ie_existe_registro_w,0) = 0) THEN
			BEGIN
			
			BEGIN
			INSERT INTO sus_aih_unif(nr_aih,
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_estabelecimento,
					ie_identificacao_aih,
					nr_proxima_aih,
					nr_anterior_aih,
					dt_emissao,
					ie_mudanca_proc,
					cd_procedimento_solic,
					ie_origem_proc_solic,
					cd_procedimento_real,
					ie_origem_proc_real,
					cd_medico_solic,
					cd_cid_principal,
					cd_cid_secundario,
					cd_cid_causa_compl,
					cd_cid_causa_morte,
					nr_interno_conta,
					nr_atendimento,
					cd_medico_responsavel,
					cd_modalidade,
					cd_motivo_cobranca,
					cd_especialidade_aih,
					ie_codigo_autorizacao,
					qt_nascido_vivo,
					qt_nascido_morto,
					qt_saida_alta,
					qt_saida_transferencia,
					qt_saida_obito,
					nr_gestante_prenatal,
					cd_orgao_emissor_aih,
					cd_carater_internacao,
					dt_inicial,
					dt_final,
					nr_proc_interno_solic,
					nr_proc_interno_real)
				VALUES (NVL(nr_aih_w, nr_atendimento_w),
					nr_sequencia_w,
					SYSDATE,
					nm_usuario_p,
					SYSDATE,
					nm_usuario_p,
					cd_estabelecimento_w,
					'01',
					NULL,
					NULL,
					dt_emissao_w,
					ie_mudanca_procedimento_w,
					DECODE(ie_mudanca_procedimento_w,'S',sus_obter_proc_orig_laudo(NULL,NULL,nr_atendimento_w,dt_emissao_laudo_w,1),cd_procedimento_solic_w),
					ie_origem_proced_w,
					cd_procedimento_solic_w,
					ie_origem_proced_w,
					NVL(cd_medico_solic_w,NVL(cd_medico_resp_ww,cd_medico_resp_w)),
					cd_cid_principal_w,
					cd_cid_secundario_laudo_w,
					cd_cid_causa_assoc_laudo_w,
					NULL,
					nr_interno_conta_w,
					nr_atendimento_w,
					NVL(cd_medico_resp_ww,cd_medico_resp_w),
					cd_modalidade_w,
						cd_motivo_alta_w,
					cd_especialidade_aih_w,
					NULL,
					0,
					0,
					0,
						0,
					0,
					NULL,
					cd_orgao_emissor_w,
					NVL(ie_carater_inter_sus_w,1),
					NULL,
					NULL,
					NULL,
					NULL);
			EXCEPTION
			WHEN OTHERS THEN
				BEGIN
				select DECODE(ie_mudanca_procedimento_w,'S',sus_obter_proc_orig_laudo(NULL,NULL,nr_atendimento_w,dt_emissao_laudo_w,1),cd_procedimento_solic_w)
				into cd_proc_solic_aux_w
				from dual;
				
				ds_macros_w := 'CD_CARATER_INTERNACAO_P=' || NVL(ie_carater_inter_sus_w,1) || 
						';CD_CID_PRINCIPAL_P=' || cd_cid_principal_w ||
						';CD_ESPECIALIDADE_AIH_P=' || cd_especialidade_aih_w ||
						';CD_ESTABELECIMENTO_P=' || cd_estabelecimento_w ||
						';CD_MEDICO_RESPONSAVEL_P=' || NVL(cd_medico_resp_ww,cd_medico_resp_w) ||
						';CD_MEDICO_SOLIC_P=' || NVL(cd_medico_solic_w,NVL(cd_medico_resp_ww,cd_medico_resp_w)) ||
						';CD_MODALIDADE_P=' || cd_modalidade_w ||
						';CD_ORGAO_EMISSOR_AIH_P=' || cd_orgao_emissor_w ||
						';CD_PROCEDIMENTO_REAL_P=' || cd_procedimento_solic_w ||
						';CD_PROCEDIMENTO_SOLIC_P=' || cd_proc_solic_aux_w ||
						';DT_EMISSAO_P=' || dt_emissao_w;
				wheb_mensagem_pck.exibir_mensagem_abort(1072648, ds_macros_w);
				END;
			END;		
			
			IF	(ie_lanca_proc_aih_w = 'S') THEN
				BEGIN
				sus_atualiza_proced_aih(NVL(nr_aih_w, nr_atendimento_w),nr_sequencia_w,nm_usuario_p,ds_erro_w,NULL);
				END;
			END IF;

			IF	(ie_vincular_conta_w	= 'S') AND
				(nr_interno_conta_w IS NULL) THEN
				BEGIN

				SELECT	MAX(nr_interno_conta)
				INTO	nr_interno_conta_w
				FROM	conta_paciente a
				WHERE	nr_atendimento	= nr_atendimento_w
				AND	ie_status_acerto = 1
				AND	ie_cancelamento IS NULL
				--and	dt_emissao_laudo_w between dt_periodo_inicial and dt_periodo_final
				AND	((ie_tipo_atend_conta IS NULL) OR (ie_tipo_atend_conta = 1))
				AND	NOT EXISTS	(	SELECT	1
								FROM	sus_aih_unif b
								WHERE	b.nr_atendimento	= nr_atendimento_w
								AND	b.nr_interno_conta	= a.nr_interno_conta);

				UPDATE 	sus_laudo_paciente
				SET 	nr_interno_conta = nr_interno_conta_w
				WHERE 	nr_seq_interno = nr_seq_interno_p
				AND	nr_interno_conta IS NULL;

				UPDATE 	sus_aih_unif
				SET 	nr_interno_conta = nr_interno_conta_w
				WHERE 	nr_aih = NVL(nr_aih_w, nr_atendimento_w)
				AND	nr_sequencia =	nr_sequencia_w
				AND	nr_interno_conta IS NULL;

				END;
			ELSIF	(nr_interno_conta_w IS NOT NULL) THEN
				UPDATE 	sus_laudo_paciente
				SET 	nr_interno_conta = nr_interno_conta_w
					WHERE 	nr_seq_interno = nr_seq_interno_p
				AND	nr_interno_conta IS NULL;
			END IF;

			END;
		END IF;

		END;
	END IF;

	END;
END IF;


END sus_gerar_aih_liberar_laudo;
/
