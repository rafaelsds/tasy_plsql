create or replace
procedure bsc_enviar_comunic_calc_geral(	nr_seq_regra_comunic_p	number,
					cd_periodo_p		number,
					cd_ano_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is 

					
					
nm_usuario_dest_w		varchar2(4000);				
nr_seq_regra_dest_w		number(10);
ds_titulo_w			varchar2(255)	:= ' ';
ds_titulo_ww			varchar2(255);
ds_comunicado_w			long;
ds_cabecalho_comunicado_w	varchar2(255);
ds_setor_adicional_w		varchar2(255);
ds_perfil_adicional_w		varchar2(255);
nm_usuario_w			varchar2(4000);
nm_usuario_ant_w		varchar2(4000);

					
					
cursor	c01 is
select	a.nr_sequencia,
	a.cd_setor_atendimento,
	a.cd_perfil
from	bsc_regra_com_calc_dest a
where	a.nr_seq_regra_comunic	= nr_seq_regra_comunic_p
order by	nvl(nm_usuario_dest, 'A'),
	nvl(cd_perfil, 0),
	nvl(cd_setor_atendimento, 0);


cursor	c02 is
select	a.nm_usuario_dest
from	bsc_regra_com_calc_dest a
where	a.nr_seq_regra_comunic	= nr_seq_regra_comunic_p
order by	nvl(nm_usuario_dest, 'A'),
	nvl(cd_perfil, 0),
	nvl(cd_setor_atendimento, 0);


begin

select	a.ds_comunicado,
	a.ds_titulo
into	ds_comunicado_w,
	ds_titulo_w
from	bsc_regra_comunic_calc a
where	a.nr_sequencia	= nr_seq_regra_comunic_p;


ds_comunicado_w		:= substr(ds_comunicado_w, 1, 2000);
--ds_titulo_ww		:= substr(ds_titulo_w || ' - Calculo dos indicadores do BSC - Per�odo '|| to_char(cd_periodo_p) || '/' ||to_char(cd_ano_p), 1, 255);
ds_titulo_ww		:= substr(ds_titulo_w || wheb_mensagem_pck.get_texto(299145, 'CD_PERIODO_P=' || to_char(cd_periodo_p) || ';CD_ANO_P=' || to_char(cd_ano_p)), 1, 255);


open c02;
loop
fetch c02 into	
	nm_usuario_w;
	
exit when c02%notfound;
	begin
	if ( nm_usuario_ant_w is null)then
		nm_usuario_ant_w := nm_usuario_w;	
	else
		nm_usuario_ant_w := nm_usuario_ant_w || ',' || nm_usuario_w;
	end if;	

end;
end loop;
close c02;

nm_usuario_dest_w := substr(nm_usuario_ant_w,1,4000);



open c01;
loop
fetch c01 into	
	nr_seq_regra_dest_w,
	ds_setor_adicional_w,
	ds_perfil_adicional_w;
exit when c01%notfound;
	begin
	gerar_comunic_padrao(	sysdate,
					ds_titulo_ww,
					ds_comunicado_w,
					nm_usuario_p,
					null,
					nm_usuario_dest_w,
					'N',
					null,
					ds_perfil_adicional_w,
					cd_estabelecimento_p,
					ds_setor_adicional_w,
					sysdate,
					null,
					null);
end;
end loop;
close c01;




commit;

end bsc_enviar_comunic_calc_geral;
/