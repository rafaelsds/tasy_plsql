/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Importar os dados em XML do resumo do arquivo de devolu��o SIB
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_imp_dev_resumo_sib_xml
			(	nr_seq_lote_sib_p		number,
				ie_tipo_info_p			varchar2,
				----------------------------------------------------------------
				nm_arquivo_p			varchar2,
				----------------------------------------------------------------
				qt_inclusao_total_p		number default null,
				qt_inclusao_processado_p	number default null,
				qt_inclusao_recusado_p		number default null,
				pr_inclusao_acerto_p		number default null,
				qt_alteracao_total_p		number default null,
				qt_alteracao_processado_p	number default null,
				qt_alteracao_recusado_p		number default null,
				pr_alteracao_acerto_p		number default null,
				qt_exclusao_total_p		number default null,
				qt_exclusao_processado_p	number default null,
				qt_exclusao_recusado_p		number default null,
				pr_exclusao_acerto_p		number default null,
				qt_reinclusao_total_p		number default null,
				qt_reinclusao_processado_p	number default null,
				qt_reinclusao_recusado_p	number default null,
				pr_reinclusao_acerto_p		number default null,
				qt_total_p			number default null,
				qt_processados_p		number default null,
				qt_recusados_p			number default null,
				pr_total_p			number default null,
				qt_retificacao_total_p		number default null,
				qt_retificacao_processado_p	number default null,
				qt_retificacao_recusado_p	number default null,
				pr_retificacao_acerto_p		number default null,
				qt_sem_movto_total_p		number default null,
				qt_sem_movto_processado_p	number default null,
				qt_sem_movto_recusado_p		number default null,
				pr_sem_movto_acerto_p		number default null,
				----------------------------------------------------------------
				cd_mensagem_p			varchar2,
				ds_mensagem_erro_p		varchar2,
				qt_ocorrencia_mensagem_p	number default null,
				----------------------------------------------------------------
				cd_estabelecimento_p		number,
				nm_usuario_p			Varchar2) is 
			
/*ie_tipo_info_p
C - Cabe�alho
P - Processdados
M - Mensagens*/

nr_seq_devolucao_resumo_w	number(10);
cd_cgc_operadora_w		varchar2(255);
nm_operadora_w			varchar2(255);
nr_operadora_w			number(10);
--------------------------------------------------------------------------------
qt_inclusao_total_w		number(10);
qt_inclusao_processado_w	number(10);
pr_inclusao_w			number(7,4);

qt_alteracao_total_w		number(10);
qt_alteracao_processado_w	number(10);
pr_alteracao_w			number(7,4);

qt_retificacao_total_w		number(10);
qt_retificacao_processado_w	number(10);
pr_retificacao_w		number(7,4);

qt_exclusao_total_w		number(10);
qt_exclusao_processado_w	number(10);
pr_exclusao_w			number(7,4);

qt_reinclusao_total_w		number(10);
qt_reinclusao_processado_w	number(10);
pr_reinclusao_w			number(7,4);

qt_total_w			number(10);
qt_processados_w		number(10);
pr_total_w			number(7,4);

qt_sem_movto_total_w		number(10);
qt_sem_movto_processado_w	number(10);
pr_sem_movto_w			number(7,4);

begin

if	(ie_tipo_info_p = 'C') then

	select	max(nr_sequencia)
	into	nr_seq_devolucao_resumo_w
	from	sib_devolucao_resumo
	where	nr_seq_lote	= nr_seq_lote_sib_p
	and	ie_tipo_item	= 'C'
	and	ie_xml		= 'S';
	
	if	(nr_seq_devolucao_resumo_w is null) then
	
		select	substr(pls_obter_dados_outorgante(cd_estabelecimento_p,'C'),1,255),
			substr(pls_obter_dados_outorgante(cd_estabelecimento_p,'N'),1,255)
		into	cd_cgc_operadora_w,
			nm_operadora_w
		from 	dual;
		
		begin
		nr_operadora_w		:= substr(nm_arquivo_p,1,7);
		exception
		when others then
				nr_operadora_w	:= null;
		end;
	
		insert into sib_devolucao_resumo
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_lote,ie_tipo_item,ie_xml,nr_operadora,cd_cgc_operadora,ds_razao_social,nm_arquivo)
		values	(	sib_devolucao_resumo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				nr_seq_lote_sib_p,'C','S',nr_operadora_w,cd_cgc_operadora_w,nm_operadora_w,nm_arquivo_p);
	end if;
elsif	(ie_tipo_info_p = 'P') then
	select	max(nr_sequencia)
	into	nr_seq_devolucao_resumo_w
	from	sib_devolucao_resumo
	where	nr_seq_lote	= nr_seq_lote_sib_p
	and	ie_tipo_item	= 'P'
	and	ie_xml		= 'S';
	
	if	(nr_seq_devolucao_resumo_w is null) then
		insert into sib_devolucao_resumo
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_lote,ie_tipo_item,ie_xml,qt_inclusao_total,qt_inclusao_processado,
				qt_inclusao_recusado,pr_inclusao_acerto,qt_alteracao_total,qt_alteracao_processado,qt_alteracao_recusado, 
				pr_alteracao_acerto,qt_exclusao_total,qt_exclusao_processado,qt_exclusao_recusado,pr_exclusao_acerto,  
				qt_reinclusao_total,qt_reinclusao_processado,qt_reinclusao_recusado,pr_reinclusao_acerto,qt_total,
				qt_processados,qt_recusados,pr_total,qt_retificacao_total,qt_retificacao_processado, 
				qt_retificacao_recusado,pr_retificacao_acerto,qt_sem_movto_total,qt_sem_movto_processado,qt_sem_movto_recusado, 
				pr_sem_movto_acerto)
		values	(	sib_devolucao_resumo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				nr_seq_lote_sib_p,'P','S',qt_inclusao_total_p,qt_inclusao_processado_p,
				qt_inclusao_recusado_p,pr_inclusao_acerto_p,qt_alteracao_total_p,qt_alteracao_processado_p,qt_alteracao_recusado_p, 
				pr_alteracao_acerto_p,qt_exclusao_total_p,qt_exclusao_processado_p,qt_exclusao_recusado_p,pr_exclusao_acerto_p,  
				qt_reinclusao_total_p,qt_reinclusao_processado_p,qt_reinclusao_recusado_p,pr_reinclusao_acerto_p,qt_total_p,
				qt_processados_p,qt_recusados_p,pr_total_p,qt_retificacao_total_p,qt_retificacao_processado_p, 
				qt_retificacao_recusado_p,pr_retificacao_acerto_p,qt_sem_movto_total_p,qt_sem_movto_processado_p,qt_sem_movto_recusado_p, 
				pr_sem_movto_acerto_p);
	else
		update	sib_devolucao_resumo
		set	qt_inclusao_total		= qt_inclusao_total + nvl(qt_inclusao_total_p,0),
			qt_inclusao_processado		= qt_inclusao_processado + nvl(qt_inclusao_processado_p,0),
			qt_inclusao_recusado		= qt_inclusao_recusado + nvl(qt_inclusao_recusado_p,0),
			qt_alteracao_total		= qt_alteracao_total + nvl(qt_alteracao_total_p,0),
			qt_alteracao_processado		= qt_alteracao_processado + nvl(qt_alteracao_processado_p,0),
			qt_alteracao_recusado		= qt_alteracao_recusado + nvl(qt_alteracao_recusado_p,0),
			qt_exclusao_total		= qt_exclusao_total + nvl(qt_exclusao_total_p,0),
			qt_exclusao_processado		= qt_exclusao_processado + nvl(qt_exclusao_processado_p,0),
			qt_exclusao_recusado		= qt_exclusao_recusado + nvl(qt_exclusao_recusado_p,0),
			qt_reinclusao_total		= qt_reinclusao_total + nvl(qt_reinclusao_total_p,0),
			qt_reinclusao_processado	= qt_reinclusao_processado + nvl(qt_reinclusao_processado_p,0),
			qt_reinclusao_recusado		= qt_reinclusao_recusado + nvl(qt_reinclusao_recusado_p,0),
			qt_total			= qt_total + nvl(qt_total_p,0),
			qt_processados			= qt_processados + nvl(qt_processados_p,0),
			qt_recusados			= qt_recusados + nvl(qt_recusados_p,0),
			qt_retificacao_total		= qt_retificacao_total + nvl(qt_retificacao_total_p,0),
			qt_retificacao_processado	= qt_retificacao_processado + nvl(qt_retificacao_processado_p,0) ,
			qt_retificacao_recusado		= qt_retificacao_recusado + nvl(qt_retificacao_recusado_p,0),
			qt_sem_movto_total		= qt_sem_movto_total + nvl(qt_sem_movto_total_p,0),
			qt_sem_movto_processado		= qt_sem_movto_processado + nvl(qt_sem_movto_processado_p,0),
			qt_sem_movto_recusado		= qt_sem_movto_recusado + nvl(qt_sem_movto_recusado_p,0)
		where	nr_sequencia			= nr_seq_devolucao_resumo_w;
		
		select	qt_inclusao_total,
			qt_inclusao_processado,
			qt_retificacao_total,
			qt_retificacao_processado,
			qt_exclusao_total,
			qt_exclusao_processado,
			qt_reinclusao_total,
			qt_reinclusao_processado,
			qt_total,
			qt_processados,
			qt_sem_movto_total,
			qt_sem_movto_processado,
			qt_alteracao_total,
			qt_alteracao_processado
		into	qt_inclusao_total_w,
			qt_inclusao_processado_w,
			qt_retificacao_total_w,
			qt_retificacao_processado_w,
			qt_exclusao_total_w,
			qt_exclusao_processado_w,
			qt_reinclusao_total_w,
			qt_reinclusao_processado_w,
			qt_total_w,
			qt_processados_w,
			qt_sem_movto_total_w,
			qt_sem_movto_processado_w,
			qt_alteracao_total_w,
			qt_alteracao_processado_w
		from	sib_devolucao_resumo
		where	nr_sequencia	= nr_seq_devolucao_resumo_w;

		if	(qt_inclusao_processado_w <> 0) then
			pr_inclusao_w	:= (qt_inclusao_processado_w*100)/qt_inclusao_total_w;
		else
			pr_inclusao_w	:= 0;
		end if;
		
		if	(qt_alteracao_processado_w <> 0) then
			pr_alteracao_w	:= (qt_alteracao_processado_w*100)/qt_alteracao_total_w;
		else
			pr_alteracao_w	:= 0;
		end if;
		
		
		if	(qt_retificacao_processado_w <> 0) then
			pr_retificacao_w	:= (qt_retificacao_processado_w*100)/qt_retificacao_total_w;
		else
			pr_retificacao_w	:= 0;
		end if;
		
		if	(qt_exclusao_processado_w <> 0) then
			pr_exclusao_w	:= (qt_exclusao_processado_w*100)/qt_exclusao_total_w;
		else
			pr_exclusao_w	:= 0;
		end if;
		
		if	(qt_reinclusao_processado_w <> 0) then
			pr_reinclusao_w	:= (qt_reinclusao_processado_w*100)/qt_reinclusao_total_w;
		else
			pr_reinclusao_w	:= 0;
		end if;
		
		if	(qt_processados_w <> 0) then
			pr_total_w	:= (qt_processados_w*100)/qt_total_w;
		else
			pr_total_w	:= 0;
		end if;
		
		if	(qt_sem_movto_processado_w <> 0) then
			pr_sem_movto_w	:= (qt_sem_movto_processado_w*100)/qt_sem_movto_total_w;
		else
			pr_sem_movto_w	:= 0;
		end if;
		
		update	sib_devolucao_resumo
		set	pr_inclusao_acerto	= pr_inclusao_w,
			pr_retificacao_acerto	= pr_retificacao_w,
			pr_exclusao_acerto	= pr_exclusao_w,
			pr_reinclusao_acerto	= pr_reinclusao_w,
			pr_total		= pr_total_w,
			pr_sem_movto_acerto	= pr_sem_movto_w,
			pr_alteracao_acerto	= pr_alteracao_w
		where	nr_sequencia		= nr_seq_devolucao_resumo_w;
		
	end if;
elsif	(ie_tipo_info_p = 'M') then
	select	max(nr_sequencia)
	into	nr_seq_devolucao_resumo_w
	from	sib_devolucao_resumo
	where	nr_seq_lote	= nr_seq_lote_sib_p
	and	cd_mensagem	= cd_mensagem_p
	and	ie_tipo_item	= 'M'
	and	ie_xml		= 'S';

	if	(nr_seq_devolucao_resumo_w is null) then
		insert into sib_devolucao_resumo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_lote,ie_tipo_item,ie_xml,cd_mensagem,ds_mensagem_erro,
					qt_ocorrencia_mensagem)
		values		(	sib_devolucao_resumo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_lote_sib_p,'M','S',cd_mensagem_p,ds_mensagem_erro_p,
					qt_ocorrencia_mensagem_p);
	else
		update	sib_devolucao_resumo
		set	qt_ocorrencia_mensagem	= qt_ocorrencia_mensagem + nvl(qt_ocorrencia_mensagem_p,0)
		where	nr_sequencia		= nr_seq_devolucao_resumo_w;
	end if;

end if;

commit;

end pls_imp_dev_resumo_sib_xml;
/
