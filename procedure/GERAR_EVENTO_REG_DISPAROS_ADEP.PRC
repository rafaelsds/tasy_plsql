create or replace 
procedure gerar_evento_reg_disparos_adep(	ie_tipo_dosagem_p varchar2,
						ie_forma_infusao_p varchar2,
						nr_seq_solucao_p number,
						nr_prescricao_p	number,
						nm_usuario_p	varchar2,
						dt_inicio_pca_p	varchar2,
						dt_fim_pca_p	varchar2,
						nr_disparo_pca_p	number,
						nr_disparo_efetivo_p number,
						qt_vol_infundido_p	number)is

nr_sequencia_w			number(10,0);

begin

/* obter sequencia */
select	prescr_solucao_evento_seq.nextval
into	nr_sequencia_w
from	dual;

/* gerar evento */
insert into prescr_solucao_evento (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_prescricao,
	nr_seq_solucao,
	ie_forma_infusao,
	ie_tipo_dosagem,
	cd_pessoa_fisica,
	ie_alteracao,
	dt_alteracao,
	ie_evento_valido,
	nr_seq_motivo,
	ds_observacao,
	ie_tipo_solucao,
	dt_inicio_pca,
	dt_fim_pca,
	nr_disparo_pca,
	nr_disparo_efetivo,
	qt_vol_infundido
	)
values (
	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_prescricao_p,
	nr_seq_solucao_p,
	ie_forma_infusao_p,
	ie_tipo_dosagem_p,	
	obter_dados_usuario_opcao(nm_usuario_p, 'C'),
	22,
	sysdate,
	'S',
	null,
	null,
	1,
	to_date(dt_inicio_pca_p,'dd/mm/yyyy hh24:mi:ss'),
	to_date(dt_fim_pca_p,'dd/mm/yyyy hh24:mi:ss'),
	nr_disparo_pca_p,
	nr_disparo_efetivo_p,
	qt_vol_infundido_p);
	
commit;

end gerar_evento_reg_disparos_adep;
/