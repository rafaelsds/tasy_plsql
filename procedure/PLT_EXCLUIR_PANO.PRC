create or replace
procedure plt_excluir_pano( 	nr_atendimento_p	number,
				nm_usuario_p		varchar2,
				cd_pessoa_fisica_p	varchar2) is 

nr_prescricao_w		number(14);				
nr_sequencia_cirurgia_w	number(14);							
begin

if	(nvl(nr_atendimento_p, 0) <> 0) and	
	(nm_usuario_p is not null) then

	select	nvl(max(a.nr_prescricao),0)
	into	nr_prescricao_w	
	from	prescr_medica a
	where	a.nr_atendimento = nr_atendimento_p
	and	a.nm_usuario_original = nm_usuario_p
	and	a.dt_liberacao is null
	and	a.dt_liberacao_medico is null
	and not exists (	select	1
         			from   cirurgia x
			where	x.nr_prescricao = a.nr_prescricao
			and    nvl(a.ie_tipo_prescr_cirur,0) <> 2);
			
elsif	(cd_pessoa_fisica_p is not null) and
	(nm_usuario_p is not null) then
				
	select	nvl(max(a.nr_prescricao),0)
	into	nr_prescricao_w	
	from	prescr_medica a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.nr_atendimento is null
	and	a.nm_usuario_original = nm_usuario_p
	and	a.dt_liberacao is null
	and	a.dt_liberacao_medico is null
	and not exists (	select	1
         			from   cirurgia x
			where	x.nr_prescricao = a.nr_prescricao
			and    nvl(a.ie_tipo_prescr_cirur,0) <> 2);
			
end if;

if	(nr_prescricao_w > 0) then
	delete	from prescr_medica
	where	nr_prescricao = nr_prescricao_w;
	commit;
end if;

end plt_excluir_pano;
/
