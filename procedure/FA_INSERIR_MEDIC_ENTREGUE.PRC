create or replace 
procedure fa_inserir_medic_entregue(nr_seq_rec_entregue_p	number,
				    nm_usuario_p		Varchar2,
				    ds_erro_p			out varchar2) is 

				    
cd_material_w		varchar2(10);
qt_dose_w		number(18,6);
qt_dispensar_w		number(18,6);
nr_seq_receita_w	number(10);
nr_seq_receita_ant_w	varchar2(15);
nr_seq_entr_item_w	number(10);
nr_seq_item_rec_w	number(10);
dt_liberacao_cih_w	date;
ie_exige_lib_cih_w	varchar2(1);
nr_saldo_w		number(10);
cd_unidade_baixa_w	varchar2(10);
nr_seq_pac_entrega_w	number(10);
qt_saldo_ant_w		number(15,3);
qt_saldo_atu_w		number(15,3);
ie_libera_sus_w		varchar2(2);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_item_receita_w	number(10);
ds_medicamento_w	varchar2(255);
ie_pmc_w		varchar2(1);
ie_controla_pmc_w	varchar2(1);
dt_periodo_inicial_w	date;	
dt_periodo_final_w	date;
ie_consiste_periodo_w	varchar2(1);
qt_saldo_anterior_w	number(5);
qt_dispensar_receita_w	number(18,6);
ds_observacao_w		varchar2(255);
nr_seq_item_w		number(10);
ie_considera_prim_entr_w varchar2(1);
ie_primeira_entrega_w	varchar2(1);
ie_tipo_medic_w		varchar2(3);
ds_erro_w		varchar2(2000);
list_med_regulados_w	varchar2(4000);
nm_med_w	varchar2(255);
ie_material_regulado_w	varchar2(1);

				    
cursor C01 is
select 	f.cd_material,
	sum(nvl(h.qt_dose_dispensar,0)),
	f.nr_seq_receita,
	f.dt_liberacao_cih,
	nvl(m.ie_exige_lib_cih,'N') ie_exige_lib_cih,
	m.cd_unidade_medida,
	e.nr_seq_paciente_entrega,
	f.nr_sequencia,
	e.dt_periodo_inicial,
	e.dt_periodo_final,
	f.nr_sequencia
from	fa_receita_farm_item_hor h,
	fa_receita_farmacia_item f,
	fa_entrega_medicacao e,
	fa_medic_farmacia_amb m,
	fa_receita_farmacia r
where	e.nr_seq_receita_amb = f.nr_seq_receita
and	r.nr_sequencia = f.nr_seq_receita
and	f.nr_sequencia = h.nr_seq_fa_item
and	e.nr_sequencia = nr_seq_rec_entregue_p
and	f.cd_material = m.cd_material
--and	trunc(h.dt_horario) between trunc(e.dt_periodo_inicial) and trunc(e.dt_periodo_final)
and	fa_obter_dias_intervalo(f.cd_intervalo,e.nr_sequencia, h.nr_sequencia, r.dt_validade_receita) = 'S'
and	r.dt_cancelamento is null
and	r.dt_liberacao is not null
AND 	((((nvl(m.ie_medicamento_pmc,'N') = ie_pmc_w and nvl(m.ie_nutricao,'N') = ie_pmc_w)
or	(ie_pmc_w = 'S' and (nvl(m.ie_medicamento_pmc,'N') = ie_pmc_w or nvl(m.ie_nutricao,'N') = ie_pmc_w)))
and 	(ie_controla_pmc_w = 'S')) or (ie_controla_pmc_w = 'N'))
and	obter_se_intervalo_prescricao(f.cd_intervalo,'S') = 'N'
and	nvl(m.cd_estabelecimento,r.cd_estabelecimento) = r.cd_estabelecimento
group by f.cd_material,f.nr_seq_receita,m.ie_exige_lib_cih,dt_liberacao_cih,m.cd_unidade_medida,
	e.nr_seq_paciente_entrega,f.nr_sequencia,e.dt_periodo_inicial,e.dt_periodo_final;

cursor C02 is
select 	h.nr_sequencia
from	fa_receita_farm_item_hor h,
	fa_receita_farmacia_item f,
	fa_entrega_medicacao e,
	fa_receita_farmacia r
where	e.nr_seq_receita_amb = f.nr_seq_receita
and	r.nr_sequencia = f.nr_seq_receita
and	f.nr_sequencia = h.nr_seq_fa_item
and	e.nr_sequencia = nr_seq_rec_entregue_p
AND	TRUNC(h.dt_horario) BETWEEN TRUNC(e.dt_periodo_inicial) AND TRUNC(e.dt_periodo_final)
and	r.dt_cancelamento is null
and	r.dt_liberacao is not null
and	f.cd_material = cd_material_w;

Cursor C03 is
	select	substr(obter_desc_material(b.cd_material),1,255),
		decode(nvl(c.ie_medicamento_pmc,'N'),'S','PMC','PNC')
	from	fa_entrega_medicacao a,
		fa_entrega_medicacao_item b,
		fa_medic_farmacia_amb c
	where	b.nr_seq_fa_entrega	= a.nr_sequencia
	and	b.cd_material 		= c.cd_material
	and	nvl(c.cd_estabelecimento,a.cd_estabelecimento) = a.cd_estabelecimento
	and	a.nr_sequencia 		= nr_seq_rec_entregue_p
	and	((nvl(c.ie_medicamento_pmc,'N') = 'S')
	or	(nvl(c.ie_nutricao,'N') = 'S'))
	and	not exists (	select	1
				from	fa_paciente_pmc x,
 					fa_paciente_pmc_medic y
				where	y.nr_seq_fa_pmc		= x.nr_sequencia
				and	x.cd_pessoa_fisica 	= a.cd_pessoa_fisica
				and	y.nr_seq_medic_fa 	= c.nr_sequencia);


--Se necess�rio
Cursor C04 is
	SELECT 	f.cd_material,
	SUM(NVL(f.qt_dose,0)),
	f.nr_seq_receita,
	f.dt_liberacao_cih,
	NVL(m.ie_exige_lib_cih,'N') ie_exige_lib_cih,
	m.cd_unidade_medida,
	e.nr_seq_paciente_entrega,
	f.nr_sequencia,
	e.dt_periodo_inicial,
	e.dt_periodo_final,
	f.nr_sequencia
FROM 	fa_receita_farmacia_item f,
	fa_entrega_medicacao e,
	fa_medic_farmacia_amb m,
	fa_receita_farmacia r
WHERE	e.nr_seq_receita_amb = f.nr_seq_receita
AND	r.nr_sequencia = f.nr_seq_receita
AND	e.nr_sequencia = nr_seq_rec_entregue_p
AND	f.cd_material = m.cd_material
and	nvl(m.cd_estabelecimento,r.cd_estabelecimento) = r.cd_estabelecimento
AND	r.dt_cancelamento IS NULL
AND	r.dt_liberacao IS NOT NULL
AND 	((NVL(m.ie_medicamento_pmc,'N') = ie_pmc_w AND (ie_controla_pmc_w = 'S')) OR (ie_controla_pmc_w = 'N'))
AND	obter_se_intervalo_prescricao(f.cd_intervalo,'S') = 'S'
GROUP BY f.cd_material,f.nr_seq_receita,m.ie_exige_lib_cih,dt_liberacao_cih,m.cd_unidade_medida,
	e.nr_seq_paciente_entrega,f.nr_sequencia,e.dt_periodo_inicial,e.dt_periodo_final;	
				
begin


list_med_regulados_w := Obter_Med_Regulaco_Receita(nr_seq_rec_entregue_p);


--verificar se � a primeira entrega da receita, n�o ser� controlado os medicamentos pmc [59]
ie_considera_prim_entr_w := obter_valor_param_usuario(10015, 59, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento); 

if (ie_considera_prim_entr_w = 'S') then
	SELECT	decode(count(*),0,'S','N')
	into	ie_primeira_entrega_w
	FROM   	fa_entrega_medicacao
	WHERE  	nr_seq_receita_amb = ( 	SELECT nr_seq_receita_amb
					FROM   fa_entrega_medicacao
					WHERE  nr_sequencia = nr_seq_rec_entregue_p)
	AND	nr_sequencia	<> nr_seq_rec_entregue_p;
end if;

if (ie_considera_prim_entr_w = 'S') and (ie_primeira_entrega_w = 'S') then
	ie_controla_pmc_w := 'N';
else	
	SELECT	DECODE(COUNT(*),0,'N','S')
	INTO	ie_controla_pmc_w
	FROM	fa_paciente_pmc a
	WHERE	a.cd_pessoa_fisica = (  SELECT  a.cd_pessoa_fisica 
				FROM   	fa_paciente_entrega a,
					fa_entrega_medicacao b
				WHERE  	a.nr_sequencia = b.nr_seq_paciente_entrega
				AND	b.nr_sequencia = nr_seq_rec_entregue_p)
	and	a.dt_saida is null;			
end if;
		    
SELECT 	DECODE(max(nr_seq_paciente_pmc),NULL,'N','S')
into	ie_pmc_w
FROM   	fa_paciente_entrega a,
	fa_entrega_medicacao b
WHERE  	a.nr_sequencia = b.nr_seq_paciente_entrega
AND	b.nr_sequencia = nr_seq_rec_entregue_p;

ds_erro_w := '';
delete
from	fa_entr_medic_item_receita
where	nr_seq_item_entrega in (select 	nr_sequencia 
				from	fa_entrega_medicacao_item
				where	nr_seq_fa_entrega = nr_seq_rec_entregue_p);

delete
from	fa_entrega_medicacao_item
where	nr_seq_fa_entrega = nr_seq_rec_entregue_p;		


open C01;
loop
fetch C01 into	
	cd_material_w,
	qt_dose_w,
	nr_seq_receita_w,
	dt_liberacao_cih_w,
	ie_exige_lib_cih_w,
	cd_unidade_baixa_w,
	nr_seq_pac_entrega_w,
	nr_seq_item_receita_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w,
	nr_seq_item_w;
exit when C01%notfound;

ie_consiste_periodo_w := fa_consiste_entrega_periodo(dt_periodo_inicial_w, dt_periodo_final_w, nr_seq_receita_w,nr_seq_rec_entregue_p,cd_material_w);
	
if (ie_consiste_periodo_w = 'S') then
	
	if (qt_dose_w > 0 ) then
		-- verifica se ha necessidade de liberar pelo CCIH
		if ((ie_exige_lib_cih_w = 'S') and (dt_liberacao_cih_w is not null)) or
		   (ie_exige_lib_cih_w = 'N')then
			
			select 	max(p.cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	fa_entrega_medicacao e,
				fa_paciente_entrega p
			where	e.nr_sequencia = nr_seq_rec_entregue_p
			and	e.nr_seq_paciente_entrega = p.nr_sequencia;
			ie_libera_sus_w := fa_obter_material_lib_sus(cd_material_w,cd_pessoa_fisica_w);
			if (ie_libera_sus_w = 'S') then

				--obtem saldo da entrega anterior
				--qt_saldo_ant_w := nvl(fa_obter_saldo_anterior(nr_seq_rec_entregue_p,nr_seq_pac_entrega_w,cd_material_w,nm_usuario_p),0);
				fa_obter_saldo_ant_intervalo(nr_seq_rec_entregue_p,nr_seq_pac_entrega_w,cd_material_w,nm_usuario_p, ds_observacao_w, qt_saldo_ant_w, qt_dose_w);
					
				qt_dispensar_w 	:= fa_obter_quantidade_disp(nr_seq_rec_entregue_p,nr_seq_pac_entrega_w,nr_seq_receita_w,nr_seq_item_receita_w,cd_material_w,qt_dose_w,nm_usuario_p);
				
				qt_saldo_atu_w  := fa_calcular_execesso_disp(nr_seq_rec_entregue_p,nr_seq_item_receita_w,cd_material_w,qt_dispensar_w,qt_dose_w,nr_seq_pac_entrega_w,nm_usuario_p);
				
				if (qt_dispensar_w < 0) then
					qt_dispensar_w:= 0;
				end if;
				
				-- verificar se tem saldo anterior pendente
				/*qt_saldo_anterior_w := fa_obter_qtd_entrega_periodo(dt_periodo_inicial_w, dt_periodo_final_w, nr_seq_receita_w, nr_seq_rec_entregue_p,cd_material_w);
				if ((qt_saldo_anterior_w > 0) and (qt_dispensar_w > qt_saldo_anterior_w)) then
					qt_dispensar_w := qt_saldo_anterior_w;
				end if;
				/*if (cd_material_w = 1310) then
					
				end if;*/
				
				/*--verificar se possui medicamentos entregue em outras receitas
				fa_consiste_periodo_receita(nr_seq_rec_entregue_p, nr_seq_receita_w, cd_material_w, qt_dispensar_w, nr_seq_receita_ant_w);
				if (nr_seq_receita_ant_w is not null) then
					ds_observacao_w := 'Considerado o saldo da receita '||nr_seq_receita_ant_w;
				end if;*/
				
				if (qt_dose_w > 0) then
				
					select 	max(obter_desc_material(cd_material_w)) ds_material
					into	nm_med_w
					from 	dual;
					
					select	max(obter_se_contido_entre_virgula(list_med_regulados_w, nm_med_w))
					into	ie_material_regulado_w
					from	dual;
					
					
					if(nvl(ie_material_regulado_w, 'N') = 'N')then
						
						select	fa_entrega_medicacao_item_seq.nextval
						into	nr_seq_entr_item_w
						from	dual;					

						insert 
						into	fa_entrega_medicacao_item
							(nr_sequencia,
							 dt_atualizacao,
							 nm_usuario,
							 dt_atualizacao_nrec,
							 nm_usuario_nrec,
							 nr_seq_fa_entrega,
							 qt_dispensar,
							 cd_material,
							 qt_gerado,
							 ie_status_medicacao, --registrar de receituario
							 cd_unidade_baixa,
							 qt_saldo_anterior,
							 qt_saldo_atual,
							 ds_observacao,
							 qt_gerado_ini,
							 nr_seq_rec_item,
							 qt_dose
							)
						values
							(nr_seq_entr_item_w,
							 sysdate,
							 nm_usuario_p,
							 sysdate,
							 nm_usuario_p,
							 nr_seq_rec_entregue_p,
							 qt_dispensar_w, --qt_dispensar
							 cd_material_w,
							 qt_dispensar_w, --qt_gerado
							 'RR',
							 cd_unidade_baixa_w,
							 nvl(qt_saldo_ant_w,0),
							 nvl(qt_saldo_atu_w,0),
							 ds_observacao_w,
							 qt_dispensar_w,
							 nr_seq_item_w,
							 qt_dose_w
							); 
					end if;
				
					open C02;
					loop
					fetch C02 into	
						nr_seq_item_rec_w;
					exit when C02%notfound;
						insert 
						into	fa_entr_medic_item_receita
							(nr_sequencia,
							 dt_atualizacao,
							 nm_usuario,
							 dt_atualizacao_nrec,
							 nm_usuario_nrec,
							 nr_seq_item_entrega,
							 nr_seq_medic_item_hor)
						values  (
							fa_entr_medic_item_receita_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_entr_item_w,
							nr_seq_item_rec_w
							);
					end loop;
					close C02;
					
				end if;
			else
				ds_erro_w := ds_erro_w ||WHEB_MENSAGEM_PCK.get_texto(277740,'CD_MATERIAL='||Obter_Desc_Material(cd_material_w))|| chr(13);
			end if;
		else
			ds_erro_w := ds_erro_w ||WHEB_MENSAGEM_PCK.get_texto(277741,'CD_MATERIAL='||Obter_Desc_Material(cd_material_w))|| chr(13);
		end if;
			
	end if;
end if;
end loop;
close C01;
--Se necess�rio
open C04;
loop
fetch C04 into	
	cd_material_w,
	qt_dose_w,
	nr_seq_receita_w,
	dt_liberacao_cih_w,
	ie_exige_lib_cih_w,
	cd_unidade_baixa_w,
	nr_seq_pac_entrega_w,
	nr_seq_item_receita_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w,
	nr_seq_item_w;
exit when C04%notfound;
	begin
		if (qt_dose_w > 0 ) then
			-- verifica se ha necessidade de liberar pelo CCIH
			if ((ie_exige_lib_cih_w = 'S') and (dt_liberacao_cih_w is not null)) or (ie_exige_lib_cih_w = 'N')then
				select 	p.cd_pessoa_fisica
				into	cd_pessoa_fisica_w
				from	fa_entrega_medicacao e,
					fa_paciente_entrega p
				where	e.nr_sequencia = nr_seq_rec_entregue_p
				and	e.nr_seq_paciente_entrega = p.nr_sequencia;
				
				ie_libera_sus_w := fa_obter_material_lib_sus(cd_material_w,cd_pessoa_fisica_w);
				if (ie_libera_sus_w = 'S') then
	
					select	fa_entrega_medicacao_item_seq.nextval
					into	nr_seq_entr_item_w
					from	dual;					
					
					select 	max(obter_desc_material(cd_material_w)) ds_material
					into	nm_med_w
					from 	dual;
					
					
					select	max(obter_se_contido_entre_virgula(list_med_regulados_w, nm_med_w)) 
					into	ie_material_regulado_w
					from	dual;
					
					if(nvl(ie_material_regulado_w, 'N') = 'N')then
						
						insert 	into	fa_entrega_medicacao_item
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_fa_entrega,
								qt_dispensar,
								cd_material,
								qt_gerado,
								ie_status_medicacao, --registrar de receituario
								cd_unidade_baixa,
								qt_saldo_anterior,
								qt_saldo_atual,
								ds_observacao,
								qt_gerado_ini,
								nr_seq_rec_item,
								qt_dose
								)
							values
								(nr_seq_entr_item_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_rec_entregue_p,
								qt_dose_w, --qt_dispensar
								cd_material_w,
								qt_dose_w, --qt_gerado
								'RR',
								cd_unidade_baixa_w,
								0,
								0,
								wheb_mensagem_pck.get_texto(307491), -- Medicamento com intervalo: Se necess�rio
								qt_dose_w,
								nr_seq_item_w,
								qt_dose_w
								); 
					end if; 		
				end if;
			end if;
		end if;
	end;
end loop;
close C04;

open C03;
loop
fetch C03 into	
	ds_medicamento_w,
	ie_tipo_medic_w;
exit when C03%notfound;
	begin
	if	(ie_tipo_medic_w = 'PMC') then
		ds_erro_w :=	ds_erro_w || ds_medicamento_w || ' ' || WHEB_MENSAGEM_PCK.get_texto(277743,null) || chr(13);
	elsif	(ie_tipo_medic_w = 'PNC') then	
		ds_erro_w :=	ds_erro_w || ds_medicamento_w || ' ' || WHEB_MENSAGEM_PCK.get_texto(277744,null) || chr(13);
	end if;	
	end;
end loop;
close C03;

if( list_med_regulados_w is not null) then
	list_med_regulados_w := replace(list_med_regulados_w, ',', chr(13) || chr(10));
	ds_erro_w := ds_erro_w || obter_desc_expressao(945317) || chr(13) || chr(10) || list_med_regulados_w;
end if;



if (length(ds_erro_w) >= 255) then
	ds_erro_w := substr(ds_erro_w,1,250) ||'...';
end if;

ds_erro_p := ds_erro_w;

commit;

end fa_inserir_medic_entregue;
/