create or replace procedure WSUITE_ADD_NOTIFICATION(
   ie_notification_type_p Varchar2,
   nr_primary_key_p  number,
   nr_auxiliary_key_p  number,
   nm_usuario_p   varchar2,
   cd_pessoa_fisica_p  varchar2,
   nr_seq_prescr_proc_p number) is

qt_user_w number(10);

begin

select count(*)
into qt_user_w
from wsuite_usuario a
where a.cd_pessoa_fisica = cd_pessoa_fisica_p
or   exists (select 1
    from pessoa_fisica_dependente b
    where b.cd_pessoa_fisica = a.cd_pessoa_fisica
    and  b.cd_pessoa_dependente = cd_pessoa_fisica_p);

if (ie_notification_type_p is not null) and (qt_user_w > 0) then

 insert into WSUITE_NOTIFICATION (nr_sequencia,
         dt_atualizacao,
         nm_usuario,
         dt_atualizacao_nrec,
         nm_usuario_nrec,
         nr_primary_key,
         nr_auxiliary_key,
         dt_notification,
         ie_notification_type,
         ie_notification_status,
         cd_pessoa_fisica,
         nr_seq_prescr_proc)
       values (WSUITE_NOTIFICATION_seq.nextval,
         sysdate,
         nm_usuario_p,
         sysdate,
         nm_usuario_p,
         nr_primary_key_p,
         nr_auxiliary_key_p,
         sysdate,
         ie_notification_type_p,
         'U',
         cd_pessoa_fisica_p,
         nr_seq_prescr_proc_p);

end if;

--commit;

end WSUITE_ADD_NOTIFICATION;

/