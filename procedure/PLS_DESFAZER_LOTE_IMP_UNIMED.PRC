create or replace
procedure pls_desfazer_lote_imp_unimed(
			nr_seq_lote_p		varchar2,
			nm_usuario_p		Varchar2) is 

begin

update	pls_lote_importacao_unimed
set	dt_consistente	= ''
where	nr_sequencia	= nr_seq_lote_p;	

commit;

end pls_desfazer_lote_imp_unimed;
/