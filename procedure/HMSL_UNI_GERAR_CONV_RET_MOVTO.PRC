Create or replace procedure HMSL_UNI_GERAR_CONV_RET_MOVTO (nr_seq_retorno_p number) is

cd_prestador_w		number(10);
dt_mesano_pagamento_w	date;
vl_cobrado_w		varchar2(255);
vl_total_pagar_w		varchar2(255);
dt_execucao_w		date;
nr_conta_w		number(10);
ds_complemento_w	varchar2(255);
cd_usuario_convenio_w	varchar2(255);
cd_item_w		number(15);

cursor c01 is

Select	to_number(substr(obter_valor_campo_separador(ds_conteudo, 2, '|'),1,255)) cd_prestador,
	substr(obter_valor_campo_separador(ds_conteudo, 7, '|'),1,255) vl_tot_cobrado,
	substr(obter_valor_campo_separador(ds_conteudo, 8, '|'),1,255) vl_tot_calc_pagar,
	0  nr_conta,
	'' ds_observacao,
	null cd_usuario_convenio,
	null dt_execucao,
	null cd_item
from	w_conv_ret_movto
where	substr(obter_valor_campo_separador(ds_conteudo, 1, '|'),1,255) = 'UP0'
union all
select	0,
	substr(obter_valor_campo_separador(ds_conteudo, 8, '|'),1,255) vl_tot_cobrado,
	substr(obter_valor_campo_separador(ds_conteudo, 9, '|'),1,255) vl_calculado,
	to_number(substr(obter_valor_campo_separador(ds_conteudo, 7, '|'),1,255)) nr_conta,
	substr(obter_valor_campo_separador(ds_conteudo, 3, '|'),1,15) ds_observacao,
	substr(obter_valor_campo_separador(ds_conteudo, 2, '|'),1,255) cd_usuario_convenio,
	to_date(substr(obter_valor_campo_separador(ds_conteudo, 4, '|'),1,255),'dd/mm/yyyy') dt_execucao,
	null cd_item
from	w_conv_ret_movto
where	substr(obter_valor_campo_separador(ds_conteudo, 1, '|'),1,255) = 'UP1'
union all
Select	0,
	substr(obter_valor_campo_separador(ds_conteudo, 7, '|'),1,255) vl_cobrado,
	substr(obter_valor_campo_separador(ds_conteudo, 8, '|'),1,255) vl_calc,
	0  nr_conta,
	substr(obter_valor_campo_separador(ds_conteudo, 11, '|'),1,230) ds_obs,
	null cd_usuario_convenio,
	null dt_execucao,
	somente_numero(substr(obter_valor_campo_separador(ds_conteudo, 2, '|'),1,50)) cd_item
from	w_conv_ret_movto
where	substr(obter_valor_campo_separador(ds_conteudo, 1, '|'),1,255) = 'UP2';

begin

open c01;
loop
fetch c01 into
	cd_prestador_w,
	vl_cobrado_w,
	vl_total_pagar_w,
	nr_conta_w,
	ds_complemento_w,
	cd_usuario_convenio_w,
	dt_execucao_w,
	cd_item_w;
exit when c01%notfound;
	
	begin

	insert into convenio_retorno_movto 
		(nr_sequencia,
		cd_prestador,
		vl_cobrado,
		vl_total_pago,
		nm_usuario,
		nr_conta,
		ds_complemento,
		dt_atualizacao,
		nr_seq_retorno,
		cd_usuario_convenio,
		dt_execucao,
		cd_item)
	values	(convenio_retorno_movto_seq.nextval,
		(to_number(cd_prestador_w)),
		(to_number(replace(replace(vl_cobrado_w, '.', ''), ',', '')) / 100),
		(to_number(replace(replace(vl_total_pagar_w, '.', ''), ',', '')) / 100),
		'Tasy',
		nr_conta_w,
		ds_complemento_w,
		sysdate,
		nr_seq_retorno_p,
		cd_usuario_convenio_w,
		dt_execucao_w,
		cd_item_w);

	end;	
end loop;
close c01;

Delete from w_conv_ret_movto 
where nr_seq_retorno  = nr_seq_retorno_p;

commit;

end HMSL_UNI_GERAR_CONV_RET_MOVTO;
/