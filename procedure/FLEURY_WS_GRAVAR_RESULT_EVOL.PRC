create or replace
procedure fleury_ws_gravar_result_evol (	nr_ficha_fleury_p   	NUMBER,
											nr_prescricao_p     	NUMBER,
											nr_seq_prescr_p     	NUMBER,
											cd_exame_p        		VARCHAR2,											
											--ds_analito_p		VARCHAR2,
											nm_usuario_p			VARCHAR2,
											cd_unidade_fleury_p		VARCHAR2 DEFAULT NULL,
											nr_seq_result_evol_p	out varchar2) IS
										

nr_seq_result_w			result_laboratorio.nr_sequencia%type;
nr_prescricao_w			prescr_procedimento.nr_prescricao%type;
nr_seq_prescr_w			prescr_procedimento.nr_sequencia%type;
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;

ie_exame_w				number(10);
nr_seq_exame_w			exame_laboratorio.nr_seq_exame%type;
nr_seq_superior_w		exame_laboratorio.nr_seq_superior%type;
nr_seq_exame_origem_w	exame_laboratorio.nr_seq_exame%type;
nr_seq_prescr_aux_w		prescr_procedimento.nr_sequencia%type;
nr_seq_result_evol_w	result_laboratorio_evol.nr_sequencia%type;

CURSOR C01 IS
	SELECT  1,
		nr_seq_exame,
		nr_seq_superior,
		NULL
	FROM    exame_laboratorio
	WHERE 	NVL(cd_exame_integracao, cd_exame) = cd_exame_p
	UNION
	SELECT  2,
		c.nr_seq_exame,
		a.nr_seq_exame,
		c.nr_seq_exame AS nr_seq_exame_origem
	FROM    exame_lab_format a,
		exame_lab_format_item b,
		exame_laboratorio c
	WHERE   a.nr_seq_formato    = b.nr_seq_formato
	AND    	b.nr_seq_exame        = c.nr_seq_exame
	AND    	NVL(c.cd_exame_integracao, c.cd_exame) = cd_exame_p
	UNION
	SELECT  3,
		nr_seq_exame,
		nr_seq_superior,
		NULL
	FROM    exame_laboratorio
	WHERE 	NVL(cd_exame_integracao, cd_exame) = REPLACE(cd_exame_p, 'URG', '')
	UNION
	SELECT  4,
		c.nr_seq_exame,
		a.nr_seq_exame,
		c.nr_seq_exame AS nr_seq_exame_origem
	FROM    exame_lab_format a,
		exame_lab_format_item b,
		exame_laboratorio c
	WHERE   a.nr_seq_formato = b.nr_seq_formato
	AND    	b.nr_seq_exame   = c.nr_seq_exame
	AND    	NVL(c.cd_exame_integracao, c.cd_exame) = REPLACE(cd_exame_p, 'URG', '')
	UNION
	SELECT	5,
		e.nr_seq_exame,
		e.nr_seq_superior,
		NULL
	FROM	exame_laboratorio e
	WHERE	e.nr_seq_exame 	= obter_equip_exame_integracao(cd_exame_p,'FLEURY',1)
	ORDER 	BY 1,2;
											
begin

nr_prescricao_w	:= nr_prescricao_p;
nr_seq_prescr_w := nr_seq_prescr_p;

IF    (NVL(nr_prescricao_w,0) = 0) THEN

	SELECT	MAX(cd_estabelecimento)
	INTO	cd_estabelecimento_w
	FROM	lab_parametro
	WHERE	cd_unidade_fleury = cd_unidade_fleury_p;


	SELECT  Obter_Prescr_Controle(nr_ficha_fleury_p, cd_estabelecimento_w)
	INTO   	nr_prescricao_w
	FROM    dual;

END IF;

IF    (cd_exame_p IS NOT NULL) THEN

    OPEN C01;
    LOOP
    FETCH C01 INTO    ie_exame_w,
            nr_seq_exame_w,
            nr_seq_superior_w,
            nr_seq_exame_origem_w;
        EXIT WHEN C01%NOTFOUND;

        SELECT MIN(NVL(nr_sequencia, NULL))
        INTO    nr_seq_prescr_aux_w
        FROM    prescr_procedimento
        WHERE nr_prescricao        = trim(nr_prescricao_w)
          AND nr_seq_exame        = nr_seq_exame_w;

        IF    NVL(nr_seq_prescr_w,0)=0  THEN

            SELECT MIN(NVL(nr_sequencia, NULL))
            INTO    nr_seq_prescr_w
            FROM    prescr_procedimento
            WHERE nr_prescricao        = trim(nr_prescricao_w)
              AND nr_seq_exame        = nr_seq_exame_w;
        END IF;

        IF    (nr_seq_superior_w IS NOT NULL) AND
            (nr_seq_prescr_aux_w IS NULL) THEN

            IF    (nr_seq_prescr_w IS NULL) THEN
                SELECT    MAX(NVL(nr_sequencia, NULL))
                INTO    nr_seq_prescr_w
                FROM    prescr_procedimento
                WHERE    nr_prescricao = trim(nr_prescricao_w)
                  AND    ie_status_atend >= 30
                  AND    nr_seq_exame = nr_seq_superior_w;

                IF    (nr_seq_prescr_w IS NULL) THEN
                    SELECT    MAX(NVL(nr_sequencia, NULL))
                    INTO    nr_seq_prescr_w
                    FROM    prescr_procedimento
                    WHERE    nr_prescricao = trim(nr_prescricao_w)
                    AND    nr_seq_exame = nr_seq_superior_w;
                END IF;
            END IF;
            nr_seq_exame_w    := nr_seq_superior_w;
        END IF;

        IF    (nr_seq_prescr_w IS NOT NULL) THEN
            EXIT;
        END IF;

    END LOOP;
    CLOSE C01;

END IF;

IF    (nr_seq_prescr_w IS NULL) THEN
    --A sequ�ncia da prescri��o n�o foi encontrada. Prescri��o:' || nr_prescricao_w  || 'Exame: ' || cd_exame_p ||' #@#@'
	Wheb_mensagem_pck.exibir_mensagem_abort(264078,'NR_PRESCRICAO='||nr_prescricao_w||';CD_EXAME='||cd_exame_p);

END IF;

SELECT	NVL(MAX(a.nr_sequencia),0)
INTO	nr_seq_result_w
FROM	result_laboratorio a
WHERE	a.nr_seq_prescricao = nr_seq_prescr_w
AND		a.nr_prescricao = nr_prescricao_w;

IF    (nr_seq_result_w > 0) THEN

	select	result_laboratorio_evol_seq.nextval
	into	nr_seq_result_evol_w
	from	dual;

	insert into result_laboratorio_evol (    
						nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						--ds_resultado,
						nr_seq_result_lab)
	values(				nr_seq_result_evol_w,
						nm_usuario_p,
						sysdate,
						--ds_analito_p,
						nr_seq_result_w);
	
	nr_seq_result_evol_p	:= nr_seq_result_evol_w;

	commit;

end if;

end fleury_ws_gravar_result_evol;
/