create or replace
procedure baixar_lote_deposito_ident
			(	nr_seq_lote_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a baixa dos itens do lote de retorno de dep�sito identificado, caso seja
encontrado dep�sito com o c�digo � realizada tentativa de baixa do item, caso ocorra um 
problema durante a baixa � gerado um log com o problema, caso nem mesmo seja realizada
identifica��o � gerado um log de que o dep�sito inexiste
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Tratamentos realizados dentro da rotina BAIXAR_ITENS_DEPOSITO_IDEN
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
				
ds_erro_baixa_w		varchar2(255);
cd_identif_dep_w	varchar2(50);
nr_identificacao_w	varchar2(30);				
cd_identificacao_w	varchar2(25);
ie_digito_w		varchar2(2);
ie_digito_ident_w	varchar2(2);
ie_identificado_w	varchar2(1)	:= null;
vl_deposito_w		number(15,2);
nr_seq_conta_banco_w	number(10);
nr_seq_deposito_w	number(10);
nr_seq_ret_dep_w	number(10);
dt_liberacao_w		date;

Cursor C01 is
	select	a.cd_identificacao,
		a.vl_deposito,
		0, 
		a.nr_sequencia
	from	lote_ret_dep_ident_item	a
	where	a.nr_seq_lote	= nr_seq_lote_p
	and	ds_erro_baixa is null;

Cursor C02 is
	select	a.nr_sequencia,
		a.nr_identificacao,
		a.ie_digito_ident
	from	deposito_identificado	a
	where	a.dt_deposito is null
	and	a.cd_estabelecimento	= cd_estabelecimento_p 
	and	a.nr_seq_conta_banco 	= nr_seq_conta_banco_w
	and	a.ie_status = 'AD'
	order by 1;

begin
if	(nr_seq_lote_p is not null) then
	-- Fazer consistencias
	select	a.nr_seq_conta_banco
	into	nr_seq_conta_banco_w
	from	lote_ret_deposito_ident	a
	where	a.nr_sequencia	= nr_seq_lote_p;

	update	lote_ret_dep_ident_item a
	set	ds_erro_baixa	= wheb_mensagem_pck.get_texto(303465) -- N�mero de identifica��o repetido.
	where	exists
		(select	1
		from	lote_ret_dep_ident_item x
		where	x.nr_sequencia		<> a.nr_sequencia
		and	x.cd_identificacao	= a.cd_identificacao
		and	x.nr_seq_lote		= a.nr_seq_lote)
	and	a.ds_erro_baixa	is null
	and	a.nr_seq_lote	= nr_seq_lote_p;

	<<comeco>>
	
	begin
	open C01;
	loop
	fetch C01 into	
		cd_identificacao_w,
		vl_deposito_w,
		ie_digito_w,
		nr_seq_ret_dep_w;
	exit when C01%notfound;
		begin	
		ds_erro_baixa_w		:= '';
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_deposito_w,
			nr_identificacao_w,
			ie_digito_ident_w;
		exit when C02%notfound;
			begin
			ie_identificado_w	:= 'N';
			
			cd_identif_dep_w	:= nr_identificacao_w || ie_digito_ident_w;
			
			if	(somente_numero(cd_identif_dep_w) = somente_numero(cd_identificacao_w)) or
				(nr_identificacao_w = somente_numero(cd_identificacao_w)) then
				baixar_itens_deposito_iden(nr_seq_deposito_w,cd_estabelecimento_p,nm_usuario_p,'N', nr_seq_lote_p);
				
				ie_identificado_w	:= 'S';
				
				exit;
			else
				ie_identificado_w	:= 'N';
			end if;
			end;
		end loop;
		close C02;
		
		if	(ie_identificado_w = 'N') then
			update  lote_ret_dep_ident_item	
			set	ds_erro_baixa	= wheb_mensagem_pck.get_texto(303466, 'NR_SEQ_DEPOSITO_W=' || nr_seq_deposito_w) -- N�mero de identifica��o n�o localizado. Dep�sito: #@NR_SEQ_DEPOSITO_W#@
			where 	nr_sequencia	= nr_seq_ret_dep_w;
		else			
			update  lote_ret_dep_ident_item	
			set	ds_erro_baixa	= null
			where 	nr_sequencia	= nr_seq_ret_dep_w;
		end if;
		end;
	end loop;
	close C01;
	exception
	when others then
		rollback;
		
		/* Tratado para fechar os cursores caso ocorra um erro durante a baixa do deop�sito identificado */
		if	(C02%ISOPEN) then
			close C02;
		end if;	

		if	(C01%ISOPEN) then
			close C01;
		end if;	

		ie_identificado_w	:= 'E';
		ds_erro_baixa_w		:= substr(sqlerrm, 1, 255);
	end;
	
	if	(ie_identificado_w = 'E') then
		ie_identificado_w	:= 'N';
		
		update  lote_ret_dep_ident_item	
		set	ds_erro_baixa	= nvl(ds_erro_baixa_w, wheb_mensagem_pck.get_texto(303468)) -- N�mero de identifica��o n�o localizado.
		where 	nr_sequencia	= nr_seq_ret_dep_w;
		
		commit;
		
		goto comeco;
	end if;
	
	-- Atualiza o status do lote
	update	lote_ret_deposito_ident
	set	nm_usuario	= nm_usuario_p,
		dt_liberacao	= sysdate
	where	nr_sequencia	= nr_seq_lote_p;
end if;

commit;

end baixar_lote_deposito_ident;
/
