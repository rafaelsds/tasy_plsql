create or replace
procedure Med_Gerar_Laudo_Interesse
		(nr_seq_laudo_p		number,
		nr_seq_classif_p		number,
		cd_medico_p		varchar2,
		nm_usuario_p		varchar2) is

nr_sequencia_w		number(10,0);
nr_seq_classif_w	number(10,0);
begin

nr_seq_classif_w := null;

if 	(nr_seq_classif_p > 0) then
	nr_seq_classif_w := nr_seq_classif_p;
end if;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	med_laudo_cdi
where	cd_medico	= cd_medico_p
and	nr_seq_laudo	= nr_seq_laudo_p;

if	(nr_sequencia_w <> 0) then
	begin

	update	med_laudo_cdi
	set	nr_seq_classif	= nr_seq_classif_w
	where	cd_medico	= cd_medico_p
	and	nr_seq_laudo	= nr_seq_laudo_p;

	end;
else
	begin

	select	med_laudo_cdi_seq.nextval
	into	nr_sequencia_w
	from 	dual;

	insert 	into med_laudo_cdi
		(nr_sequencia,	
		nr_seq_laudo,
		cd_medico,
		dt_atualizacao,
		nm_usuario,
		ds_laudo,
		nr_seq_classif)
	values	(nr_sequencia_w,
		nr_seq_laudo_p,
		cd_medico_p,
		sysdate,
		nm_usuario_p,
		' ',
		nr_seq_classif_w);

	end;
end if;

commit;

end Med_Gerar_Laudo_Interesse;
/
