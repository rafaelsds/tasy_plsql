create or replace procedure ger_dados_rel_flowsheet(cd_relatorio_p     in number,
                                                    ie_dados_p         in varchar2,
                                                    dt_inicial_p       in date,
                                                    dt_final_p         in date,
                                                    cd_pessoa_fisica_p in number) is

begin

  pck_flowsheet.gerar_dados(cd_relatorio_p,
                            ie_dados_p,
                            least(dt_inicial_p, trunc(sysdate, 'dd')),
                            least(fim_dia(dt_final_p), sysdate),
                            cd_pessoa_fisica_p);

  commit;

end ger_dados_rel_flowsheet;
/
