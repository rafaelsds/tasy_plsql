create or replace
procedure calcular_custo_padrao_html(cd_estabelecimento_p		number,
					nr_seq_tabela_p			number,
					nr_ficha_inicial_p		number,
					nr_ficha_final_p		number,
					nm_usuario_p			varchar2) is 

cd_tabela_custo_w	number;					
					
begin

cd_tabela_custo_w := obter_tab_custo_html(nr_seq_tabela_p);

calcular_custo_padrao (	cd_estabelecimento_p,
			cd_tabela_custo_w,
			nr_ficha_inicial_p,
			nr_ficha_final_p,
			nr_seq_tabela_p,
			nm_usuario_p);
				
commit;

end calcular_custo_padrao_html;
/