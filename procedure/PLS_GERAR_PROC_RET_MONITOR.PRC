create or replace
procedure PLS_GERAR_PROC_RET_MONITOR(	nr_seq_guia_ret_p	number,
					cd_grupo_proc_p		varchar2,
					cd_procedimento_p	number,
					cd_tabela_ref_p		varchar2,
					cd_dente_p	  	varchar2,
					cd_face_dente_p	  	varchar2,
					cd_regiao_boca_p  	varchar2,
					nm_usuario_p	  	varchar2,					
					nr_seq_proc_ret_p out	number) is 

				
nr_seq_proc_ret_w	pls_monitor_tiss_proc_ret.nr_sequencia%type;
begin
PLS_GERENCIA_ENVIO_ANS_PCK.GERAR_PROC_RET_MONITOR(	nr_seq_guia_ret_p,
							cd_grupo_proc_p,
							cd_procedimento_p,
							cd_tabela_ref_p,
							cd_dente_p,
							cd_face_dente_p,
							cd_regiao_boca_p,
							nm_usuario_p,					
							nr_seq_proc_ret_w);

--commit;
nr_seq_proc_ret_p := nr_seq_proc_ret_w;

end PLS_GERAR_PROC_RET_MONITOR;
/