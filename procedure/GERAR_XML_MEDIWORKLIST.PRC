create or replace
procedure Gerar_XML_MediWorkList(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					nr_seq_interno_p	number,
					nm_usuario_p		varchar2) is 

nr_seq_projeto_w 	number(10);
nr_seq_log_w		number(10);
ds_parametros_w		varchar2(255);
ds_xml_w		long;
cd_paciente_w		varchar2(10);
cd_medico_w		varchar2(10);		
integra_w           number(1);
			
			
arq_texto_w		utl_file.file_type;
	

Cursor C01 is
	select	ds_xml
	from	tasy_xml_banco
	where	nr_sequencia = nr_seq_log_w
	order by nr_Seq_geracao;
	
pragma autonomous_transaction;
	
begin
    integra_w := 0;
    select count(1) 
      into integra_w
    from	atendimento_paciente_v c,
      prescr_medica a,
      prescr_procedimento b,
      proc_interno e,
      REGRA_PROC_INTERNO_INTEGRA f
    where	c.nr_atendimento = a.nr_atendimento
    and b.nr_seq_proc_interno = e.nr_sequencia 
    and	a.nr_prescricao	 = b.nr_prescricao
    and  f.nr_seq_proc_interno (+) = e.nr_sequencia 
    and  ((f.cd_estabelecimento = c.cd_estabelecimento and f.ie_tipo_integracao = 24) or (f.nr_seq_proc_interno is null))
    and	b.nr_prescricao = nr_prescricao_p
    and	b.nr_sequencia	 = nr_sequencia_p;


    if (integra_w > 0) then
      /*Sequencia do projeto XML*/
      nr_seq_projeto_w := 100205;
      
      /*Parametros do projeto XML*/
      ds_parametros_w :='';
      /*Pegar a sequencia da tabela que ira gerar o XML*/
      select 	tasy_xml_banco_seq.nextval
      into 	nr_seq_log_w
      from 	dual;
      
      ds_parametros_w :='NR_PRESCRICAO='||nr_prescricao_p||';'||'NR_SEQ_PRESCRICAO='||nr_sequencia_p||';';
      
      dbms_output.put_line('Parametros = '||ds_parametros_w);
      dbms_output.put_line('nr_seq_log_w = '||nr_seq_log_w);
      
      wheb_exportar_xml(nr_seq_projeto_w,nr_seq_log_w,'',ds_parametros_w);
      
      
      
      commit;
      -- O parametro do banco utl_file_dir devera estar com valor "*" para que o arquivo possa ser gerado em qualquer diretorio do servidor.
      --Parametro 1: diretorio a ser gravado o arquivo no servidor de banco de dados.
      --Parametro 2: nome_do_arquivo a ser gerado
      
      
      --arq_texto_w := utl_file.fopen('/publico/',nr_seq_interno_p||'.xml','W');
      open C01;
      loop
      fetch C01 into	
        ds_xml_w;
      exit when C01%notfound;
        begin
        utl_file.put_line(arq_texto_w,ds_xml_w);
        utl_file.fflush(arq_texto_w);
        end;
      end loop;
      close C01;
      
      --fecha e libera o arquivo
      utl_file.fclose(arq_texto_w);
    end if;

end Gerar_XML_MediWorkList;
/