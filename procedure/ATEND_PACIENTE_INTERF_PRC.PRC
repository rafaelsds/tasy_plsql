CREATE OR REPLACE PROCEDURE atend_paciente_interf_prc  is

  nm_arquivo_w    varchar2(4000);
 
cursor c01 is
    select ds_arquivo 
    from sys.interf_arquivo 
    where upper(ds_arquivo) like ('ATEND%%%')
    order by substr(ds_arquivo,17,6);
    
 /*

Este objeto foi apenas documento pela philips, por�m foi desenvolvido por Jo�o Verissimo HQS
CREATE TABLE LOG_INTERF_ATEND
	(
	NM_ARQUIVO     	VARCHAR2 (2000),
	DT_ATUALIZACAO 	DATE,
	NM_USUARIO		VARCHAR2(15)
	)
	TABLESPACE TASY_DATA
	LOGGING
	NOCACHE
	STORAGE (BUFFER_POOL DEFAULT);
*/
  
begin

	begin
	  sys.list_directory('/mnt/tasy/interface/Atendimento');
	end;
  
  open  c01;
    loop
    fetch c01 into
      nm_arquivo_w;
    exit when c01%notfound;
    
      begin
        interf_atendimento(nm_arquivo_w);
      end;
      
      insert into log_interf_atend (nm_arquivo, dt_atualizacao, nm_usuario) values (nm_arquivo_w, sysdate, 'interf');
      commit;
    end loop;
  close c01;

  	  begin
	   	hracb_integ_sistema_legado('carga');
	  end;	

  delete from sys.interf_arquivo;
  
commit;

end atend_paciente_interf_prc;