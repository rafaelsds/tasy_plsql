create or replace
procedure pls_portab_confirmar_receb
			(	nr_seq_portabilidade_p	pls_portab_pessoa.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type) is

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
qt_inconsistencias_w		pls_integer;

begin
select	cd_estabelecimento
into	cd_estabelecimento_w
from	pls_portab_pessoa
where	nr_sequencia	= nr_seq_portabilidade_p;

if	(obter_valor_param_usuario(1242,6,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w) = 'N') then
	pls_consistir_portabilidade(nr_seq_portabilidade_p, nm_usuario_p, cd_estabelecimento_w);
	
	select	count(1)
	into	qt_inconsistencias_w
	from	pls_portab_solic_consist
	where	nr_seq_portabilidade = nr_seq_portabilidade_p;
	
	if	(qt_inconsistencias_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1174705);
	end if;
end if;


update	pls_portab_pessoa
set	ie_status	= 'L',
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_portabilidade_p;

pls_envio_email_portabilidade(nr_seq_portabilidade_p, 3, nm_usuario_p);

commit;

end pls_portab_confirmar_receb;
/