create or replace 
procedure Gerar_registro_ISBT(	nr_prescricao_p			number,
			nr_seq_procedimento_p		number,
			nr_bolsa_p		varchar2,
			nr_bolsa_isbt_p		varchar2,
			cd_bolsa_p		varchar2,
			qt_volume_p		number,
			nm_usuario_p		varchar2,
			ie_lavado_p		varchar2,
			ie_irradiado_p		varchar2,
			ie_filtrado_p		varchar2,
			ie_aliquotado_p		varchar2,
			nr_seq_horario_p		number default 0,
			nr_seq_horario_out_p		out number)
			is 

nr_sequencia_w		number(10,0);
nr_bolsas_w			number(10,0);
ie_status_bolsa_w	varchar2(3);
dt_horario_w		date;
nr_seq_horario_w	number(10);
nr_prescricao_w		prescr_procedimento.nr_prescricao%type;
nr_seq_proc_w		prescr_procedimento.nr_sequencia%type;
dt_atualizacao_w 	date;
param597 			varchar2(1);
ds_observacao_w		prescr_solucao_evento.ds_observacao%type;

begin

  if ((nvl(nr_prescricao_p, 0) > 0) or (nvl(nr_seq_procedimento_p, 0) > 0)) then

    dt_atualizacao_w := sysdate;

    if	((nvl(nr_prescricao_p, 0) = 0) and (nvl(nr_seq_procedimento_p, 0) > 0)) then
      select	max(nr_prescricao),
          max(nr_seq_procedimento)
      into	nr_prescricao_w,
          nr_seq_proc_w
      from	prescr_proc_hor
      where	nr_sequencia =	nr_seq_procedimento_p;
    else
      nr_seq_proc_w	:= nr_seq_procedimento_p;
      nr_prescricao_w	:= nr_prescricao_p;	
    end if;

    if	(nvl(nr_seq_horario_p,0) > 0) then
      nr_seq_horario_w	:= nr_seq_horario_p;

      select	nvl(count(nr_sequencia),0)
      into	nr_bolsas_w
      from	prescr_proc_bolsa
      where	nr_prescricao	= nr_prescricao_w
      and		nr_seq_horario	= nr_seq_horario_w;
    else
      select	nvl(count(nr_sequencia),0)
      into	nr_bolsas_w
      from	prescr_proc_bolsa
      where	nr_seq_procedimento  = nr_seq_proc_w
      and		nr_prescricao		 = nr_prescricao_w;	
    end if;

    if	(nvl(nr_bolsas_w,0) = 0) then

	    if	(nvl(nr_seq_horario_p,0) = 0) then	  
	        select	min(nr_sequencia)
      		into	nr_seq_horario_w
      		from	prescr_proc_hor
      		where	nr_seq_procedimento = nr_seq_proc_w
      		and		nr_prescricao 		= nr_prescricao_w;	
		end if;

      update	prescr_proc_hor
      set		qt_vol_hemocomp = qt_volume_p		
      where		nr_sequencia    = nr_seq_horario_w;

      param597 := Wheb_assist_pck.obterParametroFuncao(1113,597);

      if (param597 = 'U') then
        update	prescr_proc_hor
        set		dt_horario 	= dt_atualizacao_w
        where	nr_sequencia 	= nr_seq_horario_w;
      end if;
    else	
      dt_horario_w := obter_horario_apraz_bolsa(obter_perfil_ativo, nr_prescricao_w, nr_seq_proc_w);	
      gerar_horario_bolsa_hm(nr_prescricao_w, nr_seq_proc_w, dt_horario_w, qt_volume_p, nm_usuario_p, nr_seq_horario_w);
    end if;

    if	(nr_seq_horario_w = 0) then
      nr_seq_horario_w	:= null;
    end if;

    if (nvl(nr_seq_horario_w, 0) > 0) then    
      insert into prescr_proc_bolsa
        (nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_prescricao,
        nr_seq_procedimento,
        nr_bolsa,
        nr_bolsa_isbt,
        cd_bolsa,
        qt_volume,
        nr_seq_horario,
        ie_lavado,
        ie_irradiado,
        ie_filtrado,
        ie_aliquotado)
      values	(prescr_proc_bolsa_seq.nextval,
        dt_atualizacao_w,
        nm_usuario_p,
        dt_atualizacao_w,
        nm_usuario_p,
        nr_prescricao_w,
        nr_seq_proc_w,
        nr_bolsa_p,
        nr_bolsa_isbt_p,
        cd_bolsa_p,
        qt_volume_p,
        nr_seq_horario_w,
        ie_lavado_p,
        ie_irradiado_p,
        ie_filtrado_p,
        ie_aliquotado_p);

      select	nvl(max(ie_status),'N')
      into	ie_status_bolsa_w
      from	prescr_procedimento
      where	nr_seq_solic_sangue is not null
      and		nr_sequencia = nr_seq_proc_w
      and		nr_prescricao = nr_prescricao_w;

      if	(ie_status_bolsa_w = 'T') then
        update	prescr_procedimento
        set		dt_atualizacao = sysdate,
            nm_usuario = nm_usuario_p,
            ie_status = 'N'
        where	nr_seq_solic_sangue is not null
        and		nr_sequencia = nr_seq_proc_w
        and		nr_prescricao = nr_prescricao_w;
        commit;
      end if;

	if ((nr_bolsa_p is not null) and (cd_bolsa_p is null)) then
		ds_observacao_w := obter_desc_expressao(574896)||': '||nr_bolsa_p;
	elsif (cd_bolsa_p is not null) then		
		ds_observacao_w := obter_desc_expressao(574896)||': '||cd_bolsa_p;  
	elsif (nr_bolsa_isbt_p is not null) then
		ds_observacao_w := obter_desc_expressao(574896)||': '||nr_bolsa_isbt_p;
	end if;

	  /* obter sequencia */
      select	prescr_solucao_evento_seq.nextval
      into	nr_sequencia_w
      from	dual;

      /* gerar evento */
      insert into prescr_solucao_evento (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_prescricao,
        nr_seq_solucao,
        nr_seq_material,
        nr_seq_procedimento,
        nr_seq_nut,
        nr_seq_nut_neo,
        ie_forma_infusao,
        ie_tipo_dosagem,
        qt_dosagem,
        qt_vol_infundido,
        qt_vol_desprezado,
        qt_volume_parcial,
        cd_pessoa_fisica,
        ie_alteracao,
        dt_alteracao,
        ie_evento_valido,
        nr_seq_motivo,
        ds_observacao,
        ie_tipo_solucao,
        qt_volume_fase,
        nr_etapa_evento,
        dt_horario)
      values (
        nr_sequencia_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_prescricao_w,
        null,
        null,
        nr_seq_proc_w,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        obter_dados_usuario_opcao(nm_usuario_p, 'C'),
        28,
        sysdate,
        'S',
        null,
        ds_observacao_w,
        3,
        null,
        null,
        dt_horario_w);
    end if;

    nr_seq_horario_out_p := nr_seq_horario_w;

    commit;

  end if;
end Gerar_registro_ISBT;
/
