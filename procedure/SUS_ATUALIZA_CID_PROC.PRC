create or replace
procedure sus_atualiza_cid_proc(	nr_atendimento_p	in	number,
				cd_profissional_p	in	varchar2,
				cd_cid_p		out	varchar2) is 
			
cd_cid_w			Varchar2(10);
cd_estabelecimento_w	atendimento_paciente.cd_estabelecimento%type;
ie_restr_medico_w		varchar2(15) := 'N';

cursor c01 is
	select	nvl(b.cd_doenca,'')
	from	diagnostico_doenca b,
		diagnostico_medico	a
	where	a.nr_atendimento	= b.nr_atendimento
	and	a.dt_diagnostico	= b.dt_diagnostico
	and	((ie_restr_medico_w = 'N') or (a.cd_medico = nvl(cd_profissional_p,a.cd_medico)))
	and	a.nr_atendimento	= nr_atendimento_p
	order by	a.dt_diagnostico,
		b.dt_atualizacao;		

cursor	c02 is
	select	cd_doenca_cid
	from	can_loco_regional
	where	cd_pessoa_fisica	= obter_pessoa_atendimento(nr_atendimento_p,'C')
	order by dt_avaliacao;	
	
begin

begin
select	nvl(max(cd_estabelecimento),0)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;
exception
when others then
	cd_estabelecimento_w := 0;
end;

ie_restr_medico_w := nvl(obter_valor_param_usuario(1125, 156, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w),'N');

open c02;
loop
fetch c02 into	
	cd_cid_w;
exit when c02%notfound;
end loop;
close c02;

open c01;
loop
fetch c01 into	
	cd_cid_w;
exit when c01%notfound;
end loop;
close c01;

cd_cid_p	:= cd_cid_w;

end sus_atualiza_cid_proc;
/
