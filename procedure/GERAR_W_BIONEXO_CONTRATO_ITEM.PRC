create or replace
procedure gerar_w_bionexo_contrato_item(
					nr_seq_contrato_p	varchar2,        
					cd_material_p		varchar2,            
					qt_material_p		varchar2,            
					vl_material_p		varchar2,            
					ds_embalagem_p		varchar2,           
					qt_embalagem_p		varchar2,                        
					nm_usuario_p		varchar2) is
				

nr_sequencia_w	w_bionexo_contrato_item.nr_sequencia%type;

			
begin
--apapandini OS642692
select	w_bionexo_contrato_item_seq.nextval
into	nr_sequencia_w
from	dual;


insert into w_bionexo_contrato_item(	
			nr_sequencia,    
			nr_seq_contrato,        
			cd_material,            
			qt_material,            
			vl_material,            
			ds_embalagem,           
			qt_embalagem,        
			nm_usuario,        
			nm_usuario_nrec,          
			dt_atualizacao,         
			dt_atualizacao_nrec )
values(	nr_sequencia_w,                      
	to_number(nr_seq_contrato_p),        
	to_number(cd_material_p),            
	to_number(REPLACE(REPLACE(qt_material_p,',',''),'.',',')),            
	to_number(REPLACE(REPLACE(vl_material_p,',',''),'.',',')),        
	ds_embalagem_p,           
	null,
	nm_usuario_p,              
	nm_usuario_p, 
	sysdate,          
	sysdate);
	
commit;

end gerar_w_bionexo_contrato_item;
/
