create or replace
procedure pls_consistir_regra_meta
			(	nr_seq_meta_p	in	Number,
				nr_seq_regra_p	in	Number,
				vl_peso_p	in	Number,
				qt_metas_p	in	number,
				nm_usuario_p	in	Varchar2,
				ds_erro_p	out	Varchar2) is 

vl_peso_w		pls_vendedor_meta_regra.vl_peso%type;
vl_peso_permitido_w	pls_vendedor_meta_regra.vl_peso%type;
qt_composicao_w		pls_integer;

begin

if	(vl_peso_p > 10) then
	ds_erro_p := wheb_mensagem_pck.get_texto(280203);
	-- Mensagem: O peso n�o pode ser maior que 10!
else
	select	count(1)
	into	qt_composicao_w
	from	pls_vendedor_meta_regra
	where	nr_seq_meta = nr_seq_meta_p
	and	nr_sequencia <> nr_seq_regra_p;
	
	if	(qt_composicao_w > 0) then
		if	(vl_peso_p = 0) then
			ds_erro_p := wheb_mensagem_pck.get_texto(391699);			
			-- Mensagem: Favor informar o valor do peso.	
		end if;
	
		select	sum(vl_peso)
		into	vl_peso_w
		from	pls_vendedor_meta_regra
		where	nr_seq_meta = nr_seq_meta_p
		and	nr_sequencia <> nr_seq_regra_p;
			
		if	((nvl(vl_peso_w,0) + vl_peso_p) > 10) then
			vl_peso_permitido_w := 10 - vl_peso_w;
			ds_erro_p := wheb_mensagem_pck.get_texto(280204, 'VL_PESO_P=' || vl_peso_p || ';VL_PESO_PERMITIDO_P=' || vl_peso_permitido_w);
			-- Mensagem: N�o � poss�vel cadastrar peso VL_PESO_P para a regra. O max�mo permitido de acordo com as outras regras � VL_PESO_PERMITIDO_P.
		elsif	(nvl(vl_peso_w,0) = 0) and (qt_composicao_w = 1) then
			update	pls_vendedor_meta_regra
			set	vl_peso = 10 - vl_peso_p
			where	nr_seq_meta = nr_seq_meta_p
			and	nr_sequencia <> nr_seq_regra_p;
		end if;
	else
		if	(vl_peso_p = 0) then
			update	pls_vendedor_meta_regra
			set	vl_peso = 10
			where	nr_sequencia = nr_seq_regra_p;		
		end if;
	end if;		
	
	if	(ds_erro_p is null) then
		-- aaschlote 10/03/2011 OS - 294419
		pls_consistir_regra_qt_metas(nr_seq_meta_p,nr_seq_regra_p,qt_metas_p,ds_erro_p);
	end if;	
end if;	

commit;

end pls_consistir_regra_meta;
/