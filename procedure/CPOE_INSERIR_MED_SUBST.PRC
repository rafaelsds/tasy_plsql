create or replace
procedure cpoe_inserir_med_subst(	nr_prescricao_p			prescr_medica.nr_prescricao%type,
									nr_seq_material_p		prescr_material.nr_sequencia%type,
									nm_usuario_p			usuario.nm_usuario%type,
									cd_funcao_origem_p		funcao.cd_funcao%type ) is
					
nr_seq_material_w				prescr_material.nr_sequencia%type;
cd_material_w					prescr_material.cd_material%type;
cd_unidade_medida_dose_w		prescr_material.cd_unidade_medida_dose%type;
qt_dose_w						prescr_material.qt_dose%type;
ie_acm_w						prescr_material.ie_acm%type;
ie_se_necessario_w				prescr_material.ie_se_necessario%type;
ie_urgencia_w					prescr_material.ie_urgencia%type;
ds_horarios_w					prescr_material.ds_horarios%type;
hr_prim_horario_w				prescr_material.hr_prim_horario%type;
cd_intervalo_w					prescr_material.cd_intervalo%type;
ie_lado_w						prescr_material.ie_lado%type;
ie_via_aplicacao_w				prescr_material.ie_via_aplicacao%type;
nr_dia_util_w					prescr_material.nr_dia_util%type;
qt_total_dias_lib_w				prescr_material.qt_total_dias_lib%type;
ds_diluicao_edit_w				prescr_material.ds_diluicao_edit%type;
qt_hora_aplicacao_w				prescr_material.qt_hora_aplicacao%type;
qt_min_aplicacao_w				prescr_material.qt_min_aplicacao%type;
qt_solucao_w					prescr_material.qt_solucao%type;
ie_aplic_bolus_w				prescr_material.ie_aplic_bolus%type;
ie_aplic_lenta_w				prescr_material.ie_aplic_lenta%type;
qt_dia_prim_hor_w				prescr_material.qt_dia_prim_hor%type;
ds_justificativa_w				prescr_material.ds_justificativa%type;
qt_dias_solicitado_w			prescr_material.qt_dias_solicitado%type;
qt_dias_liberado_w				prescr_material.qt_dias_liberado%type;
ie_objetivo_w					prescr_material.ie_objetivo%type;
cd_microorganismo_cih_w			prescr_material.cd_microorganismo_cih%type;
cd_amostra_cih_w				prescr_material.cd_amostra_cih%type;
ie_origem_infeccao_w			prescr_material.ie_origem_infeccao%type;
cd_topografia_cih_w				prescr_material.cd_topografia_cih%type;
ie_indicacao_w					prescr_material.ie_indicacao%type;
ie_uso_antimicrobiano_w			prescr_material.ie_uso_antimicrobiano%type;
ie_medicacao_paciente_w			prescr_material.ie_medicacao_paciente%type;
ie_fator_correcao_w				prescr_material.ie_fator_correcao%type;
ie_bomba_infusao_w				prescr_material.ie_bomba_infusao%type;
ds_observacao_w					prescr_material.ds_observacao%type;
nr_ocorrencia_w					prescr_material.nr_ocorrencia%type;
nr_agrupamento_w				prescr_material.nr_agrupamento%type;
nr_agrupamento_ww				prescr_material.nr_agrupamento%type := 0;
cd_perfil_ativo_w				prescr_material.cd_perfil_ativo%type;
ie_dose_espec_agora_w			prescr_material.ie_dose_espec_agora%type;
qt_dose_especial_w				prescr_material.qt_dose_especial%type;
hr_dose_especial_w				prescr_material.hr_dose_especial%type;
qt_min_aplic_dose_esp_w			prescr_material.qt_min_aplic_dose_esp%type;
nr_seq_mat_cpoe_w				prescr_material.nr_seq_mat_cpoe%type;
ds_dose_diferenciada_w			prescr_material.ds_dose_diferenciada%type;
ie_item_superior_w          	prescr_material.ie_item_superior%type;
dt_inicio_prescr_w				prescr_medica.dt_inicio_prescr%type;
dt_validade_prescr_w			prescr_medica.dt_validade_prescr%type;
nr_atendimento_w				prescr_medica.nr_atendimento%type;
cd_pessoa_fisica_w				prescr_medica.cd_pessoa_fisica%type;
nr_sequencia_w					cpoe_material.nr_sequencia%type;
ie_administracao_w				cpoe_material.ie_administracao%type;
dt_inicio_w						cpoe_material.dt_inicio%type;
ie_duracao_w					cpoe_material.ie_duracao%type;
dt_fim_w						cpoe_material.dt_fim%type;
cd_mat_comp1_w					cpoe_material.cd_mat_comp1%type;
qt_dose_comp1_w					cpoe_material.qt_dose_comp1%type;
cd_unid_med_dose_comp1_w		cpoe_material.cd_unid_med_dose_comp1%type;
ds_dose_diferenciada_comp1_w	cpoe_material.ds_dose_diferenciada_comp1%type;
qt_dose_adic_comp1_w			cpoe_material.qt_dose_adic_comp1%type;
cd_mat_comp2_w					cpoe_material.cd_mat_comp2%type;
qt_dose_comp2_w					cpoe_material.qt_dose_comp2%type;
cd_unid_med_dose_comp2_w		cpoe_material.cd_unid_med_dose_comp2%type;
ds_dose_diferenciada_comp2_w	cpoe_material.ds_dose_diferenciada_comp2%type;
qt_dose_adic_comp2_w			cpoe_material.qt_dose_adic_comp2%type;
cd_mat_comp3_w					cpoe_material.cd_mat_comp3%type;
qt_dose_comp3_w					cpoe_material.qt_dose_comp3%type;
cd_unid_med_dose_comp3_w		cpoe_material.cd_unid_med_dose_comp3%type;
ds_dose_diferenciada_comp3_w	cpoe_material.ds_dose_diferenciada_comp3%type;
qt_dose_adic_comp3_w			cpoe_material.qt_dose_adic_comp3%type;
cd_mat_comp4_w					cpoe_material.cd_mat_comp4%type;
qt_dose_comp4_w					cpoe_material.qt_dose_comp4%type;
cd_unid_med_dose_comp4_w		cpoe_material.cd_unid_med_dose_comp4%type;
ds_dose_diferenciada_comp4_w	cpoe_material.ds_dose_diferenciada_comp4%type;
qt_dose_adic_comp4_w			cpoe_material.qt_dose_adic_comp4%type;
cd_mat_comp5_w					cpoe_material.cd_mat_comp5%type;
qt_dose_comp5_w					cpoe_material.qt_dose_comp5%type;
cd_unid_med_dose_comp5_w		cpoe_material.cd_unid_med_dose_comp5%type;
ds_dose_diferenciada_comp5_w	cpoe_material.ds_dose_diferenciada_comp5%type;
qt_dose_adic_comp5_w			cpoe_material.qt_dose_adic_comp5%type;
cd_mat_comp6_w					cpoe_material.cd_mat_comp6%type;
qt_dose_comp6_w					cpoe_material.qt_dose_comp6%type;
cd_unid_med_dose_comp6_w		cpoe_material.cd_unid_med_dose_comp6%type;
ds_dose_diferenciada_comp6_w	cpoe_material.ds_dose_diferenciada_comp6%type;
qt_dose_adic_comp6_w			cpoe_material.qt_dose_adic_comp6%type;
dt_prox_geracao_w				cpoe_material.dt_prox_geracao%type;
qt_composto_w					number(15);
cd_material_aux_w				prescr_material.cd_material%type;
qt_dose_aux_w					prescr_material.qt_dose%type;
cd_unidade_medida_aux_w			prescr_material.cd_unidade_medida_dose%type;
qt_solucao_aux_w				prescr_material.qt_solucao%type;
ie_agrupador_aux_w				prescr_material.ie_agrupador%type;
ie_rep_cpoe_w					parametro_medico.ie_rep_cpoe%type;
ds_erro_w						varchar2(2000);
ie_tipo_informacao_w			varchar2(10);
ie_item_superior_ww				prescr_material.ie_item_superior%type;
nr_agrupamento_www				prescr_material.nr_agrupamento%type;
nr_seq_material_ww				prescr_material.nr_sequencia%type;
nr_seq_mat_cpoe_ww				prescr_material.nr_seq_mat_cpoe%type;
cd_unidade_medida_w				prescr_material.cd_unidade_medida_dose%type;
cd_mat_subs_w					prescr_material.cd_material%type;
qt_dose_subs_w					prescr_material.qt_dose%type;
cd_unidade_med_subs_w			prescr_material.cd_unidade_medida_dose%type;
ds_dose_dif_subs_w				prescr_material.ds_dose_diferenciada%type;
					
cursor c01 is
select	nr_sequencia,
		cd_material,
		cd_unidade_medida_dose,
		qt_dose,
		ds_dose_diferenciada,
		nvl(ie_acm,'N'),
		nvl(ie_se_necessario,'N'),
		nvl(ie_urgencia,'N'),
		ds_horarios,
		hr_prim_horario,
		cd_intervalo,
		ie_lado,
		ie_via_aplicacao,
		nr_dia_util,
		nr_ocorrencia,
		qt_total_dias_lib,
		ds_diluicao_edit,
		qt_hora_aplicacao,
		qt_min_aplicacao,
		qt_solucao,
		nvl(ie_aplic_bolus,'N'),
		nvl(ie_aplic_lenta,'N'),
		nvl(qt_dia_prim_hor,0),
		ds_justificativa,
		qt_dias_solicitado,
		qt_dias_liberado,
		ie_objetivo,
		cd_microorganismo_cih,
		cd_amostra_cih,
		ie_origem_infeccao,
		cd_topografia_cih,
		ie_indicacao,
		nvl(ie_uso_antimicrobiano,'N'),
		nvl(ie_medicacao_paciente,'N'),
		nvl(ie_fator_correcao,'N'),
		ie_bomba_infusao,
		ds_observacao,
		nr_agrupamento,
		cd_perfil_ativo,
		nvl(ie_dose_espec_agora,'N'),
		qt_dose_especial,
		hr_dose_especial,
		qt_min_aplic_dose_esp,
    	ie_item_superior    
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and		nr_sequencia = nr_seq_material_ww
and		ie_agrupador = 1;

cursor c02 is
select	cd_material,
		qt_dose,
		cd_unidade_medida_dose,
		qt_solucao,
		ie_agrupador
from	prescr_material
where	nr_sequencia_diluicao = nr_seq_material_ww
and		nr_prescricao = nr_prescricao_p
and		ie_agrupador in (3,7,9);

cursor c03 is
select	a.cd_material,
		a.qt_dose,
		a.cd_unidade_medida_dose,
		a.ds_dose_diferenciada
from	prescr_material a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_sequencia = nr_seq_material_p
and		a.ie_agrupador = 1
and		nvl(a.ie_suspenso,'N') = 'N'
and		not exists( select	1 
					from	cpoe_material_v b
					where	b.nr_sequencia = a.nr_seq_mat_cpoe
					and		b.cd_material =  a.cd_material
					and		nvl(b.qt_dose,0) = nvl(a.qt_dose,0)
					and		nvl(b.cd_unidade_medida,'XPTO') = nvl(a.cd_unidade_medida_dose,'XPTO')
					and		nvl(b.ds_dose_diferenciada,'XPTO') = nvl(a.ds_dose_diferenciada,'XPTO'));

begin

select	max(nr_atendimento),
		max(cd_pessoa_fisica)
into	nr_atendimento_w,
		cd_pessoa_fisica_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	nvl(max(ie_rep_cpoe),'N')
into	ie_rep_cpoe_w
from	parametro_medico;

if (ie_rep_cpoe_w = 'N') then
	return;
end if;

nr_seq_material_ww := nr_seq_material_p;

select 	nvl(max(a.ie_item_superior),'N'),
		max(a.nr_agrupamento)
into	ie_item_superior_ww,
		nr_agrupamento_www
from	prescr_material a
where 	a.nr_prescricao = nr_prescricao_p
and		a.nr_sequencia = nr_seq_material_p
and		exists (select 	1
				from 	prescr_material b
				where	b.nr_prescricao = nr_prescricao_p
				and		a.nr_agrupamento = b.nr_agrupamento
				and		a.nr_sequencia <> b.nr_sequencia
				and 	nvl(b.ie_item_superior,'N') = 'S'
				and		b.ie_agrupador = 1);

if (ie_item_superior_ww = 'N') and (nr_agrupamento_www is not null) then
	select	max(nr_sequencia),
			max(nr_seq_mat_cpoe)
	into	nr_seq_material_ww,
			nr_seq_mat_cpoe_ww	
	from	prescr_material
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_agrupamento = nr_agrupamento_www
	and 	nvl(ie_item_superior,'N') = 'S'
	and 	ie_agrupador = 1;
end if;

open c01;
loop
fetch c01 into	nr_seq_material_w,
				cd_material_w,
				cd_unidade_medida_dose_w,
				qt_dose_w,
				ds_dose_diferenciada_w,
				ie_acm_w,
				ie_se_necessario_w,
				ie_urgencia_w,
				ds_horarios_w,
				hr_prim_horario_w,
				cd_intervalo_w,
				ie_lado_w,
				ie_via_aplicacao_w,
				nr_dia_util_w,
				nr_ocorrencia_w,
				qt_total_dias_lib_w,
				ds_diluicao_edit_w,
				qt_hora_aplicacao_w,
				qt_min_aplicacao_w,
				qt_solucao_w,
				ie_aplic_bolus_w,
				ie_aplic_lenta_w,
				qt_dia_prim_hor_w,
				ds_justificativa_w,
				qt_dias_solicitado_w,
				qt_dias_liberado_w,
				ie_objetivo_w,
				cd_microorganismo_cih_w,
				cd_amostra_cih_w,
				ie_origem_infeccao_w,
				cd_topografia_cih_w,
				ie_indicacao_w,
				ie_uso_antimicrobiano_w,
				ie_medicacao_paciente_w,
				ie_fator_correcao_w,
				ie_bomba_infusao_w,
				ds_observacao_w,
				nr_agrupamento_w,
				cd_perfil_ativo_w,
				ie_dose_espec_agora_w,
				qt_dose_especial_w,
				hr_dose_especial_w,
				qt_min_aplic_dose_esp_w,
        		ie_item_superior_w;
exit when c01%notfound;
	begin
		
	if (nr_agrupamento_w <> nr_agrupamento_ww) then
	
		select	nvl(nr_seq_mat_cpoe_ww,max(nr_seq_mat_cpoe))
		into	nr_seq_mat_cpoe_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_substituto = nr_seq_material_w;
		
		select 	max(ie_administracao),
				max(ie_duracao),
				max(dt_inicio),
				max(dt_fim),
				max(cd_mat_comp1),
				max(qt_dose_comp1),
				max(cd_unid_med_dose_comp1),
				max(ds_dose_diferenciada_comp1),
				max(qt_dose_adic_comp1),
				max(cd_mat_comp2),
				max(qt_dose_comp2),
				max(cd_unid_med_dose_comp2),
				max(ds_dose_diferenciada_comp2),
				max(qt_dose_adic_comp2),
				max(cd_mat_comp3),
				max(qt_dose_comp3),
				max(cd_unid_med_dose_comp3),
				max(ds_dose_diferenciada_comp3),
				max(qt_dose_adic_comp3),
				max(cd_mat_comp4),
				max(qt_dose_comp4),
				max(cd_unid_med_dose_comp4),
				max(ds_dose_diferenciada_comp4),
				max(qt_dose_adic_comp4),
				max(cd_mat_comp5),
				max(qt_dose_comp5),
				max(cd_unid_med_dose_comp5),
				max(ds_dose_diferenciada_comp5),
				max(qt_dose_adic_comp5),
				max(cd_mat_comp6),
				max(qt_dose_comp6),
				max(cd_unid_med_dose_comp6),
				max(ds_dose_diferenciada_comp6),
				max(qt_dose_adic_comp6),
				max(dt_prox_geracao)
		into	ie_administracao_w,
				ie_duracao_w,
				dt_inicio_w,
				dt_fim_w,
				cd_mat_comp1_w,
				qt_dose_comp1_w,
				cd_unid_med_dose_comp1_w,
				ds_dose_diferenciada_comp1_w,
				qt_dose_adic_comp1_w,
				cd_mat_comp2_w,
				qt_dose_comp2_w,
				cd_unid_med_dose_comp2_w,
				ds_dose_diferenciada_comp2_w,
				qt_dose_adic_comp2_w,
				cd_mat_comp3_w,
				qt_dose_comp3_w,
				cd_unid_med_dose_comp3_w,
				ds_dose_diferenciada_comp3_w,
				qt_dose_adic_comp3_w,
				cd_mat_comp4_w,
				qt_dose_comp4_w,
				cd_unid_med_dose_comp4_w,
				ds_dose_diferenciada_comp4_w,
				qt_dose_adic_comp4_w,
				cd_mat_comp5_w,
				qt_dose_comp5_w,
				cd_unid_med_dose_comp5_w,
				ds_dose_diferenciada_comp5_w,
				qt_dose_adic_comp5_w,
				cd_mat_comp6_w,
				qt_dose_comp6_w,
				cd_unid_med_dose_comp6_w,
				ds_dose_diferenciada_comp6_w,
				qt_dose_adic_comp6_w,
				dt_prox_geracao_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_mat_cpoe_w;
	
		nr_agrupamento_ww := nr_agrupamento_w;
		qt_composto_w := 1;
		
		if (ie_acm_w = 'S') then
			ie_administracao_w := 'C';
			hr_prim_horario_w := '';
			ds_horarios_w := '';
		elsif (ie_se_necessario_w = 'S') then
			ie_administracao_w := 'N';
			hr_prim_horario_w := '';
			ds_horarios_w := '';
		else
			-- Clear out the text inside the line and adds the sufix of ":00" as needed, fitting it 		
			ds_horarios_w := replace(Padroniza_horario_prescr(ds_horarios_w,null),'A','');
		end if;
		
		select	cpoe_material_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		if (cd_unidade_medida_dose_w is null) then
			select max(cd_unidade_medida_consumo)
			into	cd_unidade_medida_dose_w
			from	material
			where	cd_material = cd_material_w;
		end if;
		
		insert into cpoe_material(
					nr_sequencia,
					nr_atendimento,
					ie_controle_tempo,
					ie_administracao,
					ie_duracao,
					dt_inicio,
					dt_fim,
					dt_liberacao,
					cd_material,
					cd_unidade_medida,
					qt_dose,
					ie_acm,
					ie_se_necessario,
					ie_urgencia,
					ds_horarios,
					hr_prim_horario,
					cd_intervalo,
					ie_lado,
					ie_via_aplicacao,
					nr_dia_util,
					nr_ocorrencia,
					qt_total_dias_lib,
					ds_orientacao_preparo,
					qt_hora_aplicacao,
					qt_min_aplicacao,
					qt_solucao,
					ie_aplic_bolus,
					ie_aplic_lenta,
					ds_justificativa,
					qt_dias_solicitado,
					qt_dias_liberado,
					ie_objetivo,
					cd_microorganismo_cih,
					cd_amostra_cih,
					ie_origem_infeccao,
					cd_topografia_cih,
					ie_indicacao,
					ie_uso_antimicrobiano,
					ie_medicacao_paciente,
					ie_fator_correcao,
					ie_bomba_infusao,
					ds_observacao,
					cd_mat_comp1,
					qt_dose_comp1,
					cd_unid_med_dose_comp1,
					ds_dose_diferenciada_comp1,
					qt_dose_adic_comp1,
					cd_mat_comp2,
					qt_dose_comp2,
					cd_unid_med_dose_comp2,
					ds_dose_diferenciada_comp2,
					qt_dose_adic_comp2,
					cd_mat_comp3,
					qt_dose_comp3,
					cd_unid_med_dose_comp3,
					ds_dose_diferenciada_comp3,
					qt_dose_adic_comp3,
					cd_mat_comp4,
					qt_dose_comp4,
					cd_unid_med_dose_comp4,
					ds_dose_diferenciada_comp4,
					qt_dose_adic_comp4,
					cd_mat_comp5,
					qt_dose_comp5,
					cd_unid_med_dose_comp5,
					ds_dose_diferenciada_comp5,
					qt_dose_adic_comp5,
					cd_mat_comp6,
					qt_dose_comp6,
					cd_unid_med_dose_comp6,
					ds_dose_diferenciada_comp6,
					qt_dose_adic_comp6,
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec,
					cd_perfil_ativo,
					qt_tempo_aplicacao,
					ds_dose_diferenciada,
					cd_pessoa_fisica,
					ie_material,
					cd_funcao_origem,
					dt_prox_geracao,
					ie_antibiotico)
				values(
					nr_sequencia_w,
					nr_atendimento_w,
					'N',
					ie_administracao_w,
					ie_duracao_w,
					dt_inicio_w,
					dt_fim_w,
					sysdate,
					cd_material_w,
					cd_unidade_medida_dose_w,
					qt_dose_w,
					ie_acm_w,
					ie_se_necessario_w,
					decode(ie_urgencia_w,'S','0',null),
					ds_horarios_w,
					hr_prim_horario_w,
					cd_intervalo_w,
					ie_lado_w,
					ie_via_aplicacao_w,
					nr_dia_util_w,
					nr_ocorrencia_w,
					qt_total_dias_lib_w,
					ds_diluicao_edit_w,
					qt_hora_aplicacao_w,
					qt_min_aplicacao_w,
					qt_solucao_w,
					ie_aplic_bolus_w,
					ie_aplic_lenta_w,
					ds_justificativa_w,
					qt_dias_solicitado_w,
					qt_dias_liberado_w,
					ie_objetivo_w,
					cd_microorganismo_cih_w,
					cd_amostra_cih_w,
					ie_origem_infeccao_w,
					cd_topografia_cih_w,
					ie_indicacao_w,
					ie_uso_antimicrobiano_w,
					ie_medicacao_paciente_w,
					ie_fator_correcao_w,
					ie_bomba_infusao_w,
					ds_observacao_w,
					cd_mat_comp1_w,
					qt_dose_comp1_w,
					cd_unid_med_dose_comp1_w,
					ds_dose_diferenciada_comp1_w,
					qt_dose_adic_comp1_w,
					cd_mat_comp2_w,
					qt_dose_comp2_w,
					cd_unid_med_dose_comp2_w,
					ds_dose_diferenciada_comp2_w,
					qt_dose_adic_comp2_w,
					cd_mat_comp3_w,
					qt_dose_comp3_w,
					cd_unid_med_dose_comp3_w,
					ds_dose_diferenciada_comp3_w,
					qt_dose_adic_comp3_w,
					cd_mat_comp4_w,
					qt_dose_comp4_w,
					cd_unid_med_dose_comp4_w,
					ds_dose_diferenciada_comp4_w,
					qt_dose_adic_comp4_w,
					cd_mat_comp5_w,
					qt_dose_comp5_w,
					cd_unid_med_dose_comp5_w,
					ds_dose_diferenciada_comp5_w,
					qt_dose_adic_comp5_w,
					cd_mat_comp6_w,
					qt_dose_comp6_w,
					cd_unid_med_dose_comp6_w,
					ds_dose_diferenciada_comp6_w,
					qt_dose_adic_comp6_w,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					sysdate,
					cd_perfil_ativo_w,
					24,
					ds_dose_diferenciada_w,
					cd_pessoa_fisica_w,
					'N',
					cd_funcao_origem_p,
					dt_prox_geracao_w,
					nvl(obter_se_mat_ctrl_antibiotico(cd_material_w),'N'));
					
		if	(ie_dose_espec_agora_w = 'S') then
			update	cpoe_material
			set		ie_dose_adicional	= ie_dose_espec_agora_w,
					qt_dose_adicional	= qt_dose_especial_w,
					hr_min_aplic_adic	= obter_horas_minutos(qt_min_aplic_dose_esp_w),
					dt_adm_adicional	= to_date(to_char(nvl(dt_inicio_w,sysdate), 'dd/mm/yyyy') ||' '|| hr_dose_especial_w,'dd/mm/yyyy hh24:mi')
			where	nr_sequencia		= nr_sequencia_w;					
		end if;
					
		cpoe_rep_gerar_dil_redil_recon( nr_prescricao_p, nr_sequencia_w, nr_seq_material_w);
		
		update	prescr_material
		set		nr_seq_mat_cpoe = nr_sequencia_w
		where	nr_sequencia = nr_seq_material_w
		and		nr_prescricao = nr_prescricao_p;
		
		if (nvl(ie_item_superior_w, 'N') = 'S') then
			update	prescr_material
			set		nr_seq_mat_cpoe = nr_sequencia_w
			where	nr_prescricao = nr_prescricao_p
			and   nr_sequencia <> nr_seq_material_w
			and   nr_agrupamento = nr_agrupamento_w
			and	  nvl(ie_item_superior, 'N') = 'N'      
			and   dt_suspensao is null
			and   nr_seq_substituto is null;      
		end if;
		
		if	(ie_item_superior_ww = 'N') and
			(nr_agrupamento_www is not null) and
			(nr_seq_material_ww <> nr_seq_material_p) then
			begin
			
				select	max(nr_seq_mat_cpoe)
				into	nr_seq_mat_cpoe_w
				from	prescr_material
				where	nr_prescricao = nr_prescricao_p
				and		nr_seq_substituto = nr_seq_material_p;
				
				if	(nvl(nr_seq_mat_cpoe_w,0) > 0) then
					begin
						open c03;
						loop
						fetch c03 into	cd_material_w,
										qt_dose_w,
										cd_unidade_medida_w,
										ds_dose_diferenciada_w;
						exit when c03%notfound;
							begin
							
							select	max(a.cd_material),
									max(a.qt_dose),
									max(a.cd_unidade_medida_dose),
									max(a.ds_dose_diferenciada)
							into	cd_mat_subs_w,
									qt_dose_subs_w,
									cd_unidade_med_subs_w,
									ds_dose_dif_subs_w
							from	prescr_material a
							where 	nr_prescricao = nr_prescricao_p
							and		nr_seq_substituto = nr_seq_material_p;
							
							select	max(a.ie_tipo_item)
							into	ie_tipo_informacao_w
							from	cpoe_material_v a
							where	a.nr_sequencia = nr_sequencia_w
							and 	a.cd_material = cd_mat_subs_w
							and		nvl(a.qt_dose,0) = nvl(qt_dose_subs_w,0)
							and		nvl(a.cd_unidade_medida,'XPTO') = nvl(cd_unidade_med_subs_w,'XPTO')
							and		nvl(a.ds_dose_diferenciada,'XPTO') = nvl(ds_dose_dif_subs_w,'XPTO');
												
							if (ie_tipo_informacao_w = 'MCOMP1') then
								update	cpoe_material
								set		cd_mat_comp1 = cd_material_w,
										qt_dose_comp1 = qt_dose_w,
										cd_unid_med_dose_comp1 = cd_unidade_medida_w,
										ds_dose_diferenciada_comp1 = ds_dose_diferenciada_w
								where	nr_sequencia = nr_sequencia_w;
							elsif (ie_tipo_informacao_w = 'MCOMP2') then
								update	cpoe_material
								set		cd_mat_comp2 = cd_material_w,
										qt_dose_comp2 = qt_dose_w,
										cd_unid_med_dose_comp2 = cd_unidade_medida_w,
										ds_dose_diferenciada_comp2 = ds_dose_diferenciada_w
								where	nr_sequencia = nr_sequencia_w;
							elsif (ie_tipo_informacao_w = 'MCOMP3') then
								update	cpoe_material
								set		cd_mat_comp3 = cd_material_w,
										qt_dose_comp3 = qt_dose_w,
										cd_unid_med_dose_comp3 = cd_unidade_medida_w,
										ds_dose_diferenciada_comp3 = ds_dose_diferenciada_w
								where	nr_sequencia = nr_sequencia_w;	
							elsif (ie_tipo_informacao_w = 'MCOMP4') then
								update	cpoe_material
								set		cd_mat_comp4 = cd_material_w,
										qt_dose_comp4 = qt_dose_w,
										cd_unid_med_dose_comp4 = cd_unidade_medida_w,
										ds_dose_diferenciada_comp4 = ds_dose_diferenciada_w
								where	nr_sequencia = nr_sequencia_w;	
							elsif (ie_tipo_informacao_w = 'MCOMP5') then
								update	cpoe_material
								set		cd_mat_comp5 = cd_material_w,
										qt_dose_comp5 = qt_dose_w,
										cd_unid_med_dose_comp5 = cd_unidade_medida_w,
										ds_dose_diferenciada_comp5 = ds_dose_diferenciada_w
								where	nr_sequencia = nr_sequencia_w;	
							elsif (ie_tipo_informacao_w = 'MCOMP6') then
								update	cpoe_material
								set		cd_mat_comp6 = cd_material_w,
										qt_dose_comp6 = qt_dose_w,
										cd_unid_med_dose_comp6 = cd_unidade_medida_w,
										ds_dose_diferenciada_comp6 = ds_dose_diferenciada_w
								where	nr_sequencia = nr_sequencia_w;	
							end if;
							end;
						end loop;
						close c03;
					end;
				end if;
			end;
		end if;  
  	
		open c02;
		loop
		fetch c02 into	cd_material_aux_w,
						qt_dose_aux_w,
						cd_unidade_medida_aux_w,
						qt_solucao_aux_w,
						ie_agrupador_aux_w;
		exit when c02%notfound;
			begin
			
			if (ie_agrupador_aux_w = 2) then
				update	cpoe_material
				set		cd_mat_dil 				= cd_material_aux_w,
						qt_dose_dil 			= qt_dose_aux_w,
						cd_unid_med_dose_dil	= cd_unidade_medida_aux_w,
						qt_solucao_dil			= qt_solucao_aux_w
				where	nr_sequencia			= nr_sequencia_w;					
			elsif (ie_agrupador_aux_w = 7) then
				update	cpoe_material
				set		cd_mat_red 				= cd_material_aux_w,
						qt_dose_red 			= qt_dose_aux_w,
						cd_unid_med_dose_red	= cd_unidade_medida_aux_w,
						qt_solucao_red			= qt_solucao_aux_w
				where	nr_sequencia			= nr_sequencia_w;
			elsif (ie_agrupador_aux_w = 9) then
				update	cpoe_material
				set		cd_mat_recons 				= cd_material_aux_w,
						qt_dose_recons 			= qt_dose_aux_w,
						cd_unid_med_dose_recons	= cd_unidade_medida_aux_w
				where	nr_sequencia			= nr_sequencia_w;	
			end if;
			
			end;
		end loop;
		close c02;
	end if;
	end;
end loop;
close c01;

commit;

exception when others then
	ds_erro_w := substr(to_char(sqlerrm),1,2000);

	gravar_log_tasy(10007,
					substr('CPOE_INSERIR_MED_SUBST ERRO 573: '||ds_erro_w 	
										||' STACK: ' || dbms_utility.format_call_stack
										||' nr_prescricao_p: '||nr_prescricao_p
										||' nr_seq_material_p: '||nr_seq_material_p
									    ||' nm_usuario_p: '|| nm_usuario_p
										||' cd_funcao_origem_p: ' || cd_funcao_origem_p,1,2000), nm_usuario_p);	

end cpoe_inserir_med_subst;
/
