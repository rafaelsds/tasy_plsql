create or replace
procedure med_localizar_texto_Evolucao( 	nr_seq_cliente_p  	Number,
					texto_p		Varchar2,
					sequencias_p out 	Varchar2) is 

nr_sequencia_w 	Number(10);
ds_evolucao_w 	long;
sequencias_w 	Varchar2(200)	:= '';
ie_achou_w	Varchar2(1);


Cursor C01 is
	select	nr_sequencia,
		ds_evolucao
	from	med_evolucao
	where	nr_seq_cliente = nr_seq_cliente_p;
					
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ds_evolucao_w;
exit when C01%notfound;
	begin
	--Verificar se possui o texto
	obter_se_long_contem('med_evolucao', 'ds_evolucao', 'where nr_sequencia = :nr_sequencia','nr_sequencia='||nr_sequencia_w, texto_p, ie_achou_w);
	if( ie_achou_w	= 'S') then
		sequencias_w :=  nr_sequencia_w||','|| sequencias_w ;
	end if;
	end;
end loop;
close C01;

sequencias_p := sequencias_w;

end med_localizar_texto_Evolucao;
/
