create or replace
procedure consiste_agenda_cirurgiao_js(	cd_agenda_p                    number,  
                                          dt_agenda_p                    date,    
                                          hr_inicio_p                    	date,    
                                          nr_minuto_duracao_p            number,  
                                          nr_seq_agenda_p                number,				                  
                                          cd_pessoa_fisica_p             varchar2,                				                				                  
                                          cd_convenio_p                  number,                  
                                          cd_categoria_p                 varchar2,                
                                          cd_procedimento_p              number,                  
                                          ie_origem_proced_p             number,                  
                                          nr_seq_proc_interno_p          number,                  
                                          cd_medico_p                    varchar2,                
                                          ie_medico_p                    varchar2,                
                                          cd_plano_p                     varchar2,                								
                                          qt_tempo_p		                number,
                                          cd_estabelecimento_p           number,
                                          ds_erro_p		         out    varchar2,
                                          ie_equip_disp_p	   out    varchar2
				) is 

ds_erro_w		      varchar2(255);
ie_inconsistencia_w	varchar2(255);			
cd_retorno_w		   number(10,0);
ie_consist_js_w		varchar2(255) := '';
ie_anestesia_w		   varchar2(1);
			
begin

ds_erro_p := '';
ie_equip_disp_p := '';

select	max(nvl(ie_anestesia,'N'))
into	   ie_anestesia_w
from	   agenda_paciente
where 	nr_sequencia = nr_seq_agenda_p; 


Consistir_Duracao_Agenda_Pac(cd_agenda_p, dt_agenda_p, hr_inicio_p, nr_minuto_duracao_p, nr_seq_agenda_p,0,ds_erro_w);

if	(ds_erro_w is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(219056,'DS_ERRO_W=' || ds_erro_w);
end if;


consistir_proc_conv_agenda(cd_estabelecimento_p, cd_pessoa_fisica_p, hr_inicio_p, cd_agenda_p, cd_convenio_p, cd_categoria_p, cd_procedimento_p, ie_origem_proced_p, 
				nr_seq_proc_interno_p, cd_medico_p, ie_medico_p, cd_plano_p, ie_inconsistencia_w, ds_erro_w, null, cd_retorno_w, ie_consist_js_w,null,nr_minuto_duracao_p,null, ie_anestesia_w, null, null);

if 	(ie_inconsistencia_w is not null) then
      Wheb_mensagem_pck.exibir_mensagem_abort(219057,'IE_INCONSISTENCIA_W=' || ie_inconsistencia_w);
end if;	

consiste_qtd_agend_permitidos(nr_seq_agenda_p, ds_erro_p);	

ie_equip_disp_p := obter_se_equip_disponivel(nr_seq_agenda_p, nr_seq_proc_interno_p, cd_medico_p, qt_tempo_p);

if (cd_medico_p is not null) then
   obter_se_medico_disponivel(nr_seq_agenda_p, cd_medico_p);
end if;

end consiste_agenda_cirurgiao_js;
/