create or replace
procedure gerar_nom_rc_medicamento(nr_seq_cabecalho_p	number,
														 nm_usuario_p		varchar2) is
		
nr_atendimento_w			nom_rc_cabecalho.nr_atendimento%type;
nr_seq_episodio_w			nom_rc_cabecalho.nr_seq_episodio%type;
cd_estabelecimento_w		nom_rc_cabecalho.cd_estabelecimento%type;		

ds_medicamentos_w		nom_rc_medicamento.ds_medicamentos%type;	/*id_322*/
ds_observacao_w			nom_rc_medicamento.ds_observacao%type; /*id_323*/
ie_via_aplicacao_w			nom_rc_medicamento.ie_via_aplicacao%type; /*id_324*/
qt_dose_w			nom_rc_medicamento.qt_dose%type; /*id_326*/
dt_inicio_w			nom_rc_medicamento.dt_inicio%type; /*id_327*/
dt_final_w			nom_rc_medicamento.dt_final%type; /*id_328*/
cd_cat_medicamento_w		nom_rc_medicamento.cd_cat_medicamento%type; /*id_329*/
ds_medicamento_w			nom_rc_medicamento.ds_medicamento%type; /*id_330*/
ds_dose_w			nom_rc_medicamento.ds_dose%type; /*id_326*/
qt_registro_w			number(10)	:= 0;	
ds_html_w			varchar2(32000)	:= null;
ds_dose_desc_w		varchar2(255);


Cursor c_medicamento_adm is
select  a.nr_atendimento NR_ATENDIMENTO,
        min(c.dt_horario) DT_ADM,/*id_327*/
        max(c.dt_fim_horario) DT_FIM_ADM,/*id_328*/
        sum(d.qt_dose_adm) QT_DOSE, /*id_326*/
        b.CD_UNIDADE_MEDIDA ds_dose, 
        nvl(obter_via_aplicacao(b.ie_via_aplicacao,'C'),b.ie_via_aplicacao) ie_via_aplicacao, /*id_324*/
        obter_desc_via(nvl(obter_via_aplicacao(b.ie_via_aplicacao,'C'),b.ie_via_aplicacao)) ie_desc_via_aplicacao,
        b.ds_observacao ds_observacao, /*id_323*/
        x.cd_sistema_ant cd_cat_medicamento,/*id_329*/
		substr(x.ds_material,1,255) ds_medicamento, /*id_330*/
		obter_desc_unid_med(b.cd_unidade_medida) || '. Intervalo: ' || obter_desc_intervalo(b.cd_intervalo) ds_dose_desc
FROM    material x,
        prescr_mat_alteracao d,
        prescr_mat_hor c,
        prescr_material b,
        prescr_medica a
WHERE      d.nr_seq_horario 	= c.nr_sequencia
AND        c.nr_seq_material 	= b.nr_sequencia
AND        c.nr_prescricao 		= b.nr_prescricao
AND        b.nr_prescricao 		= a.nr_prescricao
AND        b.cd_material        = x.cd_material
AND 	   a.nr_atendimento in (select	x.nr_atendimento
								from	nom_rc_cabecalho x
								where	x.nr_sequencia = nr_seq_cabecalho_p
								and		x.nr_atendimento is not null
								union
								select	y.nr_atendimento
								from	atendimento_paciente y,
										nom_rc_cabecalho x
								where	x.nr_seq_episodio = y.nr_seq_episodio
								and		x.nr_sequencia = nr_seq_cabecalho_p)
AND 	   C.DT_SUSPENSAO IS NULL
AND	       d.ie_alteracao = 3
AND        NVL(c.ie_situacao,'A') = 'A'
AND        NVL(c.ie_adep,'S') = 'S'
AND        b.ie_agrupador in (1,2)
AND        NVL(a.ie_adep,'S') = 'S'
AND        NVL(a.dt_liberacao, a.dt_liberacao_medico) IS NOT NULL
AND        Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
AND	       x.cd_sistema_ant is not null
and    	   d.qt_dose_adm is not null
and        b.ie_via_aplicacao is not null
GROUP BY a.nr_atendimento, 
	  	 a.nr_prescricao,
		 x.ds_material,
		 d.cd_um_dose_adm,
		 b.IE_VIA_APLICACAO,
		 b.ds_observacao,
		 x.cd_sistema_ant,b.cd_material,
         b.cd_unidade_medida,
		 b.cd_intervalo
union
select	c.nr_atendimento nr_atendimento,
		min(b.dt_horario) DT_ADM,
		min(b.dt_fim_horario) dt_fim_adm,
		sum(b.qt_dose) QT_DOSE,
		a.CD_UNIDADE_MEDIDA ds_dose,
		nvl(obter_via_aplicacao(a.ie_via_aplicacao,'C'),a.ie_via_aplicacao) ie_via_aplicacao, /*id_324*/
		obter_desc_via(nvl(obter_via_aplicacao(a.ie_via_aplicacao,'C'),a.ie_via_aplicacao)) ie_desc_via_aplicacao,
		null ds_observacao, /*id_323*/
		x.cd_sistema_ant cd_cat_medicamento,/*id_329*/
		substr(x.ds_material,1,255) ds_medicamento, /*id_330*/
		obter_desc_unid_med(a.cd_unidade_medida) || '. Intervalo: ' || obter_desc_intervalo(a.cd_intervalo) ds_dose_desc
from	prescr_solucao a,
		prescr_mat_hor b,
		prescr_medica c,
		material x
where	c.nr_prescricao = a.nr_prescricao
and		c.nr_prescricao = b.nr_prescricao
and		a.nr_prescricao = b.nr_prescricao
and     b.cd_material = x.cd_material
and		a.nr_seq_solucao = b.nr_seq_solucao
and		c.nr_atendimento in (select	x.nr_atendimento
								from	nom_rc_cabecalho x
								where	x.nr_sequencia = nr_seq_cabecalho_p
								and		x.nr_atendimento is not null
								union
								select	y.nr_atendimento
								from	atendimento_paciente y,
										nom_rc_cabecalho x
								where	x.nr_seq_episodio = y.nr_seq_episodio
								and		x.nr_sequencia = nr_seq_cabecalho_p)
and		((b.nr_etapa_sol  <> ( select   nvl(max(x.nr_etapa_sol),0)
                              from     prescr_mat_hor x
                              where    x.nr_prescricao = a.nr_prescricao
                              and      x.nr_seq_solucao = a.nr_seq_solucao
                              and      x.ie_etapa_especial = 'S')) or
        (b.ie_etapa_especial = 'S'))
GROUP BY c.nr_atendimento, 
	  	 c.nr_prescricao,
         x.ds_material,
		 b.qt_dose,
		 a.IE_VIA_APLICACAO,
         x.cd_sistema_ant,b.cd_material,
         a.cd_unidade_medida,
		 a.cd_intervalo;

begin

delete from nom_rc_medicamento
where nr_seq_cabecalho = nr_seq_cabecalho_p;

select	a.nr_atendimento,
		a.nr_seq_episodio,
		a.cd_estabelecimento
into	nr_atendimento_w,
		nr_seq_episodio_w,
		cd_estabelecimento_w
from	nom_rc_cabecalho a
where	a.nr_sequencia	= nr_seq_cabecalho_p;

if	(nr_atendimento_w is not null) then
	null;
elsif	(nr_seq_episodio_w is not null) then
	select	min(nr_atendimento)
	into	nr_atendimento_w
	from	atendimento_paciente a
	where	a.nr_seq_episodio = nr_seq_episodio_w;
end if;

if	(nr_atendimento_w is not null) then

	ds_html_w	:= '<table class="wrichedit-table" width="100%" xmlns="urn:hl7-org:v3">';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || '<thead>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || '<tr>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<th>Medicamento</th>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<th>Via de administración</th>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<th>Dosis</th>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<th>Fecha de inicio</th>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<th>Fecha de fin</th>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<th>Obs. prescripción</th>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || '</tr>';
	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) ||  '</thead>';

	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || '<tbody>';

	for	r_c_medicamento in c_medicamento_adm LOOP

		qt_registro_w	:= qt_registro_w + 1;

		ds_observacao_w				:=		r_c_medicamento.ds_observacao;
		ie_via_aplicacao_w			:=		r_c_medicamento.ie_via_aplicacao;
		qt_dose_w				:=		r_c_medicamento.qt_dose;
		ds_dose_w				:=		r_c_medicamento.ds_dose;
		ds_dose_desc_w				:=		r_c_medicamento.ds_dose_desc;
		dt_inicio_w				:=		r_c_medicamento.dt_adm;
		dt_final_w				:=		r_c_medicamento.dt_fim_adm;
		cd_cat_medicamento_w	:=		r_c_medicamento.cd_cat_medicamento;
		ds_medicamento_w		:=		r_c_medicamento.ds_medicamento;

		insert into nom_rc_medicamento
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_observacao,
			ie_via_aplicacao,
			qt_dose,
			dt_inicio,
			dt_final,
			cd_cat_medicamento,
			ds_medicamento,
			ds_dose,
			nr_seq_cabecalho)
		values
			(nom_rc_medicamento_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ds_observacao_w,		
			ie_via_aplicacao_w,	
      		qt_dose_w,		
     		dt_inicio_w,	
			dt_final_w,		
      		cd_cat_medicamento_w,
      		ds_medicamento_w,
			ds_dose_w,
			nr_seq_cabecalho_p);

		if (length(ds_html_w) < 31000) then

			ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || '<tr>';

				ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || r_c_medicamento.ds_medicamento ||'</td>';
				ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || r_c_medicamento.ie_desc_via_aplicacao || '</td>';
				ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || qt_dose_w || ' ' || ds_dose_desc_w || '</td>';
				ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || obter_data_utc(dt_inicio_w, 'NOM_TABLE') || '</td>';
				ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || obter_data_utc(dt_final_w, 'NOM_TABLE')  || '</td>';
				ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || ds_observacao_w || '</td>';

			ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || '</tr>';
		end if;

	end loop;

	if (qt_registro_w = 0) then

		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || '<tr>';

		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td></td>';
		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td></td>';
		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td></td>';
		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td></td>';
		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td></td>';
		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || chr(9) || '<td>' || obter_desc_expressao(932883) || '</td>';

		ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || chr(9) || '</tr>';
	end if;

	ds_html_w	:= ds_html_w || chr(13) || chr(10) || chr(9) || '</tbody>';

	ds_html_w	:= ds_html_w || chr(13) || chr(10) || '</table>';

	insert into nom_rc_medicamento
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_medicamentos,
		nr_seq_cabecalho)
	values
		(nom_rc_medicamento_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_html_w,
		nr_seq_cabecalho_p);

end if;

end gerar_nom_rc_medicamento;
/