create or replace
procedure gravar_observacao_atend_item(
			ds_observacao_p		Varchar2,
			nr_requisicao_p		Number,
			nr_sequencia_p		Number) is 

begin

update	item_requisicao_material
set	ds_justificativa_atend 	= ds_observacao_p
where	nr_requisicao 		= nr_requisicao_p
and	nr_sequencia 		= nr_sequencia_p;

commit;

end gravar_observacao_atend_item;
/