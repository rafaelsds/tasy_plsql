create or replace
procedure fagf_importar_conta_classif2 is 



cursor c01 is
select	a.cd_classificacao,
	a.cd_conta_contabil,
	a.ds_conta_contabil,
	a.cd_classif_superior
from	conta_contabil a
where	1 = 1
and	exists(	select	1
		from	fagf_conta_classif_nova x
		where	x.cd_conta_contabil	= a.cd_conta_contabil)
order by a.cd_classificacao;



vet01	c01%rowtype;

begin


open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin
		
	/*inserir regra para a classificação vigênte hoje no sistema*/
	insert into conta_contabil_classif (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario_nrec,
		cd_conta_contabil,
		cd_classificacao,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		nm_usuario,
		dt_atualizacao_nrec,
		cd_classif_superior)
	values(	conta_contabil_classif_seq.nextval,
		sysdate,
		'Importacao',
		vet01.cd_conta_contabil,
		vet01.cd_classificacao,
		'01/01/2010',
		null,
		'Importacao',
		sysdate,
		ctb_obter_classif_conta_sup(vet01.cd_classificacao, sysdate, 1));
		
	end;
	
	
end loop;
close c01;

commit;

end fagf_importar_conta_classif2;
/