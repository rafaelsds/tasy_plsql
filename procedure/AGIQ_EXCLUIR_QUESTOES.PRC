CREATE OR REPLACE PROCEDURE agiq_excluir_questoes(ageint_resp_quest_sup_p NUMBER,
                                                  nr_seq_ageint_p         NUMBER,
                                                  nr_seq_ageint_item_p    NUMBER,
                                                  ie_excluiu_quest_p      OUT VARCHAR2,
                                                  ie_commit_p             VARCHAR2 DEFAULT NULL) IS

  qt_reg_w NUMBER(1);

BEGIN
  IF (ageint_resp_quest_sup_p IS NULL) THEN
  
    DELETE FROM ageint_resp_quest_item
     WHERE nr_seq_ageint = nr_seq_ageint_p
       AND nr_seq_item = nr_seq_ageint_item_p;
       
    IF (SQL%ROWCOUNT > 0) THEN
      ie_excluiu_quest_p := 'S';    
    END IF;
  
    DELETE FROM ageint_resp_quest a
     WHERE a.nr_seq_ageint = nr_seq_ageint_p
       AND NOT EXISTS (SELECT 1 
                         FROM ageint_resp_quest_item x 
                         WHERE x.nr_seq_resp_quest = a.nr_sequencia);
       
    IF (SQL%ROWCOUNT > 0) THEN
      ie_excluiu_quest_p := 'S';    
    END IF;
  ELSE
    FOR i IN (SELECT nr_sequencia
                FROM ageint_resp_quest
             CONNECT BY PRIOR nr_sequencia = nr_seq_superior
               START WITH nr_seq_superior = ageint_resp_quest_sup_p) LOOP
    
      DELETE FROM ageint_resp_quest_item WHERE nr_seq_resp_quest = i.nr_sequencia;
      IF (SQL%ROWCOUNT > 0) THEN
        ie_excluiu_quest_p := 'S';
      END IF;
    
      DELETE FROM ageint_resp_quest WHERE nr_sequencia = i.nr_sequencia;
      IF (SQL%ROWCOUNT > 0) THEN
        ie_excluiu_quest_p := 'S';
      END IF;
    END LOOP;
  END IF;

  IF (nvl(ie_commit_p, 'S') = 'S') THEN
    COMMIT;
  END IF;

END agiq_excluir_questoes;
/
