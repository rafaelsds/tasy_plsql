create or replace
procedure usjrp_desbloquear_cartao(	cd_usuario_plano_p	pls_segurado_carteira.cd_usuario_plano%type,
					nr_cpf_p		pessoa_fisica.nr_cpf%type,
					dt_nascimento_p		pessoa_fisica.dt_nascimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

nr_seq_segurado_carteira_w	pls_segurado_carteira.nr_sequencia%type;
	
begin

select	max(c.nr_sequencia)
into	nr_seq_segurado_carteira_w
from	pessoa_fisica a,
	pls_segurado b,
	pls_segurado_carteira c
where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	b.nr_sequencia = c.nr_seq_segurado
and	c.cd_usuario_plano = cd_usuario_plano_p
and	a.nr_cpf = nr_cpf_p
and	a.dt_nascimento = dt_nascimento_p;

if	(nr_seq_segurado_carteira_w is not null) then
	update	pls_segurado_carteira
	set	nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nm_usuario_desbloqueio	= nm_usuario_p,
		dt_desbloqueio		= sysdate,
		ie_tipo_desbloqueio	= 'M'
	where	nr_sequencia = nr_seq_segurado_carteira_w;
else
	wheb_mensagem_pck.exibir_mensagem_abort(1128394);
end if;

commit;

end usjrp_desbloquear_cartao;
/