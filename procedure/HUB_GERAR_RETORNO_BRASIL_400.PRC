create or replace
procedure HUB_GERAR_RETORNO_BRASIL_400(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p	varchar2) is 

nr_seq_reg_T_w		number(10);
nr_seq_reg_U_w		number(10);
nr_titulo_w		number(10);
vl_titulo_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_abatimento_w		number(15,2);
vl_liquido_w		number(15,2);
vl_outras_despesas_w	number(15,2);
dt_liquidacao_w		varchar2(6);
ds_titulo_w		varchar2(255);
vl_cobranca_w		number(15,2);
vl_alterar_w		number(15,2);
cd_ocorrencia_w		number(10);
nr_seq_ocorrencia_ret_w	number(10);
vl_saldo_titulo_w	number(15,2);

cursor c01 is
	select	trim(substr(ds_string,71,10)),
		to_number(substr(ds_string,153,13))/100,
		to_number(substr(ds_string,267,13))/100,
		to_number(substr(ds_string,241,13))/100,
		to_number(substr(ds_string,228,13))/100,
		to_number(substr(ds_string,254,13))/100,
		to_number(substr(ds_string,189,13))/100,
		substr(ds_string,111,6),
		substr(ds_string,109,2)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= '7';
begin

open C01;
loop
fetch C01 into	
	ds_titulo_w,
	vl_cobranca_w,
	vl_acrescimo_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_outras_despesas_w,
	dt_liquidacao_w,
	cd_ocorrencia_w;
exit when C01%notfound;
	begin

	vl_alterar_w	:= 0;
	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	/* Se encontrou o t�tulo importa, sen�o grava no log */

	if	(nr_titulo_w is not null) then

		select	vl_titulo,
			vl_saldo_titulo
		into	vl_titulo_w,
			vl_saldo_titulo_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;


		select 	nvl(to_char(max(a.nr_sequencia)),0)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco = 001
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		/* Tratar acrescimos/descontos */
		if	(vl_titulo_w <> vl_liquido_w) then
			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;	
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;
		end if;

		vl_liquido_w	:= nvl(vl_liquido_w,0) - nvl(vl_desconto_w,0);

		if	(vl_saldo_titulo_w	= nvl(vl_liquido_w,0)) then

			vl_desconto_w	:= 0;

		end if;
		
		insert	into titulo_receber_cobr (	NR_SEQUENCIA,
							NR_TITULO,
							CD_BANCO,
							VL_COBRANCA,
							VL_DESCONTO,
							VL_ACRESCIMO,
							VL_DESPESA_BANCARIA,
							VL_LIQUIDACAO,
							DT_LIQUIDACAO,
							DT_ATUALIZACAO,
							NM_USUARIO,
							NR_SEQ_COBRANCA,
							nr_seq_ocorrencia_ret)
					values	(	titulo_receber_cobr_seq.nextval,
							nr_titulo_w,
							001,
							vl_titulo_w,
							vl_desconto_w,
							vl_acrescimo_w,
							vl_outras_despesas_w,
							nvl(vl_liquido_w,0),
							nvl(to_Date(dt_liquidacao_w,'ddmmyy'),sysdate),
							sysdate,
							nm_usuario_p,
							nr_seq_cobr_escrit_p,
							nr_seq_ocorrencia_ret_w);
	else
		insert	into	log_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');

	end if;
	
	end;
end loop;
close C01;

commit;

end HUB_GERAR_RETORNO_BRASIL_400;
/