create or replace
procedure san_fechar_lote_fim_prod (	nr_seq_lote_p	number,
					nm_usuario_p	varchar2) is 

begin

if (nr_seq_lote_p is not null) then
	
	update	san_fim_prod_dia_lote
	set	nm_usuario_fechamento 	= nm_usuario_p,
		dt_fechamento 		= sysdate
	where	nr_sequencia = nr_seq_lote_p;
	
end if;

commit;

end san_fechar_lote_fim_prod;
/