create or replace
procedure pls_gerar_seq_segurado_empresa
		(	nr_seq_segurado_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 
			
qt_incremento_w		number(10);
cd_operadora_empresa_w	number(10);
nr_seq_segurado_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_segurado
	where	cd_operadora_empresa = cd_operadora_empresa_w
	and	dt_liberacao is not null
	and	nr_sequencial_empresa is not null
	and	nr_sequencia	<> nr_seq_segurado_p
	and	cd_estabelecimento	= cd_estabelecimento_p;	

TYPE 		fetch_array IS TABLE OF c01%ROWTYPE;
s_array 	fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_c01_w			Vetor;	
			
begin

select	max(cd_operadora_empresa)
into	cd_operadora_empresa_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

qt_incremento_w	:= 0;

open c01;
loop
FETCH C01 BULK COLLECT INTO s_array LIMIT 1000;
	Vetor_c01_w(i) := s_array;
	i := i + 1;
EXIT WHEN C01%NOTFOUND;
END LOOP;
CLOSE C01;

for i in 1..Vetor_c01_w.COUNT loop
	s_array := Vetor_c01_w(i);
	qt_incremento_w	:= qt_incremento_w + s_array.COUNT;
end loop;

qt_incremento_w	:= qt_incremento_w + 1;

update	pls_segurado
set	nr_sequencial_empresa	= qt_incremento_w
where	nr_sequencia		= nr_seq_segurado_p;

end pls_gerar_seq_segurado_empresa;
/
