CREATE OR REPLACE procedure pls_pf_marcar_revisado_j
            (   cd_pessoa_fisica_p    varchar2,
                ie_revisar_p        varchar2,
                nm_usuario_p        varchar2) is

begin

update    pessoa_fisica
set ie_revisar     = ie_revisar_p,
    dt_revisao    = sysdate,
    nm_usuario    = nm_usuario_p
where    cd_pessoa_fisica    = cd_pessoa_fisica_p;

commit;

end pls_pf_marcar_revisado_j;
/