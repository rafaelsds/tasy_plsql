create or replace 
procedure man_checar_builds_pendentes(nm_usuario_p	Varchar2) is

ie_tipo_geracao_w	varchar2(1);
nr_sequencia_w		number(10);
nr_seq_ordem_serv_w	number(10);
ie_aplicacao_w		varchar2(1);
cd_versao_w		varchar2(15);
ds_retorno_w		varchar2(2000);
ds_comunic_w		varchar2(2000);
ds_retorno_ci_w		varchar2(2000);
cd_pessoa_fisica_w	varchar2(10);

Cursor C01 is
	select	ie_tipo_geracao,
		nr_sequencia,
		nr_seq_ordem_serv,
		ie_aplicacao,
		cd_versao
	from	man_os_ctrl_build_emer
	where	ie_status_geracao = 'P'
	and	ds_motivo_canc is null
  union 
select 'F' ie_tipo_geracao,
		nr_sequencia,
		nr_seq_ordem_serv_orig nr_seq_ordem_serv,
		ie_aplicacao,
		cd_versao
	from	man_versao_calendario
	where	IE_TIPO_SOLICITACAO = 'A'
    and cd_build is null
    and trunc(dt_geracao) <= trunc(sysdate);

begin

-- N�o rodar no Domingo e S�bado, e apenas em hor�rio comercial
if ((pkg_date_utils.IS_BUSINESS_DAY(sysdate) = 1)
	and (to_number(to_char(sysdate, 'hh24')) between 7 and 19)) then

	ds_comunic_w := wheb_mensagem_pck.get_texto(352622);

	open C01;
	loop
	fetch C01 into
		ie_tipo_geracao_w,
		nr_sequencia_w,
		nr_seq_ordem_serv_w,
		ie_aplicacao_w,
		cd_versao_w;
	exit when C01%notfound;
		begin

		ds_retorno_w := ds_retorno_w || chr(13) ||
			wheb_mensagem_pck.get_texto(352623, 'SEQ='||nr_sequencia_w||';NR_ORDEM_SERV='||nr_seq_ordem_serv_w||';CD_VERSAO='||cd_versao_w||';IE_APLICACAO='||ie_aplicacao_w||';IE_TEMPO_ESP='||ie_tipo_geracao_w);


		ds_retorno_ci_w := ds_retorno_ci_w
			|| wheb_mensagem_pck.get_texto(352623, 'SEQ='||nr_sequencia_w||';NR_ORDEM_SERV='||nr_seq_ordem_serv_w||';CD_VERSAO='||cd_versao_w||';IE_APLICACAO='||ie_aplicacao_w||';IE_TEMPO_ESP='||ie_tipo_geracao_w)
			|| wheb_rtf_pck.get_quebra_linha;


		end;
	end loop;
	close C01;

	if	(ds_retorno_w is not null) then
		begin

		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	usuario
		where	upper(nm_usuario) = upper(nm_usuario_p);

		gerar_comunic_padrao(
			sysdate,
			wheb_mensagem_pck.get_texto(352635),
			ds_comunic_w  || ds_retorno_ci_w,
			nm_usuario_p,
			'N',
			get_users_build('S'),
			'N',
			null,
			null,
			null,
			null,
			sysdate,
			null,
			null);


		enviar_email(
			wheb_mensagem_pck.get_texto(352635),
			ds_comunic_w  || ds_retorno_w,
			'support.informatics@philips.com',
			get_emails_build('S'),
			'Tasy',
			null,
			null);

		end;
	end if;

	commit;
end if;

end man_checar_builds_pendentes;
/