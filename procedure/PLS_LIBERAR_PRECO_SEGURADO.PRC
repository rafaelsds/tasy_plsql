create or replace
procedure pls_liberar_preco_segurado
			(	nr_seq_seg_preco_p	Number,
				ie_opcao_p		Varchar2,
				nm_usuario_p		Varchar2) is 
/*
ie_opcao_p
	L = Liberar
	D = Desfazer libera��o
*/

begin
if	(ie_opcao_p = 'L') then
	update	pls_segurado_preco
	set	dt_liberacao		= sysdate,
		nm_usuario_liberacao	= nm_usuario_p
	where	nr_sequencia 		= nr_seq_seg_preco_p;
elsif	(ie_opcao_p = 'D') then
	update	pls_segurado_preco
	set	dt_liberacao		= null,
		nm_usuario_liberacao	= null
	where	nr_sequencia 		= nr_seq_seg_preco_p;
end if;

end pls_liberar_preco_segurado;
/