CREATE OR REPLACE 
TRIGGER Agfa_ADT_Atend_Pac_shs
BEFORE INSERT ON ATENDIMENTO_PACIENTE
FOR EACH ROW

DECLARE                                                                         

nr_sequencia_w			Number(08,0);                                                   
nm_mae_w			Varchar2(80);                                                        
nm_pai_w			Varchar2(80);                                                        
ie_evento_w			Varchar2(03)		:= 'A01';                                           
ds_endereco_w			Varchar2(150);/*OS98068*/                                        
nr_endereco_w			Varchar2(15);                                                   
ds_bairro_w			Varchar2(40);                                                     
ds_municipio_w			Varchar2(40);                                                   
nr_telefone_w			Varchar2(40);                                                   
ds_nacionalidade_w		Varchar2(80);                                               
ie_estado_civil_w		Varchar2(2);                                                 
nm_pessoa_fisica_w		Varchar2(80);                                               
nm_nome_w			Varchar2(80);                                                       
nm_resto_nome_w			Varchar2(80);                                                  
dt_nascimento_w			Date;                                                          
ie_sexo_w			Varchar2(01);                                                       
cd_nacionalidade_w		Number(15,0);                                               
ie_tipo_admissao_w		Varchar2(01)		:= 'R';                                       
ie_tipo_atendimento_w		Varchar2(03);		/* AA-Ambulatorio AE-Amb. Externo A2-Urgenc
ia H1-Hospitalizado*/                                                           
cd_setor_atendimento_w		Varchar2(250);		/* C�digo^Descri��o*/                    
ds_dado_medico_resp_w		Varchar2(200);		/* C�digo^Nome^Especialidade*/            
dt_entrada_w			Date;                                                            
dt_alta_w			Date;                                                               
cd_pessoa_fisica_w		Varchar2(10);                                               
cd_medico_resp_w		Varchar2(10);                                                 
cd_unidade_basica_w		Varchar2(25);                                              
cd_unidade_compl_w		Varchar2(25);                                               
cd_plano_w			varchar2(10);                                                      
ds_plano_w			Varchar2(80);                                                      
cd_convenio_w			Number(15,0);                                                   
ds_convenio_w			Varchar2(255);                                                  
dt_inicio_vigencia_w		date;                                                     
dt_final_vigencia_w		date;                                                      
nr_atecaco_w			Number(15);                                                      
nr_cpf_w			Varchar2(11);                                                       
nr_identidade_w			Varchar2(15);                                                 
ds_orgao_emissor_ci_w		varchar2(40);                                             
ds_compl_info_w			varchar2(50); /*OS123463*/                                    
ds_email_w			varchar2(50);                                                      

BEGIN                                                                           

select 	Agfa_adt_seq.nextval                                                     
into	nr_sequencia_w                                                             
from	dual;                                                                      

dt_entrada_w			:= :new.dt_entrada;                                              
dt_alta_w			:= :new.dt_alta;                                                    
cd_pessoa_fisica_w		:= :new.cd_pessoa_fisica;                                   
cd_medico_resp_w		:= :new.cd_medico_resp;                                       
ie_tipo_atendimento_w		:= :new.ie_tipo_atendimento;                             

/*                                                                              
nr_atecaco_w			:= obter_atecaco_atendimento(:new.nr_atendimento);               
if	(nr_atecaco_w is not null) then                                              
	select	cd_convenio,                                                            
		cd_plano_convenio,                                                            
		obter_nome_convenio(cd_convenio),                                             
		obter_desc_plano(cd_convenio, cd_plano_convenio)                              
	into	cd_convenio_w,                                                            
		cd_plano_w,                                                                   
		ds_convenio_w,                                                                


		ds_plano_w                                                                    
	from	atend_categoria_convenio                                                  
	where	nr_seq_interno	= nr_atecaco_w;                                           
end if;                                                                         
*/                                                                              

select	nm_pessoa_fisica,                                                        
	ie_sexo,                                                                      
	substr(ie_estado_civil,1,1),                                                  
	cd_nacionalidade,                                                             
	dt_nascimento,                                                                
	substr(obter_parte_nome(nm_pessoa_fisica,'Nome'),1,80),                       
	substr(obter_parte_nome(nm_pessoa_fisica,'RestoNome'),1,80),                  
	nr_cpf,                                                                       
	nr_identidade,                                                                
	ds_orgao_emissor_ci                                                           
into	nm_pessoa_fisica_w,                                                        
	ie_sexo_w,                                                                    
	ie_estado_civil_w,                                                            
	cd_nacionalidade_w,                                                           
	dt_nascimento_w,                                                              
	nm_nome_w,                                                                    
	nm_resto_nome_w,                                                              
	nr_cpf_w,                                                                     
	nr_identidade_w,                                                              
	ds_orgao_emissor_ci_w                                                         
from	pessoa_fisica                                                              
where	cd_pessoa_fisica	= cd_pessoa_fisica_w;                                    

select Decode(ie_tipo_atendimento_w,1,'H1',3,'A2',7,'AE',8,'AA')                
into	ie_tipo_atendimento_w                                                      
from	dual;                                                                      

select	substr(max(cd_pessoa_fisica || '^' || nm_pessoa_fisica || '^' || Obter_Especialidade_medico(cd_pessoa_fisica,'D')),1,200)                                
into	ds_dado_medico_resp_w                                                      
from 	pessoa_fisica                                                             
where	cd_pessoa_fisica	= :new.cd_medico_resp;                                   

select max(ds_endereco),                                                        
	max(nr_endereco),                                                              
	max(ds_bairro),                                                                
	max(ds_municipio),                                                             
	max(nr_telefone),                                                              
	substr(max(ds_email),1,50)                                                     
into	ds_endereco_w,                                                             
	nr_endereco_w,                                                                 
	ds_bairro_w,                                                                   
	ds_municipio_w,                                                                
	nr_telefone_w,                                                                 
	ds_email_w                                                                     
from	compl_pessoa_fisica                                                        
where	cd_pessoa_fisica	= cd_pessoa_fisica_w                                     
and	ie_tipo_complemento	= 1;                                                    

select max(nm_contato)                                                          
into	nm_pai_w                                                                   
from	compl_pessoa_fisica                                                        
where	cd_pessoa_fisica	= cd_pessoa_fisica_w                                     
and	ie_tipo_complemento	= 4;                                                    

select max(nm_contato)                                                          
into	nm_mae_w                                                                   
from	compl_pessoa_fisica                                                        
where	cd_pessoa_fisica	= cd_pessoa_fisica_w                                     
and	ie_tipo_complemento	= 5;                                                    

select	substr(max(ds_nacionalidade),1,80)                                       
into	ds_nacionalidade_w                                                         
from	nacionalidade                                                              
where	cd_nacionalidade	= cd_nacionalidade_w;                                    

if	(ie_estado_civil_w = 1) then                                                 
	ie_estado_civil_w	:= 'S';                                                      
elsif	(ie_estado_civil_w = 2) then                                              
	ie_estado_civil_w	:= 'C';                                                      
elsif	(ie_estado_civil_w = 3) then                                              
	ie_estado_civil_w	:= 'D';                                                      
elsif	(ie_estado_civil_w = 4) then                                              
	ie_estado_civil_w	:= 'D';                                                      
elsif	(ie_estado_civil_w = 5) then                                              
	ie_estado_civil_w	:= 'V';                                                      
elsif	(ie_estado_civil_w = 6) then                                              
	ie_estado_civil_w	:= 'SP';                                                     
else    ie_estado_civil_w	:= 'OT';                                                     
end if;

ds_compl_info_w := '##'||nr_identidade_w||' '||ds_orgao_emissor_ci_w||'##'||nr_cpf_w;
ds_endereco_w := substr(substr(ds_endereco_w,1,100-length(ds_compl_info_w)) || ds_compl_info_w,1,150);                                                          

insert into Agfa_Adt_v(                                                         
	ie_sistema,                                                                    
	dt_insert,                                                                     
	nr_seq_hl7,                                                                    
	ie_status,                                                                     
	ie_evento,                                                                     
	dt_transacao,                                                                  
	cd_pessoa_fisica,                                                              
	nm_sobrenome,                                                                  
	nm_primeiro_nome,                                                              
	dt_nascimento,                                                                 
	ie_sexo,                                                                       
	ds_endereco,                                                                   
	nr_endereco,                                                                   
	ds_bairro,                                                                     
	ds_municipio,                                                                  
	nr_telefone,                                                                   
	ie_estado_civil,                                                               
	ds_nacionalidade,                                                              
	nm_mae,                                                                        
	ds_mae,                                                                        
	nm_pai,                                                                        
	ds_pai,                                                                        
	ie_tipo_atendimento,                                                           
	cd_setor_atendimento,                                                          
	cd_unidade_basica,                                                             
	cd_unidade_compl,                                                              
	ie_tipo_admissao,                                                              
	ds_dado_medico_resp,                                                           
	nr_atendimento,                                                                
	dt_entrada,                                                                    
	dt_alta,                                                                       
	cd_plano,                                                                      
	ds_plano,                                                                      
	cd_convenio,                                                                   
	ds_convenio,                                                                   
	dt_inicio_vigencia,                                                            
	dt_final_vigencia,                                                             
	cd_plano2,                                                                     
	ds_email)                                                                      
values(                                                                         
	'SHS',                                                                         
	sysdate,                                                                       
	nr_sequencia_w,                                                                
	'N',                                                                           
	ie_evento_w,                                                                   
	sysdate,                                                                       
	:new.cd_pessoa_fisica,                                                         
	nm_resto_nome_w,                                                               
	nm_nome_w,                                                                     
	dt_nascimento_w,                                                               
	ie_sexo_w,                                                                     
	ds_endereco_w,                                                                 
	nr_endereco_w,                                                                 
	ds_bairro_w,                                                                   
	ds_municipio_w,                                                                
	nr_telefone_w,                                                                 
	ie_estado_civil_w,                                                             
	ds_nacionalidade_w,                                                            
	nm_mae_w,                                                                      
	'M�e',                                                                         
	nm_pai_w,                                                                      
	'Pai',                                                                         
	ie_tipo_atendimento_w,                                                         
	cd_setor_atendimento_w,                                                        
	cd_unidade_basica_w,                                                           
	cd_unidade_compl_w,                                                            
	ie_tipo_admissao_w,                                                            
	ds_dado_medico_resp_w,                                                         
	:new.nr_atendimento,                                                           
	:new.dt_entrada,                                                               
	:new.dt_alta,                                                                  
	cd_plano_w,                                                                    
	ds_plano_w,                                                                    
	cd_convenio_w,                                                                 
	ds_convenio_w,                                                                 
	dt_inicio_vigencia_w,                                                          
	dt_final_vigencia_w,                                                           
	cd_plano_w,                                                                    
	ds_email_w);                                                                   
END;                                                                            
/
