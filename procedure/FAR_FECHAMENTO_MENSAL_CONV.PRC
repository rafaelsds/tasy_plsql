create or replace procedure	far_fechamento_mensal_conv(
			nm_usuario_p		varchar2) is

nr_sequencia_w		number(10);
dt_dia_atual_w		number(2);
dt_ult_dia_w		number(2);

cursor	c01 is
select	nr_sequencia
from	far_contrato_conv a
where	trunc(sysdate) between dt_inicio_vigencia and dt_final_vigencia
and	nvl(dt_dia_fechamento,0) > 0
and	((dt_dia_fechamento = dt_dia_atual_w) or
	((dt_dia_fechamento > dt_ult_dia_w) and
	(trunc(sysdate) = pkg_date_utils.get_datetime(pkg_date_utils.end_of(sysdate,'MONTH',0),sysdate, 0))));

begin

dt_dia_atual_w		:= to_number(to_char(sysdate,'dd'));
dt_ult_dia_w		:= to_number(to_char(pkg_date_utils.end_of(sysdate,'MONTH',0),'dd'));

open c01;
loop
fetch c01 into
	nr_sequencia_w;
exit when c01%notfound;
	begin

	far_gerar_fechamento_convenio(
		nr_sequencia_w,
		pkg_date_utils.start_of(pkg_date_utils.add_month(sysdate,-1,0),'MONTH',0),
		nm_usuario_p);

	end;
end loop;
close c01;

end far_fechamento_mensal_conv;
/