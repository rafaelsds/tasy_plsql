create or replace
procedure hfp_atualizar_altura_peso(	cd_pessoa_fisica_p	VARCHAR2,
					qt_altura_cm_p		NUMBER,
					qt_peso_p		NUMBER,
					nm_usuario_p		VARCHAR2) is 

begin

if 	(cd_pessoa_fisica_p is not null) then

	UPDATE	pessoa_fisica
	SET	qt_altura_cm 	 = qt_altura_cm_p,
		qt_peso 	 = qt_peso_p,
		dt_atualizacao	 = sysdate,
		nm_usuario	 = nm_usuario_p
	WHERE  	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
end if;

commit;

end hfp_atualizar_altura_peso;
/
