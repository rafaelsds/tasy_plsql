create or replace
procedure atualizar_perfil_acesso_pep (
		nr_sequencia_p	number,
		ds_perfil_p	varchar2,
		nm_usuario_p	varchar2) is
	
begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	pep_perfil_paciente
	set	ds_perfil = ds_perfil_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;
commit;
end atualizar_perfil_acesso_pep;
/