create or replace
procedure carregar_qte_maquina(nr_sequencia_p  number) is

vl_inf_w		varchar2(100);
id_inf_w		varchar2(50);
cd_cnpj_w	varchar2(14);

Cursor C01 is
	SELECT	COUNT (DISTINCT ds_maquina), 
		Obter_Cgc_Estabelecimento (Obter_Estabelecimento_Ativo),
		Obter_Global_Name
	FROM	TASY_CONTROLE_LOGIN
	WHERE	dt_acesso BETWEEN TO_DATE ((	SELECT	MAX (a.dt_versao)
						FROM	APLICACAO_TASY_VERSAO a
						WHERE	TRUNC (a.dt_versao) <> TRUNC (SYSDATE)),'dd/mm/yy')
						AND	Fim_Dia (TO_DATE (SYSDATE, 'dd/mm/yy'))
	AND ds_aplicacao = 'TasySwing'
	AND ds_maquina IS NOT NULL
	GROUP BY Obter_Cgc_Estabelecimento (Obter_Estabelecimento_Ativo);

begin

open c01;
loop
fetch c01 into 
	vl_inf_w,
	cd_cnpj_w,
	id_inf_w;
exit when c01%notfound;
        begin

        insert_inf_base_cliente(
            nr_sequencia_p,
            vl_inf_w,
            id_inf_w,
            cd_cnpj_w,
            null,
            'Tasy');
        end;
end loop;
close c01;

commit;

end carregar_qte_maquina;
/