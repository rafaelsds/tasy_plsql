create or replace
procedure wsuite_save_log (	ie_acao_p		wsuite_log_portal.ie_acao%type,
				ds_observacao_p 	wsuite_log_portal.ds_observacao%type,
				ds_endereco_ip_p	wsuite_log_portal.ds_endereco_ip%type,
				ds_login_p		wsuite_usuario.ds_login%type,
				ie_aplicacao_p		wsuite_log_portal.ie_aplicacao%type default null) is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finality: Save the logs of the actions done by users
Caution:  IE_ACAO = Domain 8076
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

if	( ie_acao_p is not null and ds_login_p is not null  ) then

	insert into  wsuite_log_portal(	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_usuario_wsuite,
					ie_acao, ds_observacao, ds_endereco_ip,
					ds_login, dt_geracao_log, ie_aplicacao)
				values(	wsuite_log_portal_seq.nextval, sysdate, ds_login_p,
					sysdate, ds_login_p, null, 
					ie_acao_p, ds_observacao_p,ds_endereco_ip_p,
					ds_login_p, sysdate, ie_aplicacao_p);
	commit;
	
end if;

end wsuite_save_log;
/