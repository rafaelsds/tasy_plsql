CREATE OR REPLACE PROCEDURE reg_delete_resul_aval_customer (
	nr_seq_aval_customer_p		reg_avaliacao_customer.nr_sequencia%TYPE
) IS

BEGIN
	DELETE FROM reg_avaliacao_cust_resul
	WHERE nr_seq_avaliacao = nr_seq_aval_customer_p;
	COMMIT;
END reg_delete_resul_aval_customer;
/