create or replace
procedure pls_obter_regra_tipo_guia_aten
			(	nr_seq_segurado_p	in	number,
				nr_seq_tipo_guia_p	out	number,
				nr_seq_plano_p		out 	number) is 

nr_seq_plano_w		number(10);
nr_seq_tipo_guia_w	number(10);

cursor C01 is
	select	a.nr_seq_tipo_guia
	from	pls_tipo_guia_med_atend	a
	where	a.nr_seq_plano	= nr_seq_plano_w
	order by 1;

begin
select	max(a.nr_seq_plano)
into	nr_seq_plano_w
from	pls_segurado	a
where	a.nr_sequencia	= nr_seq_segurado_p;

open C01;
loop
fetch C01 into	
	nr_seq_tipo_guia_w;
exit when C01%notfound;
end loop;
close C01;

if	(nr_seq_tipo_guia_w is null) then
	nr_seq_plano_w		:= null;
end if;

nr_seq_tipo_guia_p	:= nr_seq_tipo_guia_w;
nr_seq_plano_p		:= nr_seq_plano_w;

end pls_obter_regra_tipo_guia_aten;
/
