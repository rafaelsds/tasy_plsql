create or replace
procedure mp_gerar_relat_processo_item(	nr_sequencia_p		number,
					nr_seq_superior_p	number,
					nm_usuario_p		Varchar2,
					ie_seq_unica_p		varchar2) is

nr_sequencia_w		number(10);
nr_seq_apres_w		number(10);
nm_objeto_w		varchar2(255);
nr_seq_proximo_objeto_w	number(10);
ds_unid_org_w		varchar2(255);
nr_seq_superior_w	number(10);
nr_seq_processo_w	number(10);
ie_tipo_grupo_w		varchar2(2);
nr_seq_objeto_anterior_w	number(10);
ds_objeto_w		long;
cd_classif_w		varchar2(40);
cd_classif_superior_w	varchar2(40);
cd_classif_destino_w	varchar2(40);
qt_registro_w		number(5);
nm_objeto_destino_w	varchar2(255);
nr_seq_obj_destino_w	number(10);

Cursor c01 is
select	a.nr_sequencia
from	mp_processo_objeto a
where	a.nr_seq_obj_origem	= nr_sequencia_p
and	not exists	(select	1
			from	w_relat_mp_processo y,
				w_relat_mp_processo x
			where	x.nm_usuario		= nm_usuario_p
			and	x.nr_seq_superior	= y.nr_sequencia
			and	x.nr_seq_proc_objeto	= a.nr_sequencia
			and	x.ie_tipo		= 'F'
			and	y.ie_tipo		= 'D')
order by
	a.nm_objeto desc;						

begin

if	(nr_sequencia_p is not null) then
	nr_seq_superior_w	:= nr_seq_superior_p;

	select	replace(replace(a.nm_objeto,chr(10),''),chr(13),''),
		a.nr_seq_processo,
		c.ie_tipo_grupo,
		a.ds_objeto
	into	nm_objeto_w,
		nr_seq_processo_w,
		ie_tipo_grupo_w,
		ds_objeto_w
	from	mp_grupo_objeto c,
		mp_objeto b,
		mp_processo_objeto a
	where	a.nr_seq_objeto	= b.nr_sequencia
	and	b.nr_seq_grupo	= c.nr_sequencia
	and	a.nr_sequencia	= nr_sequencia_p;
	
	select	max(b.ds_unid_org)
	into	ds_unid_org_w
	from	mp_unid_org b,
		mp_proc_obj_unid_org a
	where	a.nr_seq_unid_org	= b.nr_sequencia
	and	a.nr_seq_proc_obj	= nr_sequencia_p;

	select	w_relat_mp_processo_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	--if	(ie_seq_unica_p = 'S') then
		/*select	nvl(max(nr_seq_apres),0) + 1
		into	nr_seq_apres_w
		from	w_relat_mp_processo a
		where	nm_usuario	= nm_usuario_p
		and	nm_objeto is not null*/
		/*and	exists	(select	1
				from	mp_proc_obj_es y,
					mp_processo_objeto x
				where	y.nr_seq_proc_obj	=  x.nr_sequencia
				and	x.nr_seq_processo	= nr_seq_processo_w
				and	x.nr_sequencia		= a.nr_seq_proc_objeto);*/
	/*else*/
		select	nvl(max(nr_seq_apres),0) + 1
		into	nr_seq_apres_w
		from	w_relat_mp_processo
		where	nm_usuario	= nm_usuario_p
		and	nr_seq_superior	= nr_seq_superior_w
		and	nm_objeto is not null;
	/*end if;*/
	
	select	max(a.cd_classif)
	into	cd_classif_superior_w
	from	w_relat_mp_processo a
	where	nm_usuario	= nm_usuario_p
	and	nr_sequencia	= nr_seq_superior_w;
	
	if	(nm_objeto_w is not null) then
		cd_classif_w	:= cd_classif_superior_w || '.' || nr_seq_apres_w;
	else
		cd_classif_w	:= null;
	end if;
	
	if	(ie_tipo_grupo_w = 'C') then
		nm_objeto_w	:= replace(replace(mp_obter_proc_ativ_conexao(nr_sequencia_p),chr(13),''),chr(10),'');
	end if;
	
	if	(ie_tipo_grupo_w = 'D') then
		select	count(*)
		into	qt_registro_w
		from	w_relat_mp_processo a
		where	a.nr_seq_proc_objeto	= nr_sequencia_p;
		
		if	(qt_registro_w > 0) then
			select	min(a.cd_classif),
				min(a.nr_seq_proc_objeto)
			into	cd_classif_destino_w,
				nr_seq_obj_destino_w
			from	w_relat_mp_processo a
			where	a.nr_seq_proc_objeto	= nr_sequencia_p;
			
			select	max(replace(replace(a.nm_objeto,chr(10),''),chr(13),''))
			into	nm_objeto_destino_w
			from	mp_processo_objeto a
			where	a.nr_sequencia	= nr_seq_obj_destino_w;
		
			ds_objeto_w	:= null;
			nm_objeto_w	:= 'Ir para ' || cd_classif_destino_w || ' (' || nm_objeto_destino_w || ')';
		end if;
	end if;

	insert into w_relat_mp_processo
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_processo,
		nr_seq_proc_objeto,
		nm_objeto,
		ds_unid_org,
		nr_seq_superior,
		nr_seq_apres,
		ie_tipo,
		cd_classif)
	values	(nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_processo_w,
		nr_sequencia_p,
		nm_objeto_w,
		ds_unid_org_w,
		nr_seq_superior_w,
		nr_seq_apres_w,
		ie_tipo_grupo_w,
		cd_classif_w);
		
	update	w_relat_mp_processo
	set	ds_objeto	= ds_objeto_w
	where	nr_sequencia	= nr_sequencia_w;
		
	/* Se for uma decis�o tem que quebrar mais um n�vel */
	if	(ie_tipo_grupo_w = 'D') then
		nr_seq_superior_w	:= nr_sequencia_w;
	end if;

	/* Se for um fluxo tem que pegar qual o objeto destino */
	if	(ie_tipo_grupo_w = 'F') then
		select	max(a.nr_seq_obj_destino)
		into	nr_seq_proximo_objeto_w
		from	mp_processo_objeto a
		where	a.nr_sequencia	= nr_sequencia_p;
		
		/* Se o objeto anterior foi uma decis�o, quebrar tamb�m */
		select	max(a.nr_seq_obj_origem)
		into	nr_seq_objeto_anterior_w
		from	mp_processo_objeto a
		where	a.nr_sequencia	= nr_sequencia_p
		and	exists	(select	1
				from	mp_grupo_objeto z,
					mp_objeto y,
					mp_processo_objeto x
				where	x.nr_seq_objeto = y.nr_sequencia
				and	y.nr_seq_grupo	= z.nr_sequencia
				and	z.ie_tipo_grupo	= 'D'
				and	x.nr_sequencia	= a.nr_seq_obj_origem);
				
		if	(nr_seq_objeto_anterior_w is not null) then
			nr_seq_superior_w	:= nr_sequencia_w;
		end if;
		
		if	(nr_seq_proximo_objeto_w is not null) then
			mp_gerar_relat_processo_item(nr_seq_proximo_objeto_w,nr_seq_superior_w,nm_usuario_p,ie_seq_unica_p);
		end if;
	/* Sen�o, busca qual o fluxo que tem como origem este objeto */
	else
		open c01;
		loop
		fetch c01 into	
			nr_seq_proximo_objeto_w;
		exit when c01%notfound;
			begin
			mp_gerar_relat_processo_item(nr_seq_proximo_objeto_w,nr_seq_superior_w,nm_usuario_p,ie_seq_unica_p);
			end;
		end loop;
		close c01;
	end if;
end if;

end mp_gerar_relat_processo_item;
/