create or replace procedure  MIMS_IMP_MAT_CONV_UNIT(	cd_material_p		number,

					cd_unidade_medida_p	varchar2,

					ds_unidade_medida_p	varchar2,

					qt_conversao_p		number,

					ie_prioridade_p		number,

					nm_usuario_p		varchar2,
          
          nr_mims_ver_p number) is



material_conversao_unidade_w  IMP_MATERIAL_CONV_UNIDADE%rowtype;



qt_reg_w	number(10);



begin



select	count(1)

into	qt_reg_w

from	IMP_MATERIAL_CONV_UNIDADE

where	cd_material		= cd_material_p

and	cd_unidade_medida	= cd_unidade_medida_p;



mims_import_unit(cd_unidade_medida_p,nvl(ds_unidade_medida_p,cd_unidade_medida_p),nm_usuario_p,nr_mims_ver_p); 





if	(qt_reg_w	= 0) then

	material_conversao_unidade_w.cd_material		:= cd_material_p;

	material_conversao_unidade_w.cd_unidade_medida		:= cd_unidade_medida_p;

	material_conversao_unidade_w.dt_atualizacao		:= sysdate;

	material_conversao_unidade_w.dt_atualizacao_nrec	:= sysdate;

	material_conversao_unidade_w.nm_usuario			:= nm_usuario_p;

	material_conversao_unidade_w.nm_usuario_nrec		:= nm_usuario_p;

	material_conversao_unidade_w.ie_prioridade		:= ie_prioridade_p;

	material_conversao_unidade_w.qt_conversao		:= qt_conversao_p;

	material_conversao_unidade_w.nr_seq_mims_version := nr_mims_ver_p;															   


	insert into IMP_MATERIAL_CONV_UNIDADE	 values material_conversao_unidade_w;

	--commit;

end if; 





  



end mims_imp_mat_conv_unit;
/