create or replace
procedure fechar_negociacao_cr
			(	nr_sequencia_p		number,
				nm_usuario_p		varchar2) is
				

ds_observacao_w		varchar2(3999);
ds_titulos_w		varchar2(1000) := null;
cd_cgc_w		varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
ds_titulo_w		varchar2(10);
ie_titulo_negociado_w	varchar2(1);
vl_vencimento_w		number(15,2);
vl_debito_w		number(15,2);
vl_saldo_negociacao_w	number(15,2);
nr_seq_boleto_w		number(10);
nr_seq_debito_w		number(10);
nr_titulo_w		number(10);
cd_portador_w		number(10);
cd_tipo_taxa_juro_w	number(10);
cd_tipo_taxa_multa_w	number(10);
nr_negociacao_w		number(10);
qt_registro_w		number(10);
cd_tipo_portador_w	number(5);
cd_moeda_padrao_w	number(5);
cd_estabelecimento_w	number(4);
dt_fechamento_w		date;
dt_recebimento_w	date	:= null;
dt_vencimento_w		date;
dt_debito_w		date;
dt_negociacao_w		date;
ie_observacao_w		varchar2(1);
vl_saldo_titulo_w	titulo_receber.vl_saldo_titulo%type;
ie_dt_contab_tit_neg_w	varchar2(1);
qt_titulo_w				number(10);
dt_contabil_w			titulo_receber.dt_contabil%type;

/* T�tulos baixados na negociacao */
Cursor C01 is
	select	a.nr_titulo
	from	titulo_rec_negociado a
	where	a.nr_seq_negociacao	= nr_sequencia_p
	order by
		a.nr_titulo;
	
/* T�tulos gerados � partir da negociacao */
Cursor C02 is
	select	a.nr_titulo
	from	titulo_receber a
	where	a.nr_seq_negociacao_origem	= nr_sequencia_p
	union
	select	a.nr_titulo
	from	negociacao_cr_boleto a
	where	a.nr_seq_negociacao	= nr_sequencia_p
	union
	select	a.nr_titulo
	from	negociacao_cr_deb_cc a
	where	a.nr_seq_negociacao	= nr_sequencia_p;
begin

Obter_Param_Usuario(5514, 19, obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_w, ie_observacao_w);
Obter_Param_Usuario(5514, 23, obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_w, ie_dt_contab_tit_neg_w);

if	(nr_sequencia_p	is not null) then
	/* Obter dados da negocia��o */
	select	a.dt_negociacao,
		a.dt_fechamento,
		a.cd_estabelecimento,
		a.cd_pessoa_fisica,
		a.cd_cgc
	into	dt_negociacao_w,
		dt_fechamento_w,
		cd_estabelecimento_w,
		cd_pessoa_fisica_w,
		cd_cgc_w
	from	negociacao_cr a
	where	a.nr_sequencia	= nr_sequencia_p;
	
	/* Consist�ncias */
	if	(dt_fechamento_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(175678);
	end if;
	
	vl_saldo_negociacao_w	:= obter_valores_negociacao_cr(nr_sequencia_p,'VS');
	
	if	(vl_saldo_negociacao_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(175679,'VL_SALDO=' || vl_saldo_negociacao_w);
	elsif (obter_valores_negociacao_cr(nr_sequencia_p,'VS') < 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(175680);
	end if;
	
	/* Se a negocia��o n�o envolveu valores monet�rios j� deve atualizar a 
	data de recebimento no caixa e gerar os t�tulos  */
	select	sum(qt_registro)
	into	qt_registro_w
	from	(select	count(*) qt_registro
		from	negociacao_cr_cheque
		where	nr_seq_negociacao	= nr_sequencia_p
		union all
		select	count(*) qt_registro
		from	negociacao_cr_cartao
		where	nr_seq_negociacao	= nr_sequencia_p
		union all
		select	count(*) qt_registro
		from	negociacao_cr_especie
		where	nr_seq_negociacao	= nr_sequencia_p);
		
	if	(qt_registro_w = 0) then
		dt_recebimento_w	:= sysdate;
	end if;
	
	gerar_titulos_negociacao_cr(nr_sequencia_p,nm_usuario_p);
	
	/* Obter n�mero da negocia��o */
	select	nvl(max(a.nr_negociacao),0) + 1
	into	nr_negociacao_w
	from	negociacao_cr a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;
	
	select	nvl(max(ie_titulo_negociado),'N')
	into	ie_titulo_negociado_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	update	negociacao_cr
	set	dt_fechamento	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		nr_negociacao	= nr_negociacao_w,
		dt_recebimento	= dt_recebimento_w,
		ie_status	= 'AP'
	where	nr_sequencia	= nr_sequencia_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_titulo_w;
	exit when C01%notfound;
		begin
		ds_titulos_w	:= null;
		open C02;
		loop
		fetch C02 into	
			ds_titulo_w;
		exit when C02%notfound;
			begin
			if	(ds_titulos_w is null) then
				ds_titulos_w	:= substr(ds_titulo_w,1,1000);
			else
				ds_titulos_w	:= substr(ds_titulos_w || ', ' || ds_titulo_w,1,1000);
			end if;
			end;
		end loop;
		close C02;
		
		/*ds_observacao_w	:= 'T�tulo baixado pela negocia��o ' || nr_negociacao_w || ' e gerado(s) os t�tulo(s) ' || 
				ds_titulos_w || ' � partir da mesma.';*/
		ds_observacao_w	:= substr(wheb_mensagem_pck.get_texto(302714,'NR_NEGOCIACAO_W='||nr_negociacao_w||';DS_TITULOS_W='||ds_titulos_w),1,3999);

				
		update	titulo_receber
		set	ds_observacao_titulo	= substr(ds_observacao_titulo || ds_observacao_w,1,3999)
		where	nr_titulo		= nr_titulo_w;
		
		if	(ie_titulo_negociado_w = 'S') then --Se parametrizado, altera para Transferido quando liquida o t�tulo pela negocia��o
			update	titulo_receber
			set	ie_situacao	= '5'
			where	nr_titulo	= nr_titulo_w
			and	vl_saldo_titulo	= 0;
		end if;
		
		select	sum(vl_saldo_titulo)
		into	vl_saldo_titulo_w
		from	titulo_receber 
		where	nr_titulo = nr_titulo_w;
		
		select	max(dt_contabil)
		into	dt_contabil_w
		from	titulo_receber
		where	nr_titulo = nr_titulo_w;
		
		if	(vl_saldo_titulo_w = 0) then
			liberar_titulo_orgao_cobr(nr_titulo_w,nm_usuario_p);
		end if;		
		
		end;
	end loop;
	close C01;
	
	open C02;
	loop
	fetch C02 into	
		nr_titulo_w;
	exit when C02%notfound;
		begin
		
		if ( nvl(ie_observacao_w,'N') = 'S' ) then
			--ds_observacao_w	:= 'T�tulo gerado atrav�s da negocia��o ' || nr_negociacao_w || '. '||substr(obter_titulos_negociados(nr_sequencia_p),1,4000);
			ds_observacao_w	:= wheb_mensagem_pck.get_texto(302718,'NR_NEGOCIACAO_W='||nr_negociacao_w) || substr(obter_titulos_negociados(nr_sequencia_p),1,4000);		
		else
			--ds_observacao_w	:= 'T�tulo gerado atrav�s da negocia��o ' || nr_negociacao_w || '.';
			ds_observacao_w	:= wheb_mensagem_pck.get_texto(302718,'NR_NEGOCIACAO_W='||nr_negociacao_w);
		end if;
				
		update	titulo_receber
		set	ds_observacao_titulo	= substr(ds_observacao_titulo || ds_observacao_w,1,3999)
		where	nr_titulo		= nr_titulo_w;

		if ( nvl(ie_dt_contab_tit_neg_w,'N') = 'S' ) then
			/*Verifica qts titulos estao sendo negociados*/
			select 	count(*) 
			into	qt_titulo_w
			from	titulo_rec_negociado a
			where	a.nr_seq_negociacao	= nr_sequencia_p;

			/*se tiver apenas um negociado, ir� buscar a data contabil dele para atualizar nos titulos gerados na negociacao.*/
			if (qt_titulo_w = 1) then

				if (dt_contabil_w is not null) then
				
					update	titulo_receber
					set		dt_contabil = dt_contabil_w
					where	nr_titulo	= nr_titulo_w; 
				
				end if;
			
			end if;
		
		end if;	
		
		end;
	end loop;
	close C02;
	
	--atualizar_valores_neg_cr(nr_sequencia_p,nm_usuario_p,'N'); Retireii devido a OS 676838, pois ja chama essa rotina quando insere os titulos na negociacao, onde gera as taxas.
end if;

commit;

end fechar_negociacao_cr;
/