create or replace
procedure pep_gerar_pedido_exame_ext(
			nr_seq_pedido_p		number,
			lista_nr_seq_exame_p	varchar2,
			nm_usuario_p		varchar2) is 

nr_sequencia_w		number(10);
nr_seq_apresent_w	number(10);
qt_exame_w		number(5);
ds_justificativa_w	varchar2(4000);
nr_seq_exame_w		number(10);
ds_lista_exames_w	varchar2(2000);
ds_lista_aux_w		varchar2(2000) := '';
k			integer;

begin
ds_lista_exames_w := lista_nr_seq_exame_p;
while ds_lista_exames_w is not null loop 
	begin
	
	select	instr(ds_lista_exames_w, ',') 
	into	k
	from	dual;

	if	(k > 1) and
		(substr(ds_lista_exames_w, 1, k -1) is not null) then
		ds_lista_aux_w			:= substr(ds_lista_exames_w, 1, k-1);
		nr_seq_exame_w			:= replace(ds_lista_aux_w, ',','');
		ds_lista_exames_w		:= substr(ds_lista_exames_w, k + 1, 2000);
	elsif	(ds_lista_exames_w is not null) then
		nr_seq_exame_w			:= replace(ds_lista_exames_w,',','');
		ds_lista_exames_w		:= '';
	end if;
	
	Gerar_pedido_exame_ext_item(nr_seq_pedido_p,nr_seq_exame_w,nm_usuario_p);
	
	end;
end loop; 
	
commit;

end pep_gerar_pedido_exame_ext;
/
