create or replace procedure incons_by_dev_group_corp(	nr_group_p		number,
														dt_initial_p	date,
														dt_final_p		date) is
begin
	dictionary_inconsistencies_pck.inconsistencies_by_dev_group@whebl02_orcl(nr_group_p, dt_initial_p, dt_final_p);
end incons_by_dev_group_corp;
/