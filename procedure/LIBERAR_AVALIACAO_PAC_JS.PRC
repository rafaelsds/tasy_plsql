create or replace
procedure liberar_avaliacao_pac_js(	nr_seq_ageint_p		number,
					nm_usuario_p		varchar2) is
					
nr_sequencia_w	number(10);
					
cursor	c01 is
	select	nr_sequencia
	from	med_avaliacao_paciente
	where	nr_seq_ageint	= nr_seq_ageint_p
	and	dt_liberacao 	is null;

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	liberar_avaliacao(nr_sequencia_w,nm_usuario_p);
	end;
end loop;
close C01;

/*		
update	med_avaliacao_paciente
set	dt_liberacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_seq_ageint	= nr_seq_ageint_p;
*/

commit;
end liberar_avaliacao_pac_js;
/