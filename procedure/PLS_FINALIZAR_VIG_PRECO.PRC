create or replace
procedure pls_finalizar_vig_preco(	nr_sequencia_p		in preco_amb.nr_sequencia%type,
					dt_final_vigencia_p	in preco_amb.dt_final_vigencia%type,
					ie_origem_regra_p	in varchar2,
					nm_usuario_p		in usuario.nm_usuario%type) is
					
/* ie_origem_regra_p
	A - AMB
	T - TUSS
*/
begin


-- AMB
if	(ie_origem_regra_p = 'A') then

	-- grava a data de fim vigencia, se a mesma estiver null
	update	preco_amb
	set	dt_final_vigencia	= dt_final_vigencia_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p
	and	dt_final_vigencia	is null;
end if;


-- TUSS
if	(ie_origem_regra_p = 'T') then

	-- grava a data de fim vigencia, se a mesma estiver null
	update	preco_tuss
	set	dt_final_vigencia	= dt_final_vigencia_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p
	and	dt_final_vigencia	is null;
end if;


end pls_finalizar_vig_preco;
/