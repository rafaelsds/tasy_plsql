create or replace
procedure pls_cancelar_agenda_pac_seg
			(	nr_seq_segurado_p	number,
				dt_rescisao_p		date,
				nr_seq_mtvo_rescisao_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

cd_agenda_w			number(10);
nr_seq_agenda_w			number(10);
cd_motivo_cancelamento_w	varchar2(15);
ds_motivo_w			varchar2(255);
nr_seq_motivo_cancel_agenda_w	number(10);
cd_pessoa_fisica_w		varchar2(10);
qt_beneficiarios_w		number(10);
cd_convenio_w			number(5);
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
ie_retorno_consulta_w		varchar2(1);

Cursor C01 is
	select	a.nr_sequencia,
		a.cd_agenda,
		a.cd_convenio,
		a.nr_seq_segurado
	from	agenda_paciente a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	a.dt_agenda		> dt_rescisao_p
	and	a.ie_status_agenda not in ('C','F');

Cursor C02 is
	select	a.nr_sequencia,
		a.cd_agenda,
		a.cd_convenio,
		a.nr_seq_segurado,
		(	select	decode(max(x.ie_tipo_classif),'R','S','N')
			from	agenda_classif x
			where	x.cd_classificacao = a.ie_classif_agenda) ie_retorno
	from	agenda_consulta a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	a.dt_agenda	> dt_rescisao_p
	and	a.ie_status_agenda not in ('C','F');

Cursor C03 is
	select	nr_seq_motivo_cancel_agenda
	from	pls_regra_cancel_agenda
	where	((cd_convenio = cd_convenio_w) or (cd_convenio is null))
	and	((nr_seq_motivo_cancelamento = nr_seq_mtvo_rescisao_p) or (nr_seq_motivo_cancelamento is null))
	and	((nvl(ie_considerar_retorno_consulta,'S') = 'S') or (nvl(ie_considerar_retorno_consulta,'S') = 'N' and ie_retorno_consulta_w <> 'S'))
	order by nvl(nr_seq_motivo_cancelamento,0);

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

open C01;
loop
fetch C01 into
	nr_seq_agenda_w,
	cd_agenda_w,
	cd_convenio_w,
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	ie_retorno_consulta_w	:= 'N';
	
	if	(nr_seq_segurado_w is not null) then --Se foi informado beneficiário na agenda, somente cancela a agenda quando rescindir esse beneficiário
		if	(nr_seq_segurado_w = nr_seq_segurado_p) then
			qt_beneficiarios_w	:= 0;
		else
			qt_beneficiarios_w	:= 1;
		end if;
	else
		select	count(*)
		into	qt_beneficiarios_w
		from	pls_segurado
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_sequencia		<> nr_seq_segurado_p
		and	dt_liberacao is not null
		and	dt_rescisao is null;
	end if;
	
	if	(qt_beneficiarios_w = 0) then
		nr_seq_motivo_cancel_agenda_w	:= null;
		open C03;
		loop
		fetch C03 into
			nr_seq_motivo_cancel_agenda_w;
		exit when C03%notfound;	
		end loop;
		close C03;
		
		if	(nr_seq_motivo_cancel_agenda_w is not null) then
			select	max(cd_motivo)
			into	cd_motivo_cancelamento_w
			from	agenda_motivo_cancelamento
			where	nr_sequencia	= nr_seq_motivo_cancel_agenda_w;
			
			Alterar_status_agenda(cd_agenda_w, nr_seq_agenda_w, 'C', cd_motivo_cancelamento_w, '', 'N', nm_usuario_p);
		end if;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	nr_seq_agenda_w,
	cd_agenda_w,
	cd_convenio_w,
	nr_seq_segurado_w,
	ie_retorno_consulta_w;
exit when C02%notfound;
	begin
	if	(nr_seq_segurado_w is not null) then --Se foi informado beneficiário na agenda, somente cancela a agenda quando rescindir esse beneficiário
		if	(nr_seq_segurado_w = nr_seq_segurado_p) then
			qt_beneficiarios_w	:= 0;
		else
			qt_beneficiarios_w	:= 1;
		end if;
	else
		select	count(*)
		into	qt_beneficiarios_w
		from	pls_segurado
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_sequencia		<> nr_seq_segurado_p
		and	dt_liberacao is not null
		and	dt_rescisao is null;
	end if;
	
	if	(qt_beneficiarios_w = 0) then
		nr_seq_motivo_cancel_agenda_w	:= null;
		open C03;
		loop
		fetch C03 into
			nr_seq_motivo_cancel_agenda_w;
		exit when C03%notfound;
		end loop;
		close C03;
		
		if	(nr_seq_motivo_cancel_agenda_w is not null) then
			select	max(cd_motivo)
			into	cd_motivo_cancelamento_w
			from	agenda_motivo_cancelamento
			where	nr_sequencia	= nr_seq_motivo_cancel_agenda_w;
			
			Alterar_Status_AgeCons(cd_agenda_w, nr_seq_agenda_w, 'C', cd_motivo_cancelamento_w, '', 'N', nm_usuario_p, null);
		end if;
	end if;
	end;
end loop;
close C02;

end pls_cancelar_agenda_pac_seg;
/