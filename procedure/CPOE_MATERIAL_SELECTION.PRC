
create or replace
procedure CPOE_MATERIAL_SELECTION(	nr_atendimento_p			number,
													
										cd_estabelecimento_p		number,
										cd_perfil_p					number,
										nm_usuario_p				varchar2,
										cd_pessoa_fisica_p			varchar2,
										nr_seq_item_gerado_p		out number,
										
										ie_prescritor_aux_p			varchar2 default 'N',
										cd_medico_p					number default null,
										cd_material_p          		cpoe_material.cd_material%type,
                                        nr_seq_cpoe_order_unit_p		cpoe_material.nr_seq_cpoe_order_unit%type) is

cd_material_w				material.cd_material%type;
cd_unidade_medida_w			Varchar2(30);


BEGIN
	
	cd_material_w := cd_material_p;
    
      if (nvl(nr_seq_cpoe_order_unit_p, 0) > 0) then

            select	max(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30))
            into	cd_unidade_medida_w
            from	material
            where	cd_material	= cd_material_w;			
            
            select	cpoe_material_seq.nextval
            into	nr_seq_item_gerado_p
            from	dual;
                
            insert into 
            cpoe_material(nr_sequencia, 
                          nr_atendimento, 
                          cd_pessoa_fisica,
                          cd_material, 
                          cd_unidade_medida, 
                          nm_usuario, 
                          nm_usuario_nrec, 
                          dt_atualizacao, 
                          dt_atualizacao_nrec, 
                          cd_perfil_ativo,
                          cd_funcao_origem,
                          ie_prescritor_aux,
                          cd_medico,
                          ie_material,
                          nr_seq_cpoe_order_unit
                          )			
            values(	nr_seq_item_gerado_p,
                    nr_atendimento_p, 
                    cd_pessoa_fisica_p,
                    cd_material_w,
                    cd_unidade_medida_w,
                    nm_usuario_p,
                    nm_usuario_p,
                    sysdate,
                    sysdate,
                    cd_perfil_p,
                    2314,
                    ie_prescritor_aux_p,
                    cd_medico_p,
                    'S',
                    nr_seq_cpoe_order_unit_p);		
            commit;
    end if;

end CPOE_MATERIAL_SELECTION;
/
