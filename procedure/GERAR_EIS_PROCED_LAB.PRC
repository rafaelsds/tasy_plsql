CREATE OR REPLACE
PROCEDURE Gerar_EIS_Proced_Lab
		(dt_parametro_P   		DATE,
  		NM_USUARIO_P      	VARCHAR2) IS 


dt_atualizacao_w                date          		:= SYSDATE;
dt_parametro_fim_w              date;
dt_parametro_w                  date;
dt_parametro_mes_w              date;
dt_parametro_mes_fim_w		date;
ds_retorno_w			varchar2(255);

CD_ESTABELECIMENTO_W		NUMBER(04,0);
CD_SETOR_ATENDIMENTO_W        	NUMBER(5,0); 
DT_HORA_EXECUCAO_W		NUMBER(02,0);
CD_PROCEDIMENTO_W             	NUMBER(15); 
CD_TECNICO_W		        VARCHAR2(10); 
CD_MEDICO_EXECUTOR_W          	VARCHAR2(10); 
cd_medico_prescr_w		varchar2(10);
CD_MEDICO_SOLICITANTE_W         VARCHAR2(10); 
CD_CONTA_CONTABIL_W           	VARCHAR2(20); 
IE_ORIGEM_PROCED_W            	NUMBER(10,0); 
CD_EQUIPAMENTO_W               	NUMBER(10,0);
IE_TIPO_ATENDIMENTO_W		NUMBER(3,0);
ie_clinica_w			number(5,0);
cd_tipo_procedimento_w		number(3,0);
cd_setor_prescr_w		number(5,0);
nr_seq_grupo_w			number(10,0);
cd_convenio_w			number(5,0);

QT_PROCEDIMENTO_W             	NUMBER(15,2); 
VL_MEDICO_W                   	NUMBER(15,2); 
VL_MATERIAIS_W                	NUMBER(15,2); 
VL_CUSTO_OPERACIONAL_W        	NUMBER(15,2);
VL_AUXILIARES_W		       	NUMBER(15,2);
VL_ANESTESISTA_W	       	NUMBER(15,2);
VL_CUSTO_W			NUMBER(15,2);
vl_custo_exame_laborat_w	number(15,4);
QT_DIAS_PRAZO_W			NUMBER(15,2) := 1;
nr_seq_exame_w			number(10,0);
nr_seq_exame_ant_w		number(10,0);
vl_procedimento_w		NUMBER(15,2);
nr_atendimento_w		number(10,0);
nr_atendimentos_ant_w		number(10,0);
qt_atendimento_w		number(5,0);
cd_setor_entrega_w		number(5);
nr_seq_forma_laudo_w		Number(10,0);
qt_itens_conv_w			Number(10,0);
cd_setor_proc_w			number(5);
cd_medico_resp_w		varchar2(10);
cd_motivo_exc_conta_w		number(3);
ds_motivo_exc_conta_w		varchar2(100);
ie_considera_excluidos_w 	varchar2(1);
dt_prescricao_w			date;
dt_aprovacao_w			date;
cd_pessoa_coleta_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(255);
dt_imp_internet_w		date;
ie_status_atend_w		number(2,0);
cd_procedencia_w		number(5,0);
qt_atend_exame_w		number(10,0);
nr_seq_prescr_w			number(6);
nr_prescricao_w			number(14);
nr_sequencia_w			number(10);
nr_seq_indicacao_w		number(10);
cd_categoria_w			varchar2(10);
cd_cgc_prestador_w		varchar2(14);
nm_usuario_atend_w		varchar2(15);
nr_seq_recoleta_w		number(10);

CURSOR C01 IS
	SELECT	/*+ INDEX(A PROPACI_I4) */
 		NVL(q.cd_estabelecimento, b.cd_estabelecimento),
		p.IE_ORIGEM_PROCED,		
 		p.CD_PROCEDIMENTO, 
 		NVL(a.CD_SETOR_ATENDIMENTO,0), 
		NVL(m.cd_setor_atendimento,0),
		TO_CHAR(a.DT_PROCEDIMENTO, 'HH24'),
		B.IE_TIPO_ATENDIMENTO,
		m.cd_medico cd_medico_prescr,
		b.ie_clinica,
 		SUM(a.QT_PROCEDIMENTO),
		d.nr_seq_grupo,
		d.nr_seq_exame,
		a.cd_convenio,
		nvl(a.cd_categoria,q.CD_CATEGORIA_PARAMETRO),
		SUM(vl_procedimento),
		b.nr_atendimento,
		m.cd_setor_entrega,
		m.nr_seq_forma_laudo,
		NVL(a.CD_SETOR_ATENDIMENTO,0),
		obter_bioq_aprov_exam_lab(m.nr_prescricao, p.nr_sequencia),
		a.cd_motivo_exc_conta,
		SUBSTR(obter_motivo_exc_conta(a.cd_motivo_exc_conta),1,100),
		NVL(obter_vl_exame_laborat(d.nr_seq_exame),0),
		m.dt_prescricao,
		obter_data_aprov_lab(m.nr_prescricao,p.nr_sequencia),
		Obter_Pf_Usuario(Obter_Lab_Exec_Etapa_coleta(p.nr_prescricao, p.nr_sequencia, p.ie_status_atend, '20', 'U'),'C'),
		b.cd_pessoa_fisica, --Obter_Pf_Usuario(obter_lab_execucao_etapa(p.nr_prescricao, p.nr_sequencia, '10', 'U'),'C'),		
		max(	(select TRUNC(max(r.dt_impressao),'dd') 
			from lab_prescr_proc_impressao r 
			where r.nr_prescricao = p.nr_prescricao 
			and r.nr_seq_prescr = p.nr_sequencia)) dt_impressao,
		p.ie_status_atend,
		b.cd_procedencia,
		p.nr_sequencia nr_seq_prescr,
		p.nr_prescricao,
		b.nr_seq_indicacao,
		a.cd_cgc_prestador,
		b.nm_usuario_atend,
		p.nr_seq_recoleta
      FROM	convenio c,
		conta_paciente q, 
		exame_laboratorio d,
		prescr_procedimento p,
		prescr_medica m,
		Atendimento_paciente b,
		Procedimento_paciente a
      WHERE 	a.dt_procedimento between  	dt_parametro_w and dt_parametro_fim_w
	AND  	a.nr_interno_conta		= q.nr_interno_conta(+)
	AND 	a.nr_atendimento		= b.nr_atendimento
	AND	p.nr_prescricao			= m.nr_prescricao
	AND 	a.nr_sequencia 			<> NVL(nr_seq_proc_pacote,0)
	AND 	((a.cd_motivo_exc_conta IS NULL) OR (ie_considera_excluidos_w = 'S'))
	AND	a.nr_seq_exame			= d.nr_seq_exame
	AND	a.nr_prescricao			= p.nr_prescricao
	AND	a.nr_sequencia_prescricao	= p.nr_sequencia
	AND 	a.cd_convenio         		= c.cd_convenio
	GROUP BY 
 		NVL(q.cd_estabelecimento, b.cd_estabelecimento),
		p.IE_ORIGEM_PROCED, 
 		p.CD_PROCEDIMENTO, 
 		p.CD_SETOR_ATENDIMENTO, 
		m.cd_setor_atendimento,
		a.nr_sequencia,
		TO_CHAR(a.DT_PROCEDIMENTO, 'HH24'),
		B.IE_TIPO_ATENDIMENTO,
		b.ie_clinica,
		d.nr_seq_grupo,
		m.cd_medico,
		d.nr_seq_exame,
		a.cd_convenio,
		b.nr_atendimento,
		m.cd_setor_entrega,
		m.nr_seq_forma_laudo,
		a.CD_SETOR_ATENDIMENTO,
		obter_bioq_aprov_exam_lab(m.nr_prescricao, p.nr_sequencia),
		a.cd_motivo_exc_conta,
		SUBSTR(obter_motivo_exc_conta(a.cd_motivo_exc_conta),1,100),
		NVL(obter_vl_exame_laborat(d.nr_seq_exame),0),
		nvl(a.cd_categoria,q.CD_CATEGORIA_PARAMETRO),
		m.dt_prescricao,
		obter_data_aprov_lab(m.nr_prescricao,p.nr_sequencia),
		Obter_Pf_Usuario(Obter_Lab_Exec_Etapa_coleta(p.nr_prescricao, p.nr_sequencia, p.ie_status_atend, '20', 'U'),'C'),
		b.cd_pessoa_fisica, --Obter_Pf_Usuario(obter_lab_execucao_etapa(p.nr_prescricao, p.nr_sequencia, '10', 'U'),'C'),
		p.ie_status_atend,
		b.cd_procedencia,
		p.nr_sequencia,
		p.nr_prescricao,
		b.nr_seq_indicacao,
		a.cd_cgc_prestador,
		b.nm_usuario_atend,
		p.nr_seq_recoleta
	ORDER BY b.nr_atendimento, d.nr_seq_exame;


BEGIN

Gravar_Log_Indicador(402, wheb_mensagem_pck.get_texto(802114), sysdate, trunc(dt_parametro_p), nm_usuario_p, nr_sequencia_w);

dt_parametro_fim_w              :=  
       		(to_date((to_char(dt_parametro_p,'dd/mm/yyyy') || ' 23:59:59'),
			'dd/mm/yyyy hh24:mi:ss'));
dt_parametro_w                  :=
       		(to_date((to_char(dt_parametro_p,'dd/mm/yyyy') || ' 00:00:00'),
			'dd/mm/yyyy hh24:mi:ss'));
dt_parametro_mes_w                    	:= Trunc(dt_parametro_p,'month');
dt_parametro_mes_fim_w			:= fim_mes(dt_parametro_mes_w);

/*delete from eis_proced_laborat
where dt_referencia 	< sysdate - 60
  and ie_periodo 		= 'D';*/

delete from eis_proced_laborat
where dt_referencia between dt_parametro_w and dt_parametro_fim_w;
commit;

nr_atendimentos_ant_w	:= 0;
nr_seq_exame_ant_w	:= 0;

select	nvl(max(ie_considera_exc_eis),'N')
into	ie_considera_excluidos_w
from	lab_parametro;


OPEN C01;
LOOP
	FETCH C01 into 	
		CD_ESTABELECIMENTO_W,
 		IE_ORIGEM_PROCED_W, 
		CD_PROCEDIMENTO_W, 
 		CD_SETOR_ATENDIMENTO_W, 
		cd_setor_prescr_w,
		DT_HORA_EXECUCAO_W,
		IE_TIPO_ATENDIMENTO_W,
		cd_medico_prescr_w,
		ie_clinica_w,
 		QT_PROCEDIMENTO_W,
		nr_seq_grupo_w,
		nr_seq_exame_w,
		cd_convenio_w,
		cd_categoria_w,
		vl_procedimento_w,
		nr_atendimento_w,
		cd_setor_entrega_w,
		nr_seq_forma_laudo_w,
		cd_setor_proc_w,
		cd_medico_resp_w,
		cd_motivo_exc_conta_w,
		ds_motivo_exc_conta_w,
		vl_custo_exame_laborat_w,
		dt_prescricao_w,
		dt_aprovacao_w,
		cd_pessoa_coleta_w,
		cd_pessoa_fisica_w,
		dt_imp_internet_w,
		ie_status_atend_w,
		cd_procedencia_w,
		nr_seq_prescr_w,
		nr_prescricao_w,
		nr_seq_indicacao_w,
		cd_cgc_prestador_w,
		nm_usuario_atend_w,
		nr_seq_recoleta_w;
      EXIT WHEN C01%NOTFOUND;
		begin

		select 	count(*)
		into	qt_itens_conv_w
		from 	convenio_estabelecimento w
		where 	w.cd_convenio                    = cd_convenio_w;

		if	(qt_itens_conv_w > 0) then

			select	cd_tipo_procedimento
			into	cd_tipo_procedimento_w
			from	procedimento
			where	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w;
	
			qt_atendimento_w	:= 0;
			qt_atend_exame_w	:= 0;
			
			if (nr_atendimento_w <> nr_atendimentos_ant_w) then
				qt_atendimento_w := 1;		
			end if;
			
			if (nr_atendimento_w <> nr_atendimentos_ant_w) or (nr_atendimento_w = nr_atendimentos_ant_w and nr_seq_exame_w <> nr_seq_exame_ant_w) then
				qt_atend_exame_w := 1;	
			end if;
		
			nr_atendimentos_ant_w := nr_atendimento_w;
			nr_seq_exame_ant_w := nr_seq_exame_w;
			
	 		insert into EIS_Proced_laborat (
				DT_REFERENCIA,
 				CD_ESTABELECIMENTO,               
 				CD_SETOR_ATENDIMENTO,               
	 			IE_ORIGEM_PROCED,               
 				CD_PROCEDIMENTO,               
				IE_PERIODO,
				NM_USUARIO,
				DT_ATUALIZACAO,
 				DT_HORA_EXAME,               
 				IE_TIPO_ATENDIMENTO,               
 				QT_PROCEDIMENTO,
	 			CD_MEDICO_prescr,               
				ie_clinica,
				cd_tipo_procedimento,
				cd_setor_prescr,
				nr_seq_grupo,
				nr_seq_exame,
				cd_convenio,
				vl_procedimento,
				qt_atendimento,
				cd_setor_entrega,
				nr_seq_forma_laudo,
				cd_setor_proc,
				cd_medico_resp,
				cd_motivo_exc,
				ds_motivo_exc,
				vl_custo_exame,
				dt_prescricao,
				dt_aprovacao,
				cd_pessoa_fisica,
				cd_pessoa_coleta,
				dt_impressao_internet,
				ie_status_atend,
				cd_procedencia,
				qt_atend_exam,
				nr_atendimento,
				nr_seq_prescr,
				nr_prescricao,
				nr_seq_indicacao,
				cd_categoria,
				cd_cgc_prestador,
				nm_usuario_atend,
				nr_seq_recoleta)
	        	Values(
				dt_Parametro_w    	,   
                	  	cd_estabelecimento_w    ,
                  		cd_setor_atendimento_w  ,
	                  	ie_origem_proced_w	,
				cd_procedimento_w		,
				'D',
				nm_usuario_p		,
				dt_atualizacao_w,
				DT_HORA_EXECUCAO_W	,
				ie_tipo_atendimento_w	,
				qt_procedimento_w		,
				cd_medico_prescr_w,
				ie_clinica_w,
				cd_tipo_procedimento_w,
				cd_setor_prescr_w,
				nr_seq_grupo_w, 
				nr_seq_exame_w,
				cd_convenio_w,
				vl_procedimento_w,
				qt_atendimento_w,
				cd_setor_entrega_w,
				nr_seq_forma_laudo_w,
				cd_setor_proc_w,
				cd_medico_resp_w,
				cd_motivo_exc_conta_w,
				ds_motivo_exc_conta_w,
				vl_custo_exame_laborat_w,
				dt_prescricao_w,
				dt_aprovacao_w,
				cd_pessoa_fisica_w,
				cd_pessoa_coleta_w,
				dt_imp_internet_w,
				ie_status_atend_w,
				cd_procedencia_w,
				qt_atend_exame_w,
				nr_atendimento_w,
				nr_seq_prescr_w,
				nr_prescricao_w,
				nr_seq_indicacao_w,
				cd_categoria_w,
				cd_cgc_prestador_w,
				nm_usuario_atend_w,
				nr_seq_recoleta_w);
		end if;
        	end;          
END LOOP;
CLOSE C01; 
COMMIT;

begin

/*delete from eis_proced_laborat
where dt_referencia = dt_parametro_mes_w
and ie_periodo = 'M';

insert into EIS_Proced_laborat (
		DT_REFERENCIA		,
 		CD_ESTABELECIMENTO      ,               
 		CD_SETOR_ATENDIMENTO    ,               
 		IE_ORIGEM_PROCED        ,               
 		CD_PROCEDIMENTO         ,               
		IE_PERIODO			,
		NM_USUARIO			,
		DT_ATUALIZACAO,
 		DT_HORA_EXAME           ,               
 		IE_TIPO_ATENDIMENTO     ,               
 		QT_PROCEDIMENTO		,
 		CD_MEDICO_prescr,               
		ie_clinica,
		cd_tipo_procedimento,
		cd_setor_prescr,
		nr_seq_grupo,
		nr_seq_exame,
		cd_convenio,
		vl_procedimento,
		qt_atendimento,
		cd_setor_entrega,
		nr_seq_forma_laudo,
		cd_setor_proc,
		cd_medico_resp,
		cd_motivo_exc,
		ds_motivo_exc,
		vl_custo_exame,
		dt_prescricao,
		dt_aprovacao,
		cd_pessoa_fisica,
		cd_pessoa_coleta,
		dt_impressao_internet,
		ie_status_atend,
		cd_procedencia,
		qt_atend_exam, 
		nr_atendimento,
		nr_seq_prescr,
		nr_prescricao,
		nr_seq_indicacao,
		cd_categoria,
		cd_cgc_prestador,
		nm_usuario_atend,
		nr_seq_recoleta)
(SELECT
		DT_PARAMETRO_MES_W	,
 		CD_ESTABELECIMENTO      ,               
 		CD_SETOR_ATENDIMENTO    ,               
 		IE_ORIGEM_PROCED        ,               
 		CD_PROCEDIMENTO         ,               
		'M',
		NM_USUARIO_P		,
		DT_ATUALIZACAO_W,
 		DT_HORA_EXAME           ,               
 		IE_TIPO_ATENDIMENTO     ,               
	 	SUM(QT_PROCEDIMENTO)	,
 		CD_MEDICO_prescr,               
		ie_clinica,
		cd_tipo_procedimento,
		cd_setor_prescr,
		nr_seq_grupo,
		nr_seq_exame,
		cd_convenio,
		sum(vl_procedimento),
		sum(nvl(qt_atendimento,0)),
		cd_setor_entrega,
		nr_seq_forma_laudo,
		cd_setor_proc,
		cd_medico_resp,
		cd_motivo_exc,
		ds_motivo_exc,
		vl_custo_exame,
		dt_prescricao,
		dt_aprovacao,
		cd_pessoa_fisica,
		cd_pessoa_coleta,
		dt_impressao_internet,
		ie_status_atend,
		cd_procedencia,
		sum(nvl(qt_atend_exam,0)),
		nr_atendimento,
		nr_seq_prescr,
		nr_prescricao,
		nr_seq_indicacao,
		cd_categoria,
		cd_cgc_prestador,
		nm_usuario_atend,
		nr_seq_recoleta
FROM 	EIS_PROCED_laborat
WHERE 	DT_REFERENCIA BETWEEN DT_PARAMETRO_MES_W AND DT_PARAMETRO_MES_FIM_W
AND 	IE_PERIODO = 'D'
GROUP BY 
	DT_PARAMETRO_MES_W	,
	CD_ESTABELECIMENTO      ,               
	CD_SETOR_ATENDIMENTO    ,               
	IE_ORIGEM_PROCED        ,               
	CD_PROCEDIMENTO         ,               
	'M',
	NM_USUARIO_P		,
	DT_ATUALIZACAO_W,
 	DT_HORA_EXAME           ,               
 	IE_TIPO_ATENDIMENTO     ,               
	CD_MEDICO_prescr,               
	ie_clinica,
	cd_tipo_procedimento,
	nr_seq_grupo,
	cd_setor_prescr,
	nr_seq_exame,
	cd_convenio,
	cd_setor_entrega,
	cd_setor_proc,
	nr_seq_forma_laudo,
	cd_medico_resp,
	cd_motivo_exc,
	ds_motivo_exc,
	vl_custo_exame,
	dt_prescricao,
	dt_aprovacao,
	cd_pessoa_fisica,
	cd_pessoa_coleta,
	dt_impressao_internet,
	ie_status_atend,
	cd_procedencia,
	nr_atendimento,
	nr_seq_prescr,
	nr_prescricao,
	nr_seq_indicacao,
	cd_categoria,
	cd_cgc_prestador,
	nm_usuario_atend,
	nr_seq_recoleta);*/
	
	
delete	EIS_Proced_laborat
where	ie_periodo = 'M'
and		DT_REFERENCIA between dt_parametro_w and dt_parametro_fim_w;	
	
insert into EIS_Proced_laborat (
				DT_REFERENCIA,
 				CD_ESTABELECIMENTO,               
 				CD_SETOR_ATENDIMENTO,               
	 			IE_ORIGEM_PROCED,               
 				CD_PROCEDIMENTO,               
				IE_PERIODO,
				NM_USUARIO,
				DT_ATUALIZACAO,
 				DT_HORA_EXAME,               
 				IE_TIPO_ATENDIMENTO,               
 				QT_PROCEDIMENTO,
	 			CD_MEDICO_prescr,               
				ie_clinica,
				cd_tipo_procedimento,
				cd_setor_prescr,
				nr_seq_grupo,
				nr_seq_exame,
				cd_convenio,
				vl_procedimento,
				qt_atendimento,
				cd_setor_entrega,
				nr_seq_forma_laudo,
				cd_setor_proc,
				cd_medico_resp,
				cd_motivo_exc,
				ds_motivo_exc,
				vl_custo_exame,
				dt_prescricao,
				dt_aprovacao,
				cd_pessoa_fisica,
				cd_pessoa_coleta,
				dt_impressao_internet,
				ie_status_atend,
				cd_procedencia,
				qt_atend_exam,
				nr_atendimento,
				nr_seq_prescr,
				nr_prescricao,
				nr_seq_indicacao,
				cd_categoria,
				cd_cgc_prestador,
				nm_usuario_atend,
				nr_seq_recoleta)
		select	DT_REFERENCIA,
 				CD_ESTABELECIMENTO,               
 				CD_SETOR_ATENDIMENTO,               
	 			IE_ORIGEM_PROCED,               
 				CD_PROCEDIMENTO,               
				'M',
				NM_USUARIO,
				DT_ATUALIZACAO,
 				DT_HORA_EXAME,               
 				IE_TIPO_ATENDIMENTO,               
 				QT_PROCEDIMENTO,
	 			CD_MEDICO_prescr,               
				ie_clinica,
				cd_tipo_procedimento,
				cd_setor_prescr,
				nr_seq_grupo,
				nr_seq_exame,
				cd_convenio,
				vl_procedimento,
				qt_atendimento,
				cd_setor_entrega,
				nr_seq_forma_laudo,
				cd_setor_proc,
				cd_medico_resp,
				cd_motivo_exc,
				ds_motivo_exc,
				vl_custo_exame,
				dt_prescricao,
				dt_aprovacao,
				cd_pessoa_fisica,
				cd_pessoa_coleta,
				dt_impressao_internet,
				ie_status_atend,
				cd_procedencia,
				qt_atend_exam,
				nr_atendimento,
				nr_seq_prescr,
				nr_prescricao,
				nr_seq_indicacao,
				cd_categoria,
				cd_cgc_prestador,
				nm_usuario_atend,
				nr_seq_recoleta
		from	EIS_Proced_laborat
		where	ie_periodo = 'D'
		and		DT_REFERENCIA between dt_parametro_w and dt_parametro_fim_w;
	
	
commit;
end;

Atualizar_Log_Indicador(sysdate, nr_sequencia_w);

commit;

END Gerar_EIS_Proced_Lab;
/
