create or replace
procedure pls_reabrir_processo
			(	nr_seq_processo_p	pls_processo.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type) is 
	
cont_w	number(10,0);

begin

select	count(*)
into	cont_w
from	titulo_pagar
where	nr_seq_processo = nr_seq_processo_p
and	ie_situacao 	<> 'C';

if	(cont_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(307293);	-- Este processo j� possui t�tulo gerado!
end if;

update	pls_processo
set	ie_status	= 'A'
where	nr_sequencia	= nr_seq_processo_p;

commit;

end pls_reabrir_processo;
/
