create or replace
procedure fis_gerar_arq_K100_icmsipi(	nr_seq_controle_p	number,
					nr_linha_p		in out number	) is 


-- VARIABLES
ds_linha_w  varchar2(8000);
ds_sep_w    varchar2(1) := '|';
qt_cursor_w number(10) := 0;
nr_vetor_w  number(10) := 0;
nr_sequencia_w number(10) := 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

-- FIS_EFD_ICMSIPI_ARQUIVO
nr_linha_w fis_efd_icmsipi_arquivo.nr_linha%type;

cursor c_reg_K100 is
select	*
from	fis_efd_icmsipi_K100
where	nr_seq_controle = nr_seq_controle_p;

/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_K100*/
type reg_c_reg_K100 is table of fis_efd_icmsipi_K100%RowType;
vetRegK100 reg_c_reg_K100;

/*Cria��o do array com o tipo sendo da tabela eespecificada - fis_efd_icmsipi_arquivo */
type registro is table of fis_efd_icmsipi_arquivo%rowtype index by pls_integer;
fis_registros_w registro;

begin

nm_usuario_w := Obter_Usuario_Ativo;
nr_linha_w   := nr_linha_p;

open c_reg_K100;
loop
fetch c_reg_K100 bulk collect into vetRegK100 limit 1000;
	for i in 1 .. vetRegK100.Count loop
	begin

	/*Incrementa a variavel para o array*/
	qt_cursor_w := qt_cursor_w + 1;

	nr_linha_w := nr_linha_w + 1;
	ds_linha_w := substr(ds_sep_w || vetRegK100(i).cd_reg ||
			     ds_sep_w || to_char(vetRegK100(i).dt_ini ,'ddmmyyyy') ||
			     ds_sep_w || to_char(vetRegK100(i).dt_fin ,'ddmmyyyy') || ds_sep_w,
			     1,
			     8000);
			     
	/*   Atualiza o sequencial de ordena��o de linhas do arquivo */
	select fis_efd_icmsipi_arquivo_seq.nextval into nr_sequencia_w from dual;  

	fis_registros_w(qt_cursor_w).nr_sequencia		:= nr_sequencia_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao_nrec	:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario_nrec		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao		:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario			:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).nr_seq_controle		:= nr_seq_controle_p;
	fis_registros_w(qt_cursor_w).nr_linha			:= nr_linha_w;
	fis_registros_w(qt_cursor_w).ds_arquivo			:= substr(ds_linha_w,1,4000);
	fis_registros_w(qt_cursor_w).ds_arquivo_compl		:= substr(ds_linha_w,4001,4000);
	fis_registros_w(qt_cursor_w).cd_registro		:=  vetRegK100(i).cd_reg;

	-------> K200
	/* Gera registros do K200*/
        fis_gerar_arq_K200_icmsipi(nr_seq_controle_p,
	                           vetRegK100(i).nr_sequencia,
                                   nr_linha_w);		 

	-------> K280
	/* Gera registros do K280*/
        fis_gerar_arq_K280_icmsipi(nr_seq_controle_p,
	                           vetRegK100(i).nr_sequencia,
                                   nr_linha_w);		 

	if (nr_vetor_w >= 1000) then
	begin
	/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_K100 */
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_arquivo values fis_registros_w(i);

		nr_vetor_w := 0;
		fis_registros_w.delete;

		commit;

	end;
	end if;

	/*incrementa variavel para realizar o forall quando chegar no valor limite*/
	nr_vetor_w := nr_vetor_w + 1;

	end;
	end loop;
exit when c_reg_K100%notfound;
end loop;
close c_reg_K100;

if (fis_registros_w.count > 0) then
begin
/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
forall i in fis_registros_w.first .. fis_registros_w.last
	insert into fis_efd_icmsipi_arquivo values fis_registros_w (i);

	fis_registros_w.delete;

	commit;

end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

nr_linha_p := nr_linha_w;

end fis_gerar_arq_K100_icmsipi;
/