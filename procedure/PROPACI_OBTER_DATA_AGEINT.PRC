create or replace
procedure Propaci_Obter_data_ageint(nr_prescricao_p	in  number,
				  nr_atendimento_p	in  number,
				  cd_procedimento_p	in  number,
				  ie_origem_proced_p	in  number,
				  dt_conta_p		in  date,
				  dt_agenda_integrada_p	out Date) is 

dt_agenda_integrada_w	Date;
				  
pragma autonomous_transaction;				
				
begin

begin
select  nvl(nvl(b.dt_transferencia, a.dt_inicio_agendamento), dt_conta_p)
into	dt_agenda_integrada_w
from    prescr_medica 		e,
	prescr_procedimento 	d,
	agenda_paciente 	c,
        agenda_integrada_item 	b,
        agenda_integrada 	a
where   a.nr_sequencia = b.nr_seq_agenda_int
and     c.nr_sequencia = b.nr_seq_agenda_exame
and     d.nr_seq_agenda = c.nr_sequencia
and     d.nr_prescricao = e.nr_prescricao
and 	e.nr_atendimento = nr_atendimento_p
and 	d.cd_procedimento = cd_procedimento_p
and 	d.ie_origem_proced = ie_origem_proced_p;
exception
    	when others then
      	begin
		select  nvl(nvl(b.dt_transferencia, a.dt_inicio_agendamento), dt_conta_p)
		into	dt_agenda_integrada_w
		from    prescr_medica 		e,
			prescr_procedimento 	d,
			agenda_consulta 	c, 
		        agenda_integrada_item 	b,
		        agenda_integrada 	a
		where   a.nr_sequencia 			= b.nr_seq_agenda_int
		and     c.nr_sequencia 			= b.nr_seq_agenda_cons
		and     d.nr_seq_agenda_cons	= c.nr_sequencia
		and     d.nr_prescricao 		= e.nr_prescricao
		and 	e.nr_atendimento 		= nr_atendimento_p
		and 	d.cd_procedimento 		= cd_procedimento_p
		and 	d.ie_origem_proced 		= ie_origem_proced_p;
		exception
    	when others then
			dt_agenda_integrada_w:= dt_conta_p;
		end;		
end;

dt_agenda_integrada_p:= dt_agenda_integrada_w;

commit;

end Propaci_Obter_data_ageint;
/
