create or replace
procedure pls_inserir_incon_intercambio(nr_seq_inconsistencia_p		w_pls_inconsistencia_int.nr_seq_inconsistencia%type,
					nr_seq_fatura_p			w_pls_inconsistencia_int.nr_seq_fatura%type,
					nr_seq_contestacao_p		w_pls_inconsistencia_int.nr_seq_contestacao%type,
					ds_complemento_p		w_pls_inconsistencia_int.ds_complemento%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_tipo_arquivo_p		w_pls_inconsistencia_int.ie_tipo_arquivo%type,
					nr_seq_lote_contest_p 		pls_lote_contestacao.nr_sequencia%type ) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar inconsistencias do intercambio
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
		
ds_observacao_w		varchar2(255);
		
begin
if	(nr_seq_inconsistencia_p is not null) then
	insert into w_pls_inconsistencia_int
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_fatura,
		nr_seq_contestacao,
		nr_seq_inconsistencia,
		ds_complemento,
		ie_tipo_arquivo)
	values	(w_pls_inconsistencia_int_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_fatura_p,
		nr_seq_contestacao_p,
		nr_seq_inconsistencia_p,
		ds_complemento_p,
		nvl(ie_tipo_arquivo_p,'A'));

	if	(nr_seq_lote_contest_p is not null) then
		select	max(ds_inconsistencia)
		into	ds_observacao_w
		from	pls_inconsistencia_int
		where	nr_sequencia	= nr_seq_inconsistencia_p;
		
		-- Gerar LOG
		pls_gerar_contest_log(	nr_seq_lote_contest_p, null, null, null, null, ds_observacao_w, 'CA550', 'N', nm_usuario_p);		
	end if;
	
end if;

end pls_inserir_incon_intercambio;
/