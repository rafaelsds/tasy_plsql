create or replace
procedure GERAR_FLUXO_MENSAL_PERIODO
		(cd_estabelecimento_p		in	number,
		cd_empresa_p			in	number,
		ie_restringe_estab_p		in	varchar2,
		dt_inicial_p			in	date,
		dt_final_p				in	date,
		dt_atual_p			in	date,
		nm_usuario_p			in	varchar2,
		qt_nivel_p			in	number,
		ie_classificacao_p		in	varchar2,
		vl_saldo_anterior_p		in	number,
		ie_data_atual_p			in	varchar2,
		ie_todas_contas_p		in	varchar2) as

dt_final_w		date;
dt_atual_w		date;
ie_classificaco_w	varchar2(5)	:= '';

-- 'RM' = Relatório Mensal

begin

dt_atual_w		:= null;

if	(ie_classificacao_p in ('P', 'PG')) then		-- passado

	ie_classificaco_w		:= ie_classificacao_p;

elsif	(ie_classificacao_p in ('R','RG')) then		-- a realizar

	ie_classificaco_w		:= ie_classificacao_p;

elsif	(ie_classificacao_p in ('PR','PRG')) then	-- passado/realizar
	
	if	(ie_data_atual_p = 'R') then
		dt_atual_w	:= nvl(dt_atual_p, sysdate);
	else
		dt_atual_w	:= nvl(dt_atual_p, sysdate) + 1;	
	end if;

	if	(ie_classificacao_p = 'PR') then
		ie_classificaco_w		:= '';
	else
		ie_classificaco_w		:= 'PRGM';
	end if;

end if;

dt_final_w		:= dt_final_p;
if	(dt_final_w > OBTER_DIA_UTIL_PERIODO(cd_estabelecimento_p, dt_inicial_p, 22)) then
	dt_final_w	:= OBTER_DIA_UTIL_PERIODO(cd_estabelecimento_p, dt_inicial_p, 22);
end if;

gerar_view_fluxo_caixa(	cd_estabelecimento_p, 
			dt_inicial_p, 
			dt_final_w, 
			dt_atual_w,
			'D',
			ie_classificaco_w || 'RM', 
			qt_nivel_p, 
			cd_empresa_p,
			ie_restringe_estab_p);

commit;

end GERAR_FLUXO_MENSAL_PERIODO;
/