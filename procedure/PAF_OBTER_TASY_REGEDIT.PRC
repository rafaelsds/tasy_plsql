create or replace 
procedure paf_obter_tasy_regedit (	
			ds_maquina_p		in	varchar2,
			ds_cliente_p		in	varchar2,
			qt_blob_cache_p		out	number,
			qt_blob_size_p		out	number,
			-- ds_dir_email_p		out	varchar2,
			ds_dir_padrao_p		out	varchar2,
			ds_dir_temp_p		out	varchar2,
			-- ds_email_logo_file_p	out	varchar2,
			ds_logo_file_p		out	varchar2) is
			-- ds_porta_serial_p	out	varchar2) is

cursor c01 is
	select	qt_blob_cache,
		qt_blob_size,
		-- ds_dir_email,
		ds_dir_padrao,
		ds_dir_temp,
		-- ds_email_logo_file,
		ds_logo_file
		-- ds_porta_serial
	from	tasy_regedit
	where	nvl(ds_maquina, ds_maquina_p) = upper(ds_maquina_p)
	and	nvl(ds_cliente, ds_cliente_p) = upper(ds_cliente_p)
	order by 
		nvl(ds_maquina, ''), nvl(ds_cliente, '');
		
begin

open C01;
loop
fetch C01 into	
	qt_blob_cache_p,
	qt_blob_size_p,
	-- ds_dir_email_p,
	ds_dir_padrao_p,
	ds_dir_temp_p,
	-- ds_email_logo_file_p,
	ds_logo_file_p;
	-- ds_porta_serial_p;
exit when C01%notfound;
end loop;
close C01;

end paf_obter_tasy_regedit;
/
