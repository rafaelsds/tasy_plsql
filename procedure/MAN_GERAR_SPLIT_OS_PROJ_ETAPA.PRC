create or replace procedure man_gerar_split_os_proj_etapa
			(	lista_os_p			out	varchar2,
				cd_pessoa_solicitante_p		in	varchar2,
				nr_seq_localizacao_p		in	number,
				nr_seq_equipamento_p		in	number,
				ds_dano_breve_p			in	varchar2,
				ds_dano_p			in	varchar2,
				cd_funcao_p			in	number,
				ie_classificacao_p		in	varchar2,
				ie_forma_receb_p		in	varchar2,
				dt_inicio_previsto_p		in	date,
				dt_fim_previsto_p		in	date,
				nr_seq_estagio_p		in	number,
				ie_tipo_ordem_p			in	varchar2,
				nr_grupo_planej_p		in	number,
				nr_grupo_trabalho_p		in	number,
				ie_prioridade_p			in	varchar2,
				ie_status_ordem_p		in	varchar2,
				nm_usuario_p			in	varchar2,
				nr_seq_proj_cron_etapa_p	in	number,
				nr_seq_grupo_des_p		in	number,
				nr_seq_projeto_p		in	number,
				nr_seq_ativ_exec_p		in	number,
				qt_horas_previstos_p		in	number,
				dt_inicio_p			in	date,
				cd_estabelecimento_p		in	number,
				nr_product_requirement_p	in	number,
				nr_seq_executor_p               in      number) is
	
nr_sequencia_w		number(10);
nm_usuario_exec_w	varchar2(15);
nr_seq_gerencia_w	number(10,0);
nr_seq_estagio_w	number(10,0);
dt_inicio_rec_w		date;
qt_horas_rec_w		number(15,2);
nm_usuario_rec_w	varchar2(15);
qt_horas_ratiada_w	number(15,2);
qt_min_ratiada_w	number(15);
i			number(2);
nr_seq_localizacao_w	number(10)	:= nr_seq_localizacao_p;
nr_seq_equipamento_w	number(10)	:= nr_seq_equipamento_p;
ds_arquivo_w			varchar2(200);
ds_observacao_w			varchar2(255);
ie_resp_teste_w			varchar2(15);
nr_seq_ordem_serv_w		proj_projeto.nr_seq_ordem_serv%type;
ie_plataforma_w			man_ordem_servico.ie_plataforma%type;
nr_seq_tipo_ordem_w		man_ordem_Servico.nr_seq_tipo_ordem%type;
nr_seq_localizacao_ww	number(10);
nr_seq_equipamento_ww	number(10);
nr_sequencia_hist_w 	number(10);
nr_sequencia_ord_tec_w	number(10);
base_corp_w		varchar2(1);
nr_seq_classif_w 	number(10);
nr_seq_severidade_wheb_w  varchar2(255);

Cursor C01 is
	select	ds_arquivo,
		ds_observacao
	from	proj_cron_etapa_anexo
	where	nr_seq_etapa_cron	= nr_seq_proj_cron_etapa_p
	order by 1;
	
Cursor C02 is
	select	nr_sequencia
	from	proj_cron_etapa_hist
	where	nr_seq_etapa_cron	= nr_seq_proj_cron_etapa_p
	and		dt_liberacao is not null
	order by 1;	
	
begin
select	max(a.nr_sequencia)
into	nr_seq_gerencia_w
from	gerencia_wheb a,
	grupo_desenvolvimento b
where	a.nr_sequencia	= b.nr_seq_gerencia
and	b.nr_sequencia	= nr_seq_grupo_des_p;

nr_seq_estagio_w	:= nr_seq_estagio_p;

select 	nvl(a.ie_resp_teste, 'A')
into	ie_resp_teste_w
from	proj_cronograma a,
	proj_cron_etapa b
where 	b.nr_seq_cronograma = a.nr_sequencia
and	b.nr_sequencia = nr_seq_proj_cron_etapa_p;

select	obter_se_base_corp
into	base_corp_w
from	dual;

if	(nr_seq_gerencia_w = 2) then
	nr_seq_estagio_w	:= 131;
end if;

/*Popula corordenador da etapa do Projeto.*/
select	substr(obter_usuario_pessoa(cd_coordenador),1,15),
	nr_seq_ordem_serv,
	nvl(nr_seq_classif,0)
into	nm_usuario_exec_w,
	nr_seq_ordem_serv_w,
	nr_seq_classif_w
from	proj_projeto
where	nr_sequencia	= nr_seq_projeto_p;

if      (nr_seq_executor_p is not null) then
        select  substr(obter_usuario_pessoa(cd_pessoa_fisica),1,15)
        into    nm_usuario_exec_w
        from    proj_equipe_papel
        where   nr_sequencia = nr_seq_executor_p;
end if;

select  max(nr_seq_severidade_wheb) 
into 	nr_seq_severidade_wheb_w
from 	man_ordem_servico
where nr_sequencia = nr_seq_ordem_serv_w;


select	max(ie_plataforma),
	max(a.nr_seq_localizacao),
	max(a.nr_seq_equipamento),
	max(a.nr_seq_tipo_ordem)
into	ie_plataforma_w,
	nr_seq_localizacao_ww,
	nr_seq_equipamento_ww,
	nr_seq_tipo_ordem_w
from	man_ordem_servico	a
where	a.nr_sequencia	= nr_seq_ordem_serv_w;

if	(nr_seq_localizacao_w is null) then
	nr_seq_localizacao_w	:= nr_seq_localizacao_ww;
	nr_seq_equipamento_w	:= nr_seq_equipamento_ww;
end if;

if 	(nr_seq_localizacao_w is null) and
	(base_corp_w = 'S') then
	begin
	nr_seq_localizacao_w	:= 1272;
	end;
end if;

if 	(nr_seq_equipamento_w is null) and
	(base_corp_w = 'S') then
	begin
	nr_seq_equipamento_w	:= 5267;
	end;
end if;

if (nr_seq_classif_w = 45) then
	nr_seq_equipamento_w	:= 41;
	nr_seq_localizacao_w	:= 41;
end if;


----------------REMOVIDO GERACAO DA OS DAQUI

/*Popula recurso do Projeto*/
select	nvl(max(dt_inicio_prev),sysdate+1)-1,
	max(qt_hora_prev)
into	dt_inicio_rec_w,
	qt_horas_rec_w
from	proj_cron_etapa
where	nr_sequencia = nr_seq_proj_cron_etapa_p;

if	(nvl(qt_horas_previstos_p,0) <> 0) then
	qt_horas_rec_w := qt_horas_previstos_p;
end if;

if	(dt_inicio_p is not null) then
	dt_inicio_rec_w := dt_inicio_p - 1;
end if;

select	substr(obter_usuario_pf(max(cd_programador)),1,15)
into	nm_usuario_rec_w
from	proj_cron_etapa_equipe
where	nr_seq_etapa_cron = nr_seq_proj_cron_etapa_p;

if	(nm_usuario_rec_w is null) then
	/*rollback;*/
	/*Problema na leitura dos dados do atendimento #@NR_SEQ_ETAPA#@*/
	/*wheb_mensagem_pck.exibir_mensagem_abort(245186,'NR_SEQ_ETAPA='|| nr_seq_proj_cron_etapa_p);*/

	nm_usuario_rec_w:=nm_usuario_exec_w;
end if;

/*Caso houverem mais de 4 horas previstas, divide em dias separados.*/
qt_horas_ratiada_w:= qt_horas_rec_w;

for i in 1..ceil(dividir(qt_horas_rec_w,4)) loop
	begin
	
	/*Efetua o rateamento dos minutos previstos*/
	if	(qt_horas_ratiada_w > 4) then
		qt_horas_ratiada_w:= (qt_horas_ratiada_w-4);
		qt_min_ratiada_w	:= 240;
	else
		qt_min_ratiada_w	:= (qt_horas_ratiada_w*60);
	end if;

	/*Verifica uma data valida */
	if	(mod(i, 2) > 0) then
		dt_inicio_rec_w:= dt_inicio_rec_w+1;
	end if;
	
	while (Obter_Se_Dia_Util(dt_inicio_rec_w,cd_estabelecimento_p) = 'N') loop
		begin
		dt_inicio_rec_w:=dt_inicio_rec_w+1;
		end;
	end loop;
	
	
	select	man_ordem_servico_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert	into man_ordem_servico
		(nr_sequencia,
		nr_seq_localizacao,
		nr_seq_equipamento,
		cd_pessoa_solicitante,
		dt_ordem_servico,
		ie_prioridade,
		ie_parado,
		ds_dano_breve,
		dt_atualizacao,
		nm_usuario,
		dt_inicio_desejado,
		dt_conclusao_desejada,
		ds_dano,
		dt_inicio_previsto,
		dt_fim_previsto,
		dt_inicio_real,
		dt_fim_real,
		ie_tipo_ordem,
		ie_status_ordem,
		nr_grupo_planej,
		nr_grupo_trabalho,
		nr_seq_tipo_solucao,
		ds_solucao,
		nm_usuario_exec,
		qt_contador,
		nr_seq_planej,
		nr_seq_tipo_contador,
		nr_seq_estagio,
		cd_projeto,
		nr_seq_etapa_proj,
		dt_reabertura,
		cd_funcao,
		nm_tabela,
		ie_classificacao,
		nr_seq_origem,
		nr_seq_projeto,
		ie_grau_satisfacao,
		nr_seq_indicador,
		nr_seq_causa_dano,
		ie_forma_receb,
		nr_seq_cliente,
		nr_seq_grupo_des,
		nr_seq_grupo_sup,
		nr_seq_superior,
		ie_eficacia,
		dt_prev_eficacia,
		cd_pf_eficacia,
		nr_seq_nao_conform,
		nr_seq_complex,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_obriga_news,
		nr_seq_meta_pe,
		nr_seq_classif,
		nr_seq_nivel_valor,
		nm_usuario_lib_news,
		dt_libera_news,
		dt_envio_wheb,
		ds_contato_solicitante,
		ie_prioridade_desen,
		ie_prioridade_sup,
		nr_seq_proj_cron_etapa,
		ie_exclusiva,
		ie_origem_os,
		ie_resp_teste,
		ie_plataforma,
		ie_gerado_projeto,
		nr_seq_severidade_wheb,
		nr_seq_tipo_ordem,
		ds_just_ret_news)
	values	(nr_sequencia_w,						-- nr_sequencia,
		nr_seq_localizacao_w,					-- nr_seq_localizacao,
		nr_seq_equipamento_w,					-- nr_seq_equipamento,
		cd_pessoa_solicitante_p,				-- cd_pessoa_solicitante,
		sysdate,								-- dt_ordem_servico,
		'M',	         						-- ie_prioridade,
		'N',									-- ie_parado,
		substr(ds_dano_breve_p,1,80),			-- ds_dano_breve,
		sysdate,								-- dt_atualizacao,
		nm_usuario_p,							-- nm_usuario,
		sysdate+1,								-- dt_inicio_desejado,
		(sysdate + 15),							-- dt_conclusao_desejada,
		ds_dano_p,								-- ds_dano,
		nvl(dt_inicio_previsto_p,sysdate+1),	-- dt_inicio_previsto,
		dt_fim_previsto_p,						-- dt_fim_previsto,
		sysdate+1,								-- dt_inicio_real,
		null,									-- dt_fim_real,
		decode(nr_seq_classif_w, 45, '4', nvl(ie_tipo_ordem_p,'4')),				-- ie_tipo_ordem,
		nvl(ie_status_ordem_p,'1'),				-- ie_status_ordem,
		nr_grupo_planej_p,						-- nr_grupo_planej,
		nr_grupo_trabalho_p,					-- nr_grupo_trabalho,
		null,									-- nr_seq_tipo_solucao,
		null,									-- ds_solucao,
		decode(ie_status_ordem_p,'1',null,nm_usuario_p), -- nm_usuario_exec,
		null,					-- qt_contador,
		null,					-- nr_seq_planej,
		null,					-- nr_seq_tipo_contador,
		nr_seq_estagio_w,				-- nr_seq_estagio,
		null,					-- cd_projeto,
		null,					-- nr_seq_etapa_proj,
		null,					-- dt_reabertura,
		cd_funcao_p,				-- cd_funcao,
		null,					-- nm_tabela,
		ie_classificacao_p,				-- ie_classificacao,
		null,					-- nr_seq_origem,
		null,					-- nr_seq_projeto,
		null,					-- ie_grau_satisfacao,
		null,					-- nr_seq_indicador,
		null,					-- nr_seq_causa_dano,
		nvl(ie_forma_receb_p,'W'),			-- ie_forma_receb,
		null,					-- nr_seq_cliente,
		nr_seq_grupo_des_p,			-- nr_seq_grupo_des,
		null,					-- nr_seq_grupo_sup,
		null,					-- nr_seq_superior,
		null,					-- ie_eficacia,
		null,					-- dt_prev_eficacia,
		null,					-- cd_pf_eficacia,
		null,					-- nr_seq_nao_conform,
		2,					-- nr_seq_complex,
		null,					-- dt_atualizacao_nrec,
		nm_usuario_p,				-- nm_usuario_nrec,
		'N',					-- ie_obriga_news,
		null,					-- nr_seq_meta_pe,
		2,					-- nr_seq_classif,
		1,					-- nr_seq_nivel_valor,
		null,					-- nm_usuario_lib_news,
		null,					-- dt_libera_news,
		null,					-- dt_envio_wheb,
		null,					-- ds_contato_solicitante,
		null,					-- ie_prioridade_desen,
		null,					-- ie_prioridade_sup
		nr_seq_proj_cron_etapa_p,			-- nr_seq_proj_cron_etapa
		'P',					-- ie_exclusiva
		'1',					--ie_origem
		ie_resp_teste_w,			--ie_resp_teste	
		ie_plataforma_w,
		'S',
		nr_seq_severidade_wheb_w,--nr_seq_severidade_wheb
		nr_seq_tipo_ordem_w,
		wheb_mensagem_pck.get_texto(1079530));   

	insert 	into proj_ordem_servico
		(nr_sequencia,
		nr_seq_proj,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ordem,
		nr_seq_cron_etapa,
		dt_acordo_projeto,
		dt_proposta_cliente,
		nr_seq_setor_cli,
		ie_virada,
		ie_origem_ordem)
	Values	(proj_ordem_servico_seq.nextval,
		nr_seq_projeto_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		null,
		null,
		null,
		null,
		null,
		'P');

	if 	(nm_usuario_exec_w is not null) then
		begin
		insert into man_ordem_servico_exec(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_exec,
			dt_recebimento,
			dt_fim_execucao)
		values (man_ordem_servico_exec_seq.nextval,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nm_usuario_exec_w,
			sysdate+1,
			sysdate);
		end;
	end if;
        
        if 	(nm_usuario_rec_w is not null) then
                begin
                insert into man_ordem_servico_exec
                        (nr_sequencia,
                        nr_seq_ordem,
                        dt_atualizacao,
                        nm_usuario,
                        nm_usuario_exec,
                        dt_recebimento,
                        nr_seq_funcao)
                values (man_ordem_servico_exec_seq.nextval,
                        nr_sequencia_w,
                        sysdate,
                        nm_usuario_p,
                        nm_usuario_rec_w,
                        sysdate+1,
                        11);
                end;
        end if;

	insert into man_ordem_ativ_prev
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_ordem_serv,
		dt_prevista,
		qt_min_prev,
		nm_usuario_prev,
		ie_prioridade_desen,
		nr_seq_ativ_exec)
	values	(man_ordem_ativ_prev_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		trunc(dt_inicio_rec_w),
		qt_min_ratiada_w,
		nm_usuario_rec_w,
		10,
		nvl(nr_seq_ativ_exec_p,10));
		
	man_gerar_previsao_dia(dt_inicio_rec_w,nm_usuario_rec_w);
	
	open C01;
	loop
	fetch C01 into ds_arquivo_w,
		ds_observacao_w;
	exit when C01%notfound;
		begin
		insert	into man_ordem_serv_arq
			(nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			ds_arquivo,
			ie_anexar_email,
			ds_observacao)
		values	(man_ordem_serv_arq_seq.NextVal,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			ds_arquivo_w,
			'N',
			ds_observacao_w);
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into
		nr_sequencia_hist_w;
	exit when C02%notfound;
		begin
		select 	man_ordem_serv_tecnico_seq.nextval
		into	nr_sequencia_ord_tec_w
		from	dual;
		
		insert into
			man_ordem_serv_tecnico (
				nm_usuario,
				dt_atualizacao,
				ds_relat_tecnico,
				nr_sequencia,
				nr_seq_ordem_serv,
				ie_origem,
				dt_historico,
				nr_seq_tipo
			) values (
				nm_usuario_p,
				sysdate,
				'  ',
				nr_sequencia_ord_tec_w,
				nr_sequencia_w,
				'I',
				sysdate,
				7);	

		--commit;
		
		copia_campo_long_de_para_novo(
				'PROJ_CRON_ETAPA_HIST', 
				'DS_HISTORICO', 
				' WHERE NR_SEQUENCIA = :NR_SEQUENCIA ',
				'NR_SEQUENCIA='||nr_sequencia_hist_w,
				'MAN_ORDEM_SERV_TECNICO',
				'DS_RELAT_TECNICO',
				' WHERE NR_SEQUENCIA = :NR_SEQUENCIA ',
				'NR_SEQUENCIA='||nr_sequencia_ord_tec_w,
				'L');

		man_liberar_historico(nr_sequencia_ord_tec_w, nm_usuario_p);
		
		end;
	end loop;
	close C02;
	
	proj_do_impact_analysis (nr_sequencia_w, nr_product_requirement_p, nm_usuario_p);
	
	if (i = 1) then
		lista_os_p := nr_sequencia_w;
	else
		lista_os_p := lista_os_p || ',' || nr_sequencia_w;
	end if;
	
	
	end;	
end loop;

end man_gerar_split_os_proj_etapa;
/
