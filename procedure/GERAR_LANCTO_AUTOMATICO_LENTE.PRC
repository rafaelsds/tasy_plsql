create or replace
procedure gerar_lancto_automatico_lente(	nr_atendimento_p		number,
														nr_seq_consulta_p		number,
														cd_local_estoque_p	number,
														nm_usuario_p			varchar2) is 

Cursor C01 is
	select	nr_sequencia
	from	oft_consulta_lente
	where	nr_seq_consulta	= nr_seq_consulta_p
	and	dt_lancamento is null;

						
nr_sequencia_w		number(15);
cd_setor_atendimento_w	number(5);
cd_setor_usuario_w	number(5);
dt_entrada_unidade_w	date;
cd_unidade_medida_w	varchar2(30);
nr_seq_atepacu_w	number(10);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
nr_doc_convenio_w	varchar2(20);
ie_tipo_guia_w		varchar2(2);
cd_senha_w		varchar2(20);
cd_estab_w		number(10);
dt_entrada_unidade_ww	date;
cd_pessoa_fisica_w	varchar2(10);
nr_seq_interno_w	number(10,0);
nr_seq_lente_w		number(10,0);
cd_material_w		number(6,0);
nr_seq_lente_ww		number(10,0);
cd_local_estoque_w	number(4) := null;
ie_local_estoque_w	varchar2(15);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_perfil_w				perfil.cd_perfil%type;
nm_usuario_w			usuario.nm_usuario%type;




begin
cd_estabelecimento_w		:= wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w					:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w				:= wheb_usuario_pck.get_nm_usuario;
cd_setor_usuario_w		:= wheb_usuario_pck.get_cd_setor_atendimento;

cd_local_estoque_w 		:= cd_local_estoque_p;
if	(nvl(cd_local_estoque_w,0) = 0) then
	obter_param_usuario(3010, 2, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_local_estoque_w);
	if	(ie_local_estoque_w = '1') then
		select 	max(cd_local_estoque)
		into		cd_local_estoque_w
		from 		setor_atendimento
		where  	cd_setor_atendimento = cd_setor_usuario_w;
	end if;	

end if;

nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_p,'A');

obter_convenio_execucao(nr_atendimento_p, sysdate, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);

select	cd_estabelecimento
into	cd_estab_w
from  	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

if 	((nr_seq_consulta_p > 0) and (nr_seq_atepacu_w > 0)) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_lente_ww;
	exit when C01%notfound;
		begin
		
		select	nvl(max(nr_seq_lente),0)
		into	nr_seq_lente_w
		from	oft_consulta_lente
		where	nr_sequencia	=	nr_seq_lente_ww;	
		
		if	(nr_seq_lente_w > 0) then
			select	nvl(max(cd_material),0)
			into	cd_material_w
			from	oft_lente_contato
			where	nr_sequencia	=	nr_seq_lente_w;
		end if;

		if	(cd_material_w > 0) then

			select	cd_setor_Atendimento,
				dt_entrada_unidade
			into	cd_setor_atendimento_w,
				dt_entrada_unidade_w
			from	atend_paciente_unidade
			where	nr_seq_interno = nr_seq_atepacu_w;

			select	material_atend_paciente_seq.nextval
			into	nr_sequencia_w
			from	dual;

			select	substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo
			into	cd_unidade_medida_w
			from	material
			where	cd_material = cd_material_w;

			
			insert into material_atend_paciente(	
					nr_sequencia,
					cd_material,
					dt_atendimento,
					cd_senha,
					cd_convenio,
					nr_doc_convenio,
					ie_tipo_guia,
					cd_categoria,
					nr_seq_atepacu,
					cd_setor_atendimento,
					cd_material_exec,
					qt_executada,
					vl_unitario,
					qt_ajuste_conta,
					cd_situacao_glosa,
					ie_valor_informado,
					ie_guia_informada,
					ie_auditoria,
					dt_entrada_unidade,
					qt_material,
					cd_local_estoque,
					dt_Atualizacao,
					nm_usuario,
					nm_usuario_original,
					nr_atendimento,
					cd_unidade_medida,
					cd_acao)
			values	(	nr_sequencia_w,
					cd_material_w,
					sysdate,
					cd_senha_w,
					cd_convenio_w,
					nr_doc_convenio_w,
					ie_tipo_guia_w,
					cd_categoria_w,
					nr_seq_atepacu_w,
					cd_setor_atendimento_w,
					cd_material_w,
					1,
					0,
					0,
					0,
					'N',
					'N',
					'N',
					dt_entrada_unidade_w,
					1,
					cd_local_estoque_w,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nr_atendimento_p,
					cd_unidade_medida_w,
					'1');
								
			atualiza_preco_material(nr_sequencia_w, nm_usuario_p);					
			
			update	oft_consulta_lente
			set	dt_lancamento	=	sysdate
			where	nr_sequencia	=	nr_seq_lente_ww;
		end if;
		end;
	end loop;
	close C01;
	

						
commit;

end if;

end gerar_lancto_automatico_lente;
/