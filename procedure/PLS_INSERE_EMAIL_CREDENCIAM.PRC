create or replace procedure pls_insere_email_credenciam (
    nr_seq_mensagem_email_p   number,
    ds_mensagem_p             varchar2,
    nr_seq_grupo_p            number,
    cd_pessoa_fisica_p        varchar2) is

nr_sequencia_w  pls_email.nr_sequencia%type;
ds_assunto_w    pls_mensagem_email.ds_assunto%type;
ds_email_w      varchar2(255);

cursor c01 is
select 
    w.ds_email
from
    wsuite_usuario        w,
    subject_client        s,
    client                c,
    pls_grupo_prestador   p
where
    w.ie_situacao = 'A'
    and w.id_subject = s.id_subject
    and w.nr_sequencia = p.nr_seq_wsuite_usuario
    and s.id_client = c.id
    and c.ds_client in ('PROVIDERACCREDITATION', 'HEALTHINSURANCE', 'PROVIDERHPMS')
    and p.nr_seq_grupo_email = nr_seq_grupo_p
union
select
    substr(obter_dados_pf_pj(p.cd_pessoa_fisica, null, 'M'), 1, 255) ds_email
from
    pls_prestador_colaborador   p,
    pls_grupo_prestador         g
where
    p.nr_sequencia = g.nr_seq_prestador_colaborador
    and g.nr_seq_grupo_email = nr_seq_grupo_p
union
select
    e.ds_email
from
    pls_prestador_email   e,
    pls_prestador         pp,
    pls_grupo_prestador   g
where
    e.nr_seq_prestador = pp.nr_sequencia
    and e.nr_sequencia = g.nr_seq_prestador_email
    and g.nr_seq_grupo_email = nr_seq_grupo_p
order by 1;

begin
    if ( nr_seq_mensagem_email_p is not null and nr_seq_grupo_p is not null ) then
        select
            max(ds_assunto)
        into ds_assunto_w
        from
            pls_mensagem_email
        where
            nr_sequencia = nr_seq_mensagem_email_p;

        open c01;
            loop
                fetch c01 into ds_email_w;
                exit when c01%notfound;
                begin
                    if ( ds_email_w is not null ) then
                        select pls_email_seq.nextval 
                        into nr_sequencia_w
                        from dual;

                        insert into pls_email (
                            nr_sequencia,
                            cd_estabelecimento,
                            nm_usuario_nrec,
                            dt_atualizacao_nrec,
                            nm_usuario,
                            dt_atualizacao,
                            ie_tipo_mensagem,
                            ie_status,
                            ie_origem,
                            ds_remetente,
                            ds_mensagem,
                            ds_destinatario,
                            ds_assunto,
                            cd_prioridade,
                            cd_pessoa_fisica
                        ) values (
                            nr_sequencia_w,
                            wheb_usuario_pck.get_cd_estabelecimento,
                            wheb_usuario_pck.get_nm_usuario,
                            sysdate,
                            wheb_usuario_pck.get_nm_usuario,
                            sysdate,
                            '9',
                            'P',
                            '9',
                            null,
                            ds_mensagem_p,
                            ds_email_w,
                            ds_assunto_w,
                            3,
                            cd_pessoa_fisica_p
                        );
                        
                        if ( nr_sequencia_w is not null ) then
                            insert into pls_hist_mens_prestador (
                                nr_sequencia,
                                cd_estabelecimento,
                                nm_usuario_nrec,
                                dt_atualizacao_nrec,
                                nm_usuario,
                                dt_atualizacao,
                                nr_seq_email
                            ) values (
                                pls_hist_mens_prestador_seq.nextval,
                                wheb_usuario_pck.get_cd_estabelecimento,
                                wheb_usuario_pck.get_nm_usuario,
                                sysdate,
                                wheb_usuario_pck.get_nm_usuario,
                                sysdate,
                                nr_sequencia_w
                            );
                        end if;
                    end if;
                end;
            end loop;
        close c01;
        
        commit;
    end if;
end pls_insere_email_credenciam;
/
