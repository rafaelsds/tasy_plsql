CREATE OR REPLACE
PROCEDURE Eliminar_orientacao_checkup(	nr_sequencia_p	Number,
					ie_opcao_p	Varchar2) is

nr_seq_orientacao_w	Number(10);
qt_existe_w		Number(10)	:= 0;
nr_atendimento_w	Number(10);
nr_sequencia_w		Number(10);

Cursor C01 is
	select	b.nr_seq_orientacao,
		a.nr_atendimento,
		b.nr_sequencia
	from	checkup_diag_pac_orient b,
		checkup_diagnostico a
	where	a.nr_sequencia	= nr_sequencia_p
	and	a.nr_sequencia	= b.nr_seq_diag;

BEGIN

if	(ie_opcao_p = 'I') or
	(ie_opcao_p = 'E') then
	select	b.nr_seq_orientacao,
		a.nr_atendimento
	into	nr_seq_orientacao_w,
		nr_atendimento_w
	from	checkup_diag_pac_orient b,
		checkup_diagnostico a
	where	b.nr_sequencia	= nr_sequencia_p
	and	a.nr_sequencia	= b.nr_seq_diag;
end if;

if	(ie_opcao_p = 'E') then
	select	count(*)
	into	qt_existe_w
	from	checkup_diag_pac_orient b,
		checkup_diagnostico a
	where	b.nr_sequencia		<> nr_sequencia_p
	and	a.nr_sequencia		= b.nr_seq_diag
	and	a.nr_atendimento	= nr_atendimento_w
	and	b.nr_seq_orientacao	= nr_seq_orientacao_w
	and	a.ie_situacao		= 'A'
	and	b.ie_situacao		= 'A';

elsif	(ie_opcao_p = 'I') then
	select	count(*)
	into	qt_existe_w
	from	checkup_diag_pac_orient b,
		checkup_diagnostico a
	where	a.nr_sequencia		= b.nr_seq_diag
	and	a.nr_atendimento	= nr_atendimento_w
	and	b.nr_seq_orientacao	= nr_seq_orientacao_w
	and	a.ie_situacao		= 'A'
	and	b.ie_situacao		= 'A';
	
elsif	(ie_opcao_p = 'ED') or
	(ie_opcao_p = 'ID') then
	open c01;
	loop
		fetch c01 into	
			nr_seq_orientacao_w,
			nr_atendimento_w,
			nr_sequencia_w;
		exit when c01%notfound;
		
		select	count(*)
		into	qt_existe_w
		from	checkup_diag_pac_orient b,
			checkup_diagnostico a
		where	b.nr_sequencia		<> nr_sequencia_w
		and	a.nr_sequencia		= b.nr_seq_diag
		and	a.nr_atendimento	= nr_atendimento_w
		and	b.nr_seq_orientacao	= nr_seq_orientacao_w
		and	a.ie_situacao		= 'A'
		and	b.ie_situacao		= 'A';	
		
		if	(qt_existe_w = 0) then
			delete	checkup_orientacao
			where	nr_atendimento		= nr_atendimento_w
			and	nr_seq_orientacao	= nr_seq_orientacao_w;
		end if;

	end loop;
	close c01;
end if;

if	(qt_existe_w = 0) then
	delete	checkup_orientacao
	where	nr_atendimento		= nr_atendimento_w
	and	nr_seq_orientacao	= nr_seq_orientacao_w;
end if;

END Eliminar_orientacao_checkup;
/
