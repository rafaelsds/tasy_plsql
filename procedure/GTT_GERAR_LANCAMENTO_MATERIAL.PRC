create or replace
procedure gtt_gerar_lancamento_material(
			nr_atendimento_p		in	number,
			cd_local_estoque_p	in	number,
			cd_material_p		in	number,
			qt_atual_p		in	number,
			ie_tipo_lancamento_p	in	varchar2,
			nm_usuario_p		in	varchar2,
			cd_fornecedor_p		in	varchar2,
			ie_atualizou_p		out	varchar2,
			ds_erro_p			out	varchar2) is

cd_estabelecimento_w		number(4);
qt_estoque_w			number(15,4);
qt_material_w			number(15,4);
qt_devolvida_w			number(15,4);
cd_acao_w			varchar2(1) := '1';
cd_fornec_consignado_w		varchar2(14);
cd_setor_atendimento_w		number(5);

ie_cobra_paciente_w		varchar2(1);
cd_unidade_medida_w		varchar2(30);

cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w		varchar2(20);
ie_tipo_guia_w			varchar2(2);
cd_senha_w			varchar2(20);
dt_entrada_unidade_w		date;
cd_centro_custo_w			number(8);
cd_conta_contabil_w		varchar2(20);
cd_operacao_estoque_w		number(3);
nr_seq_atepacu_w			number(10);
ie_consignado_w			varchar2(1);
ie_atualizar_consig_w		varchar2(1);
ds_erro_w			varchar2(255);
nr_sequencia_w			number(10);

begin
ie_atualizou_p			:= 'N';

begin
select	max(cd_setor_atendimento)
into	cd_setor_atendimento_w
from	setor_local
where	cd_local_estoque = cd_local_estoque_p;
exception
when others then
	ds_erro_w := 'N�o encontrado setor de atendimento para o local de estoque!';
end;

if	(nvl(cd_setor_atendimento_w,0) = 0) then
	ds_erro_w := 'N�o encontrado setor de atendimento para o local de estoque!';
end if;

begin
select	cd_estabelecimento
into	cd_estabelecimento_w
from	local_estoque
where	cd_local_estoque = cd_local_estoque_p;
exception
when others then
	ds_erro_w := 'N�o cadastro do local de estoque no Tasy!';
end;

begin
select	cd_unidade_medida_estoque,
	ie_cobra_paciente,
	nvl(ie_consignado,'0')
into	cd_unidade_medida_w,
	ie_cobra_paciente_w,
	ie_consignado_w
from	material
where	cd_material = cd_material_p;
exception
when others then
	ds_erro_w := 'N�o encontrado cadastro do material no Tasy!';
end;

if	(ie_consignado_w <> '0') and
	(cd_fornecedor_p is not null) then
	begin
	ie_atualizar_consig_w	:= 'S';

	begin
	select	cd_cgc
	into	cd_fornec_consignado_w
	from	pessoa_juridica
	where	cd_cgc = cd_fornecedor_p;
	exception
	when others then
		ds_erro_w := substr('N�o encontrado cadastro do fornecedor no Tasy! CGC: ' || cd_fornecedor_p,1,255);
	end;

	end;
else
	begin
	ie_atualizar_consig_w	:= 'N';
	cd_fornec_consignado_w	:= null;
	end;
end if;

--qt_material_w := abs(qt_atual_p-qt_estoque_w); Retirado pois a GTT ir� passar a quantidade utilizada.
qt_material_w := qt_atual_p;

/*retirado o tratamento, para que seja considerado somente o novo parametro - OS383378
if	(qt_atual_p > qt_estoque_w) then
	cd_acao_w := '2';
end if;*/

if	(ie_tipo_lancamento_p = 'L') then
	cd_acao_w := '1';
elsif	(ie_tipo_lancamento_p = 'R') then
	cd_acao_w := '2';
else
	ds_erro_w := substr('O tipo de lan�amento n�o est� correto!',1,255);
end if;

if	(qt_material_w = 0) then
	ds_erro_w := substr('A quantidade informada � igual a zero!',1,255);
end if;

if	(ds_erro_w is null) then
	begin
	if	(nvl(nr_atendimento_p,0) > 0) then
		begin
		obter_convenio_execucao(nr_atendimento_p, sysdate, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
		--nr_seq_atepacu_w	:= obter_atepacu_paciente(nr_atendimento_p, 'A');

		select	nvl(max(nr_seq_interno),0)
		into	nr_seq_atepacu_w
		from	atend_paciente_unidade
		where 	nr_atendimento = nr_atendimento_p
		and	cd_setor_atendimento = cd_setor_atendimento_w
		and	ie_passagem_setor = 'S';

		if	(nvl(nr_seq_atepacu_w,0) = 0) then
			begin
			-- Aqui ser� gerado uma passagem para o setor informado
			gerar_passagem_setor_atend(	nr_atendimento_p,
							cd_setor_atendimento_w,
							sysdate,
							'N',
							nm_usuario_p);

			/* Busca o nr_interno da movimenta��o gerada */
			select	nvl(max(nr_seq_interno),0)
			into	nr_seq_atepacu_w
			from	atend_paciente_unidade
			where 	nr_atendimento = nr_atendimento_p
			and	cd_setor_atendimento = cd_setor_atendimento_w
			and	ie_passagem_setor = 'S';
			end;
		end if;

		select 	max(a.dt_entrada_unidade)
		into	dt_entrada_unidade_w
		from	atend_paciente_unidade a
		where	a.nr_seq_interno = nr_seq_atepacu_w;

		if	(ie_cobra_paciente_w = 'S') then
			begin
			if	(cd_acao_w = 2) then
				begin
				qt_devolvida_w	:= qt_material_w;
				qt_material_w	:= (qt_material_w * -1);
				end;
			end if;

			select	material_atend_paciente_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into material_atend_paciente (
				nr_sequencia,
				nr_atendimento,
				dt_entrada_unidade,
				cd_material,
				dt_atendimento,
				cd_unidade_medida,
				qt_material,
				dt_atualizacao,
				nm_usuario,
				cd_acao,
				cd_setor_atendimento,
				nr_seq_atepacu,
				cd_material_prescricao,
				cd_material_exec,
				ie_via_aplicacao,
				dt_prescricao,
				nr_prescricao,
				nr_sequencia_prescricao,
				cd_cgc_fornecedor,
				qt_executada,
				nr_cirurgia,
				cd_local_estoque,
				vl_unitario,
				qt_ajuste_conta,
				ie_valor_informado,
				ie_guia_informada,
				ie_auditoria,
				nm_usuario_original,
				cd_situacao_glosa,
				cd_convenio,
				cd_categoria,
				nr_doc_convenio,
				ie_tipo_guia,
				nr_seq_lote_fornec,
				cd_senha,
				dt_conta,
				nr_seq_kit_estoque,
				qt_devolvida)
			values(	nr_sequencia_w,
				nr_atendimento_p,
				dt_entrada_unidade_w,
				cd_material_p,
				sysdate,
				cd_unidade_medida_w,
				qt_material_w,
				sysdate,
				nm_usuario_p,
				cd_acao_w,
				cd_setor_atendimento_w,
				nr_seq_atepacu_w,
				cd_material_p,
				cd_material_p,
				null,
				null,
				null,
				null,
				cd_fornec_consignado_w,
				qt_material_w,
				null,
				cd_local_estoque_p,
				0,
				0,
				'N',
				'N',
				'N',
				nm_usuario_p,
				0,
				cd_convenio_w,
				cd_categoria_w,
				nr_doc_convenio_w,
				ie_tipo_guia_w,
				null,
				cd_senha_w,
				sysdate,
				null,
				qt_devolvida_w);

			atualiza_preco_material(nr_sequencia_w, nm_usuario_p);
			end;
		else
			begin
			gerar_prescricao_estoque(
				cd_estabelecimento_w,
				nr_atendimento_p,
				dt_entrada_unidade_w,
				cd_material_p,
				sysdate,
				cd_acao_w,
				cd_local_estoque_p,
				qt_material_w,
				cd_setor_atendimento_w,
				cd_unidade_medida_w,
				nm_usuario_p,
				'I',
				null,
				null,
				null,
				0,
				cd_fornec_consignado_w,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
			end;
		end if;
		end;
	else
		begin
		if	(ie_atualizar_consig_w = 'N') then
			select	max(cd_operacao_estoque)
			into	cd_operacao_estoque_w
			from	operacao_estoque
			where	ie_situacao = 'A'
			and	ie_tipo_requisicao = 5
			and	nvl(ie_consignado,0) = 0
			and	ie_atualiza_estoque = 'S'
			and	ie_entrada_saida = decode(cd_acao_w,'1','S','E');
		else
			select	max(cd_operacao_estoque)
			into	cd_operacao_estoque_w
			from	operacao_estoque
			where	ie_situacao = 'A'
			and	ie_tipo_requisicao = 5
			and	nvl(ie_consignado,0) = 7
			and	ie_entrada_saida = decode(cd_acao_w,'1','S','E');
		end if;

		cd_acao_w := '1';

		select	max(cd_centro_custo)
		into	cd_centro_custo_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;

		define_conta_material(
			cd_estabelecimento_w,
			cd_material_p,
			'2',
			null,
			cd_setor_atendimento_w,
			null,
			null,
			null,
			null,
			null,
			cd_local_estoque_p,
			null,
			sysdate,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null);

		insert into movimento_estoque(
			nr_movimento_estoque,
			cd_estabelecimento,
			cd_local_estoque,
			dt_movimento_estoque,
			cd_operacao_estoque,
			cd_acao,
			cd_material,
			dt_mesano_referencia,
			qt_movimento,
			dt_atualizacao,
			nm_usuario,
			ie_origem_documento,
			nr_documento,
			nr_sequencia_item_docto,
			cd_unidade_medida_estoque,
			cd_setor_atendimento,
			qt_estoque,
			cd_centro_custo,
			cd_unidade_med_mov,
			cd_fornecedor,
			ds_observacao,
			nr_seq_tab_orig,
			nr_seq_lote_fornec,
			cd_lote_fabricacao,
			dt_validade,
			nr_atendimento,
			nr_prescricao,
			nr_receita,
			cd_conta_contabil,
			nr_ordem_compra,
			nr_item_oci,
			nr_lote_ap,
			nr_lote_producao)
		values(	movimento_estoque_seq.nextval,
			cd_estabelecimento_w,
			cd_local_estoque_p,
			sysdate,
			cd_operacao_estoque_w,
			'1',
			cd_material_p,
			sysdate,
			qt_material_w,
			sysdate,
			nm_usuario_p,
			'3',
			null,
			null,
			cd_unidade_medida_w,
			cd_setor_atendimento_w,
			qt_material_w,
			cd_centro_custo_w,
			cd_unidade_medida_w,
			cd_fornec_consignado_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			cd_conta_contabil_w,
			null,
			null,
			null,
			null);
		end;
	end if;

	ie_atualizou_p	:= 'S';
	end;
end if;
ds_erro_p	:= ds_erro_w;
commit;
end gtt_gerar_lancamento_material;
/