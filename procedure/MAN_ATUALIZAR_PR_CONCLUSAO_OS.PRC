create or replace
procedure man_atualizar_pr_conclusao_os(nr_sequencia_p		number,
					pr_conclusao_p		number,
					dt_inicio_prev_p	date,
					dt_fim_prev_p		date,
					nm_usuario_p		varchar2,
					dt_fim_repactuado_p	date) is 

ie_status_ordem_w man_ordem_servico.ie_status_ordem%type;

begin

if	(nr_sequencia_p is not null) then
	
	if	(nvl(pr_conclusao_p,0) = 0) then		
		ie_status_ordem_w := '1';		
	elsif 	(nvl(pr_conclusao_p,0) > 0 and nvl(pr_conclusao_p,0) < 100) then
		ie_status_ordem_w := '2';
	elsif 	(nvl(pr_conclusao_p,0) = 100) then
		ie_status_ordem_w := '3';
	end if;
	
	update	man_ordem_servico
	set	pr_conclusao_os	= pr_conclusao_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		ie_status_ordem = ie_status_ordem_w,
		dt_inicio_previsto = nvl(dt_inicio_prev_p,dt_inicio_previsto),
		dt_fim_previsto = nvl(dt_fim_prev_p,dt_fim_previsto),
		dt_fim_repactuado 	= nvl(dt_fim_repactuado_p, dt_fim_repactuado)
	where	nr_sequencia	= nr_sequencia_p;

	commit;
end if;

end man_atualizar_pr_conclusao_os;
/
