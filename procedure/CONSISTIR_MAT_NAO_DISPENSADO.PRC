create or replace
procedure consistir_mat_nao_dispensado  ( nr_prescricao_p	number, 	 
					  nm_usuario_p		Varchar2,
					  ds_itens_p		out varchar2) is 

nr_seq_lote_fornec_w		number(10);
cd_material_w			number(6);				    
nr_cirurgia_w			number(10);
nr_seq_prescricao_w		number(6);
qt_devolucao_w			number(18,6);
qt_mat_disp_w			number(18,6);
ds_material_w			varchar2(255);
cd_perfil_w			number(5);
cd_estabelecimento_w		number(5);
ie_consiste_mat_w		varchar2(15);
ie_achou_w			varchar2(1);
qt_material_w			number(18,6);
qt_dispensacao_w		number(18,6);
ds_mensagem_w			varchar2(2000);
qt_diferenca_w			number(18,6);
ie_consulta_w			varchar2(15);
ie_consolidacao_generico_w	varchar2(15);

expressao1_w			varchar2(255) := obter_desc_expressao_idioma(773868, null, wheb_usuario_pck.get_nr_seq_idioma);
expressao2_w			varchar2(255) := obter_desc_expressao_idioma(722457, null, wheb_usuario_pck.get_nr_seq_idioma);
expressao3_w			varchar2(255) := obter_desc_expressao_idioma(698149, null, wheb_usuario_pck.get_nr_seq_idioma);

expressao4_w			varchar2(255) := obter_desc_expressao_idioma(773872, null, wheb_usuario_pck.get_nr_seq_idioma);--Encontrado o material
expressao5_w			varchar2(255) := obter_desc_expressao_idioma(773873, null, wheb_usuario_pck.get_nr_seq_idioma);
expressao6_w			varchar2(255) := obter_desc_expressao_idioma(292766, null, wheb_usuario_pck.get_nr_seq_idioma);--Lote

expressao7_w			varchar2(255) := obter_desc_expressao_idioma(773874, null, wheb_usuario_pck.get_nr_seq_idioma);--por�m o mesmo n�o foi previsto na cirurgia

expressao8_w			varchar2(255) := obter_desc_expressao_idioma(774510, null, wheb_usuario_pck.get_nr_seq_idioma);--previsto para a cirurgia
expressao9_w			varchar2(255) := obter_desc_expressao_idioma(774513, null, wheb_usuario_pck.get_nr_seq_idioma);--registrado como perda
expressao10_w			varchar2(255) := obter_desc_expressao_idioma(774514, null, wheb_usuario_pck.get_nr_seq_idioma);--dispensado no PEPO

Cursor C01 is
	select 	cd_material,
		nr_seq_lote_fornec,
		nr_sequencia
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p;
	
cursor	c02 is
	SELECT	SUM(qt_material) - 
		(	obter_qt_dispensada_pepo(nr_prescricao,decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(cd_material),cd_material),nr_seq_lote_fornec,'D') - 
			obter_qt_dispensada_pepo(nr_prescricao,decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(cd_material),cd_material),nr_seq_lote_fornec,'P')) qt_diferenca,
		cd_material,
		nr_seq_lote_fornec,
		expressao1_w || ' '||cd_material ||' - '||SUBSTR(obter_desc_material(cd_material),1,80)||'.'||CHR(13)||
		expressao2_w || ' '||SUM(qt_material) ||CHR(13)||
		expressao3_w || ' '||(obter_qt_dispensada_pepo(nr_prescricao,cd_material,nr_seq_lote_fornec,'D') -
		obter_qt_dispensada_pepo(nr_prescricao,cd_material,nr_seq_lote_fornec,'P'))  ds_mensagem,
		'A' ie_consulta
	FROM	prescr_material
	WHERE	nr_prescricao 		= nr_prescricao_p
	AND	obter_qt_dispensada_pepo(nr_prescricao,cd_material,nr_seq_lote_fornec,'D') <> 0
	GROUP BY cd_material,nr_seq_lote_fornec,nr_prescricao
	UNION
	SELECT	SUM(a.qt_material) qt_diferenca,
		a.cd_material,
		a.nr_seq_lote_fornec,
		expressao4_w || ' '||a.cd_material ||' - '||substr(obter_desc_material(a.cd_material),1,80)||','||expressao8_w||', '||chr(13)||
		expressao5_w || ' '||chr(13)||
		expressao6_w || ' '|| a.nr_seq_lote_fornec ds_mensagem,
		'B' ie_consulta
	FROM	prescr_material a
	WHERE	a.nr_prescricao = nr_prescricao_p
	AND	NOT EXISTS	(	SELECT	1
					FROM	cirurgia x,
						cirurgia_agente_disp y
					WHERE  	x.nr_cirurgia 	   		= y.nr_cirurgia
					AND	x.nr_prescricao 	   	= a.nr_prescricao
					AND	y.cd_material   	   	= decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(a.cd_material),a.cd_material)
					AND	NVL(y.nr_seq_lote_fornec,0)	= NVL(a.nr_seq_lote_fornec,0))	  
	GROUP BY cd_material,nr_seq_lote_fornec,nr_prescricao
	UNION
	SELECT	NVL(SUM(b.qt_dispensacao),0),
		decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material),
		b.nr_seq_lote_fornec,
		expressao4_w || ' '||decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material)||' - '||substr(obter_desc_material(decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material)),1,80)||','||expressao9_w||', '||chr(13)||
		expressao7_w || ' '||chr(13)||
		expressao6_w || ' '||b.nr_seq_lote_fornec ds_mensagem,
		'C' ie_consulta
	FROM	cirurgia a,
		cirurgia_agente_disp b
	WHERE	a.nr_cirurgia			= b.nr_cirurgia
	AND	a.nr_prescricao 		= nr_prescricao_p
	AND	b.ie_operacao			= 'P'
	AND	NOT EXISTS	( 	SELECT 	1 
					FROM	prescr_material x
					WHERE  	x.cd_material   		= decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material)
					AND	NVL(x.nr_seq_lote_fornec,0)	= NVL(b.nr_seq_lote_fornec,0)
					AND	x.nr_prescricao			= a.nr_prescricao)    
	GROUP BY decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material),b.nr_seq_lote_fornec
	UNION
	SELECT	NVL(SUM(b.qt_dispensacao),0),
		decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material),
		b.nr_seq_lote_fornec,
		expressao4_w || ' '||decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material)||' - '||substr(obter_desc_material(decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material)),1,80)||','||expressao10_w||', '||chr(13)||
		expressao7_w || ' '||chr(13)||
		expressao6_w || ' '||b.nr_seq_lote_fornec ds_mensagem,
		'D' ie_consulta
	FROM	cirurgia a,
		cirurgia_agente_disp b
	WHERE	a.nr_cirurgia			= b.nr_cirurgia
	AND	a.nr_prescricao 		= nr_prescricao_p
	AND	b.ie_operacao			= 'D'
	AND	NOT EXISTS	( 	SELECT 	1 
					FROM   	prescr_material x
					WHERE  	x.cd_material   		= decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material)
					AND	NVL(x.nr_seq_lote_fornec,0)	= NVL(b.nr_seq_lote_fornec,0)
					AND	x.nr_prescricao			= a.nr_prescricao)
	GROUP BY decode(ie_consolidacao_generico_w,'S',Obter_controlador_estoque(b.cd_material),b.cd_material),b.nr_seq_lote_fornec				
	ORDER BY ds_mensagem;
 
	
begin





cd_perfil_w		:= wheb_usuario_pck.get_cd_perfil;
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(900, 380, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w,ie_consiste_mat_w);
obter_param_usuario(872, 152, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w,ie_consolidacao_generico_w);

if	(nr_prescricao_p is not null) then
	select max(nr_cirurgia)
	into   nr_cirurgia_w
	from   cirurgia 
	where  nr_prescricao = nr_prescricao_p;
end if;

if	(ie_consiste_mat_w = 'S') then
	open C01;
	loop
	fetch C01 into	
		  cd_material_w,
		  nr_seq_lote_fornec_w,
		  nr_seq_prescricao_w;
	exit when C01%notfound;

		select	count(*)
		into	qt_devolucao_w
		from	prescr_material b,
			prescr_material_devolucao a
		where	b.nr_prescricao 		=  a.nr_prescricao
		and	b.nr_sequencia  		=  a.nr_seq_prescricao 
		and     b.cd_material   		=  cd_material_w
		and     nvl(b.nr_seq_lote_fornec,0)	=  nvl(nr_seq_lote_fornec_w,0)
		and     b.nr_prescricao 		=  nr_prescricao_p
		AND     a.nr_seq_prescricao 		=  nr_seq_prescricao_w;
		
		
		if (qt_devolucao_w = 0) then
		
			select count(*)
			into   qt_mat_disp_w
			from   cirurgia_agente_disp 
			where  nr_cirurgia 			= nr_cirurgia_w
			and    cd_material 			= cd_material_w
			and    nvl(nr_seq_lote_fornec,0)	= nvl(nr_seq_lote_fornec_w,0);
			
			if (qt_mat_disp_w = 0) then
		       
				select substr(obter_desc_material(cd_material_w),1,255) ds_material 
				into   ds_material_w
				from   dual;
				
			ds_itens_p :=  substr(ds_material_w || ', ' || ds_itens_p,1,255);
				
			end if;
		end if;				
	end loop;
	close c01;
elsif	(ie_consiste_mat_w = 'SQ') then
	exclui_w_consistencia_cirurgia(null,nr_prescricao_p);
	open C02;
	loop
	fetch C02 into	
		qt_diferenca_w,
		cd_material_w,
		nr_seq_lote_fornec_w,
		ds_mensagem_w,
		ie_consulta_w;
	exit when C02%notfound;
		begin
		if	(ie_consulta_w <> 'A') or
			(qt_diferenca_w <> 0) then			
				gerar_w_consistencia_cirurgia(null,nr_prescricao_p,substr(ds_mensagem_w,1,2000));							
		end if;	
		end;
	end loop;
	close C02;
end if;

ds_itens_p 	:= substr(ds_itens_p,1,length(ds_itens_p) - 2);

end consistir_mat_nao_dispensado;
/
