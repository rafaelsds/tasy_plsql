create or replace
procedure transfere_gastos_rn(	nr_sequencia_p		number,
				ie_proc_mat_p		number,
				nr_atend_dest_p		number,
				cd_setor_atendimento_p	number,
				dt_entrada_unidade_p	date,
				nr_seq_atepacu_p 		number,
				nm_usuario_p		varchar2) is 

cd_convenio_w		number(10,0);
cd_categoria_w		varchar2(10);
cd_estabelecimento_w	number(4,0);	
dt_entrada_w		date;
dt_alta_w			date;		
nr_doc_convenio_w	varchar2(20);
dt_acerto_conta_w		date;
nr_interno_conta_w		number(10,0);
cd_convenio_calculo_w	number(6,0);
cd_categoria_calculo_w	varchar2(10);
				
begin

select 	max(cd_estabelecimento),
	max(dt_entrada),
	max(dt_alta)
into	cd_estabelecimento_w,
	dt_entrada_w,
	dt_alta_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atend_dest_p;

if	(ie_proc_mat_p = 1) then --m�e para rn (procedimento)

	/*insert 	into logxxxxxx_tasy
		(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values
		(sysdate, nm_usuario_p, 7009, ' Transf. Gastos RN Seq Proc: ' || nr_sequencia_p || ' Atend: ' || nr_atend_dest_p); */
		
	select 	nvl(max(cd_convenio),1),
		nvl(max(cd_categoria),'1'),
		max(nr_doc_convenio)
	into	cd_convenio_w,
		cd_categoria_w,
		nr_doc_convenio_w
	from 	procedimento_paciente
	where 	nr_sequencia = nr_sequencia_p;		
	
	obter_conta_paciente	(cd_estabelecimento_w,
				nr_atend_dest_p,
				cd_convenio_w,
				cd_categoria_w,
				nm_usuario_p,
				sysdate,
				dt_entrada_w,
				dt_alta_w,
				nr_doc_convenio_w,
				cd_setor_atendimento_p,
				null,
				dt_acerto_conta_w,
				nr_interno_conta_w,
				cd_convenio_calculo_w,
				cd_categoria_calculo_w);
	
	update	procedimento_paciente
	set	nr_atendimento = nr_atend_dest_p,
		nr_seq_atepacu = nr_seq_atepacu_p,
		dt_entrada_unidade = dt_entrada_unidade_p,
		cd_setor_atendimento = cd_setor_atendimento_p,
		nr_interno_conta = nr_interno_conta_w,
		--ds_observacao	= substr(ds_observacao || ' Atend original '|| nr_atendimento ||' Setor Original '||cd_setor_atendimento,1,255),
		ds_observacao	= substr(ds_observacao || ' ' || WHEB_MENSAGEM_PCK.get_texto(297127)  || ' '|| nr_atendimento ||' ' || WHEB_MENSAGEM_PCK.get_texto(297128) || ' '||cd_setor_atendimento,1,255),
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

	commit;

elsif	(ie_proc_mat_p = 2) then --m�e para rn (material)

	/*insert 	into logxxxxxx_tasy
		(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values
		(sysdate, nm_usuario_p, 7009, ' Transf. Gastos RN Seq Mat: ' || nr_sequencia_p || ' Atend: ' || nr_atend_dest_p);*/ 

	select 	nvl(max(cd_convenio),1),
		nvl(max(cd_categoria),'1'),
		max(nr_doc_convenio)
	into	cd_convenio_w,
		cd_categoria_w,
		nr_doc_convenio_w
	from 	material_atend_paciente
	where 	nr_sequencia = nr_sequencia_p;

	obter_conta_paciente	(cd_estabelecimento_w,
				nr_atend_dest_p,
				cd_convenio_w,
				cd_categoria_w,
				nm_usuario_p,
				sysdate,
				dt_entrada_w,
				dt_alta_w,
				nr_doc_convenio_w,
				cd_setor_atendimento_p,
				null,
				dt_acerto_conta_w,
				nr_interno_conta_w,
				cd_convenio_calculo_w,
				cd_categoria_calculo_w);
	
	update	material_atend_paciente
	set	nr_atendimento = nr_atend_dest_p,
		nr_seq_atepacu = nr_seq_atepacu_p,
		dt_entrada_unidade = dt_entrada_unidade_p,
		cd_setor_atendimento = cd_setor_atendimento_p,
		nr_interno_conta = nr_interno_conta_w,
		--ds_observacao	= substr(ds_observacao || ' Atend original '|| nr_atendimento ||' Setor Original '||cd_setor_atendimento,1,255),
		ds_observacao	= substr(ds_observacao || ' ' || WHEB_MENSAGEM_PCK.get_texto(297127) || ' '|| nr_atendimento ||' ' ||  WHEB_MENSAGEM_PCK.get_texto(297128) || ' '||cd_setor_atendimento,1,255),		
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

	commit;
	
elsif	(ie_proc_mat_p = 3) then -- rn para m�e (procedimento)
	
	/*insert 	into logxxxxx_tasy
		(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values
		(sysdate, nm_usuario_p, 7009, ' Transf. Gastos RN Seq Proc: ' || nr_sequencia_p || ' Atend: ' || nr_atend_dest_p); */
		
	select 	nvl(max(cd_convenio),1),
		nvl(max(cd_categoria),'1'),
		max(nr_doc_convenio)
	into	cd_convenio_w,
		cd_categoria_w,
		nr_doc_convenio_w
	from 	procedimento_paciente
	where 	nr_sequencia = nr_sequencia_p;		
	
	obter_conta_paciente	(cd_estabelecimento_w,
				nr_atend_dest_p,
				cd_convenio_w,
				cd_categoria_w,
				nm_usuario_p,
				sysdate,
				dt_entrada_w,
				dt_alta_w,
				nr_doc_convenio_w,
				cd_setor_atendimento_p,
				null,
				dt_acerto_conta_w,
				nr_interno_conta_w,
				cd_convenio_calculo_w,
				cd_categoria_calculo_w);
	
	update	procedimento_paciente
	set	nr_seq_atepacu = nr_seq_atepacu_p,
		dt_entrada_unidade = dt_entrada_unidade_p,
		cd_setor_atendimento = cd_setor_atendimento_p,
		nr_interno_conta = nr_interno_conta_w,
		--ds_observacao	= substr(ds_observacao || ' Atend original '|| nr_atendimento ||' Setor Original '||cd_setor_atendimento,1,255),
		ds_observacao	= substr(ds_observacao || ' ' || WHEB_MENSAGEM_PCK.get_texto(297127) || ' '|| nr_atendimento ||' ' || WHEB_MENSAGEM_PCK.get_texto(297128) || ' '||cd_setor_atendimento,1,255),	
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

	commit;
	
elsif	(ie_proc_mat_p = 4) then --rn para m�e(material)

	/*insert 	into logxxxx_tasy
		(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values
		(sysdate, nm_usuario_p, 7009, ' Transf. Gastos RN Seq Mat: ' || nr_sequencia_p || ' Atend: ' || nr_atend_dest_p); */

	select 	nvl(max(cd_convenio),1),
		nvl(max(cd_categoria),'1'),
		max(nr_doc_convenio)
	into	cd_convenio_w,
		cd_categoria_w,
		nr_doc_convenio_w
	from 	material_atend_paciente
	where 	nr_sequencia = nr_sequencia_p;

	obter_conta_paciente	(cd_estabelecimento_w,
				nr_atend_dest_p,
				cd_convenio_w,
				cd_categoria_w,
				nm_usuario_p,
				sysdate,
				dt_entrada_w,
				dt_alta_w,
				nr_doc_convenio_w,
				cd_setor_atendimento_p,
				null,
				dt_acerto_conta_w,
				nr_interno_conta_w,
				cd_convenio_calculo_w,
				cd_categoria_calculo_w);
	
	update	material_atend_paciente
	set	nr_seq_atepacu = nr_seq_atepacu_p,
		dt_entrada_unidade = dt_entrada_unidade_p,
		cd_setor_atendimento = cd_setor_atendimento_p,
		nr_interno_conta = nr_interno_conta_w,
		--ds_observacao	= substr(ds_observacao || ' Atend original '|| nr_atendimento ||' Setor Original '||cd_setor_atendimento,1,255),
		ds_observacao	= substr(ds_observacao || ' ' || WHEB_MENSAGEM_PCK.get_texto(297127) || ' '|| nr_atendimento ||' ' || WHEB_MENSAGEM_PCK.get_texto(297128) || ' '||cd_setor_atendimento,1,255),	
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

	commit;
	
end if;

commit;			

end transfere_gastos_rn;
/