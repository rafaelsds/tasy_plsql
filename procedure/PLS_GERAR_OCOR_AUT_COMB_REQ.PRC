create or replace procedure pls_gerar_ocor_aut_comb_req
			(	nr_seq_ocorrencia_p	Number,
				nr_seq_ocor_combinada_p	Number,
				ie_aplicacao_regra_p	Varchar2,
				ie_utiliza_filtro_p	Varchar2,
				nr_seq_segurado_p	Number,
				nr_seq_motivo_glosa_p	Number,
				nr_seq_requisicao_p	Number,
				nr_seq_param1_p		Number,
				nr_seq_param2_p		Number,
				nr_seq_param3_p		Number,
				nr_seq_param4_p		Number,
				nr_seq_param5_p		Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Procedure utilizada para validar a regra de PCMSO do prestador e do beneficiario em relacao ao atendmento, esta rotina e
utilizada para geracao de ocorrencia na Autorizacao / Requisicao e Execucao
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao: Ao realizar alguma alteracao testar se continua funcionando para as requisicoes.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin
/* Tratamento para a validacao Somente filtros */
if	(ie_aplicacao_regra_p = 1) then
	pls_gerar_ocor_aut_somente_fil(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, null, null,
					null, null, null,
					nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao  de contratacao */
if	(ie_aplicacao_regra_p	= 2) then
	pls_valida_ocor_aut_contrato(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao de limite de execucao por prestador */
if	(ie_aplicacao_regra_p	= 4) then
	pls_valida_ocor_aut_lim_exe_pr(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao do atendimento de PCMSO */
if	(ie_aplicacao_regra_p	= 5) then
	pls_valida_ocor_aut_pcmso(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao de concorrentes */
if	(ie_aplicacao_regra_p	= 6) then
	pls_valida_ocor_aut_concor(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao de limite de execucao por beneficiario*/
if	(ie_aplicacao_regra_p	= 7) then
	pls_valida_ocor_aut_lim_exe_bf(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao rescisao futura */
if	(ie_aplicacao_regra_p	= 8) then
	pls_valida_ocor_aut_rescisao(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para regra de especialidade x procedimento */
if	(ie_aplicacao_regra_p	= 9) then
	pls_valida_ocor_aut_proc_esp(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;
/* Tratamento para validacao da atualizacao cadastral */
if	(ie_aplicacao_regra_p	= 10) then
	pls_valida_ocor_aut_cadastral(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;
--Tratamento para validacao rol procedimentos
if	(ie_aplicacao_regra_p	= 11) then
	pls_valida_ocor_aut_rol_proc(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para a validacao quantidade incidencia*/
if	(ie_aplicacao_regra_p	= 12) then
	pls_valida_ocor_aut_qt_incid(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para a Valida caracteristica financeira*/
if	(ie_aplicacao_regra_p	= 13) then
	pls_valida_ocor_aut_caract_fin(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para a Valida biometria prestador*/
if	(ie_aplicacao_regra_p	= 14) then
	pls_valida_ocor_aut_biom_prest(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para a Validacao de carencia em guia de urgencia/emergencia*/
if	(ie_aplicacao_regra_p	= 16) then
	pls_valida_ocor_aut_carencia(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para a Validacao declaracao de saude liberada sem incidencia de CPT*/
if	(ie_aplicacao_regra_p	= 17) then
	pls_valida_ocor_aut_incid_cpt(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para a Validacao de glosas geradas*/
if	(ie_aplicacao_regra_p	= 18) then
	pls_valida_ocor_aut_glosa_ger(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validacao de beneficiafio de fundacao*/

if	(ie_aplicacao_regra_p	= 19) then
	pls_valida_ocor_aut_fundacao(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p);					
end if;

/* Tratamento para validacao de lancamento automatico */
if	(ie_aplicacao_regra_p	= 20) then
	pls_valida_ocor_aut_lanc_auto(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, nm_usuario_p,
					cd_estabelecimento_p);
end if;

/* Tratamento para a Validacao  do anexo de guias */
if	(ie_aplicacao_regra_p	= 22) then
	pls_valida_ocor_aut_anexo_guia(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validar afastamento do prestador/medico da guia*/
if	(ie_aplicacao_regra_p	= 23) then
	pls_valida_aut_aft_coop_guia(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, nm_usuario_p,cd_estabelecimento_p);
end if;

/* Tratamento para validar a vigencia dos itens*/
if	(ie_aplicacao_regra_p	= 24) then
	pls_valida_ocor_aut_vig_item(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validar a duplicidade de requisicoes*/
if	(ie_aplicacao_regra_p	= 25) then
	pls_valida_ocor_aut_duplicid(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validar a duplicidade de requisicoes*/
if	(ie_aplicacao_regra_p	= 26) then
	pls_valida_ocor_aut_idad_benef(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/*Tratamento para validar o alto custo de prestador de intercambio*/
if	(ie_aplicacao_regra_p	= 27) then
	pls_valida_ocor_aut_prest_ac(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validar a regra de carteira*/
if	(ie_aplicacao_regra_p	= 28) then
	pls_valida_ocor_aut_regra_cart(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validar pacote*/
if	(ie_aplicacao_regra_p	= 29) then
	pls_valida_ocor_aut_pacote_int(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_segurado_p,
					nr_seq_motivo_glosa_p, null, nr_seq_requisicao_p,
					null, ie_utiliza_filtro_p, null,
					null, null, null,
					null, nm_usuario_p, cd_estabelecimento_p);
end if;

/* Tratamento para validar o executor guia X solicitante requisicao*/
if	(ie_aplicacao_regra_p	= 32) then
	pls_valida_ocor_aut_exec_solic(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					nr_seq_requisicao_p, ie_utiliza_filtro_p, null,
					null, null, null, 
					null, nm_usuario_p, cd_estabelecimento_p); 
end if;

/* Tratamento para validar obrigatoriedade de uma guia de consulta como referencia*/
if	(ie_aplicacao_regra_p	= 34) then
	pls_valida_ocor_aut_cons_obr(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, ie_utiliza_filtro_p, 
					nm_usuario_p, cd_estabelecimento_p); 
end if;

/* Tratamento para validar a biometria do beneficiario */
if	(ie_aplicacao_regra_p	= 35) then
	pls_valida_ocor_aut_biometri(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if; 

/* Tratamento para validar o tipo de internacao X CID */
if	(ie_aplicacao_regra_p	= 36) then
	pls_valida_ocor_aut_int_cid(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 					
end if;

/* Tratamento para validar o mesmo procedimento de CPT com CID diferente */
if	(ie_aplicacao_regra_p	= 37) then
	pls_valida_ocor_aut_cpt_cid(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 	
end if;

/* Tratamento para validar a quantidade maxima de diarias permitidas */
if	(ie_aplicacao_regra_p	= 38) then
	pls_valida_ocor_aut_dia_perm(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 	
end if;

/* Tratamento para validar a exigencia de OPME web */
if	(ie_aplicacao_regra_p	= 39) then
	pls_valida_ocor_aut_opm_web(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					nr_seq_requisicao_p, ie_utiliza_filtro_p, nm_usuario_p, 
					cd_estabelecimento_p); 
end if;

/* Tratamento para validar a estrutura de itens */
if	(ie_aplicacao_regra_p	= 40) then
	pls_valida_ocor_aut_est_itens(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;
					
/* Tratamento para validar os itens simultaneos */
if	(ie_aplicacao_regra_p	= 41) then
	pls_valida_ocor_aut_simult(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 					
end if;

/* Tratamento para validar a  exigencia de procedimento liberado para pacote */
if	(ie_aplicacao_regra_p	= 42) then
	pls_valida_ocor_aut_proc_pct(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 	
end if;

/* Tratamento para validar a  especialidade/quantidade dias retorno especialidade */
if	(ie_aplicacao_regra_p	= 43) then
	pls_valida_ocor_aut_ret_espec(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;

/* Tratamento para validar  o contrato para analise de estipulante/contratante */
if	(ie_aplicacao_regra_p	= 44) then
	pls_valida_ocor_aut_estip_con(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, nr_seq_segurado_p,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;

/* Tratamento para validar a tabela de preco de itens no proceso de intercambio online*/
if	(ie_aplicacao_regra_p	= 45) then
	pls_valida_ocor_aut_tab_prec(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;

/* Tratamento para validar a versao TISS recebida nas solicitacoes de procedimentos.*/
if	(ie_aplicacao_regra_p	= 46) then
	pls_valida_ocor_aut_tiss(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;

if	(ie_aplicacao_regra_p	= 47) then
	pls_valida_ocor_aut_tipo_ate(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;

if	(ie_aplicacao_regra_p	= 48) then
	pls_valida_ocor_aut_exec_cirur(	nr_seq_ocor_combinada_p, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
					null, nr_seq_requisicao_p, null,
					ie_utiliza_filtro_p, nm_usuario_p, cd_estabelecimento_p); 
end if;

end pls_gerar_ocor_aut_comb_req;
/
