create or replace
procedure desfazer_liberacao(	table_name_p	user_tables.table_name%type,
								nr_sequencia_p	number) is

nm_usuario_w   usuario.nm_usuario%type;

begin
nm_usuario_w   := wheb_usuario_pck.get_nm_usuario;

if	(nvl(nr_sequencia_p,0) > 0) then
	execute immediate
	'update	' || table_name_p ||
	' set	dt_liberacao   = null,
			dt_atualizacao = sysdate,
			nm_usuario     = :1' ||
	' where	nr_sequencia   = :2'
	using nm_usuario_w, nr_sequencia_p;
	commit;
end if;

end desfazer_liberacao;
/