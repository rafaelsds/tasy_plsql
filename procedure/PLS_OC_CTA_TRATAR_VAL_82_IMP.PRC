create or replace
procedure pls_oc_cta_tratar_val_82_imp(	nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

					
tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;	
nr_idx_w		pls_integer := 0;				
					
cursor C01 (	nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	a.ie_valida_limite_utilizacao,
		a.ie_autorizacao_valida
	from	pls_oc_cta_val_resc_item a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_pc;

cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	sel.nr_sequencia 				nr_seq_selecao,
		sel.dt_item,
		fim_dia(nvl(seg.dt_limite_utilizacao,seg.dt_rescisao))	dt_limite_utilizacao,
		'P' ie_tipo_item,
		conta.nr_seq_guia
	from	pls_oc_cta_selecao_imp 	sel,
		pls_segurado		seg,
		pls_conta_imp_v		conta
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	sel.ie_tipo_registro	= 'P'
	and	sel.nr_seq_segurado	= seg.nr_sequencia
	and 	conta.nr_sequencia	= sel.nr_seq_conta
	union all
	select	sel.nr_sequencia 				nr_seq_selecao,
		sel.dt_item,
		fim_dia(nvl(seg.dt_limite_utilizacao,seg.dt_rescisao))	dt_limite_utilizacao,
		'M' ie_tipo_item,
		conta.nr_seq_guia
	from	pls_oc_cta_selecao_imp	sel,
		pls_segurado		seg,
		pls_conta_imp_v		conta
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and	sel.ie_tipo_registro	= 'M'
	and	sel.ie_valido 		= 'S'
	and	sel.nr_seq_segurado	= seg.nr_sequencia
	and 	conta.nr_sequencia	= sel.nr_seq_conta;

begin

-- Deve existir informa��o da regra para aplicar a valida��o
if	(nr_seq_combinada_p is not null) then

	for	r_C01_w in C01(nr_seq_combinada_p) loop

		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null, 'N');

		-- Incializar as listas para cada regra.
		pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
	
		/*Verifica se � para validar a ocorr�ncia*/
		if	(r_C01_w.ie_valida_limite_utilizacao = 'S')	then
			
			for r_C02_w in C02(nr_id_transacao_p) loop
			
				if	(r_C02_w.dt_limite_utilizacao is not null) and
					(r_C02_w.dt_item is not null) and
					(r_C02_w.dt_item	> r_C02_w.dt_limite_utilizacao) and 
					(((r_C02_w.nr_seq_guia is null) 
					and (r_C01_w.ie_autorizacao_valida = 'S'))
					or (r_C01_w.ie_autorizacao_valida = 'N'))then
					
					tb_seq_selecao_w(nr_idx_w)	:= r_C02_w.nr_seq_selecao;
					tb_valido_w(nr_idx_w)		:= 'S';
					tb_observacao_w(nr_idx_w)	:= wheb_mensagem_pck.get_texto(784849,	'DT_LIMITE_UTILIZACAO=' || to_char(r_C02_w.dt_limite_utilizacao,'dd/mm/yyyy') ||
												';ITEM PARA DT_ITEM=' || to_char(r_C02_w.dt_item,'dd/mm/yyyy'));
					
					if	(nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
						
						--Grava as informa��es na tabela de sele��o
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
												tb_valido_w,
												tb_observacao_w, 
												nr_id_transacao_p, 
												'SEQ');
						--limpa as vari�veis
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
						
						nr_idx_w := 0;

					else
						nr_idx_w := nr_idx_w + 1;
					end if;
				end if;
			end loop;
			
			

		end if;
		
		--Grava as informa��es na tabela de sele��o
		pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
								tb_observacao_w, nr_id_transacao_p, 'SEQ');	

		-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
		pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', ie_regra_excecao_p, 
							null, nr_id_transacao_p, null);
		
	end loop;										
	
end if;

end pls_oc_cta_tratar_val_82_imp;
/
