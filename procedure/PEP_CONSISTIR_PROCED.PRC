create or replace
procedure PEP_consistir_proced(	cd_pessoa_fisica_p	varchar2,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,	
					ds_retorno_p		out varchar2,
					nm_usuario_p		Varchar2) is 
ie_situacao_w	varchar2(1) := 'A';
begin

if (consistir_idade_paciente_proc(cd_pessoa_fisica_p, cd_procedimento_p, ie_origem_proced_p) = 'N') then
	--A idade do paciente est� fora do per�odo m�n/m�x do procedimento. - 326062
	ds_retorno_p := obter_texto_tasy(326062,wheb_usuario_pck.get_nr_seq_idioma);
else
	select 	nvl(max(ie_situacao),'A') ie_situacao 
	into	ie_situacao_w
	from 	procedimento 
	where	cd_procedimento 	= cd_procedimento_p
	and   	ie_origem_proced 	= ie_origem_proced_p;
	if (ie_situacao_w = 'I') then
		--Este procedimento est� inativo. N�o pode ser prescrito. - 326063
		ds_retorno_p := obter_texto_tasy(326063,wheb_usuario_pck.get_nr_seq_idioma);
	end if;
end if;

end PEP_consistir_proced;
/