create or replace
procedure fis_gerar_reg_E110_efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p	in out	number,
				nr_sequencia_p	in out	number) is 

nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;
dt_fim_w			date;

--gera apenas notas de sa�da por digita��o e sa�da por emiss�o
cursor c01 is
select	distinct 'E110' tp_registro,
	sum(nfe_obter_valores_totais_num(a.nr_sequencia, 'ICMS', 'VL')) vl_tot_debitos,
	0 vl_aj_debitos,
	0 vl_tot_aj_debitos,
	0 vl_estornos_cred,
	0 vl_tot_creditos,
	0 vl_aj_creditos,
	0 vl_tot_aj_creditos,
	sum(nfe_obter_valores_totais_num(a.nr_sequencia, 'ICMS', 'VL')) vl_estornos_deb,
	0 vl_sld_credor_ant,
	0 vl_sld_apurado,
	0 vl_tot_ded,
	0 vl_icms_recolher,
	0 vl_sld_credor_transportar,
	0 deb_esp
from	nota_fiscal a
where	a.cd_estabelecimento	= cd_estabelecimento_p
--and	a.cd_operacao_nf in (30, 44, 4, 31, 25, 35, 23, 1, 2, 29, 42, 41, 21, 28)
and 	Obter_se_nota_entrada_saida(a.nr_sequencia) = 'S'
and     a.dt_emissao between dt_inicio_p and dt_fim_w --nota de saida
and 	a.ie_situacao = 1
and	a.ie_tipo_nota in ('SD', 'SE');

vet01	c01%rowtype;

begin
dt_fim_w := fim_dia(dt_fim_p); --coloca a hora junto ex: 01/01/2013 23:59:59

open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin	

	ds_linha_w	:= substr(	sep_w || vet01.tp_registro	|| 				
					sep_w || vet01.vl_tot_debitos			||              
					sep_w || vet01.vl_aj_debitos				||      
					sep_w || vet01.vl_tot_aj_debitos		||              
					sep_w || vet01.vl_estornos_cred			||              
					sep_w || vet01.vl_tot_creditos			||              
					sep_w || vet01.vl_aj_creditos				||      
					sep_w || vet01.vl_tot_aj_creditos		||              
					sep_w || vet01.vl_estornos_deb			||              
					sep_w || vet01.vl_sld_credor_ant			||      
					sep_w || vet01.vl_sld_apurado				||      
					sep_w || vet01.vl_tot_ded		||              
					sep_w || vet01.vl_icms_recolher			||              
					sep_w || vet01.vl_sld_credor_transportar			||      
					sep_w || vet01.deb_esp		|| sep_w,1,8000);     
	                                                                                                
	ds_arquivo_w			:= substr(ds_linha_w,1,4000);                                   
	ds_arquivo_compl_w		:= substr(ds_linha_w,4001,4000);                                
	nr_seq_registro_w		:= nr_seq_registro_w + 1;
	nr_linha_w			:= nr_linha_w + 1;

	insert into fis_efd_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_efd,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		'E110',
		ds_arquivo_w,
		ds_arquivo_compl_w);
	end;
end loop;
close c01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_E110_efd;
/
