CREATE OR REPLACE
PROCEDURE Gerar_Prescr_Prot_Pesquisa	(nr_seq_agenda_p	number,
						nm_usuario_p		varchar2) is

cd_paciente_w			varchar2(10);
cd_medico_w			varchar2(10);
nr_atendimento_w		number(10,0);
nr_seq_pq_proc_w		number(10,0);
cd_setor_atendimento_w	number(5,0);
cd_prescritor_w		varchar2(10);
ie_funcao_prescritor_w	varchar2(3);
cd_estabelecimento_w		number(4,0);
dt_prim_horario_w		date;
dt_atualizacao_w		date	:= sysdate;
nr_prescricao_w		number(10,0);
nr_seq_proc_interno_w	number(10,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_sequencia_w		number(10,0);
nr_seq_interno_w		number(10,0);

BEGIN

if	(nr_seq_agenda_p is not null) then
	/* obter dados da agenda */
	select	max(b.cd_pessoa_fisica),
		max(a.cd_pessoa_fisica),
		max(b.nr_atendimento),
		max(b.nr_seq_pq_proc)
	into	cd_paciente_w,
		cd_medico_w,
		nr_atendimento_w,
		nr_seq_pq_proc_w
	from	agenda a,
		agenda_consulta b
	where	a.cd_agenda = b.cd_agenda
	and	b.nr_sequencia = nr_seq_agenda_p;

	/* obter dados do usu�rio*/
	select	max(a.cd_setor_atendimento),
		max(a.cd_pessoa_fisica),
		max(a.ie_tipo_evolucao)
	into	cd_setor_atendimento_w,
		cd_prescritor_w,
		ie_funcao_prescritor_w
	from	usuario a
	where	a.nm_usuario = nm_usuario_p;

	/* obter dados do atendimento */
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_w;

	/* obter dados da prescri��o */
	select	nvl(obter_prim_horario_setor(cd_Setor_atendimento_w), to_date((to_char(sysdate,'dd/mm/yyyy hh24')||':00:00'),'dd/mm/yyyy 
hh24:mi:ss') +1/24)
	into	dt_prim_horario_w
	from	dual;	

	/* gerar seq��ncia */
	select	prescr_medica_seq.nextval
	into	nr_prescricao_w
	from	dual;
	
	/* inserir registro na tabela */
	insert into prescr_medica	(
					nr_prescricao,
					cd_pessoa_fisica,
					nr_atendimento,
					cd_medico,
					dt_prescricao,
					dt_atualizacao,
					nm_usuario,
					nr_horas_validade,
					dt_primeiro_horario,
					cd_setor_atendimento,
					ie_recem_nato,
					ie_origem_inf,
					cd_setor_entrega,
					nm_usuario_original,
					cd_estabelecimento,
					cd_prescritor,
					ie_emergencia,
					ie_funcao_prescritor
					)
				values	(
					nr_prescricao_w,
					cd_paciente_w,
					nr_atendimento_w,
					cd_medico_w,
					dt_atualizacao_w,
					dt_atualizacao_w,
					nm_usuario_p,
					24,
					dt_prim_horario_w,
					cd_setor_atendimento_w,
					'N',
					'1',
					cd_setor_atendimento_w,
					nm_usuario_p,
					cd_estabelecimento_w,
					cd_prescritor_w,
					'N',
					ie_funcao_prescritor_w					
					);

	/* validar registros inseridos */
	COMMIT;

	/* obter dados do protocolo de pesquisa */
	select	max(a.nr_seq_proc_interno)
	into	nr_seq_proc_interno_w
	from	pq_procedimento a
	where	a.nr_sequencia = nr_seq_pq_proc_w;

	/* obter dados do procedimento */
	obter_proc_tab_interno(nr_seq_proc_interno_w, nr_prescricao_w, nr_atendimento_w, 0, cd_procedimento_w, ie_origem_proced_w,null,null);

	/* gerar seq��ncia */
	select	nvl(max(a.nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_w;

	/* gerar sequencia */
	select	prescr_procedimento_seq.nextval
	into	nr_seq_interno_w
	from	dual;

	/* inserir registro na tabela */
	insert into prescr_procedimento	(
						nr_prescricao,
						nr_sequencia,
						cd_procedimento,
						qt_procedimento,
						dt_atualizacao,
						nm_usuario,
						ds_horarios,
						cd_motivo_baixa,
						ie_origem_proced,
						cd_intervalo,
						ie_urgencia,
						cd_setor_atendimento,
						dt_prev_execucao,
						ie_suspenso,
						ie_amostra,
						ie_origem_inf,
						ie_executar_leito,
						nr_agrupamento,
						ie_se_necessario,
						ie_acm,
						nr_ocorrencia,
						ie_status_execucao,
						nr_seq_interno,
						ie_avisar_result,
						nr_seq_proc_interno,
						nr_seq_pq_proc
						)
					values	(
						nr_prescricao_w,
						nr_sequencia_w,
						cd_procedimento_w,
						1,
						dt_atualizacao_w,
						nm_usuario_p,
						null,
						0,
						ie_origem_proced_w,
						null,
						'N',
						cd_setor_atendimento_w,
						dt_atualizacao_w,
						'N',
						'N',
						'P',
						'N',
						1,
						'N',
						'N',
						1,
						'10',
						nr_seq_interno_w,
						'N',
						nr_seq_proc_interno_w,
						nr_seq_pq_proc_w
						);
end if;
COMMIT;
END	Gerar_Prescr_Prot_Pesquisa;
/