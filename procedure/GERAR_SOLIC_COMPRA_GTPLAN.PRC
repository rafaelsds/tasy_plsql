create or replace
procedure gerar_solic_compra_GTPLAN is


nr_sequencia_w				w_solic_compra_gtplan.nr_sequencia%type;
nr_documento_externo_w			w_solic_compra_gtplan.nr_documento_externo%type;
cd_estabelecimento_w			w_solic_compra_gtplan.cd_estabelecimento%type;
dt_solicitacao_compra_w			w_solic_compra_gtplan.dt_solicitacao_compra%type;
nm_usuario_gtp_w				w_solic_compra_gtplan.nm_usuario_gtp%type;
cd_local_estoque_w			w_solic_compra_gtplan.cd_local_estoque%type;
ie_urgente_w				w_solic_compra_gtplan.ie_urgente%type;
ds_observacao_w				w_solic_compra_gtplan.ds_observacao%type;
cd_local_estoque				w_solic_compra_gtplan.cd_local_estoque%type;
ie_envio_integracao_w			w_solic_compra_gtplan.ie_envio_integracao%type;
nm_comprador_w				w_solic_compra_gtplan.nm_comprador%type;
cd_comprador_w				pessoa_fisica.cd_pessoa_fisica%type;

nr_seq_item_w				w_solic_compra_item_gtplan.nr_sequencia%type;
cd_material_w				w_solic_compra_item_gtplan.cd_material%type;
cd_unidade_medida_compra_w		w_solic_compra_item_gtplan.cd_unidade_medida_compra%type;
qt_material_w				w_solic_compra_item_gtplan.qt_material%type;
dt_solic_item_w				w_solic_compra_item_gtplan.dt_solic_item%type;
ds_obs_item_w				w_solic_compra_item_gtplan.ds_observacao%type;
qt_entrega_w				w_solic_compra_entr_gtplan.qt_entrega%type;
dt_entrega_w				w_solic_compra_entr_gtplan.dt_entrega%type;
nr_solic_compra_w				solic_compra.nr_solic_compra%type;
cd_pessoa_solicitante_w			solic_compra.cd_pessoa_solicitante%type;
nr_item_solic_compra_w			solic_compra_item.nr_item_solic_compra%type;
nr_item_solic_compra_entr_w			solic_compra_item_entrega.nr_item_solic_compra_entr%type;
nr_seq_nf_w				nota_fiscal.nr_sequencia%type;
dt_autorizacao_w				solic_compra.dt_autorizacao%type;
ie_tipo_integracao_w			varchar2(15);
nr_seq_integracao_w			number(10);
ie_integra_tipo_solic_w			varchar2(15);
qt_registro_w				number(10);
ie_erro_w 				varchar2(1) := 'N';
ds_erro_w				varchar2(3000);
vl_ultima_compra_w			number(13,4);
vl_custo_total_w			solic_compra_item.vl_custo_total%type;
vl_estoque_total_w			solic_compra_item.vl_estoque_total%type;
cd_unid_med_compra_mat_w		unidade_medida.cd_unidade_medida%type;
qt_saldo_disp_estoque_w			solic_compra_item.qt_saldo_disp_estoque%type;
vl_parametro_165_w			varchar2(15);
cd_perfil_w				solic_compra.cd_perfil%type;

cd_perfil_ww            usuario_perfil.cd_perfil%type;
vl_parameter_219_w      funcao_param_usuario.vl_parametro%type;
vl_parameter_220_w      funcao_param_usuario.vl_parametro%type;
list_oc_w                varchar2(2000);

cursor c01 is					
select	nr_sequencia,
	nr_documento_externo,
	cd_estabelecimento,
	dt_solicitacao_compra,
	nm_usuario_gtp,
	nvl(ie_urgente,'N'),
	ds_observacao,
	cd_local_estoque,
	nvl(ie_envio_integracao,'N'),
	nm_comprador
from	w_solic_compra_gtplan;

cursor c02 is
select	nr_sequencia,
	cd_material,
	cd_unidade_medida_compra,
	nvl(qt_material,0),
	dt_solic_item,
	ds_observacao
from	w_solic_compra_item_gtplan
where	nr_seq_registro = nr_sequencia_w;


cursor c03 is
select	dt_entrega,
	qt_entrega
from	w_solic_compra_entr_gtplan
where	nr_seq_item = nr_seq_item_w
order by   dt_entrega;
					
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nr_documento_externo_w,
	cd_estabelecimento_w,
	dt_solicitacao_compra_w,
	nm_usuario_gtp_w,
	ie_urgente_w,
	ds_observacao_w,
	cd_local_estoque_w,
	ie_envio_integracao_w,
	nm_comprador_w;
exit when C01%notfound;
	begin
	ie_erro_w := 'N';
	
	
	
	--delete from log_gtplan where nr_solic_gtplan = nr_documento_externo_w; OS 717507
	
	if	(nm_usuario_gtp_w is not null) then
		
		select	count(*)
		into	qt_registro_w
		from	usuario
		where	upper(nm_usuario) = upper(nm_usuario_gtp_w);
		
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E', Wheb_mensagem_pck.get_Texto(302177, 'NM_USUARIO_GTP_W='|| NM_USUARIO_GTP_W), nm_usuario_gtp_w);
										/*Nao existe o usuario #@NM_USUARIO_GTP_W#@ no Tasy.*/
			
		else
			select	obter_pessoa_fisica_usuario(nm_usuario_gtp_w,'C')
			into	cd_pessoa_solicitante_w
			from	dual;
			
			if	(cd_pessoa_solicitante_w is null) then
				ie_erro_w := 'S';
				gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302178, 'NM_USUARIO_GTP_W='|| NM_USUARIO_GTP_W) , nm_usuario_gtp_w);
											/*O usuario #@NM_USUARIO_GTP_W#@ nao esta vinculado com uma pessoa fisica.*/
				
			end if;
		end if;
	end if;	
	
	if	(nvl(cd_estabelecimento_w,0) = 0) then
		ie_erro_w := 'S';
		gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E', Wheb_mensagem_pck.get_Texto(302179), nm_usuario_gtp_w);
									/*'A GTPlan nao informou o estabelecimento.'*/
	end if;	
		
	if	(dt_solicitacao_compra_w is null) then
		ie_erro_w := 'S';
		gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302181), nm_usuario_gtp_w);
									/*'A GTPlan nao informou a data da solicitacao.'*/
	end if;	
	
	if	(nm_usuario_gtp_w is null) then
		ie_erro_w := 'S';
		gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E', Wheb_mensagem_pck.get_Texto(302183), nm_usuario_gtp_w);
									/*A GTPlan nao informou o usuario.*/
	end if;
	
	if	(nm_usuario_gtp_w is null) then
		ie_erro_w := 'S';
		gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302184) , nm_usuario_gtp_w);
									/*A GTPlan nao informou a data o usuario.*/
	end if;
	
	if	(nvl(cd_local_estoque_w,0) = 0) then
		ie_erro_w := 'S';
		gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302185) , nm_usuario_gtp_w);
									/*A GTPlan nao informou a data o local de estoque.*/
	end if;
	
	if	(nvl(cd_estabelecimento_w,0) > 0) then
		
		select	count(*)
		into	qt_registro_w
		from	estabelecimento
		where	cd_estabelecimento = cd_estabelecimento_w
		and	ie_situacao = 'A';
	
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302187, 'CD_ESTABELECIMENTO_W='|| CD_ESTABELECIMENTO_W) , nm_usuario_gtp_w);
										/*Nao existe o estabelecimento ' || cd_estabelecimento_w || ' no Tasy.*/
		end if;
	end if;
	
	if	(nvl(cd_local_estoque_w,0) > 0) then
		
		select	count(*)
		into	qt_registro_w
		from	local_estoque
		where	cd_local_estoque = cd_local_estoque_w;
	
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302193, 'CD_LOCAL_ESTOQUE_W='|| CD_LOCAL_ESTOQUE_W) , nm_usuario_gtp_w);
										/*Nao existe o local de estoque #@CD_LOCAL_ESTOQUE_W#@ no Tasy.*/
		end if;		
	end if;
	
	
	
	/*begin
	select 	1
	into	qt_registro_w
	from	comprador
	where 	cd_pessoa_fisica = cd_pessoa_solicitante_w
	and		cd_estabelecimento = cd_estabelecimento_w
	and	rownum = 1;
	exception
	when others then
		qt_registro_w	:=	0;
	end;
	
	if	(qt_registro_w = 0) then
		ie_erro_w := 'S';
		gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',WHEB_MENSAGEM_PCK.get_texto(302194,'CD_PESSOA_SOLICITANTE_W='|| CD_PESSOA_SOLICITANTE_W ||';CD_ESTABELECIMENTO_W='|| CD_ESTABELECIMENTO_W) , nm_usuario_gtp_w);
									/*Nao existe comprador cadastrado para esta pessoa fisica (#@CD_PESSOA_SOLICITANTE_W#@) e neste estabelecimento (#@CD_ESTABELECIMENTO_W#@) no Tasy.*/
									
	--end if;
	
	if	(ie_erro_w = 'N') then
			
		begin
		select	solic_compra_seq.nextval
		into	nr_solic_compra_w
		from	dual;
		
		
		select 	obter_comprador_usuario(nm_comprador_w,cd_estabelecimento_w)
		into	cd_comprador_w
		from 	dual;
		
		select  max(cd_perfil_imp_sc)
		into	cd_perfil_w
		from	parametro_compras
		where 	cd_estabelecimento = cd_estabelecimento_w;
	
		insert into solic_compra(
			nr_solic_compra,
			cd_estabelecimento,
			dt_solicitacao_compra,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			cd_pessoa_solicitante,
			cd_local_estoque,
			ds_observacao,
			ie_aviso_chegada,
			ie_aviso_aprov_oc,
			ie_urgente,
			nr_doc_externo_receb,
			cd_comprador_resp,
			ie_tipo_solicitacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_envia_integracao,
			nr_solic_importacao,
			cd_perfil)
		values(	nr_solic_compra_w,
			cd_estabelecimento_w,
			dt_solicitacao_compra_w,
			sysdate,
			nm_usuario_gtp_w,
			'A',
			cd_pessoa_solicitante_w,
			cd_local_estoque_w,
			ds_observacao_w,
			'N',
			'N',
			ie_urgente_w,
			nr_documento_externo_w,
			cd_comprador_w,
			'2',
			sysdate,
			nm_usuario_gtp_w,
			ie_envio_integracao_w,
			nr_sequencia_w,
			cd_perfil_w);
		exception
		when others then
			ie_erro_w	:= 'S';
			gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302196), nm_usuario_gtp_w);
										/*Ocorreu erro ao gravar a solicitacao de compras no Tasy*/
		end;			
			
		open C02;
		loop
		fetch C02 into	
			nr_seq_item_w,
			cd_material_w,
			cd_unidade_medida_compra_w,
			qt_material_w,
			dt_solic_item_w,
			ds_obs_item_w;
		exit when C02%notfound;
			begin
			
			if	(nvl(cd_material_w,0) = 0) then
				ie_erro_w := 'S';
				gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302199), nm_usuario_gtp_w);
											/*A GTPlan nao informou o material.*/
			end if;
				
			if	(nvl(cd_material_w,0) > 0) then
				
				select	count(*)
				into	qt_registro_w
				from	material
				where	cd_material = cd_material_w
				and	ie_situacao = 'A';
				
				if	(qt_registro_w = 0) then
					ie_erro_w := 'S';
					gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302200, 'CD_MATERIAL_W='|| CD_MATERIAL_W) , nm_usuario_gtp_w);
												/*Nao existe o material #@CD_MATERIAL_W#@ no Tasy.*/
				end if;
			end if;
			
			if	(cd_unidade_medida_compra_w is null) then
				ie_erro_w := 'S';
				gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302202), nm_usuario_gtp_w);
											/*A GTPlan nao informou a unidade de medida.*/
			end if;
			
			if	(cd_unidade_medida_compra_w is not null) then
				
				select	count(*)
				into	qt_registro_w
				from	unidade_medida
				where	cd_unidade_medida = cd_unidade_medida_compra_w
				and	ie_situacao = 'A';
				
				if	(qt_registro_w = 0) then
					ie_erro_w := 'S';
					gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302203, 'CD_UNIDADE_MEDIDA_COMPRA_W='|| CD_UNIDADE_MEDIDA_COMPRA_W) , nm_usuario_gtp_w);
												/*Nao existe a unidade de medida ' || CD_UNIDADE_MEDIDA_COMPRA_W || ' no Tasy.*/
												
				end if;
			end if;	

			if	(dt_solic_item_w is null) then
				ie_erro_w := 'S';
				gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302204) , nm_usuario_gtp_w);
											/*A GTPlan nao informou a data de entrega para o item.*/
			end if;
			
			if	(ie_erro_w = 'N') then
				
				/*
				Obter_ultima_compra_material(
					cd_estabelecimento_w,
					cd_material_w,
					cd_unidade_medida_compra_w,
					'C',
					sysdate - nvl(9999, 90),
					nr_seq_nf_w,
					vl_ultima_compra_w);*/
					
				
				select	nvl(max(obter_Valor_Param_Usuario(913, 165, obter_perfil_ativo, nm_usuario_gtp_w, cd_estabelecimento_w)),'0')
				into	vl_parametro_165_w
				from	dual;
				
				
				if	(vl_parametro_165_w = '0') then
					select	obter_dados_ult_compra_data(cd_estabelecimento_w, cd_material_w, null, sysdate, 0 , 'VE')
					into	vl_ultima_compra_w
					from	dual;
				elsif	(vl_parametro_165_w = '1') then
					select	obter_dados_ult_compra_data(cd_estabelecimento_w, cd_material_w, null, sysdate, 0 , 'VU')
					into	vl_ultima_compra_w
					from	dual;
				elsif	(vl_parametro_165_w = '2') then
					select	obter_maior_vl_compra_mat(cd_estabelecimento_w, 9999, cd_material_w, null, 'N')
					into	vl_ultima_compra_w
					from	dual;
				elsif	(vl_parametro_165_w = '3') then
					select	obter_maior_vl_compra_12_mes(cd_estabelecimento_w, 9999, cd_material_w, null, 'N')
					into	vl_ultima_compra_w
					from	dual;
				elsif	(vl_parametro_165_w = '4') then
					select	obter_maior_valor_compra_geral(cd_material_w, null, 'N', cd_unidade_medida_compra_w)
					into	vl_ultima_compra_w
					from	dual;
				elsif	(vl_parametro_165_w = '5') then
					select	obter_maior_valor_compra_geral(cd_material_w, null, 'S', cd_unidade_medida_compra_w)
					into	vl_ultima_compra_w
					from	dual;								
				end if;
				 
				select	obter_cm_mat_convertido(cd_estabelecimento_w, cd_material_w, cd_unidade_medida_compra_w, dt_solicitacao_compra_w) * nvl(qt_material_w,1)
				into	vl_custo_total_w
				from	dual;
				
				select	cd_unidade_medida_compra
				into	cd_unid_med_compra_mat_w
				from	material
				where	cd_material = cd_material_w;
				
				select	obter_saldo_estoque_nuvem(cd_estabelecimento_w, cd_material_w, cd_local_estoque_w, trunc(sysdate,'mm'))
				into	qt_saldo_disp_estoque_w
				from	dual;				

				select	obter_custo_medio_material(cd_estabelecimento_w, dt_solicitacao_compra_w, cd_material_w) * 
					(obter_quantidade_convertida(cd_material_w, qt_material_w, cd_unidade_medida_compra_w, cd_unid_med_compra_mat_w) + 
					nvl(qt_saldo_disp_estoque_w,0))
				into	vl_estoque_total_w
				from	dual;
			
				select	nvl(max(nr_item_solic_compra),0) + 1
				into	nr_item_solic_compra_w
				from	solic_compra_item
				where	nr_solic_compra = nr_solic_compra_w;
				
				begin
				insert into solic_compra_item(
					nr_solic_compra,
					nr_item_solic_compra,
					cd_material,
					cd_unidade_medida_compra,
					qt_material,
					dt_atualizacao,
					nm_usuario,
					ie_situacao,
					ds_observacao,
					dt_solic_item,
					ie_geracao,
					vl_unit_previsto,
					qt_saldo_disp_estoque,
					qt_conv_compra_est_orig,
					vl_custo_total,
					vl_estoque_total)
				values(	nr_solic_compra_w,
					nr_item_solic_compra_w,
					cd_material_w,
					cd_unidade_medida_compra_w,
					qt_material_w,
					sysdate,
					nm_usuario_gtp_w,
					'A',
					ds_obs_item_w,
					null,
					'S',
					vl_ultima_compra_w,
					qt_saldo_disp_estoque_w,
					obter_dados_material(cd_material_w,'QCE'),
					vl_custo_total_w,
					vl_estoque_total_w);
				exception
				when others then
					ie_erro_w	:= 'S';
					gravar_mensagem_log_gtplan(nr_documento_externo_w, 'E',Wheb_mensagem_pck.get_Texto(302205) , nm_usuario_gtp_w);
												/*Ocorreu erro ao gravar o item da solicitacao de compras no Tasy.*/
					
				end;
				
					
				if	(ie_erro_w = 'N') then
					open C03;
					loop
					fetch C03 into	
						dt_entrega_w,
						qt_entrega_w;
					exit when C03%notfound;
						begin
						
						select	nvl(max(nr_item_solic_compra_entr),0) + 1
						into	nr_item_solic_compra_entr_w
						from	solic_compra_item_entrega
						where	nr_solic_compra = nr_solic_compra_w
						and	nr_item_solic_compra = nr_item_solic_compra_w;
						
						insert into solic_compra_item_entrega(
							nr_solic_compra,
							nr_item_solic_compra,
							nr_item_solic_compra_entr,
							qt_entrega_solicitada,
							dt_entrega_solicitada,
							dt_atualizacao,
							nm_usuario)
						values(	nr_solic_compra_w,
							nr_item_solic_compra_w,
							nr_item_solic_compra_entr_w,
							qt_entrega_w,
							dt_entrega_w,
							sysdate,
							nm_usuario_gtp_w);
							
						update 	solic_compra_item 
						set 	dt_solic_item = dt_entrega_w 
						where  	nr_solic_compra = nr_solic_compra_w
						and	nr_item_solic_compra = nr_item_solic_compra_w
						and	dt_solic_item is null;
							
						end;
					end loop;
					close C03;
				end if;			
			end if;			
			end;
		end loop;
		close C02;			
	end if;
	
	if	(ie_erro_w = 'S') then
		delete from solic_compra
		where nr_solic_compra = nr_solic_compra_w;
	else
		gerar_aprov_solic_compra(nr_solic_compra_w,0,nm_usuario_gtp_w,9999,'N');
		
		select	dt_autorizacao
		into	dt_autorizacao_w
		from	solic_compra
		where	nr_solic_compra = nr_solic_compra_w;
		
		select	nvl(max(ie_integracao_solic),'')
		into	ie_tipo_integracao_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_w;
		
		select	obter_se_integr_tipo_solic('ES')
		into	ie_integra_tipo_solic_w
		from	dual;
		
		if	((dt_autorizacao_w is not null) and
			 (ie_envio_integracao_w = 'S') and
			 (((ie_tipo_integracao_w = 'BCD') or (ie_tipo_integracao_w = 'BCE')) or
			 ((ie_integra_tipo_solic_w = 'S') or (ie_integra_tipo_solic_w = 'BI')))) then
			
			
			begin
				intcom_gerar_int_solic_compra(nr_solic_compra_w, '', ie_tipo_integracao_w, nm_usuario_gtp_w, 'G', nr_seq_integracao_w);
				wheb_exportar_xml_pck.exportar(100480, nr_seq_integracao_w, 'INTCOM', 'nr_solic_compra=' || nr_solic_compra_w || ';');
			
				update	registro_integr_com_xml
				set	ie_status = 'NP'
				where	nr_sequencia = nr_seq_integracao_w;
				
				insert into solic_compra_hist(	
					nr_sequencia,
					nr_solic_compra,
					dt_atualizacao,
					nm_usuario,
					dt_historico,
					ds_titulo,
					ds_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_tipo,
					cd_evento,
					dt_liberacao)
				values(	solic_compra_hist_seq.nextval,
					nr_solic_compra_w,
					sysdate,
					nm_usuario_gtp_w,
					sysdate,
					Wheb_mensagem_pck.get_Texto(302206), /*'Integracao da solicitacao do GTPlan com a Bionexo',*/
					substr(Wheb_mensagem_pck.get_Texto(302207, 'IE_ENVIO_INTEGRACAO_W='|| IE_ENVIO_INTEGRACAO_W),1,4000), /*Essa solicitacao de compras foi recebida pela GTPlan, aprovada automaticamente, e integrada com a Bionexo. IE_ENVIO_INTEGRACAO_W = #@IE_ENVIO_INTEGRACAO_W#@*/
					sysdate,
					nm_usuario_gtp_w,
					'S',
					'U',
					sysdate);
				
			exception
			when others then
				ds_erro_w := substr(sqlerrm,1,3000);
			
				insert into solic_compra_hist(	
					nr_sequencia,
					nr_solic_compra,
					dt_atualizacao,
					nm_usuario,
					dt_historico,
					ds_titulo,
					ds_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_tipo,
					cd_evento,
					dt_liberacao)
				values(	solic_compra_hist_seq.nextval,
					nr_solic_compra_w,
					sysdate,
					nm_usuario_gtp_w,
					sysdate,
					Wheb_mensagem_pck.get_Texto(302209), /*'Problema ao integrar a solicitacao GTPlan com a Bionexo',*/
					substr(Wheb_mensagem_pck.get_Texto(302210, 'DS_ERRO_W='|| DS_ERRO_W),1,4000),	/*Essa solicitacao de compras foi recebida pela GTPlan, e ao enviar para a Bionexo ocorreu um erro: #@DS_ERRO_W#@*/					
					sysdate,
					nm_usuario_gtp_w,
					'S',
					'U',
					sysdate);
			end;			
		end if;
        
        begin
        cd_perfil_ww	:= obter_perfil_ativo;
        vl_parameter_219_w := nvl((obter_valor_param_usuario(913, 219, cd_perfil_ww, nm_usuario_gtp_w, cd_estabelecimento_w)), 'N');
        vl_parameter_220_w := nvl((obter_valor_param_usuario(913, 220, cd_perfil_ww, nm_usuario_gtp_w, cd_estabelecimento_w)), ' ');
                
        if (vl_parameter_219_w <> 'N' and vl_parameter_220_w <> ' ' and
           (vl_parameter_219_w <> 'U' or (vl_parameter_219_w = 'U' and ie_urgente_w = 'N'))) then

            sup_gerar_ordens_solic_liberar(nr_solic_compra_w, cd_comprador_w, sysdate, sysdate, sysdate, cd_estabelecimento_w,
                                        null, 'C', null, 'N', 'N', ie_urgente_w, nm_usuario_gtp_w, vl_parameter_220_w,
                                        null, 'S', list_oc_w);

            insert into solic_compra_hist(
					nr_sequencia,
					nr_solic_compra,
					dt_atualizacao,
					nm_usuario,
					dt_historico,
					ds_titulo,
					ds_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_tipo,
					cd_evento,
					dt_liberacao)
				values(	solic_compra_hist_seq.nextval,
					nr_solic_compra_w,
					sysdate,
					nm_usuario_gtp_w,
					sysdate,
					Wheb_mensagem_pck.get_Texto(298461),
					substr(Wheb_mensagem_pck.get_Texto(140192) || list_oc_w,1,4000),			
					sysdate,
					nm_usuario_gtp_w,
					'S',
					'G',
					sysdate);
        end if;
        exception
			when others then
            begin
            ds_erro_w := substr(sqlerrm,1,3000);
            insert into solic_compra_hist(
					nr_sequencia,
					nr_solic_compra,
					dt_atualizacao,
					nm_usuario,
					dt_historico,
					ds_titulo,
					ds_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_tipo,
					cd_evento,
					dt_liberacao)
				values(	solic_compra_hist_seq.nextval,
					nr_solic_compra_w,
					sysdate,
					nm_usuario_gtp_w,
					sysdate,
					Wheb_mensagem_pck.get_Texto(1145174),
					substr(Wheb_mensagem_pck.get_Texto(1145175) || ds_erro_w,1,4000),	
					sysdate,
					nm_usuario_gtp_w,
					'S',
					null,
					sysdate);
                end;
            end;
		
		
	end if;
	
	delete from w_solic_compra_gtplan
	where nr_sequencia = nr_sequencia_w;
	
	end;
end loop;
close C01;

commit;

end gerar_solic_compra_GTPLAN;
/
