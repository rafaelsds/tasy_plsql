create or replace
procedure update_case_type(	nr_sequencia_p in episodio_paciente.nr_sequencia%type,
							nr_seq_tipo_episodio_p in episodio_paciente.nr_seq_tipo_episodio%type) is

begin

	update 	episodio_paciente
	set 	nr_seq_tipo_episodio = nr_seq_tipo_episodio_p
	where 	nr_sequencia = nr_sequencia_p;
	
	commit;

end update_case_type;
/
