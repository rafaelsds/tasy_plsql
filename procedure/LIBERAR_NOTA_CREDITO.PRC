create or replace
procedure liberar_nota_credito(	nr_seq_nf_credito_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

qt_sem_definicao_w	number(10,0);

begin

if	(nr_seq_nf_credito_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	count(1)
	into	qt_sem_definicao_w
	from	nf_credito_item
	where	nr_seq_nf_credito = nr_seq_nf_credito_p
	and	(nvl(pr_aplicacao,0) <> 0 or nvl(vl_aplicacao,0) <> 0 or nvl(qt_aplicacao,0) <> 0);
	
	if	(qt_sem_definicao_w = 0) then
		--Para liberar a nota de cr�dito, deve alterar o valor de pelo menos um item.
		wheb_mensagem_pck.exibir_mensagem_abort(306941);
	end if;
	
	gerar_nota_fiscal_credito(nr_seq_nf_credito_p,cd_estabelecimento_p,nm_usuario_p);

	update	nf_credito
	set	dt_liberacao = sysdate,
		nm_usuario_lib = nm_usuario_p
	where	nr_sequencia = nr_seq_nf_credito_p;

	commit;

	end;
end if;

end liberar_nota_credito;
/
