create or replace
procedure Gerar_prescr_cirurgia(cd_estabelecimento_p	in	number,
				nr_cirurgia_p		in	number,
				nr_seq_agenda_p		in	number,
				nm_usuario_p		in	varchar2,
				nr_prescricao_p		out	number) is

nr_prescricao_w		number(10,0);
cd_setor_atendimento_w	number(10,0);
cd_setor_atend_agenda_w number(10,0);
dt_inicio_prevista_w	date;
cd_medico_cirurgiao_w	varchar2(10);
cd_pessoa_fisica_w	varchar2(10);
ie_tipo_pessoa_w		number(01,0);
cd_pessoa_agenda_w	varchar2(10);
ie_forma_adep_w		varchar2(10);
ie_adep_w		varchar2(10);
ie_utiliza_med_req_w	varchar2(1);
cd_medico_req_w		varchar2(10);
nr_cirurgia_w		varchar2(10);
ds_erro_w			varchar2(255);
cd_setor_Agenda_ww	varchar2(255);
ie_existe_regra_w		varchar2(1);	
cd_perfil_w				Number(15);
ie_cirurgiao_prescritor_w	varchar2(1);
dt_primeiro_horario_w	prescr_medica.dt_primeiro_horario%type;
cd_medico_w				varchar2(10);


begin

Obter_Param_Usuario(871,174,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_utiliza_med_req_w);
Obter_Param_Usuario(871,154,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, cd_setor_Agenda_ww);
Obter_Param_Usuario(871,831,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_cirurgiao_prescritor_w);

select	nvl(max(nr_prescricao), 0)
into	nr_prescricao_w
from	prescr_medica
where	nr_seq_agenda = nr_seq_agenda_p
and	ie_tipo_prescr_cirur is null;


if	(nr_prescricao_w <> 0) and
	(nr_cirurgia_p <> 0) then
	select	nvl(max(cd_pessoa_fisica), '0')
	into	cd_pessoa_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	
	if	(cd_pessoa_agenda_w <> '0') then
		update	prescr_medica
		set	cd_pessoa_fisica	= cd_pessoa_agenda_w
		where	nr_prescricao	= nr_prescricao_w
		and	(NVL(cd_pessoa_fisica,'0') <> cd_pessoa_agenda_w);
	end if;


end if;

nr_cirurgia_w := nr_cirurgia_p;

begin
	select	dt_inicio_prevista,
			cd_medico_cirurgiao,
			cd_pessoa_fisica,
			cd_setor_atendimento,
			cd_medico_req
	into	dt_inicio_prevista_w,
			cd_medico_cirurgiao_w,
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w,
			cd_medico_req_w
	from	cirurgia
	where	nr_cirurgia = nr_cirurgia_w;
exception
when others then
	nr_cirurgia_w := 0;
end;

if	(nr_prescricao_w = 0) and
	(nr_cirurgia_w <> 0) then
	begin
	
	if	(cd_setor_Agenda_ww is null) then
		select	nvl(max(cd_setor_exclusivo),0)
		into		cd_setor_atend_agenda_w
		from		Agenda b,
				agenda_paciente a
		where 	a.cd_agenda	= b.cd_agenda
		  and	a.nr_sequencia	= nr_seq_agenda_p;

		if	(cd_setor_atend_agenda_w = 0) then
			select cd_setor_atendimento
			into	cd_setor_atend_agenda_w
			from usuario
			where nm_usuario = nm_usuario_p;
		end if;
	end if;
	
	select	nvl(max(b.ie_tipo_pessoa),'1')
	into	ie_tipo_pessoa_w
	from	pessoa_fisica b,
		usuario a
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nm_usuario	= nm_usuario_p;

	select	prescr_medica_SEQ.NEXTVAL
	into	nr_prescricao_w
	from	dual;
	
	Obter_Param_Usuario(924,246,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_forma_adep_w);
		
	if	(ie_forma_adep_w = 'DS') then
		select	nvl(max(ie_adep),'N')
		into	ie_adep_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;
	elsif	(ie_forma_adep_w = 'NV') then
		ie_adep_w   := 'N';
	elsif	(ie_forma_adep_w = 'PV') then
		ie_adep_w := 'S';
	elsif	(ie_forma_adep_w = 'PNV') then
		ie_adep_w := 'N';
	else	
		ie_adep_w := 'S';
	end if;
	
	cd_perfil_w	:= nvl(Obter_perfil_ativo,0);
		
	select	decode(count(*),0, 'N','S')
	into	ie_existe_regra_w
	from	prescr_horario_setor
	where	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0))	= nvl(cd_setor_atendimento_w,0)
	and	nvl(nm_usuario_regra, nvl(nm_usuario_p, 0))			= nvl(nm_usuario_p, 0)
	and	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and	nvl(cd_perfil, cd_perfil_w)			= cd_perfil_w
	and	(('R'	= nvl(ie_funcao,'A')) or (nvl(ie_funcao,'A')	= 'A'));
		
	select	nvl(max(Obter_Prim_Horario_Prescricao(a.nr_atendimento, cd_setor_atendimento_w, to_date(to_char(a.dt_agenda,'dd/mm/yyyy') || ' ' || to_char(hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'), nm_usuario_p, 'R')),sysdate),
			max(a.cd_medico)
	into	dt_primeiro_horario_w,
			cd_medico_w
	from	agenda_paciente a
	where	a.nr_sequencia = nr_seq_agenda_p; 
	
	begin
	insert	into prescr_medica(
		dt_prescricao,
		cd_medico,
		cd_pessoa_fisica,
		cd_estabelecimento,
		nm_usuario_original,
		nr_horas_validade,
		nr_prescricao,
		ie_recem_nato,
		ie_origem_inf,
		dt_primeiro_horario,
		nm_usuario,
		dt_atualizacao,
		cd_prescritor,
		nr_seq_agenda,
		ie_adep,
		CD_SETOR_ATENDIMENTO,
		CD_SETOR_entrega)
	values(	dt_inicio_prevista_w,
		decode(ie_utiliza_med_req_w,'S',nvl(cd_medico_req_w, cd_medico_cirurgiao_w), cd_medico_cirurgiao_w), 
		cd_pessoa_fisica_w,
		cd_estabelecimento_p,
		nm_usuario_p,
		24,
		nr_prescricao_w,
		'N', 
		ie_tipo_pessoa_w,
		decode(ie_existe_regra_w, 'S', dt_primeiro_horario_w, sysdate),
		nm_usuario_p,
		sysdate,
		decode(ie_cirurgiao_prescritor_w, 'S', cd_medico_w, obter_dados_usuario_opcao(nm_usuario_p, 'C')),
		decode(nr_seq_agenda_p,0,null,nr_seq_agenda_p),
		ie_adep_w,
		cd_setor_atend_agenda_w,
		cd_setor_atend_agenda_w);
	exception
	when others then
		ds_erro_w 	:= substr(SQLERRM(sqlcode),1,255);
		gerar_cirurgia_hist(nr_cirurgia_w,'GP',wheb_usuario_pck.get_nm_usuario,ds_erro_w || ' - '||nr_prescricao_w ||' - '||nr_seq_agenda_p,'S');
	end;	
	end;

end if;

commit;

nr_prescricao_p	:= nr_prescricao_w;

end Gerar_prescr_cirurgia;
/
