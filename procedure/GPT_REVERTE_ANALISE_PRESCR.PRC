create or replace
procedure gpt_reverte_analise_prescr(	nr_atendimento_p		number,
										nm_usuario_p			Varchar2,
										cd_estabelecimento_p	number) is
										
begin

if 	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null)  then

	update	prescr_medica
	set		dt_inicio_analise_farm = null,
		 	nm_usuario_analise_farm = null
	where	dt_inicio_analise_farm is not null
	and		dt_liberacao is not null
	and 	dt_liberacao_farmacia is null
	and 	cd_funcao_origem not in (924,950)
	and		nr_seq_atend is null
	and		dt_prescricao between trunc(sysdate) and fim_dia(sysdate+7)
	and		nr_atendimento 					= nr_atendimento_p
	and	 	nm_usuario_analise_farm			= nm_usuario_p;

	commit;

end if;

end gpt_reverte_analise_prescr;
/
