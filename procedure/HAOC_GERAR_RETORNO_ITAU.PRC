create or replace
procedure HAOC_GERAR_RETORNO_ITAU
		(nr_seq_banco_escrit_p	in	number,
		nm_usuario_p		in	varchar2) is

ds_nr_titulo_w		varchar2(255);
ds_dt_pagamento_w	varchar2(255);
ds_vl_pagamento_w	varchar2(255);
ds_nr_documento_w	varchar2(255);
ds_ocorrencia_w		varchar2(255);

ie_reg_favorecido_w	number(1);
ie_conf_envio_w		number(1);
ie_retorno_liq_w	number(1);

nr_titulo_w		number(10,0);
cd_tipo_baixa_w		number(10,0);
nr_seq_trans_escrit_w	number(10,0);
nr_seq_conta_banco_w	number(10,0);
nr_sequencia_w		number(10,0);
dt_pagamento_w		date;
vl_pagamento_w		number(15,2);
nr_documento_w		varchar2(255);
ds_forma_pagto_w	varchar2(50);
dt_vencimento_w		date;

cd_estabelecimento_w		number(10,0);
nr_sequencia_inicial_w		number(10,0);
nr_sequencia_final_w		number(10,0);
nr_seq_interf_w			number(10,0);
cd_erro_w			varchar2(2);
cd_banco_w			number(5);
vl_saldo_titulo_w			number(15,2);
nr_seq_trans_fin_baixa_w		number(10);
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_desconto_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_remessa_w			number(15,2);

cursor c01 is
select	nr_sequencia,
	substr(ds_conteudo,12,2) ds_forma_pagto
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '1'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
order	by nr_sequencia;

cursor c02 is
select	nr_sequencia,
	substr(ds_conteudo,74,20) ds_nr_titulo,
	substr(ds_conteudo,155,8) ds_dt_pagamento,
	substr(ds_conteudo,163,15) ds_vl_pagamento,
	substr(ds_conteudo,198,6) ds_nr_documento,
	substr(ds_conteudo,231,10) ds_ocorrencia
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
and	ds_forma_pagto_w		in ('03','01','41')
and	nr_sequencia			> nr_sequencia_inicial_w
and	nr_sequencia			< nr_sequencia_final_w		-- pagamento em doc
union
select	nr_sequencia,
	substr(ds_conteudo,183,20) ds_nr_titulo,
	substr(ds_conteudo,145,8) ds_dt_pagamento,
	substr(ds_conteudo,153,15) ds_vl_pagamento,
	'' ds_nr_documento,
	substr(ds_conteudo,231,10) ds_ocorrencia
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
and	ds_forma_pagto_w		in ('30','31')
and	nr_sequencia			> nr_sequencia_inicial_w
and	nr_sequencia			< nr_sequencia_final_w		-- pagamento com bloqueto
union					
select	nr_sequencia,
	substr(ds_conteudo,175,20) ds_nr_titulo,
	substr(ds_conteudo,137,8) ds_dt_pagamento,
	substr(ds_conteudo,122,15) ds_vl_pagamento,
	'' ds_nr_documento,
	substr(ds_conteudo,231,10) ds_ocorrencia
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
and	ds_forma_pagto_w		in ('13')
and	nr_sequencia			> nr_sequencia_inicial_w
and	nr_sequencia			< nr_sequencia_final_w
order	by nr_sequencia;						-- pagamento de concession�rias

/* cursor	c03 is
select	a.cd_erro
from	erro_escritural a
where	a.cd_banco	= cd_banco_w; */

begin

select	max(a.cd_estabelecimento),
	max(a.nr_seq_conta_banco),
	max(a.cd_banco)
into	cd_estabelecimento_w,
	nr_seq_conta_banco_w,
	cd_banco_w
from	banco_escritural a
where	a.nr_sequencia		= nr_seq_banco_escrit_p;

select	max(nr_seq_trans_escrit)
into	nr_seq_trans_escrit_w
from	parametro_tesouraria
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(max(cd_tipo_baixa_padrao),1)
into	cd_tipo_baixa_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

open c01;
loop
fetch c01 into
	nr_sequencia_inicial_w,
	ds_forma_pagto_w;
exit when c01%notfound;

	begin

	select	min(nr_sequencia)
	into	nr_sequencia_final_w
	from	w_interf_retorno_itau
	where	substr(ds_conteudo, 8, 1)	<> '3'
	and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
	and	nr_sequencia			> nr_sequencia_inicial_w;

	open c02;
	loop 
	fetch c02 into
		nr_seq_interf_w,
		ds_nr_titulo_w,
		ds_dt_pagamento_w,
		ds_vl_pagamento_w,
		ds_nr_documento_w,
		ds_ocorrencia_w;
	exit when c02%notfound;

		select	max(a.nr_titulo),
			to_number(obter_saldo_titulo_pagar(max(a.nr_titulo),sysdate)),
			max(a.nr_seq_trans_fin_baixa),
			max(a.dt_vencimento_atual)
		into	nr_titulo_w,
			vl_saldo_titulo_w,
			nr_seq_trans_fin_baixa_w,
			dt_vencimento_w
		from	titulo_pagar a
		where	a.nr_titulo	= to_number(ds_nr_titulo_w);

		if	(nr_titulo_w is not null) and (vl_saldo_titulo_w <> 0) then

			dt_pagamento_w		:= obter_proximo_dia_util(cd_estabelecimento_w,nvl(dt_vencimento_w,to_date(ds_dt_pagamento_w, 'ddmmyyyy')));
			vl_pagamento_w		:= to_number(ds_vl_pagamento_w);
			vl_pagamento_w		:= dividir_sem_round(vl_pagamento_w, 100);
			nr_documento_w		:= ds_nr_documento_w;
   		
			gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);

			select	max(decode(a.cd_reg_favorecido,ds_ocorrencia_w,1,0)),
				max(decode(a.cd_conf_envio,ds_ocorrencia_w,1,0)),
				max(decode(a.cd_retorno_liq,ds_ocorrencia_w,1,0))
			into	ie_reg_favorecido_w,
				ie_conf_envio_w,
				ie_retorno_liq_w
			from	banco_retorno_cp a
			where	a.cd_banco	= cd_banco_w;

			if 	(ie_reg_favorecido_w	= 1) then
				update	titulo_pagar_escrit
				set	ds_erro			= ds_ocorrencia_w
				where	nr_seq_escrit		= nr_seq_banco_escrit_p
				and	nr_titulo		= nr_titulo_w;
			elsif	(ie_conf_envio_w	= 1) then
				update	titulo_pagar_escrit
				set	ds_erro			= ds_ocorrencia_w
				where	nr_seq_escrit		= nr_seq_banco_escrit_p
				and	nr_titulo		= nr_titulo_w;
			elsif	(ie_retorno_liq_w	= 1) then
				/*	BD � agendamento e n�o deve baixar
					00 � liquida��o e deve baixar */

				dbms_output.put_line('nr_titulo_w = ' || nr_titulo_w);

				select	nvl(max(a.vl_juros),0),
					nvl(max(a.vl_desconto),0),
					nvl(max(a.vl_multa),0),
					nvl(max(a.vl_acrescimo),0),
					nvl(max(a.vl_escritural),0)
				into	vl_juros_w,
					vl_desconto_w,
					vl_multa_w,
					vl_acrescimo_w,
					vl_remessa_w
				from	banco_escritural b,
					titulo_pagar_escrit a
				where	b.ie_remessa_retorno	= 'R'
				and	a.nr_seq_escrit		= b.nr_sequencia
				and	a.nr_titulo		= nr_titulo_w;

				if	(vl_remessa_w <> nvl(vl_pagamento_w,0)) then
					vl_pagamento_w	:= nvl(vl_pagamento_w,0) + vl_desconto_w - vl_juros_w - vl_multa_w - vl_acrescimo_w;
				end if;

				update	titulo_pagar_escrit
				set	vl_escritural	= vl_pagamento_w,
					vl_juros		= vl_juros_w,
					vl_multa		= vl_multa_w,
					vl_desconto	= vl_desconto_w,
					vl_acrescimo	= vl_acrescimo_w
				where	nr_titulo		= nr_titulo_w
				and	nr_seq_escrit	= nr_seq_banco_escrit_p;

				baixa_titulo_pagar
						(cd_estabelecimento_w,
						cd_tipo_baixa_w,
						nr_titulo_w,
						vl_pagamento_w,
						nm_usuario_p,
						nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_escrit_w),
						null,
						nr_seq_banco_escrit_p,
						dt_pagamento_w,
						nr_seq_conta_banco_w);

				select	max(nr_sequencia)
				into	nr_sequencia_w
				from	titulo_pagar_baixa
				where	nr_titulo	= nr_titulo_w;

				gerar_movto_tit_baixa
						(nr_titulo_w,
						nr_sequencia_w,
						'P',
						nm_usuario_p,
						'N');

				atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
				Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
			end if;
		
			update	titulo_pagar_escrit
			set	ds_erro			= ds_ocorrencia_w
			where	nr_seq_escrit		= nr_seq_banco_escrit_p
			and	nr_titulo		= nr_titulo_w;

		end if;

	end loop;
	close c02;


	exception
	when others then
		rollback;
		delete	from w_interf_retorno_itau
		where	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;
		commit;
		raise_application_error(-20011, sqlerrm || chr(13) ||
					'nr_titulo_w = ' || nr_titulo_w || chr(13) ||
					'ds_nr_titulo_w = ' || ds_nr_titulo_w || chr(13) ||
					'ds_dt_pagamento_w = ' || ds_dt_pagamento_w || chr(13) ||
					'ds_vl_pagamento_w = ' || ds_vl_pagamento_w || chr(13) ||
					'ds_nr_documento_w = ' || ds_nr_documento_w || chr(13) ||
					'ds_ocorrencia_w = ' || ds_ocorrencia_w); 

	end;



end loop;
close c01;

delete	from w_interf_retorno_itau
where	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

commit;

end HAOC_GERAR_RETORNO_ITAU;
/