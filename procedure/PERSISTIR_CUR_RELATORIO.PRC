create or replace procedure persistir_cur_relatorio(
    nm_usuario_p cur_relatorio.nm_usuario%type,
    ds_relatorio_p cur_relatorio.ds_relatorio%type,
    ds_arquivo_p cur_relatorio.ds_arquivo%type,
    nr_atendimento_p cur_relatorio.nr_atendimento%type
) is

nr_seq_cur_relatorio_w cur_relatorio.nr_sequencia%type;

begin

    SELECT cur_relatorio_seq.NEXTVAL
    INTO nr_seq_cur_relatorio_w
    FROM dual;

    insert into cur_relatorio (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        ds_relatorio,
        ds_arquivo,
        nr_atendimento
    )
    values (
        nr_seq_cur_relatorio_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        ds_relatorio_p,
        ds_arquivo_p,
        nr_atendimento_p
    );

    commit;
end;
/
