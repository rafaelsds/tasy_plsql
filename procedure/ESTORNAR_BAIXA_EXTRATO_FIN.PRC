create or replace
procedure estornar_baixa_extrato_fin ( nr_seq_extrato_arq_p number,
                     nm_usuario_p     varchar2) is 


nr_seq_parcela_w      extrato_cartao_cr_movto.nr_seq_parcela%type;
nr_seq_baixa_w        movto_cartao_cr_baixa.nr_sequencia%type;        

Cursor  c01 is
select  b.nr_seq_parcela
from  extrato_cartao_cr_movto b,
    extrato_cartao_cr_res a
where b.nr_seq_parcela    is not null
and   a.nr_sequencia      = b.nr_seq_extrato_res
and   a.nr_seq_extrato_arq  = nr_seq_extrato_arq_p
union
select  d.nr_sequencia
from  movto_cartao_cr_parcela d,
    extrato_cartao_cr_parcela c,
    extrato_cartao_cr_movto b,
    extrato_cartao_cr_res a
where c.nr_sequencia        = d.nr_seq_extrato_parcela
and   b.nr_seq_extrato_parcela  = c.nr_sequencia
and   a.nr_sequencia        = b.nr_seq_extrato_res
and   a.nr_seq_extrato_arq    = nr_seq_extrato_arq_p;

begin

/* Estorno das parcelas */

open  c01;
loop
fetch c01 into
  nr_seq_parcela_w;
exit  when c01%notfound;

  select  max(a.nr_sequencia)
  into  nr_seq_baixa_w
  from  movto_cartao_cr_baixa a
  where a.nr_seq_parcela = nr_seq_parcela_w
  and   a.vl_baixa > 0 --  A baixa tem que ser de valor positivo, pois se for negativo ja e uma baixa de estorno.
  and   a.nr_seq_baixa_orig is null -- Nao pode ser uma baixa que tenha origem, pois se tiver origem quer dizer que e estorno 
  and   not exists (select 1 --Nao pode existir uma baixa gerada nessa parcela que a origem da mesma seja essa, pois indica que ela ja foi estornada.
            from   movto_cartao_cr_baixa x
            where  x.nr_seq_baixa_orig  = a.nr_sequencia
            and    x.nr_seq_parcela   = nr_seq_parcela_w);

if	(nr_seq_baixa_w is not null) then
  
  Estornar_movto_cartao_baixa ( nr_seq_baixa_w,
                  nm_usuario_p,
                  'S', -- Passa fixo S aqui igual ocorre na chamada dessa proc diretamente no CorCre_F5, no bt direito estornar_baixa_parcela
                  'N', -- O commit vai ocorrer apenas no final dessa proc, e nao a cada  estorno.
                  null);
end if;
end loop;
close c01;

update  extrato_cartao_cr_arq
set   dt_baixa  = null,
    dt_atualizacao  = sysdate,
    nm_usuario  = nm_usuario_p
where nr_sequencia  = nr_seq_extrato_arq_p;

commit;

end estornar_baixa_extrato_fin;
/
