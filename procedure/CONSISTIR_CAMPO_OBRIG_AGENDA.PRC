create or replace
procedure Consistir_campo_obrig_agenda(	cd_agenda_p		number,
				qt_peso_p		varchar2,
				qt_altura_cm_p		varchar2,
				cd_medico_req_p		varchar2,
				ie_anestesia_p		varchar2,
				nr_seq_motivo_anest_p	varchar2,
				nr_seq_unid_solic_ext_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2,
				ds_erro_p		out	varchar2) is 

ie_peso_w		varchar2(1);
ie_altura_w		varchar2(1);
ie_requisitante_w		varchar2(1);
ie_motivo_anestesia_w	varchar2(1);
ie_unidade_externa_w	varchar2(1);
ds_campos_w		varchar2(255) := '';
ds_enter_w		varchar2(10)  :=  chr(13) || chr(10);       
				       
begin

select	nvl(max(ie_peso),'N'),
	nvl(max(ie_altura),'N'),
	nvl(max(ie_requisitante),'N'),
	nvl(max(ie_motivo_anestesia),'N'),
	nvl(max(ie_unidade_externa),'N')
into	ie_peso_w,
	ie_altura_w,
	ie_requisitante_w,
	ie_motivo_anestesia_w,
	ie_unidade_externa_w
from	regra_agenda_campo_obrig
where	cd_agenda = cd_agenda_p;

if	(ie_peso_w	= 'S' and qt_peso_p is null) then                                 
	ds_campos_w	:= ds_campos_w ||WHEB_MENSAGEM_PCK.get_texto(279596,null);
end if;                                                                         

if	(ie_altura_w	= 'S' and qt_altura_cm_p is null) then                          
	ds_campos_w	:= ds_campos_w ||WHEB_MENSAGEM_PCK.get_texto(279595,null);
end if;                                                                         

if	(ie_requisitante_w	= 'S' and cd_medico_req_p is null) then                          
	ds_campos_w	:= ds_campos_w ||WHEB_MENSAGEM_PCK.get_texto(281469,null);
end if;

if	(ie_motivo_anestesia_w	= 'S' and nr_seq_motivo_anest_p is null and ie_anestesia_p = 'S') then                          
	ds_campos_w	:= ds_campos_w ||WHEB_MENSAGEM_PCK.get_texto(281471,null);
end if;

if	(ie_unidade_externa_w	= 'S' and nr_seq_unid_solic_ext_p = 0) then                          
	ds_campos_w	:= ds_campos_w ||WHEB_MENSAGEM_PCK.get_texto(281474,null);
end if;

if	(ds_campos_w is not null) then                                                 
	ds_erro_p	:= substr(wheb_mensagem_pck.get_texto(281479,null) || ds_enter_w || substr(ds_campos_w,1,length(ds_campos_w||' ')-2),1,255);
end if;

end Consistir_campo_obrig_agenda;
/