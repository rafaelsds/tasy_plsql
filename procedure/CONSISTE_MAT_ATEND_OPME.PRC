create or replace 
procedure consiste_mat_atend_opme(cd_material_p		number,
				qt_material_p		number,
				nr_seq_lote_fornec_p	number,
				cd_estabelecimento_p	number,
				cd_tipo_baixa_p		number,
				cd_local_estoque_p	number,
				nm_usuario_p           	varchar2,
				ds_erro_p	out 	varchar2) is


ie_saldo_estoque_w		varchar2(1);
cd_cgc_fornec_w			varchar2(14);
ie_consignado_w			varchar2(1);
ds_erro_w			varchar2(255);
ie_consiste_saldo_lote_w	varchar2(1);
qt_saldo_lote_fornec_w		number(13,4);
ie_atualiza_estoque_w		varchar2(1);
ie_estoque_disp_w		varchar2(1);
	
begin

select	nvl(max(ie_consignado),'0')
into	ie_consignado_w
from	material
where	cd_material = cd_material_p;

select	nvl(max(ie_atualiza_estoque),'S')
into	ie_atualiza_estoque_w
from	tipo_baixa_prescricao
where	ie_prescricao_devolucao = 'P'
and 	cd_tipo_baixa = cd_tipo_baixa_p
and 	ie_situacao = 'A';

if	(ie_consignado_w <> '0') then
	begin
	if	(nvl(nr_seq_lote_fornec_p, 0) > 0) then
		select	cd_cgc_fornec
		into	cd_cgc_fornec_w
		from	material_lote_fornec
		where	nr_sequencia = nr_seq_lote_fornec_p;
	else
		cd_cgc_fornec_w	:= obter_fornecedor_regra_consig(
					cd_estabelecimento_p,
					cd_material_p,
					'1');
	end if;
	end;
end if;	

obter_disp_estoque(cd_material_p, cd_local_estoque_p, cd_estabelecimento_p, 0, qt_material_p, cd_cgc_fornec_w, ie_saldo_estoque_w);

if	((ie_saldo_estoque_w = 'S') or (nvl(ie_atualiza_estoque_w,'S') = 'N')) then
	ie_estoque_disp_w:= 'S';
else
	ie_estoque_disp_w:= 'N';
end if;

if	(nvl(nr_seq_lote_fornec_p, 0) > 0) then
	qt_saldo_lote_fornec_w	:= obter_saldo_lote_fornec(nr_seq_lote_fornec_p);
end if;
	
if 	((ie_saldo_estoque_w <> 'S') and (ie_estoque_disp_w <> 'S')) then
	ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(278857);
elsif	(ie_consiste_saldo_lote_w = 's') and
	(nvl(nr_seq_lote_fornec_p,0) > 0) and
	(nvl(qt_saldo_lote_fornec_w,0) <= 0) then
	ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(278858) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(278859);
end if;

ds_erro_p:= ds_erro_w;

end consiste_mat_atend_opme;
/