create or replace
procedure pls_consiste_data_sib
			(dt_referencia_p		date,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number) is

count_w			number(10);
vl_parametro_w		varchar2(1);

begin

Obter_Param_Usuario(1202, 16, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);

if	(nvl(vl_parametro_w,'N') = 'S') then
	select	count(1)
	into	count_w
	from	pls_lote_sib
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_envio		= 'E'
	and	(trunc(dt_referencia_p,'month') < trunc(dt_mesano_referencia,'month')
	or	trunc(dt_referencia_p,'month') <
				(select	max(trunc(dt_mesano_referencia,'month'))
				from	pls_lote_sib
				where	cd_estabelecimento = cd_estabelecimento_p));

	if	(count_w = 0) then
		select	count(1)
		into	count_w
		from	pls_sib_lote
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	ie_tipo_lote		= 'M'
		and	ie_status		<> 1
		and	dt_referencia_p between trunc(dt_inicio_mov,'dd') and fim_dia(dt_fim_mov);
	end if;

	if	(count_w > 0) then
		-- J� foi enviado um lote para o SIB no m�s #@DT_REFERENCIA#@.
		wheb_mensagem_pck.exibir_mensagem_abort(266786, 'DT_REFERENCIA=' || to_char(dt_referencia_p,'mm/yyyy'));
	end if;
end if;

end pls_consiste_data_sib;
/