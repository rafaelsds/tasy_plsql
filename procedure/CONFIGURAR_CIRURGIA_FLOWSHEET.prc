create or replace procedure configurar_cirurgia_flowsheet(	nr_cirurgia_p 	number,
											nr_seq_pepo_p	number,	
											nr_seq_modelo_p number,
											nm_usuario_p 	varchar2) is
										
cursor c_secao_nova is
select  nvl(a.ie_modo_visualizacao, 'VI') ie_modo_visualizacao,
		a.nr_sequencia,
		a.nr_seq_apresentacao,
		a.ds_titulo
from 	pepo_modelo_secao a
where 	a.nr_seq_modelo = nr_seq_modelo_p
and not exists(	select 1 
				from 	w_flowsheet_config_grupo b 
				where 	b.nr_seq_modelo_secao = a.nr_sequencia
				and		nvl(b.nr_cirurgia,0) = nvl(nr_cirurgia_p,0)
				and 	nvl(b.nr_seq_pepo,0) = nvl(nr_seq_pepo_p, 0)
				and 	b.nm_usuario  	= nm_usuario_p
				and 	b.nr_seq_modelo = nr_seq_modelo_p
				);

nr_seq_config_grupo_w number(10);

begin
if (nr_seq_modelo_p is not null) then
update  w_flowsheet_config_grupo a 
set 	a.ds_titulo		       = (select max(b.ds_titulo) from pepo_modelo_secao b where a.nr_seq_modelo_secao = b.nr_sequencia),
		a.nr_seq_apresentacao  = (select max(b.nr_seq_apresentacao) from pepo_modelo_secao b where a.nr_seq_modelo_secao = b.nr_sequencia),
		a.nm_usuario  	 	   = nm_usuario_p,
		a.dt_atualizacao 	   = sysdate
where nvl(a.nr_cirurgia,0) 	 = nvl(nr_cirurgia_p,0)
and 	nvl(a.nr_seq_pepo,0) = nvl(nr_seq_pepo_p, 0)
and 	a.nm_usuario  		 = nm_usuario_p
and 	a.nr_seq_modelo 	 = nr_seq_modelo_p;	

<<read_secao_nova>>
for r_secao_nova in c_secao_nova
	loop
	select   w_flowsheet_config_grupo_seq.nextval
	into     nr_seq_config_grupo_w
	from     dual;	
	
	insert into w_flowsheet_config_grupo(nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_cirurgia,
										nr_seq_pepo,
										nr_seq_modelo,
										ie_modo_visualizacao,
										nr_seq_modelo_secao,
										nr_seq_apresentacao,
										ds_titulo
										)
	values								(nr_seq_config_grupo_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_cirurgia_p,
										nr_seq_pepo_p,
										nr_seq_modelo_p,
										r_secao_nova.ie_modo_visualizacao,
										r_secao_nova.nr_sequencia,
										r_secao_nova.nr_seq_apresentacao,
										r_secao_nova.ds_titulo
										);									
end loop read_secao_nova;

commit;

end if;
end configurar_cirurgia_flowsheet;
/