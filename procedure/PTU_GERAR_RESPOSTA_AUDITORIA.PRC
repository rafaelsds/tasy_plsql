create or replace
procedure ptu_gerar_resposta_auditoria
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_complemento_p	Number,
				ds_mensagem_p		Varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2,
				nr_seq_origem_p	out	Number) is

cd_unimed_executora_w		Number(4);
cd_unimed_beneficiario_w	Number(4);
nr_seq_execucao_w		Number(10);
nr_seq_origem_			Number(10);
ds_observacao_w			Varchar2(4000);
dt_validade_senha_w		Date;
nr_seq_auditoria_w		Number(10);
cd_proced_w			Number(8);
ie_origem_proced_w		Number(10);
qt_proced_w			Number(7,3);
ie_status_proc_w		Varchar2(2);
ie_proc_autor_w			Number(2);
nr_seq_mat_w			Number(10);
qt_material_w			Number(7,3);
ie_status_mat_w			Varchar2(2);
ie_mat_autor_w			Number(2);
ds_material_w			Varchar2(255);
ds_proced_w			Varchar2(255);
qt_registros_w			Number(2);
ie_tipo_cliente_w		Varchar2(2);
ie_classificacao_w		Number(2);
ie_tipo_tabela_w		Number(2);
ie_tipo_despesa_w		Number(2);
nr_seq_compl_ptu_w		Number(10);
cd_servico_w			Number(8);
nr_seq_pacote_w			Number(10);
ie_autorizado_w			number(1);
qt_servico_w			number(10);
ie_origem_servico_w		number(10);
ie_tipo_servico_w		varchar2(2);
ie_status_complemento_w		number(1);
ds_servico_w			varchar2(80);
nr_seq_material_w		number(10);
nr_seq_guia_w			number(10);
qt_reg_aprov_w			number(4);
qt_reg_neg_w			number(4);
nr_seq_pedido_w			number(10);
qt_reg_auditoria_w		number(10) := 0;
nr_seq_requisicao_w		number(10);
nr_seq_congenere_w		number(10);
cd_servico_conversao_w		number(15);
ie_origem_proced_ww		number(10);
nr_seq_regra_w			number(10);
nr_seq_segurado_w		number(10);
ds_opme_w			Varchar2(255);
nr_seq_pedido_aut_w		number(10);
cd_material_ptu_w		number(10);
nr_seq_pedido_aut_serv_w	number(10);
ie_estagio_w			number(10);
nr_seq_pedido_compl_w		number(10);
cd_proc_mat_w			number(10);

Cursor C01 is
	select	cd_servico,
		cd_servico_consersao,
		ie_tipo_tabela,
		substr(ds_opme,1,80)
	from	ptu_pedido_aut_servico
	where	nr_seq_pedido = nr_seq_pedido_aut_w
	union
	select	cd_servico,
		cd_servico_conversao,
		to_char(ie_tipo_tabela),
		substr(ds_opme,1,80)
	from	ptu_pedido_compl_aut_serv
	where	nr_seq_pedido = nr_seq_pedido_compl_w;

Cursor c02 is
	select	ie_origem_proced,
		qt_autorizada,
		ie_status
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p
	and	cd_procedimento		= nvl(cd_servico_conversao_w,cd_servico_w)
	union
	select	ie_origem_proced,
		qt_procedimento,
		ie_status
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	cd_procedimento		= nvl(cd_servico_conversao_w,cd_servico_w);

Cursor c03 is
	select	qt_autorizada,
		ie_status
	from	pls_guia_plano_mat
	where	nr_seq_guia		= nr_seq_guia_p
	and	nr_seq_material		= pls_obter_seq_codigo_material('',nvl(cd_servico_conversao_w,cd_servico_w))
	union
	select	qt_material,
		ie_status
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	nr_seq_material		= pls_obter_seq_codigo_material('',nvl(cd_servico_conversao_w,cd_servico_w));

begin

if	(nr_seq_guia_p	is not null) then
	begin
	select	a.nr_sequencia
	into	nr_seq_origem_p
	from	ptu_pedido_autorizacao	b,
		ptu_controle_execucao		a
	where	a.nr_seq_pedido_aut		= b.nr_sequencia
	and	b.nr_seq_guia			= nr_seq_guia_p;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(179719);
	end;

	select	count(1)
	into	qt_reg_auditoria_w
	from	ptu_resposta_auditoria
	where	nr_seq_origem	= nr_seq_origem_p;

	if	(qt_reg_auditoria_w	> 0) then
		goto final;
	end if;

	begin
	select	cd_unimed_executora,
		cd_unimed_beneficiario,
		nr_seq_execucao,
		ie_tipo_cliente
	into	cd_unimed_executora_w,
		cd_unimed_beneficiario_w,
		nr_seq_execucao_w,
		ie_tipo_cliente_w
	from	ptu_resposta_autorizacao
	where	nr_seq_origem	= nr_seq_origem_p;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(179720);
	end;

	select	nvl(ds_observacao,' '),
		nvl(dt_validade_senha, sysdate),
		nr_seq_segurado
	into	ds_observacao_w,
		dt_validade_senha_w,
		nr_seq_segurado_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;

	pls_guia_gravar_historico(nr_seq_guia_p,2,substr('Enviada a resposta de auditoria para a Unimed '||cd_unimed_executora_w||' com a mensagem: '||chr(10)||ds_mensagem_p,1,4000),'',nm_usuario_p);
elsif	(nr_seq_requisicao_p	is not null) then
	begin
	select	a.nr_sequencia,
		a.nr_seq_pedido_aut
	into	nr_seq_origem_p,
		nr_seq_pedido_aut_w
	from	ptu_pedido_autorizacao	b,
		ptu_controle_execucao		a
	where	a.nr_seq_pedido_aut		= b.nr_sequencia
	and	b.nr_seq_requisicao		= nr_seq_requisicao_p;
	exception
	when others then
		begin
			select	a.nr_sequencia,
				a.nr_seq_pedido_compl
			into	nr_seq_origem_p,
				nr_seq_pedido_compl_w
			from	ptu_pedido_compl_aut	b,
				ptu_controle_execucao	a
			where	a.nr_seq_pedido_compl	= b.nr_sequencia
			and	b.nr_seq_requisicao	= nr_seq_requisicao_p;
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(179719);
		end;
	end;

	select	count(1)
	into	qt_reg_auditoria_w
	from	ptu_resposta_auditoria
	where	nr_seq_origem	= nr_seq_origem_p;

	if	(qt_reg_auditoria_w	> 0) then
		goto final;
	end if;

	begin
	select	cd_unimed_executora,
		cd_unimed_beneficiario,
		nr_seq_execucao,
		nr_seq_origem,
		ie_tipo_cliente
	into	cd_unimed_executora_w,
		cd_unimed_beneficiario_w,
		nr_seq_execucao_w,
		nr_seq_origem_p,
		ie_tipo_cliente_w
	from	ptu_resposta_autorizacao
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(179720);
	end;

	select	nvl(ds_observacao,' '),
		nvl(dt_validade_senha, sysdate),
		nr_seq_segurado
	into	ds_observacao_w,
		dt_validade_senha_w,
		nr_seq_segurado_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_p;

	pls_requisicao_gravar_hist(nr_seq_requisicao_p,'L',substr('Enviada a resposta de auditoria para a Unimed '||cd_unimed_executora_w||' com a mensagem: '||chr(10)||ds_mensagem_p,1,4000),null,nm_usuario_p);
end if;

select	ptu_resposta_auditoria_seq.NextVal
into	nr_seq_auditoria_w
from	dual;

insert	into ptu_resposta_auditoria
	(nr_sequencia, cd_transacao, ie_tipo_cliente,
	 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
	 nr_seq_origem, ds_observacao, dt_atualizacao,
	 nm_usuario, dt_validade, nr_seq_guia,
	 nr_seq_requisicao)
values	(nr_seq_auditoria_w, '00304', ie_tipo_cliente_w,
	 cd_unimed_executora_w, cd_unimed_beneficiario_w, nr_seq_execucao_w,
	 nr_seq_origem_p, nvl(replace(replace(ds_mensagem_p,chr(13),''),chr(10),''),' '), sysdate,
	 nm_usuario_p, dt_validade_senha_w, nr_seq_guia_p,
	 nr_seq_requisicao_p);

select	max(nr_sequencia)
into	nr_seq_pedido_w
from	ptu_resposta_autorizacao
where	nr_seq_origem		= nr_seq_origem_p;

if	((nr_seq_guia_p	is not null) or (nr_seq_requisicao_p is not null)) then
	open c01;
	loop
	fetch c01 into
		cd_servico_w,
		cd_servico_conversao_w,
		ie_tipo_tabela_w,
		ds_opme_w;
	exit when c01%notfound;
		begin
		ie_status_proc_w	:= '';
		ie_status_mat_w		:= '';
		
		if	(ie_tipo_tabela_w	in('0','1','4')) then
			open c02;
			loop
			fetch c02 into
				ie_origem_proced_w,
				qt_proced_w,
				ie_status_proc_w;
			exit when c02%notfound;
			end loop;
			close c02;

			if	(ie_origem_proced_w is null) then
				select	max(ie_origem_proced)
				into	ie_origem_proced_w
				from    procedimento
				where   cd_procedimento		= nvl(cd_servico_conversao_w,cd_servico_w)
				and	ie_origem_proced	in(1,4,5)
				and	ie_situacao		= 'A';
			end if;

			if	(ie_status_proc_w	in ('M','G','C','N','D','K')) then
				ie_proc_autor_w	:= 1;
			elsif	(ie_status_proc_w	in ('P','S','T','L')) then
				ie_proc_autor_w	:= 2;
			end if;
			
			if	(cd_servico_w	is not null) then
				if	(nvl(ds_opme_w,'X')	= 'X') then
					ds_proced_w	:= substr(obter_descricao_procedimento(nvl(cd_servico_conversao_w,cd_servico_w),ie_origem_proced_w),1,80);
				else
					ds_proced_w	:= ds_opme_w;
				end if;

				insert	into ptu_resp_auditoria_servico
					(nr_sequencia, ie_tipo_tabela, cd_servico,
					 ie_origem_servico, ds_servico, ie_autorizado,
					 dt_atualizacao, nm_usuario, qt_autorizado,
					 nr_seq_auditoria, cd_servico_conversao)
				values	(ptu_resp_auditoria_servico_seq.NextVal, ie_tipo_tabela_w, cd_servico_w,
					 ie_origem_proced_w, ds_proced_w, ie_proc_autor_w,
					 sysdate, nm_usuario_p, qt_proced_w,
					 nr_seq_auditoria_w, cd_servico_conversao_w);
			end if;
		elsif	(ie_tipo_tabela_w	in('2','3')) then
			open c03;
			loop
			fetch c03 into
				qt_material_w,
				ie_status_mat_w;
			exit when c03%notfound;
			end loop;
			close c03;

			if	(ie_status_mat_w	in ('M','G','C','N','D','K')) then
				ie_mat_autor_w	:= 1;
			elsif	(ie_status_mat_w	in ('P','S','T','L')) then
				ie_mat_autor_w	:= 2;
			end if;
			
			begin
				select	nr_sequencia
				into	nr_seq_material_w
				from	pls_material
				where	cd_material_ops = nvl(cd_servico_conversao_w,cd_servico_w);
			exception
			when others then
				nr_seq_material_w	:= 0;
			end;
			
			if	(cd_servico_w	is not null) then
				if	(nvl(ds_opme_w,'X')	= 'X') then
					ds_material_w	:= substr(nvl(pls_obter_desc_material(nr_seq_material_w),'N�o encontrado'),1,80);
				else
					ds_material_w	:= ds_opme_w;
				end if;

				insert	into ptu_resp_auditoria_servico
					(nr_sequencia, ie_tipo_tabela, cd_servico,
					 ie_origem_servico, ds_servico, ie_autorizado,
					 dt_atualizacao, nm_usuario, qt_autorizado,
					 nr_seq_auditoria, cd_servico_conversao)
				values	(ptu_resp_auditoria_servico_seq.NextVal, ie_tipo_tabela_w, cd_servico_w,
					 null, ds_material_w, ie_mat_autor_w,
					 sysdate, nm_usuario_p, qt_material_w,
					 nr_seq_auditoria_w, cd_servico_conversao_w);
			end if;
		end if;
		end;
	end loop;
	close c01;
end if;

select	count(1)
into	qt_reg_aprov_w
from	ptu_resp_auditoria_servico
where	nr_seq_auditoria	= nr_seq_auditoria_w
and	ie_autorizado		= 2;

select	count(1)
into	qt_reg_neg_w
from	ptu_resp_auditoria_servico
where	nr_seq_auditoria	= nr_seq_auditoria_w
and	ie_autorizado		= 1;

if	(qt_reg_aprov_w	= 0) and (qt_reg_neg_w	> 0) and (ds_mensagem_p	is null) then
	update	ptu_resposta_auditoria
	set	ds_observacao	= 'Pedido negado'
	where	nr_sequencia	= nr_seq_auditoria_w;
elsif	(qt_reg_aprov_w	> 0) and (qt_reg_neg_w	= 0) and (ds_mensagem_p	is null) then
	update	ptu_resposta_auditoria
	set	ds_observacao	= 'Pedido autorizado'
	where	nr_sequencia	= nr_seq_auditoria_w;
elsif	(qt_reg_aprov_w	> 0) and (qt_reg_neg_w	> 0) and (ds_mensagem_p	is null) then
	update	ptu_resposta_auditoria
	set	ds_observacao	= 'Pedido parcialmente autorizado'
	where	nr_sequencia	= nr_seq_auditoria_w;

end if;

begin
	select	ie_estagio
	into	ie_estagio_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_p;
exception
when others then
	ie_estagio_w	:= 0;
end;

if	(nvl(ie_estagio_w,0)	in (2,6)) then
	pls_executar_req_interc_aprov(nr_seq_requisicao_p, cd_estabelecimento_p, nm_usuario_p);
end if;

ptu_consistir_transacao('00304',nvl(nr_seq_guia_p,nr_seq_guia_w),nvl(nr_seq_requisicao_p,nr_seq_requisicao_w),nr_seq_complemento_p,cd_estabelecimento_p,nm_usuario_p);

<<final>>
qt_reg_auditoria_w	:= 0;

commit;

end ptu_gerar_resposta_auditoria;
/