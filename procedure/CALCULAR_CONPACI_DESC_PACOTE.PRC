create or replace
procedure Calcular_ConPaci_Desc_Pacote(nr_sequencia_p		number,
					ie_acao_p			varchar2,
					nm_usuario_p		varchar2) is

nr_interno_conta_w	number(10);

cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_seq_pacote_w			number(10);
vl_itens_pacote_w		number(15,2);
vl_pacote_w			number(15,2);

vl_desc_procedimento_w		number(15,2);

nr_seq_w			number(10);

vl_desconto_w			number(15,2);

cursor c01 is 
	select cd_convenio,
		 cd_categoria,
		 nr_seq_proc_pacote,
		 sum(vl_item)
	from conta_paciente_v
	where nr_interno_conta = nr_interno_conta_w
	  and nr_seq_proc_pacote is not null
	  and nr_sequencia <> nr_seq_proc_pacote
	group by	cd_convenio,
			cd_categoria,
			nr_seq_proc_pacote;

BEGIN

select	nr_interno_conta
into	nr_interno_conta_w
from	conta_paciente_desconto
where nr_sequencia = nr_sequencia_p;

OPEN C01;
LOOP
	FETCH C01 into	cd_convenio_w,
				cd_categoria_w,
				nr_seq_pacote_w,
				vl_itens_pacote_w;
	exit	when C01%notfound;
		begin

		select vl_procedimento
		into vl_pacote_w
		from procedimento_paciente
		where nr_sequencia = nr_seq_pacote_w;

		if	(vl_pacote_w <> vl_itens_pacote_w) then
			begin
			update procedimento_paciente
			set vl_procedimento = vl_itens_pacote_w,
			    vl_custo_operacional = vl_itens_pacote_w
			where nr_sequencia = nr_seq_pacote_w;
			end;

			vl_desc_procedimento_w := vl_pacote_w - vl_itens_pacote_w;
			if 	(vl_pacote_w < vl_itens_pacote_w) then
				vl_desc_procedimento_w := vl_itens_pacote_w - vl_pacote_w;
			end if;

			if	(ie_acao_p	= 'I') then
				begin
				select nvl(max(nr_sequencia), 0) + 1
				into	 nr_seq_w
				from	 proc_paciente_valor
				where	 nr_seq_procedimento	= nr_seq_pacote_w;

				insert into proc_paciente_valor
					(nr_seq_procedimento,
					nr_sequencia,
					ie_tipo_valor,
					dt_atualizacao,
					nm_usuario,
					vl_procedimento,
					vl_medico,
					vl_anestesista,
					vl_materiais,
					vl_custo_operacional,
					vl_auxiliares,
					cd_convenio,
					cd_categoria,
					pr_valor)
				values
					(nr_seq_pacote_w,
					nr_seq_w,
					3,
					sysdate,
					nm_usuario_p,
					vl_desc_procedimento_w,
					0,
					0,
					0,
					vl_desc_procedimento_w,
					0,
					cd_convenio_w,
					cd_categoria_w,
					0);
				exception
					when others then
						--Erro Incluir Desconto Proc' || chr(13) || chr(10) || SQLERRM);
						Wheb_mensagem_pck.exibir_mensagem_abort(261629, 'ERRO='|| SQLERRM);
				end;
			else
				select nvl(sum(vl_procedimento),0)
				into vl_desconto_w
				from proc_paciente_valor
				where nr_seq_procedimento = nr_seq_pacote_w
				  and ie_tipo_valor = 3;

				update procedimento_paciente
				set	 vl_procedimento		= (vl_procedimento + vl_desconto_w),
					 vl_custo_operacional	= (vl_procedimento + vl_desconto_w)
				where	 nr_sequencia		= nr_seq_pacote_w;

				/* excluir proc_paciente_valor */
				delete from proc_paciente_valor
				where	 nr_seq_procedimento 	= nr_seq_pacote_w
				and	 ie_tipo_valor		= 3;
				
			end if;
		end if;
		end;
end loop;
CLOSE C01;

END Calcular_ConPaci_Desc_Pacote;
/