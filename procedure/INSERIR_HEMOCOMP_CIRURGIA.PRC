create or replace
procedure inserir_hemocomp_cirurgia(	nr_seq_proc_interno_p	number,
					nr_prescricao_p		number,
					nr_seq_solic_sangue_p	number,
					cd_perfil_p		number,
					nm_usuario_p		Varchar2) is 

ie_tipo_w			number(10,0);
nr_sequencia_w			number(10,0);
nr_seq_proced_w			number(10,0);
dt_prescricao_w			date;
dt_prev_execucao_w		date;
dt_programada_w			date;
dt_inicio_prescr_w		date;
qt_procedimento_w		number(15);
qt_procedimento_ww		number(15);
qt_vol_hemocomp_w		number(15);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_intervalo_w			varchar2(7);
dt_primeiro_horario_w		date;
nr_horas_validade_w		number(5,0);
ds_horarios_1_w			varchar2(255);
ds_horarios_2_w			varchar2(255);
nr_intervalo_w			number(15,0);
nr_atendimento_w		number(10,0);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
cd_estabelecimento_w		number(4,0);
ie_tipo_convenio_w		number(2,0);
ie_tipo_atendimento_w 		number(3,0);
cd_setor_atendimento_w		number(5,0);
cd_setor_prescricao_w		number(5,0);
nr_seq_proc_interno_w		number(10,0);
ds_erro_w			varchar2(510);
ie_prescr_proc_sem_lib_w	varchar2(30);
ie_via_aplicacao_w		varchar2(5);
ds_erro_ww			varchar(255);
ie_consitir_novo_hemoc_W	varchar2(1);
ie_irradiado_w			varchar2(1);

cursor c01 is
select	b.nr_sequencia,
	b.ie_irradiado,
	a.qt_derivado
from	procedimento_derivado a,
	san_derivado b
where	a.nr_seq_derivado	= b.nr_sequencia
and	a.nr_seq_proc_interno	= nr_seq_proc_interno_p
and	b.ie_situacao = 'A'
and	b.ie_mostra_prescricao = 'S'
and	b.ie_tipo_derivado	in (	select	ie_grupo_hemocom
					from	prescr_sol_bs_indicacao
					where	nr_seq_solic_bs = nr_seq_solic_sangue_p);

begin
  
select	dt_inicio_prescr,
	dt_primeiro_horario,
	nr_horas_validade,
	nr_atendimento,
	cd_estabelecimento,
	nvl(cd_setor_atendimento,0),
	obter_convenio_atendimento(nr_atendimento_w),
	obter_tipo_convenio(obter_convenio_atendimento(nr_atendimento_w)),
	obter_tipo_atendimento(nr_atendimento_w),
	obter_categoria_atendimento(nr_atendimento_w)
into 	dt_inicio_prescr_w,
	dt_primeiro_horario_w,
	nr_horas_validade_w,
	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_setor_prescricao_w,
	cd_convenio_w,
	ie_tipo_convenio_w,
	ie_tipo_atendimento_w,
	cd_categoria_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

Obter_Param_Usuario(924,530,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_prescr_proc_sem_lib_w);   
Obter_Param_Usuario(924,574,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_consitir_novo_hemoc_W);

select	ie_tipo,
	dt_programada
into	ie_tipo_w,
	dt_programada_w
from	prescr_solic_bco_sangue
where	nr_sequencia	=	nr_seq_solic_sangue_p;

if	(ie_tipo_w = 4) then
	dt_prev_execucao_w	:= sysdate;
elsif	(dt_programada_w is null) then
	dt_prev_execucao_w	:= Obter_data_prev_exec(dt_inicio_prescr_w,dt_inicio_prescr_w,0, nr_prescricao_p, 'A');
else
	dt_prev_execucao_w	:= dt_programada_w;
end if;

select	max(cd_intervalo)
into	cd_intervalo_w
from 	intervalo_prescricao
where 	ie_situacao = 'A'
and 	ie_prescricao_dieta = 'B';

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ie_irradiado_w,
	qt_procedimento_w;
exit when C01%notfound;
	begin

	qt_procedimento_ww := 1;
	atualizar_volume_hemocomp(nr_sequencia_w,qt_vol_hemocomp_w,qt_procedimento_ww,'BV');
	atualizar_volume_hemocomp(nr_sequencia_w,qt_vol_hemocomp_w,qt_procedimento_ww,'V');
	atualizar_volume_hemocomp(nr_sequencia_w,qt_vol_hemocomp_w,qt_procedimento_ww,'B');

	Calcular_Horario_Prescricao(	nr_prescricao_p,cd_intervalo_w,dt_primeiro_horario_w,dt_prev_execucao_w,nr_horas_validade_w,0,0,0,nr_intervalo_w,
					ds_horarios_1_w,ds_horarios_2_w,'N', null);
					
	Obter_Proced_sangue(	0,nr_sequencia_w,cd_estabelecimento_w,ie_tipo_atendimento_w,ie_tipo_convenio_w,cd_convenio_w, cd_categoria_w, cd_setor_atendimento_w,
				cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w);
				
	select	max(cd_procedimento),
		max(ie_origem_proced),
		max(nr_sequencia)
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;	

	select	max(ie_via_aplicacao)
	into	ie_via_aplicacao_w
	from	san_derivado
	where	nr_sequencia = nr_sequencia_w
	and	obter_se_via_hemoc_setor(ie_via_aplicacao, cd_setor_prescricao_w, nr_sequencia) = 'S';

	if 	(nvl(ie_consitir_novo_hemoc_W,'N') <> 'S') then		
	
		Consiste_solic_hemocomponente(	nr_seq_solic_sangue_p,nr_sequencia_w,cd_estabelecimento_w, nm_usuario_p, ds_erro_w);
		
		if	(ds_erro_w is not null) then
			-- #@DS_ERRO#@
			Wheb_mensagem_pck.exibir_mensagem_abort( 193870 , 'DS_ERRO='|| ds_erro_w);
		end if;
	end if;
				
	select	nvl(max(nr_sequencia), 0) + 1
	into	nr_seq_proced_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p;

	insert into prescr_procedimento(
		nr_prescricao,
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		qt_procedimento,
		ie_urgencia,
		ie_suspenso,
		dt_prev_execucao,
		ie_status_atend,
		dt_atualizacao,
		nm_usuario,
		ie_origem_inf,
		nr_seq_interno,
		ie_avisar_result,
		cd_motivo_baixa,
		nr_seq_solic_sangue,
		ds_horarios,
		nr_seq_derivado,
		qt_vol_hemocomp,
		cd_intervalo,
		cd_setor_atendimento,
		ie_util_hemocomponente,
		ie_unid_med_hemo,
		ie_status,
		dt_status,
		ie_via_aplicacao,
		ie_irradiado,
		ie_lavado,
		ie_aliquotado,
		ie_acm,
		ie_se_necessario)
	values	(nr_prescricao_p,
		nr_seq_proced_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		qt_procedimento_w, 
		'N', 
		'N',
		dt_prev_execucao_w,
		5, 
		sysdate, 
		nm_usuario_p,
		'1', 
		prescr_procedimento_seq.NextVal,
		'N',
		0,
		nr_seq_solic_sangue_p,
		ds_horarios_1_w || ds_horarios_2_w,
		nr_sequencia_w,
		qt_vol_hemocomp_w,
		cd_intervalo_w,
		cd_setor_atendimento_w,
		'C',
		'gpm',
		'P',
		sysdate,
		ie_via_aplicacao_w,
		ie_irradiado_w,
		'N',
		'N',
		'N',
		'N');
		
	if (ie_prescr_proc_sem_lib_w = 'S') then
		Gerar_prescr_proc_sem_dt_lib(nr_prescricao_p,nr_seq_proced_w,cd_perfil_p,'N',nm_usuario_p);
	end if;
	
	if 	(nvl(ie_consitir_novo_hemoc_w,'N') = 'S') then			
		Consistir_Prescr_Procedimento(nr_prescricao_p,nr_seq_proced_w,nm_usuario_p,cd_perfil_p,ds_erro_ww);
	end if;
	
	end;
end loop;
close C01;

commit;

end inserir_hemocomp_cirurgia;
/