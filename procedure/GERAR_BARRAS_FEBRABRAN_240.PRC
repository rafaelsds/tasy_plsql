create or replace
procedure GERAR_BARRAS_FEBRABRAN_240
		(nr_seq_banco_escrit_p	in	number,
		nm_usuario_p		in	varchar2,
		nr_seq_lote_p		in	number) is

cd_banco_w		varchar2(3);
cd_barras_w		varchar2(255);
cd_moeda_w		varchar2(1);
ds_campo_livre_w	varchar2(25);
ds_fator_venc_w		varchar2(4);
ie_digito_w		varchar2(1);
nr_seq_barras_w		number(10);
vl_transacao_w		varchar2(10);
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(15);
cd_cgc_w		varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
cd_agencia_bancaria_w	varchar2(5);
nr_conta_w		varchar2(13);
cd_carteira_w		varchar2(3);
nr_nosso_numero_w	varchar2(12);
ie_forma_importacao_w	varchar2(1);
cd_estabelecimento_w	number(4);
nr_seq_banco_escrit_w	number(10);
nm_pessoa_externo_w	varchar2(30);
nr_pagamento_w		number(20);
dt_emissao_w		date;
dt_limite_desconto_w	date;
ds_limite_desconto_w	varchar2(8);
nr_titulo_w		number(10);
nr_seq_escrit_barras_w	number(10);
cd_banco_lote_w		number(3);
ie_alt_tit_bloq_barras_w	varchar2(1);
ie_vincular_tit_bloq_barras_w	varchar2(1);
nr_documento_w			banco_escrit_barras.nr_documento%type;
qt_barras_lote_w		number(10);

cursor c01 is
/* Registro de transacao */
select	substr(a.ds_conteudo,18,3) cd_banco,
	substr(a.ds_conteudo,62,1) ie_tipo_inscricao,
	substr(a.ds_conteudo,63,15) nr_inscricao,
	substr(a.ds_conteudo,21,1) cd_moeda,
	substr(a.ds_conteudo,22,1) ie_digito,
	substr(a.ds_conteudo,23,4) ds_fator_venc,
	substr(a.ds_conteudo,27,10) vl_transacao,
	substr(a.ds_conteudo,37,25) ds_campo_livre,
	somente_numero(substr(a.ds_conteudo,148,15)) nr_pagamento,
  somente_numero(substr(a.ds_conteudo,148,15)) nr_documento,
	to_date(substr(a.ds_conteudo,182,8),'dd/mm/yyyy') dt_emissao,
	substr(a.ds_conteudo,206,8) ds_limite_desconto,
	a.nr_sequencia
from	w_barras a
where	substr(a.ds_conteudo,14,1)	= 'G'
and	substr(a.ds_conteudo,8,1)	= '3'
order by	a.nr_sequencia;

begin

select	max(a.cd_estabelecimento),
	max(a.cd_banco)
into	cd_estabelecimento_w,
	cd_banco_lote_w
from	banco_escritural a
where	a.nr_sequencia	= nr_seq_banco_escrit_p;

obter_param_usuario(857,32,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_forma_importacao_w);
obter_param_usuario(857, 66, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_vincular_tit_bloq_barras_w); --Vincular o codigo de barras ao titulo apos importacao
obter_param_usuario(857, 60, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_alt_tit_bloq_barras_w); --Alterar o tipo do titulo para "Bloqueto" apos a leitura do mesmo por codigo de barras

if	(ie_forma_importacao_w = '2') then

	select	max(b.cd_banco)
	into	cd_banco_lote_w
	from	banco_estabelecimento b,
		banco_escrit_lote a
	where	a.nr_seq_conta_banco	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_lote_p;

	nr_seq_banco_escrit_w	:= null;
else
	nr_seq_banco_escrit_w	:= nr_seq_banco_escrit_p;
end if;

open	c01;
loop
fetch	c01 into
	cd_banco_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	cd_moeda_w,
	ie_digito_w,
	ds_fator_venc_w,
	vl_transacao_w,
	ds_campo_livre_w,
	nr_pagamento_w,
  nr_documento_w,
	dt_emissao_w,
	ds_limite_desconto_w,
	nr_seq_barras_w;
exit	when c01%notfound;

	nr_inscricao_w := replace(nr_inscricao_w,' ','');
	if (length(nr_inscricao_w) = 15 and substr(nr_inscricao_w,1,1) = '0') then
		nr_inscricao_w := substr(nr_inscricao_w,2,14);
	end if;

	cd_barras_w	:=	to_char(somente_numero(cd_banco_w),'FM000') ||
				to_char(somente_numero(cd_moeda_w),'FM0') ||
				to_char(somente_numero(ie_digito_w),'FM0') ||
				to_char(somente_numero(ds_fator_venc_w),'FM0000') ||
				to_char(somente_numero(vl_transacao_w),'FM0000000000') ||
				to_char(somente_numero(ds_campo_livre_w),'FM0000000000000000000000000');

	if	(ie_tipo_inscricao_w = '1') then	/* CPF */

		select	max(a.cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica a
		where	somente_numero(a.nr_cpf)	= somente_numero(nr_inscricao_w);

	elsif	(ie_tipo_inscricao_w = '2') then	/* CNPJ */

		select	max(a.cd_cgc)
		into	cd_cgc_w
		from	pessoa_juridica a
		where	somente_numero(a.cd_cgc)	= somente_numero(nr_inscricao_w)
		and		ie_situacao		= 'A';

	end if;

	if	(cd_barras_w is not null) then
	
		select 	count(1) 
		into	qt_barras_lote_w
		from	banco_escrit_barras
		where	cd_barras = cd_barras_w
		and		decode(ie_forma_importacao_w,'2',0,nr_seq_banco_escrit) = decode(ie_forma_importacao_w,'2',0,nr_seq_banco_escrit_w);

		if (qt_barras_lote_w = 0) then	

			if	(ds_limite_desconto_w <> '00000000') then
				dt_limite_desconto_w	:= to_date(ds_limite_desconto_w,'dd/mm/yyyy');
			else
				dt_limite_desconto_w	:= null;
			end if;

			obter_titulo_regra_barras(cd_banco_lote_w,nr_documento_w,cd_cgc_w,cd_barras_w,nr_titulo_w,'N',null);

			select	banco_escrit_barras_seq.nextval
			into	nr_seq_escrit_barras_w
			from	dual;

			insert	into banco_escrit_barras
				(cd_barras,
				dt_atualizacao,
				nm_usuario,
				nr_seq_banco_escrit,
				nr_sequencia,
				cd_cgc,
				cd_pessoa_fisica,
				nr_seq_lote,
				cd_pessoa_externo,
				nm_pessoa_externo,
				nr_pagamento,
		  nr_documento,
				dt_emissao,
				cd_lancamento,
				dt_limite_desconto,
				nr_titulo)
			values	(cd_barras_w,
				sysdate,
				nm_usuario_p,
				nr_seq_banco_escrit_w,
				nr_seq_escrit_barras_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				nr_seq_lote_p,
				nr_inscricao_w,
				nm_pessoa_externo_w,
				nr_pagamento_w,
		  nr_documento_w,
				dt_emissao_w,
				null,
				dt_limite_desconto_w,
				nr_titulo_w);
		
			if (nvl(nr_titulo_w,0) <> 0) then
			--Vinculo conforme objeto VINCULAR_BARRAS_TIT_PAGAR. O objeto nao sera executado aqui, pois as validacoes podem impactar na importacao.
				if (nvl(ie_vincular_tit_bloq_barras_w, 'N') = 'S') then 
					update	titulo_pagar
					set	nr_bloqueto	= cd_barras_w,
						nm_usuario	= nm_usuario_p,
						ie_bloqueto     = 'S'   
					where	nr_titulo	= nr_titulo_w;			
				end if;			
				
				if (nvl(ie_alt_tit_bloq_barras_w, 'N') = 'S') then   			
					update	titulo_pagar
					set	ie_tipo_titulo	= '1', 
						nm_usuario	= nm_usuario_p
					where	nr_titulo	= nr_titulo_w;
				end if;
			end if;
		end if;
	end if;

end	loop;
close	c01;

delete	from w_barras;

commit;

end GERAR_BARRAS_FEBRABRAN_240;
/
