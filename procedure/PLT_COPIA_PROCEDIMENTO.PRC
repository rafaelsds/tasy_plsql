create or replace
PROCEDURE PLT_copia_procedimento (nr_prescricao_p		NUMBER,
				 nr_seq_regra_p			NUMBER,
				 dt_prescricao_p		DATE,
				 dt_procedimento_p		DATE,
				 ds_ind_clinica_p		VARCHAR2,
				 cd_perfil_p			NUMBER,
				 cd_prescritor_p		VARCHAR2,
				 ie_procedimento_p		VARCHAR2,
				 ds_lista_p			VARCHAR2,
				 ie_modificar_p			VARCHAR2,
				 nm_usuario_p			VARCHAR2,
				 cd_estabelecimento_p		number,
				 ie_estende_inc_p		varchar2,
				 ie_copia_item_agora_p	varchar2) IS

cd_classif_setor_w			Varchar2(2);				 
ie_prescr_agora_pa_w		varchar2(1);				 
dt_horario_w				date;				 
ie_operacao_w				Varchar2(1);
qt_regra_1h_w				number(10,0);
ie_regra_prim_hor_ant_w		varchar2(15);
ie_regra_horarios_ant_w		varchar2(15);
nr_seq_material_w			NUMBER(10);
cd_especialidade_w			VARCHAR2(50);
nr_sequencia_w				NUMBER(6,0);
nr_seq_proc_ant_w			NUMBER(15,0);
nr_agrupamento_w			NUMBER(6,0);
ie_executar_leito_w			VARCHAR2(1);
ie_amostra_w				VARCHAR2(1);
cd_procedimento_w			NUMBER(15,0);
cd_procedimento_ww			NUMBER(15,0);
qt_procedimento_w			NUMBER(8,3);
ds_horarios_w				VARCHAR2(2000);
ds_horarios_aux_w			VARCHAR2(2000);
ds_observacao_w				VARCHAR2(2000);
ds_observacao_enf_w			VARCHAR2(2000);
cd_motivo_baixa_w			NUMBER(3,0);
nr_atendimento_w			NUMBER(15,0);
dt_baixa_w					DATE;
cd_procedimento_aih_w		VARCHAR2(15);
cd_intervalo_param_w		VARCHAR2(7);
cd_intervalo_proc_agora_w	VARCHAR2(7);
ie_origem_proced_w			NUMBER(10,0);
cd_intervalo_w				VARCHAR2(7);
ie_urgencia_w				VARCHAR2(1);
ie_regra_medic_w			VARCHAR2(15);
cd_setor_atendimento_w		NUMBER(5,0);
dt_emissao_setor_atend_w	DATE;
ds_dado_clinico_w			VARCHAR2(2000);
dt_prev_exec_w				DATE;
dt_prev_exec_ww				date;
dt_prev_exec2_ww			date;
ie_ctrl_glic_w				VARCHAR(3);
ie_suspenso_w				VARCHAR2(1);
cd_material_exame_w			VARCHAR2(20);
nr_seq_exame_w				NUMBER(10);
ds_material_especial_w		VARCHAR2(255);
ie_status_atend_w			NUMBER(2);
ie_origem_inf_w				VARCHAR2(1);
ie_se_necessario_w			VARCHAR2(1);
ie_acm_w					VARCHAR2(1);
nr_ocorrencia_w				NUMBER(15,4);
nr_seq_proc_interno_w		NUMBER(10,0);
qt_peca_ap_w				NUMBER(3);
ds_qualidade_peca_ap_w		VARCHAR2(2000);
ds_diag_provavel_ap_w		VARCHAR2(255);
ds_exame_anterior_ap_w		VARCHAR2(255);
nr_seq_derivado_w			NUMBER(10);
nr_seq_exame_sangue_w		NUMBER(10);
nr_seq_solic_sangue_w		NUMBER(10);
cd_unid_med_sangue_w		VARCHAR2(5);
cd_pessoa_coleta_w			VARCHAR2(10);
dt_coleta_w					DATE;
dt_prev_execucao_ww			DATE;
ie_avisar_result_w			VARCHAR2(1);
qt_vol_hemocomp_w			NUMBER(15);
nr_seq_prot_glic_w			NUMBER(10);
ie_respiracao_w				VARCHAR2(15);
cd_mod_vent_w				VARCHAR2(15);
ie_disp_resp_esp_w			VARCHAR2(15);
qt_fluxo_oxigenio_w			NUMBER(15,4);
ds_rotina_w					VARCHAR2(80);
ie_autorizacao_w			VARCHAR2(3);
ie_lado_w					VARCHAR2(1);
nr_seq_topografia_w			number(10,0);
nr_seq_interno_w			NUMBER(10,0);

ie_copiar_w					VARCHAR2(10);
ie_regra_geral_w			VARCHAR2(10);
nr_seq_regra_w				NUMBER(15);
nr_seq_proc_interno_aux_w	NUMBER(15);
cd_intervalo_aux_w			VARCHAR2(50);
ie_proced_agora_w			VARCHAR2(50);
ie_regra_prim_hor_proc_w	VARCHAR2(50);

cd_intervalo_proc_w			VARCHAR2(50);
dt_prev_execucao_w			DATE;
ie_situacao_w				VARCHAR2(50);
nr_prescricoes_w			VARCHAR2(255);
dt_inicio_prescr_ww			DATE;
dt_inicio_prescr_w			DATE;
dt_validade_prescr_w		DATE;
nr_horas_validade_w			NUMBER(15,0);
qt_hora_intervalo_w			NUMBER(2,0);
qt_min_intervalo_w			NUMBER(5,0);
nr_seq_proc_int_w			NUMBER(15,0);
ds_horarios_ww				VARCHAR2(2000);
ds_horarios2_w				VARCHAR2(2000);
ds_erro_w					VARCHAR2(2000);
cd_setor_prescr_w			NUMBER(5);
ie_exame_w					VARCHAR2(1);
nr_seq_bco_w				NUMBER(10);
	
ds_lista_w					VARCHAR2(4000);
tam_lista_w					NUMBER(10,0);
tam_prescricao_w			NUMBER(10,0);
ie_pos_virgula_w			NUMBER(3,0);
ie_pos_espaco_w				NUMBER(3,0);
nr_prescr_lista_w			NUMBER(14);
nr_prescr_ant_w				NUMBER(14);
nr_seq_lista_w				NUMBER(10);
ds_prescricao_w				VARCHAR2(255);
qt_copia_w					NUMBER(10);
nr_agrup_acum_w				NUMBER(10,0);
nr_prescr_estendido_w		number(20);
ds_procedimento_w			procedimento.ds_procedimento%TYPE;
dt_suspensao_progr_w		date;
nr_prescricao_original_w	number(14);
nr_seq_anterior_w			number(10);
dt_prev_horario_w			Varchar2(20);
ie_regra_horarios_w			varchar2(15);
ie_reordenar_ant_w			varchar2(15);
ie_reordenar_w				varchar2(15);
ds_horarios_reordenados_w	Varchar2(2000);
qt_inconsistencia_w			number(10);
ie_mesmo_zerado_w			varchar2(1);
ie_setor_paciente_w			varchar2(07);
ie_minuto_w					varchar2(1);
ie_tipo_proced_w			varchar2(5) := '';
ie_regra_hor_especial_w		varchar2(1);
ds_justificativa_w	 		varchar2(4000);
ie_copiar_justificativa_w 	varchar2(1) := 'N';

ie_solicitado_w 			varchar2(1);
cont_peca_w					number(10);
ie_relancar_assoc_proc_w	varchar2(1);
nr_seq_rotina_w				prescr_procedimento.nr_seq_rotina%type;

ie_log_dt_prev_execucao_w 	varchar2(1);
ds_log_dt_prev_execucao_w	varchar2(4000);

cursor c02 is
	select	nvl(a.ie_copiar,'N'),
		a.ie_regra_geral,
		a.nr_sequencia,
		nvl(nr_seq_proc_interno,0) nr_seq_proc_interno,
		nvl(cd_intervalo,'AAAAAAAAAA') cd_intervalo,
		a.ie_proced_agora,
		a.ie_dose_especial,
		nvl(a.ie_copiar_justificativa, 'N') ie_copiar_justificativa
	from	rep_regra_copia_crit a
	where	a.ie_tipo_item	= 'PRO'
	and	((a.nr_seq_regra	= nr_seq_regra_p) or
		 (nvl(nr_seq_regra_p,0) = 0))
	and	nvl(a.nr_seq_proc_interno,nvl(nr_seq_proc_interno_w,0))	= nvl(nr_seq_proc_interno_w,0)
	and	nvl(a.nr_seq_exame,nvl(nr_seq_exame_w,0)) = nvl(nr_seq_exame_w,0)
	and	((a.cd_intervalo = cd_intervalo_w) or  (a.cd_intervalo is null))	 
	order by nr_seq_proc_interno,
		cd_intervalo,
		ie_agora;

cursor c03 is
	select	decode(ie_regra_geral_w, 'H', null, ie_regra_prim_hor_proc),
		decode(ie_regra_geral_w, 'H', ie_regra_horarios),
		decode(ie_regra_geral_w, 'H',ie_reordenar)
	from	rep_regra_copia_hor
	where	nr_seq_regra_copia = nr_seq_regra_w
	order by nvl(nr_seq_apres,999) desc;

cursor c04 is
	select	dt_horario
	from	prescr_proc_hor
	where	nr_prescricao	= nr_prescr_lista_w
	and	nr_seq_procedimento = nr_seq_lista_w
	and	nvl(ie_situacao,'A')	= 'A'
	and	((dt_suspensao is null) or (ie_modificar_p = 'S'))
	and ((nr_seq_hor_glic is null) or (ie_regra_hor_especial_w = 'S'))
	order by dt_horario;

	Cursor C013 is
	select	a.nr_seq_proc_interno,
			a.nr_sequencia,
			a.cd_procedimento
	from	Prescr_Procedimento a
	where	nr_prescricao	= nr_prescricao_p
	and		ie_origem_inf	<> 'L'
	and		nr_seq_origem	is null
	and		nvl(OBTER_DADOS_EXAME_LAB_REP(cd_perfil_p, cd_especialidade_w, cd_setor_prescr_w, nr_seq_exame, cd_estabelecimento_p, 'C'), 'S') = 'S'
	and		consiste_copia_prescr_rotina(a.nr_seq_proc_interno, a.cd_procedimento, a.ie_origem_proced, a.nr_seq_exame, 'S', 'S') = 'S'; 

procedure add_log_dt_prev_execucao(nr_linha pls_integer, nr_seq_proc prescr_procedimento.nr_sequencia%type, ds_log_p varchar2) is
begin
	if (ie_log_dt_prev_execucao_w = 'S') then	
		ds_log_dt_prev_execucao_w := substr(ds_log_dt_prev_execucao_w||chr(10)||$$PLSQL_UNIT||':'||nr_linha||' NR_SEQ_PROCEDIMENTO('||nr_seq_proc||') '||ds_log_p, 1, 4000);
	end if;
end;

BEGIN

ds_lista_w	:= ds_lista_p;

ie_log_dt_prev_execucao_w := obter_se_info_rastre_prescr('D', wheb_usuario_pck.get_nm_usuario, obter_perfil_ativo, wheb_usuario_pck.get_cd_estabelecimento);

SELECT	NVL(MAX(nr_agrupamento),0) + 1
INTO	nr_agrup_acum_w
FROM	prescr_procedimento
WHERE	nr_prescricao = nr_prescricao_p;

SELECT	MAX(nr_atendimento),
	max(dt_inicio_prescr),
	max(dt_validade_prescr)
INTO	nr_atendimento_w,
	dt_inicio_prescr_w,
	dt_validade_prescr_w
FROM	prescr_medica
WHERE	nr_prescricao	= nr_prescricao_p
and		rownum = 1;

cd_setor_prescr_w := NVL(obter_unidade_atendimento(nr_atendimento_w,'IA','CS'), obter_unidade_atendimento(nr_atendimento_w,'A','CS'));
cd_especialidade_w := obter_especialidade_medico(cd_prescritor_p, 'C');

SELECT	MAX(nr_sequencia)
INTO	nr_seq_bco_w
FROM	prescr_solic_bco_sangue
WHERE	nr_prescricao = nr_prescricao_p;

SELECT	NVL(MAX(nr_sequencia),0) + 1
INTO	nr_seq_material_w
FROM	prescr_material
WHERE	nr_prescricao = nr_prescricao_p;

ie_exame_w := ie_procedimento_p;

Obter_Param_Usuario(924,35,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,cd_intervalo_param_w);
Obter_Param_Usuario(924,259,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_regra_medic_w);
Obter_Param_Usuario(924,311,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_prescr_agora_pa_w);
Obter_Param_Usuario(924,603,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_relancar_assoc_proc_w);

WHILE ds_lista_w IS NOT NULL LOOP
	BEGIN
	tam_lista_w		:= LENGTH(ds_lista_w);
	ie_pos_virgula_w	:= INSTR(ds_lista_w,',');
	ds_prescricao_w		:= SUBSTR(ds_lista_w,1,(ie_pos_virgula_w - 1));
	ie_pos_espaco_w		:= INSTR(ds_prescricao_w,' ');
	tam_prescricao_w	:= LENGTH(ds_prescricao_w);
	nr_prescr_lista_w	:= TO_NUMBER(SUBSTR(ds_prescricao_w,1,(ie_pos_espaco_w - 1)));
	
	nr_seq_lista_w		:= TO_NUMBER(SUBSTR(ds_prescricao_w,(ie_pos_espaco_w + 1), tam_prescricao_w));

	ie_tipo_proced_w := obter_tipo_proc(nr_seq_lista_w,nr_prescr_lista_w);
	PLT_consiste_extensao_item(dt_inicio_prescr_w, dt_validade_prescr_w, nr_prescr_lista_w, nr_seq_lista_w, ie_tipo_proced_w, nr_seq_regra_p, nm_usuario_p, cd_perfil_p, cd_estabelecimento_p);
	
	select	count(nr_sequencia)
	into	qt_inconsistencia_w
	from	w_copia_plano
	where	nr_prescricao	= nr_prescr_lista_w
	and	nr_seq_item	= nr_seq_lista_w
	and	ie_tipo_item	in ('P','L','CCG','CIG','I','SADT')
	and	nm_usuario	= nm_usuario_p
	and	((ie_permite	= 'N') or
		 (ie_estende_inc_p = 'N'))
	and	rownum = 1;
	
	SELECT	COUNT(*)
	INTO	qt_copia_w
	FROM	Prescr_Procedimento a
	WHERE	nr_prescricao	= nr_prescr_lista_w
	AND	a.nr_sequencia  = nr_seq_lista_w
	AND	NVL(obter_dados_exame_lab_rep(cd_perfil_p, cd_especialidade_w, cd_setor_prescr_w, nr_seq_exame, cd_estabelecimento_p, 'C'), 'S') = 'S'
	AND	consiste_copia_prescr_rotina(a.nr_seq_proc_interno, a.cd_procedimento, a.ie_origem_proced, a.nr_seq_exame, 'S', 'S') = 'S'
	and	((nvl(ie_copia_item_agora_p,'S') = 'S') or
		 ((a.ie_urgencia = 'N') and
	      (nvl(ie_copia_item_agora_p,'S') = 'N')))
	and	rownum = 1;
	
	
	select	nvl(max(obter_se_exame_solicitado(nr_seq_exame)),'S')
	into    ie_solicitado_w	
	from	prescr_Procedimento a
	where	nr_prescricao	= nr_prescr_lista_w
	and	a.nr_sequencia  = nr_seq_lista_w;
		
	IF	(((qt_copia_w 		> 0) and
		  (qt_inconsistencia_w	= 0)) or
		 (ie_modificar_p = 'S')) and
		(ie_solicitado_w = 'S' )  THEN

		SELECT	nr_sequencia,
			NVL(nr_agrupamento,0) + nr_agrup_acum_w,
			ie_executar_leito,
			'N',
			cd_procedimento,
			qt_procedimento,
			ds_horarios,
			trim(reordenar_horarios(dt_inicio_prescr_w, ds_horarios)),
			ds_observacao,
			ds_observacao_enf,
			0,
			NULL,
			NULL,
			ie_origem_proced,
			cd_intervalo,
			'N',
			cd_setor_atendimento,
			NULL,
			NVL(SUBSTR(ds_ind_clinica_p,1,255),ds_dado_clinico),
			obter_data_prev_exec(dt_prescricao_p,dt_procedimento_p,cd_setor_atendimento, nr_prescricao_p,'A'),
			a.dt_prev_execucao,
			'N',
			cd_material_exame,
			nr_seq_exame,
			ds_material_especial,
			5,
			ie_origem_inf,
			NVL(ie_se_necessario,'N'),
			NVL(ie_acm,'N'),
			nr_ocorrencia,
			nr_seq_proc_interno,
			qt_peca_ap,
			ds_qualidade_peca_ap,
			ds_diag_provavel_ap,
			ds_exame_anterior_ap,
			nr_seq_derivado,
			nr_seq_exame_sangue,
			DECODE(nr_seq_solic_sangue,NULL,NULL,nr_seq_bco_w),
			cd_unid_med_sangue,
			cd_pessoa_coleta,
			NULL,
			'N',
			qt_vol_hemocomp,
			nr_seq_prot_glic,
			ie_respiracao,
			cd_mod_vent,
			ie_disp_resp_esp,
			qt_fluxo_oxigenio,
			ds_rotina,
			a.qt_hora_intervalo,
			a.qt_min_intervalo,
			'L',
			ie_lado,
			nr_seq_topografia,
			dt_suspensao_progr,
			nvl(nr_prescricao_original, nr_prescricao),
			nvl(nr_seq_anterior, nr_sequencia),
			substr(Obter_Desc_Prescr_Proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,255),
			ds_justificativa,
			nr_seq_rotina
		INTO	nr_seq_proc_ant_w,
			nr_agrupamento_w,
			ie_executar_leito_w,
			ie_amostra_w,
			cd_procedimento_w,
			qt_procedimento_w,
			ds_horarios_w,
			ds_horarios_reordenados_w,
			ds_observacao_w,
			ds_observacao_enf_w,
			cd_motivo_baixa_w,
			dt_baixa_w,
			cd_procedimento_aih_w,
			ie_origem_proced_w,
			cd_intervalo_w,
			ie_urgencia_w,
			cd_setor_atendimento_w,
			dt_emissao_setor_atend_w,
			ds_dado_clinico_w,
			dt_prev_exec_w,
			dt_prev_exec_ww,
			ie_suspenso_w,
			cd_material_exame_w,
			nr_seq_exame_w,
			ds_material_especial_w,
			ie_status_atend_w,
			ie_origem_inf_w,
			ie_se_necessario_w,
			ie_acm_w,
			nr_ocorrencia_w,
			nr_seq_proc_interno_w,
			qt_peca_ap_w,
			ds_qualidade_peca_ap_w,
			ds_diag_provavel_ap_w,
			ds_exame_anterior_ap_w,
			nr_seq_derivado_w,
			nr_seq_exame_sangue_w,
			nr_seq_solic_sangue_w,
			cd_unid_med_sangue_w,
			cd_pessoa_coleta_w,
			dt_coleta_w,
			ie_avisar_result_w,
			qt_vol_hemocomp_w,
			nr_seq_prot_glic_w,
			ie_respiracao_w,
			cd_mod_vent_w,
			ie_disp_resp_esp_w,
			qt_fluxo_oxigenio_w,
			ds_rotina_w,
			qt_hora_intervalo_w,
			qt_min_intervalo_w,
			ie_autorizacao_w,
			ie_lado_w,
			nr_seq_topografia_w,
			dt_suspensao_progr_w,
			nr_prescricao_original_w,
			nr_seq_anterior_w,
			ds_procedimento_w,
			ds_justificativa_w,
			nr_seq_rotina_w
		FROM	Prescr_Procedimento a
		WHERE	nr_prescricao	= nr_prescr_lista_w
		AND	a.nr_sequencia  = nr_seq_lista_w;
				
		open C02;
		loop
		fetch C02 into	
			ie_copiar_w,
			ie_regra_geral_w,
			nr_seq_regra_w,
			nr_seq_proc_interno_aux_w,
			cd_intervalo_aux_w,
			ie_proced_agora_w,
			ie_regra_hor_especial_w,
			ie_copiar_justificativa_w;
		exit when C02%notfound;
			ie_copiar_w		:= ie_copiar_w;
			ie_regra_geral_w	:= ie_regra_geral_w;
			nr_seq_regra_w		:= nr_seq_regra_w;
			ie_proced_agora_w	:= ie_proced_agora_w;
			cd_intervalo_aux_w	:= cd_intervalo_aux_w;
			nr_seq_proc_interno_aux_w	:= nr_seq_proc_interno_aux_w;
			ie_regra_hor_especial_w     := ie_regra_hor_especial_w;
			ie_copiar_justificativa_w 	:= ie_copiar_justificativa_w;
		end loop;
		close C02;
				
		if	(ie_regra_geral_w	<> 'I') then
			open C03;
			loop
			fetch C03 into	
				ie_regra_prim_hor_proc_w,
				ie_regra_horarios_w,
				ie_reordenar_w;
			exit when C03%notfound;
				ie_regra_prim_hor_proc_w	:= ie_regra_prim_hor_proc_w;
				ie_regra_horarios_w		:= ie_regra_horarios_w;
				ie_reordenar_w			:= ie_reordenar_w;		
				
				ie_regra_prim_hor_ant_w	:= ie_regra_prim_hor_proc_w;
				ie_regra_horarios_ant_w	:= ie_regra_horarios_w;
				ie_reordenar_ant_w	:= ie_reordenar_w;			
			end loop;
			close C03;
		end if;			 
		 
		if	(ie_regra_geral_w <> 'I') and
			(nr_atendimento_w > 0) and
			(ie_prescr_agora_pa_w = 'S') then
			Select	cd_classif_setor
			into	cd_classif_setor_w
			from	setor_atendimento
			where	cd_setor_atendimento = (select Obter_Unidade_Atendimento(nr_atendimento_w,'A','CS') from dual);

			if	(cd_classif_setor_w	= '1') then
				dt_prev_horario_w	:= to_char(sysdate + 10/1440,'dd/mm/yyyy hh24:mi:ss');
				dt_prev_exec_w		:= to_date(substr(dt_prev_horario_w,1,15)||'0:00','dd/mm/yyyy hh24:mi:ss');
				add_log_dt_prev_execucao($$PLSQL_LINE, null, 'dt_prev_exec_w:'||to_char(dt_prev_exec_w,'dd/mm/yyyy hh24:mi:ss')||' cd_classif_setor_w:'||cd_classif_setor_w);
			end if;
		end if;					 

		if	(ie_reordenar_w = 'S') then
			ds_horarios_w	:= ds_horarios_reordenados_w;
			Reordenar_Horarios_Prescr(dt_inicio_prescr_w, ds_horarios_w);
		end if;

		IF	(nr_prescr_lista_w <> nr_prescr_ant_w) AND
			(nr_prescr_ant_w IS NOT NULL) THEN
			SELECT	MAX(nr_agrupamento) + 1
			INTO	nr_agrup_acum_w
			FROM	prescr_procedimento
			WHERE	nr_prescricao	= nr_prescricao_p;
		END IF;

		IF	(NVL(nr_seq_proc_interno_w,0) > 0) THEN
			SELECT	NVL(MAX(ie_setor_paciente),'N')
			INTO	ie_setor_paciente_w
			FROM	proc_interno
			WHERE	nr_sequencia	= nr_seq_proc_interno_w
			and		rownum = 1;

			IF	(ie_setor_paciente_w = 'S') THEN
				cd_setor_atendimento_w	:= obter_setor_atendimento(nr_atendimento_w);
			END IF;
		END IF;	
		
		SELECT	(NVL(MAX(nr_sequencia),0) + 1)
		INTO	nr_sequencia_w
		FROM	prescr_procedimento
		WHERE	nr_prescricao = nr_prescricao_p;
		
		IF 	(ie_copiar_justificativa_w = 'N') THEN
			ds_justificativa_w := null;			
		END IF;
		
		SELECT	prescr_procedimento_seq.NEXTVAL
		INTO	nr_seq_interno_w
		FROM	dual;		
		
			INSERT  INTO Prescr_Procedimento (
				nr_prescricao,
				nr_sequencia,
				nr_agrupamento,
				ie_executar_leito,
				ie_amostra,
				cd_procedimento,
				qt_procedimento,
				dt_atualizacao,
				nm_usuario,
				ds_horarios,
				ds_observacao,
				ds_observacao_enf,
				cd_motivo_baixa,
				dt_baixa,
				cd_procedimento_aih,
				ie_origem_proced,
				cd_intervalo,
				ie_urgencia,
				cd_setor_atendimento,
				dt_emissao_setor_atend,
				ds_dado_clinico,
				dt_prev_execucao,
				ie_suspenso,
				cd_material_exame,
				nr_seq_exame,
				ds_material_especial,
				ie_status_atend,
				ie_origem_inf,
				ie_se_necessario,
				ie_acm,
				nr_ocorrencia,
				nr_seq_interno,
				nr_seq_proc_interno,
				qt_peca_ap,
				ds_qualidade_peca_ap,
				ds_diag_provavel_ap,
				ds_exame_anterior_ap,
				nr_seq_derivado,
				nr_seq_exame_sangue,
				nr_seq_solic_sangue,
				cd_unid_med_sangue,
				cd_pessoa_coleta,
				dt_coleta,
				ie_avisar_result,
				qt_vol_hemocomp,
				nr_seq_prot_glic,
				ie_respiracao,
				cd_mod_vent,
				ie_disp_resp_esp,
				qt_fluxo_oxigenio,
				ds_rotina,
				qt_hora_intervalo,
				qt_min_intervalo,
				ie_autorizacao,
				ie_lado,
				nr_seq_topografia,
				dt_suspensao_progr,
				nr_prescricao_original,
				nr_seq_anterior,
				ds_justificativa,
				nr_seq_rotina)
			VALUES	(nr_prescricao_p,
				nr_sequencia_w,
				nr_agrupamento_w,
				ie_executar_leito_w,
				ie_amostra_w,
				cd_procedimento_w,
				qt_procedimento_w,
				dt_prescricao_p,
				nm_usuario_p,
				ds_horarios_w,
				ds_observacao_w,
				ds_observacao_enf_w,
				cd_motivo_baixa_w,
				dt_baixa_w,
				cd_procedimento_aih_w,
				ie_origem_proced_w,
				cd_intervalo_w,
				ie_urgencia_w,
				cd_setor_atendimento_w,
				dt_emissao_setor_atend_w,
				ds_dado_clinico_w,
				dt_prev_exec_w,
				ie_suspenso_w,
				cd_material_exame_w,
				nr_seq_exame_w,
				ds_material_especial_w,
				ie_status_atend_w,
				ie_origem_inf_w,
				ie_se_necessario_w,
				ie_acm_w,
				nr_ocorrencia_w,
				nr_seq_interno_w,
				nr_seq_proc_interno_w,
				qt_peca_ap_w,
				ds_qualidade_peca_ap_w,
				ds_diag_provavel_ap_w,
				ds_exame_anterior_ap_w,
				nr_seq_derivado_w,
				nr_seq_exame_sangue_w,
				nr_seq_solic_sangue_w,
				cd_unid_med_sangue_w,
				cd_pessoa_coleta_w,
				dt_coleta_w,
				ie_avisar_result_w,
				qt_vol_hemocomp_w,
				nr_seq_prot_glic_w,
				ie_respiracao_w,
				cd_mod_vent_w,
				ie_disp_resp_esp_w,
				qt_fluxo_oxigenio_w,
				ds_rotina_w,
				qt_hora_intervalo_w,
				qt_min_intervalo_w,
				ie_autorizacao_w,
				ie_lado_w,
				nr_seq_topografia_w,
				dt_suspensao_progr_w,
				nr_prescricao_original_w,
				nr_seq_anterior_w,
				ds_justificativa_w,
				nr_seq_rotina_w);
				
		if (ie_relancar_assoc_proc_w = 'S') then	
			
			Gerar_med_mat_assoc(nr_prescricao_p, nr_sequencia_w);
			
			if	(nr_seq_prot_glic_w is not null) then
				Gerar_Med_Mat_Assoc_Glic(nr_prescricao_p, nr_sequencia_w, nr_seq_prot_glic_w);
			end if;
		else
			REP_copia_item_assoc_proc(nr_prescricao_p, nr_prescr_lista_w, nr_seq_proc_ant_w, nr_sequencia_w, 'S', cd_perfil_p, nr_seq_material_w, nm_usuario_p,null,ie_modificar_p);
		end if;
		
		copiar_niveis_ccg(nr_prescricao_p, nr_prescr_lista_w, nr_seq_proc_ant_w, nr_sequencia_w, nm_usuario_p);
		
		select	count(*)
		into	cont_peca_w
		from	prescr_procedimento a, prescr_proc_peca b
		where	rownum = 1
		and		a.nr_prescricao		= b.nr_prescricao
		and		a.nr_sequencia		= b.nr_seq_prescr
		and		b.nr_prescricao		= nr_prescricao_original_w
		and		b.nr_seq_prescr		= nr_seq_anterior_w;

		if	(cont_peca_w > 0) then
			--nova Prescrição, nova sequencia do proc,  Prescrição antiga, Sequencia  antiga do Proc, Usuario
			REP_copia_pecas_procedimento(nr_prescricao_p, nr_sequencia_w, nr_prescricao_original_w, nr_seq_anterior_w, nm_usuario_p);
		end if;
		
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

		SELECT	a.nr_sequencia,
			a.cd_intervalo,
			a.dt_prev_execucao,
			a.cd_procedimento,
			a.ie_origem_proced,
			a.ds_horarios,
			NVL(b.ie_situacao,'A'),
			a.nr_seq_exame,
			c.dt_inicio_prescr,
			c.nr_horas_validade,
			a.qt_hora_intervalo,
			a.qt_min_intervalo,
			a.nr_seq_proc_interno
		INTO	nr_sequencia_w,
			cd_intervalo_proc_w,
			dt_prev_execucao_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			ds_horarios_w,
			ie_situacao_w,
			nr_seq_exame_w,
			dt_inicio_prescr_ww,
			nr_horas_validade_w,
			qt_hora_intervalo_w,
			qt_min_intervalo_w,
			nr_seq_proc_int_w
		FROM	intervalo_prescricao b,
			Prescr_procedimento a,
			prescr_medica c
		WHERE	c.nr_prescricao	= nr_prescricao_p
		AND	a.nr_seq_interno = nr_seq_interno_w
		AND	c.nr_prescricao	= a.nr_prescricao
		AND	a.cd_intervalo	= b.cd_intervalo (+);
		
		add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_w:'||to_char(dt_prev_execucao_w, 'dd/mm/yyyy hh24:mi:ss'));

		dt_prev_execucao_ww	:= NULL;
		nr_ocorrencia_w		:= 0;
		
		IF	(ie_situacao_w = 'I') THEN
			cd_intervalo_proc_w	:= cd_intervalo_param_w;
		END IF;			
		
		IF	(nr_seq_exame_w IS NULL) AND
			(ie_regra_medic_w = 'S') THEN
			IF	(somente_numero(nr_prescricoes_w) = 0) THEN
				nr_prescricoes_w	:= TO_CHAR(NR_PRESCRICAO_P)||','||to_char(nr_prescr_lista_w);
			END IF;

			IF	(ie_regra_prim_hor_proc_w = 'I') THEN
				dt_prev_execucao_ww	:= obter_prim_hor_proc_interv(nr_atendimento_w,
											nr_prescricoes_w,
											dt_inicio_prescr_ww,
											cd_procedimento_w,
											ie_origem_proced_w,
											cd_intervalo_proc_w,
											ie_regra_prim_hor_proc_w,
											null);
		
				add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_ww:'||to_char(dt_prev_execucao_ww, 'dd/mm/yyyy hh24:mi:ss')||'|ie_regra_prim_hor_proc_w:'||ie_regra_prim_hor_proc_w);
			END IF;

			
			
			IF	(dt_prev_execucao_ww IS NOT NULL) THEN
				dt_prev_execucao_w	:= dt_prev_execucao_ww;
			elsif	(dt_prev_exec_ww is not null)	and
				(ie_regra_geral_w = 'I') then
				dt_prev_execucao_w := Converte_Char_Data(to_char(dt_inicio_prescr_w,'dd/mm/yyyy'),to_char(dt_prev_exec_ww,'hh24:mi:ss'),null);
			END IF;
			
			add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_w:'||to_char(dt_prev_execucao_w, 'dd/mm/yyyy hh24:mi:ss')||'|dt_prev_execucao_ww:'||to_char(dt_prev_execucao_ww, 'dd/mm/yyyy hh24:mi:ss')
				||'|dt_prev_exec_ww:'||to_char(dt_prev_exec_ww, 'dd/mm/yyyy hh24:mi:ss')||'|ie_regra_geral_w:'||ie_regra_geral_w
				||'|dt_inicio_prescr_w:'||dt_inicio_prescr_w);
			
			if	(ie_regra_geral_w = 'I') and
				(dt_prev_execucao_w is not null) then
				if	(dt_prev_execucao_w between dt_inicio_prescr_w and dt_validade_prescr_w) then
					dt_prev_exec2_ww := dt_prev_execucao_w;
				else					
					dt_prev_exec2_ww := Converte_Char_Data(to_char(dt_validade_prescr_w,'dd/mm/yyyy'),to_char(dt_prev_exec_ww,'hh24:mi:ss'),null);
				end if;
				
				if	(dt_prev_exec2_ww between  dt_inicio_prescr_w and dt_validade_prescr_w) then
					dt_prev_execucao_w	:= dt_prev_exec2_ww;
				else
					dt_prev_execucao_w	:= dt_inicio_prescr_w;
				end if;
				
				add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_w:'||to_char(dt_prev_execucao_w, 'dd/mm/yyyy hh24:mi:ss')||'|dt_prev_exec2_ww:'||to_char(dt_prev_exec2_ww, 'dd/mm/yyyy hh24:mi:ss')
					||'|dt_inicio_prescr_w:'||to_char(dt_inicio_prescr_w, 'dd/mm/yyyy hh24:mi:ss')||'|dt_validade_prescr_w:'||to_char(dt_validade_prescr_w, 'dd/mm/yyyy hh24:mi:ss')||'|ie_regra_geral_w:'||ie_regra_geral_w);
			end if;
			
			if	(ie_acm_w	= 'S') then
				ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807); -- ACM
			elsif	(ie_se_necessario_w	= 'S') then
				ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808); -- SN
			else	
				ds_horarios_ww	:= ds_horarios_w;

				Calcular_Horario_Prescricao(	nr_prescricao_p, cd_intervalo_proc_w, dt_inicio_prescr_ww, dt_prev_execucao_w,nr_horas_validade_w,
								cd_procedimento_w, qt_hora_intervalo_w, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w, ds_horarios2_w,'N', NULL);
			end if;
						
			IF	(NVL(ie_ctrl_glic_w,'KK') <> 'CIG') THEN
				UPDATE	prescr_procedimento
				SET	ds_horarios	= SUBSTR(ds_horarios_w ||ds_horarios2_w,1,2000),
					cd_intervalo	= cd_intervalo_proc_w,
					dt_prev_execucao= dt_prev_execucao_w,
					nr_ocorrencia	= nr_ocorrencia_w
				WHERE	nr_prescricao	= nr_prescricao_p
				AND	nr_sequencia	= nr_sequencia_w;
			ELSIF	(ie_ctrl_glic_w = 'CIG') THEN
				UPDATE	prescr_procedimento --Controle intensivo  glicemia
				SET	ds_horarios	= ds_horarios_ww,
					cd_intervalo	= cd_intervalo_proc_w,
					dt_prev_execucao= dt_prev_execucao_w,
					nr_ocorrencia	= nr_ocorrencia_w
				WHERE	nr_prescricao	= nr_prescricao_p
				AND	nr_sequencia	= nr_sequencia_w;
			END IF;
		else
			select	count(*)
			into	qt_regra_1h_w
			from	intervalo_prescricao d,
				prescr_procedimento a,
				prescr_medica c
			where	a.cd_intervalo		= d.cd_intervalo
			and	a.nr_prescricao		= c.nr_prescricao
			and	a.nr_prescricao 	= nr_prescricao_p
			and	a.nr_sequencia		= nr_sequencia_w;
			
			if	(ie_regra_geral_w = 'H') then
			
				if	(ie_regra_horarios_w = 'HA') then
					
					ds_horarios_w	:= null;

					if	(ie_acm_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807);
					elsif	(ie_se_necessario_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808);
					else
					
						select	nvl(max('S'),'N')
						into	ie_minuto_w
						from	prescr_proc_hor
						where	nr_prescricao	= nr_prescr_lista_w
						and	nr_seq_procedimento = nr_seq_lista_w
						and	to_char(dt_horario,'mi')	<> '00'
						and	nvl(ie_situacao,'A')	= 'A'
						and	((dt_suspensao is null) or
							 (ie_modificar_p = 'S'));
					
						open c04;
						loop
						fetch c04 into	
							dt_horario_w;
						exit when c04%notfound;
							if	(ie_minuto_w	= 'N') then
								if	(ds_horarios_w is null) then
									ds_horarios_w	:= to_char(dt_horario_w,'hh24');
								else
									ds_horarios_w	:= ds_horarios_w ||' ' || to_char(dt_horario_w,'hh24');
								end if;
							else
								if	(ds_horarios_w is null) then
									ds_horarios_w	:= to_char(dt_horario_w,'hh24:mi');
								else
									ds_horarios_w	:= ds_horarios_w ||' ' || to_char(dt_horario_w,'hh24:mi');
								end if;
							end if;
						end loop;
						close c04;
					
						if	(nvl(ie_reordenar_w,'S') = 'S') then
							ds_horarios_w	:= reordenar_horarios(dt_inicio_prescr_w, trim(ds_horarios_w));
							Reordenar_Horarios_Prescr(dt_inicio_prescr_w, ds_horarios_w);
						end if;					
					end if;
						
					update	prescr_procedimento
					set 	ds_horarios		= ds_horarios_w
					where	nr_prescricao 		= nr_prescricao_p
					and	nr_sequencia		= nr_sequencia_w
					and	dt_prev_execucao	is not null;
					
				elsif	(ie_regra_horarios_w = 'HO') then
					
					if	(ie_acm_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807);
					elsif	(ie_se_necessario_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808);
					end if;	
					
					update	prescr_procedimento
					set 	ds_horarios		= ds_horarios_w
					where	nr_prescricao 		= nr_prescricao_p
					and	nr_sequencia		= nr_sequencia_w
					and	dt_prev_execucao	is not null;
				
				elsif	(ie_regra_horarios_w = 'HS') then
						
					select	max(ds_horario)
					into	ds_horarios_aux_w
					from	rep_horario_hor_pac
					where	nr_atendimento	= nr_atendimento_w
					and		cd_procedimento	= cd_procedimento_w
					and		cd_intervalo	= cd_intervalo_w
					and		dt_cancelamento is null;
					
					if	(ds_horarios_aux_w is not null) then
						ds_horarios_w	:= ds_horarios_aux_w;
						
						if	(ie_acm_w	= 'S') then
							ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807);
						elsif	(ie_se_necessario_w	= 'S') then
							ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808);
						end if;	
						
						update	prescr_procedimento
						set 	ds_horarios		= ds_horarios_w
						where	nr_prescricao 	= nr_prescricao_p
						and	nr_sequencia		= nr_sequencia_w
						and	dt_prev_execucao		is not null;
					end if;
				end if;
				
			elsif	(ie_regra_geral_w = '1H') and
				(qt_regra_1h_w > 0) then

				select	d.ie_operacao,
					c.dt_inicio_prescr
				into	ie_operacao_w,
					dt_inicio_prescr_ww
				from	intervalo_prescricao d,
					prescr_procedimento a,
					prescr_medica c
				where	a.cd_intervalo		= d.cd_intervalo
				and	a.nr_prescricao		= c.nr_prescricao
				and	a.nr_prescricao 	= nr_prescricao_p
				and	a.nr_sequencia		= nr_sequencia_w;
				
				ds_horarios_ww		:= ds_horarios_w;
				dt_prev_execucao_ww	:= dt_prev_exec_w;
				
				add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_ww:'||to_char(dt_prev_execucao_ww, 'dd/mm/yyyy hh24:mi:ss'));
					
				if	(ie_se_necessario_w <> 'S') then

					nr_ocorrencia_w		:= 0;
					dt_prev_execucao_ww	:= obter_data_prev_exec(dt_prescricao_p,dt_procedimento_p,cd_setor_atendimento_w, nr_prescricao_p,'A');
					
					if	(ie_regra_prim_hor_proc_w = 'I') then
						begin
						nr_prescricoes_w		:= to_char(NR_PRESCRICAO_P)||','||to_char(nr_prescr_lista_w);
						dt_prev_execucao_ww		:= obter_prim_hor_proc_interv(	nr_atendimento_w,
														nr_prescricoes_w,
														dt_inicio_prescr_ww,
														cd_procedimento_w,
														ie_origem_proced_w,
														cd_intervalo_proc_w,
														ie_regra_prim_hor_proc_w,
														null);
						if	(dt_prev_execucao_ww 	is null) or
							((dt_prev_execucao_ww 	< dt_inicio_prescr_ww) or 
							((dt_prev_execucao_ww 	> dt_validade_prescr_w)))then
							dt_prev_execucao_ww	:= obter_data_prev_exec(dt_prescricao_p,dt_procedimento_p,cd_setor_atendimento_w, nr_prescricao_p,'A');
				
							add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_ww:'||to_char(dt_prev_execucao_ww, 'dd/mm/yyyy hh24:mi:ss'));
						end if;
				
						add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_ww:'||to_char(dt_prev_execucao_ww, 'dd/mm/yyyy hh24:mi:ss')||'|ie_regra_prim_hor_proc_w:'||ie_regra_prim_hor_proc_w);
						end;
					end if;

				/*	if	(ie_operacao_w in ('F','V')) then
						dt_prev_execucao_ww	:= obter_data_prev_exec(dt_prescricao_p,dt_procedimento_p,cd_setor_atendimento_w, nr_prescricao_p,'A');
					end if;*/
						
					if	(ie_regra_geral_w <> 'I') then
						dt_prev_exec_w	:= dt_prev_execucao_ww;
						
						update	prescr_procedimento
						set	dt_prev_execucao	= dt_prev_execucao_ww
						where	nr_prescricao 		= nr_prescricao_p
						and	nr_sequencia		= nr_sequencia_w;
					end if;

					if	(ie_se_necessario_w = 'S') then
						select	nvl(max(b.qt_se_necessario),0),
								nvl(max(b.ie_mesmo_zerado),'N')
						into		nr_ocorrencia_w,
								ie_mesmo_zerado_w
						from		intervalo_prescricao a,
								intervalo_setor b
						where	a.cd_intervalo 		= b.cd_intervalo
						and		nvl(b.cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
						and		nvl(b.cd_estab, cd_estabelecimento_p)			= cd_estabelecimento_p
						and		a.cd_intervalo 		= cd_intervalo_w
						and		((b.cd_unidade_basica = nvl(obter_unid_atend_setor_atual(nr_atendimento_w,cd_setor_atendimento_w,'UB'),b.cd_unidade_basica)) or
								 (b.cd_unidade_basica is null));						
							
						if	(nvl(nr_ocorrencia_w,0) = 0) and
							(ie_mesmo_zerado_w = 'N') then
						
							select	nvl(max(qt_se_necessario),1)
							into	nr_ocorrencia_w
							from	intervalo_prescricao
							where	cd_intervalo	= cd_intervalo_w;
						end if;
							
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808);

					elsif	(ie_acm_w = 'S') then

						select	nvl(max(b.qt_se_necessario),0),
								nvl(max(b.ie_mesmo_zerado),'N')
						into		nr_ocorrencia_w,
								ie_mesmo_zerado_w
						from		intervalo_prescricao a,
								intervalo_setor b
						where	a.cd_intervalo 		= b.cd_intervalo
						and		nvl(b.cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
						and		nvl(b.cd_estab, cd_estabelecimento_p)			= cd_estabelecimento_p
						and		a.cd_intervalo 		= cd_intervalo_w
						and		((b.cd_unidade_basica = nvl(obter_unid_atend_setor_atual(nr_atendimento_w,cd_setor_atendimento_w,'UB'),b.cd_unidade_basica)) or
								 (b.cd_unidade_basica is null));
								 
						if	(nvl(nr_ocorrencia_w,0) = 0) and
							(ie_mesmo_zerado_w = 'N') then
					
							select	nvl(max(qt_se_necessario),1)
							into	nr_ocorrencia_w
							from	intervalo_prescricao
							where	cd_intervalo	= cd_intervalo_w;
						end if;

						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807);
					else

						Calcular_Horario_Prescricao(	nr_prescricao_p, cd_intervalo_proc_w, dt_inicio_prescr_ww, dt_prev_exec_w,nr_horas_validade_w,
										cd_procedimento_w, qt_hora_intervalo_w,	qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w, ds_horarios2_w,'N', NULL);

						ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;						
					end if;

				end if;

				if	(ie_acm_w	= 'S') then
					ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807);
				elsif	(ie_se_necessario_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808);
				elsif	(dt_prev_execucao_ww is null) then
					ds_horarios_w	:= ds_horarios_ww;
				end if;

				if	(ie_reordenar_w = 'S') and
					(ie_acm_w <> 'S') and
					(ie_se_necessario_w	<> 'S') then
					ds_horarios_w	:= reordenar_horarios(dt_inicio_prescr_ww, ds_horarios_w);
				end if;
				
				if (nvl(nr_seq_proc_interno_w,0) > 0) and
				   (obter_ctrl_glic_proc(nr_seq_proc_interno_w) = 'CIG') then
					ds_horarios_w := to_char(dt_prev_exec_w,'hh24:mi');
					nr_ocorrencia_w := 1;
				end if;	
				
				update	prescr_procedimento
				set 	nr_ocorrencia		= nr_ocorrencia_w,
					ds_horarios		= ds_horarios_w
				where	nr_prescricao 		= nr_prescricao_p
				and	nr_sequencia		= nr_sequencia_w
				and	dt_prev_execucao	is not null;

			elsif	(ie_regra_geral_w	= 'I') then

					ds_horarios_w	:= null;
					dt_prev_execucao_w := Converte_Char_Data(to_char(dt_inicio_prescr_w,'dd/mm/yyyy'),to_char(dt_prev_exec_ww,'hh24:mi:ss'),null);
					add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_w:'||to_char(dt_prev_execucao_w, 'dd/mm/yyyy hh24:mi:ss')||'|ie_regra_geral_w:'||ie_regra_geral_w);
					
					if	(dt_prev_execucao_w is not null) then
						if	(dt_prev_execucao_w between dt_inicio_prescr_w and dt_validade_prescr_w) then
							dt_prev_exec2_ww := dt_prev_execucao_w;
						else					
							dt_prev_exec2_ww := Converte_Char_Data(to_char(dt_validade_prescr_w,'dd/mm/yyyy'),to_char(dt_prev_exec_ww,'hh24:mi:ss'),null);
						end if;
						
						if	(dt_prev_exec2_ww between  dt_inicio_prescr_w and dt_validade_prescr_w) then
							dt_prev_execucao_w	:= dt_prev_exec2_ww;
						else
							dt_prev_execucao_w	:= dt_inicio_prescr_w;
						end if;
						
						add_log_dt_prev_execucao($$PLSQL_LINE, nr_sequencia_w, 'dt_prev_execucao_w:'||to_char(dt_prev_execucao_w, 'dd/mm/yyyy hh24:mi:ss')||'|dt_prev_exec2_ww:'||to_char(dt_prev_exec2_ww, 'dd/mm/yyyy hh24:mi:ss')
							||'|dt_inicio_prescr_w:'||to_char(dt_inicio_prescr_w, 'dd/mm/yyyy hh24:mi:ss')||'|dt_validade_prescr_w:'||to_char(dt_validade_prescr_w, 'dd/mm/yyyy hh24:mi:ss'));
					end if;
					if	(ie_acm_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309807);
					elsif	(ie_se_necessario_w	= 'S') then
						ds_horarios_w	:= wheb_mensagem_pck.get_texto(309808);
					else		
						
						select	nvl(max('S'),'N')
						into	ie_minuto_w
						from	prescr_proc_hor
						where	nr_prescricao	= nr_prescr_lista_w
						and	nr_seq_procedimento = nr_seq_lista_w
						and	to_char(dt_horario,'mi')	<> '00'
						and	nvl(ie_situacao,'A')	= 'A'
						and	((dt_suspensao is null) or
							 (ie_modificar_p = 'S'));						
						
						open c04;
						loop
						fetch c04 into	
							dt_horario_w;
						exit when c04%notfound;
							if	(ie_minuto_w	= 'N') then
								if	(ds_horarios_w is null) then
									ds_horarios_w	:= to_char(dt_horario_w,'hh24');
								else
									ds_horarios_w	:= ds_horarios_w ||' ' || to_char(dt_horario_w,'hh24');
								end if;
							else
								if	(ds_horarios_w is null) then
									ds_horarios_w	:= to_char(dt_horario_w,'hh24:mi');
								else
									ds_horarios_w	:= ds_horarios_w ||' ' || to_char(dt_horario_w,'hh24:mi');
								end if;
							end if;
						end loop;
						close c04;
					
						ds_horarios_w	:= reordenar_horarios(dt_inicio_prescr_w, trim(ds_horarios_w));
						Reordenar_Horarios_Prescr(dt_inicio_prescr_w, ds_horarios_w);
					end if;
					
					update	prescr_procedimento
					set 	ds_horarios		= ds_horarios_w, 
						dt_prev_execucao	= dt_prev_execucao_w
					where	nr_prescricao 		= nr_prescricao_p
					and	nr_sequencia		= nr_sequencia_w
					and	dt_prev_execucao	is not null;
			
			end if;			
			
			IF	(ie_proced_agora_w = 'S') THEN
							
				select	max(cd_intervalo)
				into	cd_intervalo_proc_agora_w
				from	intervalo_prescricao
				where	ie_agora	= 'S'
				and	ie_situacao	= 'A'
				and 	Obter_se_intervalo(cd_intervalo,'P') = 'S'
				and 	obter_se_exibe_intervalo(nr_prescricao_p, cd_intervalo, cd_setor_atendimento_w) = 'S';

				UPDATE	prescr_procedimento
				SET	ie_urgencia 		= 'S',
					ie_se_necessario	= 'N',
					ie_acm		 	= 'N',
					cd_intervalo		= cd_intervalo_proc_agora_w,
					dt_prev_execucao	= TRUNC(SYSDATE, 'mi'),
					ds_horarios		= TO_CHAR(SYSDATE, 'hh24:mm')
				WHERE	nr_prescricao	= nr_prescricao_p
				AND	nr_seq_interno	= nr_seq_interno_w;
			END IF;			
		end if;	

		ajustar_prescr_mat_proc(nr_prescricao_p, nr_sequencia_w, cd_perfil_p,null);
		consistir_prescr_procedimento(nr_prescricao_p, nr_sequencia_w, nm_usuario_p, 0, ds_erro_w);			
		
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;			

	END IF;

	nr_prescr_ant_w			:= nr_prescr_lista_w;
	ds_lista_w			:= SUBSTR(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
	END;
END LOOP;

open C013;
loop
fetch C013 into	
	nr_seq_proc_int_w,
	nr_sequencia_w,
	cd_procedimento_ww;
exit when C013%notfound;
	begin
	cd_procedimento_w := null;
	ie_origem_proced_w := null;	
	
	if	(nr_seq_proc_int_w is not null)	then	
		begin
		Obter_Proc_Tab_Interno(nr_seq_proc_int_w, nr_prescricao_p,nr_atendimento_w,null,cd_procedimento_w,ie_origem_proced_w,null,null); 
		end;
	end if;
	
	if	((cd_procedimento_w is not null) and
		(ie_origem_proced_w is not null)) then
		begin
		
		update	prescr_procedimento
		set		cd_procedimento = cd_procedimento_w,
				ie_origem_proced = ie_origem_proced_w
		where	nr_prescricao = nr_prescricao_p
		and		nr_sequencia = nr_sequencia_w;
		
		begin
			update 	w_rep_t
			set     cd_item = cd_procedimento_w
			where 	cd_item = cd_procedimento_ww
			and     ie_modificar_p  =  'S';
		exception when others then
			update 	w_rep_t
			set     cd_item = cd_procedimento_w
			where 	cd_item = to_char(cd_procedimento_ww)
			and     ie_modificar_p  =  'S';
		end;
		
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	
		end;			
	end if;
	
	end;
end loop;
close C013;

if ((ds_log_dt_prev_execucao_w is not null) and (ie_log_dt_prev_execucao_w = 'S')) then
	gerar_log_prescricao(nr_prescricao_p, null, null, null, null, 'Rastreabilidade DT_PREV_EXECUCAO'||ds_log_dt_prev_execucao_w, wheb_usuario_pck.get_nm_usuario, 17733, wheb_usuario_pck.get_ie_commit);
end if;

END PLT_copia_procedimento;
/
