create or replace
procedure inserir_conta_etapa_protocolo (	nr_seq_protocolo_p		number,
					nr_seq_etapa_p		number,
					cd_setor_atend_p		number,
					nm_usuario_p		varchar2) is
							

nr_interno_conta_w	number(10);
vl_adicional		varchar2(10);
cd_pessoa_fisica_w	varchar2(10);
							
Cursor C01 is
	select	nr_interno_conta
	from	pessoa_fisica c,
		atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	a.nr_seq_protocolo = nr_seq_protocolo_p
	order by	a.nr_atendimento;

begin

select 	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from 	usuario
where 	nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_interno_conta_w;
exit when C01%notfound;
	begin
	Inserir_Conta_Etapa(nr_interno_conta_w, nm_usuario_p, nr_seq_etapa_p, cd_setor_atend_p, null, cd_pessoa_fisica_w, null, null,null, vl_adicional);	
	end;
end loop;
close C01;

end inserir_conta_etapa_protocolo;
/