create or replace
procedure fa_liberar_receita_ccih_lista(	ds_sequencias_medic_p	varchar2,
						nm_usuario_p		varchar2) is 

nr_seq_medic_w		varchar2(255);
ds_linha_w		varchar2(1000);				
					
begin
ds_linha_w	:= ds_sequencias_medic_p;

while	(ds_linha_w is not null) loop
	nr_seq_medic_w	:= substr(ds_linha_w,1,instr(ds_linha_w,';') - 1);
	ds_linha_w	:= substr(ds_linha_w,instr(ds_linha_w,';') + 1,length(ds_linha_w));
	fa_liberar_receita_ccih(nr_seq_medic_w,nm_usuario_p);
end loop;

commit;

end fa_liberar_receita_ccih_lista;
/