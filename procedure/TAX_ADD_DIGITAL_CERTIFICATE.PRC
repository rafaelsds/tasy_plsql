create or replace
procedure tax_add_digital_certificate(	nm_user_p				varchar2,
										ds_certificate_p		varchar2,
										ds_password_p			varchar2,
										cd_establishment_p		number,
										cd_use_p				number,
										sequence_certificate_p 	out tax_certificate.nr_sequencia%type) is

sequence_certificate_w tax_certificate.nr_sequencia%type;

begin

	select 	tax_certificate_seq.nextval
	into	sequence_certificate_w
	from 	dual;
	
	insert into tax_certificate(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ds_certificate,
								ds_password,
								dt_installation,
								cd_estabelecimento,
								cd_use)
						 values(sequence_certificate_w,
								sysdate,
								nm_user_p,
								sysdate,
								nm_user_p,
								ds_certificate_p,
								ds_password_p,
								sysdate,
								cd_establishment_p,
								cd_use_p);
	commit;
	
	sequence_certificate_p := sequence_certificate_w;
	
end tax_add_digital_certificate;
/
