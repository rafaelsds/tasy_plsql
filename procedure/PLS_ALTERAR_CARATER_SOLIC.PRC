create or replace
procedure pls_alterar_carater_solic
			(	nr_seq_item_analise_p	Number,
				ie_carater_internacao_p	varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 
				
nr_seq_conta_w			number(10);
ie_carater_internacao_w		varchar2(10);
				
begin

select	nr_seq_conta
into	nr_seq_conta_w
from	w_pls_resumo_conta
where	nr_sequencia = nr_seq_item_analise_p;

update	pls_conta
set	ie_carater_internacao = ie_carater_internacao_p
where	nr_sequencia = nr_seq_conta_w;

commit;

end pls_alterar_carater_solic;
/