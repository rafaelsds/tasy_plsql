create or replace PROCEDURE Validate_dataset_hcp (nr_seq_dataset_p NUMBER, 

                                                  ie_dataset_p     VARCHAR2, 

                                                  nm_usuario_p     VARCHAR2, 

                                                  nr_atendimento_p NUMBER) 

IS 

  qt_rec_segmento_w   NUMBER(10); 
  qt_erro_w           NUMBER(10); 
  qt_alerta_w         NUMBER(10); 
  nr_seq_estrut_arq_w NUMBER(10); 
  nr_count_hcp        NUMBER(10); 
  nr_count_snap       NUMBER(10); 

  CURSOR c01 IS 
    SELECT nr_sequencia, 
           ie_segment ,
           IE_MANDATORY
    FROM   HCP_SEGMENT_STRUCT 

    WHERE  ie_segment = decode(ie_dataset_p, 'PHDB', 'PHD', 'SNAP' ,'SNP' ,'HCP')  ;

  c01_w               c01%ROWTYPE; 

BEGIN 
    OPEN c01; 
    LOOP 
        FETCH c01 INTO c01_w; 
        exit WHEN c01%NOTFOUND; 
        IF( c01_w.ie_segment = 'HCP' or c01_w.ie_segment = 'PHD' ) THEN 
			nr_count_hcp := 0; 
			SELECT Count(*) 
			INTO   nr_count_hcp 
			FROM   hcp_segment_data 
			WHERE  nr_seq_dataset = nr_seq_dataset_p; 
      if(nr_count_hcp=0)then
          Generate_hcp_ds_inconsistency(nr_seq_dataset_p, 'E', 
          Obter_desc_exp_idioma(861837, 9, 'IE_SEGMENTO=' 
                                || c01_w.ie_segment), nm_usuario_p, 
								c01_w.ie_segment, nr_atendimento_p); 
          end if;
          if(nr_count_hcp>0) then
           Validate_segment_hcp(c01_w.nr_sequencia, nr_seq_dataset_p, 
								c01_w.ie_segment, nm_usuario_p, nr_atendimento_p);

          end if;
        END IF; 
      IF( c01_w.ie_segment = 'SNP' ) THEN 
			nr_count_snap := 0; 
			SELECT Count(*) 
			INTO   nr_count_snap 
			FROM   hcp_snap_seg_data 
			WHERE  nr_seq_dataset = nr_seq_dataset_p; 
 

        if(nr_count_snap=0)then      
        
			Generate_hcp_ds_inconsistency(nr_seq_dataset_p, 'E', 

										wheb_mensagem_pck.Get_texto(996261,'IE_SEGMENTO=' || c01_w.ie_segment), nm_usuario_p, 

										c01_w.ie_segment, nr_atendimento_p); 
        end if;

		if(nr_count_snap>0) then
			Validate_segment_hcp(c01_w.nr_sequencia, nr_seq_dataset_p, 
								 c01_w.ie_segment,nm_usuario_p, nr_atendimento_p);

        end if;
        END IF; 
    END LOOP; 
	CLOSE c01; 



    BEGIN 
        SELECT 1  
        INTO   qt_erro_w 
        FROM   hcp_inconsistency_ds 
        WHERE  nr_seq_dataset = nr_seq_dataset_p 
               AND ie_type = 'E' 
               AND ROWNUM = 1; 

    EXCEPTION 
        WHEN OTHERS THEN 
          qt_erro_w := 0; 
    END; 

    BEGIN 
        SELECT 1 
        INTO   qt_alerta_w 
        FROM   hcp_inconsistency_ds 
        WHERE  nr_seq_dataset = nr_seq_dataset_p 
        AND ie_type = 'A' 
		AND ROWNUM = 1;  

    EXCEPTION 
        WHEN OTHERS THEN 
          qt_alerta_w := 0; 
    END; 

    IF ( qt_erro_w > 0 ) THEN 
      UPDATE hcp_dataset_send 
      SET    ie_validation_status = 'E' 
      WHERE  nr_sequencia = nr_seq_dataset_p; 

    ELSIF ( qt_alerta_w > 0 ) THEN 
      UPDATE hcp_dataset_send 
      SET    ie_validation_status = 'A' 
      WHERE  nr_sequencia = nr_seq_dataset_p; 

    ELSE 

      UPDATE hcp_dataset_send 
      SET    ie_validation_status = 'V', 
             ie_sending_status = 'P' 
      WHERE  nr_sequencia = nr_seq_dataset_p; 

    END IF; 

END validate_dataset_hcp;
/