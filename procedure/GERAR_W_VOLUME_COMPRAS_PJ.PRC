create or replace
procedure gerar_w_volume_compras_pj(	cd_cnpj_p		varchar2,
					dt_inicio_p		date,
					dt_final_p			date,
					cd_grupo_material_p	number,
					cd_subgrupo_material_p	number,
					cd_classe_material_p	number,
					cd_material_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is

nr_sequencia_w		number(10);					
ie_entrada_saida_w		varchar2(1);
vl_compra_w		number(13,4) := 0;
vl_liquido_w		number(13,4);
qt_item_estoque_w		number(13,4);
qt_compra_w		number(13,4) := 0;

cursor c01 is
select distinct
	a.nr_sequencia,
	p.ie_entrada_saida
from	condicao_pagamento c,
	operacao_estoque p,
	operacao_nota o,
	nota_fiscal a
where	a.dt_entrada_saida between dt_inicio_p and fim_dia(dt_final_p)
and	a.cd_operacao_nf = o.cd_operacao_nf
and	p.ie_tipo_requisicao in (6,0)
and	((p.ie_consignado is null) or (p.ie_consignado in (0,1,3)))
and	a.dt_atualizacao_estoque is not null
and	o.cd_operacao_estoque	= p.cd_operacao_estoque
and	a.cd_condicao_pagamento	= c.cd_condicao_pagamento (+)
and	a.ie_situacao	= '1'
and	o.ie_tipo_operacao = '1'
and	nvl(a.cd_cgc, a.cd_cgc_emitente) is not null
and	nr_interno_conta is null 
and	nr_seq_protocolo is null
and	decode(p.ie_tipo_requisicao, 0, nvl(a.cd_cgc, a.cd_cgc_emitente), a.cd_cgc_emitente) = cd_cnpj_p
and	a.cd_estabelecimento = cd_estabelecimento_p;

cursor c02 is
select	nvl(a.qt_item_estoque,0),
	a.vl_liquido
from	material m,
	nota_fiscal_item a,
	estrutura_material_v e
where	a.nr_sequencia = nr_sequencia_w
and	a.cd_material = m.cd_material
and	e.cd_material = a.cd_material
and	nvl(vl_liquido,0) <> 0
and	ie_entrada_saida_w = 'E'
and	((cd_grupo_material_p = 0) or (e.cd_grupo_material = cd_grupo_material_p))
and	((cd_subgrupo_material_p = 0) or (e.cd_subgrupo_material = cd_subgrupo_material_p))
and	((cd_classe_material_p = 0) or (e.cd_classe_material = cd_classe_material_p))
and	((cd_material_p = 0) or (e.cd_material = cd_material_p))
union all
select	nvl(a.qt_item_estoque,0),
	(a.vl_liquido * -1)
from	material m,
	nota_fiscal_item a,
	estrutura_material_v e
where	a.nr_sequencia	= nr_sequencia_w
and	a.cd_material = m.cd_material
and	e.cd_material = a.cd_material
and	nvl(vl_liquido,0) <> 0
and	ie_entrada_saida_w = 'S'
and	((cd_grupo_material_p = 0) or (e.cd_grupo_material = cd_grupo_material_p))
and	((cd_subgrupo_material_p = 0) or (e.cd_subgrupo_material = cd_subgrupo_material_p))
and	((cd_classe_material_p = 0) or (e.cd_classe_material = cd_classe_material_p))
and	((cd_material_p = 0) or (e.cd_material = cd_material_p));
					
					
begin

delete from w_volume_compras_pj
where nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ie_entrada_saida_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into
		qt_item_estoque_w,
		vl_liquido_w;
	exit when C02%notfound;
		begin
		
		vl_compra_w	:= vl_compra_w + vl_liquido_w;
		qt_compra_w	:= qt_compra_w + qt_item_estoque_w;
		end;
	end loop;
	close C02;	
	end;
end loop;
close C01;

insert into w_volume_compras_pj(
	cd_cnpj,
	nm_usuario,
	vl_compra,
	qt_compra)
values(	cd_cnpj_p,
	nm_usuario_p,
	vl_compra_w,
	qt_compra_w);


commit;

end gerar_w_volume_compras_pj;
/