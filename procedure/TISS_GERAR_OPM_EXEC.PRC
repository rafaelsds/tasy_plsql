create or replace
procedure TISS_GERAR_OPM_EXEC ( nr_seq_conta_guia_p	number,
				nr_seq_guia_p		number,
				nm_usuario_p		varchar2) is

cd_opm_w		varchar2(20);
vl_opm_w		number(15,2);
vl_total_opm_w		number(15,2);
qt_proc_guia_w		number(15,0);
nr_seq_guia_w		number(15,0);
ds_opm_w		varchar2(255);
ds_fabricante_w		varchar2(255);
qt_opm_w		number(10);
nr_seq_apresent_opm_w	number(10);
nr_seq_apres_opm_compl_w 	number(10);
cd_edicao_amb_w		varchar2(20);
qt_autorizada_w		number(15,2);
nr_interno_conta_w		number(10);
cd_convenio_w		number(5);
cd_estabelecimento_w	number(4);
cd_barras_w		varchar2(255);
cd_opm_gravado_w	varchar2(20);
nr_seq_opme_w		number(10);
ie_agrupar_opme_w	varchar2(1);
dt_mesano_referencia_w	date;
ds_versao_w		varchar2(20);

cursor c01 is
select	cd_opm,
	cd_tabela,
	ds_opm,
	qt_opm,
	vl_unitario_opm,
	sum(vl_opm),
	cd_barras,
	decode(ie_agrupar_opme_w,'N',nr_sequencia,null)
from	tiss_conta_opm_exec 
where	nr_seq_guia		= nr_seq_conta_guia_p
and	nr_seq_reap_conta	is null
group by cd_opm,
	cd_tabela,
	ds_opm,
	qt_opm,
	vl_unitario_opm,
	cd_barras,
	decode(ie_agrupar_opme_w,'N',nr_sequencia,null)
union all
select	a.cd_opm,
	a.cd_tabela,
	a.ds_opm,
	a.qt_opm,
	a.vl_unitario_opm,
	sum(a.vl_opm),
	a.cd_barras,
	decode(ie_agrupar_opme_w,'N',a.nr_sequencia,null)
from	tiss_conta_opm_exec a,
	tiss_conta_guia b
where	b.nr_seq_reap_conta	= a.nr_seq_reap_conta
and	b.nr_sequencia		= nr_seq_conta_guia_p
and	a.nr_seq_reap_conta	is not null
group by cd_opm,
	cd_tabela,
	ds_opm,
	qt_opm,
	vl_unitario_opm,
	cd_barras,
	decode(ie_agrupar_opme_w,'N',a.nr_sequencia,null);

begin

select	max(nr_interno_conta)
into	nr_interno_conta_w
from	tiss_conta_guia
where	nr_sequencia	= nr_seq_conta_guia_p;

select	max(cd_convenio_parametro),
	max(cd_estabelecimento),
	max(dt_mesano_referencia)
into	cd_convenio_w,
	cd_estabelecimento_w,
	dt_mesano_referencia_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_w;

select	nvl(max(ie_agrupar_opme),'S')
into	ie_agrupar_opme_w
from	tiss_parametros_convenio
where	cd_convenio		= cd_convenio_w
and	cd_estabelecimento	= cd_estabelecimento_w;

select	max(tiss_obter_versao(cd_convenio_w, cd_estabelecimento_w,dt_mesano_referencia_w))
into	ds_versao_w
from	dual;

qt_opm_w 		:= 0;
nr_seq_apresent_opm_w	:= 0;
nr_seq_apres_opm_compl_w := 0;

qt_proc_guia_w		:= 0;

--Somente gera OPME se n�o for da v3 pois na v3 as OPME s�o geradas na TISS_CONTA_DESP
if	(obter_se_projeto_versao(0,12,ds_versao_w,0) = 'N')then

	open c01;
	loop
	fetch c01 into
		cd_opm_w,
		cd_edicao_amb_w,
		ds_opm_w,
		qt_autorizada_w,
		vl_opm_w,
		vl_total_opm_w,
		cd_barras_w,
		nr_seq_opme_w;
	exit when c01%notfound;

		select	max(cd_opm)
		into	cd_opm_gravado_w
		from	w_tiss_opm_exec
		where	nm_usuario		= nm_usuario_p
		and	nvl(vl_unitario,0)	= nvl(vl_opm_w,0)
		and	nr_interno_conta 	= nr_interno_conta_w
		and	upper(ds_opm)		= upper(ds_opm_w)  /*OS76974.18/12/2007. Tive que incluir esta compara��o pois pode haver */
		and	cd_opm			= cd_opm_w
		and	nvl(ie_agrupar_opme_w,'S') = 'S';	

		if	(nvl(cd_opm_gravado_w,'0') = '0') then		

			qt_opm_w 		:= qt_opm_w + 1;
			nr_seq_apresent_opm_w	:= nr_seq_apresent_opm_w + 1;	
				
			if	(qt_opm_w <= 9) then
					insert into w_tiss_opm_exec
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_guia,
						cd_opm,
						cd_edicao,
						ds_opm,
						qt_solicitada,
						qt_autorizada,
						vl_unitario,
						vl_opm,
						nr_seq_apresentacao,
						cd_barras,
						nr_interno_conta)
					values (w_tiss_opm_exec_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_guia_p,
						cd_opm_w,
						cd_edicao_amb_w,
						ds_opm_w,
						qt_autorizada_w,
						qt_autorizada_w,
						vl_opm_w,
						vl_total_opm_w,
						nr_seq_apresent_opm_w,
						cd_barras_w,
						nr_interno_conta_w);
			else
				qt_proc_guia_w		:= qt_proc_guia_w + 1;
				nr_seq_apres_opm_compl_w := nr_seq_apres_opm_compl_w + 1;

				if	(qt_proc_guia_w = 1) then
					tiss_gerar_cabecalho_spsadt(nr_seq_conta_guia_p, nr_seq_guia_w, nm_usuario_p);
					TISS_COMPLETAR_GUIA(nr_seq_guia_p, nm_usuario_p);
				end if;

				insert into w_tiss_opm_exec
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_guia,
					cd_opm,
					cd_edicao,
					ds_opm,
					qt_solicitada,
					qt_autorizada,
					vl_unitario,
					vl_opm,
					nr_seq_apresentacao,
					cd_barras,
					nr_interno_conta)
				values (w_tiss_opm_exec_seq.nextval,
					sysdate,
					nm_usuario_p,
					nr_seq_guia_w,
					cd_opm_w,
					cd_edicao_amb_w,
					ds_opm_w,
					qt_autorizada_w,
					qt_autorizada_w,
					vl_opm_w,
					vl_total_opm_w,
					nr_seq_apres_opm_compl_w,
					cd_barras_w,
					nr_interno_conta_w);

			end if;
		end if;

	end loop;
	close c01;
end if;

TISS_COMPLETAR_GUIA(nr_seq_guia_w, nm_usuario_p);

end TISS_GERAR_OPM_EXEC;
/