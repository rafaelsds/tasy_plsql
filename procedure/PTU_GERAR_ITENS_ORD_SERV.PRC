create or replace
procedure ptu_gerar_itens_ord_serv(	nr_seq_ordem_serv_p  			ptu_requisicao_ordem_serv.nr_sequencia%type,
					nr_seq_resposta_p			ptu_resposta_req_ord_serv.nr_sequencia%type,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Rotina utilizada para valida��o da transa��o 00806 - Ordem de servi�o do PTU
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_resp_req_serv_w		ptu_resposta_req_servico.nr_sequencia%type;
cd_unimed_solicitante_w		ptu_requisicao_ordem_serv.cd_unimed_solicitante%type;
cd_unimed_beneficiario_w	ptu_requisicao_ordem_serv.cd_unimed_beneficiario%type;
nr_seq_material_w		pls_material.nr_sequencia%type;
ie_aceito_w			varchar2(1);
qt_registros_w			number(10);

Cursor C01 is
	select	cd_servico,
		cd_servico_consersao,
		ie_origem_servico,
		ie_tipo_tabela,
		qt_servico_aut,
		nr_seq_item
	from	ptu_req_ord_serv_servico
	where	nr_seq_req_ord = nr_seq_ordem_serv_p;

begin

select	cd_unimed_beneficiario,
	cd_unimed_solicitante
into	cd_unimed_beneficiario_w,
	cd_unimed_solicitante_w
from	ptu_requisicao_ordem_serv
where	nr_sequencia	= nr_seq_ordem_serv_p;

for	r_c01_w	in c01	loop
	select	ptu_resposta_req_servico_seq.nextval
	into	nr_seq_resp_req_serv_w
	from	dual;

	if	(r_c01_w.ie_tipo_tabela	in('0','1','4')) then
		if	(cd_unimed_solicitante_w	= cd_unimed_beneficiario_w) then
			ie_aceito_w	:= ptu_obter_item_req_ord_aceito(	nvl(r_c01_w.cd_servico_consersao, r_c01_w.cd_servico), r_c01_w.ie_origem_servico, null,
										cd_unimed_beneficiario_w);
		else
			ie_aceito_w	:= 'S';
		end if;
		
		insert	into ptu_resposta_req_servico 
			(nr_sequencia, nr_seq_resp_req_ord, cd_servico,
			 qt_servico_aut, ie_status_requisicao, ie_tipo_tabela,
			 nm_usuario, dt_atualizacao, nm_usuario_nrec,
			 dt_atualizacao_nrec, nr_seq_item)
		values	(nr_seq_resp_req_serv_w, nr_seq_resposta_p, r_c01_w.cd_servico,
			 decode(ie_aceito_w,'S',r_c01_w.qt_servico_aut,0), decode(ie_aceito_w,'S',2,1), r_c01_w.ie_tipo_tabela,
			 nm_usuario_p, sysdate, nm_usuario_p,
			 sysdate, r_c01_w.nr_seq_item);

		if	(ie_aceito_w	= 'N') then
			ptu_inserir_inconsistencia(	null, null, 2010,
							'',cd_estabelecimento_p, nr_seq_resposta_p, 
							'OR', '00807', nr_seq_resp_req_serv_w, 
							null, null, nm_usuario_p);
		end if;
	elsif	(r_c01_w.ie_tipo_tabela	in('2','3','5','6')) then
		nr_seq_material_w	:= substr(pls_obter_dados_material_a900(nvl(r_c01_w.cd_servico_consersao, r_c01_w.cd_servico),null,'NR'),1,255);

		if	(nvl(nr_seq_material_w,0)	= 0) then
			nr_seq_material_w	:= substr(pls_obter_seq_codigo_material('',nvl(r_c01_w.cd_servico_consersao, r_c01_w.cd_servico)),1,8);
		end if;

		if	(cd_unimed_solicitante_w	= cd_unimed_beneficiario_w) then
			ie_aceito_w	:= ptu_obter_item_req_ord_aceito(null, null, nr_seq_material_w, cd_unimed_beneficiario_w);
		else
			ie_aceito_w	:= 'S';
		end if;
		
		insert	into ptu_resposta_req_servico 
			(nr_sequencia, nr_seq_resp_req_ord, cd_servico,
			 qt_servico_aut, ie_status_requisicao, ie_tipo_tabela,
			 nm_usuario, dt_atualizacao, nm_usuario_nrec,
			 dt_atualizacao_nrec, nr_seq_item)
		values	(nr_seq_resp_req_serv_w, nr_seq_resposta_p, r_c01_w.cd_servico,
			 decode(ie_aceito_w,'S',r_c01_w.qt_servico_aut,0), decode(ie_aceito_w,'S',2,1), r_c01_w.ie_tipo_tabela,
			 nm_usuario_p, sysdate, nm_usuario_p,
			 sysdate, r_c01_w.nr_seq_item);

		if	(ie_aceito_w	= 'N') then
			ptu_inserir_inconsistencia(	null, null, 2010,
							'',cd_estabelecimento_p, nr_seq_resposta_p, 
							'OR', '00807', null, 
							nr_seq_resp_req_serv_w, null, nm_usuario_p);
		end if;
	end if;
end loop;

select	count(1)
into	qt_registros_w
from	ptu_resposta_req_servico
where	ie_status_requisicao	= 2
and	nr_seq_resp_req_ord	= nr_seq_resposta_p;

if	(qt_registros_w	> 0) then
	-- Se a resposta da ordem de servi�o vier com servi�os recusados e aceitos, a ordem de servi�o fica pendente de pedido de autoriza��o
	update	ptu_requisicao_ordem_serv
	set	ie_estagio		= 8
	where	nr_sequencia		= nr_seq_ordem_serv_p;
else
	-- Se a resposta da ordem de servi�o for totalmente recusada, se encerra o processo
	update	ptu_requisicao_ordem_serv
	set	ie_estagio		= 4
	where	nr_sequencia		= nr_seq_ordem_serv_p;
end if;

commit;

end ptu_gerar_itens_ord_serv;
/