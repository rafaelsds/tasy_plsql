create or replace
procedure pls_sel_tab_simul_coletivo
		(	nr_seq_simul_perfil_p	number,
			nr_seq_tabela_p		number,
			ie_checado_p		varchar2,
			nm_usuario_p		Varchar2) is 
			
nr_seq_simulacao_w	number(10);
vl_simulacao_perfil_w	number(15,2);

begin

update	pls_simul_perfil_tabela
set	ie_tabela_selecionada	= ie_checado_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_seq_simul_perfil	= nr_seq_simul_perfil_p
and	nr_seq_tabela		= nr_seq_tabela_p;

update	pls_simulacao_perfil
set	vl_simulacao_perfil	= 0,
	VL_SIMULACAO_SCA	= 0
where	nr_sequencia		= nr_seq_simul_perfil_p;

select	max(nr_seq_simulacao)
into	nr_seq_simulacao_w
from	pls_simulacao_perfil
where	nr_sequencia		= nr_seq_simul_perfil_p;

select	sum(vl_simulacao_perfil)
into	vl_simulacao_perfil_w
from	pls_simulacao_perfil
where	nr_seq_simulacao	= nr_seq_simulacao_w;

if	(vl_simulacao_perfil_w is null) then
	vl_simulacao_perfil_w	:= 0;
end if;	

update	pls_simulacao_preco
set	vl_simulacao	= vl_simulacao_perfil_w
where	nr_sequencia	= nr_seq_simulacao_w;

commit;

end pls_sel_tab_simul_coletivo;
/
