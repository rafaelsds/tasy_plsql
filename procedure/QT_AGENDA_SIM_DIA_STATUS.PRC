CREATE OR REPLACE 
PROCEDURE Qt_Agenda_Sim_Dia_Status(
					cd_estabelecimento_p		Number,
					nr_seq_pac_sim_p     	Number,
					dt_Inicial_p			Date,
					dt_final_p			Date,
					nm_usuario_p			varchar2,
					hr_min_local_p			number,
					hr_max_local_p			number,
					nr_minuto_duracao_p		number,
					ie_Status_p		Out		Varchar2) is


ie_Status_w		Varchar2(4000);
ie_Status_dia_w		Varchar2(10);
dt_atual_w		Date;
cd_tipo_agenda_w	Number(15,0);
ie_feriado_w		Varchar2(10);
ie_dia_feriado_w	Varchar2(10);
ie_dia_semana_w		Varchar2(10);
qt_horario_w		Number(15,0);
qt_horario_livre_w	Number(15,0);
qt_regra_horario_w	Number(15,0);
qt_horario_bloqueado_w	Number(15,0);
ds_horarios_w		Varchar2(255);
ie_bloqueio_w		Varchar2(10);
ie_gravar_w		Varchar2(10) := 'N';
ie_sobra_horario_w	Varchar2(10) := 'S';
ds_retorno_w		Varchar2(4000) := '';
qt_bloqueio_w		number(15,0);
qt_horario_ocupado_w	number(15,0);
qt_dia_pend_w		number(10);
qt_agendado_w		number(10);
dt_min_trat_w		date;
dt_max_trat_w		date;
hr_hora_w		number(2);
ie_primeiro_w		varchar2(1);
ie_gerado_w		varchar2(1)	:= 'N';
qt_tempo_medic_w	number(10);
dt_horario_w		date;
--ie_status_w		varchar2(15);
dt_primeiro_livre_w	date;
ie_dia_livre_w		varchar2(1)	:= 'N';
qt_agendamento_w	number(10);
nr_Seq_local_w		number(10);
ie_status_hor_w		varchar2(15);
qt_tempo_prep_medic_w	number(10);
qt_tempo_prep_pac_w	number(10);
qt_tempo_div_tela_w	number(10);

Cursor C01 is
	select	dt_horario,
		ie_Status
	from	w_agenda_quimio
	where	nr_seq_paciente		= nr_seq_pac_sim_p
	and	trunc(dt_horario)	= trunc(dt_atual_w)
	and	nr_seq_local		= nr_seq_local_w
	order by 1;

Cursor C02 is
	select	nr_sequencia
	from	qt_local
	where	ie_situacao	= 'A'
	order by 1;

BEGIN
if	((Dt_final_p - dt_inicial_p) > 255) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262405);
end if;

select	nvl(max(Obter_Valor_Param_Usuario(865, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	qt_tempo_prep_medic_w
from	dual;

select	nvl(max(Obter_Valor_Param_Usuario(865, 2, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	qt_tempo_prep_pac_w
from	dual;

select	nvl(max(Obter_Valor_Param_Usuario(865, 3, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	qt_tempo_div_tela_w
from	dual;

select 	min(nvl(dt_real, dt_prevista))
into	dt_min_trat_w
from	paciente_atendimento_sim
where	nr_seq_paciente	= nr_seq_pac_sim_p;

select 	max(nvl(dt_Real, dt_prevista))
into	dt_max_trat_w
from	paciente_atendimento_sim
where	nr_seq_paciente	= nr_seq_pac_sim_p;

select	max(a.qt_tempo_medic)
into	qt_tempo_medic_w
from	paciente_setor_sim a,
	paciente_atendimento_sim b
where	a.nr_sequencia	= b.nr_seq_paciente
and	b.nr_seq_paciente	= nr_seq_pac_sim_p;

qt_tempo_medic_w	:= qt_tempo_medic_w + qt_tempo_prep_medic_w + qt_tempo_prep_pac_w;

ie_status_w					:= '';
dt_atual_w					:= trunc(dt_inicial_p, 'dd');
WHILE	(dt_atual_w	<= Trunc(dt_final_p,'dd')) LOOP
	begin
	ie_status_dia_w			:= 'X';
	if	(trunc(dt_atual_w)	>= trunc(dt_min_trat_w)) and
		(trunc(dt_atual_w)	<= trunc(dt_max_trat_w)) then
		select	count(*)
		into	qt_agendamento_w
		from	paciente_atendimento_sim
		where	nr_seq_paciente	= nr_seq_pac_sim_p
		and	trunc(nvl(dt_real, dt_prevista))	= trunc(dt_atual_w);
		if	(qt_agendamento_w	> 0) then
			ie_status_dia_w	:= 'L';			
		end if;
		ie_dia_livre_w	:= 'N';
		ie_primeiro_w	:= 'S';
	end if;
	
	ie_status_w	:= ie_status_w || ie_status_dia_w;
	dt_atual_w	:= dt_atual_w + 1;
	end;
END LOOP;
ie_status_p		:= ie_status_w;

commit; 

END Qt_Agenda_Sim_Dia_Status;
/
