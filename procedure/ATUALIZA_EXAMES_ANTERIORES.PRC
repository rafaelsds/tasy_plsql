create or replace
procedure atualiza_exames_anteriores(ds_exames_anteriores_p	Varchar2,
				     nr_prescricao_p number) is 

ds_exames_anteriores_w	varchar2(255);

begin

ds_exames_anteriores_w := substr(ds_exames_anteriores_p,1,254);

if	(nr_prescricao_p is not null) then
	begin

	update	prescr_medica
	set	DS_EXAME_ANTERIOR = ds_exames_anteriores_w
	where   nr_prescricao = nr_prescricao_p;

	end;
	end if;
commit;

end atualiza_exames_anteriores;
/