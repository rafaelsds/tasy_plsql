create or replace
procedure Gerar_kit_solucao(	nr_prescricao_p		number,	
				nm_usuario_p		Varchar2,
				cd_perfil_p		number) is 

cd_setor_atendimento_w		number(5);
qt_min_w			number(3);
qt_max_w			number(3);
cd_kit_material_w		number(5);
nr_seq_regra_w			number(10);
ie_ordem_w			number(3);
qt_soro_w			number(5);
ds_lista_w			varchar2(2000);
nr_sequencia_w			number(10);
nr_seq_lista_w			number(10);
cd_estabelecimento_w		number(4);
nr_atendimento_w		number(10);
ie_tipo_atendimento_w		number(3);
cd_material_kit_w		number(15);
qt_unitaria_w			Number(18,6);
qt_material_kit_w		number(15,3);
cd_unidade_medida_w		Varchar2(30);
ie_agrupador_w			number(2);
cd_intervalo_kit_w		varchar2(7);
ie_entra_conta_w		varchar2(01);
cd_motivo_baixa_w		number(3,0);
cd_motivo_baixa_ww		number(3,0);
ie_permite_alterar_w		Varchar2(7);
ie_via_aplicacao_w		Varchar2(5);
cd_recem_nato_w			varchar2(10);
ie_recem_nato_w			varchar2(1);
cd_convenio_w			Number(5,0);
ie_tipo_convenio_w		Number(2);
ie_sexo_paciente_w		varchar2(1);
qt_idade_paciente_w     	number(5,0);
qt_material_w			number(15,3);
dt_primeiro_horario_w		date;
hr_prim_horario_w		varchar2(5);
nr_horas_validade_w		number(5);
ds_horarios_kit_w		varchar2(2000);
ds_horarios_kit_ww		varchar2(2000);
nr_intervalo_w			number(10);
ie_utiliza_cobra_kit_w		varchar2(01);
ie_prescr_mat_sem_lib_w		Varchar2(255);	
cd_material_disp_w		number(6,0);
cd_intervalo_disp_w		varchar2(7);
ie_via_aplicacao_disp_w		varchar2(5);
qt_unitaria_disp_w		number(18,6);
qt_material_disp_w		number(15,3);
qt_dose_especial_disp_w		number(18,6);
nr_ocorrencia_disp_w		number(15,4);
ds_dose_diferenciada_disp_w	varchar2(50);
ie_origem_inf_disp_w		varchar2(1);
cd_unidade_medida_dose_disp_w	varchar2(30);
qt_dias_util_disp_w		number(3,0);
qt_total_dispensar_w		number(18,6);
ie_regra_disp_w			varchar2(1);
ds_erro_disp_w			varchar2(2000);
ie_acm_w			varchar2(1);
ie_se_necessario_w		varchar2(1);

cursor	c01 is
select	nr_sequencia,
	qt_min,
	qt_max,
	cd_kit_material,
	decode(cd_setor_atendimento,null,2,1) ie_ordem
from	rep_regra_kit
where	nvl(cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w;
			
cursor	c02 is
select	c.nr_sequencia
from	rep_regra_kit_soro a,
	estrutura_material_v b,
	prescr_material c
where	c.nr_prescricao = nr_prescricao_p
and	c.cd_material	= b.cd_material
and	c.ie_agrupador	= 1
and	a.nr_seq_regra	= nr_seq_regra_w
and	nvl(a.cd_grupo_material,nvl(b.cd_grupo_material,0)) = nvl(b.cd_grupo_material,0)
and	nvl(a.cd_subgrupo_material,nvl(b.cd_subgrupo_material,0)) = nvl(b.cd_subgrupo_material,0)
and	nvl(a.cd_classe_material,nvl(b.cd_classe_material,0)) = nvl(b.cd_classe_material,0)
and	nvl(a.cd_material,c.cd_material) = c.cd_material;
			
CURSOR C03 IS
select	c.cd_material,
	nvl(decode(nvl(c.ie_multiplica_dose,'N'),'S',c.qt_material * qt_unitaria_w,c.qt_material),1),
	nvl(c.cd_unidade_medida_prescr, substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_w,'UMS'),1,30)),
	OBTER_CLASSIF_MATERIAL_PROCED(c.cd_material,null,null) ie_agrupador,
	c.cd_intervalo,
	c.ie_entra_conta,
	nvl(c.cd_motivo_baixa,0),
	c.ie_permite_alterar,
	c.ie_via_aplicacao
from	material d,
	componente_kit c,
	kit_material a
where	a.cd_kit_material	= cd_kit_material_w
and	a.cd_kit_material	= c.cd_kit_material
and	a.ie_situacao		= 'A'
and	c.ie_situacao		= 'A'
and	d.ie_situacao		= 'A'
and	c.cd_material     	= d.cd_material
and	((c.cd_estab_regra is null) or 
	 (c.cd_estab_regra = cd_estabelecimento_w))
and	cd_kit_material_w	is not null
and 	nvl(c.cd_convenio,0) =	(select nvl(max(x.cd_convenio), 0)
				from	componente_kit x
				where	x.cd_kit_material = c.cd_kit_material
				and	x.cd_material	  = c.cd_material
				and	x.cd_convenio     = cd_convenio_w
				and	((x.cd_estab_regra is null) or 
					 (x.cd_estab_regra = cd_estabelecimento_w)))
and 	nvl(c.ie_tipo_convenio,0) =	(select nvl(max(x.ie_tipo_convenio), 0)
				from	componente_kit x
				where	x.cd_kit_material = c.cd_kit_material
				and	x.cd_material	  = c.cd_material
				and	x.ie_tipo_convenio     = ie_tipo_convenio_w
				and	((x.cd_estab_regra is null) or 
					 (x.cd_estab_regra = cd_estabelecimento_w)))					 
and	nvl(c.ie_sexo, nvl(ie_sexo_paciente_w,0)) = nvl(ie_sexo_paciente_w,0)
and	((c.ie_recem_nato =  nvl(ie_recem_nato_w, 'A')) or 
	 (nvl(c.ie_recem_nato, 'A') = 'A'))
and	(qt_idade_paciente_w 	between nvl(c.qt_idade_minima,-1) and nvl(c.qt_idade_maxima,999));
			
begin

select	nvl(max(cd_setor_atendimento), 0),
	nvl(max(obter_convenio_atendimento(nr_atendimento)),0),
	max(obter_tipo_atendimento(nr_atendimento)),
	max(cd_recem_nato),
	nvl(max(ie_recem_nato), 'A'),
	max(cd_estabelecimento),
	max(substr(obter_sexo_pf(cd_pessoa_fisica, 'C'),1,1)),
	nvl(max(obter_idade_pf(cd_pessoa_fisica,sysdate,'A')),0),
	max(dt_primeiro_horario),
	max(nr_horas_validade),
	max(to_char(dt_primeiro_horario,'hh24:mi'))
into	cd_setor_atendimento_w,
	cd_convenio_w,
	ie_tipo_atendimento_w,
	cd_recem_nato_w,
	ie_recem_nato_w,
	cd_estabelecimento_w,
	ie_sexo_paciente_w,
	qt_idade_paciente_w,
	dt_primeiro_horario_w,
	nr_horas_validade_w,
	hr_prim_horario_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

if	(cd_convenio_w	is not null) and
	(cd_convenio_w	> 0) then
	select	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio = cd_convenio_w;
end if;

if	(cd_estabelecimento_w is null) then
	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	usuario
	where	nm_usuario	= nm_usuario_p;
end if;

Obter_Param_Usuario(924, 194, cd_perfil_p, nm_usuario_p, cd_estabelecimento_w, cd_motivo_baixa_ww);
Obter_Param_Usuario(924, 530, cd_perfil_p, nm_usuario_p, cd_estabelecimento_w, ie_prescr_mat_sem_lib_w);

select	nvl(max(IE_COBRA_KIT_PRESCR), 'N')
into	ie_utiliza_cobra_kit_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

--Cursor com as regras
open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	qt_min_w,
	qt_max_w,
	cd_kit_material_w,
	ie_ordem_w;
exit when C01%notfound;
	begin

	ds_lista_w	:= '';
	
	--Cursor para pegar os soros cadastrados na regra, e presentes na prescri��o como medicamentos
	open C02;
	loop
	fetch C02 into	
		nr_seq_lista_w;
	exit when C02%notfound;
		if	(ds_lista_w	is null) then
			ds_lista_w := nr_seq_lista_w;
		else
			ds_lista_w := ds_lista_w||','||nr_seq_lista_w;
		end if;
	end loop;
	close C02;
	
	--Contar os registros que s�o considerados pela regra como solu��o
	select	sum(qt_soro)
	into	qt_soro_w
	from	(	--Quantidade de solu��es
			select	count(distinct nr_seq_solucao) qt_soro
			from	prescr_solucao a
			where	nr_prescricao = nr_prescricao_p
			and		exists (select 1 from prescr_material x, estrutura_material_v b, rep_regra_kit_soro c 
							where x.nr_prescricao = a.nr_prescricao 
							and x.nr_sequencia_solucao = a.nr_seq_solucao
							and	x.cd_material	= b.cd_material
							and	c.nr_seq_regra	= nr_seq_regra_w
							and	nvl(c.cd_grupo_material,nvl(b.cd_grupo_material,0)) = nvl(b.cd_grupo_material,0)
							and	nvl(c.cd_subgrupo_material,nvl(b.cd_subgrupo_material,0)) = nvl(b.cd_subgrupo_material,0)
							and	nvl(c.cd_classe_material,nvl(b.cd_classe_material,0)) = nvl(b.cd_classe_material,0)
							and	nvl(c.cd_material,x.cd_material) = x.cd_material)
			union all
			--Quantidade de soros prescritos como medicamentos
			select	count(nr_sequencia) qt_soro
			from	prescr_material
			where	nr_prescricao = nr_prescricao_p
			and	ie_agrupador = 1
			and	obter_se_contido(nr_sequencia, ds_lista_w) = 'S'
			union all
			--Quantidade de medicamentos que possuem diluentes (n�o considerando os que j� foram contados)
			select	count(distinct nr_sequencia_diluicao) qt_soro
			from	prescr_material
			where	nr_prescricao = nr_prescricao_p
			and	ie_agrupador in (3,7,9)
			and	nr_sequencia_diluicao is not null
			and	obter_se_contido(nr_sequencia_diluicao, ds_lista_w) = 'N'
			and	ds_lista_w is not null
			union all
			--Quantidade de medicamentos que possuem algum composto (n�o considerando os que j� foram contados)
			select	count(distinct a.nr_agrupamento) qt_soro
			from	prescr_material a
			where	a.nr_prescricao = nr_prescricao_p
			and	a.ie_agrupador	= 1
			and	a.nr_agrupamento is not null
			and	obter_se_contido(a.nr_sequencia, ds_lista_w) = 'N'
			and	ds_lista_w is not null
			and	not exists (	select	1 
						from	prescr_material b,
							prescr_material d
						where	b.nr_prescricao = a.nr_prescricao
						and	b.nr_prescricao	= d.nr_prescricao
						and	a.nr_prescricao	= d.nr_prescricao
						and	d.nr_sequencia_diluicao = b.nr_sequencia
						and	b.ie_agrupador	= 1
						and	d.ie_agrupador	in (3,7,9)
						and	a.nr_agrupamento = b.nr_agrupamento)
			and	exists	(	select	1
						from	prescr_material c
						where	a.nr_prescricao	= c.nr_prescricao
						and	a.nr_agrupamento = c.nr_agrupamento
						and	a.ie_agrupador	= 1
						and	obter_se_contido(a.nr_sequencia, ds_lista_w) = 'N'						
						and	c.nr_sequencia	<> a.nr_sequencia)
			and	not exists (	select	1
						from	prescr_material c
						where	a.nr_prescricao	= c.nr_prescricao
						and	a.nr_agrupamento = c.nr_agrupamento
						and	a.ie_agrupador	= 1
						and	obter_se_contido(a.nr_sequencia, ds_lista_w) = 'S'));
	
	if	(qt_soro_w > 0) and
		(qt_soro_w	>= qt_min_w) and
		(qt_soro_w	<= qt_max_w) then
		
		--Cursor com os itens do kit
		OPEN C03;
		LOOP
		FETCH C03 into
			cd_material_kit_w,
			qt_material_kit_w,
			cd_unidade_medida_w,
			ie_agrupador_w,
			cd_intervalo_kit_w,
			ie_entra_conta_w,
			cd_motivo_baixa_w,
			ie_permite_alterar_w,
			ie_via_aplicacao_w;
		exit 	when c03%notfound;
		
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	prescr_material
			where	nr_prescricao = nr_prescricao_p;

			if	(ie_agrupador_w <> 1) then
				ie_agrupador_w := 1;
			else
				ie_agrupador_w := 2;
			end if;

			if	(cd_intervalo_kit_w is not null) then
				qt_material_w := qt_material_kit_w;

				hr_prim_horario_w	:= obter_primeiro_horario(cd_intervalo_kit_w, nr_prescricao_p,cd_material_kit_w,ie_via_aplicacao_w);

				calcular_horario_prescricao(nr_prescricao_p, cd_intervalo_kit_w, dt_primeiro_horario_w, to_date(to_char(dt_primeiro_horario_w,'dd/mm/yyyy') || ' ' || hr_prim_horario_w,'dd/mm/yyyy hh24:mi'), nr_horas_validade_w, cd_material_kit_w, 0,0, nr_intervalo_w, ds_horarios_kit_w, ds_horarios_kit_ww, 'N', null);
				ds_horarios_kit_w := ds_horarios_kit_w || ds_horarios_kit_ww;
			else
				qt_material_w 		:= qt_material_kit_w;
				nr_intervalo_w		:= null;
				ds_horarios_kit_w	:= null;
			end if;

			/*
			Inserindo os itens do kit, mas sem v�nculo com kit e nem com medicamento da prescri��o,
			pois o kit n�o est� sendo derivado de apenas um medicamento. Nestes casos ser� utilizada a integridade com a regra (NR_SEQ_SORO)
			*/
			insert into prescr_material (
				nr_prescricao,
				nr_sequencia,
				ie_origem_inf,
				cd_material,
				cd_unidade_medida,
				qt_dose,
				qt_unitaria,
				qt_material,
				dt_atualizacao,
				nm_usuario,
				cd_intervalo,
				nr_ocorrencia,
				qt_total_dispensar,
				ie_suspenso,
				cd_unidade_medida_dose,
				ie_medicacao_paciente,
				cd_motivo_baixa,
				ie_agrupador,
				ie_bomba_infusao,
				ie_aplic_bolus,
				ie_aplic_lenta,
				ds_horarios,
				ie_recons_diluente_fixo,
				ie_sem_aprazamento,
				IE_COBRA_PACIENTE,
				ie_permite_alterar,
				hr_prim_horario,
				ie_via_aplicacao,
				nr_seq_soro,
				cd_kit_material)
			Values(
				nr_prescricao_p,
				nr_sequencia_w,
				'K',
				cd_material_kit_w,
				cd_unidade_medida_w,
				qt_material_kit_w,
				nvl(qt_material_kit_w,1),
				qt_material_w,
				sysdate,
				nm_usuario_p,
				cd_intervalo_kit_w,
				nvl(nr_intervalo_w,1),
				qt_material_w,
				'N',
				cd_unidade_medida_w,
				'N',
				cd_motivo_baixa_w,
				ie_agrupador_w,
				'N',
				'N',
				'N',
				ds_horarios_kit_w,
				'N',
				'N',
				decode(ie_utiliza_cobra_kit_w, 'S', ie_entra_conta_w, null),
				ie_permite_alterar_w,
				hr_prim_horario_w,
				ie_via_aplicacao_w,
				nr_seq_regra_w,
				cd_kit_material_w);
			
			if (ie_prescr_mat_sem_lib_w = 'S') then
				Gerar_prescr_mat_sem_dt_lib(nr_prescricao_p,nr_sequencia_w,cd_perfil_p,'N',nm_usuario_p,null);
			end if;	

			select	cd_material,
				cd_intervalo,
				ie_via_aplicacao,
				qt_unitaria,
				qt_material,
				qt_dose_especial,
				nr_ocorrencia,
				ds_dose_diferenciada,
				ie_origem_inf,
				cd_unidade_medida_dose,
				qt_dias_util,
				nvl(ie_se_necessario,'N'),
				nvl(ie_acm,'N')
			into	cd_material_disp_w,
				cd_intervalo_disp_w,
				ie_via_aplicacao_disp_w,
				qt_unitaria_disp_w,
				qt_material_disp_w,
				qt_dose_especial_disp_w,
				nr_ocorrencia_disp_w,
				ds_dose_diferenciada_disp_w,
				ie_origem_inf_disp_w,
				cd_unidade_medida_dose_disp_w,
				qt_dias_util_disp_w,
				ie_se_necessario_w,
				ie_acm_w
			from	prescr_material
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;

			obter_quant_dispensar (
				cd_estabelecimento_w,
				cd_material_disp_w,
				nr_prescricao_p,
				nr_sequencia_w,
				cd_intervalo_disp_w,
				ie_via_aplicacao_disp_w,
				qt_unitaria_disp_w,
				qt_dose_especial_disp_w,
				nr_ocorrencia_disp_w,
				ds_dose_diferenciada_disp_w,
				ie_origem_inf_disp_w,
				cd_unidade_medida_dose_disp_w,
				qt_dias_util_disp_w,
				qt_material_disp_w,
				qt_total_dispensar_w,
				ie_regra_disp_w,
				ds_erro_disp_w,
				ie_se_necessario_w,
				ie_acm_w);

			update	prescr_material
			set	qt_material		= qt_material_disp_w,
				qt_total_dispensar	= qt_total_dispensar_w,
				ie_regra_disp		= decode(nvl(ie_regra_disp,'X'), 'D', ie_regra_disp, ie_regra_disp_w)
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;

		END LOOP;
		CLOSE C03;
		COMMIT;
		
		exit;
		
	end if;
	
	end;
end loop;
close C01;

end Gerar_kit_solucao;
/