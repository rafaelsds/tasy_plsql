create or replace
procedure hsfa_duplicar_plano_ans is

begin

insert into ctb_plano_ans (	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	cd_plano,
	cd_classificacao,
	ds_conta,
	ie_tipo,
	cd_empresa,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_versao)
(select	ctb_plano_ans_seq.nextval,
	sysdate,
	'CopiaPlanoANS',
	a.cd_plano,
	a.cd_classificacao,
	a.ds_conta,
	a.ie_tipo,
	a.cd_empresa,
	sysdate,
	'CopiaPlanoANS',
	2
from	ctb_plano_ans a
where	a.ie_versao	= 0);
commit;

end hsfa_duplicar_plano_ans;
/