create or replace
procedure GERAR_HSM_PROT_CONV_RET
			(nm_usuario_p		varchar2,
			dt_inicial_p		date,
			dt_final_p		date,
			ie_tipo_data_p		varchar2,
			nr_seq_protocolo_p	number) is

nr_seq_protocolo_w	number(10);
vl_protocolo_w		number(15,2);	
dt_emissao_w		date;
dt_entrega_convenio_w	date;
dt_recebimento_w	date;
count_w			number(10);	
nr_titulo_w		number(10);
nr_seq_retorno_w	number(10);
nr_seq_nf_w		number(10);	
vl_recebido_w		number(15,2);
vl_rec_recurso_w	number(15,2);
cd_convenio_w		number(10);
vl_recursado_w		number(15,2);
vl_glosa_w		number(15,2);
vl_glosa_aceita_grg_w	number(15,2);
vl_glosa_aceita_ret_w	number(15,2);
vl_glosa_aceita_w	number(15,2);
vl_recebido_prot_w	number(15,2) := 0;
vl_soma_receb_prot_w	number(15,2) := 0;
vl_rec_recurso_prot_w	number(15,2) := 0;
vl_recursado_prot_w	number(15,2) := 0;
vl_glosa_prot_w		number(15,2) := 0;
vl_glosa_aceita_prot_w	number(15,2) := 0;
vl_saldo_protocolo_w	number(15,2) := 0;
vl_adicional_w		number(15,2) := 0;
vl_adicional_rec_w	number(15,2) := 0;
vl_adicional_prot_w	number(15,2) := 0;
vl_adicional_rec_prot_w	number(15,2) := 0;
dt_rec_recurso_w	date;
nr_sequencia_w		convenio_receb.nr_sequencia%type;

			
cursor c01 is
select	a.nr_seq_protocolo,
	obter_total_protocolo(a.nr_seq_protocolo),
	e.dt_emissao,
	a.dt_entrega_convenio,
	obter_dados_titulo_receber(d.nr_titulo,'NF'),
	a.cd_convenio,
	max(f.dt_recebimento)
from	protocolo_convenio a,
	conta_paciente b,
	convenio_retorno c,
	convenio_retorno_item d,
	titulo_receber e,
	convenio_receb f,
	convenio_ret_receb g
where	a.nr_seq_protocolo		= b.nr_seq_protocolo
and	b.nr_interno_conta		= d.nr_interno_conta
and	d.nr_seq_retorno		= c.nr_sequencia
and	a.nr_seq_protocolo		= e.nr_seq_protocolo
and	f.nr_sequencia			= g.nr_seq_receb
and	g.nr_seq_retorno		= c.nr_sequencia
and	(a.nr_seq_protocolo		= nr_seq_protocolo_p or '0' = nvl(nr_seq_protocolo_p,'0'))
and	(('E' = ie_tipo_data_p) and	
	 (e.dt_emissao 		between dt_inicial_p and	fim_dia(dt_final_p)))	
group by a.nr_seq_protocolo,
	obter_total_protocolo(a.nr_seq_protocolo),
	e.dt_emissao,
	a.dt_entrega_convenio,
	obter_dados_titulo_receber(d.nr_titulo,'NF'),
	a.cd_convenio
union
select	a.nr_seq_protocolo,
	obter_total_protocolo(a.nr_seq_protocolo),
	e.dt_emissao,
	a.dt_entrega_convenio,
	obter_dados_titulo_receber(d.nr_titulo,'NF'),
	a.cd_convenio,
	max(f.dt_recebimento)
from	protocolo_convenio a,
	conta_paciente b,
	convenio_retorno c,
	convenio_retorno_item d,
	titulo_receber e,
	convenio_receb f,
	convenio_ret_receb g
where	a.nr_seq_protocolo		= b.nr_seq_protocolo
and	b.nr_interno_conta		= d.nr_interno_conta
and	d.nr_seq_retorno		= c.nr_sequencia
and	a.nr_seq_protocolo		= e.nr_seq_protocolo
and	f.nr_sequencia			= g.nr_seq_receb
and	g.nr_seq_retorno		= c.nr_sequencia
and	(a.nr_seq_protocolo		= nr_seq_protocolo_p or '0' = nvl(nr_seq_protocolo_p,'0'))
and	(('C' = ie_tipo_data_p) and	
	 (a.dt_entrega_convenio between dt_inicial_p and	fim_dia(dt_final_p)))	 
group by a.nr_seq_protocolo,
	obter_total_protocolo(a.nr_seq_protocolo),
	e.dt_emissao,
	a.dt_entrega_convenio,
	obter_dados_titulo_receber(d.nr_titulo,'NF'),
	a.cd_convenio;

cursor c02 is
select	a.nr_sequencia nr_seq_retorno
from	convenio_retorno a,
	convenio_retorno_item b,
	conta_paciente c,
	titulo_receber d
where	a.nr_sequencia		= b.nr_seq_retorno
and	b.nr_interno_conta	= c.nr_interno_conta
and	c.nr_seq_protocolo	= d.nr_seq_protocolo
--and	a.ie_status_retorno	= 'F'
and	c.nr_seq_protocolo	= nr_seq_protocolo_w
group by a.nr_sequencia
order by nr_seq_retorno;

/*Protocolo de contas sem retorno*/
cursor c03 is
select	b.nr_seq_protocolo,
	sum(c.vl_procedimento + c.vl_material),	
	b.dt_entrega_convenio,	
	b.cd_convenio,
	null dt_emissao
from	conta_Paciente_Resumo c,
	protocolo_convenio b,
	conta_paciente a
	--titulo_receber c
where 	a.ie_status_acerto 		=  2
and     b.ie_status_protocolo		=  2
and 	a.nr_seq_protocolo		= b.nr_seq_protocolo
and  	a.nr_interno_conta  		= c.nr_interno_conta(+)
--and	b.nr_seq_protocolo		= c.nr_seq_protocolo
and	(b.nr_seq_protocolo		= nr_seq_protocolo_p or '0' = nvl(nr_seq_protocolo_p,'0'))
and	(('C' = ie_tipo_data_p) and	
	 (b.dt_entrega_convenio	between dt_inicial_p and fim_dia(dt_final_p)))	
and	not exists
	(select	1
	from	convenio_retorno z,
		convenio_retorno_item x,
		conta_paciente y
	where	x.nr_interno_conta	= y.nr_interno_conta
	and	x.nr_seq_retorno	= z.nr_sequencia
	and	z.ie_status_retorno	in ('F','V')
	and	y.nr_seq_protocolo	= a.nr_seq_protocolo)
and	not exists
	(select 1
	from	hsm_protocolo_convenio_ret z
	where	z.nr_seq_protocolo	= b.nr_seq_protocolo)	
group by 	b.nr_seq_protocolo,	
	b.dt_entrega_convenio,	
	b.cd_convenio
	--c.dt_emissao;
union
select	b.nr_seq_protocolo,
	sum(c.vl_procedimento + c.vl_material),	
	b.dt_entrega_convenio,	
	b.cd_convenio,
	null dt_emissao
from	conta_Paciente_Resumo c,
	protocolo_convenio b,
	conta_paciente a,
	titulo_receber d
where 	a.ie_status_acerto 		=  2
and     b.ie_status_protocolo		=  2
and 	a.nr_seq_protocolo		= b.nr_seq_protocolo
and  	a.nr_interno_conta  		= c.nr_interno_conta(+)
and	b.nr_seq_protocolo		= d.nr_seq_protocolo
and	(b.nr_seq_protocolo		= nr_seq_protocolo_p or '0' = nvl(nr_seq_protocolo_p,'0'))
and	(('E' = ie_tipo_data_p) and	
	 (d.dt_emissao 		between dt_inicial_p and	fim_dia(dt_final_p)))	
and	not exists
	(select	1
	from	convenio_retorno z,
		convenio_retorno_item x,
		conta_paciente y
	where	x.nr_interno_conta	= y.nr_interno_conta
	and	x.nr_seq_retorno	= z.nr_sequencia
	and	z.ie_status_retorno	in ('F','V')
	and	y.nr_seq_protocolo	= a.nr_seq_protocolo)
and	not exists
	(select 1
	from	hsm_protocolo_convenio_ret z
	where	z.nr_seq_protocolo	= b.nr_seq_protocolo)	
group by 	b.nr_seq_protocolo,	
	b.dt_entrega_convenio,	
	b.cd_convenio;
	
/*Protocolo com retornos n�o fechados*/	
cursor c04 is
select	b.nr_seq_protocolo,
	sum(g.vl_procedimento + g.vl_material),	
	b.dt_entrega_convenio,	
	b.cd_convenio,
	c.dt_emissao
from	conta_Paciente_Resumo g,
	protocolo_convenio b,
	conta_paciente a,
	titulo_receber c,
	convenio_retorno_item d,
	convenio_retorno e
where 	a.ie_status_acerto 		=  2
and     b.ie_status_protocolo		=  2
and 	a.nr_seq_protocolo		= b.nr_seq_protocolo
and	b.nr_seq_protocolo		= c.nr_seq_protocolo
and	d.nr_interno_conta		= a.nr_interno_conta
and  	a.nr_interno_conta  		= g.nr_interno_conta
and	(b.nr_seq_protocolo		= nr_seq_protocolo_p or '0' = nvl(nr_seq_protocolo_p,'0'))
and	d.nr_seq_retorno		= e.nr_sequencia
and	e.ie_status_retorno		not in ('F','V')
and 	b.dt_entrega_convenio    	between dt_inicial_p and fim_dia(dt_final_p)
and	not exists
	(select 1
	from	hsm_protocolo_convenio_ret x
	where	x.nr_seq_protocolo	= b.nr_seq_protocolo)
group by 	b.nr_seq_protocolo,	
	b.dt_entrega_convenio,	
	b.cd_convenio,
	c.dt_emissao;
			
begin

delete	from hsm_protocolo_convenio_ret;

open C01;
loop
fetch C01 into	
	nr_seq_protocolo_w,
	vl_protocolo_w,
	dt_emissao_w,
	dt_entrega_convenio_w,
	nr_seq_nf_w,
	cd_convenio_w,
	dt_recebimento_w;
exit when C01%notfound;
	begin
	
	vl_recebido_w	:= 0;	
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_retorno_w;
	exit when C02%notfound;
		begin
		
		select	nvl(sum(a.vl_pago),0),
			nvl(sum(a.vl_adicional),0)
		into	vl_recebido_w,
			vl_adicional_w
		from	convenio_retorno_item a,
			conta_paciente b
		where	a.nr_seq_retorno	= nr_seq_retorno_w
		and	a.nr_interno_conta	= b.nr_interno_conta
		and	b.nr_seq_protocolo	= nr_seq_protocolo_w
		and	not exists
			(select	1
			from	convenio_retorno_item b
			where	b.nr_interno_conta	= a.nr_interno_conta
			and	b.cd_autorizacao	= a.cd_autorizacao
			and	b.nr_seq_retorno	< nr_seq_retorno_w);	

		select	nvl(sum(a.vl_pago),0),
			nvl(sum(a.vl_adicional),0),
			max(b.dt_recebimento)
		into	vl_rec_recurso_w,
			vl_adicional_rec_w,
			dt_rec_recurso_w
		from	convenio_retorno_item a,
			conta_paciente c,
			titulo_receber_liq b
		where	a.nr_sequencia		= b.nr_seq_ret_item
		and	a.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_w
		and	a.nr_titulo		= b.nr_titulo
		and	a.nr_seq_retorno	= nr_seq_retorno_w
		and	exists
			(select	1
			from	convenio_retorno_item b
			where	b.nr_interno_conta	= a.nr_interno_conta
			and	b.cd_autorizacao	= a.cd_autorizacao
			and	b.nr_seq_retorno	< nr_seq_retorno_w);
			
		select	nvl(sum(obter_valores_guia_grc(a.nr_seq_lote_hist,a.nr_interno_conta,a.cd_autorizacao,'VA')),0)
		into	vl_recursado_w
		from	lote_audit_hist_guia a,
			conta_paciente b,
			protocolo_convenio c
		where	a.nr_interno_conta 	= b.nr_interno_conta
		and	c.nr_seq_protocolo	= b.nr_Seq_protocolo
		and	c.nr_Seq_protocolo	= nr_seq_protocolo_w		
		and	a.nr_Seq_retorno	= nr_seq_retorno_w;
		
		select	--nvl(sum(d.vl_glosado),0) +
			nvl(sum(d.vl_amenor),0)
		into	vl_glosa_w
		from	convenio_retorno_item d,
			conta_paciente c
		where	d.nr_seq_retorno	= nr_seq_retorno_w
		and	d.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_w;
		--and	d.ie_glosa 		<> 'S';
		
		select	nvl(sum(d.vl_glosado),0) +
			nvl(sum(d.vl_amenor),0)
		into	vl_glosa_aceita_ret_w
		from	convenio_retorno_item d,
			conta_paciente c
		where	d.nr_seq_retorno	= nr_seq_retorno_w
		and	d.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_w
		and	d.ie_glosa 		= 'S';
		
		select	nvl(sum(obter_valores_guia_grc(x.nr_seq_lote_hist,x.nr_interno_conta,x.cd_autorizacao,'VG')),0)
		into	vl_glosa_aceita_grg_w
		from    lote_audit_hist_guia x,
			conta_paciente y
		where   x.nr_seq_retorno        = nr_seq_retorno_w
		and	y.nr_interno_conta	= x.nr_interno_conta
		and	y.nr_seq_protocolo	= nr_seq_protocolo_w;

		vl_glosa_aceita_w 	:= vl_glosa_aceita_ret_w + vl_glosa_aceita_grg_w;
		
		select	max(a.dt_recebimento)
		into	dt_recebimento_w
		from	convenio_receb a,
			convenio_ret_receb b
		where	b.nr_seq_receb	 = a.nr_sequencia
		and	b.nr_seq_retorno = nr_seq_retorno_w;
		
		--vl_recebido_w		:= vl_recebido_w + vl_adicional_w;
		--vl_rec_recurso_w	:= vl_rec_recurso_w + vl_adicional_rec_w;
		
		insert into hsm_protocolo_convenio_ret
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			vl_protocolo,
			dt_emissao,
			dt_entrega,
			nr_seq_retorno,
			nr_seq_nf,
			cd_convenio,
			vl_recebido,
			vl_rec_recurso,
			vl_recursado,
			vl_glosa,
			vl_glosa_aceita,
			vl_adicional,
			vl_adicional_rec,
			dt_rec_recurso,
			dt_recebimento,
			ie_ordem)
		values	(hsm_protocolo_convenio_ret_seq.nextval,
			nm_usuario_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_w,
			vl_protocolo_w,
			dt_emissao_w,
			dt_entrega_convenio_w,
			nr_seq_retorno_w,
			nr_seq_nf_w,
			cd_convenio_w,
			vl_recebido_w,
			vl_rec_recurso_w,
			vl_recursado_w,
			vl_glosa_w,
			vl_glosa_aceita_w,
			vl_adicional_w,
			vl_adicional_rec_w,
			dt_rec_recurso_w,
			dt_recebimento_w,
			1);		
		
		end;
	end loop;
	close C02;
	
	select	nvl(sum(vl_recebido),0),
		nvl(sum(vl_rec_recurso),0),
		nvl(sum(vl_recursado),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_glosa_aceita),0),
		nvl(sum(vl_adicional),0),
		nvl(sum(vl_adicional_rec),0)
	into	vl_recebido_prot_w,
		vl_rec_recurso_prot_w,
		vl_recursado_prot_w,
		vl_glosa_prot_w,
		vl_glosa_aceita_prot_w,
		vl_adicional_prot_w,
		vl_adicional_rec_prot_w
	from	hsm_protocolo_convenio_ret
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	
	select	count(*)
	into	count_w
	from	hsm_protocolo_convenio_ret
	where	nr_seq_protocolo = nr_seq_protocolo_w;
	
	/*
	if	(vl_rec_recurso_prot_w <> 0) then
		vl_saldo_protocolo_w	:= vl_protocolo_w - vl_recebido_prot_w - vl_glosa_aceita_prot_w - vl_rec_recurso_prot_w;-- + vl_adicional_rec_prot_w;
	elsif	(vl_recursado_prot_w <> 0) then
		vl_saldo_protocolo_w 	:= vl_recursado_prot_w;	
	else
		vl_soma_receb_prot_w	:= vl_recebido_prot_w + vl_rec_recurso_prot_w + vl_glosa_aceita_prot_w + vl_glosa_prot_w;
		if	(vl_soma_receb_prot_w > vl_protocolo_w) then
			vl_saldo_protocolo_w 	:= 0;
		else
			vl_saldo_protocolo_w 	:= vl_protocolo_w - vl_soma_receb_prot_w;
		end if;			
	end if;	
	*/
	
	vl_saldo_protocolo_w	:= vl_protocolo_w - vl_recebido_prot_w - vl_glosa_prot_w + vl_recursado_prot_w - vl_rec_recurso_prot_w;
	
	update	hsm_protocolo_convenio_ret
	set	vl_saldo_protocolo	= vl_saldo_protocolo_w
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	
	end;
end loop;
close C01;

open C03;
loop
fetch C03 into	
	nr_seq_protocolo_w,
	vl_protocolo_w,	
	dt_entrega_convenio_w,	
	cd_convenio_w,
	dt_emissao_w;
exit when C03%notfound;
	begin
	
	insert into hsm_protocolo_convenio_ret
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_protocolo,
		vl_protocolo,
		dt_emissao,
		dt_entrega,
		nr_seq_retorno,
		nr_seq_nf,
		cd_convenio,
		vl_recebido,
		vl_rec_recurso,
		vl_recursado,
		vl_glosa,
		vl_glosa_aceita,
		dt_rec_recurso,
		dt_recebimento,
		vl_saldo_protocolo,
		ie_ordem)
	values	(hsm_protocolo_convenio_ret_seq.nextval,
		nm_usuario_p,
		sysdate,
		sysdate,
		nm_usuario_p,
		nr_seq_protocolo_w,
		vl_protocolo_w,
		dt_emissao_w,
		dt_entrega_convenio_w,
		null,
		null,
		cd_convenio_w,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		vl_protocolo_w,
		3);
	
	
	
	end;
end loop;
close C03;

open C04;
loop
fetch C04 into	
	nr_seq_protocolo_w,
	vl_protocolo_w,	
	dt_entrega_convenio_w,	
	cd_convenio_w,
	dt_emissao_w;
exit when C04%notfound;
	begin
		
	vl_recebido_w	:= 0;	
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_retorno_w;
	exit when C02%notfound;
		begin
		
		select	nvl(sum(a.vl_pago),0) + nvl(sum(a.vl_adicional),0)
		into	vl_recebido_w
		from	convenio_retorno_item a,
			conta_paciente b
		where	a.nr_seq_retorno	= nr_seq_retorno_w
		and	a.nr_interno_conta	= b.nr_interno_conta
		and	b.nr_seq_protocolo	= nr_seq_protocolo_w
		and	not exists
			(select	1
			from	convenio_retorno_item b
			where	b.nr_interno_conta	= a.nr_interno_conta
			and	b.cd_autorizacao	= a.cd_autorizacao
			and	b.nr_seq_retorno	< nr_seq_retorno_w);	

		select	nvl(sum(a.vl_pago),0) + nvl(sum(a.vl_adicional),0),
			max(b.dt_recebimento)
		into	vl_rec_recurso_w,
			dt_rec_recurso_w
		from	convenio_retorno_item a,
			conta_paciente c,
			titulo_receber_liq b
		where	a.nr_sequencia		= b.nr_seq_ret_item
		and	a.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_w
		and	a.nr_titulo		= b.nr_titulo
		and	a.nr_seq_retorno	= nr_seq_retorno_w
		and	exists
			(select	1
			from	convenio_retorno_item b
			where	b.nr_interno_conta	= a.nr_interno_conta
			and	b.cd_autorizacao	= a.cd_autorizacao
			and	b.nr_seq_retorno	< nr_seq_retorno_w);
			
		select	nvl(sum(obter_valores_guia_grc(a.nr_seq_lote_hist,a.nr_interno_conta,a.cd_autorizacao,'VA')),0)
		into	vl_recursado_w
		from	lote_audit_hist_guia a,
			conta_paciente b,
			protocolo_convenio c
		where	a.nr_interno_conta 	= b.nr_interno_conta
		and	c.nr_seq_protocolo	= b.nr_Seq_protocolo
		and	c.nr_Seq_protocolo	= nr_seq_protocolo_w		
		and	a.nr_Seq_retorno	= nr_seq_retorno_w;
		
		select	--nvl(sum(d.vl_glosado),0) +
			nvl(sum(d.vl_amenor),0)
		into	vl_glosa_w
		from	convenio_retorno_item d,
			conta_paciente c
		where	d.nr_seq_retorno	= nr_seq_retorno_w
		and	d.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_w;
		--and	d.ie_glosa 		<> 'S';
		
		select	--nvl(sum(d.vl_glosado),0) +
			nvl(sum(d.vl_amenor),0)
		into	vl_glosa_aceita_ret_w
		from	convenio_retorno_item d,
			conta_paciente c
		where	d.nr_seq_retorno	= nr_seq_retorno_w
		and	d.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_w
		and	d.ie_glosa 		= 'S';
		
		select	nvl(sum(obter_valores_guia_grc(x.nr_seq_lote_hist,x.nr_interno_conta,x.cd_autorizacao,'VG')),0)
		into	vl_glosa_aceita_grg_w
		from    lote_audit_hist_guia x,
			conta_paciente y
		where   x.nr_seq_retorno        = nr_seq_retorno_w
		and	y.nr_interno_conta	= x.nr_interno_conta
		and	y.nr_seq_protocolo	= nr_seq_protocolo_w;

		--vl_glosa_aceita_w := vl_glosa_aceita_ret_w + vl_glosa_aceita_grg_w;
		vl_glosa_aceita_w := vl_glosa_aceita_grg_w;
		
		insert into hsm_protocolo_convenio_ret
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			vl_protocolo,
			dt_emissao,
			dt_entrega,
			nr_seq_retorno,
			nr_seq_nf,
			cd_convenio,
			vl_recebido,
			vl_rec_recurso,
			vl_recursado,
			vl_glosa,
			vl_glosa_aceita,
			dt_rec_recurso,
			dt_recebimento,
			ie_ordem)
		values	(hsm_protocolo_convenio_ret_seq.nextval,
			nm_usuario_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_w,
			vl_protocolo_w,
			dt_emissao_w,
			dt_entrega_convenio_w,
			nr_seq_retorno_w,
			nr_seq_nf_w,
			cd_convenio_w,
			vl_recebido_w,
			vl_rec_recurso_w,
			vl_recursado_w,
			vl_glosa_w,
			vl_glosa_aceita_w,
			dt_rec_recurso_w,
			dt_recebimento_w,
			2);		
		
		end;
	end loop;
	close C02;
	
--	update	hsm_protocolo_convenio_ret
--	set	vl_saldo_protocolo	= vl_protocolo_w
--	where	nr_seq_protocolo	= nr_seq_protocolo_w;	
	
	end;
end loop;
close C04;

commit;

end;
/