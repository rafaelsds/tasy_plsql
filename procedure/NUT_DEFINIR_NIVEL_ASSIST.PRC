create or replace
procedure nut_definir_nivel_assist(	nr_atendimento_p		number,
					nr_seq_nivel_assist_p		number,
					nr_seq_classificacao_pac_p	number,
					nm_usuario_p			Varchar2) is 

nr_seq_dados_pac_w		number(10);	

nr_seq_nivel_assist_w		number(10);
nr_seq_classificacao_pac_w	number(10);
	
begin
if (nr_atendimento_p is not null) then

	select	max(nr_sequencia)
	into	nr_seq_dados_pac_w
	from	nut_dados_paciente
	where	nr_atendimento = nr_atendimento_p;

	if (nr_seq_dados_pac_w is null) then
		
		insert	into	nut_dados_paciente(	nr_sequencia,
							dt_atualizacao,
							dt_Atualizacao_nrec,
							nm_usuario,
							nm_usuario_nrec,
							nr_atendimento,
							nr_seq_nivel_assistencial,
							nr_seq_classificacao_pac)
					values	(	nut_dados_paciente_seq.NextVal,
							sysdate,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							nr_atendimento_p,
							nr_seq_nivel_assist_p,
							nr_seq_classificacao_pac_p);
	else
		select	max(nr_seq_nivel_assistencial),
			max(nr_seq_classificacao_pac)
		into	nr_seq_nivel_assist_w,
			nr_seq_classificacao_pac_w
		from	nut_dados_paciente
		where	nr_sequencia = nr_seq_dados_pac_w;

		update	nut_dados_paciente
		set	nr_seq_nivel_assistencial = nvl(nr_seq_nivel_assist_p,nr_seq_nivel_assistencial),
			nr_seq_classificacao_pac = nvl(nr_seq_classificacao_pac_p,nr_seq_classificacao_pac)
		where	nr_sequencia = nr_seq_dados_pac_w;

		if (nr_seq_nivel_assist_p is not null) then
			nut_atualiza_dados_pac_hist(nr_seq_nivel_assist_p,nr_seq_nivel_assist_w,null,null,nm_usuario_p);
		else
			nut_atualiza_dados_pac_hist(null,null,nr_seq_classificacao_pac_p,nr_seq_classificacao_pac_w,nm_usuario_p);
		end if;

	end if;
end if;

commit;

end nut_definir_nivel_assist;
/