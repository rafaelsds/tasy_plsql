CREATE OR REPLACE 
PROCEDURE HD_PACIENTE_INT_EXT_FINALIZA(dt_verificacao_p DATE     DEFAULT SYSDATE,
                                       nr_periodo_p     NUMBER   DEFAULT 30,
                                       nm_usuario       VARCHAR2 DEFAULT 'TASY') IS

    CURSOR c01 IS
        SELECT pi.cd_pessoa_fisica
          FROM hd_paciente_int_ext pi,
               hd_escala_dialise   hed
         WHERE pi.cd_pessoa_fisica = hed.cd_pessoa_fisica
           AND hed.dt_fim IS NULL
           AND pi.dt_retorno IS NULL
           AND pi.dt_internacao <= trunc(dt_verificacao_p - nr_periodo_p);
BEGIN
    wheb_usuario_pck.set_nm_usuario(nm_usuario);
    FOR i IN c01 LOOP
        hd_encerrar_escalas_tratamento(cd_pessoa_fisica_p => i.cd_pessoa_fisica);
    END LOOP;
END;
/
