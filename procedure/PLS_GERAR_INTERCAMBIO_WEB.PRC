create or replace
procedure pls_gerar_intercambio_web
			(	nr_seq_guia_p				Number,
				nr_seq_req_p				Number,
				nm_usuario_p				Varchar2,
				cd_estabelecimento_p			Number,
				ds_transacao_p				Varchar2,
				ds_observacao_p				Varchar2,
				nr_seq_execucao_p		out	Number,
				ds_diretorio_p			out	Varchar2,
				ie_tipo_transacao_p		out	Varchar2,
				ds_arquivo_p			out	Varchar2) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Gerar guia de intercâmbio no portal web
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relatórios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de atenção:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Retorno
I - Intercâmbio
N - Normal

*/				

cd_usuario_plano_w		Varchar2(30);
nr_via_w			Varchar2(5);
nr_seq_segurado_w		Number(10);
dt_validade_w			Varchar2(20);
cd_pessoa_fisica_w		Varchar2(10);
cd_cooperativa_w		Varchar2(10) := '';
cd_operadora_usuario_w		Varchar2(10);
nr_seq_emissor_w		Number(10);
ie_tipo_transacao_w		Varchar2(10) := 'N';
ds_diretorio_w			Varchar2(255);
ds_hora_w			Varchar2(15);
ie_tipo_trans_w			Varchar2(4);
nr_seq_execucao_w		Number(10);
nr_seq_origem_w			Number(10);


begin

if	(nr_seq_guia_p is not null) then
	select	substr(pls_obter_dados_segurado(nr_seq_segurado,'CR'),1,255),
		replace(to_char(sysdate, 'hh24:mi:ss'), ':')
	into	cd_usuario_plano_w,
		ds_hora_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;

	ie_tipo_trans_w	:= '01';

elsif	(nr_seq_req_p is not null) then
	pls_finalizar_requisicao_web(nr_seq_req_p, nm_usuario_p);
	
	select	substr(pls_obter_dados_segurado(nr_seq_segurado,'CR'),1,255),
		replace(to_char(sysdate, 'hh24:mi:ss'), ':')
	into	cd_usuario_plano_w,
		ds_hora_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_req_p;

	ie_tipo_trans_w	:= '04';
end if;

if	(cd_usuario_plano_w is not null) then
	/*pls_obter_dados_codigo_barras(cd_usuario_plano_w, nm_beneficiario_w, nr_via_w, nr_seq_segurado_w, dt_validade_w, nr_digito_w, cd_pessoa_fisica_w, cd_estabelecimento_p, nm_usuario_p);*/

	select	max(nr_seq_emissor)
	into	nr_seq_emissor_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_p;

	select	pls_obter_campos_carteira(cd_usuario_plano_w,nr_seq_emissor_w,'CM')
	into	cd_cooperativa_w
	from	dual;

	cd_operadora_usuario_w := nvl(pls_obter_unimed_estab(cd_estabelecimento_p), '0');

	if	(to_number(cd_operadora_usuario_w) <> to_number(cd_cooperativa_w)) then
		ie_tipo_transacao_w  		:=  	'I';

		select 	max(nvl(ds_diretorio_saida_web, ds_diretorio_saida_cli)) ds_diretorio
		into	ds_diretorio_w
		from 	pls_parametros_scs;

		/*  Pedido autorização 00500  - PTU 3.5
		     Pedido audotiração 00600 - PTU 4.0
		     Cancelamento 00211 - PTU 3.5
		     Cancelamento 00311 - PTU 4.0
		     Resposta auditoria 00404 - PTU 4.0
		     Pedido de Complementação de Autorização  00605 - PTU 4.0/PTU 5.0
		*/
		if	(ds_transacao_p = '00600' ) then
			ptu_gestao_envio_pedido_autor(nr_seq_req_p,nr_seq_guia_p, cd_estabelecimento_p,nm_usuario_p);
			--ptu_gerar_pedido_autorizacao(nr_seq_req_p,nr_seq_guia_p, cd_estabelecimento_p,nm_usuario_p);
		elsif 	(ds_transacao_p = '00311' ) then
			ptu_gestao_envio_cancelamento(nr_seq_guia_p,nr_seq_req_p, null, ds_observacao_p,  nr_seq_execucao_w, nm_usuario_p);
			--ptu_gerar_cancelamento(nr_seq_guia_p,nr_seq_req_p, null,  nr_seq_execucao_w, nm_usuario_p);
			nr_seq_execucao_p  := nr_seq_execucao_w;
		elsif 	(ds_transacao_p = '00404' ) then 
			ptu_gestao_env_resp_auditoria (	null, nr_seq_req_p, null, 
							null, cd_estabelecimento_p, nm_usuario_p, 
							nr_seq_origem_w);
		
			pls_valid_regr_incons_ptu_web (nr_seq_req_p, nm_usuario_p, cd_estabelecimento_p);
		elsif	(ds_transacao_p = '00605' ) then 
			ptu_gestao_envio_ped_compl_aut(	nr_seq_guia_p, nr_seq_req_p, cd_estabelecimento_p, nm_usuario_p);
		end if;
	end if;
end if;

if	(cd_cooperativa_w is not null and length(cd_cooperativa_w) = 3) then
	cd_cooperativa_w 	:=	'0'||cd_cooperativa_w;
end if;

ie_tipo_transacao_p 		:= 	ie_tipo_transacao_w;
ds_diretorio_p			:= 	ds_diretorio_w;
ds_arquivo_p			:=	cd_cooperativa_w||'_'||ds_transacao_p||'_'||nvl(nr_seq_guia_p,nr_seq_req_p)||ie_tipo_trans_w||ds_hora_w||'.1in';

if	(ds_transacao_p = '00600' and ds_diretorio_w is not null) then
	if	(nr_seq_guia_p is not null ) then
		update  ptu_pedido_autorizacao
		set     ds_arquivo_pedido = ds_arquivo_p
		where   nr_seq_guia       = nr_seq_guia_p;
	elsif	(nr_seq_req_p is not null)then
		update  ptu_pedido_autorizacao
		set     ds_arquivo_pedido = ds_arquivo_p
		where   nr_seq_requisicao = nr_seq_req_p;
	end if;
	commit;
elsif 	(ds_transacao_p = '00311' and ds_diretorio_w is not null) then
	if	(nr_seq_guia_p is not null ) then
		update  ptu_cancelamento
		set     ds_arquivo_pedido = ds_arquivo_p
		where   nr_seq_guia       = nr_seq_guia_p;
	elsif	(nr_seq_req_p is not null)then
		update  ptu_cancelamento
		set     ds_arquivo_pedido = ds_arquivo_p
		where   nr_seq_requisicao = nr_seq_req_p;
	end if;
	commit;
elsif 	(ds_transacao_p = '00605' and ds_diretorio_w is not null) then
	if	(nr_seq_guia_p is not null ) then
		update  ptu_pedido_compl_aut
		set     ds_arquivo_pedido = ds_arquivo_p
		where   nr_seq_guia       = nr_seq_guia_p;
	elsif	(nr_seq_req_p is not null)then
		update  ptu_pedido_compl_aut
		set     ds_arquivo_pedido = ds_arquivo_p
		where   nr_seq_requisicao = nr_seq_req_p;
	end if;
	commit;
end if;

end pls_gerar_intercambio_web;
/
