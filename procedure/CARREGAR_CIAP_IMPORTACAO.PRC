create or replace
procedure carregar_ciap_importacao(	nm_usuario_p		Varchar2,
									nr_seq_versao_p		number,
									ie_somente_proc_p	Varchar2,
									ds_code_p			Varchar2,
									ds_chapter_p		Varchar2,
									ds_component_p		Varchar2,
									ds_preferred_p		Varchar2,
									ds_short_title_p	Varchar2,
									ds_inclusion_p		Varchar2,
									ds_exclusion_p		Varchar2,
									ds_criteria_p		Varchar2,
									ds_consider_p		Varchar2,
									ds_note_p			Varchar2,
									ds_icd10_p			Varchar2,
									ds_icd10_freq_p		Varchar2) is 

			
			
			
ds_capitulo_ciap_w		varchar2(255);
cd_capitulo_w			varchar2(10);
cd_componente_w			varchar2(10);
ds_componente_w			varchar2(255);
ds_cids_possiveis_w		varchar2(4000);
ds_cids_possiveis_w2	varchar2(4000);
vl_pos_w				number(3);
qt_reg_cid_w			number(5);
cd_doenca_freq_w		varchar2(10);

nr_seq_capitulo_w		number(10);
cd_capitulo_proc_w		varchar2(10);
nr_seq_capitulo_proc_w	number(10);
cd_ciap_proc_w			varchar2(10);

nr_seq_componente_w		number(10);
nr_seq_problema_ciap_w	number(10);


Cursor C01 is
	select	nr_sequencia,
			CD_CAPITULO
	from	capitulo_ciap
	where	nvl(ie_situacao,'A') = 'A'
	order by cd_capitulo;

--- In�cio Function
	function obter_capitulo ( ds_codigo_ciap_p	varchar2) return number is	
	
	cd_capitulo_seq_w	number	(10) := 0;	
	ds_codigo			varchar2(5) := '';
	
	
	begin
	
	ds_codigo := substr(ds_codigo_ciap_p,0,1);
	if	(ds_codigo = '-') then
		ds_codigo := '-';
	end if;
	
	select	nvl(max(nr_sequencia),0)
	into	cd_capitulo_seq_w
	from	capitulo_ciap
	where	cd_capitulo = ds_codigo
	and		nvl(ie_situacao,'A') = 'A';
	
		
	return	cd_capitulo_seq_w;
	
	end;
-- Fim Function	

--- In�cio Function
	function obter_comp_cap ( nr_seq_capitulo_p	number, ds_componente_ciap_p	varchar2) return number is	
	
	cd_retorno_w		number(10) := 0;
	
	begin
	
	select 	nvl(max(nr_sequencia),0)
	into	cd_retorno_w
	from	componente_ciap
	where	upper(ds_componente) = upper(ds_componente_ciap_p)
	and		nr_seq_capitulo = nr_seq_capitulo_p
	and		nvl(ie_situacao,'A') = 'A';
		
	return	cd_retorno_w;
	
	end;
-- Fim Function	

begin

select	decode(substr(ds_code_p,0,1),'-','-',substr(ds_code_p,0,1))
into	cd_capitulo_w
from	dual;

ds_cids_possiveis_w	:= ds_icd10_p;
cd_doenca_freq_w	:= ds_icd10_freq_p;
cd_doenca_freq_w	:= replace(cd_doenca_freq_w,'.','');

if ( cd_capitulo_w = '-' )  then

	open C01;
	loop
	fetch C01 into	
		nr_seq_capitulo_proc_w,
		cd_capitulo_proc_w;
	exit when C01%notfound;
		begin
		
		nr_seq_componente_w := 	obter_comp_cap(nr_seq_capitulo_proc_w, ds_component_p);		
		
		if	( nr_seq_componente_w = 0) then
		
			Select 	componente_ciap_seq.nextval
			into	nr_seq_componente_w
			from 	dual;

			select 	nvl(max(cd_componente_ciap),0)
			into	cd_componente_w
			from	componente_ciap
			where	nr_seq_capitulo = nr_seq_capitulo_proc_w
			and		nvl(ie_situacao,'A') = 'A';
			
			cd_componente_w := cd_componente_w + 1;


			insert into componente_ciap ( nr_sequencia,
										cd_componente_ciap,     
										dt_atualizacao,         
										nm_usuario,            
										dt_atualizacao_nrec,    
										nm_usuario_nrec,        
										ie_situacao,            
										ds_componente,          
										nr_seq_capitulo) 
								values	( nr_seq_componente_w,
										cd_componente_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										'A',
										ds_component_p,
										nr_seq_capitulo_proc_w);

		else
				
				select 	nvl(max(cd_componente_ciap),0)
				into	cd_componente_w
				from	componente_ciap
				where	nr_sequencia = nr_seq_componente_w;									
										
										
		end if;
		
		
		cd_ciap_proc_w := 	cd_capitulo_proc_w||substr(ds_code_p,2,5);
			
		insert into	problema_ciap 	(	nr_sequencia,
									CD_CIAP,                
									DT_ATUALIZACAO,         
									NM_USUARIO,             
									DT_ATUALIZACAO_NREC,    
									NM_USUARIO_NREC,       
									IE_SITUACAO,            
									DS_CRITERIO_INCLUSAO,   -- Crit�rios de inclus�o (Crit�rios de inclus�o)
									DS_OBSERVACAO,         	-- Observa��o (Nota)
									DS_REFERENCIA,          -- Refer�ncia (Considerar)
									DS_DEFINICAO,           -- Defini��o (Defini��o) 
									DS_DESCRICAO_ORIGINAL,	-- Descri��o oritinal (Titulo original)
									DS_CRITERIO_EXCLUSAO,   -- Crit�rios de exclus�o (Crit�rios de exclus�o)
									DS_CIAP,                -- CIAP (T�tulo leigo)
									NR_SEQ_COMPONENTE)
				values			(	problema_ciap_seq.nextval,
									cd_ciap_proc_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									'A',
									substr(ds_inclusion_p,0,255),
									substr(ds_note_p,0,255),
									substr(ds_inclusion_p,0,255),
									substr(ds_criteria_p,0,255),
									substr(ds_preferred_p,0,255),
									substr(ds_exclusion_p,0,255),
									substr(ds_short_title_p,0,255),
									nr_seq_componente_w);
		
		end;
	end loop;
	close C01;
	



elsif ( nvl(ie_somente_proc_p,'N') = 'N') then

	nr_seq_capitulo_w := obter_capitulo(ds_code_p);	

	if	( nr_seq_capitulo_w = 0) then
	
		Select 	capitulo_ciap_seq.nextval
		into	nr_seq_capitulo_w
		from 	dual;

		ds_capitulo_ciap_w	:= ds_chapter_p;

		insert into capitulo_ciap (	NR_SEQUENCIA,
									CD_CAPITULO,            
									DT_ATUALIZACAO,         
									NM_USUARIO,             
									DT_ATUALIZACAO_NREC,
									NM_USUARIO_NREC,        
									IE_SITUACAO,            
									DS_CAPITULO,
									NR_SEQ_VERSAO) 
							values (nr_seq_capitulo_w,
									cd_capitulo_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									'A',
									ds_capitulo_ciap_w,
									nr_seq_versao_p);
		
	end if;
	
	nr_seq_componente_w :=  obter_comp_cap(nr_seq_capitulo_w, ds_component_p);	
	
	if	( nr_seq_componente_w = 0) then
	
		Select 	componente_ciap_seq.nextval
		into	nr_seq_componente_w
		from 	dual;

		select 	nvl(max(cd_componente_ciap),0)
		into	cd_componente_w
		from	componente_ciap
		where	nr_seq_capitulo = nr_seq_capitulo_w
		and		nvl(ie_situacao,'A') = 'A';
		
		cd_componente_w := cd_componente_w + 1;


		insert into componente_ciap ( nr_sequencia,
									cd_componente_ciap,     
									dt_atualizacao,         
									nm_usuario,            
									dt_atualizacao_nrec,    
									nm_usuario_nrec,        
									ie_situacao,            
									ds_componente,          
									nr_seq_capitulo) 
							values	( nr_seq_componente_w,
									cd_componente_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									'A',
									ds_component_p,
									nr_seq_capitulo_w );

	else
			
			select 	nvl(max(cd_componente_ciap),0)
			into	cd_componente_w
			from	componente_ciap
			where	nr_sequencia = nr_seq_componente_w;									
									
									
	end if;



	select	count(*)
	into	qt_reg_cid_w
	from	cid_doenca
	where	cd_doenca_cid = nvl(cd_doenca_freq_w,'XPTO');

	if	( qt_reg_cid_w = 0) then
		cd_doenca_freq_w	:= null;
	end if;
	
	Select  problema_ciap_seq.nextval
	into	nr_seq_problema_ciap_w
	from	dual;

	insert into	problema_ciap 	(	nr_sequencia,
									CD_CIAP,                
									DT_ATUALIZACAO,         
									NM_USUARIO,             
									DT_ATUALIZACAO_NREC,    
									NM_USUARIO_NREC,       
									IE_SITUACAO,            
									DS_CRITERIO_INCLUSAO,   -- Crit�rios de inclus�o (Crit�rios de inclus�o)
									DS_OBSERVACAO,         	-- Observa��o (Nota)
									DS_REFERENCIA,          -- Refer�ncia (Considerar)
									DS_DEFINICAO,           -- Defini��o (Defini��o) 
									DS_DESCRICAO_ORIGINAL,	-- Descri��o oritinal (Titulo original)
									DS_CRITERIO_EXCLUSAO,   -- Crit�rios de exclus�o (Crit�rios de exclus�o)
									DS_CIAP,                -- CIAP (T�tulo leigo)
									NR_SEQ_COMPONENTE,
									CD_DOENCA_CID)
				values			(	nr_seq_problema_ciap_w,
									ds_code_p,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									'A',
									substr(ds_inclusion_p,0,255),
									substr(ds_note_p,0,255),
									substr(ds_inclusion_p,0,255),
									substr(ds_criteria_p,0,255),
									substr(ds_preferred_p,0,255),
									substr(ds_exclusion_p,0,255),
									substr(ds_short_title_p,0,255),
									nr_seq_componente_w,
									cd_doenca_freq_w);
									
	if	(cd_capitulo_w <> 'PR') and
		(ds_cids_possiveis_w is not null) then


		if	(instr(ds_cids_possiveis_w, ',') > 0) then
			
		   while   (length(ds_cids_possiveis_w) > 0) loop
				   begin
				   vl_pos_w        := instr(ds_cids_possiveis_w,',');
				   if      (vl_pos_w > 0)  then
						   ds_cids_possiveis_w2     := substr(ds_cids_possiveis_w, 1,                (vl_pos_w - 1));
						   ds_cids_possiveis_w      := substr(ds_cids_possiveis_w, (vl_pos_w + 1),   length(ds_cids_possiveis_w));
				   else
						   ds_cids_possiveis_w2     := ds_cids_possiveis_w;
						   ds_cids_possiveis_w      := '';
				   end     if;
				   
					ds_cids_possiveis_w2	:= 	replace(ds_cids_possiveis_w2,'.','');
					ds_cids_possiveis_w2	:= 	replace(ds_cids_possiveis_w2,' ','');

					select 	count(*)
					into	qt_reg_cid_w
					from	cid_doenca
					where	cd_doenca_cid = ds_cids_possiveis_w2;
					
					
					if (qt_reg_cid_w > 0) then
					
										   
						insert into diagnostico_ciap	(	NR_SEQUENCIA,           
												DT_ATUALIZACAO,         
												NM_USUARIO,             
												DT_ATUALIZACAO_NREC,
												NM_USUARIO_NREC,        
												NR_SEQ_PROBLEMA,                
												CD_DOENCA_CID) 
								values		(	diagnostico_ciap_seq.nextval,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p,
												nr_seq_problema_ciap_w,
												ds_cids_possiveis_w2);	
					end if;
				   
				   
				   end;
		   end loop;
			
		else
		
			ds_cids_possiveis_w	:= 	replace(ds_cids_possiveis_w,'.','');
		
			select 	count(*)
			into	qt_reg_cid_w
			from	cid_doenca
			where	cd_doenca_cid = ds_cids_possiveis_w;		

			if ( qt_reg_cid_w > 0) then			
			
				insert into diagnostico_ciap	(	NR_SEQUENCIA,           
													DT_ATUALIZACAO,         
													NM_USUARIO,             
													DT_ATUALIZACAO_NREC,
													NM_USUARIO_NREC,        
													NR_SEQ_PROBLEMA,                
													CD_DOENCA_CID) 
									values		(	diagnostico_ciap_seq.nextval,
													sysdate,
													nm_usuario_p,
													sysdate,
													nm_usuario_p,
													nr_seq_problema_ciap_w,
													ds_cids_possiveis_w);	
												
			end if;
		
		end if;
										
	end if;
end if;	

commit;

end carregar_ciap_importacao;
/