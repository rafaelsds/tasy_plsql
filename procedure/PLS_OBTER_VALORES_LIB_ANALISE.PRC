create or replace
procedure pls_obter_valores_lib_analise(nr_seq_item_resumo_p	in number,
					cd_estabelecimento_p	in number,
					vl_glosa_p		out number,
					vl_liberado_p		in out number,
					qt_liberada_p		in out number,
					vl_liberado_co_p	in out number,
					vl_liberado_material_p	in out number,
					vl_liberado_hi_p	in out number,
					vl_glosa_co_p		out number,
					vl_glosa_material_p	out number,
					vl_glosa_hi_p		out number) is
					
vl_base_glosa_w			number(15,2) := 0;
vl_base_w			number(15,2);
vl_liberado_co_w		number(15,2);
vl_liberado_material_w		number(15,2);
vl_liberado_hi_w		number(15,2);
vl_liberado_w			number(15,2);
qt_liberada_w			number(12,4);
vl_glosa_co_w			number(15,2);
vl_glosa_material_w		number(15,2);
vl_glosa_hi_w			number(15,2);
ie_valor_base_w			number(15,2);
vl_calculado_w			number(15,2);
vl_apresentado_w		number(15,2);
qt_apresentado_w		number(12,4);
ie_calculo_base_glosa_w		varchar2(1)	:= 'N';
vl_calculado_mat_w		number(15,2);
vl_calculado_co_w		number(15,2);
vl_calculado_hi_w		number(15,2);
vl_glosa_w			number(15,2);

begin
if	(nr_seq_item_resumo_p is not null) then
	select	nvl(max(ie_calculo_base_glosa),'N')
	into	ie_calculo_base_glosa_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;

	select	a.ie_valor_base,
		nvl(a.vl_total_apres,0),
		nvl(a.vl_calculado,0),
		nvl(a.qt_apresentado,0),
		nvl(a.vl_calculado_co,0),
		nvl(a.vl_calculado_material,0),
		nvl(a.vl_calculado_hi,0)
	into	ie_valor_base_w,
		vl_apresentado_w,
		vl_calculado_w,
		qt_apresentado_w,
		vl_calculado_co_w,
		vl_calculado_mat_w,
		vl_calculado_hi_w
	from	w_pls_resumo_conta a
	where	a.nr_sequencia	= nr_seq_item_resumo_p;

	if	(ie_valor_base_w = 1) then
		vl_base_w	:= vl_apresentado_w;
	else
		vl_base_w	:= vl_calculado_w;
	end if;

	if	(vl_apresentado_w > 0) then
		vl_base_glosa_w	:= vl_apresentado_w;
	end if;

	vl_liberado_w		:= nvl(vl_liberado_p,vl_base_w);
	vl_glosa_w		:= vl_base_glosa_w - vl_liberado_w;	
	qt_liberada_w		:= nvl(qt_liberada_p,qt_apresentado_w);

	vl_liberado_co_w	:= nvl(vl_liberado_co_p,pls_obter_valor_proporcional(vl_calculado_w,vl_calculado_co_w,vl_liberado_w));
	vl_liberado_material_w	:= nvl(vl_liberado_material_p,pls_obter_valor_proporcional(vl_calculado_w,vl_calculado_mat_w,vl_liberado_w));
	vl_liberado_hi_w	:= nvl(vl_liberado_hi_p,pls_obter_valor_proporcional(vl_calculado_w,vl_calculado_hi_w,vl_liberado_w));

	/*if	(nvl(vl_liberado_co_p,0) = 0) and
		(nvl(vl_liberado_material_p,0) = 0) and
		(nvl(vl_liberado_hi_p,0) = 0) then
		vl_glosa_co_w	:= pls_obter_valor_proporcional(vl_calculado_w,vl_calculado_co_w,vl_glosa_w);
		vl_glosa_material_w	:= pls_obter_valor_proporcional(vl_calculado_w,vl_calculado_mat_w,vl_glosa_w);
		vl_glosa_hi_w	:= pls_obter_valor_proporcional(vl_calculado_w,vl_calculado_hi_w,vl_glosa_w);
	else
		vl_glosa_co_w	:= vl_calculado_co_w - vl_liberado_co_w;
		vl_glosa_material_w := vl_calculado_mat_w - vl_liberado_material_w;
		vl_glosa_hi_w	:= vl_calculado_hi_w - vl_liberado_hi_w;
		
		vl_glosa_w	:= vl_glosa_co_w + vl_glosa_material_w + vl_glosa_hi_w;
	end if;*/
	
	if	(vl_glosa_w < 0) then
		vl_glosa_w := 0;
	end if;
	
	vl_glosa_p	:= vl_glosa_w;
	vl_liberado_p	:= vl_liberado_w;
	qt_liberada_p	:= qt_liberada_w;
	vl_liberado_co_p	:= vl_liberado_co_w;
	vl_liberado_material_p	:= vl_liberado_material_w;
	vl_liberado_hi_p	:= vl_liberado_hi_w;
	vl_glosa_co_p		:= vl_glosa_co_w;
	vl_glosa_material_p	:= vl_glosa_material_w;
	vl_glosa_hi_p		:= vl_glosa_hi_w;
end if;
/* N�o pode commit aqui */

end pls_obter_valores_lib_analise;
/
