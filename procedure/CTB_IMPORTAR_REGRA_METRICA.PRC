create or replace
procedure ctb_importar_regra_metrica(	cd_empresa_p		number,
				cd_estabelecimento_p	number,
				nr_seq_cenario_p		number,
				nr_seq_regra_p		number,
				nr_seq_metrica_p		number,
				nr_seq_incremento_p	number,
				cd_centro_custo_p		number,
				dt_mes_inic_p		date,
				dt_mes_fim_p		date,
				qt_metrica_p		number,
				ie_sobrepor_p		varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_regra_w			number(10);

begin

select	nvl(max(nr_seq_regra),0)
into	nr_seq_regra_w
from	ctb_regra_metrica
where	nr_seq_cenario		= nr_seq_cenario_p
and	cd_estabelecimento	= cd_estabelecimento_p;

nr_seq_regra_w	:= nr_seq_regra_w + nvl(nr_seq_incremento_p,1);

insert into ctb_regra_metrica(
	nr_sequencia,
	cd_estabelecimento,
	nr_seq_cenario,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_regra,
	nr_seq_metrica,
	nr_seq_mes_ref,
	cd_centro_custo,
	dt_mes_inic,
	dt_mes_fim,
	ie_regra,
	pr_aplicar,
	ie_sobrepor,
	qt_fixa,
	qt_leito,
	tx_ocupacao)
values(	ctb_regra_metrica_seq.nextval,
	cd_estabelecimento_p,
	nr_seq_cenario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nvl(nr_seq_regra_p,0),
	nr_seq_metrica_p,
	null,
	cd_centro_custo_p,
	dt_mes_inic_p,
	dt_mes_fim_p,
	'QF',
	null,
	'S',
	qt_metrica_p,
	null,
	null);



commit;

end ctb_importar_regra_metrica;
/
