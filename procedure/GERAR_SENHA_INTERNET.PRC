create or replace
procedure gerar_senha_internet( nr_atendimento_p	number,
				dt_alta_p		date,				
				dt_cancelamento_p	date,
				dt_exclusao_p		date,
				ds_senha_p		varchar2,
				ie_acao_p		varchar2) is				 

ds_senha_w		varchar2(20);
nm_usuario_w		varchar2(15);

dt_exclusao_w		date;
dt_alta_w               date;
dt_cancelamento_w	date;
nr_atendimento_w	number(10);


/*  
ie_acao_p

GA - Gerar alta

CA - Cancelar atendimento

EA - Excluir atendimento
*/
begin
nm_usuario_w		:= nvl(wheb_usuario_pck.get_nm_usuario, 'Tasy_integracao');
nr_atendimento_w	:= nr_atendimento_p;
dt_alta_w		:= null;
dt_cancelamento_w	:= null;
dt_exclusao_w		:= null;
		
		
if(ie_acao_p =  'GA') then
	dt_alta_w	:= dt_alta_p;
elsif(ie_acao_p =  'CA') then
	dt_cancelamento_w	:= dt_cancelamento_p;
elsif(ie_acao_p =  'EA') then
	dt_exclusao_w := 	dt_exclusao_p;		
end if;

insert into  atend_senha_internet(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_senha,
					dt_leitura,
					dt_alta,
					dt_cancelamento,
					dt_exclusao,
					nr_atendimento )
			values(		atend_senha_internet_seq.nextval,
					sysdate,
					nm_usuario_w,
					sysdate,
					nm_usuario_w,
					ds_senha_p,
					null,
					dt_alta_w,
					dt_cancelamento_w,
					dt_exclusao_w,
					nr_atendimento_w );

end gerar_senha_internet;
/