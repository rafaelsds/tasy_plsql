create or replace
procedure rxt_adicionar_tipo_trat_adic( nr_seq_tumor_p	number,
					nr_seq_tipo_p	number,
					nm_usuario_p	Varchar2) is 
begin
if	(nr_seq_tumor_p is not null) and
	(nr_seq_tipo_p is not null) then
	insert into rxt_tratamento_pac (	
		nr_sequencia,
		nr_seq_tipo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_tumor,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	rxt_tratamento_pac_seq.nextval,
		nr_seq_tipo_p,
		sysdate,
		nm_usuario_p,
		nr_seq_tumor_p,
		sysdate,
		nm_usuario_p);
end if;

commit;

end rxt_adicionar_tipo_trat_adic;
/