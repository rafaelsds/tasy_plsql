create or replace
procedure intpd_recebe_inventario_mat(	nr_sequencia_p		in	number,
					xml_p		in	xmltype) is 

qt_registros_w			number(10);
ie_conversao_w			intpd_eventos_sistema.ie_conversao%type;
nr_seq_sistema_w			intpd_eventos_sistema.nr_seq_sistema%type;					
nr_seq_projeto_xml_w		intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_regra_conv_w		conversao_meio_externo.nr_seq_regra%type;
ie_sistema_externo_w		varchar2(15);
inventario_w			inventario%rowtype;
inventario_material_w		inventario_material%rowtype;
reg_integracao_w			gerar_int_padrao.reg_integracao_conv;
nr_inventario_delete_w		inventario_material.nr_sequencia%type;
i				integer;
ds_erro_w			varchar2(4000);


cursor c01 is
select	*
from	xmltable('/STRUCTURE/INVENTORY' passing xml_p columns
	ie_action			varchar2(15)	path	'IE_ACTION',
	cd_estabelecimento		varchar2(40)	path	'CD_ESTABLISHMENT',
	cd_fornecedor		varchar2(40)	path	'CD_SUPPLIER',
	cd_local_estoque		varchar2(40)	path	'CD_STOCK_LOCATION',
	ds_observacao		varchar2(255)	path	'DS_OBSERVATION',
	dt_atualizacao		varchar2(40)	path	'DT_UPDATE',
	dt_bloqueio		varchar2(40)	path	'DT_BLOCK',
	dt_inicio			varchar2(40)	path	'DT_START',
	ie_consignado		varchar2(40)	path	'IE_CONSIGNED',
	nm_usuario		varchar2(40)	path	'NM_USER',
	nm_usuario_bloqueio	varchar2(40)	path	'NM_USER_BLOCK',
	nr_documento_externo	varchar2(255)	path	'NR_EXTERNAL_DOCUMENT',
	qt_max_contagem		varchar2(40)	path	'QT_MAXIMUM_NUMBER_COUNTS',
	qt_max_definir		varchar2(40)	path	'QT_MAXIMUM_COUNT_DEFINED',
	xml_itens_inventario		xmltype 	path	'ITEMS');
c01_w	c01%rowtype;    
    
cursor c02 is
select	*
from	xmltable('/ITEMS/ITEM' passing c01_w.xml_itens_inventario columns
	cd_fornecedor		varchar2(40)	path	'CD_SUPPLIER',
	cd_material		varchar2(40)	path	'CD_MATERIAL',
	ds_justificativa		varchar2(4000)	path	'DS_JUSTIFICATION',
	ds_observacao		varchar2(255)	path	'DS_OBSERVATION',
	dt_atualizacao		varchar2(40)	path	'DT_UPDATE',
	ie_agrupador		varchar2(40)	path	'IE_GROUP_MAKER',
	nm_usuario		varchar2(40)	path	'NM_USER');
c02_w	c02%rowtype;

begin

select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_conv_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w	:=	nr_seq_sistema_w;

reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:=	'R';
reg_integracao_w.ie_sistema_externo		:=	ie_sistema_externo_w;
reg_integracao_w.ie_conversao		:=	ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:=	nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao	:=	nr_seq_regra_conv_w;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	
	if	(c01_w.ie_action = 'DELETE') then
		
		select	nvl(max(nr_sequencia),0)
		into	nr_inventario_delete_w
		from	inventario
		where	nr_documento_externo = c01_w.nr_documento_externo
		and	ie_sistema_origem = ie_sistema_externo_w;

		
		if	(nr_inventario_delete_w > 0) then
		
			select	dt_bloqueio,
				dt_atualizacao_saldo,
				dt_aprovacao,
				cd_estabelecimento
			into	inventario_w.dt_bloqueio,
				inventario_w.dt_atualizacao_saldo,
				inventario_w.dt_aprovacao,
				inventario_w.cd_estabelecimento
			from	inventario
			where	nr_sequencia = nr_inventario_delete_w;
			
			/*Se o invet�rio n�o est� bloqueado, ele pode ser deletado.*/
			if	(inventario_w.dt_bloqueio is null) then
				
				delete from inventario_material	where nr_seq_inventario = nr_inventario_delete_w;				
				delete from inventario where nr_sequencia = nr_inventario_delete_w;
				
			else					
				begin
				
				/*Se o invent�rio est� atualizado ou aprovado, n�o pode ser deletado.*/
				if	((inventario_w.dt_atualizacao_saldo is not null) or (inventario_w.dt_aprovacao is not null)) then
				
					intpd_gravar_log_recebimento(nr_sequencia_p, wheb_mensagem_pck.get_Texto(235097),nvl(c01_w.nm_usuario,'INTPD'));
					/*Este invent�rio j� possui data de atualiza��o de saldo ou j� esta aprovado. Favor verificar.*/
			
				else					
					/*Se o invent�rio est� bloqueado e n�o atualizado e n�o aprovado, ele ser� desbloqueado e deletado.*/
					desbloquear_itens_inventario(nr_inventario_delete_w, inventario_w.cd_estabelecimento, nvl(c01_w.nm_usuario,'INTPD'));
					delete from inventario_material where nr_seq_inventario = nr_inventario_delete_w;
					delete from inventario where nr_sequencia = nr_inventario_delete_w;			
				end if;
				end;
			end if;
		end if;
	else
	
		begin
		reg_integracao_w.nm_tabela			:=	'INVENTARIO';
		reg_integracao_w.nm_elemento		:=	'INVENTORY';
		reg_integracao_w.nr_seq_visao		:=	25153;
	
		intpd_processar_atributo(reg_integracao_w, 'CD_ESTABELECIMENTO', c01_w.cd_estabelecimento, 'S', inventario_w.cd_estabelecimento);
		intpd_processar_atributo(reg_integracao_w, 'CD_FORNECEDOR', c01_w.cd_fornecedor, 'S', inventario_w.cd_fornecedor);
		intpd_processar_atributo(reg_integracao_w, 'CD_LOCAL_ESTOQUE', c01_w.cd_local_estoque, 'S', inventario_w.cd_local_estoque);
		intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c01_w.ds_observacao, 'N', inventario_w.ds_observacao);		
		intpd_processar_atributo(reg_integracao_w, 'DT_ATUALIZACAO', c01_w.dt_atualizacao, 'N', inventario_w.dt_atualizacao);		
		intpd_processar_atributo(reg_integracao_w, 'DT_INICIO', c01_w.dt_inicio, 'N', inventario_w.dt_inicio);
		intpd_processar_atributo(reg_integracao_w, 'IE_CONSIGNADO', c01_w.ie_consignado, 'N', inventario_w.ie_consignado);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c01_w.nm_usuario, 'N', inventario_w.nm_usuario);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_BLOQUEIO', c01_w.nm_usuario_bloqueio, 'N', inventario_w.nm_usuario_bloqueio);
		intpd_processar_atributo(reg_integracao_w, 'NR_DOCUMENTO_EXTERNO', c01_w.nr_documento_externo, 'N', inventario_w.nr_documento_externo);
		intpd_processar_atributo(reg_integracao_w, 'QT_MAX_CONTAGEM', c01_w.qt_max_contagem, 'N', inventario_w.qt_max_contagem);
		intpd_processar_atributo(reg_integracao_w, 'QT_MAX_DEFINIR', c01_w.qt_max_definir, 'N', inventario_w.qt_max_definir);		
		intpd_processar_atributo(reg_integracao_w, 'IE_SISTEMA_ORIGEM', ie_sistema_externo_w, 'N', inventario_w.ie_sistema_origem);
		inventario_w.ie_novo_item_barra		:= 'N';
		inventario_w.dt_atualizacao_nrec		:= inventario_w.dt_atualizacao;
		inventario_w.nm_usuario_nrec		:= inventario_w.nm_usuario;
		inventario_w.ie_contagem_atual		:= '1';
		inventario_w.ie_tipo_contagem		:= '1';
		inventario_w.ie_desconsidera_saldo_kit		:= 'N';
		inventario_w.dt_bloqueio			:= null;
		
		
		if	(reg_integracao_w.qt_reg_log = 0) then
		
			select	nvl(max(nr_sequencia),0)
			into	inventario_w.nr_sequencia
			from	inventario
			where	nr_documento_externo = inventario_w.nr_documento_externo
			and	ie_sistema_origem = inventario_w.ie_sistema_origem
			and	dt_atualizacao_saldo is null;
			
			if	(inventario_w.nr_sequencia > 0) then

				update	inventario
				set	row = inventario_w
				where	nr_sequencia = inventario_w.nr_sequencia;
				
				
			else
				select	inventario_seq.nextval
				into	inventario_w.nr_sequencia
				from	dual;
				
				insert into inventario values inventario_w;
			end if;
		end if;
		
		open C02;
		loop
		fetch C02 into	
			c02_w;
		exit when C02%notfound;
			begin
			
			reg_integracao_w.nm_tabela			:=	'INVENTARIO_MATERIAL';
			reg_integracao_w.nm_elemento		:=	'ITEM';
			reg_integracao_w.nr_seq_visao		:=	17500;
			
			intpd_processar_atributo(reg_integracao_w, 'CD_FORNECEDOR', c02_w.cd_fornecedor, 'S', inventario_material_w.cd_fornecedor);
			intpd_processar_atributo(reg_integracao_w, 'CD_MATERIAL', c02_w.cd_material, 'S', inventario_material_w.cd_material);
			intpd_processar_atributo(reg_integracao_w, 'DS_JUSTIFICATIVA', c02_w.ds_justificativa, 'N', inventario_material_w.ds_justificativa);
			intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c02_w.ds_observacao, 'N', inventario_material_w.ds_observacao);
			intpd_processar_atributo(reg_integracao_w, 'DT_ATUALIZACAO', c02_w.dt_atualizacao, 'N', inventario_material_w.dt_atualizacao);
			intpd_processar_atributo(reg_integracao_w, 'IE_AGRUPADOR', c02_w.ie_agrupador, 'N', inventario_material_w.ie_agrupador);
			intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c02_w.nm_usuario, 'N', inventario_material_w.nm_usuario);
			inventario_material_w.nr_seq_inventario		:= inventario_w.nr_sequencia;
			inventario_material_w.dt_atualizacao		:= inventario_w.dt_atualizacao;
			inventario_material_w.nm_usuario		:= inventario_w.nm_usuario;
			inventario_material_w.dt_atualizacao_nrec	:= inventario_w.dt_atualizacao;
			inventario_material_w.nm_usuario_nrec		:= inventario_w.nm_usuario;
			inventario_material_w.ie_contagem_atual	:= '1';
			
			
			
			if	(reg_integracao_w.qt_reg_log = 0) then
		
				select	cd_material_estoque
				into	inventario_material_w.cd_material
				from	material
				where	cd_material = inventario_material_w.cd_material;
				
				select	nvl(max(nr_sequencia),0)
				into	inventario_material_w.nr_sequencia
				from	inventario_material
				where	nr_seq_inventario = inventario_w.nr_sequencia
				and	cd_material = inventario_material_w.cd_material;
				
				if	(inventario_material_w.nr_sequencia > 0) then

					update	inventario_material
					set	row = inventario_material_w
					where	nr_sequencia = inventario_material_w.nr_sequencia;
					
					
				else
					select	inventario_material_seq.nextval
					into	inventario_material_w.nr_sequencia
					from	dual;
					
					insert into inventario_material values inventario_material_w;
				end if;
			end if;			
			end;
		end loop;
		close C02;
		
		if	(reg_integracao_w.qt_reg_log = 0) and
			(c01_w.dt_bloqueio is not null) then
			
			bloquear_itens_inventario(inventario_w.nr_sequencia,inventario_w.cd_estabelecimento,inventario_w.nm_usuario, ds_erro_w);
		
		end if;
		end;
	end if;	
	end;
end loop;
close C01;

if	(reg_integracao_w.qt_reg_log > 0) then
	begin
	
	rollback;
 
	update intpd_fila_transmissao
	set	ie_status = 'E',
		ie_tipo_erro = 'F'
	where	nr_sequencia = nr_sequencia_p;
 
	for i in 0..reg_integracao_w.qt_reg_log-1 loop		
		INTPD_GRAVAR_LOG_RECEBIMENTO(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;

else

	update	intpd_fila_transmissao
	set	ie_status = 'S',
		nr_seq_documento = nvl(nr_inventario_delete_w,inventario_w.nr_sequencia),
		nr_doc_externo = c01_w.nr_documento_externo
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end intpd_recebe_inventario_mat;
/