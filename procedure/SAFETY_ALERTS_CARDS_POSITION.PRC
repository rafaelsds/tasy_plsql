create or replace
procedure safety_alerts_cards_position(
			nm_usuario_p		Varchar2,
			nr_seq_obj_p		number,	
			qt_x_p			number,
			qt_y_p			number,
			ie_type_p		Varchar2,
			ds_title_p		Varchar2) is 
			
record_exists_w	number(10);
core_table_settings_record_w	number(10);
nr_seq_w	safety_warnings_position.nr_sequencia%type;	

begin
	
	
	if	(ie_type_p = 'AD') then
	
		begin
		select	count(*)
		into	core_table_settings_record_w
		from	safety_warnings_position
		where	nr_object_schematic = nr_seq_obj_p
		and	nm_usuario = nm_usuario_p
		and	ds_title = ds_title_p;
		exception
		when others then
			record_exists_w := 0;
		end;
	end if;
	
	if	(ie_type_p <> 'AD') then
	
		begin
		select	count(*)
		into	record_exists_w
		from	safety_warnings_position
		where	nr_object_schematic = nr_seq_obj_p
		and	nm_usuario = nm_usuario_p;
		exception
		when others then
			record_exists_w := 0;
		end;
	end if;
	
	if	(record_exists_w = 0 or core_table_settings_record_w = 0) then
	
		select	safety_warnings_position_seq.nextval
		into	nr_seq_w
		from	dual;
	
		insert into	safety_warnings_position(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_object_schematic,
					qt_position_x,
					qt_position_y,
					ie_type,
					ds_title)
		values			(nr_seq_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_obj_p,
					qt_x_p,
					qt_y_p,
					ie_type_p,
					ds_title_p);
	end if;
	
	if	(core_table_settings_record_w > 0) then

		update	safety_warnings_position
		set	dt_atualizacao = sysdate,
			qt_position_x = qt_x_p,
			qt_position_y = qt_y_p
		where	nr_object_schematic = nr_seq_obj_p
		and	nm_usuario = nm_usuario_p
		and	ds_title = ds_title_p;
	end if;

	
	if	(record_exists_w > 0) then
		
		update	safety_warnings_position
		set	dt_atualizacao = sysdate,
			qt_position_x = qt_x_p,
			qt_position_y = qt_y_p
		where	nr_object_schematic = nr_seq_obj_p
		and	nm_usuario = nm_usuario_p;
	end if;
	
end safety_alerts_cards_position;
/