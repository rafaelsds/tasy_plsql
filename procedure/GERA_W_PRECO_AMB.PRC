CREATE OR REPLACE
PROCEDURE Gera_W_PRECO_AMB	(CD_EDICAO_AMB_P	NUMBER,
				CD_CONVENIO_P		NUMBER,
				CD_CATEGORIA_P	VARCHAR2,
				CD_ESPECIALIDADE_P	NUMBER,
				DT_INICIO_VIGENCIA_P	DATE) IS

DT_ATUALIZACAO_W		DATE		:= SYSDATE;
qt_pontos_w			preco_amb.qt_pontuacao%type;
CD_PROCEDIMENTO_W       	NUMBER(15)	:= 0;
VL_PROCEDIMENTO_W       	NUMBER(15,2) 	:= 0;
VL_CUSTO_OPERACIONAL_W  	NUMBER(15,2) 	:= 0;
VL_ANESTESISTA_W        	NUMBER(15,2) 	:= 0;
VL_MEDICO_W             	NUMBER(15,2) 	:= 0;
VL_AUXILIARES_W         	NUMBER(15,2) 	:= 0;
VL_FILME_W              	NUMBER(15,4) 	:= 0;
QT_FILME_W              	NUMBER(15,4) 	:= 0;
NR_AUXILIARES_W         	NUMBER(2)    	:= 0;
NR_INCIDENCIA_W         	NUMBER(2)    	:= 0; 
QT_PORTE_ANESTESICO_W   	NUMBER(2)    	:= 0;
CD_ESTABELECIMENTO_W    	NUMBER(4)    	:= 1;

VL_PTO_CUSTO_OPERAC_W	NUMBER(15,2) 	:= 0;
VL_PTO_PROCEDIMENTO_W   	NUMBER(15,2) 	:= 0;
VL_PTO_ANESTESISTA_W    	NUMBER(15,2) 	:= 0;
VL_PTO_MEDICO_W         	NUMBER(15,2) 	:= 0;
VL_PTO_AUXILIARES_W     	NUMBER(15,4) 	:= 0;
VL_PTO_MATERIAIS_W      	NUMBER(15,4)	:= 0;
cd_edicao_amb_w		NUMBER(6)	:= 0;
nr_porte_anestesico_w	NUMBER(2)	:= 0;
ie_origem_proced_w		NUMBER(10);

cd_usuario_convenio_w	VARCHAR2(40);
cd_plano_w			VARCHAR2(20);
ie_clinica_w			NUMBER(15,0);
cd_empresa_ref_w		NUMBER(15,0);
ie_preco_informado_w		varchar2(01);
nr_seq_ajuste_proc_w	number(10,0);

CURSOR C01 IS
select	A.CD_PROCEDIMENTO,
       A.QT_FILME,               
       A.NR_AUXILIARES,          
       A.NR_INCIDENCIA,          
       A.QT_PORTE_ANESTESICO,
	a.ie_origem_proced    
from	GRUPO_PROC B,
       PROCEDIMENTO C,
       PRECO_AMB A
WHERE	A.CD_EDICAO_AMB    	= CD_EDICAO_AMB_P
AND	A.CD_PROCEDIMENTO  	= C.CD_PROCEDIMENTO
and	a.ie_origem_proced	= c.ie_origem_proced
AND	B.CD_GRUPO_PROC    	= C.CD_GRUPO_PROC
AND	B.CD_ESPECIALIDADE 	= nvl(CD_ESPECIALIDADE_P,-1)
AND	nvl(A.DT_INICIO_VIGENCIA, sysdate)	= (	
			select 	max( nvl( x.dt_inicio_vigencia, sysdate) )
			from	PRECO_AMB x
			where	x.cd_procedimento 	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.CD_EDICAO_AMB    	= CD_EDICAO_AMB_P
			and	nvl( x.dt_inicio_vigencia, sysdate)	<= nvl(DT_INICIO_VIGENCIA_P, nvl( x.dt_inicio_vigencia, sysdate) )
			)
union
select	A.CD_PROCEDIMENTO,
	A.QT_FILME,               
       A.NR_AUXILIARES,          
       A.NR_INCIDENCIA,          
       A.QT_PORTE_ANESTESICO,
	a.ie_origem_proced    
from	PRECO_AMB A
WHERE	A.CD_EDICAO_AMB    	= CD_EDICAO_AMB_P
and	nvl(CD_ESPECIALIDADE_P,-1) 	= -1
AND	nvl( A.DT_INICIO_VIGENCIA, sysdate)	= (	
			select 	max( nvl( x.dt_inicio_vigencia, sysdate) )
			from	PRECO_AMB x
			where	x.cd_procedimento 	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.CD_EDICAO_AMB    	= CD_EDICAO_AMB_P
			and	nvl( x.dt_inicio_vigencia, sysdate)	<= nvl( DT_INICIO_VIGENCIA_P, nvl( x.dt_inicio_vigencia, sysdate) )
			);


BEGIN

/* Limpar tabela W_PRECO_AMB */
begin
DELETE FROM W_PRECO_AMB;
COMMIT;
end;

/* Gerar tabela W_PRECO_AMB a partir da PRECO_AMB */
OPEN C01;
LOOP
   FETCH C01 into	CD_PROCEDIMENTO_W,
                  	QT_FILME_W,               
                  	NR_AUXILIARES_W,          
                  	NR_INCIDENCIA_W,          
                  	QT_PORTE_ANESTESICO_W,
			ie_origem_proced_w;    
	if	C01%FOUND then
		begin
		Define_Preco_Procedimento	(CD_ESTABELECIMENTO_W,
						CD_CONVENIO_P,
						CD_CATEGORIA_P,
						DT_ATUALIZACAO_W,
						CD_PROCEDIMENTO_W,
				 		null,
						null,
				 		null,
				 		null,
				 		null,
						null,
				 		null,
						null,
						cd_usuario_convenio_w, 
						cd_plano_w, 
						ie_clinica_w, 
						cd_empresa_ref_w,null,
     						VL_PROCEDIMENTO_W,
	                   			VL_CUSTO_OPERACIONAL_W,
        	           			VL_ANESTESISTA_W,
                	   			VL_MEDICO_W,
                   				VL_AUXILIARES_W,
                   				VL_FILME_W,
	            	 			VL_PTO_PROCEDIMENTO_W,
        	    	 			VL_PTO_CUSTO_OPERAC_W,
            		 			VL_PTO_ANESTESISTA_W,
            	 				VL_PTO_MEDICO_W,
            	 				VL_PTO_AUXILIARES_W,
	            	 			VL_PTO_MATERIAIS_W,
				 		nr_porte_anestesico_w,
						qt_pontos_w,
				 		cd_edicao_amb_w, ie_preco_informado_w, nr_seq_ajuste_proc_w,
						0, null, 0, null, null,
						NULL, NULL, null, null, 
						null, null, null, null,
						null, null, null,null,null, null, null);

		INSERT INTO W_PRECO_AMB	(
						CD_EDICAO_AMB,
                	    			CD_PROCEDIMENTO,
		        			VL_PROCEDIMENTO,
     			  			VL_CUSTO_OPERACIONAL,
				  		VL_ANESTESISTA,
				  		VL_MEDICO,
				  		VL_FILME,
				  		QT_FILME,
			  			NR_AUXILIARES,
				 		NR_INCIDENCIA,
				  		QT_PORTE_ANESTESICO,
                	    			IE_ORIGEM_PROCED,
                    				VL_AUXILIARES
						)
					VALUES	(
						CD_EDICAO_AMB_P,
                	    			CD_PROCEDIMENTO_W,
		        			nvl(VL_PROCEDIMENTO_W,0),
	    			  		VL_CUSTO_OPERACIONAL_W,
				  		VL_ANESTESISTA_W,
				  		VL_MEDICO_W,
				  		VL_FILME_W,
			  			QT_FILME_W,
				  		NR_AUXILIARES_W,
				  		NR_INCIDENCIA_W,
				  		QT_PORTE_ANESTESICO_W, 
                    				ie_origem_proced_w,
                    				VL_AUXILIARES_W
						);
		end;
	else
		exit;
	end if;
END LOOP;
CLOSE C01;

COMMIT;
END	Gera_W_PRECO_AMB;
/
