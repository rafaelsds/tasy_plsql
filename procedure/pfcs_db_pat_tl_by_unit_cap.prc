create or replace procedure pfcs_db_pat_tl_by_unit_cap (
    nr_seq_indicator_p	number,
    cd_estabelecimento_p	number,
    nm_usuario_p	varchar2) is

	    nr_seq_panel_w					pfcs_panel_detail.nr_seq_panel%type;

/* in use = Patients on Tele / Total  Devices */

cursor c01_device_from_pfcs is
   select	sum(total_device_in_use) total_device_in_use,
			sum(total_devices) total_devices,
			ds_department
	from (	select	count(1) total_device_in_use,
					0 total_devices,
					sa.ds_setor_atendimento ds_department
			from	pfcs_encounter enc,
					pfcs_patient pat,					
					pfcs_encounter_location el,
					pfcs_device dev,
					unidade_atendimento uni,
					setor_atendimento sa
			where	dev.si_status = 'ACTIVE'
			and		dev.ds_device_type = 'Monitor'
			and 	pat.nr_sequencia = dev.nr_seq_patient
			and 	pat.ie_active = '1'			
			and		enc.nr_seq_patient = pat.nr_sequencia
			and		enc.si_status in ('PLANNED', 'ARRIVED')
			and		el.nr_seq_encounter = enc.nr_sequencia
			and 	uni.nr_seq_location = el.nr_seq_location
			and 	uni.cd_setor_atendimento = sa.cd_setor_atendimento
			and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
			and 	sa.cd_estabelecimento_base = cd_estabelecimento_p			
			group by sa.ds_setor_atendimento,0
			union
			select	0 total_device_in_use,
					sa.nr_unit_tl_capacity total_devices,
					sa.ds_setor_atendimento ds_department
			from	setor_atendimento sa
			where 	sa.cd_estabelecimento_base = cd_estabelecimento_p
			and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
			and 	sa.ie_situacao = 'A'
			group by sa.ds_setor_atendimento, sa.nr_unit_tl_capacity, 0
		)
	group by ds_department;

begin
	for r_c02 in c01_device_from_pfcs loop
		pfcs_pck_v2.pfcs_generate_results(
					vl_indicator_p => r_c02.total_device_in_use,
					vl_indicator_aux_p => r_c02.total_devices,
					ds_reference_value_p => r_c02.ds_department,
					nr_seq_indicator_p => nr_seq_indicator_p,
					nr_seq_operational_level_p => cd_estabelecimento_p,
					nm_usuario_p => nm_usuario_p,
        		nr_seq_panel_p => nr_seq_panel_w);

		pfcs_pck_v2.pfcs_update_detail(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_panel_p => nr_seq_panel_w,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p);
	end loop;

	commit;

	pfcs_pck_v2.pfcs_activate_records(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p);

end pfcs_db_pat_tl_by_unit_cap;
/
