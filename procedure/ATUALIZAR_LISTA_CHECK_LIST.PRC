create or replace
procedure	atualizar_lista_check_list(	nr_sequencia_p	number,
						ie_resultado_item_p	varchar2,
						ds_observacao_p	varchar2) is
						
begin

if	(nvl(nr_sequencia_p,0) > 0) then
	
	if (ie_resultado_item_p is not null) then
	
		update	sl_check_list_unid_item
		set		ie_result_item	= ie_resultado_item_p
		where	nr_sequencia	= nr_sequencia_p;
		
	end if;
	
	if (ds_observacao_p is not null) then
	
		update	sl_check_list_unid_item
		set		ds_observacao	= ds_observacao_p
		where	nr_sequencia	= nr_sequencia_p;
		
	end if;
	
end if;

commit;

end atualizar_lista_check_list;
/
