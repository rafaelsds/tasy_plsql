create or replace
procedure  supplypoint_atual_inf_pac(
				cd_pessoa_fisica_p	number,
				nm_usuario_p		varchar2) is

ie_setor_supplypoint_w		varchar2(1) := 'N';
ds_param_integ_hl7_w		varchar2(4000) := '';
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
cd_setor_w			setor_atendimento.cd_setor_atendimento%type;

begin
	begin	
		nr_atendimento_w := obter_ultimo_atendimento(cd_pessoa_fisica_p);
		cd_setor_w := obter_setor_atendimento(nr_atendimento_w);
	  
		begin
		select	decode(count(*),0,'N','S')
		into	ie_setor_supplypoint_w
		from	far_setores_integracao
		where	nr_seq_empresa_int = 82
		and	cd_setor_atendimento = cd_setor_w
		and	rownum = 1;
		exception
		when others then
			ie_setor_supplypoint_w := 'N';
		end;

		if (ie_setor_supplypoint_w = 'S') then
			ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || cd_pessoa_fisica_p || obter_separador_bv;
			gravar_agend_integracao(634, ds_param_integ_hl7_w);
		end if;
	exception
		when others then
			cd_setor_w := 0;
	end;
end supplypoint_atual_inf_pac;
/
