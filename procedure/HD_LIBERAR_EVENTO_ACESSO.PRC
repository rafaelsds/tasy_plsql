CREATE OR REPLACE PROCEDURE hd_liberar_evento_acesso(nr_seq_p NUMBER) IS
    nr_seq_evento_w         NUMBER;
    nr_seq_acesso_w         NUMBER;
    nr_seq_proc_interno_w   NUMBER;
    cd_pessoa_fisica_w      VARCHAR2(255);
    estabelecimento_ativo_w NUMBER;
    nr_ultimo_atendimento_w NUMBER;
    dt_ent_unidade_w        DATE;
    nr_seq_proc_pac_w       NUMBER;
    ds_erro_w               VARCHAR2(255);
    usuario_ativo_w         VARCHAR2(255);
    cd_setor_atendimento_w  NUMBER;
    nr_seq_atepacu_w        NUMBER;
    cd_convenio_w           NUMBER;
    cd_categoria_w          VARCHAR2(255);
    ie_tipo_atendimento_w   NUMBER;
    nr_seq_classificacao_w  NUMBER;
    cd_procedimento_w       NUMBER;
    ie_origem_proced_w      NUMBER;
    cd_profissional_w       VARCHAR2(255);
    ie_medico_executor_w    VARCHAR2(255);
    cd_cgc_prest_regra_w    VARCHAR2(255);
    cd_medico_executor_w    VARCHAR2(255);
    cd_pes_fis_regra_w      VARCHAR2(255);
    cd_medico_exec_w        VARCHAR2(255);
    cd_medico_laudo_sus_w   VARCHAR2(255);
    cd_local_estoque_w      NUMBER;
BEGIN

    -- select * from hd_evento_acesso_pac where nr_SEQUENCIA = 51
    -- select * from hd_evento_acesso
    -- select * from hd_acesso  

    usuario_ativo_w := obter_usuario_ativo;
    --usuario_ativo_w := 'gmllessa';

    estabelecimento_ativo_w := obter_estabelecimento_ativo;
    --estabelecimento_ativo_w := 1;

    SELECT max(nr_seq_evento),
           max(nr_seq_acesso)
      INTO nr_seq_evento_w,
           nr_seq_acesso_w
      FROM hd_evento_acesso_pac
     WHERE nr_sequencia = nr_seq_p;

    BEGIN
        SELECT nr_seq_proc_interno
          INTO nr_seq_proc_interno_w
          FROM hd_evento_acesso
         WHERE nr_sequencia = nr_seq_evento_w;
    EXCEPTION
        WHEN no_data_found THEN
            nr_seq_proc_interno_w := NULL;
    END;

    IF nr_seq_proc_interno_w IS NOT NULL THEN
        BEGIN
            SELECT max(cd_pessoa_fisica) INTO cd_pessoa_fisica_w FROM hd_acesso WHERE nr_sequencia = nr_seq_acesso_w;
        
            nr_ultimo_atendimento_w := hd_obter_ultimo_atend_pac(cd_pessoa_fisica_w, estabelecimento_ativo_w);
        
            IF ((nr_ultimo_atendimento_w = '0') OR (nr_ultimo_atendimento_w IS NULL)) THEN
                BEGIN
                    -- ds_erro_w          := wheb_mensagem_pck.get_texto(793796)||chr(13);
                    wheb_mensagem_pck.exibir_mensagem_abort(191445);
                END;
            
            ELSE
                BEGIN
                
                    cd_setor_atendimento_w := wheb_usuario_pck.get_cd_setor_atendimento;
                    --cd_setor_atendimento_w := 157;
                
                    SELECT MAX(dt_entrada_unidade),
                           MAX(nr_seq_interno)
                      INTO dt_ent_unidade_w,
                           nr_seq_atepacu_w
                      FROM atend_paciente_unidade
                     WHERE nr_atendimento = nr_ultimo_atendimento_w
                       AND cd_setor_atendimento = cd_setor_atendimento_w;
                
                    IF (nvl(nr_seq_atepacu_w, 0) = 0) THEN
                        gerar_passagem_setor_atend(nr_ultimo_atendimento_w,
                                                   cd_setor_atendimento_w,
                                                   SYSDATE,
                                                   'S',
                                                   usuario_ativo_w);
                    
                        SELECT MAX(dt_entrada_unidade),
                               MAX(nr_seq_interno)
                          INTO dt_ent_unidade_w,
                               nr_seq_atepacu_w
                          FROM atend_paciente_unidade
                         WHERE nr_atendimento = nr_ultimo_atendimento_w
                           AND cd_setor_atendimento = cd_setor_atendimento_w;
                    END IF;
                
                    SELECT MAX(obter_convenio_atendimento(nr_atendimento)),
                           MAX(obter_dados_categ_conv(nr_atendimento, 'CA')),
                           MAX(ie_tipo_atendimento),
                           MAX(nr_seq_classificacao)
                      INTO cd_convenio_w,
                           cd_categoria_w,
                           ie_tipo_atendimento_w,
                           nr_seq_classificacao_w
                      FROM atendimento_paciente
                     WHERE nr_atendimento = nr_ultimo_atendimento_w;
                
                    obter_proc_tab_interno_conv(nr_seq_proc_interno_w,
                                                estabelecimento_ativo_w,
                                                cd_convenio_w,
                                                cd_categoria_w,
                                                NULL,
                                                cd_setor_atendimento_w,
                                                cd_procedimento_w,
                                                ie_origem_proced_w,
                                                NULL,
                                                SYSDATE,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL);
                
                    SELECT MAX(cd_pessoa_fisica)
                      INTO cd_profissional_w
                      FROM usuario
                     WHERE nm_usuario = usuario_ativo_w;
                
                    consiste_medico_executor(estabelecimento_ativo_w,
                                             cd_convenio_w,
                                             cd_setor_atendimento_w,
                                             cd_procedimento_w,
                                             ie_origem_proced_w,
                                             ie_tipo_atendimento_w,
                                             NULL,
                                             nr_seq_proc_interno_w,
                                             ie_medico_executor_w,
                                             cd_cgc_prest_regra_w,
                                             cd_medico_executor_w,
                                             cd_pes_fis_regra_w,
                                             NULL,
                                             SYSDATE,
                                             nr_seq_classificacao_w,
                                             'N',
                                             NULL,
                                             NULL);
                    IF (cd_medico_executor_w IS NOT NULL) AND
                       (cd_medico_exec_w IS NULL) THEN
                        cd_medico_exec_w := cd_medico_executor_w;
                    END IF;
                
                    IF (cd_medico_executor_w IS NULL) AND
                       (ie_medico_executor_w = 'N') THEN
                        cd_medico_exec_w := NULL;
                    END IF;
                
                    IF (ie_medico_executor_w = 'S') THEN
                        SELECT MAX(cd_medico_requisitante)
                          INTO cd_medico_laudo_sus_w
                          FROM sus_laudo_paciente
                         WHERE nr_atendimento = nr_ultimo_atendimento_w
                           AND cd_procedimento_solic = cd_procedimento_w
                           AND ie_origem_proced = ie_origem_proced_w;
                    
                        cd_medico_exec_w := nvl(cd_medico_laudo_sus_w, cd_medico_exec_w);
                    END IF;
                
                    IF (ie_medico_executor_w = 'M') THEN
                        BEGIN
                            cd_medico_laudo_sus_w := sus_obter_dados_sismama_atend(nr_ultimo_atendimento_w, 'M', 'CMR');
                            cd_medico_exec_w      := nvl(cd_medico_laudo_sus_w, cd_medico_exec_w);
                        END;
                    END IF;
                
                    IF (ie_medico_executor_w = 'A') AND
                       (cd_medico_exec_w IS NULL) THEN
                        SELECT MAX(cd_medico_resp)
                          INTO cd_medico_exec_w
                          FROM atendimento_paciente
                         WHERE nr_atendimento = nr_ultimo_atendimento_w;
                    END IF;
                
                    IF (ie_medico_executor_w = 'F') AND
                       (cd_medico_executor_w IS NOT NULL) THEN
                        cd_medico_exec_w := cd_medico_executor_w;
                    END IF;
                
                    IF (cd_pessoa_fisica_w IS NOT NULL) OR
                       (cd_profissional_w IS NOT NULL) AND
                       (ie_medico_executor_w = 'Y') THEN
                        cd_pes_fis_regra_w := NULL;
                        cd_profissional_w  := NULL;
                    END IF;
                
                    IF (cd_pessoa_fisica_w IS NOT NULL) OR
                       (cd_profissional_w IS NOT NULL) AND
                       (ie_medico_executor_w = 'Y') THEN
                        cd_pes_fis_regra_w := NULL;
                        cd_profissional_w  := NULL;
                    END IF;
                
					select procedimento_paciente_seq.nextval
					into nr_seq_proc_pac_w
					from dual;
                    INSERT INTO procedimento_paciente
                        (nr_sequencia,
                         nr_atendimento,
                         dt_entrada_unidade,
                         cd_procedimento,
                         dt_procedimento,
                         qt_procedimento,
                         dt_atualizacao,
                         nm_usuario,
                         cd_setor_atendimento,
                         ie_origem_proced,
                         nr_seq_atepacu,
                         nr_seq_proc_interno,
                         cd_convenio,
                         cd_categoria,
                         cd_pessoa_fisica,
                         cd_medico_executor,
                         cd_cgc_prestador)
                    VALUES
                        (nr_seq_proc_pac_w,
                         nr_ultimo_atendimento_w,
                         dt_ent_unidade_w,
                         cd_procedimento_w,
                         SYSDATE,
                         1,
                         SYSDATE,
                         usuario_ativo_w,
                         cd_setor_atendimento_w,
                         ie_origem_proced_w,
                         nr_seq_atepacu_w,
                         nr_seq_proc_interno_w,
                         cd_convenio_w,
                         cd_categoria_w,
                         nvl(cd_profissional_w, cd_pes_fis_regra_w),
                         cd_medico_exec_w,
                         cd_cgc_prest_regra_w);
                
                    SELECT MAX(cd_local_estoque)
                      INTO cd_local_estoque_w
                      FROM setor_atendimento
                     WHERE cd_setor_atendimento = cd_setor_atendimento_w;
                
                    consiste_exec_procedimento(nr_seq_proc_pac_w, ds_erro_w);
                    atualiza_preco_procedimento(nr_seq_proc_pac_w, cd_convenio_w, usuario_ativo_w);
                    gerar_lancamento_automatico(nr_ultimo_atendimento_w,
                                                cd_local_estoque_w,
                                                34,
                                                usuario_ativo_w,
                                                nr_seq_proc_pac_w,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL);
                
                END;
            END IF;
        
        END;
    END IF;

    UPDATE hd_evento_acesso_pac
       SET dt_liberacao   = SYSDATE,
           nm_usuario_lib = usuario_ativo_w,
           nr_seq_propaci = nr_seq_proc_pac_w
     WHERE nr_sequencia = nr_seq_p;

    COMMIT;
END;
/
