create or replace
procedure pls_oc_cta_tratar_val_102_imp(nr_seq_combinada_p	pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p	pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Realiza uma compara��o entre os procedimento da conta consistida e sua autoriza��o, 
	quando a conta consistida possuir algum procedimento que n�o existe na autoriza��o, ser� 
	verificado se a autoriza��o possui algum procedimento semelhante, com base no cadastro 
	de itens semelhantes na pasta Regras> Ocorr�ncia> Itens semelhantes.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
qt_encontrado_w		pls_integer;
i			pls_integer;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;

-- cursor da regra
cursor	c01 (	nr_seq_oc_cta_comb_p	pls_oc_cta_combinada.nr_sequencia%type) is
	select	nr_sequencia,
		nr_seq_regra_semelhante
	from 	pls_oc_cta_val_item_semel
	where	nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

-- cursor com os procedimentos a serem avaliados
cursor	c02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	proc.ie_origem_proced_conv 	ie_origem_proced,
		proc.cd_procedimento_conv	cd_procedimento,
		conta.nr_seq_guia_conv		nr_seq_guia,
		sel.nr_sequencia		nr_seq_selecao
	from	pls_oc_cta_selecao_imp	sel,
		pls_conta_proc_imp	proc,
		pls_conta_imp		conta
	where	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido		= 'S'
	and	proc.nr_seq_conta	= sel.nr_seq_conta
	and	proc.nr_sequencia	= sel.nr_seq_conta_proc
	and	proc.nr_seq_conta	= conta.nr_sequencia
	and	conta.nr_seq_guia_conv is not null;

begin

-- somente executa se tiver regra
if	(nr_seq_combinada_p is not null) then
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', ie_regra_excecao_p, nr_id_transacao_p, null);	
	
	for r_c01_w in c01 (nr_seq_combinada_p) loop

		i := 0;
		pls_ocor_imp_pck.limpar_nested_tables(tb_seq_selecao_w, tb_valido_w, tb_observacao_w);		
		for r_c02_w in c02 (nr_id_transacao_p) loop
	
			-- Verifica se o procedimento testado est� na autoriza��o
			select	count(1)
			into	qt_encontrado_w
			from	table(pls_conta_autor_pck.obter_dados(r_c02_w.nr_seq_guia, 'P', cd_estabelecimento_w, r_c02_w.ie_origem_proced, r_c02_w.cd_procedimento)) a;
			
			-- se n�o encontrou nenhum, ent�o olhar se tem item semelhante
			if	(qt_encontrado_w = 0) then
				-- conta se existe algum item semelhante em rela��o ao validado e a autoriza��o
				select	count(1)
				into	qt_encontrado_w
				from	table(pls_conta_autor_pck.obter_dados(r_c02_w.nr_seq_guia, 'P', cd_estabelecimento_w, null, null)) a
				where	pls_obter_proc_semelhante(r_c01_w.nr_seq_regra_semelhante, cd_estabelecimento_w, r_c02_w.ie_origem_proced, r_c02_w.cd_procedimento, a.ie_origem_proced, a.cd_procedimento) = 'S';
				
				-- se tem semelhantes, gera a ocorrencia
				if	(qt_encontrado_w > 0) then
					tb_valido_w(i)		:= 'S';	
					tb_observacao_w(i)	:= 'O item apresentado n�o est� autorizado, mas foi encontrado um semelhante na autoriza��o.';
					tb_seq_selecao_w(i)	:= r_c02_w.nr_seq_selecao;
				end if;
				
			end if; 
			
			-- Quando a quantidade de itens da lista tiver chegado ao m�ximo definido na pls_util_pck, ent�o os registros s�o persistidos no banco
			if	(i = pls_util_pck.qt_registro_transacao_w) then
				
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w, nr_id_transacao_p, 'SEQ');
				i := 0;
				
				pls_ocor_imp_pck.limpar_nested_tables(tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
			else
				i := i + 1;
			end if;
		
		end loop; -- c02 - procedimentos
		
		-- Quando tiver sobrado algo na lista ir� gravar o que restou ap�s a execu��o do loop.
		if	(i > 0) then
			pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w, nr_id_transacao_p, 'SEQ');
		end if;
		
	end loop; -- c01 regras 

	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', ie_regra_excecao_p, null, nr_id_transacao_p, null);	
end if; 

end pls_oc_cta_tratar_val_102_imp;
/
