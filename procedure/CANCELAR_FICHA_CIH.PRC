create or replace
procedure cancelar_ficha_cih(
			nr_ficha_ocorrencia_p		number,
			nr_seq_motivo_cancel_p		number,
			nm_usuario_p			varchar2) is 

begin

if	(nr_ficha_ocorrencia_p is not null) then
	begin
	update	cih_ficha_ocorrencia
	set	dt_cancelamento		= sysdate,
		nm_usuario_cancelamento = nm_usuario_p,
		nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p
	where	nr_ficha_ocorrencia	= nr_ficha_ocorrencia_p;
	end;
end if;
commit;

end cancelar_ficha_cih;
/