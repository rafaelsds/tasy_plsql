create or replace
procedure Man_Gerar_Projeto_OS(	nr_seq_ordem_p			number,
								cd_responsavel_p		varchar2,
								nm_usuario_p			varchar2,
								cd_estabelecimento_p	number) is

dt_ordem_servico_w			date;
nr_seq_grupo_des_w			number(10,0);
cd_pessoa_solicitante_w		varchar2(10);
ds_dano_breve_w				varchar2(80);
nr_seq_localizacao_w		number(10,0);
nr_seq_projeto_w			number(10,0);
nr_seq_cliente_w			number(10,0);
cd_cnpj_w					varchar2(14);
nr_seq_gerencia_w			number(10,0);
nm_pessoa_usuario_w			varchar2(100);
nr_seq_hist_w				number(10,0);
nr_seq_equipe_w				number(10);
nr_seq_papel_w				number(10);
nm_usuario_w				varchar2(15);
cd_gerente_w				varchar2(10);
ds_setor_atendimento_w		varchar2(100);
ds_fone_w					varchar2(40);
ds_email_w					varchar2(255);
cd_analista_w				varchar2(10);
cd_funcao_w					number(5);

begin
select	dt_ordem_servico,
		nr_seq_grupo_des,
		cd_pessoa_solicitante,
		substr(ds_dano_breve,1,80),
		nr_seq_localizacao,
		cd_funcao
into	dt_ordem_servico_w,
		nr_seq_grupo_des_w,
		cd_pessoa_solicitante_w,
		ds_dano_breve_w,
		nr_seq_localizacao_w,
		cd_funcao_w
from	man_ordem_servico
where	nr_sequencia = nr_seq_ordem_p;

select	proj_projeto_seq.nextval
into	nr_seq_projeto_w
from	dual;

select	max(cd_cnpj)
into	cd_cnpj_w
from	man_localizacao
where	nr_sequencia = nr_seq_localizacao_w;

select	nvl(max(nr_seq_cliente),0)
into	nr_seq_cliente_w
from	man_ordem_servico
where	nr_sequencia = nr_seq_ordem_p;

if	(nr_seq_cliente_w = 0) then
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_cliente_w
	from	com_cliente
	where	cd_cnpj	= cd_cnpj_w;

	if	(nr_seq_cliente_w = 0) then
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_cliente_w
		from	com_cliente
		where	cd_cnpj	in (	select	cd_cnpj
					from	man_localizacao_pj
					where	nr_seq_localizacao = nr_seq_localizacao_w);
	end if;
	end;
end if;

select	max(nr_seq_gerencia)
into	nr_seq_gerencia_w
from	grupo_desenvolvimento
where	nr_sequencia = nr_seq_grupo_des_w;

/* Cria��o do projeto baseado na ordem de servi�o */
insert	into proj_projeto
	(nr_sequencia,
	nr_seq_cliente,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_coordenador,
	cd_gerente_cliente,
	ds_titulo,
	dt_inicio_prev,
	dt_fim_prev,
	ie_status,
	dt_inicio_real,
	dt_fim_real,
	nr_seq_classif,
	nm_curto_agenda,
	dt_projeto,
	dt_repasse_comercial,
	nr_seq_forma_cont,
	ds_observacao,
	dt_virada,
	ie_escore,
	ie_origem,
	nr_seq_gerencia,
	nr_seq_ordem_serv,
	nr_seq_estagio,
	nr_seq_grupo_des,
	ie_relat_completo,
	cd_estabelecimento,
	cd_funcao)
values	(nr_seq_projeto_w,
	nr_seq_cliente_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_responsavel_p,
	cd_pessoa_solicitante_w,
	ds_dano_breve_w,
	trunc(sysdate, 'dd'),
	null,
	'I',
	null, null, 11, null,
	trunc(sysdate, 'dd'),
	null, null, 'Projeto gerado a partir da OS' || nr_seq_ordem_p,
	null, null, 'D',
	nr_seq_gerencia_w,
	nr_seq_ordem_p,
	1,
	nr_seq_grupo_des_w,
	'S',
	cd_estabelecimento_p,
	cd_funcao_w);

select	proj_equipe_seq.nextval
into	nr_seq_equipe_w
from	dual;

insert	into proj_equipe -- Equipe de an�lise
	(ds_objetivo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_inicio,
	dt_liberacao,
	ie_interna_externa,
	ie_virada,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_apres,
	nr_seq_cliente,
	nr_seq_equipe_funcao,
	nr_seq_proj,
	nr_sequencia,
	nr_seq_virada)
values	(null,
	sysdate,
	sysdate,
	null,
	null,
	'I',
	'N',
	nm_usuario_p,
	nm_usuario_p,
	1,
	null,
	1,
	nr_seq_projeto_w,
	nr_seq_equipe_w,
	null);

select	proj_equipe_papel_seq.nextval
into	nr_seq_papel_w
from	dual;

select	max(a.nm_usuario)
into	nm_usuario_w
from	usuario a
where	a.cd_pessoa_fisica = cd_pessoa_solicitante_w;

cd_gerente_w := cd_pessoa_solicitante_w;

/*
	Realizado ajuste, uma vez que, foi alterado o PD e n�o s�o apenas os gerentes que podem solicitar a abertura do projeto

	select	trim(sis_obter_gerente(nm_usuario_w,'C'))
	into	cd_gerente_w
	from	dual;
*/

select	max(b.ds_setor_atendimento)
into	ds_setor_atendimento_w
from  	setor_atendimento b, 
	usuario a 
where 	a.cd_setor_atendimento = b.cd_setor_atendimento 
and   	a.cd_pessoa_fisica = cd_gerente_w;

select	max(decode(obter_compl_pf(cd_gerente_w,2,'DDT'),null,null, '(' || obter_compl_pf(cd_gerente_w,2,'DDT') || ')' ) || obter_compl_pf(cd_gerente_w,2,'T') ||
	' / ' || decode(a.nr_ddd_celular,null,null,'(' || a.nr_ddd_celular || ')' ) || a.nr_telefone_celular)
into	ds_fone_w
from	pessoa_fisica a
where	a.cd_pessoa_fisica = cd_gerente_w
order by 1;

select	max(obter_compl_pf(cd_gerente_w,2,'M'))
into	ds_email_w
from	dual
order by 1;

insert	into proj_equipe_papel -- insere gerente
	(cd_pessoa_fisica,
	ds_atuacao,
	ds_cargo,
	ds_email,
	ds_fone,
	ds_setor,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_fim,
	dt_fim_resp,
	dt_inicio_resp,
	ie_alerta,
	ie_alocado_original,
	ie_funcao_rec_migr,
	ie_situacao,
	nm_guerra,
	nm_pessoa,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_apres,
	nr_seq_cadastro,
	nr_seq_equipe,
	nr_seq_funcao,
	nr_sequencia,
	qt_horas_rec_proj)
values	(cd_gerente_w,
	null,
	null,
	ds_email_w,
	ds_fone_w,
	ds_setor_atendimento_w,
	sysdate,
	sysdate,
	null,
	null,
	null,
	null,
	'S',
	null,
	'A',
	null,
	null,
	nm_usuario_p,
	nm_usuario_p,
	1,
	null,
	199,
	51,
	nr_seq_papel_w,
	null);

select	proj_equipe_papel_seq.nextval
into	nr_seq_papel_w
from	dual;

select	max(a.cd_pessoa_fisica)
into	cd_analista_w
from	usuario a,
	man_ordem_serv_tecnico b
where	a.nm_usuario = b.nm_usuario
and	b.nr_sequencia  = (	select	max(x.nr_sequencia)
				from	man_ordem_serv_tecnico x
				where	x.nr_seq_tipo = 21
				and	x.dt_liberacao is not null
				and	x.nr_seq_ordem_serv = nr_seq_ordem_p);

select	max(b.ds_setor_atendimento)
into	ds_setor_atendimento_w
from  	setor_atendimento b, 
	usuario a 
where 	a.cd_setor_atendimento = b.cd_setor_atendimento 
and   	a.cd_pessoa_fisica = cd_analista_w;

select	decode(obter_compl_pf(cd_analista_w,2,'DDT'),null,null, '(' || obter_compl_pf(cd_analista_w,2,'DDT') || ')' ) || obter_compl_pf(cd_analista_w,2,'T') ||
	' / ' || decode(a.nr_ddd_celular,null,null,'(' || a.nr_ddd_celular || ')' ) || a.nr_telefone_celular
into	ds_fone_w
from	pessoa_fisica a
where	a.cd_pessoa_fisica = cd_analista_w
order by 1;

select	obter_compl_pf(cd_analista_w,2,'M')
into	ds_email_w
from	dual
order by 1;

insert	into proj_equipe_papel -- insere Analista
	(cd_pessoa_fisica,
	ds_atuacao,
	ds_cargo,
	ds_email,
	ds_fone,
	ds_setor,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_fim,
	dt_fim_resp,
	dt_inicio_resp,
	ie_alerta,
	ie_alocado_original,
	ie_funcao_rec_migr,
	ie_situacao,
	nm_guerra,
	nm_pessoa,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_apres,
	nr_seq_cadastro,
	nr_seq_equipe,
	nr_seq_funcao,
	nr_sequencia,
	qt_horas_rec_proj)
values	(cd_analista_w,
	null,
	null,
	ds_email_w,
	ds_fone_w,
	ds_setor_atendimento_w,
	sysdate,
	sysdate,
	null,
	null,
	null,
	null,
	'S',
	null,
	'A',
	null,
	null,
	nm_usuario_p,
	nm_usuario_p,
	5,
	null,
	199,
	44,
	nr_seq_papel_w,
	null);

select	proj_equipe_seq.nextval
into	nr_seq_equipe_w
from	dual;

insert	into proj_equipe -- Equipe do solicitante
	(ds_objetivo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_inicio,
	dt_liberacao,
	ie_interna_externa,
	ie_virada,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_apres,
	nr_seq_cliente,
	nr_seq_equipe_funcao,
	nr_seq_proj,
	nr_sequencia,
	nr_seq_virada)
values	(null,
	sysdate,
	sysdate,
	null,
	null,
	'I',
	'N',
	nm_usuario_p,
	nm_usuario_p,
	10,
	null,
	1,
	nr_seq_projeto_w,
	nr_seq_equipe_w,
	null);

select	proj_equipe_papel_seq.nextval
into	nr_seq_papel_w
from	dual;

select	max(b.ds_setor_atendimento)
into	ds_setor_atendimento_w
from  	setor_atendimento b, 
	usuario a 
where 	a.cd_setor_atendimento = b.cd_setor_atendimento 
and   	a.cd_pessoa_fisica = cd_pessoa_solicitante_w;

select	decode(obter_compl_pf(cd_pessoa_solicitante_w,2,'DDT'),null,null, '(' || obter_compl_pf(cd_pessoa_solicitante_w,2,'DDT') || ')' ) || obter_compl_pf(cd_pessoa_solicitante_w,2,'T') ||
	' / ' || decode(a.nr_ddd_celular,null,null,'(' || a.nr_ddd_celular || ')' ) || a.nr_telefone_celular
into	ds_fone_w
from	pessoa_fisica a
where	a.cd_pessoa_fisica = cd_pessoa_solicitante_w
order by 1;

select	obter_compl_pf(cd_pessoa_solicitante_w,2,'M')
into	ds_email_w
from	dual
order by 1;

insert	into proj_equipe_papel -- insere Analista
	(cd_pessoa_fisica,
	ds_atuacao,
	ds_cargo,
	ds_email,
	ds_fone,
	ds_setor,
	dt_atualizacao,
	dt_atualizacao_nrec,
	dt_fim,
	dt_fim_resp,
	dt_inicio_resp,
	ie_alerta,
	ie_alocado_original,
	ie_funcao_rec_migr,
	ie_situacao,
	nm_guerra,
	nm_pessoa,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_apres,
	nr_seq_cadastro,
	nr_seq_equipe,
	nr_seq_funcao,
	nr_sequencia,
	qt_horas_rec_proj)
values	(cd_pessoa_solicitante_w,
	null,
	null,
	ds_email_w,
	ds_fone_w,
	ds_setor_atendimento_w,
	sysdate,
	sysdate,
	null,
	null,
	null,
	null,
	'S',
	null,
	'A',
	null,
	null,
	nm_usuario_p,
	nm_usuario_p,
	1,
	null,
	199,
	24,
	nr_seq_papel_w,
	null);

select	man_ordem_serv_tecnico_seq.nextval
into	nr_seq_hist_w
from 	dual;

select	obter_nome_pf(cd_pessoa_fisica)
into 	nm_pessoa_usuario_w
from 	usuario
where	nm_usuario = nm_usuario_p;

/* Inclus�o do hist�rico de abertura do projeto na ordem de servi�o - Conforme norma */
Insert into man_ordem_serv_tecnico
	(nr_sequencia,
	nr_seq_ordem_serv,
	dt_atualizacao,
	nm_usuario,
	ds_relat_tecnico,
	cd_versao_tasy,
	dt_historico,
	ie_origem,
	nr_seq_tipo,
	nm_usuario_lib,
	dt_liberacao,
	ie_grau_satisfacao)
values
	(nr_seq_hist_w,
	nr_seq_ordem_p,
	sysdate,
	nm_usuario_p,
	'Abertura de projeto por ' || nm_pessoa_usuario_w,
	null,
	sysdate,
	'I',
	4, nm_usuario_p, sysdate, null);

/* Mudan�a do status da ordem de servi�o para Desenvolvimento em projeto - Conforme norma */
update	man_ordem_servico
set	nr_seq_estagio = 4
where	nr_sequencia = nr_seq_ordem_p;

/* Vincula��o da ordem de servi�o ao projeto - Conforme norma */
/* jcaraujo 26/09/11 - OS362361 - Registrar a Ordem de servi�o na documenta��o do projeto */
insert 	into proj_ordem_servico
	(nr_sequencia,           
	nr_seq_proj,            
	dt_atualizacao,         
	nm_usuario,             
	dt_atualizacao_nrec,
	nm_usuario_nrec,        
	nr_seq_ordem,           
	nr_seq_cron_etapa,      
	dt_acordo_projeto,      
	dt_proposta_cliente,
	nr_seq_setor_cli,       
	ie_virada,
	ie_origem_ordem)
Values	(proj_ordem_servico_seq.nextval,
	nr_seq_projeto_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_ordem_p,
	null,
	null,
	null,
	null,
	'N',
	'P');
	
/* Inclus�o do hist�rico de abertura do projeto no projeto - Conforme norma */		

if	(nr_seq_cliente_w = 0) then
	begin
	insert 	into com_cliente_hist
		(nr_sequencia,           
		nr_seq_cliente,         
		dt_atualizacao,         
		nm_usuario,             
		dt_historico,           
		nr_seq_canal,           
		nr_seq_tipo,            
		ds_titulo,              
		ds_historico,           
		dt_atualizacao_nrec,
		nm_usuario_nrec,        
		nr_seq_ativ,            
		dt_liberacao,           
		nr_seq_classif,         
		ie_status_projeto,      
		nr_seq_projeto)
	values	(com_cliente_hist_seq.nextval,
		nr_seq_cliente_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		null,
		8,
		'Abertura de projeto',
		'Abertura de projeto por ' || nm_pessoa_usuario_w,
		sysdate,
		nm_usuario_p,
		null,
		sysdate,
		null,
		'I',
		nr_seq_projeto_w);
	end;
end if;

insert into com_cliente_hist(	nr_sequencia, 
	nr_seq_cliente, 
	dt_atualizacao, 
	nm_usuario, 
	dt_historico, 
	nr_seq_tipo, 
	ds_historico,
	ds_titulo,
	dt_liberacao,
	nr_seq_projeto)
values(	com_cliente_hist_seq.nextval,
	nr_seq_cliente_w, 
	sysdate, 
	nm_usuario_p, 
	sysdate, 
	'1', 
	'Projeto aberto atraves da OS: ' || nr_seq_ordem_p,
	'Hist�rico de abertura',
	sysdate,
	nr_seq_projeto_w);

--proj_desenv_gerar_cronograma(nr_seq_projeto_w, 'I', nm_usuario_p);
/* OS690983/694213/722048
proj_desenv_gerar_cronograma(nr_seq_projeto_w, 'P', nm_usuario_p);
proj_desenv_gerar_cronograma(nr_seq_projeto_w, 'E', nm_usuario_p);
*/	
commit;

end Man_Gerar_Projeto_OS;
/