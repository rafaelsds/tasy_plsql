create or replace
procedure fleury_remover_dt_integracao (	nr_prescricao_p		number) is 

Cursor C01 is
	select	a.nr_prescricao,
		a.nr_sequencia
	from	prescr_procedimento a
	where	a.nr_seq_exame is not null
	and 	obter_sigla_exame_fleury(a.nr_prescricao, a.nr_sequencia) IS NOT NULL
	and	a.nr_prescricao = nr_prescricao_p
	union
	select	a.nr_prescricao,
		a.nr_sequencia
	from	prescr_procedimento a
	where	a.nr_seq_exame is null
	and 	obter_sigla_exame_fleury_diag(a.nr_prescricao, a.nr_sequencia) IS NOT NULL
	and	a.nr_prescricao = nr_prescricao_p;
	
c01_w		c01%rowtype;
nr_seq_log_w	number(20);

begin

open c01;
loop
fetch c01 into c01_w;
	exit when c01%notfound;
	begin
	
	update	prescr_procedimento
	set	dt_integracao = null
	where	nr_prescricao = c01_w.nr_prescricao
	and	nr_sequencia = c01_w.nr_sequencia;
	
	lab_gravar_fleury_log(	'Enviado pela conta paciente: '||to_char(c01_w.nr_prescricao)||' - '||to_char(c01_w.nr_sequencia),
				' ',
				' ',
				' ',
				wheb_usuario_pck.get_nm_usuario,
				c01_w.nr_prescricao,
				'0',
				nr_seq_log_w);
	end;
end loop;
close c01;

commit;

end fleury_remover_dt_integracao;
/