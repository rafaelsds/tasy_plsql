
create or replace
procedure Lab_atual_seq_equip_lote_lsf (nr_seq_lote_p	number,
				nm_usuario_p	Varchar2) is 

										
nr_prescricao_w		varchar2(20);										
										
										
Cursor C01 is
	select	distinct nr_prescricao
	from	prescr_procedimento
	where	nr_seq_lote_externo = nr_seq_lote_p
	order by 1;

begin

open C01;
loop
fetch C01 into	
	nr_prescricao_w;
exit when C01%notfound;
	begin
		Lab_atual_seq_atual_equip('LSFRANCO',nm_usuario_p, nr_prescricao_w);
	end;
end loop;
close C01;


commit;

end Lab_atual_seq_equip_lote_lsf;
/