create or replace
procedure dacon_registro_r30(	nm_usuario_p		varchar2,
				nr_seq_dacon_p		number,
				cd_estabelecimento_p	number) is

cd_cgc_w		varchar2(14);
cd_darf_w		varchar2(4);
cd_tributo_pis_w		number(3);
cd_tributo_cofins_w		number(3);	
base_calculo_w		varchar2(14);
vl_pis_w			varchar2(14);
vl_cofins_w		varchar2(14);
contador_w		number(10) := 1;
	
cursor C01 is

select 	rpad(nvl(cd_cgc,0),14,0),
	lpad(nvl(cd_darf,0),4,0),
	lpad(replace(campo_mascara(nvl(sum(vb),'0'),2),'.',''),14,0),
	lpad(replace(campo_mascara(nvl(sum(pis),'0'),2),'.',''),14,0), 
	lpad(replace(campo_mascara(nvl(sum(cofins),'0'),2),'.',''),14,0) 
from	(select	distinct 
		b.cd_cgc cd_cgc,
		b.nr_sequencia,
		obter_codigo_darf(c.cd_tributo,b.cd_estabelecimento,b.cd_cgc,b.cd_pessoa_fisica) cd_Darf,
		obter_vl_tributo(b.nr_sequencia, nvl(cd_tributo_pis_w, cd_tributo_cofins_w), 'VB') vb,
		obter_vl_tributo(b.nr_sequencia,cd_tributo_pis_w) pis,
		obter_vl_tributo(b.nr_sequencia,cd_tributo_cofins_w) cofins
	from	nota_fiscal b,
		nota_fiscal_trib t,
		dacon_nota_fiscal a,
		tributo c
	where	b.nr_sequencia = t.nr_sequencia
	and	b.nr_sequencia = a.nr_seq_nota_fiscal
	and	t.cd_tributo = c.cd_tributo
	and	(c.cd_tributo = cd_tributo_pis_w or c.cd_tributo = cd_tributo_cofins_w)
	and	a.nr_seq_dacon = nr_seq_dacon_p)
having	sum(pis) > 0
and	sum(cofins) > 0
group by cd_cgc,cd_darf
order by cd_darf,cd_cgc;

begin				

select	cd_tributo_pis,
	cd_tributo_cofins
into	cd_tributo_pis_w,
	cd_tributo_cofins_w	
from	dacon
where	nr_sequencia = nr_seq_dacon_p;	

open C01;
loop
fetch C01 into
	cd_cgc_w,
	cd_darf_w,
	base_calculo_w,
	vl_pis_w,
	vl_cofins_w;
exit when C01%notfound;

	insert into w_dacon 	(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_arquivo,
			nr_linha,
			ie_tipo_registro
			)
	values		(w_dacon_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			rpad('R30',4,' ')||lpad(0,4,0)||cd_cgc_w||cd_darf_w||base_calculo_w||vl_pis_w||vl_cofins_w||lpad(' ',10,' '),
			contador_w,
			40
			);
	
	contador_w := contador_w + 1;
	
	if (mod(contador_w,100) = 0) then
		commit;
	end if;	
	
end loop;
close C01;
commit;

end dacon_registro_r30;
/