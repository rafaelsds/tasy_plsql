create or replace
procedure validar_regra_diretiva_eup (	cd_pessoa_fisica_p	varchar2,					
					nm_usuario_p		varchar2) is 

qt_dias_validade_w  	diretriz_atendimento_param.qt_dias_validade%type;
ie_novo_atendimento_w	diretriz_atendimento_param.ie_novo_atendimento%type;
ie_diretriz_w		diretriz_atendimento_param.ie_diretriz%type;
dt_alta_w		atendimento_paciente.dt_alta%type;
dt_diretriz_w		atendimento_paciente.dt_atualizacao%type;
nr_seq_diretriz_w	diretriz_atendimento.nr_sequencia%type;
qt_dias_ult_atend_w	number(10);


cursor c01 is
select 	qt_dias_validade,
	ie_novo_atendimento,
	ie_diretriz
from 	diretriz_atendimento_param
where	ie_situacao = 'A';		
					
begin

	open C01;
	loop
	fetch C01 into	
		qt_dias_validade_w,
		ie_novo_atendimento_w,
		ie_diretriz_w;
	exit when C01%notfound;
		begin
			select	max(nr_sequencia)
			into	nr_seq_diretriz_w
			from	diretriz_atendimento
			where	cd_pessoa_fisica = cd_pessoa_fisica_p		
			and	ie_diretriz = ie_diretriz_w
			and	ie_status = 'C'
			and	dt_liberacao is not null;
			
			if	(nr_seq_diretriz_w is not null) then
				if	(ie_novo_atendimento_w = 'S') then
					alterar_status_diretriz(nr_seq_diretriz_w, 'P', null, nm_usuario_p);
				else
					select 	max(dt_alta)
					into	dt_alta_w
					from	atendimento_paciente a,
						diretriz_atendimento b
					where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
					and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
					and	b.nr_sequencia = nr_seq_diretriz_w
					and	dt_alta >= b.dt_atualizacao
					and	ie_tipo_atendimento in (1,3); --Internado, Pronto socorro;
					
					if	(dt_alta_w is not null) then 
						qt_dias_ult_atend_w := sysdate - dt_alta_w;
						
						if	(qt_dias_ult_atend_w > qt_dias_validade_w) then
							alterar_status_diretriz(nr_seq_diretriz_w, 'P', null, nm_usuario_p);
						end if;
					end if;
				end if;
			end if;
		end;
	end loop;
	close C01;

commit;

end validar_regra_diretiva_eup;
/