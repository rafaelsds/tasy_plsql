create or replace
procedure gravar_log_env_cot_compra(	nr_cot_compra_p		number,
				ds_arquivo_p		varchar2,
				ds_conteudo_p		Long,
				ie_envio_retorno_p		varchar2,
				nm_usuario_p		varchar2) is 

begin

insert into cot_compra_arq (	nr_sequencia,
			nr_cot_compra,
			dt_atualizacao,
			nm_usuario,
			ds_arquivo,
			ds_conteudo,
			ie_envio_retorno,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
values(	cot_compra_arq_seq.nextval,
	nr_cot_compra_p,
	sysdate,
	nm_usuario_p,
	ds_arquivo_p,
	ds_conteudo_p,
	ie_envio_retorno_p,
	sysdate,
	nm_usuario_p);

commit;

end gravar_log_env_cot_compra;
/