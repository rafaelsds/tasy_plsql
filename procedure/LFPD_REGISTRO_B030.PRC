create or replace
procedure lfpd_registro_B030(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				ds_separador_p		varchar2, 
				qt_linha_p		in out number,
				nr_sequencia_p		in out number) is

contador_w		number(10)	:= 0;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10)	:= qt_linha_p;
nr_seq_registro_w	number(10)	:= nr_sequencia_p;
sep_w			varchar2(1)	:= ds_separador_p;
--nr_primeiro_doc_w	nota_fiscal.nr_sequencia%type;
--nr_ultimo_doc_w		nota_fiscal.nr_sequencia%type;

cursor c01 is
	select	'B030' cd_registro,
		m.cd_modelo_nf cd_modelo,
		n.cd_serie_nf cd_serie,
		n.cd_serie_referencia cd_subserie,
		to_char(n.dt_emissao,'ddMMyyyy') dt_prestador_serv,
		substr(ELIMINA_CARACTERE_ESPECIAL(a.cd_cfps), 1, 4) cd_cfps,		
		'' nr_lanc_contabil,
		replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',') vl_base_calculo,		
		replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),2),'.',',') vl_tributo,
		replace(Campo_Mascara(sum(n.vl_total_nota),2),'.',',') vl_total_documentos,
		replace(Campo_Mascara(sum(n.vl_total_nota)-sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',') vl_nao_tributadas,
		sum((	select	count(*)
			from	dual
			where	n.ie_situacao = 9)) qt_calceladas,
		min(n.nr_sequencia) nr_primeiro_doc,
		max(n.nr_sequencia) nr_ultimo_doc,
		/*decode(n.ds_observacao,'','',n.nr_sequencia)*/ '' ds_observacao
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o,
		natureza_operacao a,
		modelo_nota_fiscal m
	where	i.nr_sequencia 		= n.nr_sequencia
	and	n.cd_operacao_nf 	= o.cd_operacao_nf
	and	a.cd_natureza_operacao 	= i.cd_natureza_operacao
	and 	n.nr_seq_modelo		= m.nr_sequencia
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'
	and 	i.cd_procedimento 	is not null
	and	to_char(n.dt_emissao,'dd/MM/yy') between to_date(dt_inicio_p,'dd/MM/yy') and to_date(dt_fim_p,'dd/MM/yy')
	and	n.cd_estabelecimento 	= cd_estabelecimento_p
	and 	o.ie_servico 		= 'S'
	and	n.vl_total_nota 	> 0
	and 	m.cd_modelo_nf 	= '3A'
	group by	
		'B030',
		m.cd_modelo_nf,
		n.cd_serie_nf,
		n.cd_serie_referencia,
		to_char(n.dt_emissao,'ddMMyyyy'),
		a.cd_cfps;

vet01	c01%RowType;

cursor c02 is
	select	m.cd_modelo_nf cd_modelo,
		n.cd_serie_nf cd_serie,
		n.cd_serie_referencia cd_subserie,
		to_char(n.dt_emissao,'ddMMyyyy') dt_prestador_serv,
		substr(ELIMINA_CARACTERE_ESPECIAL(a.cd_cfps), 1, 4) cd_cfps,				
		replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',') vl_base_calculo,		
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS'),2),'.',',') aliquota_iss,
		replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),2),'.',',') vl_tributo,
		replace(Campo_Mascara(sum(n.vl_total_nota),2),'.',',') vl_total_documentos,
		replace(Campo_Mascara(sum(n.vl_total_nota)-sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',') vl_nao_tributadas		
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o,
		natureza_operacao a,
		modelo_nota_fiscal m
	where	i.nr_sequencia 		= n.nr_sequencia
	and	n.cd_operacao_nf 	= o.cd_operacao_nf
	and	a.cd_natureza_operacao 	= i.cd_natureza_operacao
	and 	n.nr_seq_modelo		= m.nr_sequencia
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'
	and 	i.cd_procedimento 	is not null
	and	to_char(n.dt_emissao,'dd/MM/yy') between to_date(dt_inicio_p,'dd/MM/yy') and to_date(dt_fim_p,'dd/MM/yy')
	and	n.cd_estabelecimento 	= cd_estabelecimento_p
	and 	o.ie_servico 		= 'S'
	and	n.vl_total_nota 	> 0
	and 	m.cd_modelo_nf 	= '3A'
	group by		
		m.cd_modelo_nf,
		n.cd_serie_nf,
		n.cd_serie_referencia,
		to_char(n.dt_emissao,'ddMMyyyy'),
		a.cd_cfps,
		obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS');	

vet02	c02%RowType;

begin

/*open c02;
loop
fetch c02 into
	vet02;
exit when c02%notfound;
	begin*/
	
	open c01;
	loop
	fetch c01 into
		vet01;
	exit when c01%notfound;
		begin
		
		--if	(vet01.dt_prestador_serv = vet02.dt_emissao) then
		
			/*select	min(n.nr_sequencia) nr_primeiro_doc,
				max(n.nr_sequencia) nr_ultimo_doc
			into	nr_primeiro_doc_w,
				nr_ultimo_doc_w
			from	nota_fiscal n,
				nota_fiscal_item i,
				modelo_nota_fiscal m
			where 	n.dt_emissao  		= vet01.dt_prestador_serv			
			and	i.nr_sequencia 		= n.nr_sequencia
			and 	n.nr_seq_modelo 	= m.nr_sequencia
			and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'			
			and	n.cd_estabelecimento	= cd_estabelecimento_p
			and	n.vl_total_nota 	> 0
			and 	i.cd_procedimento 	is not null
			and 	m.cd_modelo_nf 	= '3A';*/
					
			contador_w := contador_w + 1;
				
			ds_linha_w := substr(	sep_w	||	'B030'				|| sep_w ||
								vet01.cd_modelo			|| sep_w ||
								vet01.cd_serie			|| sep_w ||
								vet01.cd_subserie		|| sep_w ||
								vet01.nr_primeiro_doc		|| sep_w ||
								vet01.nr_ultimo_doc		|| sep_w ||
								vet01.dt_prestador_serv		|| sep_w ||
								vet01.cd_cfps			|| sep_w ||
								vet01.nr_lanc_contabil		|| sep_w ||
								vet01.qt_calceladas		|| sep_w ||
								vet01.vl_total_documentos	|| sep_w ||
								vet01.vl_nao_tributadas		|| sep_w ||
								vet01.vl_base_calculo		|| sep_w ||
								--vet01.aliquota_iss		|| sep_w ||
								vet01.vl_tributo		|| sep_w ||
								vet01.ds_observacao		|| sep_w, 1, 8000);

			ds_arquivo_w		:= substr(ds_linha_w,1,4000);
			ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			nr_linha_w		:= nr_linha_w + 1;

			insert into 	fis_lfpd_arquivo
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_linha,
					ds_arquivo,
					ds_arquivo_compl,
					cd_registro,
					nr_seq_controle_lfpd)
			values		(fis_lfpd_arquivo_seq.NextVal,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_linha_w,
					ds_arquivo_w,
					ds_arquivo_compl_w,
					'B030',
					nr_seq_controle_p);
			
			/*B035*/
			
			open c02;
			loop
			fetch c02 into
				vet02;
			exit when c02%notfound;
				begin	
				
				if (	nvl(vet01.cd_modelo,' ') 	= nvl(vet02.cd_modelo,' ') and	
					nvl(vet01.cd_serie,' ') 	= nvl(vet02.cd_serie,' ') and
					nvl(vet01.cd_subserie,' ') 	= nvl(vet02.cd_subserie,' ') and 
					nvl(vet01.dt_prestador_serv,0) 	= nvl(vet02.dt_prestador_serv,0) and
					nvl(vet01.cd_cfps,' ') 		= nvl(vet02.cd_cfps,' ') )then				
				
					ds_linha_w := substr(	sep_w	||	'B035'				|| sep_w ||
										vet02.vl_total_documentos	|| sep_w ||									
										vet02.vl_nao_tributadas		|| sep_w ||
										vet02.vl_base_calculo		|| sep_w ||
										vet02.aliquota_iss		|| sep_w ||
										vet02.vl_tributo		|| sep_w, 1, 8000);
	
					ds_arquivo_w		:= substr(ds_linha_w,1,4000);
					ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
					nr_seq_registro_w	:= nr_seq_registro_w + 1;
					nr_linha_w		:= nr_linha_w + 1;
		
					insert into	fis_lfpd_arquivo
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_linha,
							ds_arquivo,
							ds_arquivo_compl,
							cd_registro,
							nr_seq_controle_lfpd)
					values		(fis_lfpd_arquivo_seq.NextVal,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_linha_w,
							ds_arquivo_w,
							ds_arquivo_compl_w,
							'B035',
							nr_seq_controle_p);
				end if;
			end;
			end loop;
			close c02;
		--end if;
		
		if (mod(contador_w,100) = 0) then
			commit;
		end if;

		end;
	end loop;
	close c01;

	/*end;
end loop;
close c02;	*/

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_registro_B030;
/