create or replace
procedure sus_gerar_sadt_carga(	cd_estabelecimento_p		in 	number,
				dt_vigencia_p			in	date,
				nm_usuario_p			in	varchar2,
				ds_retorno_p			out	varchar2) is


cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
qt_ato_medico_w			number(04,0);
vl_sadt_w			number(15,2);
dt_competencia_w			date;
nr_sequencia_w			number(10,0);
cd_proc_unif_w			number(15,0);
qt_reg_w				number(10,0);

cursor	c01 is
	select	cd_procedimento,
		ie_origem_proced,
		qt_ato_medico,
		vl_sadt
	from	sus_preco_procaih
	where	dt_competencia	= dt_competencia_w
	order by 	4 desc;

begin

select	count(*)
into	qt_reg_w
from	sus_aih_sadt
where	cd_estabelecimento	= cd_estabelecimento_p;


if	(qt_reg_w = 0) then
	begin

	select	max(dt_competencia)
	into	dt_competencia_w
	from	sus_preco_procaih;

	open	c01;
	loop
	fetch	c01 into
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_ato_medico_w,
		vl_sadt_w;
	exit	when c01%notfound;
		begin

		begin
		select	a.cd_proc_unif
		into	cd_proc_unif_w
		from	sus_origem a,
			procedimento b
		where	a.cd_procedimento	= cd_procedimento_w
		and	a.ie_origem_proced	= ie_origem_proced_w
		and	a.cd_proc_unif		= b.cd_procedimento
		and	a.ie_origem_proc_unif	= b.ie_origem_proced
		and	b.ie_situacao		= 'A'
		and	not exists(	select	1
					from	sus_aih_sadt x
					where	x.cd_procedimento	= a.cd_proc_unif
					and	dt_vigencia_inicial 	= dt_vigencia_p);
		exception
			when others then
				cd_proc_unif_w	:= 0;
		end;

		if	(cd_proc_unif_w > 0) and
			((qt_ato_medico_w > 0) or (vl_sadt_w > 0)) then

			select	sus_aih_sadt_seq.nextval
			into	nr_sequencia_w
			from 	dual;

			insert	into sus_aih_sadt
				(nr_sequencia,
				cd_estabelecimento,
				cd_procedimento,
				ie_origem_proced,
				dt_vigencia_inicial,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				qt_ponto,
				vl_sadt,
				dt_vigencia_final)
			values	(nr_sequencia_w,
				cd_estabelecimento_p,
				cd_proc_unif_w, 7,
				dt_vigencia_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				qt_ato_medico_w,
				vl_sadt_w, null);
		end if;

		end;
	end loop;
	close	c01;

	ds_retorno_p	:= 'Carga completada';
	end;
else
	ds_retorno_p	:= 'A carga j� foi efetuada';
end if;

end sus_gerar_sadt_carga;
/