create or replace
procedure pls_liberar_analise_grupo
			(	nr_seq_analise_p		number,
				 nm_usuario_p			varchar2,
				 nr_seq_grupo_atual_p		number,
				 ie_consistir_pendencias_p	varchar2,
				 cd_estabelecimento_p		number) is


nm_usuario_exec_w		varchar2(255);
ie_existe_grupo_final_w		varchar2(1);
ie_existe_final_anali_w		varchar2(1);
ie_analise_grupo_w		number(10);
nr_seq_grupo_analise_w		number(10);
nr_seq_grupo_w			number(10);
ie_existe_usuario_grupo_w	number(10);
ie_existe_grupos_abertos_w	number(10);
nr_seq_regra_w			number(10);
qt_grupos_analise_w		number(10);
nr_seq_fatura_w			number(10);

begin
if	(nvl(ie_consistir_pendencias_p, 'S') = 'S') then
	/*Se houver an�lises de ocorrencia a fazer n�o � possivel liberar*/
	if	(pls_obter_se_itens_aud_grupo(nr_seq_analise_p, nr_seq_grupo_atual_p, nm_usuario_p) = 'N') then
		--Existe ocorr�ncias que necessitam ser auditadas. Processo abortado.
		wheb_mensagem_pck.exibir_mensagem_abort(183530);
	end if;

	/*Se houver ocorrencias a corrigir n�o � possivel liberar*/
	if	(pls_obter_se_itens_cor_grupo(nr_seq_analise_p, nr_seq_grupo_atual_p, nm_usuario_p) = 'N') then
		--Existe ocorr�ncias que necessitam de corre��o. Processo abortado.
		wheb_mensagem_pck.exibir_mensagem_abort(183531);
	end if;
end if;

update	pls_auditoria_conta_grupo
set	dt_liberacao 		= sysdate,
	dt_final_analise	= sysdate,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia	=	(select	max(nr_sequencia) 
				from	pls_auditoria_conta_grupo
				where	nr_seq_grupo 		= nr_seq_grupo_atual_p	
				and	nr_seq_analise		= nr_seq_analise_p
				and	dt_liberacao 		is null		
				and	nr_seq_ordem 		=	(select	min(nr_seq_ordem)		 
									from	pls_auditoria_conta_grupo
									where	nr_seq_grupo 		= nr_seq_grupo_atual_p	
									and	nr_seq_analise		= nr_seq_analise_p
									and	dt_liberacao		is null	));

pls_gravar_inicio_fim_analise(nr_seq_analise_p, nr_seq_grupo_atual_p, 'F', nm_usuario_p);

select	count(*)
into	ie_existe_grupos_abertos_w
from	pls_auditoria_conta_grupo	a		
where	a.nr_seq_analise	= nr_seq_analise_p
and	nvl(ie_pre_analise,'N')	= 'N'
and	a.dt_liberacao is null;

/*Fazer verifica��o se existe grupos de analise ainda em aberto se n�o houver  fechar a analise*/
if	(ie_existe_grupos_abertos_w = 0) then
	/*Obter o grupo responsavel por fechar a conta*/
	pls_obter_grupo_fechar_analise(nr_seq_analise_p, cd_estabelecimento_p, nr_seq_regra_w, nr_seq_grupo_w);
	
	if	(nvl(nr_seq_grupo_w,0) > 0) then
		ie_existe_grupo_final_w	:= 'S';
	
		/*obter se este grupo j� foi inserido na an�lise*/
		select	decode(count(nr_sequencia), 0, 'N', 'S')
		into	ie_existe_final_anali_w
		from	pls_auditoria_conta_grupo
		where	nr_seq_grupo	= nr_seq_grupo_w
		and	nr_seq_analise	= nr_seq_analise_p;
	else		
		/*Caso n�o haja regra de grupo de finaliza��o*/
		ie_existe_grupo_final_w	:= 'N';
	end if;	
	
	/*Se n�o existe grupo de finaliza��o na an�lise esta � encerrada*/
	if	(ie_existe_grupo_final_w = 'N') then
		pls_alterar_status_analise_cta(nr_seq_analise_p, 'L', 'PLS_LIBERAR_ANALISE_GRUPO', nm_usuario_p, cd_estabelecimento_p);
		
		update	pls_analise_conta
		set	dt_liberacao_analise	= sysdate,
			dt_final_analise	= sysdate
		where	nr_sequencia		= nr_seq_analise_p;
	else			
		/*Se o grupo de finaliza��o existir na an�lise.*/
		if (ie_existe_final_anali_w = 'S') then
			select	count(nr_sequencia)
			into	qt_grupos_analise_w
			from	pls_auditoria_conta_grupo
			where	nr_seq_analise = nr_seq_analise_p;
			
			/*Se existir mais de uma grupo de analise*/
			if (qt_grupos_analise_w > 1) and 
			   (pls_obter_se_auditor_grupo(nr_seq_grupo_w, nm_usuario_p) = 'N') then
				/*Se o grupo de finaliza��o existir na an�lise ent�o seu sua libera��o � desfeita. Permitindo que o mesmo se torne o fluxo da vez. */
				pls_desf_final_grupo_analise(nr_seq_analise_p, nr_seq_grupo_w, null, nm_usuario_p, cd_estabelecimento_p,'N');
			else
				/*Se existir somente o grupo do auditor ent�o � liberado a an�lise.*/
				pls_alterar_status_analise_cta(nr_seq_analise_p, 'L', 'PLS_LIBERAR_ANALISE_GRUPO', nm_usuario_p, cd_estabelecimento_p);
				
				update	pls_analise_conta
				set	dt_liberacao_analise	= sysdate,
					dt_final_analise	= sysdate
				where	nr_sequencia		= nr_seq_analise_p;
			end if;
		else
			/*Se o grupo de finaliza��o n�o existir na an�lise este � acrescentado*/
			pls_inserir_grupo_analise(nr_seq_analise_p, nr_seq_grupo_w, 'Grupo inserido atrav�s da regra de finaliza��o '||nr_seq_regra_w,
						nr_seq_grupo_atual_p, 'N', nm_usuario_p, cd_estabelecimento_p);
		end if;
	end if;	
end if;

pls_inserir_hist_analise(null, nr_seq_analise_p, 7, null, null, null, null, null, nr_seq_grupo_atual_p, nm_usuario_p, cd_estabelecimento_p);

update	pls_analise_conta
set	ie_status_pre_analise	= decode(ie_pre_analise, 'S', 'F', ie_status_pre_analise)
where	nr_sequencia		= nr_seq_analise_p;

pls_atualizar_grupo_penden(nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);

select  max(a.nr_sequencia)
into    nr_seq_fatura_w
from    ptu_fatura              a,
        pls_conta               b
where   b.nr_seq_fatura         = a.nr_sequencia
and     b.nr_seq_analise        = nr_seq_analise_p;

/* Atualizar valores PTU Fatura*/
pls_atualizar_valor_ptu_fatura(nr_seq_fatura_w,'N');
				
commit;

end pls_liberar_analise_grupo;
/