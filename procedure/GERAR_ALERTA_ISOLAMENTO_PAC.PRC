create or replace
procedure gerar_alerta_isolamento_pac(	cd_setor_atendimento_p	number,
					ds_alerta_p		varchar2,
					ds_lista_p		varchar2,
					nm_usuario_p		varchar2) is

ds_lista_w		varchar2(2000);
ds_resultado_w		varchar2(2000);
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
ie_pos_arroba_w		number(3,0);
ds_listas_w		varchar2(2000);
cd_unidade_basica_w	varchar2(2000);
cd_unidade_compl_w	varchar2(2000);

begin

if	(cd_setor_atendimento_p is not null) and
	(ds_alerta_p is not null) and
	(ds_lista_p is not null) then
	begin
	ds_lista_w	:= ds_lista_p;	
	
	while	ds_lista_w is not null loop
		begin
		tam_lista_w		:= length(ds_lista_w);
		ie_pos_virgula_w	:= instr(ds_lista_w,',');
		
		if	(ie_pos_virgula_w <> 0) then
			begin
			ds_resultado_w	:= substr(ds_lista_w,1, (ie_pos_virgula_w - 1));
			ds_lista_w	:= substr(ds_lista_w, (ie_pos_virgula_w + 1), tam_lista_w);
			end;
		else
			ds_lista_w	:= null;
		end if;
		
		if	(ds_resultado_w is not null) then
			begin
			ie_pos_arroba_w	:= instr(ds_resultado_w,'@');
			
			if	(ie_pos_arroba_w <> 0) then
				begin
				cd_unidade_basica_w	:= substr(ds_resultado_w,1, (ie_pos_arroba_w - 2));
				ds_listas_w		:= substr(ds_resultado_w, (ie_pos_arroba_w + 1), tam_lista_w);			
				end;
			else
				ds_listas_w	:= null;
			end if;
			end;
		end if;
		
		if	(ds_listas_w is not null) then
			begin
			
			ie_pos_arroba_w	:= instr(ds_listas_w,'@');
			
			if	(ie_pos_arroba_w <> 0) then
				begin
				cd_unidade_compl_w	:= substr(ds_listas_w,1, (ie_pos_arroba_w - 2));
				ds_listas_w		:= substr(ds_listas_w, (ie_pos_arroba_w + 1), tam_lista_w);			
				end;
			else
				ds_listas_w	:= null;
			end if;
			
			end;
		end if;
		
		ds_listas_w	:= instr(ds_listas_w,'@');
		
		if	(trim(cd_unidade_basica_w) is not null) and
			(trim(cd_unidade_compl_w) is not null) and
			(ds_listas_w = 0) then
			begin			
			inserir_alerta_unid_setor(cd_setor_atendimento_p, cd_unidade_basica_w, cd_unidade_compl_w, ds_alerta_p, nm_usuario_p);			
			end;
		end if;
		end;
	end loop;
	end;	
end if;

commit;

end gerar_alerta_isolamento_pac;
/