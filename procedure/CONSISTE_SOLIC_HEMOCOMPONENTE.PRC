create or replace
procedure Consiste_solic_hemocomponente
		(nr_sequencia_p		number,
		nr_seq_derivado_p	Number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2,
		ds_erro_p	out	Varchar2) is

ie_hemoglobina_w	Varchar2(1);
ie_hematocrito_w	Varchar2(1);
ie_plaquetas_w		Varchar2(1);
ie_tap_w		Varchar2(1);
ie_inr_w		Varchar2(1);
ie_consiste_ambos_w	Varchar2(1);
ie_obriga_albumina_w	Varchar2(1);
ie_obriga_calcio_w	Varchar2(1);
ie_obriga_magnesio_w	Varchar2(1);
ie_obriga_bili_dir_w	Varchar2(1);
ie_obriga_bili_ind_w	Varchar2(1);
ds_coagulopatia_w	Varchar2(255);
ie_consiste_programado_w	Varchar2(1);
ie_obriga_hemoglobina_s_w Varchar2(1);
ie_obriga_coombs_w	Varchar2(1);
ie_obriga_coagulopatia_w	Varchar2(1);

qt_plaqueta_w		Number(15);
qt_hemoglobina_w	Number(15,2);
qt_tap_w		Number(15,1);
qt_tap_inr_w		Number(15,2);
qt_fibrinogenio_w		Number(15,2);
qt_hematocrito_w	Number(15,1);
qt_ttpa_w		Number(15,1);
qt_ttpa_rel_w		Number(15,1);
ie_tipo_w		Number(10);
ie_consiste_extrema_w	varchar2(2);
cd_setor_regra_w	number(5);
cd_setor_atendimento_w	number(5);
ie_consistir_w		varchar2(1);
ie_fibrinogenio_w	varchar2(10);
nr_prescricao_w		number(15);
count_w			number(10);
ds_erro_w		Varchar2(2000) := '';
qt_albumina_w		number(15,4);
qt_calcio_w		number(15,4);
qt_magnesio_w		number(15,4);
qt_bilirrubina_dir_w	number(15,4);
qt_bilirrubina_ind_w	number(15,4);
qt_hemoglobina_s_w	number(15,4);
ie_coombs_direto_w	varchar2(15);
ie_consiste_urgencia_w	varchar2(2);
ie_obriga_ttpa_w	varchar2(1);
ie_obriga_ttpa_rel_w	varchar2(1);
cd_perfil_w				number(5) := obter_Perfil_ativo;
ie_consiste_reserva_w	varchar2(1);
ie_reserva_w			varchar2(1);

cursor c01 is
select	cd_setor_atendimento,
	nvl(ie_consistir,'S')
from	regra_san_derivado_setor b,
	san_derivado a
where	a.nr_sequencia 		= b.nr_seq_derivado
and	a.nr_sequencia 		= nr_seq_derivado_p
and	cd_setor_atendimento 	= cd_setor_atendimento_w;

begin

Obter_Param_Usuario(924,343,cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_consiste_extrema_w);
Obter_Param_Usuario(924,632, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_consiste_urgencia_w);
Obter_Param_Usuario(924,942, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_consiste_programado_w);
Obter_Param_Usuario(924,1111, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_consiste_reserva_w);

select	nvl(max(ie_hemoglobina),'N'),
	nvl(max(ie_hematocrito),'N'),
	nvl(max(ie_plaquetas),'N'),
	nvl(max(ie_tap),'N'),
	nvl(max(ie_inr),'N'),
	nvl(max(ie_fibrinogenio),'N'),
	nvl(max(IE_OBRIGA_AMBOS),'N'),
	nvl(max(ie_obriga_albumina),'N'),
	nvl(max(ie_obriga_calcio),'N'),
	nvl(max(ie_obriga_magnesio),'N'),
	nvl(max(ie_obriga_bili_dir),'N'),
	nvl(max(ie_obriga_bili_ind),'N'),
	nvl(max(ie_obriga_hemoglobina_s),'N'),
	nvl(max(ie_obriga_coombs),'N'),
	nvl(max(ie_obriga_coagulopatia),'N'),
	coalesce(max(ie_obriga_ttpa),'N')
into	ie_hemoglobina_w,
	ie_hematocrito_w,
	ie_plaquetas_w,
	ie_tap_w,
	ie_inr_w,
	ie_fibrinogenio_w,
	ie_consiste_ambos_w,
	ie_obriga_albumina_w,
	ie_obriga_calcio_w,
	ie_obriga_magnesio_w,
	ie_obriga_bili_dir_w,
	ie_obriga_bili_ind_w,
	ie_obriga_hemoglobina_s_w,
	ie_obriga_coombs_w,
	ie_obriga_coagulopatia_w,
	ie_obriga_ttpa_w
from	san_derivado
where	nr_sequencia	= nr_seq_derivado_p;

begin
select	nr_prescricao,
	qt_plaqueta,
	qt_hemoglobina,
	qt_tap,
	qt_tap_inr,
	qt_hematocrito,
	ie_tipo,
	qt_ttpa,
	qt_fibrinogenio,
	qt_albumina,
	qt_calcio,
	qt_magnesio,
	qt_bilirrubina_dir,
	qt_bilirrubina_ind,
	qt_hemoglobina_s,
	ie_coombs_direto,
	ds_coagulopatia,
	qt_ttpa_rel,
	ie_reserva
into	nr_prescricao_w,
	qt_plaqueta_w,
	qt_hemoglobina_w,
	qt_tap_w,
	qt_tap_inr_w,
	qt_hematocrito_w,
	ie_tipo_w,
	qt_ttpa_w,
	qt_fibrinogenio_w,
	qt_albumina_w,
	qt_calcio_w,
	qt_magnesio_w,
	qt_bilirrubina_dir_w,
	qt_bilirrubina_ind_w,
	qt_hemoglobina_s_w,
	ie_coombs_direto_w,
	ds_coagulopatia_w,
	qt_ttpa_rel_w,
	ie_reserva_w
from	prescr_solic_bco_sangue
where	nr_sequencia	= nr_sequencia_p;
exception
when others then
	qt_plaqueta_w		:= null;
	qt_hemoglobina_w	:= null;
	qt_tap_w		:= null;
	qt_tap_inr_w		:= null;
	qt_hematocrito_w	:= null;
	qt_ttpa_w		:= null;	
end;

if	((ie_tipo_w <> 1) or (ie_consiste_programado_w = 'S')) and
	((ie_consiste_extrema_w	= 'S') or
	 (ie_tipo_w		<> 4)) and
	((ie_consiste_urgencia_w = 'S') or
	 (ie_tipo_w		<> 3))  then
	begin
	
	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_w;
	
	select	nvl(count(*),0)
	into	count_w
	from	regra_san_derivado_setor b,
		san_derivado a
	where	a.nr_sequencia 		= b.nr_seq_derivado
	and	a.nr_sequencia 		= nr_seq_derivado_p
	and	b.cd_setor_atendimento 	= cd_setor_atendimento_w;
		
	if	(count_w > 0) then
		
		open C01;
		loop
		fetch C01 into	
			cd_setor_regra_w,
			ie_consistir_w;
		exit when C01%notfound;
			begin
			
			if	(ie_consistir_w = 'S') then
			
				if	(ie_hemoglobina_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and
					(qt_hemoglobina_w is null) then
					ds_erro_w	:= wheb_mensagem_pck.get_texto(278266);
				end if;

				if	(ie_hematocrito_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and				
					(qt_hematocrito_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278267);
				end if;

				if	(ie_plaquetas_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and				
					(qt_plaqueta_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278268);
				end if;
				
				if	(ie_fibrinogenio_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and				
					(qt_fibrinogenio_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278269);
				end if;
				
				if	(ie_obriga_albumina_w = 'S') and
					(qt_albumina_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278270);
				end if;
				
				if	(ie_obriga_calcio_w = 'S') and
					(qt_calcio_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278271);
				end if;
				
				if	(ie_obriga_magnesio_w = 'S') and
					(qt_magnesio_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278273);
				end if;
				
				if	(ie_obriga_bili_dir_w = 'S') and
					(qt_bilirrubina_dir_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278277);
				end if;
				
				if	(ie_obriga_bili_ind_w = 'S') and
					(qt_bilirrubina_ind_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278280);
				end if;
	
				if	(ie_obriga_hemoglobina_s_w = 'S') and
					(qt_hemoglobina_s_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278284);
				end if;
				
				if	(ie_obriga_coombs_w = 'S') and
					(ie_coombs_direto_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) ||wheb_mensagem_pck.get_texto(278287);
				end if;
				
				if	(ie_obriga_coagulopatia_w = 'S') and
					(ds_coagulopatia_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) ||wheb_mensagem_pck.get_texto(278289);
				end if;
					
				if	(ie_consiste_ambos_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and				
					(qt_hematocrito_w is null) and
					(qt_hemoglobina_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278293);
				end if;
				
				/*
				if	(((ie_tap_w = 'S') and
					  (qt_tap_w is null) and
					  (qt_tap_inr_w is null) and
					  (qt_ttpa_w is null) and
					  (qt_ttpa_rel_w is null)) or
					 ((ie_inr_w = 'S') and
					  (qt_tap_inr_w is null))) then
					ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(278295);
				end if;
				*/
				
				if	(ie_obriga_ttpa_w = 'S') and
					((qt_ttpa_w is null) or (qt_ttpa_rel_w is null)) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(292785);	
				end if;
				
				if	(ie_tap_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and				
					(qt_tap_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(292850);	
				end if;
								
				
				if	(ie_inr_w = 'S') and
					((coalesce(ie_reserva_w,'N') = 'N') or
					 (ie_consiste_reserva_w = 'S')) and				
					(qt_tap_inr_w is null) then
					ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(292851);	
				end if;		

				if	(ds_erro_w is not null) then
					ds_erro_w	:= wheb_mensagem_pck.get_texto(278297) || ds_erro_w;
				end if;	
			end if;
				
			end;
		end loop;
		close C01;
		
	else	
		
		if	(ie_hemoglobina_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and		
			(qt_hemoglobina_w is null) then
			ds_erro_w	:= wheb_mensagem_pck.get_texto(278266);
		end if;

		if	(ie_hematocrito_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and				
			(qt_hematocrito_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278267);			
		end if;

		if	(ie_plaquetas_w = 'S') and
			(qt_plaqueta_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278268);			
		end if;
		
		if	(ie_fibrinogenio_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and				
			(qt_fibrinogenio_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278269);			
		end if;
		
		if	(ie_obriga_albumina_w = 'S') and
			(qt_albumina_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278270);			
		end if;
		
		if	(ie_obriga_calcio_w = 'S') and
			(qt_calcio_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278271);			
		end if;
		
		if	(ie_obriga_magnesio_w = 'S') and
			(qt_magnesio_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278273);			
		end if;
		
		if	(ie_obriga_bili_dir_w = 'S') and
			(qt_bilirrubina_dir_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) ||wheb_mensagem_pck.get_texto(278277);			
		end if;
		
		if	(ie_obriga_bili_ind_w = 'S') and
			(qt_bilirrubina_ind_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278280);			
		end if;

		if	(ie_obriga_hemoglobina_s_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and				
			(qt_hemoglobina_s_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278284);			
		end if;
		
		if	(ie_obriga_coombs_w = 'S') and
			(ie_coombs_direto_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278287);			
		end if;
		
		if	(ie_obriga_coagulopatia_w = 'S') and
			(DS_COAGULOPATIA_w is null) then
			ds_erro_w	:= ds_erro_w|| chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278289);			
		end if;
		
		if	(ie_consiste_ambos_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and				
			(qt_hematocrito_w is null) and
			(qt_hemoglobina_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(278293);			
		end if;
		--
		if	(ie_obriga_ttpa_w = 'S') and
			((qt_ttpa_w is null) or (qt_ttpa_rel_w is null)) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(292785);				
		end if;
		
		if	(ie_tap_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and				
			(qt_tap_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(292850);				
		end if;
		
		if	(ie_inr_w = 'S') and
			((coalesce(ie_reserva_w,'N') = 'N') or
			 (ie_consiste_reserva_w = 'S')) and				
			(qt_tap_inr_w is null) then
			ds_erro_w	:= ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(292851);				
		end if;		
		--
/*
		if	(((ie_tap_w = 'S') and
			  (qt_tap_w is null) and
			  (qt_tap_inr_w is null) and
			  (qt_ttpa_w is null) and
			  (qt_ttpa_rel_w is null)) or
			 ((ie_inr_w = 'S') and
			  (qt_tap_inr_w is null))) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(278295);
		end if;*/
		
					
		if	(ds_erro_w is not null) then
			ds_erro_w	:= wheb_mensagem_pck.get_texto(278297) || ds_erro_w;			
		end if;	

	end if;

	end;
	
end if;

ds_erro_p	:= SUBSTR(ds_erro_w,1,510);

end Consiste_solic_hemocomponente;
/