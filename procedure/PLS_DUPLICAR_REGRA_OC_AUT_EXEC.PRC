create or replace
procedure pls_duplicar_regra_oc_aut_exec
			(	nr_seq_regra_ocor_p	number,	
				ie_origem_p		varchar2,
				nm_usuario_p		Varchar2) is 
				
				
/* 
ie_origem_p 
A - Autoriza��o
AI - Autoriza��o Procedimento/Material (Item)
AE - Autoriza��o Exce��o
E - Execu��o requisi��o
EI - Execu��o requisi��o Procedimento/Material
EE - Execu��o requisi��o Exce��o
*/	
		
nr_seq_regra_ocor_exec_w	number(10);
nr_seq_regra_ocor_item_w	number(10);
nr_seq_regra_ocor_excecao_w	number(10);
ie_aplicacao_regra_w		varchar2(1);
ie_autorizacao_w		varchar2(1);
ie_execucao_requisicao_w	varchar2(1);

begin

if	(ie_origem_p in ('A','E')) then
	select	pls_ocorrencia_regra_seq.nextval
	into	nr_seq_regra_ocor_exec_w
	from	dual;

	if	(ie_origem_p = 'A') then
		ie_aplicacao_regra_w := 'E';
	elsif	(ie_origem_p = 'E') then
		ie_aplicacao_regra_w := 'A';
	end if;
		
	insert into pls_ocorrencia_regra 
		(nr_sequencia, dt_inicio_vigencia, dt_fim_vigencia,
		ie_situacao, nr_seq_estrutura, nr_seq_grupo_servico,
		cd_procedimento, cd_doenca_cid, ie_tipo_guia,
		ie_tipo_processo_autor, ie_tipo_atend_tiss, nr_seq_congenere,
		nr_seq_simultaneo, nr_seq_concorrente, nr_seq_regra_cid_int,
		dt_dia_semana, ie_feriado, ie_tipo_feriado,
		hr_inicial, hr_final, ie_carater_internacao,
		ie_exige_procedimento, qt_dias_cobranca_prev, ie_consiste_qt_diaria,
		nr_seq_tipo_acomodacao, nr_seq_tipo_acomod_conta, nr_seq_prestador, 
		nr_seq_tipo_prestador, cd_especialidade_medica, ie_prestador_inativo,
		ie_preco, nr_seq_grupo_contrato, nr_contrato,
		nr_seq_intercambio, ie_tipo_contrato_intercambio, ie_regulamentacao,
		ie_exige_senha_estipulante, ie_afastamento_cooperado, ie_tipo_segurado,
		qt_idade_min, qt_idade_max, ie_unid_tempo_idade,
		ie_sexo, ie_possui_liminar, ie_benef_autorizacao,
		qt_dias_mensal_vencido, ie_tipo_pagador, ie_validacao_cpt,
		qt_liberada, qt_tipo_quantidade, ie_tipo_qtde,
		ie_somar_estrutura, ie_qt_lib_posterior, ie_regra_qtde,
		ie_tipo_pessoa_qtde, qt_diarias_permitida, vl_minimo,
		ie_guia_referencia, ie_exige_indicacao_clinica, ie_restringe_pacote,
		ie_utiliza_opme, ds_documentacao, ie_tipo_ocorrencia, 
		ie_origem_proced, nr_seq_ocorrencia, ie_aplicacao_regra, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
		nm_usuario_nrec, cd_prestador)
	(select nr_seq_regra_ocor_exec_w, dt_inicio_vigencia, dt_fim_vigencia,
		ie_situacao, nr_seq_estrutura, nr_seq_grupo_servico,
		cd_procedimento, cd_doenca_cid, ie_tipo_guia,
		ie_tipo_processo_autor, ie_tipo_atend_tiss, nr_seq_congenere,
		nr_seq_simultaneo, nr_seq_concorrente, nr_seq_regra_cid_int,
		dt_dia_semana, ie_feriado, ie_tipo_feriado,
		hr_inicial, hr_final, ie_carater_internacao,
		ie_exige_procedimento, qt_dias_cobranca_prev, ie_consiste_qt_diaria,
		nr_seq_tipo_acomodacao, nr_seq_tipo_acomod_conta, nr_seq_prestador, 
		nr_seq_tipo_prestador, cd_especialidade_medica, ie_prestador_inativo,
		ie_preco, nr_seq_grupo_contrato, nr_contrato,
		nr_seq_intercambio, ie_tipo_contrato_intercambio, ie_regulamentacao,
		ie_exige_senha_estipulante, ie_afastamento_cooperado, ie_tipo_segurado,
		qt_idade_min, qt_idade_max, ie_unid_tempo_idade,
		ie_sexo, ie_possui_liminar, ie_benef_autorizacao,
		qt_dias_mensal_vencido, ie_tipo_pagador, ie_validacao_cpt,
		qt_liberada, qt_tipo_quantidade, ie_tipo_qtde,
		ie_somar_estrutura, ie_qt_lib_posterior, ie_regra_qtde,
		ie_tipo_pessoa_qtde, qt_diarias_permitida, vl_minimo,
		ie_guia_referencia, ie_exige_indicacao_clinica, ie_restringe_pacote,
		ie_utiliza_opme, ds_documentacao, ie_tipo_ocorrencia, 
		ie_origem_proced, nr_seq_ocorrencia, ie_aplicacao_regra_w, 
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, cd_prestador
	from	pls_ocorrencia_regra
	where	nr_sequencia = nr_seq_regra_ocor_p);

elsif	(ie_origem_p in ('AI','EI')) then
	select	pls_ocorrencia_regra_item_seq.nextval
	into	nr_seq_regra_ocor_item_w
	from	dual;	
	
	if	(ie_origem_p = 'AI') then
		ie_autorizacao_w	:= 'N';
		ie_execucao_requisicao_w := 'S';
	elsif	(ie_origem_p = 'EI') then
		ie_autorizacao_w	:= 'S';
		ie_execucao_requisicao_w := 'N';
	end if;

	insert into pls_ocorrencia_regra_item 
		(nr_sequencia, dt_inicio_vigencia, dt_fim_vigencia,
		ie_situacao, ie_tipo_item, ie_tipo_conta,
		nr_seq_estrutura, cd_area_procedimento, cd_especialidade,
		cd_grupo_proc, cd_procedimento, nr_seq_grupo_rec,
		nr_seq_estrutura_mat, nr_seq_material, nr_seq_prestador_exec,
		ie_tipo_desp_proc, ie_tipo_despesa_mat, vl_minimo_item,
		vl_max_item, nr_seq_grupo_contrato, ie_bloqueio_mat,
		ie_exige_nf, ie_exige_fornecedor, ie_exige_medico,
		ie_consiste_sexo, ie_consiste_idade, ie_consiste_qtd,
		ie_classif_sip, ie_pacote_honorario, nr_seq_grupo_produto,
		nr_seq_ocorrencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, ie_autorizacao,
		ie_conta_medica, ie_nao_informado, ie_origem_proced,
		ie_nao_utilizado, ie_mat_autorizacao_esp, ie_via_acesso,
		nr_seq_grupo_servico, ie_consistencia_valor, ie_regra_valor_copartic,
		ie_regra_valor_co, ie_tipo_segurado, qt_idade_min,
		qt_idade_max, ie_sexo, ie_unid_tempo_idade,
		ie_sem_evento, nr_seq_plano, nr_seq_grupo_material,
		nr_seq_regra_duplic, ie_exige_hora_item, nr_seq_tipo_acomodacao,
		nr_seq_tipo_acomod_conta, ie_execucao_requisicao, cd_prestador_exec)
	(select	nr_seq_regra_ocor_item_w, dt_inicio_vigencia, dt_fim_vigencia,
		ie_situacao, ie_tipo_item, ie_tipo_conta,
		nr_seq_estrutura, cd_area_procedimento, cd_especialidade,
		cd_grupo_proc, cd_procedimento, nr_seq_grupo_rec,
		nr_seq_estrutura_mat, nr_seq_material, nr_seq_prestador_exec,
		ie_tipo_desp_proc, ie_tipo_despesa_mat, vl_minimo_item,
		vl_max_item, nr_seq_grupo_contrato, ie_bloqueio_mat,
		ie_exige_nf, ie_exige_fornecedor, ie_exige_medico,
		ie_consiste_sexo, ie_consiste_idade, ie_consiste_qtd,
		ie_classif_sip, ie_pacote_honorario, nr_seq_grupo_produto,
		nr_seq_ocorrencia, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, ie_autorizacao_w,
		'N', ie_nao_informado, ie_origem_proced,
		ie_nao_utilizado, ie_mat_autorizacao_esp, ie_via_acesso,
		nr_seq_grupo_servico, ie_consistencia_valor, ie_regra_valor_copartic,
		ie_regra_valor_co, ie_tipo_segurado, qt_idade_min,
		qt_idade_max, ie_sexo, ie_unid_tempo_idade,
		ie_sem_evento, nr_seq_plano, nr_seq_grupo_material,
		nr_seq_regra_duplic, ie_exige_hora_item, nr_seq_tipo_acomodacao,
		nr_seq_tipo_acomod_conta, ie_execucao_requisicao_w, cd_prestador_exec
	from	pls_ocorrencia_regra_item
	where	nr_sequencia = nr_seq_regra_ocor_p);
	
elsif	(ie_origem_p in ('AE','EE')) then
	select	pls_excecao_ocorrencia_seq.nextval
	into	nr_seq_regra_ocor_excecao_w
	from	dual;

	if	(ie_origem_p = 'AE') then
		ie_autorizacao_w	:= 'N';
		ie_execucao_requisicao_w := 'S';
	elsif	(ie_origem_p = 'EE') then
		ie_autorizacao_w	:= 'S';
		ie_execucao_requisicao_w := 'N';
	end if;	
	
	insert into pls_excecao_ocorrencia
		(nr_sequencia, dt_inicio_vigencia, dt_fim_vigencia,
		ie_situacao, nr_seq_estrutura, cd_procedimento,
		ie_tipo_guia, ie_tipo_processo_autor, ie_tipo_atend_tiss,
		cd_doenca_cid, qt_dias, ie_porte_anestesico,
		ie_beneficiario_tratamento, nr_seq_grupo_prestador, nr_seq_classificacao,
		nr_seq_prestador, cd_especialidade_medica, ie_preco,
		nr_seq_grupo_contrato, nr_contrato, nr_seq_intercambio,
		ie_tipo_contrato_intercambio, ie_regulamentacao, nr_seq_tipo_acomodacao,
		nr_seq_tipo_acomod_conta, ie_tipo_segurado, qt_idade_min,
		qt_idade_max, ie_unid_tempo_idade, ie_sexo,
		ie_possui_liminar, nr_seq_ocorrencia, ie_autorizacao,
		ie_conta_medica, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, ie_origem_proced,
		cd_area_procedimento, cd_especialidade, cd_grupo_proc,
		ie_medico, nr_seq_proc_espec, cd_medico_executor,
		nr_seq_tipo_prestador, ie_tipo_prestador, nr_seq_grupo_servico,
		nr_seq_estrut_mat, nr_seq_grau_partic, nr_seq_clinica,
		nr_seq_grupo_material, ie_execucao_requisicao, ie_cobranca_previa,
		cd_prestador_exec)
	(select	nr_seq_regra_ocor_excecao_w, dt_inicio_vigencia, dt_fim_vigencia,
		ie_situacao, nr_seq_estrutura, cd_procedimento,
		ie_tipo_guia, ie_tipo_processo_autor, ie_tipo_atend_tiss,
		cd_doenca_cid, qt_dias, ie_porte_anestesico,
		ie_beneficiario_tratamento, nr_seq_grupo_prestador, nr_seq_classificacao,
		nr_seq_prestador, cd_especialidade_medica, ie_preco,
		nr_seq_grupo_contrato, nr_contrato, nr_seq_intercambio,
		ie_tipo_contrato_intercambio, ie_regulamentacao, nr_seq_tipo_acomodacao,
		nr_seq_tipo_acomod_conta, ie_tipo_segurado, qt_idade_min,
		qt_idade_max, ie_unid_tempo_idade, ie_sexo,
		ie_possui_liminar, nr_seq_ocorrencia, ie_autorizacao_w,
		'N', sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, ie_origem_proced,
		cd_area_procedimento, cd_especialidade, cd_grupo_proc,
		ie_medico, nr_seq_proc_espec, cd_medico_executor,
		nr_seq_tipo_prestador, ie_tipo_prestador, nr_seq_grupo_servico,
		nr_seq_estrut_mat, nr_seq_grau_partic, nr_seq_clinica,
		nr_seq_grupo_material, ie_execucao_requisicao_w, ie_cobranca_previa,
		cd_prestador_exec
	from	pls_excecao_ocorrencia
	where	nr_sequencia = nr_seq_regra_ocor_p);
end if;


commit;

end pls_duplicar_regra_oc_aut_exec;
/
