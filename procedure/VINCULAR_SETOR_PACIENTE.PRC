create or replace
procedure vincular_setor_paciente(
		cd_setor_atendimento_p	number,
		cd_pessoa_fisica_p	varchar,
		cd_estabelecimento_p	number,
		nm_usuario_p		Varchar2) is 

begin
	
	insert into paciente_setor (   
	nr_seq_paciente,
	cd_estabelecimento,
	cd_pessoa_fisica,
	cd_setor_atendimento,
	ie_status,
	dt_atualizacao,
	nm_usuario)
	values (    paciente_setor_seq.nextval,
	cd_estabelecimento_p,
	cd_pessoa_fisica_p,
	cd_setor_atendimento_p,
	'A',
	sysdate,
	nm_usuario_p );

commit;

end vincular_setor_paciente;
/