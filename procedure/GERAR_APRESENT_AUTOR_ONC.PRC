create or replace PROCEDURE GERAR_APRESENT_AUTOR_ONC(	nr_seq_paciente_setor_p number,
					nr_seq_atendimento_p	number,
					nr_ciclo_p		number,
					ie_forma_autor_ciclo_p	varchar2,
					nr_sequencia_autor_p	number,
					cd_convenio_p		number,
					cd_estabelecimento_p	NUMBER,
					nm_usuario_p		VARCHAR2) IS
/* vetor */
TYPE colunas IS RECORD (cd_material_w		NUMBER(6,0),
			cd_unidade_medida_w 	VARCHAR2(30),
			qt_dose_w		NUMBER(18,6),
			nr_seq_fabric_w		NUMBER(10,0),
			qt_frasco_disp_w		NUMBER(18,6),
			qt_perda_fornec_w		NUMBER(18,6));
TYPE vetor IS TABLE OF colunas INDEX BY BINARY_INTEGER;

vetor_w			vetor;
vetor_aux_w		vetor;
cd_convenio_w		number(10);
vetor_fornecedor_w	vetor;
cd_material_w           number(6,0);
cd_material_regra_w	number(6,0);
cd_unid_med_prescr_w    varchar2(30);
qt_dose_prescricao_w    number(18,6);
qt_unitaria_w   	number(18,6);
qt_total_dispensar_w	number(18,6);
qt_conversao_dose_w	number(18,6);
ie_define_disp_w	varchar2(5);
nr_seq_fabric_w		number(10,0);
cd_unid_med_consumo_w	varchar2(30);
qt_minimo_multiplo_solic_w	number(18,6);
ie_tipo_material_w		varchar2(14);
nr_seq_ficha_tecnica_w	number(15,0);
qt_dose_prescr_aux_w	number(18,6);
qt_dose_total_prescr_w	number(18,6);
qt_dose_regra_w		number(18,6);
qt_dose_total_aux_w	number(18,6);
qt_dose_total_aux_ww	number(18,6);
cont_w			number(10,0);
qt_registros_vetor_w	number(10,0);
qt_controle_w		number(15,0) := 0;
qt_controle_ww		number(15,0) := 0;
qt_controle_www		number(15,0) := 0;
qt_controle_wwww	number(15,0) := 0;
qt_perda_w		number(18,6);
qt_perda_2w		number(18,6);
qt_desconto_w		number(18,6);
qt_a_lancar_w		number(18,6);
qt_perda_final_w	number(18,6);
qt_perda_final_2w	number(18,6);
pr_desconto_w		number(18,6);
qt_mat_vetor_w		number(18,6);
x			number(15,0) := 0;
i			number(15,0) := 0;
k			number(15,0) := 0;
y			number(15,0) := 0;
cd_mat_vetor_w		number(15,0);
ie_reg_w			number(15,0);
cd_unidade_medida_consumo_w	varchar2(30);
nr_seq_fabric_melhor_w	number(10,0);
qt_fornec_w		number(10,0);
qt_menor_perda_w	number(18,6);
qt_menor_frasco_w	number(18,6);
qt_frasco_perda_zero_w	number(18,6);
qt_frasco_disp_w	number(10,0);
ie_regra_w		varchar2(15);
cd_medicamento_w	number(6);
qt_dose_w		number(18,6);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
nr_sequencia_regra_w	number(10);
ie_fabricante_w		VARCHAR2(10);
nr_seq_orc_mat_w	number(10,0);
cd_classe_material_w	number(5,0);
ie_via_aplicacao_w	varchar2(5);
cd_intervalo_w		varchar2(7);
nr_seq_fabricante_w	number(10);
cd_plano_w		varchar2(10);
dt_inicio_trat_w	date;
ie_regra_plano_w	varchar2(3)	:= null;
ds_retorno_w		varchar2(255)	:= null;
ie_bloqueia_agenda_w	varchar2(2)	:= 'N';
nr_seq_regra_w		number(10);
ie_autor_mat_conta_w	convenio_estabelecimento.ie_autor_mat_conta%type;
cd_categoria_w		varchar2(10);
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
cd_mat_informado_w	material_autorizado.cd_material_informado%type;

qt_dias_util_w		paciente_atend_medic.qt_dias_util%type;
cd_pessoa_fisica_w	paciente_setor.cd_pessoa_fisica%type;

-- qtp
medicamentos_w				qt_prescricao_pck.medicamento_vetor;
medicamento_w				qt_prescricao_pck.medicamento;
tabela_calculo_prescricao_w		qt_prescricao_pck.tabela_prescricao;
melhor_prescricao_w			qt_prescricao_pck.prescricao;
indice_saida_w					number;
cd_mat_especifico_w			material.cd_material%type;
ie_tipo_atendimento_w	paciente_setor_convenio.ie_tipo_atendimento%type;
ie_regra_aplicacao_w 		regra_apresentacao_quimio.ie_regra_aplicacao%type;
ie_tipo_material_considerar_w   regra_apresentacao_quimio.ie_tipo_material_considerar%type;


CURSOR c00 IS
select	ie_regra,
	nr_sequencia,
	ie_regra_aplicacao,
	ie_tipo_material_considerar
from	regra_apresentacao_quimio
where	nvl(cd_material,cd_material_w)				= cd_material_w
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_estabelecimento,cd_estabelecimento_p)		= cd_estabelecimento_p
and	nvl(cd_convenio,cd_convenio_w)				= cd_convenio_w
and          nvl(ie_situacao,'A') = 'A'
and	(sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate))
and	nvl(ie_tipo_material,ie_tipo_material_w)	= ie_tipo_material_w
and	nvl(cd_classe_material,cd_classe_material_w)	= cd_classe_material_w
and 	nvl(cd_pessoa_fisica,cd_pessoa_fisica_w) 	= cd_pessoa_fisica_w
order by
	nvl(cd_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_grupo_material,0),
	nvl(cd_pessoa_fisica,0),
	nvl(cd_convenio,0),
	nvl(cd_estabelecimento,0),
	nvl(ie_tipo_material,0);


--sum(nvl(a.qt_dias_util,1) * nvl(a.qt_dose_prescricao,a.qt_dose)),
cursor c01 is
select	a.cd_material,
	a.cd_unid_med_prescr,
	nvl(a.qt_dose_prescricao,a.qt_dose),
	substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
	d.qt_minimo_multiplo_solic,
	d.ie_tipo_material,
	max(a.qt_dose_prescricao),
	d.nr_seq_ficha_tecnica,
	nvl(a.ie_via_aplicacao,d.ie_via_aplicacao),
	a.cd_intervalo,
	a.qt_dias_util
from	paciente_atendimento b,
	paciente_atend_medic a,
	material	d
where	a.cd_material			= d.cd_material
and	b.dt_cancelamento is null
and	a.nr_seq_atendimento		= b.nr_seq_atendimento
and	b.nr_seq_paciente		= nr_seq_paciente_setor_p
and	((ie_forma_autor_ciclo_p	= 'D' and a.nr_seq_atendimento	= nr_seq_atendimento_p) or
	 (ie_forma_autor_ciclo_p	= 'C'))
and	b.nr_ciclo			= nr_ciclo_p
and	not exists (	select	1
			from	material_autorizado x,
				autorizacao_convenio y, 
				estagio_autorizacao z
			where	y.nr_sequencia = x.nr_sequencia_autor
			and	y.nr_ciclo     = nr_ciclo_p
			and	((ie_forma_autor_ciclo_p	= 'D' and y.nr_seq_paciente	= nr_seq_atendimento_p) or
				(ie_forma_autor_ciclo_p	= 'C'))
			and	x.nr_sequencia_autor 	= nr_sequencia_autor_p
			and	x.cd_material_informado = a.cd_material
			and 	y.nr_seq_estagio = z.nr_sequencia	
			and 	z.ie_interno not in ('70', '90'))
group by a.cd_material,
	a.cd_unid_med_prescr,
	nvl(a.qt_dose_prescricao,a.qt_dose),
	substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UMS'),1,30),
	d.qt_minimo_multiplo_solic,
	d.ie_tipo_material,
	d.nr_seq_ficha_tecnica,
	nvl(a.ie_via_aplicacao,d.ie_via_aplicacao),
	a.cd_intervalo,
	a.qt_dias_util;

CURSOR c02 IS
SELECT	b.cd_material,
	obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_prescr_w) qt_dose,
	substr(obter_dados_material_estab(b.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo
FROM	material_estab c,
	material b,
	medic_ficha_tecnica a
WHERE	b.nr_seq_ficha_tecnica	= a.nr_sequencia
AND	c.cd_material		= b.cd_material
AND	c.cd_estabelecimento	= cd_estabelecimento_p
AND	a.nr_sequencia		= nr_seq_ficha_tecnica_w
AND	(ie_via_aplicacao_w is null or Obter_se_via_adm(b.cd_material, ie_via_aplicacao_w) = 'S')
and  not exists (select 1
	     from   regra_apresent_quimio_excl a
	     where  a.cd_material 	   = b.cd_material
	     and    a.nr_seq_regra	   = nr_sequencia_regra_w)
AND	((b.cd_material		<> cd_material_w) OR
	 (ie_define_disp_w	= 'S'))
and (cd_mat_especifico_w is null or b.cd_material = cd_mat_especifico_w)
AND	((NVL(ie_fabricante_w,'N')	= 'S') OR (b.nr_seq_fabric		= nr_seq_fabric_w))
AND	NVL(obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_prescr_w),0) > 0
AND	b.ie_situacao		= 'A'
AND	c.ie_prescricao		= 'S'
AND	obter_regra_mat_sem_apres(b.cd_material, cd_material_w, cd_convenio_w) = 'S'
and 	(cd_mat_especifico_w is not null or (ie_regra_aplicacao_w = 'P' and (ie_tipo_material_considerar_w is null or ie_tipo_material_considerar_w = b.ie_tipo_material)))
ORDER BY qt_dose DESC;

CURSOR c03 IS
SELECT	DISTINCT
	b.nr_seq_fabric
FROM	material_estab c,
	material b,
	medic_ficha_tecnica a
WHERE	b.nr_seq_ficha_tecnica	= a.nr_sequencia
AND	c.cd_material		= b.cd_material
AND	c.cd_estabelecimento	= cd_estabelecimento_p
AND	a.nr_sequencia		= nr_seq_ficha_tecnica_w
AND	NVL(obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_prescr_w),0) > 0
AND	b.nr_seq_fabric		IS NOT NULL
AND	((b.cd_material		<> cd_material_w) OR
	 (ie_define_disp_w	= 'S'))
AND	b.ie_situacao		= 'A'
AND	c.ie_prescricao		= 'S'
ORDER BY b.nr_seq_fabric;

BEGIN

--obter_param_usuario(3130, 133, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_define_disp_w);
ie_define_disp_w:= 'N';

select	max(a.cd_convenio),
	max(a.cd_plano),
	max(b.dt_inicio_trat),
	max(a.cd_categoria),
	max(a.ie_tipo_atendimento)
into	cd_convenio_w,
	cd_plano_w,
	dt_inicio_trat_w,
	cd_categoria_w,
	ie_tipo_atendimento_w
from	paciente_setor b,
	paciente_setor_convenio a
where	a.nr_seq_paciente	= b.nr_seq_paciente
and	a.nr_seq_paciente	= nr_seq_paciente_setor_p;

select	nvl(max(ie_autor_mat_conta),'N')
into	ie_autor_mat_conta_w
from	convenio_estabelecimento
where	cd_convenio = cd_convenio_w
and	cd_estabelecimento = cd_estabelecimento_p;

SELECT	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
FROM	paciente_setor
WHERE	nr_seq_paciente = nr_seq_paciente_setor_p;

OPEN c01;
LOOP
FETCH c01 INTO
	cd_material_w,
	cd_unid_med_prescr_w,
	qt_dose_prescricao_w,
	cd_unid_med_consumo_w,
	qt_minimo_multiplo_solic_w,
	ie_tipo_material_w,
	qt_dose_total_prescr_w,
	nr_seq_ficha_tecnica_w,
	ie_via_aplicacao_w,
	cd_intervalo_w,
	qt_dias_util_w;
EXIT WHEN c01%NOTFOUND;
	BEGIN

	select	max(cd_grupo_material),
		max(cd_subgrupo_material),
		max(cd_classe_material)
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_w;

	--Verifica se o material informado no TTO necessita de autorizacao
	--Caso contrario, nem gera os itens pela apresentacao.
	consiste_mat_plano_convenio(	cd_convenio_w,
					cd_plano_w,
					cd_material_w,
					null,
					null,
					ds_retorno_w,
					ie_bloqueia_agenda_w,
					ie_regra_plano_w,
					nr_seq_regra_w,
					qt_dose_prescricao_w,
					nvl(dt_inicio_trat_w,sysdate),
					null,
					cd_estabelecimento_p,
					null,
					ie_tipo_atendimento_w);


	if	(ie_regra_plano_w in (3,6)) then

		ie_regra_w := null;

		open C00;
		loop
		fetch C00 into
			ie_regra_w,
			nr_sequencia_regra_w,
			ie_regra_aplicacao_w,
			ie_tipo_material_considerar_w;
		exit when C00%notfound;
			begin
			ie_regra_w := ie_regra_w;
			end;
		end loop;
		close C00;


		SELECT	NVL(COUNT(*),0)
		INTO	cont_w
		FROM	material b
		WHERE	b.nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w;

		IF	((cont_w		> 0) AND
			((ie_tipo_material_w	= '6'))) and
			 (nvl(ie_regra_w,'MA')	= 'MA') THEN

			select	max(ie_fabricante_dist)
			into	ie_fabricante_w
			from	material
			WHERE	cd_material	= cd_material_w;

			SELECT	NVL(MAX(pr_desconto),0),
				NVL(nvl(ie_fabricante_w, MAX(ie_fabricante)),'N')
			INTO	pr_desconto_w,
				ie_fabricante_w
			FROM	material_sem_apresentacao
			WHERE	cd_material	= cd_material_w;


			IF	(ie_fabricante_w = 'N') THEN
				qt_fornec_w	:= 0;
				ie_reg_w	:= 0;
				OPEN c03;
				LOOP
				FETCH c03 INTO
					nr_seq_fabric_w;
				EXIT WHEN c03%NOTFOUND;

                    cd_mat_especifico_w := qt_obter_mat_especifico_regra(cd_material_w, cd_convenio_w, cd_pessoa_fisica_w, cd_estabelecimento_p);

					cont_w	:= 0;
					vetor_w.DELETE;
					OPEN c02;
					LOOP
					FETCH c02 INTO
						cd_material_regra_w,
						qt_dose_regra_w,
						cd_unidade_medida_consumo_w;
					EXIT WHEN c02%NOTFOUND;
						vetor_w(cont_w).cd_material_w	:= cd_material_regra_w;
						vetor_w(cont_w).qt_dose_w	:= qt_dose_regra_w;
						vetor_w(cont_w).cd_unidade_medida_w := cd_unidade_medida_consumo_w;
						cont_w := cont_w + 1;
					END LOOP;
					CLOSE c02;

					qt_dose_total_aux_w	:= qt_dose_total_prescr_w;
					qt_desconto_w		:= ((pr_desconto_w * qt_dose_total_aux_w) / 100);
					qt_controle_w		:= 0;

					-- alteracao melhor prescr
					if (vetor_w.count > 0) then
						medicamentos_w.delete;
						for indice in vetor_w.first .. vetor_w.last loop
							medicamento_w.codigo := vetor_w(indice).cd_material_w;
							medicamento_w.nome := obter_desc_material(vetor_w(indice).cd_material_w);
							medicamento_w.dose := vetor_w(indice).qt_dose_w;
							medicamento_w.unidade_medida := vetor_w(indice).cd_unidade_medida_w;
							medicamento_w.quantidade := 1;

							medicamentos_w(medicamentos_w.count) := medicamento_w;
						end loop;

						tabela_calculo_prescricao_w	:= qt_prescricao_pck.calcular_tabela(qt_dose_total_prescr_w, medicamentos_w);
						melhor_prescricao_w	:=	qt_prescricao_pck.calcular_prescricao(qt_dose_total_prescr_w, qt_desconto_w, tabela_calculo_prescricao_w);

						-- inserir os medicamentos
						indice_saida_w	:=	vetor_aux_w.count;
						for indice in	melhor_prescricao_w.medicamentos.first .. melhor_prescricao_w.medicamentos.last loop
							medicamento_w  := melhor_prescricao_w.medicamentos(indice);

							for quantidade in 1..ceil(medicamento_w.quantidade) loop
								vetor_aux_w(indice_saida_w).cd_material_w 			:= medicamento_w.codigo;
								vetor_aux_w(indice_saida_w).qt_dose_w				:= medicamento_w.dose;
								vetor_aux_w(indice_saida_w).cd_unidade_medida_w 		:= medicamento_w.unidade_medida;
								vetor_aux_w(indice_saida_w).nr_seq_fabric_w 			:= nr_seq_fabric_w;

								indice_saida_w := indice_saida_w + 1;
							end loop;

						end loop;
					end if;

					-- fim alteracao melhor prescr

					-- inserir os itens totais no vetor de fornecedores.
					k		:= -1;
					qt_mat_vetor_w	:= 0;
					qt_frasco_disp_w := 0;

					WHILE	(k < vetor_aux_w.COUNT - 1) LOOP
						k	:= k + 1;
						IF	(vetor_aux_w(k).nr_seq_fabric_w	= nr_seq_fabric_w) THEN
							qt_mat_vetor_w		:= qt_mat_vetor_w + vetor_aux_w(k).qt_dose_w;
							qt_frasco_disp_w	:= qt_frasco_disp_w + 1;
						END IF;
					END LOOP;
					vetor_fornecedor_w(qt_fornec_w).nr_seq_fabric_w	 := nr_seq_fabric_w;
					vetor_fornecedor_w(qt_fornec_w).qt_frasco_disp_w := qt_frasco_disp_w;
					vetor_fornecedor_w(qt_fornec_w).qt_perda_fornec_w := greatest(qt_mat_vetor_w - qt_dose_total_prescr_w, 0);
					qt_fornec_w	:= qt_fornec_w + 1;

				END LOOP;
				CLOSE c03;

				-- descobrir qual fornecedor tem a menor perda e a menor quantidade de frasco dispensados.

				k			:= -1;
				qt_menor_frasco_w	:= 99999;
				qt_menor_perda_w	:= 99999;
				qt_frasco_perda_zero_w	:= 99999;
				WHILE	(k < vetor_fornecedor_w.COUNT - 1) LOOP
					k	:= k + 1;

					if (vetor_fornecedor_w(k).qt_frasco_disp_w > 0) then
						IF	((vetor_fornecedor_w(k).qt_perda_fornec_w >= 0) and (vetor_fornecedor_w(k).qt_perda_fornec_w < qt_menor_perda_w)) OR
							((vetor_fornecedor_w(k).qt_perda_fornec_w = qt_menor_perda_w) AND (vetor_fornecedor_w(k).qt_frasco_disp_w <= qt_frasco_perda_zero_w)) THEN
							nr_seq_fabric_melhor_w	:= vetor_fornecedor_w(k).nr_seq_fabric_w;
							qt_menor_perda_w	:= vetor_fornecedor_w(k).qt_perda_fornec_w;
							qt_menor_frasco_w	:= vetor_fornecedor_w(k).qt_frasco_disp_w;
							IF	(vetor_fornecedor_w(k).qt_perda_fornec_w = 0) THEN
								qt_frasco_perda_zero_w	:= vetor_fornecedor_w(k).qt_frasco_disp_w;
							END IF;
						END IF;
					end if;

				END LOOP;

				vetor_fornecedor_w.DELETE;

				k		:= -1;
				y		:= -1;
				cd_mat_vetor_w	:= 0;
				qt_mat_vetor_w	:= 0;
				qt_a_lancar_w	:= qt_dose_total_prescr_w;
				--ie_posicao_ultimo_w	:= 0;

				WHILE	(k < vetor_aux_w.COUNT - 1) LOOP
					k	:= k + 1;
					--inserir somente os itens do melhor fabricante.
					IF	(vetor_aux_w(k).nr_seq_fabric_w = nr_seq_fabric_melhor_w) THEN
						IF	((cd_mat_vetor_w = vetor_aux_w(k).cd_material_w) OR
							 (cd_mat_vetor_w = 0)) AND
							(vetor_aux_w.COUNT > 1) THEN
							--(k <> (vetor_aux_w.count -1)) then
							qt_mat_vetor_w			:= qt_mat_vetor_w + vetor_aux_w(k).qt_dose_w;
							qt_unitaria_w			:= vetor_aux_w(k).qt_dose_w;
							cd_mat_vetor_w			:= vetor_aux_w(k).cd_material_w;
							qt_conversao_dose_w		:= vetor_aux_w(k).qt_dose_w;
							cd_unidade_medida_consumo_w 	:= vetor_aux_w(k).cd_unidade_medida_w;
						ELSE

							qt_a_lancar_w	:= (qt_a_lancar_w - qt_mat_vetor_w);

							IF	(qt_a_lancar_w >= 0) THEN
								qt_total_dispensar_w := qt_a_lancar_w;
							ELSE
								qt_mat_vetor_w := (qt_mat_vetor_w - ABS(qt_a_lancar_w));
							END IF;

							qt_unitaria_w	:= dividir(qt_mat_vetor_w, qt_unitaria_w);

							IF	(NVL(cd_mat_vetor_w,0) = '0') THEN
								cd_mat_vetor_w	:= vetor_aux_w(k).cd_material_w;
							END IF;

							IF	(NVL(qt_mat_vetor_w,0) > 0) THEN

								select	max(nr_seq_fabric)
								into	nr_seq_fabricante_w
								from	material
								where	cd_material = cd_mat_vetor_w;


								if	(ie_autor_mat_conta_w = 'S') and
									(cd_mat_vetor_w	is not null) then
									cd_mat_informado_w:= cd_material_w;
									cd_mat_vetor_w	:= obter_material_conta(cd_estabelecimento_p,
														cd_convenio_w,
														cd_categoria_w,
														cd_mat_vetor_w,
														cd_mat_vetor_w,
														cd_mat_vetor_w,
														cd_plano_w,
														cd_setor_atendimento_w,
														sysdate,
														null, --Local estoque
														null);
								end if;

								insert	into material_autorizado
									(nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_sequencia_autor,
									cd_material,
									qt_solicitada,
									nr_seq_fabricante,
									qt_autorizada,
									ie_via_aplicacao,
									cd_intervalo_medic_quimio,
									nr_seq_regra_plano,
									nr_seq_regra_quimio,
									cd_material_informado,
									qt_dose_quimio,	
									qt_dose_total_quimio)
								values	(material_autorizado_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_sequencia_autor_p,
									cd_mat_vetor_w,
									ceil(obter_dose_convertida(cd_mat_vetor_w, calcula_dose_total_quimio(qt_mat_vetor_w, qt_dias_util_w, cd_intervalo_w), cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30))),
									nr_seq_fabricante_w,
									0,
									ie_via_aplicacao_w,
									cd_intervalo_w,
									nr_seq_regra_w,
									nr_sequencia_regra_w,
									nvl(cd_mat_informado_w,cd_material_w),
									qt_mat_vetor_w,	
									qt_dose_total_prescr_w);
								commit;

							END IF;
							cd_mat_vetor_w			:= vetor_aux_w(k).cd_material_w;
							qt_mat_vetor_w			:= vetor_aux_w(k).qt_dose_w;
							qt_unitaria_w			:= vetor_aux_w(k).qt_dose_w;
							qt_conversao_dose_w		:= vetor_aux_w(k).qt_dose_w;
							cd_unidade_medida_consumo_w 	:= vetor_aux_w(k).cd_unidade_medida_w;
							COMMIT;
						END IF;
					END IF;
				END LOOP;

				qt_a_lancar_w	:= (qt_a_lancar_w - qt_mat_vetor_w);

				IF	(qt_a_lancar_w >= 0) THEN
					qt_total_dispensar_w := qt_a_lancar_w;
					IF	(qt_mat_vetor_w = 0) THEN
						qt_mat_vetor_w	:= qt_a_lancar_w;
					END IF;
				ELSE
					qt_mat_vetor_w := (qt_mat_vetor_w - ABS(qt_a_lancar_w));
				END IF;

				qt_unitaria_w	:= dividir(qt_mat_vetor_w, qt_unitaria_w);

				IF	(NVL(cd_mat_vetor_w,0) = 0) AND
					(vetor_aux_w.COUNT > 0) THEN
					cd_mat_vetor_w	:= vetor_aux_w(k).cd_material_w;
				END IF;


				IF	(cd_mat_vetor_w <> 0) THEN


					if	(ie_autor_mat_conta_w = 'S') and
						(cd_mat_vetor_w	is not null) then
						cd_mat_informado_w:= cd_material_w;
						cd_mat_vetor_w	:= obter_material_conta(cd_estabelecimento_p,
											cd_convenio_w,
											cd_categoria_w,
											cd_mat_vetor_w,
											cd_mat_vetor_w,
											cd_mat_vetor_w,
											cd_plano_w,
											cd_setor_atendimento_w,
											sysdate,
											null, --Local estoque
											null);
					end if;

					insert	into material_autorizado
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_sequencia_autor,
						cd_material,
						qt_solicitada,
						nr_seq_fabricante,
						qt_autorizada,
						ie_via_aplicacao,
						cd_intervalo_medic_quimio,
						nr_seq_regra_plano,
						nr_seq_regra_quimio,
						cd_material_informado,
						qt_dose_quimio,	
						qt_dose_total_quimio)
					values	(material_autorizado_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_sequencia_autor_p,
						cd_mat_vetor_w,
						ceil(obter_dose_convertida(cd_mat_vetor_w,calcula_dose_total_quimio(qt_mat_vetor_w, qt_dias_util_w, cd_intervalo_w),cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30))),
						nr_seq_fabricante_w,
						0,
						ie_via_aplicacao_w,
						cd_intervalo_w,
						nr_seq_regra_w,
						nr_sequencia_regra_w,
						nvl(cd_mat_informado_w,cd_material_w),
						qt_mat_vetor_w,	
						qt_dose_total_prescr_w);
					commit;
				END IF;

				vetor_aux_w.DELETE;

			ELSE

                cd_mat_especifico_w := qt_obter_mat_especifico_regra(cd_material_w, cd_convenio_w, cd_pessoa_fisica_w, cd_estabelecimento_p);

				cont_w	:= 0;
				vetor_w.DELETE;
				OPEN c02;
				LOOP
				FETCH c02 INTO
					cd_material_regra_w,
					qt_dose_regra_w,
					cd_unidade_medida_consumo_w;
				EXIT WHEN c02%NOTFOUND;
					vetor_w(cont_w).cd_material_w	:= cd_material_regra_w;
					vetor_w(cont_w).qt_dose_w	:= qt_dose_regra_w;
					vetor_w(cont_w).cd_unidade_medida_w := cd_unidade_medida_consumo_w;
					cont_w := cont_w + 1;
				END LOOP;
				CLOSE c02;

				qt_dose_total_aux_w	:= qt_dose_total_prescr_w;
				qt_desconto_w		:= ((pr_desconto_w * qt_dose_total_aux_w) / 100);
				ie_reg_w		:= 0;

				WHILE	(qt_dose_total_aux_w	> 0) AND
					(qt_controle_w		< 1000) LOOP

					qt_registros_vetor_w	:= vetor_w.COUNT - 1;
					qt_controle_ww		:= 0;
					x			:= -1;

					WHILE	(x < qt_registros_vetor_w) AND
						(qt_dose_total_aux_w	> 0) AND
						(qt_controle_ww		< 1000) LOOP

						x := x + 1;
						qt_controle_ww	:= qt_controle_ww + 1;

						IF	((qt_dose_total_aux_w - vetor_w(x).qt_dose_w) >= 0) THEN

							qt_dose_total_aux_w	:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
							vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(x).cd_material_w;
							vetor_aux_w(ie_reg_w).qt_dose_w		:= vetor_w(x).qt_dose_w;
							vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(x).cd_unidade_medida_w;
							ie_reg_w	:= ie_reg_w + 1;
							x := -1;

						ELSIF	(qt_dose_total_aux_w <= qt_desconto_w) THEN
							qt_dose_total_aux_w	:= 0;
						ELSIF	(qt_registros_vetor_w = x) THEN

							qt_dose_total_aux_w	:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
							vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(x).cd_material_w;
							vetor_aux_w(ie_reg_w).qt_dose_w	:= vetor_w(x).qt_dose_w;
							vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(x).cd_unidade_medida_w;
							ie_reg_w	:= ie_reg_w + 1;

						ELSIF	((qt_dose_total_aux_w - vetor_w(x).qt_dose_w) < 0) THEN
							qt_perda_w		:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
							qt_dose_total_aux_ww	:= qt_dose_total_aux_w;
							qt_perda_final_w	:= 0;
							qt_controle_wwww	:= 0;
							WHILE	(qt_dose_total_aux_ww	> 0) AND
								(qt_controle_wwww	< 1000) LOOP

								qt_controle_www := 0;
								i 		:= -1;
								WHILE	(i < qt_registros_vetor_w) AND
									(qt_dose_total_aux_ww	> 0) AND
									(qt_controle_www	< 1000) LOOP

									i	:= i + 1;
									qt_controle_www	:= qt_controle_www + 1;
									IF	((qt_dose_total_aux_ww - vetor_w(i).qt_dose_w) >= 0) THEN
										qt_perda_final_w	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
										qt_dose_total_aux_ww	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
										i := -1;
									ELSIF	(qt_registros_vetor_w = i) THEN
										qt_perda_final_w	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
										qt_dose_total_aux_ww	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
									END IF;
								END LOOP;

								qt_controle_wwww := qt_controle_wwww + 1;

							END LOOP;

							qt_perda_2w		:= (qt_dose_total_aux_w - vetor_w(x).qt_dose_w);
							qt_dose_total_aux_ww	:= qt_dose_total_aux_w;
							qt_perda_final_2w	:= 0;
							qt_controle_wwww	:= 0;
							WHILE	(qt_dose_total_aux_ww	> 0) AND
								(qt_controle_wwww	< 1000) LOOP

								qt_controle_www := 0;
								i 		:= -1;
								WHILE	(i < qt_registros_vetor_w) AND
									(qt_dose_total_aux_ww	> 0) AND
									(qt_controle_www	< 1000) LOOP

									i	:= i + 1;
									qt_controle_www	:= qt_controle_www + 1;
									IF	((qt_dose_total_aux_ww - vetor_w(i).qt_dose_w) >= 0) THEN
										qt_perda_final_2w	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
										qt_dose_total_aux_ww	:= (qt_dose_total_aux_ww - vetor_w(i).qt_dose_w);
										i := -1;
									END IF;
								END LOOP;
								qt_controle_wwww := qt_controle_wwww + 1;
							END LOOP;

							IF	(qt_dose_total_aux_ww < qt_desconto_w) THEN
								NULL;
							ELSIF	(qt_perda_w >= qt_perda_final_w) THEN
								vetor_aux_w(ie_reg_w).cd_material_w	:= vetor_w(x).cd_material_w;
								vetor_aux_w(ie_reg_w).qt_dose_w		:= vetor_w(x).qt_dose_w;
								vetor_aux_w(ie_reg_w).cd_unidade_medida_w := vetor_w(x).cd_unidade_medida_w;
								ie_reg_w		:= ie_reg_w + 1;
								qt_dose_total_aux_w	:= 0;
							END IF;
						END IF;
					END LOOP;

					qt_controle_w	:= qt_controle_w + 1;
				END LOOP;

				k		:= -1;
				cd_mat_vetor_w	:= 0;
				qt_mat_vetor_w	:= 0;
				qt_a_lancar_w	:= qt_dose_total_prescr_w;

				WHILE	(k < vetor_aux_w.COUNT - 1) LOOP
					k	:= k + 1;

					IF	((cd_mat_vetor_w = vetor_aux_w(k).cd_material_w) OR
						 (cd_mat_vetor_w = 0)) AND
						(vetor_aux_w.COUNT > 1) THEN
						qt_mat_vetor_w			:= qt_mat_vetor_w + vetor_aux_w(k).qt_dose_w;
						qt_unitaria_w			:= vetor_aux_w(k).qt_dose_w;
						cd_mat_vetor_w			:= vetor_aux_w(k).cd_material_w;
						qt_conversao_dose_w		:= vetor_aux_w(k).qt_dose_w;
						cd_unidade_medida_consumo_w 	:= vetor_aux_w(k).cd_unidade_medida_w;
					ELSE
						qt_a_lancar_w	:= (qt_a_lancar_w - qt_mat_vetor_w);

						IF	(qt_a_lancar_w >= 0) THEN
							qt_total_dispensar_w := qt_a_lancar_w;
						ELSE
							qt_mat_vetor_w := (qt_mat_vetor_w - ABS(qt_a_lancar_w));
						END IF;

						qt_unitaria_w	:= dividir(qt_mat_vetor_w, qt_unitaria_w);

						IF	(NVL(cd_mat_vetor_w,0) = '0') THEN
							cd_mat_vetor_w	:= vetor_aux_w(k).cd_material_w;
						END IF;

						IF	(qt_mat_vetor_w > 0) THEN


							if	(ie_autor_mat_conta_w = 'S') and
								(cd_mat_vetor_w	is not null) then
								cd_mat_informado_w:= cd_material_w;
								cd_mat_vetor_w	:= obter_material_conta(cd_estabelecimento_p,
													cd_convenio_w,
													cd_categoria_w,
													cd_mat_vetor_w,
													cd_mat_vetor_w,
													cd_mat_vetor_w,
													cd_plano_w,
													cd_setor_atendimento_w,
													sysdate,
													null, --Local estoque
													null);
							end if;

							insert	into material_autorizado
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_sequencia_autor,
								cd_material,
								qt_solicitada,
								nr_seq_fabricante,
								qt_autorizada,
								ie_via_aplicacao,
								cd_intervalo_medic_quimio,
								nr_seq_regra_plano,
								nr_seq_regra_quimio,
								cd_material_informado,
								qt_dose_quimio,	
								qt_dose_total_quimio)
							values	(material_autorizado_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_sequencia_autor_p,
								cd_mat_vetor_w,
								ceil(obter_dose_convertida(cd_mat_vetor_w,calcula_dose_total_quimio(qt_mat_vetor_w, qt_dias_util_w, cd_intervalo_w),cd_unid_med_prescr_w,substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30))),
								nr_seq_fabricante_w,
								0,
								ie_via_aplicacao_w,
								cd_intervalo_w,
								nr_seq_regra_w,
								nr_sequencia_regra_w,
								nvl(cd_mat_informado_w,cd_material_w),
								qt_mat_vetor_w,	
								qt_dose_total_prescr_w);
							commit;
						END IF;

						cd_mat_vetor_w			:= vetor_aux_w(k).cd_material_w;
						qt_mat_vetor_w			:= vetor_aux_w(k).qt_dose_w;
						qt_unitaria_w			:= vetor_aux_w(k).qt_dose_w;
						qt_conversao_dose_w		:= vetor_aux_w(k).qt_dose_w;
						cd_unidade_medida_consumo_w 	:= vetor_aux_w(k).cd_unidade_medida_w;
						COMMIT;
					END IF;
				END LOOP;

				vetor_aux_w.DELETE;

				IF	(NVL(cd_mat_vetor_w,0) <> '0') THEN

					qt_a_lancar_w	:= (qt_a_lancar_w - qt_mat_vetor_w);
					IF	(qt_a_lancar_w >= 0) THEN
						qt_total_dispensar_w := qt_a_lancar_w;
					ELSE
						qt_mat_vetor_w := (qt_mat_vetor_w - ABS(qt_a_lancar_w));
					END IF;

					qt_unitaria_w	:= dividir(qt_mat_vetor_w, qt_unitaria_w);


					if	(ie_autor_mat_conta_w = 'S') and
						(cd_mat_vetor_w	is not null) then
						cd_mat_informado_w:= cd_material_w;
						cd_mat_vetor_w	:= obter_material_conta(cd_estabelecimento_p,
											cd_convenio_w,
											cd_categoria_w,
											cd_mat_vetor_w,
											cd_mat_vetor_w,
											cd_mat_vetor_w,
											cd_plano_w,
											cd_setor_atendimento_w,
											sysdate,
											null, --Local estoque
											null);
					end if;

					SELECT	MAX(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30))
					INTO	cd_unidade_medida_consumo_w
					FROM  	material
					WHERE  	cd_material = cd_mat_vetor_w;


					insert	into material_autorizado
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_sequencia_autor,
						cd_material,
						qt_solicitada,
						nr_seq_fabricante,
						qt_autorizada,
						ie_via_aplicacao,
						cd_intervalo_medic_quimio,
						nr_seq_regra_plano,
						nr_seq_regra_quimio,
						cd_material_informado,
						qt_dose_quimio,	
						qt_dose_total_quimio)
					values	(material_autorizado_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_sequencia_autor_p,
						cd_mat_vetor_w,
						ceil(obter_dose_convertida(cd_mat_vetor_w,
						calcula_dose_total_quimio(qt_mat_vetor_w, qt_dias_util_w, cd_intervalo_w),
						cd_unid_med_prescr_w,
						substr(Obter_Dados_Material(cd_mat_vetor_w,'UMC'),1,30))),
						nr_seq_fabricante_w,
						0,
						ie_via_aplicacao_w,
						cd_intervalo_w,
						nr_seq_regra_w,
						nr_sequencia_regra_w,
						nvl(cd_mat_informado_w,cd_material_w),
						qt_mat_vetor_w,	
						qt_dose_total_prescr_w);

					commit;

				END IF;
			END IF;
		ELSIF 	(ie_regra_w = 'MF') or
			(ie_regra_w = 'MG') THEN

			if 	(ie_regra_w = 'MG') then
				select 	nvl(max(cd_material),0)
				into 	cd_medicamento_w
				from 	material
				where 	ie_tipo_material = 3
				and 	nr_seq_ficha_tecnica = nr_seq_ficha_tecnica_w;

				if	(cd_medicamento_w = 0) then
					wheb_mensagem_pck.Exibir_Mensagem_Abort(388788, 'DS_FICHA_TECNICA_W='||nr_seq_ficha_tecnica_w);
				end if;
			else
				select 	nvl(max(cd_material),0)
				into 	cd_medicamento_w
				from 	material
				where 	ie_tipo_material = 8
				and 	nr_seq_ficha_tecnica = nr_seq_ficha_tecnica_w;

				if	(cd_medicamento_w = 0) then
					wheb_mensagem_pck.Exibir_Mensagem_Abort(196339, 'DS_FICHA_TECNICA_W='||nr_seq_ficha_tecnica_w);
				end if;
			end if;

			if	(ie_autor_mat_conta_w = 'S') and
				(cd_medicamento_w	is not null) then
				cd_medicamento_w	:= obter_material_conta(cd_estabelecimento_p,
								cd_convenio_w,
								cd_categoria_w,
								cd_medicamento_w,
								cd_medicamento_w,
								cd_medicamento_w,
								cd_plano_w,
								cd_setor_atendimento_w,
								sysdate,
								null, --Local estoque
								null);
			end if;


			IF  (cd_unid_med_consumo_w = cd_unid_med_prescr_w) THEN
				qt_conversao_dose_w	:= 1;
			ELSE
				BEGIN
				SELECT	qt_conversao
				INTO	qt_conversao_dose_w
				FROM	material_conversao_unidade
				WHERE	cd_material 	= cd_medicamento_w
				AND	cd_unidade_medida = cd_unid_med_prescr_w;
				EXCEPTION
					WHEN OTHERS THEN
						qt_conversao_dose_w	:= 1;
				END;
			END IF;

			qt_unitaria_w		:= dividir(qt_dose_prescricao_w , qt_conversao_dose_w);
			qt_total_dispensar_w	:= dividir(qt_unitaria_w , qt_minimo_multiplo_solic_w) * qt_minimo_multiplo_solic_w;

			IF	(qt_total_dispensar_w < qt_unitaria_w) THEN
				qt_total_dispensar_w := qt_total_dispensar_w + qt_minimo_multiplo_solic_w;
			END IF;


			insert	into material_autorizado
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_sequencia_autor,
				cd_material,
				qt_solicitada,
				nr_seq_fabricante,
				qt_autorizada,
				ie_via_aplicacao,
				cd_intervalo_medic_quimio,
				nr_seq_regra_plano,
				nr_seq_regra_quimio,
				cd_material_informado)
			values	(material_autorizado_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_autor_p,
				cd_medicamento_w,
				qt_total_dispensar_w,
				nr_seq_fabricante_w,
				0,
				ie_via_aplicacao_w,
				cd_intervalo_w,
				nr_seq_regra_w,
				nr_sequencia_regra_w,
				cd_material_w);

		ELSIF 	(IE_REGRA_W = 'ME') THEN
			select 	nvl(max(cd_material_especifico),0)
			into	cd_medicamento_w
			from	regra_apresentacao_quimio
			where 	nr_sequencia = nr_sequencia_regra_w;

			if	(cd_medicamento_w = 0) then
				 wheb_mensagem_pck.Exibir_Mensagem_Abort(196338);
			end if;

			if	(ie_autor_mat_conta_w = 'S') and
				(cd_medicamento_w	is not null) then
				cd_medicamento_w	:= obter_material_conta(cd_estabelecimento_p,
								cd_convenio_w,
								cd_categoria_w,
								cd_medicamento_w,
								cd_medicamento_w,
								cd_medicamento_w,
								cd_plano_w,
								cd_setor_atendimento_w,
								sysdate,
								null, --Local estoque
								null);
			end if;



			IF  (cd_unid_med_consumo_w = cd_unid_med_prescr_w) THEN
				qt_conversao_dose_w	:= 1;
			ELSE
				BEGIN
				SELECT	qt_conversao
				INTO	qt_conversao_dose_w
				FROM	material_conversao_unidade
				WHERE	cd_material 	= cd_medicamento_w
				AND	cd_unidade_medida = cd_unid_med_prescr_w;
				EXCEPTION
					WHEN OTHERS THEN
						qt_conversao_dose_w	:= 1;
				END;
			END IF;

			qt_unitaria_w		:= dividir(qt_dose_prescricao_w , qt_conversao_dose_w);
			qt_total_dispensar_w	:= dividir(qt_unitaria_w , qt_minimo_multiplo_solic_w) * qt_minimo_multiplo_solic_w;

			IF	(qt_total_dispensar_w < qt_unitaria_w) THEN
				qt_total_dispensar_w := qt_total_dispensar_w + qt_minimo_multiplo_solic_w;
			END IF;

			insert	into material_autorizado
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_sequencia_autor,
				cd_material,
				qt_solicitada,
				nr_seq_fabricante,
				qt_autorizada,
				ie_via_aplicacao,
				cd_intervalo_medic_quimio,
				nr_seq_regra_plano,
				nr_seq_regra_quimio,
				cd_material_informado)
			values	(material_autorizado_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_autor_p,
				cd_medicamento_w,
				obter_dose_convertida(cd_medicamento_w, calcula_dose_total_quimio(qt_total_dispensar_w, qt_dias_util_w, cd_intervalo_w), cd_unid_med_prescr_w, substr(Obter_Dados_Material(cd_medicamento_w,'UMC'),1,30)),
				nr_seq_fabricante_w,
				0,
				ie_via_aplicacao_w,
				cd_intervalo_w,
				nr_seq_regra_w,
				nr_sequencia_regra_w,
				cd_material_w);

			commit;
		END IF;
	END IF;

	END;
END LOOP;
CLOSE c01;

COMMIT;

END GERAR_APRESENT_AUTOR_ONC;
/
