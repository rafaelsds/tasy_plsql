create or replace
procedure Alterar_pf_pj_titulo_rec_H5 (
			nr_titulo_p		number,
			cd_pessoa_fisica_p	varchar2,
			cd_cgc_p		varchar2,
			nm_usuario_p		Varchar2) is 

begin

if	((cd_pessoa_fisica_p is null) and (cd_cgc_p is null)) or
	((cd_pessoa_fisica_p is not null) and (cd_cgc_p is not null)) then
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(281648);
end if;


Alterar_pf_pj_titulo_receber(nr_titulo_p,
			cd_pessoa_fisica_p,
			cd_cgc_p,
			nm_usuario_p);

atualiza_hist_lote_receber(nm_usuario_p, wheb_mensagem_pck.GET_TEXTO(712659), nr_titulo_p);

commit;

end Alterar_pf_pj_titulo_rec_H5;
/

