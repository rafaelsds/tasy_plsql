CREATE OR REPLACE
PROCEDURE GERAR_DATA_FIM_ALERTA(nr_sequencia_p	NUMBER,
								dt_fim_p		VARCHAR2,
								dt_fim_date_p	DATE DEFAULT NULL,
								ie_opcao_p VARCHAR2 DEFAULT 'P') IS 				   
BEGIN
IF	(nr_sequencia_p IS NOT NULL) AND 
	(NVL(ie_opcao_p,'P') = 'P') THEN
	
	IF	(dt_fim_date_p	IS NOT NULL) THEN
		UPDATE 	alerta_paciente
		SET 	dt_fim_alerta = dt_fim_date_p,
			dt_atualizacao = sysdate,
			nm_usuario = wheb_usuario_pck.get_nm_usuario		
		WHERE 	nr_sequencia = nr_sequencia_p;

	ELSE

		UPDATE 	alerta_paciente
		SET 	dt_fim_alerta = TO_DATE(dt_fim_p,'dd/mm/yyyy hh24:mi:ss'),
			dt_atualizacao = sysdate,
			nm_usuario = wheb_usuario_pck.get_nm_usuario
		WHERE 	nr_sequencia = nr_sequencia_p;
	
	END IF;
ELSE 
	IF	(dt_fim_date_p	IS NOT NULL) THEN
		UPDATE 	atendimento_alerta
		SET 	dt_fim_alerta = dt_fim_date_p,
			dt_atualizacao = sysdate,
			nm_usuario = wheb_usuario_pck.get_nm_usuario
		WHERE 	nr_sequencia = nr_sequencia_p;

	ELSE
	
		UPDATE 	atendimento_alerta
		SET 	dt_fim_alerta = TO_DATE(dt_fim_p,'dd/mm/yyyy hh24:mi:ss'),
			dt_atualizacao = sysdate,
			nm_usuario = wheb_usuario_pck.get_nm_usuario
		WHERE 	nr_sequencia = nr_sequencia_p;
	
	END IF;
END IF;

COMMIT;

END GERAR_DATA_FIM_ALERTA;
/