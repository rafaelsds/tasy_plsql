create or replace 
procedure gerar_d301_seg_fab_rech(	nr_atendimento_p in atendimento_paciente.nr_atendimento%type,
					nr_seq_dataset_p in number,
					nm_usuario_p	 in varchar2)  is

cd_departamento_w              departamento_medico.cd_departamento%type;
nr_seq_301_depart_w            d301_segmento_fab.nr_seq_301_depart%type;
   
cursor c01 is
select 	distinct apu.dt_entrada_unidade as dt_referencia,
	apu.cd_departamento as cd_departamento
from 	atend_paciente_unidade apu,
	setor_atendimento      sea
where 	apu.nr_atendimento       = nr_atendimento_p    
and 	apu.cd_setor_atendimento = sea.cd_setor_atendimento
and 	sea.cd_classif_setor in (2,3,4);

c01_w c01%rowtype;

begin

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	nr_seq_301_depart_w := obter_seq_valor_301('C301_6_DEPARTAMENTO','CD_DEPARTAMENTO',obter_conversao_301('C301_6_DEPARTAMENTO','DEPARTAMENTO_MEDICO',NULL,c01_w.cd_departamento,'I'));

	insert into d301_segmento_fab
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_dataset,
		nr_seq_301_depart) 
	values (d301_segmento_fab_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_dataset_p,
		nr_seq_301_depart_w);

end loop;
close C01;
  
end gerar_d301_seg_fab_rech;
/