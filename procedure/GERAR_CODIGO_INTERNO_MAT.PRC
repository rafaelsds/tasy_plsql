create or replace
procedure GERAR_CODIGO_INTERNO_MAT (	cd_convenio_p	in	number,
					nm_usuario_p	in	varchar2) is


nr_seq_w		number(10,0);
nr_seq_conv_w		number(10) := 0;
nr_seq_conv_prox_w	number(10) := 0;
cd_item_convenio_w	varchar2(20);
cd_item_convenio_ww	varchar2(20);
cd_material_w		number(10,0);
qt_update_w		number(1) := 0;
nr_seq_ant_w		Number(10) := 0;
cd_mat_simpro_w		number(6);
cd_mat_brasindice_w	number(6);
ds_item_conv_carga_w		varchar2(150);
ds_item_conv_conversao_w	varchar2(200);
ie_atualiza_desc_w		varchar2(1);
dt_vigencia_w		date;
cd_estabelecimento_w	number(10) := wheb_usuario_pck.get_cd_estabelecimento;

cursor c01 is
select	NR_SEQUENCIA,
	CD_ITEM_CONVENIO,
	ds_item_convenio
from	convenio_carga_preco
where	cd_convenio = cd_convenio_p
and	ie_status = 'C'
order by cd_item_convenio,
	nr_sequencia;

cursor c02 is
select	cd_material,
	cd_material_convenio,
	nr_sequencia,
	ds_material_convenio
from	conversao_material_convenio a
where	a.cd_convenio		= cd_convenio_p
and	a.cd_material_convenio	= cd_item_convenio_w
and 	a.ie_situacao = 'A'
and	dt_vigencia_w between nvl(dt_inicio_vigencia,dt_vigencia_w) and nvl(dt_final_vigencia,dt_vigencia_w)
and	nvl(a.cd_estabelecimento,cd_estabelecimento_w)	= cd_estabelecimento_w
and	not exists
	(select	1
	from 	convenio_carga_preco x
	where	x.cd_convenio		= cd_convenio_p
	and	x.ie_status 		= 'V'
	and	x.CD_ITEM_CONVENIO	= cd_item_convenio_w
	and	x.cd_material		= a.cd_material)
order by nr_sequencia,
	cd_material_convenio;

begin

dt_vigencia_w		:= sysdate;
ie_atualiza_desc_w	:= nvl(Obter_Valor_Param_Usuario(921,125,obter_perfil_ativo,nm_usuario_P,wheb_usuario_pck.get_cd_estabelecimento),'N');

open c01;
loop
fetch c01 into
	nr_seq_w,
	cd_item_convenio_w,
	ds_item_conv_carga_w;
exit when c01%notfound;
	begin
	open c02;
	loop
	fetch c02 into
		cd_material_w,
		cd_item_convenio_ww,
		nr_seq_conv_w,
		ds_item_conv_conversao_w;
	exit when c02%notfound;
		begin				
		if	(nr_seq_ant_w <> nr_seq_w) then
			
			if	(ie_atualiza_desc_w = 'S') and 
				(ds_item_conv_carga_w <> ds_item_conv_conversao_w) then
			
				update  conversao_material_convenio
				set 	ds_material_convenio = ds_item_conv_carga_w
				where	nr_sequencia = nr_seq_conv_w;
				
			end if;
			
			update	convenio_carga_preco
			set	cd_material	= cd_material_w,
				ie_status	= 'V',
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate,
				nr_seq_regra_conv = nr_seq_conv_w
			where	nr_sequencia	= nr_seq_w;
			nr_seq_ant_w	:= nr_seq_w;
		else
			duplica_carga_preco(nr_seq_w, cd_material_w, nm_usuario_p, nr_seq_conv_w);
		end if;
		end;
	end loop;
	close c02;
	end;
	qt_update_w := 0;
end loop;
close c01;


if	(Obter_Valor_Param_Usuario(obter_funcao_Ativa,33,obter_perfil_ativo,nm_usuario_P,0)	= 'S') then
	open C01;
	loop
	fetch C01 into
		nr_seq_w,
		cd_item_convenio_w,
		ds_item_conv_carga_w;
	exit when C01%notfound;
		begin
		cd_material_w		:= null;
		cd_mat_simpro_w		:= obter_material_simpro(somente_numero(cd_item_convenio_w));
		cd_mat_brasindice_w	:= obter_material_brasindice(cd_item_convenio_w,null,null);
		if	(cd_mat_simpro_w	is not null) then
			cd_material_w		:= cd_mat_simpro_w;
		elsif	(cd_mat_brasindice_w	is not null) then
			cd_material_w		:= cd_mat_brasindice_w;
		end if;		
		
		if	(cd_material_w	is not null) then
	
			update	convenio_carga_preco
			set	cd_material	= cd_material_w,
				ie_status	= 'V',
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_w;

		end if;
		end;
	end loop;
	close C01;
end if;


commit;

end GERAR_CODIGO_INTERNO_MAT;
/