create or replace
PROCEDURE qua_excluir_nao_conformidade(nr_sequencia_p	Number) is

BEGIN

delete	from qua_nao_conform_setor
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from qua_nao_conform_causa
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from man_ordem_servico
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from qua_nao_conform_eficacia
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from qua_nao_conform_hist
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from qua_nao_conform_anexo
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from qua_nao_conform_envio
where	nr_seq_nao_conform = nr_sequencia_p;

delete	from qua_nao_conformidade
where	nr_sequencia = nr_sequencia_p;

commit;

END qua_excluir_nao_conformidade;
/