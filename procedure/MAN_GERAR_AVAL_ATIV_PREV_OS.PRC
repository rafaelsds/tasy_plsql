create or replace
procedure man_gerar_aval_ativ_prev_os(
				nr_sequencia_p		number,
				nm_usuario_p		varchar2) is
				
nr_seq_tipo_avaliacao_w		number(10);
				
Cursor C01 is
	select	b.nr_seq_tipo_avaliacao
	from	des_ativ_exec b,
		man_ordem_ativ_prev a
	where	b.nr_sequencia = a.nr_seq_ativ_exec
	and	trunc(a.dt_prevista) >= trunc(sysdate)
	and	a.dt_real is null
	and	b.nr_seq_tipo_avaliacao is not null
	and	a.nr_seq_ordem_serv = nr_sequencia_p
	and not exists(	select	1
			from	man_ordem_serv_avaliacao x
			where	x.nr_seq_ordem_servico = a.nr_seq_ordem_serv
			and	x.nr_seq_tipo_avaliacao = b.nr_seq_tipo_avaliacao)
	group by b.nr_seq_tipo_avaliacao;

begin

if	(nvl(nr_sequencia_p,0) <> 0) and
	(nvl(nm_usuario_p,'X') <> 'X') then
	begin
	open C01;
	loop
	fetch C01 into	
		nr_seq_tipo_avaliacao_w;
	exit when C01%notfound;
		insert into man_ordem_serv_avaliacao(
					nr_sequencia,
					nr_seq_ordem_servico,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_avaliacao,
					nr_seq_tipo_avaliacao)
				values(	man_ordem_serv_avaliacao_seq.nextval,
					nr_sequencia_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_tipo_avaliacao_w);
	end loop;
	close C01;
	
	commit;
	end;
end if;

end man_gerar_aval_ativ_prev_os;
/