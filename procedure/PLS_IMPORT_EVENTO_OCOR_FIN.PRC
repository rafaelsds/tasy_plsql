create or replace
procedure pls_import_evento_ocor_fin(	ds_conteudo_p		in varchar2,
					nr_linha_p		in number,
					nr_seq_lote_p		in pls_evento_imp_lote.nr_sequencia%type,
					nm_usuario_p		in usuario.nm_usuario%type) is

nr_cpf_prest_w		varchar2(14);
nr_cgc_prest_w		varchar2(14);
vl_lancamento_w		number(15,2);
ds_observacao_w		varchar2(150);

begin

if	(ds_conteudo_p is not null) then

	-- tenta converter o valor para number, caso n�o consiga insere zero
	begin
		vl_lancamento_w := to_number(substr(ds_conteudo_p, 15, 13) || ',' || substr(ds_conteudo_p, 28, 2));
	exception
	when others then
		vl_lancamento_w := 0;
	end;

	-- usado trim para remover os espa�os em branco
	nr_cgc_prest_w := trim(substr(ds_conteudo_p, 1, 14));
	
	-- caso tenha 11 posi��es, ent�o � cpf
	if	(length(nr_cgc_prest_w) = 11) then
	
		nr_cpf_prest_w := nr_cgc_prest_w;
		nr_cgc_prest_w := null;
	end if;
	
	ds_observacao_w := substr(ds_conteudo_p, 30, 150);
	
	insert into pls_evento_imp_ocor_fin(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, nr_cpf_prest,
		cd_cgc_prest, vl_lancamento, ds_observacao,
		nr_linha, nr_seq_lote, ie_consistente
	) values (
		pls_evento_imp_ocor_fin_seq.nextval, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, nr_cpf_prest_w,
		nr_cgc_prest_w, vl_lancamento_w, ds_observacao_w,
		nr_linha_p, nr_seq_lote_p, 'N' -- no momento da importa��o ainda n�o foi consistido
	);

	update	pls_evento_imp_lote
	set	dt_importacao = sysdate
	where	nr_sequencia = nr_seq_lote_p;
	
	commit;
end if;

end pls_import_evento_ocor_fin;
/