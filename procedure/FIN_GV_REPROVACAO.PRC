create or replace
procedure fin_gv_reprovacao(	nr_seq_pend_aprov_p	number,
				ie_tipo_p		varchar2,
				nr_mtvo_reprov_p	number,
				ds_mtvo_reprov_p	varchar2,
				nm_usuario_p		Varchar2) is 

/*
ie_tipo_p indica tipo de aprovação:
A = Reprovação de Adiantamento
D = Reprovação de Despesas
*/	

nr_seq_viagem_w		number(10);		
cd_pessoa_fisica_w  	varchar2(10);
cd_pessoa_aprov_w  	varchar2(10);
nm_usuario_viajante_w	varchar2(15);
nm_usuario_confirm_w	varchar2(15);
nm_usuario_confir_w	varchar2(15);
ds_usuario_receb_w	varchar2(255);
ds_destino_w		varchar2(255);
ds_origem_w		varchar2(255);
ds_destino_confirm_w	varchar2(255);

begin
select	nr_seq_viagem, cd_pessoa_fisica, cd_pessoa_aprov
into	nr_seq_viagem_w, cd_pessoa_fisica_w, cd_pessoa_aprov_w
from	fin_gv_pend_aprov
where	nr_sequencia = nr_seq_pend_aprov_p;

if 	(ie_tipo_p = 'A') then
	begin
	update 	fin_gv_pend_aprov
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		dt_reprov_adia = sysdate,
		nr_mtvo_reprov_adia = nr_mtvo_reprov_p,
		ds_mtvo_reprov_adia = ds_mtvo_reprov_p,
		ie_situacao_pend = ie_situacao_pend
	where	nr_sequencia = nr_seq_pend_aprov_p;

	update 	via_adiantamento
	set	dt_reprovacao = sysdate,
		nm_usuario_reprov = nm_usuario_p,
		nr_mtvo_reprov = nr_mtvo_reprov_p
	where	nr_seq_viagem = nr_seq_viagem_w
	and	dt_liberacao is not null
	and	dt_aprovacao is null
	and	dt_reprovacao is null;
	end;
elsif	(ie_tipo_p = 'D') then
	begin
	update 	fin_gv_pend_aprov
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		dt_reprov_desp = sysdate,
		nr_mtvo_reprov_desp = nr_mtvo_reprov_p,
		ds_mtvo_reprov_desp = ds_mtvo_reprov_p,		
		ie_situacao_pend = ie_situacao_pend
	where	nr_sequencia = nr_seq_pend_aprov_p;
	
	update 	via_viagem	
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_etapa_viagem = 7
	where	nr_sequencia = nr_seq_viagem_w;	
	end;
elsif	(ie_tipo_p = 'T') then
	begin
	update 	fin_gv_pend_aprov
	set	nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		dt_aprov_transp		= null,
		dt_reprov_transp	= sysdate,
		nr_mtvo_reprov_adia	= nr_mtvo_reprov_p,
		ds_mtvo_reprov_adia	= ds_mtvo_reprov_p
	where	nr_sequencia		= nr_seq_pend_aprov_p;

	update 	via_viagem -- Retornar GV para prevista
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_etapa_viagem = 0,
		nm_usuario_confir = null,
		dt_confirmacao = null
	where	nr_sequencia = nr_seq_viagem_w;
	end;
elsif	(ie_tipo_p = 'H') then
	begin
	update 	fin_gv_pend_aprov
	set	nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		dt_reprov_hosp	= sysdate,
		dt_aprov_hosp	= null,
		nr_mtvo_reprov_adia	= nr_mtvo_reprov_p,
		ds_mtvo_reprov_adia	= ds_mtvo_reprov_p
	where	nr_sequencia	= nr_seq_pend_aprov_p;

	update 	via_viagem -- Retornar GV para prevista
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_etapa_viagem = 0,
		nm_usuario_confir = null,
		dt_confirmacao = null
	where	nr_sequencia = nr_seq_viagem_w;
	end;
elsif	(ie_tipo_p = 'R') then
	begin
	update 	fin_gv_pend_aprov
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		dt_reprov_hosp = sysdate,
		dt_aprov_hosp = null,
		dt_reprov_transp = null,
		dt_aprov_transp = null,
		dt_reprov_gv = null,
		nr_mtvo_reprov_desp = nr_mtvo_reprov_p,
		ds_mtvo_reprov_desp = ds_mtvo_reprov_p,
		ie_situacao_pend = 'RR'
	where	nr_sequencia = nr_seq_pend_aprov_p;

	update	via_relat_desp
	set	dt_aprovacao = null,
		dt_liberacao = null,
		nm_usuario_aprov = null,
		nm_usuario_libera = null
	where	nr_seq_viagem = nr_seq_viagem_w;
	
	update	via_viagem
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_etapa_viagem = 4
	where	nr_sequencia = nr_seq_viagem_w;
	end;
elsif	(ie_tipo_p = 'V') then
	begin
	update 	fin_gv_pend_aprov
	set	nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		dt_aprov_gv	= null,
		dt_reprov_gv	= sysdate,
		nr_mtvo_reprov_adia	= nr_mtvo_reprov_p,
		ds_mtvo_reprov_adia	= ds_mtvo_reprov_p,
		ie_situacao_pend	= 'CG'
	where	nr_sequencia	= nr_seq_pend_aprov_p;

	update 	via_viagem -- reprovar viagem
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_etapa_viagem = 9
	where	nr_sequencia = nr_seq_viagem_w;
	
	select	max(a.nm_usuario)
	into	nm_usuario_viajante_w
	from	usuario a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	select	max(a.nm_usuario_confir)
	into	nm_usuario_confir_w
	from	via_viagem a
	where	a.nr_sequencia = nr_seq_viagem_w;
	
	ds_usuario_receb_w := nm_usuario_viajante_w || ',' || nm_usuario_confir_w;

	gerar_comunic_padrao(	sysdate,
				'Adiantamento da GV: '|| nr_seq_viagem_w || ' Reprovado.',
				'Pessoa Solicitante: ' || substr(obter_nome_usuario(nm_usuario_viajante_w),1,80) || chr(13) || chr(10) ||
				'Confirmada por: ' || obter_nome_usuario(nm_usuario_viajante_w) || chr(13) || chr(10) || chr(13) || chr(10) ||
				'reprovado por: ' || obter_nome_usuario(nm_usuario_p),
				nm_usuario_p, 'N', ds_usuario_receb_w, 'S', null, '', '', '', sysdate, '', '');

	select 	nvl(max(ds_email), '') email_origem
	into 	ds_origem_w
	from 	usuario 
	where 	nm_usuario = nm_usuario_p;

	select 	nvl(max(ds_email), '') email_destino
	into 	ds_destino_w
	from 	usuario 
	where 	nm_usuario = nm_usuario_viajante_w;

	select	nvl(max(ds_email), '')
	into	ds_destino_confirm_w
	from	usuario
	where	nm_usuario = nm_usuario_confir_w;

	ds_destino_w := ds_destino_w || ';' || ds_destino_confirm_w;

	enviar_email(	'Adiantamento da GV: '|| nr_seq_viagem_w || ' aprovado.',
			'Pessoa Solicitante: ' || substr(obter_nome_usuario(nm_usuario_viajante_w),1,80) || chr(13) || chr(10) ||
			'Confirmada por: ' || obter_nome_usuario(nm_usuario_confir_w) || chr(13) || chr(10) || chr(13) || chr(10) ||
			'Aprovador por: ' || obter_nome_usuario(nm_usuario_p),ds_origem_w,ds_destino_w,nm_usuario_p,'M');
	end;
end if;

commit;

end fin_gv_reprovacao;
/