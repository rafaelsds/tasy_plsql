create or replace
procedure obter_dados_pat_transf(
		vl_original_p	out	number,
		nr_sequencia_p		number,
		nr_seq_tipo_p	 	number,
		ie_vl_venda_p	out	varchar2,
		nm_usuario_p		varchar2) is

vl_original_w	number(15,2);
ie_valor_w	varchar2(1);
begin
if 	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin

	select	nvl(vl_original,0)
	into	vl_original_w
	from	pat_bem
	where	nr_sequencia = nr_sequencia_p;

	select	nvl(a.ie_valor, 'N')
	into	ie_valor_w
	from	pat_tipo_historico a
	where	a.nr_sequencia	= nr_seq_tipo_p;

	if	(ie_valor_w = 'V') then
		begin
		ie_vl_venda_p := ie_valor_w;
		end;
	end if;
	end;
end if;	
vl_original_p	:= vl_original_w;
commit;
end obter_dados_pat_transf;
/