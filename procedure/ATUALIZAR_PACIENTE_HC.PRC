create or replace
procedure atualizar_paciente_hc(	nr_atendimento_p	number,
				nr_sequencia_p		number) is 

begin

if	(nr_atendimento_p is not null)then
	begin
	
	update 	paciente_hc_atend
	set 	nr_atendimento 	= nr_atendimento_p
	where 	nr_sequencia 	= nr_sequencia_p;
	
	end;
end if;

commit;

end atualizar_paciente_hc;
/