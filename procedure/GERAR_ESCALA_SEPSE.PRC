create or replace
procedure gerar_escala_sepse(   nr_atendimento_p    number,
                                cd_profissional_p   varchar2,
                                nr_seq_p            number,
                                nm_usuario_p        Varchar2,
                                nm_tabela_origem_p  Varchar2 default null) is 

nr_sequencia_w              number(10);
dt_alta_w                   date;
qt_var_suspeita_w           number(10);
qt_var_confirmada_w         number(10);
qt_total_w                  number(10);
cd_estabelecimento_w        number(4);
qt_horas_sepsis_w           number(10);
qt_reg_sepsis_w             number(10) := 0; 
existe_liberacao_sepsis_w   varchar2(1);
ie_finaliza_manual_w        varchar2(1);
nr_seq_origem_w             number(10) := null; 
nm_tabela_origem_w          varchar(50):= null;
nr_seq_sv_w                 number(10) := null;
ie_status_sepsis_w			varchar(2);

begin
	
if 	(nvl(nr_atendimento_p,0) > 0) then

	Select 	max(dt_alta)
	into	dt_alta_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	if	(dt_alta_w is null) then		    
			
			select obter_dados_atendimento(nr_atendimento_p,'EST'),
				   obter_se_sepse_liberada(nr_atendimento_p)
			into   cd_estabelecimento_w,
				   existe_liberacao_sepsis_w
			from	dual;

		if	(existe_liberacao_sepsis_w <> 'N' ) then

			Select  max(QT_HORAS_SEPSIS)
			into	qt_horas_sepsis_w
			from 	parametro_medico
			where	cd_estabelecimento = cd_estabelecimento_w;
			
			nr_seq_origem_w    := nr_seq_p;
			nm_tabela_origem_w := nm_tabela_origem_p;
			if ((nm_tabela_origem_w is null) and (nr_seq_p > 0)) then
				nm_tabela_origem_w := 'ATENDIMENTO_SINAL_VITAL';
				nr_seq_origem_w    := nr_seq_p;
				nr_seq_sv_w        := nr_seq_p;
			end if;

			--- SEPSE PEDIATRICA
			if (existe_liberacao_sepsis_w = 'P') then
			
				ie_finaliza_manual_w := obter_se_sepse_liberada(nr_atendimento_p, 'F');
				
				if (ie_finaliza_manual_w = 'S') then
					Select  count(*)
					into	qt_reg_sepsis_w
					from	escala_sepse_infantil
					where 	nr_atendimento = nr_atendimento_p
					and     dt_fim_protocolo is null
					and	    nvl(ie_situacao,'A') = 'A';
					
				elsif (nvl(qt_horas_sepsis_w,0) > 0) then
				
					Select  count(*)
					into	qt_reg_sepsis_w
					from	escala_sepse_infantil
					where 	nr_atendimento = nr_atendimento_p
					and	dt_atualizacao_nrec between (sysdate  - (1/24 * qt_horas_sepsis_w)) and sysdate
					and	nvl(ie_situacao,'A') = 'A';
				
				end if;
				
				if	(qt_reg_sepsis_w = 0) then						

					select  escala_sepse_infantil_seq.NEXTVAL
					into    nr_sequencia_w
					from    dual;                  
					

					insert into escala_sepse_infantil (nr_sequencia,
											  dt_atualizacao,
											  nm_usuario,
											  nr_atendimento,
											  dt_avaliacao,
											  ie_situacao,
											  cd_profissional,
											  qt_pontuacao,
											  dt_liberacao,
											  nr_seq_sv,
											  nm_usuario_status,
											  dt_atualizacao_nrec,
											  nm_usuario_nrec, 
											  ie_padrao_variavel,
											  nm_tabela,
											  nr_seq_origem)
									  values (nr_sequencia_w,
											  sysdate,
											  nm_usuario_p,
											  nr_atendimento_p,
											  sysdate,
											  'A',
											  cd_profissional_p,
											  0,
											  sysdate,
											  nr_seq_sv_w,
											  nm_usuario_p,
											  sysdate,
											  nm_usuario_p, 
											  obter_padrao_deflagador_sepse,
											  nm_tabela_origem_w,
											  nr_seq_origem_w);
		
					commit;
					
					gerar_itens_escala_sepse(nr_sequencia_w, nr_atendimento_p, nm_usuario_p);
					
					select count (1)
					into   qt_reg_sepsis_w
					from   escala_sepse_infantil
					where  nr_sequencia = nr_sequencia_w
					and    ie_status_sepsis is null;
		
					if (qt_reg_sepsis_w > 0) then
						delete from escala_sepse_infantil_item where nr_seq_escala = nr_sequencia_w;
						
						delete from	escala_sepse_infantil where	nr_sequencia = nr_sequencia_w;
							
						commit;
					
					end if;
				
				end if;
			
			--- SEPSE ADULTO				
			else
				if	(existe_liberacao_sepsis_w = 'A' ) and
					(obter_se_gera_sepsis(nr_atendimento_p, cd_estabelecimento_w) = 'S')then
					
					select  escala_sepse_seq.NEXTVAL
					into    nr_sequencia_w
					from    dual;
				
					insert into escala_sepse (nr_sequencia,
										  dt_atualizacao,
										  nm_usuario,
										  nr_atendimento,
										  dt_avaliacao,
										  ie_situacao,
										  cd_profissional,
										  qt_pontuacao,
										  dt_liberacao,
										  nr_seq_sv,
										  nm_usuario_status,
										  dt_atualizacao_nrec,
										  nm_usuario_nrec, 
										  ie_padrao_variavel,
										  nm_tabela,
										  nr_seq_origem)
								  values (nr_sequencia_w,
										  sysdate,
										  nm_usuario_p,
										  nr_atendimento_p,
										  sysdate,
										  'A',
										  cd_profissional_p,
										  0,
										  sysdate,
										  nr_seq_sv_w,
										  nm_usuario_p,
										  sysdate,
										  nm_usuario_p, 
										  obter_padrao_deflagador_sepse,
										  nm_tabela_origem_w,
										  nr_seq_origem_w);

				commit;

				gerar_itens_escala_sepse(nr_sequencia_w, nr_atendimento_p, nm_usuario_p);

				delete 
				from	escala_sepse
				where	nr_sequencia = nr_sequencia_w
				and     ie_status_sepsis is null;
					
				commit;
				
				end if;
			end if;
		end if;	
	end if;
end if;	

end gerar_escala_sepse;
/
