create or replace
procedure delete_reg_program_prs (nr_customer_requirement_p number) is  
begin
delete from reg_program_prs
where nr_sequencia in (    select b.nr_sequencia
            from reg_product_requirement a,
                reg_program_prs b
            where a.nr_sequencia = b.nr_seq_product_requirement
            and a.nr_customer_requirement =  nr_customer_requirement_p);
end delete_reg_program_prs;
/