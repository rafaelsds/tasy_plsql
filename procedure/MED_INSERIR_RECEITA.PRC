create or replace
procedure Med_Inserir_Receita
			(cd_medico_p		varchar2,
			nr_ficha_p		number,
			dt_receita_p		date,
			ds_receita_p		varchar2,
			nm_usuario_p		varchar2) is


nr_seq_receita_w	number(10,0);
nr_seq_cliente_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_cliente_w
from	med_cliente
where	cd_medico		= cd_medico_p
and	cd_pessoa_sist_orig	= nr_ficha_p;


if	(nr_seq_cliente_w > 0) then
	begin

	select	med_receita_seq.nextval
	into	nr_seq_receita_w
	from	dual;

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	med_cliente
	where	nr_sequencia	= nr_seq_cliente_w;

	insert	into med_receita
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_receita,
		ds_receita,
		nr_seq_cliente,
		cd_pessoa_fisica)
	values	(nr_seq_receita_w,
		sysdate,
		nm_usuario_p,
		dt_receita_p,
		nvl(ds_receita_p, ' '),
		nr_seq_cliente_w,
		cd_pessoa_fisica_w);
	
	end;
end if;

commit;

end Med_Inserir_Receita;
/