create or replace
procedure gerar_horarios_livres_medico(	cd_medico_p		varchar2,
					cd_estabelecimento_p	number,
					dt_agenda_p		date,
					dt_final_p		date,
					nm_usuario_p		varchar2,
					nr_seq_proc_interno_p	number,
					cd_agenda_p		number,
					cd_perfil_p		number)
					is
cd_agenda_w	number(10,0);
dt_agenda_w	date;
ds_retorno_w	varchar2(255);
ie_lib_sala_w	varchar2(1) := 'S';

cursor c00 is
	select	distinct a.cd_agenda
	from	agenda_medico a
	where	obter_se_lib_agenda_medico(a.cd_agenda,cd_medico_p) = 'S'
	and	((cd_agenda_p is null) or (cd_agenda	= cd_agenda_p))
	and	'S'		= (	select	nvl(max(x.ie_permite), 'S')
                     			from	proc_interno_agenda x
	                  		where	x.nr_seq_proc_interno  	= nr_seq_proc_interno_p
                  			and	x.cd_agenda		= a.cd_agenda)
	and	(obter_se_lib_agenda_perfil(a.cd_agenda,cd_perfil_p) = 'S')
	and	exists(select 1 from agenda x where a.cd_agenda = x.cd_agenda and nvl(x.ie_situacao,'A') = 'A')
	union
	select	distinct c.cd_agenda
	from	agenda c
	where	ie_situacao 	= 'A'
	and	cd_tipo_agenda	= 1
	and	((cd_agenda_p is null) or (cd_agenda	= cd_agenda_p))
	and	'S'		= (	select	nvl(max(x.ie_permite), 'S')
                     			from	proc_interno_agenda x
	                  		where	x.nr_seq_proc_interno  	= nr_seq_proc_interno_p
                  			and	x.cd_agenda		= c.cd_agenda)
	and	ie_lib_sala_w = 'N'
	and	(obter_se_lib_agenda_perfil(c.cd_agenda,cd_perfil_p) = 'S');

begin

obter_param_usuario(870,8,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_lib_sala_w);

dt_agenda_w	:=	dt_agenda_p;

open c00;
loop
	fetch c00	into
		cd_agenda_w;
	exit when c00%notfound;
	begin

	dt_agenda_w	:=	dt_agenda_p;

	while	((trunc(dt_agenda_w) < trunc(dt_final_p)) or
          (trunc(dt_agenda_w) = trunc(dt_final_p))) loop
		Obter_Horarios_Livres_cirurgia(cd_estabelecimento_p, cd_agenda_w, trunc(dt_agenda_w,'dd'),
					trunc(dt_agenda_w,'dd'),0,'','','',nm_usuario_p,
					'S',ds_retorno_w);
		dt_agenda_w	:=	dt_agenda_w + 1;	
	end loop;

	end;

end loop;
close c00;

commit;

end gerar_horarios_livres_medico;
/
