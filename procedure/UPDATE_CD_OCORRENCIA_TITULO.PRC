create or replace
procedure update_cd_ocorrencia_titulo(
		cd_ocorencia_p 		varchar2,
		nr_sequencia_p 		number,
		nm_usuario_p		Varchar2,
		cd_banco_p		number) is 

begin
	if	((cd_ocorencia_p is not null) and (nr_sequencia_p is not null)) then
		begin
		update  titulo_receber_cobr 
		set     cd_ocorrencia  	= cd_ocorencia_p,
			nm_usuario 	= nm_usuario_p,
			cd_banco	= cd_banco_p,
			dt_atualizacao	= sysdate
		where   nr_sequencia	= nr_sequencia_p;
		end;
	end if; 
commit;

end update_cd_ocorrencia_titulo;
/
