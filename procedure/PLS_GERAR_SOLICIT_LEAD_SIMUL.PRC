create or replace
procedure pls_gerar_solicit_lead_simul
			(	nr_seq_simulacao_p		number,
				nm_pessoa_solicitacao_p		varchar2,
				nm_contato_p			varchar2,
				cd_cep_p			varchar2,
				ds_endereco_p			varchar2,
				nr_endereco_p			varchar2,
				ds_bairro_p			varchar2,
				ds_complemento_p		varchar2,
				cd_municipio_ibge_p		varchar2,
				sg_estado_p			varchar2,
				dt_nascimento_p			varchar2,
				nr_ddi_p			varchar2,
				nr_ddd_p			varchar2,
				nr_telefone_p			varchar2,
				ds_email_p			varchar2,
				nr_celular_p			varchar2,
				nr_acao_origem_p		number,
				ie_tipo_contratacao_p		varchar2,
				nr_seq_classificacao_p		number,
				nr_seq_agente_motiv_p		number,
				cd_pf_vinculado_p		varchar2,
				cd_cgc_vinculado_p		varchar2,
				nr_seq_solicitacao_p	out	number,
				cd_estabelecimento_p		number,
				nm_usuario_p			Varchar2) is 
			
nr_seq_solicitacao_w		number(10);
dt_nascimento_w			date;
qt_idade_w			number(10);

begin

select	pls_solicitacao_comercial_seq.nextval
into	nr_seq_solicitacao_w
from	dual;

if	(elimina_caracteres_especiais(dt_nascimento_p) is not null) then
	dt_nascimento_w	:=	to_date(dt_nascimento_p);
	/*aaschlote 30/03/2011 OS - 300982*/
	qt_idade_w	:= 	obter_idade(dt_nascimento_w, SYSDATE,'A');
else
	dt_nascimento_w	:= null;
	qt_idade_w	:= 0;
end if;	


insert into pls_solicitacao_comercial
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		cd_estabelecimento,nm_pessoa_fisica,nm_contato,ie_status,ie_origem_solicitacao,
		cd_cep,ds_endereco,nr_endereco,ds_bairro,ds_complemento,
		cd_municipio_ibge,sg_uf_municipio,dt_nascimento,nr_ddi,nr_ddd,
		nr_telefone,ds_email,nr_celular,ie_tipo_contratacao,nr_acao_origem,
		ie_etapa_solicitacao,nr_seq_agente_motivador,nr_seq_classificacao,cd_pf_vinculado,cd_cnpj_vinculado,
		ds_observacao,dt_solicitacao,qt_idade)
values	(	nr_seq_solicitacao_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		cd_estabelecimento_p,substr(nm_pessoa_solicitacao_p,1,60),nm_contato_p,'PE','S',
		cd_cep_p,ds_endereco_p,nr_endereco_p,ds_bairro_p,ds_complemento_p,
		cd_municipio_ibge_p,sg_estado_p,dt_nascimento_w,nr_ddi_p,nr_ddd_p,
		nr_telefone_p,ds_email_p,nr_celular_p,ie_tipo_contratacao_p,nr_acao_origem_p,
		'T',nr_seq_agente_motiv_p,nr_seq_classificacao_p,cd_pf_vinculado_p,cd_cgc_vinculado_p,
		'Gerado a partir da simula��o de pre�o '|| nr_seq_simulacao_p,sysdate,qt_idade_w);
		
update	pls_simulacao_preco
set	nr_seq_solicitacao	= nr_seq_solicitacao_w
where	nr_sequencia		= nr_seq_simulacao_p;
		
nr_seq_solicitacao_p	:= nr_seq_solicitacao_w;		

commit;

end pls_gerar_solicit_lead_simul;
/
