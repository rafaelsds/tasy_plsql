create or replace
procedure gera_same_pront_oper(
								nr_seq_tipo_operacao_p		number,
								cd_pessoa_fisica_p			varchar2,
								nm_usuario_p				varchar2,
								cd_estabelecimento_p		number) is 

nr_seq_prontuario_w 	number;
ds_parametro_w		varchar2(10) := obter_valor_param_usuario(941,103,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
								
Cursor c01 is
	select	nr_sequencia
	from	same_prontuario
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	((ds_parametro_w = 'N') or (cd_estabelecimento = cd_estabelecimento_p));
	
begin

open c01;
loop
fetch c01 into	
	nr_seq_prontuario_w;
exit when c01%notfound;
	begin
	
	insert	into	same_prontuario_operacao
			(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_tipo_operacao,
			nr_seq_prontuario
			)
			values
			(
			same_prontuario_operacao_seq.NextVal,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_tipo_operacao_p,
			nr_seq_prontuario_w
			);
	
	end;
end loop;
close c01;

commit;

end gera_same_pront_oper;
/
