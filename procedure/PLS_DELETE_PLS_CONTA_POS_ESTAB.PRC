create or replace
procedure pls_delete_pls_conta_pos_estab(	nr_seq_conta_proc_p	pls_conta_pos_estabelecido.nr_seq_conta_proc%type,
						nr_seq_conta_mat_P	pls_conta_pos_estabelecido.nr_seq_conta_mat%type,
						nr_seq_conta_p		pls_conta_pos_estabelecido.nr_seq_conta%type,
						nr_seq_pos_estab_p	pls_conta_pos_estabelecido.nr_sequencia%type) is 
		
nr_seq_mensalidade_seg_w	pls_conta_pos_estabelecido.nr_seq_mensalidade_seg%type;
qt_registros_w			pls_integer;
nr_seq_regra_limite_mens_w	pls_conta_pos_estabelecido.nr_seq_regra_limite_mens%type;
qt_registros_mensalidade_w	number(10);

Cursor C01 is
	select	nr_seq_conta_proc
	from	pls_conta_pos_estabelecido
	where	nr_seq_conta	= nr_seq_conta_p
	and	nr_seq_regra_tx_opme is not null
	and	ie_status_faturamento	!= 'A'
	and	ie_cobrar_mensalidade	!= 'A';
		
begin

if	(nr_seq_conta_proc_p	is not null) then
	select	count(1)
	into	qt_registros_w
	from	pls_conta_pos_estabelecido a
	where	nr_seq_conta_proc = nr_seq_conta_proc_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_proc b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);
				
	select	max(nr_seq_mensalidade_seg),
		max(nr_seq_regra_limite_mens)
	into	nr_seq_mensalidade_seg_w,
		nr_seq_regra_limite_mens_w
	from	pls_conta_pos_estabelecido a			
	where	nr_seq_conta_proc = nr_seq_conta_proc_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_proc b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);

	if	((nr_seq_mensalidade_seg_w is null) or (nr_seq_regra_limite_mens_w is null)) then
		select  count(1)
		into 	qt_registros_mensalidade_w
		from  	pls_mensalidade_item_conta a,
			pls_conta_pos_estabelecido e
		where  	a.nr_seq_conta_pos_estab = e.nr_sequencia
		and	e.ie_status_faturamento	!= 'A'
		and	e.ie_cobrar_mensalidade	!= 'A'
		and	e.nr_seq_conta_proc = nr_seq_conta_proc_p;
	end if;
				
	if	(qt_registros_w > 0) 	and
		(nr_seq_mensalidade_seg_w is null) and
		(nr_seq_regra_limite_mens_w is null) and 
		(qt_registros_mensalidade_w = 0 ) then
				
		delete	pls_ocorrencia_benef
		where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta_proc	= nr_seq_conta_proc_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
							
		delete	pls_ocorrencia_benef
		where	nr_seq_conta_proc in (	select	a.nr_sequencia          
						from	pls_conta_proc		a
						where	a.ie_status		= 'M'
						and	a.nr_seq_regra_pct_fat	is not null
						and	a.nr_sequencia		= nr_seq_conta_proc_p);
							
		delete	pls_conta_glosa
		where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta_proc	= nr_seq_conta_proc_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
		-- Tenta apagar tamb?m as glosas geradas diretamente pelos itens de pacote, necess?rio para os casos onde n?o tem p?s-estabelecido
		delete	pls_conta_glosa x
		where	x.nr_seq_conta_proc in (	select	a.nr_sequencia          
							from	pls_conta_proc		a
							where	a.ie_status		= 'M'
							and	a.nr_seq_regra_pct_fat	is not null
							and	a.nr_sequencia		= nr_seq_conta_proc_p);
							
		delete	pls_lib_fat_conta_pos a
		where	a.nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta_proc	= nr_seq_conta_proc_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
		
		delete  pls_conta_pos_estab_taxa
		where	nr_seq_conta_pos_estab	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_seq_conta_proc	= nr_seq_conta_proc_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_proc b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
		
		delete	pls_hist_analise_conta
		where	nr_seq_conta_proc in (	select	a.nr_sequencia
						from	pls_conta_proc	a
						where	a.ie_status	= 'M'
						and	a.nr_seq_regra_pct_fat is not null
						and	a.nr_sequencia	= nr_seq_conta_proc_p);
		
		delete	pls_analise_fluxo_ocor
		where	nr_seq_fluxo_item in (	select	a.nr_sequencia
						from	pls_analise_fluxo_item a,
							pls_conta_proc b
						where	a.nr_seq_conta_proc = b.nr_sequencia
						and	b.ie_status = 'M'
						and	b.nr_seq_regra_pct_fat is not null
						and	b.nr_sequencia = nr_seq_conta_proc_p);
		
		delete	pls_analise_fluxo_item
		where	nr_seq_conta_proc in (	select	a.nr_sequencia
						from	pls_conta_proc	a
						where	a.ie_status	= 'M'
						and	a.nr_seq_regra_pct_fat is not null
						and	a.nr_sequencia	= nr_seq_conta_proc_p);
		
		delete 	pls_conta_proc	a
		where	a.ie_status	= 'M'
		and	a.nr_seq_regra_pct_fat is not null
		and	a.nr_sequencia	= nr_seq_conta_proc_p
		and	not exists (	select	1
					from	pls_conta_pos_estabelecido	b
					where	b.nr_seq_conta_proc		= a.nr_sequencia
					and	b.ie_status_faturamento		= 'A'
					and	b.ie_cobrar_mensalidade		= 'A')
		and not exists (	select	1
							from	pls_conta_pos_estab_prev	b
							where	b.nr_seq_conta_proc		= a.nr_sequencia);
		
		delete	pls_conta_pos_estab_aprop
		where	nr_seq_conta_pos_estab 	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_seq_conta_proc	= nr_seq_conta_proc_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_proc b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
							
		delete	pls_conta_pos_estabelecido	a
		where	nr_seq_conta_proc	= nr_seq_conta_proc_p
		and	a.ie_status_faturamento	!= 'A'
		and	a.ie_cobrar_mensalidade	!= 'A'
		and	not exists	(select	1
					from	pls_fatura_proc b
					where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);

		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_seq_conta_proc	= nr_seq_conta_proc_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	else
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_seq_conta_proc	= nr_seq_conta_proc_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	end if;	
elsif	(nr_seq_conta_mat_p	is not null) then
	select	count(1)
	into	qt_registros_w
	from	pls_conta_pos_estabelecido	a
	where	nr_seq_conta_mat	= nr_seq_conta_mat_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_mat b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);
	
	select	max(nr_seq_mensalidade_seg),
		max(nr_seq_regra_limite_mens)
	into	nr_seq_mensalidade_seg_w,
		nr_seq_regra_limite_mens_w
	from	pls_conta_pos_estabelecido	a
	where	nr_seq_conta_mat = nr_seq_conta_mat_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_mat b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);

	if	((nr_seq_mensalidade_seg_w is null) or (nr_seq_regra_limite_mens_w is null)) then
		select  count(1)
		into 	qt_registros_mensalidade_w
		from  	pls_mensalidade_item_conta a,
			pls_conta_pos_estabelecido e
		where 	a.nr_seq_conta_pos_estab = e.nr_sequencia
		and	e.ie_status_faturamento	!= 'A'
		and	e.ie_cobrar_mensalidade	!= 'A'
		and	e.nr_seq_conta_mat 	= nr_seq_conta_mat_p;
	end if;
	
	if	(qt_registros_w > 0) 	and
		(nr_seq_mensalidade_seg_w is null) and
		(nr_seq_regra_limite_mens_w is null) and 
		(qt_registros_mensalidade_w = 0 ) then
		
		delete	pls_ocorrencia_benef
		where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta_mat	= nr_seq_conta_mat_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
							
		delete	pls_ocorrencia_benef
		where	nr_seq_conta_mat in (	select	a.nr_sequencia          
						from	pls_conta_mat		a
						where	a.ie_status		= 'M'
						and	a.nr_seq_regra_pct_fat	is not null
						and	a.nr_sequencia		= nr_seq_conta_mat_p);
							
		delete	pls_conta_glosa
		where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta_mat	= nr_seq_conta_mat_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
		
		-- Tenta apagar tamb?m as glosas geradas diretamente pelos itens de pacote, necess?rio para os casos onde n?o tem p?s-estabelecido
		delete	pls_conta_glosa x
		where	x.nr_seq_conta_mat	 in (	select	a.nr_sequencia          
							from	pls_conta_mat		a
							where	a.ie_status		= 'M'
							and	a.nr_seq_regra_pct_fat	is not null
							and	a.nr_sequencia		= nr_seq_conta_mat_p);
							
							
		delete	pls_lib_fat_conta_pos a
		where	a.nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta_mat	= nr_seq_conta_mat_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
		delete  pls_conta_pos_estab_taxa
		where	nr_seq_conta_pos_estab	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_seq_conta_mat	= nr_seq_conta_mat_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_mat b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
		
		delete	pls_conta_pos_estab_aprop
		where	nr_seq_conta_pos_estab 	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_seq_conta_mat	= nr_seq_conta_mat_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_mat b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
		
		delete	pls_hist_analise_conta
		where	nr_seq_conta_mat in (	select	a.nr_sequencia
						from	pls_conta_mat	a
						where	a.ie_status	= 'M'
						and	a.nr_seq_regra_pct_fat is not null
						and	a.nr_sequencia	= nr_seq_conta_mat_p);
		
		delete	pls_analise_fluxo_ocor
		where	nr_seq_fluxo_item in (	select	a.nr_sequencia
						from	pls_analise_fluxo_item a,
							pls_conta_mat b
						where	a.nr_seq_conta_mat = b.nr_sequencia
						and	b.ie_status = 'M'
						and	b.nr_seq_regra_pct_fat is not null
						and	b.nr_sequencia = nr_seq_conta_mat_p);
		
		delete	pls_analise_fluxo_item
		where	nr_seq_conta_mat in (	select	a.nr_sequencia
						from	pls_conta_mat	a
						where	a.ie_status	= 'M'
						and	a.nr_seq_regra_pct_fat is not null
						and	a.nr_sequencia	= nr_seq_conta_mat_p);
		
		delete 	pls_conta_mat	a
		where	a.ie_status	= 'M'
		and	a.nr_seq_regra_pct_fat is not null
		and	a.nr_sequencia	= nr_seq_conta_mat_p
		and	not exists (	select	1
					from	pls_conta_pos_estabelecido	b
					where	b.nr_seq_conta_mat		= a.nr_sequencia
					and	b.ie_status_faturamento		= 'A'
					and	b.ie_cobrar_mensalidade		= 'A')
		and not exists (select 1
						from 	pls_conta_pos_estab_prev b
						where	b.nr_seq_conta_mat		= a.nr_sequencia);
		
		delete	pls_conta_pos_estabelecido	a
		where	nr_seq_conta_mat	= nr_seq_conta_mat_p
		and	a.ie_status_faturamento	!= 'A'
		and	a.ie_cobrar_mensalidade	!= 'A'
		and	not exists	(select	1
					from	pls_fatura_mat b
					where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);
					
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_seq_conta_mat	= nr_seq_conta_mat_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	else
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_seq_conta_mat	= nr_seq_conta_mat_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	end if;
elsif	(nr_seq_conta_p		is not null) then	
	select	count(1)
	into	qt_registros_w
	from	pls_conta_pos_estabelecido	a
	where	nr_seq_conta	= nr_seq_conta_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_mat b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
	and	not exists	(select	1
				from	pls_fatura_proc b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);

	select	max(nr_seq_mensalidade_seg),
		max(nr_seq_regra_limite_mens)
	into	nr_seq_mensalidade_seg_w,
		nr_seq_regra_limite_mens_w
	from	pls_conta_pos_estabelecido	a
	where	nr_seq_conta	= nr_seq_conta_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_mat b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
	and	not exists	(select	1
				from	pls_fatura_proc b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);
		
	if	((nr_seq_mensalidade_seg_w is null) or (nr_seq_regra_limite_mens_w is null)) then
		select  count(1)
		into 	qt_registros_mensalidade_w
		from  	pls_mensalidade_seg_item a
		where 	a.nr_seq_conta = nr_seq_conta_p;
	end if;
		
	if	(qt_registros_w > 0) 	and
		(nr_seq_mensalidade_seg_w is null) and
		(nr_seq_regra_limite_mens_w is null) and 
		(qt_registros_mensalidade_w = 0 ) then

		
		delete  pls_analise_glo_ocor_grupo
		where 	nr_seq_ocor_benef in (select	nr_sequencia
						from 	pls_ocorrencia_benef
						where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
											from	pls_conta_pos_estabelecido	b
											where	nr_seq_conta	= nr_seq_conta_p
											and	b.ie_status_faturamento	!= 'A'
											and	b.ie_cobrar_mensalidade	!= 'A'));
											
		delete  pls_analise_glo_ocor_grupo
		where 	nr_seq_ocor_benef in (	select	nr_sequencia
						from 	pls_ocorrencia_benef
						where	nr_seq_conta_proc in (	select	a.nr_sequencia          
										from	pls_conta_proc		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));

		delete  pls_analise_glo_ocor_grupo
		where 	nr_seq_ocor_benef in (	select	nr_sequencia
						from 	pls_ocorrencia_benef
						where	nr_seq_conta_mat in (	select	a.nr_sequencia          
										from	pls_conta_mat		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));
														
														
		delete pls_analise_fluxo_ocor
		where 	nr_seq_ocor_benef in (	select	nr_sequencia
						from 	pls_ocorrencia_benef
						where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
											from	pls_conta_pos_estabelecido	b
											where	nr_seq_conta	= nr_seq_conta_p
											and	b.ie_status_faturamento	!= 'A'
											and	b.ie_cobrar_mensalidade	!= 'A'));

		delete pls_analise_fluxo_ocor
		where 	nr_seq_ocor_benef in (	select	nr_sequencia
						from 	pls_ocorrencia_benef
						where	nr_seq_conta_proc in (	select	a.nr_sequencia          
										from	pls_conta_proc		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));

		delete pls_analise_fluxo_ocor
		where 	nr_seq_ocor_benef in (	select	nr_sequencia
						from 	pls_ocorrencia_benef
						where	nr_seq_conta_mat in (	select	a.nr_sequencia          
										from	pls_conta_mat		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));


		delete	pls_ocorrencia_benef
		where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta	= nr_seq_conta_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
		delete	pls_ocorrencia_benef
		where	nr_seq_conta_proc in (	select	a.nr_sequencia          
						from	pls_conta_proc		a
						where	a.ie_status		= 'M'
						and	a.nr_seq_regra_pct_fat	is not null
						and	a.nr_seq_conta		= nr_seq_conta_p);
									
		delete	pls_ocorrencia_benef
		where	nr_seq_conta_mat in (	select	a.nr_sequencia          
						from	pls_conta_mat		a
						where	a.ie_status		= 'M'
						and	a.nr_seq_regra_pct_fat	is not null
						and	a.nr_seq_conta		= nr_seq_conta_p);
		
		delete pls_analise_fluxo_ocor
		where 	nr_seq_conta_glosa in (	select	nr_sequencia
						from 	pls_conta_glosa
						where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
											from	pls_conta_pos_estabelecido	b
											where	nr_seq_conta	= nr_seq_conta_p
											and	b.ie_status_faturamento	!= 'A'
											and	b.ie_cobrar_mensalidade	!= 'A'));
											
		delete pls_analise_fluxo_ocor
		where 	nr_seq_conta_glosa in (	select	nr_sequencia
						from 	pls_conta_glosa
						where	nr_seq_conta_proc in  (	select	a.nr_sequencia          
										from	pls_conta_proc		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));

		delete pls_analise_fluxo_ocor
		where 	nr_seq_conta_glosa in (	select	nr_sequencia
						from 	pls_conta_glosa
						where	nr_seq_conta_mat in  (	select	a.nr_sequencia          
										from	pls_conta_mat		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));
											
		delete  pls_analise_glo_ocor_grupo
		where 	nr_seq_conta_glosa in (	select	nr_sequencia
						from 	pls_conta_glosa
						where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
											from	pls_conta_pos_estabelecido	b
											where	nr_seq_conta	= nr_seq_conta_p
											and	b.ie_status_faturamento	!= 'A'
											and	b.ie_cobrar_mensalidade	!= 'A'));
											
		delete  pls_analise_glo_ocor_grupo
		where 	nr_seq_conta_glosa in (	select	nr_sequencia
						from 	pls_conta_glosa
						where	nr_seq_conta_proc in (	select	a.nr_sequencia          
										from	pls_conta_proc		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));

		delete  pls_analise_glo_ocor_grupo
		where 	nr_seq_conta_glosa in (	select	nr_sequencia
						from 	pls_conta_glosa
						where	nr_seq_conta_mat in (	select	a.nr_sequencia          
										from	pls_conta_mat		a
										where	a.ie_status		= 'M'
										and	a.nr_seq_regra_pct_fat	is not null
										and	a.nr_seq_conta		= nr_seq_conta_p));
											
		delete	pls_conta_glosa
		where	nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta	= nr_seq_conta_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
							
		-- Tenta apagar tamb?m as glosas geradas diretamente pelos itens de pacote, necess?rio para os casos onde n?o tem p?s-estabelecido
		delete	pls_conta_glosa x
		where	x.nr_seq_conta_proc in (	select	a.nr_sequencia          
							from	pls_conta_proc		a
							where	a.ie_status		= 'M'
							and	a.nr_seq_regra_pct_fat	is not null
							and	a.nr_seq_conta		= nr_seq_conta_p);
		-- Tenta apagar tamb?m as glosas geradas diretamente pelos itens de pacote, necess?rio para os casos onde n?o tem p?s-estabelecido
		delete	pls_conta_glosa x
		where	x.nr_seq_conta_mat	 in (	select	a.nr_sequencia          
							from	pls_conta_mat		a
							where	a.ie_status		= 'M'
							and	a.nr_seq_regra_pct_fat	is not null
							and	a.nr_seq_conta		= nr_seq_conta_p
							and	not exists (	select	1
										from	pls_conta_pos_estabelecido	b
										where	b.nr_seq_conta_mat		= a.nr_sequencia
										and	b.ie_status_faturamento		= 'A'
										and	b.ie_cobrar_mensalidade		= 'A'));
							
		delete	pls_lib_fat_conta_pos a
		where	a.nr_seq_conta_pos_estab in (	select 	b.nr_sequencia
							from	pls_conta_pos_estabelecido	b
							where	nr_seq_conta	= nr_seq_conta_p
							and	b.ie_status_faturamento	!= 'A'
							and	b.ie_cobrar_mensalidade	!= 'A');
		
		delete  pls_conta_pos_estab_taxa
		where	nr_seq_conta_pos_estab	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_seq_conta	= nr_seq_conta_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_mat b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
							and	not exists	(select	1
										from	pls_fatura_proc b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
										
		delete	pls_conta_pos_estab_aprop
		where	nr_seq_conta_pos_estab 	in(	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_seq_conta	= nr_seq_conta_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_mat b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
							and	not exists	(select	1
										from	pls_fatura_proc b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
		
		delete	pls_conta_pos_estabelecido	a
		where	nr_seq_conta	= nr_seq_conta_p
		and	a.ie_status_faturamento	!= 'A'
		and	a.ie_cobrar_mensalidade	!= 'A'
		and	not exists	(select	1
					from	pls_fatura_mat b
					where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
		and	not exists	(select	1
					from	pls_fatura_proc b
					where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
		and	nr_seq_regra_tx_opme is null;
		
		delete	pls_hist_analise_conta
		where	nr_seq_conta_proc in (	select	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_regra_pct_fat is not null
						and	nr_seq_conta	= nr_seq_conta_p);
		
		delete	pls_analise_fluxo_ocor
		where	nr_seq_fluxo_item in (	select	a.nr_sequencia
						from	pls_analise_fluxo_item a,
							pls_conta_proc b
						where	a.nr_seq_conta_proc = b.nr_sequencia
						and	b.ie_status = 'M'
						and	b.nr_seq_regra_pct_fat is not null
						and	b.nr_seq_conta = nr_seq_conta_p);
		
		delete	pls_analise_fluxo_item
		where	nr_seq_conta_proc in (	select	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_regra_pct_fat is not null
						and	nr_seq_conta	= nr_seq_conta_p);
		
		delete 	pls_conta_proc		a
		where	a.ie_status		= 'M'
		and	a.nr_seq_regra_pct_fat	is not null
		and	a.nr_seq_conta		= nr_seq_conta_p
		and	not exists	 (	select	1
						from	pls_conta_pos_estabelecido	b
						where	b.nr_seq_conta_proc	= a.nr_sequencia
						and	b.ie_status_faturamento	= 'A'
						and	b.ie_cobrar_mensalidade	= 'A')
		and 	not exists(	select 	1 
						from 	pls_conta_pos_estab_prev 
						where 	nr_seq_conta_proc = a.nr_sequencia);
		
		delete	pls_hist_analise_conta
		where	nr_seq_conta_mat in (	select	nr_sequencia
						from	pls_conta_mat
						where	ie_status	= 'M'
						and	nr_seq_regra_pct_fat is not null
						and	nr_seq_conta	= nr_seq_conta_p);
		
		delete	pls_analise_fluxo_ocor
		where	nr_seq_fluxo_item in (	select	a.nr_sequencia
						from	pls_analise_fluxo_item a,
							pls_conta_mat b
						where	a.nr_seq_conta_mat = b.nr_sequencia
						and	b.ie_status = 'M'
						and	b.nr_seq_regra_pct_fat is not null
						and	b.nr_seq_conta = nr_seq_conta_p);
		
		delete	pls_analise_fluxo_item
		where	nr_seq_conta_mat in (	select	nr_sequencia
						from	pls_conta_mat
						where	ie_status	= 'M'
						and	nr_seq_regra_pct_fat is not null
						and	nr_seq_conta	= nr_seq_conta_p);
		
		delete 	pls_conta_mat		a
		where	a.ie_status		= 'M'
		and	a.nr_seq_regra_pct_fat	is not null
		and	a.nr_seq_conta		= nr_seq_conta_p
		and	not exists	 (	select	1
						from	pls_conta_pos_estabelecido	b
						where	b.nr_seq_conta_mat	= a.nr_sequencia
						and	b.ie_status_faturamento	= 'A'
						and	b.ie_cobrar_mensalidade	= 'A')
		and 	not exists(	select 	1 
						from 	pls_conta_pos_estab_prev 
						where 	nr_seq_conta_mat = a.nr_sequencia);
		
		--Excluir os registros de p?s-estabelecido de regra de taxa de OPME
		for r_c01_w in C01 loop
	
			delete	pls_conta_pos_estabelecido
			where	nr_seq_conta_proc	= r_c01_w.nr_seq_conta_proc
			and	ie_status_faturamento	!= 'A'
			and	ie_cobrar_mensalidade	!= 'A';
			
			
			delete	pls_hist_analise_conta
			where	nr_seq_conta_proc = r_c01_w.nr_seq_conta_proc;
			
			delete	pls_analise_fluxo_ocor
			where	nr_seq_fluxo_item in (	select	a.nr_sequencia
							from	pls_analise_fluxo_item a,
								pls_conta_proc b
							where	a.nr_seq_conta_proc = b.nr_sequencia
							and	b.nr_sequencia = r_c01_w.nr_seq_conta_proc);
			
			delete	pls_analise_fluxo_item
			where	nr_seq_conta_proc 	= r_c01_w.nr_seq_conta_proc;
			
			delete	pls_conta_proc a
			where	a.nr_sequencia		= r_c01_w.nr_seq_conta_proc
			and 	not exists(	select 	1 
						from 	pls_conta_pos_estab_prev 
						where 	nr_seq_conta_proc = a.nr_sequencia);	
		end loop;
		
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_seq_conta	= nr_seq_conta_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	else
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_seq_conta	= nr_seq_conta_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	end if;	
elsif	(nr_seq_pos_estab_p	is not null) then
	select	max(nr_seq_mensalidade_seg),
		max(nr_seq_regra_limite_mens)
	into	nr_seq_mensalidade_seg_w,
		nr_seq_regra_limite_mens_w
	from	pls_conta_pos_estabelecido	a
	where	nr_sequencia	= nr_seq_pos_estab_p
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	and	not exists	(select	1
				from	pls_fatura_mat b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
	and	not exists	(select	1
				from	pls_fatura_proc b
				where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);
	
	if	((nr_seq_mensalidade_seg_w is null) or (nr_seq_regra_limite_mens_w is null)) then
		select  count(1)
		into 	qt_registros_mensalidade_w
		from  	pls_mensalidade_item_conta a,
			pls_conta_pos_estabelecido e
		where 	a.nr_seq_conta_pos_estab = e.nr_sequencia
		and	e.ie_status_faturamento	!= 'A'
		and	e.ie_cobrar_mensalidade	!= 'A'
		and	e.nr_sequencia = nr_seq_pos_estab_p;
	end if;

	if	(qt_registros_w > 0) 	and
		(nr_seq_mensalidade_seg_w is null) and
		(nr_seq_regra_limite_mens_w is null) and 
		(qt_registros_mensalidade_w = 0 ) then

		delete	pls_ocorrencia_benef
		where	nr_seq_conta_pos_estab = nr_seq_pos_estab_p;
							
		delete	pls_conta_glosa
		where	nr_seq_conta_pos_estab = nr_seq_pos_estab_p;
							
		delete	pls_lib_fat_conta_pos a
		where	a.nr_seq_conta_pos_estab = nr_seq_pos_estab_p;
		
		delete  pls_conta_pos_estab_taxa
		where	nr_seq_conta_pos_estab	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_sequencia	= nr_seq_pos_estab_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_mat b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
							and	not exists	(select	1
										from	pls_fatura_proc b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
		
		delete	pls_conta_pos_estab_aprop
		where	nr_seq_conta_pos_estab 	in (	select 	nr_sequencia
							from  	pls_conta_pos_estabelecido	a
							where	nr_sequencia	= nr_seq_pos_estab_p
							and	a.ie_status_faturamento	!= 'A'
							and	a.ie_cobrar_mensalidade	!= 'A'
							and	not exists	(select	1
										from	pls_fatura_mat b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
							and	not exists	(select	1
										from	pls_fatura_proc b
										where	a.nr_sequencia	= b.nr_seq_conta_pos_estab));
		
		delete	pls_conta_pos_estabelecido	a
		where	nr_sequencia	= nr_seq_pos_estab_p
		and	a.ie_status_faturamento	!= 'A'
		and	a.ie_cobrar_mensalidade	!= 'A'
		and	not exists	(select	1
					from	pls_fatura_mat b
					where	a.nr_sequencia	= b.nr_seq_conta_pos_estab)
		and	not exists	(select	1
					from	pls_fatura_proc b
					where	a.nr_sequencia	= b.nr_seq_conta_pos_estab);
					
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_sequencia	= nr_seq_pos_estab_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	else
		update	pls_conta_pos_estabelecido
		set	ie_situacao	= 'I'
		where	nr_sequencia	= nr_seq_pos_estab_p
		and	ie_status_faturamento	!= 'A'
		and	ie_cobrar_mensalidade	!= 'A';
	end if;
end if;

end pls_delete_pls_conta_pos_estab;
/
