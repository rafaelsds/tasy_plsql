create or replace
procedure Alterar_valor_rubrica(	nr_seq_demo_p		number,
					nr_seq_rubrica_p		number,
					nr_seq_col_p		number,
					vl_referencia_p		number,
					nm_usuario_p		Varchar2) is 

begin

UPDATE 	ctb_demo_rubrica 
SET 	vl_referencia 	= vl_referencia_p 
WHERE 	nr_seq_rubrica 	= nr_seq_rubrica_p
and 	nr_seq_demo	= nr_seq_demo_p
and 	nr_seq_col	= nr_seq_col_p;

commit;

end Alterar_valor_rubrica;
/