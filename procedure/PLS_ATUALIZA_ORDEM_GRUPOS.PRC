create or replace
procedure pls_atualiza_ordem_grupos
			(	nr_sequencia_p		number,
				nr_seq_ordem_p		number,
				nr_seq_analise_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2	) is 

nr_sequencia_w		Number(10);
nr_seq_ordem_w		Number(10);	
				
Cursor C01 is
	select	nr_sequencia,
		nr_seq_ordem
	from	pls_auditoria_conta_grupo
	where	nr_seq_analise	= nr_seq_analise_p
	and	nr_seq_ordem	= nr_seq_ordem_p
	and	nr_sequencia	<> nr_sequencia_p; --Muito importante para n�o entrar em loop <<<< N�O REMOVER <<< N�O ACRESCENTAR CLAUSALA "OR"
				
begin

update	pls_auditoria_conta_grupo
set	nr_seq_ordem	= nr_seq_ordem_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia   = nr_sequencia_p;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nr_seq_ordem_w;
exit when C01%notfound;
	begin
	
	pls_atualiza_ordem_grupos(nr_sequencia_w, nr_seq_ordem_w + 1, nr_seq_analise_p,
				  cd_estabelecimento_p, nm_usuario_p);
	
	end;
end loop;
close C01;

end pls_atualiza_ordem_grupos;
/