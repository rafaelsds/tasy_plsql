create or replace
procedure aceitar_divergencia_inspecao(	nr_sequencia_p		number,
					cd_pessoa_resp_p		varchar2,
					ds_justificativa_p		varchar2,
					nm_usuario_p		varchar2) is 

begin

update	inspecao_divergencia
set	cd_pessoa_resp_justif = cd_pessoa_resp_p,
	ie_motivo_aceite = 'A',
	ds_justificativa = substr(ds_justificativa_p,1,255),
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_sequencia_p;


commit;

end aceitar_divergencia_inspecao;
/