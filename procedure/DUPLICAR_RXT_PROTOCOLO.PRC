CREATE OR REPLACE
PROCEDURE duplicar_rxt_protocolo(   nr_sequencia_p		number,
                                    nm_usuario_p		varchar2,
                                    nr_seq_prot_p	out number) is
				
nr_seq_protocolo_w  number(10);
nr_seq_w            number(10);
nr_seq_campo_w      number(10);
nr_seq_volume_protocolo_w number(10);

Cursor C01 is
	select	nr_sequencia			
	from 	rxt_campo_prot_roentgen
	where   nr_seq_protocolo = nr_sequencia_p;

Cursor C02 is
	select	nr_sequencia			
	from 	rxt_fase_protocolo
	where   nr_seq_protocolo = nr_sequencia_p;
	
Cursor C03 is
	select	nr_sequencia			
	from rxt_campo_protocolo
	where nr_seq_protocolo = nr_sequencia_p;
	
Cursor C04 is
	select	nr_sequencia			
	from rxt_braq_aplic_prot
	where nr_seq_protocolo = nr_sequencia_p;
	
BEGIN
	if (nr_sequencia_p is not null) then
		
		select rxt_protocolo_seq.nextval
		into nr_seq_protocolo_w
		from dual;
		
		insert into rxt_protocolo(
			nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			nm_protocolo,
			nr_seq_tipo,
			nr_seq_equipamento,
			ie_situacao,
			qt_dose_total,
			qt_check_film,
			ie_frequencia
			)
		select	nr_seq_protocolo_w,
			sysdate,
			nm_usuario_p,
			dt_atualizacao,
			nm_usuario_p,
			substr(wheb_mensagem_pck.get_texto(285929) || nm_protocolo,1,80),
			nr_seq_tipo,
			nr_seq_equipamento,
			ie_situacao,
			qt_dose_total,
			qt_check_film,
			ie_frequencia
		from	rxt_protocolo
		where	nr_sequencia = nr_sequencia_p;	
		
		insert into rxt_protocolo_acessorio(nr_sequencia,
			nr_seq_protocolo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_acessorio,
			nr_seq_campo)
		select rxt_protocolo_acessorio_seq.nextval,
			nr_seq_protocolo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_acessorio,
			nr_seq_campo
		from rxt_protocolo_acessorio
		where nr_seq_protocolo = nr_sequencia_p;

		select rxt_volume_protocolo_seq.nextval
 		into nr_seq_volume_protocolo_w
		from dual;
		
		insert into rxt_volume_protocolo(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			nr_seq_volume,
			ds_volume)
		select nr_seq_volume_protocolo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_w,
			nr_seq_volume,
			ds_volume
		from rxt_volume_protocolo
		where nr_seq_protocolo = nr_sequencia_p;

		open C01;
		loop
		fetch C01 into	
			nr_seq_campo_w;
		exit when C01%notfound;
                        begin		
			select rxt_campo_prot_roentgen_seq.nextval
			into nr_seq_w
			from dual;
			
			insert into rxt_campo_prot_roentgen(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_campo,
				qt_dia_trat,
				qt_dose_campo,
				qt_dose_dia,
				ds_protecao,
				nr_seq_protocolo,
				nr_seq_volume_protocolo)
			select nr_seq_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nm_campo,
				qt_dia_trat,
				qt_dose_campo,
				qt_dose_dia,
				ds_protecao,
				nr_seq_protocolo_w,
				nr_seq_volume_protocolo_w
			from rxt_campo_prot_roentgen
			where nr_sequencia = nr_seq_campo_w;
			
			
			insert into rxt_aplic_campo_prot_roent(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_kvp,
				nr_seq_ma,
				nr_seq_aplicador,
				ds_filtro_kvp,
				ds_info_adic_kvp,
				ds_info_adic_ma,
				nr_seq_campo_prot,
				nr_minuto_duracao,
				qt_tamanho_x,
				qt_tamanho_y,
				nr_segundo_duracao)
			select rxt_aplic_campo_prot_roent_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_kvp,
				nr_seq_ma,
				nr_seq_aplicador,
				ds_filtro_kvp,
				ds_info_adic_kvp,
				ds_info_adic_ma,
				nr_seq_w,
				nr_minuto_duracao,
				qt_tamanho_x,
				qt_tamanho_y,
				nr_segundo_duracao
			from rxt_aplic_campo_prot_roent
			where nr_seq_campo_prot = nr_seq_campo_w;
			
			insert into rxt_imagem_prot_roent(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_titulo,
				ds_arquivo,
				nr_seq_campo_prot)
			select rxt_imagem_prot_roent_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_titulo,
				ds_arquivo,
				nr_seq_w
			from rxt_imagem_prot_roent
			where nr_seq_campo_prot = nr_seq_campo_w;
			
			end;
		end loop;
		close C01;

		open C04;
		loop
		fetch C04 into	
			nr_seq_campo_w;
		exit when C04%notfound;
                        begin
			select rxt_braq_aplic_prot_seq.nextval
			into nr_seq_w
			from dual;
			
			insert into rxt_braq_aplic_prot(nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_situacao,
				nr_seq_protocolo,
				nr_seq_aplicador,
				nr_insercao,
				qt_dose_total,
				qt_dose_insercao,
				qt_insercao_semana,				
				qt_intervalo_insercoes,
				nr_ordem_execucao_aplic)
			select nr_seq_w,
				cd_estabelecimento,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ie_situacao,
				nr_seq_protocolo_w,
				nr_seq_aplicador,
				nr_insercao,
				qt_dose_total,
				qt_dose_insercao,
				qt_insercao_semana,
				qt_intervalo_insercoes,
				nr_ordem_execucao_aplic
			from rxt_braq_aplic_prot
			where nr_sequencia = nr_seq_campo_w;
			
			insert into rxt_braq_campo_aplic_prot(nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_aplic_prot,
				nr_insercao,
				ds_resultado,
				qt_resultado,
				nr_seq_campo,
				ie_situacao)
			select rxt_braq_campo_aplic_prot_seq.nextval,
				cd_estabelecimento,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_w,
				nr_insercao,
				ds_resultado,
				qt_resultado,
				nr_seq_campo,
				ie_situacao	
			from rxt_braq_campo_aplic_prot
			where nr_seq_aplic_prot = nr_seq_campo_w; 		
			end;
			end loop;
			close C04;
			
			open C03;
			loop
			fetch C03 into	
				nr_seq_campo_w;
			exit when C03%notfound;
			begin
				
				select rxt_campo_protocolo_seq.nextval
				into nr_seq_w
				from dual;
				
				insert into rxt_campo_protocolo(nr_sequencia,
					nr_seq_protocolo,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_campo,
					ie_situacao,
					nr_seq_apresent)
				select nr_seq_w,
					nr_seq_protocolo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ds_campo,
					ie_situacao,
					nr_seq_apresent
				from rxt_campo_protocolo
				where nr_sequencia = nr_seq_campo_w;

				insert into rxt_protocolo_acessorio(nr_sequencia,
					nr_seq_protocolo,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_acessorio,
					nr_seq_campo)
				select rxt_protocolo_acessorio_seq.nextval,
					nr_seq_protocolo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_acessorio,
					nr_seq_w
				from rxt_protocolo_acessorio
				where nr_seq_protocolo = nr_sequencia_p
				and nr_seq_campo = nr_seq_campo_w;
				
				
			end;
			end loop;
			close C03;

			open C02;
			loop
			fetch C02 into	
				nr_seq_campo_w;
			exit when C02%notfound;
				begin
				
				select rxt_fase_protocolo_seq.nextval
				into nr_seq_w
				from dual;	
				
				insert into rxt_fase_protocolo(nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_protocolo,
					nm_fase,
					qt_dose_fase,
					ie_final_semana,
					qt_dia_trat,
					qt_dose_dia,
					nr_seq_decubito,
					nr_seq_volume_protocolo,
					qt_intervalo)
				select nr_seq_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_protocolo_w,
					nm_fase,
					qt_dose_fase,
					ie_final_semana,
					qt_dia_trat,
					qt_dose_dia,
					nr_seq_decubito,
					nr_seq_volume_protocolo_w,
					qt_intervalo
				from rxt_fase_protocolo
				where nr_sequencia = nr_seq_campo_w;

				insert into rxt_campo_fase_prot(nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_fase,
					nr_seq_apres,
					nr_seq_campo,
					qt_dose_total,
					qt_dose_fracao)
				select rxt_campo_fase_prot_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_w,
					nr_seq_apres,
					nr_seq_campo,
					qt_dose_total,
					qt_dose_fracao
				from rxt_campo_fase_prot
				where nr_seq_fase = nr_seq_campo_w;
				
				insert into rxt_protocolo_fase_imagem(nr_sequencia,
					nr_seq_fase,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_arquivo,
					ds_titulo)
				select rxt_protocolo_fase_imagem_seq.nextval,
					nr_seq_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ds_arquivo,
					ds_titulo
				from rxt_protocolo_fase_imagem
				where nr_seq_fase = nr_seq_campo_w;
				
				end;
			end loop;
			close C02;

			insert into rxt_protocolo_proc_exec(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_proc_interno,
        nr_seq_protocolo,
        cd_convenio,
        nr_seq_modalidade)
			select rxt_protocolo_proc_exec_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_proc_interno,
        nr_seq_protocolo_w,
        cd_convenio,
        nr_seq_modalidade
			from rxt_protocolo_proc_exec
			where nr_seq_protocolo = nr_sequencia_p;
			

        end if;

nr_seq_prot_p := nr_seq_protocolo_w;
commit;


END duplicar_rxt_protocolo;
/
