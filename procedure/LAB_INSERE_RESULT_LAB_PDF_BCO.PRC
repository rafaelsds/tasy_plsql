create or replace
procedure Lab_insere_result_lab_pdf_bco( nr_prescricao_p	varchar2,
				     nr_seq_prescr_p	varchar2,
				     ds_pdf_serial_p	long,	
				     nm_usuario_p	Varchar2,
				     ds_erro_p		out varchar2 ) is 

nr_seq_result_lab_w	number(10);				
nr_seq_exame_w		number(10);
ie_formato_texto_w	number(2);

begin

select 	MAX(nr_sequencia),
		MAX(nvl(ie_formato_texto,1))
into 	nr_seq_result_lab_w,
		ie_formato_texto_w
from 	result_laboratorio
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_prescricao = nr_seq_prescr_p;

select 	MAX(nr_seq_exame)
into	nr_seq_exame_w
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia = nr_seq_prescr_p;

if 	(nr_seq_result_lab_w is null) then
	insert into result_laboratorio (
		nr_sequencia,
		ds_resultado,
		nr_prescricao,
		nr_seq_prescricao,
		nm_usuario,
		dt_atualizacao,
		ie_formato_texto,
		ie_cobranca)
	values (result_laboratorio_seq.nextval,
		WHEB_MENSAGEM_PCK.get_texto(280689,null),
		nr_prescricao_p,
		nr_seq_prescr_p,
		nm_usuario_p,
		sysdate,
		3,
		'N');

end if;

select 	MAX(nr_sequencia),
		MAX(nvl(ie_formato_texto,1))
into 	nr_seq_result_lab_w,
		ie_formato_texto_w
from 	result_laboratorio
where	nr_prescricao	= nr_prescricao_p
and		nr_seq_prescricao = nr_seq_prescr_p;


if	(nr_seq_result_lab_w is not null) then
	begin
	
	if (ie_formato_texto_w <> 3) then
		update 	result_laboratorio
		set 	ie_formato_texto = 3,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
		where 	nr_prescricao = nr_prescricao_p
		and		nr_seq_prescricao = nr_seq_prescr_p;
		
		commit;
	
	end if;
	
	insert into result_laboratorio_pdf (	nr_sequencia,
						nr_prescricao, 
						dt_atualizacao, 
						nm_usuario, 
						dt_atualizacao_nrec, 
						nm_usuario_nrec, 
						nr_seq_prescricao, 
						nr_seq_resultado, 
						nr_seq_exame, 
						ds_pdf_serial		
						)
	values				   (	result_laboratorio_pdf_seq.nextval,
						nr_prescricao_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_prescr_p,
						nr_seq_result_lab_w,
						nr_seq_exame_w,
						ds_pdf_serial_p
						);
		
	exception
		when others then
			ds_erro_p := SQLERRM(SQLCode);
		end;

end if;

commit;

end Lab_insere_result_lab_pdf_bco;
/
