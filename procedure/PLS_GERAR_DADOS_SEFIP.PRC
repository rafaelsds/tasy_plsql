create or replace
procedure pls_gerar_dados_sefip( nr_seq_lote_p		number,
				nm_usuario_p		varchar2) is 

ie_forma_geracao_w	sefip_lote.ie_forma_geracao%type;
				
begin
select	nvl(max(ie_forma_geracao),'SA')
into	ie_forma_geracao_w
from	sefip_lote
where	nr_sequencia	= nr_seq_lote_p;

if	(ie_forma_geracao_w = 'MA') then
	pls_gerar_lote_sefip_ma(nr_seq_lote_p, nm_usuario_p);
elsif	(ie_forma_geracao_w = 'PA') then
	pls_gerar_lote_sefip_pa(nr_seq_lote_p, nm_usuario_p);
elsif	(ie_forma_geracao_w = 'SA') then
	pls_gerar_lote_sefip_sa(nr_seq_lote_p, nm_usuario_p);
end if;

commit;

end pls_gerar_dados_sefip;
/
