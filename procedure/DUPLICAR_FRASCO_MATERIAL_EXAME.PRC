create or replace
procedure duplicar_frasco_material_exame (
		nr_seq_exame_p		number,
		nm_usuario_p		varchar2,
		nr_seq_frasco_p		number,
		nr_seq_material_p	number,
		ds_frasco_inadequado_p	varchar2,
		ds_transporte_p		varchar2,
		nr_sequencia_p		out number) is
		
nr_sequencia_w	number(10,0) := 0;
		
begin

select	exame_lab_frasco_seq.nextval
into	nr_sequencia_w
from 	dual;


if	(nr_seq_exame_p is not null) and
	(nm_usuario_p is not null) and
	(nr_seq_material_p is not null) and
	(nr_sequencia_w > 0) then
	begin
	insert into exame_lab_frasco(	nr_sequencia,
					nr_seq_exame,
					nm_usuario,
					dt_atualizacao,
					nr_seq_frasco,
					nr_seq_material,
					ds_frasco_inadequado,
					ds_transporte,
					ie_situacao)

				values(	nr_sequencia_w,
					nr_seq_exame_p,
					nm_usuario_p,
					sysdate,
					nr_seq_frasco_p,
					nr_seq_material_p,
					ds_frasco_inadequado_p,
					ds_transporte_p,
					'A');
	end;
end if;

nr_sequencia_p := nr_sequencia_w;

commit;

end duplicar_frasco_material_exame;
/