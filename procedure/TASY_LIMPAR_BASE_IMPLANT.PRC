create or replace PROCEDURE Tasy_Limpar_Base_Implant(
			ds_senha_p		Number,
			nr_atendimento_p	Number,
			qt_tabelas_limpas_p	out Number) IS

nm_tabela_w		Varchar2(50);
nm_atributo_w		Varchar2(50);
ds_comando_w		Varchar2(2000);
qt_reg_w		Number(15,0);
ie_acao_w		Varchar2(1);
ds_set_update_w	Varchar2(255);
ds_senha_w		Number(15,0);
nr_atendimento_w	Number(15,0);

sofar_w number(10) := 0;
total_work_w number(10) := 100;
n_cleanupdb_pck_w number(10) := 0;
nm_fase_atualizacao_w cloud_upgrade_log.nm_fase_atualizacao%type := 'DATABASE_CLEANUP';

/* ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO ATENCAO 
ao alterar o cursor c01, eh necessario alterar o SQL dentro do CorSis_FN, pois faz a verificacao se este SQL retorna algo para executar esta procedure*/
Cursor C01 is
Select	a.nm_tabela,
	a.ie_acao,
	a.ds_set_update
from	tabela_sistema b,
	tabela_limpeza a
where	a.nm_tabela	= b.nm_tabela
and	dt_avaliacao	> sysdate - 2
and	b.qt_registros_atual > 0
order by nr_seq_limpeza, a.nm_tabela;

BEGIN

SELECT COUNT(1)
INTO n_cleanupdb_pck_w
FROM USER_OBJECTS
WHERE OBJECT_NAME = 'CLEANUPDB_PCK'
AND STATUS = 'VALID';

  Select	count(1) into total_work_w
    from	tabela_sistema b,
            tabela_limpeza a
    where	a.nm_tabela	= b.nm_tabela
    and	dt_avaliacao	> sysdate - 2
    and	b.qt_registros_atual > 0;
    
  select count(trigger_name) into sofar_w
	from user_triggers
	where status = 'ENABLED';

  total_work_w := total_work_w + (sofar_w * 2);

  select count(1) into sofar_w
	from user_constraints
	where status = 'ENABLED';
  
  total_work_w := total_work_w + (sofar_w * 2);

  SELECT count(1) into sofar_w
  FROM   tabela_sistema;

  total_work_w := total_work_w + sofar_w;
select     max(8501 +
   campo_numerico(to_char(sysdate,'yyyy')) +
   campo_numerico(to_char(sysdate,'mm')) +
   campo_numerico(to_char(sysdate,'dd')))
into	ds_senha_w
from dual;
select	count(*)
into	nr_atendimento_w
from	atendimento_paciente;

qt_tabelas_limpas_p := 0;

if	(ds_senha_p = ds_senha_w) and
	(nr_atendimento_w = nr_atendimento_p) then
	update aplicacao_tasy
	set ie_status_aplicacao = 'I';
	commit;

    if (n_cleanupdb_pck_w > 1) then
        execute immediate 'begin cleanupdb_pck.cleanupdb_init; end;';
        execute immediate 'begin cleanupdb_pck.init_progress(:nm_fase_atualizacao_w,0,:total_work_w); end;' using nm_fase_atualizacao_w, total_work_w;
    end if;
    
	Tasy_Disable_Trigger;
	Tasy_Disable_Constraint;
    
	open c01;
	loop
	fetch c01 into
		nm_tabela_w,
		ie_acao_w,
		ds_set_update_w;
	exit when c01%notfound;
		begin
			if	(ie_acao_w = 'T') then
				ds_comando_w := 'truncate table ' || nm_tabela_w;
			elsif	(ie_acao_w = 'U') then
				ds_comando_w := 'update ' || nm_tabela_w ||
					' set ' || ds_set_update_w;
			elsif	(ie_acao_w in ('D','E')) then
				ds_comando_w := 'delete from ' || nm_tabela_w || ds_set_update_w;
			else
				ds_comando_w := 'select 1 from dual ';
			end	if;
			obter_valor_dinamico(ds_comando_w, qt_reg_w);
			qt_tabelas_limpas_p := qt_tabelas_limpas_p + 1;
            if (n_cleanupdb_pck_w > 1) then
              execute immediate 'begin cleanupdb_pck.add_progress(nm_fase_atualizacao_w, 1); end;';
            end if;
		exception
		when others then
			null;
		end;
	end loop;
	close c01;

	Tasy_Enable_Constraint;
	Tasy_Enable_Trigger;

	update aplicacao_tasy
	set ie_status_aplicacao = 'A';
	commit;
    
	--tasy_consistir_base;
    
end if;

END Tasy_Limpar_Base_Implant;
/