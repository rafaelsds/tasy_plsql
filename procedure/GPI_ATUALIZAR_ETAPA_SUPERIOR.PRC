create or replace
procedure gpi_atualizar_etapa_superior(	nr_sequencia_p		number,
					nm_usuario_p		varchar2,
					nr_seq_superior_p	number default null) is 


				
dt_inicio_prev_w	date;
dt_fim_prev_w		date;
dt_inicio_real_w	date;
dt_fim_real_w		date;
nr_seq_superior_w	number(10);
nr_seq_superior_ww	number(10);
nr_seq_cronograma_w	number(10);
pr_etapa_w		number(15,2);
pr_etapa_ww		number(15,2);
pr_total_etapa_w	number(15,2);
qt_hora_prev_w		number(15,2);
qt_hora_prev_ww		number(15,2)	:= 0;
qt_hora_real_w		number(15,2);
qt_registro_w		number(10);
pr_calculo_w		number(15,2);
ie_forma_calculo_w	varchar2(3);
cd_estabelecimento_w	number(3);


cursor	c01 is
select	nr_sequencia,
	pr_etapa,
	abs(nvl(decode(qt_hora_prev, 0, obter_hora_entre_datas(trunc(dt_inicio_prev,'dd'), fim_dia(dt_fim_prev)), qt_hora_prev),0)) qt_hora_prev
from	gpi_cron_etapa
where	nr_seq_superior = nr_seq_superior_w;

vet01 c01%rowtype;


begin

cd_estabelecimento_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

select	substr(nvl(max(obter_valor_param_usuario(7027, 93, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)),'Q'),1,3)
into	ie_forma_calculo_w
from	dual;

if	(nvl(nr_seq_superior_p,0) <> 0) then
	nr_seq_superior_w	:= nr_seq_superior_p;
end if;

if	(nr_seq_superior_w is null) then
	select	max(a.nr_seq_superior)
	into	nr_seq_superior_w
	from	gpi_cron_etapa a
	where	a.nr_sequencia	= nr_sequencia_p;
end if;

select	nvl(sum(a.qt_hora_prev),0),
	nvl(sum(a.qt_hora_real),0),
	nvl(sum(a.pr_etapa),0),
	min(dt_inicio_real),
	max(dt_fim_real),
	min(dt_inicio_prev),
	max(dt_fim_prev)
into	qt_hora_prev_w,
	qt_hora_real_w,
	pr_etapa_w,
	dt_inicio_real_w,
	dt_fim_real_w,
	dt_inicio_prev_w,
	dt_fim_prev_w
from	gpi_cron_etapa a
where	a.nr_seq_superior		= nr_seq_superior_w;


if	(ie_forma_calculo_w = 'P') then
	begin
	qt_hora_prev_ww	:= nvl(qt_hora_prev_w,0);
	
	if	(qt_hora_prev_ww = 0) then
		begin
		/*qt_hora_prev_ww	:= abs(obter_hora_entre_datas(trunc(dt_inicio_prev_w,'dd'), fim_dia(dt_fim_prev_w)));*/
		
		open c01;
		loop
		fetch c01 into	
			vet01;
		exit when c01%notfound;
			begin
			qt_hora_prev_ww	:= qt_hora_prev_ww + vet01.qt_hora_prev;
			end;
		end loop;
		close c01;
		
		end;
	end if;	
	
	
	
	open c01;
	loop
	fetch c01 into	
		vet01;
	exit when c01%notfound;
		begin
		pr_calculo_w	:= dividir_sem_round(vet01.qt_hora_prev, qt_hora_prev_ww) * 100;
		pr_calculo_w	:= dividir_sem_round(pr_calculo_w * vet01.pr_etapa, 100);	
		pr_etapa_ww	:= nvl(pr_etapa_ww,0) + pr_calculo_w;
		end;
	end loop;
	
	end;
elsif	(ie_forma_calculo_w = 'Q') then
	begin
	
	qt_hora_prev_ww	:= nvl(qt_hora_prev_w,0);
	
	select	count(*)
	into	qt_registro_w
	from	gpi_cron_etapa a
	where	a.nr_seq_superior	= nr_seq_superior_w
	and	a.dt_fim_real is null;

	select	(count(*) *100) --corresponderá ao 100%
	into	pr_total_etapa_w
	from	gpi_cron_etapa a
	where	a.nr_seq_superior	= nr_seq_superior_w;

	pr_etapa_ww	:= (dividir((pr_etapa_w * 100), pr_total_etapa_w));
	
	end;
end if;

if	(nvl(qt_registro_w,0) <> 0) then

	dt_fim_real_w	:= null;

end if;

if	(pr_etapa_ww > 100) then
	begin
	pr_etapa_ww	:= 100;
	end;
end if;



if	(qt_hora_prev_ww = 0) then
	qt_hora_prev_w	:= 0;
end if;



update	gpi_cron_etapa
set	qt_hora_prev		= qt_hora_prev_w,
	qt_hora_real		= qt_hora_real_w,
	dt_inicio_real		= dt_inicio_real_w,
	dt_fim_real		= dt_fim_real_w,
	dt_inicio_prev		= dt_inicio_prev_w,
	dt_fim_prev		= dt_fim_prev_w,
	dt_atualizacao		= sysdate,
	pr_etapa		= pr_etapa_ww,		
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_superior_w;

if	(nvl(nr_seq_superior_p,0) <> 0) and
	(nr_sequencia_p = nr_seq_superior_p) then
	select	max(a.nr_seq_superior)
	into	nr_seq_superior_w
	from	gpi_cron_etapa a
	where	a.nr_sequencia	= nr_sequencia_p;
end if;

if	(nvl(nr_seq_superior_w, 0) <> 0) then /*Enquanto houver superiores ...*/
	
	gpi_atualizar_etapa_superior(	nr_seq_superior_w, 
				nm_usuario_p,null);
					
end if;


select	max(nr_seq_cronograma)
into	nr_seq_cronograma_w
from	gpi_cron_etapa
where	nr_sequencia	= nr_sequencia_p;

gpi_atualizar_cronograma(	nr_seq_cronograma_w,
			nm_usuario_p);
	
commit;


end gpi_atualizar_etapa_superior;
/
