create or replace
procedure atualizar_utilizacao_kit_estq(nr_kit_estoque_p	number,
					nm_usuario_p		varchar2) is 
begin
if	(nr_kit_estoque_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	kit_estoque 
	set	dt_utilizacao 	= sysdate,
		dt_atualizacao 	= sysdate,
		nm_usuario	= nm_usuario_p
	where  	nr_sequencia 	= nr_kit_estoque_p;
	end;
end if;
commit;
end atualizar_utilizacao_kit_estq;
/