create or replace
procedure gravar_log_alteracao(
            ds_valor_old_p         varchar2,
            ds_valor_new_p         varchar2,
            nm_usuario_p         varchar2,
            nr_seq_log_atual_p     in out number,
            nm_campo_p         varchar2,
            ie_log_p         in out varchar2,
            ds_descricao_p         in varchar2,
            nm_tabela_p         in varchar2,
            ds_chave_simples_p    in varchar2,
            ds_chave_composta_p    in varchar2) is
nr_sequencia_w    number(10);
nr_seq_just_w    number(10);
ds_just_w    varchar2(255);
ie_callstack_w    varchar2(1) := '';
ds_callstack_w    varchar2(500) := '';
nr_ordem_servico_w number := 0;
ie_exige_ccb_w number := 0;
  nm_maquina_w         VARCHAR2(250);
  cd_maquina_ip_w      VARCHAR2(250);
  nr_seq_equipamento_w NUMBER;
  nr_seq_terminal_w    NUMBER;
begin
    if    ((ds_valor_old_p is null) and (ds_valor_new_p is not null)) or
        ((ds_valor_new_p is null) and (ds_valor_old_p is not null)) or
        (ds_valor_old_p <> ds_valor_new_p) then
            if     (ie_log_p is null) then
                ie_log_p := 'S';
                select    tasy_log_alteracao_seq.nextval
                into    nr_seq_log_atual_p
                from    dual;
                
                obter_valor_dinamico('select wheb_log_pck.get_nr_seq_log_tasy from dual', nr_seq_just_w);
                if    (nr_seq_just_w = 0) then
                    nr_seq_just_w := null;
                end if;
                obter_valor_dinamico_char_bv('select substr(wheb_log_pck.get_ds_log_tasy,1,255) from dual', null, ds_just_w);            
                exec_sql_dinamico('Almir', 'begin wheb_log_pck.set_log_tasy(null,null); end; ');
                
                obter_valor_dinamico_char_bv('select substr(wheb_log_pck.get_ie_gerar_callstack(:nm_tabela_p),1,500) from dual', 'nm_tabela_p='||nm_tabela_p||';', ie_callstack_w);    
                                    
                if    (ie_callstack_w = 'S') then
                    begin
                    ds_callstack_w := 'CS - ' || substr(dbms_utility.format_call_stack,30,530);
                    end;
                end if;

                if OBTER_SE_BASE_WHEB = 'S' then                    	
			begin
				obter_valor_dinamico_bv(
					'SELECT COUNT(0) FROM TABELA_SISTEMA_CCB WHERE NM_TABELA = UPPER(:nm_tabela_p)',
					'nm_tabela_p='|| nm_tabela_p,
					ie_exige_ccb_w
					);					
							
				obter_valor_dinamico_bv(
					'SELECT REG_OBJECT_LOG_PCK.load_so_activity_corp(:nm_usuario_p) FROM DUAL',
					'nm_usuario_p=' || nm_usuario_p,
					nr_ordem_servico_w
					);
										
				if (ie_exige_ccb_w > 0 
					or (nm_tabela_p in ('DIC_EXPRESSAO', 'DIC_EXPRESSAO_IDIOMA') and nr_ordem_servico_w > 0)) then
					begin
						if ds_just_w <> null and length(ds_just_w) > 0 then
							ds_just_w := ds_just_w || ' - OS '|| nr_ordem_servico_w;
						else
							ds_just_w := 'OS '|| nr_ordem_servico_w;
						end if;
					end;
                    		end if;		
			end;
                end if;                    

                nm_maquina_w    := wheb_usuario_pck.get_nm_maquina();
                cd_maquina_ip_w := wheb_usuario_pck.get_cd_maquina_ip();
                IF nm_maquina_w IS NOT NULL OR
				    cd_maquina_ip_w IS NOT NULL THEN
					BEGIN
					  SELECT met.nr_sequencia,
							 met.nr_seq_equipamento
						INTO nr_seq_terminal_w,
							 nr_seq_equipamento_w
						FROM man_equipamento_terminal met
					   WHERE (met.ds_hostname = nm_maquina_w OR met.ds_ipv4 = cd_maquina_ip_w)
						 AND rownum <= 1;
					EXCEPTION
					  WHEN no_data_found THEN
						nr_seq_terminal_w    := NULL;
						nr_seq_equipamento_w := NULL;
					END;
					IF nr_seq_terminal_w IS NOT NULL THEN
					  ds_callstack_w := 'MC - ' || 
										' Equipamento : ' || nr_seq_equipamento_w || 
										' Terminal : ' || nr_seq_terminal_w || 
										' IP : ' || cd_maquina_ip_w || 
										' Host : ' || nm_maquina_w ||
										 ds_callstack_w;
					END IF;
			    END IF;    
                    
                insert into tasy_log_alteracao (
                    nr_sequencia,
                    nm_tabela,
                    ds_chave_simples,
                    ds_chave_composta,
                    dt_atualizacao,
                    nm_usuario,
                    ds_descricao,
                    nr_seq_justificativa,
                    ds_justificativa)
                values (nr_seq_log_atual_p,
                    upper(nm_tabela_p),
                    upper(ds_chave_simples_p),
                    upper(ds_chave_composta_p),
                    sysdate,
                    nm_usuario_p,
                    substr(ds_descricao_p || ds_callstack_w,1,500),
                    nr_seq_just_w,
                    ds_just_w);    
                                                                        
            end if;
        
            select    tasy_log_alt_campo_seq.nextval
            into    nr_sequencia_w
            from     dual;
        
            insert into tasy_log_alt_campo (
                nr_sequencia,
                ds_valor_old,
                ds_valor_new,
                dt_atualizacao,
                nm_usuario,
                nr_seq_log_alteracao,
                nm_atributo)
            values (
                nr_sequencia_w,
                ds_valor_old_p,
                ds_valor_new_p,
                sysdate,
                nm_usuario_p,
                nr_seq_log_atual_p,
                nm_campo_p);
    end if;
end gravar_log_alteracao;
/
