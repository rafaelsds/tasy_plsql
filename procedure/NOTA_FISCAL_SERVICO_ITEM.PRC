create or replace
procedure nota_fiscal_servico_item (cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				nm_usuario_p		varchar2) is

contador_w			number(10) := 0;
qt_registros_W			number(10);
ds_arquivo_ww			varchar2(3000);
nr_sequencia_w			varchar2(10);	
nr_seq_aux_w			varchar2(10);
nr_serie_nf_w			varchar2(5);
nr_nota_fiscal_w		varchar2(6);
dt_emissao_documento_w	varchar2(8);
nr_item_nf_w			varchar2(3);
cd_item_servico_w		varchar2(20);
ds_item_servico_w		varchar2(45);
vl_unitario_item_nf_w	varchar2(17);	
vl_desconto_w			varchar2(17);
vl_aliq_ir_w			varchar2(5);
vl_base_ir_w			varchar2(17);
vl_valor_ir_w			varchar2(17);

cursor	c01 is
select	n.nr_sequencia												nr_sequencia,
	rpad(n.cd_serie_nf,5,' ')											nr_serie_nf,
	lpad(substr(n.nr_nota_fiscal,1,6),6,' ')										nr_nota_fiscal,
	lpad(substr(to_char(n.dt_emissao,'ddmmyyyy'),1,8),8,' ')							dt_emissao_documento,
	lpad(i.nr_item_nf,3,'0') 												nr_item_nf,
	lpad(nvl(substr(decode(i.cd_material,null,obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'),obter_procedimento_nfse(n.nr_sequencia,'O')),'CD'),i.cd_material),1,20),' '),20,' ') cd_item_servico,
	rpad(nvl(decode(i.cd_material,null,obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'),obter_procedimento_nfse(n.nr_sequencia,'O')),'DS'),obter_desc_material(i.cd_material)),' '),45, ' ') ds_item_servico,	
	lpad(replace(replace(replace(to_char(nvl(i.vl_unitario_item_nf,0),'999,999,999,990.99'),',',''),'.',''),' ',''),17,' ')	vl_unitario_item_nf,	
	lpad(replace(replace(replace(to_char(nvl(i.vl_desconto,0),'999,999,999,990.99'),',',''),'.',''),' ',''),17,' ')		vl_desconto,	
	lpad(nvl(replace(replace(replace(to_char(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'IR'),0),'999,999,999,990.00'),',',''),'.',''),' ',''),' '),5,' ')vl_aliq_ir,
	lpad(nvl(replace(replace(replace(to_char(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'IR'),0),'999,999,999,990.00'),',',''),'.',''),' ',''),' '),17,' ')vl_base_ir,
	lpad(nvl(replace(replace(replace(to_char(nvl(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'IR'),0),'999,999,999,990.00'),',',''),'.',''),' ',''),' '),17,' ')vl_valor_ir
from	nota_fiscal n,
	nota_fiscal_item i,
	operacao_nota o
where	i.nr_sequencia = n.nr_sequencia
and	o.cd_operacao_nf = n.cd_operacao_nf
and	o.ie_servico = 'S'
and	n.ie_tipo_nota in ('SD','SE','SF','ST')
and	n.dt_emissao between to_date(to_char(dt_inicio_p,'dd/mm/yyyy'),'dd/mm/yyyy') and fim_dia(to_date(to_char(dt_fim_p,'dd/mm/yyyy'),'dd/mm/yyyy'))	
order by n.nr_sequencia, i.nr_item_nf;

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_serie_nf_w,
	nr_nota_fiscal_w,
	dt_emissao_documento_w,
	nr_item_nf_w,
	cd_item_servico_w,
	ds_item_servico_w,	
	vl_unitario_item_nf_w,	
	vl_desconto_w,	
	vl_aliq_ir_w,
	vl_base_ir_w,
	vl_valor_ir_w;
exit when c01%notfound;
	begin
	
	contador_w := contador_w + 1;
	
	ds_arquivo_ww	:=			nr_serie_nf_w			||
						nr_nota_fiscal_w		||
						dt_emissao_documento_w	||
						nr_item_nf_w			||
						cd_item_servico_w		||
						ds_item_servico_w		||	
						vl_unitario_item_nf_w	||	
						vl_desconto_w			||	
						vl_aliq_ir_w			||
						vl_base_ir_w			||
						vl_valor_ir_w;	

insert into w_inss_direp_arquivo ( nr_sequencia, 
				cd_estabelecimento, 
				dt_atualizacao, 
				nm_usuario, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				ds_arquivo_w)
		values		(w_inss_direp_arquivo_seq.nextval,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_ww);

	end;
end loop;
close c01;

commit;

end nota_fiscal_servico_item;
/
