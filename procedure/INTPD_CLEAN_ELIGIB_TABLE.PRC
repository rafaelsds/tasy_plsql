create or replace procedure intpd_clean_eligib_table (
		nm_usuario_p varchar2) is

begin

  delete
  from w_insurance_eligibility
  where nm_usuario = nm_usuario_p;

  delete
  from w_insur_eligibility_log
  where nm_usuario = nm_usuario_p;

end intpd_clean_eligib_table;
/