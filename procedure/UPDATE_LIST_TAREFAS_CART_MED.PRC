create or replace
procedure update_list_tarefas_cart_med(
		nr_seq_carta_mae_p	number,
		nr_seq_tarefa_p		number) is

begin		

update	wl_worklist
set	nr_seq_carta_mae = nr_seq_carta_mae_p
where	nr_sequencia = nr_seq_tarefa_p;

commit;

end update_list_tarefas_cart_med;
/
