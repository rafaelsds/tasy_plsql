create or replace
procedure HCL_enviar_email_aut_medico is 

ds_titulo_w		varchar2(2000);
ds_email_w		varchar2(2000);
cd_agenda_w		number(10);
ie_dia_semana_w		number(1);
hr_envio_w		date;
nr_sequencia_w		number(10);
cd_pessoa_medico_w	varchar2(10);
ds_email_med_w		varchar2(255);
ds_remetente_w		varchar2(500);
cd_dia_semana_w		number(1);
ds_erro_w		varchar2(255);
ds_dados_agenda_w	varchar2(2000);
nm_paciente_w		varchar2(255);
dt_agendamento_w	varchar2(50);
ds_item_w		varchar2(2000);
nm_usuario_w		varchar2(255);
				
Cursor C01 is		
	select	a.nr_sequencia,
		a.ds_titulo,
		a.ds_email,
		a.ds_remetente,
		b.cd_agenda,
		b.ie_dia_semana,
		b.hr_envio	
	from	EMAIL_ENVIO_AUT_AGENDA a,
		REGRA_ENVIO_EMAIL_AUT_AGE b		
	where	a.nr_sequencia 			= b.nr_seq_email
	and	to_char(b.hr_envio, 'hh24:mi')	= to_char(sysdate,'hh24:mi')
	and	((cd_dia_semana_w		= b.ie_dia_semana) or (b.ie_dia_semana = 9 and cd_dia_semana_w not in (1,7)))
	order 	by 3;
	
Cursor C02 is
	select	to_char(a.dt_agenda, 'dd/mm/yyyy hh24:mi'),
		substr(nvl(obter_nome_pf(a.cd_pessoa_fisica), a.nm_paciente),1,255)
	from	agenda_consulta a,
		agenda b
	where	a.cd_agenda 		= b.cd_agenda
	and	b.ie_situacao		= 'A'
	--and	b.cd_estabelecimento	=  wheb_usuario_pck.get_cd_estabelecimento	
	and	b.cd_agenda		= cd_agenda_w
	and	a.dt_agenda		between trunc(sysdate) and trunc(sysdate) + 86399/86400
	and	a.ie_status_agenda	<> 'L'
	order by 1;			
	
begin
cd_dia_semana_w	:= obter_cod_dia_semana(sysdate);
nm_usuario_w	:= 'Tasy';

if	(sysdate >= trunc(sysdate) + 8/24) and
	(sysdate <= trunc(sysdate) + 18/24) then
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		ds_titulo_w,
		ds_email_w,
		ds_remetente_w,
		cd_agenda_w,
		ie_dia_semana_w,
		hr_envio_w;
	exit when C01%notfound;
		begin
		if	(cd_agenda_w is not null)then
			select	max(a.cd_pessoa_fisica)
			into	cd_pessoa_medico_w
			from	agenda a
			where	a.cd_agenda	= cd_agenda_w;			
		end if;
		
			if	(cd_pessoa_medico_w is not null)then
				select	max(obter_compl_pf(cd_pessoa_medico_w,1,'M'))
				into	ds_email_med_w
				from	dual;
			end if;
		
		open C02;
		loop
		fetch C02 into	
			dt_agendamento_w,
			nm_paciente_w;
		exit when C02%notfound;
			begin
			ds_item_w	:= ds_item_w||' '||dt_agendamento_w||' - '||nm_paciente_w||chr(10);			
			end;
		end loop;
		close C02;
		
		if	(nr_sequencia_w > 0) then
			begin			
			ds_email_w	:= substr(replace_macro(ds_email_w, '@DADOS@', ds_item_w),1,2000);			
			enviar_email(ds_titulo_w, ds_email_w, ds_remetente_w, ds_email_med_w, nm_usuario_w, 'M');
			exception
			when others then
				ds_erro_w	:= '';
			end;				
		end if;
		
		end;
	end loop;
	close C01;
end if;

end HCL_enviar_email_aut_medico;
/