create or replace
procedure san_altera_texto_envio_corresp	(nr_seq_envio_p		number,
					nr_seq_texto_padrao_p	number) is 

nr_seq_envio_w		number;
nr_seq_corresp_item_w	number;
ds_texto_padrao_w		long;

Cursor C01 is
	select	a.nr_sequencia
	from	san_envio_corresp_item a
	where	a.nr_seq_envio = nr_seq_envio_p;

begin
if	(nr_seq_envio_p is not null) then

	select 	ds_texto
	into	ds_texto_padrao_w
	from	san_texto_padrao
	where 	nr_sequencia  = nr_seq_texto_padrao_p;

	open C01;
	loop
	fetch C01 into	
		nr_seq_envio_w;
	exit when C01%notfound;
		begin
		update	san_envio_corresp_item
		set	ds_texto_padrao	= ds_texto_padrao_w
		where	nr_seq_envio	= nr_seq_envio_p;
		end;
	end loop;
	close C01;

end if;

commit;

end san_altera_texto_envio_corresp;
/