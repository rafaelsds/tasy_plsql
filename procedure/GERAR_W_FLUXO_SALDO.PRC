create or replace
procedure GERAR_W_FLUXO_SALDO(	cd_empresa_p		number,
				cd_estabelecimento_p	number,
				dt_inicial_p		date,
				dt_final_p		date,
				nm_usuario_p		varchar2) is

vl_saldo_banco_w	number(15,2);
vl_saldo_tesouraria_w	number(15,2);
vl_saldo_fluxo_w	number(15,2);
dt_referencia_w		date;
cd_conta_saldo_w	number(10);
nr_seq_regra_comp_w	Parametro_Fluxo_caixa.nr_seq_regra_comp%type;


begin

delete	from	w_fluxo_saldo
where	nm_usuario	= nm_usuario_p;

select	max(cd_conta_financ_sant),
	max(nr_seq_regra_comp)
into 	cd_conta_saldo_w,
	nr_seq_regra_comp_w
from 	Parametro_Fluxo_caixa
where	cd_estabelecimento = cd_estabelecimento_p;

gerar_fluxo_caixa_passado(cd_estabelecimento_p,
			dt_inicial_p,
			dt_final_p,
			nm_usuario_p,
			cd_empresa_p,
			'S',
			0,
			'D',
			'N',
			'N',
			'A',
			'N',
			nr_seq_regra_comp_w);

dt_referencia_w	:= dt_inicial_p;

while	(dt_referencia_w >= dt_inicial_p) and
	(dt_referencia_w <= dt_final_p) loop
	begin	

	select	nvl(sum(obter_saldo_banco_diario(b.nr_sequencia, fim_dia(dt_referencia_w - 1))),0)
	into	vl_saldo_banco_w
	from	banco_saldo b,
		banco_estabelecimento a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	OBTER_EMPRESA_ESTAB(a.cd_estabelecimento)	= cd_empresa_p
	and	b.nr_seq_conta		= a.nr_sequencia
	and	a.ie_tipo_relacao	in ('C', 'ECC')
	and	a.ie_fluxo_caixa	= 'S'
	and	a.ie_situacao = 'A'
	and	b.dt_referencia =	(select	max(dt_referencia)
					from	banco_saldo x
					where	x.nr_seq_conta	= b.nr_seq_conta
					and	x.dt_referencia <= dt_referencia_w);

	select	nvl(sum(a.vl_saldo),0)
	into	vl_saldo_tesouraria_w
	from	caixa b,
		caixa_saldo_diario a
	where	b.cd_estabelecimento	= cd_estabelecimento_p
	and	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento)	= cd_empresa_p
	and	a.nr_seq_caixa	= b.nr_sequencia
	and	b.ie_situacao	= 'A'
	and	b.ie_fluxo	= 'S'
	and	a.dt_saldo = 	(select	max(dt_saldo)
				from	caixa_saldo_diario x
				where	x.nr_seq_caixa	= a.nr_seq_caixa
				and	trunc(x.dt_saldo,'dd') <= dt_referencia_w - 1);

	select	nvl(max(vl_fluxo), 0)
	into 	vl_saldo_fluxo_w
	from	fluxo_caixa
	where	dt_referencia		= dt_referencia_w
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	OBTER_EMPRESA_ESTAB(cd_estabelecimento)	= cd_empresa_p
	and	ie_classif_fluxo	= 'P'
	and	ie_periodo		= 'D'
	and	cd_conta_financ		= cd_conta_saldo_w
	and	cd_empresa		= cd_empresa_p;

	insert	into	w_fluxo_saldo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_saldo,
		vl_saldo_banco,
		vl_saldo_tesouraria,
		vl_saldo_fluxo)
	values	(w_fluxo_saldo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		dt_referencia_w,
		vl_saldo_banco_w,
		vl_saldo_tesouraria_w,
		vl_saldo_fluxo_w);

	dt_referencia_w	:= dt_referencia_w + 1;    

	end;
end loop;

end GERAR_W_FLUXO_SALDO;
/