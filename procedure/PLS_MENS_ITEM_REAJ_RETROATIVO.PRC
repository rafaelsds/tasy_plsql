create or replace 
procedure pls_mens_item_reaj_retroativo
			(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
				nr_seq_segurado_p		pls_mensalidade_segurado.nr_seq_segurado%type,
				dt_referencia_p			pls_mensalidade_segurado.dt_mesano_referencia%type,
				dt_contratacao_p		pls_segurado.dt_contratacao%type,
				dt_rescisao_p			pls_segurado.dt_rescisao%type,
				nm_usuario_p			varchar2,
				nr_seq_pagador_p		pls_contrato_pagador.nr_sequencia%type) is

/* OBS: A logica dessa rotina nao foi refeita, devido a necessidade de reestruturacao do reajuste antes de revisar a mesma
Rotina original: PLS_GERAR_MENSALIDADE_RETRO
*/
nr_seq_reajuste_preco_w		pls_reajuste.nr_sequencia%type		:= null;
ie_tipo_reajuste_w		pls_reajuste.ie_tipo_reajuste%type	:= null;
dt_reajuste_w			pls_reajuste.dt_reajuste%type		:= null;
tx_reajuste_w			pls_reajuste.tx_reajuste%type		:= null;
nr_seq_lote_deflat_w		pls_reajuste.nr_seq_lote_deflator%type	:= null;
nr_seq_reajuste_retro_w		pls_reajuste.nr_sequencia%type		:= null;
nr_seq_reajuste_deflator_w	pls_reajuste.nr_sequencia%type		:= null;
nr_seq_reajuste_def_w		pls_reajuste.nr_sequencia%type		:= null;
nr_seq_lote_reajuste_w		pls_reajuste.nr_sequencia%type		:= null;
vl_preco_atual_w		pls_segurado_preco.vl_preco_atual%type	:= 0;
vl_desconto_w			pls_segurado_preco.vl_desconto%type	:= 0;
nr_seq_segurado_preco_retro_w	pls_segurado_preco.nr_sequencia%type	:= null;
nr_seq_segurado_preco_w		pls_segurado_preco.nr_sequencia%type	:= null;
nr_seq_preco_ant_w		pls_segurado_preco.nr_sequencia%type	:= null;
vl_preco_cobr_retroativo_w	pls_mensalidade_segurado.vl_pre_estabelecido%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
dt_referencia_reajuste_w	pls_reajuste_cobr_retro.dt_referencia_reajuste%type;
vl_reajuste_w			number(15,2)	:= 0;
vl_diferenca_w			number(15,2)	:= 0;
vl_reajustado_w			number(15,2)	:= 0;
vl_reajustado_retro_w		number(15,2)	:= 0;
vl_reajustado_nao_subsid_w	number(15,2)	:= 0;
vl_reajustado_retro_subsid_w	number(15,2)	:= 0;
vl_segurado_ant_w		number(15,2)	:= 0;
qt_mensalidade_gerada_w		pls_integer	:= 0;
qt_reajuste_retro_w		pls_integer	:= 0;
qt_mens_rescisao_w		pls_integer	:= 0;
qt_mens_retro_w			pls_integer	:= 0;
ds_mensagem_reajuste_w		varchar2(4000)	:= null;
ie_data_base_proporcional_w	varchar2(1);
dt_ultimo_dia_mes_w		varchar2(2);
qt_dias_proporcional_w		pls_integer;
nr_seq_reajuste_w		pls_segurado_preco.nr_seq_reajuste%type	:= null;
nr_seq_reajuste_ww		pls_segurado_preco.nr_seq_reajuste%type	:= null;
cd_motivo_reajuste_w		pls_segurado_preco.cd_motivo_reajuste%type;
nr_seq_reajuste_cb_retro_w	pls_reajuste_cobr_retro.nr_sequencia%type := null;
nr_seq_reajuste_cb_retro_ant_w  pls_reajuste_cobr_retro.nr_sequencia%type := null;
nr_seq_plano_w			pls_plano.nr_sequencia%type;
nr_seq_plano_ww			pls_plano.nr_sequencia%type;
ie_inseriu_item_w		varchar2(1);
qt_mens_retro_ww		pls_integer	:= 0;
qt_item_pre_w			number(10);
dt_inclusao_pce_w		date;
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
apropriacao_item_w		pls_mens_itens_pck.tb_apropriacao_item;

Cursor C01 is
	select	a.dt_referencia_reajuste,
		a.nr_sequencia
	from	pls_reajuste_cobr_retro	a,
		pls_reajuste		b
	where	a.nr_seq_reajuste	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_lote_reajuste_w
	and	a.dt_referencia_mensalidade	= dt_referencia_p
	and	a.dt_liberacao is null; --Se estiver liberado serao gerados lancamentos programados, que serao gerados na rotina PLS_MENS_ITEM_LANC_PROG_BENEF

begin
ie_data_base_proporcional_w	:= pls_mensalidade_util_pck.get_ie_data_base_proporcional;
apropriacao_item_w.delete;
select	count(1)
into	qt_mensalidade_gerada_w
from	pls_mensalidade_segurado
where	nr_sequencia	= nr_seq_mensalidade_seg_p;

if	(qt_mensalidade_gerada_w > 0) then
	select	max(a.nr_sequencia),
		max(a.nr_seq_reajuste),
		max(e.nr_sequencia),
		max(b.dt_inclusao_pce),
		max(b.nr_seq_pagador)
	into	nr_seq_segurado_preco_w,
		nr_seq_reajuste_ww,
		nr_seq_plano_w,
		dt_inclusao_pce_w,
		nr_seq_pagador_w
	from	pls_segurado_preco	a,
		pls_segurado		b,
		pls_plano_preco		c,
		pls_tabela_preco	d,
		pls_plano		e
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_seq_preco		= c.nr_sequencia
	and	c.nr_seq_tabela		= d.nr_sequencia
	and	d.nr_seq_plano		= e.nr_sequencia
	and	b.nr_sequencia		= nr_seq_segurado_p
	and	a.dt_liberacao	is not null
	and	a.cd_motivo_reajuste = 'I';
	
	if	(nvl(nr_seq_segurado_preco_w,0) > 0) then
		select	nvl(a.vl_preco_atual,0) - nvl(vl_desconto,0),
			nvl(a.nr_seq_reajuste,0),
			nvl(a.vl_reajuste,0),
			nvl(vl_desconto,0),
			nvl(nr_seq_lote_reajuste,0)
		into	vl_preco_atual_w,
			nr_seq_reajuste_preco_w,
			vl_reajuste_w,
			vl_desconto_w,
			nr_seq_lote_reajuste_w
		from	pls_segurado_preco	a
		where	a.nr_sequencia		= nr_seq_segurado_preco_w;
		
		vl_diferenca_w := vl_reajuste_w;
		
		if	(nr_seq_lote_reajuste_w = 0) and
			(nr_seq_reajuste_preco_w <> 0) then
			select	nr_seq_reajuste
			into	nr_seq_lote_reajuste_w
			from	pls_reajuste_preco
			where	nr_sequencia	= nr_seq_reajuste_preco_w;
		end if;
		
		if	(nr_seq_lote_reajuste_w <> 0) then
			select	ie_tipo_reajuste,
				dt_reajuste,
				tx_reajuste,
				cd_estabelecimento
			into	ie_tipo_reajuste_w,
				dt_reajuste_w,
				tx_reajuste_w,
				cd_estabelecimento_w
			from	pls_reajuste
			where	nr_sequencia	= nr_seq_lote_reajuste_w;
		end if;
		
		if	(nvl(vl_reajuste_w,0) = 0) or
			(nvl(vl_desconto_w,0) <> 0)then
			/* Obter o valor COM desconto anterior do beneficiario */
			select	max(a.nr_sequencia)
			into	nr_seq_preco_ant_w
			from 	pls_segurado_preco	a,
				pls_segurado		b
			where	a.nr_seq_segurado	= b.nr_sequencia
			and	b.nr_sequencia		= nr_seq_segurado_p
			and	a.dt_liberacao	is not null
			and	a.nr_sequencia	<> nr_seq_segurado_preco_w
			and	a.dt_reajuste	< dt_reajuste_w;
			
			/* Segundo a OS 144763, o valor do reajuste deve ser sobre os valores com desconto */
			if	(nvl(nr_seq_preco_ant_w,0) <> 0) then
				select	nvl(vl_preco_atual,0) - nvl(vl_desconto,0)
				into	vl_segurado_ant_w
				from	pls_segurado_preco
				where	nr_sequencia	= nr_seq_preco_ant_w;
				
				vl_reajuste_w	:= nvl(vl_preco_atual_w,0) - nvl(vl_segurado_ant_w,0);
				vl_diferenca_w  := vl_reajuste_w;
			end if;
		end if;
	end if;
	
	select	count(1)
	into	qt_reajuste_retro_w
	from	pls_reajuste_cobr_retro	a,
		pls_reajuste		b
	where	a.nr_seq_reajuste	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_lote_reajuste_w
	and	a.dt_referencia_mensalidade	= dt_referencia_p;
	
	if	(qt_reajuste_retro_w > 0) and
		((dt_rescisao_p is null) or
		(trunc(dt_rescisao_p,'mm') >= trunc(dt_referencia_p,'mm')) or
		(pls_mensalidade_util_pck.get_ie_reaj_retro_benef_resc = 'S')) then
		if	(ie_tipo_reajuste_w	= 'I') then
			open C01;
			loop
			fetch C01 into
				dt_referencia_reajuste_w,
				nr_seq_reajuste_cb_retro_w;
			exit when C01%notfound;
				begin
				if	(dt_referencia_reajuste_w >= trunc(dt_contratacao_p,'month')) then
					select	min(nr_seq_segurado_preco)
					into	nr_seq_reajuste_retro_w
					from	pls_mensalidade_segurado a,
						pls_mensalidade b
					where	a.nr_seq_mensalidade = b.nr_sequencia
					and	a.nr_seq_segurado = nr_seq_segurado_p
					and	a.dt_mesano_referencia = dt_referencia_reajuste_w
					and	b.ie_cancelamento is null;
					
					select	max(nr_seq_reajuste)
					into	nr_seq_reajuste_w
					from	pls_segurado_preco
					where	nr_seq_segurado = nr_seq_segurado_p
					and	dt_reajuste between trunc(dt_referencia_reajuste_w,'month') and last_day(dt_referencia_reajuste_w)
					and	nr_seq_reajuste is not null;
					
					/* Verificar se existe o mes da cobranca retroativa ja nao estava reajustado, ou se ja foi realizada a cobranca retroativa desse mes */
					select	count(1)
					into	qt_mens_retro_w
					from	pls_mensalidade_segurado	a,
						pls_segurado			b,
						pls_mensalidade			c,
						pls_mensalidade_seg_item	d
					where	a.nr_seq_segurado	= b.nr_sequencia
					and	a.nr_seq_mensalidade	= c.nr_sequencia
					and	d.nr_seq_mensalidade_seg = a.nr_sequencia
					and	c.ie_cancelamento is null
					and	b.nr_sequencia		= nr_seq_segurado_p
					and	((d.nr_seq_reajuste = nr_seq_reajuste_preco_w and a.dt_mesano_referencia = dt_referencia_reajuste_w)
						or ((a.dt_mensalidade_retro = dt_referencia_reajuste_w)));
					
					qt_item_pre_w := 1;
					
					select	count(1)
					into	qt_item_pre_w
					from	pls_mensalidade a,
						pls_mensalidade_segurado b,
						pls_mensalidade_seg_item c
					where	a.nr_sequencia = b.nr_seq_mensalidade
					and	b.nr_sequencia = c.nr_seq_mensalidade_seg
					and	a.ie_cancelamento is null
					and	a.nr_seq_pagador = nr_seq_pagador_p
					and	b.nr_seq_segurado = nr_seq_segurado_p
					and	c.ie_tipo_item = '1'
					and	b.dt_mesano_referencia = dt_referencia_reajuste_w
					and	not exists( 	select 	1
								from	pls_mensalidade_seg_item x
								where	x.nr_seq_mensalidade_seg = b.nr_sequencia
								and	x.ie_tipo_item = '25');
					
					if	((nr_seq_reajuste_w = nr_seq_reajuste_ww) or (nr_seq_reajuste_w is null)) and 
						((nvl(nr_seq_reajuste_retro_w,0) <> nr_seq_segurado_preco_w) and
						((qt_mens_retro_w = 0) and (qt_item_pre_w > 0))) then /* Verifica se ja foi realizada a cobranca retroativa ou se ja estava reajustada */
						ds_mensagem_reajuste_w	:= 	wheb_mensagem_pck.get_texto(1130832) || ' retroativa ao ' || 
										wheb_mensagem_pck.get_texto(1130833) || ' ' || dt_referencia_reajuste_w||', referente ao reajuste de '||tx_reajuste_w||'% de '||to_char(dt_reajuste_w,'mm/yyyy');
						
						select	sum(a.vl_pre_estabelecido)
						into	vl_preco_cobr_retroativo_w
						from	pls_mensalidade_segurado	a,
							pls_segurado			b,
							pls_mensalidade			c
						where	a.nr_seq_segurado	= b.nr_sequencia
						and	a.nr_seq_mensalidade	= c.nr_sequencia
						and	b.nr_sequencia		= nr_seq_segurado_p
						and	a.dt_mesano_referencia	= dt_referencia_reajuste_w
						and	c.ie_cancelamento is null;
						
						/*aaschlote 03/09/2013 - OS 640267*/
						if	(nvl(vl_preco_cobr_retroativo_w,0) <> 0) and
							(nvl(vl_segurado_ant_w,0) <> 0) and
							(vl_segurado_ant_w <> vl_preco_cobr_retroativo_w) then
							vl_reajuste_w	:= nvl(vl_preco_atual_w,0) - nvl(vl_preco_cobr_retroativo_w,0);
						end if;
						
						if	(dt_contratacao_p is not null) then
							select	obter_dias_entre_datas(dt_contratacao_p, max(b.dt_fim_cobertura))
							into	qt_dias_proporcional_w
							from	pls_mensalidade_segurado	b,
								pls_mensalidade			a
							where	a.nr_sequencia 		= b.nr_seq_mensalidade
							and	b.dt_mesano_referencia 	= dt_referencia_reajuste_w
							and	b.nr_seq_segurado 	= nr_seq_segurado_p
							and	dt_contratacao_p between dt_inicio_cobertura and dt_fim_cobertura;
							
							if	(nvl(qt_dias_proporcional_w,0) > 0) then
								if	(ie_data_base_proporcional_w = 'T') then
									dt_ultimo_dia_mes_w	:= 30;
									if	(to_char(dt_referencia_reajuste_w,'mm') = 02) then
										dt_ultimo_dia_mes_w := to_char(last_day(dt_referencia_reajuste_w),'dd');
									end if;	
								else
									dt_ultimo_dia_mes_w	:= to_char(last_day(dt_referencia_reajuste_w),'dd');
								end if;
								
								qt_dias_proporcional_w := qt_dias_proporcional_w + 1;
								
								if	(qt_dias_proporcional_w > to_number(dt_ultimo_dia_mes_w)) then
									qt_dias_proporcional_w := to_number(dt_ultimo_dia_mes_w);
								end if;
								
								vl_reajuste_w := vl_reajuste_w * (qt_dias_proporcional_w / to_number(dt_ultimo_dia_mes_w));
							end if;
						end if;
						
						if	(vl_reajuste_w > 0) then
							update	pls_mensalidade_segurado
							set	dt_mensalidade_retro = dt_referencia_reajuste_w
							where	nr_sequencia = nr_seq_mensalidade_seg_p;
							
							pls_mens_itens_pck.add_item_preco(	nr_seq_mensalidade_seg_p, '4', vl_reajuste_w, 
												null, null, nr_seq_plano_w, 
												ds_mensagem_reajuste_w, '', dt_referencia_reajuste_w, 
												nr_seq_segurado_p, null, null,
												apropriacao_item_w, ie_inseriu_item_w);
						end if;
					end if;
				end if;
				end;
			end loop;
			close C01;
		elsif	(ie_tipo_reajuste_w	= 'C') then
			open C01;
			loop
			fetch C01 into
				dt_referencia_reajuste_w,
				nr_seq_reajuste_cb_retro_w;
			exit when C01%notfound;
				begin
				if	(dt_referencia_reajuste_w >= trunc(dt_contratacao_p,'month')) then
					select	max(a.nr_seq_reajuste),
						max(e.nr_sequencia)
					into	nr_seq_reajuste_w,
						nr_seq_plano_ww
					from	pls_segurado_preco	a,
						pls_segurado		b,
						pls_plano_preco		c,
						pls_tabela_preco	d,
						pls_plano		e
					where	a.nr_seq_segurado	= b.nr_sequencia
					and	a.nr_seq_preco		= c.nr_sequencia
					and	c.nr_seq_tabela		= d.nr_sequencia
					and	d.nr_seq_plano		= e.nr_sequencia
					and	a.nr_seq_segurado = nr_seq_segurado_p
					and	a.dt_reajuste between trunc(dt_referencia_reajuste_w,'month') and last_day(dt_referencia_reajuste_w)
					and	a.nr_seq_reajuste is not null
					and	a.cd_motivo_reajuste = 'I';
					
					/* Verificar se existe o mes da cobranca retroativa ja nao estava reajustado, ou se ja foi realizada a cobranca retroativa desse mes */
					select	count(1)
					into	qt_mens_retro_w
					from	pls_mensalidade_segurado	a,
						pls_segurado			b,
						pls_mensalidade			c,
						pls_mensalidade_seg_item	d
					where	a.nr_seq_segurado	= b.nr_sequencia
					and	a.nr_seq_mensalidade	= c.nr_sequencia
					and	d.nr_seq_mensalidade_seg = a.nr_sequencia
					and	c.ie_cancelamento is null
					and	b.nr_sequencia		= nr_seq_segurado_p
					and	((d.nr_seq_reajuste = nr_seq_reajuste_preco_w and a.dt_mesano_referencia = dt_referencia_reajuste_w)
						or ((a.dt_mensalidade_retro = dt_referencia_reajuste_w) 
						or ((a.nr_seq_reajuste = nr_seq_reajuste_preco_w) and (a.dt_mesano_referencia = dt_referencia_reajuste_w))));
					
					select	count(1)
					into	qt_mens_retro_ww
					from	pls_mensalidade_segurado a,
						pls_mensalidade_seg_item b,
						pls_mensalidade c
					where	a.nr_sequencia = b.nr_seq_mensalidade_seg
					and	a.nr_seq_mensalidade = c.nr_sequencia
					and	a.nr_seq_segurado = nr_seq_segurado_p
					and	b.ie_tipo_item = '4'
					and	c.ie_cancelamento is null
					and	a.dt_mesano_referencia = dt_referencia_p;
					
					select	min(b.nr_seq_reajuste)
					into	nr_seq_reajuste_retro_w
					from	pls_mensalidade_segurado b,
						pls_mensalidade		 a
					where	a.nr_sequencia	= b.nr_seq_mensalidade
					and	b.nr_seq_segurado	= nr_seq_segurado_p
					and	b.dt_mesano_referencia	= dt_referencia_reajuste_w
					and	b.nr_seq_reajuste > 0
					and	nr_seq_reajuste_preco_w > 0
					and	b.nr_sequencia	<> nr_seq_mensalidade_seg_p
					and	a.ie_cancelamento is null
					and	not exists	(	select	1
									from	pls_mensalidade_seg_item	x
									where	x.nr_seq_mensalidade_seg	= b.nr_sequencia);
					
					qt_item_pre_w := 1;
					
					select	count(1)
					into	qt_item_pre_w
					from	pls_mensalidade a,
						pls_mensalidade_segurado b,
						pls_mensalidade_seg_item c
					where	a.nr_sequencia = b.nr_seq_mensalidade
					and	b.nr_sequencia = c.nr_seq_mensalidade_seg
					and	a.ie_cancelamento is null
					and	a.nr_seq_pagador = nr_seq_pagador_p
					and	b.nr_seq_segurado = nr_seq_segurado_p
					and	c.ie_tipo_item = '1'
					and	b.dt_mesano_referencia = dt_referencia_reajuste_w
					and	not exists( 	select 	1
								from	pls_mensalidade_seg_item x
								where	x.nr_seq_mensalidade_seg = b.nr_sequencia
								and	x.ie_tipo_item = '25');
							
					if	(dt_inclusao_pce_w is not null) then
						if	(trunc(dt_inclusao_pce_w,'month') = trunc(dt_referencia_reajuste_w,'month')) then
							if	(nr_seq_pagador_w <> nr_seq_pagador_p) then
								qt_mens_retro_ww := 1;
							end if;
						end if;
					end if;
					
					if	(((((nr_seq_reajuste_w = nr_seq_reajuste_ww) or (nr_seq_reajuste_w is null)) or 
						(nvl(nr_seq_reajuste_cb_retro_ant_w,nr_seq_reajuste_cb_retro_w) <> nr_seq_reajuste_cb_retro_w)) and
						((nr_seq_reajuste_preco_w > nr_seq_reajuste_retro_w) or (nvl(nr_seq_reajuste_retro_w,0) = 0))) or
						(nr_seq_plano_w <> nvl(nr_seq_plano_ww,nr_seq_plano_w))) and
						((qt_mens_retro_w = 0) and (qt_mens_retro_ww = 0) and (qt_item_pre_w > 0)) then
						
						select	sum(a.vl_pre_estabelecido)
						into	vl_preco_cobr_retroativo_w
						from	pls_mensalidade_segurado	a,
							pls_segurado			b,
							pls_mensalidade			c
						where	a.nr_seq_segurado	= b.nr_sequencia
						and	a.nr_seq_mensalidade	= c.nr_sequencia
						and	b.nr_sequencia		= nr_seq_segurado_p
						and	a.dt_mesano_referencia	= dt_referencia_reajuste_w
						and	c.ie_cancelamento is null;
						
						if	(nvl(vl_preco_cobr_retroativo_w,0) <> 0) and
							(nvl(vl_segurado_ant_w,0) <> 0) and
							(vl_segurado_ant_w <> vl_preco_cobr_retroativo_w) then
							vl_diferenca_w	:= nvl(vl_preco_atual_w,0) - nvl(vl_preco_cobr_retroativo_w,0);
						end if;
						
						if	(dt_contratacao_p is not null) then
							select	obter_dias_entre_datas(dt_contratacao_p, max(b.dt_fim_cobertura))
							into	qt_dias_proporcional_w
							from	pls_mensalidade_segurado	b,
								pls_mensalidade			a
							where	a.nr_sequencia 		= b.nr_seq_mensalidade
							and	b.dt_mesano_referencia 	= dt_referencia_reajuste_w
							and	b.nr_seq_segurado 	= nr_seq_segurado_p
							and	dt_contratacao_p between dt_inicio_cobertura and dt_fim_cobertura;
							
							if	(nvl(qt_dias_proporcional_w,0) > 0) then
								if	(ie_data_base_proporcional_w = 'T') then
									dt_ultimo_dia_mes_w	:= 30;
								else
									dt_ultimo_dia_mes_w	:= to_char(last_day(dt_referencia_reajuste_w),'dd');
								end if;
								
								qt_dias_proporcional_w := qt_dias_proporcional_w + 1;
								
								if	(qt_dias_proporcional_w > to_number(dt_ultimo_dia_mes_w)) then
									qt_dias_proporcional_w := to_number(dt_ultimo_dia_mes_w);
								end if;
								
								vl_diferenca_w := vl_diferenca_w * (qt_dias_proporcional_w / to_number(dt_ultimo_dia_mes_w));
							end if;
						end if;
						
						ds_mensagem_reajuste_w	:= 	wheb_mensagem_pck.get_texto(1130832) || ' referente ao reajuste do ' || 
										wheb_mensagem_pck.get_texto(1130833) || ' ' || dt_referencia_reajuste_w||'.';
						
						if	(nvl(vl_diferenca_w,0) > 0) then
							update	pls_mensalidade_segurado
							set	nr_seq_reajuste	= nr_seq_reajuste_preco_w,
								dt_mensalidade_retro	= dt_referencia_reajuste_w
							where	nr_sequencia	= nr_seq_mensalidade_seg_p;
							
							pls_mens_itens_pck.add_item_preco(	nr_seq_mensalidade_seg_p, '4', vl_diferenca_w, 
												null, null, nr_seq_plano_w, 
												ds_mensagem_reajuste_w, '', dt_referencia_reajuste_w, 
												nr_seq_segurado_p, null, null, 
												apropriacao_item_w, ie_inseriu_item_w);
							
							if	(ie_inseriu_item_w = 'S') then
								nr_seq_reajuste_cb_retro_ant_w := nr_seq_reajuste_cb_retro_w;
							end if;
						end if;
					end if;
				end if;
				end;
			end loop;
			close C01;
		end if;
	end if;
end if;

/* Nao pode dar commit nesta procedure */
--commit;

end pls_mens_item_reaj_retroativo;
/