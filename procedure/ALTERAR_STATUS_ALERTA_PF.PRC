create or replace
procedure alterar_status_alerta_pf	(nr_seq_alerta_p	number,
					ie_status_p		varchar2) is

begin
if	(nr_seq_alerta_p is not null) and
	(ie_status_p is not null) then

	if	(ie_status_p = 'L') then /* liberar */

		update	pf_alerta
		set		dt_liberacao	= sysdate
		where	nr_sequencia	= nr_seq_alerta_p;

	elsif	(ie_status_p = 'C') then /* ciente */

		update	pf_alerta
		set		dt_ciencia	= sysdate
		where	nr_sequencia	= nr_seq_alerta_p;
		
	elsif	(ie_status_p = 'R') then /* Resolu��o*/
	
		update  pf_alerta
		set	 	dt_resolucao = sysdate
		where	nr_sequencia = nr_seq_alerta_p;
	
	end if;
	
end if;

commit;

end alterar_status_alerta_pf;
/
