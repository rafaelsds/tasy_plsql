create or replace
procedure proj_excluir_etapas_importar( nm_usuario_p		Varchar2,
										nr_seq_cronograma_p	number,
										nr_seq_etapa_ini_p	number,
										ds_sql_in_p	varchar2) is 



nr_sequencia_w number(10);

										
cursor c01 is
select nr_sequencia	from proj_cron_etapa 
where	nr_seq_cronograma = nr_seq_cronograma_p
and		nr_sequencia < nr_seq_etapa_ini_p
and		nr_sequencia not in (select nr_seq_etapa 
							from 	w_etapas_importacao 
							where 	nm_usuario_p = nm_usuario);

										
begin
							
open c01;
loop
fetch c01 into 
nr_sequencia_w;	
exit when c01%notfound;
	proj_excluir_etapa_cron (nr_seq_cronograma_p, nr_sequencia_w, nm_usuario_p);
end loop;
close c01;

commit;

end proj_excluir_etapas_importar;
/
