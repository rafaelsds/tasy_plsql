create or replace
procedure encerrar_conta_lib_resultado(	cd_setor_encerramento_p		number,
					nr_prescricao_p			number,
					nr_seq_prescr_p			number,
					nr_seq_resultado_p		number,
					nm_usuario_p			varchar2,
					ds_erro_p			out varchar2) is 

cd_setor_atendimento_w		number(5);
ds_erro_w			varchar2(255);
					
begin

select 	cd_setor_atendimento
into 	cd_setor_atendimento_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

if 	((cd_setor_encerramento_p is null) or (cd_setor_encerramento_p = 0) or
	 (cd_setor_encerramento_p = cd_setor_atendimento_w)) then
	 
	 encerr_conta_lib_resu_exam(nr_seq_prescr_p, nr_seq_resultado_p, nm_usuario_p, ds_erro_w);
	 
	 if	(ds_erro_w is not null) then
		ds_erro_p := ds_erro_w;
	 end if;
end if;

end encerrar_conta_lib_resultado;
/
