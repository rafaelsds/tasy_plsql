create or replace
procedure gerar_tiss28_sae(nr_sequencia_p		number,
			 nm_usuario_p		varchar2) is

nr_atendimento_w	number(10);
qt_reg_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_item_escala_w	number(10);

/* Atributos da escala */
ie_ab_monit_padrao_w		varchar2(1);
ie_ab_laboratorio_w		varchar2(1);
ie_ab_medic_unica_w		varchar2(1);
ie_ab_medic_endovenosa_w	varchar2(1);
ie_ab_curativo_rotina_w		varchar2(1);
ie_ab_curativo_frequente_w	varchar2(1);
ie_ab_dreno_w			varchar2(1);
ie_sv_vent_mecanica_w		varchar2(1);
ie_sv_sup_vent_supl_w		varchar2(1);
ie_sv_vias_aerias_w		varchar2(1);
ie_sv_trat_funcao_pulmonar_w	varchar2(1);
ie_sc_medic_vasoativa_unica_w	varchar2(1);
ie_sc_medic_vasoativa_mult_w	varchar2(1);
ie_sc_rep_volemica_w		varchar2(1);
ie_sc_cateter_art_perif_w	varchar2(1);
ie_sc_monit_atrio_esq_w		varchar2(1);
ie_sc_via_venosa_central_w	varchar2(1);
ie_sc_ressuscit_cardio_w	varchar2(1);
ie_sr_tec_hemofiltracao_w	varchar2(1);
ie_sr_med_debito_urina_w	varchar2(1);
ie_sr_diurese_ativa_w		varchar2(1);
ie_sn_pressao_intracraniana_w	varchar2(1);
ie_sm_acidose_alcalose_w	varchar2(1);
ie_sm_nut_parent_w		varchar2(1);
ie_sm_nut_enteral_w		varchar2(1);
ie_ie_inter_unica_uti_w		varchar2(1);
ie_ie_inter_mult_uti_w		varchar2(1);
ie_ie_inter_fora_uti_w		varchar2(1);

	function obter_valor_escala( nr_seq_item_escala_p	number) return varchar2 is

	ie_retorno_w	varchar2(15);
	nr_seq_result_escala_w	varchar2(15);
	begin
	
	select	max(f.nr_seq_result_escala)
	into	nr_seq_result_escala_w
	from	pe_prescricao a,
		pe_prescr_item_result b,
		pe_regra_item_escala c,
		pe_regra_result_escala f
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_sequencia = b.nr_seq_prescr
	and	b.nr_seq_item = c.nr_seq_item_sae
	and	c.nr_seq_item_escala = nr_seq_item_escala_p
	and	f.nr_seq_result_sae = b.nr_seq_result
	and	f.nr_seq_regra = c.nr_sequencia;
	
	select 	max (x.vl_result)
	into	ie_retorno_w
	from	pe_result_item x,
		pe_regra_result_escala f
	where	x.nr_sequencia = nr_seq_result_escala_w;
	

	
	return ie_retorno_w;
	end;

begin

select	max(a.cd_prescritor),
	max(a.nr_atendimento)
into	cd_pessoa_fisica_w,
	nr_atendimento_w
from	pe_prescricao a,
	pe_prescr_item_result b
where	a.nr_sequencia = b.nr_seq_prescr
and	a.nr_sequencia = nr_sequencia_p;

select	count(*)
into	qt_reg_w
from 	pe_prescricao a,
	pe_prescr_item_result b,
	pe_regra_item_escala c,
	pe_regra_result_escala d
where	a.nr_sequencia	= b.nr_seq_prescr
and	b.nr_seq_result = d.nr_seq_result_sae
and	b.nr_seq_item	= c.nr_seq_item_sae
and	a.nr_sequencia	= nr_sequencia_p
and	c.nr_seq_item_escala	is not null
and 	c.ie_escala = 12;

if 	(qt_reg_w > 0) then
	begin
		ie_ab_monit_padrao_w		:= obter_valor_escala(33);
		ie_ab_laboratorio_w		:= obter_valor_escala(34);
		ie_ab_medic_unica_w		:= obter_valor_escala(35);
		ie_ab_medic_endovenosa_w	:= obter_valor_escala(36);
		ie_ab_curativo_rotina_w		:= obter_valor_escala(37);
		ie_ab_curativo_frequente_w	:= obter_valor_escala(38);
		ie_ab_dreno_w			:= obter_valor_escala(39);
		ie_sv_vent_mecanica_w		:= obter_valor_escala(40);
		ie_sv_sup_vent_supl_w		:= obter_valor_escala(41);
		ie_sv_vias_aerias_w		:= obter_valor_escala(42);
		ie_sv_trat_funcao_pulmonar_w	:= obter_valor_escala(43);
		ie_sc_medic_vasoativa_unica_w	:= obter_valor_escala(44);
		ie_sc_medic_vasoativa_mult_w	:= obter_valor_escala(45);
		ie_sc_rep_volemica_w		:= obter_valor_escala(46);
		ie_sc_cateter_art_perif_w	:= obter_valor_escala(47);
		ie_sc_monit_atrio_esq_w		:= obter_valor_escala(48);
		ie_sc_via_venosa_central_w	:= obter_valor_escala(49);
		ie_sc_ressuscit_cardio_w	:= obter_valor_escala(50);
		ie_sr_tec_hemofiltracao_w	:= obter_valor_escala(51);
		ie_sr_med_debito_urina_w	:= obter_valor_escala(52);
		ie_sr_diurese_ativa_w		:= obter_valor_escala(53);
		ie_sn_pressao_intracraniana_w	:= obter_valor_escala(54);
		ie_sm_acidose_alcalose_w	:= obter_valor_escala(55);
		ie_sm_nut_parent_w		:= obter_valor_escala(57);
		ie_sm_nut_enteral_w		:= obter_valor_escala(58);
		ie_ie_inter_unica_uti_w		:= obter_valor_escala(59);
		ie_ie_inter_mult_uti_w		:= obter_valor_escala(60);
		ie_ie_inter_fora_uti_w		:= obter_valor_escala(62);
	end;
	
	insert into tiss_interv_terapeutica (nr_sequencia,
				cd_pessoa_fisica,
				dt_atualizacao,
				dt_avaliacao,
				ie_ab_monit_padrao,
				ie_ab_laboratorio,	
				ie_ab_medic_unica,	
				ie_ab_medic_endovenosa,
				ie_ab_curativo_rotina,
				ie_ab_curativo_frequente,
				ie_ab_dreno,
				ie_sv_vent_mecanica,	
				ie_sv_sup_vent_supl,	
				ie_sv_vias_aerias,
				ie_sv_trat_funcao_pulmonar,
				ie_sc_medic_vasoativa_unica,
				ie_sc_medic_vasoativa_mult,
				ie_sc_rep_volemica,
				ie_sc_cateter_art_perif,
				ie_sc_monit_atrio_esq,
				ie_sc_via_venosa_central,
				ie_sc_ressuscit_cardio,
				ie_sr_tec_hemofiltracao,
				ie_sr_med_debito_urina,
				ie_sr_diurese_ativa,
				ie_sn_pressao_intracraniana,
				ie_sm_acidose_alcalose,
				ie_sm_nut_parent,
				ie_sm_nut_enteral,
				ie_ie_inter_unica_uti,
				ie_ie_inter_mult_uti,
				ie_ie_inter_fora_uti,
				nm_usuario,
				nr_atendimento,
				ie_situacao,
				dt_liberacao,
				qt_pontuacao)
	values	(	tiss_interv_terapeutica_SEQ.nextval,
				cd_pessoa_fisica_w,
				sysdate,
				sysdate,
				nvl(ie_ab_monit_padrao_w,'N'),
				nvl(ie_ab_laboratorio_w,'N'),	
				nvl(ie_ab_medic_unica_w,'N'),
				nvl(ie_ab_medic_endovenosa_w,'N'),
				nvl(ie_ab_curativo_rotina_w,'N'),
				nvl(ie_ab_curativo_frequente_w,'N'),
				nvl(ie_ab_dreno_w,'N'),
				nvl(ie_sv_vent_mecanica_w,'N'),
				nvl(ie_sv_sup_vent_supl_w,'N'),	
				nvl(ie_sv_vias_aerias_w,'N'),
				nvl(ie_sv_trat_funcao_pulmonar_w,'N'),
				nvl(ie_sc_medic_vasoativa_unica_w,'N'),
				nvl(ie_sc_medic_vasoativa_mult_w,'N'),
				nvl(ie_sc_rep_volemica_w,'N'),
				nvl(ie_sc_cateter_art_perif_w,'N'),
				nvl(ie_sc_monit_atrio_esq_w,'N'),
				nvl(ie_sc_via_venosa_central_w,'N'),
				nvl(ie_sc_ressuscit_cardio_w,'N'),
				nvl(ie_sr_tec_hemofiltracao_w,'N'),
				nvl(ie_sr_med_debito_urina_w,'N'),
				nvl(ie_sr_diurese_ativa_w,'N'),
				nvl(ie_sn_pressao_intracraniana_w,'N'),
				nvl(ie_sm_acidose_alcalose_w,'N'),
				nvl(ie_sm_nut_parent_w,'N'),
				nvl(ie_sm_nut_enteral_w,'N'),
				nvl(ie_ie_inter_unica_uti_w,'N'),
				nvl(ie_ie_inter_mult_uti_w,'N'),
				nvl(ie_ie_inter_fora_uti_w,'N'),
				nm_usuario_p,
				nr_atendimento_w,
				'A',
				sysdate,
				0);
	
end if;	

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_tiss28_sae;
/