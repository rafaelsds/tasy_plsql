create or replace
procedure pcs_alterar_executor_atividade(
					nr_ordem_servico_p	number,
					cd_executor_p		Varchar2,
					nr_seq_motivo_pcs_p	number,
					nm_usuario_p		varchar2) is

nr_ordem_servico_w	man_ordem_servico.nr_sequencia%type;
nm_usuario_exec_w	usuario.nm_usuario%type;
nr_seq_agenda_w		pcs_agenda_atividades.nr_sequencia%type;


/*man_ordem_ativ_prev*/

nr_seq_ativ_prev_w	man_ordem_ativ_prev.nr_sequencia%type;
ds_atividade_w		man_ordem_ativ_prev.ds_atividade%type;
dt_prevista_w		man_ordem_ativ_prev.dt_prevista%type;
qt_min_prev_w		man_ordem_ativ_prev.qt_min_prev%type;
nm_usuario_prev_w	man_ordem_ativ_prev.nm_usuario_prev%type;
qt_existe_w		number(5);
nr_seq_grupo_trabalho_w man_ordem_ativ_prev.nr_seq_grupo_trabalho%type;
qt_pertence_grupo_trabalho_w	number(5);
nr_grupo_trabalho_w	man_ordem_servico.nr_grupo_trabalho%type;


/*Cursor C01 is
	select	ds_atividade,
	        dt_prevista,
		qt_min_prev,
		nm_usuario_prev,
		nr_seq_grupo_trabalho
	from	man_ordem_ativ_prev
	where	nr_seq_ordem_serv = nr_ordem_servico_p
	and	nm_usuario_prev <> nm_usuario_exec_w;*/

begin

select	man_ordem_servico_seq.nextval
into	nr_ordem_servico_w
from	dual;

if	(cd_executor_p is not null) then
	select	max(nm_usuario)
	into	nm_usuario_exec_w
	from	usuario
	where	cd_pessoa_fisica = cd_executor_p
	and	ie_situacao = 'A';
	
	if (nm_usuario_exec_w is not null) then

		select	nr_grupo_trabalho
		into	nr_grupo_trabalho_w
		from	man_ordem_servico
		where	nr_sequencia = nr_ordem_servico_p;
		
		select	count(*)
		into	qt_pertence_grupo_trabalho_w
		from	usuario
		where	nm_usuario in (select nm_usuario_param
					from man_grupo_trab_usuario
					where nr_seq_grupo_trab = nr_grupo_trabalho_w)
					and ie_situacao = 'A'
					and nm_usuario = nm_usuario_exec_w;
					
		if (qt_pertence_grupo_trabalho_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(350481);
		end if;

	end if;
end if;

insert	into man_ordem_servico(
		nr_sequencia,
		nr_seq_localizacao,
		nr_seq_equipamento,
		cd_pessoa_solicitante,
		dt_ordem_servico,
		ie_prioridade,
		ie_parado,
		ds_dano_breve,
		dt_atualizacao,
		nm_usuario,
		dt_inicio_desejado,
		dt_conclusao_desejada,
		ds_dano,
		dt_inicio_previsto,
		dt_fim_previsto,
		dt_inicio_real,
		dt_fim_real,
		ie_tipo_ordem,
		ie_status_ordem,
		nr_grupo_planej,
		nr_grupo_trabalho,
		nr_seq_tipo_solucao,
		ds_solucao,
		nm_usuario_exec,
		qt_contador,
		nr_seq_planej,
		nr_seq_tipo_contador,
		nr_seq_estagio,
		cd_projeto,
		nr_seq_etapa_proj,
		dt_reabertura,
		cd_funcao,
		nm_tabela,
		ie_classificacao,
		nr_seq_origem,
		nr_seq_projeto,
		ie_grau_satisfacao,
		nr_seq_indicador,
		nr_seq_causa_dano,
		ie_forma_receb,
		nr_seq_cliente,
		nr_seq_grupo_des,
		nr_seq_grupo_sup,
		nr_seq_superior,
		ie_eficacia,
		dt_prev_eficacia,
		cd_pf_eficacia,
		nr_seq_nao_conform,
		nr_seq_complex,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_obriga_news,
		nr_seq_meta_pe,
		nr_seq_classif,
		nr_seq_nivel_valor,
		nm_usuario_lib_news,
		dt_libera_news,
		dt_envio_wheb,
		ds_contato_solicitante,
		ie_prioridade_desen,
		ie_prioridade_sup,
		ie_origem_os,
		nr_seq_regional,
		nr_seq_atividade,
		nr_Seq_grupamento,
		ie_origem_pcs,
		nr_seq_regra_ativ,
		nr_seq_lista)
	select	nr_ordem_servico_w,
		nr_seq_localizacao,
		nr_seq_equipamento,
		cd_executor_p,
		dt_ordem_servico,
		ie_prioridade,
		ie_parado,
		ds_dano_breve,
		sysdate,
		nm_usuario_p,
		dt_inicio_desejado,
		dt_conclusao_desejada,
		ds_dano,
		dt_inicio_previsto,
		dt_fim_previsto,
		dt_inicio_real,
		dt_fim_real,
		ie_tipo_ordem,
		ie_status_ordem,
		nr_grupo_planej,
		nr_grupo_trabalho,
		nr_seq_tipo_solucao,
		ds_solucao,
		nm_usuario_exec_w,
		qt_contador,
		nr_seq_planej,
		nr_seq_tipo_contador,
		nr_seq_estagio,
		cd_projeto,
		nr_seq_etapa_proj,
		dt_reabertura,
		cd_funcao,
		nm_tabela,
		ie_classificacao,
		nr_ordem_servico_p,
		nr_seq_projeto,
		ie_grau_satisfacao,
		nr_seq_indicador,
		nr_seq_causa_dano,
		ie_forma_receb,
		nr_seq_cliente,
		nr_seq_grupo_des,
		nr_seq_grupo_sup,
		nr_seq_superior,
		ie_eficacia,
		dt_prev_eficacia,
		cd_pf_eficacia,
		nr_seq_nao_conform,
		nr_seq_complex,
		sysdate,
		nm_usuario_p,
		ie_obriga_news,
		nr_seq_meta_pe,
		nr_seq_classif,
		nr_seq_nivel_valor,
		nm_usuario_lib_news,
		dt_libera_news,
		dt_envio_wheb,
		ds_contato_solicitante,
		ie_prioridade_desen,
		ie_prioridade_sup,
		ie_origem_os,
		nr_seq_regional,
		nr_seq_atividade,
		nr_Seq_grupamento,
		ie_origem_pcs,
		nr_seq_regra_ativ,
		nr_seq_lista
	from	man_ordem_servico
	where	nr_sequencia = nr_ordem_servico_p;

	commit;

	select 	count(*)
	into	qt_existe_w
	from	man_ordem_servico_exec
	where	nr_seq_ordem = nr_ordem_Servico_w
	and	nm_usuario_exec = nm_usuario_exec_w;

	if	(qt_existe_w = 0) then
		insert into  man_ordem_servico_exec(
				nr_sequencia,
				nr_seq_ordem,
				dt_atualizacao,
				nm_usuario,
				nm_usuario_exec)
			values(	man_ordem_servico_exec_seq.nextval,
				nr_ordem_Servico_w,
				sysdate,
				nm_usuario_p,
				nm_usuario_exec_w);
	end if;		
	
	
	select	count(*)
	into	qt_existe_w
	from	man_ordem_ativ_prev
	where	nr_seq_ordem_serv = nr_ordem_servico_p;
	
	if (qt_existe_w > 0) then
		select	man_ordem_ativ_prev_seq.nextval
		into	nr_seq_ativ_prev_w
		from   	dual;
		
		insert into man_ordem_ativ_prev (	
			nr_sequencia,
			nr_seq_ordem_serv,
			dt_atualizacao,
			nm_usuario,
			ds_atividade,
			dt_prevista,
			qt_min_prev,
			nm_usuario_prev,
			nr_seq_grupo_trabalho)
		select 	nr_seq_ativ_prev_w,
			nr_ordem_servico_w,
			sysdate,
			nm_usuario_p,
			ds_atividade,
			dt_prevista,
			qt_min_prev,
			nm_usuario_exec_w,
			nr_seq_grupo_trabalho
		from	man_ordem_ativ_prev
		where	nr_seq_ordem_serv = nr_ordem_servico_p
		and 	rownum = 1;
		
		commit;
	end if;
	
/*	open C01;
	loop
	fetch C01 into	
		ds_atividade_w,
	        dt_prevista_w,
		qt_min_prev_w,
		nm_usuario_prev_w,
		nr_seq_grupo_trabalho_w;
	exit when C01%notfound;
		begin

		select	man_ordem_ativ_prev_seq.nextval
		into	nr_seq_ativ_prev_w
		from   	dual;
		
		insert into man_ordem_ativ_prev (	
			nr_sequencia,
			nr_seq_ordem_serv,
			dt_atualizacao,
			nm_usuario,
			ds_atividade,
			dt_prevista,
			qt_min_prev,
			nm_usuario_prev,
			nr_seq_grupo_trabalho)
		values(	nr_seq_ativ_prev_w,
			nr_ordem_servico_w,
			sysdate,
			nm_usuario_p,
			ds_atividade_w,
			dt_prevista_w,
			qt_min_prev_w,
			nm_usuario_prev_w,
			nr_seq_grupo_trabalho_w);		
			
			
		commit;
		end;
	end loop;
	close C01;*/
	
	update	man_ordem_servico
	set	nr_seq_motivo_pcs = nr_seq_motivo_pcs_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_status_ordem = 3
	where	nr_sequencia = nr_ordem_Servico_p;
	
	begin
	
	select	nr_sequencia
	into	nr_seq_agenda_w
	from 	pcs_agenda_atividades
	where	nr_ordem_servico = nr_ordem_Servico_p;
	exception
	when others then
		nr_seq_agenda_w := 0;
	end;	

if (nr_seq_agenda_w > 0) then
	update	pcs_agenda_atividades
	set	nr_ordem_servico = nr_ordem_servico_w
	where	nr_sequencia = nr_seq_agenda_w;
end if;

commit;

end pcs_alterar_executor_atividade;
/