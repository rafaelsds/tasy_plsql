create or replace
procedure proj_log_ultimo_recalc (
	nr_seq_cronograma_p	proj_cronograma.nr_sequencia%type,
	ie_origem_p		varchar2,
	ie_operacao_p		varchar2,
	nm_usuario_p		usuario.nm_usuario%type) is

/*
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Objective: Generate a log that shows the last time a user changed a
		data that needs the schedule recalculation
	------------------------------------------------------------------------
	Direct call: 
	[ X ] Dictionary objects
	[   ] Tasy (Delphi/Java/HTML5)
	[   ] Portal
	[   ] Reports
	[   ] Others:
	 -----------------------------------------------------------------------
	Attention points: This object is called within a trigger
		DO NOT COMMIT ON THIS OBJECT
	------------------------------------------------------------------------
	References:
		Tables: N/A
		Objects: N/A
	------------------------------------------------------------------------
	Called in:
		Java: N/A
		HTML5: N/A
		Object:	PROJ_CRON_EXCESSAO_AFTER,
			PROJ_CRON_CALENDARIO_AFTER
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

begin

if	(nr_seq_cronograma_p is not null) then

	insert into proj_cron_log_alt (
		dt_atualizacao,
		dt_atualizacao_nrec,
		ie_operacao,
		ie_origem,
		ie_status,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_cronograma,
		nr_sequencia)
	values (sysdate,
		sysdate,
		ie_operacao_p,
		ie_origem_p,
		'P',
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_cronograma_p,
		proj_cron_log_alt_seq.nextval);

end if;

end proj_log_ultimo_recalc;
/
