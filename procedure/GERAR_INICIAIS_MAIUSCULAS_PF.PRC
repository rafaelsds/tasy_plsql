create or replace
procedure gerar_iniciais_maiusculas_pf is

cursor c01 is
select	cd_pessoa_fisica,
	nm_pessoa_fisica
from	pessoa_fisica
where	nvl(length(nm_pessoa_fisica),0) > 4
order by
	cd_pessoa_fisica;

cd_pessoa_fisica_w	varchar2(10);
nm_pessoa_fisica_w	varchar2(60);
qt_registro_w		number(5,0);

begin
open c01;
loop
fetch c01 into	cd_pessoa_fisica_w,
			nm_pessoa_fisica_w;
exit when c01%notfound;
	begin
	nm_pessoa_fisica_w := initcap(nm_pessoa_fisica_w);
	nm_pessoa_fisica_w := replace(nm_pessoa_fisica_w,' De ', ' de ');
	nm_pessoa_fisica_w := replace(nm_pessoa_fisica_w,' Da ', ' da ');
	nm_pessoa_fisica_w := replace(nm_pessoa_fisica_w,' Das ', ' das ');
	nm_pessoa_fisica_w := replace(nm_pessoa_fisica_w,' Do ', ' do ');
	nm_pessoa_fisica_w := replace(nm_pessoa_fisica_w,' Dos ', ' dos ');

	update 	pessoa_fisica
	set		nm_pessoa_fisica = nm_pessoa_fisica_w
	where		cd_pessoa_fisica = cd_pessoa_fisica_w;

	if	(qt_registro_w = 10000) then
		commit;
		qt_registro_w := 0;
	else
		qt_registro_w := qt_registro_w + 1;
	end if;
	end;
end loop;
close c01;

commit;

end gerar_iniciais_maiusculas_pf;
/