create or replace
procedure ctb_reorganizar_agrup_movto(
        cd_empresa_p            empresa.cd_empresa%type,
        cd_estab_p              estabelecimento.cd_estabelecimento%type,
        dt_inicial_p            date,
        dt_final_p              date,
        nm_usuario_p            varchar2,
        ie_considera_compl_p    varchar2,
        ie_lancamento_lote_p    varchar2 default 'N',
	    ie_considera_part_p	    varchar2 default 'N',
        nr_lancamento_p         in out ctb_movimento.nr_agrup_sequencial%type) is

qt_commit_w                     number(10);
nr_lancamento_w                 ctb_movimento.nr_agrup_sequencial%type;
nr_lanc_aux_w                   ctb_movimento.nr_agrup_sequencial%type;
ie_tem_contrapartida_w          varchar2(1) := 'N';
i                               integer    	:= 0;
ind 				integer		:= 0;
dt_inicio_dia_w                 date;
dt_final_dia_w                  date;
ie_debito_credito_partida_w     varchar2(1);
ie_debito_credito_w             varchar2(1);
ds_chave_w                      varchar2(255);
vl_movimento_w                  number(15,2);
nr_pos_ini_w                    integer;
nr_pos_fim_w                    integer;
vl_debito_w                     ctb_movimento.vl_movimento%type;
vl_credito_w                    ctb_movimento.vl_movimento%type;
vl_diferenca_w                  ctb_movimento.vl_movimento%type;
/* variaveis do procedimento de ajuste final */
cd_historico_atual_w            number(10);
qt_contador_w                   number(10);
ds_status_proc_longo_w          varchar2(100);
qt_meses_w                      number(10);
/*
Situacoes para identificacao da contra-partida
1 - Movimentos de partida
2 - Movimentos de contra-partida
3 - Movimentos de agrupamento
4 - Outros (Compl historico)
*/

/*
===============================================================================
Objetivo
    Percorrer todos os meses da contabilidade do periodo selecionado

Parametros
    CD_EMPRESA_PC
        Sequencia da empresa que serao reorganizados os movimentos

    DT_INICIAL_PC
        Data do primeiro mes que serao reorganizados os movimentos, deve
        estar truncada para horas minutos e segundos
    DT_FINAL_PC
        Data do ultimo mes que serao reorganizados os movimentos

Retorno
    NR_SEQ_MES_REF
        Sequencia do mes da contabilidade
    DT_REFERENCIA
        Data de referencia do mes da contabilidade
===============================================================================
*/
cursor c01(
    cd_empresa_pc in ctb_mes_ref.cd_empresa%type,
    dt_inicial_pc in ctb_mes_ref.dt_referencia%type,
    dt_final_pc in ctb_mes_ref.dt_referencia%type
    ) is
    select  a.nr_sequencia nr_seq_mes_ref,
            a.dt_referencia
    from    ctb_mes_ref a
    where   a.cd_empresa = cd_empresa_pc
    and     a.dt_referencia between dt_inicial_pc and dt_final_pc
    order by
            a.dt_referencia;

type t_c01 is table of c01%rowtype index by pls_integer;
l_c01 t_c01;

/*
===============================================================================
Objetivo
    Retornar a dada inicial e final para cada dia do mes que possui movimentos

Parametros
    NR_SEQ_MES_REF_PC
        Sequencia do mes de referencia da CTB_MES_REF, usada
        para restringir a quantidade de informacoes da CTB_MOVIMENTO, por
        questao de performance e para considerar apenas movimentos do mes

    DT_REFERENCIA_PC
        Data de referencia para os movimentos do mes, sera usada
        para montar a data inicial e final para consulta dos movimentos

Retorno
    DT_INICIO_DIA
        Data inicial dos movimentos do dia no formato dd/mm/yyyy 00:00:00

    DT_FINAL_DIA
        Data final dos movimentos do dia no formato dd/mm/yyyy 23:59:59
===============================================================================
*/
cursor c02(
    nr_seq_mes_ref_pc in ctb_movimento.nr_seq_mes_ref%type,
    dt_referencia_pc in ctb_movimento.dt_movimento%type
    ) is
    select  dt_inicio_dia,
            dt_final_dia
    from (
        select  trunc(dt_referencia_pc + (level-1)) dt_inicio_dia,
                fim_dia(dt_referencia_pc + (level-1)) dt_final_dia
        from    dual
        connect by
                level <= extract(day from last_day(dt_referencia_pc))
    ) x
    where exists (
                select  1
                from    ctb_movimento a
                where   a.nr_seq_mes_ref = nr_seq_mes_ref_pc
                and     a.dt_movimento between x.dt_inicio_dia and x.dt_final_dia
                );

type t_c02 is table of c02%rowtype index by pls_integer;
l_c02 t_c02;

/* Percorrer todos os movimentos do mes de referencia entre as datas(inicio e fim do dia, ou seja todos os movimentos do dia) */
cursor c03(nr_seq_mes_ref_pc 	ctb_mes_ref.nr_sequencia%type,
	   dt_inicio_dia_pc	ctb_movimento.dt_movimento%type,
	   dt_final_dia_pc	ctb_movimento.dt_movimento%type) is
    select    a.nr_sequencia,
        a.dt_movimento,
        a.nr_agrup_sequencial,
        a.cd_conta_debito,
        a.cd_conta_credito,
        a.nr_seq_agrupamento,
        a.nr_lote_contabil,
        a.nr_seq_movto_partida,
        a.cd_historico,
        a.ds_compl_historico,
        a.vl_movimento
    from    ctb_movimento a
    where    a.nr_seq_mes_ref    = nr_seq_mes_ref_pc
    and    a.nr_agrup_sequencial    = 0
    and    a.dt_movimento between dt_inicio_dia_pc and dt_final_dia_pc
    order by
        a.dt_movimento,
        a.nr_lote_contabil,
        a.nr_sequencia;

c03_w    c03%rowtype;
type c03_type is table of c03%rowtype;
c03_regs_w c03_type;

/* Percorrer todos os numeros de agrumento do mes de referencia selecionado dentro do periodo informado,
que tenham numero de agrupamento e a soma de debito e credito agrupando por numero de agrupamento seja diferente */
cursor c04(nr_seq_mes_ref_pc 	ctb_mes_ref.nr_sequencia%type,
	   dt_inicio_dia_pc	ctb_movimento_v2.dt_movimento%type,
	   dt_final_dia_pc	ctb_movimento_v2.dt_movimento%type) is -- C_AJUSTE_MOVTO
    select    y.nr_seq_agrupamento
    from    ctb_movimento_v2 y
    where    y.nr_seq_mes_ref    = nr_seq_mes_ref_pc
    and    y.nr_seq_agrupamento    <> 0
    and    y.dt_movimento between dt_inicio_dia_pc and dt_final_dia_pc
    having    (nvl(sum(y.vl_debito),0) <> nvl(sum(y.vl_credito),0))
    group by
        y.nr_seq_agrupamento;

c04_w    c04%rowtype;

/*Somente agrupadores com diferenca */
cursor c05(nr_seq_mes_ref_pc 	ctb_mes_ref.nr_sequencia%type,
	   dt_inicio_dia_pc	ctb_movimento.dt_movimento%type,
	   dt_final_dia_pc	ctb_movimento.dt_movimento%type) is -- C_AJUSTE_AGRUPADOR
    select    a.nr_sequencia,
        a.dt_movimento,
        a.nr_agrup_sequencial,
        a.cd_conta_debito,
        a.cd_conta_credito,
        a.nr_seq_agrupamento,
        a.nr_lote_contabil,
        a.nr_seq_movto_partida,
        a.cd_historico,
        substr(trim(replace(replace(a.ds_compl_historico,chr(13),''),chr(10),'')),1,255) ds_compl_historico,
        a.vl_movimento
    from    ctb_movimento a,
        lote_contabil b
    where    b.nr_lote_contabil    = a.nr_lote_contabil
    and    b.nr_seq_mes_ref    = nr_seq_mes_ref_pc
    and    a.nr_seq_agrupamento    = c04_w.nr_seq_agrupamento
    and    a.dt_movimento between dt_inicio_dia_pc and dt_final_dia_pc
    and    a.nr_agrup_sequencial    <> 0
    order by
        a.dt_movimento,
        a.nr_lote_contabil,
        a.nr_sequencia;

c05_w    c05%rowtype;

cursor c09 is -- C_PARTIDA_HISTORICO
    select    a.nr_sequencia
    from    ctb_movimento a
    where    nr_lote_contabil    = c03_w.nr_lote_contabil
    and    ds_compl_historico    = c03_w.ds_compl_historico
    and    dt_movimento between dt_inicio_dia_w and dt_final_dia_w
    and    coalesce(nr_agrup_sequencial,0)  = 0
    and    nr_sequencia    != c03_w.nr_sequencia
    union
    select    a.nr_sequencia
    from    ctb_movimento_v a
    where    nr_lote_contabil    = c03_w.nr_lote_contabil
    and    ds_compl_historico like '%' || ds_chave_w || '%'
    and    coalesce(ds_chave_w,'0') != '0'
    and    dt_movimento between dt_inicio_dia_w and dt_final_dia_w
    and    nr_sequencia != c03_w.nr_sequencia
    and    coalesce(nr_agrup_sequencial,0)  = 0;

c09_w    c09%rowtype;

begin
/* ****************************************************************************
CONSISTENCIAS
 **************************************************************************** */
/* A data de inicio esta maior que a data final! Favor verificar */
if    (dt_inicial_p > dt_final_p)then
    wheb_mensagem_pck.exibir_mensagem_abort(294888);
end if;

/* ****************************************************************************
LOG CARREGANDO INFORMACOES
 **************************************************************************** */
/* Carregando informacoes */
ds_status_proc_longo_w    := wheb_mensagem_pck.get_texto(299429);
gravar_processo_longo(ds_status_proc_longo_w ,'CTB_REORGANIZAR_AGRUP_MOVTO',qt_contador_w);

/* ****************************************************************************
INICIALIZAR VARIAVEIS
 **************************************************************************** */
/* Quantidade de meses para LOG */
qt_meses_w    := obter_meses_entre_datas_util(dt_inicial_p, dt_final_p);
/* Carregar ultimo numero de lancamento */
nr_lancamento_w    := nvl(nr_lancamento_p, 0);
/* Contador de registros */
qt_contador_w    := 0;
/* Atualizando */
ds_status_proc_longo_w    := wheb_mensagem_pck.get_texto(331829) || ': ';
/* ****************************************************************************
INICIO DA ROTINA
Percorrer todos os meses do periodo selecionado
 **************************************************************************** */

if    (trunc(dt_inicial_p,'mm') = trunc(dt_inicial_p,'yyyy')) then
    nr_lancamento_w    := 0;
else
    if  (nvl(nr_lancamento_p,0) = 0) then
        select  nvl(max(a.nr_agrup_sequencial),0)
        into    nr_lancamento_w
        from    ctb_mes_ref b,
            ctb_movimento a
        where   b.nr_sequencia  = a.nr_seq_mes_ref
        and     b.cd_empresa    = cd_empresa_p
        and     b.dt_referencia = trunc(add_months(dt_inicial_p,-1),'mm');
    end if;
end if;

/* Tratamento para nao disparar a trigger CTB_MOVIMENTO_ATUAL, da CTB_MOVIMENTO */
philips_contabil_pck.set_ie_consistindo_lote('S');

begin
    /* Tratamento para nao disparar a trigger CTB_MOVIMENTO_ATUAL, da CTB_MOVIMENTO */
    philips_contabil_pck.set_ie_consistindo_lote('S');
    open c01(
        cd_empresa_pc => cd_empresa_p,
        dt_inicial_pc => dt_inicial_p,
        dt_final_pc => dt_final_p
        );
    loop fetch c01 bulk collect into l_c01;
    exit when l_c01.count = 0;
        for ind in 1..l_c01.count loop
            qt_contador_w     := qt_contador_w + 1;
            gravar_processo_longo(ds_status_proc_longo_w || l_c01(ind).dt_referencia || ' ('|| qt_contador_w ||'/' || qt_meses_w || ')','CTB_REORGANIZAR_AGRUP_MOVTO',qt_contador_w);
            open c02(
                nr_seq_mes_ref_pc => l_c01(ind).nr_seq_mes_ref,
                dt_referencia_pc => l_c01(ind).dt_referencia
                );
            loop fetch c02 bulk collect into l_c02;
            exit when l_c02.count = 0;
                for l in 1..l_c02.count loop
                    /*
                    ===============================================================================
                    Tratamento para nr_seq_movto_partida - partida simples e dobrada
                    ===============================================================================
                    */
			if (ie_considera_part_p =  'S') then				
				merge into ctb_movimento a
				using (
					select  x.nr_sequencia,
						nr_lancamento_w + (dense_rank() over (order by  x.dt_primeiro_movto,x.nr_lote_contabil,x.nr_seq_movto_agrup)) nr_agrup_sequencial
					from    (
						select  b.nr_sequencia,
							b.nr_lote_contabil,
							nvl(b.nr_seq_movto_partida,b.nr_sequencia) nr_seq_movto_agrup,
							first_value(b.dt_movimento) over (partition by nvl(b.nr_seq_movto_partida,b.nr_sequencia) order by b.dt_movimento) dt_primeiro_movto
						from    ctb_movimento b
						where   b.nr_seq_mes_ref = l_c01(ind).nr_seq_mes_ref
						and     b.dt_movimento between l_c02(l).dt_inicio_dia and l_c02(l).dt_final_dia
					) x
				)	 y
				on (y.nr_sequencia = a.nr_sequencia)
				when matched then
				update
				set a.nr_agrup_sequencial = y.nr_agrup_sequencial;
				commit;

				/* Obter o ultimo numero de agrupador do mes */
				select  max(nr_agrup_sequencial)
				into    nr_lancamento_w
				from    ctb_movimento a
				where   a.nr_seq_mes_ref = l_c01(ind).nr_seq_mes_ref
				and     a.dt_movimento between l_c02(l).dt_inicio_dia and l_c02(l).dt_final_dia;
			else
				dt_inicio_dia_w        := trunc(l_c02(l).dt_inicio_dia);
				dt_final_dia_w        := fim_dia(l_c02(l).dt_final_dia);
				if    (nvl(ie_lancamento_lote_p,'N') = 'S') then
					begin
					-- VERIFICAR VERIFICAR VERIFICAR VERIFICAR VERIFICAR VERIFICAR VERIFICAR
					ctb_definir_agrup_dia_lote(cd_empresa_p, l_c01(ind).nr_seq_mes_ref, l_c02(l).dt_inicio_dia, nm_usuario_p);
					end;
				end if;

				/* ****************************************************************************
				INICIO PARTIDA SIMPLES e DOBRADA
				PARTIDA SIMPLES - Conta debito e credito no mesmo registro
				PARTIDA DOBRADA - Dois ou mais movimentos
				**************************************************************************** */				
				open c03(l_c01(ind).nr_seq_mes_ref, dt_inicio_dia_w, dt_final_dia_w);
				loop
				fetch c03 bulk collect into c03_regs_w limit 3000;
					for i in 1 .. c03_regs_w.count
					loop
					/* Se a conta de debido e credito estiverem informadas no movimento entao e PARTIDA SIMPLES */
					if    (nvl(c03_regs_w(i).cd_conta_debito,'X') <> 'X') and
						(nvl(c03_regs_w(i).cd_conta_credito,'X') <> 'X') then
						begin
						nr_lancamento_w            := nr_lancamento_w + 1;
						update    ctb_movimento
						set    nr_agrup_sequencial    = nr_lancamento_w
						where    nr_sequencia        = c03_regs_w(i).nr_sequencia
						and    nr_agrup_sequencial    = 0;
						end;
					else
						begin
						/* Se a conta de debido e credito NAO estiverem informadas no movimento entao eh PARTIDA DOBRADA */
						nr_lanc_aux_w := nr_lancamento_w + 1;
						update    ctb_movimento a
						set    nr_agrup_sequencial    = nr_lanc_aux_w
						where    nr_seq_movto_partida     = c03_regs_w(i).nr_sequencia
						and    nr_agrup_sequencial    = 0;

						if    (sql%rowcount > 0) then
							begin
							/* Se atualizou algum registro entao atualiza o registro pai */
							nr_lancamento_w := nr_lanc_aux_w;
							update    ctb_movimento
							set    nr_agrup_sequencial    = nr_lancamento_w
							where    nr_sequencia        = c03_regs_w(i).nr_sequencia;
							end;
						end if;

						if    (nvl(c03_w.nr_seq_movto_partida,0) > 0) then
							begin
							nr_lanc_aux_w := nr_lancamento_w + 1;
							update    ctb_movimento a
							set    nr_agrup_sequencial    = nr_lanc_aux_w
							where    a.nr_seq_movto_partida    = c03_regs_w(i).nr_seq_movto_partida
							and    a.nr_lote_contabil    = c03_regs_w(i).nr_lote_contabil
							and    a.nr_agrup_sequencial    = 0;

							if    (sql%rowcount > 0) then
								begin
								nr_lancamento_w := nr_lanc_aux_w;
								update    ctb_movimento
								set    nr_agrup_sequencial    = nr_lancamento_w
								where    nr_sequencia        = c03_regs_w(i).nr_seq_movto_partida;
								end;
							end if;
							end;
						end if;
						end;
					end if;
					end loop;
					commit;
				exit when c03%notfound;
				end loop;
				commit;
				close c03;
				/* ****************************************************************************
				FIM PARTIDA SIMPLES e DOBRADA
				**************************************************************************** */

				merge into ctb_movimento a
				using    (
					select    x.nr_seq_agrupamento,
						x.nr_lote_contabil,
						rownum + nr_lancamento_w rn
					from (
						select    b.nr_seq_agrupamento,
							min(b.dt_movimento) keep (dense_rank first order by b.dt_movimento,b.nr_lote_contabil,b.nr_sequencia) dt_movimento,
							--min(b.nr_lote_contabil) keep (dense_rank first order by b.dt_movimento,b.nr_lote_contabil,b.nr_sequencia) nr_lote_contabil,
							b.nr_lote_contabil,
							min(b.nr_sequencia) keep (dense_rank first order by b.dt_movimento,b.nr_lote_contabil,b.nr_sequencia) nr_sequencia
						from    ctb_movimento b
						where    b.nr_seq_mes_ref = l_c01(ind).nr_seq_mes_ref
						and    b.dt_movimento between dt_inicio_dia_w and dt_final_dia_w
						and    b.nr_agrup_sequencial    = 0
						having    count(*) > 1
						group by
							b.nr_seq_agrupamento,
							b.nr_lote_contabil
						order by
							2,3,4
						) x
					) y
				on    (y.nr_seq_agrupamento = a.nr_seq_agrupamento and y.nr_lote_contabil = a.nr_lote_contabil)
				when matched then
				update
				set    a.nr_agrup_sequencial    = y.rn
				where    a.nr_seq_mes_ref    = l_c01(ind).nr_seq_mes_ref
				and    a.dt_movimento between dt_inicio_dia_w and dt_final_dia_w
				and    a.nr_agrup_sequencial    = 0;

				select    max(nr_agrup_sequencial)
				into    nr_lancamento_w
				from    ctb_movimento a
				where    a.nr_seq_mes_ref = l_c01(ind).nr_seq_mes_ref;
				/* ****************************************************************************
				FIM MOVIMENTOS COM AGRUPADOR
				**************************************************************************** */
				if    (ie_considera_compl_p = 'S') then
					/* ****************************************************************************
					INICIO AJUSTAR AGRUPADOR
					**************************************************************************** */
					begin
					open c04(l_c01(ind).nr_seq_mes_ref, dt_inicio_dia_w, dt_final_dia_w);
					loop
					fetch c04 into
						c04_w;
					exit when c04%notfound;
						begin
						open c05(l_c01(ind).nr_seq_mes_ref, dt_inicio_dia_w, dt_final_dia_w);
						loop
						fetch c05 into
							c05_w;
						exit when c05%notfound;
							begin
							qt_commit_w        := qt_commit_w + 1;

							update    ctb_movimento a
							set    a.nr_agrup_sequencial    = c05_w.nr_agrup_sequencial
							where    a.nr_lote_contabil    = c05_w.nr_lote_contabil
							and    a.dt_movimento between dt_inicio_dia_w and dt_final_dia_w
							and    substr(trim(replace(replace(a.ds_compl_historico,chr(13),''),chr(10),'')),1,255) = c05_w.ds_compl_historico
							and    nr_agrup_sequencial    = 0;
							end;
						end loop;
						close c05;
						end;
					end loop;
					close c04;
					end;
					/* ****************************************************************************
					FIM AJUSTAR AGRUPADOR
					**************************************************************************** */
					/* ****************************************************************************
					INICIO MOVIMENTOS COMPLEMENTO DE HISTORICO
					**************************************************************************** */
					begin
					open c03(l_c01(ind).nr_seq_mes_ref, dt_inicio_dia_w, dt_final_dia_w);
					loop
					fetch c03 into
						c03_w;
					exit when c03%notfound;
						begin
						qt_commit_w        := qt_commit_w + 1;

						/*INICIO Contrapartidas por complemento de historico*/
						if    (nvl(c03_w.cd_conta_debito,'X') <> 'X') then
							ie_debito_credito_partida_w    := 'D';
						elsif    (nvl(c03_w.cd_conta_credito,'X') <> 'X') then
							ie_debito_credito_partida_w    := 'C';
						end if;

						if    (ie_debito_credito_partida_w = 'D') then
							ie_debito_credito_w    := 'C';
						else
							ie_debito_credito_w    := 'D';
						end if;

						nr_pos_ini_w    := nvl(instr(c03_w.ds_compl_historico,'('),0);
						nr_pos_fim_w    := nvl(instr(c03_w.ds_compl_historico,')'),0);
						ds_chave_w    := null;

						if    (nr_pos_ini_w >0) and
							(nr_pos_fim_w > 0) then
							begin
							ds_chave_w    := substr(c03_w.ds_compl_historico, nr_pos_ini_w, (nr_pos_fim_w - nr_pos_ini_w)+1);
							end;
						elsif    (nr_pos_ini_w = 0) and
							(nr_pos_fim_w = 0) then
							begin
							nr_pos_ini_w    := 1;
							nr_pos_fim_w    := nvl(instr(c03_w.ds_compl_historico,','),0);
							ds_chave_w    := substr(c03_w.ds_compl_historico, nr_pos_ini_w, (nr_pos_fim_w - nr_pos_ini_w)+1);

							if    (campo_numerico(ds_chave_w) = 0) then
								begin
								nr_pos_fim_w    := nvl(instr(c03_w.ds_compl_historico,' '),0);
								ds_chave_w    := substr(c03_w.ds_compl_historico, nr_pos_ini_w, (nr_pos_fim_w - nr_pos_ini_w)+1);
								if    (campo_numerico(ds_chave_w) = 0) then
									ds_chave_w    := null;
								end if;
								end;
							end if;
							end;
						end if;
				    
						nr_lanc_aux_w    := null;
						i    := 0;
						open c09;
						loop
						fetch c09 into
							c09_w;
						exit when c09%notfound;
							begin
							i    := i + 1;
							if    (i = 1) and
								(nvl(nr_lanc_aux_w,0)  = 0) then
								nr_lancamento_w            := nr_lancamento_w + 1;
								update    ctb_movimento
								set    nr_agrup_sequencial     = nr_lancamento_w
								where    nr_sequencia         = c03_w.nr_sequencia
								and        coalesce(nr_agrup_sequencial,0)  = 0;
							end if;
							if    (nvl(nr_lanc_aux_w,0) != 0) then
								update    ctb_movimento
								set    nr_agrup_sequencial     = nr_lanc_aux_w
								where    nr_sequencia         = c09_w.nr_sequencia
								and    coalesce(nr_agrup_sequencial,0)  = 0;
							else
								update    ctb_movimento
								set    nr_agrup_sequencial     = nr_lancamento_w
								where    nr_sequencia         = c09_w.nr_sequencia
								and    coalesce(nr_agrup_sequencial,0)  = 0;
							end if;
							end;
						end loop;
						close c09;

						/*FIM Contrapartidas por complemento de historico*/

						if    (qt_commit_w >= 2000) then
							commit;
							qt_commit_w    := 0;
						end if;
						end;
					end loop;
					close c03;
					end;
				end if;
				/* ****************************************************************************
				FIM MOVIMENTOS COMPLEMENTO DE HISTORICO
				**************************************************************************** */
				--end;
			end if;
		end loop; --end loop l_c02
            end loop; -- end loop c02
            close c02;

            /* Atualizar a data de reorganizacao dos lancamentos no mes da contabilidade */
            update  ctb_mes_ref a
            set     a.dt_reorg_lancto = sysdate
            where   a.nr_sequencia    = l_c01(ind).nr_seq_mes_ref;
            commit;
        end loop; -- end loop l_c01
    end loop; -- end loop c01
    close c01;

    /* Tratamento para voltar a disparar a trigger CTB_MOVIMENTO_ATUAL */
    philips_contabil_pck.set_ie_consistindo_lote('N');
exception
when others then
    /* Tratamento para voltar a disparar a trigger CTB_MOVIMENTO_ATUAL, da CTB_MOVIMENTO em caso de erros */
    philips_contabil_pck.set_ie_consistindo_lote('N');
    raise;
end;
nr_lancamento_p := nr_lancamento_w;
commit;

end ctb_reorganizar_agrup_movto;
/