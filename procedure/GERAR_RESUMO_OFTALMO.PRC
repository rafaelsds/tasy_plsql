create or replace
procedure gerar_resumo_oftalmo(
		cd_pessoa_fisica_p	varchar2,
		nm_procedure_p		varchar2,
		ds_fundoscopia_p	varchar2,
		nm_usuario_p		varchar2) is 
begin
if	(nm_procedure_p is not null) and
	(nm_usuario_p is not null) then
	begin
	if	('GERAR_RESUMO_OFTALMO' = upper(nm_procedure_p)) then
		begin
		gerar_resumo_oftalmo_pkg.gerar_resumo_oftalmo(cd_pessoa_fisica_p, nm_usuario_p, ds_fundoscopia_p);
		end;
	elsif	('BV_GERAR_RESUMO_OFTALMO' = upper(nm_procedure_p)) then
		begin
		gerar_resumo_oftalmo_pkg.bv_gerar_resumo_oftalmo(cd_pessoa_fisica_p, nm_usuario_p, ds_fundoscopia_p);
		end;
	elsif	('HDH_GERAR_RESUMO_OFTALMO' = upper(nm_procedure_p)) then
		begin
		gerar_resumo_oftalmo_pkg.hdh_gerar_resumo_oftalmo(cd_pessoa_fisica_p, nm_usuario_p, ds_fundoscopia_p);
		end;
	end if;
	end;
end if;
commit;
end gerar_resumo_oftalmo;
/