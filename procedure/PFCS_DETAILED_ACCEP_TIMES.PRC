create or replace procedure pfcs_detailed_accep_times(
			nr_seq_indicator_p	number,
			cd_estabelecimento_p	varchar2,
			nm_usuario_p	varchar2) is

--queue
	Cursor C01 is
	select	a.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(a.cd_pessoa_fisica, 'list'), obter_nome_pf(a.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(a.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(a.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(a.cd_pessoa_fisica, 'I') qt_idade_paciente,
		a.dt_geracao_senha dt_entrance,
		null nr_encounter,
		round((sysdate - a.dt_geracao_senha) * 1440) as qt_time_stage_pa,
		'1' cd_stage_pa,
		obter_valor_dominio(9512, '1') ds_stage_pa
	from	paciente_senha_fila a, pessoa_fisica pf
	where	a.dt_utilizacao is null
	and	a.dt_inutilizacao is null
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.cd_pessoa_fisica = pf.cd_pessoa_fisica (+);

--reception
	Cursor C02 is
	select	ac.cd_pessoa_fisica id_patient,
        nvl(get_formatted_person_name(ac.cd_pessoa_fisica, 'list'), obter_nome_pf(ac.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(ac.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(ac.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(ac.cd_pessoa_fisica, 'I') qt_idade_paciente,
		ac.dt_entrada dt_entrance,
		ac.nr_atendimento nr_encounter,
		round((sysdate - ac.dt_entrada) * 1440) as qt_time_stage_pa,
		'2' cd_stage_pa,
		obter_valor_dominio(9512, '2') ds_stage_pa
	from	atendimento_paciente ac, pessoa_fisica pf
	where	ac.ie_tipo_atendimento = 3
	and	ac.cd_estabelecimento = cd_estabelecimento_p
	and	ac.dt_inicio_atendimento is null
	and	get_if_encounter_still_pa(ac.nr_atendimento) = 'S'
	and	ac.cd_pessoa_fisica = pf.cd_pessoa_fisica
    and ac.dt_alta is null;

--triage

  Cursor C03 is
	select ac.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(ac.cd_pessoa_fisica, 'list'), obter_nome_pf(ac.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(ac.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(ac.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(ac.cd_pessoa_fisica, 'I') qt_idade_paciente,
		tpa.dt_inicio_triagem dt_entrance,
		ac.nr_atendimento nr_encounter,
		round((sysdate - tpa.dt_inicio_triagem) * 1440) as qt_time_stage_pa,
		'3' cd_stage_pa,
		obter_valor_dominio(9512, '3') ds_stage_pa
	from  atendimento_paciente ac, pessoa_fisica pf, triagem_pronto_atend tpa
	where  ac.ie_tipo_atendimento = 3
	and  ac.cd_estabelecimento = cd_estabelecimento_p
	and  tpa.dt_inicio_triagem is not null
	and  tpa.dt_fim_triagem is null
	and  tpa.dt_cancelamento is null
	and  get_if_encounter_still_pa(ac.nr_atendimento) = 'S'
	and ac.dt_alta is null
  and  ac.cd_pessoa_fisica = pf.cd_pessoa_fisica
  and tpa.nr_atendimento = ac.nr_atendimento
  and  tpa.cd_pessoa_fisica = pf.cd_pessoa_fisica;

--medic call
	Cursor C04 is
	 	select	ac.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(ac.cd_pessoa_fisica, 'list'), obter_nome_pf(ac.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(ac.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(ac.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(ac.cd_pessoa_fisica, 'I') qt_idade_paciente,
		ac.dt_entrada dt_entrance,
		ac.nr_atendimento nr_encounter,
		round((sysdate - tpa.dt_inicio_triagem) * 1440) as qt_time_stage_pa,
		'4' cd_stage_pa,
		obter_valor_dominio(9512, '4') ds_stage_pa
	from	atendimento_paciente ac, pessoa_fisica pf, triagem_pronto_atend tpa
	where	ac.ie_tipo_atendimento = 3
	and	ac.cd_estabelecimento = cd_estabelecimento_p
	and	(ac.dt_fim_triagem is not null or
	(ac.dt_chamada_paciente is not null	or ac.dt_atend_medico is not null))
	and	ac.dt_inicio_esp_exame is null
	and	ac.dt_inicio_prescr_pa is null
	and	get_if_encounter_still_pa(ac.nr_atendimento) = 'S'
	and ac.dt_alta is null
	and	ac.cd_pessoa_fisica = pf.cd_pessoa_fisica
	and tpa.nr_atendimento = ac.nr_atendimento
	and  tpa.cd_pessoa_fisica = pf.cd_pessoa_fisica;

--exam
	Cursor C05 is
	select	ac.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(ac.cd_pessoa_fisica, 'list'), obter_nome_pf(ac.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(ac.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(ac.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(ac.cd_pessoa_fisica, 'I') qt_idade_paciente,
		ac.dt_entrada dt_entrance,
		ac.nr_atendimento nr_encounter,
		round((sysdate - ac.dt_inicio_esp_exame) * 1440) as qt_time_stage_pa,
		'5' cd_stage_pa,
		obter_valor_dominio(9512, '5') ds_stage_pa
	from	atendimento_paciente ac, pessoa_fisica pf
	where	ac.ie_tipo_atendimento = 3
	and	ac.cd_estabelecimento = cd_estabelecimento_p
	and	ac.dt_inicio_esp_exame is not null
	and	get_if_encounter_still_pa(ac.nr_atendimento) = 'S'
	and (select count(*) from atendimento_alta aa where (ie_desfecho = 'I' or ie_desfecho = 'T') and aa.nr_atendimento = ac.nr_atendimento) = 0
	and ac.dt_alta is null
    and	ac.cd_pessoa_fisica = pf.cd_pessoa_fisica;

--medic
	Cursor C06 is
	select	ac.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(ac.cd_pessoa_fisica, 'list'), obter_nome_pf(ac.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(ac.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(ac.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(ac.cd_pessoa_fisica, 'I') qt_idade_paciente,
		ac.dt_entrada dt_entrance,
		ac.nr_atendimento nr_encounter,
		round((sysdate - ac.dt_inicio_prescr_pa) * 1440) as qt_time_stage_pa,
		'6' cd_stage_pa,
		obter_valor_dominio(9512, '6') ds_stage_pa
	from	atendimento_paciente ac, pessoa_fisica pf
	where	ac.ie_tipo_atendimento = 3
	and	ac.cd_estabelecimento = cd_estabelecimento_p
	and	ac.dt_inicio_prescr_pa is not null
	and	get_if_encounter_still_pa(ac.nr_atendimento) = 'S'
	and	ac.cd_pessoa_fisica = pf.cd_pessoa_fisica
	and (select count(*) from atendimento_alta aa where (ie_desfecho = 'I' or ie_desfecho = 'T') and aa.nr_atendimento = ac.nr_atendimento) = 0
    and ac.dt_alta is null;

--outcome
	Cursor C07 is
	select	ac.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(ac.cd_pessoa_fisica, 'list'), obter_nome_pf(ac.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(ac.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(ac.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(ac.cd_pessoa_fisica, 'I') qt_idade_paciente,
		ac.dt_entrada dt_entrance,
		ac.nr_atendimento nr_encounter,
		round((sysdate - nvl(aa.dt_desfecho, aa.dt_atualizacao)) * 1440) as qt_time_stage_pa,
		'7' cd_stage_pa,
		obter_valor_dominio(9512, '7') ds_stage_pa
	from	atendimento_paciente ac, pessoa_fisica pf, atendimento_alta aa
	where	ac.ie_tipo_atendimento = 3
	and	ac.cd_estabelecimento = cd_estabelecimento_p
	and	ac.cd_pessoa_fisica = pf.cd_pessoa_fisica
	and	ac.nr_atendimento = aa.nr_atendimento
	and	(aa.ie_desfecho = 'I' or aa.ie_desfecho = 'T')
    and ac.DT_ALTA_MEDICO is null
	and	(
			(
				(select max(nvl(p.ie_liberar_desfecho,'N')) from parametro_medico p where p.cd_estabelecimento = cd_estabelecimento_p) = 'N')
				or	((aa.dt_liberacao is not null) and (aa.dt_inativacao is null)
			)
		)
	and	obter_atepacu_paciente(ac.nr_atendimento, 'PI') = 0;

	ie_excedeu_w					varchar2(1);
	pfcs_panel_detail_seq_w				pfcs_panel_detail.nr_sequencia%type;
	nr_seq_operational_level_w			pfcs_operational_level.nr_sequencia%type;
	nr_seq_panel_w					pfcs_panel.nr_sequencia%type;

	qt_person_total_w	pfcs_panel.vl_indicator%type := 0;
	qt_person_beyond_w	pfcs_panel.vl_indicator_aux%type := 0;
	qt_time_total_w		pfcs_panel.vl_indicator%type := 0;

	function	pfcs_stages_times(	cd_stage_p		number)
			 		return number as

		qt_rule_time_w			pfcs_time_stage_pa.qt_time%type;

		begin

			select nvl(qt_time, 0)
			into qt_rule_time_w
			from pfcs_time_stage_pa
			where cd_rule = cd_stage_p;

			return	qt_rule_time_w;

		end;

	function pfcs_average_time (time_field_p number,
					persons_field_p number)
				return number as

		final_value_w number(15);

		begin

		if (persons_field_p = 0 ) then
			final_value_w := 0;
		else
			final_value_w := time_field_p / persons_field_p;
		end if;

		return final_value_w;

		end;

begin

	nr_seq_operational_level_w := pfcs_get_structure_level(
		cd_establishment_p => cd_estabelecimento_p,
		ie_level_p => 'O',
		ie_info_p => 'C');

	for c01_w in c01 loop
		begin

			if	(c01_w.qt_time_stage_pa > 0 ) then
				qt_person_total_w := qt_person_total_w + 1;
				qt_time_total_w :=  c01_w.qt_time_stage_pa + qt_time_total_w ;
				if	(pfcs_stages_times(1) < c01_w.qt_time_stage_pa ) then
					qt_person_beyond_w := qt_person_beyond_w + 1;
					ie_excedeu_w := 'S';
				end if;
			end if;

			select	pfcs_panel_detail_seq.nextval
			into	pfcs_panel_detail_seq_w
			from	dual;

			insert into pfcs_panel_detail(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				ie_situation,
				nr_seq_indicator,
				nr_seq_operational_level)
			values (
				pfcs_panel_detail_seq_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				'T',
				nr_seq_indicator_p,
				nr_seq_operational_level_w);

			insert into pfcs_detail_patient(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_detail,
				nr_encounter,
				dt_entrance,
				id_patient,
				nm_patient,
				ds_gender,
				ds_classification,
				dt_birthdate,
                ds_age_range,
				qt_time_stage_pa,
				cd_stage_pa,
				ds_stage_pa,
				ie_over_threshold)
			values (
				pfcs_detail_patient_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				pfcs_panel_detail_seq_w,
				c01_w.nr_encounter,
				c01_w.dt_entrance,
				c01_w.id_patient,
				c01_w.nm_patient,
				c01_w.ds_gender,
				c01_w.ds_classification,
				c01_w.dt_birthdate,
                c01_w.qt_idade_paciente,
				c01_w.qt_time_stage_pa,
				c01_w.cd_stage_pa,
				c01_w.ds_stage_pa,
				ie_excedeu_w);

			ie_excedeu_w := 'N';

		end;
	end loop;

	commit;

	pfcs_pck.pfcs_generate_results(
		vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
		vl_indicator_aux_p => qt_person_beyond_w,
		ds_reference_value_p => obter_valor_dominio(9512, '1'),
		cd_reference_value_p => '1',
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck.pfcs_update_detail(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_panel_p => nr_seq_panel_w,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p);

	qt_person_total_w	:= 0;
	qt_person_beyond_w	:= 0;
	qt_time_total_w		:= 0;

	for c02_w in c02 loop
		begin

			if	(c02_w.qt_time_stage_pa > 0 ) then
				qt_person_total_w := qt_person_total_w + 1;
				qt_time_total_w :=  c02_w.qt_time_stage_pa + qt_time_total_w ;
				if	(pfcs_stages_times(2) < c02_w.qt_time_stage_pa ) then
					qt_person_beyond_w := qt_person_beyond_w + 1;
					ie_excedeu_w := 'S';
				end if;
			end if;

			select	pfcs_panel_detail_seq.nextval
			into	pfcs_panel_detail_seq_w
			from	dual;

			insert into pfcs_panel_detail(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				ie_situation,
				nr_seq_indicator,
				nr_seq_operational_level)
			values (
				pfcs_panel_detail_seq_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				'T',
				nr_seq_indicator_p,
				nr_seq_operational_level_w);

			insert into pfcs_detail_patient(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_detail,
				nr_encounter,
				dt_entrance,
				id_patient,
				nm_patient,
				ds_gender,
				ds_classification,
				dt_birthdate,
                ds_age_range,
				qt_time_stage_pa,
				cd_stage_pa,
				ds_stage_pa,
				ie_over_threshold)
			values (
				pfcs_detail_patient_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				pfcs_panel_detail_seq_w,
				c02_w.nr_encounter,
				c02_w.dt_entrance,
				c02_w.id_patient,
				c02_w.nm_patient,
				c02_w.ds_gender,
				c02_w.ds_classification,
				c02_w.dt_birthdate,
                c02_w.qt_idade_paciente,
				c02_w.qt_time_stage_pa,
				c02_w.cd_stage_pa,
				c02_w.ds_stage_pa,
				ie_excedeu_w);

			ie_excedeu_w := 'N';

		end;
	end loop;

	commit;

	pfcs_pck.pfcs_generate_results(
		vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
		vl_indicator_aux_p => qt_person_beyond_w,
		ds_reference_value_p => obter_valor_dominio(9512, '2'),
		cd_reference_value_p => '2',
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck.pfcs_update_detail(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_panel_p => nr_seq_panel_w,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p);

	qt_person_total_w	:= 0;
	qt_person_beyond_w	:= 0;
	qt_time_total_w		:= 0;

	for c03_w in c03 loop
		begin

			if	(c03_w.qt_time_stage_pa > 0 ) then
				qt_person_total_w := qt_person_total_w + 1;
				qt_time_total_w :=  c03_w.qt_time_stage_pa + qt_time_total_w ;
				if	(pfcs_stages_times(3) < c03_w.qt_time_stage_pa ) then
					qt_person_beyond_w := qt_person_beyond_w + 1;
					ie_excedeu_w := 'S';
				end if;
			end if;

			select	pfcs_panel_detail_seq.nextval
			into	pfcs_panel_detail_seq_w
			from	dual;

			insert into pfcs_panel_detail(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				ie_situation,
				nr_seq_indicator,
				nr_seq_operational_level)
			values (
				pfcs_panel_detail_seq_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				'T',
				nr_seq_indicator_p,
				nr_seq_operational_level_w);

			insert into pfcs_detail_patient(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_detail,
				nr_encounter,
				dt_entrance,
				id_patient,
				nm_patient,
				ds_gender,
				ds_classification,
				dt_birthdate,
                ds_age_range,
				qt_time_stage_pa,
				cd_stage_pa,
				ds_stage_pa,
				ie_over_threshold)
			values (
				pfcs_detail_patient_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				pfcs_panel_detail_seq_w,
				c03_w.nr_encounter,
				c03_w.dt_entrance,
				c03_w.id_patient,
				c03_w.nm_patient,
				c03_w.ds_gender,
				c03_w.ds_classification,
				c03_w.dt_birthdate,
                c03_w.qt_idade_paciente,
				c03_w.qt_time_stage_pa,
				c03_w.cd_stage_pa,
				c03_w.ds_stage_pa,
				ie_excedeu_w);

			ie_excedeu_w := 'N';

		end;
	end loop;

	commit;

	pfcs_pck.pfcs_generate_results(
		vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
		vl_indicator_aux_p => qt_person_beyond_w,
		ds_reference_value_p => obter_valor_dominio(9512, '3'),
		cd_reference_value_p => '3',
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck.pfcs_update_detail(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_panel_p => nr_seq_panel_w,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p);

	qt_person_total_w	:= 0;
	qt_person_beyond_w	:= 0;
	qt_time_total_w		:= 0;

	for c04_w in c04 loop
		begin

			if	(c04_w.qt_time_stage_pa > 0 ) then
				qt_person_total_w := qt_person_total_w + 1;
				qt_time_total_w :=  c04_w.qt_time_stage_pa + qt_time_total_w ;
				if	(pfcs_stages_times(4) < c04_w.qt_time_stage_pa ) then
					qt_person_beyond_w := qt_person_beyond_w + 1;
					ie_excedeu_w := 'S';
				end if;
			end if;

			select	pfcs_panel_detail_seq.nextval
			into	pfcs_panel_detail_seq_w
			from	dual;

			insert into pfcs_panel_detail(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				ie_situation,
				nr_seq_indicator,
				nr_seq_operational_level)
			values (
				pfcs_panel_detail_seq_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				'T',
				nr_seq_indicator_p,
				nr_seq_operational_level_w);

			insert into pfcs_detail_patient(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_detail,
				nr_encounter,
				dt_entrance,
				id_patient,
				nm_patient,
				ds_gender,
				ds_classification,
				dt_birthdate,
                ds_age_range,
				qt_time_stage_pa,
				cd_stage_pa,
				ds_stage_pa,
				ie_over_threshold)
			values (
				pfcs_detail_patient_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				pfcs_panel_detail_seq_w,
				c04_w.nr_encounter,
				c04_w.dt_entrance,
				c04_w.id_patient,
				c04_w.nm_patient,
				c04_w.ds_gender,
				c04_w.ds_classification,
				c04_w.dt_birthdate,
                c04_w.qt_idade_paciente,
				c04_w.qt_time_stage_pa,
				c04_w.cd_stage_pa,
				c04_w.ds_stage_pa,
				ie_excedeu_w);

			ie_excedeu_w := 'N';

		end;
	end loop;

	commit;

	pfcs_pck.pfcs_generate_results(
		vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
		vl_indicator_aux_p => qt_person_beyond_w,
		ds_reference_value_p => obter_valor_dominio(9512, '4'),
		cd_reference_value_p => '4',
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck.pfcs_update_detail(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_panel_p => nr_seq_panel_w,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p);

	qt_person_total_w	:= 0;
	qt_person_beyond_w	:= 0;
	qt_time_total_w		:= 0;

	for c05_w in c05 loop
		begin

			if	(c05_w.qt_time_stage_pa > 0 ) then
				qt_person_total_w := qt_person_total_w + 1;
				qt_time_total_w :=  c05_w.qt_time_stage_pa + qt_time_total_w ;
				if	(pfcs_stages_times(5) < c05_w.qt_time_stage_pa ) then
					qt_person_beyond_w := qt_person_beyond_w + 1;
					ie_excedeu_w := 'S';
				end if;
			end if;

			select	pfcs_panel_detail_seq.nextval
			into	pfcs_panel_detail_seq_w
			from	dual;

			insert into pfcs_panel_detail(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				ie_situation,
				nr_seq_indicator,
				nr_seq_operational_level)
      values (
        pfcs_panel_detail_seq_w,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        'T',
        nr_seq_indicator_p,
        nr_seq_operational_level_w);

      insert into pfcs_detail_patient(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        nr_encounter,
        dt_entrance,
        id_patient,
        nm_patient,
        ds_gender,
        ds_classification,
        dt_birthdate,
        ds_age_range,
        qt_time_stage_pa,
        cd_stage_pa,
        ds_stage_pa,
        ie_over_threshold)
      values (
        pfcs_detail_patient_seq.nextval,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c05_w.nr_encounter,
        c05_w.dt_entrance,
        c05_w.id_patient,
        c05_w.nm_patient,
        c05_w.ds_gender,
        c05_w.ds_classification,
        c05_w.dt_birthdate,
        c05_w.qt_idade_paciente,
        c05_w.qt_time_stage_pa,
        c05_w.cd_stage_pa,
        c05_w.ds_stage_pa,
        ie_excedeu_w);

      ie_excedeu_w := 'N';

    end;
  end loop;

  commit;

  pfcs_pck.pfcs_generate_results(
    vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
    vl_indicator_aux_p => qt_person_beyond_w,
    ds_reference_value_p => obter_valor_dominio(9512, '5'),
    cd_reference_value_p => '5',
    nr_seq_indicator_p => nr_seq_indicator_p,
    nr_seq_operational_level_p => nr_seq_operational_level_w,
    nm_usuario_p => nm_usuario_p,
    nr_seq_panel_p => nr_seq_panel_w);

  pfcs_pck.pfcs_update_detail(
      nr_seq_indicator_p => nr_seq_indicator_p,
      nr_seq_panel_p => nr_seq_panel_w,
      nr_seq_operational_level_p => nr_seq_operational_level_w,
      nm_usuario_p => nm_usuario_p);

  qt_person_total_w  := 0;
  qt_person_beyond_w  := 0;
  qt_time_total_w    := 0;

  for c06_w in c06 loop
    begin

      if  (c06_w.qt_time_stage_pa > 0 ) then
        qt_person_total_w := qt_person_total_w + 1;
        qt_time_total_w :=  c06_w.qt_time_stage_pa + qt_time_total_w ;
        if  (pfcs_stages_times(6) < c06_w.qt_time_stage_pa ) then
          qt_person_beyond_w := qt_person_beyond_w + 1;
          ie_excedeu_w := 'S';
        end if;
      end if;

      select  pfcs_panel_detail_seq.nextval
      into  pfcs_panel_detail_seq_w
      from  dual;

      insert into pfcs_panel_detail(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        ie_situation,
        nr_seq_indicator,
        nr_seq_operational_level)
      values (
        pfcs_panel_detail_seq_w,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        'T',
        nr_seq_indicator_p,
        nr_seq_operational_level_w);

      insert into pfcs_detail_patient(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        nr_encounter,
        dt_entrance,
        id_patient,
        nm_patient,
        ds_gender,
        ds_classification,
        dt_birthdate,
        ds_age_range,
        qt_time_stage_pa,
        cd_stage_pa,
        ds_stage_pa,
        ie_over_threshold)
      values (
        pfcs_detail_patient_seq.nextval,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c06_w.nr_encounter,
        c06_w.dt_entrance,
        c06_w.id_patient,
        c06_w.nm_patient,
        c06_w.ds_gender,
        c06_w.ds_classification,
        c06_w.dt_birthdate,
        c06_w.qt_idade_paciente,
        c06_w.qt_time_stage_pa,
        c06_w.cd_stage_pa,
        c06_w.ds_stage_pa,
        ie_excedeu_w);

      ie_excedeu_w := 'N';

    end;
  end loop;

  commit;

  pfcs_pck.pfcs_generate_results(
    vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
    vl_indicator_aux_p => qt_person_beyond_w,
    ds_reference_value_p => obter_valor_dominio(9512, '6'),
    cd_reference_value_p => '6',
    nr_seq_indicator_p => nr_seq_indicator_p,
    nr_seq_operational_level_p => nr_seq_operational_level_w,
    nm_usuario_p => nm_usuario_p,
    nr_seq_panel_p => nr_seq_panel_w);

  pfcs_pck.pfcs_update_detail(
      nr_seq_indicator_p => nr_seq_indicator_p,
      nr_seq_panel_p => nr_seq_panel_w,
      nr_seq_operational_level_p => nr_seq_operational_level_w,
      nm_usuario_p => nm_usuario_p);

  qt_person_total_w  := 0;
  qt_person_beyond_w  := 0;
  qt_time_total_w    := 0;

  for c07_w in c07 loop
    begin

      if  (c07_w.qt_time_stage_pa > 0 ) then
        qt_person_total_w := qt_person_total_w + 1;
        qt_time_total_w :=  c07_w.qt_time_stage_pa + qt_time_total_w ;
        if  (pfcs_stages_times(7) < c07_w.qt_time_stage_pa ) then
          qt_person_beyond_w := qt_person_beyond_w + 1;
          ie_excedeu_w := 'S';
        end if;
      end if;

      select  pfcs_panel_detail_seq.nextval
      into  pfcs_panel_detail_seq_w
      from  dual;

      insert into pfcs_panel_detail(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        ie_situation,
        nr_seq_indicator,
        nr_seq_operational_level)
      values (
        pfcs_panel_detail_seq_w,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        'T',
        nr_seq_indicator_p,
        nr_seq_operational_level_w);

      insert into pfcs_detail_patient(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        nr_encounter,
        dt_entrance,
        id_patient,
        nm_patient,
        ds_gender,
        ds_classification,
        dt_birthdate,
        ds_age_range,
        qt_time_stage_pa,
        cd_stage_pa,
        ds_stage_pa,
        ie_over_threshold)
      values (
        pfcs_detail_patient_seq.nextval,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c07_w.nr_encounter,
        c07_w.dt_entrance,
        c07_w.id_patient,
        c07_w.nm_patient,
        c07_w.ds_gender,
        c07_w.ds_classification,
        c07_w.dt_birthdate,
        c07_w.qt_idade_paciente,
        c07_w.qt_time_stage_pa,
        c07_w.cd_stage_pa,
        c07_w.ds_stage_pa,
        ie_excedeu_w);

      ie_excedeu_w := 'N';

    end;
  end loop;

  commit;

  pfcs_pck.pfcs_generate_results(
    vl_indicator_p => pfcs_average_time(qt_time_total_w, qt_person_total_w),
    vl_indicator_aux_p => qt_person_beyond_w,
    ds_reference_value_p => obter_valor_dominio(9512, '7'),
    cd_reference_value_p => '7',
    nr_seq_indicator_p => nr_seq_indicator_p,
    nr_seq_operational_level_p => nr_seq_operational_level_w,
    nm_usuario_p => nm_usuario_p,
    nr_seq_panel_p => nr_seq_panel_w);

  pfcs_pck.pfcs_update_detail(
      nr_seq_indicator_p => nr_seq_indicator_p,
      nr_seq_panel_p => nr_seq_panel_w,
      nr_seq_operational_level_p => nr_seq_operational_level_w,
      nm_usuario_p => nm_usuario_p);

  qt_person_total_w  := 0;
  qt_person_beyond_w  := 0;
  qt_time_total_w    := 0;

  pfcs_pck.pfcs_activate_records(
    nr_seq_indicator_p => nr_seq_indicator_p,
    nr_seq_operational_level_p => nr_seq_operational_level_w,
    nm_usuario_p => nm_usuario_p);

end pfcs_detailed_accep_times;
/