create or replace
procedure calcular_atualizar_valor_item(nr_seq_nota_p		number,
					cd_material_p		number,
					cd_estabelecimento_p	number,
					cd_operacao_nf_p	number,
					nr_item_nf_p		number,
					nm_usuario_p		varchar2) is 

vl_total_item_nf_w	nota_fiscal_item.vl_total_item_nf%type;
vl_desconto_w		nota_fiscal_item.vl_desconto%type;
qt_item_nf_w		nota_fiscal_item.qt_item_nf%type;
vl_liquido_w		nota_fiscal_item.vl_liquido%type;
pr_desconto_w		nota_fiscal_item.pr_desconto%type;
vl_unitario_item_nf_w	nota_fiscal_item.vl_unitario_item_nf%type;

begin

obter_preco_material_entrada(cd_material_p, cd_estabelecimento_p, cd_operacao_nf_p, nm_usuario_p, vl_unitario_item_nf_w);

select	vl_desconto,
	qt_item_nf
into	vl_desconto_w,
	qt_item_nf_w
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nota_p
and	nr_item_nf = nr_item_nf_p;

vl_total_item_nf_w := (vl_unitario_item_nf_w * qt_item_nf_w);
vl_liquido_w := (vl_total_item_nf_w - vl_desconto_w);

if	(vl_total_item_nf_w <> 0) then 
	pr_desconto_w := (vl_desconto_w * 100)/vl_total_item_nf_w;
else
	pr_desconto_w := (vl_desconto_w * 100);
end if;


if (vl_unitario_item_nf_w >= 0) then
	update	nota_fiscal_item
	set	vl_total_item_nf = vl_total_item_nf_w,
		vl_liquido = vl_liquido_w,
		pr_desconto = pr_desconto_w,
		vl_unitario_item_nf = vl_unitario_item_nf_w
	where	nr_sequencia = nr_seq_nota_p
	and	nr_item_nf = nr_item_nf_p;
else
	wheb_mensagem_pck.exibir_mensagem_abort(1095050);
end if;

commit;

end calcular_atualizar_valor_item;
/
