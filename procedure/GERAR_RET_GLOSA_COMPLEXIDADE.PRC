create or replace
procedure gerar_ret_glosa_complexidade(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		number,
			ie_procedimento_p	varchar2,
			ie_honorario_p		varchar2,
			ie_taxa_p		varchar2,
			ie_diaria_p		varchar2,
			ie_material_p		varchar2,
			ie_medicamento_p	varchar2,
			ie_extra_p		varchar2,
			ie_complexidade_p		varchar2,
			ie_tipo_complex_p		varchar2) is 
			
			
nr_seq_item_p	number(10);	
			
Cursor C01 is
	select	a.nr_sequencia
	from	convenio_retorno_item a
	where	a.nr_seq_retorno = nr_sequencia_p
	order by a.nr_sequencia;			

begin

open C01;
loop
fetch C01 into	
	nr_seq_item_p;
exit when C01%notfound;
	begin	
	gerar_retorno_glosa_conta_sus(nr_seq_item_p,nm_usuario_p,'G',1,ie_procedimento_p,ie_honorario_p,ie_taxa_p,ie_diaria_p,ie_material_p,ie_medicamento_p,ie_extra_p,ie_complexidade_p,'',ie_tipo_complex_p);
	end;
end loop;
close C01;

end gerar_ret_glosa_complexidade;
/
