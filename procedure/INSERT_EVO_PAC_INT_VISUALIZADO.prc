create or replace procedure INSERT_EVO_PAC_INT_VISUALIZADO(    nr_atendimento_p    number ,
                                nm_usuario_p     varchar2 ,
                                cd_evolucao_p    evolucao_paciente.cd_evolucao%type) is
cd_evolucao_w         evolucao_paciente.cd_evolucao%type := cd_evolucao_p;
nr_sequencia_visual_w evo_pac_int_visualizado.nr_sequencia%type;
nr_sequencia_w        number;
begin
   begin
      select nvl(count(nr_sequencia),0)
      into   nr_sequencia_w
      from   evo_pac_int_visualizado
      where  nm_usuario   = nm_usuario_p
      and    nr_seq_evo_paciente = cd_evolucao_w;
   exception
      when no_data_found then
           nr_sequencia_w := 0;
   end;                              
   if nr_sequencia_w = 0 then
      select evo_pac_int_visualizado_seq.nextval
      into   nr_sequencia_visual_w
      from   dual;       
      insert into  evo_pac_int_visualizado
              (nr_sequencia,
               nm_usuario,
               dt_atualizacao,
               nm_usuario_nrec,
               dt_atualizacao_nrec,
               nr_seq_evo_paciente)
       values (nr_sequencia_visual_w,
               nm_usuario_p,
               sysdate,
               nm_usuario_p,
               sysdate,
               cd_evolucao_w);
      commit;
      for c1 in (select min(nr_sequencia) first_nr_seq,
                    nr_seq_evo_paciente,
                    nm_usuario
               from evo_pac_int_visualizado
              group by nr_seq_evo_paciente, nm_usuario
             having count(*) > 1) loop
        delete evo_pac_int_visualizado a
         where a.nr_seq_evo_paciente = c1.nr_seq_evo_paciente
           and a.nm_usuario = c1.nm_usuario
           and a.nr_sequencia <> c1.first_nr_seq;
        commit;
      end loop;
    end if;
end INSERT_EVO_PAC_INT_VISUALIZADO;
/
