create or replace
procedure sbc_gerar_cobr_bb_250_CBR454
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is
	
/* CONTE�DO */
ds_conteudo_w			varchar2(250);

/* CONTADOR */
nr_seq_apres_w			number(15) := 1;

/* HEADER ARQUIVO */
ie_tipo_registro_w			varchar2(255);
cd_agencia_bancaria_w		varchar2(255);
nr_digito_agencia_w		varchar2(255);
cd_cgc_cedente_w			varchar2(255);
nr_digito_verificador_w		varchar2(255);
cd_carteira_w			varchar2(255);
cd_variacao_carteira_w		varchar2(255);
ds_zeros_w			varchar2(255);
nm_empresa_w			varchar2(255);
ds_sigla_cedente_w		varchar2(255);
tp_impressao_w			varchar2(255);
ds_endereco_w			varchar2(255);
cd_cep_w			varchar2(255);
ds_praca_devolucao_w		varchar2(255);
nr_seq_remessa_w			varchar2(255);
ds_indic_conferencia_w		varchar2(255);
ds_ident_arquivo_w			varchar2(255);
cd_personalizacao_w		varchar2(255);
cd_convenio_banco_w		varchar2(255);

/* DETALHE ARQUIVO */
tp_documento_w			varchar2(255);
nr_cgc_cpf_w			varchar2(255);
nm_pagador_w			varchar2(255);
ds_praca_sacado_w		varchar2(255);
sg_estado_w			varchar2(255);
dt_emissao_w			varchar2(255);
dt_vencimento_w			varchar2(255);
ds_aceite_w			varchar2(255);
ie_especie_titulo_w			varchar2(255);
nr_nosso_numero_w		varchar2(255);
nr_titulo_w			varchar2(255);
cd_moeda_w			varchar2(255);
qt_moeda_variavel_w		varchar2(255);
vl_titulo_w			varchar2(255);
qt_dias_protesto_w			varchar2(255);
qt_total_parcelas_w			varchar2(255);
ie_documento_w			varchar2(1);

/* BRANCOS */
ds_brancos_233_w			varchar2(233);
ds_brancos_38_w			varchar2(38);
ds_brancos_6_w			varchar2(6);
ds_brancos_4_w			varchar2(4);
ds_brancos_3_w			varchar2(3);
ds_brancos_5_w			varchar2(5);

ds_mensagem_1_w			varchar2(60);
ds_mensagem_2_w			varchar2(60);
ds_mensagem_3_w			varchar2(60);
ds_mensagem_4_w			varchar2(60);
ds_mensagem_5_w			varchar2(240);
	
cursor C01 is					
	select	'11' tp_documento,
		decode(pls_obter_dados_pagador(k.nr_seq_pagador, 'T'),'PF', 1,
		decode(pls_obter_dados_pagador(k.nr_seq_pagador, 'T'),'PJ',2,3)) ie_documento,
		lpad(nvl(b.cd_cgc_cpf,'0'),15, '0') ds_cgc_cpf,		
		rpad(upper(substr(nvl(pls_obter_dados_pagador(k.nr_seq_pagador, 'N'),' '),1,60)),60,' ') nm_pagador,		
		rpad(substr(nvl(pls_obter_compl_pagador(k.nr_seq_pagador, 'CI'), ' '), 1, 18), 18, ' ') ds_praca_sacado,
		rpad(substr(nvl(pls_obter_compl_pagador(k.nr_seq_pagador, 'UF'), ' '), 1, 2), 2, ' ') sg_estado,
		to_char(b.dt_emissao,'ddmmyy') dt_emissao,
		to_char(b.dt_pagamento_previsto,'ddmmyy') dt_vencimento,
		'N' ds_aceite,
		'DM' ie_especie_titulo,
		/*lpad(substr(nvl(d.cd_convenio_banco,'0'), 1, 7), 7,'0') || */lpad(substr(nvl(b.nr_nosso_numero,'0'), 1, 17), 17,'0') nr_nosso_numero,
		lpad(nvl(b.nr_titulo,'0'),10,'0') nr_titulo,
		'09' cd_moeda,
		lpad('0',15,'0') qt_moeda_variavel,
		lpad(replace(replace(campo_mascara_virgula(b.vl_titulo),',',''),'.',''),15,'0') vl_titulo,
		lpad(nvl(d.qt_dias_protesto,'0'),2,'0') qt_dias_protesto,
		'00' qt_total_parcelas,
		rpad(substr(substr(decode(nvl(obter_logr_abreviado_sp(pls_obter_compl_pagador(k.nr_seq_pagador, 'DTL')), 'RUA'), 'RUA', 
		nvl(pls_obter_compl_pagador(k.nr_seq_pagador, 'DTL'), ' '),
		nvl(obter_logr_abreviado_sp(pls_obter_compl_pagador(k.nr_seq_pagador, 'DTL')), ' ')), 1, 19) || ' ' ||		
		nvl(pls_obter_compl_pagador(k.nr_seq_pagador, 'ES'), ' '), 1, 60), 60, ' ') ds_endereco,
		rpad(substr(nvl(pls_obter_compl_pagador(k.nr_seq_pagador, 'CEP'), ' '), 1, 8), 8, ' ') cd_cep,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,1),' '),60, ' ') ds_mensagem_1,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,2),' '),60, ' ') ds_mensagem_2,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,3),' '),60, ' ') ds_mensagem_3,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,4),' '),60, ' ') ds_mensagem_4,
		rpad(nvl(substr(x.ds_observacao_titulo,1, 240),' '),240, ' ') ds_mensagem_5
	from	pls_mensalidade		k,
		banco_estabelecimento 	e,
		banco_carteira 		d,
		titulo_receber		x,
		titulo_receber_cobr 		c,
		titulo_receber_v 		b,
		cobranca_escritural 	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	b.nr_titulo			= x.nr_titulo
	and	x.nr_seq_mensalidade   	= k.nr_sequencia(+)
	and	c.nr_titulo			= b.nr_titulo
	and	a.nr_seq_conta_banco	= e.nr_sequencia
	and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
begin

select	lpad(' ', 233, ' '),
		lpad(' ', 38, ' '),
		lpad(' ', 6, ' '),
		lpad(' ', 4, ' '),
		lpad(' ', 3, ' '),
		lpad(' ', 5, ' ')
into	ds_brancos_233_w,
		ds_brancos_38_w,
		ds_brancos_6_w,
		ds_brancos_4_w,
		ds_brancos_3_w,
		ds_brancos_5_w
from	dual;
	
delete from w_envio_banco where nm_usuario = nm_usuario_p;	
	
/* HEADER ARQUIVO */
select	'01' ie_tipo_registro,
	lpad(replace(replace(c.cd_agencia_bancaria,'-',''),'.',''),4,'0') cd_agencia_bancaria,
	substr(nvl(to_number(e.ie_digito),calcula_digito('Modulo11',c.cd_agencia_bancaria)),1,1) nr_digito_agencia,		
	lpad(substr(nvl(c.cd_cedente,c.cd_conta),1,9),9,'0') cd_cedente,
	decode(c.cd_cedente,null,substr(nvl(c.ie_digito_conta,'0'),1,1),'0') nr_digito_verificador,
	lpad(substr(nvl(d.cd_carteira, '0'),1,3), 3, '0') cd_carteira,
	lpad(substr(replace(nvl(d.cd_variacao_carteira, '0'), '-', ''),1,3), 3,'0')	cd_variacao_carteira,
	lpad('0',6,'0')	ds_zeros,
	rpad(upper(substr(obter_razao_social(b.cd_cgc),1,45)),45,' ') nm_empresa,
	rpad(' ',10,' ') sigla_cedente,
	'01' tp_impressao,
	rpad(substr(obter_dados_pf_pj(null, b.cd_cgc, 'EC'),1,60),60,' ') ds_endereco,
	lpad(substr(obter_dados_pf_pj(null, b.cd_cgc, 'CEP'),1,8),8,'0') cd_cep,
	rpad(substr(nvl(obter_dados_pf_pj(null, b.cd_cgc, 'ME'),' '),1,20),20,' ') praca_devolucao,
	lpad(nvl(a.nr_remessa,'0'),7,'0') nr_seq_remessa,
	'N' indicador_conferencia,
	rpad('CBR454',8,' ') identificador_arquivo,
	lpad(' ',5,' ')	cd_personalizacao,
	lpad(substr(nvl(d.cd_convenio_banco,'0'), 1, 7), 7,'0') cd_convenio_banco
into	ie_tipo_registro_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	cd_cgc_cedente_w,
	nr_digito_verificador_w,
	cd_carteira_w,
	cd_variacao_carteira_w,
	ds_zeros_w,
	nm_empresa_w,
	ds_sigla_cedente_w,
	tp_impressao_w,
	ds_endereco_w,
	cd_cep_w,
	ds_praca_devolucao_w,
	nr_seq_remessa_w,
	ds_indic_conferencia_w,
	ds_ident_arquivo_w,
	cd_personalizacao_w,
	cd_convenio_banco_w
from	agencia_bancaria 		e,
	banco_carteira 		d,
	banco_estabelecimento 	c,
	estabelecimento 		b,
	cobranca_escritural 	a	
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr	= d.nr_sequencia(+)
and	c.cd_agencia_bancaria	= e.cd_agencia_bancaria
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:= ie_tipo_registro_w || cd_agencia_bancaria_w || nr_digito_agencia_w || cd_cgc_cedente_w ||
		    nr_digito_verificador_w || cd_carteira_w || cd_variacao_carteira_w || ds_zeros_w || nm_empresa_w ||
		    ds_sigla_cedente_w || tp_impressao_w || ds_endereco_w || cd_cep_w || ds_praca_devolucao_w || nr_seq_remessa_w ||
		    ds_indic_conferencia_w || ds_brancos_4_w || ds_ident_arquivo_w || ds_brancos_3_w || ds_brancos_38_w ||
		    cd_personalizacao_w || cd_convenio_banco_w || ds_brancos_3_w;

ds_conteudo_w	:= substr(elimina_acentuacao(ds_conteudo_w), 1, 255);
			
insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values (w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w,
	0);
/* FIM HEADER ARQUIVO */

/* DETALHE */
open C01;
loop
fetch C01 into	
	tp_documento_w,
	ie_documento_w,
	nr_cgc_cpf_w,
	nm_pagador_w,
	ds_praca_sacado_w,
	sg_estado_w,
	dt_emissao_w,
	dt_vencimento_w,
	ds_aceite_w,
	ie_especie_titulo_w,
	nr_nosso_numero_w,
	nr_titulo_w,
	cd_moeda_w,
	qt_moeda_variavel_w,
	vl_titulo_w,
	qt_dias_protesto_w,
	qt_total_parcelas_w,
	ds_endereco_w,
	cd_cep_w,
	ds_mensagem_1_w,
	ds_mensagem_2_w,
	ds_mensagem_3_w,
	ds_mensagem_4_w,
	ds_mensagem_5_w;
exit when C01%notfound;
	begin
	nr_seq_apres_w	:= nr_seq_apres_w + 1;

	ds_conteudo_w	:= '04' || '   ' || ds_mensagem_5_w || ds_brancos_5_w;
	
	ds_conteudo_w	:= substr(elimina_acentuacao(ds_conteudo_w), 1, 255);
	
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values (w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w,
		0);
	
	nr_seq_apres_w	:= nr_seq_apres_w + 1;

	ds_conteudo_w	:= '051111' || ds_mensagem_1_w || ds_mensagem_2_w || ds_mensagem_3_w || ds_mensagem_4_w || '    ';
	
	ds_conteudo_w	:= substr(elimina_acentuacao(ds_conteudo_w), 1, 255);
	
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values (w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w,
		0);
	
	nr_seq_apres_w	:= nr_seq_apres_w + 1;

	ds_conteudo_w	:= tp_documento_w || ie_documento_w || nr_cgc_cpf_w || nm_pagador_w || ds_endereco_w || cd_cep_w || ds_praca_sacado_w || sg_estado_w ||
			    dt_emissao_w || dt_vencimento_w || ds_aceite_w || ie_especie_titulo_w || nr_nosso_numero_w || nr_titulo_w || cd_moeda_w ||
			    qt_moeda_variavel_w || vl_titulo_w || qt_dias_protesto_w || ds_brancos_6_w || qt_total_parcelas_w;
	
	ds_conteudo_w	:= substr(elimina_acentuacao(ds_conteudo_w), 1, 255);
	
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values (w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w,
		0);
	end;	
end loop;
close C01;
/* FIM DETALHE ARQUIVO */

/* TRAILLER ARQUIVO */	
ds_conteudo_w	:= '99' || lpad(nr_seq_apres_w,15,0) || ds_brancos_233_w;

nr_seq_apres_w	:= nr_seq_apres_w + 1;

ds_conteudo_w	:= substr(elimina_acentuacao(ds_conteudo_w), 1, 255);

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values (w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w,
	0);
/* FIM TRAILLER ARQUIVO */

commit;

end sbc_gerar_cobr_bb_250_CBR454;
/