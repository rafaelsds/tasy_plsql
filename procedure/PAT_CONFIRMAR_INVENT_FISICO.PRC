create or replace
procedure pat_confirmar_invent_fisico(
		cd_estabelecimento_p	number,
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is 

dt_consistencia_w		pat_inventario.dt_consistencia%type;
nr_seq_inventario_w		pat_inventario.nr_sequencia%type;
cd_centro_custo_w		pat_bem.cd_centro_custo%type;
nr_seq_local_w			pat_bem.nr_seq_local%type;
nr_seq_bem_w			pat_bem.nr_sequencia%type;
cd_bem_w			pat_bem.cd_bem%type;


begin
/* Obter codigo do bem*/
select	a.cd_bem,
	a.nr_seq_bem,
	b.dt_consistencia,
	b.nr_sequencia
into	cd_bem_w,
	nr_seq_bem_w,
	dt_consistencia_w,
	nr_seq_inventario_w
from	pat_inventario b,
	pat_inventario_bem a
where	a.nr_seq_inventario = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

/* Obter o local do bem e o centro de custo do local do bem */
select	a.nr_seq_local,
	b.cd_centro_custo
into	nr_seq_local_w,
	cd_centro_custo_w
from	pat_bem a,
	pat_local b
where	a.nr_seq_local = b.nr_sequencia
and	a.nr_sequencia = nr_seq_bem_w;

/*	Atualiza o local e o centro de custo do inventário com os dados do bem, respectivamente
	e retira as consistências. */
update	pat_inventario_bem 
set	nr_seq_local	= nr_seq_local_w,
	cd_centro_custo	= cd_centro_custo_w
where	nr_sequencia	= nr_sequencia_p;

if	(dt_consistencia_w is not null) then
	begin
	update	pat_inventario a
	set	a.dt_consistencia = null
	where	a.nr_sequencia = nr_seq_inventario_w;
	end;
end if;

commit;

end pat_confirmar_invent_fisico;
/
