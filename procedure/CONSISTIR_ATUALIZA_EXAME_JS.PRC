create or replace
procedure consistir_atualiza_exame_js(	cd_material_exame_p		in out varchar2,
					nr_seq_exame_p			number,
					ie_pendente_amostra_p		varchar2,
					nr_prescricao_p			number,
					dt_prev_execucao_p		date,
					cd_setor_solicitacao_p		number,
					dt_coleta_p			date,
					ie_urgencia_p			varchar2,
					cd_setor_exclusivo_p		in out number,
					cd_procedimento_p		number,
					cd_setor_origem_p		number,
					nr_atendimento_p		number,
					nr_seq_proc_interno_p		number,
					ie_origem_proced_p		number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number,
					ie_origem_proced_user_p		number,						
					dt_prevista_retorno_p		out date,
					cd_setor_atendimento_p		out varchar2,
					cd_setor_coleta_p		out varchar2,
					cd_setor_entrega_p		out varchar2,
					qt_dia_entrega_p		out number,
					ie_emite_mapa_p			out varchar2,
					ds_hora_fixa_p			out varchar2,
					ie_data_resultado_p		out varchar2,
					qt_min_entrega_p		out number,
					ie_atualizar_recoleta_p 	out varchar2,
					ie_gerar_setor_p		out varchar2,
					ds_msg_info_p			out varchar2,
					ie_exclusao_p			Varchar2,
					ie_forma_atual_dt_result_p 	out Varchar2,
					qt_min_atraso_p 		out number 
					) is 

cd_material_exame_w	varchar2(20);
dt_prevista_retorno_w	date;

cd_setor_atendimento_w	varchar2(255);
cd_setor_coleta_w	varchar2(255);
cd_setor_entrega_w	varchar2(255);
qt_dia_entrega_w	number(3,0);
ie_emite_mapa_w		varchar2(1);
ds_hora_fixa_w		varchar2(10);
ie_data_resultado_w	varchar2(1);
qt_min_entrega_w	number(10,0);
ie_atualizar_recoleta_w	varchar2(1);

cd_setor_exclusivo_w	number(5,0);
ie_gerar_setor_w	varchar2(1);
ie_dia_semana_final_w 	number(10);
ie_forma_atual_dt_result_w	exame_lab_regra_setor.ie_atul_data_result%type;

begin

if	(cd_material_exame_p <> '')then
	begin
	
	select   b.cd_material_exame
	into	 cd_material_exame_w
	from     material_exame_lab b, 
		 exame_lab_material a
	where    b.nr_sequencia = a.nr_seq_material
	and      a.nr_seq_exame = nr_seq_exame_p
	and	a.ie_situacao = 'A'
	order by a.ie_prioridade;
	
	end;
end if;

if	(nr_seq_exame_p <> 0)then
	begin
	
	atualiza_dt_realizacao_exame(nr_seq_exame_p, nr_prescricao_p, dt_prev_execucao_p, dt_prevista_retorno_w);
	
	obter_setor_exame_lab_eup(nr_prescricao_p, nr_seq_exame_p, ie_pendente_amostra_p, cd_setor_solicitacao_p, cd_material_exame_p, dt_coleta_p, 'S', cd_setor_atendimento_w, cd_setor_coleta_w, cd_setor_entrega_w,
				qt_dia_entrega_w, ie_emite_mapa_w, ds_hora_fixa_w, ie_data_resultado_w, qt_min_entrega_w, ie_atualizar_recoleta_w, ie_urgencia_p, ie_dia_semana_final_w,ie_exclusao_p, ie_forma_atual_dt_result_w, qt_min_atraso_p, dt_prev_execucao_p);
	
	end;
end if;

if	(nvl(cd_setor_exclusivo_p, 0) = 0)then
	begin
	
	cd_setor_exclusivo_w	:= obter_setor_atend_proc_lab(cd_estabelecimento_p, cd_procedimento_p, ie_origem_proced_user_p, somente_numero(cd_setor_atendimento_w), cd_setor_origem_p, nm_usuario_p, nr_seq_exame_p, nr_atendimento_p);

	ie_gerar_setor_w	:= obter_se_gerar_setor_rotina(cd_estabelecimento_p, 916, cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, nr_seq_exame_p, cd_setor_entrega_p, nm_usuario_p);
	
	end;
end if;
ds_msg_info_p		:= obter_texto_tasy(119670, wheb_usuario_pck.get_nr_seq_idioma);
cd_material_exame_p	:= cd_material_exame_w;
dt_prevista_retorno_p	:= dt_prevista_retorno_w;
cd_setor_atendimento_p	:= replace(replace(cd_setor_atendimento_w,'(',''),')','');
cd_setor_coleta_p	:= replace(replace(cd_setor_coleta_w,'(',''),')','');
cd_setor_entrega_p	:= replace(replace(cd_setor_entrega_w,'(',''),')','');
qt_dia_entrega_p	:= qt_dia_entrega_w;
ie_emite_mapa_p		:= ie_emite_mapa_w;
ds_hora_fixa_p		:= ds_hora_fixa_w;
ie_data_resultado_p	:= ie_data_resultado_w;
qt_min_entrega_p	:= qt_min_entrega_w;
ie_atualizar_recoleta_p	:= ie_atualizar_recoleta_w;
cd_setor_exclusivo_p	:= cd_setor_exclusivo_w;
ie_gerar_setor_p	:= ie_gerar_setor_w;
ie_forma_atual_dt_result_p := ie_forma_atual_dt_result_w;

commit;

end consistir_atualiza_exame_js;
/