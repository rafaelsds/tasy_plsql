create or replace
procedure emitir_bloqueto_titulo(	ie_tipo_titulo_p	number,
				nr_titulo_p	number) is
begin

if (nr_titulo_p is not null) then
	
update	titulo_receber
set	ie_tipo_titulo       = ie_tipo_titulo_p,
	dt_emissao_bloqueto  = sysdate
where	nr_titulo            = nr_titulo_p;
	
end if;

commit;

end emitir_bloqueto_titulo;
/