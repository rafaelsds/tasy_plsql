create or replace procedure Gerar_Npt_Harris_Benedict(nr_seq_npt_cpoe_p        number
			           ,nr_seq_protocolo_p       number
				   ,qt_peso_p                number
				   ,qt_tmb_p                 number
				   ,pr_proteina_p            number
				   ,pr_carboidrato_p         number
				   ,pr_lipidio_p             number
				   ,nr_seq_fator_stress_p    number
				   ,nr_seq_fator_ativ_p      number
				   ,nm_usuario_p             varchar2
				   ,cd_estabelecimento_p     number
				   ,param_1034_p             number
				   ,param_1035_p             number
				   ,ds_json_p			      out clob ) is
        
nr_seq_elem_prescr_w          number(10);
nr_seq_nut_elem_mat_w         number(10);
ie_novo_produto_w	      varchar2(1);
qt_formula_lipideo_w 	      number(15,4);
qt_formula_carb_w             number(15,4);
ie_via_administracao_w	      varchar2(1);
cd_unidade_medida_elem_w      varchar2(30);
nr_seq_elemento_w	      number(10);
qt_elemento_w		      number(15,4);
qt_diaria_w	              number(15,4);
qt_vol_diaria_w		      number(18,6);
qt_dose_diaria_w              number(18,6);
ie_prod_adicional_elem_w      varchar2(1);
ie_prim_fase_w		      varchar2(1);
ie_seg_fase_w                 varchar2(1);
ie_terc_fase_w                varchar2(1);
ie_quar_fase_w                varchar2(1);
ie_npt_w                      varchar2(1);
qt_osmolaridade_w	      number(15,2);
ie_tipo_elemento_w	      varchar2(1);	
ie_glutamina_w		      varchar2(1);
nr_sequencia_mat_w	      number(10);
cd_material_prod_w	      number(6);
qt_volume_w		      number(18,6);
qt_vol_1_fase_w		      number(15,4);
qt_vol_2_fase_w               number(15,4);
qt_vol_3_fase_w               number(15,4);
qt_vol_4_fase_w               number(15,4);
qt_dose_w		      number(18,6);
cd_unidade_medida_prod_w      varchar2(30);	
ie_prod_adicional_prod_w      varchar2(1);	
ie_somar_volume_w	      varchar2(1);
qt_elem_kg_dia_w              number(15,4);
qt_kcal_w                     cpoe_npt_elemento.qt_kcal%type;
qt_grama_nitrogenio_w         number(15,4);
qt_vol_prot_w                 number(15,4);
qt_nitrogenio_prod_w          number(15,4);
qt_nitrogenio_w               number(15,4);
qt_nitrogenio_total_w	      number(15,4);                
qt_grama_proteina_kg_dia_w    number(15,4);
qt_kcal_proteina_w            number(15,4);
qt_kcal_lipidio_w             number(15,4);
qt_kcal_carboidrato_w         number(15,4);
pr_conc_proteina_solucao_w    number(20,6);	
pr_conc_lipidio_solucao_w     number(20,6);
pr_conc_glic_solucao_w        number(20,6);
qt_kcal_total_w               number(20,6);
qt_kcal_kg_w                  number(15,4);
qt_rel_cal_nit_w              number(15,4);
qt_vel_inf_glicose_w          number(15,4);
qt_kcal_proteico_w            number(15,4);
qt_kcal_nao_proteico_w        number(15,4);
nr_horas_validade_w	      number(5,0);
ie_calculo_auto_w	      varchar2(1);
qt_volume_total_w	      number(15,4);
qt_gotejo_w		      number(15,2);
qt_osmolaridade_total_w       number(15,2);
qt_fator_stress_w	      number(15,4);
qt_fator_ativ_w		      number(15,4);
qt_conversao_ml_w	      number(15,4);
qt_conversao_kcal_w	      nut_elem_material.qt_conversao_kcal%type;
qt_elem_lipidio_w	      number(1);
qt_lipidio_prop_w	      number(15,4);
qt_diaria_glutamina_w         number(15,4);
qt_diaria_aa_w                number(15,4);
aa_glut_ratio_w               number(15,4);
qt_adjusted_glut_w            number(15,4);  
qt_adjusted_aa_w              number(15,4);
aa_qt_conversao_ml_w          number(15,4);
qt_elem_proteina_w            number(3);         
ds_json_elem_w                clob;
ds_json_prod_w                clob;
ds_json_cpoe_w                clob;
qt_limite_litro_w	      nut_elemento.qt_limite_litro%type;
calc_conforme_protocolo_w     varchar2(1);
qt_volume_protocolo_w         number(18,6);
EXEC_W                        varchar2(4000);
qt_soma_perc_npt_w            number;
cd_unid_med_usua_g_w          varchar2(30);
cd_unid_medida_ori_w		      varchar2(30);
cd_unid_medida_dest_w		      varchar2(30);
cd_unid_med_cons_w			      varchar2(30);
qt_conversao_und_ori_w		    number(18,6);
qt_conversao_und_dest_w	     	number(18,6);


        
CURSOR c01 IS
/** CPOE_NPT_ELEMENTO **/
SELECT a.cd_unidade_medida
      ,a.nr_seq_elemento
      ,a.qt_elemento			
      ,a.ie_prod_adicional	
      ,NVL(a.ie_prim_fase, 'N')
      ,NVL(a.ie_seg_fase, 'N')
      ,NVL(a.ie_terc_fase, 'N')
      ,NVL(a.ie_quar_fase, 'N')
      ,'S'
      ,a.qt_osmolaridade
      ,c.ie_tipo_elemento
      ,c.ie_glutamina
/** CPOE_NPT_PRODUTO **/
      ,d.nr_sequencia
      ,b.cd_material
      ,b.qt_volume
      ,b.qt_vol_1_fase
      ,b.qt_vol_2_fase
      ,b.qt_vol_3_fase
      ,b.qt_vol_4_fase	
      ,b.qt_dose
      ,b.cd_unidade_medida
      ,b.ie_prod_adicional
      ,NVL(b.ie_somar_volume, 'S')
      ,d.qt_nitrogenio
      ,NVL(d.qt_conversao_ml,0)
      ,nvl(d.qt_conversao_kcal,0)
FROM  protocolo_npt_item a,
      protocolo_npt_prod b,
      nut_elemento c,
      nut_elem_material d	
WHERE a.nr_seq_protocolo = nr_seq_protocolo_p
AND   c.nr_sequencia = a.nr_seq_elemento
AND   a.nr_seq_protocolo = b.nr_seq_protocolo
AND   b.cd_material = d.cd_material
AND   a.nr_seq_elemento = d.nr_seq_elemento 
AND   d.ie_situacao = 'A'
AND   nvl(c.IE_SITUACAO, 'A') = 'A'
AND   nvl(c.IE_DIETA_ENTERAL,'N') = 'N'
ORDER BY NVL(a.nr_seq_apres, 999);    
BEGIN
  if (pr_proteina_p is not null and pr_lipidio_p is not null and pr_carboidrato_p is not null) then
      begin
        EXEC_W := 'CALL GER_NPT_HARRIS_BENEDI_MD_PCK.OBTER_SOMA_PERCENTUAIS_NPT_MD(:1,:2,:3) INTO :result';
        EXECUTE IMMEDIATE EXEC_W USING IN pr_proteina_p,
                                       IN pr_lipidio_p,
                                       IN pr_carboidrato_p,
                                       OUT qt_soma_perc_npt_w;
      exception
        when others then
            qt_soma_perc_npt_w := null;
      end;

      if (qt_soma_perc_npt_w <> 100) then
        wheb_mensagem_pck.exibir_mensagem_abort(176792);		
      end if;
  end if;

  Obter_Param_Usuario(924, 1167, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_novo_produto_w);
  Obter_Param_Usuario(924, 1085, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, calc_conforme_protocolo_w);
  
  qt_formula_lipideo_w  := param_1034_p;
  qt_formula_carb_w     := param_1035_p;
  
  SELECT ie_via_administracao
  INTO ie_via_administracao_w
  FROM protocolo_npt
  WHERE nr_sequencia = nr_seq_protocolo_p;

  /** CPOE_DIETA attributes **/
  qt_nitrogenio_total_w := 0;
  qt_grama_proteina_kg_dia_w := 0;
  qt_kcal_proteina_w := 0;
  qt_kcal_lipidio_w := 0;
  qt_kcal_carboidrato_w := 0;
  pr_conc_proteina_solucao_w := 0;
  pr_conc_lipidio_solucao_w := 0;
  pr_conc_glic_solucao_w := 0;
  qt_volume_total_w := 0;  

  ds_json_elem_w := null;
  ds_json_prod_w := null;
  
  select	nvl(max(qt_fator),1)
	into	qt_fator_stress_w
	from	nut_fator_stress
	where	nr_sequencia	= nr_seq_fator_stress_p;

	select	nvl(max(qt_fator),1)
	into	qt_fator_ativ_w
	from	nut_fator_ativ
	where	nr_sequencia	= nr_seq_fator_ativ_p;
  
  begin
    EXEC_W := 'CALL GER_NPT_HARRIS_BENEDI_MD_PCK.OBTER_KCAL_TOTAL_NPT_MD(:1,:2,:3) INTO :result';
    
    EXECUTE IMMEDIATE EXEC_W USING IN qt_tmb_p,
                                   IN qt_fator_Ativ_w,
                                   IN qt_fator_stress_w,
                                   OUT qt_kcal_total_w;
  exception
    when others then
        qt_kcal_total_w := null;
  end;

  begin
    EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_ELEM_KCAL_NTP_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9); END;';

    EXECUTE IMMEDIATE EXEC_W USING IN qt_kcal_total_w,
                                   IN pr_proteina_p,
                                   IN pr_lipidio_p,
                                   IN qt_fator_stress_w,
                                   IN qt_peso_p,
                                   OUT qt_kcal_proteina_w,
                                   OUT qt_kcal_lipidio_w,
                                   OUT qt_kcal_carboidrato_w,
                                   OUT qt_kcal_kg_w;
  exception
    when others then
        qt_kcal_proteina_w    := null;
        qt_kcal_lipidio_w     := null;
        qt_kcal_carboidrato_w := null;
        qt_kcal_kg_w          := null;
  end;

  SELECT COUNT(ie_tipo_elemento)
  INTO	qt_elem_lipidio_w
  FROM protocolo_npt_item a,
  	   nut_elemento b
  WHERE a.nr_seq_elemento = b.nr_sequencia
  AND a.nr_seq_protocolo = nr_seq_protocolo_p
  AND b.ie_tipo_elemento = 'L';
  
  SELECT COUNT(ie_tipo_elemento)
  INTO	qt_elem_proteina_w
  FROM protocolo_npt_item a,
  	   nut_elemento b
  WHERE a.nr_seq_elemento = b.nr_sequencia
  AND a.nr_seq_protocolo = nr_seq_protocolo_p
  AND b.ie_tipo_elemento = 'P';
  
  SELECT NVL(MAX(a.qt_elemento),0),
         NVL(MAX(c.qt_conversao_ml),0)
  INTO qt_diaria_aa_w,
       aa_qt_conversao_ml_w
  FROM protocolo_npt_item a,
       nut_elemento b,
  	 nut_elem_material c,
  	 protocolo_npt_prod d
  WHERE b.nr_sequencia = a.nr_seq_elemento
  AND a.nr_seq_protocolo = d.nr_seq_protocolo
  AND a.nr_seq_elemento = c.nr_seq_elemento
  AND d.cd_material = c.cd_material
  AND a.nr_seq_protocolo = nr_seq_protocolo_p
  AND b.ie_tipo_elemento = 'P'
  AND b.ie_glutamina = 'N';
  
  SELECT NVL(MAX(a.qt_elemento),0)
  INTO	qt_diaria_glutamina_w
  FROM protocolo_npt_item a,
	   nut_elemento b 
  WHERE a.nr_seq_elemento = b.nr_sequencia
  AND a.nr_seq_protocolo = nr_seq_protocolo_p
  AND b.ie_tipo_elemento = 'P'
  AND b.ie_glutamina = 'S';
 
  if (qt_elem_proteina_w > 1) then 
    begin
      EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_DADOS_AJUST_NPT_MD(:1,:2,:3,:4,:5,:6,:7); END;';

      EXECUTE IMMEDIATE EXEC_W USING IN qt_diaria_aa_w,
                                     IN qt_diaria_glutamina_w,
                                     IN qt_kcal_proteina_w,
                                     IN aa_qt_conversao_ml_w,
                                     OUT aa_glut_ratio_w,
                                     OUT qt_adjusted_glut_w,
                                     OUT qt_adjusted_aa_w;
    exception
      when others then
          aa_glut_ratio_w     := null;
          qt_adjusted_glut_w  := null;
          qt_adjusted_aa_w    := null;
    end;
  end if;  

  OPEN c01;
  LOOP
  FETCH c01 INTO
      /**ELEMENTO**/
      cd_unidade_medida_elem_w
      ,nr_seq_elemento_w
      ,qt_elemento_w
      ,ie_prod_adicional_elem_w
      ,ie_prim_fase_w
      ,ie_seg_fase_w
      ,ie_terc_fase_w
      ,ie_quar_fase_w
      ,ie_npt_w
      ,qt_osmolaridade_w
      ,ie_tipo_elemento_w
      ,ie_glutamina_w
      /**PRODUTO**/
      ,nr_sequencia_mat_w
      ,cd_material_prod_w
      ,qt_volume_w
      ,qt_vol_1_fase_w
      ,qt_vol_2_fase_w
      ,qt_vol_3_fase_w
      ,qt_vol_4_fase_w
      ,qt_dose_w
      ,cd_unidade_medida_prod_w
      ,ie_prod_adicional_prod_w
      ,ie_somar_volume_w
      ,qt_nitrogenio_w
      ,qt_conversao_ml_w
      ,qt_conversao_kcal_w;
  EXIT WHEN c01%notfound;	
  
  --GENERATES PKs for CPOE_NPT_ELEMENTO and CPOE_NPT_PRODUTO
  SELECT cpoe_npt_elemento_seq.nextval
  INTO	nr_seq_elem_prescr_w
  FROM	dual;
  
  SELECT cpoe_npt_produto_seq.nextval
  INTO nr_seq_nut_elem_mat_w
  FROM dual;
   
  qt_kcal_w		:= 0;
  qt_grama_nitrogenio_w := 0; 
  qt_diaria_w := 0;
  qt_vol_diaria_w := qt_volume_w;
  qt_dose_diaria_w := qt_volume_w;
 
  cd_unid_medida_ori_w	:= upper(obter_unid_med_usua('ml'));  
  cd_unid_medida_dest_w	:= upper(cd_unidade_medida_prod_w);

  begin
      select nvl(a.cd_unidade_medida_consumo, b.cd_unidade_medida_consumo)
      into	 cd_unid_med_cons_w
      from	 material_estab a,
            material b
      where	b.cd_material	= a.cd_material
      and	  a.cd_material	= cd_material_prod_w
      and  	a.cd_estabelecimento = cd_estabelecimento_p;
  exception
     when others then
        begin
            select	cd_unidade_medida_consumo
            into	cd_unid_med_cons_w
            from	material
            where	cd_material = cd_material_prod_w;
        exception
        when others then
          null;
        end;
  end;

  select nvl(max(qt_conversao),0)
  into	 qt_conversao_und_ori_w
  from	 material_conversao_unidade
  where	 upper(cd_unidade_medida) = cd_unid_medida_ori_w
  and		 cd_material = cd_material_prod_w;

  select nvl(max(qt_conversao),0)
  into	 qt_conversao_und_dest_w
  from	 material_conversao_unidade
  where	 upper(cd_unidade_medida) = cd_unid_medida_dest_w
  and		 cd_material = cd_material_prod_w;

  if	(ie_tipo_elemento_w = 'C') then
    begin
      EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_CARB_NPT_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12); END;';

      EXECUTE IMMEDIATE EXEC_W USING IN qt_kcal_carboidrato_w,
                                     IN qt_formula_carb_w,
                                     IN qt_conversao_ml_w,
                                     IN cd_unid_medida_ori_w,
                                     IN cd_unid_medida_dest_w,
                                     IN cd_unid_med_cons_w,
                                     IN qt_conversao_und_ori_w,
                                     IN qt_conversao_und_dest_w,
                                     OUT qt_diaria_w,
                                     OUT qt_vol_diaria_w,
                                     OUT pr_conc_glic_solucao_w,
                                     IN OUT qt_dose_diaria_w;
    exception
      when others then
          qt_diaria_w            := null;
          qt_vol_diaria_w        := null;
          pr_conc_glic_solucao_w := null;
          qt_dose_diaria_w       := null;
    end;

  elsif	(ie_tipo_elemento_w = 'P') then 
    begin
      EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_PROT_NPT_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,
                                                                            :11,:12,:13,:14,:15,:16,:17,:18,:19,:20
                                                                            ,:21,:22,:23); END;';

      EXECUTE IMMEDIATE EXEC_W USING IN ie_tipo_elemento_w,
                                     IN qt_kcal_proteina_w,
                                     IN qt_conversao_ml_w,
                                     IN cd_unid_medida_ori_w,
                                     IN cd_unid_medida_dest_w,
                                     IN cd_unid_med_cons_w,
                                     IN qt_conversao_und_ori_w,
                                     IN qt_conversao_und_dest_w,
                                     IN ie_glutamina_w,
                                     IN qt_conversao_kcal_w, 
                                     IN qt_formula_lipideo_w,  
                                     IN qt_kcal_lipidio_w, 
                                     IN qt_elem_lipidio_w, 
                                     IN cd_unid_med_usua_g_w,  
                                     OUT qt_diaria_w,
                                     OUT qt_vol_diaria_w,
                                     IN OUT qt_dose_diaria_w,
                                     IN OUT qt_grama_nitrogenio_w,
                                     OUT qt_kcal_w,
                                     IN OUT pr_conc_proteina_solucao_w,
                                     IN OUT qt_nitrogenio_total_w,
                                     OUT qt_lipidio_prop_w,
                                     IN OUT pr_conc_lipidio_solucao_w;

    exception
      when others then
          qt_diaria_w                := null;
          qt_vol_diaria_w            := null;
          qt_dose_diaria_w           := null;
          qt_grama_nitrogenio_w      := null;
          qt_kcal_w                  := null;
          pr_conc_proteina_solucao_w := null;
          qt_nitrogenio_total_w      := null;

          qt_lipidio_prop_w          := null;
          pr_conc_lipidio_solucao_w  := null;
    end;

  elsif	(ie_tipo_elemento_w = 'L') then      
    cd_unid_med_usua_g_w	:= upper(obter_unid_med_usua('g'));  

    select nvl(max(qt_conversao),0)
    into	 qt_conversao_und_ori_w
    from	 material_conversao_unidade
    where	 upper(cd_unidade_medida) = cd_unid_med_usua_g_w
    and		 cd_material = cd_material_prod_w;

    begin
      EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_PROT_NPT_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,
                                                                            :11,:12,:13,:14,:15,:16,:17,:18,:19,:20
                                                                            ,:21,:22,:23); END;';


      EXECUTE IMMEDIATE EXEC_W USING IN ie_tipo_elemento_w,
                                     IN qt_kcal_proteina_w,
                                     IN qt_conversao_ml_w,
                                     IN cd_unid_medida_ori_w,
                                     IN cd_unid_medida_dest_w,
                                     IN cd_unid_med_cons_w,
                                     IN qt_conversao_und_ori_w,
                                     IN qt_conversao_und_dest_w,
                                     IN ie_glutamina_w,
                                     IN qt_conversao_kcal_w, 
                                     IN qt_formula_lipideo_w,  
                                     IN qt_kcal_lipidio_w, 
                                     IN qt_elem_lipidio_w, 
                                     IN cd_unid_med_usua_g_w,  
                                     OUT qt_diaria_w,
                                     OUT qt_vol_diaria_w,
                                     IN OUT qt_dose_diaria_w,
                                     IN OUT qt_grama_nitrogenio_w,
                                     OUT qt_kcal_w,
                                     IN OUT pr_conc_proteina_solucao_w,
                                     IN OUT qt_nitrogenio_total_w,
                                     OUT qt_lipidio_prop_w,
                                     IN OUT pr_conc_lipidio_solucao_w;

    exception
      when others then
          qt_diaria_w                := null;
          qt_vol_diaria_w            := null;
          qt_dose_diaria_w           := null;
          qt_grama_nitrogenio_w      := null;
          qt_kcal_w                  := null;
          pr_conc_proteina_solucao_w := null;
          qt_nitrogenio_total_w      := null;

          qt_lipidio_prop_w          := null;
          pr_conc_lipidio_solucao_w  := null;
    end;
  end if;
  
  begin
      EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_VOL_PROT_NPT_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,
                                                                                :11,:12,:13); END;';

      EXECUTE IMMEDIATE EXEC_W USING IN qt_volume_w,
                                     IN calc_conforme_protocolo_w,
                                     IN qt_elemento_w, 
                                     IN qt_vol_diaria_w, 
                                     IN qt_conversao_ml_w,
                                     IN qt_peso_p, 
                                     IN ie_tipo_elemento_w,
                                     IN qt_formula_carb_w, 
                                     IN qt_formula_lipideo_w, 
                                     OUT qt_volume_protocolo_w,
                                     OUT qt_diaria_w,
                                     OUT qt_elem_kg_dia_w,
                                     IN OUT qt_kcal_w;                  

  exception
      when others then
        qt_volume_protocolo_w := null;
        qt_diaria_w           := null;
        qt_elem_kg_dia_w      := null;
        qt_kcal_w             := null;
  end;

  SELECT MAX(d.qt_limite_litro)
  INTO   qt_limite_litro_w
  FROM   protocolo_npt_prod a,
	 protocolo_npt_item b,
	 nut_elem_material c,
	 nut_elemento d
  WHERE  a.nr_seq_protocolo = nr_seq_protocolo_p
  AND    a.cd_material = cd_material_prod_w
  AND	 a.cd_material = c.cd_material
  AND    a.nr_seq_protocolo = b.nr_seq_protocolo
  AND	 b.nr_seq_elemento  = c.nr_seq_elemento
  AND	 b.nr_seq_elemento  = d.nr_sequencia
  AND	 c.nr_seq_elemento  = d.nr_sequencia
  AND	 NVL(c.ie_tipo,'NPT')	= 'NPT'
  AND	 NVL(d.qt_limite_litro,0) < NVL(qt_diaria_w,0)
  AND	 NVL(d.qt_limite_litro,0) > 0
  AND	 NVL(c.ie_situacao,'A') = 'A'
  AND	 NVL(d.ie_limite_concentracao,'N') = 'S';
  
  if (qt_limite_litro_w is not null or ie_somar_volume_w = 'S') then
    begin
        EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_KCAL_ELEMENTOS_NPT_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,
                                                                                  :11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22); END;';
        
        EXECUTE IMMEDIATE EXEC_W USING IN qt_limite_litro_w,
                                      IN qt_peso_p,
                                      IN ie_tipo_elemento_w, 
                                      IN qt_formula_carb_w,
                                      IN qt_formula_lipideo_w,       
                                      IN qt_conversao_ml_w,
                                      IN cd_unidade_medida_elem_w, 
                                      IN cd_unidade_medida_prod_w, 
                                      IN ie_somar_volume_w, 
                                      IN OUT qt_diaria_w,
                                      IN OUT qt_elem_kg_dia_w,
                                      IN OUT qt_kcal_w,
                                      IN OUT qt_dose_diaria_w,
                                      IN OUT qt_vol_diaria_w,
                                      IN OUT pr_conc_glic_solucao_w,
                                      IN OUT qt_kcal_carboidrato_w,
                                      IN OUT qt_vel_inf_glicose_w,
                                      IN OUT pr_conc_proteina_solucao_w, 
                                      IN OUT qt_kcal_proteina_w, 
                                      IN OUT pr_conc_lipidio_solucao_w, 
                                      IN OUT qt_kcal_lipidio_w, 
                                      IN OUT qt_volume_total_w;

    exception
        when others then
          qt_diaria_w                := null;
          qt_elem_kg_dia_w           := null;
          qt_kcal_w                  := null;
          qt_dose_diaria_w           := null;
          qt_vol_diaria_w            := null;
          pr_conc_glic_solucao_w     := null; 
          qt_kcal_carboidrato_w      := null; 
          qt_vel_inf_glicose_w       := null;  
          pr_conc_proteina_solucao_w := null;
          qt_kcal_proteina_w         := null; 
          pr_conc_lipidio_solucao_w  := null;
          qt_kcal_lipidio_w          := null;
          qt_volume_total_w          := null;
    end;  
  end if;

  --ATTRIBUTES FOR ELEMENTS AND PRODUCTS    
  ds_json_elem_w := ds_json_elem_w || '{'
      || format_array_json('NR_SEQUENCIA', nr_seq_elem_prescr_w, 1)
      || format_array_json('DS_ELEMENTO', substr(obter_descricao_padrao('NUT_ELEMENTO','DS_ELEMENTO',nr_seq_elemento_w),1,40), 1)
	  || format_array_json('DS_PADRAO', substr(obter_padrao_elem_nut_pac(nr_seq_elemento_w),1,40), 1)
      || format_array_json('DS_UNIDADE_MEDIDA', substr(obter_desc_unid_med(cd_unidade_medida_elem_w),1,40), 1)
      || format_array_json('NM_USUARIO', nm_usuario_p, 1)
      || format_array_json('NR_SEQ_NPT_CPOE', nr_seq_npt_cpoe_p, 1)
      || format_array_json('CD_UNIDADE_MEDIDA', cd_unidade_medida_elem_w, 1)
      || format_array_json('NR_SEQ_ELEMENTO', nr_seq_elemento_w, 1)
      || format_array_json_number('QT_DIARIA', qt_diaria_w)
      || format_array_json_number('QT_KCAL', qt_kcal_w)
      || format_array_json_number('QT_PROTOCOLO', qt_elemento_w)
      || format_array_json_number('QT_ELEM_KG_DIA', qt_elem_kg_dia_w)
      || format_array_json('IE_PROD_ADICIONAL', ie_prod_adicional_elem_w, 1)      
      || format_array_json('IE_PRIM_FASE', ie_prim_fase_w, 1)
      || format_array_json('IE_SEG_FASE', ie_seg_fase_w, 1)
      || format_array_json('IE_TERC_FASE', ie_terc_fase_w, 1)
      || format_array_json('IE_QUAR_FASE', ie_quar_fase_w, 1)
      || format_array_json('IE_NPT', ie_npt_w, 1)      
      || format_array_json_number('QT_GRAMA_NITROGENIO', qt_grama_nitrogenio_w)
      || format_array_json('IE_TIPO_ELEMENTO', ie_tipo_elemento_w, 1)
      || format_array_json('IE_GLUTAMINA', ie_glutamina_w, 1);            
  ds_json_elem_w := substr(ds_json_elem_w, 1, length(ds_json_elem_w) -2) || '}, ';  
      
  ds_json_prod_w := ds_json_prod_w || '{'
      || format_array_json('NR_SEQUENCIA', nr_seq_nut_elem_mat_w, 1)
      || format_array_json('NM_USUARIO', nm_usuario_p, 1)
      || format_array_json('NR_SEQ_ELEM_MAT', nr_sequencia_mat_w, 1)
      || format_array_json('NR_SEQ_ELEMENTO', nr_seq_elem_prescr_w, 1)
      || format_array_json('NR_SEQ_NPT_CPOE', nr_seq_npt_cpoe_p, 1)      
      || format_array_json('CD_MATERIAL', cd_material_prod_w, 1)      
      || format_array_json_number('QT_VOLUME', qt_vol_diaria_w)
      || format_array_json_number('QT_VOL_1_FASE', qt_vol_1_fase_w)
      || format_array_json_number('QT_VOL_2_FASE', qt_vol_2_fase_w)
      || format_array_json_number('QT_VOL_3_FASE', qt_vol_3_fase_w)
      || format_array_json_number('QT_VOL_4_FASE', qt_vol_4_fase_w)
      || format_array_json_number('QT_PROTOCOLO', qt_volume_protocolo_w) 
      || format_array_json_number('QT_DOSE', nvl(qt_dose_diaria_w, qt_dose_w))
      || format_array_json('CD_UNIDADE_MEDIDA', cd_unidade_medida_prod_w, 1)
      || format_array_json('IE_PROD_ADICIONAL', ie_prod_adicional_prod_w, 1)
      || format_array_json('IE_SOMAR_VOLUME', ie_somar_volume_w, 1)      
      || format_array_json('DS_MATERIAL', substr(obter_desc_material(cd_material_prod_w),1,60), 1)
      || format_array_json('DS_UNIDADE_MEDIDA', substr(obter_desc_unid_med(cd_unidade_medida_prod_w),1,40), 1)      
      || format_array_json('IE_TIPO_ELEMENTO', ie_tipo_elemento_w, 1);
  ds_json_prod_w := substr(ds_json_prod_w, 1, length(ds_json_prod_w) -2) || '}, ';  
  
END loop;
CLOSE c01;

  ds_json_elem_w := '"CPOE_NPT_ELEMENTO": [' || substr(ds_json_elem_w, 1, length(ds_json_elem_w) -2) || ']';
  ds_json_prod_w := '"CPOE_NPT_PRODUTO": [' || substr(ds_json_prod_w, 1, length(ds_json_prod_w) -2) || ']';  
  
  begin
      EXEC_W := 'BEGIN GER_NPT_HARRIS_BENEDI_MD_PCK.CALCULA_QTDS_NPT_HARRIS_MD(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,
                                                                                :11,:12,:13,:14,:15); END;';

      EXECUTE IMMEDIATE EXEC_W USING IN pr_conc_proteina_solucao_w,
                                     IN qt_peso_p,
                                     IN qt_kcal_lipidio_w, 
                                     IN qt_kcal_carboidrato_w,
                                     IN qt_nitrogenio_total_w,       
                                     IN pr_conc_glic_solucao_w,
                                     IN qt_kcal_proteina_w, 
                                     IN qt_kcal_total_w, 
                                     IN qt_volume_total_w, 			  
                                     OUT qt_grama_proteina_kg_dia_w,
                                     OUT qt_rel_cal_nit_w,
                                     OUT qt_vel_inf_glicose_w,
                                     OUT qt_kcal_proteico_w,
                                     OUT qt_kcal_nao_proteico_w,
                                     OUT qt_gotejo_w;

  exception
      when others then
        qt_grama_proteina_kg_dia_w   := null;
        qt_rel_cal_nit_w             := null; 
        qt_vel_inf_glicose_w         := null;
        qt_kcal_proteico_w           := null;
        qt_kcal_nao_proteico_w       := null; 
        qt_gotejo_w                  := null;
  end;

    ds_json_cpoe_w := '{'
      || format_array_json_number('QT_KCAL_TOTAL', qt_kcal_total_w)
      || format_array_json_number('QT_KCAL_KG', qt_kcal_kg_w)      
      || format_array_json_number('PR_PROTEINA', pr_proteina_p)
      || format_array_json_number('PR_LIPIDIO', pr_lipidio_p)
      || format_array_json_number('PR_CARBOIDRATO', pr_carboidrato_p)
      || format_array_json_number('QT_KCAL_PROTEINA', qt_kcal_proteina_w)
      || format_array_json_number('QT_KCAL_LIPIDIO', qt_kcal_lipidio_w)
      || format_array_json_number('QT_KCAL_CARBOIDRATO', qt_kcal_carboidrato_w)
      || format_array_json_number('PR_CONC_PROTEINA_SOLUCAO', pr_conc_proteina_solucao_w)
      || format_array_json_number('PR_CONC_LIPIDIO_SOLUCAO', pr_conc_lipidio_solucao_w)
      || format_array_json_number('PR_CONC_GLIC_SOLUCAO', pr_conc_glic_solucao_w)      
      || format_array_json_number('QT_GRAMA_PROTEINA_KG_DIA', qt_grama_proteina_kg_dia_w)	
      || format_array_json_number('QT_GRAMA_NITROGENIO', qt_nitrogenio_total_w)			
      || format_array_json_number('QT_REL_CAL_NIT', qt_rel_cal_nit_w)
      || format_array_json_number('QT_VEL_INF_GLICOSE', qt_vel_inf_glicose_w)
      || format_array_json_number('QT_KCAL_PROTEICO', qt_kcal_proteico_w)
      || format_array_json_number('QT_KCAL_NAO_PROTEICO', qt_kcal_nao_proteico_w)
      || format_array_json_number('QT_GOTEJO_NPT', qt_gotejo_w)      
      || format_array_json_number('QT_VOLUME_DIARIO', qt_volume_total_w)
      || format_array_json_number('NR_SEQ_PROTOCOLO', nr_seq_protocolo_p);
      ds_json_cpoe_w := substr(ds_json_cpoe_w, 1, length(ds_json_cpoe_w) -2) || '}, ';                              
  
  ds_json_cpoe_w := substr(ds_json_cpoe_w, 1, length(ds_json_cpoe_w) -2);
  
  ds_json_p := '{"CPOE_DIETA": ' || ds_json_cpoe_w || ', '
                  || ds_json_elem_w || ', '
                  || ds_json_prod_w ||'}';                      

END Gerar_Npt_Harris_Benedict;
/