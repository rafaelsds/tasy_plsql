create or replace
procedure UB_GERAR_COBR_DEB_CAIXA_150
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is
-- OS 410127
-- Layout Padr�o Febraban 240 posi��es V04
-- AGOSTO/1998

ds_conteudo_w		varchar2(150);
qt_reg_arquivo_w		number(10) := 0;

/* Header - A - Trailler - Z*/
dt_geracao_arquivo_w	varchar2(8);
ds_banco_w		varchar2(20);
cd_banco_w		varchar2(3);
nm_empresa_w		varchar2(20);
cd_convenio_banco_w	varchar2(20);
vl_total_w		varchar2(17);
vl_soma_w		number(15,2) := 0;

/* Registros */
ie_ident_cliente_w		varchar2(25);
ie_ident_cliente_2_w	varchar2(25);
cd_agencia_bancaria_w	varchar2(4);
nr_conta_w		varchar2(8);
ie_digito_w		varchar2(3);
ds_ocorrencia_1_w		varchar2(40);
ds_ocorrencia_2_w		varchar2(40);
ds_ocorencia_w		varchar2(60);
dt_vencimento_w		varchar2(8);
vl_debito_w		varchar2(15);
cd_moeda_w		varchar2(2);
vl_cobranca_w		number(15,2) := 0;
nr_titulo_w		varchar2(60);
nr_seq_pagador_w		number(10);
cd_operacao_w		varchar2(3);
nr_seq_mensalidade_w	titulo_receber.nr_seq_mensalidade%type;
nr_seq_mens_segurado_w	titulo_receber.nr_seq_mens_segurado%type;
cd_pessoa_fisica_w	titulo_receber.cd_pessoa_fisica%type;

/* Bracos - Zeros */
ds_brancos_52_w		varchar2(52);
ds_brancos_25_w		varchar2(97);
ds_brancos_20_w		varchar2(20);
ds_brancos_109_w		varchar2(109);

dt_atualizacao_nrec_w	date;
nr_remessa_w		number(10);

Cursor C01 is
	select	lpad(nvl(nvl(pls_obter_dados_pagador_fin(e.nr_seq_pagador, 'A'), b.cd_agencia_bancaria), '0'), 4, '0') cd_agencia_bancaria,
		lpad(substr(nvl(nvl(pls_obter_dados_pagador_fin(e.nr_seq_pagador, 'C'), b.nr_conta),'0'),4,11), 8, '0') nr_conta,
		rpad(substr(nvl(nvl(pls_obter_dados_pagador_fin(e.nr_seq_pagador, 'DC'), b.ie_digito_conta), '   '), 1, 2), 3, ' ') ie_digito,
		rpad(' ',40,' ') ds_ocorrencia_1,
		rpad(' ',40,' ') ds_ocorrencia_2,
		rpad(' ',60,' ') ds_ocorencia,
		rpad(substr(to_char(d.dt_pagamento_previsto, 'YYYYMMDD'), 1, 8), 8, ' ') dt_vencimento,
		lpad(replace(to_char(b.vl_cobranca, 'fm0000000000000.00'), '.', ''), 15, '0') vl_debito,
		b.vl_cobranca,
		'03' cd_moeda,
		rpad(d.nr_titulo, 60, ' ') nr_titulo_w,
		e.nr_seq_pagador,
		lpad(substr(nvl(nvl(pls_obter_dados_pagador_fin(e.nr_seq_pagador, 'C'), b.nr_conta),'0'),1,3), 3, '0') cd_operacao,
		d.cd_pessoa_fisica,
		d.nr_seq_mensalidade,
		d.nr_seq_mens_segurado
	from	pls_mensalidade e,
		titulo_receber d,
		banco_estabelecimento c,
		titulo_receber_cobr b,
		cobranca_escritural a
	where	a.nr_seq_conta_banco	= c.nr_sequencia 
	and	b.nr_seq_cobranca		= a.nr_sequencia
	and	b.nr_titulo			= d.nr_titulo
	and	d.nr_seq_mensalidade	= e.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',52,' '),
	lpad(' ',25,' '),
	lpad(' ',20,' '),
	lpad(' ',109,' ')
into	ds_brancos_52_w,
	ds_brancos_25_w,
	ds_brancos_20_w,
	ds_brancos_109_w
from	dual;

qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;

/* Header A */
select	to_char(sysdate,'YYYYMMDD') dt_geracao_arquivo,
	rpad(substr(upper(elimina_acentuacao(obter_nome_banco(c.cd_banco))),1,20),20,' ') ds_banco,
	lpad(c.cd_banco,3,'0') cd_banco,
	rpad(upper(substr(elimina_acentuacao(obter_razao_social(b.cd_cgc)),1,20)),20,' ') nm_empresa,
	rpad(nvl(c.cd_conv_banco_deb,c.cd_convenio_banco),20,' ') cd_convenio_banco,
	nvl(a.nr_remessa,a.nr_sequencia)
into	dt_geracao_arquivo_w,
	ds_banco_w,
	cd_banco_w,
	nm_empresa_w,
	cd_convenio_banco_w,
	nr_remessa_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	'A1' || cd_convenio_banco_w || nm_empresa_w || cd_banco_w || ds_banco_w || dt_geracao_arquivo_w || 
			lpad(nr_remessa_w, 6, '0') || '04' || 'DEBITO AUTOMATICO' || ds_brancos_52_w;
			
insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	qt_reg_arquivo_w);
	
open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	nr_conta_w,
	ie_digito_w,
	ds_ocorrencia_1_w,
	ds_ocorrencia_2_w,
	ds_ocorencia_w,
	dt_vencimento_w,
	vl_debito_w,
	vl_cobranca_w,
	cd_moeda_w,
	nr_titulo_w,
	nr_seq_pagador_w,
	cd_operacao_w,
	cd_pessoa_fisica_w,
	nr_seq_mensalidade_w,
	nr_seq_mens_segurado_w;
exit when C01%notfound;
	begin

	select	substr(pls_obter_dados_segurado(max(x.nr_seq_segurado),'C'),1,25)
	into	ie_ident_cliente_w
	from	pls_segurado y,
		pls_mensalidade_segurado x
	where	y.cd_pessoa_fisica		= cd_pessoa_fisica_w
	and	x.nr_seq_segurado		= y.nr_sequencia
	and	x.nr_sequencia		= nvl(nr_seq_mens_segurado_w,x.nr_sequencia)
	and	x.nr_seq_mensalidade	= nr_seq_mensalidade_w;

	if	(ie_ident_cliente_w	is null) then

		select	substr(pls_obter_dados_segurado(max(x.nr_seq_segurado),'C'),1,25)
		into	ie_ident_cliente_w
		from	pls_mensalidade_segurado x
		where	x.nr_sequencia		= nvl(nr_seq_mens_segurado_w,x.nr_sequencia)
		and	x.nr_seq_mensalidade	= nr_seq_mensalidade_w;

	end if;

	ie_ident_cliente_w	:= rpad(nvl(ie_ident_cliente_w,'0'),25,' ');

	vl_soma_w	:= nvl(vl_soma_w,0) + nvl(vl_cobranca_w,0);

	qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
			
	-- Registro E 
	ds_conteudo_w	:= 'E' || ie_ident_cliente_w || cd_agencia_bancaria_w || cd_operacao_w || nr_conta_w || ie_digito_w ||
				dt_vencimento_w || vl_debito_w || cd_moeda_w || nr_titulo_w || ds_brancos_20_w || '0';
				
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		qt_reg_arquivo_w);	
	end;
end loop;
close C01;

/* Trailler Z */
vl_total_w	:= lpad(somente_numero(to_char(vl_soma_w,'999999999999990.00')),17,'0');

qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;

ds_conteudo_w	:= 'Z' || lpad(qt_reg_arquivo_w, 6, '0') || vl_total_w || lpad('0', 17, '0') || ds_brancos_109_w;

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	qt_reg_arquivo_w);

commit;

end UB_GERAR_COBR_DEB_CAIXA_150;
/
