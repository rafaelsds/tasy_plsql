CREATE OR REPLACE
Procedure Gerar_medic_diag_checkup
				(	nr_sequencia_p	number,
					ds_retorno_p	out varchar2) is


ds_receita_w		varchar2(2500);
ds_material_w		varchar2(255);
ds_via_aplicacao_w	varchar2(60);
ds_generico_w		varchar2(100);
ds_posologia_w		varchar2(2000);


Begin


select	ds_material ||' - '||ds_generico,
	ds_via_aplicacao,
	ds_posologia
into	ds_material_w,
	ds_via_aplicacao_w,
	ds_posologia_w
from	checkup_medic_associado
where	nr_sequencia	= nr_sequencia_p;


if	(ds_material_w is not null) then
	ds_receita_w	:= ds_material_w || chr(13);
end if;


if	(ds_via_aplicacao_w is not null) then
	ds_receita_w	:= ds_receita_w || ds_via_aplicacao_w || chr(13);
end if;

/*
if 	(ds_generico_w is not null) then
	ds_receita_w	:= ds_receita_w || ds_generico_w || chr(13);
end if;
*/

if 	(ds_posologia_w is not null) then
	ds_receita_w	:= ds_receita_w || ds_posologia_w || chr(10);
end if;


ds_retorno_p	:= ds_receita_w;

END Gerar_medic_diag_checkup;
/