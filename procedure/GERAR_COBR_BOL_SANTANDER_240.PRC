create or replace
procedure gerar_cobr_bol_santander_240(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is 
			
ds_conteudo_w			varchar2(240);
nr_remessa_w			varchar2(8);
qt_lote_arquivo_w		varchar2(6) := 0;
qt_reg_lote_w			varchar2(6) := 0;
qt_registro_lote_w		varchar2(6) := 0;
nr_seq_arquivo_w		varchar2(6) := 0;
nr_seq_registro_w		varchar2(255) := 0;
nm_empresa_w			varchar2(30);
cd_cgc_w			varchar2(15);
nm_banco_w			varchar2(30);
dt_geracao_w			varchar2(8);
nr_lote_w_w			varchar2(4) := 0;
nr_lote_w			varchar2(4);

/* Segmentos */
nr_nosso_numero_w		varchar2(13);
ie_documento_w			varchar2(1);
dt_vencimento_w			varchar2(8);
vl_titulo_w			varchar2(15);
ie_titulo_w			varchar2(2);
dt_emissao_w			varchar2(8);
cd_juros_mora_w			varchar2(1);
dt_juros_mora_w			varchar2(8);
vl_juros_diario_w		varchar2(15);
cd_desconto_w			varchar2(1);
dt_desconto_w			varchar2(8);
vl_desconto_dia_w		varchar2(15);
vl_iof_w			varchar2(15);
vl_abatimento_w			varchar2(15);
ds_ident_titulo_emp_w		varchar2(25);
cd_protesto_w			varchar2(1);
qt_dias_protestos_w		varchar2(2);
cd_moeda_w			varchar2(2);
cd_agencia_w			varchar2(4);
ie_digito_agencia_w		varchar2(1);
nr_conta_w			varchar2(9);
ie_digito_conta_w		varchar2(1);
cd_conta_cobr_w			varchar2(9);
ie_conta_cobr_w			varchar2(1);
ie_tipo_cobranca_w		varchar2(1);
ie_forma_cad_w			varchar2(1);
nr_seu_numero_w			varchar2(15);
cd_agencia_cobr_w		varchar2(4);
ie_agencia_cobr_w		varchar2(1);
ie_ident_titulo_w		varchar2(1);
cd_baixa_w			varchar2(1);
nr_dias_baixa_w			varchar2(2);
ie_tipo_inscricao_w		varchar2(1);
nr_inscricao_w			varchar2(15);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
ds_bairro_sacado_w		varchar2(15);
cd_cep_sacado_w			varchar2(8);
ds_municipio_sacado_w		varchar2(15);
ds_estado_sacado_w		varchar2(2);
ds_avalista_w			varchar2(40);
nr_avalista_w			varchar2(16);
cd_mov_remessa_w		varchar2(2);
ie_carne_w			varchar2(3);
nr_parcela_w			varchar2(3);
qt_total_parcela_w		varchar2(3);
nr_plano_w			varchar2(3);
cd_transmissao_w		varchar2(15);
nm_cedente_w			varchar2(30);
nr_seq_mensalidade_w		number(10);
nr_seq_contrato_w		number(10);

/* Ds_Brancos */
ds_brancos_8_w			varchar2(8);
ds_brancos_25_w			varchar2(25);
ds_brancos_10_w			varchar2(10);
ds_brancos_6_w			varchar2(6);
ds_brancos_74_w			varchar2(74);
ds_brancos_2_w			varchar2(2);
ds_brancos_1_w			varchar2(1);
ds_brancos_20_w			varchar2(20);
ds_brancos_5_w			varchar2(5);
ds_brancos_41_w			varchar2(41);
ds_brancos_9_w			varchar2(9);
ds_brancos_217_w		varchar2(217);
ds_brancos_211_w		varchar2(211);
ds_brancos_19_w			varchar2(19);
ds_brancos_11_w			varchar2(11);
ds_branco_15_w			varchar2(15);
ds_brancos_119_w		varchar2(119);
ds_mensagen_1_w			varchar2(40);
ds_mensagen_2_w			varchar2(40);
ds_mensagem_w			varchar2(100);
nr_linha_w			number(10) := 0;

/* Mensagens */
pr_juros_w			varchar2(255);
pr_multa_w			varchar2(255);
dt_mensalidade_w		varchar2(255);
nr_contrato_w			varchar2(255);
ds_plano_w			varchar2(255);
nr_protocolo_ans_w		varchar2(255);
dt_proximo_reajuste_w		varchar2(255);
cd_usuario_plano_w		varchar2(255);
dt_contratacao_w		varchar2(255);
vl_mensalidade_w		varchar2(255);
vl_coparticipacao_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(255);
dt_solicitacao_w		varchar2(255);
dt_resposta_w			varchar2(255);
vl_outros_n_w			number(15,2);
vl_outros_w			varchar2(255)	:= null;
vl_total_w			varchar2(255)	:= null;
ie_adiciona_linhas_w		varchar2(255)	:= 'N';
nr_seq_plano_w			number(15);
nr_seq_segurado_w		number(15);
nr_seq_segurado_mens_w		number(15);
qt_itens_w			number(15)	:= 0;
nr_seq_pagador_w		number(15);
qt_coparticipacao_w		number(15);

/* UTL_FILE */
nm_arquivo_w			varchar2(255);
arq_texto_w			utl_file.file_type;
arq_texto_log_w			utl_file.file_type;
ds_erro_w			varchar2(255);
ds_local_w			varchar2(255);
nr_seq_mensalidade_seg_w	number(10);
vl_item_w			number(15,2);
dt_reajuste_prox_w		date;
dt_ref_mensalidade_w		date;

dt_reajuste_w			date;
dt_remessa_retorno_w		date;
cd_cgc_estip_w			varchar2(14);

nr_dia_w				varchar2(2);
nr_mes_atual_w			varchar2(2);
dt_ref_w				date;

/* Segmento P */
cursor C01 is
	select	lpad(nvl(substr(c.cd_ocorrencia,1,2),'01'),2,'0') cd_mov_remessa, 
		lpad(to_char(x.cd_agencia_bancaria),4,'0') cd_agencia,
		/*rpad(substr(nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),'0'),1,1),1,'0') */
		lpad(nvl(substr(z.ie_digito,1,1),'0'),1,'0') ie_digito_agencia,
		lpad(substr(x.cd_conta,1,9),9,'0') nr_conta,
		lpad(nvl(substr(c.ie_digito_conta,1,1),'0'),1,'0') ie_digito_conta, 
		lpad(substr(x.cd_conta,1,9),9,'0') cd_conta_cobr,
		rpad(nvl(substr(x.ie_digito_conta,1,1),'0'),1,'0') ie_conta_cobr, --rpad(nvl(substr(x.ie_digito_conta,1,1),'0'),1,'0') ie_conta_cobr,
		lpad(nvl(trim(substr(obter_nosso_numero_interf(x.cd_banco,b.nr_titulo),1,12)),'0')||calcula_digito('Modulo11',(lpad(b.nr_titulo,8,'0'))),13,'0') nr_nosso_numero ,
		'1' ie_tipo_cobranca,
		'2' ie_forma_cad,
		'1' ie_documento,
		rpad(substr(b.nr_titulo,1,15),15,'0') nr_seu_numero,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyyyy') dt_vencimento,
		lpad(replace(to_char(nvl(b.vl_titulo,0), 'fm00000000000.00'),'.',''),15,'0') vl_titulo,
		'0000' cd_agencia_cobr,
		'0' ie_agencia_cobr,
		'04' ie_titulo,
		'N' ie_ident_titulo,
		to_char(w.dt_geracao_titulos,'ddmmyyyy') dt_emissao,
		'3' cd_juros_mora,
		'00000000' dt_juros_mora,
		lpad(replace(to_char(nvl(obter_vl_juros_diario_tit(null, b.nr_titulo),0), 'fm00000000000.00'),'.',''),15,'0') vl_juros_diario,
		campo_mascara_virgula_casas(obter_dados_titulo_receber(b.nr_titulo,'PDJ'),3) pr_juros_diario, 
		campo_mascara_virgula(obter_dados_titulo_receber(b.nr_titulo,'TXM')) pr_multa_diario, 
		'0' cd_desconto,
		'00000000' dt_desconto,
		lpad('0',15,'0') vl_desconto_dia,
		lpad('0',15,'0') vl_iof,
		lpad('0',15,'0') vl_abatimento,
		lpad(' ',25,' ') ds_ident_titulo_emp,
		'0' cd_protesto,
		'00' qt_dias_protestos,
		'3' cd_baixa,
		'00' nr_dias_baixa,
		'00' cd_moeda,
/* Segmento Q */	decode(b.cd_cgc, null,'1','2') ie_tipo_inscricao, 
		lpad(nvl(b.cd_cgc_cpf,'0'),15,'0') nr_inscricao,
		rpad(upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))),40,' ') nm_sacado,
		rpad(upper(elimina_acentuacao(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'EN')),1,40))),40,' ') ds_endereco_sacado,
		rpad(upper(elimina_acentuacao(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,15))),15,' ') ds_bairro_sacado,
		lpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8),8,'0') cd_cep_sacado,
		rpad(upper(elimina_acentuacao(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15))),15,' ') ds_municipio_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),2,' ') ds_estado_sacado,
		lpad('0',16,'0') nr_avalista,
		rpad(' ',40,' ') ds_avalista,
		'000' ie_carne,
		'000' nr_parcela,
		'000' qt_total_parcela,
		'000' nr_plano,
		d.nr_sequencia,
/*Segmento S*/	f.nr_seq_contrato,
		pls_obter_segurado_pagador(d.nr_seq_pagador) nr_seq_segurado,
		d.nr_seq_pagador,
		b.cd_pessoa_fisica
	from	agencia_bancaria	z,
		banco_estabelecimento	x,
		pls_lote_mensalidade	w,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	z.cd_agencia_bancaria	= x.cd_agencia_bancaria
	and	z.cd_banco		= x.cd_banco
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	d.nr_seq_lote		= w.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

Cursor C02 is
	select	campo_mascara_virgula(nvl(a.vl_mensalidade - a.vl_coparticipacao - a.vl_outros,0)) vl_mensalidade,
		campo_mascara_virgula(nvl(a.vl_coparticipacao,0)) vl_coparticipacao,
		a.nr_seq_segurado
	from	pls_mensalidade_segurado 	a
	where	a.nr_seq_mensalidade 	= nr_seq_mensalidade_w
	and	rownum 			<= 9
	order by a.nr_sequencia;
	
begin
nm_arquivo_w	:= to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '.rem';

obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);
begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W'); --arq_texto_w := utl_file.fopen('/srvfs03/FINANCEIRO/TASY/',nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
	--R aise_application_error(-20011,ds_erro_w);
end;

update	cobranca_escritural
set	ds_arquivo	= ds_local_w || nm_arquivo_w
where	nr_sequencia	= nr_seq_cobr_escrit_p;

select	rpad(' ',8,' '),
	rpad(' ',25,' '),
	rpad(' ',10,' '),
	rpad(' ',6,' '),
	rpad(' ',74,' '),
	rpad(' ',2,' '),
	rpad(' ',1,' '),
	rpad(' ',20,' '),
	rpad(' ',5,' '),
	rpad(' ',41,' '),
	lpad(' ',9,' '),
	rpad(' ',217,' '),
	rpad(' ',211,' '),
	rpad(' ',19,' '),
	rpad(' ',11,' '),
	rpad(' ',40,' '),
	rpad(' ',40,' '),
	lpad('0',15,'0'),
	rpad(' ',119,' ')
into	ds_brancos_8_w,
	ds_brancos_25_w,
	ds_brancos_10_w,
	ds_brancos_6_w,
	ds_brancos_74_w,
	ds_brancos_2_w,
	ds_brancos_1_w,
	ds_brancos_20_w,
	ds_brancos_5_w,
	ds_brancos_41_w,
	ds_brancos_9_w,
	ds_brancos_217_w,
	ds_brancos_211_w,
	ds_brancos_19_w,
	ds_brancos_11_w,
	ds_mensagen_1_w,
	ds_mensagen_2_w,
	ds_branco_15_w,
	ds_brancos_119_w
from	dual;

/* Header Arquivo*/
select	rpad(elimina_acentuacao(substr(obter_nome_pf_pj(null,b.cd_cgc),1,30)),30,' ') nm_empresa,
	rpad(upper(substr(obter_nome_banco(c.cd_banco),1,30)),30,' ') nm_banco,
	lpad(b.cd_cgc,15,'0') cd_cgc,
	lpad(to_char(a.nr_sequencia),6,'0') nr_seq_arquivo,
	to_char(sysdate,'ddmmyyyy') dt_geracao,
	lpad(substr(nvl(c.cd_transmissao,'0'),1,15),15,'0') cd_transmissao
into	nm_empresa_w,
	nm_banco_w,
	cd_cgc_w,
	nr_seq_arquivo_w,
	dt_geracao_w,
	cd_transmissao_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	'033' || '0000' || '0' || ds_brancos_8_w || '2' || cd_cgc_w || cd_transmissao_w || ds_brancos_25_w || nm_empresa_w || 
			nm_banco_w || ds_brancos_10_w || '1' || dt_geracao_w || ds_brancos_6_w || nr_seq_arquivo_w || '040' || ds_brancos_74_w;		
			
utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
utl_file.fflush(arq_texto_w);

qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
/* Fim Header Arquivo */

/* Header Lote */
nr_lote_w_w	:= nr_lote_w_w + 1;
nr_lote_w	:= lpad(nr_lote_w_w,4,'0');

select	lpad(b.cd_cgc,15,'0') cd_cgc,
	rpad(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30)),30,' ') nm_cedente,
	lpad(substr(nvl(a.nr_remessa,a.nr_sequencia),1,8),8,'0') nr_remessa,
	to_char(sysdate,'ddmmyyyy') dt_geracao,
	lpad(substr(nvl(c.cd_transmissao,'0'),1,15),15,'0') cd_transmissao,
	dt_remessa_retorno
into	cd_cgc_w,
	nm_cedente_w,
	nr_remessa_w,
	dt_geracao_w,
	cd_transmissao_w,
	dt_remessa_retorno_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	'033' || nr_lote_w || '1' || 'R' || '01' || ds_brancos_2_w || '030' || ds_brancos_1_w || '2' || cd_cgc_w ||
			ds_brancos_20_w || cd_transmissao_w || ds_brancos_5_w || nm_cedente_w || ds_mensagen_1_w || ds_mensagen_2_w || nr_remessa_w || 
			dt_geracao_w || ds_brancos_41_w;
			
utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
utl_file.fflush(arq_texto_w);
	
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
qt_reg_lote_w		:= qt_reg_lote_w + 1;
/* Fim Header Lote */

/* Inicio Segmentos */
open C01;
loop
fetch C01 into	
	cd_mov_remessa_w,
	cd_agencia_w,
	ie_digito_agencia_w,
	nr_conta_w,
	ie_digito_conta_w,
	cd_conta_cobr_w,
	ie_conta_cobr_w,
	nr_nosso_numero_w,
	ie_tipo_cobranca_w,
	ie_forma_cad_w,
	ie_documento_w,
	nr_seu_numero_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_agencia_cobr_w,
	ie_agencia_cobr_w,
	ie_titulo_w,
	ie_ident_titulo_w,
	dt_emissao_w,
	cd_juros_mora_w,
	dt_juros_mora_w,
	vl_juros_diario_w,
	pr_juros_w,
	pr_multa_w,
	cd_desconto_w,
	dt_desconto_w,
	vl_desconto_dia_w,
	vl_iof_w,
	vl_abatimento_w,
	ds_ident_titulo_emp_w,
	cd_protesto_w,
	qt_dias_protestos_w,
	cd_baixa_w,
	nr_dias_baixa_w,
	cd_moeda_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	ds_bairro_sacado_w,
	cd_cep_sacado_w,
	ds_municipio_sacado_w,
	ds_estado_sacado_w,
	nr_avalista_w,
	ds_avalista_w,
	ie_carne_w,
	nr_parcela_w,
	qt_total_parcela_w,
	nr_plano_w,
	nr_seq_mensalidade_w,
	nr_seq_contrato_w,
	nr_seq_segurado_w,
	nr_seq_pagador_w,
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	/* Segmento P*/
	nr_seq_registro_w:= nr_seq_registro_w + 1;

	ds_conteudo_w	:=	'033' || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'P' || ds_brancos_1_w || cd_mov_remessa_w || 
				cd_agencia_w || ie_digito_agencia_w || nr_conta_w || ie_conta_cobr_w /*ie_digito_conta_w*/ || cd_conta_cobr_w || 
				ie_conta_cobr_w || ds_brancos_2_w || nr_nosso_numero_w || ie_tipo_cobranca_w || ie_forma_cad_w || ie_documento_w ||
				ds_brancos_2_w || nr_seu_numero_w || dt_vencimento_w || vl_titulo_w || cd_agencia_cobr_w || ie_agencia_cobr_w || 
				ds_brancos_1_w || ie_titulo_w || ie_ident_titulo_w || dt_emissao_w || cd_juros_mora_w || dt_juros_mora_w || 
				vl_juros_diario_w || cd_desconto_w || dt_desconto_w || vl_desconto_dia_w || vl_iof_w || vl_abatimento_w ||
				ds_ident_titulo_emp_w || cd_protesto_w || qt_dias_protestos_w || cd_baixa_w || '0' || nr_dias_baixa_w || 
				cd_moeda_w || ds_brancos_11_w; 
	
	utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
	utl_file.fflush(arq_texto_w);
	/* Fim segmento P*/
	
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;

	/* Segmento Q*/
	ds_conteudo_w	 :=	null;
	nr_seq_registro_w:= 	nr_seq_registro_w + 1;
	
	ds_conteudo_w	:=	'033' || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'Q' || ds_brancos_1_w || cd_mov_remessa_w || 
				ie_tipo_inscricao_w || nr_inscricao_w || nm_sacado_w || ds_endereco_sacado_w || ds_bairro_sacado_w ||
				cd_cep_sacado_w || ds_municipio_sacado_w || ds_estado_sacado_w || nr_avalista_w || ds_avalista_w || ie_carne_w || 
				nr_parcela_w || qt_total_parcela_w || nr_plano_w || ds_brancos_19_w; 
				
	utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
	utl_file.fflush(arq_texto_w);
		
	/* Fim segmento Q*/
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;
	
	
	/* Segmento R */
	ds_conteudo_w	 :=	null;
	nr_seq_registro_w:= 	nr_seq_registro_w + 1;
	
	if	(nr_seq_mensalidade_w is not null) then
		select	trim(to_char(dt_referencia,'MONTH'))||'/'||to_char(dt_referencia,'yyyy')
		into	dt_mensalidade_w
		from	pls_mensalidade
		where	nr_sequencia = nr_seq_mensalidade_w;
	end if;
	
	ds_conteudo_w	:=	'033' || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'R' || ds_brancos_1_w || cd_mov_remessa_w || 
				'0' || '        ' || '               ' || '                        ' || ' ' || '        ' || '               ' ||
				'          ' ||
				rpad('RECEBIMENTO ATE 59 DIAS PARA PAGAMENTO VIA BANCO  APOS VENC. MULTA DE ' || pr_multa_w || '% E JUROS ' || pr_juros_w 
				|| '% AO DIA. MES REF. ' || dt_mensalidade_w, 141, ' ');
				
	utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
	utl_file.fflush(arq_texto_w);
		
	/* Fim segmento R */
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;
	end;	
	
	/*Mensagem*/
	nr_contrato_w		:= null;
	ds_plano_w		:= null;
	nr_protocolo_ans_w	:= null;
	dt_mensalidade_w	:= null;
	dt_proximo_reajuste_w	:= null;
	qt_itens_w		:= 0;
	nr_linha_w		:= 1;
	
	if	(nr_seq_mensalidade_w is not null) then
		select	trim(to_char(dt_referencia,'MONTH'))||'/'||to_char(dt_referencia,'yyyy'),
			dt_referencia
		into	dt_mensalidade_w,
			dt_ref_mensalidade_w
		from	pls_mensalidade
		where	nr_sequencia = nr_seq_mensalidade_w;
	end if;
	
	if	(nr_seq_contrato_w is not null) then

		select 	to_char(nvl(dt_reajuste,dt_contrato),'dd'),
				'01/' || to_char(nvl(dt_reajuste,dt_contrato),'mm') || '/' || pkg_date_utils.extract_field('YEAR', dt_ref_mensalidade_w, 0)
		into 	nr_dia_w,
				dt_ref_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;

		if	(nr_dia_w > 28) then
			
			nr_mes_atual_w := to_char(dt_ref_w, 'mm');
		
			if (nr_mes_atual_w in ('04','06','09','11')) and (nr_dia_w > 30) then
				
				nr_dia_w := 30;
		
			elsif (nr_mes_atual_w = '02') and (nr_dia_w > 28) then
			
				if	(obter_se_ano_bisexto(dt_ref_w) = 'S') then
					nr_dia_w := 29;
				else
					nr_dia_w := 28;
					
				end if;
				
		     end if;
			 
			begin	
			
			dt_reajuste_w := to_date(nr_dia_w || '/' || to_char(dt_ref_w, 'mm/yyyy'));

			exception when others then
			nr_dia_w := 28; /*Se der qq excess�o, pega 28, pois esse dia tem em todos os meses.*/
			dt_reajuste_w := to_date(nr_dia_w || '/' || to_char(dt_ref_w, 'mm/yyyy'));
			end;

		end if;	

		select	nr_contrato,
			trim(to_char(nvl(dt_reajuste,dt_contrato),'MONTH'))||'/'||
			to_char(pls_obter_proximo_reajuste(nr_sequencia),'yyyy'),
			pls_obter_proximo_reajuste(nr_sequencia),
		/*to_date(to_char(nvl(dt_reajuste,dt_contrato),'dd/mm') || '/' || to_char(dt_ref_mensalidade_w, 'yyyy')),*/
			cd_cgc_estipulante
		into	nr_contrato_w,
			dt_proximo_reajuste_w,
			dt_reajuste_prox_w,
			/*dt_reajuste_w,*/
			cd_cgc_estip_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
		
		if	(to_char(dt_reajuste_prox_w,'mm/yyyy') = to_char(dt_ref_mensalidade_w,'mm/yyyy')) then
			select	trim(to_char(nvl(dt_reajuste,dt_contrato),'MONTH'))||'/'||
				to_char(add_months(pls_obter_proximo_reajuste(nr_sequencia),12),'yyyy')
			into	dt_proximo_reajuste_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
		end if;
	end if;
	
	if	(nr_seq_segurado_w is null) then
		select	max(a.nr_sequencia)
		into	nr_seq_segurado_w
		from	pls_segurado a
		where	a.nr_seq_pagador 	= nr_seq_pagador_w;
	end if;
	
	if	(nr_seq_segurado_w is not null) then
		select	max(a.nr_seq_plano)
		into	nr_seq_plano_w
		from	pls_segurado_carteira	b,
			pls_segurado		a
		where	a.nr_sequencia	= b.nr_seq_segurado
		and	a.nr_sequencia 	= nr_seq_segurado_w;
	
		if	(nr_seq_mensalidade_w is not null) then
			select  sum(nvl(a.vl_outros,0)) vl_outros,
				campo_mascara_virgula(nvl(sum(a.vl_mensalidade),0)) vl_total_mens,
				max(a.nr_sequencia)
			into	vl_outros_n_w,
				vl_total_w,
				nr_seq_mensalidade_seg_w
			from  	pls_mensalidade_segurado 	a
			where 	a.nr_seq_mensalidade   		= nr_seq_mensalidade_w
			and  	rownum 			<= 9
			order by a.nr_sequencia;
			
			select	sum(nvl(a.vl_item,0))
			into	vl_item_w
			from	pls_mensalidade_seg_item a,
				pls_mensalidade_segurado b
			where	a.nr_seq_mensalidade_seg	= b.nr_sequencia
			and	b.nr_seq_mensalidade   		= nr_seq_mensalidade_w
			and	a.ie_tipo_item			= 4
			and  	rownum 				<= 9
			order by a.nr_sequencia;

			if	(nvl(vl_item_w,0) <= nvl(vl_outros_n_w,0)) then
				vl_outros_w := campo_mascara_virgula(nvl(vl_outros_n_w,0) - nvl(vl_item_w,0));
			end if;
		end if;
	end if;
	
	if	(nr_seq_plano_w is not null) then
		select	ds_plano,
			nr_protocolo_ans
		into	ds_plano_w,
			nr_protocolo_ans_w
		from	pls_plano
		where	nr_sequencia		= nr_seq_plano_w;
	end if;	
	
	while (nr_linha_w <= 22) loop
		ie_adiciona_linhas_w	:= 	'N';
		nr_seq_registro_w	:=	nr_seq_registro_w + 1;
		ds_conteudo_w		:=	'033' || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'S' || ds_brancos_1_w || 
						cd_mov_remessa_w || '1';
		
		/* Cada "i" � uma linha do arquivo (Mensagem), assim vai gravando linha por linha  */
		if	(nr_linha_w = 1) then
			if	(cd_cgc_estip_w is not null) then
				select	substr(ds_observacao,1,100)
				into	ds_mensagem_w
				from	pls_reajuste
				where	nr_contrato	= nr_contrato_w
				and	dt_reajuste_aux	= to_char(dt_reajuste_w, 'mm/yyyy');
			else
				ds_mensagem_w	:=	substr(pls_obter_mensagem_reajuste(nr_seq_mensalidade_w,1,'RA'),1,100);
			end if;
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 2) then
			if	(cd_cgc_estip_w is not null) then
				select	substr(ds_observacao,101,100)
				into	ds_mensagem_w
				from	pls_reajuste
				where	nr_contrato	= nr_contrato_w
				and	dt_reajuste_aux	= to_char(dt_reajuste_w, 'mm/yyyy');
			else
				ds_mensagem_w	:=	substr(pls_obter_mensagem_reajuste(nr_seq_mensalidade_w,2,'RA'),1,100);
			end if;
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 3) then
			ds_mensagem_w	:=	substr(pls_obter_mensagem_reajuste(nr_seq_mensalidade_w,3,'RA'),1,100);
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 4) then
			ds_mensagem_w	:=	'';
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 5) then
			ds_mensagem_w	:= 	'APOS VENC. MULTA DE '||pr_multa_w||'% E JUROS '||pr_juros_w||'% AO DIA. MES REF. '||dt_mensalidade_w;			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 6) then
			if	(nr_contrato_w is not null) then
				ds_mensagem_w	:= 'Contrato :     '||lpad(nr_contrato_w,15,'0');
			else	
				ds_mensagem_w	:= lpad(' ',30,' ');
			end if;
			
			if	(nr_protocolo_ans_w is not null) then
				ds_mensagem_w	:= ds_mensagem_w || '         Reg. Produto ANS '|| nr_protocolo_ans_w;
			end if;
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 7) then
			select	to_char(max(b.dt_solicitacao),'dd/mm/yyyy'),
				to_char(nvl(max(b.dt_resposta),sysdate),'dd/mm/yyyy')
			into	dt_solicitacao_w,
				dt_resposta_w
			from	pls_portab_pessoa	b,
				pls_segurado		a
			where	b.nr_sequencia = a.nr_seq_portabilidade
			and	a.nr_sequencia = nr_seq_segurado_w;
			
			--if	(dt_solicitacao_w is not null) then
			
			if	(pls_obter_meses_entre_datas(dt_ref_mensalidade_w, dt_reajuste_w) = 1) then
				ds_mensagem_w	:= 	'Portabilidade de Carencias: Periodo de ' || to_char(trunc(dt_reajuste_w,'month'),'dd/mm/yyyy') || ' a ' ||
								to_char(fim_mes(add_months(dt_reajuste_w,3)),'dd/mm/yyyy');
			else
				ds_mensagem_w	:= '';
			end if;	
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 8) then
			if	(dt_proximo_reajuste_w is not null) then
				ds_mensagem_w	:= 'Proximo reajuste: '||dt_proximo_reajuste_w;
			else	
				ds_mensagem_w	:= '';
			end if;
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 9) then
			if	(ds_plano_w is not null) then
				ds_mensagem_w	:= 'Plano: ' || ds_plano_w;
			else
				ds_mensagem_w 	:= '';
			end if;
		
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = 10) then
			ds_mensagem_w	:=	'>>Resumo da Fatura. Detalhes Co-Part.: www.ativia.com.br<<';			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;						
		elsif	(nr_linha_w = 11) then
			ds_mensagem_w	:= 	'Numero Cartao Id.    Inicio     Reg. Prod.    Mensal.      Co-Part.      Qt.CP  ';
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;		
		elsif	(nr_linha_w = 12) then
			open C02;
			loop
			fetch C02 into	
				vl_mensalidade_w,
				vl_coparticipacao_w,
				nr_seq_segurado_mens_w;
			exit when C02%notfound;
				begin	
				select	b.cd_usuario_plano,
					to_char(a.dt_contratacao,'dd/mm/yyyy')
				into	cd_usuario_plano_w,
					dt_contratacao_w
				from	pls_segurado_carteira	b,
					pls_segurado		a
				where	a.nr_sequencia	= b.nr_seq_segurado
				and	a.nr_sequencia 	= nr_seq_segurado_mens_w;
				
				select	count(1)
				into	qt_coparticipacao_w
				from	pls_mensalidade_seg_item b,
					pls_mensalidade_segurado a
				where	b.nr_seq_mensalidade_seg = a.nr_sequencia
				and	a.nr_seq_mensalidade	= nr_seq_mensalidade_w
				and	a.nr_seq_segurado	= nr_seq_segurado_mens_w
				and	b.ie_tipo_item 		= 3;
				
				ds_conteudo_w	:=	'033' || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'S' || ds_brancos_1_w || 
							cd_mov_remessa_w || '1';
				ds_mensagem_w	:= 	rpad(cd_usuario_plano_w,21,' ') || rpad(dt_contratacao_w,11,' ') || rpad(nr_protocolo_ans_w,14,' ') ||
							rpad(vl_mensalidade_w,14,' ') 	|| rpad(vl_coparticipacao_w,14,' ')|| rpad(qt_coparticipacao_w,11,' ');
							
				ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
							rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
							
				qt_itens_w		:= qt_itens_w + 1;
				nr_linha_w 		:= nr_linha_w + 1;				
				
								
				utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
				utl_file.fflush(arq_texto_w);	
				
				qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
				qt_registro_lote_w	:= qt_registro_lote_w + 1;
				ie_adiciona_linhas_w	:= 'S';			
				end;
			end loop;
			close C02;			
			if	(ie_adiciona_linhas_w = 'N') then
				ds_mensagem_w	:= '';
				ds_conteudo_w	:=	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
							rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
			end if;
		elsif	(nr_linha_w = (13 + qt_itens_w)) then			
			ds_mensagem_w	:= 	rpad('Outros eventos:',37,' ') || nvl(vl_outros_w,'0,00');
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w = (14 + qt_itens_w)) then
			if	(vl_total_w is not null) then
				ds_mensagem_w	:=	rpad('Totalizacao:',37,' ') || vl_total_w;
			else
				ds_mensagem_w	:= '';
			end if;
			
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		elsif	(nr_linha_w <= 22) then
			ds_mensagem_w	:= '';
			ds_conteudo_w	:= 	ds_conteudo_w || lpad(nr_linha_w,2,'0') || '2' || 
						rpad(nvl(ds_mensagem_w,' '),100,' ') || ds_brancos_119_w;
		end if;		
		if	(ie_adiciona_linhas_w = 'N') then
			nr_linha_w := nr_linha_w + 1;
			
			utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
			utl_file.fflush(arq_texto_w);				
			
			qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
			qt_registro_lote_w	:= qt_registro_lote_w + 1;
		end if;									
		end loop;
	/*Fim - Mensagem*/
				
end loop;
close C01;
/*Fim Segmentos */

/* Trailler Lote*/
begin
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
	
ds_conteudo_w	:= '033' || nr_lote_w || '5' || ds_brancos_9_w || lpad(qt_registro_lote_w,6,'0') || ds_brancos_217_w; 

utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
utl_file.fflush(arq_texto_w);
end;
/* Fim Trailler Lote*/


/* Trailler Arquivo*/
begin	
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;

ds_conteudo_w	:= '033' || '9999' || '9' || ds_brancos_9_w || lpad(qt_reg_lote_w,6,'0') || lpad(qt_lote_arquivo_w,6,'0') || ds_brancos_211_w;

utl_file.put_line(arq_texto_w,ds_conteudo_w|| chr(13));
utl_file.fflush(arq_texto_w);
end;
/* Fim Trailler Arquivo*/

commit;

end gerar_cobr_bol_santander_240;
/