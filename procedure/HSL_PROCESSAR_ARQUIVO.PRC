create or replace
procedure	HSL_Processar_Arquivo(	nr_atendimento_p	number,
					nr_prontuario_p		number) is
nr_sequencia_w	number(10);

Cursor	C01 is
	select	nr_sequencia
	from	hsl_lantis
	where	ie_status is null
	or	ie_status = 'I'
	order by	nr_sequencia;

Cursor	C02 is
	select	nr_sequencia
	from	hsl_lantis
	where	nr_atendimento = nr_atendimento_p
	and	nr_prontuario = nr_prontuario_p
	order by	nr_sequencia;

begin

if	(nr_atendimento_p	<> 0) and
	(nr_prontuario_p	<> 0) then
	update	hsl_lantis
	set	nr_atendimento	= nr_atendimento_p,
		ie_status	= 'I'
	where	nr_prontuario	= nr_prontuario_p;
	
	commit;	

	open C02;
	loop
		fetch C02 into 
			nr_sequencia_w;
		exit when C02%notfound;
			HSL_Consistir_Arquivo(nr_sequencia_w);
	end loop;
	close c02;
else

	open C01;
	loop
		fetch C01 into 
			nr_sequencia_w;
		exit when C01%notfound;
			HSL_Consistir_Arquivo(nr_sequencia_w);
	end loop;
	close c01;
end	if;

end	HSL_Processar_Arquivo;
/