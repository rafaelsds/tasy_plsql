create or replace PROCEDURE TRA_GERAR_EXP_CADASTRO is

    ds_sep_bv_w				varchar2(50);

    ds_comando_select_w		varchar2(4000);

    cur_id					number;

    nr_sequencia_w 			number(10);
    nm_usuario_w			varchar2(50);
    vl_flag_tipo_w          varchar2(3);
    vl_campo_ds_w			varchar2(4000);
    vl_campo_exp_w			varchar2(4000);

    retorno_w				number(10);

    cd_exp_retorno_w		dic_expressao.cd_expressao%type;

    ds_comando_update_w		varchar2(4000);

    ds_erro_w				varchar2(4000);

    cursor c01 is
        select	nr_sequencia,
                nm_tabela,
                nm_atributo,
                nm_atributo_exp,
                to_char(nvl(dt_ultima_atualizacao, sysdate), 'dd/mm/yyyy HH24:mi') dt_ultima_atualizacao
        from	corp_tabelas_traducao
        where	ie_situacao = 'A'
        and     	ie_sincronizar = 'S';

    dt_atualizacao_w        date;
    
    tabela_possui_col_situacao_w varchar2(1);

BEGIN
    ds_sep_bv_w	:= obter_separador_bv;

    dt_atualizacao_w := sysdate;

    FOR rs_w IN c01 LOOP
        BEGIN

            select  nvl(max('S'), 'N')
            into    tabela_possui_col_situacao_w
            from    user_tab_cols
            where   upper(table_name) = rs_w.nm_tabela
            and     upper(column_name) = 'IE_SITUACAO';

            if (tabela_possui_col_situacao_w = 'S') then
            
                ds_comando_select_w := 'select nr_sequencia, nm_usuario, ' || CHR(39) || 'UPD' || CHR(39) || ',' ||
                                        rs_w.nm_atributo 		|| ', ' ||
                                        rs_w.nm_atributo_exp 	||
                                    ' from 	' || rs_w.nm_tabela ||
                                    ' where	dt_atualizacao > to_date(' ||chr(39) ||rs_w.dt_ultima_atualizacao || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy HH24:mi' || chr(39) || ')' ||
                                    ' and ' || rs_w.nm_atributo_exp || ' is not null ' ||
                                    ' and ' || rs_w.nm_atributo || ' is not null ' ||
                                    ' and nvl(ie_situacao, ' || CHR(39) || 'A' || CHR(39) || ') = ' || CHR(39) || 'A' || CHR(39) ||
    
                                    ' union ' ||
    
                                    'select	nr_sequencia, nm_usuario, ' || CHR(39) || 'NEW' || CHR(39) || ',' ||
                                        rs_w.nm_atributo 		|| ', ' ||
                                        rs_w.nm_atributo_exp 	||
                                    ' from	' || rs_w.nm_tabela ||
                                    ' where ' || rs_w.nm_atributo_exp || ' is null ' ||
                                    ' and ' || rs_w.nm_atributo || ' is not null ' ||
                                    ' and nvl(ie_situacao, ' || CHR(39) || 'A' || CHR(39) || ') = ' || CHR(39) || 'A' || CHR(39);
            else

                ds_comando_select_w := 'select nr_sequencia, nm_usuario, ' || CHR(39) || 'UPD' || CHR(39) || ',' ||
                                        rs_w.nm_atributo 		|| ', ' ||
                                        rs_w.nm_atributo_exp 	||
                                    ' from 	' || rs_w.nm_tabela ||
                                    ' where	dt_atualizacao > to_date(' ||chr(39) ||rs_w.dt_ultima_atualizacao || chr(39) || ', ' || chr(39) || 'dd/mm/yyyy HH24:mi' || chr(39) || ')' ||
                                    ' and ' || rs_w.nm_atributo_exp || ' is not null ' ||
                                    ' and ' || rs_w.nm_atributo || ' is not null ' ||
    
                                    ' union ' ||
    
                                    'select	nr_sequencia, nm_usuario, ' || CHR(39) || 'NEW' || CHR(39) || ',' ||
                                        rs_w.nm_atributo 		|| ', ' ||
                                        rs_w.nm_atributo_exp 	||
                                    ' from	' || rs_w.nm_tabela ||
                                    ' where ' || rs_w.nm_atributo_exp || ' is null ' ||
                                    ' and ' || rs_w.nm_atributo || ' is not null ';
            end if;

            cur_id := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(cur_id, ds_comando_select_w, DBMS_SQL.NATIVE);
            DBMS_SQL.DEFINE_COLUMN(cur_id, 1, nr_sequencia_w);
            DBMS_SQL.DEFINE_COLUMN(cur_id, 2, nm_usuario_w, 50);
            DBMS_SQL.DEFINE_COLUMN(cur_id, 3, vl_flag_tipo_w, 3);
            DBMS_SQL.DEFINE_COLUMN(cur_id, 4, vl_campo_ds_w, 4000);
            DBMS_SQL.DEFINE_COLUMN(cur_id, 5, vl_campo_exp_w, 4000);

            retorno_w := DBMS_SQL.execute(cur_id);

            WHILE DBMS_SQL.FETCH_ROWS(cur_id) > 0 LOOP

                DBMS_SQL.COLUMN_VALUE(cur_id, 1, nr_sequencia_w);
                DBMS_SQL.COLUMN_VALUE(cur_id, 2, nm_usuario_w);
                DBMS_SQL.COLUMN_VALUE(cur_id, 3, vl_flag_tipo_w);
                DBMS_SQL.COLUMN_VALUE(cur_id, 4, vl_campo_ds_w);
                DBMS_SQL.COLUMN_VALUE(cur_id, 5, vl_campo_exp_w);

                if (vl_flag_tipo_w = 'NEW') then
                    man_traduzir_cad_corp(nm_usuario_w, substr(vl_campo_ds_w,1,4000), rs_w.nm_tabela, rs_w.nm_atributo, NULL, cd_exp_retorno_w);
                else

                    select 	max(a.cd_expressao)
                    into 	cd_exp_retorno_w
                    from 	dic_expressao_idioma a
                    where 	a.cd_expressao = vl_campo_exp_w
                    and 	a.ds_expressao = vl_campo_ds_w
                    and 	a.ds_locale in ('pt_BR', 'es_MX', 'en_US');

                    if (cd_exp_retorno_w is null) then
                        man_traduzir_cad_corp(nm_usuario_w, substr(vl_campo_ds_w,1,4000), rs_w.nm_tabela, rs_w.nm_atributo, NULL, cd_exp_retorno_w);
                    end if;

                end if;

                if	( nvl(cd_exp_retorno_w, 0) > 0 ) then
                    ds_comando_update_w :=  ' update ' || rs_w.nm_tabela || ' ' ||
                                            ' set ' || rs_w.nm_atributo_exp || ' = :cd_exp_p ' ||
                                            ' where nr_sequencia = :nr_sequencia_p ';

                    Exec_Sql_Dinamico_bv(
                        'TRA_GERAR_EXP_CADASTRO',
                        ds_comando_update_w,
                        'cd_exp_p='         || cd_exp_retorno_w || ds_sep_bv_w ||
                        'nr_sequencia_p='   || nr_sequencia_w
                    );
                end if;

            END LOOP;

            DBMS_SQL.CLOSE_CURSOR(cur_id);

            insert into LOG_CORP_TABELAS_TRADUCAO (nm_tabela, ds_log, dt_execucao) values (rs_w.nm_tabela, 'Grava��o da data de execu��o da job.', dt_atualizacao_w);

        EXCEPTION
        WHEN OTHERS THEN
           BEGIN
            ds_erro_w := SUBSTR(SQLERRM,1,4000);
            insert into LOG_CORP_TABELAS_TRADUCAO (nm_tabela, ds_log, dt_execucao) values (rs_w.nm_tabela, ds_erro_w, null);
            END;
        END;

        update corp_tabelas_traducao
        set dt_ultima_atualizacao = dt_atualizacao_w
        where nr_sequencia = rs_w.nr_sequencia;

        commit;

    END LOOP;
END TRA_GERAR_EXP_CADASTRO;
/