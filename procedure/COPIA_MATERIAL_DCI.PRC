create or replace
procedure copia_material_dci(		cd_estabelecimento_p	number,
				cd_material_p      		number,
				cd_material_novo_p	number,
				nm_usuario_p		varchar2) is

/* Campos da tabela LOOP */
nr_seq_dci_w			number(10);

/*  Sequencia da tabela */
nr_sequencia_w			number(10);

Cursor C01 is
	select	nr_seq_dci
	from	material_dci
	where	cd_material = cd_material_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_dci_w;
exit when C01%notfound;
	begin

	select	material_dci_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into material_dci (	nr_sequencia,
				cd_material,
				dt_atualizacao,
				nm_usuario,
				nr_seq_dci
				)
	values		(	nr_sequencia_w,
				cd_material_novo_p,
				sysdate,
				nm_usuario_p,
				nr_seq_dci_w
				);

	end;
end loop;
close C01;

commit;

end copia_material_dci;
/