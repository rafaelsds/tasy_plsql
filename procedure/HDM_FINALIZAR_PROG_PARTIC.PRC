create or replace
procedure HDM_FINALIZAR_PROG_PARTIC(nm_usuario_p varchar2) is

nr_seq_participante_w	mprev_participante.nr_sequencia%type;
ie_contem_w		varchar2(1) := 'N';


cursor c01 is
	select distinct p.nr_sequencia
	from   mprev_participante p,
	       mprev_programa_partic t
	where  t.nr_seq_participante = p.nr_sequencia
	and    t.dt_exclusao is null;

begin

open c01;
loop
fetch c01 into
	nr_seq_participante_w;
exit when c01%notfound;
	begin

		select	decode(nvl(max(a.nr_sequencia),0),0, 'N','S')
		into	ie_contem_w
		from	mprev_prog_partic_modulo a,
			mprev_programa_partic b
		where	a.nr_seq_programa_partic = b.nr_sequencia
		and     b.nr_seq_participante = nr_seq_participante_w
		and	(dt_saida is null or dt_saida > sysdate);

		if(ie_contem_w = 'N')then
			select  decode(nvl(max(nr_sequencia),0),0,'N','S')
			into	ie_contem_w
			from    mprev_campanha_partic
			where   nr_seq_participante = nr_seq_participante_w
			and     (dt_exclusao is null or dt_exclusao > sysdate);

			if(ie_contem_w = 'N')then

				select  decode(nvl(max(nr_sequencia),0),0,'N','S')
				into	ie_contem_w
				from    mprev_grupo_turma_partic
				where   nr_seq_participante = nr_seq_participante_w
				and	(dt_saida is null or dt_saida > sysdate);

				if(ie_contem_w = 'N')then

					select  decode(nvl(max(nr_sequencia),0),0,'N','S')
					into	ie_contem_w
					from    mprev_agendamento
					where   nr_seq_participante = nr_seq_participante_w
					and	ie_status_agenda in('A','CN');

					if(ie_contem_w = 'N')then

						update	mprev_programa_partic
						set	dt_exclusao    	= sysdate,
							dt_atualizacao 	= sysdate,
							nm_usuario	= nm_usuario_p
						where	nr_seq_participante = nr_seq_participante_w;

					end if;
				end if;
			end if;
		end if;
	end;
end loop;
close c01;

commit;

end HDM_FINALIZAR_PROG_PARTIC;
/
