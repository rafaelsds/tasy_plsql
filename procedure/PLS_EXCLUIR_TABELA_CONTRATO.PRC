create or replace
procedure pls_excluir_tabela_contrato
			(	nr_seq_tabela_p		Number,
				nm_usuario_p		Varchar2) is 

nr_seq_tabela_origem_w		number(10);

begin

select	nvl(max(nr_seq_tabela_origem),0)
into	nr_seq_tabela_origem_w
from	pls_tabela_preco
where	nr_sequencia	= nr_seq_tabela_p;

if	(nr_seq_tabela_origem_w > 0) then
	delete	from pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p;
	
	delete	from pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_p;
end if;

commit;

end pls_excluir_tabela_contrato;
/