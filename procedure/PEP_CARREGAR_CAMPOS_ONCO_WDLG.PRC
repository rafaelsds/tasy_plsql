create or replace
procedure pep_carregar_campos_onco_wdlg(	nr_seq_paciente_p		number,
											cd_convenio_pac_p		number,
											nm_usuario_p			varchar2,
											cd_estabelecimento_p	number,
											nr_prim_dia_p		out	number,
											nr_ciclo_inicial_p	out	number,
											nr_seq_protocolo_p	out	number,
											qt_horas_conv_p		out	number,
											ie_apres_dt_prev_p	out	varchar2,
											dt_hora_ciclo_p		out date) is

cd_convenio_w		convenio.cd_convenio%type := 0;
cd_conv_retorno_w	convenio.cd_convenio%type := 0;
dt_hora_ciclo_aux_w	date;
											
begin
nr_seq_protocolo_p	:= 1;
nr_prim_dia_p		:= 1;
nr_ciclo_inicial_p	:= 1;
qt_horas_conv_p		:= 0;
ie_apres_dt_prev_p	:= 'N';
dt_hora_ciclo_p		:= null;

if 	(nr_seq_paciente_p is not null) then
	nr_prim_dia_p	:= nvl(obter_prim_dia_trat_onc(nr_seq_paciente_p),1);

	select	nvl(max(nr_ciclo), 0) + 1,
			nvl(max(nr_seq_protocolo),1)
	into	nr_ciclo_inicial_p,
			nr_seq_protocolo_p
	from	paciente_atendimento 
	where	nr_seq_paciente = nr_seq_paciente_p
	and		dt_suspensao is null
	and		dt_cancelamento is null;
	
	select	nvl(max('S'),'N')
	into	ie_apres_dt_prev_p
	from	paciente_atendimento
	where	rownum = 1
	and		nr_seq_paciente = nr_seq_paciente_p;
	
	obter_horas_conv_onc(cd_convenio_pac_p,nr_seq_paciente_p, cd_estabelecimento_p, qt_horas_conv_p, cd_conv_retorno_w);
			
	if	(nvl(qt_horas_conv_p,0) = 0) then
		qt_horas_conv_p	:= nvl(obter_param_medico(cd_estabelecimento_p, 'QT_HORAS_INICIO_CICLO'),0);
	end if;

	if	(nvl(qt_horas_conv_p,0) > 0) then
	
	
		if	(nvl(cd_convenio_pac_p,0) > 0) then
			cd_convenio_w	:= cd_convenio_pac_p;
		else
			cd_convenio_w	:= cd_conv_retorno_w;
		end if;
		
		dt_hora_ciclo_p			:= nvl(obter_dia_util_trat_onc(cd_convenio_w,cd_estabelecimento_p,nr_seq_paciente_p),sysdate);			
		
	end if;

	dt_hora_ciclo_aux_w	:= nvl(obter_prox_dia_util_trat_onc(nr_seq_paciente_p),sysdate);		
	
	if	(dt_hora_ciclo_aux_w > nvl(dt_hora_ciclo_p,sysdate)) then
		 dt_hora_ciclo_p	:= dt_hora_ciclo_aux_w;
	end if;
end if;

end pep_carregar_campos_onco_wdlg;
/
