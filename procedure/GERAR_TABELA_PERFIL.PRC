create or replace
procedure gerar_tabela_perfil(	cd_perfil_p	number,
				nm_tabela_p	varchar2,
				nm_usuario_p	varchar2) is

ie_controle_w	varchar2(1)	:= 'T';
ds_aplicacao_w	varchar2(15);

begin

select	ds_aplicacao
into	ds_aplicacao_w
from	tabela_sistema
where	nm_tabela = nm_tabela_p;

if	(ds_aplicacao_w = 'TasySis') then
	ie_controle_w	:= 'L';
end	if;

insert	into 
	tabela_sistema_perfil(	cd_perfil, 
				nm_tabela, 
				dt_atualizacao, 
				nm_usuario, 
				ie_controle) 
			values	(cd_perfil_p,
				nm_tabela_p,
				sysdate,
				nm_usuario_p,
				ie_controle_w);			
commit;

end	gerar_tabela_perfil;
/
