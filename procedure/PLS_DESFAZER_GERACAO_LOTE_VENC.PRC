create or replace
procedure pls_desfazer_geracao_lote_venc
			(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is

begin

delete	pls_carteira_vencimento
where	nr_seq_lote = nr_sequencia_p;

update	pls_lote_carteira
set	ie_situacao	= 'P'
where	nr_sequencia	= nr_sequencia_p;

commit;

end pls_desfazer_geracao_lote_venc;
/