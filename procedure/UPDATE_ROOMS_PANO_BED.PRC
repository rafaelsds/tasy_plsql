create or replace PROCEDURE UPDATE_ROOMS_PANO_BED (
    nm_usuario_p    IN   VARCHAR2,
    nr_room_p         IN NUMBER,
    nr_bed_p          IN NUMBER
) IS
BEGIN
    UPDATE UNIDADE_ATENDIMENTO
    SET NR_SEQ_ROOM_NUMBER = nr_room_p, NM_USUARIO = nm_usuario_p
    WHERE NR_SEQ_INTERNO = nr_bed_p;
    COMMIT;
END UPDATE_ROOMS_PANO_BED;
/
