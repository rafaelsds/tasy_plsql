create or replace
procedure importar_dep_ident_santander(	nr_seq_lote_dep_ident_p	number,
					nm_usuario_p		varchar2) is 
					
					
dt_deposito_w			date;
dt_credito_w			date;
vl_deposito_w			number(15,2);
vl_deposito_cheque_w		number(15,2);
vl_deposito_especie_w		number(15,2);
nr_sequencia_w			number(10);
ds_dt_deposito_w		varchar2(10);
ds_dt_credito_w			varchar2(10);
cd_identificacao_w		varchar2(15);
cd_agencia_w			varchar2(5);
dt_geracao_arquivo_w		date;
ds_dt_geracao_arquivo_w		varchar2(10);
cd_banco_w			varchar2(3);
qt_registro_w			number(10) := 0;
cd_historico_w			varchar2(15);

cursor c01 is
	select	substr(ds_string,143,8) dt_deposito,
		substr(ds_string,143,8) dt_credito,
		somente_numero(substr(ds_string,151,16)) || ',' || substr(ds_string,167,2) vl_especie,
		'0' vl_cheque,
		somente_numero(substr(ds_string,151,16)) || ',' || substr(ds_string,167,2) vl_deposito,
		somente_numero(substr(ds_string,202,39)) cd_identificacao,
		substr(ds_string,53,5) cd_agencia		
	from	w_retorno_banco
	where	substr(ds_string,8,1)		= '3'
	and	substr(ds_string,14,1)		= 'E'
	and	substr(ds_string,173,4)		in (	select	a.cd_historico
							from	banco_hist_dep_ident a
							where	a.cd_banco = cd_banco_w)
	and	nr_seq_lote_dep_ident		= nr_seq_lote_dep_ident_p
	and	nm_usuario 			= nm_usuario_p;
	
	
begin
delete	lote_ret_dep_ident_item
where	nr_seq_lote = nr_seq_lote_dep_ident_p;

select	max(substr(ds_string,144,8)) ds_dt_geracao,
	max(substr(ds_string,1,3)) cd_banco
into	ds_dt_geracao_arquivo_w,
	cd_banco_w
from	w_retorno_banco a
where	a.nr_seq_lote_dep_ident	= nr_seq_lote_dep_ident_p
and	substr(ds_string,7,1) = '0';

cd_banco_w := somente_numero(cd_banco_w);

/* Verificar se tem regra de hist�rico para arquivo de extrato */
select	count(1)
into	qt_registro_w
from	banco_hist_dep_ident	a
where	a.cd_banco	= cd_banco_w;

if	(length(ds_dt_geracao_arquivo_w) = 8) then
	dt_geracao_arquivo_w	:= to_date(ds_dt_geracao_arquivo_w,'DDMMYYYY');
end if;

if	(qt_registro_w > 0) then
	open c01;
	loop
	fetch c01 into	
		ds_dt_deposito_w,
		ds_dt_credito_w,
		vl_deposito_especie_w,
		vl_deposito_cheque_w,
		vl_deposito_w,
		cd_identificacao_w,
		cd_agencia_w;
	exit when c01%notfound;
		begin
		cd_identificacao_w := substr(cd_identificacao_w,1,length(cd_identificacao_w) -1);
		
		select	lote_ret_dep_ident_item_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		if	(length(ds_dt_deposito_w) = 8) then
			dt_deposito_w	:= to_date(ds_dt_deposito_w,'DDMMYYYY');
		end if;
		
		if	(length(ds_dt_credito_w) = 8) then
			dt_credito_w	:= to_date(ds_dt_credito_w,'DDMMYYYY');
		end if;
		
		insert into lote_ret_dep_ident_item
					(nr_sequencia, 
					dt_deposito, 
					vl_deposito, 
					vl_deposito_cheque,
					nm_usuario,
					dt_atualizacao,
					nr_seq_lote,
					vl_deposito_especie,
					cd_identificacao,
					dt_credito,
					cd_agencia_recebedora)
				values	(nr_sequencia_w, 
					dt_deposito_w, 
					vl_deposito_w, 
					vl_deposito_cheque_w,
					nm_usuario_p,
					sysdate,
					nr_seq_lote_dep_ident_p,
					vl_deposito_especie_w,
					cd_identificacao_w,
					dt_credito_w,
					cd_agencia_w);
		end;
		
	end loop;

	/* Atualizar data de gera��o do arquivo */
	update	lote_ret_deposito_ident
	set	dt_geracao_arquivo	= dt_geracao_arquivo_w,
		dt_importacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_lote_dep_ident_p;

	commit;
else
	/* Caso n�o tenha regra dos hist�ricos*/
	wheb_mensagem_pck.exibir_mensagem_abort(167266);
end if;

end importar_dep_ident_santander;
/