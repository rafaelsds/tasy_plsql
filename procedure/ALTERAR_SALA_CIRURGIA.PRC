create or replace
procedure alterar_sala_cirurgia(	nr_atendimento_p	number,
					nr_cirurgia_p		number,
					dt_entrada_unidade_p	date,
					dt_inicio_real_p	date,
					cd_setor_atendimento_p	number,
					cd_unidade_basica_p	varchar2,
					cd_unidade_compl_p	varchar2,
					cd_tipo_acomodacao_p	number,
					nm_usuario_p		varchar2,
					dt_termino_p		date) is 

dt_entrada_unidade_w	date;
qt_atendimento_w	number(10,0);
qt_passagem_w		number(10,0);
nr_sequencia_w		number(10,0);
nr_seq_interno_w	number(10,0);
nr_seq_interno_origem_w	number(10,0);
					
begin

select	dt_entrada_unidade
into	dt_entrada_unidade_w
from	cirurgia
where	nr_cirurgia		= nr_cirurgia_p;

select	nvl(max(nr_seq_interno),0)
into	nr_seq_interno_origem_w
from	atend_paciente_unidade
where	dt_entrada_unidade 	= dt_entrada_unidade_w
and	nr_atendimento		= nr_atendimento_p;

select	count(*)
into	qt_atendimento_w
from	atendimento_paciente
where	dt_fim_conta is not null
and	nr_atendimento		= nr_atendimento_p;

if	(qt_atendimento_w > 0) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(177185);
end if;

select	count(*)
into	qt_passagem_w
from	atend_paciente_unidade
where	nr_atendimento 		= nr_atendimento_p
and	dt_entrada_unidade 	= dt_entrada_unidade_p;

if	(qt_passagem_w > 0) then 
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(177186);
end if;

/* Gerar nova passagem */
select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	atend_paciente_unidade
where	nr_atendimento		= nr_atendimento_p;

select	atend_paciente_unidade_seq.nextval
into	nr_seq_interno_w
from	dual;

insert	into atend_paciente_unidade
	(nr_atendimento,
	nr_sequencia,
	cd_setor_atendimento,
	cd_unidade_basica,
	cd_unidade_compl,
	dt_entrada_unidade,
	dt_atualizacao,
	nm_usuario,
	cd_tipo_acomodacao,
	dt_saida_unidade,
	nr_atend_dia,
	ds_observacao,
	nm_usuario_original,
	dt_saida_interno,
	ie_passagem_setor,
	nr_acompanhante,
	nr_seq_interno,
	ie_calcular_dif_diaria,
	nr_seq_motivo_transf)
values	(nr_atendimento_p,
	nr_sequencia_w,
	cd_setor_atendimento_p,
	cd_unidade_basica_p,
	cd_unidade_compl_p,
	dt_entrada_unidade_p,
	sysdate,
	nm_usuario_p,
	cd_tipo_acomodacao_p,
	dt_termino_p,
	null,
	null,
	nm_usuario_p,
	dt_termino_p,
	'S',
	null,
	nr_seq_interno_w,
	'S',
	null);

update	cirurgia
set	nr_atendimento		= nr_atendimento_p,
	dt_entrada_unidade	= dt_entrada_unidade_p,
	dt_inicio_real		= dt_inicio_real_p,
	nm_usuario		= nm_usuario_p
where	nr_cirurgia		= nr_cirurgia_p;
commit;
		
Atend_Paciente_Unid_AfterPost(nr_seq_interno_w, 'I', nm_usuario_p);


/* Transferência de gastos */
if	(nr_seq_interno_origem_w > 0) then
	update	procedimento_paciente a
	set	a.cd_setor_Atendimento	=	cd_setor_atendimento_p,
		a.dt_entrada_unidade	=	dt_entrada_unidade_p,
		a.nr_seq_atepacu	=	nr_seq_interno_w
	where	a.nr_seq_atepacu     	= 	nr_seq_interno_origem_w
	and     a.nr_atendimento 	= 	nr_atendimento_p
	and    	not exists
			(	select	nr_atendimento
				from 	conta_paciente c
				where	a.nr_interno_conta   	= c.nr_interno_conta
				and	c.ie_status_acerto 	= 2);
	
	update	material_atend_paciente a
	set	a.cd_setor_Atendimento	=	cd_setor_atendimento_p,
		a.dt_entrada_unidade	=	dt_entrada_unidade_p,
		a.nr_seq_atepacu	=	nr_seq_interno_w
	where	a.nr_seq_atepacu     	= 	nr_seq_interno_origem_w
	and     a.nr_atendimento 	= 	nr_atendimento_p
	and    	not exists
			(	select	nr_atendimento
				from 	conta_paciente c
				where	a.nr_interno_conta   	= c.nr_interno_conta
				and	c.ie_status_acerto 	= 2);
				
	/* Deletar passagem anterior */
	delete	from atend_paciente_unidade
	where	nr_seq_interno	=	nr_seq_interno_origem_w
	and	nr_atendimento	=	nr_atendimento_p;	
end if;

commit;

end alterar_sala_cirurgia;
/