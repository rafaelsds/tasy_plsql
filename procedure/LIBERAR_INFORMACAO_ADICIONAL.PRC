create or replace
procedure liberar_informacao_adicional (nr_seq_proced_info_p	number) is 

begin

if	(nr_seq_proced_info_p is not null) then
	update	prescr_proced_inf_adic
	set	dt_liberacao = sysdate
	where	nr_sequencia = nr_seq_proced_info_p;
end if;

commit;

end liberar_informacao_adicional;
/