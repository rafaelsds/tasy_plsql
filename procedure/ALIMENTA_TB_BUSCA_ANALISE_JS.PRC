create or replace
procedure alimenta_tb_busca_analise_js
			(	ds_param_busca_p	varchar2,
				nm_usuario_p		in usuario.nm_usuario%type,
				cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
				nr_id_transacao_p	in out pls_analise_conta_temp.nr_id_transacao%type) is

begin
pls_analise_cta_pck. alimenta_tb_busca_analise(	ds_param_busca_p,
						nm_usuario_p,
						cd_estabelecimento_p,
						nr_id_transacao_p);

end alimenta_tb_busca_analise_js;
/
