create or replace
procedure projeto_recurso_comunicao(
			nr_sequencia_p			number,
			ds_perfil_adicional_p		varchar2,			
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number,
			ie_tipo_comunic_p			varchar2) is 


			
			

nr_sequencia_w				number(10,0);
nr_seq_classif_w				number(10,0);
dt_projeto_w				date;
ds_projeto_w				varchar2(80);
ds_origem_recurso_w			varchar2(15);
nr_processo_w				varchar(60);
ds_convenio_ano_w			varchar(20);
ds_receita_w				varchar(255);
ds_elemento_despesa_w			varchar(255);
nm_representante_w			varchar(255);	
vl_projeto_w				number(15,2);
dt_inicio_exec_w				date;
dt_fim_exec_w				date;
dt_fim_prest_conta_w			date;
ds_conta_w				varchar(255);
vl_utilizado_projeto_w			number(15,2);
nr_seq_conta_bco_w			number(10);		
ds_titulo_w					varchar2(100);
ds_comunicado_w				varchar2(4000);




begin

select	dt_projeto,
	ds_projeto,
	/*decode(ie_origem_recurso,'F','Federal',
				'E','Estadual',
				'M','Municipal',
				'P','Privado') ds_origem,*/
	decode(ie_origem_recurso,'F',wheb_mensagem_pck.get_texto(311665),
				 'E',wheb_mensagem_pck.get_texto(311667),
				 'M',wheb_mensagem_pck.get_texto(311668),
				 'P',wheb_mensagem_pck.get_texto(311669)) ds_origem,
	nvl(nr_processo,'') nr_processo,
	nvl(substr(ds_convenio_ano,1,20),'') ds_convenio_ano,
	nvl(substr(ds_receita,1,255),'')ds_receita,
	nvl(substr(ds_elemento_despesa,1,255),'') ds_elemento_despesa,
	nvl(substr(obter_nome_pf(cd_pf_representante),1,255),'') nm_representante,
	vl_projeto,
	dt_inicio_exec,
	dt_fim_exec,
	dt_fim_prest_conta,
	nvl(obter_saldo_projeto(nr_sequencia_p,1),0),
	nvl(nr_seq_conta_bco,0) nr_seq_conta_bco
into	dt_projeto_w,
	ds_projeto_w,	
	ds_origem_recurso_w,
	nr_processo_w,
	ds_convenio_ano_w,
	ds_receita_w,
	ds_elemento_despesa_w,
	nm_representante_w,
	vl_projeto_w,	
	dt_inicio_exec_w,
	dt_fim_exec_w,			
	dt_fim_prest_conta_w,
	vl_utilizado_projeto_w,
	nr_seq_conta_bco_w
from	projeto_recurso
where	nr_sequencia = nr_sequencia_p;


if	(nr_seq_conta_bco_w > 0) then
	select	ds_conta
	into  	ds_conta_w
	from	banco_estabelecimento_v a
	where	nr_sequencia = nr_seq_conta_bco_w;
else
	--ds_conta_w := 'Banco n�o informado.';	
	ds_conta_w := wheb_mensagem_pck.get_texto(311664);
end if;
	
	select	obter_classif_comunic('F')
	into	nr_seq_classif_w
	from	dual;

	select	comunic_interna_seq.nextval
	into	nr_sequencia_w
	from	dual;	
	
	
if (ie_tipo_comunic_p = 'L') then	
	--ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Liberado o projeto' || ' ' || ds_projeto_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311672, 'DS_PROJETO=' || ds_projeto_w) || chr(13);
	
else
	--ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Estornada a libera��o do projeto' || ' ' || ds_projeto_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311673, 'DS_PROJETO=' || ds_projeto_w) || chr(13);
end if;	
	/*ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Data projeto:' || ' ' || dt_projeto_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Origem:' || ' ' || ds_origem_recurso_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'N processo:' || ' ' || nr_processo_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Conv�nio N/Ano:' || ' ' || ds_convenio_ano_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Receita:' || ' ' || ds_receita_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Elemento:' || ' ' || ds_elemento_despesa_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Dirigente/Rep. legal:' || ' ' || nm_representante_w ||chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Valor Total Projeto:' || ' ' || substr(campo_mascara_virgula(vl_projeto_w),1,30) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Valor Utilizado:' || ' ' ||  substr(campo_mascara_virgula(vl_utilizado_projeto_w),1,30) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'In�cio Execu��o:' || ' ' || dt_inicio_exec_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Fim prest. Conta:' || ' ' || dt_fim_prest_conta_w || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' || 'Banco/Ag�ncia:' || ' ' || ds_conta_w || chr(13);*/
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311675, 'DT_PROJETO=' || dt_projeto_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311676, 'DS_ORIGEM_RECURSO=' || ds_origem_recurso_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311677, 'NR_PROCESSO=' || nr_processo_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311679, 'DS_CONVENIO_ANO=' || ds_convenio_ano_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311680, 'DS_RECEITA=' || ds_receita_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311681, 'DS_ELEMENTO_DESPESA=' || ds_elemento_despesa_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311682, 'NM_REPRESENTANTE=' || nm_representante_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311683, 'VL_PROJETO=' || substr(campo_mascara_virgula(vl_projeto_w),1,30)) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311684, 'VL_UTILIZADO_PROJETO=' || substr(campo_mascara_virgula(vl_utilizado_projeto_w),1,30)) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311685, 'DT_INICIO_EXEC=' || dt_inicio_exec_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311686, 'DT_FIM_PREST_CONTA=' || dt_fim_prest_conta_w) || chr(13);
	ds_comunicado_w :=	ds_comunicado_w || ' ' ||  wheb_mensagem_pck.get_texto(311687, 'DS_CONTA=' || ds_conta_w) || chr(13);
	
if (ie_tipo_comunic_p = 'L') then	
	--ds_titulo_w :=	'Libera��o de Projeto';
	ds_titulo_w :=	wheb_mensagem_pck.get_texto(311662);
else
	--ds_titulo_w :=	'Estorno da libera��o do Projeto';
	ds_titulo_w :=	wheb_mensagem_pck.get_texto(311663);
end if;	
	
	
	insert into comunic_interna(
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nr_sequencia,
		ie_gerencial,
		dt_liberacao,
		ds_perfil_adicional)
	values(
		sysdate,
		ds_titulo_w,
		ds_comunicado_w,
		nm_usuario_p,
		sysdate,	
		'N',
		nr_sequencia_w,
		'N',
		sysdate,
		ds_perfil_adicional_p);

	commit;	
	
end projeto_recurso_comunicao;
/