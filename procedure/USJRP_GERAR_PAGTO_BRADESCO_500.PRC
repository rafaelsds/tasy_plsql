create or replace 
procedure usjrp_gerar_pagto_bradesco_500
	(	nr_seq_envio_p		number,
		nm_usuario_p		varchar2) is

/* PADR�O CNAB 500 */

ds_conteudo_w		varchar2(500);
nr_sequencia_w		number(10)  := 1;

/* Header de arquivo */
cd_convenio_banco_w	varchar2(8);
cd_cgc_w		varchar2(14);
nm_empresa_w		varchar2(40);
nr_remessa_w		number(15);
dt_arquivo_w		varchar2(14);
cd_estabelecimento_w	number(4);

/* Transa��o */
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(15);
nm_pessoa_w		varchar2(30);
ds_endereco_w		varchar2(40);
nr_cep_w		varchar2(8);
cd_banco_w		number(3);
cd_agencia_bancaria_w	varchar2(5);
ie_digito_agencia_w	varchar2(1);
nr_conta_w		varchar2(13);
ie_digito_conta_w	varchar2(2);
nr_titulo_w		varchar2(255);
dt_vencimento_w		date;
dt_emissao_w		date;
nr_fator_vencimento_w	varchar2(4);
vl_titulo_w		number(15,2);
ie_tipo_pagamento_w	varchar2(3);
ie_modalidade_w		varchar2(2);
nr_lote_servico_w	number(10)  := 1;  /* o primeiro tem que ser dois */
vl_escritural_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_acrescimo_adic_w	number(15,2);
vl_desconto_w		number(15,2);
vl_bloqueto_w		number(15,2);
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
vl_despesa_w		number(15,2);
vl_dia_antecipacao_w	number(15,2);
vl_desc_bloqueto_w	number(15,2);
nr_conta_estab_w	varchar2(7);
dt_desconto_w		varchar2(8);
ds_inf_compl_w		varchar2(40);
ie_titularidade_w	varchar2(1);
cd_banco_estab_w	number(3);
nr_bloqueto_w		varchar2(44);
dt_remessa_retorno_w	varchar2(8);
ie_tipo_conta_fornec_w	varchar2(1);
cd_conta_extrato_w	varchar2(5);
ie_tipo_conta_w		varchar2(3);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_estab_w		varchar2(14);

cd_conv_banco_interf_w 	banco_estab_interf.cd_convenio_banco%type;
nr_seq_conta_banco_w	banco_escritural.nr_seq_conta_banco%type;


cursor	c01 is
select	decode(b.cd_pessoa_fisica,null,'2','1') ie_tipo_inscricao,
	lpad(decode(b.cd_pessoa_fisica,null,nvl(OBTER_RESP_PAGTO_TIT_PAGAR(b.nr_titulo, 'PJ'), b.cd_cgc),substr(lpad(c.nr_cpf,11,'0'),1,9) || '0000' || substr(lpad(c.nr_cpf,11,'0'),10,2)),15,'0') nr_inscricao,
	rpad(substr(obter_nome_pf_pj(b.cd_pessoa_fisica,nvl(OBTER_RESP_PAGTO_TIT_PAGAR(b.nr_titulo, 'PJ'), b.cd_cgc)),1,255),30,' ') nm_pessoa,
	rpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,nvl(OBTER_RESP_PAGTO_TIT_PAGAR(b.nr_titulo, 'PJ'), b.cd_cgc),'R'),1,40),' '),40,' ') ds_endereco,
	lpad(substr(somente_numero(obter_dados_pf_pj(b.cd_pessoa_fisica,nvl(OBTER_RESP_PAGTO_TIT_PAGAR(b.nr_titulo, 'PJ'), b.cd_cgc),'CEP')),1,8),8,'0') nr_cep,
	a.cd_banco,
	lpad(substr(nvl(a.cd_agencia_bancaria,'0'),1,5),5,'0') cd_agencia_bancaria,
	substr(nvl(a.ie_digito_agencia,' '),1,1) ie_digito_agencia,
	lpad(substr(nvl(a.nr_conta,'0'),1,13),13,'0') nr_conta,
	rpad(substr(nvl(a.ie_digito_conta,' '),1,2),2,' ') ie_digito_conta,
	a.nr_titulo,
	b.dt_vencimento_atual,
	b.dt_emissao,
	lpad(nvl(substr(b.nr_bloqueto,6,4),'0'),4,'0') nr_fator_vencimento,
	b.vl_titulo,
	a.vl_escritural,
	a.vl_acrescimo,
	a.vl_desconto,
	a.ie_tipo_pagamento,
	decode(a.ie_tipo_pagamento,'CC','05','CCP','05','OP','02','DOC','03','TED','08','31') ie_modalidade,
	a.vl_juros,
	a.vl_multa,
	a.vl_despesa,
	rpad(nvl(substr(b.nr_bloqueto,1,44),'0'),44,'0') nr_bloqueto,
	decode(a.ie_tipo_pagamento,'CC','1','CCP','2',' ') ie_tipo_conta_fornec,
	b.cd_pessoa_fisica,
	nvl(OBTER_RESP_PAGTO_TIT_PAGAR(b.nr_titulo, 'PJ'), b.cd_cgc),
	nvl(b.vl_dia_antecipacao,0)
from	pessoa_fisica c,
	titulo_pagar b,
	titulo_pagar_escrit a
where	b.cd_pessoa_fisica	= c.cd_pessoa_fisica(+)
and	a.nr_titulo		= b.nr_titulo
and	a.nr_seq_escrit		= nr_seq_envio_p;

/* Trailer do arquivo */
vl_total_w		number(15,2)  := 0;

begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* Header do arquivo */

select	lpad(substr(nvl(b.cd_convenio_banco,' '),1,8),8,'0') cd_convenio_banco,
	nvl(a.nr_remessa,a.nr_sequencia),
	to_char(sysdate,'yyyymmddhhmiss') dt_arquivo,
	a.cd_estabelecimento,
	lpad(substr(nvl(b.cd_conta,'0'),1,7),7,'0') nr_conta_estab,
	b.cd_banco,
	to_char(a.dt_remessa_retorno,'yyyymmdd') dt_remessa_retorno,
	c.cd_cgc,
	rpad(substr(obter_nome_pf_pj(null,c.cd_cgc),1,255),40,' ') nm_empresa,
	lpad(nvl(null,'0'),5,'0') cd_conta_extrato,
	a.nr_seq_conta_banco
into	cd_convenio_banco_w,
	nr_remessa_w,
	dt_arquivo_w,
	cd_estabelecimento_w,
	nr_conta_estab_w,
	cd_banco_estab_w,
	dt_remessa_retorno_w,
	cd_cgc_estab_w,
	nm_empresa_w,
	cd_conta_extrato_w,
	nr_seq_conta_banco_w
from	estabelecimento c,
	banco_estabelecimento b,
	banco_escritural a
where	c.cd_estabelecimento	= decode(b.nr_sequencia, 22, 5, nvl(b.cd_estabelecimento,a.cd_estabelecimento))
and	a.nr_seq_conta_banco	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_envio_p;

if	(nr_seq_conta_banco_w is not null) then

	select	max(a.cd_convenio_banco)
	into	cd_conv_banco_interf_w
	from	banco_estab_interf a
	where	a.ie_tipo_interf_fin 	= 'PE' /*Somente se a interface estiver cadastrada como Pagamento escritural envio*/
	and		upper(a.ds_objeto)		= upper('USJRP_GERAR_PAGTO_BRADESCO_500')
	and		a.nr_seq_conta_banco	= nr_seq_conta_banco_w
	and		upper(a.nm_tabela)		= upper('W_ENVIO_BANCO')
	and		a.ie_situacao			= 'S';
	
	if	(cd_conv_banco_interf_w	is not null) then
		cd_convenio_banco_w	:= lpad(substr(nvl(cd_conv_banco_interf_w,' '),1,8),8,'0');
	end if;
end if;

ds_conteudo_w	:=	'0' ||
			cd_convenio_banco_w ||
			'2' ||
			lpad(cd_cgc_estab_w,15,'0') ||
			nm_empresa_w ||
			'20' ||
			'1' ||
			lpad(nr_remessa_w,5,'0') ||
			'00000' ||
			dt_arquivo_w ||
			rpad(' ',5,' ') ||
			rpad(' ',3,' ') ||
			rpad(' ',5,' ') ||
			'0' ||
			rpad(' ',74,' ') ||
			rpad(' ',80,' ') ||
			rpad(' ',217,' ') ||
			/* lpad(nr_remessa_w,9,'0') || */
			lpad('0',9,'0') ||
			rpad(' ',8,' ') ||
			lpad(nr_sequencia_w,6,'0');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_lote_servico_w,
	nr_lote_servico_w,
	w_envio_banco_seq.nextval);

/* Transa��o */

open	c01;
loop
fetch	c01 into
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	nr_cep_w,
	cd_banco_w,
	cd_agencia_bancaria_w,
	ie_digito_agencia_w,
	nr_conta_w,
	ie_digito_conta_w,
	nr_titulo_w,
	dt_vencimento_w,
	dt_emissao_w,
	nr_fator_vencimento_w,
	vl_titulo_w,
	vl_escritural_w,
	vl_acrescimo_w,
	vl_desconto_w,
	ie_tipo_pagamento_w,
	ie_modalidade_w,
	vl_juros_w,
	vl_multa_w,
	vl_despesa_w,
	nr_bloqueto_w,
	ie_tipo_conta_fornec_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	vl_dia_antecipacao_w;
exit	when c01%notfound;

	vl_bloqueto_w		:= null;
	vl_desc_bloqueto_w	:= null;

	if	(ie_modalidade_w  = '31') and
		(somente_numero(nr_bloqueto_w) > 0) then -- Edgar 04/01/2013, tirar o valor de despesa
		vl_escritural_w		:= vl_escritural_w + vl_despesa_w;
		vl_bloqueto_w		:= dividir_sem_round(somente_numero(substr(nr_bloqueto_w, 10, 10)),100);
		vl_desc_bloqueto_w	:= vl_dia_antecipacao_w;
		vl_despesa_w		:= 0;
	end if;

	if	(cd_cgc_w	is not null) then

		select  max(a.ie_tipo_conta)
		into	ie_tipo_conta_w
		from	pessoa_juridica_conta a
		where	a.ie_situacao			= 'A'
		and	a.ie_tipo_conta			= 'CS'
		and	somente_numero(a.nr_conta)	= somente_numero(nr_conta_w)
		and	a.cd_cgc			= cd_cgc_w;

	else

		select	max(a.ie_tipo_conta)
		into	ie_tipo_conta_w
		from	pessoa_fisica_conta a
		where	a.ie_situacao			= 'A'
		and	a.ie_tipo_conta			= 'CS'
		and	somente_numero(a.nr_conta)	= somente_numero(nr_conta_w)
		and	a.cd_pessoa_fisica		= cd_pessoa_fisica_w;

	end if;

	if	(nvl(ie_tipo_conta_w,'X')	<> 'CS') then

		cd_conta_extrato_w	:= lpad('0',5,'0');

	else

		cd_conta_extrato_w	:= '00298';

	end if;

	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	vl_total_w		:= nvl(vl_total_w,0) + nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0) + nvl(vl_despesa_w,0);

	if	(nvl(vl_desconto_w,0)	<> 0) or (nvl(vl_desc_bloqueto_w,0) <> 0) then
		dt_desconto_w	:= to_char(dt_vencimento_w,'yyyymmdd');
	else
		dt_desconto_w	:= '00000000';
	end if;

	if	(ie_modalidade_w	= '01') or
		(ie_modalidade_w	= '05') then

		ds_inf_compl_w  := rpad(' ',40,' ');

	elsif	(ie_modalidade_w	= '02') then

		ds_inf_compl_w	:= rpad(' ',40,' ');

	elsif	(ie_modalidade_w	= '03') or
		(ie_modalidade_w	= '08') then

		if	(cd_banco_w	= cd_banco_estab_w) then
			ds_inf_compl_w	:= 'D';
		else
			ds_inf_compl_w	:= 'C';
		end if;

		ds_inf_compl_w	:=	ds_inf_compl_w ||
					lpad('0',6,'0') ||
					'01' ||
					'01' ||
					lpad('0',18,'0') ||
					rpad(' ',11,' ');

	elsif	(ie_modalidade_w	= '30') then

		ds_inf_compl_w	:=	rpad(' ',25,' ') ||
					nr_inscricao_w;
		cd_conta_extrato_w	:= lpad('0',5,'0');

	elsif	(ie_modalidade_w	= '31') then

		ds_inf_compl_w	:=	substr(nr_bloqueto_w,20,25) ||
					substr(nr_bloqueto_w,5,1) ||
					substr(nr_bloqueto_w,4,1) ||
					rpad(' ',13,' ');
		cd_conta_extrato_w	:= lpad('0',5,'0');

		if	(cd_banco_w <> 237) then
			cd_agencia_bancaria_w	:= '00000';
			ie_digito_agencia_w	:= '0';
			nr_conta_w		:= '0000000000000';
			ie_digito_conta_w	:= '00';
		end if;

	else

		cd_conta_extrato_w	:= lpad('0',5,'0');

	end if;

	/* Edgar 23/01/2013, retirado est� dando problema, o informage tb n�o gera
	if	(somente_numero(nr_bloqueto_w) > 0) and
		(somente_numero(nr_fator_vencimento_w)	= 0) then

		nr_fator_vencimento_w	:= to_char(trunc(dt_vencimento_w,'dd') - to_date('07/10/1997','dd/mm/yyyy'));

	end if;
	*/
	
	-- OS 587254 - Quando for bloqueto informa valor total, e n�o informa valor de acrescimo
	select	decode(ie_tipo_pagamento_w,'BLQ',0,vl_acrescimo_w)
	into	vl_acrescimo_adic_w
	from	dual;

	ds_conteudo_w	:=	'1' ||
				ie_tipo_inscricao_w ||
				nr_inscricao_w ||
				nm_pessoa_w ||
				ds_endereco_w ||
				nr_cep_w ||
				lpad(cd_banco_w,3,'0') ||
				lpad(cd_agencia_bancaria_w,5,'0') ||
				lpad(ie_digito_agencia_w,1,'0') ||
				lpad(nr_conta_w, 13, '0') ||
				lpad(ie_digito_conta_w,2, '0') ||
				rpad(nr_titulo_w || '-' || nr_seq_envio_p ,16,' ') ||
				'000' ||
				'000000000000' ||
				rpad(nr_titulo_w,15,' ') ||
				to_char(dt_vencimento_w,'yyyymmdd') ||
				to_char(dt_emissao_w,'yyyymmdd') ||
				dt_desconto_w ||
				'0' ||
				nr_fator_vencimento_w ||
				lpad(somente_numero(to_char(nvl(nvl(vl_bloqueto_w, vl_escritural_w),0),'99999990.00')),10,'0') ||
				lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||
				lpad(somente_numero(to_char(nvl(vl_desconto_w,0) + nvl(vl_desc_bloqueto_w, 0),'9999999999990.00')),15,'0') ||
				lpad(somente_numero(to_char(nvl(vl_acrescimo_adic_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
				'05' ||
				'0000000000' ||
				rpad(' ',2,' ') ||
				ie_modalidade_w ||
				dt_remessa_retorno_w ||
				rpad(' ',3,' ') ||
				'01' ||
				rpad(' ',10,' ') ||
				'0' ||
				'00' ||
				rpad(' ',4,' ') ||
				rpad(' ',15,' ') ||
				rpad(' ',15,' ') ||
				rpad(' ',6,' ') ||
				rpad(' ',40,' ') ||
				' ' ||
				' ' ||
				ds_inf_compl_w ||
				'00' ||
				rpad(' ',35,' ') ||
				rpad(' ',22,' ') ||
				cd_conta_extrato_w ||
				' ' ||
				ie_tipo_conta_fornec_w ||
				nr_conta_estab_w ||
				rpad(' ',8,' ') ||
				lpad(nr_lote_servico_w,6,'0');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_lote_servico_w,
		nr_lote_servico_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c01;

/* Trailer do arquivo */

nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;

ds_conteudo_w	:=	'9' ||
			lpad(nr_lote_servico_w,6,'0') ||
			lpad(somente_numero(to_char(nvl(vl_total_w,0),'999999999999990.00')),17,'0') ||
			rpad(' ',470,' ') ||
			lpad(nr_lote_servico_w,6,'0');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_lote_servico_w,
	nr_lote_servico_w,
	w_envio_banco_seq.nextval);

commit;

end usjrp_gerar_pagto_bradesco_500;
/
