create or replace PROCEDURE schematic4test_sp_jenkins (nm_usuario_p VARCHAR2,ds_version_p VARCHAR2, nr_package_p NUMBER,cd_funcao_p VARCHAR2) IS
nr_seq_executionw NUMBER(10, 0);
nr_seq_suitew NUMBER(10, 0);
nr_seq_urlw NUMBER(10, 0);
nr_seq_browserw NUMBER(10, 0);
nr_seq_servicew NUMBER(10, 0);
nr_seq_exe_scripw NUMBER(10, 0);
nr_seq_gridw NUMBER(10, 0);
nr_seq_robotw NUMBER(10, 0);
nr_seq_scriptw NUMBER(10, 0);
newsequencepackage NUMBER(10, 0);
nr_seq_presetw NUMBER(10, 0);
newsequencesuite NUMBER(10, 0);
nr_seq_execution_w NUMBER(10, 0);
qtd_w NUMBER(10, 0);
qtd_w2 NUMBER(10, 0);
QTD_W3 NUMBER(10, 0);
QTD_REGRA_VV3 NUMBER(10, 0);
QTD_REGRA_VV2 NUMBER(10, 0);
QTD_REGRA_VV NUMBER(10, 0);
nm_packagew VARCHAR(255);
nm_suitew VARCHAR(255);
ds_versionw VARCHAR(255);
ie_jobsw VARCHAR(255);
ie_chainw VARCHAR(255);
nr_seq_snapshotw NUMBER(10, 0);
ie_switchitemw VARCHAR2(255);
ie_jobsitemw VARCHAR(255);
ds_version_w VARCHAR(255);
NEWSEQUENCESCHEDPACKAGE NUMBER(10, 0);
newsequencesuite2 NUMBER(10, 0);
NR_SEQ_EXECUTIONW2 NUMBER(10, 0);
NR_SEQ_GRIDW2 NUMBER(10, 0);
NR_SEQ_ROBOTW2 NUMBER(10, 0);
NR_SEQ_SCRIPTW2 NUMBER(10, 0);
newsequencesuitelocale NUMBER(10, 0);
QTD_SUITE_SCOPE NUMBER(10, 0);

CURSOR c01 IS --loop lista todas suites
SELECT item.nr_seq_execution,item.nr_seq_suite,item.nr_seq_url,item.nr_seq_browser,item.nr_seq_service,item.ie_switch,item.ie_jobs
  INTO nr_seq_executionw,nr_seq_suitew,nr_seq_urlw,nr_seq_browserw,nr_seq_servicew,ie_switchitemw,ie_jobsitemw
FROM schem_test_package_item item 
WHERE item.nr_seq_package = nr_package_p;

CURSOR C03 IS --loop suite scope
  SELECT DISTINCT teste.NR_SEQ_EXECUTION, teste.NR_SEQ_GRID, teste.NR_SEQ_ROBOT, teste.NR_SEQ_SCRIPT
    INTO 	nr_seq_exe_scripw, nr_seq_gridw, nr_seq_robotw,	nr_seq_scriptw
									FROM SCHEM_TEST_TEST teste, SCHEM_TEST_PACKAGE_ITEM item, SCHEM_TEST_SCRIPT script, SCHEM_TEST_SUITE suite WHERE teste.NR_SEQ_SUITE = item.NR_SEQ_SUITE AND item.NR_SEQ_PACKAGE = nr_package_p
									AND teste.NR_SEQ_SCRIPT = SCRIPT.NR_SEQUENCIA 
                  AND suite.NR_SEQUENCIA = teste.NR_SEQ_SUITE
                  AND suite.NM_SUITE LIKE 'Version - Minimum Scope'
                  AND teste.IE_SWITCH = '1';

sql_c01_qtd VARCHAR2(32000);
sql_c02 VARCHAR2(32000);

TYPE s4tcur IS REF CURSOR;
c02 s4tcur;

BEGIN
  --se diferente de null, entra no bloco.
  IF (cd_funcao_p IS NOT NULL) THEN   
  IF ( nr_package_p IS NOT NULL ) THEN
    IF ( ds_version_p IS NULL ) THEN
      ds_version_w := 0;
    ELSE
      ds_version_w := ds_version_p;
  END IF;

  --Insere o pacote
  SELECT schem_test_package_seq.NEXTVAL sequencia
    INTO newsequencepackage
  FROM dual;

  SELECT nm_package, ie_jobs, ie_chain, nr_seq_snapshot 
    INTO nm_packagew, ie_jobsw, ie_chainw, nr_seq_snapshotw
  FROM schem_test_package
  WHERE nr_sequencia = nr_package_p;

  INSERT INTO schem_test_package (nr_sequencia,nm_package,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_jobs,ie_chain,nm_owner,nr_seq_snapshot,ie_situacao)
    VALUES (newsequencepackage,nm_packagew || ' #SP ',sysdate,nm_usuario_p,sysdate,nm_usuario_p,ie_jobsw,ie_chainw,nm_usuario_p,nr_seq_snapshotw,'I');
    
  --suites que tem script das funcoes da branch do sp
  OPEN c01;
  LOOP
  FETCH c01 INTO
    nr_seq_executionw,
    nr_seq_suitew,
    nr_seq_urlw,
    nr_seq_browserw,
    nr_seq_servicew,
    ie_switchitemw,
    ie_jobsitemw;
  EXIT WHEN c01%notfound;
  BEGIN
    sql_c01_qtd := 'SELECT COUNT(teste.nr_sequencia) qtd
                    FROM SCHEM_TEST_TEST teste, SCHEM_TEST_PACKAGE_ITEM item, SCHEM_TEST_SUITE suite
                    WHERE teste.NR_SEQ_SUITE = '|| nr_seq_suitew|| ' AND teste.NR_SEQ_SUITE = item.NR_SEQ_SUITE 
                    AND suite.NR_SEQUENCIA = teste.NR_SEQ_SUITE AND suite.NM_SUITE NOT LIKE ''Version - Minimum Scope''
                    AND teste.NR_SEQ_SCRIPT IN (SELECT NR_SEQUENCIA FROM SCHEM_TEST_SCRIPT WHERE CD_FUNCAO IN('|| schematic4test_nrseq_in_list(cd_funcao_p)|| '))';

    EXECUTE IMMEDIATE sql_c01_qtd
      INTO qtd_w;
      IF ( qtd_w <> 0 ) THEN
        SELECT nr_seq_preset,nm_suite,ds_version 
          INTO nr_seq_presetw,nm_suitew,ds_versionw
        FROM schem_test_suite
        WHERE nr_sequencia = nr_seq_suitew;
        
        --pega nova sequencia de suite e insere a suite com os valores das variaveis.
        SELECT schem_test_suite_seq.NEXTVAL sequencia
          INTO newsequencesuite
        FROM dual;
        
        INSERT INTO schem_test_suite (nr_sequencia,nm_suite,ds_version,ie_runtype,nm_owner,dt_atualizacao,nm_usuario,nm_usuario_nrec,dt_atualizacao_nrec,nr_seq_preset,ds_repo,ie_jobs,ie_test_local,ie_situacao)
          VALUES (newsequencesuite,nm_suitew || ' #SP ',ds_versionw,1,nm_usuario_p,sysdate,nm_usuario_p,nm_usuario_p,sysdate,nr_seq_presetw,'1','0',0,'I');
          
        sql_c02 := 'SELECT DISTINCT teste.NR_SEQ_EXECUTION, teste.NR_SEQ_GRID, teste.NR_SEQ_ROBOT, teste.NR_SEQ_SCRIPT FROM SCHEM_TEST_TEST teste, SCHEM_TEST_PACKAGE_ITEM item, SCHEM_TEST_SUITE suite WHERE 
                    teste.NR_SEQ_SUITE = '|| nr_seq_suitew || ' AND teste.NR_SEQ_SUITE = item.NR_SEQ_SUITE AND item.NR_SEQ_PACKAGE = '|| nr_package_p || ' 
                    AND suite.NR_SEQUENCIA = teste.NR_SEQ_SUITE AND suite.NM_SUITE NOT LIKE ''Version - Minimum Scope''
                    AND teste.NR_SEQ_SCRIPT IN (SELECT NR_SEQUENCIA FROM SCHEM_TEST_SCRIPT WHERE CD_FUNCAO IN('|| schematic4test_nrseq_in_list(cd_funcao_p)|| '))AND teste.IE_SWITCH ='|| '''1''';
        
        --loop dos testes associado
        OPEN c02 FOR sql_c02;
        LOOP
        FETCH c02 INTO
          nr_seq_exe_scripw,
          nr_seq_gridw,
          nr_seq_robotw,
          nr_seq_scriptw;
        EXIT WHEN c02%notfound;
        BEGIN
        IF ( nr_seq_execution_w IS NULL ) THEN
            nr_seq_execution_w := 0;
        END IF;
        
        SELECT COUNT(nr_sequencia) qtd 
          INTO qtd_w2
        FROM schem_test_test 
        WHERE nr_seq_script = nr_seq_scriptw
        AND nr_seq_suite = newsequencesuite;

        IF ( qtd_w2 = 0 ) THEN
            INSERT INTO schem_test_test (nr_sequencia,nr_seq_execution,nr_seq_script,nr_seq_suite,nr_seq_robot,nr_seq_grid,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_jobs,ie_switch) 
              VALUES (schem_test_test_seq.NEXTVAL,nr_seq_execution_w + 5,nr_seq_scriptw,newsequencesuite,nr_seq_robotw,nr_seq_gridw,sysdate,nm_usuario_p,sysdate,nm_usuario_p,'0','1');

        SELECT MAX(nr_seq_execution) nr_seq_execution
            INTO nr_seq_execution_w
        FROM schem_test_test
        WHERE nr_seq_suite = newsequencesuite;
      END IF;
      END;
      END LOOP;
      CLOSE c02;
        INSERT INTO schem_test_package_item (nr_sequencia,nr_seq_execution,nr_seq_suite,nr_seq_url,nr_seq_browser,nr_seq_service,dt_atualizacao_nrec,nr_seq_package,nm_usuario_nrec,ie_switch,ie_jobs)
          VALUES (schem_test_package_item_seq.NEXTVAL,nr_seq_executionw,newsequencesuite,nr_seq_urlw,nr_seq_browserw,nr_seq_servicew,sysdate,newsequencepackage,nm_usuario_p,ie_switchitemw,ie_jobsitemw);
      END IF;
      END;
      END LOOP;
      CLOSE c01;
      
      SELECT COUNT(*)
						INTO QTD_SUITE_SCOPE
				FROM schem_test_suite
				WHERE NM_SUITE LIKE 'Version - Minimum Scope';
        
        IF (QTD_SUITE_SCOPE <> 0) THEN
          SELECT nr_seq_preset,nm_suite,ds_version 
						INTO nr_seq_presetw,nm_suitew,ds_versionw
          FROM schem_test_suite
          WHERE NM_SUITE LIKE 'Version - Minimum Scope';
          
           SELECT schem_test_suite_seq.NEXTVAL sequencia
                INTO newsequencesuite
              FROM dual;
            
          INSERT INTO schem_test_suite (nr_sequencia,nm_suite,ds_version,ie_runtype,nm_owner,dt_atualizacao,nm_usuario,nm_usuario_nrec,dt_atualizacao_nrec,nr_seq_preset,ds_repo,ie_jobs,ie_test_local,ie_situacao)
                VALUES (newsequencesuite,nm_suitew || ' #SP ',ds_versionw,1,nm_usuario_p,sysdate,nm_usuario_p,nm_usuario_p,sysdate,nr_seq_presetw,'1','0','0','I');
                  
              OPEN C03;
							LOOP
							FETCH C03 INTO
								nr_seq_exe_scripw,
								nr_seq_gridw,
								nr_seq_robotw,
								nr_seq_scriptw;
							EXIT WHEN C03%notfound;
							BEGIN           
                    IF (nr_seq_execution_w IS NULL) THEN
                      nr_seq_execution_w := 0;
                    END IF;
        
                    SELECT COUNT(nr_sequencia) qtd 
                      INTO qtd_w3
                    FROM schem_test_test 
                    WHERE nr_seq_script = nr_seq_scriptw
                    AND nr_seq_suite = newsequencesuite;

                  IF (qtd_w3 = 0) THEN                	
                    INSERT INTO schem_test_test (nr_sequencia,nr_seq_execution,nr_seq_script,nr_seq_suite,nr_seq_robot,nr_seq_grid,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_jobs,ie_switch) 
                      VALUES (schem_test_test_seq.NEXTVAL,nr_seq_execution_w + 5,nr_seq_scriptw,newsequencesuite,nr_seq_robotw,nr_seq_gridw,sysdate,nm_usuario_p,sysdate,nm_usuario_p,'0','1');
                  
                    SELECT MAX(nr_seq_execution) nr_seq_execution
                      INTO nr_seq_execution_w
                    FROM schem_test_test
                    WHERE nr_seq_suite = newsequencesuite;
								END IF;
							END;
							END LOOP;
							CLOSE C03;       
              
              INSERT INTO schem_test_package_item (nr_sequencia,nr_seq_execution,nr_seq_suite,nr_seq_url,nr_seq_browser,nr_seq_service,dt_atualizacao_nrec,nr_seq_package,nm_usuario_nrec,ie_switch,ie_jobs)
                VALUES (schem_test_package_item_seq.NEXTVAL,nr_seq_executionw,newsequencesuite,nr_seq_urlw,nr_seq_browserw,nr_seq_servicew,sysdate,newsequencepackage,nm_usuario_p,ie_switchitemw,ie_jobsitemw);         
              END IF;
              
      --pega nova sequencia de grupo de atividade.
      SELECT schem_test_package_sched_seq.NEXTVAL sequencia
        INTO newsequenceschedpackage
      FROM dual;
       
      --Insere o grupo de agendamento
      INSERT INTO schem_test_package_sched (nr_sequencia,nr_seq_package,ds_version,nm_owner,ds_info,dt_atualizacao_nrec,dt_atualizacao,nm_usuario_nrec,nm_usuario,ie_status,dt_schedule, ie_video, IE_ESTAG_EXEC, IE_AUTO_RESTORE) 
          VALUES (newsequenceschedpackage,newsequencepackage,ds_version_w,nm_usuario_p,'By '||nm_usuario_p,sysdate,sysdate,nm_usuario_p,nm_usuario_p,1,sysdate, 'N', '0', 'S');
      COMMIT;
      END IF;
    ELSE
	 IF (nr_package_p IS NOT NULL) THEN
		IF (ds_version_p IS NULL) THEN
			ds_version_w := '0';
		ELSE
			ds_version_w := ds_version_p;
		END IF;

		SELECT schem_test_package_seq.NEXTVAL sequencia
			INTO newsequencepackage
		FROM dual;

		SELECT nm_package, ie_jobs, ie_chain, nr_seq_snapshot 
			INTO nm_packagew, ie_jobsw, ie_chainw, nr_seq_snapshotw
		FROM schem_test_package
		WHERE nr_sequencia = nr_package_p;

		INSERT INTO schem_test_package (nr_sequencia,nm_package,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_jobs,ie_chain,nm_owner,nr_seq_snapshot,ie_situacao)
			VALUES (newsequencepackage,nm_packagew || ' #SP ',sysdate,nm_usuario_p,sysdate,nm_usuario_p,ie_jobsw,ie_chainw,nm_usuario_p,nr_seq_snapshotw,'I');
      
       SELECT COUNT(*)
						INTO QTD_SUITE_SCOPE
				FROM schem_test_suite
				WHERE NM_SUITE LIKE 'Version - Minimum Scope';
        
        IF (QTD_SUITE_SCOPE <> 0) THEN
          SELECT nr_seq_preset,nm_suite,ds_version 
						INTO nr_seq_presetw,nm_suitew,ds_versionw
          FROM schem_test_suite
          WHERE NM_SUITE LIKE 'Version - Minimum Scope';
          
           SELECT schem_test_suite_seq.NEXTVAL sequencia
                INTO newsequencesuite
              FROM dual;
            
          INSERT INTO schem_test_suite (nr_sequencia,nm_suite,ds_version,ie_runtype,nm_owner,dt_atualizacao,nm_usuario,nm_usuario_nrec,dt_atualizacao_nrec,nr_seq_preset,ds_repo,ie_jobs,ie_test_local,ie_situacao)
                VALUES (newsequencesuite,nm_suitew || ' #SP ',ds_versionw,1,nm_usuario_p,sysdate,nm_usuario_p,nm_usuario_p,sysdate,nr_seq_presetw,'1','0','0','I');
                  
              OPEN C03;
							LOOP
							FETCH C03 INTO
								nr_seq_exe_scripw,
								nr_seq_gridw,
								nr_seq_robotw,
								nr_seq_scriptw;
							EXIT WHEN C03%notfound;
							BEGIN           
                    IF (nr_seq_execution_w IS NULL) THEN
                      nr_seq_execution_w := 0;
                    END IF;
        
                    SELECT COUNT(nr_sequencia) qtd 
                      INTO qtd_w3
                    FROM schem_test_test 
                    WHERE nr_seq_script = nr_seq_scriptw
                    AND nr_seq_suite = newsequencesuite;

                  IF (qtd_w3 = 0) THEN                	
                    INSERT INTO schem_test_test (nr_sequencia,nr_seq_execution,nr_seq_script,nr_seq_suite,nr_seq_robot,nr_seq_grid,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_jobs,ie_switch) 
                      VALUES (schem_test_test_seq.NEXTVAL,nr_seq_execution_w + 5,nr_seq_scriptw,newsequencesuite,nr_seq_robotw,nr_seq_gridw,sysdate,nm_usuario_p,sysdate,nm_usuario_p,'0','1');
                  
                    SELECT MAX(nr_seq_execution) nr_seq_execution
                      INTO nr_seq_execution_w
                    FROM schem_test_test
                    WHERE nr_seq_suite = newsequencesuite;
								END IF;
							END;
							END LOOP;
							CLOSE C03;       
              
            SELECT nr_seq_url, nr_seq_browser, nr_seq_service
              INTO nr_seq_urlw, nr_seq_browserw, nr_seq_servicew
            from schem_test_package_item
            WHERE NR_SEQ_SUITE = (SELECT NR_SEQUENCIA FROM SCHEM_TEST_SUITE WHERE NM_SUITE LIKE 'Version - Minimum Scope')
            AND NR_SEQ_PACKAGE = nr_package_p;
              
              INSERT INTO schem_test_package_item (nr_sequencia,nr_seq_execution,nr_seq_suite,nr_seq_url,nr_seq_browser,nr_seq_service,dt_atualizacao_nrec,nr_seq_package,nm_usuario_nrec,ie_switch,ie_jobs)
                VALUES (schem_test_package_item_seq.NEXTVAL,5,newsequencesuite,nr_seq_urlw,nr_seq_browserw,nr_seq_servicew,sysdate,newsequencepackage,nm_usuario_p,ie_switchitemw,ie_jobsitemw);         
              END IF;
              
              SELECT schem_test_package_sched_seq.NEXTVAL sequencia
                INTO newsequenceschedpackage
              FROM dual;
      
               INSERT INTO schem_test_package_sched (nr_sequencia,nr_seq_package,ds_version,nm_owner,ds_info,dt_atualizacao_nrec,dt_atualizacao,nm_usuario_nrec,nm_usuario,ie_status,dt_schedule, ie_video, IE_ESTAG_EXEC, IE_AUTO_RESTORE) 
          VALUES (newsequenceschedpackage,newsequencepackage,ds_version_w,nm_usuario_p,'By '||nm_usuario_p,sysdate,sysdate,nm_usuario_p,nm_usuario_p,1,sysdate, 'N', '0', 'S');
               END IF;
      END IF;
EXCEPTION
WHEN no_data_found THEN
  dbms_output.put_line('Erro: Data not found');
END schematic4test_sp_jenkins;
/