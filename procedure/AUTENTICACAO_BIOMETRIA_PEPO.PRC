create or replace
procedure autenticacao_biometria_pepo	(	nr_sequencia_p		number,
						nr_prescricao_p		number,
						cd_pessoa_fisica_p	varchar2,
						ie_opcao_p		number,
						nm_usuario_p		varchar2) is 
/*
1 - Materiais  Resumo de gastos
2 - Procedimentos
*/
ds_biometria_w			varchar2(4000);

begin
begin

if	(cd_pessoa_fisica_p is not null) then

	select	max(ds_biometria)
	into	ds_biometria_w
	from	pessoa_fisica_biometria
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	if	(ie_opcao_p = 1) then
		update	cirurgia_agente_disp
		set	nm_usuario_biometria = nm_usuario_p,
			dt_biometria = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where   nr_sequencia = nr_sequencia_p;
		
	elsif	(ie_opcao_p = 2) then
		
		update	prescr_procedimento
		set	nm_usuario_biometria = nm_usuario_p,
			dt_biometria = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia = nr_sequencia_p;
		
	end if;

end if;

commit;

exception
when others then
	null;
end;	

end autenticacao_biometria_pepo;
/