create or replace 
procedure suspender_etapas_sne_js(	ds_sequencia_hor_p	Varchar2,
					nr_seq_motivo_p		Number,
					ds_justificativa_p 	Varchar2,
					nm_usuario_p		Varchar2) is

ds_horarios_w	Varchar2(4000) := '';
nr_sequencia_w	Number(14);
k		integer;
					
begin

ds_horarios_w := ds_sequencia_hor_p;

while	ds_horarios_w is not null loop
	begin
	select	instr(ds_horarios_w, ',') 
	into	k
	from	dual;

	if	(k > 1) and
		(substr(ds_horarios_w, 1, k -1) is not null) then
		nr_sequencia_w		:= substr(ds_horarios_w, 1, k-1);
		nr_sequencia_w		:= replace(nr_sequencia_w, ',','');
		ds_horarios_w	:= substr(ds_horarios_w, k + 1, 4000);
	elsif	(ds_horarios_w is not null) then
		nr_sequencia_w		:= replace(ds_horarios_w,',','');
		ds_horarios_w	:= '';
	end if;
	
	suspender_etapa_sne(nr_sequencia_w, nm_usuario_p, nr_seq_motivo_p, ds_justificativa_p);
	
	end;
end loop;

commit;

end suspender_etapas_sne_js;
/