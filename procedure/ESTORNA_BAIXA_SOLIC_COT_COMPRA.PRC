create or replace
procedure estorna_baixa_solic_cot_compra(	nr_cot_compra_p		number,
					nm_usuario_p		Varchar2) is 

nr_solic_compra_w				number(10);
nr_item_solic_compra_w				number(10);

cursor c01 is
select	distinct
	nr_solic_compra,
	nr_item_solic_compra
from(	select	a.nr_solic_compra,
		a.nr_item_solic_compra
	from	cot_compra_item a
	where	a.nr_cot_compra = nr_cot_compra_p
	and	a.nr_solic_compra is not null
	union
	select	b.nr_solic_compra,
		b.nr_item_solic_compra
	from	cot_compra_item a,
		cot_compra_solic_agrup b
	where	a.nr_cot_compra = b.nr_cot_compra
	and	a.nr_item_cot_compra = b.nr_item_cot_compra
	and	a.nr_cot_compra = nr_cot_compra_p
	and	b.nr_solic_compra is not null);
			
begin

open C01;
loop
fetch C01 into	
	nr_solic_compra_w,
	nr_item_solic_compra_w;
exit when C01%notfound;
	begin
	update	solic_compra
	set	dt_baixa = ''
	where	nr_solic_compra	= nr_solic_compra_w
	and	nr_seq_motivo_cancel is null
	and	dt_baixa is not null;
	
	update	solic_compra_item
	set	dt_baixa = ''
	where	nr_solic_compra	= nr_solic_compra_w
	and	nr_item_solic_compra = nr_item_solic_compra_w
	and	nr_seq_motivo_cancel is null
	and	dt_baixa is not null;	
	
	
	gerar_historico_solic_compra(
			nr_solic_compra_w,
			wheb_mensagem_pck.get_texto(300287),
			wheb_mensagem_pck.get_texto(300289, 'NR_COT_COMPRA=' || nr_cot_compra_p),
			'T',
			nm_usuario_p);
	
	end;
end loop;
close C01;

commit;

end estorna_baixa_solic_cot_compra;
/