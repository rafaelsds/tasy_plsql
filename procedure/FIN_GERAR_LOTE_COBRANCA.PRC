create or replace
procedure fin_gerar_lote_cobranca(	dt_inicio_p		date default sysdate,
				dt_fim_p			date default sysdate,
				ie_tipo_p			integer default 0,
				ds_titulo_p		varchar2 default '',
				ie_consiste_data_p		varchar2 default 'N',
				cd_cnpj_p		varchar2 default 'X',
				nm_usuario_p		varchar2) is

dt_referencia_w			date;
dt_emissao_w			date;
dt_pagamento_previsto_w		date;

nr_titulo_w			number(10);
nr_seq_cliente_w			number(10);
nr_seq_lote_w			number(10);
nr_seq_tit_cob_w			number(10);
cd_estabelecimento_w		number(4);
vl_titulo_w			number(15,2);
vl_saldo_lote_w			number(15,2) := 0;
pr_indice_w			number(15,2);
vl_saldo_tit_w			number(15,2);

nr_nfe_imp_w			varchar2(255);
ds_observacao_w			varchar2(255);

cursor	c01 is
select	a.nr_titulo,
	nvl(a.nr_seq_cliente,b.nr_seq_cliente),
	nvl(a.vl_saldo_titulo,0),
	b.nr_nfe_imp,
	a.dt_emissao,
	a.dt_pagamento_previsto,
	substr(a.ds_observacao_titulo,1,255)
from	nota_fiscal b,
	titulo_receber a
where	a.nr_seq_nf_saida = b.nr_sequencia (+)
and	nvl(a.vl_saldo_titulo,0) > 0
and	(a.cd_estabelecimento = cd_estabelecimento_w or a.cd_estab_financeiro = cd_estabelecimento_w)
and 	substr(obter_se_prod_perfil(a.nr_seq_grupo_prod,1817),1,1) = 'S'
and	(((ie_consiste_data_p = 'S') and (a.dt_pagamento_previsto between dt_inicio_p and fim_dia(dt_fim_p))) or (ie_consiste_data_p = 'N'))
and	a.ie_situacao not in ('3','5')
and	nvl(a.nr_seq_cliente,b.nr_seq_cliente) in (	select	a.nr_sequencia
							from	com_cliente a
							where	a.ie_classificacao in ('C','EX')
							and	((a.cd_cnpj = cd_cnpj_p) or (nvl(cd_cnpj_p,'X') = 'X')))
and	not exists(	select	1
			from	titulo_rec_negociado x
			where	x.nr_titulo = a.nr_titulo)
group by a.nr_titulo,a.vl_saldo_titulo,b.nr_nfe_imp,a.dt_emissao,a.dt_pagamento_previsto, substr(a.ds_observacao_titulo,1,255),a.nr_seq_cliente,b.nr_seq_cliente
order by 1;

begin
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
select	fin_lote_cobranca_seq.nextval
into	nr_seq_lote_w
from	dual;
-- lote completo
insert into fin_lote_cobranca(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	nr_seq_lote_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p);

open c01;
loop
fetch c01 into
	nr_titulo_w,
	nr_seq_cliente_w,
	vl_titulo_w,
	nr_nfe_imp_w,
	dt_emissao_w,
	dt_pagamento_previsto_w,
	ds_observacao_w;
exit when c01%notfound;
	begin	-- lote cliente
	vl_saldo_lote_w := vl_saldo_lote_w + vl_titulo_w;
	insert into fin_titulo_lote_clie (	nr_sequencia,
						nr_lote_cliente,
						nr_titulo,
						nr_seq_cliente,
						dt_atualizacao,
						nm_usuario,
						vl_titulo,
						nr_nfe_imp,
						dt_emissao,
						dt_pagamento_previsto,
						ds_observacao,
						nr_lote_ref)
					values(	fin_lote_cli_cobranca_seq.nextval,
						nr_seq_lote_w,
						nr_titulo_w,
						nr_seq_cliente_w,
						sysdate,
						nm_usuario_p,
						vl_titulo_w,
						nr_nfe_imp_w,
						dt_emissao_w,
						dt_pagamento_previsto_w,
						ds_observacao_w,
						nr_seq_lote_w);
	end;
end loop;
close c01;

update	fin_lote_cobranca
set	vl_saldo	= vl_saldo_lote_w,
	ds_titulo	= ds_titulo_p,
	nr_tipo_cob	= ie_tipo_p
where	nr_sequencia	= nr_seq_lote_w;

commit;

fin_gerar_perc_tit_cob(nr_seq_lote_w,nm_usuario_p);

end fin_gerar_lote_cobranca;
/