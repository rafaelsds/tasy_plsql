CREATE OR REPLACE PROCEDURE JUSTIFICATIVA_ENCERRAR_ETAPA(NR_SEQ_CRONOGRAMA_P NUMBER, NR_SEQ_CRON_ETAPA_P NUMBER, DS_JUSTIFICATIVA_P VARCHAR2, NM_USUARIO_P VARCHAR2) IS

  dt_atual_w          DATE   := SYSDATE;
  ds_justificativa_w  VARCHAR2(4000);
  nr_seq_projeto_w    proj_projeto.nr_sequencia%TYPE;
  nr_seq_cliente_w    proj_projeto.nr_seq_cliente%TYPE;

BEGIN

  BEGIN
  
    SELECT pj.nr_sequencia, pj.nr_seq_cliente INTO nr_seq_projeto_w, nr_seq_cliente_w FROM proj_projeto pj, proj_cronograma pc
    WHERE pj.nr_sequencia = pc.nr_seq_proj
    AND   pc.nr_sequencia = NR_SEQ_CRONOGRAMA_P;  
  
  END;


  ds_justificativa_w := obter_texto_dic_objeto(1084095, wheb_usuario_pck.get_nr_seq_idioma, 'NR_SEQ_CRON_ETAPA='||NR_SEQ_CRON_ETAPA_P||';DT_ATUAL='||dt_atual_w||';NM_USUARIO='||nm_usuario_p||';DS_JUSTIFICATIVA='|| DS_JUSTIFICATIVA_P) ;

  BEGIN
     insert into com_cliente_hist (nr_sequencia,
                        nr_seq_tipo, 		
                        nr_seq_cliente,  	
                        nm_usuario,  		
                        dt_historico, 		
                        dt_atualizacao, 	
                        dt_atualizacao_nrec,
                        nm_usuario_nrec,	
                        nr_seq_projeto, 	
                        ds_titulo, 			
                        dt_liberacao, 		
                        ds_historico) 		
                     values(com_cliente_hist_seq.nextval,	
                        8 ,
                        nr_seq_cliente_w,
                        nm_usuario_p,
                        sysdate, 
                        sysdate, 
                        sysdate, 
                        nm_usuario_p,
                        nr_seq_projeto_w,
                        obter_texto_dic_objeto(1084096, wheb_usuario_pck.get_nr_seq_idioma, null),
                        sysdate,
                       ds_justificativa_w);
  END;

END JUSTIFICATIVA_ENCERRAR_ETAPA;
/