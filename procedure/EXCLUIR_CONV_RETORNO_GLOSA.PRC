create or replace
procedure Excluir_Conv_Retorno_Glosa(
			nr_seq_ret_item_p		number) is 
			
begin

delete convenio_retorno_glosa
where nr_seq_ret_item = nr_seq_ret_item_p;
	
commit;

end Excluir_Conv_Retorno_Glosa;
/
