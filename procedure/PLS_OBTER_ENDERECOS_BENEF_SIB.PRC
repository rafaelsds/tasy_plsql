/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Retornar os endere�os do SIB do benefici�rio
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_obter_enderecos_benef_sib
		(	cd_pessoa_fisica_p		varchar2,
			nr_seq_contrato_p		number,
			ie_cal_digito_municip_p		varchar2,
			ie_forma_endereco_p		varchar2,
			ie_tipo_logradouro_p	out	varchar2,
			ds_endereco_p		out	varchar2,
			nr_endereco_p		out	varchar2,
			ds_complemento_p	out	varchar2,
			ds_bairro_p		out	varchar2,
			cd_municipio_ibge_p	out	varchar2,
			cd_municipio_resid_p	out	varchar2,
			sg_estado_p		out	varchar2,
			cd_cep_p		out	varchar2) is 
			
ds_logradouro_w			varchar2(250);
ds_numero_w			varchar2(5);
ds_complemento_w		varchar2(40);
ds_bairro_w			varchar2(80);
ds_municipio_w			varchar2(50);
uf_w				compl_pessoa_fisica.sg_estado%type;
cep_w				varchar2(15);
ie_tipo_logradouro_w		varchar2(10);
cd_tipo_logradouro_w		varchar2(10);
nr_seq_cns_tipo_log_w		number(10);
----------------------------------------------------------------------
qt_registros_w			number(10);
qt_endereco_residencial_w	number(10);
qt_endereco_comercial_w		number(10);

cd_municipio_ibge_resid_w	varchar2(10);
IE_TIPO_ENDERECO_w		varchar2(10);
cd_cgc_estipulante_w		varchar2(14);

begin

select	count(1)
into	qt_registros_w
from	pls_regra_endereco_sib
where	nr_seq_contrato	= nr_seq_contrato_p
and	rownum		<= 1;

if	(qt_registros_w	= 0) then
	select	count(1)
	into	qt_endereco_residencial_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tipo_complemento	= 1
	and	cd_cep is not null
	and	ds_endereco is not null
	and	sg_estado is not null
	and	rownum	<= 1;

	 /* endere�o residencial */
	if	(qt_endereco_residencial_w > 0) then
		ie_tipo_logradouro_w	:= '2';
		begin
		select	b.ds_endereco,
			to_char(b.nr_endereco),
			b.ds_complemento,
			b.ds_bairro,
			b.cd_municipio_ibge,
			b.sg_estado,
			b.cd_cep,
			b.cd_tipo_logradouro
		into	ds_logradouro_w,
			ds_numero_w,
			ds_complemento_w,
			ds_bairro_w,
			ds_municipio_w,
			uf_w,
			cep_w,
			cd_tipo_logradouro_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	b.ie_tipo_complemento	= 1
		and	rownum < 2;
		exception
		when others then
			ds_logradouro_w		:= '';
			ds_numero_w		:= '';
			ds_complemento_w	:= '';
			ds_bairro_w		:= '';
			ds_municipio_w		:= '';
			uf_w			:= '';
			cep_w			:= '00000000';
			cd_tipo_logradouro_w	:= '';
		end;
	else
		select	count(1)
		into	qt_endereco_comercial_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_tipo_complemento	= 2;

		/* endere�o comercial */
		if	(qt_endereco_comercial_w > 0) then
			ie_tipo_logradouro_w	:= '1'; 
			begin
			select	b.ds_endereco,
				to_char(b.nr_endereco),
				b.ds_complemento,
				b.ds_bairro,
				b.cd_municipio_ibge,
				b.sg_estado,
				b.cd_cep,
				b.cd_tipo_logradouro
			into	ds_logradouro_w,
				ds_numero_w,
				ds_complemento_w,
				ds_bairro_w,
				ds_municipio_w,
				uf_w,
				cep_w,
				cd_tipo_logradouro_w
			from	pessoa_fisica a,
				compl_pessoa_fisica b
			where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
			and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	b.ie_tipo_complemento	= 2
			and	rownum < 2;
			exception
			when others then
				ds_logradouro_w		:= '';
				ds_numero_w		:= '';
				ds_complemento_w	:= '';
				ds_bairro_w		:= '';
				ds_municipio_w		:= '';
				uf_w			:= '';
				cep_w			:= '00000000';
				cd_tipo_logradouro_w	:= '';
			end;
			
			cd_municipio_ibge_resid_w	:= substr(obter_compl_pf(cd_pessoa_fisica_p,1,'CDM'),1,10);
			
			if	(cd_municipio_ibge_resid_w is null) or
				(cd_municipio_ibge_resid_w = '0') then
				cd_municipio_ibge_resid_w	:= substr(ds_municipio_w,1,10);
			end if;
		end if;
	end if;
else
	select	max(ie_tipo_endereco)
	into	ie_tipo_endereco_w
	from	pls_regra_endereco_sib
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	rownum		<= 1;
	
	if	(ie_tipo_endereco_w	= 'RC') then
		ie_tipo_logradouro_w	:= '1'; 
		begin
		select	b.ds_endereco,
			to_char(b.nr_endereco),
			b.ds_complemento,
			b.ds_bairro,
			b.cd_municipio_ibge,
			b.sg_estado,
			b.cd_cep,
			b.cd_tipo_logradouro
		into	ds_logradouro_w,
			ds_numero_w,
			ds_complemento_w,
			ds_bairro_w,
			ds_municipio_w,
			uf_w,
			cep_w,
			cd_tipo_logradouro_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	b.ie_tipo_complemento	= 2
		and	rownum < 2;
		exception
		when others then
			ds_logradouro_w		:= '';
			ds_numero_w		:= '';
			ds_complemento_w	:= '';
			ds_bairro_w		:= '';
			ds_municipio_w		:= '';
			uf_w			:= '';
			cep_w			:= '00000000';
			cd_tipo_logradouro_w	:= '';
		end;
		
		cd_municipio_ibge_resid_w	:= substr(obter_compl_pf(cd_pessoa_fisica_p,1,'CDM'),1,10);
		
		if	(cd_municipio_ibge_resid_w is null) or
			(cd_municipio_ibge_resid_w = '0') then
			cd_municipio_ibge_resid_w	:= substr(ds_municipio_w,1,10);
		end if;
	elsif	(ie_tipo_endereco_w	= 'RE') then
		select	cd_cgc_estipulante
		into	cd_cgc_estipulante_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_p;
		
		ie_tipo_logradouro_w	:= '1'; 
		select	ds_endereco,
			to_char(nr_endereco),
			substr(ds_complemento,1,40),
			ds_bairro,
			cd_municipio_ibge,
			sg_estado,
			cd_cep,
			NR_SEQ_TIPO_LOGRADOURO
		into	ds_logradouro_w,
			ds_numero_w,
			ds_complemento_w,
			ds_bairro_w,
			ds_municipio_w,
			uf_w,
			cep_w,
			nr_seq_cns_tipo_log_w
		from	pessoa_juridica
		where	cd_cgc		= cd_cgc_estipulante_w;
		
		cd_municipio_ibge_resid_w	:= substr(obter_compl_pf(cd_pessoa_fisica_p,1,'CDM'),1,10);
		
		if	(cd_municipio_ibge_resid_w is null) or
			(cd_municipio_ibge_resid_w = '0') then
			cd_municipio_ibge_resid_w	:= substr(ds_municipio_w,1,10);
		end if;	
	end if;
end if;

if	(ie_cal_digito_municip_p = 'S') then
	if	(ds_municipio_w is not null) then
		ds_municipio_w	:= ds_municipio_w||calcula_digito('MODULO10',substr(ds_municipio_w,1,10));
	end if;
	
	if	(cd_municipio_ibge_resid_w is not null) then
		cd_municipio_ibge_resid_w	:= cd_municipio_ibge_resid_w||calcula_digito('MODULO10',cd_municipio_ibge_resid_w);
	end if;
		
end if;

/*aaschlote 09/12/2013 OS - 611774*/
if	(ie_forma_endereco_p = '2') then
	if	(nr_seq_cns_tipo_log_w is not null) then
		select	cd_tipo_logradouro
		into	cd_tipo_logradouro_w
		from	cns_tipo_logradouro
		where	nr_sequencia	= nr_seq_cns_tipo_log_w;
	end if;
	
	if	(cd_tipo_logradouro_w is not null) then
		ds_logradouro_w	:= substr(Sus_Obter_Desc_TipoLog(cd_tipo_logradouro_w),1,255) || ' ' || ds_logradouro_w;
	end if;
end if;

ds_endereco_p		:= substr(ds_logradouro_w,1,50);
nr_endereco_p		:= substr(ds_numero_w,1,5);
ds_complemento_p	:= substr(ds_complemento_w,1,15);
ds_bairro_p		:= substr(ds_bairro_w,1,20);
cd_municipio_ibge_p	:= substr(ds_municipio_w,1,10);
cd_municipio_resid_p	:= substr(cd_municipio_ibge_resid_w,1,10);
sg_estado_p		:= substr(uf_w,1,5);
cd_cep_p		:= lpad(cep_w,8,'0');
ie_tipo_logradouro_p	:= ie_tipo_logradouro_w;

end pls_obter_enderecos_benef_sib;
/
