create or replace
procedure gerar_conta_etapa_completo ( 
					nr_interno_conta_p	number,
					nm_usuario_p		varchar2,
					nr_seq_etapa_p		number,
					nr_motivo_devol_p	number,
					dt_etapa_p		date,
					cd_pessoa_fisica_p	varchar2,
					cd_pessoa_exec_p	varchar2,
					cd_setor_atendimento_p	number,
					ds_observacao_p		varchar2,
					dt_fim_etapa_p		date,
					ie_etapa_critica_p	varchar2) is

nr_sequencia_w		number(10);
nr_motivo_devol_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_pessoa_exec_w	varchar2(10);
cd_setor_atendimento_w	conta_paciente_etapa.cd_setor_atendimento%type;
dt_fim_etapa_w		date;

begin

select	conta_paciente_etapa_seq.nextval
into	nr_sequencia_w
from 	dual;

nr_motivo_devol_w := nr_motivo_devol_p;
if	(nr_motivo_devol_w = 0) then
	nr_motivo_devol_w := null;
end if;	

cd_setor_atendimento_w := cd_setor_atendimento_p;
if	(cd_setor_atendimento_w = 0) then
	cd_setor_atendimento_w := null;
end if;	

dt_fim_etapa_w := dt_fim_etapa_p;
if	(dt_fim_etapa_w = to_date('31/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) then
	dt_fim_etapa_w := null;
end if;

cd_pessoa_fisica_w := cd_pessoa_fisica_p;
if	(cd_pessoa_fisica_w = '') or
	(cd_pessoa_fisica_w is null) then
	select 	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from 	usuario
	where 	nm_usuario = nm_usuario_p;
end if;

cd_pessoa_exec_w := cd_pessoa_exec_p;
if	(cd_pessoa_exec_w = '') then
	cd_pessoa_exec_w := null;
end if;

insert into conta_paciente_etapa (
			nr_sequencia,
			nr_interno_conta,
			dt_atualizacao,
			nm_usuario,
			dt_etapa,
			nr_seq_etapa,
			cd_setor_atendimento,
			cd_pessoa_fisica,
			nr_seq_motivo_dev,
			ds_observacao,
			nr_lote_barras,
			cd_pessoa_exec,
			dt_fim_etapa,
			ie_etapa_critica)
		values	(nr_sequencia_w,
			nr_interno_conta_p,
			sysdate,
			nm_usuario_p,
			dt_etapa_p,
			nr_seq_etapa_p,
			cd_setor_atendimento_w,
			cd_pessoa_fisica_w,
			nr_motivo_devol_w,
			substr(ds_observacao_p,1,2000),
			null,
			cd_pessoa_exec_w,
			dt_fim_etapa_w,
			ie_etapa_critica_p);

atualiza_final_etapa_conta(nr_sequencia_w, nm_usuario_p);

commit;

end gerar_conta_etapa_completo;
/
