create or replace 
procedure Gravar_Log_Impressao(	cd_classif_relat_p	varchar2,
				cd_relatorio_p		varchar2,
				ds_parametros_p		varchar2,
				nm_usuario_p		varchar2,
				ie_tipo_log_p		number,
				nr_sequencia_p	out	number,
				ds_acesso_p		varchar2 default null) as
begin

select	log_impressao_seq.NextVal
into	nr_sequencia_p
from 	dual;

insert into log_impressao (	nr_sequencia,	
				cd_classif_relat,
				cd_relatorio,	
				dt_atualizacao,
				nm_usuario,	
				ds_parametros,
				ie_tipo_log,
				ds_acesso)
		values	  (	nr_sequencia_p,	
				cd_classif_relat_p,
				cd_relatorio_p,	
				sysdate,
				nm_usuario_p,	
				substr(ds_parametros_p,1,2000),
				ie_tipo_log_p,
				ds_acesso_p);

commit;

end Gravar_Log_Impressao;
/
