create or replace
procedure pls_lib_lote_portal_web	
			(nr_seq_lote_p	number,
			 nm_usuario_p 	varchar2)  is
begin
if	(nr_seq_lote_p is not null) then
	update	pls_lote_mensalidade
	set	ie_visualizar_portal 	= 'S',
		dt_atualizacao 	= sysdate,    
    		nm_usuario = nm_usuario_p		
	where	nr_sequencia	= nr_seq_lote_p;
end if;

commit;

end pls_lib_lote_portal_web;
/

