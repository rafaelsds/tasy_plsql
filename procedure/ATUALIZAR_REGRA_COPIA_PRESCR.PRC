create or replace
procedure Atualizar_regra_copia_prescr(	nr_prescricao_p				number,
										nr_prescricao_antiga		number,
										nr_seq_regra_p				number,
										cd_medico_p					number,
										nr_seq_transcricao_p		number,
										cd_perfil_p					number,
										cd_estabelecimento_p		number,
										nr_nova_prescricao_p	out	number,
										nm_usuario_p				varchar2,
										cd_medic_posicionar_p	out	number) is 

			
dt_prescricao_w			date;
dt_primeiro_horario_w	varchar2(5);
nr_atendimento_w		number(10);
nr_nova_prescricao_w	number(14);

begin

select	dt_prescricao,
		substr(to_char(dt_primeiro_horario,'dd/mm/yyyy hh24:mi:ss'),12,5),
		nr_atendimento
into	dt_prescricao_w,
		dt_primeiro_horario_w,
		nr_atendimento_w
from 	prescr_medica 
where 	nr_prescricao = nr_prescricao_antiga;

delete 	from prescr_medica 
where	nr_prescricao = nr_prescricao_p;

rep_copia_prescricao_regra(nr_prescricao_antiga, nr_atendimento_w, nr_seq_regra_p, nm_usuario_p, cd_medico_p, dt_prescricao_w, 'S', '', dt_primeiro_horario_w, null, cd_perfil_p, 0, '', '', '', dt_prescricao_w, null, nr_seq_transcricao_p, nr_nova_prescricao_w, 8030, cd_medic_posicionar_p);

gerar_dados_copia_rep(nr_prescricao_antiga, cd_perfil_p, cd_estabelecimento_p, nm_usuario_p);

nr_nova_prescricao_p := nr_nova_prescricao_w;

commit;

end atualizar_regra_copia_prescr;
/