create or replace 
procedure autorizacao_pedido_exame_ext(	nr_seq_pedido_p  number,
										ie_tiss_tipo_guia_p  number,
										ie_honorario_p  varchar2) is

cd_autorizacao_w	varchar2(255) := '';
ie_guia_w			varchar2(255) := '';

begin

tiss_obter_guia('4',null,cd_autorizacao_w,'N',null,null,null,null,null,ie_guia_w,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

update	pedido_exame_externo 
set  	cd_autorizacao  = cd_autorizacao_w
where	nr_sequencia 	= nr_seq_pedido_p;


commit;

end	autorizacao_pedido_exame_ext;
/