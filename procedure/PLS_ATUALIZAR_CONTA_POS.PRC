create or replace
procedure pls_atualizar_conta_pos
			(	nr_seq_analise_p	number,
				nr_seq_conta_p		number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar as informa��es atuais da conta nas tabelas "W" e tamb�m atualizar as contas com as 
informa��es das tabelas "W" ao dar ok, passar "G" para gerar as informa��es e "A" para atualizar
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

QUANDO INCLUIR QUALQUER CAMPO NOS UPDATES, DEVE SER TRATADO O MESMO NO HISTORICO

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_observacao_w			varchar2(4000);
ds_indicacao_clinica_w		varchar2(500);
cd_senha_externa_w		varchar2(30);
nr_crm_exec_w			varchar2(20);
cd_medico_executor_w		varchar2(20);
cd_medico_solicitante_w		varchar2(10);
ie_tipo_guia_w			varchar2(2);
ie_carater_internacao_w		varchar2(1);
ie_regime_internacao_w		varchar2(1);
ie_indicacao_acidente_w		varchar2(1);
ie_tipo_doenca_w		varchar2(1);
ie_transtorno_w			varchar2(1);
ie_classificacao_w		varchar2(1);
nr_seq_segurado_w		number(10);
nr_seq_prestador_exec_w		number(10);
nr_seq_prestador_solic_w	number(10);
nr_seq_saida_int_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_clinica_w		number(10);
nr_seq_tipo_acomodacao_w	number(10);
nr_seq_saida_spsadt_w		number(10);
nr_seq_tipo_atendimento_w	number(10);
nr_seq_saida_consulta_w		number(10);
nr_seq_cbo_saude_w		number(10);
nr_seq_cbo_saude_solic_w	number(10);
nr_seq_grau_partic_w		number(10);
nr_seq_tipo_conta_w		number(10);
qt_registro_w			number(10);
nr_seq_item_w			number(10);
nr_seq_conta_item_w		number(10);
ie_obito_mulher_w		number(2);
ie_tipo_consulta_w		number(1);
dt_entrada_w			date;
dt_alta_w			date;
dt_atendimento_w		date;
nr_seq_prestador_exec_orig_w	Number(10);
ds_historico_w			varchar2(4000);
/* Dados da conta anterior */
nr_seq_segurado_ant_w		number(10);
nr_seq_prestador_exec_ant_w	number(10);
nr_seq_prestador_solic_ant_w	number(10);
ie_carater_internacao_ant_w	varchar2(3);
nr_seq_saida_int_ant_w		number(10);
ie_tipo_guia_ant_w		varchar2(2);
nr_seq_clinica_ant_w		number(10);
ie_regime_internacao_ant_w	varchar2(1);
ie_indicacao_acidente_ant_w	varchar2(1);
nr_seq_tipo_acomodacao_ant_w	number(10);
nr_seq_saida_spsadt_ant_w	number(10);
nr_seq_tipo_atendimento_ant_w	number(10);
ie_tipo_doenca_ant_w		varchar2(1);
nr_seq_saida_consulta_ant_w	number(10);
ie_tipo_consulta_ant_w		varchar2(3);
dt_entrada_ant_w		date;
dt_alta_ant_w			date;
dt_atendimento_ant_w		date;
cd_senha_externa_ant_w		varchar2(30);
nr_crm_exec_ant_w		varchar2(20);
cd_medico_solicitante_ant_w	varchar2(10);
cd_medico_executor_ant_w	varchar2(10);
ds_indicacao_clinica_ant_w	varchar2(500);
nr_seq_cbo_saude_ant_w		number(10);
nr_seq_cbo_saude_solic_ant_w	number(10);
nr_seq_grau_partic_ant_w	number(10);
nr_seq_tipo_conta_ant_w		number(10);
ie_tipo_acomodacao_ptu_w	Varchar2(2);
ie_tipo_acomodacao_ptu_ww	Varchar2(2);
nr_seq_prestador_w		pls_conta_pos_cabecalho.nr_seq_prestador%type;
ie_recem_nascido_w		pls_conta_pos_cabecalho.ie_recem_nascido%type;
ie_recem_nascido_ant_w		pls_conta_pos_cabecalho.ie_recem_nascido%type;
ie_preco_plano_w		pls_plano.ie_preco%type;
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;
nr_seq_plano_w			pls_segurado.nr_seq_plano%type;
cd_doenca_w			pls_diag_conta_obito_pos.cd_doenca%type;
ds_diagnostico_w		pls_diagnostico_conta_pos.ds_diagnostico%type;
ie_classific_diag_w		pls_diagnostico_conta_pos.ie_classificacao%type;
qt_reg_w			pls_integer;
ie_indicador_dorn_w		pls_diag_conta_obito_pos.ie_indicador_dorn%type;
nr_declaracao_obito_w		pls_diag_conta_obito_pos.nr_declaracao_obito%type;
nr_decl_nasc_vivo_w		pls_diagnos_nasc_viv_pos.nr_decl_nasc_vivo%type;
cd_doenca_ant_w			pls_diag_conta_obito_pos.cd_doenca%type;
ds_diagnostico_ant_w		pls_diagnostico_conta_pos.ds_diagnostico%type;
ie_classific_diag_ant_w		pls_diagnostico_conta_pos.ie_classificacao%type;
ie_indicador_dorn_ant_w		pls_diag_conta_obito_pos.ie_indicador_dorn%type;
nr_declaracao_obito_ant_w	pls_diag_conta_obito_pos.nr_declaracao_obito%type;
nr_decl_nasc_vivo_ant_w		pls_diagnos_nasc_viv_pos.nr_decl_nasc_vivo%type;
ie_novo_pos_estab_w		pls_visible_false.ie_novo_pos_estab%type;

begin

select	nvl(max(ie_novo_pos_estab),'N')
into	ie_novo_pos_estab_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_opcao_p = 'G') then
	delete	from w_pls_conta_alteracao
	where	nr_seq_conta	= nr_seq_conta_p
	and	nm_usuario	= nm_usuario_p;
	
	if	(ie_novo_pos_estab_w = 'S') then
		delete	w_pls_diagnostico_conta
		where	nr_seq_conta	= nr_seq_conta_p
		and	nm_usuario	= nm_usuario_p;
		
		delete	w_pls_diagnost_conta_obito
		where	nr_seq_conta	= nr_seq_conta_p
		and	nm_usuario	= nm_usuario_p;
		
		delete	w_pls_diagnostico_nasc_viv
		where	nr_seq_conta	= nr_seq_conta_p
		and	nm_usuario	= nm_usuario_p;
	end if;
	
	select	nr_seq_conta,
		nr_seq_segurado,
		nr_seq_tipo_atendimento,
		dt_alta,
		nr_seq_saida_spsadt,
		dt_entrada,
		ie_tipo_consulta,
		dt_atendimento,
		cd_medico_solicitante,
		cd_medico_executor,
		ie_carater_internacao,
		ie_indicacao_acidente,
		nr_seq_tipo_acomodacao,
		nr_seq_clinica,
		ie_regime_internacao,
		ie_recem_nascido,
		nr_seq_saida_consulta,  
		nr_seq_saida_int,       
		ie_tipo_guia,              
		nr_seq_prestador,       
		nr_seq_prestador_exec,
		ie_tipo_doenca,
		cd_senha_externa,
		nr_seq_grau_partic,
		nr_seq_tipo_conta
	into	nr_seq_conta_w,
		nr_seq_segurado_w,
		nr_seq_tipo_atendimento_w,
		dt_alta_w,
		nr_seq_saida_spsadt_w,
		dt_entrada_w,
		ie_tipo_consulta_w,
		dt_atendimento_w,
		cd_medico_solicitante_w,
		cd_medico_executor_w,
		ie_carater_internacao_w,
		ie_indicacao_acidente_w,
		nr_seq_tipo_acomodacao_w,
		nr_seq_clinica_w,
		ie_regime_internacao_w,
		ie_recem_nascido_w,
		nr_seq_saida_consulta_w,  
		nr_seq_saida_int_w,       
		ie_tipo_guia_w,             
		nr_seq_prestador_solic_w,       
		nr_seq_prestador_exec_w,
		ie_tipo_doenca_w,
		cd_senha_externa_w,
		nr_seq_grau_partic_w,
		nr_seq_tipo_conta_w
	from	pls_conta_pos_cabecalho		a
	where	a.nr_seq_conta   = nr_seq_conta_p;

	insert into w_pls_conta_alteracao
		(nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_segurado,
		nr_seq_prestador_exec,
		nr_seq_prestador,
		ie_carater_internacao,
		nr_seq_saida_int,
		nr_seq_conta,
		ie_tipo_guia,
		nr_seq_clinica,
		ie_regime_internacao,
		ie_indicacao_acidente,
		nr_seq_tipo_acomodacao,
		nr_seq_saida_spsadt,
		nr_seq_tipo_atendimento,
		ie_tipo_doenca,
		nr_seq_saida_consulta,
		dt_entrada,
		dt_alta,
		dt_atendimento,
		cd_senha_externa,
		cd_medico_solicitante,
		cd_medico_executor,
		nr_seq_grau_partic,
		ie_tipo_consulta,
		ie_recem_nascido,
		nr_seq_tipo_conta)
	values	(w_pls_conta_alteracao_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_segurado_w,
		nr_seq_prestador_exec_w,
		nr_seq_prestador_solic_w,
		ie_carater_internacao_w,
		nr_seq_saida_int_w,
		nr_seq_conta_w,
		ie_tipo_guia_w,
		nr_seq_clinica_w,
		ie_regime_internacao_w,
		ie_indicacao_acidente_w,
		nr_seq_tipo_acomodacao_w,
		nr_seq_saida_spsadt_w,
		nr_seq_tipo_atendimento_w,
		ie_tipo_doenca_w,
		nr_seq_saida_consulta_w,
		dt_entrada_w,
		dt_alta_w,
		dt_atendimento_w,
		cd_senha_externa_w,
		cd_medico_solicitante_w,
		cd_medico_executor_w,
		nr_seq_grau_partic_w,
		ie_tipo_consulta_w,
		ie_recem_nascido_w,
		nr_seq_tipo_conta_w);
	
	if	(ie_novo_pos_estab_w = 'S') then
		select	max(cd_doenca),
			max(ds_diagnostico),
			max(ie_classificacao),
			count(1)
		into	cd_doenca_w,
			ds_diagnostico_w,
			ie_classific_diag_w,
			qt_reg_w
		from	pls_diagnostico_conta_pos
		where	nr_seq_conta = nr_seq_conta_p;
		
		if	(qt_reg_w > 0) then
			insert	into	w_pls_diagnostico_conta
				(	cd_doenca, ds_diagnostico, dt_atualizacao,
					dt_atualizacao_nrec, ie_classificacao, nm_usuario,
					nm_usuario_nrec, nr_seq_conta, nr_sequencia)
			values	(	cd_doenca_w, ds_diagnostico_w, sysdate,
					sysdate, ie_classific_diag_w, nm_usuario_p,
					nm_usuario_p, nr_seq_conta_p, w_pls_diagnostico_conta_seq.nextval);
		end if;
		
		select	max(cd_doenca),
			max(ie_indicador_dorn),
			max(nr_declaracao_obito),
			count(1)
		into	cd_doenca_w,
			ie_indicador_dorn_w,
			nr_declaracao_obito_w,
			qt_reg_w
		from	pls_diag_conta_obito_pos
		where	nr_seq_conta = nr_seq_conta_p;
		
		if	(qt_reg_w > 0) then
			insert	into	w_pls_diagnost_conta_obito
				(	cd_doenca, dt_atualizacao, dt_atualizacao_nrec,
					ie_indicador_dorn, nm_usuario, nm_usuario_nrec,
					nr_declaracao_obito, nr_seq_conta, nr_sequencia)
			values	(	cd_doenca_w, sysdate, sysdate,
					ie_indicador_dorn_w, nm_usuario_p, nm_usuario_p,
					nr_declaracao_obito_w, nr_seq_conta_p, w_pls_diagnost_conta_obito_seq.nextval);
			commit;
		end if;
		
		select	max(nr_decl_nasc_vivo),
			count(1)
		into	nr_decl_nasc_vivo_w,
			qt_reg_w
		from	pls_diagnos_nasc_viv_pos
		where	nr_seq_conta = nr_seq_conta_p;
		
		if	(qt_reg_w > 0) then
			insert	into	w_pls_diagnostico_nasc_viv
				(	dt_atualizacao, dt_atualizacao_nrec, nm_usuario,
					nm_usuario_nrec, nr_decl_nasc_vivo, nr_seq_conta,
					nr_sequencia)
			values	(	sysdate, sysdate, nm_usuario_p,
					nm_usuario_p, nr_decl_nasc_vivo_w, nr_seq_conta_p,
					w_pls_diagnostico_nasc_viv_seq.nextval);
			commit;
		end if;
	end if;
	
elsif	(ie_opcao_p = 'A') then

	select	nr_seq_segurado,
		nr_seq_tipo_atendimento,
		dt_alta,
		nr_seq_saida_spsadt,
		dt_entrada,
		ie_tipo_consulta,
		dt_atendimento,
		cd_medico_solicitante,
		cd_medico_executor,
		ie_carater_internacao,
		ie_indicacao_acidente,
		nr_seq_tipo_acomodacao,
		nr_seq_clinica,
		ie_regime_internacao,
		ie_recem_nascido,
		nr_seq_saida_consulta,  
		nr_seq_saida_int,       
		ie_tipo_guia,              
		nr_seq_prestador,       
		nr_seq_prestador_exec,
		ie_tipo_doenca,
		cd_senha_externa,
		nr_seq_grau_partic,
		nr_seq_tipo_conta
	into	nr_seq_segurado_ant_w,
		nr_seq_tipo_atendimento_ant_w,
		dt_alta_ant_w,
		nr_seq_saida_spsadt_ant_w,
		dt_entrada_ant_w,
		ie_tipo_consulta_ant_w,
		dt_atendimento_ant_w,
		cd_medico_solicitante_ant_w,
		cd_medico_executor_ant_w,
		ie_carater_internacao_ant_w,
		ie_indicacao_acidente_ant_w,
		nr_seq_tipo_acomodacao_ant_w,
		nr_seq_clinica_ant_w,
		ie_regime_internacao_ant_w,
		ie_recem_nascido_ant_w,
		nr_seq_saida_consulta_ant_w,  
		nr_seq_saida_int_ant_w,       
		ie_tipo_guia_ant_w,             
		nr_seq_prestador_solic_ant_w,       
		nr_seq_prestador_exec_ant_w,
		ie_tipo_doenca_ant_w,
		cd_senha_externa_ant_w,
		nr_seq_grau_partic_ant_w,
		nr_seq_tipo_conta_ant_w
	from	pls_conta_pos_cabecalho		a
	where	a.nr_seq_conta   = nr_seq_conta_p;
	
	nr_seq_prestador_exec_orig_w := nr_seq_prestador_exec_ant_w;
	
	select	nr_seq_segurado,
		nr_seq_prestador_exec,
		nr_seq_prestador,
		ie_carater_internacao,
		nr_seq_saida_int,
		nr_seq_conta,
		ie_tipo_guia,
		nr_seq_clinica,
		ie_regime_internacao,
		ie_indicacao_acidente,
		nr_seq_tipo_acomodacao,
		nr_seq_saida_spsadt,
		nr_seq_tipo_atendimento,
		ie_tipo_doenca,
		nr_seq_saida_consulta,
		dt_entrada,
		dt_alta,
		dt_atendimento,
		cd_senha_externa,
		cd_medico_solicitante,
		cd_medico_executor,
		nr_seq_grau_partic,
		ie_tipo_consulta,
		ie_recem_nascido,
		nr_seq_tipo_conta
	into	nr_seq_segurado_w,
		nr_seq_prestador_exec_w,
		nr_seq_prestador_solic_w,
		ie_carater_internacao_w,
		nr_seq_saida_int_w,
		nr_seq_conta_w,
		ie_tipo_guia_w,
		nr_seq_clinica_w,
		ie_regime_internacao_w,
		ie_indicacao_acidente_w,
		nr_seq_tipo_acomodacao_w,
		nr_seq_saida_spsadt_w,
		nr_seq_tipo_atendimento_w,
		ie_tipo_doenca_w,
		nr_seq_saida_consulta_w,
		dt_entrada_w,
		dt_alta_w,
		dt_atendimento_w,
		cd_senha_externa_w,
		cd_medico_solicitante_w,
		cd_medico_executor_w,
		nr_seq_grau_partic_w,
		ie_tipo_consulta_w,
		ie_recem_nascido_w,
		nr_seq_tipo_conta_w
	from	w_pls_conta_alteracao	a
	where	a.nr_seq_conta   = nr_seq_conta_p
	and	nm_usuario	 = nm_usuario_p;
	
	update	pls_conta_pos_cabecalho
	set	nr_seq_segurado			= nr_seq_segurado_w,
		nr_seq_tipo_atendimento		= nr_seq_tipo_atendimento_w,
		dt_alta				= dt_alta_w,
		nr_seq_saida_spsadt		= nr_seq_saida_spsadt_w,
		dt_entrada			= dt_entrada_w,
		ie_tipo_consulta		= ie_tipo_consulta_w,
		dt_atendimento			= dt_atendimento_w,
		cd_medico_solicitante		= cd_medico_solicitante_w,
		cd_medico_executor		= cd_medico_executor_w,
		ie_carater_internacao		= ie_carater_internacao_w,
		ie_indicacao_acidente		= ie_indicacao_acidente_w,
		nr_seq_tipo_acomodacao		= nr_seq_tipo_acomodacao_w,
		nr_seq_clinica			= nr_seq_clinica_w,
		ie_regime_internacao		= ie_regime_internacao_w,
		ie_recem_nascido		= ie_recem_nascido_w,
		nr_seq_saida_consulta		= nr_seq_saida_consulta_w,  
		nr_seq_saida_int		= nr_seq_saida_int_w,       
		ie_tipo_guia			= ie_tipo_guia_w,              
		nr_seq_prestador		= nr_seq_prestador_solic_w,       
		nr_seq_prestador_exec		= nr_seq_prestador_exec_w,
		ie_tipo_doenca			= ie_tipo_doenca_w,
		cd_senha_externa		= cd_senha_externa_w,
		nr_seq_grau_partic		= nr_seq_grau_partic_w,
		nr_seq_tipo_conta		= nr_seq_tipo_conta_w
	where	nr_seq_conta			= nr_seq_conta_p;
	
	ds_historico_w	:= null;
	
	if	(ie_novo_pos_estab_w = 'S') then
		select	max(cd_doenca),
			max(ds_diagnostico),
			max(ie_classificacao),
			count(1)
		into	cd_doenca_ant_w,
			ds_diagnostico_ant_w,
			ie_classific_diag_ant_w,
			qt_reg_w
		from	pls_diagnostico_conta_pos
		where	nr_seq_conta = nr_seq_conta_p;
		
		if	(qt_reg_w > 0) then
			select	max(cd_doenca),
				max(ds_diagnostico),
				max(ie_classificacao)
			into	cd_doenca_w,
				ds_diagnostico_w,
				ie_classific_diag_w
			from	w_pls_diagnostico_conta
			where	nr_seq_conta = nr_seq_conta_p;
			
			update	pls_diagnostico_conta_pos
			set	cd_doenca = cd_doenca_w,
				ds_diagnostico = ds_diagnostico_w,
				ie_classificacao = ie_classific_diag_w,
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p
			where	nr_seq_conta = nr_seq_conta_p;
			
			if	(nvl(cd_doenca_w,'X') <> nvl(cd_doenca_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado CID do diagn�stico de ' || cd_doenca_ant_w || ' para ' || cd_doenca_w;
			end if;
			
			if	(nvl(ds_diagnostico_w,'X') <> nvl(ds_diagnostico_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado descri��o do diagn�stico de ' || ds_diagnostico_ant_w || ' para ' || ds_diagnostico_w;
			end if;
			
			if	(nvl(ie_classific_diag_w,'X') <> nvl(ie_classific_diag_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado classifica��o do diagn�stico de ' || ie_classific_diag_ant_w || ' para ' || ie_classific_diag_w;
			end if;
		end if;
		
		select	max(cd_doenca),
			max(ie_indicador_dorn),
			max(nr_declaracao_obito),
			count(1)
		into	cd_doenca_ant_w,
			ie_indicador_dorn_ant_w,
			nr_declaracao_obito_ant_w,
			qt_reg_w
		from	pls_diag_conta_obito_pos
		where	nr_seq_conta = nr_seq_conta_p;
		
		if	(qt_reg_w > 0) then
			select	max(cd_doenca),
				max(ie_indicador_dorn),
				max(nr_declaracao_obito)
			into	cd_doenca_w,
				ie_indicador_dorn_w,
				nr_declaracao_obito_w
			from	w_pls_diagnost_conta_obito
			where	nr_seq_conta = nr_seq_conta_p;
			
			update	pls_diag_conta_obito_pos
			set	cd_doenca = cd_doenca_w,
				ie_indicador_dorn = ie_indicador_dorn_w,
				nr_declaracao_obito = nr_declaracao_obito_w,
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p
			where	nr_seq_conta = nr_seq_conta_p;
			
			if	(nvl(cd_doenca_w,'X') <> nvl(cd_doenca_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado CID do �bito de ' || cd_doenca_ant_w || ' para ' || cd_doenca_w;
			end if;
			
			if	(nvl(ie_indicador_dorn_w,'X') <> nvl(ie_indicador_dorn_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado indicador de �bito de RN de ' || ie_indicador_dorn_ant_w || ' para ' || ie_indicador_dorn_w;
			end if;
			
			if	(nvl(nr_declaracao_obito_w,'X') <> nvl(nr_declaracao_obito_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado declara��o de �bito de ' || nr_declaracao_obito_ant_w || ' para ' || nr_declaracao_obito_w;
			end if;
		end if;
		
		select	max(nr_decl_nasc_vivo),
			count(1)
		into	nr_decl_nasc_vivo_ant_w,
			qt_reg_w
		from	pls_diagnos_nasc_viv_pos
		where	nr_seq_conta = nr_seq_conta_p;
		
		if	(qt_reg_w > 0) then
			select	max(nr_decl_nasc_vivo)
			into	nr_decl_nasc_vivo_w
			from	w_pls_diagnostico_nasc_viv
			where	nr_seq_conta = nr_seq_conta_p;
			
			update	pls_diagnos_nasc_viv_pos
			set	nr_decl_nasc_vivo = nr_decl_nasc_vivo_w,
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p
			where	nr_seq_conta = nr_seq_conta_p;
			
			if	(nvl(nr_decl_nasc_vivo_w,'X') <> nvl(nr_decl_nasc_vivo_ant_w,'X')) then
				ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado declara��o de nascido-vivo de ' || nr_decl_nasc_vivo_ant_w || ' para ' || nr_decl_nasc_vivo_w;
			end if;
		end if;
	end if;
	
	/* Gravar hist�rico na an�lise da altera��o */
	if	(nvl(nr_seq_segurado_w,0) <> nvl(nr_seq_segurado_ant_w,0)) then
		select	max(nr_seq_plano),
			max(ie_tipo_segurado)
		into	nr_seq_plano_w,
			ie_tipo_segurado_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		if	( nr_seq_plano_w is not null ) then
			select	max(ie_preco)
			into	ie_preco_plano_w
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano_w;
		end if;
		
		if	(not((ie_preco_plano_w in ('2','3')) or
			(ie_tipo_segurado_w in ('I','C','T','H')))) then
			wheb_mensagem_pck.exibir_mensagem_abort(247098);
		end if;
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado benefici�rio de ' || nr_seq_segurado_ant_w || ' para ' || nr_seq_segurado_w;
		
		--aaschlote 16/02/2016 OS 996873
		update	pls_analise_conta
		set	nr_seq_segurado	= nr_seq_segurado_w
		where	nr_sequencia	= nr_seq_analise_p;
		
	end if;
	if	(nvl(nr_seq_prestador_exec_w,0) <> nvl(nr_seq_prestador_exec_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado prestador exec de ' || nr_seq_prestador_exec_ant_w || ' para ' || nr_seq_prestador_exec_w;
	end if;
	if	(nvl(nr_seq_prestador_solic_w,0) <> nvl(nr_seq_prestador_solic_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado prestador solic de ' || nr_seq_prestador_solic_ant_w || ' para ' || nr_seq_prestador_solic_w;
	end if;
	if	(nvl(ie_carater_internacao_w,0) <> nvl(ie_carater_internacao_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado carater interna��o de ' || ie_carater_internacao_ant_w || ' para ' || ie_carater_internacao_w;
	end if;
	if	(nvl(nr_seq_saida_int_w,0) <> nvl(nr_seq_saida_int_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo sa�da interna��o de ' || nr_seq_saida_int_ant_w || ' para ' || nr_seq_saida_int_w;
	end if;
	
	if	(nvl(ie_tipo_guia_w,0) <> nvl(ie_tipo_guia_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo de guia de ' || ie_tipo_guia_ant_w || ' para ' || ie_tipo_guia_w;
	end if;
	if	(nvl(nr_seq_clinica_w,0) <> nvl(nr_seq_clinica_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado clinica de ' || nr_seq_clinica_ant_w || ' para ' || nr_seq_clinica_w;
	end if;
	if	(nvl(ie_regime_internacao_w,0) <> nvl(ie_regime_internacao_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado regime interna��o de ' || ie_regime_internacao_ant_w || ' para ' || ie_regime_internacao_w;
	end if;
	if	(nvl(ie_indicacao_acidente_w,0) <> nvl(ie_indicacao_acidente_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado indica��o acid. de ' || ie_indicacao_acidente_ant_w || ' para ' || ie_indicacao_acidente_w;
	end if;
	if	(nvl(nr_seq_tipo_acomodacao_w,0) <> nvl(nr_seq_tipo_acomodacao_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo acomod. de ' || nr_seq_tipo_acomodacao_ant_w || ' para ' || nr_seq_tipo_acomodacao_w;
	end if;
	if	(nvl(nr_seq_saida_spsadt_w,0) <> nvl(nr_seq_saida_spsadt_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo sa�da SADT de ' || nr_seq_saida_spsadt_ant_w || ' para ' || nr_seq_saida_spsadt_w;
	end if;
	if	(nvl(nr_seq_tipo_atendimento_w,0) <> nvl(nr_seq_tipo_atendimento_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo atend. de ' || nr_seq_tipo_atendimento_ant_w || ' para ' || nr_seq_tipo_atendimento_w;
	end if;
	if	(nvl(ie_tipo_doenca_w,0) <> nvl(ie_tipo_doenca_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo doen�a de ' || ie_tipo_doenca_ant_w || ' para ' || ie_tipo_doenca_w;
	end if;
	if	(nvl(nr_seq_saida_consulta_w,0) <> nvl(nr_seq_saida_consulta_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo sa�da cons. de ' || nr_seq_saida_consulta_ant_w || ' para ' || nr_seq_saida_consulta_w;
	end if;
	if	(nvl(ie_tipo_consulta_w,0) <> nvl(ie_tipo_consulta_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo consulta de ' || ie_tipo_consulta_ant_w || ' para ' || ie_tipo_consulta_w;
	end if;
	if	(nvl(dt_entrada_w,sysdate) <> nvl(dt_entrada_ant_w,sysdate)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado data de entrada de ' || dt_entrada_ant_w || ' para ' || dt_entrada_w;
	end if;
	if	(nvl(dt_alta_w,sysdate) <> nvl(dt_alta_ant_w,sysdate)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado data de alta de ' || dt_alta_ant_w || ' para ' || dt_alta_w;
	end if;
	if	(nvl(dt_atendimento_w,sysdate) <> nvl(dt_atendimento_ant_w,sysdate)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado data atendimento de ' || dt_atendimento_ant_w || ' para ' || dt_atendimento_w;
	end if;
	if	(nvl(cd_senha_externa_w,0) <> nvl(cd_senha_externa_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado senha externa de ' || cd_senha_externa_ant_w || ' para ' || cd_senha_externa_w;
	end if;
	if	(nvl(cd_medico_solicitante_w,0) <> nvl(cd_medico_solicitante_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado medico solic. de ' || cd_medico_solicitante_ant_w || ' para ' || cd_medico_solicitante_w;
	end if;
	if	(nvl(cd_medico_executor_w,0) <> nvl(cd_medico_executor_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado medico exec. de ' || cd_medico_executor_ant_w || ' para ' || cd_medico_executor_w;
	end if;
	if	(nvl(nr_seq_grau_partic_w,0) <> nvl(nr_seq_grau_partic_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado grau de partic. de ' || nr_seq_grau_partic_ant_w || ' para ' || nr_seq_grau_partic_w;
	end if;
	
	if	(nvl(nr_seq_tipo_conta_w,0) <> nvl(nr_seq_tipo_conta_ant_w,0)) then
		ds_historico_w	:= ds_historico_w || chr(13) || chr(10) || 'Alterado tipo conta de ' || nr_seq_tipo_conta_ant_w || ' para ' || nr_seq_tipo_conta_w;
	end if;	
	
	if	(ds_historico_w is not null) then
		ds_historico_w	:= 'Conta: ' || nr_seq_conta_p|| ds_historico_w;
		insert into pls_hist_analise_conta
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_analise,
			nr_seq_conta,
			ie_tipo_historico,
			ds_observacao,
			ds_call_stack)
		values	(pls_hist_analise_conta_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_analise_p,
			nr_seq_conta_p,
			17,
			ds_historico_w,
			substr(dbms_utility.format_call_stack,1,4000));
			
		ds_historico_w	:= null;
	end if;
end if;

commit;

end pls_atualizar_conta_pos;
/
