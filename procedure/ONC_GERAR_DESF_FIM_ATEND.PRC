create or replace
procedure onc_gerar_desf_fim_atend( 	nr_sequencia_p number,
					nm_usuario_p varchar2,
					ie_opcao_p	varchar2) is

dt_chegada_w	date;

begin

select  max(dt_chegada) 
into    dt_chegada_w
from	agenda_consulta
where  	nr_sequencia = nr_sequencia_p;

if (nr_sequencia_p is not null) then
	if (ie_opcao_p = 'F') then
	
		update  agenda_consulta
		set  	ie_status_agenda = 'AD',
			dt_atendido = sysdate,
			nm_usuario = nm_usuario_p
		where  	nr_sequencia = nr_sequencia_p;
		
	elsif (ie_opcao_p = 'D') then
	
		if (dt_chegada_w is null) then
			update  agenda_consulta
			set  	ie_status_agenda = 'N',
				dt_atendido = null,
				nm_usuario = nm_usuario_p
			where  	nr_sequencia = nr_sequencia_p;
		else	
			update  agenda_consulta
			set  	ie_status_agenda = 'A',
				dt_atendido = null,
				nm_usuario = nm_usuario_p
			where  	nr_sequencia = nr_sequencia_p;
		end if;
		
	end if;
end if;

commit;

end ONC_GERAR_DESF_FIM_ATEND;
/