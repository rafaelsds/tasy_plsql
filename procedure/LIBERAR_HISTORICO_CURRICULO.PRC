create or replace
procedure liberar_historico_curriculo(
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is

begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	pf_curriculo_historico
	set	dt_liberacao		= sysdate,
		nm_usuario_liberacao	= nm_usuario_p
	where	nr_sequencia		= nr_sequencia_p;
	end;
end if;

commit;

end liberar_historico_curriculo;
/
