create or replace
procedure pcs_gerar_itens_transf_estoque(
			nr_seq_registro_p		in number,
			cd_local_estoque_p		in number,	
			cd_local_estoque_solicitante_p	in number,			
			cd_material_p			in number,
			cd_condicao_pagto_p		in number,
			cd_comprador_p		in varchar2,
			qt_material_p		in number,
			cd_estab_solicitado_p		in number,
			cd_estab_solicitante_p		in number,
			nm_usuario_p			in varchar2,
			nr_ordem_compra_p		in out number) is

cd_estabelecimento_w		number(4);
cd_material_w			number(10);
qt_minimo_w			number(15,4)	:= 0;
qt_maximo_w			number(15,4)	:= 0;
cd_unidade_medida_estoque_w	varchar2(30)  	:= '';
qt_estoque_w           		number(15,4) 	:= 0;
qt_item_w	           		number(15,4) 	:= 0;
nr_item_oci_w			number(5) 	:= 0;
dt_entrega_w			date;
qt_pendentes_transf_w		number(13,4);


nr_ordem_compra_w		number(10);
cd_condicao_pagamento_w number(10);
cd_comprador_w			varchar2(10);
cd_moeda_w				number(5);
cd_pessoa_solicitante_w	varchar2(10);
ie_gerou_w				varchar2(1);
qt_existe_w			number(3);
cd_cgc_solicitado_w		varchar2(14);

cursor c01 is
select	c.cd_material,
		substr(obter_dados_material_estab(c.cd_material,cd_estab_solicitante_p,'UME'),1,30) cd_unidade_medida_estoque
from	material c
where	(c.cd_material 			= nvl(cd_material_p, c.cd_material))
and		(c.ie_situacao			= 'A')
order by c.cd_material;

begin
ie_gerou_w	:= 'N';

/*select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from 	local_estoque
where	cd_local_estoque = cd_local_estoque_p;*/

begin
select	a.cd_pessoa_fisica
into	cd_pessoa_solicitante_w
from	usuario a
where	a.nm_usuario_pesquisa = upper(nm_usuario_p);
exception
when others then
	cd_pessoa_solicitante_w	:= null;
end;

select cd_cgc
into cd_cgc_solicitado_w
from estabelecimento
where cd_estabelecimento = cd_estab_solicitado_p;

open C01;
loop
fetch C01 into
	cd_material_w,
	cd_unidade_medida_estoque_w;
exit when C01%notfound;
	begin
	if	(ie_gerou_w = 'N') then
		begin
		if	(nvl(nr_ordem_compra_p, 0) > 0) then
			nr_ordem_compra_w	:= nr_ordem_compra_p;
		
		end if;
	
		if	(nvl(nr_ordem_compra_p, 0) = 0) then
		
			select	ordem_compra_seq.nextval
			into	nr_ordem_compra_w
			from	dual;
		
					insert into ordem_compra (
					nr_ordem_compra,
					cd_estabelecimento,
					cd_estab_transf,
					cd_condicao_pagamento,
					cd_comprador,
					dt_ordem_compra,
					dt_atualizacao,
					nm_usuario,
					cd_moeda,
					ie_situacao,
					dt_inclusao,
					cd_pessoa_solicitante,
					ie_frete,
					dt_entrega,
					ie_aviso_chegada,
					ie_emite_obs,
					ie_urgente,
					ie_tipo_ordem,
					--cd_pessoa_fisica,
					cd_local_transf,
					cd_cgc_fornecedor,
					cd_local_entrega)
			values(	nr_ordem_compra_w,
					cd_estab_solicitante_p,
					cd_estab_solicitado_p,
					cd_condicao_pagto_p,
					cd_comprador_p,
					sysdate,
					sysdate,
					nm_usuario_p,
					1,
					'A',
					sysdate,
					cd_pessoa_solicitante_w,
					'C',
					sysdate + 7,
					'N',
					'N',
					'N',
					'Z',
					--cd_pessoa_solicitante_w,
					cd_local_estoque_p,
					cd_cgc_solicitado_w,
					cd_local_estoque_solicitante_p);
	
		
		end if;					
		ie_gerou_w	:= 'S';
		
		end;
	end if;
	
	select 	count(*)
	into	qt_existe_w
	from	ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_w
	and	cd_material = cd_material_w
	and	cd_local_estoque = cd_local_estoque_p;
	
	if	(qt_existe_w > 0) then
		begin
		select max(nr_item_oci)
		into	nr_item_oci_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w
		and	cd_material = cd_material_w
		and	cd_local_estoque = cd_local_estoque_p;
	
		update	ordem_compra_item
		set	qt_material = qt_material_p,
			cd_unidade_medida_compra = cd_unidade_medida_estoque_w,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			vl_total_item = round(( qt_material_p * vl_unitario_material),4)
		where	nr_ordem_compra = nr_ordem_compra_w
		and	nr_item_oci = nr_item_oci_w;
		end;
	else
		begin
	
		select nvl(max(nr_item_oci),0)
		into	nr_item_oci_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w;
	
		nr_item_oci_w := nr_item_oci_w + 1;
	
		insert into ordem_compra_item(
			nr_ordem_compra,
			nr_item_oci,
			cd_material,
			qt_material,
			qt_original,
			dt_atualizacao,
			nm_usuario,
			cd_unidade_medida_compra,
			ie_situacao,
			nr_seq_reg_pcs,
			cd_local_estoque)
		values(
			nr_ordem_compra_w,
			nr_item_oci_w,
			cd_material_w,
			qt_material_p,
			qt_material_p,
			sysdate,
			nm_usuario_p,
			cd_unidade_medida_estoque_w,			
			'A',
			nr_seq_registro_p,
			cd_local_estoque_solicitante_p);
			
		update	pcs_reg_analise_itens
		set		nr_ordem_compra = nr_ordem_compra_w
		where	nr_seq_registro = nr_seq_registro
		and		cd_material = cd_material_w;
	
		insert into ordem_compra_item_entrega(
			nr_ordem_compra,
			nr_item_oci,
			dt_prevista_entrega,
			qt_prevista_entrega,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia)
		values(
			nr_ordem_compra_w,
			nr_item_oci_w,
			sysdate + 7,
			qt_material_p,
			sysdate,
			nm_usuario_p,
			ordem_compra_item_entrega_seq.nextval);
		end;
	end if;
	end;
end loop;
close C01;
	
commit;

nr_ordem_compra_p := nvl(nr_ordem_compra_w,0);

end pcs_gerar_itens_transf_estoque;
/
