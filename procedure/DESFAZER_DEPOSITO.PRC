create or replace
procedure DESFAZER_DEPOSITO	(nr_seq_deposito_p	in	number,
				dt_atualizacao_p	in 	date,
				nm_usuario_p		in	varchar2) is


nr_seq_cheque_w		number(10,0);
dt_deposito_w			date;
dt_reapresentacao_w		date;
dt_seg_reapresentacao_w	date;

cursor	c01 is
select	c.nr_seq_cheque,
	c.dt_deposito,
	c.dt_reapresentacao,
	c.dt_seg_reapresentacao
from	cheque_cr c,
	deposito_cheque b
where	b.nr_seq_deposito	= nr_seq_deposito_p
and	b.nr_seq_cheque	= c.nr_seq_cheque;

begin

open c01;
loop
fetch c01 into
	nr_seq_cheque_w,
	dt_deposito_w,
	dt_reapresentacao_w,
	dt_seg_reapresentacao_w;
exit when c01%notfound;

	/* Desfazendo segunda reapresentação */
	if	(dt_seg_reapresentacao_w is not null) then
		update	cheque_cr
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_seg_reapresentacao	= null
		where	nr_seq_cheque		= nr_seq_cheque_w;

		gerar_cheque_cr_hist(nr_seq_cheque_w,wheb_mensagem_pck.get_texto(304948),'N',nm_usuario_p);
											  
	end if;

	/* Desfazendo reapresentação */
	if	(dt_reapresentacao_w is not null) and
		(dt_seg_reapresentacao_w is null) then
		update	cheque_cr
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_reapresentacao	= null
		where	nr_seq_cheque		= nr_seq_cheque_w;

		gerar_cheque_cr_hist(nr_seq_cheque_w,wheb_mensagem_pck.get_texto(304945),'N',nm_usuario_p);
	end if;

	/* Desfazendo depósito */
	if	(dt_deposito_w is not null) and
		(dt_reapresentacao_w is null) then
		update	cheque_cr
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_deposito		= null
		where	nr_seq_cheque		= nr_seq_cheque_w;

		gerar_cheque_cr_hist(nr_seq_cheque_w,wheb_mensagem_pck.get_texto(304946),'N',nm_usuario_p);
	end if;

	ATUALIZAR_DESBLOQUEIO_CHEQUE(nr_seq_cheque_w, nm_usuario_p);
	atualizar_saldo_neg_cheque_cr(nr_seq_cheque_w,nm_usuario_p);
	atualizar_cobranca_cheque(nr_seq_cheque_w,nm_usuario_p);

end loop;
close c01;

update	deposito
set	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_deposito	= null
where	nr_sequencia	= nr_seq_deposito_p;

GERAR_MOVTO_DEPOSITO(nr_seq_deposito_p,dt_atualizacao_p,nm_usuario_p);

commit;

end DESFAZER_DEPOSITO;
/