create or replace
procedure Atualizar_Atend_Triagem(	nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number) is 
nr_seq_triagem_w	number(10);
qt_horas_validade_w	number(10);
qt_reg_w		number(10);
NR_SEQ_CLASSIF_w	number(10);
NR_SEQ_TRIAGEM_PRIORIDADE_w	number(10);
nr_seq_queixa_w		number(10);
nm_usuario_nrec_w 	varchar2(15);

begin

qt_horas_validade_w		:= nvl(obter_valor_param_usuario(916, 512, Obter_Perfil_Ativo, nm_usuario_p, nvl(cd_estabelecimento_p,0)),0);

dbms_output.put_line('qt_horas_validade_w='||qt_horas_validade_w);
if	(qt_horas_validade_w	> 0) then
	select	max(a.nr_sequencia),
		max(a.NR_SEQ_CLASSIF),
		max(a.NR_SEQ_TRIAGEM_PRIORIDADE),
		max(a.NR_SEQ_QUEIXA),
		max(a.NM_USUARIO_NREC)
	into	nr_seq_triagem_w,
		NR_SEQ_CLASSIF_w,
		NR_SEQ_TRIAGEM_PRIORIDADE_w,
		nr_seq_queixa_w,
		nm_usuario_nrec_w
	from	triagem_pronto_atend a
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	nr_atendimento	is null
	and	dt_atualizacao_nrec between sysdate - (qt_horas_validade_w / 24) and sysdate; 
	
	select	count(*)
	into	qt_reg_w
	from	triagem_pronto_atend
	where	nr_atendimento	= nr_atendimento_p;
	

	if	(qt_reg_w	= 0) and
		(nr_seq_triagem_w	is not null) then
	
		update	triagem_pronto_atend
		set	nr_atendimento	= nr_atendimento_p
		where	nr_sequencia	= nr_seq_triagem_w;
		
		update	atendimento_paciente
		set	NR_SEQ_TRIAGEM	= NR_SEQ_CLASSIF_w,
			NR_SEQ_TRIAGEM_PRIORIDADE = NR_SEQ_TRIAGEM_PRIORIDADE_w,
			nr_seq_queixa = nr_seq_queixa_w,
			nm_usuario 		= nm_usuario_nrec_w
		where	nr_atendimento	= nr_atendimento_p
		and	nr_seq_triagem is null;
		
		update	atendimento_sinal_vital
		set	nr_atendimento	= nr_atendimento_p
		where	nr_seq_triagem	= nr_seq_triagem_w
		and	nr_atendimento	is null;
		
		
		commit;
		
		atualiza_atendimento_triagem(nr_seq_triagem_w,nm_usuario_p);
	end if;
end if;
commit;

end Atualizar_Atend_Triagem;
/
