create or replace
procedure gpi_atual_anexo_doc_email(	nr_sequencia_p	number,
				ie_anexar_p	varchar2,
				nm_usuario_p	varchar2) is 

begin

update	gpi_projeto_doc
set	ie_anexar_email	= ie_anexar_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_seq_projeto	= nr_sequencia_p;

commit;

end gpi_atual_anexo_doc_email;
/