create or replace 
procedure update_response_code_ihc(
									nr_account_p		in number,
									cd_response_code_p	in number,
									cd_transaction_p	in varchar2) is

nr_sequencia_w	ihc_claim.nr_sequencia%type;
begin
	select 	max(nr_sequencia)
	into 	nr_sequencia_w
	from 	ihc_claim
	where 	nr_account = nr_account_p;

	update	ihc_claim 
	set		cd_response_code 	= cd_response_code_p,
			cd_transaction		= cd_transaction_p
	where	nr_sequencia = nr_sequencia_w;

	commit;
end update_response_code_ihc;
/
