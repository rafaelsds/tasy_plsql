create or replace
procedure pls_salvar_lote_ax_guias_aut( nr_seq_lote_anexo_p		number,
					nm_usuario_p			varchar2,
					cd_ans_p			varchar2,
					cd_guia_p			varchar2,
					cd_guia_prestador_p		varchar2,
					cd_guia_referencia_p		varchar2,
					dt_solicitacao_p		date,
					cd_senha_p			varchar2,
					dt_autorizacao_p		date,
					cd_usuario_plano_p		varchar2,
					ie_recem_nascido_p		varchar2,
					nm_beneficiario_p		varchar2,
					cd_cnes_p			varchar2,
					ds_biometria_p			varchar2,
					nm_profissional_solic_p		varchar2,
					nr_telef_prof_solic_p		varchar2,
					ds_justificativa_p		varchar2,
					ds_email_prof_solic_p		varchar2,
					ds_especificacao_p		varchar2,
					qt_peso_p			number,
					qt_altura_p			number,
					qt_superficie_corporal_p 	number,
					qt_idade_benef_p		number,
					ie_sexo_p			varchar2,
					ds_procedimento_cirurgico_p	varchar2,
					dt_real_proc_cirurgico_p 	date,
					ds_area_irradiada_p		varchar2,
					dt_radioterapia_p		date,
					nr_ciclo_previsto_p		number,
					qt_intervalo_ciclo_p		number,
					nr_ciclo_atual_p		number,
					ds_quimioterapia_p		varchar2,
					dt_quimioterapia_p		date,
					qt_dose_total_p			number,
					qt_dose_dia_p			number,
					nr_campos_p			number,
					dt_inicio_previsto_p		date,
					qt_dias_previsto_p		number,
					nr_seq_guia_p			number,
					ie_tipo_anexo_p			varchar2,
					ds_observacao_p			varchar2,
					nr_seq_guia_plano_imp_p		pls_guia_plano_imp.nr_sequencia%type,
					nr_seq_lote_ax_guias_p	 out	number,
					nr_dias_ciclo_atual_p		number,
					ie_tipo_ident_benef_p		pls_lote_anexo_guias_imp.ie_tipo_ident_benef%type,
					cd_ident_biometria_benef_p	pls_lote_anexo_guias_imp.cd_ident_biometria_benef%type,
					cd_template_biomet_benef_p	pls_lote_anexo_guias_imp.cd_template_biomet_benef%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Salvar  as guias no lote de anexo.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  x]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_lote_guia_w		Number(10);
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
nr_seq_guia_pricipal_w		pls_lote_anexo_guias_imp.nr_seq_guia_principal%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;

begin
nr_seq_guia_w := nr_seq_guia_p;

--Verifica se o benefici�rio existe benefici�rio na base
nr_seq_segurado_w := pls_obter_dados_carteira(null,cd_usuario_plano_p,'S');

if	( cd_guia_referencia_p is not null and nr_seq_guia_p is null ) then

	--Verifica se existe guia n�o autorizada para ser adicionado o anexo
	select	max(nr_sequencia)
	into	nr_seq_guia_pricipal_w
	from	pls_guia_plano
	where   cd_guia 	= cd_guia_referencia_p
	and	nr_seq_segurado = nr_seq_segurado_w
	and	ie_status <> '1';
	
	--Caso n�o encontre a guia pelo CD_GUIA ser� realizada a busca pelo c�digo da guia do prestador CD_GUIA_PRESTADOR
	if	( nr_seq_guia_pricipal_w is null ) then
		select	max(nr_sequencia)
		into	nr_seq_guia_pricipal_w
		from	pls_guia_plano
		where	cd_guia_prestador 	= cd_guia_referencia_p
		and	nr_seq_segurado 	= nr_seq_segurado_w
		and	ie_status <> '1';
	end if; 
		
	nr_seq_guia_w := nr_seq_guia_pricipal_w;
end if;

select	max(nr_seq_requisicao)
into	nr_seq_requisicao_w
from	pls_guia_plano_imp
where	nr_seq_guia_plano = nr_seq_guia_w;

select	pls_lote_anexo_guias_imp_seq.nextval
into	nr_seq_lote_guia_w
from	dual; 

insert	into	pls_lote_anexo_guias_imp(
	nr_sequencia, nr_seq_lote_anexo, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	cd_ans, cd_guia, cd_guia_prestador,
	cd_guia_referencia, dt_solicitacao, cd_senha,
	dt_autorizacao, cd_usuario_plano, ie_recem_nascido,
	nm_beneficiario, cd_cnes, ds_biometria,
	nm_profissional_solic, nr_telef_prof_solic, ds_justificativa,
	ds_email_prof_solic, ds_especificacao, qt_peso,
	qt_altura, qt_superficie_corporal, qt_idade_benef,
	ie_sexo, ds_procedimento_cirurgico, dt_real_proc_cirurgico,
	ds_area_irradiada, dt_radioterapia, nr_ciclo_previsto,
	nr_ciclo_atual, qt_intervalo_ciclo, ds_quimioterapia,
	dt_quimioterapia, qt_dose_total, qt_dose_dia,
	nr_campos, dt_inicio_previsto, qt_dias_previsto,
	nr_seq_guia, ie_status,  ie_tipo_anexo, 
	ds_observacao, nr_seq_guia_principal, nr_seq_guia_plano_imp,
	nr_seq_requisicao, nr_dia_ciclo_atual, ie_tipo_ident_benef, 
	cd_ident_biometria_benef, cd_template_biomet_benef)
  values(nr_seq_lote_guia_w, nr_seq_lote_anexo_p, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	cd_ans_p, cd_guia_p, cd_guia_prestador_p,
	cd_guia_referencia_p, dt_solicitacao_p, cd_senha_p,
	dt_autorizacao_p, cd_usuario_plano_p, ie_recem_nascido_p,
	nm_beneficiario_p, cd_cnes_p,  ds_biometria_p,
	nm_profissional_solic_p, nr_telef_prof_solic_p, ds_justificativa_p,
	ds_email_prof_solic_p, ds_especificacao_p, qt_peso_p,
	qt_altura_p, qt_superficie_corporal_p, qt_idade_benef_p,
	ie_sexo_p, ds_procedimento_cirurgico_p, dt_real_proc_cirurgico_p,
	ds_area_irradiada_p, dt_radioterapia_p, nr_ciclo_previsto_p,
	nr_ciclo_atual_p, qt_intervalo_ciclo_p, ds_quimioterapia_p,
	dt_quimioterapia_p, qt_dose_total_p, qt_dose_dia_p,
	nr_campos_p, dt_inicio_previsto_p, qt_dias_previsto_p,
	nr_seq_guia_w, '2',  ie_tipo_anexo_p, 
	ds_observacao_p, nr_seq_guia_pricipal_w, nr_seq_guia_plano_imp_p,
	nr_seq_requisicao_w, nr_dias_ciclo_atual_p, ie_tipo_ident_benef_p, 
	cd_ident_biometria_benef_p, cd_template_biomet_benef_p);

nr_seq_lote_ax_guias_p := nr_seq_lote_guia_w;

commit;

end pls_salvar_lote_ax_guias_aut;
/
