create or replace
procedure Obter_Kit_Procedimento
		(nr_atendimento_p	in	number,
		 cd_estabelecimento_p	in	number,
		 cd_procedimento_p	in	number,
		 ie_origem_proced_p	in	number,
		 nr_seq_proc_interno_p	in	number,
		 cd_setor_atendimento_p	in	number,
		 cd_medico_p		in	varchar2,
		 cd_kit_material_p	out	number) is

cd_kit_material_w	number(10,0)	:= 0;
cd_setor_atendimento_w	number(15); 
cd_medico_w		varchar2(10);
qt_idade_w		number(15,2);
cd_convenio_w		number(5,0);

Cursor C01 is
	select	nvl(cd_kit_material,0)
	from		proc_interno_kit
	where	nr_seq_proc_interno	= nr_seq_proc_interno_p
	and		nvl(cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
	and		cd_estabelecimento	= cd_estabelecimento_p
	and		nvl(cd_medico,cd_medico_w)	= cd_medico_w	
	and		nvl(qt_idade_w,0) between nvl(obter_idade_kit_proced(nr_sequencia,'MIN'),0) and nvl(obter_idade_kit_proced(nr_sequencia,'MAX'),999)
	and		nvl(cd_convenio, cd_convenio_w)	= cd_convenio_w
	and		nvl(cd_perfil, nvl(obter_perfil_ativo,0))	= nvl(obter_perfil_ativo,0)
	order by 
			nvl(cd_medico,'0'),
			nvl(cd_setor_atendimento,0),
			nvl(cd_convenio,0);

begin

select	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DIA')),
		nvl(max(obter_convenio_atendimento(a.nr_atendimento)),0)
into		qt_idade_w,
		cd_convenio_w
from		atendimento_paciente a,
		pessoa_fisica b
where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and		a.nr_atendimento = nr_atendimento_p;

cd_setor_atendimento_w		:= nvl(cd_setor_atendimento_p,0);
cd_medico_w	:= nvl(cd_medico_p,'0');

open C01;
loop
fetch C01 into	
	cd_kit_material_w;
exit when C01%notfound;
	
end loop;
close C01;

if	(nvl(cd_kit_material_w,0)	= 0) then
	begin
	select	nvl(max(cd_kit_material),0)
	into		cd_kit_material_w
	from		proc_interno
	where	nr_sequencia	= nr_seq_proc_interno_p;
	exception
		when others then
		cd_kit_material_w	:= 0;
	end;
end if;

if	(cd_kit_material_w = 0) then
	select	nvl(max(cd_kit_material),0)
	into		cd_kit_material_w
	from 	procedimento
	where	cd_procedimento		= cd_procedimento_p
	and		ie_origem_proced	= ie_origem_proced_p;
end if;

cd_kit_material_p	:= cd_kit_material_w;

end Obter_Kit_Procedimento;
/