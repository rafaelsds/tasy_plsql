create or replace procedure pls_gerar_sjrp_arq_guarani(	nr_sequencia_p		number,
					ds_local_p		varchar2,
					nm_arquivo_p	 	varchar2) is 

ds_erro_w			varchar2(255);
ds_conteudo_w			varchar2(4000);
arq_texto_w			utl_file.file_type;

Cursor C01 is
	select	substr(rpad(to_char(n.dt_mesano_referencia, 'YYYYMM') || substr(j.cd_usuario_plano,2,15), 21, '0'),1,21) nr_periodo_identificacao,
		rpad(substr(g.nm_pessoa_fisica,1,30),30,' ') nm_beneficiario,
		rpad(substr(i.cd_guia,1,10),10,' ') nr_guia,
		to_char(coalesce(i.dt_atendimento, i.dt_atendimento_referencia, i.dt_atendimento_imp, i.dt_atendimento_imp_referencia), 'dd/mm/yyyy') dt_atendimento,
		lpad(nvl(substr(pls_obter_dados_conta_proc(h.nr_seq_conta_proc,'C'),1,8), substr(pls_obter_dados_conta_mat(h.nr_seq_conta_mat,'C'),1,8)),8,' ') cd_procedimento,
		rpad(nvl(substr(pls_obter_dados_conta_proc(h.nr_seq_conta_proc,'D'),1,50), substr(pls_obter_dados_conta_mat(h.nr_seq_conta_mat,'D'),1,50)),50,' ') ds_procedimento,
		lpad(replace(replace(campo_mascara_virgula(m.vl_coparticipacao),'.',''),',',''),15,'0') vl_coparticipacao,
		lpad(substr(nvl(f.cd_matricula_estipulante, '0'),1,16),16,'0') nr_matricula,
		lpad(substr(j.cd_usuario_plano,1,17), 17, '0') cd_usuario_plano
	from	pls_ame_lote_rem_valor		a,
		pls_ame_lote_rem_arquivo	b,
		pls_ame_lote_rem_destino	c,
		pls_ame_lote_remessa		d,
		pls_ame_lote_rem_valor_det	e,
		pls_segurado			f,
		pessoa_fisica			g,
		pls_conta_coparticipacao	h,
		pls_conta			i,
		pls_segurado_carteira		j,
		pls_ame_lote_rem_valor_cop	m,
		pls_mensalidade_segurado	n
	where	c.nr_seq_lote_rem		= d.nr_sequencia
	and	c.nr_sequencia			= b.nr_seq_lote_rem_dest
	and	b.nr_sequencia			= a.nr_seq_lote_rem_arq
	and	a.nr_sequencia			= e.nr_seq_lote_rem_valor
	and	f.nr_sequencia			= a.nr_seq_segurado
	and 	e.nr_sequencia      		= m.nr_seq_rem_valor_detalhe
	and 	m.nr_seq_conta_coparticipacao   = h.nr_sequencia
	and	g.cd_pessoa_fisica		= f.cd_pessoa_fisica
	and	i.nr_sequencia			= h.nr_seq_conta
	and	f.nr_sequencia			= j.nr_seq_segurado
	and	n.nr_sequencia			= e.nr_seq_mensalidade_seg
	and	a.nr_seq_lote_rem_arq		= nr_sequencia_p;

begin

begin
arq_texto_w := utl_file.fopen(ds_local_p,nm_arquivo_p,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);
end if;

for r_c01_w in C01 loop
	begin		
	
	ds_conteudo_w	:= 	r_c01_w.nr_periodo_identificacao || ';' || r_c01_w.nm_beneficiario || ';' || r_c01_w.dt_atendimento || ';' || r_c01_w.nr_guia || ';' ||
				r_c01_w.cd_procedimento || ';' || r_c01_w.ds_procedimento || ';' || r_c01_w.vl_coparticipacao || ';' || r_c01_w.nr_matricula || ';' ||
				r_c01_w.cd_usuario_plano;
	
	utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
	utl_file.fflush(arq_texto_w);
	end;
end loop;

utl_file.fclose(arq_texto_w);

end pls_gerar_sjrp_arq_guarani;
/
