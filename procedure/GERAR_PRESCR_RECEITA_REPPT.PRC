create or replace
procedure gerar_prescr_receita_RepPT(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					nm_usuario_p 		varchar2) is

begin
   if (nr_prescricao_p is not null) and
      (nr_sequencia_p is not null) then   
	insert into rep_medicamento_receita(nr_sequencia,
					    dt_atualizacao,          
					    nm_usuario,
					    dt_atualizacao_nrec,
					    nm_usuario_nrec,         
					    nr_prescricao,           
					    nr_seq_material)
				     values(rep_medicamento_receita_seq.nextval,
				            sysdate,
					    nm_usuario_p,
					    sysdate,
					    nm_usuario_p,
					    nr_prescricao_p,
					    nr_sequencia_p);
                                      				      
   

end if;
commit;
end gerar_prescr_receita_RepPT;
/