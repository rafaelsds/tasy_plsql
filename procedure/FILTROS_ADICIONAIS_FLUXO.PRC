create or replace
procedure filtros_adicionais_fluxo(
			ie_tipo_filtro_p		Varchar2,
			ie_disp_selec_p 		varchar2,
			nm_usuario_p 			varchar2,
			cd_banco_p 			varchar2,
			nr_seq_proj_rec_p		varchar2,
			nr_seq_caixa_p 			varchar2,
			cd_estabelecimento_p		varchar2) is 

begin

if	(ie_tipo_filtro_p = 'B') then
	if	(ie_disp_selec_p = 'D') then	
		insert	into	w_filtro_fluxo
	                        (dt_atualizacao, 
				nm_usuario, 
				nr_sequencia, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				cd_banco)
			values (sysdate,
				nm_usuario_p,
				w_filtro_fluxo_seq.nextval,
				sysdate,
				nm_usuario_p,
				cd_banco_p);
	
	elsif	(ie_disp_selec_p = 'S') then
		delete  
		from 	w_filtro_fluxo
                where	cd_banco = cd_banco_p;
	end if;

elsif	(ie_tipo_filtro_p = 'P') then
	if	(ie_disp_selec_p = 'D') then
		insert	into	w_filtro_fluxo
                                (dt_atualizacao,
				nm_usuario,
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_proj_rec)
			values (sysdate,
				nm_usuario_p,
				w_filtro_fluxo_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_proj_rec_p);
	
	elsif	(ie_disp_selec_p = 'S') then
		delete  
		from 	w_filtro_fluxo
                where	nr_seq_proj_rec = nr_seq_proj_rec_p;
	end if;
elsif	(ie_tipo_filtro_p = 'C') then
	if	(ie_disp_selec_p = 'D') then
		insert	into	w_filtro_fluxo
                                (dt_atualizacao,
				nm_usuario,
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_caixa)
			values (sysdate,
				nm_usuario_p,
				w_filtro_fluxo_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_caixa_p);
	
	elsif	(ie_disp_selec_p = 'S') then
		delete  
		from 	w_filtro_fluxo
                where	nr_seq_caixa = nr_seq_caixa_p;
	end if;
elsif	(ie_tipo_filtro_p = 'E') then
	if	(ie_disp_selec_p = 'D') then
		insert	into	w_filtro_fluxo
                                (dt_atualizacao,
				nm_usuario,
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_proj_rec)
			values (sysdate,
				nm_usuario_p,
				w_filtro_fluxo_seq.nextval,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p);
	
	elsif	(ie_disp_selec_p = 'S') then
		delete  
		from 	w_filtro_fluxo
                where	cd_estabelecimento = cd_estabelecimento_p;
	end if;
end if;

commit;

end filtros_adicionais_fluxo;
/