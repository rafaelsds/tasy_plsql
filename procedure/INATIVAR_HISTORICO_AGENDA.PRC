create or replace
PROCEDURE inativar_historico_agenda(	nr_sequencia_p	NUMBER,
					nm_usuario_p		VARCHAR2) IS

BEGIN

UPDATE	agenda_pac_hist
SET	nm_usuario_inativacao = nm_usuario_p,
	ie_situacao	= 'I',
	dt_inativacao = SYSDATE
WHERE	nr_sequencia = nr_sequencia_p;

COMMIT;

END inativar_historico_agenda;
/
