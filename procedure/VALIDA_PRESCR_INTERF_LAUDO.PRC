create or replace
procedure Valida_prescr_interf_laudo(	nr_prescricao_p	in  number,
					cd_interface_p	in  number,
					ds_mensagem_p	out varchar2,
					nm_paciente_p	out varchar2) is 
qt_items_w	number := 0;					
ds_mensagem_w	varchar2(255);
begin


if (cd_interface_p = 2018) then
	select  count(*)
	into	qt_items_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	if (qt_items_w = 0) then			
		ds_mensagem_w := substr(obter_texto_tasy(186035, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	else
		select  count(*)
		into	qt_items_w		
		from  	prescr_medica b, 
			prescr_procedimento a, 
			laudo_paciente l,
			procedimento_paciente e
		where   a.nr_prescricao = b.nr_prescricao
		and     a.nr_prescricao = e.nr_prescricao
		and     a.nr_sequencia  = e.nr_sequencia_prescricao
		and     l.nr_seq_proc   = e.nr_sequencia
		and     l.dt_liberacao  is not null
		and	b.nr_prescricao = nr_prescricao_p;
		
		if (qt_items_w = 0) then
			ds_mensagem_w := substr(obter_texto_tasy(186036, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		else
			select  count(*)
			into	qt_items_w		
			from  	prescr_medica b, 
				prescr_procedimento a, 
				laudo_paciente l,
				procedimento_paciente e
			where   a.nr_prescricao = b.nr_prescricao
			and     a.nr_prescricao = e.nr_prescricao
			and     a.nr_sequencia  = e.nr_sequencia_prescricao
			and     l.nr_seq_proc   = e.nr_sequencia
			and     l.dt_liberacao  is not null
			and	l.dt_integracao_interf is null
			and	b.nr_prescricao = nr_prescricao_p;
			
			if (qt_items_w = 0) then
				ds_mensagem_w := substr(obter_texto_tasy(186037, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			else
				select  count(*)
				into	qt_items_w
				from   	hmj_laboratorio_v
				where  	nr_prescricao =nr_prescricao_p;		 
				
				if (qt_items_w = 0) then
					ds_mensagem_w := substr(obter_texto_tasy(186132, wheb_usuario_pck.get_nr_seq_idioma),1,255);
				end if;
				
			end if;
			
		end if;
		
	end if;	
	
	select  max(substr(nvl(obter_nome_pf(a.cd_pessoa_fisica),''),1,255))
	into	nm_paciente_p
	from	pessoa_fisica a,
		prescr_medica b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.nr_prescricao = nr_prescricao_p;
	
end if;

if (ds_mensagem_w is not null) then
	ds_mensagem_p := substr(ds_mensagem_w,1,255);
end if;

end Valida_prescr_interf_laudo;
/
