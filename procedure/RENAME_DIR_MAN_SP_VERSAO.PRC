CREATE OR REPLACE PROCEDURE RENAME_DIR_MAN_SP_VERSAO(
ie_rename_sp_p    man_service_pack_versao.ie_rename_folder_sp%type,
nr_service_pack_p Man_Service_Pack_Versao.nr_service_pack%type,
cd_versao_p       Man_Service_Pack_Versao.cd_versao%type

) IS
BEGIN

  update  man_service_pack_Versao
  set     dt_atualizacao          = sysdate,
          ie_rename_folder_sp     = ie_rename_sp_p
  where   nr_service_pack         = nr_service_pack_p
    and   cd_versao               = cd_versao_p;
          
  commit;
END RENAME_DIR_MAN_SP_VERSAO;
/