create or replace
procedure gera_item_contrato_solic_pagto(	nr_solic_compra_p			number,
					nr_seq_contrato_p			number,
					nr_seq_regra_pagto_p		number,
					qt_dias_entrega_p			number,
					nr_seq_orc_item_gpi_p		varchar2,
					ie_opcao_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is

nr_seq_regra_pagto_w		number(10);
cd_material_w			number(10);
vl_pagto_w			number(13,4);
cd_local_estoque_w		number(10);
cd_centro_custo_w			number(10);
nr_item_solic_compra_w		number(10);
cd_conta_contabil_w		varchar2(20);
cd_unidade_medida_compra_w	varchar2(30);
ie_tipo_conta_w			number(3);
nr_seq_conta_financ_w		contrato_regra_nf.nr_seq_conta_financ%type;
nr_seq_crit_rateio_w		contrato_regra_nf.nr_seq_crit_rateio%type;
dt_solicitacao_compra_w		solic_compra.dt_solicitacao_compra%type;
	
cursor c01 is
select	nr_sequencia,
	cd_material,
	nvl(vl_pagto,0),
	nr_Seq_conta_financ,
	nr_seq_crit_rateio
from	contrato_regra_nf
where	nr_seq_contrato = nr_seq_contrato_p
and	ie_opcao_p = 0
and	((dt_inicio_vigencia is null) or (trunc(dt_inicio_vigencia,'dd') <= trunc(sysdate,'dd')))
and	((dt_fim_vigencia is null) or (trunc(dt_fim_vigencia,'dd') >= trunc(sysdate,'dd')))
and	(cd_estab_regra is null or cd_estab_regra = cd_estabelecimento_p)
union
select	nr_sequencia,
	cd_material,
	nvl(vl_pagto,0),
	nr_Seq_conta_financ,
	nr_seq_crit_rateio
from	contrato_regra_nf
where	nr_sequencia = nr_seq_regra_pagto_p
and	ie_opcao_p = 1;
						
begin

select	cd_local_estoque,
	cd_centro_custo,
	dt_solicitacao_compra
into	cd_local_estoque_w,
	cd_centro_custo_w,
	dt_solicitacao_compra_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_pagto_w,
	cd_material_w,
	vl_pagto_w,
	nr_seq_conta_financ_w,
	nr_seq_crit_rateio_w;
exit when C01%notfound;
	begin
	
	select	nvl(max(nr_item_solic_compra),0) + 1
	into	nr_item_solic_compra_w
	from	solic_compra_item
	where	nr_solic_compra = nr_solic_compra_p;	
	
	select	cd_unidade_medida_compra
	into	cd_unidade_medida_compra_w
	from	material
	where	cd_material = cd_material_w;
	
	ie_tipo_conta_w := 3;

	if	(cd_centro_custo_w is null) then
		ie_tipo_conta_w	:= 2;
	end if;
	
	define_conta_material(
		cd_estabelecimento_p,
		cd_material_w,
		ie_tipo_conta_w,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		cd_local_estoque_w,
		0,
		sysdate,
		cd_conta_contabil_w,
		cd_centro_custo_w,
		null,
		'N');		
		
	insert into solic_compra_item(
		nr_solic_compra,
		nr_item_solic_compra,
		cd_material,
		cd_unidade_medida_compra,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		dt_solic_item,
		vl_unit_previsto,
		ie_geracao,
		nr_contrato,
		nr_seq_regra_contrato,
		cd_conta_contabil,
		qt_saldo_disp_estoque,
		nr_seq_orc_item_gpi,
		nr_seq_conta_financ)
	values(	nr_solic_compra_p,
		nr_item_solic_compra_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		1,
		sysdate,
		nm_usuario_p,
		'A',
		trunc(sysdate + qt_dias_entrega_p,'dd'),
		vl_pagto_w,
		'A',
		nr_seq_contrato_p,
		nr_seq_regra_pagto_w,
		cd_conta_contabil_w,
		obter_saldo_disp_estoque(cd_estabelecimento_p, cd_material_w, cd_local_estoque_w, trunc(sysdate,'mm')),
		nr_seq_orc_item_gpi_p,
		nr_seq_conta_financ_w);
	
	insert into solic_compra_item_entrega(
		nr_solic_compra,
		nr_item_solic_compra,
		nr_item_solic_compra_entr,
		qt_entrega_solicitada,
		dt_entrega_solicitada,
		dt_atualizacao,
		nm_usuario)
	values(	nr_solic_compra_p,
		nr_item_solic_compra_w,
		1,
		1,
		trunc(sysdate + qt_dias_entrega_p,'dd'),
		sysdate,
		nm_usuario_p);
	
	if	(nr_seq_crit_rateio_w > 0) then
		ratear_item_solic_compra(
			nr_solic_compra_p,
			nr_item_solic_compra_w,
			nr_seq_crit_rateio_w,
			cd_estabelecimento_p,
			nm_usuario_p,
			dt_solicitacao_compra_w);
	end if;
	
	end;
end loop;
close C01;

commit;

end gera_item_contrato_solic_pagto;
/
