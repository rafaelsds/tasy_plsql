create or replace
procedure pls_gerar_lote_giss
			(	nr_seq_lote_p		number,
				ie_opcao_p		varchar2, 
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_sequencia_nf_w		pls_tomador_prestador_giss.nr_sequencia_nf%type;
cd_pessoa_fisica_w		pls_tomador_prestador_giss.cd_pessoa_fisica%type;
cd_cgc_w			pls_tomador_prestador_giss.cd_cgc%type;
nr_layout_w			pls_tomador_prestador_giss.nr_layout%type;
ie_prestador_tomador_w		pls_tomador_prestador_giss.ie_prestador_tomador%type;
nr_nf_inicial_w			pls_tomador_prestador_giss.nr_nf_inicial%type;
nr_nf_final_w			pls_tomador_prestador_giss.nr_nf_final%type;
cd_serie_nf_w			pls_tomador_prestador_giss.cd_serie_nf%type;
vl_nota_fiscal_w		pls_tomador_prestador_giss.vl_nota_fiscal%type;
vl_base_calculo_w		pls_tomador_prestador_giss.vl_base_calculo%type;
cd_atividade_prestada_w		pls_tomador_prestador_giss.cd_atividade_prestada%type;
ie_prestador_municipio_w	pls_tomador_prestador_giss.ie_prestador_municipio%type;
ie_servico_municipio_w		pls_tomador_prestador_giss.ie_servico_municipio%type;
cd_indicador_w			pls_tomador_prestador_giss.cd_indicador%type;
cd_municipio_ibge_w		pessoa_juridica.cd_municipio_ibge%type;
ie_tipo_nota_w			nota_fiscal.ie_tipo_nota%type;
ie_tipo_lote_w			pls_lote_envio_giss.ie_tipo_lote%type;
ie_origem_doc_w			pls_tomador_prestador_giss.ie_origem_doc%type;
nr_titulo_w			titulo_receber.nr_titulo%type;
vl_tributo_w			pls_tomador_prestador_giss.vl_tributo%type;
vl_trib_nao_retido_w		pls_tomador_prestador_giss.vl_trib_nao_retido%type;
ie_origem_doc_lote_w		pls_lote_envio_giss.ie_origem_doc%type;
nr_titulo_pagar_w		titulo_pagar.nr_titulo%type;
ie_cooperado_w			pls_lote_envio_giss.ie_cooperado%type;
ie_retencao_w			pls_lote_envio_giss.ie_retencao%type;
ie_estabelecido_w		pls_lote_envio_giss.ie_estabelecido%type;
ie_inscricao_mun_w		pls_lote_envio_giss.ie_inscricao_mun%type;
nr_inscricao_municipal_w	pessoa_juridica.nr_inscricao_municipal%type;

cd_municipio_ibge_ww		varchar2(6);
ie_gera_w			varchar2(1);
ie_gera_giss_w			varchar2(1);
qt_tributo_w			number(10)	:= 0;
qt_cooperado_w			number(10)	:= 0;
dt_mes_referencia_w		date;
dt_emissao_nf_w			date;

Cursor C01 is
	select	b.nr_sequencia,
		null,
		b.cd_pessoa_fisica,
		b.cd_cgc,
		b.dt_emissao,
		b.nr_nota_fiscal,
		b.nr_nota_fiscal,
		b.cd_serie_nf,
		nvl(b.vl_mercadoria,0),
		nvl(a.vl_base_calculo,0),
		null cd_atividade_prestada,
		'S' ie_servico_municipio,
		b.ie_tipo_nota,
		decode(b.nr_seq_pgto_prest,null,'P','CP') ie_origem_doc,
		nvl(a.vl_tributo,0),
		nvl(a.vl_trib_nao_retido,0),
		null
	from	tributo			c,
		nota_fiscal_trib	a,
		nota_fiscal		b
	where	b.nr_sequencia			= a.nr_sequencia
	and	a.cd_tributo			= c.cd_tributo
	and	c.ie_tipo_tributo		= 'ISS'
	and	a.vl_base_calculo > 0
	and	trunc(b.dt_emissao,'month')	= trunc(dt_mes_referencia_w,'month')
	and	b.cd_estabelecimento		= cd_estabelecimento_p
	and	not exists	(select	1
				from	pls_tomador_prestador_giss x
				where	x.nr_sequencia_nf = b.nr_sequencia
				and	x.nr_seq_lote	<> nr_seq_lote_p)
	and	(exists		(select	1
				from	pls_envio_lote_giss_trib z
				where	z.nr_seq_lote = nr_seq_lote_p
				and	c.cd_tributo  = z.cd_tributo)
	or	qt_tributo_w = 0)
	union all
	/* T�tulos de fatura de interc�mbio - Sem nota */
	select	null,
		e.nr_titulo,
		e.cd_pessoa_fisica,
		e.cd_cgc,
		e.dt_emissao,
		to_char(e.nr_documento),
		to_char(e.nr_documento),
		e.cd_serie,
		nvl(e.vl_titulo,0),
		nvl(d.vl_base_calculo,0),
		null cd_atividade_prestada,
		'S' ie_servico_municipio,
		'TT' ie_tipo_nota,
		decode(e.nr_seq_ptu_fatura,null,decode((select 1 from pls_pag_prest_vencimento x where x.nr_titulo = e.nr_titulo),null,'CP','P'),'I') ie_origem_doc,
		nvl(d.vl_tributo,0),
		nvl(d.vl_trib_nao_retido,0),
		null
	from	ptu_fatura		g,
		tributo			f,
		titulo_receber_trib	d,
		titulo_receber		e
	where	e.nr_titulo			= d.nr_titulo
	and	d.cd_tributo			= f.cd_tributo
	and	e.nr_seq_ptu_fatura		= g.nr_sequencia
	and	f.ie_tipo_tributo		= 'ISS'
	and	d.vl_base_calculo > 0
	and	trunc(e.dt_emissao,'month')	= trunc(dt_mes_referencia_w,'month')
	and	e.cd_estabelecimento		= cd_estabelecimento_p
	and	not exists	(select	1
				from	pls_tomador_prestador_giss x
				where	x.nr_titulo_receber 	= e.nr_titulo
				and	x.nr_seq_lote		<> nr_seq_lote_p)
	and	(exists		(select	1
				from	pls_envio_lote_giss_trib z
				where	z.nr_seq_lote = nr_seq_lote_p
				and	f.cd_tributo  = z.cd_tributo)
	or	qt_tributo_w = 0)
	union all
	/* T�tulos de fatura de interc�mbio - Sem nota */
	select	null,
		null,--e.nr_titulo,
		e.cd_pessoa_fisica,
		e.cd_cgc,
		e.dt_emissao,
		to_char(e.nr_documento),
		to_char(e.nr_documento),
		null,--e.cd_serie,
		nvl(e.vl_titulo,0),
		nvl(d.vl_base_calculo,0),
		null cd_atividade_prestada,
		'S' ie_servico_municipio,
		'TT' ie_tipo_nota,
		'I' ie_origem_doc,
		nvl(d.vl_imposto,0),
		nvl(d.vl_nao_retido,0),
		e.nr_titulo
	from	ptu_fatura		g,
		tributo			f,
		titulo_pagar_imposto	d,
		titulo_pagar		e
	where	e.nr_titulo			= d.nr_titulo
	and	d.cd_tributo			= f.cd_tributo
	and	e.nr_titulo			= g.nr_titulo
	and	f.ie_tipo_tributo		= 'ISS'
	and	d.vl_base_calculo > 0
	and	trunc(e.dt_emissao,'month')	= trunc(dt_mes_referencia_w,'month')
	and	e.cd_estabelecimento		= cd_estabelecimento_p
	and	not exists	(select	1
				from	pls_tomador_prestador_giss x
				where	x.nr_titulo_receber 	= e.nr_titulo
				and	x.nr_seq_lote		<> nr_seq_lote_p)
	and	(exists		(select	1
				from	pls_envio_lote_giss_trib z
				where	z.nr_seq_lote = nr_seq_lote_p
				and	f.cd_tributo  = z.cd_tributo)
	or	qt_tributo_w = 0)
	union all
	select	null,
		null,--e.nr_titulo,
		e.cd_pessoa_fisica,
		e.cd_cgc,
		e.dt_emissao,
		to_char(e.nr_documento),
		to_char(e.nr_documento),
		null,--e.cd_serie,
		nvl(e.vl_titulo,0),
		nvl(e.vl_titulo,0),
		null cd_atividade_prestada,
		'S' ie_servico_municipio,
		'TT' ie_tipo_nota,
		'I' ie_origem_doc,
		0,
		0,
		e.nr_titulo
	from	ptu_fatura		g,
		titulo_pagar		e
	where	e.nr_titulo			= g.nr_titulo
	and	not exists	(select	1
				from	titulo_pagar_imposto	d
				where	e.nr_titulo			= d.nr_titulo)
	and	trunc(e.dt_emissao,'month')	= trunc(dt_mes_referencia_w,'month')
	and	e.cd_estabelecimento		= cd_estabelecimento_p
	and	not exists	(select	1
				from	pls_tomador_prestador_giss x
				where	x.nr_titulo_receber 	= e.nr_titulo
				and	x.nr_seq_lote		<> nr_seq_lote_p)
	union all
	select	null,
		null,--e.nr_titulo,
		e.cd_pessoa_fisica,
		e.cd_cgc,
		e.dt_emissao,
		to_char(e.nr_documento),
		to_char(e.nr_documento),
		null,--e.cd_serie,
		nvl(e.vl_titulo,0),
		nvl(e.vl_titulo,0),
		null cd_atividade_prestada,
		'S' ie_servico_municipio,
		'TT' ie_tipo_nota,
		'P' ie_origem_doc,
		0,
		0,
		e.nr_titulo
	from	titulo_pagar		e
	where	e.ie_origem_titulo	= '20'
	and	not exists	(select	1
				from	titulo_pagar_imposto	d
				where	e.nr_titulo			= d.nr_titulo)
	and	trunc(e.dt_emissao,'month')	= trunc(dt_mes_referencia_w,'month')
	and	e.cd_estabelecimento		= cd_estabelecimento_p
	and	not exists	(select	1
				from	pls_tomador_prestador_giss x
				where	x.nr_titulo_receber 	= e.nr_titulo
				and	x.nr_seq_lote		<> nr_seq_lote_p)
	order by
		1;

begin
if	(nr_seq_lote_p is not null) then
	if	(upper(ie_opcao_p) = 'G') then
		select	nvl(ie_tipo_lote,'A'),
			nvl(ie_origem_doc,'T'),
			nvl(ie_cooperado, 'A'),
			nvl(ie_retencao,'T'),
			nvl(ie_estabelecido,'T'),
			nvl(ie_inscricao_mun,'T')
		into	ie_tipo_lote_w,
			ie_origem_doc_lote_w,
			ie_cooperado_w,
			ie_retencao_w,
			ie_estabelecido_w,
			ie_inscricao_mun_w
		from	pls_lote_envio_giss
		where	nr_sequencia	= nr_seq_lote_p;
	
		select	count(1)
		into	qt_tributo_w
		from	pls_envio_lote_giss_trib
		where	nr_seq_lote	= nr_seq_lote_p
		and	rownum		= 1;
	
		select	max(dt_mes_referencia)
		into	dt_mes_referencia_w
		from	pls_lote_envio_giss
		where	nr_sequencia	= nr_seq_lote_p;
		
		select	max(a.cd_municipio_ibge)
		into	cd_municipio_ibge_w
		from	estabelecimento		b,
			pessoa_juridica		a
		where	a.cd_cgc		= b.cd_cgc
		and	b.cd_estabelecimento	= cd_estabelecimento_p;
		
		ie_servico_municipio_w		:= null;
		ie_prestador_municipio_w	:= 'N';
		
		open C01;
		loop
		fetch C01 into	
			nr_sequencia_nf_w,
			nr_titulo_w,
			cd_pessoa_fisica_w,
			cd_cgc_w,
			dt_emissao_nf_w,
			nr_nf_inicial_w,
			nr_nf_final_w,
			cd_serie_nf_w,
			vl_nota_fiscal_w,
			vl_base_calculo_w,
			cd_atividade_prestada_w,
			ie_servico_municipio_w,
			ie_tipo_nota_w,
			ie_origem_doc_w,
			vl_tributo_w,
			vl_trib_nao_retido_w,
			nr_titulo_pagar_w;
		exit when C01%notfound;
			begin
			ie_gera_w	:= 'S';
			
			if	(ie_origem_doc_lote_w = 'T') or
				(ie_origem_doc_lote_w = ie_origem_doc_w) then
				
				if	(ie_tipo_nota_w in ('EF','EN','EP')) then -- Tomador
					ie_prestador_tomador_w	:= 'T';
					cd_indicador_w		:= 'T';
				elsif	(ie_tipo_nota_w in ('SD','SE','SF','ST')) then -- Prestador
					ie_prestador_tomador_w	:= 'P';
					cd_indicador_w		:= 'C'; 
				end if;
				
				if	(ie_tipo_nota_w = 'TT') then
					if	(nr_titulo_pagar_w is not null) then
						ie_prestador_tomador_w	:= 'T';
					else
						ie_prestador_tomador_w	:= 'P';
					end if;
				end if;
				
				-- Tipo do lote for igual ao Prestador ou Tomador, ou tipo do lote permitir ambos
				if	(ie_tipo_lote_w = ie_prestador_tomador_w) or (ie_tipo_lote_w = 'A') then			
					if	(ie_cooperado_w <> 'A') then
						select	sum(qt_cooperado)
						into	qt_cooperado_w
						from	(select	count(1) qt_cooperado
							from	pls_cooperado	x
							where	x.cd_pessoa_fisica	= cd_pessoa_fisica_w
							and	dt_mes_referencia_w between trunc(nvl(x.dt_inclusao,dt_mes_referencia_w)) and trunc(nvl(x.dt_exclusao,dt_mes_referencia_w))
							union all
							select	count(1) qt_cooperado
							from	pls_cooperado	x
							where	cd_cgc	= cd_cgc_w
							and	dt_mes_referencia_w between trunc(nvl(x.dt_inclusao,dt_mes_referencia_w)) and trunc(nvl(x.dt_exclusao,dt_mes_referencia_w)));
						
						ie_gera_w	:= 'N';
						
						if	(nvl(qt_cooperado_w,0) > 0) and
							(ie_cooperado_w = 'C') then
							ie_gera_w	:= 'S';
						elsif	(nvl(qt_cooperado_w,0) = 0) and
							(ie_cooperado_w = 'N') then
							ie_gera_w	:= 'S';
						end if;
					end if;
				
					if	(ie_gera_w = 'S') then
						if	(cd_pessoa_fisica_w is not null) then
							select	max(cd_municipio_ibge)
							into	cd_municipio_ibge_ww
							from	compl_pessoa_fisica
							where	cd_pessoa_fisica	= cd_pessoa_fisica_w
							and	cd_municipio_ibge	= cd_municipio_ibge_w;
							
							select	max(nr_codigo_serv_prest)
							into	cd_atividade_prestada_w
							from	pessoa_fisica
							where	cd_pessoa_fisica = cd_pessoa_fisica_w;
							
							if	(ie_tipo_nota_w in ('EF','SF')) then
								nr_layout_w	:= 1; 
							end if;
						elsif	(cd_cgc_w is not null) then
							select	max(cd_municipio_ibge)
							into	cd_municipio_ibge_ww
							from	pessoa_juridica
							where	cd_cgc			= cd_cgc_w
							and	cd_municipio_ibge	= cd_municipio_ibge_w;
							
							select	max(a.nr_inscricao_municipal)
							into	nr_inscricao_municipal_w
							from	pessoa_juridica	a
							where	a.cd_cgc			= cd_cgc_w;
							
							select	max(nr_codigo_serv_prest)
							into	cd_atividade_prestada_w
							from	pessoa_juridica_estab
							where	cd_cgc = cd_cgc_w;
							
							if	(ie_tipo_nota_w in ('EN','EP')) and 
								(ie_prestador_tomador_w = 'T') then
								nr_layout_w	:= 2; 
							elsif	(ie_tipo_nota_w in ('SD','SE','ST')) and
								(ie_prestador_tomador_w = 'P') then
								nr_layout_w	:= 3; 
							end if;
						end if;
						
						if	(ie_prestador_tomador_w is null) and
							(nr_titulo_pagar_w is not null) then
							ie_prestador_tomador_w	:= 'P';
						end if;
						
						-- Sem nota fiscal
						if	(ie_tipo_nota_w = 'TT') then
							cd_indicador_w	:= 'P';
							nr_layout_w	:= '2';				
						end if;
						
						if	(cd_municipio_ibge_ww is not null) then
							ie_prestador_municipio_w := 'S';
						end if;
						
						if	(ie_origem_doc_w = 'I') then
							cd_indicador_w		:= 'T';
							nr_layout_w		:= 1;
							cd_serie_nf_w		:= 'A';
							cd_atividade_prestada_w	:= '04.23.00';
						end if;
						
						ie_gera_giss_w	:= 'S';
						
						if	((ie_retencao_w	=  'S') and
							(vl_tributo_w	= 0 ))  then
							ie_gera_giss_w	:= 'N';
						elsif   ((ie_retencao_w	=  'N')	and
							(vl_tributo_w	!= 0))	then
							ie_gera_giss_w	:= 'N';	
						end if;
						
						if	(ie_estabelecido_w = 'E') and
							(ie_prestador_municipio_w = 'N') then
							ie_gera_giss_w	:= 'N';	
						elsif	(ie_estabelecido_w = 'N') and
							(ie_prestador_municipio_w = 'S') then
							ie_gera_giss_w	:= 'N';	
						end if;
						
						if	(cd_cgc_w is not null) then
							if	(ie_inscricao_mun_w = 'I') and
								(nr_inscricao_municipal_w is null) then
								ie_gera_giss_w	:= 'N';	
							elsif	(ie_inscricao_mun_w = 'N') and
								(nr_inscricao_municipal_w is not null) then
								ie_gera_giss_w	:= 'N';	
							end if;
						end if;
						
						if	(cd_pessoa_fisica_w is not null) and
							(ie_inscricao_mun_w = 'I') then
							ie_gera_giss_w	:= 'N';	
						end if;
							
						if	(ie_gera_giss_w = 'S') then
							begin
							insert	into pls_tomador_prestador_giss
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_lote,
								cd_pessoa_fisica,
								cd_cgc,
								nr_layout,
								ie_prestador_tomador,
								dt_emissao_nf,
								nr_nf_inicial,
								nr_nf_final,
								cd_serie_nf,
								vl_nota_fiscal,
								vl_base_calculo,
								cd_atividade_prestada,
								ie_prestador_municipio,
								ie_servico_municipio,
								cd_indicador,
								ie_origem_doc,
								nr_sequencia_nf,
								nr_titulo_receber,
								ie_envio,
								vl_tributo,
								vl_trib_nao_retido,
								nr_titulo_pagar)
							values	(pls_tomador_prestador_giss_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_lote_p,
								cd_pessoa_fisica_w,
								cd_cgc_w,
								nr_layout_w,
								ie_prestador_tomador_w,
								dt_emissao_nf_w,
								nr_nf_inicial_w,
								nr_nf_final_w,
								cd_serie_nf_w,
								vl_nota_fiscal_w,
								vl_base_calculo_w,
								cd_atividade_prestada_w,
								ie_prestador_municipio_w,
								ie_servico_municipio_w,
								cd_indicador_w,
								ie_origem_doc_w,
								nr_sequencia_nf_w,
								nr_titulo_w,
								'S',
								vl_tributo_w,
								vl_trib_nao_retido_w,
								nr_titulo_pagar_w);
							end;	
						end if;
					end if;
				end if;
			end if;
			end;
		end loop;
		close C01;
		
		update	pls_lote_envio_giss
		set	dt_geracao_lote = sysdate
		where	nr_sequencia	= nr_seq_lote_p;
		
	elsif	(upper(ie_opcao_p) = 'D') then
		delete
		from	pls_tomador_prestador_giss
		where	nr_seq_lote	= nr_seq_lote_p;
	
		update	pls_lote_envio_giss
		set	dt_geracao_lote	= null
		where	nr_sequencia	= nr_seq_lote_p;
	end if;
	
commit;

end if;

end pls_gerar_lote_giss;
/