create or replace
procedure transf_consig_proprio_emp(	nr_emprestimo_p		number,
				nr_seq_item_p		number,
				nm_usuario_p		varchar2, 
				dt_atendimento_p		date) is
				
/* Procedure utilizada na fun��o Administra��o de Estoques
para o processo de empr�stimo, onde o local de estoque que ir� atender o empr�stimo
possui somente saldo consignado do item solicitado. Para permitir atender
o empr�stimo, o saldo consignado deve ser transformado em saldo pr�prio.*/

dt_atualizacao_w			date := sysdate;
nr_movimento_estoque_w		number(10,0);
cd_acao_w			varchar2(1) := '1';
ie_origem_documento_w		varchar2(1) := '3';
cd_estabelecimento_w		number(3,0) := 0;
cd_operacao_cons_consignado_w	number(4) := 0;
cd_setor_atendimento_w		number(5);
cd_setor_atendimento_dest_w	number(5);
cd_cgc_fornec_w			pessoa_juridica.cd_cgc%type;
ie_gera_ordem_w			varchar2(1);
cd_local_entrega_w		local_estoque.cd_local_estoque%type;
cd_centro_custo_w			number(15,0);
pr_desconto_w			number(15,0);
nm_usuario_dest_w			varchar2(255);
cd_perfil_comunic_w		number(5);
cd_pessoa_solicitante_w		varchar2(10);
ie_gera_oc_reposicao_w		varchar2(1);
cd_comprador_consig_w		parametro_compras.cd_comprador_consig%type;
ie_solicitante_oc_consig_w		parametro_compras.ie_solicitante_oc_consig%type;
cd_comprador_padrao_w		parametro_compras.cd_comprador_padrao%type;
qt_compra_w			ordem_compra_item.qt_material%type;
qt_conv_compra_estoque_w		material.qt_conv_compra_estoque%type;
qt_conv_estoque_consumo_w	material.qt_conv_estoque_consumo%type;
cd_unidade_medida_compra_w	material.cd_unidade_medida_compra%type;
ds_material_w			material.ds_material%type;
nr_ordem_compra_gerada_w		ordem_compra_item.nr_ordem_compra%type;
nr_item_oci_gerado_w		ordem_compra_item.nr_item_oci%type;
lista_usuario_w			varchar2(2000);
ie_pos_virgula_w			number(10);
ds_email_usuario_w			varchar2(255);
ds_lista_email_usuario_w		varchar2(2000);
tam_lista_w			number(10);
ds_comunicacao_w			varchar2(2000);
nm_usuario_w			usuario.nm_usuario%type;
cd_operacao_entrada_w		operacao_estoque.cd_operacao_estoque%type;
cd_material_estoque_w		material.cd_material_estoque%type;
vl_movimento_w			movimento_estoque_valor.vl_movimento%type;
cd_material_w			material.cd_material%type;
cd_unidade_medida_w		material.cd_unidade_medida_estoque%type;
qt_saldo_proprio_w			number(15,4);
qt_saldo_consig_w			number(15,4);
qt_saldo_entrega_w		emprestimo_material.qt_emprestimo%type;
qt_transferir_w			number(15,4);
qt_saldo_consig_fornec_w		number(15,4);
qt_movimento_w			number(15,4);
cd_local_estoque_w		local_estoque.cd_local_estoque%type;

begin

begin
select	cd_estabelecimento,
	cd_local_estoque
into	cd_estabelecimento_w,
	cd_local_estoque_w
from	emprestimo
where	nr_emprestimo = nr_emprestimo_p;
exception
  	when no_data_found then
   	-- N�o encontrado estabelecimento;
	wheb_mensagem_pck.exibir_mensagem_abort(261633);
end;

begin
select	cd_operacao_cons_consignado
into	cd_operacao_cons_consignado_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_w;
exception
	when others then
	-- N�o encontrado oper transf paciente;
	wheb_mensagem_pck.exibir_mensagem_abort(261634);
end;

if	(cd_operacao_cons_consignado_w is null) then
	-- N�o encontrado oper transf paciente;
	wheb_mensagem_pck.exibir_mensagem_abort(261634);
end if;

begin
select	b.cd_setor_atendimento
into	cd_setor_atendimento_w
from	local_estoque a,
	setor_atendimento b
where	a.cd_centro_custo = b.cd_centro_custo
and	a.cd_local_estoque = cd_local_estoque_w
and	rownum = 1;
exception
	when others then
	-- N�o encontrado setor atendimento;
	wheb_mensagem_pck.exibir_mensagem_abort(261635);
end;

begin
select	b.cd_setor_atendimento
into	cd_setor_atendimento_dest_w
from	local_estoque a,
	setor_atendimento b
where	a.cd_centro_custo  = b.cd_centro_custo
and	a.cd_local_estoque = cd_local_estoque_w
and	rownum = 1;
exception
	when others then
	-- N�o encontrado setor atendimento destino;
	wheb_mensagem_pck.exibir_mensagem_abort(261636);
end;

/* Tratar consumo de consignados  */
select	nvl(max(ie_solicitante_oc_consig), 'R'),
	max(cd_comprador_consig), 
	max(cd_comprador_padrao)
into	ie_solicitante_oc_consig_w,
	cd_comprador_consig_w,
	cd_comprador_padrao_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;
	
select	cd_material,
	substr(obter_dados_material(cd_material,'UME'),1,30) cd_unidade_medida_estoque,
	qt_emprestimo
into	cd_material_w,
	cd_unidade_medida_w,
	qt_saldo_entrega_w
from	emprestimo_material
where	nr_emprestimo = nr_emprestimo_p
and	nr_sequencia = nr_seq_item_p;	

begin
select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMC'),1,30) cd_unidade_medida_compra,
	ds_material,
	qt_conv_compra_estoque,
	qt_conv_estoque_consumo
into	cd_unidade_medida_compra_w,
	ds_material_w,
	qt_conv_compra_estoque_w,
	qt_conv_estoque_consumo_w
from	material
where	cd_material = cd_material_w;
exception
	when others then
     	/*(-20011,'Falta indicador baixa e/ou min. mult. solic do material');*/
	wheb_mensagem_pck.exibir_mensagem_abort(196049);
end;

/* Retorna a quantidade do material em estoque pr�prio */
qt_saldo_proprio_w := obter_saldo_disp_estoque(cd_estabelecimento_w, cd_material_w, cd_local_estoque_w, null);

/* Calcula a quantidade necess�ria a ser transferida para saldo pr�prio para atender a solicita��o de transfer�ncia */
qt_transferir_w := qt_saldo_entrega_w - qt_saldo_proprio_w;

/* Se a quantidade a transferir for menor ou igual a zero, quer dizer que h� quantidade em estoque pr�rpio suficiente para atender a solicita��o de transfer�ncia */
if	(qt_transferir_w > 0) then
	begin
	/* Retorna a quantidade do material em estoque consignado */
	qt_saldo_consig_w := obter_saldo_disp_consig(cd_estabelecimento_w, null, cd_material_w, cd_local_estoque_w, null);
	
	/* Se a quantidade a transferir para saldo pr�prio for maior que a quantidade em estoque consignado,
	ent�o a quantidade a transferir assume a quantidade de consignados em estoque,
	para que sejam transferidos todos os consignados dispon�veis naquele local para saldo pr�prio */
	if	(qt_transferir_w > qt_saldo_consig_w) then
		qt_transferir_w := qt_saldo_consig_w;
	end if;
	
	/* Loop para realizar a busca do fornecedor e realizar a transferencia de saldo consignado para pr�prio enquanto houver saldo consignado dispon�vel
	ou at� que seja transferida para saldo pr�prio toda a quantidade necess�ria para atendimento da solicita��o de transfer�ncia de estabelecimento */
	while	(qt_transferir_w > 0) loop
		begin
		/* Retorna o c�digo do fornecedor conforme regra */
		cd_cgc_fornec_w	:= substr(obter_fornecedor_regra_consig(cd_estabelecimento_w, cd_material_w, '1', cd_local_estoque_w),1,14);
		
		/* Retorna a quantidade de material consignado dispon�vel do fornecedor obtido */
		qt_saldo_consig_fornec_w := obter_saldo_disp_consig(cd_estabelecimento_w, cd_cgc_fornec_w, cd_material_w, cd_local_estoque_w, null);
		
		/* Se a quantidade a transferir para o saldo pr�prio for maior que o saldo consignado dispon�vel para o fornecedor,
		ent�o a quantidade a ser transferida ser� o total dispon�vel para o fornecedor.
		Caso contr�rio, ser� transferida a quantidade necess�ria para atendimento da solicita��o de transfer�ncia de estabelecimento */
		if	(qt_transferir_w > qt_saldo_consig_fornec_w) then
			qt_movimento_w := qt_saldo_consig_fornec_w;
		else
			qt_movimento_w := qt_transferir_w;
		end if;
		
		/* Caso a quantidade a ser movimentada n�o for maior que zero, significa que o fornecedor n�o possui saldo. Nesse caso,
		a regra de fornecedor consignado do cliente deve estar para 'Mostrar todos os fornecedores'. Nesse caso sempre trar� o primeiro fornecedor.
		Assim, para n�o entrar em loop infinito, no else � finalizado o loop. */
		if	(qt_movimento_w > 0) then
			begin		

			obter_regra_ordem_consignado
				(cd_estabelecimento_w,
				cd_local_estoque_w,
				cd_operacao_cons_consignado_w,
				cd_material_w,
				cd_cgc_fornec_w,
				0,
				cd_setor_atendimento_w,
				ie_gera_ordem_w,
				cd_local_entrega_w,
				cd_centro_custo_w,
				pr_desconto_w,
				nm_usuario_dest_w,
				cd_perfil_comunic_w,
				cd_pessoa_solicitante_w,
				ie_gera_oc_reposicao_w);

			/* Solicitante do consumo */
			if	(ie_solicitante_oc_consig_w = 'C') then
				cd_pessoa_solicitante_w	:= obter_pessoa_fisica_usuario(nm_usuario_p,'C');
			end if;

			ds_comunicacao_w := wheb_mensagem_pck.get_texto(301938,	'cd_material_p='||cd_material_w||
										';ds_material_w='||ds_material_w||
										';qt_movimento_p='||campo_mascara_virgula(qt_movimento_w)||
										';cd_acao_w='||cd_acao_w||
										';nm_fornecedor_w='||cd_cgc_fornec_w);

			if	(ie_gera_ordem_w = 'N') and 
				(ie_gera_oc_reposicao_w = 'S') then
				
				qt_compra_w	:= (qt_movimento_w / qt_conv_compra_estoque_w / qt_conv_estoque_consumo_w);
				
				gerar_ordem_reposicao_consig(
					cd_estabelecimento_w,
					cd_cgc_fornec_w,
					cd_material_w,
					qt_movimento_w,
					dt_atendimento_p,
					nm_usuario_p,
					null,--nr_prescricao_p,
					null,--nr_seq_prescricao_p,
					null,--nr_atendimento_p,
					cd_local_estoque_w,
					null,
					cd_local_estoque_w,
					cd_centro_custo_w,
					pr_desconto_w,
					null,
					cd_pessoa_solicitante_w,
					null,
					cd_setor_atendimento_w,
					null,
					qt_compra_w,
					cd_unidade_medida_compra_w,
					nvl(cd_comprador_consig_w,cd_comprador_padrao_w));

			/* S = gera a ordem; A = gera a ordem + comunica��o interna; O = ordem + e-mail */
			elsif	(ie_gera_ordem_w in ('S','A','O')) then

				gerar_ordem_compra_consig(
					cd_estabelecimento_w,
					cd_cgc_fornec_w,
					cd_material_w,
					qt_movimento_w,
					dt_atendimento_p,
					nm_usuario_p,
					null,--nr_prescricao_p,
					null,--nr_seq_prescricao_p,
					null,--nr_atendimento_p,
					cd_local_estoque_w,
					null,
					cd_local_entrega_w,
					cd_centro_custo_w,
					pr_desconto_w,
					null,
					cd_pessoa_solicitante_w,
					null,
					cd_setor_atendimento_w,
					ie_gera_oc_reposicao_w,
					null,
					nr_ordem_compra_gerada_w,
					nr_item_oci_gerado_w);
				
				/* Para enviar a comunica��o interna quando a op��o 'A' - ordem + comunica��o */
				if	(ie_gera_ordem_w = 'A') then
					gerar_comunic_consignado(nm_usuario_p,nm_usuario_dest_w,ds_comunicacao_w,cd_perfil_comunic_w);
				end if;
				
				/* Para enviar e-mail quando a op��o 'O' = ordem + e-mail */
				if	(ie_gera_ordem_w = 'O') then
					lista_usuario_w	:= substr(nm_usuario_dest_w,1,2000);
					
					while	(lista_usuario_w is not null) and
						(trim(lista_usuario_w) <> ',') loop
						
						tam_lista_w := length(lista_usuario_w);
						ie_pos_virgula_w := instr(lista_usuario_w,',');
						
						if	(ie_pos_virgula_w <> 0) then
						
							nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
							lista_usuario_w	:= substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w);
							
							select	trim(max(ds_email))
							into	ds_email_usuario_w
							from	usuario
							where	nm_usuario = nm_usuario_w;
							
							if	(nvl(ds_email_usuario_w,'X') <> 'X') then
								ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end if;
						elsif	(tam_lista_w > 1) then
							nm_usuario_w	:= lista_usuario_w;
							lista_usuario_w	:= null;
							
							select	trim(max(ds_email))
							into	ds_email_usuario_w
							from	usuario
							where	nm_usuario = nm_usuario_w;
							
							if	(nvl(ds_email_usuario_w,'X') <> 'X') then
								begin
								ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
								end;
							end if;
						else
							lista_usuario_w	:= null;
						end if;
					end loop;
					
					if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
						begin
						enviar_email(wheb_mensagem_pck.get_texto(301943),ds_comunicacao_w, null, ds_lista_email_usuario_w, nm_usuario_p,'M');
						exception
						when others then
							null;
						end;
					end if;
				end if;

			/* C = comunica��o, A = gera ordem e envia comunica��o */
			elsif	(ie_gera_ordem_w = 'C') and (cd_local_estoque_w is not null) then
				gerar_comunic_consignado(nm_usuario_p,
							nm_usuario_dest_w,
							wheb_mensagem_pck.get_texto(301944, 'cd_material_p='||cd_material_w||
											';ds_material_w='||ds_material_w||
											';qt_movimento_p='||campo_mascara_virgula(qt_movimento_w)||
											';cd_acao_w='||cd_acao_w),
							cd_perfil_comunic_w);
							
			/*'E' = e-mail */
			elsif	(ie_gera_ordem_w = 'E') and (cd_local_estoque_w is not null) then
				lista_usuario_w	:= substr(nm_usuario_dest_w,1,2000);
				
				while	(lista_usuario_w is not null) and
					(trim(lista_usuario_w) <> ',') loop
					tam_lista_w := length(lista_usuario_w);
					ie_pos_virgula_w := instr(lista_usuario_w,',');
					
					if	(ie_pos_virgula_w <> 0) then
						nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
						lista_usuario_w	:= substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w);
						
						select	trim(max(ds_email))
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;
						
						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w := substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					elsif	(tam_lista_w > 1) then
						nm_usuario_w	:= lista_usuario_w;
						lista_usuario_w	:= null;
						
						select	trim(max(ds_email))
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;
						
						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
						end if;
					else
						lista_usuario_w	:= null;
					end if;
				end loop;
				
				if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
					begin
					enviar_email(wheb_mensagem_pck.get_texto(301943),ds_comunicacao_w, null, ds_lista_email_usuario_w, nm_usuario_p,'M');
					exception
					when others then
						null;
					end;
				end if;
			end if;
			
			begin
			select	cd_operacao_estoque
			into	cd_operacao_entrada_w
			from	operacao_estoque
			where	ie_entrada_saida = 'E'
			and	ie_tipo_requisicao = 6
			and	ie_consignado = 0
			and	ie_altera_custo = 'S'
			and	ie_situacao = 'A'
			and	ie_atualiza_estoque = 'S'
			and	ie_consumo = 'N'
			and	rownum = 1
			order by cd_operacao_estoque;
			exception
			      when others then
				-- N�o encontrado operacao correspondente;
				wheb_mensagem_pck.exibir_mensagem_abort(261637);
			end;

			select movimento_estoque_seq.nextval
			into nr_movimento_estoque_w
			from dual;

			insert into movimento_estoque
				(nr_movimento_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				cd_operacao_estoque,
				cd_acao,
				cd_material,
				dt_mesano_referencia,
				qt_movimento,
				dt_atualizacao,
				nm_usuario,
				ie_origem_documento,
				nr_documento,
				nr_sequencia_item_docto,
				cd_unidade_medida_estoque,
				qt_estoque,
				cd_unidade_med_mov,
				cd_setor_atendimento,
				cd_fornecedor)
			values
				(nr_movimento_estoque_w,
				cd_estabelecimento_w,
				cd_local_estoque_w,
				dt_atendimento_p,
				cd_operacao_cons_consignado_w,
				cd_acao_w,
				cd_material_w,
				trunc(dt_atendimento_p,'mm'),
				qt_movimento_w,
				dt_atualizacao_w,
				nm_usuario_p,
				ie_origem_documento_w,
				nr_emprestimo_p,
				nr_seq_item_p,
				cd_unidade_medida_w,
				qt_movimento_w,
				cd_unidade_medida_w,
				cd_setor_atendimento_w,
				cd_cgc_fornec_w);

			select movimento_estoque_seq.nextval
			into nr_movimento_estoque_w
			from dual;

			insert into movimento_estoque
				(nr_movimento_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				cd_operacao_estoque,
				cd_acao,
				cd_material,
				dt_mesano_referencia,
				qt_movimento,
				dt_atualizacao,
				nm_usuario,
				ie_origem_documento,
				nr_documento,
				nr_sequencia_item_docto,
				cd_unidade_medida_estoque,
				qt_estoque,
				cd_unidade_med_mov,
				cd_setor_atendimento,
				cd_fornecedor)
			values	
				(nr_movimento_estoque_w,
				cd_estabelecimento_w,
				cd_local_estoque_w,
				dt_atendimento_p,
				cd_operacao_entrada_w,
				cd_acao_w,
				cd_material_w,
				trunc(dt_atendimento_p,'mm'),
				qt_movimento_w,
				dt_atualizacao_w,
				nm_usuario_p,
				ie_origem_documento_w,
				nr_emprestimo_p,
				nr_seq_item_p,
				cd_unidade_medida_w,
				qt_movimento_w,
				cd_unidade_medida_w,
				cd_setor_atendimento_dest_w,
				cd_cgc_fornec_w);
				
			vl_movimento_w := qt_movimento_w * obter_custo_medio_consig(cd_estabelecimento_w, cd_local_estoque_w, cd_material_w, cd_cgc_fornec_w, dt_atendimento_p);

			insert into movimento_estoque_valor(
				nr_movimento_estoque,
				cd_tipo_valor,
				vl_movimento,
				dt_atualizacao,
				nm_usuario)
			values(nr_movimento_estoque_w,
				1,
				vl_movimento_w,
				dt_atualizacao_w,
				nm_usuario_p);
				
			select	cd_material_estoque
			into	cd_material_estoque_w
			from	material
			where	cd_material = cd_material_w;

			if	(substr(sup_obter_metodo_valorizacao(trunc(dt_atendimento_p,'mm'), cd_estabelecimento_w),1,15) = 'MPM') then
				val_estoque_media_ponderada.val_prod_mat(trunc(dt_atendimento_p,'mm'), cd_estabelecimento_w, cd_material_estoque_w, nm_usuario_p);
			else
				val_mensal_estoque.val_est_prod_mat(trunc(dt_atendimento_p,'mm'), cd_estabelecimento_w, cd_material_estoque_w, nm_usuario_p);
			end if;	

			/* � retirada da quantidade a transferir para saldo pr�prio a quantidade j� transferida */
			qt_transferir_w := qt_transferir_w - qt_movimento_w;

			end;
		else
			exit;
		end if;		
		end;
	end loop;
	
	commit;
	end;
end if;

end transf_consig_proprio_emp;
/