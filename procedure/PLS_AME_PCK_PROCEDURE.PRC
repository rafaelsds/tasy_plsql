create or replace
procedure pls_ame_pck_procedure(	ie_opcao_p			varchar2,
					nr_seq_lote_rem_p		pls_ame_lote_remessa.nr_sequencia%type,
					arquivos_mensalidade_p		out varchar2,
					nm_procedure_p			varchar2,
					ds_linha_arquivo_p		varchar2,
					nr_seq_lote_retorno_p		number,
					ie_origem_geracao_p		varchar2,
					nr_seq_segurado_p		number,
					dt_rescisao_p			date,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is
/*
G - Gerar lote
D - Desfazer lote
C - Confirmar envio lote
A - Gerar arquivos
I - Importar arquivo
DI - Desfazer importa��o arquivo
GT - Gerar titulos lote
FI - Finaliza importa��o arquivo
AD  - Adicionar benefici�rio libera��o de d�bito
*/

begin

pls_ame_pck.set_nm_usuario(nm_usuario_p);
pls_ame_pck.set_cd_estabelecimento(cd_estabelecimento_p);

if (ie_opcao_p = 'G') then
	pls_ame_pck.inicia_processo_geracao_lote(nr_seq_lote_rem_p);
elsif (ie_opcao_p = 'D') then
	pls_ame_pck.desfazer_lote_remessa(nr_seq_lote_rem_p);
elsif (ie_opcao_p = 'C') then
	pls_ame_pck.confirmar_envio_lote_remessa(nr_seq_lote_rem_p);
elsif (ie_opcao_p = 'A') then
	pls_ame_pck.gerar_arquivos_lote_remessa(nr_seq_lote_rem_p,ie_origem_geracao_p, arquivos_mensalidade_p);
elsif (ie_opcao_p = 'I') then
	pls_ame_pck.importar_arquivo_retorno(nm_procedure_p, ds_linha_arquivo_p, nr_seq_lote_retorno_p);
elsif (ie_opcao_p = 'DI') then
	pls_ame_pck.desfazer_importacao_arquivo(nr_seq_lote_retorno_p);
elsif (ie_opcao_p = 'GT') then
	pls_ame_pck.gerar_titulos_lote(	nr_seq_lote_retorno_p);
elsif (ie_opcao_p = 'FI') then
	pls_ame_pck.finaliza_importacao_arquivo(nr_seq_lote_retorno_p);
elsif (ie_opcao_p = 'AD') then
	pls_ame_pck.adicionar_benef_lib_debito(nr_seq_lote_rem_p,nr_seq_segurado_p,dt_rescisao_p);
end if;

pls_ame_pck.set_nm_usuario('');
pls_ame_pck.set_cd_estabelecimento(null);

end pls_ame_pck_procedure;
/
