create or replace
procedure pls_copiar_regras_copartic
		(	nr_seq_proposta_p	number,
			nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Copiar as regras de coparticipacao do plano viculado ao beneficiario para a tabela de regras de coparticipacao da proposta.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 			
nr_seq_regra_coparticipacao_w	number(10);
qt_planos_proposta_w		number(10);
nr_seq_plano_w			number(10);
qt_registros_w			number(10);
nr_seq_tipo_coparticipacao_w	number(10);
nr_seq_regra_retorno_w		number(10);
nr_seq_regra_copartic_w		number(10);

-------------------------------------------------------------------------------------------
qt_eventos_minimo_w		pls_regra_coparticipacao.qt_eventos_minimo%type;
nr_seq_prestador_w		pls_regra_coparticipacao.nr_seq_prestador%type;
nr_seq_tipo_prestador_w		pls_regra_coparticipacao.nr_seq_tipo_prestador%type;
ie_tipo_data_consistencia_w	pls_regra_coparticipacao.ie_tipo_data_consistencia%type;
ie_forma_cobr_internacao_w	pls_regra_coparticipacao.ie_forma_cobr_internacao%type;
ie_titularidade_w		pls_regra_coparticipacao.ie_titularidade%type;
ie_tipo_parentesco_w		pls_regra_coparticipacao.ie_tipo_parentesco%type;
qt_ocorrencias_w		pls_regra_coparticipacao.qt_ocorrencias%type;
qt_ocorrencia_grupo_serv_w	pls_regra_coparticipacao.qt_ocorrencia_grupo_serv%type;
qt_periodo_ocor_w		pls_regra_coparticipacao.qt_periodo_ocor%type;
nr_seq_grupo_serv_w		pls_regra_coparticipacao.nr_seq_grupo_serv%type;
cd_procedimento_w		pls_regra_coparticipacao.cd_procedimento%type;
qt_idade_min_w			pls_regra_coparticipacao.qt_idade_min%type;
qt_idade_max_w			pls_regra_coparticipacao.qt_idade_max%type;

Cursor C01 is
	select	a.nr_seq_plano
	from	pls_proposta_plano	a
	where	a.nr_seq_proposta = nr_seq_proposta_p;
			
Cursor C02 is
	select	nr_sequencia,
		nr_seq_tipo_coparticipacao,
		nvl(qt_eventos_minimo,0),
		nvl(ie_tipo_data_consistencia,'N'),
		nvl(ie_forma_cobr_internacao,'C'),
		nvl(ie_titularidade,'A'),
		nvl(ie_tipo_parentesco,'X'),
		nvl(qt_ocorrencias,0),
		nvl(qt_ocorrencia_grupo_serv,0),
		nvl(qt_periodo_ocor,0),
		nvl(nr_seq_grupo_serv,0),
		nvl(cd_procedimento,0),
		nvl(qt_idade_min,0),
		nvl(qt_idade_max,0),
		nvl(NR_SEQ_PRESTADOR,0),
		nvl(NR_SEQ_TIPO_PRESTADOR,0)
	from	pls_regra_coparticipacao	a
	where	a.nr_seq_plano = nr_seq_plano_w
	and	a.nr_seq_contrato is null
	and	a.nr_seq_proposta is null;
	
Cursor C03 is
	select	nr_sequencia
	from	pls_regra_copartic_retorno
	where	nr_seq_regra_copartic	= nr_seq_regra_coparticipacao_w;

begin

select	count(*)
into	qt_planos_proposta_w
from	pls_proposta_plano
where	nr_seq_proposta = nr_seq_proposta_p;

if	(qt_planos_proposta_w > 0) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_plano_w;
	exit when C01%notfound;
		begin
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_regra_coparticipacao_w,
			nr_seq_tipo_coparticipacao_w,
			qt_eventos_minimo_w,
			ie_tipo_data_consistencia_w,
			ie_forma_cobr_internacao_w,
			ie_titularidade_w,
			ie_tipo_parentesco_w,
			qt_ocorrencias_w,
			qt_ocorrencia_grupo_serv_w,
			qt_periodo_ocor_w,
			nr_seq_grupo_serv_w,
			cd_procedimento_w,
			qt_idade_min_w,
			qt_idade_max_w,
			NR_SEQ_PRESTADOR_w,
			NR_SEQ_TIPO_PRESTADOR_w;
		exit when C02%notfound;
			begin		
			
			select	count(1)
			into	qt_registros_w
			from	pls_regra_coparticipacao
			where	nr_seq_proposta				= nr_seq_proposta_p
			and	nr_seq_tipo_coparticipacao		= nr_seq_tipo_coparticipacao_w
			and	((nr_seq_plano				= nr_seq_plano_w) or (nr_seq_plano is null))
			and	nvl(qt_eventos_minimo,0)		= qt_eventos_minimo_w
			and	nvl(ie_tipo_data_consistencia,'N')	= ie_tipo_data_consistencia_w
			and	nvl(ie_forma_cobr_internacao,'C')	= ie_forma_cobr_internacao_w
			and	nvl(ie_titularidade,'A')		= ie_titularidade_w
			and	nvl(ie_tipo_parentesco,'X')		= ie_tipo_parentesco_w
			and	nvl(qt_ocorrencias,0)			= qt_ocorrencias_w
			and	nvl(qt_ocorrencia_grupo_serv,0)		= qt_ocorrencia_grupo_serv_w
			and	nvl(qt_periodo_ocor,0)			= qt_periodo_ocor_w
			and	nvl(nr_seq_grupo_serv,0)		= nr_seq_grupo_serv_w
			and	nvl(cd_procedimento,0)			= cd_procedimento_w
			and	nvl(QT_IDADE_MIN,0)			= QT_IDADE_MIN_w
			and	nvl(QT_IDADE_MAX,0)			= QT_IDADE_MAX_w
			and	nvl(NR_SEQ_PRESTADOR,0)			= NR_SEQ_PRESTADOR_w
			and	nvl(NR_SEQ_TIPO_PRESTADOR,0)		= NR_SEQ_TIPO_PRESTADOR_w
			and	rownum					= 1;
			
			if	(qt_registros_w	= 0) then
			
				select	pls_regra_coparticipacao_seq.nextval
				into	nr_seq_regra_copartic_w
				from	dual;
			
				insert into 	pls_regra_coparticipacao 
					(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, nr_seq_plano, tx_coparticipacao, nr_seq_tipo_coparticipacao,
						vl_maximo, ie_tipo_atendimento, ie_situacao, 
						vl_coparticipacao, dt_inicio_vigencia, dt_fim_vigencia, dt_contrato_de,
						dt_contrato_ate, qt_eventos_minimo, qt_meses_intervalo,
						qt_ocorrencias, ie_tipo_ocorrencia, ie_tipo_data_consistencia,
						ie_prestador_cooperado, qt_idade_min, qt_idade_max, qt_ocorrencia_grupo_serv,
						qt_periodo_ocor, ie_tipo_periodo_ocor, nr_seq_grupo_serv, nr_seq_regra_origem,
						ie_reajuste, ie_titularidade, cd_procedimento, ie_origem_proced,
						ie_tipo_parentesco, qt_diaria_inicial, qt_diaria_final, 
						nr_seq_prestador, nr_seq_tipo_prestador, nr_seq_proposta,
						ie_incidencia_valor_maximo,IE_PERIODO_VALOR_MAXIMO,IE_FORMA_COBR_INTERNACAO,IE_ANO_CALENDARIO_OUTRAS_OCOR,
						IE_CONSIDERA_OUTRA_OCOR_REGRA,ie_tipo_incidencia,
						ie_inc_demais_itens, vl_base_min, vl_base_max, ie_incidencia_psiquiatria)
				(	select	nr_seq_regra_copartic_w, sysdate, nm_usuario_p, sysdate,
						nm_usuario_p, nr_seq_plano, tx_coparticipacao, nr_seq_tipo_coparticipacao,
						vl_maximo, ie_tipo_atendimento, ie_situacao, 
						vl_coparticipacao, dt_inicio_vigencia, dt_fim_vigencia, dt_contrato_de,
						dt_contrato_ate, qt_eventos_minimo, qt_meses_intervalo,
						qt_ocorrencias, ie_tipo_ocorrencia, ie_tipo_data_consistencia,
						ie_prestador_cooperado, qt_idade_min, qt_idade_max, qt_ocorrencia_grupo_serv,
						qt_periodo_ocor, ie_tipo_periodo_ocor, nr_seq_grupo_serv, nr_seq_regra_origem,
						ie_reajuste, ie_titularidade, cd_procedimento, ie_origem_proced,
						ie_tipo_parentesco, qt_diaria_inicial, qt_diaria_final,
						nr_seq_prestador, nr_seq_tipo_prestador, nr_seq_proposta_p,
						ie_incidencia_valor_maximo,IE_PERIODO_VALOR_MAXIMO,IE_FORMA_COBR_INTERNACAO,IE_ANO_CALENDARIO_OUTRAS_OCOR,
						IE_CONSIDERA_OUTRA_OCOR_REGRA,nvl(ie_tipo_incidencia,'B'),
						ie_inc_demais_itens, vl_base_min, vl_base_max, ie_incidencia_psiquiatria
					from	pls_regra_coparticipacao
					where	nr_sequencia = nr_seq_regra_coparticipacao_w);
						
					open C03;
					loop
					fetch C03 into
						nr_seq_regra_retorno_w;
					exit when C03%notfound;
						begin
						insert	into	pls_regra_copartic_retorno
							(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
								nr_seq_regra_copartic, qt_dias_retorno)
							(select	pls_regra_copartic_retorno_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
								nr_seq_regra_copartic_w, qt_dias_retorno
							from	pls_regra_copartic_retorno
							where	nr_sequencia	= nr_seq_regra_retorno_w);
						end;
					end loop;
					close C03;
			end if;
					
			end;
		end loop;
		close C02;
		
		end;
	end loop;
	close C01;			
end if;
		
commit;

end pls_copiar_regras_copartic;
/
