create or replace procedure TRE_ESTORNAR_STATUS_DIA (dt_registro_p	Date,
					nr_seq_inscrito_p	Number,
					ie_status_agenda_p	Varchar,
					nm_usuario_p		varchar) is 
begin

update	tre_paciente_dia
set	ie_status_paciente = 'N',
	ds_motivo_status = decode(ie_status_paciente,'I',null,ds_motivo_status)
where	trunc(dt_registro) = trunc(dt_registro_p)
and	   	nr_seq_inscrito = nr_seq_inscrito_p ;

if	(ie_status_agenda_p in ('I','F')) then
	begin
	tre_aplica_regra_falta(dt_registro_p, nr_seq_inscrito_p, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
	end;
end if;

if	(ie_status_agenda_p in ('AD','E')) then
	begin
	update	tre_paciente_dia
	set	nr_atendimento = null
	where	trunc(dt_registro) = dt_registro_p
	and	nr_seq_inscrito = nr_seq_inscrito_p ;
	end;
end if;

commit;

end TRE_ESTORNAR_STATUS_DIA ;
/
