CREATE OR REPLACE
PROCEDURE hd_encerrar_escala_trat_pac(CD_SEQ_PACIENTE_P VARCHAR2) IS

    CURSOR c01(cd_seq_pacientes_p VARCHAR2) IS
        SELECT hed.nr_sequencia
          FROM hd_paciente_int_ext pie,
               hd_escala_dialise   hed
         WHERE pie.cd_pessoa_fisica = hed.cd_pessoa_fisica
           AND hed.dt_fim IS NULL
           AND obter_se_contido(pie.nr_sequencia, cd_seq_pacientes_p) = 'S';

BEGIN
    FOR i IN c01(cd_seq_pacientes_p => cd_seq_paciente_p) LOOP
        hd_encerrar_escala_tratamento(i.nr_sequencia);
    END LOOP;

    COMMIT;

END hd_encerrar_escala_trat_pac;
/
