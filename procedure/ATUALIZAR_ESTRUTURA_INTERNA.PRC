create or replace
procedure Atualizar_Estrutura_Interna(
			nm_usuario_p		Varchar2,
			cd_material_p		number,
			nr_seq_estrutura_p	number,
			dt_vigencia_p		date default sysdate,
			nr_sequencia_p		number default null,
			ie_tipo_brasindice_p	varchar2 default null) is 
			
qt_cadastro_w			number(10);
ie_atualiza_brasindice_w	varchar2(1);
nr_seq_mat_estrutura_w		number(10,0);
dt_inicio_vigencia_w		date;
ie_tipo_brasindice_w		varchar2(1);

begin

dt_inicio_vigencia_w:= nvl(dt_vigencia_p, sysdate);
ie_tipo_brasindice_w:= nvl(ie_tipo_brasindice_p,'R');

select	count(*)
into	qt_cadastro_w
from	mat_estrutura_cadastro
where	cd_material		= cd_material_p
and	nr_seq_estrutura	= nr_seq_estrutura_p;

if	(qt_cadastro_w = 0) then

	select 	mat_estrutura_cadastro_seq.nextval
	into	nr_seq_mat_estrutura_w
	from 	dual;

	insert into mat_estrutura_cadastro ( 
		cd_material,             
		dt_atualizacao,          
		dt_atualizacao_nrec,     
		nm_usuario,              
		nm_usuario_nrec,         
		nr_seq_estrutura,        
		nr_sequencia)
	values	(
		cd_material_p,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_estrutura_p,
		nr_seq_mat_estrutura_w);
		
	select 	nvl(max(ie_atualiza_brasindice),'N')
	into	ie_atualiza_brasindice_w
	from 	MAT_ESTRUTURA
	where	ie_situacao = 'A'
	and 	nr_sequencia = nr_seq_estrutura_p;
	
	if	(ie_atualiza_brasindice_w = 'S') then
		insert into mat_estrutura_cad_vig (
				NR_SEQUENCIA, NR_SEQ_MAT_ESTRUTURA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, DT_INICIO_VIGENCIA, 
				DT_FINAL_VIGENCIA, NR_SEQ_BRASINDICE, IE_TIPO_BRASINDICE)
			values (mat_estrutura_cad_vig_seq.NextVal, nr_seq_mat_estrutura_w , sysdate, nm_usuario_p, sysdate, nm_usuario_p, dt_inicio_vigencia_w, 
				null, nr_sequencia_p, ie_tipo_brasindice_w);
	end if;

end if;

commit;

end Atualizar_Estrutura_Interna;
/