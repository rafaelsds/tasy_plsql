CREATE OR REPLACE
PROCEDURE Consistir_data_SAE(	qt_horas_futura_p	number,
			dt_SAE_p	Date) IS 

BEGIN

if	((sysdate + qt_horas_futura_p / 24) < dt_SAE_p) then
	wheb_mensagem_pck.exibir_mensagem_abort(281986,'QT_HORAS_FUTURA='||qt_horas_futura_p);
end if;


END Consistir_data_SAE;
/
