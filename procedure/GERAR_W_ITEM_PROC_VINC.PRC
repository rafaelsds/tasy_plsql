create or replace
procedure gerar_w_item_proc_vinc(
			nr_prescricao_p		number,
			nr_seq_rotina_p		number,
			nm_usuario_p		Varchar2) is 

cd_categoria_w			number(10);
cd_convenio_w			number(10);
cd_estabelecimento_w	number(4);
cd_plano_w				atend_categoria_convenio.cd_plano_convenio%type;
cd_procedimento_w		number(15);
ie_origem_proc_filtro_w	number(10);
ie_origem_proced_w		number(10);
ie_sexo_prescr_w		varchar2(1);
ie_tipo_atendimento_w	number(3);
ie_tipo_convenio_w		number(2);
nr_atendimento_w		number(10);
nr_seq_atecaco_w		number(10);
nr_seq_item_w			number(10);
nr_seq_proc_interno_w	number(10);
qt_idade_pac_w			number(10);
qt_proced_w				number(8,3);
cd_medico_w				number(10);

cursor C01 is
Select  a.nr_sequencia,
	a.qt_proc_adic,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.nr_seq_proc_int_adic
FROM	proc_int_proc_prescr a
WHERE	a.nr_seq_proc_interno	= nr_seq_proc_interno_w
and 	nvl(a.cd_convenio,cd_convenio_w) = cd_convenio_w
and		((a.cd_estabelecimento is null) or (cd_estabelecimento = cd_estabelecimento_w))
and	nvl(a.ie_situacao,'A')	= 'A'
and	nvl(a.ie_somente_agenda_cir,'N') = 'N'
and 	((a.cd_convenio_excluir is null) or
	 (a.cd_convenio_excluir <> cd_convenio_w))
and	((a.cd_categoria_convenio is null) or
	 (a.cd_categoria_convenio = cd_categoria_w))
and	((a.cd_plano_convenio	= cd_plano_w) or
	 (a.cd_plano_convenio is null))
and	((a.cd_medico_prescritor is null) or
	 (a.cd_medico_prescritor = cd_medico_w))
and	((a.cd_medico_excluir is null) or
	 (a.cd_medico_excluir <> cd_medico_w))
and 	((a.ie_origem_proc_filtro = ie_origem_proc_filtro_w) or (a.ie_origem_proc_filtro is null))
and 	((nvl(a.ie_permite_duplicado,'N')	= 'S') or (not exists (
		Select	1
		from	prescr_procedimento b
		where	b.nr_prescricao	= nr_prescricao_p
		and	b.nr_seq_proc_interno	= a.nr_seq_proc_int_adic)))
and	(((qt_idade_pac_w is null) or (a.qt_idade_min is null and a.qt_idade_max is null)) or
	((qt_idade_pac_w is not null) and (qt_idade_pac_w between nvl(a.qt_idade_min,qt_idade_pac_w) and
	nvl(a.qt_idade_max,qt_idade_pac_w))))
and	((ie_sexo = ie_sexo_prescr_w) or
	(ie_sexo is null));
begin

Select	max(nr_proc_interno)
into	nr_seq_proc_interno_w
from	procedimento_rotina
where	nr_sequencia	= nr_seq_rotina_p;

if	(nr_seq_proc_interno_w is not null) then
	Select	max(a.nr_atendimento),
		max(obter_atecaco_atendimento(a.nr_atendimento)),
		max(a.cd_estabelecimento),
		max(cd_medico)
	into	nr_atendimento_w,
		nr_seq_atecaco_w,
		cd_estabelecimento_w,
		cd_medico_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;

	Select	nvl(max(c.cd_convenio),0),
		nvl(max(c.cd_categoria),0),
		nvl(max(c.cd_plano_convenio),0),
		nvl(max(Obter_Idade(p.dt_nascimento,nvl(p.dt_obito,sysdate), 'A')),0),
		max(a.ie_tipo_atendimento)
	into	cd_convenio_w,
		cd_categoria_w,
		cd_plano_w,
		qt_idade_pac_w,
		ie_tipo_atendimento_w
	from	Pessoa_fisica p,
		atend_categoria_convenio c,
		atendimento_paciente a
	where	a.cd_pessoa_fisica	= p.cd_pessoa_fisica
	and 	c.nr_Atendimento	= a.nr_atendimento
	and 	c.nr_Seq_interno	= nr_seq_atecaco_w
	and 	a.nr_Atendimento	= nr_atendimento_w;
	
	select 	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from 	convenio
	where 	cd_convenio = cd_convenio_w;
	
	ie_sexo_prescr_w := Obter_Sexo_Prescricao(nr_prescricao_p);
	ie_origem_proc_filtro_w:= Obter_Origem_Proced_Cat(cd_estabelecimento_w, ie_tipo_atendimento_w, ie_tipo_convenio_w, cd_convenio_w, cd_categoria_w);
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_item_w,
		qt_proced_w,
		cd_procedimento_w, 
		ie_origem_proced_w,
		nr_seq_proc_interno_w;
	exit when C01%notfound;
		begin
		Delete	from w_item_prescr
		where	nr_seq_item	= nr_seq_item_w
		and	nr_prescricao	= nr_prescricao_p;
		
		insert into w_item_prescr (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_prescricao,
			nr_seq_item,
			qt_dose,
			nr_seq_proc_interno,
			cd_procedimento,
			ie_origem_proced,
			cd_protocolo,
			ie_origem_inf)
		Values(w_item_prescr_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_prescricao_p,
			nr_seq_item_w,
			qt_proced_w,
			nr_seq_proc_interno_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_rotina_p,
			'PVPi');
		end;
	end loop;
	close C01;
end if;

commit;

end gerar_w_item_proc_vinc;
/
