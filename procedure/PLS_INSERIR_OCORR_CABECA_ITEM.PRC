create or replace
procedure pls_inserir_ocorr_cabeca_item
			(	nr_seq_requisicao_p	Number,
				nr_seq_req_proc_p	Number,
				nr_seq_req_mat_p	Number,
				nm_usuario_p		Varchar2) is

qt_reg_ocorr_w			Number(10);
qt_reg_glosa_w			Number(10);
qt_ocorrencia_w			Number(10);
qt_glosa_w			Number(10);

begin

select	count(1)
into	qt_reg_ocorr_w
from	pls_ocorrencia_benef
where	nr_seq_requisicao	= nr_seq_requisicao_p
and	nr_seq_proc		is null
and	nr_seq_mat		is null;

if	(qt_reg_ocorr_w	> 0) then
	if	(nvl(nr_seq_req_proc_p,0)	<> 0) then
		insert	into pls_ocorrencia_benef
			(nr_sequencia, nr_seq_segurado, nr_seq_ocorrencia,
			 dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			 nm_usuario_nrec, nr_seq_requisicao, nr_seq_conta,
			 nr_seq_proc, nr_seq_regra, nr_seq_mat,
			 nr_seq_guia_plano, dt_liberacao, nm_usuario_lib,
			 ie_auditoria, nr_nivel_liberacao, cd_mensagem_erro,
			 nr_seq_mot_liberacao, ds_observacao, ie_fechar_conta,
			 nr_seq_glosa, qt_glosa, vl_glosa,
			 nr_seq_execucao, ie_tipo_glosa, ie_pre_analise,
			 ie_documento_fisico, ds_documentacao, ie_situacao,
			 nr_seq_proc_partic, nr_seq_conta_pos_estab, ie_lib_manual,
			 nr_seq_glosa_guia, nr_seq_glosa_req, ie_finalizar_analise)
		(select	 pls_ocorrencia_benef_seq.NextVal, nr_seq_segurado, nr_seq_ocorrencia,
			 dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			 nm_usuario_nrec, nr_seq_requisicao, nr_seq_conta,
			 nr_seq_req_proc_p, nr_seq_regra, nr_seq_mat,
			 nr_seq_guia_plano, dt_liberacao, nm_usuario_lib,
			 ie_auditoria, nr_nivel_liberacao, cd_mensagem_erro,
			 nr_seq_mot_liberacao, ds_observacao, ie_fechar_conta,
			 nr_seq_glosa, qt_glosa, vl_glosa,
			 nr_seq_execucao, ie_tipo_glosa, ie_pre_analise,
			 ie_documento_fisico, ds_documentacao, ie_situacao,
			 nr_seq_proc_partic, nr_seq_conta_pos_estab, ie_lib_manual,
			 nr_seq_glosa_guia, nr_seq_glosa_req, ie_finalizar_analise
		from	pls_ocorrencia_benef
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_proc		is null
		and	nr_seq_mat		is null);

		select	count(1)
		into	qt_ocorrencia_w
		from	pls_ocorrencia		b,
			pls_ocorrencia_benef	a
		where	a.nr_seq_ocorrencia	= b.nr_sequencia
		and	a.nr_seq_requisicao	= nr_seq_requisicao_p
		and	a.nr_seq_proc 		is null
		and	a.nr_seq_mat 		is null
		and	b.nr_seq_motivo_glosa	is null
		and	b.ie_auditoria 		= 'S';

		if	(qt_ocorrencia_w	> 0) then
			update	pls_auditoria_item
			set	ie_status		= 'P',
				ie_status_solicitacao	= 'P'
			where	nr_seq_proc_origem	= nr_seq_req_proc_p;

			update	pls_requisicao_proc
			set	ie_status	= 'A'
			where	nr_sequencia	= nr_seq_req_proc_p;
		end if;
	elsif	(nvl(nr_seq_req_mat_p,0)	<> 0) then
		insert	into pls_ocorrencia_benef
			(nr_sequencia, nr_seq_segurado, nr_seq_ocorrencia,
			 dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			 nm_usuario_nrec, nr_seq_requisicao, nr_seq_conta,
			 nr_seq_proc, nr_seq_regra, nr_seq_mat,
			 nr_seq_guia_plano, dt_liberacao, nm_usuario_lib,
			 ie_auditoria, nr_nivel_liberacao, cd_mensagem_erro,
			 nr_seq_mot_liberacao, ds_observacao, ie_fechar_conta,
			 nr_seq_glosa, qt_glosa, vl_glosa,
			 nr_seq_execucao, ie_tipo_glosa, ie_pre_analise,
			 ie_documento_fisico, ds_documentacao, ie_situacao,
			 nr_seq_proc_partic, nr_seq_conta_pos_estab, ie_lib_manual,
			 nr_seq_glosa_guia, nr_seq_glosa_req, ie_finalizar_analise)
		(select	 pls_ocorrencia_benef_seq.NextVal, nr_seq_segurado, nr_seq_ocorrencia,
			 dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			 nm_usuario_nrec, nr_seq_requisicao, nr_seq_conta,
			 nr_seq_proc, nr_seq_regra, nr_seq_req_mat_p,
			 nr_seq_guia_plano, dt_liberacao, nm_usuario_lib,
			 ie_auditoria, nr_nivel_liberacao, cd_mensagem_erro,
			 nr_seq_mot_liberacao, ds_observacao, ie_fechar_conta,
			 nr_seq_glosa, qt_glosa, vl_glosa,
			 nr_seq_execucao, ie_tipo_glosa, ie_pre_analise,
			 ie_documento_fisico, ds_documentacao, ie_situacao,
			 nr_seq_proc_partic, nr_seq_conta_pos_estab, ie_lib_manual,
			 nr_seq_glosa_guia, nr_seq_glosa_req, ie_finalizar_analise
		from	pls_ocorrencia_benef
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_proc		is null
		and	nr_seq_mat		is null);

		select	count(1)
		into	qt_ocorrencia_w
		from	pls_ocorrencia		b,
			pls_ocorrencia_benef	a
		where	a.nr_seq_ocorrencia	= b.nr_sequencia
		and	a.nr_seq_requisicao	= nr_seq_requisicao_p
		and	a.nr_seq_proc 		is null
		and	a.nr_seq_mat 		is null
		and	b.nr_seq_motivo_glosa	is null
		and	b.ie_auditoria 		= 'S';

		if	(qt_ocorrencia_w	> 0) then
			update	pls_auditoria_item
			set	ie_status		= 'P',
				ie_status_solicitacao	= 'P'
			where	nr_seq_mat_origem	= nr_seq_req_mat_p;

			update	pls_requisicao_mat
			set	ie_status	= 'A'
			where	nr_sequencia	= nr_seq_req_mat_p;
		end if;
	end if;
end if;

select	count(1)
into	qt_reg_glosa_w
from	pls_requisicao_glosa
where	nr_seq_requisicao	= nr_seq_requisicao_p
and	nr_seq_req_proc		is null
and	nr_seq_req_mat		is null;

if	(qt_reg_glosa_w	> 0 ) then
	if	(nvl(nr_seq_req_proc_p,0)	<> 0) then
		insert	into pls_requisicao_glosa
			(nr_sequencia, nr_seq_motivo_glosa, dt_atualizacao,
			 nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			 nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			 nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			 nr_seq_exec_proc, nr_seq_exec_mat)
		(select	pls_requisicao_glosa_seq.NextVal, nr_seq_motivo_glosa, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_requisicao, ds_observacao, nr_seq_req_proc_p,
			nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			nr_seq_exec_proc, nr_seq_exec_mat
		from	pls_requisicao_glosa
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_req_proc		is null
		and	nr_seq_req_mat		is null);

		select	count(1)
		into	qt_glosa_w
		from	pls_requisicao_glosa
		where	nr_seq_requisicao  = nr_seq_requisicao_p
		and	nr_seq_ocorrencia is null;

		if	(qt_glosa_w = 0) then
			select	count(1)
			into	qt_glosa_w
			from	pls_ocorrencia		b,
				pls_ocorrencia_benef	a
			where	a.nr_seq_ocorrencia	= b.nr_sequencia
			and	a.nr_seq_requisicao	= nr_seq_requisicao_p
			and	a.nr_seq_proc 		is null
			and	a.nr_seq_mat 		is null
			and	b.nr_seq_motivo_glosa	is not null
			and	b.ie_auditoria 		= 'N';
		end if;

		if	(qt_glosa_w	> 0) then
			update	pls_auditoria_item
			set	ie_status		= 'N',
				ie_status_solicitacao	= 'N'
			where	nr_seq_proc_origem	= nr_seq_req_proc_p;

			update	pls_requisicao_proc
			set	ie_status	= 'N'
			where	nr_sequencia	= nr_seq_req_proc_p;
		end if;
	elsif	(nvl(nr_seq_req_mat_p,0)	<> 0) then
		insert	into pls_requisicao_glosa
			(nr_sequencia, nr_seq_motivo_glosa, dt_atualizacao,
			 nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			 nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			 nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			 nr_seq_exec_proc, nr_seq_exec_mat)
		(select	pls_requisicao_glosa_seq.NextVal, nr_seq_motivo_glosa, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			nr_seq_req_mat_p, nr_seq_ocorrencia, nr_seq_execucao,
			nr_seq_exec_proc, nr_seq_exec_mat
		from	pls_requisicao_glosa
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_req_proc		is null
		and	nr_seq_req_mat		is null);

		select	count(1)
		into	qt_glosa_w
		from	pls_requisicao_glosa
		where	nr_seq_requisicao  = nr_seq_requisicao_p
		and	nr_seq_ocorrencia is null;

		if	(qt_glosa_w = 0) then
			select	count(1)
			into	qt_glosa_w
			from	pls_ocorrencia		b,
				pls_ocorrencia_benef	a
			where	a.nr_seq_ocorrencia	= b.nr_sequencia
			and	a.nr_seq_requisicao	= nr_seq_requisicao_p
			and	a.nr_seq_proc 		is null
			and	a.nr_seq_mat 		is null
			and	b.nr_seq_motivo_glosa	is not null
			and	b.ie_auditoria 		= 'N';
		end if;

		if	(qt_glosa_w	> 0) then
			update	pls_auditoria_item
			set	ie_status		= 'N',
				ie_status_solicitacao	= 'N'
			where	nr_seq_mat_origem	= nr_seq_req_mat_p;

			update	pls_requisicao_mat
			set	ie_status	= 'N'
			where	nr_sequencia	= nr_seq_req_mat_p;
		end if;
	end if;
end if;

commit;

end pls_inserir_ocorr_cabeca_item;
/