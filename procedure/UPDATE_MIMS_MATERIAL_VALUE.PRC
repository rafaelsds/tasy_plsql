CREATE OR replace PROCEDURE Update_mims_material_value ( 
cd_material_p        imp_material.cd_material%TYPE, 
nm_usuario_p         VARCHAR2, 
cd_estabelecimento_p NUMBER) 
IS 
  ds_reacao_w   imp_material_reacao.ds_reacao%TYPE; 
  cd_material_w NUMBER; 
BEGIN 
dbms_output.put_line(cd_material_p);
    SELECT Max(cd_material) 
    INTO   cd_material_w 
    FROM   material_reacao 
    WHERE  cd_material = cd_material_p; 

    -- If value exisits then update 
    IF ( Nvl(cd_material_w, 0) <> 0 ) THEN 
      -- check material_reacao ( Reactions ) 
      SELECT max(a.ds_reacao)
      INTO   ds_reacao_w 
      FROM   imp_material_reacao a 
      WHERE  a.cd_material = cd_material_p 
             AND a.ie_dirty_check = 1; 

      IF ( ds_reacao_w IS NOT NULL ) THEN 
        UPDATE material_reacao a 
        SET    a.ds_reacao = ds_reacao_w 
        WHERE  a.cd_material = cd_material_p; 

        UPDATE imp_material_reacao a 
        SET    a.ie_dirty_check = 0 
        WHERE  a.cd_material = cd_material_p; 
      END IF;
      
      --  If value does not exisits then update 
    ELSE 
      -- insert into material_reacao ( Reactions ) 
      INSERT INTO material_reacao 
                  (nr_sequencia, 
                   cd_material, 
                   dt_atualizacao, 
                   nm_usuario, 
                   dt_atualizacao_nrec, 
                   nm_usuario_nrec, 
                   ds_reacao) 
      SELECT nr_sequencia, 
             cd_material, 
             dt_atualizacao, 
             nm_usuario, 
             dt_atualizacao_nrec, 
             nm_usuario_nrec, 
             ds_reacao 
      FROM   imp_material_reacao 
      WHERE  cd_material = cd_material_p; 

      -- reset the dirty check 
      UPDATE imp_material_reacao a 
      SET    a.ie_dirty_check = 0 
      WHERE  a.cd_material = cd_material_p; 
    END IF; 

    COMMIT; 
END update_mims_material_value; 
/