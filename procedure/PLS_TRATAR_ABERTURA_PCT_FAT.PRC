create or replace procedure pls_tratar_abertura_pct_fat(        nr_seq_conta_p                pls_conta.nr_sequencia%type,
                                        nr_seq_conta_proc_p        pls_conta_proc.nr_sequencia%type,
                                        nr_seq_analise_p        pls_analise_conta.nr_sequencia%type,
                                        nr_seq_lote_analise_p        pls_lote_protocolo_conta.nr_sequencia%type,
                                        cd_estabelecimento_p        estabelecimento.cd_estabelecimento%type,
                                        nm_usuario_p                usuario.nm_usuario%type) is

nr_irrelevante_w                number(18,2);
nr_seq_regra_pct_fat_w                pls_conta_proc.nr_seq_regra_pct_fat%type;
ie_filtro_w                        varchar2(1);
dados_conta_proc_w                pls_cta_valorizacao_pck.dados_conta_proc;
dados_prestador_exec_w                pls_cta_valorizacao_pck.dados_prestador_exec;
ie_tipo_intercambio_seg_w        pls_conta_v.ie_tipo_intercambio%type;
nr_seq_tipo_acomodacao_w        pls_conta.nr_seq_tipo_acomodacao%type;
dt_pacote_w                     date;
ie_tipo_data_w					pls_parametros.ie_forma_pacote_fat%type;

cursor c01(        ie_filtro_pc                varchar2,
                nr_seq_conta_pc                pls_conta.nr_sequencia%type,
                nr_seq_conta_proc_pc        pls_conta_proc.nr_sequencia%type,
                nr_seq_analise_pc        pls_analise_conta.nr_sequencia%type,
                nr_seq_lote_analise_pc        pls_lote_protocolo_conta.nr_sequencia%type) is
        select        a.cd_estabelecimento,
                b.nr_sequencia nr_seq_conta_proc,
                b.nr_seq_conta,
                b.ie_origem_proced,
                b.cd_procedimento,
                a.nr_seq_prestador_exec,
                (select        max(x.nr_seq_classificacao)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) nr_seq_clas_prest_exec,
                (select        max(x.cd_prestador)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) cd_prestador_exec,
                a.nr_seq_tipo_acomodacao,
                b.dt_procedimento_referencia,
                pls_obter_se_internado(        a.nr_sequencia, null, a.ie_tipo_guia,
                                        a.nr_seq_tipo_atendimento, null, a.cd_estabelecimento,
                                        'N') ie_internado,
                (select        max(x.nr_seq_plano)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_plano,
                (select        max(x.nr_seq_congenere)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_congenere_seg,
                a.ie_origem_conta,
                pls_obter_tipo_intercambio(nvl(a.nr_seq_congenere, c.nr_seq_congenere), a.cd_estabelecimento) ie_tipo_intercambio,
                (select        max(x.nr_seq_intercambio)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_intercambio,
                a.nr_seq_segurado,
                a.nr_seq_prest_inter,
                (select        max(y.nr_contrato)
                from        pls_contrato y,
                        pls_segurado x
                where        y.nr_sequencia = x.nr_seq_contrato
                and        x.nr_sequencia = a.nr_seq_segurado) nr_contrato,
                (select        x.nr_seq_contrato
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_contrato,
                a.nr_seq_guia,
                (        select        max(y.dt_autorizacao)
                        from        pls_guia_plano_proc        x,
                                pls_guia_plano                y
                        where        x.nr_seq_guia                = y.nr_sequencia
                        and        y.nr_sequencia                = a.nr_seq_guia) dt_autorizacao		
        from        pls_protocolo_conta c,
                pls_conta a,
                pls_conta_proc b
        where        ie_filtro_pc = 'P'
        and        b.nr_sequencia = nr_seq_conta_proc_pc
        and        b.ie_status != 'D'
        and        a.nr_sequencia = b.nr_seq_conta
        and        c.nr_sequencia = a.nr_seq_protocolo
        and        exists(        select        1
                        from        pls_pacote x
                        where        x.cd_procedimento = b.cd_procedimento
                        and        x.ie_origem_proced = b.ie_origem_proced
                        and        x.ie_situacao = 'A')
        and not exists(        select        1
                        from        pls_pacote_tipo_acomodacao x
                        where        x.nr_sequencia = b.nr_seq_preco_pacote
                        and        x.ie_abrir_contas_medicas = 'S')
        union all
        select        a.cd_estabelecimento,
                b.nr_sequencia nr_seq_conta_proc,
                b.nr_seq_conta,
                b.ie_origem_proced,
                b.cd_procedimento,
                a.nr_seq_prestador_exec,
                (select        max(x.nr_seq_classificacao)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) nr_seq_clas_prest_exec,
                (select        max(x.cd_prestador)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) cd_prestador_exec,
                a.nr_seq_tipo_acomodacao,
                b.dt_procedimento_referencia,
                pls_obter_se_internado(        a.nr_sequencia, null, a.ie_tipo_guia,
                                        a.nr_seq_tipo_atendimento, null, a.cd_estabelecimento,
                                        'N') ie_internado,
                (select        max(x.nr_seq_plano)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_plano,
                (select        max(x.nr_seq_congenere)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_congenere_seg,
                a.ie_origem_conta,
                pls_obter_tipo_intercambio(nvl(a.nr_seq_congenere, c.nr_seq_congenere), a.cd_estabelecimento) ie_tipo_intercambio,
                (select        max(x.nr_seq_intercambio)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_intercambio,
                a.nr_seq_segurado,
                a.nr_seq_prest_inter,
                (select        max(y.nr_contrato)
                from        pls_contrato y,
                        pls_segurado x
                where        y.nr_sequencia = x.nr_seq_contrato
                and        x.nr_sequencia = a.nr_seq_segurado) nr_contrato,
                (select        x.nr_seq_contrato
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_contrato,
                a.nr_seq_guia,
                (        select        max(y.dt_autorizacao)
                        from        pls_guia_plano_proc        x,
                                pls_guia_plano                y
                        where        x.nr_seq_guia                = y.nr_sequencia
                        and        y.nr_sequencia                = a.nr_seq_guia) dt_autorizacao	
        from        pls_protocolo_conta c,
                pls_conta a,
                pls_conta_proc b
        where        ie_filtro_pc = 'C'
        and        a.nr_sequencia = nr_seq_conta_pc
        and        b.nr_seq_conta = nr_seq_conta_pc
        and        b.ie_status != 'D'
        and        c.nr_sequencia = a.nr_seq_protocolo
        and        exists(        select        1
                        from        pls_pacote x
                        where        x.cd_procedimento = b.cd_procedimento
                        and        x.ie_origem_proced = b.ie_origem_proced
                        and        x.ie_situacao = 'A')
        and not exists(        select        1
                        from        pls_pacote_tipo_acomodacao x
                        where        x.nr_sequencia = b.nr_seq_preco_pacote
                        and        x.ie_abrir_contas_medicas = 'S')
        union all
        select        a.cd_estabelecimento,
                b.nr_sequencia nr_seq_conta_proc,
                b.nr_seq_conta,
                b.ie_origem_proced,
                b.cd_procedimento,
                a.nr_seq_prestador_exec,
                (select        max(x.nr_seq_classificacao)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) nr_seq_clas_prest_exec,
                (select        max(x.cd_prestador)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) cd_prestador_exec,
                a.nr_seq_tipo_acomodacao,
                b.dt_procedimento_referencia,
                pls_obter_se_internado(        a.nr_sequencia, null, a.ie_tipo_guia,
                                        a.nr_seq_tipo_atendimento, null, a.cd_estabelecimento,
                                        'N') ie_internado,
                (select        max(x.nr_seq_plano)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_plano,
                (select        max(x.nr_seq_congenere)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_congenere_seg,
                a.ie_origem_conta,
                pls_obter_tipo_intercambio(nvl(a.nr_seq_congenere, c.nr_seq_congenere), a.cd_estabelecimento) ie_tipo_intercambio,
                (select        max(x.nr_seq_intercambio)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_intercambio,
                a.nr_seq_segurado,
                a.nr_seq_prest_inter,
                (select        max(y.nr_contrato)
                from        pls_contrato y,
                        pls_segurado x
                where        y.nr_sequencia = x.nr_seq_contrato
                and        x.nr_sequencia = a.nr_seq_segurado) nr_contrato,
                (select        x.nr_seq_contrato
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_contrato,
                a.nr_seq_guia,
                (        select        max(y.dt_autorizacao)
                        from        pls_guia_plano_proc        x,
                                pls_guia_plano                y
                        where        x.nr_seq_guia                = y.nr_sequencia
                        and        y.nr_sequencia                = a.nr_seq_guia) dt_autorizacao	
        from        pls_protocolo_conta c,
                pls_conta a,
                pls_conta_proc b
        where        ie_filtro_pc = 'A'
        and        a.nr_seq_analise = nr_seq_analise_pc
        and        b.nr_seq_conta = a.nr_sequencia
        and        b.ie_status != 'D'
        and        c.nr_sequencia = a.nr_seq_protocolo
        and        exists(        select        1
                        from        pls_pacote x
                        where        x.cd_procedimento = b.cd_procedimento
                        and        x.ie_origem_proced = b.ie_origem_proced
                        and        x.ie_situacao = 'A')
        and not exists(        select        1
                        from        pls_pacote_tipo_acomodacao x
                        where        x.nr_sequencia = b.nr_seq_preco_pacote
                        and        x.ie_abrir_contas_medicas = 'S')
        union all
        select        a.cd_estabelecimento,
                b.nr_sequencia nr_seq_conta_proc,
                b.nr_seq_conta,
                b.ie_origem_proced,
                b.cd_procedimento,
                a.nr_seq_prestador_exec,
                (select        max(x.nr_seq_classificacao)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) nr_seq_clas_prest_exec,
                (select        max(x.cd_prestador)
                from        pls_prestador x
                where        x.nr_sequencia = a.nr_seq_prestador_exec) cd_prestador_exec,
                a.nr_seq_tipo_acomodacao,
                b.dt_procedimento_referencia,
                pls_obter_se_internado(        a.nr_sequencia, null, a.ie_tipo_guia,
                                        a.nr_seq_tipo_atendimento, null, a.cd_estabelecimento,
                                        'N') ie_internado,
                (select        max(x.nr_seq_plano)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_plano,
                (select        max(x.nr_seq_congenere)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_congenere_seg,
                a.ie_origem_conta,
                pls_obter_tipo_intercambio(nvl(a.nr_seq_congenere, c.nr_seq_congenere), a.cd_estabelecimento) ie_tipo_intercambio,
                (select        max(x.nr_seq_intercambio)
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_intercambio,
                a.nr_seq_segurado,
                a.nr_seq_prest_inter,
                (select        max(y.nr_contrato)
                from        pls_contrato y,
                        pls_segurado x
                where        y.nr_sequencia = x.nr_seq_contrato
                and        x.nr_sequencia = a.nr_seq_segurado) nr_contrato,
                (select        x.nr_seq_contrato
                from        pls_segurado x
                where        x.nr_sequencia = a.nr_seq_segurado) nr_seq_contrato,
                a.nr_seq_guia,
                (        select        max(y.dt_autorizacao)
                        from        pls_guia_plano_proc        x,
                                pls_guia_plano                y
                        where        x.nr_seq_guia                = y.nr_sequencia
                        and        y.nr_sequencia                = a.nr_seq_guia) dt_autorizacao	
        from        pls_protocolo_conta c,
                pls_conta a,
                pls_conta_proc b,
                pls_analise_conta d
        where        ie_filtro_pc = 'L'
        and        d.nr_seq_lote_protocolo = nr_seq_lote_analise_pc
        and        a.nr_seq_analise = d.nr_sequencia
        and        b.nr_seq_conta = a.nr_sequencia
        and        b.ie_status != 'D'
        and        c.nr_sequencia = a.nr_seq_protocolo
        and        exists(        select        1
                        from        pls_pacote x
                        where        x.cd_procedimento = b.cd_procedimento
                        and        x.ie_origem_proced = b.ie_origem_proced
                        and        x.ie_situacao = 'A')
        and not exists(        select        1
                        from        pls_pacote_tipo_acomodacao x
                        where        x.nr_sequencia = b.nr_seq_preco_pacote
                        and        x.ie_abrir_contas_medicas = 'S');

begin

-- verifica qual o filtro que ira aplicar, da prioridade sempre para o mais restritivo passado
if        (nr_seq_conta_proc_p is not null) then

        ie_filtro_w := 'P'; -- conta proc

elsif        (nr_seq_conta_p is not null) then

        ie_filtro_w := 'C'; -- conta

elsif        (nr_seq_analise_p is not null) then

        ie_filtro_w := 'A'; -- analise
else
        ie_filtro_w := 'L'; -- lote de analise
end if;

-- retorna de acordo com o filtro identificado
for r_c01_w in c01(        ie_filtro_w, nr_seq_conta_p, nr_seq_conta_proc_p,
                        nr_seq_analise_p, nr_seq_lote_analise_p) loop

        -- dados do procedimento
        dados_conta_proc_w.cd_procedimento := r_c01_w.cd_procedimento;
        dados_conta_proc_w.ie_origem_proced:= r_c01_w.ie_origem_proced;
        -- dados do prestador executor
        dados_prestador_exec_w.nr_seq_prestador := r_c01_w.nr_seq_prestador_exec;
        dados_prestador_exec_w.nr_seq_classificacao := r_c01_w.nr_seq_clas_prest_exec;
        dados_prestador_exec_w.cd_prestador := r_c01_w.cd_prestador_exec;

        ie_tipo_intercambio_seg_w := pls_obter_tipo_intercambio(r_c01_w.nr_seq_congenere_seg, r_c01_w.cd_estabelecimento);

        if        (r_c01_w.nr_seq_tipo_acomodacao is null) and
                (r_c01_w.nr_seq_guia is not null)then
                select        max(nr_seq_tipo_acomodacao)
                into        nr_seq_tipo_acomodacao_w
                from        pls_guia_plano
                where         nr_sequencia        = r_c01_w.nr_seq_guia;
        else
                nr_seq_tipo_acomodacao_w        := r_c01_w.nr_seq_tipo_acomodacao;
        end if;

		
			select	max(nvl(ie_forma_pacote_fat,'N'))
			into	ie_tipo_data_w
			from	pls_parametros
			where	cd_estabelecimento	=	nvl(wheb_usuario_pck.get_cd_estabelecimento, (select max(e.cd_estabelecimento)
													from pls_outorgante po,
													estabelecimento e
													where	po.cd_cgc_outorgante = e.cd_cgc));

		if	(ie_tipo_data_w = 'N') then
			dt_pacote_w := r_c01_w.dt_procedimento_referencia;
		elsif	(ie_tipo_data_w = 'S' and r_c01_w.dt_autorizacao < '01/12/2020') then 
			dt_pacote_w := r_c01_w.dt_autorizacao;
		else
			dt_pacote_w := r_c01_w.dt_procedimento_referencia;
		end if; 	

        pls_define_preco_pacote_cta(        nvl(cd_estabelecimento_p, r_c01_w.cd_estabelecimento), dados_conta_proc_w, dados_prestador_exec_w,
                                        nr_seq_tipo_acomodacao_w, Dt_pacote_w, r_c01_w.ie_internado,
                                        r_c01_w.nr_seq_plano, r_c01_w.nr_contrato, r_c01_w.nr_seq_congenere_seg,
                                        nm_usuario_p, r_c01_w.ie_origem_conta, ie_tipo_intercambio_seg_w,
                                        nr_irrelevante_w, nr_seq_regra_pct_fat_w, nr_irrelevante_w,
                                        nr_irrelevante_w, nr_irrelevante_w, nr_irrelevante_w,
                                        nr_irrelevante_w, nr_irrelevante_w, r_c01_w.nr_seq_intercambio,
                                        r_c01_w.nr_seq_segurado, 'N', r_c01_w.nr_seq_prest_inter,
                                        1, 'S', r_c01_w.nr_seq_contrato);

        update        pls_conta_proc
        set        nr_seq_regra_pct_fat = nr_seq_regra_pct_fat_w
        where         nr_sequencia = r_c01_w.nr_seq_conta_proc
        and        ie_status <> 'M';
end loop;

end pls_tratar_abertura_pct_fat;
/
