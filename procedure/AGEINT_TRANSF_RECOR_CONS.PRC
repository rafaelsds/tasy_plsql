create or replace
procedure Ageint_transf_recor_cons
					(	dt_prev_transf_p		date,
						dt_prev_selecionada_p		date,
						nr_seq_ageint_p			number,
						nr_seq_ageint_item_p		number,
						ie_opcao_p			number,
						cd_estabelecimento_p		number,
						nm_usuario_p			Varchar2,
						ie_gerar_final_semana_p		varchar2,	
						ie_gerar_feriado_p		varchar2,
						ie_diario_p			varchar2 default null,
						ds_dias_p			varchar2 default null,
						ie_semanal_p			varchar2 default null,
						dt_prev_final_p			date	default null) is 


dt_agenda_w	date;				    
qt_dias_w	number(10,0);
nr_seq_ageint_item_w	number(10,0);
dt_agendamento_w	date;
ie_dia_semana_w		varchar2(3);
ie_Feriado_w		number(1);
ie_gerar_w			varchar2(1)	:= 'N';

/* Altera��es nesse cursor devem ser feitas tamb�m na rotina obter_dt_fim_transf_recorr e rotina ageint_transf_recorrencia_serv*/
Cursor C01 is
	select	b.nr_sequencia,
			trunc(a.dt_agenda)
	from	agenda_consulta a,
		agenda_integrada_item b
	where	a.nr_sequencia = b.nr_seq_agenda_cons
	and	b.nr_seq_agenda_int = nr_seq_ageint_p
	and	a.ie_status_agenda <> 'C'
	--and	a.dt_agenda >= dt_agenda_w
	and	b.nr_sequencia <> nr_seq_ageint_item_p
	and	(b.nr_sequencia > nr_seq_ageint_item_p or ie_opcao_p = 0)
	union all
	select	b.nr_sequencia,
			trunc(a.dt_agenda)
	from	agenda_consulta a,
		agenda_integrada_item b
	where	a.nr_sequencia = b.nr_seq_Agenda_cons
	and	b.nr_seq_agenda_int = nr_seq_ageint_p
	and	a.ie_status_agenda <> 'C'
	and	b.nr_sequencia = nr_seq_ageint_item_p
	order by 1;
				    
begin

/* op��o
    0 - Somente data selecionada
    1 - Todas as datas de recorr�ncia
*/

update	agenda_integrada_item
set	dt_prev_transf_item	= null
where	nr_Seq_agenda_int	= nr_seq_ageint_p;

commit;

select	max(a.dt_agenda)
into	dt_agenda_w
from	agenda_consulta a,
		agenda_integrada_item b
where	a.nr_sequencia		= b.nr_seq_agenda_cons
and		b.nr_seq_agenda_int	= nr_seq_ageint_p;

if	(ie_opcao_p = 0) then

	ie_dia_semana_w	:= obter_cod_dia_semana(dt_agenda_w);
	ie_Feriado_w	:= obter_se_feriado(cd_estabelecimento_p, dt_agenda_w);
	if	(ie_gerar_final_semana_p	= 'S') and
		(ie_Gerar_feriado_p		= 'S') then
		update	agenda_integrada_item
		set		dt_prev_transf_item = dt_prev_transf_p
		where	nr_sequencia = nr_seq_ageint_item_p;
	else
		qt_dias_w := 0;--dt_prev_transf_p - dt_prev_selecionada_p;
		ie_gerar_w	:= 'N';
		while (ie_gerar_w	= 'N') loop 
			begin
			ie_dia_semana_w	:= obter_cod_dia_semana(dt_prev_transf_p + qt_dias_w);
			ie_Feriado_w	:= obter_se_feriado(cd_estabelecimento_p, dt_prev_transf_p + qt_dias_w);

			if	(ie_gerar_final_semana_p	= 'N') then
				if	(ie_dia_semana_w not in (1,7)) then
					ie_Gerar_w	:= 'S';
				else
					ie_gerar_w	:= 'N';
				end if;
			end if;
			
			if	(ie_Gerar_feriado_p	= 'N') and
				(ie_Gerar_w			= 'S') then 
				if	(ie_feriado_w	= 0) then
					ie_Gerar_w	:= 'S';
				else 
					ie_gerar_w	:= 'N';
				end if;
			end if;
			
			if	(ie_Gerar_w	= 'N') then
				qt_dias_w	:= qt_dias_w + 1;
			end if;
			end;
		end loop;
		
		update	agenda_integrada_item
		set		dt_prev_transf_item = dt_prev_transf_p + qt_dias_w
		where	nr_sequencia = nr_seq_ageint_item_p;
	end if;
else
	if ((ie_diario_p = 'S') or (ie_semanal_p = 'S')) and (dt_prev_final_p is not null) and (ie_opcao_p = 1) then
	
		ageint_transf_recorrencia_serv (nr_seq_ageint_p, nm_usuario_p, ie_diario_p, ie_semanal_p, ds_dias_p, dt_prev_transf_p, dt_prev_final_p, ie_gerar_final_semana_p, nr_seq_ageint_item_p);
	
	else
		qt_dias_w := dt_prev_transf_p - dt_prev_selecionada_p;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_ageint_item_w,
			dt_agendamento_w;
		exit when C01%notfound;
			begin		
			ie_gerar_w	:= 'N';
			--qt_dias_w := dt_prev_transf_p - dt_prev_selecionada_p;
			
			ie_dia_semana_w	:= obter_cod_dia_semana(dt_agendamento_w);
			ie_Feriado_w	:= obter_se_feriado(cd_estabelecimento_p, dt_agendamento_w);
			
			if	(ie_gerar_final_semana_p	= 'S') and
				(ie_Gerar_feriado_p		= 'S') then
				update	agenda_integrada_item
				set		dt_prev_transf_item = dt_agendamento_w + qt_dias_w
				where	nr_sequencia = nr_seq_ageint_item_w;
			else
							 
				while (ie_gerar_w	= 'N') loop 
					begin
					ie_dia_semana_w	:= obter_cod_dia_semana(dt_agendamento_w + qt_dias_w);
					ie_Feriado_w	:= obter_se_feriado(cd_estabelecimento_p, dt_agendamento_w + qt_dias_w);

					if	(ie_gerar_final_semana_p	= 'N') then
						if	(ie_dia_semana_w not in (1,7)) then
							ie_Gerar_w	:= 'S';
						else
							ie_gerar_w	:= 'N';
						end if;
					end if;
					
					if	(ie_Gerar_feriado_p	= 'N') and
						(ie_Gerar_w			= 'S') then
						if	(ie_feriado_w	= 0) then
							ie_Gerar_w	:= 'S';
						else
							ie_gerar_w	:= 'N';
						end if;
					end if;
					
					if	(ie_Gerar_w	= 'N') then
						qt_dias_w	:= qt_dias_w + 1;
					end if;
					
					end;
				end loop;
				
				update	agenda_integrada_item
				set		dt_prev_transf_item = dt_agendamento_w + qt_dias_w
				where	nr_sequencia = nr_seq_ageint_item_w;
			end if;
			
			end;
		end loop;
		close C01;
	end if;	
end if;

commit;

end Ageint_transf_recor_cons;
/