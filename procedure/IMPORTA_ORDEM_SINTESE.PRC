create or replace
procedure importa_ordem_sintese(	nr_cot_compra_p			number,
					nm_usuario_p			varchar2,
					ie_aprovada_p		out	varchar2,
					nr_ordem_compra_p	out	number) is 

/*Para cada sequencia, ou sem, cada ordem de compra enviada pela Sintese, e uma chamada dessa procedure*/					
					
nr_ordem_compra_w		number(10);
nr_seq_ordem_w			number(10);
nr_seq_oc_prod_w		w_sintese_oc_prod.nr_sequencia%type;
cd_fornecedor_w			varchar2(14);
cd_fornecedor_ww		varchar2(14);
cd_condicao_pagamento_w		number(10);
nr_ordem_sintese_w		ordem_compra.nr_documento_externo%type;
dt_previsao_entrega_w		date;
ds_observacao_w			varchar2(4000);
qt_registro_w			number(10);
cd_estabelecimento_w		number(10);
cd_moeda_padrao_w		number(10);
cd_comprador_w			varchar2(10);
cd_pessoa_solicitante_w		varchar2(10);
nm_pessoa_contato_w		varchar2(255);
cd_material_w			w_sintese_oc_prod.cd_produto%type;
cd_marca_w			w_sintese_oc_prod.cd_marca%type;
ds_marca_w			w_sintese_oc_prod.ds_marca%type;
qt_material_w			number(13,4);
tp_frete_w			w_sintese_oc_prod.tp_frete%type;
vl_unitario_material_w		w_sintese_oc_prod.vl_preco_produto%type;
vl_item_liquido_w		w_sintese_oc_prod.vl_preco_produto%type;
pr_ipi_w			w_sintese_oc_prod.pc_ipi_produto%type;
ds_observacao_item_w		w_sintese_oc_prod.ds_observacao_produto%type;
nr_item_oci_w			number(10);
cd_unidade_medida_compra_w	varchar2(30);
nr_solic_compra_w			number(10);
nr_item_solic_compra_w		number(10);
cd_local_estoque_w		number(10);
cd_centro_custo_w			number(10);
cd_conta_contabil_w		varchar2(20);
ie_aviso_chegada_w		varchar2(1)	:= 'N';
ie_urgente_w			varchar2(1)	:= 'N';
ie_tipo_conta_w			number(5)	:= 2;
nr_seq_marca_w			number(10);
nr_seq_tipo_marca_w		number(10);
nr_item_cot_compra_w		number(10);
cd_tributo_w			number(10);
vl_tributo_w			number(13,4);
nr_seq_fornecedor_w		number(10);
ie_liberar_ordem_cotacao_w		varchar2(15);
ie_aprovada_w			varchar2(1)	:= 'N';
dt_aprovacao_w			date;
ie_erro_w				varchar2(1) := 'N';
ds_erro_w			varchar2(255);
nr_cot_compra_w			number(10);
qt_registro_fornec_w		number(10);
qt_ordens_w			number(10);
cd_cnpj_comprador_w		varchar2(14);
nr_seq_motivo_solic_w		number(10);
qt_item_entrega_w		number(10);
ie_gera_nova_cotacao_w		varchar2(1);

cursor c01 is
select	nr_sequencia,
	cd_fornecedor,
	cd_comprador,
	somente_numero(cd_condicao_pagamento),
	substr(cd_ordem_compra,1,40),
	dt_previsao_entrega,
	ds_observacao
from	w_sintese_oc
where	nr_sequencia = nr_cot_compra_p;

cursor c02 is
select	nr_sequencia,
	cd_produto,	
	cd_marca,
	ds_marca,
	qt_produto,
	tp_frete,
	vl_preco_produto,
	pc_ipi_produto,
	substr(ds_observacao_produto,1,255)
from	w_sintese_oc_prod
where	nr_seq_ordem = nr_seq_ordem_w;

cursor c03 is
select	a.nr_solic_compra,
	a.cd_local_estoque,
	a.cd_centro_custo,
	a.cd_conta_contabil,
	nvl(a.ie_aviso_chegada,'N'),
	nvl(a.ie_urgente,'N'),
	b.nr_item_solic_compra
from	solic_compra a,
	solic_compra_item_agrup_v b	
where	a.nr_solic_compra	= b.nr_solic_compra
and	nr_cot_compra		= nr_cot_compra_w
and	nr_item_cot_compra	= nr_item_cot_compra_w
order by nr_solic_compra;

cursor c04 is
select	dt_entrega, 
	qt_prevista_entrega, 
	dt_entrega_limite, 
	dt_entrega_original, 
	dt_real_entrega, 
	substr(ds_observacao_entrega,1,255) ds_observacao_entrega
from	w_sintese_oc_prod_en
where	nr_seq_ordem = nr_seq_ordem_w
and	nr_seq_oc_prod = nr_seq_oc_prod_w;

begin

select	max(cd_requisicao),
	max(cd_fornecedor)
into	nr_cot_compra_w,
	cd_fornecedor_ww
from	w_sintese_oc
where	nr_sequencia = nr_cot_compra_p;

delete from log_sintese
where	nr_cot_compra	= nr_cot_compra_w
and	ds_entidade	= 'ORDEM';

select	count(*)
into	qt_registro_w
from	cot_compra
where	nr_cot_compra = nr_cot_compra_w;

select	count(*)
into	qt_registro_fornec_w
from	cot_compra_forn
where	nr_cot_compra = nr_cot_compra_w;

select	count(*)
into	qt_ordens_w
from	ordem_compra a,
	ordem_compra_item b
where	a.nr_ordem_compra = b.nr_ordem_compra
and	b.nr_cot_compra = nr_cot_compra_w
and	a.cd_cgc_fornecedor = cd_fornecedor_ww
and	a.nr_seq_motivo_cancel is null;

if	(qt_registro_w > 0) and
	(qt_ordens_w = 0) and
	(qt_registro_fornec_w > 0) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_ordem_w,
		cd_fornecedor_w,
		cd_cnpj_comprador_w,
		cd_condicao_pagamento_w,
		nr_ordem_sintese_w,
		dt_previsao_entrega_w,
		ds_observacao_w;
	exit when C01%notfound;
		begin
		
		select	nvl(max(cd_estabelecimento),0)
		into	cd_estabelecimento_w
		from	estabelecimento
		where	cd_cgc = cd_cnpj_comprador_w;
		
		if	(cd_estabelecimento_w = 0) then
		
			select	cd_estabelecimento
			into	cd_estabelecimento_w
			from	cot_compra
			where	nr_cot_compra = nr_cot_compra_w;
			
		end if;
		
		select	a.cd_comprador,
			a.cd_pessoa_solicitante
		into	cd_comprador_w,
			cd_pessoa_solicitante_w
		from	cot_compra a
		where	a.nr_cot_compra = nr_cot_compra_w;
		
		select	a.cd_moeda_padrao,			
			a.nr_seq_tipo_marca,
			a.ie_liberar_ordem_cotacao,
			nvl(cd_pessoa_solicitante_w, a.cd_pessoa_solic_padrao)
		into	cd_moeda_padrao_w,			
			nr_seq_tipo_marca_w,
			ie_liberar_ordem_cotacao_w,
			cd_pessoa_solicitante_w
		from	parametro_compras a
		where	a.cd_estabelecimento = cd_estabelecimento_w;
		
		select	count(*)
		into	qt_registro_w
		from	pessoa_juridica
		where	cd_cgc = cd_fornecedor_w;
			
		if	(qt_registro_w = 0) then
			inserir_log_sintese('ERRO','ORDEM', wheb_mensagem_pck.get_texto(302954, 'CD_CNPJ=' || cd_fornecedor_w), '00011', nr_cot_compra_w, '' ,nm_usuario_p);
			ie_erro_w := 'S';
			ds_erro_w := wheb_mensagem_pck.get_texto(302954, 'CD_CNPJ=' || cd_fornecedor_w);
		end if;
		
		
		select	count(*)
		into	qt_registro_w
		from	condicao_pagamento
		where	cd_condicao_pagamento = cd_condicao_pagamento_w;
		
		if	(qt_registro_w = 0) then
			inserir_log_sintese('ERRO','ORDEM', wheb_mensagem_pck.get_texto(302957, 'CD_CONDICAO_PAGTO=' || cd_condicao_pagamento_w), '00012', nr_cot_compra_w, '' , nm_usuario_p);
			ie_erro_w := 'S';
			ds_erro_w := wheb_mensagem_pck.get_texto(302957, 'CD_CONDICAO_PAGTO=' || cd_condicao_pagamento_w);
		end if;
		
		if	(nr_ordem_sintese_w is null) then
			inserir_log_sintese('ERRO','ORDEM', wheb_mensagem_pck.get_texto(302958), '00014', nr_cot_compra_w, '' , nm_usuario_p);
			ie_erro_w := 'S';
			ds_erro_w := wheb_mensagem_pck.get_texto(302958);
		end if;
		
		if	(ie_erro_w = 'N') then
		
			select	nm_pessoa_contato
			into	nm_pessoa_contato_w
			from	pessoa_juridica
			where	cd_cgc = cd_fornecedor_w;
			
			select	ordem_compra_seq.nextval
			into	nr_ordem_compra_w
			from	dual;
			
			insert into ordem_compra(
				nr_ordem_compra,
				cd_estabelecimento,
				cd_cgc_fornecedor,
				cd_condicao_pagamento,
				cd_comprador,
				dt_ordem_compra,
				dt_atualizacao,
				nm_usuario,
				cd_moeda,
				ie_situacao,
				dt_inclusao,
				cd_pessoa_solicitante,
				ie_frete,
				vl_frete,
				ds_pessoa_contato,
				ds_observacao,
				dt_entrega,
				ie_aviso_chegada,
				ie_emite_obs,
				ie_urgente,
				vl_despesa_acessoria,
				nr_documento_externo,
				vl_desconto,
				ie_tipo_ordem,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	nr_ordem_compra_w,
				cd_estabelecimento_w,
				cd_fornecedor_w,
				cd_condicao_pagamento_w,
				cd_comprador_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				cd_moeda_padrao_w,
				'A',
				sysdate,
				cd_pessoa_solicitante_w,
				'C',
				0,
				nm_pessoa_contato_w,
				ds_observacao_w,
				dt_previsao_entrega_w,
				'N',
				'N',
				'N',
				0,
				nr_ordem_sintese_w,
				0,
				'E',
				sysdate,
				nm_usuario_p);
			
			open C02;
			loop
			fetch C02 into
				nr_seq_oc_prod_w,
				cd_material_w,
				cd_marca_w,
				ds_marca_w,
				qt_material_w,
				tp_frete_w,
				vl_unitario_material_w,
				pr_ipi_w,
				ds_observacao_item_w;
			exit when C02%notfound;
				begin
				
				if	(tp_frete_w is not null) then
					update	ordem_compra
					set	ie_frete	= decode(upper(tp_frete_w),'CIF','C','F')
					where	nr_ordem_compra	= nr_ordem_compra_w;
				end if;
				
				select	count(*)
				into	qt_registro_w
				from	material
				where	cd_material = cd_material_w;
				
				select	min(nr_item_cot_compra)
				into	nr_item_cot_compra_w
				from	cot_compra_item
				where	nr_cot_compra = nr_cot_compra_w
				and	cd_material = cd_material_w;
				
				if	(nr_item_cot_compra_w > 0) then
					
					/*Buscar dados da solicitacao de compras para preencher na ordem de compra*/
					open C03;
					loop
					fetch C03 into	
						nr_solic_compra_w,
						cd_local_estoque_w,
						cd_centro_custo_w,
						cd_conta_contabil_w,
						ie_aviso_chegada_w,
						ie_urgente_w,
						nr_item_solic_compra_w;
					exit when C03%notfound;
						begin
						nr_solic_compra_w		:= nr_solic_compra_w;					
						cd_local_estoque_w		:= cd_local_estoque_w;
						cd_centro_custo_w		:= cd_centro_custo_w;
						cd_conta_contabil_w		:= cd_conta_contabil_w;
						ie_aviso_chegada_w		:= ie_aviso_chegada_w;
						ie_urgente_w			:= ie_urgente_w;
						nr_item_solic_compra_w		:= nr_item_solic_compra_w;
						end;
					end loop;
					close C03;				
				
				end if;
				
				if	(qt_registro_w > 0) then
				
					select	cd_unidade_medida_compra
					into	cd_unidade_medida_compra_w
					from	material
					where	cd_material = cd_material_w;				
					
					vl_item_liquido_w	:= 0;
					vl_item_liquido_w	:= nvl(qt_material_w,0) * nvl(vl_unitario_material_w,0);
					
					if	(cd_conta_contabil_w is null) then

						if	(cd_centro_custo_w is null) then
							ie_tipo_conta_w	:= 2;
						else
							ie_tipo_conta_w	:= 3;
						end if;

						define_conta_material(	cd_estabelecimento_w,
									cd_material_w,
									ie_tipo_conta_w,
									null, null, null, null, null, null, null,
									cd_local_estoque_w,
									null, sysdate,
									cd_conta_contabil_w,
									cd_centro_custo_w,null);
					end if;
					
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_marca_w
					from	marca
					where	cd_sistema_ant = cd_marca_w;
					
					if	(nr_seq_marca_w > 0) then
					
						select	count(*)
						into	qt_registro_w
						from	material_marca
						where	cd_material	= cd_material_w
						and	nr_sequencia	= nr_seq_marca_w;
					
						if	(qt_registro_w = 0) then
							
							insert into material_marca(
								cd_material,
								nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ie_situacao)
							values(	cd_material_w,
								nr_seq_marca_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								'A');
						end if;
					else
					
						if	(ds_marca_w is not null) and
							(nr_seq_tipo_marca_w > 0) then
						
							select 	marca_seq.nextval
							into	nr_seq_marca_w
							from	dual;
								
							insert into marca(
								nr_sequencia,
								ds_marca,
								ie_situacao,
								dt_atualizacao,
								nm_usuario,
								nr_seq_tipo,
								qt_dia_ressup,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								cd_sistema_ant)
							values(	nr_seq_marca_w,
								ds_marca_w,
								'A',
								sysdate,
								nm_usuario_p,
								nr_seq_tipo_marca_w,
								7,
								sysdate,
								nm_usuario_p,
								cd_marca_w);
							
							insert into material_marca(
								cd_material,
								nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ie_situacao)
							values(	cd_material_w,
								nr_seq_marca_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								'A');
						else
							nr_seq_marca_w := null;
						end if;
					end if;
					
					select	nvl(max(nr_item_oci),0) + 1
					into	nr_item_oci_w
					from	ordem_compra_item
					where	nr_ordem_compra = nr_ordem_compra_w;
					
					insert into ordem_compra_item(
						nr_ordem_compra,
						nr_item_oci,
						cd_material,
						cd_unidade_medida_compra,
						vl_unitario_material,
						qt_material,
						dt_atualizacao,
						nm_usuario,
						ie_situacao,
						cd_local_estoque,
						ds_observacao,
						nr_cot_compra,
						nr_item_cot_compra,
						vl_item_liquido,
						cd_centro_custo,
						cd_conta_contabil,
						nr_solic_compra,
						nr_item_solic_compra,
						qt_original,
						nr_seq_marca,
						vl_desconto,
						pr_descontos,
						pr_desc_financ,
						vl_unit_mat_original,
						vl_total_item)
					values(	nr_ordem_compra_w,
						nr_item_oci_w,
						cd_material_w,
						cd_unidade_medida_compra_w,
						vl_unitario_material_w,
						qt_material_w,
						sysdate,
						nm_usuario_p,
						'A',
						cd_local_estoque_w,
						ds_observacao_item_w,
						nr_cot_compra_w,
						nr_item_cot_compra_w,
						vl_item_liquido_w,
						cd_centro_custo_w,
						cd_conta_contabil_w,
						nr_solic_compra_w,
						nr_item_solic_compra_w,
						qt_material_w,
						nr_seq_marca_w,
						0,
						0,
						0,
						vl_unitario_material_w,
						round((vl_unitario_material_w * qt_material_w),4));

					select	count(*)
					into	qt_item_entrega_w
					from	w_sintese_oc_prod_en
					where	nr_seq_ordem = nr_seq_ordem_w
					and	nr_seq_oc_prod = nr_seq_oc_prod_w;						
					
					if (qt_item_entrega_w > 0) then
						begin
						for c04_row in c04 loop
						insert into ordem_compra_item_entrega(
							nr_sequencia,
							nr_ordem_compra,
							nr_item_oci,
							dt_prevista_entrega,
							qt_prevista_entrega,
							dt_atualizacao,
							nm_usuario,
							dt_entrega_original,
							dt_entrega_limite,
							ds_observacao)
						values(	ordem_compra_item_entrega_seq.nextval,
							nr_ordem_compra_w,
							nr_item_oci_w,
							c04_row.dt_entrega,
							c04_row.qt_prevista_entrega,
							sysdate,
							nm_usuario_p,
							nvl(c04_row.dt_entrega_original, c04_row.dt_entrega),
							nvl(c04_row.dt_entrega_limite, c04_row.dt_entrega),
							c04_row.ds_observacao_entrega);
						end loop;
						end;
					else
						begin
						insert into ordem_compra_item_entrega(
							nr_sequencia,
							nr_ordem_compra,
							nr_item_oci,
							dt_prevista_entrega,
							qt_prevista_entrega,
							dt_atualizacao,
							nm_usuario,
							dt_entrega_original,
							dt_entrega_limite)
						values(	ordem_compra_item_entrega_seq.nextval,
							nr_ordem_compra_w,
							nr_item_oci_w,
							dt_previsao_entrega_w,
							qt_material_w,
							sysdate,
							nm_usuario_p,
							dt_previsao_entrega_w,
							dt_previsao_entrega_w);
						end;
					end if;
					
					update	cot_compra_item
					set	cd_cgc_fornecedor_venc_alt	= cd_fornecedor_w
					where	nr_cot_compra			= nr_cot_compra_w
					and	nr_item_cot_compra		= nr_item_cot_compra_w;
					
					
					select	nvl(max(a.nr_sequencia),0)
					into	nr_seq_fornecedor_w
					from	cot_compra_forn a,
						cot_compra_forn_item b
					where	a.nr_sequencia		= b.nr_seq_cot_forn
					and	a.nr_cot_compra 	= nr_cot_compra_w
					and	b.nr_item_cot_compra	= nr_item_cot_compra_w
					and	a.cd_cgc_fornecedor	= cd_fornecedor_w;
					
					if	(nr_seq_fornecedor_w > 0) then				
						update	cot_compra_forn_item
						set	nr_ordem_compra		= nr_ordem_compra_w
						where	nr_cot_compra		= nr_cot_compra_w
						and	nr_item_cot_compra	= nr_item_cot_compra_w
						and	nr_seq_cot_forn		= nr_seq_fornecedor_w
						and	nr_ordem_compra is null;
					end if;
					
					if	(pr_ipi_w > 0) then
				
						select	nvl(min(cd_tributo),0)
						into	cd_tributo_w
						from	tributo
						where	ie_situacao		= 'A'
						and	ie_tipo_tributo		= 'IPI'
						and	ie_corpo_item		= 'I';
						
						if	(cd_tributo_w > 0) then
							
							vl_tributo_w	:= 0;
							vl_tributo_w	:= (dividir(pr_ipi_w,100) * vl_item_liquido_w);
							
							insert into ordem_compra_item_trib(
								nr_ordem_compra,
								nr_item_oci,
								cd_tributo,
								pr_tributo,
								vl_tributo,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec)
							values(	nr_ordem_compra_w,
								nr_item_oci_w,
								cd_tributo_w,
								pr_ipi_w,
								vl_tributo_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p);
						end if;
					end if;
					
				else
					inserir_historico_ordem_compra(
						nr_ordem_compra_w,
						'S',
						wheb_mensagem_pck.get_texto(302961),
						wheb_mensagem_pck.get_texto(302974, 'CD_MATERIAL=' || cd_material_w),
						nm_usuario_p);
				end if;
				
				end;
			end loop;
			close C02;
		end if;
		
		select	nvl(max(nr_solic_compra),0)
		into	nr_solic_compra_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w;

		if	(nr_solic_compra_w > 0) then
	
			select	nvl(max(nr_seq_motivo_solic),0)
			into	nr_seq_motivo_solic_w
			from	solic_compra
			where	nr_solic_compra = nr_solic_compra_w;

			if	(nr_seq_motivo_solic_w > 0) then
			begin
				update	ordem_compra
				set		nr_seq_motivo_solic = nr_seq_motivo_solic_w
				where	nr_ordem_compra = nr_ordem_compra_w;
			end;
			end if;
		end if;
		
		update	ordem_compra
		set	cd_local_entrega	= cd_local_estoque_w,
			ie_urgente		= ie_urgente_w,
			ie_aviso_chegada	= ie_aviso_chegada_w
		where	nr_ordem_compra		= nr_ordem_compra_w;
		
		if	(ie_liberar_ordem_cotacao_w in ('A','S')) then
			liberar_ordem_sintese(nr_ordem_compra_w, ie_liberar_ordem_cotacao_w, nm_usuario_p);
			
			select	dt_aprovacao
			into	dt_aprovacao_w
			from	ordem_compra
			where	nr_ordem_compra = nr_ordem_compra_w;
			
			if	(dt_aprovacao_w is not null) then
				ie_aprovada_w	:= 'S';			
			end if;
		end if;	
		
		end;
	end loop;
	close C01;
	
	select	count(*)
	into	qt_registro_w
	from	ordem_compra_item
	where	nr_cot_compra = nr_cot_compra_w;
	
	if	(qt_registro_w > 0) then
		alt_dt_geracao_ordem_compra(nm_usuario_p, nr_cot_compra_w);	
	end if;
	
else
	if	(qt_ordens_w > 0) then
		ie_erro_w := 'S';
		ds_erro_w := wheb_mensagem_pck.get_texto(302976, 'NR_COT_COMPRA=' || nr_cot_compra_w) /* JA EXISTE ORDEM DE COMPRA EM ANDAMENTO PARA A COTACAO NUMERO #@NR_COT_COMPRA#@. */
			|| ' ' || wheb_mensagem_pck.get_texto(156580) || ' ' || cd_fornecedor_ww; /* Fornecedor cd_fornecedor_ww */
		inserir_log_sintese('ERRO','ORDEM', ds_erro_w, '00025', nr_cot_compra_w, '' ,nm_usuario_p);
	elsif	(qt_registro_w = 0) then
		ie_erro_w := 'S';
		/* NAO EXISTE NO TASY A COTACAO NUMERO #@NR_COT_COMPRA#@. */
		ds_erro_w := wheb_mensagem_pck.get_texto(302984, 'NR_COT_COMPRA=' || nr_cot_compra_w);
		inserir_log_sintese('ERRO','ORDEM', ds_erro_w, '00010', nr_cot_compra_w, '' ,nm_usuario_p);
	elsif	(qt_registro_fornec_w = 0) then
		ie_erro_w := 'S';
		/* NAO EXISTE PRECOS CADASTRADOS NA COTACAO NUMERO #@NR_COT_COMPRA#@. */
		ds_erro_w := wheb_mensagem_pck.get_texto(302981, 'NR_COT_COMPRA=' || nr_cot_compra_w);
		inserir_log_sintese('ERRO','ORDEM', ds_erro_w, '00020', nr_cot_compra_w, '' ,nm_usuario_p);
	end if;
end if;

select	count(*)
into	qt_registro_w
from	ordem_compra_item
where	nr_cot_compra = nr_cot_compra_w;

if	(qt_registro_w > 0) and
	(ie_erro_w = 'N') then
	/* ORDEM DE COMPRA IMPORTADA COM SUCESSO. */
	inserir_log_sintese('OK','ORDEM',wheb_mensagem_pck.get_texto(302988), '', nr_cot_compra_w, '', nm_usuario_p);
	
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_w;

	select	NVL(obter_valor_param_usuario(915, 46, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N')
	into	ie_gera_nova_cotacao_w
	from	dual;
	
	if 	(ie_gera_nova_cotacao_w <> 'N') then				
		sup_exclui_item_nao_cotado(nr_cot_compra_w,'S',nm_usuario_p);
	end if;
	
	begin
	gerar_historico_cotacao(nr_cot_compra_w, wheb_mensagem_pck.get_texto(302991), 
		wheb_mensagem_pck.get_texto(302992) /* Ordem importada com sucesso do portal Sintese. */
		|| chr(13) || chr(10) || wheb_mensagem_pck.get_texto(299129) || ' ' || nr_ordem_compra_w
		|| chr(13) || chr(10) || wheb_mensagem_pck.get_texto(998486) || ' ' || nr_ordem_sintese_w, 'S', nm_usuario_p);
	exception
	when others then
		ds_erro_w := ds_erro_w;
	end;	
elsif	(ie_erro_w = 'S') then	
	delete from ordem_compra where nr_ordem_compra = nvl(nr_ordem_compra_w,0);
	begin
	gerar_historico_cotacao(nr_cot_compra_w, wheb_mensagem_pck.get_texto(302993), wheb_mensagem_pck.get_texto(302994, 'DS_ERRO=' || ds_erro_w),'S',nm_usuario_p);
	exception
	when others then
		ds_erro_w := ds_erro_w;
	end;
end if;

delete	from w_sintese_oc
where	nr_sequencia = nr_cot_compra_p;

ie_aprovada_p		:= ie_aprovada_w;
nr_ordem_compra_p	:= nr_ordem_compra_w;

commit;

end importa_ordem_sintese;
/