create or replace
function pls_obter_regra_campo_ptu_mat(	dt_referencia_p			date,
					ie_tipo_intercambio_p		pls_regra_campo_ptu_mat.ie_tipo_intercambio%type,
					ie_tipo_despesa_p		pls_regra_campo_ptu_mat.ie_tipo_despesa_mat%type,
					cd_material_p			varchar2,
					ie_tipo_guia_p			pls_regra_campo_ptu_mat.ie_tipo_guia%type,
					ds_campo_p			pls_regra_campo_ptu_mat.ds_campo%type )
						return varchar2 is

ie_retorno_w		varchar2(5) := 'N';	
nr_seq_regra_w		pls_regra_campo_ptu_mat.nr_sequencia%type;
dt_referencia_w		date := trunc(dt_referencia_p);
						
begin

select	max(nr_sequencia)
into	nr_seq_regra_w
from	pls_regra_campo_ptu_mat
where	dt_referencia_w between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_referencia_w)
and	((cd_material_inicial is null and cd_material_final is null) or (cd_material_p between cd_material_inicial and cd_material_final))
and	(ie_tipo_intercambio = 'A' or ie_tipo_intercambio = ie_tipo_intercambio_p)
and	(ie_tipo_despesa_mat is null or ie_tipo_despesa_mat = ie_tipo_despesa_p)
and	(ie_tipo_guia is null or ie_tipo_guia = ie_tipo_guia_p)
and	ds_campo	= ds_campo_p;

if	(nr_seq_regra_w is not null) then
	ie_retorno_w := 'S';
end if;

return	ie_retorno_w;

end pls_obter_regra_campo_ptu_mat;
/