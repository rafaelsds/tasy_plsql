create or replace procedure suspender_prescr_mat_hor	(	nr_sequencia_p		number,
						nm_usuario_p		varchar2,
						nr_seq_motivo_susp_p	number,
						ds_motivo_susp_p	varchar2,
						ie_evento_duplicidade_p	varchar2,
						ds_observacao_p		varchar2) is

nr_prescricao_w		number(14,0);
nr_seq_material_w	number(6,0);
nr_seq_horario_w	number(14,0);
nr_agrupamento_w	number(14,0);
nr_seq_composto_w	number(14,0);
dt_horario_w		date;
ie_agrupador_w		number(14,0);
ie_consistir_hora_w	varchar2(1);
ie_gravar_justif_w	varchar2(1);
nr_atendimento_w	number(15,0);
cd_material_w		number(15,0);
nr_seq_alteracao_w	number(15,0);
ie_arq_pyxis_w		varchar2(1);
cd_estabelecimento_w	number(4);
qt_existe_regra_w	number(10);
nr_seq_lote_w		number(10);
qt_lote_w		number(10);
ie_status_lote_w	varchar2(5);
nr_seq_processo_w prescr_mat_hor.nr_seq_processo%type;
qt_processo_w     number(5);

cursor c01 is
select	nr_sequencia
from	prescr_material
where	nr_prescricao	= nr_prescricao_w
and	nr_sequencia	<> nr_seq_material_w
and	nr_agrupamento	= nr_agrupamento_w
and	ie_agrupador	= 1
and	nr_seq_kit	is null;

begin
ie_consistir_hora_w := consiste_hora_suspensao(null,nm_usuario_p,nr_sequencia_p,null,'H');

gravar_log_cpoe('CPOE_SUSPENDER_HOR_ITEM_PRESCR nr_sequencia_p: '||nr_sequencia_p||
		' nr_seq_motivo_susp_p: '||nr_seq_motivo_susp_p||
		' ds_motivo_susp_p: '||ds_motivo_susp_p||
		' ie_evento_duplicidade_p: '||ie_evento_duplicidade_p||
		' ds_observacao_p: '||ds_observacao_p||
		' nm_usuario_p: '||ds_observacao_p||
		' ie_consistir_hora_w: ' || ie_consistir_hora_w||
		' get_ie_commit: ' || wheb_usuario_pck.get_ie_commit);

if	(ie_consistir_hora_w = 'S') then
	--'Nao e possivel suspender o horario! Parametro[801]';
	Wheb_mensagem_pck.exibir_mensagem_abort(214137);
end if;

update	prescr_mat_hor
set	dt_suspensao		= sysdate,
	nm_usuario_susp		= nm_usuario_p,
	nr_seq_motivo_susp	= decode(nr_seq_motivo_susp_p,0,null,nr_seq_motivo_susp_p),
	ds_motivo_susp          = ds_motivo_susp_p
where	nr_sequencia 		= nr_sequencia_p
and	dt_fim_horario	is null
and	dt_suspensao	is null;

select	a.nr_prescricao,
	a.nr_seq_material,
	a.dt_horario,
	b.nr_atendimento,
	a.cd_material,
	b.cd_estabelecimento,
	a.nr_seq_lote,
  a.nr_seq_processo
into	nr_prescricao_w,
	nr_seq_material_w,
	dt_horario_w,
	nr_atendimento_w,
	cd_material_w,
	cd_estabelecimento_w,
	nr_seq_lote_w,
  nr_seq_processo_w
from	prescr_mat_hor a,
	prescr_medica b
where	a.nr_sequencia = nr_sequencia_p
and	a.nr_prescricao = b.nr_prescricao;

update	prescr_material
set	ie_horario_susp	= 'S'
where	nr_prescricao	= nr_prescricao_w
and	nr_sequencia	= nr_seq_material_w;

update	prescr_mat_hor
set	dt_suspensao		= sysdate,
	nm_usuario_susp		= nm_usuario_p,
	nr_seq_motivo_susp	= decode(nr_seq_motivo_susp_p,0,null,nr_seq_motivo_susp_p),
	ds_motivo_susp          = ds_motivo_susp_p
where	nr_prescricao		= nr_prescricao_w
and	nr_seq_superior		= nr_seq_material_w
and	dt_horario		= dt_horario_w
and	dt_fim_horario		is null
and	dt_suspensao		is null;

select	max(nr_agrupamento),
	max(ie_agrupador)
into	nr_agrupamento_w,
	ie_agrupador_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_w
and	nr_sequencia	= nr_seq_material_w;

Obter_Param_Usuario(924,890,obter_perfil_ativo,nm_usuario_p,0,ie_gravar_justif_w);

if	(ie_agrupador_w 			= 1) and
	(nvl(ie_evento_duplicidade_p,'S')	= 'S') then

	select	prescr_mat_alteracao_seq.nextval
	into	nr_seq_alteracao_w
	from	dual;

	insert	into prescr_mat_alteracao
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_prescricao,
		nr_seq_prescricao,
		nr_seq_horario,
		dt_alteracao,
		cd_pessoa_fisica,
		ie_alteracao,
		ds_justificativa,
		ie_tipo_item,
		dt_horario,
		nr_atendimento,
		cd_item,
		ds_observacao)
	values	(nr_seq_alteracao_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_prescricao_w,
		nr_seq_material_w,
		nr_sequencia_p,
		sysdate,
		obter_dados_usuario_opcao(nm_usuario_p,'C'),
		12,
		decode(ie_gravar_justif_w,'S',OBTER_DESC_EXPRESSAO(727956)), --'Liberacao de itens duplicados'
		decode(ie_agrupador_w, 1, 'M', 'MAT'),
		dt_horario_w,
		nr_atendimento_w,
		cd_material_w,
		ds_observacao_p);
end if;

open C01;
loop
fetch C01 into
	nr_seq_composto_w;
exit when C01%notfound;
	select	max(nr_sequencia)
	into	nr_seq_horario_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_w
	and	nr_seq_material	= nr_seq_composto_w
	and	dt_horario	= dt_horario_w
	and	dt_fim_horario		is null
	and	dt_suspensao		is null;

	if	(nvl(nr_seq_horario_w,0) > 0) then
		update	prescr_mat_hor
		set	dt_suspensao		= sysdate,
			nm_usuario_susp		= nm_usuario_p,
			nr_seq_motivo_susp	= decode(nr_seq_motivo_susp_p,0,null,nr_seq_motivo_susp_p),
			ds_motivo_susp          = ds_motivo_susp_p
		where	nr_sequencia 		= nr_seq_horario_w
		and	dt_fim_horario	is null
		and	dt_suspensao	is null;

		update	prescr_material
		set	ie_horario_susp	= 'S'
		where	nr_prescricao	= nr_prescricao_w
		and	nr_sequencia	= nr_seq_composto_w;

	end if;

end loop;
close C01;

select 	nvl(max(ie_arq_pyxis),'N')
into	ie_arq_pyxis_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_arq_pyxis_w = 'S') then
	begin
	select 	count(*)
	into	qt_existe_regra_w
	from	dis_regra_local
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	(nvl(qt_existe_regra_w,0) > 0) then
		dis_gerar_arq_prescr(nr_prescricao_w,'2');
	end if;
	end;
end if;

update	ap_lote_item
set	dt_supensao	= sysdate,
	nm_usuario_susp	= nm_usuario_p
where	nr_seq_lote	= nr_seq_lote_w
and	nr_seq_mat_hor	= nr_sequencia_p
and	dt_supensao	is null;

/*Verifica se todos os itens pendentes, ja estao suspensos.. se sim. suspende o lote*/
select	count(*)
into	qt_lote_w
from	ap_lote_item
where	nr_seq_lote	= nr_seq_lote_w
and	dt_supensao	is null;

-- Somente se ele estiver com Status Gerado
if	(qt_lote_w = 0) then
	select	max(ie_status_lote)
	into	ie_status_lote_w
	from	ap_lote
	where	nr_sequencia	= nr_seq_lote_w;
	if	(ie_status_lote_w = 'G') then
		update	ap_lote
		set	ie_status_lote = 'S'
		where	nr_sequencia	= nr_seq_lote_w;

		insert into ap_lote_historico(
			nr_sequencia,			dt_atualizacao,
			nm_usuario,			nr_seq_lote,
			ds_evento,			ds_log)
		values(	ap_lote_historico_seq.nextval,	sysdate,
			nm_usuario_p,			nr_seq_lote_w,
			--'Suspensao do lote',		'Lote suspenso na suspensao do horario:'||nr_sequencia_p||' - SUSPENDER_PRESCR_MAT_HOR.');
			OBTER_DESC_EXPRESSAO(348748),		WHEB_MENSAGEM_PCK.get_texto(456876,'nr_sequencia_p='|| nr_sequencia_p) || ' - SUSPENDER_PRESCR_MAT_HOR.');
	end if;
end if;

if(ie_agrupador_w in (1,2,8,12,16)) then
    
  select nvl(count(*), 0)
  into   qt_processo_w
  from   prescr_mat_hor
  where  nr_seq_processo = nr_seq_processo_w
  and    dt_suspensao is null;     
                    
  if(qt_processo_w = 0) then
    update	adep_processo
    set		  ie_inconsistencia	= 'S',
            dt_cancelamento		= sysdate,
            nm_usuario_cancelamento = nm_usuario_p,
            dt_suspensao = sysdate,
            nm_usuario_susp = nm_usuario_p
    where	  nr_sequencia		= nr_seq_processo_w;
  end if;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end suspender_prescr_mat_hor;
/