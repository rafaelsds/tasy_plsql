create or replace procedure pfcs_db_tl_disbyunit_cen(
    nr_seq_indicator_p   number,
    cd_estabelecimento_p number,
    nm_usuario_p         varchar2) is

nr_seq_panel_w				pfcs_panel.nr_sequencia%type;
qt_time_telemetry_rule_w 	pfcs_telemetry_config.qt_time_on_tele__rule%type;

cursor cur_get_tot_review_pfcs is
	select ds_unit,
		sum(qt_orders_written) qt_orders_written,
		sum(qt_patients_to_review) qt_patients_to_review
    from (
        select 	sa.ds_setor_atendimento ds_unit,
				count(enc.nr_sequencia)  qt_orders_written,
				0 qt_patients_to_review
        from 	pfcs_service_request sr,
				pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_encounter_location el,
				pfcs_device pd,
				unidade_atendimento uni,
				setor_atendimento sa
        where 	sr.si_status =  'ACTIVE'
		and 	sr.cd_service = 'E0403'
		and 	sr.nr_seq_encounter = enc.nr_sequencia
		and 	enc.si_status in ('PLANNED', 'ARRIVED')
		and 	enc.nr_seq_patient = pat.nr_sequencia
		and 	pat.ie_active = '1'
		and 	pat.nr_sequencia = pd.nr_seq_patient
		and 	pd.si_status = 'ACTIVE'
		and 	pd.ds_device_type = 'Monitor'
		and 	enc.nr_sequencia = el.nr_seq_encounter
		and 	el.nr_seq_location = uni.nr_seq_location
		and 	uni.ie_situacao = 'A'
		and 	uni.cd_setor_atendimento = sa.cd_setor_atendimento
		and 	sa.ie_situacao = 'A'
		and 	sa.cd_classif_setor in ('1','3','4','9','11','12')       
		and 	sa.cd_estabelecimento_base = cd_estabelecimento_p
		group by sa.ds_setor_atendimento
    union
        select 	sa.ds_setor_atendimento ds_unit,
				0 qt_orders_written,
				count(enc.nr_sequencia) qt_patients_to_review
        from 	pfcs_service_request sr,
				pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_encounter_location el,
				pfcs_device pd,
				unidade_atendimento uni,
				setor_atendimento sa
		where 	sr.si_status = 'COMPLETED'
		and 	sr.cd_service = 'E0404'
		and 	sr.nr_seq_encounter = enc.nr_sequencia
		and 	enc.si_status in ('PLANNED', 'ARRIVED')
		and 	enc.nr_seq_patient = pat.nr_sequencia
		and 	pat.ie_active = '1'
		and 	pat.nr_sequencia = pd.nr_seq_patient
		and 	pd.si_status = 'ACTIVE'
		and 	pd.ds_device_type = 'Monitor'
		and 	enc.nr_sequencia = el.nr_seq_encounter
		and 	el.nr_seq_location = uni.nr_seq_location
		and 	uni.ie_situacao = 'A'
		and 	uni.cd_setor_atendimento = sa.cd_setor_atendimento
		and 	sa.ie_situacao = 'A'
		and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
		and 	sa.cd_estabelecimento_base = cd_estabelecimento_p		
		and 	pfcs_get_tele_time(pd.nr_sequencia, 'S') > (qt_time_telemetry_rule_w * 60)
        group by sa.ds_setor_atendimento)
    group by ds_unit;

begin
	-- Get hours configurable from telemetry settings
	select	nvl(max(qt_time_on_tele__rule), 12)
	into	qt_time_telemetry_rule_w
	from	pfcs_telemetry_config;

	for c01_w in cur_get_tot_review_pfcs loop

      pfcs_pck_v2.pfcs_generate_results (
        vl_indicator_p => c01_w.qt_orders_written,
        vl_indicator_aux_p => c01_w.qt_patients_to_review,
        ds_reference_value_p => c01_w.ds_unit,
        nr_seq_indicator_p => nr_seq_indicator_p,
        nr_seq_operational_level_p => cd_estabelecimento_p,
        nm_usuario_p => nm_usuario_p,
        nr_seq_panel_p => nr_seq_panel_w );

	end loop;

commit;

  pfcs_pck_v2.pfcs_activate_records (
    nr_seq_indicator_p => nr_seq_indicator_p,
    nr_seq_operational_level_p => cd_estabelecimento_p,
    nm_usuario_p => nm_usuario_p );

end pfcs_db_tl_disbyunit_cen;
/
