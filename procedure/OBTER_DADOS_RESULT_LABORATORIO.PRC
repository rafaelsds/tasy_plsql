create or replace
procedure obter_dados_result_laboratorio(	nr_seq_result_p 	in 	number,
					ds_resultado_p 		out 	long,
					qt_item_evol_p 		out 	number,
					ie_formato_texto_p 	out 	number) is

ds_resultado_w 		long;
ie_formato_texto_w 	number(10);
qt_item_evol_w 		number(10);
qt_registros_w		number(5);

begin

begin
	select 	ds_resultado,
		ie_formato_texto
	into	ds_resultado_w,
		ie_formato_texto_w
	from 	result_laboratorio
	where 	nr_sequencia = nr_seq_result_p;
exception when others then
	null;
end;

select 	count(*)
into 	qt_item_evol_w
from 	result_laboratorio_evol
where 	nr_seq_result_lab = nr_seq_result_p;

ds_resultado_p 		:= ds_resultado_w;
qt_item_evol_p 		:= qt_item_evol_w;
ie_formato_texto_p 	:= ie_formato_texto_w;

end obter_dados_result_laboratorio;
/
