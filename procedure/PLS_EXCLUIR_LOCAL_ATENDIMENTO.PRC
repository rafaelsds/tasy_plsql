create or replace
procedure pls_excluir_local_atendimento(nr_seq_local_p		Number,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2) is 
					
Cursor C01 is
	select	nr_sequencia
	from	local_atend_med_maquina
	where	nr_seq_local_atend  = nr_seq_local_p;	
	
nr_seq_maquina_w	 Number(10);
	
begin

if	(nr_seq_local_p is not null) then 

	open C01;
	loop
	fetch C01 into	
		nr_seq_maquina_w;
	exit when C01%notfound;
	
		update local_atend_med_maq_finger 
		set	dt_fim_licenca = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where nr_seq_maquina = nr_seq_maquina_w;
	
	end loop;
	close C01;
	
	update  local_atend_med_maquina 
	set     ie_situacao = 'I',
	        nm_usuario = nm_usuario_p,
	        dt_atualizacao = sysdate
	where   nr_seq_local_atend  = nr_seq_local_p;	
	
	update  local_atend_med_prest 
	set     nm_usuario = nm_usuario_p,
	        dt_atualizacao = sysdate,
	        dt_fim_vigencia = sysdate
	where   nr_seq_local_atend = nr_seq_local_p;
	
	update  local_atendimento_medico 
	set	ie_situacao = 'I',
	        nm_usuario = nm_usuario_p,
	        dt_atualizacao = sysdate
	where   nr_sequencia = nr_seq_local_p;
	
	commit;
end if;

end pls_excluir_local_atendimento;
/
