create or replace
procedure salvar_atendimento_gv(
			nr_sequencia_p		number,
			nr_atendimento_p	number) is 

begin

if	(nvl(nr_sequencia_p,0) > 0) and
	(nvl(nr_atendimento_p,0) > 0) then
	
	update	gestao_vaga
	set	nr_atendimento = nr_atendimento_p
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end salvar_atendimento_gv;
/