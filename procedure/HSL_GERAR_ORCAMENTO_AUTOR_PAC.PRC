create or replace
procedure HSL_GERAR_ORCAMENTO_AUTOR_PAC(nr_seq_paciente_setor_p	number,
					nr_seq_atendimento_p	number,
					nr_ciclo_p		number,					
					cd_convenio_p		number,
					cd_categoria_p		varchar2,
					cd_plano_convenio_p	varchar2,
					nm_usuario_p		varchar2) is

/*Procedure desenvolvida na OS 671116
De acordo com o tipo do conv�nio, ser� gerada um Or�amento para o paciente ou Autoriza��o Conv�nio
*/

ie_tipo_convenio_w	convenio.ie_tipo_convenio%type;

begin

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio	= cd_convenio_p;

if	(ie_tipo_convenio_w = 1) then --Particular

	HSL_GERAR_ORCAMENTO_QUIMIO(	nr_seq_paciente_setor_p,
					nr_seq_atendimento_p,
					cd_convenio_p,
					cd_categoria_p,
					cd_plano_convenio_p,
					nm_usuario_p);

elsif	(ie_tipo_convenio_w = 2) then --Conv�nio

	HSL_GERAR_AUTOR_CONV_QUIMIO(	nr_seq_paciente_setor_p,
					nr_ciclo_p,
					cd_convenio_p,
					cd_categoria_p,
					cd_plano_convenio_p,
					nm_usuario_p);
end if;

commit;

end HSL_GERAR_ORCAMENTO_AUTOR_PAC;
/