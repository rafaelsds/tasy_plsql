create or replace
procedure Ageint_Obter_Se_Cons_Agenda(
			cd_pessoa_Fisica_p		varchar2,
			dt_agenda_p				date,
			cd_estabelecimento_p	number,
			nm_usuario_p			Varchar2,
			ie_consulta_p	out		varchar2,
			ie_exame_p		out		varchar2,
			ie_quimio_p		out		varchar2) is 

qt_agenda_w		number(10);
ie_exame_w		varchar2(1)	:= 'N';
ie_consulta_w	varchar2(1)	:= 'N';
ie_quimio_w		varchar2(1)	:= 'N';
			
begin

begin
	select	1
	into	qt_agenda_w
	from	agenda_paciente
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and		hr_inicio >= sysdate
	and		rownum	= 1;
exception
when no_data_found then
	qt_agenda_w	:= 0;
end;

if	(qt_agenda_w	> 0) then
	ie_exame_w	:= 'S';
end if;

begin
	select	1
	into	qt_agenda_w
	from	agenda_consulta
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and		dt_agenda >= sysdate
	and		rownum	= 1;
exception
when no_data_found then
	qt_agenda_w	:= 0;
end;

if	(qt_agenda_w	> 0) then
	ie_consulta_w	:= 'S';
end if;

begin
	select	1
	into	qt_agenda_w
	from	agenda_quimio
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and		dt_agenda >= sysdate
	and		rownum	= 1;
exception
when no_data_found then
	qt_agenda_w	:= 0;
end;

if	(qt_agenda_w	> 0) then
	ie_quimio_w	:= 'S';
end if;

ie_exame_p		:= ie_exame_w;
ie_consulta_p	:= ie_consulta_w;
ie_quimio_p		:= ie_quimio_w;

commit;

end Ageint_Obter_Se_Cons_Agenda;
/