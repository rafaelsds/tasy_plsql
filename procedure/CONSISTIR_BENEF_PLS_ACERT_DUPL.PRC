create or replace
procedure consistir_benef_pls_acert_dupl	(cd_pessoa_fisica_p		varchar2,
						ie_consistencia_p out	varchar2) is

ie_beneficiario_w	varchar2(1);
ie_revisar_w		varchar2(1);
ie_consistencia_w	varchar2(1) := 'N';
ie_benef_inativo_w	varchar2(10);

begin
if	(cd_pessoa_fisica_p is not null) then

	ie_benef_inativo_w	:= nvl(obter_valor_param_usuario(5, 227, Obter_Perfil_Ativo, wheb_usuario_pck.GET_NM_USUARIO, wheb_usuario_pck.GET_cd_estabelecimento), 'N');

	/* obter se beneficiario pls */
	select	decode(count(*),0,'N','S')
	into	ie_beneficiario_w
        from    pls_segurado_carteira y,
        	pls_segurado x
	where	y.nr_seq_segurado = x.nr_sequencia
	and	y.dt_inicio_vigencia <= sysdate
	and	nvl(y.dt_validade_carteira,sysdate) >= sysdate
	and	x.dt_liberacao is not null
	and	((ie_benef_inativo_w = 'N' and x.dt_rescisao is null) or (ie_benef_inativo_w = 'S'))
        and     x.cd_pessoa_fisica = cd_pessoa_fisica_p;

	if	(ie_beneficiario_w = 'S') then
		ie_consistencia_w := 'B';
	else
		/* obter se cadastro revisado */
		select	nvl(max(ie_revisar),'N')
		into	ie_revisar_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;

		if	(ie_revisar_w = 'S') then
			ie_consistencia_w := 'R';
		end if;
	end if;
end if;

ie_consistencia_p := ie_consistencia_w;

end consistir_benef_pls_acert_dupl;
/
