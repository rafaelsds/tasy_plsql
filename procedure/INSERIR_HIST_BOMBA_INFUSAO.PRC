create or replace procedure inserir_hist_bomba_infusao(	nr_sequencia_p      in bomba_infusao.nr_sequencia%type,
							nm_usuario_p        in bomba_infusao.nm_usuario%type,
							ie_tipo_historico_p in bomba_infusao_hist.ie_tipo_historico%type,
							ie_commit           in varchar2 default 'S',
							nr_seq_canal_bomba_p in bomba_infusao.NR_SEQ_CANAL_BOMBA%type default null) is

	nr_sequencia_w bomba_infusao_hist.nr_sequencia%type;
	ds_historico_w bomba_infusao_hist.ds_historico%type;

begin

	begin

		select	bomba_infusao_hist_seq.nextval
		into	nr_sequencia_w
		from	dual;

		if ( ie_tipo_historico_p = 'AS' ) then
			ds_historico_w := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 1118698) || ' ' || nm_usuario_p;
		else
			ds_historico_w := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 1118699) || ' ' || nm_usuario_p;
		end if;

		insert into bomba_infusao_hist (	nr_sequencia,
							nr_seq_bomba,
							ds_historico,
							dt_historico,
							ie_tipo_historico,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nr_seq_canal_bomba)
		values				(	nr_sequencia_w,
							nr_sequencia_p,
							ds_historico_w,
							sysdate,
							ie_tipo_historico_P,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nr_seq_canal_bomba_p);
	end;

	if (ie_commit = 'S') then
		commit;
	end if;

end inserir_hist_bomba_infusao;
/
