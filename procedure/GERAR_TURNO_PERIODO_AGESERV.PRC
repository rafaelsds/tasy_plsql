create or replace
procedure Gerar_turno_periodo_ageserv(cd_agenda_p		number,
				      ie_frequencia_p		varchar2,
				      ie_dia_semana_p		number,
				      hr_inicial_p		varchar2,
				      hr_final_p		varchar2,
				      nr_min_duracao_p		number,
				      cd_classificacao_p 	varchar2,
				      qt_classif_p		number,
				      cd_estabelecimento_p	number,
				      nm_usuario_p		varchar2,
				      cd_medico_exec_p		varchar2,
				      cd_medico_solic_p		varchar2,
					  dt_inicio_vigencia_p  date,
					  dt_final_vigencia_p	date,
            qt_idade_min_p      number default null,
            qt_idade_max_p      number  default null) is 

dt_atual_w	date;
hr_final_w	date;
nr_seq_turno_w	number(10,0);	
nr_seq_classif_w	number(10,0);			
				      
begin

begin
dt_atual_w	:= to_date('30/12/1899'|| ' ' || hr_inicial_p,'dd/mm/yyyy hh24:mi:ss');
hr_final_w	:= to_date('30/12/1899'|| ' ' || hr_final_p,'dd/mm/yyyy hh24:mi:ss');
exception
when others then
	Wheb_mensagem_pck.exibir_mensagem_abort(262527);
end;

if	(qt_classif_p > 0) and
	(nr_min_duracao_p > 0) then

	while (dt_atual_w < hr_final_w) loop 
		begin
		
		select	 agenda_turno_seq.nextval
		into	nr_seq_turno_w
		from	dual;
			
		insert into agenda_turno(nr_sequencia,
					cd_agenda,
					ie_dia_semana,          
					hr_inicial,             
					hr_final,               
					nr_minuto_intervalo,
					nm_usuario,     
					dt_atualizacao,
					ie_encaixe,
					ie_frequencia,
					qt_total_turno,
					dt_inicio_vigencia,
					dt_final_vigencia,
          qt_idade_min,
          qt_idade_max)
				values	(nr_seq_turno_w,
					cd_agenda_p,
					ie_dia_semana_p,
					dt_atual_w,
					dt_atual_w + (nr_min_duracao_p/1440),
					nr_min_duracao_p,
					nm_usuario_p,
					sysdate,
					'N',
					ie_frequencia_p,
					0,
					dt_inicio_vigencia_p,
					dt_final_vigencia_p,
          qt_idade_min_p,
          qt_idade_max_p);
		
		select	agenda_turno_classif_seq.nextval
		into	nr_seq_classif_w
		from	dual;
		
		insert into agenda_turno_classif(nr_sequencia,           
						 nr_seq_turno,           
						 cd_classificacao,       
						 dt_atualizacao,         
						 nm_usuario,             
						 qt_classif,
						 ie_gera_feriado,
						 cd_medico,
						 cd_medico_solic)
					values	(nr_seq_classif_w,
						nr_seq_turno_w,
						cd_classificacao_p,
						sysdate,
						nm_usuario_p,
						qt_classif_p,
						'S',
						cd_medico_exec_p,
						cd_medico_solic_p);
		
		dt_atual_w	:= dt_atual_w + (nr_min_duracao_p/1440);
		
		end;
	end loop;

	commit;
end if;
	
end Gerar_turno_periodo_ageserv;
/
