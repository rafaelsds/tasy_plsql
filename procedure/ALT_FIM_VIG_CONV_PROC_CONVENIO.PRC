create or replace
procedure alt_fim_vig_conv_proc_convenio(nr_sequencia_p number,
						dt_vigencia_final_p date) is

begin

update	conversao_proc_convenio
set	dt_vigencia_final	= dt_vigencia_final_p 
where	nr_sequencia		= nr_sequencia_p;

commit;	
end alt_fim_vig_conv_proc_convenio;
/