create or replace
procedure sus_update_modalidade(
			nm_usuario_p		Varchar2) is 

begin

update	sus_modalidade
set     ie_situacao =  'I',
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate;

commit;

end sus_update_modalidade;
/