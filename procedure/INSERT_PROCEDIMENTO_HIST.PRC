create or replace
procedure insert_procedimento_hist (
		ds_historico_p		varchar2,
		cd_procedimento_p	number,
		ie_origem_p		number,
		ds_titulo_p		varchar2,
		nm_usuario_p		varchar2) is

begin

insert into procedimento_hist (  
	nr_sequencia,         
	dt_atualizacao,       
	nm_usuario,           
	ds_historico,         
	cd_procedimento, 
	ie_origem_proced, 
	ds_titulo)    
values (procedimento_hist_seq.nextval,           
	sysdate,      
	nm_usuario_p,
	ds_historico_p,
	cd_procedimento_p,
	ie_origem_p,
	ds_titulo_p);
	
commit;

end insert_procedimento_hist;
/