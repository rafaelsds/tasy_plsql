CREATE OR REPLACE
PROCEDURE Duplicar_Proc_Paciente(	
				nr_sequencia_p  		Number,
				nm_usuario_p			Varchar2,
				nr_seq_gerada_p	out		Number) is

nr_sequencia_w			Number(10,0);


BEGIN

select Procedimento_paciente_seq.nextval
into nr_sequencia_w
from dual;

insert into Procedimento_paciente(
	nr_sequencia, nr_atendimento, dt_entrada_unidade, cd_procedimento,
	dt_procedimento, qt_procedimento, dt_atualizacao, nm_usuario,
	cd_medico, cd_convenio, cd_categoria, cd_pessoa_fisica, dt_prescricao,
	ds_observacao, vl_procedimento, vl_medico, vl_anestesista, 
	vl_materiais, cd_edicao_amb, cd_tabela_servico, dt_vigencia_preco,
	cd_procedimento_princ, dt_procedimento_princ, dt_acerto_conta,
	dt_acerto_convenio, dt_acerto_medico, vl_auxiliares, vl_custo_operacional,
	tx_medico, tx_anestesia, nr_prescricao, nr_sequencia_prescricao,
	cd_motivo_exc_conta, ds_compl_motivo_excon, cd_acao, qt_devolvida,
	cd_motivo_devolucao, nr_cirurgia, nr_doc_convenio, cd_medico_executor,
	ie_cobra_pf_pj, nr_laudo, dt_conta, cd_setor_atendimento,
	cd_conta_contabil, cd_procedimento_aih, ie_origem_proced, nr_aih,
	ie_responsavel_credito, tx_procedimento, cd_equipamento, ie_valor_informado,
	cd_estabelecimento_custo, cd_tabela_custo, cd_situacao_glosa,
	nr_lote_contabil, cd_procedimento_convenio, nr_seq_autorizacao,
	ie_tipo_servico_sus, ie_tipo_ato_sus, cd_cgc_prestador, nr_nf_prestador,
	cd_atividade_prof_bpa, nr_interno_conta, nr_seq_proc_princ, ie_guia_informada,
	dt_inicio_procedimento, ie_emite_conta, ie_funcao_medico, ie_classif_sus,
	cd_especialidade, nm_usuario_original, nr_seq_proc_pacote, ie_tipo_proc_sus,
	cd_setor_receita, vl_adic_plant, qt_porte_anestesico, tx_hora_extra,
	ie_emite_conta_honor, nr_seq_atepacu, ie_proc_princ_atend, 
	cd_medico_req, ie_tipo_guia, ie_video, ie_auditoria, nr_seq_exame,nr_seq_aih,nr_seq_proc_interno, nr_seq_material,
	ie_via_acesso, ie_tecnica_utilizada)
select
	nr_sequencia_w, nr_atendimento, dt_entrada_unidade, cd_procedimento,
	dt_procedimento + 3/86400, qt_procedimento, sysdate, nm_usuario_p,
	cd_medico, cd_convenio, cd_categoria, cd_pessoa_fisica, dt_prescricao,
	ds_observacao, vl_procedimento, vl_medico, vl_anestesista, 
	vl_materiais, cd_edicao_amb, cd_tabela_servico, dt_vigencia_preco,
	cd_procedimento_princ, dt_procedimento_princ, dt_acerto_conta,
	dt_acerto_convenio, dt_acerto_medico, vl_auxiliares, vl_custo_operacional,
	tx_medico, tx_anestesia, nr_prescricao, nr_sequencia_prescricao,
	cd_motivo_exc_conta, ds_compl_motivo_excon, cd_acao, qt_devolvida,
	cd_motivo_devolucao, nr_cirurgia, nr_doc_convenio, cd_medico_executor,
	ie_cobra_pf_pj, nr_laudo, dt_conta, cd_setor_atendimento,
	cd_conta_contabil, cd_procedimento_aih, ie_origem_proced, nr_aih,
	ie_responsavel_credito, tx_procedimento, cd_equipamento, ie_valor_informado,
	cd_estabelecimento_custo, cd_tabela_custo, cd_situacao_glosa,
	nr_lote_contabil, cd_procedimento_convenio, nr_seq_autorizacao,
	ie_tipo_servico_sus, ie_tipo_ato_sus, cd_cgc_prestador, nr_nf_prestador,
	cd_atividade_prof_bpa, nr_interno_conta, nr_seq_proc_princ, ie_guia_informada,
	dt_inicio_procedimento, ie_emite_conta, ie_funcao_medico, ie_classif_sus,
	cd_especialidade, nm_usuario_original, nr_seq_proc_pacote, ie_tipo_proc_sus,
	cd_setor_receita, vl_adic_plant, qt_porte_anestesico, tx_hora_extra,
	ie_emite_conta_honor, nr_seq_atepacu, ie_proc_princ_atend,
	cd_medico_req, ie_tipo_guia, ie_video, ie_auditoria, nr_seq_exame,nr_seq_aih,nr_seq_proc_interno, nr_seq_material,
	ie_via_acesso, ie_tecnica_utilizada
from 	procedimento_paciente
where 	nr_sequencia = nr_sequencia_p;

nr_seq_gerada_p		:= nr_sequencia_w;

END Duplicar_Proc_Paciente;
/
