create or replace
procedure alterar_status_baixa_unid(data_p		varchar2,
				    ds_observacao_p	varchar2,
				    nr_seq_veiculo_p	number) is 
data_w	date;
begin
if (data_p is not null) then
	data_w := to_date(data_p,'dd/mm/yyyy');
else
	data_w := null;
end if;
	
update 	eme_veiculo 
set	dt_baixa = data_p,
	ds_observacao_baixa = ds_observacao_p
where	nr_sequencia = nr_seq_veiculo_p;

commit;

end alterar_status_baixa_unid;
/