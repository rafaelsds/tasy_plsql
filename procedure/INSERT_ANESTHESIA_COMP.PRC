create or replace procedure INSERT_ANESTHESIA_COMP (
		nr_sequencia_p		number,	
		nr_cirurgia_p	    number,
		nr_seq_pepo_p		number,
		nm_usuario_p		varchar2,	
		DS_LISTA_NR_SEQUENCIA_P	varchar2) is

ds_lista_w 		varchar2(10000) := '';
posicao_virgula_w 	integer := 0;
nr_max_loop_w 		integer := 10000;
nr_sequencia_w 		number(10,0) := 0;
tamanho_lista_w		number(10);
ds_observacao_w     varchar2(2000);
cd_complicacao_w    number(10);
cd_pessoa_fisica_w  varchar2(10) := obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C');
			
begin
ds_lista_w := DS_LISTA_NR_SEQUENCIA_P;

while 	ds_lista_w is not null and
	nr_max_loop_w > 0 loop
	begin
	tamanho_lista_w := length(ds_lista_w);
	posicao_virgula_w := instr(ds_lista_w, ',');
	
	if	(posicao_virgula_w > 1) and
		(substr(ds_lista_w, 1, posicao_virgula_w - 1) is not null) then
		begin
		nr_sequencia_w := to_number(substr(ds_lista_w, 1, posicao_virgula_w -1));
		ds_lista_w := substr(ds_lista_w, (posicao_virgula_w +1), tamanho_lista_w);
		end;
	elsif	(ds_lista_w is not null) then
		nr_sequencia_w := to_number(replace(ds_lista_w, ',', ''));
		ds_lista_w := '';
	end if;
	
	if	(nr_sequencia_w is not null) and
		(nr_sequencia_w > 0) then
		begin
			Select cd_complicacao,ds_observacao
			into cd_complicacao_w,ds_observacao_w
			from COMPLICACOES_ANEST_ROTINA where nvl(ie_situacao, 'A')  = 'A'
			and  nr_sequencia  =  nr_sequencia_w;
			
			INSERT INTO ANESTESIA_COMPLICACOES
			(NR_SEQUENCIA,DT_ATUALIZACAO,NM_USUARIO,DT_ATUALIZACAO_NREC,NM_USUARIO_NREC,CD_COMPLICACAO,
			DS_OBSERVACAO,IE_SITUACAO,NR_CIRURGIA,NR_SEQ_PEPO,CD_PROFISSIONAL)			
			VALUES (ANESTESIA_COMPLICACOES_SEQ.NEXTVAL,sysdate,nm_usuario_p,
			sysdate,nm_usuario_p,cd_complicacao_w,ds_observacao_w,'A',nr_cirurgia_p,nr_seq_pepo_p,cd_pessoa_fisica_w);
			commit;
		end;
	end if;
	nr_max_loop_w := nr_max_loop_w -1;
	end;
end loop;

commit;

end INSERT_ANESTHESIA_COMP;
/



