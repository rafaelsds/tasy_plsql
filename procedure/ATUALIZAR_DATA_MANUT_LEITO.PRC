create or replace
procedure atualizar_data_manut_leito(	nr_sequencia_p 	number,
										nm_usuario_p  	varchar2,
										ie_acao_p 		varchar2) is

begin

if (nvl(nr_sequencia_p,0) > 0) then
	if (ie_acao_p = 'I') then
	
		update sl_unid_atend
		set  dt_manutencao     = sysdate,
		nm_usuario_manutencao = nm_usuario_p
		where nr_sequencia    = nr_sequencia_p;
		
	elsif	(ie_acao_p = 'F')  then
	
		update sl_unid_atend
		set  dt_fim_manutencao     = sysdate,
		nm_usuario_fim_manutencao = nm_usuario_p
		where nr_sequencia       = nr_sequencia_p;
		
	end if;
end if;

commit;

end atualizar_data_manut_leito;
/