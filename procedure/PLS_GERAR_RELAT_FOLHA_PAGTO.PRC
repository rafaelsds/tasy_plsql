create or replace
procedure pls_gerar_relat_folha_pagto	(nr_seq_prestador_p	number,
					dt_inicio_comp_p	date,
					dt_final_comp_p		date,
					nm_usuario_p		varchar2) is 



nm_prestador_w			varchar2(255);
cd_prestador_w			varchar2(30);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
ie_tipo_pessoa_prest_w		varchar2(2);
vl_serv_prestado_w		number(15,2)	:= 0;
vl_complemento_w		number(15,2)	:= 0;
vl_complemento_pf_w		number(15,2)	:= 0;
vl_complemento_prest_w		number(15,2)	:= 0;
vl_desconto_w			number(15,2)	:= 0;
vl_glosa_w			number(15,2)	:= 0;
vl_sub_total_w			number(15,2)	:= 0;
vl_base_inss_w			number(15,2)	:= 0;
vl_inss_retido_w		number(15,2)	:= 0;
vl_base_irrf_w			number(15,2)	:= 0;
vl_irrf_retido_w		number(15,2)	:= 0;
vl_base_iss_w			number(15,2)	:= 0;
vl_inss_20_w			number(15,2)	:= 0;
vl_total_w			number(15,2)	:= 0;
vl_base_cofins_w		number(15,2)	:= 0;
vl_cofins_retido_w		number(15,2)	:= 0;
vl_base_pis_w			number(15,2)	:= 0;
vl_base_csll_w			number(15,2)	:= 0;
vl_sub_total_ww			number(15,2)	:= 0;
nr_seq_lote_w			number(10);
nr_seq_prestador_w		number(10);
nr_seq_pagto_prest_w		number(10);
nr_seq_relatorio_w		number(10);

Cursor C01 is
	select	a.nr_sequencia,
		b.nr_seq_prestador,
		b.nr_sequencia
	from	pls_pagamento_prestador	b,
		pls_lote_pagamento	a
	where	a.nr_sequencia	= b.nr_seq_lote
	and	a.dt_mes_competencia between trunc(nvl(dt_inicio_comp_p,sysdate)) and fim_dia(nvl(dt_final_comp_p, sysdate))
	and	(b.nr_seq_prestador = nr_seq_prestador_p or nr_seq_prestador_p is null)
	order by
		b.nr_seq_prestador;

begin
delete	w_pls_relat_folha_pagto
where	nm_usuario	= nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_seq_lote_w,
	nr_seq_prestador_w,
	nr_seq_pagto_prest_w;
exit when C01%notfound;
	begin
	vl_serv_prestado_w	:= 0;
	vl_complemento_w	:= 0;
	vl_desconto_w		:= 0;
	vl_sub_total_w		:= 0;
	vl_base_inss_w		:= 0;
	vl_inss_retido_w	:= 0;
	vl_base_irrf_w		:= 0;
	vl_irrf_retido_w	:= 0;
	vl_base_iss_w		:= 0;
	vl_inss_20_w		:= 0;
	vl_total_w		:= 0;
	vl_base_cofins_w	:= 0;
	vl_cofins_retido_w	:= 0;
	vl_complemento_prest_w	:= 0;
	vl_complemento_pf_w	:= 0;
	vl_base_pis_w		:= 0;
	vl_base_csll_w		:= 0;
	vl_sub_total_ww		:= 0;
	vl_glosa_w		:= 0;
	
	select	cd_prestador,
		substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc), 1, 255),
		decode(cd_pessoa_fisica, null, 'PJ', 'PF'),
		cd_cgc,
		cd_pessoa_fisica
	into	cd_prestador_w,
		nm_prestador_w,
		ie_tipo_pessoa_prest_w,
		cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_w;
	
	if	(cd_cgc_w is not null) then
		select	decode(ie_tipo_tributacao, '4', 'IS', 'PJ')
		into	ie_tipo_pessoa_prest_w
		from	pessoa_juridica
		where	cd_cgc	= cd_cgc_w;
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_relatorio_w
	from	w_pls_relat_folha_pagto
	where	nr_seq_prestador = nr_seq_prestador_w
	and	nm_usuario 	 = nm_usuario_p;
	
	if	(nr_seq_relatorio_w is null) then
		select	w_pls_relat_folha_pagto_seq.nextval
		into	nr_seq_relatorio_w
		from	dual;
	
		insert into w_pls_relat_folha_pagto
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_prestador,
			nr_seq_prestador,
			nm_prestador,
			ie_tipo_pessoa_prest,
			vl_serv_prestado,
			vl_complemento,
			vl_desconto,
			vl_sub_total,
			vl_base_inss,
			vl_inss_retido,
			vl_base_irrf,
			vl_irrf_retido,
			vl_base_iss,
			vl_inss_20,
			vl_total,
			vl_base_cofins,
			vl_cofins_retido,
			vl_base_pis,
			vl_base_csll)
		values	(nr_seq_relatorio_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_prestador_w,
			nr_seq_prestador_w,
			nm_prestador_w,
			ie_tipo_pessoa_prest_w,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0);
	end if;

	-- Serv. Prest.
	select	sum(nvl(vl_item, 0)) -- sum(nvl(vl_glosa, 0))
	into	vl_serv_prestado_w
	from	pls_pagamento_item a
	where	a.nr_seq_pagamento	= nr_seq_pagto_prest_w;
	
	-- Captation
	select	sum(nvl(x.vl_fixo, 0))
	into	vl_complemento_prest_w
	from	pls_prestador_pgto_fixo	x
	where	x.nr_seq_prestador_pf	= nr_seq_prestador_w
	and	sysdate between trunc(x.dt_inicio_vigencia) and fim_dia(nvl(x.dt_fim_vigencia, sysdate));
	
	select	sum(nvl(x.vl_fixo, 0))
	into	vl_complemento_pf_w
	from	pls_prestador_pgto_fixo	x
	where	x.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	sysdate between trunc(x.dt_inicio_vigencia) and fim_dia(nvl(x.dt_fim_vigencia, sysdate));
	
	vl_complemento_w := nvl(vl_complemento_prest_w, 0) + nvl(vl_complemento_pf_w, 0);
	
	-- Desconto
	select	abs(sum(nvl(x.vl_item, 0)))
	into	vl_desconto_w
	from	pls_pagamento_item	x,
		pls_evento		a
	where	a.nr_sequencia	 = x.nr_seq_evento
	and	a.ie_natureza	 = 'D'
	and	nr_seq_pagamento = nr_seq_pagto_prest_w;
	
	-- Glosa	
	select	abs(nvl(sum(a.vl_glosa),0))	
	into	vl_glosa_w
	from	pls_conta_medica_resumo a
	where	a.nr_seq_prestador_pgto = nr_seq_prestador_w
	and		a.nr_seq_lote_pgto		= nr_seq_lote_w;	
	
	vl_serv_prestado_w := nvl(vl_serv_prestado_w,0) + nvl(vl_glosa_w,0);
	
	vl_desconto_w	:= (nvl(vl_desconto_w,0) + nvl(vl_glosa_w, 0));
	
	-- Subtotal
	vl_sub_total_w	:= nvl(vl_serv_prestado_w, 0) + (nvl(vl_desconto_w, 0) * -1);
	
	-- Base IRRF
	select	sum(nvl(vl_item, 0)) - sum(nvl(vl_glosa, 0))
	into	vl_base_irrf_w
	from	pls_pagamento_item
	where	nr_seq_pagamento	= nr_seq_pagto_prest_w
	and	nr_seq_evento		= 1;
	
	-- IRRF
	select	sum(nvl(x.vl_imposto, 0))
	into	vl_irrf_retido_w
	from	pls_pag_prest_venc_trib		x,
		pls_pag_prest_vencimento	y,
		pls_pagamento_item		z,
		pls_pagamento_prestador		w,
		tributo				h
	where	w.nr_sequencia		= z.nr_seq_pagamento
	and	w.nr_sequencia		= y.nr_seq_pag_prestador
	and	y.nr_sequencia		= x.nr_seq_vencimento
	and	h.cd_tributo		= x.cd_tributo
	and	h.ie_tipo_tributo	= 'IR'
	and	w.nr_sequencia		= nr_seq_pagto_prest_w;
	
	-- ISS
	select	sum(nvl(x.vl_imposto, 0))
	into	vl_base_iss_w
	from	pls_pag_prest_venc_trib		x,
		pls_pag_prest_vencimento	y,
		pls_pagamento_item		z,
		pls_pagamento_prestador		w,
		tributo				h
	where	w.nr_sequencia		= z.nr_seq_pagamento
	and	w.nr_sequencia		= y.nr_seq_pag_prestador
	and	y.nr_sequencia		= x.nr_seq_vencimento
	and	h.cd_tributo		= x.cd_tributo
	and	h.ie_tipo_tributo	= 'ISS'
	and	w.nr_sequencia		= nr_seq_pagto_prest_w;	
	
	-- INSS(20%)
	vl_inss_20_w	:= vl_sub_total_w * 0.20;
	
	-- CSLL
	select	sum(nvl(x.vl_imposto, 0))
	into	vl_base_csll_w
	from	pls_pag_prest_venc_trib		x,
		pls_pag_prest_vencimento	y,
		pls_pagamento_item		z,
		pls_pagamento_prestador		w,
		tributo				h
	where	w.nr_sequencia		= z.nr_seq_pagamento
	and	w.nr_sequencia		= y.nr_seq_pag_prestador
	and	y.nr_sequencia		= x.nr_seq_vencimento
	and	h.cd_tributo		= x.cd_tributo
	and	h.ie_tipo_tributo	= 'CSLL'
	and	w.nr_sequencia		= nr_seq_pagto_prest_w;
	
	-- PIS
	select	sum(nvl(x.vl_imposto, 0))
	into	vl_base_pis_w
	from	pls_pag_prest_venc_trib		x,
		pls_pag_prest_vencimento	y,
		pls_pagamento_item 		z,
		pls_pagamento_prestador 	w,
		tributo				h
	where	w.nr_sequencia		= z.nr_seq_pagamento
	and	w.nr_sequencia		= y.nr_seq_pag_prestador
	and	y.nr_sequencia		= x.nr_seq_vencimento
	and	h.cd_tributo		= x.cd_tributo
	and	h.ie_tipo_tributo	= 'PIS'
	and	w.nr_sequencia		= nr_seq_pagto_prest_w;	
	
	-- Total
	select	sum(nvl(vl_liquido, 0))
	into	vl_total_w
	from	pls_pag_prest_vencimento	y,
		pls_pagamento_item		z,
		pls_pagamento_prestador		w
	where	w.nr_sequencia	= z.nr_seq_pagamento
	and	w.nr_sequencia	= y.nr_seq_pag_prestador
	and	w.nr_sequencia	= nr_seq_pagto_prest_w;
	
	-- Buscar as informações de valores PF
	-- Buscar as informações de valores Isento
	if	(ie_tipo_pessoa_prest_w = 'PF') or
		(ie_tipo_pessoa_prest_w = 'IS') then		
		-- Base INSS
		select	sum(nvl(x.vl_base_calculo, 0))
		into	vl_base_inss_w
		from	pls_pag_prest_venc_trib		x,
			pls_pag_prest_vencimento	y,
			pls_pagamento_item		z,
			pls_pagamento_prestador		w,
			tributo				h
		where	w.nr_sequencia		= z.nr_seq_pagamento
		and	w.nr_sequencia		= y.nr_seq_pag_prestador
		and	y.nr_sequencia		= x.nr_seq_vencimento
		and	h.cd_tributo		= x.cd_tributo
		and	x.ie_pago_prev		= 'V'
		and	h.ie_tipo_tributo	= 'INSS'
		and	w.nr_sequencia		= nr_seq_pagto_prest_w;
		
		-- INSS Ret.
		select	sum(nvl(x.vl_imposto,0))
		into	vl_inss_retido_w
		from	pls_pag_prest_venc_trib		x,
			pls_pag_prest_vencimento	y,
			pls_pagamento_prestador 	w,
			tributo				h
		where	w.nr_sequencia		= y.nr_seq_pag_prestador
		and	y.nr_sequencia		= x.nr_seq_vencimento
		and	h.cd_tributo		= x.cd_tributo
		and	x.ie_pago_prev		= 'V'
		and	h.ie_tipo_tributo	= 'INSS'
		and	w.nr_sequencia		= nr_seq_pagto_prest_w;
		
		-- Base IRRF			
		vl_base_irrf_w	:= nvl(vl_base_irrf_w, 0) - nvl(vl_inss_retido_w, 0);
	end if;
	
	-- Buscar as informações de valores PJ
	if	(ie_tipo_pessoa_prest_w = 'PJ') then
		-- Base COFINS	
		select	sum(nvl(vl_item, 0)) - sum(nvl(vl_glosa, 0))
		into	vl_base_cofins_w
		from	pls_pagamento_item
		where	nr_seq_pagamento = nr_seq_pagto_prest_w
		and	nr_seq_evento = 1;
		
		-- COFINS
		select	sum(nvl(x.vl_imposto, 0))
		into	vl_cofins_retido_w
		from	pls_pag_prest_venc_trib		x,
			pls_pag_prest_vencimento	y,
			pls_pagamento_item		z,
			pls_pagamento_prestador		w,
			tributo				h
		where	w.nr_sequencia		= z.nr_seq_pagamento
		and	w.nr_sequencia		= y.nr_seq_pag_prestador
		and	y.nr_sequencia		= x.nr_seq_vencimento
		and	h.cd_tributo		= x.cd_tributo
		and	h.ie_tipo_tributo	= 'COFINS'
		and	w.nr_sequencia		= nr_seq_pagto_prest_w;
	end if;
	
	if	(ie_tipo_pessoa_prest_w = 'PF') then
		vl_total_w	:= vl_sub_total_w - (nvl(vl_inss_retido_w,0) + nvl(vl_irrf_retido_w,0));
	else
		vl_total_w	:= vl_sub_total_w - (nvl(vl_cofins_retido_w,0) + nvl(vl_irrf_retido_w,0) + nvl(vl_base_pis_w,0) + nvl(vl_base_iss_w,0) + 
							nvl(vl_base_csll_w,0));
	end if;
	
	-- Gravar
	update	w_pls_relat_folha_pagto
	set	vl_serv_prestado = vl_serv_prestado + nvl(vl_serv_prestado_w,0),
		vl_complemento	 = vl_complemento + nvl(vl_complemento_w,0),
		vl_desconto	 = vl_desconto + nvl(vl_desconto_w,0),
		vl_sub_total	 = vl_sub_total + nvl(vl_sub_total_w,0),
		vl_base_inss	 = vl_base_inss + nvl(vl_base_inss_w,0),
		vl_inss_retido	 = vl_inss_retido + nvl(vl_inss_retido_w,0),
		vl_base_irrf	 = vl_base_irrf + nvl(vl_base_irrf_w,0),
		vl_irrf_retido	 = vl_irrf_retido + nvl(vl_irrf_retido_w,0),
		vl_base_iss	 = vl_base_iss + nvl(vl_base_iss_w,0),
		vl_inss_20	 = vl_inss_20 + nvl(vl_inss_20_w,0),
		vl_total	 = vl_total + nvl(vl_total_w,0),
		vl_base_cofins	 = vl_base_cofins + nvl(vl_base_cofins_w,0),
		vl_cofins_retido = vl_cofins_retido + nvl(vl_cofins_retido_w,0),
		vl_base_csll	 = vl_base_csll + nvl(vl_base_csll_w,0),
		vl_base_pis	 = vl_base_pis + nvl(vl_base_pis_w,0)
	where	nr_seq_prestador = nr_seq_prestador_w
	and	nm_usuario 	 = nm_usuario_p;	
	end;
end loop;
close C01;

commit;

end pls_gerar_relat_folha_pagto;
/