create or replace
procedure movto_cartao_parc_delete_js(
		nr_seq_movto_p		number) is

begin	

	delete	from movto_cartao_cr_parcela
	where	nr_seq_movto = nr_seq_movto_p;
	
commit;
end movto_cartao_parc_delete_js;
/