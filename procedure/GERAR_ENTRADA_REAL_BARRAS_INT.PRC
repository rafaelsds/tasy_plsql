create or replace
procedure gerar_entrada_real_barras_int(	nr_atendimento_p	number, 
											dt_entrada_real_p     date) is
		
nr_atendimento_w	number(10,0)	:= 0;
cd_unidade_w		varchar2(255);
cd_setor_atendimento_w	varchar2(255);
cd_unidade_basica_w	varchar2(255);
cd_unidade_compl_w	varchar2(255);
	
begin

if	(nr_atendimento_p is not null) then
	begin

	select	nr_atendimento
	into	nr_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;
	
	cd_unidade_w	:= substr(obter_unidade_atendimento(nr_atendimento_p, 'A', 'NR'),1,255);

	if	(nr_atendimento_w > 0) and
		(cd_unidade_w > 0) then
		begin
		
		cd_setor_atendimento_w	:= substr(obter_unidade_atendimento(nr_atendimento_p, 'A', 'CS'),1,255);
		cd_unidade_basica_w	:= substr(obter_unidade_atendimento(nr_atendimento_p, 'A', 'UB'),1,255);
		cd_unidade_compl_w	:= substr(obter_unidade_atendimento(nr_atendimento_p, 'A', 'UC'),1,255);
		
		update	atend_paciente_unidade
		set	dt_entrada_real		= decode(dt_entrada_real_p,null,sysdate,dt_entrada_real_p)
		where	nr_atendimento 		= nr_atendimento_w
		and	cd_setor_atendimento	= cd_setor_atendimento_w
                and	cd_unidade_basica	= cd_unidade_basica_w
                and	cd_unidade_compl	= cd_unidade_compl_w
                and	dt_saida_unidade is null
		and	dt_entrada_real is null;		
		
		commit;

		end;
	end if;
	
	end;
end if;

end gerar_entrada_real_barras_int;
/
