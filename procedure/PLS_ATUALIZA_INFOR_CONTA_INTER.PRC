create or replace
procedure pls_atualiza_infor_conta_inter
				(	nr_seq_inf_conta_interc_p	Number,
					nr_seq_resp_benef_p		Number,
					nr_seq_param1_p			Number,
					nr_seq_param2_p			Number,
					nr_seq_param3_p			Number,
					nm_usuario_p			Varchar2) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar o resultado da consulta de dados do beneficiário na tabela de informações de contas de intercâmbio.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ie_confirmacao_w	ptu_resp_consulta_benef.ie_confirmacao%type;	--Varchar
cd_mensagem_erro_w	ptu_resp_consulta_benef.cd_mensagem_erro%type;	--Number

begin

begin
	select	ie_confirmacao,
		cd_mensagem_erro
	into	ie_confirmacao_w,
		cd_mensagem_erro_w
	from	ptu_resp_consulta_benef
	where	nr_sequencia	= nr_seq_resp_benef_p;
exception
when others then
	ie_confirmacao_w	:= 'N';
end;

if	(ie_confirmacao_w	= 'S') and (cd_mensagem_erro_w	is null) then
	update	pls_analise_inf_conta_int
	set	ie_situacao_origem	= 1,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_inf_conta_interc_p;
else
	update	pls_analise_inf_conta_int
	set	ie_situacao_origem	= 2,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_inf_conta_interc_p;
end if;

commit;

end pls_atualiza_infor_conta_inter;
/