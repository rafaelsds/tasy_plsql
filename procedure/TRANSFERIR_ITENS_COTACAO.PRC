create or replace
procedure transferir_itens_cotacao(
			nr_cot_compra_p		number,
			nr_item_cot_compra_p	number,
			nm_usuario_p		varchar2,
			nr_cot_compra_nova_p in out number) as

qt_existe_w		number(5);
nr_item_cot_compra_w	number(10);
nr_cot_compra_w		number(10);
nr_seq_cot_forn_w		number(10);
nr_seq_cot_forn_ww	number(10);
cd_cgc_fornecedor_w	varchar2(14);
cd_condicao_pagamento_w	number(10);
cd_moeda_w		number(5);
ie_frete_w		varchar2(1);
vl_previsto_frete_w		number(15,2);
nr_documento_fornec_w	varchar2(10);
dt_documento_w		date;
ie_tipo_documento_w	varchar2(1);
qt_dias_validade_w		number(5);
qt_dias_entrega_w		number(5);
pr_desconto_w		number(15,4);
pr_desconto_pgto_antec_w	number(15,4);
pr_juros_negociado_w	number(13,4);
ds_observacao_w		varchar2(2000);
cd_pessoa_fisica_w	varchar2(10);
ie_status_w		varchar2(5);
vl_despesa_acessoria_w	number(13,2);
vl_desconto_w		number(13,2);
ie_exclusivo_w		varchar2(1);
nr_solic_compra_w		number(10);
nr_item_solic_compra_w	number(5);


cursor c01 is
select	a.nr_sequencia,
	a.cd_cgc_fornecedor,
	a.cd_condicao_pagamento,
	a.cd_moeda,
	a.ie_frete,
	a.vl_previsto_frete,
	a.nr_documento_fornec,
	a.dt_documento,
	a.ie_tipo_documento,
	a.qt_dias_validade,
	a.qt_dias_entrega,
	a.pr_desconto,
	a.pr_desconto_pgto_antec,
	a.pr_juros_negociado,
	a.ds_observacao,
	a.cd_pessoa_fisica,
	a.ie_status,
	a.vl_despesa_acessoria,
	a.vl_desconto,
	a.ie_exclusivo
from	cot_compra_forn a
where	a.nr_cot_compra = nr_cot_compra_p
and exists(
	select	1
	from	cot_compra_forn_item b
	where	b.nr_seq_cot_forn = a.nr_sequencia
	and	b.nr_item_cot_compra = nr_item_cot_compra_p)
order by
	nvl(a.cd_cgc_fornecedor, a.cd_pessoa_fisica);

begin

if	(nvl(nr_cot_compra_nova_p, 0) > 0) then
	select	count(*)
	into	qt_existe_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_nova_p;
	if	(qt_existe_w = 0) then		
		wheb_mensagem_pck.exibir_mensagem_abort(248287,'NR_COT_COMPRA_NOVA='||nr_cot_compra_nova_p);
	end if;
end if;

nr_cot_compra_w		:= nr_cot_compra_nova_p;

if	(nvl(nr_cot_compra_nova_p, 0) = 0) then
	select	cot_compra_seq.nextval
	into	nr_cot_compra_w
	from	dual;
	nr_cot_compra_nova_p	:= nr_cot_compra_w;

	insert into cot_compra(
		nr_cot_compra,
		dt_cot_compra,
		dt_atualizacao,
		cd_comprador,
		nm_usuario,
		ds_observacao,
		cd_pessoa_solicitante,
		cd_estabelecimento,
		ie_finalidade_cotacao)
	select	nr_cot_compra_w,
		sysdate,
		sysdate,
		cd_comprador,
		nm_usuario_p,
		ds_observacao,
		cd_pessoa_solicitante,
		cd_estabelecimento,
		nvl(ie_finalidade_cotacao,'C')
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_p;
		
end if;

select	nvl(max(nr_item_cot_compra), 0) + 1
into	nr_item_cot_compra_w
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_nova_p;

insert into cot_compra_item(
	nr_cot_compra,
	nr_item_cot_compra,	
	cd_material,
	qt_material,
	cd_unidade_medida_compra,
	dt_atualizacao,
	dt_limite_entrega,
	nm_usuario,
	ie_situacao,
	ds_material_direto_w,
	ie_regra_preco,
	nr_solic_compra,
	nr_item_solic_compra,
	cd_estab_item)
select	nr_cot_compra_w,
	nr_item_cot_compra_w,
	cd_material,
	qt_material,
	cd_unidade_medida_compra,
	sysdate,
	dt_limite_entrega,
	nm_usuario_p,
	ie_situacao,
	ds_material_direto_w,
	ie_regra_preco,
	nr_solic_compra,
	nr_item_solic_compra,
	cd_estab_item
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	nr_item_cot_compra = nr_item_cot_compra_p;

update	cot_compra_anexo
set		nr_cot_compra = nr_cot_compra_w,
		nr_item_cot_compra = nr_item_cot_compra_w
where	nr_cot_compra = nr_cot_compra_p
and		nr_item_cot_compra = nr_item_cot_compra_p;

gerar_cot_compra_item_entrega(nr_cot_compra_w, nr_item_cot_compra_w, nm_usuario_p);

open C01;
loop
fetch C01 into
	nr_seq_cot_forn_ww,
	cd_cgc_fornecedor_w,
	cd_condicao_pagamento_w,
	cd_moeda_w,
	ie_frete_w,
	vl_previsto_frete_w,
	nr_documento_fornec_w,
	dt_documento_w,
	ie_tipo_documento_w,
	qt_dias_validade_w,
	qt_dias_entrega_w,
	pr_desconto_w,
	pr_desconto_pgto_antec_w,
	pr_juros_negociado_w,
	ds_observacao_w,
	cd_pessoa_fisica_w,
	ie_status_w,
	vl_despesa_acessoria_w,
	vl_desconto_w,
	ie_exclusivo_w;
exit when c01%notfound;
	begin

	if	(cd_cgc_fornecedor_w is not null) then
		select	nvl(max(nr_sequencia), 0)
		into	nr_seq_cot_forn_w
		from	cot_compra_forn
		where	nr_cot_compra = nr_cot_compra_w
		and	cd_cgc_fornecedor = cd_cgc_fornecedor_w;
	elsif	(cd_pessoa_fisica_w is not null) then
		select	nvl(max(nr_sequencia), 0)
		into	nr_seq_cot_forn_w
		from	cot_compra_forn
		where	nr_cot_compra = nr_cot_compra_w
		and	cd_pessoa_fisica = cd_pessoa_fisica_w;
	end if;

	if	(nr_seq_cot_forn_w = 0) then
		begin

		select	cot_compra_forn_seq.nextval
		into	nr_seq_cot_forn_w
		from	dual;

		insert into cot_compra_forn(
			nr_sequencia,
			nr_cot_compra,
			cd_cgc_fornecedor,
			cd_condicao_pagamento,
			cd_moeda,
			ie_frete,
			dt_atualizacao,
			nm_usuario,
			vl_previsto_frete,
			nr_documento_fornec,
			dt_documento,
			ie_tipo_documento,
			qt_dias_validade,
			qt_dias_entrega,
			pr_desconto,
			pr_desconto_pgto_antec,
			pr_juros_negociado,
			ds_observacao,
			cd_pessoa_fisica,
			ie_status,
			vl_despesa_acessoria,
			vl_desconto,
			ie_exclusivo)
		values( nr_seq_cot_forn_w,
			nr_cot_compra_w,
			cd_cgc_fornecedor_w,
			cd_condicao_pagamento_w,
			cd_moeda_w,
			ie_frete_w,
			sysdate,
			nm_usuario_p,
			vl_previsto_frete_w,
			nr_documento_fornec_w,
			dt_documento_w,
			ie_tipo_documento_w,
			qt_dias_validade_w,
			qt_dias_entrega_w,
			pr_desconto_w,
			pr_desconto_pgto_antec_w,
			pr_juros_negociado_w,
			ds_observacao_w,
			cd_pessoa_fisica_w,
			ie_status_w,
			vl_despesa_acessoria_w,
			vl_desconto_w,
			ie_exclusivo_w);

		end;
	end if;

	insert into cot_compra_forn_item(
		nr_sequencia,
		nr_cot_compra,
		nr_item_cot_compra,
		cd_cgc_fornecedor,
		qt_material,
		vl_unitario_material,
		dt_atualizacao,
		nm_usuario,
		pr_desconto,
		vl_frete_rateio,
		vl_preco_liquido,
		vl_total_liquido_item,
		ds_observacao,
		vl_presente,
		ds_marca,
		ds_marca_fornec,
		qt_prioridade,
		vl_unid_fornec,
		qt_conv_unid_fornec,
		ds_material_direto,
		ie_situacao,
		dt_validade,
		nr_seq_cot_forn,
		cd_motivo_falta_preco,
		vl_preco,
		cd_material,
		vl_unitario_liquido,
		vl_desconto,
		vl_unitario_inicial)
	select	cot_compra_forn_item_seq.nextval,
		nr_cot_compra_w,
		nr_item_cot_compra_w,
		cd_cgc_fornecedor,
		qt_material,
		vl_unitario_material,
		sysdate,
		nm_usuario_p,
		pr_desconto,
		vl_frete_rateio,
		vl_preco_liquido,
		vl_total_liquido_item,
		ds_observacao,
		vl_presente,
		ds_marca,
		ds_marca_fornec,
		qt_prioridade,
		vl_unid_fornec,
		qt_conv_unid_fornec,
		ds_material_direto,
		ie_situacao,
		dt_validade,
		nr_seq_cot_forn_w,
		cd_motivo_falta_preco,
		vl_preco,
		cd_material,
		vl_unitario_liquido,
		vl_desconto,
		vl_unitario_inicial
	from	cot_compra_forn_item
	where	nr_seq_cot_forn = nr_seq_cot_forn_ww
	and	nr_item_cot_compra = nr_item_cot_compra_p;

	end;
end loop;
close c01;

select	nvl(max(nr_solic_compra),0),
	nvl(max(nr_item_solic_compra),0)
into	nr_solic_compra_w,
	nr_item_solic_compra_w
from	solic_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	nr_item_cot_compra = nr_item_cot_compra_p;

delete
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	nr_item_cot_compra = nr_item_cot_compra_p;

if	(nr_solic_compra_w > 0) then
	update	solic_compra_item
	set	nr_cot_compra = nr_cot_compra_w,
		nr_item_cot_compra = nr_item_cot_compra_w
	where	nr_solic_compra = nr_solic_compra_w
	and	nr_item_solic_compra = nr_item_solic_compra_w;	
end if;

/*Tratamento para excluir a cotacao se foi transferido integralmente*/
select	count(*)
into	qt_existe_w
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p;
if	(qt_existe_w = 0) then
	delete
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_p;
	
	gerar_historico_cotacao(
		nr_cot_compra_nova_p,
		WHEB_MENSAGEM_PCK.get_texto(298025),
		WHEB_MENSAGEM_PCK.get_texto(298026,'NR_COT_COMPRA_P='||nr_cot_compra_p),
		'S',
		nm_usuario_p);
end if;

commit;

end transferir_itens_cotacao;
/
