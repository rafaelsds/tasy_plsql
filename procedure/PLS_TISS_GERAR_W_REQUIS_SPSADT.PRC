create or replace
procedure pls_tiss_gerar_w_requis_spsadt
				(	nr_seq_requisicao_p	number,
					ds_dir_padrao_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is

cd_guia_w			varchar2(20);
cd_guia_principal_w		Varchar2(20);
cd_ans_w			varchar2(20);
cd_senha_w			varchar2(20);
dt_autorizacao_w		date;
dt_validade_senha_w		date;
dt_emissao_w			date;
nr_seq_plano_w			number(10);
nr_seq_segurado_w		number(10);
cd_medico_solicitante_w		varchar2(10);
nr_seq_prestador_w		number(10);
cd_usuario_plano_w		varchar2(30);
dt_validade_carteira_w		date;
cd_pessoa_fisica_w		varchar2(10);
ds_plano_w			varchar2(255);
ie_carater_atendimento_w	varchar2(1);
nr_seq_tiss_guia_w		number(10);
nr_cartao_nac_sus_w		varchar(60);
nm_pessoa_fisica_w		varchar2(255);
sg_conselho_w			varchar2(20);
cd_doenca_cid_w			varchar2(10);
ds_diagnostico_w		varchar2(2000);
cd_tabela_relat_w		varchar2(2);
cd_porte_anestesico_w		Varchar2(10);
ds_retorno_w			Varchar2(255);
ds_indicacao_w			Varchar2(255);

cgc_exec_w			Varchar2(14);
cpf_exec_w			Varchar2(50);
nm_contratado_exec_w		Varchar2(255);
ds_logradouro_w			Varchar2(255);
nm_municipio_w			Varchar2(255);
sg_estado_w			pessoa_juridica.sg_estado%type;
cd_municipio_ibge_w		Varchar2(15);
cd_cep_w			Varchar2(50);
cd_cnes_w			Varchar2(20);

qt_proc_req_w 			number(10);
nr_seq_apresentacao_w		number(10);
nr_seq_req_proc_w		number(10);
cd_procedimento_w		number(15);
nr_seq_guia_principal_w		number(10);
ds_procedimento_w		varchar2(255);
ie_origem_proced_w		number(10);
qt_solicitada_w			pls_requisicao_proc.qt_solicitado%type;
qt_autorizada_w			pls_requisicao_proc.qt_procedimento%type;
nr_cpf_w			varchar2(11);
nr_crm_w			varchar2(20);
uf_crm_w			medico.uf_crm%type;
cd_cbo_w			varchar2(10);
nm_medico_solicitante_w		varchar(255);
ds_observacao_w			varchar2(4000);

vl_procedimento_w		number(15,2);
vl_custo_oper_w			number(15,2);
vl_anestesista_w		number(15,2);
vl_medico_w			number(15,2);
vl_filme_w			number(15,2);
vl_auxiliares_w			number(15,2);
nr_seq_regra_w			number(10);
cd_edicao_amb_w			number(6);
ie_valor_informado_w		Varchar2(1)	:= 'N';
nr_aux_regra_w			Number(10);
nr_seq_regra_autogerado_w	Number(10);
ie_tipo_compl_prest_w		Varchar2(4);
ds_endereco_w			varchar2(255);
ds_complemento_w		varchar2(255);
ds_bairro_w			varchar2(255);
ds_email_w			varchar2(255);
ds_website_w			varchar2(255);
ds_fax_w			varchar2(80);
nr_telefone_w			varchar2(30);
nr_tel_simples_w		varchar2(30);
ds_fone_adic_w			varchar2(255);
nr_ddi_telefone_w		varchar2(6);
nr_ddd_telefone_w		varchar2(6);
nr_endereco_w			varchar2(5);
nr_ramal_w			number(5);
nr_ddd_fax_w			varchar2(3);
ds_municipio_w			varchar2(40);
cd_prestador_exec_w		varchar2(30);
nr_seq_prestador_exec_w		Number(10);
nr_seq_compl_pf_tel_adic_w	pls_prestador.nr_seq_compl_pf_tel_adic%type;
nr_seq_tipo_compl_adic_w	pls_prestador.nr_seq_tipo_compl_adic%type;

cursor c01 is
	select	nr_sequencia,
		cd_procedimento,
		substr(obter_descricao_procedimento(cd_procedimento,ie_origem_proced),1,255),
		ie_origem_proced,
		qt_solicitado,
		qt_procedimento
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	ie_status		in ('S','P')
	and	nr_seq_motivo_exc	is null;

begin

delete	from w_tiss_guia
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_dados_atendimento
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_beneficiario
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_proc_paciente
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_contratado_exec
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_totais
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_proc_solic
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_solicitacao
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_contratado_solic
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_relatorio
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_opm
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_opm_exec
where	nm_usuario		= nm_usuario_p;

commit;

if	(nvl(nr_seq_requisicao_p,0) > 0) then
	begin
		select	nvl(a.cd_senha,a.cd_senha_externa),
			a.dt_requisicao,
			a.dt_validade_senha,
			a.nr_seq_plano,
			a.nr_seq_segurado,
			a.cd_medico_solicitante,
			a.nr_seq_prestador,
			a.nr_seq_prestador_exec,
			substr(pls_obter_dados_cart_segurado(a.nr_seq_segurado,'C'),1,30),
			to_date(pls_obter_dados_cart_segurado(a.nr_seq_segurado,'V'),'dd/mm/yyyy'),
			b.cd_pessoa_fisica,
			c.ds_plano,
			a.ie_carater_atendimento,
			a.cd_guia_principal,
			substr(a.ds_observacao,1,255),
			substr(a.ds_indicacao_clinica,1,255)
		into	cd_senha_w,
			dt_autorizacao_w,
			dt_validade_senha_w,
			nr_seq_plano_w,
			nr_seq_segurado_w,
			cd_medico_solicitante_w,
			nr_seq_prestador_w,
			nr_seq_prestador_exec_w,
			cd_usuario_plano_w,
			dt_validade_carteira_w,
			cd_pessoa_fisica_w,
			ds_plano_w,
			ie_carater_atendimento_w,
			cd_guia_principal_w,
			ds_observacao_w,
			ds_indicacao_w
		from	pls_plano c,
			pls_segurado b,
			pls_requisicao a
		where	a.nr_sequencia		= nr_seq_requisicao_p
		and	a.nr_seq_segurado	= b.nr_sequencia
		and	b.nr_seq_plano		= c.nr_sequencia;
	exception
	when others then
		cd_senha_w			:= '';
		dt_autorizacao_w		:= null;
		dt_validade_senha_w		:= null;
		nr_seq_plano_w			:= null;
		nr_seq_segurado_w		:= null;
		cd_medico_solicitante_w		:= null;
		nr_seq_prestador_w		:= null;
		nr_seq_prestador_exec_w		:= null;
		cd_usuario_plano_w		:= '';
		dt_validade_carteira_w		:= null;
		cd_pessoa_fisica_w		:= null;
		ds_plano_w			:= '';
		ie_carater_atendimento_w	:= '';
		cd_guia_principal_w		:= '';
		ds_observacao_w			:= '';
		ds_indicacao_w			:= '';
	end;
	
	begin
		select	max(cd_ans)
		into	cd_ans_w
		from	pls_outorgante
		where	cd_ans	is not null;
	exception
	when others then
		cd_ans_w := pls_obter_dados_outorgante(cd_estabelecimento_p, 'ANS');
	end;
	
	insert	into w_tiss_relatorio
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_arquivo_logo)
	values	(w_tiss_relatorio_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_dir_padrao_p || '\pls_logo.jpg');

	qt_proc_req_w 		:= 0;
	nr_seq_apresentacao_w	:= 0;

	open c01;
	loop
	fetch c01 into	nr_seq_req_proc_w,
			cd_procedimento_w,
			ds_procedimento_w,
			ie_origem_proced_w,
			qt_solicitada_w,
			qt_autorizada_w;
	exit when c01%notfound;
		begin
		qt_proc_req_w		:= qt_proc_req_w + 1;

		if	(qt_proc_req_w = 1) then
			select	w_tiss_guia_seq.nextval
			into	nr_seq_tiss_guia_w
			from	dual;

			insert	into w_tiss_guia
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				cd_ans,
				cd_autorizacao,
				dt_autorizacao,
				cd_senha,
				dt_validade_senha,
				dt_emissao_guia,
				nr_sequencia_autor,
				cd_autorizacao_princ,
				ds_observacao,
				ie_tiss_tipo_guia,
				dt_entrada)
			values	(nr_seq_tiss_guia_w,
				sysdate,
				nm_usuario_p,
				cd_ans_w,
				cd_guia_w,
				dt_autorizacao_w,
				cd_senha_w,
				dt_validade_senha_w,
				null,
				nr_seq_requisicao_p,
				cd_guia_principal_w,
				ds_observacao_w,
				'2',
				null);

			begin
				select	nr_cartao_nac_sus,
					substr(obter_nome_pf(cd_pessoa_fisica),1,255)
				into	nr_cartao_nac_sus_w,
					nm_pessoa_fisica_w
				from	pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
			exception
			when others then
				nr_cartao_nac_sus_w	:= '';
				nm_pessoa_fisica_w	:= '';
			end;

			insert	into w_tiss_beneficiario
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_guia,
				cd_pessoa_fisica,
				nm_pessoa_fisica,
				nr_cartao_nac_sus,
				ds_plano,
				dt_validade_carteira,
				cd_usuario_convenio)
			values	(w_tiss_beneficiario_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tiss_guia_w,
				cd_pessoa_fisica_w,
				nm_pessoa_fisica_w,
				nr_cartao_nac_sus_w,
				ds_plano_w,
				dt_validade_carteira_w,
				cd_usuario_plano_w);

			begin
				select	b.nr_cpf,
					substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
					substr(obter_conselho_profissional(b.nr_seq_conselho,'S'),1,10),
					nr_crm,
					uf_crm,
					substr(obter_descricao_padrao('CBO_SAUDE','CD_CBO',b.nr_seq_cbo_saude),1,10)
				into	nr_cpf_w,
					nm_medico_solicitante_w,
					sg_conselho_w,
					nr_crm_w,
					uf_crm_w,
					cd_cbo_w
				from	pessoa_fisica b,
					medico a
				where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
				and	a.cd_pessoa_fisica	= cd_medico_solicitante_w;
			exception
			when others then
				nr_cpf_w := null;
				nm_medico_solicitante_w := null;
				sg_conselho_w := null;
				nr_crm_w := null;
				uf_crm_w := null;
				cd_cbo_w := null;
			end;
			
			insert into w_tiss_contratado_solic
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_guia,
				cd_cgc,
				cd_interno,
				nr_cpf,
				nm_contratado,
				nm_solicitante,
				cd_cnes,
				sg_conselho,
				nr_crm,
				uf_crm,
				cd_cbo_saude)
			values	(w_tiss_contratado_solic_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tiss_guia_w,
				'',
				pls_obter_dados_prestador(nr_seq_prestador_w,'CD'),
				obter_compl_pf(cd_medico_solicitante_w,1,'CPF'),
				pls_obter_dados_prestador(nr_seq_prestador_w,'NF'),
				nm_medico_solicitante_w,
				pls_obter_dados_prestador(nr_seq_prestador_w,'CNES'),
				sg_conselho_w,
				nr_crm_w,
				uf_crm_w,
				cd_cbo_w);

			select	max(cd_doenca),
				max(ds_diagnostico)
			into	cd_doenca_cid_w,
				ds_diagnostico_w
			from	pls_requisicao_diagnostico
			where	nr_seq_requisicao	= nr_seq_requisicao_p
			and	ie_classificacao	= 'P';
			
			insert into w_tiss_solicitacao
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_guia,
				dt_solicitacao,
				ie_carater_solic,
				cd_cid,
				ds_indicacao)
			values	(w_tiss_solicitacao_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tiss_guia_w,
				dt_autorizacao_w,
				ie_carater_atendimento_w,
				cd_doenca_cid_w,
				ds_indicacao_w);
		end if;
		
		nr_seq_apresentacao_w	:= nr_seq_apresentacao_w + 1;
		cd_tabela_relat_w := pls_obter_tabela_proc_guia(nr_seq_requisicao_p, cd_estabelecimento_p, nr_seq_prestador_w, cd_procedimento_w, ie_origem_proced_w, dt_autorizacao_w);

		insert into w_tiss_proc_solic
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_guia,
			cd_procedimento,
			cd_edicao_amb,
			ds_procedimento,
			qt_solicitada,
			qt_autorizada,
			nr_seq_apresentacao)
		values	(w_tiss_proc_solic_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_seq_tiss_guia_w,
			cd_procedimento_w,
			cd_tabela_relat_w,
			ds_procedimento_w,
			qt_solicitada_w,
			qt_autorizada_w,
			nr_seq_apresentacao_w);

		if	(qt_proc_req_w = 5) then
			if	(nr_seq_prestador_w	is not null) then
				begin
					select	pls_obter_dados_prestador(nr_sequencia,'CGC'),
						pls_obter_dados_prestador(nr_sequencia,'PF'),
						ie_tipo_endereco,
						cd_prestador,
						nr_seq_compl_pf_tel_adic,
						nr_seq_tipo_compl_adic
					into	cgc_exec_w,
						cpf_exec_w,
						ie_tipo_compl_prest_w,
						cd_prestador_exec_w,
						nr_seq_compl_pf_tel_adic_w,
						nr_seq_tipo_compl_adic_w
					from	pls_prestador
					where	nr_sequencia	= nr_seq_prestador_exec_w;
				exception
				when others then
					cgc_exec_w		:= '';
					cpf_exec_w		:= '';
					ie_tipo_compl_prest_w	:= '';
					cd_prestador_exec_w	:= '';
				end;

				nm_contratado_exec_w	:= pls_obter_dados_prestador(nr_seq_prestador_exec_w,'NF');
			end if;
			if	((cpf_exec_w	is not null) or (cgc_exec_w	is not null)) then
				pls_obter_dados_end_prestador(	cpf_exec_w, cgc_exec_w, ie_tipo_compl_prest_w, 'N', nr_seq_compl_pf_tel_adic_w, nr_seq_tipo_compl_adic_w,
								ds_endereco_w, nr_endereco_w, ds_complemento_w, ds_bairro_w, cd_cep_w, nr_telefone_w, ds_email_w,
								ds_website_w, nr_ddi_telefone_w, nr_ddd_telefone_w, nr_ramal_w, cd_municipio_ibge_w,
								sg_estado_w, ds_fax_w, nr_ddd_fax_w, ds_municipio_w, ds_fone_adic_w);

				ds_logradouro_w	:= substr(pls_obter_end_prestador(nr_seq_prestador_w,null, null),1,255);
				if	(cd_municipio_ibge_w	is not null) then
					nm_municipio_w	:= substr(obter_desc_municipio_ibge(cd_municipio_ibge_w),1,255);
				end if;
				cd_cnes_w		:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'CNES');
			end if;
					
			insert	into w_tiss_contratado_exec
				(nr_sequencia, 	dt_atualizacao, nm_usuario,
				 nr_seq_guia, cd_cgc, cd_interno,
				 nr_cpf, nm_contratado, ds_tipo_logradouro,
				 ds_logradouro, nm_municipio, sg_estado,
				 cd_municipio_ibge, cd_cep, cd_cnes)
			values	(w_tiss_contratado_exec_seq.nextval,sysdate,nm_usuario_p,
				 nr_seq_tiss_guia_w, cgc_exec_w, cd_prestador_exec_w,
				 cpf_exec_w, nm_contratado_exec_w,'',
				 substr(ds_logradouro_w,1,53), nm_municipio_w, sg_estado_w,
				 cd_municipio_ibge_w, cd_cep_w, cd_cnes_w);

			insert into w_tiss_proc_paciente
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_guia)
			values	(w_tiss_proc_paciente_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tiss_guia_w);

			insert	into w_tiss_totais
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_guia)
			values	(w_tiss_totais_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tiss_guia_w);

			qt_proc_req_w	:= 0;
		end if;
		end;
	end loop;
	close c01;

	if	(qt_proc_req_w > 0) and
		(qt_proc_req_w < 5) then
		if	(nr_seq_prestador_w	is not null) then
			begin
				select	pls_obter_dados_prestador(nr_sequencia,'CGC'),
					pls_obter_dados_prestador(nr_sequencia,'PF'),
					ie_tipo_endereco,
					cd_prestador,
					nr_seq_compl_pf_tel_adic,
					nr_seq_tipo_compl_adic
				into	cgc_exec_w,
					cpf_exec_w,
					ie_tipo_compl_prest_w,
					cd_prestador_exec_w,
					nr_seq_compl_pf_tel_adic_w,
					nr_seq_tipo_compl_adic_w
				from	pls_prestador
				where	nr_sequencia	= nr_seq_prestador_exec_w;
			exception
			when others then
				cgc_exec_w		:= '';
				cpf_exec_w		:= '';
				ie_tipo_compl_prest_w	:= '';
				cd_prestador_exec_w	:= '';
			end;

			nm_contratado_exec_w	:= pls_obter_dados_prestador(nr_seq_prestador_exec_w,'NF');
		end if;

		if	((cpf_exec_w	is not null) or (cgc_exec_w	is not null)) then
			pls_obter_dados_end_prestador(	cpf_exec_w, cgc_exec_w, ie_tipo_compl_prest_w, 'N', nr_seq_compl_pf_tel_adic_w, nr_seq_tipo_compl_adic_w,
							ds_endereco_w, nr_endereco_w, ds_complemento_w, ds_bairro_w, cd_cep_w, nr_telefone_w, ds_email_w,
							ds_website_w, nr_ddi_telefone_w, nr_ddd_telefone_w, nr_ramal_w, cd_municipio_ibge_w,
							sg_estado_w, ds_fax_w, nr_ddd_fax_w, ds_municipio_w, ds_fone_adic_w);

			ds_logradouro_w	:=	substr(pls_obter_end_prestador(nr_seq_prestador_exec_w,null, null),1,255);
			
			if	(cd_municipio_ibge_w	is not null) then
				nm_municipio_w	:= substr(obter_desc_municipio_ibge(cd_municipio_ibge_w),1,255);
			end if;
			
			cd_cnes_w		:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'CNES');
		end if;

		insert	into w_tiss_contratado_exec
			(nr_sequencia, 	dt_atualizacao, nm_usuario,
			 nr_seq_guia, cd_cgc, cd_interno,
			 nr_cpf, nm_contratado, ds_tipo_logradouro,
			 ds_logradouro, nm_municipio, sg_estado,
			 cd_municipio_ibge, cd_cep, cd_cnes)
		values	(w_tiss_contratado_exec_seq.nextval,sysdate,nm_usuario_p,
			 nr_seq_tiss_guia_w, cgc_exec_w, cd_prestador_exec_w,
			 cpf_exec_w, nm_contratado_exec_w,'',
			 substr(ds_logradouro_w,1,53), nm_municipio_w, sg_estado_w,
			 cd_municipio_ibge_w, cd_cep_w, cd_cnes_w);

		insert into w_tiss_proc_paciente
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_guia)
		values	(w_tiss_proc_paciente_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_seq_tiss_guia_w);

		insert	into w_tiss_totais
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_guia)
		values	(w_tiss_totais_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_seq_tiss_guia_w);
	end if;

	pls_tiss_completar_req_guia(nr_seq_tiss_guia_w, nm_usuario_p);
end if;

commit;

end pls_tiss_gerar_w_requis_spsadt;
/
