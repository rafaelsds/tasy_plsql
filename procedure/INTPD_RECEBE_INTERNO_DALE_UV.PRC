create or replace procedure intpd_recebe_interno_dale_uv(
    nm_usuario_p		varchar2,
    cd_estabelecimento_p number,
    xml_p		in	xmltype) is
/*'
  Receber XML DALE_UV
'*/

nr_sequencia_w	 	number(10,0);
nr_seq_lote_w    	number(10,0);
nr_seq_retorno_w 	number(10,0);
nr_seq_tipo_mensagem_w	number(10,0);

cursor C01 IS 
select	*
from	xmltable('/quitt/arzt' passing xml_p columns 
	cd_cnes_arquivo		varchar2(30)	path 'arzt_ik',
	ds_email		varchar2(255)	path 'arzt_mail',
	dt_fim_assinatura 	date       	path 'arzt_sigzeit');	
c01_w	c01%ROWTYPE; 
   
cursor C02 IS 
select	*
from	xmltable('/quitt/unidav' passing xml_p columns 
	cd_uni_dav 		varchar2(50)	path  'unidav_ik',
	ds_uni_dav 		varchar2(255)	path 'unidav_name',
	ds_endereco_uni_dav 	varchar2(255)	path  'unidav_ort');
c02_w    c02%ROWTYPE;
 
cursor C03 IS
select *
from xmltable('/quitt/datei' passing xml_p columns 
	ds_arquivo 		varchar2(255)	path 'dat_nam',
	tipo_mensagem 		varchar2(255)	path 'dat_typ',
	ds_id_objeto 		varchar2(20)	path 'dat_obj_id',
	dt_recebimento_date 	date  		path 'dat_dat',
	dt_recebimento_time	varchar(8)    	path 'dat_time',
	cd_cnes_destino 	varchar2(50)	path 'dat_empf',
	nm_destino 		varchar2(255)	path 'dat_empf_name',
	ie_status 		varchar2(10)	path 'dat_status');
c03_w  c03%ROWTYPE;
      
CURSOR C04 IS
select 	* 
from 	xmltable('/quitt/datei/dat_fehler' passing xml_p columns 
	ds_erro 		varchar2(4000) 	path 'fehler_text',
	ds_tag_erro 		varchar2(255) 	path 'fehler_feld_tech',
	ds_campo_incorreto 	varchar2(4000) 	path 'fehler_feld_fach',
	cd_erro 		varchar2(30) 	path 'fehler_code');
c04_w  c04%ROWTYPE;
    
begin
  
/*necess�rio para um to_char n�o retirar a hora da data nas rotinas que recebem valores desse tipo de dado*/
execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	if (c01_w.cd_cnes_arquivo is not null) then 

		select	DUV_LOTE_RETORNO_SEQ.nextval
		into	nr_sequencia_w
		from	dual;

		nr_seq_lote_w := nr_sequencia_w;

		open C02;
		loop
		fetch C02 into	
			c02_w;
		exit when C02%notfound;  

			insert into DUV_LOTE_RETORNO(
				NR_SEQUENCIA,
				CD_ESTABELECIMENTO,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,
				CD_CNES_ARQUIVO, 
				DS_EMAIL, 
				DT_FIM_ASSINATURA, 
				CD_UNI_DAV, 
				DS_UNI_DAV, 
				DS_ENDERECO_UNI_DAV,
				IE_STATUS)
			values(	nr_sequencia_w,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				null,
				null,
				c01_w.cd_cnes_arquivo, 
				c01_w.ds_email, 
				c01_w.dt_fim_assinatura, 
				c02_w.cd_uni_dav, 
				c02_w.ds_uni_dav, 
				c02_w.ds_endereco_uni_dav,
				'1'); --Importado

		end loop;
		close C02;

	end if;

end loop;
close C01;

Open C03;
loop
fetch C03 into 
	c03_w;
exit when C03%notfound;

	if (c03_w.ds_arquivo is not null) then

		select	DUV_RETORNO_SEQ.nextval
		into	nr_sequencia_w
		from	dual;

		nr_seq_retorno_w := nr_sequencia_w;

		select	max(nr_sequencia)
		into	nr_seq_tipo_mensagem_w
		from	duv_tipo_mensagem
		where	CD_MENSAGEM = lower(c03_w.tipo_mensagem);
		
		insert into DUV_RETORNO(
			NR_SEQUENCIA,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			NR_SEQ_LOTE,                  
			DS_ARQUIVO,
			NR_SEQ_TIPO_MENSAGEM,
			DS_ID_OBJETO,
			DT_RECEBIMENTO,
			CD_CNES_DESTINO,
			NM_DESTINO,
			IE_STATUS)
		values(	nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			NULL,
			NULL,
			nr_seq_lote_w,
			c03_w.ds_arquivo,
			nr_seq_tipo_mensagem_w,
			c03_w.ds_id_objeto,
			pkg_date_utils.get_DateTime(to_char(c03_w.dt_recebimento_date,'yyyy-mm-dd') || 'T' || c03_w.dt_recebimento_time),
			c03_w.cd_cnes_destino,
			c03_w.nm_destino,
			c03_w.ie_status );    

		Open C04;
		loop
		fetch C04 into
			c04_w;
		exit when C04%notfound;  

			if (c04_w.ds_erro is not null) then

			select	DUV_RETORNO_ERRO_SEQ.nextval
			into	nr_sequencia_w
			from	dual;

			Insert into DUV_RETORNO_ERRO(
				NR_SEQUENCIA,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,
				NR_SEQ_RETORNO,
				DS_ERRO, 
				DS_TAG_ERRO, 
				DS_CAMPO_INCORRETO, 
				CD_ERRO)
			values( nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				null,
				null,
				nr_seq_retorno_w,
				c04_w.ds_erro,
				c04_w.ds_tag_erro, 
				c04_w.ds_campo_incorreto, 
				c04_w.cd_erro );

			end if;

		end loop;
		close C04;
	end if;

end loop;
close C03;

commit;

end intpd_recebe_interno_dale_uv;
/