create or replace
procedure ctb_gerar_regra_cenario(	cd_estabelecimento_p	number,
				nr_seq_cenario_p		number,
				ie_regra_p		number,
				cd_centro_custo_p		number,
				nr_seq_metrica_p		number,
				cd_conta_contabil_p	varchar2,
				dt_referencia_p		date,
				dt_refer_final_p		date,
				vl_mes_p			number,
				ie_sobrepor_p		varchar2,
				ie_opcao_p		varchar2,
				nm_usuario_p		Varchar2) is 

dt_refer_final_w			date;
nr_seq_regra_w			number(10)	:= 1;
ie_sobrepor_w			varchar2(1);
vl_mes_w				number(15,2);
ie_periodo_w			number(10);

BEGIN

dt_refer_final_w		:= dt_refer_final_p;
if	(dt_refer_final_p is null) then
	dt_refer_final_w	:= pkg_date_utils.add_month(dt_referencia_p,11,0);
end if;

ie_sobrepor_w		:= nvl(ie_sobrepor_p, 'N');

if	(nvl(ie_opcao_p, 0) = 0) then

	vl_mes_w		:= vl_mes_p;
else	
	ie_periodo_w	:= abs(months_between(dt_referencia_p, dt_refer_final_w)) +1;
	vl_mes_w		:= dividir(vl_mes_p, ie_periodo_w);

end if;
if	(ie_regra_p = 0) then /* Regra M�trica*/
	begin
	/* Matheus comentei esta rotina em 08/10/2008 OS 111947 
	select	nvl(max(nr_seq_regra),0) + 5
	into	nr_seq_regra_w
	from	ctb_regra_metrica
	where	nr_seq_cenario	= nr_seq_cenario_p;*/
	if	(nvl(cd_centro_custo_p,0) <> 0) and
		(nvl(nr_seq_metrica_p,0) <> 0) then
		begin
		insert into ctb_regra_metrica(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_cenario,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_regra,
			nr_seq_metrica,
			nr_seq_mes_ref,
			cd_centro_custo,
			dt_mes_inic,
			dt_mes_fim,
			ie_regra,
			pr_aplicar,
			ie_sobrepor,
			qt_fixa)
		values(	ctb_regra_metrica_seq.nextval,
			cd_estabelecimento_p,
			nr_seq_cenario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_regra_w,
			nr_seq_metrica_p,
			null,
			cd_centro_custo_p,
			dt_referencia_p,
			dt_refer_final_w,
			'QF',
			null,
			ie_sobrepor_w,
			vl_mes_w);
		end;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(241166);
	end if;	
		
	end;
elsif	(ie_regra_p = 1) then /* Regra ticket medio*/
	begin
	
	
	insert into ctb_regra_ticket_medio(
		nr_sequencia,
		cd_estabelecimento,
		nr_seq_cenario,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_regra,
		nr_seq_metrica,
		nr_seq_mes_ref,
		cd_centro_custo,
		cd_conta_contabil,
		dt_mes_inic,
		dt_mes_fim,
		ie_regra,
		pr_aplicar,
		ie_sobrepor,
		vl_fixo)
	values(	ctb_regra_ticket_medio_seq.nextval,
		cd_estabelecimento_p,
		nr_seq_cenario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_regra_w,
		nr_seq_metrica_p,
		null,
		cd_centro_custo_p,
		cd_conta_contabil_p,
		dt_referencia_p,
		dt_refer_final_w,
		'VF',
		null,
		ie_sobrepor_w,
		vl_mes_w);
	end;
elsif	(ie_regra_p = 2) then /*Regra valor */
	begin
		
	insert into ctb_orc_cen_regra(
		nr_sequencia,
		cd_estabelecimento,
		nr_seq_cenario,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_regra,
		nr_seq_mes_ref_orig,
		cd_centro_custo,
		cd_conta_contabil,
		cd_classif_conta,
		cd_classif_centro,
		nr_seq_criterio_rateio,
		dt_mes_inic,
		dt_mes_fim,
		cd_centro_origem,
		cd_conta_origem,
		ie_regra_valor,
		pr_aplicar,
		ie_sobrepor,
		vl_fixo)
	values(	ctb_orc_cen_regra_seq.nextval,
		cd_estabelecimento_p,
		nr_seq_cenario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_regra_w,
		null,
		cd_centro_custo_p,
		cd_conta_contabil_p,
		null,
		null,
		null,
		dt_referencia_p,
		dt_refer_final_w,
		null,
		null,
		'VF',
		null,
		ie_sobrepor_w,
		vl_mes_w);
	end;
end if;

commit;

END ctb_gerar_regra_cenario;
/