create or replace
procedure TISS_GRAVAR_LOG_REAP
			(nr_seq_retorno_p	in number,
			nr_seq_lote_hist_p	in number,
			ds_log_p		in varchar2,
			nm_usuario_p		in varchar2) is
			
begin

insert into tiss_log_reap
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_retorno,
	nr_seq_lote_hist,
	ds_log)
values	(tiss_log_reap_seq.nextval,
	sysdate,
	nm_usuario_p,
	nr_seq_retorno_p,
	nr_seq_lote_hist_p,
	ds_log_p);
	
commit;

end TISS_GRAVAR_LOG_REAP;
/
