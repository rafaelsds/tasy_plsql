Create or replace
procedure CSL_GERAR_INTERF_ITAU	(nr_seq_envio_p		number,
				nm_usuario_p		varchar2) is


ie_tipo_registro_w		number(3);
nr_seq_forma_pagto_w		number(3);
ie_tipo_reg_w			number(3);
nr_inscricao_w			varchar2(20);
cd_agencia_w			varchar2(20);
cd_conta_w			varchar2(20);
ds_dac_agencia_w		varchar2(2);
nm_empresa_w			varchar2(255);
ds_banco_w			varchar2(255);
dt_arquivo_w			date;
nr_lote_w			number(4) := 0;
nr_seq_envio_w			number(10);

ds_endereco_w			varchar2(255);
nr_endereco_w			varchar2(10);
ds_cidade_w			varchar2(40);
cd_cep_w			varchar2(10);
sg_estado_w			varchar2(15);

nm_fornecedor_w			varchar2(80);
nr_documento_w			number(10);
dt_vencimento_w			date;
vl_saldo_titulo_w		number(15,2);
cd_cgc_fornecedor_w		varchar2(14);
cd_banco_w			number(5);

cd_barras_w			varchar2(255);
vl_titulo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_acrescimo_w			number(15,2);
dt_pagto_w			date;
vl_pagto_w			number(15,2);

qt_registros_w			number(10);
vl_total_pagto_w		number(15,2);

qt_lote_arquivo_w		number(10);
qt_reg_tabela_w			number(10);
qt_tot_reg_w			number(10);
nr_registro_w			number(10);

cursor c01 is
	select	distinct			-- 	Header lote Conta Corrente
		1,
		1,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,10),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	  and	c.nr_titulo		= k.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'CC'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote DOC
		1,
		3,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,10),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	  and	c.nr_titulo		= k.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'DOC'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote Ordem Pagamento
		1,
		10,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,10),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	  and	c.nr_titulo		= k.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'OP'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote TED
		1,
		41,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,10),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	  and	c.nr_titulo		= k.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'TED'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote bloqueto Ita�
		4,
		30,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,40),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc			= j.cd_cgc
	  and	c.nr_titulo			= k.nr_titulo
	  and	a.nr_sequencia			= c.nr_seq_escrit
	  and	a.cd_estabelecimento		= e.cd_estabelecimento
	  and	b.nr_sequencia			= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao		in ('EP','ECC')
	  and	c.ie_tipo_pagamento		= 'BLQ'
	  and	substr(k.nr_bloqueto,1,3)	= 341
	  and	a.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote bloqueto n�o Ita�
		4,
		31,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,40),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc			= j.cd_cgc
	  and	c.nr_titulo			= k.nr_titulo
	  and	a.nr_sequencia			= c.nr_seq_escrit
	  and	a.cd_estabelecimento		= e.cd_estabelecimento
	  and	b.nr_sequencia			= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao		in ('EP','ECC')
	  and	c.ie_tipo_pagamento		= 'BLQ'
	  and	substr(k.nr_bloqueto,1,3)	<> 341
	  and	a.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote Pagamento Concession�rias
		7,
		13,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,10),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	  and	c.nr_titulo		= k.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'PC'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct			-- 	Header lote DOC poupan�a
		1,
		5,
		substr(e.cd_cgc,1,14),
		substr(b.cd_agencia_bancaria,1,8),
		substr(b.cd_conta,1,15),
		substr(b.ie_digito_conta,1,2),
		substr(upper(elimina_acentuacao(j.ds_razao_social)),1,80),
		substr(upper(elimina_acentuacao(j.ds_endereco)),1,10),
		substr(j.nr_endereco,1,10),
		substr(upper(elimina_acentuacao(j.ds_municipio)),1,40),
		substr(j.cd_cep,1,10),
		substr(upper(j.sg_estado),1,2)
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	  and	c.nr_titulo		= k.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'CCP'
	  and	a.nr_sequencia		= nr_seq_envio_p
	order by 1;

cursor c02 is
	select	distinct
		2,			-- 	Detalhe Conta Corrente
		1,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		'',
		0,
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0) vl_acrescimo,
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao		= 'A'
	and	c.nr_titulo		= d.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'CC'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct
		2,			-- 	Detalhe DOC
		3,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		'',
		0,
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0) vl_acrescimo,
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao		= 'A'
	and	c.nr_titulo		= d.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'DOC'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct
		2,			-- 	Detalhe Ordem Pagamento
		10,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		'',
		0,
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0) vl_acrescimo,
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao		= 'A'
	and	c.nr_titulo		= d.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'OP'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct
		2,			-- 	Detalhe TED
		41,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		'',
		0,
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0) vl_acrescimo,
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao		= 'A'
	and	c.nr_titulo		= d.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'TED'
	  and	a.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct
		5,			-- 	Detalhe lote bloqueto Ita�
		30,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		d.nr_bloqueto,
		nvl(d.vl_titulo,0),
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0),
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao			= 'A'
	and	c.nr_titulo			= d.nr_titulo
	  and	a.nr_sequencia			= c.nr_seq_escrit
	  and	a.nr_seq_conta_banco		= b.nr_sequencia
	  and	a.cd_estabelecimento		= e.cd_estabelecimento
	  and 	b.ie_tipo_relacao		in ('EP','ECC')
	  and 	c.ie_tipo_pagamento		= 'BLQ'
	  and	substr(d.nr_bloqueto,1,3)	= 341
	  and	a.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct
		5,			--	Detalhe lote bloqueto n�o Ita�
		31,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		d.nr_bloqueto,
		nvl(d.vl_titulo,0),
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0),
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao			= 'A'
	and	c.nr_titulo			= d.nr_titulo
	  and	a.nr_sequencia			= c.nr_seq_escrit
	  and	a.nr_seq_conta_banco		= b.nr_sequencia
	  and	a.cd_estabelecimento		= e.cd_estabelecimento
	  and 	b.ie_tipo_relacao		in ('EP','ECC')
	  and 	c.ie_tipo_pagamento		= 'BLQ'
	  and	substr(d.nr_bloqueto,1,3)	<> 341
	  and	a.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct
		8,			--	Detalhe lote pagto concession�rias
		13,
		substr(e.cd_cgc,1,14),
		substr(b.ie_digito_conta,1,2),
		substr(b.cd_conta,1,15),
		b.cd_banco,
		substr(b.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		d.nr_bloqueto,
		0,
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0) vl_acrescimo,
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao			= 'A'
	and	c.nr_titulo			= d.nr_titulo
	  and	a.nr_sequencia			= c.nr_seq_escrit
	  and	a.nr_seq_conta_banco		= b.nr_sequencia
	  and	a.cd_estabelecimento		= e.cd_estabelecimento
	  and 	b.ie_tipo_relacao		in ('EP','ECC')
	  and 	c.ie_tipo_pagamento		= 'PC'
	  and	a.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct
		2,			-- 	Detalhe DOC Poupan�a
		5,
		substr(e.cd_cgc,1,14),
		substr(c.ie_digito_conta,1,2),
		substr(c.nr_conta,1,15),
		c.cd_banco,
		substr(c.cd_agencia_bancaria,1,8),
		substr(upper(elimina_acentuacao(substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,255))),1,80),
		d.nr_titulo,
		d.dt_vencimento_atual,
		d.vl_saldo_titulo,
		nvl(f.nr_cpf,d.cd_cgc) cd_favorecido,
		'',
		0,
		c.vl_desconto,
		decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0) vl_acrescimo,
		(nvl(c.vl_escritural,0) + decode(nvl(c.vl_acrescimo,0),0,nvl(d.vl_outros_acrescimos,0),nvl(c.vl_acrescimo,0)) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(d.vl_outras_despesas,0)) vl_pagto
	from	pessoa_fisica f,
		estabelecimento e,
		banco_estabelecimento_v b,
		titulo_pagar_v d,
		titulo_pagar_escrit c,
		banco_escritural a
	where	d.cd_pessoa_fisica	= f.cd_pessoa_fisica(+)
	and	d.ie_situacao		= 'A'
	and	c.nr_titulo		= d.nr_titulo
	  and	a.nr_sequencia		= c.nr_seq_escrit
	  and	a.cd_estabelecimento	= e.cd_estabelecimento
	  and	b.nr_sequencia		= a.nr_seq_conta_banco
	  and	b.ie_tipo_relacao	in ('EP','ECC')
	  and	c.ie_tipo_pagamento	= 'CCP'
	  and	a.nr_sequencia		= nr_seq_envio_p
	order by 1,2;

/* Francisco - 25/03/2008 - OS 83242 - Tirei de todos os selects "-c.vl_desconto + c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)" */
cursor c03 is
	select	distinct		-- 	Trailler Conta Corrente
		3,
		1,
		count(*),
		sum((nvl(c.vl_escritural,0) + nvl(c.vl_acrescimo,0) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		estabelecimento a,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia		= c.nr_seq_escrit
	  and	e.cd_estabelecimento    = a.cd_estabelecimento
	  and	k.nr_titulo		= c.nr_titulo
	  and	b.nr_sequencia		= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao      	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento	= 'CC'
	  and	e.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct		-- 	Trailler documento
		3,
		3,
		count(*),
		sum((nvl(c.vl_escritural,0) + nvl(c.vl_acrescimo,0) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		estabelecimento a,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia		= c.nr_seq_escrit
	  and	e.cd_estabelecimento    = a.cd_estabelecimento
	  and	k.nr_titulo		= c.nr_titulo
	  and	b.nr_sequencia		= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao      	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento	= 'DOC'
	  and	e.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct		-- 	Trailler Ordem Pagamento
		3,
		10,
		count(*),
		sum((nvl(c.vl_escritural,0) + nvl(c.vl_acrescimo,0) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		estabelecimento a,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia		= c.nr_seq_escrit
	  and	e.cd_estabelecimento    = a.cd_estabelecimento
	  and	k.nr_titulo		= c.nr_titulo
	  and	b.nr_sequencia		= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao      	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento	= 'OP'
	  and	e.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct		-- 	Trailler TED
		3,
		41,
		count(*),
		sum((nvl(c.vl_escritural,0) + nvl(c.vl_acrescimo,0) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0)))  vl_total_pagto
	from	banco_estabelecimento_v b,
		estabelecimento a,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia		= c.nr_seq_escrit
	  and	e.cd_estabelecimento    = a.cd_estabelecimento
	  and	k.nr_titulo		= c.nr_titulo
	  and	b.nr_sequencia		= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao      	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento	= 'TED'
	  and	e.nr_sequencia		= nr_seq_envio_p
	union
	select	distinct		--	Trailler bloqueto Ita�
		6,
		30,
		count(*),
		sum((c.vl_escritural + c.vl_acrescimo - c.vl_desconto + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		titulo_pagar_v2 d,
		estabelecimento a,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia			= c.nr_seq_escrit
	  and	c.nr_titulo			= d.nr_titulo
	  and	e.cd_estabelecimento    	= a.cd_estabelecimento
	  and	b.nr_sequencia			= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao       	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento		= 'BLQ'
	  and	substr(d.nr_bloqueto,1,3)	= 341
	  and	e.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct		--	Trailler bloqueto n�o Ita�
		6,
		31,
		count(*),
		sum((c.vl_escritural + c.vl_acrescimo - c.vl_desconto + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		titulo_pagar_v2 d,
		estabelecimento a,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia			= c.nr_seq_escrit
	  and	c.nr_titulo			= d.nr_titulo
	  and	e.cd_estabelecimento    	= a.cd_estabelecimento
	  and	b.nr_sequencia			= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao       	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento		= 'BLQ'
	  and	substr(d.nr_bloqueto,1,3)	<> 341
	  and	e.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct		--	Trailler pagto concession�rias
		9,
		13,
		count(*),
		sum((nvl(c.vl_escritural,0) + nvl(c.vl_acrescimo,0) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		titulo_pagar_v2 d,
		estabelecimento a,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia			= c.nr_seq_escrit
	  and	c.nr_titulo			= d.nr_titulo
	  and	e.cd_estabelecimento    	= a.cd_estabelecimento
	  and	b.nr_sequencia			= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao       	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento		= 'PC'
	  and	e.nr_sequencia			= nr_seq_envio_p
	union
	select	distinct		-- 	Trailler DOC poupan�a
		3,
		5,
		count(*),
		sum((nvl(c.vl_escritural,0) + nvl(c.vl_acrescimo,0) - nvl(c.vl_desconto,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) + nvl(c.vl_despesa,0))) vl_total_pagto
	from	banco_estabelecimento_v b,
		estabelecimento a,
		titulo_pagar k,
		titulo_pagar_escrit c,
		banco_escritural e
	where	e.nr_sequencia		= c.nr_seq_escrit
	  and	e.cd_estabelecimento    = a.cd_estabelecimento
	  and	k.nr_titulo		= c.nr_titulo
	  and	b.nr_sequencia		= e.nr_seq_conta_banco
	  and	b.ie_tipo_relacao      	in ('EP','ECC')
	  and 	c.ie_tipo_pagamento	= 'CCP'
	  and	e.nr_sequencia		= nr_seq_envio_p
	order by 1;

begin

delete	from w_interf_itau;
commit;

ie_tipo_reg_w	:= 0;
nr_registro_w	:= 0;

select	a.cd_cgc,
	somente_numero(g.cd_agencia_bancaria),
	g.cd_conta,
	g.ie_digito_conta,
	upper(p.ds_razao_social),
	g.ds_banco,
	e.dt_atualizacao,
	e.nr_sequencia
into	nr_inscricao_w,
	cd_agencia_w,
	cd_conta_w,
	ds_dac_agencia_w,
	nm_empresa_w,
	ds_banco_w,
	dt_arquivo_w,
	nr_seq_envio_w
from	estabelecimento a,
	banco_estabelecimento_v g,
	pessoa_juridica p,
	banco_escritural e
where	e.cd_estabelecimento   	= a.cd_estabelecimento
  and	a.cd_cgc		= p.cd_cgc
  and	g.nr_sequencia		= e.nr_seq_conta_banco
  and	g.ie_tipo_relacao       in ('EP','ECC')
  and	e.nr_sequencia		= nr_seq_envio_p;

if	(nr_seq_envio_w > 0) and
	(nr_seq_envio_w is not null) then
	/*	Header do Arquivo	*/
	insert into w_interf_itau
		(nr_sequencia,
		ie_tipo_registro,
		nr_seq_forma_pagto,
		nr_inscricao,
		cd_agencia_bancaria,
		cd_conta,
		ds_dac_agencia,
		nm_empresa,
		ds_banco,
		dt_geracao,
		dt_atualizacao,
		nm_usuario,
		nr_lote)
	values	(w_interf_itau_seq.nextval,
		0,
		0,
		nr_inscricao_w,
		cd_agencia_w,
		cd_conta_w,
		ds_dac_agencia_w,
		nm_empresa_w,
		ds_banco_w,
		dt_arquivo_w,
		sysdate,
		nm_usuario_p,
		0);

	open c01;
	loop
	fetch c01 into 	ie_tipo_registro_w,
			nr_seq_forma_pagto_w,
			nr_inscricao_w,
			cd_agencia_w,
			cd_conta_w,
			ds_dac_agencia_w,
			nm_empresa_w,
			ds_endereco_w,
			nr_endereco_w,
			ds_cidade_w,
			cd_cep_w,
			sg_estado_w;
	exit when c01%notfound;
		begin
		insert into w_interf_itau
			(nr_sequencia,
			ie_tipo_registro,
			nr_seq_forma_pagto,
			nr_lote,
			nr_inscricao,
			cd_agencia_bancaria,
			cd_conta,
			ds_dac_agencia,
			nm_empresa,
			ds_endereco,
			nr_endereco,
			ds_cidade,
			cd_cep,
			sg_estado,
			dt_atualizacao,
			nm_usuario)
		values	(w_interf_itau_seq.nextval,
			ie_tipo_registro_w,
			nr_seq_forma_pagto_w,
			nr_lote_w,
			nr_inscricao_w,
			cd_agencia_w,
			cd_conta_w,
			ds_dac_agencia_w,
			nm_empresa_w,
			ds_endereco_w,
			nr_endereco_w,
			ds_cidade_w,
			cd_cep_w,
			substr(sg_estado_w,1,2),
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into ie_tipo_registro_w,
			nr_seq_forma_pagto_w,
			nr_inscricao_w,
			ds_dac_agencia_w,
			cd_conta_w,
			cd_banco_w,
			cd_agencia_w,
			nm_fornecedor_w,
			nr_documento_w,
			dt_vencimento_w,
			vl_saldo_titulo_w,
			cd_cgc_fornecedor_w,
			cd_barras_w,
			vl_titulo_w,
			vl_desconto_w,
			vl_acrescimo_w,
			vl_pagto_w;
	exit when c02%notfound;
		begin	

		if	(ie_tipo_reg_w	= 0) then
			nr_registro_w	:= nr_registro_w + 1;
			ie_tipo_reg_w	:= nr_seq_forma_pagto_w;
		elsif	(ie_tipo_reg_w	= nr_seq_forma_pagto_w) then
			nr_registro_w	:= nr_registro_w + 1;
		else
			nr_registro_w	:= 1;
			ie_tipo_reg_w	:= nr_seq_forma_pagto_w;
		end if;

		insert into w_interf_itau
			(nr_sequencia,
			ie_tipo_registro,
			nr_seq_forma_pagto,
			nr_lote,
			nr_registro,
			nr_inscricao,
			ds_dac_agencia,
			cd_conta,
			cd_banco_fornec,
			cd_agencia_bancaria,
			nm_fornecedor,
			nr_documento,
			dt_vencimento,
			vl_saldo_titulo,
			cd_cgc_fornecedor,
			cd_barras,
			vl_titulo,
			vl_desconto,
			vl_acrescimo,
			dt_pagto,
			vl_pagto,
			dt_atualizacao,
			nm_usuario)
		values	(w_interf_itau_seq.nextval,
			ie_tipo_registro_w,
			nr_seq_forma_pagto_w,
			nr_lote_w,
			nr_registro_w,
			nr_inscricao_w,
			ds_dac_agencia_w,
			cd_conta_w,
			cd_banco_w,
			cd_agencia_w,
			nm_fornecedor_w,
			nr_documento_w,
			dt_vencimento_w,
			vl_saldo_titulo_w,
			cd_cgc_fornecedor_w,
			cd_barras_w,
			vl_titulo_w,
			vl_desconto_w,
			vl_acrescimo_w,
			dt_vencimento_w,
			vl_pagto_w,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close c02;

	open c03;
	loop
	fetch c03 into	ie_tipo_registro_w,
			nr_seq_forma_pagto_w,
			qt_registros_w,
			vl_total_pagto_w;
	exit when c03%notfound;

		if	(qt_registros_w > 0) then

			begin
			insert into w_interf_itau
				(nr_sequencia,
				ie_tipo_registro,
				nr_seq_forma_pagto,
				nr_lote,
				qt_registros,
				vl_total_pagto,
				dt_atualizacao,
				nm_usuario)
			values	(w_interf_itau_seq.nextval,
				ie_tipo_registro_w,
				nr_seq_forma_pagto_w,
				nr_lote_w,
				qt_registros_w + 2,
				vl_total_pagto_w,
				sysdate,
				nm_usuario_p);
			end;
		end if;
	end loop;
	close c03;
end if;

commit;

select	count(*)
into	qt_reg_tabela_w
from	w_interf_itau;

if	(qt_reg_tabela_w > 0) then

	select	count(*)
	into	qt_lote_arquivo_w
	from	w_interf_itau
	where	ie_tipo_registro in (1,4,7);

	select	w_interf_itau_seq.nextval
	into	nr_seq_envio_w
	from	dual;

	select	count(*)
	into	qt_tot_reg_w
	from	banco_estabelecimento_v b,
		estabelecimento a,
		banco_escritural e
	where	e.cd_estabelecimento    = a.cd_estabelecimento
	  and	b.ie_tipo_relacao       in ('EP','ECC')
	  and	b.nr_sequencia		= e.nr_seq_conta_banco
	  and	e.nr_sequencia		= nr_seq_envio_p;

	insert into w_interf_itau
		(nr_sequencia,
		ie_tipo_registro,
		qt_lote_arquivo,
		qt_tot_reg,
		dt_atualizacao,
		nm_usuario)
	values	(nr_seq_envio_w,
		10,
		qt_lote_arquivo_w,
		qt_tot_reg_w,
		sysdate,
		nm_usuario_p);
end if;

select	count(*)
into	qt_registros_W
from	w_interf_itau
where	ie_tipo_registro 	= 2
and	nr_seq_forma_pagto	= 1;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (1,2,3)
	  and	nr_seq_forma_pagto	= 1;
end if;

select	count(*)
into	qt_registros_W
from	w_interf_itau
where	ie_tipo_registro = 2
and	nr_seq_forma_pagto	= 3;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (1,2,3)
	  and	nr_seq_forma_pagto	= 3;
end if;

select	count(*)
into	qt_registros_W
from	w_interf_itau
where	ie_tipo_registro = 2
and	nr_seq_forma_pagto	= 10;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (1,2,3)
	  and	nr_seq_forma_pagto	= 10;
end if;

select	count(*)
into	qt_registros_W
from	w_interf_itau
where	ie_tipo_registro = 2
and	nr_seq_forma_pagto	= 41;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (1,2,3)
	  and	nr_seq_forma_pagto	= 41;
end if;

select	count(*)
into	qt_registros_w
from	w_interf_itau
where	ie_tipo_registro	= 5
and	nr_seq_forma_pagto	= 30;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (4,5,6)
	  and	nr_seq_forma_pagto	= 30;
end if;

select	count(*)
into	qt_registros_w
from	w_interf_itau
where	ie_tipo_registro	= 5
and	nr_seq_forma_pagto	= 31;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (4,5,6)
	  and	nr_seq_forma_pagto	= 31;
end if;

select	count(*)
into	qt_registros_w
from	w_interf_itau
where	ie_tipo_registro	= 8
and	nr_seq_forma_pagto	= 13;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (7,8,9)
	  and	nr_seq_forma_pagto	= 13;
end if;

/* Gerar um novo lote para forma de pagto 5 - S�rio */
select	count(*)
into	qt_registros_w
from	w_interf_itau
where	ie_tipo_registro	= 2
and	nr_seq_forma_pagto	= 5;

if	(qt_registros_w > 0) then
	nr_lote_w	:= nr_lote_w + 1;

	update	w_interf_itau
	set	nr_lote			= nr_lote_w
	where	ie_tipo_registro	in (1,2,3)
	  and	nr_seq_forma_pagto	= 5;
end if;

commit;

end CSL_GERAR_INTERF_ITAU;
/