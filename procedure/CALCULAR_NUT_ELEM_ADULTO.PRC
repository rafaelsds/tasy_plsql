create or replace 
procedure Calcular_Nut_Elem_Adulto(	
				nr_sequencia_p		number,
				nm_usuario_p		varchar2) is

qt_peso_w					number(15,4);
nr_sequencia_w				number(10);
nr_seq_elemento_w			number(10);
qt_elem_kg_dia_w			number(15,4);
qt_diaria_w					number(15,4);
pr_total_w					number(3);
qt_kcal_w					nut_pac_elemento.qt_kcal%type;
ie_tipo_elemento_w			varchar2(3);
qt_conversao_ml_w			number(15,4);
qt_volume_w					number(15,4);
qt_vol_fase_w				number(15,4);
qt_vol_1_fase_w				number(15,4);
qt_vol_2_fase_w				number(15,4);
qt_vol_3_fase_w				number(15,4);
qt_vol_4_fase_w				number(15,4);
qt_fase_npt_w				number(15);
qt_fase_npt_per_w			number(15);
nr_seq_elem_mat_w			number(10,0);
cd_material_w				number(6);
qt_protocolo_w				number(15,4);
qt_limite_litro_w			number(15,4);
qt_volume_diario_w			number(15,2);
qt_formula_lipideo_w 		number(15,4);
qt_formula_carb_w			number(15,4);
VarCAlculaQuantidade_w		varchar2(01);
qt_grama_nitrogenio_w		Number(15,4);
qt_vol_prot_w				Number(15,3);
qt_vol_glutamina_w			Number(15,3);
qt_diaria_glutamina_w		number(30,6);
qt_nitrogenio_w				Number(15,4);
VarConformeProtocolo_w		varchar2(1);
nr_seq_elem_w				number(10);
ie_editado_w				varchar2(1);
ie_glutamina_w				varchar2(1);
nr_seq_nut_elem_mat_w		number(10);
ie_prim_fase_w				nut_pac_elemento.ie_prim_fase%type;
ie_seg_fase_w				nut_pac_elemento.ie_seg_fase%type;
ie_terc_fase_w				nut_pac_elemento.ie_terc_fase%type;
ie_quar_fase_w				nut_pac_elemento.ie_quar_fase%type;

cursor c01 is
select	nr_sequencia,
		qt_volume,
		nr_seq_elem_mat,
		cd_material,
		nvl(ie_editado,'N')
from	nut_pac_elem_mat
where	nr_seq_nut_pac	= nr_sequencia_p;

cursor c02 is
select	a.nr_sequencia,
		c.qt_conversao_ml,
		b.ie_tipo_elemento,
		a.qt_diaria,
		a.qt_protocolo,
		a.nr_seq_elemento
from	nut_elem_material c,
		nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_elemento	= b.nr_sequencia
and		b.nr_sequencia	= c.nr_seq_elemento
and		c.cd_material	= cd_material_w
and		a.nr_seq_nut_pac	= nr_sequencia_p
and		nvl(c.ie_tipo,'NPT')	= 'NPT';
	  
cursor c03 is
select	a.nr_sequencia,
		c.qt_conversao_ml,
		b.ie_tipo_elemento,
		a.qt_diaria,
		a.qt_protocolo,
		b.qt_limite_litro
from	nut_elem_material c,
		nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_elemento	= b.nr_sequencia
and		b.nr_sequencia	= c.nr_seq_elemento
and		c.cd_material	= cd_material_w
and		a.nr_seq_nut_pac	= nr_sequencia_p
and		nvl(c.ie_tipo,'NPT')	= 'NPT'
and		b.qt_limite_litro is not null
and		nvl(b.ie_limite_concentracao,'N') = 'S';

begin

Obter_Param_Usuario(924,1034,obter_perfil_ativo,nm_usuario_p,0,qt_formula_lipideo_w);
Obter_Param_Usuario(924,1035,obter_perfil_ativo,nm_usuario_p,0,qt_formula_carb_w);
Obter_Param_Usuario(924,1072,obter_perfil_ativo,nm_usuario_p,0,VarCalculaQuantidade_w);
Obter_Param_Usuario(924,1085,obter_perfil_ativo,nm_usuario_p,0,VarConformeProtocolo_w);

select	nvl(max(nvl(qt_peso_ajustado,qt_peso)),0),
		nvl(max(qt_volume_diario),0),
		nvl(max(qt_fase_npt),0)
into	qt_peso_w,
		qt_volume_diario_w,
		qt_fase_npt_w
from	nut_pac
where	nr_sequencia	= nr_sequencia_p;

if	(VarConformeProtocolo_w = 'S') then
	update	nut_pac_elemento
	set		qt_diaria		= nvl(qt_protocolo,0)
	where	nr_seq_nut_pac	= nr_sequencia_p;
else
	update	nut_pac_elemento
	set		qt_diaria		= 0
	where	nr_seq_nut_pac	= nr_sequencia_p;
end if;

commit;

open c01;
loop
fetch c01 into
	nr_seq_nut_elem_mat_w,
	qt_volume_w,
	nr_seq_elem_mat_w,
	cd_material_w,
	ie_editado_w;	
exit when c01%notfound;
	
	open c02;
	loop
	fetch c02 into
      		nr_seq_elemento_w,
			qt_conversao_ml_w,
			ie_tipo_elemento_w,
			qt_diaria_w,
			qt_protocolo_w,
			nr_seq_elem_w;	
	exit when c02%notfound;
		if	(VarConformeProtocolo_w = 'N') then
			qt_diaria_w			:= dividir(qt_volume_w,qt_conversao_ml_w);
		elsif (ie_editado_w = 'S') then
			select	sum(dividir(a.qt_volume,b.qt_conversao_ml))
			into	qt_diaria_w
			from	nut_pac_elem_mat a,
					nut_elem_material b
			where	a.nr_seq_nut_pac = nr_sequencia_p 
			and		a.cd_material = b.cd_material
			and		b.nr_seq_elemento = nr_seq_elem_w;
		end if;
		
		qt_elem_kg_dia_w	:= dividir(qt_diaria_w, qt_peso_w);
		
		qt_kcal_w		:= 0;
		if	(ie_tipo_elemento_w = 'C') then /* Carboidrato */
			qt_kcal_w	:= nvl(qt_diaria_w,qt_protocolo_w) * qt_formula_carb_w;
		elsif	(ie_tipo_elemento_w = 'P') then /* Proteina */
			
			if	(VarCAlculaQuantidade_w = 'N') then
				qt_kcal_w	:= nvl(qt_diaria_w,qt_protocolo_w) * 4;
			else
				select	nvl(sum(Obter_vol_elem_nut_pac_ad(a.nr_sequencia)),0),
						max(Obter_n2_elem_nut_pac(a.nr_sequencia))
				into	qt_vol_prot_w,
						qt_nitrogenio_w
				from	nut_elemento b,
						nut_pac_elemento a
				where	a.nr_seq_nut_pac	= nr_sequencia_p
				and		a.nr_seq_elemento	= b.nr_sequencia
				and		b.ie_tipo_elemento	= 'P'
				and		a.nr_sequencia = nr_seq_elemento_w;
				
				select	nvl(sum(Obter_dos_elem_nut_pac_ad(a.nr_sequencia)),0),
						nvl(sum(Obter_vol_elem_nut_pac_ad(a.nr_sequencia)),0)
				into	qt_diaria_glutamina_w,
						qt_vol_glutamina_w
				from	nut_elemento b,
						nut_pac_elemento a
				where	a.nr_seq_nut_pac	= nr_sequencia_p
				and		a.nr_seq_elemento	= b.nr_sequencia
				and		b.ie_tipo_elemento	= 'P'
				and		nvl(b.ie_glutamina,'N') = 'S';
				
				qt_grama_nitrogenio_w	:= ((qt_vol_prot_w * nvl(qt_nitrogenio_w,1.6) / 100) + (qt_diaria_glutamina_w/6.25)) ;
				qt_kcal_w				:= (qt_vol_prot_w * nvl(qt_nitrogenio_w,1.6) / 100) * 6.25 * 4;

			end if;	
		elsif	(ie_tipo_elemento_w = 'L') then  /* Lipidio */
			qt_kcal_w	:= nvl(qt_diaria_w,qt_protocolo_w) * qt_formula_lipideo_w;
		end if;
			
		update	nut_pac_elemento
		set		qt_diaria			= nvl(qt_diaria_w,0),
				qt_elem_kg_dia		= nvl(qt_elem_kg_dia_w,0),
				qt_kcal				= nvl(qt_kcal_w,0),
				qt_grama_nitrogenio = qt_grama_nitrogenio_w
		where	nr_sequencia		= nr_seq_elemento_w
		and		nr_seq_nut_pac		= nr_sequencia_p;
		commit;
	end loop;
	close c02;
	
	select	nvl(max(ie_prim_fase),'N'),
			nvl(max(ie_seg_fase),'N'),
			nvl(max(ie_terc_fase),'N'),
			nvl(max(ie_quar_fase),'N')
	into	ie_prim_fase_w,
			ie_seg_fase_w,
			ie_terc_fase_w,
			ie_quar_fase_w
	from	nut_pac_elemento a,
			nut_elem_material b,
			nut_pac_elem_mat c			
	where	a.nr_seq_elemento	= b.nr_seq_elemento
	and		a.nr_seq_nut_pac 	= c.nr_seq_nut_pac
	and		b.cd_material		= c.cd_material
	and		c.nr_sequencia 		= nr_seq_nut_elem_mat_w;
	
	verificar_limite_litro_npt(nr_sequencia_p,nr_seq_nut_elem_mat_w,nm_usuario_p);
	

	select	max(qt_volume)
	into	qt_volume_w
	from	nut_pac_elem_mat
	where	nr_sequencia	= nr_seq_nut_elem_mat_w;
	
	qt_fase_npt_per_w := 0;
	if (ie_quar_fase_w = 'S') then
		qt_fase_npt_per_w := 4;
	elsif (ie_terc_fase_w = 'S') then
		qt_fase_npt_per_w := 3;
	elsif (ie_seg_fase_w = 'S') then
		qt_fase_npt_per_w := 2;	
	elsif (ie_prim_fase_w = 'S') then
		qt_fase_npt_per_w := 1;
	end if;
	qt_vol_fase_w 	:= 0;
	qt_vol_1_fase_w	:= 0;
	qt_vol_2_fase_w	:= 0;
	qt_vol_3_fase_w	:= 0;
	qt_vol_4_fase_w	:= 0;
	if (qt_fase_npt_w > qt_fase_npt_per_w) then
		qt_vol_fase_w := dividir_sem_round(qt_volume_w,qt_fase_npt_per_w);
	else
		qt_vol_fase_w := dividir_sem_round(qt_volume_w,qt_fase_npt_w);
	end if;
	
	if (qt_fase_npt_w > 0) and
	   (ie_prim_fase_W = 'S')then
		qt_vol_1_fase_w := qt_vol_fase_w;
	end if;
	if (qt_fase_npt_w > 1) and
	   (ie_seg_fase_w = 'S')then
		qt_vol_2_fase_w := qt_vol_fase_w;
	end if;
	if (qt_fase_npt_w > 2) and
	   (ie_terc_fase_w = 'S') then
		qt_vol_3_fase_w := qt_vol_fase_w;
	end if;
	if (qt_fase_npt_w > 3) and
	   (ie_quar_fase_w = 'S') then
		qt_vol_2_fase_w := qt_vol_fase_w;
	end if;
	
	update	nut_pac_elem_mat
	set		qt_vol_1_fase	= nvl(qt_vol_1_fase_w,0),
			qt_vol_2_fase	= nvl(qt_vol_2_fase_w,0),
			qt_vol_3_fase	= nvl(qt_vol_3_fase_w,0),
			qt_vol_4_fase	= nvl(qt_vol_4_fase_w,0)
	where	nr_sequencia	= nr_seq_nut_elem_mat_w;

end loop;
close c01;

commit;

end Calcular_Nut_Elem_Adulto;
/