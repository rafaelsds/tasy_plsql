Create or replace
PROCEDURE Gerar_Taxa_Sala_Cir_Esp(
				nr_sequencia_p		in	Number,
				nm_usuario_p		in  	Varchar2,
				ie_regra_p		out 	varchar2) as

nr_atendimento_w		Number(10);
cd_estabelecimento_w		Number(10);
cd_convenio_w			Number(5);

nr_sequencia_w			Number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_taxa_w			number(10);

qt_proc_porte_w			number(10);
qt_porte_w			number(10);
nr_seq_gerada_w			number(10);
dt_entrada_unidade_w		date;
ie_regra_w			varchar2(01) := 'N';
nr_seq_proc_interno_w		number(10,0);
cd_categoria_w			varchar2(10);
dt_procedimento_w		date;
cd_setor_atend_w		setor_atendimento.cd_setor_atendimento%type;
nr_seq_proc_int_w		number(10,0);
qt_lanc_taxa_w			number(10,0);

type campos is record (nr_sequencia	number(10));
type Vetor is table of campos index by binary_integer;

proc_princ_w			Vetor;
i				integer	:= 0;

Cursor C01 is 
	select	a.nr_sequencia,
		b.cd_procedimento,
		b.ie_origem_proced,
		b.nr_sequencia nr_seq_taxa,
		b.qt_porte,
		b.nr_seq_proc_interno,
		b.qt_procedimento
	from	Tipo_taxa_cirurgica 	b,
		procedimento_paciente	a
	where	nr_atendimento	= nr_atendimento_w
	and	b.nr_sequencia	= obter_porte_especial(
					cd_estabelecimento_w,
					a.cd_convenio,
					a.cd_procedimento,
					a.ie_origem_proced,
					a.cd_setor_atendimento,
					a.dt_procedimento)
	order by qt_porte;

BEGIN

select	b.nr_atendimento,
	b.cd_estabelecimento,
	a.cd_convenio,
	a.cd_categoria,
	a.dt_procedimento,
	a.cd_setor_atendimento
into	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	cd_categoria_w,
	dt_procedimento_w,
	cd_setor_atend_w
from	atendimento_paciente b,
	procedimento_paciente a
where	a.nr_sequencia	= nr_sequencia_p
  and	a.nr_atendimento	= b.nr_atendimento;  

OPEN C01;
LOOP
FETCH C01 into	nr_sequencia_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_taxa_w,
		qt_porte_w,
		nr_seq_proc_interno_w,
		qt_lanc_taxa_w;
EXIT when C01%notfound;
	nr_sequencia_w	:= nr_sequencia_w;
	
	select	count(*)
	into	qt_proc_porte_w
	from procedimento_paciente
	where nr_seq_proc_princ = nr_sequencia_w
	  and ds_observacao like '%' || obter_desc_expressao(729219) || '%';

	if	(qt_proc_porte_w > 0) then
		i := i + 1;
		proc_princ_w(i).nr_sequencia := nr_sequencia_w;
	end if;
END LOOP;
CLOSE C01;

if	(nr_sequencia_p = nr_sequencia_w) then
	FOR i in 1..proc_princ_w.count LOOP
		update procedimento_paciente
		set nr_seq_proc_princ = null
		where nr_seq_proc_princ in (	select nr_sequencia 
						from procedimento_paciente
						where nr_seq_proc_princ = proc_princ_w(i).nr_sequencia
						  and ds_observacao like '%' || obter_desc_expressao(729219) || '%');

		delete procedimento_paciente
		where nr_seq_proc_princ = proc_princ_w(i).nr_sequencia;
	END LOOP;

	select	count(*)
	into	qt_proc_porte_w
	from procedimento_paciente
	where nr_seq_proc_princ = nr_sequencia_w
	  and ds_observacao like '%' || obter_desc_expressao(729219) || '%'; 
	
	if	(qt_proc_porte_w = 0) then
		duplicar_proc_paciente(nr_sequencia_w, nm_usuario_p, nr_seq_gerada_w);

		nr_seq_proc_int_w:= null;
		if	(nvl(cd_procedimento_w,0) = 0) and (nvl(nr_seq_proc_interno_w,0) > 0) then
			Obter_Proc_Tab_Interno_Conv(nr_seq_proc_interno_w, cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, null,
					null, cd_procedimento_w, ie_origem_proced_w,cd_setor_atend_w,dt_procedimento_w, 0,
					null, null, null, null, null, null, null);
			nr_seq_proc_int_w:=  nr_seq_proc_interno_w;
		end if;
		
		update procedimento_paciente
		set	cd_procedimento		= cd_procedimento_w,
			ie_origem_proced	= ie_origem_proced_w,
			nr_seq_proc_interno 	= nr_seq_proc_int_w,
			qt_porte_anestesico	= qt_porte_w,
			ie_valor_informado	= 'N',
			tx_procedimento		= 100,
			nr_seq_proc_princ	= nr_sequencia_w,
			cd_pessoa_fisica	= null,
			cd_medico_executor	= null,
			ie_funcao_medico	= null,
			ie_emite_conta	= null,
			ie_emite_conta_honor	= null,
			ds_observacao 		= substr(decode(ds_observacao, null, '', ds_observacao || chr(10)) ||
						  obter_desc_expressao(729219) || nr_seq_taxa_w,1,255),
			qt_procedimento		= nvl(qt_lanc_taxa_w,qt_procedimento)
		where nr_sequencia = nr_seq_gerada_w;

		Atualiza_Preco_Procedimento(nr_seq_gerada_w, cd_convenio_w, nm_usuario_p);

		Gerar_lancamento_automatico(nr_atendimento_w, null, 34, 'Tasy',	nr_seq_gerada_w,null,null,null,null,null);

		ie_regra_w	:= 'S';
	end if;
end if;

ie_regra_p	:= ie_regra_w;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

END Gerar_Taxa_Sala_Cir_Esp;
/
