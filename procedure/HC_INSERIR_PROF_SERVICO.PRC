create or replace
procedure hc_inserir_prof_servico(nr_seq_servico_p	number,
				nr_seq_pac_servico_p	number,
				cd_origem_servico_p	Varchar2,
				nm_usuario_p		Varchar2) is 


cd_pessoa_fisica_w	varchar2(10);
nr_seq_equipe_w		number(10);
			
Cursor C01 is
	select	a.nr_seq_equipe,
		b.cd_pessoa_fisica
	from	hc_servico_equipe a,
		HC_EQUIPE_PROF b
	where	nr_seq_servico = nr_seq_servico_p
	and	a.nr_seq_equipe = b.nr_seq_equipe
	and	not exists (	select	1
				from	hc_resp_servico_paciente c
				where	c.nr_seq_equipe = a.nr_seq_equipe
				and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
				and	c.nr_seq_pac_servico = nr_seq_pac_servico_p);

Cursor C02 is  				
	SELECT	a.nr_seq_equipe,
		b.cd_pessoa_fisica
	FROM	hc_servico_equipe a,
		HC_SERVICO_PROF b
	WHERE	a.nr_seq_servico = b.nr_seq_servico
	AND	a.nr_seq_servico = nr_seq_servico_p
	AND	NOT EXISTS (	SELECT	1
				FROM	hc_resp_servico_paciente c
				WHERE	c.nr_seq_equipe = a.nr_seq_equipe
				AND	b.cd_pessoa_fisica = c.cd_pessoa_fisica
				AND	c.nr_seq_pac_servico = nr_seq_pac_servico_p);
	
begin

if	(nr_seq_pac_servico_p is not null) and
	(nr_seq_servico_p is not null) and 
	(cd_origem_servico_p = 'S')then

	open C01;
	loop
	fetch C01 into	
		nr_seq_equipe_w,
		cd_pessoa_fisica_w;
	exit when C01%notfound;
		begin
		
		insert into hc_resp_servico_paciente (	
			nr_sequencia,
			cd_pessoa_fisica,
			nr_seq_equipe,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_pac_servico)
		values(	hc_resp_servico_paciente_seq.nextVal,
			cd_pessoa_fisica_w,
			nr_seq_equipe_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_pac_servico_p);
		
		end;
	end loop;
	close C01;
		
elsif	(nr_seq_pac_servico_p is not null) and
	(nr_seq_servico_p is not null) and 
	(cd_origem_servico_p = 'E') then

	open C02;
	loop
	fetch C02 into	
		nr_seq_equipe_w,
		cd_pessoa_fisica_w;
	exit when C02%notfound;
		begin
		
		insert into hc_resp_servico_paciente (	
			nr_sequencia,
			cd_pessoa_fisica,
			nr_seq_equipe,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_pac_servico)
		values(	hc_resp_servico_paciente_seq.nextVal,
			cd_pessoa_fisica_w,
			nr_seq_equipe_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_pac_servico_p);
		
		end;
	end loop;
	close C02;	
	
end if;
commit;

end hc_inserir_prof_servico;
/