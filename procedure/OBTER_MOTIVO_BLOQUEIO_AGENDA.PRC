create or replace
procedure Obter_motivo_bloqueio_agenda	(cd_agenda_p		number,
						dt_horario_p		date,
						ie_dia_semana_p	number,
						nr_seq_motivo_p out	varchar2) is

nr_seq_motivo_bloq_ag_w		number(10,0);

begin
nr_seq_motivo_bloq_ag_w	:= null;

if	(cd_agenda_p is not null) and
	(dt_horario_p is not null) and
	(ie_dia_semana_p is not null) then
	/* consistir bloqueio per�odo */
	select	max(nr_seq_motivo_bloq_ag)
	into	nr_seq_motivo_bloq_ag_w
	from	agenda_bloqueio
	where	cd_agenda = cd_agenda_p
	and	trunc(dt_horario_p) between dt_inicial and dt_final
	and	hr_inicio_bloqueio is null
	and	hr_final_bloqueio is null
	and	ie_dia_semana is null;


	/* consistir bloqueio hor�rio */
	if	(nr_seq_motivo_bloq_ag_w is null) then
		select	max(nr_seq_motivo_bloq_ag)
		into	nr_seq_motivo_bloq_ag_w
		from	agenda_bloqueio
		where	cd_agenda = cd_agenda_p
		and	trunc(dt_horario_p) between dt_inicial and dt_final
		and	dt_horario_p between	to_date(to_char(dt_horario_p,'dd/mm/yyyy') ||' '|| to_char(hr_inicio_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and 						to_date(to_char(dt_horario_p,'dd/mm/yyyy') ||' '|| to_char(hr_final_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		and	hr_inicio_bloqueio is not null
		and	hr_final_bloqueio is not null
		and	hr_inicio_bloqueio < hr_final_bloqueio
		and	ie_dia_semana is null;
	end if;
	/* consistir bloqueio dia */
	
	if	(nr_seq_motivo_bloq_ag_w is null) then
		select	max(nr_seq_motivo_bloq_ag)
		into	nr_seq_motivo_bloq_ag_w
		from	agenda_bloqueio
		where	cd_agenda = cd_agenda_p
		and	trunc(dt_horario_p) between dt_inicial and dt_final
		and	hr_inicio_bloqueio is null
		and	hr_final_bloqueio is null
		and	ie_dia_semana is not null
		and	((ie_dia_semana = ie_dia_semana_p) or (ie_dia_semana = 9));
	end if;
	/* consistir bloqueio dia e hor�rio */
	if	(nr_seq_motivo_bloq_ag_w is null) then
		select	max(nr_seq_motivo_bloq_ag)
		into	nr_seq_motivo_bloq_ag_w
		from	agenda_bloqueio
		where	cd_agenda = cd_agenda_p
		and	trunc(dt_horario_p) between dt_inicial and dt_final
		and	dt_horario_p between	to_date(to_char(dt_horario_p,'dd/mm/yyyy') ||' '|| to_char(hr_inicio_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and 						to_date(to_char(dt_horario_p,'dd/mm/yyyy') ||' '|| to_char(hr_final_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		and	hr_inicio_bloqueio is not null
		and	hr_final_bloqueio is not null
		and	hr_inicio_bloqueio < hr_final_bloqueio
		and	ie_dia_semana is not null
		and	((ie_dia_semana = ie_dia_semana_p) or (ie_dia_semana = 9));
	end if;
end if;

nr_seq_motivo_p := nr_seq_motivo_bloq_ag_w;

end Obter_motivo_bloqueio_agenda;
/
