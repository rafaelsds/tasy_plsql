create or replace
procedure ctb_trocar_conta_contabil_mov(cd_estabelecimento_p		number,
					nr_seq_mes_ref_p		number,
					nr_lote_contabil_p		number,
					cd_conta_contabil_antiga_p	varchar2,
					cd_conta_contabil_nova_p	varchar2,
					nm_usuario_p			Varchar2) is 

cd_classificacao_w			varchar2(40);
cd_classif_credito_w		varchar2(40);
cd_classif_debito_w			varchar2(40);
dt_referencia_w				date;
nr_lote_contabil_w			number(10);
ds_erro_w                   varchar2(255) := null;
contador_w                  number(10) := 0;

cursor c01 is
select	a.nr_lote_contabil
from	lote_contabil a
where	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.dt_atualizacao_saldo is null
and	a.nr_lote_contabil	= nvl(nr_lote_contabil_w, a.nr_lote_contabil)
and	exists(	select	1
		from	ctb_movimento_v y
		where	y.nr_lote_contabil	= a.nr_lote_contabil
		and	y.cd_conta_contabil	= cd_conta_contabil_antiga_p); 



cursor c02 is
select	a.nr_sequencia,
	a.cd_conta_debito cd_conta_contabil,
	'D' ie_debito_credito
from	ctb_movimento a
where	a.nr_lote_contabil	= nr_lote_contabil_w
and	a.cd_conta_debito	= cd_conta_contabil_antiga_p
union
select	a.nr_sequencia,
	a.cd_conta_credito,
	'C' ie_debito_credito
from	ctb_movimento a
where	a.nr_lote_contabil	= nr_lote_contabil_w
and	a.cd_conta_credito	= cd_conta_contabil_antiga_p;

vet02	C02%Rowtype;

begin

if	(nvl(nr_lote_contabil_p,0) <> 0) then
	nr_lote_contabil_w	:= nr_lote_contabil_p;
end if;

select	dt_referencia
into	dt_referencia_w
from	ctb_mes_ref
where	nr_sequencia	= nr_seq_mes_ref_p;


cd_classificacao_w	:= substr(ctb_obter_classif_conta(cd_conta_contabil_nova_p, null, dt_referencia_w),1,40);

open c01;
loop
fetch c01 into
	nr_lote_contabil_w;
exit when c01%notfound;
	begin	
	
	open C02;
	loop
	fetch C02 into	
		vet02;
	exit when C02%notfound;
		begin
		
		cd_classif_credito_w	:= '';
		cd_classif_debito_w	:= '';
			
		if	(vet02.ie_debito_credito = 'D') then
			begin
                cd_classif_debito_w	:= cd_classificacao_w;
                update	ctb_movimento
                set	cd_conta_debito		= cd_conta_contabil_nova_p,
                    cd_classif_debito		= cd_classif_debito_w,
                    nm_usuario		= nm_usuario_p,
                    dt_atualizacao		= sysdate
                where	nr_sequencia		= vet02.nr_sequencia;
                exception when others then
                    ds_erro_w := SQLERRM(sqlcode);
			end;
		elsif	(vet02.ie_debito_credito = 'C') then
			begin
                cd_classif_credito_w		:= cd_classificacao_w;
                
                update	ctb_movimento
                set	cd_conta_credito		= cd_conta_contabil_nova_p,
                    cd_classif_credito		= cd_classif_credito_w,
                    nm_usuario		= nm_usuario_p,
                    dt_atualizacao		= sysdate
                where	nr_sequencia		= vet02.nr_sequencia;
                exception when others then
                    ds_erro_w := SQLERRM(sqlcode);
			end;
		end if;
		contador_w := contador_w + 1;
		end;
	end loop;
	close C02;

	end;
end loop;
close c01;

if(ds_erro_w is null and contador_w > 0) then	
    commit;
end if;

if (ds_erro_w is not null) then
    wheb_mensagem_pck.exibir_mensagem_abort(substr(ds_erro_w,1,255));
end if;

if(contador_w = 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(140484);--Conta nao encontrada
end if;

end ctb_trocar_conta_contabil_mov;
/