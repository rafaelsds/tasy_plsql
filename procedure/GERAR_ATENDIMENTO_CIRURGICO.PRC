create or replace
procedure gerar_atendimento_cirurgico	(	nr_seq_agenda_p			in		number,
														ds_questiona_aval_pre_p	out	varchar2,
														nr_prescricao_p out number) 
														is
												
cd_estabelecimento_w				number(5);
cd_perfil_w							number(5);
nm_usuario_w						varchar2(15);
nr_atendimento_w					number(10);
cd_pf_usuario_w					varchar2(10);
ie_gera_data_prev_alta_w		varchar2(15);
ie_status_painel_w				varchar2(15);
ie_executa_evento_w				varchar2(15);
ie_atualiza_ged_w					varchar2(15);
ie_gera_oftalmo_w					varchar2(15);
nr_prescr_aval_pre_w				number(14);
ie_pf_nula_prescr_w				varchar2(15);
nr_cirurgia_w						number(10);
cd_medico_w							varchar2(10);
cd_procedimento_w					number(15);
cd_pessoa_fisica_w				varchar2(10);
nr_seq_atend_futuro_w			number(10);
nr_seq_pepo_w						number(10);
ie_gerar_cirurgia_w				varchar2(15);
cd_setor_atendimento_w			number(5);
cd_setor_cirurgia_w				number(5);
ie_vincula_atend_cirurgia_w	varchar2(15);
ie_integracao_w					varchar2(15);
ie_estrutura_pepo_w				varchar2(15);
ie_origem_proced_w				agenda_paciente.ie_origem_proced%type;



begin
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w				:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w			:= wheb_usuario_pck.get_nm_usuario;

select	max(obter_pf_usuario(nm_usuario_w,'C'))
into		cd_pf_usuario_w
from		dual;

Obter_Param_Usuario(871, 144, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_gerar_cirurgia_w);
Obter_Param_Usuario(871, 154, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,cd_setor_cirurgia_w);
Obter_Param_Usuario(871, 257, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_integracao_w);
Obter_Param_Usuario(871, 327, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_vincula_atend_cirurgia_w);
Obter_Param_Usuario(871, 441, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_gera_data_prev_alta_w);
Obter_Param_Usuario(871, 528, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_status_painel_w);
Obter_Param_Usuario(871, 657, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_atualiza_ged_w);
Obter_Param_Usuario(871, 688, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_gera_oftalmo_w);
Obter_Param_Usuario(872, 158, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w,ie_estrutura_pepo_w);

select	nvl(max(nr_atendimento),0)
into		nr_atendimento_w
from		agenda_paciente
where		nr_sequencia = nr_seq_agenda_p;

if	(nr_atendimento_w > 0) then
	gerar_evento_internacao_opme(nr_seq_agenda_p,nm_usuario_w,cd_estabelecimento_w);
	gerar_evento_gerar_atendimento(nr_seq_agenda_p,nm_usuario_w,cd_estabelecimento_w);

	if	(ie_gera_data_prev_alta_w = 'S') then
		atualiza_data_previsao_alta(nr_seq_agenda_p);
	end if;

	if	(ie_status_painel_w is not null) then
		gerar_dados_painel_cirurgia(ie_status_painel_w,nr_seq_agenda_p,'A',nm_usuario_w,'S');
	end if;

	select	max(obter_se_existe_evento_agenda(cd_estabelecimento_w,'AGA','CI'))
	into		ie_executa_evento_w
	from 		dual;

	if	(ie_executa_evento_w = 'S') then
		executar_evento_agenda('AGA','CI',nr_seq_agenda_p,cd_estabelecimento_w,nm_usuario_w,null,null);
	end if;

	if	(ie_atualiza_ged_w = 'S') then
		exportar_anexo_agenda_ged(nm_usuario_w,cd_pf_usuario_w,nr_seq_agenda_p);
	end if;	

	if	(ie_gera_oftalmo_w = 'S') then
		gerar_consulta_oft_agenda(null,nr_seq_agenda_p,nm_usuario_w,cd_estabelecimento_w);
	end if;

	select	nvl(max(b.nr_prescricao),0)
	into		nr_prescr_aval_pre_w
	from 		conclusao_recom_apae b,
				aval_pre_anestesica a
	where		a.nr_sequencia 	= b.nr_seq_aval_pre
	and		a.nr_seq_agenda 	= nr_seq_agenda_p;

	if	(nr_prescr_aval_pre_w > 0) then
		ds_questiona_aval_pre_p	:= substr(obter_texto_tasy (88840, wheb_usuario_pck.get_nr_seq_idioma),1,255); --Deseja vincular o atendimento � prescri��o da avalia��o pr�-anest�sica?
		nr_prescricao_p := nr_prescr_aval_pre_w;
	end if;

	select	nullable
	into		ie_pf_nula_prescr_w
	from   	user_tab_columns
	where  	table_name 		= 'PRESCR_MEDICA'
	and    	column_name 	= 'CD_PESSOA_FISICA';


	select	max(nr_atendimento),
				max(nr_cirurgia),
				max(cd_medico),
				max(cd_procedimento),
				max(cd_pessoa_fisica),
				max(ie_origem_proced),
				max(nr_seq_atend_futuro)
	into		nr_atendimento_w,
				nr_cirurgia_w,
				cd_medico_w,
				cd_procedimento_w,
				cd_pessoa_fisica_w,
				ie_origem_proced_w,
				nr_seq_atend_futuro_w
	from		agenda_paciente
	where		nr_sequencia = nr_seq_agenda_p;

	if	(ie_estrutura_pepo_w = 'S') then
		select	max(nr_seq_pepo)
		into		nr_seq_pepo_w
		from		cirurgia
		where		nr_cirurgia = nr_cirurgia_w;
	end if;

	vinc_atendimento_cirurgia(nr_seq_agenda_p,nr_atendimento_w,nm_usuario_w);

	enviar_email_regra(nr_seq_agenda_p,'SA',nm_usuario_w,cd_estabelecimento_w);

	if	(ie_gerar_cirurgia_w = 'S') and
		(nr_cirurgia_w is null) and
		(nr_atendimento_w is not null) and
		(cd_medico_w is not null) and
		((ie_pf_nula_prescr_w = 'Y') or (cd_pessoa_fisica_w is not null)) and
		(cd_procedimento_w is not null) and
		(ie_origem_proced_w is not null) then
		if	(cd_setor_cirurgia_w is not null) then
			cd_setor_atendimento_w := cd_setor_cirurgia_w;
		else	
			select	nvl(max(cd_setor_atendimento),wheb_usuario_pck.get_cd_setor_atendimento)
			into		cd_setor_atendimento_w
			from 		atend_paciente_unidade
			where 	nr_seq_interno = obter_atepacu_paciente(nr_atendimento_w, 'A');
		end if;	
		Gerar_Cirurgia_Agenda(cd_estabelecimento_w,nr_seq_agenda_p,nm_usuario_w,cd_setor_atendimento_w);
	end if;	

	atualizar_dados_atend_futuro (nr_seq_atend_futuro_w,nr_atendimento_w,nm_usuario_w);

	if	(ie_vincula_atend_cirurgia_w = 'S') and (nr_cirurgia_w is not null) then
		if	(nr_seq_pepo_w is not null) then
			vincular_atend_cirurgia(nr_atendimento_w,nr_cirurgia_w,nm_usuario_w,nr_seq_pepo_w);
		else
			vincular_atend_cirurgia(nr_atendimento_w,nr_cirurgia_w,nm_usuario_w,null);
		end if;
	end if;

	if	(ie_integracao_w <> 'N') then
		vincula_atend_agend_opme(nr_seq_agenda_p,'V',nm_usuario_w);
	end if;	
end if;	

end gerar_atendimento_cirurgico;
/
