create or replace
procedure GERAR_FLUXO_CAIXA_LOTE
			(dt_referencia_p	date,
			cd_conta_financ_p	number,
			ds_observacao_p		varchar2,
			ie_identificacao_p	varchar2,
			ie_integracao_p		varchar2,
			nm_usuario_p		varchar2,
			cd_agenda_p		number,
			nr_interno_conta_p	number,
			nr_ordem_compra_p	number,
			nr_repasse_terceiro_p	number,
			nr_seq_cheque_cp_p	number,
			nr_seq_cheque_cr_p	number,
			nr_seq_contrato_p	number,
			nr_seq_conv_receb_p	number,
			nr_seq_lote_fluxo_p	number,
			nr_seq_movto_cartao_p	number,
			nr_seq_movto_trans_p	number,
			nr_seq_proj_recurso_p	number,
			nr_seq_protocolo_p	number,
			nr_seq_regra_p		number,
			nr_titulo_pagar_p	number,
			nr_titulo_receber_p	number,
			vl_fluxo_p		number) is

nr_seq_fluxo_item_w	fluxo_caixa_item.nr_sequencia%type;
ie_acumular_fluxo_w	regra_fluxo_caixa.ie_acumular_fluxo%type	:= 'S';
dt_referencia_w		date;
cd_moeda_fluxo_w	number(5);
ds_pessoa_w		fluxo_caixa_item.ds_nome%type;

/*--------------------------------------------------------------- ATEN��O ----------------------------------------------------------------*/
/* Cuidado ao realizar altera��es no fluxo de caixa em lote. Toda e qualquer altera��o realizada em qualquer uma */
/* das procedures do fluxo de caixa em lote deve ser cuidadosamente verificada e realizada no fluxo de caixa      */
/* convencional. Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando     */
/* assim que existam diferen�as entre os fluxos de caixa.                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA EM LOTE ALTERAR TAMB�M O FLUXO DE CAIXA ---------------*/

/*IE_IENTIFICACAO_P
1P at� 11P	- procedure gerar_fluxo_caixa_lote_passado
1R at� 9R 	- procedure gerar_fluxo_caixa_lote_real
1V at� 3V	- procedure  gerar_fluxo_caixa_lot_vencido
12 - procedure gerar_fluxo_lote_saldo_banco
13 - procedure gerar_fluxo_caixa_lote_agrup
14 - procedure gerar_fluxo_caixa_lote_externo
15 - procedure gerar_fluxo_caixa_lote_agenda
16 - procedure gerar_fluxo_caixa_lote_contrat
17 - procedure gerar_fluxo_lote_prev_contrato
18 - procedure gerar_fluxo_caixa_lote_oc
19 - procedure gerar_fluxo_caixa_lote_sus
20 - procedure gerar_fluxo_caixa_lote_repasse
21 - procedure gerar_fluxo_caixa_lote_vl_fixo
22 - procedure gerar_fluxo_caixa_lote_vl_regr
23 - procedure acumula_fluxo_caixa_lote_nivel
24 - procedure calcular_fluxo_caixa_lote
25 - procedure gerar_fluxo_caixa_lote_mes_ant
26 - procedure gerar_fluxo_regra_saldo
*/

begin

dt_referencia_w	:= trunc(dt_referencia_p);

/* Projeto Multimoeda - Busca a moeda do fluxo. */
select	max(cd_moeda)
into	cd_moeda_fluxo_w
from 	fluxo_caixa_lote
where	nr_sequencia = nr_seq_lote_fluxo_p;

select	nvl(max(nr_sequencia),0)
into	nr_seq_fluxo_item_w
from	fluxo_caixa_item
where	cd_conta_financ			= cd_conta_financ_p
and	dt_referencia			= dt_referencia_w
and	ie_integracao			= ie_integracao_p
and	nvl(nr_interno_conta,0)		= nvl(nr_interno_conta_p,0)
and	nvl(nr_ordem_compra,0)		= nvl(nr_ordem_compra_p,0)
and	nvl(nr_repasse_terceiro,0)	= nvl(nr_repasse_terceiro_p,0)
and	nvl(cd_agenda,0)		= nvl(cd_agenda_p,0)
and	nvl(nr_seq_cheque_cp,0)		= nvl(nr_seq_cheque_cp_p,0)
and	nvl(nr_seq_cheque_cr,0)		= nvl(nr_seq_cheque_cr_p,0)
and	nvl(nr_seq_contrato,0)		= nvl(nr_seq_contrato_p,0)
and	nvl(nr_seq_conv_receb,0)	= nvl(nr_seq_conv_receb_p,0)
and	nvl(nr_seq_lote_fluxo,0)	= nvl(nr_seq_lote_fluxo_p,0)
and	nvl(nr_seq_movto_cartao,0)	= nvl(nr_seq_movto_cartao_p,0)
and	nvl(nr_seq_movto_trans,0)	= nvl(nr_seq_movto_trans_p,0)
and	nvl(nr_seq_proj_recurso,0)	= nvl(nr_seq_proj_recurso_p,0)
and	nvl(nr_seq_protocolo,0)		= nvl(nr_seq_protocolo_p,0)
and	nvl(nr_seq_regra,0)		= nvl(nr_seq_regra_p,0)
and	nvl(nr_titulo_pagar,0)		= nvl(nr_titulo_pagar_p,0)
and	nvl(nr_titulo_receber,0)	= nvl(nr_titulo_receber_p,0)
and	nvl(cd_moeda,0) 		= nvl(cd_moeda_fluxo_w,0);

if	(nr_seq_fluxo_item_w = 0) then
	
	if (nvl(nr_titulo_pagar_p,0) > 0) then
		select	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc), 1,100)
		into	ds_pessoa_w
		from	titulo_pagar
		where	nr_titulo = nr_titulo_pagar_p;
	elsif (nvl(nr_titulo_receber_p,0) > 0) then
		select	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc), 1,100)
		into	ds_pessoa_w
		from	titulo_receber
		where	nr_titulo = nr_titulo_receber_p;
	elsif (nvl(nr_seq_cheque_cp_p,0) > 0) then
		select	substr(nvl(ds_destinatario,obter_nome_pf_pj(cd_pessoa_destinatario, cd_cgc_destinatario)), 1,100)
		into	ds_pessoa_w
		from	cheque
		where	nr_sequencia = nr_seq_cheque_cp_p;
	elsif (nvl(nr_seq_cheque_cr_p,0) > 0) then
		select	substr(obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc), 1,100)
		into	ds_pessoa_w
		from	cheque_cr
		where	nr_seq_cheque = nr_seq_cheque_cr_p;
	elsif (nvl(nr_interno_conta_p,0) > 0) then
		select	substr(obter_nome_pf_pj(max(cd_pessoa_fisica), max(cd_cgc)), 1,100)
		into	ds_pessoa_w
		from	titulo_receber
		where	nr_interno_conta = nr_interno_conta_p;
	elsif (nvl(nr_seq_protocolo_p,0) > 0) then
		select	substr(wheb_mensagem_pck.get_texto(296585) || ' ' || a.nr_protocolo || ' - ' || b.ds_convenio,1,100)
		into	ds_pessoa_w
		from	protocolo_convenio a,
			convenio b
		where	a.nr_seq_protocolo = nr_seq_protocolo_p
		and	a.cd_convenio = b.cd_convenio;
	elsif (nvl(nr_seq_contrato_p,0) > 0) then
		select	(substr(ds_objeto_contrato,1,100) || ' - ' || substr(obter_nome_pf_pj(cd_pessoa_contratada,cd_cgc_contratado),1,60))
		into	ds_pessoa_w
		from	contrato
		where	nr_sequencia = nr_seq_contrato_p;
	elsif (nvl(nr_ordem_compra_p,0) > 0) then
		select	substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc_fornecedor),1,100)
		into	ds_pessoa_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_p;
	elsif (nvl(nr_seq_conv_receb_p,0) > 0) then
		select	substr(b.ds_convenio,1,100)
		into	ds_pessoa_w
		from	convenio_receb a,
			convenio b
		where	a.cd_convenio = b.cd_convenio
		and	a.nr_sequencia = nr_seq_conv_receb_p;
	elsif (nvl(nr_seq_movto_cartao_p,0) > 0) then
		select	substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc),1,100)
		into	ds_pessoa_w
		from	movto_cartao_cr
		where	nr_sequencia = nr_seq_movto_cartao_p;
	elsif (nvl(nr_seq_movto_trans_p,0) > 0) then
		select	case 	when a.nr_seq_caixa is not null then substr(c.ds_caixa||' - '||b.ds_transacao||' - '||obter_nome_pf_pj(null,a.cd_cgc),1,255)
				when a.nr_seq_banco is not null then substr(d.ds_banco||' - '||b.ds_transacao||' - '||obter_nome_pf_pj(null,a.cd_cgc),1,255)
				else substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,100) end
		into	ds_pessoa_w
		from	movto_trans_financ a,
			transacao_financeira b,
			caixa c,
			banco_estabelecimento_v d
		where	a.nr_seq_trans_financ = b.nr_sequencia
		and	a.nr_seq_caixa = c.nr_sequencia(+)
		and	a.nr_seq_banco = d.nr_sequencia(+)
		and	a.nr_sequencia = nr_seq_movto_trans_p;
	elsif (nvl(nr_seq_proj_recurso_p,0) > 0) then
		ds_pessoa_w := '';
	elsif (nvl(nr_repasse_terceiro_p,0) > 0) then
		ds_pessoa_w := '';
	elsif (nvl(cd_agenda_p,0) > 0) then
		ds_pessoa_w := '';
	elsif (nvl(nr_seq_regra_p,0) > 0) then
		select	substr(ds_titulo,1,100)
		into	ds_pessoa_w
		from	regra_fluxo_caixa
		where	nr_sequencia = nr_seq_regra_p;
	end if;
	
	insert into fluxo_caixa_item (
		nr_sequencia,
		cd_conta_financ,
		ds_observacao,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_referencia,
		ie_identificacao,
		ie_integracao,
		nm_usuario,
		nm_usuario_nrec,
		nr_interno_conta,
		nr_ordem_compra,
		nr_repasse_terceiro,
		cd_agenda,
		nr_seq_cheque_cp,
		nr_seq_cheque_cr,
		nr_seq_contrato,
		nr_seq_conv_receb,
		nr_seq_lote_fluxo,
		nr_seq_movto_cartao,
		nr_seq_movto_trans,
		nr_seq_proj_recurso,
		nr_seq_protocolo,
		nr_seq_regra,
		nr_titulo_pagar,
		nr_titulo_receber,
		vl_fluxo,
		cd_moeda,
		ds_nome)
	values (fluxo_caixa_item_seq.nextval,
		cd_conta_financ_p,
		ds_observacao_p,
		sysdate,
		sysdate,
		dt_referencia_w,
		ie_identificacao_p,
		ie_integracao_p,
		nm_usuario_p,
		nm_usuario_p,
		cd_agenda_p,
		nr_interno_conta_p,
		nr_ordem_compra_p,
		nr_repasse_terceiro_p,
		nr_seq_cheque_cp_p,
		nr_seq_cheque_cr_p,
		nr_seq_contrato_p,
		nr_seq_conv_receb_p,
		nr_seq_lote_fluxo_p,
		nr_seq_movto_cartao_p,
		nr_seq_movto_trans_p,
		nr_seq_proj_recurso_p,
		nr_seq_protocolo_p,
		nr_seq_regra_p,
		nr_titulo_pagar_p,
		nr_titulo_receber_p,
		vl_fluxo_p,
		cd_moeda_fluxo_w,
		ds_pessoa_w);
else
	if	(nr_seq_regra_p is not null) then
		select	ie_acumular_fluxo
		into	ie_acumular_fluxo_w
		from	regra_fluxo_caixa
		where	nr_sequencia = nr_seq_regra_p;
	end if;
	/* Aten��o! Quando o fluxo for referente a saldo di�rio de conta banc�ria, ie_identificacao_p = 12, 
		n�o deve ocorrer acumulo do fluxo de caixa, uma vez que saldo banc�rio n�o acumula. 
		O mesmo vale para o ie_identificacao_p = 26, onde � gerado o saldo di�rio de banco + saldo di�rio de caixa, onde n�o deve acumular valores. */
	update	fluxo_caixa_item
	set	vl_fluxo	= decode(ie_identificacao_p,'12',nvl(vl_fluxo_p,0),'26',nvl(vl_fluxo_p,0),
							decode(ie_acumular_fluxo_w, 'S', nvl(vl_fluxo,0) + nvl(vl_fluxo_p,0), nvl(vl_fluxo_p,0)))
	where	nr_sequencia	= nr_seq_fluxo_item_w;
end if;

end GERAR_FLUXO_CAIXA_LOTE;
/
