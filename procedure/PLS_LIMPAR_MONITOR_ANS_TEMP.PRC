create or replace
procedure pls_limpar_monitor_ans_temp
			(	ie_opcao_p	varchar2) is 

/*
ie_opcao_p:		TR - TRUNCATE - W_PLS_MONITOR_ANS_RET
		TQ - TRUNCATE - W_PLS_MONITOR_ANS_QUALID
*/

begin

if	(nvl(ie_opcao_p,'X') = 'TR') then
	
	execute immediate 'truncate table w_pls_monitor_ans_ret';

elsif	(nvl(ie_opcao_p,'X') = 'TQ') then
	
	execute immediate 'truncate table w_pls_monitor_ans_qualid';
end if;

commit;

end pls_limpar_monitor_ans_temp;
/