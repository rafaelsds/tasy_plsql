create or replace procedure fis_gerar_reg_C101_icmsipi(	nr_seq_controle_p	number,
							nr_seq_nota_p 		number) is

/*REGISTRO C101: INFORMA��O COMPLEMENTAR DOS DOCUMENTOS FISCAIS QUANDO DAS OPERA��ES INTERESTADUAIS DESTINADAS A CONSUMIDOR FINAL N�O CONTRIBUINTE EC 87/15 (C�DIGO 55)*/

-- VARIABLES
ie_gerou_dados_bloco_w varchar2(1) := 'N';

nr_seq_icmsipi_C101_w fis_efd_icmsipi_C101.nr_sequencia%type;

qt_cursor_w number(10) := 0;
nr_vetor_w  number(10) := 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

/*Cursor que retorna as informa��es para o registro C101 restringindo pela sequencia da nota fiscal*/
cursor c_reg_C101 is
select 	0 vl_fcp_uf_dest, 
	0 vl_icms_uf_dest, 
	0 vl_icms_uf_rem
from dual;

/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_C101*/
type reg_c_reg_C101 is table of c_reg_C101%RowType;
vetRegC101 reg_c_reg_C101;

/*Cria��o do array com o tipo sendo da tabela eespecificada - FIS_EFD_ICMSIPI_C101 */
type registro is table of fis_efd_icmsipi_C101%rowtype index by pls_integer;
fis_registros_w registro;

begin

/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_reg_C101;
loop
fetch c_reg_C101 bulk collect	into vetRegC101 limit 1000;
	for i in 1 .. vetRegC101.Count loop
	begin     

	/*Incrementa a variavel para o array*/
	qt_cursor_w:=	qt_cursor_w + 1;

	if (ie_gerou_dados_bloco_w = 'N') then
	ie_gerou_dados_bloco_w := 'S';
	end if;      

	/*Busca da sequencia da tabela especificada - fis_efd_icmsipi_C101 */
	select fis_efd_icmsipi_C101_seq.nextval	into nr_seq_icmsipi_C101_w	from dual;      

	/*Inserindo valores no array para realiza��o do forall posteriormente*/      
	fis_registros_w(qt_cursor_w).nr_sequencia 		:= nr_seq_icmsipi_C101_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao 		:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario 		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao_nrec 	:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario_nrec 		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).cd_reg 			:= 'C101';	
	fis_registros_w(qt_cursor_w).vl_fcp_uf_dest 		:= vetRegC101(i).vl_fcp_uf_dest;
	fis_registros_w(qt_cursor_w).vl_icms_uf_dest 		:= vetRegC101(i).vl_icms_uf_dest;
	fis_registros_w(qt_cursor_w).vl_icms_uf_rem 		:= vetRegC101(i).vl_icms_uf_rem;
	fis_registros_w(qt_cursor_w).nr_seq_nota 		:= nr_seq_nota_p;
	fis_registros_w(qt_cursor_w).nr_seq_controle 		:= nr_seq_controle_p;

	if (nr_vetor_w >= 1000) then
	begin
		/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_C101 */
		forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_C101 values fis_registros_w(i);          

		nr_vetor_w := 0;
		fis_registros_w.delete;          

		commit;          

	end;
	end if;      

	/*incrementa variavel para realizar o forall quando chegar no valor limite*/
	nr_vetor_w := nr_vetor_w + 1;

	end;
	end loop;
exit when c_reg_C101%notfound;
end loop;

close c_reg_C101;

if (fis_registros_w.count > 0) then
begin
	/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
	forall i in fis_registros_w.first .. fis_registros_w.last
	insert into fis_efd_icmsipi_C101 values fis_registros_w (i);

	fis_registros_w.delete;

	commit;

end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

/*Atualiza��o informa��o no controle de gera��o de registro para SIM*/
if (ie_gerou_dados_bloco_w = 'S') then
update fis_efd_icmsipi_controle set ie_mov_C = 'S' where nr_sequencia = nr_seq_controle_p;
end if;
end fis_gerar_reg_C101_icmsipi;
/