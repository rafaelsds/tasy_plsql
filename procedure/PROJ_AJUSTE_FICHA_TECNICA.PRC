create or replace
procedure proj_ajuste_ficha_tecnica is 

nr_seq_projeto_w	number(10);
qt_registros_w		number(10);
ds_objetivo_w		varchar2(2000);

Cursor C01 is
select 	nr_sequencia,
	ds_objetivo
from	proj_projeto
where	ds_objetivo is not null;	


begin

open C01;
loop
fetch C01 into	
	nr_seq_projeto_w,
	ds_objetivo_w;
exit when C01%notfound;
	begin
		
		select 	count(*)
		into	qt_registros_w
		from	proj_ficha_projeto
		where	nr_seq_projeto = nr_seq_projeto_w;
		
		
		if (qt_registros_w = 0) then
			insert into proj_ficha_projeto (NR_SEQUENCIA, 
							DT_ATUALIZACAO, 
							NM_USUARIO, 
							DT_ATUALIZACAO_NREC, 
							NM_USUARIO_NREC, 
							NR_SEQ_PROJETO, 
							DS_OBJETIVO)
			values			       (proj_ficha_projeto_seq.nextval,
							sysdate,
							'Tasy',
							sysdate,
							'Tasy',
							nr_seq_projeto_w,
							ds_objetivo_w);
		else
			select 	count(*)
			into	qt_registros_w
			from	proj_ficha_projeto
			where	nr_seq_projeto = nr_seq_projeto_w
			and	ds_objetivo is null;
			
			if (qt_registros_w > 0 ) then
				update 	proj_ficha_projeto
				set	ds_objetivo = ds_objetivo_w
				where	nr_seq_projeto = nr_seq_projeto_w
				and	ds_objetivo is null;
			end if;
		end if;
		
		
	end;
end loop;
close C01;


commit;

end proj_ajuste_ficha_tecnica;
/