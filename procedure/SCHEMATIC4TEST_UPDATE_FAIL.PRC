CREATE OR REPLACE PROCEDURE SCHEMATIC4TEST_UPDATE_FAIL(NR_SEQ_SCHEDULE_P NUMBER, NR_SEQ_SUITE_P NUMBER) IS
NR_SEQ_SCRIPT_W NUMBER(10,0);
NR_SEQ_SUITE_W NUMBER(10,0);
NR_SEQUENCIA_W  NUMBER(10,0);
NR_SEQ_SCRIPT_W2 NUMBER(10,0);
DS_ARQUIVO_W2 VARCHAR2(4000);
NM_SUITE_W VARCHAR2(255);
NM_SCRIPT_W VARCHAR2(255);
DS_VERSION_W VARCHAR2(255);
DT_EXECUTION_W DATE;
DT_ATUALIZACAO_NREC_W DATE;
DT_EXECUTION_SOMA DATE;
QTD NUMBER(10,0);
QTD2 NUMBER(10,0);
QTD3 NUMBER(10,0);
QTD4 NUMBER(10,0);
QTD5 NUMBER(10,0);
QTD_VERSION_W NUMBER(10,0);
NR_SEQ_SCHEDULE_OLD_W NUMBER(10,0);
IE_TIPO_ANEXO_W VARCHAR2(5);
NM_USUARIO_NREC_OLD_W VARCHAR2(15);

--loop scripts with succeful
CURSOR C01 IS
  SELECT log1.NR_SEQ_SCRIPT NR_SEQ_SCRIPT
      INTO NR_SEQ_SCRIPT_W
  FROM SCHEM_TEST_LOG log1
	WHERE log1.NR_SEQ_SCHEDULE = NR_SEQ_SCHEDULE_P
  AND log1.IE_STATUS = '4';   
  
BEGIN
    --procedure that update suite that was reexec fail
    SELECT COUNT(suite.NM_SUITE) NM_SUITE 
        INTO QTD
    FROM SCHEM_TEST_SUITE suite
    WHERE NM_SUITE LIKE 'Failure from%'
    AND NR_SEQUENCIA = NR_SEQ_SUITE_P;   

    IF (QTD <> 0) THEN   
      SELECT MAX(NR_SEQUENCIA) NR_SEQUENCIA, NM_SUITE
        INTO NR_SEQ_SUITE_W, NM_SUITE_W
      FROM SCHEM_TEST_SUITE suite
      WHERE NM_SUITE LIKE 'Failure from%'
      AND NR_SEQUENCIA = NR_SEQ_SUITE_P
      GROUP BY NR_SEQUENCIA, NM_SUITE;      
      
      NR_SEQ_SCHEDULE_OLD_W := TO_NUMBER(regexp_replace(substr(NM_SUITE_W,1,20), '[^0-9]', ''));
    
      UPDATE SCHEM_TEST_SCHEDULE SET DS_RETEST = NR_SEQ_SCHEDULE_P WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_OLD_W;
      COMMIT;
      
      SELECT COUNT(NR_SEQUENCIA) QTD_VERSION
          INTO QTD_VERSION_W
      FROM SCHEM_TEST_SCHEDULE WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_P AND DS_VERSION IS NOT NULL AND DS_VERSION NOT LIKE '0';      
      
      SELECT NM_USUARIO_NREC 
          INTO NM_USUARIO_NREC_OLD_W
      FROM SCHEM_TEST_SCHEDULE WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_P;
      
      IF (QTD_VERSION_W <> 0) THEN
        SELECT DS_VERSION 
          INTO DS_VERSION_W
        FROM SCHEM_TEST_SCHEDULE WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_P AND DS_VERSION IS NOT NULL AND DS_VERSION NOT LIKE '0';
      
         UPDATE SCHEM_TEST_SCHEDULE SET DS_VERSION = DS_VERSION_W WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_OLD_W;
      END IF;      
     
      COMMIT;
      
      IF (NM_USUARIO_NREC_OLD_W LIKE 'Auto Robot') THEN
        SELECT DT_EXECUTION
           INTO DT_EXECUTION_W
        FROM SCHEM_TEST_SCHEDULE 
        WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_OLD_W;
        
        SELECT DT_EXECUTION_W+obter_minutos_intervalo(DT_EXECUTION,DT_ATUALIZACAO_NREC,0)/(24*60)
           INTO DT_EXECUTION_SOMA
        FROM SCHEM_TEST_SCHEDULE 
        WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_P;
        
        IF (DT_EXECUTION_W IS NULL) THEN
          SELECT DT_EXECUTION
               INTO DT_EXECUTION_SOMA
          FROM SCHEM_TEST_SCHEDULE 
          WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_P;
        END IF;
        
        UPDATE SCHEM_TEST_SCHEDULE SET DT_EXECUTION = DT_EXECUTION_SOMA WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_OLD_W;
        COMMIT;
      END IF;
      
      OPEN C01;
      LOOP
      FETCH C01 INTO	
        NR_SEQ_SCRIPT_W;
      EXIT WHEN C01%NOTFOUND;
      BEGIN	
          SELECT COUNT(log1.NR_SEQUENCIA) NR_SEQUENCIA
              INTO QTD4
          FROM SCHEM_TEST_LOG log1 
          WHERE log1.NR_SEQ_SCHEDULE = NR_SEQ_SCHEDULE_OLD_W
          AND log1.NR_SEQ_SCRIPT = NR_SEQ_SCRIPT_W 
          AND log1.IE_STATUS = '2';
          
          IF (QTD4 <> 0) THEN
              SELECT MAX(log1.NR_SEQUENCIA) NR_SEQUENCIA, log1.NM_SCRIPT NM_SCRIPT 
                  INTO NR_SEQUENCIA_W, NM_SCRIPT_W
              FROM SCHEM_TEST_LOG log1 
              WHERE log1.NR_SEQ_SCHEDULE = NR_SEQ_SCHEDULE_OLD_W
              AND log1.NR_SEQ_SCRIPT = NR_SEQ_SCRIPT_W 
              AND log1.IE_STATUS = '2'
              GROUP BY log1.NM_SCRIPT;
              
              UPDATE SCHEM_TEST_LOG SET IE_STATUS = '4' WHERE NR_SEQUENCIA = NR_SEQUENCIA_W;
			  COMMIT;
              
			  UPDATE SCHEM_TEST_SCHEDULE_ATTACH SET DS_ARQUIVO = REPLACE(DS_ARQUIVO, NR_SEQ_SCHEDULE_OLD_W, NR_SEQ_SCHEDULE_P)
				WHERE NR_SEQ_SCHEDULE = NR_SEQ_SCHEDULE_OLD_W AND NR_SEQ_SCRIPT = NR_SEQ_SCRIPT_W AND DS_OBSERVACAO LIKE '%.html';
              COMMIT;	  
			  
              DELETE FROM SCHEM_TEST_SCHEDULE_ATTACH WHERE NR_SEQ_SCHEDULE = NR_SEQ_SCHEDULE_OLD_W AND NR_SEQ_SCRIPT = NR_SEQ_SCRIPT_W AND DS_OBSERVACAO LIKE '%.png';
              COMMIT;
          END IF;
      END;
      END LOOP;
      CLOSE C01; 
      
      SCHEMATIC4TEST_SETSCHEDSTATUS(NR_SEQ_SCHEDULE_OLD_W);
      SCHEMATIC4TEST_ERASE_DUPLI(NR_SEQ_SCHEDULE_OLD_W,'1');
      SCHEMATIC4TEST_ERASE_DUPLI(NR_SEQ_SCHEDULE_OLD_W,'2');
      SCHEMATIC4TEST_ERASE_DUPLI(NR_SEQ_SCHEDULE_OLD_W,'4');
      
      UPDATE SCHEM_TEST_SCHEDULE SET IE_JOBS = '1' WHERE NR_SEQUENCIA = NR_SEQ_SCHEDULE_P;
      COMMIT;
      
	   UPDATE SCHEM_TEST_SUITE SET IE_JOBS = '1' WHERE NR_SEQUENCIA = NR_SEQ_SUITE_P;
      COMMIT;      
    END IF;		
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Erro: Data not found');
END SCHEMATIC4TEST_UPDATE_FAIL;  
/
