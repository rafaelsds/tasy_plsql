create or replace
procedure gerar_equipes_proj_migr_funcao (
		nr_seq_projeto_p	number,
		cd_gerente_p		varchar2,
		nm_usuario_p		varchar2) is
		
begin
if	(nr_seq_projeto_p is not null) and
	(cd_gerente_p is not null) and
	(nm_usuario_p is not null) then
	begin
	gerar_equipe_analise_proj_migr (
		nr_seq_projeto_p,
		cd_gerente_p,
		nm_usuario_p);
		
	gerar_equipe_desenv_proj_migr (
		nr_seq_projeto_p,
		cd_gerente_p,
		nm_usuario_p);		
	end;
end if;
commit;
end gerar_equipes_proj_migr_funcao;
/