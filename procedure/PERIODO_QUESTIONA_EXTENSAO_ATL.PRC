create or replace trigger periodo_questiona_extensao_atl
before insert or update on periodo_questiona_extensao
for each row

declare

begin
begin
	if (:new.hr_fim is not null) and ((:new.hr_fim <> :old.hr_fim) or (:old.dt_fim is null)) then
		:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_fim,'dd/mm/yyyy hh24:mi');
	end if;	
	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/

