create or replace
procedure ajustar_horario_agenda_cir(	nr_seq_agenda_p		number,
					ie_status_agenda_p	varchar2) 
					is
					
hr_inicio_w	date;
cd_agenda_w	number(10);
qt_item_w	number(10);
ie_encaixe_w	varchar2(2);
					
begin

select	max(hr_inicio),
	max(cd_agenda)
into	hr_inicio_w,
	cd_agenda_w
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p;	

select 	count(*)
into	qt_item_w
from	agenda_paciente
where	cd_agenda 		= cd_agenda_w
and	hr_inicio 		= hr_inicio_w
and	ie_status_agenda	= ie_status_agenda_p
and	Obter_Se_Agenda_Encaixe(nr_sequencia) <> 'S'
and	ie_status_agenda	not in ('C','L')
and	nr_sequencia		<> nr_seq_agenda_p;

if	(qt_item_w > 0) then
	ie_encaixe_w := Obter_Se_Agenda_Encaixe(nr_seq_agenda_p);
	
	if	(ie_encaixe_w = 'S') then
		update	agenda_paciente
		set	hr_inicio	 = hr_inicio + 1/86400
		where	nr_sequencia	 = nr_seq_agenda_p;
		commit;
	end if;	
end if;


end ajustar_horario_agenda_cir;
/