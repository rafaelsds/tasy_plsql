create or replace
procedure pls_obter_grupo_fechar_analise
			(nr_seq_analise_p		Number,
			 cd_estabelecimento_p		Number,
			 nr_seq_regra_p		out	Number,
			 nr_seq_grupo_p		out	Number) is 
			 
nr_seq_grupo_w		Number(10);
nr_seq_regra_w 		Number(10);
			 
Cursor C01 is
	select	nr_sequencia,
		nr_seq_grupo
	from	pls_fim_analise_conta
	where	cd_estabelecimento = cd_estabelecimento_p	
	and	ie_situacao = 'A'
	order by nr_sequencia;

begin

open C01;
loop
fetch C01 into
	nr_seq_regra_w,
	nr_seq_grupo_w;
exit when C01%notfound;	
end loop;
close C01;

nr_seq_regra_p := nr_seq_regra_w;
nr_seq_grupo_p := nr_seq_grupo_w;

end pls_obter_grupo_fechar_analise;
/