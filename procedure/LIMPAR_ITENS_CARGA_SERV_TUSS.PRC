create or replace
procedure Limpar_Itens_Carga_Serv_Tuss(	nr_seq_carga_tuss_p	Number,
					nm_usuario_p		Varchar2) is 

nr_sequencia_w	number(10,0);	
ind_w		number(10,0);				
dt_liberacao_w	date;
					
Cursor C01 is
	select	nr_sequencia
	from	tuss_servico_item
	where	nr_seq_carga_tuss = nr_seq_carga_tuss_p	
	order by 1;	
					
begin

ind_w:= 0;

select 	max(dt_liberacao)
into	dt_liberacao_w
from 	tuss_servico
where 	nr_sequencia = nr_seq_carga_tuss_p;

if	(dt_liberacao_w is not null) then
	-- N�o � poss�vel excluir itens de uma carga tuss j� liberada!
	Wheb_mensagem_pck.exibir_mensagem_abort(244309);
end if;

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	begin
	delete from tuss_servico_item
	where	nr_sequencia = nr_sequencia_w;
	exception
		when others then
		ind_w:= ind_w;
	end;
	
	ind_w:= ind_w + 1;
	
	if	(ind_w > 100) then
		ind_w:= 0;
		commit;
	end if;
	
	end;
end loop;
close C01;

commit;

end Limpar_Itens_Carga_Serv_Tuss;
/
