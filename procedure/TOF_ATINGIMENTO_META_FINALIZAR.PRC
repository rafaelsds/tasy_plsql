create or replace
procedure tof_atingimento_meta_finalizar(	cd_estabelecimento_p	varchar2,
					nm_usuario_p		varchar2,
					dt_evento_p		date,
					nr_atendimento_p	number) is 


contador_w			number(10);
contador_ww			number(10);
contador_www			number(10);
contador_wwww			number(10);										
contador_wwwww			number(10);										

begin


select	count(*)
into	contador_w
from	tof_meta_atend a,
	tof_meta m
where	m.nr_sequencia = a.nr_seq_meta
and	nr_atendimento = nr_atendimento_p
and	ie_regra = 'E'
and	nvl(m.ie_situacao,'A') = 'A'
and	a.dt_finalizacao is null
and a.dt_liberacao is not null
and a.dt_inativacao is null;

select	count(*)
into	contador_ww
from	tof_meta_atend a,
	tof_meta m
where	m.nr_sequencia = a.nr_seq_meta
and	nr_atendimento = nr_atendimento_p
and	ie_regra = 'TEV'
and	nvl(m.ie_situacao,'A') = 'A'
and	a.dt_finalizacao is null
and a.dt_liberacao is not null
and a.dt_inativacao is null;

select	count(*)
into	contador_www
from	tof_meta_atend a,
	tof_meta m
where	m.nr_sequencia = a.nr_seq_meta
and	nr_atendimento = nr_atendimento_p
and	ie_regra = 'I'
and	nvl(m.ie_situacao,'A') = 'A'
and	a.dt_finalizacao is null
and a.dt_liberacao is not null
and a.dt_inativacao is null;

select	count(*)
into	contador_wwww
from	tof_meta_atend a,
	tof_meta m
where	m.nr_sequencia = a.nr_seq_meta
and	nr_atendimento = nr_atendimento_p
and	ie_regra = 'ESC'
and	nvl(m.ie_situacao,'A') = 'A'
and	a.dt_finalizacao is null
and a.dt_liberacao is not null
and a.dt_inativacao is null;

select	count(*)
into	contador_wwwww
from	tof_meta_atend a,
	tof_meta m
where	m.nr_sequencia = a.nr_seq_meta
and	nr_atendimento = nr_atendimento_p
and	ie_regra = 'P'
and	nvl(m.ie_situacao,'A') = 'A'
and	a.dt_finalizacao is null
and a.dt_liberacao is not null
and a.dt_inativacao is null;


if	(contador_w > 0) then
	begin
		tof_verificar_meta_eventos(nr_atendimento_p, nm_usuario_p, cd_estabelecimento_p, dt_evento_p, 'S');
	end;
end if;

if	(contador_ww > 0) then
	begin
		tof_verificar_meta_tev(nr_atendimento_p, nm_usuario_p, dt_evento_p, 'S');
	end;
end if;


if	(contador_www > 0) then
	begin
		tof_verificar_meta_job_interv(nr_atendimento_p, nm_usuario_p, cd_estabelecimento_p, dt_evento_p, 'S');
	end;
end if;

if	(contador_wwww > 0) then
	begin
		tof_verificar_meta_escalas(nr_atendimento_p, nm_usuario_p, cd_estabelecimento_p, dt_evento_p, 'S');
	end;
end if;

if (contador_wwwww > 0) then
	begin
		tof_verificar_meta_alta(nr_atendimento_p, nm_usuario_p, cd_estabelecimento_p, dt_evento_p, 'S');
	end;
end if;

end tof_atingimento_meta_finalizar;
/
