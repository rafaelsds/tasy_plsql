create or replace
procedure liberar_registro_sae_aux(
					nr_sequencia_p	number,				
					nm_usuario_p	varchar2) is
					
begin

if	(nr_sequencia_p is not null) then
	begin
	
	update 	pe_prescricao
	set 	dt_liberacao_aux = sysdate,
		    nm_usuario 	= nm_usuario_p
	where	nr_sequencia 	= nr_sequencia_p;
	
	end;
end if;

end liberar_registro_sae_aux;
/