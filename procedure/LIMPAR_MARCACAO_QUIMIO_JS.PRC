create or replace
procedure limpar_marcacao_quimio_js(
		nr_seq_pend_agenda_p	number,
		nm_usuario_p		varchar2) is 

qt_registro_w	number(10);
begin
if	(nr_seq_pend_agenda_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	count(*)
	into	qt_registro_w
	from	agenda_quimio_marcacao
	where	nr_seq_pend_agenda = nr_seq_pend_agenda_p
	and	nvl(ie_gerado, 'N') = 'N'
	and	nm_usuario = nm_usuario_p;

	if (qt_registro_w > 0) then
		begin
		qt_limpar_dados_marcacao(nm_usuario_p);
		end;
	end if;
	end;
end if;
commit;
end limpar_marcacao_quimio_js;
/