create or replace
procedure ops_insere_dic_objeto_fil_gru(	cd_exp_titulo_p			varchar2,
						nm_titulo_p			varchar2,
						nr_seq_obj_filtro_p		varchar2,
						nm_usuario_p			varchar2,
						nr_seq_dic_obj_p	out	varchar2) is 

begin

insert into dic_objeto_filtro_grupo (
	nr_sequencia, dt_atualizacao, nm_usuario,
	dt_atualizacao_nrec, nm_usuario_nrec, cd_exp_titulo,
	nm_titulo, ie_apresenta_titulo, nr_seq_obj_filtro,
	ie_apresentacao_inicial )
values (
	dic_objeto_filtro_grupo_seq.nextval, sysdate, nm_usuario_p,
	sysdate, nm_usuario_p, cd_exp_titulo_p,
	nm_titulo_p, '', nr_seq_obj_filtro_p,
	'F'
	) returning nr_sequencia into nr_seq_dic_obj_p;

commit;

end ops_insere_dic_objeto_fil_gru;
/