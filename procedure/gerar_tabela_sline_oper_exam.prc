create or replace procedure gerar_tabela_sline_oper_exam(nr_seq_laudo_oper_p number
                                                        ,ds_oper_exam_p varchar
                                                        ,nm_usuario_p varchar) as
nr_seq_xml_laudo_oper_exam_w number(10);
begin

select SLINE_XML_LAUDO_OPER_EXAM_seq.nextval
  into nr_seq_xml_laudo_oper_exam_w
  from dual;

insert
into sline_xml_laudo_oper_exam(nr_sequencia
                              ,dt_atualizacao
                              ,nm_usuario
                              ,dt_atualizacao_nrec
                              ,nm_usuario_nrec
                              ,nr_seq_laudo_oper
                              ,ds_oper_exam)
                     values   (nr_seq_xml_laudo_oper_exam_w
                              ,sysdate
                              ,nm_usuario_p
                              ,sysdate
                              ,nm_usuario_p
                              ,nr_seq_laudo_oper_p
                              ,ds_oper_exam_p);
commit;
end gerar_tabela_sline_oper_exam;
/
