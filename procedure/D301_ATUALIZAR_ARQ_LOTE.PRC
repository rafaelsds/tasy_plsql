create or replace
procedure D301_ATUALIZAR_ARQ_LOTE
			(nr_seq_arquivo_p	number,
			ds_arquivo_p		varchar2,
			nm_usuario_p		varchar2) is
			
begin

update	D301_ARQUIVO_ENVIO
set	DS_ARQUIVO	= ds_arquivo_p,
	dt_atualizacao	= sysdate,
	DT_GERACAO	= SYSDATE,
	nm_usuario	= nm_usuario_p,
	ie_status_envio	= 'E'
where	nr_sequencia	= nr_seq_arquivo_p;

commit;

end D301_ATUALIZAR_ARQ_LOTE;
/