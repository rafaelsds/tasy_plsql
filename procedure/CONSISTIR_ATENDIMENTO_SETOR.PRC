create or replace
procedure consistir_atendimento_setor(
		nr_atendimento_p			number,
		cd_setor_atendimento_p		number,
		dt_entrada_p			date,
		ie_tipo_visita_p			varchar2,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		ds_pergunta_p		out	varchar2,
		ds_erro_p			out	varchar2) is 

ie_visita_fora_horario_w	varchar2(1);
ie_visita_fora_hor_w	varchar2(5);

begin

if	(cd_setor_atendimento_p is not null) then
	begin
	/* Controle de Visitas - Parametro [54] - Caso esteja fora do hor�rio de visita do setor, permite visita */
	obter_param_usuario(8014, 54, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_visita_fora_horario_w);

	if	(ie_visita_fora_horario_w <> 'S') then
		begin
		verifica_hor_visita_setor(
			dt_entrada_p,
			cd_setor_atendimento_p,
			nr_atendimento_p,
			ie_tipo_visita_p,
			ie_visita_fora_hor_w);

		if	(ie_visita_fora_horario_w = 'N') then
			begin
			if	(ie_visita_fora_hor_w = 'S') then
				ds_erro_p	:= substr(obter_texto_tasy(52284, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			elsif	(ie_visita_fora_hor_w = 'LH') then
				ds_erro_p	:= substr(obter_texto_tasy(60153, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			end if;
			end;
		elsif	(ie_visita_fora_horario_w = 'Q') then
			begin
			if	(ie_visita_fora_hor_w = 'S') then
				ds_pergunta_p	:= substr(obter_texto_tasy(60156, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			elsif	(ie_visita_fora_hor_w = 'LH') then
				ds_pergunta_p	:= substr(obter_texto_tasy(60157, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;

end consistir_atendimento_setor;
/