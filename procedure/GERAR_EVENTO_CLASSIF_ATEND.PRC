create or replace 
procedure gerar_evento_classif_atend(	nr_atendimento_p	number,
				        cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is
nr_seq_evento_w		number(10);
qt_reg_w		number(10);
qt_idade_w		number(10);
nr_seq_classif_w	number(10);
		

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_evento_disp = 'C'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	(((nr_seq_classif is not null) and (nvl(nr_seq_classif, nr_seq_classif_w) = nr_seq_classif_w)) or
		 ((nr_seq_classif is null) and (obter_classif_regra_pf(nr_sequencia,nvl(nr_seq_classif_w,0)) = 'S')))
	and	nvl(ie_situacao,'A') = 'A';

begin

select	count(*)
into	qt_reg_w
from	pessoa_classif a,
	atendimento_paciente b
where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
and	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	nvl(dt_inicio_vigencia,b.dt_entrada) <= b.dt_entrada
and	nvl(dt_final_vigencia,b.dt_entrada) >= b.dt_entrada
and	nr_atendimento = nr_atendimento_p;

qt_idade_w	  := nvl(obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A'),0);
nr_seq_classif_w  := obter_classificacao_pf(cd_pessoa_fisica_p);

if	(qt_reg_w >= 1) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_fisica_p,null,nm_usuario_p,null);
		end;
	end loop;
	close C01;
end if;

end gerar_evento_classif_atend;
/
