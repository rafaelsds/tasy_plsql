create or replace
procedure atualiza_trans_externa(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		number,
			nr_seq_vaga_p		number) is 

begin

if	(nvl(nr_sequencia_p,0) > 0) and 
	(nvl(nr_seq_vaga_p,0) > 0) then
	
	update	solic_transf_externa
	set	dt_geracao_vaga = sysdate,
		nr_seq_vaga	= nr_seq_vaga_p
	where	nr_sequencia = nr_sequencia_p;
end if;
	
commit;

end atualiza_trans_externa;
/