create or replace
procedure baixar_devolucao_palmweb(	
				nr_devolucao_p		number,
				nr_atendimento_p		number,
				cd_local_estoque_p	number,
				cd_setor_atendimento_p	number,
				cd_material_p		number,
				cd_motivo_devolucao_p	varchar2,
				cd_unidade_medida_p	varchar2,
				nr_seq_lote_fornec_p	number,
				qt_material_p		number,
				ie_gerar_p		varchar2,
				nm_usuario_p		varchar2) is 

nr_sequencia_w		item_devolucao_material_pac.nr_sequencia%type;
qt_existe_w		number(10);
nr_seq_lote_fornec_w	material_lote_fornec.nr_sequencia%type;
cd_material_w		material.cd_material%type;
cd_unidade_medida_w	unidade_medida.cd_unidade_medida%type;
qt_material_tot_w		number(15,4);
qt_pendente_w		number(15,4);
qt_material_w		number(15,4);
nr_sequencia_prescr_w	prescr_material.nr_sequencia%type;
nr_prescricao_w		prescr_medica.nr_prescricao%type;
qt_mat_devol_w		w_devolucao_pda.qt_material%type;
nr_seq_atendimento_w	material_atend_paciente.nr_sequencia%type;
cd_local_estoque_w		number(10);
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
ie_local_user_logado_w 		varchar2(1);
cd_perfil_ativo_w			number(5);

cursor C01 is
	select	a.nr_prescricao,
		a.nr_sequencia_prescricao,
		sum(a.qt_material),
		max(a.nr_sequencia)
	from	tipo_baixa_prescricao c,
		material b,
		material_em_devolucao_v a
	where	a.cd_material = b.cd_material
	and	a.nr_atendimento = nr_atendimento_p
	and	a.cd_setor_atendimento = nvl(cd_setor_atendimento_p, a.cd_setor_atendimento)
	and	a.cd_motivo_baixa = c.cd_tipo_baixa(+)
	and	a.ie_prescr_dev = c.ie_prescricao_devolucao(+)
	and	((qt_material < 0) or (a.cd_local_estoque is not null) or ((c.ie_atualiza_estoque = 'N') and (c.ie_conta_paciente = 'S')))
	and	a.cd_material = cd_material_w
	and	((a.nr_seq_lote_fornec = nr_seq_lote_fornec_w) or (nr_seq_lote_fornec_w is null))
	group by a.nr_prescricao,
		a.nr_sequencia_prescricao
	having sum(a.qt_material) > 0;
	
cursor C02 is
	select	cd_material,
		cd_unidade_medida,
		sum(qt_material),
		nr_seq_lote_fornec
	from	w_devolucao_pda
	where	nr_devolucao = nr_devolucao_p
	group by	cd_material,
		cd_unidade_medida,
		nr_seq_lote_fornec
	order by cd_material;

begin

obter_param_usuario(88, 321, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_local_user_logado_w);

cd_local_estoque_w := cd_local_estoque_p;

if	(nvl(ie_local_user_logado_w,'N') = 'S') then

	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	usuario
	where	nm_usuario = nm_usuario_p;

	select 	nvl(cd_local_estoque,cd_local_estoque_p)
	into	cd_local_estoque_w
	from	setor_atendimento
	where 	cd_setor_atendimento = cd_setor_atendimento_w;

end if;

update	devolucao_material_pac 
set	cd_setor_atendimento = cd_setor_atendimento_p 
where	nr_devolucao = nr_devolucao_p 
and	nr_atendimento = nr_atendimento_p;

if	(ie_gerar_p = 'S') and
	(nr_atendimento_p is not null) then
	
	if	(nr_seq_lote_fornec_p is not null) then

		select	nvl(sum(a.qt_material),0)
		into	qt_material_w
		from	tipo_baixa_prescricao c,
			material b,
			material_em_devolucao_v a
		where	a.cd_material = b.cd_material
		and	a.nr_atendimento = nr_atendimento_p
		and	a.cd_setor_atendimento = nvl(cd_setor_atendimento_p, a.cd_setor_atendimento)
		and	a.cd_motivo_baixa = c.cd_tipo_baixa(+)
		and	a.ie_prescr_dev = c.ie_prescricao_devolucao(+)
		and	((qt_material < 0) or (a.cd_local_estoque is not null) or ((c.ie_atualiza_estoque = 'N') and (c.ie_conta_paciente = 'S')))
		and	a.cd_material = cd_material_p
		and	a.nr_seq_lote_fornec = nr_seq_lote_fornec_p;
		
		select	nvl(sum(qt_material), 0)
		into	qt_mat_devol_w
		from	w_devolucao_pda
		where	nr_devolucao = nr_devolucao_p
		and	cd_material = cd_material_p
		and	nr_seq_lote_fornec = nr_seq_lote_fornec_p;
		
		if	(qt_material_w > 0) and	
			(qt_material_w >= qt_mat_devol_w) then
			nr_seq_lote_fornec_w := nr_seq_lote_fornec_p;
		end if;
	end if;
	
	insert into w_devolucao_pda(
		nr_devolucao,
		cd_material,
		qt_material,
		cd_unidade_medida,
		nr_seq_lote_fornec)
	values(	nr_devolucao_p,
		cd_material_p,
		qt_material_p,
		cd_unidade_medida_p,
		nr_seq_lote_fornec_w);

elsif	(nr_atendimento_p is not null) and
	(nr_devolucao_p	is not null) then
	
	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		cd_unidade_medida_w,
		qt_material_tot_w,
		nr_seq_lote_fornec_w;
	exit when C02%notfound;
		begin
		qt_pendente_w := null;
		
		open C01;
		loop
		fetch C01 into	
			nr_prescricao_w,
			nr_sequencia_prescr_w,
			qt_material_w,
			nr_seq_atendimento_w;
		exit when C01%notfound;
			begin
			
			if	(nvl(qt_pendente_w, qt_material_tot_w) > qt_material_w) then

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	item_devolucao_material_pac
				where	nr_devolucao = nr_devolucao_p;

				insert into item_devolucao_material_pac(
					nr_devolucao,
					nr_sequencia,
					cd_local_estoque,
					cd_material,
					cd_motivo_devolucao,
					cd_unidade_medida,
					dt_atendimento,
					dt_atualizacao,
					nm_usuario,
					nm_usuario_devol,
					qt_material,
					nr_prescricao,
					nr_sequencia_prescricao,
					nr_seq_lote_fornec,
					nr_seq_atendimento)
				values(	nr_devolucao_p,
					nr_sequencia_w,
					cd_local_estoque_w,
					cd_material_w,
					cd_motivo_devolucao_p,
					cd_unidade_medida_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					qt_material_w,
					nr_prescricao_w,
					nr_sequencia_prescr_w,
					nr_seq_lote_fornec_w,
					nr_seq_atendimento_w);
				commit;

				qt_pendente_w := abs(nvl(qt_pendente_w, qt_material_tot_w) - qt_material_w);
			
			else
			
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	item_devolucao_material_pac
				where	nr_devolucao = nr_devolucao_p;

				insert into item_devolucao_material_pac(
					nr_devolucao,
					nr_sequencia,
					cd_local_estoque,
					cd_material,
					cd_motivo_devolucao,
					cd_unidade_medida,
					dt_atendimento,
					dt_atualizacao,
					nm_usuario,
					nm_usuario_devol,
					qt_material,
					nr_prescricao,
					nr_sequencia_prescricao,
					nr_seq_lote_fornec,
					nr_seq_atendimento)
				values(	nr_devolucao_p,
					nr_sequencia_w,
					cd_local_estoque_w,
					cd_material_w,
					cd_motivo_devolucao_p,
					cd_unidade_medida_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nvl(qt_pendente_w, qt_material_tot_w),
					nr_prescricao_w,
					nr_sequencia_prescr_w,
					nr_seq_lote_fornec_w,
					nr_seq_atendimento_w);
				commit;
				
				qt_pendente_w := 0;

			end if;
			
			if	(qt_pendente_w = 0) then
				exit;
			end if;

			end;
		end loop;
		close C01;
		end;
	end loop;
	close C02;
	
	delete from w_devolucao_pda 
	where nr_devolucao = nr_devolucao_p;
	
end if;

commit;

end baixar_devolucao_palmweb;
/