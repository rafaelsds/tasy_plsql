create or replace
procedure vipe_obter_horarios_jejum (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_exibe_sem_lib_farm_p		varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is
					
dt_horario_w	date;
					
cursor c01 is
select	c.dt_inicio
from	rep_jejum c,
	prescr_medica a
where	c.nr_prescricao = a.nr_prescricao
and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	((obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S') or
	((ie_exibe_sem_lib_farm_p = 'S') and
	(nvl(a.IE_PRESCR_NUTRICAO, 'N') = 'S')))
and	c.dt_inicio between dt_inicial_horarios_p and dt_final_horarios_p
and	((ie_exibir_suspensos_p = 'S') or (nvl(c.ie_suspenso,'N') = 'N'))
and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = Obter_Unidade_Atendimento(nr_atendimento_p, 'IA', 'CS'))))
group by
	c.dt_inicio;
					
begin
open c01;
loop
fetch c01 into dt_horario_w;
exit when c01%notfound;
	begin
	insert into w_vipe_horarios_t (
		nm_usuario,
		dt_horario)
	values (
		nm_usuario_p,
		dt_horario_w);
	end;
end loop;
close c01;
end vipe_obter_horarios_jejum;
/
