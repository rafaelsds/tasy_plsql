create or replace
procedure alterar_TEV_mobilidade_red(	nr_sequencia_p	number,
										ie_opcao_p	Varchar2 ) is
				
ie_opcao_w	Varchar2(2);

begin

ie_opcao_w := substr(ie_opcao_p,1,1);

if	(ie_opcao_w is not null) then

	update  escala_tev
	set		IE_MOB_RED_FINALIZADA = ie_opcao_w
	where	nr_sequencia = nr_sequencia_p;

	commit; 

end if;

end alterar_TEV_mobilidade_red;
/