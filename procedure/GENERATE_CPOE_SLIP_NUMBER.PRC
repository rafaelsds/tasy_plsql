create or replace
procedure GENERATE_CPOE_SLIP_NUMBER(
					nr_atendimento_p number,
					ie_tipo_atendimento_p number,
					cd_pessoa_fisica_p varchar2,
                    nr_seq_agenda_consulta_p number,
                    cd_estabelecimento_p number,
                    nm_usuario_p varchar2) as

si_event_type_w cpoe_rule_slip_tipo_ped.si_event_type%type  := null;
cd_pessoa_fisica_w pessoa_fisica.cd_pessoa_fisica%type  := null;
nr_seq_cpoe_rule_slip_w cpoe_rule_slip_number.nr_sequencia%type := null;
cd_slip_number_w cpoe_order_type_identify.cd_slip_number%type := null;
new_day_w varchar(1)  := 'N';
nr_current_seq_w    cpoe_rule_slip_number.nr_current_seq%type := null;
cd_estabelecimento_w  estabelecimento.cd_estabelecimento%type;
nr_seq_pac_reab_w	rp_paciente_reabilitacao.nr_sequencia%type;
nr_seq_agenda_consulta_w	agenda_consulta.nr_sequencia%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type	:= null;
nr_order_serial_w int_serial_number.nr_serial%type;

cursor C_rule is
select	a.nr_sequencia
from	cpoe_rule_slip_tipo_ped b,
		cpoe_rule_slip_number a
where	a.nr_sequencia = b.nr_seq_cpoe_rule_slip(+)
AND		sysdate Between pkg_date_utils.start_of(a.dt_vigencia_ini,'DAY') and NVL(pkg_date_utils.end_of(a.Dt_vigencia_fim,'DAY'), sysdate)
AND		(b.ie_tipo_atendimento = ie_tipo_atendimento_w or ie_tipo_atendimento_w is null)
and		b.si_event_type = si_event_type_w
order by
		nvl(b.ie_tipo_atendimento,0);
          
cursor C_Update ( nr_sequencia_pc  number) is
select	nr_sequencia,
		CD_PREFIXO_SLIP_NUMBER,
		ds_timestamp_format,
		nr_current_seq,
		qt_maximo,
		si_seq_restart_daily,
		dt_last_seq_reset,
		ds_sufixo
from	cpoe_rule_slip_number
where	nr_sequencia = nr_sequencia_pc

for update of dt_last_seq_reset, nr_current_seq;

begin
  
if  (nr_atendimento_p > 0) then
	si_event_type_w := 'AT';

	ie_tipo_atendimento_w := ie_tipo_atendimento_p;
	cd_pessoa_fisica_w := cd_pessoa_fisica_p;
elsif	(nr_seq_agenda_consulta_p > 0) then
	si_event_type_w := 'AC';

	select	a.cd_pessoa_fisica,
			a.nr_seq_pac_reab
	INTO	cd_pessoa_fisica_w,
			nr_seq_pac_reab_w
	from	agenda_consulta a
	where	a.nr_sequencia = nr_seq_agenda_consulta_p;
	
	IF	(nr_seq_pac_reab_w is not null) THEN
		nr_seq_agenda_consulta_w	:= null;
	ELSE
		nr_seq_agenda_consulta_w	:= nr_seq_agenda_consulta_p;
	end if;
end if;

cd_estabelecimento_w  := nvl(cd_estabelecimento_p,wheb_usuario_pck.get_cd_estabelecimento);

if  (si_event_type_w is not null) and
	(cd_pessoa_fisica_w is not null) then

	for r_c_rule in c_rule loop
		nr_seq_cpoe_rule_slip_w := r_c_rule.nr_sequencia;
	end loop;

	if	nr_seq_cpoe_rule_slip_w is not null then 
		for rLin in C_Update ( nr_seq_cpoe_rule_slip_w ) loop
			new_day_w := 'N';
			
			if  (rLin.si_seq_restart_daily = 'S') and
				( (rLin.dt_last_seq_reset < trunc(sysdate,'dd')) or
				(rLin.dt_last_seq_reset is null) ) then
				new_day_w := 'S';
			end if;   
			
			if new_day_w = 'S' then
				nr_current_seq_w  := 1;
			else    
				nr_current_seq_w  := rLin.nr_current_seq + 1;
			end if;

			cd_slip_number_w := rLin.CD_PREFIXO_SLIP_NUMBER || TO_CHAR(SYSDATE, rLin.ds_timestamp_format)
				   || LPAD(nr_current_seq_w,LENGTH(rLin.qt_maximo), '0')
				   || rLin.ds_sufixo;

			if (new_day_w = 'S') then
				update	cpoe_rule_slip_number
				set		nr_current_seq = 0,
						dt_last_seq_reset = sysdate
				where current of C_Update;
			end if;

			update	cpoe_rule_slip_number
			set		nr_current_seq = nr_current_seq + 1
			where current of C_Update;
		end loop;
	end if;

  generate_int_serial_number(cd_pessoa_fisica_w, 'ACC_ORDER_SEQ', cd_estabelecimento_w, nm_usuario_p, nr_order_serial_w, 949);
	
  if  (cd_slip_number_w is not null) then

		insert into CPOE_ORDER_TYPE_IDENTIFY	(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			cd_slip_number,
			nr_atendimento,
			nr_seq_agenda_consulta,
			nr_seq_rp_reabilitacao,
			cd_pessoa_fisica,
			ie_situacao,
			nr_order_patient_seq)
			values	(
			cpoe_order_type_identify_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_w,
			cd_slip_number_w,
			nr_atendimento_p,
			nr_seq_agenda_consulta_w,
			nr_seq_pac_reab_w,
			cd_pessoa_fisica_w,
			'A',
			nr_order_serial_w);
	end if;
end if;

end GENERATE_CPOE_SLIP_NUMBER;
/
