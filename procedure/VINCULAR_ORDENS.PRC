create or replace
procedure vincular_ordens(
			nr_ordem_agrup_p	number,
			nr_ordem_compra_p	number) is 

begin

update	ordem_compra 
set 	nr_ordem_agrup	= nr_ordem_agrup_p
where 	nr_ordem_compra = nr_ordem_compra_p;

commit;

end vincular_ordens;
/