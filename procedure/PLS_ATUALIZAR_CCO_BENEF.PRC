create or replace
procedure pls_atualizar_cco_benef
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2) is

nr_seq_segurado_w		number(10);
nr_cco_w			number(10);
ie_digito_cco_w			number(2);
cd_usuario_plano_w		varchar2(30);
nr_seq_devolucao_w		number(10);
cd_cco_w			pls_segurado.cd_cco%type;

Cursor C01 is
	select	nr_sequencia,
		nr_cco,
		ie_digito_cco,
		cd_usuario_plano
	from	sib_devolucao_cco
	where	nr_seq_lote	= nr_seq_lote_p
	and	ie_xml = 'S';

begin

open C01;
loop
fetch C01 into
	nr_seq_devolucao_w,
	nr_cco_w,
	ie_digito_cco_w,
	cd_usuario_plano_w;
exit when C01%notfound;
	begin
	begin
	select	a.nr_seq_segurado
	into	nr_seq_segurado_w
	from	pls_segurado_carteira a
	where	a.cd_usuario_plano	= cd_usuario_plano_w;
	exception
	when others then
		nr_seq_segurado_w	:= 0;
	end;
	
	/* OS 490019 */
	if	(nr_seq_segurado_w = 0) then
		begin
		select	nr_sequencia
		into	nr_seq_segurado_w
		from	pls_segurado
		where	cd_cartao_ident_ans_sist_ant	= cd_usuario_plano_w;
		exception
		when others then
			nr_seq_segurado_w	:= 0;
		end;
	end if;
	
	if	(nr_seq_segurado_w	> 0) then
		cd_cco_w	:= lpad(nr_cco_w,10,0) || lpad(ie_digito_cco_w,2,0);
		update	pls_segurado
		set	nr_cco		= nr_cco_w,
			ie_digito_cco	= ie_digito_cco_w,
			cd_cco		= cd_cco_w
		where	nr_sequencia	= nr_seq_segurado_w;
		
		update	sib_devolucao_cco
		set	nr_seq_segurado	= nr_seq_segurado_w
		where	nr_sequencia	= nr_seq_devolucao_w;
	end if;
	end;
end loop;
close C01;

update	pls_lote_sib
set	ie_cco_atualizado	= 'S'
where	nr_sequencia		= nr_seq_lote_p;

commit;

end pls_atualizar_cco_benef;
/