create or replace
procedure impacto_documento_delete(nr_sequencia_p	number) is 

begin
if nr_sequencia_p is not null then
	update   qua_documento
	set      ds_impacto     = null
	where    nr_sequencia   = nr_sequencia_p;
end if;	

commit;

end impacto_documento_delete;
/