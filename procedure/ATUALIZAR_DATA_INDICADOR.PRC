create or replace
procedure atualizar_data_indicador(nr_seq_indicador_p	number) is 

begin

update 	INDICADOR_GESTAO 
set 	dt_alteracao = sysdate
where	nr_sequencia = nr_seq_indicador_p;

commit;

end ATUALIZAR_DATA_INDICADOR;
/