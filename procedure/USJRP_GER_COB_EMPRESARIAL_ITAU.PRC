create or replace
procedure usjrp_ger_cob_empresarial_itau
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar arquivo de remessa para o banco de Ita�
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Abrir os arquvios de remessa - na nossa base           \\192.168.0.230\UTLFILE
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
					
ds_conteudo_w			varchar2(400);
ds_conteudo_mens_w		varchar2(400);
ds_conteudo_flash_w		varchar2(400);
ds_header_w			varchar2(400);
ds_conteudo_arq_w		varchar2(400);
ds_trailer_w			varchar2(400);
ie_arquivo1_w			varchar2(1) := 'N';
ie_arquivo2_w			varchar2(1) := 'N';
qt_arquivo1_w			number(10) := 1;
qt_arquivo2_w			number(10) := 1;
qt_registro_w			number(10);
nr_seq_registro1_w		number(10) := 0;
nr_seq_registro2_w		number(10) := 0;

nm_empresa_w			varchar2(30);
dt_geracao_w			varchar2(6);	

ds_ender_mens_w			varchar2(400);
ds_ender2_mens_w		varchar2(400);					
ds_endereco_w			varchar2(255);
ds_endereco1_w			varchar2(255);
ds_endereco2_w			varchar2(255);
ds_endereco3_w			varchar2(255);
nm_pagador_w			varchar2(80);
ds_bairro_w			varchar2(40);
ds_municipio_w			varchar2(40);
nr_nosso_numero_w		varchar2(8);
cd_conta_w			varchar2(15);
nr_digito_agencia_w		varchar2(15);
ds_cgc_cpf_w			varchar2(14);
cd_carteira_w			varchar2(1);
cd_cep_w			varchar2(10);
cd_agencia_bancaria_w		varchar2(8);
ie_digito_conta_w		varchar2(2);
sg_estado_w			varchar2(2);
ie_dig_nosso_numero_w		varchar2(2);
ie_tipo_pessoa_w		varchar2(1);
vl_titulo_w			varchar2(13);
vl_titulo_flash_w		varchar2(15);
nr_documento_w			number(10);
nr_titulo_w			number(10);
dt_vencimento_w			varchar2(6);
dt_vencimento_flash_w		varchar2(10);
dt_emissao_w			varchar2(6);	
ie_tipo_inscricao_w		varchar2(2);
nr_inscricao_w			varchar2(15);
ie_instrucao1_w			varchar2(2);
ie_instrucao2_w			varchar2(2);
cd_ocorrencia_w			varchar2(2);
nr_carteira_w			varchar2(3);
ie_especie_w			varchar2(2);
ie_aceite_w			varchar2(1);
vl_juros_diario_w		varchar2(13);
dt_desconto_w			varchar2(6);
vl_desconto_w			varchar2(13);
vl_iof_w			varchar2(13);
vl_abatimento_w			varchar2(13);
nm_segurado_mens_w		varchar2(30);
vl_consulta_w			varchar2(13);
vl_exame_w			varchar2(13);
vl_total_mens_w			varchar2(13);
vl_tratamento_w 		varchar2(13);
nr_seq_pagador_w		varchar2(10);
vl_pre_estabelecido_w		varchar2(13);
ds_plano_w			varchar2(30);	
dt_vencimento_mens_w		varchar2(11);
dt_pag_prev_w			varchar2(8);
dt_30_dias_vencimento_w		varchar2(11);
ds_mensagem_1_w			varchar2(255);
ds_mensagem_2_w			varchar2(255);
nr_seq_carteira_cobr_w		number(10);
cd_banco_w			number(3);
ds_observacao_w			varchar2(4000);

vl_mensalidade_w		varchar2(20);
vl_coparticipacao_w		varchar2(20);
nr_seq_mensalidade_w		number(10);

cd_flash_w			varchar2(4);
dt_referencia_w			varchar2(8);
dt_emissao_flash_w		varchar2(8);
dt_referencia_flash_w		varchar2(11);
dt_mes_referencia_w		varchar2(11);
ie_tipo_registro_w		varchar2(1);

ds_brancos_8_w			varchar2(8);
ds_brancos_294_w		varchar2(294);
ds_brancos_4_w			varchar2(4);
ds_brancos_21_w			varchar2(21);
ds_brancos_10_w			varchar2(10);
ds_brancos_393_w		varchar2(393);
ds_brancos_9_w			varchar2(9);
ds_brancos_31_w			varchar2(31);
ds_brancos_45_w			varchar2(45);
ds_brancos_82_w			varchar2(82);
ds_brancos_30_w			varchar2(30);

i				number(2);
nr_linha_w			number(2);

nr_carteira_tit_w		varchar2(3);
nm_arquivo_w			varchar2(255);
nm_arquivo2_w			varchar2(255);
nm_arquivo_log_w       		varchar2(255);
arq_texto_w			utl_file.file_type;
arq_texto2_w			utl_file.file_type;
arq_texto_log_w			utl_file.file_type;
ds_erro_w			varchar2(255);
ds_local_w			varchar2(255);
ds_zeros_4_w			varchar2(4);
ds_zeros_5_w			varchar2(5);
ds_zeros_6_w			varchar2(6);
vl_juros_w			varchar2(13);
qt_moeda_w			varchar2(13);
nr_inscricao_empresa_w		varchar2(14);
cd_agencia_cobradora_w		varchar2(5);
nm_sacador_avalista_w		varchar2(30);
ie_digito_conta_ww		varchar2(1);
nr_seq_apres_2_w		number(10)  := 0;
ie_exibe_w           	 	varchar2(1);
nr_seq_segurado_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_mens_seg_w		number(10);
nr_seq_apres_ant_w		number(10);
ds_mensagem_compl_w		varchar2(4000);
ie_tipo_coluna_w		varchar2(10);
vl_contas_w			varchar2(20);
nr_seq_mens_ant_w		number(10);
nr_seq_mens_w			number(10);
nr_seq_plano_w			number(10);
qt_tit_aberto_w			number(10);
ds_info_1_w			varchar2(150);
ds_info_2_w			varchar2(150);
ds_info_3_w			varchar2(150);
ds_info_4_w			varchar2(150);
nr_seq_contrato_ww		number(10);
dt_reajuste_w			date;
vl_copartic_w			varchar2(14);
vl_preestabelecido_w    	varchar2(15);
vl_outros_w			varchar2(13);
nr_seq_forma_cobranca_w		number(10);
ie_msg_mens_w			varchar2(1);
nr_seq_coluna_w			number(10) := 0;
dt_pagamento_prev_w		date;
ie_tipo_reg_w			varchar2(1) := '8';
qt_beneficiarios_w		number(10);

ds_tit_depend_w			varchar2(255);
vl_mensalidade_mens_w		number(15,2);
vl_total_w			number(15,2);
ds_ordem_w			varchar2(255);
vl_ato_cooperado_w		number(15,2);
vl_ato_cooperado2_w		number(15,2);
vl_ato_auxiliar_w		number(15,2);
vl_total_benef_w		number(15,2);
vl_imposto_ir_w			number(15,2);
vl_ato_nao_cooperado_w		number(15,2);
ds_aliquota_w			varchar2(255);
ds_mensagem_nota_w		varchar2(255);
ds_mensagem_nota2_w		varchar2(255);
ds_base_inss_w			varchar2(255);
nr_seq_mensagem_w		number(10);
qt_coluna_w			number(10);
ie_inicio_w			varchar2(1);
ds_vencimento_w			varchar2(400);
ie_endereco_w			varchar2(1) := 'N';
ie_imprimir_end_w		varchar2(1) := 'S';

Cursor C01 is
	select	lpad(nvl(c.cd_agencia_bancaria,'0'),4,'0'),
		lpad(nvl(e.cd_conta,'0'),5,'0'),
		nvl(c.ie_digito_conta,'0'),
		a.nr_seq_carteira_cobr nr_carteira,
		substr(e.cd_banco,1,3) cd_banco,
		lpad(substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(d.nr_seq_pagador),'C'),1,10),10,'0') nr_documento,
		lpad(nvl(c.nr_titulo,'0'),10,'0') nr_titulo,
		replace(to_char(b.vl_titulo, 'fm00000000000.00'),'.',''),
		substr(campo_mascara_virgula(b.vl_titulo),1,15),
		to_char(nvl(b.dt_vencimento,sysdate),'ddmmyy'),
		to_char(nvl(b.dt_vencimento,sysdate),'dd/mm/yyyy'),
		to_char(nvl(b.dt_emissao,sysdate),'ddmmyy'),
		rpad(substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,255),30,' ') nm_pagador,
		rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'EN')),1,120),40,' ') ds_endereco,
		rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'B')),1,40),12,' ') ds_bairro,
		rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'CEP')),1,120),8,' ') cd_cep,
		rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'CI')),1,40),15,' ') ds_cidade,
		rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'UF')),1,2),2, ' ') ds_uf,		
		lpad(substr(c.nr_titulo,1,8),8,'0') nr_nosso_numero,
		substr(obter_nosso_numero_banco(341,c.nr_titulo),length(obter_nosso_numero_banco(341,c.nr_titulo)),1) nr_digito,
		decode(b.cd_cgc, null,'01','02') ie_tipo_inscricao,
		lpad(nvl(b.cd_cgc_cpf,'0'),15, '0') nr_inscricao,
		lpad(substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(d.nr_seq_pagador),'C'),1,1),1,'0') cd_carteira,
		'08' ie_especie,
		'A' ie_aceite,
		campo_mascara_virgula(b.vl_titulo*0.0302/30.2) vl_juros_diario,
		d.nr_sequencia,
		to_char(add_months(nvl(d.dt_referencia,sysdate),-1),'mon/yyyy'),
		to_char(nvl(b.dt_emissao,sysdate),'dd/mm/yyyy'),
		to_char(nvl(b.dt_emissao,sysdate),'mm/yyyy'),
		d.nr_seq_pagador,
		to_char(nvl(b.dt_vencimento,sysdate),'dd/mm/yyyy'),
		to_char(add_months(nvl(b.dt_pagamento_previsto ,sysdate),-1),'mon/yyyy'),
		to_char(add_months(nvl(b.dt_vencimento,sysdate),1),'dd/mm/yyyy'),
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,1),' '),80, ' ') ds_mensagem_1,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,2),' '),80, ' ') ds_mensagem_2,
		lpad(' ',30,' ') nm_sacador_avalista,
		lpad('0',13,'0') vl_desconto,
		lpad('0',13,'0') vl_iof,
		lpad('0',13,'0') vl_abatimento,
		lpad(nvl(0,elimina_caracteres_especiais(x.tx_juros)),13,'0') vl_juros_diario, -- NVL invertido
		lpad('0',13,'0') qt_moeda,
		substr(lpad(obter_cgc_estabelecimento(cd_estabelecimento_p),14,'0'),1,14) nr_inscricao_empresa,
		lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0') cd_agencia,
		d.ds_observacao,
		substr(e.cd_carteira,1,3) cd_carteira
	from    titulo_receber                  x,
		pls_mensalidade                 d,
		titulo_receber_v                b,
		titulo_receber_cobr             c,
		cobranca_escritural             a,
		banco_estabelecimento           e
	where   x.nr_seq_mensalidade    = d.nr_sequencia(+)
	and     x.nr_titulo             = b.nr_titulo
	and     c.nr_titulo             = b.nr_titulo
	and     a.nr_sequencia          = c.nr_seq_cobranca
	and     a.nr_seq_conta_banco    = e.nr_sequencia
	and     a.nr_sequencia          = nr_seq_cobr_escrit_p;
	
Cursor C05 is
	select	ds_tit_depend,	
		vl_mensalidade,
		vl_total,
		ds_ordem
	from	(select	rpad(substr(' '||decode(ds_tit_dep,'X',ds_plano,decode(sum(qt_titular),0, decode(sum(qt_dependente),0, null,(lpad(sum(qt_dependente),4,'0')||'  '||
			ds_tit_dep||'  '||ds_plano||'  '||qt_faixa)),(lpad(sum(qt_titular),4,'0')||'  '||ds_tit_dep||'  '||ds_plano||'  '||
			qt_faixa))),1,59),60,' ') ds_tit_depend,
			vl_mensalidade,
			sum(vl_mensalidade) vl_total,
			qt_faixa,
			ds_tit_dep,
			substr(ds_tit_dep||'(s)  '||ds_plano||'  '||qt_faixa,1,255) ds_ordem
		from	(select	qt_titular,
				qt_dependente,
				ds_plano,
				ds_tit_dep,
				qt_faixa,
				vl_mensalidade
			from	pls_mens_relat_110_v
			where	nr_titulo	= nr_titulo_w)
		where	vl_mensalidade <> 0
		group by qt_titular,
			qt_dependente,
			ds_plano,
			qt_faixa,
			vl_mensalidade,
			ds_tit_dep)
	where	rownum	<= 15;
	
Cursor C06 is
	select	ds_tit_depend,	
		vl_mensalidade,
		vl_total,
		ds_ordem
	from	(select	rpad(substr(' '||decode(ds_tit_dep,'X',ds_plano,decode(sum(qt_titular),0, decode(sum(qt_dependente),0, null,
			(lpad(sum(qt_dependente),4,'0')||'  '||ds_tit_dep||'  '||ds_plano||'  '||qt_faixa)),(lpad(sum(qt_titular),4,'0')||'  '||ds_tit_dep||
			'  '||ds_plano||'  '||qt_faixa))),1,59),60,' ') ds_tit_depend,
			vl_mensalidade,
			sum(vl_mensalidade) vl_total,
			qt_faixa,
			ds_tit_dep,
			ds_tit_dep||'(s)  '||ds_plano||'  '||qt_faixa ds_ordem
		from	(select	qt_titular,
				qt_dependente,
				ds_plano,
				ds_tit_dep,
				qt_faixa,
				vl_mensalidade
			from	pls_mens_relat_110_v
			where	nr_titulo	= nr_titulo_w)
		where	vl_mensalidade <> 0
		group by qt_titular,
			 qt_dependente,
			 ds_plano,
			 qt_faixa,
			 vl_mensalidade,
			 ds_tit_dep)
	where	rownum	<= 43;
	
Cursor C13 is	-- Registro 7
	select	ds_conteudo,
		nr_sequencia
	from	w_pls_mensagem_tit
	where	nr_seq_cobranca  = nr_seq_cobr_escrit_p
	and	nm_usuario	 = nm_usuario_p
	and	nr_titulo	 = nr_titulo_w
	and	ie_tipo_mensagem = '7'
	order by nr_sequencia;
	
Cursor C14 is	-- Registro 8
	select	ds_conteudo,
		nr_sequencia
	from	w_pls_mensagem_tit
	where	nr_seq_cobranca	 = nr_seq_cobr_escrit_p
	and	nm_usuario	 = nm_usuario_p
	and	nr_titulo	 = nr_titulo_w
	order by nr_sequencia;

begin
nm_arquivo_w	:= to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '.rem';

obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

nm_arquivo2_w	:= to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '2.rem';

begin
arq_texto2_w := utl_file.fopen(ds_local_w,nm_arquivo2_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

delete	w_pls_mensagem_tit
where	nm_usuario	= nm_usuario_p
and	nr_seq_cobranca	= nr_seq_cobr_escrit_p;

update	cobranca_escritural
set	ds_arquivo	= ds_local_w || nm_arquivo_w
where	nr_sequencia	= nr_seq_cobr_escrit_p;

-- Brancos
select	lpad(' ',8,' '),
	lpad(' ',9,' '),
	lpad(' ',294,' '),
	lpad(' ',4,' '),
	lpad(' ',21,' '),
	lpad(' ',10,' '),
	lpad(' ',393,' '),
	lpad(' ',31,' '),
	lpad(' ',45,' '),
	lpad(' ',82,' '),
	lpad(' ',30,' '),
	lpad('0',4,'0'),
	lpad('0',5,'0'),
	lpad('0',6,'0')
into	ds_brancos_8_w,
	ds_brancos_9_w,
	ds_brancos_294_w,
	ds_brancos_4_w,
	ds_brancos_21_w,
	ds_brancos_10_w,
	ds_brancos_393_w,
	ds_brancos_31_w,
	ds_brancos_45_w,
	ds_brancos_82_w,
	ds_brancos_30_w,
	ds_zeros_4_w,
	ds_zeros_5_w,
	ds_zeros_6_w
from	dual;

-- Header
select	lpad(nvl(b.cd_conta,'0'),5,'0'),
	lpad(nvl(b.cd_agencia_bancaria,'0'),4,'0'),
	nvl(b.ie_digito_conta,'0'),
	rpad(obter_nome_estabelecimento(cd_estabelecimento_p),30,' ') nm_empresa,
	to_char(sysdate, 'DDMMYY')
into	cd_conta_w,
	cd_agencia_bancaria_w,
	ie_digito_conta_w,
	nm_empresa_w,
	dt_geracao_w
from	banco_estabelecimento b,
	cobranca_escritural a
where	a.nr_seq_conta_banco	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_header_w	:= 	'01' ||
			'REMESSA' ||
			'01' ||
			'COBRANCA       ' ||
			cd_agencia_bancaria_w ||
			'00' ||
			cd_conta_w ||
			ie_digito_conta_w ||
			ds_brancos_8_w ||
			nm_empresa_w ||
			'341' ||
			'BANCO ITAU SA  ' ||
			dt_geracao_w ||
			ds_brancos_294_w ||
			lpad(substr('1',1,6),6,'0');	
			
nr_seq_registro1_w := nr_seq_registro1_w + 1;
nr_seq_registro2_w := nr_seq_registro2_w + 1;
-- Fim - Header

-- Registro Detalhe
open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	cd_conta_w,
	ie_digito_conta_ww,
	nr_seq_carteira_cobr_w,
	cd_banco_w,
	nr_documento_w,
	nr_titulo_w,
	vl_titulo_w,
	vl_titulo_flash_w,
	dt_vencimento_w,
	dt_vencimento_flash_w,
	dt_emissao_w,
	nm_pagador_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_municipio_w,
	sg_estado_w,
	nr_nosso_numero_w,
	nr_digito_agencia_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	cd_carteira_w,
	ie_especie_w,
	ie_aceite_w,
	vl_juros_diario_w,
	nr_seq_mensalidade_w,
	dt_emissao_flash_w,
	dt_referencia_flash_w,
	dt_mes_referencia_w,
	nr_seq_pagador_w,
	dt_vencimento_mens_w,
	dt_pag_prev_w,
	dt_30_dias_vencimento_w,
	ds_mensagem_1_w,
	ds_mensagem_2_w,
	nm_sacador_avalista_w,
	vl_desconto_w,
	vl_iof_w,
	vl_abatimento_w,
	vl_juros_w,
	qt_moeda_w,
	nr_inscricao_empresa_w,
	cd_agencia_cobradora_w,
	ds_observacao_w,
	nr_carteira_w;
exit when C01%notfound;
	begin	
	if	(nr_carteira_w is null) then
		select	substr(max(cd_carteira),1,3)
		into	nr_carteira_w
		from	banco_carteira
		where	nr_sequencia 	= nr_seq_carteira_cobr_w
		and	cd_banco	= cd_banco_w;
		
		if	(nr_carteira_w is null) then
			-- Busca a carteira pelo t�tulo
			select	substr(max(b.cd_carteira),1,3)
			into	nr_carteira_tit_w
			from	banco_carteira b,
				titulo_receber a
			where	b.nr_sequencia  = a.nr_seq_carteira_cobr
			and	a.nr_titulo 	= nr_titulo_w;
			
			-- Se houver carteira do t�tulo, efetuar a troca
			if	(nr_carteira_tit_w is not null) then
				nr_carteira_w := nr_carteira_tit_w;
			end if;
		end if;
	end if;
	
	ds_conteudo_w :=	'1' ||
				'02' ||
				nr_inscricao_empresa_w ||
				substr(lpad(cd_agencia_bancaria_w,4,'0'),1,4) ||
				'00' ||
				substr(cd_conta_w,1,5) ||				
				ie_digito_conta_ww ||
				ds_brancos_4_w ||
				ds_zeros_4_w ||
				lpad(nvl(nr_titulo_w,'0'),25,'0') ||
				lpad(nvl(nr_nosso_numero_w,'0'),8,'0') ||
				qt_moeda_w ||
				lpad(nvl(nr_carteira_w,0),3,'0') ||
				ds_brancos_21_w ||
				'I' ||
				'01' ||
				lpad(nvl(to_char(nr_titulo_w),'0'),10,'0') ||
				dt_vencimento_w ||
				vl_titulo_w ||
				'341' ||
				ds_zeros_5_w || --cd_agencia_cobradora_w ||
				ie_especie_w ||
				ie_aceite_w ||
				dt_emissao_w ||
				'0000' ||
				vl_juros_w ||
				ds_zeros_6_w || --to_char(sysdate,'ddmmyy') ||
				vl_desconto_w ||
				vl_iof_w ||
				vl_abatimento_w ||
				ie_tipo_inscricao_w ||
				substr(nr_inscricao_w,1,14)||
				nm_pagador_w ||
				ds_brancos_10_w ||
				rpad(nvl(ds_endereco_w,' '),40,' ') ||
				rpad(nvl(ds_bairro_w,' '),12,' ') ||
				lpad(nvl(cd_cep_w,'0'),8,'0') ||
				rpad(nvl(ds_municipio_w,' '),15,' ') ||
				rpad(nvl(sg_estado_w,' '),2,' ') ||
				nm_sacador_avalista_w ||
				ds_brancos_4_w ||
				'000000' ||
				'00 ';
				
	usjrp_gerar_mensagem_cob_flash(ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	usjrp_gerar_mensagem_cob_flash(' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
					
	-- Flash
	nr_linha_w		:= 0;
	ie_tipo_coluna_w	:= '0';
	i			:= 1;
	cd_flash_w		:= '740B';
	ie_inicio_w 		:= 'S';
	qt_coluna_w		:= 1;
			 
	select 	count(1)
	into	qt_beneficiarios_w
	from	(select	1,
			qt_titular,
			qt_dependente,
			ds_plano,
			qt_faixa,
			vl_mensalidade
		from	(select	qt_titular,
				qt_dependente,
				ds_plano,
				ds_tit_dep,
				qt_faixa,
				vl_mensalidade
			from	pls_mens_relat_110_v
			where	nr_titulo	= nr_titulo_w)
		where	vl_mensalidade <> 0
		group by qt_titular,
			qt_dependente,
			ds_plano,
			qt_faixa,
			vl_mensalidade);
			
	ds_conteudo_w := ' DESCRICAO                                                                VLR. UNITARIO                              VLR. TOTAL';
				
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
		
	if	(qt_beneficiarios_w <= 15) then
		open C05;
		loop
		fetch C05 into	
			ds_tit_depend_w,
			vl_mensalidade_mens_w,
			vl_total_w,
			ds_ordem_w;
		exit when C05%notfound;
			begin
			ds_conteudo_w := ds_tit_depend_w || lpad(campo_mascara_virgula(vl_mensalidade_mens_w),27,' ') || lpad(campo_mascara_virgula(vl_total_w),40,' ');
			
			usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
			end;
		end loop;
		close C05;
		
	elsif	(qt_beneficiarios_w > 15) then
		open C06;
		loop
		fetch C06 into	
			ds_tit_depend_w,
			vl_mensalidade_mens_w,
			vl_total_w,
			ds_ordem_w;
		exit when C06%notfound;
			begin
			ds_conteudo_w := ds_tit_depend_w || lpad(campo_mascara_virgula(vl_mensalidade_mens_w),27,' ') || lpad(campo_mascara_virgula(vl_total_w),40,' ');
			
			usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
			end;
		end loop;
		close C06;	
	end if;
	
	ds_conteudo_w := null;
	
	select	decode(nr_seq_reajuste,null,null,'ATENCAO: Mensalidade com reajuste de contrato de ' ||
		to_number(substr(pls_obter_dados_lote_reajuste(nr_seq_reajuste,'P'),1,20))||'%')
	into	ds_conteudo_w
	from	(select	max(c.nr_seq_reajuste) nr_seq_reajuste
		from	pls_plano			e,
			titulo_receber			d,
			pls_mensalidade_seg_item	c,
			pls_mensalidade_segurado	b,
			pls_mensalidade			a
		where	d.nr_seq_mensalidade		= a.nr_sequencia
		and	b.nr_seq_mensalidade		= a.nr_sequencia
		and	c.nr_seq_mensalidade_seg	= b.nr_sequencia
		and	b.nr_seq_plano			= e.nr_sequencia
		and	d.nr_titulo			= nr_titulo_w
		and	e.ie_tipo_contratacao		= 'CA');
		
	if	(ds_conteudo_w is not null) then
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
		usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	else
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	end if;
	
	select	distinct
		nvl(b.vl_mensalidade,0) vl_mensalidade,
		decode(g.ie_tipo_lote,'CO',sum(d.vl_ato_cooperado),null) vl_ato_cooperado,
		sum(d.vl_ato_cooperado) vl_ato_cooperado2,
		b.vl_mensalidade - sum(d.vl_ato_cooperado) vl_ato_auxiliar,
		nvl(sum(d.vl_ato_nao_cooperado),0) vl_ato_nao_cooperado,
		sum(d.vl_ato_cooperado) vl_total_benef,
		decode(obter_dados_nota_fiscal(a.nr_seq_nf_saida,7),0,null,obter_dados_nota_fiscal(a.nr_seq_nf_saida,7)) vl_imposto_ir,
		rpad(substr(' IRRF AL�QUOTA LEGAL DE 1,5% C�D.RECOLHIMENTO 3280: ',1,59),60,' ') ||
		lpad(nvl(decode(obter_dados_nota_fiscal(a.nr_seq_nf_saida,7),0,null,obter_dados_nota_fiscal(a.nr_seq_nf_saida,7)),'0,00'),27,' ') ds_aliquota,
		' Para impress�o da nota fical eletr�nica acesse: http://sjrp.ginfes.com.br no link ' ds_mensagem_nota,
		' Autentica��o:  Nota fiscal N�:' || f.nr_nfe_imp || ' C�digo de verifica��o ' || f.cd_verificacao_nfse ds_mensagem_nota2
	into	vl_mensalidade_mens_w,
		vl_ato_cooperado_w,
		vl_ato_cooperado2_w,
		vl_ato_auxiliar_w,
		vl_ato_nao_cooperado_w,
		vl_total_benef_w,
		vl_imposto_ir_w,
		ds_aliquota_w,
		ds_mensagem_nota_w,
		ds_mensagem_nota2_w
	from	pls_conta 		 e,
		pls_mensalidade_seg_item d,
		pls_mensalidade_segurado c,
		pls_mensalidade 	b,
		titulo_receber 		a,
		nota_fiscal		f,
		pls_lote_mensalidade	g
	where	c.nr_sequencia		= d.nr_seq_mensalidade_seg
	and	c.nr_seq_mensalidade	= b.nr_sequencia
	and	a.nr_seq_mensalidade  	= b.nr_sequencia
	and	b.nr_seq_lote		= g.nr_sequencia
	and	d.nr_seq_conta		= e.nr_sequencia(+)
	and	f.nr_seq_mensalidade(+)	= b.nr_sequencia
	and	a.nr_titulo		= nr_titulo_w
	group by
		a.vl_titulo,
		b.vl_mensalidade,
		e.vl_total_beneficiario,
		a.nr_seq_nf_saida,
		f.ds_observacao,
		f.nr_nfe_imp,
		f.cd_verificacao_nfse,
		g.ie_tipo_lote;
		
	ds_conteudo_w := rpad(substr(' '||'SUBTOTAL DA FATURA',1,59),60,' ') || lpad(campo_mascara_virgula(vl_mensalidade_mens_w),27,' ');
	
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	ds_conteudo_w := rpad(substr(' BASE DE C�LCULO DOS SERVI�OS PROFISSIONAIS PRESTADOS',1,127),127,' ');
	
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
			
	ds_conteudo_w := rpad(substr(' AOS USU�RIOS DO CONTRATO (LEI No. 8.981/95 - ART. 64):',1,59),60,' ') ||
			lpad(campo_mascara_virgula(vl_total_benef_w),27,' ');
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');

	ds_conteudo_w :=	ds_aliquota_w;
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	select	decode(a.ie_tipo_lote,'CO',' BASE INSS A RECOLHER ALIQUOTA 15% LEI 9876/99: ',null)
	into	ds_base_inss_w
	from	pls_mensalidade		c,
		titulo_receber		b,
		pls_lote_mensalidade	a
	where	b.nr_seq_mensalidade	= c.nr_sequencia
	and	c.nr_seq_lote		= a.nr_sequencia
	and	b.nr_titulo		= nr_titulo_w;
	
	if	(ds_base_inss_w is not null) then
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
		ds_conteudo_w := rpad(substr(ds_base_inss_w,1,59),60,' ') || lpad(campo_mascara_virgula(vl_total_benef_w),27,' ');
				
		usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
		
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	else
		ds_conteudo_w := null;
	end if;
	
	ds_conteudo_w :=	rpad(substr(' ATOS COOPERATIVOS  PRINCIPAIS ',1,59),60,' ') || lpad(campo_mascara_virgula(vl_ato_cooperado2_w),27,' ');
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	ds_conteudo_w :=	rpad(substr(' ATOS COOPERATIVOS AUXILIARES ',1,59),60,' ') || lpad(campo_mascara_virgula(vl_ato_auxiliar_w),27,' ');
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	ds_conteudo_w :=	rpad(substr(' ATO NAO COOPERATIVO ',1,59),60,' ') || lpad(campo_mascara_virgula(vl_ato_nao_cooperado_w),27,' ');
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	ds_conteudo_w :=	rpad(substr(ds_mensagem_nota_w,1,99),100,' ');
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	ds_conteudo_w :=	rpad(substr(ds_mensagem_nota2_w,1,99),100,' ');
			
	usjrp_gerar_mensagem_cob_flash(	ds_conteudo_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
	
	select	count(1)
	into	qt_registro_w
	from	w_pls_mensagem_tit
	where	nr_titulo = nr_titulo_w;
	
	while (qt_registro_w < 50) loop	
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
		
		qt_registro_w := qt_registro_w + 1;
	end loop;
	
	while (qt_registro_w < 50) loop	
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I');
		
		qt_registro_w := qt_registro_w + 1;
	end loop;
	
	begin
	select	rpad(' ',16,' ') || rpad(substr(pls_obter_dados_pagador(nr_sequencia,'N'),1,96),96,' ') ||
		rpad(substr(pls_obter_dados_pagador(nr_sequencia,'NC'),1,15),15,' ')
	into	ds_endereco_w
	from	pls_contrato_pagador
	where	nr_sequencia   = nr_seq_pagador_w;
	exception
	when others then
		ds_endereco_w := null;
	end;
	
	begin
	select	rpad(' ',12,' ') || 
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'E'),pls_obter_compl_pagador(d.nr_sequencia,'E')),1,30) || ' ' ||
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'NR'),pls_obter_compl_pagador(d.nr_sequencia,'NR')),1,10) || ' ' ||
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'CO'),pls_obter_compl_pagador(d.nr_sequencia,'CO')),1,20),80, ' ') ||
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'B'),pls_obter_compl_pagador(d.nr_sequencia,'B')),1,20)
	into	ds_endereco1_w
	from	pls_contrato_pagador d
	where	d.nr_sequencia = nr_seq_pagador_w;
	exception
	when others then
		ds_endereco1_w := null;
	end;
	
	begin
	select	rpad(' ',12,' ') ||
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'CI'),pls_obter_compl_pagador(d.nr_sequencia,'CI')),1,30),34,' ') ||
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'UF'),pls_obter_compl_pagador(d.nr_sequencia,'UF')),1,10),43,' ') ||
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'CEP'),pls_obter_compl_pagador(d.nr_sequencia,'CEP')),1,20),20,' ')
	into	ds_endereco2_w
	from	pls_contrato_pagador d
	where   nr_sequencia    = nr_seq_pagador_w;
	exception
	when others then
		ds_endereco2_w := null;
	end;
	
	begin
	select	rpad(nvl(rpad(' ',11,' ') || rpad(nvl(nvl(obter_cpf_pessoa_fisica(cd_pessoa_fisica),cd_cgc),' '),62,' ') ||
		rpad(nvl(obter_dados_pf_pj(cd_pessoa_fisica, cd_cgc, 'IE'),' '),112,' '), ' '),112,' ') || rpad(nvl(dt_referencia_w,' '),12,' ') ds_endereco3_w,
		rpad(' ',30,' ') || rpad(nr_titulo_w,155,' ') || rpad(nvl(to_char(dt_pagamento_previsto,'dd/mm/yyyy'),' '),12,' ') ds_vencimento_w
	into	ds_endereco3_w,
		ds_vencimento_w
	from	titulo_receber
	where	nr_titulo   = nr_titulo_w;
	exception
	when others then
		ds_endereco3_w := null;
		ds_vencimento_w := null;
	end;
	
	if	(qt_registro_w < 51) and (ie_arquivo1_w = 'N') then
		ie_arquivo1_w := 'S';
	
	elsif	(ie_arquivo2_w = 'N') then
		ie_arquivo2_w := 'S';
	end if;
	
	if	(ie_arquivo1_w = 'S') then
		utl_file.put_line(arq_texto_w,ds_header_w|| chr(13));
		utl_file.fflush(arq_texto_w);
		
		ie_arquivo1_w := 'X';
	end if;

	if	(ie_arquivo2_w = 'S') then
		utl_file.put_line(arq_texto2_w,ds_header_w|| chr(13));
		utl_file.fflush(arq_texto2_w);
		
		ie_arquivo2_w := 'X';
	end if;
	
	ie_endereco_w := 'N';
	ie_imprimir_end_w := 'S';
	
	if	(qt_registro_w < 51) then  	-- Registro 7
		ds_conteudo_w := '740B';
		
		-- ENDERE�O
		usjrp_gerar_mensagem_cob_flash(	ds_endereco_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 50
		usjrp_gerar_mensagem_cob_flash(	ds_endereco1_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 51
		usjrp_gerar_mensagem_cob_flash(	ds_endereco2_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 52
		usjrp_gerar_mensagem_cob_flash(	ds_endereco3_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 53
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 54
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 55
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 56		
		usjrp_gerar_mensagem_cob_flash(	ds_vencimento_w,nr_seq_cobr_escrit_p,nr_titulo_w,'1','N','S',nm_usuario_p,cd_estabelecimento_p,'I'); -- 57
	
		open C13;
		loop
		fetch C13 into	
			ds_conteudo_flash_w,
			nr_seq_mensagem_w;
		exit when C13%notfound;
			begin					
			if	(ie_tipo_coluna_w <> '0') then
				ds_conteudo_w := ds_conteudo_w || lpad(qt_coluna_w,2,'0') || rpad(substr(nvl(ds_conteudo_flash_w,' '),1,128),128,' ');				
				qt_coluna_w := qt_coluna_w + 1;
				
			elsif	(ie_tipo_coluna_w = '0') then
				ds_conteudo_w := ds_conteudo_flash_w;
			end if;
			
			if	(ie_tipo_coluna_w = '3') or 
				(ie_tipo_coluna_w = '0') then
				nr_seq_registro1_w := nr_seq_registro1_w + 1;
				
				utl_file.put_line(arq_texto_w,ds_conteudo_w ||lpad(nr_seq_registro1_w,6,'0')|| chr(13));
				utl_file.fflush(arq_texto_w);
				
				ds_conteudo_w := '740B';
			end if;

			case	ie_tipo_coluna_w
				when '1' then ie_tipo_coluna_w := '2';
				when '2' then ie_tipo_coluna_w := '3';
				when '3' then ie_tipo_coluna_w := '1';
				else ie_tipo_coluna_w := '1';
			end case;
			end;
		end loop;
		close C13;		
	else					-- Registro 8 / 7
		ds_conteudo_w := null;
		ie_tipo_registro_w := '8';
	
		open C14;
		loop
		fetch C14 into	
			ds_conteudo_flash_w,
			nr_seq_mensagem_w;
		exit when C14%notfound;
			begin
			-- Registro 8
			if	(ie_tipo_registro_w = '8') then
				update	w_pls_mensagem_tit
				set	ie_tipo_mensagem	= '8'
				where	nr_sequencia		= nr_seq_mensagem_w;
				
				if	(qt_coluna_w > 99) then
					qt_coluna_w := to_number(substr(qt_coluna_w,2,2));
				end if;
			
				if	(ie_tipo_coluna_w = '0') then
					ds_conteudo_w := ds_conteudo_flash_w;
					
				elsif	(ie_tipo_coluna_w = '1') then					
					ds_conteudo_w := ds_conteudo_w || ie_tipo_registro_w || lpad(qt_coluna_w,2,'0') || 
								substr(rpad(nvl(ds_conteudo_flash_w,' '),180,' '),1,180) || lpad(' ',10,' ');
					qt_coluna_w := qt_coluna_w + 1;
					
				elsif	(ie_tipo_coluna_w = '2') then
					ds_conteudo_w := ds_conteudo_w || lpad(qt_coluna_w,2,'0') || 
								substr(rpad(nvl(ds_conteudo_flash_w,' '),199,' '),1,199);
					qt_coluna_w := qt_coluna_w + 1;
				end if;

				if	(qt_coluna_w = 28) and
					(ie_tipo_coluna_w = '1') then					
					ds_conteudo_w := ds_conteudo_w || lpad(qt_coluna_w,2,'0') || rpad(' ',199,' ');
					ie_tipo_coluna_w := '2';
				end if;
				
				if	((ie_tipo_coluna_w = '2') or 
					(ie_tipo_coluna_w = '0')) and
					(ie_endereco_w = 'N') then
					nr_seq_registro2_w := nr_seq_registro2_w + 1;
					
					utl_file.put_line(arq_texto2_w,ds_conteudo_w ||lpad(nr_seq_registro2_w,6,'0')|| chr(13));
					utl_file.fflush(arq_texto2_w);
					
					ds_conteudo_w := null;
					
					if	(qt_coluna_w = 28) and
						(ie_tipo_coluna_w = '2') and
						(ie_imprimir_end_w = 'S') then	
						ie_endereco_w := 'S';
						ie_imprimir_end_w := 'N';
					end if;
				end if;
				
				if	(ie_endereco_w = 'S') then
					-- ENDERE�O
					nr_seq_registro2_w := 	nr_seq_registro2_w + 1;
					
					ds_ender_mens_w :=	'8' || '50' || substr(rpad(nvl(ds_endereco_w,' '),180,' '),1,180) || lpad(' ',10,' ') ||
								'51' || substr(rpad(nvl(ds_endereco1_w,' '),199,' '),1,199) || lpad(nr_seq_registro2_w,6,'0');
								
					utl_file.put_line(arq_texto2_w,ds_ender_mens_w || chr(13));
					utl_file.fflush(arq_texto2_w);
					
					nr_seq_registro2_w := 	nr_seq_registro2_w + 1;
								
					ds_ender2_mens_w :=	'8' || '52' || substr(rpad(nvl(ds_endereco2_w,' '),180,' '),1,180) || lpad(' ',10,' ') ||
								'53' || substr(rpad(nvl(ds_endereco3_w,' '),199,' '),1,199) || lpad(nr_seq_registro2_w,6,'0');	

					utl_file.put_line(arq_texto2_w,ds_ender2_mens_w || chr(13));
					utl_file.fflush(arq_texto2_w);	

					ds_ender2_mens_w := null;
					
					nr_seq_registro2_w := 	nr_seq_registro2_w + 1;

					ds_ender2_mens_w :=	'8' || '54' || lpad(' ',190,' ') || '55' || lpad(' ',199,' ') || lpad(nr_seq_registro2_w,6,'0');	

					utl_file.put_line(arq_texto2_w,ds_ender2_mens_w || chr(13));
					utl_file.fflush(arq_texto2_w);	
					
					ds_ender2_mens_w := null;
					
					nr_seq_registro2_w := 	nr_seq_registro2_w + 1;
					
					ds_ender2_mens_w :=	'8' || '56' || lpad(' ',190,' ') || '57' || 
								substr(rpad(nvl(ds_vencimento_w,' '),199,' '),1,199) || lpad(nr_seq_registro2_w,6,'0');	

					utl_file.put_line(arq_texto2_w,ds_ender2_mens_w || chr(13));
					utl_file.fflush(arq_texto2_w);
					
					ie_endereco_w := 'N';
				end if;
				
				case	ie_tipo_coluna_w
					when '1' then ie_tipo_coluna_w := '2';
					when '2' then ie_tipo_coluna_w := '1';
					else ie_tipo_coluna_w := '1';
				end case;
			end if;
			end;
		end loop;
		close C14;		
	end if;
	
	delete	w_pls_mensagem_tit
	where	nm_usuario	= nm_usuario_p
	and	nr_seq_cobranca	= nr_seq_cobr_escrit_p
	and	nr_titulo	= nr_titulo_w;
	
	commit;
	end;
end loop;
close C01;
-- Fim - Registro Detalhe

-- Trailer
ds_conteudo_w	:= '9' || ds_brancos_393_w;

if	(ie_arquivo1_w = 'X') then
	nr_seq_registro1_w := nr_seq_registro1_w + 1;
	utl_file.put_line(arq_texto_w,ds_conteudo_w || lpad(nr_seq_registro1_w,6,'0') || chr(13));
	utl_file.fflush(arq_texto_w);
end if;

if	(ie_arquivo2_w = 'X') then
	nr_seq_registro2_w := nr_seq_registro2_w + 1;
	utl_file.put_line(arq_texto2_w,ds_conteudo_w || lpad(nr_seq_registro2_w,6,'0') || chr(13));
	utl_file.fflush(arq_texto2_w);
end if;		   
-- Fim - Trailer

end usjrp_ger_cob_empresarial_itau;
/