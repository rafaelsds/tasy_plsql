create or replace procedure gerar_medic_npt(nr_prescricao_p	number) is

cd_material_w				number(6);
qt_dispensar_w				number(18,6);
cd_unid_med_prescr_w		varchar2(30);
nr_sequencia_w				number(6);
nm_usuario_w				varchar2(15);
cd_intervalo_w				varchar2(15);
cd_intervalo_ww				varchar2(15);
ie_dispensacao_w			varchar2(4);
ie_regra_disp_w				prescr_material.ie_regra_disp%type;
qt_vol_cor_w				number(15,4);
qt_conv_unid_cons_w			number(18,6);
cd_unidade_medida_consumo_w	varchar2(30);
qt_total_dispensar_w		number(18,6);
nr_seq_protocolo_w			number(10);
nr_seq_nut_pac_w			number(10);
ie_acm_w					varchar2(1);
qt_fase_npt_w				nut_pac.qt_fase_npt%type;
qt_vol_1_fase_w				nut_pac_elem_mat.qt_vol_1_fase%type;
qt_vol_2_fase_w				nut_pac_elem_mat.qt_vol_1_fase%type;
qt_vol_3_fase_w				nut_pac_elem_mat.qt_vol_1_fase%type;
qt_vol_4_fase_w				nut_pac_elem_mat.qt_vol_1_fase%type;
hr_prim_horario_w			nut_pac.hr_prim_horario%type;
ie_gerar_lote_w             prescr_material.ie_gerar_lote%type;
ds_dose_diferenciada_w		prescr_material.ds_dose_diferenciada%type;
nr_ocorrencia_w				prescr_material.nr_ocorrencia%type;
ds_horarios_w				prescr_material.ds_horarios%type;
ds_horarios_ww				prescr_material.ds_horarios%type;
i						    number(15);
cd_motivo_baixa_param_w 	number(15);
dt_inicio_prescr_w			prescr_medica.dt_inicio_prescr%type;
dt_inicio_item_w			prescr_medica.dt_inicio_prescr%type;
cd_estabelecimento_w		prescr_medica.cd_estabelecimento%type;
qt_min_intervalo_w			intervalo_prescricao.qt_min_intervalo%type;

sql_w varchar2(300);

cursor c01 is
	select	c.cd_material,
			b.qt_dispensar,
			b.cd_unid_med_prescr,
			a.nm_usuario,
			a.hr_prim_horario
	from	nut_elemento_cadastro c,
			nut_paciente_elemento b,
			nut_paciente a,
			prescr_medica k
	where	k.nr_prescricao		= nr_prescricao_p
	and		k.nr_prescricao		= a.nr_prescricao
	and		a.nr_sequencia		= b.nr_seq_nut_pac
	and		b.nr_seq_elem_mat	= c.nr_sequencia
	and		b.nr_seq_elem_mat is not null
	and		b.cd_unid_med_prescr is not null
	and		b.qt_dispensar is not null
	and		b.qt_dispensar > 0;

cursor c02 is
	select	e.cd_material,
			decode(nvl(c.qt_vol_cor,0),0,c.qt_volume,c.qt_vol_cor),
			substr(obter_dados_material_estab(e.cd_material,k.cd_estabelecimento,'UMS'),1,30) cd_unidade_medida_consumo,
			a.nm_usuario,
			a.nr_seq_protocolo,
			a.nr_sequencia,
			nvl(a.ie_acm,'N'),
			nvl(a.qt_fase_npt,1),
			obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),nvl(c.qt_vol_1_fase,0)) qt_vol_1_fase,
			obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),nvl(c.qt_vol_2_fase,0)) qt_vol_2_fase,
			obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),nvl(c.qt_vol_3_fase,0)) qt_vol_3_fase,
			obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),nvl(c.qt_vol_4_fase,0)) qt_vol_4_fase,
			a.cd_intervalo,
			a.hr_prim_horario,
			obter_se_npt_disp(a.nr_seq_protocolo, e.cd_material) ie_dispensar
	from	material 			e,
			nut_pac_elem_mat 	c,
			nut_pac 			a,
			prescr_medica k
	where	k.nr_prescricao		= nr_prescricao_p
	and		k.nr_prescricao		= a.nr_prescricao
	and		a.nr_sequencia		= c.nr_seq_nut_pac
	and		c.cd_material		= e.cd_material
	and		decode(nvl(c.qt_vol_cor,0),0,c.qt_volume,c.qt_vol_cor) > 0;

cursor c03 is
select	e.cd_material,
		decode(nvl(c.qt_vol_cor,0),0,c.qt_volume,c.qt_vol_cor),
		substr(obter_dados_material_estab(e.cd_material,1,'UMS'),1,30) cd_unidade_medida_consumo,
		a.nm_usuario,
		a.nr_seq_protocolo,
		a.nr_sequencia,
		nvl(a.ie_acm,'N'),
		nvl(a.qt_fase_npt,1),
		obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),NVL(c.qt_vol_1_fase,0)) qt_vol_1_fase,
		obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),NVL(c.qt_vol_2_fase,0)) qt_vol_2_fase,
		obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),NVL(c.qt_vol_3_fase,0)) qt_vol_3_fase,
		obter_conversao_unid_med_cons(e.cd_material, obter_unid_med_usua('ml'),NVL(c.qt_vol_4_fase,0)) qt_vol_4_fase,
		a.cd_intervalo,
		a.hr_prim_horario
from   	nut_pac a,
		nut_pac_elemento b,
		nut_pac_elem_mat c,
		nut_elem_material d,
		material e
where  	a.nr_sequencia = b.nr_seq_nut_pac
and	   	b.nr_sequencia = c.nr_seq_pac_elem
and	   	b.nr_seq_elemento = d.nr_seq_elemento
and	   	d.cd_material = e.cd_material
and	   	a.nr_prescricao = nr_prescricao_p
and	   	d.ie_situacao = 'A'
and	   	d.ie_padrao = 'S'
and		decode(nvl(c.qt_vol_cor,0),0,c.qt_volume,c.qt_vol_cor) > 0;

begin

select	nvl(max(b.nr_sequencia),0) + 1
into	nr_sequencia_w
from	prescr_material b,
		prescr_medica a
where	a.nr_prescricao	= nr_prescricao_p
and		a.nr_prescricao	= b.nr_prescricao;

select	max(cd_estabelecimento),
		max(dt_inicio_prescr)
into	cd_estabelecimento_w,
		dt_inicio_prescr_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

cd_intervalo_ww := obter_interv_prescr_padrao(cd_estabelecimento_w);

open C01;
loop
	fetch C01 into
		cd_material_w,
		qt_dispensar_w,
		cd_unid_med_prescr_w,
		nm_usuario_w,
		hr_prim_horario_w;
	exit when C01%notfound;

	insert into prescr_material(
		nr_prescricao,
		nr_sequencia,
		ie_origem_inf,
		cd_material,
		cd_unidade_medida,
		qt_dose,
		qt_unitaria,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		cd_intervalo,
		ie_utiliza_kit,
		cd_unidade_medida_dose,
		ie_urgencia,
		nr_ocorrencia,
		qt_total_dispensar,
		ie_medicacao_paciente,
		ie_agrupador,
		ie_suspenso,
		ie_se_necessario,
		ie_bomba_infusao,
		ie_aplic_bolus,
		ie_aplic_lenta,
		ie_acm,
		cd_motivo_baixa,
		ie_recons_diluente_fixo,
		ie_sem_aprazamento)
	values(
		nr_prescricao_p,
		nr_sequencia_w,
		1,
		cd_material_w,
		cd_unid_med_prescr_w,
		qt_dispensar_w,
		qt_dispensar_w,
		qt_dispensar_w,
		sysdate,
		nm_usuario_w,
		cd_intervalo_ww,
		'N',
		cd_unid_med_prescr_w,
		'N',
		1,
		qt_dispensar_w,
		'N',
		10,
		'N',
		'N',
		'N',
		'N',
		'N',
		'N',
		0,
		'N',
		'N');

	nr_sequencia_w	:= nr_sequencia_w + 1;

end loop;
close C01;

nr_sequencia_w	:= nr_sequencia_w + 1;

open C02;
loop
fetch C02 into
	cd_material_w,
	qt_vol_cor_w,
	cd_unidade_medida_consumo_w,
	nm_usuario_w,
	nr_seq_protocolo_w,
	nr_seq_nut_pac_w,
	ie_acm_w,
	qt_fase_npt_w,
	qt_vol_1_fase_w,
	qt_vol_2_fase_w,
	qt_vol_3_fase_w,
	qt_vol_4_fase_w,
	cd_intervalo_w,
	hr_prim_horario_w,
	ie_regra_disp_w;
exit when C02%notfound;

	cd_motivo_baixa_param_w	:= 0;

	if (ie_regra_disp_w = 'D') then
		ie_regra_disp_w := null;
	elsif (ie_regra_disp_w = 'N') then
		cd_motivo_baixa_param_w		:= Wheb_assist_pck.obterParametroFuncao(924,194);
	end if;

	qt_total_dispensar_w	:= obter_conversao_unid_med_cons(cd_material_w, obter_unid_med_usua('ml'),qt_vol_cor_w);
	nr_ocorrencia_w := null;

	if (hr_prim_horario_w is null) then
		hr_prim_horario_w := to_char(dt_inicio_prescr_w,'hh24:mi');
	end if;

	dt_inicio_item_w := ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w, hr_prim_horario_w);

	if (cd_intervalo_w is not null) then

		select	max(qt_min_intervalo)
		into	qt_min_intervalo_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_w;

		Calcular_horario_prescricao( nr_prescricao_p, cd_intervalo_w, dt_inicio_prescr_w, dt_inicio_item_w, 24, cd_material_w, 0, qt_min_intervalo_w,
									 nr_ocorrencia_w, ds_horarios_w, ds_horarios_ww, 'N', null);

		ds_horarios_w := ds_horarios_w||ds_horarios_ww;
	else
		nr_ocorrencia_w := 1;
		ds_horarios_w 	:= hr_prim_horario_w;
		cd_intervalo_w	:= cd_intervalo_ww;
	end if;
	--- Inicio MD1
  
	begin
	sql_w := 'call obter_dose_diferenciada_md(:1, :2, :3, :4, :5) into :ds_dose_diferenciada_w';
	execute immediate sql_w using in qt_fase_npt_w,
								  in qt_vol_1_fase_w,
								  in qt_vol_2_fase_w,
								  in qt_vol_3_fase_w,
								  in qt_vol_4_fase_w,
								  out ds_dose_diferenciada_w;
	exception
	when others then
	  ds_dose_diferenciada_w := null;
	end;
  
	--- Fim MD1
	insert into prescr_material(
		nr_prescricao,
		nr_sequencia,
		ie_origem_inf,
		cd_material,
		cd_unidade_medida,
		qt_dose,
		qt_unitaria,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		cd_intervalo,
		ie_utiliza_kit,
		cd_unidade_medida_dose,
		ie_urgencia,
		nr_ocorrencia,
		qt_total_dispensar,
		ie_medicacao_paciente,
		ie_agrupador,
		ie_suspenso,
		ie_se_necessario,
		ie_bomba_infusao,
		ie_aplic_bolus,
		ie_aplic_lenta,
		ie_acm,
		cd_motivo_baixa,
		ie_recons_diluente_fixo,
		ie_sem_aprazamento,
		ie_regra_disp,
		nr_seq_nut_pac,
		ds_dose_diferenciada,
		ds_horarios,
		hr_prim_horario,
		dt_inicio_medic)
	values(
		nr_prescricao_p,
		nr_sequencia_w,
		1,
		cd_material_w,
		cd_unidade_medida_consumo_w,
		qt_vol_cor_w,
		qt_total_dispensar_w,
		qt_total_dispensar_w,
		sysdate,
		nm_usuario_w,
		cd_intervalo_w,
		'N',
		obter_unid_med_usua('ml'),
		'N',
		nr_ocorrencia_w,
		qt_total_dispensar_w,
		'N',
		11,
		'N',
		'N',
		'N',
		'N',
		'N',
		ie_acm_w,
		cd_motivo_baixa_param_w,
		'N',
		'N',
		ie_regra_disp_w,
		nr_seq_nut_pac_w,
		ds_dose_diferenciada_w,
		ds_horarios_w,
		hr_prim_horario_w,
		dt_inicio_item_w);

	nr_sequencia_w	:= nr_sequencia_w + 1;

end loop;
close C02;

open c03;
loop
fetch c03 into
	cd_material_w,
	qt_vol_cor_w,
	cd_unidade_medida_consumo_w,
	nm_usuario_w,
	nr_seq_protocolo_w,
	nr_seq_nut_pac_w,
	ie_acm_w,
	qt_fase_npt_w,
	qt_vol_1_fase_w,
	qt_vol_2_fase_w,
	qt_vol_3_fase_w,
	qt_vol_4_fase_w,
	cd_intervalo_w,
	hr_prim_horario_w;
exit when c03%notfound;

	if (nr_seq_protocolo_w is not null) then
		select	nvl(max(ie_dispensacao),'S')
		into	ie_dispensacao_w
		from	protocolo_npt_prod
		where	nr_seq_protocolo = nr_seq_protocolo_w
		and		cd_material 	 = cd_material_w;
		if (ie_dispensacao_w = 'T') then
			ie_regra_disp_w := 'S';
		elsif (ie_dispensacao_w = 'N') then
			ie_regra_disp_w := 'N';

			cd_motivo_baixa_param_w		:= Wheb_assist_pck.obterParametroFuncao(924,194);

		else
			ie_regra_disp_w := null;
		end if;
	else
		ie_regra_disp_w := 'N';
		ie_gerar_lote_w := 'N';

	end if;

	qt_total_dispensar_w	:= obter_conversao_unid_med_cons(cd_material_w, obter_unid_med_usua('ml'),qt_vol_cor_w);
	nr_ocorrencia_w := null;

	if (hr_prim_horario_w is null) then
		hr_prim_horario_w := to_char(dt_inicio_prescr_w,'hh24:mi');
	end if;

	dt_inicio_item_w := ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w, hr_prim_horario_w);

	if (cd_intervalo_w is not null) then

		select	max(qt_min_intervalo)
		into	qt_min_intervalo_w
		from	intervalo_prescricao
		where	cd_intervalo = cd_intervalo_w;

		dt_inicio_item_w := ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w, hr_prim_horario_w);

		Calcular_horario_prescricao( nr_prescricao_p, cd_intervalo_w, dt_inicio_prescr_w, dt_inicio_item_w, 24, cd_material_w, 0, qt_min_intervalo_w,
									 nr_ocorrencia_w, ds_horarios_w, ds_horarios_ww, 'N', null);

		ds_horarios_w := ds_horarios_w||ds_horarios_ww;
	else
		nr_ocorrencia_w := 1;
		ds_horarios_w 	:= hr_prim_horario_w;
		cd_intervalo_w	:= cd_intervalo_ww;
	end if;
	--- Inicio MD2 ver md 1
	begin
      sql_w := 'call obter_dose_diferenciada_md(:1, :2, :3, :4, :5) into :ds_dose_diferenciada_w';
      execute immediate sql_w using in qt_fase_npt_w,
                                    in qt_vol_1_fase_w,
                                    in qt_vol_2_fase_w,
                                    in qt_vol_3_fase_w,
                                    in qt_vol_4_fase_w,
                                    out ds_dose_diferenciada_w;
    exception
      when others then
        ds_dose_diferenciada_w := null;
    end;
	--- Fim MD2
	insert into prescr_material(
		nr_prescricao,
		nr_sequencia,
		ie_origem_inf,
		cd_material,
		cd_unidade_medida,
		qt_dose,
		qt_unitaria,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		cd_intervalo,
		ie_utiliza_kit,
		cd_unidade_medida_dose,
		ie_urgencia,
		nr_ocorrencia,
		qt_total_dispensar,
		ie_medicacao_paciente,
		ie_agrupador,
		ie_suspenso,
		ie_se_necessario,
		ie_bomba_infusao,
		ie_aplic_bolus,
		ie_aplic_lenta,
		ie_acm,
		cd_motivo_baixa,
		ie_recons_diluente_fixo,
		ie_sem_aprazamento,
		ie_regra_disp,
		nr_seq_nut_pac,
		ds_dose_diferenciada,
		ie_gerar_lote,
		ds_horarios,
		hr_prim_horario,
		dt_inicio_medic)
	values(
		nr_prescricao_p,
		nr_sequencia_w,
		1,
		cd_material_w,
		cd_unidade_medida_consumo_w,			   
		qt_vol_cor_w,
		qt_total_dispensar_w,
		qt_total_dispensar_w,
		sysdate,
		nm_usuario_w,
		cd_intervalo_w,
		'N',
		obter_unid_med_usua('ml'),
		'N',
		nr_ocorrencia_w,
		qt_total_dispensar_w,
		'N',
		11,
		'N',
		'N',
		'N',
		'N',
		'N',
		ie_acm_w,
		decode(ie_regra_disp_w, 'N', cd_motivo_baixa_param_w, 0), --0,
		'N',
		'N',
		ie_regra_disp_w,
		nr_seq_nut_pac_w,
		ds_dose_diferenciada_w,
		ie_gerar_lote_w,
		ds_horarios_w,
		hr_prim_horario_w,
		dt_inicio_item_w);

	nr_sequencia_w	:= nr_sequencia_w + 1;

end loop;
close c03;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

END Gerar_medic_NPT;
/