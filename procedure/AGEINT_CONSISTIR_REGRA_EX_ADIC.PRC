create or replace
procedure Ageint_Consistir_Regra_Ex_Adic(
			nr_seq_ageint_p		number,
			nr_seq_proc_interno_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2,
			nr_seq_regra_p	out	number,
			ds_retorno_p	out	varchar2) is 

cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
nr_seq_item_regra_w	number(10);
nr_seq_regra_w		number(10); 
qt_exames_regra_w	number(10);
qt_exames_w		number(10);
ds_retorno_w		varchar2(255);
nr_seq_grupo_w		number(10);
nr_seq_proc_interno_w	number(10,0);
qt_regra_tempo_exames_w	number(10,0) := 0;
qt_regra_tempo_exames_pinc_w	number(10,0) := 0;
nr_seq_proc_interno_adic_w	number(10,0);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_grupo_selec,
		nr_seq_proc_interno
	from	agenda_integrada_item
	where	nr_seq_regra_ex_adic	= nr_seq_regra_w
	and	nr_seq_agenda_int	= nr_seq_ageint_p
	and	ds_retorno_w is null;
	
Cursor C02 is
	select	nr_seq_proc_interno
	from	ageint_exame_adic_item
	where	nr_seq_item	= nr_seq_item_Regra_w;
			
begin

if	(nr_seq_proc_interno_p is not null) then
	select	max(cd_convenio),
		max(cd_categoria)
	into	cd_convenio_w,
		cd_categoria_w
	from	agenda_integrada
	where	nr_sequencia	= nr_seq_Ageint_p;
	
	nr_seq_regra_w	:= Ageint_Obter_Seq_Regra_Ex_Adic(nr_seq_proc_interno_p, cd_convenio_w, cd_categoria_w, cd_estabelecimento_p, nm_usuario_p);

	if	(nvl(nr_seq_regra_w,0)	> 0) then
		select	qt_exames
		into	qt_exames_regra_w
		from	ageint_qtd_exames_adic
		where	nr_sequencia	= nr_seq_regra_w;

		open C01;
		loop
		fetch C01 into	
			nr_seq_item_regra_w,
			nr_seq_grupo_w,
			nr_seq_proc_interno_w;
		exit when C01%notfound;
			begin
			
			if	(qt_regra_tempo_exames_pinc_w = 0) then
				select	count(*)
				into	qt_regra_tempo_exames_pinc_w
				from	ageint_tempo_entre_exames
				where	((cd_exame_pri	= nr_seq_proc_interno_w)
				or	(cd_exame_seg	= nr_seq_proc_interno_w))
				and	((cd_exame_pri	= nr_seq_proc_interno_p)
				or	(cd_exame_seg	= nr_seq_proc_interno_p))
				and	ie_situacao	= 'A';
			end if;
			
			select	count(*)
			into	qt_exames_w
			from	ageint_exame_adic_item
			where	nr_seq_item	= nr_seq_item_Regra_w;
			
			open C02;
			loop
			fetch C02 into	
				nr_seq_proc_interno_adic_w;
			exit when C02%notfound;
				/*begin*/
				begin				
				if	(qt_regra_tempo_exames_w = 0) then
					select	count(*)
					into	qt_regra_tempo_exames_w
					from	ageint_tempo_entre_exames
					where	((cd_exame_pri	= nr_seq_proc_interno_adic_w)
					or	(cd_exame_seg	= nr_seq_proc_interno_adic_w))
					and	((cd_exame_pri	= nr_seq_proc_interno_p)
					or	(cd_exame_seg	= nr_seq_proc_interno_p))
					and	ie_situacao	= 'A';
				end if;
				/*exception
				when no_data_found then
					qt_regra_tempo_exames_w	:= 0;
				end;*/
				end;
			end loop;
			close C02;
			
			if	(qt_regra_tempo_exames_pinc_w = 0) and
				(qt_regra_tempo_exames_w = 0) then
			
			
				if	(qt_exames_w + 1 >= qt_exames_regra_w) then
					nr_seq_regra_p	:= nr_seq_regra_w;
				else
					insert into ageint_exame_adic_item
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_proc_interno,
							nr_seq_item,
							nr_seq_grupo_selec)
						values
							(ageint_exame_adic_item_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_proc_interno_p,
							nr_seq_item_regra_w,
							nr_seq_grupo_w);
					commit;
					ds_retorno_w	:= wheb_mensagem_pck.get_texto(790854);
				end if;
			
			end if;
			end;
		end loop;
		close C01;
		if	(ds_retorno_w is null) then
			nr_seq_regra_p	:= nr_seq_regra_w;
		end if;
	end if;

	ds_retorno_p	:= ds_retorno_w;
end if;
	
commit;

end Ageint_Consistir_Regra_Ex_Adic;
/
