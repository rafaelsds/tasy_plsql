create or replace
procedure Assume_Atend_Hospitalidade(	cd_pessoa_fisica_p	varchar2,
					nr_atendimento_p	number) is
					
nr_sequencia_w		number(10);

begin

begin
select	max(nr_sequencia)
into	nr_sequencia_w
from	atend_profissional
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	nr_atendimento		 = nr_atendimento_p
and ((ie_profissional	 = 'H') or (IS_COPY_BASE_LOCALE('es_MX') = 'S' and ie_profissional = '12'));
exception
	when others then
		nr_sequencia_w	:= 0;
end;

if	((nr_sequencia_w is null) or 
	 (nr_sequencia_w = 0)) and
	 (nvl(nr_atendimento_p,0) > 0) then
	
	insert into atend_profissional (
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_atendimento,
		cd_pessoa_fisica,
		ie_profissional,
		dt_inicio_vigencia
	) values (
		atend_profissional_seq.nextval,
		obter_usuario_pf(cd_pessoa_fisica_p),
		sysdate,
		obter_usuario_pf(cd_pessoa_fisica_p),
		sysdate,
		nr_atendimento_p,
		cd_pessoa_fisica_p,
		'H',
		sysdate
	);
	
end if;

commit;

end Assume_Atend_Hospitalidade;
/