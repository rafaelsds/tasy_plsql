create or replace
procedure excluir_mat_atend_pac_opme(	nr_seq_mat_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ds_erro_p	out	varchar2) is 

cd_motivo_exc_conta_w		number(3);
ie_status_acerto_w		number(1);
nr_interno_conta_w		number(10);
nr_atendimento_w		number(10);
dt_entrada_unidade_w		date;
cd_material_w			number(6);
dt_atendimento_w		date;
qt_material_w			number(9,3);
cd_setor_atendimento_w		number(5);
cd_unidade_medida_w		varchar2(30);
cd_local_estoque_w		number(4);
nr_prescricao_w			number(10);
cd_cgc_w			varchar2(14);
ie_estorna_mov_w		varchar2(1);
qt_dev_w			number(5);
					
begin
Obter_Param_Usuario(1750,46,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_estorna_mov_w);


select 	max(cd_motivo_exc_conta)
into	cd_motivo_exc_conta_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p;

select	nvl(max(nr_interno_conta),0)
into	nr_interno_conta_w
from	material_atend_paciente
where 	nr_sequencia = nr_seq_mat_p;

select	nvl(max(ie_status_acerto),0)
into	ie_status_acerto_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_w;

if	(nvl(nr_seq_mat_p,0) > 0) and
	(ie_status_acerto_w = 1) then

	update	material_atend_paciente
	set	nr_interno_conta = null,
		cd_motivo_exc_conta  = cd_motivo_exc_conta_w,
		nm_usuario = nm_usuario_p
	where 	nr_sequencia = nr_seq_mat_p;
	
	if	(ie_estorna_mov_w = 'S') then
		
		select	count(*)
		into	qt_dev_w
		from	material_atend_paciente a
		where	nr_sequencia = nr_seq_mat_p
		and	dt_atualizacao_estoque is not null;
		
		if	(nvl(qt_dev_w,0) > 0) then
			select	a.nr_atendimento,
				a.dt_entrada_unidade,
				nvl(a.cd_material_exec, a.cd_material), 
				a.dt_atendimento,
				a.qt_material,
				a.cd_setor_atendimento,
				a.cd_unidade_medida,
				a.cd_local_estoque,
				a.nr_prescricao,
				a.cd_cgc_fornecedor
			into	nr_atendimento_w,
				dt_entrada_unidade_w,
				cd_material_w,
				dt_atendimento_w,
				qt_material_w,
				cd_setor_atendimento_w,
				cd_unidade_medida_w,
				cd_local_estoque_w,
				nr_prescricao_w,
				cd_cgc_w
			from	material_atend_paciente a
			where	nr_sequencia = nr_seq_mat_p
			and	dt_atualizacao_estoque is not null;
			
			gerar_prescricao_estoque(	cd_estabelecimento_p,
							nr_atendimento_w,
							dt_entrada_unidade_w,
							cd_material_w,
							dt_atendimento_w,
							2,
							cd_local_estoque_w,
							qt_material_w,
							cd_setor_atendimento_w,
							cd_unidade_medida_w,
							nm_usuario_p,
							'I',
							nr_prescricao_w,
							0,
							0,
							0,
							cd_cgc_w,
							null,
							null,
							0,
							null,
							null,
							null,
							null,
							null);
		end if;
	end if;

elsif	(ie_status_acerto_w = 2) then

	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279080);

end if;

commit;

end excluir_mat_atend_pac_opme;
/