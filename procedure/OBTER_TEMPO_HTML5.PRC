create or replace
procedure obter_tempo_html5(
			ie_tipo_componente_p	varchar2,
			nm_tabela_p				varchar2,
			nr_seq_visao_p			number,
			cd_codigo_p				number,
			nr_seq_legenda_p		number,
			qt_colunas_grid_p	out number,
			qt_campos_detalhe_p	out number,
			qt_menu_itens_p		out number,
			qt_param_ativacao_p	out	number,
			qt_niveis_tree_p	out	number,
			qt_grupos_wscb_p	out number,
			ie_ativa_action_p	out	varchar2,
			ie_existe_regra_p	out	varchar2) is 

qt_colunas_grid_w 		number(10) := -1;
qt_campos_detalhe_w		number(10) := -1;
qt_menu_itens_w			number(10) := -1;
qt_param_ativacao_w		number(10) := -1;
qt_niveis_tree_w		number(10) := -1;
qt_grupos_wscb_w		number(10) := -1;
ie_ativa_action_w	   varchar2(1) := 'N';
ie_existe_regra_w	   varchar2(1) := 'N';
begin
if (ie_tipo_componente_p is not null) then
	begin
	if (ie_tipo_componente_p = 'WCPanel') then
		begin
		if (cd_codigo_p is not null) then
			begin
			if (cd_codigo_p > 0) then
				begin
				select	count(*)
				into	qt_colunas_grid_w
				from	dic_objeto
				where	nr_seq_obj_sup = cd_codigo_p;
				
				select	length(ds_sql) - length(replace(ds_sql,':',''))
				into	qt_param_ativacao_w
				from	dic_objeto
				where	nr_sequencia = cd_codigo_p;
				end;
			else
				begin
				qt_colunas_grid_w  	:= 0;
				qt_param_ativacao_w := 0;
				end;
			end if;				
			end;
		end if;
		end;
	elsif (ie_tipo_componente_p = 'WDBPanel') then
		begin
		if (nm_tabela_p is not null) then
			begin
			if (nr_seq_visao_p is not null) and
				(nr_seq_visao_p > 0) then
				begin
				select	count(*)
				into	qt_campos_detalhe_w
				from	tabela_visao_atributo
				where	nr_sequencia = nr_seq_visao_p
				and		nr_seq_apresent is not null
				and		(cd_exp_label is not null 
							or ds_label is not null);
				
				select	count(*)
				into	qt_colunas_grid_w
				from	tabela_visao_atributo
				where	nr_sequencia = nr_seq_visao_p
				and		(nr_seq_apresent is not null
							or nr_seq_grid is not null)
				and		(cd_exp_label_grid is not null
							or ds_label_grid is not null);
				
				select	length(ds_sql_ativacao) - length(replace(ds_sql_ativacao,':',''))
				into	qt_param_ativacao_w
				from	tabela_visao
				where	nr_sequencia = nr_seq_visao_p;
				end;
			else
				begin
				select	count(*)
				into	qt_campos_detalhe_w
				from	tabela_atributo
				where	nm_tabela = nm_tabela_p
				and		nr_seq_apresent is not null
				and		(cd_exp_label is not null 
							or ds_label is not null);
				
				select	count(*)
				into	qt_colunas_grid_w
				from	tabela_atributo
				where	nm_tabela = nm_tabela_p
				and		(nr_seq_apresent is not null
							or nr_seq_grid is not null)
				and		(cd_exp_label_grid is not null
							or ds_label_grid is not null);
				end;
			end if;
			end;
		end if;		
		end;
	elsif (ie_tipo_componente_p = 'WSelecaoCB') then
		begin
		if (cd_codigo_p is not null) then
			begin
			if (cd_codigo_p > 0) then
				begin
				select	count(*)
				into	qt_grupos_wscb_w
				from	dic_objeto
				where	nr_seq_obj_sup = cd_codigo_p
				and		ie_tipo_objeto = 'GCWSCB';
				
				select	length(ds_informacao) - length(replace(ds_informacao,':',''))
				into	qt_param_ativacao_w
				from	dic_objeto
				where	nr_sequencia = cd_codigo_p;
				
				select  qt_param_ativacao_w + count(*)
				into	qt_param_ativacao_w
				from	dic_objeto
				where	nr_seq_obj_sup = cd_codigo_p
				and		ie_tipo_objeto = 'ACCWSCB';
				end;
			else
				begin
				qt_grupos_wscb_w 	:= 0;
				qt_param_ativacao_w := 0;
				end;
			end if;
			
			end;
		end if;
		end;
	elsif (ie_tipo_componente_p = 'WJPopUpMenu') then
		begin
		if (cd_codigo_p is not null) then
			begin
			select	count(*)
			into	qt_menu_itens_w
			from	dic_objeto
			where	nr_seq_obj_sup = cd_codigo_p;
			end;
		end if;
		end;
	elsif (ie_tipo_componente_p = 'WLegenda') then
		begin
		if (nr_seq_legenda_p is not null) then
			begin
			select	decode(count(*),0,'N','S')
			into	ie_existe_regra_w
			from	regra_condicao
			where	nr_seq_legenda = nr_seq_legenda_p;
			
			select	count(*)
			into	qt_campos_detalhe_w			
			from	tasy_padrao_cor
			where	nr_seq_legenda = nr_seq_legenda_p;
			end;
		end if;
		end;
	elsif (ie_tipo_componente_p = 'WFiltro') then
		begin
		if (cd_codigo_p is not null) then
			begin
			select	count(*)
			into	qt_campos_detalhe_w
			from	dic_objeto_filtro
			where	nr_seq_objeto = cd_codigo_p;
			end;
		end if;
		end;
	elsif (ie_tipo_componente_p = 'WDetalhePanel') then
		begin
		if (cd_codigo_p is not null) then
			begin
			select	count(*)
			into	qt_campos_detalhe_w
			from	dic_objeto
			where	nr_seq_obj_sup = cd_codigo_p;
			
			select	length(ds_sql) - length(replace(ds_sql,':',''))
			into	qt_param_ativacao_w
			from	dic_objeto
			where	nr_sequencia = cd_codigo_p;
			end;
		end if;
		end;
	elsif 	(ie_tipo_componente_p = 'WJTreeNew') or
			(ie_tipo_componente_p = 'WJTree') then
		begin
		if (cd_codigo_p is not null) then
			begin
			select 	count(*)
			into	qt_niveis_tree_w					
			from 	dic_objeto
			where	nr_seq_obj_sup = cd_codigo_p;
			
			select	count(*)
			into	qt_param_ativacao_w
			from	dic_objeto
			where	nr_seq_obj_sup in (
					select 	x.nr_sequencia
					from	dic_objeto x
					where 	x.nr_seq_obj_sup = cd_codigo_p);
					
			if (qt_niveis_tree_w = 1) then
				begin
				select	length(ds_sql) - length(replace(upper(ds_sql),'@RESTRICAO',''))
				into	qt_campos_detalhe_w
				from	dic_objeto
				where	nr_seq_obj_sup = cd_codigo_p;
				
				if (qt_campos_detalhe_w > 0) then
					begin
					ie_ativa_action_w := 'S';
					end;
				end if;
				end;
			elsif (qt_niveis_tree_w = 0) then
				begin
				ie_ativa_action_w := 'S';
				end;
			end if;
			end;
		end if;
		end;	
	end if;
	end;
end if;

commit;

qt_colunas_grid_p		:= qt_colunas_grid_w;
qt_campos_detalhe_p		:= qt_campos_detalhe_w;
qt_menu_itens_p			:= qt_menu_itens_w;
qt_param_ativacao_p		:= qt_param_ativacao_w;
qt_niveis_tree_p		:= qt_niveis_tree_w;
ie_ativa_action_p		:= ie_ativa_action_w;
ie_existe_regra_p		:= ie_existe_regra_w;
qt_grupos_wscb_p		:= qt_grupos_wscb_w;

end obter_tempo_html5;
/