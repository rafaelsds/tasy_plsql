create or replace
procedure pls_liquidar_titulos_rescisao
			(	nr_seq_pagador_p	number,
				dt_rescisao_p		date,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_titulo_w			number(10);
vl_saldo_titulo_w		number(15,2);
ie_liquidar_titulos_rescisao_w	varchar2(1);
cd_tipo_receb_rescisao_w	number(5);
nr_seq_trans_fin_rescisao_w	number(10);
ie_gerar_cobr_rescisao_w	varchar2(1);
qt_titulo_cobranca_w		number(10);
nr_seq_cobranca_w		number(10);
dt_emissao_w			titulo_receber.dt_emissao%type;
dt_baixa_w			date;
nr_seq_baixa_w			titulo_receber_liq.nr_sequencia%type;

Cursor c01 is
	select	a.nr_titulo,
		a.vl_saldo_titulo,
		a.dt_emissao
	from	pls_mensalidade b,
		titulo_receber a
	where	a.nr_seq_mensalidade	= b.nr_sequencia
	and	b.nr_seq_pagador	= nr_seq_pagador_p
	and	a.dt_liquidacao is null
	and	(((ie_liquidar_titulos_rescisao_w = 'F') and (a.dt_pagamento_previsto > dt_rescisao_p)) or (ie_liquidar_titulos_rescisao_w <> 'F'));

begin

if	(nr_seq_pagador_p is not null) and
	(cd_estabelecimento_p is not null) and
	(dt_rescisao_p is not null) then
	select	nvl(max(ie_liquidar_titulos_rescisao),'N'),
		max(cd_tipo_receb_rescisao),
		max(nr_seq_trans_fin_rescisao),
		nvl(max(ie_gerar_cobr_rescisao),'N')
	into	ie_liquidar_titulos_rescisao_w,
		cd_tipo_receb_rescisao_w,
		nr_seq_trans_fin_rescisao_w,
		ie_gerar_cobr_rescisao_w
	from	pls_parametros_cr a
	where	a.cd_estabelecimento	= cd_estabelecimento_p;
	
	if	(ie_liquidar_titulos_rescisao_w in ('S','F')) then
		if	(cd_tipo_receb_rescisao_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(185661);
			--'N�o foi informado o tipo de recebimento para liquida��o dos t�tulos na rescis�o.' || chr(13) || chr(10) ||
			--				'Verifique em OPS - Gest�o de Operadoras -> Par�metros OPS -> Contas a Receber.#@#@');
		end if;
		
		if	(nr_seq_trans_fin_rescisao_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(185662);
			--'N�o foi informada a transa��o financeira para liquida��o dos t�tulos na rescis�o.' || chr(13) || chr(10) ||
			--				'Verifique em OPS - Gest�o de Operadoras -> Par�metros OPS -> Contas a Receber.#@#@');
		end if;
		
		open c01;
		loop
		fetch c01 into
			nr_titulo_w,
			vl_saldo_titulo_w,
			dt_emissao_w;
		exit when c01%notfound;
			begin
			if	(ie_gerar_cobr_rescisao_w = 'S') then
				select	count(*)
				into	qt_titulo_cobranca_w
				from	cobranca
				where	nr_titulo	= nr_titulo_w;
				
				if	(qt_titulo_cobranca_w = 0) then
					importar_cobranca(nr_titulo_w, 'N', nm_usuario_p, null);
					
					select	max(nr_sequencia)
					into	nr_seq_cobranca_w
					from	cobranca a
					where	nr_titulo	= nr_titulo_w;
					
					tratar_destino_cobranca(nr_seq_cobranca_w,null,cd_estabelecimento_p,nm_usuario_p);
				end if;
			end if;
			
			if	(dt_emissao_w > dt_rescisao_p) then
				dt_baixa_w	:= dt_emissao_w;
			else
				dt_baixa_w	:= dt_rescisao_p;
			end if;
			
			baixa_titulo_receber(cd_estabelecimento_p,
				cd_tipo_receb_rescisao_w,
				nr_titulo_w,
				nr_seq_trans_fin_rescisao_w,
				vl_saldo_titulo_w,
				dt_baixa_w,
				nm_usuario_p,
				null,
				null,
				null,
				0,
				0);
			
			select	max(nr_sequencia)
			into	nr_seq_baixa_w
			from	titulo_receber_liq
			where	nr_titulo	= nr_titulo_w;
			
			update	titulo_receber_liq			
			set	ie_baixa_rescisao_contrato	= 'S'
			where	nr_titulo	= nr_titulo_w
			and	nr_sequencia	= nr_seq_baixa_w;
			
			atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);
			end;
		end loop; 
		close c01;
	end if;
end if;

/* N�o pode ter commit */

end pls_liquidar_titulos_rescisao;
/