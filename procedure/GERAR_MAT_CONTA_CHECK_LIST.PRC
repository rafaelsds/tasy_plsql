create or replace
procedure Gerar_Mat_Conta_Check_List(
			nr_seq_check_list_p	number,
			nr_atendimento_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 

cd_material_w		number(6);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);	
nr_seq_Atepacu_w	number(10);
nr_seq_proc_pac_w	number(10);
cd_local_estoque_w	number(4);
cd_setor_atendimento_w	number(5);
	
cursor C01 is
	select	b.cd_material
	from	sl_item_check_list b,
		sl_check_list_unid_item a
	where	a.nr_seq_checklist		= nr_seq_check_list_p
	and	a.nr_seq_item_check_list	= b.nr_sequencia
	and	a.ie_resultado			= 'S'
	and	b.cd_material is not null;
					
begin

nr_seq_atepacu_w	:= obter_atepacu_paciente(nr_atendimento_p, 'A');

select	cd_setor_Atendimento
into	cd_setor_atendimento_w
from	atend_paciente_unidade
where	nr_seq_interno	= nr_seq_atepacu_w;

cd_local_estoque_w	:= obter_local_estoque_setor(cd_setor_atendimento_w, cd_estabelecimento_p);

select	nvl(max(cd_convenio),0),
	nvl(max(cd_categoria),0)
into	cd_convenio_w,
	cd_categoria_w
from 	Atend_categoria_convenio a
where	a.nr_atendimento		= nr_atendimento_p
and	a.dt_inicio_vigencia	= 
		(	select	max(dt_inicio_vigencia)
			from	Atend_categoria_convenio b
			where	nr_atendimento		= nr_atendimento_p);

open C01;
loop
fetch C01 into	
	cd_material_w;
exit when C01%notfound;
	begin
	Inserir_Mat_Conta_Check_List(
					nr_atendimento_p,
					cd_material_w,
					sysdate,
					cd_convenio_w,
					cd_categoria_w,
					nr_seq_atepacu_w,
					nm_usuario_p,
					1,
					cd_local_estoque_w,
					1,
					'N',
					nr_seq_proc_pac_w);
	
	atualiza_preco_material(nr_seq_proc_pac_w,nm_usuario_p);
	end;
end loop;
close C01;

commit;

end Gerar_Mat_Conta_Check_List;
/