create or replace
procedure pls_desfazer_prop_vinc_mig_ben(nr_seq_proposta_benef_p 	pls_proposta_beneficiario.nr_sequencia%type,
					   nm_usuario_p			Varchar2) is 

begin
update	pls_proposta_beneficiario
set	nr_seq_beneficiario = 	null,
	dt_atualizacao 		= sysdate,
	nm_usuario 		= nm_usuario_p
where	nr_sequencia		= nr_seq_proposta_benef_p;

commit;

end pls_desfazer_prop_vinc_mig_ben;
/