create or replace
procedure atualiza_contrato_lib_usuario is 


nr_seq_contrato_w	number(10);
nr_seq_contrato_atual_w	number(10);
cd_perfil_w		number(10);
cd_setor_w		number(10);
nm_usuario_lib_w	contrato_usuario_lib.nm_usuario_lib%type;
ie_permite_w		varchar2(1);

cursor c01 is
select	a.nr_sequencia,
	a.nr_seq_contrato_atual
from	contrato a
where	a.ie_classificacao in ('AD','ER')
and not exists(
	select	1
	from	contrato_usuario_lib x
	where	x.nr_seq_contrato = a.nr_sequencia);
	
	
cursor c02 is
select	cd_perfil,
	cd_setor,
	ie_permite,
	nm_usuario_lib
from	contrato_usuario_lib
where	nr_seq_contrato = nr_seq_contrato_atual_w
and	nvl(ie_geracao,'S') = 'S';

begin

open C01;
loop
fetch C01 into	
	nr_seq_contrato_w,
	nr_seq_contrato_atual_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		cd_perfil_w,
		cd_setor_w,
		ie_permite_w,
		nm_usuario_lib_w;
	exit when C02%notfound;
		begin
		
		insert into contrato_usuario_lib(
			nr_sequencia,
			nr_seq_contrato,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_lib,
			ie_aviso_vencimento,
			cd_perfil,
			cd_setor,
			ie_permite,
			ie_aviso_revisao,
			ie_geracao)
		values(	contrato_usuario_lib_seq.nextval,
			nr_seq_contrato_w,
			sysdate,
			'TasyImp',
			nm_usuario_lib_w,
			'N',
			cd_perfil_w,
			cd_setor_w,
			ie_permite_w,
			'N',
			'U');
		
		end;
	end loop;
	close C02;
	
	
	end;
end loop;
close C01;



commit;

end atualiza_contrato_lib_usuario;
/