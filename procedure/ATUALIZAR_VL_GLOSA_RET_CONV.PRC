create or replace
procedure atualizar_vl_glosa_ret_conv(	nr_seq_conv_ret_glosa_p	number,
					qt_glosa_p		number,
					vl_glosa_p		number,
					cd_motivo_glosa_p	number,
					ds_observacao_p		varchar2,
					vl_amaior_p		number,
					ie_tipo_p		number,
					nm_usuario_p		varchar2) is 

begin

if	(nr_seq_conv_ret_glosa_p is not null) and
	(qt_glosa_p is not null) and
	(vl_glosa_p is not null) then

begin

	if	(ie_tipo_p = 1) then	
	begin
		update	convenio_retorno_glosa
		set	qt_glosa	= qt_glosa + qt_glosa_p,
			vl_glosa	= vl_glosa_p,
			cd_motivo_glosa	= cd_motivo_glosa_p,
			ds_observacao	= ds_observacao_p
		where	nr_sequencia	= nr_seq_conv_ret_glosa_p;
	end;
	
	else if	(ie_tipo_p = 2) then
	begin	
		update	convenio_retorno_glosa
		set	qt_glosa	= qt_glosa + qt_glosa_p,
			vl_glosa	= 0,
			vl_amaior	= vl_amaior_p,
			cd_motivo_glosa	= cd_motivo_glosa_p,
			ds_observacao	= ds_observacao_p
		where	nr_sequencia	= nr_seq_conv_ret_glosa_p;
	end;
	
	else
	begin
		update	convenio_retorno_glosa
		set	vl_glosa	= vl_glosa_p,
			cd_motivo_glosa	= cd_motivo_glosa_p
		where	nr_sequencia	= nr_seq_conv_ret_glosa_p;
	end;
	
	end if;
	
	end if;

	commit;

end;

end if;

end atualizar_vl_glosa_ret_conv;
/