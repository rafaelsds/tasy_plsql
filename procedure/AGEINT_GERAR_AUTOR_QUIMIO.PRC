create or replace
procedure Ageint_Gerar_Autor_Quimio(	nr_sequencia_p in number,
				ie_tipo_agendamento_p in varchar2,
				nm_usuario_p		varchar2) is
				   
qt_agendamento_w	number(10,0);
nr_sequencia_w		number(10,0);
ie_tipo_agendamento_w	varchar2(15);
nr_Seq_pend_quimio_w	number(10);
nr_Seq_paciente_w	number(10);
nr_ciclo_w		number(3);

Cursor c01 is
	select	nr_Seq_pend_quimio
	from	agenda_integrada_item
	where	nr_seq_agenda_int	= nr_Sequencia_p
	and	nvl(ie_tipo_agendamento,'X')	= 'Q'
	and	nvl(ie_tipo_pend_Agenda,'X')	= 'Q';
				   

begin

open C01;
loop
fetch C01 into	
	nr_Seq_pend_quimio_w;
exit when C01%notfound;
	begin
	
	select	min(nr_seq_paciente),
		min(nr_ciclo)
	into	nr_Seq_paciente_w,
		nr_ciclo_W
	from	paciente_Atendimento
	where	nr_seq_pend_agenda	= nr_Seq_pend_quimio_w
	and 	dt_cancelamento is null;
	if	(nr_Seq_paciente_w is not null) then
		GERAR_AUTOR_CONV_QUIMIOTERAPIA(nr_seq_paciente_w, nr_ciclo_W, nm_usuario_p,'S');
	end if;
	
	end;
end loop;
close C01;
 
	
end Ageint_Gerar_Autor_Quimio;
/