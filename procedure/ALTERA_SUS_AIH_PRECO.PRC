create or replace
procedure altera_sus_aih_preco(
			dt_competencia_sus_p		date,
			ie_opcao_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar dados AIH para a tabela sus_preco
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

if	(cd_estabelecimento_p is not null) then
	if	(upper(ie_opcao_p) = 'N')then
		update	parametro_faturamento 
		set		dt_competencia_sus   = dt_competencia_sus_p 
		where	cd_estabelecimento   = cd_estabelecimento_p;
	else
		update	parametro_faturamento 
		set		dt_competencia_sus   = dt_competencia_sus_p ;
	end if;
	commit;
end if;

end altera_sus_aih_preco;
/