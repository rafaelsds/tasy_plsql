create or replace 
procedure slot_overlap_services(nr_minuto_duracao_p	number,
				dt_agenda_p		date,
				cd_agenda_p		number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number,
				nr_seq_agenda_p		number,
				ds_exist_patient_detail_p out varchar2) is 

nr_minuto_duracao_w		number(10,0);				      
hr_param_w			date;
qt_agendado_w			number(10,0);
ie_sobreposicao_encaixe_w	varchar2(1) := 'S';
nm_pessoa_fisica_w 		varchar2(255);
dt_agenda_w			varchar2(255);


cursor c01 is
	select	obter_nome_pf(a.cd_pessoa_fisica),
		date_as_varchar2(a.dt_agenda, 'shortTime')
	from	agenda_consulta a
	where	cd_agenda = cd_agenda_p
	and    dt_agenda > dt_agenda_p and  dt_agenda <= hr_param_w - 1/1440
	and	((ie_sobreposicao_encaixe_w = 'N' and nvl(ie_encaixe, 'N') <> 'S' ) or ie_sobreposicao_encaixe_w = 'S')
	and 	(ie_status_agenda not in ('C','L','B','F','I','II'))
	and 	nr_sequencia <> nr_seq_agenda_p
	order by 1;

begin

select	nvl(max(ie_sobreposicao_encaixe),'S')
into	ie_sobreposicao_encaixe_w
from	parametro_agenda
where	cd_estabelecimento = cd_estabelecimento_p;

hr_param_w := dt_agenda_p + nr_minuto_duracao_p / 1440;

select	count(*)
into	qt_agendado_w
from	agenda_consulta
where	cd_agenda = cd_agenda_p
and    dt_agenda > dt_agenda_p and  dt_agenda <= hr_param_w - 1/1440
and	((ie_sobreposicao_encaixe_w = 'N' and nvl(ie_encaixe, 'N') <> 'S' ) or ie_sobreposicao_encaixe_w = 'S')
and 	(ie_status_agenda not in ('C','L','B','F','I','II'))
AND 	nr_sequencia <> nr_seq_agenda_p;

if	(qt_agendado_w > 0) then
        open C01;
	loop
	fetch C01 into  
		nm_pessoa_fisica_w,
		dt_agenda_w;
	exit when C01%notfound;
		begin
		
		if	(nm_pessoa_fisica_w is not null) then
			ds_exist_patient_detail_p := nm_pessoa_fisica_w || ' - ' || dt_agenda_w || chr(10) || ds_exist_patient_detail_p;
		end if;
		
		end;
	end loop;
        close C01;
end if;

end slot_overlap_services;
/