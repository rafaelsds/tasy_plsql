create or replace
procedure ehr_template_cont_AfterScroll(	nr_seq_elemento_p	number,
						ie_cliente_cadastra_p	out varchar2,
						ie_tipo_item_p	out varchar2,
						ie_origem_inf_p		out varchar2,
						nr_seq_entidade_p	out number) is

ie_cliente_cadastra_w		varchar2(1)		:= 'N';
ie_tipo_item_w				varchar2(15)	:= '';
ie_origem_inf_w				varchar2(15)	:= '';
nr_seq_entidade_w			number(10);

BEGIN

if	(nr_seq_elemento_p is not null) then
	begin
	
	select	nvl(UPPER(ie_cliente_cadastra),''),
			nvl(UPPER(ie_tipo_item),'')
	into	ie_cliente_cadastra_w,
			ie_tipo_item_w
	from	ehr_elemento
	where	nr_sequencia = nr_seq_elemento_p;

	ie_origem_inf_w		:= ehr_obter_inf_elemento(nr_seq_elemento_p, 'IO');

	nr_seq_entidade_w	:= to_number(ehr_obter_inf_elemento(nr_seq_elemento_p, 'EN'));


	end;
end if;


ie_cliente_cadastra_p	:= ie_cliente_cadastra_w;
ie_tipo_item_p			:= ie_tipo_item_w;
ie_origem_inf_p		:= ie_origem_inf_w;
nr_seq_entidade_p	:= nr_seq_entidade_w;


END ehr_template_cont_AfterScroll;
/
