create or replace
procedure pls_delete_monit_disc( nr_seq_lote_p		pls_lote_discussao.nr_sequencia%type,
				nm_usuario_p		Varchar2) is 

Cursor C01 (nr_seq_lote_pc	pls_lote_discussao.nr_sequencia%type) is
	select	nr_sequencia,
		pls_obter_dados_contestacao(a.nr_seq_contestacao,'NC') nr_seq_conta
	from	pls_contestacao_discussao	a
	where	nr_seq_lote	= nr_seq_lote_pc;
	
begin

for r_c01_w in C01(nr_seq_lote_p) loop
	begin
		delete 	pls_monitor_tiss_alt
		where	nr_seq_conta_disc 	= r_c01_w.nr_sequencia
		and	ie_tipo_evento		= 'PD';
	end;
end loop;


end pls_delete_monit_disc;
/