create or replace
procedure reg_reincl_item_test_plan (	nr_seq_ignored_prs_p		number,
					nm_usuario_p			varchar2) is 

begin

reg_test_plan_pck.reinclude_item_in_plan(nr_seq_ignored_prs_p, nm_usuario_p);

end reg_reincl_item_test_plan;
/