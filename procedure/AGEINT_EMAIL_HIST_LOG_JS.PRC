create or replace
procedure ageint_email_hist_log_js(
		nr_seq_ageint_p		number,
		ds_email_p		clob,
		ds_destinatario_p	varchar2,
		ds_assunto_p		varchar2,
		nm_usuario_p		varchar2,
		ie_tipo_email_p		varchar2,
		ie_formatado_p		varchar2,
		ds_email_cc_p		varchar2,
		ie_html_p           varchar2 default 'N') is
begin
	if (ie_formatado_p in ('S','E')) then
		ageint_gerar_hist_log_email(nr_seq_ageint_p, ds_destinatario_p, ds_assunto_p, nm_usuario_p, ie_tipo_email_p, ds_email_cc_p, ds_email_p, ie_html_p);
	else
		ageint_gerar_hist_email(nr_seq_ageint_p,ds_email_p,ds_destinatario_p,ds_assunto_p,nm_usuario_p,ie_tipo_email_p);
		ageint_gravar_log_email(nr_seq_ageint_p,ds_email_p,ds_destinatario_p,ds_assunto_p,nm_usuario_p,ie_formatado_p, 'A', ds_email_cc_p);
	end if;
commit;
end ageint_email_hist_log_js;
/
