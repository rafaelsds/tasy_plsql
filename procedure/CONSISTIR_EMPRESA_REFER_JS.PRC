create or replace
procedure consistir_empresa_refer_js(	cd_empresa_p	number,
					cd_convenio_p	number,
					ds_msg_aviso_p	out varchar2) is 

qt_empresa_referencia_w	number(10,0);
ds_empresa_w		varchar2(100);

begin

select 	nvl(count(*),0)
into	qt_empresa_referencia_w
from   	empresa_referencia_conv
where  	cd_empr_ref = cd_empresa_p
and    	cd_convenio is not null;

if	(qt_empresa_referencia_w > 0)then
	begin
	
	select 	nvl(count(*),0)
	into	qt_empresa_referencia_w
	from   	empresa_referencia_conv
	where  	cd_empr_ref = cd_empresa_p
	and    	cd_convenio = cd_convenio_p;
	
	if	(qt_empresa_referencia_w = 0)then
		begin
		
		ds_empresa_w	:= obter_nome_empresa_ref(cd_empresa_p);
		ds_msg_aviso_p	:= wheb_mensagem_pck.get_texto(313584,'NM_EMPRESA_CONV='||ds_empresa_w);
		
		end;
	end if;
	
	end;
end if;

commit;

end consistir_empresa_refer_js;
/