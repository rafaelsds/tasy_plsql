create or replace
procedure SAN_CONSISTE_REGRA_IDADE( 	cd_pessoa_fisica_p	varchar2,
					cd_estabelecimento_p	number,
					ds_mensagem_p out	varchar2,
					ie_acao_p out		varchar2) is 

nr_idade_doador_w	number;
nr_idade_min_w		number;
nr_idade_max_w		number;
ie_acao_w		varchar2(1);

begin
if	(cd_pessoa_fisica_p is not null) then
	select  max(to_number(substr(obter_idade(dt_nascimento,sysdate,'A'),1,50)))
	into	nr_idade_doador_w
	from    pessoa_fisica
	where   cd_pessoa_Fisica = cd_pessoa_Fisica_p;

	select	max(nr_idade_min),
		max(nr_idade_max),
		max(ie_acao)
	into	nr_idade_min_w,
		nr_idade_max_w,
		ie_acao_w
	from 	san_regra_idade
	where	nr_sequencia = ( select max(nr_sequencia)
				from	san_regra_idade
				where 	ie_situacao = 'A'
				and	cd_estabelecimento = cd_estabelecimento_p);
	
	
	if (nr_idade_min_w is not null) and (nr_idade_max_w is not null) and (ie_acao_w is not null) then
		if (nr_idade_doador_w < nr_idade_min_w) then
			if (ie_acao_w = 'A') then
				ds_mensagem_p := wheb_mensagem_pck.get_texto(309906, 'NR_IDADE_MIN_W=' || to_char(nr_idade_min_w)); -- Idade inferior a #@NR_IDADE_MIN_W#@ anos.
				ie_acao_p := 'A';
			elsif (ie_acao_w = 'Q') then
				ds_mensagem_p := wheb_mensagem_pck.get_texto(309906, 'NR_IDADE_MIN_W=' || to_char(nr_idade_min_w)) || ' ' || wheb_mensagem_pck.get_texto(309910); -- Idade inferior a #@NR_IDADE_MIN_W#@ anos. Deseja continuar? 
				ie_acao_p := 'Q';
			elsif (ie_acao_w = 'B') then
				ds_mensagem_p := wheb_mensagem_pck.get_texto(309906, 'NR_IDADE_MIN_W=' || to_char(nr_idade_min_w)); -- Idade inferior a #@NR_IDADE_MIN_W#@ anos.
				ie_acao_p := 'B';		
			end if;
		elsif (nr_idade_doador_w > nr_idade_max_w) then	
			if (ie_acao_w = 'A') then
				ds_mensagem_p := wheb_mensagem_pck.get_texto(309914, 'NR_IDADE_MAX_W='|| to_char(nr_idade_max_w)); -- Idade superior a #@NR_IDADE_MAX_W#@ anos.
				ie_acao_p := 'A';
			elsif (ie_acao_w = 'Q') then
				ds_mensagem_p := wheb_mensagem_pck.get_texto(309914, 'NR_IDADE_MAX_W='|| to_char(nr_idade_max_w)) || ' ' || wheb_mensagem_pck.get_texto(309910); --  Idade superior a #@NR_IDADE_MAX_W#@ anos.
				ie_acao_p := 'Q';
			elsif (ie_acao_w = 'B') then
				ds_mensagem_p := wheb_mensagem_pck.get_texto(309914, 'NR_IDADE_MAX_W='|| to_char(nr_idade_max_w)); -- Idade superior a #@NR_IDADE_MAX_W#@ anos.
				ie_acao_p := 'B';	
			end if;
		end if;
	else
		if (nr_idade_doador_w < 18) then
			ds_mensagem_p := wheb_mensagem_pck.get_texto(309906, 'NR_IDADE_MIN_W=18') || ' ' || wheb_mensagem_pck.get_texto(309910); -- Idade inferior a #@NR_IDADE_MIN_W#@ anos. Deseja continuar? 
			ie_acao_p := 'N';	
		elsif (nr_idade_doador_w > 65) then
			ds_mensagem_p := wheb_mensagem_pck.get_texto(309914, 'NR_IDADE_MAX_W=65') || ' ' || wheb_mensagem_pck.get_texto(309910); -- Idade superior a #@NR_IDADE_MAX_W#@ anos. Deseja continuar? 
			ie_acao_p := 'N';
		end if;
	end if;	
			
	
end if;
commit;

end san_consiste_regra_idade;
/
