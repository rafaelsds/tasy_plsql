create or replace
procedure contabiliza_nota_credito
                        (       nr_lote_contabil_p      in      number,
                                ie_exclusao_p           in      varchar2,
                                ds_retorno_p            out     varchar2,
                                nm_usuario_p            in      varchar2) is

ds_conteudo_w                   varchar2(4000);
nr_titulo_orig_cr_w             varchar2(255);
nm_pessoa_nota_credito_w        varchar2(255);
ds_origem_w                     varchar2(120);
nr_titulo_origem_hist_guia_w    varchar2(20);
cd_conta_contabil_w             varchar2(20);
nr_documento_w                  varchar2(20);
cd_pessoa_nota_credito_w        varchar2(14);
vl_transacao_w                  number(15,2);
nr_seq_conta_banco_w            number(10);
cd_estabelecimento_w            number(10);
cd_tipo_lote_contabil_w         number(10);
dt_contabil_inicial_w           date;
dt_contabil_final_w             date;
dt_referencia_w                 date;
nr_seq_movto_w                  number(10);
nr_documento_ww                 movimento_contabil.nr_documento%type;
ie_origem_documento_w           movimento_contabil.ie_origem_documento%type;
ie_regra_w                      varchar2(255);
ds_atributos_w                  varchar2(4000);
nm_agrupador_w                  varchar2(255);
nr_seq_agrupamento_w            w_movimento_contabil.nr_seq_agrupamento%type;
dt_cancelamento_w               nota_credito.dt_cancelamento%type;
dt_movimento_w                  date;

cursor c01 is
        select  a.nr_sequencia,
                a.dt_nota_credito,
                a.dt_vencimento,
                nvl(nvl(b.vl_classificacao,a.vl_nota_credito),0) vl_transacao,
                'VL_NOTA_CREDITO' nm_atributo,
                b.cd_centro_custo,
                b.cd_conta_contabil,
                a.cd_cgc,
                a.cd_pessoa_fisica,
                a.nr_seq_trans_fin_contab,
                a.ie_origem,
                a.nr_seq_lote_audit_hist,
                a.nr_seq_lote_hist_guia,
                a.nr_seq_motivo,
                a.ds_observacao,
                a.nr_seq_conta_banco,
                a.nr_titulo_receber,
                nr_seq_credito_n_ident,
                a.dt_cancelamento
        from    nota_credito a,
                nota_credito_classif b
        where   a.nr_sequencia          = b.nr_seq_nota_credito(+)
        and     a.cd_estabelecimento    = cd_estabelecimento_w
        and     a.nr_lote_contabil      = nr_lote_contabil_p
        union all
        select  a.nr_sequencia,
                a.dt_nota_credito,
                a.dt_vencimento,
                nvl(nvl(b.vl_classificacao,a.vl_nota_credito),0) * -1 vl_transacao,
                'VL_NOTA_CREDITO' nm_atributo,
                b.cd_centro_custo,
                b.cd_conta_contabil,
                a.cd_cgc,
                a.cd_pessoa_fisica,
                a.nr_seq_trans_fin_contab,
                a.ie_origem,
                a.nr_seq_lote_audit_hist,
                a.nr_seq_lote_hist_guia,
                a.nr_seq_motivo,
                a.ds_observacao,
                a.nr_seq_conta_banco,
                a.nr_titulo_receber,
                nr_seq_credito_n_ident,
                a.dt_cancelamento
        from    nota_credito a,
                nota_credito_classif b
        where   a.nr_sequencia          = b.nr_seq_nota_credito(+)
        and     a.cd_estabelecimento    = cd_estabelecimento_w
        and     a.nr_lote_contabil_est  = nr_lote_contabil_p
        and     a.dt_cancelamento       is not null;

vet01   c01%rowtype;

begin

delete
from    lote_contabil_log
where   nr_lote_contabil = nr_lote_contabil_p
and     cd_log_lote in (9,10);

commit;

select  a.cd_estabelecimento,
        a.dt_referencia,
        a.cd_tipo_lote_contabil
into    cd_estabelecimento_w,
        dt_referencia_w,
        cd_tipo_lote_contabil_w
from    lote_contabil a
where   a.nr_lote_contabil      = nr_lote_contabil_p;

dt_contabil_inicial_w   := trunc(dt_referencia_w,'month');
dt_contabil_final_w     := fim_dia(dt_referencia_w);

if      (ie_exclusao_p = 'S') then
        --exec_sql_dinamico(nm_usuario_p,'truncate table w_movimento_contabil');
        delete  w_movimento_contabil
        where   nr_lote_contabil = nr_lote_contabil_p;

        delete  movimento_contabil
        where   nr_lote_contabil        = nr_lote_contabil_p;

        update  lote_contabil
        set     vl_credito      = 0,
                vl_debito       = 0
        where   nr_lote_contabil        = nr_lote_contabil_p;

        update  nota_credito
        set     nr_lote_contabil        = null
        where   nr_lote_contabil        = nr_lote_contabil_p
        and     dt_cancelamento         is null;

        update  nota_credito
        set     nr_lote_contabil_est    = null
        where   nr_lote_contabil_est    = nr_lote_contabil_p
        and     dt_cancelamento         is not null;

else
        update  nota_credito
        set     nr_lote_contabil        = nr_lote_contabil_p
        where   cd_estabelecimento      = cd_estabelecimento_w
        and     dt_nota_credito between dt_contabil_inicial_w and dt_contabil_final_w
        and     nr_lote_contabil        is null
        and     dt_cancelamento         is null;

        update  nota_credito
        set     nr_lote_contabil_est    = nr_lote_contabil_p
        where   cd_estabelecimento      = cd_estabelecimento_w
        and     dt_cancelamento between dt_contabil_inicial_w and dt_contabil_final_w
        and     nr_lote_contabil        is not null
        and     nr_lote_contabil_est    is null;

        nm_agrupador_w  := nvl(trim(obter_agrupador_contabil(cd_tipo_lote_contabil_w)),'NR_SEQ_NOTA_CREDITO');

        open c01;
        loop
        fetch c01 into
                vet01;
        exit when c01%notfound;
                vl_transacao_w                          := vet01.vl_transacao;
                nr_documento_w                          := vet01.nr_sequencia;
                ds_origem_w                                     := substr(obter_valor_dominio(3346, vet01.ie_origem),1,120);
                cd_pessoa_nota_credito_w        := substr(nvl(vet01.cd_pessoa_fisica, vet01.cd_cgc),1,14);
                nm_pessoa_nota_credito_w        := substr(obter_nome_pf_pj(vet01.cd_pessoa_fisica, vet01.cd_cgc),1,255);
                nr_seq_conta_banco_w            := vet01.nr_seq_conta_banco;
                dt_movimento_w                  := vet01.dt_nota_credito;
                if      (vet01.dt_cancelamento is not null) and
                        (trunc(vet01.dt_cancelamento,'dd') <> trunc(vet01.dt_nota_credito,'dd')) then
                        dt_movimento_w  := vet01.dt_cancelamento;
                end if;


                if      (nm_agrupador_w = 'NR_SEQ_NOTA_CREDITO')then
                        nr_seq_agrupamento_w    :=      vet01.nr_sequencia;
                end if;

                if      (nvl(nr_seq_agrupamento_w,0) = 0)then
                        nr_seq_agrupamento_w    :=      vet01.nr_sequencia;
                end if;

                if      (nvl(vet01.nr_seq_lote_hist_guia,0) <> 0) then
                        select  max(somente_numero(obter_titulo_conta_guia(nr_interno_conta,cd_autorizacao,null,null)))
                        into    nr_titulo_origem_hist_guia_w
                        from    lote_audit_hist_guia
                        where   nr_sequencia    = vet01.nr_seq_lote_hist_guia;
                end if;

                nr_titulo_orig_cr_w     := substr(obter_titulo_orig_cr(vet01.nr_sequencia),1,255);

                if      (nvl(nr_titulo_orig_cr_w,'X') = 'X') then
                        nr_titulo_orig_cr_w := vet01.nr_titulo_receber;
                end if;

                ds_conteudo_w           := substr(      vet01.nr_sequencia              || '#@' ||
                                                        cd_pessoa_nota_credito_w        || '#@' ||
                                                        nm_pessoa_nota_credito_w        || '#@' ||
                                                        ds_origem_w                     || '#@' ||
                                                        nr_titulo_origem_hist_guia_w    || '#@' ||
                                                        nr_titulo_orig_cr_w             || '#@' ||
                                                        vet01.nr_seq_credito_n_ident,1,4000);

                ds_atributos_w  := null;

                ds_atributos_w  :=      'NR_TITULO_RECEBER=' || vet01.nr_titulo_receber;

                ctb_obter_doc_movto(    cd_tipo_lote_contabil_w,
                                        vet01.nm_atributo,
                                        'VR',
                                        vet01.dt_nota_credito,
                                        null,
                                        null,
                                        ds_atributos_w,
                                        nm_usuario_p,
                                        ie_regra_w,
                                        nr_documento_ww,
                                        ie_origem_documento_w);

                gerar_contab_trans_financ(      cd_estabelecimento_w,
                                                cd_estabelecimento_w,
                                                nr_lote_contabil_p,
                                                nm_usuario_p,
                                                vet01.cd_conta_contabil,
                                                vet01.cd_centro_custo,
                                                nr_documento_w,
                                                nr_seq_agrupamento_w,
                                                dt_movimento_w,
                                                vl_transacao_w,
                                                vet01.nr_seq_trans_fin_contab,
                                                nr_seq_conta_banco_w,
                                                vet01.nm_atributo,
                                                vet01.cd_pessoa_fisica,
                                                vet01.cd_cgc,
                                                null,
                                                null,
                                                nr_documento_ww,
                                                null,
                                                ds_conteudo_w,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                nr_seq_movto_w,
                                                null,
                                                null,
                                                null,
                                                ie_origem_documento_w);
        end loop;
        close c01;

        agrupa_movimento_contabil(      nr_lote_contabil_p,
                                        nm_usuario_p);
end if;

if      (ds_retorno_p is null) then
        update  lote_contabil
        set     ie_situacao = 'A',
                dt_geracao_lote = sysdate
        where   nr_lote_contabil = nr_lote_contabil_p;
        if  (ie_exclusao_p = 'S') then
                ds_retorno_p            := wheb_mensagem_pck.get_texto(298780);

                ctb_gravar_log_lote( nr_lote_contabil_p, 2, '', nm_usuario_p);
        else
                ds_retorno_p            := wheb_mensagem_pck.get_texto(298781);

                ctb_gravar_log_lote( nr_lote_contabil_p, 1, '', nm_usuario_p);
        end if;
        commit;
else
        rollback;
end if;

commit;

end contabiliza_nota_credito;
/