create or replace procedure efetivar_importacao_materiais(
    nr_seq_inventario_p number,
    nm_usuario_p varchar2) is

nr_seq_item_w inventario_material.nr_sequencia%type;
cd_material_w inventario_material.cd_material%type;
nr_seq_lote_w inventario_material_lote.nr_seq_lote_fornec%type;

cursor C01 is
    select  cd_material, 
            nr_seq_lote
    from    importar_material
    where   nr_seq_inventario = nr_seq_inventario_p;
    
cursor C02 is
    select  a.nr_sequencia,
            b.nr_seq_lote
    from    inventario_material a,
            importar_material b
    where   a.nr_seq_inventario = nr_seq_inventario_p
    and     a.nr_seq_inventario = b.nr_seq_inventario
    and     a.cd_material = b.cd_material;
    
begin
    --importa os materiais
    open C01;
    loop
    fetch C01 into cd_material_w, nr_seq_lote_w;
    exit when C01%NotFound;
        begin
            insert into inventario_material (nr_sequencia, nr_seq_inventario , cd_material , dt_atualizacao, nm_usuario)
            values (inventario_material_seq.nextval, nr_seq_inventario_p , cd_material_w, sysdate, nm_usuario_p);
        end;
    end loop;
    close C01;
    
    --importa os lotes
    open C02;
    loop
    fetch C02 into nr_seq_item_w, nr_seq_lote_w;
    exit when C02%NotFound;
        begin
            insert into inventario_material_lote (nr_sequencia, nr_seq_inventario, nr_seq_item, nr_seq_lote_fornec, dt_atualizacao, nm_usuario)
            values (inventario_material_lote_seq.nextval, nr_seq_inventario_p, nr_seq_item_w, nr_seq_lote_w, sysdate, nm_usuario_p);
        end;
    end loop;
    close C01;
    
    commit;

end efetivar_importacao_materiais;
/