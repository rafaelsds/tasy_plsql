create or replace procedure REMOVER_DADOS_QB_DAR(nr_seq_qb_dados_p number) is
begin

	DELETE 
	FROM QUERY_BUILDER_DISPLAY qbdi
	WHERE EXISTS (
				select
					1 
				from
					query_builder_dados qbda 
				where
					qbdi.nm_tabela = qbda.nm_tabela
					and qbdi.cd_funcao = qbda.cd_funcao 
					and qbdi.nr_seq_objeto_schematic = qbda.nr_seq_objeto_schematic 
					and qbda.nr_sequencia = nr_seq_qb_dados_p
	);
    
    DELETE 
	FROM DAR_QUERY_CONDITIONS dqc
	WHERE EXISTS (
				select
					1 
				from
					query_builder_dados qbda 
				where
					dqc.nm_tabela = qbda.nm_tabela
					and dqc.cd_funcao = qbda.cd_funcao 
					and dqc.nr_seq_objeto_schematic = qbda.nr_seq_objeto_schematic 
					and qbda.nr_sequencia = nr_seq_qb_dados_p
	);
    
    DELETE 
	FROM DAR_QUERY_FILTER_DATE dqfd
	WHERE EXISTS (
				select
					1 
				from
					query_builder_dados qbda 
				where
					dqfd.nm_tabela = qbda.nm_tabela
					and dqfd.cd_funcao = qbda.cd_funcao 
					and dqfd.nr_seq_objeto_schematic = qbda.nr_seq_objeto_schematic 
					and qbda.nr_sequencia = nr_seq_qb_dados_p
	);
    
    DELETE FROM QUERY_BUILDER_DADOS WHERE NR_SEQUENCIA = nr_seq_qb_dados_p;

    commit;
end REMOVER_DADOS_QB_DAR;
/
