CREATE OR REPLACE procedure HMSL_IMPORTA_ORDEM_COMPRA_ENT is


qt_existe_w			number(10);

ds_observacao_w			varchar2(255);
dt_cancelamento_w		date;
dt_confirma_entrega_w		date;
dt_entrega_limite_w		date;
dt_entrega_original_w		date;
dt_prevista_entrega_w		date;
dt_real_entrega_w		date;
nr_ordem_compra_w		number(10);
nr_ordem_compra_ww		number(10);
nr_item_oc_ent_w		number(10);
qt_prevista_entrega_w		number(13,4);
qt_prevista_entrega_ww		number(13,4);
qt_real_entrega_w		number(13,4);
cd_material_w			number(10);


cursor c01 is
select	SUBSTR(DS_OBSERVACAO,1,255),
	DT_CANCELAMENTO,
	DT_CONFIRMA_ENTREGA,
	DT_ENTREGA_LIMITE,
	DT_ENTREGA_ORIGINAL,
	DT_PREVISTA_ENTREGA,
	DT_REAL_ENTREGA,
	nvl(NR_ORDEM_COMPRA,0),
NR_ITEM_OCI,
	nvl(QT_PREVISTA_ENTREGA,0),
	QT_REAL_ENTREGA,
	CD_MATERIAL
FROM	OC_ENTREGA_HMSL a
where	not exists(	select	1
			from	hmsl_log_importacao_ordem x
			where	x.nr_ordem_compra = a.nr_ordem_compra);
begin

delete from hmsl_log_importacao_ordem_ENT;

open C01;
loop
fetch C01 into
	ds_observacao_w,
	dt_cancelamento_w,
	dt_confirma_entrega_w,
	dt_entrega_limite_w,
	dt_entrega_original_w,
	dt_prevista_entrega_w,
	dt_real_entrega_w,
	nr_ordem_compra_w,
	nr_item_oc_ent_w,
	qt_prevista_entrega_w,
	qt_real_entrega_w,
	cd_material_w;
exit when C01%notfound;
	begin

	nr_ordem_compra_ww := nr_ordem_compra_w;

	select	max(nr_ordem_compra)
	into	nr_ordem_compra_w
	from	ordem_compra
	where	nr_documento_externo = nr_ordem_compra_ww;

	if	(nvl(nr_ordem_compra_ww,0) = 0) then
		insert into hmsl_log_importacao_ordem_ent(nr_ordem_compra, cd_material, ds_log) values (null,cd_material_w,'N�o possui numero da ordem de compra. Campo obrigat�rio');
	else
		select	count(*)
		into	qt_existe_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_w;

		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_ent(nr_ordem_compra, cd_material, ds_log) values (nr_ordem_compra_ww,cd_material_w,'Esta OC n�o existe no Tasy ' || nr_ordem_compra_w);
		else
			select	count(*)
			into	qt_existe_w
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_w
			and	cd_material = cd_material_w;

			if	(qt_existe_w =0) then
				insert into hmsl_log_importacao_ordem_ent(nr_ordem_compra, cd_material, ds_log) values (nr_ordem_compra_ww,cd_material_w, 'N�o existe na ordem de compra ' || nr_ordem_compra_w || ' o material ' || cd_material_w);
			end if;
		end if;
	end if;

	if	(dt_prevista_entrega_w is null) then
		insert into hmsl_log_importacao_ordem_ent(nr_ordem_compra, cd_material, ds_log) values (nr_ordem_compra_ww,cd_material_w,'N�o possui data prevista de entrega informado. Campo obrigat�rio');
	end if;

	commit;

	select	count(*)
	into	qt_existe_w
	from	hmsl_log_importacao_ordem_ent
	where	nr_ordem_compra = nvl(nr_ordem_compra_ww,0)
	and	cd_material = cd_material_w;

	if	(qt_existe_w = 0) and
		(nvl(nr_ordem_compra_w,0) > 0) then

		select	max(nr_item_oci)
		into	nr_item_oc_ent_w
		from	ordem_compra_item
		where	cd_material = cd_material_w
		and	nr_ordem_compra = nr_ordem_compra_w;
		
		qt_prevista_entrega_ww := nvl(qt_prevista_entrega_w,0);

		select	nvl(max(qt_material),0)
		into	qt_prevista_entrega_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w
		and	nr_item_oci = nr_item_oc_ent_w;

		qt_real_entrega_w := (qt_prevista_entrega_w - qt_prevista_entrega_ww);


		insert into ordem_compra_item_entrega(
			nr_ordem_compra,
			nr_item_oci,
			dt_prevista_entrega,
			dt_real_entrega,
			qt_prevista_entrega,
			qt_real_entrega,
			dt_atualizacao,
			nm_usuario,
			ds_observacao,
			dt_cancelamento,
			nr_sequencia,
			dt_entrega_original,
			dt_entrega_limite,
			dt_confirma_entrega)
		values(	nr_ordem_compra_w,
			nr_item_oc_ent_w,
			dt_prevista_entrega_w,
			dt_real_entrega_w,
			qt_prevista_entrega_w,
			qt_real_entrega_w,
			sysdate,
			'Importacao',
			ds_observacao_w,
			dt_cancelamento_w,
			ordem_compra_item_entrega_seq.nextval,
			dt_entrega_original_w,
			dt_entrega_limite_w,
			dt_confirma_entrega_w);
	end if;
	end;
end loop;
close C01;

commit;

end HMSL_IMPORTA_ORDEM_COMPRA_ENT;
/