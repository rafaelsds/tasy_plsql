create or replace
procedure ai_gerar_necess_vaga_texto(
		nr_seq_ageint_p		number,
		ie_gerar_necessidade_p	varchar2,
		ie_chamar_necessidade_p	varchar2,
		ie_enviar_p		out varchar2,
		ds_email_checklist_p	out varchar2,
		ie_tipo_agendamento_p 	out varchar2,
		nr_seq_agenda_p       	out number) is
		
ie_tipo_agendamento_w	varchar2(15);
nr_seq_agenda_w		number(10);
ie_enviar_w		varchar2(1);
ds_email_checklist_w	varchar2(5000);

begin

	if	(ie_gerar_necessidade_p = 'S') and
		(ie_chamar_necessidade_p = 'S') then
		begin
		ageint_gerar_necess_vaga_serv(nr_seq_ageint_p, ie_tipo_agendamento_w, nr_seq_agenda_w);
		end;
	end if;	
	
	select	decode(count(*),0,'N','S')
	into	ie_enviar_w
	from	ageint_check_list_pac_item_v
	where	ds_resultado = 'S'
	and 	nr_seq_ageint = nr_seq_ageint_p;
	
	ds_email_checklist_w := ageint_obter_emails_check(nr_seq_ageint_p);	
	
ie_enviar_p 		:= ie_enviar_w;
ds_email_checklist_p	:= ds_email_checklist_w;
ie_tipo_agendamento_p	:= ie_tipo_agendamento_w;
nr_seq_agenda_p		:= nr_seq_agenda_w;

end ai_gerar_necess_vaga_texto;
/