create or replace procedure UB_GERAR_RETORNO_HSBC_400(  nr_seq_cobr_escrit_p  number,
          nm_usuario_p  varchar2) is

nr_seq_reg_T_w    number(10);
nr_seq_reg_U_w    number(10);
nr_titulo_w    number(10);
vl_titulo_w    number(15,2);
vl_acrescimo_w    number(15,2);
vl_desconto_w    number(15,2);
vl_abatimento_w    number(15,2);
vl_liquido_w    number(15,2);
vl_outras_despesas_w  number(15,2);
dt_liquidacao_w    varchar2(6);
ds_titulo_w    varchar2(255);
vl_cobranca_w    number(15,2);
vl_alterar_w    number(15,2);
cd_ocorrencia_w    varchar2(5);
nr_seq_ocorrencia_ret_w  number(10);
nr_seq_ocorr_motivo_w number;
cd_ocorrencia_retorno_w varchar2(04);
dt_vencimento_w      		date;
nr_seq_tipo_w 			cobranca_escritural.nr_seq_tipo%type;

vl_saldo_multa_w		number(15,2);	

vl_juros_w			titulo_receber_cobr.vl_juros%type;
vl_multa_w			titulo_receber_cobr.vl_multa%type;
vl_tributos_w		titulo_receber_trib.vl_tributo%type;		


cursor c01 is
  select  trim(substr(ds_string,66,7)),
    somente_numero(substr(ds_string,153,13))/100,
    somente_numero(substr(ds_string,267,13))/100,
    somente_numero(substr(ds_string,241,13))/100,
    somente_numero(substr(ds_string,228,13))/100,
    somente_numero(substr(ds_string,254,13))/100,
    0,
    --substr(ds_string,111,6),
    decode(substr(ds_string,111,6),'000000',null,substr(ds_string,111,6)) liquidacao,
    substr(ds_string,109,2),
    substr(ds_string,302,2)
  from  w_retorno_banco
  where  nr_seq_cobr_escrit  = nr_seq_cobr_escrit_p
  and  substr(ds_string,1,1)  = '1';
begin

open C01;
loop
fetch C01 into
  ds_titulo_w,
  vl_cobranca_w,
  vl_acrescimo_w,
  vl_desconto_w,
  vl_abatimento_w,
  vl_liquido_w,
  vl_outras_despesas_w,
  dt_liquidacao_w,
  cd_ocorrencia_w,
  cd_ocorrencia_retorno_w;
exit when C01%notfound;
  begin

  vl_alterar_w  := 0;
  vl_saldo_multa_w := 0;
  vl_juros_w := 0;
  vl_multa_w := 0;

  select  max(nr_titulo)
  into  nr_titulo_w
  from  titulo_receber
  where  nr_titulo  = (ds_titulo_w);

  /* Se encontrou o t�tulo importa, sen�o grava no log */

  if  (nr_titulo_w is not null) then

    select  vl_titulo,
            dt_vencimento
    into  vl_titulo_w,
          dt_vencimento_w
    from  titulo_receber
    where  nr_titulo  = nr_titulo_w;

	select 	nvl(sum(a.vl_tributo),0) 
	into	vl_tributos_w
	from 	titulo_receber_trib a
	where 	a.nr_titulo	 	= nr_titulo_w
	and 	nvl(a.ie_origem_tributo, 'C') in ('D','CD');

	select nvl(max(nr_seq_tipo),4)
	into nr_seq_tipo_w
	from cobranca_escritural
	where nr_sequencia = nr_seq_cobr_escrit_p;
		
    select   max(a.nr_sequencia)
    into  nr_seq_ocorrencia_ret_w
    from  banco_ocorr_escrit_ret a
    where  a.cd_banco  = 399
    and a.nr_seq_tipo = nr_seq_tipo_w
    and  a.cd_ocorrencia = cd_ocorrencia_w;

    select   max(m.nr_sequencia)  
    into  nr_seq_ocorr_motivo_w
    from  banco_ocorr_escrit_ret a, banco_ocorr_motivo_ret m
    where a.nr_sequencia = m.nr_seq_escrit_ret
    and  a.cd_banco = 399
    and  a.cd_ocorrencia     = cd_ocorrencia_w
    and m.cd_motivo = cd_ocorrencia_retorno_w
    and  nvl(a.ie_forma_cobranca,2)  = 2;

		/* Tratar acrescimos/descontos */
		if	((vl_titulo_w - vl_tributos_w) <> vl_liquido_w) then
			if to_Date(dt_liquidacao_w,'ddmmyy') > dt_vencimento_w then
				vl_alterar_w	:= vl_liquido_w - (vl_titulo_w - vl_tributos_w);

				if	(vl_alterar_w > 0) then
				        /*OS 1345212 - Calcular multa e juros q vem tdo junto no total do titulo, e lan�ar separado na cobran�a, para refletir na baixa do titulo.*/
						vl_saldo_multa_w	:= obter_juros_multa_titulo(nr_titulo_w,to_Date(dt_liquidacao_w,'ddmmyy'),'R','M');
						vl_juros_w := nvl(vl_alterar_w,0) - nvl(vl_saldo_multa_w,0);
						vl_multa_w := nvl(vl_alterar_w,0) - nvl(vl_juros_w,0);
				end if;
			end if;
		end if;

    insert  into titulo_receber_cobr (  NR_SEQUENCIA,
              NR_TITULO,
              CD_BANCO,
              VL_COBRANCA,
              VL_DESCONTO,
              VL_ACRESCIMO,
              VL_DESPESA_BANCARIA,
              VL_LIQUIDACAO,
              DT_LIQUIDACAO,
              DT_ATUALIZACAO,
              NM_USUARIO,
              NR_SEQ_COBRANCA,
              nr_seq_ocorrencia_ret,
              nr_seq_ocorr_motivo,
              vl_saldo_inclusao,
			  vl_juros,
			  vl_multa)
          values  (  titulo_receber_cobr_seq.nextval,
              nr_titulo_w,
              399,
              vl_titulo_w,
              vl_desconto_w,
              0, --vl_acrescimo_w, Retirei a pedido do Jo�o, OS 1345212.
              vl_outras_despesas_w,
              vl_liquido_w,
              to_Date(dt_liquidacao_w,'ddmmyy'),
              sysdate,
              nm_usuario_p,
              nr_seq_cobr_escrit_p,
              nr_seq_ocorrencia_ret_w,
              nr_seq_ocorr_motivo_w,
              vl_titulo_w,
			  vl_juros_w,
			  vl_multa_w);
  else
    fin_gerar_log_controle_banco(  3,
            substr(wheb_mensagem_pck.get_texto(302918, 'DS_TITULO_W=' || ds_titulo_w),1,4000),
            nm_usuario_p,
            'N');
  end if;

  end;
end loop;
close C01;

commit;

end UB_GERAR_RETORNO_HSBC_400;
/