CREATE OR REPLACE
PROCEDURE pep_gerar_liberacao_ciclo(	nr_seq_paciente_p	Number,
					ie_todos_p		Varchar2,
					cd_pessoa_fisica_p	Varchar2,
					qt_ciclos_p		Number,
					nm_usuario_p		Varchar2) is


nr_sequencia_w		Number(10);
nr_ciclo_w		Number(3);
nr_ciclo_final_w		Number(3);
nr_ciclo_atual_w		Number(3);
cd_pessoa_fisica_w	Varchar2(10);
ie_liberar_w		Varchar2(1);
nr_atendimento_w		Number(10);
ie_tipo_convenio_w		Number(2);
cd_material_w		Number(6);
qt_autorizado_w		Number(10);
cd_convenio_w		Number(5);
ds_material_w		Varchar2(255);
ds_material_acum_w	Varchar2(4000);
ie_consiste_mat_aut_w	varchar2(1);
qt_dose_w		number(10,0);
ie_exige_laudo_w	varchar2(10);

Cursor C01 is
	select	b.cd_material,
		m.ds_material
	from	material_autorizado b,
		autorizacao_convenio a,
		estagio_autorizacao e,
		material m
	where	a.nr_sequencia		= b.nr_sequencia_autor
	and	e.nr_sequencia		= a.nr_seq_estagio
	and	b.cd_material		= m.cd_material
	and	a.nr_seq_paciente_setor = nr_seq_paciente_p
	and	a.cd_convenio 		= cd_convenio_w
	and	e.ie_interno		<> '10'
	and	((ie_todos_p = 'S') or ((ie_todos_p = 'N') and (a.nr_ciclo = nr_ciclo_w)));

BEGIN

select  count(*)
into	qt_dose_w
from    material b,
        paciente_protocolo_medic a
where   a.cd_material           	= b.cd_material
and     a.nr_seq_paciente       	= nr_seq_paciente_p
and     a.qt_dose 		= 0;

if	(qt_dose_w > 0) then
	--Protocolo cont�m medicamento(s) com dose(s) zerada(s). N�o ser� poss�vel liberar o ciclo. #@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(261442);
end if;

select	Obter_Valor_Param_Usuario(281,170, obter_perfil_ativo, nm_usuario_p,0)
into	ie_consiste_mat_aut_w
from	dual;

ie_exige_laudo_w	:= Obter_Valor_Param_Usuario(281,134, obter_perfil_ativo, nm_usuario_p,0);

select	nvl(max(nr_ciclo),0) + 1
into	nr_ciclo_w
from	paciente_setor_lib
where	nr_seq_paciente	= nr_seq_paciente_p
and	dt_cancelamento is null;

select	nvl(max(nr_ciclos),-1),
	max(cd_pessoa_fisica)
into	nr_ciclo_final_w,
	cd_pessoa_fisica_w
from	paciente_setor
where	nr_seq_paciente	= nr_seq_paciente_p;

select	max(nr_atendimento)
into	nr_atendimento_w
from	atendimento_paciente
where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

if	(nr_atendimento_w > 0) then
	select	obter_tipo_convenio(obter_convenio_atendimento(nr_atendimento_w)),
		obter_convenio_atendimento(nr_atendimento_w)
	into	ie_tipo_convenio_w,
		cd_convenio_w
	from	dual;
end if;

if	(ie_tipo_convenio_w 	= 3) and
	(ie_exige_laudo_w 	= 'S')then /*SUS*/
	select	substr(Obter_se_laudo_vigente(cd_pessoa_fisica_w,sysdate,1),1,1)
	into	ie_liberar_w
	from	dual;

	if	(ie_liberar_w = 'N') then
		--N�o � permitido liberar ciclos sem ter um laudo de quimio digitado.');
		Wheb_mensagem_pck.exibir_mensagem_abort(261443);
	end if;
end if;

if	(ie_tipo_convenio_w not in (1,3)) and (ie_consiste_mat_aut_w = 'S') then /*SUS e Particular*/

	open	c01;
	loop
	fetch	c01 into
		cd_material_w,
		ds_material_w;
	exit	when c01%notfound;
		begin	
		select	count(1)
		into	qt_autorizado_w
		from	paciente_protocolo_medic a
		where	a.cd_material		= cd_material_w
		and	a.nr_seq_paciente	= nr_seq_paciente_p;
		
		if	(qt_autorizado_w > 0) then 
			ds_material_acum_w	:= ds_material_acum_w ||ds_material_w ||chr(13) ||chr(10);
		end if;
		end;
	end loop;
	close c01;

	if	(ds_material_acum_w is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(277333, 'DS_MATERIAL_ACUM_P=' || ds_material_acum_w ||chr(13) ||chr(10) || obter_desc_expressao(619521) || nr_ciclo_w);
	end if;
	
	select	paciente_setor_lib_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_setor_lib(
		nr_sequencia,           
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_paciente,
		nr_ciclo,
		dt_liberacao,
		cd_pessoa_fisica)
	values(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_paciente_p,
		nr_ciclo_w,
		sysdate,
		cd_pessoa_fisica_p);		
			
end if;

nr_ciclo_final_w	:= qt_ciclos_p;

if	(nr_ciclo_final_w <> -1) and
	(nr_ciclo_w > nr_ciclo_final_w) then
	--Aten��o! Foram prescritos '||to_char(nr_ciclo_final_w)||' ciclos. Voc� esta tentando liberar o ciclo '||to_char (nr_ciclo_w));
	Wheb_mensagem_pck.exibir_mensagem_abort(261445,	'CICLO='|| to_char(nr_ciclo_final_w) || ';' ||
													'NR_CICLO=' || to_char(nr_ciclo_w));
end if;

commit;
		
END pep_gerar_liberacao_ciclo;
/