create or replace
procedure atualiza_status_leito	(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

begin
if (nvl(nr_sequencia_p,0) > 0) then

	update	sl_unid_atend
	set	ie_status_serv = 'P',
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia  = nr_sequencia_p;

end if;
commit;

end atualiza_status_leito;
/