create or replace
procedure man_vincular_story_impedimento(nr_story_p  	number,
                              nr_impedimento_p       	number,
                              nm_usuario_p      	varchar2, 
			      ds_erro_p	out		varchar2) is

qt_existe_w	number(10);
			      
begin

select	count(*)
into	qt_existe_w
from	desenv_story
where	nr_sequencia = nr_impedimento_p;

if	(qt_existe_w = 0) then
	ds_erro_p := wheb_mensagem_pck.get_texto(388893);
	return;
end if;

select	count(*)
into	qt_existe_w
from	desenv_story_impedimento
where	nr_story = nr_story_p
and	nr_impedimento = nr_impedimento_p;

if	(qt_existe_w = 0) then
	insert into desenv_story_impedimento(
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_impedimento,
		nr_sequencia,
		nr_story)
	values(	sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_impedimento_p,
		desenv_story_impedimento_seq.nextval,
		nr_story_p);
	
end if;

end man_vincular_story_impedimento;
/
