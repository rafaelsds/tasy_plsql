create or replace procedure inserir_alocacao_usuario(nm_usuario_grupo_p varchar2,
                                                     ie_emprestimo_p    varchar2,
                                                     dt_inicio_p        date,
                                                     dt_fim_p           date,
                                                     nr_seq_grupo_des_p number,
                                                     nr_seq_grupo_sup_p number,
                                                     pr_alocacao_p      number,
                                                     nm_usuario_p       varchar2) is

begin

    if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
		null;
	end if;

    insert into alocacao_usuario_grupo
                        (dt_atualizacao,
                         dt_atualizacao_nrec,
                         dt_fim_vigencia,
                         dt_inicio_vigencia,
                         ie_emprestimo,
                         nm_usuario,
                         nm_usuario_grupo,
                         nm_usuario_nrec,
                         nr_seq_grupo_des,
                         nr_seq_grupo_sup,
                         nr_sequencia,
                         pr_alocacao)
                        values
                        (sysdate,
                         sysdate,
                         dt_fim_p,
                         dt_inicio_p,
                         ie_emprestimo_p,
                         nm_usuario_p,
                         nm_usuario_grupo_p,
                         nm_usuario_p,
                         nr_seq_grupo_des_p,
                         nr_seq_grupo_sup_p,
                         alocacao_usuario_grupo_seq.nextval,
                         pr_alocacao_p);

end inserir_alocacao_usuario;
/