create or replace
procedure pls_gerar_relat_utili_bene_nov(	ds_login_p		varchar2,
						dt_inicio_p		date,
						dt_fim_p		date,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
						
nr_seq_segurado_w	pls_segurado.nr_sequencia%type;
nr_id_transacao_w	w_pls_utilizacao_benef.nr_id_transacao%type;

begin

select	max(a.nr_sequencia)
into	nr_seq_segurado_w
from	pls_segurado 		a,
	wsuite_usuario 		b
where 	a.nr_sequencia 		= b.nr_seq_segurado
and	b.ds_login		= ds_login_p
and	a.cd_estabelecimento	= cd_estabelecimento_p;

pls_utilizacao_benef_pck.gerar_utilizacao_benef (	nr_seq_segurado_w,
							dt_inicio_p,
							dt_fim_p,
							nm_usuario_p,
							cd_estabelecimento_p,
							nr_id_transacao_w,
							null);
							
end PLS_GERAR_RELAT_UTILI_BENE_NOV;
/