create or replace
procedure HCB_GERAR_RETORNO_PAGTO_BRAD
		(nr_seq_banco_escrit_p	in	number,
		nm_usuario_p		in	varchar2) is

/* 	Rotina criada para o Layout PagFor 
Estrutura
Header de arquivo
Registros transacao titulos
Trailler de arquivo
*/

ds_nr_titulo_w		varchar2(15);
ds_vl_pagamento_w	varchar2(15);
ds_ocorrencia_w		varchar2(10);
nr_titulo_w		number(10,0);
cd_tipo_baixa_w		number(10,0);
nr_seq_trans_escrit_w	number(10,0);
nr_seq_conta_banco_w	number(10,0);
nr_sequencia_w		number(10,0);
vl_pagamento_w		number(15,2);
cd_estabelecimento_w	number(10,0);
qt_titulo_w		number(10);
dt_baixa_w		date;
cd_banco_w		number(3);
cd_retorno_liq_w	varchar2(3);
qt_retorno_liq_w	number(10);
ie_movto_bco_pag_escrit_w		parametros_contas_pagar.ie_movto_bco_pag_escrit%type;

/* registro transacao */
cursor c01 is
select	substr(ds_conteudo,120,15) ds_nr_titulo,
	substr(ds_conteudo,205,15) ds_vl_pagamento,
	substr(ds_conteudo,279,10) ds_ocorrencia,	/* cada ocorrencia possui dois digitos mas o registro pode possuir ate 5 ocorrencias */
	to_date(substr(ds_conteudo,266,8),'yyyy/mm/dd') dt_baixa
from	w_interf_retorno_itau
where	substr(ds_conteudo, 1, 1)	= '1'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
order	by nr_sequencia;

begin

select	max(a.cd_estabelecimento),
	max(a.nr_seq_conta_banco),
	max(a.nr_seq_trans_financ),
	max(b.cd_banco)
into	cd_estabelecimento_w,
	nr_seq_conta_banco_w,
	nr_seq_trans_escrit_w,
	cd_banco_w
from	banco_estabelecimento b,
	banco_escritural a
where	a.nr_seq_conta_banco	= b.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_banco_escrit_p;

select	max(a.cd_retorno_liq)
into	cd_retorno_liq_w
from	banco_retorno_cp a
where	a.cd_banco	= cd_banco_w;

if	(nr_seq_trans_escrit_w	is null) then

	select	max(nr_seq_trans_escrit)
	into	nr_seq_trans_escrit_w
	from	parametro_tesouraria
	where	cd_estabelecimento	= cd_estabelecimento_w;

end if;

select	nvl(max(cd_tipo_baixa_padrao),1),
		max(ie_movto_bco_pag_escrit)
into	cd_tipo_baixa_w,
		ie_movto_bco_pag_escrit_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

open c01;
loop
fetch c01 into
	ds_nr_titulo_w,
	ds_vl_pagamento_w,
	ds_ocorrencia_w,
	dt_baixa_w;
exit when c01%notfound;

	begin

	select	max(a.nr_titulo)
	into	nr_titulo_w
	from	titulo_pagar a
	where	a.nr_titulo	= to_number(somente_numero(ds_nr_titulo_w));	

	if	(nr_titulo_w is null) then
		/* O titulo ds_nr_titulo_w nao existe no sistema */
		wheb_mensagem_pck.exibir_mensagem_abort(226797,'DS_NR_TITULO_W='||ds_nr_titulo_w);
	end if;

	vl_pagamento_w		:= to_number(ds_vl_pagamento_w);
	vl_pagamento_w		:= dividir_sem_round(vl_pagamento_w,100);

	select	count(*)
	into	qt_titulo_w
	from	titulo_pagar_escrit a
	where	a.nr_titulo	= nr_titulo_w
	and	a.nr_seq_escrit	= nr_seq_banco_escrit_p;

	if	(qt_titulo_w <> 0) then
		update	titulo_pagar_escrit
		set	ds_erro			= ds_ocorrencia_w
		where	nr_seq_escrit		= nr_seq_banco_escrit_p
		and	nr_titulo		= nr_titulo_w;
	end if;


	qt_retorno_liq_w	:= 0;

	/* se tiver alguma das ocorrencias de liquidacao baixa o titulo */
	if	(instr(ds_ocorrencia_w,'BW') > 0) or
		(instr(ds_ocorrencia_w,cd_retorno_liq_w) > 0) then

		qt_retorno_liq_w	:= 1;

	else

		select	count(*)
		into	qt_retorno_liq_w
		from	erro_escritural a
		where	a.ie_rejeitado	= 'L'
		and	instr(ds_ocorrencia_w,a.cd_erro)	> 0
		and	a.cd_banco	= cd_banco_w;

	end if;

	if	(qt_titulo_w <> 0 and qt_retorno_liq_w	> 0) and (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T') then

		dbms_output.put_line('nr_titulo_w = ' || nr_titulo_w);

		baixa_titulo_pagar
				(cd_estabelecimento_w,
				cd_tipo_baixa_w,
				nr_titulo_w,
				vl_pagamento_w,
				nm_usuario_p,
				nr_seq_trans_escrit_w,
				null,
				nr_seq_banco_escrit_p,
				nvl(dt_baixa_w,sysdate),
				nr_seq_conta_banco_w);

		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	titulo_pagar_baixa
		where	nr_titulo	= nr_titulo_w;

		gerar_movto_tit_baixa
				(nr_titulo_w,
				nr_sequencia_w,
				'P',
				nm_usuario_p,
				'N');

		atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
		Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
	end if;

	exception
	when others then
		rollback;
		delete	from w_interf_retorno_itau
		where	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;
		commit;
		wheb_mensagem_pck.exibir_mensagem_abort(226798,
							'SQL_ERRM=' || sqlerrm ||
							';NR_TITULO_W=' || nr_titulo_w ||
							';DS_NR_TITULO_W=' || ds_nr_titulo_w ||
							';DS_VL_PAGAMENTO_W=' || ds_vl_pagamento_w ||
							';DS_OCORRENCIA_W=' || ds_ocorrencia_w);
	end;
end loop;
close c01;

if (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T') then
	update	banco_escritural
	set	dt_baixa	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_banco_escrit_p;
end if;

delete	from w_interf_retorno_itau
where	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

commit;

end HCB_GERAR_RETORNO_PAGTO_BRAD;
/
