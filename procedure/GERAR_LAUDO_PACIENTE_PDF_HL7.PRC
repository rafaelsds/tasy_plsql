create or replace
procedure gerar_laudo_paciente_pdf_hl7 (	nr_seq_laudo_p		number,
											nr_acesso_dicom_p	number,
											nr_seq_laudo_pdf_p	out number) is 

nr_acesso_dicom_w	laudo_paciente_pdf.nr_acesso_dicom%type;

begin

if	(nr_seq_laudo_p is not null) then
	select	max(nr_acc_number)
	into	nr_acesso_dicom_w
	from	w_integracao_laudo_hl7
	where	nr_sequencia	= nr_acesso_dicom_p;

	select	laudo_paciente_pdf_seq.nextval
	into	nr_seq_laudo_pdf_p
	from	dual;
	
	insert into laudo_paciente_pdf(	NR_SEQUENCIA,
									DT_ATUALIZACAO,
									DT_ATUALIZACAO_NREC,
									NM_USUARIO,
									NM_USUARIO_NREC,
									NR_ACESSO_DICOM,
									NR_SEQ_LAUDO)
							values(	nr_seq_laudo_pdf_p,
									sysdate,
									sysdate,
									Obter_Desc_Expressao(292128),
									Obter_Desc_Expressao(292128),
									nr_acesso_dicom_w,
									nr_seq_laudo_p								
								);
								
	update	laudo_paciente
	set		ie_formato = 3
	where	nr_sequencia = nr_seq_laudo_p;

	commit;
end if;

end gerar_laudo_paciente_pdf_hl7;
/