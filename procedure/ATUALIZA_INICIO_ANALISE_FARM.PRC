create or replace 
procedure atualiza_inicio_analise_farm(	nr_prescricao_p		number,
					ds_justificativa_p	varchar,
					nm_usuario_p		varchar)
					is
ie_inicia_w			varchar2(1):='N';													
nm_usuario_w			varchar2(15);
nm_pessoa_fisica_w 		varchar2(255);
dt_inicio_analise_farm_w	date;
													
begin

/*
select	decode(count(*),0,'S','N')
into	ie_inicia_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p
and	dt_inicio_analise_farm is not null;
*/

--if	(ie_inicia_w = 'N') then
select	nm_usuario_analise_farm,
	obter_nome_pf(obter_dados_usuario_opcao(nm_usuario_analise_farm,'C')),
	dt_inicio_analise_farm,
	decode(dt_inicio_analise_farm,null,'S','N')
into	nm_usuario_w,
	nm_pessoa_fisica_w,
	dt_inicio_analise_farm_w,
	ie_inicia_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;
--end if;	

update	prescr_medica
set	dt_inicio_analise_farm 	= decode(ie_inicia_w,'S',sysdate,null),
	nm_usuario_analise_farm = decode(ie_inicia_w,'S',nm_usuario_p,null)
where	nr_prescricao 		= nr_prescricao_p;

insert	into	log_analise_prescr(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_prescricao,
		ie_acao,
		dt_acao,
		nm_usuario_acao,
		ds_motivo_acao)
values		(log_analise_prescr_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_prescricao_p,
		decode(ie_inicia_w,'S','I','D'),
		sysdate,
		nm_usuario_p,
		ds_justificativa_p);

commit;

end	atualiza_inicio_analise_farm;
/