create or replace
procedure pls_acao_analise_recurso(	nr_seq_analise_rec_p		pls_analise_conta_rec.nr_sequencia%type,
					nr_id_transacao_p		pls_analise_conta_rec.nr_id_transacao%type,
					vl_acatado_p			pls_analise_conta_rec.vl_acatado%type,
					nr_seq_grupo_p			pls_grupo_auditor.nr_sequencia%type,
					ds_parecer_p			pls_analise_parecer_rec.ds_parecer%type,
					ie_acao_p			varchar2,
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nr_seq_protocolo_p		pls_rec_glosa_protocolo.nr_sequencia%type,
          ds_observacao_p       pls_conta_observacao_a500.ds_observacao%type default null) is 
					 

-- IE_ACAO_P
-- L - Liberar item total
-- P - Liberar item parcial
-- N - Negado todo atendimento
					
vl_glosa_w			number(15,2);
vl_recursado_w			number(15,2);
vl_negado_w			number(15,2);
vl_acatado_w			number(15,2);
vl_acatado_aud_w		number(15,2);
ds_observacao_w			varchar2(4000);
ie_tipo_item_w			varchar2(5);
ds_item_w			pls_analise_conta_rec.ds_item%type;
nr_seq_conta_w			pls_analise_conta_rec.nr_seq_conta%type;
nr_seq_analise_w		pls_analise_conta_rec.nr_seq_analise%type;
ie_tipo_historico_w		pls_hist_analise_conta.ie_tipo_historico%type;
ds_tipo_historico_w		varchar2(255);
nr_seq_proc_rec_w		pls_analise_conta_rec.nr_seq_proc_rec%type;
nr_seq_mat_rec_w		pls_analise_conta_rec.nr_seq_mat_rec%type;
nr_seq_conta_rec_w		pls_analise_conta_rec.nr_seq_conta_rec%type;
ie_acao_total_w			pls_rec_glosa_conta.ie_acatado_glosa%type;
nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_mat.nr_sequencia%type;
	
Cursor C01 (nr_seq_protocolo_pc		pls_rec_glosa_protocolo.nr_sequencia%type) is
	select	nr_seq_conta,
		nr_seq_analise,
		ie_tipo_item,
		ds_item,
		nr_seq_proc_rec,
		nr_seq_mat_rec,
		nr_seq_conta_rec,
		max(nr_sequencia) nr_seq_analise_rec,
		max(nr_id_transacao) nr_id_transacao
	from	pls_recurso_item_v
	where	nr_seq_protocolo_rec = nr_seq_protocolo_pc
	group by
		nr_seq_conta,
		nr_seq_analise,
		ie_tipo_item,
		ds_item,
		nr_seq_proc_rec,
		nr_seq_mat_rec,
		nr_seq_conta_rec;
			
begin
if	(nr_seq_protocolo_p is not null) then

	if	(ie_acao_p = 'L') then
		
		for r_C01_w in C01 (nr_seq_protocolo_p) loop
			
			ie_tipo_historico_w 	:= 39;
			ds_tipo_historico_w 	:= 'Atendimento acatado';
			ds_observacao_w 	:= 	'Tipo de liberacao:  '||chr(13)||chr(10)||
							chr(9)||'Liberacao favoravel'||chr(13)||chr(10)||chr(13)||chr(10)||
							'Parecer: '||chr(13)||chr(10)||
							chr(9)||ds_parecer_p;
			
			pls_inserir_hist_analise(	r_C01_w.nr_seq_conta, r_C01_w.nr_seq_analise, ie_tipo_historico_w,
							r_C01_w.nr_seq_analise_rec, r_C01_w.ie_tipo_item, null,
							null, ds_tipo_historico_w || '.' || chr(13) || chr(10) || ds_observacao_w,
							nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
							
			update	pls_analise_conta_rec
			set	vl_acatado		= nvl(vl_recursado,0),
				ie_situacao		= 'A'
			where	nr_sequencia		= r_C01_w.nr_seq_analise_rec;
			
			if	(r_C01_w.nr_seq_proc_rec is not null) then
      
				update	pls_rec_glosa_proc
				set	vl_acatado		= nvl(vl_recursado,0),
					ie_status 		= 'A',
					ie_acao_total		= 'S',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= r_C01_w.nr_seq_proc_rec;
				
			elsif	(r_C01_w.nr_seq_mat_rec is not null) then
				update	pls_rec_glosa_mat
				set	vl_acatado		= nvl(vl_recursado,0),
					ie_status 		= 'A',
					ie_acao_total		= 'S',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= r_C01_w.nr_seq_mat_rec;
			end if;
			
			update	pls_rec_glosa_conta
			set	ie_acao_total	= 'S',
				nm_usuario	= nm_usuario_p,
				dt_atualizacao 	= sysdate
			where	nr_sequencia 	= r_C01_w.nr_seq_conta_rec;
			
			-- Atualizar valor recurso de glosa
			pls_atualizar_valor_recurso( r_C01_w.nr_seq_conta_rec, 'C', nm_usuario_p);

			-- Gerar o parecer dos itens 
			pls_inserir_parecer_recurso( r_C01_w.nr_seq_analise_rec, r_C01_w.nr_id_transacao, ds_parecer_p, nm_usuario_p, cd_estabelecimento_p);
		end loop;
		
	elsif	(ie_acao_p = 'N') then
		
		for r_C01_w in C01 (nr_seq_protocolo_p) loop
			
			ie_tipo_historico_w 	:= 28;
			ds_tipo_historico_w 	:= 'Atendimento glosado';
			ds_observacao_w 	:= 	'Tipo de liberacao:  '||chr(13)||chr(10)||
							chr(9)||'Liberacao desfavoravel'||chr(13)||chr(10)||chr(13)||chr(10)||
							'Parecer: '||chr(13)||chr(10)||
							chr(9)||ds_parecer_p;
			
			pls_inserir_hist_analise(	r_C01_w.nr_seq_conta, r_C01_w.nr_seq_analise, ie_tipo_historico_w,
							r_C01_w.nr_seq_analise_rec, r_C01_w.ie_tipo_item, null,
							null, ds_tipo_historico_w || '.' || chr(13) || chr(10) || ds_observacao_w,
							nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
							
			update	pls_analise_conta_rec
			set	vl_acatado		= 0,
				ie_situacao		= 'N'
			where	nr_sequencia		= r_C01_w.nr_seq_analise_rec;
			
			if	(r_C01_w.nr_seq_proc_rec is not null) then
				update	pls_rec_glosa_proc
				set	vl_acatado		= 0,
					ie_status 		= 'N',
					ie_acao_total		= 'S',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= r_C01_w.nr_seq_proc_rec;
				
			elsif	(r_C01_w.nr_seq_mat_rec is not null) then
				update	pls_rec_glosa_mat
				set	vl_acatado		= 0,
					ie_status 		= 'N',
					ie_acao_total		= 'S',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= r_C01_w.nr_seq_mat_rec;
			end if;
			
			update	pls_rec_glosa_conta
			set	ie_acao_total	= 'S',
				nm_usuario	= nm_usuario_p,
				dt_atualizacao 	= sysdate
			where	nr_sequencia 	= r_C01_w.nr_seq_conta_rec;
			
			-- Atualizar valor recurso de glosa
			pls_atualizar_valor_recurso( r_C01_w.nr_seq_conta_rec, 'C', nm_usuario_p);

			-- Gerar o parecer dos itens 
			pls_inserir_parecer_recurso( r_C01_w.nr_seq_analise_rec, r_C01_w.nr_id_transacao, ds_parecer_p, nm_usuario_p, cd_estabelecimento_p);
		end loop;
	end if;		
	
	-- Atualiza a justificativa do protocolo, somente deve ser atualizado a 
	-- justificativa do protocolo quando a acao for a nivel de protocolo
	update	pls_rec_glosa_protocolo
	set	ds_justificativa_oper	= ds_parecer_p,
		nm_usuario 		= nm_usuario_p,
		dt_atualizacao 		= sysdate
	where	nr_sequencia 		= nr_seq_protocolo_p;
else
	select	nvl(vl_glosa,0), -- Valor de origem Glosa
		nvl(vl_recursado,0), -- Valor recursado pelo Prestador
		nvl(vl_negado,0), -- Valor negado (Nao vai ser pago)
		nvl(vl_acatado,0), -- Valor aceito (Vai ser pago)
		nr_seq_conta,
		nr_seq_analise,
		ie_tipo_item,
		ds_item,
		decode(ie_acao_p,'P',vl_acatado_p,vl_recursado) vl_acatado_aud,
		nr_seq_proc_rec,
		nr_seq_mat_rec,
		nr_seq_conta_rec
	into	vl_glosa_w,
		vl_recursado_w,
		vl_negado_w,
		vl_acatado_w,
		nr_seq_conta_w,
		nr_seq_analise_w,
		ie_tipo_item_w,
		ds_item_w,
		vl_acatado_aud_w,
		nr_seq_proc_rec_w,
		nr_seq_mat_rec_w,
		nr_seq_conta_rec_w
	from	pls_recurso_item_v
	where	nr_sequencia	= nr_seq_analise_rec_p;
	
	if	(nr_seq_proc_rec_w is not null) then
		select	nvl(ie_acao_total,'N')
		into	ie_acao_total_w
		from	pls_rec_glosa_proc
		where	nr_sequencia = nr_seq_proc_rec_w;
	else
		select	nvl(ie_acao_total,'N')
		into	ie_acao_total_w
		from	pls_rec_glosa_mat
		where	nr_sequencia = nr_seq_mat_rec_w;
	end if;
	
	if	(ie_acao_total_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(710361); -- Ja foi dado um parecer ao atendimento deste recurso. Acao abortada.
	end if;
	
	if	(ie_acao_p = 'L') or
		(vl_acatado_aud_w = vl_recursado_w) then
		ie_tipo_historico_w	:= 25; -- Aceito valor recursado
		ds_tipo_historico_w	:= 'Aceito valor recursado';
		
	elsif	(vl_acatado_aud_w = 0) then
		ie_tipo_historico_w	:= 26; -- Negado valor recursado
		ds_tipo_historico_w	:= 'Negado valor recursado';
		
	elsif	(vl_acatado_aud_w <> vl_recursado_w) then
		ie_tipo_historico_w	:= 27; -- Recurso aceito parcialmente
		ds_tipo_historico_w	:= 'Recurso aceito parcialmente';
	end if;
	
	-- Caso esteja nulo vai pela acao selecionada
	if	(ie_tipo_historico_w is null) then
		if	(ie_acao_p = 'L') then
			ie_tipo_historico_w	:= 25; -- Aceito valor recursado
			ds_tipo_historico_w	:= 'Aceito valor recursado';
		elsif	(ie_acao_p = 'N') then
			ie_tipo_historico_w	:= 26; -- Negado valor recursado
			ds_tipo_historico_w	:= 'Negado valor recursado';
		elsif	(ie_acao_p = 'P') then
			ie_tipo_historico_w	:= 27; -- Recurso aceito parcialmente
			ds_tipo_historico_w	:= 'Recurso aceito parcialmente';
		end if;
	end if;

	if	(ie_acao_p = 'P') and
		(vl_acatado_w <> vl_acatado_aud_w) then
		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
					'Valor aceito:'||chr(13)||chr(10)||
					chr(9)||'Anterior: '||vl_acatado_w||' - Modificada: '||vl_acatado_aud_w||chr(13)||chr(10);

	elsif	(ie_acao_p = 'L') then
		ds_observacao_w := 	'Tipo de liberacao:  '||chr(13)||chr(10)||
					chr(9)||'Liberacao favoravel'||chr(13)||chr(10)||chr(13)||chr(10)||
					'Parecer: '||chr(13)||chr(10)||
					chr(9)||ds_parecer_p;		
	end if;

	if	(nvl(ds_observacao_w,'X') <> 'X') then	
		pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w,ie_tipo_historico_w,nr_seq_analise_rec_p,ie_tipo_item_w, null,null, 
					'Modificado pelo auditor '||obter_nome_usuario(nm_usuario_p)||'.'||chr(13)||chr(10)||
					'Item '||nr_seq_analise_rec_p||' - '||ds_item_w||'.'||chr(13)||chr(10)||ds_observacao_w,
					nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
	else
		pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w,ie_tipo_historico_w,nr_seq_analise_rec_p,ie_tipo_item_w, null,null, 
					ds_tipo_historico_w, nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
	end if;

	update	pls_analise_conta_rec
	set	vl_acatado		= nvl(vl_acatado_aud_w,0),
		ie_situacao		= decode(nvl(vl_acatado_aud_w,0),0,'N',decode(nvl(vl_acatado_aud_w,0),nvl(vl_recursado,0),'A','P'))
	where	nr_sequencia		= nr_seq_analise_rec_p;

	if	(nr_seq_proc_rec_w is not null) then
		update	pls_rec_glosa_proc
		set	vl_acatado		= nvl(vl_acatado_aud_w,0),
			ie_status 		= decode(nvl(vl_acatado_aud_w,0),0,'4',decode(nvl(vl_acatado_aud_w,0),nvl(vl_recursado,0),'3','2')),
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_proc_rec_w;
		
	elsif	(nr_seq_mat_rec_w is not null) then
		update	pls_rec_glosa_mat
		set	vl_acatado		= nvl(vl_acatado_aud_w,0),
			ie_status 		= decode(nvl(vl_acatado_aud_w,0),0,'4',decode(nvl(vl_acatado_aud_w,0),nvl(vl_recursado,0),'3','2')),
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_mat_rec_w;
	end if;
	
	-- Atualizar valor recurso de glosa
	pls_atualizar_valor_recurso( nr_seq_conta_rec_w, 'C', nm_usuario_p);

	-- Gerar o parecer dos itens 
	pls_inserir_parecer_recurso( nr_seq_analise_rec_p, nr_id_transacao_p, ds_parecer_p, nm_usuario_p, cd_estabelecimento_p);
end if;

	if (ds_observacao_p is not null) then
	
	select	nr_seq_conta_proc,
		nr_seq_conta_mat
	into	nr_seq_conta_proc_w,
		nr_seq_conta_mat_w
	from	pls_recurso_item_v
	where	nr_sequencia	= nr_seq_analise_rec_p;	

        insert into pls_conta_observacao_a500    /* Observacao*/
            (nr_sequencia,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            ds_observacao,
            nr_seq_conta,
            nr_seq_conta_proc,
            nr_seq_conta_mat)
        values(pls_conta_observacao_a500_seq.nextval,
            sysdate,
            nm_usuario_p,
            sysdate,
            nm_usuario_p,
            ds_observacao_p,
            nr_seq_conta_w,
            nr_seq_conta_proc_w,
            nr_seq_conta_mat_w); 
     
	end if;
commit;

end pls_acao_analise_recurso;
/
