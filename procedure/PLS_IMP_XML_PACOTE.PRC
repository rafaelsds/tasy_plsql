create or replace
procedure pls_imp_xml_pacote(	ie_opcao_p		in	varchar2,
				ie_tipo_registro_p	in	varchar2,
				ds_linha_p		in	varchar2,
				nm_usuario_p      	in 	usuario.nm_usuario%type,
				nr_seq_lote_p		in out 	pls_imp_lote_pacote.nr_sequencia%type) is

nr_seq_lote_w		pls_imp_lote_pacote.nr_sequencia%type;

/*	ie_opcao_p
C - Criar lote
I - Insere registros lidos do arquivo
G - Gerar pacotes
*/

begin

nr_seq_lote_w := nr_seq_lote_p;

if	(ie_opcao_p = 'C') then
	pls_imp_xml_pacote_pck.criar_lote_importacao(nm_usuario_p, nr_seq_lote_w);
	
elsif	(ie_opcao_p = 'I') then
	pls_imp_xml_pacote_pck.incluir_linha_xml(ie_tipo_registro_p, ds_linha_p, nr_seq_lote_w);
	
elsif	(ie_opcao_p = 'G') then
	pls_imp_xml_pacote_pck.gerar_pacotes(nr_seq_lote_w, nm_usuario_p);

elsif	(ie_opcao_p = 'L') then
	pls_imp_xml_pacote_pck.limpar_tabela_temp(nr_seq_lote_w);

elsif	(ie_opcao_p = 'E') then
	pls_imp_xml_pacote_pck.exclui_lote_importacao(nr_seq_lote_w);
	
end if;

nr_seq_lote_p := nr_seq_lote_w;

commit;

end pls_imp_xml_pacote;
/