create or replace
procedure nut_grava_info_catraca	(	cd_barras_p 		Number,
						ie_tipo_pessoa_p	Varchar2,
						dt_refeicao_p		Date,
						nm_usuario_p		Varchar2) is 

						
nr_seq_integracao_w	number(10);
						
begin

if	(cd_barras_p is not null) then

	select	max(nr_sequencia)
	into	nr_seq_integracao_w
	from	nut_integracao_catraca
	where	trunc(dt_refeicao) = trunc(dt_refeicao_p);
	
	if	( nr_seq_integracao_w is null) then

		select	nut_integracao_catraca_seq.nextval
		into	nr_seq_integracao_w
		from	dual;
		
		insert into nut_integracao_catraca (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_refeicao,
			ds_observacao)
		values (nr_seq_integracao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			dt_refeicao_p,
			'');
		end if;

	begin
		insert into nut_integ_catraca_item (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_refeicao,
			cd_barras,
			ie_tipo_pessoa,
			nr_seq_integracao)
		values(	nut_integ_catraca_item_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			dt_refeicao_p,
			cd_barras_p, 
			ie_tipo_pessoa_p,
			nr_seq_integracao_w );
	exception
		when others then
		insert into log_nutricao (
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
		values 	     ( 	sysdate, 
				nm_usuario_p,  
				10, 
				wheb_mensagem_pck.get_texto(799713,
							'CD_BARRAS='||cd_barras_p||
							';NR_SEQ_INTEGRACAO='||nr_seq_integracao_w));				 
	end;
	nut_gerar_refeicoes_catraca(dt_refeicao_p, nm_usuario_p);
end if;
commit;

end Nut_grava_info_catraca;
/
