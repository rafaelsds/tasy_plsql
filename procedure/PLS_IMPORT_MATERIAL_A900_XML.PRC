create or replace
procedure pls_import_material_a900_xml( nr_seq_lote_imp_p   number,
                                        nm_arquivo_p        varchar2,
                                        nm_usuario_p        varchar2 ) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Goal: Insert the materials records on the table W_PLS_MATERIAL_UNIMED
 ----------------------------------------------------------------------------------------------------
 Direct call locations:
 [   ] Dictionary objects [ X ] Tasy (Delphi/Java/HTML5) [   ] Portal [    ] Reports [   ] Others:
 ---------------------------------------------------------------------------------------------------
 Attention points: Made by lots
 ---------------------------------------------------------------------------------------------------
 Obs: This routine was create by Vagner (HPMS Analyst). I'm just committing 'cause he passed to me
 commit for him.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

ds_comando_job_w    varchar2(2000);
jobno               number;
ds_local_w          varchar2(100);
bfile_w             BFILE;
clob_w              CLOB;
erro_w              varchar2(2000);

/* 'falhas' is not null, whats means that was already executed once. And if this was executed once, 
clean up. During the executation this will be null*/
cursor C01 is
    select
        job
    from
        job_v
    where
        lower(comando) like 'pls_gerar_import_mat_a900_new%'
    and falhas is not null;

begin

-- Verify if the job already exists, if yes, clean up the informations
for r_C01_w in C01 loop
    update
        pls_lote_imp_mat_unimed
    set
        dt_fim_importacao = null,
        dt_inicio_importacao = null,
        nm_usuario_imp = nm_usuario_p,
        ie_status = '4',
        ds_log = obter_desc_expressao(953962)
    where
        nr_sequencia = nr_seq_lote_imp_p;

    commit;

    return;
end loop;

ds_local_w := null;

select
    max(ut.ds_dir_oracle)
into
    ds_local_w
from
    evento_tasy_utl_file ut
where
    cd_evento = 2;

-- Validate if the directory oracle was informed to copy file
if ds_local_w is null then
    update
        pls_lote_imp_mat_unimed
    set
        dt_fim_importacao = null,
        dt_inicio_importacao = null,
        nm_usuario_imp = nm_usuario_p,
        ie_status = '4',
        ds_log = obter_desc_expressao(957116)
    where
        nr_sequencia = nr_seq_lote_imp_p;

    commit;

    return;
end if;

delete from
    w_pls_mat_unimed_xml;
commit;

-- Accomplish the file content copy from directory to a DB's column
begin
insert into w_pls_mat_unimed_xml(   nr_sequencia,   ds_arquivo,     dt_atualizacao,
                                    nm_usuario )
values (    1,    empty_clob(),   sysdate,
            nm_usuario_p ) 
return ds_arquivo into clob_w;

bfile_w := BFILENAME(ds_local_w, nm_arquivo_p);

dbms_lob.fileopen(bfile_w, dbms_lob.file_readonly);

dbms_lob.loadfromfile(clob_w, bfile_w, dbms_lob.getlength(bfile_w));

dbms_lob.fileclose(bfile_w);

commit;

exception
    when others then
        erro_w := obter_desc_expressao(957118) || '-' || SQLERRM;

        update
            pls_lote_imp_mat_unimed
        set
            dt_fim_importacao = null,
            dt_inicio_importacao = null,
            nm_usuario_imp = nm_usuario_p,
            ie_status = '4',
            ds_log = erro_w
        where
            nr_sequencia = nr_seq_lote_imp_p;

        commit;

        return;
end;

ds_comando_job_w := 'pls_gerar_import_mat_a900_new(' || nr_seq_lote_imp_p || ',' -- nr_seq_lote_imp_p 
|| pls_util_pck.aspas_w || nm_arquivo_p || pls_util_pck.aspas_w || ',' -- nm_arquivo_p
|| pls_util_pck.aspas_w || nm_usuario_p || pls_util_pck.aspas_w || ');';

/* This code will start the procedure that was declared in "ds_comando_job_w" variable, in 2.5 
seconds after the conclude from this routine, and always throws the next execution for 1000 days from
now*/
dbms_job.submit(
    jobno,
    ds_comando_job_w,
    sysdate + 2.5 * (1 / 24 / 60 / 60)
);

-- Attaches JOB id responsible for executing process
update
    pls_lote_imp_mat_unimed
set
    ie_status = '1',
    ds_log = null
where
    nr_sequencia = nr_seq_lote_imp_p;

commit;

end PLS_IMPORT_MATERIAL_A900_XML;

/