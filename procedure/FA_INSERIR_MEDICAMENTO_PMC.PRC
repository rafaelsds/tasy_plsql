create or replace
procedure fa_inserir_medicamento_pmc(	nr_sequencia_p		number,
					ds_lista_medic_p		varchar2,
					nm_usuario_p		Varchar2) is 

nr_sequencia_w	number(10,0);	
nr_seq_pac_pm_w	number(10,0);


nr_seq_medic_fa_w	number(10,0);
ds_lista_w	varchar2(1000);
tam_lista_w	number(10,0);
ie_pos_virgula_w	number(3,0);		     
			     					
begin
if	(nr_sequencia_p is not null) and (ds_lista_medic_p is not null) then
	ds_lista_w 	:= ds_lista_medic_p;
	nr_sequencia_w 	:= nr_sequencia_p;
	
	
	
	
	while (ds_lista_w is not null) loop 
		begin
		tam_lista_w		:= length(ds_lista_w);
		ie_pos_virgula_w		:= instr(ds_lista_w,',');
		if	(ie_pos_virgula_w <> 0) then
			nr_seq_medic_fa_w	:= to_number(substr(ds_lista_w,1,(ie_pos_virgula_w - 1)));
			ds_lista_w	:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;
		
		select	fa_paciente_pmc_medic_seq.nextval
		into	nr_seq_pac_pm_w
		from	dual;
		
		if	(nr_sequencia_w is not null) then
			begin
			
	
			insert into fa_paciente_pmc_medic(	nr_sequencia,           
							dt_atualizacao,         
							nm_usuario,             
							dt_atualizacao_nrec,    
							nm_usuario_nrec,        
							nr_seq_medic_fa,        
							nr_seq_fa_pmc)
						values   (nr_seq_pac_pm_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_medic_fa_w,
							nr_sequencia_w);
			end;
		end if;	
		end;
	end loop;
end if;	

commit;

end fa_inserir_medicamento_pmc;
/