create or replace
procedure Atualizar_Liberacao_Tuss_serv(nr_sequencia_p		number,
				       ie_acao_p		varchar2,
				       nm_usuario_p		Varchar2) is 

begin

if	(ie_acao_p = 'L') then
	update	tuss_servico
	set	dt_liberacao = sysdate,
		nm_usuario_liberacao = nm_usuario_p
	where 	nr_sequencia = nr_sequencia_p;
else
	update	tuss_servico
	set	dt_liberacao = null,
		nm_usuario_liberacao = null
	where 	nr_sequencia = nr_sequencia_p;
end if;

commit;

end Atualizar_Liberacao_Tuss_serv;
/