create or replace
procedure obter_dados_regitro_ivc_js(	nr_atendimento_p	Number,
				nm_usuario_p		Varchar2,
				nr_seq_irrigacao_p	out Number,
				ie_hor_esp_p		out Varchar2,
				ie_mensagem_p		out varchar2) is 

begin

nr_seq_irrigacao_p := obter_ivc_ativo_atend(nr_atendimento_p);

select  nvl(max('S'),'N')                        
into	ie_hor_esp_p
from    adep_irrigacao a,                             
	prescr_procedimento b,                        
	prescr_proc_hor c                             
where   a.nr_prescricao  	 = b.nr_prescricao    
and     a.nr_seq_procedimento    = b.nr_sequencia     
and     c.nr_seq_procedimento    = b.nr_sequencia     
and     c.nr_Prescricao          = b.nr_Prescricao    
and     c.ie_Horario_especial    = 'S'
and	a.nr_sequencia           = nr_seq_irrigacao_p;

select	nvl(max('N'),'S')
into	ie_mensagem_p
from	ADEP_IRRIGACAO_ETAPA a 
where	a.NR_SEQ_IRRIGACAO       = nr_seq_irrigacao_p;

end obter_dados_regitro_ivc_js;
/
