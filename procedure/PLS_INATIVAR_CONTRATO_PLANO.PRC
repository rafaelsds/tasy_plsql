create or replace
procedure pls_inativar_contrato_plano
			(	nr_sequencia_p		number,
				dt_inativacao_p		date,
				nr_seq_contrato_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is
				
nr_seq_plano_w		pls_contrato_plano.nr_seq_plano%type;

begin

update	pls_contrato_plano
set	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_inativacao	= dt_inativacao_p,
	ie_situacao	= 'I'
where	nr_sequencia	= nr_sequencia_p;

select 	nr_seq_plano
into	nr_seq_plano_w
from 	pls_contrato_plano
where	nr_sequencia	= nr_sequencia_p;

insert into pls_contrato_historico (
					nr_sequencia,
					cd_estabelecimento,
					nr_seq_contrato,
					dt_historico,
					ie_tipo_historico,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_historico)
				values(	pls_contrato_historico_seq.nextval,
					cd_estabelecimento_p,
					nr_seq_contrato_p,
					sysdate,
					'4',
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					'Inativação do produto ' || nr_seq_plano_w || ' - ' || dt_inativacao_p);
commit;

end pls_inativar_contrato_plano;
/
