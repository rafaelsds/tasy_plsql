create or replace
procedure ctb_pls_atualizar_prov_prod_in
			(	nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nr_seq_atualizacao_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				qt_movimento_p		in out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar as informacoes contabeis de provisao de pagamento de producao medica.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_erro_w			varchar2(4000);
cd_classificacao_credito_w	varchar2(255);
cd_classificacao_debito_w	varchar2(255);
ie_tipo_ato_w			varchar2(255)	:= '1';
cd_classificacao_cred_w		varchar2(255);
cd_classificacao_deb_w		varchar2(255);
ds_mascara_w			varchar2(40);
vl_fixo_w			varchar2(30);
cd_classificacao_item_w		varchar2(30);
cd_conta_credito_w		varchar2(20);
cd_conta_debito_w		varchar2(20);
cd_conta_contabil_w		varchar2(20);
cd_conta_cred_w			varchar2(20);
cd_conta_deb_w			varchar2(20);
ie_liminar_judicial_w		varchar2(15);
cd_cgc_prestador_w		varchar2(14);
cd_medico_executor_w		varchar2(10);
ie_tipo_prestador_w		varchar2(10);
ie_tipo_guia_ww			varchar2(10);
ie_tipo_segurado_ww		varchar2(10);
cd_pf_prestador_w		varchar2(10);
ie_tipo_prestador_atend_w	varchar2(10);
ie_tipo_prestador_exec_w	varchar2(10);
ie_tipo_prestador_solic_w	varchar2(10);
ie_grupo_despesa_ans_w		varchar2(10);
ie_tipo_segurado_w		varchar2(3);
ie_classif_grupo_w		varchar2(5);
ie_classif_grupo_ww		varchar2(5);
ie_preco_w			varchar2(2);
ie_tipo_relacao_w		varchar2(2);
ie_regulamentacao_w		varchar2(2);
ie_tipo_contratacao_w		varchar2(2);
ie_segmentacao_w		varchar2(2);
ie_tipo_contrato_w		varchar2(2);
ie_tipo_guia_w			varchar2(2);
ie_codificacao_w		varchar2(2);
ie_tipo_ptu_ww			varchar2(2);
ie_tipo_relacao_ww		varchar2(2);
ie_tipo_relacao_atend_w		varchar2(2);
ie_tipo_relacao_exec_w		varchar2(2);
ie_tipo_relacao_solic_w		varchar2(2);
ie_tipo_despesa_w		varchar2(1);
ie_regime_internacao_w		varchar2(1);
ie_ato_cooperado_w		varchar2(1);
ie_debito_credito_w		varchar2(1);
ie_prestador_codificacao_w	varchar2(1);
ie_tipo_repasse_w		varchar2(1);
ie_status_conta_w		varchar2(1);
cd_procedimento_w		number(15);
nr_seq_grupo_ans_w		number(10);
nr_seq_grupo_superior_w		number(10);
nr_seq_grupo_ans_ww		number(10);
nr_seq_plano_w			pls_plano.nr_sequencia%type;
nr_seq_esquema_w		number(10);
cd_historico_padrao_w		number(10);
cd_historico_rev_prov_w		number(10);
nr_seq_conta_item_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_tipo_atendimento_w	number(10);
nr_seq_conselho_w		number(10);
nr_seq_prestador_w		number(10);
nr_seq_tipo_prestador_w		number(10);
nr_seq_tipo_prestador_atend_w	number(10);
nr_seq_tipo_prestador_solic_w	number(10);
nr_seq_tipo_prestador_exec_w	number(10);
nr_seq_contrato_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_prestador_atend_w	number(10);
nr_seq_prestador_exec_w		number(10);
nr_seq_prestador_solic_w	number(10);
nr_seq_prestador_ww		number(10);
nr_seq_esquema_ww		number(10);
cd_historico_padrao_ww		number(10);
cd_historico_rev_prov_ww	number(10);
nr_seq_plano_ww			number(10);
nr_seq_tipo_prestador_ww	number(10);
nr_seq_material_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_intercambio_ww		number(10);
nr_seq_contrato_ww		number(10);
nr_seq_classificacao_sca_w	number(10);
nr_seq_classif_sca_ww		number(10);
nr_seq_fatura_w			number(10);
nr_seq_conta_proc_w		number(10);
nr_seq_conta_mat_w		number(10);
qt_movimento_w			number(10);
dt_referencia_w			date;
cd_empresa_w			estabelecimento.cd_empresa%type;
ie_lote_ajuste_prod_w		pls_parametro_contabil.ie_lote_ajuste_prod%type;
ie_tipo_movimento_w		varchar2(2);
nr_id_w				rowid;
nr_seq_tipo_conta_w		pls_conta.nr_seq_tipo_conta%type;

ie_tipo_vinculo_operadora_w	pls_esquema_contabil.ie_tipo_vinculo_operadora%type;
ie_tipo_vinculo_operadora_ww	pls_esquema_contabil.ie_tipo_vinculo_operadora%type;
cd_estab_setor_pessoa_ww	pls_esquema_contabil.cd_estab_setor_pessoa%type;
cd_estabelecimento_setor_w	setor_atendimento.cd_estabelecimento_base%type;
nr_seq_titular_w		pls_segurado.nr_seq_titular%type;
cd_pessoa_fisica_titular_w	pls_segurado.cd_pessoa_fisica%type;
cd_pessoa_fisica_w		pls_segurado.cd_pessoa_fisica%type;
ie_tipo_compartilhamento_w	pls_esquema_contabil.ie_tipo_compartilhamento%type;
ie_benef_remido_w		pls_esquema_contabil.ie_benef_remido%type;
nr_seq_congenere_w		pls_protocolo_conta.nr_seq_congenere%type;
dt_ref_repasse_w		date;
dt_repasse_w			date;
dt_fim_repasse_w		date;

Cursor c_itens_conta is
	select	rowid nr_id,
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		ie_tipo_despesa,
		nr_seq_grupo_ans,
		ie_ato_cooperado,
		null nr_seq_material,
		nvl(dt_procedimento, dt_referencia_w) dt_ref_repasse
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	((nr_sequencia = nr_seq_conta_proc_p) or (nr_seq_conta_proc_p is null))
	and	ie_status  not in ('D','M')
	union all
	select	rowid nr_id,
		nr_sequencia,
		null,
		null,
		ie_tipo_despesa,
		nr_seq_grupo_ans,
		ie_ato_cooperado,
		nr_seq_material,
		nvl(dt_atendimento, dt_referencia_w) dt_ref_repasse
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	((nr_sequencia = nr_seq_conta_mat_p) or (nr_seq_conta_mat_p is null))
	and	ie_status  not in ('D','M');

Cursor c_esquema is
	select	nr_sequencia,
		cd_historico_padrao,
		cd_historico_rev_prov,
		ie_tipo_ptu,
		nr_seq_prestador,
		ie_tipo_relacao,
		nvl(ie_prestador_codificacao,'E'),
		nr_seq_plano,
		ie_tipo_guia,
		nr_seq_tipo_prestador,
		nr_seq_intercambio,
		nr_seq_contrato,
		nr_seq_classif_sca,
		ie_tipo_vinculo_operadora,
		cd_estab_setor_pessoa
	from	pls_esquema_contabil
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_regra		= 'PP'
	and	dt_referencia_w between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_referencia_w)
	and	((ie_tipo_segurado	= ie_tipo_segurado_w) or (ie_tipo_segurado is null))
	and	((ie_tipo_repasse	= ie_tipo_repasse_w) or (ie_tipo_repasse is null))
	and	((ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento is null))
	and	((ie_liminar_judicial 	= ie_liminar_judicial_w) or (nvl(ie_liminar_judicial,'T') = 'T'))
	and	nvl(ie_tipo_movimentacao,ie_tipo_movimento_w)	= ie_tipo_movimento_w
	and	((ie_benef_remido	= ie_benef_remido_w) or (ie_benef_remido is null))
	and	((nr_seq_tipo_conta 	= nr_seq_tipo_conta_w) or (nr_seq_tipo_conta is null))
	order by
		nvl(nr_seq_prestador,0),
		nvl(nr_seq_classif_sca,0),
		nvl(nr_seq_plano,0),
		nvl(ie_tipo_guia,' '),
		nvl(nr_seq_contrato,0),
		nvl(nr_seq_intercambio,0),
		nvl(ie_tipo_relacao,' '),
		nvl(nr_seq_tipo_prestador,0),
		nvl(ie_tipo_ptu,' '),
		nvl(ie_tipo_compartilhamento,0),
		nvl(ie_tipo_repasse,' '),
		nvl(ie_tipo_segurado,' '),
		nvl(cd_conta_credito,'A'),
		nvl(cd_conta_debito,'A'),
		nvl(ie_esquema_contabil,0),
		nvl(ie_tipo_movimentacao,0),
		nvl(ie_liminar_judicial,'A'),
		nvl(ie_benef_remido,' '),
		nvl(ie_tipo_vinculo_operadora,' '),
		nvl(cd_estab_setor_pessoa,0),
		nvl(nr_seq_tipo_conta,0),
		nvl(dt_inicio_vigencia,sysdate);

Cursor c_segmentacao is
	select	ie_codificacao,
		vl_fixo,
		cd_conta_contabil,
		ie_debito_credito,
		ds_mascara
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema = nr_seq_esquema_w
	order by
		ie_debito_credito,
		nr_seq_apresentacao;

Cursor c_tipo_movimento is
	select	11 ie_tipo_movimento --Evento
	from	dual
	union
	select	18 ie_tipo_movimento -- Valor Ajuste
	from	dual
	where 	ie_lote_ajuste_prod_w = 'P';

begin
qt_movimento_w	:= qt_movimento_p;

select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(nvl(ie_lote_ajuste_prod,'R'))
into	ie_lote_ajuste_prod_w
from	pls_parametro_contabil
where	cd_estabelecimento	= cd_estabelecimento_p;

begin
select	d.ie_tipo_contratacao,
	d.ie_preco,
	d.ie_segmentacao,
	d.ie_regulamentacao,
	b.dt_mes_competencia,
	nvl(nvl(a.ie_tipo_segurado,c.ie_tipo_segurado),'B'),
	d.nr_sequencia,
	c.nr_seq_contrato,
	c.nr_seq_intercambio,
	b.nr_seq_prestador,
	a.nr_seq_prestador_exec,
	a.nr_seq_prestador,
	a.ie_tipo_guia,
	a.nr_seq_tipo_atendimento,
	a.cd_medico_executor,
	a.ie_regime_internacao,
	c.nr_sequencia,
	d.nr_seq_classificacao,
	a.ie_status,
	a.nr_seq_fatura,
	c.ie_tipo_vinculo_operadora,
	c.nr_seq_titular,
	c.cd_pessoa_fisica,
	(select decode(count(1), 0, 'N', 'S')
	from 	pls_guia_liminar_judicial 	g
	where	g.nr_seq_guia 	= a.nr_seq_guia) ie_liminar_judicial,
	pls_obter_se_benef_remido(c.nr_sequencia,b.dt_mes_competencia) ie_benef_remido,
	b.nr_seq_congenere,
	a.nr_seq_tipo_conta
into	ie_tipo_contratacao_w,
	ie_preco_w,
	ie_segmentacao_w,
	ie_regulamentacao_w,
	dt_referencia_w,
	ie_tipo_segurado_w,
	nr_seq_plano_w,
	nr_seq_contrato_w,
	nr_seq_intercambio_w,
	nr_seq_prestador_atend_w,
	nr_seq_prestador_exec_w,
	nr_seq_prestador_solic_w,
	ie_tipo_guia_w,
	nr_seq_tipo_atendimento_w,
	cd_medico_executor_w,
	ie_regime_internacao_w,
	nr_seq_segurado_w,
	nr_seq_classificacao_sca_w,
	ie_status_conta_w,
	nr_seq_fatura_w,
	ie_tipo_vinculo_operadora_w,
	nr_seq_titular_w,
	cd_pessoa_fisica_w,
	ie_liminar_judicial_w,
	ie_benef_remido_w,
	nr_seq_congenere_w,
	nr_seq_tipo_conta_w
from	pls_conta 		a,
	pls_protocolo_conta	b,
	pls_segurado		c,
	pls_plano 		d
where	a.nr_seq_protocolo	= b.nr_sequencia
and	a.nr_seq_segurado	= c.nr_sequencia(+)
and	a.nr_seq_plano		= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_conta_p
and	nvl(b.ie_tipo_protocolo,'C') in ('C', 'F')
and	a.ie_status		<> 'C'
and	b.ie_situacao not in ('I','RE','A')
and	rownum			<= 1;
exception
when others then
	ds_erro_w	:= '';
end;

if	(nr_seq_titular_w is null) then
	cd_pessoa_fisica_titular_w	:= cd_pessoa_fisica_w;
else
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_titular_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_titular_w;
end if;

select	max(cd_estabelecimento_base)
into	cd_estabelecimento_setor_w
from	pessoa_fisica_loc_trab	a,
	setor_atendimento	b
where	a.cd_setor_atendimento	= b.cd_setor_atendimento
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_titular_w
and	a.ie_local_principal	= 'S';

if	(nr_seq_fatura_w is not null) then
	select	max(a.dt_mes_competencia)
	into	dt_referencia_w
	from	ptu_fatura	a
	where	a.nr_sequencia	= nr_seq_fatura_w;
end if;

dt_referencia_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_referencia_w);

if	(nvl(nr_seq_contrato_w,0) <> 0) then
	select	decode(cd_pf_estipulante, null, 'PJ', 'PF')
	into	ie_tipo_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w
	and	rownum		<= 1;
elsif	(nvl(nr_seq_intercambio_w,0) <> 0) then
	select	decode(cd_pessoa_fisica, null, 'PJ', 'PF')
	into	ie_tipo_contrato_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_w
	and	rownum		<= 1;
end if;

/*Dados do prestador de atendimento*/
if	(nr_seq_prestador_atend_w is not null) then
	select	ie_tipo_relacao,
		nr_seq_tipo_prestador
	into	ie_tipo_relacao_atend_w,
		nr_seq_tipo_prestador_atend_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_atend_w
	and	rownum		<= 1;
	if	(nr_seq_tipo_prestador_atend_w is not null) then
		select	ie_tipo_ptu
		into	ie_tipo_prestador_atend_w
		from	pls_tipo_prestador
		where	nr_sequencia	= nr_seq_tipo_prestador_atend_w
		and	rownum		<= 1;
	end if;
end if;

/*Dados do prestador executante*/
if	(nr_seq_prestador_exec_w is not null) then
	select	ie_tipo_relacao,
		nr_seq_tipo_prestador
	into	ie_tipo_relacao_exec_w,
		nr_seq_tipo_prestador_exec_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_exec_w
	and	rownum		<= 1;
	if	(nr_seq_tipo_prestador_exec_w is not null) then
		select	ie_tipo_ptu
		into	ie_tipo_prestador_exec_w
		from	pls_tipo_prestador
		where	nr_sequencia	= nr_seq_tipo_prestador_exec_w
		and	rownum		<= 1;
	end if;
end if;

/*Dados do prestador solicitante*/
if	(nr_seq_prestador_solic_w is not null) then
	select	ie_tipo_relacao,
		nr_seq_tipo_prestador
	into	ie_tipo_relacao_solic_w,
		nr_seq_tipo_prestador_solic_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_solic_w
	and	rownum		<= 1;
	if	(nr_seq_tipo_prestador_solic_w is not null) then
		select	ie_tipo_ptu
		into	ie_tipo_prestador_solic_w
		from	pls_tipo_prestador
		where	nr_sequencia	= nr_seq_tipo_prestador_solic_w
		and	rownum		<= 1;
	end if;
end if;

nr_seq_conselho_w	:= null;

if	(cd_medico_executor_w is not null) then
	select	nr_seq_conselho
	into	nr_seq_conselho_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_medico_executor_w;
end if;

begin
select	cd_cgc,
	cd_pessoa_fisica
into	cd_cgc_prestador_w,
	cd_pf_prestador_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_w;
exception
when others then
	cd_cgc_prestador_w	:= 0;
	cd_pf_prestador_w	:= 0;
end;

open c_itens_conta;
loop
fetch c_itens_conta into
	nr_id_w,
	nr_seq_conta_item_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_tipo_despesa_w,
	nr_seq_grupo_ans_w,
	ie_ato_cooperado_w,
	nr_seq_material_w,
	dt_ref_repasse_w;
exit when c_itens_conta%notfound;
	begin
	cd_classificacao_credito_w	:= null;
	cd_classificacao_debito_w	:= null;
	cd_conta_credito_w		:= null;
	cd_conta_debito_w		:= null;
	
	pls_obter_dados_repasse(	dt_ref_repasse_w,
					nr_seq_segurado_w,
					nr_seq_congenere_w,
					ie_tipo_repasse_w,
					ie_tipo_compartilhamento_w,
					dt_repasse_w,
					dt_fim_repasse_w);
	
	/* GRUPO ANS */
	/*
	if	(nvl(nr_seq_grupo_ans_w,0) = 0) then
		if	(nvl(nr_seq_material_w,0) > 0) then
			select	nvl(max(ie_tipo_despesa),'')
			into	ie_tipo_despesa_w
			from	pls_material
			where	nr_sequencia	= nr_seq_material_w;
		end if;
		nr_seq_grupo_ans_w	:= pls_obter_grupo_ans(	cd_procedimento_w,
								ie_origem_proced_w,
								nr_seq_conselho_w,
								nr_seq_tipo_atendimento_w,
								ie_tipo_guia_w,
								ie_regime_internacao_w,
								ie_tipo_despesa_w,
								'G',
								nvl(cd_estabelecimento_p, 0));
	end if;
	*/
	/* 5 - Grupo ANS com base nos valores do ITAMED */
	if	(nvl(nr_seq_grupo_ans_w,0) > 0) then
		begin
		select	nr_seq_grupo_superior
		into	nr_seq_grupo_superior_w
		from	ans_grupo_despesa
		where	nr_sequencia	= nr_seq_grupo_ans_w;
		exception
		when others then
			nr_seq_grupo_superior_w	:= null;
		end;
	end if;
	if	(nvl(nr_seq_grupo_superior_w, 0) = 0) then
		nr_seq_grupo_ans_ww	:= nr_seq_grupo_ans_w;
	else
		nr_seq_grupo_ans_ww	:= nr_seq_grupo_superior_w;
	end if;
	begin
	select	ie_tipo_grupo_ans
	into	ie_grupo_despesa_ans_w
	from	ans_grupo_despesa
	where	nr_sequencia	= nr_seq_grupo_ans_ww;
	exception
	when others then
		ie_grupo_despesa_ans_w	:= null;
	end;
	if	(ie_grupo_despesa_ans_w = 10) then /* 1 - Consultas */
		ie_classif_grupo_w	:= '1';
		ie_classif_grupo_ww	:= '0';
	elsif	(ie_grupo_despesa_ans_w = 20) then /* 49 - Exames */
		ie_classif_grupo_w	:= '2';
		ie_classif_grupo_ww	:= '0';
	elsif	(ie_grupo_despesa_ans_w = 30) then /* 51 - Terapias */
		ie_classif_grupo_w	:= '3';
		ie_classif_grupo_ww	:= '0';
	elsif	(ie_grupo_despesa_ans_w = 41) then /* 7 - Internacao - Honorario medico */
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '1';
	elsif	(ie_grupo_despesa_ans_w = 42) then /* 8 - Internacao - Exames */
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '2';
	elsif	(ie_grupo_despesa_ans_w = 43) then /* 9 - Internacao - Terapias*/
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '3';
	elsif	(ie_grupo_despesa_ans_w = 44) then /* 10 - Internacao - Materiais medicos */
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '4';
	elsif	(ie_grupo_despesa_ans_w = 45) then /* 11 - Internacao - Medicamentos */
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '5';
	elsif	(ie_grupo_despesa_ans_w = 46) then /* 11 - Internacao - Procedimentos com preco fixo */
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '6';
	elsif	(ie_grupo_despesa_ans_w = 49) then /* 12 - Internacao - Outras despesas */
		ie_classif_grupo_w	:= '4';
		ie_classif_grupo_ww	:= '9';
	elsif	(ie_grupo_despesa_ans_w = 50) then /* 6 - Outros atendimentos - Ambulatoriais */
		ie_classif_grupo_w	:= '5';
		ie_classif_grupo_ww	:= '0';
	elsif	(ie_grupo_despesa_ans_w = 60) then /* 16 - Demais despesas assistenciais */
		ie_classif_grupo_w	:= '6';
		ie_classif_grupo_ww	:= '0';
	end if;
	/* FIM GRUPO ANS */
	open c_tipo_movimento;
	loop
	fetch c_tipo_movimento into
		ie_tipo_movimento_w;
	exit when c_tipo_movimento%notfound;
		begin
		nr_seq_esquema_w		:= null;
		cd_conta_credito_w		:= null;
		cd_conta_debito_w		:= null;
		cd_historico_padrao_w		:= null;
		cd_classificacao_credito_w	:= null;
		cd_classificacao_debito_w	:= null;
		open c_esquema;
		loop
		fetch c_esquema into
			nr_seq_esquema_ww,
			cd_historico_padrao_ww,
			cd_historico_rev_prov_ww,
			ie_tipo_ptu_ww,
			nr_seq_prestador_ww,
			ie_tipo_relacao_ww,
			ie_prestador_codificacao_w,
			nr_seq_plano_ww,
			ie_tipo_guia_ww,
			nr_seq_tipo_prestador_ww,
			nr_seq_intercambio_ww,
			nr_seq_contrato_ww,
			nr_seq_classif_sca_ww,
			ie_tipo_vinculo_operadora_ww,
			cd_estab_setor_pessoa_ww;
		exit when c_esquema%notfound;
			begin
			if	(ie_prestador_codificacao_w = 'E') then
				nr_seq_prestador_w	:= nr_seq_prestador_exec_w;
				ie_tipo_relacao_w	:= ie_tipo_relacao_exec_w;
				ie_tipo_prestador_w	:= ie_tipo_prestador_exec_w;
				nr_seq_tipo_prestador_w	:= nr_seq_tipo_prestador_exec_w;
			elsif	(ie_prestador_codificacao_w = 'S') then
				nr_seq_prestador_w	:= nr_seq_prestador_solic_w;
				ie_tipo_relacao_w	:= ie_tipo_relacao_solic_w;
				ie_tipo_prestador_w	:= ie_tipo_prestador_solic_w;
				nr_seq_tipo_prestador_w	:= nr_seq_tipo_prestador_solic_w;
			else
				nr_seq_prestador_w	:= nr_seq_prestador_atend_w;
				ie_tipo_relacao_w	:= ie_tipo_relacao_atend_w;
				ie_tipo_prestador_w	:= ie_tipo_prestador_atend_w;
				nr_seq_tipo_prestador_w	:= nr_seq_tipo_prestador_atend_w;
			end if;
			if	((nvl(nr_seq_prestador_ww,nvl(nr_seq_prestador_w,0)) = nvl(nr_seq_prestador_w,0)) and
				(nvl(nr_seq_tipo_prestador_ww,nvl(nr_seq_tipo_prestador_w,0)) = nvl(nr_seq_tipo_prestador_w,0)) and
				(nvl(ie_tipo_ptu_ww,nvl(ie_tipo_prestador_w,'0')) = nvl(ie_tipo_prestador_w,'0')) and
				(nvl(nr_seq_plano_ww,nvl(nr_seq_plano_w,0)) = nvl(nr_seq_plano_w,0)) and
				(nvl(ie_tipo_guia_ww,nvl(ie_tipo_guia_w,'X')) = nvl(ie_tipo_guia_w,'X')) and
				(nvl(ie_tipo_relacao_ww,nvl(ie_tipo_relacao_w,'X')) = nvl(ie_tipo_relacao_w,'X')) and
				(nvl(nr_seq_intercambio_ww,nvl(nr_seq_intercambio_w,0)) = nvl(nr_seq_intercambio_w,0)) and
				(nvl(nr_seq_contrato_ww,nvl(nr_seq_contrato_w,0)) = nvl(nr_seq_contrato_w,0)) and
				(nvl(nr_seq_classif_sca_ww,nvl(nr_seq_classificacao_sca_w,0)) = nvl(nr_seq_classificacao_sca_w,0)) and
				(nvl(ie_tipo_vinculo_operadora_ww,nvl(ie_tipo_vinculo_operadora_w,'X')) = nvl(ie_tipo_vinculo_operadora_w,'X')) and
				(nvl(cd_estab_setor_pessoa_ww,nvl(cd_estabelecimento_setor_w,0)) = nvl(cd_estabelecimento_setor_w,0))) then
				nr_seq_esquema_w	:= nr_seq_esquema_ww;
				cd_historico_padrao_w	:= cd_historico_padrao_ww;
				cd_historico_rev_prov_w	:= cd_historico_rev_prov_ww;
			end if;
			
			end;
		end loop;
		close c_esquema;
		open c_segmentacao;
		loop
		fetch c_segmentacao into
			ie_codificacao_w,
			vl_fixo_w,
			cd_conta_contabil_w,
			ie_debito_credito_w,
			ds_mascara_w;
		exit when c_segmentacao%notfound;
			begin
			cd_classificacao_item_w	:= null;
			if	(ie_debito_credito_w = 'C') then /* Classificacao CREDITO */
				if	(ie_codificacao_w = 'CR') then /* Codigo reduzido */
					begin
					select	cd_classificacao_atual
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
					exception
						when others then
						cd_classificacao_credito_w	:= null;
					end;
					cd_conta_credito_w	:= cd_conta_contabil_w;
				elsif	(ie_codificacao_w = 'JP') then /* Conta de pagamento da pessoa prestador */
					if	(nvl(cd_cgc_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_jur_conta_cont a
						where	a.cd_cgc	= cd_cgc_prestador_w
						and	a.cd_empresa	= cd_empresa_w
						and	a.ie_tipo_conta	= 'P'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					elsif	(nvl(cd_pf_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_fisica_conta_ctb	a
						where	a.cd_pessoa_fisica 	= cd_pf_prestador_w
						and	a.cd_empresa		= cd_empresa_w
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
						and	a.ie_tipo_conta		= 'P'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					end if;
					select	max(cd_classificacao_atual)
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
					cd_conta_credito_w	:= cd_conta_contabil_w;
				elsif	(ie_codificacao_w = 'FX') then /* Fixo */
					cd_classificacao_item_w	:= vl_fixo_w;
				elsif	(ie_codificacao_w = 'FP') then /* Formacao de Preco */
					if	(ie_preco_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_formacao_preco(ie_preco_w);
					else
						cd_classificacao_item_w	:= 'FP';
					end if;
				elsif	(ie_codificacao_w = 'R') then /* Regulamentacao */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(ie_regulamentacao_w);
				elsif	(ie_codificacao_w = 'TC') then /* Tipo de contratacao */
					if	(ie_tipo_contratacao_w in ('I','CE','CA')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_contratacao(ie_tipo_contratacao_w);
					else
						cd_classificacao_item_w	:= 'TC';
					end if;
				elsif	(ie_codificacao_w = 'TP') then /* Tipo de Contrato (Pessoa fisica ou Juridica) */
					if	(ie_tipo_contrato_w in ('PF','PJ')) then
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
					else
						cd_classificacao_item_w	:= 'TP';
					end if;
				elsif	(ie_codificacao_w = 'S') then /* Segmentacaoo */
					cd_classificacao_item_w	:= lpad(ie_segmentacao_w,2,'0');
				elsif	(ie_codificacao_w = 'TA') then /* Tipo de ato cooperado */
					if	(ie_ato_cooperado_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_ato_cooperado(ie_ato_cooperado_w);
					else
						cd_classificacao_item_w	:= 'TA';
					end if;
				elsif	(ie_codificacao_w = 'TR') then /* Tipo de relacao com a OPS */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_relacao(ie_tipo_relacao_w);
				elsif	(ie_codificacao_w = 'GA') then /* Grupo ANS */
					if	(ie_classif_grupo_w is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_w;
					else
						cd_classificacao_item_w	:= 'GA';
					end if;
				elsif	(ie_codificacao_w = 'CG') then /* Complemento grupo ANS */
					if	(ie_classif_grupo_ww is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_ww;
					else
						cd_classificacao_item_w	:= 'CG';
					end if;
				elsif	(ie_codificacao_w = 'RC') then /* Tipo de contratacao / Regulamentacao */
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(ie_tipo_contratacao_w,ie_regulamentacao_w);
				elsif   (ie_codificacao_w = 'TD') then                                 
					if (nr_seq_material_w is null) then
						if (ie_tipo_despesa_w is not null) then
                                        		cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_despesa(ie_tipo_despesa_w);
						else
							cd_classificacao_item_w := 'TD';
						end if;
					else
						cd_classificacao_item_w := 1;
					end if;
                                end if;
				if	(cd_classificacao_item_w is not null) then
					if	(ds_mascara_w = '00') then
						cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
					elsif	(ds_mascara_w = '0.0') then
						cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
					elsif	(ds_mascara_w = '0_') then
						cd_classificacao_item_w	:= cd_classificacao_item_w;
					else
						cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
					end if;
					cd_classificacao_credito_w	:= cd_classificacao_credito_w || cd_classificacao_item_w;
				end if;
			elsif	(ie_debito_credito_w = 'D') then /* Classificacao DEBITO */
				if	(ie_codificacao_w = 'CR') then /* Codigo reduzido */
					begin
					select	cd_classificacao_atual
					into	cd_classificacao_debito_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
					exception
						when others then
						cd_classificacao_debito_w	:= null;
					end;
					cd_conta_debito_w	:= cd_conta_contabil_w;
				elsif	(ie_codificacao_w = 'JP') then /* Conta de pagamento da pessoa prestador */
					if	(nvl(cd_cgc_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_jur_conta_cont a
						where	a.cd_cgc	= cd_cgc_prestador_w
						and	a.cd_empresa	= cd_empresa_w
						and	a.ie_tipo_conta	= 'P'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					elsif	(nvl(cd_pf_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_fisica_conta_ctb	a
						where	a.cd_pessoa_fisica 	= cd_pf_prestador_w
						and	a.cd_empresa		= cd_empresa_w
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
						and	a.ie_tipo_conta		= 'P'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					end if;
					select	max(cd_classificacao_atual)
					into	cd_classificacao_debito_w
					from	conta_contabil
					where	cd_conta_contabil	= cd_conta_contabil_w;
					cd_conta_debito_w	:= cd_conta_contabil_w;
				elsif	(ie_codificacao_w = 'FX') then /* Fixo */
					cd_classificacao_item_w	:= vl_fixo_w;
				elsif	(ie_codificacao_w = 'FP') then /* Formacao de Preco */
					if	(ie_preco_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_formacao_preco(ie_preco_w);
					else
						cd_classificacao_item_w	:= 'FP';
					end if;
				elsif	(ie_codificacao_w = 'R') then /* Regulamentacao */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(ie_regulamentacao_w);
				elsif	(ie_codificacao_w = 'TC') then /* Tipo de contratacao */
					if	(ie_tipo_contratacao_w in ('I','CE','CA')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_contratacao(ie_tipo_contratacao_w);
					else
						cd_classificacao_item_w	:= 'TC';
					end if;
				elsif	(ie_codificacao_w = 'S') then /* Segmentacaoo */
					cd_classificacao_item_w	:= lpad(ie_segmentacao_w,2,'0');
				elsif	(ie_codificacao_w = 'TP') then /* Tipo de Contrato (Pessoa fisica ou Juridica) */
					if	(ie_tipo_contrato_w in ('PF','PJ')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
					else
						cd_classificacao_item_w	:= 'TP';
					end if;
				elsif	(ie_codificacao_w = 'TA') then /* Tipo de ato cooperado */
					if	(ie_ato_cooperado_w in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_ato_cooperado(ie_ato_cooperado_w);
					else
						cd_classificacao_item_w	:= 'TA';
					end if;
				elsif	(ie_codificacao_w = 'TR') then /* Tipo de relacao com a OPS */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_relacao(ie_tipo_relacao_w);
				elsif	(ie_codificacao_w = 'GA') then /* Grupo ANS */
					if	(ie_classif_grupo_w is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_w;
					else
						cd_classificacao_item_w	:= 'GA';
					end if;
				elsif	(ie_codificacao_w = 'CG') then /* Complemento grupo ANS */
					if	(ie_classif_grupo_ww is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_ww;
					else
						cd_classificacao_item_w	:= 'CG';
					end if;
				elsif	(ie_codificacao_w = 'RC') then /* Tipo de contratacao / Regulamentacao */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(ie_tipo_contratacao_w,ie_regulamentacao_w);
                                elsif   (ie_codificacao_w = 'TD') then
					if (nr_seq_material_w is null) then
						if (ie_tipo_despesa_w is not null) then
                                        		cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_despesa(ie_tipo_despesa_w);
						else
							cd_classificacao_item_w := 'TD';
						end if;
					else
						cd_classificacao_item_w := 1;
					end if;
                                end if;
				if	(cd_classificacao_item_w is not null) then
					if	(ds_mascara_w = '00') then
						cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
					elsif	(ds_mascara_w = '0.0') then
						cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
					elsif	(ds_mascara_w = '0_') then
						cd_classificacao_item_w	:= cd_classificacao_item_w;
					else
						cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
					end if;
					cd_classificacao_debito_w	:= cd_classificacao_debito_w || cd_classificacao_item_w;
				end if;
			end if;
			end;
		end loop;
		close c_segmentacao;
		/* Remover o ultimo ponto da classificacao */
		if	(substr(cd_classificacao_credito_w,length(cd_classificacao_credito_w),length(cd_classificacao_credito_w)) = '.') then
			cd_classificacao_credito_w	:= substr(cd_classificacao_credito_w,1,length(cd_classificacao_credito_w)-1);
		end if;
		if	(substr(cd_classificacao_debito_w,length(cd_classificacao_debito_w),length(cd_classificacao_debito_w)) = '.') then
			cd_classificacao_debito_w	:= substr(cd_classificacao_debito_w,1,length(cd_classificacao_debito_w)-1);
		end if;
		if	(cd_conta_credito_w is null) then
			begin
			cd_conta_credito_w	:= ctb_obter_conta_classif(cd_classificacao_credito_w,dt_referencia_w,cd_estabelecimento_p);
			exception
			when others then
				cd_conta_credito_w	:= null;
			end;
		end if;
		if	(cd_conta_debito_w is null) then
			begin
			cd_conta_debito_w	:= ctb_obter_conta_classif(cd_classificacao_debito_w,dt_referencia_w,cd_estabelecimento_p);
			exception
			when others then
				cd_conta_debito_w	:= null;
			end;
		end if;
		nr_seq_conta_proc_w	:= null;
		nr_seq_conta_mat_w	:= null;
		if 	((cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
			nr_seq_conta_proc_w	:= nr_seq_conta_item_w;
			if	(ie_tipo_movimento_w = 11) then
				update	pls_conta_proc
				set	cd_conta_provisao_cred		= cd_conta_credito_w,
					cd_conta_provisao_deb		= cd_conta_debito_w,
					cd_classif_prov_cred		= cd_classificacao_credito_w,
					cd_classif_prov_deb		= cd_classificacao_debito_w,
					nr_seq_esquema_prov		= nr_seq_esquema_w,
					cd_historico_prov		= cd_historico_padrao_w,
					cd_historico_rev_prov		= cd_historico_rev_prov_w,
					nr_seq_grupo_ans		= nr_seq_grupo_ans_ww
				where	rowid				= nr_id_w;
			elsif	(ie_tipo_movimento_w = 18) then
				update	pls_conta_proc
				set	cd_historico_ajuste		= cd_historico_padrao_w
				where	rowid				= nr_id_w;
			end if;
			qt_movimento_w	:= qt_movimento_w + 1;
		elsif 	(nr_seq_material_w is not null) then
			nr_seq_conta_mat_w	:= nr_seq_conta_item_w;
			if	(ie_tipo_movimento_w = 11) then
				update	pls_conta_mat
				set	cd_conta_provisao_cred		= cd_conta_credito_w,
					cd_conta_provisao_deb		= cd_conta_debito_w,
					cd_classif_prov_cred		= cd_classificacao_credito_w,
					cd_classif_prov_deb		= cd_classificacao_debito_w,
					nr_seq_esquema_prov		= nr_seq_esquema_w,
					cd_historico_prov		= cd_historico_padrao_w,
					cd_historico_rev_prov		= cd_historico_rev_prov_w,
					nr_seq_grupo_ans		= nr_seq_grupo_ans_ww
				where	rowid				= nr_id_w;
			elsif	(ie_tipo_movimento_w = 18) then
				update	pls_conta_mat
				set	cd_historico_ajuste		= cd_historico_padrao_w
				where	rowid				= nr_id_w;
			end if;
			qt_movimento_w	:= qt_movimento_w + 1;
		end if;

		if	(nr_seq_atualizacao_p is not null) and
			(ie_status_conta_w <> 'C') then
			if	(nr_seq_esquema_w is null) then
				pls_gravar_mov_contabil(nr_seq_atualizacao_p,
							1,
							null,
							nr_seq_conta_proc_w,
							nr_seq_conta_mat_w,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							nm_usuario_p,
							nr_seq_esquema_w);
			elsif	((cd_conta_credito_w is null) or (cd_conta_debito_w is null)) then
				pls_gravar_mov_contabil(nr_seq_atualizacao_p,
							2,
							null,
							nr_seq_conta_proc_w,
							nr_seq_conta_mat_w,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							nm_usuario_p,
							nr_seq_esquema_w);
                        elsif   (nvl(nr_seq_plano_w, 0) = 0) then
                                pls_gravar_mov_contabil(nr_seq_atualizacao_p,
							8,
							null,
							nr_seq_conta_proc_w,
							nr_seq_conta_mat_w,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							nm_usuario_p,
							nr_seq_esquema_w);
			end if;
		end if;
		end;
	end loop;
	close c_tipo_movimento;
	end;
end loop;
close c_itens_conta;

qt_movimento_p	:= qt_movimento_w;
--commit;

end ctb_pls_atualizar_prov_prod_in;
/