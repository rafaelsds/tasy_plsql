create or replace
procedure pls_atualizar_total_cedida      ( nm_usuario_p		varchar2,
					    nr_seq_superior_p	        number,
					    nr_seq_periodo_p	        number) is

vl_mes_n_w			diops_movto_corresp_cedida.vl_mes_n%type;
vl_mes_n1_w			diops_movto_corresp_cedida.vl_mes_n1%type;
vl_mes_n2_w			diops_movto_corresp_cedida.vl_mes_n2%type;
vl_mes_n3_w			diops_movto_corresp_cedida.vl_mes_n3%type;
vl_mes_n4_w			diops_movto_corresp_cedida.vl_mes_n4%type;
vl_mes_n5_w			diops_movto_corresp_cedida.vl_mes_n5%type;
vl_mes_n6_w                     diops_movto_corresp_cedida.vl_mes_n6%type;
vl_mes_n7_w                     diops_movto_corresp_cedida.vl_mes_n7%type;
vl_mes_n8_w                     diops_movto_corresp_cedida.vl_mes_n8%type;
vl_mes_n9_w                     diops_movto_corresp_cedida.vl_mes_n9%type;
vl_mes_n10_w                    diops_movto_corresp_cedida.vl_mes_n10%type;
vl_mes_n11_w                    diops_movto_corresp_cedida.vl_mes_n11%type;
vl_mes_n12_w                    diops_movto_corresp_cedida.vl_mes_n12%type;
vl_mes_n13_w                    diops_movto_corresp_cedida.vl_mes_n13%type;
vl_mes_n14_w                    diops_movto_corresp_cedida.vl_mes_n14%type;
vl_mes_n15_w                    diops_movto_corresp_cedida.vl_mes_n15%type;
vl_mes_n16_w                    diops_movto_corresp_cedida.vl_mes_n16%type;
vl_mes_n17_w                    diops_movto_corresp_cedida.vl_mes_n17%type;
vl_mes_n18_w                    diops_movto_corresp_cedida.vl_mes_n18%type;
vl_mes_n19_w                    diops_movto_corresp_cedida.vl_mes_n19%type;
vl_mes_n20_w                    diops_movto_corresp_cedida.vl_mes_n20%type;
vl_mes_n21_w			diops_movto_corresp_cedida.vl_mes_n21%type;
vl_mes_n22_w			diops_movto_corresp_cedida.vl_mes_n22%type;
vl_mes_n23_w			diops_movto_corresp_cedida.vl_mes_n23%type;
vl_mes_n24_w			diops_movto_corresp_cedida.vl_mes_n24%type;
vl_mes_n25_w			diops_movto_corresp_cedida.vl_mes_n25%type;
vl_mes_n26_w			diops_movto_corresp_cedida.vl_mes_n26%type;
vl_mes_n27_w			diops_movto_corresp_cedida.vl_mes_n27%type;
vl_mes_n28_w			diops_movto_corresp_cedida.vl_mes_n28%type;
vl_mes_n29_w			diops_movto_corresp_cedida.vl_mes_n29%type;
vl_mes_n30_w			diops_movto_corresp_cedida.vl_mes_n30%type;
vl_mes_n31_w			diops_movto_corresp_cedida.vl_mes_n31%type;
vl_mes_n32_w			diops_movto_corresp_cedida.vl_mes_n32%type;
vl_mes_n33_w			diops_movto_corresp_cedida.vl_mes_n33%type;
vl_mes_n34_w			diops_movto_corresp_cedida.vl_mes_n34%type;
vl_mes_n35_w			diops_movto_corresp_cedida.vl_mes_n35%type;
nr_seq_superior_w		diops_movto_corresp_cedida.nr_seq_tipo_movto%type;

begin
wheb_usuario_pck.set_ie_executar_trigger('N');

nr_seq_superior_w	:= nr_seq_superior_p;

begin

	select	sum(a.vl_mes_n) vl_mes_n,
		sum(a.vl_mes_n1) vl_mes_n1,
		sum(a.vl_mes_n2) vl_mes_n2,
		sum(a.vl_mes_n3) vl_mes_n3,
		sum(a.vl_mes_n4) vl_mes_n4,
		sum(a.vl_mes_n5) vl_mes_n5,
		sum(a.vl_mes_n6) vl_mes_n6,
		sum(a.vl_mes_n7) vl_mes_n7,
		sum(a.vl_mes_n8) vl_mes_n8,
		sum(a.vl_mes_n9) vl_mes_n9,
		sum(a.vl_mes_n10) vl_mes_n10,
		sum(a.vl_mes_n11) vl_mes_n11,
		sum(a.vl_mes_n12) vl_mes_n12,
		sum(a.vl_mes_n13) vl_mes_n13,
		sum(a.vl_mes_n14) vl_mes_n14,
		sum(a.vl_mes_n15) vl_mes_n15,
		sum(a.vl_mes_n16) vl_mes_n16,
		sum(a.vl_mes_n17) vl_mes_n17,
		sum(a.vl_mes_n18) vl_mes_n18,
		sum(a.vl_mes_n19) vl_mes_n19,
		sum(a.vl_mes_n20) vl_mes_n20,
		sum(a.vl_mes_n21) vl_mes_n21,
		sum(a.vl_mes_n22) vl_mes_n22,
		sum(a.vl_mes_n23) vl_mes_n23,
		sum(a.vl_mes_n24) vl_mes_n24,
		sum(a.vl_mes_n25) vl_mes_n25,
		sum(a.vl_mes_n26) vl_mes_n26,
		sum(a.vl_mes_n27) vl_mes_n27,
		sum(a.vl_mes_n28) vl_mes_n28,
		sum(a.vl_mes_n29) vl_mes_n29,
		sum(a.vl_mes_n30) vl_mes_n30,
		sum(a.vl_mes_n31) vl_mes_n31,
		sum(a.vl_mes_n32) vl_mes_n32,
		sum(a.vl_mes_n33) vl_mes_n33,
		sum(a.vl_mes_n34) vl_mes_n34,
		sum(a.vl_mes_n35) vl_mes_n35
	into	vl_mes_n_w,
		vl_mes_n1_w,
		vl_mes_n2_w,
		vl_mes_n3_w,
		vl_mes_n4_w,
		vl_mes_n5_w,
		vl_mes_n6_w,
		vl_mes_n7_w,
		vl_mes_n8_w,
		vl_mes_n9_w,
		vl_mes_n10_w,
		vl_mes_n11_w,
		vl_mes_n12_w,
		vl_mes_n13_w,
		vl_mes_n14_w,
		vl_mes_n15_w,
		vl_mes_n16_w,
		vl_mes_n17_w,
		vl_mes_n18_w,
		vl_mes_n19_w,
		vl_mes_n20_w,
		vl_mes_n21_w,
		vl_mes_n22_w,
		vl_mes_n23_w,
		vl_mes_n24_w,
		vl_mes_n25_w,
		vl_mes_n26_w,
		vl_mes_n27_w,
		vl_mes_n28_w,
		vl_mes_n29_w,
		vl_mes_n30_w,
		vl_mes_n31_w,
		vl_mes_n32_w,
		vl_mes_n33_w,
		vl_mes_n34_w,
		vl_mes_n35_w
	from	diops_movto_corresp_cedida		a,
		diops_tipo_movto_pel	                b
	where	b.nr_sequencia		= a.nr_seq_tipo_movto
	and	b.nr_seq_superior	= nr_seq_superior_w
	and	a.nr_seq_periodo	= nr_seq_periodo_p;
exception
when others then
	nr_seq_superior_w	:= null;
end;
if	(nvl(nr_seq_superior_w,0) > 0) then
	update	diops_movto_corresp_cedida
	set	nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		vl_mes_n		= vl_mes_n_w,
		vl_mes_n1		= vl_mes_n1_w,
		vl_mes_n2		= vl_mes_n2_w,
		vl_mes_n3		= vl_mes_n3_w,
		vl_mes_n4		= vl_mes_n4_w,
		vl_mes_n5		= vl_mes_n5_w,
		vl_mes_n6		= vl_mes_n6_w,
		vl_mes_n7		= vl_mes_n7_w,
		vl_mes_n8		= vl_mes_n8_w,
		vl_mes_n9		= vl_mes_n9_w,
		vl_mes_n10		= vl_mes_n10_w,
		vl_mes_n11		= vl_mes_n11_w,
		vl_mes_n12		= vl_mes_n12_w,
		vl_mes_n13		= vl_mes_n13_w,
		vl_mes_n14		= vl_mes_n14_w,
		vl_mes_n15		= vl_mes_n15_w,
		vl_mes_n16		= vl_mes_n16_w,
		vl_mes_n17		= vl_mes_n17_w,
		vl_mes_n18		= vl_mes_n18_w,
		vl_mes_n19		= vl_mes_n19_w,
		vl_mes_n20		= vl_mes_n20_w,
		vl_mes_n21		= vl_mes_n21_w,
		vl_mes_n22		= vl_mes_n22_w,
		vl_mes_n23		= vl_mes_n23_w,
		vl_mes_n24		= vl_mes_n24_w,
		vl_mes_n25		= vl_mes_n25_w,
		vl_mes_n26		= vl_mes_n26_w,
		vl_mes_n27		= vl_mes_n27_w,
		vl_mes_n28		= vl_mes_n28_w,
		vl_mes_n29		= vl_mes_n29_w,
		vl_mes_n30		= vl_mes_n30_w,
		vl_mes_n31		= vl_mes_n31_w,
		vl_mes_n32		= vl_mes_n32_w,
		vl_mes_n33		= vl_mes_n33_w,
		vl_mes_n34		= vl_mes_n34_w,
		vl_mes_n35		= vl_mes_n35_w
	where	nr_seq_tipo_movto	= nr_seq_superior_w
	and	nr_seq_periodo		= nr_seq_periodo_p;
	
	
end if;

wheb_usuario_pck.set_ie_executar_trigger('S');

commit;

end pls_atualizar_total_cedida;
/