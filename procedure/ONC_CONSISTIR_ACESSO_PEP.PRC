create or replace
procedure onc_consistir_acesso_pep(	nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number,
					cd_paciente_p		varchar2,
					cd_pessoa_fisica_p	varchar2,
					dt_logon_p		date,
					ie_permite_p	  out	Varchar2,
					nr_seq_mensagem_p out	Number) is 
					
nr_atendimento_w	number(10);
ie_permite_w		varchar2(5); 
nr_seq_mensagem_w	number(10);
nr_seq_lib_acesso_w		number(10);
ie_lib_acesso_w			varchar2(1);


begin

if 	(cd_paciente_p is not null) then
	select 	nvl(max(nr_atendimento),0) 
	into	nr_atendimento_w
	from	atendimento_paciente
	where	cd_pessoa_fisica	=	cd_paciente_p
	and	dt_alta is null;
	
	if 	(nr_atendimento_w is not null) then
		Consistir_acesso_pep( 	cd_estabelecimento_P, 
					nr_atendimento_w, 
					cd_paciente_p, 
					cd_pessoa_fisica_p, 
					nm_usuario_p,
					dt_logon_p,
					ie_permite_w, 
					nr_seq_mensagem_w,
					nr_seq_lib_acesso_w,
					ie_lib_acesso_w);
	else
		Consistir_acesso_pep(	cd_estabelecimento_P, 
					0, 
					cd_paciente_p, 
					cd_pessoa_fisica_p, 
					nm_usuario_p,
					dt_logon_p,
					ie_permite_w, 
					nr_seq_mensagem_w,
					nr_seq_lib_acesso_w,
					ie_lib_acesso_w);
	end if;
	
	ie_permite_p	  	:= 	ie_permite_w;
	nr_seq_mensagem_p 	:=	nr_seq_mensagem_w;
end if;

end onc_Consistir_acesso_pep;
/