create or replace
procedure Gravar_hist_cominic_cobranc (
			nr_lote_p		number,
			nr_interno_conta_p	number,
			ie_somente_visualiza_p	varchar2,
			ie_tipo_p		varchar2,
			nm_usuario_p		Varchar2) is
			
/*
ie_tipo_p
AR - Relat�rio de AR (Formul�rio do Correio)
NF - Relat�rio de NFSe
TE - Relat�rio de Telegrama (Formul�rio do Correio)
CP - Relat�rio de conta paciente.
*/			

nr_seq_hist_w	number(10);
nr_seq_cobr_w	number(10);
ds_historico_w	varchar2(4000);
ds_tipo_w	varchar2(255);

Cursor C01 is
select	nr_seq_cobranca
from	cobranca_paciente_titulo
where	nr_seq_lote	= nr_lote_p
union all
select	to_number(substr(obter_cobranca_titulo(nr_titulo,'S'),1,255))
from	cobranca_paciente_conta
where	nr_seq_lote	= nr_lote_p;

Cursor C02 is
select	to_number(substr(obter_cobranca_titulo(nr_titulo,'S'),1,255))
from	cobranca_paciente_conta
where	nr_interno_conta	= nr_interno_conta_p
and	nr_seq_lote		= nr_lote_p;

begin

select	nr_seq_historico
into	nr_seq_hist_w
from	cobranca_paciente_lote
where	nr_sequencia	= nr_lote_p;

if	(nvl(ie_somente_visualiza_p,'S') = 'S') then
	if	(nvl(ie_tipo_p,'') = 'AR') then
		/* 'Hist�rico gerado pela vizualiza��o do relat�rio de AR do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819425, 'NR_LOTE_P=' || nr_lote_p);
	elsif	(nvl(ie_tipo_p,'') = 'NF') then
		/* 'Hist�rico gerado pela vizualiza��o do relat�rio de NFS-e do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819426, 'NR_LOTE_P=' || nr_lote_p);
	elsif	(nvl(ie_tipo_p,'') = 'TE') then
		/* 'Hist�rico gerado pela vizualiza��o do relat�rio do telegrama do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819427, 'NR_LOTE_P=' || nr_lote_p);
	elsif	(nvl(ie_tipo_p,'') = 'CP') then
		/* 'Hist�rico gerado pela vizualiza��o do relat�rio da conta paciente do lote de cobran�a : #@NR_LOTE_P#@.'  */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819428, 'NR_LOTE_P=' || nr_lote_p);
	end if;		
else
	if	(nvl(ie_tipo_p,'') = 'AR') then
		/* 'Hist�rico gerado pela impress�o do relat�rio de AR do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819429, 'NR_LOTE_P=' || nr_lote_p);
	elsif	(nvl(ie_tipo_p,'') = 'NF') then
		/* 'Hist�rico gerado pela impress�o do relat�rio de NFS-e do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819430, 'NR_LOTE_P=' || nr_lote_p);
	elsif	(nvl(ie_tipo_p,'') = 'TE') then
		/* 'Hist�rico gerado pela impress�o do relat�rio do telegrama do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819431, 'NR_LOTE_P=' || nr_lote_p);
	elsif	(nvl(ie_tipo_p,'') = 'CP') then
		/* 'Hist�rico gerado pela impress�o do relat�rio da conta paciente do lote de cobran�a : #@NR_LOTE_P#@.' */
		ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(819432, 'NR_LOTE_P=' || nr_lote_p);
	end if;
end if;

if	(ie_tipo_p <> 'CP') then
	open C01;
	loop
	fetch C01 into	
		nr_seq_cobr_w;
	exit when C01%notfound;
		begin
		if	(nr_seq_cobr_w is not null) then
			gerar_cobranca_historico (nr_seq_hist_w,
						null,
						nr_seq_cobr_w,
						ds_historico_w,
						sysdate,
						nm_usuario_p);
		end if;
		end;
	end loop;
	close C01;
else
	open C02;
	loop
	fetch C02 into	
		nr_seq_cobr_w;
	exit when C02%notfound;
		begin
		if	(nr_seq_cobr_w is not null) then
			gerar_cobranca_historico (nr_seq_hist_w,
						null,
						nr_seq_cobr_w,
						ds_historico_w,
						sysdate,
						nm_usuario_p);
		end if;
		end;
	end loop;
	close C02;
end if;

commit;

end Gravar_hist_cominic_cobranc;
/