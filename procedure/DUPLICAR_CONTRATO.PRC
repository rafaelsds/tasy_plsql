create or replace
procedure duplicar_contrato(
		nr_seq_contrato_p		Number,
		nm_usuario_p			Varchar2,
		cd_cgc_cpf_p			Varchar2,
		cd_estab_p			number,
		nr_sequencia_p	out	number,
		ie_copiar_dt_fim_p		Varchar2 default 'S') is


	--contrato

se_w				Number(1);
cd_cgc_contratado_w		varchar(255);
cd_cpf_contratado_w		varchar(255);
NR_SEq_contrato_w		Number(10);
NR_SEQ_TIPO_CONTRATO_w		Number(10);
DS_OBJETO_CONTRATO_w		Varchar2(1000);
IE_RENOVACAO_w			VARCHAR2(1);
DT_INICIO_w			date;
DT_FIM_w			date;
QT_DIAS_RESCISAO_w		Number(5);
IE_PRAZO_CONTRATO_w		Varchar2(1);
NM_CONTATO_w			Varchar2(40);
VL_TOTAL_CONTRATO_w		Number(15,2);
DS_ATRIBUICAO_w			Varchar2(4000);
IE_PAGAR_RECEBER_w		Varchar2(1);
CD_PESSOA_NEGOC_w		Number(10);
DT_REVISAO_w			date;
CD_ESTABELECIMENTO_w		Number(4);
IE_AVISA_VENCIMENTO_w		Varchar2(1);
QT_DIAS_AVISO_VENC_w		Number(5);
CD_CARGO_w			Number(10);
DS_MOTIVO_RESCISAO_W		Varchar2(255);
NR_SEQ_FORMA_RESCISAO_w		Number(10);
VL_MULTA_CONTRATUAL_w		Number(15,2);
CD_SETOR_w			Number(5);
CD_CONDICAO_PAGAMENTO_w		Number(10);
IE_AVISA_VENC_SETOR_w		Varchar2(1);
PR_MULTA_CONTRATUAL_w		Number(15,2);
NR_SEQ_SUBTIPO_CONTRATO_w	Number(10);
IE_CLASSIFICACAO_w		Varchar2(2);
NR_SEQ_ADITIVO_w		Number(10);
DT_ATUALIZACAO_ADITIVO_w	date;
NR_SEQ_CONTRATO_ATUAL_w		Number(10);
QT_DIAS_INTERV_AVISO_w		Number(10);
NR_SEQ_AREA_w			Number(10);
NR_SEQ_INDICE_REAJUSTE_w	Number(10);
IE_AVISA_REAJUSTE_w		Varchar2(1);
CD_OPERACAO_NF_w		Number(10);
CD_NATUREZA_OPERACAO_w		Number(10);
IE_EMAIL_ORIGEM_w		Varchar2(1);
QT_DIAS_REAJUSTE_w		Number(10);
CD_PESSOA_CONTRATANTE_w contrato.cd_pessoa_contratante%type;
CD_CNPJ_CONTRATANTE_w	contrato.cd_cnpj_contratante%type;															

	--regra contrato

NR_SEQ_regra_con_w			Number(10);
IE_FORMA_w			Varchar2(1);
DT_PRIMEIRO_VENCTO_w		date;
IE_TIPO_VALOR_w			Varchar2(3);
VL_PAGTO_r_w			Number(15,2);
CD_MOEDA_w			Number(3);
CD_INDICE_REAJUSTE_w		Number(3);
IE_PERIODO_REAJUSTE_w		Varchar2(3);
CD_CONTA_FINANC_w		Number(10);
VL_IR_w				Number(15,2);
VL_INSS_w			Number(15,2);
VL_ISS_w			Number(15,2);
DT_INICIO_VIGENCIA_r_w		date;
DT_FINAL_VIGENCIA_r_w		date;
DS_OBSERVACAO_r_w		Varchar2(255);
QT_INDICE_REAJUSTE_w		Number(9,4);
DS_REF_INDICE_REAJUSTE_w	Varchar2(80);
DS_REGRA_VENCIMENTO_w		Varchar2(4000);
NR_SEQ_TRANS_FINANC_w		Number(10);
NR_SEQ_TRANS_FIN_BAIXA_w	Number(10);
IE_CENTRO_CONTRATO_w		Varchar2(1);
TX_DESC_ANTECIPACAO_w		Number(7,4);
CD_CONTA_CONTABIL_r_w		VARCHAR2(20);
QT_AVISO_REAJUSTE_w		Number(5);
NR_SEQ_CRIT_RATEIO_r_w		Number(10);

	--etapa

nr_seq_etapa_s_w		Number(10);
DS_ETAPA_w			varchar(80);
DT_PREVISTA_w			date;
DT_REAL_w			date;
DS_OBSERVACAO_e_w		Varchar2(255);
NR_SEQ_TIPO_ETAPA_w		Number(10);


	--centro de custo
nr_seq_etapa_c_w		Number(10);
CD_CENTRO_CUSTO_c_w		Number(8);
PR_ALOCACAO_w			Number(6,3);
DS_OBSERVACAO_c_w		Varchar2(255);
QT_ALOCACAO_w			Number(9,3);
VL_ALOCACAO_w			Number(15,2);
DT_INICIO_VIGENCIA_CC_W	DATE;
DT_FIM_VIGENCIA_CC_W		DATE;

	--historico

nr_seq_etapa_h_w		Number(10);
DT_HISTORICO_w			date;
DS_HISTORICO_w			Varchar2(4000);
DS_TITULO_h_w			Varchar2(255);
IE_EFETIVADO_w			Varchar2(1);

	--renovacao

nr_seq_etapa_r_w		Number(10);
DT_RENOVACAO_w			date;
DS_OBSERVACAO_re_w		Varchar2(4000);
CD_PESSOA_FISICA_w 		VARCHAR2(10);
DS_TITULO_re_W			Varchar2(80);

	--texto

nr_seq_etapa_t_w		Number(10);
NR_SEQUENCIA_t_w	        Number(10);
DS_TITULO_t_w			Varchar2(150);

	-- regra doc

nr_seq_etapa_d_w		Number(10);
DS_REGRA_w			Varchar2(80);
IE_REGRA_DIA_w 			Varchar2(1);
QT_DIA_MES_w			Number(3);
DS_OBSERVACAO_d_w		Varchar2(255);
DT_ESPECIFICA_w 		Date;
IE_AVISA_VENCIMENTO_r_w		Varchar2(1);


	--regra nf itens

nr_seq_etapa_i_w		Number(10);
CD_MATERIAL_w			Number(6);
CD_CONTA_CONTABIL_w		varchar(20);
CD_CENTRO_CUSTO_r_w		Number(8);
NR_SEQ_CRIT_RATEIO_w		Number(10);
DS_COMPLEMENTO_w		Varchar2(255);
DS_OBSERVACAO_i_w		Varchar2(255);
NR_SEQ_CONTA_FINANC_w		Number(10);
VL_PAGTO_w			Number(17,4);
DT_INICIO_VIGENCIA_w		date;
DT_FIM_VIGENCIA_w		date;
PR_VARIACAO_VALOR_w		Number(5,2);
QT_CONVERSAO_NF_w		Number(5,2);
IE_CONVERSAO_Nf_w		Varchar2(1);
NR_SEQ_EQUIPAMENTO_w		Number(10);
CD_ESTAB_REGRA			Number(4);
CD_LOCAL_ESTOQUE_w		Number(4);
DT_VENC_PAGTO_w			Number(3);
QT_DIAS_ANTES_GERAR_SC_w	Number(5,2);
IE_GERA_SC_AUTOMATICO_w		Varchar2(1);
IE_TIPO_REGRA_w			Varchar2(15);
CD_PESSOA_SOLICITANTE_w		Varchar2(10);
IE_LIBERA_SOLIC_w		Varchar2(1);
NR_SEQ_MARCA_w			Number(10);
VL_SALDO_TOTAL_W		Number(15,2);
ie_periodo_nf_w			varchar2(15);
qt_maximo_nf_periodo_w		number(5);
qt_dias_revisao_w		number(10);

	--clausula

nr_seq_etapa_l_w		Number(10);
NR_SEQ_TIPO_w 			Number(10);
NR_SEQUENCIA_l_w		Number(10);
DS_TITULO_l_w			Varchar2(100);
ds_clausula_w			contrato_clausula.ds_clausula%type;
vl_desconto_w			contrato_regra_nf.vl_desconto%type;
pr_desconto_w			contrato_regra_nf.pr_desconto%type;

--regra contrato
CURSOR C01 IS
select  IE_FORMA,
	DT_PRIMEIRO_VENCTO,
	IE_TIPO_VALOR,
	VL_PAGTO,
	CD_MOEDA,
	CD_INDICE_REAJUSTE,
	IE_PERIODO_REAJUSTE,
	CD_CONTA_FINANC,
	VL_IR,
	VL_INSS,
	VL_ISS,
	DT_INICIO_VIGENCIA,
	DT_FINAL_VIGENCIA,
	DS_OBSERVACAO,
	QT_INDICE_REAJUSTE,
	DS_REF_INDICE_REAJUSTE,
	DS_REGRA_VENCIMENTO,
	NR_SEQ_TRANS_FINANC,
	NR_SEQ_TRANS_FIN_BAIXA,
	IE_CENTRO_CONTRATO,
	TX_DESC_ANTECIPACAO,
	CD_CONTA_CONTABIL,
	QT_AVISO_REAJUSTE,
	NR_SEQ_CRIT_RATEIO
from	CONTRATO_REGRA_PAGTO
where	nr_seq_contrato = nr_seq_contrato_p;


--etapa
CURSOR C02 IS
select	DS_ETAPA,
	DT_PREVISTA,
	DT_REAL,
	DS_OBSERVACAO,
	NR_SEQ_TIPO_ETAPA
from	CONTRATO_ETAPA
where	nr_seq_contrato = nr_seq_contrato_p;

--centro custo
CURSOR C03 IS
select	CD_CENTRO_CUSTO,
	PR_ALOCACAO,
	DS_OBSERVACAO,
	QT_ALOCACAO,
	VL_ALOCACAO,
	DT_INICIO_VIGENCIA,
	DT_FIM_VIGENCIA
from	CONTRATO_CENTRO_CUSTO
where	nr_seq_contrato = nr_seq_contrato_p;

--historico
CURSOR C04 IS
select	DT_HISTORICO,
	DS_HISTORICO,
	DS_TITULO,
	IE_EFETIVADO
from	CONTRATO_HISTORICO
where	nr_seq_contrato = nr_seq_contrato_p;

--renovacao
CURSOR C05 IS
select	DT_RENOVACAO,
	DS_OBSERVACAO,
	CD_PESSOA_FISICA,
	DS_TITULO
from	CONTRATO_RENOVACAO
where	nr_seq_contrato = nr_seq_contrato_p;

--texto

CURSOR C06 IS
select	
        NR_SEQUENCIA,
        DS_TITULO
from	CONTRATO_TEXTO
where	nr_seq_contrato = nr_seq_contrato_p;

--regra doc
CURSOR C07 IS
select	DS_REGRA,
	IE_REGRA_DIA,
	QT_DIA_MES,
	DS_OBSERVACAO,
	DT_ESPECIFICA,
	IE_AVISA_VENCIMENTO
from	CONTRATO_REGRA_DOC
where	nr_seq_contrato = nr_seq_contrato_p;

--regra nf itens
CURSOR C08 IS
select	CD_MATERIAL,
	CD_CONTA_CONTABIL,
	CD_CENTRO_CUSTO,
	NR_SEQ_CRIT_RATEIO,
	DS_COMPLEMENTO,
	DS_OBSERVACAO,
	NR_SEQ_CONTA_FINANC,
	VL_PAGTO,
	DT_INICIO_VIGENCIA,
	DT_FIM_VIGENCIA,
	PR_VARIACAO_VALOR,
	QT_CONVERSAO_NF,
	IE_CONVERSAO_NF,
	NR_SEQ_EQUIPAMENTO,
	CD_ESTAB_REGRA,
	CD_LOCAL_ESTOQUE,
	DT_VENC_PAGTO,
	QT_DIAS_ANTES_GERAR_SC,
	IE_GERA_SC_AUTOMATICO,
	IE_TIPO_REGRA,
	CD_PESSOA_SOLICITANTE,
	IE_LIBERA_SOLIC,
	NR_SEQ_MARCA,
	VL_DESCONTO,
	PR_DESCONTO
from	CONTRATO_REGRA_NF
where	nr_seq_contrato = nr_seq_contrato_p;

--clausula
CURSOR C09 IS
select	NR_SEQUENCIA,
	NR_SEQ_TIPO,
	DS_TITULO,
	ds_clausula
from	CONTRATO_CLAUSULA
where	nr_seq_contrato = nr_seq_contrato_p;

BEGIN


select	contrato_seq.nextval
into	NR_SEq_contrato_w
from	dual;

SELECT DECODE(LENGTH(cd_cgc_cpf_p), 14, 1, 2)
into	se_w
FROM dual;


if 	se_w = 1 then
	cd_cgc_contratado_w := cd_cgc_cpf_p;
	cd_cpf_contratado_w := null;
end if;

if 	se_w = 2 then
	cd_cpf_contratado_w := cd_cgc_cpf_p;
	cd_cgc_contratado_w := null;
end if;

select 	NR_SEQ_TIPO_CONTRATO,
	DS_OBJETO_CONTRATO,
	IE_RENOVACAO,
	DT_INICIO,
	DT_FIM,
	QT_DIAS_RESCISAO,
	IE_PRAZO_CONTRATO,
	VL_TOTAL_CONTRATO,
	DS_ATRIBUICAO,
	IE_PAGAR_RECEBER,
	CD_PESSOA_NEGOC,
	DT_REVISAO,
	CD_ESTABELECIMENTO,
	IE_AVISA_VENCIMENTO,
	QT_DIAS_AVISO_VENC,
	CD_CARGO,
	DS_MOTIVO_RESCISAO,
	NR_SEQ_FORMA_RESCISAO,
	VL_MULTA_CONTRATUAL,
	CD_SETOR,
	CD_CONDICAO_PAGAMENTO,
	IE_AVISA_VENC_SETOR,
	PR_MULTA_CONTRATUAL,
	NR_SEQ_SUBTIPO_CONTRATO,
	IE_CLASSIFICACAO,
	NR_SEQ_ADITIVO,
	DT_ATUALIZACAO_ADITIVO,
	NR_SEQ_CONTRATO_ATUAL,
	QT_DIAS_INTERV_AVISO,
	NR_SEQ_AREA,
	NR_SEQ_INDICE_REAJUSTE,
	IE_AVISA_REAJUSTE,
	CD_OPERACAO_NF,
	CD_NATUREZA_OPERACAO,
	IE_EMAIL_ORIGEM,
	QT_DIAS_REAJUSTE,
	nvl(ie_periodo_nf,'N'),
	qt_maximo_nf_periodo,		
	qt_dias_revisao,
	cd_pessoa_contratante,
	cd_cnpj_contratante
into	NR_SEQ_TIPO_CONTRATO_w,
	DS_OBJETO_CONTRATO_w,
	IE_RENOVACAO_w,
	DT_INICIO_w,
	DT_FIM_w,
	QT_DIAS_RESCISAO_w,
	IE_PRAZO_CONTRATO_w,
	VL_TOTAL_CONTRATO_w,
	DS_ATRIBUICAO_w,
	IE_PAGAR_RECEBER_w,
	CD_PESSOA_NEGOC_w,
	DT_REVISAO_w,
	CD_ESTABELECIMENTO_w,
	IE_AVISA_VENCIMENTO_w,
	QT_DIAS_AVISO_VENC_w,
	CD_CARGO_w,
	DS_MOTIVO_RESCISAO_W,
	NR_SEQ_FORMA_RESCISAO_w,
	VL_MULTA_CONTRATUAL_w,
	CD_SETOR_w,
	CD_CONDICAO_PAGAMENTO_w,
	IE_AVISA_VENC_SETOR_w,
	PR_MULTA_CONTRATUAL_w,
	NR_SEQ_SUBTIPO_CONTRATO_w,
	IE_CLASSIFICACAO_w,
	NR_SEQ_ADITIVO_w,
	DT_ATUALIZACAO_ADITIVO_w,
	NR_SEQ_CONTRATO_ATUAL_w,
	QT_DIAS_INTERV_AVISO_w,
	NR_SEQ_AREA_w,
	NR_SEQ_INDICE_REAJUSTE_w,
	IE_AVISA_REAJUSTE_w,
	CD_OPERACAO_NF_w,
	CD_NATUREZA_OPERACAO_w,
	IE_EMAIL_ORIGEM_w,
	QT_DIAS_REAJUSTE_w,
	ie_periodo_nf_w,
	qt_maximo_nf_periodo_w,
	qt_dias_revisao_w,
	CD_PESSOA_CONTRATANTE_w,
	CD_CNPJ_CONTRATANTE_w	
from 	contrato
where	nr_sequencia = nr_seq_contrato_p;


insert into	contrato( NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		NR_SEQ_TIPO_CONTRATO,
		DS_OBJETO_CONTRATO,
		IE_RENOVACAO,
		DT_INICIO,
		DT_FIM,
		QT_DIAS_RESCISAO,
		IE_PRAZO_CONTRATO,
		VL_TOTAL_CONTRATO,
		DS_ATRIBUICAO,
		IE_PAGAR_RECEBER,
		CD_PESSOA_NEGOC,
		DT_REVISAO,
		CD_ESTABELECIMENTO,
		IE_AVISA_VENCIMENTO,
		QT_DIAS_AVISO_VENC,
		CD_CARGO,
		DS_MOTIVO_RESCISAO,
		NR_SEQ_FORMA_RESCISAO,
		VL_MULTA_CONTRATUAL,
		CD_SETOR,
		CD_CONDICAO_PAGAMENTO,
		IE_AVISA_VENC_SETOR,
		PR_MULTA_CONTRATUAL,
		NR_SEQ_SUBTIPO_CONTRATO,
		IE_CLASSIFICACAO,
		NR_SEQ_ADITIVO,
		DT_ATUALIZACAO_ADITIVO,
		NR_SEQ_CONTRATO_ATUAL,
		QT_DIAS_INTERV_AVISO,
		NR_SEQ_AREA,
		NR_SEQ_INDICE_REAJUSTE,
		IE_AVISA_REAJUSTE,
		CD_OPERACAO_NF,
		CD_NATUREZA_OPERACAO,
		IE_EMAIL_ORIGEM,
		QT_DIAS_REAJUSTE,
		CD_CGC_CONTRATADO,
		CD_PESSOA_CONTRATADA,
		ie_situacao,
		ie_periodo_nf,
		qt_maximo_nf_periodo,
		qt_dias_revisao,
		cd_pessoa_contratante,
		cd_cnpj_contratante)
values		(nr_seq_contrato_w,
		sysdate,
		nm_usuario_p,
		NR_SEQ_TIPO_CONTRATO_w,
		DS_OBJETO_CONTRATO_w,
		IE_RENOVACAO_w,
		DT_INICIO_w,
		DT_FIM_w,
		QT_DIAS_RESCISAO_w,
		IE_PRAZO_CONTRATO_w,
		VL_TOTAL_CONTRATO_w,
		DS_ATRIBUICAO_w,
		IE_PAGAR_RECEBER_w,
		CD_PESSOA_NEGOC_w,
		DT_REVISAO_w,
		CD_ESTABELECIMENTO_w,
		IE_AVISA_VENCIMENTO_w,
		QT_DIAS_AVISO_VENC_w,
		CD_CARGO_w,
		DS_MOTIVO_RESCISAO_W,
		NR_SEQ_FORMA_RESCISAO_w,
		VL_MULTA_CONTRATUAL_w,
		CD_SETOR_w,
		CD_CONDICAO_PAGAMENTO_w,
		IE_AVISA_VENC_SETOR_w,
		PR_MULTA_CONTRATUAL_w,
		NR_SEQ_SUBTIPO_CONTRATO_w,
		IE_CLASSIFICACAO_w,
		NR_SEQ_ADITIVO_w,
		DT_ATUALIZACAO_ADITIVO_w,
		NR_SEQ_CONTRATO_ATUAL_w,
		QT_DIAS_INTERV_AVISO_w,
		NR_SEQ_AREA_w,
		NR_SEQ_INDICE_REAJUSTE_w,
		IE_AVISA_REAJUSTE_w,
		CD_OPERACAO_NF_w,
		CD_NATUREZA_OPERACAO_w,
		IE_EMAIL_ORIGEM_w,
		QT_DIAS_REAJUSTE_w,
		cd_cgc_contratado_w,
		cd_cpf_contratado_w,
		'A',
		ie_periodo_nf_w,
		qt_maximo_nf_periodo_w,
		qt_dias_revisao_w,
		CD_PESSOA_CONTRATANTE_w,
		CD_CNPJ_CONTRATANTE_w);

OPEN C01;
LOOP
FETCH C01 into
	IE_FORMA_w,
	DT_PRIMEIRO_VENCTO_w,
	IE_TIPO_VALOR_w,
	VL_PAGTO_r_w,
	CD_MOEDA_w,
	CD_INDICE_REAJUSTE_w,
	IE_PERIODO_REAJUSTE_w,
	CD_CONTA_FINANC_w,
	VL_IR_w,
	VL_INSS_w,
	VL_ISS_w,
	DT_INICIO_VIGENCIA_r_w,
	DT_FINAL_VIGENCIA_r_w,
	DS_OBSERVACAO_r_w,
	QT_INDICE_REAJUSTE_w,
	DS_REF_INDICE_REAJUSTE_w,
	DS_REGRA_VENCIMENTO_w,
	NR_SEQ_TRANS_FINANC_w,
	NR_SEQ_TRANS_FIN_BAIXA_w,
	IE_CENTRO_CONTRATO_w,
	TX_DESC_ANTECIPACAO_w,
	CD_CONTA_CONTABIL_r_w,
	QT_AVISO_REAJUSTE_w,
	NR_SEQ_CRIT_RATEIO_r_w;
EXIT WHEN C01%NOTFOUND;
	begin
	select 	CONTRATO_REGRA_PAGTO_seq.nextval
	into	NR_SEq_regra_con_w
	from	dual;
	
	if (nvl(ie_copiar_dt_fim_p,'S') = 'N') then
		dt_final_vigencia_r_w := null;
	end if;
	
	insert into CONTRATO_REGRA_PAGTO(nr_sequencia,
		DT_ATUALIZACAO,
		NM_USUARIO,
		nr_seq_contrato,
		IE_FORMA,
		DT_PRIMEIRO_VENCTO,
		IE_TIPO_VALOR,
		VL_PAGTO,
		CD_MOEDA,
		CD_INDICE_REAJUSTE,
		IE_PERIODO_REAJUSTE,
		CD_CONTA_FINANC,
		VL_IR,
		VL_INSS,
		VL_ISS,
		DT_INICIO_VIGENCIA,
		DT_FINAL_VIGENCIA,
		DS_OBSERVACAO,
		QT_INDICE_REAJUSTE,
		DS_REF_INDICE_REAJUSTE,
		DS_REGRA_VENCIMENTO,
		NR_SEQ_TRANS_FINANC,
		NR_SEQ_TRANS_FIN_BAIXA,
		IE_CENTRO_CONTRATO,
		TX_DESC_ANTECIPACAO,
		CD_CONTA_CONTABIL,
		QT_AVISO_REAJUSTE,
		NR_SEQ_CRIT_RATEIO
		)
	values(
		NR_SEq_regra_con_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		IE_FORMA_w,
		DT_PRIMEIRO_VENCTO_w,
		IE_TIPO_VALOR_w,
		VL_PAGTO_r_w,
		CD_MOEDA_w,
		CD_INDICE_REAJUSTE_w,
		IE_PERIODO_REAJUSTE_w,
		CD_CONTA_FINANC_w,
		VL_IR_w,
		VL_INSS_w,
		VL_ISS_w,
		DT_INICIO_VIGENCIA_r_w,
		DT_FINAL_VIGENCIA_r_w,
		DS_OBSERVACAO_r_w,
		QT_INDICE_REAJUSTE_w,
		DS_REF_INDICE_REAJUSTE_w,
		DS_REGRA_VENCIMENTO_w,
		NR_SEQ_TRANS_FINANC_w,
		NR_SEQ_TRANS_FIN_BAIXA_w,
		IE_CENTRO_CONTRATO_w,
		TX_DESC_ANTECIPACAO_w,
		CD_CONTA_CONTABIL_r_w,
		QT_AVISO_REAJUSTE_w,
		NR_SEQ_CRIT_RATEIO_r_w);
	end;
END LOOP;
close c01;


OPEN C02;
LOOP
FETCH C02 into
	DS_ETAPA_w,
	DT_PREVISTA_w,
	DT_REAL_w,
	DS_OBSERVACAO_e_w,
	NR_SEQ_TIPO_ETAPA_w;
EXIT WHEN C02%NOTFOUND;
	begin
	select	CONTRATO_ETAPA_seq.nextval
	into	nr_seq_etapa_s_w
	from	dual;
	insert into CONTRATO_ETAPA(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		DS_ETAPA,
		DT_PREVISTA,
		DT_REAL,
		DS_OBSERVACAO,
		NR_SEQ_TIPO_ETAPA)
	values(
		nr_seq_etapa_s_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		DS_ETAPA_w,
		DT_PREVISTA_w,
		DT_REAL_w,
		DS_OBSERVACAO_e_w,
		NR_SEQ_TIPO_ETAPA_w);
	end;
END LOOP;
close c02;


OPEN C03;
LOOP
FETCH C03 into
	CD_CENTRO_CUSTO_c_w,
	PR_ALOCACAO_w,
	DS_OBSERVACAO_c_w,
	QT_ALOCACAO_w,
	VL_ALOCACAO_w,
	DT_INICIO_VIGENCIA_CC_W,
	DT_FIM_VIGENCIA_CC_W;
EXIT WHEN C03%NOTFOUND;
	begin
	select	CONTRATO_CENTRO_CUSTO_seq.nextval
	into	nr_seq_etapa_c_w
	from	dual;
	insert into CONTRATO_CENTRO_CUSTO(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		CD_CENTRO_CUSTO,
		PR_ALOCACAO,
		DS_OBSERVACAO,
		QT_ALOCACAO,
		VL_ALOCACAO,
		DT_INICIO_VIGENCIA,
		DT_FIM_VIGENCIA)
	values(
		nr_seq_etapa_c_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		CD_CENTRO_CUSTO_c_w,
		PR_ALOCACAO_w,
		DS_OBSERVACAO_c_w,
		QT_ALOCACAO_w,
		VL_ALOCACAO_w,
		DT_INICIO_VIGENCIA_CC_W,
		DT_FIM_VIGENCIA_CC_W);
	end;
END LOOP;
close c03;

OPEN C04;
LOOP
FETCH C04 into
	DT_HISTORICO_w,
	DS_HISTORICO_w,
	DS_TITULO_h_w,
	IE_EFETIVADO_w;
EXIT WHEN C04%NOTFOUND;
	begin
	select	CONTRATO_HISTORICO_seq.nextval
	into	nr_seq_etapa_h_w
	from	dual;
	insert into CONTRATO_HISTORICO(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		DT_HISTORICO,
		DS_HISTORICO,
		DS_TITULO,
		IE_EFETIVADO
		)
	values(
		nr_seq_etapa_h_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		DT_HISTORICO_w,
		DS_HISTORICO_w,
		DS_TITULO_h_w,
		IE_EFETIVADO_w);
	end;
END LOOP;
close c04;

OPEN C05;
LOOP
FETCH C05 into
	DT_RENOVACAO_w,
	DS_OBSERVACAO_re_w,
	CD_PESSOA_FISICA_w,
	DS_TITULO_re_w;
EXIT WHEN C05%NOTFOUND;
	begin
	select	CONTRATO_renovacao_seq.nextval
	into	nr_seq_etapa_r_w
	from	dual;
	insert into CONTRATO_renovacao(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		DT_RENOVACAO,
		DS_OBSERVACAO,
		CD_PESSOA_FISICA,
		DS_TITULO
		)
	values(
		nr_seq_etapa_r_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		DT_RENOVACAO_w,
		DS_OBSERVACAO_re_w,
		CD_PESSOA_FISICA_w,
		DS_TITULO_re_w);
	end;
END LOOP;
close c05;

OPEN C06;
LOOP
FETCH C06 into
        NR_SEQUENCIA_t_w,
	DS_TITULO_t_w;
EXIT WHEN C06%NOTFOUND;
	begin
	select	CONTRATO_TEXTO_seq.nextval
	into	nr_seq_etapa_t_w
	from	dual;
	insert into CONTRATO_TEXTO(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		DS_TITULO,
		DS_CONTRATO
		)
	values(
		nr_seq_etapa_t_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		DS_TITULO_t_w,
		' ');
	end;

END LOOP;
close c06;

OPEN C07;
LOOP
FETCH C07 into
	DS_REGRA_w,
	IE_REGRA_DIA_w,
	QT_DIA_MES_w,
	DS_OBSERVACAO_d_w,
	DT_ESPECIFICA_w,
	IE_AVISA_VENCIMENTO_r_w;
EXIT WHEN C07%NOTFOUND;
	begin
	select	CONTRATO_REGRA_DOC_seq.nextval
	into	nr_seq_etapa_d_w
	from	dual;
	insert into CONTRATO_REGRA_DOC(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		DS_REGRA,
		IE_REGRA_DIA,
		QT_DIA_MES,
		DS_OBSERVACAO,
		DT_ESPECIFICA,
		IE_AVISA_VENCIMENTO
		)
	values(
		nr_seq_etapa_d_w,
		sysdate,
		nm_usuario_p,
		NR_seq_contrato_w,
		DS_REGRA_w,
		IE_REGRA_DIA_w,
		QT_DIA_MES_w,
		DS_OBSERVACAO_d_w,
		DT_ESPECIFICA_w,
		IE_AVISA_VENCIMENTO_r_w);
	end;
END LOOP;
close c07;


OPEN C08;
LOOP
FETCH C08 into
	CD_MATERIAL_w,
	CD_CONTA_CONTABIL_w,
	CD_CENTRO_CUSTO_r_w,
	NR_SEQ_CRIT_RATEIO_w,
	DS_COMPLEMENTO_w,
	DS_OBSERVACAO_i_w,
	NR_SEQ_CONTA_FINANC_w,
	VL_PAGTO_w,
	DT_INICIO_VIGENCIA_w,
	DT_FIM_VIGENCIA_w,
	PR_VARIACAO_VALOR_w,
	QT_CONVERSAO_NF_w,
	IE_CONVERSAO_Nf_w,
	NR_SEQ_EQUIPAMENTO_w,
	CD_ESTAB_REGRA,
	CD_LOCAL_ESTOQUE_w,
	DT_VENC_PAGTO_w,
	QT_DIAS_ANTES_GERAR_SC_w,
	IE_GERA_SC_AUTOMATICO_w,
	IE_TIPO_REGRA_w,
	CD_PESSOA_SOLICITANTE_w,
	IE_LIBERA_SOLIC_w,
	NR_SEQ_MARCA_w,
	VL_DESCONTO_W,
	PR_DESCONTO_W;
EXIT WHEN C08%NOTFOUND;
	begin
	select	CONTRATO_REGRA_NF_seq.nextval
	into	nr_seq_etapa_i_w
	from	dual;
	insert into CONTRATO_REGRA_NF(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		NR_SEq_contrato,
		CD_MATERIAL,
		CD_CONTA_CONTABIL,
		CD_CENTRO_CUSTO,
		NR_SEQ_CRIT_RATEIO,
		DS_COMPLEMENTO,
		DS_OBSERVACAO,
		NR_SEQ_CONTA_FINANC,
		VL_PAGTO,
		DT_INICIO_VIGENCIA,
		DT_FIM_VIGENCIA,
		PR_VARIACAO_VALOR,
		QT_CONVERSAO_NF,
		IE_CONVERSAO_NF,
		NR_SEQ_EQUIPAMENTO,
		CD_ESTAB_REGRA,
		CD_LOCAL_ESTOQUE,
		DT_VENC_PAGTO,
		QT_DIAS_ANTES_GERAR_SC,
		IE_GERA_SC_AUTOMATICO,
		IE_TIPO_REGRA,
		CD_PESSOA_SOLICITANTE,
		IE_LIBERA_SOLIC,
		NR_SEQ_MARCA,
		VL_SALDO_TOTAL,
		VL_DESCONTO,
		PR_DESCONTO)
	values(
		nr_seq_etapa_i_w,
		sysdate,
		nm_usuario_p,
		NR_SEq_contrato_w,
		CD_MATERIAL_w,
		CD_CONTA_CONTABIL_w,
		CD_CENTRO_CUSTO_r_w,
		NR_SEQ_CRIT_RATEIO_w,
		DS_COMPLEMENTO_w,
		DS_OBSERVACAO_i_w,
		NR_SEQ_CONTA_FINANC_w,
		VL_PAGTO_w,
		DT_INICIO_VIGENCIA_w,
		DT_FIM_VIGENCIA_w,
		PR_VARIACAO_VALOR_w,
		QT_CONVERSAO_NF_w,
		IE_CONVERSAO_Nf_w,
		NR_SEQ_EQUIPAMENTO_w,
		CD_ESTAB_REGRA,
		CD_LOCAL_ESTOQUE_w,
		DT_VENC_PAGTO_w,
		QT_DIAS_ANTES_GERAR_SC_w,
		IE_GERA_SC_AUTOMATICO_w,
		IE_TIPO_REGRA_w,
		CD_PESSOA_SOLICITANTE_w,
		IE_LIBERA_SOLIC_w,
		NR_SEQ_MARCA_w,
		VL_SALDO_TOTAL_W,
		VL_DESCONTO_W,
		PR_DESCONTO_W);
	end;
END LOOP;
close c08;

OPEN C09;
LOOP
FETCH C09 into
	NR_SEQUENCIA_l_w,
	NR_SEQ_TIPO_w,
	DS_TITULO_L_w,
	ds_clausula_w;
EXIT WHEN C09%NOTFOUND;
	begin
	select	CONTRATO_CLAUSULA_seq.nextval
	into	nr_seq_etapa_l_w
	from	dual;
	insert into CONTRATO_CLAUSULA(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		NR_SEq_contrato,
		NR_SEQ_TIPO,
		DS_TITULO,
		DS_CLAUSULA
		)
	values(
		nr_seq_etapa_l_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		NR_SEq_contrato_w,
		NR_SEQ_TIPO_w,
		DS_TITULO_L_w,
		ds_clausula_w);
	end;
END LOOP;
close c09;

commit;

copia_campo_long_de_para_Java(	'CONTRATO_TEXTO',
				'DS_CONTRATO',
				' where	nr_sequencia = :nr_sequencia',
				'nr_sequencia='||NR_SEQUENCIA_t_w,
				'CONTRATO_TEXTO',
				'DS_CONTRATO', 
				' where	nr_sequencia = :nr_sequencia',
				'nr_sequencia='||nr_seq_etapa_t_w,
				'L');	

commit;

copia_campo_long_de_para_Java(	'CONTRATO_CLAUSULA',
				'DS_CLAUSULA',
				' where	nr_sequencia = :nr_sequencia',
				'nr_sequencia='||NR_SEQUENCIA_l_w,
				'CONTRATO_CLAUSULA',
				'DS_CLAUSULA', 
				' where	nr_sequencia = :nr_sequencia',
				'nr_sequencia='||nr_seq_etapa_l_w,
				'L');	
commit;

nr_sequencia_p	:= NR_SEq_contrato_w;
				
end DUPLICAR_CONTRATO;
/
