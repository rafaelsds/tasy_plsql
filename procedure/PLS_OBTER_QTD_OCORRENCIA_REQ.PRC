create or replace
procedure pls_obter_qtd_ocorrencia_req
			(	nr_seq_requisicao_p		Number,
				qt_tipo_quantidade_p		Number,
				ie_tipo_qtde_p			Varchar2,
				qt_liberada_p			Number,
				ie_rotorno_p		out	Varchar2,
				ds_observacao_p		out	Varchar2) is 

dt_requisicao_w			Date;
nr_seq_segurado_w		Number(10);
nr_seq_prestador_w		Number(10);
dt_liberacao_w			Date;
nr_seq_requisicao_w		Number(10);
dt_requisicao_ww		Date;
nm_medico_solic_w		Varchar(255);

begin

ie_rotorno_p	:= pls_obter_se_qtd_ocorr_req(	nr_seq_requisicao_p, qt_tipo_quantidade_p, ie_tipo_qtde_p,
						qt_liberada_p);

if	(ie_rotorno_p	= 'N') then
	begin					
		select	dt_requisicao,
			nr_seq_segurado,
			nr_seq_prestador
		into	dt_requisicao_w,
			nr_seq_segurado_w,
			nr_seq_prestador_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;

		if	(ie_tipo_qtde_p	= 'D') then   	
			dt_liberacao_w	:= trunc(dt_requisicao_w - (qt_tipo_quantidade_p - 1));		
		elsif	(ie_tipo_qtde_p	= 'M') then
			dt_liberacao_w	:= (add_months(dt_requisicao_w, -qt_tipo_quantidade_p) + 1);
		elsif	(ie_tipo_qtde_p	= 'A') then
			dt_liberacao_w	:= (add_months(dt_requisicao_w, -qt_tipo_quantidade_p * 12) + 1); /* Vezes 12 meses ao ano */
		elsif	(ie_tipo_qtde_p = 'G') then
			dt_liberacao_w := sysdate;
		end if;

		select 	max(nr_sequencia)
		into	nr_seq_requisicao_w
		from	pls_requisicao		
		where	ie_estagio		= 2
		and	nr_seq_segurado		= nr_seq_segurado_w
		and	nr_sequencia		<> nr_seq_requisicao_p
		and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_requisicao) 	between dt_liberacao_w and ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_requisicao_w);

		select	dt_requisicao,
			substr(obter_nome_pessoa_fisica(cd_medico_solicitante, null),1,255)
		into	dt_requisicao_ww,
			nm_medico_solic_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;

		ds_observacao_p	:= substr('Requisicao: '||nr_seq_requisicao_w||'. Data:'||PKG_DATE_FORMATERS.TO_VARCHAR(dt_requisicao_ww,'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.get_nm_usuario)||'. Medico solic: '||nm_medico_solic_w,1,400);
	exception
	when others then
		ds_observacao_p	:= '';
	end;
end if;

--commit;

end pls_obter_qtd_ocorrencia_req;
/