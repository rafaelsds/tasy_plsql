create or replace
procedure sup_gerar_ajuste_cm(
			nr_sequencia_p	in	Number,
			nm_usuario_p	in	Varchar2) as

nr_movimento_estoque_w		number(10);
cd_unidade_medida_w		varchar2(30);
cd_estabelecimento_w		number(6);
cd_local_estoque_w		number(6);
dt_mesano_referencia_w		date;
dt_movimento_estoque_w		date;
dt_processo_w			date;
nm_usuario_w			varchar2(15);
cd_material_w			number(6);
cd_material_estoque_w		number(6);
vl_ajuste_w			number(15,4);
vl_movimento_w			number(15,4);
qt_estoque_w			number(22,4);

cd_operacao_estoque_w		number(6);
cd_operacao_invent_w		number(6);
cd_operacao_estoque_normal_w	number(6);
cd_operacao_invent_normal_w	number(6);
cd_operacao_estoque_consig_w	number(6);
cd_operacao_invent_consig_w	number(6);

dt_mes_w			date;
ie_erro_w			varchar2(255);
vl_bruto_w			number(15,4);
vl_movto_estoque_w		number(15,4);
cd_acao_w			varchar2(1);
ie_entrada_saida_w      		operacao_estoque.ie_entrada_saida%type;
vl_estoque_w            		number(15,2) := 0;
qt_saldo_estoque_w      		number(15,4) := 0;
ie_altera_custo_w       	operacao_estoque.ie_altera_custo%type;
cd_centro_custo_w		sup_ajuste_estoque_consumo.cd_centro_custo%type;
ie_consignado_w			varchar2(1);
qt_existe_w			number(10);
cd_fornecedor_w			varchar2(14);

qt_movto_estoque_w		number(13,4);

ie_estoque_lote_w		varchar2(1);
nr_seq_lote_fornec_w		number(10);
ds_material_w			varchar2(255);
nm_id_job_w			sup_ajuste_estoque_consumo.nm_id_job%type;
ds_titulo_w			sup_notificacao_fila.ds_titulo%type;
ds_conteudo_w			sup_notificacao_fila.ds_conteudo%type;

cursor c01 is
select	a.cd_material,
	a.vl_ajuste,
	b.cd_centro_custo,
	a.cd_fornecedor
from	sup_ajuste_estoque_consumo b,
	sup_ajuste_est_cons_mat a
where	a.nr_seq_ajuste	= b.nr_sequencia
and	b.nr_sequencia	= nr_sequencia_p;

cursor c02 is
select	a.nr_movimento_estoque,
	a.cd_acao,
	b.ie_entrada_saida,
	nvl(a.qt_estoque,0),
	b.ie_altera_custo
from	operacao_estoque b,
	movimento_estoque a
where	a.cd_operacao_estoque	= b.cd_operacao_estoque
and	nvl(b.ie_consignado,0)  = 0
and	b.ie_altera_custo	= 'S'
and	a.dt_processo is not null
and	a.cd_material_estoque	= cd_material_estoque_w
and	a.dt_mesano_referencia	= dt_mesano_referencia_w
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	ie_consignado_w = 'N'
union all
select	a.nr_movimento_estoque,
	a.cd_acao,
	b.ie_entrada_saida,
	nvl(a.qt_estoque,0),
	b.ie_altera_custo
from	operacao_estoque b,
	movimento_estoque a
where	a.cd_operacao_estoque	= b.cd_operacao_estoque
and	nvl(b.ie_consignado,0)  <> 0
and	b.ie_altera_custo	= 'S'
and	a.dt_processo is not null
and	a.cd_fornecedor	 	= cd_fornecedor_w
and	a.cd_material_estoque	= cd_material_estoque_w
and	a.dt_mesano_referencia	= dt_mesano_referencia_w
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	ie_consignado_w = 'S'
order by 1;

cursor c03 is
select	dt_mes
from	mes_v
where	dt_mes >= PKG_DATE_UTILS.start_of(dt_mesano_referencia_w,'mm',0)
order by 1;

begin
update	sup_ajuste_estoque_consumo
set	dt_geracao_inicio	= sysdate
where	nr_sequencia		= nr_sequencia_p;
commit;

select	cd_estabelecimento,
	cd_local_estoque,
	dt_mesano_referencia,
	nm_id_job
into	cd_estabelecimento_w,
	cd_local_estoque_w,
	dt_mesano_referencia_w,
	nm_id_job_w
from	sup_ajuste_estoque_consumo
where	nr_sequencia	= nr_sequencia_p;

-- feito por job, gerar notificacao de inicio de processo
if	(nm_id_job_w is not null) then
	ds_titulo_w := substr(wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 1188729,vl_macros_p => 'NR_SEQ_LOTE='||nr_sequencia_p),1,80);
	ds_conteudo_w := substr(wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 1188733),1,2000);
	
	sup_gerar_notificacao_async (nm_usuario_p => nm_usuario_p, ds_titulo_p => ds_titulo_w, ds_conteudo_p => ds_conteudo_w, 
					nm_usuarios_destino_p => nm_usuario_p, cd_profiles_destino_p => null, cd_grupos_destino_p => null, 
					cd_setores_destino_p => null, ie_commit_p => 'S');
end if;

sup_verifica_cria_operacao(cd_estabelecimento_w, 6, nm_usuario_p, cd_operacao_estoque_normal_w);
sup_verifica_cria_operacao(cd_estabelecimento_w, 7, nm_usuario_p, cd_operacao_estoque_consig_w);

select	nvl(max(cd_oper_inv_ajuste_cm), 0)
into	cd_operacao_invent_normal_w
from	parametro_estoque
where	cd_estabelecimento	= cd_estabelecimento_w;
if	(cd_operacao_invent_normal_w = 0) then
	select	min(cd_operacao_estoque)
	into	cd_operacao_invent_normal_w
	from	operacao_estoque
	where	ie_tipo_requisicao	= '5'
	and	ie_entrada_saida	= 'S'
	and	ie_consignado		= '0'
	and	IE_ALTERA_CUSTO		= 'N'
	and	ie_situacao		= 'A';
end if;

select	nvl(max(cd_oper_inv_ajuste_cm_consig), 0)
into	cd_operacao_invent_consig_w
from	parametro_estoque
where	cd_estabelecimento	= cd_estabelecimento_w;
if	(cd_operacao_invent_consig_w = 0) then
	select	min(cd_operacao_estoque)
	into	cd_operacao_invent_consig_w
	from	operacao_estoque
	where	ie_tipo_requisicao	= '5'
	and	ie_entrada_saida	= 'S'
	and	ie_consignado		= '7'
	and	IE_ALTERA_CUSTO	= 'N'
	and	ie_situacao		= 'A';
end if;

dt_movimento_estoque_w	:= dt_mesano_referencia_w;
dt_processo_w		:= dt_mesano_referencia_w;
nm_usuario_w		:= 'Ajuste_CM';

open c01;
loop
fetch c01 into
	cd_material_w,
	vl_ajuste_w,
	cd_centro_custo_w,
	cd_fornecedor_w;
exit when c01%notfound;
	begin
	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UME'),1,30) cd_unidade_medida_estoque,
		cd_material_estoque,
		ie_consignado,
		ds_material
	into	cd_unidade_medida_w,
		cd_material_estoque_w,
		ie_consignado_w,
		ds_material_w
	from	material
	where	cd_material = cd_material_w;
	
	select	nvl(max(ie_estoque_lote),'N')
	into	ie_estoque_lote_w
	from	material_estab
	where	cd_material = cd_material_estoque_w
	and	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(ie_consignado_w = 1) or ((ie_consignado_w = 2) and (cd_fornecedor_w is not null)) then
		ie_consignado_w := 'S';
	else
		ie_consignado_w := 'N';
	end if;
	
	if	(ie_consignado_w = 'S') then
		begin		
		select	nvl(sum(vl_estoque),0),
			nvl(sum(qt_estoque),0)
		into	vl_estoque_w,
			qt_saldo_estoque_w
		from	fornecedor_mat_consignado
		where	cd_fornecedor		= cd_fornecedor_w
		and	cd_material		= cd_material_estoque_w
		and	dt_mesano_referencia	= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_w,-1,0)
		and	cd_estabelecimento	= cd_estabelecimento_w;
		
		cd_operacao_estoque_w 	:= cd_operacao_estoque_consig_w;
		cd_operacao_invent_w	:= cd_operacao_invent_consig_w;
		
		if	(ie_estoque_lote_w = 'S') then
			begin
			select	max(nr_seq_lote)
			into	nr_seq_lote_fornec_w
			from	fornecedor_mat_consig_lote
			where	cd_estabelecimento = cd_estabelecimento_w
			and	cd_local_estoque = cd_local_Estoque_w
			and	cd_material = cd_material_estoque_w
			and	cd_fornecedor = cd_fornecedor_w
			and	dt_mesano_referencia = dt_mesano_referencia_w;
			
			if	(nr_seq_lote_fornec_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_lote_fornec_w
				from	material_lote_fornec
				where	nvl(ie_situacao,'A') = 'A'
				and	nvl(ie_bloqueio,'N') = 'N'
				and	cd_cgc_fornec = cd_fornecedor_w
				and	cd_material in (select	x.cd_material
							from	material x
							where	x.cd_material_estoque = cd_material_estoque_w)
				and	cd_estabelecimento = cd_estabelecimento_w;
			end if;
			end;
		end if;
		end;
	else
		begin
		select	nvl(sum(vl_estoque),0),
			nvl(sum(qt_estoque),0)
		into	vl_estoque_w,
			qt_saldo_estoque_w
		from	saldo_estoque
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_material		= cd_material_estoque_w
		and	dt_mesano_referencia	= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_w,-1,0);
		
		cd_fornecedor_w		:= null;
		cd_operacao_estoque_w 	:= cd_operacao_estoque_normal_w;
		cd_operacao_invent_w	:= cd_operacao_invent_normal_w;
		
		if	(ie_estoque_lote_w = 'S') then
			begin
			select	max(nr_seq_lote)
			into	nr_seq_lote_fornec_w
			from	saldo_estoque_lote
			where	cd_estabelecimento = cd_estabelecimento_w
			and	cd_local_estoque = cd_local_Estoque_w
			and	cd_material = cd_material_estoque_w
			and	dt_mesano_referencia = dt_mesano_referencia_w;
			
			if	(nr_seq_lote_fornec_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_lote_fornec_w
				from	material_lote_fornec
				where	nvl(ie_situacao,'A') = 'A'
				and	nvl(ie_bloqueio,'N') = 'N'
				and	cd_material in (select	x.cd_material
							from	material x
							where	x.cd_material_estoque = cd_material_estoque_w)
				and	cd_estabelecimento = cd_estabelecimento_w;				
			end if;
			end;
		end if;
		end;
	end if;
	
	if	(ie_estoque_lote_w = 'S') and
		(nvl(nr_seq_lote_fornec_w,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(181229,'DS_MATERIAL='||ds_material_w);
	end if;
	
	if	(substr(sup_obter_metodo_valorizacao(dt_mesano_referencia_w, cd_estabelecimento_w),1,15) = 'MPM') then		
		begin
		dt_processo_w	:=	sysdate; 
		
		val_estoque_media_ponderada.val_prod_mat(dt_mesano_referencia_w, cd_estabelecimento_w, cd_material_estoque_w, nm_usuario_w);
		
		select	nvl(sum(vl_estoque),0),
			nvl(sum(qt_estoque),0)
		into	vl_estoque_w,
			qt_saldo_estoque_w
		from	saldo_estoque
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_material		= cd_material_estoque_w
		and	dt_mesano_referencia	= dt_mesano_referencia_w;
		end;
	else
		begin
		open c02;
		loop
		fetch c02 into
			nr_movimento_estoque_w,
			cd_acao_w,
			ie_entrada_saida_w,
			qt_estoque_w,
			ie_altera_custo_w;
		exit when c02%notfound;
			begin
			select	nvl(sum(decode(b.ie_aumenta_diminui_valor,
					'D', vl_movimento * -1,
					'A', vl_movimento,
					'N', 0)),0)
			into	vl_movimento_w
			from	tipo_valor b,
				movimento_estoque_valor a
			where	nr_movimento_estoque = nr_movimento_estoque_w
			and	a.cd_tipo_valor      = b.cd_tipo_valor;

			if	(ie_entrada_saida_w = 'S') then
				qt_estoque_w	:= qt_estoque_w * -1;
				vl_movimento_w	:= vl_movimento_w * -1;
			end if;

			if	(cd_acao_w <> '1') then
				qt_estoque_w	:= qt_estoque_w * -1;
				vl_movimento_w	:= vl_movimento_w * -1;
			end if;

			qt_saldo_estoque_w	:= qt_saldo_estoque_w + qt_estoque_w;
			vl_estoque_w		:= vl_estoque_w + vl_movimento_w;
			end;
		end loop;
		close c02;
		end;
	end if;	

	qt_movto_estoque_w	:=	1;	
	vl_bruto_w		:=	vl_ajuste_w * (qt_saldo_estoque_w + qt_movto_estoque_w);
	vl_movto_estoque_w	:=	vl_bruto_w - vl_estoque_w;
	
	if	(round(vl_movto_estoque_w,2) <> vl_movto_estoque_w) then
		qt_movto_estoque_w	:=	10;	
		vl_bruto_w		:=	vl_ajuste_w * (qt_saldo_estoque_w + qt_movto_estoque_w);
		vl_movto_estoque_w	:=	vl_bruto_w - vl_estoque_w;
	end if;
	
	if	(round(vl_movto_estoque_w,2) <> vl_movto_estoque_w) then
		qt_movto_estoque_w	:=	100;	
		vl_bruto_w		:=	vl_ajuste_w * (qt_saldo_estoque_w + qt_movto_estoque_w);
		vl_movto_estoque_w	:=	round(vl_bruto_w - vl_estoque_w,2);
	end if;

	/*Ajuste CM*/
	select	movimento_estoque_seq.nextval
	into	nr_movimento_estoque_w
	from	dual;

	insert into movimento_estoque(
		nr_movimento_estoque,		cd_estabelecimento,
		cd_local_estoque,		dt_movimento_estoque,
		cd_operacao_estoque,		cd_acao,
		cd_material,			dt_mesano_referencia,
		qt_movimento,			dt_atualizacao,
		nm_usuario,			ie_origem_documento,
		nr_documento,			nr_sequencia_item_docto,
		cd_unidade_medida_estoque,	qt_estoque,
		cd_unidade_med_mov,		cd_material_estoque,
		dt_processo,			cd_centro_custo,
		cd_fornecedor,			nr_Seq_lote_fornec)
	values( nr_movimento_estoque_w,		cd_estabelecimento_w,
		cd_local_estoque_w,		dt_movimento_estoque_w,
		cd_operacao_estoque_w,		1,
		cd_material_w,			dt_mesano_referencia_w,
		qt_movto_estoque_w,		sysdate,
		nm_usuario_w,			9,
		nr_movimento_estoque_w,		1,
		cd_unidade_medida_w,		qt_movto_estoque_w,
		cd_unidade_medida_w,		cd_material_estoque_w,
		dt_processo_w,			cd_centro_custo_w,
		cd_fornecedor_w,		nr_seq_lote_fornec_w);	

	insert into movimento_estoque_valor(
		nr_movimento_estoque,
		cd_tipo_valor,
		vl_movimento,
		dt_atualizacao,
		nm_usuario)
	values(	nr_movimento_estoque_w,
		1,
		nvl(vl_movto_estoque_w,0),
		sysdate,
		nm_usuario_w);	
		
	/*Diminuicao Inventario*/
	select	movimento_estoque_seq.nextval
	into	nr_movimento_estoque_w
	from	dual;

	insert into movimento_estoque(
		nr_movimento_estoque,			cd_estabelecimento,
		cd_local_estoque,				dt_movimento_estoque,
		cd_operacao_estoque,			cd_acao,
		cd_material,				dt_mesano_referencia,
		qt_movimento,				dt_atualizacao,
		nm_usuario,				ie_origem_documento,
		nr_documento,				nr_sequencia_item_docto,
		cd_unidade_medida_estoque,		qt_estoque,
		cd_unidade_med_mov,			cd_material_estoque,
		dt_processo,				cd_centro_custo,
		cd_fornecedor,				nr_Seq_lote_fornec)
	values( nr_movimento_estoque_w,			cd_estabelecimento_w,
		cd_local_estoque_w,			dt_movimento_estoque_w,
		cd_operacao_invent_w,			1,
		cd_material_w,				dt_mesano_referencia_w,
		qt_movto_estoque_w,			sysdate,
		nm_usuario_w,				9,
		nr_movimento_estoque_w,			1,
		cd_unidade_medida_w,			qt_movto_estoque_w,
		cd_unidade_medida_w,			cd_material_estoque_w,
		dt_processo_w,				cd_centro_custo_w,
		cd_fornecedor_w,			nr_seq_lote_fornec_w);
	
	insert into movimento_estoque_valor(
		nr_movimento_estoque,
		cd_tipo_valor,
		vl_movimento,
		dt_atualizacao,
		nm_usuario)
	values(	nr_movimento_estoque_w,
		1,
		nvl(vl_ajuste_w,0),
		sysdate,
		nm_usuario_w);
				
	if	(ie_consignado_w = 'S') then
		begin
		select	count(*)
		into	qt_existe_w
		from	fornecedor_mat_consignado
		where	cd_fornecedor		= cd_fornecedor_w
		and	cd_material		= cd_material_estoque_w
		and	dt_mesano_referencia	= dt_mesano_referencia_w
		and	cd_estabelecimento	= cd_estabelecimento_w;
		
		if	(qt_existe_w = 0) then
			insert into fornecedor_mat_consignado(
				cd_estabelecimento,
				dt_mesano_referencia,
				cd_local_estoque,
				cd_material,
				cd_fornecedor,
				qt_estoque,
				vl_estoque,
				dt_atualizacao,
				nm_usuario,
				ie_bloqueio_inventario)
			values (cd_estabelecimento_w,
				dt_mesano_referencia_w,
				cd_local_estoque_w,
				cd_material_estoque_w,
				cd_fornecedor_w,
				0,
				0,
				sysdate,
				'Tasy',
				'N');
		end if;
		end;
	else
		begin
		select	count(*)
		into	qt_existe_w
		from	saldo_estoque
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_material		= cd_material_estoque_w
		and	dt_mesano_referencia	= dt_mesano_referencia_w;
		
		if	(qt_existe_w = 0) then
			insert into saldo_estoque(
				cd_estabelecimento,
				dt_mesano_referencia,
				cd_local_estoque,
				cd_material,
				qt_estoque,
				vl_estoque,
				qt_reservada,
				qt_reservada_requisicao,
				dt_atualizacao,
				nm_usuario,
				ie_bloqueio_inventario)
			values (cd_estabelecimento_w,
				dt_mesano_referencia_w,
				cd_local_estoque_w,
				cd_material_estoque_w,
				0,
				0,
				0,
				0,
				sysdate,
				'Tasy',
				'N');
		end if;
		end;
	end if;
	end;

end loop;
close c01;

commit;

open c03;
loop
fetch c03 into
	dt_mes_w;
exit when c03%notfound;
	begin
	dt_mes_w := dt_mes_w;
	verifica_dif_saldo(dt_mes_w,1555);
	verifica_dif_valor(dt_mes_w,1555);
	valorizar_estoque(dt_mes_w, cd_estabelecimento_w, 'N', 'Tasy');
	gerar_resumo_movto_estoque(dt_mes_w, cd_estabelecimento_w, 'Tasy');
	end;
end loop;
close c03;

update	sup_ajuste_estoque_consumo
set	dt_geracao		= sysdate,
	dt_geracao_fim		= sysdate,
	nm_usuario_geracao	= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

-- feito por job, gerar notificacao de fim de processo
if	(nm_id_job_w is not null) then
	ds_titulo_w := substr(wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 1188729,vl_macros_p => 'NR_SEQ_LOTE='||nr_sequencia_p),1,80);
	ds_conteudo_w := substr(wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 1188731),1,2000);

	sup_gerar_notificacao_async (nm_usuario_p => nm_usuario_p, ds_titulo_p => ds_titulo_w, ds_conteudo_p => ds_conteudo_w, 
					nm_usuarios_destino_p => nm_usuario_p, cd_profiles_destino_p => null, cd_grupos_destino_p => null, 
					cd_setores_destino_p => null, ie_commit_p => 'N');
end if;

commit;

end sup_gerar_ajuste_cm;
/