create or replace
procedure  pls_imp_rec_glosa_conta(	cd_guia_prestador_p		in pls_conta.cd_guia_prestador%type,
									cd_ans_p				in pls_rec_glosa_conta_imp.cd_ans%type,
									cd_guia_p				in pls_conta.cd_guia_ok%type,
									nm_operadora_p			in varchar2,
									cd_prestador_executor_p	in pls_rec_glosa_conta_imp.cd_prestador_executor%type,
									nm_prestador_executor_p	in varchar2,
									ie_recurso_glosa_p		in pls_rec_glosa_conta_imp.ie_recurso_glosa%type,
									nr_protocolo_p 			in pls_rec_glosa_conta_imp.nr_protocolo%type,
									dt_realizacao_p			in date,
									dt_fim_internacao_p		in date,
									vl_total_recursado_p	in number,
									dt_recurso_p			in date,
									ds_senha_p				in varchar2,
									cd_guia_recurso_p		in varchar2,
									nr_lote_p				in varchar2,
									nm_usuario_p			in usuario.nm_usuario%type,
									nr_seq_protocolo_cta_p	in pls_rec_glosa_prot_cta_imp.nr_sequencia%type,
									nm_beneficiario_p		in varchar2,
									nr_seq_conta_imp_p		out pls_rec_glosa_conta_imp.nr_sequencia%type,
									nr_seq_conta_p			out pls_conta.nr_sequencia%type,
									ds_mensagem_abort_p		out varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importar todas as guias do arquivo de Recurso de Glosa do padr�o TISS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_conta_w			pls_conta.nr_sequencia%type;
nr_seq_conta_imp_w		pls_rec_glosa_conta_imp.nr_sequencia%type;
nr_seq_prestador_w		pls_rec_glosa_prot_cta_imp.nr_seq_prestador%type;
qt_pagamento_w			pls_integer;
cd_guia_pesquisa_w		pls_rec_glosa_conta_imp.cd_guia%type;
ie_qt_max_conta_w		pls_parametros_rec_glosa.ie_qt_max_conta%type;
qt_rec_protocolo_w		pls_integer;
ie_cooperado_w			varchar2(1) := 'N';
ie_participante_cooperado_w	pls_parametros_rec_glosa.ie_participante_cooperado%type;

begin

select	nvl(max(ie_qt_max_conta),0)
into	ie_qt_max_conta_w
from	pls_parametros_rec_glosa;

select	max(nr_seq_prestador)
into	nr_seq_prestador_w
from	pls_rec_glosa_prot_cta_imp
where	nr_sequencia = nr_seq_protocolo_cta_p;

cd_guia_pesquisa_w := pls_converte_cd_guia_pesquisa(cd_guia_p);

select 	nvl(max(ie_participante_cooperado),'N')
into	ie_participante_cooperado_w
from 	pls_parametros_rec_glosa;

if (ie_participante_cooperado_w = 'S') then

		select 	nvl(max(pls_obter_se_cooperado(cd_pessoa_fisica, cd_cgc)),'N')
		into	ie_cooperado_w
		from 	pls_prestador
		where 	nr_sequencia = nr_seq_prestador_w;

end if;

--Primeiro tenta fazer uma pesquisa pela guia operadora, guia prestador e o prestador, pode ocorrer que a guia operadora pode estar em 2 atendimentos diferentes com guias prestadores diferentes=
if	(cd_guia_pesquisa_w is not null) and
	(cd_guia_prestador_p is not null) then

	if (ie_participante_cooperado_w = 'S' and ie_cooperado_w = 'S') then
		
		select	max(b.nr_sequencia)
		into	nr_seq_conta_w
		from	pls_protocolo_conta a,
				pls_conta b	
		where 	b.nr_seq_protocolo = a.nr_sequencia
		and		a.ie_status in ('3','4','5','6')
		and		( (b.nr_seq_prestador_exec = nr_seq_prestador_w) or ( exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
		and		b.cd_guia_prestador = cd_guia_prestador_p 
		and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
		and		b.cd_senha = ds_senha_p
		and		b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
		and		b.vl_glosa > 0;
		
		if	(nr_seq_conta_w is null) then
			select	max(b.nr_sequencia)
			into	nr_seq_conta_w
			from	pls_protocolo_conta a,
					pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and		a.ie_status in ('3','4','5','6')
			and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
			and		b.cd_guia_prestador = cd_guia_prestador_p 
			and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
			and		b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and		b.vl_glosa > 0;
		end if;
	
	else
	
		select	max(b.nr_sequencia)
		into	nr_seq_conta_w
		from	pls_protocolo_conta a,
			pls_conta b	
		where 	b.nr_seq_protocolo = a.nr_sequencia
		and	a.ie_status in ('3','4','5','6')
		and	b.nr_seq_prestador_exec = nr_seq_prestador_w
		and	b.cd_guia_prestador = cd_guia_prestador_p 
		and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
		and	b.cd_senha = ds_senha_p
		and	b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
		and	b.vl_glosa > 0;
		
		if	(nr_seq_conta_w is null) then
			select	max(b.nr_sequencia)
			into	nr_seq_conta_w
			from	pls_protocolo_conta a,
				pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and	a.ie_status in ('3','4','5','6')
			and	b.nr_seq_prestador_exec = nr_seq_prestador_w
			and	b.cd_guia_prestador = cd_guia_prestador_p 
			and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
			and	b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and	b.vl_glosa > 0;
		end if;
		
	end if;
	
end if;

/*
	Para tratar os casos onde um atendimento � composto por mais guias e em consequ�ncia acaba tendo mais de uma  conta
	com mesmo cd_guia_ok(e em alguns casos ambas tenham valor de glosa o que fazia sempre retornar a maior sequ�ncia), priorizamos o cd_guia_prestador
	
*/
									
if (nr_seq_conta_w is null) then

	if (ie_participante_cooperado_w = 'S' and ie_cooperado_w = 'S') then
	
		select	max(b.nr_sequencia) nr_seq_conta
		into	nr_seq_conta_w
		from	pls_protocolo_conta a,
				pls_conta b	
		where 	b.nr_seq_protocolo = a.nr_sequencia
		and		a.ie_status in ('3','4','5','6')
		and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
		and		b.cd_guia_prestador = cd_guia_prestador_p
		and		b.cd_guia_ok = cd_guia_p
		and		b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
		and		b.vl_glosa > 0;
		
	else
	
		select	max(b.nr_sequencia) nr_seq_conta
		into	nr_seq_conta_w
		from	pls_protocolo_conta a,
				pls_conta b	
		where 	b.nr_seq_protocolo = a.nr_sequencia
		and	a.ie_status in ('3','4','5','6')
		and	b.nr_seq_prestador_exec = nr_seq_prestador_w
		and	b.cd_guia_prestador = cd_guia_prestador_p
		and	b.cd_guia_ok = cd_guia_p
		and	b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
		and	b.vl_glosa > 0;
		
	end if;
		
end if;


/* Verifica se existe uma conta na base com a guia apresentanda se esta conta possui valor glosado */
if	(nr_seq_conta_w is null) then

	if (ie_participante_cooperado_w = 'S' and ie_cooperado_w = 'S') then
	
		select	max(nr_seq_conta)
		into	nr_seq_conta_w
		from 	(
			select	b.nr_sequencia nr_seq_conta
			from	pls_protocolo_conta a,
					pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and		a.ie_status in ('3','4','5','6')
			and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
			and		b.cd_guia_pesquisa = cd_guia_pesquisa_w 
			and		b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and		b.vl_glosa > 0
			union all
			select	b.nr_sequencia nr_seq_conta
			from	pls_protocolo_conta a,
					pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and		a.ie_status in ('3','4','5','6')
			and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
			and		b.cd_guia_prestador = cd_guia_prestador_p and cd_guia_p is null
			and		b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and		b.vl_glosa > 0
			union all
			select	b.nr_sequencia nr_seq_conta
			from	pls_protocolo_conta a,
					pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and		a.ie_status in ('3','4','5','6')
			and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
			and		b.cd_guia_ok = cd_guia_p
			and		b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and		b.cd_guia is null
			and		b.vl_glosa > 0);
			
	else
		select	max(nr_seq_conta)
		into	nr_seq_conta_w
		from 	(
			select	b.nr_sequencia nr_seq_conta
			from	pls_protocolo_conta a,
				pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and	a.ie_status in ('3','4','5','6')
			and	b.nr_seq_prestador_exec = nr_seq_prestador_w
			and	b.cd_guia_pesquisa = cd_guia_pesquisa_w 
			and	b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and	b.vl_glosa > 0
			union all
			select	b.nr_sequencia nr_seq_conta
			from	pls_protocolo_conta a,
				pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and	a.ie_status in ('3','4','5','6')
			and	b.nr_seq_prestador_exec = nr_seq_prestador_w
			and	b.cd_guia_prestador = cd_guia_prestador_p and cd_guia_p is null
			and	b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and	b.vl_glosa > 0
			union all
			select	b.nr_sequencia nr_seq_conta
			from	pls_protocolo_conta a,
				pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and	a.ie_status in ('3','4','5','6')
			and	b.nr_seq_prestador_exec = nr_seq_prestador_w
			and	b.cd_guia_ok = cd_guia_p
			and	b.nr_seq_conta_referencia is null --N�o buscar as contas de abertura
			and	b.cd_guia is null
			and	b.vl_glosa > 0);
	end if;
		
end if;

--Aqui faz o mesmo processo novamente, mas dessa vez procura pelas contas que s�o de abertura
if	(nr_seq_conta_w is null) then
	--Primeiro tenta fazer uma pesquisa pela guia operadora, guia prestador e o prestador, pode ocorrer que a guia operadora pode estar em 2 atendimentos diferentes com guias prestadores diferentes
	if	(cd_guia_pesquisa_w is not null) and
		(cd_guia_prestador_p is not null) then

		if (ie_participante_cooperado_w = 'S' and ie_cooperado_w = 'S') then
		
			select	max(b.nr_sequencia)
			into	nr_seq_conta_w
			from	pls_protocolo_conta a,
					pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and		a.ie_status in ('3','4','5','6')
			and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
			and		b.cd_guia_prestador = cd_guia_prestador_p 
			and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
			and		b.cd_senha = ds_senha_p
			and		b.vl_glosa > 0;
			
			if	(nr_seq_conta_w is null) then
				select	max(b.nr_sequencia)
				into	nr_seq_conta_w
				from	pls_protocolo_conta a,
						pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and		a.ie_status in ('3','4','5','6')
				and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
				and		b.cd_guia_prestador = cd_guia_prestador_p 
				and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
				and		b.vl_glosa > 0;
			end if;
		
		else
		
			select	max(b.nr_sequencia)
			into	nr_seq_conta_w
			from	pls_protocolo_conta a,
				pls_conta b	
			where 	b.nr_seq_protocolo = a.nr_sequencia
			and	a.ie_status in ('3','4','5','6')
			and	b.nr_seq_prestador_exec = nr_seq_prestador_w
			and	b.cd_guia_prestador = cd_guia_prestador_p 
			and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
			and	b.cd_senha = ds_senha_p
			and	b.vl_glosa > 0;
			
			if	(nr_seq_conta_w is null) then
				select	max(b.nr_sequencia)
				into	nr_seq_conta_w
				from	pls_protocolo_conta a,
					pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and	a.ie_status in ('3','4','5','6')
				and	b.nr_seq_prestador_exec = nr_seq_prestador_w
				and	b.cd_guia_prestador = cd_guia_prestador_p 
				and 	b.cd_guia_pesquisa = cd_guia_pesquisa_w
				and	b.vl_glosa > 0;
			end if;
		end if;
			
	end if;

	/* Verifica se existe uma conta na base com a guia apresentanda se esta conta possui valor glosado */
	if	(nr_seq_conta_w is null) then

		if (ie_participante_cooperado_w = 'S' and ie_cooperado_w = 'S') then
			
			select	max(nr_seq_conta)
			into	nr_seq_conta_w
			from 	(
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
						pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and		a.ie_status in ('3','4','5','6')
				and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
				and		b.cd_guia_pesquisa = cd_guia_pesquisa_w 
				and		b.vl_glosa > 0
				union all
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
						pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and		a.ie_status in ('3','4','5','6')
				and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
				and		b.cd_guia_prestador = cd_guia_prestador_p and cd_guia_p is null
				and		b.vl_glosa > 0
				union all
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
						pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and		a.ie_status in ('3','4','5','6')
				and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
				and		b.cd_guia_ok = cd_guia_p
				and		b.cd_guia is null
				and		b.vl_glosa > 0
				union all
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
						pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and		a.ie_status in ('3','4','5','6')
				and		((b.nr_seq_prestador_exec = nr_seq_prestador_w) or (exists(	select 	1 
																				from 	pls_conta_proc c, 
																						pls_proc_participante d 
																				where 	c.nr_sequencia = d.nr_seq_conta_proc 
																				and 	d.nr_seq_prestador = nr_seq_prestador_w)))
				and		b.cd_guia_ref_pesquisa = cd_guia_p
				and		b.cd_guia is null
				and		b.vl_glosa > 0);
		else
		
			select	max(nr_seq_conta)
			into	nr_seq_conta_w
			from 	(
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
					pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and	a.ie_status in ('3','4','5','6')
				and	b.nr_seq_prestador_exec = nr_seq_prestador_w
				and	b.cd_guia_pesquisa = cd_guia_pesquisa_w 
				and	b.vl_glosa > 0
				union all
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
					pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and	a.ie_status in ('3','4','5','6')
				and	b.nr_seq_prestador_exec = nr_seq_prestador_w
				and	b.cd_guia_prestador = cd_guia_prestador_p and cd_guia_p is null
				and	b.vl_glosa > 0
				union all
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
					pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and	a.ie_status in ('3','4','5','6')
				and	b.nr_seq_prestador_exec = nr_seq_prestador_w
				and	b.cd_guia_ok = cd_guia_p
				and	b.cd_guia is null
				and	b.vl_glosa > 0
				union all
				select	b.nr_sequencia nr_seq_conta
				from	pls_protocolo_conta a,
					pls_conta b	
				where 	b.nr_seq_protocolo = a.nr_sequencia
				and	a.ie_status in ('3','4','5','6')
				and	b.nr_seq_prestador_exec = nr_seq_prestador_w
				and	b.cd_guia_ref_pesquisa = cd_guia_p
				and	b.cd_guia is null
				and	b.vl_glosa > 0);
		end if;
		
	end if;
end if;

--aaschlote 01/12/2015 OS 966413 - Tratamento para verificar se a conta est� em um lote de pagamento
if	(nr_seq_conta_w is not null) then

	select	sum(qt)
	into	qt_pagamento_w
	from (
		select	count(1) qt
		from	pls_conta_medica_resumo	a,
			pls_lote_pagamento b
		where	a.nr_seq_conta = nr_seq_conta_w
		and	b.nr_sequencia = a.nr_seq_lote_pgto
		and	a.ie_situacao = 'A'
		union all
		select	count(1) qt
		from	pls_conta_medica_resumo	a,
			pls_pp_lote b
		where	a.nr_seq_conta = nr_seq_conta_w
		and	b.nr_sequencia = a.nr_seq_pp_lote
		and	a.ie_situacao = 'A');

	if	(qt_pagamento_w = 0) then

		nr_seq_conta_w	:= null;
	end if;
end if;

select	count(1)
into	qt_rec_protocolo_w
from	pls_rec_glosa_conta_imp
where	nr_seq_protocolo_cta = nr_seq_protocolo_cta_p;

if	(qt_rec_protocolo_w < ie_qt_max_conta_w) or
	(ie_qt_max_conta_w = 0) then
	insert into pls_rec_glosa_conta_imp(
		nr_sequencia, nr_seq_protocolo_cta , dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_conta, cd_ans, cd_guia,                
		cd_guia_prestador, nm_operadora, ie_recurso_glosa,
		cd_prestador_executor, nm_prestador_executor, nr_lote,
		nr_protocolo, dt_realizacao, dt_fim_internacao,
		ds_senha, cd_guia_recurso, vl_total_recursado,
		dt_recurso, nm_beneficiario 
	) values (
		pls_rec_glosa_conta_imp_seq.nextval,nr_seq_protocolo_cta_p , sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		nr_seq_conta_w, cd_ans_p, cd_guia_p,                
		cd_guia_prestador_p, nm_operadora_p, ie_recurso_glosa_p,
		cd_prestador_executor_p, nm_prestador_executor_p, nr_lote_p,
		nr_protocolo_p, dt_realizacao_p, dt_fim_internacao_p,
		ds_senha_p, cd_guia_recurso_p, vl_total_recursado_p,
		dt_recurso_p, nm_beneficiario_p
	) returning nr_sequencia into nr_seq_conta_imp_w;
else
	ds_mensagem_abort_p := wheb_mensagem_pck.get_texto(877719);
end if;

nr_seq_conta_imp_p := nr_seq_conta_imp_w;
nr_seq_conta_p := nr_seq_conta_w;

end pls_imp_rec_glosa_conta;
/
