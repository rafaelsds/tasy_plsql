create or replace
procedure san_solicita_token_sht(ds_bifrost_event_p varchar2,
							   nr_seq_lote_p	number,
							   nm_usuario_p varchar2) is
						  
	ds_param_integ_res_w 	clob;   
    data_auth_w             clob;
	json_login_w			philips_json;
	json_request_w			philips_json;  
begin	
	json_login_w		:= philips_json();
	json_request_w		:= philips_json();
	
	json_login_w.put('username', '1595af6435015c77a7149e92a551338e');
	json_login_w.put('password', '86nSp64J');
    json_login_w.put('lote', nr_seq_lote_p);
    json_request_w.put('auth', json_login_w);
    
	dbms_lob.createtemporary(lob_loc => data_auth_w, cache => true, dur => dbms_lob.call);
    json_request_w.to_clob(data_auth_w);
            
	begin
		select bifrost.send_integration_content(ds_bifrost_event_p, data_auth_w, nm_usuario_p) 
        into ds_param_integ_res_w
        from dual;     
	exception
		when others then
		rollback;
	end;
    
end san_solicita_token_sht;
/
