create or replace procedure inserir_atend_paciente_log_bio(
    nr_atendimento_p      number,
    cd_pessoa_fisica_p    varchar2,
    ds_biometria_p        varchar2,
    nm_usuario_p          varchar2,
    ie_biometria_valida_p varchar2,
    ie_acao_p             number,
    ds_observacao_p       varchar2 )
is
  ds_acao_p varchar(50);
begin
  case
  when ie_acao_p = 1 then ds_acao_p := obter_desc_expressao(786509);
  when ie_acao_p = 2 then ds_acao_p := obter_desc_expressao(786511);
  when ie_acao_p = 3 then ds_acao_p := obter_desc_expressao(786513);
  when ie_acao_p = 4 then ds_acao_p := obter_desc_expressao(786517);
  else
    ds_acao_p := '';
  end case;
  
  insert
  into atend_paciente_log_bio
    (
      nr_atendimento,
      nr_sequencia,
      cd_pessoa_fisica,
      ds_biometria,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      ie_biometria_valida,
      ds_acao,
      ds_observacao
    )
    values
    (
      nr_atendimento_p,
      atend_paciente_log_bio_seq.nextval,
      cd_pessoa_fisica_p,
      ds_biometria_p,
      sysdate,
      nm_usuario_p,
      ie_biometria_valida_p ,
      ds_acao_p,
      ds_observacao_p
    );
  commit;
end inserir_atend_paciente_log_bio;
/