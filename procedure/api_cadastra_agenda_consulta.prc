create or replace PROCEDURE api_cadastra_agenda_consulta(dt_referencia_p        DATE,
                                                         dt_validade_carteira_p DATE DEFAULT NULL,
                                                         cd_agenda_p            NUMBER DEFAULT NULL,
                                                         nr_sequencia_p         NUMBER DEFAULT NULL,
                                                         cd_convenio_p          NUMBER DEFAULT NULL,
                                                         cd_procedimento_p      NUMBER DEFAULT NULL,
                                                         ie_origem_proced_p     NUMBER DEFAULT NULL,
                                                         nr_seq_proc_interno_p  NUMBER DEFAULT NULL,
                                                         nr_seq_turno_p         NUMBER DEFAULT NULL,
                                                         nr_seq_turno_esp_p     NUMBER DEFAULT NULL,
                                                         nr_seq_indicacao_p     NUMBER DEFAULT NULL,
                                                         nr_seq_sala_p          NUMBER DEFAULT NULL,
                                                         nr_minuto_duracao_p    NUMBER DEFAULT NULL,
                                                         ie_tipo_atendimento_p  NUMBER DEFAULT NULL,
                                                         cd_estabelecimento_p   NUMBER DEFAULT NULL,
                                                         qt_idade_p             NUMBER DEFAULT NULL,
                                                         ie_classif_agenda_p    VARCHAR2 DEFAULT NULL,
                                                         cd_setor_atendimento_p VARCHAR2 DEFAULT NULL,
                                                         cd_cid_p               VARCHAR2 DEFAULT NULL,
                                                         cd_categoria_p         VARCHAR2 DEFAULT NULL,
                                                         cd_plano_p             VARCHAR2 DEFAULT NULL,
                                                         cd_senha_p             VARCHAR2 DEFAULT NULL,
                                                         dt_validade_senha_p    DATE DEFAULT NULL,
                                                         cd_guia_p              VARCHAR2 DEFAULT NULL,
                                                         cd_pessoa_fisica_p     VARCHAR2 DEFAULT NULL,
                                                         cd_usuario_convenio_p  VARCHAR2 DEFAULT NULL,
                                                         nm_usuario_p           VARCHAR2 DEFAULT NULL,
                                                         cd_empresa_ref_p       NUMBER DEFAULT NULL,
                                                         cd_medico_agenda_p     VARCHAR2 DEFAULT NULL,
                                                         cd_especialidade_p     VARCHAR2 DEFAULT NULL,
                                                         ie_hr_disp_p           VARCHAR2 DEFAULT NULL,
                                                         ie_status_agenda_p     VARCHAR2 DEFAULT NULL,
                                                         nm_pessoa_fisica_p     VARCHAR2 DEFAULT NULL,
                                                         cd_tipo_acomodacao_p   NUMBER DEFAULT NULL,
                                                         dt_nascimento_pac_p    DATE DEFAULT NULL,
                                                         cd_medico_req_p        VARCHAR2 DEFAULT NULL,
                                                         ds_email_p		        VARCHAR2 DEFAULT NULL,
                                                         nr_telefone_p		    varchar2 default null,
                                                         cd_sistema_ant_p	    varchar2 default null,
                                                         ds_erro_p              IN OUT NOCOPY VARCHAR2,
                                                         nr_matricula_p         varchar2 default null
                                                            ) IS
    --    ds_erro_p                   VARCHAR2(2000);
    ds_mensagem_setor_w         VARCHAR2(2000);
    ds_mensagem_guia_w          VARCHAR2(2000);
    ie_perm_pf_classif_w        VARCHAR2(2000);
    ie_solic_pessoa_w           VARCHAR2(2000);
    ds_consiste_idade_pac_w     VARCHAR2(2000);
    ds_consiste_agenda_w        VARCHAR2(2000);
    ds_msg_regra_liber_cidade_w VARCHAR2(2000);
    ds_msg_restringe_empresa_w  VARCHAR2(2000);
    ie_plano_w                  VARCHAR2(2000);
    ie_especialidade_w          VARCHAR2(2000);
    ie_mesmo_setor_w            VARCHAR2(1);
    ie_usuario_categoria_r      VARCHAR2(2000);
    ie_regra_bloqueio_r         VARCHAR2(2000);
    ds_observacao_r             VARCHAR2(2000);
    ie_clinica_r                VARCHAR2(2000);
    rn_seq_regra_r              NUMBER;
    ie_se_pf_medico_w           VARCHAR2(1);
    ie_exige_empresa_conv_w     VARCHAR2(1);
    ie_retorno_w                VARCHAR2(4000);
    ie_classif_retorno_w        VARCHAR2(2000);
    nr_atendimento_consulta_w   VARCHAR2(2000);
    nr_seq_regra_msg_w          VARCHAR2(2000);
    ds_msg_regra_w              VARCHAR2(2000);
    ie_bloq_agen_w              VARCHAR2(2000);
    dt_nascimento_w		date;
    cd_pessoa_fisica_w		varchar2(10)	:= cd_pessoa_fisica_p;
    nm_pessoa_fisica_w		agenda_consulta.nm_paciente%type	:= nm_pessoa_fisica_p;
    dt_referencia_w		date	:= dt_referencia_p;
    ---- Variaveis dos parametros 

    cd_perfil_ativo_w perfil.cd_perfil%TYPE;

    ---- Parametros da funcao 
    ie_param_276_w VARCHAR2(10);
    ie_param_279_w VARCHAR2(10);
    ie_param_469_w VARCHAR2(10);
    ie_param_94_w  VARCHAR2(10);
    ie_param_471_w VARCHAR2(10);
    ie_param_502_w VARCHAR2(10);
    ie_param_58_w  VARCHAR2(10);
    ie_param_95_w  VARCHAR2(10);

BEGIN
    ds_erro_p := NULL;
    BEGIN
        SELECT us.cd_perfil_inicial INTO cd_perfil_ativo_w FROM usuario us WHERE us.nm_usuario = nm_usuario_p;
    EXCEPTION
        WHEN OTHERS THEN
            cd_perfil_ativo_w := NULL;
    END;
    
    if	(cd_sistema_ant_p is not null) and
	(cd_pessoa_fisica_w is null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_sistema_ant = cd_sistema_ant_p;
    end if;
    
    if	(dt_referencia_p is null) then
	select	max(dt_agenda)
	into	dt_referencia_w
	from	agenda_consulta
	where	nr_sequencia = nr_sequencia_p;
    
    end if;

    obter_param_usuario(821, 279, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_279_w);
    obter_param_usuario(821, 276, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_276_w);
    obter_param_usuario(821, 469, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_469_w);
    obter_param_usuario(821, 94, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_94_w);
    obter_param_usuario(821, 471, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_471_w);
    obter_param_usuario(821, 502, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_502_w);
    obter_param_usuario(821, 58, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_58_w);
    obter_param_usuario(821, 95, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_param_95_w);
    --agendaConsultaWDBPBeforePost
    -- 42029 L. 1197
    before_post_agenda_consulta(dt_referencia_p           => dt_referencia_w,
                                dt_validade_carteira_p    => dt_validade_carteira_p,
                                cd_agenda_p               => cd_agenda_p,
                                nr_sequencia_p            => nr_sequencia_p,
                                cd_convenio_p             => cd_convenio_p,
                                cd_procedimento_p         => cd_procedimento_p,
                                ie_origem_proced_p        => ie_origem_proced_p,
                                nr_seq_proc_interno_p     => nr_seq_proc_interno_p,
                                nr_seq_turno_p            => nr_seq_turno_p,
                                nr_seq_turno_esp_p        => nr_seq_turno_esp_p,
                                nr_seq_indicacao_p        => nr_seq_indicacao_p,
                                nr_seq_sala_p             => nr_seq_sala_p,
                                nr_minuto_duracao_p       => nr_minuto_duracao_p,
                                ie_tipo_atendimento_p     => ie_tipo_atendimento_p,
                                cd_estabelecimento_p      => cd_estabelecimento_p,
                                qt_idade_p                => qt_idade_p,
                                ie_classif_agenda_p       => ie_classif_agenda_p,
                                cd_cid_p                  => cd_cid_p,
                                cd_categoria_p            => cd_categoria_p,
                                cd_plano_p                => cd_plano_p,
                                cd_senha_p                => cd_senha_p,
                                cd_guia_p                 => cd_guia_p,
                                cd_pessoa_fisica_p        => cd_pessoa_fisica_w,
                                cd_usuario_convenio_p     => cd_usuario_convenio_p,
                                ie_consistir_regra_guia_p => ie_param_276_w, -- getParametrosFuncao().get(276),
                                ie_consiste_idade_pac_p   => ie_param_94_w, --getParametrosFuncao().get(94),                                
                                nm_usuario_p              => nm_usuario_p,
                                
                                cd_empresa_ref_p            => cd_empresa_ref_p,
                                cd_medico_agenda_p          => cd_medico_agenda_p,
                                ds_erro_p                   => ds_erro_p,
                                ds_mensagem_setor_p         => ds_mensagem_setor_w,
                                ds_mensagem_guia_p          => ds_mensagem_guia_w,
                                ie_perm_pf_classif_p        => ie_perm_pf_classif_w,
                                ie_solic_pessoa_p           => ie_solic_pessoa_w,
                                ds_consiste_idade_pac_p     => ds_consiste_idade_pac_w,
                                ds_consiste_agenda_p        => ds_consiste_agenda_w,
                                ds_msg_regra_liber_cidade_p => ds_msg_regra_liber_cidade_w,
                                ds_msg_restringe_empresa_p  => ds_msg_restringe_empresa_w);

    IF (ie_perm_pf_classif_w = 'N') THEN
        ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 24027); -- L. 1200
    END IF;
    IF (ie_solic_pessoa_w = 'N') THEN
        ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 9619); -- L. 1210
    END IF;
    IF ds_msg_restringe_empresa_w IS NOT NULL THEN
        ds_erro_p := ds_msg_restringe_empresa_w;
    END IF;

    IF ds_erro_p IS NULL AND
       cd_medico_req_p IS NOT NULL THEN
        -- 204691 L.1238
        ie_se_pf_medico_w := obter_se_medico(cd_medico_req_p, 'M');
        IF ie_se_pf_medico_w IS NOT NULL AND
           ie_se_pf_medico_w != 'S' THEN
            ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 49229); -- L. 1243       
            GOTO go_final;
        
        END IF;
    END IF;
    IF ds_erro_p IS NULL THEN
        IF ie_param_95_w != 'N' THEN
            -- 35153  L. 1256
            consistir_horario_med_agenda(nr_seq_agenda_p     => nr_sequencia_p,
                                         cd_medico_p         => NULL,
                                         nr_minuto_duracao_p => nr_minuto_duracao_p,
                                         cd_tipo_agenda_p    => 3,
                                         ds_erro_p           => ds_erro_p,
                                         dt_agenda_p         => dt_referencia_w);
            IF ie_param_95_w = 'E' AND
               (ds_erro_p IS NOT NULL OR ie_hr_disp_p = 'N') THEN
                ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 24075); -- L. 1266      
                GOTO go_final;
            END IF;
        END IF;
    
    END IF;

    IF ds_erro_p IS NULL THEN
        -- 17086  L.1314
        cons_regra_agecons_conv_plano(cd_convenio_p           => cd_convenio_p,
                                      cd_categoria_p          => cd_categoria_p,
                                      dt_agenda_p             => dt_referencia_w,
                                      cd_agenda_p             => cd_agenda_p,
                                      cd_setor_atendimento_p  => cd_setor_atendimento_p,
                                      nr_min_duracao_p        => nr_minuto_duracao_p,
                                      cd_plano_convenio_p     => cd_plano_p,
                                      cd_pessoa_fisica_p      => cd_pessoa_fisica_w,
                                      ie_consist_especialid_p => 'N',
                                      ie_plano_p              => ie_plano_w,
                                      ie_especialidade_p      => ie_especialidade_w,
                                      nm_usuario_p            => nm_usuario_p,
                                      cd_estabelecimento_p    => cd_estabelecimento_p,
                                      cd_empresa_ref_p        => cd_empresa_ref_p,
                                      nr_sequencia_p          => nr_sequencia_p,
                                      ds_mens_conv_p          => ds_erro_p);
    
    END IF;
    IF ie_param_279_w = 'S' AND
       cd_usuario_convenio_p IS NOT NULL THEN
        -- 7514 L.1373
        consiste_debito_paciente(cd_pessoa_fisica_p    => cd_pessoa_fisica_w,
                                 cd_convenio_p         => cd_convenio_p,
                                 cd_usuario_convenio_p => cd_usuario_convenio_p,
                                 ie_tipo_atendimento_p => 0,
                                 ie_clinica_p          => 0,
                                 cd_especialidade_p    => cd_especialidade_p,
                                 cd_categoria_p        => cd_categoria_p,
                                 dt_entrada_p          => NULL,
                                 ds_erro_p             => ds_erro_p);
        ds_erro_p := NULL;
        --Forca todos os DLG
    END IF;
    -- 262935 - L. 1400
    ie_exige_empresa_conv_w := obter_se_exige_empresa_ref(cd_convenio_p, cd_estabelecimento_p);
    IF (ie_exige_empresa_conv_w = 'S' AND cd_empresa_ref_p IS NULL) THEN
        ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 262978);
        GOTO go_final;
    END IF;
    IF ds_erro_p IS NULL THEN
    
        BEGIN
            --  49204 L. 1414
            agecons_consiste_cmce(cd_pessoa_fisica_p => cd_pessoa_fisica_w,
                                  cd_convenio_p      => cd_convenio_p,
                                  cd_agenda_p        => cd_agenda_p,
                                  dt_agenda_p        => dt_referencia_w);
        EXCEPTION
            WHEN OTHERS THEN
                ds_erro_p := SQLERRM;
                GOTO go_final;
        END;
        IF (ie_param_469_w != 'N') THEN
            -- executaConsultaPadrao 275077 L. 1421
            ie_mesmo_setor_w := obter_se_mesmo_setor_exclusivo(cd_pessoa_fisica_p => cd_pessoa_fisica_w,
                                                               dt_agenda_p        => dt_referencia_w,
                                                               cd_agenda_p        => cd_agenda_p);
            IF (ie_mesmo_setor_w = 'N') THEN
                IF (ie_param_469_w = 'S') THEN
                    ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 275227); -- L. 1425
                ELSE
                    ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 275235); -- L. 1426
                END IF;
                GOTO go_final;
            END IF;
        END IF;
        IF ds_erro_p IS NULL THEN
            -- executaConsultaPadrao 264940 L.1435
            IF cd_senha_p IS NOT NULL AND
               dt_validade_senha_p IS NOT NULL THEN
                IF dt_validade_senha_p < dt_referencia_w THEN
                    ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 264945);
                    GOTO go_final;
                END IF;
            END IF;
        END IF;
        IF ie_param_471_w = 'S' THEN
            --CONSISTE_USUARIO_CATEGORIA 6948 L. 1453
            consiste_usuario_categoria(cd_estabelecimento_p   => cd_estabelecimento_p,
                                       cd_convenio_p          => cd_convenio_p,
                                       cd_categoria_p         => cd_categoria_p,
                                       ie_tipo_atendimento_p  => ie_tipo_atendimento_p,
                                       cd_usuario_convenio_p  => cd_usuario_convenio_p,
                                       ie_usuario_categoria_p => ie_usuario_categoria_r,
                                       ie_regra_bloqueio_p    => ie_regra_bloqueio_r,
                                       ds_observacao_p        => ds_observacao_r,
                                       ie_clinica_p           => ie_clinica_r,
                                       nr_seq_regra_p         => rn_seq_regra_r);
            IF (ie_usuario_categoria_r != 'S') AND
               ie_regra_bloqueio_r = 'B' THEN
                IF (ie_usuario_categoria_r = 'N') THEN
                    ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 289508); -- L. 1461
                    GOTO go_final;
                ELSIF (ie_usuario_categoria_r = 'C') THEN
                    ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 289509); -- L. 1463
                    GOTO go_final;
                END IF;
            
            END IF;
            -- 1023836 L. 1490
        
            SELECT (decode(MAX(ie_tipo_classif), 'R', 'S', 'N')) ie_retorno
              INTO ie_retorno_w
              FROM agenda_classif
             WHERE cd_classif_tasy = 'R'
               AND ie_agenda IN ('A', 'C')
               AND ie_situacao = 'A'
               AND cd_classificacao = ie_classif_agenda_p;
        
            IF dt_referencia_w > SYSDATE AND
               ie_status_agenda_p <> 'E' AND
               ie_retorno_w = 'N' AND
               ie_param_502_w IN ('S', 'A') THEN
                IF ie_param_58_w = 'S' THEN
                    -- IDENTIFICA_AGENDA_RETORNO 5320 L. 1503
                    identifica_agenda_retorno(dt_agenda_p               => dt_referencia_w,
                                              cd_pessoa_fisica_p        => cd_pessoa_fisica_w,
                                              cd_convenio_p             => cd_convenio_p,
                                              nm_usuario_p              => nm_usuario_p,
                                              cd_agenda_p               => cd_agenda_p,
                                              nr_seq_ageint_item_p      => NULL,
                                              ds_retorno_p              => ie_retorno_w,
                                              ie_classif_retorno_p      => ie_classif_retorno_w,
                                              nr_atendimento_consulta_p => nr_atendimento_consulta_w,
                                              nr_seq_regra_msg_p        => nr_seq_regra_msg_w,
                                              ds_msg_regra_p            => ds_msg_regra_w,
                                              ie_bloq_agen_p            => ie_bloq_agen_w,
                                              cd_medico_p               => NULL);
                    IF ie_bloq_agen_w = 'S' AND
                       ie_retorno_w IS NOT NULL THEN
                        ds_erro_p := ie_retorno_w;
                        GOTO go_final;
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;
    <<go_final>>
    
    
    if	(nm_pessoa_fisica_w is null) and
	(cd_pessoa_fisica_w is not null) then
	nm_pessoa_fisica_w	:= obter_nome_pf(cd_pessoa_fisica_w);
    end if;
    
    IF cd_pessoa_fisica_w IS NULL AND
       nm_pessoa_fisica_w IS NULL THEN
        ds_erro_p := wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 30197);
    END IF;

    IF ds_erro_p IS NULL THEN
    
	dt_nascimento_w	:= dt_nascimento_pac_p;
    
	if	(dt_nascimento_w	is null) and
		(cd_pessoa_fisica_p is not null) then
		select	max(dt_nascimento)
		into	dt_nascimento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	end if;
    
        UPDATE agenda_consulta a
           SET a.dt_agendamento      	= NVL(a.dt_agendamento, SYSDATE),
               a.nm_usuario          	= nm_usuario_p,
               a.nm_usuario_origem   	= NVL(a.nm_usuario_origem, nm_usuario_p),
               a.ie_status_agenda    	= ie_status_agenda_p,
               a.cd_pessoa_fisica    	= cd_pessoa_fisica_w,
               a.nm_paciente         	= nm_pessoa_fisica_w,
               a.qt_idade_pac        	= qt_idade_p,
               a.nr_atendimento      	= nr_atendimento_consulta_w,
               a.dt_validade_carteira	= dt_validade_carteira_p,
               a.dt_nascimento_pac   	= dt_nascimento_w,
               a.cd_senha            	= cd_senha_p,
               a.ie_tipo_atendimento 	= ie_tipo_atendimento_p,
               a.ie_classif_agenda   	= NVL(ie_classif_agenda_p,a.ie_classif_agenda   ),
               a.cd_setor_atendimento	= cd_setor_atendimento_p,
               a.cd_cid              	= cd_cid_p,
               a.cd_categoria        	= cd_categoria_p,
               a.cd_plano            	= cd_plano_p,
               a.dt_validade_senha   	= dt_validade_senha_p,
               a.nr_doc_convenio     	= cd_guia_p,
               a.cd_usuario_convenio 	= cd_usuario_convenio_p,
               a.cd_empresa_ref      	= cd_empresa_ref_p,
               a.cd_especialidade    	= cd_especialidade_p,
               a.cd_tipo_acomodacao  	= cd_tipo_acomodacao_p,
               a.cd_medico_req       	= cd_medico_req_p,
               a.nr_seq_proc_interno 	= nr_seq_proc_interno_p,
               a.cd_convenio	     	= cd_convenio_p,
               a.ds_email	     	    = ds_email_p,
               a.nr_telefone	     	= nr_telefone_p,
               a.nr_matricula_ageweb    = nr_matricula_p
         WHERE a.nr_sequencia 	     	= nr_sequencia_p;
    
    END IF;
END;
/
