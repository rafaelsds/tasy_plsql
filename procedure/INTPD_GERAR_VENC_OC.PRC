create or replace
procedure intpd_gerar_venc_oc(	nm_usuario_p			varchar2,
								nr_seq_ordem_compra_p	number,
								dt_vencimento_p			date,
								vl_vencimento_p			number,
								ds_observacao_p			varchar2) is
									
begin

insert into intpd_ordem_compra_venc(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_ordem_compra,
	dt_vencimento,
	vl_vencimento,
	ds_observacao)
values (intpd_ordem_compra_venc_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_ordem_compra_p,
	dt_vencimento_p,
	vl_vencimento_p,
	ds_observacao_p);

commit;

end intpd_gerar_venc_oc;
/