create or replace
procedure GERAR_MOVTO_ESTOQUE_PRODUCAO(
				nr_lote_producao_p		number,
				nm_usuario_p		varchar2,
				IE_TIPO_P			varchar2) is

/*tipos de geracao
ie_tipo_p = 1   entrada
ie_tipo_p = 2   estorno  */

nr_movimento_estoque_w		number(10);
cd_estabelecimento_w		number(4);
cd_local_estoque_w		number(4);
dt_movimento_estoque_w		date;
cd_operacao_estoque_w		number(3);
cd_acao_w			varchar2(1);
cd_material_w			number(6);
qt_movimento_w			number(13,4);
dt_atualizacao_w			date;
ie_origem_documento_w		varchar2(3) := '7';
nr_documento_w			number(7);
nr_sequencia_item_docto_w		number(2);
nr_sequencia_documento_w		number(2);
cd_unidade_medida_consumo_w	varchar2(30);
qt_conv_estoque_consumo_w	number(13,4);
qt_estoque_w			number(15,4);
cd_oper_producao_w		number(4);
cd_oper_baixa_producao_w		number(4);
dt_mesano_referencia_w		date;
dt_validade_w			date;
nr_sequencia_comp_w		number(5);
nr_lote_producao_w		number(10);
cd_local_estoque_comp_w		number(4);
dt_inicio_w			date;
ie_acao_w			varchar2(1);
cd_material_comp_w		number(7);
qt_real_w				number(13,4);
qt_componente_cons_w		number(13,4);
qt_componente_etq_w		number(13,4);
cd_unidade_medida_w		varchar2(30);
cd_unid_med_cons_w		varchar2(30);
cd_unid_med_etq_w		varchar2(30);
dt_confirmacao_w			date;
ie_baixa_comp_producao_w		varchar2(1);
ie_material_estoque_w		varchar2(1);
material_estoque_comp_w		varchar2(1);
cd_centro_custo_w			number(8);
ie_estoque_lote_w			varchar2(1);
ds_retorno_w			varchar2(255);
nr_seq_lote_fornec_w		number(10);

/* servicos */
nr_seq_servico_w			number(10);
nr_seq_proc_interno_w		number(15,2);
vl_item_w				number(15,2);
qt_item_w			number(15,2);
vl_original_w			number(15,2);

vl_servico_w			number(15,4);
vl_total_servico_w			number(15,4) := 0;
nr_seq_op_opm_w			lote_producao.nr_seq_op_opm%type;
nr_seq_tipo_equip_w		man_equipamento.nr_seq_tipo_equip%type;
nr_seq_planej_w			man_equipamento.nr_seq_planej%type;
nr_seq_trab_w			man_equipamento.nr_seq_trab%type;
nr_seq_local_w			man_equipamento.nr_seq_local%type;
ds_equipamento_w			man_equipamento.ds_equipamento%type;
ie_gerar_equip_prod_opm_w	parametro_estoque.ie_gerar_equip_prod_opm%type;
ie_regra_saldo_consig_w		parametro_estoque.ie_regra_saldo_consig%type;
cd_oper_baixa_prod_consig_w	parametro_estoque.cd_oper_baixa_prod_consig%type;
ie_consignado_comp_w		material.ie_consignado%type;
cd_cgc_fornecedor_w		movimento_estoque.cd_fornecedor%type;
cd_operacao_baixa_w		parametro_estoque.cd_oper_baixa_producao%type;

cursor c02 is
select	nr_sequencia,
	cd_material,
	cd_unidade_medida,
	cd_unid_med_etq,
	cd_local_estoque,
	nvl(qt_real,0) + nvl(qt_perda,0),
	nvl(qt_estoque,0) + nvl(qt_perda_etq,0),
	cd_fornecedor,
	nr_seq_lote_fornec
from	lote_producao_comp
where	nr_lote_producao = nr_lote_producao_p
and	ie_baixa_comp_producao_w = 'S';

cursor c03 is
select	nr_sequencia,
	nr_seq_proc_interno,
	vl_item,
	qt_item,
	vl_original
from	lote_producao_serv
where	nr_lote_producao = nr_lote_producao_p
and	ie_baixa_comp_producao_w = 'S';

begin

ie_acao_w	:= ie_tipo_p;

select	dt_confirmacao,
	cd_estabelecimento,
	nr_seq_op_opm
into	dt_confirmacao_w,
	cd_estabelecimento_w,
	nr_seq_op_opm_w
from	lote_producao
where	nr_lote_producao = nr_lote_producao_p;

select	nvl(max(ie_baixa_comp_producao),'S'),
	nvl(max(ie_regra_saldo_consig), 0)
into	ie_baixa_comp_producao_w,
	ie_regra_saldo_consig_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_w;

if	(dt_confirmacao_w is null) then
	begin
	/* geracao da entrada da producao se for material de estoque */
	select	nr_lote_producao,
		dt_inicio,
		cd_material,
		cd_estabelecimento,
		cd_local_estoque,
		cd_unidade_medida,
		PKG_DATE_UTILS.start_of(dt_mesano_referencia,'month',0),
		nvl(qt_real,qt_prevista)
	into	nr_lote_producao_w,
		dt_inicio_w,
		cd_material_w,
		cd_estabelecimento_w,
		cd_local_estoque_w,
		cd_unidade_medida_w,
		dt_mesano_referencia_w,
		qt_real_w
	from	lote_producao
	where	nr_lote_producao = nr_lote_producao_p
	and	dt_confirmacao_w is null;
	
	ie_estoque_lote_w := obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_w);
	
	if 	(ie_estoque_lote_w = 'S') then -- Juan // Somente gerar lote fornecedor na acao tipo 1
		if (ie_acao_w = '1') then
			gerar_lote_fornec(nr_lote_producao_p, nm_usuario_p, ds_retorno_w);
		end if;
		
		select	max(nr_sequencia)
		into	nr_seq_lote_fornec_w
		from	material_lote_fornec
		where	nr_lote_producao = nr_lote_producao_p;
	end if;

	select	substr(obter_se_material_estoque(cd_estabelecimento_w, 0, cd_material),1,1)
	into	ie_material_estoque_w
	from	material
	where	cd_material = cd_material_w;

	if	(ie_material_estoque_w = 'S') then
		begin
		select	nvl(max(cd_oper_producao),0),
			nvl(max(cd_oper_baixa_producao),0),
			nvl(max(cd_oper_baixa_prod_consig),0)
		into	cd_oper_producao_w,
			cd_oper_baixa_producao_w,
			cd_oper_baixa_prod_consig_w
		from	parametro_estoque
		where	cd_estabelecimento	= cd_estabelecimento_w;

		if	(cd_oper_producao_w = 0) or (cd_oper_baixa_producao_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(194310);
			-- falta informar as operacoes de estoque (producao e baixa producao) nos parametros de estoque
		end if;

		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
			qt_conv_estoque_consumo
		into	cd_unidade_medida_consumo_w,
			qt_conv_estoque_consumo_w
		from	material
		where	cd_material = cd_material_w;

		select	movimento_estoque_seq.nextval
		into	nr_movimento_estoque_w
		from	dual;

		insert into movimento_estoque(
			nr_movimento_estoque,
			cd_estabelecimento,
	 		cd_local_estoque,
 			dt_movimento_estoque,
 			cd_operacao_estoque,
	 		cd_acao,
 			cd_material,
 			dt_mesano_referencia,
	 		qt_movimento,
 			dt_atualizacao,
	 		nm_usuario,
 			ie_origem_documento,
 			nr_documento,
	 		nr_sequencia_item_docto,
 			cd_cgc_emitente,
			cd_serie_nf,
	 		nr_sequencia_documento,
 			vl_movimento,
 			cd_unidade_medida_estoque,
	 		cd_procedimento,
 			cd_setor_atendimento,
 			cd_conta,
	 		dt_contabil,
 			cd_lote_fabricacao,
 			dt_validade,
	 		qt_estoque,
 			dt_processo,
 			cd_local_estoque_destino,
	 		cd_centro_custo,
			cd_unidade_med_mov,
	 		nr_movimento_estoque_corresp,
 			cd_conta_contabil,
 			cd_material_estoque,
	 		ie_origem_proced,
 			cd_fornecedor,
 			nr_lote_contabil,
			ds_observacao,
			nr_seq_lote_fornec,
			nr_lote_producao)
		values(	nr_movimento_estoque_w,
 		 	cd_estabelecimento_w,
	 		cd_local_estoque_w,
			sysdate,
			cd_oper_producao_w,
			ie_acao_w,
			cd_material_w,
			dt_mesano_referencia_w,
	 		(qt_real_w * qt_conv_estoque_consumo_w),
		 	sysdate,
			nm_usuario_p,
			ie_origem_documento_w,
			nr_lote_producao_p,
			null,
			null,
			null,
			null,
			null,
			cd_unidade_medida_w,
			null,
			null,
			null,
			null,
			nr_lote_producao_p,
			dt_validade_w,
			qt_real_w,
			null,
			null,
			null,
			cd_unidade_medida_consumo_w,
			null,
			null,
			null,
			null,
			null,
			null,
			decode(ie_baixa_comp_producao_w, 'N', wheb_mensagem_pck.get_texto(310770), ''), /*'Os componentes nao geram movimentos'*/
			nr_seq_lote_fornec_w,
			nr_lote_producao_p);

		/* geracao das saidas para a producao (servicos)  */
		open c03;
		loop
		fetch c03 into
			nr_seq_servico_w,
			nr_seq_proc_interno_w,
			vl_item_w,
			qt_item_w,
			vl_original_w;
		exit when c03%notfound;
			begin
			vl_total_servico_w := vl_total_servico_w + vl_item_w;
			end;
		end loop;
		close c03;
		if	(vl_total_servico_w > 0) then
			begin

			begin
			insert into movimento_estoque_valor(
				nr_movimento_estoque,
				cd_tipo_valor,
				vl_movimento,
				dt_atualizacao,
				nm_usuario)
			values(nr_movimento_estoque_w,
				16,
				vl_total_servico_w,
				sysdate,
				nm_usuario_p);
			exception when others then
				null;
			end;

			end;
		end if;

		end;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(194311);
		-- o material do lote nao e material de estoque.
	end if;

	/* geracao das saidas para a producao */
	open c02;
	loop
	fetch c02 into
		nr_sequencia_comp_w,
		cd_material_comp_w,
		cd_unid_med_cons_w,
		cd_unid_med_etq_w,
		cd_local_estoque_comp_w,
		qt_componente_cons_w,
		qt_componente_etq_w,
		cd_cgc_fornecedor_w,
		nr_seq_lote_fornec_w;
      	exit when c02%notfound;
      	begin
	select	substr(obter_se_material_estoque(cd_estabelecimento_w, 0, cd_material),1,1),
		substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material),1,1),
		ie_consignado
	into	material_estoque_comp_w,
		ie_estoque_lote_w,
		ie_consignado_comp_w
	from	material
	where	cd_material = cd_material_comp_w;

	if	(material_estoque_comp_w = 'S') then
		begin
		if	(ie_consignado_comp_w = '1') or
			(ie_consignado_comp_w = '2' and
			nvl(cd_cgc_fornecedor_w, 'X') <> 'X') then
			cd_operacao_baixa_w := cd_oper_baixa_prod_consig_w;
			
			if	(cd_operacao_baixa_w = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(1110064);
				 /* 1110064 - Para finalizar a producao e necessario informar a operacao de baixa producao consignado. */
			end if;
		else
			cd_operacao_baixa_w := cd_oper_baixa_producao_w;
		end if;

		if	(ie_consignado_comp_w = '2') and
			(ie_regra_saldo_consig_w > 0) and
			(ie_estoque_lote_w = 'N') and
			(nvl(cd_cgc_fornecedor_w, 'X') = 'X') then
			/* Necessario realizar a conversao ao inves de simplesmente passar a variavel qt_componente_etq_w
			porque a tela dos itens do Lote producao esta se comportando de forma incorreta, 
			permitindo modificar a quantidade de estoque, sem que a quantidade de consumo seja atualizada.
			Assim sendo, para preservar o funcionamento habitual, precisamos converter a quantidade de consumo para quantidade de estoque.
			Caso o comportamento for ajustado, nao sera necessario conversao, passando diretamente a qt_componente_etq_w. */
			qt_componente_etq_w := obter_quantidade_convertida(
							cd_material_p			=> cd_material_comp_w,
							qt_material_p			=> qt_componente_cons_w,
							cd_unidade_medida_p		=> cd_unid_med_cons_w,
							cd_unidade_medida_retorno_p	=> 'UME');

			gerar_movimento_consig_ambos(
				cd_material_p		=> cd_material_comp_w,
				qt_movimento_p		=> qt_componente_etq_w,
				cd_unidade_medida_p	=> cd_unid_med_etq_w,
				ie_acao_p		=> ie_acao_w,
				cd_local_estoque_p	=> cd_local_estoque_comp_w,
				cd_estabelecimento_p	=> cd_estabelecimento_w,
				ie_rotina_movimento_p	=> 'MEP',
				nm_usuario_p		=> nm_usuario_p,
				nr_seq_lote_fornec_p	=> nr_seq_lote_fornec_w,
				nr_sequencia_p		=> nr_lote_producao_p,
				nr_seq_item_p		=> nr_sequencia_comp_w);
		else
			select	nvl(max(cd_centro_custo), 0)
			into	cd_centro_custo_w
			from	local_estoque
			where	cd_local_estoque = cd_local_estoque_w;

			select	movimento_estoque_seq.nextval
			into	nr_movimento_estoque_w
			from	dual;
			
			insert into movimento_estoque(
				nr_movimento_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				cd_operacao_estoque,
				cd_acao,
				cd_material,
				dt_mesano_referencia,
				qt_movimento,
				dt_atualizacao,
				nm_usuario,
				ie_origem_documento,
				nr_documento,
				cd_unidade_med_mov,
				cd_unidade_medida_estoque,
				cd_lote_fabricacao,
				dt_validade,
				qt_estoque,
				dt_processo,
				cd_centro_custo,
				cd_fornecedor,
				nr_seq_lote_fornec,
				nr_lote_producao,
				nr_sequencia_item_docto)
			values(	nr_movimento_estoque_w,
				cd_estabelecimento_w,
				cd_local_estoque_comp_w,
				sysdate,
				cd_operacao_baixa_w,
				ie_acao_w,
				cd_material_comp_w,
				dt_mesano_referencia_w,
				qt_componente_cons_w,
				sysdate,
				nm_usuario_p,
				ie_origem_documento_w,
				nr_lote_producao_p,
				cd_unid_med_cons_w,
				cd_unid_med_etq_w,
				nr_lote_producao_p,
				dt_validade_w,
				qt_componente_etq_w,
				null,
				decode(cd_centro_custo_w, 0, null, cd_centro_custo_w),
				cd_cgc_fornecedor_w,
				nr_seq_lote_fornec_w,
				nr_lote_producao_p,
				nr_sequencia_comp_w);
		end if;

		end;
	end if;
	end;
	end loop;
	close c02;	

	if (ie_acao_w = '1') then -- Juan // Somente atualiza data na acao tipo 1
		update lote_producao
		set dt_confirmacao = sysdate
		where nr_lote_producao = nr_lote_producao_p;
	end if;
	
	select 	ie_gerar_equip_prod_opm
	into	ie_gerar_equip_prod_opm_w
	from 	parametro_estoque
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(nr_seq_op_opm_w > 0 and ie_gerar_equip_prod_opm_w = 'S') and (ie_acao_w = '1') then -- Juan // Somente gera equipamento na acao tipo 1
		begin
		select	max(nr_sequencia)
		into	nr_seq_tipo_equip_w
		from	man_tipo_equipamento
		where	ds_tipo_equip like 'OPM';
		
		if	(nr_seq_tipo_equip_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(457078);
		end if;
		
		select	max(nr_sequencia)
		into	nr_seq_planej_w
		from	man_grupo_planejamento
		where	substr(obter_se_usuario_grupo_planej(nr_sequencia, nm_usuario_p),1,1) = 'S';
		
		if	(nr_seq_planej_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(457079);
		end if;
		
		select	max(nr_sequencia)
		into	nr_seq_trab_w
		from	man_grupo_trabalho
		where	substr(obter_se_usuario_grupo_trab(nr_sequencia, nm_usuario_p),1,1) = 'S';
		
		if	(nr_seq_trab_w is null)	 then
			wheb_mensagem_pck.exibir_mensagem_abort(457080);
		end if;
		
		select	max(a.nr_sequencia)
		into	nr_seq_local_w
		from	man_localizacao a,
			estabelecimento b
		where	a.cd_cnpj = b.cd_cgc
		and	b.cd_estabelecimento = cd_estabelecimento_w
		and	a.ie_situacao = 'A';
		
		if	(nr_seq_local_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(457092);
		end if;
		
		ds_equipamento_w	:=	substr(wheb_mensagem_pck.get_texto(457077) || nr_seq_op_opm_w || ' - ' || obter_dados_material(cd_material_w,'DR'),1,80);
		
		insert into man_equipamento(
			nr_sequencia,
			ds_equipamento,
			nr_seq_local,
			nr_seq_tipo_equip,
			dt_atualizacao,
			nm_usuario,
			nr_seq_planej,
			nr_seq_trab,
			ie_situacao,
			ie_disponibilidade,
			ie_rotina_seguranca,
			ie_propriedade,
			ie_parado,
			ie_controle_setor)
		values(	man_equipamento_seq.nextval,
			ds_equipamento_w,
			nr_seq_local_w,
			nr_seq_tipo_equip_w,
			sysdate,
			nm_usuario_p,
			nr_seq_planej_w,
			nr_seq_trab_w,
			'A',
			'N',
			'N',
			'T',
			'N',
			'S');		
		end;
	end if;

	commit;
  
   if	(nvl(nr_seq_op_opm_w,0) > 0) then
      if (ie_tipo_p = '1') then
         gravar_status_op_opm(nr_seq_op_opm_w,'ACLP',null,nm_usuario_p,cd_estabelecimento_w);
      else 
         gravar_status_op_opm(nr_seq_op_opm_w,'AELP',null,nm_usuario_p,cd_estabelecimento_w);
      end if;
   end if;
	end;
end if;

end gerar_movto_estoque_producao;
/