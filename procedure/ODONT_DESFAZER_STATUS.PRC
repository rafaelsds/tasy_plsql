create or replace
procedure odont_desfazer_status(nr_seq_odont_proced_p	number,
				ie_status_p		varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_odont_historico_w	number(10);
ie_status_w			varchar2(3);
nr_atendimento_w		number(10);
ds_historico_w			varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
cd_procedimento_w		number(15);
nr_seq_proc_interno_w		number(10);
ie_origem_proced_w		number(10);
qt_procedimento_w		number(8,3);
NR_SEQ_AREA_w		varchar2(3);
NR_SEQ_CONSULTA_w	number(10);
NR_SEQ_LOCAL_w		varchar2(3);
NR_SEQ_DENTE_w		varchar2(3);	
NR_SEQ_SEXTANTE_w	varchar2(3);
NR_SEQ_ARCADA_w		varchar2(3);
NR_SEQ_TRATAMENTO_w	varchar2(3);
NR_SEQ_FACE_w		varchar2(3);
ie_raiz_w			varchar2(1);
ds_observacao_w		varchar2(4000);

begin
if	(nr_seq_odont_proced_p is not null) then
	select	max(nr_sequencia)
	into	nr_seq_odont_historico_w
	from	odont_historico
	where	nr_seq_odont_proced = nr_seq_odont_proced_p
	and	dt_historico = (select	max(dt_historico)
				from	odont_historico
				where	nr_seq_odont_proced = nr_seq_odont_proced_p
				and	dt_historico <> (select	max(dt_historico)
							from	odont_historico
							where	nr_seq_odont_proced = nr_seq_odont_proced_p));
				
	if	(nr_seq_odont_historico_w is not null) then

		select	max(ie_status),
			max(nr_atendimento),
			max(cd_pessoa_fisica)
		into	ie_status_w,
				nr_atendimento_w,
				cd_pessoa_fisica_w
		from	odont_historico
		where	nr_sequencia = nr_seq_odont_historico_w;
		
		select	max(NR_SEQ_AREA),
				max(NR_SEQ_CONSULTA),
				max(NR_SEQ_LOCAL),
				max(NR_SEQ_DENTE),
				max(NR_SEQ_SEXTANTE),
				max(NR_SEQ_ARCADA),
				max(NR_SEQ_TRATAMENTO),
				max(NR_SEQ_FACE),
				max(IE_RAIZ)
		into	NR_SEQ_AREA_w,
				NR_SEQ_CONSULTA_w,
				NR_SEQ_LOCAL_w,
				NR_SEQ_DENTE_w,
				NR_SEQ_SEXTANTE_w,
				NR_SEQ_ARCADA_w,
				NR_SEQ_TRATAMENTO_w,
				NR_SEQ_FACE_w,
				IE_RAIZ_w
		from	odont_procedimento
		where	nr_sequencia = nr_seq_odont_proced_p;

		update	odont_procedimento
		set		ie_status = ie_status_w
		where	nr_sequencia = nr_seq_odont_proced_p;
		
		select	max(wheb_mensagem_pck.get_texto(798633)||': '|| substr(obter_valor_dominio(5100,ie_status_w),1,255)||chr(10)||
			wheb_mensagem_pck.get_texto(798634)||': '|| substr(obter_valor_dominio(5100,ie_status_p),1,255)||chr(10)||
			wheb_mensagem_pck.get_texto(798635)||': '|| nr_dente||chr(10)||
			wheb_mensagem_pck.get_texto(308034)||': '|| nr_atendimento)
		into	ds_historico_w	
		from	odont_procedimento		
		where	nr_sequencia = nr_seq_odont_proced_p
		and	nr_dente is not null;
		
		if	(ds_historico_w is null) then
			select	max(wheb_mensagem_pck.get_texto(798633)||': '|| substr(obter_valor_dominio(5100,ie_status_w),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(798634)||': '|| substr(obter_valor_dominio(5100,ie_status_p),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(798641)||': '|| substr(obter_valor_dominio(5101,ie_tratamento),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(308034)||': '|| nr_atendimento)
			into	ds_historico_w	
			from	odont_procedimento		
			where	nr_sequencia = nr_seq_odont_proced_p
			and	ie_tratamento is not null;
		end if;
		
		select	max(wheb_mensagem_pck.get_texto(798633)||': '|| substr(obter_valor_dominio(5100,ie_status_w),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(798634)||': '|| substr(obter_valor_dominio(5100,ie_status_p),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(798641)||': '|| substr(obter_valor_dominio(5101,ie_tratamento),1,255)||chr(10)||
				wheb_mensagem_pck.get_texto(308034)||': '|| nr_atendimento)
			into	ds_observacao_w	
			from	odont_procedimento		
			where	nr_sequencia = nr_seq_odont_proced_p;
		
		
				
		insert into odont_historico(	
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_titulo,
			ds_historico,
			nr_atendimento,
			ie_tipo,
			dt_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_status,
			nr_seq_odont_proced,
			cd_pessoa_fisica,
			ds_observacao,
			NR_SEQ_AREA,
			NR_SEQ_CONSULTA,
			NR_SEQ_LOCAL,
			NR_SEQ_DENTE,
			NR_SEQ_SEXTANTE,
			NR_SEQ_ARCADA,
			NR_SEQ_TRATAMENTO,
			NR_SEQ_FACE,
			IE_RAIZ)
		values(	odont_historico_seq.nextVal,
			sysdate,
			nm_usuario_p,
			wheb_mensagem_pck.get_texto(798654) || ': '||substr(obter_valor_dominio(5100,ie_status_w),1,255),
			nvl(ds_historico_w, wheb_mensagem_pck.get_texto(798654) || ': '|| substr(obter_valor_dominio(5100,ie_status_w),1,255)),
			nr_atendimento_w,
			'S',
			sysdate,
			sysdate,
			nm_usuario_p,
			ie_status_w,
			nr_seq_odont_proced_p,
			cd_pessoa_fisica_w,
			ds_observacao_w,
			NR_SEQ_AREA_w,
			NR_SEQ_CONSULTA_w,
			NR_SEQ_LOCAL_w,
			NR_SEQ_DENTE_w,
			NR_SEQ_SEXTANTE_w,
			NR_SEQ_ARCADA_w,
			NR_SEQ_TRATAMENTO_w,
			NR_SEQ_FACE_w,
			IE_RAIZ_w);
			
		if (ie_status_p = 'R') then
			begin
			select	max(cd_procedimento),
				max(nr_seq_proc_interno),
				max(ie_origem_proced),
				max(qt_procedimento)
			into	cd_procedimento_w,
				nr_seq_proc_interno_w,
				ie_origem_proced_w,
				qt_procedimento_w
			from	odont_procedimento
			where	nr_sequencia = nr_seq_odont_proced_p;

			odonto_inserir_proced_conta(nr_atendimento_w, cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w, (qt_procedimento_w*-1), nr_seq_odont_proced_p, nm_usuario_p);
			end;
		end if;
		
		
		if (ie_status_p = 'C') and (ie_status_w = 'R') then
			begin
			select	max(cd_procedimento),
				max(nr_seq_proc_interno),
				max(ie_origem_proced),
				max(qt_procedimento)
			into	cd_procedimento_w,
				nr_seq_proc_interno_w,
				ie_origem_proced_w,
				qt_procedimento_w
			from	odont_procedimento
			where	nr_sequencia = nr_seq_odont_proced_p;

			odonto_inserir_proced_conta(nr_atendimento_w, cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w, qt_procedimento_w, nr_seq_odont_proced_p, nm_usuario_p);
			end;
		end if;		

	end if;

end if;

commit;

end odont_desfazer_status;
/