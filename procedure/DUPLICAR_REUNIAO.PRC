create or replace
procedure duplicar_reuniao( nr_seq_reuniao_p	number,
			  nr_seq_classif_p	number,
			  nr_seq_tipo_p		number,
			  nm_usuario_p		Varchar2,
			  dt_reuniao_p		date,
			  nr_seq_nova_reuniao_p	out number) is 
			  
nr_seq_reuniao_w	number(10);
cd_estabelecimento_w	number(10);
ds_assunto_w		varchar2(500);
ds_observacao_w		varchar2(4000);
cd_pessoa_responsavel_w	varchar2(10);
nr_seq_ordem_serv_w	number(10);

begin

select	ata_reuniao_seq.nextval
into	nr_seq_reuniao_w
from	dual;


select	cd_estabelecimento,
	ds_assunto,
	ds_observacao,
	cd_pessoa_responsavel,
	nr_seq_ordem_serv
into	cd_estabelecimento_w,
	ds_assunto_w,
	ds_observacao_w,
	cd_pessoa_responsavel_w,
	nr_seq_ordem_serv_w
from	ata_reuniao
where	nr_sequencia = nr_seq_reuniao_p;


insert into ata_reuniao (nr_sequencia,
			 dt_reuniao,
			 cd_estabelecimento,
			 ds_assunto,
			 dt_atualizacao,
			 nm_usuario,
			 dt_atualizacao_nrec,
			 nm_usuario_nrec,
			 ds_observacao,
			 cd_pessoa_responsavel,
			 nr_seq_classif_reuniao,
			 nr_seq_ordem_serv,
			 nr_seq_tipo_reuniao)
values			(nr_seq_reuniao_w,
			 dt_reuniao_p,
			 cd_estabelecimento_w,
			 ds_assunto_w,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 ds_observacao_w,
			 cd_pessoa_responsavel_w,
			 nr_seq_classif_p,
			 nr_seq_ordem_serv_w,
			 nr_seq_tipo_p);

insert into ata_participante (nr_sequencia,
			      nr_seq_reuniao,
			      ie_faltou,
			      nm_pessoa_participante,
			      ds_Setor,
			      dt_atualizacao,
			      nm_usuario,
			      dt_atualizacao_nrec,
			      nm_usuario_nrec,
			      cd_setor_atendimento,
			      cd_pessoa_participante,
			      dt_ciente_ata)
			      (select 
				ata_participante_seq.nextval,
				nr_seq_reuniao_w,
				'N',
				nm_pessoa_participante,
				ds_Setor,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_setor_atendimento,
				cd_pessoa_participante,
				dt_ciente_ata
			      from ata_participante
			      where nr_seq_reuniao = nr_seq_reuniao_p);

insert into ata_reuniao_pauta	(nr_sequencia,
				 dt_atualizacao,
				 nm_usuario,
				 dt_atualizacao_nrec,
				 nm_usuario_nrec,
				 nr_seq_reuniao,
				 ds_pauta,
				 ds_conteudo,
				 cd_pessoa_resp)
				(select
				  ata_reuniao_pauta_seq.nextval,
				  sysdate,
				  nm_usuario_p,
				  sysdate,
				  nm_usuario_p,
				  nr_seq_reuniao_w,
				  ds_pauta,
				  ds_conteudo,
				  cd_pessoa_resp
				from ata_reuniao_pauta
				where nr_seq_reuniao = nr_seq_reuniao_p);

nr_seq_nova_reuniao_p := nr_seq_reuniao_w;
commit;

end DUPLICAR_REUNIAO;
/