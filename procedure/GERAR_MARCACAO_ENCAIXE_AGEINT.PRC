create or replace
PROCEDURE gerar_marcacao_encaixe_ageint	(cd_estabelecimento_p		NUMBER,
						cd_agenda_p		NUMBER,
						dt_agenda_p		DATE,
						hr_encaixe_p		VARCHAR2,
						qt_duracao_p		NUMBER,
						cd_pessoa_fisica_p	VARCHAR2,
						cd_convenio_p		NUMBER,
						cd_categoria_p		VARCHAR2,
						nr_seq_proc_interno_p	NUMBER,
						nm_usuario_p		VARCHAR2,
						cd_setor_atendimento_p	NUMBER,
						nr_seq_ageint_item_p	NUMBER,
						nr_Seq_ageint_p		NUMBER,
						nr_seq_ageint_lib_p	NUMBER,
						cd_medico_p		VARCHAR2,
						nm_usuario_confirm_encaixe_p varchar2 default null,
						ie_classif_agenda_p	varchar2,
						ds_observacao_p		varchar2) IS
						--nr_seq_encaixe_p out	number) is

dt_encaixe_w		DATE;
ds_consistencia_w	VARCHAR2(255);
cd_turno_w		VARCHAR2(1);
nr_seq_classif_w	NUMBER(10,0);
ie_forma_convenio_w	VARCHAR2(2);
cd_convenio_w		NUMBER(5,0);
cd_categoria_w		VARCHAR2(10);
cd_usuario_convenio_w	VARCHAR2(30);
cd_plano_w		VARCHAR2(10);
dt_validade_w		DATE;
nr_doc_convenio_w	VARCHAR2(20);
cd_tipo_acomodacao_w	NUMBER(4,0);
nr_seq_agenda_w		NUMBER(10,0);
ie_proc_agenda_w	VARCHAR2(1);
cd_tipo_Agenda_w	NUMBER(10);
cd_procedimento_w	NUMBER(15);
ie_origem_proced_w	NUMBER(10);
ie_reservado_w		VARCHAR2(1);
ie_principal_w		varchar2(1);
ie_sobreposicao_w	varchar2(1);
qt_horarios_bloq_w	number(10);
ie_dia_semana_w		number(5);
ie_bloqueio_w		varchar2(1);
ie_consiste_encaixe_w	varchar2(1);
ds_erro_w		varchar2(255);
qt_encaixe_w		number(10,0);
qt_encaixe_existe_w	number(10,0);
qt_encaixe_turno_w	number(10) := 0;
qt_perm_enc_turno_w	number(10) := 0;
nr_seq_turno_val_marc_w	number(10);
hr_inicial_turno_w	date;
hr_final_turno_w	date;
nr_seq_turno_w		number(10,0);
ie_regra_horario_turno_w	varchar2(1);
qt_idade_min_enc_w			number(10);
qt_idade_max_enc_w			number(10);
qt_idade_pac_w				number(10);
dt_nascimento_w				date;
ds_consist_encaixe_turno_w	varchar2(255);
ie_permite_classif_w		varchar2(1) := 'S';
ie_consiste_classif_lib_w		varchar2(1);
ie_conv_cancelado_w		varchar2(1);
cd_medico_agenda_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_especialidade_w		especialidade_proc.cd_especialidade%type;
cd_setor_agenda_w		setor_atendimento.cd_setor_atendimento%type;
cd_setor_exclusivo_w		setor_atendimento.cd_setor_atendimento%type;
cd_especialidade_item_w		especialidade_medica.cd_especialidade%type;
nr_seq_regra_bloq_w		number(10);
CD_SETOR_ATENDIMENTO_W		setor_atendimento.cd_setor_atendimento%type;
ie_regra_turno_exames_w varchar2(1);
qt_dias_fut_w		number(10,0);

BEGIN

select 	SUBSTR(Obter_se_Convenio_Cancelado(cd_convenio_p, dt_agenda_p),1,2)				
into 	ie_conv_cancelado_w
from 	dual;

if (ie_conv_cancelado_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(857353);
end if;

if	(hr_encaixe_p = '  :  ') then
	wheb_mensagem_pck.exibir_mensagem_abort (184601);
end if;

dt_encaixe_w := TO_DATE(TO_CHAR(dt_agenda_p,'dd/mm/yyyy') || ' ' || hr_encaixe_p || ':00','dd/mm/yyyy hh24:mi:ss');

Obter_Param_Usuario(869, 162, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_sobreposicao_w);
Obter_Param_Usuario(869, 219, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_encaixe_w);
Obter_Param_Usuario(869, 403, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_classif_lib_w);
Obter_Param_Usuario(821, 219, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_regra_horario_turno_w);
Obter_Param_Usuario(820, 440, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_regra_turno_exames_w);

select	max(dt_nascimento)
into	dt_nascimento_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

select  max(cd_tipo_agenda),
		nvl(max(qt_idade_min_encaixe),0),
		nvl(max(qt_idade_max_encaixe),0),
		max(cd_pessoa_fisica),
		max(cd_especialidade),
		max(cd_setor_agenda),
		max(cd_setor_exclusivo),
    max(nr_dias_fut_agendamento)
into    cd_tipo_agenda_w,
		qt_idade_min_enc_w,
		qt_idade_max_enc_w,
		cd_medico_agenda_w,
		cd_especialidade_w,
		cd_setor_agenda_w,
		cd_setor_exclusivo_w,
    qt_dias_fut_w
from    agenda
where   cd_agenda = cd_agenda_p;

select 	obter_cod_dia_semana(dt_agenda_p),
		obter_idade(dt_nascimento_w, trunc(dt_encaixe_w), 'A')
into	ie_dia_semana_w,
		qt_idade_pac_w
from 	dual;

if  (qt_dias_fut_w > 0) and (dt_encaixe_w > trunc(sysdate + qt_dias_fut_w)) then
  wheb_mensagem_pck.exibir_mensagem_abort(1134591);
end if;

if	(ie_sobreposicao_w = 'S') then
	if      (cd_tipo_agenda_w = 3) then
		consistir_horario_agecons(cd_agenda_p,dt_encaixe_w,qt_duracao_p,'E',ds_consistencia_w);
		if      (ds_consistencia_w is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
		end if;
	elsif   (cd_tipo_agenda_w = 2) then
		consistir_horario_agenda_exame(cd_agenda_p,dt_encaixe_w,qt_duracao_p,'E',ds_consistencia_w);
		if      (ds_consistencia_w is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
		end if;
	end if;
end if;
 
/*Validar caso exista algum bloqueio para determinado exame na pasta Cadastros > Exames > Regras exames > Bloqueios*/
if	(ie_consiste_encaixe_w	= 'S') then
	ageint_consistir_bloq_agenda(cd_Agenda_p, dt_encaixe_w, ie_dia_semana_w, nr_seq_proc_interno_p, ie_bloqueio_w, cd_pessoa_fisica_p);
	if	(ie_bloqueio_w	= 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(184602);
	end if;
end if;

--Exames
if	(cd_tipo_agenda_w = 2) then
	select	nvl(max(qt_encaixe),0)
	into	qt_encaixe_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
	
	if	(qt_encaixe_w > 0) then
		select	count(*)
		into	qt_encaixe_existe_w
		from	agenda_paciente
		where	nvl(ie_encaixe,'N') = 'S'
		and	cd_agenda = cd_agenda_p
		and	ie_status_agenda not in ('C','F','I')
		and	dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 83699/86400;
	
		if	(qt_encaixe_existe_w >= qt_encaixe_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(238107);
		end if;
	end if;
--Consultas
elsif	(cd_tipo_agenda_w = 3) then
	select	nvl(max(qt_encaixe),0)
	into	qt_encaixe_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
	
	if	(qt_encaixe_w > 0) then
		select	count(*)
		into	qt_encaixe_existe_w
		from	agenda_consulta
		where	nvl(ie_encaixe,'N') = 'S'
		and		cd_agenda = cd_agenda_p
		and		ie_status_agenda not in ('C','F','I')
		and		dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 83699/86400;
	
		if	(qt_encaixe_existe_w >= qt_encaixe_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(238107);
		end if;
	end if;
	
	
end if;

/*Regra de quantidade de encaixe por convenio*/
if (cd_tipo_agenda_w = 3) then
	nr_seq_turno_w := obter_turno_encaixe_agecons(cd_agenda_p,dt_encaixe_w);
	
	ageint_consistir_turno_conv(null,cd_agenda_p,dt_agenda_p,nr_seq_turno_w,cd_convenio_p,ie_classif_agenda_p,ds_consist_encaixe_turno_w,nm_usuario_p,cd_estabelecimento_p,'S');
	if (ds_consist_encaixe_turno_w is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(ds_consist_encaixe_turno_w);
	end if;	
end if; 

--Consistir idade do paciente
if	(qt_idade_max_enc_w > 0) and
	(cd_tipo_agenda_w = 3) then	
	if	(qt_idade_pac_w < qt_idade_min_enc_w) then
		--A idade do paciente e menor que a idade definida na configuracao da agenda para a realizacao de encaixes!
		wheb_mensagem_pck.exibir_mensagem_abort(324140);
	elsif	(qt_idade_pac_w > qt_idade_max_enc_w) then
		--A idade do paciente e maior que a idade definida na configuracao da agenda para a realizacao de encaixes!
		wheb_mensagem_pck.exibir_mensagem_abort(324141);
	end if;
	
end if;	

if	(cd_tipo_agenda_w = 3) then	
	if (ie_regra_horario_turno_w = 'T') then
		select	obter_turno_encaixe_d_agecons(cd_agenda_p,dt_encaixe_w,'N')
		into	nr_seq_turno_w
		from	dual;	
	else
		select	obter_turno_encaixe_agecons(cd_agenda_p,dt_encaixe_w)
		into	nr_seq_turno_w
		from	dual;	
	end if;
	
	if	(ie_regra_horario_turno_w <> 'S') and
		(nr_seq_turno_w is null) then 
		wheb_mensagem_pck.Exibir_Mensagem_Abort(288472);
	end if;
	
end if;

--bloqueio parametro 440 agenda de exames
if	(cd_tipo_agenda_w = 2 and ie_regra_turno_exames_w = 'N') then
    if obter_turno_encaixe_ageexame(cd_agenda_p,dt_encaixe_w) is null then
        wheb_mensagem_pck.Exibir_Mensagem_Abort(1069380);
    end if;
end if;

-- Consistencia de classificacoes liberadas
if	(cd_tipo_agenda_w = 3) then
	if (nvl(ie_consiste_classif_lib_w,'N') = 'S') -- Novo parametro na AGI
	and (ie_classif_agenda_p is not null) then
		ie_permite_classif_w	:= obter_agecons_agenda_classif(cd_agenda_p,ie_classif_agenda_p);
		if	(ie_permite_classif_w = 'N') then
			wheb_mensagem_pck.Exibir_Mensagem_Abort(262323);
		end if;
	end if;
end if;	
	
	
/* insert ageint_horarios_usuario */
SELECT	obter_turno_horario_agenda(cd_agenda_p, dt_encaixe_w)
INTO	cd_turno_w
FROM	dual;

/*Consistir a tabela ageint_regra_convenio*/
ageint_consistir_regra_conv (cd_convenio_p, nr_seq_ageint_item_p, dt_encaixe_w, cd_estabelecimento_p, nm_usuario_p, ds_erro_w);

if	(ds_erro_w is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_erro_w);
end if;

ageint_consistir_dias_autor(dt_encaixe_w,nr_seq_ageint_item_p, nm_usuario_p, cd_estabelecimento_p, ds_erro_w);

if	(ds_erro_w is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_erro_w);
end if;

if (cd_tipo_agenda_w = 2) then
	select	max(nr_sequencia)
	into	nr_seq_turno_val_marc_w
	from	agenda_horario
	where	to_char(dt_encaixe_w, 'hh24:mi:ss') between to_char(hr_inicial,'hh24:mi:ss') and to_char(hr_final,'hh24:mi:ss')
	and		nvl(dt_inicio_vigencia, to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) <=
			to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	and		nvl(dt_final_vigencia, to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) >=
			to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	and		nr_minuto_intervalo > 0
	and		((dt_dia_semana = obter_cod_dia_semana(dt_agenda_p)) or ((dt_dia_semana = 9) and (obter_cod_dia_semana(dt_agenda_p) not in (7,1))))
	and		cd_agenda 		= cd_agenda_p
	and		qt_encaixe		is not null;								

	if	(nr_seq_turno_val_marc_w is not null)then
		begin
		
		select	max(hr_inicial),
				max(hr_final),
				max(qt_encaixe)
		into	hr_inicial_turno_w,
				hr_final_turno_w,
				qt_perm_enc_turno_w
		from	agenda_horario
		where	nr_sequencia = nr_seq_turno_val_marc_w;
		
		if	(hr_inicial_turno_w is not null) and
			(hr_final_turno_w is not null)then
			select	count(*) + 1
			into	qt_encaixe_turno_w
			from	agenda_paciente
			where	nvl(ie_encaixe,'N')	= 'S'		
			and		hr_inicio 		between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial_turno_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and		to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final_turno_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and		ie_status_agenda	not in ('C', 'F', 'I', 'II', 'B', 'R')
			and		cd_agenda			= cd_agenda_p;
			--and		nr_seq_horario		= nr_seq_turno_val_marc_w;
		
		end if;
		
		exception
		when others then
			ds_erro_w := substr(sqlerrm,1,255);
		end;
	end if;

	if	(qt_perm_enc_turno_w is not null) and
		(nvl(qt_encaixe_turno_w,0) > nvl(qt_perm_enc_turno_w,0))then
		/*A quantidade limite de encaixes para este turno foi atingida! Verifique a qtd. permitida no cadastro dos horarios da agenda.*/
		wheb_mensagem_pck.exibir_mensagem_abort(260557);
	end if;
	
elsif (cd_tipo_agenda_w = 3) then
	CONSISTE_ENCAIXE_TURNO_AGECONS( dt_agenda_p, dt_encaixe_w, cd_agenda_p );
end if;

if	(nr_seq_proc_interno_p is not null) then
	obter_proc_tab_interno_conv(
		nr_seq_proc_interno_p,
		cd_estabelecimento_p,
		cd_convenio_p,
		cd_categoria_p,
		null,
		null,
		cd_procedimento_w,
		ie_origem_proced_w,
		null,
		dt_encaixe_w,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null);
end if;

select 	max(cd_especialidade)
into	cd_especialidade_item_w
from 	agenda_integrada_item
where 	nr_sequencia = nr_seq_ageint_item_p;

nr_seq_regra_bloq_w := obter_se_bloq_geral_agenda (cd_estabelecimento_p,
					cd_agenda_p,
					ie_classif_agenda_p, -- no caso de exames, ou ie_classif_item_w para consultas/servicos
					null, -- no caso de exames, ou null para consultas
					null, -- nao considerado pois nao esta indo para agenda_consulta
					obter_setor_regras_ageint (cd_tipo_agenda_w,cd_agenda_p,cd_procedimento_w,'S',nm_usuario_p,cd_estabelecimento_p),
					nr_seq_proc_interno_p,
					cd_procedimento_w,
					ie_origem_proced_w,
					cd_medico_p, -- verificar no insert o campo a ser utilizado
					dt_encaixe_w, -- verificar no insert o campo a ser utilizado
					'N',
					'N');

if (nr_seq_regra_bloq_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(obter_mensagem_bloq_geral_age(nr_seq_regra_bloq_w));
end if;

INSERT INTO ageint_encaixe_usuario
	(nr_sequencia,
	cd_agenda,
	--hr_agenda,
	--ie_status_agenda,
	nm_usuario,
	nr_Seq_Ageint,
	nr_minuto_duracao,
	--ie_encaixe,
	--nr_seq_ageint_lib,
	cd_turno,
	nr_Seq_ageint_item,
	dt_encaixe,
	dt_atualizacao,
	cd_pessoa_fisica,
	ie_classif_agenda,
	ds_observacao)
VALUES	(ageint_encaixe_usuario_seq.NEXTVAL,
	cd_agenda_p,
	--dt_encaixe_w,
	--'N',
	nm_usuario_p,
	nr_Seq_Ageint_p,
	qt_duracao_p,
	--'S',
	--nr_seq_ageint_lib_p,
	cd_turno_w,
	nr_Seq_ageint_item_p,
	dt_encaixe_w,
	SYSDATE,
	cd_medico_p,
	ie_classif_agenda_p,
	ds_observacao_p);

Atualiza_Dados_Marcacao(cd_agenda_p, dt_encaixe_w, nr_Seq_ageint_p, 'I', qt_duracao_p, nm_usuario_p, nr_seq_ageint_item_p, nr_seq_ageint_lib_p, 'S', cd_medico_p, ie_reservado_w, nm_usuario_confirm_encaixe_p, ie_principal_w);

COMMIT;

--nr_seq_encaixe_p := nr_seq_agenda_w;

END gerar_marcacao_encaixe_ageint;
/
