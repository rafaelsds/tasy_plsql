create or replace
procedure san_atualizar_doacao_campanha(nr_seq_campanha_p	number,
					nr_seq_doacao_p		number) is 

begin

update	san_doacao_campanha
set	nr_seq_doacao = nr_seq_doacao_p
where	nr_Sequencia = nr_seq_campanha_p;

commit;

end san_atualizar_doacao_campanha;
/