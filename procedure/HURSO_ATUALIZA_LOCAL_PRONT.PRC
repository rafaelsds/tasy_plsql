create or replace
procedure hurso_atualiza_local_pront is 
nr_seq_same_w		number(10);
ie_contador_w		number(10) := 0;
nr_seq_local_ant_w	number(10);
Cursor C01 is
	select	nr_sequencia
	from	same_prontuario
	where	substr(obter_prontuario_pf(nvl(cd_estabelecimento,1),cd_pessoa_fisica),length(obter_prontuario_pf(nvl(cd_estabelecimento,1),cd_pessoa_fisica))-1,length(obter_prontuario_pf(nvl(cd_estabelecimento,1),cd_pessoa_fisica))) = 24;			
begin
open C01;
loop
fetch C01 into	
	nr_seq_same_w;
exit when C01%notfound;
	begin
	ie_contador_w := ie_contador_w + 1;
		
	select	nr_seq_local
	into	nr_seq_local_ant_w
	from	same_prontuario
	where	nr_sequencia = nr_seq_same_w;
		
	gravar_log_tasy(78547,'nr_seq_same_w= '||nr_seq_same_w||' nr_seq_local_anterior= '||nr_seq_local_ant_w,'Tasy');
	
	update	same_prontuario
	set	nr_seq_local = 49
	where	nr_sequencia = nr_seq_same_w;	
	
	if	(ie_contador_w = 2000) then
		ie_contador_w := 1;
		commit;
	end if;
	end;
end loop;
close C01;

commit;

end hurso_atualiza_local_pront;
/