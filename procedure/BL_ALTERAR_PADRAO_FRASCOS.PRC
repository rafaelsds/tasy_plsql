create or replace
procedure BL_alterar_padrao_frascos(	nr_seq_agrupamento_p	Number,
					nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_agrupamento_p is not null) then

	update	bl_agrup_frasco
	set	ie_frasco_padrao = 'N',
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_seq_agrupamento = nr_seq_agrupamento_p;

end if;

commit;

end BL_alterar_padrao_frascos;
/