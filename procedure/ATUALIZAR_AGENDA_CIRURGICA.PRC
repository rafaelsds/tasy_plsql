create or replace
procedure atualizar_agenda_cirurgica(	nr_seq_vaga_p	number,
					nr_seq_agenda_p	number,
					nm_usuario_p	varchar2) IS 

cd_pessoa_fisica_w			varchar2(10);
cd_pessoa_fisica_age_w			varchar2(10);
cd_convenio_w				number(15,0);
cd_categoria_w				varchar2(10);
cd_plano_convenio_w			varchar2(10);
ds_cod_usuario_w			varchar2(30);         
ds_compl_w				varchar2(30);         
dt_validade_w           		date;
cd_senha_w         			varchar2(20);
cd_tipo_agenda_w			number(10);
dt_nascimento_w				date;
ds_idade_w				varchar2(40);
ie_permite_alterar_status_w		varchar2(1);
ie_status_normal_pf_w			varchar2(1); --Criado para o sirio, pois para pessoa fisica tem que atualizar para normal e quando nao tem pf trata o parametro 151.
ie_atualiza_dt_chegada_prev_w		varchar2(1);
dt_prevista_w				date;
qt_idade_W				number(10);
ds_observacao_w				varchar2(4000);
ie_atualiza_observacao_w		varchar2(1);

begin

select		max(cd_pessoa_fisica),
		max(cd_convenio),
		max(cd_categoria),
		max(cd_plano_convenio),
		max(ds_cod_usuario),         
		max(ds_compl),
		max(dt_validade),            
		max(cd_senha),
		max(cd_tipo_agenda),
		max(substr(obter_idade(to_date(obter_dados_pf(cd_pessoa_fisica,'DN'),'dd/mm/yyyy'),SYSDATE,'A'),1,40)),
		max(dt_prevista),
		substr(max(ds_observacao),1,4000)
into		cd_pessoa_fisica_w,
		cd_convenio_w,
		cd_categoria_w,
		cd_plano_convenio_w,
		ds_cod_usuario_w,
		ds_compl_w,
		dt_validade_w,
		cd_senha_w,
		cd_tipo_agenda_w,
		ds_idade_w,
		dt_prevista_w,
		ds_observacao_w
from	gestao_vaga
where	nr_sequencia = nr_seq_vaga_p;

select	max(dt_nascimento)
into	dt_nascimento_w
from	pessoa_fisica
where 	cd_pessoa_fisica = cd_pessoa_fisica_w;

if 	(cd_tipo_agenda_w = 5) then	
	update	agenda_consulta
	set	ie_status_agenda = 'N'
	where	nr_sequencia		= nr_seq_agenda_p
	and 	cd_pessoa_fisica 	is null;
	
	update	agenda_consulta
	set	cd_pessoa_fisica	= cd_pessoa_fisica_w,
		cd_convenio		= nvL(cd_convenio_w,cd_convenio),
		cd_categoria		= nvl(cd_categoria_w,cd_categoria),
		cd_plano		= nvl(cd_plano_convenio_w,cd_plano),
		cd_usuario_convenio	= nvl(ds_cod_usuario_w, cd_usuario_convenio),
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		dt_validade_carteira	= dt_validade_w
	where	nr_sequencia		= nr_seq_agenda_p;	
else
	obter_param_usuario(871, 151, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alterar_status_w);
	obter_param_usuario(1002, 82, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_status_normal_pf_w);	
	obter_param_usuario(1002, 83, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_atualiza_dt_chegada_prev_w);		
	obter_param_usuario(1002, 116, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_atualiza_observacao_w);		
	if 	(ie_permite_alterar_status_w = 'S') then	
		update	agenda_paciente
		set	ie_status_agenda = 'N'
		where	nr_sequencia		= nr_seq_agenda_p
		and 	cd_pessoa_fisica 	is null;		
	end if;

	if 	(ie_status_normal_pf_w = 'S') then	
		update	agenda_paciente
		set	ie_status_agenda = 'N'
		where	nr_sequencia		= nr_seq_agenda_p
		and 	cd_pessoa_fisica 	is not null;	
	end if;
	
	if	(ie_atualiza_dt_chegada_prev_w = 'S') then	
		update	agenda_paciente
		set	dt_chegada_prev = dt_prevista_w
		where	nr_sequencia	= nr_seq_agenda_p;	
	end if;

	
	if	(substr(ds_idade_w,1,3) = '0') then
		ds_idade_w := null;
	end if;
	
	update	agenda_paciente
	set	cd_pessoa_fisica	= cd_pessoa_fisica_w,
		dt_nascimento_pac	= nvl(dt_nascimento_w,dt_nascimento_pac),
		qt_idade_paciente	= nvl(substr(ds_idade_w,1,3),qt_idade_paciente),
		cd_convenio		= nvL(cd_convenio_w,cd_convenio),
		cd_categoria		= nvl(cd_categoria_w,cd_categoria),
		cd_plano		= nvl(cd_plano_convenio_w,cd_plano),
		cd_usuario_convenio	= nvl(ds_cod_usuario_w, cd_usuario_convenio),
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		dt_validade_carteira	= dt_validade_w,
		ds_observacao		= decode(ie_atualiza_observacao_w, 'S', ds_observacao_w, ds_observacao)
	where	nr_sequencia	= nr_seq_agenda_p;

end if;

commit;

end atualizar_agenda_cirurgica;
/
