create or replace
procedure HPD_gerar_trib_bordero_prev(NR_BORDERO_P		number,
				      DT_BAIXA_BORDERO_P	date,
				      NM_USUARIO_P		varchar2,
				      CD_ESTABELECIMENTO_P	number ) is 

nr_titulo_w		number(15);	
dt_real_pagamento_w	date;			      

Cursor C01 is
	select	nr_titulo
	from	TITULO_PAGAR_BORDERO_V
	where	nr_bordero = nr_bordero_p;			      				      
begin

select max(dt_real_pagamento)
into	dt_real_pagamento_w
from	bordero_pagamento
where	nr_bordero = nr_bordero_p;

if (dt_real_pagamento_w is null) then /*somente deletar se o bordero ainda estiver em aberto*/
	delete	w_hpd_tit_pag_trib_prev
	where	nm_usuario = NM_USUARIO_P;
end if;

open C01;
loop
fetch C01 into	
	nr_titulo_w;
exit when C01%notfound;
	begin
	HPD_Gerar_Tributo_Titulo_prev(nr_titulo_w,nm_usuario_p,'S',nr_bordero_p,nvl(dt_baixa_bordero_p,sysdate),null,null,null,cd_estabelecimento_p,null);
	end;
end loop;
close C01;
commit;
end HPD_gerar_trib_bordero_prev;
/