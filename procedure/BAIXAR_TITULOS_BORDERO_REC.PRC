create or replace procedure BAIXAR_TITULOS_BORDERO_REC
			(	nr_bordero_p		in	number,
				dt_recebimento_p	in	date,
				nm_usuario_p		in	varchar2,
				ie_obriga_dados_p	in	varchar2,
				ie_obriga_dados_perda_p	in	varchar2) is

ds_transacao_w			varchar2(255);
ie_prior_trans_bordero_rec_w	varchar2(3);
ie_bordero_banco_w		varchar2(2);
vl_saldo_titulo_w		number(15,2);
vl_saldo_bordero_rec_w		number(15,2);
vl_glosa_w			number(15,2);
vl_cambial_ativo_w		number(15,2);
vl_cambial_passivo_w		number(15,2);
vl_bordero_w			number(15,2);
vl_despesa_bordero_w		number(15,2);
vl_tributo_w			number(15,2);
nr_titulo_w			number(10,0);
nr_seq_conta_banco_w		number(10,0);
nr_seq_movto_w			number(10,0);
nr_seq_trans_fin_w		number(10,0);
nr_seq_trans_fin_bordero_w	number(10,0);
nr_seq_trans_fin_baixa_w	number(10,0);
cont_w				number(10);
nr_seq_baixa_w			number(10);
nr_seq_trans_prior_w		number(10);
nr_seq_trans_tit_bordero_w	number(10);
nr_seq_trans_fin_desp_w		number(10);
nr_seq_tf_bordero_tit_rec_w	number(10);
nr_seq_movto_pend_w		number(10);
nr_seq_trans_financ_w		number(10);
cd_tipo_recebimento_w		number(5);
cd_estabelecimento_w		number(4);
cd_tributo_w			number(3);
ie_acao_w			number(2);
ie_acresc_bordero_w		varchar2(1);
nr_seq_movto_banco_w		movto_trans_financ.nr_sequencia%type;
/* Projeto Multimoeda - Variaveis */
vl_bordero_estrang_w		number(15,2);
vl_complemento_w		number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
cd_moeda_w			number(5);
vl_despesa_bord_estrang_w	number(15,2);
vl_cotacao_bordero_w		cotacao_moeda.vl_cotacao%type;
cd_moeda_bordero_w		number(5);
cd_moeda_empresa_w		number(5);
qt_baixas_cni_w			number(10);
cd_convenio_w           bordero_recebimento.cd_convenio%type;
vl_abaixar_w              bordero_tit_rec.vl_abaixar%type;
vl_recebido_w              convenio_receb_titulo.vl_recebido%type;
lista_titulos_vl_divergente_w varchar2(4000);

cursor C01 is
	select	b.nr_titulo,
		b.cd_estabelecimento,
		nvl(a.vl_abaixar,0),
		nvl(a.vl_glosa,0),
		b.nr_seq_trans_fin_baixa,
		nvl(a.vl_cambial_ativo, 0),
		nvl(a.vl_cambial_passivo, 0),
		a.nr_seq_trans_financ
	from	titulo_receber	b,
		bordero_tit_rec	a
	where	a.nr_titulo	= b.nr_titulo
	and	a.nr_bordero	= nr_bordero_p;

Cursor C02 is
	select	a.nr_titulo,
		b.nr_seq_trans_fin_baixa,
		nvl(a.vl_abaixar,0) + decode(ie_acresc_bordero_w,'N',0,nvl(a.vl_juros,0) + nvl(a.vl_multa,0) + nvl(a.vl_rec_maior,0)),
		a.nr_seq_trans_financ,
		a.vl_abaixar_estrang,
		a.vl_cotacao,
		a.cd_moeda
	from	titulo_receber	b,
		bordero_tit_rec	a
	where	a.nr_titulo	= b.nr_titulo
	and	a.nr_bordero	= nr_bordero_p;

cursor C03 is
	select	nr_seq_movto_pend
	from	movto_banco_pend_baixa
	where	nr_bordero_rec	= nr_bordero_p;


cursor	C04 is
	select	nvl(sum(a.vl_abaixar),0) + decode(ie_acresc_bordero_w,'N',0,nvl(sum(a.vl_juros),0) + nvl(sum(a.vl_multa),0) + nvl(sum(a.vl_rec_maior),0)) vl_transacao,
		nvl(b.nr_seq_trans_fin_baixa,a.nr_seq_trans_financ) nr_seq_trans_fin,
		nvl(sum(a.vl_abaixar_estrang),0),
		a.vl_cotacao,
		a.cd_moeda
	from	titulo_receber	b,
		bordero_tit_rec	a
	where	a.nr_titulo	= b.nr_titulo
	and	a.nr_bordero	= nr_bordero_p
	group by nvl(b.nr_seq_trans_fin_baixa,a.nr_seq_trans_financ),
		a.vl_cotacao,
		a.cd_moeda;

cursor	C05 is
	select	b.nr_titulo,
		b.cd_estabelecimento,
		nvl(a.vl_abaixar,0),
		nvl(b.nr_seq_trans_fin_baixa,a.nr_seq_trans_financ)
	from	titulo_receber	b,
		bordero_tit_rec	a
	where	a.nr_titulo	= b.nr_titulo
	and	a.nr_bordero	= nr_bordero_p;

Cursor C06 is
	select	a.vl_tributo,
		a.nr_seq_trans_financ,
		a.cd_tributo,
		a.nr_titulo
	from	bordero_tit_rec_valor	a
	where	a.nr_bordero	= nr_bordero_p
	and	a.cd_tributo is not null;

cursor C07 is
    select	a.nr_titulo,
            to_number(obter_dados_bordero_tit_rec(a.nr_bordero, a.nr_titulo,'VC')) vl_calculado,
            b.vl_recebido
    from bordero_tit_rec a,
         convenio_receb_titulo b
    where a.nr_bordero = nr_bordero_p
    and a.nr_bordero  = b.nr_bordero (+)
    and a.nr_titulo  = b.nr_titulo (+);

begin
select	nvl(max(cd_tipo_recebimento),0),
	max(cd_estabelecimento),
	max(nr_seq_trans_fin),
	max(vl_despesa_bordero),
	max(nr_seq_trans_fin_desp),
	max(vl_cotacao),
	max(cd_moeda)
into	cd_tipo_recebimento_w,
	cd_estabelecimento_w,
	nr_seq_trans_fin_bordero_w,
	vl_despesa_bordero_w,
	nr_seq_trans_fin_desp_w,
	vl_cotacao_bordero_w,
	cd_moeda_bordero_w
from	bordero_recebimento
where	nr_bordero	= nr_bordero_p;

/* Projeto Multimoeda - Busca a moeda padrao da empresa */
select	obter_moeda_padrao_empresa(cd_estabelecimento_w,'E')
into	cd_moeda_empresa_w
from	dual;

select	nvl(max(ie_prior_trans_bordero_rec),'TB'),
	nvl(max(ie_bordero_banco),'B'),
	max(nr_seq_trans_tit_bordero),
	nvl(max(a.ie_acresc_bordero),'S')
into	ie_prior_trans_bordero_rec_w,
	ie_bordero_banco_w,
	nr_seq_trans_tit_bordero_w,
	ie_acresc_bordero_w
from	parametro_contas_receber a
where	cd_estabelecimento	= cd_estabelecimento_w;

if 	(cd_tipo_recebimento_w = 0) then
	/* Selecione o tipo de recebimento do bordero! */
	wheb_mensagem_pck.exibir_mensagem_abort(185562);
end if;

if	(nvl(ie_obriga_dados_p,'N') = 'S') then
	select	count(*)
	into	cont_w
	from	bordero_tit_rec
	where	vl_desconto > 0
	and	(nr_seq_motivo_desc is null or cd_centro_custo_desc is null)
	and	nr_bordero	 = nr_bordero_p;

	if	(cont_w > 0) then
		/* Existem titulos com desconto e sem centro de custo e/ou motivo de desconto informados! */
		wheb_mensagem_pck.exibir_mensagem_abort(185564);
	end if;
end if;

if	(nvl(ie_obriga_dados_perda_p,'N') = 'S') then
	select	count(*)
	into	cont_w
	from	bordero_tit_rec
	where	vl_perdas > 0
	and	(nr_seq_motivo_desc is null or cd_centro_custo_desc is null)
	and	nr_bordero	 = nr_bordero_p;

	if	(cont_w > 0) then
		/* Existem titulos com perdas e sem centro de custo e/ou motivo de desconto informados! */
		wheb_mensagem_pck.exibir_mensagem_abort(185566);
	end if;
end if;

select	max(nr_seq_conta_banco),
        max(cd_convenio)
into	nr_seq_conta_banco_w,
        cd_convenio_w
from	bordero_recebimento
where	nr_bordero = nr_bordero_p;

if (cd_convenio_w is not null) then

    open C07;
    loop
    fetch C07 into
        nr_titulo_w,
        vl_abaixar_w,
        vl_recebido_w;
    exit when C07%notfound;
        begin
            if (vl_recebido_w != vl_abaixar_w or vl_recebido_w is null) then
                lista_titulos_vl_divergente_w := substr(lista_titulos_vl_divergente_w || nr_titulo_w || ' ',1,4000);
            end if;
        end;
    end loop;
    close C07;
    
    if (lista_titulos_vl_divergente_w is not null) then
        wheb_mensagem_pck.exibir_mensagem_abort(1108721,'DS_TITULOS_W=' || lista_titulos_vl_divergente_w);
    end if;
end if;

if 	(nvl(nr_seq_conta_banco_w, 0) <> 0) then /*Francisco - OS 37663 */
	if	(ie_bordero_banco_w = 'B') then	/* um lancamento por bodero */
		if	(nvl(nr_seq_trans_fin_bordero_w,0) = 0) then
			select	nvl(min(nr_sequencia), 0)
			into	nr_seq_trans_fin_bordero_w
			from	transacao_financeira
			where	ie_acao 		= 17
			and	nvl(ie_banco,'N') 	<> 'N'
			and	ie_situacao 		= 'A'
			and	cd_estabelecimento	= cd_estabelecimento_w;
		end if;

		if 	(nvl(nr_seq_trans_fin_bordero_w,0) = 0) then
			/* Transacao de baixa de bordero recebimento nao cadastrada! */
			wheb_mensagem_pck.exibir_mensagem_abort(185567);
		end if;

		select	nvl(sum(b.vl_abaixar),0) + decode(ie_acresc_bordero_w,'N',0,nvl(sum(vl_juros),0) + nvl(sum(vl_multa),0) + nvl(sum(vl_rec_maior),0)),
			nvl(sum(b.vl_abaixar_estrang),0),
			max(b.vl_cotacao),
			max(b.cd_moeda)
		into	vl_saldo_bordero_rec_w,
			vl_bordero_estrang_w,
			vl_cotacao_w,
			cd_moeda_w
		from	bordero_tit_rec b,
			titulo_receber a
		where	a.nr_titulo	= b.nr_titulo
		and	b.nr_bordero 	= nr_bordero_p
		and	a.dt_liquidacao is null;

		select	movto_trans_financ_seq.nextval
		into	nr_seq_movto_w
		from	dual;

		/* Projeto Multimoeda - Verifica se o bordero eh moeda estrangeira, caso positivo calcula o complemento para gravar no movimento */
		if (nvl(vl_bordero_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			-- Verifica se o valor em moeda estrangeira bate com o valor em moeda nacional, caso necessario converte o valor para moeda estrangeira
			if ((vl_bordero_estrang_w * vl_cotacao_w) <> vl_saldo_bordero_rec_w) then
				vl_bordero_estrang_w := vl_saldo_bordero_rec_w / vl_cotacao_w;
			end if;
			vl_complemento_w := vl_saldo_bordero_rec_w - vl_bordero_estrang_w;
		else
			vl_bordero_estrang_w := null;
			vl_complemento_w := null;
			vl_cotacao_w := null;
			cd_moeda_w := null;
		end if;

		insert into movto_trans_financ
			(nr_sequencia,
			dt_transacao,
			nr_seq_trans_financ,
			vl_transacao,
			dt_atualizacao,
			nm_usuario,
			nr_bordero_rec,
			dt_referencia_saldo,
			nr_seq_banco,
			cd_tipo_recebimento,
			nr_lote_contabil,
			ie_conciliacao,
			nr_documento,
			vl_transacao_estrang,
			vl_complemento,
			vl_cotacao,
			cd_moeda)
		values	(nr_seq_movto_w,
			dt_recebimento_p,
			nr_seq_trans_fin_bordero_w,
			vl_saldo_bordero_rec_w,
			sysdate,
			nm_usuario_p,
			nr_bordero_p,
			dt_recebimento_p,
			nr_seq_conta_banco_w,
			cd_tipo_recebimento_w,
			0,
			'N',
			nr_bordero_p,
			vl_bordero_estrang_w,
			vl_complemento_w,
			vl_cotacao_w,
			cd_moeda_w);

		atualizar_transacao_financeira(cd_estabelecimento_w, nr_seq_movto_w, nm_usuario_p, 'I');

	/* Francisco - 29/10/2009 - OS 173868 */
	elsif	(ie_bordero_banco_w = 'T') then	/* um lancamento por titulo */
		open C02;
		loop
		fetch C02 into
			nr_titulo_w,
			nr_seq_trans_fin_baixa_w,
			vl_bordero_w,
			nr_seq_tf_bordero_tit_rec_w,
			vl_bordero_estrang_w,
			vl_cotacao_w,
			cd_moeda_w;
		exit when C02%notfound;
			begin
			if	(nr_seq_trans_tit_bordero_w is null) and (nr_seq_tf_bordero_tit_rec_w is null) then
				/* Nao foi informada a "Transacao titulo bordero"! Verifique os parametros do Contas a Receber. */
				wheb_mensagem_pck.exibir_mensagem_abort(184026);
			end if;

			select	max(ie_acao),
				max(ds_transacao)
			into	ie_acao_w,
				ds_transacao_w
			from	transacao_financeira
			where	nr_sequencia	= nvl(nr_seq_tf_bordero_tit_rec_w, nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_tit_bordero_w));

			if	(ie_acao_w not in (1,18)) then
				/* A transacao "ds_transacao_w" nao possui acao "Baixa de titulo a receber", verifique. */
				wheb_mensagem_pck.exibir_mensagem_abort(185570,'DS_TRANSACAO_W=' || ds_transacao_w);
			end if;

			select	movto_trans_financ_seq.nextval
			into	nr_seq_movto_w
			from	dual;

			/* Projeto Multimoeda - Verifica se o bordero eh moeda estrangeira, caso positivo calcula o complemento para gravar no movimento */
			if (nvl(vl_bordero_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
				-- Verifica se o valor em moeda estrangeira bate com o valor em moeda nacional, caso necessario converte o valor para moeda estrangeira
				if ((vl_bordero_estrang_w * vl_cotacao_w) <> vl_bordero_w) then
					vl_bordero_estrang_w := vl_bordero_w / vl_cotacao_w;
				end if;
				vl_complemento_w := vl_bordero_w - vl_bordero_estrang_w;
			else
				vl_bordero_estrang_w := null;
				vl_complemento_w := null;
				vl_cotacao_w := null;
				cd_moeda_w := null;
			end if;

			insert into movto_trans_financ
				(nr_sequencia,
				dt_transacao,
				nr_seq_trans_financ,
				vl_transacao,
				dt_atualizacao,
				nm_usuario,
				nr_bordero_rec,
				dt_referencia_saldo,
				nr_seq_banco,
				cd_tipo_recebimento,
				nr_lote_contabil,
				ie_conciliacao,
				nr_seq_titulo_receber,
				nr_documento,
				vl_transacao_estrang,
				vl_complemento,
				vl_cotacao,
				cd_moeda)
			values	(nr_seq_movto_w,
				dt_recebimento_p,
				nvl(nr_seq_tf_bordero_tit_rec_w, nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_tit_bordero_w)),
				vl_bordero_w,
				sysdate,
				nm_usuario_p,
				nr_bordero_p,
				dt_recebimento_p,
				nr_seq_conta_banco_w,
				cd_tipo_recebimento_w,
				0,
				'N',
				nr_titulo_w,
				nr_bordero_p,
				vl_bordero_estrang_w,
				vl_complemento_w,
				vl_cotacao_w,
				cd_moeda_w);

			atualizar_transacao_financeira(cd_estabelecimento_w, nr_seq_movto_w, nm_usuario_p, 'I');
			end;
		end loop;
		close C02;

		update	bordero_recebimento
		set	dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_recebimento	= dt_recebimento_p
		where	nr_bordero	= nr_bordero_p;
	elsif	(ie_bordero_banco_w = 'R') then	/* Um lancamento para cada transacao de baixa dos titulos */
		open C04;
		loop
		fetch C04 into
			vl_bordero_w,
			nr_seq_trans_fin_baixa_w,
			vl_bordero_estrang_w,
			vl_cotacao_w,
			cd_moeda_w;
		exit when C04%notfound;
			begin
			if	(nr_seq_trans_tit_bordero_w is null) and (nr_seq_trans_fin_baixa_w is null) then
				/* Nao foi informada a "Transacao titulo bordero"! Verifique os parametros do Contas a Receber. */
				wheb_mensagem_pck.exibir_mensagem_abort(184026);
			end if;

			select	movto_trans_financ_seq.nextval
			into	nr_seq_movto_w
			from	dual;

			/* Projeto Multimoeda - Verifica se o bordero eh moeda estrangeira, caso positivo calcula o complemento para gravar no movimento */
			if (nvl(vl_bordero_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
				-- Verifica se o valor em moeda estrangeira bate com o valor em moeda nacional, caso necessario converte o valor para moeda estrangeira
				if ((vl_bordero_estrang_w * vl_cotacao_w) <> vl_bordero_w) then
					vl_bordero_estrang_w := vl_bordero_w / vl_cotacao_w;
				end if;
				vl_complemento_w := vl_bordero_w - vl_bordero_estrang_w;
			else
				vl_bordero_estrang_w := null;
				vl_complemento_w := null;
				vl_cotacao_w := null;
				cd_moeda_w := null;
			end if;

			insert into movto_trans_financ
				(nr_sequencia,
				dt_transacao,
				nr_seq_trans_financ,
				vl_transacao,
				dt_atualizacao,
				nm_usuario,
				dt_referencia_saldo,
				nr_seq_banco,
				cd_tipo_recebimento,
				nr_lote_contabil,
				ie_conciliacao,
				nr_seq_titulo_receber,
				nr_bordero_rec,
				nr_documento,
				vl_transacao_estrang,
				vl_complemento,
				vl_cotacao,
				cd_moeda)
			values	(nr_seq_movto_w,
				dt_recebimento_p,
				nvl(nr_seq_trans_fin_baixa_w, nr_seq_trans_tit_bordero_w),
				vl_bordero_w,
				sysdate,
				nm_usuario_p,
				dt_recebimento_p,
				nr_seq_conta_banco_w,
				cd_tipo_recebimento_w,
				0,
				'N',
				nr_titulo_w,
				nr_bordero_p,
				nr_bordero_p,
				vl_bordero_estrang_w,
				vl_complemento_w,
				vl_cotacao_w,
				cd_moeda_w);

			atualizar_transacao_financeira(cd_estabelecimento_w, nr_seq_movto_w, nm_usuario_p, 'I');
			end;
		end loop;
		close c04;

		open C05;
		loop
		fetch C05 into
			nr_titulo_w,
			cd_estabelecimento_w,
			vl_saldo_titulo_w,
			nr_seq_trans_fin_baixa_w;
		exit when C05%notfound;
			begin
			baixa_titulo_receber(	cd_estabelecimento_w,
						cd_tipo_recebimento_w,
						nr_titulo_w,
						nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_tit_bordero_w),
						vl_saldo_titulo_w,
						dt_recebimento_p,
						nm_usuario_p,
						0,
						nr_bordero_p,
						null,
						0,
						0);

			select	max(nr_sequencia)
			into	nr_seq_baixa_w
			from	titulo_receber_liq
			where	nr_titulo	= nr_titulo_w;

			atualizar_baixa_gratuidade(nr_titulo_w,nr_seq_baixa_w ,nm_usuario_p);

			gerar_baixa_nota_credito(nr_seq_baixa_w,null,nr_bordero_p,nr_titulo_w,nm_usuario_p,'B');

			atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);

			/*aamfirmo OS 881490 - Gravar na baixa do titulo a sequencia do movimento bancario gerado para o bordero.*/
			select 	max(nr_sequencia)
			into	nr_seq_baixa_w
			from	titulo_receber_liq
			where	nr_titulo	= nr_titulo_w
			and		nr_bordero 	= nr_bordero_p;

			if (nr_seq_baixa_w is not null) and (nr_titulo_w is not null) then
				/*Buscar o movimento gerado no banco para a transacao*/
				select	max(nr_sequencia)
				into	nr_seq_movto_banco_w
				from	movto_trans_financ
				where	nr_bordero_rec		= nr_bordero_p
				and		nr_seq_trans_financ	= nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_tit_bordero_w);

				if (nr_seq_movto_banco_w is not null) then
					update	titulo_receber_liq
					set		nr_seq_movto_trans_fin	= nr_seq_movto_banco_w
					where	nr_sequencia			= nr_seq_baixa_w
					and		nr_titulo				= nr_titulo_w;
				end if;

			end if;

			end;
		end loop;
		close C05;

		update	bordero_recebimento
		set	dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_recebimento	= dt_recebimento_p
		where	nr_bordero	= nr_bordero_p;
	end if;

	if	(nr_seq_trans_fin_desp_w is not null) and
		(nvl(vl_despesa_bordero_w,0) <> 0) then
		select	movto_trans_financ_seq.nextval
		into	nr_seq_movto_w
		from	dual;

		/* Projeto Multimoeda - Verifica se o bodero eh moeda estrangeira, caso positivo converte o valor da despesa para gerar o movimento */
		if (nvl(cd_moeda_bordero_w,cd_moeda_empresa_w) <> cd_moeda_empresa_w and nvl(vl_cotacao_bordero_w,0) <> 0) then
			vl_despesa_bord_estrang_w := vl_despesa_bordero_w / vl_cotacao_bordero_w;
			vl_complemento_w := vl_despesa_bordero_w - vl_despesa_bord_estrang_w;
			vl_cotacao_w := vl_cotacao_bordero_w;
			cd_moeda_w := cd_moeda_bordero_w;
		else
			vl_despesa_bord_estrang_w := null;
			vl_complemento_w := null;
			vl_cotacao_w := null;
			cd_moeda_w := null;
		end if;

		insert into movto_trans_financ
			(nr_sequencia,
			dt_transacao,
			nr_seq_trans_financ,
			vl_transacao,
			dt_atualizacao,
			nm_usuario,
			nr_bordero_rec,
			dt_referencia_saldo,
			nr_seq_banco,
			cd_tipo_recebimento,
			nr_lote_contabil,
			ie_conciliacao,
			nr_documento,
			vl_transacao_estrang,
			vl_complemento,
			vl_cotacao,
			cd_moeda)
		values	(nr_seq_movto_w,
			dt_recebimento_p,
			nr_seq_trans_fin_desp_w,
			vl_despesa_bordero_w,
			sysdate,
			nm_usuario_p,
			nr_bordero_p,
			dt_recebimento_p,
			nr_seq_conta_banco_w,
			cd_tipo_recebimento_w,
			0,
			'N',
			nr_bordero_p,
			vl_despesa_bord_estrang_w,
			vl_complemento_w,
			vl_cotacao_w,
			cd_moeda_w);

		atualizar_transacao_financeira(cd_estabelecimento_w, nr_seq_movto_w, nm_usuario_p, 'I');
	end if;
else
	open C01;
	loop
	fetch C01 into
		nr_titulo_w,
		cd_estabelecimento_w,
		vl_saldo_titulo_w,
		vl_glosa_w,
		nr_seq_trans_fin_baixa_w,
		vl_cambial_ativo_w,
		vl_cambial_passivo_w,
		nr_seq_trans_financ_w;
	exit when C01%notfound;
		begin
		if	(ie_prior_trans_bordero_rec_w = 'TB') then
			nr_seq_trans_prior_w	:= nvl(nr_seq_trans_fin_baixa_w, nvl(nr_seq_trans_fin_bordero_w,nr_seq_trans_financ_w));
		else
			nr_seq_trans_prior_w	:= nvl(nr_seq_trans_fin_bordero_w, nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_financ_w));
		end if;

		baixa_titulo_receber(	cd_estabelecimento_w,
					cd_tipo_recebimento_w,
					nr_titulo_w,
					nr_seq_trans_prior_w,
					vl_saldo_titulo_w,
					dt_recebimento_p,
					nm_usuario_p,
					vl_glosa_w,
					nr_bordero_p,
					null,
					0,
					null);

		select	max(nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_receber_liq
		where	nr_titulo	= nr_titulo_w;

		update	titulo_receber_liq
		set	vl_cambial_ativo		= vl_cambial_ativo_w,
			vl_cambial_passivo		= vl_cambial_passivo_w
		where	nr_titulo			= nr_titulo_w
		and	nr_sequencia			= nr_seq_baixa_w;

		atualizar_baixa_gratuidade(nr_titulo_w,nr_seq_baixa_w ,nm_usuario_p);

		gerar_baixa_nota_credito(nr_seq_baixa_w,null,nr_bordero_p,nr_titulo_w,nm_usuario_p,'B');

		atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);
		end;
	end loop;
	close C01;

	update	bordero_recebimento
	set	dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_recebimento	= dt_recebimento_p
	where	nr_bordero	= nr_bordero_p;
end if;

open C03;
loop
fetch C03 into
	nr_seq_movto_pend_w;
exit when C03%notfound;
	begin
	update	movto_banco_pend_baixa
	set		dt_baixa		= dt_recebimento_p,
			ie_lib_bordero_rec 	= 'S',
			nm_usuario			= nm_usuario_p,
			dt_atualizacao		= sysdate
	where	nr_bordero_rec	= nr_bordero_p
	and		ie_lib_bordero_rec = 'N';

	atualizar_saldo_movto_bco_pend(nr_seq_movto_pend_w,'S',nm_usuario_p);
	end;
end loop;
close C03;

open C06;
loop
fetch C06 into
	vl_tributo_w,
	nr_seq_trans_financ_w,
	cd_tributo_w,
	nr_titulo_w;
exit when C06%notfound;
	begin
	insert into titulo_receber_trib
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_trans_financ,
		nr_lote_contabil,
		nr_titulo,
		cd_tributo,
		tx_tributo,
		vl_tributo,
		vl_base_calculo,
		vl_trib_adic,
		vl_base_adic,
		vl_trib_nao_retido,
		vl_base_nao_retido,
		ie_origem_tributo,
		dt_tributo)
		values	(titulo_receber_trib_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_trans_financ_w,
		0,
		nr_titulo_w,
		cd_tributo_w,
		0,
		vl_tributo_w,
		0,
		0,
		0,
		0,
		0,
		'D',
		dt_recebimento_p);
	end;
end loop;
close C06;

commit;

end BAIXAR_TITULOS_BORDERO_REC;
/