create or replace
procedure pls_consistir_carencia_mat(	nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
					nr_seq_guia_mat_p		pls_guia_plano_mat.nr_sequencia%type,
					nr_seq_guia_p			number,
					nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type,
					nr_seq_requisicao_mat_p		pls_requisicao_mat.nr_sequencia%type,
					nr_seq_material_p		pls_material.nr_sequencia%type,
					nr_seq_estrut_mat_p		number,
					dt_solicitacao_p		date,
					nr_seq_tipo_acomod_p		number,
					ie_tipo_guia_p			varchar2,
					ie_carencia_abrangencia_ant_p	varchar2,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type) is

nr_seq_plano_w			number(10);
nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_carencia_mat_w		number(10);
qt_dias_w			number(5);
dt_inicio_vigencia_w		date;
dt_carencia_w			date;
dt_inclusao_operadora_w		date;
ie_liberado_w			varchar2(1);
ie_origem_w			varchar2(1);
nr_seq_carencia_w		number(10,0);
ie_mes_posterior_w		varchar2(1);
nr_seq_guia_w			number(10);
ie_tipo_cobertura_w		varchar2(1);
nr_seq_prestador_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_tipo_carencia_w		number(10);
ie_tipo_consistencia_w		varchar2(2);
nr_requisicao_w			number(10);
ie_tipo_despesa_w		varchar2(10);
ie_localizacao_w		varchar2(255);
ds_carencia_w			varchar2(255);
qt_carencia_mat_w		pls_integer;
nr_seq_segurado_ant_w		number(10);
ie_consistir_caren_prod_ant_w	varchar2(10);
dt_contratacao_w		date;
ds_observacao_abrang_ant_w	varchar2(255);
nr_seq_tipo_carencia_aux_w	number(10);
nr_seq_uni_exec_w		number(10);
qt_regra_carencia_w		pls_integer;
ie_carater_solic_w		varchar2(1);
nr_seq_regra_carencia_w		number(10);
qt_glosa_carencia_w		pls_integer;
ie_gerar_glosa_w		varchar2(1);
nr_seq_plano_aux_w		number(10);
qt_dias_fora_abrang_ant_w	pls_carencia.qt_dias_fora_abrang_ant%type;
ie_tipo_protocolo_w		pls_protocolo_conta.ie_tipo_protocolo%type;
ie_valido_w			varchar2(1);
nr_seq_rede_w			pls_carencia.nr_seq_rede%type;

Cursor c01 is
	select	a.nr_sequencia,
		nvl(a.ie_mes_posterior,'N') ie_mes_posterior,
		a.nr_seq_tipo_carencia,
		a.nr_seq_rede
	from	pls_carencia a
	where	a.nr_seq_segurado = nr_seq_segurado_p
	union all
	select	a.nr_sequencia,
		nvl(a.ie_mes_posterior,'N'),
		a.nr_seq_tipo_carencia,
		a.nr_seq_rede
	from	pls_carencia a
	where	a.nr_seq_contrato = nr_seq_contrato_w
	and	((a.nr_seq_plano_contrato = nr_seq_plano_w) or (a.nr_seq_plano_contrato is null))
	and not exists(	select	1
			from	pls_carencia b
			where	b.nr_seq_segurado = nr_seq_segurado_p
			and	b.ie_cpt = 'N')
	union all
	select	a.nr_sequencia,
		nvl(a.ie_mes_posterior,'N'),
		a.nr_seq_tipo_carencia,
		a.nr_seq_rede
	from	pls_carencia a
	where	a.nr_seq_plano = nr_seq_plano_w
	and not exists(	select	1
			from	pls_carencia b
			where	b.nr_seq_segurado = nr_seq_segurado_p
			and	b.ie_cpt = 'N')
	and not exists(	select	1
			from	pls_carencia c
			where	c.nr_seq_contrato = nr_seq_contrato_w
			and	c.ie_cpt = 'N')
	and	((a.dt_inicio_vig_plano is null) or (dt_contratacao_w >= a.dt_inicio_vig_plano))
	and	((a.dt_fim_vig_plano is null) or (dt_contratacao_w <= a.dt_fim_vig_plano));

-- tudo que era feito OR passei a retornar no cursor e validar dentro do FOR
--precisei colocar o nr_seq_material novamente por causa da quantidade de registros na carencia da UM
cursor c02 is
	select	a.nr_sequencia nr_seq_carencia,
		a.ie_liberado,
		c.qt_dias,
		c.dt_inicio_vigencia,
		c.nr_seq_segurado,
		c.nr_seq_contrato,
		c.nr_seq_plano,
		a.nr_seq_tipo_carencia,
		a.ie_tipo_despesa ie_despesa,
		a.nr_seq_estrut_mat nr_seq_estru,
		a.nr_seq_material nr_seq_mat,
		decode(a.ie_liberado, 'S', 0, 1) ie_lib,
		c.qt_dias_fora_abrang_ant,
		a.nr_seq_grupo_material,
		pls_se_grupo_preco_material(a.nr_seq_grupo_material, nr_seq_material_p) ie_grupo_preco_mat,
		pls_obter_se_mat_estrutura(nr_seq_material_p, a.nr_seq_estrut_mat) ie_estru_mat
	from	pls_carencia_mat a,
		pls_carencia c
	where	c.nr_sequencia = nr_seq_carencia_w
	and	a.nr_seq_tipo_carencia = c.nr_seq_tipo_carencia
	and	(a.nr_seq_material = nr_seq_material_p or a.nr_seq_material is null)
	and not exists(	select	1
			from	pls_tipo_carencia b
			where 	b.nr_sequencia = a.nr_seq_tipo_carencia
			and	b.ie_cpt = 'S')
	union all
	select	a.nr_sequencia,
		a.ie_liberado,
		d.qt_dias,
		d.dt_inicio_vigencia,
		d.nr_seq_segurado,
		d.nr_seq_contrato,
		d.nr_seq_plano,
		a.nr_seq_tipo_carencia,
		a.ie_tipo_despesa ie_despesa,
		a.nr_seq_estrut_mat nr_seq_estru,
		a.nr_seq_material nr_seq_mat,
		decode(a.ie_liberado, 'S', 0, 1) ie_lib,
		d.qt_dias_fora_abrang_ant,
		a.nr_seq_grupo_material,
		pls_se_grupo_preco_material(a.nr_seq_grupo_material, nr_seq_material_p) ie_grupo_preco_mat,
		pls_obter_se_mat_estrutura(nr_seq_material_p, a.nr_seq_estrut_mat) ie_estru_mat
	from	pls_carencia d,
		pls_grupo_carencia c,
		pls_tipo_carencia b,
		pls_carencia_mat a
	where	b.nr_sequencia = a.nr_seq_tipo_carencia
	and	d.nr_seq_grupo_carencia = c.nr_sequencia
	and	c.nr_sequencia = b.nr_seq_grupo
	and	d.nr_sequencia = nr_seq_carencia_w
	and	d.nr_seq_grupo_carencia is not null
	and	a.nr_seq_material = nr_seq_material_p 
	and	b.ie_cpt = 'N'
	union all
	select	a.nr_sequencia,
		a.ie_liberado,
		d.qt_dias,
		d.dt_inicio_vigencia,
		d.nr_seq_segurado,
		d.nr_seq_contrato,
		d.nr_seq_plano,
		a.nr_seq_tipo_carencia,
		a.ie_tipo_despesa ie_despesa,
		a.nr_seq_estrut_mat nr_seq_estru,
		a.nr_seq_material nr_seq_mat,
		decode(a.ie_liberado, 'S', 0, 1) ie_lib,
		d.qt_dias_fora_abrang_ant,
		a.nr_seq_grupo_material,
		pls_se_grupo_preco_material(a.nr_seq_grupo_material, nr_seq_material_p) ie_grupo_preco_mat,
		pls_obter_se_mat_estrutura(nr_seq_material_p, a.nr_seq_estrut_mat) ie_estru_mat
	from	pls_carencia d,
		pls_grupo_carencia c,
		pls_tipo_carencia b,
		pls_carencia_mat a
	where	b.nr_sequencia = a.nr_seq_tipo_carencia
	and	d.nr_seq_grupo_carencia = c.nr_sequencia
	and	c.nr_sequencia = b.nr_seq_grupo
	and	d.nr_sequencia = nr_seq_carencia_w
	and	d.nr_seq_grupo_carencia is not null
	and	a.nr_seq_material is null
	and	b.ie_cpt = 'N'
	order by ie_despesa,
		nr_seq_estru,
		nr_seq_mat,
		ie_lib;

begin
if 	(nvl(nr_seq_guia_mat_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'G';
elsif	(nvl(nr_seq_conta_mat_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'C';
elsif	(nvl(nr_seq_requisicao_mat_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'R';
end if;

select	max(nr_seq_plano),
	max(nr_seq_contrato),
	max(dt_inclusao_operadora),
	max(nr_seq_segurado_ant),
	max(dt_contratacao)
into	nr_seq_plano_w,
	nr_seq_contrato_w,
	dt_inclusao_operadora_w,
	nr_seq_segurado_ant_w,
	dt_contratacao_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

-- Obter dados do cadastro do material 
select	max(ie_tipo_despesa)
into	ie_tipo_despesa_w
from	pls_material
where	nr_sequencia = nr_seq_material_p;

--So entrar na rotina se tem carencia de material
select	count(1)
into	qt_carencia_mat_w
from	pls_carencia_mat
where	rownum <= 1;

if	(qt_carencia_mat_w > 0) then
	if	(ie_tipo_consistencia_w = 'G') then
		select 	max(nr_seq_guia),
			max(ie_tipo_cobertura)
		into	nr_seq_guia_w,
			ie_tipo_cobertura_w
		from	pls_guia_plano_mat
		where	nr_sequencia = nr_seq_guia_mat_p;
		
		select	max(nr_seq_prestador),
			max(nr_seq_uni_exec),
			max(ie_carater_internacao)
		into	nr_seq_prestador_w,
			nr_seq_uni_exec_w,
			ie_carater_solic_w
		from	pls_guia_plano
		where	nr_sequencia = nr_seq_guia_w;
	elsif	(ie_tipo_consistencia_w = 'C') then
		select	max(nr_seq_conta),
			max(ie_tipo_cobertura)
		into	nr_seq_conta_w,
			ie_tipo_cobertura_w
		from	pls_conta_mat
		where	nr_sequencia = nr_seq_conta_mat_p;
		
		select	max(a.nr_seq_prestador),
			max(a.nr_seq_congenere),
			max(b.ie_carater_internacao),
			max(a.ie_tipo_protocolo)
		into	nr_seq_prestador_w,
			nr_seq_uni_exec_w,
			ie_carater_solic_w,
			ie_tipo_protocolo_w
		from	pls_protocolo_conta a,
			pls_conta b
		where	a.nr_sequencia = b.nr_seq_protocolo
		and	b.nr_sequencia = nr_seq_conta_w;
		
		if	(ie_tipo_protocolo_w = 'R') then
			ie_tipo_consistencia_w := 'CR';
		end if;
	elsif	(ie_tipo_consistencia_w = 'R') then
		select 	max(nr_seq_requisicao),
			max(ie_tipo_cobertura)
		into	nr_requisicao_w,
			ie_tipo_cobertura_w
		from	pls_requisicao_mat
		where	nr_sequencia = nr_seq_requisicao_mat_p;
		
		select	max(nr_seq_prestador),
			max(nr_seq_uni_exec),
			max(ie_carater_atendimento)
		into	nr_seq_prestador_w,
			nr_seq_uni_exec_w,
			ie_carater_solic_w
		from	pls_requisicao
		where	nr_sequencia = nr_requisicao_w;
	end if;
	
	ie_consistir_caren_prod_ant_w	:= 'S';
	
	if	(ie_carencia_abrangencia_ant_p = 'S') and
		(nr_seq_segurado_p	is not null)then
		pls_obter_se_caren_plano_ant(nr_seq_segurado_p, nr_seq_plano_w, nr_seq_segurado_ant_w,nr_seq_prestador_w, nr_seq_uni_exec_w, ie_consistir_caren_prod_ant_w);
	end if;
	
	open c01;
	loop
	fetch c01 into
		nr_seq_carencia_w,
		ie_mes_posterior_w,
		nr_seq_tipo_carencia_aux_w,
		nr_seq_rede_w;
	exit when c01%notfound;

		select	count(1)
		into	qt_carencia_mat_w
		from	pls_carencia_mat a
		where	a.nr_seq_tipo_carencia = nr_seq_tipo_carencia_aux_w;

		if	(nr_seq_rede_w is not null) then
			if	(pls_obter_se_prest_rede_atend(nr_seq_prestador_w,nr_seq_rede_w) = 'N') then
				goto fim_cursor_C01;
			end if;
		end if;
		
		if	(qt_carencia_mat_w > 0) then
			if	(ie_mes_posterior_w = 'S') then
				dt_inclusao_operadora_w	:= add_months(trunc(dt_inclusao_operadora_w,'month'),1);
			end if;
			
			if	(ie_tipo_consistencia_w in ('G','C','R','CR')) then
				ie_liberado_w	:= 'N';
				
				for r_c02_w in c02 loop
					ie_valido_w := 'S';
				
					-- tipo de despesa
					if	(r_c02_w.ie_despesa is not null) and
						(r_c02_w.ie_despesa != ie_tipo_despesa_w) then
						ie_valido_w := 'N';
					end if;
				
					-- se foi passado a estrutura de material por parametro so pode considerar as regras que tenham a mesma estrutura
					if	(nr_seq_estrut_mat_p is not null) and
						(nr_seq_estrut_mat_p != r_c02_w.nr_seq_estru) then
						ie_valido_w := 'N';
					end if;

					-- se possui estrutura de material na regra entao verifica se o material pertence a estrutura
					if	(r_c02_w.nr_seq_estru is not null) and
						(r_c02_w.ie_estru_mat = 'N') then
						ie_valido_w := 'N';
					end if;
					
					-- se possui grupo de material informado entao verifica
					if 	(r_c02_w.nr_seq_grupo_material is not null) and
						(r_c02_w.ie_grupo_preco_mat = 'N') then
						ie_valido_w := 'N';
					end if;
					
					-- se for valido alimenta as variaveis, senao limpa elas a ultima ira definir se e ou nao valida
					if	(ie_valido_w = 'S') then
						qt_dias_w := r_c02_w.qt_dias;
						ie_liberado_w := r_c02_w.ie_liberado;
						nr_seq_segurado_w := r_c02_w.nr_seq_segurado;
						nr_seq_contrato_w := r_c02_w.nr_seq_contrato;
						nr_seq_plano_aux_w := r_c02_w.nr_seq_plano;
						dt_inicio_vigencia_w := r_c02_w.dt_inicio_vigencia;
						nr_seq_carencia_mat_w := r_c02_w.nr_seq_carencia;
						nr_seq_tipo_carencia_w := r_c02_w.nr_seq_tipo_carencia;
						qt_dias_fora_abrang_ant_w := r_c02_w.qt_dias_fora_abrang_ant;
						exit;
					else
						qt_dias_w := null;
						ie_liberado_w := null;
						nr_seq_segurado_w := null;
						nr_seq_contrato_w := null;
						nr_seq_plano_aux_w := null;
						dt_inicio_vigencia_w := null;
						nr_seq_carencia_mat_w := null;
						nr_seq_tipo_carencia_w := null;
						qt_dias_fora_abrang_ant_w := null;
					end if;
				end loop;
				
				if	(nr_seq_segurado_w is not null) then
					ie_origem_w	:= 'B';
				elsif	(nr_seq_contrato_w is not null) then
					ie_origem_w	:= 'C';
				else
					ie_origem_w	:= 'P';
				end if;
				
				if	(ie_liberado_w	= 'S') then
					ds_observacao_abrang_ant_w	:= '';
					/*Caso a abrangencia anterior nao tinha abrangencia no prestador atual, entao o sistema consiste a data pela migracao do produto novo*/
					if	(ie_consistir_caren_prod_ant_w = 'N') then
						ds_observacao_abrang_ant_w	:= ' ' || wheb_mensagem_pck.get_texto(1108841);
						
						if	(nr_seq_segurado_ant_w is not null) then
							dt_carencia_w := dt_contratacao_w + nvl(qt_dias_fora_abrang_ant_w,qt_dias_w);
						else
							select	max(dt_alteracao)
							into	dt_carencia_w
							from	pls_segurado_alt_plano a
							where	nr_seq_segurado	= nr_seq_segurado_p
							and	ie_situacao = 'A'
							and	nr_seq_plano_atual	= nr_seq_plano_w;
							
							dt_carencia_w := dt_carencia_w + nvl(qt_dias_fora_abrang_ant_w,qt_dias_w);
						end if;
					else
						dt_carencia_w := nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w) + qt_dias_w;
					end if;
					
					if	(nvl(dt_solicitacao_p,sysdate) < dt_carencia_w) then
						if	(nr_seq_tipo_carencia_w is not null) then
							select	ds_carencia
							into	ds_carencia_w
							from	pls_tipo_carencia
							where	nr_sequencia = nr_seq_tipo_carencia_w;
						end if;
						
						if	(nr_seq_carencia_w is not null) then
							select	decode(nr_seq_segurado,null,(decode(nr_seq_contrato,null,'Produto','Contrato')),wheb_mensagem_pck.get_texto(1108818))
							into	ie_localizacao_w
							from	pls_carencia
							where	nr_sequencia	= nr_seq_carencia_w;
						end if;
						
						if 	(ie_tipo_consistencia_w = 'G') then
							select	count(1)
							into	qt_regra_carencia_w
							from	pls_regra_lanc_carencia
							where	rownum = 1;
							
							if	(qt_regra_carencia_w > 0) then
								pls_obter_regra_carencia(	ie_carater_solic_w, nr_seq_plano_aux_w, nm_usuario_p, 
												cd_estabelecimento_p, nr_seq_regra_carencia_w);
								
								if	(nr_seq_regra_carencia_w > 0) then
									pls_guia_gravar_historico(	nr_seq_guia_w, 2, wheb_mensagem_pck.get_texto(1108815, 'NR_SEQ_REGRA_CARENCIA='||nr_seq_regra_carencia_w),
													'', nm_usuario_p);
								else
									ie_gerar_glosa_w := 'S';
								end if;
							else
								ie_gerar_glosa_w := 'S';
							end if;
							
							if	(ie_gerar_glosa_w = 'S') then
								update	pls_guia_plano_mat
								set	nr_seq_tipo_carencia = nr_seq_tipo_carencia_w
								where	nr_sequencia = nr_seq_guia_mat_p;
								
								pls_gravar_motivo_glosa('1410', null, null,
											nr_seq_guia_mat_p, wheb_mensagem_pck.get_texto(1108799, 'NR_SEQ_TIPO_CARENCIA='||nr_seq_tipo_carencia_w||';DS_CARENCIA='||ds_carencia_w||';NR_SEQ_CARENCIA_MAT='||
											nr_seq_carencia_mat_w||';IE_LOCALIZACAO='||ie_localizacao_w||';DT_CARENCIA='||to_char(dt_carencia_w,'dd/mm/yyyy')||';QT_DIAS='||qt_dias_w||';DS_OBSERVACAO_ABRANG_ANT='||ds_observacao_abrang_ant_w), nm_usuario_p,
											ie_origem_w, 'CG', nr_seq_prestador_w,
											'(Regra ' || nr_seq_tipo_carencia_w || ' -> ' || nr_seq_carencia_mat_w || ')',null);
							end if;
						elsif 	(ie_tipo_consistencia_w = 'C') then
							select	count(1)
							into	qt_regra_carencia_w
							from	pls_regra_lanc_carencia
							where	rownum = 1;
							
							if	(qt_regra_carencia_w > 0) then
								pls_obter_regra_carencia(	ie_carater_solic_w, nr_seq_plano_aux_w, nm_usuario_p, 
												cd_estabelecimento_p, nr_seq_regra_carencia_w);
								
								if	(nr_seq_regra_carencia_w > 0) then
									ie_gerar_glosa_w := 'N';
								else
									ie_gerar_glosa_w := 'S';
								end if;
							else
								ie_gerar_glosa_w := 'S';
							end if;
							
							if	(ie_gerar_glosa_w = 'S') then
								pls_gravar_conta_glosa('1410', null, null,
										nr_seq_conta_mat_p, 'N', wheb_mensagem_pck.get_texto(1108799, 'NR_SEQ_TIPO_CARENCIA='||nr_seq_tipo_carencia_w||';DS_CARENCIA='||ds_carencia_w||';NR_SEQ_CARENCIA_MAT='||
										nr_seq_carencia_mat_w||';IE_LOCALIZACAO='||ie_localizacao_w||';DT_CARENCIA='||to_char(dt_carencia_w,'dd/mm/yyyy')||';QT_DIAS='||qt_dias_w||';DS_OBSERVACAO_ABRANG_ANT='||ds_observacao_abrang_ant_w), nm_usuario_p,
										'A', 'CC', nr_seq_prestador_w,
										cd_estabelecimento_p, '', null);
							end if;
						elsif 	(ie_tipo_consistencia_w = 'R') then
							select	count(1)
							into	qt_regra_carencia_w
							from	pls_regra_lanc_carencia
							where	rownum = 1;
							
							if	(qt_regra_carencia_w > 0) then
								pls_obter_regra_carencia(	ie_carater_solic_w, nr_seq_plano_aux_w, nm_usuario_p, 
												cd_estabelecimento_p, nr_seq_regra_carencia_w);
								
								if	(nr_seq_regra_carencia_w > 0) then
									pls_requisicao_gravar_hist(	nr_requisicao_w, 'L', wheb_mensagem_pck.get_texto(1108815, 'NR_SEQ_REGRA_CARENCIA='||nr_seq_regra_carencia_w),
													null, nm_usuario_p);
								else
									ie_gerar_glosa_w := 'S';
								end if;
							else
								ie_gerar_glosa_w := 'S';
							end if;
							
							if	(ie_gerar_glosa_w = 'S') then
								update	pls_requisicao_mat
								set	nr_seq_tipo_carencia = nr_seq_tipo_carencia_w
								where	nr_sequencia = nr_seq_requisicao_mat_p;

								pls_gravar_requisicao_glosa(	'1410', null, null,
												nr_seq_requisicao_mat_p, wheb_mensagem_pck.get_texto(1108799, 'NR_SEQ_TIPO_CARENCIA='||nr_seq_tipo_carencia_w||';DS_CARENCIA='||ds_carencia_w||';NR_SEQ_CARENCIA_MAT='||
												nr_seq_carencia_mat_w||';IE_LOCALIZACAO='||ie_localizacao_w||';DT_CARENCIA='||to_char(dt_carencia_w,'dd/mm/yyyy')||';QT_DIAS='||qt_dias_w||';DS_OBSERVACAO_ABRANG_ANT='||ds_observacao_abrang_ant_w), nm_usuario_p,
												nr_seq_prestador_w, cd_estabelecimento_p, null,
												'');
							end if;
						elsif 	(ie_tipo_consistencia_w = 'CR') then
							pls_gravar_conta_glosa('1410', null, null,
									nr_seq_conta_mat_p, 'N', wheb_mensagem_pck.get_texto(1108799, 'NR_SEQ_TIPO_CARENCIA='||nr_seq_tipo_carencia_w||';DS_CARENCIA='||ds_carencia_w||';NR_SEQ_CARENCIA_MAT='||
									nr_seq_carencia_mat_w||';IE_LOCALIZACAO='||ie_localizacao_w||';DT_CARENCIA='||to_char(dt_carencia_w,'dd/mm/yyyy')||';QT_DIAS='||qt_dias_w||';DS_OBSERVACAO_ABRANG_ANT='||ds_observacao_abrang_ant_w), nm_usuario_p,
									'A', 'CR', nr_seq_prestador_w,
									cd_estabelecimento_p, '', null);
						end if;
					end if;
				end if;
			end if;
		end if; /* So se tiver regra de carencia de material para esta carencia */
		<<fim_cursor_C01>>
		nr_seq_carencia_w	:= nr_seq_carencia_w;
	end loop;
	close c01;
end if;

select	count(1)
into	qt_regra_carencia_w
from	pls_regra_lanc_carencia
where	rownum = 1;

if	(qt_regra_carencia_w > 0) then
	if 	(ie_tipo_consistencia_w in ('C','CR')) then
		select	count(1)
		into	qt_glosa_carencia_w
		from	pls_conta_glosa a,
			tiss_motivo_glosa b
		where	a.nr_seq_motivo_glosa = b.nr_sequencia
		and	b.cd_motivo_tiss = '1410'
		and	a.nr_seq_conta_mat = nr_seq_conta_mat_p
		and	rownum <= 1;
		
		if	(qt_glosa_carencia_w > 0) then
			pls_obter_regra_carencia(	ie_carater_solic_w, nr_seq_plano_w, nm_usuario_p, 
							cd_estabelecimento_p, nr_seq_regra_carencia_w);
			
			if	(nr_seq_regra_carencia_w > 0) then
				update	pls_conta_glosa x
				set	x.ie_situacao 	= 'I',
					x.ds_observacao = wheb_mensagem_pck.get_texto(1108798, 'NR_SEQ_REGRA_CARENCIA='||nr_seq_regra_carencia_w)
				where	x.nr_sequencia in (	select	a.nr_sequencia
								from	pls_conta_glosa		a,
									tiss_motivo_glosa	b
								where	a.nr_seq_motivo_glosa 	= b.nr_sequencia
								and	b.cd_motivo_tiss	= '1410'
								and	a.nr_seq_conta_mat	= nr_seq_conta_mat_p
								and	rownum			<= 1 );
				
				update	pls_ocorrencia_benef x
				set	x.ie_situacao 	= 'I',
					x.ds_observacao = wheb_mensagem_pck.get_texto(1108797, 'NR_SEQ_REGRA_CARENCIA='||nr_seq_regra_carencia_w)
				where	x.nr_seq_glosa in(	select	a.nr_sequencia
								from	pls_conta_glosa		a,
									tiss_motivo_glosa	b
								where	a.nr_seq_motivo_glosa 	= b.nr_sequencia
								and	b.cd_motivo_tiss	= '1410'
								and	a.nr_seq_conta_mat	= nr_seq_conta_mat_p
								and	rownum			<= 1 );
			end if;
		end if;
	end if;
end if;

-- nao utilizar commit nesta rotina

end pls_consistir_carencia_mat;
/
