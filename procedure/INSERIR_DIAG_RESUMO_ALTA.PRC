create or replace
procedure Inserir_Diag_Resumo_alta(		NR_SEQ_ATEND_SUMARIO_p	number,
										cd_doenca_p			Varchar2,
										nm_usuario_p		Varchar2) is 

begin


insert into ATEND_SUMARIO_ALTA_ITEM (
			 NR_SEQUENCIA,
			 CD_DOENCA,
			 DT_ATUALIZACAO,
			 NM_USUARIO,
			 DT_ATUALIZACAO_NREC,
			 NM_USUARIO_NREC,
			 NR_SEQ_EXAME,
			 IE_TIPO_ITEM,
			 CD_RECOMENDACAO,
			 CD_PROCEDIMENTO,
			 IE_ORIGEM_PROCED,
			 NR_SEQ_ATEND_SUMARIO)
		values (
			 ATEND_SUMARIO_ALTA_ITEM_seq.nextval,
			 cd_doenca_p,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 null,
			 'D',
			 null,
			 null,
			 null,
			 NR_SEQ_ATEND_SUMARIO_p);



commit;

end Inserir_Diag_Resumo_alta;
/