CREATE OR REPLACE
PROCEDURE Enviar_CI_Prescr_Nao_padrao
				(nr_prescricao_p	Number,
				cd_estabelecimento_p	number,
				ds_comunicado_p		varchar2,
				cd_perfil_p		number,
				nm_usuario_p		Varchar2) is

ds_titulo_w			varchar2(255);
nm_paciente_w			varchar2(255);
nm_medico_w			varchar2(255);
nm_usuario_destino_w		varchar2(255);
ds_comunicado_w			varchar2(4000);
ds_lista_usuario_destino_w	varchar2(4000);
ds_comunic_w			varchar2(4000);
ds_material_w			varchar2(4000);
ds_justificativa_w		varchar2(4000);
ds_comunic_final_w		varchar2(4000);
ie_somente_padrao_w		varchar2(1);
ie_primeiro_dia_w		varchar2(1);
cd_mat_w			number(15,0);
cd_perfil_w			varchar2(4000);
ds_setor_w			varchar2(1000);
ie_enviar_atb_w			varchar2(1);
ie_medicacao_paciente_w		varchar2(40);
ds_dose_w			varchar2(400);
ie_classif_custo_w		varchar2(4);
ds_convenio_w			varchar2(1000);
cd_perfil_destino_w		number(15,0);
nr_sequencia_w			number(15,0);  
nr_sequencia_ww			number(15,0);
cd_setor_w			number(15,0);
ie_ctrl_medic_w			number(15);
nr_seq_medic_w			number(15);
cd_material_w			number(15,0);
nr_atendimento_w		number(15,0);
cd_grupo_material_w		number(08,0);
cd_subgrupo_material_w		number(08,0);
nr_min_prescr_w			number(15,0);
cd_classe_material_w		number(08,0);
ie_enviar_prescritor_w		varchar2(1);
nr_seq_comunic_w		number(15,0);
ie_objetivo_w			varchar2(60);
qt_dias_solicitado_w		number(3,0);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
qt_dias_solic_w			number(3,0);
qt_dias_liberado_w		number(3,0);
ds_objetivo_w			varchar2(50);
ds_microorganismo_w		varchar2(255);
ds_amostra_w			varchar2(50);
ds_origem_infeccao_w		varchar2(50);
ds_topografia_w			varchar2(50);
ds_indicacao_w			varchar2(50);
ds_uso_atb_w			varchar2(50);
ds_horarios_w			varchar2(2000);
ie_dias_util_medic_w	varchar2(2);
ie_gera_comunicado_w	varchar2(2) := 'S';
ie_agrupador_w			prescr_material.ie_agrupador%type;
qt_dias_antib_w			parametro_medico.qt_dias_antibiotico%type;
nr_dia_util_w			prescr_material.nr_dia_util%type;

cursor c01 is
select	a.nr_sequencia,
	a.ds_titulo,
	a.ds_comunicado,
	a.ie_somente_padrao,
	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material,
	a.cd_material,
	nvl(a.ie_enviar_prescritor,'S'),
	a.ie_primeiro_dia,
	nvl(a.ie_dados_atb,'S'),
	ie_classif_custo
from	rep_regra_envio_ci_padrao a
where	obter_se_setor_lib_ci(a.nr_sequencia, cd_setor_w) = 'S'
and	((cd_estab is null) or (cd_estabelecimento_p = cd_estab))
and	nvl(a.ie_paciente_medic,'N') = 'N'
and	nvl(ie_momento,'L') = 'L'
and	(nvl(a.cd_convenio,nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0))
and	(nvl(a.cd_categoria,nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0'));

cursor c02 is
select	b.cd_perfil
from	rep_regra_envio_ci_perfil b
where	b.nr_seq_regra	= nr_sequencia_ww;

cursor c14 is
SELECT	ds_justificativa,
	substr(obter_descricao_mat_prescr(c.cd_material),1,80),
	a.cd_material,
	(c.qt_dose || c.cd_unidade_medida_dose),
	c.ie_medicacao_paciente,
	obter_ctrl_antimicrobiano_mat(c.cd_material),
	c.nr_sequencia,
	nvl(c.qt_dias_solicitado,0),
	nvl(c.qt_dias_liberado,0),
	substr(obter_valor_dominio(1280, c.ie_objetivo),1,50) ds_objetivo,
	substr(obter_desc_microorganismo(c.cd_microorganismo_cih),1,255) ds_microorganismo,
	substr(obter_cih_amostra(c.cd_amostra_cih),1,50) ds_amostra,
	substr(obter_valor_dominio(1288, c.ie_origem_infeccao),1,50) ds_origem_infeccao,
	substr(obter_desc_topografia(c.cd_topografia_cih),1,50) ds_topografia,
	substr(obter_valor_dominio(1927, c.ie_indicacao),1,50) ds_indicacao,
	decode(nvl(ie_uso_antimicrobiano,'N'), 'S', OBTER_DESC_EXPRESSAO(327113), OBTER_DESC_EXPRESSAO(327114)) ds_uso_atb, -- 327113:'Sim';	327114:'N�o'
	c.ds_horarios
from	Estrutura_material_v a,
	prescr_material c,
	material b
where	a.cd_material		= c.cd_material
and	c.nr_prescricao		= nr_prescricao_p
and	b.cd_material = c.cd_material
and	c.ie_agrupador in (1,2,4)
and	c.nr_seq_kit		is null
and	c.nr_sequencia_diluicao	is null 
and	((ie_classif_custo_w is null) or (ie_classif_custo_w = obter_dados_material_estab(c.cd_material, cd_estabelecimento_p, 'CU')))
and	((cd_mat_w is null) or
	 (a.cd_material	= cd_mat_w))
and	((cd_grupo_material_w is null) or
	 (a.cd_grupo_material	= cd_grupo_material_w))
and	((cd_subgrupo_material_w is null) or
	 (a.cd_subgrupo_material = cd_subgrupo_material_w))
and	((cd_classe_material_w is null) or
	 (a.cd_classe_material = cd_classe_material_w))
and	(((nvl(ie_somente_padrao_w,'S') = 'N') and (substr(obter_se_material_padronizado(cd_estabelecimento_p, a.cd_material),1,1) = 'S')) or
	 (substr(obter_se_material_padronizado(cd_estabelecimento_p, a.cd_material),1,1) = 'N'))
and	((c.ie_agrupador in(2,4)) or
		 ((ie_primeiro_dia_w = 'N') or 
		  (((b.IE_DIAS_UTIL_MEDIC = 'O') and
			 (c.nr_dia_util = 0)) or 
		   ((b.IE_DIAS_UTIL_MEDIC = 'S') and
			 (c.nr_dia_util = 1)) or 
          (b.IE_DIAS_UTIL_MEDIC= 'N'))));		
	 
cursor	c15 is
select	b.nm_usuario_regra
from	rep_regra_envio_ci_padrao a,
	rep_usuario_ci_medic b
where	a.nr_sequencia	= b.nr_seq_regra
and	b.nr_seq_regra	= nr_sequencia_ww;
--and	obter_se_envia_ci_setor(a.nr_sequencia, cd_setor_w) = 'S';

BEGIN

select	min(nr_sequencia) 
into	nr_sequencia_w
from	comunic_interna_classif 
where	ie_tipo	= 'F';

select	max(obter_nome_pf_pj(cd_pessoa_fisica,null)),
	max(obter_nome_pf_pj(cd_prescritor,null)),
	max(cd_setor_atendimento),
	max(nr_atendimento),
	max(obter_nome_setor(cd_setor_atendimento)),
	max(obter_nome_convenio(obter_Convenio_atendimento(nr_atendimento)))
into	nm_paciente_w,
	nm_medico_w,
	cd_setor_w,
	nr_atendimento_w,
	ds_setor_w,
	ds_convenio_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	max(cd_convenio),
	max(cd_categoria)
into	cd_convenio_w,
	cd_categoria_w
from	atend_categoria_convenio
where	nr_atendimento	= nr_atendimento_w;

select nvl(max(qt_dias_antibiotico),1)
into	qt_dias_antib_w
from parametro_medico
where cd_estabelecimento = cd_estabelecimento_p;

open C01;
loop
fetch C01 into	
	nr_sequencia_ww,
	ds_titulo_w,
	ds_comunic_w,
	ie_somente_padrao_w,
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	cd_mat_w,
	ie_enviar_prescritor_w,
	ie_primeiro_dia_w,
	ie_enviar_atb_w,
	ie_classif_custo_w;
exit when C01%notfound;
	ds_comunicado_w := null;
	open C14;
	loop
	fetch C14 into	
		ds_justificativa_w,
		ds_material_w,
		cd_material_w,
		ds_dose_w,
		ie_medicacao_paciente_w,
		ie_ctrl_medic_w,
		nr_seq_medic_w,
		qt_dias_solic_w,
		qt_dias_liberado_w,
		ds_objetivo_w,
		ds_microorganismo_w,
		ds_amostra_w,
		ds_origem_infeccao_w,
		ds_topografia_w,
		ds_indicacao_w,
		ds_uso_atb_w,
		ds_horarios_w;
	exit when C14%notfound; 		
		
		if	(ds_comunicado_w is not null) then
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || chr(13) || ds_material_w  || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(326100) || ' ' || ds_dose_w,1,2000); 	--326100: 'Dose:'
		else
			ds_comunicado_w	:= substr(chr(13) || ds_material_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(326100) || ' ' || ds_dose_w,1,2000); 		--326100: 'Dose:'
		end if;
		if	(ds_horarios_w is not null) then
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(727994) || ' '|| ds_horarios_w,1,2000); 				--727994: 'Hor�rios prescritos:'
		end if;
		if	(qt_dias_solic_w > 0) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(727998) || ' ' || qt_dias_solic_w,1,2000);			--727998: 'Dias previstos para utiliza��o:'
		end if;
		if	(qt_dias_liberado_w > 0) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(343379) || ' ' || qt_dias_liberado_w,1,2000);		--343379: 'Liberado:'
		end if;
		if	(ds_objetivo_w is not null) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(728000) || ' ' || ds_objetivo_w,1,2000);				--728000: 'Objetivo do uso:'
		end if;
		if	(ds_microorganismo_w is not null) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(728004) || ' ' || ds_microorganismo_w,1,2000);		--728004: 'Micro-organismo:'
		end if;
		if	(ds_amostra_w is not null) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(728006) || ' ' || ds_amostra_w,1,2000);				--728006: 'Amostra(Material):'
		end if;
		if	(ds_origem_infeccao_w is not null) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(294936) || ': ' || ds_origem_infeccao_w,1,2000);		--294936: 'Origem da infec��o'
		end if;
		if	(ds_topografia_w is not null) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(300156) || ': ' || ds_topografia_w,1,2000);			--300156: 'Topografia'
		end if;
		if	(ds_indicacao_w is not null) then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(291834) || ': ' || ds_indicacao_w,1,2000);			--291834: 'Indica��o'
		end if;
		if	(ds_uso_atb_w is not null) and
			(ie_enviar_atb_w = 'S') then		
			ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || '   ' || OBTER_DESC_EXPRESSAO(300885) || ': ' || ds_uso_atb_w,1,2000);				--300885: 'Uso anterior antimicrobiano'
		end if;
		
		if	(ie_ctrl_medic_w > 0) then
			select	substr(obter_valor_dominio(1280,ie_objetivo),1,60),
				nvl(qt_dias_solicitado,0)qt_dias_solicitado
			into	ie_objetivo_w,
				qt_dias_solicitado_w
			from	prescr_material
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_seq_medic_w;
			
			ds_comunicado_w := substr(ds_comunicado_w || chr(13) || 
						'  ' || OBTER_DESC_EXPRESSAO(727882) || ':  ' || nvl(ds_justificativa_w,'') || ' (' || ie_objetivo_w || ')' || chr(13) || 		--727882: 'Justificativa de uso'
						'  ' || OBTER_DESC_EXPRESSAO(303740) || ':  ' || qt_dias_solicitado_w,1,2000);													--303740: 'Dias previstos'
		elsif	(ds_justificativa_w is not null) then
			ds_comunicado_w := substr(ds_comunicado_w || chr(13) || ds_justificativa_w,1,2000);
		end if;	
		
		if	(ie_medicacao_paciente_w = 'S') then
			ds_comunicado_w := substr(ds_comunicado_w || chr(13) || OBTER_DESC_EXPRESSAO(339240),1,2000);												--339240: 'Medica��o do paciente'
		end if;
	
	end loop;
	close C14;
	
	cd_perfil_w := null;
	open c02;
	loop
	fetch c02 into
		cd_perfil_destino_w;
	exit when c02%notfound;
		if	(cd_perfil_w is not null) or
			(cd_perfil_w <> '') then
			cd_perfil_w := cd_perfil_w || ',';
		end if;
		
		cd_perfil_w := substr(cd_perfil_w || cd_perfil_destino_w,1,1000);	
		
	end loop;
	close c02;
	
	if	(cd_perfil_w is not null) and
		(substr(cd_perfil_w,length(cd_perfil_w), 1) <> ',') then
		cd_perfil_w	:= cd_perfil_w || ',';
	end if;
	
	if	(ie_primeiro_dia_w = 'S') then
	
		select	min(a.nr_prescricao)
		into	nr_min_prescr_w
		from	prescr_medica b,
				prescr_material a,
				material c
		where	a.nr_prescricao		= b.nr_prescricao
		and		b.nr_atendimento	= nr_atendimento_w
		and		a.cd_material		= c.cd_material
		and		a.cd_material		= cd_material_w
		and		a.dt_suspensao is null
		and		b.dt_suspensao is null
		and		nvl(c.ie_dias_util_medic, 'N') = 'N'
		and 	a.ie_agrupador = 1
		and 	b.dt_inicio_prescr > (sysdate - 6);

		if	(nr_min_prescr_w < nr_prescricao_p) then
			ds_comunicado_w	:= '';
		end if;
	end if;
	
	if	(ds_comunicado_w is not null) then	
		ds_lista_usuario_destino_w := null;
		open C15;
		loop
		fetch C15 into
			nm_usuario_destino_w;
		exit when C15%notfound;
			if	(ds_lista_usuario_destino_w is not null) then
				ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
			end if;
			
			ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_destino_w,1,1000);	
		end loop;
		close C15;
		
		/*ds_comunic_w	:=	substr('Prescri��o: ' || nr_prescricao_p || chr(13) ||
					'Atendimento: ' || nr_atendimento_w || chr(13) ||
					'Paciente: ' || nm_paciente_w || chr(13) ||
					'Setor: ' || ds_setor_w|| chr(13) ||
					'Conv�nio: ' || ds_convenio_w|| chr(13) ||
					'Prescritor: ' || nm_medico_w || chr(13) || chr(13) ||
					ds_comunic_w,1,2000);*/
		ds_comunic_w	:=	substr(WHEB_MENSAGEM_PCK.get_texto(456898,
					'nr_prescricao_p='|| nr_prescricao_p ||
					';nr_atendimento_w='|| nr_atendimento_w ||
					';nm_paciente_w='|| nm_paciente_w ||
					';ds_setor_w='|| ds_setor_w ||
					';ds_convenio_w='|| ds_convenio_w ||
					';nm_medico_w='|| nm_medico_w ||
					';ds_comunic_w='|| ds_comunic_w),1,2000);
		
		if	(ds_titulo_w is not null) then
		
			select	comunic_interna_seq.nextval
			into	nr_seq_comunic_w
			from	dual;
			
			ds_comunic_final_w	:= substr(ds_comunic_w || chr(13) || ds_comunicado_w,1,2000);
			
			insert	into comunic_interna(	
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				ie_gerencial,
				ds_perfil_adicional,
				nr_sequencia,
				nr_seq_classif,
				dt_liberacao,
				nm_usuario_destino)
			values(
				sysdate,
				ds_titulo_w,
				ds_comunic_final_w, 
				nm_usuario_p,
				sysdate,
				'N',
				'N',
				cd_perfil_w,
				nr_seq_comunic_w,
				nr_sequencia_w,
				sysdate,
				ds_lista_usuario_destino_w);
				
			if	(ie_enviar_prescritor_w = 'N') then
				insert into comunic_interna_lida 
				values(	nr_seq_comunic_w,
					nm_usuario_p,
					sysdate);
			end if;
			 
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;	
			
		end if;
	end if;
	
end loop;
close C01;

END Enviar_CI_Prescr_Nao_padrao;
/