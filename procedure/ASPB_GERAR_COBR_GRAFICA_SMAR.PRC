create or replace
procedure aspb_gerar_cobr_grafica_smar
			(	nr_seq_cobr_escrit_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

ds_conteudo_w			varchar2(400);
nr_documento_tit_w		varchar2(16);
ds_produto_w			varchar2(83);

nm_beneficiario_w		varchar2(33);
dt_inclusao_w			varchar2(10);
ds_faixa_etaria_w		varchar2(15);
dt_mes_reajuste_w		varchar2(15);
vl_mensalidade_w		number(12,2);

nm_paciente_w			varchar2(20);
dt_execucao_w			varchar2(10);
nm_prestador_w			varchar2(30);
ds_servico_w			varchar2(20);
vl_referencia_w			number(12);

ds_endereco_w			varchar2(80);
ds_bairro_w			varchar2(50);
ds_cep_w			varchar2(9);
ds_cidade_w			varchar2(60);

dt_vencimento_w			varchar2(10);
ds_cedente_w			varchar2(80);
ds_agencia_w			varchar2(20);
dt_emissao_w			varchar2(10);
nr_documento_w			varchar2(10);
ie_especie_documento_w		varchar2(10);
ie_aceite_w			varchar2(1);
dt_processamento_w		varchar2(10);
nr_nosso_numero_w		varchar2(20);
uso_banco_w			varchar2(10);
cd_carteira_w			varchar2(2);
ie_especie_w			varchar2(2);
nr_quanditade_w			varchar2(9);
valor_w				varchar2(12);
vl_documento_w			varchar2(12);		

nr_sequencia_w			number(10);
instrucoes_w			varchar(80);

mensagens_w			varchar(80);

nr_titulo_w			number(10);
nr_seq_registro_w		number(10) := 0;

Cursor C00 is
	select	b.nr_titulo
	from	titulo_receber		b,
		titulo_receber_cobr	a
	where	a.nr_titulo 		= b.nr_titulo
	and	a.nr_seq_cobranca 	= nr_seq_cobr_escrit_p;

Cursor C01 is
	select	lpad(substr(nvl(to_char(a.nr_titulo),'0'),1,16),16,'0') nr_documento,
		rpad(substr(nvl(pls_obter_nome_produto(e.nr_seq_plano), ' '),1,83),83,' ') ds_produto
	from	pls_contrato_pagador	c,
		pls_contrato_plano	e,
		pls_contrato		d,
		titulo_receber		b,
		titulo_receber_cobr	a
	where	a.nr_titulo 		= b.nr_titulo
	and	b.nr_seq_pagador 	= c.nr_sequencia
	and	c.nr_seq_contrato 	= d.nr_sequencia
	and	e.nr_seq_contrato 	= d.nr_sequencia
	and	b.nr_titulo		= nr_titulo_w
	and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;
				
Cursor C02 is
	select	lpad(substr(nvl(to_char(a.nr_titulo),'0'),1,16),16,'0') nr_documento,
		rpad(substr(nvl(pls_obter_dados_segurado(g.nr_seq_segurado,'N'), ' '),1,33),33,' ') nm_beneficiario,
		rpad(nvl(pls_obter_dados_segurado(g.nr_seq_segurado,'I'),' '), 10, ' ') dt_inclusao,
		lpad(substr(nvl(pls_obter_faixa_etaria_benef(c.nr_seq_contrato,'D'),' '),1,15),15,' ') faixa_etaria,
		decode((select max(h.nr_seq_reajuste) from pls_mensalidade_seg_item h where g.nr_sequencia = h.nr_seq_mensalidade_seg),null, rpad(' ',15, ' '),
			rpad(nvl(to_char(pls_obter_proximo_reajuste(c.nr_seq_contrato)),' '), 15, ' ')) mes_reajuste,
		to_number(substr(f.vl_mensalidade,1,12))
	from	pls_mensalidade_segurado	g,
		pls_mensalidade			f,
		pls_contrato_pagador		c,
		titulo_receber			b,
		titulo_receber_cobr		a
	where	a.nr_titulo 		= b.nr_titulo
	and	c.nr_sequencia		= b.nr_seq_pagador
	and	f.nr_sequencia		= b.nr_seq_mensalidade
	and	f.nr_sequencia		= g.nr_seq_mensalidade
	and	c.nr_sequencia		= f.nr_seq_pagador
	and	b.nr_titulo		= nr_titulo_w
	and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

Cursor C03 is
	select	lpad(substr(nvl(to_char(a.nr_titulo),'0'),1,16),16,'0') nr_documento,
		rpad(substr(nvl(pls_obter_dados_segurado(j.nr_seq_segurado, 'N'), ' '),1,20),20,' ') nm_paciente,
		rpad(nvl(to_char(k.dt_procedimento,'dd/mm/yyyy'),' '),10,' '),
		rpad(substr(nvl(pls_obter_dados_prestador(cd_prestador_exec_imp, 'N'),' '),1,30),30,' ') nm_prestador,	
		rpad(substr(obter_descricao_procedimento(cd_procedimento,ie_origem_proced),1,20),20,' ') ds_servico,
		i.vl_item
	from	titulo_receber_cobr		a,
		titulo_receber			b,
		pls_mensalidade			f,
		pls_mensalidade_segurado	g,
		pls_mensalidade_seg_item	i,
		pls_conta			j,
		pls_conta_proc			k
	where	a.nr_titulo 		= b.nr_titulo
	and	f.nr_sequencia		= b.nr_seq_mensalidade
	and	f.nr_sequencia		= g.nr_seq_mensalidade
	and	g.nr_sequencia		= i.nr_seq_mensalidade_seg
	and	j.nr_sequencia		= i.nr_seq_conta
	and	j.nr_sequencia		= k.nr_seq_conta
	and	b.nr_titulo		= nr_titulo_w
	and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;
	
Cursor C04 is
	select	lpad(substr(nvl(to_char(a.nr_titulo),'0'),1,16),16,'0') nr_documento,
		rpad(substr(nvl(obter_dados_pf_pj(h.cd_pessoa_fisica, b.cd_cgc, 'E'), ' '),1,80),80,' ') ds_endereco,
		rpad(substr(nvl(obter_dados_pf_pj(h.cd_pessoa_fisica, b.cd_cgc, 'B'),' '),1,50),50,' ') ds_bairro,
		lpad(substr(nvl(obter_dados_pf_pj(h.cd_pessoa_fisica, b.cd_cgc, 'CEP'),' '),1,9),9,' ') ds_cep,
		rpad(substr(nvl(obter_dados_pf_pj(h.cd_pessoa_fisica, b.cd_cgc, 'CI') || '-' || 
		obter_dados_pf_pj(h.cd_pessoa_fisica, b.cd_cgc, 'UF'),' '),1,60),60,' ') ds_cidade_uf
	from	titulo_receber_cobr	a,
		titulo_receber		b,
		pls_contrato_pagador	c,
		pls_segurado		h
	where	a.nr_titulo 		= b.nr_titulo
	and	c.nr_sequencia		= b.nr_seq_pagador
	and	c.nr_sequencia		= h.nr_seq_pagador
	and	b.nr_titulo		= nr_titulo_w
	and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;
			
Cursor C05 is

	select	lpad(substr(nvl(to_char(a.nr_titulo),'0'),1,16),16,'0') nr_documento,
		rpad(nvl(to_char(b.dt_vencimento,'ddmmyyyy'),' '),10,' '),
		rpad(substr('ASSOCIA��O DE SA�DE PORTUGUESA DE BENEFIC�NCIA',1,80),80,' ') ds_cedente,
		'Ag.2913 C/c:141470-4' ds_agencia,
		rpad(nvl(to_char(b.dt_emissao,'ddmmyyyy'),' '),10,' '),
		lpad(substr(nvl(a.nr_titulo,'0'),1,10),10,'0') nr_titulo,
		rpad(' ', 8,' ') || 'RC' ie_especie_documento,
		'N' aceite,
		rpad(nvl(to_char(n.dt_remessa_retorno,'ddmmyyyy'),' '),10,' ') dt_processamento,
		substr(rpad(m.cd_banco,10,'0') || lpad(b.nr_titulo,10,'0'),1,20) nosso_numero,
		rpad(substr(nvl(obter_nome_banco(b.cd_banco),' '),1,10),10,' ') uso_banco,
		lpad(substr('17',1,2),2,'0') carteira,
		'R$' especie,
		'         ' quanditade,
		lpad(' ',12,' ') valor,
		lpad(somente_numero(to_char(nvl(b.vl_titulo,0),'99999999990.00')),12,'0') vl_documento
	from	titulo_receber_cobr	a,
		titulo_receber 		b,
		banco_estabelecimento 	m,
		cobranca_escritural 	n
	where 	a.nr_titulo 		= b.nr_titulo
	and	n.nr_sequencia		= a.nr_seq_cobranca
	and	m.nr_sequencia		= b.nr_seq_conta_banco
	and	b.nr_titulo		= nr_titulo_w
	and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

	
begin
delete w_envio_banco
where nm_usuario	= nm_usuario_p;

-- TITULOS
open C00;
loop
fetch C00 into	
	nr_titulo_w;
exit when C00%notfound;
	begin
	/* Produto */
	open C01;
	loop
	fetch C01 into	
		nr_documento_tit_w,
		ds_produto_w;
	exit when C01%notfound;
		begin
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 1 ' || ds_produto_w;
			
		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			nr_seq_apres,
			ds_conteudo)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			nr_seq_registro_w,
			ds_conteudo_w);		
		end;
	end loop;
	close C01;
	/* Fim Produto */
	
	/* Benefici�rio*/
	open C02;
	loop
	fetch C02 into	
		nr_documento_tit_w,
		nm_beneficiario_w,
		dt_inclusao_w,
		ds_faixa_etaria_w,
		dt_mes_reajuste_w,
		vl_mensalidade_w;
	exit when C02%notfound;
		begin
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 2 ' || nm_beneficiario_w || dt_inclusao_w || 
						ds_faixa_etaria_w || dt_mes_reajuste_w || lpad(nvl(vl_mensalidade_w,'0'),12,'0');
			
		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			nr_seq_apres,
			ds_conteudo)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			nr_seq_registro_w,
			ds_conteudo_w);		
		end;
	end loop;
	close C02;
	/* Fim Benefici�rio */	
	
	/* Co Participa��o*/
	open C03;
	loop
	fetch C03 into	
		nr_documento_tit_w,
		nm_paciente_w,
		dt_execucao_w,
		nm_prestador_w,
		ds_servico_w,
		vl_referencia_w;
	exit when C03%notfound;
		begin
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 3 ' || nm_paciente_w || dt_execucao_w || nm_prestador_w || 
					ds_servico_w || lpad(substr(nvl(vl_referencia_w,'0'),1,12),12,'0');

		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			nr_seq_apres,
			ds_conteudo)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			nr_seq_registro_w,
			ds_conteudo_w);		
		end;
	end loop;
	close C03;
	/*fim Co Participa��o*/
	
	/* Correspondencia*/
	open C04;
	loop
	fetch C04 into	
		nr_documento_tit_w,
		ds_endereco_w,
		ds_bairro_w,
		ds_cep_w,
		ds_cidade_w;
	exit when C04%notfound;
		begin
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 4 ' || ds_endereco_w || ds_bairro_w || ds_cep_w || 
						ds_cidade_w;

		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			nr_seq_apres,
			ds_conteudo)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			nr_seq_registro_w,
			ds_conteudo_w);			
		end;
	end loop;
	close C04;
	/* fim Correspondencia*/	
	
	/* Boleto bancario*/
	open C05;
	loop
	fetch C05 into	
		nr_documento_tit_w,
		dt_vencimento_w,
		ds_cedente_w,
		ds_agencia_w,
		dt_emissao_w,
		nr_documento_w,
		ie_especie_documento_w,
		ie_aceite_w,
		dt_processamento_w,
		nr_nosso_numero_w,
		uso_banco_w,
		cd_carteira_w,
		ie_especie_w,
		nr_quanditade_w,
		valor_w,
		vl_documento_w;
	exit when C05%notfound;
		begin
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 5 ' || dt_vencimento_w || ds_cedente_w || ds_agencia_w || 
						dt_emissao_w || nr_documento_w || ie_especie_documento_w || ie_aceite_w || 
						dt_processamento_w || nr_nosso_numero_w || uso_banco_w || cd_carteira_w || ie_especie_w || 
						nr_quanditade_w || valor_w || vl_documento_w;

		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			nr_seq_apres,
			ds_conteudo)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			nr_seq_registro_w,
			ds_conteudo_w);		
		end;
	end loop;
	close C05;
	/* fim Boleto bancario*/	
	
	-- 6.1
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 6 ' || lpad(nvl(substr(to_char(nr_seq_registro_w), 1, 3),'0'),3,'0') || 
					lpad('Ap�s vencimento cobrar Multa de 2% + Juros de 0,05% ao dia', 80, ' ');

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		nr_seq_apres,
		ds_conteudo)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		nr_seq_registro_w,
		ds_conteudo_w);	

	--6.2	
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	ds_conteudo_w		:= lpad(nvl(nr_documento_tit_w,'0'), 16, '0') || ' 6 ' || lpad(nvl(substr(to_char(nr_seq_registro_w), 1, 3),'0'),3,'0') || 
					lpad('N�o receber ap�s 15 dias do vencimento', 80, ' ');
	
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		nr_seq_apres,
		ds_conteudo)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		nr_seq_registro_w,
		ds_conteudo_w);			
	end;
end loop;
close C00;	

commit;

end aspb_gerar_cobr_grafica_smar;
/