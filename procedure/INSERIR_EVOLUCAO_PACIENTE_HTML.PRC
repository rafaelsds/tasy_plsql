create or replace
procedure inserir_evolucao_paciente_html(	nr_atendimento_p		number,
				ie_tipo_evolucao_p		varchar2,
				cd_pessoa_fisica_p		varchar2,
				nm_usuario_p			varchar2,
				cd_medico_p				varchar2,
				ie_evolucao_clinica_p	varchar2) is 

cd_evolucao_w			number(10);
ie_nivel_atencao_w		varchar2(1);
ie_evolucao_clinica_w   varchar2(255);	

begin


If (cd_pessoa_fisica_p is not null and ie_tipo_evolucao_p is not null) then

	select	evolucao_paciente_seq.nextval
	into	cd_evolucao_w
	from	dual;

	Select	wheb_assist_pck.get_nivel_atencao_perfil,
			Obter_Valor_Param_Usuario(281, 693, Obter_Perfil_Ativo, nm_usuario_p, obter_estabelecimento_ativo)
	into	ie_nivel_atencao_w,
			ie_evolucao_clinica_w
	from 	dual;	
	
	insert into evolucao_paciente (	nr_atendimento,
					cd_evolucao,
					dt_evolucao,
					ie_tipo_evolucao,
					cd_pessoa_fisica,
					nm_usuario,
					cd_medico,
					dt_liberacao,
					ie_evolucao_clinica,
					ie_situacao,
					dt_atualizacao,
					ie_nivel_atencao)
				values (	nr_atendimento_p,
					cd_evolucao_w,
					sysdate,
					ie_tipo_evolucao_p,
					cd_pessoa_fisica_p,
					nm_usuario_p,
					cd_medico_p,
					sysdate,
					nvl(ie_evolucao_clinica_w,'E'),
					'A',
					sysdate,
					ie_nivel_atencao_w);
	
	COPIA_CAMPO_LONG_DE_PARA_NOVO('W_EVOLUCAO_GRUPO_FAMILIAR',
					'DS_EVOLUCAO',
					'where nm_usuario = :nm_usuario_p',
					'nm_usuario_p='||nm_usuario_p,
					'EVOLUCAO_PACIENTE',
					'DS_EVOLUCAO', 
					'where cd_evolucao = :cd_evolucao',
					'cd_evolucao='||cd_evolucao_w,
					'L');			
					
	
	commit;
	
end if;
	
end inserir_evolucao_paciente_html;
/