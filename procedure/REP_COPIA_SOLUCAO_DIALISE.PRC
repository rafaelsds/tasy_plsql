create or replace
procedure rep_copia_solucao_dialise(	
					nr_prescricao_orig_p		number,
					nr_prescricao_p				number,
					nr_seq_regra_p				number,
					ie_copia_agora_p			varchar2,
					dt_prescricao_p				date,
					nm_usuario_p				Varchar2,
					nr_seq_dialise_p			number,
					nr_seq_material_p			number,
					nr_seq_dialise_origem_p 	number,
					ie_modificar_p				varchar2) is 

ie_copia_pca_w						varchar2(1);
ie_copia_se_necessario_w			varchar2(1);
ie_copia_acm_w						varchar2(1);
ie_copia_esquema_alternado_w		varchar2(1);
ie_copiar_w							varchar2(20);
cd_estabelecimento_w				number(4,0);
nr_seq_material_w					number(15,0);
QT_TEMSOL_W							number(15,4);
qt_itens_w							number(15,0);
nr_seq_solucao_filtro_w				number(15,0);
ie_via_aplicacao_w					varchar2(5);
ie_tipo_dosagem_w					varchar2(3);
ie_recalcular_hor_sol_w				varchar2(3);
ie_acm_w							varchar2(1);
ie_loop_w							varchar2(1);
ie_esquema_alternado_w				varchar2(1);
ie_calc_aut_w						varchar2(1);
VarCalcularEtapaSolucao_w			varchar2(10);
VarCalculaVolumeVelocidade_w		varchar2(10);
qt_volume_componentes_ml_w			number(15,3);
VarCalculaTempoSolucao_w			varchar2(10);
ie_hemodialise_w					varchar2(1);
qt_velocidade_infusao_ml_w			number(15,4);
ds_erro_w							varchar2(5000);
ie_tipo_solucao_w					varchar2(15);
ds_horarios2_w						varchar2(255);
ie_pos_dialisador_w					varchar2(1);
ie_agora_w							varchar2(1);
ie_arredondar_etapa_w				varchar2(1);
ie_unid_vel_inf_w					varchar2(3);
VarHorarioSolucao_w					varchar2(3);
ie_etapa_especial_w					varchar2(3);
ie_apap_w							varchar2(15);
ie_solucao_pca_w					varchar2(1);
ie_tipo_analgesia_w					varchar2(15);
ie_pca_modo_prog_w					varchar2(15);
ie_se_necessario_w					varchar2(1);
cd_intervalo_w						varchar2(7);
cd_unidade_medida_w					varchar2(30);
ds_solucao_w						varchar2(100);
ds_horarios_w						varchar2(2000);
ds_orientacao_w						varchar2(255);
qt_dosagem_w						number(15,4);
qt_solucao_total_w					number(15,4);
qt_tempo_aplicacao_w	 			number(15,4);
qt_volume_w							number(15,4);
qt_hora_fase_w						number(15,4);
NR_AGRUP_ACUM_W						number(10);
qt_temp_solucao_w					number(5,1);
qt_dose_ataque_w					number(15,2);
qt_vol_infusao_pca_w				number(15,1);
qt_bolus_pca_w						number(10,1);
qt_intervalo_bloqueio_w				number(10);
qt_limite_quatro_hora_w				number(10,1);
qt_dose_inicial_pca_w				number(10,1);
nr_etapas_w							number(3);
nr_agrupamento_w					number(15);
nr_seq_protocolo_w					number(10);
nr_seq_mat_w						number(10);
nr_seq_solucao_w					number(10,0);
qt_volsolu_w						number(15,4);
hr_prim_horario_w					varchar2(5);
dt_prescricao_www					date;
dt_inicio_prescr_w					date;
ie_prim_horario_setor_w				varchar2(10);
hr_setor_w							varchar2(10);
cd_setor_atendimento_w				number(10);
nr_atendimento_w					number(15,0);
nr_sequencia_w						number(15,0);
ie_origem_inf_w						varchar2(1);
cd_material_w						number(15,0);
qt_dose_w							number(18,6);
qt_unitaria_w						number(18,6);
qt_material_w						number(18,6);
ds_observacao_w						varchar2(4000);
ds_observacao_enf_w					varchar2(2000);
ie_cobra_paciente_w					varchar2(1);
cd_motivo_baixa_w					number(10,0);
dt_baixa_w							date;
ie_utiliza_kit_w					varchar2(5);
cd_unidade_medida_dose_w		 	varchar2(50);
qt_conversao_dose_w					number(18,6);
nr_ocorrencia_w						number(18,6);
qt_total_dispensar_w				number(18,6);
cd_fornec_consignado_w				varchar2(255);
nr_sequencia_proc_w					number(10,0);
qt_solucao_w						number(18,6);
ds_dose_diferenciada_w				varchar2(255);
ie_medicacao_paciente_w				varchar2(2);
nr_sequencia_diluicao_w				number(10,0);
nr_sequencia_dieta_w				number(10,0);
ie_agrupador_w						number(2,0);
nr_dia_util_w						number(10,0);
ie_situacao_w						varchar2(5);
qt_min_aplicacao_w					number(10,0);
ie_bomba_infusao_w					varchar2(5);
IE_APLIC_BOLUS_w					varchar2(5);
IE_APLIC_LENTA_w					varchar2(5);
ie_objetivo_w						varchar2(5);
cd_topografia_cih_w					number(10,0);
ie_origem_infeccao_w				varchar2(5);
cd_amostra_cih_w					number(10,0);
cd_microorganismo_cih_w				number(10,0);
nr_horas_validade_w					number(10,0);
ie_uso_antimicrobiano_w				varchar2(5);
cd_protocolo_w						number(10,0);
nr_seq_mat_protocolo_w				number(10,0);
qt_hora_aplicacao_w					number(10,0);
QT_VEL_INFUSAO_w					number(15,4);
ds_justificativa_w					varchar2(2000);
ie_sem_aprazamento_w				varchar2(5);
ie_indicacao_w						varchar2(5);
dt_proxima_dose_w					date;
qt_total_dias_lib_w					number(5,0);
nr_seq_substituto_w					number(6,0);
ie_lado_w							varchar2(5);
dt_inicio_medic_w					date;
qt_dia_prim_hor_w					number(10,0);
ie_regra_disp_w						varchar2(5);
qt_vol_adic_reconst_w				number(15,4);
qt_hora_intervalo_w					number(2,0);
qt_min_intervalo_w					number(5,0);
dt_primeiro_horario_w				date;
nr_seq_anterior_w					number(15);	
ie_copia_valid_igual_w				varchar2(1);
dt_validade_origem_w				date;
dt_validade_nova_w					date;
nr_seq_disp_w						number(10);
ie_recalc_total_w					varchar2(5);
hr_inicio_capd_w					varchar2(5);
hr_fim_capd_w						varchar2(5);
ie_lavar_linhas_w					varchar2(5);
ie_tidal_w							varchar2(5);
ie_ultima_bolsa_w					varchar2(5);
pr_concentracao_w					number(8,3);
qt_hora_permanencia_w				number(5);
qt_tempo_permanencia_w				number(5);
nr_seq_solucao_orig_w				number(10);
cd_funcao_w 						number(10);
ie_categoria_w						varchar2(1);

cursor C01 is
select	nr_seq_solucao,
		nr_seq_solucao,
		ie_via_aplicacao,
		cd_intervalo,
		cd_unidade_medida,
		ie_tipo_dosagem,
		qt_dosagem,
		qt_solucao_total,
		qt_tempo_aplicacao,
		ds_solucao,
		decode(nvl(ie_acm,'N'),'S',qt_volume,dividir(qt_solucao_total,nr_etapas)),
		nr_etapas,
		ds_horarios,
		ie_bomba_infusao,
		nr_agrupamento,
		ie_esquema_alternado,
		nvl(ie_calc_aut,'N'),
		nvl(ie_acm,'N'),
		hr_prim_horario,
		qt_hora_fase,
		ds_orientacao,
		ie_hemodialise,
		ie_tipo_solucao,
		ie_pos_dialisador,
		nr_seq_protocolo,
		ie_unid_vel_inf,
		qt_temp_solucao,
		ie_apap,
		qt_dose_ataque,
		ie_solucao_pca,
		ie_tipo_analgesia,
		ie_pca_modo_prog,
		qt_vol_infusao_pca,
		qt_bolus_pca,
		qt_intervalo_bloqueio,
		qt_limite_quatro_hora,
		qt_dose_inicial_pca,
		nvl(ie_se_necessario,'N'),
		ie_urgencia,
		ie_etapa_especial,
		hr_inicio_capd,
		hr_fim_capd,
		ie_lavar_linhas,
		ie_tidal,
		ie_ultima_bolsa,
		pr_concentracao,
		qt_hora_permanencia,
		qt_tempo_permanencia
from	prescr_solucao
where	nr_prescricao = nr_prescricao_orig_p
and		nr_seq_dialise = nr_seq_dialise_origem_p
and		nvl(ie_hemodialise,'N') in('S','P')
and		((ie_copia_agora_p = 'S') or 
		 (ie_urgencia = 'N'));
	
cursor C02 is
select	nvl(ie_copiar,'N')
from	rep_regra_copia_crit
where	nr_seq_regra	= nr_seq_regra_p
and		ie_tipo_item	= 'SOL'
and		((nvl(ie_pca, 'S') = 'S') or (nvl(ie_solucao_pca_w, 'N') =  nvl(ie_pca, 'S')))
and		((nvl(ie_se_necessario, 'S') = 'S') or (nvl(ie_se_necessario_w, 'N') =  nvl(ie_se_necessario, 'S')))
and		((nvl(ie_acm, 'S') = 'S') or (nvl(ie_acm_w, 'N') =  nvl(ie_acm, 'S')))
and		((nvl(ie_agora, 'S') = 'S') or (nvl(ie_agora_w, 'N') =  nvl(ie_agora, 'S')))
and		((nvl(ie_esquema_alternado, 'S') = 'S') or (nvl(ie_esquema_alternado_w, 'N') =  nvl(ie_esquema_alternado, 'S')))
order by 
		nvl(nr_seq_apres, 99);

begin

select	nvl(max(cd_estabelecimento),1),
		max(cd_setor_atendimento),
		max(nr_atendimento),
		max(nr_horas_validade),
		max(dt_inicio_prescr),
		max(cd_funcao_origem)
into	cd_estabelecimento_w,
		cd_setor_atendimento_w,
		nr_atendimento_w,
		nr_horas_validade_w,
		dt_primeiro_horario_w,
		cd_funcao_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;
	
select	max(ie_categoria)
into	ie_categoria_w
from	hd_prescricao
where	nr_prescricao = nr_prescricao_p;

obter_param_usuario(8030, 64, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_recalcular_hor_sol_w);
obter_param_usuario(924, 322, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_recalc_total_w);
obter_param_usuario(924, 742, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_arredondar_etapa_w);
obter_param_usuario(924, 461, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, varcalcularetapasolucao_w);
obter_param_usuario(924, 636, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, varcalculavolumevelocidade_w);
obter_param_usuario(924, 394, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, varcalculatemposolucao_w);
obter_param_usuario(924, 284, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, varhorariosolucao_w);

open C01;
loop
fetch C01 into	
	nr_seq_solucao_w,
	nr_seq_solucao_filtro_w,
	ie_via_aplicacao_w,
	cd_intervalo_w,
	cd_unidade_medida_w,
	ie_tipo_dosagem_w,
	qt_dosagem_w,
	qt_solucao_total_w,
	qt_tempo_aplicacao_w,
	ds_solucao_w,
	qt_volume_w,
	nr_etapas_w,
	ds_horarios_w,
	ie_bomba_infusao_w,
	nr_agrupamento_w,
	ie_esquema_alternado_w,
	ie_calc_aut_w,
	ie_acm_w,
	hr_prim_horario_w,
	qt_hora_fase_w,
	ds_orientacao_w,
	ie_hemodialise_w,
	ie_tipo_solucao_w,
	ie_pos_dialisador_w,
	nr_seq_protocolo_w,
	ie_unid_vel_inf_w,
	qt_temp_solucao_w,
	ie_apap_w,
	qt_dose_ataque_w,
	ie_solucao_pca_w,
	ie_tipo_analgesia_w,
	ie_pca_modo_prog_w,
	qt_vol_infusao_pca_w,
	qt_bolus_pca_W,
	qt_intervalo_bloqueio_w,
	qt_limite_quatro_hora_w,
	qt_dose_inicial_pca_w,
	ie_se_necessario_w,
	ie_agora_w,
	ie_etapa_especial_w,
	hr_inicio_capd_w,
	hr_fim_capd_w,
	ie_lavar_linhas_w,
	ie_tidal_w,
	ie_ultima_bolsa_w,
	pr_concentracao_w,
	qt_hora_permanencia_w,
	qt_tempo_permanencia_w;
exit when C01%notfound;
	begin
	
	nr_seq_solucao_orig_w	:= nr_seq_solucao_w;
	ie_copiar_w := 'N';
	
	if	(cd_funcao_w <> 8030) or 
	    (ie_recalcular_hor_sol_w = 'S')	then
		hr_prim_horario_w := to_char(dt_primeiro_horario_w,'hh24:mi');
	end if;
	
	open C02;
	loop
	fetch C02 into	
		ie_copiar_w;
	exit when C02%notfound;
		ie_copiar_w := ie_copiar_w;
	end loop;
	close C02;
	
	if	(ie_recalc_total_w <> 'N') then
		select	nvl(max(qt_hora_sessao), 24)
		into	qt_tempo_aplicacao_w
		from	hd_prescricao
		where	nr_prescricao = nr_prescricao_p;		
	end if;
	
	if	(nvl(ie_copiar_w,'N')	= 'S') then	
		--Busca a pr�xima sequ�ncia
		ie_loop_w	:= 'S';
		nr_seq_solucao_w	:= 1;
		while	(ie_loop_w = 'S') loop
			select	count(*)
			into	qt_itens_w
			from	prescr_solucao
			where	nr_prescricao	= nr_prescricao_p
			and		nr_seq_solucao	= nr_seq_solucao_w;
			if	(qt_itens_w	= 0) then
				ie_loop_w	:= 'N';
			else	
				nr_seq_solucao_w	:= nr_seq_solucao_w + 1;
			end if;
		end loop;
		
		--Busca o pr�ximo agrupamento
		ie_loop_w	:= 'S';
		nr_agrupamento_w	:= 1;
		while	(ie_loop_w = 'S') loop
			select	count(*)
			into	qt_itens_w
			from	prescr_solucao
			where	nr_prescricao	= nr_prescricao_p
			and		nr_agrupamento	= nr_agrupamento_w;
			if	(qt_itens_w	= 0) then
				ie_loop_w	:= 'N';
			else	
				nr_agrupamento_w	:= nr_agrupamento_w + 1;
			end if;
		end loop;		
	end if;			
	
	if	(ie_copiar_w	= 'S') then
		insert into prescr_solucao (
				nr_prescricao,
				nr_seq_solucao,
				ie_via_aplicacao,
				cd_intervalo,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				ie_tipo_dosagem,
				qt_dosagem,
				qt_solucao_total,
				qt_tempo_aplicacao,
				ds_solucao,
				qt_volume,
				nr_etapas,
				ds_horarios,
				ie_bomba_infusao,
				ie_suspenso,
				nr_agrupamento,
				ie_esquema_alternado,
				ie_calc_aut,
				ie_acm,
				hr_prim_horario,
				qt_hora_fase,
				ie_urgencia,
				ds_orientacao,
				ie_hemodialise,
				ie_tipo_solucao,
				ie_pos_dialisador,
				nr_seq_protocolo,
				ie_unid_vel_inf,
				qt_temp_solucao,
				ie_apap,
				qt_dose_ataque,
				ie_solucao_pca,
				ie_tipo_analgesia,
				ie_pca_modo_prog,
				qt_vol_infusao_pca,
				qt_bolus_pca,
				qt_intervalo_bloqueio,
				qt_limite_quatro_hora,
				qt_dose_inicial_pca,
				ie_etapa_especial,
				ie_se_necessario,
				nr_seq_anterior,
				nr_prescricao_original,
				nr_prescricao_anterior,
				nr_sequencia_anterior,
				nr_seq_dialise,
				hr_inicio_capd,
				hr_fim_capd,
				ie_lavar_linhas,
				ie_tidal,
				ie_ultima_bolsa,
				pr_concentracao,
				qt_hora_permanencia,
				qt_tempo_permanencia)
		values(
				nr_prescricao_p,
				nr_seq_solucao_w,
				ie_via_aplicacao_w,
				cd_intervalo_w,
				dt_prescricao_p,
				nm_usuario_p,
				cd_unidade_medida_w,
				ie_tipo_dosagem_w,
				qt_dosagem_w,
				nvl(qt_volsolu_w, qt_solucao_total_w),
				qt_tempo_aplicacao_w,
				ds_solucao_w,
				qt_volume_w,
				nr_etapas_w,
				ds_horarios_w,
				ie_bomba_infusao_w,
				'N',
				nr_agrupamento_w,
				ie_esquema_alternado_w,
				ie_calc_aut_w,
				ie_acm_w,
				hr_prim_horario_w,
				qt_hora_fase_w,
				'N',
				ds_orientacao_w,
				ie_hemodialise_w,
				ie_tipo_solucao_w,
				ie_pos_dialisador_w,
				nr_seq_protocolo_w,
				ie_unid_vel_inf_w,
				qt_temp_solucao_w,
				ie_apap_w,
				qt_dose_ataque_w,
				ie_solucao_pca_w,
				ie_tipo_analgesia_w,
				ie_pca_modo_prog_w,
				qt_vol_infusao_pca_w,
				qt_bolus_pca_w,
				qt_intervalo_bloqueio_w,
				qt_limite_quatro_hora_w,
				qt_dose_inicial_pca_w,
				'N',
				ie_se_necessario_w,
				nr_seq_solucao_filtro_w,
				nr_prescricao_orig_p,
				nr_prescricao_orig_p,
				nr_seq_solucao_filtro_w,
				nr_seq_dialise_p,
				hr_inicio_capd_w,
				hr_fim_capd_w,
				ie_lavar_linhas_w,
				ie_tidal_w,
				ie_ultima_bolsa_w,
				pr_concentracao_w,
				qt_hora_permanencia_w,
				qt_tempo_permanencia_w);
		commit;
	
		insert into prescr_solucao_esquema(
				nr_sequencia,
				nr_prescricao,
				nr_seq_solucao,
				dt_atualizacao,
				nm_usuario,
				qt_volume,
				qt_dosagem,
				ds_horario)
		select	prescr_solucao_esquema_seq.nextval,
				nr_prescricao_p,
				nr_seq_solucao_w,
				sysdate,
				nm_usuario_p,
				b.qt_volume,
				b.qt_dosagem,
				b.ds_horario
		from	prescr_solucao_esquema b,
				prescr_solucao a
		where	a.nr_prescricao 		= nr_prescricao_orig_p
		and		a.nr_prescricao			= b.nr_prescricao
		and		a.nr_seq_solucao		= b.nr_seq_solucao
		and		a.nr_seq_solucao 		= nr_seq_solucao_w
		and		((ie_copia_agora_p 		= 'S') or (a.ie_urgencia = 'N'))
		and		nvl(a.ie_suspenso,'N')	<> 'S';
		
		commit;
	
		if	(ie_recalc_total_w <> 'N') then
			select	nvl(max(qt_hora_sessao), 24)
			into	qt_tempo_aplicacao_w
			from	hd_prescricao
			where	nr_prescricao = nr_prescricao_p;
			
			if	(nvl(nr_etapas_w,0)	> 0) then
				qt_hora_fase_w	:= dividir(qt_tempo_aplicacao_w,nr_etapas_w);
			end if;
			
			calcula_horarios_etapas(dt_primeiro_horario_w,nr_etapas_w,qt_hora_fase_w,nm_usuario_p,VarHorarioSolucao_w,qt_tempo_aplicacao_w,ds_horarios_w,ds_horarios2_w,ie_etapa_especial_w,'S');
						
			ds_horarios_w	:= ds_horarios_w || ds_horarios2_w; 
			
			update	prescr_solucao
			set		nr_etapas		= nr_etapas_w,
					ds_horarios		= ds_horarios_w,
					hr_prim_horario = obter_prim_dshorarios(ds_horarios_w),
					qt_hora_fase	= qt_hora_fase_w
			where	nr_prescricao	= nr_prescricao_p
			and		nr_seq_solucao	= nr_seq_solucao_w;
			commit;
			
		end if;
	
		select	nvl(max(nr_agrupamento),0)
		into	nr_agrup_acum_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p;
		
		select	nvl(max(nr_sequencia),0) +1
		into	nr_seq_mat_w
		From	Prescr_Material a
		where	a.nr_prescricao = nr_prescricao_p;

		insert  into prescr_material (
				nr_prescricao,
				nr_sequencia,
				ie_origem_inf,
				cd_material,
				cd_unidade_medida,
				qt_dose,
				qt_unitaria,
				qt_material,
				dt_atualizacao,
				nm_usuario,
				cd_intervalo,
				ds_horarios,
				ds_observacao,
				ds_observacao_enf,
				ie_via_aplicacao,
				nr_agrupamento,
				ie_cobra_paciente,
				cd_motivo_baixa,
				dt_baixa,
				ie_utiliza_kit,
				cd_unidade_medida_dose,
				qt_conversao_dose,
				ie_urgencia,
				nr_ocorrencia,
				qt_total_dispensar,
				cd_fornec_consignado,
				nr_sequencia_solucao,
				nr_sequencia_proc,
				qt_solucao,
				hr_dose_especial,
				qt_dose_especial,
				ds_dose_diferenciada,
				ie_medicacao_paciente,
				nr_sequencia_diluicao,
				hr_prim_horario,
				nr_sequencia_dieta,
				ie_agrupador,
				nr_dia_util,
				ie_suspenso,
				ie_se_necessario,
				qt_min_aplicacao,
				ie_bomba_infusao,
				ie_aplic_bolus,
				ie_aplic_lenta,
				ie_acm,
				ie_objetivo,
				cd_topografia_cih,
				ie_origem_infeccao,
				cd_amostra_cih,
				cd_microorganismo_cih,
				ie_uso_antimicrobiano,
				cd_protocolo,
				nr_seq_protocolo,
				nr_seq_mat_protocolo,
				qt_hora_aplicacao,
				ie_recons_diluente_fixo,
				qt_vel_infusao,
				ds_justificativa,
				ie_sem_aprazamento,
				ie_indicacao,
				dt_proxima_dose,
				qt_total_dias_lib,
				nr_seq_substituto,
				ie_lado,
				dt_inicio_medic,
				qt_dia_prim_hor,
				ie_regra_disp,
				qt_vol_adic_reconst,
				qt_hora_intervalo,
				qt_min_intervalo,
				nr_prescricao_original)		
		select  nr_prescricao_p,
				rownum + nr_seq_mat_w,
				a.ie_origem_inf,
				a.cd_material,
				a.cd_unidade_medida,
				a.qt_dose,
				a.qt_unitaria,
				a.qt_material,
				dt_prescricao_p,
				nm_usuario_p,
				a.cd_intervalo,
				a.ds_horarios,
				a.ds_observacao,
				a.ds_observacao_enf,
				a.ie_via_aplicacao,
				a.nr_agrupamento + nr_agrup_acum_w,
				nvl(a.ie_cobra_paciente,'S'),
				decode(nvl(a.ie_regra_disp,'X'), 'D', a.cd_motivo_baixa, decode(nvl(a.ie_cobra_paciente,'S'), 'S', 0, a.cd_motivo_baixa)),
				decode(nvl(a.ie_regra_disp,'X'), 'D', sysdate, decode(nvl(a.ie_cobra_paciente,'S'), 'S', null, sysdate)),
				a.ie_utiliza_kit,
				a.cd_unidade_medida_dose,
				a.qt_conversao_dose,
				'N',
				a.nr_ocorrencia,
				a.qt_total_dispensar,
				a.cd_fornec_consignado,
				nr_seq_solucao_w,
				decode(a.nr_sequencia_proc, null, null,a.nr_sequencia_proc),
				a.qt_solucao,
				null,
				null,
				a.ds_dose_diferenciada,
				a.ie_medicacao_paciente,
				decode(a.nr_sequencia_diluicao, null, null,a.nr_sequencia_diluicao),
				decode(a.ie_se_necessario, 'S', null, a.hr_prim_horario),
				decode(a.nr_sequencia_dieta, null,null,a.nr_sequencia_dieta),
				a.ie_agrupador,
				a.nr_dia_util,
				decode(b.ie_situacao, 'A', 'N', 'S'),
				a.ie_se_necessario,
				a.qt_min_aplicacao,
				a.ie_bomba_infusao,
				nvl(a.ie_aplic_bolus,'N'),
				nvl(a.ie_aplic_lenta,'N'),
				nvl(a.ie_acm,'N'),
				a.ie_objetivo,
				a.cd_topografia_cih,
				a.ie_origem_infeccao,
				a.cd_amostra_cih,
				a.cd_microorganismo_cih,
				nvl(a.ie_uso_antimicrobiano,'N'),
				a.cd_protocolo,
				a.nr_seq_protocolo,
				a.nr_seq_mat_protocolo,
				a.qt_hora_aplicacao,
				'N',
				a.qt_vel_infusao,
				a.ds_justificativa,
				a.ie_sem_aprazamento,
				a.ie_indicacao,
				a.dt_proxima_dose,
				a.qt_total_dias_lib,
				a.nr_seq_substituto,
				a.ie_lado,
				a.dt_inicio_medic,
				a.qt_dia_prim_hor,
				decode(nvl(a.ie_regra_disp,'X'), 'D', a.ie_regra_disp, null),
				a.qt_vol_adic_reconst,
				a.qt_hora_intervalo,
				a.qt_min_intervalo,
				nr_prescricao_orig_p
		from	material b,
				prescr_material a
		where	a.nr_prescricao 		= nr_prescricao_orig_p
		and		a.cd_material 			= b.cd_material
		and		((a.ie_suspenso 		<> 'S') or (ie_modificar_p = 'S'))
		and		b.ie_situacao			= 'A'
		and		a.ie_origem_inf			<> 'K'
		and		a.ie_agrupador			in(13,14)
		and		a.nr_sequencia_solucao  = nr_seq_solucao_orig_w;
		
		if (ie_categoria_w = 'C') then
			calc_etapa_sol_dialise(nr_prescricao_p, nr_seq_dialise_p, nr_seq_solucao_w,nm_usuario_p);
		end if;
		
		if (ie_tipo_solucao_w <> 'C') then
			calc_vol_total_sol_protocol(nr_prescricao_p, nr_seq_dialise_p, nr_seq_solucao_w);
		end if;
	
	end if;
	
	end;
end loop;
close C01;

commit;

end rep_copia_solucao_dialise;
/