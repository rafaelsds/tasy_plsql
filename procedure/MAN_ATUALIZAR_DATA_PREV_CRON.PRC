create or replace
procedure man_atualizar_data_prev_cron(nr_seq_cronograma_p	number,
			nm_usuario_p		Varchar2) is 

dt_inicio_prev_w	date;			
dt_fim_prev_w		date;
nr_seq_projeto_w	number(10);
			
begin
select	min(dt_inicio_prev)
into	dt_inicio_prev_w
from	proj_cron_etapa
where	nr_seq_cronograma = nr_seq_cronograma_p;

select	max(dt_fim_prev)
into	dt_fim_prev_w
from	proj_cron_etapa
where	nr_seq_cronograma = nr_seq_cronograma_p;


if	(dt_inicio_prev_w is not null) and
	(dt_fim_prev_w is not null) then
	update	proj_cronograma
	set	dt_inicio = dt_inicio_prev_w,
		dt_fim	  = dt_fim_prev_w
	where	nr_sequencia = nr_seq_cronograma_p;	
end if;


select	max(nr_seq_proj)	
into	nr_seq_projeto_w
from	proj_cronograma
where	nr_sequencia = nr_seq_cronograma_p;

select	min(dt_inicio),
	max(dt_fim)
into	dt_inicio_prev_w,
	dt_fim_prev_w
from	proj_cronograma
where	ie_situacao = 'A'
and	nr_seq_proj = nr_seq_projeto_w;

if	(dt_inicio_prev_w is not null) and
	(dt_fim_prev_w is not null) then
	update	proj_projeto
	set	dt_inicio_prev = dt_inicio_prev_w,
		dt_fim_prev	  = dt_fim_prev_w
	where	nr_sequencia = nr_seq_projeto_w;	
end if;	


commit;

end man_atualizar_data_prev_cron;
/
