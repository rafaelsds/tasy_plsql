create or replace
procedure pls_glosar_item_conta
			(	nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number) is
				
/* Felipe - OS 355351 - 25/08/2011 - Ajustado para o valor glosa ser o valor apresentado, mas se for zerado ent�o ser� o valor total liberado */ 

ie_status_fatura_w		varchar2(10);
ie_via_acesso_w			varchar2(5);
ie_via_acesso_inf_w		varchar2(5);
ie_origem_conta_w		varchar2(1);
ie_via_acesso_regra_w		varchar2(1);
ie_via_acesso_imp_w		varchar2(1);
ie_vl_calculado_glosa_w		varchar2(1);
vl_glosa_w			number(15,2);
vl_liberado_w			number(15,2);
vl_apresentado_w		number(15,2);
vl_procedimento_imp_w		number(15,2);
vl_procedimento_w		number(15,2);
nr_seq_protocolo_w		number(10);
nr_seq_conta_w			number(10);
vl_total_procedimento_w		number(10);
vl_material_w			number(10);
qt_glosa_mat_w			number(10);
qt_glosa_proc_w			number(10);
nr_seq_conta_proc_w		number(10);
nr_seq_conta_mat_w		number(10);
nr_seq_analise_w		number(10);
nr_seq_pos_estab_w		number(10);
nr_seq_regra_via_acesso_w	number(10);
qt_glosa_ativa_w		number(10);
nr_seq_fatura_w			number(10);
dt_devolucao_w			date;
vl_material_imp_w		number(15,2);
ie_analise_cm_nova_w		varchar2(3);
ie_via_acesso_manual_w		pls_conta_proc.ie_via_acesso_manual%type;

Cursor C01 is
	select	nr_sequencia,
		nvl(vl_procedimento_imp,0),
		nvl(vl_procedimento,0)
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_status	<> 'D'
	order by 1;	

Cursor C02 is
	select	nr_sequencia,
		vl_material_imp,
		vl_material
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_status	<> 'D'
	order by 1;

Cursor C03 is
	select	nr_sequencia,
		ie_via_acesso,
		ie_via_acesso_imp,
		nr_seq_regra_via_acesso,
		ie_via_acesso_manual
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_w
	and	ie_status	<> 'D'
	order by 1;
begin
nr_seq_pos_estab_w	:= 0;

select	nvl(ie_calculo_base_glosa, 'N'),
	nvl(ie_analise_cm_nova,'N')
into	ie_vl_calculado_glosa_w,
	ie_analise_cm_nova_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(nvl(nr_seq_conta_p,0) > 0) then
	/* Felipe - 28/03/2011 - OS 288481 - Hist�rico 25/03/2011 10:18:59 */
	begin
	select	a.nr_seq_fatura
	into	nr_seq_fatura_w
	from	pls_conta a
	where	a.nr_sequencia	= nr_seq_conta_p;
	exception
		when others then
		nr_seq_fatura_w	:= null;
	end;
	
	if	(nr_seq_fatura_w is not null) then
		select	a.ie_status,
			a.dt_devolucao
		into	ie_status_fatura_w,
			dt_devolucao_w
		from	ptu_fatura a
		where	a.nr_sequencia	= nr_seq_fatura_w;
		
		if	(ie_status_fatura_w <> 'CA') and
			(dt_devolucao_w is null) then
			select	count(1)
			into	qt_glosa_ativa_w
			from	pls_conta_glosa	a
			where	a.nr_seq_conta	= nr_seq_conta_p
			and	a.nr_seq_conta_proc is null
			and	a.nr_seq_conta_mat is null
			and	a.nr_seq_proc_partic is null
			and	a.ie_situacao	= 'A';
			
			if	(qt_glosa_ativa_w = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(210739);
			end if;
		end if;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_conta_proc_w,
		vl_procedimento_imp_w,
		vl_procedimento_w;
	exit when C01%notfound;
		begin
		nr_seq_pos_estab_w := 0;
		
		vl_glosa_w := vl_procedimento_imp_w;

		update	pls_conta_proc a
		set	a.vl_liberado		= 0,
			a.qt_procedimento	= 0,
			a.vl_unitario		= 0,
			a.vl_glosa		= vl_glosa_w,
			a.vl_prestador		= 0,
			a.vl_pag_medico_conta	= 0,
			a.ie_status		= 'L',
			a.ds_log		= 'pls_glosar_item_conta',
			a.ie_glosa		= 'S',
			a.ie_status_pagamento	= 'G',
			vl_liberado_hi		= 0,       
			vl_liberado_co          = 0,
			vl_liberado_material    = 0,
			vl_liberado_regra       = 0,
			vl_lib_original         = 0,
			vl_lib_taxa_co          = 0,
			vl_lib_taxa_material    = 0,
			vl_lib_taxa_servico     = 0,
			vl_glosa_co    		= vl_co_ptu_imp,         
			vl_glosa_hi             = vl_procedimento_ptu_imp,
			vl_glosa_material       = vl_material_ptu_imp,
			vl_glosa_taxa_co        = vl_taxa_co_imp,
			vl_glosa_taxa_material	= vl_taxa_material_imp,
			vl_glosa_taxa_servico   = vl_taxa_servico_imp
		where	a.nr_sequencia		= nr_seq_conta_proc_w;
		
		pls_delete_pls_conta_pos_estab(	nr_seq_conta_proc_w,null,null,
						null);
		
		pls_deletar_coparticipacao(nr_seq_conta_p,null,'S', 'N', null, null, nm_usuario_p,cd_estabelecimento_p);
		end;
	end loop;
	close C01;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_conta_mat_w,
		vl_material_imp_w,
		vl_material_w;
	exit when C02%notfound;
		begin
		if	(vl_material_imp_w = 0 ) and
			(ie_vl_calculado_glosa_w = 'S') then
			vl_glosa_w := vl_material_w;
		else
			vl_glosa_w := vl_material_imp_w;
		end if;
		
		nr_seq_pos_estab_w := 0;
		update	pls_conta_mat a
		set	a.vl_liberado	= 0,
			a.qt_material	= 0,
			a.vl_unitario	= 0,
			a.vl_glosa	= vl_glosa_w,
			a.ie_status		= 'L',
			a.ie_glosa		= 'S',
			a.ds_log		= 'pls_glosar_item_conta',
			a.ie_status_pagamento	= 'G',		
			vl_liberado_regra       = 0,
			vl_lib_original         = 0,
			vl_lib_taxa_material    = 0,
			vl_glosa_taxa_material  = vl_taxa_material_imp
		where	a.nr_sequencia	= nr_seq_conta_mat_w;
		
		pls_delete_pls_conta_pos_estab(	null, nr_seq_conta_mat_w,null,
						null);
		
		pls_deletar_coparticipacao(nr_seq_conta_p,null,'S', 'N', null, null, nm_usuario_p,cd_estabelecimento_p);
		end;
	end loop;
	close C02;

	nr_seq_conta_w		:= nr_seq_conta_p;
	
	pls_atualizar_conta_resumo(	nr_seq_conta_p,
					cd_estabelecimento_p,
					nm_usuario_p);
					
				
	pls_cta_consistir_pck.gerar_resumo_conta(null, null, null, nr_seq_conta_w, nm_usuario_p, cd_estabelecimento_p);
	
	select	max(nr_seq_analise)
	into	nr_seq_analise_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_p;
	
	select	max(ie_origem_conta)
	into	ie_origem_conta_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_p;
	
	if	(nvl(nr_seq_analise_w,0) > 0) and
		(ie_analise_cm_nova_w = 'N') then
		if	(ie_origem_conta_w = 'A') then
			pls_atual_w_resumo_conta_ptu(	nr_seq_conta_p,
							null,
							null,
							null,
							nr_seq_analise_w,
							nm_usuario_p);
		else
			pls_atualiza_w_resumo_conta(	nr_seq_conta_p,
							null,
							null,
							null,
							nr_seq_analise_w,
							nm_usuario_p);
		end if;
	end if;
elsif	(nvl(nr_seq_conta_proc_p,0) > 0) then
	select	nr_seq_conta,
		vl_procedimento,
		vl_procedimento_imp
	into	nr_seq_conta_w,
		vl_procedimento_w,
		vl_procedimento_imp_w
	from	pls_conta_proc
	where	nr_sequencia	= nr_seq_conta_proc_p;

	select	max(ie_origem_conta)
	into	ie_origem_conta_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_w;
	
	if	(vl_procedimento_imp_w = 0 ) and
		(ie_vl_calculado_glosa_w = 'S') then
		vl_glosa_w := vl_procedimento_w;
	else
		vl_glosa_w := vl_procedimento_imp_w;
	end if;
	
	update	pls_conta_proc
	set	vl_liberado		= 0,
		qt_procedimento		= 0,
		vl_unitario		= 0,
		vl_prestador		= 0,
		vl_pag_medico_conta	= 0,
		ie_status		= 'L',
		ds_log			= 'pls_glosar_item_conta',
		ie_glosa		= 'S',
		vl_liberado_hi		= 0,
		vl_liberado_co          = 0,
		vl_liberado_material    = 0,
		vl_liberado_regra       = 0,
		vl_lib_original         = 0,
		vl_lib_taxa_co          = 0,
		vl_lib_taxa_material    = 0,
		vl_lib_taxa_servico     = 0
	where	nr_sequencia		= nr_seq_conta_proc_p;
	
	
	pls_delete_pls_conta_pos_estab(	nr_seq_conta_proc_p, null,null,
					null);
	
	pls_deletar_coparticipacao(nr_seq_conta_p,null,'S', 'N', null, null, nm_usuario_p,cd_estabelecimento_p);
		
	select	nvl(ie_via_acesso_regra, 'N')
	into	ie_via_acesso_regra_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_p;
	
	open C03;
	loop 
	fetch C03 into	
		nr_seq_conta_proc_w,
		ie_via_acesso_w,
		ie_via_acesso_imp_w,
		nr_seq_regra_via_acesso_w,
		ie_via_acesso_manual_w;
	exit when C03%notfound;
		begin
		ie_via_acesso_inf_w	:= 'S';
		
		if	(nvl(ie_origem_conta_w, 'D') = 'E') and
			(ie_via_acesso_regra_w = 'S') then
			if	(nvl(ie_via_acesso_w, 'X') = nvl(ie_via_acesso_imp_w, 'X')) and
				(not ie_via_acesso_manual_w	= 'S')then
				ie_via_acesso_inf_w	:= 'N';
			end if;
		end if;
		
		if	(ie_via_acesso_inf_w = 'N') or
			(nvl(nr_seq_regra_via_acesso_w, 0) > 0)then
			update	pls_conta_proc
			set	ie_via_acesso		= null,
				nr_seq_regra_via_acesso	= null,
				tx_item			= 100,
				nr_seq_proc_ref		= null
			where	nr_sequencia		= nr_seq_conta_proc_w;
		end if;
		end;
	end loop;
	close C03;

	
	/*Retirado pois estava gerando inconsist�cncia no valor total liberado da conta
	Alinhado com o analista Diogo*/
	/*pls_consiste_procedimento(	nr_seq_conta_w,
					'CC',
					nm_usuario_p,
					cd_estabelecimento_p);
					
	pls_atualizar_via_acesso_conta(	nr_seq_conta_w,
					nm_usuario_p);
					
	pls_recalcular_conta(	nr_seq_conta_w,
				nm_usuario_p,
				'G',
				'S');*/
	
	select	vl_procedimento,
		vl_procedimento_imp
	into	vl_procedimento_w,
		vl_procedimento_imp_w
	from	pls_conta_proc
	where	nr_sequencia	= nr_seq_conta_proc_p;
	
	if	(vl_procedimento_imp_w = 0) and
		(ie_vl_calculado_glosa_w = 'S') then
		vl_glosa_w	:= vl_procedimento_w;
	else
		vl_glosa_w	:= vl_procedimento_imp_w;
	end if;
	
	update	pls_conta_proc
	set	vl_glosa	= vl_glosa_w
	where	nr_sequencia	= nr_seq_conta_proc_p;
	
	pls_atualizar_conta_resumo(	nr_seq_conta_w,
					cd_estabelecimento_p,
					nm_usuario_p);
					
	pls_cta_consistir_pck.gerar_resumo_conta(null, null, null, nr_seq_conta_w, nm_usuario_p, cd_estabelecimento_p);
	
elsif	(nvl(nr_seq_conta_mat_p, 0) > 0) then
	select	nr_seq_conta,
		vl_material_w
	into	nr_seq_conta_w,
		vl_material_w
	from	pls_conta_mat
	where	nr_sequencia	= nr_seq_conta_mat_p;

	/*Diego OS 303277 -	Alterada a linha decode(nvl(vl_material_imp,0),0,nvl(vl_material,0)),
			Quando existe o valor do importado importado e tenta-se glosar, o valor da glosa � jogado nulo pois n�o existia o sen�o do decode*/	
	/* Felipe - 04/07/2011 - OS 327943 - O valor glosado, deve ser o total que ser� pago atrav�s do resumo */
	select	nvl(sum(vl_liberado),vl_material_w)
	into	vl_liberado_w
	from	pls_conta_medica_resumo
	where	nr_seq_conta_mat	= nr_seq_conta_mat_p
	and	nr_seq_conta		= nr_seq_conta_w
	and	((ie_situacao != 'I') or (ie_situacao is null));
	
	update	pls_conta_mat
	set	vl_liberado	= 0,
		qt_material	= 0,
		vl_unitario	= 0,
		vl_glosa	= decode(nvl(vl_material_imp, 0), 0, vl_liberado_w, vl_material_imp),
		ie_status	= 'L',
		ie_glosa	= 'S',		
		vl_liberado_regra       = 0,
		vl_lib_original         = 0,
		vl_lib_taxa_material    = 0
	where	nr_sequencia	= nr_seq_conta_mat_p;

	pls_atualizar_item_resumo(	nr_seq_conta_mat_p,
					'M',
					nm_usuario_p,
					'N');
	
	pls_delete_pls_conta_pos_estab(	null, nr_seq_conta_mat_p,null,
					null);
					
	
	pls_deletar_coparticipacao(nr_seq_conta_p,null,'S', 'N', null, null, nm_usuario_p,cd_estabelecimento_p);
end if;

select	count(1)
into	qt_glosa_proc_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_w
and	nvl(ie_glosa,'N')	= 'N';

select	count(1)
into	qt_glosa_mat_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_w
and	nvl(ie_glosa,'N')	= 'N';

if	(nvl(qt_glosa_proc_w,0) = 0) and
	(nvl(qt_glosa_mat_w,0) = 0) then
	update	pls_conta
	set	ie_glosa	= 'S'
	where	nr_sequencia	= nr_seq_conta_w;
	if	(nvl(obter_valor_param_usuario(1322, 1, Obter_Perfil_Ativo, nm_usuario_p, 0), 'N')	= 'S') then
		pls_gerar_documento_conta( nr_seq_conta_w||',', cd_estabelecimento_p, nm_usuario_p);
	end if;
end if;

pls_atualizar_utilizacao_guia(nr_seq_conta_w, cd_estabelecimento_p, nm_usuario_p);

pls_fechar_conta(nr_seq_conta_w,
		'N',
		'S',
		'S',
		cd_estabelecimento_p,
		nm_usuario_p,
		null,
		null);

select	nr_seq_protocolo
into	nr_seq_protocolo_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_w;

pls_altera_status_protocolo(	nr_seq_protocolo_w,
				'L',
				'N',
				cd_estabelecimento_p,
				nm_usuario_p);

end pls_glosar_item_conta;
/
