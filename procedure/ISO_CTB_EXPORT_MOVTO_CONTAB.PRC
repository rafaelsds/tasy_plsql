create or replace
procedure ISO_CTB_export_movto_contab(	nr_lote_contabil_p		number,
					nm_usuario_p		varchar2) is 
					

nr_seq_lancamento_w		ctb_movimento.nr_sequencia%type;
nr_seq_movimento_w		ctb_movimento.nr_sequencia%type;
nr_lote_contabil_w			lote_contabil.nr_lote_contabil%type;
nr_seq_agrupamento_w		ctb_movimento.nr_seq_agrupamento%type;
nr_seq_agrupamento_ant_w		ctb_movimento.nr_seq_agrupamento%type;
cd_conta_credito_w		conta_contabil.cd_conta_contabil%type;	
cd_conta_debito_w			conta_contabil.cd_conta_contabil%type;
vl_movimento_w			ctb_movimento.vl_movimento%type;
dt_movimento_w			ctb_movimento.dt_movimento%type;
dt_movimento_ww			ctb_movimento.dt_movimento%type;
cd_historico_w			historico_padrao.cd_historico%type;
ds_complemento_w			ctb_movimento.ds_compl_historico%type;
cd_conta_centro_credito_w		centro_custo.cd_centro_custo%type;	
cd_conta_centro_debito_w		centro_custo.cd_centro_custo%type;
nr_sequencia_w			iso_ctb_movimento_export.nr_sequencia%type;
nr_sequencia_ant_w		iso_ctb_movimento_export.nr_sequencia%type;
qt_contador_w			number(4);

nr_lote_contabil_sub_lanc_w		lote_contabil.nr_lote_contabil%type;
nr_seq_agrupamento_sub_lanc_w	ctb_movimento.nr_seq_agrupamento%type;
cd_conta_sub_lanc_w		conta_contabil.cd_conta_contabil%type;	
vl_movimento_sub_lanc_w		ctb_movimento.vl_movimento%type;
cd_historico_sub_lanc_w		historico_padrao.cd_historico%type;			
ds_compl_historico_sub_lanc_w	ctb_movimento.ds_compl_historico%type;
cd_ccentro_cred_sub_lanc_w	centro_custo.cd_centro_custo%type;
cd_ccentro_deb_sub_lanc_w		centro_custo.cd_centro_custo%type;
					
Cursor C01 is -- Cursor que seleciona os movimentos simples (1 d�bito para 1 cr�dito)
	select	a.nr_sequencia,
		a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_credito,
		a.cd_conta_debito,
		a.vl_movimento,
		to_char(a.dt_movimento,'dd/mm/yyyy') dt_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito
	from	ctb_movimento a	
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	cd_conta_credito is not null
	and	cd_conta_debito is null
	and	exists	
		(select 1 from ctb_movimento b
		where	a.nr_lote_contabil = b.nr_lote_contabil
		and	a.nr_seq_agrupamento = b.nr_seq_agrupamento
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		and	b.cd_conta_credito is not null
		and	b.cd_conta_debito is null
		group	by	nr_seq_agrupamento
		having	count(*) = 1)
	and	not exists 
		(select 1 
		from 	ctb_movimento b
		where  	a.nr_lote_contabil = b.nr_lote_contabil
		and	a.nr_seq_agrupamento = b.nr_seq_agrupamento
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		and	b.cd_conta_credito is null
		and	b.cd_conta_debito is not null
		group 	by nr_seq_agrupamento
		having 	count(*) > 1)
	union
	select	a.nr_sequencia,
		a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_credito,
		a.cd_conta_debito,
		a.vl_movimento,
		to_char(a.dt_movimento,'dd/mm/yyyy') dt_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito
	from	ctb_movimento a	
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	a.cd_conta_credito is null
	and	a.cd_conta_debito is not null
	and	exists	
		(select	1 
		from 	ctb_movimento b
		where	a.nr_lote_contabil = b.nr_lote_contabil
		and	a.nr_seq_agrupamento = b.nr_seq_agrupamento
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		and	b.cd_conta_credito is null
		and	b.cd_conta_debito is not null
		group 	by	nr_seq_agrupamento
		having	count(*) = 1)
	and	not		exists 
		(select 1 
		from 	ctb_movimento b
		where  	a.nr_lote_contabil = b.nr_lote_contabil
		and	a.nr_seq_agrupamento = b.nr_seq_agrupamento
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		and	b.cd_conta_credito is not null
		and	b.cd_conta_debito is null
		group 	by 	b.nr_seq_agrupamento 
		having count(*) > 1)
	union
	select	a.nr_sequencia,
		a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_credito,
		a.cd_conta_debito,
		a.vl_movimento,
		to_char(a.dt_movimento,'dd/mm/yyyy') dt_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito
	from	ctb_movimento a
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	a.cd_conta_credito is not null
	and	a.cd_conta_debito is not null
	order	by nr_seq_agrupamento,dt_movimento;
	
Cursor C02 is -- Seleciona o movimento de d�bito principal
	select	a.nr_sequencia,
		a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_credito,
		a.cd_conta_debito,
		a.vl_movimento,
		to_char(a.dt_movimento,'dd/mm/yyyy') dt_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito
	from	ctb_movimento a	
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	cd_conta_credito is null
	and	cd_conta_debito is not null
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is null
		and	b.cd_conta_credito is not null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) > 1)
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is not null
		and	b.cd_conta_credito is null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) = 1)	
	order	by a.nr_seq_agrupamento,dt_movimento;

Cursor 	C03 is	-- Seleciona os cr�ditos do mesmo agrupador do d�bito principal.
	select	a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_credito,
		a.vl_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito	
	from	ctb_movimento a	
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	a.nr_seq_agrupamento = nr_seq_agrupamento_w
	and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(dt_movimento_w,'dd/mm/yyyy')
	and	a.cd_conta_debito is null
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is null
		and	b.cd_conta_credito is not null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) > 1)
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is not null
		and	b.cd_conta_credito is null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) = 1)	;
	
Cursor C04 is -- Seleciona o movimento de cr�dito principal
	select	a.nr_sequencia,
		a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_credito,
		a.cd_conta_debito,
		a.vl_movimento,
		to_char(a.dt_movimento,'dd/mm/yyyy') dt_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito
	from	ctb_movimento a	
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	cd_conta_credito is not null
	and	cd_conta_debito is null
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is not null
		and	b.cd_conta_credito is null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) > 1)
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is null
		and	b.cd_conta_credito is not null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) = 1)
	order	by a.nr_seq_agrupamento,dt_movimento;

Cursor 	C05 is	-- Seleciona os d�bitos do mesmo agrupador do cr�dito proncipal.
	select	a.nr_lote_contabil,
		a.nr_seq_agrupamento,
		a.cd_conta_debito,
		a.vl_movimento,
		a.cd_historico,
		a.ds_compl_historico,
		null cd_conta_centro_credito,
		null cd_conta_centro_debito	
	from	ctb_movimento a	
	where	a.nr_lote_contabil = nr_lote_contabil_p
	and	a.nr_seq_agrupamento = nr_seq_agrupamento_w
	and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(dt_movimento_w,'dd/mm/yyyy')
	and	a.cd_conta_credito is null
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is not null
		and	b.cd_conta_credito is null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) > 1)
	and	exists     
		(select 1 from ctb_movimento b
		where	b.nr_seq_agrupamento = a.nr_seq_agrupamento
		and	b.cd_conta_debito is null
		and	b.cd_conta_credito is not null
		and	b.nr_lote_contabil = a.nr_lote_contabil
		and	to_char(a.dt_movimento,'dd/mm/yyyy') = to_char(b.dt_movimento,'dd/mm/yyyy')
		group 	by a.cd_conta_debito
		having	count(*) = 1);
begin

delete	from	ISO_CTB_MOVIMENTO_EXPORT
where	nr_lote_contabil	= nr_lote_contabil_p
and	nm_usuario		= nm_usuario_p;
commit;

qt_contador_w := 0;
open C01;
loop
fetch C01 into	
	nr_seq_movimento_w,
	nr_lote_contabil_w,
	nr_seq_agrupamento_w,
	cd_conta_credito_w,
	cd_conta_debito_w,
	vl_movimento_w,
	dt_movimento_w,
	cd_historico_w,
	ds_complemento_w,
	cd_conta_centro_credito_w,
	cd_conta_centro_debito_w;
exit when C01%notfound;
	begin
	
	qt_contador_w := qt_contador_w + 1;
	
	select	iso_ctb_movimento_export_seq.nextval
	into	nr_sequencia_w
	from 	dual;
	
	if	((nr_seq_agrupamento_ant_w = nr_seq_agrupamento_w) and
			(dt_movimento_ww = dt_movimento_w))then
			
			
			if	(nvl(cd_conta_debito_w,'0') <> '0') then
				update	iso_ctb_movimento_export
				set	cd_conta_debito = cd_conta_debito_w
				where	nr_sequencia = nr_sequencia_ant_w
				and	cd_conta_credito is not null
				and nr_seq_agrupamento = nr_seq_agrupamento_w;
				commit;
			end if;
			
			if	(nvl(cd_conta_credito_w,'0') <> '0') then
				update	iso_ctb_movimento_export
				set	cd_conta_credito = cd_conta_credito_w
				where	nr_sequencia = nr_sequencia_ant_w
				and	cd_conta_debito is not null
				and	nr_seq_agrupamento = nr_seq_agrupamento_w;
				commit;
			end if;
			
		
	else
		iso_ctb_movto_export_INSERT(	nr_sequencia_w,
						nr_lote_contabil_w,
						nm_usuario_p,
						dt_movimento_w,
						vl_movimento_w,
						cd_historico_w,
						cd_conta_debito_w,
						cd_conta_credito_w,
						ds_complemento_w,
						nr_seq_agrupamento_w,
						nr_seq_movimento_w,
						cd_conta_centro_debito_w,
						cd_conta_centro_credito_w);
							
	end if;
	
	if	(qt_contador_w >= 100) then
		qt_contador_w := 0;
		commit;
	end if;
	nr_sequencia_ant_w		:= nr_sequencia_w;	
	nr_seq_agrupamento_ant_w	:= nr_seq_agrupamento_w;
	dt_movimento_ww			:= dt_movimento_w;
	nr_seq_agrupamento_w		:= 0;
	end;
end loop;
close C01;
if	(qt_contador_w > 0) then
	qt_contador_w := 0;
	commit;
end if;

open C02;
loop
fetch C02 into	
	nr_seq_movimento_w,
	nr_lote_contabil_w,
	nr_seq_agrupamento_w,
	cd_conta_credito_w,
	cd_conta_debito_w,
	vl_movimento_w,
	dt_movimento_w,
	cd_historico_w,
	ds_complemento_w,
	cd_conta_centro_credito_w,
	cd_conta_centro_debito_w;
exit when C02%notfound;
	begin
	
	select	iso_ctb_movimento_export_seq.nextval
	into	nr_sequencia_w
	from 	dual;
	
	iso_ctb_movto_export_INSERT(	nr_sequencia_w,
					nr_lote_contabil_w,
					nm_usuario_p,
					dt_movimento_w,
					vl_movimento_w,
					cd_historico_w,
					cd_conta_debito_w,
					'M',
					ds_complemento_w,
					nr_seq_agrupamento_w,
					nr_seq_movimento_w,
					cd_conta_centro_debito_w,
					cd_conta_centro_credito_w);
			
	open C03;
	loop
	fetch C03 into	
		nr_lote_contabil_sub_lanc_w,
		nr_seq_agrupamento_sub_lanc_w,
		cd_conta_sub_lanc_w,
		vl_movimento_sub_lanc_w,
		cd_historico_sub_lanc_w,
		ds_compl_historico_sub_lanc_w,
		cd_ccentro_cred_sub_lanc_w,
		cd_ccentro_deb_sub_lanc_w;
	exit when C03%notfound;
		begin
		
		select	iso_ctb_movimento_export_seq.nextval
		into	nr_sequencia_w
		from 	dual;
		
		iso_ctb_movto_export_INSERT(	nr_sequencia_w,
						nr_lote_contabil_sub_lanc_w,
						nm_usuario_p,
						dt_movimento_w,
						vl_movimento_sub_lanc_w,
						cd_historico_sub_lanc_w,
						cd_conta_debito_w,
						cd_conta_sub_lanc_w,
						ds_compl_historico_sub_lanc_w,
						nr_seq_agrupamento_sub_lanc_w,
						nr_seq_movimento_w,
						cd_ccentro_cred_sub_lanc_w,
						cd_ccentro_cred_sub_lanc_w);

		end;
	end loop;
	close C03;
	commit;
	end;
end loop;
close C02;

open C04;
loop
fetch C04 into	
	nr_seq_movimento_w,
	nr_lote_contabil_w,
	nr_seq_agrupamento_w,
	cd_conta_credito_w,
	cd_conta_debito_w,
	vl_movimento_w,
	dt_movimento_w,
	cd_historico_w,
	ds_complemento_w,
	cd_conta_centro_credito_w,
	cd_conta_centro_debito_w;
exit when C04%notfound;
	begin
	
		select	iso_ctb_movimento_export_seq.nextval
		into	nr_sequencia_w
		from 	dual;
		
		iso_ctb_movto_export_INSERT(	nr_sequencia_w,
						nr_lote_contabil_w,
						nm_usuario_p,
						dt_movimento_w,
						vl_movimento_w,
						cd_historico_w,
						'M',
						cd_conta_credito_w,
						ds_complemento_w,
						nr_seq_agrupamento_w,
						nr_seq_movimento_w,
						cd_conta_centro_debito_w,
						cd_conta_centro_credito_w);
				
		open C05;
		loop
		fetch C05 into	
			nr_lote_contabil_sub_lanc_w,
			nr_seq_agrupamento_sub_lanc_w,
			cd_conta_sub_lanc_w,
			vl_movimento_sub_lanc_w,
			cd_historico_sub_lanc_w,
			ds_compl_historico_sub_lanc_w,
			cd_ccentro_cred_sub_lanc_w,
			cd_ccentro_deb_sub_lanc_w;
		exit when C05%notfound;
			begin
				
			select	iso_ctb_movimento_export_seq.nextval
			into	nr_sequencia_w
			from 	dual;
			
			iso_ctb_movto_export_INSERT(	nr_sequencia_w,
							nr_lote_contabil_sub_lanc_w,
							nm_usuario_p,
							dt_movimento_w,
							vl_movimento_sub_lanc_w,
							cd_historico_sub_lanc_w,
							cd_conta_sub_lanc_w,
							cd_conta_credito_w,
							ds_compl_historico_sub_lanc_w,
							nr_seq_agrupamento_sub_lanc_w,
							nr_seq_movimento_w,
							cd_ccentro_cred_sub_lanc_w,
							cd_ccentro_cred_sub_lanc_w);

			end;
		end loop;
		close C05;
		commit;
	end;
end loop;
close C04;

ISO_CTB_export_movto_multiplo(nr_lote_contabil_p,nm_usuario_p);

commit;

end ISO_CTB_export_movto_contab;
/