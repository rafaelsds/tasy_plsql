create or replace
procedure gerar_liberacao_ciclo_js(
		nr_seq_paciente_p	number,
		ie_todos_p		varchar2,
		cd_pessoa_fisica_p	varchar2,
		nm_usuario_p		varchar2,
		ds_msg_p	out	varchar2) is
		
nr_seq_retorno_w		number(10);
ds_consistencia_w		varchar2(255);
		
begin

	gerar_liberacao_ciclo(nr_seq_paciente_p, ie_todos_p, cd_pessoa_fisica_p, nm_usuario_p,nr_seq_retorno_w,ds_consistencia_w);
	
	ds_msg_p := obter_texto_tasy(108183, wheb_usuario_pck.get_nr_seq_idioma);

end gerar_liberacao_ciclo_js;
/
