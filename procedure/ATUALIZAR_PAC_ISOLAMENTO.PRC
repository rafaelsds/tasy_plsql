create or replace
procedure atualizar_pac_isolamento(	nr_sequencia_p	number,
				nr_atendimento_p	number,
				nm_usuario_p	varchar2) is

begin

if	(nr_atendimento_p is not null) then 
	
	update	atendimento_paciente 
	set    	ie_paciente_isolado = 'N', 
		dt_atualizacao = sysdate, 
		nm_usuario = nm_usuario_p
	where  	nr_atendimento = nr_atendimento_p;
	
	update	atendimento_precaucao
	set	dt_final_precaucao = sysdate,
		dt_atualizacao = sysdate, 
		nm_usuario = nm_usuario_p
	where  nr_sequencia = nr_sequencia_p;
	
	gerar_atendimento_precaucao(0,nr_atendimento_p,'T',0,nm_usuario_p);
	gerar_motivo_isolamento(nr_atendimento_p,0,nm_usuario_p,'T','','',0);	
	
end if;	

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end atualizar_pac_isolamento;
/
