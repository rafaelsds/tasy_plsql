create or replace
procedure Validar_Contato_Lista_Espera(
			nr_seq_contato_p		number) is 

ie_tipo_contato_w	varchar2(10);			
nr_seq_lista_espera_w	number(10);
nr_seq_pac_mut_w		number(10);
nr_seq_mutirao_w		number(10);

begin

if(nr_seq_contato_p is not null)then

	select  max(b.ie_tipo_contato),
			max(a.nr_seq_lista_espera) 
	into 	ie_tipo_contato_w,
			nr_seq_lista_espera_w
	from    ag_lista_espera_contato a,
			lista_espera_situacao_cont b
	where   a.nr_seq_situacao_atual = b.nr_sequencia
	and     b.ie_situacao = 'A'
	and     a.nr_sequencia = nr_seq_contato_p;
	
	if(nvl(ie_tipo_contato_w, 'XPTO') = 'PJ' and nr_seq_lista_espera_w is not null)then

		update agenda_lista_espera
		set ie_status_espera = 'C',
			dt_atualizacao = sysdate,
			nm_usuario = substr(wheb_usuario_pck.get_nm_usuario,1,15)
		where nr_sequencia = nr_seq_lista_espera_w;

		
		select  max(nr_sequencia),
				max(nr_seq_mutirao)
		into 	nr_seq_pac_mut_w,
				nr_seq_mutirao_w
		from    paciente_mutirao
		where   nr_seq_lista_espera = nr_seq_lista_espera_w;
		
		if(nr_seq_pac_mut_w is not null)then
		
			update  paciente_mutirao
			set 	dt_exclusao = sysdate
			where   nr_sequencia = nr_seq_pac_mut_w;
		
			commit;
			
			gerar_participante_mutirao(nr_seq_mutirao_w, wheb_usuario_pck.get_nm_usuario);
		
		
		end if;
		
	end if;

end if;

commit;

end Validar_Contato_Lista_Espera;
/