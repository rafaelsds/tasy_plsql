create or replace
procedure importar_canc_tit_pag_arq	(nr_titulo_p		varchar2,
					nm_usuario_p		varchar2,
					dt_cancelamento_p	date,
					nr_externo_p		varchar2) is


nr_seq_alt_valor_w	number(10);
qt_externo_w		number(10);

/* 
Procedure utilizada para importa��o padr�o WHEB dos calncelamentos de t�tulos a pagar
Usar interface 1624 - "Integra��o WHEB - Importar Cancelamento t�tulos a pagar 1.0" 
*/

BEGIN

IF	(nr_titulo_p IS NULL) THEN
	Wheb_mensagem_pck.exibir_mensagem_abort(230949);
end if;


select	count(*)
into	qt_externo_w
from	titulo_pagar_alt_valor
where	nr_titulo 	= nr_titulo_p
and	nr_externo 	= nr_externo_p;


if (qt_externo_w = 0) then
	begin
	Cancelar_Titulo_pagar(nr_titulo_p,nm_usuario_p,dt_cancelamento_p);
	
	select	max(nr_sequencia)
	into	nr_seq_alt_valor_w
	from	titulo_pagar_alt_valor
	where	nr_titulo = nr_titulo_p;
	
	update	titulo_pagar_alt_valor
	set	nr_externo 	= nr_externo_p
	where	nr_titulo 	= nr_titulo_p
	and	nr_sequencia	= nr_seq_alt_valor_w;
	end;


/* 	Altera��o realizada conforme OS 558926
else

	
	insert into log_tasy
		(cd_log,
		nm_usuario,
		dt_atualizacao,
		ds_log)
	values	(55786,
		'IMP_WHEB',
		sysdate,
		'Erro ao inserir o t�tulo ' || nr_titulo_p || chr(13) || chr(10) ||
		'Erro: Este cancelamento j� foi importado!');
*/

end if;

commit;

end importar_canc_tit_pag_arq;
/