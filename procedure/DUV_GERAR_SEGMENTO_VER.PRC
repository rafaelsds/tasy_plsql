create or replace procedure duv_gerar_segmento_ver(nr_seq_mensagem_p in duv_mensagem.nr_sequencia%type,
                                                   nm_usuario_p      in usuario.nm_usuario%type,
                                                   nr_seq_episodio_p in episodio_paciente.nr_sequencia%type)is
                                                   
  cursor c01 is
    select cf.nr_seq_localizacao pr_cabeca,
           cf.nr_seq_localizacao pr_pescoco, 
           cf.nr_seq_localizacao pr_queimado_frente, 
           cf.nr_seq_localizacao pr_queimado_costas, 
           cf.nr_seq_localizacao pr_nadega_direita, 
           cf.nr_seq_localizacao pr_nadega_esquerda, 
           cf.nr_seq_localizacao pr_genitalia, 
           cf.nr_seq_localizacao pr_braco_direito, 
           cf.nr_seq_localizacao pr_braco_esquerdo, 
           cf.nr_seq_localizacao pr_antebraco_direito, 
           cf.nr_seq_localizacao pr_antebraco_esquerdo, 
           cf.nr_seq_localizacao pr_mao_direita, 
           cf.nr_seq_localizacao pr_mao_esquerda, 
           cf.nr_seq_localizacao pr_coxa_direita, 
           cf.nr_seq_localizacao pr_coxa_esquerda, 
           cf.nr_seq_localizacao pr_perna_direita, 
           cf.nr_seq_localizacao pr_perna_esquerda, 
           cf.nr_seq_localizacao pr_pe_direito, 
           cf.nr_seq_localizacao pr_pe_esquerdo, 
           null                  ie_grau_severidade   
    from  atendimento_paciente    ap,
          episodio_paciente       ep,
          cur_ferida              cf           
    where ap.nr_seq_episodio        =ep.nr_sequencia
    and   ap.nr_seq_episodio        =nr_seq_episodio_p  --1091
    and   ap.nr_atendimento         =cf.nr_atendimento; 
                        

  c01_w c01%rowtype;                                                   
                                                   
begin 
  c01_w := null;
  open c01;
  fetch c01 into c01_w;
  close c01;
  insert into duv_ver (nr_sequencia, 
                       dt_atualizacao, 
                       nm_usuario, 
                       dt_atualizacao_nrec, 
                       nm_usuario_nrec, 
                       nr_seq_mensagem, 
                       pr_cabeca, 
                       pr_pescoco, 
                       pr_queimado_frente, 
                       pr_queimado_costas, 
                       pr_nadega_direita, 
                       pr_nadega_esquerda, 
                       pr_genitalia, 
                       pr_braco_direito, 
                       pr_braco_esquerdo, 
                       pr_antebraco_direito, 
                       pr_antebraco_esquerdo, 
                       pr_mao_direita, 
                       pr_mao_esquerda, 
                       pr_coxa_direita, 
                       pr_coxa_esquerda, 
                       pr_perna_direita, 
                       pr_perna_esquerda, 
                       pr_pe_direito, 
                       pr_pe_esquerdo, 
                       ie_grau_severidade) values (duv_ver_seq.nextval,
                                                   sysdate,
                                                   nm_usuario_p,
                                                   sysdate,
                                                   nm_usuario_p,
                                                   nr_seq_mensagem_p,
                                                   c01_w.pr_cabeca, 
                                                   c01_w.pr_pescoco, 
                                                   c01_w.pr_queimado_frente, 
                                                   c01_w.pr_queimado_costas, 
                                                   c01_w.pr_nadega_direita, 
                                                   c01_w.pr_nadega_esquerda, 
                                                   c01_w.pr_genitalia, 
                                                   c01_w.pr_braco_direito, 
                                                   c01_w.pr_braco_esquerdo, 
                                                   c01_w.pr_antebraco_direito, 
                                                   c01_w.pr_antebraco_esquerdo, 
                                                   c01_w.pr_mao_direita, 
                                                   c01_w.pr_mao_esquerda, 
                                                   c01_w.pr_coxa_direita, 
                                                   c01_w.pr_coxa_esquerda, 
                                                   c01_w.pr_perna_direita, 
                                                   c01_w.pr_perna_esquerda, 
                                                   c01_w.pr_pe_direito, 
                                                   c01_w.pr_pe_esquerdo, 
                                                   c01_w.ie_grau_severidade);
end duv_gerar_segmento_ver;
/