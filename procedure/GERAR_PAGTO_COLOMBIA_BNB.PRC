CREATE OR REPLACE 
PROCEDURE GERAR_PAGTO_COLOMBIA_BNB (
    nr_seq_envio_p      in BANCO_ESCRITURAL.NR_SEQUENCIA%type,
    nm_usuario_p        in W_ENVIO_BANCO.NM_USUARIO%type
) IS

ie_situacao_s               CONSTANT TITULO_PAGAR.IE_SITUACAO%type  := 'A';
cd_moeda_w                  TITULO_PAGAR.CD_MOEDA%type              := 1;
nr_seq_apres_w              W_ENVIO_BANCO.NR_SEQ_APRES%type         := 1;
ds_conteudo_w               varchar2(197);

cd_estab_w                  varchar2(14);
nr_client_w                 varchar2(14);
nr_debit_account_w          varchar2(10);
dt_file_emission_w          varchar2(8);
qt_file_rows_w              varchar2(6);
sum_currency_a1lowance_1_w  number(19);
sum_currency_allowance_2_w  number(19);
ds_empty_w                  varchar2(137);

cd_estabelecimento_w        varchar2(10);
ie_transaction_type_w       varchar2(2);
ie_ident_type_doc_w         varchar2(2);
nr_ident_doc_w              varchar2(15);
nm_recepient_w              varchar2(40);
dt_allowance_w              varchar2(6);
ie_currency_allowance_w     varchar2(1);
qt_allowance_w              varchar2(13);
cd_bank_w                   varchar2(4);
nr_account_w                varchar2(20);
nr_recepient_phone_w        varchar2(15);
nr_oficina_dest_transac_w   varchar2(2);
cd_refer_transaction_w      varchar2(15);
ds_transaction_w            varchar2(60);
ie_allowance_type_w         varchar2(2);

CURSOR c01 IS
    SELECT  a.CD_ESTABELECIMENTO NR_CLIENT,
            DECODE(c.IE_TIPO_PAGAMENTO, 'DOC', 1, 'CC', 2, 'CCP', 2, 'CHQ', 3, 'TED', 4, 'OP', 1)       IE_TRANSACTION_TYPE,
            SUBSTR(DECODE(d.CD_PESSOA_FISICA, null, 9, 1), 1, 2)                                        IE_IDENTIFICATION_TYPE_DOC,
            SUBSTR(DECODE(d.CD_PESSOA_FISICA, null, i.CD_RFC, g.NR_IDENTIDADE), 1, 15)                  NR_IDENTIFICATION_DOC,
            SUBSTR(DECODE(d.CD_PESSOA_FISICA, null, i.DS_RAZAO_SOCIAL, g.NM_PESSOA_FISICA), 1, 40)      NM_RECEPIENT,
            TO_CHAR(d.DT_VENCIMENTO_ATUAL, 'YYYYMM')                                                    DT_ALLOWANCE,
            SUBSTR(DECODE(d.CD_MOEDA, 21, 1, 2), 1, 1)                                                  IE_CURRENCY_ALLOWANCE,
            SUBSTR(c.VL_ESCRITURAL, 1, 13)                                                              QT_ALLOWANCE,
            SUBSTR(f.CD_BANCO_EXTERNO, 1, 4)                                                            CD_BANK,
            SUBSTR(c.NR_CONTA, 1, 20)                                                                   NR_ACCOUNT,
            SUBSTR(DECODE(d.CD_PESSOA_FISICA, null, i.NR_TELEFONE, g.NR_TELEFONE_CELULAR), 1, 15)       NR_RECEPIENT_PHONE,
            SUBSTR(DECODE(d.CD_PESSOA_FISICA, null,
                h.SG_ESTADO, (
                    SELECT MAX(SG_ESTADO) 
                    FROM   PESSOA_JURIDICA_COMPL 
                    WHERE  CD_CGC = i.CD_CGC
                )), 1, 2)                                                                               DS_OFICINA_DEST_TRANSACTION,
            SUBSTR(d.NR_NOSSO_NUMERO,1,15)                                                              CD_REFER_TRANSACTION,
            SUBSTR(NVL(d.DS_OBSERVACAO_TITULO,' '), 1, 60)                                              DS_OBS_TRANSACTION,
            DECODE(d.IE_TIPO_TITULO, 1, '10', 6, '07', 18, '08', 17, '09', 7, '11', 10, '12', 0, '10')  IE_ALLOWANCE_TYPE
    FROM    BANCO_ESTABELECIMENTO a,
            BANCO_ESCRITURAL b,
            TITULO_PAGAR_ESCRIT c,
            TITULO_PAGAR d,
            BANCO f,
            PESSOA_FISICA g,
            COMPL_PESSOA_FISICA h,
            PESSOA_JURIDICA i
    WHERE   1                   = 1
    AND     a.NR_SEQUENCIA      = b.NR_SEQ_CONTA_BANCO
    AND     a.CD_BANCO          = f.CD_BANCO
    AND     (b.CD_ESTABELECIMENTO = a.CD_ESTABELECIMENTO OR OBTER_ESTAB_FINANCEIRO(b.CD_ESTABELECIMENTO) = a.CD_ESTABELECIMENTO)
    AND     b.NR_SEQUENCIA      = c.NR_SEQ_ESCRIT
    AND     c.NR_TITULO         = d.NR_TITULO
    AND     d.CD_PESSOA_FISICA  = g.CD_PESSOA_FISICA(+)
    AND     g.CD_PESSOA_FISICA  = h.CD_PESSOA_FISICA(+)
    AND     d.CD_CGC            = i.CD_CGC(+)
    AND     d.IE_SITUACAO       = ie_situacao_s
    AND     b.NR_SEQUENCIA      = nr_seq_envio_p;

BEGIN

DELETE  FROM W_ENVIO_BANCO
WHERE   NM_USUARIO  = nm_usuario_p;

SELECT  a.CD_ESTABELECIMENTO            CD_ESTAB,
        a.NR_CONTRATO                   NR_CLIENT,
        a.CD_CONTA                      NR_ACCOUNT,
        TO_CHAR(SYSDATE, 'YYYYMMDD')    DT_FILE,
        d.CD_MOEDA,
        SUM(NVL(c.VL_ESCRITURAL, 0))    SUM_CURRENCY_ALLOWANCE_1,
        SUM(NVL(c.VL_ESCRITURAL, 0))    SUM_CURRENCY_ALLOWANCE_2,
        RPAD(' ', 136, ' ')             DS_EMPTY
INTO    cd_estab_w,
        nr_client_w,
        nr_debit_account_w,
        dt_file_emission_w,
        cd_moeda_w,
        sum_currency_a1lowance_1_w,
        sum_currency_allowance_2_w,
        ds_empty_w
FROM    BANCO_ESTABELECIMENTO a,
        BANCO_ESCRITURAL b,
        TITULO_PAGAR_ESCRIT c,
        TITULO_PAGAR d
WHERE   a.NR_SEQUENCIA          = b.NR_SEQ_CONTA_BANCO
AND     (b.CD_ESTABELECIMENTO   = a.CD_ESTABELECIMENTO OR OBTER_ESTAB_FINANCEIRO(b.CD_ESTABELECIMENTO) = a.CD_ESTABELECIMENTO)
AND     b.NR_SEQUENCIA          = c.NR_SEQ_ESCRIT
AND     c.NR_TITULO             = d.NR_TITULO
AND     d.IE_SITUACAO           = ie_situacao_s
AND     b.NR_SEQUENCIA          = nr_seq_envio_p
GROUP BY a.CD_ESTABELECIMENTO,
         a.NR_CONTRATO,
         a.CD_CONTA,
         TO_CHAR(SYSDATE, 'YYYYMMDD'),
         d.CD_MOEDA;

SELECT  COUNT(1) 
INTO    qt_file_rows_w
FROM    BANCO_ESTABELECIMENTO a,
        BANCO_ESCRITURAL b,
        TITULO_PAGAR_ESCRIT c,
        TITULO_PAGAR d
WHERE   a.NR_SEQUENCIA          = b.NR_SEQ_CONTA_BANCO
AND     (b.CD_ESTABELECIMENTO = a.CD_ESTABELECIMENTO OR OBTER_ESTAB_FINANCEIRO(b.CD_ESTABELECIMENTO) = a.CD_ESTABELECIMENTO)
AND     b.NR_SEQUENCIA          = c.NR_SEQ_ESCRIT
AND     c.NR_TITULO             = d.NR_TITULO
AND     d.IE_SITUACAO           = ie_situacao_s
AND     b.NR_SEQUENCIA          = nr_seq_envio_p;

ds_conteudo_w   :=  SUBSTR(NVL(nr_client_w, 0), 1, 14)                          ||
                    LPAD(SUBSTR(NVL(nr_debit_account_w, ' '), 1, 10), 10, '0')  ||
                    dt_file_emission_w                                          ||
                    LPAD(qt_file_rows_w, 6, '0');

IF (cd_moeda_w = 21) THEN
    ds_conteudo_w   :=  ds_conteudo_w                                                               ||
                        LPAD(SUBSTR(SOMENTE_NUMERO(sum_currency_a1lowance_1_w), 1, 13), 13, '0')    ||
                        LPAD(0, 13, '0');
ELSIF (cd_moeda_w = 3) THEN
    ds_conteudo_w   :=  ds_conteudo_w       ||
                        LPAD(0, 13, '0')    ||
                        LPAD(SUBSTR(SOMENTE_NUMERO(sum_currency_allowance_2_w), 1, 13), 13, '0');
END IF;

ds_conteudo_w   :=  ds_conteudo_w ||
                    ds_empty_w;

INSERT INTO W_ENVIO_BANCO (
    CD_ESTABELECIMENTO,
    DS_CONTEUDO,
    DT_ATUALIZACAO,
    NM_USUARIO,
    NR_SEQ_APRES,
    NR_SEQ_APRES_2,
    NR_SEQUENCIA
) VALUES (
    cd_estab_w,
    ds_conteudo_w,
    SYSDATE,
    nm_usuario_p,
    nr_seq_apres_w,
    nr_seq_apres_w,
    W_ENVIO_BANCO_SEQ.NEXTVAL
);

OPEN c01;
LOOP
FETCH c01 INTO
    cd_estabelecimento_w,
    ie_transaction_type_w,
    ie_ident_type_doc_w,
    nr_ident_doc_w,
    nm_recepient_w,
    dt_allowance_w,
    ie_currency_allowance_w,
    qt_allowance_w,
    cd_bank_w,
    nr_account_w,
    nr_recepient_phone_w,
    nr_oficina_dest_transac_w,
    cd_refer_transaction_w,
    ds_transaction_w,
    ie_allowance_type_w;
EXIT WHEN c01%notfound;

    nr_seq_apres_w  :=  NVL(nr_seq_apres_w, 0) + 1;

    ds_conteudo_w   :=  LPAD(ie_transaction_type_w, 2, '0')             ||
                        LPAD(ie_ident_type_doc_w, 2, '0')               ||
                        RPAD(nr_ident_doc_w, 15, ' ')                   ||
                        RPAD(UPPER(nm_recepient_w), 40, ' ')            ||
                        dt_allowance_w                                  ||
                        ie_currency_allowance_w                         ||
                        LPAD(SOMENTE_NUMERO(qt_allowance_w), 13, '0')   ||
                        cd_bank_w;

    IF (ie_transaction_type_w = 3) THEN
        ds_conteudo_w   :=  ds_conteudo_w || 
                            RPAD(' ', 20, ' ');
    ELSE
        ds_conteudo_w   :=  ds_conteudo_w || 
                            RPAD(NVL(nr_account_w, ' '), 20, ' ');
    END IF;

    IF (ie_transaction_type_w = 2 OR ie_transaction_type_w = 3) THEN
        ds_conteudo_w   :=  ds_conteudo_w                           ||
                            LPAD(nr_recepient_phone_w, 15, 0)       ||
                            LPAD(nr_oficina_dest_transac_w, 2, 0);
    ELSE
        ds_conteudo_w   :=  ds_conteudo_w ||
                            LPAD(0, 17, 0);
    END IF;

    ds_conteudo_w   :=  ds_conteudo_w                                   ||            
                        RPAD(NVL(cd_refer_transaction_w,' '),15,' ')    ||
                        RPAD(UPPER(ds_transaction_w),60,' ')            ||
                        LPAD(ie_allowance_type_w,2,'0');

    INSERT  INTO W_ENVIO_BANCO (
        CD_ESTABELECIMENTO,
        DS_CONTEUDO,
        DT_ATUALIZACAO,
        NM_USUARIO,
        NR_SEQ_APRES,
        NR_SEQ_APRES_2,
        NR_SEQUENCIA
    ) values (
        cd_estabelecimento_w,
        ds_conteudo_w,
        SYSDATE,
        nm_usuario_p,
        nr_seq_apres_w,
        nr_seq_apres_w,
        W_ENVIO_BANCO_SEQ.NEXTVAL
    );

END LOOP;
CLOSE c01;

COMMIT;

END GERAR_PAGTO_COLOMBIA_BNB;
/
