create or replace
procedure SCHEMATIC_CRIAR_OBJETO
			(cd_funcao_p		number,
			ie_tipo_objeto_p	varchar2,
			nr_seq_tipo_schematic_p	number,
			nm_usuario_p		varchar2,
			nr_seq_schematic_p	number,
			nr_seq_objeto_sup_p	number,
			qt_objeto_p		number default null,
			nr_seq_bo_p		number default null)
			is

nr_seq_schematic_w		number(10) := null;
nr_seq_objeto_sup_w		number(10) := null;
nr_seq_obj_fiho_w		number(10) := null;
nr_seq_objeto_w			number(10) := null;
nr_seq_tipo_schematic_w		number(10) := null;
qt_regiao_w			number(10);
cd_exp_desc_obj_w		number(10);
ie_tipo_objeto_w		varchar2(15);
ie_tipo_obj_master_regiao_w	varchar2(15);
ie_tipo_obj_navegador_w		varchar2(15);
ie_tipo_componente_w		varchar2(15);
ie_tipo_obj_painel_w		varchar2(15);
ds_tipo_objeto_w		varchar2(255);
nr_seq_apres_w			number(5);
qt_objeto_w			number(5);
cd_funcao_w			number(5);
nr_seq_bo_w			number(10);
qt_objeto_filho_w		number(10);
i				integer;
j				integer;
id_objeto_w			objeto_schematic.id_objeto%type;

cursor c01 is
select	nr_sequencia,
	ie_tipo_objeto,
	ie_tipo_obj_painel,
	ie_tipo_obj_master_regiao,
	ie_tipo_obj_navegador,     
	ie_tipo_componente,         
	substr(obter_desc_expressao(CD_EXP_DESC_OBJ,ds_objeto),1,254) ds_objeto,
	qt_objeto,
	cd_exp_desc_obj,
	ie_tipo_obj_button
from	TIPO_SCHEMATIC_OBJETO
where	nr_seq_tipo_schematic	= nr_seq_tipo_schematic_p
and	nr_seq_obj_sup		is null
order by nr_sequencia;

c01_w	c01%rowtype;

begin

cd_funcao_w	:= cd_funcao_p;
if	(nr_seq_bo_p = 0) then
	nr_seq_bo_w	:= null;
else
	nr_seq_bo_w	:= nr_seq_bo_p;
end if;

if	(nr_seq_schematic_p > 0) then
	nr_seq_schematic_w	:= nr_seq_schematic_p;
end if;
if	(nr_seq_objeto_sup_p > 0) then
	nr_seq_objeto_sup_w	:= nr_seq_objeto_sup_p;
end if;
if	(nvl(nr_seq_bo_w,0) > 0) then --se for BO, deve ficar vinculado apenas ao BO
	cd_funcao_w		:= null;
	nr_seq_schematic_w	:= null;
end if;

select	substr(obter_desc_expressao(CD_EXP_DESC_OBJ,ds_tipo_objeto),1,254),
	ie_tipo_obj_master_regiao,
	ie_tipo_obj_navegador,
	cd_exp_desc_obj,
	ie_tipo_componente,
	ie_tipo_obj_painel
into	ds_tipo_objeto_w,
	ie_tipo_obj_master_regiao_w,
	ie_tipo_obj_navegador_w,
	cd_exp_desc_obj_w,
	ie_tipo_componente_w,
	ie_tipo_obj_painel_w
from	tipo_schematic
where	nr_sequencia	= nr_seq_tipo_schematic_p;

if	(ie_tipo_objeto_p = 'C') then
	cd_exp_desc_obj_w	:= null;
end if;

select	objeto_schematic_seq.nextval
into	nr_seq_objeto_w
from	dual;

if	(nvl(nr_seq_bo_w,0) > 0) then 
	select	nvl(max(nr_seq_apres),0)
	into	nr_seq_apres_w
	from	objeto_schematic
	where	nr_seq_bo	= nr_seq_bo_w
	and	(nr_seq_objeto_sup_w is null or NR_SEQ_OBJ_SUP = nr_seq_objeto_sup_w);
	
	id_objeto_w	:= nr_seq_objeto_w;

else
	select	nvl(max(nr_seq_apres),0)
	into	nr_seq_apres_w
	from	objeto_schematic
	where	cd_funcao	= cd_funcao_w
	and	(nr_seq_objeto_sup_w is null or NR_SEQ_OBJ_SUP = nr_seq_objeto_sup_w);
end if;
	
if	(ie_tipo_obj_navegador_w = 'BCDBP') then
	nr_seq_apres_w	:= -1;
end if;

insert into objeto_schematic
	(nr_sequencia,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_atualizacao,
	nm_usuario,
	cd_funcao,
	ds_objeto,
	ie_tipo_objeto,
	ie_tipo_obj_master_regiao,
	ie_tipo_obj_navegador,
	ie_tipo_componente,
	ie_tipo_obj_painel,
	nr_seq_obj_sup,
	nr_seq_funcao_schematic,
	nr_seq_tipo_schematic,
	nr_seq_apres,
	cd_exp_desc_obj,
	nr_seq_bo,
	id_objeto)
values	(nr_seq_objeto_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_funcao_w,
	substr(ds_tipo_objeto_w,1,49),
	ie_tipo_objeto_p,
	ie_tipo_obj_master_regiao_w,
	ie_tipo_obj_navegador_w,
	ie_tipo_componente_w,
	ie_tipo_obj_painel_w,
	nr_seq_objeto_sup_w,
	nr_seq_schematic_w,
	nr_seq_tipo_schematic_p,
	nr_seq_apres_w + 1,
	cd_exp_desc_obj_w,
	nr_seq_bo_w,
	id_objeto_w);
	
if	(qt_objeto_p = 1) then
	qt_objeto_w 	:= null;
else
	qt_objeto_w	:= qt_objeto_p;
end if;


if (ie_tipo_componente_w = 'WAV') then
	INSERT INTO OBJ_SCHEMATIC_ATIV_PARAM ( NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NM_PARAMETRO, NR_SEQ_APRES, NR_SEQ_OBJ_REF, NM_ATRIBUTO_REF, NR_SEQ_ATIVACAO, NR_SEQ_OBJETO, NR_SEQ_SCHEMATIC_RELATORIO, IE_ORIGEM_PARAM, CD_FUNCAO, NR_SEQ_PARAM, NR_SEQ_OBJ_ATIVACAO, NR_SEQ_OBJ_EVT_ACAO, VL_PARAMETRO ) VALUES ( 
	OBJ_SCHEMATIC_ATIV_PARAM_seq.nextval,  sysdate, nm_usuario_p
	, sysdate, nm_usuario_p, 'NR_SEQUENCIA'
	, 1, NULL, NULL, NULL, nr_seq_objeto_w, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
	INSERT INTO OBJ_SCHEMATIC_ATIV_PARAM ( NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NM_PARAMETRO, NR_SEQ_APRES, NR_SEQ_OBJ_REF, NM_ATRIBUTO_REF, NR_SEQ_ATIVACAO, NR_SEQ_OBJETO, NR_SEQ_SCHEMATIC_RELATORIO, IE_ORIGEM_PARAM, CD_FUNCAO, NR_SEQ_PARAM, NR_SEQ_OBJ_ATIVACAO, NR_SEQ_OBJ_EVT_ACAO, VL_PARAMETRO ) VALUES ( 
	OBJ_SCHEMATIC_ATIV_PARAM_seq.nextval, sysdate, nm_usuario_p
	,  sysdate, nm_usuario_p, 'NR_SEQ_TIPO_AVALIACAO'
	, 2, NULL, NULL, NULL, nr_seq_objeto_w, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 

elsif (ie_tipo_componente_w = 'WEHR') then
	INSERT INTO OBJ_SCHEMATIC_ATIV_PARAM ( NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NM_PARAMETRO, NR_SEQ_APRES, NR_SEQ_OBJ_REF, NM_ATRIBUTO_REF, NR_SEQ_ATIVACAO, NR_SEQ_OBJETO, NR_SEQ_SCHEMATIC_RELATORIO, IE_ORIGEM_PARAM, CD_FUNCAO, NR_SEQ_PARAM, NR_SEQ_OBJ_ATIVACAO, NR_SEQ_OBJ_EVT_ACAO, VL_PARAMETRO ) VALUES ( 
	OBJ_SCHEMATIC_ATIV_PARAM_seq.nextval,  sysdate, nm_usuario_p
	, sysdate, nm_usuario_p, 'NR_SEQUENCIA'
	, 1, NULL, NULL, NULL, nr_seq_objeto_w, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
	INSERT INTO OBJ_SCHEMATIC_ATIV_PARAM ( NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NM_PARAMETRO, NR_SEQ_APRES, NR_SEQ_OBJ_REF, NM_ATRIBUTO_REF, NR_SEQ_ATIVACAO, NR_SEQ_OBJETO, NR_SEQ_SCHEMATIC_RELATORIO, IE_ORIGEM_PARAM, CD_FUNCAO, NR_SEQ_PARAM, NR_SEQ_OBJ_ATIVACAO, NR_SEQ_OBJ_EVT_ACAO, VL_PARAMETRO ) VALUES ( 
	OBJ_SCHEMATIC_ATIV_PARAM_seq.nextval, sysdate, nm_usuario_p
	,  sysdate, nm_usuario_p, 'NR_SEQ_TEMPLATE'
	, 2, NULL, NULL, NULL, nr_seq_objeto_w, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
end if;


open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
	begin

	j := 0;
	for j in 1..nvl(qt_objeto_w,c01_w.qt_objeto) loop
		begin
		
		select	objeto_schematic_seq.nextval
		into	nr_seq_obj_fiho_w
		from	dual;
		
		if	(nvl(nr_seq_bo_w,0) > 0) then
			select	nvl(max(nr_seq_apres),0)
			into	nr_seq_apres_w
			from	objeto_schematic
			where	nr_seq_bo	= nr_seq_bo_w
			and	nr_seq_obj_sup	= nr_seq_objeto_w;
			
			id_objeto_w	:= nr_seq_obj_fiho_w;
			
		else
			select	nvl(max(nr_seq_apres),0)
			into	nr_seq_apres_w
			from	objeto_schematic
			where	cd_funcao	= cd_funcao_w
			and	nr_seq_obj_sup	= nr_seq_objeto_w;
		end if;

		select	max(nr_sequencia)
		into	nr_seq_tipo_schematic_w
		from	tipo_schematic
		where	ie_tipo_objeto			= c01_w.ie_tipo_objeto
		and	(ie_tipo_obj_painel 		= c01_w.ie_tipo_obj_painel or
			ie_tipo_obj_master_regiao	= c01_w.ie_tipo_obj_master_regiao or
			ie_tipo_obj_navegador		= c01_w.ie_tipo_obj_navegador or
			ie_tipo_componente		= c01_w.ie_tipo_componente)
		and	nvl(ie_situacao,'A')	= 'A';		

		insert into objeto_schematic
			(nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			cd_funcao,
			ds_objeto,
			ie_tipo_objeto,
			ie_tipo_obj_painel,
			ie_tipo_obj_master_regiao,
			ie_tipo_obj_navegador,     
			ie_tipo_componente,
			nr_seq_obj_sup,
			nr_seq_funcao_schematic,
			nr_seq_tipo_schematic,
			nr_seq_apres,
			cd_exp_desc_obj,
			ie_tipo_obj_button,
			nr_seq_bo,
			id_objeto)
		values	(nr_seq_obj_fiho_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_funcao_w,
			c01_w.ds_objeto,
			c01_w.ie_tipo_objeto,
			c01_w.ie_tipo_obj_painel,
			c01_w.ie_tipo_obj_master_regiao,
			c01_w.ie_tipo_obj_navegador,     
			c01_w.ie_tipo_componente,
			nr_seq_objeto_w,
			nr_seq_schematic_w,
			nr_seq_tipo_schematic_w,
			nr_seq_apres_w + 1,
			c01_w.cd_exp_desc_obj,
			c01_w.ie_tipo_obj_button,
			nr_seq_bo_w,
			id_objeto_w);
			

		select	count(*)
		into	qt_objeto_filho_w
		from	tipo_schematic_objeto
		where	nr_seq_obj_sup	= c01_w.nr_sequencia;

		if	(qt_objeto_filho_w > 0) then
			SCHEMATIC_CRIAR_OBJ_FILHO(nr_seq_obj_fiho_w,c01_w.nr_sequencia,nm_usuario_p,cd_funcao_w, nr_seq_bo_w);
		end if;

		end;
	end loop;

	end;
end loop;
close C01;

commit;

end SCHEMATIC_CRIAR_OBJETO;
/