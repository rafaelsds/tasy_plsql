create or replace 
procedure pls_duplicar_regra_desconto
			(	nr_seq_contrato_p		Number,
				nr_seq_proposta_adesao_p	Number,
				nr_seq_regra_p		 	Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		number) is
				
qt_min_vidas_w			Number(10);
qt_max_vidas_w			Number(10);
tx_desconto_w			Number(7,4);	
nr_seq_regra_desc_w		Number(10);
dt_inicio_vigencia_w		Date;
ie_tipo_segurado_w		Varchar2(1);
vl_desconto_w			Number(15,2);

Cursor C01 is
	select	qt_min_vidas,
		qt_max_vidas, 
		tx_desconto,
		ie_tipo_segurado,
		vl_desconto
	from 	pls_preco_regra
	where	nr_seq_regra = nr_seq_regra_p
	order by nr_sequencia;

begin

if	(nvl(nr_seq_contrato_p,0) > 0) then
	select 	dt_contrato
	into	dt_inicio_vigencia_w
	from	pls_contrato
	where	nr_sequencia = nr_seq_contrato_p;
elsif	(nvl(nr_seq_proposta_adesao_p,0) > 0) then
	select	nvl(dt_inicio_proposta,sysdate)
	into	dt_inicio_vigencia_w
	from	pls_proposta_adesao
	where	nr_sequencia	= nr_seq_proposta_adesao_p;
end if;

select 	pls_regra_desconto_seq.nextval
into 	nr_seq_regra_desc_w
from	dual;

insert into pls_regra_desconto(	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_regra,
	nr_seq_plano,
	nr_seq_contrato,
	ie_situacao,
	dt_inicio_vigencia,
	dt_fim_vigencia,
	ie_tipo_regra,
	nr_seq_proposta,
	cd_estabelecimento)
select	nr_seq_regra_desc_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ds_regra,
	null,
	nr_seq_contrato_p,
	'A',
	dt_inicio_vigencia_w,
	null,
	ie_tipo_regra,
	nr_seq_proposta_adesao_p,
	cd_estabelecimento_p
from	pls_regra_desconto
where 	nr_sequencia	= nr_seq_regra_p;

open C01;
loop
fetch C01 into	
	qt_min_vidas_w,
	qt_max_vidas_w,
	tx_desconto_w,
	ie_tipo_segurado_w,
	vl_desconto_w;
exit when c01%notfound;
	insert into pls_preco_regra(	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		qt_min_vidas,
		qt_max_vidas,
		nr_seq_regra,
		tx_desconto,
		ie_tipo_segurado,
		vl_desconto)
	values(	pls_preco_regra_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		qt_min_vidas_w,
		qt_max_vidas_w,
		nr_seq_regra_desc_w,
		tx_desconto_w,
		ie_tipo_segurado_w,
		vl_desconto_w);
end loop;
close C01;

commit;
end pls_duplicar_regra_desconto;
/