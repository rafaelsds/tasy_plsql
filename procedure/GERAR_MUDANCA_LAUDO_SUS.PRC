create or replace
procedure Gerar_Mudanca_Laudo_Sus(	nr_seq_interno_p	number,
					nm_usuario_p		varchar2,
					nr_seq_novo_p		out number) is
nr_atendimento_w		Number(10);
nr_interno_conta_w		Number(10);
nr_Seq_apac_w			Number(10);
nr_Seq_novo_w			Number(10);


begin

select	nr_atendimento,
	nr_interno_conta
into	nr_atendimento_w,
	nr_interno_conta_w
from	sus_laudo_paciente
where	nr_seq_interno	= nr_seq_interno_p;

begin
select	nr_sequencia
into	nr_seq_apac_w
from	sus_apac_movto
where	nr_interno_conta	= nr_interno_conta_w;
exception
when others then
	nr_seq_apac_w	:= 0;
end;

Duplicar_SUS_Laudo_Paciente(nr_seq_interno_p, nr_atendimento_w, nm_usuario_p, nr_seq_novo_p);

update	sus_laudo_paciente
set	dt_cancelamento	= sysdate
where	nr_seq_interno	= nr_seq_interno_p;

update	sus_laudo_paciente
set	cd_procedimento_solic	= null,
	ie_origem_proced	= null,
	ie_origem_laudo_apac	= 1
where	nr_seq_interno		= nr_seq_novo_p;

update	procedimento_paciente
set	qt_procedimento	= 0
where	nr_interno_conta= nr_interno_conta_w;

if	(nr_seq_apac_w <> 0) then
	update	sus_apac_proc
	set	qt_procedimento	= 0
	where	nr_seq_apac	= nr_seq_apac_w;
	
	update	sus_apac_movto
	set	cd_motivo_cobranca	= 74
	where	nr_sequencia		= nr_seq_apac_w;
end if;

commit;

end Gerar_Mudanca_Laudo_Sus;
/