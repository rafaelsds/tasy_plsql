create or replace
procedure Transferir_cirurgia_Unidade
		(nr_unidade_dest_p	number,
		nr_atendimento_p	number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2) is


ie_transfere_cirurgia_w		varchar2(01);
dt_entrada_unidade_w		date;
nr_atend_destino_w		number(10,0);
nr_cirurgia_w			number(10,0);
qt_reg_proc_w			number(10,0);
qt_reg_mat_w			number(10,0);

Cursor C01 is
	select	distinct nr_cirurgia
	from	w_transf_cirurgia_atend
	where	nr_atend_origem = nr_atendimento_p
	and 	nr_atend_destino = nr_atend_destino_w
	and 	ie_transfere_cirurgia_w = 'S'
	and 	nm_usuario	 = nm_usuario_p
	order by nr_cirurgia;

begin


select	nvl(max(ie_transfere_unidade_cir),'N')
into	ie_transfere_cirurgia_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	dt_entrada_unidade,
	nr_atendimento
into	dt_entrada_unidade_w,
	nr_atend_destino_w
from	atend_paciente_unidade
where	nr_seq_interno = nr_unidade_dest_p;

if	(ie_transfere_cirurgia_w = 'S') and
	(dt_entrada_unidade_w is not null) then
	
	open C01;
	loop
	fetch C01 into	
		nr_cirurgia_w;
	exit when C01%notfound;
		begin
		
		select 	count(*)
		into	qt_reg_proc_w
		from 	procedimento_paciente
		where 	nr_atendimento = nr_atendimento_p
		and 	nr_cirurgia = nr_cirurgia_w;
		
		select 	count(*)
		into	qt_reg_mat_w
		from 	material_atend_paciente
		where 	nr_atendimento = nr_atendimento_p
		and 	nr_cirurgia = nr_cirurgia_w;
		
		if	((qt_reg_mat_w + qt_reg_proc_w) = 0) then
		
			update	cirurgia
			set	dt_entrada_unidade	= dt_entrada_unidade_w,
				nr_atendimento		= nr_atend_destino_w
			where	nr_atendimento		= nr_atendimento_p
			and 	nr_cirurgia		= nr_cirurgia_w;
			
		end if;
		
		end;
	end loop;
	close C01;
	
	delete from w_transf_cirurgia_atend
	where	nr_atend_origem = nr_atendimento_p
	and 	nr_atend_destino = nr_atend_destino_w
	and 	nm_usuario	 = nm_usuario_p;
	
	commit;
	
	update	cirurgia
	set	dt_entrada_unidade	= dt_entrada_unidade_w
	where	nr_atendimento		= nr_atendimento_p;	

end if;

commit;

end Transferir_cirurgia_Unidade;
/