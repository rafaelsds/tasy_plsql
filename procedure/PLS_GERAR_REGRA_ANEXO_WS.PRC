create or replace
procedure pls_gerar_regra_anexo_ws (	nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,
					nr_seq_requisicao_p	pls_requisicao.nr_sequencia%type,
					nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a valida��o dos itens enviados como anexo para um pedido de autoriza��o,
pedido de lote de anexo
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x }Outros:
WebService
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

-- Se existe importa��o para requisi��o o anexo sera adicionado na mesma
if	( nr_seq_requisicao_p is not null ) then
	pls_gerar_regra_anexo_req_ws( nr_seq_requisicao_p, nm_usuario_p );
else
	pls_gerar_regra_anexo_aut_ws( nr_seq_guia_p, nm_usuario_p  );
end if;

end pls_gerar_regra_anexo_ws;
/