create or replace
procedure atualizar_diagnostico_doenca(
			nr_atendimento_p	number,
			dt_diagnostico_p	Date,
			cd_doenca_p		varchar2,
			nm_usuario_p 		varchar2,
			ds_justificativa_p 	varchar2) is

ie_liberado_w	varchar2(1);
begin
select	decode(dt_liberacao,null,'N','S')
into	ie_liberado_w
from	diagnostico_doenca
where	nr_atendimento = nr_atendimento_p
and	dt_diagnostico = dt_diagnostico_p
and	cd_doenca      = cd_doenca_p;

if	(ie_liberado_w = 'N') then
	begin
	update	diagnostico_doenca 
	set	dt_liberacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_atendimento = nr_atendimento_p
	and	dt_diagnostico = dt_diagnostico_p
	and	cd_doenca      = cd_doenca_p;
	end;
else
	begin
	update	diagnostico_doenca 
	set	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		ds_justificativa = ds_justificativa_p
	where	nr_atendimento = nr_atendimento_p
	and	dt_diagnostico = dt_diagnostico_p
	and	cd_doenca      = cd_doenca_p;
	end;
end if;

commit;			

end atualizar_diagnostico_doenca;
/