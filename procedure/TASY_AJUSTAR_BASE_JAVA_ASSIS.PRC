create or replace
procedure Tasy_Ajustar_Base_Java_Assis(nm_usuario_p	varchar2) is

dt_versao_atual_cliente_w	date;
qt_registros_w			number(10,0) := 0;

begin
/*FAVOR COLOCAR OS NOVOS AJUSTES SEMPRE NO FINAL DA PROCEDURE.*/
/*AJUSTES DE BASE DO GRUPO ASSISTENCIAL JAVA */

abortar_se_base_wheb;

dt_versao_atual_cliente_w := nvl(to_date(to_char(obter_data_geracao_versao-1,'dd/mm/yyyy') ||' 23:59:59','dd/mm/yyyy hh24:mi:ss'),sysdate-90);

if	(dt_versao_atual_cliente_w < to_date('21/07/2009','dd/mm/yyyy')) then
	begin
	exec_sql_dinamico('Rafael', 'alter table  prescr_recomendacao modify (DS_RECOMENDACAO varchar2(2000))');
	end;
end if;

if	(dt_versao_atual_cliente_w < to_date('31/05/2010','dd/mm/yyyy')) then
	begin
	exec_sql_dinamico('Rafael', 'alter table PF_CURRICULO_PROF modify DS_ATIVIDADE null');
	exec_sql_dinamico('Rafael', 'alter table PF_CURRICULO_PROF modify DS_CARGO null');
	exec_sql_dinamico('Rafael', 'alter table PF_CURRICULO_PROF modify NM_EMPRESA null');
	
	delete
	from	valor_dominio
	where	cd_dominio = 3290
	and	vl_dominio not in ('O','T','TDBGrid','TDBPanel','TMenuItem','TPageControl','TPopUpMenu','TTabSheet','TWCPanel','TWDLGPanel','TWSelecaoCB');
	end;
end if;

if	(dt_versao_atual_cliente_w < to_date('12/10/2010','dd/mm/yyyy hh24:mi:ss')) then
	begin
	select	count(*)
	into	qt_registros_w
	from	user_objects
	where	object_name = 'GERAR_PREVISAO_PROJ_MIGR';
	
	if	(qt_registros_w > 0) then
		begin
		exec_sql_dinamico('Rafael', 'drop procedure GERAR_PREVISAO_PROJ_MIGR');
		end;
	end if;
	end;
end if;

if	(dt_versao_atual_cliente_w < to_date('04/05/2011','dd/mm/yyyy hh24:mi:ss')) then
	begin
	select	count(*)
	into	qt_registros_w
	from	user_objects
	where	object_name = 'AI_CONFIRMAR_AGEINT_AGFA';
	
	if	(qt_registros_w > 0) then
		begin
		exec_sql_dinamico('Rafael', 'drop procedure AI_CONFIRMAR_AGEINT_AGFA');
		end;
	end if;
	end;
end if;

if	(dt_versao_atual_cliente_w < to_date('24/05/2011','dd/mm/yyyy hh24:mi:ss')) then
	begin
	select	count(*)
	into	qt_registros_w
	from	user_objects
	where	object_name = 'PLS_OBTER_IE_COMPLEMENTAR';
	
	if	(qt_registros_w > 0) then
		begin
		exec_sql_dinamico('Rafael', 'drop function PLS_OBTER_IE_COMPLEMENTAR');
		end;
	end if;
	end;
end if;

if	(dt_versao_atual_cliente_w < to_date('27/06/2011','dd/mm/yyyy hh24:mi:ss')) then
	begin
	select	count(*)
	into	qt_registros_w
	from	user_objects
	where	object_name = 'PLS_PROPOSTA_DADOS_CONTRATO_2';
	
	if	(qt_registros_w > 0) then
		begin
		exec_sql_dinamico('Rafael', 'drop procedure PLS_PROPOSTA_DADOS_CONTRATO_2');
		end;
	end if;
	end;
end if;

end Tasy_Ajustar_Base_Java_Assis;
/