create or replace
procedure oft_insere_olho_seco (	nr_seq_consulta_p			number,
											nr_seq_consulta_form_p	number,
											vListaOlhoSeco			strRecTypeFormOft) is

nr_sequencia_w					oft_olho_seco.nr_sequencia%type;
dt_exame_w						oft_olho_seco.dt_registro%type;
ds_observacao_w				oft_olho_seco.ds_observacao%type;
vl_secrecao_oe_w				oft_olho_seco.vl_secrecao_oe%type;
vl_secrecao_od_w				oft_olho_seco.vl_secrecao_od%type;
cd_material_w					oft_olho_seco.cd_material%type;
qt_but_od_w						oft_olho_seco.qt_but_od%type;
qt_but_oe_w						oft_olho_seco.qt_but_oe%type;
ie_reacao_temporal_od_w		oft_olho_seco.ie_reacao_temporal_od%type;
ie_reacao_temporal_oe_w		oft_olho_seco.ie_reacao_temporal_oe%type;
ie_reacao_nasal_od_w			oft_olho_seco.ie_reacao_nasal_od%type;
ie_reacao_nasal_oe_w			oft_olho_seco.ie_reacao_nasal_oe%type;
ie_reacao_centro_od_w		oft_olho_seco.ie_reacao_centro_od%type;	
ie_reacao_centro_oe_w		oft_olho_seco.ie_reacao_centro_oe%type;
vl_secrecao_ii_od_w			oft_olho_seco.vl_secrecao_ii_od%type;
vl_secrecao_ii_oe_w			oft_olho_seco.vl_secrecao_ii_oe%type;
cd_profissional_w				oft_olho_seco.cd_profissional%type;	
nm_usuario_w					usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
ie_registrado_w				varchar2(1) := 'N';
ds_erro_w						varchar2(4000);
											
begin
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaOlhoSeco.count > 0) then
	for i in 1..vListaOlhoSeco.count loop
		begin
		if	(vListaOlhoSeco(i).ds_valor is not null) or 
			(vListaOlhoSeco(i).nr_valor is not null) then
			case upper(vListaOlhoSeco(i).nm_campo)
				when 'CD_PROFISSIONAL' then
					cd_profissional_w							:= vListaOlhoSeco(i).ds_valor;
				when 'DT_REGISTRO' then
					dt_exame_w 									:= pkg_date_utils.get_DateTime(vListaOlhoSeco(i).ds_valor);
				when 'DS_OBSERVACAO' then
					ds_observacao_w							:= vListaOlhoSeco(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'VL_SECRECAO_OE' then
					vl_secrecao_oe_w							:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_SECRECAO_OD' then
					vl_secrecao_od_w							:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'CD_MATERIAL' then
					cd_material_w								:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'QT_BUT_OD' then
					qt_but_od_w									:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'QT_BUT_OE' then
					qt_but_oe_w									:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'IE_REACAO_TEMPORAL_OD' then
					ie_reacao_temporal_od_w					:= vListaOlhoSeco(i).ds_valor;
					if	(ie_reacao_temporal_od_w = 'S') then
						ie_registrado_w	:= 'S';
					end if;	
				when 'IE_REACAO_TEMPORAL_OE' then
					ie_reacao_temporal_oe_w					:= vListaOlhoSeco(i).ds_valor;
					if	(ie_reacao_temporal_oe_w = 'S') then
						ie_registrado_w	:= 'S';
					end if;
				when 'IE_REACAO_NASAL_OD' then
					ie_reacao_nasal_od_w						:= vListaOlhoSeco(i).ds_valor;
					if	(ie_reacao_nasal_od_w = 'S') then
						ie_registrado_w	:= 'S';
					end if;
				when 'IE_REACAO_NASAL_OE' then
					ie_reacao_nasal_oe_w						:= vListaOlhoSeco(i).ds_valor;
					if	(ie_reacao_nasal_oe_w = 'S') then
						ie_registrado_w	:= 'S';
					end if;
				when 'IE_REACAO_CENTRO_OD' then
					ie_reacao_centro_od_w					:= vListaOlhoSeco(i).ds_valor;
					if	(ie_reacao_centro_od_w = 'S') then
						ie_registrado_w	:= 'S';
					end if;
				when 'IE_REACAO_CENTRO_OE' then
					ie_reacao_centro_oe_w					:= vListaOlhoSeco(i).ds_valor;
					if	(ie_reacao_centro_oe_w = 'S') then
						ie_registrado_w	:= 'S';
					end if;
				when 'VL_SECRECAO_II_OD' then
					vl_secrecao_ii_od_w						:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_SECRECAO_II_OE' then
					vl_secrecao_ii_oe_w						:= vListaOlhoSeco(i).nr_valor;
					ie_registrado_w							:= 'S';
				else
					null;	
			end case;	
		end if;	
	end;
	end loop;
	
	select	max(nr_sequencia)
	into		nr_sequencia_w
	from		oft_olho_seco
	where		nr_seq_consulta_form = nr_seq_consulta_form_p
	and		nr_seq_consulta		= nr_seq_consulta_p
	and		dt_liberacao 			is null
	and		nm_usuario				= nm_usuario_w;
	
	if	(nr_sequencia_w is not null) then
		update	oft_olho_seco
		set		dt_atualizacao				=	sysdate, 
					nm_usuario					=	nm_usuario_w, 
					cd_profissional			= nvl(cd_profissional_w,cd_profissional),	
					dt_registro					=	nvl(dt_exame_w,dt_registro), 
					ds_observacao				=	ds_observacao_w,
					vl_secrecao_oe				=	vl_secrecao_oe_w,	 
					vl_secrecao_od				=	vl_secrecao_od_w, 
					cd_material					=	cd_material_w, 
					qt_but_od					=	qt_but_od_w, 
					qt_but_oe					=	qt_but_oe_w, 
					ie_reacao_temporal_od	=	nvl(ie_reacao_temporal_od_w,ie_reacao_temporal_od),
					ie_reacao_temporal_oe	=	nvl(ie_reacao_temporal_oe_w,ie_reacao_temporal_oe), 
					ie_reacao_nasal_od		=	nvl(ie_reacao_nasal_od_w,ie_reacao_nasal_od), 
					ie_reacao_nasal_oe		=	nvl(ie_reacao_nasal_oe_w,ie_reacao_nasal_oe), 
					ie_reacao_centro_od		=	nvl(ie_reacao_centro_od_w,ie_reacao_centro_od) ,
					ie_reacao_centro_oe		=	nvl(ie_reacao_centro_oe_w,ie_reacao_centro_oe), 
					vl_secrecao_ii_od			=	vl_secrecao_ii_od_w, 
					vl_secrecao_ii_oe			=	vl_secrecao_ii_oe_w
		where		nr_sequencia				= 	nr_sequencia_w;
		wheb_usuario_pck.set_ie_commit('S');
	else
		if	(ie_registrado_w = 'S') then
			wheb_usuario_pck.set_ie_commit('S');
			select	oft_olho_seco_seq.nextval
			into		nr_sequencia_w	
			from		dual;

			insert	into oft_olho_seco
													(	nr_sequencia, 
														dt_atualizacao, 
														nm_usuario, 
														dt_atualizacao_nrec, 
														nm_usuario_nrec, 
														cd_profissional, 
														dt_registro,
														nr_seq_consulta, 
														ie_situacao,
														ds_observacao,
														vl_secrecao_oe, 
														vl_secrecao_od, 
														cd_material, 
														qt_but_od, 
														qt_but_oe, 
														ie_reacao_temporal_od, 
														ie_reacao_temporal_oe, 
														ie_reacao_nasal_od, 
														ie_reacao_nasal_oe, 
														ie_reacao_centro_od, 
														ie_reacao_centro_oe, 
														vl_secrecao_ii_od, 
														vl_secrecao_ii_oe,
														nr_seq_consulta_form)
			values								(	nr_sequencia_w, 
														sysdate, 
														nm_usuario_w, 
														sysdate, 
														nm_usuario_w, 
														nvl(cd_profissional_w,obter_pf_usuario(nm_usuario_w,'C')), 
														nvl(dt_exame_w,sysdate), 
														nr_seq_consulta_p,
														'A',
														ds_observacao_w,
														vl_secrecao_oe_w,
														vl_secrecao_od_w,
														cd_material_w,
														qt_but_od_w,
														qt_but_oe_w,
														nvl(ie_reacao_temporal_od_w,'N'),
														nvl(ie_reacao_temporal_oe_w,'N'),
														nvl(ie_reacao_nasal_od_w,'N'),
														nvl(ie_reacao_nasal_oe_w,'N'),
														nvl(ie_reacao_centro_od_w,'N'),
														nvl(ie_reacao_centro_oe_w,'N'),
														vl_secrecao_ii_od_w,
														vl_secrecao_ii_oe_w,
														nr_seq_consulta_form_p);
		end if;												
	end if;											
end if;	

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
	update	OFT_CONSULTA_FORMULARIO
	set		ds_stack			=	substr(dbms_utility.format_call_stack||ds_erro_w,1,4000)
	where		nr_sequencia	= 	nr_seq_consulta_form_p;
end;

end oft_insere_olho_seco;
/