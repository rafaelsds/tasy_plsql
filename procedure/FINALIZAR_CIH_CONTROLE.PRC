create or replace
procedure finalizar_cih_controle( nr_seq_controle_p		number) is 

begin

update	cih_controle
set	dt_finalizacao 	= sysdate
where	nr_sequencia 	= nr_seq_controle_p
and	dt_finalizacao is null;


update	cih_controle_atend
set	dt_finalizacao 	= sysdate
where	nr_seq_controle = nr_seq_controle_p
and	dt_finalizacao 	is null;

commit;

end finalizar_cih_controle;
/