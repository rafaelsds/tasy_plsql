create or replace
procedure pls_oc_cta_tratar_val_23_ant
		(	dados_regra_p		varchar2,
			nr_id_transacao_p	varchar2,
			nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o para gerar ocorr�ncia em itens que j� foram executados
	de forma a identificar a duplica��o de registros lan�ados em determinado per�odo.
------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

A aplica��o da valida��o se dar� contando a quantidade de itens que foram lan�adas obedecendo
a regra montada para a valida��o.  Ser� executado um comando que traga a os registros que se 
encaixaram na regra montada  e colocando os mesmos na oserva��o da ocorr�ncia para possibilitar
identificar e verificar com o prestador o lan�amento dos itens.

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjung OS 602086  03/07/2013 - Cria��o da rotina.
------------------------------------------------------------------------------------------------------------------
jjung OS 659951 - 20/10/2013 - 

Altera��o;	Implementado para trabalhar com collections para mandar os dados para a tabela de sele��o.

Motivo:	Foi identificado que a tabela sofre muita altera��o no momento de gera��o das ocorr�ncias,
	ent�o foi identificado que mandar listas para o banco com o comando FORALL e atualizar os 
	dados em um �nico acesso ao SQL seria muito menos custoso do que acessar o SQL a cada
	registro.
------------------------------------------------------------------------------------------------------------------
jjung OS 724638 - 11/04/2014 

Altera��o:	NR_FATURA da PTU_FATURA para VARCHAR2;
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
jjung OS 755826 - 03/07/2014 -

Altera��o:	Deprecia��o da rotina- N�o � mais utilizada;
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin
null;
end pls_oc_cta_tratar_val_23_ant;
/