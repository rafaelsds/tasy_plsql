create or replace
procedure inserir_tipo_gp_grupo_apap	(nr_seq_grupo_apap_p	number,
				ds_lista_tipo_gp_p		varchar2,
				nm_usuario_p		varchar2) is
					
lista_tipo_gp_w		varchar2(1000);
tam_lista_w		number(10,0);
ie_pos_virgula_w		number(3,0);
nr_seq_tipo_gp_w		number(10,0);
nr_seq_grupo_gp_w	number(10,0);
nr_seq_inf_w		number(10,0);
ie_contador_w		number(10,0) := 0;

begin
lista_tipo_gp_w	:= ds_lista_tipo_gp_p;

while 	(lista_tipo_gp_w is not null) or
	(ie_contador_w > 200) loop
	begin
	
	tam_lista_w	:= length(lista_tipo_gp_w);
	ie_pos_virgula_w	:= instr(lista_tipo_gp_w,',');
	
	if	(ie_pos_virgula_w <> 0) then
		nr_seq_tipo_gp_w	:= substr(lista_tipo_gp_w,1,(ie_pos_virgula_w - 1));
		lista_tipo_gp_w	:= substr(lista_tipo_gp_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	select	nr_seq_grupo
	into	nr_seq_grupo_gp_w
	from	tipo_perda_ganho
	where	nr_sequencia = nr_seq_tipo_gp_w;
	
	select	pep_apap_inf_seq.nextval
	into	nr_seq_inf_w
	from	dual;
	
	insert into pep_apap_inf (
		nr_sequencia,
		nr_seq_apap,
		nr_seq_grupo_apap,
		nr_seq_apres,
		ie_manter_nulo,
		ie_grafico,
		nr_seq_grupo_pg,
		nr_seq_tipo_pg,		
		dt_atualizacao_nrec,		
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario) 
	values (
		nr_seq_inf_w,
		null,
		nr_seq_grupo_apap_p,
		999,
		'S',
		'N',
		nr_seq_grupo_gp_w,
		nr_seq_tipo_gp_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);
		
	ie_contador_w	:= ie_contador_w + 1;
	
	end;
end loop;

commit;

end inserir_tipo_gp_grupo_apap;
/