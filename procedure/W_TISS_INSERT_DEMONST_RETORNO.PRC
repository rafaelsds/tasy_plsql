create or replace procedure   w_tiss_insert_demonst_retorno 
                              ( nm_usuario_p            in varchar2 ,
                                ie_limpa_tabela_p       in varchar2 ,
                                nr_seq_demonstra_ret_p  out number) as 
                                
-- IE_LIMPA_TABELA_P: 'S' -  Limpar tabela / 'N' - N�o limpar

nr_seq_demonstra_ret_w number(10); 

begin

if(ie_limpa_tabela_p = 'S') then

  delete 	from W_TISS_DEMONSTRA_RET_ITENS where	dt_atualizacao < sysdate - 1;
  commit;
  delete 	from W_TISS_DEMONSTRA_RETORNO 	where	dt_atualizacao < sysdate - 1;
  commit;
  
end if;

select  W_TISS_DEMONSTRA_RETORNO_SEQ.nextval
into    nr_seq_demonstra_ret_w
from    dual;

insert into W_TISS_DEMONSTRA_RETORNO
            ( dt_atualizacao,
              nm_usuario,
              nr_sequencia) values (
              sysdate,
              nm_usuario_p,
              nr_seq_demonstra_ret_w  );
              
nr_seq_demonstra_ret_p := nr_seq_demonstra_ret_w;
              
commit;
                
end w_tiss_insert_demonst_retorno;
/