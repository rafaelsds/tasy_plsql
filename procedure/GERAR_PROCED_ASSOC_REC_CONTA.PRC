create or replace
procedure Gerar_proced_assoc_rec_conta( nr_prescricao_p			number,
										nr_seq_recomendacao_p	number,
										cd_tipo_recomendacao_p	number,
										nm_usuario_p			varchar2,
										nr_seq_horario_p		number,
										ie_gerar_estorn_p		varchar2) is 

cd_procedimento_w		number(15,0);
ie_orig_proced_w		number(10,0);
qt_procedimento_w		number(15,4);
nr_seq_exame_w			number(10,0);
ds_observacao_w			varchar2(255);
cd_setor_atendimento_w	number(5,0);
nr_agrup_acum_w			number(10,0);
nr_seq_acum_w			number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_atendimento_w		number(15,0);
cont_w					number(15,0);
nr_horas_validade_w		number(15,0);
ie_cobrar_intervalo_w	varchar2(10);
ie_cobrar_horario_w		varchar2(10);
ie_considerar_dia_w		varchar2(10);
ie_cobrar_internacao_w	varchar2(1);
dt_prescricao_w			date;
cd_material_exame_w		varchar2(20);
cd_intervalo_w			varchar2(7);
ds_horarios_w			varchar2(2000);
nr_ocorrencia_w			number(15,4);
nr_ocorrencia_ww		prescr_recomendacao.nr_ocorrencia%type;
cd_convenio_w			number(10);
cd_estabelecimento_w	number(10);
ie_conta_rep_w			varchar2(1);
ie_quant_validade_w		varchar2(1);
ie_tipo_convenio_w		number(2);
cd_setor_atendimento_ww	setor_atendimento.cd_setor_atendimento%type;
qt_total_proc_w			Number(15,4);

cursor c01 is
select	cd_procedimento,
		ie_origem_proced,
		qt_procedimento,
		nr_seq_exame,
		cd_material_exame,
		ds_observacao,
		cd_setor_atendimento,
		nr_seq_proc_interno,
		nvl(cd_intervalo,cd_intervalo_w),
		ie_considerar_dia,
		ie_cobrar_intervalo,
		ie_cobrar_internacao,
		nvl(a.ie_conta_rep,'P') ie_conta_rep,
		nvl(a.ie_quantidade_validade, 'N') ie_quantidade_validade
from	regra_proced_recomendacao a
where	(((nvl(a.ie_conta_rep,'P')	= 'C') and (nr_seq_horario_p is not null)) or
		 ((nvl(a.ie_conta_rep,'P')	= 'T') and (nr_seq_horario_p is null)))
and		((cd_estab is null) or (cd_estab = cd_estabelecimento_w))
and		((a.cd_convenio is null) or (a.cd_convenio = cd_convenio_w))
and		((a.ie_tipo_convenio is null) or (a.ie_tipo_convenio = ie_tipo_convenio_w))
and		((a.cd_setor_regra is null) or (a.cd_setor_regra = cd_setor_atendimento_w))
and		a.cd_tipo_recomendacao	= cd_tipo_recomendacao_p
and		((a.cd_intervalo is null) or (a.cd_intervalo = cd_intervalo_w));

begin

select	max(dt_prescricao),
		max(cd_estabelecimento),
		max(nr_atendimento),
		max(nr_horas_validade),
		max(obter_convenio_atendimento(nr_atendimento)),
		max(cd_estabelecimento),
		max(obter_tipo_convenio_atend(nr_atendimento)),
		max(cd_setor_atendimento)
into	dt_prescricao_w,
		cd_estabelecimento_w,
		nr_atendimento_w,
		nr_horas_validade_w,
		cd_convenio_w,
		cd_estabelecimento_w,
		ie_tipo_convenio_w,
		cd_setor_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	max(cd_intervalo),
		max(ds_horarios),
		obter_ocorrencias_horarios_rep(max(ds_horarios))
into	cd_intervalo_w,
		ds_horarios_w,
		nr_ocorrencia_ww
from	prescr_recomendacao
where	cd_recomendacao	= cd_tipo_recomendacao_p
and		nr_sequencia	= nr_seq_recomendacao_p
and		nr_prescricao	= nr_prescricao_p;

open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_orig_proced_w,
	qt_procedimento_w,
	nr_seq_exame_w,
	cd_material_exame_w,
	ds_observacao_w,
	cd_setor_atendimento_ww,
	nr_seq_proc_interno_w,
	cd_intervalo_w,
	ie_considerar_dia_w,
	ie_cobrar_intervalo_w,
	ie_cobrar_internacao_w,
	ie_conta_rep_w,
	ie_quant_validade_w;
exit when C01%notfound;

	
	if	(ie_conta_rep_w = 'T') and
		(ie_quant_validade_w = 'S') and
		(nr_horas_validade_w is not null) then
		qt_procedimento_w	:= nr_horas_validade_w;
	end if;
	
	select	nvl(max(nr_sequencia),0),
			nvl(max(nr_agrupamento),0)	
	into	nr_seq_acum_w,
			nr_agrup_acum_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p;
	
	if	(nr_seq_proc_interno_w is not null) then
		Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,nr_prescricao_p,nr_atendimento_w, 0, cd_procedimento_w, ie_orig_proced_w,null,null);
	end if;
	
	select	count(1)
	into	qt_total_proc_w
	from	prescr_rec_hor a
	where	(	select	nvl(sum(qt_procedimento),0)
				from	procedimento_paciente
				where	1 = 1
				and		nr_seq_proc_interno	= nr_seq_proc_interno_w
				and		cd_procedimento		= cd_procedimento_w
				and		ie_origem_proced	= ie_orig_proced_w
				and		nr_prescricao		= nr_prescricao_p) > 0
	and		dt_fim_horario is not null
	and		nr_sequencia		<> nr_seq_horario_p
	and		nr_seq_recomendacao 	= nr_seq_recomendacao_p
	and		nr_prescricao		= nr_prescricao_p;
	
	if	(nvl(nr_seq_proc_interno_w,0) > 0) then
		select	nvl(max(ie_cobrar_horario),'N')
		into	ie_cobrar_horario_w
		from	proc_interno
		where	nr_sequencia	= nr_seq_proc_interno_w;
		
		if	(ie_cobrar_horario_w = 'N') then
			qt_total_proc_w := 0;
		end if;
	end if;
	
	if	(ie_considerar_dia_w = 'S') and
		(qt_total_proc_w = 0) then 
		
		select	nvl(sum(qt_procedimento),0)
		into	qt_total_proc_w
		from	procedimento_paciente a,
				conta_paciente b
		where	a.nr_interno_conta	= b.nr_interno_conta
		and		a.cd_procedimento	= cd_procedimento_w
		and		b.nr_atendimento	= nr_atendimento_w
		and		pkg_date_utils.start_of(a.dt_conta,'DD',0)	= pkg_date_utils.start_of(sysdate,'DD',0)
		and		a.ie_origem_proced	= ie_orig_proced_w;
		
	end if;
	
	if	(ie_cobrar_internacao_w = 'S') and
		(qt_total_proc_w = 0) then 
	
		select	nvl(sum(qt_procedimento),0)
		into	qt_total_proc_w
		from	procedimento_paciente a,
				conta_paciente b
		where	a.nr_interno_conta	= b.nr_interno_conta
		and		b.nr_atendimento	= nr_atendimento_w
		and		a.cd_procedimento	= cd_procedimento_w
		and		a.ie_origem_proced	= ie_orig_proced_w;
		
	end if;
		
	if	(ie_cobrar_intervalo_w = 'S') then
		nr_ocorrencia_w		:= nvl(nr_ocorrencia_ww,Obter_ocorrencia_intervalo(cd_intervalo_w, nr_horas_validade_w, 'O'));
		qt_procedimento_w	:= qt_procedimento_w * nvl(nr_ocorrencia_w,1);
	end if;
		
	if	((qt_total_proc_w = 0) and (ie_gerar_estorn_p <> 'E')) then		
		gerar_proc_pac_item_prescr(nr_prescricao_p, null, null,null,nr_seq_proc_interno_w, cd_procedimento_w, ie_orig_proced_w, qt_procedimento_w, cd_setor_atendimento_ww,1, SYSDATE, nm_usuario_p, NULL, nr_seq_exame_w, NULL, null);
	elsif ((qt_total_proc_w > 0) and (ie_gerar_estorn_p = 'E')) then		
		qt_procedimento_w	:= qt_procedimento_w * -1;
		gerar_proc_pac_item_prescr(nr_prescricao_p, null, null,null,nr_seq_proc_interno_w, cd_procedimento_w, ie_orig_proced_w, qt_procedimento_w, cd_setor_atendimento_ww,1, SYSDATE, nm_usuario_p, NULL, nr_seq_exame_w, NULL, null);	
	end if;
	
end loop;
close C01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_proced_assoc_rec_conta;
/