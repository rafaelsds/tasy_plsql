create or replace
procedure pls_vincular_perito
			(	nr_seq_analise_p	Number,
				nm_usuario_p		Varchar2,
				nr_seq_perito_p		number,
				dt_atendimento_p	date) is 
				
ie_horario_valido_w		varchar2(10);
				
begin

/*aaschlote 24/04/2012 OS- 438903*/
if	(nr_seq_perito_p is not null) then
	pls_consistir_horarios_perito(nr_seq_perito_p,dt_atendimento_p,ie_horario_valido_w);

	if	(ie_horario_valido_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(117930,'');
	end if;
end if;

update	pls_analise_adesao
set	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_atendimento	= dt_atendimento_p,
	nr_seq_perito	= nr_seq_perito_p
where	nr_sequencia	= nr_seq_analise_p;

commit;

end pls_vincular_perito;
/
