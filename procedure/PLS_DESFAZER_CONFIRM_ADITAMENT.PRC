create or replace
procedure pls_desfazer_confirm_aditament
			(
				nr_seq_contrato_p Number,
				dt_confirmacao_p Date,
				nm_usuario_p  Varchar2,
				nr_seq_aditamento_p Number) is

begin

update	pls_contrato_aditamento
set	dt_aditamento		= '',
	ie_status_aditamento	= 'P',
	nm_usuario_confirmacao	= '',
	dt_confirmacao		= '',
	dt_fim_aditamento	= ''
where	nr_seq_contrato		= nr_seq_contrato_p
and	ie_status_aditamento	= 'A'
and	nr_sequencia		= nr_seq_aditamento_p;

commit;

end pls_desfazer_confirm_aditament;
/