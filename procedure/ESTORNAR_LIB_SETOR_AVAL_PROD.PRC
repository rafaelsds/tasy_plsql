create or replace
procedure estornar_lib_setor_aval_prod(
				nr_seq_mat_aval_p	number,
				nm_usuario_p		varchar2) is 

begin

update	mat_avaliacao
set	dt_liberacao_setor 	= null
where	nr_sequencia 		= nr_seq_mat_aval_p; 

commit;

end estornar_lib_setor_aval_prod;
/