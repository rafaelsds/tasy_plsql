create or replace
procedure baixa_saldo_disp_ap_lote(
				nr_prescricao_p		number,
				cd_material_p		number,
				qt_material_p		number,
				nr_seq_lote_p		number,
				nr_seq_lote_fornec_p	number,
				cd_local_estoque_p	number,
				cd_tipo_baixa_p		number,
				cd_estabelecimento_p	number,
				nr_seq_material_p	number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p	out	varchar2) is 

ds_erro_w			varchar2(2000);
ds_fornec_prescrito_w		varchar2(255);
ds_fornecedor_lote_w		varchar2(255);
cd_cgc_fornec_lote_w		varchar2(14);
cd_cgc_fornec_w			varchar2(14);
ie_mat_suspenso_w		varchar2(1);
ie_consiste_baixa_w		varchar2(1);
ie_estoque_lote_w		varchar2(1);
ie_material_estoque_w		varchar2(1);
ie_consiste_lote_fornec_dif_w	varchar2(1);
ie_local_valido_w		varchar2(1);
ie_consignado_w			varchar2(1);
ie_atualiza_estoque_w		varchar2(1);
ie_consiste_saldo_w		varchar2(1);
ie_estoque_disp_w		varchar2(1);
ie_consiste_saldo_lote_w	varchar2(1);
ie_saldo_estoque_w		varchar2(1);
qt_dispensar_w			number(15,2);
qt_material_w			number(15,2);
qt_saldo_lote_fornec_w		number(13,4);
qt_existe_lote_fornec_w		number(10);
qt_registro_w			number(10);
cd_local_estoque_w		number(4);
cd_motivo_baixa_w		number(3);
nr_sequencia_w			number(10);

/* ie_opcao_p
C - Consiste baixa item
B - Baixa saldo disp */

begin
cd_local_estoque_w := cd_local_estoque_p;
ds_erro_w := '';

if	(ie_opcao_p = 'C') then
	obter_param_usuario(7029,45,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_mat_suspenso_w);
	obter_param_usuario(7029,58,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_saldo_w);
	obter_param_usuario(7029,59,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_saldo_lote_w);
	obter_param_usuario(7029,84,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_lote_fornec_dif_w);
	obter_param_usuario(7029,87,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_baixa_w);

	select	max(ie_atualiza_estoque)
	into	ie_atualiza_estoque_W
	from	ap_lote
	where	nr_sequencia = nr_seq_lote_p;

	if	(ie_atualiza_estoque_w is null) then
		select	nvl(max(ie_atualiza_estoque),'S')
		into	ie_atualiza_estoque_w
		from	tipo_baixa_prescricao
		where	ie_prescricao_devolucao = 'P'
		and 	cd_tipo_baixa = cd_tipo_baixa_p
		and 	ie_situacao = 'A';
	end if;

	if	(ds_erro_w is null) and
		(nvl(ie_mat_suspenso_w,'N') = 'S') then
		consiste_ap_lote_suspenso(nr_seq_lote_p,cd_material_p,qt_material_p,ds_erro_w);
	end if;

	if	(nvl(ie_consiste_baixa_w,'N') = 'S') then
		select	nvl(max(a.cd_motivo_baixa),0)
		into	cd_motivo_baixa_w
		from	prescr_mat_hor c,
			material b,
			prescr_material a
		where	a.nr_prescricao	= nr_prescricao_p
		and	a.cd_material = cd_material_p
		and	c.nr_seq_lote = nr_seq_lote_p
		and	a.cd_material = b.cd_material
		and	a.nr_prescricao = c.nr_prescricao
		and	a.cd_material = c.cd_material
		and	a.nr_sequencia = c.nr_seq_material
		and	a.ie_origem_inf	<> 'A'
		and	b.ie_tipo_material <> 6 -- COnforme solicitado pelo Daniel Dalcastagne foi tratado para quando for sem apresenta��o n�o consistir
		and	((a.ie_acm = 'N') and (a.ie_se_necessario = 'N'));
		
		if	(cd_motivo_baixa_w <> 0) then
			ds_erro_w := ds_erro_w || ' - ' || WHEB_MENSAGEM_PCK.get_texto(277417) || cd_material_p || '.' || chr(13) || chr(10) ||
				     WHEB_MENSAGEM_PCK.get_texto(277418);
		end if;
	end if;

	-- Estoque lote sem lote fornec informado
	select	nvl(max(ie_estoque_lote),'N'),
		nvl(max(ie_material_estoque),'N')
	into	ie_estoque_lote_w,
		ie_material_estoque_w
	from	material_estab
	where	cd_material = cd_material_p
	and	cd_estabelecimento = cd_estabelecimento_p;

	if	(ds_erro_w is null) and
		(ie_estoque_lote_w = 'S') and
		(nr_seq_lote_fornec_p = 0) and
		(qt_material_p > 0) then
		ds_erro_w := ds_erro_w || ' - ' || WHEB_MENSAGEM_PCK.get_texto(277420) || cd_material_p || ')';
	end if;
	-- Estoque lote sem lote fornec informado

	/*Consiste  se o lote fonecedor prescrito � igual ao lote fornecedor  bipado*/
	if	(ie_consiste_lote_fornec_dif_w = 'S') and
		(nvl(nr_seq_lote_fornec_p, 0) > 0) and
		(ds_erro_w is null) then

		select	nvl(ie_consignado,'0')
		into	ie_consignado_w
		from	material
		where	cd_material = cd_material_p;

		if	(ie_consignado_w = '1') then
			select	cd_cgc_fornec
			into	cd_cgc_fornec_lote_w
			from	material_lote_fornec
			where	nr_sequencia = nr_seq_lote_fornec_p;
			
			select  count(*)
			into	qt_existe_lote_fornec_w
			from 	prescr_material a,
				prescr_mat_hor b
			where 	a.nr_sequencia = b.nr_seq_material
			and 	a.nr_prescricao = b.nr_prescricao
			and	a.nr_prescricao = nr_prescricao_p
			and 	a.cd_material = cd_material_p
			and	b.nr_seq_lote = nr_seq_lote_p
			and 	a.cd_fornec_consignado = cd_cgc_fornec_lote_w;
			
			if	(qt_existe_lote_fornec_w = 0) then
				
				select  obter_dados_pf_pj(null,a.cd_fornec_consignado,'N'),
					obter_dados_pf_pj(null,cd_cgc_fornec_lote_w,'N')
				into	ds_fornec_prescrito_w,
					ds_fornecedor_lote_w
				from 	prescr_material a,
					prescr_mat_hor b
				where 	a.nr_sequencia = b.nr_seq_material
				and 	a.nr_prescricao = b.nr_prescricao
				and	a.nr_prescricao = nr_prescricao_p
				and 	a.cd_material = cd_material_p
				and	b.nr_seq_lote = nr_seq_lote_p;
				
				ds_erro_w := ds_erro_w || ' - ' || WHEB_MENSAGEM_PCK.get_texto(277422) || ds_fornec_prescrito_w || chr(13) || chr(10) ||
					     WHEB_MENSAGEM_PCK.get_texto(277423) || ds_fornecedor_lote_w;
				
			end if;
		end if;
	end if;

	select	nvl(ie_consignado,'0')
	into	ie_consignado_w
	from	material
	where	cd_material = cd_material_p;

	if	(ie_consignado_w <> '0') then
		begin
		if	(nvl(nr_seq_lote_fornec_p, 0) > 0) then
			select	cd_cgc_fornec
			into	cd_cgc_fornec_w
			from	material_lote_fornec
			where	nr_sequencia = nr_seq_lote_fornec_p;
		else
			cd_cgc_fornec_w	:= obter_fornecedor_regra_consig(
						cd_estabelecimento_p,
						cd_material_p,
						'1');
		end if;
		end;
	end if;

	obter_local_valido(cd_estabelecimento_p,cd_local_estoque_w,cd_material_p,null,ie_local_valido_w);
	obter_disp_estoque(cd_material_p,cd_local_estoque_w,cd_estabelecimento_p,0,qt_material_p,cd_cgc_fornec_w,ie_saldo_estoque_w);

	if	(nvl(nr_seq_lote_fornec_p, 0) > 0) and (ie_consiste_saldo_lote_w = 'S') and (ie_material_estoque_w = 'S') and (ie_estoque_lote_w = 'N') then
		qt_saldo_lote_fornec_w	:= obter_saldo_lote_fornec(nr_seq_lote_fornec_p);
		
		ie_consiste_saldo_lote_w := 'S';
		if	(qt_saldo_lote_fornec_w > 0) and (qt_saldo_lote_fornec_w >= qt_material_p) then
			ie_consiste_saldo_lote_w := 'N';
		end if;
	elsif	(nvl(nr_seq_lote_fornec_p, 0) > 0) and (ie_material_estoque_w = 'S') and (ie_estoque_lote_w = 'S') then
		qt_saldo_lote_fornec_w	:= obter_saldo_lote_fornec_local(nr_seq_lote_fornec_p,cd_local_estoque_w);
		
		ie_saldo_estoque_w := 'N';
		if	(qt_saldo_lote_fornec_w > 0) and (qt_saldo_lote_fornec_w >= qt_material_p) then
			ie_saldo_estoque_w := 'S';
		end if;
	end if;

	if	(ie_saldo_estoque_w = 'S') or (nvl(ie_atualiza_estoque_w,'S') = 'N') then
		ie_estoque_disp_w:= 'S';
	else
		ie_estoque_disp_w:= 'N';
	end if;

	if	(nvl(ie_atualiza_estoque_w,'S') = 'S') then
		begin
		if	((ie_local_valido_w <> 'S') and (ie_material_estoque_w = 'S')) then
			ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(277427);
		elsif	((ie_consiste_saldo_w = 'S') and (ie_estoque_disp_w <> 'S') and (ie_material_estoque_w = 'S')) then
			ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(277428);
		elsif	(ie_consiste_saldo_lote_w = 'S') and (nvl(nr_seq_lote_fornec_p,0) > 0) and
			(nvl(qt_saldo_lote_fornec_w,0) <= 0) and (ie_material_estoque_w = 'S') and (ie_estoque_lote_w = 'N') then
			ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(277430);
			enviar_comunic_consumo_lote(	nr_seq_lote_fornec_p,
							qt_saldo_lote_fornec_w,
							qt_material_p,
							cd_local_estoque_p,
							nr_seq_lote_p,
							cd_estabelecimento_p,
							nm_usuario_p);
		end if;
		end;
	end if;
elsif	(ie_opcao_p = 'B') then
	/* ie_status
	C - Consiste baixa item
	B - Baixa saldo disp */
	select	ap_lote_item_consistido_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into ap_lote_item_consistido
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_material,
		qt_material,
		nr_lote_fornec,
		nr_lote_ap,
		nr_seq_material,
		nr_prescricao,
		cd_local_estoque,
		ie_status)
	values	(nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		cd_material_p,
		qt_material_p,
		decode(nr_seq_lote_fornec_p,0,null,nr_seq_lote_fornec_p),
		nr_seq_lote_p,
		nr_seq_material_p,
		nr_prescricao_p,
		cd_local_estoque_p,
		'C');
	
	select	sum(a.qt_dispensar)
	into	qt_dispensar_w
	from	prescr_mat_hor b,
		ap_lote_item a,
		ap_lote c
	where	b.nr_seq_material = nr_seq_material_p
	and	a.cd_material = cd_material_p
	and	a.nr_seq_lote = nr_seq_lote_p
	and	c.nr_sequencia = a.nr_seq_lote
	and	a.nr_seq_mat_hor = b.nr_sequencia
	and	b.dt_suspensao is null
	and	a.dt_supensao is null
	and	c.ie_status_lote = 'G'
	and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
	group by a.cd_material,
		b.nr_seq_material
	having sum(a.qt_dispensar) > 0;
	
	select	sum(qt_material)
	into	qt_material_w
	from	ap_lote_item_consistido
	where	nr_seq_material = nr_seq_material_p
	and	cd_material = cd_material_p
	and	nr_lote_ap = nr_seq_lote_p;
	
	if	(qt_material_w >= qt_dispensar_w) then
		update	ap_lote_item
		set	ie_consistido = 'S'
		where	nr_seq_lote = nr_seq_lote_p
		and	nr_seq_mat_hor in (	select	nr_sequencia
						from	prescr_mat_hor
						where	nr_seq_lote = nr_seq_lote_p
						and	nr_seq_material = nr_seq_material_p);
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	ap_lote_item
	where	nr_seq_lote = nr_seq_lote_p
	and	qt_dispensar > 0
	and	nvl(ie_consistido,'N') <> 'S';
	
	if	(qt_registro_w = 0) then
		update	ap_lote
		set	ie_status_lote = 'CO'
		where	nr_sequencia = nr_seq_lote_p;
	end if;
	
	commit;
elsif	(ie_opcao_p = 'E') then
	begin
	delete	ap_lote_item_consistido
	where	nr_lote_ap = nr_seq_lote_p;
	
	update	ap_lote_item
	set	ie_consistido = 'N'
	where	nr_seq_lote = nr_seq_lote_p;
	
	update	ap_lote
	set	ie_status_lote = 'G'
	where	nr_sequencia = nr_seq_lote_p;
	
	commit;
	exception
	when others then
		ds_erro_w := '';
	end;
end if;

ds_erro_p := ds_erro_w;

end baixa_saldo_disp_ap_lote;
/