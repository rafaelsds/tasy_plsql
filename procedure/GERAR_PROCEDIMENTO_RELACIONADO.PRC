create or replace 
procedure gerar_procedimento_relacionado(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, 
                                         nm_usuario_p in usuario.nm_usuario%type) is

nr_seq_procedimento_w   procedimento_paciente.nr_sequencia%type;
cd_procedimento_w       procedimento_paciente.cd_procedimento%type;
nr_seq_exame_w          procedimento_paciente.nr_seq_exame%type;
nr_seq_proc_interno_w   procedimento_paciente.nr_seq_proc_interno%type;
ie_origem_proced_w      procedimento_paciente.ie_origem_proced%type;
cd_setor_atendimento_w  procedimento_paciente.cd_setor_atendimento%type;
nr_atendimento_w        procedimento_paciente.nr_atendimento%type;
cd_medico_w             procedimento_paciente.cd_medico%type;
nr_seq_atepacu_w        procedimento_paciente.nr_seq_atepacu%type;
cd_convenio_w           procedimento_paciente.cd_convenio%type;
cd_categoria_w          procedimento_paciente.cd_categoria%type;
cd_estabelecimento_w    conta_paciente.cd_estabelecimento%type;
nr_seq_regra_excl_w     regra_exclusao_proc.nr_sequencia%type;
cd_motivo_exc_conta_w   regra_exclusao_proc.cd_motivo_exc_conta%type;
ie_proced_exists_w      number := 0;
qt_ocorrencias_w        number := 0;
ds_expressao_w          varchar(255);  
nr_seq_new_proced_w     procedimento_paciente.nr_sequencia%type;
cd_medico_req_w	 	procedimento_paciente.cd_medico_req%type := null;
cd_medico_executor_w	procedimento_paciente.cd_medico_executor%type := null;
vl_procedimento_w       procedimento_paciente.vl_procedimento%type := null;
vl_custo_operacional_w  procedimento_paciente.vl_custo_operacional%type := null;
vl_medico_w             procedimento_paciente.vl_medico%type := null;
vl_materiais_w          procedimento_paciente.vl_materiais%type := null;
vl_anestesista_w        procedimento_paciente.vl_anestesista%type := null;
vl_auxiliares_w         procedimento_paciente.vl_auxiliares%type := null;

cursor c01 is
	select nr_sequencia
	from   procedimento_paciente
	where  nr_interno_conta = nr_interno_conta_p;
begin

open c01;
loop
	fetch c01  
	into  nr_seq_procedimento_w;
	exit when c01%notfound;
	begin
		select  max(c.cd_convenio_parametro), 
			max(c.nr_atendimento)
		into    cd_convenio_w,
			nr_atendimento_w
		from    regra_proc_relacionado a, 
			procedimento_paciente b, 
			conta_paciente c,
			regra_exclusao_proc d
		where   a.cd_procedimento = b.cd_procedimento
		and     a.ie_origem_proced = b.ie_origem_proced
		and     c.nr_interno_conta = b.nr_interno_conta 
		and     c.cd_convenio_parametro = d.cd_convenio
		and     a.nr_seq_regra_excl = d.nr_sequencia
		and     b.cd_motivo_exc_conta is null
		and     b.nr_sequencia = nr_seq_procedimento_w
		and     a.ie_situacao = 'A';
		
		select  nr_sequencia,
			cd_motivo_exc_conta
		into    nr_seq_regra_excl_w,
			cd_motivo_exc_conta_w
		from    regra_exclusao_proc a
		where   a.ie_situacao = 'A'
		and     a.cd_convenio = cd_convenio_w;
	exception
		when no_data_found then nr_seq_regra_excl_w := 0;
	end;

	if (nr_seq_regra_excl_w > 0) then
		begin
			select max(qt_procedimento)
			into qt_ocorrencias_w
			from ( 
				select 	count(1) qt_procedimento,
					cd_procedimento  
				from (
					select  c.cd_procedimento
					from    procedimento_paciente a,
						conta_paciente b,
						regra_proc_relacionado c,
						regra_exclusao_proc d
					where   a.nr_interno_conta = b.nr_interno_conta
					and     c.cd_procedimento = a.cd_procedimento
					and     c.ie_origem_proced = a.ie_origem_proced
					and     c.nr_seq_regra_excl = d.nr_sequencia
					and     b.cd_convenio_parametro = d.cd_convenio
					and     a.cd_motivo_exc_conta is null
					and     b.cd_convenio_parametro = cd_convenio_w
					and     b.nr_atendimento = nr_atendimento_w
					and     c.ie_situacao = 'A'
					and     c.nr_seq_regra_excl = nr_seq_regra_excl_w
					union all
					select  a.cd_procedimento
					from    regra_proc_relacionado a
					where   a.nr_seq_regra_excl = nr_seq_regra_excl_w
					and     a.ie_situacao = 'A'
				) group by cd_procedimento
			) group by qt_procedimento
			  having qt_procedimento = 1;
		exception
			when no_data_found then qt_ocorrencias_w := 0;
		end;

		if (qt_ocorrencias_w = 0) then 
			begin
				begin
					select  count(max(a.cd_procedimento)) 
					into    ie_proced_exists_w
					from    procedimento_paciente a,
						conta_paciente b,
						regra_exclusao_proc c
					where   a.nr_interno_conta = b.nr_interno_conta
					and     c.cd_procedimento = a.cd_procedimento
					and     c.ie_origem_proced = a.ie_origem_proced
					and     b.nr_atendimento = nr_atendimento_w
					and     a.cd_motivo_exc_conta is null
					and     c.nr_sequencia = nr_seq_regra_excl_w
					and     c.ie_situacao = 'A'
					group by a.cd_procedimento;
				exception
					when no_data_found then ie_proced_exists_w := 0;
				end;

				select  a.cd_procedimento,
					b.nr_seq_exame,
					b.nr_seq_proc_interno,
					a.ie_origem_proced,
					b.cd_setor_atendimento,
					b.nr_atendimento,
					c.cd_estabelecimento,
					b.cd_medico,
					b.nr_seq_atepacu,
					b.cd_convenio,
					b.cd_categoria
				into    cd_procedimento_w,
					nr_seq_exame_w,
					nr_seq_proc_interno_w,
					ie_origem_proced_w,
					cd_setor_atendimento_w,
					nr_atendimento_w,
					cd_estabelecimento_w,
					cd_medico_w,
					nr_seq_atepacu_w,
					cd_convenio_w,
					cd_categoria_w
				from    regra_exclusao_proc a,
					procedimento_paciente b,
				        conta_paciente c
				where   a.cd_convenio = b.cd_convenio
				and     a.ie_origem_proced = b.ie_origem_proced
				and     b.nr_interno_conta = c.nr_interno_conta
				and     b.cd_motivo_exc_conta is null
				and     b.nr_sequencia = nr_seq_procedimento_w
				and     a.nr_sequencia = nr_seq_regra_excl_w
				and     a.ie_situacao = 'A';

				if (ie_proced_exists_w = 0) then
					begin
						ds_expressao_w := substr(obter_desc_expressao(1038546) || ' ' || nr_seq_regra_excl_w, 1, 255);

						execute immediate 'begin inserir_procedimento_paciente(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26); end;'
											using cd_procedimento_w,
												  1,
												  nr_seq_exame_w,
												  nr_seq_proc_interno_w,
												  ie_origem_proced_w,
												  cd_setor_atendimento_w,
												  nr_atendimento_w,
												  cd_estabelecimento_w,
												  nm_usuario_p,
												  '',
												  'N',
												  cd_medico_w,
												  nr_seq_atepacu_w,
												  sysdate,
												  cd_convenio_w,
												  cd_categoria_w,
												  out nr_seq_new_proced_w, 
												  cd_medico_req_w,
												  cd_medico_executor_w, 
												  vl_procedimento_w, 
												  vl_custo_operacional_w,
												  vl_medico_w,
												  vl_materiais_w,
												  vl_anestesista_w,
												  vl_auxiliares_w,
												  ds_expressao_w;		
					
						if (cd_motivo_exc_conta_w is not null) then
							begin
								ds_expressao_w := substr(obter_desc_expressao(1038554) || ' ' || nr_seq_regra_excl_w, 1, 255);

								update  procedimento_paciente a
								set     a.cd_motivo_exc_conta = cd_motivo_exc_conta_w,
									a.ds_observacao = ds_expressao_w
								where   a.nr_sequencia in (select a.nr_sequencia
											   from   procedimento_paciente a,
												  conta_paciente b,
												  regra_proc_relacionado c,
												  regra_exclusao_proc d
											   where  a.nr_interno_conta = b.nr_interno_conta
											   and    c.cd_procedimento = a.cd_procedimento
											   and    c.ie_origem_proced = a.ie_origem_proced
											   and    b.cd_convenio_parametro = d.cd_convenio
											   and    c.nr_seq_regra_excl = d.nr_sequencia				   and    a.cd_motivo_exc_conta is null
											   and    b.cd_convenio_parametro = cd_convenio_w
											   and    b.nr_atendimento = nr_atendimento_w
											   and    c.ie_situacao = 'A'
											   and    c.nr_seq_regra_excl = nr_seq_regra_excl_w);

								if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then 
									commit; 
								end if;			
							end;
						end if; 
					end;
				end if;
			end;
		end if;
	end if;
	
end loop;
close c01;

end gerar_procedimento_relacionado;
/