create or replace
procedure consiste_classif_tit_rec_lote ( nr_titulo_p		number,
										 nm_usuario_p		Varchar2) is 

ie_situacao_w			titulo_receber.ie_situacao%type;
nr_interno_conta_w		titulo_receber.nr_interno_conta%type;
nr_seq_protocolo_w		titulo_receber.nr_seq_protocolo%type;
nr_seq_mensalidade_w	titulo_receber.nr_seq_mensalidade%type;

nr_sequencia_w			w_cons_recalc_tit_classif.nr_sequencia%type;
ds_motivo_w				w_cons_recalc_tit_classif.ds_motivo%type;
ds_situacao_w			varchar2(20);
ie_origem_titulo_w		titulo_receber.ie_origem_titulo%type;
ds_origem_tit_w			varchar2(50);
nr_seq_pls_lote_contest_w	titulo_receber.nr_seq_pls_lote_contest%type;
nr_seq_lote_w			pls_fatura.nr_seq_lote%type;

qt_titulos_neg_w			number(10);
nr_seq_negociacao_w			number(10);
																				
begin

if (nr_titulo_p is not null) then

	select	max(a.ie_situacao),
			max(a.nr_interno_conta),
			max(a.nr_seq_protocolo),
			max(a.nr_seq_mensalidade),
			max(a.ie_origem_titulo)
	into	ie_situacao_w,
			nr_interno_conta_w,
			nr_seq_protocolo_w,
			nr_seq_mensalidade_w,
			ie_origem_titulo_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_p;
	
	select	max(a.nr_seq_pls_lote_contest)
	into	nr_seq_pls_lote_contest_w
	from	titulo_receber a
	where	a.nr_titulo	= nr_titulo_p
	and   	not exists ( select	x.nr_titulo 
						 from 	pls_fatura x 
						 where 	x.nr_titulo = a.nr_titulo or x.nr_titulo_ndc = a.nr_titulo);	
						 
	select  max(x.nr_seq_lote)
	into	nr_seq_lote_w
	from	titulo_receber a, 
			pls_fatura  x
	where	(x.nr_titulo = a.nr_titulo or x.nr_titulo_ndc = a.nr_titulo)
	and   	a.nr_titulo	= nr_titulo_p;	

	/*OS 1903161 - Recalcular t�tulos originados de negoci��o de contas a receber.*/
	select 	sum(qt_titulo),
			max(nr_seq_negociacao)
	into	qt_titulos_neg_w,
			nr_seq_negociacao_w
	from (	select  count(*) qt_titulo,
					max(nr_seq_negociacao) nr_seq_negociacao
			from	negociacao_cr_boleto
			where   nr_titulo = nr_titulo_p
			union
			select  count(*) qt_titulo,
					max(nr_seq_negociacao) nr_seq_negociacao
			from	negociacao_cr_deb_cc
			where	nr_titulo = nr_titulo_p
			union
			select	count(*) qt_titulo,
					max(c.nr_seq_negociacao) nr_seq_negociacao
			from	deposito_ident_titulo a,
					deposito_identificado b,
					negociacao_cr_dep_ident c
			where	 a.nr_titulo = nr_titulo_p
			and	 	a.nr_seq_deposito = b.nr_sequencia
			and	 	b.nr_sequencia = c.nr_seq_deposito_ident
		 );	

	/*Se o t�tulo n�o estiver com situa��o aberto, ou n�o for vinculado a conta pac, protocolo, mensalidade, ops faturamento e lote ops contesta��o a rotina recalcular_titulo_rec_classif n vai recalcular, entao precisa ser informado ao usu�rio*/	
    if (ie_situacao_w <> '1') or 
	   ((nvl(nr_interno_conta_w,0) = 0) and (nvl(nr_seq_protocolo_w,0) = 0 ) and (nvl(nr_seq_mensalidade_w,0) = 0) and (nvl(nr_seq_pls_lote_contest_w,0) = 0) and (nvl(nr_seq_lote_w,0) = 0) and (nvl(nr_seq_negociacao_w,0) = 0))  then
	    begin
	    if (ie_situacao_w <> '1') then
			select 	substr(obter_desc_expressao(max(a.cd_exp_valor_dominio)),1,20)
			into	ds_situacao_w	
			from	valor_dominio_v a
			where	a.cd_dominio = 710
			and		a.vl_dominio = ie_situacao_w;

			ds_motivo_w   := substr(wheb_mensagem_pck.get_texto(684883,'ds_situacao_w='|| ds_situacao_w),1,254);

		end if;
		
		if  ((nvl(nr_interno_conta_w,0) = 0) and ( nvl(nr_seq_protocolo_w,0) = 0 ) and (nvl(nr_seq_mensalidade_w,0) = 0) and (nvl(nr_seq_pls_lote_contest_w,0) = 0) and (nvl(nr_seq_lote_w,0) = 0) and (nvl(nr_seq_negociacao_w,0) = 0)) then
		
			select 	substr(obter_desc_expressao(max(a.cd_exp_valor_dominio)),1,50)
			into	ds_origem_tit_w	
			from	valor_dominio_v a
			where	a.cd_dominio = 709
			and		a.vl_dominio = ie_origem_titulo_w;
			
			if (ds_motivo_w is not null) then
				ds_motivo_w := substr(ds_motivo_w || '. ',1,254);
			end if;
			
			ds_motivo_w := substr(ds_motivo_w || wheb_mensagem_pck.get_texto(684885,'ds_origem_tit_w='||ds_origem_tit_w),1,254); 

		end if;
	    exception when others then
			ds_motivo_w := '';
		end;
	   
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	w_cons_recalc_tit_classif;

		insert into w_cons_recalc_tit_classif( 		nr_sequencia,
													nr_titulo_receber,
													dt_atualizacao,
													dt_atualizacao_nrec,
													nm_usuario_nrec,
													nm_usuario,
													ds_motivo)
											values( nr_sequencia_w,
													nr_titulo_p,
													sysdate,
													sysdate,
													nm_usuario_p,
													nm_usuario_p,
													substr(ds_motivo_w,1,255));

	end if;   
	
end if;

commit;

end consiste_classif_tit_rec_lote;
/