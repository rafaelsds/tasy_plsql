create or replace procedure PATIENT_DATA_IMPORT(      ds_parametros_p clob,
                                nm_usuario_p        varchar2,
                                nr_atendimento_p    number ,
                                source_system_p     varchar2 default null,
                                ie_operacao_p       varchar2 default null,
                                nr_seq_import_obr_p number   default null,
                                template_p          out varchar2) is

TYPE campos IS RECORD ( obx31       VARCHAR2(100),
                        obx33       VARCHAR2(100),
                        vl_field    VARCHAR2(100),
                        obx4        VARCHAR2(10),
                        obx11       VARCHAR2(1),
                        obx14       VARCHAR2(20),
                        obx15       VARCHAR2(4000),
                        ds_lista    clob
                        );

TYPE Vetor IS TABLE OF campos INDEX BY BINARY_INTEGER;
Vetor_w                  Vetor;
indice_w                INTEGER;
ds_parametros_w         CLOB;

cursor c01 is
select c.nr_seq_template,
       d.nr_seq_elemento,
       d.nr_sequencia sequencia_conteudo,
       a.obx_code_system,
       a.obx_name_system
from   patient_imp_fie_det a,
       patient_import_fields b,
       patient_import_template c,
       ehr_template_conteudo d
where  a.nr_seq_pat_imp_field(+) = b.nr_sequencia
and    b.nr_seq_pat_imp_temp     = c.nr_sequencia
and    b.nr_seq_templ_cont       = d.nr_sequencia
order by    nr_seq_template;

cursor c02 (nr_seq_linked_p   tabela_atributo_linked.nr_seq_linked%type,
            nr_seq_template_p number) is
select  a.nm_atributo,
        nvl(nvl(c.vl_padrao,a.vl_padrao),0) vl_padrao,
        a.ie_tipo_atributo
from    tabela_atributo_linked a
join    ehr_template_conteudo  b on (a.nr_seq_elemento = b.nr_sequencia)
left join    tabela_atributo_regra_ld c on (a.nr_sequencia = c.nr_seq_tabela_linked)
where   1=1
and     a.ie_obrigatorio = 'S'
and     a.nr_seq_linked     = nr_seq_linked_p
and     b.nr_seq_template   = nr_seq_template_p
and     a.nm_atributo not in ('NM_USUARIO','NM_USUARIO_NREC','DT_ATUALIZACAO','DT_ATUALIZACAO_NREC','NR_SEQUENCIA');

cursor c03 (nr_seq_linked_data_p  patient_data_imp_final.nr_seq_linked_data%type,
           nr_seq_template_p     patient_data_imp_final.nr_seq_template%type,
           nm_atributo_p         patient_data_imp_final.nm_atributo%type,
           nr_seq_pat_imp_obr_p  patient_data_imp_final.nr_seq_pat_imp_obr%type) is
select pif.nr_seq_record_template,
       pif.ie_resultado
from   patient_data_imp_final pif
where  pif.nr_seq_linked_data  = nr_seq_linked_data_p
and    pif.nr_seq_template     = nr_seq_template_p
and    pif.nm_atributo         = nm_atributo_p
and    pif.nr_seq_pat_imp_obr  = nr_seq_pat_imp_obr_p
;

nr_ehr_reg_w                ehr_registro.nr_sequencia%type;
nr_ehr_reg_templ_w          ehr_reg_template.nr_sequencia%type;
nr_seq_template_conteudo_w  ehr_template_conteudo.nr_sequencia%type;
nr_seq_elemento_w           ehr_template_conteudo.nr_sequencia%type;
nr_sequencia2_w             ehr_reg_elemento.nr_sequencia%type;
nr_seq_template_w           ehr_template.nr_sequencia%type;
nr_seq_linked_data_w        tabela_atributo_linked.nr_sequencia%type;
nr_ehr_reg_atual_w          ehr_reg_template.nr_sequencia%type;
nr_seq_template_atual_w     ehr_template.nr_sequencia%type;
obx_name_system_w           varchar2(200);
obx_code_system_w           varchar2(200);
ds_lista_aux_w              clob;
primeira_passagem_w         boolean := true;
verifica_se_linked_w        number(1);
ds_comando_w                varchar2(2000);
seq_tab_temp_w              number(38);
ds_response_w               varchar2(2000);
nm_atributo_w               varchar2(200);
v_numero_w                  number;
atualiza_reg_elemento_w     boolean :=true;
cd_pessoa_fisica_w          pessoa_fisica.cd_pessoa_fisica%type;
vl_padrao_w                 tabela_atributo_linked.vl_padrao%type;
nm_atributo_padrao_w        varchar2(200);
nr_seq_data_imp_final       patient_data_imp_final.nr_sequencia%type;
dt_resultado_w              varchar2(20);
nm_table_w                  linked_data.nm_table%type;
ds_template_w               varchar2(2000);
sysdate_w                   varchar2(20) := to_char(sysdate,'yyyymmddhh24miss');
ie_tipo_atributo_w          varchar2(15);
nr_sequencia_padrao_w       number(10);
ie_origem_proced_w          proc_interno.ie_origem_proced%type;
cd_procedimento_w           proc_interno.cd_procedimento%type;   
ds_value_w		              varchar2(4000);
reg_atualizado              number(1) := 0;

function is_number (numero_p varchar2) return boolean is
begin
   v_numero_w := to_number(numero_p);
   return true;
exception
  when value_error then
     return false;
end;
begin
    ds_parametros_w  := ds_parametros_p;
    indice_w  := 0;
    WHILE (LENGTH(ds_parametros_w) > 0) LOOP
        BEGIN
        indice_w  := indice_w+1;
        IF  (INSTR(ds_parametros_w,';')  >0)  THEN
            Vetor_w(indice_w).ds_lista  := SUBSTR(ds_parametros_w,1,INSTR(ds_parametros_w,';')-1 );
            ds_parametros_w := SUBSTR(ds_parametros_w,INSTR(ds_parametros_w,';')+1,40000);
        ELSE
            Vetor_w(indice_w).ds_lista  := SUBSTR(ds_parametros_w,1,LENGTH(ds_parametros_w) - 1);
            ds_parametros_w  := NULL;
        END IF;
        END;
    END LOOP;
    FOR j IN 1..Vetor_w.COUNT LOOP
        BEGIN
          ds_lista_aux_w       := Vetor_w(j).ds_lista;
          Vetor_w(j).obx31     := SUBSTR(ds_lista_aux_w,1,INSTR(ds_lista_aux_w,'#@#@')-1 );
          ds_lista_aux_w       := SUBSTR(ds_lista_aux_w,INSTR(ds_lista_aux_w,'#@#@')+4,40000);
          Vetor_w(j).obx33     := SUBSTR(ds_lista_aux_w,1,INSTR(ds_lista_aux_w,'#@#@')- 1 );
          ds_lista_aux_w       := SUBSTR(ds_lista_aux_w,INSTR(ds_lista_aux_w,'#@#@')+4,40000);
          Vetor_w(j).obx4      := nvl(SUBSTR(ds_lista_aux_w,1,INSTR(ds_lista_aux_w,'#@#@')- 1 ),'DEF');
          ds_lista_aux_w       := SUBSTR(ds_lista_aux_w,INSTR(ds_lista_aux_w,'#@#@')+4,40000);
          Vetor_w(j).vl_field  := nvl(SUBSTR(ds_lista_aux_w,1,INSTR(ds_lista_aux_w,'#@#@')-1 ),ds_lista_aux_w);
          ds_lista_aux_w       := SUBSTR(ds_lista_aux_w,INSTR(ds_lista_aux_w,'#@#@')+4,40000);
          Vetor_w(j).obx11     := nvl(SUBSTR(ds_lista_aux_w,1,INSTR(ds_lista_aux_w,'#@#@')- 1 ),'F');
          ds_lista_aux_w       := SUBSTR(ds_lista_aux_w,INSTR(ds_lista_aux_w,'#@#@')+4,40000);
          Vetor_w(j).obx14     := nvl(SUBSTR(ds_lista_aux_w,1,INSTR(ds_lista_aux_w,'#@#@')- 1 ),ds_lista_aux_w);
          ds_lista_aux_w       := SUBSTR(ds_lista_aux_w,INSTR(ds_lista_aux_w,'#@#@')+4,40000);
          Vetor_w(j).obx15  := SUBSTR(ds_lista_aux_w,1,4000 );
       END;
    END LOOP;
    --
    open c01;
    loop
    fetch c01 into
          nr_seq_template_w,
          NR_SEQ_ELEMENTO_w,
          nr_seq_template_conteudo_w,
          obx_code_system_w,
          obx_name_system_w;
    exit when c01%notfound;
      FOR i IN 1..Vetor_w.COUNT LOOP
        BEGIN
          IF (Vetor_w(i).obx31 = obx_code_system_w and Vetor_w(i).obx33 = obx_name_system_w) THEN
             select nvl(cd_pessoa_fisica,0)
               into cd_pessoa_fisica_w
               from atendimento_paciente
              where nr_atendimento = nr_atendimento_p;
              ---------------
              if (ie_operacao_p = 'I' and ((nr_seq_template_w <> nr_seq_template_atual_w) or (primeira_passagem_w))) then
                   EXECUTAR_ACAO_TEMPLATE_GENERIC(  'A',
                                                    nm_usuario_p,
                                                    null,
                                                    nr_atendimento_p,
                                                    cd_pessoa_fisica_w,
                                                    nr_seq_template_w,
                                                    nr_ehr_reg_w,
                                                    nr_ehr_reg_templ_w,
                                                    ds_response_w);
                    select nr_seq_template
                    into nr_seq_template_atual_w
                    from ehr_reg_template
                    where nr_sequencia = nr_ehr_reg_templ_w ;
                    primeira_passagem_w := false;
                    nr_ehr_reg_atual_w := nr_ehr_reg_templ_w;
              end if;
              select count(1)
              into verifica_se_linked_w
              from TABELA_ATRIBUTO_LINKED a
              join EHR_TEMPLATE_CONTEUDO  b on (a.NR_SEQ_ELEMENTO = b.NR_SEQUENCIA)
              join PATIENT_IMP_FIE_DET    c on (a.nr_sequencia = c.NR_SEQ_TAB_ATRI_LINKED)
              where 1=1
              and a.NR_SEQ_ELEMENTO = nr_seq_template_conteudo_w
              and c.OBX_NAME_SYSTEM = obx_name_system_w
              and c.OBX_CODE_SYSTEM = obx_code_system_w ;
              if (verifica_se_linked_w >= 1) then
                 atualiza_reg_elemento_w := false;
                 select a.NR_SEQ_LINKED,
                        a.NM_ATRIBUTO,
                        a.ie_tipo_atributo
                 into nr_seq_linked_data_w,
                      nm_atributo_w,
                      ie_tipo_atributo_w
                 from  TABELA_ATRIBUTO_LINKED a
                 join EHR_TEMPLATE_CONTEUDO  b on (a.NR_SEQ_ELEMENTO = b.NR_SEQUENCIA)
                 join PATIENT_IMP_FIE_DET    c on (a.nr_sequencia = c.NR_SEQ_TAB_ATRI_LINKED)
                 where 1=1
                 and a.NR_SEQ_ELEMENTO = nr_seq_template_conteudo_w
                 and c.OBX_NAME_SYSTEM = obx_name_system_w
                 and c.OBX_CODE_SYSTEM = obx_code_system_w;
                 ds_comando_w := 'select nvl(max(nr_sequencia),0)
                                    from EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                  ' where  nr_seq_template     ='||nr_seq_template_atual_w||
                                  ' and    nr_seq_linked_data  ='||nr_seq_linked_data_w||
                                  ' and    nr_seq_reg_template ='||nr_ehr_reg_atual_w;

                 obter_valor_dinamico(ds_comando_w,seq_tab_temp_w);
                 --
                 if Vetor_w(i).obx4 = 'PROC'  then
                   ds_value_w   := obter_conv_receb('PROC_INTERNO','NR_SEQUENCIA', NULL, Vetor_w(i).vl_field);
                   if ds_value_w is not null then 
                      Vetor_w(i).vl_field    :=  ds_value_w;
                   else
                      Vetor_w(i).vl_field    :=  Vetor_w(i).vl_field;
                   end if;                                    
                 end if; 
                 --
                 IF ie_operacao_p = 'I' THEN -- 
                   if (seq_tab_temp_w = 0) then
                       ds_comando_w := 'select EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||'_seq.nextval from dual';
                       obter_valor_dinamico(ds_comando_w,seq_tab_temp_w);
                       if (UPPER(ie_tipo_atributo_w)='DATE')then   
                           ds_comando_w := 'insert into  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                 '(nr_sequencia,
                                   nr_seq_template,
                                   nr_seq_linked_data,
                                   nr_seq_reg_template,
                                   nm_usuario,
                                   dt_atualizacao,
                                   nm_usuario_nrec,
                                   dt_atualizacao_nrec,'
                                   ||nm_atributo_w||')
                              values
                                   ('||seq_tab_temp_w||',
                                    '|| nr_seq_template_atual_w ||',
                                    '||nr_seq_linked_data_w     ||',
                                    '||nr_ehr_reg_atual_w       ||',
                                    '||CHR(39)||nm_usuario_p ||CHR(39)||',
                                    '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                    '||CHR(39)||nm_usuario_p ||CHR(39)||',
                                    '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                    '||'(SELECT to_date('||Vetor_w(i).vl_field||','||CHR(39)||'yyyymmdd hh24miss'||CHR(39)||')FROM DUAL)'||' )';
                                    
                                    
                       else
                           ds_comando_w := 'insert into  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                       '(nr_sequencia,
                                         nr_seq_template,
                                         nr_seq_linked_data,
                                         nr_seq_reg_template,
                                         nm_usuario,
                                         dt_atualizacao,
                                         nm_usuario_nrec,
                                         dt_atualizacao_nrec,'
                                         ||nm_atributo_w||')
                                    values
                                         ('||seq_tab_temp_w||',
                                          '|| nr_seq_template_atual_w ||',
                                          '||nr_seq_linked_data_w     ||',
                                          '||nr_ehr_reg_atual_w       ||',
                                          '||CHR(39)||nm_usuario_p ||CHR(39)||',
                                          '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                          '||CHR(39)||nm_usuario_p ||CHR(39)||',
                                          '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                          '||CHR(39)||Vetor_w(i).vl_field||CHR(39)||')';
                       end if;
                       exec_sql_dinamico('TASY', ds_comando_w);
                       commit;
                       if template_p is not null then
                           template_p :=  template_p||seq_tab_temp_w||','||nr_seq_template_atual_w||','||nr_seq_linked_data_w||';';
                       else
                           template_p :=  seq_tab_temp_w||','||nr_seq_template_atual_w||','||nr_seq_linked_data_w||';';
                       end if;
                       open c02 (nr_seq_linked_data_w,
                                 nr_seq_template_atual_w);
                       loop
                       fetch c02 
                        into nm_atributo_padrao_w,vl_padrao_w,ie_tipo_atributo_w;
                        if substr(nvl(vl_padrao_w,0),1,1) = '@' then
                          if ( upper(replace(vl_padrao_w,'@')) = 'SYSDATE'and UPPER(ie_tipo_atributo_w)='DATE' ) then
                              vl_padrao_w := sysdate_w;
                          elsif upper(replace(vl_padrao_w,'@')) = 'NR_ATENDIMENTO' then
                              vl_padrao_w := nr_atendimento_p;
                          elsif upper(replace(vl_padrao_w,'@')) = 'CD_PESSOA_FISICA' then
                              vl_padrao_w := cd_pessoa_fisica_w;
                          elsif upper(replace(vl_padrao_w,'@')) = 'PESSOA_USUARIO' then
                               select CD_MEDICO_ATENDIMENTO 
                               into vl_padrao_w
                               from atendimento_paciente
                               where nr_atendimento = nr_atendimento_p;
                               if (vl_padrao_w is null) then
                                   begin
                                    select CD_PESSOA_FISICA
                                       into vl_padrao_w
                                       from usuario 
                                    where upper (nm_usuario) = 'INTEGRATION';    
                                   exception
                                    when no_data_found then
                                         vl_padrao_w := '1';
                                   end;                              
                               end if;
                          elsif upper(replace(vl_padrao_w,'@')) = 'SETOR_USUARIO' then                                
                              SELECT OBTER_SETOR_ATUAL_PACIENTE(NR_ATENDIMENTO_P)
                              INTO vl_padrao_w
                              FROM DUAL ;
                          elsif upper(replace(vl_padrao_w,'@')) = 'CD_PACIENTE' then   
                               vl_padrao_w :=  cd_pessoa_fisica_w;        
                          else
                              vl_padrao_w := null;
                          end if;
                        else
                          if (UPPER(ie_tipo_atributo_w)='DATE' and upper(vl_padrao_w) = 'SYSDATE') then 
                               vl_padrao_w := sysdate_w;
                          elsif upper(nm_atributo_padrao_w) = 'CD_PACIENTE' then
                               vl_padrao_w :=  cd_pessoa_fisica_w; 
                          elsif upper(nm_atributo_padrao_w) = 'NR_ATENDIMENTO' then
                               vl_padrao_w := nr_atendimento_p;
                          else                    
                               vl_padrao_w := vl_padrao_w;
                          end if;   
                        end if;
                        if (UPPER(ie_tipo_atributo_w)='DATE')then
                            ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                   ' set '||nm_atributo_padrao_w||' = '||'to_date('||CHR(39)||vl_padrao_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||
                                   'where nr_sequencia = '||seq_tab_temp_w ||' and '||nm_atributo_padrao_w||' is null';
                        else
                            ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                            ' set '||nm_atributo_padrao_w||' = '||CHR(39)||vl_padrao_w||CHR(39)||
                            ' where nr_sequencia = '||seq_tab_temp_w ||' and '||nm_atributo_padrao_w||' is null';
                        end if;
                        exec_sql_dinamico('TASY', ds_comando_w);
                        commit;
                        exit when c02%notfound;
                       end loop;
                       nm_atributo_padrao_w := null;
                       vl_padrao_w          := null;
                       ie_tipo_atributo_w   := null;
                       close c02;
                   elsif(seq_tab_temp_w >0 ) then
                        ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                       ' set '||nm_atributo_w||' = '||CHR(39)||Vetor_w(i).vl_field||CHR(39)||' where nr_sequencia = '||seq_tab_temp_w;
                        exec_sql_dinamico('TASY', ds_comando_w);
                        commit;
                        IF (UPPER(ie_tipo_atributo_w)='DATE')THEN
                            ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                       ' set '||nm_atributo_w||' = ('||'SELECT to_date('||Vetor_w(i).vl_field||','||CHR(39)||'yyyymmdd hh24miss'||CHR(39)||')FROM DUAL)
                                        where nr_sequencia = '||seq_tab_temp_w;
                           exec_sql_dinamico('TASY', ds_comando_w);
                           commit;
                        END IF;
                
                        open c02 (nr_seq_linked_data_w,
                                  nr_seq_template_atual_w);
                        loop
                        fetch c02 
                        into nm_atributo_padrao_w,vl_padrao_w,ie_tipo_atributo_w;
                        if substr(nvl(vl_padrao_w,0),1,1) = '@' then
                          if ( upper(replace(vl_padrao_w,'@')) = 'SYSDATE'and UPPER(ie_tipo_atributo_w)='DATE' ) then
                              vl_padrao_w := sysdate_w;
                          elsif upper(replace(vl_padrao_w,'@')) = 'NR_ATENDIMENTO' then
                              vl_padrao_w := nr_atendimento_p;
                          elsif upper(replace(vl_padrao_w,'@')) = 'CD_PESSOA_FISICA' then
                              vl_padrao_w := cd_pessoa_fisica_w;
                          elsif upper(replace(vl_padrao_w,'@')) = 'PESSOA_USUARIO' then
                               select CD_MEDICO_ATENDIMENTO 
                               into vl_padrao_w
                               from atendimento_paciente
                               where nr_atendimento = nr_atendimento_p;
                               if (vl_padrao_w is null) then
                                   begin
                                    select CD_PESSOA_FISICA
                                       into vl_padrao_w
                                       from usuario 
                                    where upper (nm_usuario) = 'INTEGRATION';    
                                   exception
                                    when no_data_found then
                                         vl_padrao_w := '1';
                                   end;                              
                               end if;
                          elsif upper(replace(vl_padrao_w,'@')) = 'SETOR_USUARIO' then                                
                              SELECT OBTER_SETOR_ATUAL_PACIENTE(NR_ATENDIMENTO_P)
                              INTO vl_padrao_w
                              FROM DUAL ;
                          elsif upper(replace(vl_padrao_w,'@')) = 'CD_PACIENTE' then   
                               vl_padrao_w :=  cd_pessoa_fisica_w;                 
                          else
                              vl_padrao_w := null;
                          end if;
                        else
                          if (UPPER(ie_tipo_atributo_w)='DATE' and upper(vl_padrao_w) = 'SYSDATE') then 
                               vl_padrao_w := sysdate_w;
                          elsif upper(nm_atributo_padrao_w) = 'CD_PACIENTE' then
                               vl_padrao_w :=  cd_pessoa_fisica_w; 
                          elsif upper(nm_atributo_padrao_w) = 'NR_ATENDIMENTO' then
                               vl_padrao_w := nr_atendimento_p;
                          else                    
                               vl_padrao_w := vl_padrao_w;
                          end if;   
                        end if;
                        if (UPPER(ie_tipo_atributo_w)='DATE')then
                            ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                   ' set '||nm_atributo_padrao_w||' = '||'to_date('||CHR(39)||vl_padrao_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||
                                   'where nr_sequencia = '||seq_tab_temp_w ||' and '||nm_atributo_padrao_w||' is null';
                        else
                            ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                            ' set '||nm_atributo_padrao_w||' = '||CHR(39)||vl_padrao_w||CHR(39)||
                            ' where nr_sequencia = '||seq_tab_temp_w ||' and '||nm_atributo_padrao_w||' is null';
                        end if;
                        exec_sql_dinamico('TASY', ds_comando_w);
                        commit;
                        exit when c02%notfound;
                        end loop;
                        nm_atributo_padrao_w := null;
                        vl_padrao_w          := null;
                        ie_tipo_atributo_w   := null;
                        close c02;
                   end if;
                   select patient_data_imp_final_seq.nextval
                        into nr_seq_data_imp_final
                   from dual;
                   --
                   select nm_table into nm_table_w
                   from linked_data where nr_sequencia = nr_seq_linked_data_w;

                   if Vetor_w(i).obx14 is not null then
                     dt_resultado_w    := Vetor_w(i).obx14;
                   else
                     dt_resultado_w := null;
                   end if;
                   
                   ds_comando_w := 'insert into  patient_data_imp_final
                                    (nr_sequencia,
                                     dt_atualizacao,
                                     nm_usuario,
                                     dt_atualizacao_nrec,
                                     nm_usuario_nrec,
                                     nm_tabela,
                                     nm_atributo,
                                     nm_source_system,
                                     nr_seq_fk,
                                     dt_resultado,
                                     ie_resultado,
                                     nr_seq_linked_data,
                                     nr_seq_template,
                                     nr_seq_record_template,
                                     nr_seq_pat_imp_obr,
                                     ds_justificativa)
                              values
                                   ('||nr_seq_data_imp_final ||',
                                    '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                    '||CHR(39)||nm_usuario_p ||CHR(39)||',
                                    '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                    '||CHR(39)||nm_usuario_p ||CHR(39)||',
                                    '||CHR(39)||nm_table_w||CHR(39)||',
                                    '||CHR(39)||nm_atributo_w||CHR(39)||',
                                    '||CHR(39)||source_system_p||CHR(39)||',
                                    '||CHR(39)||null||CHR(39)||',
                                    '||'to_date('||CHR(39)||dt_resultado_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                    '||CHR(39)||Vetor_w(i).obx11||CHR(39)||',
                                    '||nr_seq_linked_data_w||',
                                    '||nr_seq_template_atual_w||',
                                    '||seq_tab_temp_w ||',
                                    '||nr_seq_import_obr_p||',
                                    '||CHR(39)||Vetor_w(i).obx15||CHR(39)||')';
                  exec_sql_dinamico('TASY', ds_comando_w);
                 else -- ie_operacao_p U,D
                   select max(nr_seq_template)
                     into nr_seq_template_atual_w
                     from patient_data_imp_final a
                    where a.nr_seq_pat_imp_obr = nr_seq_import_obr_p
                      and a.nr_seq_linked_data = nr_seq_linked_data_w;
                   FOR r3 IN c03(nr_seq_linked_data_w,
                                nr_seq_template_atual_w,
                                nm_atributo_w,
                                nr_seq_import_obr_p  )
                     LOOP
                       if ie_operacao_p = 'U' then
                          ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                         ' set '||nm_atributo_w||' = '||CHR(39)||Vetor_w(i).vl_field||CHR(39)||
                                         ' where nr_sequencia = '||r3.nr_seq_record_template ;

                       elsif ie_operacao_p = 'D' then
                          ds_comando_w := 'update  EHR_LINKED_'||nr_seq_template_atual_w||'_'||nr_seq_linked_data_w||
                                         ' set '||nm_atributo_w||' = '||CHR(39)||null||CHR(39)||
                                         ' where nr_sequencia = '||r3.nr_seq_record_template;
                       end if;
                       exec_sql_dinamico('TASY', ds_comando_w);
                       if r3.ie_resultado <> Vetor_w(i).obx11 then
                          ds_comando_w := 'update  patient_data_imp_final'||
                                           ' set ie_resultado     = '||CHR(39)||Vetor_w(i).obx11||CHR(39)||',
                                                 ds_justificativa = '||CHR(39)||Vetor_w(i).obx15||CHR(39)||',
                                                 dt_atualizacao = '||'to_date('||CHR(39)||sysdate_w||CHR(39)||','||CHR(39)||'yyyymmddhh24miss'||CHR(39)||')'||',
                                                 nm_usuario       = '||CHR(39)||nm_usuario_p ||CHR(39)||'
                                            where nr_seq_record_template = '||r3.nr_seq_record_template||
                                           ' and  nr_seq_linked_data  = '||nr_seq_linked_data_w ||
                                           ' and  nr_seq_template     = '||nr_seq_template_atual_w||
                                           ' and  nm_atributo         = '||CHR(39)||nm_atributo_w||CHR(39)||
                                           ' and  nr_seq_pat_imp_obr  = '||nr_seq_import_obr_p;
                       end if;
                       exec_sql_dinamico('TASY', ds_comando_w);
                       commit;
                       
                       if nvl(ds_template_w,'o') <> r3.nr_seq_record_template||','||nr_seq_template_atual_w||','||nr_seq_linked_data_w||';' then
                        ds_template_w := r3.nr_seq_record_template||','||nr_seq_template_atual_w||','||nr_seq_linked_data_w||';' ;
                         if template_p is not null then
                            template_p :=  template_p||r3.nr_seq_record_template||','||nr_seq_template_atual_w||','||nr_seq_linked_data_w||';';
                         else
                            template_p :=  r3.nr_seq_record_template||','||nr_seq_template_atual_w||','||nr_seq_linked_data_w||';';                            
                         end if;
                       end if;
                     END LOOP;
                   end if;
                end if;
                IF ie_operacao_p = 'I' THEN
                  if(is_number(Vetor_w(i).vl_field) and (atualiza_reg_elemento_w)) then
                      Atualizar_Reg_Elemento( nr_seq_template_conteudo_w,
                                              nr_ehr_reg_atual_w,
                                              NR_SEQ_ELEMENTO_w,
                                              nm_usuario_p,
                                              null,
                                              Vetor_w(i).vl_field,
                                              sysdate,
                                              null,
                                              null,
                                              nr_sequencia2_w);
                      atualiza_reg_elemento_w := true;
                      reg_atualizado          := 1;
                  elsif((is_number(Vetor_w(i).vl_field) = false) and (atualiza_reg_elemento_w)) then
                      Atualizar_Reg_Elemento( nr_seq_template_conteudo_w,
                                              nr_ehr_reg_atual_w,
                                              NR_SEQ_ELEMENTO_w,
                                              nm_usuario_p,
                                             CHR(39)||Vetor_w(i).vl_field||CHR(39),
                                              null,
                                              sysdate,
                                              null,
                                              null,
                                              nr_sequencia2_w);
                      atualiza_reg_elemento_w := true;
                      reg_atualizado          := 1;
                  end if;
                END IF;
            END IF;
            if (reg_atualizado = 1 and template_p is null ) then 
               update ehr_reg_template e
               set    e.dt_liberacao   = sysdate,
                      e.nm_usuario     = nm_usuario_p
               where e.nr_sequencia    = nr_ehr_reg_atual_w;
              --
              update ehr_registro e
               set    e.dt_liberacao         = sysdate,
                      e.nm_usuario_liberacao = nm_usuario_p
               where e.nr_sequencia          = (select t.nr_seq_reg from ehr_reg_template t
                                                where t.nr_sequencia = nr_ehr_reg_atual_w );
              commit;
               template_p := '-1'||';';
            end if;
        END;
      END LOOP;
    end  loop;
    close   c01;  
end patient_data_import;
/
