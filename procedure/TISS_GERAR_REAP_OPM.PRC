create or replace
procedure tiss_gerar_reap_opm(	nr_seq_conta_reap_p     in      number,
                                nm_usuario_p            in      varchar2) is
								
nr_sequencia_w		number(15);
nr_interno_conta_w		number(15);
qt_opm_w		number(15);
cd_barras_w		varchar2(255);
vl_unitario_w		number(15,2);
vl_opm_w		number(15,2);
cd_opm_w		number(15);
ds_opm_w		varchar2(255);
cd_autorizacao_w	varchar2(255);
nr_seq_tiss_tabela_w	number(10);
cd_tabela_xml_w		varchar2(10);	

cursor c01 is
select 	qt_opm,
	cd_barras,
	vl_unitario,
	vl_opm,
	cd_material,
	substr(nvl(ds_opm,obter_desc_material(cd_material)),1,254),
	a.nr_seq_tiss_tabela
from	tiss_reap_opm a
where	a.nr_seq_conta  = nr_seq_conta_reap_p;

begin

select	nr_interno_conta,
	cd_autorizacao
into	nr_interno_conta_w,
	cd_autorizacao_w
from	tiss_reap_conta
where	nr_sequencia	= nr_seq_conta_reap_p;

open c01;
loop
fetch c01 into
	qt_opm_w,
	cd_barras_w,
	vl_unitario_w,
	vl_opm_w,
	cd_opm_w,
	ds_opm_w,
	nr_seq_tiss_tabela_w;
exit when c01%notfound;

	select  tiss_conta_opm_exec_seq.nextval
	into    nr_sequencia_w
	from    dual;
	
	select	max(cd_tabela_xml)
	into	cd_tabela_xml_w
	from	tiss_tipo_tabela
	where	nr_sequencia	= nr_seq_tiss_tabela_w;

	insert into tiss_conta_opm_exec(
		nr_sequencia,
		nr_seq_reap_conta,
		cd_autorizacao,
		dt_atualizacao,
		nr_interno_conta,
		qt_opm,
		cd_barras,
		vl_unitario_opm,
		vl_opm,
		cd_opm,
		ds_opm,
		nm_usuario,
		cd_tabela)
	values(	nr_sequencia_w,
		nr_seq_conta_reap_p,
		cd_autorizacao_w,
		sysdate,
		nr_interno_conta_w,
		qt_opm_w,
		cd_barras_w,
		vl_unitario_w,
		vl_opm_w,
		lpad(cd_opm_w,8,0),
		ds_opm_w,
		nm_usuario_p,
		cd_tabela_xml_w);
			
end loop;
close c01;

end tiss_gerar_reap_opm;
/