create or replace
procedure pls_fechar_franq_pag(	nr_seq_franq_pag_p	pls_franq_pag.nr_sequencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is 

qt_prestadores_w	pls_integer := 0;
ie_funcao_pagamento_w	pls_parametro_pagamento.ie_funcao_pagamento%type;

begin

select	count(1)
into	qt_prestadores_w
from	pls_franq_pag_prest a
where	a.nr_seq_franq_pag = nr_seq_franq_pag_p;

select	nvl(max(ie_funcao_pagamento), '1')
into	ie_funcao_pagamento_w
from	pls_parametro_pagamento
where	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_prestadores_w > 0) then

	-- s� gera a evento movimento se for pagamento atual
	if	(ie_funcao_pagamento_w = '1') then
		pls_gerar_lote_evento_franq(nr_seq_franq_pag_p, nm_usuario_p);
	end if;

	update 	pls_franq_pag
	set 	dt_fechamento_lote = sysdate
	where	nr_sequencia = nr_seq_franq_pag_p;

	commit;
else
	wheb_mensagem_pck.exibir_mensagem_abort(311844);
end if;

end pls_fechar_franq_pag;
/