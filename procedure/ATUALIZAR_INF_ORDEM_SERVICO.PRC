create or replace
procedure atualizar_inf_ordem_servico(
		nr_seq_wheb_p		number,
		nr_seq_estagio_p		number,
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is 
begin
if	(nr_seq_wheb_p is not null) and
	(nr_seq_estagio_p is not null) and
	(nr_sequencia_p is not null) then
	update 	man_ordem_servico 
	set 	nr_seq_wheb 	= nr_seq_wheb_p,
		dt_envio_wheb 	= sysdate, 
		nr_seq_estagio 	= decode(nr_seq_estagio_p, 0, nr_seq_estagio, nr_seq_estagio_p), 
		dt_atualizacao 	= sysdate,
		nm_usuario 	= nm_usuario_p 
	where  	nr_sequencia  	= nr_sequencia_p;
	commit;
end if;
end atualizar_inf_ordem_servico;
/