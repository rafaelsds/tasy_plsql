create or replace procedure pls_gerar_w_analise_proc_html
			(	nr_seq_analise_p	number,
				nr_seq_conta_p		number,
				nr_seq_grupo_p		number,
				ie_minha_analise_p	varchar2,
				ie_pendentes_p		varchar2,
				nm_usuario_p		varchar2,
				ie_somente_ocor_p	varchar2 default 'N',
				ie_ocultar_canc_p	varchar2 default 'N',
				nr_id_transacao_p	w_pls_analise_item.nr_id_transacao%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir na tabela tempor�ria dos itens da an�lise os procedimentos que n�o s�o
agrupadores (procedimentos sem participantes)
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: N�o colocar nos selects desta procedure participantes, os mesmos ser�o
gerados em outra procedure
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

tb_ds_item_w			pls_util_cta_pck.t_varchar2_table_255;
tb_ds_via_acesso_w		pls_util_cta_pck.t_varchar2_table_255;
tb_nm_prestador_pag_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ds_item_importacao_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ds_status_item_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_autorizado_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_tipo_linha_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_tipo_despesa_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_tipo_item_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_via_acesso_w		pls_util_cta_pck.t_varchar2_table_255;
tb_cd_porte_anestesico_w	pls_util_cta_pck.t_varchar2_table_255;
tb_ie_pagamento_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_status_item_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_status_analise_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_valor_base_w		pls_util_cta_pck.t_number_table;
tb_ie_pend_grupo_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_item_nao_encontrado_w	pls_util_cta_pck.t_varchar2_table_255;
tb_ie_selecionado_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_sem_fluxo_w		pls_util_cta_pck.t_varchar2_table_255;
tb_vl_taxa_servico_imp_w	pls_util_cta_pck.t_number_table;
tb_vl_taxa_co_imp_w		pls_util_cta_pck.t_number_table;
tb_vl_taxa_material_imp_w	pls_util_cta_pck.t_number_table;
tb_vl_taxa_servico_w		pls_util_cta_pck.t_number_table;
tb_vl_taxa_co_w			pls_util_cta_pck.t_number_table;
tb_vl_taxa_material_w		pls_util_cta_pck.t_number_table;
tb_tx_medico_w			pls_util_cta_pck.t_number_table;
tb_tx_material_w		pls_util_cta_pck.t_number_table;
tb_tx_custo_operacional_w	pls_util_cta_pck.t_number_table;
tb_vl_procedimento_imp_w	pls_util_cta_pck.t_number_table;
tb_vl_proc_unitario_w		pls_util_cta_pck.t_number_table;
tb_vl_procedimento_w		pls_util_cta_pck.t_number_table;
tb_vl_glosa_w			pls_util_cta_pck.t_number_table;
tb_vl_liberado_w		pls_util_cta_pck.t_number_table;
tb_vl_glosa_material_w		pls_util_cta_pck.t_number_table;
tb_vl_glosa_hi_w		pls_util_cta_pck.t_number_table;
tb_vl_glosa_co_w		pls_util_cta_pck.t_number_table;
tb_vl_custo_operacional_w	pls_util_cta_pck.t_number_table;
tb_vl_liberado_material_w	pls_util_cta_pck.t_number_table;
tb_vl_liberado_co_w		pls_util_cta_pck.t_number_table;
tb_vl_liberado_hi_w		pls_util_cta_pck.t_number_table;
tb_vl_calculado_material_w	pls_util_cta_pck.t_number_table;
tb_vl_calculado_co_w		pls_util_cta_pck.t_number_table;
tb_vl_calculado_hi_w		pls_util_cta_pck.t_number_table;
tb_vl_taxa_intercambio_w	pls_util_cta_pck.t_number_table;
tb_vl_taxa_intercambio_imp_w	pls_util_cta_pck.t_number_table;
tb_vl_unitario_apres_w		pls_util_cta_pck.t_number_table;
tb_vl_lib_taxa_co_w		pls_util_cta_pck.t_number_table;
tb_vl_lib_taxa_material_w	pls_util_cta_pck.t_number_table;
tb_vl_lib_taxa_servico_w	pls_util_cta_pck.t_number_table;
tb_vl_glosa_taxa_co_w		pls_util_cta_pck.t_number_table;
tb_vl_glosa_taxa_material_w	pls_util_cta_pck.t_number_table;
tb_vl_glosa_taxa_servico_w	pls_util_cta_pck.t_number_table;
tb_vl_material_ptu_imp_w	pls_util_cta_pck.t_number_table;
tb_vl_procedimento_ptu_imp_w	pls_util_cta_pck.t_number_table;
tb_vl_co_ptu_imp_w		pls_util_cta_pck.t_number_table;
tb_cd_procedimento_w		pls_util_cta_pck.t_number_table;
tb_qt_liberado_w		pls_util_cta_pck.t_number_table;
tb_qt_procedimento_imp_w	pls_util_cta_pck.t_number_table;
tb_qt_liberar_w			pls_util_cta_pck.t_number_table;
tb_nr_seq_conta_proc_w		pls_util_cta_pck.t_number_table;
tb_ie_origem_proced_w		pls_util_cta_pck.t_number_table;
tb_nr_seq_setor_atend_w		pls_util_cta_pck.t_number_table;
tb_nr_auxiliares_w		pls_util_cta_pck.t_number_table;
tb_nr_sequencia_w		pls_util_cta_pck.t_number_table;
tb_nr_seq_autogerado_w		pls_util_cta_pck.t_number_table;
tb_nr_identificador_w		pls_util_cta_pck.t_number_table;
tb_tx_item_w			pls_util_cta_pck.t_number_table;
tb_tx_intercambio_w		pls_util_cta_pck.t_number_table;
tb_tx_intercambio_imp_w		pls_util_cta_pck.t_number_table;
tb_dt_procedimento_w		pls_util_cta_pck.t_date_table;
tb_ds_grau_partic_w		pls_util_cta_pck.t_varchar2_table_255;
tb_cd_grau_partic_tiss_w	pls_util_cta_pck.t_varchar2_table_255;
tb_ie_pacote_w			pls_util_cta_pck.t_varchar2_table_255;
tb_qt_cobranca_w		pls_util_cta_pck.t_number_table;
tb_nr_seq_regra_qtde_exec_w	pls_util_cta_pck.t_number_table;
tb_cd_dente_w			pls_util_cta_pck.t_varchar2_table_255;
tb_ds_dente_w			pls_util_cta_pck.t_varchar2_table_255;
tb_cd_regiao_boca_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ds_regiao_boca_w		pls_util_cta_pck.t_varchar2_table_255;
tb_cd_face_dente_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ds_face_dente_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ie_alto_custo_w		pls_util_cta_pck.t_varchar2_table_255;
tb_ds_complemento_w		pls_util_cta_pck.t_varchar2_table_255;
tp_rede_min_w			pls_util_cta_pck.t_number_table;
tb_seq_guia_w			pls_util_cta_pck.t_number_table;
tb_ie_aviso_a520_proc_w		pls_util_cta_pck.t_varchar2_table_5;

ie_glosa_w			varchar2(255);
ds_procedimento_w		procedimento.ds_procedimento%TYPE;
ds_identacao_w			varchar2(30);
cd_guia_w			varchar2(30);
ie_autorizado_w			varchar2(20);
ie_minha_analise_w		varchar2(1)	:= 'N';
ie_pendentes_w			varchar2(1)	:= 'N';
nr_nivel_w			number(3)	:= 1;
dt_emissao_conta_w		date;
nr_seq_grau_partic_w		number(10);
nr_seq_guia_w			pls_conta.nr_seq_guia%type;
nm_prestador_solic_w		varchar2(255);
nm_prestador_exec_w		varchar2(255);
nr_seq_prestador_exec_w		pls_conta.nr_seq_prestador_exec%type;
nr_seq_protocolo_w		pls_conta.nr_seq_protocolo%type;
ds_espec_cbo_w			varchar2(255);
ds_medico_executor_w		varchar2(255);
index_w				pls_integer	:= 1;
nr_seq_prestador_pgto_w		number(10);
qt_selecao_w			number(10);
ie_tipo_guia_w			varchar2(255);
ds_tipo_guia_w			varchar2(255);
ie_somente_ocor_w		varchar2(1);
ie_ocultar_canc_w		varchar2(5);
nr_total_partic_w		pls_integer;

Cursor C01 is
	-- lista todos os procedimentos, inclusive com glosa / ocorrencia
	select	a.nr_sequencia,
		'IC' ie_item_conta, /* Item da conta */
		'P' ie_tipo_item, /* Procedimento */
		a.cd_procedimento,
		a.dt_procedimento,
		nvl(b.ds_procedimento,'N�o encontrado') ds_procedimento,
		a.ie_tipo_despesa,
		a.qt_procedimento_imp,
		a.vl_procedimento_imp,
		a.vl_proc_unitario,
		a.vl_procedimento,
		a.ie_origem_proced,
		a.ie_via_acesso,
		a.nr_auxiliares,
		a.cd_porte_anestesico,
		a.nr_seq_setor_atend,
		substr(a.cd_procedimento_imp || ' - ' || a.ds_procedimento_imp,1,255) ds_item_imp,
		a.vl_glosa_material,
		a.vl_glosa_hi,
		a.vl_glosa_co,
		a.vl_custo_operacional,
		a.vl_liberado_material,
		a.vl_liberado_co,
		a.vl_liberado_hi,
		a.vl_calc_mat_util,
		a.vl_calc_co_util,
		a.vl_calc_hi_util,
		a.tx_intercambio,
		a.tx_intercambio_imp,
		a.qt_procedimento,
		a.tx_item,
		a.vl_glosa,
		a.vl_liberado,
		a.vl_unitario_imp,
		a.vl_taxa_servico_imp,
		a.vl_taxa_co_imp,
		a.vl_taxa_material_imp,
		a.vl_taxa_servico,
		a.vl_taxa_co,
		a.vl_taxa_material,
		a.ie_status,
		a.ie_valor_base,
		decode(b.cd_procedimento,null,'S',null) nao_encontrado,
		a.nr_seq_prestador_pgto,
		vl_lib_taxa_co,
		vl_lib_taxa_material,
		vl_lib_taxa_servico,
		vl_glosa_taxa_co,
		vl_glosa_taxa_material,
		vl_glosa_taxa_servico,
		a.ie_glosa,
		a.tx_medico,
		a.tx_material,
		a.tx_custo_operacional,
		a.ie_status_pagamento,
		a.nr_seq_autogerado,
		a.nr_id_analise,
		c.nr_seq_grau_partic,
		vl_material_ptu_imp,
		vl_procedimento_ptu_imp,
		vl_co_ptu_imp,
		a.ie_pacote_ptu,
		a.nr_seq_regra_qtde_exec,
		a.cd_dente,
		tiss_obter_desc_dente(a.cd_dente) ds_dente,
		a.cd_face_dente,
		tiss_obter_desc_face_dente(a.cd_face_dente) ds_face_dente,
		a.cd_regiao_boca,
		tiss_obter_desc_reg_boca(a.cd_regiao_boca) ds_regiao_boca,
		a.ie_alto_custo,
		b.ds_complemento,
		(	select 	tp_rede_min
			from	pls_conta_proc_regra x
			where 	x.nr_sequencia = a.nr_sequencia) tp_rede_min,
		(	select 	ds_item_ptu
			from	pls_conta_proc_regra x
			where 	x.nr_sequencia = a.nr_sequencia) ds_desc_ptu,
		nvl(a.nr_seq_guia,c.nr_seq_guia) nr_seq_guia,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520
	from	pls_conta	c,
		procedimento	b,
		pls_conta_proc	a
	where	a.cd_procedimento		= b.cd_procedimento(+)
	and	a.ie_origem_proced		= b.ie_origem_proced(+)
	and	a.nr_seq_conta			= c.nr_sequencia
	and	a.nr_seq_conta			= nr_seq_conta_p
	and	c.ie_status <> 'C'
	and a.ie_status != 'M'
	and	(a.ie_lanc_manual_pos = 'N' or a.ie_lanc_manual_pos is null)
	and	ie_somente_ocor_w	= 'N'
	union all
	-- somente os procedimentos com glosa / ocorrencia
	select	a.nr_sequencia,
		'IC' ie_item_conta, /* Item da conta */
		'P' ie_tipo_item, /* Procedimento */
		a.cd_procedimento,
		a.dt_procedimento,
		nvl(b.ds_procedimento,'N�o encontrado') ds_procedimento,
		a.ie_tipo_despesa,
		a.qt_procedimento_imp,
		a.vl_procedimento_imp,
		a.vl_proc_unitario,
		a.vl_procedimento,
		a.ie_origem_proced,
		a.ie_via_acesso,
		a.nr_auxiliares,
		a.cd_porte_anestesico,
		a.nr_seq_setor_atend,
		substr(a.cd_procedimento_imp || ' - ' || a.ds_procedimento_imp,1,255) ds_item_imp,
		a.vl_glosa_material,
		a.vl_glosa_hi,
		a.vl_glosa_co,
		a.vl_custo_operacional,
		a.vl_liberado_material,
		a.vl_liberado_co,
		a.vl_liberado_hi,
		a.vl_calc_mat_util,
		a.vl_calc_co_util,
		a.vl_calc_hi_util,
		a.tx_intercambio,
		a.tx_intercambio_imp,
		a.qt_procedimento,
		a.tx_item,
		a.vl_glosa,
		a.vl_liberado,
		a.vl_unitario_imp,
		a.vl_taxa_servico_imp,
		a.vl_taxa_co_imp,
		a.vl_taxa_material_imp,
		a.vl_taxa_servico,
		a.vl_taxa_co,
		a.vl_taxa_material,
		a.ie_status,
		a.ie_valor_base,
		decode(b.cd_procedimento,null,'S',null) nao_encontrado,
		a.nr_seq_prestador_pgto,
		vl_lib_taxa_co,
		vl_lib_taxa_material,
		vl_lib_taxa_servico,
		vl_glosa_taxa_co,
		vl_glosa_taxa_material,
		vl_glosa_taxa_servico,
		a.ie_glosa,
		a.tx_medico,
		a.tx_material,
		a.tx_custo_operacional,
		a.ie_status_pagamento,
		a.nr_seq_autogerado,
		a.nr_id_analise,
		c.nr_seq_grau_partic,
		vl_material_ptu_imp,
		vl_procedimento_ptu_imp,
		vl_co_ptu_imp,
		a.ie_pacote_ptu,
		a.nr_seq_regra_qtde_exec,
		a.cd_dente,
		tiss_obter_desc_dente(a.cd_dente) ds_dente,
		pls_obter_faces_dente_proc(a.nr_sequencia) cd_face_dente,
		tiss_obter_desc_face_dente(pls_obter_faces_dente_proc(a.nr_sequencia)) ds_face_dente,
		a.cd_regiao_boca,
		tiss_obter_desc_reg_boca(a.cd_regiao_boca) ds_regiao_boca,
		a.ie_alto_custo,
		b.ds_complemento,
		(	select 	tp_rede_min
			from	pls_conta_proc_regra x
			where 	x.nr_sequencia = a.nr_sequencia) tp_rede_min,
		(	select 	ds_item_ptu
			from	pls_conta_proc_regra x
			where 	x.nr_sequencia = a.nr_sequencia) ds_desc_ptu,
		nvl(a.nr_seq_guia,c.nr_seq_guia) nr_seq_guia,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520
	from	pls_conta	c,
		procedimento	b,
		pls_conta_proc	a
	where	a.cd_procedimento		= b.cd_procedimento(+)
	and	a.ie_origem_proced		= b.ie_origem_proced(+)
	and	a.nr_seq_conta			= c.nr_sequencia
	and	a.nr_seq_conta			= nr_seq_conta_p
	and	c.ie_status <> 'C'
	and a.ie_status != 'M'
	and	(a.ie_lanc_manual_pos = 'N' or a.ie_lanc_manual_pos is null)
	and	ie_somente_ocor_w	= 'S'
	and	exists (	select	1
				from	pls_conta_glosa
				where	ie_situacao	= 'A'
				and	nr_seq_conta_proc	= a.nr_sequencia
				union all
				select	1
				from	pls_ocorrencia_benef
				where	ie_situacao	= 'A'
				and	nr_seq_conta_proc	= a.nr_sequencia);

begin
ie_minha_analise_w	:= nvl(ie_minha_analise_p,'N');
ie_pendentes_w		:= nvl(ie_pendentes_p,'N');
ie_ocultar_canc_w	:= nvl(ie_ocultar_canc_p, 'N');
ds_identacao_w		:= '        ';
ie_somente_ocor_w	:= ie_somente_ocor_p;


/* Obter dados da conta/guia */
nr_seq_guia_w		:= pls_consulta_analise_pck.get_nr_seq_guia;
ie_autorizado_w		:= pls_consulta_analise_pck.get_ie_autorizado;
cd_guia_w		:= pls_consulta_analise_pck.get_cd_guia;
ds_tipo_guia_w		:= pls_consulta_analise_pck.get_ds_tipo_guia;
nm_prestador_solic_w	:= pls_consulta_analise_pck.get_nm_prestador_solic;
nm_prestador_exec_w	:= pls_consulta_analise_pck.get_nm_prestador_exec;
nr_seq_prestador_exec_w	:= pls_consulta_analise_pck.get_nr_seq_prestador_exec;
dt_emissao_conta_w	:= pls_consulta_analise_pck.get_dt_emissao_conta;
nr_seq_protocolo_w	:= pls_consulta_analise_pck.get_nr_seq_protocolo;
ds_espec_cbo_w		:= pls_consulta_analise_pck.get_ds_espec_cbo;
ds_medico_executor_w	:= pls_consulta_analise_pck.get_ds_medico_executor;
nr_nivel_w		:= pls_consulta_analise_pck.get_nr_nivel;
ie_tipo_guia_w		:= pls_consulta_analise_pck.get_ie_tipo_guia;

if	(nr_nivel_w > 0) then
	for r_C01_w in C01 loop

		tb_nr_seq_conta_proc_w(index_w)		:= r_C01_w.nr_sequencia;
		tb_ie_tipo_linha_w(index_w)		:= r_C01_w.ie_item_conta;
		tb_ie_tipo_item_w(index_w)		:= r_C01_w.ie_tipo_item;
		tb_cd_procedimento_w(index_w)		:= r_C01_w.cd_procedimento;
		tb_dt_procedimento_w(index_w)		:= r_C01_w.dt_procedimento;
		ds_procedimento_w			:= r_C01_w.ds_procedimento;
		tb_ie_tipo_despesa_w(index_w)		:= r_C01_w.ie_tipo_despesa;
		tb_qt_procedimento_imp_w(index_w)	:= r_C01_w.qt_procedimento_imp;
		tb_vl_procedimento_imp_w(index_w)	:= r_C01_w.vl_procedimento_imp;
		tb_vl_proc_unitario_w(index_w)		:= r_C01_w.vl_proc_unitario;
		tb_vl_procedimento_w(index_w)		:= r_C01_w.vl_procedimento;
		tb_ie_origem_proced_w(index_w)		:= r_C01_w.ie_origem_proced;
		tb_ie_via_acesso_w(index_w)		:= r_C01_w.ie_via_acesso;
		tb_nr_auxiliares_w(index_w)		:= r_C01_w.nr_auxiliares;
		tb_cd_porte_anestesico_w(index_w)	:= r_C01_w.cd_porte_anestesico;
		tb_nr_seq_setor_atend_w(index_w)	:= r_C01_w.nr_seq_setor_atend;
		tb_ds_item_importacao_w(index_w)	:= r_C01_w.ds_item_imp;
		tb_vl_glosa_material_w(index_w)		:= r_C01_w.vl_glosa_material;
		tb_vl_glosa_hi_w(index_w)		:= r_C01_w.vl_glosa_hi;
		tb_vl_glosa_co_w(index_w)		:= r_C01_w.vl_glosa_co;
		tb_vl_custo_operacional_w(index_w)	:= r_C01_w.vl_custo_operacional;
		tb_vl_liberado_material_w(index_w)	:= r_C01_w.vl_liberado_material;
		tb_vl_liberado_co_w(index_w)		:= r_C01_w.vl_liberado_co;
		tb_vl_liberado_hi_w(index_w)		:= r_C01_w.vl_liberado_hi;
		tb_vl_calculado_material_w(index_w)	:= r_C01_w.vl_calc_mat_util;
		tb_vl_calculado_co_w(index_w)		:= r_C01_w.vl_calc_co_util;
		tb_vl_calculado_hi_w(index_w)		:= r_C01_w.vl_calc_hi_util;
		tb_tx_intercambio_w(index_w)		:= r_C01_w.tx_intercambio;
		tb_tx_intercambio_imp_w(index_w)	:= r_C01_w.tx_intercambio_imp;
		tb_qt_liberado_w(index_w)		:= r_C01_w.qt_procedimento;
		tb_tx_item_w(index_w)			:= r_C01_w.tx_item;
		tb_vl_glosa_w(index_w)			:= r_C01_w.vl_glosa;
		tb_vl_liberado_w(index_w)		:= r_C01_w.vl_liberado;
		tb_vl_unitario_apres_w(index_w)		:= r_C01_w.vl_unitario_imp;
		tb_vl_taxa_servico_imp_w(index_w)	:= r_C01_w.vl_taxa_servico_imp;
		tb_vl_taxa_co_imp_w(index_w)		:= r_C01_w.vl_taxa_co_imp;
		tb_vl_taxa_material_imp_w(index_w)	:= r_C01_w.vl_taxa_material_imp;
		tb_vl_taxa_servico_w(index_w)		:= r_C01_w.vl_taxa_servico;
		tb_vl_taxa_co_w(index_w)		:= r_C01_w.vl_taxa_co;
		tb_vl_taxa_material_w(index_w)		:= r_C01_w.vl_taxa_material;
		tb_ie_status_item_w(index_w)		:= r_C01_w.ie_status;
		tb_ie_valor_base_w(index_w)		:= r_C01_w.ie_valor_base;
		tb_ie_item_nao_encontrado_w(index_w)	:= r_C01_w.nao_encontrado;
		nr_seq_prestador_pgto_w			:= r_C01_w.nr_seq_prestador_pgto;
		tb_vl_lib_taxa_co_w(index_w)		:= r_C01_w.vl_lib_taxa_co;
		tb_vl_lib_taxa_material_w(index_w)	:= r_C01_w.vl_lib_taxa_material;
		tb_vl_lib_taxa_servico_w(index_w)	:= r_C01_w.vl_lib_taxa_servico;
		tb_vl_glosa_taxa_co_w(index_w)		:= r_C01_w.vl_glosa_taxa_co;
		tb_vl_glosa_taxa_material_w(index_w)	:= r_C01_w.vl_glosa_taxa_material;
		tb_vl_glosa_taxa_servico_w(index_w)	:= r_C01_w.vl_glosa_taxa_servico;
		ie_glosa_w				:= r_C01_w.ie_glosa;
		tb_tx_medico_w(index_w)			:= r_C01_w.tx_medico;
		tb_tx_material_w(index_w)     		:= r_C01_w.tx_material;
		tb_tx_custo_operacional_w(index_w)	:= r_C01_w.tx_custo_operacional;
		tb_ie_pagamento_w(index_w)		:= r_C01_w.ie_status_pagamento;
		tb_nr_seq_autogerado_w(index_w) 	:= r_C01_w.nr_seq_autogerado;
		tb_nr_identificador_w(index_w)		:= r_C01_w.nr_id_analise;
		nr_seq_grau_partic_w			:= r_C01_w.nr_seq_grau_partic;
		tb_vl_material_ptu_imp_w(index_w)  	:= r_C01_w.vl_material_ptu_imp;
		tb_vl_procedimento_ptu_imp_w(index_w)	:= r_C01_w.vl_procedimento_ptu_imp;
		tb_vl_co_ptu_imp_w(index_w)		:= r_C01_w.vl_co_ptu_imp;
		tb_ie_pacote_w(index_w)			:= r_C01_w.ie_pacote_ptu;
		tb_nr_seq_regra_qtde_exec_w(index_w)	:= r_C01_w.nr_seq_regra_qtde_exec;
		tb_cd_dente_w(index_w)			:= r_C01_w.cd_dente;
		tb_ds_dente_w(index_w)			:= r_C01_w.ds_dente;
		tb_cd_face_dente_w(index_w)		:= r_C01_w.cd_face_dente;
		tb_ds_face_dente_w(index_w)		:= r_C01_w.ds_face_dente;
		tb_cd_regiao_boca_w(index_w)		:= r_C01_w.cd_regiao_boca;
		tb_ds_regiao_boca_w(index_w)		:= r_C01_w.ds_regiao_boca;
		tb_ie_alto_custo_w(index_w)		:= r_C01_w.ie_alto_custo;
		tb_ds_complemento_w(index_w)		:= r_C01_w.ds_complemento;
		tb_seq_guia_w(index_w)			:= r_C01_w.nr_seq_guia;
		tb_ie_aviso_a520_proc_w(index_w)	:= r_C01_w.ie_a520;

		tb_ds_via_acesso_w(index_w)		:= null;
		tb_ie_pend_grupo_w(index_w)		:= null;
		tb_ie_pend_grupo_w(index_w)		:= pls_obter_pend_grupo_analise(nr_seq_analise_p,nr_seq_conta_p, tb_nr_seq_conta_proc_w(index_w),
											null, null, nr_seq_grupo_p, 'N');
		/* Obter o status geral da an�lise do item */
		tb_ie_status_analise_w(index_w)		:= pls_obter_status_analise_item(nr_seq_analise_p,null, tb_nr_seq_conta_proc_w(index_w), null,
											null, tb_ie_item_nao_encontrado_w(index_w),'N');
		if	(tb_ie_status_analise_w(index_w) = 'V') and
			(tb_ie_pend_grupo_w(index_w) = 'S') then
			tb_ie_status_analise_w(index_w) := 'A';
		end if;

		tb_vl_taxa_intercambio_imp_w(index_w)	:= 0;
		tb_vl_taxa_intercambio_w(index_w)	:= 0;
		tb_nm_prestador_pag_w(index_w)		:= null;
		tb_ds_status_item_w(index_w)		:= null;
		tb_ie_autorizado_w(index_w)			:= null;
		tb_ie_sem_fluxo_w(index_w)			:= null;
		tb_qt_liberar_w(index_w)			:= null;
		tb_ie_selecionado_w(index_w)		:= null;
		tb_ds_grau_partic_w(index_w) 		:= null;
		tb_cd_grau_partic_tiss_w(index_w)	:= null;
		tb_qt_cobranca_w(index_w)			:= 0;
		tb_ds_item_w(index_w)				:= null;
		tp_rede_min_w(index_w)				:= r_C01_w.tp_rede_min;

		if	(((ie_minha_analise_w = 'S') and
			(tb_ie_pend_grupo_w(index_w) is not null)) or
			(ie_minha_analise_w = 'N')) and
			((ie_pendentes_w = tb_ie_pend_grupo_w(index_w)) or
			(tb_ie_status_analise_w(index_w) in ('E','A')) or
			(ie_pendentes_w = 'N')) or
			(tb_vl_liberado_w(index_w) = 0 and tb_vl_procedimento_w(index_w) = 0 and --para listar cmo pendente nos casos de liber com valor apres e calc = 0...Ficava em an�lise e n�o era
			 tb_ie_status_item_w(index_w) = 'A') 				--listado como pendente e ao fechar ultimo grupo ele acusava item pendente devido ao status
			then
			if	(ie_ocultar_canc_w = 'N') or
				((ie_ocultar_canc_w = 'S') and
				(tb_ie_status_item_w(index_w) <> 'D')) then

				if	(r_c01_w.ds_desc_ptu is not null) then

					tb_ds_item_w(index_w)		:= ds_identacao_w || r_c01_w.ds_desc_ptu;
				else
					tb_ds_item_w(index_w)		:= ds_identacao_w || ds_procedimento_w;
				end if;

				tb_vl_taxa_intercambio_imp_w(index_w)	:= nvl(tb_vl_taxa_servico_imp_w(index_w),0) + nvl(tb_vl_taxa_co_imp_w(index_w),0) + nvl(tb_vl_taxa_material_imp_w(index_w),0);
				tb_vl_taxa_intercambio_w(index_w)	:= nvl(tb_vl_taxa_servico_w(index_w),0) + nvl(tb_vl_taxa_co_w(index_w),0) + nvl(tb_vl_taxa_material_w(index_w),0);

				if	(nr_seq_prestador_pgto_w is null) then
					select  max(nr_seq_prestador_pgto),
						max(nm_prestador_pgto)
					into	nr_seq_prestador_pgto_w,
						tb_nm_prestador_pag_w(index_w)
					from    pls_conta_medica_resumo
					where   nr_seq_conta_proc      = tb_nr_seq_conta_proc_w(index_w)
					and     nr_seq_conta           = nr_seq_conta_p
					and	((ie_situacao != 'I') or (ie_situacao is null));
				end if;

				if	(nr_seq_prestador_pgto_w is not null) then
					if	(tb_nm_prestador_pag_w(index_w) is null) then
						tb_nm_prestador_pag_w(index_w)		:= substr(pls_obter_dados_prestador(nr_seq_prestador_pgto_w,'N'),1,255);
					end if;
				end if;

				if	(tb_ie_status_item_w(index_w) is not null) then
					select	ds_valor_dominio
					into	tb_ds_status_item_w(index_w)
					from	valor_dominio_v
					where	cd_dominio	= 1870
					and	vl_dominio	= tb_ie_status_item_w(index_w);
				end if;

				if	(tb_ie_via_acesso_w(index_w) is not null) then
					select	ds_valor_dominio
					into	tb_ds_via_acesso_w(index_w)
					from	valor_dominio_v
					where	cd_dominio	= 1268
					and	vl_dominio	= tb_ie_via_acesso_w(index_w);
				end if;


				if	(nr_seq_guia_w is not null) then
					select	decode(count(1), 0, 'N', ie_autorizado_w)
					into	tb_ie_autorizado_w(index_w)
					from	pls_guia_plano_proc	a
					where	a.nr_seq_guia		= nr_seq_guia_w
					and	a.cd_procedimento	= tb_cd_procedimento_w(index_w)
					and	a.ie_origem_proced	= tb_ie_origem_proced_w(index_w);
				end if;

				if	(tb_ie_pagamento_w(index_w) is null) then
					tb_ie_pagamento_w(index_w)	:= pls_analise_obter_status_pag(tb_ie_status_item_w(index_w), tb_qt_procedimento_imp_w(index_w), tb_qt_liberado_w(index_w),
													tb_vl_procedimento_imp_w(index_w), tb_vl_procedimento_w(index_w), tb_vl_liberado_w(index_w),
													tb_vl_glosa_w(index_w), ie_glosa_w);
				end if;

				/* Obter se possui fluxo de an�lise */
				tb_ie_sem_fluxo_w(index_w)	:= pls_obter_se_item_sem_fluxo(nr_seq_analise_p,null, tb_nr_seq_conta_proc_w(index_w), null,null);

				tb_nr_sequencia_w(index_w)	:= pls_consulta_analise_pck.get_nr_seq_item;

				if	(pls_consulta_analise_pck.get_se_selecao) then
					select	count(1),
						max(qt_liberar)
					into	qt_selecao_w,
						tb_qt_liberar_w(index_w)
					from	w_pls_analise_selecao_item	a
					where	a.nr_seq_analise	= nr_seq_analise_p
					and	a.nr_seq_w_item		= tb_nr_sequencia_w(index_w);
				end if;

				if	(qt_selecao_w > 0) then
					tb_ie_selecionado_w(index_w)	:= 'S';
				else
					tb_ie_selecionado_w(index_w)	:= 'N';
				end if;

				if(nr_seq_grau_partic_w is null) then
					select 	count(nr_sequencia)
					into 	nr_total_partic_w
					from 	pls_proc_participante
					where 	nr_seq_conta_proc = tb_nr_seq_conta_proc_w(index_w)
					and 	ie_status != 'C';

					--Se possuir apenas um participante, mostrar o grau de participa��o no registro principal do procedimento
					if(nr_total_partic_w = 1) then
						select 	max(nr_seq_grau_partic)
						into 	nr_seq_grau_partic_w
						from 	pls_proc_participante
						where 	nr_seq_conta_proc = tb_nr_seq_conta_proc_w(index_w)
						and 	ie_status != 'C';
					end if;
				end if;

				if	(nr_seq_grau_partic_w is not null) then
					select	substr(ds_grau_participacao,1,255),
						cd_tiss
					into	tb_ds_grau_partic_w(index_w),
						tb_cd_grau_partic_tiss_w(index_w)
					from	pls_grau_participacao
					where	nr_sequencia	= nr_seq_grau_partic_w;
				end if;

				tb_qt_cobranca_w(index_w)	:= 0;
				begin
				select	nvl(a.qt_item,0)
				into	tb_qt_cobranca_w(index_w)
				from	pls_conta_pos_estabelecido a
				where	a.nr_seq_conta_proc = tb_nr_seq_conta_proc_w(index_w);
				exception
					when others then
					tb_qt_cobranca_w(index_w)	:= 0;
				end;

				if	(tb_ie_status_item_w(index_w) <> 'D') then
					begin
					pls_consulta_analise_pck.set_vl_apresentado(tb_vl_procedimento_imp_w(index_w));
					pls_consulta_analise_pck.set_vl_calculado(tb_vl_procedimento_w(index_w));
					pls_consulta_analise_pck.set_vl_liberado(tb_vl_liberado_w(index_w));
					pls_consulta_analise_pck.set_vl_glosado(tb_vl_glosa_w(index_w));
					pls_consulta_analise_pck.set_vl_taxa(tb_vl_taxa_servico_w(index_w));
					end;
				end if;

				pls_consulta_analise_pck.set_nr_seq_item(tb_nr_sequencia_w(index_w) + 1);
				index_w := index_w + 1;
			end if;
		end if;

	end loop;
	if	(tb_nr_sequencia_w.count > 0) then
		forall i in tb_nr_sequencia_w.first..tb_nr_sequencia_w.last
			insert into w_pls_analise_item (nr_sequencia, nm_usuario, dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec,
							nr_seq_analise, nr_seq_conta, nr_seq_conta_proc, cd_item, dt_item,
							ds_item, ie_tipo_despesa, ie_tipo_linha, ie_tipo_item, qt_apresentada,
							vl_apresentado, vl_calculado_unitario, vl_calculado, cd_procedimento, ie_origem_proced,
							ie_via_acesso, ds_via_acesso, nm_prestador_solic, nr_auxiliares, cd_porte_anestesico,
							ds_tipo_guia, nm_prestador_exec, nm_prestador_pag, ds_espec_cbo, nr_seq_protocolo,
							ie_autoriz_previa, cd_guia, ds_setor_atend, ds_item_importacao, ds_medico_executor,
							vl_glosa_material, vl_glosa_hi, vl_glosa_co, vl_custo_operacional, vl_liberado_material,
							vl_liberado_co, vl_liberado_hi, vl_calculado_material, vl_calculado_co, vl_calculado_hi,
							tx_intercambio, tx_intercambio_imp, vl_taxa_intercambio, vl_taxa_intercambio_imp, qt_liberado,
							tx_item, vl_glosa, vl_liberado, vl_unitario_apres, nr_seq_prestador_exec,
							nr_seq_guia, ie_status_item, ds_status_item, ie_status_analise, ie_pend_grupo,
							ie_pagamento, ie_item_nao_encontrado, ie_selecionado, qt_liberar, ie_sem_fluxo,
							ie_tipo_guia, vl_lib_taxa_co, vl_lib_taxa_material, vl_lib_taxa_servico, vl_glosa_taxa_co,
							vl_glosa_taxa_material, vl_glosa_taxa_servico, vl_taxa_servico_imp, vl_taxa_co_imp, vl_taxa_material_imp,
							vl_taxa_servico, vl_taxa_co, vl_taxa_material, ie_valor_base, tx_medico,
							tx_material, tx_custo_operacional, nr_seq_autogerado, nr_identificador, ds_grau_partic,
							cd_grau_partic_tiss, vl_material_ptu_imp, vl_procedimento_ptu_imp, vl_co_ptu_imp, ie_pacote_ptu,
							qt_cobranca, nr_seq_regra_qtde_exec, cd_dente, ds_dente, cd_face_dente,
							ds_face_dente, cd_regiao_boca, ds_regiao_boca, ie_alto_custo, ds_complemento,
							tp_rede_min, nr_id_transacao, ie_a520)
			values	(tb_nr_sequencia_w(i), nm_usuario_p, sysdate, nm_usuario_p, sysdate,
				nr_seq_analise_p, nr_seq_conta_p, tb_nr_seq_conta_proc_w(i), tb_cd_procedimento_w(i), tb_dt_procedimento_w(i),
				tb_ds_item_w(i), tb_ie_tipo_despesa_w(i), tb_ie_tipo_linha_w(i), tb_ie_tipo_item_w(i), tb_qt_procedimento_imp_w(i),
				tb_vl_procedimento_imp_w(i), tb_vl_proc_unitario_w(i), tb_vl_procedimento_w(i), tb_cd_procedimento_w(i), tb_ie_origem_proced_w(i),
				tb_ie_via_acesso_w(i), tb_ds_via_acesso_w(i), nm_prestador_solic_w, tb_nr_auxiliares_w(i), tb_cd_porte_anestesico_w(i),
				ds_tipo_guia_w, nm_prestador_exec_w, tb_nm_prestador_pag_w(i), ds_espec_cbo_w, nr_seq_protocolo_w,
				tb_ie_autorizado_w(i), cd_guia_w, null, tb_ds_item_importacao_w(i), ds_medico_executor_w,
				tb_vl_glosa_material_w(i), tb_vl_glosa_hi_w(i), tb_vl_glosa_co_w(i), tb_vl_custo_operacional_w(i), tb_vl_liberado_material_w(i),
				tb_vl_liberado_co_w(i), tb_vl_liberado_hi_w(i), tb_vl_calculado_material_w(i), tb_vl_calculado_co_w(i), tb_vl_calculado_hi_w(i),
				tb_tx_intercambio_w(i), tb_tx_intercambio_imp_w(i), tb_vl_taxa_intercambio_w(i), tb_vl_taxa_intercambio_imp_w(i), tb_qt_liberado_w(i),
				tb_tx_item_w(i), tb_vl_glosa_w(i), tb_vl_liberado_w(i), tb_vl_unitario_apres_w(i), nr_seq_prestador_exec_w,
				tb_seq_guia_w(i), tb_ie_status_item_w(i), tb_ds_status_item_w(i), tb_ie_status_analise_w(i), tb_ie_pend_grupo_w(i),
				tb_ie_pagamento_w(i), tb_ie_item_nao_encontrado_w(i), tb_ie_selecionado_w(i), tb_qt_liberar_w(i), tb_ie_sem_fluxo_w(i),
				ie_tipo_guia_w, tb_vl_lib_taxa_co_w(i), tb_vl_lib_taxa_material_w(i), tb_vl_lib_taxa_servico_w(i), tb_vl_glosa_taxa_co_w(i),
				tb_vl_glosa_taxa_material_w(i), tb_vl_glosa_taxa_servico_w(i), tb_vl_taxa_servico_imp_w(i), tb_vl_taxa_co_imp_w(i), tb_vl_taxa_material_imp_w(i),
				tb_vl_taxa_servico_w(i), tb_vl_taxa_co_w(i), tb_vl_taxa_material_w(i), tb_ie_valor_base_w(i), tb_tx_medico_w(i),
				tb_tx_material_w(i), tb_tx_custo_operacional_w(i), tb_nr_seq_autogerado_w(i), tb_nr_identificador_w(i), tb_ds_grau_partic_w(i),
				tb_cd_grau_partic_tiss_w(i), tb_vl_material_ptu_imp_w(i), tb_vl_procedimento_ptu_imp_w(i), tb_vl_co_ptu_imp_w(i), tb_ie_pacote_w(i),
				tb_qt_cobranca_w(i), tb_nr_seq_regra_qtde_exec_w(i), tb_cd_dente_w(i), tb_ds_dente_w(i), tb_cd_face_dente_w(i),
				tb_ds_face_dente_w(i), tb_cd_regiao_boca_w(i), tb_ds_regiao_boca_w(i), tb_ie_alto_custo_w(i), tb_ds_complemento_w(i),
				tp_rede_min_w(i), nr_id_transacao_p, tb_ie_aviso_a520_proc_w(i));
		commit;

		tb_ds_item_w.delete;
		tb_ds_via_acesso_w.delete;
		tb_nm_prestador_pag_w.delete;
		tb_ds_item_importacao_w.delete;
		tb_ds_status_item_w.delete;
		tb_ie_autorizado_w.delete;
		tb_ie_tipo_linha_w.delete;
		tb_ie_tipo_despesa_w.delete;
		tb_ie_tipo_item_w.delete;
		tb_ie_via_acesso_w.delete;
		tb_cd_porte_anestesico_w.delete;
		tb_ie_pagamento_w.delete;
		tb_ie_status_item_w.delete;
		tb_ie_status_analise_w.delete;
		tb_ie_valor_base_w.delete;
		tb_ie_pend_grupo_w.delete;
		tb_ie_item_nao_encontrado_w.delete;
		tb_ie_selecionado_w.delete;
		tb_ie_sem_fluxo_w.delete;
		tb_vl_taxa_servico_imp_w.delete;
		tb_vl_taxa_co_imp_w.delete;
		tb_vl_taxa_material_imp_w.delete;
		tb_vl_taxa_servico_w.delete;
		tb_vl_taxa_co_w.delete;
		tb_vl_taxa_material_w.delete;
		tb_tx_medico_w.delete;
		tb_tx_material_w.delete;
		tb_tx_custo_operacional_w.delete;
		tb_vl_procedimento_imp_w.delete;
		tb_vl_proc_unitario_w.delete;
		tb_vl_procedimento_w.delete;
		tb_vl_glosa_w.delete;
		tb_vl_liberado_w.delete;
		tb_vl_glosa_material_w.delete;
		tb_vl_glosa_hi_w.delete;
		tb_vl_glosa_co_w.delete;
		tb_vl_custo_operacional_w.delete;
		tb_vl_liberado_material_w.delete;
		tb_vl_liberado_co_w.delete;
		tb_vl_liberado_hi_w.delete;
		tb_vl_calculado_material_w.delete;
		tb_vl_calculado_co_w.delete;
		tb_vl_calculado_hi_w.delete;
		tb_vl_taxa_intercambio_w.delete;
		tb_vl_taxa_intercambio_imp_w.delete;
		tb_vl_unitario_apres_w.delete;
		tb_vl_lib_taxa_co_w.delete;
		tb_vl_lib_taxa_material_w.delete;
		tb_vl_lib_taxa_servico_w.delete;
		tb_vl_glosa_taxa_co_w.delete;
		tb_vl_glosa_taxa_material_w.delete;
		tb_vl_glosa_taxa_servico_w.delete;
		tb_vl_material_ptu_imp_w.delete;
		tb_vl_procedimento_ptu_imp_w.delete;
		tb_vl_co_ptu_imp_w.delete;
		tb_cd_procedimento_w.delete;
		tb_qt_liberado_w.delete;
		tb_qt_procedimento_imp_w.delete;
		tb_qt_liberar_w.delete;
		tb_nr_seq_conta_proc_w.delete;
		tb_ie_origem_proced_w.delete;
		tb_nr_seq_setor_atend_w.delete;
		tb_nr_auxiliares_w.delete;
		tb_nr_sequencia_w.delete;
		tb_nr_seq_autogerado_w.delete;
		tb_nr_identificador_w.delete;
		tb_tx_item_w.delete;
		tb_tx_intercambio_w.delete;
		tb_tx_intercambio_imp_w.delete;
		tb_dt_procedimento_w.delete;
		tb_ds_grau_partic_w.delete;
		tb_cd_grau_partic_tiss_w.delete;
		tb_ie_pacote_w.delete;
		tb_qt_cobranca_w.delete;
		tb_nr_seq_regra_qtde_exec_w.delete;
		tb_cd_dente_w.delete;
		tb_ds_dente_w.delete;
		tb_cd_regiao_boca_w.delete;
		tb_ds_regiao_boca_w.delete;
		tb_cd_face_dente_w.delete;
		tb_ds_face_dente_w.delete;
		tb_ie_alto_custo_w.delete;
		tb_ds_complemento_w.delete;
		tb_seq_guia_w.delete;
		tp_rede_min_w.delete;
		tb_ie_aviso_a520_proc_w.delete;
	end if;
end if;

end pls_gerar_w_analise_proc_html;
/
