create or replace
procedure gca_atualizar_item(		nr_sequencia_p		number,
									NR_SEQ_RESULT_p		number,
									nm_usuario_p		Varchar2) is 

begin

update	GCA_ATEND_RESULT
set		NR_SEQ_RESULT  	= NR_SEQ_RESULT_p,
		dt_atualizacao 	= sysdate,
		nm_usuario 		= nm_usuario_p
where	nr_sequencia  = nr_sequencia_p;

commit;

end gca_atualizar_item;
/