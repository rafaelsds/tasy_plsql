create or replace
procedure consistir_parecer_medico(	
				cd_profissional_p		number,
				cd_especialidade_prof_p		number,
				cd_especialidade_dest_prof_p	number,
				cd_pessoa_parecer_p		Varchar2,
				nr_parecer_p			number,
				nr_atendimento_p		number default 0,
				nm_usuario_p			Varchar2) is 

nr_parecer_w		number(10);
qt_parecer_w		number(5);
nm_pessoa_parecer_w	varchar2(255);
nm_medico		varchar2(255);
ie_parecer_w		varchar2(1);
ie_consiste_w		varchar2(1);
nr_atendimento_w	number(10);

begin
Obter_Param_Usuario(281, 745, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_parecer_w);
Obter_Param_Usuario(281, 1217, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_consiste_w);

select	max(nr_atendimento)
into	nr_atendimento_w
from	parecer_medico_req
where	nr_parecer = nr_parecer_p;

if	(nvl(nr_atendimento_p,0) > 0) and
	(nvl(nr_atendimento_w,0) = 0) then
	nr_atendimento_w := nr_atendimento_p;
end if;

if	(ie_parecer_w = 'S') and
	(ie_consiste_w = 'S') then
	begin
	select	nvl(max(nr_parecer),0)
	into	nr_parecer_w
	from	parecer_medico_req
	where	cd_medico = cd_profissional_p
	--and	cd_especialidade_prof = cd_especialidade_prof_p
	and	nr_parecer <> nr_parecer_p
	and	(cd_especialidade_dest_prof = cd_especialidade_dest_prof_p or cd_pessoa_parecer = cd_pessoa_parecer_p)
	and	ie_situacao = 'A';

	if	(nr_parecer_w > 0) then
		select	count(*)
		into	qt_parecer_w
		from	parecer_medico
		where	nr_parecer = nr_parecer_w
		and	nvl(ie_status,'F') = 'F';

		if	(qt_parecer_w = 0) then
			select	max(substr(obter_nome_pf(cd_pessoa_parecer),1,255)),
				max(substr(obter_nome_pf(cd_medico),1,255))
			into	nm_pessoa_parecer_w,
				nm_medico
			from	parecer_medico_req
			where	nr_parecer = nr_parecer_w;
			
			wheb_mensagem_pck.exibir_mensagem_abort(192954,'NR_PARECER=' || nr_parecer_w);
		end if;
	end if;
	end;
elsif	(ie_parecer_w = 'N') and
	(ie_consiste_w = 'S') then
	begin
	select	nvl(max(nr_parecer),0)
	into	nr_parecer_w
	from	parecer_medico_req
	where	cd_medico = cd_profissional_p
	--and	cd_especialidade = cd_especialidade_prof_p
	and	nr_parecer <> nr_parecer_p
	and	(cd_especialidade_dest = cd_especialidade_dest_prof_p or cd_pessoa_parecer = cd_pessoa_parecer_p)
	and	nr_atendimento = nr_atendimento_w
	and	ie_situacao = 'A';

	if	(nr_parecer_w > 0) then
		select	count(*)
		into	qt_parecer_w
		from	parecer_medico
		where	nr_parecer = nr_parecer_w
		and	nvl(ie_status,'F') = 'F';
		
		if	(qt_parecer_w = 0) then
			select	max(substr(obter_nome_pf(cd_pessoa_parecer),1,255)),
				max(substr(obter_nome_pf(cd_medico),1,255))
			into	nm_pessoa_parecer_w,
				nm_medico
			from	parecer_medico_req
			where	nr_parecer = nr_parecer_w;
			
			wheb_mensagem_pck.exibir_mensagem_abort(192954,'NR_PARECER=' || nr_parecer_w);
		end if;		

	end if;


	end;
end if;

commit;

end consistir_parecer_medico;
/