create or replace
procedure Ageint_Gerar_Copia_Grupo_Proc(
			nr_seq_grupo_p		number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			nr_seq_regra_p	out	 number) is 

nr_seq_proc_interno_w	number(10);
ds_grupo_w		varchar2(80);
nr_seq_grupo_w		number(10);
			
Cursor C01 is
	select	nr_Seq_proc_interno
	from	agenda_int_grupo_item
	where	nr_Seq_grupo	= nr_Seq_grupo_p
	and	nr_seq_proc_interno is not null
	order by nr_Seq_Apres;
			
begin

select	max(ds_grupo)
into	ds_grupo_w
from	agenda_int_grupo
where	nr_sequencia	= nr_seq_grupo_p;

select	ageint_regra_grupo_proc_seq.nextval
into	nr_seq_grupo_w
from	dual;

insert into ageint_regra_grupo_proc
	(nr_Sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_Atualizacao_nrec,
	nm_usuario_nrec,
	ie_situacao,
	ds_grupo)
values	
	(nr_Seq_grupo_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	'A',
	ds_grupo_w);
	
open C01;
loop
fetch C01 into	
	nr_seq_proc_interno_w;
exit when C01%notfound;
	begin
	insert into ageint_proced_regra
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_Seq_proc_interno,
		ie_situacao,
		nr_seq_grupo)
	values
		(ageint_proced_regra_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_proc_interno_w,
		'A',
		nr_seq_grupo_w);
	end;
end loop;
close C01;

nr_seq_regra_p	:= nr_seq_grupo_w;

commit;

end Ageint_Gerar_Copia_Grupo_Proc;
/