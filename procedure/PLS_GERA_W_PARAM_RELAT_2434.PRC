create or replace
procedure pls_gera_w_param_relat_2434(
			dt_inicio_p		w_pls_param_relat_2434.dt_inicio%type,
			dt_fim_p		w_pls_param_relat_2434.dt_fim%type,
			nr_contrato_p		w_pls_param_relat_2434.nr_contrato%Type,
			ie_nivel_p		w_pls_param_relat_2434.ie_nivel%type,
			vl_carregamento_pj_p	w_pls_param_relat_2434.vl_carregamento_pj%type,
			vl_carregamento_pf_nr_p	w_pls_param_relat_2434.vl_carregamento_pf_nr%type,
			vl_carregamento_pf_r_p	w_pls_param_relat_2434.vl_carregamento_pf_r%type) is 
begin

execute immediate ' truncate table w_pls_param_relat_2434 ';

insert into w_pls_param_relat_2434 
	(nr_sequencia, dt_inicio,dt_fim,nr_contrato,ie_nivel,vl_carregamento_pj,vl_carregamento_pf_nr,vl_carregamento_pf_r)
values 	(w_pls_param_relat_2434_seq.nextval, dt_inicio_p, dt_fim_p, nr_contrato_p,ie_nivel_p,vl_carregamento_pj_p,vl_carregamento_pf_nr_p,vl_carregamento_pf_r_p);

commit;

end pls_gera_w_param_relat_2434;
/
