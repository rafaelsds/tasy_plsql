create or replace
procedure pls_dividir_fatura_taxa (	nr_seq_lote_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2,
					ie_origem_conta_p		varchar2 default 'A') is 

nr_seq_regra_fat_w		pls_lote_faturamento.nr_sequencia%type;
ie_div_fat_taxa_w		pls_regra_faturamento.ie_div_fat_taxa%type;
nr_seq_fatura_w			pls_fatura.nr_sequencia%type;
nr_seq_pagador_w		pls_fatura.nr_seq_pagador%type;
ie_impedimento_cobranca_w	pls_fatura.ie_impedimento_cobranca%type;
nr_agrupamento_fat_w		pls_fatura.nr_agrupamento%type;
nr_seq_fatura_nova_w		pls_fatura.nr_sequencia%type := null;
nr_lote_contabil_w		pls_fatura_conta.nr_lote_contabil%type;
vl_faturado_w			pls_fatura_proc.vl_faturado%type;
nr_seq_fatura_mat_w		pls_fatura_mat.nr_sequencia%type;
vl_faturado_mat_w		pls_fatura_mat.vl_faturado%type;
vl_faturado_proc_w		pls_fatura_proc.vl_faturado%type;
nr_seq_evento_pagador_w		number(15);
nr_seq_evento_fatura_w		number(15);
nr_seq_evento_w			number(15);
nr_seq_conta_w			number(15);
nr_seq_fatura_evento_w		number(15);
nr_seq_segurado_w		number(15);
nr_seq_fatura_conta_w		number(15);
nr_seq_fatura_proc_w		pls_fatura_proc.nr_sequencia%type;
ie_fatura_taxa_w		pls_fatura.ie_fatura_taxa%type;
ie_forma_impedimento_cobr_w	pls_regra_faturamento.ie_forma_impedimento_cobr%type;
nr_seq_fat_conta_w		pls_fatura_conta.nr_sequencia%type;
ie_tipo_cobr_fat_conta_w	pls_fatura_conta.ie_tipo_cobranca%type;
cd_guia_referencia_w		pls_fatura_conta.cd_guia_referencia%type;
nr_seq_congenere_w		pls_fatura_conta.nr_seq_congenere%type;
nr_seq_fat_conta_ant_w		pls_fatura_conta.nr_sequencia%type;


cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_pagador,
		a.ie_impedimento_cobranca,
		a.nr_agrupamento,
		a.ie_fatura_taxa
	from	pls_fatura		a
	where	a.nr_seq_lote	= nr_seq_lote_p
	order by a.nr_seq_pagador;
	
cursor c02 is
	select	b.nr_seq_conta,
		a.nr_seq_evento,
		a.nr_sequencia,
		b.nr_seq_segurado,
		b.nr_sequencia,
		d.nr_sequencia,
		null seq_mat,
		b.nr_lote_contabil,
		b.ie_tipo_cobranca,
		b.cd_guia_referencia,
		b.nr_seq_congenere
	from	pls_fatura_proc		d,
		pls_segurado		c,
		pls_fatura_conta 	b,
		pls_fatura_evento 	a
	where	a.nr_seq_fatura 	= nr_seq_fatura_w
	and	a.nr_sequencia		= b.nr_seq_fatura_evento
	and	c.nr_sequencia		= b.nr_seq_segurado
	and	b.nr_sequencia		= d.nr_seq_fatura_conta
	and	d.vl_faturado		> 0
	group by b.nr_seq_conta,
		a.nr_seq_evento,
		a.nr_sequencia,
		b.nr_seq_segurado,
		b.nr_sequencia,
		d.nr_sequencia,
		d.ie_tipo_cobranca,
		b.nr_lote_contabil,
		b.ie_tipo_cobranca,
		b.cd_guia_referencia,
		b.nr_seq_congenere
	union all
	select	b.nr_seq_conta,
		a.nr_seq_evento,
		a.nr_sequencia,
		b.nr_seq_segurado,
		b.nr_sequencia,
		null seq_proc,
		d.nr_sequencia,
		b.nr_lote_contabil,
		b.ie_tipo_cobranca,
		b.cd_guia_referencia,
		b.nr_seq_congenere
	from	pls_fatura_mat		d,
		pls_segurado		c,
		pls_fatura_conta 	b,
		pls_fatura_evento 	a
	where	a.nr_seq_fatura 	= nr_seq_fatura_w
	and	a.nr_sequencia		= b.nr_seq_fatura_evento
	and	c.nr_sequencia		= b.nr_seq_segurado
	and	b.nr_sequencia		= d.nr_seq_fatura_conta
	and	d.vl_faturado		> 0
	group by b.nr_seq_conta,
		a.nr_seq_evento,
		a.nr_sequencia,
		b.nr_seq_segurado,
		b.nr_sequencia,
		d.nr_sequencia,
		d.ie_tipo_cobranca,
		b.nr_lote_contabil,
		b.ie_tipo_cobranca,
		b.cd_guia_referencia,
		b.nr_seq_congenere;

begin

if	(nr_seq_lote_p is not null) then
	select	max(a.nr_seq_regra_fat)
	into	nr_seq_regra_fat_w
	from	pls_lote_faturamento a
	where	a.nr_sequencia	= nr_seq_lote_p;

	-- Verificar se tem regra de divisao de taxa marcada
	select	nvl(max(ie_div_fat_taxa),'N'),
		nvl(max(ie_forma_impedimento_cobr),'U')
	into	ie_div_fat_taxa_w,
		ie_forma_impedimento_cobr_w
	from	pls_regra_faturamento
	where	nr_sequencia = nr_seq_regra_fat_w;
	
	if	(ie_div_fat_taxa_w = 'S') then
		open C01;
		loop
		fetch C01 into	
			nr_seq_fatura_w,
			nr_seq_pagador_w,
			ie_impedimento_cobranca_w,
			nr_agrupamento_fat_w,
			ie_fatura_taxa_w;
		exit when C01%notfound;
			begin
				nr_seq_fatura_nova_w	:= null;
				
			open C02;
			loop
			fetch C02 into	
				nr_seq_conta_w,
				nr_seq_evento_w,
				nr_seq_fatura_evento_w,
				nr_seq_segurado_w,
				nr_seq_fatura_conta_w,
				nr_seq_fatura_proc_w,
				nr_seq_fatura_mat_w,
				nr_lote_contabil_w,
				ie_tipo_cobr_fat_conta_w,
				cd_guia_referencia_w,
				nr_seq_congenere_w;
			exit when C02%notfound;
				begin
				
				if	(ie_forma_impedimento_cobr_w = 'U') then
					select	max(b.nr_sequencia),
						max(c.nr_sequencia)
					into	nr_seq_evento_pagador_w,
						nr_seq_fat_conta_w
					from	pls_segurado		d,
						pls_fatura_conta	c,
						pls_fatura_evento	b,
						pls_fatura		a
					where	a.nr_sequencia 		= b.nr_seq_fatura
					and	b.nr_sequencia 		= c.nr_seq_fatura_evento
					and	d.nr_sequencia		= c.nr_seq_segurado
					and	c.nr_sequencia		<> nr_seq_fatura_conta_w
					and	a.nr_sequencia		<> nr_seq_fatura_w
					and	a.nr_seq_pagador 	= nr_seq_pagador_w
					and	a.nr_seq_lote  		= nr_seq_lote_p
					and	nvl(a.ie_impedimento_cobranca,'X') = nvl(ie_impedimento_cobranca_w,'X')
					and	a.nr_agrupamento	= nvl(nr_agrupamento_fat_w,a.nr_agrupamento);
					
				elsif	(ie_forma_impedimento_cobr_w = 'A') then
					select	max(b.nr_sequencia),
						max(c.nr_sequencia)
					into	nr_seq_evento_pagador_w,
						nr_seq_fat_conta_w
					from	pls_segurado		d,
						pls_fatura_conta	c,
						pls_fatura_evento	b,
						pls_fatura		a
					where	a.nr_sequencia 		= b.nr_seq_fatura
					and	b.nr_sequencia 		= c.nr_seq_fatura_evento
					and	d.nr_sequencia		= c.nr_seq_segurado
					and	c.nr_sequencia		<> nr_seq_fatura_conta_w
					and	a.nr_sequencia		<> nr_seq_fatura_w
					and	a.nr_seq_pagador 	= nr_seq_pagador_w
					and	a.nr_seq_lote  		= nr_seq_lote_p
					and	nvl(a.ie_impedimento_cobranca,'X') = nvl(ie_impedimento_cobranca_w,'X');
				else
					select	max(b.nr_sequencia),
						max(c.nr_sequencia)
					into	nr_seq_evento_pagador_w,
						nr_seq_fat_conta_w
					from	pls_segurado		d,
						pls_fatura_conta	c,
						pls_fatura_evento	b,
						pls_fatura		a
					where	a.nr_sequencia 		= b.nr_seq_fatura
					and	b.nr_sequencia 		= c.nr_seq_fatura_evento
					and	d.nr_sequencia		= c.nr_seq_segurado
					and	c.nr_sequencia		<> nr_seq_fatura_conta_w
					and	a.nr_sequencia		<> nr_seq_fatura_w
					and	a.nr_seq_pagador 	= nr_seq_pagador_w
					and	a.nr_seq_lote  		= nr_seq_lote_p
					and	a.nr_agrupamento	= nvl(nr_agrupamento_fat_w,a.nr_agrupamento);
				end if;
				
				if	(nr_seq_fat_conta_w is null) and (nvl(nr_seq_evento_pagador_w, nr_seq_fatura_evento_w) = nr_seq_fatura_evento_w)  then
				
					--Cria uma nova fatura 
					select	pls_fatura_seq.nextval
					into	nr_seq_fatura_nova_w
					from	dual;
					
					insert into pls_fatura
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_pagador,
						nr_seq_lote,
						dt_vencimento,
						nr_seq_congenere,
						dt_mes_competencia,
						vl_fatura,
						dt_vencimento_ndc,
						vl_total_ndc,
						nr_fatura,
						nr_seq_fat_divisao,
						ie_impedimento_cobranca,
						nr_agrupamento,
						ie_remido,
						ie_fatura_taxa,
						ie_tipo_fatura)
					select	nr_seq_fatura_nova_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_pagador,
						nr_seq_lote,
						dt_vencimento,
						nr_seq_congenere,
						dt_mes_competencia,
						0,
						dt_vencimento_ndc,
						0,
						nr_seq_fatura_nova_w,
						nr_seq_fatura_w,
						ie_impedimento_cobranca_w,
						nr_agrupamento_fat_w,
						ie_remido,
						'S',
						ie_tipo_fatura
					from	pls_fatura
					where	nr_sequencia = nr_seq_fatura_w;
				end if;
					
				if	(nr_seq_fatura_nova_w is not null) then
					select	max(nr_sequencia)
					into	nr_seq_evento_fatura_w
					from 	pls_fatura_evento
					where	nr_seq_fatura = nr_seq_fatura_nova_w
					and	nr_seq_evento = nr_seq_evento_w;
					
					if	(nr_seq_evento_fatura_w is null) then
						select	pls_fatura_evento_seq.nextval
						into	nr_seq_evento_fatura_w
						from	dual;
						
						insert into pls_fatura_evento
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_evento,
							nr_seq_fatura,
							vl_evento)
						values	(nr_seq_evento_fatura_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_evento_w,
							nr_seq_fatura_nova_w,
							0);
					end if;	
					
					select	max(nr_sequencia)
					into	nr_seq_fatura_conta_w
					from 	pls_fatura_conta
					where	nr_seq_fatura_evento	= nr_seq_evento_fatura_w
					and	nr_seq_conta		= nr_seq_conta_w;
					
					if	(nr_seq_fatura_conta_w is null) then
						select	pls_fatura_conta_seq.nextval
						into	nr_seq_fatura_conta_w
						from	dual;
						
						insert into pls_fatura_conta
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_fatura_evento,
							nr_seq_conta,
							nr_lote_contabil,
							nr_seq_segurado,
							vl_faturado,
							ie_tipo_cobranca,
							cd_guia_referencia,
							ie_tipo_vinculacao,
							nr_seq_congenere)
						values	(nr_seq_fatura_conta_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_evento_fatura_w,
							nr_seq_conta_w,
							nr_lote_contabil_w,
							nr_seq_segurado_w,
							0,
							ie_tipo_cobr_fat_conta_w,
							cd_guia_referencia_w,
							ie_origem_conta_p,
							nr_seq_congenere_w);
							
						if	(nr_seq_fatura_proc_w is not null) then
						
							insert into	pls_fatura_proc
									(cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									ie_tipo_cobranca,
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_conta_proc,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_proc_cancel,
									nr_seq_fatura_conta,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_proc,
									nr_seq_pos_proc_fat,
									nr_seq_pos_proc_tx,
									nr_seq_pos_proc_tx_fat,
									nr_seq_pos_taxa_contab,
									nr_sequencia,
									vl_custo_operacional,
									vl_faturado,
									vl_faturado_ndc,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									vl_materiais,
									vl_medico)
								select	cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									'3',
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_conta_proc,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_proc_cancel,
									nr_seq_fatura_conta_w,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_proc,
									nr_seq_pos_proc_fat,
									nr_seq_pos_proc_tx,
									nr_seq_pos_proc_tx_fat,
									nr_seq_pos_taxa_contab,
									pls_fatura_proc_seq.nextval,
									0,
									vl_faturado,
									0,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									0,
									0
								from	pls_fatura_proc
								where	nr_sequencia	= nr_seq_fatura_proc_w;
								
							update	pls_fatura_proc
							set	vl_faturado		= 0,
								vl_lib_taxa_servico	= 0,
								vl_lib_taxa_co		= 0,
								vl_lib_taxa_material	= 0
							where	nr_sequencia		= nr_seq_fatura_proc_w;
						
						elsif	(nr_seq_fatura_mat_w is not null) then
						
							insert into	pls_fatura_mat
									(cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									ie_tipo_cobranca,
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_mat,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_mat_cancel,
									nr_seq_fatura_conta,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_mat,
									nr_seq_pos_mat_fat,
									nr_seq_pos_mat_tx,
									nr_seq_pos_mat_tx_fat,
									nr_seq_pos_taxa_contab,
									nr_sequencia,
									vl_custo_operacional,
									vl_faturado,
									vl_faturado_ndc,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									vl_materiais,
									vl_medico)
								select	cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									'3',
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_mat,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_mat_cancel,
									nr_seq_fatura_conta_w,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_mat,
									nr_seq_pos_mat_fat,
									nr_seq_pos_mat_tx,
									nr_seq_pos_mat_tx_fat,
									nr_seq_pos_taxa_contab,
									pls_fatura_mat_seq.nextval,
									0,
									vl_faturado,
									0,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									0,
									0
								from	pls_fatura_mat
								where	nr_sequencia	= nr_seq_fatura_mat_w;
								
							update	pls_fatura_mat
							set	vl_faturado		= 0,
								vl_lib_taxa_co		= 0,
								vl_lib_taxa_material	= 0,
								vl_lib_taxa_servico	= 0
							where	nr_sequencia		= nr_seq_fatura_mat_w;
						end if;
					elsif 	(nr_seq_evento_pagador_w is not null) then
					
						if	(nr_seq_fatura_proc_w is not null) then
						
							insert into	pls_fatura_proc
									(cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									ie_tipo_cobranca,
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_conta_proc,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_proc_cancel,
									nr_seq_fatura_conta,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_proc,
									nr_seq_pos_proc_fat,
									nr_seq_pos_proc_tx,
									nr_seq_pos_proc_tx_fat,
									nr_seq_pos_taxa_contab,
									nr_sequencia,
									vl_custo_operacional,
									vl_faturado,
									vl_faturado_ndc,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									vl_materiais,
									vl_medico)
								select	cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									'3',
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_conta_proc,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_proc_cancel,
									nr_seq_fatura_conta_w,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_proc,
									nr_seq_pos_proc_fat,
									nr_seq_pos_proc_tx,
									nr_seq_pos_proc_tx_fat,
									nr_seq_pos_taxa_contab,
									pls_fatura_proc_seq.nextval,
									0,
									vl_faturado,
									0,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									0,
									0
								from	pls_fatura_proc
								where	nr_sequencia	= nr_seq_fatura_proc_w;
								
							update	pls_fatura_proc
							set	vl_faturado		= 0,
								vl_lib_taxa_servico	= 0,
								vl_lib_taxa_co		= 0,
								vl_lib_taxa_material	= 0
							where	nr_sequencia		= nr_seq_fatura_proc_w;
							
						elsif	(nr_seq_fatura_mat_w is not null) then
						
							insert into	pls_fatura_mat
									(cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									ie_tipo_cobranca,
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_mat,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_mat_cancel,
									nr_seq_fatura_conta,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_mat,
									nr_seq_pos_mat_fat,
									nr_seq_pos_mat_tx,
									nr_seq_pos_mat_tx_fat,
									nr_seq_pos_taxa_contab,
									nr_sequencia,
									vl_custo_operacional,
									vl_faturado,
									vl_faturado_ndc,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									vl_materiais,
									vl_medico)
								select	cd_classif_cred,
									cd_classif_cred_dif,
									cd_classif_cred_ndc,
									cd_classif_deb,
									cd_classif_deb_dif,
									cd_classif_deb_ndc,
									cd_conta_credito,
									cd_conta_credito_dif,
									cd_conta_credito_ndc,
									cd_conta_debito,
									cd_conta_debito_dif,
									cd_conta_debito_ndc,
									cd_historico,
									cd_historico_baixa,
									cd_historico_dif,
									cd_historico_estorno,
									cd_historico_ndc,
									dt_atualizacao,
									dt_atualizacao_nrec,
									ie_liberado,
									'3',
									nm_usuario,
									nm_usuario_nrec,
									nr_lote_contabil,
									nr_seq_conta_mat,
									nr_seq_conta_pos_contab,
									nr_seq_conta_pos_estab,
									nr_seq_esquema,
									nr_seq_esquema_dif,
									nr_seq_esquema_ndc,
									nr_seq_fat_mat_cancel,
									nr_seq_fatura_conta_w,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_mat,
									nr_seq_pos_mat_fat,
									nr_seq_pos_mat_tx,
									nr_seq_pos_mat_tx_fat,
									nr_seq_pos_taxa_contab,
									pls_fatura_mat_seq.nextval,
									0,
									vl_faturado,
									0,
									vl_lib_taxa_co,
									vl_lib_taxa_material,
									vl_lib_taxa_servico,
									0,
									0
								from	pls_fatura_mat
								where	nr_sequencia	= nr_seq_fatura_mat_w;
								
							update	pls_fatura_mat
							set	vl_faturado		= 0,
								vl_lib_taxa_co		= 0,
								vl_lib_taxa_material	= 0,
								vl_lib_taxa_servico	= 0
							where	nr_sequencia		= nr_seq_fatura_mat_w;
							
						end if;
					end if;
				elsif 	(nr_seq_fatura_w is not null) and (ie_fatura_taxa_w = 'N') then
				
					select	max(a.nr_sequencia)
					into	nr_seq_fatura_w
					from	pls_fatura	a
					where	a.nr_seq_lote		= nr_seq_lote_p
					and	a.nr_seq_pagador	= nr_seq_pagador_w
					and	a.ie_fatura_taxa	= 'S';
						
					if	(nr_seq_fatura_w is not null) then
						
						select	max(nr_sequencia)
						into	nr_seq_evento_fatura_w
						from 	pls_fatura_evento
						where	nr_seq_fatura = nr_seq_fatura_w
						and	nr_seq_evento = nr_seq_evento_w;
						
						if	(nr_seq_evento_fatura_w is null) then
							select	pls_fatura_evento_seq.nextval
							into	nr_seq_evento_fatura_w
							from	dual;
							
							insert into pls_fatura_evento
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_evento,
								nr_seq_fatura,
								vl_evento)
							values	(nr_seq_evento_fatura_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_evento_w,
								nr_seq_fatura_w,
								0);
						end if;
						
						select	max(nr_sequencia)
						into	nr_seq_fatura_conta_w
						from 	pls_fatura_conta
						where	nr_seq_fatura_evento	= nr_seq_evento_fatura_w
						and	nr_seq_conta		= nr_seq_conta_w;
						
						if	(nr_seq_fatura_conta_w is null) then
							select	pls_fatura_conta_seq.nextval
							into	nr_seq_fatura_conta_w
							from	dual;
							
							insert into pls_fatura_conta
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_fatura_evento,
								nr_seq_conta,
								nr_lote_contabil,
								nr_seq_segurado,
								vl_faturado,
								ie_tipo_cobranca,
								cd_guia_referencia,
								ie_tipo_vinculacao,
								nr_seq_congenere)
							values	(nr_seq_fatura_conta_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_evento_fatura_w,
								nr_seq_conta_w,
								nr_lote_contabil_w,
								nr_seq_segurado_w,
								0,
								ie_tipo_cobr_fat_conta_w,
								cd_guia_referencia_w,
								ie_origem_conta_p,
								nr_seq_congenere_w);
								
							if	(nr_seq_fatura_proc_w is not null) then
							
								insert into	pls_fatura_proc
										(cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										ie_tipo_cobranca,
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_conta_proc,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_proc_cancel,
										nr_seq_fatura_conta,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_proc,
										nr_seq_pos_proc_fat,
										nr_seq_pos_proc_tx,
										nr_seq_pos_proc_tx_fat,
										nr_seq_pos_taxa_contab,
										nr_sequencia,
										vl_custo_operacional,
										vl_faturado,
										vl_faturado_ndc,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										vl_materiais,
										vl_medico)
									select	cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										'3',
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_conta_proc,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_proc_cancel,
										nr_seq_fatura_conta_w,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_proc,
										nr_seq_pos_proc_fat,
										nr_seq_pos_proc_tx,
										nr_seq_pos_proc_tx_fat,
										nr_seq_pos_taxa_contab,
										pls_fatura_proc_seq.nextval,
										0,
										vl_faturado,
										0,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										0,
										0
									from	pls_fatura_proc
									where	nr_sequencia	= nr_seq_fatura_proc_w;
									
								update	pls_fatura_proc
								set	vl_faturado		= 0,
									vl_lib_taxa_servico	= 0,
									vl_lib_taxa_co		= 0,
									vl_lib_taxa_material	= 0
								where	nr_sequencia	= nr_seq_fatura_proc_w;
							
							elsif	(nr_seq_fatura_mat_w is not null) then
							
								insert into	pls_fatura_mat
										(cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										ie_tipo_cobranca,
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_mat,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_mat_cancel,
										nr_seq_fatura_conta,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_mat,
										nr_seq_pos_mat_fat,
										nr_seq_pos_mat_tx,
										nr_seq_pos_mat_tx_fat,
										nr_seq_pos_taxa_contab,
										nr_sequencia,
										vl_custo_operacional,
										vl_faturado,
										vl_faturado_ndc,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										vl_materiais,
										vl_medico)
									select	cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										'3',
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_mat,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_mat_cancel,
										nr_seq_fatura_conta_w,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_mat,
										nr_seq_pos_mat_fat,
										nr_seq_pos_mat_tx,
										nr_seq_pos_mat_tx_fat,
										nr_seq_pos_taxa_contab,
										pls_fatura_mat_seq.nextval,
										0,
										vl_faturado,
										0,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										0,
										0
									from	pls_fatura_mat
									where	nr_sequencia	= nr_seq_fatura_mat_w;
									
								update	pls_fatura_mat
								set	vl_faturado		= 0,
									vl_lib_taxa_co		= 0,
									vl_lib_taxa_material	= 0,
									vl_lib_taxa_servico	= 0
								where	nr_sequencia	= nr_seq_fatura_mat_w;
							end if;
						elsif 	(nr_seq_evento_pagador_w is not null) then
						
							if	(nr_seq_fatura_proc_w is not null) then
							
								insert into	pls_fatura_proc
										(cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										ie_tipo_cobranca,
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_conta_proc,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_proc_cancel,
										nr_seq_fatura_conta,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_proc,
										nr_seq_pos_proc_fat,
										nr_seq_pos_proc_tx,
										nr_seq_pos_proc_tx_fat,
										nr_seq_pos_taxa_contab,
										nr_sequencia,
										vl_custo_operacional,
										vl_faturado,
										vl_faturado_ndc,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										vl_materiais,
										vl_medico)
									select	cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										'3',
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_conta_proc,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_proc_cancel,
										nr_seq_fatura_conta_w,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_proc,
										nr_seq_pos_proc_fat,
										nr_seq_pos_proc_tx,
										nr_seq_pos_proc_tx_fat,
										nr_seq_pos_taxa_contab,
										pls_fatura_proc_seq.nextval,
										0,
										vl_faturado,
										0,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										0,
										0
									from	pls_fatura_proc
									where	nr_sequencia	= nr_seq_fatura_proc_w;
								
								update	pls_fatura_proc
								set	vl_faturado		= 0,
									vl_lib_taxa_servico	= 0,
									vl_lib_taxa_co		= 0,
									vl_lib_taxa_material	= 0
								where	nr_sequencia	= nr_seq_fatura_proc_w;
								
							elsif	(nr_seq_fatura_mat_w is not null) then
							
								insert into	pls_fatura_mat
										(cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										ie_tipo_cobranca,
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_mat,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_mat_cancel,
										nr_seq_fatura_conta,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_mat,
										nr_seq_pos_mat_fat,
										nr_seq_pos_mat_tx,
										nr_seq_pos_mat_tx_fat,
										nr_seq_pos_taxa_contab,
										nr_sequencia,
										vl_custo_operacional,
										vl_faturado,
										vl_faturado_ndc,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										vl_materiais,
										vl_medico)
									select	cd_classif_cred,
										cd_classif_cred_dif,
										cd_classif_cred_ndc,
										cd_classif_deb,
										cd_classif_deb_dif,
										cd_classif_deb_ndc,
										cd_conta_credito,
										cd_conta_credito_dif,
										cd_conta_credito_ndc,
										cd_conta_debito,
										cd_conta_debito_dif,
										cd_conta_debito_ndc,
										cd_historico,
										cd_historico_baixa,
										cd_historico_dif,
										cd_historico_estorno,
										cd_historico_ndc,
										dt_atualizacao,
										dt_atualizacao_nrec,
										ie_liberado,
										'3',
										nm_usuario,
										nm_usuario_nrec,
										nr_lote_contabil,
										nr_seq_conta_mat,
										nr_seq_conta_pos_contab,
										nr_seq_conta_pos_estab,
										nr_seq_esquema,
										nr_seq_esquema_dif,
										nr_seq_esquema_ndc,
										nr_seq_fat_mat_cancel,
										nr_seq_fatura_conta_w,
										nr_seq_pos_estab_taxa,
										nr_seq_pos_mat,
										nr_seq_pos_mat_fat,
										nr_seq_pos_mat_tx,
										nr_seq_pos_mat_tx_fat,
										nr_seq_pos_taxa_contab,
										pls_fatura_mat_seq.nextval,
										0,
										vl_faturado,
										0,
										vl_lib_taxa_co,
										vl_lib_taxa_material,
										vl_lib_taxa_servico,
										0,
										0
									from	pls_fatura_mat
									where	nr_sequencia	= nr_seq_fatura_mat_w;
									
								update	pls_fatura_mat
								set	vl_faturado		= 0,
									vl_lib_taxa_co		= 0,
									vl_lib_taxa_material	= 0,
									vl_lib_taxa_servico	= 0
								where	nr_sequencia	= nr_seq_fatura_mat_w;
								
							end if;
						end if;
					end if;
				end if;
				end;
			end loop;
			close C02;
			end;
			commit;
		end loop;
		close C01;
		commit;
		
		-- Atualizar os valores das faturas
		if	(nr_seq_fatura_nova_w is not null) then	
			pls_atualizar_vl_lote_fatura(nr_seq_lote_p,nm_usuario_p,'N','S');
		end if;
		
	end if;
end if;

end pls_dividir_fatura_taxa;
/