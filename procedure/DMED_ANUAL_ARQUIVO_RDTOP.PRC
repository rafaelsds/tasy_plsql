create or replace
procedure dmed_anual_arquivo_rdtop
			(	nr_sequencia_p			number,
				cd_pessoa_beneficiario_p		varchar,
				nm_usuario_p			varchar2,
				nr_cpf_beneficiario_p		varchar2,
				nr_linha_p		in out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_linha_w			number(10)	:= nr_linha_p;
ds_arquivo_w			varchar2(2000);
cd_pessoa_beneficiario_w	varchar2(15);
cpf_cnpj_benef_w		varchar2(15);
nm_beneficiario_w		varchar2(255);
vl_pago_w			number(15,2);
contador_w			number(10)	:= 0;
order_union_w			number(1);	
	
cursor c_reemb_dependente is
	select	1 order_union,
		p.nr_cpf cpf_cnpj_benef,
		p.nm_pessoa_fisica nm_beneficiario,
		replace(replace(to_char(sum(t.vl_pago),'999,999,990.00'),'.',''),',','') vl_pago_titular
	from	dmed_titulos_mensal	t,
		dmed_agrupar_lote	l,
		pessoa_fisica		p
	where	p.cd_pessoa_fisica	= t.cd_pf_prestador
	and	t.nr_seq_dmed_mensal	= l.nr_seq_dmed_mensal
	and	t.ie_prestadora_ops	= 'O'
	and	t.ie_tipo_documento	= 'RE'
	and	t.cd_pessoa_titular <> t.cd_pessoa_beneficiario
	and 	t.cd_cgc is null
	and	l.nr_seq_dmed_anual	= nr_sequencia_p
	and	t.cd_pessoa_beneficiario in	(select	f.cd_pessoa_fisica
						from	pessoa_fisica	f
						where	nvl(f.nr_cpf,'0')	= nvl(nr_cpf_beneficiario_p,nvl(f.nr_cpf,'0'))
						and	f.cd_pessoa_fisica = cd_pessoa_beneficiario_p)
	group by
		p.nr_cpf,
		p.nm_pessoa_fisica
	union
	select	2 order_union,
		p.cd_cgc cpf_cnpj_benef,
		p.ds_razao_social nm_beneficiario,
		replace(replace(to_char(sum(t.vl_pago),'999,999,990.00'),'.',''),',','') vl_pago_titular
	from	dmed_titulos_mensal	t,
		dmed_agrupar_lote	l,
		pessoa_juridica		p
	where	p.cd_cgc		= t.cd_cgc
	and	t.nr_seq_dmed_mensal	= l.nr_seq_dmed_mensal
	and	t.ie_prestadora_ops	= 'O'
	and	t.ie_tipo_documento	= 'RE'
	and	t.cd_pessoa_titular <> t.cd_pessoa_beneficiario
	and 	t.cd_cgc is not null
	and	l.nr_seq_dmed_anual	= nr_sequencia_p
	and	t.cd_pessoa_beneficiario in	(select	f.cd_pessoa_fisica
						from	pessoa_fisica	f
						where	nvl(f.nr_cpf,'0')	= nvl(nr_cpf_beneficiario_p,nvl(f.nr_cpf,'0'))
						and	f.cd_pessoa_fisica = cd_pessoa_beneficiario_p)
	group by
		p.cd_cgc,
		p.ds_razao_social
	order by
		order_union,
		cpf_cnpj_benef;
		
type 		fetch_array is table of c_reemb_dependente%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_reemb_dependente_w	vetor;

begin
open c_reemb_dependente;
loop
fetch c_reemb_dependente bulk collect into s_array limit 1000;
	vetor_reemb_dependente_w(i)	:= s_array;
	i				:= i + 1;
exit when c_reemb_dependente%notfound;
end loop;
close c_reemb_dependente;

for i in 1..vetor_reemb_dependente_w.count loop
	begin
	s_array := vetor_reemb_dependente_w(i);
	for z in 1..s_array.count loop
		order_union_w		:= s_array(z).order_union;
		cpf_cnpj_benef_w	:= s_array(z).cpf_cnpj_benef;
		nm_beneficiario_w	:= fis_remove_special_characters(s_array(z).nm_beneficiario);
		vl_pago_w			:= s_array(z).vl_pago_titular;
		begin
		contador_w	:= contador_w + 1;
		ds_arquivo_w 	:= 'RDTOP' || '|' || cpf_cnpj_benef_w || '|' || nm_beneficiario_w || '|' || vl_pago_w || '|' || '|';
		nr_linha_w	:= nr_linha_w + 1;
			
		insert into w_dacon
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_arquivo,
			nr_linha,
			ie_tipo_registro,
			nr_origem)
		values	(w_dacon_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ds_arquivo_w,
			nr_linha_w,
			6,
			cpf_cnpj_benef_w);

		if	(mod(contador_w,100) = 0) then
			commit;
		end if;
		end;
	end loop;
	end;
end loop;

nr_linha_p	:= nr_linha_w;

commit;

end dmed_anual_arquivo_rdtop;
/
