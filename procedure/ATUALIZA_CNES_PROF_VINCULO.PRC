create or replace
procedure atualiza_cnes_prof_vinculo(nr_seq_prof_p number,
				qt_hora_ambulatorial_p varchar2, 
				qt_hora_hospitalar_p varchar2,
				qt_hora_outros_p varchar2,
				nr_registro_p varchar2,
				sg_uf_emissor_p varchar2,
				nm_usuario_p varchar2,
				nr_sequencia_vinc_p number,
                ie_sus_p varchar2) is

begin

-- Atualiza o vinculo do profissional 

update  cnes_profissional_vinculo
set     dt_atualizacao = sysdate,
        nm_usuario = nm_usuario_p,
        qt_hora_ambulatorial = qt_hora_ambulatorial_p,
        qt_hora_hospitalar = qt_hora_hospitalar_p,
        qt_hora_outros = qt_hora_outros_p,
        nr_registro = nr_registro_p,
        sg_uf_emissor = sg_uf_emissor_p
where	nr_seq_profissional = nr_seq_prof_p
and     nr_sequencia = nr_sequencia_vinc_p;

commit;

end atualiza_cnes_prof_vinculo;
/
