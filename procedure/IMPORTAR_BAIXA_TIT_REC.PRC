create or replace
procedure importar_baixa_tit_rec	(nr_titulo_externo_p		varchar2,
					dt_recebimento_p		date,
					vl_recebido_p			number,
					vl_descontos_p			number,
					vl_juros_p			number,
					vl_multa_p			number,
					cd_moeda_p			number,
					dt_atualizacao_p		date,
					nm_usuario_p			varchar2,
					cd_tipo_recebimento_p		number,
					ie_acao_p			varchar2,
					cd_serie_nf_devol_p		varchar2,
					nr_nota_fiscal_devol_p		number,
					cd_banco_p			number,
					cd_agencia_bancaria_p		varchar2,
					nr_documento_p			varchar2,
					nr_lote_banco_p			varchar2,
					cd_cgc_emp_cred_p		varchar2,
					nr_cartao_cred_p		varchar2,
					nr_seq_trans_fin_p		number,
					vl_rec_maior_p			number,
					vl_glosa_p			number,
					nr_seq_conta_banco_p		number,
					vl_despesa_bancaria_p		number,
					ds_observacao_p			varchar2,
					nr_seq_trans_caixa_p		number,
					cd_centro_custo_desc_p		number,
					nr_seq_motivo_desc_p		number,
					dt_integracao_externa_p		date,
					vl_perdas_p			number,
					dt_autenticacao_p		date,
					vl_outros_acrescimos_p		number) is


nr_sequencia_w			number(5);
nr_titulo_w			number(10);

begin
select	max(nr_titulo)
into	nr_titulo_w
from	titulo_receber
where	nr_titulo_externo = nr_titulo_externo_p;

if	(nr_titulo_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265410,'');
	--Mensagem: T�tulo n�o encontrado!
else
	select 	nvl(max(nr_sequencia),0) + 1 
	into	nr_sequencia_w
	from	titulo_receber_liq
	where	nr_titulo = nr_titulo_w;
end if;

insert	into titulo_receber_liq
	(nr_titulo,
	nr_sequencia,
	dt_recebimento,
	vl_recebido,
	vl_descontos,
	vl_juros,
	vl_multa,
	cd_moeda,
	dt_atualizacao,
	nm_usuario,
	cd_tipo_recebimento,
	ie_acao,
	cd_serie_nf_devol,
	nr_nota_fiscal_devol,
	cd_banco,
	cd_agencia_bancaria,
	nr_documento,
	nr_lote_banco,
	cd_cgc_emp_cred,
	nr_cartao_cred,
	nr_adiantamento,
	nr_lote_contabil,
	nr_seq_trans_fin,
	vl_rec_maior,
	vl_glosa,
	nr_seq_retorno,
	vl_adequado,
	nr_seq_ret_item,
	nr_seq_conta_banco,
	vl_despesa_bancaria,
	ds_observacao,
	nr_seq_caixa_rec,
	ie_lib_caixa,
	nr_seq_trans_caixa,
	cd_centro_custo_desc,
	nr_seq_motivo_desc,
	nr_bordero,
	nr_seq_movto_trans_fin,
	nr_seq_cobranca,
	dt_integracao_externa,
	vl_perdas,
	dt_autenticacao,
	vl_outros_acrescimos,
	nr_lote_contab_antecip,
	nr_lote_contab_pro_rata)
values	(nr_titulo_w,
	nr_sequencia_w,
	dt_recebimento_p,
	vl_recebido_p,
	vl_descontos_p,
	vl_juros_p,
	vl_multa_p,
	cd_moeda_p,
	dt_atualizacao_p,
	nm_usuario_p,
	cd_tipo_recebimento_p,
	ie_acao_p,
	cd_serie_nf_devol_p,
	nr_nota_fiscal_devol_p,
	cd_banco_p,
	cd_agencia_bancaria_p,
	nr_documento_p,
	nr_lote_banco_p,
	cd_cgc_emp_cred_p,
	nr_cartao_cred_p,
	null,
	0,
	nr_seq_trans_fin_p,
	vl_rec_maior_p,
	vl_glosa_p,
	null,
	0,
	null,
	nr_seq_conta_banco_p,
	vl_despesa_bancaria_p,
	ds_observacao_p,
	null,
	'S',
	nr_seq_trans_caixa_p,
	cd_centro_custo_desc_p,
	nr_seq_motivo_desc_p,
	null,
	null,
	null,
	dt_integracao_externa_p,
	vl_perdas_p,
	dt_autenticacao_p,
	vl_outros_acrescimos_p,
	0,
	0);

atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);

commit;

end importar_baixa_tit_rec;
/
