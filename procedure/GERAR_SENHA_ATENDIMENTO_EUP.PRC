create or replace
procedure Gerar_senha_atendimento_eup(	nr_atendimento_p	number,
					ie_tipo_atendimento_p	number,
					nr_seq_regra_funcao_p	number,
					nm_usuario_p		Varchar2) is 

ds_senha_nova_w	varchar2(255);
cd_pessoa_fisica_w varchar2(10);

begin

if (nr_atendimento_p is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
end if;


ds_senha_nova_w := gerar_senha_atendimento(ie_tipo_atendimento_p, nr_seq_regra_funcao_p, cd_pessoa_fisica_w, '');

if	(ds_senha_nova_w is not null)  then  -- Retirado  da cl�usula a condi��o de vazio, pois esta, n�o entrava quando a vari�vel possu�a valor - sbherkenhoff

	update	atendimento_paciente
	set	ds_senha = ds_senha_nova_w
	where	nr_atendimento = nr_Atendimento_p;
		
end if;

commit;

end Gerar_senha_atendimento_eup;
/
