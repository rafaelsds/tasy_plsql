create or replace
procedure gravar_alter_disp_interv_sae(	nr_atendimento_p		number,
										nr_seq_prescr_p			number,
										nr_seq_proc_p			number,
										nr_seq_dispositivo_p	number,
										nr_seq_disp_atend_p		number,
										ie_gerar_p				varchar2,
										dt_prevista_p			varchar2) is
										
ds_justificativa_w			Varchar2(255);
nm_usuario_w				Varchar2(15);
qt_reg_w					Number(10);
ds_justif_incons_quimio_w	Varchar2(255);
qt_dose_prescricao_w		Number(15,4);
dt_prevista_w				Date;										

begin

if	(nr_atendimento_p is not null) and
	(nr_seq_prescr_p is not null) and
	(nr_seq_proc_p is not null) and
	(ie_gerar_p    is not null) and
	(nr_seq_dispositivo_p is not null) and
	(nr_seq_disp_atend_p is not null)then
	
	if	(dt_prevista_p is not null) then
	
	dt_prevista_w := to_date(dt_prevista_p,'hh24:mi');
	
	end if;
	
	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
		
	Select  count(*)
	into	qt_reg_w
	from	dispositivo_interv_sae_alt
	where	nr_atendimento = nr_atendimento_p
	and		nr_seq_prescr = nr_seq_prescr_p
	and		nr_seq_proc = nr_seq_proc_p
	and		nr_seq_dispositivo = nr_seq_dispositivo_p
	and		nr_seq_disp_atend = nr_seq_disp_atend_p;
	
	if	( qt_reg_w = 0) then
	
		insert into dispositivo_interv_sae_alt(nr_sequencia,
											 nr_atendimento,
											 nr_seq_prescr,											
											 nr_seq_proc,
											 nm_usuario,
											 nm_usuario_nrec,
											 dt_atualizacao,
											 dt_atualizacao_nrec,
											 ie_gerar,
											 hr_prim_horario,
											 nr_seq_dispositivo,
											 nr_seq_disp_atend)
									values ( dispositivo_interv_sae_alt_seq.nextval,
											nr_atendimento_p,
											nr_seq_prescr_p,
											nr_seq_proc_p,
											nm_usuario_w,
											nm_usuario_w,
											sysdate,
											sysdate,
											ie_gerar_p,
											to_char(dt_prevista_w,'hh24:mi'),
											nr_seq_dispositivo_p,
											nr_seq_disp_atend_p);
	
	else	

			Update	dispositivo_interv_sae_alt
			set		ie_gerar = ie_gerar_p,
					hr_prim_horario = to_char(dt_prevista_w,'hh24:mi'),
					dt_atualizacao = sysdate,
					nm_usuario = nm_usuario_w
			where	nr_atendimento = nr_atendimento_p
			and		nr_seq_prescr = nr_seq_prescr_p
			and		nr_seq_proc = nr_seq_proc_p
			and		nr_seq_dispositivo = nr_seq_dispositivo_p
			and		nr_seq_disp_atend = nr_seq_disp_atend_p;
			
		
	end if;
	
	commit;
	
end if;

end gravar_alter_disp_interv_sae;
/
