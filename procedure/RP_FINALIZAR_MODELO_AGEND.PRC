create or replace
procedure rp_finalizar_modelo_agend(nr_seq_modelo_p	varchar2,
				    nr_seq_motivo_p	varchar2,
				    nm_usuario_p	Varchar2,
				    ie_opcao_p		varchar2,
				    dt_fim_tratamento_p DATE) is 

nr_seq_modelo_pac_w	number(10,0);
ie_existe_sem_fim_w	varchar2(1);
nr_seq_hora_w		agenda_consulta.nr_seq_hora%type;		    
				    
/*
Opcoes
	M - Finalizar Modelo
	I -  Finalizar Item do Modelo
*/	
Cursor C01 is
	select	nr_sequencia,
				cd_agenda,
				dt_agenda
	from	agenda_consulta
	where	((ie_opcao_p = 'M' and nr_seq_rp_mod_item in (	select	nr_sequencia 
																			from 	rp_pac_modelo_agend_item 
																			where 	nr_seq_modelo_pac = nr_seq_modelo_p))
			or (ie_opcao_p = 'I' and nr_seq_rp_mod_item 	= nr_seq_modelo_p))
	and	ie_status_agenda not in ('C','B','E','F', 'I', 'S', 'AD');
	
c01_w	C01%ROWTYPE;
	
begin

if	(ie_opcao_p = 'M') then
	begin
	
	update	rp_pac_modelo_agendamento
	set	nr_seq_motivo_fim_tratamento 	= nr_seq_motivo_p,
		dt_fim_tratamento 		= dt_fim_tratamento_p,
		nm_usuario 			= nm_usuario_p,
		dt_atualizacao			= sysdate
	where	nr_sequencia  			= nr_seq_modelo_p;
		
	update	rp_pac_modelo_agend_item
	set	nr_seq_motivo_fim_tratamento 	= nr_seq_motivo_p,
		dt_fim_tratamento 		= dt_fim_tratamento_p,
		nm_usuario 			= nm_usuario_p,
		dt_atualizacao			= sysdate
	where	nr_seq_modelo_pac		= nr_seq_modelo_p
	and	dt_fim_tratamento is null;
	
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		nr_seq_hora_w	:=  null;
		
		select	nvl(max(nr_seq_hora),0) + 1
		into	nr_seq_hora_w
		from	agenda_consulta
		where	cd_agenda	= c01_w.cd_agenda
		and	dt_agenda	= c01_w.dt_agenda;
	
		update	agenda_consulta
		set	ie_status_agenda 	= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nr_seq_hora		= nr_seq_hora_w
		where	nr_sequencia = c01_w.nr_sequencia;
	
	end loop;
	close C01;
	
	end;
elsif	(ie_opcao_p = 'I') then
	begin
	
	select	max(nr_seq_modelo_pac)
	into	nr_seq_modelo_pac_w
	from	rp_pac_modelo_agend_item
	where	nr_sequencia 			= nr_seq_modelo_p;
	
	update	rp_pac_modelo_agend_item
	set	nr_seq_motivo_fim_tratamento 	= nr_seq_motivo_p,
		dt_fim_tratamento 		= dt_fim_tratamento_p,
		nm_usuario 			= nm_usuario_p,
		dt_atualizacao			= sysdate
	where	nr_sequencia     		= nr_seq_modelo_p;

	open C01;
	loop
	fetch C01 into	
			c01_w;
	exit when C01%notfound;
	
		nr_seq_hora_w	:=  null;
		
		select	nvl(max(nr_seq_hora),0) + 1
		into	nr_seq_hora_w
		from	agenda_consulta
		where	cd_agenda	= c01_w.cd_agenda
		and	dt_agenda	= c01_w.dt_agenda;
	
		update	agenda_consulta
		set	ie_status_agenda 	= 'C',
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				nr_seq_hora		= nr_seq_hora_w
		where	nr_sequencia = c01_w.nr_sequencia;
	
	end loop;
	close C01;
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_sem_fim_w
	from	rp_pac_modelo_agend_item
	where	nr_seq_modelo_pac		= nr_seq_modelo_pac_w
	and	dt_fim_tratamento is null;
	
	if	(ie_existe_sem_fim_w = 'N') then
		begin
		update	rp_pac_modelo_agendamento
		set	nr_seq_motivo_fim_tratamento 	= nr_seq_motivo_p,
			dt_fim_tratamento 		= dt_fim_tratamento_p,
			nm_usuario 			= nm_usuario_p,
			dt_atualizacao			= sysdate
		where	nr_sequencia  			= nr_seq_modelo_pac_w;
		end;
	end if;
	
	end;
end if;
	
commit;

end rp_finalizar_modelo_agend;
/
