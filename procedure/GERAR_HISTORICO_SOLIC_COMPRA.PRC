create or replace
procedure gerar_historico_solic_compra(
				nr_solic_compra_p	Number,
				ds_titulo_p	varchar2,
				ds_historico_p	varchar2,
				cd_evento_p	varchar2,
				nm_usuario_p	varchar2,
				ie_commit_p	varchar2 default 'S') as

begin

insert into solic_compra_hist(
	nr_sequencia,
	nr_solic_compra,
	dt_atualizacao,
	nm_usuario,
	dt_historico,
	ds_titulo,
	ds_historico,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_tipo,
	cd_evento,
	dt_liberacao)
values	(solic_compra_hist_seq.nextval,
	nr_solic_compra_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	ds_titulo_p,
	ds_historico_p,
	sysdate,
	nm_usuario_p,
	'S',
	cd_evento_p,
	sysdate);
	
if	(nvl(ie_commit_p,'S') = 'S') then
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end if;

end gerar_historico_solic_compra;
/
