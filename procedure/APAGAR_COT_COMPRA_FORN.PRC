create or replace
procedure apagar_cot_compra_forn(
				nr_cot_compra_p varchar2) is

begin

delete from cot_compra_forn
where nr_cot_compra = nr_cot_compra_p;

commit;

end apagar_cot_compra_forn;
/