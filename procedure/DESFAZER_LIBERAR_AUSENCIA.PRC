create or replace
procedure desfazer_liberar_ausencia(nr_sequencia_p		number,
				    cd_pessoa_fisica_p		varchar2,
				    dt_inicial_P		date,
				    dt_final_p			date,
				    ie_status_p  		varchar2) is 
				    
nr_seq_agenda_w			number(10,0);				    
dt_agenda_w				agenda_consulta.dt_agenda%type;
nr_seq_hora_w			agenda_consulta.nr_seq_hora%type;
cd_agenda_w				agenda_consulta.cd_agenda%type;

Cursor C01 is
	SELECT	b.nr_sequencia,
			b.cd_agenda,
			b.dt_agenda
	FROM	agenda a,
		agenda_consulta b
	WHERE	a.cd_agenda = b.cd_agenda
	AND	a.cd_tipo_agenda = 5
	AND	b.cd_pessoa_fisica = NVL(cd_pessoa_fisica_p,cd_pessoa_fisica_p)
	AND	b.dt_agenda BETWEEN TRUNC(dt_inicial_p) AND dt_final_p + 86399/86400
	AND	(
		(EXISTS (SELECT 1 FROM rp_pac_modelo_agend_item 
			WHERE	nr_sequencia = b.nr_seq_rp_mod_item)) 
		OR 
		(EXISTS (SELECT 1 FROM rp_pac_agend_individual 
			WHERE  nr_sequencia = b.nr_seq_rp_item_ind)))
	AND	((b.ie_status_agenda = ie_status_p) );
	

begin


open C01;
loop
fetch C01 into	
	nr_seq_agenda_w,
	cd_agenda_w,
	dt_agenda_w;
exit when C01%notfound;
	begin
	
	select 	nvl(max(nr_seq_hora), 0) + 1
	into	nr_seq_hora_w
	from agenda_consulta
	where cd_agenda = cd_agenda_w
	and ie_status_agenda	= 'N'
	and dt_agenda = dt_agenda_w;

	update	agenda_consulta
	set	ie_status_agenda	= 'N',
		ds_motivo_status	= null,
		cd_motivo_cancelamento	= null,
		nr_seq_hora 		= nr_seq_hora_w
	where	nr_sequencia		= nr_seq_agenda_w;

				
	end;
end loop;
close C01;


if (nvl(nr_sequencia_p,0) > 0) then 
	update 	rp_licenca
	set 	dt_liberacao = null,
		nm_usuario_lib = null
	where 	nr_sequencia = nr_sequencia_p;
end if;

commit;

end desfazer_liberar_ausencia;
/
