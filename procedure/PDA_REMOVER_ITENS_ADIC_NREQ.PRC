create or replace
procedure 	pda_remover_itens_adic_nreq(	nr_seq_item_nreq_p	Number,
											nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_item_nreq_p is not null) then
	
	delete
	from	pda_processo_item_nreq
	where	nr_sequencia = nr_seq_item_nreq_p;
								
	
end if;

commit;

end pda_remover_itens_adic_nreq;
/