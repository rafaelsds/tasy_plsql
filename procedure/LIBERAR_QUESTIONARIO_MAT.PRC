create or replace 
procedure liberar_questionario_mat( nm_usuario_p varchar2,
				nr_sequencia_p   number		) is
				
begin

update 	mat_aval_quest 
set 	dt_liberacao = sysdate, 
	nm_usuario_lib = nm_usuario_p
where 	nr_sequencia = nr_sequencia_p;

commit;

end liberar_questionario_mat;
/