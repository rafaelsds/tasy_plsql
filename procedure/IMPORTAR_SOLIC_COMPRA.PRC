create or replace
procedure importar_solic_compra(
			nr_solic_importacao_p		in		varchar2,
			dt_solicitacao_compra_p		in		date,
			cd_pessoa_solicitante_p		in		varchar2,
			cd_local_estoque_p			in 		number,
			cd_centro_custo_p			in		number,
			ie_urgente_p				in		varchar2,
			cd_material_p				in		number,
			cd_unidade_medida_compra_p	in		varchar2,
			qt_material_p				in		number,
			nm_usuario_p				in 		varchar2,
			cd_estabelecimento_p		in		number,
			ds_erro_p					out		varchar2) is

nm_usuario_w				solic_compra.nm_usuario%type;
dt_liberacao_w				solic_compra.dt_liberacao%type;
nr_solic_compra_w			solic_compra.nr_solic_compra%type;
cd_local_estoque_w			solic_compra.cd_local_estoque%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_item_solic_compra_w		solic_compra_item.nr_item_solic_compra%type;
cd_pessoa_solicitante_w		solic_compra.cd_pessoa_solicitante%type;
cd_unidade_medida_compra_w	solic_compra_item.cd_unidade_medida_compra%type;
qt_erro_w 					number(5) := 0;

begin
nm_usuario_w := nm_usuario_p;
cd_local_estoque_w := cd_local_estoque_p;
cd_estabelecimento_w := cd_estabelecimento_p;
cd_pessoa_solicitante_w := cd_pessoa_solicitante_p;
cd_unidade_medida_compra_w := cd_unidade_medida_compra_p;


/* Verifica se o solicitante foi informado */
if (cd_pessoa_solicitante_w is null) then
	select cd_pessoa_solic_padrao
	into	cd_pessoa_solicitante_w
	from parametro_compras
	where cd_estabelecimento = cd_estabelecimento_w;
end if;

/* Verifica se o centro de custo ou local de estoque foram informados */
if (cd_centro_custo_p is null and cd_local_estoque_w is null) then
	select cd_local_estoque_padrao
	into	cd_local_estoque_w
	from parametro_compras
	where cd_estabelecimento = cd_estabelecimento_w;
end if;

/* Verifica se as informacoes necessarias foram preenchidas */
if (nr_solic_importacao_p is null) or 
	(cd_material_p is null) or 
	(qt_material_p is null) or 
	(cd_pessoa_solicitante_w is null) or
	(cd_centro_custo_p is null and cd_local_estoque_w is null) then
	
	qt_erro_w := qt_erro_w + 1;
end if;

if (qt_erro_w  = 0) then
	
	select max(nr_solic_compra)
	into nr_solic_compra_w
	from solic_compra
	where nr_solic_importacao = nr_solic_importacao_p;

	if (nr_solic_compra_w is null) then
		select	solic_compra_seq.nextval
		into	nr_solic_compra_w
		from	dual;
		
		begin
			insert into solic_compra(
					nr_solic_compra,
					nr_solic_importacao,
					dt_solicitacao_compra,
					ie_aviso_aprov_oc,
					cd_pessoa_solicitante,
					cd_local_estoque,
					cd_centro_custo,
					ie_urgente,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					ie_situacao)
				values (nr_solic_compra_w,
					nr_solic_importacao_p,
					nvl(dt_solicitacao_compra_p,sysdate),
					'N',
					cd_pessoa_solicitante_w,
					cd_local_estoque_w,
					cd_centro_custo_p,
					nvl(ie_urgente_p,'N'),
					cd_estabelecimento_w,
					sysdate,
					nm_usuario_w,
					'A');
		exception
			when others then
			qt_erro_w := qt_erro_w + 1;
		end;
	end if;
	
	if(qt_erro_w = 0) then
	
		select max(dt_liberacao)
		into dt_liberacao_w
		from solic_compra
		where nr_solic_compra = nr_solic_compra_w;
		
		if (dt_liberacao_w is null) then
			select 	nvl(max(nr_item_solic_compra),0)+1
			into	nr_item_solic_compra_w
			from 	solic_compra_item
			where 	nr_solic_compra = nr_solic_compra_w; 
			
			/* Verifica se a unidade de medida de compra foi informada */
			if (cd_unidade_medida_compra_w is null) then
				begin 
					select	max(cd_unidade_medida_compra)
					into	cd_unidade_medida_compra_w
					from	material
					where	cd_material = cd_material_p;
				exception
				when others then
				qt_erro_w := qt_erro_w + 1;
				end;
			end if;
			
			begin		
				insert into solic_compra_item(
						nr_solic_compra,
						nr_item_solic_compra,
						cd_material,
						cd_unidade_medida_compra,
						qt_material,
						dt_atualizacao,
						nm_usuario,
						ie_situacao,
						ie_geracao)
					values(nr_solic_compra_w,
						nr_item_solic_compra_w,
						cd_material_p,
						cd_unidade_medida_compra_w,
						qt_material_p,
						sysdate,
						nm_usuario_w,
						'A',
						'S');
			exception
				when others then
				qt_erro_w := qt_erro_w + 1;
			end;
		else
			qt_erro_w := qt_erro_w + 1;
		end if;
	end if;
end if;
if(qt_erro_w > 0) then
	rollback;
	ds_erro_p := wheb_mensagem_pck.get_texto(302196)|| ' ' || wheb_mensagem_pck.get_texto(800325)
	|| ' ' || nr_solic_importacao_p || ' - ' || wheb_mensagem_pck.get_texto(791832) || ' ' || cd_material_p;
else
	commit;
end if;

end importar_solic_compra;
/
