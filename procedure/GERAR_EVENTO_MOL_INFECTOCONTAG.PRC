create or replace 
procedure gerar_evento_mol_infectocontag(	nr_atendimento_p	number,
						cd_pessoa_fisica_p	varchar2,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number) is
nr_seq_evento_w	number(10);
qt_reg_w	number(10);
qt_idade_w	number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms a,
		ev_evento b
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_evento_disp = 'C'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	a.nr_seq_evento = b.nr_sequencia
	and	upper(b.ds_evento) like upper('%Infectocontagiosa%')
	and	nvl(a.ie_situacao,'A') = 'A';

begin

select  count(*)
into	qt_reg_w
from    CIH_LOCAL_INFECCAO a
where   CIH_Obter_dados_Ficha(a.nr_ficha_ocorrencia,2) = cd_pessoa_fisica_p
and     a.ie_doenca_infectocontagiosa = 'S';
qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A'),0);
if	(qt_reg_w >= 1) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_fisica_p,null,nm_usuario_p,null);
		end;
	end loop;
	close C01;
end if;

end gerar_evento_mol_infectocontag;
/