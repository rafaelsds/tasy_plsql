create or replace procedure cpoe_gerar_plano_protocolo(
    nr_atendimento_p atendimento_paciente.nr_atendimento%type,
    dt_referencia_p date,
    nr_seq_cpoe_p cpoe_material.nr_sequencia%type,
    nm_usuario_p usuario.nm_usuario%type,
	ie_tipo_item_p varchar2)
is
  nr_sequencia_w 		cpoe_plan_protocol.nr_sequencia%type;
  nr_seq_mat_cpoe_w		cpoe_plan_protocol.nr_seq_mat_cpoe%type := null;
  nr_seq_diet_cpoe_w	cpoe_plan_protocol.nr_seq_diet_cpoe%type := null;
  nr_seq_dial_cpoe_w	cpoe_plan_protocol.nr_seq_dial_cpoe%type := null;
  nr_seq_exam_cpoe_w	cpoe_plan_protocol.nr_seq_exam_cpoe%type := null;
  nr_seq_rec_cpoe_w		cpoe_plan_protocol.nr_seq_rec_cpoe%type := null;
  nr_seq_gaso_cpoe_w	cpoe_plan_protocol.nr_seq_gaso_cpoe%type := null;
  nr_seq_hemo_cpoe_w	cpoe_plan_protocol.nr_seq_hemo_cpoe%type := null;
  nr_seq_anat_cpoe_w	cpoe_plan_protocol.nr_seq_anat_cpoe%type := null;	
begin
  if ((nr_atendimento_p is not null) and (dt_referencia_p is not null) and (nr_seq_cpoe_p is not null) and (ie_tipo_item_p is not null)) then
    begin
	  if ie_tipo_item_p = 'M' then
	    nr_seq_mat_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'MA' then
	    nr_seq_mat_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'N' then
	    nr_seq_diet_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'P' then
	    nr_seq_exam_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'G' then
	    nr_seq_gaso_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'R' then
	    nr_seq_rec_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'H' then
	    nr_seq_hemo_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'D' then
	    nr_seq_dial_cpoe_w := nr_seq_cpoe_p;
	  elsif ie_tipo_item_p = 'AP' then
	    nr_seq_anat_cpoe_w := nr_seq_cpoe_p;
  	  end if;

      select cpoe_plan_protocol_seq.nextval 
      into nr_sequencia_w 
      from dual;
    
      insert
      into cpoe_plan_protocol
        (
          nr_sequencia,
          nr_atendimento,
          dt_protocol_end,
		  nr_seq_mat_cpoe,
		  nr_seq_diet_cpoe,
		  nr_seq_dial_cpoe,
		  nr_seq_exam_cpoe,
		  nr_seq_rec_cpoe,
		  nr_seq_gaso_cpoe,
		  nr_seq_hemo_cpoe,
		  nr_seq_anat_cpoe,
          nm_usuario,
          dt_atualizacao,
          nm_usuario_nrec,
          dt_atualizacao_nrec
        )
        values
        (
          nr_sequencia_w,
          nr_atendimento_p,
          dt_referencia_p,
		  nr_seq_mat_cpoe_w,
		  nr_seq_diet_cpoe_w,
		  nr_seq_dial_cpoe_w,
		  nr_seq_exam_cpoe_w,
		  nr_seq_rec_cpoe_w,
		  nr_seq_gaso_cpoe_w,
		  nr_seq_hemo_cpoe_w,
		  nr_seq_anat_cpoe_w,
          nm_usuario_p,
          sysdate,
          nm_usuario_p,
          sysdate
        );
        
      commit;
    exception
      when others then
        gravar_log_tasy(10008, 'cpoe_gerar_plano_protocolo Erro:' || substr(to_char(sqlerrm),1,2000) 
          || '//nr_sequencia: ' || nr_sequencia_w 
          || '-nr_atendimento: ' || nr_atendimento_p 
          || '-nm_usuario: ' || nm_usuario_p 
          || '-nr_seq_cpoe: ' || nr_seq_cpoe_p
		  || '-ie_tipo_item: ' || ie_tipo_item_p
		  || '-nr_seq_mat_cpoe_w: ' || nvl(nr_seq_mat_cpoe_w, 'null')
		  || '-nr_seq_diet_cpoe_w: ' || nvl(nr_seq_diet_cpoe_w, 'null')
		  || '-nr_seq_dial_cpoe_w: ' || nvl(nr_seq_dial_cpoe_w, 'null')
		  || '-nr_seq_exam_cpoe_w: ' || nvl(nr_seq_exam_cpoe_w, 'null')
		  || '-nr_seq_rec_cpoe_w: ' || nvl(nr_seq_rec_cpoe_w, 'null')
		  || '-nr_seq_gaso_cpoe_w: ' || nvl(nr_seq_gaso_cpoe_w, 'null')
		  || '-nr_seq_hemo_cpoe_w: ' || nvl(nr_seq_hemo_cpoe_w, 'null')
          || '-dt_referencia: ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_referencia_p,'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.gettimezone)
          || '-dt_atualizacao: ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate,'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.gettimezone), 
          nm_usuario_p);
    end;
  end if;
end cpoe_gerar_plano_protocolo;
/
