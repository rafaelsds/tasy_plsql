create or replace
procedure libera_laudo_paciente_sus_js(	nr_seq_interno_p	number) is 

begin

if	(nr_seq_interno_p is not null)then
	begin
	
	update 	sus_laudo_paciente 
	set 	dt_liberacao 	= sysdate 
	where 	nr_seq_interno 	= nr_seq_interno_p;
	
	end;
end if;

commit;

end libera_laudo_paciente_sus_js;
/