create or replace
procedure generate_3m_data(	xml_p 			in varchar2,
				nm_usuario_p 		in varchar2,
				nr_atendimento_p	in number,
				cd_setor_atend_p 	in number,
				cd_convenio_p 		in number)is

xml_w 			xmltype;
nr_sequencia_w 		number(10);
nr_seq_edition_w 	health_fund_drg.nr_seq_edition%type;
nr_seq_drg_w 		number(10);
nr_seq_episode_w 	number(10);
qt_achi_count_w 	number(10);
nr_seq_medico_w 	number(10);
list_icd_w 		varchar2(4000);
list_achi_w 		varchar2(4000);
list_morp_w     	varchar2(4000);
cd_procedimento_w 	procedimento.cd_procedimento%type;
qt_diag_count_w 	number(5);
qt_diag_sec_w 		number(5);
qt_icd_w		number(5);
cd_medico_resp_w	atendimento_paciente.cd_medico_resp%type;
cd_estabelecimento_w 	atendimento_paciente.cd_estabelecimento%type;
dt_diagnostico_w  	diagnostico_doenca.dt_diagnostico%type := null;
ie_tipo_diagnostico_w 	number;
qt_doctor_diagnosis_w	number;
qt_morphology_w 	number;
qt_morpicd_w 		number;
cd_user_w     		varchar2(100);

type r_gen_items is record (
	cd_procedimento		number(15),
	ie_origem_proced	number(5),
	cd_doenca		varchar2(10),
	ie_tipo_item		varchar2(10),
  nr_seq_drg   number(10));

type t_gen_items is table of r_gen_items index by binary_integer;
t_gen_items_w		t_gen_items;

i	integer;
x	integer;

cursor c01 is
select	*
from  	XMLTABLE('/CRS/ENCOUNTER/CLAIM/DRG' passing xml_w columns
		valueid varchar(10) path 'VALUE');
c01_w	c01%rowtype;

cursor c02 is
select	*
from  	XMLTABLE('/CRS/ENCOUNTER/CLAIM/I10DXP' passing xml_w columns
		valueid varchar(10) path 'VALUE');
c02_w	c02%rowtype;

cursor c03 is
select	*
from 	XMLTABLE('/CRS/ENCOUNTER/CLAIM/ACHI' passing xml_w columns
		valueid varchar(10) path 'VALUE',
		procdt	varchar(10) path 'DATE');
c03_w	c03%rowtype;

cursor 	c04 is
select 	cd_doenca
from 	diagnostico_doenca
where   nr_atendimento = nr_atendimento_p
and	ie_situacao = 'A'
and     cd_doenca not in  (select nvl(regexp_substr(list_icd_w,'[^,]+', 1, level),0) from dual
        connect by regexp_substr(list_icd_w, '[^,]+', 1, level) is not null);
c04_w	c04%rowtype;

cursor c05 is
select  nr_sequencia
from    procedimento_pac_medico
where   nr_atendimento = nr_atendimento_p
and     Ie_Origem_Proced = 19
and	ie_situacao = 'A'
and     nr_sequencia not in (select nvl(regexp_substr(list_achi_w,'[^,]+', 1, level),0) from dual
        connect by regexp_substr(list_achi_w, '[^,]+', 1, level) is not null);
c05_w	c05%rowtype;

cursor c06 is
select	*
from  	XMLTABLE('/CRS/ENCOUNTER/CLAIM/I10DX' passing xml_w columns
		valueid varchar(10) path 'VALUE');
c06_w	c06%rowtype;

cursor c08 is
	select	*
	from  	XMLTABLE('/CRS/ENCOUNTER/CLAIM/I10M' passing xml_w columns
			valueid varchar(10) path 'VALUE');
c08_w	c08%rowtype;

cursor c09 is
	select	cd_morfologia
	from    can_loco_regional
	where	nr_atendimento = nr_atendimento_p
	and     ie_situacao = 'A'
	and 	cd_morfologia not in (select nvl(regexp_substr(list_morp_w,'[^,]+', 1, level),0) from dual
		connect by regexp_substr(list_morp_w, '[^,]+', 1, level) is not null);

c09_w	c09%rowtype;

cursor	c07 is
	select  nr_seq_propaci
	from    episodio_paciente_drg
	where   nr_atendimento = nr_atendimento_p
	and     ie_situacao = 'A';

  procedure insert_drg_items
        ( cd_procedimento_p		number,
          ie_origem_proced_p	number,
          cd_doenca_p		varchar2,
          ie_tipo_item_p		varchar2,
          nr_seq_drg_p   number) is
  begin
    t_gen_items_w(i).cd_procedimento := cd_procedimento_p;
    t_gen_items_w(i).ie_origem_proced := ie_origem_proced_p;
    t_gen_items_w(i).cd_doenca := cd_doenca_p;
    t_gen_items_w(i).ie_tipo_item := ie_tipo_item_p;
    t_gen_items_w(i).nr_seq_drg := nr_seq_drg_p;

    i := i+1;

  end insert_drg_items;

begin
xml_w 	:= Xmltype(REPLACE(xml_p, 'xmlns="http://www.3m.com/de/his/BusinessObjects/KodipDataModel"', ''));
i := 0;

select  max(b.nr_sequencia)
into    nr_sequencia_w
from    convenio a,
        health_fund_drg b
where   a.cd_convenio = b.cd_health_fund
and     a.cd_convenio = cd_convenio_p
and     sysdate between b.dt_start_validity and nvl(b.dt_end_validity,sysdate);

select  max(nr_seq_edition)
into    nr_seq_edition_w
from    health_fund_drg
where   nr_sequencia = nr_sequencia_w;

select 	cd_medico_resp,
        cd_estabelecimento
into	cd_medico_resp_w,
	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;


open c01;
loop
fetch c01 into
	c01_w;
	exit when c01%notfound;
	begin

	select  max(b.nr_sequencia)
	into    nr_seq_drg_w
	from    edition_drg a,
		drg_procedimento b
	where   b.nr_seq_edition = a.nr_sequencia
	and     nr_seq_edition = nr_seq_edition_w
	and     b.cd_drg = c01_w.valueid;

	if	(nr_seq_drg_w is null) then
		select	max(nr_seq_standard_edition)
		into	nr_seq_edition_w
		from	parametro_drg;

		select  max(b.nr_sequencia)
		into    nr_seq_drg_w
		from    edition_drg a,
			drg_procedimento b
		where   b.nr_seq_edition = a.nr_sequencia
		and     nr_seq_edition = nr_seq_edition_w
		and     b.cd_drg = c01_w.valueid;

		if	(nr_seq_drg_w is null) then
			Wheb_Mensagem_Pck.Exibir_Mensagem_Abort(1075947);
		end if;
	end if;

	for r7 in C07 loop
		begin
		excluir_proc_pac_drg(r7.nr_seq_propaci,nm_usuario_p);
		end;
	end loop;

	update  Episodio_Paciente_Drg
	set     Ie_Situacao = 'I'
	where   nr_atendimento = nr_atendimento_p;


	select	episodio_paciente_drg_seq.nextval
	into	nr_seq_episode_w
	from	dual;


	insert  into episodio_paciente_drg(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ie_tipo_drg,
				nr_seq_drg_proc,
				nr_atendimento,
				ie_situacao)
	values 			(nr_seq_episode_w,
				sysdate,
				nm_usuario_p,
				'D',
				nr_seq_drg_w,
				nr_atendimento_p,
				'A');

	billing_i18n_pck.set_INATIVAR_DRG('N');

end;
end loop;
close c01;

open c02;
loop
fetch c02 into
	c02_w;
	exit when c02%notfound;
	begin

	select	count(1)
	into	qt_diag_count_w
	from	diagnostico_doenca
	where	nr_atendimento = nr_atendimento_p
	and 	ie_situacao = 'A'
	and     ie_classificacao_doenca = 'P'
	and 	cd_doenca = c02_w.valueid;

	select	count(1)
	into	qt_diag_sec_w
	from	diagnostico_doenca
	where	nr_atendimento = nr_atendimento_p
	and 	ie_situacao = 'A'
	and 	cd_doenca = c02_w.valueid;

	list_icd_w := concat(list_icd_w,c02_w.valueid || ',');

	if(qt_diag_count_w = 0 and qt_diag_sec_w <> 0) then

		update	diagnostico_doenca
		set	ie_situacao = 'I'
		where	nr_atendimento = nr_atendimento_p
		and 	cd_doenca = c02_w.valueid;
	end if;

	select	count(1)
	into	qt_icd_w
	from	cid_doenca
	where	cd_doenca = c02_w.valueid
	and	ie_situacao = 'A';

	if(qt_diag_count_w = 0 and qt_icd_w <> 0) then

		select	nvl(max(2),1)
		into	ie_tipo_diagnostico_w
		from	diagnostico_medico
		where	nr_atendimento	= nr_atendimento_p;

		select	count(*)
		into    qt_doctor_diagnosis_w
		from	diagnostico_medico
		where	nr_atendimento = nr_atendimento_p
		and	dt_diagnostico = dt_diagnostico_w;

		if	(qt_doctor_diagnosis_w = 0) then
			dt_diagnostico_w := sysdate;

			insert into diagnostico_medico(
					nr_atendimento,
					dt_diagnostico,
					ie_tipo_diagnostico,
					cd_medico,
					dt_atualizacao,
					nm_usuario,
					ds_diagnostico)
			values		(nr_atendimento_p,
					dt_diagnostico_w,
					ie_tipo_diagnostico_w,
					cd_medico_resp_w,
					sysdate,
					nm_usuario_p,
					null);
		end if;


		insert into diagnostico_doenca(
				nr_atendimento,
				dt_diagnostico,
				cd_doenca,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				ie_classificacao_doenca)
		values		(nr_atendimento_p,
				dt_diagnostico_w,
				c02_w.valueid,
				sysdate,
				nm_usuario_p,
				'A',
				'P');
	end if;

	if(qt_icd_w <> 0) then
		insert_drg_items(null,null,c02_w.valueid,'CID',nr_seq_episode_w);
	end if;
end;
end loop;
close c02;

open c06;
loop
fetch c06 into
	c06_w;
	exit when c06%notfound;
	begin

	select	count(1)
	into	qt_diag_count_w
	from	diagnostico_doenca
	where	nr_atendimento = nr_atendimento_p
	and 	ie_situacao = 'A'
	and     ie_classificacao_doenca = 'S'
	and 	cd_doenca = c06_w.valueid;

	list_icd_w := concat(list_icd_w,c06_w.valueid || ',');

	select	count(1)
	into	qt_diag_sec_w
	from	diagnostico_doenca
	where	nr_atendimento = nr_atendimento_p
	and 	ie_situacao = 'A'
	and 	cd_doenca = c06_w.valueid;

	if(qt_diag_count_w = 0 and qt_diag_sec_w <> 0) then
		update	diagnostico_doenca
		set     ie_situacao = 'I'
		where	nr_atendimento = nr_atendimento_p
		and 	cd_doenca = c06_w.valueid;
	end if;

	select	count(1)
	into	qt_icd_w
	from	cid_doenca
	where	cd_doenca = c06_w.valueid
	and	ie_situacao = 'A';

	if(qt_diag_count_w = 0 and qt_icd_w <> 0) then

		select	nvl(max(2),1)
		into	ie_tipo_diagnostico_w
		from	diagnostico_medico
		where	nr_atendimento	= nr_atendimento_p;

		select	count(*)
		into    qt_doctor_diagnosis_w
		from	diagnostico_medico
		where	nr_atendimento = nr_atendimento_p
		and	dt_diagnostico = dt_diagnostico_w;

		if	(qt_doctor_diagnosis_w = 0) then
			dt_diagnostico_w := sysdate;

			insert into diagnostico_medico(
					nr_atendimento,
					dt_diagnostico,
					ie_tipo_diagnostico,
					cd_medico,
					dt_atualizacao,
					nm_usuario,
					ds_diagnostico)
			values		(nr_atendimento_p,
					dt_diagnostico_w,
					ie_tipo_diagnostico_w,
					cd_medico_resp_w,
					sysdate,
					nm_usuario_p,
					null);
		end if;


		insert into diagnostico_doenca(
				nr_atendimento,
				dt_diagnostico,
				cd_doenca,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				ie_classificacao_doenca)
		values		(nr_atendimento_p,
				dt_diagnostico_w,
				c06_w.valueid,
				sysdate,
				nm_usuario_p,
				'A',
				'S');
	end if;

	if(qt_icd_w <> 0) then
		insert_drg_items(null,null,c06_w.valueid,'CID',nr_seq_episode_w);
	end if;
end;
end loop;
close c06;

list_icd_w := substr(list_icd_w,0,(length(list_icd_w)-1));

open c03;
loop
fetch c03 into
	c03_w;
	exit when c03%notfound;
	begin

	select  nvl(max(cd_procedimento),0)
	into    cd_procedimento_w
	from    procedimento
	where   cd_procedimento_loc = (
		select  substr(c03_w.valueid,0,length(c03_w.valueid)-2) || '-' || substr(c03_w.valueid,length(c03_w.valueid)-1,length(c03_w.valueid))
		from    dual)
	and 	ie_situacao = 'A'
	and	ie_origem_proced 	= 19;

	select	nvl(max(nr_sequencia),0)
	into 	qt_achi_count_w
	from  	procedimento_pac_medico
	where  	nr_atendimento 		= nr_atendimento_p
	and    	cd_procedimento 	= cd_procedimento_w
	and	dt_procedimento		= to_date(nvl(c03_w.procdt,sysdate),'dd/mm/yyyy')
	and 	ie_situacao 		= 'A'
	and	ie_origem_proced 	= 19;

	select	nvl(max(nr_sequencia),0)
	into 	qt_achi_count_w
	from  	procedimento_pac_medico
	where  	nr_atendimento 		= nr_atendimento_p
	and    	cd_procedimento 	= cd_procedimento_w
	and 	ie_situacao 		= 'A'
	and	ie_origem_proced 	= 19;


	list_achi_w := concat(list_achi_w,qt_achi_count_w||',');

	if	(qt_achi_count_w = 0 and cd_procedimento_w <> 0) then

		select	procedimento_pac_medico_seq.nextval
		into	nr_seq_medico_w
		from	dual;

		insert into procedimento_pac_medico(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_atendimento,
					ie_situacao,
					cd_procedimento,
					ie_origem_proced,
					qt_procedimento,
					dt_procedimento,
					cd_setor_atendimento)
		values			(nr_seq_medico_w,
					sysdate,
					nm_usuario_p,
					nr_atendimento_p,
					'A',
					cd_procedimento_w,
					19,
					1,
					to_date(nvl(c03_w.procdt,sysdate),'dd/mm/yyyy'),
					cd_setor_atend_p);

		list_achi_w := concat(list_achi_w,nr_seq_medico_w||',');
	else
		update	procedimento_pac_medico
		set	dt_procedimento = to_date(nvl(c03_w.procdt,sysdate),'dd/mm/yyyy')
		where	nr_sequencia = qt_achi_count_w;
	end if;

	if(cd_procedimento_w <> 0) then
		insert_drg_items(cd_procedimento_w,19,null,'PROC',nr_seq_episode_w);
	end if;
end;
end loop;
close c03;

list_achi_w := substr(list_achi_w,0,(length(list_achi_w)-1));

open c08;
loop
fetch c08 into
	c08_w;
	exit when c08%notfound;
	begin

	select  count(1)
	into    qt_morphology_w
	from    cido_morfologia
	where   cd_morfologia = c08_w.valueid
	and     ie_situacao = 'A';

	list_morp_w := concat(list_morp_w,c08_w.valueid||',');

	select  count(1)
	into    qt_morpicd_w
	from    can_loco_regional
	where	nr_atendimento = nr_atendimento_p
	and     ie_situacao = 'A'
	and	dt_liberacao is not null
	and 	cd_morfologia = c08_w.valueid;

	if	(qt_morphology_w <> 0 and qt_morpicd_w = 0) then

		select	max(cd_pessoa_fisica)
		into   	cd_user_w
		from   	atendimento_paciente
		where  	nr_atendimento = nr_atendimento_p;

		insert into can_loco_regional(
				nr_sequencia,
				nr_atendimento,
				cd_morfologia,
				cd_medico,
				ie_situacao,
				dt_avaliacao,
				dt_atualizacao,
				nm_usuario,
				cd_estabelecimento,
				cd_pessoa_fisica,
				dt_liberacao)
		values		(can_loco_regional_seq.nextval,
				nr_atendimento_p,
				c08_w.valueid,
				cd_medico_resp_w,
				'A',
				sysdate,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_w,
				cd_user_w,
				sysdate);
		end if;
	end;
end loop;
close c08;

list_morp_w := substr(list_morp_w,0,(length(list_morp_w)-1));

open c04;
loop
fetch c04 into
	c04_w;
	exit when c04%notfound;
	begin

	update	diagnostico_doenca
	set    	ie_situacao = 'I',
		dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p
	where  	nr_atendimento = nr_atendimento_p
	and    	cd_doenca = c04_w.cd_doenca;
	end;
end loop;
close c04;

open c05;
loop
fetch c05 into
	c05_w;
	exit when c05%notfound;
	begin

	update 	procedimento_pac_medico
	set    	ie_situacao = 'I',
		dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p
	where  	nr_atendimento = nr_atendimento_p
	and    	nr_sequencia = c05_w.nr_sequencia;
	end;
end loop;
close c05;

open c09;
loop
fetch c09 into
	c09_w;
	exit when c09%notfound;
	begin

	update 	can_loco_regional
	set    	ie_situacao = 'I',
		dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p
	where  	nr_atendimento = nr_atendimento_p
	and    	cd_morfologia = c09_w.cd_morfologia;

	end;
end loop;
close c09;

	if (t_gen_items_w.count > 0) then

		x := 0;
		for 	x in 0..t_gen_items_w.count -1 loop

			insert into EPISODIO_PACIENTE_DRG_ITEM
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_pac_drg,
					ie_tipo_item,
					cd_doenca,
					cd_procedimento,
					ie_origem_proced)
			values		(EPISODIO_PACIENTE_DRG_ITEM_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_episode_w,
					t_gen_items_w(x).ie_tipo_item,
					t_gen_items_w(x).cd_doenca,
					t_gen_items_w(x).cd_procedimento,
					t_gen_items_w(x).ie_origem_proced);

		end loop;
	end if;
billing_i18n_pck.set_INATIVAR_DRG('S');

end generate_3m_data;
/