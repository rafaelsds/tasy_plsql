create or replace
procedure GERAR_PESSOA_FISICA_HIST
		(cd_pessoa_fisica_p	in	varchar2,
		 nr_seq_tipo_hist_p	in	number,
		 dt_liberacao_p		in	date,
		 ds_historico_p		in	varchar2,
		 nm_usuario_p		in	varchar2) is

begin

insert into PESSOA_FISICA_HISTORICO
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nm_pessoa_fisica,
		cd_pessoa_fisica,
		nr_seq_tipo_hist,
		dt_liberacao,
		ds_historico)
values		(PESSOA_FISICA_HISTORICO_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		cd_pessoa_fisica_p,
		nr_seq_tipo_hist_p,
		dt_liberacao_p,
		substr(ds_historico_p,1,4000));
commit;

end GERAR_PESSOA_FISICA_HIST;
/