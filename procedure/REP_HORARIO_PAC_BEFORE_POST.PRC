CREATE OR REPLACE
PROCEDURE rep_horario_pac_before_post(	cd_item_p		varchar2,
					nr_prescricoes_p	varchar2,
					ds_horarios_p		varchar2,
					nr_atendimento_p	number,
					nm_usuario_p		varchar2,
					ie_tipo_item_p		varchar2, 
					cd_procedimento_p	varchar2) is

ds_erro_w	varchar2(255);

BEGIN

if	(cd_item_p is not null) and
	(nr_prescricoes_p is not null) and
	(ds_horarios_p is not null) and
	(nr_atendimento_p > 0) then
	begin	
	Consistir_horarios_padroes(	cd_item_p,
					nr_prescricoes_p,
					ds_horarios_p,
					0,
					ds_erro_w);
					
	if	(ds_erro_w is null)then
		Atualizar_rep_horario_pac(	nr_atendimento_p,
						cd_item_p,
						nm_usuario_p,
						ie_tipo_item_p,
						cd_procedimento_p);
	else
		Wheb_mensagem_pck.exibir_mensagem_abort(238415,'DS_ERRO_W='||ds_erro_w);
	end if;	
	end;
end if;
					
END rep_horario_pac_before_post;
/
