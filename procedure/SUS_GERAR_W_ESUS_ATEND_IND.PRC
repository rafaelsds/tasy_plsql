create or replace
procedure sus_gerar_w_esus_atend_ind(	nr_seq_lote_p	esus_lote_envio.nr_sequencia%type,
					ie_tipo_lote_esus_p	esus_lote_envio.ie_tipo_lote_esus%type,
					cd_cnes_estab_p		estabelecimento.cd_cns%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

cd_cns_paciente_w		w_esus_atend_individual.cd_cns_paciente%type;				
dt_nascimento_paciente_w	w_esus_atend_individual.dt_nascimento_paciente%type;					
ie_sexo_paciente_w		w_esus_atend_individual.ie_sexo_paciente%type;
cd_cns_profissional_w		w_esus_atend_individual.cd_cns_profissional%type;
nr_seq_exp_w			w_esus_atend_individual.nr_sequencia%type;
cd_uuid_original_w		w_esus_atend_individual.cd_uuid_original%type;
nm_mae_pessoa_fisica_w		w_esus_atend_individual.nm_mae_pessoa_fisica%type;
cd_municipio_ibge_w		w_esus_atend_individual.cd_municipio_ibge%type;
cd_municipio_ibge_ww		w_esus_atend_individual.cd_municipio_ibge%type;
nm_municipio_nac_pac_w		w_esus_atend_individual.nm_municipio_nac_pac%type;
nr_data_atendimento_w		w_esus_atend_individual.nr_data_atendimento%type;
nr_data_ult_mest_w		w_esus_atend_individual.nr_data_ult_mest%type;
nr_data_nasc_pac_w		w_esus_atend_individual.nr_data_nasc_pac%type;
nr_data_hora_fim_atend_w        w_esus_atend_individual.nr_data_hora_fim_atend%type;
nr_cpf_paciente_w               w_esus_atend_individual.nr_cpf_paciente%type;

nr_sequencia_w 			esus_atend_individual.nr_sequencia%type;
cd_estabelecimento_w 		esus_atend_individual.cd_estabelecimento%type;
dt_atualizacao_w 		esus_atend_individual.dt_atualizacao%type;
nm_usuario_w 			esus_atend_individual.nm_usuario%type;
dt_atualizacao_nrec_w 		esus_atend_individual.dt_atualizacao_nrec%type;
nm_usuario_nrec_w 		esus_atend_individual.nm_usuario_nrec%type;
cd_pessoa_fisica_w 		esus_atend_individual.cd_pessoa_fisica%type;
cd_profissional_w 		esus_atend_individual.cd_profissional%type;
cd_prof_pri_adic_w 		esus_atend_individual.cd_prof_pri_adic%type;
cd_prof_sec_adic_w 		esus_atend_individual.cd_prof_sec_adic%type;
cd_cbo_prof_w 			esus_atend_individual.cd_cbo_prof%type;
cd_cbo_pri_adic_w 		esus_atend_individual.cd_cbo_pri_adic%type;
cd_cbo_sec_adic_w 		esus_atend_individual.cd_cbo_sec_adic%type;
cd_cnes_unidade_w 		esus_atend_individual.cd_cnes_unidade%type;
nr_seq_sus_equipe_w 		esus_atend_individual.nr_seq_sus_equipe%type;
dt_liberacao_w 			esus_atend_individual.dt_liberacao%type;
ie_ret_fundo_olho_w 		esus_atend_individual.ie_ret_fundo_olho%type;
ie_teste_olhinho_w 		esus_atend_individual.ie_teste_olhinho%type;
ie_encam_caps_w 		esus_atend_individual.ie_encam_caps%type;
ie_hdl_w 			esus_atend_individual.ie_hdl%type;
ie_encam_ser_ate_dom_w 		esus_atend_individual.ie_encam_ser_ate_dom%type;
ie_encam_int_hosp_w 		esus_atend_individual.ie_encam_int_hosp%type;
ie_antiglobulina_tia_w 		esus_atend_individual.ie_antiglobulina_tia%type;
ie_encam_serv_espec_w 		esus_atend_individual.ie_encam_serv_espec%type;
ie_tipo_atendimento_w 		esus_atend_individual.ie_tipo_atendimento%type;
ie_ficou_observ_w 		esus_atend_individual.ie_ficou_observ%type;
ie_demanda_espont_w 		esus_atend_individual.ie_demanda_espont%type;
ie_dpoc_w 			esus_atend_individual.ie_dpoc%type;
ie_saude_mental_w 		esus_atend_individual.ie_saude_mental%type;
ie_ldl_w 			esus_atend_individual.ie_ldl%type;
ie_tabagismo_w 			esus_atend_individual.ie_tabagismo%type;
ie_hiper_arterial_w 		esus_atend_individual.ie_hiper_arterial%type;
ie_exame_escarro_w 		esus_atend_individual.ie_exame_escarro%type;
ie_aleitamento_w 		esus_atend_individual.ie_aleitamento%type;
ie_saude_sex_reprod_w 		esus_atend_individual.ie_saude_sex_reprod%type;
ie_encam_urgencia_w 		esus_atend_individual.ie_encam_urgencia%type;
ie_dengue_w 			esus_atend_individual.ie_dengue%type;
ie_puericultura_w 		esus_atend_individual.ie_puericultura%type;
ie_asma_w 			esus_atend_individual.ie_asma%type;
ie_espirometria_w 		esus_atend_individual.ie_espirometria%type;
ie_turno_atendimento_w 		esus_atend_individual.ie_turno_atendimento%type;
ie_agend_nasf_w 		esus_atend_individual.ie_agend_nasf%type;
ie_aval_diag_w 			esus_atend_individual.ie_aval_diag%type;
ie_sorol_dengue_w 		esus_atend_individual.ie_sorol_dengue%type;
ie_ultrasson_obst_w 		esus_atend_individual.ie_ultrasson_obst%type;
ie_desnutricao_w 		esus_atend_individual.ie_desnutricao%type;
ie_teste_pezinho_w 		esus_atend_individual.ie_teste_pezinho%type;
ie_hemograma_w 			esus_atend_individual.ie_hemograma%type;
ie_pre_natal_w 			esus_atend_individual.ie_pre_natal%type;
ds_ciap01_w 			esus_atend_individual.ds_ciap01%type;
ie_creatinina_w 		esus_atend_individual.ie_creatinina%type;
ie_diabetes_w 			esus_atend_individual.ie_diabetes%type;
ie_local_atendimento_w 		esus_atend_individual.ie_local_atendimento%type;
ie_retorno_cons_agend_w 	esus_atend_individual.ie_retorno_cons_agend%type;
ie_proced_cli_terap_w 		esus_atend_individual.ie_proced_cli_terap%type;
nr_prontuario_w 		esus_atend_individual.nr_prontuario%type;
qt_peso_w 			esus_atend_individual.qt_peso%type;
qt_altura_w 			esus_atend_individual.qt_altura%type;
ie_vacina_em_dia_w 		esus_atend_individual.ie_vacina_em_dia%type;
dt_ultima_mest_w 		esus_atend_individual.dt_ultima_mest%type;
ie_gravid_plane_w 		esus_atend_individual.ie_gravid_plane%type;
qt_idade_gestac_w 		esus_atend_individual.qt_idade_gestac%type;
qt_gestas_previas_w 		esus_atend_individual.qt_gestas_previas%type;
qt_partos_w 			esus_atend_individual.qt_partos%type;
ie_atencao_domic_w 		esus_atend_individual.ie_atencao_domic%type;
ie_obesidade_w 			esus_atend_individual.ie_obesidade%type;
ie_puerperio_w 			esus_atend_individual.ie_puerperio%type;
ie_usuario_alcool_w 		esus_atend_individual.ie_usuario_alcool%type;
ie_usuario_drogas_w 		esus_atend_individual.ie_usuario_drogas%type;
ie_reabilitacao_w 		esus_atend_individual.ie_reabilitacao%type;
ie_tuberculose_w 		esus_atend_individual.ie_tuberculose%type;
ie_hanseniase_w 		esus_atend_individual.ie_hanseniase%type;
ie_dst_w 			esus_atend_individual.ie_dst%type;
ie_cancer_colo_utero_w 		esus_atend_individual.ie_cancer_colo_utero%type;
ie_cancer_mama_w 		esus_atend_individual.ie_cancer_mama%type;
ie_risco_cardio_w 		esus_atend_individual.ie_risco_cardio%type;
ds_ciap02_w 			esus_atend_individual.ds_ciap02%type;
cd_doenca_cid_w 		esus_atend_individual.cd_doenca_cid%type;
ie_colesterol_total_w 		esus_atend_individual.ie_colesterol_total%type;
ie_eas_equ_w 			esus_atend_individual.ie_eas_equ%type;
ie_eletrocardiograma_w 		esus_atend_individual.ie_eletrocardiograma%type;
ie_eletroforese_hemo_w 		esus_atend_individual.ie_eletroforese_hemo%type;
ie_glicemia_w 			esus_atend_individual.ie_glicemia%type;
ie_hemoglob_glicada_w 		esus_atend_individual.ie_hemoglob_glicada%type;
ie_sorol_sifilis_w 		esus_atend_individual.ie_sorol_sifilis%type;
ie_sorol_hiv_w 			esus_atend_individual.ie_sorol_hiv%type;
ie_teste_orelhinha_w 		esus_atend_individual.ie_teste_orelhinha%type;
ie_teste_gravidez_w 		esus_atend_individual.ie_teste_gravidez%type;
ie_urocultura_w 		esus_atend_individual.ie_urocultura%type;
ie_usou_pic_w 			esus_atend_individual.ie_usou_pic%type;
ie_prescr_terap_w 		esus_atend_individual.ie_prescr_terap%type;
ie_retorno_cont_prog_w 		esus_atend_individual.ie_retorno_cont_prog%type;
ie_agend_grupos_w 		esus_atend_individual.ie_agend_grupos%type;
ie_alta_episodio_w 		esus_atend_individual.ie_alta_episodio%type;
ie_encam_int_dia_w 		esus_atend_individual.ie_encam_int_dia%type;
ie_encam_intersetor_w 		esus_atend_individual.ie_encam_intersetor%type;
nr_seq_lote_envio_w 		esus_atend_individual.nr_seq_lote_envio%type;
ie_coleste_total_sol_w 		esus_atend_individual.ie_coleste_total_sol%type;
ie_creatinina_solic_w 		esus_atend_individual.ie_creatinina_solic%type;
ie_eas_equ_solicit_w 		esus_atend_individual.ie_eas_equ_solicit%type;
ie_eletrocard_solic_w 		esus_atend_individual.ie_eletrocard_solic%type;
ie_eletrof_hemo_sol_w 		esus_atend_individual.ie_eletrof_hemo_sol%type;
ie_espirometria_soli_w 		esus_atend_individual.ie_espirometria_soli%type;
ie_exame_escarro_sol_w 		esus_atend_individual.ie_exame_escarro_sol%type;
ie_glicemia_solicita_w 		esus_atend_individual.ie_glicemia_solicita%type;
ie_hdl_solicitado_w 		esus_atend_individual.ie_hdl_solicitado%type;
ie_hemog_glicada_sol_w 		esus_atend_individual.ie_hemog_glicada_sol%type;
ie_hemograma_solicit_w 		esus_atend_individual.ie_hemograma_solicit%type;
ie_ldl_solicitado_w 		esus_atend_individual.ie_ldl_solicitado%type;
ie_ret_fun_olho_solic_w 	esus_atend_individual.ie_ret_fun_olho_solic%type;
ie_sorol_sifilis_sol_w 		esus_atend_individual.ie_sorol_sifilis_sol%type;
ie_sorol_dengue_sol_w 		esus_atend_individual.ie_sorol_dengue_sol%type;
ie_sorol_hiv_solicit_w 		esus_atend_individual.ie_sorol_hiv_solicit%type;
ie_antiglob_tia_solic_w 	esus_atend_individual.ie_antiglob_tia_solic%type;
ie_teste_orelhin_sol_w 		esus_atend_individual.ie_teste_orelhin_sol%type;
ie_teste_gravid_sol_w 		esus_atend_individual.ie_teste_gravid_sol%type;
ie_teste_olhinho_sol_w 		esus_atend_individual.ie_teste_olhinho_sol%type;
ie_teste_pezinho_sol_w 		esus_atend_individual.ie_teste_pezinho_sol%type;
ie_ultras_obste_soli_w 		esus_atend_individual.ie_ultras_obste_soli%type;
ie_urocultura_solic_w 		esus_atend_individual.ie_urocultura_solic%type;
dt_atendimento_w 		esus_atend_individual.dt_atendimento%type;
cd_cnes_unidade_ww 		esus_atend_individual.cd_cnes_unidade%type;
qt_perim_cefalic_w		esus_atend_individual.qt_perim_cefalic%type;
cd_doenca_cid2_w		esus_atend_individual.cd_doenca_cid2%type;
dt_final_atendimento_w          esus_atend_individual.dt_final_atendimento%type;

cd_cns_prof_pri_adic_w		varchar2(20);
cd_cns_prof_sec_adic_w		varchar2(20);

cd_contra_chave_rem_w		w_esus_header_footer.cd_contra_chave_rem%type;
cd_uuid_instal_rem_w		w_esus_header_footer.cd_uuid_instal_rem%type;
cd_contra_chave_ori_w		w_esus_header_footer.cd_contra_chave_ori%type;
cd_uuid_instal_orig_w		w_esus_header_footer.cd_uuid_instal_orig%type;

Cursor AtInd is
	select 	nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		cd_profissional,
		cd_prof_pri_adic,
		cd_prof_sec_adic,
		cd_cbo_prof,
		cd_cbo_pri_adic,
		cd_cbo_sec_adic,
		cd_cnes_unidade,
		nr_seq_sus_equipe,
		dt_liberacao,
		ie_ret_fundo_olho,
		ie_teste_olhinho,
		ie_encam_caps,
		ie_hdl,
		ie_encam_ser_ate_dom,
		ie_encam_int_hosp,
		ie_antiglobulina_tia,
		ie_encam_serv_espec,
		ie_tipo_atendimento,
		ie_ficou_observ,
		ie_demanda_espont,
		ie_dpoc,
		ie_saude_mental,
		ie_ldl,
		ie_tabagismo,
		ie_hiper_arterial,
		ie_exame_escarro,
		ie_aleitamento,
		ie_saude_sex_reprod,
		ie_encam_urgencia,
		ie_puericultura,
		ie_dengue,
		ie_asma,
		ie_espirometria,
		ie_turno_atendimento,
		ie_agend_nasf,
		ie_aval_diag,
		ie_sorol_dengue,
		ie_ultrasson_obst,
		ie_desnutricao,
		ie_teste_pezinho,
		ie_hemograma,
		ie_pre_natal,
		cd_ciap01 ds_ciap01,
		ie_creatinina,
		ie_diabetes,
		ie_local_atendimento,
		ie_retorno_cons_agend,
		ie_proced_cli_terap,
		nr_prontuario,
		qt_peso,
		qt_altura,
		ie_vacina_em_dia,
		dt_ultima_mest,
		ie_gravid_plane,
		qt_idade_gestac,
		qt_gestas_previas,
		qt_partos,
		ie_atencao_domic,
		ie_obesidade,
		ie_puerperio,
		ie_usuario_alcool,
		ie_usuario_drogas,
		ie_reabilitacao,
		ie_tuberculose,
		ie_hanseniase,
		ie_dst,
		ie_cancer_colo_utero,
		ie_cancer_mama,
		ie_risco_cardio,
		cd_ciap02 ds_ciap02,
		cd_doenca_cid,
		ie_colesterol_total,
		ie_eas_equ,
		ie_eletrocardiograma,
		ie_eletroforese_hemo,
		ie_glicemia,
		ie_hemoglob_glicada,
		ie_sorol_sifilis,
		ie_sorol_hiv,
		ie_teste_orelhinha,
		ie_teste_gravidez,
		ie_urocultura,
		ie_usou_pic,
		ie_prescr_terap,
		ie_retorno_cont_prog,
		ie_agend_grupos,
		ie_alta_episodio,
		ie_encam_int_dia,
		ie_encam_intersetor,
		nr_seq_lote_envio,
		ie_coleste_total_sol,
		ie_creatinina_solic,
		ie_eas_equ_solicit,
		ie_eletrocard_solic,
		ie_eletrof_hemo_sol,
		ie_espirometria_soli,
		ie_exame_escarro_sol,
		ie_glicemia_solicita,
		ie_hdl_solicitado,
		ie_hemog_glicada_sol,
		ie_hemograma_solicit,
		ie_ldl_solicitado,
		ie_ret_fun_olho_solic,
		ie_sorol_sifilis_sol,
		ie_sorol_dengue_sol,
		ie_sorol_hiv_solicit,
		ie_antiglob_tia_solic,
		ie_teste_orelhin_sol,
		ie_teste_gravid_sol,
		ie_teste_olhinho_sol,
		ie_teste_pezinho_sol,
		ie_ultras_obste_soli,
		ie_urocultura_solic,
		dt_atendimento,
		cd_uuid_original,
		qt_perim_cefalic,
		cd_doenca_cid2,
                dt_final_atendimento
	from	esus_atend_individual
	where	nr_seq_lote_envio =	nr_seq_lote_p
	order by dt_atendimento, nr_sequencia;

type 		fetch_array is table of AtInd%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_AtInd_w			vetor;

Cursor AtIndExa is
	select 	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_procedimento,
			ie_origem_proced,
			ie_avaliado,
			nr_seq_atend_ind,
			ie_solicitado
	from 	esus_atend_ind_exame
	where 	nr_seq_atend_ind = nr_sequencia_w;

AtIndExa_w		AtIndExa%rowtype;
	
begin

delete 	from w_esus_atend_individual
where	nr_seq_lote_envio = nr_seq_lote_p;

delete	from w_esus_atend_ind_exame
where	nr_seq_lote_envio = nr_seq_lote_p;

open AtInd;
loop
fetch AtInd bulk collect into s_array limit 1000;
	vetor_AtInd_w(i) := s_array;
	i := i + 1;
exit when AtInd%notfound;
end loop;
close AtInd;


for i in 1..vetor_AtInd_w.count loop
	begin
	s_array := vetor_AtInd_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_sequencia_w				:= s_array(z).nr_sequencia;
		cd_estabelecimento_w			:= s_array(z).cd_estabelecimento;
		dt_atualizacao_w			:= s_array(z).dt_atualizacao;
		nm_usuario_w				:= s_array(z).nm_usuario;
		dt_atualizacao_nrec_w			:= s_array(z).dt_atualizacao_nrec;
		nm_usuario_nrec_w			:= s_array(z).nm_usuario_nrec;
		cd_pessoa_fisica_w			:= s_array(z).cd_pessoa_fisica;
		cd_profissional_w			:= s_array(z).cd_profissional;
		cd_prof_pri_adic_w			:= s_array(z).cd_prof_pri_adic;
		cd_prof_sec_adic_w			:= s_array(z).cd_prof_sec_adic;
		cd_cbo_prof_w				:= s_array(z).cd_cbo_prof;
		cd_cbo_pri_adic_w			:= s_array(z).cd_cbo_pri_adic;
		cd_cbo_sec_adic_w			:= s_array(z).cd_cbo_sec_adic;
		cd_cnes_unidade_w			:= s_array(z).cd_cnes_unidade;
		nr_seq_sus_equipe_w			:= s_array(z).nr_seq_sus_equipe;
		dt_liberacao_w				:= s_array(z).dt_liberacao;
		ie_ret_fundo_olho_w			:= s_array(z).ie_ret_fundo_olho;
		ie_teste_olhinho_w			:= s_array(z).ie_teste_olhinho;
		ie_encam_caps_w				:= s_array(z).ie_encam_caps;
		ie_hdl_w				:= s_array(z).ie_hdl;
		ie_encam_ser_ate_dom_w			:= s_array(z).ie_encam_ser_ate_dom;
		ie_encam_int_hosp_w			:= s_array(z).ie_encam_int_hosp;
		ie_antiglobulina_tia_w			:= s_array(z).ie_antiglobulina_tia;
		ie_encam_serv_espec_w			:= s_array(z).ie_encam_serv_espec;
		ie_tipo_atendimento_w			:= s_array(z).ie_tipo_atendimento;
		ie_ficou_observ_w			:= s_array(z).ie_ficou_observ;
		ie_demanda_espont_w			:= s_array(z).ie_demanda_espont;
		ie_dpoc_w				:= s_array(z).ie_dpoc;
		ie_saude_mental_w			:= s_array(z).ie_saude_mental;
		ie_ldl_w				:= s_array(z).ie_ldl;
		ie_tabagismo_w				:= s_array(z).ie_tabagismo;
		ie_hiper_arterial_w			:= s_array(z).ie_hiper_arterial;
		ie_exame_escarro_w			:= s_array(z).ie_exame_escarro;
		ie_aleitamento_w			:= s_array(z).ie_aleitamento;
		ie_saude_sex_reprod_w			:= s_array(z).ie_saude_sex_reprod;
		ie_encam_urgencia_w			:= s_array(z).ie_encam_urgencia;
		ie_dengue_w				:= s_array(z).ie_dengue;
		ie_puericultura_w			:= s_array(z).ie_puericultura;
		ie_asma_w				:= s_array(z).ie_asma;
		ie_espirometria_w			:= s_array(z).ie_espirometria;
		ie_turno_atendimento_w			:= s_array(z).ie_turno_atendimento;
		ie_agend_nasf_w				:= s_array(z).ie_agend_nasf;
		ie_aval_diag_w				:= s_array(z).ie_aval_diag;
		ie_sorol_dengue_w			:= s_array(z).ie_sorol_dengue;
		ie_ultrasson_obst_w			:= s_array(z).ie_ultrasson_obst;
		ie_desnutricao_w			:= s_array(z).ie_desnutricao;
		ie_teste_pezinho_w			:= s_array(z).ie_teste_pezinho;
		ie_hemograma_w				:= s_array(z).ie_hemograma;
		ie_pre_natal_w				:= s_array(z).ie_pre_natal;
		ds_ciap01_w				:= s_array(z).ds_ciap01;
		ie_creatinina_w				:= s_array(z).ie_creatinina;
		ie_diabetes_w				:= s_array(z).ie_diabetes;
		ie_local_atendimento_w			:= s_array(z).ie_local_atendimento;
		ie_retorno_cons_agend_w			:= s_array(z).ie_retorno_cons_agend;
		ie_proced_cli_terap_w			:= s_array(z).ie_proced_cli_terap;
		nr_prontuario_w				:= s_array(z).nr_prontuario;
		qt_peso_w				:= s_array(z).qt_peso;
		qt_altura_w				:= s_array(z).qt_altura;
		ie_vacina_em_dia_w			:= s_array(z).ie_vacina_em_dia;
		dt_ultima_mest_w			:= s_array(z).dt_ultima_mest;
		ie_gravid_plane_w			:= s_array(z).ie_gravid_plane;
		qt_idade_gestac_w			:= s_array(z).qt_idade_gestac;
		qt_gestas_previas_w			:= s_array(z).qt_gestas_previas;
		qt_partos_w				:= s_array(z).qt_partos;
		ie_atencao_domic_w			:= s_array(z).ie_atencao_domic;
		ie_obesidade_w				:= s_array(z).ie_obesidade;
		ie_puerperio_w				:= s_array(z).ie_puerperio;
		ie_usuario_alcool_w			:= s_array(z).ie_usuario_alcool;
		ie_usuario_drogas_w			:= s_array(z).ie_usuario_drogas;
		ie_reabilitacao_w			:= s_array(z).ie_reabilitacao;
		ie_tuberculose_w			:= s_array(z).ie_tuberculose;
		ie_hanseniase_w				:= s_array(z).ie_hanseniase;
		ie_dst_w				:= s_array(z).ie_dst;
		ie_cancer_colo_utero_w			:= s_array(z).ie_cancer_colo_utero;
		ie_cancer_mama_w			:= s_array(z).ie_cancer_mama;
		ie_risco_cardio_w			:= s_array(z).ie_risco_cardio;
		ds_ciap02_w				:= s_array(z).ds_ciap02;
		cd_doenca_cid_w				:= s_array(z).cd_doenca_cid;
		ie_colesterol_total_w			:= s_array(z).ie_colesterol_total;
		ie_eas_equ_w				:= s_array(z).ie_eas_equ;
		ie_eletrocardiograma_w			:= s_array(z).ie_eletrocardiograma;
		ie_eletroforese_hemo_w			:= s_array(z).ie_eletroforese_hemo;
		ie_glicemia_w				:= s_array(z).ie_glicemia;
		ie_hemoglob_glicada_w			:= s_array(z).ie_hemoglob_glicada;
		ie_sorol_sifilis_w			:= s_array(z).ie_sorol_sifilis;
		ie_sorol_hiv_w				:= s_array(z).ie_sorol_hiv;
		ie_teste_orelhinha_w			:= s_array(z).ie_teste_orelhinha;
		ie_teste_gravidez_w			:= s_array(z).ie_teste_gravidez;
		ie_urocultura_w				:= s_array(z).ie_urocultura;
		ie_usou_pic_w				:= s_array(z).ie_usou_pic;
		ie_prescr_terap_w			:= s_array(z).ie_prescr_terap;
		ie_retorno_cont_prog_w			:= s_array(z).ie_retorno_cont_prog;
		ie_agend_grupos_w			:= s_array(z).ie_agend_grupos;
		ie_alta_episodio_w			:= s_array(z).ie_alta_episodio;
		ie_encam_int_dia_w			:= s_array(z).ie_encam_int_dia;
		ie_encam_intersetor_w			:= s_array(z).ie_encam_intersetor;
		nr_seq_lote_envio_w			:= s_array(z).nr_seq_lote_envio;
		ie_coleste_total_sol_w			:= s_array(z).ie_coleste_total_sol;
		ie_creatinina_solic_w			:= s_array(z).ie_creatinina_solic;
		ie_eas_equ_solicit_w			:= s_array(z).ie_eas_equ_solicit;
		ie_eletrocard_solic_w			:= s_array(z).ie_eletrocard_solic;
		ie_eletrof_hemo_sol_w			:= s_array(z).ie_eletrof_hemo_sol;
		ie_espirometria_soli_w			:= s_array(z).ie_espirometria_soli;
		ie_exame_escarro_sol_w			:= s_array(z).ie_exame_escarro_sol;
		ie_glicemia_solicita_w			:= s_array(z).ie_glicemia_solicita;
		ie_hdl_solicitado_w			:= s_array(z).ie_hdl_solicitado;
		ie_hemog_glicada_sol_w			:= s_array(z).ie_hemog_glicada_sol;
		ie_hemograma_solicit_w			:= s_array(z).ie_hemograma_solicit;
		ie_ldl_solicitado_w			:= s_array(z).ie_ldl_solicitado;
		ie_ret_fun_olho_solic_w			:= s_array(z).ie_ret_fun_olho_solic;
		ie_sorol_sifilis_sol_w			:= s_array(z).ie_sorol_sifilis_sol;
		ie_sorol_dengue_sol_w			:= s_array(z).ie_sorol_dengue_sol;
		ie_sorol_hiv_solicit_w			:= s_array(z).ie_sorol_hiv_solicit;
		ie_antiglob_tia_solic_w			:= s_array(z).ie_antiglob_tia_solic;
		ie_teste_orelhin_sol_w			:= s_array(z).ie_teste_orelhin_sol;
		ie_teste_gravid_sol_w			:= s_array(z).ie_teste_gravid_sol;
		ie_teste_olhinho_sol_w			:= s_array(z).ie_teste_olhinho_sol;
		ie_teste_pezinho_sol_w			:= s_array(z).ie_teste_pezinho_sol;
		ie_ultras_obste_soli_w			:= s_array(z).ie_ultras_obste_soli;
		ie_urocultura_solic_w			:= s_array(z).ie_urocultura_solic;
		dt_atendimento_w			:= s_array(z).dt_atendimento;
		cd_uuid_original_w			:= s_array(z).cd_uuid_original;	
		qt_perim_cefalic_w			:= s_array(z).qt_perim_cefalic;	
		cd_doenca_cid2_w			:= s_array(z).cd_doenca_cid2;	
		dt_final_atendimento_w			:= s_array(z).dt_final_atendimento;	
		
		if	(dt_atendimento_w is not null) then
			nr_data_atendimento_w := rpad(((dt_atendimento_w - to_date('01/01/1970 00:00:00','dd/mm/yyyy hh24:mi:ss')) * 86400),13,0);
		else
			nr_data_atendimento_w := null;
		end if;
		
		if	(dt_ultima_mest_w is not null) then
			nr_data_ult_mest_w := rpad(((dt_ultima_mest_w - to_date('01/01/1970 00:00:00','dd/mm/yyyy hh24:mi:ss')) * 86400),13,0);
		else
			nr_data_ult_mest_w := null;
		end if;
                
                if	(dt_final_atendimento_w is not null) then
			nr_data_hora_fim_atend_w := rpad(((dt_final_atendimento_w - to_date('01/01/1970 00:00:00','dd/mm/yyyy hh24:mi:ss')) * 86400),13,0);
		else
			nr_data_hora_fim_atend_w := null;
		end if;
				
		if	(nvl(cd_pessoa_fisica_w,'X') <> 'X') then
			begin
				cd_cns_paciente_w		:= substr(obter_dados_pf(cd_pessoa_fisica_w,'CNS'),1,20);
				dt_nascimento_paciente_w	:= to_date(substr(obter_dados_pf(cd_pessoa_fisica_w,'DN'),1,10),'dd/mm/yyyy');
				ie_sexo_paciente_w		:= substr(sus_gerar_depara_esus(obter_dados_pf(cd_pessoa_fisica_w,'SE'),'SEXO'),1,20);
				nm_mae_pessoa_fisica_w		:= substr(elimina_acentos(obter_compl_pf(cd_pessoa_fisica_w, 5, 'N'),'S'),1,250);
				cd_municipio_ibge_w		:= substr(obter_dados_pf_pj(cd_pessoa_fisica_w,'','CDMDV'),1,7);
				nm_municipio_nac_pac_w		:= substr(elimina_acentos(obter_dados_pf(cd_pessoa_fisica_w,'CNP'),'S'),1,255);
                                nr_cpf_paciente_w               := substr(obter_dados_pf(cd_pessoa_fisica_w,'CPF'),1,11);
				if	(dt_nascimento_paciente_w is not null) then
					nr_data_nasc_pac_w := rpad(((dt_nascimento_paciente_w - to_date('01/01/1970 00:00:00','dd/mm/yyyy hh24:mi:ss')) * 86400),13,0);
				else
					nr_data_nasc_pac_w := null;
				end if;
				if	(nvl(nr_prontuario_w,0) = 0) then
					nr_prontuario_w := to_number(obter_dados_pf(cd_pessoa_fisica_w,'NP'));
				end if;
			end;
		end if;		

		begin
			cd_cns_profissional_w := substr(obter_dados_pf(cd_profissional_w,'CNS'),1,20);		
		exception
		when others then
			cd_cns_profissional_w := '';
		end;
		
		begin
			cd_cns_prof_pri_adic_w := substr(obter_dados_pf(cd_prof_pri_adic_w,'CNS'),1,20);		
		exception
		when others then
			cd_cns_prof_pri_adic_w := '';
		end;
		
		begin
			cd_cns_prof_sec_adic_w := substr(obter_dados_pf(cd_prof_sec_adic_w,'CNS'),1,20);		
		exception
		when others then
			cd_cns_prof_sec_adic_w := '';
		end;
		
		select	w_esus_atend_individual_seq.nextval
		into	nr_seq_exp_w
		from	dual;		
		
		select	nvl(max(cd_cnes_unidade),'0')
		into	cd_cnes_unidade_ww
		from	w_esus_header_footer
		where	nr_seq_lote_envio = nr_seq_lote_p;
		
		if	(cd_cnes_unidade_w <> cd_cnes_unidade_ww) then
			begin
			cd_contra_chave_rem_w	:= substr(sus_gerar_uuid_esus(cd_cnes_unidade_w,'C'),1,50);
			cd_uuid_instal_rem_w	:= substr(sus_gerar_uuid_esus(cd_cnes_unidade_w,'U'),1,50);
			cd_contra_chave_ori_w	:= cd_contra_chave_rem_w;
			cd_uuid_instal_orig_w	:= cd_uuid_instal_rem_w;			
			cd_municipio_ibge_ww	:= sus_obter_dados_equipe(nr_seq_sus_equipe_w,'CM');
			cd_municipio_ibge_ww	:= substr(cd_municipio_ibge_ww||Calcula_Digito('MODULO10',cd_municipio_ibge_ww),1,7);
			
			update 	w_esus_header_footer
			set 	cd_cnes_unidade = cd_cnes_unidade_w,
				cd_cnes_serealizado = cd_cnes_unidade_w,
				cd_contra_chave_rem = cd_contra_chave_rem_w,
				cd_uuid_instal_rem = cd_uuid_instal_rem_w,
				cd_contra_chave_ori = cd_contra_chave_ori_w,
				cd_uuid_instal_orig = cd_uuid_instal_orig_w,
				cd_municipio_ibge = cd_municipio_ibge_ww
			where	nr_seq_lote_envio = nr_seq_lote_p;
			
			end;
		end if;
		
		if	(nvl(cd_uuid_original_w,'X') = 'X')  then
			cd_uuid_original_w := nvl(sus_gerar_uuid_esus(cd_cnes_unidade_w,'U'),'0');
			update esus_atend_individual set cd_uuid_original = cd_uuid_original_w where  nr_sequencia = nr_sequencia_w;
		end if;	
		
		insert into w_esus_atend_individual (nr_sequencia,                   
							cd_estabelecimento,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							dt_atendimento,
							nm_usuario_nrec,
							cd_pessoa_fisica,
							cd_profissional,
							cd_prof_pri_adic,
							cd_prof_sec_adic,
							cd_cbo_prof,
							cd_cbo_pri_adic,
							cd_cbo_sec_adic,
							cd_cnes_unidade,
							nr_seq_sus_equipe,
							ie_turno_atendimento,
							nr_prontuario,
							ie_local_atendimento,
							ie_tipo_atendimento,
							ie_demanda_espont,
							qt_peso,
							qt_altura,
							ie_vacina_em_dia,
							ie_aleitamento,
							dt_ultima_mest,
							ie_gravid_plane,
							qt_idade_gestac,
							qt_gestas_previas,
							qt_partos,
							ie_atencao_domic,
							ie_asma,
							ie_desnutricao,
							ie_diabetes,
							ie_dpoc,
							ie_hiper_arterial,
							ie_obesidade,
							ie_pre_natal,
							ie_puericultura,
							ie_puerperio,
							ie_saude_sex_reprod,
							ie_tabagismo,
							ie_usuario_alcool,
							ie_usuario_drogas,
							ie_saude_mental,
							ie_reabilitacao,
							ie_tuberculose,
							ie_hanseniase,
							ie_dengue,
							ie_dst,
							ie_cancer_colo_utero,
							ie_cancer_mama,
							ie_risco_cardio,
							ds_ciap01,
							ds_ciap02,
							cd_doenca_cid,
							ie_coleste_total_sol,
							ie_colesterol_total,
							ie_creatinina_solic,
							ie_creatinina,
							ie_eas_equ_solicit,
							ie_eas_equ,
							ie_eletrocard_solic,
							ie_eletrocardiograma,
							ie_eletrof_hemo_sol,
							ie_eletroforese_hemo,
							ie_espirometria_soli,
							ie_espirometria,
							ie_exame_escarro_sol,
							ie_exame_escarro,
							ie_glicemia_solicita,
							ie_glicemia,
							ie_hdl_solicitado,
							ie_hdl,
							ie_hemog_glicada_sol,
							ie_hemoglob_glicada,
							ie_hemograma_solicit,
							ie_hemograma,
							ie_ldl_solicitado,
							ie_ldl,
							ie_ret_fun_olho_solic,
							ie_ret_fundo_olho,
							ie_sorol_sifilis_sol,
							ie_sorol_sifilis,
							ie_sorol_dengue_sol,
							ie_sorol_dengue,
							ie_sorol_hiv_solicit,
							ie_sorol_hiv,
							ie_antiglob_tia_solic,
							ie_antiglobulina_tia,
							ie_teste_orelhin_sol,
							ie_teste_orelhinha,
							ie_teste_gravid_sol,
							ie_teste_gravidez,
							ie_teste_olhinho_sol,
							ie_teste_olhinho,
							ie_teste_pezinho_sol,
							ie_teste_pezinho,
							ie_ultras_obste_soli,
							ie_ultrasson_obst,
							ie_urocultura_solic,
							ie_urocultura,
							ie_usou_pic,
							ie_ficou_observ,
							ie_aval_diag,
							ie_proced_cli_terap,
							ie_prescr_terap,
							ie_retorno_cons_agend,
							ie_retorno_cont_prog,
							ie_agend_grupos,
							ie_agend_nasf,
							ie_alta_episodio,
							ie_encam_int_dia,
							ie_encam_serv_espec,
							ie_encam_caps,
							ie_encam_int_hosp,
							ie_encam_urgencia,
							ie_encam_ser_ate_dom,
							ie_encam_intersetor,
							nr_seq_lote_envio,
							cd_municipio_ibge,
							nr_seq_ficha,
							cd_cns_paciente,
							dt_nascimento_paciente,
							cd_cns_profissional,
							ie_sexo_paciente,
							nm_mae_pessoa_fisica,
							nm_municipio_nac_pac,
							cd_uuid_original,
							cd_cns_prof_pri_adic,
							cd_cns_prof_sec_adic,
							nr_data_atendimento,
							nr_data_ult_mest,
							nr_data_nasc_pac,
							qt_perim_cefalic,
							cd_doenca_cid2,
                                                        dt_final_atendimento,
                                                        nr_data_hora_fim_atend,
                                                        nr_cpf_paciente)
					values	(	nr_seq_exp_w,
							cd_estabelecimento_w,
							dt_atualizacao_w,
							nm_usuario_w,
							dt_atualizacao_nrec_w,
							dt_atendimento_w,
							nm_usuario_nrec_w,
							cd_pessoa_fisica_w,
							cd_profissional_w,
							cd_prof_pri_adic_w,
							cd_prof_sec_adic_w,
							cd_cbo_prof_w,
							cd_cbo_pri_adic_w,
							cd_cbo_sec_adic_w,
							cd_cnes_unidade_w,
							nr_seq_sus_equipe_w,
							ie_turno_atendimento_w,
							nr_prontuario_w,
							ie_local_atendimento_w,
							ie_tipo_atendimento_w,
							ie_demanda_espont_w,
							replace(to_char(qt_peso_w),',','.'),
							replace(to_char(qt_altura_w),',','.'),
							decode(ie_vacina_em_dia_w,'S','true','false'),
							ie_aleitamento_w,
							decode(ie_sexo_paciente_w,'0',null,dt_ultima_mest_w),
							decode(ie_sexo_paciente_w,'0','',decode(ie_gravid_plane_w,'S','true','false')),
							decode(ie_sexo_paciente_w,'0','',qt_idade_gestac_w),
							decode(ie_sexo_paciente_w,'0','',qt_gestas_previas_w),
							decode(ie_sexo_paciente_w,'0','',qt_partos_w),
							ie_atencao_domic_w,
							decode(ie_asma_w,'S','ABP009',''),
							decode(ie_desnutricao_w,'S','ABP008',''),
							decode(ie_diabetes_w,'S','ABP006',''),
							decode(ie_dpoc_w,'S','ABP010',''),
							decode(ie_hiper_arterial_w,'S','ABP005',''),
							decode(ie_obesidade_w,'S','ABP007',''),
							decode(ie_pre_natal_w,'S','ABP001',''),
							decode(ie_puericultura_w,'S','ABP004',''),
							decode(ie_puerperio_w,'S','ABP002',''),
							decode(ie_saude_sex_reprod_w,'S','ABP003',''),
							decode(ie_tabagismo_w,'S','ABP011',''),
							decode(ie_usuario_alcool_w,'S','ABP012',''),
							decode(ie_usuario_drogas_w,'S','ABP013',''),
							decode(ie_saude_mental_w,'S','ABP014',''),
							decode(ie_reabilitacao_w,'S','ABP015',''),
							decode(ie_tuberculose_w,'S','ABP017',''),
							decode(ie_hanseniase_w,'S','ABP018',''),
							decode(ie_dengue_w,'S','ABP019',''),
							decode(ie_dst_w,'S','ABP020',''),
							decode(ie_cancer_colo_utero_w,'S','ABP022',''),
							decode(ie_cancer_mama_w,'S','ABP023',''),
							decode(ie_risco_cardio_w,'S','ABP024',''),
							ds_ciap01_w,
							ds_ciap02_w,
							cd_doenca_cid_w,
							decode(ie_coleste_total_sol_w,'S','ABEX002',''),
							decode(ie_colesterol_total_w,'S','ABEX002',''),
							decode(ie_creatinina_solic_w,'S','ABEX003',''),
							decode(ie_creatinina_w,'S','ABEX003',''),
							decode(ie_eas_equ_solicit_w,'S','ABEX027',''),
							decode(ie_eas_equ_w,'S','ABEX027',''),
							decode(ie_eletrocard_solic_w,'S','ABEX004',''),
							decode(ie_eletrocardiograma_w,'S','ABEX004',''),
							decode(ie_eletrof_hemo_sol_w,'S','ABEX030',''),
							decode(ie_eletroforese_hemo_w,'S','ABEX030',''),
							decode(ie_espirometria_soli_w,'S','ABEX005',''),
							decode(ie_espirometria_w,'S','ABEX005',''),
							decode(ie_exame_escarro_sol_w,'S','ABEX006',''),
							decode(ie_exame_escarro_w,'S','ABEX006',''),
							decode(ie_glicemia_solicita_w,'S','ABEX026',''),
							decode(ie_glicemia_w,'S','ABEX026',''),
							decode(ie_hdl_solicitado_w,'S','ABEX007',''),
							decode(ie_hdl_w,'S','ABEX007',''),
							decode(ie_hemog_glicada_sol_w,'S','ABEX008',''),
							decode(ie_hemoglob_glicada_w,'S','ABEX008',''),
							decode(ie_hemograma_solicit_w,'S','ABEX028',''),
							decode(ie_hemograma_w,'S','ABEX028',''),
							decode(ie_ldl_solicitado_w,'S','ABEX009',''),
							decode(ie_ldl_w,'S','ABEX009',''),
							decode(ie_ret_fun_olho_solic_w,'S','ABEX013',''),
							decode(ie_ret_fundo_olho_w,'S','ABEX013',''),
							decode(ie_sorol_sifilis_sol_w,'S','ABEX019',''),
							decode(ie_sorol_sifilis_w,'S','ABEX019',''),
							decode(ie_sorol_dengue_sol_w,'S','ABEX016',''),
							decode(ie_sorol_dengue_w,'S','ABEX016',''),
							decode(ie_sorol_hiv_solicit_w,'S','ABEX018',''),
							decode(ie_sorol_hiv_w,'S','ABEX018',''),
							decode(ie_antiglob_tia_solic_w,'S','ABEX031',''),
							decode(ie_antiglobulina_tia_w,'S','ABEX031',''),
							decode(ie_teste_orelhin_sol_w,'S','ABEX020',''),
							decode(ie_teste_orelhinha_w,'S','ABEX020',''),
							decode(ie_teste_gravid_sol_w,'S','ABEX023',''),
							decode(ie_teste_gravidez_w,'S','ABEX023',''),
							decode(ie_teste_olhinho_sol_w,'S','ABEX022',''),
							decode(ie_teste_olhinho_w,'S','ABEX022',''),
							decode(ie_teste_pezinho_sol_w,'S','ABEX021',''),
							decode(ie_teste_pezinho_w,'S','ABEX021',''),
							decode(ie_ultras_obste_soli_w,'S','ABEX024',''),
							decode(ie_ultrasson_obst_w,'S','ABEX024',''),
							decode(ie_urocultura_solic_w,'S','ABEX029',''),
							decode(ie_urocultura_w,'S','ABEX029',''),
							ie_usou_pic_w,
							decode(ie_ficou_observ_w,'S','true','false'),
							decode(ie_aval_diag_w,'S','1',''),
							decode(ie_proced_cli_terap_w,'S','2',''),
							decode(ie_prescr_terap_w,'S','3',''),
							decode(ie_retorno_cons_agend_w,'S','1',''),
							decode(ie_retorno_cont_prog_w,'S','2',''),
							decode(ie_agend_grupos_w,'S','12',''),
							decode(ie_agend_nasf_w,'S','3',''),
							decode(ie_alta_episodio_w,'S','9',''),
							decode(ie_encam_int_dia_w,'S','11',''),
							decode(ie_encam_serv_espec_w,'S','4',''),
							decode(ie_encam_caps_w,'S','5',''),
							decode(ie_encam_int_hosp_w,'S','6',''),
							decode(ie_encam_urgencia_w,'S','7',''),
							decode(ie_encam_ser_ate_dom_w,'S','8',''),
							decode(ie_encam_intersetor_w,'S','10',''),
							nr_seq_lote_envio_w,
							cd_municipio_ibge_w,
							nr_sequencia_w,
							cd_cns_paciente_w,
							dt_nascimento_paciente_w,
							cd_cns_profissional_w,
							ie_sexo_paciente_w,
							nm_mae_pessoa_fisica_w,
							nm_municipio_nac_pac_w,
							cd_uuid_original_w,
							cd_cns_prof_pri_adic_w,
							cd_cns_prof_sec_adic_w,
							nr_data_atendimento_w,
							decode(ie_sexo_paciente_w,'0',null,nr_data_ult_mest_w),
							nr_data_nasc_pac_w,
							replace(to_char(qt_perim_cefalic_w),',','.'),
							cd_doenca_cid2_w,
                                                        dt_final_atendimento_w,
                                                        nr_data_hora_fim_atend_w,
                                                        nr_cpf_paciente_w);							
															
					open AtIndExa;
					loop
					fetch AtIndExa into AtIndExa_w;
					exit when AtIndExa%notfound;
						begin						
						insert into w_esus_atend_ind_exame(nr_sequencia,           
											dt_atualizacao,       
											nm_usuario,             
											dt_atualizacao_nrec,    
											nm_usuario_nrec,        
											ie_solicitado,
											ie_avaliado,
											nr_seq_atend_ind,
											nr_seq_w_atend_ind,     
											cd_procedimento,
											ie_origem_proced,
											nr_seq_lote_envio,
											nr_seq_ficha)
						values (w_esus_atend_ind_exame_seq.nextval,
										AtIndExa_w.dt_atualizacao,
										AtIndExa_w.nm_usuario,
										AtIndExa_w.dt_atualizacao_nrec,
										AtIndExa_w.nm_usuario_nrec,
										decode(AtIndExa_w.ie_solicitado,'S','S',''),
										decode(AtIndExa_w.ie_avaliado,'S','A',''),
										AtIndExa_w.nr_seq_atend_ind,
										nr_seq_exp_w,
										AtIndExa_w.cd_procedimento,
										AtIndExa_w.ie_origem_proced,
										nr_seq_lote_envio_w,
										nr_sequencia_w);
						end;
					end loop;
					close AtIndExa;

		end;
	end loop;
	end;
end loop;

commit;

end sus_gerar_w_esus_atend_ind;
/
