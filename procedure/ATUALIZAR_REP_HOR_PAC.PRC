create or replace
procedure atualizar_rep_hor_pac (
		cd_material_p		number,
		nr_prescricoes_p	varchar2,
		ds_horarios_p		varchar2,
		nr_atendimento_p	number,
		nm_usuario_p 		varchar2) is

ds_erro_w	varchar2(255);
	
begin
if	(cd_material_p is not null) and
	(nr_prescricoes_p is not null) and
	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	consistir_horarios_padroes(cd_material_p,nr_prescricoes_p,ds_horarios_p,null,ds_erro_w);
	
	if	((ds_erro_w is null) or
		(ds_erro_w = '')) then
		begin
		atualizar_rep_horario_pac(nr_atendimento_p,cd_material_p,nm_usuario_p,'M',null);
		end;
	end if;
	end;
end if;
commit;
end atualizar_rep_hor_pac;
/
