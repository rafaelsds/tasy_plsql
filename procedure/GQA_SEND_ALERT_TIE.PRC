CREATE OR REPLACE PROCEDURE gqa_send_alert_tie (
    nr_encounter_p            NUMBER,
    nr_calculation_rule_p     NUMBER,
    vl_calculation_result_p   NUMBER
) IS

    json_message_w              philips_json;
    json_header_w               philips_json;
    json_patient_w              philips_json;
    json_visit_w                philips_json;
    json_observation_result_w   philips_json;
    json_data_w                 CLOB;
    cd_patient_id_w             pessoa_fisica.cd_pessoa_fisica%TYPE;
    ds_alert_message_w          VARCHAR2(300);
    ds_calculation_name_w       calculos_engine.ds_nome_calculo%TYPE;
    ds_severity_value_w         VARCHAR2(20);
    nr_minimum_value_w          gqa_pendencia_regra.vl_minimo%TYPE;
    nr_maximum_value_w          gqa_pendencia_regra.vl_maximo%TYPE;
    ds_hour_w                   VARCHAR2(10);
    ds_current_datetime_w       VARCHAR2(20);
    ds_low_level_desc_w         VARCHAR2(50);
    ds_high_level_desc_w        VARCHAR2(50);
    
    CURSOR c01 IS
    SELECT
        *
    FROM
        bft_encounter_v
    WHERE
        nr_atendimento = nr_encounter_p;

    CURSOR c02 IS
    SELECT
        alert.ds_alerta
    FROM
        calculos_engine            calc
        INNER JOIN gqa_pendencia              calcevent ON calcevent.nr_seq_calc = calc.nr_sequencia
        INNER JOIN gqa_pendencia_regra        calcrule ON calcrule.nr_seq_pendencia = calcevent.nr_sequencia
        INNER JOIN calculos_engine_advisory   alert ON alert.nr_seq_regra = calcrule.nr_sequencia
    WHERE
        calcrule.ie_situacao = 'A'
        AND calcrule.nr_sequencia = nr_calculation_rule_p;

    rowencounter                c01%rowtype;
BEGIN
    json_message_w := philips_json();
    json_header_w := philips_json();
    json_visit_w := philips_json();
    json_observation_result_w := philips_json();
    
    SELECT
        to_char(current_date, 'YYYYMMDDHH24MISS'),
        to_char(current_date, 'HH:MI AM'),
        obter_desc_expressao(327348),
        obter_desc_expressao(327347)
    INTO
        ds_current_datetime_w,
        ds_hour_w,
        ds_low_level_desc_w,
        ds_high_level_desc_w
    FROM
        dual;

    SELECT
        calc.ds_nome_calculo
    INTO ds_calculation_name_w
    FROM
        gqa_pendencia         calcevent
        INNER JOIN calculos_engine       calc ON calcevent.nr_seq_calc = calc.nr_sequencia
        INNER JOIN gqa_pendencia_regra   calcrule ON calcrule.nr_seq_pendencia = calcevent.nr_sequencia
    WHERE
        calcrule.nr_sequencia = nr_calculation_rule_p;

    SELECT
        nvl(calcrule.vl_minimo, 0) minimum_value,
        nvl(calcrule.vl_maximo, 0) maximum_value,
        nvl(calcrule.ie_advisory_severity, 0) severity_value
    INTO
        nr_minimum_value_w,
        nr_maximum_value_w,
        ds_severity_value_w
    FROM
        calculos_engine       calc
        INNER JOIN gqa_pendencia         calcevent ON calcevent.nr_seq_calc = calc.nr_sequencia
        INNER JOIN gqa_pendencia_regra   calcrule ON calcrule.nr_seq_pendencia = calcevent.nr_sequencia
    WHERE
        calcrule.ie_situacao = 'A'
        AND calcrule.nr_sequencia = nr_calculation_rule_p;

    json_header_w.put('sendingApplication', 'TASY');
    json_header_w.put('sendingFacility', 'PHILIPSEMR');
    json_header_w.put('receivingApplication', 'ACM');
    json_header_w.put('receivingFacility', 'ACMFACILITY');
    json_header_w.put('processingID', 'P');
    
    OPEN c01;
    FETCH c01 INTO rowencounter;
    json_visit_w.put('roomId', rowencounter.room_id);
    json_visit_w.put('bedId', rowencounter.bed_id);
    json_visit_w.put('pointOfCare', rowencounter.point_of_care);
    json_visit_w.put('patientClass', rowencounter.type_encounter_id);
    cd_patient_id_w := rowencounter.patient_id;
    CLOSE c01;
    
    json_patient_w := person_json_pck.get_person(cd_patient_id_w);
    
    FOR r_c02 IN c02 LOOP
        ds_alert_message_w := r_c02.ds_alerta;
        IF vl_calculation_result_p < nr_minimum_value_w THEN
            ds_alert_message_w := ds_alert_message_w
                                  || ' @'
                                  || ds_hour_w
                                  || ' ('
                                  || ds_low_level_desc_w
                                  || ' < '
                                  || nr_minimum_value_w
                                  || ')';

        ELSIF vl_calculation_result_p > nr_maximum_value_w THEN
            ds_alert_message_w := ds_alert_message_w
                                  || ' @'
                                  || ds_hour_w
                                  || ' ('
                                  || ds_high_level_desc_w
                                  || ' > '
                                  || nr_maximum_value_w
                                  || ')';
        ELSE
            GOTO end_loop;
        END IF;

        json_observation_result_w.put('observationValueDescriptor', ds_alert_message_w);
        json_observation_result_w.put('observationValueIdentificationNumber', ds_calculation_name_w);
        json_observation_result_w.put('observationValueSeverityValue', ds_severity_value_w);
        json_observation_result_w.put('observationDateTime', ds_current_datetime_w);
        json_message_w.put('header', json_header_w.to_json_value);
        json_message_w.put('patient', json_patient_w.to_json_value);
        json_message_w.put('patientVisit', json_visit_w.to_json_value);
        json_message_w.put('observationResult', json_observation_result_w.to_json_value);
        sys.dbms_lob.createtemporary(json_data_w, true);
        json_message_w.to_clob(json_data_w);
        json_data_w := bifrost.send_integration_content('outbound.send.alerts.ORUR40', json_data_w, wheb_usuario_pck.get_nm_usuario);
        <<end_loop>>
        NULL;
    END LOOP;

END gqa_send_alert_tie;
/