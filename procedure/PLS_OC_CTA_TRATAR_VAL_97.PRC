create or replace
procedure pls_oc_cta_tratar_val_97(  dados_regra_p    pls_tipos_ocor_pck.dados_regra,
          nr_id_transacao_p  pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
          nm_usuario_p    usuario.nm_usuario%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a compara��o entre prestadores. A combina��o pode ser entre os prestadores do protocolo,
conta ou procedimento, Para cada  combina��o, pode-se selecionar o tipo do prestador a ser comparado.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [ ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_cont_w    pls_integer;
dados_tb_sel_w    pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_gera_ocorrencia_w  varchar2(1);
qt_registro_w    pls_integer;
nr_seq_prest_w    number(20);
nr_seq_prest_comp_w  pls_prestador.nr_sequencia%type;
cd_prestador_w    pls_prestador.cd_prestador%type;
cd_prestador_comp_w  pls_prestador.cd_prestador%type;

-- Informa��es sobre a Regra
cursor c01 (  nr_seq_oc_cta_comb_pc  dados_regra_p.nr_sequencia%type) is
  select  ie_valida_prest_info,
    ie_valida_prest_info_comp,
    ie_tipo_prestador,
    ie_tipo_prestador_comp,
    ie_comparacao,
    ie_campo
  from  pls_oc_cta_val_prest_dif
  where  nr_seq_oc_cta_comb = nr_seq_oc_cta_comb_pc;

-- Carrega os dados do prestador
cursor  c02 (  nr_id_transacao_pc  pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is
  select  a.nr_sequencia nr_seq_selecao,
    c.nr_sequencia nr_seq_protocolo,
    b.nr_seq_prestador_imp_ref nr_seq_prestador_imp,
    pls_obter_cod_prestador(b.nr_seq_prestador_imp_ref, null) cd_prestador_imp,
    b.nr_seq_prestador_exec_imp_ref nr_seq_prestador_exec_imp,
    pls_obter_cod_prestador(b.nr_seq_prestador_exec_imp_ref, null) cd_prestador_exec_imp,
    b.nr_seq_prestador,
    pls_obter_cod_prestador(b.nr_seq_prestador, null) cd_prestador,
    b.nr_seq_prestador_exec,
    pls_obter_cod_prestador(b.nr_seq_prestador_exec, null) cd_prestador_exec,
    c.nr_seq_prestador_imp_ref nr_seq_prest_imp_prot,
    pls_obter_cod_prestador(c.nr_seq_prestador_imp_ref, null) cd_prest_imp_prot,
    c.nr_seq_prestador nr_seq_prest_prot,
    pls_obter_cod_prestador(c.nr_seq_prestador, null) cd_prestador_prot,
    b.nr_seq_prestador_solic_ref,
    pls_obter_cod_prestador(b.nr_seq_prestador_solic_ref, null) cd_prest_solic_ref,
    b.nr_seq_guia,
    b.nr_sequencia nr_seq_conta
  from  pls_oc_cta_selecao_ocor_v a,
    pls_conta b,
    pls_protocolo_conta c
  where  a.nr_id_transacao = nr_id_transacao_pc
  and  a.ie_valido = 'S'
  and  b.nr_sequencia = a.nr_seq_conta
  and  c.nr_sequencia = b.nr_seq_protocolo
  and  b.ie_status  != 'C';

begin

if  	(dados_regra_p.nr_sequencia is not null) then
	nr_cont_w := 0;
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_tipos_ocor_pck.atualiza_campo_auxiliar('V', nr_id_transacao_p, null, dados_regra_p);
	pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);

	-- Carrega as regras
	for r_c01_w in c01 (dados_regra_p.nr_sequencia) loop
      
		-- S� executa a regra se ela n�o for de tipos de prestadores iguais
		if  	(r_c01_w.ie_tipo_prestador <> r_c01_w.ie_tipo_prestador_comp) then
    
			-- abre o cursor com os prestadores a serem comparados
			for 	r_c02_w in c02 (nr_id_transacao_p) loop

				-- inicia como n�o gera ocorr�ncia
				ie_gera_ocorrencia_w := 'N';
				dados_tb_sel_w.ds_observacao(nr_cont_w) := null;

				-- define qual � o prestador que precisa verificar
				case  (r_c01_w.ie_tipo_prestador)
					-- atendimento
					when 'A' then
						if  	(dados_regra_p.ie_evento = 'IMP') then
							nr_seq_prest_w := r_c02_w.nr_seq_prest_imp_prot;
							cd_prestador_w := r_c02_w.cd_prest_imp_prot;
						else
							nr_seq_prest_w := r_c02_w.nr_seq_prest_prot;
							cd_prestador_w := r_c02_w.cd_prestador_prot;
						end if;
					-- solicitante
					when 'S' then
						if  	(dados_regra_p.ie_evento = 'IMP') then
							nr_seq_prest_w := r_c02_w.nr_seq_prestador_imp;
							cd_prestador_w := r_c02_w.cd_prestador_imp;
						else
							nr_seq_prest_w := r_c02_w.nr_seq_prestador;
							cd_prestador_w := r_c02_w.cd_prestador;
						end if;
					-- executor
					when 'E' then
						if  (dados_regra_p.ie_evento = 'IMP') then
							nr_seq_prest_w := r_c02_w.nr_seq_prestador_exec_imp;
							cd_prestador_w := r_c02_w.cd_prestador_exec_imp;
						else
							nr_seq_prest_w := r_c02_w.nr_seq_prestador_exec;
							cd_prestador_w := r_c02_w.cd_prestador_exec;
						end if;
					else
						nr_seq_prest_w := null;
						cd_prestador_w := null;
				end case;

				-- define qual prestador de compara��o
				case  (r_c01_w.ie_tipo_prestador_comp)
					-- atendimento
					when 'A' then
						if  	(dados_regra_p.ie_evento = 'IMP') then
							nr_seq_prest_comp_w := r_c02_w.nr_seq_prest_imp_prot;
							cd_prestador_comp_w := r_c02_w.cd_prest_imp_prot;
						else
							nr_seq_prest_comp_w := r_c02_w.nr_seq_prest_prot;
							cd_prestador_comp_w := r_c02_w.cd_prestador_prot;
						end if;
					-- solicitante
					when 'S' then
						if  	(dados_regra_p.ie_evento = 'IMP') then
							nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador_imp;
							cd_prestador_comp_w := r_c02_w.cd_prestador_imp;
						else
							nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador;
							cd_prestador_comp_w := r_c02_w.cd_prestador;
						end if;
					-- executor
					when 'E' then
						if  	(dados_regra_p.ie_evento = 'IMP') then
							nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador_exec_imp;
							cd_prestador_comp_w := r_c02_w.cd_prestador_exec_imp;
						else
							nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador_exec;
							cd_prestador_comp_w := r_c02_w.cd_prestador_exec;
						end if;
					-- Requisitante
					when 'R' then
						nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador_solic_ref;
						cd_prestador_comp_w := r_c02_w.cd_prest_solic_ref;
					when 'G' then

						select  max(nr_seq_prestador)
						into  	nr_seq_prest_comp_w
						from 	pls_guia_plano
						where  	nr_sequencia  = r_c02_w.nr_seq_guia;

						cd_prestador_comp_w := pls_obter_cod_prestador(nr_seq_prest_comp_w, null);

					else
						nr_seq_prest_comp_w := null;
						cd_prestador_comp_w := null;
				end case;

				-- se obriga o prestador, e o mesmo n�o est� informado...
				if  	(r_c01_w.ie_valida_prest_info = 'S') and
					(nr_seq_prest_w is null) then
					ie_gera_ocorrencia_w := 'S';
				end if;

				-- caso seja para validar o participante a valida��o � feita de uma outra forma
				if  	(r_c01_w.ie_tipo_prestador_comp = 'P') and
					(ie_gera_ocorrencia_w = 'N') then

					-- se for para verificar a igualdade
					if  	(r_c01_w.ie_comparacao = 'I') then

					-- verifica se existe algum participante diferente do prestador que precisa ser verificado
						select  sum(qt)
						into  qt_registro_w
						from  (
							-- aqui retorna os que devem verificar a sequ�ncia
							select  count(1) qt
							from  pls_selecao_ocor_cta a,
							pls_conta b,
							pls_conta_proc c,
							pls_proc_participante d,
							pls_protocolo_conta e
							where  r_c01_w.ie_campo = 'S'
							and  a.nr_id_transacao = nr_id_transacao_p
							and  a.ie_valido = 'S'
							and  b.nr_sequencia = a.nr_seq_conta
							and  c.nr_seq_conta = b.nr_sequencia
							and  c.ie_status not in ('D','M')
							and  d.nr_seq_conta_proc(+) = c.nr_sequencia
							and  e.nr_sequencia = b.nr_seq_protocolo
							and  e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and  d.nr_seq_prestador = nr_seq_prest_w
							union all
							-- aqui retorna caso tenha que validar nulos
							select  count(1) qt
							from  pls_selecao_ocor_cta a,
							pls_conta b,
							pls_conta_proc c,
							pls_proc_participante d,
							pls_protocolo_conta e
							where  a.nr_id_transacao = nr_id_transacao_p
							and  a.ie_valido = 'S'
							and  b.nr_sequencia = a.nr_seq_conta
							and  c.nr_seq_conta = b.nr_sequencia
							and  c.ie_status not in ('D','M')
							and  d.nr_seq_conta_proc(+) = c.nr_sequencia
							and  e.nr_sequencia = b.nr_seq_protocolo
							and  e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and  d.nr_seq_prestador is null
							and  r_c01_w.ie_valida_prest_info_comp = 'S'
							union all
							-- aqui retorna quando deve validar o c�digo do prestador
							select  count(1) qt
							from  pls_selecao_ocor_cta a,
							pls_conta b,
							pls_conta_proc c,
							pls_proc_participante d,
							pls_protocolo_conta e,
							pls_prestador f
							where  r_c01_w.ie_campo = 'C'
							and  a.nr_id_transacao = nr_id_transacao_p
							and  a.ie_valido = 'S'
							and  b.nr_sequencia = a.nr_seq_conta
							and  c.ie_status not in ('D','M')
							and  c.nr_seq_conta = b.nr_sequencia
							and  d.nr_seq_conta_proc(+) = c.nr_sequencia
							and  e.nr_sequencia = b.nr_seq_protocolo
							and  e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and  f.nr_sequencia = d.nr_seq_prestador
							and  f.cd_prestador = cd_prestador_w
							);
					else
						-- verifica se existe algum participante diferente do prestador que precisa ser verificado
						select  sum(qt)
						into  	qt_registro_w
						from  (
							-- aqui retorna os que devem verificar a sequ�ncia
							select  count(1) qt
							from  pls_selecao_ocor_cta a,
							pls_conta b,
							pls_conta_proc c,
							pls_proc_participante d,
							pls_protocolo_conta e
							where  r_c01_w.ie_campo = 'S'
							and  a.nr_id_transacao = nr_id_transacao_p
							and  a.ie_valido = 'S'
							and  b.nr_sequencia = a.nr_seq_conta
							and  c.nr_seq_conta = b.nr_sequencia
							and  c.ie_status not in ('D','M')
							and  d.nr_seq_conta_proc(+) = c.nr_sequencia
							and  e.nr_sequencia = b.nr_seq_protocolo
							and  e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and  d.nr_seq_prestador != nr_seq_prest_w
							union all
							-- aqui retorna caso tenha que validar nulos
							select  count(1) qt
							from  pls_selecao_ocor_cta a,
							pls_conta b,
							pls_conta_proc c,
							pls_proc_participante d,
							pls_protocolo_conta e
							where  a.nr_id_transacao = nr_id_transacao_p
							and  a.ie_valido = 'S'
							and  b.nr_sequencia = a.nr_seq_conta
							and  c.nr_seq_conta = b.nr_sequencia
							and  c.ie_status not in ('D','M')
							and  d.nr_seq_conta_proc(+) = c.nr_sequencia
							and  e.nr_sequencia = b.nr_seq_protocolo
							and  e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and  d.nr_seq_prestador is null
							and  r_c01_w.ie_valida_prest_info_comp = 'S'
							union all
							-- aqui retorna quando deve validar o c�digo do prestador
							select  count(1) qt
							from  pls_selecao_ocor_cta a,
							pls_conta b,
							pls_conta_proc c,
							pls_proc_participante d,
							pls_protocolo_conta e,
							pls_prestador f
							where  r_c01_w.ie_campo = 'C'
							and  a.nr_id_transacao = nr_id_transacao_p
							and  a.ie_valido = 'S'
							and  c.ie_status not in ('D','M')
							and  b.nr_sequencia = a.nr_seq_conta
							and  c.nr_seq_conta = b.nr_sequencia
							and  d.nr_seq_conta_proc(+) = c.nr_sequencia
							and  e.nr_sequencia = b.nr_seq_protocolo
							and  e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and  f.nr_sequencia = d.nr_seq_prestador
							and  f.cd_prestador != cd_prestador_w
							);
					end if;

					-- se encontrou, registra a ocorrencia
					if  	(qt_registro_w > 0) then
						ie_gera_ocorrencia_w := 'S';
					end if;

        -- Valida��o por prestador fornecedor
				elsif  	(r_c01_w.ie_tipo_prestador_comp = 'F') and
					(ie_gera_ocorrencia_w = 'N') then
                    
					-- se for para verificar a igualdade
					if  	(r_c01_w.ie_comparacao = 'I') then
						select  sum(qt)
						into  	qt_registro_w
						from  (
							-- aqui retorna os que devem verificar a sequ�ncia
							select  count(1) qt
							from  	pls_selecao_ocor_cta a,
								pls_conta_mat_ocor_v c
							where   r_c01_w.ie_campo   	= 'S'
							and   	a.nr_id_transacao   	= nr_id_transacao_p
							and  	a.ie_valido     	= 'S'
							and  	c.nr_seq_conta     	= a.nr_seq_conta
							and  	c.nr_seq_conta   	= r_c02_w.nr_seq_conta
							and  	c.nr_seq_prest_fornec_mat   = nr_seq_prest_w
							union all
							-- aqui retorna caso tenha que validar nulos
							select  count(1) qt
							from  	pls_selecao_ocor_cta a,
								pls_conta_mat_ocor_v c
							where  	a.nr_id_transacao   	= nr_id_transacao_p
							and  	a.ie_valido     	= 'S'
							and  	c.nr_seq_conta     	= a.nr_seq_conta
							and  	c.nr_seq_conta   	= r_c02_w.nr_seq_conta
							and  	c.nr_seq_prest_fornec_mat   is null
							and  	r_c01_w.ie_valida_prest_info_comp = 'S'
							union all
							-- aqui retorna quando deve validar o c�digo do prestador
							select  count(1) qt
							from  	pls_selecao_ocor_cta    a,
								pls_conta_mat_ocor_v    c,    
								pls_prestador     	e
							where  	r_c01_w.ie_campo   	= 'C'
							and  	a.nr_id_transacao   	= nr_id_transacao_p
							and  	a.ie_valido     	= 'S'
							and  	c.nr_seq_conta     	= a.nr_seq_conta
							and  	c.nr_seq_conta   	= r_c02_w.nr_seq_conta
							and  	e.nr_sequencia     	= c.nr_seq_prest_fornec_mat
							and	e.cd_prestador     	= cd_prestador_w
							);
					else
						select  sum(qt)
						into  qt_registro_w
						from  (
							-- aqui retorna os que devem verificar a sequ�ncia
							select  count(1) qt
							from  	pls_selecao_ocor_cta a,
								pls_conta_mat_ocor_v c
							where  	r_c01_w.ie_campo 	 = 'S'
							and  	a.nr_id_transacao 	 = nr_id_transacao_p
							and  	a.ie_valido 		 = 'S'
							and  	c.nr_seq_conta 		 = a.nr_seq_conta
							and  	c.nr_seq_protocolo 	  = r_c02_w.nr_seq_protocolo
							and  	c.nr_seq_prest_fornec_mat != nr_seq_prest_w
							union all
							-- aqui retorna caso tenha que validar nulos
							select  count(1) qt
							from  	pls_selecao_ocor_cta a,
								pls_conta_mat_ocor_v c
							where  	a.nr_id_transacao 	= nr_id_transacao_p
							and  	a.ie_valido 		= 'S'
							and  	c.nr_seq_conta 		= a.nr_seq_conta
							and  	c.nr_seq_protocolo 	= r_c02_w.nr_seq_protocolo
							and  	c.nr_seq_prest_fornec_mat 	is null
							and  	r_c01_w.ie_valida_prest_info_comp = 'S'
							union all
							-- aqui retorna quando deve validar o c�digo do prestador
							select  count(1) qt
							from  	pls_selecao_ocor_cta   	a,
								pls_conta_mat_ocor_v  	m,
								pls_prestador    	e
							where  	r_c01_w.ie_campo   	= 'C'
							and  	a.nr_id_transacao   	= nr_id_transacao_p
							and  	a.nr_seq_conta    	= m.nr_seq_conta
							and  	a.ie_valido     	= 'S'
							and  	e.nr_sequencia     	= m.nr_seq_prest_fornec_mat
							and  	m.nr_seq_protocolo  	= r_c02_w.nr_seq_protocolo
							and  	e.cd_prestador     	!= cd_prestador_w
							);
					end if;
          
					-- se encontrou, registra a ocorrencia
					if  	(qt_registro_w > 0) then
						ie_gera_ocorrencia_w := 'S';
					end if;

				elsif  (ie_gera_ocorrencia_w = 'N') then
					-- se obriga o prestador, e o mesmo n�o est� informado... Prestador da compara��o
					if  	(r_c01_w.ie_valida_prest_info_comp = 'S') and
						(nr_seq_prest_comp_w is null) then

						ie_gera_ocorrencia_w := 'S';
					end if;

					-- se for para validar a sequ�ncia
					if  (r_c01_w.ie_campo = 'C') then

						-- se for para validar igualdade
						if  (r_c01_w.ie_comparacao = 'I') then

							-- se os prestadores forem iguais gera a ocorr�ncia
							-- caso n�o seja para verificar se o prestador est� informado corretamente e algum deles for nulo,
							-- n�o deve gerar a ocorr�ncia, para tratar os prestadores nulos deve ser marcado algum ou ambos os checkbox
							if  (cd_prestador_comp_w = cd_prestador_w) then

							ie_gera_ocorrencia_w := 'S';
							end if;
						else
						-- se os prestadores forem diferentes gera a ocorr�ncia
							if  (cd_prestador_comp_w != cd_prestador_w) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					else
						-- se for para validar igualdade
						if  (r_c01_w.ie_comparacao = 'I') then
							-- se os prestadores forem iguais gera a ocorr�ncia
							-- caso n�o seja para verificar se o prestador est� informado corretamente e algum deles for nulo,
							-- n�o deve gerar a ocorr�ncia, para tratar os prestadores nulos deve ser marcado algum ou ambos os checkbox
							if  (nr_seq_prest_comp_w = nr_seq_prest_w) then

								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							-- se os prestadores forem diferentes gera a ocorr�ncia
							if  (nr_seq_prest_comp_w != nr_seq_prest_w) 	then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					end if;
				end if;

				if  	(ie_gera_ocorrencia_w = 'S') then

					dados_tb_sel_w.ie_valido(nr_cont_w) := 'S';
					dados_tb_sel_w.nr_seq_selecao(nr_cont_w) := r_C02_w.nr_seq_selecao;

					dados_tb_sel_w.ds_observacao(nr_cont_w) := 'O prestador ' ||
						case   r_c01_w.ie_tipo_prestador
							when 'A' then 'do protocolo '
							when 'S' then 'solicitante '
							when 'E' then 'executor '
							when 'P' then 'participante '
						end
						|| '(' ||
						case 	r_c01_w.ie_campo
							when 'S' then 'seq. ' || nr_seq_prest_w
							when 'C' then 'c�d. ' || cd_prestador_w
						end
						|| ') ' ||
						case 	r_c01_w.ie_comparacao
							when 'I' then '� igual ao prestador '
							when 'D' then '� diferente do prestador '
						end ||
						case   r_c01_w.ie_tipo_prestador_comp
							when 'A' then 'do protocolo '
							when 'S' then 'solicitante '
							when 'E' then 'executor '
							when 'P' then 'participante '
							when 'R' then 'requisitante '
							when 'F' then 'Fornecedor '
							when 'G' then 'da Guia'
						end
						|| '(' ||
						case r_c01_w.ie_campo                        
							  when 'S' then 'seq. ' || nr_seq_prest_comp_w
							  when 'C' then 'c�d. ' || cd_prestador_comp_w
						end || ')';

					if  	(nr_cont_w >= pls_util_pck.qt_registro_transacao_w) then

						pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
												'SEQ', dados_tb_sel_w.ds_observacao,
												dados_tb_sel_w.ie_valido, nm_usuario_p,
												nr_id_transacao_p);

						pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);

						nr_cont_w := 0;
					else
						nr_cont_w := nr_cont_w + 1;
					end if;
				end if;
			end loop; -- Prestadores a serem comparados por regra

			/*Lan�a as glosas caso existir registros que n�o foram gerados*/
			if  	(nr_cont_w > 0)  then
				pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
									    'SEQ', dados_tb_sel_w.ds_observacao,
									    dados_tb_sel_w.ie_valido, nm_usuario_p,
									    nr_id_transacao_p);
			end if;
		end if; -- fim tipo prestador diferente
	end loop; -- regras

	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;


end pls_oc_cta_tratar_val_97;
/