create or replace
procedure realizar_perda_medic( nr_seq_ordem_p		number,
				cd_setor_atendimento_p	number,
				cd_operacao_p		number,
				cd_material_p		number,
				nm_usuario_p		Varchar2) is 

			
nr_seq_cabine_w		number(10);
nr_seq_etapa_prod_w	number(10);
nr_atendimento_w	number(10,0);
cd_material_w		number(06,0);
cd_unidade_medida_w	varchar2(30);
qt_material_w		number(15,3);
dt_prescricao_w		date;
nr_prescricao_w		Number(14);
cd_motivo_baixa_w	varchar2(1);
nr_seq_lote_fornec_w	number(10,0);
ie_via_aplicacao_w	varchar2(5);
nr_seq_item_prescr_w	number(10);
cd_local_estoque_w	Number(4);
cd_oper_perda_etq_w	number(3);
nr_sequencia_w		number(10);
			
Cursor C01 is
	select	nvl(a.nr_atendimento,c.nr_atendimento),
		b.cd_material,
		b.cd_unidade_medida_real,
		b.qt_dose_real,
		c.dt_prescricao,
		a.nr_prescricao,
		b.nr_seq_lote_fornec,
		a.ie_via_aplicacao,
		b.nr_seq_item_prescr,
		nr_seq_etapa_prod
	from	prescr_medica c,
		can_ordem_prod_mat b,
		can_ordem_prod a
	where	a.nr_sequencia	= nr_seq_ordem_p
	and	a.nr_prescricao	= c.nr_prescricao
	and	a.nr_sequencia	= b.nr_seq_ordem
	and 	b.cd_material = cd_material_p
	and 	nvl(a.ie_medic_paciente,'N') = 'N';
    
    cursor c02 is
    select	nr_sequencia
	from	material_atend_paciente
	where 	nr_seq_ordem_prod   = nr_seq_ordem_p
	and	    cd_material         = cd_material_p;
				
begin

if 	(cd_operacao_p is not null) and (cd_setor_atendimento_p is not null) then
		
    open C02;
    loop
    fetch C02 into	
    	nr_sequencia_w;
    exit when C02%notfound;
    	begin
        voltar_item_pendente_prescr(nr_sequencia_w, nm_usuario_p);
        excluir_glosa_valor_mat(nr_sequencia_w, nm_usuario_p); 
    	end;
    end loop;
    close C02;
    

	open C01;
	loop
	fetch C01 into	
		nr_atendimento_w,
		cd_material_w, 
		cd_unidade_medida_w,
		qt_material_w,
		dt_prescricao_w,
		nr_prescricao_w,
		nr_seq_lote_fornec_w,
		ie_via_aplicacao_w,
		nr_seq_item_prescr_w,
		nr_seq_etapa_prod_w;
	exit when C01%notfound;
		begin 
		
		select	max(nr_seq_cabine)
		into	nr_seq_cabine_w
		from	far_etapa_producao 
		where	nr_sequencia = nr_seq_etapa_prod_w;
		
		select	cd_local_estoque,
			cd_oper_perda_etq
		into	cd_local_estoque_w,
			cd_oper_perda_etq_w
		from	far_cabine_seg_biol
		where	nr_sequencia = nr_seq_cabine_w;
		
		-- Baixa como perda de medica��o
		insert into movimento_estoque(
			nr_movimento_estoque,		cd_estabelecimento,
			cd_local_estoque,		dt_movimento_estoque,
			cd_operacao_estoque,		cd_acao,
			cd_material,			cd_unidade_med_mov,
			qt_movimento,			dt_mesano_referencia,
			dt_atualizacao,			nm_usuario,
			ie_origem_documento,		nr_documento,
			cd_unidade_medida_estoque,	cd_setor_atendimento,
			qt_estoque,			ds_observacao,
			nr_atendimento,			nr_prescricao,
			nr_seq_lote_fornec)
		values(movimento_estoque_seq.nextval,	wheb_usuario_pck.get_cd_estabelecimento,
			cd_local_estoque_w,		sysdate,
			cd_operacao_p,			'1',
			cd_material_w,			cd_unidade_medida_w,
			qt_material_w,			sysdate,
			sysdate,			nm_usuario_p,
			'10',				nr_seq_ordem_p,
			cd_unidade_medida_w,		cd_setor_atendimento_p,
			qt_material_w,			'realizar_perda_medic' || nr_seq_ordem_p,
			nr_atendimento_w,		nr_prescricao_w,
			nr_seq_lote_fornec_w);
		commit;
		
		end;
	end loop;
	close C01; 

		update 	can_ordem_prod_mat 
	set 	ie_devolve_cabine = 'N'
	where 	nr_seq_ordem = nr_seq_ordem_p 
	and	cd_material = cd_material_p;
	
	commit;
	
	delete 	from can_ordem_prod_mat 
	where 	nr_seq_ordem = nr_seq_ordem_p 
	and	cd_material = cd_material_p;

	commit;

	update	can_ordem_prod
	set	dt_entrega_setor 	= null,
		nm_usuario_disp		= null,
		dt_inicio_preparo	= null,
		nm_usuario_inic_prep	= null,
		dt_fim_preparo		= null,
		dt_solic_perda 		= null
	where	nr_sequencia		= nr_seq_ordem_p;


	commit;
end if;

end realizar_perda_medic;
/