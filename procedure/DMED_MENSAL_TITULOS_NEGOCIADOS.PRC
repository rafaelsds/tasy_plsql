create or replace
procedure dmed_mensal_titulos_negociados
			(	nr_seq_dmed_p		number,
				dt_referencia_p		date,
				cd_estabelecimento_p	number) as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
	DMED_MENSAL_TITULAR_PLANO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_titular_w			varchar(10);
dt_liquidacao_w			date;
nr_titulo_w				number(10);
vl_titulo_w				number(15,2);
vl_recebido_w			number(15,2);
vl_juros_w				number(15,2);
vl_multa_W				number(15,2);
dt_ref_inicial_w		date;
dt_ref_final_w			date;
ie_juros_multa_w		dmed_regra_geral.ie_juros_multa%type;
vl_juros_multa_neg_w	pls_titulo_rec_liq_neg.vl_baixa%type;
cd_beneficiario_w 		varchar2(14);
vl_total_w				number(15,2);

cursor  c01 is
	select		c.cd_pessoa_fisica titular,
			l.vl_juros,
			l.vl_multa,
			l.dt_recebimento,
			o.nr_titulo,
			o.vl_titulo vl_titulo,
			l.vl_recebido vl_recebido,
			'Tasy2.1' ds_union,
			l.nr_sequencia nr_seq_baixa
	from 		titulo_receber_liq	l,
			titulo_receber		o,
			negociacao_cr		c
	where		c.ie_status	<> 'CA'
	and		c.cd_estabelecimento = cd_estabelecimento_p
	and		l.nr_titulo	= o.nr_titulo
	and		l.dt_recebimento between dt_ref_inicial_w and dt_ref_final_w
	and 		c.cd_pessoa_fisica is not null	
	and 		o.nr_titulo in 	(select	distinct a.nr_titulo
					from 	titulo_receber a
					where 	a.nr_seq_negociacao_origem = c.nr_sequencia)
	and	exists	(select	1
			from	dmed_regra_tipo_tit	w
			where	w.ie_tipo_receber		= l.cd_tipo_recebimento
			and	nvl(w.ie_prestadora_ops,'P')	= 'O')
	and	exists	(select	1
			from	dmed_regra_origem_tit	w
			where	w.ie_origem_titulo	= o.ie_origem_titulo)
	and	 not exists (	select	1
				from	dmed_titulos_mensal x
				where	x.nr_documento = o.nr_titulo)
	union		
	select	c.cd_pessoa_fisica titular,
			sum(l.vl_juros) vl_juros,
			sum(l.vl_multa) vl_multa,
			l.dt_recebimento dt_recebimento,
			b.nr_titulo,
			sum(o.vl_titulo) vl_titulo,
			sum(l.vl_recebido) vl_recebido,
			'Tasy2.2' ds_union,
			l.nr_sequencia nr_seq_baixa
	from 	titulo_receber_liq	l,
			titulo_receber		o,
			negociacao_cr_boleto	b,
			negociacao_cr_parcela	p,
			negociacao_cr		c
	where 	b.nr_seq_negociacao	= p.nr_seq_negociacao
	and		c.cd_estabelecimento = cd_estabelecimento_p
	and		c.ie_status	<> 'CA'
	and		p.ie_forma_pagto = 'B'
	AND 	b.nr_seq_parcela = p.nr_sequencia
	and    c.nr_sequencia = b.nr_seq_negociacao
	and		l.nr_titulo	= o.nr_titulo
	and		o.nr_titulo	 = b.nr_titulo
	and 		c.cd_pessoa_fisica is not null
	--and		b.nr_titulo	= substr(obter_tit_tesouraria_neg_cr(p.nr_seq_negociacao,p.nr_sequencia),1,20)	
	and		l.dt_recebimento between dt_ref_inicial_w and dt_ref_final_w
	and		l.vl_recebido > 0
	and not exists (select 1 from titulo_receber_liq x where x.nr_titulo = l.nr_titulo and x.nr_seq_liq_origem = l.nr_sequencia and	rownum = 1)
	and	exists	(select	1
			from	dmed_regra_tipo_tit	w
			where	w.ie_tipo_receber		= l.cd_tipo_recebimento
			and	nvl(w.ie_prestadora_ops,'P')	= 'O')
	and	exists	(select	1
			from	dmed_regra_origem_tit	w
			where	w.ie_origem_titulo	= o.ie_origem_titulo)
	and	 not exists (	select	1
				from	dmed_titulos_mensal x
				where	x.nr_documento = o.nr_titulo
				and 	x.nm_usuario <> 'Tasy2.2'
				and   trunc(x.dt_liquidacao, 'YEAR') = trunc(l.dt_recebimento, 'YEAR'))
	group by	c.cd_pessoa_fisica,
			b.nr_titulo,
			'Tasy2.2',
			l.dt_recebimento,
			l.nr_sequencia
	union
	select  c.cd_pessoa_fisica titular,
			l.vl_juros,
			l.vl_multa,
			l.dt_recebimento,
			o.nr_titulo,
			o.vl_titulo vl_titulo,
			sum(b.vl_debito) vl_recebido,
			max('Tasy2.3') ds_union,
			l.nr_sequencia nr_seq_baixa
	from  		negociacao_cr c,
			negociacao_cr_deb_cc b,
			titulo_receber o,
			titulo_receber_liq l
	where 		b.nr_seq_negociacao = c.nr_sequencia
	and 		b.nr_titulo = o.nr_titulo
	and 		l.nr_titulo = o.nr_titulo
	and 		c.cd_estabelecimento = cd_estabelecimento_p
	and 		l.dt_recebimento between dt_ref_inicial_w and dt_ref_final_w
	and 		c.cd_pessoa_fisica is not null
	and 	exists  (select 1
			from  dmed_regra_tipo_tit w
			where w.ie_tipo_receber   = l.cd_tipo_recebimento
			and nvl(w.ie_prestadora_ops,'P')  = 'O')
	and 	exists  (select 1
			from  dmed_regra_origem_tit w
			where w.ie_origem_titulo  = o.ie_origem_titulo)
	and	 not exists (	select	1
				from	dmed_titulos_mensal x
				where	x.nr_documento = o.nr_titulo)
	group by  	c.cd_pessoa_fisica,
			l.vl_juros,
			l.vl_multa,
			l.dt_recebimento,
			o.nr_titulo,
			l.nr_sequencia,
			o.vl_titulo
	order by 5;

vet01	c01%RowType;
	
cursor c02(nr_titulo_p number, nr_seq_baixa_p number) is
	select  sum(nvl(t.vl_baixa, 0)) vl_item,
			(	select	s.cd_pessoa_fisica
				from	pls_segurado	s
				where	s.nr_sequencia	= t.nr_seq_segurado ) cd_beneficiario
    from  	pls_negociacao_mens_item n,
			pls_titulo_rec_liq_neg t 
	where 	n.nr_sequencia = t.nr_seq_neg_mens_item
	and   	n.nr_seq_segurado = t.nr_seq_segurado
	and   	n.ie_tipo_item not in (23,24)
	and   	t.nr_seq_baixa = nr_seq_baixa_p
	and   	t.nr_titulo = nr_titulo_p
	group by t.nr_seq_segurado;

vet02 c02%RowType;

begin
dt_ref_inicial_w	:= trunc(dt_referencia_p, 'mm');
dt_ref_final_w		:= fim_dia(fim_mes(dt_referencia_p));

select 	nvl(max(ie_juros_multa), 'N')
into	ie_juros_multa_w
from	dmed_regra_geral;
	
open c01;
loop
fetch c01 into
	vet01;
exit when c01%notfound;
	begin
		cd_titular_w		:= vet01.titular;
		cd_beneficiario_w	:= vet01.titular;
		vl_juros_w			:= vet01.vl_juros;
		vl_multa_w			:= vet01.vl_multa;
		dt_liquidacao_w		:= vet01.dt_recebimento;
		vl_titulo_w			:= vet01.vl_titulo;
		vl_recebido_w		:= vet01.vl_recebido;
		nr_titulo_w			:= vet01.nr_titulo;
		
		
		if (ie_juros_multa_w = 'S') then
			vl_recebido_w := vl_recebido_w + vl_juros_w + vl_multa_w;
		else
			select	nvl(sum(t.vl_baixa), 0)
			into	vl_juros_multa_neg_w
			from	pls_negociacao_mens_item n,
					pls_titulo_rec_liq_neg t 
			where	n.nr_sequencia = t.nr_seq_neg_mens_item
			and		n.nr_seq_segurado = t.nr_seq_segurado
			and		n.ie_tipo_item in (23,24)
			and		t.nr_seq_baixa = vet01.nr_seq_baixa
			and 	t.nr_titulo = nr_titulo_w;
			
			if vl_juros_multa_neg_w > 0 then
				vl_recebido_w := vl_recebido_w - vl_juros_multa_neg_w;
			end if;	
		end if;
		
		open c02(nr_titulo_w, vet01.nr_seq_baixa);
			loop
				fetch c02 into	
					vet02;
				exit when c02%notfound;
					begin
						cd_beneficiario_w := vet02.cd_beneficiario;
						--vl_total_w	:= round((vl_recebido_w / vl_titulo_w) * vet02.vl_item, 2);
						vl_total_w := round(vet02.vl_item, 2);
						
						insert into dmed_titulos_mensal
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_dmed_mensal,
							nr_documento,
							ie_tipo_documento,
							cd_pessoa_titular,
							cd_pessoa_beneficiario,
							vl_pago,
							dt_liquidacao,
							ie_prestadora_ops)
						values	(dmed_titulos_mensal_seq.NextVal,
							sysdate,
							vet01.ds_union,
							sysdate,
							vet01.ds_union,
							nr_seq_dmed_p,
							nr_titulo_w,
							'1',
							cd_titular_w,
							cd_beneficiario_w,
							vl_total_w,
							dt_liquidacao_w,
							'O');	
					end;	
			end loop;
			if c02%rowcount = 0 then
				insert into dmed_titulos_mensal
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_dmed_mensal,
					nr_documento,
					ie_tipo_documento,
					cd_pessoa_titular,
					cd_pessoa_beneficiario,
					vl_pago,
					dt_liquidacao,
					ie_prestadora_ops)
				values	(dmed_titulos_mensal_seq.NextVal,
					sysdate,
					vet01.ds_union,
					sysdate,
					vet01.ds_union,
					nr_seq_dmed_p,
					nr_titulo_w,
					'1',
					cd_titular_w,
					cd_beneficiario_w,
					vl_recebido_w,
					dt_liquidacao_w,
					'O');
			end if;
		close C02;
	end;
end loop;
close C01;

commit;

end dmed_mensal_titulos_negociados;
/