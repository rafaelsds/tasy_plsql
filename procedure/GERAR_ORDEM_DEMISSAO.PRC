create or replace
procedure gerar_ordem_demissao(	nr_sequencia_p		in     	number,
				nm_usuario_p		in	varchar2,
				cd_pessoa_fisica_p	in	varchar2) is

nr_sequencia_w			man_ordem_servico.nr_sequencia%type;
cd_usu_log_w			usuario.cd_pessoa_fisica%type;
nr_seq_usu_ord_w			man_ordem_servico_exec.nr_sequencia%type;
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type; --pessoa demitida
nr_seq_ativ_exec_w		regra_envio_os_usu_pf_ativ.nr_seq_ativ_exec%type;
qt_hora_prev_w			number(5,0);
qt_min_prev_w			number(15,0);
ds_dano_breve_w			regra_envio_os_usu_pf.ds_dano_breve%type;
ds_dano_w			regra_envio_os_usu_pf.ds_dano%type;
sg_conselho_w			conselho_profissional.sg_conselho%type;
ds_codigo_prof_w			pessoa_fisica.ds_codigo_prof%type;
ds_cargo_w			varchar2(255);
ds_observacao_w			pessoa_fisica.ds_observacao%type;
dt_admissao_w		pessoa_fisica.dt_admissao_hosp%type;

cursor C01 is
	select	nr_seq_ativ_exec,
		qt_hora_prev,
		qt_min_prev
	from	regra_envio_os_usu_pf_ativ
	where	nr_seq_regra = nr_sequencia_p
	order by nr_sequencia;

begin

select	man_ordem_servico_seq.nextval
into	nr_sequencia_w
from	dual;

if	(nvl(cd_pessoa_fisica_p,'0') <> '0') then
	select	SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA), 0, 255) as nm_pessoa_fisica,
		substr(Obter_Conselho_Profissional(nr_seq_conselho,'S'),1,10),
		ds_codigo_prof,
		substr(obter_desc_cargo(cd_cargo),1,255) ds_cargo,
		ds_observacao,
		dt_admissao_hosp
	into	nm_pessoa_fisica_w,
		sg_conselho_w,
		ds_codigo_prof_w,
		ds_cargo_w,
		ds_observacao_w,
		dt_admissao_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
end if;

select	a.cd_pessoa_fisica
into	cd_usu_log_w
from	usuario a
where	a.nm_usuario = nm_usuario_p;

if	(nr_sequencia_p > 0) then
	select	ds_dano_breve,
		ds_dano
	into	ds_dano_breve_w,
		ds_dano_w
	from	regra_envio_os_usu_pf
	where   nr_sequencia = nr_sequencia_p;

	ds_dano_w 	:= substr(replace_macro(ds_dano_w,'@NOME',nm_pessoa_fisica_w),1,4000);
	ds_dano_w 	:= substr(replace_macro(ds_dano_w,'@CONSELHO',sg_conselho_w),1,4000);
	ds_dano_w 	:= substr(replace_macro(ds_dano_w,'@CODIGOPROF',ds_codigo_prof_w),1,4000);
	ds_dano_w  	:= substr(replace_macro(ds_dano_w,'@CARGO',ds_cargo_w),1,4000);
	ds_dano_w	:= substr(replace_macro(ds_dano_w,'@OBSPF',ds_observacao_w),1,4000);
	ds_dano_w	:= substr(replace_macro(ds_dano_w,'@DATAADMISSAO',dt_admissao_w),1,4000);

	ds_dano_breve_w	:= substr(replace_macro(ds_dano_breve_w,'@NOME',nm_pessoa_fisica_w),1,80);
	ds_dano_breve_w	:= substr(replace_macro(ds_dano_breve_w,'@CONSELHO',sg_conselho_w),1,80);
	ds_dano_breve_w 	:= substr(replace_macro(ds_dano_breve_w,'@CODIGOPROF',ds_codigo_prof_w),1,80);
	ds_dano_breve_w 	:= substr(replace_macro(ds_dano_breve_w,'@CARGO',ds_cargo_w),1,80);
	ds_dano_breve_w 	:= substr(replace_macro(ds_dano_breve_w,'@OBSPF',ds_observacao_w),1,80);
	ds_dano_breve_w	:= substr(replace_macro(ds_dano_breve_w,'@DATAADMISSAO',dt_admissao_w),1,80);
end if;


insert	into man_ordem_servico(
	nr_sequencia,
	nr_seq_localizacao,
	nr_seq_equipamento,
	cd_pessoa_solicitante,
	dt_ordem_servico,
	ie_prioridade,
	ie_parado,
	ds_dano_breve,
	dt_atualizacao,
	nm_usuario,
	dt_inicio_desejado,
	dt_conclusao_desejada,
	ds_dano,
	dt_inicio_previsto,
	dt_fim_previsto,
	dt_inicio_real,
	dt_fim_real,
	ie_tipo_ordem,
	ie_status_ordem,
	nr_grupo_planej,
	nr_grupo_trabalho,
	nr_seq_tipo_solucao,
	ds_solucao,
	nm_usuario_exec,
	qt_contador,
	nr_seq_planej,
	nr_seq_tipo_contador,
	nr_seq_estagio,
	cd_projeto,
	nr_seq_etapa_proj,
	dt_reabertura,
	cd_funcao,
	nm_tabela,
	ie_classificacao,
	nr_seq_origem,
	nr_seq_projeto,
	ie_grau_satisfacao,
	nr_seq_indicador,
	nr_seq_causa_dano,
	ie_forma_receb,
	nr_seq_cliente,
	nr_seq_grupo_des,
	nr_seq_grupo_sup,
	nr_seq_superior,
	ie_eficacia,
	dt_prev_eficacia,
	cd_pf_eficacia,
	nr_seq_nao_conform,
	nr_seq_complex,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_obriga_news,
	nr_seq_meta_pe,
	nr_seq_classif,
	nr_seq_nivel_valor,
	nm_usuario_lib_news,
	dt_libera_news,
	dt_envio_wheb,
	ds_contato_solicitante,
	ie_prioridade_desen,
	ie_prioridade_sup,
	nr_seq_proj_cron_etapa,
	nr_seq_tre_agenda,
	ie_origem_os,
	nr_seq_origem_dano)
select  nr_sequencia_w,				-- nr_sequencia,
	nr_seq_localizacao,
	nr_seq_equipamento,
	cd_usu_log_w,				-- cd_pessoa_solicitante,
	sysdate,					-- dt_ordem_servico,
	'A',					-- ie_prioridade,
	'N',					-- ie_parado,
	substr(ds_dano_breve_w,1,80),		-- ds_dano_breve,
	sysdate,					-- dt_atualizacao,
	nm_usuario_p,				-- nm_usuario,
	sysdate,					-- dt_inicio_desejado,
	(sysdate + 15),				-- dt_conclusao_desejada,
	substr(wheb_mensagem_pck.get_texto(303933,null) || ': ' || nm_pessoa_fisica_w || chr(13) || chr(10) || ds_dano_w,1,4000), -- ds_dano Funcionario: 
	sysdate,		                		-- dt_inicio_previsto,
	null,					-- dt_fim_previsto,
	sysdate,					-- dt_inicio_real,
	null,					-- dt_fim_real,
	1,					-- ie_tipo_ordem,
	1,					-- ie_status_ordem,
	nr_grupo_planej,				-- nr_grupo_planej,
	nr_grupo_trabalho,				-- nr_grupo_trabalho,
	null,					-- nr_seq_tipo_solucao,
	null,					-- ds_solucao,
	nm_usuario_exec, 				-- nm_usuario_exec,
	null,					-- qt_contador,
	null,					-- nr_seq_planej,
	null,					-- nr_seq_tipo_contador,
	nr_seq_estagio,				-- nr_seq_estagio,
	null,					-- cd_projeto,
	null,					-- nr_seq_etapa_proj,
	null,					-- dt_reabertura,
	null,			       	  	-- cd_funcao,
	null,					-- nm_tabela,
	ie_classificacao,				-- ie_classificacao,
	null,					-- nr_seq_origem,
	null,					-- nr_seq_projeto,
	null,					-- ie_grau_satisfacao,
	null,					-- nr_seq_indicador,
	null,					-- nr_seq_causa_dano,
	nvl(ie_forma_receb,'W'),			-- ie_forma_receb,
	null,					-- nr_seq_cliente,
	null,					-- nr_seq_grupo_des,
	null,					-- nr_seq_grupo_sup,
	null,					-- nr_seq_superior,
	null,					-- ie_eficacia,
	null,					-- dt_prev_eficacia,
	null,					-- cd_pf_eficacia,
	null,					-- nr_seq_nao_conform,
	null,					-- nr_seq_complex,
	null,					-- dt_atualizacao_nrec,
	nm_usuario_p,				-- nm_usuario_nrec,
	null,					-- ie_obriga_news,
	null,					-- nr_seq_meta_pe,
	null,					-- nr_seq_classif,
	null,					-- nr_seq_nivel_valor,
	null,					-- nm_usuario_lib_news,
	null,					-- dt_libera_news,
	null,					-- dt_envio_wheb,
	null,					-- ds_contato_solicitante,
	null,					-- ie_prioridade_desen,
	null,					-- ieed_prioridade_sup
	null,					-- nr_seq_proj_cron_etapa
	null,					--nr_seq_tre_agenda
	null,					--ie_origem_os
	nr_seq_origem_dano			--nr_seq_origem_dano
from	regra_envio_os_usu_pf
where   nr_sequencia = nr_sequencia_p;

select	man_ordem_servico_exec_seq.nextval
into	nr_seq_usu_ord_w
from 	dual;

insert 	into man_ordem_servico_exec( nr_sequencia,
				  nr_seq_ordem,
				  nr_seq_tipo_exec,
				  qt_min_prev,
				  nm_usuario_exec,
				  dt_atualizacao,
				  nm_usuario)
select				  nr_seq_usu_ord_w,
				  nr_sequencia_w,
				  nr_seq_tipo_exec,
				  qt_min_prev,
				  nm_usuario_exec,
				  sysdate,
				  nm_usuario_p
from 	regra_envio_os_usu_pf
where	nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01 into
	nr_seq_ativ_exec_w,
	qt_hora_prev_w,
	qt_min_prev_w;
exit when C01%notfound;
	begin
	qt_min_prev_w := (nvl(qt_min_prev_w,0) + nvl(qt_hora_prev_w,0)*60);
	
	if	(nvl(qt_min_prev_w,0) = 0) then
		select	nvl(max(qt_min_ativ),0)
		into	qt_min_prev_w
		from	des_ativ_exec
		where	nr_sequencia = nr_seq_ativ_exec_w;
	end if;
	
	insert into man_ordem_ativ_prev(
		nr_sequencia,
		nr_seq_ordem_serv,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		qt_min_prev,
		nr_seq_ativ_exec)
	values(	man_ordem_ativ_prev_seq.nextval,
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		qt_min_prev_w,
		nr_seq_ativ_exec_w);
	end;
end loop;
close c01;

commit;

end gerar_ordem_demissao;
/