create or replace
procedure ageint_consistir_regra_conv(cd_convenio_p		number,
			    nr_seq_item_p		number,
			    dt_agenda_p		date,
			    cd_estabelecimento_p	number,
			    nm_usuario_p		Varchar2,
			    ds_erro_p 		out varchar2) is 

qt_permitida_w		number(10,0);				    
qt_consulta_w		number(10,0);
ie_classif_agenda_w	varchar2(5);
nr_seq_regra_w		number(10,0);
ie_classif_agenda_regra_w 	varchar2(5);
cd_convenio_regra_w	number(10,0);
ie_forma_consistencia_w	varchar2(1);
qt_agendado_exame_w number(10,0);
ie_tipo_Agendamento_w	agenda_integrada_item.ie_tipo_Agendamento%TYPE;
				    
begin
select	max(ie_classif_agenda),
		max(ie_tipo_agendamento)
into	ie_classif_agenda_w,
		ie_tipo_Agendamento_w
from	agenda_integrada_item
where 	nr_sequencia = nr_seq_item_p; 

if	(ie_tipo_Agendamento_w	= 'C') then
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	ageint_regra_convenio
	where	nvl(cd_convenio,cd_convenio_p) = cd_convenio_p
	and	((ie_classif_agenda = ie_classif_agenda_w) or (ie_classif_agenda_w is null) or (ie_classif_agenda is null))
	and	cd_estabelecimento = cd_estabelecimento_p;

	select	max(qt_permitida),
		max(ie_classif_agenda),
		max(cd_convenio),
		nvl(max(ie_forma_consistencia),'M')
	into	qt_permitida_w,
		ie_classif_agenda_regra_w,
		cd_convenio_regra_w,
		ie_forma_consistencia_w
	from	ageint_regra_convenio
	where	nr_sequencia = nr_seq_regra_w;

	if	(ie_forma_consistencia_w = 'D') then
		select	count(*)
		into	qt_consulta_w
		from	agenda a,
			agenda_consulta b
		where	a.cd_agenda = b.cd_agenda
		and	a.cd_tipo_agenda = 3
		and	a.cd_estabelecimento = cd_estabelecimento_p
		and	dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 83699/86400
		and	((cd_convenio = cd_convenio_regra_w) or (cd_convenio_regra_w is null))
		and	((ie_classif_agenda = ie_classif_agenda_regra_w) or (ie_classif_agenda_regra_w is null))
		and	b.ie_status_agenda not in ('C','L','B','F','I');
	else
		select	count(*)
		into	qt_consulta_w
		from	agenda a,
			agenda_consulta b
		where	a.cd_agenda = b.cd_agenda
		and	a.cd_tipo_agenda = 3
		and	a.cd_estabelecimento = cd_estabelecimento_p
		and	dt_agenda between trunc(dt_agenda_p,'month') and last_day(trunc(dt_agenda_p)) + 83699/86400
		and	((cd_convenio = cd_convenio_regra_w) or (cd_convenio_regra_w is null))
		and	((ie_classif_agenda = ie_classif_agenda_regra_w) or (ie_classif_agenda_regra_w is null))
		and	b.ie_status_agenda not in ('C','L','B','F','I');
	end if;

	if	(qt_consulta_w >= qt_permitida_w) then
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279284,null);
	end if;	
end if;

end ageint_consistir_regra_conv;
/
