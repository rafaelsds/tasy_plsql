create or replace
procedure consistir_regra_wbarras_html5	(	nr_seq_regra_p		number,
										cd_material_p		number,
										qt_material_p		number,
										nr_id_p 			number,			
										ds_mensagem_p out	varchar2,
										ie_regra_p out		varchar2) is

ie_regra_valida_w				varchar2(1);
cd_grupo_material_w				number(3,0);
cd_subgrupo_material_w			number(3,0);
cd_classe_material_w			number(5,0);
ie_regra_w						varchar2(1) 	:= null;
ds_mensagem_w					varchar2(255)	:= null;
qt_material_w       			number(3);
qt_material_regra_w				number(3);
cd_grupo_material_cursor_w		number(3,0);
cd_subgrupo_material_cursor_w	number(3,0); 
cd_classe_material_cursor_w		number(5,0);
cd_material_cursor_w		    number(6,0);
qt_material_tabela_w            number(17,5);

cursor C03 is
	select	qt_material,
		ie_regra
	from	item_adic_cod_barra
	where	nr_seq_regra = nr_seq_regra_p
	order by qt_material,
		nvl(cd_grupo_material,'99999999999'),
		nvl(cd_subgrupo_material,'99999999999'),
		nvl(cd_classe_material,'99999999999'),
		nvl(cd_material,'99999999999');

cursor c02 is
select	ie_regra,
        cd_grupo_material, 
        cd_subgrupo_material, 
        cd_classe_material, 
        cd_material
from	item_adic_cod_barra
where	nr_seq_regra	= nr_seq_regra_p
and     qt_material     is not null
order by
	nvl(cd_grupo_material,'99999999999'),
	nvl(cd_subgrupo_material,'99999999999'),
	nvl(cd_classe_material,'99999999999'),
	nvl(cd_material,'99999999999');

cursor c01 is
select	ie_regra
from	item_adic_cod_barra
where	nr_seq_regra							= nr_seq_regra_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(cd_material, cd_material_p)				= cd_material_p
and qt_material     is null
and	ie_regra <> 'N'
order by
	nr_sequencia;

begin
if	(nr_seq_regra_p is not null) and
	(cd_material_p is not null) then
    
    insert into material_nrec_wbarras ( nr_id,
                                        cd_material,
                                        qt_material,
                                        ds_material) 
                               values (nr_id_p,
                                        cd_material_p,
                                        qt_material_p,
                                        substr(obter_desc_material(cd_material_p),1,255));
    
	/* obter se existe regra e regra ativa*/
	select	decode(count(*),0,'N','S')
	into	ie_regra_valida_w
	from	regra_item_adic_cod_barra
	where	nr_sequencia	= nr_seq_regra_p
	and	ie_situacao	= 'A';

	if	(ie_regra_valida_w = 'S') then
	
		/* obter estrutura material */
		select	max(cd_grupo_material),
			max(cd_subgrupo_material),
			max(cd_classe_material)
		into	cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w
		from	estrutura_material_v
		where	cd_material = cd_material_p;
		
		open C03;
		loop
		fetch C03 into	
			qt_material_regra_w,
			ie_regra_w;
		exit when C03%notfound;
			begin
			if	(qt_material_regra_w is not null) then
				/* consistir regra qunatidade*/
				open c02;
				loop
				fetch c02 into 
					ie_regra_w,
					cd_grupo_material_cursor_w,		
					cd_subgrupo_material_cursor_w,
					cd_classe_material_cursor_w,		
					cd_material_cursor_w;
				exit when c02%notfound;
					begin
				
					select  sum(qt_material)
					into    qt_material_tabela_w
					from    material_nrec_wbarras
					where   obter_dados_material(cd_material_p,'CGRU')	= nvl(cd_grupo_material_cursor_w,obter_dados_material(cd_material_p,'CGRU'))
					and	    obter_dados_material(cd_material_p,'CSUB')	= nvl(cd_subgrupo_material_cursor_w,obter_dados_material(cd_material_p,'CSUB'))
					and	    obter_dados_material(cd_material_p,'CCLA')	= nvl(cd_classe_material_cursor_w,obter_dados_material(cd_material_p,'CCLA'))
					and	    cd_material 								= nvl(cd_material_p, cd_material)
					and     nr_ID = nr_id_p;
					
					select  max(qt_material),
							max(ie_regra)
					into    qt_material_w,
							ie_regra_w
					from	item_adic_cod_barra
					where	nr_seq_regra	= nr_seq_regra_p
					and     qt_material     is not null
					and	    nvl(cd_grupo_material,cd_grupo_material_w)                = cd_grupo_material_w
					and	    nvl(cd_subgrupo_material , cd_subgrupo_material_w)        = cd_subgrupo_material_w
					and	    nvl(cd_classe_material,cd_classe_material_w)              = cd_classe_material_w
					and	    nvl(cd_material,cd_material_p) 		                      = cd_material_p; 
					
					if	(qt_material_w > 0) then
						if	(ie_regra_w = 'N')then
							ie_regra_w := 'N';
							rollback;
							goto Sair;
						elsif (qt_material_tabela_w > qt_material_w) then
							rollback;
							ie_regra_w := 'N';
							goto Sair;
						else
							goto Sair;	
						end if;
					else 
						ie_regra_w := 'S';
					end if;
					end;
				end loop;
				close c02;
				
			else
			
				ie_regra_w := 'N';
				
				/* consistir regra */
				open c01;
				loop
				fetch c01 into 
					ie_regra_w;
				exit when c01%notfound;
					begin
					ie_regra_w := ie_regra_w;
					
					if	(ie_regra_w in ('S','Q')) then
						goto Sair;
					elsif	(ie_regra_w = 'N') then
						rollback;
						goto Sair;
					end if;
					end;
				end loop;
				close c01;
				
			end if;			
			end;
		end loop;
		close C03;

        <<Sair>>
		/* obter mensagem regra */
		if	(ie_regra_w = 'N') or
			(ie_regra_w is null) then
            rollback;
			select	max(ds_mensagem)
			into	ds_mensagem_w
			from	regra_item_adic_cod_barra
			where	nr_sequencia = nr_seq_regra_p;

		elsif	(ie_regra_w = 'Q') then
			ie_regra_p	:= 'Q';
		else
			ds_mensagem_w := null;

		end if;

	end if;	

end if;

ds_mensagem_p := ds_mensagem_w;

commit;

end consistir_regra_wbarras_html5;
/
