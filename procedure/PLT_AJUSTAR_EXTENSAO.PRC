create or replace
procedure PLT_ajustar_extensao(		nr_prescricao_p		number,
					nr_sequencia_p		number,
					nm_tabela_p		varchar2,
					nm_usuario_p		Varchar2) is 

cd_pessoa_fisica_w		varchar2(10);
cd_item_w			varchar2(255);
ie_origem_proced_w		number(10);
nr_prescricao_original_w	number(14);
nr_seq_anterior_w		number(16);
nr_prescr_w			number(14);
nr_seq_w			number(16);
dt_extensao_w			date;
ie_agrupador_w			number(2);
nr_atendimento_w		number(10);
ie_tipo_item_w			varchar2(15);

cursor c01 is
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_material a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_material a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c02 is
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_dieta a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_dieta a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c03 is
select	a.nr_sequencia
from	rep_jejum a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_sequencia
from	rep_jejum a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c04 is
select	a.nr_sequencia
from	nut_pac a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_sequencia
from	nut_pac a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c05 is
select	a.nr_prescricao,
	a.nr_seq_solucao
from	prescr_solucao a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_prescricao,
	a.nr_seq_solucao
from	prescr_solucao a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c06 is
select	a.nr_sequencia
from	prescr_gasoterapia a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_sequencia
from	prescr_gasoterapia a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c07 is
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_procedimento a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_procedimento a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

cursor c08 is
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_recomendacao a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.nr_atendimento		= nr_atendimento_w
union all
select	a.nr_prescricao,
	a.nr_sequencia
from	prescr_recomendacao a,
	prescr_medica b
where	a.nr_prescricao			= b.nr_prescricao
and	a.nr_prescricao_original	= nr_prescricao_original_w
and	a.nr_seq_anterior		= nr_seq_anterior_w
and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
and	b.nr_atendimento		is null
and	nr_atendimento_w		is null;

begin

nr_prescricao_original_w	:= PLT_obter_item_orig(nr_prescricao_p, nr_sequencia_p, upper(nm_tabela_p),'P');
nr_seq_anterior_w		:= PLT_obter_item_orig(nr_prescricao_p, nr_sequencia_p, upper(nm_tabela_p),'S');

select	max(cd_pessoa_fisica),
	max(nr_atendimento)
into	cd_pessoa_fisica_w,
	nr_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_original_w;	

if	(upper(nm_tabela_p) = 'PRESCR_MATERIAL') then


	if	(nr_atendimento_w	is not null) then
	
		select	max(b.dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_material a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_material a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	end if;

	if	(nr_prescricao_original_w	is not null) and
		(nr_seq_anterior_w		is not null) then
	
		update	prescr_material
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_prescricao	= nr_prescricao_original_w
		and	nr_sequencia	= nr_seq_anterior_w;

		open C01;
		loop
		fetch C01 into	
			nr_prescr_w,
			nr_seq_w;
		exit when C01%notfound;
			begin
			
			update	prescr_material
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_prescricao	= nr_prescr_w
			and	nr_sequencia	= nr_seq_w;			
			
			end;
		end loop;
		close C01;
	
	end if;
		
	select	max(ie_agrupador)	
	into	ie_agrupador_w
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_p;
		
	if	(ie_agrupador_w = 1) then
		ie_tipo_item_w	:= 'M';
	elsif	(ie_agrupador_w	= 2) then
		ie_tipo_item_w	:= 'MAT';
	elsif	(ie_agrupador_w = 8) then
		ie_tipo_item_w	:= 'SNE';
	elsif	(ie_agrupador_w = 4) then
		ie_tipo_item_w	:= 'SOL';
	elsif	(ie_agrupador_w = 12) then
		ie_tipo_item_w	:= 'S';
	elsif	(ie_agrupador_w = 16) then
		ie_tipo_item_w	:= 'LD';		
	end if;
	
	Atualizar_plt_controle(null, null, cd_pessoa_fisica_w, ie_tipo_item_w, 'S', nr_prescricao_p);
		
elsif	(upper(nm_tabela_p) = 'PRESCR_DIETA') then

	if	(nr_atendimento_w	is not null) then
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_dieta a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_dieta a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;	
	
	end if;

	if	(nr_prescricao_original_w	is not null) and
		(nr_seq_anterior_w		is not null) then
	
		update	prescr_dieta
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_prescricao	= nr_prescricao_original_w
		and	nr_sequencia	= nr_seq_anterior_w;

		open C02;
		loop
		fetch C02 into	
			nr_prescr_w,
			nr_seq_w;
		exit when C02%notfound;
			begin
			
			update	prescr_dieta
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_prescricao	= nr_prescr_w
			and	nr_sequencia	= nr_seq_w;			
			
			end;
		end loop;
		close C02;
	
	end if;
	
elsif	(upper(nm_tabela_p) = 'REP_JEJUM') then

	if	(nr_atendimento_w	is not null) then
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	rep_jejum a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	b.dt_suspensao is null;
	
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w	
		from	rep_jejum a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	b.dt_suspensao is null;
		
	end if;

	if	(nr_seq_anterior_w		is not null) then
	
		update	rep_jejum
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_anterior_w;

		open C03;
		loop
		fetch C03 into	
			nr_seq_w;
		exit when C03%notfound;
			begin
			
			update	rep_jejum
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_w;			

			end;
		end loop;
		close C03;		

	end if;
	
elsif	(upper(nm_tabela_p) = 'NUT_PAC') then

	if	(nr_atendimento_w	is not null) then

		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	nut_pac a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	else

		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	nut_pac a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;

	end if;

	if	(nr_seq_anterior_w		is not null) then
	
		update	nut_pac
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_anterior_w;

		open C04;
		loop
		fetch C04 into	
			nr_seq_w;
		exit when C04%notfound;
			begin
			
			update	nut_pac
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_w;			

			end;
		end loop;
		close C04;
	
	end if;

elsif	(upper(nm_tabela_p) = 'PRESCR_SOLUCAO') then

	if	(nr_atendimento_w	is not null) then
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_solucao a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
		
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_solucao a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	end if;

	if	(nr_prescricao_original_w	is not null) and
		(nr_seq_anterior_w		is not null) then
	
		update	prescr_solucao
		set	dt_extensao	= dt_extensao_w
		where	nr_prescricao	= nr_prescricao_original_w
		and	nr_seq_solucao	= nr_seq_anterior_w;

		open C05;
		loop
		fetch C05 into	
			nr_prescr_w,
			nr_seq_w;
		exit when C05%notfound;
			begin
			
			update	prescr_solucao
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_prescricao	= nr_prescr_w
			and	nr_seq_solucao	= nr_seq_w;

			end;
		end loop;
		close C05;		
	
	end if;

elsif	(upper(nm_tabela_p) = 'PRESCR_GASOTERAPIA') then

	if	(nr_atendimento_w	is not null) then

		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_gasoterapia a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
		
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w
		from	prescr_gasoterapia a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	end if;

	if	(nr_seq_anterior_w		is not null) then
	
		update	prescr_gasoterapia
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_anterior_w;

		open C06;
		loop
		fetch C06 into	
			nr_seq_w;
		exit when C06%notfound;
			begin
			
			update	prescr_gasoterapia
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_w;
			
			end;
		end loop;
		close C06;
	
	end if;
	
elsif	(upper(nm_tabela_p) = 'PRESCR_PROCEDIMENTO') then

	if	(nr_atendimento_w	is not null) then
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w	
		from	prescr_procedimento a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
		
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w	
		from	prescr_procedimento a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;

	end if;

	if	(nr_prescricao_original_w	is not null) and
		(nr_seq_anterior_w		is not null) then
	
		update	prescr_procedimento
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_prescricao	= nr_prescricao_original_w
		and	nr_sequencia	= nr_seq_anterior_w;

		open C07;
		loop
		fetch C07 into	
			nr_prescr_w,
			nr_seq_w;
		exit when C07%notfound;
			begin
			
			update	prescr_procedimento
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_prescricao	= nr_prescr_w
			and	nr_sequencia	= nr_seq_w;
			
			end;
		end loop;
		close C07;		

	end if;	

elsif	(upper(nm_tabela_p) = 'PRESCR_RECOMENDACAO') then

	if	(nr_atendimento_w	is not null) then

		select	max(dt_validade_prescr)
		into	dt_extensao_w	
		from	prescr_recomendacao a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.nr_atendimento		= nr_atendimento_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
		
	else
	
		select	max(dt_validade_prescr)
		into	dt_extensao_w	
		from	prescr_recomendacao a,
			prescr_medica b
		where	a.nr_prescricao			= b.nr_prescricao
		and	a.nr_prescricao_original	= nr_prescricao_original_w
		and	a.nr_seq_anterior		= nr_seq_anterior_w
		and	b.cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	nvl(a.ie_suspenso, 'N')		= 'N'
		and	a.dt_suspensao is null
		and	b.dt_suspensao is null;
	
	end if;

	if	(nr_prescricao_original_w	is not null) and
		(nr_seq_anterior_w		is not null) then
	
		update	prescr_recomendacao
		set	dt_extensao	= dt_extensao_w,
			nm_usuario	= nm_usuario_p
		where	nr_prescricao	= nr_prescricao_original_w
		and	nr_sequencia	= nr_seq_anterior_w;

		open C08;
		loop
		fetch C08 into	
			nr_prescr_w,
			nr_seq_w;
		exit when C08%notfound;
			begin

			update	prescr_recomendacao
			set	dt_extensao	= dt_extensao_w,
				nm_usuario	= nm_usuario_p
			where	nr_prescricao	= nr_prescr_w
			and	nr_sequencia	= nr_seq_w;
			
			end;
		end loop;
		close C08;				
	
	end if;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end PLT_ajustar_extensao;
/
