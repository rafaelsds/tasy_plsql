create or replace
procedure consiste_tipo_acomodacao(	nr_atendimento_p	in number,
					ds_consistencia_p	out varchar2 )
					is


cd_nivel_acomodacao_wW	number(2,0);
cd_nivel_acomodacao_w	number(2,0);

begin

select	nvl(max(a.cd_nivel_acomodacao),99)
into	cd_nivel_acomodacao_w
from	tipo_acomodacao a,
	atend_categoria_convenio b
where	a.cd_tipo_acomodacao 	= b.cd_tipo_acomodacao
and	b.nr_seq_interno	= obter_atecaco_atendimento(nr_atendimento_p);

select	nvl(max(a.cd_nivel_acomodacao),99)
into	cd_nivel_acomodacao_wW
from	tipo_acomodacao a,
	atend_paciente_unidade b
where	a.cd_tipo_acomodacao 	= b.cd_tipo_acomodacao
and	b.nr_seq_interno	= obter_atepacu_paciente(nr_atendimento_p,'A');

if	(cd_nivel_acomodacao_w > cd_nivel_acomodacao_ww) then
	--ds_consistencia_p := 'O n�vel do tipo de acomoda��o do conv�nio � superior a de que o paciente esta. Deseja bloquear os demais leitos da unidade?';
	ds_consistencia_p := WHEB_MENSAGEM_PCK.get_texto(387377);
end if;

end consiste_tipo_acomodacao;
/
