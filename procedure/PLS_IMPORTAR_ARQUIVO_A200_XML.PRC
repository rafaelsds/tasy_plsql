create or replace
procedure pls_importar_arquivo_a200_xml
			(	nr_seq_intercambio_p	ptu_intercambio.nr_sequencia%type,
				nm_arquivo_p		varchar2,
				nm_usuario_p		varchar2) is

nr_seq_retorno_mov_w	ptu_retorno_movimentacao.nr_sequencia%type;
ds_ptu_w		varchar2(4);
dt_geracao_w		date;
cd_uni_destino_w	varchar2(255);
cd_uni_origem_w		varchar2(255);
ie_tipo_mov_w		ptu_intercambio.ie_tipo_mov%type;
dt_mov_inicio_w		ptu_intercambio.dt_mov_inicio%type;
dt_mov_fim_w		ptu_intercambio.dt_mov_fim%type;
nr_versao_transacao_w	varchar2(255);
nr_protocolo_w		varchar2(255);
nm_arquivo_w		varchar2(255);

identificacao_benef_w	varchar2(2000);
identificacao_benef_comp_w varchar2(2000);
cd_inconsistencia_w	varchar2(255);
cd_unimed_w		varchar2(255);
cd_usuario_plano_w	varchar2(255);
cd_unimed_comp_w	varchar2(255);
cd_usuario_plano_comp_w	varchar2(255);
nr_via_carteira_w	varchar2(255);
dt_validade_carteira_w	date;
nr_seq_benef_intercambio_w	ptu_intercambio_benef.nr_sequencia%type;
nr_seq_benef_simp_w		ptu_intercambio_benef_simp.nr_sequencia%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;

Cursor C01 is
	select	ds_conteudo,
		ie_tipo_registro
	from	w_importar_xml_a100
	where	nm_usuario = nm_usuario_p
	order by 2;

cursor c02 (	cd_unimed_cad_pc	varchar2,
		nr_seq_segurado_pc	pls_segurado.nr_sequencia%type) is
	select	a.nr_sequencia
	from	pls_segurado_repasse a,
		pls_congenere b
	where	b.nr_sequencia = a.nr_seq_congenere
	and	a.nr_seq_segurado = nr_seq_segurado_pc
	and	lpad(b.cd_cooperativa,4,'0') = cd_unimed_cad_pc
	and	a.nr_cartao_intercambio is null;
	
begin

for r_c01_w in C01 loop
	begin
	if	(instr(r_c01_w.ds_conteudo,'ptu:') > 0 ) then
		ds_ptu_w := 'ptu:';
	else
		ds_ptu_w := '';
	end if;
	
	if	(r_c01_w.ie_tipo_registro = '1') then
		dt_geracao_w		:= to_date(pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'dt_geracao>'),'YYYYmmdd');
		cd_uni_destino_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'cd_Uni_Destino>');
		cd_uni_origem_w		:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'cd_Uni_Origem>');
		nr_versao_transacao_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'nrVerTra_PTU>');
		nr_protocolo_w		:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'nr_protocolo>');
		nm_arquivo_w		:= obter_nome_arquivo(nm_arquivo_p, 'N');
		
		select	ie_tipo_mov,
			dt_mov_inicio,
			dt_mov_fim
		into	ie_tipo_mov_w,
			dt_mov_inicio_w,
			dt_mov_fim_w
		from	ptu_intercambio
		where	nr_sequencia	= nr_seq_intercambio_p;
		
		insert	into	ptu_retorno_movimentacao
			(	nr_sequencia, dt_geracao, ie_tipo_mov,
				dt_mov_inicio, dt_mov_fim, ie_operacao,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
				nm_usuario_nrec, nr_versao_transacao, nr_seq_intercambio,
				cd_unimed_origem, cd_unimed_destino, ie_tipo_retorno)
			values (ptu_retorno_movimentacao_seq.nextval, dt_geracao_w, ie_tipo_mov_w,
				dt_mov_inicio_w, dt_mov_fim_w, 'E',
				sysdate, nm_usuario_p, sysdate,
				nm_usuario_p, nr_versao_transacao_w, nr_seq_intercambio_p,
				cd_uni_origem_w, cd_uni_destino_w, 'A100')
			returning nr_sequencia into nr_seq_retorno_mov_w;
	elsif	(r_c01_w.ie_tipo_registro = '2') then
		cd_inconsistencia_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'cd_mens_retorno>');
		
		identificacao_benef_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'identificacaoBenef>');
		cd_unimed_w		:= pls_extrair_dado_tag_xml(identificacao_benef_w,'<'||ds_ptu_w||'cd_Unimed>');
		cd_usuario_plano_w	:= pls_extrair_dado_tag_xml(identificacao_benef_w,'<'||ds_ptu_w||'id_Benef>');
		
		identificacao_benef_comp_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_conteudo,'<'||ds_ptu_w||'identificacaoBenef_compRisco>');
		cd_unimed_comp_w		:= pls_extrair_dado_tag_xml(identificacao_benef_comp_w,'<'||ds_ptu_w||'cd_Unimed>');
		cd_usuario_plano_comp_w		:= pls_extrair_dado_tag_xml(identificacao_benef_comp_w,'<'||ds_ptu_w||'id_Benef>');
		nr_via_carteira_w		:= pls_extrair_dado_tag_xml(identificacao_benef_comp_w,'<'||ds_ptu_w||'via_cartao>');
		dt_validade_carteira_w		:= to_date(pls_extrair_dado_tag_xml(identificacao_benef_comp_w,'<'||ds_ptu_w||'dt_val_carteira>'),'YYYYmmdd');
		
		select	max(a.nr_sequencia)
		into	nr_seq_benef_intercambio_w
		from	ptu_intercambio			c,
			ptu_intercambio_empresa		b,
			ptu_intercambio_benef		a
		where	a.nr_seq_empresa		= b.nr_sequencia
		and	b.nr_seq_intercambio		= c.nr_sequencia
		and	c.nr_sequencia			= nr_seq_intercambio_p
		and	a.cd_usuario_plano		= cd_usuario_plano_w;
		
		select	max(nr_sequencia)
		into	nr_seq_benef_simp_w
		from	ptu_intercambio_benef_simp
		where	nr_seq_intercambio	= nr_seq_intercambio_p
		and	cd_beneficiario		= cd_usuario_plano_w;
		
		insert	into	ptu_retorno_mov_benef
			(	nr_sequencia, nr_seq_retorno_mov, cd_usuario_plano,
				cd_inconsistencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, cd_unimed,
				nr_via_carteira, dt_validade_carteira, cd_unimed_cad,
				cd_usuario_plano_cad, nr_seq_benef_intercambio, nr_seq_benef_simp)
			values(	ptu_retorno_mov_benef_seq.nextval, nr_seq_retorno_mov_w, cd_usuario_plano_w,
				cd_inconsistencia_w, sysdate, nm_usuario_p,
				sysdate, nm_usuario_p, cd_unimed_w,
				nr_via_carteira_w, dt_validade_carteira_w, cd_unimed_comp_w,
				cd_usuario_plano_comp_w, nr_seq_benef_intercambio_w, nr_seq_benef_simp_w);
		
		--Se foi incluido, gravar o cartao de intercambio
		if	(cd_inconsistencia_w = '3201') and
			(cd_usuario_plano_comp_w is not null) then
			if	(nr_seq_benef_intercambio_w is not null) then
				select	max(nr_seq_segurado)
				into	nr_seq_segurado_w
				from	ptu_intercambio_benef
				where	nr_sequencia	= nr_seq_benef_intercambio_w;
			elsif	(nr_seq_benef_simp_w is not null) then
				select	max(nr_seq_segurado)
				into	nr_seq_segurado_w
				from	ptu_intercambio_benef_simp
				where	nr_sequencia	= nr_seq_benef_simp_w;
			end if;
			
			if	(nr_seq_segurado_w is not null) then
				update	pls_segurado_carteira
				set	nr_cartao_intercambio	= lpad(cd_unimed_comp_w,4,'0')||lpad(cd_usuario_plano_comp_w,13,'0'),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_seq_segurado		= nr_seq_segurado_w;
				
				for r_c02_w in c02 (	cd_unimed_comp_w,
							nr_seq_segurado_w) loop
					begin
					update	pls_segurado_repasse
					set	nr_cartao_intercambio = lpad(cd_unimed_comp_w,4,'0')||lpad(cd_usuario_plano_comp_w,13,'0'),
						dt_atualizacao = sysdate,
						nm_usuario = nm_usuario_p
					where	nr_sequencia = r_c02_w.nr_sequencia;
					end;
				end loop;
			end if;
		end if;
	end if;
	
	end;
end loop;

update	ptu_intercambio
set	dt_importacao_retorno	= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_intercambio_p;

commit;

end pls_importar_arquivo_a200_xml;
/
