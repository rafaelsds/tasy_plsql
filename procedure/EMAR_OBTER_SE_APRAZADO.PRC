create or replace
function emar_obter_se_aprazado	(	nr_sequencia_p	number ) return varchar2 is

ie_aprazado_w	varchar2(1) := 'N';

begin

select	nvl(max('S'),'N')
into	ie_aprazado_w
from 	prescr_solucao_evento
where 	nr_sequencia = nr_sequencia_p
and 	ie_alteracao = 16
and 	dt_aprazamento is not null;

return ie_aprazado_w;

end;
/