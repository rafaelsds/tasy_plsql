create or replace
procedure Consistir_agend_espec_dif(cd_pessoa_fisica_p		varchar2,
				     cd_agenda_p		number,
				     dt_agenda_p		date,
				     nr_min_duracao_p		number,
				     ie_permite_p        	out varchar2) is

cd_especialidade_w	number(5,0);
qt_agendamento_w	number(10,0);
				     
begin

select	nvl(max(cd_especialidade),0)
into	cd_especialidade_w
from	agenda
where	cd_agenda = cd_agenda_p;

select	count(*)
into	qt_agendamento_w
from	agenda_consulta b,
		agenda a
where	a.cd_agenda		= b.cd_agenda
and		a.cd_tipo_agenda	= 3
and		b.cd_pessoa_fisica	= cd_pessoa_fisica_p
and		((b.dt_agenda between dt_agenda_p and dt_agenda_p + (nr_min_duracao_p - 1) / 1440)
		or (dt_agenda_p between b.dt_agenda and b.dt_agenda + (b.nr_minuto_duracao - 1) / 1440))
and		b.ie_status_agenda	not in ('C','F','I')
and		(obter_especialidade_agenda(a.cd_agenda) <> cd_especialidade_w);

if	(qt_agendamento_w >= 1) then
	ie_permite_p := 'N';
else
	ie_permite_p := 'S';
end if;

commit;

end Consistir_agend_espec_dif;
/