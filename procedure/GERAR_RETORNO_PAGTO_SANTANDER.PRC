create or replace procedure GERAR_RETORNO_PAGTO_SANTANDER(	nr_seq_banco_escrit_p	number,
				nm_usuario_p		varchar2) is

/*
Layout CNAB 240 - PgFor Trib Conc SET-2009_3.pdf

*/

ds_dt_liquidacao_w		varchar2(255);
ds_vl_liquidacao_w		varchar2(255);
ds_vl_desconto_w		varchar2(255);
ds_vl_juros_w			varchar2(255);
ds_vl_multa_w			varchar2(255);
cd_arquivo_w			varchar2(255);

cd_retorno_liq_w			varchar2(50);

nr_titulo_w			number(10,0);
nr_nosso_numero_w		titulo_pagar.nr_nosso_numero%type;
cd_tipo_baixa_w			number(10,0);
nr_seq_trans_escrit_w		number(10,0);
nr_seq_conta_banco_w		number(10,0);
nr_sequencia_w			number(10,0);
dt_liquidacao_w			date;
vl_liquidacao_w			number(15,2);
vl_desconto_w			number(15,2);
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_juros_ret_w			number(15,2);
vl_multa_ret_w			number(15,2);
vl_desconto_ret_w		number(15,2);
qt_reg_w			number(10);
cd_estabelecimento_w		number(10,0);
vl_escritural_w			number(15,2);
cd_tipo_baixa_padrao_w		parametros_contas_pagar.cd_tipo_baixa_padrao%type;

cd_ocorrencia1_w		varchar2(255);
cd_ocorrencia2_w		varchar2(255);
cd_ocorrencia3_w		varchar2(255);
cd_ocorrencia4_w		varchar2(255);
cd_ocorrencia5_w		varchar2(255);
cd_banco_w			number(10);
ie_estornar_baixa_w		varchar2(10);
nr_seq_baixa_w			number(10);

dt_remessa_retorno_w		date;
ds_nr_titulo_w			varchar2(20);
nr_seq_trans_fin_baixa_w	titulo_pagar.nr_seq_trans_fin_baixa%type;
cd_ocorrencia_ret_banco_w	erro_escritural.cd_erro%type;
ie_movto_bco_pag_escrit_w	parametros_contas_pagar.ie_movto_bco_pag_escrit%type;
nr_pagamento_w			titulo_pagar_escrit.nr_pagamento%type;

ie_liquidacao_w			varchar2(1);
qt_baixa_w		        number(10);

cursor c01 is
select	substr(ds_string,74,20) nr_titulo,
	substr(ds_string,94,8)			ds_dt_liquidacao,
	substr(ds_string,120,15)		ds_vl_liquidacao,
	''    					ds_vl_desconto,
	''    					ds_vl_juros,
	''    					ds_vl_multa,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5,
	substr(ds_string,135,20)		nr_pagamento
from	w_retorno_banco
where	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'A'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p
union
select	substr(ds_string,18,20) nr_titulo,
	substr(ds_string,88,8)			ds_dt_liquidacao,
	substr(ds_string,96,15)			ds_vl_liquidacao,
	''					ds_vl_desconto,
	substr(ds_string,175,15)		ds_vl_juros,
	substr(ds_string,190,15)		ds_vl_multa,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5,
	substr(ds_string,38,20)			nr_pagamento
from	w_retorno_banco
where	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'N'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p
union
select	substr(ds_string,183,10) nr_titulo,
	substr(ds_string,145,8)			ds_dt_liquidacao,
	substr(ds_string,153,15)		ds_vl_liquidacao,
	substr(ds_string,115,15)    		ds_vl_desconto,
	substr(ds_string,130,15)		ds_vl_juros, -- Segundo o layout do banco, juros e multa sao somados em um unico campo
	''    					ds_vl_multa,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5,
	substr(ds_string,203,20)		nr_pagamento
from	w_retorno_banco
where	substr(ds_string,18,2)	<> '52'
and	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'J'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p
union
/*Segmento O - Pagamento de Contas e tributos com codigo de barras  Implementando em 25/04/2013 */
select	substr(ds_string,123,20) nr_titulo,
	substr(ds_string,100,8)			ds_dt_liquidacao,
	substr(ds_string,108,15)		ds_vl_liquidacao,
	''    					ds_vl_desconto,
	''    					ds_vl_juros,
	''    					ds_vl_multa,
	substr(ds_string,231,2)			cd_ocorrencia1,
	substr(ds_string,233,2)			cd_ocorrencia2,
	substr(ds_string,235,2)			cd_ocorrencia3,
	substr(ds_string,237,2)			cd_ocorrencia4,
	substr(ds_string,239,2)			cd_ocorrencia5,
	substr(ds_string,143,20)		nr_pagamento
from	w_retorno_banco
where	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'O'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

begin

begin
select	b.cd_retorno_liq,
	a.cd_estabelecimento,
	a.nr_seq_conta_banco,
	a.cd_banco,
	a.dt_remessa_retorno
into	cd_retorno_liq_w,
	cd_estabelecimento_w,
	nr_seq_conta_banco_w,
	cd_banco_w,
	dt_remessa_retorno_w
from	banco_retorno_cp b,
	banco_escritural a
where	a.cd_banco		= b.cd_banco
and	a.nr_sequencia		= nr_seq_banco_escrit_p;
exception
	when no_data_found then
	/* Nao foi encontrado o codigo de retorno da liquidacao!
	Verifique o cadastro de "Retorno CP" no cadastro de bancos. */
	wheb_mensagem_pck.exibir_mensagem_abort(192789);
end;

select	max(nr_seq_trans_escrit)
into	nr_seq_trans_escrit_w
from	parametro_tesouraria
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(max(cd_tipo_baixa_padrao), 1),
	max(ie_movto_bco_pag_escrit)
into	cd_tipo_baixa_padrao_w,
	ie_movto_bco_pag_escrit_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

select	substr(ds_string,143,1)
into	cd_arquivo_w
from 	w_retorno_banco
where	substr(ds_string,8,1) = '0'
and	substr(ds_string,4,4) = '0000'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

if	(cd_arquivo_w	= '2')	then

	open c01;
	loop
	fetch c01 into
		ds_nr_titulo_w,
		ds_dt_liquidacao_w,
		ds_vl_liquidacao_w,
		ds_vl_desconto_w,
		ds_vl_juros_w,
		ds_vl_multa_w,
		cd_ocorrencia1_w,
		cd_ocorrencia2_w,
		cd_ocorrencia3_w,
		cd_ocorrencia4_w,
		cd_ocorrencia5_w,
		nr_pagamento_w;
	exit when c01%notfound;


		nr_titulo_w		:= substr(somente_numero(ds_nr_titulo_w),1,10);
		dt_liquidacao_w		:= to_date(ds_dt_liquidacao_w,'ddMMrrrr');
		vl_liquidacao_w		:= somente_numero(ds_vl_liquidacao_w);
		vl_liquidacao_w		:= nvl(dividir_sem_round(vl_liquidacao_w,100),0);
		vl_desconto_ret_w	:= somente_numero(ds_vl_desconto_w);
		vl_desconto_ret_w	:= nvl(dividir_sem_round(vl_desconto_ret_w,100),0);		
		vl_juros_ret_w		:= somente_numero(ds_vl_juros_w);
		vl_juros_ret_w		:= nvl(dividir_sem_round(vl_juros_ret_w,100),0);
		vl_multa_ret_w		:= somente_numero(ds_vl_multa_w);
		vl_multa_ret_w		:= nvl(dividir_sem_round(vl_multa_ret_w,100),0);
		
		vl_desconto_w	:= null;
		vl_juros_w	:= null;
		vl_multa_w	:= null;
				

		select	count(*)
		into	qt_reg_w
		from	titulo_pagar_escrit
		where	nr_seq_escrit	= nr_seq_banco_escrit_p
		and	nr_titulo			= nr_titulo_w;

		if	(qt_reg_w = 0) then
			gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);

			select	count(*) /*Coloquei de novo para contar, para somente  baixar os titulso caso tenha localizado o mesmo*/
			into	qt_reg_w
			from	titulo_pagar_escrit
			where	nr_seq_escrit	= nr_seq_banco_escrit_p
			and	nr_titulo			= nr_titulo_w;

		end if;

		ie_liquidacao_w	:= 'N';

		if	(cd_retorno_liq_w	is not null) and
			(cd_retorno_liq_w	in (cd_ocorrencia1_w,cd_ocorrencia2_w,cd_ocorrencia3_w,cd_ocorrencia4_w,cd_ocorrencia5_w)) then

			ie_liquidacao_w	:= 'S';

		else

			select	nvl(max('S'),'N')
			into	ie_liquidacao_w
			from	erro_escritural b
			where	b.cd_banco	= cd_banco_w
			and	b.ie_rejeitado	= 'L'
			and	b.cd_erro	in (cd_ocorrencia1_w,cd_ocorrencia2_w,cd_ocorrencia3_w,cd_ocorrencia4_w,cd_ocorrencia5_w);

		end if;


		if	(nvl(ie_liquidacao_w,'N')	= 'S') and
			(qt_reg_w	> 0)	then

			select	vl_escritural
			into	vl_escritural_w
			from	titulo_pagar_escrit
			where	nr_seq_escrit	= nr_seq_banco_escrit_p
			and	nr_titulo	= nr_titulo_w;
			/* Colocado este tratamento para nao efetuar baixa com valor maior que o titulo */
			if	(vl_liquidacao_w	> vl_escritural_w) then
				vl_liquidacao_w	:= vl_escritural_w;
			end if;

			if	(nvl(vl_liquidacao_w,0)	<> 0) and (nvl(ie_movto_bco_pag_escrit_w,'T') = 'T') then

				select	max(a.nr_seq_trans_fin_baixa)
				into	nr_seq_trans_fin_baixa_w
				from	titulo_pagar a
				where	nr_titulo 	= nr_titulo_w;

				select	max(cd_tipo_baixa)
				into	cd_tipo_baixa_w
				from	transacao_financeira
				where	nr_sequencia = nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_escrit_w);
				
				--Buscar os dados necessarios
				--Validar uma a um, desconto, juros, multa.

				select	nvl(vl_desconto, 0),
					nvl(vl_juros, 0),
					nvl(vl_multa, 0)
				into	vl_desconto_w,
					vl_juros_w,
					vl_multa_w
				from	titulo_pagar_escrit
				where	nr_titulo	= nr_titulo_w
				and	nr_seq_escrit	= nr_seq_banco_escrit_p;

				if vl_desconto_ret_w > 0 then
					vl_desconto_w := vl_desconto_ret_w;
				end if;
				
				if vl_juros_ret_w > 0 then
					vl_juros_w := vl_juros_ret_w;
				end if;
				
				if vl_multa_ret_w > 0 then
					vl_multa_w := vl_multa_ret_w;
				end if;
        			
        --OS 1987123: Precisa atualizar esses valores antes da baixa para ocorrer o preenchimento dos valores de desconto, multa e juros na baixa.
        update 	titulo_pagar_escrit 
          set	vl_desconto	= nvl(vl_desconto_w,vl_desconto),
   				vl_juros	= nvl(vl_juros_w,vl_juros),
  				vl_multa	= nvl(vl_multa_w,vl_multa)
          where	nr_titulo	= nr_titulo_w
          and	nr_seq_escrit	= nr_seq_banco_escrit_p;

				baixa_titulo_pagar
						(cd_estabelecimento_w,
						nvl(cd_tipo_baixa_w,cd_tipo_baixa_padrao_w),
						nr_titulo_w,
						vl_liquidacao_w + vl_desconto_w,
						nm_usuario_p,
						nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_escrit_w),
						null,
						nr_seq_banco_escrit_p,
						dt_liquidacao_w,
						nr_seq_conta_banco_w);

        qt_baixa_w	:= nvl(qt_baixa_w,0) + 1;

				select	max(nr_sequencia)
				into	nr_sequencia_w
				from	titulo_pagar_baixa
				where	nr_titulo	= nr_titulo_w;

				gerar_movto_tit_baixa
						(nr_titulo_w,
						nr_sequencia_w,
						'P',
						nm_usuario_p,
						'N');

				atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
				Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);

			end if;
		else

			if	(qt_reg_w	= 0) then

				gerar_log_escritural('P',nr_seq_banco_escrit_p,nm_usuario_p,'O titulo ' || ds_nr_titulo_w || ' nao consta no sistema!','N');

			end if;

			select	nvl(max(ie_estornar_baixa),'N')
			into	ie_estornar_baixa_w
			from	erro_escritural
			where	cd_erro		in (cd_ocorrencia1_w,cd_ocorrencia2_w,cd_ocorrencia3_w,cd_ocorrencia4_w,cd_ocorrencia5_w)
			and	cd_banco	= cd_banco_w;

			if	(ie_estornar_baixa_w = 'S') then
				begin

				select	max(nr_sequencia)
				into	nr_seq_baixa_w
				from	titulo_pagar_baixa
				where	nr_titulo	= nr_titulo_w
				and	nr_seq_escrit is not null;

				if	(nr_seq_baixa_w is not null) then
					estornar_tit_pagar_baixa(nr_titulo_w, nr_seq_baixa_w, nvl(dt_liquidacao_w,dt_remessa_retorno_w), nm_usuario_p,'S');
				end if;
				end;
			end if;

		end if;

			update 	titulo_pagar_escrit
			set	vl_desconto 	= nvl(vl_desconto_w,vl_desconto),
				vl_juros	= nvl(vl_juros_w,vl_juros),
				vl_multa	= nvl(vl_multa_w,vl_multa),
				ds_erro		= substr(cd_ocorrencia1_w||cd_ocorrencia2_w||cd_ocorrencia3_w||cd_ocorrencia4_w||cd_ocorrencia5_w,1,120),
				nr_pagamento 	= trim(nr_pagamento_w)
			where	nr_titulo	= nr_titulo_w
			and	nr_seq_escrit	= nr_seq_banco_escrit_p;
		/*if nvl(vl_desconto_w,0)	<> 0 then
		else
			update	titulo_pagar_escrit
			set	ds_erro		= substr(cd_ocorrencia1_w||cd_ocorrencia2_w||cd_ocorrencia3_w||cd_ocorrencia4_w||cd_ocorrencia5_w,1,120),
				nr_pagamento 	= trim(nr_pagamento_w)
			where	nr_titulo	= nr_titulo_w
			and	nr_seq_escrit	= nr_seq_banco_escrit_p;
		end if;*/

		-- Atribuicao do nosso numero. - OS 1814571
		select 	max(a.nr_nosso_numero)
		into 	nr_nosso_numero_w
		from 	titulo_pagar a
		where 	a.nr_titulo = nr_titulo_w;

		if( nvl(nr_nosso_numero_w,0) <> nr_pagamento_w ) then
			update 	titulo_pagar
			set 	nr_nosso_numero = nr_nosso_numero_w
			where 	nr_titulo = nr_titulo_w;
		end if;


	end loop;
	close c01;

else
	/* O arquivo que esta sendo importado nao e de retorno, favor verifique! */
	wheb_mensagem_pck.exibir_mensagem_abort(192790);
end if;

if ((nvl(ie_movto_bco_pag_escrit_w,'T') = 'T') and (nvl(qt_baixa_w,0)	> 0)) then
	update	banco_escritural
	set	dt_baixa	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_banco_escrit_p;
end if;

commit;

end GERAR_RETORNO_PAGTO_SANTANDER;
/
