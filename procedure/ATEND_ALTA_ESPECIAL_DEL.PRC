create or replace
procedure atend_alta_especial_del ( 	nr_sequencia_p	Number) is 

begin
if (nvl(nr_sequencia_p,0) > 0) then

	delete 
	from	atend_alta_esp_mat
	where	nr_seq_alta_esp = nr_sequencia_p;
end if;

commit;

end atend_alta_especial_del;
/