create or replace
procedure Substituir_medic_uso_continuo	(cd_pessoa_fisica_p	varchar2,
					ie_uso_continuo_p	varchar2,
					ie_laudo_lme_p		varchar2,
					cd_cid_principal_p	varchar2,
					cd_cid_secundario_p	varchar2,
					cd_intervalo_p		varchar2,
					ie_via_aplicacao_p	varchar2,
					cd_material_p		number,
					qt_dose_p		number,
					cd_unidade_medida_p	varchar2,
					nr_dias_uso_p		number,
					nr_seq_motivo_p		number,
					nr_seq_medic_ant_p	number,
					dt_inicio_p		date,
					nm_usuario_p		varchar2) is
begin
if	(nr_seq_motivo_p = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(198078);
elsif	(cd_material_p = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(198079);
elsif	(qt_dose_p = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(198080);
elsif	(cd_intervalo_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(198081);
elsif	(cd_unidade_medida_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(198082);
else
	insert into medic_uso_continuo	(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_pessoa_fisica,
					dt_inicio,
					cd_material,
					qt_dose,
					cd_unidade_medida,
					nr_dias_uso,
					ie_uso_continuo,
					ie_laudo_lme,
					cd_cid_principal,
					cd_cid_secundario,
					cd_intervalo,
					ie_via_aplicacao,
					ds_observacao,
					nr_seq_medic_subst)
				values	(medic_uso_continuo_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_pessoa_fisica_p,
					dt_inicio_p,
					cd_material_p,
					qt_dose_p,
					cd_unidade_medida_p,
					decode(nr_dias_uso_p,0,null,nr_dias_uso_p),
					ie_uso_continuo_p,
					ie_laudo_lme_p,
					cd_cid_principal_p,
					cd_cid_secundario_p,
					cd_intervalo_p,
					ie_via_aplicacao_p,
					-- Medicamento substituído em 
					wheb_mensagem_pck.get_texto(455790) || PKG_DATE_FORMATERS.to_varchar(sysdate, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),
					nr_seq_medic_ant_p);

	update	medic_uso_continuo
	set	dt_suspensao		= sysdate,
		nm_usuario_susp 	= nm_usuario_p,
		nr_seq_motivo_susp	= nr_seq_motivo_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_medic_ant_p;

	commit;
	
end if;

end Substituir_medic_uso_continuo;
/