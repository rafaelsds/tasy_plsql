create or replace
procedure atualizar_lab_solic_acesso(	ds_email_p		varchar2,
					nm_solicitante_p	varchar2,
					nr_crm_p			varchar2,
					nm_usuario_p		varchar2,
					nr_sequencia_p	out	number) is 
nr_sequencia_w	number(10);

begin

select  lab_solic_acesso_portal_seq.nextval
into	nr_sequencia_w
from	dual;

insert into  lab_solic_acesso_portal(
					nr_sequencia,           
					dt_solicitacao,         
					nm_solicitante,         
					nr_crm,                 
					ds_email)
				values(
					nr_sequencia_w,
					sysdate,
					nm_solicitante_p,
					nr_crm_p,
					ds_email_p
				);
				
nr_sequencia_p := nr_sequencia_w;

commit;

end atualizar_lab_solic_acesso;
/