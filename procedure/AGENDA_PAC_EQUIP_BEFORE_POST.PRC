create or replace
procedure agenda_pac_equip_before_post(
		nr_seq_agenda_p		number,
		cd_equipamento_p		number,
		nr_seq_classif_equip_p	number,
		nm_usuario_p		varchar2,
		ie_novo_reg_p		varchar2,
		ie_status_equipamento_p	out varchar2,
		ds_erro_p			out varchar2) is

ds_valor_parametro_w	varchar2(255);
ds_valor_parametro2_w	varchar2(255);
ds_erro_w		varchar2(1000);
qt_retorno_w		number(10,0);

begin
if	(nr_seq_agenda_p is not null) then
	begin
	Obter_Param_Usuario(871, 457, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ds_valor_parametro_w);
	if	(ds_valor_parametro_w = 'S') then
		begin
		select	count(*)
		into	qt_retorno_w
		from	agenda_pac_equip
		where	ie_origem_inf	= 'I' 
		and	nr_seq_agenda	= nr_seq_agenda_p
		and	cd_equipamento	= cd_equipamento_p;
		if	(qt_retorno_w > 0) then
			begin
			wheb_mensagem_pck.exibir_mensagem_abort(183208,'DS_ERRO_P='|| obter_texto_tasy(70177, wheb_usuario_pck.get_nr_seq_idioma));
			end;
		end if;
		end;
	end if;
	Obter_Param_Usuario(871, 81, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ds_valor_parametro_w);
	if	(ds_valor_parametro_w <> 'N') then
		begin
		Obter_Param_Usuario(871, 100, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ds_valor_parametro2_w);
		if	(ds_valor_parametro2_w = 'C') then
			begin
			obter_se_classif_equip_disp(
					nr_seq_agenda_p,
					nr_seq_classif_equip_p,
					ds_erro_w,
					nm_usuario_p,
					null,
					null,
					ie_novo_reg_p);
			end;
		elsif	(ds_valor_parametro2_w = 'E') then
			begin
			obter_se_equip_disp_cirur(nr_seq_agenda_p,
				cd_equipamento_p,
				ds_erro_w,
				nm_usuario_p,
				null,
				null,
				ie_novo_reg_p);
			ie_status_equipamento_p := obter_status_equipamento(nr_seq_agenda_p, cd_equipamento_p, 'S');
			end;
		end if;
		if	(ds_erro_w is not null) then
			ds_erro_p := ds_erro_w;
		end if;
		end;
	end if;
	end;
end if;
end agenda_pac_equip_before_post;
/
