create or replace
procedure pls_gerar_rel_ressarcidos( 	dt_recebimento_ini_p	date,
				dt_recebimento_fin_p	date,
				nr_seq_segurado_p	pls_ressarcimento_v.nr_seq_segurado%type,
				nm_arquivo_p		varchar2,
				nm_usuario_p		Varchar2) is 

ds_conteudo_w			varchar2(4000);
nm_arquivo_w			varchar2(255);
ds_local_w			varchar2(255) := null;
ds_erro_w			varchar2(255);
arq_texto_w			utl_file.file_type;

Cursor C01 is
	select 		nr_seq_conta ||';'|| 
			cd_procedimento ||';'|| 
			ds_procedimento ||';'|| 
			dt_procedimento ||';'|| 
			dt_recebimento ||';'|| 
			cd_usuario_plano ||';'|| 
			nm_segurado ||';'|| 
			nm_prestador_pagto ||';'|| 
			ds_def ||';'|| 
			ds_grupo_ans ||';'|| 
			ds_tipo_relacao ||';'|| 
			ds_ato_cooperado ||';'|| 
			ds_preco ||';'|| 
			ds_regulamentacao ||';'|| 
			ds_tipo_contratacao ||';'|| 
			ds_segmentacao ||';'|| 
			vl_evento ||';'|| 
			vl_ressarcimento ||';'|| 
			dt_movimento ||';'|| 
			cd_classif_prov_deb ||';'|| 
			cd_classif_prov_cred ||';'|| 
			nr_seq_segurado ||';'|| 
			dt_mes_competencia ||';'|| 
			nr_titulo ||';'|| 
			cd_guia ds_conteudo
	from 		pls_ressarcimento_v a
	where 		0=0
	and		dt_recebimento between dt_recebimento_ini_p and fim_dia ( dt_recebimento_fin_p )
	and		( nr_seq_segurado_p = '0' or a.nr_seq_segurado = nr_seq_segurado_p );

begin

begin
obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);
exception
when others then
	ds_local_w := null;
end;

nm_arquivo_w	:= nm_arquivo_p;

begin
arq_texto_w := utl_file.fopen( ds_local_w,nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

select 		'Nr conta' ||';'||
		'C�digo procedimento material' ||';'|| 
		'Ds procedimento material' ||';'|| 
		'Dt procedimento' ||';'|| 
		'Dt recebimento' ||';'|| 
		'Cd usuario plano' ||';'|| 
		'Nm segurado' ||';'|| 
		'Prestador' ||';'|| 
		'Defini��o opera��o' ||';'|| 
		'Tipo proc' ||';'|| 
		'Tipo v�nculo'||';'|| 
		'Defini��o dos Atos' ||';'|| 
		'Modalidade contrata��o' ||';'|| 
		'Tipo regulamenta��o' ||';'|| 
		'Tipo de contrata��o' ||';'|| 
		'Segmenta��o' ||';'|| 
		'Valor evento' ||';'|| 
		'Valor recupera��o' ||';'|| 
		'Data contabiliza��o' ||';'|| 
		'Conta cont�bil d�bito'||';'|| 
		'Conta cont�bil cr�dito' ||';'|| 
		'Nr Seq Segurado' ||';'|| 
		'Data contabiliza��o' ||';'|| 
		'N�mero do t�tulo de cobran�a' ||';'|| 
		'Cd Guia' ds_conteudo
into 		ds_conteudo_w
from 		dual;

utl_file.put_line( arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush( arq_texto_w);
ds_conteudo_w := null;

open C01;
loop
fetch C01 into	
	ds_conteudo_w;
exit when C01%notfound;
	begin
	utl_file.put_line( arq_texto_w,ds_conteudo_w || chr(13));
	utl_file.fflush( arq_texto_w);
	ds_conteudo_w := null;	
	end;
end loop;
close C01;
	
end pls_gerar_rel_ressarcidos;
/