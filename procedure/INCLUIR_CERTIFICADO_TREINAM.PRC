create or replace
procedure incluir_certificado_treinam	(nr_sequencia_p	number,
				ds_arquivo_p	varchar2) is

begin

if	(ds_arquivo_p is not null) then
	
	update	qms_treinamento
	set	ds_arquivo_certificado = ds_arquivo_p
	where	nr_sequencia = nr_sequencia_p;
	
	commit;
end if;

end incluir_certificado_treinam;
/