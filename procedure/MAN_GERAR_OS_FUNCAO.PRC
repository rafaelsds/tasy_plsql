create or replace
procedure man_gerar_os_funcao(	nr_seq_funcao_grupo_des_p		in	number,
				ds_dano_breve_p			in	varchar2,
				ds_dano_p			in	varchar2,
				nm_usuario_p			in	varchar2) is

nr_sequencia_w			number(10);
nm_usuario_exec_w		varchar2(255);
nr_seq_gerencia_w			number(10,0);
NM_USUARIO_LIDER_W		varchar2(255);
CD_PESSOA_SOLICITANTE_W	varchar2(255);
NR_SEQ_GRUPO_DES_W		number(10,0);
CD_FUNCAO_W			number(10,0);
nr_seq_estagio_w			number(10,0);

cursor c01 is
select	cd_funcao
from	Funcao_grupo_des
where	nr_seq_grupo		= nr_seq_grupo_des_w;

begin

select	max(NR_SEQ_GRUPO),
	max(cd_funcao)
into	nr_seq_grupo_des_w,
	cd_funcao_w
from	Funcao_grupo_des
where	nr_sequencia		= nr_seq_funcao_grupo_des_p;

select	max(a.nr_sequencia),
	max(c.nm_usuario_grupo)
into	nr_seq_gerencia_w,
	NM_USUARIO_LIDER_w
from	gerencia_wheb a, 
	grupo_desenvolvimento b,
	usuario_grupo_des c
where	a.nr_sequencia	= b.nr_seq_gerencia
and	b.nr_sequencia = c.nr_seq_grupo
and	c.ie_funcao_usuario = 'S'
and	b.nr_sequencia	= nr_seq_grupo_des_w;

select	max(cd_pessoa_fisica)
into	cd_pessoa_solicitante_w
from	usuario
where	nm_usuario	= NM_USUARIO_LIDER_w;

open c01;
loop
fetch c01 into
	cd_funcao_w;
exit when c01%notfound;

	select	man_ordem_servico_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert	into man_ordem_servico(
		nr_sequencia,
		nr_seq_localizacao,
		nr_seq_equipamento,
		cd_pessoa_solicitante,
		dt_ordem_servico,
		ie_prioridade,
		ie_parado,
		ds_dano_breve,
		dt_atualizacao,
		nm_usuario,
		dt_inicio_desejado,
		dt_conclusao_desejada,
		ds_dano,
		dt_inicio_previsto,
		dt_fim_previsto,
		dt_inicio_real,
		dt_fim_real,
		ie_tipo_ordem,
		ie_status_ordem,
		nr_grupo_planej,
		nr_grupo_trabalho,
		nr_seq_tipo_solucao,
		ds_solucao,
		nm_usuario_exec,
		qt_contador,
		nr_seq_planej,
		nr_seq_tipo_contador,
		nr_seq_estagio,
		cd_projeto,
		nr_seq_etapa_proj,
		dt_reabertura,
		cd_funcao,
		nm_tabela,
		ie_classificacao,
		nr_seq_origem,
		nr_seq_projeto,
		ie_grau_satisfacao,
		nr_seq_indicador,
		nr_seq_causa_dano,
		ie_forma_receb,
		nr_seq_cliente,
		nr_seq_grupo_des,
		nr_seq_grupo_sup,
		nr_seq_superior,
		ie_eficacia,
		dt_prev_eficacia,
		cd_pf_eficacia,
		nr_seq_nao_conform,
		nr_seq_complex,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_obriga_news,
		nr_seq_meta_pe,
		nr_seq_classif,
		nr_seq_nivel_valor,
		nm_usuario_lib_news,
		dt_libera_news,
		dt_envio_wheb,
		ds_contato_solicitante,
		ie_prioridade_desen,
		ie_prioridade_sup,
		nr_seq_proj_cron_etapa,
		ie_exclusiva,
		ie_origem_os)
	values(	nr_sequencia_w,
		1272,					-- nr_seq_localizacao,
		1395,					-- nr_seq_equipamento,
		cd_pessoa_solicitante_w,		-- cd_pessoa_solicitante,
		sysdate,				-- dt_ordem_servico,
		'M',					-- ie_prioridade,
		'N',					-- ie_parado,
		substr(nvl(ds_dano_breve_p, 'Revisar par�metros da fun��o'),1,80),		-- ds_dano_breve,
		sysdate,				-- dt_atualizacao,
		nm_usuario_p,				-- nm_usuario,
		sysdate,				-- dt_inicio_desejado,
		(sysdate + 15),				-- dt_conclusao_desejada,
		nvl(ds_dano_p, 'Revisar par�metros da fun��o'),				-- ds_dano,
		sysdate,				-- dt_inicio_previsto,
		sysdate + 15,				-- dt_fim_previsto,
		sysdate,				-- dt_inicio_real,
		null,					-- dt_fim_real,
		'1',					-- ie_tipo_ordem,
		'1',					-- ie_status_ordem,
		2,					-- nr_grupo_planej,
		2,					-- nr_grupo_trabalho,
		null,					-- nr_seq_tipo_solucao,
		null,					-- ds_solucao,
		NM_USUARIO_LIDER_w, 			-- nm_usuario_exec,
		null,					-- qt_contador,
		null,					-- nr_seq_planej,
		null,					-- nr_seq_tipo_contador,
		1191,					-- nr_seq_estagio,
		null,					-- cd_projeto,
		null,					-- nr_seq_etapa_proj,
		null,					-- dt_reabertura,
		cd_funcao_w,				-- cd_funcao,
		null,					-- nm_tabela,
		'S',					-- ie_classificacao,
		null,					-- nr_seq_origem,
		null,					-- nr_seq_projeto,
		null,					-- ie_grau_satisfacao,
		null,					-- nr_seq_indicador,
		null,					-- nr_seq_causa_dano,
		'U',					-- ie_forma_receb,
		null,					-- nr_seq_cliente,
		nr_seq_grupo_des_w,			-- nr_seq_grupo_des,
		null,					-- nr_seq_grupo_sup,
		null,					-- nr_seq_superior,
		null,					-- ie_eficacia,
		null,					-- dt_prev_eficacia,
		null,					-- cd_pf_eficacia,
		null,					-- nr_seq_nao_conform,
		null,					-- nr_seq_complex,
		null,					-- dt_atualizacao_nrec,
		nm_usuario_p,				-- nm_usuario_nrec,
		null,					-- ie_obriga_news,
		null,					-- nr_seq_meta_pe,
		null,					-- nr_seq_classif,
		null,					-- nr_seq_nivel_valor,
		null,					-- nm_usuario_lib_news,
		null,					-- dt_libera_news,
		null,					-- dt_envio_wheb,
		null,					-- ds_contato_solicitante,
		null,					-- ie_prioridade_desen,
		null,					-- ie_prioridade_sup
		null,					-- nr_seq_proj_cron_etapa
		'E',					-- ie_exclusiva
		'1');					

	insert into man_ordem_servico_exec(
		nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		nm_usuario_exec,
		dt_recebimento)
	values (man_ordem_servico_exec_seq.nextval,
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		NM_USUARIO_LIDER_w,
		sysdate);

end loop;
close c01;

commit;

end man_gerar_os_funcao;
/
