create or replace
procedure hc_replicar_regra_bloq_ag_cd (nr_seq_agenda_copia_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					nr_seq_bloqueio_p	number,
					nr_seq_agenda_dest_p	number) is

/* regras exame x bloqueio */
dt_inicial_w			date;
dt_final_w			date;
ie_motivo_bloqueio_w		varchar2(1);
ds_observacao_w			varchar2(255);
ie_dia_semana_w			number(5,0);
hr_inicio_bloqueio_w		date;
hr_final_bloqueio_w		date;
nr_seq_bloqueio_w		number(10,0);

/* obter regras bloqueio */
cursor c01 is
select	dt_inicial,
	dt_final,
	ie_motivo_bloqueio,
	ds_observacao,
	ie_dia_semana,
	hr_inicio_bloqueio,
	hr_final_bloqueio
from	hc_agenda_bloqueio
where	nr_seq_agenda = nr_seq_agenda_copia_p
and	nr_sequencia = nr_seq_bloqueio_p;

begin
if	(nr_seq_agenda_copia_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) and
	(nr_seq_bloqueio_p is not null) and
	(nr_seq_agenda_dest_p is not null) then
	
	/* gera regras */
	open c01;
	loop
	fetch c01 into	dt_inicial_w,
			dt_final_w,
			ie_motivo_bloqueio_w,
			ds_observacao_w,
			ie_dia_semana_w,
			hr_inicio_bloqueio_w,
			hr_final_bloqueio_w;
	exit when c01%notfound;
		begin
		
		select	hc_agenda_bloqueio_seq.nextval
		into	nr_seq_bloqueio_w
		from	dual;	
		
		insert into hc_agenda_bloqueio	(
						nr_sequencia,
						nr_seq_agenda,
						dt_inicial,
						dt_final,
						ie_motivo_bloqueio,
						ds_observacao,
						ie_dia_semana,
						hr_inicio_bloqueio,
						hr_final_bloqueio,
						nm_usuario,
						dt_atualizacao
						)
				values		(
						nr_seq_bloqueio_w,
						nr_seq_agenda_dest_p,
						dt_inicial_w,
						dt_final_w,
						ie_motivo_bloqueio_w,
						ds_observacao_w,
						ie_dia_semana_w,
						hr_inicio_bloqueio_w,
						hr_final_bloqueio_w,
						nm_usuario_p,
						sysdate
						);
		
		
		end;
	end loop;
	close c01;	
		
end if;

commit;

end hc_replicar_regra_bloq_ag_cd;
/
