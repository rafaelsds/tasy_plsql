create or replace
procedure pls_vincular_nota_lote_prod(	nr_seq_lote_p			pls_lote_pagamento.nr_sequencia%type,
					nr_seq_prestador_pgto_p		pls_prestador.nr_sequencia%type,
					nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
					cd_serie_nf_p			nota_fiscal.cd_serie_nf%type,
					cd_operacao_nf_p		nota_fiscal.cd_operacao_nf%type,
					dt_emissao_p			date,
					cd_natureza_operacao_p		number,
					ds_observacao_p			varchar2,
					ds_complemento_p		varchar2,
					dt_base_venc_p			date,
					nr_nota_fiscal_p		nota_fiscal.nr_nota_fiscal%type,
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_base_venc_p		 	varchar2,
					nr_seq_nota_fiscal_p	out	nota_fiscal.nr_sequencia%type) is 

nr_seq_nota_fiscal_w		nota_fiscal.nr_sequencia%type;
ds_consistencia_w		varchar2(255);
ie_perm_vin_nf_fechada_w	varchar2(1);

Cursor C01 is
	select	b.nr_titulo
	from	titulo_pagar b,
		pls_pagamento_prestador a
	where	b.nr_seq_pls_pag_prest	= a.nr_sequencia
	and	b.ie_tipo_titulo	<> '4'
	and	a.nr_sequencia		= nr_seq_prestador_pgto_p
	and	ie_perm_vin_nf_fechada_w = 'N' 
	and 	b.ie_situacao = 'A'
	union all
	select	b.nr_titulo
	from	titulo_pagar b,
		pls_pagamento_prestador a
	where	b.nr_seq_pls_pag_prest	= a.nr_sequencia
	and	b.ie_tipo_titulo	<> '4'
	and	a.nr_sequencia		= nr_seq_prestador_pgto_p
	and	ie_perm_vin_nf_fechada_w = 'S';
	
Cursor C02 is
	select	nvl(sum(decode(b.ie_situacao,'A',1,0)),0) qt_aberto,
		nvl(sum(decode(b.ie_situacao,'A',0,1)),0) qt_nao_aberto
	from	titulo_pagar 		b,
		pls_pagamento_prestador a
	where	b.nr_seq_pls_pag_prest	= a.nr_sequencia
	and	b.ie_tipo_titulo 	<> '4'
	and	a.nr_sequencia		= nr_seq_prestador_pgto_p;
	
begin

ie_perm_vin_nf_fechada_w := obter_valor_param_usuario(1282, 30, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);

if	(ie_perm_vin_nf_fechada_w = 'N') then

	for r_C02_w in C02 loop
	
		if	(r_C02_w.qt_aberto = 0) and 
			(r_C02_w.qt_nao_aberto > 0) then
			
			--N�o � poss�vel vincular notas fiscais em t�tulos a pagar que n�o estejam com a situa��o em "Aberto".
			wheb_mensagem_pck.exibir_mensagem_abort(355986);
		end if;
	end loop;
end if;

/*Verificado se j� existe uma nota fiscal gerada para este vencimento*/
select	max(nr_sequencia)
into	nr_seq_nota_fiscal_w
from	nota_fiscal
where	nr_seq_pgto_prest = nr_seq_prestador_pgto_p;

if	(nr_seq_nota_fiscal_w is not null) then

	wheb_mensagem_pck.exibir_mensagem_abort(265325,'');
	--Mensagem: J� existe nota fiscal gerada para este vencimento!
end if;

update	pls_pagamento_prestador
set	nr_nota_fiscal	= nr_nota_fiscal_p
where	nr_sequencia	= nr_seq_prestador_pgto_p;

pls_gerar_notas_lote_prod(	nr_seq_lote_p, cd_serie_nf_p, cd_operacao_nf_p,
				dt_emissao_p, cd_natureza_operacao_p, ds_observacao_p,
				ds_complemento_p, dt_base_venc_p, nr_nota_fiscal_p,
				nm_usuario_p, 'S', nr_seq_prestador_p,
				nr_seq_prestador_pgto_p, cd_estabelecimento_p, ie_base_venc_p,
				nr_seq_nota_fiscal_w);

if	(nr_seq_nota_fiscal_w is null) then

	wheb_mensagem_pck.exibir_mensagem_abort(265323,'');
	--Mensagem: N�o foi gerada nota fiscal para o lote!
end if;

nr_seq_nota_fiscal_p := nr_seq_nota_fiscal_w;

for r_c01_w in c01 loop

	vincular_titulo_pagar_nf(	nr_seq_nota_fiscal_w, r_c01_w.nr_titulo, nm_usuario_p, 
					'A','S', ds_consistencia_w);
end loop;

commit;

end pls_vincular_nota_lote_prod;
/