create or replace
procedure alterar_fornec_nota_fiscal(	nr_sequencia_p	number,
					cd_cgc_p	varchar2,
					nm_usuario_p	varchar2) is 

cd_estabelecimento_w		number(10);
nr_nota_fiscal_w		varchar2(255);
cd_serie_nf_w			nota_fiscal.cd_serie_nf%type;
nr_sequencia_nf_w		number(10);
nr_Sequencia_w			number(10);
qt_registro_w			number(10);

cursor c01 is
select	nr_Sequencia
from	nota_fiscal
where	cd_estabelecimento = cd_estabelecimento_w
and	cd_cgc_emitente = cd_cgc_p
and	cd_serie_nf = cd_serie_nf_w
and	nr_nota_fiscal = nr_nota_fiscal_w
and	nr_sequencia_nf = nr_sequencia_nf_w
and	ie_situacao = '1';
				
begin

if	(cd_cgc_p is not null) then

	select	cd_estabelecimento,
		nr_nota_fiscal,
		cd_serie_nf,
		nr_sequencia_nf
	into	cd_estabelecimento_w,
		nr_nota_fiscal_w,
		cd_serie_nf_w,
		nr_sequencia_nf_w
	from	nota_fiscal
	where	nr_sequencia = nr_sequencia_p;

	select	count(*)
	into	qt_registro_w
	from	nota_fiscal
	where	cd_estabelecimento = cd_estabelecimento_w
	and	cd_cgc_emitente = cd_cgc_p
	and	cd_serie_nf = cd_serie_nf_w
	and	nr_nota_fiscal = nr_nota_fiscal_w
	and	nr_sequencia_nf = nr_sequencia_nf_w
	and	ie_situacao = '1';
	
	if	(qt_registro_w > 0) then
		
		open C01;
		loop
		fetch C01 into	
			nr_sequencia_w;
		exit when C01%notfound;
			begin
			nr_sequencia_w := nr_sequencia_w;
			end;
		end loop;
		close C01;
		
		if	(nr_sequencia_w > 0) then
			wheb_mensagem_pck.Exibir_Mensagem_Abort(237506,'NR_SEQUENCIA_W='||nr_sequencia_w);
		end if;

	else

		select	count(*)
		into	qt_registro_w
		from	nota_fiscal
		where	cd_estabelecimento = cd_estabelecimento_w
		and	cd_cgc_emitente = cd_cgc_p
		and	cd_serie_nf = cd_serie_nf_w
		and	nr_nota_fiscal = nr_nota_fiscal_w
		and	nr_sequencia_nf = nr_sequencia_nf_w
		and	ie_situacao <> '1';

		if	(qt_registro_w > 0) then
			
			select	nvl(max(nr_sequencia_nf),0) + 1
			into	nr_sequencia_nf_w
			from	nota_fiscal
			where	cd_estabelecimento = cd_estabelecimento_w
			and	cd_cgc_emitente = cd_cgc_p
			and	cd_serie_nf = cd_serie_nf_w
			and	nr_nota_fiscal = nr_nota_fiscal_w;
			
			update	nota_fiscal
			set	nr_sequencia_nf = nr_sequencia_nf_w,
				cd_cgc_emitente = cd_cgc_p,
				cd_cgc = cd_cgc_p,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where	nr_sequencia = nr_sequencia_p;
		else
		
			update	nota_fiscal
			set	cd_cgc_emitente = cd_cgc_p,
				cd_cgc = cd_cgc_p,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where	nr_sequencia = nr_sequencia_p;
			
		end if;
		
	end if;

end if;

commit;

end alterar_fornec_nota_fiscal;
/
