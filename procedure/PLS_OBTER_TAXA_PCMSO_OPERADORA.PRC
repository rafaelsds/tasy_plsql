create or replace
procedure pls_obter_taxa_pcmso_operadora
			(	dt_procedimento_p		date,
				nr_seq_congenere_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			Varchar2,
				pr_taxa_p		out	number) is 

pr_taxa_w			number(15,2)	:= 0;

Cursor C01 is
	select	a.pr_taxa
	from	pls_regra_pcmso	a
	where	dt_procedimento_p	between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_procedimento_p)
	and	a.nr_seq_congenere	= nr_seq_congenere_p
	order by dt_inicio_vigencia;
begin
open C01;
loop
fetch C01 into	
	pr_taxa_w;
exit when C01%notfound;
end loop;
close C01;

pr_taxa_p	:= nvl(pr_taxa_w,0);

end pls_obter_taxa_pcmso_operadora;
/ 
