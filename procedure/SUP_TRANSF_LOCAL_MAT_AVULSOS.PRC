create or replace
procedure	sup_transf_local_mat_avulsos(
			nr_seq_reg_kit_p			number,
			cd_local_destino_p			number,
			cd_operacao_estoque_p		number,
			cd_setor_atendimento_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

nr_seq_material_w		kit_estoque_comp_avulso.nr_sequencia%type;

cursor	c01 is
select	nr_sequencia
from	kit_estoque_comp_avulso
where	nr_seq_reg_kit = nr_seq_reg_kit_p;

begin

open c01;
loop
fetch c01 into
	nr_seq_material_w;
exit when c01%notfound;
	begin
	
	sup_transfere_mat_avulso_local(nr_seq_reg_kit_p, cd_local_destino_p, cd_operacao_estoque_p, cd_setor_atendimento_p, cd_estabelecimento_p, nr_seq_material_w, nm_usuario_p);
	
	end;
end loop;
close c01;

end sup_transf_local_mat_avulsos;
/