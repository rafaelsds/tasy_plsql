create or replace
procedure lab_incluir_frase_obs_exame_js(	nr_prescricao_p		NUMBER,
									nr_seq_prescr_p	  	NUMBER,
									nr_sequencia_p		NUMBER,
									ds_frase_p	  	varchar2) is 

nr_seq_resultado_w	number(10);
									
begin

select	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado	
where 	nr_prescricao = nr_prescricao_p;

if 	(nr_prescricao_p is not null) and
	(nr_seq_prescr_p is not null) and
	(ds_frase_p is not null) then

	if (nr_sequencia_p <> 0) then
		update	exame_lab_result_item
		set 	ds_observacao = ds_observacao||' '||ds_frase_p
		where	nr_seq_prescr = nr_seq_prescr_p
		and	nr_seq_resultado = nr_seq_resultado_w
		and	nr_sequencia = nr_sequencia_p;
	else
		update	exame_lab_result_item
		set 	ds_observacao = ds_observacao||' '||ds_frase_p
		where	nr_seq_prescr = nr_seq_prescr_p
		and	nr_seq_resultado = nr_seq_resultado_w
		and 	nr_seq_formato is not null
		and 	nr_seq_material is not null;
	end if;
	
end if;

commit;

end lab_incluir_frase_obs_exame_js;
/
