create or replace
procedure gerar_grid_espasticidade (	cd_pessoa_fisica_p	varchar2,
					ie_ordem_p		number,
					nm_usuario_p		Varchar2) is 

					
/* vetor */
type colunas is record ( nm_coluna_w varchar2(255),
			 dt_registro_w	date,
			 ie_tipo_w	varchar2(10),
			 ds_tipo_w	varchar2(255),
			 nr_sequencia_w		number(10),
			 IE_TIPO_AVALIACAO	varchar2(10));
type vetor is table of colunas index by binary_integer;


/* globais */
vetor_w			vetor;			
ivet			number(10) := 0;	
ind			integer;
dt_registro_w		date;	
nr_sequencia_w		number(10);
nr_seq_registro_w	number(10);	
ie_tipo_w		varchar2(10);
ds_comando_w		varchar2(3000);
ds_parametro_w		varchar2(3000);
ds_valor_w		varchar2(3000);
qt_reg_w		number(10);
nr_seq_art_mov_musculo_w	number(10);
ie_ordem_w			number(10)	:= nvl(ie_ordem_p,0);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(300239, null, wheb_usuario_pck.get_nr_seq_idioma);--Toxina
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(319192, null, wheb_usuario_pck.get_nr_seq_idioma);--Ashworth
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(305747, null, wheb_usuario_pck.get_nr_seq_idioma);--Pr�
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(305724, null, wheb_usuario_pck.get_nr_seq_idioma);--P�s
	
					
Cursor C01 is
	select	a.DT_AVALIACAO dt_avaliacao,
		'T' ie_tipo,
		expressao1_w ds_tipo,
		a.nr_sequencia,
		null ds_TIPO_AVALIACAO
	from	ATENDIMENTO_TOXINA a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	union
	select	a.DT_AVALIACAO dt_avaliacao,
		'A' ie_tipo,
		expressao2_w ds_tipo,
		a.nr_sequencia,
		decode(a.IE_TIPO_AVALIACAO,'PR',expressao3_w,'PO',expressao4_w) DS_TIPO_AVALIACAO
	from	escala_ashworth a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	order by dt_avaliacao;
	
Cursor C001 is
	select	a.DT_AVALIACAO dt_avaliacao,
		'T' ie_tipo,
		expressao1_w ds_tipo,
		a.nr_sequencia,
		null ds_TIPO_AVALIACAO
	from	ATENDIMENTO_TOXINA a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	union
	select	a.DT_AVALIACAO dt_avaliacao,
		'A' ie_tipo,
		expressao2_w ds_tipo,
		a.nr_sequencia,
		decode(a.IE_TIPO_AVALIACAO,'PR',expressao3_w,'PO',expressao4_w) DS_TIPO_AVALIACAO
	from	escala_ashworth a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	order by dt_avaliacao desc;  
	
	
cursor c02 is	
	select	b.nr_seq_art_mov_musculo,
		obter_seq_mov_musc(b.nr_seq_art_mov_musculo) nr_seq_apres,
		obter_mov_musc_ashworth(b.nr_seq_art_mov_musculo) ds_mov_musculo
	from	atendimento_toxina a,
		atend_toxina_item b
	where	a.nr_sequencia	= b.nr_seq_atendimento
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	group by b.nr_seq_art_mov_musculo
	union
	select	c.nr_seq_art_mov_musculo,
		obter_seq_mov_musc(c.nr_seq_art_mov_musculo) nr_seq_apres,
		obter_mov_musc_ashworth(c.nr_seq_art_mov_musculo) ds_mov_musculo
	from	escala_ashworth a,
		atendimento_paciente b,
		escala_ashworth_item c
	where	a.nr_sequencia = c.nr_seq_ashworth
	and	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	group by c.nr_seq_art_mov_musculo
	order by nr_seq_apres,ds_mov_musculo;
	
	
cursor c03 is
	select	a.nr_seq_art_mov_musculo,
		a.ie_ashworth_d,
		ie_ashworth_e
	from	escala_ashworth_item a
	where	a.nr_seq_ashworth = nr_seq_registro_w;
	
cursor c04 is
	select	a.nr_seq_art_mov_musculo,
		nvl(a.QT_DOSE_ADM,a.QT_DOSE_PRESCR) qt_dose,
		a.ie_lado
	from	ATEND_TOXINA_ITEM a
	where	a.NR_SEQ_ATENDIMENTO = nr_seq_registro_w;
	
cursor c05 is
	select	nvl(a.QT_DOSE_ADM,a.QT_DOSE_PRESCR) qt_dose,
		a.ie_lado
	from	ATEND_TOXINA_ITEM a
	where	nr_seq_atendimento	= nr_seq_registro_w
	and	NR_SEQ_ART_MOV_MUSCULO	= NR_SEQ_ART_MOV_MUSCULO_w
	order by a.ie_lado;
	
c01_w 	c01%rowtype;
c001_w 	c001%rowtype;
c02_w 	c02%rowtype;
c03_w	c03%rowtype;
c04_w	c04%rowtype;
c05_w	c05%rowtype;
					
begin

delete from w_result_espasticidade
where nm_usuario	= nm_usuario_p;

commit;
ivet	:= 0;

if	(ie_ordem_w	= 0) then
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin
		ivet := ivet + 1;
		vetor_w(ivet).nm_coluna_w   	:= 	to_char(c01_w.DT_AVALIACAO,'dd/mm/yy')||' '||chr(13)||
							to_char(c01_w.DT_AVALIACAO,'hh24:mi')||' '||chr(13)||
							c01_w.ds_tipo;
		if	(c01_w.DS_TIPO_AVALIACAO is not null) then
			
			vetor_w(ivet).nm_coluna_w	:= vetor_w(ivet).nm_coluna_w ||chr(13)||'('||c01_w.DS_TIPO_AVALIACAO||')';
		end if;
		vetor_w(ivet).dt_registro_w   	:= c01_w.DT_AVALIACAO;
		vetor_w(ivet).ie_tipo_w   	:= c01_w.ie_tipo;
		vetor_w(ivet).ds_tipo_w   	:= c01_w.ds_tipo;
		vetor_w(ivet).nr_sequencia_w   	:= c01_w.nr_sequencia;
		
		end;
	end loop;
	close C01;

else
	open C001;
	loop
	fetch C001 into	
		C001_w;
	exit when C001%notfound;
		begin
		ivet := ivet + 1;
		vetor_w(ivet).nm_coluna_w   	:= 	to_char(C001_w.DT_AVALIACAO,'dd/mm/yy')||' '||chr(13)||
							to_char(C001_w.DT_AVALIACAO,'hh24:mi')||' '||chr(13)||
							C001_w.ds_tipo;
		if	(C001_w.DS_TIPO_AVALIACAO is not null) then
			
			vetor_w(ivet).nm_coluna_w	:= vetor_w(ivet).nm_coluna_w ||chr(13)||'('||C001_w.DS_TIPO_AVALIACAO||')';
		end if;
		vetor_w(ivet).dt_registro_w   	:= C001_w.DT_AVALIACAO;
		vetor_w(ivet).ie_tipo_w   	:= C001_w.ie_tipo;
		vetor_w(ivet).ds_tipo_w   	:= C001_w.ds_tipo;
		vetor_w(ivet).nr_sequencia_w   	:= C001_w.nr_sequencia;
		
		end;
	end loop;
	close C001;

end if;

/* completar vetor se necess�rio  */
ind := ivet;
while	(ind < 50) loop
	begin
	ind := ind + 1;
	vetor_w(ind).nm_coluna_w   	:= null;
	vetor_w(ind).dt_registro_w   	:= null;
	vetor_w(ind).ie_tipo_w   	:= null;
	vetor_w(ind).ds_tipo_w   	:= null;
	vetor_w(ind).nr_sequencia_w   	:= null;
	end;
end loop;

select	w_result_espasticidade_seq.nextval
into	nr_sequencia_w
from	dual;

insert into w_result_espasticidade(
	nr_sequencia,           
	nm_usuario,
	dt_atualizacao,
	ie_ordem,
	ds_result1,
	ds_result2,
	ds_result3,
	ds_result4,
	ds_result5,
	ds_result6,
	ds_result7,
	ds_result8,
	ds_result9,
	ds_result10,
	ds_result11,
	ds_result12,
	ds_result13,
	ds_result14,
	ds_result15,
	ds_result16,
	ds_result17,
	ds_result18,
	ds_result19,
	ds_result20,
	ds_result21,
	ds_result22,
	ds_result23,
	ds_result24,
	ds_result25,
	ds_result26,
	ds_result27,
	ds_result28,
	ds_result29,
	ds_result30,
	ds_result31,
	ds_result32,
	ds_result33,
	ds_result34,
	ds_result35,
	ds_result36,
	ds_result37,
	ds_result38,
	ds_result39,
	ds_result40,
	ds_result41,
	ds_result42,
	ds_result43,
	ds_result44,
	ds_result45,
	ds_result46,
	ds_result47,
	ds_result48,
	ds_result49,
	ds_result50)
Values( nr_sequencia_w,
	nm_usuario_p,
	sysdate,
	-3,
	vetor_w(1).nm_coluna_w,
	vetor_w(2).nm_coluna_w,
	vetor_w(3).nm_coluna_w,
	vetor_w(4).nm_coluna_w,
	vetor_w(5).nm_coluna_w,
	vetor_w(6).nm_coluna_w,
	vetor_w(7).nm_coluna_w,
	vetor_w(8).nm_coluna_w,
	vetor_w(9).nm_coluna_w,
	vetor_w(10).nm_coluna_w,
	vetor_w(11).nm_coluna_w,
	vetor_w(12).nm_coluna_w,
	vetor_w(13).nm_coluna_w,
	vetor_w(14).nm_coluna_w,
	vetor_w(15).nm_coluna_w,
	vetor_w(16).nm_coluna_w,
	vetor_w(17).nm_coluna_w,
	vetor_w(18).nm_coluna_w,
	vetor_w(19).nm_coluna_w,
	vetor_w(20).nm_coluna_w,
	vetor_w(21).nm_coluna_w,
	vetor_w(22).nm_coluna_w,
	vetor_w(23).nm_coluna_w,
	vetor_w(24).nm_coluna_w,
	vetor_w(25).nm_coluna_w,
	vetor_w(26).nm_coluna_w,
	vetor_w(27).nm_coluna_w,
	vetor_w(28).nm_coluna_w,
	vetor_w(29).nm_coluna_w,
	vetor_w(30).nm_coluna_w,
	vetor_w(31).nm_coluna_w,
	vetor_w(32).nm_coluna_w,
	vetor_w(33).nm_coluna_w,
	vetor_w(34).nm_coluna_w,
	vetor_w(35).nm_coluna_w,
	vetor_w(36).nm_coluna_w,
	vetor_w(37).nm_coluna_w,
	vetor_w(38).nm_coluna_w,
	vetor_w(39).nm_coluna_w,
	vetor_w(40).nm_coluna_w,
	vetor_w(41).nm_coluna_w,
	vetor_w(42).nm_coluna_w,
	vetor_w(43).nm_coluna_w,
	vetor_w(44).nm_coluna_w,
	vetor_w(45).nm_coluna_w,
	vetor_w(46).nm_coluna_w,
	vetor_w(47).nm_coluna_w,
	vetor_w(48).nm_coluna_w,
	vetor_w(49).nm_coluna_w,
	vetor_w(50).nm_coluna_w);



	
open C02;
loop
fetch C02 into	
	c02_w;
exit when C02%notfound;
	begin
	
	select	w_result_espasticidade_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into w_result_espasticidade(
		nr_sequencia,           
		nm_usuario,
		dt_atualizacao,
		ie_ordem,
		nr_seq_art_mov_musculo,
		ds_mov_musculo)
	values( nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		1,
		c02_w.nr_seq_art_mov_musculo,
		c02_w.ds_mov_musculo);

	end;
end loop;
close C02;

commit;


ind := 0;
while (ind < 50) loop
	begin
	ind := ind + 1;
	nr_seq_registro_w	:= vetor_w(ind).nr_sequencia_w;
	ie_tipo_w		:= vetor_w(ind).ie_tipo_w;
	
	ds_comando_w	:= ' update w_result_espasticidade ' ||
		  ' set ds_result' || to_char(ind) ||'= :ds_valor '||
		  ' where nm_usuario = :nm_usuario ' ||
		  ' and   NR_SEQ_ART_MOV_MUSCULO = :NR_SEQ_ART_MOV_MUSCULO ';
	if	(ie_tipo_w	= 'A') then
		open C03;
		loop
		fetch C03 into	
			c03_w;
		exit when C03%notfound;
			begin
			
			ds_valor_w	:= null;
			
			if	(c03_w.IE_ASHWORTH_D is not null) then
				ds_valor_w	:= 'D '||c03_w.IE_ASHWORTH_D||'  ';
			end if;
			
			if	(c03_w.IE_ASHWORTH_E is not null) then
				ds_valor_w	:= ds_valor_w||'E '||c03_w.IE_ASHWORTH_E||'  ';
			end if;
			
			ds_valor_w	:= trim(ds_valor_w);
			
			ds_parametro_w	:= 'ds_valor='||ds_valor_w||'#@#@nm_usuario='||nm_usuario_p||'#@#@NR_SEQ_ART_MOV_MUSCULO='||c03_w.NR_SEQ_ART_MOV_MUSCULO||'#@#@';
			exec_sql_dinamico_bv('TASY', ds_comando_w,ds_parametro_w);
			
			
			end;
		end loop;
		close C03;
		
	elsif	(ie_tipo_w	= 'T') then
		open C04;
		loop
		fetch C04 into	
			c04_w;
		exit when C04%notfound;
			begin
			
			
			select	count(*)
			into	qt_reg_w
			from	ATEND_TOXINA_ITEM
			where	nr_seq_atendimento	= nr_seq_registro_w
			and	NR_SEQ_ART_MOV_MUSCULO	= c04_w.NR_SEQ_ART_MOV_MUSCULO;
			
			ds_valor_w	:= null;
			
			if	(qt_reg_w	> 1) then
			
				NR_SEQ_ART_MOV_MUSCULO_w	:= c04_w.NR_SEQ_ART_MOV_MUSCULO;
				
				open C05;
				loop
				fetch C05 into	
					c05_w;
				exit when C05%notfound;
					begin
					ds_valor_w	:= ds_valor_w||c05_w.ie_lado||' '||c05_w.qt_dose||'  '; 
					
					end;
				end loop;
				close C05;
			
			else
			
				ds_valor_w	:= nvl(c04_w.ie_lado,'A')||' '||c04_w.qt_dose||'  '; 
			
			end if;
			
			ds_valor_w	:= trim(ds_valor_w);
			
			
			ds_parametro_w	:= 'ds_valor='||ds_valor_w||'#@#@nm_usuario='||nm_usuario_p||'#@#@NR_SEQ_ART_MOV_MUSCULO='||c04_w.NR_SEQ_ART_MOV_MUSCULO||'#@#@';
			exec_sql_dinamico_bv('TASY', ds_comando_w,ds_parametro_w);
			end;
		end loop;
		close C04;
	
	end if;
	
	end;
end loop;	

commit;


end gerar_grid_espasticidade;
/
