create or replace
procedure send_diagnosis_integration(nr_seq_interno_p number, 
										nr_atendimento_p number,
										cd_doenca_p varchar2,
										dt_diagnostico_p date,
										ie_classificacao_doenca_p varchar2,
										ie_situacao_p varchar2,
										dt_liberacao_p date,
										dt_inativacao_p date,
										dt_atualizacao_p date) is 
								
visit_id_w 					diagnostico_doenca.nr_atendimento%type;
patient_id_w 				atendimento_paciente.cd_pessoa_fisica%type;
diagnosis_id_w				diagnostico_doenca.nr_seq_interno%type;
diagnosis_origin_code_w		varchar2(30);
diagnosis_code_w			diagnostico_doenca.cd_doenca%type;
diagnosis_date_w			varchar2(30);
is_main_diagnosis_w			diagnostico_doenca.ie_classificacao_doenca%type;
diagnosis_event_w			diagnostico_doenca.ie_situacao%type;
diagnosis_created_on_w		varchar2(30);
updatetimestampfilter_w 	diagnostico_doenca.dt_atualizacao%type;
nm_user_w               	diagnostico_doenca.nm_usuario%type;
ds_integracao_w				clob;
establishment_id_w   number(4);

begin

if(obter_setor(obter_setor_atual_paciente(nr_atendimento_p)) is not null) then

	select 	max(nr_atendimento_p) visit_id, 
			max(nr_seq_interno_p) diagnosis_id,
      obter_estabelecimento_setor(obter_setor_atual_paciente(max(nr_atendimento_p))) establishment_id,  
			obter_setor_origem_atend(max(nr_atendimento_p)) diagnosis_origin_code,  
			max(cd_doenca_p) diagnosis_code, 
			to_char(max(dt_diagnostico_p), 'yyyymmddhh24miss') diagnosis_date, 
			decode(upper(max(ie_classificacao_doenca_p)),'P',1,2) is_main_diagnosis, 
			decode(upper(max(ie_situacao_p)),'A',1,2) diagnosis_event, 
			to_char(max(dt_atualizacao_p), 'yyyymmddhh24miss') diagnosis_created_on,
			to_date(max(dt_atualizacao_p)) updatetimestampfilter
	into 	visit_id_w,
			diagnosis_id_w,
      establishment_id_w,
			diagnosis_origin_code_w,
			diagnosis_code_w,
			diagnosis_date_w,
			is_main_diagnosis_w,
			diagnosis_event_w,
			diagnosis_created_on_w,
			updatetimestampfilter_w
	from dual 
	where dt_liberacao_p is not null
	and dt_inativacao_p  is null;
	
	nm_user_w := WHEB_USUARIO_PCK.GET_NM_USUARIO;
	patient_id_w := OBTER_PESSOA_ATENDIMENTO(nr_atendimento_p,'C');
	
	if (patient_id_w is not null) and
	   (visit_id_w is not null) then
	
		SELECT BIFROST.SEND_INTEGRATION( 'patientinformation.diagnosis',
										'com.philips.tasy.integration.atepac.patientinformation.diagnosis.Diagnosis',
										'{ "patientId" : "' ||patient_id_w || '",'
										||'"visitId" : "' ||visit_id_w || '",'
                    ||'"establishmentId" : "'||establishment_id_w||'",'
										||'"diagnosisId" : "' ||diagnosis_id_w || '",'
										|| '"diagnosisOriginCode" : "' || diagnosis_origin_code_w ||'",'
										|| '"diagnosisCode" : "' || diagnosis_code_w ||'" ,'
										|| '"diagnosisDate" : "' || diagnosis_date_w ||'" ,'
										|| '"isMainDiagnosis" : "' || is_main_diagnosis_w ||'" ,'
										|| '"diagnosisEvent" : "' || diagnosis_event_w ||'" ,'
										|| '"diagnosisCreatedOn" : "' || diagnosis_created_on_w ||'" ,'
										|| '"updatetimestampfilter" : "' || updatetimestampfilter_w ||'"'
										|| '}',
										nm_user_w)
			INTO DS_INTEGRACAO_W
			FROM DUAL;   
	end if;		
	
end if;

end send_diagnosis_integration;
/
