create or replace
procedure desdobrar_requisicao_consig(
			nr_requisicao_p		number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			nr_desdobrada_p	out	number) is 
	
cd_operacao_estoque_w	number(3);
nr_requisicao_w		number(10);
nr_seq_item_w		number(10);
qt_itens_desdobrar_w	number(10);
ie_consignado_w		varchar2(1);
ie_entrada_saida_w	varchar2(1);
ie_tipo_requisicao_w	varchar2(3);
ie_tipo_perda_w		varchar2(3);
	
begin

select	cd_operacao_estoque
into	cd_operacao_estoque_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

select	decode(ie_consignado,null,'N','0','N','S'),
	ie_tipo_requisicao,
	ie_entrada_saida,
	ie_tipo_perda
into	ie_consignado_w,
	ie_tipo_requisicao_w,
	ie_entrada_saida_w,
	ie_tipo_perda_w
from	operacao_estoque
where	cd_operacao_estoque = cd_operacao_estoque_w;

if	(ie_tipo_requisicao_w = '1') then
	if	(ie_consignado_w = 'S') then
		select	decode(nvl(ie_tipo_perda_w,'0'),'0',cd_operacao_consumo_setor,cd_oper_perda_cons)
		into	cd_operacao_estoque_w
		from	parametro_estoque
		where	cd_estabelecimento = cd_estabelecimento_p;
	else
		select	decode(nvl(ie_tipo_perda_w,'0'),'0',cd_operacao_cons_consignado,cd_oper_perda_cons_con)
		into	cd_operacao_estoque_w
		from	parametro_estoque
		where	cd_estabelecimento = cd_estabelecimento_p;
	end if;
elsif	(ie_tipo_requisicao_w in ('2','21')) then
	if	(ie_consignado_w = 'S') then
		select	cd_operacao_transf_setor
		into	cd_operacao_estoque_w
		from	parametro_estoque
		where	cd_estabelecimento = cd_estabelecimento_p;
	else
		select	cd_operacao_transf_setor_consi
		into	cd_operacao_estoque_w
		from	parametro_estoque
		where	cd_estabelecimento = cd_estabelecimento_p;
	end if;
end if;

select	count(*)
into	qt_itens_desdobrar_w
from	item_requisicao_material
where	nr_requisicao = nr_requisicao_p
and	decode(obter_dados_material(cd_material,'CON'),'0','N','1','S','2',decode(cd_cgc_fornecedor,null,'N','S')) <> ie_consignado_w;

if	(qt_itens_desdobrar_w > 0) then

	select	requisicao_seq.nextval
	into	nr_requisicao_w
	from	dual;

	nr_desdobrada_p := nr_requisicao_w;

	insert into requisicao_material(
		cd_centro_custo,		cd_estabelecimento,		cd_estabelecimento_destino,
		cd_local_estoque,		cd_local_estoque_destino,	cd_operacao_estoque,
		cd_pessoa_requisitante,		cd_pessoa_solicitante,		cd_setor_atendimento,
		cd_setor_entrega,		ds_observacao,			dt_atualizacao,
		dt_atualizacao_nrec,		dt_emissao_loc_estoque,		dt_emissao_setor,
		dt_solicitacao_requisicao,	ie_geracao,			ie_origem_requisicao,
		ie_urgente,			nm_usuario,			nm_usuario_aprov,
		nm_usuario_nrec,		nm_usuario_recebedor,		nr_adiantamento,
		nr_atendimento,			nr_cirurgia,			nr_documento_externo,
		nr_prescricao,			nr_requisicao,			nr_requisicao_orig,
		nr_seq_inv_roupa,		nr_seq_justificativa,		nr_seq_ordem_serv,
		nr_seq_proj_gpi,		nr_seq_protocolo)
	select	cd_centro_custo,		cd_estabelecimento,		cd_estabelecimento_destino,
		cd_local_estoque,		cd_local_estoque_destino,	cd_operacao_estoque_w,
		cd_pessoa_requisitante,		cd_pessoa_solicitante,		cd_setor_atendimento,
		cd_setor_entrega,		ds_observacao,			dt_atualizacao,
		dt_atualizacao_nrec,		dt_emissao_loc_estoque,		dt_emissao_setor,
		dt_solicitacao_requisicao,	ie_geracao,			ie_origem_requisicao,
		ie_urgente,			nm_usuario,			nm_usuario_aprov,
		nm_usuario_nrec,		nm_usuario_recebedor,		nr_adiantamento,
		nr_atendimento,			nr_cirurgia,			nr_documento_externo,
		nr_prescricao,			nr_requisicao_w,		nr_requisicao,
		nr_seq_inv_roupa,		nr_seq_justificativa,		nr_seq_ordem_serv,
		nr_seq_proj_gpi,		nr_seq_protocolo
	from	requisicao_material
	where	nr_requisicao = nr_requisicao_p;

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_item_w
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_w;
	
	insert into item_requisicao_material(
		cd_cgc_fornecedor,		cd_conta_contabil,		cd_estabelecimento,
		cd_kit_material,		cd_material,			cd_material_lido,
		cd_material_req,		cd_processo_aprov,		cd_unidade_medida,
		cd_unidade_medida_estoque,	ds_justificativa,		ds_observacao,
		dt_atualizacao,			ie_acao,			ie_geracao,
		ie_msg_estoque_max,		nm_usuario,			nm_usuario_aprov,
		nr_documento_externo,		nr_item_docto_externo,		nr_requisicao,
		nr_seq_aprovacao,		nr_seq_cor_exec,		nr_seq_etapa_gpi,
		nr_seq_kit_estoque,		nr_seq_lote_fornec,		nr_seq_proc_aprov,
		nr_sequencia,			qt_estoque,			qt_estoque_superou,
		qt_material_atendida,		qt_material_req_auto,		qt_material_requisitada,
		qt_saldo_estoque,		qt_saldo_estoque_dest,		vl_material,
		vl_preco_venda,			vl_unit_previsto,		nr_seq_justificativa)
	select	cd_cgc_fornecedor,		cd_conta_contabil,		cd_estabelecimento,
		cd_kit_material,		cd_material,			cd_material_lido,
		cd_material_req,		cd_processo_aprov,		cd_unidade_medida,
		cd_unidade_medida_estoque,	ds_justificativa,		ds_observacao,
		dt_atualizacao,			ie_acao,			ie_geracao,
		ie_msg_estoque_max,		nm_usuario,			nm_usuario_aprov,
		nr_documento_externo,		nr_item_docto_externo,		nr_requisicao_w,
		nr_seq_aprovacao,		nr_seq_cor_exec,		nr_seq_etapa_gpi,
		nr_seq_kit_estoque,		nr_seq_lote_fornec,		nr_seq_proc_aprov,
		nr_seq_item_w + rownum,		qt_estoque,			qt_estoque_superou,
		qt_material_atendida,		qt_material_req_auto,		qt_material_requisitada,
		qt_saldo_estoque,		qt_saldo_estoque_dest,		vl_material,
		vl_preco_venda,			vl_unit_previsto,		nr_seq_justificativa
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p
	and	decode(obter_dados_material(cd_material,'CON'),'0','N','1','S','2',decode(cd_cgc_fornecedor,null,'N','S')) <> ie_consignado_w;

	delete	from item_requisicao_material
	where	nr_requisicao = nr_requisicao_p
	and	decode(obter_dados_material(cd_material,'CON'),'0','N','1','S','2',decode(cd_cgc_fornecedor,null,'N','S')) <> ie_consignado_w;
end if;
	
commit;

end desdobrar_requisicao_consig;
/