create or replace
procedure Rxt_Obter_Proced_Fat(
			cd_convenio_p			number,
			ie_tipo_lancamento_p		varchar2,
			nr_seq_proc_interno_p	out	number,
			cd_procedimento_p	out	number,
			ie_origem_proced_p	out	number,
			nm_usuario_p			Varchar2) is 

nr_seq_proc_interno_w	number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
			
Cursor C01 is
	select	nr_Seq_proc_interno,
		cd_procedimento,
		ie_origem_proced
	from	rxt_proced_faturamento
	where	nvl(cd_convenio, cd_convenio_p)	= cd_convenio_p
	and	ie_tipo_lancamento		= ie_tipo_lancamento_p
	and	ie_situacao			= 'A'
	order by nvl(cd_convenio,0);			
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_proc_interno_w,
	cd_procedimento_w,
	ie_origem_proced_w;
exit when C01%notfound;
	begin
	nr_seq_proc_interno_w	:= nr_seq_proc_interno_w;
	cd_procedimento_w	:= cd_procedimento_w;
	ie_origem_proced_w	:= ie_origem_proced_w;
	end;
end loop;
close C01;

nr_seq_proc_interno_p	:= nr_seq_proc_interno_w;
cd_procedimento_p	:= cd_procedimento_w;
ie_origem_proced_p	:= ie_origem_proced_w;

end Rxt_Obter_Proced_Fat;
/