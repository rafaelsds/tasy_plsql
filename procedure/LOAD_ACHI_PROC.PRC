create or replace PROCEDURE LOAD_ACHI_PROC AS

	qt_registro_procedimento_w NUMBER;

	qt_registro_proc_versao_w  NUMBER;

	cd_code_id_w               NUMBER(7) ;

	ds_error_w                 VARCHAR2(2000) ;

	get_seq_procedimento_w     NUMBER(15) ;

	cd_grupo_proc_w            NUMBER(15) ;

	cd_procedimento_w          NUMBER(15) ;

	cd_mbs_w                   VARCHAR2(5) ;

	cd_mbs_exist_w             NUMBER(1) ;

	cd_procedimento_mbs_w      NUMBER(15) ;

  

  cursor c01 is

	select * from w_achiload_proc;

  r1 c01%rowtype;

BEGIN 

open c01;

loop

fetch c01 into r1;

  if c01%notfound then

      update mbs_import_log

      set dt_end = sysdate

      where ie_status = 'IACHI'

      and dt_end is null;

      exit;

  end if;

BEGIN  

	BEGIN

    gravar_processo_longo('Importing item '||r1.ds_code_id||' - '||substr(r1.ds_ascii_desc,1,255),'INSERT_ACHI_PROCEDIMENTO',null);

		SELECT cd_grupo_proc

		INTO cd_grupo_proc_w

		FROM grupo_proc

		WHERE cd_original   =r1.cd_block

		AND IE_SITUACAO='A'

		AND ie_origem_proced=19;

		

		SELECT COUNT(*)

		INTO qt_registro_procedimento_w

		FROM procedimento

		WHERE cd_procedimento_loc     =r1.ds_code_id

		and CD_GRUPO_PROC=cd_grupo_proc_w

		AND ie_origem_proced          =19;

		

		IF(cd_grupo_proc_w            >0) THEN

			IF(qt_registro_procedimento_w=0) THEN

				BEGIN

					SELECT procedimento_seq.nextval

					INTO get_seq_procedimento_w

					FROM dual;

					INSERT

					INTO procedimento

						(

							cd_procedimento,

							cd_procedimento_loc,

							cd_grupo_proc,

							ds_procedimento,

							ie_situacao,

							ie_sexo_sus,

							qt_idade_minima_sus,

							qt_idade_maxima_sus,

							ie_alta_complexidade,

							ie_ativ_prof_bpa,

							ie_localizador,

							ie_classificacao,

							ie_origem_proced,

							dt_atualizacao,

							nm_usuario,

							ie_gera_associado,

							ie_assoc_adep,

							ie_classif_custo,

							qt_exec_barra,

							ie_cobra_adep,

							ie_ignora_origem,

							ie_exige_autor_sus

						)

						VALUES

						(

							get_seq_procedimento_w,

							r1.ds_code_id,

							cd_grupo_proc_w,

							SUBSTR(r1.ds_ascii_desc, 1, 255),

							DECODE(r1.dt_inactive_from, NULL, 'A', 'I'),

							DECODE(r1.ie_sexo, 1, 'M', 2, 'F', 'E'),

							r1.cd_agel,

							r1.cd_ageh,

							'N',

							'N',

							'S',

							1,

							19,

							sysdate,

							r1.nm_usuario,

							'S',

							'S',

							'B',

							1,

							'S',

							'N',

							'N'

						) ;

					cd_mbs_w:=SUBSTR(r1.ds_code_id, 1, 5) ;

					SELECT COUNT(*)

					INTO cd_mbs_exist_w

					FROM procedimento

					WHERE cd_procedimento_loc=cd_mbs_w

					AND ie_origem_proced     =20;

					IF(cd_mbs_exist_w        =1) THEN

						SELECT cd_procedimento

						INTO cd_procedimento_mbs_w

						FROM procedimento

						WHERE cd_procedimento_loc=cd_mbs_w

						AND ie_origem_proced     =20;

						INSERT

						INTO procedimento_conv_sus

							(

								nr_sequencia,

								dt_atualizacao,

								nm_usuario,

								cd_procedimento,

								ie_origem_proced,

								cd_procedimento_sus,

								ie_origem_proced_sus,

								ie_tipo_atendimento,

								ie_clinica,

								cd_procedimento_loc

							)

							VALUES

							(

								procedimento_conv_sus_seq.nextval,

								sysdate,

								r1.nm_usuario,

								cd_procedimento_mbs_w,

								20,

								get_seq_procedimento_w,

								19,

								NULL,

								NULL,

								r1.ds_code_id

							) ;

					END IF;

				END;

			ELSE

				UPDATE procedimento

				SET ie_situacao          ='A',

					nm_usuario              =r1.nm_usuario,

					dt_atualizacao          =sysdate,

					ds_procedimento         =SUBSTR(r1.ds_ascii_desc, 1, 255)

				WHERE cd_procedimento_loc=r1.ds_code_id

				and CD_GRUPO_PROC=cd_grupo_proc_w

				AND ie_origem_proced     =19;

			END IF;

		END IF;

	END;

	BEGIN

		SELECT cd_procedimento

		INTO cd_procedimento_w

		FROM procedimento

		WHERE cd_procedimento_loc=r1.ds_code_id

		and CD_GRUPO_PROC=cd_grupo_proc_w

		AND ie_origem_proced     =19;

		SELECT COUNT(*)

		INTO qt_registro_proc_versao_w

		FROM procedimento_versao

		WHERE cd_procedimento       =cd_procedimento_w

		AND ie_origem_proced        =19;

		IF(qt_registro_proc_versao_w=0) THEN

			BEGIN

				INSERT

				INTO procedimento_versao

					(

						nr_sequencia,

						dt_versao,

						dt_atualizacao,

						nm_usuario,

						dt_atualizacao_nrec,

						nm_usuario_nrec,

						dt_vigencia_inicial,

						dt_vigencia_final,

						cd_procedimento,

						ie_origem_proced,

						cd_versao

					)

					VALUES

					(

						procedimento_versao_seq.nextval,

						NULL,

						sysdate,

						r1.nm_usuario,

						sysdate,

						r1.nm_usuario,

						r1.dt_effective_from,

						r1.dt_inactive_from,

						get_seq_procedimento_w,

						19,

						NULL

					) ;

			END;

		ELSE

			UPDATE procedimento_versao

			SET nm_usuario       =r1.nm_usuario,

				dt_atualizacao      =sysdate,

				dt_vigencia_inicial =r1.dt_effective_from,

				dt_vigencia_final   =r1.dt_inactive_from

			WHERE cd_procedimento=cd_procedimento_w

			AND ie_origem_proced =19;

		END IF;

	END;

EXCEPTION

when others then

	ds_error_w:= 'error ==>  '|| sqlerrm(sqlcode) || ' occurred for code id  ' || r1.ds_code_id ;

    insert into mbs_import_log(dt_start,ie_status,ds_error) values(sysdate,'EACHI',ds_error_w);

	ds_error_w := null;

 end;          

end loop;

delete w_achiload_proc;

close c01;

commit;

END load_achi_proc;
/