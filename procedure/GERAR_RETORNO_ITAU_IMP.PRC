create or replace
procedure GERAR_RETORNO_ITAU_IMP
		(nr_seq_banco_escrit_p	in	number,
		nm_usuario_p		in	varchar2) is

ds_nr_titulo_w		varchar2(255);
ds_dt_pagamento_w	varchar2(255);
ds_vl_pagamento_w	varchar2(255);
ds_nr_documento_w	varchar2(255);
ds_ocorrencia_w		varchar2(255);

cd_reg_favorecido_w	varchar2(50);
cd_conf_envio_w		varchar2(50);
cd_retorno_liq_w	varchar2(50);

nr_titulo_w		number(10,0);
cd_tipo_baixa_w		number(10,0);
nr_seq_trans_escrit_w	number(10,0);
nr_seq_conta_banco_w	number(10,0);
nr_sequencia_w		number(10,0);
dt_pagamento_w		date;
vl_pagamento_w		number(15,2);
nr_documento_w		varchar2(255);
ds_forma_pagto_w	varchar2(50);

cd_estabelecimento_w		number(10,0);
nr_sequencia_inicial_w		number(10,0);
nr_sequencia_final_w		number(10,0);
nr_seq_interf_w			number(10,0);
cd_erro_w			varchar2(2);
cd_banco_w			number(5);
vl_saldo_titulo_w			number(15,2);
nr_seq_trans_fin_baixa_w		number(10);

qt_titulo_rec_cobr_w		number(15);

cursor c01 is
select	nr_sequencia,
	substr(ds_conteudo,12,2) ds_forma_pagto
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '1'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
order	by nr_sequencia;

cursor c02 is
select	nr_sequencia,
	substr(ds_conteudo,74,20) ds_nr_titulo,
	substr(ds_conteudo,155,8) ds_dt_pagamento,
	substr(ds_conteudo,163,15) ds_vl_pagamento,
	substr(ds_conteudo,198,6) ds_nr_documento,
	substr(ds_conteudo,231,10) ds_ocorrencia
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
and	ds_forma_pagto_w		in ('03','01','41')
and	nr_sequencia			> nr_sequencia_inicial_w
and	nr_sequencia			< nr_sequencia_final_w		-- pagamento em doc
union
select	nr_sequencia,
	substr(ds_conteudo,183,20) ds_nr_titulo,
	substr(ds_conteudo,145,8) ds_dt_pagamento,
	substr(ds_conteudo,153,15) ds_vl_pagamento,
	'' ds_nr_documento,
	substr(ds_conteudo,231,10) ds_ocorrencia
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
and	ds_forma_pagto_w		in ('30','31')
and	nr_sequencia			> nr_sequencia_inicial_w
and	nr_sequencia			< nr_sequencia_final_w		-- pagamento com bloqueto
union					
select	nr_sequencia,
	substr(ds_conteudo,175,20) ds_nr_titulo,
	substr(ds_conteudo,137,8) ds_dt_pagamento,
	substr(ds_conteudo,122,15) ds_vl_pagamento,
	'' ds_nr_documento,
	substr(ds_conteudo,231,10) ds_ocorrencia
from	w_interf_retorno_itau
where	substr(ds_conteudo, 8, 1)	= '3'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
and	ds_forma_pagto_w		in ('13')
and	nr_sequencia			> nr_sequencia_inicial_w
and	nr_sequencia			< nr_sequencia_final_w
order	by nr_sequencia;						-- pagamento de concessionárias

/* cursor	c03 is
select	a.cd_erro
from	erro_escritural a
where	a.cd_banco	= cd_banco_w; */

begin


select	max(b.cd_reg_favorecido),
	max(b.cd_conf_envio),
	max(b.cd_retorno_liq),
	max(a.cd_estabelecimento),
	max(a.nr_seq_conta_banco),
	max(a.cd_banco)
into	cd_reg_favorecido_w,
	cd_conf_envio_w,
	cd_retorno_liq_w,
	cd_estabelecimento_w,
	nr_seq_conta_banco_w,
	cd_banco_w
from	banco_retorno_cp b,
	banco_escritural a
where	a.cd_banco		= b.cd_banco(+)
and	a.nr_sequencia		= nr_seq_banco_escrit_p;

select	max(nr_seq_trans_escrit)
into	nr_seq_trans_escrit_w
from	parametro_tesouraria
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(max(cd_tipo_baixa_padrao),1)
into	cd_tipo_baixa_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

open c01;
loop
fetch c01 into
	nr_sequencia_inicial_w,
	ds_forma_pagto_w;
exit when c01%notfound;

	begin

	select	min(nr_sequencia)
	into	nr_sequencia_final_w
	from	w_interf_retorno_itau
	where	substr(ds_conteudo, 8, 1)	<> '3'
	and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p
	and	nr_sequencia			> nr_sequencia_inicial_w;

	open c02;
	loop 
	fetch c02 into
		nr_seq_interf_w,
		ds_nr_titulo_w,
		ds_dt_pagamento_w,
		ds_vl_pagamento_w,
		ds_nr_documento_w,
		ds_ocorrencia_w;
	exit when c02%notfound;

		select	max(a.nr_titulo),
			to_number(obter_saldo_titulo_pagar(max(a.nr_titulo),sysdate)),
			max(a.nr_seq_trans_fin_baixa)
		into	nr_titulo_w,
			vl_saldo_titulo_w,
			nr_seq_trans_fin_baixa_w
		from	titulo_pagar a
		where	a.nr_titulo	= to_number(ds_nr_titulo_w);

		if	(nr_titulo_w is not null) and (vl_saldo_titulo_w <> 0) then

			dt_pagamento_w		:= to_date(ds_dt_pagamento_w, 'ddmmyyyy');
			vl_pagamento_w		:= to_number(ds_vl_pagamento_w);
			vl_pagamento_w		:= dividir_sem_round(vl_pagamento_w, 100);
			nr_documento_w		:= ds_nr_documento_w;
   		
			gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);

			if 	(cd_reg_favorecido_w = ds_ocorrencia_w) then
				update	titulo_pagar_escrit
				set	ds_erro			= ds_ocorrencia_w
				where	nr_seq_escrit		= nr_seq_banco_escrit_p
				and	nr_titulo		= nr_titulo_w;
			elsif	(cd_conf_envio_w = ds_ocorrencia_w) then
				update	titulo_pagar_escrit
				set	ds_erro			= ds_ocorrencia_w
				where	nr_seq_escrit		= nr_seq_banco_escrit_p
				and	nr_titulo		= nr_titulo_w;
			elsif	(cd_retorno_liq_w = ds_ocorrencia_w) then

				dbms_output.put_line('nr_titulo_w = ' || nr_titulo_w);

				baixa_titulo_pagar
						(cd_estabelecimento_w,
						cd_tipo_baixa_w,
						nr_titulo_w,
						vl_pagamento_w,
						nm_usuario_p,
						nvl(nr_seq_trans_fin_baixa_w,nr_seq_trans_escrit_w),
						null,
						nr_seq_banco_escrit_p,
						dt_pagamento_w,
						nr_seq_conta_banco_w);

				select	max(nr_sequencia)
				into	nr_sequencia_w
				from	titulo_pagar_baixa
				where	nr_titulo	= nr_titulo_w;

				gerar_movto_tit_baixa
						(nr_titulo_w,
						nr_sequencia_w,
						'P',
						nm_usuario_p,
						'N');

				atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
				Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
			end if;
		
			update	titulo_pagar_escrit
			set	ds_erro			= ds_ocorrencia_w
			where	nr_seq_escrit		= nr_seq_banco_escrit_p
			and	nr_titulo		= nr_titulo_w;

		end if;

	end loop;
	close c02;


	exception
	when others then
		rollback;
		delete	from w_interf_retorno_itau
		where	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;
		commit;
		/*r.aise_application_error(-20011, sqlerrm || chr(13) ||
					'nr_titulo_w = ' || nr_titulo_w || chr(13) ||
					'ds_nr_titulo_w = ' || ds_nr_titulo_w || chr(13) ||
					'ds_dt_pagamento_w = ' || ds_dt_pagamento_w || chr(13) ||
					'ds_vl_pagamento_w = ' || ds_vl_pagamento_w || chr(13) ||
					'ds_nr_documento_w = ' || ds_nr_documento_w || chr(13) ||
					'ds_ocorrencia_w = ' || ds_ocorrencia_w); */
		wheb_mensagem_pck.exibir_mensagem_abort(267380,	'nr_titulo_w=' || nr_titulo_w || ';' ||
								'ds_nr_titulo_w=' || ds_nr_titulo_w || ';' ||
								'ds_dt_pagamento_w=' || ds_dt_pagamento_w || ';' ||
								'ds_vl_pagamento_w=' || ds_vl_pagamento_w || ';' ||
								'ds_nr_documento_w=' || ds_nr_documento_w || ';' ||
								'ds_ocorrencia_w=' || ds_ocorrencia_w);			

	end;



end loop;
close c01;

Select 	count(*)
into	qt_titulo_rec_cobr_w
from	titulo_pagar_escrit
where	nr_seq_escrit = nr_seq_banco_escrit_p;

if (qt_titulo_rec_cobr_w > 0) then
	
	update	banco_escritural
	set	dt_baixa	= sysdate
	where	nr_sequencia	= nr_seq_banco_escrit_p;

end if;

delete	from w_interf_retorno_itau
where	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

commit;

end GERAR_RETORNO_ITAU_IMP;
/