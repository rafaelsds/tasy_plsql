create or replace
procedure gerar_evolucao_cirurgia(	nr_cirurgia_p		number,
					nr_sequencia_p		number,
					nm_usuario_p		varchar2,
					nr_seq_pepo_p		number default null)
					is

cd_evolucao_w		number(10,0);
nr_atendimento_w	number(10,0);
ie_tipo_evolucao_w	varchar2(3);
ds_diagnostico_w	varchar2(2000);
ds_resumo_cirurgia_w	varchar2(2000);
ds_diagnostico_pos_w	varchar2(2000);
ds_exame_rad_w		varchar2(2000);
ds_exame_anatomo_w	varchar2(2000);
ds_cirurgia_w		varchar2(32000);
cd_responsavel_w	varchar2(10);
dt_liberacao_w		date;
qt_controle_chr_w	number(10,0);
ds_pos_inicio_rtf_w	number(10,0);
ds_resumo_w		varchar2(32000);
ds_conteudo_w		varchar2(32000);
ie_evolucao_clinica_w	varchar2(3);
cd_estabelecimento_w	number(4,0);
cd_pessoa_fisica_w	varchar2(10);
ie_html_w			varchar2(1);
qt_tamanho_w		number(10);
qt_posicao_w		number(10);
cd_perfil_w			perfil.cd_perfil%type := wheb_usuario_pck.get_cd_perfil;
ds_achados_operatorios_w   	varchar2(2000);
ds_intercorrencia_w               	varchar2(2000);
ds_po_imediato_w                 	varchar2(2000);


expressao1_w	varchar2(255) := obter_desc_expressao_idioma(287710, null, wheb_usuario_pck.get_nr_seq_idioma);--Diagn�stico pr�-operat�rio
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(774087, null, wheb_usuario_pck.get_nr_seq_idioma);--Cirurgia resumida
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(287709, null, wheb_usuario_pck.get_nr_seq_idioma);--Diagn�stico p�s-operat�rio
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(289718, null, wheb_usuario_pck.get_nr_seq_idioma);--Exame radiol�gico
expressao5_w	varchar2(255) := obter_desc_expressao_idioma(289686, null, wheb_usuario_pck.get_nr_seq_idioma);--Exame anatomopatol�gico
expressao6_w         	varchar2(255) := obter_desc_expressao_idioma(283146, null, wheb_usuario_pck.get_nr_seq_idioma);--Achados operat�rios
expressao7_w         	varchar2(255) := obter_desc_expressao_idioma(292148, null, wheb_usuario_pck.get_nr_seq_idioma);--Intercorr�ncia
expressao8_w  	varchar2(255) := obter_desc_expressao_idioma(730815, null, wheb_usuario_pck.get_nr_seq_idioma);--P�s operat�rio imediato

begin

if	(nvl(nr_cirurgia_p,0) > 0) then
	select	nvl(max(cd_estabelecimento),0)
	into		cd_estabelecimento_w
	from		cirurgia
	where		nr_cirurgia	=	nr_cirurgia_p;
else	
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
end if;	

obter_param_usuario(872,53,cd_perfil_w,nm_usuario_p,cd_estabelecimento_w,ie_evolucao_clinica_w);	

select	cd_responsavel,
		ds_diagnostico,
		ds_resumo_cirurgia,
		ds_diagnostico_pos,
		ds_exame_rad,
		ds_exame_anatomo,
		ds_cirurgia,
		dt_liberacao,
		ds_achados_operatorios,
		ds_intercorrencia,
		ds_po_imediato
into	cd_responsavel_w,
		ds_diagnostico_w,
		ds_resumo_cirurgia_w,
		ds_diagnostico_pos_w,
		ds_exame_rad_w,
		ds_exame_anatomo_w,
		ds_cirurgia_w,
		dt_liberacao_w,
		ds_achados_operatorios_w,
    		ds_intercorrencia_w,
    		ds_po_imediato_w
from	cirurgia_descricao
where	nr_sequencia	=	nr_sequencia_p;

if	(nvl(nr_cirurgia_p,0) > 0) then
	select	max(nr_atendimento),
				max(cd_pessoa_fisica)
	into		nr_atendimento_w,
				cd_pessoa_fisica_w
	from		cirurgia
	where		nr_cirurgia	=	nr_cirurgia_p;
else	
	select	max(nr_atendimento),
				max(cd_pessoa_fisica)
	into		nr_atendimento_w,
				cd_pessoa_fisica_w
	from		pepo_cirurgia
	where		nr_sequencia	=	nr_seq_pepo_p;
end if;	

select	max(ie_tipo_evolucao)
into	ie_tipo_evolucao_w
from	usuario
where	nm_usuario = nm_usuario_p;

if	(ie_tipo_evolucao_w is null) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(231834);
	--r a i s e _application_error(-20011,'Tipo de evolu��o n�o informado para este usu�rio');
end if;

if	(cd_pessoa_fisica_w is not null) then
	select	evolucao_paciente_seq.nextval
	into	cd_evolucao_w
	from	dual;
	 
	if (instr(ds_cirurgia_w,'{\rtf') = 0) then	
		ds_cirurgia_w := wheb_rtf_pck.get_cabecalho ||chr(13)||chr(10)|| replace(ds_cirurgia_w, chr(13)||chr(10), chr(13)||chr(10)|| wheb_rtf_pck.get_quebra_linha) ||chr(13)||chr(10)|| wheb_rtf_pck.get_rodape;	
	end if;

	ds_resumo_w :=	substr((expressao1_w	|| chr(13) ||
			substr(ds_diagnostico_w,1,2000)	|| chr(13) || chr(13) ||
			expressao2_w 		|| chr(13) ||
			substr(ds_resumo_cirurgia_w,1,2000) 	|| chr(13) || chr(13) ||
			expressao3_w 	|| chr(13) ||
			substr(ds_diagnostico_pos_w,1,2000)	|| chr(13) || chr(13) ||
			expressao4_w 		|| chr(13) ||
			substr(ds_exame_rad_w,1,2000)	|| chr(13) || chr(13) ||
			expressao5_w 	|| chr(13) ||
			substr(ds_exame_anatomo_w,1,2000)	|| chr(13) || chr(13) ||
      			expressao6_w  	|| chr(13) ||
      			substr(ds_achados_operatorios_w,1,2000) || chr(13) || chr(13) ||
      			expressao7_w 	|| chr(13) ||
      			substr(ds_intercorrencia_w,1,2000) 	|| chr(13) || chr(13) ||
      			expressao8_w  	|| chr(13) ||
      			substr(ds_po_imediato_w,1,2000) 	|| chr(13) || chr(13)),1,32000);

	if (instr(upper(ds_cirurgia_w),'<HTML TASY="HTML5">') > 0)  then
		ie_html_w	:= 'S';
	else 
		ie_html_w	:= 'N';
	end if;
			
	if	(nvl(ie_html_w,'N') = 'S') then
		qt_tamanho_w := length(ds_cirurgia_w);
		qt_posicao_w := instr(upper(ds_cirurgia_w), '<HTML TASY="HTML5">');
		ds_cirurgia_w := substr(ds_cirurgia_w, qt_posicao_w,qt_tamanho_w);
		qt_posicao_w := instr(upper(ds_cirurgia_w), '</HTML>');
		ds_cirurgia_w := substr(ds_cirurgia_w, 1, qt_posicao_w-1);
		
		ds_conteudo_w := '<html tasy="html5">' || ds_resumo_w || ds_cirurgia_w ||  '</html>';
	else 
		if	(ds_cirurgia_w is not null) then
			/*INICIO Substituir os caracteres CHR(13) e CHR(10) pelo \par que que representa o ENTER no RTF*/
			qt_controle_chr_w :=0;
					
			while	( instr(ds_resumo_w,chr(13)) > 0 ) and 
					( qt_controle_chr_w < 100 ) loop
					ds_resumo_w := replace(ds_resumo_w,chr(13),'\par ');
					qt_controle_chr_w := qt_controle_chr_w + 1;
			end loop;

			qt_controle_chr_w := 0;

			while	( instr(ds_resumo_w,chr(10)) > 0 ) and 
					( qt_controle_chr_w < 100 ) loop
					ds_resumo_w := replace(ds_resumo_w,chr(10),'');
					qt_controle_chr_w := qt_controle_chr_w + 1;
			end loop;
			/*FIM Substituir os caracteres CHR(13) e CHR(10) pelo \par que que representa o ENTER no RTF*/
			
			/*Pega o cabecalho do RTF*/
			
			if	((instr(ds_cirurgia_w,'viewkind4\uc1\pard\qj\cf1\f0\fs20') > 0) or 
				(instr(ds_cirurgia_w,'viewkind4\uc1\pard\cf1\f0\fs16') > 0)  or
				(instr(ds_cirurgia_w,'viewkind4\uc1\pard\cf1\fs') > 0)) then
				begin /*tratamento para quando � inserido via meu texto padrao para nao quebrar o RTF, visto que quando usa APENAS o texto padrao, o RTF fica noutro padrao*/
				ds_conteudo_w := '{\rtf1\ansi\deff0{\fonttbl{\f0\fnil Arial;}{\f1\fnil\fcharset0 Arial;}}' ||
								'{\colortbl ;\red0\green0\blue0;}' ||
								'\viewkind4\uc1\pard\cf1\lang1046\f0\fs20 ';
									
				if (instr(ds_cirurgia_w,'viewkind4\uc1\pard\qj\cf1\f0\fs20') > 0) then
					ds_pos_inicio_rtf_w := instr(ds_cirurgia_w, 'viewkind4\uc1\pard\qj\cf1\f0\fs20') + 34;
				elsif (instr(ds_cirurgia_w,'viewkind4\uc1\pard\cf1\f0\fs16') > 0) then
					ds_pos_inicio_rtf_w := instr(ds_cirurgia_w, 'viewkind4\uc1\pard\cf1\f0\fs16') + 31;
				elsif (instr(ds_cirurgia_w,'viewkind4\uc1\pard\cf1\fs') > 0) then
					ds_pos_inicio_rtf_w := instr(ds_cirurgia_w, 'viewkind4\uc1\pard\cf1\fs') + 28;
				end if;
				
				end;
			elsif ((instr(ds_cirurgia_w,'{\rtf1\ansi\') = 1) and (instr(ds_cirurgia_w,'lang') = 0)) then
				begin
				ds_pos_inicio_rtf_w := 12;
				ds_conteudo_w := substr(ds_cirurgia_w,1,ds_pos_inicio_rtf_w) || 'fs20 ';
				end;
			else /* esta parte ficou como era antes, para n�o impactar nos clientes que usam corretamente*/
				begin
				ds_pos_inicio_rtf_w := instr(ds_cirurgia_w,'lang')+8;
				ds_conteudo_w := substr(ds_cirurgia_w,1,ds_pos_inicio_rtf_w) || 'fs20 ';
				end;
			end if;
					
			/*Acrecenta conteudo texto livre*/
			ds_conteudo_w := ds_conteudo_w || ds_resumo_w;
			/*Acrecenta resto do conteudo do RTF*/
			ds_conteudo_w := ds_conteudo_w || '\par '|| substr(ds_cirurgia_w,ds_pos_inicio_rtf_w,length(ds_cirurgia_w));
		else 
			ds_conteudo_w := ds_resumo_w; 
		end if;
	end if;
	
	insert into evolucao_paciente (
		cd_evolucao,
		dt_evolucao,
		ie_tipo_evolucao,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		ds_evolucao,
		cd_medico,
		dt_liberacao,
		ie_evolucao_clinica,
		cd_especialidade_medico,
		ie_recem_nato,
		ie_situacao,
		nr_cirurgia,
		nr_seq_pepo)
	values (
		cd_evolucao_w,
		sysdate,
		ie_tipo_evolucao_w,
		cd_pessoa_fisica_w,
		sysdate,
		nm_usuario_p,
		nr_atendimento_w,
		ds_conteudo_w,
		cd_responsavel_w,
		dt_liberacao_w,
		ie_evolucao_clinica_w,
		obter_especialidade_medico(cd_responsavel_w, 'C'),
		'N',
		'A',
		nr_cirurgia_p,
		nr_seq_pepo_p);
		
	update	cirurgia_descricao
	set	cd_evolucao 	= cd_evolucao_w
	where	nr_sequencia	= nr_sequencia_p;
	
			
	commit;
end if;	


end gerar_evolucao_cirurgia;
/
