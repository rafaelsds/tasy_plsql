create or replace
procedure San_atualizar_agrup_reserva (	nr_seq_agupamento_p	number,	
					lista_informacao_p	varchar2,
					nm_usuario_p		varchar2) is

lista_informacao_w		varchar2(400);
ie_contador_w			number(10,0)	:= 0;
tam_lista_w			number(10,0);
ie_pos_virgula_w		number(3,0);
nr_sequencia_w			number(10,0);
nr_seq_reserva_prod_w		number(10,0);

begin

lista_informacao_w	:= lista_informacao_p;

while	lista_informacao_w is not null or
	ie_contador_w > 200 loop
	begin
	tam_lista_w		:= length(lista_informacao_w);
	ie_pos_virgula_w	:= instr(lista_informacao_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_reserva_prod_w	:= substr(lista_informacao_w,1,(ie_pos_virgula_w - 1));
		lista_informacao_w	:= substr(lista_informacao_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;

	
	update 	san_reserva_prod
	set 	nr_agrupamento 	= nr_seq_agupamento_p
	where	nr_sequencia 	= nr_seq_reserva_prod_w;
	
	ie_contador_w	:= ie_contador_w + 1;
	
	end;
end loop;

end San_atualizar_agrup_reserva;
/
