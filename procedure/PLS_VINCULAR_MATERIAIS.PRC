create or replace 
procedure pls_vincular_materiais (
	ds_materiais_p		varchar2,
	ie_tipo_despesa_p		varchar2,
	nr_seq_estrut_mat_p	number,
	cd_estabelecimento_p	number,
	nm_usuario_p		varchar2) is

ds_material_w			varchar2(1000); 	/* Conjunto de identificadores  */
ie_pos_virgula_w			number(3,0); 	/* posicao da virgula */
tam_lista_w			number(10,0); 	/* tamanha da lista de identificadores */
cd_material_w			number(10,0); 	/* identificador atual */
nr_controle_loop_w			number(3,0) := 0;	/* controle do loop */

begin
if	(ds_materiais_p is not null) then
	begin
	ds_material_w := ds_materiais_p;
	
	while	(ds_material_w is not null) and
		(nr_controle_loop_w < 100) loop
		begin
		tam_lista_w	:= length(ds_material_w);
		ie_pos_virgula_w	:= instr(ds_material_w,',');
		cd_material_w	:= 0;

		if	(ie_pos_virgula_w <> 0) then
			begin
			cd_material_w	:= to_number(substr(ds_material_w,1,(ie_pos_virgula_w - 1)));
			ds_material_w	:= substr(ds_material_w,(ie_pos_virgula_w + 1),tam_lista_w);
			end;
		else
			begin
			cd_material_w	:= to_number(ds_material_w);
			ds_material_w	:= null;
			end;
		end if;
		
		if	(nvl(cd_material_w,0) > 0) then
			begin
			pls_vincular_material(cd_material_w, ie_tipo_despesa_p, nr_seq_estrut_mat_p, cd_estabelecimento_p, nm_usuario_p);
			end;
		end if;
		
		nr_controle_loop_w := nr_controle_loop_w + 1;
		end;
	end loop;
	end;
end if;
commit;
end pls_vincular_materiais;
/