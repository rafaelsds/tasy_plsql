create or replace
procedure pls_rescindir_pf_contrato_vinc
			(	nr_seq_segurado_p	number,
				nr_seq_motivo_p		number,
				dt_rescisao_p		date,
				dt_limite_utilizacao_p	date,
				ie_tipo_rescisao_p	varchar2,
				nr_seq_causa_rescisao_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 
			
nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
dt_contratacao_w		date;
nr_seq_vinculo_sca_w		number(10);
dt_liberacao_w			date;
ie_situacao_atend_w		varchar2(10);
nr_seq_contrato_vinc_w		number(10);
qt_beneficiarios_w		number(10);
qt_benef_inativos_w		number(10);
ie_gerar_validade_cartao_w	varchar2(10);
ie_situacao_atend_aux_w		pls_segurado.ie_situacao_atend%type;
	
Cursor C01 is
	select	b.nr_sequencia,
		a.nr_sequencia,
		b.ie_situacao_atend
	from	pls_segurado		b,
		pls_contrato		a
	where	b.nr_seq_contrato	= a.nr_sequencia
	and	a.nr_contrato_principal	= nr_seq_contrato_w
	and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	b.dt_rescisao is null;
	
Cursor C02 is
	select	a.nr_sequencia,
		a.dt_liberacao
	from	pls_sca_vinculo a,
		pls_segurado b,
		pls_contrato c
	where	b.nr_sequencia	= a.nr_seq_segurado
	and	c.nr_sequencia	= b.nr_seq_contrato
	and	b.nr_sequencia	= nr_seq_segurado_w;
	
begin

select	nr_seq_contrato,
	cd_pessoa_fisica
into	nr_seq_contrato_w,
	cd_pessoa_fisica_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	max(ie_gerar_validade_cartao)
into	ie_gerar_validade_cartao_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_gerar_validade_cartao_w is null) then
	ie_gerar_validade_cartao_w	:= 'S';
end if;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w,
	nr_seq_contrato_vinc_w,
	ie_situacao_atend_aux_w;
exit when C01%notfound;
	begin
	
	if	(trunc(dt_limite_utilizacao_p,'dd') >= trunc(sysdate,'dd')) and
		(ie_situacao_atend_aux_w <> 'S') then
		ie_situacao_atend_w	:= 'A';
	elsif	((dt_limite_utilizacao_p < sysdate) or
		(ie_situacao_atend_aux_w = 'S')) then
		ie_situacao_atend_w	:= 'I';
	end if;
	
	select	dt_contratacao
	into	dt_contratacao_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_w;
	
	if	(trunc(dt_contratacao_w,'dd') > trunc(dt_rescisao_p,'dd')) then
		wheb_mensagem_pck.exibir_mensagem_abort(196219);
	else
		/* Rescindir o segurado */
		update	pls_segurado
		set	dt_rescisao			= dt_rescisao_p,
			dt_limite_utilizacao		= dt_limite_utilizacao_p,
			nm_usuario			= nm_usuario_p,
			dt_atualizacao			= sysdate,
			nr_seq_motivo_cancelamento 	= nr_seq_motivo_p,
			ie_situacao_atend		= ie_situacao_atend_w,
			ie_tipo_rescisao		= ie_tipo_rescisao_p,
			dt_reativacao			= null
		where	nr_sequencia			= nr_seq_segurado_w;

		/* Rescindir a carteirinha */
		update	pls_segurado_carteira
		set	dt_validade_carteira	= dt_limite_utilizacao_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_seq_segurado		= nr_seq_segurado_w
		and	dt_validade_carteira is null
		and	ie_gerar_validade_cartao_w	= 'S';


		/* Rescindir SCA's */ 
		open C02;
		loop
		fetch C02 into	
			nr_seq_vinculo_sca_w,
			dt_liberacao_w;
		exit when C02%notfound;
			begin
		
			if	(dt_liberacao_w is not null) then
				pls_rescindir_sca_benef(nr_seq_vinculo_sca_w,dt_rescisao_p,'N',nm_usuario_p);
			end if;	
		
			end;
		end loop;
		close C02;

		/* Gerar hist�rico */
		pls_gerar_segurado_historico(	nr_seq_segurado_w, '1', dt_rescisao_p, 'Rescis�o',
						'Rescis�o do benefici�rio atr�ves do seu contrato principal', null, null, null,
						nr_seq_motivo_p, dt_rescisao_p, null, null,
						null, null, null, null,
						nm_usuario_p, 'N');			
	end if;
	
	/* Verificar se ap�s rescindir os benefici�rios vai ser rescindido o contrato tamb�m */
	if	(nr_seq_contrato_vinc_w is not null) then
		pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_contrato_vinc_w, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);

		
		select	count(*)
		into	qt_beneficiarios_w
		from	pls_segurado
		where	nr_seq_contrato	= nr_seq_contrato_vinc_w;
		
		select	count(*)
		into	qt_benef_inativos_w
		from	pls_segurado
		where	nr_seq_contrato	= nr_seq_contrato_vinc_w
		and	dt_liberacao is not null
		and	dt_rescisao is not null;
		
		if	(qt_beneficiarios_w > 0) and
			(qt_benef_inativos_w = qt_beneficiarios_w) then
			update	pls_contrato
			set	dt_rescisao_contrato	= dt_rescisao_p,
				ie_situacao		= '3',
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate,
				nr_seq_motivo_rescisao	= nr_seq_motivo_p,
				dt_limite_utilizacao	= dt_limite_utilizacao_p,
				nr_seq_causa_rescisao	= nr_seq_causa_rescisao_p
			where	nr_sequencia		= nr_seq_contrato_vinc_w;		
		end if;
	end if;
	
	end;
end loop;
close C01;

end pls_rescindir_pf_contrato_vinc;
/
