create or replace
procedure pls_oc_cta_tratar_val_30( 	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de materiais n�o habilitados para o prestador. Esta valida��o ir� verificar 
	o cadastro de materiais do prestador, fun��o OPS - Prestador \ pasta Prestadores \ pasta 
	Servi�os e Materiais \ pasta Materiais.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_table_selecao_ocor_w	pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_gera_ocorrencia_w		varchar2(1);
nr_count_w			pls_integer;

-- Mateirias da tabela de sele��oq ue n�o est�o habilitados para o prestador quandoa  valida��o estiver sendo feita na importa��o do arquivo
cursor cs_mat_n_hab_imp(	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is
	select	sel.nr_sequencia nr_seq_selecao,
		(select	count(1)
		from	table (pls_grupos_aux_pck.obter_materiais_lib_prest(	mat.nr_seq_prestador_exec_imp,
										mat.nr_seq_prest_fornec_imp,
										mat.dt_atendimento_imp,
										mat.ie_tipo_internado,
										prest.nr_seq_tipo_prestador,
										mat.nr_seq_material)) pm) qt_mat_lib,
		mat.nr_seq_prestador_exec_imp nr_seq_prestador_exec_imp,
		mat.nr_seq_prest_fornec_imp nr_seq_prest_fornec_imp
	from	pls_oc_cta_selecao_ocor_v	sel,
		pls_conta_mat_ocor_v		mat,
		pls_prestador			prest
	where	sel.nr_id_transacao		= nr_id_transacao_pc
	and	sel.ie_valido			= 'S'
	and	mat.nr_sequencia		= sel.nr_seq_conta_mat
	and	mat.nr_seq_material		is not null
	and	prest.nr_sequencia		= mat.nr_seq_prestador_exec_imp;
			
-- Materiais da tabela de sele��o que n�o est�o habilitados para o prestador quando a valida��o for feita no TASY.
cursor cs_mat_n_hab(	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is	
	select	sel.nr_sequencia nr_seq_selecao,
		(select	count(1)
		from	table (pls_grupos_aux_pck.obter_materiais_lib_prest(	mat.nr_seq_prestador_exec,
										mat.nr_seq_prest_fornec,
										mat.dt_atendimento,
										mat.ie_tipo_internado,
										prest.nr_seq_tipo_prestador,
										mat.nr_seq_material)) pm) qt_mat_lib,
		mat.nr_seq_prestador_exec nr_seq_prestador_exec,
		mat.nr_seq_prest_fornec nr_seq_prest_fornec
	from	pls_oc_cta_selecao_ocor_v	sel,
		pls_conta_mat_ocor_v		mat,
		pls_prestador			prest
	where	sel.nr_id_transacao		= nr_id_transacao_pc
	and	sel.ie_valido			= 'S'
	and	mat.nr_sequencia		= sel.nr_seq_conta_mat
	and	mat.nr_seq_material		is not null
	and	prest.nr_sequencia		= mat.nr_seq_prestador_exec;
			
-- Informa��es da regra de valida��o dos materiais n�o habilitados.
cursor C02 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_valida_mat_prestador
	from	pls_oc_cta_val_mat_prest a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

begin

-- S� ir� executar qualquer copnsulta e\ou valida��o quando existir informa��o v�lida da regra.
if	(dados_regra_p.nr_sequencia is not null) then

	-- usa uma tabela para armazenar as estruturas dos materiais
	pls_gerencia_upd_obj_pck.atualizar_objetos(nm_usuario_p, 'PLS_OC_CTA_TRATAR_VAL_30', 'PLS_ESTRUTURA_MATERIAL_TM');
	
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
	
	-- Informa��es da regra de valida��o da carteira.
	for	r_C02_w in C02(dados_regra_p.nr_sequencia) loop

		-- Se tiver informa��o na tabela e a informa��o for diferente de nenhuma ser� verificado, caso contr�rio sai da rotina e nem abre as informa��es das contas.
		if	(r_C02_w.ie_valida_mat_prestador = 'S') then

			nr_count_w := 0;
			-- Quando a consist�ncia for executada durante a importa��o do arquivo ent�o o acesso as tabela muda e estamos  usando outro cursor.
			-- Favor quando alterar qualquer um dos dois cursores verificar se n�o deve ser alterado o outro tamb�m.
			if	(dados_regra_p.ie_evento = 'IMP') then

				-- Buscar os materiais que n�o est�o habilitados para o prestador da conta.
				for 	r_cs_mat_n_hab_imp_w in cs_mat_n_hab_imp( nr_id_transacao_p) loop

					ie_gera_ocorrencia_w := 'N';

					if	( r_cs_mat_n_hab_imp_w.qt_mat_lib = 0) then
						ie_gera_ocorrencia_w := 'S';
					end if;

					if 	( ie_gera_ocorrencia_w = 'S') then 

						dados_table_selecao_ocor_w.nr_seq_selecao(nr_count_w) := r_cs_mat_n_hab_imp_w.nr_seq_selecao;
						dados_table_selecao_ocor_w.ie_valido(nr_count_w) := 'S';
						dados_table_selecao_ocor_w.ds_observacao(nr_count_w) := pls_oc_cta_obs_val_30(	r_cs_mat_n_hab_imp_w.nr_seq_prestador_exec_imp,	pls_obter_dados_prestador(r_cs_mat_n_hab_imp_w.nr_seq_prestador_exec_imp, 'N'),
															r_cs_mat_n_hab_imp_w.nr_seq_prest_fornec_imp, pls_obter_dados_prestador(r_cs_mat_n_hab_imp_w.nr_seq_prest_fornec_imp, 'N'));

						if	( nr_count_w >= pls_util_pck.qt_registro_transacao_w) then
						
							pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_table_selecao_ocor_w.nr_seq_selecao, pls_tipos_ocor_pck.clob_table_vazia,
													'SEQ', dados_table_selecao_ocor_w.ds_observacao, 
													dados_table_selecao_ocor_w.ie_valido, nm_usuario_p);
							pls_tipos_ocor_pck.limpar_nested_tables(dados_table_selecao_ocor_w);
							nr_count_w := 0;
						else
							nr_count_w := nr_count_w + 1;
						end if;
					end if;
				end loop; -- cs_mat_n_hab_imp
			else
				
				-- Buscar os materiais que n�o est�o habilitados para o prestador da conta.
				for 	r_cs_mat_n_hab_w in cs_mat_n_hab( nr_id_transacao_p) loop

					ie_gera_ocorrencia_w := 'N';

					if	( r_cs_mat_n_hab_w.qt_mat_lib = 0) then
						ie_gera_ocorrencia_w := 'S';
					end if;

					if 	( ie_gera_ocorrencia_w = 'S') then 

						dados_table_selecao_ocor_w.nr_seq_selecao(nr_count_w) := r_cs_mat_n_hab_w.nr_seq_selecao;
						dados_table_selecao_ocor_w.ie_valido(nr_count_w) := 'S';
						dados_table_selecao_ocor_w.ds_observacao(nr_count_w) := pls_oc_cta_obs_val_30(	r_cs_mat_n_hab_w.nr_seq_prestador_exec,	pls_obter_dados_prestador(r_cs_mat_n_hab_w.nr_seq_prestador_exec, 'N'),
															r_cs_mat_n_hab_w.nr_seq_prest_fornec, pls_obter_dados_prestador(r_cs_mat_n_hab_w.nr_seq_prest_fornec, 'N'));

						if	( nr_count_w >= pls_util_pck.qt_registro_transacao_w) then

							pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_table_selecao_ocor_w.nr_seq_selecao, pls_tipos_ocor_pck.clob_table_vazia,
													'SEQ', dados_table_selecao_ocor_w.ds_observacao, 
													dados_table_selecao_ocor_w.ie_valido, nm_usuario_p);
							pls_tipos_ocor_pck.limpar_nested_tables(dados_table_selecao_ocor_w);
							nr_count_w := 0;
						else
							nr_count_w := nr_count_w + 1;
						end if;
					end if;
				end loop; -- cs_mat_n_hab
			end if;
		end if;
	end loop; -- C02

	pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_table_selecao_ocor_w.nr_seq_selecao, pls_tipos_ocor_pck.clob_table_vazia,
							'SEQ', dados_table_selecao_ocor_w.ds_observacao, 
							dados_table_selecao_ocor_w.ie_valido, nm_usuario_p);
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;
end pls_oc_cta_tratar_val_30;
/