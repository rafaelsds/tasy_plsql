create or replace PROCEDURE nut_orient_notas_link_actions (
    ie_action_p               VARCHAR2,
    nr_atendimento_p          atendimento_paciente.nr_atendimento%TYPE,
    nr_seq_orient_pad_not_p   nut_orient_padrao_notas.nr_sequencia%TYPE default null,
    ds_orientacao_p           nut_orientacao.ds_orientacao%TYPE default null,
    nm_usuario_p              VARCHAR2 default wheb_usuario_pck.get_nm_usuario
) IS
/*
ie_action_p:
C (Check - Insert)
U (Uncheck - Delete)
O (OK - Delete all)
R (reload all)
*/
    nr_sequencia_w              nut_orient_pad_not_linkage.nr_sequencia%TYPE;
    nr_seq_orient_pad_not_w     nut_orient_padrao_notas.nr_sequencia%TYPE;
    ds_texto_adicional_label_w  varchar2(200) := obter_desc_expressao(299345);
    ie_texto_adicional_w        number(2);
    
    cursor c_titulos is
    select regexp_substr(ds_orientacao_p,'[^;]+', 1, level) ds_titulo from dual 
    connect by regexp_substr(ds_orientacao_p, '[^;]+', 1, level) is not null;

BEGIN
    SELECT
        nut_orient_pad_not_linkage_seq.NEXTVAL
    INTO nr_sequencia_w
    FROM
        dual;

    CASE ie_action_p
        WHEN 'C' THEN
            INSERT INTO nut_orient_pad_not_linkage (
                nr_sequencia,
                nr_atendimento,
                nr_seq_orient_pad_not,
                ds_texto_adicional,
                nm_usuario_nrec,
                nm_usuario,
                dt_atualizacao_nrec,
                dt_atualizacao
            ) VALUES (
                nr_sequencia_w,
                nr_atendimento_p,
                nr_seq_orient_pad_not_p,
                NULL,
                nm_usuario_p,
                nm_usuario_p,
                SYSDATE,
                SYSDATE);

        WHEN 'U' THEN
            DELETE nut_orient_pad_not_linkage
            WHERE
                nr_atendimento = nr_atendimento_p
                AND nr_seq_orient_pad_not = nr_seq_orient_pad_not_p;

        WHEN 'O' THEN
            DELETE nut_orient_pad_not_linkage
            WHERE
                nr_atendimento = nr_atendimento_p;

        WHEN 'R' THEN
            DELETE nut_orient_pad_not_linkage
            WHERE
                nr_atendimento = nr_atendimento_p;
            BEGIN
              FOR c_titulos_row IN c_titulos
              LOOP
                IF (c_titulos_row.ds_titulo IS NOT NULL OR c_titulos_row.ds_titulo <> '') THEN
                    select INSTR(c_titulos_row.ds_titulo, ds_texto_adicional_label_w) 
                    into ie_texto_adicional_w 
                    from dual;
                    
                    IF ie_texto_adicional_w = 1 THEN
                        UPDATE nut_orient_pad_not_linkage SET
                            ds_texto_adicional = SUBSTR(REPLACE(c_titulos_row.ds_titulo, ds_texto_adicional_label_w),3)
                        WHERE nr_sequencia = nr_sequencia_w;
                    
                    ELSE                    
                        SELECT
                            nut_orient_pad_not_linkage_seq.NEXTVAL
                        INTO nr_sequencia_w
                        FROM
                            dual;
                        
                        SELECT
                            MAX(nr_sequencia)
                        INTO nr_seq_orient_pad_not_w
                        FROM
                            nut_orient_padrao_notas
                        WHERE
                            ds_titulo = c_titulos_row.ds_titulo;
                                        
                        INSERT INTO nut_orient_pad_not_linkage (
                            nr_sequencia,
                            nr_atendimento,
                            nr_seq_orient_pad_not,
                            ds_texto_adicional,
                            nm_usuario_nrec,
                            nm_usuario,
                            dt_atualizacao_nrec,
                            dt_atualizacao
                        ) VALUES (
                            nr_sequencia_w,
                            nr_atendimento_p,
                            nr_seq_orient_pad_not_w,
                            NULL,
                            nm_usuario_p,
                            nm_usuario_p,
                            SYSDATE,
                            SYSDATE);
                    END IF;
                END IF;
              END LOOP;
            END;        

    END CASE;

END nut_orient_notas_link_actions;
/
