create or replace
procedure sup_integracao_lote(	nr_prescricao_p		number,
				nm_usuario_p		Varchar2) is 

cd_material_w		number(6);
qt_dispensar_hor_w		number(13,4);
nr_seq_lote_w		number(10);
cd_setor_atendimento_w	number(5);
ds_setor_atendimento_w	varchar2(255);
cd_unidade_basica_w	varchar2(30);
nm_paciente_w		varchar2(255);
dt_liberacao_w		date;
cd_operacao_estoque_w	varchar2(5);
ie_branco_w		varchar2(5);
ds_observacao_w		varchar2(255);
cd_pessoa_fisica_w	varchar2(10);
cd_unidade_medida_w 	varchar2(30);
ds_justificativa_w      varchar2(2000);
ie_forma_integracao_w	varchar2(1);
nr_sequencia_lote_w	number(10);
cd_local_estoque_w	number(4);
ds_local_estoque_w	varchar2(40);	
nr_seq_turno_w		number(10);
ds_turno_w		varchar2(80);
ds_classif_urgente_w	varchar2(80);
dt_dispensacao_w		date;
hr_dispensacao_w		date;
qt_dose_w              		number(12,4);
ds_leito_w		varchar2(100);
nr_prescricao_w		number(14);

Cursor C01 is
	select 	a.qt_dispensar_hor,
		c.qt_dose,
		c.cd_unidade_medida,
		substr(c.ds_observacao,1,255),
		c.cd_local_estoque,
		substr(obter_desc_local_estoque(c.cd_local_estoque),1,80) ds_loca_estoque,
		substr(obter_desc_turno_disp(l.nr_seq_turno),1,80) ds_turno,
		substr(obter_desc_classif_lote(l.nr_seq_classif),1,100) ds_classif,
		l.dt_atend_lote,
		l.dt_atend_lote,
		l.nr_seq_turno,
		l.nr_sequencia,
		l.cd_setor_atendimento,
		substr(obter_nome_setor(l.cd_setor_atendimento),1,100) ds_setor_atendimento,
		substr(obter_nome_pf(b.cd_pessoa_fisica),1,60) nm_paciente,
		b.cd_pessoa_fisica,
		b.dt_liberacao,
		c.cd_material,
		substr(Obter_Unidade_Atendimento(b.nr_atendimento,'A','UB')||Obter_Unidade_Atendimento(b.nr_atendimento,'A','UC'),1,80) ds_leito,
		b.nr_prescricao
	from    prescr_mat_hor a,
		prescr_medica b,
		prescr_material c,
		estrutura_material_v s,
		material e,
		ap_lote l
	where   a.nr_seq_lote is not null
	and	a.cd_material = s.cd_material
	and     a.nr_prescricao = b.nr_prescricao
	and     c.nr_prescricao = b.nr_prescricao
	and     a.nr_seq_material = c.nr_sequencia
	and     a.nr_prescricao = nr_prescricao_p
	and     a.cd_material = e.cd_material
	and     a.nr_seq_lote = l.nr_sequencia
	and     nvl(l.ie_integracao,'N') = 'N'
	order by a.nr_seq_lote;

begin

open C01;
loop
fetch C01 into	
	qt_dispensar_hor_w,
	qt_dose_w,
	cd_unidade_medida_w,
	ds_observacao_w,
	cd_local_estoque_w,
	ds_local_estoque_w,
	ds_turno_w,
	ds_classif_urgente_w,
	dt_dispensacao_w,
	hr_dispensacao_w,
	nr_seq_turno_w,
	nr_seq_lote_w,
	cd_setor_atendimento_w,
	ds_setor_atendimento_w,
	nm_paciente_w,
	cd_pessoa_fisica_w,
	dt_liberacao_w,
	cd_material_w,
	ds_leito_w,
	nr_prescricao_w;
exit when C01%notfound;
	begin
	
	if	(nvl(nr_seq_lote_w,0) > 0) then
		begin
		insert into sup_int_lote_prescr(
				nr_sequencia, 
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_material,
				qt_dispensar_hor,
				nr_seq_lote,
				cd_setor_atendimento,
				ds_setor_atendimento,
				cd_unidade_medida,
				nm_pessoa_fisica,
				cd_pessoa_fisica,
				dt_liberacao,
				cd_operacao_estoque,
				ie_lido,
				ds_observacao,
				cd_local_estoque,
				ds_local_estoque,
				nr_seq_turno,
				ds_turno,
				ds_classif_urgente,
				dt_dispensacao,
				hr_dispensacao,
				ds_leito,
				nr_prescricao)
		values	(	sup_int_lote_prescr_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_material_w,
				qt_dispensar_hor_w,
				nr_seq_lote_w,
				cd_setor_atendimento_w,
				ds_setor_atendimento_w,
				cd_unidade_medida_w,
				nm_paciente_w,
				cd_pessoa_fisica_w,
				dt_liberacao_w,
				'O',
				'',
				substr(ds_observacao_w,1,255),
				cd_local_estoque_w,
				ds_local_estoque_w,
				nr_seq_turno_w,
				ds_turno_w,
				ds_classif_urgente_w,
				dt_dispensacao_w,
				hr_dispensacao_w,
				ds_leito_w,
				nr_prescricao_w);
		end;	
	end if;	
	end;
end loop;
close C01;



commit;

end sup_integracao_lote;
/