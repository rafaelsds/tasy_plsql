create or replace
procedure Consiste_Minimo_Multiplo_Req
		(cd_material_p	in	number,
		qt_requisitada_p	in	number,
		ds_erro_p		out	varchar2) is

qt_minimo_multiplo_w		number(15,4);
qt_fracao_w			number(15,4);
qt_multiplo_w			number(15,4);

begin

ds_erro_p		:= '';
select	max(qt_minimo_multiplo_solic)
into	qt_minimo_multiplo_w
from	material
where	cd_material		= cd_material_p;


qt_multiplo_w	:= trunc(dividir(qt_requisitada_p, qt_minimo_multiplo_w),0);
qt_fracao_w	:= qt_requisitada_p - (qt_multiplo_w * qt_minimo_multiplo_w);

if	(qt_fracao_w <> 0) then
	ds_erro_p  := 	WHEB_MENSAGEM_PCK.get_texto(279249) || qt_minimo_multiplo_w ||
			WHEB_MENSAGEM_PCK.get_texto(279250) || qt_fracao_w || ' )';
end if;

end Consiste_Minimo_Multiplo_Req;
/