create or replace procedure hlm_gerar_w_rel_poc(nr_atendimento_p 	number) is

ds_classif_w		varchar2(120);
ds_proced_w		varchar2(120);
ds_freq_w		varchar2(120);
ds_modelo_w		varchar2(120);
ie_tipo_atendimento_w 	number(5);
cd_pessoa_fisica_w 	varchar2(200);
ie_possui_cid_w 	varchar2(1);
cd_doenca_sus_w 	varchar2(200);
cd_doenca_w 		varchar2(200);
qt_count_agenda_w 	number(5);
nr_seq_trat_reab_w 	number(10);
nr_seq_trat_of_w 	number(10);
qt_freq_agenda_w	number(5);
dt_entrada_w		varchar2(100);


/*
CREATE TABLE hlm_rel_proj_oficina(ds_classif VARCHAR2(200),
		ds_proced VARCHAR2(200),
		ds_freq VARCHAR2(200),
		ds_modelo VARCHAR2(200),
		nm_usuario VARCHAR2(100));
*/

Cursor C01 is
/*	select 	cd_doenca_cid
	from 	atendimento_cid_v
	where 	nr_atendimento = nr_atendimento_p;*/

	select nvl(max(a.cd_doenca),'x')
	from diagnostico_doenca a, atendimento_paciente_v b, pessoa_fisica c
	where a.nr_atendimento = b.NR_ATENDIMENTO
	and b.CD_PESSOA_FISICA = c.cd_pessoa_fisica
	--and a.nr_atendimento = 2413
	and c.cd_pessoa_fisica = cd_pessoa_fisica_w;

begin

delete hlm_rel_proj_oficina where nm_usuario = wheb_usuario_pck.get_nm_usuario;


select	ie_tipo_atendimento,
	to_char(dt_entrada,'mm/yyyy')
into	ie_tipo_atendimento_w,
	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

if (ie_tipo_atendimento_w = 1) then

	--se atendimento internado
	ds_modelo_w :=	'Modelo: ';
	ds_proced_w := '';
	ds_freq_w := '';
	ds_classif_w:= 'CLASSIF.: AIH';

else

select 	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

open C01;
loop
fetch C01 into
	cd_doenca_w;
exit when C01%notfound;
	begin
	if 	(cd_doenca_w <> 'x') then
		ie_possui_cid_w := 'S';
	-- precisa ter um CID
		select	nvl(max(a.cd_doenca_cid),'x')
		into	cd_doenca_sus_w
		from    cid_doenca b,
			sus_procedimento_cid a
		where	a.cd_doenca_cid      = b.cd_doenca_cid
		and	a.cd_procedimento    in ('0301070075','0301070121','0301070067','0301070130')
		and	a.ie_origem_proced   = 7
		and	a.cd_doenca_cid = cd_doenca_w
		order by a.cd_doenca_cid;
		--tabela unificada do sus (precisa ser o mesmo CID do atend)
		--insert into vieira values('teste');
		ds_proced_w := 'x';
		ds_classif_w := 'x';

		if (cd_doenca_sus_w = 'x') then
			ds_proced_w := 'PROCED.: 0301070091';
			ds_classif_w := 'CLASSIF.: BPA-C';
		end if;
	--se n�o tiver:
	--Campo Proced: 0301070091
	-- BPA-C
	else
		ds_proced_w := 'PROCED.: 0301070091';
		ds_classif_w := 'CLASSIF.: BPA-C';
		ie_possui_cid_w := 'N';
		--insert into vieira values('teste2');
	end if;


/*	else
		ds_proced_w := 'x';
		ds_classif_w := 'x';
	end if;			*/
	end;
end loop;
close C01;

	if (ds_classif_w = 'x') and (ds_proced_w = 'x') then

		--insert into vieira values('teste3');
		select	count(distinct cd_agenda)
		into	qt_count_agenda_w
		from	agenda_consulta
		where	ie_status_agenda <> 'C'
		and	cd_pessoa_fisica = cd_pessoa_fisica_w
		and 	to_char(dt_agendamento,'mm/yyyy') = dt_entrada_w;

		if (cd_doenca_sus_w <> 'x') then

			--insert into vieira values('teste4');
			select 	nvl(max(b.nr_sequencia),0)
			into 	nr_seq_trat_reab_w
			from 	rp_tratamento a,
				rp_paciente_reabilitacao b
			where 	nr_seq_tipo_tratamento = 1 --Reabilita��o
			and 	a.nr_seq_pac_reav = b.nr_sequencia
			and 	cd_pessoa_fisica = cd_pessoa_fisica_w;
			-- 2 ou mais agendamentos em agendas diferentes


			select 	nvl(max(b.nr_sequencia),0)
			into 	nr_seq_trat_of_w
			from 	rp_tratamento a,
				rp_paciente_reabilitacao b
			where 	nr_seq_tipo_tratamento = 2 --Oficina
			and 	a.nr_seq_pac_reav = b.nr_sequencia
			and 	cd_pessoa_fisica = cd_pessoa_fisica_w;

			if  (qt_count_agenda_w >= 2) or (nr_seq_trat_reab_w > 0) then
				--insert into vieira values('teste5');
				ds_classif_w := 'CLASSIF.: BPA-I';
			else
				ds_classif_w := 'CLASSIF.: BPA-C';
			end if;
		end if;



		if 	(cd_doenca_sus_w <> 'x') and
			(ds_classif_w = 'CLASSIF.: BPA-I') then
			 --se for o mesmo CID do atend,BPA-I e


				if 	(nr_seq_trat_of_w > 0) then
				--entao PROCED:
					select  'PROCED.: 0' || max(a.cd_procedimento)
					into 	ds_proced_w
					from	cid_doenca b,
						sus_procedimento_cid a
					where	a.cd_doenca_cid      = b.cd_doenca_cid
					and     a.cd_procedimento    in ('0301070075','0301070121','0301070067','0301070130')
					and     a.ie_origem_proced   = 7
					and     a.cd_doenca_cid = cd_doenca_w
					order by a.cd_doenca_cid;

				end if;
		end if;

		if 	(ie_possui_cid_w = 'S') and
			(qt_count_agenda_w = 1) and
			(nr_seq_trat_of_w > 0) then

			ds_proced_w := 'PROCED.: 0301070091';
			-- tem cid informado e apenas 1 agendamento
			-- Campo Proced: 0301070091;
		end if;
		if 	(ie_possui_cid_w = 'S') and
			(qt_count_agenda_w >= 1) and
			(nr_seq_trat_reab_w > 0) then

		ds_proced_w := 'PROCED.: 0301070121';
		end if;
	end if;
--end if;
	SELECT	COUNT(nr_atendimento)
	into	qt_freq_agenda_w
	FROM	agenda_consulta
	WHERE	cd_pessoa_fisica =  cd_pessoa_fisica_w
	AND	nr_seq_rp_mod_item IS NOT NULL
	and 	to_char(dt_agendamento,'mm/yyyy') = dt_entrada_w;

	if (qt_freq_agenda_w >= 1) then
	/*if (qt_count_agenda_w > 1) then*/
		ds_freq_w := 'FREQ.: S';
	else
		ds_freq_w := 'FREQ.: N';
	end if;

	if (ds_classif_w = 'CLASSIF.: BPA-C') then
		ds_freq_w := '';
	end if;

	-- se count >=1 FREQ:S
	-- senao : N

	--Se BPA-C entao FREQ : ''
end if;

	if (ds_proced_w = 'x') then ds_proced_w := '';
	end if;
	if (ds_classif_w = 'x') then ds_classif_w := '';
	end if;

	insert into hlm_rel_proj_oficina(ds_classif,
					ds_proced,
					ds_freq,
					ds_modelo,
					nm_usuario)
			  values(	ds_classif_w,
					ds_proced_w,
					ds_freq_w,
					ds_modelo_w,
					wheb_usuario_pck.get_nm_usuario);

commit;

end hlm_gerar_w_rel_poc;
/
