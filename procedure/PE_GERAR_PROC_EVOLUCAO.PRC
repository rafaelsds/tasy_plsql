Create or Replace
PROCEDURE Pe_Gerar_Proc_Evolucao(
                         	nr_seq_evolucao_p		Number,
					qt_hora_inicio_p		Number,
					qt_min_inicio_p		Number,
					nm_usuario_p		Varchar2) IS

nr_seq_proc_w			Number(10,0);
cd_intervalo_w			Varchar2(7);
ds_horario_w			Varchar2(80);

CURSOR C01 IS
	select	b.nr_seq_proc,
             	p.cd_intervalo
 	from  pe_procedimento p,
        	pe_item_result_proc b,
           	pe_evolucao_resultado e
 where      e.nr_seq_evolucao = nr_seq_evolucao_p
   and      e.nr_seq_resultado = b.nr_seq_Result
   and      b.nr_seq_proc = p.nr_sequencia;

BEGIN

delete from pe_evolucao_proc
where nr_seq_evolucao 	= nr_seq_evolucao_p;

OPEN C01;
LOOP
      FETCH C01 into 	
		nr_seq_proc_w,
		cd_intervalo_w;
        EXIT WHEN C01%NOTFOUND;
		begin
		insert into pe_evolucao_proc(
			NR_SEQ_EVOLUCAO, NR_SEQ_PROC, DT_ATUALIZACAO,
			NM_USUARIO, CD_INTERVALO, DS_HORARIO)
		values(
			nr_seq_evolucao_p, nr_seq_proc_w, sysdate,
			nm_usuario_p, cd_intervalo_w, ds_horario_w);
		exception
			when others then
			nr_seq_proc_w := nr_seq_proc_w;
		end;
END LOOP;
CLOSE C01;
commit;
END Pe_Gerar_Proc_Evolucao;
/