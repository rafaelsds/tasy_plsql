create or replace
procedure estornar_lib_solic_kit (nr_sequencia_p		number,
				nm_usuario_p		varchar2) is

begin

delete
from	kit_estoque
where	nr_seq_solic_kit = nr_sequencia_p;

update	solic_kit_material
set	dt_liberacao = null,
	nm_usuario_lib = null,
	dt_estorno_lib = sysdate,
	nm_usuario_est_lib = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

commit;

end estornar_lib_solic_kit;
/