create or replace
procedure pls_consiste_data_lote_mens_js(
		nr_sequencia_p			number,
		ie_status_p			number,
		ie_novo_lote_aberto_p		varchar2,
		dt_mesano_ref_p			date,
		dt_contabil_p			date,
		dt_mesano_referencia_p		date,
		ie_opcao_p			varchar2,
		cd_estabelecimento_p		number,
		ds_retorno_p		out	varchar2) is 

qt_lote_aberto_w	number(10);
ie_mes_fechado_w	varchar2(2);

begin

select	count(*)
into	qt_lote_aberto_w
from	pls_lote_mensalidade
where	nr_sequencia	<> nr_sequencia_p
and	ie_status		<> ie_status_p;

if	(qt_lote_aberto_w > 0
	and	ie_novo_lote_aberto_p = 'N') then
		ds_retorno_p	:= substr(obter_texto_tasy(38892, wheb_usuario_pck.get_nr_seq_idioma),1,255);
else
	begin
	pls_consistir_data_lote_mens(
		dt_mesano_ref_p,
		dt_contabil_p,
		ds_retorno_p,
		cd_estabelecimento_p,
		wheb_usuario_pck.get_nm_usuario);--Coloquei os campos estab e usu�rio (Geliard)
	
	if	(ds_retorno_p is null) then
		begin
		select	substr(pls_obter_se_mes_fechado(
				nvl(dt_contabil_p,dt_mesano_referencia_p),
				ie_opcao_p,
				cd_estabelecimento_p), 1, 2)
		into	ie_mes_fechado_w
		from	dual;
		
		if	(ie_mes_fechado_w = 'S') then
			ds_retorno_p	:= substr(obter_texto_tasy(38893, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		end if;
		end;
	end if;
	end;
end if;

end pls_consiste_data_lote_mens_js;
/