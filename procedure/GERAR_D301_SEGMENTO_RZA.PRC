create or replace procedure gerar_d301_segmento_rza(nr_seq_dataset_p	number,
                                   		                       nm_usuario_p		varchar2) is 
  
  cursor c01 is
     select ap.nr_atendimento,   
            obter_departamento_data(ap.nr_atendimento,ap.dt_alta)                                    nr_seq_301_depart,
            (select md.nr_crm from medico md where md.cd_pessoa_fisica=ap.cd_medico_referido)        cd_medico_referencia,
            (select md.cd_estab_orig from medico md where md.cd_pessoa_fisica=ap.cd_medico_referido) cd_estab_medico_ref,   
             null                                                                                    cd_medico_ref_dentista,
            (select md.nr_crm  from medico md where md.cd_pessoa_fisica = ap.cd_pessoa_fisica)       cd_medico_atend, 
            (select to_char (apu.dt_entrada_unidade,'yyyymmdd')  
              from atend_paciente_unidade apu 
              where apu.nr_seq_interno = obter_atepacu_paciente(ap.nr_atendimento,'A'))              ds_dt_transf_asv,  
            (select pj.cd_cnes 
             from pessoa_juridica pj ,
                  estabelecimento es 
              where pj.cd_cgc = es.cd_cgc
              and   es.cd_estabelecimento = ap.cd_estabelecimento)                                   cd_cnes,
              null                                                                                   cd_medico_coop,
              null                                                                                   ds_dt_versao_ebm,
              null                                                                                   nr_seq_dataset,
              null                                                                                   nr_estab_medico   
      from d301_dataset_envio        d3,
           atendimento_paciente      ap
      where d3.nr_atendimento     = ap.nr_atendimento 
      and   d3.nr_sequencia          = nr_seq_dataset_p;   
    
  c01_w c01%rowtype;
  
  cursor c02(nr_atendimento_p in diagnostico_doenca.nr_atendimento%type) is
     select  dd.cd_doenca cd_cid_transf_1,   
             dd.ie_lado,
             dd.ie_tipo_diagnostico,
             dd.cd_doenca
     from diagnostico_doenca           dd,
          medic_diagnostico_doenca  mdd,
          classificacao_diagnostico cd
     where dd.nr_atendimento           = nr_atendimento_p
     and   mdd.nr_atendimento          = nr_atendimento_p
     and   dd.cd_doenca                   = mdd.cd_doenca  
     and   cd.nr_sequencia               = mdd.nr_seq_classificacao  
     and   upper(cd.ds_classificacao)  = 'UBERWEISUNGSDIAGNOSE'
     and   dd.cd_doenca_superior is null;      
   
  c02_w c02%rowtype;
            

  cursor c03(nr_atendimento_p in diagnostico_doenca.nr_atendimento%type,
                    cd_doenca_p      in diagnostico_doenca.cd_doenca%type) is

     select    dd.cd_doenca cd_cid_transf_2,   
                  dd.ie_lado,
                  dd.ie_tipo_diagnostico
      from diagnostico_doenca        dd,
           medic_diagnostico_doenca  mdd,
           classificacao_diagnostico cd
      where dd.nr_atendimento           = nr_atendimento_p
      and   mdd.nr_atendimento          = nr_atendimento_p
      and   dd.cd_doenca                = mdd.cd_doenca  
      and   cd.nr_sequencia             = mdd.nr_seq_classificacao  
      and   upper(cd.ds_classificacao)  = 'UBERWEISUNGSDIAGNOSE'
      and   dd.cd_doenca_superior = cd_doenca_p;
            
  c03_w c03%rowtype;

  nr_seq_301_local_transf_1_w    c301_conversao_dados.ds_valor_tasy%type;    
  nr_seq_301_confdiag_trans_1_w  c301_conversao_dados.ds_valor_tasy%type;
  nr_seq_301_local_transf_2_w    c301_conversao_dados.ds_valor_tasy%type;
  nr_seq_301_confdiag_trans_2_w  c301_conversao_dados.ds_valor_tasy%type;
  nr_seq_301_distrito_kv_w       c301_conversao_dados.ds_valor_tasy%type;
  
begin
  open c01;
  fetch c01 into c01_w;
    nr_seq_301_distrito_kv_w       :=obter_conversao_301('C301_26_DISTRITO_KV',null,50,c01_w.cd_cnes,'I');
  close c01;
    --Take primary disease
    for c02_w in c02(c01_w.nr_atendimento) loop
        nr_seq_301_local_transf_1_w    :=obter_conversao_301('C301_16_LOCALIZACAO',null,1372,c02_w.ie_lado,'I');
        nr_seq_301_confdiag_trans_1_w  :=obter_conversao_301('C301_17_CONFIANCA_DIAG',null,1372,c02_w.ie_tipo_diagnostico,'I');   
          
        --Take secondary disease    
        open c03(c01_w.nr_atendimento,c02_w.cd_doenca);
        fetch c03 into c03_w;
        loop
           nr_seq_301_local_transf_2_w    :=obter_conversao_301('C301_16_LOCALIZACAO',null,1372,c03_w.ie_lado,'I');
           nr_seq_301_confdiag_trans_2_w  :=obter_conversao_301('C301_17_CONFIANCA_DIAG',null,1372,c03_w.ie_tipo_diagnostico,'I');
             
           insert into d301_segmento_rza(nr_sequencia,
                                         dt_atualizacao,
                                         nm_usuario,
                                         dt_atualizacao_nrec,
                                         nm_usuario_nrec,
                                         nr_seq_301_depart,
                                         cd_medico_referencia,
                                         cd_estab_medico_ref,
                                         cd_medico_ref_dentista,
                                         cd_cid_transf_1,
                                         nr_seq_301_local_transf_1,
                                         nr_seq_301_confdiag_trans_1,
                                         cd_cid_transf_2,
                                         nr_seq_301_local_transf_2,
                                         nr_seq_301_confdiag_trans_2,
                                         cd_medico_atend,
                                         cd_medico_coop,
                                         ds_dt_transf_asv,
                                         nr_seq_301_distrito_kv,
                                         ds_dt_versao_ebm,
                                         nr_seq_dataset,
                                         nr_estab_medico) values (d301_segmento_rza_seq.nextval,
                                                                                 sysdate,
                                                                                 nm_usuario_p,
                                                                                 sysdate,
                                                                                 nm_usuario_p,
                                                                                 c01_w.nr_seq_301_depart,
                                                                                 c01_w.cd_medico_referencia,
                                                                                 c01_w.cd_estab_medico_ref,
                                                                                 c01_w.cd_medico_ref_dentista,
                                                                                 c02_w.cd_cid_transf_1,
                                                                                 nr_seq_301_local_transf_1_w,
                                                                                 nr_seq_301_confdiag_trans_1_w,
                                                                                 c03_w.cd_cid_transf_2,
                                                                                 nr_seq_301_local_transf_2_w,
                                                                                 nr_seq_301_confdiag_trans_2_w,
                                                                                 c01_w.cd_medico_atend,
                                                                                 c01_w.cd_medico_coop,
                                                                                 c01_w.ds_dt_transf_asv,
                                                                                 nr_seq_301_distrito_kv_w,
                                                                                 c01_w.ds_dt_versao_ebm,
                                                                                 nr_seq_dataset_p,
                                                                                 c01_w.nr_estab_medico );  
  
        fetch c03 into c03_w;
        exit when c03%notfound;     
        end loop;
        close c03;
           
    end loop;                                                       
  
end gerar_d301_segmento_rza;
/
