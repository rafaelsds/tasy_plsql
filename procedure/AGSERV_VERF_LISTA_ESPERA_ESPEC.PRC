create or replace
procedure Agserv_verf_lista_espera_espec(	cd_agenda_p		in number,					
					cd_pessoa_fisica_p	in varchar2,
					nm_pessoa_lista_p	in varchar2,
					dt_desejada_p		in date,
					ds_consistencia_p	out varchar2) is 

qt_agendamentos_w	number(10);
ds_mensagem_w		varchar2(2000);
			
begin
ds_consistencia_p := null;
SELECT	COUNT(*) 
INTO	qt_agendamentos_w
FROM 	agenda_lista_espera a,
	agenda b
WHERE	a.cd_agenda = b.cd_agenda
AND	a.cd_agenda <> cd_agenda_p
AND	b.cd_especialidade = obter_especialidade_agenda(cd_agenda_p)
AND	a.dt_desejada BETWEEN TRUNC(dt_desejada_p) AND fim_dia(dt_desejada_p)
AND	((a.cd_pessoa_fisica = cd_pessoa_fisica_p)
OR	(a.nm_pessoa_lista = nm_pessoa_lista_p));

if	(qt_agendamentos_w > 0) then	
	ds_mensagem_w := obter_desc_expressao(774835);	
end if;

ds_consistencia_p := ds_mensagem_w;

end Agserv_verf_lista_espera_espec;
/