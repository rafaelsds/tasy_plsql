create or replace
procedure desfaz_liberacao_bordero(
		nr_sequencia_p	number) is 

begin

update	conta_pagar_lib 
set 	dt_liberacao	= null
where 	nr_sequencia	= nr_sequencia_p;
commit;

end desfaz_liberacao_bordero;
/