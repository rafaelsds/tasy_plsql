create or replace
procedure Cancelar_comunic_interna(	nr_sequencia_p		number,
				nr_seq_motivo_cancel_p	number,
				nm_usuario_p		varchar2) is 
qt_registro_w		number(10);

begin

if	((nvl(nr_sequencia_p,0) > 0) and
	(nvl(nr_seq_motivo_cancel_p,0) > 0))then
	begin
	
	select	count(*)
	into	qt_registro_w
	from	motivo_cancelamento_ci
	where	nr_sequencia = nr_seq_motivo_cancel_p;
	
	if	(qt_registro_w > 0)	then
		update	COMUNIC_INTERNA
		set	dt_atualizacao 		= 	sysdate,
			dt_cancelamento 		= 	sysdate,
			nr_seq_motivo_cancel	=	nr_seq_motivo_cancel_p,
			nm_usuario		=	nm_usuario_p
		where	nr_sequencia		=	nr_sequencia_p;
	end	if;
	
	end;
end 	if;

commit;

end Cancelar_comunic_interna;
/