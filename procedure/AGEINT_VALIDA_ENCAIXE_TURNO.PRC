CREATE OR REPLACE PROCEDURE AGEINT_VALIDA_ENCAIXE_TURNO(CD_AGENDA_P NUMBER, 
                                                        DT_ENCAIXE_P DATE) IS
  NR_SEQ_TURNO_W AGENDA_TURNO.NR_SEQUENCIA%TYPE;
  IE_PERM_ENCAIXE_TURNO_W AGENDA_TURNO.IE_ENCAIXE%TYPE;
  ie_regra_horario_turno_w 		varchar2(1);
BEGIN
  Obter_Param_Usuario(821, 219, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_regra_horario_turno_w);
  ie_regra_horario_turno_w := nvl(ie_regra_horario_turno_w,'N');
  
  if (ie_regra_horario_turno_w = 'T') then
	select	obter_turno_encaixe_d_agecons(CD_AGENDA_P,DT_ENCAIXE_P,'N')
	into	nr_seq_turno_w
	from	dual;	
  else
	select	obter_turno_encaixe_agecons(CD_AGENDA_P,DT_ENCAIXE_P)
	into	nr_seq_turno_w
	from	dual;
  end if;  

  SELECT NVL(MAX(IE_ENCAIXE), 'S')
  INTO IE_PERM_ENCAIXE_TURNO_W
  FROM AGENDA_TURNO
  WHERE NR_SEQUENCIA = NR_SEQ_TURNO_W;

  IF (IE_PERM_ENCAIXE_TURNO_W = 'N') THEN
     WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(224365);
  END IF;
END AGEINT_VALIDA_ENCAIXE_TURNO;
/