create or replace
procedure lab_aprova_exame_prescricao (	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					nr_seq_resultado_p	number,
					cd_medico_resp_p	varchar2,
					nm_usuario_p		varchar2) is

nr_seq_prescr_w		number(14);
					
cursor c01 is
	select 	c.nr_sequencia
	from 	atendimento_paciente a,
			prescr_medica b,
			prescr_procedimento c,
			exame_laboratorio d,
			exame_lab_resultado e,
			exame_lab_result_item f,
			material_exame_lab g
	where 	b.nr_atendimento = a.nr_atendimento
	and 	c.nr_prescricao = b.nr_prescricao
	and 	d.nr_seq_exame = f.nr_seq_exame
	and 	e.nr_prescricao = b.nr_prescricao
	and 	e.nr_atendimento = a.nr_atendimento
	and 	f.nr_seq_resultado = e.nr_seq_resultado
	and 	f.nr_seq_prescr = c.nr_sequencia
	and 	g.cd_material_exame = c.cd_material_exame
	and 	c.ie_status_atend = 30
	and 	f.nr_seq_material is not null
	and 	c.nr_seq_exame is not null
	and 	nvl(c.ie_suspenso,'N') = 'N'
	and 	c.cd_motivo_baixa = 0
	and 	f.ie_status = 1
	and 	(lab_obter_regra_estab(1, a.cd_estabelecimento) = 'S')
	and 	b.nr_prescricao = nr_prescricao_p;
begin

open c01;
loop
fetch c01 into 
	nr_seq_prescr_w;
exit when c01%notfound;	

	update 	exame_lab_result_item
	set 	dt_aprovacao = sysdate,
			nm_usuario_aprovacao = nm_usuario_p,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p,
			cd_medico_resp = cd_medico_resp_p
	where 	nr_seq_resultado = nr_seq_resultado_p
	and 	nr_seq_prescr = nr_seq_prescr_w
	and 	nr_seq_material is not null;
	
	update 	prescr_procedimento
	set 	ie_status_atend = 35
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia = nr_seq_prescr_w
	and 	ie_status_atend < 35;
	
end loop;
close c01;

commit;

end lab_aprova_exame_prescricao ;
/