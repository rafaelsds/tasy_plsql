create or replace
procedure pls_atualiza_valor_lanc_manual
		(	nr_seq_mensalidade_p	number,
			cd_estabelecimento_p	number,
			nr_seq_lanc_manual_p	number,
			NM_USUARIO_P		varchar2) is 

nr_seq_tipo_lanc_w	number(10);
qt_regra_pro_rata_dia_w	number(10);
ie_operacao_motivo_w	varchar2(1);
vl_item_w		pls_mensalidade_seg_item.vl_item%type;
nr_seq_lote_w		number(10);
		
begin

if	(nvl(nr_seq_lanc_manual_p,0) <> 0) then

	select	max(nr_seq_tipo_lanc),
		vl_item
	into	nr_seq_tipo_lanc_w,
		vl_item_w
	from	pls_mensalidade_seg_item
	where	nr_sequencia = nr_seq_lanc_manual_p
	group by vl_item;

	select	max(ie_operacao_motivo)
	into	ie_operacao_motivo_w
	from	pls_tipo_lanc_adic
	where	nr_sequencia = nr_seq_tipo_lanc_w;

	if	(((nvl(ie_operacao_motivo_w,'S') = 'D') and (vl_item_w > 0)) or
		 ((nvl(ie_operacao_motivo_w,'S') = 'S') and (vl_item_w < 0))) then
		update	pls_mensalidade_seg_item
		set	vl_item = (vl_item * -1)
		where	nr_sequencia = nr_seq_lanc_manual_p;
	end if;
end if;

pls_atualizar_ato_coop_mens(nr_seq_mensalidade_p,CD_ESTABELECIMENTO_P,NM_USUARIO_P);

select	count(1)
into	qt_regra_pro_rata_dia_w
from	pls_regra_pro_rata_dia
where	cd_estabelecimento	= cd_estabelecimento_p
and	rownum = 1;

if	(qt_regra_pro_rata_dia_w	> 0) then
	pls_gerar_pro_rata_dia(nr_seq_mensalidade_p);
end if;

pls_atualizar_tributos_mens(nr_seq_mensalidade_p,CD_ESTABELECIMENTO_P,NM_USUARIO_P);

select	max(nr_seq_lote)
into	nr_seq_lote_w
from	pls_mensalidade
where	nr_sequencia	= nr_seq_mensalidade_p;

pls_atualiza_valor_mensalidade(nr_seq_lote_w,NM_USUARIO_P);
	
commit;

end pls_atualiza_valor_lanc_manual;
/
