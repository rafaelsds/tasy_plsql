create or replace
procedure Med_Inserir_Alerta
		(cd_medico_p	varchar2,
		nr_ficha_p	number,
		ds_alerta_p	varchar2,
		nm_usuario_p	varchar2) is

nr_seq_cliente_w	number(10,0);
nr_seq_alerta_w		number(10,0);


begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_cliente_w
from	med_cliente
where	cd_medico		= cd_medico_p
and	cd_pessoa_sist_orig	= nr_ficha_p;

if	(nr_seq_cliente_w <> 0) then
	begin

	select	med_alerta_seq.nextval
	into	nr_seq_alerta_w
	from	dual;


	insert	into med_alerta
		(nr_sequencia,
		nr_seq_cliente,
		dt_alerta,
		ie_tipo_alerta,
		dt_atualizacao,
		nm_usuario,
		ds_alerta)
	values	(nr_seq_alerta_w,
		nr_seq_cliente_w,
		sysdate,'M',
		sysdate,
		nm_usuario_p,
		substr(ds_alerta_p,1,255));
	end;
end if;


commit;

end Med_Inserir_Alerta;
/