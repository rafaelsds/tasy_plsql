create or replace
procedure pls_finalizar_analise_recurso	(nr_seq_analise_p	pls_analise_conta.nr_sequencia%type,
					nr_id_transacao_p	pls_analise_conta_rec.nr_id_transacao%type,
					nr_seq_grupo_atual_p	pls_grupo_auditor.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_encerrar_analise_p	varchar2 default 'N',
					ie_encaminha_prot_p	varchar2 default 'N') is 

ie_existe_grupos_abertos_w	pls_integer;
nr_seq_regra_w			Number(10);
nr_seq_grupo_w			Number(10);
ie_existe_grupo_final_w		varchar2(1);
ie_existe_final_anali_w		varchar2(1);
qt_grupos_analise_w		pls_integer;
ie_status_analise_w		pls_analise_conta.ie_status%type;
ds_parecer_w			pls_analise_parecer_rec.ds_parecer%type;
nr_seq_parecer_w		pls_analise_parecer_rec.nr_sequencia%type;
nr_seq_conta_rec_w		pls_analise_conta_rec.nr_sequencia%type;
qt_registro_w			pls_integer;
ie_finaliza_sem_itens_pend_w	pls_parametros_rec_glosa.ie_finaliza_sem_itens_pend%type;
ie_grupo_pendente_w		varchar2(1);
qt_grupo_pend_w			pls_integer;
t_contas_w    pls_util_cta_pck.t_number_table;
index_w        pls_integer := 0;

Cursor c01 (	nr_seq_analise_pc		pls_analise_conta_rec.nr_seq_analise%type,
		nr_id_transacao_pc		pls_analise_conta_rec.nr_id_transacao%type) is
	select	a.nr_seq_conta_rec,
		a.nr_seq_proc_rec,
		a.nr_seq_mat_rec
	from	pls_analise_conta_rec a
	where	a.nr_seq_analise	= nr_seq_analise_pc
	and	a.nr_id_transacao	= nr_id_transacao_pc;
				
Cursor c02 (	nr_seq_analise_pc		pls_analise_conta_rec.nr_seq_analise%type,
		nr_id_transacao_pc		pls_analise_conta_rec.nr_id_transacao%type) is
	select	distinct a.nr_seq_conta_rec
	from	pls_analise_conta_rec a
	where	a.nr_seq_analise	= nr_seq_analise_pc
	and	a.nr_id_transacao	= nr_id_transacao_pc;
				
begin

t_contas_w.delete;
if	(nr_seq_analise_p > 0) and
	(nr_seq_grupo_atual_p > 0) then
	
	select	nvl(ie_finaliza_sem_itens_pend,'N')
	into	ie_finaliza_sem_itens_pend_w
	from	pls_parametros_rec_glosa
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(ie_finaliza_sem_itens_pend_w = 'S') then
		ie_grupo_pendente_w := pls_obter_se_grupo_pend_rec(nr_seq_analise_p, null, null, nr_seq_grupo_atual_p, 'A');
		
		if	(ie_grupo_pendente_w = 'S') then
			wheb_mensagem_pck.exibir_mensagem_abort('Existem itens pendentes de an�lise para o grupo auditor. Favor verificar atrav�s do campo "Pendentes".');
		end if;
	end if;
	
	update	pls_auditoria_conta_grupo
	set	dt_liberacao 	= sysdate
	where	nr_sequencia	= (	select	max(nr_sequencia) 
					from	pls_auditoria_conta_grupo
					where	nr_seq_grupo 		= nr_seq_grupo_atual_p	
					and	nr_seq_analise		= nr_seq_analise_p
					and	dt_liberacao 		is null		
					and	nr_seq_ordem 		= (	select	min(nr_seq_ordem)		 
										from	pls_auditoria_conta_grupo
										where	nr_seq_grupo 		= nr_seq_grupo_atual_p	
										and	nr_seq_analise		= nr_seq_analise_p
										and	dt_liberacao		is null	));

	pls_gravar_inicio_fim_analise(nr_seq_analise_p, nr_seq_grupo_atual_p, 'F', nm_usuario_p);
	
	select	count(*)
	into	ie_existe_grupos_abertos_w
	from	pls_auditoria_conta_grupo a		
	where	a.nr_seq_analise = nr_seq_analise_p
	and	a.dt_liberacao is null;

	/*Fazer verifica��o se existe grupos de analise ainda em aberto se n�o houver  fechar a analise*/
	if	(ie_existe_grupos_abertos_w = 0) then
		
		/*Obter o grupo responsavel por fechar a conta*/
		pls_obter_grupo_fechar_analise(nr_seq_analise_p, cd_estabelecimento_p, nr_seq_regra_w, nr_seq_grupo_w);
		
		if	(nvl(nr_seq_grupo_w,0) > 0) then
			ie_existe_grupo_final_w := 'S';
		
			/*obter se este grupo j� foi inserido na an�lise*/
			select	decode(count(nr_sequencia),0,'N','S')
			into	ie_existe_final_anali_w
			from	pls_auditoria_conta_grupo
			where	nr_seq_grupo = nr_seq_grupo_w
			and	nr_seq_analise = nr_seq_analise_p;
		else		
			/*Caso n�o haja regra de grupo de finaliza��o*/
			ie_existe_grupo_final_w := 'N';
		end if;	
		
		/*Se n�o existe grupo de finaliza��o na an�lise esta � encerrada*/
		if	(ie_existe_grupo_final_w = 'N') then		
			update	pls_analise_conta
			set	dt_liberacao_analise	= sysdate,
				ie_status 		= 'L',
				dt_final_analise	= sysdate
			where	nr_sequencia = nr_seq_analise_p;
		else			
			/*Se o grupo de finaliza��o existir na an�lise.*/
			if (ie_existe_final_anali_w = 'S') then
				select	count(nr_sequencia)
				into	qt_grupos_analise_w
				from	pls_auditoria_conta_grupo
				where	nr_seq_analise = nr_seq_analise_p;
				
				/*Se existir mais de uma grupo de analise*/
				if (qt_grupos_analise_w > 1) and 
				   (pls_obter_se_auditor_grupo(nr_seq_grupo_w, nm_usuario_p) = 'N') then
					/*Se o grupo de finaliza��o existir na an�lise ent�o seu sua libera��o � desfeita. Permitindo que o mesmo se torne o fluxo da vez. */
					pls_desf_final_grupo_analise(nr_seq_analise_p, nr_seq_grupo_w, null, nm_usuario_p, cd_estabelecimento_p,'N');
				else
					/*Se existir somente o grupo do auditor ent�o � liberado a an�lise.*/
					update	pls_analise_conta
					set	dt_liberacao_analise	= sysdate,
						ie_status = 'L',
						dt_final_analise	= sysdate
					where	nr_sequencia = nr_seq_analise_p;
				end if;
			else
				/*Se o grupo de finaliza��o n�o existir na an�lise este � acrescentado*/
				pls_inserir_grupo_analise(nr_seq_analise_p, nr_seq_grupo_w, 'Grupo inserido atrav�s da regra de finaliza��o '||nr_seq_regra_w,
							nr_seq_grupo_atual_p, 'N', nm_usuario_p, cd_estabelecimento_p);
			end if;
		end if;
				
	end if;		 
	
	/* Atualizar a discuss�o */
	for c01_w in c01( nr_seq_analise_p, nr_id_transacao_p) loop
		
		if	(c01_w.nr_seq_proc_rec is not null) then
		
			select	max(nr_sequencia)
			into	nr_seq_parecer_w
			from	pls_analise_parecer_rec
			where	nr_seq_proc_rec	= c01_w.nr_seq_proc_rec
			and	ds_parecer	is not null;
			
			select	max(ds_parecer)
			into	ds_parecer_w
			from	pls_analise_parecer_rec
			where	nr_sequencia	= nr_seq_parecer_w;
			
			update	pls_rec_glosa_proc
			set	ds_justificativa_oper	= substr(ds_parecer_w,1,150)
			where	nr_sequencia		= c01_w.nr_seq_proc_rec;
		
		elsif	(c01_w.nr_seq_mat_rec is not null) then
		
			select	max(nr_sequencia)
			into	nr_seq_parecer_w
			from	pls_analise_parecer_rec
			where	nr_seq_mat_rec	= c01_w.nr_seq_mat_rec
			and	ds_parecer	is not null;
			
			select	max(ds_parecer)
			into	ds_parecer_w
			from	pls_analise_parecer_rec
			where	nr_sequencia	= nr_seq_parecer_w;
			
			update	pls_rec_glosa_mat
			set	ds_justificativa_oper	= substr(ds_parecer_w,1,150)
			where	nr_sequencia		= c01_w.nr_seq_mat_rec;
			
		end if;
		
		if	(c01_w.nr_seq_conta_rec is not null) then
		
			select	max(nr_sequencia)
			into	nr_seq_parecer_w
			from	pls_analise_parecer_rec
			where	nr_seq_conta_rec	= c01_w.nr_seq_conta_rec
			and	ds_parecer	is not null;
			
			select	max(ds_parecer)
			into	ds_parecer_w
			from	pls_analise_parecer_rec
			where	nr_sequencia	= nr_seq_parecer_w;
			
			update	pls_rec_glosa_conta
			set	ds_justificativa_oper	= substr(ds_parecer_w,1,150)
			where	nr_sequencia		= c01_w.nr_seq_conta_rec;
			
		end if;
					
		t_contas_w(index_w) := c01_w.nr_seq_conta_rec;
		index_w := index_w + 1;
	end loop;
	
	select	count(1)
	into	qt_grupo_pend_w
	from	pls_auditoria_conta_grupo
	where	nr_seq_analise = nr_seq_analise_p
	and	dt_liberacao is null;	
	
	if	(qt_grupo_pend_w > 0) then
		pls_alterar_status_analise_cta(nr_seq_analise_p, 'A', 'PLS_FINALIZAR_ANALISE_RECURSO', nm_usuario_p, cd_estabelecimento_p);
	elsif	(ie_encerrar_analise_p = 'S') then -- Se o par�metro permitir, encerra a an�lise
		pls_alterar_status_analise_cta(nr_seq_analise_p, 'T', 'PLS_FINALIZAR_ANALISE_RECURSO', nm_usuario_p, cd_estabelecimento_p);
	end if;
	
	select	max(ie_status)
	into	ie_status_analise_w
	from	pls_analise_conta
	where	nr_sequencia	= nr_seq_analise_p;
	
	-- Somente ir� fechar as contas de recurso, caso a an�lise esteja encerrada. OS 1063216 - aedemuth
	if	(ie_status_analise_w in ('T','L')) then
		-- Fechar conta recurso de glosa 

			for r_c02_w in C02(nr_seq_analise_p, nr_id_transacao_p) loop				
				pls_fechar_rec_glosa_conta( r_c02_w.nr_seq_conta_rec, null, nm_usuario_p, cd_estabelecimento_p, ie_encaminha_prot_p );  
			end loop;

	end if;
	
	pls_inserir_hist_analise(null, nr_seq_analise_p, 7, null, null, null, null, null, nr_seq_grupo_atual_p, nm_usuario_p, cd_estabelecimento_p);
end if;

pls_atualizar_grupo_penden(nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);

commit;

end pls_finalizar_analise_recurso;
/
