create or replace procedure UB_GERAR_COBR_UNICRED_45
    (  nr_seq_cobr_escrit_p    number,
      cd_estabelecimento_p    number,
      nm_usuario_p      varchar2) is

ds_conteudo_w    varchar2(500);
ds_numero_doc_w     varchar2(9);
nr_seq_apres_w    number(10) := 0;

/* Header de arquivo */
dt_remessa_retorno_w  date;
qt_registro_w    number(10);
vl_total_remessa_w    number(15,2);

/* Registro */
nr_conta_w    titulo_receber_cobr.nr_conta%type;
vl_cobranca_w    titulo_receber_cobr.vl_cobranca%type;
dt_pagamento_previsto_w  titulo_receber.dt_pagamento_previsto%type;

/*UTL File*/
arq_texto_w      utl_file.file_type;
ds_erro_w      varchar2(255);
ds_local_w      varchar2(255);
nm_arquivo_w      varchar2(255);
ds_mensagem_w      varchar2(255);
ds_local_rede_w      varchar2(255);

cursor  c01 is
select  decode(length(ub_obter_dados_pagador_fin(b.nr_seq_pagador,'C',b.dt_pagamento_previsto)), 6, substr(ub_obter_dados_pagador_fin(b.nr_seq_pagador,'C',b.dt_pagamento_previsto), 1,8) ,
  substr(ub_obter_dados_pagador_fin(b.nr_seq_pagador,'C',b.dt_pagamento_previsto), 1,8)) || '-' || substr(ub_obter_dados_pagador_fin(b.nr_seq_pagador,'DC',b.dt_pagamento_previsto),1,1) ,
  c.vl_cobranca,
  b.dt_pagamento_previsto,
  c.nr_titulo
from  cobranca_escritural a,
  titulo_receber_v b,
  titulo_receber_cobr c,
  pls_mensalidade		d,
  pls_contrato_pagador_fin	g
where  a.nr_sequencia    = c.nr_seq_cobranca
and  c.nr_titulo      = b.nr_titulo
and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
and 	d.nr_seq_pagador 		= g.nr_seq_pagador(+)
and trunc(to_date(d.dt_referencia),'MM') between trunc(to_date(nvl(g.dt_inicio_vigencia,sysdate)),'MM') and trunc(to_date(fim_dia(g.dt_fim_vigencia) + 360),'MM')
and (g.dt_fim_vigencia is null or g.dt_fim_vigencia > d.dt_referencia)
and  a.nr_sequencia    = nr_seq_cobr_escrit_p;

begin

nm_arquivo_w  := to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '.rem';

update  cobranca_escritural
set    ds_arquivo    = ds_local_w || nm_arquivo_w
where  nr_sequencia  = nr_seq_cobr_escrit_p;

commit;

delete  from w_envio_banco
where  nm_usuario  = nm_usuario_p;

/* Header do arquivo */

select  nvl(max(a.dt_remessa_retorno),sysdate)
into  dt_remessa_retorno_w
from  cobranca_escritural a
where  a.nr_sequencia  = nr_seq_cobr_escrit_p;

select  count(*),
  sum(a.vl_cobranca)
into  qt_registro_w,
  vl_total_remessa_w
from  titulo_receber_cobr a
where  a.nr_seq_cobranca  = nr_seq_cobr_escrit_p;

ds_conteudo_w  :=  to_char(dt_remessa_retorno_w,'yymmdd') || /*Pos 01 a 06*/
          lpad(qt_registro_w,4,'0') || /*Pos 07 a 10*/
          lpad('0',9,'0') || /*Pos 11 a 19*/
          lpad(somente_numero(to_char(nvl(vl_total_remessa_w,0),'99999999999990.00')),16,'0') || /*Pos 20 a 35*/
          lpad(' ',6,' '); /*Pos 36 a 41*/

nr_seq_apres_w  := nr_seq_apres_w + 1;

insert into w_envio_banco
    (  nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      cd_estabelecimento,
      ds_conteudo,
      nr_seq_apres)
  values  (  w_envio_banco_seq.nextval,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      cd_estabelecimento_p,
      ds_conteudo_w,
      nr_seq_apres_w);

/* Fim Header */

/* Detalhe */

open  c01;
loop
fetch  c01 into
  nr_conta_w,
  vl_cobranca_w,
  dt_pagamento_previsto_w,
  ds_numero_doc_w;
exit  when c01%notfound;

  ds_conteudo_w  :=  lpad(nr_conta_w,10,0) || /*Pos 01 a 10*/
            '227' || /*Pos 11 a 14*/ -- Alterado de 0227 para 227 devido a OS 303737
            lpad(ds_numero_doc_w,9,0) || /*Pos 15 a 23*/
            lpad(somente_numero(to_char(nvl(vl_cobranca_w,0),'99999999999990.00')),16,'0') || /*Pos 24 a 39*/
            to_char(dt_pagamento_previsto_w ,'yymmdd'); /*Pos 40 a 45*/

  nr_seq_apres_w  := nr_seq_apres_w + 1;

  insert into w_envio_banco
      (  nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        cd_estabelecimento,
        ds_conteudo,
        nr_seq_apres)
    values  (  w_envio_banco_seq.nextval,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        cd_estabelecimento_p,
        ds_conteudo_w,
        nr_seq_apres_w);

end  loop;
close  c01;

/* Fim  Detalhe */

commit;

end UB_GERAR_COBR_UNICRED_45;
/
