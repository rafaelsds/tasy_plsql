create or replace
procedure intcom_gerar_int_material(	cd_material_p		number,
				ds_xml_p			long,
				cd_estabelecimento_p		number,
				nm_usuario_p		varchar2) is 

nr_seq_registro_w	number(10);
ds_login_w		varchar2(255);
ds_senha_w		varchar2(255);			
				
begin

select	registro_integr_compras_seq.nextval
into	nr_seq_registro_w
from	dual;

select	a.ds_login_integr_compras_ws,
	a.ds_senha_integr_compras_ws
into	ds_login_w,
	ds_senha_w
from	parametro_compras a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

insert into registro_integr_compras(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_tipo_operacao,
	cd_material)
values(	nr_seq_registro_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,	
	'WIP',	
	cd_material_p);

insert into registro_integr_com_xml(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_registro,
	ie_status,
	ie_operacao,
	ie_sistema_origem,
	ds_erro,
	ds_retorno,
	ds_xml,
	ie_tipo_operacao,
	ds_login,
	ds_senha)
values(	registro_integr_com_xml_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_registro_w,
	'NP',
	'E',
	'TASY',
	null,
	null,
	ds_xml_p,
	'WIP',
	ds_login_w,
	ds_senha_w);

insert into material_historico(	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_historico,
	cd_material,
	ds_titulo,
	ie_tipo)
values(	material_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	substr(wheb_mensagem_pck.get_texto(315168),1,255),
	cd_material_p,
	substr(wheb_mensagem_pck.get_texto(315171),1,255),
	'S');
	
	
commit;

end intcom_gerar_int_material;
/
