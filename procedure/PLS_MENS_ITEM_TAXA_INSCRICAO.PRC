create or replace
procedure pls_mens_item_taxa_inscricao
			(	nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
				nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
				nr_seq_plano_p			pls_plano.nr_sequencia%type,
				nr_seq_pagador_p		pls_contrato_pagador.nr_sequencia%type,
				dt_referencia_p			pls_mensalidade_segurado.dt_mesano_referencia%type,
				nr_parcela_segurado_p		pls_mensalidade_segurado.nr_parcela%type,
				nr_seq_contrato_p		pls_contrato.nr_sequencia%type,
				nr_seq_intercambio_p		pls_intercambio.nr_sequencia%type,
				nr_seq_subestipulante_p		pls_sub_estipulante.nr_sequencia%type,
				ie_acao_contrato_p		pls_segurado.ie_acao_contrato%type,
				vl_base_inscricao_p		pls_mensalidade_seg_item.vl_item%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
				nm_usuario_p			usuario.nm_usuario%type) is

nr_seq_regra_w		pls_regra_inscricao.nr_sequencia%type;
vl_inscricao_w		pls_regra_inscricao.vl_inscricao%type;
tx_inscricao_w		pls_regra_inscricao.tx_inscricao%type;
ie_inseriu_item_w	varchar2(1);

begin
pls_obter_taxa_inscricao(nr_seq_segurado_p, nr_seq_contrato_p, nr_seq_intercambio_p, nr_seq_plano_p, null, nr_parcela_segurado_p, dt_referencia_p, 
				nr_seq_subestipulante_p, ie_acao_contrato_p, nr_seq_regra_w, vl_inscricao_w, tx_inscricao_w);

if	(nvl(tx_inscricao_w,0) > 0) and
	(vl_base_inscricao_p > 0)then
	vl_inscricao_w	:= dividir_sem_round((vl_base_inscricao_p * tx_inscricao_w),100) + nvl(vl_inscricao_w,0);
end if;

if	(nvl(vl_inscricao_w,0) > 0) then
	pls_mens_itens_pck.add_item_taxa_insc(nr_seq_mensalidade_seg_p, '2', vl_inscricao_w, 'N', null, null, '', ie_inseriu_item_w);
end if;

end pls_mens_item_taxa_inscricao;
/