create or replace
procedure pls_gerar_pag_prest_desc_prior
			(	nr_seq_vencimento_p		number,
				nr_seq_prestador_p		number,
				nr_seq_pagamento_p		number,
				vl_vencimento_p			number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar os valores de desconto conforme o tipo de gera��o destes valores
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	Utilizada no lugar da pls_pag_prest_desc_prior
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_saldo_negativo_w		varchar2(255);
ie_saldo_negativo_conf_w	varchar2(255);
ie_tipo_valor_w			varchar2(3);
vl_item_w			number(15,2);
vl_descontar_w			number(15,2);
vl_vencimento_w			number(15,2);
vl_producao_w			number(15,2);
vl_pagamento_item_w		number(15,2);
nr_seq_evento_w			number(10);
dt_mes_competencia_w		pls_lote_pagamento.dt_mes_competencia%type;
qt_pag_negativo_max_w		pls_prestador_pagto.qt_pag_negativo_max%type;
qt_pag_negativo_ant_w		number(10,0);
ie_gerar_titulo_w		varchar2(255);
ds_observacao_w			pls_pag_prest_venc_valor.ds_observacao%type;
nr_seq_lote_w			pls_lote_pagamento.nr_sequencia%type;
dt_ultima_ger_tit_w		pls_lote_pagamento.dt_geracao_titulos%type;
ie_trib_saldo_tit_nf_w		parametros_contas_pagar.ie_trib_saldo_tit_nf%type;
vl_trib_acum_w			number(15,2);
dt_mes_competencia_ww		pls_lote_pagamento.dt_mes_competencia%type;
nr_fluxo_pgto_w			pls_periodo_pagamento.nr_fluxo_pgto%type;
vl_minimo_tit_liq_w		pls_prestador_pagto.vl_minimo_tit_liq%type := 0;
nr_seq_evento_movto_w		pls_evento_movimento.nr_sequencia%type;
ie_excecao_pag_negativo_w	pls_prestador_pagto.ie_excecao_pag_negativo%type;
nr_seq_prestador_pagto_w	pls_prestador_pagto.nr_sequencia%type;

Cursor C01 is
	select	sum(a.vl_item),
		b.nr_sequencia,
		nvl(b.ie_saldo_negativo, 'CP')
	from	pls_evento		b,
		pls_pagamento_item 	a
	where	b.nr_sequencia 		= a.nr_seq_evento
	and	a.nr_seq_pagamento 	= nr_seq_pagamento_p
	group by
		b.nr_sequencia,
		b.ie_saldo_negativo,
		a.nr_prior_desc	
	having sum(vl_item) < 0
	order by
		nvl(a.nr_prior_desc, 0);

Cursor C02 is
	select	distinct nr_seq_lote,
		dt_mes_competencia,
		nr_fluxo_pgto
	from	(select	b.nr_sequencia nr_seq_lote,
			b.dt_mes_competencia dt_mes_competencia,
			a.nr_fluxo_pgto nr_fluxo_pgto
		from	pls_periodo_pagamento 	a,
			pls_pagamento_prestador c,
			pls_lote_pagamento	b
		where	b.nr_sequencia		= c.nr_seq_lote
		and	b.nr_seq_periodo	= a.nr_sequencia
		and	c.ie_cancelamento	is null
		and	b.dt_fechamento 	is not null
		and	b.nr_sequencia		<> nr_seq_lote_w
		and	b.dt_mes_competencia	<= dt_mes_competencia_w
		and	c.nr_seq_prestador	= nr_seq_prestador_p
		and	(b.dt_mes_competencia	> dt_ultima_ger_tit_w or dt_ultima_ger_tit_w is null)
		and	nvl(a.ie_complementar,'N') <> 'S'
		and	((ie_excecao_pag_negativo_w = 'N') or
			((ie_excecao_pag_negativo_w = 'S') and
			not exists(	select	1
					from	pls_regra_pag_neg_max_exec 	z
					where	z.nr_seq_prest_pagto		= nr_seq_prestador_pagto_w
					and	z.nr_seq_periodo		= a.nr_sequencia)))
		order by b.dt_mes_competencia desc, a.nr_fluxo_pgto desc)
	where	rownum	<= qt_pag_negativo_max_w
	order by dt_mes_competencia desc,
		nr_fluxo_pgto desc;
begin
vl_vencimento_w	:= vl_vencimento_p;

if	(vl_vencimento_w <= 0) then
	select	max(nr_sequencia)
	into	nr_seq_prestador_pagto_w
	from	pls_prestador_pagto
	where	nr_seq_prestador = nr_seq_prestador_p;
	
	select	nvl(max(ie_saldo_negativo),'CP'),
		nvl(max(qt_pag_negativo_max),0),
		nvl(max(vl_minimo_tit_liq),0),
		nvl(max(ie_excecao_pag_negativo),'N')
	into	ie_saldo_negativo_conf_w,
		qt_pag_negativo_max_w,
		vl_minimo_tit_liq_w,
		ie_excecao_pag_negativo_w
	from	pls_prestador_pagto
	where	nr_sequencia = nr_seq_prestador_pagto_w;
	
	if	(ie_saldo_negativo_conf_w = 'CP') then
		select	nvl(max(ie_saldo_negativo), 'PP')
		into	ie_saldo_negativo_conf_w
		from	pls_parametro_pagamento
		where	cd_estabelecimento = cd_estabelecimento_p;
	end if;
	
	select	max(a.nr_seq_lote),
		max(b.dt_mes_competencia)
	into	nr_seq_lote_w,
		dt_mes_competencia_w
	from	pls_lote_pagamento b,
		pls_pagamento_prestador a
	where	a.nr_seq_lote	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_pagamento_p;

	select	sum(vl_item)
	into	vl_producao_w
	from	pls_pagamento_item a
	where	a.nr_seq_pagamento 	= nr_seq_pagamento_p;
	
	begin
	select	nvl(ie_trib_saldo_tit_nf,'N')
	into	ie_trib_saldo_tit_nf_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		ie_trib_saldo_tit_nf_w := 'N';
	end;

	vl_trib_acum_w := 0;
	if	(ie_trib_saldo_tit_nf_w = 'S') then
		select	nvl(sum(a.vl_imposto),0)
		into	vl_trib_acum_w
		from	tributo b,
			pls_pag_prest_venc_trib a
		where	a.cd_tributo		= b.cd_tributo
		and	a.ie_pago_prev	 	= 'V'
		and	a.nr_seq_vencimento 	= nr_seq_vencimento_p
		and	nvl(b.ie_saldo_tit_pagar,'S')	= 'S';
	else
		select	nvl(sum(decode(b.ie_soma_diminui, 'D', a.vl_imposto, 'S', a.vl_imposto * -1, 0)),0)
		into	vl_trib_acum_w
		from	tributo b,
			pls_pag_prest_venc_trib a
		where	a.cd_tributo		= b.cd_tributo
		and	a.ie_pago_prev	 	= 'V'
		and	a.nr_seq_vencimento 	= nr_seq_vencimento_p;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		vl_item_w,
		nr_seq_evento_w,
		ie_saldo_negativo_w;
	exit when C01%notfound;
		begin
		ds_observacao_w	:= '';
		
		if	(ie_saldo_negativo_w = 'CP') then
			ie_saldo_negativo_w	:= ie_saldo_negativo_conf_w;
		end if;
		
		if	(vl_item_w > vl_vencimento_w) then
			vl_descontar_w	:= vl_item_w;
			vl_vencimento_w	:= vl_vencimento_w - vl_item_w;
		else
			select	decode(ie_saldo_negativo_w, 'AT', vl_item_w, vl_vencimento_w)
			into	vl_descontar_w
			from	dual;
			
			select	decode(ie_saldo_negativo_w, 'AT', vl_vencimento_w, 0)
			into	vl_vencimento_w
			from	dual;
		end if;
		
		if	(vl_vencimento_w > 0) and
			(ie_saldo_negativo_w <> 'AT') then
			vl_descontar_w	:= 0;
		end if;
		
		if	(ie_saldo_negativo_w = 'TR') then
			vl_producao_w := vl_producao_w - abs(vl_descontar_w) - vl_trib_acum_w;
			
			if	(vl_producao_w >= 0) then
				ie_saldo_negativo_w := 'PP';
			end if;
		end if;
		
		if	(vl_descontar_w <> 0) then
		
			if	(qt_pag_negativo_max_w > 0) then
				ie_gerar_titulo_w		:= 'S';
				qt_pag_negativo_ant_w		:= 0;
				
				begin
					select	max(b.dt_geracao_titulos)
					into	dt_ultima_ger_tit_w
					from	pls_pag_prest_vencimento	d,
						pls_periodo_pagamento 		a,
						pls_pagamento_prestador 	c,
						pls_lote_pagamento		b
					where	c.nr_sequencia		= d.nr_seq_pag_prestador
					and	b.nr_sequencia		= c.nr_seq_lote
					and	b.nr_seq_periodo	= a.nr_sequencia
					and	b.nr_sequencia		<> nr_seq_lote_w
					and	b.dt_mes_competencia	<= dt_mes_competencia_w
					and	c.nr_seq_prestador	= nr_seq_prestador_p
					and	nvl(a.ie_complementar,'N') <> 'S'
					and	d.nr_titulo_receber is not null
					and	c.ie_cancelamento is null
					and	b.dt_fechamento is not null;
				exception 
					when others then
					dt_ultima_ger_tit_w := null;
				end;
				
				open C02;
				loop
				fetch C02 into	
					nr_seq_lote_w,
					dt_mes_competencia_ww,
					nr_fluxo_pgto_w;
				exit when C02%notfound or ie_gerar_titulo_w = 'N';
					
					select	nvl(sum(vl_item),0)
					into 	vl_pagamento_item_w
					from	pls_pagamento_item	b,
						pls_pagamento_prestador	a
					where	a.nr_sequencia		= b.nr_seq_pagamento
					and	a.nr_seq_lote 		= nr_seq_lote_w
					and	a.nr_seq_prestador 	= nr_seq_prestador_p;
					
					if	(vl_pagamento_item_w > 0) then
						ie_gerar_titulo_w	:= 'N';
					else
						qt_pag_negativo_ant_w	:= qt_pag_negativo_ant_w + 1;
					end if;
					
				end loop;
				close C02;
				
				if	((qt_pag_negativo_ant_w >= qt_pag_negativo_max_w) and (ie_gerar_titulo_w = 'S')) and 
					((vl_minimo_tit_liq_w = 0) or ((vl_minimo_tit_liq_w > 0) and (abs(vl_descontar_w) >= vl_minimo_tit_liq_w))) then
					ie_saldo_negativo_w	:= 'TR'; -- Gerar t�tulo receber
					ds_observacao_w		:= 'Este valor negativo gerou t�tulo a receber porque este prestador j� excedeu ' || to_char(qt_pag_negativo_max_w) ||
								' pagamento(s) negativos anteriores.' || chr(13) || chr(10) ||
								'Em caso de d�vida verifique a fun��o OPS - Prestadores\Dados complementares\Dados para pagamento.';
				end if;
			end if;
			
			select	max(b.nr_sequencia)
			into	nr_seq_evento_movto_w
			from	pls_evento_movimento		b,
				pls_lote_evento			a
			where	a.nr_sequencia			= b.nr_seq_lote
			and	b.nr_seq_prestador		= nr_seq_prestador_p
			and	a.nr_seq_lote_pgto_apropr	= nr_seq_lote_w
			and	b.ie_cancelamento is null;
			
			insert into pls_pag_prest_venc_valor
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_tipo_valor,
				nr_seq_evento,
				nr_seq_vencimento,
				vl_vencimento,
				ds_observacao,
				nr_seq_evento_movto)
			values	(pls_pag_prest_venc_valor_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ie_saldo_negativo_w,
				nr_seq_evento_w,
				nr_seq_vencimento_p,
				vl_descontar_w,
				ds_observacao_w,
				nr_seq_evento_movto_w);
		end if;
		end;
	end loop;
	close C01;
end if;

end pls_gerar_pag_prest_desc_prior;
/