create or replace
procedure copiar_criterio_distribuicao(	cd_estabelecimento_p	number,
				cd_tabela_custo_p		number,
				cd_tabela_destino_p	number,
				nr_seq_tabela_p		number,
				nr_seq_tabela_dest_p	number,
				nm_usuario_p		varchar2) is

type campos is record (		nr_seq_old 	number(10), 
				nr_seq_new 	number(10));
type vetor is table of campos index 	by binary_integer;

nr_sequencia_criterio_w		number(10);
cd_sequencia_criterio_w         	number(10);
cd_estabelecimento_w              	number(4);
cd_tabela_custo_w                 	number(5);
qt_base_distribuicao_w            	number(15,4);
vl_fixo_w				number(15,4);
ds_base_distribuicao_w            	varchar2(200);
nr_seq_distribuicao_w             	number(10);
cd_natureza_gasto_orig_w          	number(20);
cd_natureza_gasto_dest_w          	number(20);
dt_atualizacao_w			date;
nm_usuario_w			varchar2(15);
cd_centro_controle_w              	number(8);
cd_grupo_natureza_gasto_w 		number(8);
nr_seq_gng_w			number(10);
nr_seq_gng_ref_w			number(10);
cd_natureza_gasto_w		number(20);
ie_criterio_distribuicao_w		varchar2(1);
ie_criterio_valor_w			varchar2(1);
ie_criterio_centro_w			varchar2(1);
nr_seq_criterio_ref_w		number(10,0);
pr_distribuir_w			number(05,2) := 0;
i				integer := 1;
vetor_proc_w			vetor;
ie_tipo_gasto_w			varchar2(15);
ie_limitado_w			varchar2(1);
ds_view_w			varchar2(30);
nr_seq_ng_orig_w			number(10);
nr_seq_ng_w			number(10);
nr_seq_ng_dest_w			number(10);
nr_seq_ng_ref_w			number(10);
ie_copiar_centros_destino_w		varchar2(1);
ie_copiar_w			varchar2(1);
ie_tipo_centro_controle_w	varchar2(2);
cursor c01 is
select a.cd_sequencia_criterio,
	a.cd_estabelecimento,
	a.cd_tabela_custo,
	a.qt_base_distribuicao,
	a.ds_base_distribuicao,
	a.nr_seq_distribuicao,
	a.cd_natureza_gasto_orig,
	a.cd_centro_controle,
	a.cd_grupo_natureza_gasto,
	a.cd_natureza_gasto,
	a.ie_criterio_distribuicao,
	a.ie_criterio_valor,
	a.ie_criterio_centro,
	a.nr_seq_criterio_ref,
	a.pr_distribuir,
	a.cd_natureza_gasto_dest,
	a.ie_tipo_gasto,
	a.vl_fixo,
	nvl(a.ie_limitado,'N'),
	a.ds_view,
	a.nr_seq_gng,
	a.nr_seq_gng_ref,
	a.nr_seq_ng_orig,
	a.nr_seq_ng,
	a.nr_seq_ng_dest,
	a.nr_seq_ng_ref,
	a.ie_tipo_centro_controle
from	tabela_custo b,
	criterio_distr_orc  a
where	a.nr_seq_tabela		= b.nr_sequencia
/*OS1992919 - Jean - Projeto Da vita - Inicio */
and	exists(	select	1
		from	tabela_custo_acesso_v tca
		where	tca.nr_sequencia	= nr_seq_tabela_p
		and	tca.nr_sequencia	= b.nr_sequencia
		and	tca.cd_estabelecimento	= a.cd_estabelecimento);
/*OS1992919 - Jean - Projeto Da vita - Final*/

begin


delete	from criterio_distr_orc
where	nr_seq_tabela	= nr_seq_tabela_dest_p;

open c01;
loop
fetch c01 into
 	cd_sequencia_criterio_w,
	cd_estabelecimento_w,
	cd_tabela_custo_w,
	qt_base_distribuicao_w,
	ds_base_distribuicao_w,
	nr_seq_distribuicao_w,
	cd_natureza_gasto_orig_w,
	cd_centro_controle_w,
	cd_grupo_natureza_gasto_w,
	cd_natureza_gasto_w,
	ie_criterio_distribuicao_w,
	ie_criterio_valor_w,
	ie_criterio_centro_w,
	nr_seq_criterio_ref_w,
	pr_distribuir_w,
	cd_natureza_gasto_dest_w,
	ie_tipo_gasto_w,
	vl_fixo_w,
	ie_limitado_w,
	ds_view_w,
	nr_seq_gng_w,
	nr_seq_gng_ref_w,
	nr_seq_ng_orig_w,
	nr_seq_ng_w,
	nr_seq_ng_dest_w,
	nr_seq_ng_ref_w,
	ie_tipo_centro_controle_w;
exit when c01%notfound;
	begin
	
	ie_copiar_centros_destino_w	:=  nvl(obter_valor_param_usuario(927, 43, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 'N');

	ie_copiar_w	:= 'S';
	select	criterio_distr_orc_seq.nextval 
	into	nr_sequencia_criterio_w
	from	dual;
	
	insert into criterio_distr_orc(
		cd_sequencia_criterio,
		cd_estabelecimento,
		cd_tabela_custo,
		qt_base_distribuicao,
		ds_base_distribuicao,
		nr_seq_distribuicao,
		cd_natureza_gasto_orig,
		cd_centro_controle,
		cd_grupo_natureza_gasto,
		cd_natureza_gasto,
		dt_atualizacao,
		nm_usuario,
		ie_criterio_distribuicao,
		ie_criterio_valor,
		ie_criterio_centro,
		nr_seq_criterio_ref,
		pr_distribuir,
		cd_natureza_gasto_dest,
		ie_tipo_gasto,
		vl_fixo,
		ie_limitado,
		ds_view,
		nr_seq_gng,
		nr_seq_gng_ref,
		nr_seq_ng_orig,
		nr_seq_ng,
		nr_seq_ng_dest,
		nr_seq_ng_ref,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_revisado,
		nr_seq_tabela,
		ie_tipo_centro_controle)
	values(	nr_sequencia_criterio_w,
		cd_estabelecimento_w,
		cd_tabela_destino_p,
		qt_base_distribuicao_w,
		ds_base_distribuicao_w,
		nr_seq_distribuicao_w,
		cd_natureza_gasto_orig_w,
		cd_centro_controle_w,
		cd_grupo_natureza_gasto_w,
		cd_natureza_gasto_w,
		sysdate,
		nm_usuario_p,
		ie_criterio_distribuicao_w,
		ie_criterio_valor_w,
		ie_criterio_centro_w,
		nr_seq_criterio_ref_w,
		pr_distribuir_w,
		cd_natureza_gasto_dest_w,
		ie_tipo_gasto_w,
		vl_fixo_w,
		ie_limitado_w,
		ds_view_w,
		nr_seq_gng_w,
		nr_seq_gng_ref_w,
		nr_seq_ng_orig_w,
		nr_seq_ng_w,
		nr_seq_ng_dest_w,
		nr_seq_ng_ref_w,
		nm_usuario_p,
		sysdate,
		'N',
		nr_seq_tabela_dest_p,
		ie_tipo_centro_controle_w);
		
	
	if	(ie_copiar_centros_destino_w = 'N') and
		(ie_criterio_centro_w = 'C') then
		
		ie_copiar_w	:= 'N';
		
	end if;
		
	insert into criterio_distr_orc_dest(
		cd_sequencia_criterio,
		cd_estabelecimento,
		cd_centro_controle_dest,
		qt_distribuicao,
		pr_distribuicao,
		cd_natureza_gasto_dest,
		dt_atualizacao,
		nm_usuario,
		qt_peso,
		nr_seq_ng_dest,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	select	nr_sequencia_criterio_w,
		cd_estabelecimento,
		cd_centro_controle_dest,
		qt_distribuicao,
		pr_distribuicao,
		cd_natureza_gasto_dest,
		dt_atualizacao,
		nm_usuario,
		qt_peso,
		nr_seq_ng_dest,
		sysdate,
		nm_usuario_p
	from	criterio_distr_orc_dest
	where	cd_sequencia_criterio	= cd_sequencia_criterio_w
	and	ie_copiar_w		= 'S';
	
	
	vetor_proc_w(i).nr_seq_old 		:= cd_sequencia_criterio_w;
	vetor_proc_w(i).nr_seq_new 		:= nr_sequencia_criterio_w;
	i := i + 1;
	end;
end loop;
close c01;
for i in 1..vetor_proc_w.count loop
	update	criterio_distr_orc
	set	nr_seq_criterio_ref	= vetor_proc_w(i).nr_seq_new
	where	nr_seq_criterio_ref	= vetor_proc_w(i).nr_seq_old
	and	nr_seq_tabela		= nr_seq_tabela_dest_p;
end loop;

commit;
end copiar_criterio_distribuicao;
/
