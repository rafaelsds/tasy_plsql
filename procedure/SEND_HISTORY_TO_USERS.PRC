create or replace
procedure send_history_to_users(ds_users_receiver_p	Varchar2,
			ds_history_p		Varchar2,
			cd_establishment_p	Number,
			nr_seq_history_p	Number,
			nm_user_p		Varchar2) is 

nr_seq_prospect_w	number(10,0);
nr_seq_comunic_w	number(10,0);
nr_seq_text_w		varchar2(10);
ds_text_ci_w		varchar2(4000);
ds_prospect_data_w	varchar2(400);
ds_client_w		varchar2(255);
ds_person_w		varchar2(80);
			
begin

if	(nvl(nr_seq_history_p,0) > 0) then

	select	nr_seq_prospect
	into	nr_seq_prospect_w
	from	cml_prospect_hist
	where	nr_sequencia = nr_seq_history_p;
	
	select	max(ds_cliente),
		max(substr(decode(cd_pessoa_fisica, null, obter_nome_pf_pj(null, cd_cnpj), obter_nome_pf_pj(cd_pessoa_fisica, null)),1,80))
	into	ds_client_w,
		ds_person_w
	from	cml_prospect
	where	nr_sequencia = nr_seq_prospect_w;
	
	ds_prospect_data_w	:= '\par ' || wheb_mensagem_pck.get_texto(342186) || ': ' || ds_client_w || '\par ' || wheb_mensagem_pck.get_texto(342189) || ': ' || ds_person_w || '\par ';
	
	ds_text_ci_w		:= replace(ds_history_p, '\par }', ds_prospect_data_w || '\par }');

	enviar_ci_hist_prospect(ds_text_ci_w, nm_user_p, ds_users_receiver_p, cd_establishment_p, null, nr_seq_comunic_w);
	
	if	(nvl(nr_seq_comunic_w,0) > 0) then
		enviar_ci_anexo_prospect(nm_user_p, nr_seq_prospect_w, nr_seq_comunic_w);
	end if;

end if;

commit;

end send_history_to_users;
/