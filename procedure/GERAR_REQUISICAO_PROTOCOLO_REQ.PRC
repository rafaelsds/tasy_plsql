create or replace
procedure gerar_requisicao_protocolo_req(
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			nr_sequencia_p		number,
			cd_pessoa_req_p 	number,
			nr_requisicao_p	out	number) is

nr_sequencia_w			number(10);
cd_pessoa_requisitante_w		varchar2(10);
cd_operacao_estoque_w		number(3);
cd_centro_custo_w			number(8);
cd_local_estoque_w		number(4);
cd_local_estoque_destino_w		number(4);
nr_requisicao_w			number(10);
nr_seq_item_w			number(5);
cd_material_w			number(6);
qt_conv_estoque_consumo_w	number(13,4);
cd_unidade_medida_estoque_w	varchar2(30);
cd_unidade_medida_consumo_w	varchar2(30);
ie_forma_w			varchar2(1);
qt_estoque_w			number(13,4);
qt_material_w			number(13,4);
ie_liberada_w			varchar2(1);
ie_itens_w			varchar2(1);
ie_saldo_maior_igual_min_w		varchar2(1);
ie_controlado_w			varchar2(1);
ie_prescr_cirurgia_w		varchar2(1);
ie_lancado_cirurgia_w		varchar2(1);
ie_agenda_cirurgica_w		varchar2(1);
qt_itens_desdobra_req_w		number(10);
ie_mat_comercial_w		prot_requisicao.ie_mat_comercial%type;

cursor c01 is
select	a.nr_sequencia,
	nvl(cd_pessoa_req_p,a.cd_pessoa_requisitante),
	a.cd_operacao_estoque,
	a.cd_centro_custo,
	a.cd_local_estoque,
	a.cd_local_estoque_destino,
	a.ie_liberada,
	a.ie_forma,
	max(nvl(a.ie_saldo_maior_igual_min,'N')),
	nvl(ie_controlado,'A'),
	nvl(ie_prescr_cirurgia,'N'),
	nvl(ie_lancado_cirurgia,'N'),
	nvl(ie_agenda_cirurgica,'N'),
	nvl(ie_mat_comercial,'N')
from	prot_requisicao a
where	a.ie_situacao		= 'A'
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	(a.nr_sequencia		= nr_sequencia_p or nvl(nr_sequencia_p, 0) = 0)
and	(obter_se_gera_req_protocolo(a.nr_sequencia) = 'S')
and	a.qt_dias_intervalo > 0
and not exists(
	select	1
	from	requisicao_material b
	where	b.nr_seq_protocolo = a.nr_sequencia
	and	trunc(b.dt_solicitacao_requisicao,'dd') >= trunc(SYSDATE,'dd') - a.qt_dias_intervalo)
group by a.nr_sequencia,
	a.cd_pessoa_requisitante,
	a.cd_operacao_estoque,
	a.cd_centro_custo,
	a.cd_local_estoque,
	a.cd_local_estoque_destino,
	a.ie_liberada,
	a.ie_forma,
	nvl(ie_controlado,'A'),
	nvl(ie_prescr_cirurgia,'N'),
	nvl(ie_lancado_cirurgia,'N'),
	nvl(ie_agenda_cirurgica,'N'),
	nvl(ie_mat_comercial,'N')
union all
select	a.nr_sequencia,
	nvl(cd_pessoa_req_p,a.cd_pessoa_requisitante),
	a.cd_operacao_estoque,
	a.cd_centro_custo,
	a.cd_local_estoque,
	a.cd_local_estoque_destino,
	a.ie_liberada,
	a.ie_forma,
	max(nvl(a.ie_saldo_maior_igual_min,'N')),
	nvl(ie_controlado,'A'),
	nvl(ie_prescr_cirurgia,'N'),
	nvl(ie_lancado_cirurgia,'N'),
	nvl(ie_agenda_cirurgica,'N'),
	nvl(ie_mat_comercial,'N')
from	prot_requisicao a
where	a.ie_situacao		= 'A'
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	(a.nr_sequencia		= nr_sequencia_p or nvl(nr_sequencia_p, 0) = 0)
and	(obter_se_gera_req_protocolo(a.nr_sequencia) = 'S')
and	a.qt_dias_intervalo = 0
group by a.nr_sequencia,
	a.cd_pessoa_requisitante,
	a.cd_operacao_estoque,
	a.cd_centro_custo,
	a.cd_local_estoque,
	a.cd_local_estoque_destino,
	a.ie_liberada,
	a.ie_forma,
	nvl(ie_controlado,'A'),
	nvl(ie_prescr_cirurgia,'N'),
	nvl(ie_lancado_cirurgia,'N'),
	nvl(ie_agenda_cirurgica,'N'),
	nvl(ie_mat_comercial,'N');


cursor c02 is
select	cd_material,
	qt_material
from	prot_requisicao_item
where	nr_seq_prot_requisicao	= nr_sequencia_w;

					
begin
select	nvl(max(QT_ITENS_REQUISICAO),0)
into 	qt_itens_desdobra_req_w
from 	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;

open c01;
loop
fetch c01 into 
	nr_sequencia_w,
	cd_pessoa_requisitante_w,
	cd_operacao_estoque_w,
	cd_centro_custo_w,
	cd_local_estoque_w,
	cd_local_estoque_destino_w,
	ie_liberada_w,
	ie_forma_w,
	ie_saldo_maior_igual_min_w,
	ie_controlado_w,
	ie_prescr_cirurgia_w,
	ie_lancado_cirurgia_w,
	ie_agenda_cirurgica_w,
	ie_mat_comercial_w;
exit when c01%notfound;
	begin
	if	(ie_forma_w = 'R') then
		begin
		gravar_requisicao_material(
			cd_estabelecimento_p,
			null,
			null,
			null,
	    		cd_local_estoque_destino_w,
           		cd_local_estoque_w,
			null,
            		cd_pessoa_requisitante_w,
            		nm_usuario_p,
			ie_prescr_cirurgia_w,
			ie_lancado_cirurgia_w,
			ie_agenda_cirurgica_w,
			ie_mat_comercial_w,
			ie_saldo_maior_igual_min_w,
			'N',
			'N',
			'N',
			'N',
			'X',
			null,
			null,
			null,
			null,
			null,
			null,
			ie_controlado_w,
			'A',
			'N',
			'D',
			'N',
			'N',
			'0',
			null,
			'N',
			'N',
			qt_itens_desdobra_req_w,
			null,
			'N',
			'N',
			nr_requisicao_w);

		update	requisicao_material
		set	nr_seq_protocolo	= nr_sequencia_w,
			ds_observacao		= wheb_mensagem_pck.get_texto(302212)
		where	nr_requisicao		= nr_requisicao_w;
		end;
	else
		begin
		select	requisicao_seq.nextval
		into	nr_requisicao_w
		from	dual;

		insert into requisicao_material(
			nr_requisicao,
			cd_estabelecimento,
			cd_local_estoque,
			dt_solicitacao_requisicao,
			dt_atualizacao,
			nm_usuario,
			cd_operacao_estoque,
			cd_pessoa_requisitante,
			cd_local_estoque_destino,
			cd_centro_custo,
			ie_urgente,
			ie_geracao,
			nr_seq_protocolo,
			ds_observacao)
		values(	nr_requisicao_w,
			cd_estabelecimento_p,
			cd_local_estoque_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			cd_operacao_estoque_w,
			cd_pessoa_requisitante_w,
			cd_local_estoque_destino_w,
			cd_centro_custo_w,
			'N',
			'I',
			nr_sequencia_w,
			wheb_mensagem_pck.get_texto(302212));

		ie_itens_w	:= 'N';
		open c02;
		loop
		fetch c02 into 
			cd_material_w,
			qt_material_w;
		exit when c02%notfound;
			begin
			ie_itens_w	:= 'S';

			select	nvl(max(nr_sequencia),0) +1
			into	nr_seq_item_w
			from	item_requisicao_material
			where	nr_requisicao = nr_requisicao_w;

			select	qt_conv_estoque_consumo,
				substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
				substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo
			into	qt_conv_estoque_consumo_w,
				cd_unidade_medida_estoque_w,
				cd_unidade_medida_consumo_w
			from	material
			where	cd_material = cd_material_w;

			qt_estoque_w := dividir(qt_material_w,qt_conv_estoque_consumo_w);
			
			if	(ie_mat_comercial_w = 'S') then
				cd_material_w	:= obter_mat_comercial(cd_material_w,'C');
			end if;

			insert into item_requisicao_material(
				nr_requisicao,
				nr_sequencia,
				cd_estabelecimento,
				cd_material,
				qt_material_requisitada,
				qt_material_atendida,
				qt_material_req_auto,
				vl_material,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				ie_acao,
				cd_motivo_baixa,
				qt_estoque,
				cd_unidade_medida_estoque,
				ie_geracao)
			values(	nr_requisicao_w,
				nr_seq_item_w,
				cd_estabelecimento_p,
				cd_material_w,
				qt_material_w,
				0,
				qt_material_w,
				0,
				sysdate,
				nm_usuario_p,
				cd_unidade_medida_consumo_w,
				'I',
				0,
				qt_estoque_w,
				cd_unidade_medida_estoque_w,
				'I');
			end;
		end loop;
		close c02;

		end;
	end if;

	if	(ie_liberada_w = 'S') then
		update	requisicao_material
		set	dt_liberacao		= sysdate,
			nm_usuario_lib		= nm_usuario_p,
			dt_aprovacao		= sysdate,
			nm_usuario_aprov		= nm_usuario_p
		where	nr_requisicao		= nr_requisicao_w;
		
		update	item_requisicao_material
		set	dt_aprovacao		= sysdate,
			nm_usuario_aprov		= nm_usuario_p
		where	nr_requisicao		= nr_requisicao_w;
		end if;

	if	(ie_itens_w = 'N') then
		delete
		from	requisicao_material
		where	nr_requisicao = nr_requisicao_w;

	end if;

	end;
end loop;
close c01;

nr_requisicao_p := nr_requisicao_w;

commit;
end gerar_requisicao_protocolo_req;
/
