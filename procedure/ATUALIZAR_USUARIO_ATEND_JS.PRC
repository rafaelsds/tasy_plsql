create or replace
procedure atualizar_usuario_atend_js(	nr_atendimento_p	number,
					nm_usuario_p		varchar2) is 

begin

if	(nr_atendimento_p is not null)then
	begin
	
	update 	atendimento_paciente
	set	nm_usuario 	= nm_usuario_p
	where 	nr_atendimento	= nr_atendimento_p;
	
	end;
end if;

commit;

end atualizar_usuario_atend_js;
/