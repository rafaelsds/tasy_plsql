create or replace
procedure consisitir_regra_prescricao_js(	nr_atendimento_p		number,
						nr_min_prescr_alta_p		number,
						ie_gerar_prescr_conta_def_p	varchar2,
						ds_msg_erro_p			out varchar2) is 

qt_regra_alta_w		number(10,0);
dt_alta_prescr_w	date;
ie_continuar_exec_w	varchar2(1)	:= 'S';
qt_atend_pac_w		number(10,0);
ds_erro_w		varchar2(255);
			
begin
if (nvl(nr_atendimento_p,0) > 0) then

	select 	count(*) 
	into	qt_regra_alta_w
	from 	regra_cons_min_prescr_alta;

	select  (nvl(dt_alta, sysdate) + obter_min_alta_prescr(ie_tipo_atendimento)/1440)
	into	dt_alta_prescr_w  
	from  	atendimento_paciente 
	where 	nr_atendimento = nr_atendimento_p;

	if	(qt_regra_alta_w > 0)and
		(dt_alta_prescr_w < sysdate)then
		begin
		
		ds_msg_erro_p		:= obter_texto_tasy(82833, wheb_usuario_pck.get_nr_seq_idioma);
		ie_continuar_exec_w	:= 'N';
		
		end;
	end if;

	if	(ie_continuar_exec_w = 'S')then
		begin
		
		if	(qt_regra_alta_w = 0)and
			(nr_min_prescr_alta_p <> -1)then
			begin
			
			select  (nvl(dt_alta, sysdate) + nr_min_prescr_alta_p /1440)   
			into	dt_alta_prescr_w
			from  	atendimento_paciente 
			where 	nr_atendimento = nr_atendimento_p;
			
			if	(dt_alta_prescr_w < sysdate)then
				begin
				
				ds_msg_erro_p		:= obter_texto_tasy(82835, wheb_usuario_pck.get_nr_seq_idioma);
				ie_continuar_exec_w	:= 'N';
				
				end;
			end if;
			
			end;
		end if;
		
		end;
	end if;

	if	(ie_continuar_exec_w = 'S')then
		begin
		
		select  count(*)
		into	qt_atend_pac_w
		from  	atendimento_paciente 
		where 	nr_atendimento = nr_atendimento_p 
		and   	dt_fim_conta is not null;
		
		if	(qt_atend_pac_w = 1)then
			begin
			
			ds_msg_erro_p		:= obter_texto_tasy(82843, wheb_usuario_pck.get_nr_seq_idioma);
			ie_continuar_exec_w	:= 'N';
			
			end;
		end if;
		
		end;
	end if;

	if	(ie_continuar_exec_w = 'S')then
		begin
		
		if	(ie_gerar_prescr_conta_def_p = 'S')then
			begin
			
			consistir_prescr_conta(nr_atendimento_p, ds_erro_w);
			
			if	(ds_erro_w <> '')then
				begin
				
				ds_msg_erro_p		:= ds_erro_w;
				ie_continuar_exec_w	:= 'N';
				
				end;
			end if;
			
			end;
		end if;
		
		end;
	end if;

	commit;
end if;

end consisitir_regra_prescricao_js;
/