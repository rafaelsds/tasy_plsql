create or replace
procedure COPIAR_PROC_AUTOR_AGENDA_PROC
			(nr_seq_proc_autor_p	in number,
			nm_usuario_p		in varchar2) is

nr_seq_agenda_w		autorizacao_convenio.nr_seq_agenda%type;
cd_autorizacao_w	autorizacao_convenio.cd_autorizacao%type;
nr_seq_estagio_w	autorizacao_convenio.nr_seq_estagio%type;
cd_procedimento_w	procedimento_autorizado.cd_procedimento%type;
nr_seq_agenda_proc_w	procedimento_autorizado.nr_seq_agenda_proc%type;
cd_convenio_w		agenda_paciente.cd_convenio%type;
cd_categoria_w		agenda_paciente.cd_categoria%type;
cd_plano_w		agenda_paciente.cd_plano%type;

count_w			number(10);
ie_autorizacao_w	varchar2(3);

begin

select	max(a.nr_seq_agenda),
	max(c.cd_convenio),
	max(c.cd_categoria),
	max(c.cd_plano),	
	max(b.nr_seq_agenda_proc),
	max(a.cd_autorizacao),
	max(a.nr_seq_estagio)
into	nr_seq_agenda_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w,	
	nr_seq_agenda_proc_w,
	cd_autorizacao_w,
	nr_seq_estagio_w
from	autorizacao_convenio a,
	procedimento_autorizado b,
	agenda_paciente c,
	procedimento d
where	a.nr_sequencia		= b.nr_sequencia_autor
and	a.nr_seq_agenda		= c.nr_sequencia
and	b.cd_procedimento	= d.cd_procedimento
and	b.ie_origem_proced	= d.ie_origem_proced
and	d.cd_tipo_procedimento	= 99 -- somente proc cirurgico
and	b.nr_sequencia		= nr_seq_proc_autor_p
and	not exists
	(select	1
	from	agenda_paciente_proc x
	where	x.nr_sequencia		= c.nr_sequencia
	and	x.cd_procedimento	= b.cd_procedimento
	and	x.ie_origem_proced	= b.ie_origem_proced);

if	(nr_seq_agenda_w is not null) and
	(nr_seq_agenda_proc_w is null) then --N�o continua se procedimento j� vinculado
	
	select	max(ie_autor_agenda)
	into	ie_autorizacao_w
	from	estagio_autorizacao
	where	nr_sequencia	= nr_seq_estagio_w
	and		OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
	
	select	nvl(max(nr_seq_agenda),0) + 1
	into	nr_seq_agenda_proc_w
	from	agenda_paciente_proc
	where	nr_sequencia	= nr_seq_agenda_w;

	insert	into agenda_paciente_proc
		(cd_autorizacao,
		cd_categoria,
		cd_convenio,
		cd_medico,
		cd_medico_req,
		cd_pessoa_indicacao,
		cd_plano,
		cd_procedimento,
		cd_procedimento_tuss,
		ds_indicacao_clinica,
		ds_observacao,
		dt_atualizacao,
		ie_autorizacao,
		ie_lado,
		ie_origem_proced,
		nm_usuario,
		nr_seq_agenda,
		nr_seq_agenda_princ,
		nr_seq_item_adic_ageint,
		nr_seq_proc_interno,
		nr_sequencia,
		qt_min_proc,
		qt_procedimento)
	select	cd_autorizacao_w,
		cd_categoria_w,
		cd_convenio_w,
		null,
		null,
		null,
		cd_plano_w,
		a.cd_procedimento,
		a.cd_procedimento_tuss,
		null,
		a.ds_observacao,
		sysdate,
		ie_autorizacao_w,
		null,
		a.ie_origem_proced,
		nm_usuario_p,
		nr_seq_agenda_proc_w,
		null,
		null,
		a.nr_seq_proc_interno,
		nr_seq_agenda_w,
		0,
		nvl(a.qt_solicitada,1)
	from	procedimento_autorizado a
	where	a.nr_sequencia		= nr_seq_proc_autor_p;	
	
	update	procedimento_autorizado
	set	nr_seq_agenda		= nr_seq_agenda_w,
		nr_seq_agenda_proc	= nr_seq_agenda_proc_w
	where	nr_sequencia		= nr_seq_proc_autor_p;
	
	commit;
		
end if;

end COPIAR_PROC_AUTOR_AGENDA_PROC;
/
