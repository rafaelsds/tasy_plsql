create or replace
procedure vincular_ordem_serv_qua(
			nr_seq_ordem_serv_p	number,
			nr_seq_os_vinculada_p 	number,
			nm_usuario_p		varchar2) is 
begin
if	(nr_seq_ordem_serv_p is not null)
	and (nr_seq_os_vinculada_p is not null) then
	insert 	into man_ordem_serv_vinc(
			nr_sequencia,
			nr_seq_ordem_servico,
			nr_seq_os_vinculada,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec)
	values	(  	
			man_ordem_serv_vinc_seq.nextval,
			nr_seq_ordem_serv_p,
			nr_seq_os_vinculada_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p); 
	commit;
end if;
end vincular_ordem_serv_qua;
/