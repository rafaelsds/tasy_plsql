create or replace procedure fucs_gerar_w_valor_consumo_mes (	cd_estabelecimento_p		number,
					dt_mesano_inicio_p		date,
					dt_mesano_fim_p			date,
					ie_opcao_p			varchar,
					nm_usuario_p			varchar) is

/*
ie_opcao_p	- CE - Consumo estoque
		- NF - Nota Fiscal
		- CC - Consumo consignado
*/

dt_inicio_mes_w		date;
dt_fim_mes_w		date;

ie_tipo_w			varchar2(2);
cd_material_w		number(6);
cd_centro_custo_w		number(8);
cd_conta_contabil_w	varchar2(20);
cd_local_estoque_w	number(4);
vl_consumo_w		number(13,2);
vl_direto_w		number(13,2);
vl_consignado_w		number(13,2);
qt_estoque_w		number(13,4);
qt_item_nf_w		number(13,4);
vl_item_nf_w		number(13,4);
vl_preco_w		number(13,4);
ds_centro_custo_w		varchar2(80);
ds_conta_contabil_w	varchar2(255);
nr_sequencia_w		Number(10,0);
ie_consumo_w		varchar(1);
cd_operacao_estoque_w	number(3);

cursor C01 is
	select ie_tipo,
		cd_material,
		cd_centro_custo,
		cd_local_estoque,
		cd_conta_contabil,
		substr(nvl(obter_desc_centro_custo(cd_centro_custo), 'Centro Custo n�o Informado'),1,80),
		substr(nvl(obter_desc_conta_contabil(cd_conta_contabil), 'Conta Cont�bil n�o Informado'),1,255),
		sum(qt_estoque),
		sum(vl_consumo),
		sum(vl_direto),
		sum(vl_consignado),
		ie_consumo,
		cd_operacao_estoque
	from (
	select 'CE' ie_tipo,
		cd_material,
		cd_centro_custo,
		cd_local_estoque,
		cd_conta_contabil,
		decode(b.ie_consumo, 'A', qt_estoque, qt_estoque * -1) qt_estoque,
		decode(b.ie_consumo, 'A', vl_estoque, vl_estoque * -1) vl_consumo,
		0 vl_direto,
		0 vl_consignado,
		b.ie_consumo,
		b.cd_operacao_estoque
	from	operacao_estoque b,
		resumo_movto_estoque a
	where	a.cd_operacao_estoque	= b.cd_operacao_estoque
	and	b.ie_consumo in ('A', 'D')
	and	a.cd_estabelecimento 	= cd_estabelecimento_p
	and	dt_mesano_referencia between dt_inicio_mes_w and dt_fim_mes_w
	and	a.ie_consignado = 'N'
	union all
	select 'NF',
		a.cd_material,
		a.cd_centro_custo,
		a.cd_local_estoque,
		cd_conta_contabil,
		a.qt_movimento qt_estoque,
		0 vl_consumo,
		vl_movimento vl_direto,
		0 vl_consignado,
		''ie_consumo,
		0 cd_operacao_estoque
	from	local_estoque b,
		nota_fiscal_centro_custo_v a
	where	a.cd_local_estoque = b.cd_local_estoque
	and	a.cd_estabelecimento 	= cd_estabelecimento_p
	and	dt_atualizacao_estoque between dt_inicio_mes_w and dt_fim_mes_w
	and	b.ie_tipo_local = 8
	and	a.ie_consignado = 'N'
	union	all
	select 'CC',
		cd_material,
		cd_centro_custo,
		cd_local_estoque,
		cd_conta_contabil,
		decode(b.ie_consumo, 'A', qt_estoque, qt_estoque * -1) qt_estoque,
		0,
		0,
		0 vl_consignado,
		b.ie_consumo,
		b.cd_operacao_estoque
	from	operacao_estoque b,
		resumo_movto_estoque a
	where	a.cd_operacao_estoque	= b.cd_operacao_estoque
	and	b.ie_consumo in ('A', 'D')
	and	a.cd_estabelecimento 	= cd_estabelecimento_p
	and	dt_mesano_referencia between dt_inicio_mes_w and dt_fim_mes_w
	and	a.ie_consignado = 'S')
	group by	ie_tipo,
		cd_material,
		cd_centro_custo,
		cd_local_estoque,
		cd_conta_contabil,
		ie_consumo,
		cd_operacao_estoque;

begin

delete w_valor_consumo_mes;

dt_inicio_mes_w		:= PKG_DATE_UTILS.start_of(dt_mesano_inicio_p, 'MONTH', 0);
dt_fim_mes_w		:= pkg_date_utils.end_of(dt_mesano_fim_p, 'MONTH', 0);

open C01;
loop
	fetch C01 into		ie_tipo_w,
				cd_material_w,
				cd_centro_custo_w,
				cd_local_estoque_w,
				cd_conta_contabil_w,
				ds_centro_custo_w,
				ds_conta_contabil_w,
				qt_estoque_w,
				vl_consumo_w,
				vl_direto_w,
				vl_consignado_w,
				ie_consumo_w,
				cd_operacao_estoque_w;
		exit when C01%notfound;
		
		select w_valor_consumo_mes_seq.nextval
		into	nr_sequencia_w
		from	dual;

		if	(ie_tipo_w = 'CC') and
			(qt_estoque_w <> 0) then
			select nvl(max(vl_total_item_nf),0),
				nvl(max(qt_item_estoque),0)
			into 	vl_item_nf_w,
				qt_item_nf_w
			from	nota_fiscal_item a
			where	a.cd_material = cd_material_w
			and	a.nr_sequencia = (	select	max(a.nr_sequencia)
							from	nota_fiscal_item b,
								nota_fiscal a
							where	a.nr_sequencia = b.nr_sequencia
							and	a.dt_entrada_saida between 
								PKG_DATE_UTILS.ADD_MONTH(dt_inicio_mes_w,-3,0) and dt_fim_mes_w
							and	b.cd_material = cd_material_w);
			vl_consignado_w	:= qt_estoque_w * dividir(vl_item_nf_w, qt_item_nf_w);

			if	(vl_item_nf_w = 0) then
				select nvl(max(vl_preco_venda),0)
				into vl_preco_w
				from preco_material a
				where cd_estabelecimento = cd_estabelecimento_p
				and cd_material = cd_material_w
				and dt_inicio_vigencia = (	select	max(x.dt_inicio_vigencia)
								from	preco_material x
								where	x.cd_estabelecimento = cd_estabelecimento_p
								and	x.cd_material = cd_material_w);
			vl_consignado_w	:= qt_estoque_w * vl_preco_w;
			end if;
		end if;
		insert into w_valor_consumo_mes(
			cd_centro_custo,
			cd_local_estoque,
			cd_material,
			ds_centro_custo,
			qt_estoque,
			vl_consumo,
			vl_direto,
			vl_consignado,
			ie_opcao,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia,
			cd_conta_contabil,
			ds_conta_contabil,
			ie_consumo,
			cd_operacao_estoque)
		values(cd_centro_custo_w,
			cd_local_estoque_w,
			cd_material_w,
			ds_centro_custo_w,
			qt_estoque_w,
			vl_consumo_w,
			vl_direto_w,
			vl_consignado_w,
			ie_tipo_w,
			sysdate,
			nm_usuario_p,
			nr_sequencia_w,
			cd_conta_contabil_w,
			ds_conta_contabil_w,
			ie_consumo_w,
			cd_operacao_estoque_w);
end loop;
close C01;

commit;

end fucs_gerar_w_valor_consumo_mes;
/