create or replace
PROCEDURE Calcular_Pac_Internado
			(NM_USUARIO_P      	VARCHAR2,
                        DT_PARAMETRO_P    	DATE) IS
nr_atendimento_w				Number(10);
cd_estabelecimento_w			Number(4);
dt_entrada_w				Date;
ie_tipo_convenio_w			Number(2);
ie_tipo_atendimento_w			Number(3);
cd_proc_longa_perm_w			Number(15);
qt_dia_longa_perm_w			Number(2);
qt_perm_sus_calc_w			number(3);
qt_perm_real_calc_w			number(3);
qt_longa_perm_calc_w			number(3);
nr_aih_calc_w				number(13);
CURSOR C00 IS
	select	e.nr_atendimento,
      	     	e.dt_entrada,
		e.cd_estabelecimento,
		nvl(e.ie_tipo_convenio,0),
		e.ie_tipo_atendimento
	from 	atendimento_paciente e
	where 	e.dt_entrada    		<= fim_dia(dt_parametro_p)
	and 	e.dt_fim_conta 		is null
	and 	e.dt_alta is null
	and	e.dt_entrada > dt_parametro_p - 1
	and	e.ie_tipo_atendimento 	= 1
	and	e.ie_tipo_convenio	<> 3
	and	e.ie_tipo_convenio	<> 10
	and 	not exists(
		select x.nr_atendimento 
		from procedimento_paciente x	
		where x.nr_atendimento  = e.nr_atendimento
		and	x.cd_motivo_exc_conta is null
		and	x.cd_procedimento in (
			select distinct(cd_procedimento) 
			from tipo_acomodacao
			union
			select distinct(cd_procedimento) 
			from tipo_acomod_convenio)
		and	to_char(x.dt_procedimento,'dd/mm/yyyy') = to_char(dt_parametro_p,'dd/mm/yyyy'));
BEGIN
/* Calculo de lanšamento paciente */
OPEN C00;
LOOP
   	FETCH C00 into 
          	nr_atendimento_w,
		dt_entrada_w,
		cd_estabelecimento_w,
		ie_tipo_convenio_w,
		ie_tipo_atendimento_w;
   	exit when c00%notfound;
	BEGIN

/* Edilson em 24/03/06 OS 31345 Comentei aqui e coloquei na Calcular_Diaria_Atend_Intern
	Gerar_lancamento_automatico(
			nr_atendimento_w,
			null,
			82,
			nm_usuario_p,
			null,null,null); */

	Calcular_Diaria_Atend_Intern(
			cd_estabelecimento_w,
			nr_atendimento_w,
			dt_entrada_w,
			fim_dia(dt_parametro_p),
			nm_usuario_p,
			'N', 'N');
	END;
END LOOP;
CLOSE C00;
commit;
END Calcular_Pac_Internado;
/