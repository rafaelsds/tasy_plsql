create or replace
procedure sus_duplicar_bpa_unif(	nr_sequencia_p		sus_bpa_unif.nr_sequencia%type,
					nm_usuario_p		Varchar2) is 

qt_bpa_atend_w		number(10) := 0;
nr_atendimento_w	sus_bpa_unif.nr_atendimento%type;
ds_erro_w		varchar2(4000);
					
begin

select	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from	sus_bpa_unif
where	nr_sequencia = nr_sequencia_p;

select	count(1)
into	qt_bpa_atend_w
from	sus_bpa_unif
where	nr_atendimento = nr_atendimento_w
and	nr_bpa is not null
and	nr_interno_conta is null
and	rownum = 1;

if	(qt_bpa_atend_w = 0) then
	begin

	begin
	insert into sus_bpa_unif (	
		cd_carater_atendimento,
		cd_estabelecimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_emissao,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_bpa,
		nr_interno_conta,
		nr_sequencia)
	select	cd_carater_atendimento,
		cd_estabelecimento,
		sysdate,
		sysdate,
		dt_emissao,
		nm_usuario_p,
		nm_usuario_p,
		nr_atendimento,
		nr_bpa,
		null,
		sus_bpa_unif_seq.nextval
	from	sus_bpa_unif
	where	nr_sequencia = nr_sequencia_p;
	exception
	when others then
		ds_erro_w	:= substr(sqlerrm(sqlcode),1,4000);
		wheb_mensagem_pck.exibir_mensagem_abort(275051,'ERRO_P='||ds_erro_w);
	end;
	
	commit;
	
	end;
else
	wheb_mensagem_pck.exibir_mensagem_abort(275059);
end if;

end sus_duplicar_bpa_unif;
/
