create or replace
procedure definir_auxiliar_etapa_quimio(
		nr_seq_etapa_prod_p	number,
		cd_auxiliar_p		varchar2) is 

begin

update	can_ordem_prod
set	cd_auxiliar	= cd_auxiliar_p
where	nr_seq_etapa_prod	= nr_seq_etapa_prod_p;

commit;

end definir_auxiliar_etapa_quimio;
/