create or replace procedure        GERAR_ALERTA_TASY(
			nr_sequencia_p     		number,
			nm_usuario_p			varchar2,
			qt_seg_proc_p			out	number) is

qt_resultado_w			number(22,3);
ds_comando_w			varchar2(32000);
ds_comando_adic_w		varchar2(32000);
ds_result_adic_w		varchar2(32000);
qt_valor_minimo_w		number(22,3);
qt_valor_maximo_w		number(22,3);
nm_usuario_w			varchar2(15);
nm_usuario_ww			varchar2(15);
cd_perfil_w				number(5);
cd_perfil_ww				number(5);
ds_perfil_w				varchar2(40);
cd_estabelecimento_w	number(05,0);
nr_sequencia_w			number(10,0);
nr_seq_com_w			number(10,0)	:= null;
ie_gera_comunic_w		varchar2(01);
nr_seq_classif_w		number(10,0);
nm_alerta_w				varchar2(80);
ds_acao_w				varchar2(255);
ds_objetivo_w			varchar2(255);
ds_comunicado_w			varchar2(32000);
ds_comunicado_email_w	varchar2(32000);
dt_inicio_w				date;
ie_gerar_email_w		varchar2(01);
ds_email_usuario_w		varchar2(255);
ds_email_destino_w		varchar2(2000);
ds_assunto_w			varchar2(255);
ie_sem_result_w			Varchar2(1)	:= 'N';
qt_tamanho_fonte_w		number(05,0);
ds_fonte_padrao_w		varchar2(255);
ie_utiliza_fonte_dif_w	varchar2(01);
ie_ci_geral_w			varchar2(15);
ie_sms_w				varchar2(15);
ds_remetente_sms_w		varchar2(80);
ie_existe_classif_w		varchar2(15);
id_sms_w				number(10);
nr_celular_w			varchar2(40);
nr_seq_grupo_usuario_w	number(10,0);
nr_seq_grupo_usuario_ww	number(10,0);
cd_cargo_w				number(10,0);
nm_usuario_alertar_w	varchar2(15);
dt_exec_comando_aux_w	varchar2(20);
ds_enter_w				varchar2(10) := chr(13) || chr(10);
ds_email_destino_ww		alerta_tasy_regra.ds_email_destino%type;
cd_estab_destino_w		estabelecimento.cd_estabelecimento%type;
cd_pf_destino_w			varchar2(10);
ds_email_pac_w			varchar2(255);
nr_celular_pac_w		varchar2(40);
ds_esconder_ddi_w		varchar2(1);
ds_pf_destination_w		varchar2(255);
ds_result_adic_aux_w	varchar2(5000);
ds_remainder_email_w 	varchar2(5000);
ds_list_pf_w			dbms_sql.varchar2_table;
ds_perfil_adic_w			comunic_interna.ds_perfil_adicional%type;
nm_usuario_dest_w		comunic_interna.nm_usuario_destino%type;
ds_grupo_w					comunic_interna.ds_grupo%type;
ie_insere_comunic_w		varchar2(1);
nr_seq_ordem_w			number(10);

type colunas_cargo is record(
	cd_cargo number(10),
	cd_prefil_cargo number(5),
	nr_seq_grupo_usuario	number(10,0),
	nm_usuario_cargo	varchar(15),
	ie_ci_geral	varchar2(1)
);

type tCargo is table of colunas_cargo index by BINARY_INTEGER;
vet_cargo tCargo;
index_cargo_w	number(5)	:= 1;

cursor	c01 is
select	nm_usuario_alertar,
	cd_perfil_alertar,
	substr(obter_desc_perfil(cd_perfil_alertar),1,40),
	NVL(ie_gera_comunic,'N'),
	NVL(ie_gera_email,'N'),
	nvl(nr_seq_classif,3), /*passava fixo 3 na procedure antes de criar o campo na regra*/
	nvl(ie_ci_geral,'N'),
	nvl(ie_sms,'N'),
	nr_seq_grupo_usuario,
	cd_cargo,
	ds_email_destino,
	cd_estab_destino,
	cd_pf_destino
from	alerta_tasy_regra
where	nr_seq_alerta		= nr_sequencia_p;

cursor c02 is
select distinct a.nm_usuario
from  usuario_perfil a,
  usuario b,
  alerta_tasy_regra c
where  a.nm_usuario = b.nm_usuario
and a.cd_perfil = c.cd_perfil_alertar
and  b.ie_situacao = 'A'
and c.nr_seq_alerta = nr_sequencia_p
and c.ie_gera_email = 'S'
union
select  distinct a.nm_usuario_grupo
from  usuario_grupo a,
  usuario b,
  alerta_tasy_regra c
where  a.nm_usuario_grupo = b.nm_usuario
and  c.nr_seq_grupo_usuario = a.nr_seq_grupo
and  b.ie_situacao = 'A'
and c.nr_seq_alerta = nr_sequencia_p
and c.ie_gera_email = 'S'
union
select  distinct a.nm_usuario
from  pessoa_fisica b,
  usuario a,
  alerta_tasy_regra c
where  a.cd_pessoa_fisica = b.cd_pessoa_fisica
and c.nm_usuario_alertar = a.nm_usuario
and  a.ie_situacao = 'A'
and  b.cd_cargo = c.cd_cargo
and c.nr_seq_alerta = nr_sequencia_p
and c.ie_gera_email = 'S';

cursor C03 is
	select	a.nm_usuario
	from	usuario a
	where	a.ie_situacao	= 'A' 
	and	a.nm_usuario	= nm_usuario_w
	union
	select	distinct
		a.nm_usuario
	from	usuario_grupo b,
		usuario a
	where	a.nm_usuario	= b.nm_usuario_grupo
	and	a.ie_situacao	= 'A'
	and	b.ie_situacao	= 'A'
	and	b.nr_seq_grupo	= nr_seq_grupo_usuario_w;

cursor C04 is
	select a.cd_funcao,
		a.cd_pessoa_solicitante,
		a.nr_seq_localizacao,
		a.nr_seq_equipamento,
		a.ie_classificacao,
		a.ds_dano_breve,
		a.ds_dano,
		a.nr_grupo_trabalho,
		nr_seq_estagio
	from tipo_alerta_regra_os a,
		alerta_tasy b
	where	a.nr_seq_tipo_alerta = b.nr_seq_tipo
	and b.nr_sequencia = nr_sequencia_p
	and a.ie_situacao = 'A';

begin

ds_esconder_ddi_w := obter_valor_param_usuario(0,214,0,obter_usuario_ativo,obter_estabelecimento_ativo);

select	ds_comando,
	NVL(qt_valor_minimo,0),
	NVL(qt_valor_maximo,999999999999999),
	cd_estabelecimento,
	ds_acao_usuario,
	ds_objetivo,
	nm_alerta,
	ds_comando_adic,
	ds_remetente_sms,
	to_char(nvl(dt_exec_comando_adic,dt_ultimo_proc),'dd/mm/yyyy hh24:mi:ss')
into	ds_comando_w,
	qt_valor_minimo_w,
	qt_valor_maximo_w,
	cd_estabelecimento_w,
	ds_acao_w,
	ds_objetivo_w,
	nm_alerta_w,
	ds_comando_adic_w,
	ds_remetente_sms_w,
	dt_exec_comando_aux_w
from	alerta_tasy
where	nr_sequencia	= nr_sequencia_p;

obter_param_usuario(4000, 84, obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ie_utiliza_fonte_dif_w);
obter_param_usuario(4000, 64, obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ie_sem_result_w);
obter_param_usuario(87, 3, obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, qt_tamanho_fonte_w);
obter_param_usuario(87, 15, obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ds_fonte_padrao_w);

dt_inicio_w		:= sysdate;
ds_comando_w		:= substr(replace_macro(ds_comando_w,'@dt_exec_comando','to_date(''' || dt_exec_comando_aux_w || ''',''dd/mm/yyyy hh24:mi:ss'')'),1,32000);
Obter_Valor_dinamico(ds_comando_w, qt_resultado_w);
qt_seg_proc_p		:= (sysdate - dt_inicio_w) * 86400;
qt_resultado_w		:= NVL(qt_resultado_w, qt_valor_minimo_w);
ds_result_adic_w	:= '';

if	(ds_comando_adic_w is not null) then
	begin
	ds_comando_adic_w := substr(replace_macro(ds_comando_adic_w,'@dt_exec_comando','to_date(''' || dt_exec_comando_aux_w || ''',''dd/mm/yyyy hh24:mi:ss'')'),1,32000);
	Obter_select_concat_extendido(ds_comando_adic_w, '', ',', ds_result_adic_w);
	--Changes made for SO 2015528
	if	(instr(ds_comando_adic_w, '#PF') > 0) then
		obtain_reminder_letter_list(ds_comando_adic_w, ds_list_pf_w);
	end if;
	--Changes made for SO 2015528
	end;
end if;

update	alerta_tasy
set	dt_exec_comando_adic = sysdate
where	nr_sequencia = nr_sequencia_p;
commit;

if	((ds_result_adic_w is not null) and
	(qt_resultado_w	< qt_valor_minimo_w) or
	(qt_resultado_w	> qt_valor_maximo_w)) then

	open C01;
	loop
	fetch C01 into
		nm_usuario_w,
		cd_perfil_w,
		ds_perfil_w,
		ie_gera_comunic_w,
		ie_gerar_email_w,
		nr_seq_classif_w,
		ie_ci_geral_w,
		ie_sms_w,
		nr_seq_grupo_usuario_w,
		cd_cargo_w,
		ds_email_destino_ww,
		cd_estab_destino_w,
		cd_pf_destino_w;
	exit when C01%notfound;
		begin

		if	(nr_seq_classif_w = 3) then
			begin
			select 	'S'
			into	ie_existe_classif_w
			from	comunic_interna_classif
			where	nr_sequencia = 3;
			exception
			when others then
				nr_seq_classif_w := null;
			end;
		end if;
		
		ds_assunto_w		:= substr(obter_desc_expressao_idioma(283347,null,get_seq_language_establishment(cd_estabelecimento_w)) ||': ' || nm_alerta_w,1,255);
		ds_comunicado_email_w	:= substr(substr(obter_desc_expressao_idioma(283347,null,get_seq_language_establishment(cd_estabelecimento_w)),1,20) || ': ' || nm_alerta_w || ds_enter_w || ds_enter_w ||
					 	substr(obter_desc_expressao_idioma(294566,null,get_seq_language_establishment(cd_estabelecimento_w)),1,20) || ': ' || ds_objetivo_w || ds_enter_w || ds_enter_w ||
					 	substr(obter_desc_expressao_idioma(283090,null,get_seq_language_establishment(cd_estabelecimento_w)),1,20) || ': ' || ds_acao_w || ds_enter_w || ds_enter_w ||
					 	substr(obter_desc_expressao_idioma(297817,null,get_seq_language_establishment(cd_estabelecimento_w)),1,20) || ': ' || qt_resultado_w,1,32000);
		
		if	(ds_result_adic_w is not null) then
			if	(nvl(ie_sem_result_w,'N') = 'N') then
				ds_comunicado_email_w	:= substr(ds_comunicado_email_w || ds_enter_w || ds_enter_w || substr(obter_desc_expressao_idioma(345870,null,get_seq_language_establishment(cd_estabelecimento_w)),1,80) || ': ' || ds_result_adic_w,1,32000);
			else
				ds_comunicado_email_w	:= substr(ds_result_adic_w,1,32000);
			end if;
		end if;
			
		
		ds_comunicado_w	:= ds_comunicado_email_w;
		
		if	(nvl(ie_utiliza_fonte_dif_w,'N') = 'S') then
			begin
			ds_comunicado_w	:= substr(replace(ds_comunicado_w, ds_enter_w, ' \par '),1,32000);
			ds_comunicado_w	:= substr(replace(ds_comunicado_w, chr(10) || chr(13), ' \par '),1,32000);
			ds_comunicado_w	:= substr(replace(ds_comunicado_w, chr(13), ' \par '),1,32000);
			ds_comunicado_w	:= substr(replace(ds_comunicado_w, chr(10), ' \par '),1,32000);
			ds_comunicado_w	:= substr('{\rtf1\ansi\deff0{\fonttbl{\f0\fnil\fcharset0 ' || 
								nvl(ds_fonte_padrao_w,'MS Sans Serif ') || ';}{\f1\fnil ' || 
								nvl(ds_fonte_padrao_w,'MS Sans Serif ') || ';}}'||
						'{\colortbl ;\red0\green0\blue128;}' ||
						'\viewkind4\uc1\pard\cf1\lang1046\f0\fs' || (nvl(qt_tamanho_fonte_w,8) * 2) ||
						ds_comunicado_w || '\b0 \par }',1,32000);
			end;
		end if;

		if	(ie_gera_comunic_w = 'S') and
			((nvl(nm_usuario_w,'X') <> 'X') or
			(nvl(cd_perfil_w,0) <> 0) or
			(nvl(nr_seq_grupo_usuario_w,0) <> 0) or
			(nvl(cd_cargo_w,0) <> 0) or
			(ie_ci_geral_w = 'S')) then
			
			ie_insere_comunic_w	:= 'S';
			
			if	(nvl(cd_cargo_w,0) <> 0)  then
				
				vet_cargo(index_cargo_w).cd_cargo := cd_cargo_w;
				vet_cargo(index_cargo_w).cd_prefil_cargo	:= cd_perfil_w;
				vet_cargo(index_cargo_w).nr_seq_grupo_usuario	:= nr_seq_grupo_usuario_w;
				vet_cargo(index_cargo_w).nm_usuario_cargo	:=	nm_usuario_w;
				vet_cargo(index_cargo_w).ie_ci_geral	:= ie_ci_geral_w;
				index_cargo_w	:= index_cargo_w + 1;
			
			elsif(ie_ci_geral_w <> 'S') then
				if	(nvl(nm_usuario_w,'X') <> 'X') then
					nm_usuario_dest_w	:=  nm_usuario_dest_w || nm_usuario_w || ',';
				end if;

				if(nvl(cd_perfil_w,0) <> 0) then 
					ds_perfil_adic_w	:=  ds_perfil_adic_w || cd_perfil_w || ',';
				end if;
		
				if(nvl(nr_seq_grupo_usuario_w,0) <> 0) then
					ds_grupo_w	:= ds_grupo_w || nr_seq_grupo_usuario_w || ',';
				end if;
			else
				select comunic_interna_seq.nextval
				into nr_seq_Com_w
				from dual;
	
				insert into comunic_interna(
					dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					dt_atualizacao,
					ie_geral,
					nm_usuario_destino,
					ds_perfil_adicional,
					nr_sequencia,
					ie_gerencial,
					nr_seq_classif,
					dt_liberacao,
					cd_estab_destino,
					ds_grupo,
					cd_cargo)
				values(	sysdate,
					substr(nm_alerta_w,1,80),
					ds_comunicado_w,
					nm_usuario_p,
					sysdate,
					ie_ci_geral_w,
					nm_usuario_w,
					cd_perfil_w,
					nr_seq_com_w,
					'N',
					nr_seq_classif_w,
					sysdate,
					nvl(cd_estab_destino_w,decode(ie_ci_geral_w,'S',cd_estabelecimento_w,null)),
					nr_seq_grupo_usuario_w,
					cd_cargo_w);
					
				open C03;
				loop
				fetch C03 into	
					nm_usuario_alertar_w;
				exit when C03%notfound;
					begin
					select	alerta_tasy_ocor_seq.nextval
					into	nr_sequencia_w
					from	dual;
		
					insert into alerta_tasy_ocor(
							nr_sequencia,
							cd_estabelecimento,
							nr_seq_alerta,
							dt_atualizacao,
							nm_usuario,
							dt_alerta,
							qt_result_consulta,
							ie_usuario_ciente,
							nm_usuario_alertado,
							nr_seq_comunic)
						values(	nr_sequencia_w,
							cd_estabelecimento_w,
							nr_sequencia_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							qt_resultado_w,
							'N',
							nm_usuario_alertar_w,
							nr_seq_com_w);
					end;
				end loop;
				close C03;
			end if;
		end if;
		
		if	(cd_pf_destino_w is not null) then
			begin
			
			if(ie_gerar_email_w = 'S' AND ds_list_pf_w.COUNT = 0) then
				begin
				ds_email_pac_w := obter_email_pf(cd_pf_destino_w); --email do paciente
				if	(ds_email_pac_w is not null) then	
					begin
					Enviar_Email(ds_assunto_w, ds_comunicado_email_w, null, ds_email_pac_w, nm_usuario_p,'M');
					end;
				end if;
				end;
			end if;
			if(ie_sms_w = 'S') then
				begin
				select  nr_ddd_celular || nr_telefone_celular
				into    nr_celular_pac_w	--celular do paciente
				from    pessoa_fisica
				where 	cd_pessoa_fisica = cd_pf_destino_w;
				if	(nr_celular_pac_w is not null) then	
					begin
					wheb_sms.enviar_sms(ds_remetente_sms_w,nr_celular_pac_w,substr(ds_comunicado_email_w,1,500),nm_usuario_alertar_w,id_sms_w);
					end;
				end if;
			end;
		end if;
			end;	
		end if;
		
		if	(ie_gerar_email_w = 'S' AND ds_list_pf_w.COUNT = 0) then
			begin
			if	(nm_usuario_w is not null) then
				select	max(ds_email)
				into	ds_email_usuario_w
				from	usuario
				where	nm_usuario = nm_usuario_w
				and	ie_situacao = 'A';
				begin
				Enviar_Email(ds_assunto_w, ds_comunicado_email_w, null, ds_email_usuario_w, nm_usuario_p,'M');
				exception
				when others then
					null;
				end;
			end if;
			
			if	(nvl(ds_email_destino_ww,'X') <> 'X') then
				begin
					Enviar_Email(ds_assunto_w, ds_comunicado_email_w, null, ds_email_destino_ww, nm_usuario_p,'M');
					exception
					when others then
						null;
				end;		
			end if;
			end;
		end if;

		if	(ie_sms_w = 'S') and
			(nvl(ds_remetente_sms_w,'X') <> 'X') and
			(cd_perfil_w is null) then
			begin	
			
			open C03;
			loop
			fetch C03 into	
				nm_usuario_alertar_w;
			exit when C03%notfound;
				begin

				select	substr(decode(ds_esconder_ddi_w,'S',p.nr_ddd_celular||p.nr_telefone_celular,p.nr_ddi_celular||p.nr_ddd_celular||p.nr_telefone_celular),1,40) nr_telefone_celular
				into	nr_celular_w
				from	pessoa_fisica p,
					usuario u
				where	u.cd_pessoa_fisica 	= p.cd_pessoa_fisica
				and	u.nm_usuario 	= nm_usuario_alertar_w
				and	u.ie_situacao = 'A';
				
				if	(nvl(trim(nr_celular_w),'X') <> 'X') then
					begin
					wheb_sms.enviar_sms(ds_remetente_sms_w,nr_celular_w,substr(ds_comunicado_email_w,1,500),nm_usuario_alertar_w,id_sms_w);
					exception
					when others then
						null;
					end;
				end if;
				end;
			end loop;
			close C03;
			end;
		end if;
		end;
		--Changes made for SO 2015528
		if	(ie_gerar_email_w = 'S' and ds_list_pf_w.COUNT > 0) then
			FOR i IN ds_list_pf_w.first..ds_list_pf_w.last LOOP
				ds_remainder_email_w := null;
				ds_remainder_email_w := ds_list_pf_w(i);
					if(instr(ds_remainder_email_w, '#PF') > 0 ) then
						
						cd_pf_destino_w := SUBSTR (ds_remainder_email_w, instr(ds_remainder_email_w, '#PF') + 3,  instr(ds_remainder_email_w, 'PF#') - instr(ds_remainder_email_w, '#PF') - 3);
						
						ds_remainder_email_w := replace(ds_remainder_email_w, '#PF' ||  cd_pf_destino_w || 'PF#' , '');
						
						if(cd_pf_destino_w is not null) then 
							ds_email_pac_w := obter_email_pf(cd_pf_destino_w);				
							if	(ie_gerar_email_w = 'S' and ds_list_pf_w.COUNT > 0) then
					
								if	(ds_email_pac_w is not null) then	
									begin
										Enviar_Email(nm_alerta_w, ds_remainder_email_w, null, ds_email_pac_w, nm_usuario_p,'M');
									end;
								end if;
							end if;					
						end if;
					end if;
			end loop;
		end if;
		--Changes made for SO 2015528
	end loop;
	close C01;
	
	if	(ds_list_pf_w.count = 0) then
		open C02;
			loop
			fetch C02 into
				nm_usuario_alertar_w;
			exit when C02%notfound;
				begin
				select	max(ds_email)
				into	ds_email_usuario_w
				from	usuario
				where	nm_usuario = nm_usuario_alertar_w
				and	ie_situacao = 'A';
				
				if	(nvl(ds_email_usuario_w,'X') <> 'X') then
					begin
					Enviar_Email(ds_assunto_w, ds_comunicado_email_w, null, ds_email_usuario_w, nm_usuario_p,'M');
					exception
					when others then
						null;
					end;
				end if;
				end;
			end loop;
			close c02;
	end if;
	
	if	(ie_insere_comunic_w = 'S') then
		if	(vet_cargo.count > 0) then
			for i in vet_cargo.first..vet_cargo.last loop
				select comunic_interna_seq.nextval
				into nr_seq_Com_w
				from dual;
		
				insert into comunic_interna(
					dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					dt_atualizacao,
					ie_geral,
					nm_usuario_destino,
					ds_perfil_adicional,
					nr_sequencia,
					ie_gerencial,
					nr_seq_classif,
					dt_liberacao,
					cd_estab_destino,
					ds_grupo,
					cd_cargo)
				values(	sysdate,
					substr(nm_alerta_w,1,80),
					ds_comunicado_w,
					nm_usuario_p,
					sysdate,
					vet_cargo(i).ie_ci_geral,
					vet_cargo(i).nm_usuario_cargo,
					vet_cargo(i).cd_prefil_cargo,
					nr_seq_com_w,
					'N',
					nr_seq_classif_w,
					sysdate,
					nvl(cd_estab_destino_w,decode(ie_ci_geral_w,'S',cd_estabelecimento_w,null)),
					vet_cargo(i).nr_seq_grupo_usuario,
					vet_cargo(i).cd_cargo);
					
				open C03;
				loop
				fetch C03 into	
					nm_usuario_alertar_w;
				exit when C03%notfound;
					begin
					select	alerta_tasy_ocor_seq.nextval
					into	nr_sequencia_w
					from	dual;
		
					insert into alerta_tasy_ocor(
							nr_sequencia,
							cd_estabelecimento,
							nr_seq_alerta,
							dt_atualizacao,
							nm_usuario,
							dt_alerta,
							qt_result_consulta,
							ie_usuario_ciente,
							nm_usuario_alertado,
							nr_seq_comunic)
						values(	nr_sequencia_w,
							cd_estabelecimento_w,
							nr_sequencia_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							qt_resultado_w,
							'N',
							nm_usuario_alertar_w,
							nr_seq_com_w);
					end;
				end loop;
				close C03;	
			end loop;
		end if;
		if((nvl(nm_usuario_dest_w,'X') <> 'X') or
			(nvl(ds_perfil_adic_w,'X') <> 'X') or
			(nvl(ds_grupo_w,'X') <> 'X')) then
			
			select comunic_interna_seq.nextval
			into nr_seq_Com_w
			from dual;

			insert into comunic_interna(
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				nr_seq_classif,
				dt_liberacao,
				cd_estab_destino,
				ds_grupo,
				cd_cargo)
			values(	sysdate,
				substr(nm_alerta_w,1,80),
				ds_comunicado_w,
				nm_usuario_p,
				sysdate,
				'N',
				nm_usuario_dest_w,
				ds_perfil_adic_w,
				nr_seq_com_w,
				'N',
				nr_seq_classif_w,
				sysdate,
				nvl(cd_estab_destino_w,decode(ie_ci_geral_w,'S',cd_estabelecimento_w,null)),
				ds_grupo_w,
				cd_cargo_w);
				
			open C03;
			loop
			fetch C03 into	
				nm_usuario_alertar_w;
			exit when C03%notfound;
				begin
				select	alerta_tasy_ocor_seq.nextval
				into	nr_sequencia_w
				from	dual;
		
				insert into alerta_tasy_ocor(
						nr_sequencia,
						cd_estabelecimento,
						nr_seq_alerta,
						dt_atualizacao,
						nm_usuario,
						dt_alerta,
						qt_result_consulta,
						ie_usuario_ciente,
						nm_usuario_alertado,
						nr_seq_comunic)
					values(	nr_sequencia_w,
						cd_estabelecimento_w,
						nr_sequencia_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						qt_resultado_w,
						'N',
						nm_usuario_alertar_w,
						nr_seq_com_w);
				end;
			end loop;
			close C03;
		end if;
	end if;
	commit;
	
	/* Apos enviar todos os e-mails e CI - Verificar se existem regras para abertura de OS e abre elas*/

	for c_regra_os_w in C04 loop
		begin

		select	man_ordem_servico_seq.nextval
		into	nr_seq_ordem_w
		from	dual;
		
		insert	into man_ordem_servico(
			nr_sequencia,
			nr_seq_localizacao,
			nr_seq_equipamento,
			cd_pessoa_solicitante,
			dt_ordem_servico,
			ie_prioridade,
			ie_parado,
			ds_dano_breve,
			dt_atualizacao,
			nm_usuario,
			dt_inicio_desejado,
			ds_dano,
			dt_inicio_previsto,
			dt_inicio_real,
			ie_tipo_ordem,
			ie_status_ordem,
			nr_grupo_trabalho,
			nr_seq_estagio,
			cd_funcao,
			ie_classificacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_origem_os)
		values(nr_seq_ordem_w,
			c_regra_os_w.nr_seq_localizacao,
			c_regra_os_w.nr_seq_equipamento,
			c_regra_os_w.cd_pessoa_solicitante,
			sysdate,
			'M',
			'N',
			c_regra_os_w.ds_dano_breve,
			sysdate,
			nm_usuario_p,
			sysdate,
			c_regra_os_w.ds_dano,
			sysdate,
			sysdate,
			1,
			1,
			c_regra_os_w.nr_grupo_trabalho,
			c_regra_os_w.nr_seq_estagio,
			c_regra_os_w.cd_funcao,
			c_regra_os_w.ie_classificacao,
			sysdate,
			nm_usuario_p,
			'4');
		
		commit;	
		
		end;
	end loop;

end if;

exception
when others then
	null;

end Gerar_Alerta_Tasy;
/
