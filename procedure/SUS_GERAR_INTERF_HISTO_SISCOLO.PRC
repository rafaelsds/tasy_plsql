create or replace
procedure sus_gerar_interf_histo_siscolo(	nr_seq_protocolo_p	number ,
						nm_usuario_p		varchar2 )as

cd_cnpj_unidade_saude_w	varchar2(14);
cd_profissional_w		varchar2(10);
nr_interno_conta_w		number(10);
nr_atendimento_w		number(10);
nr_seq_siscolo_w		number(10);
dt_emissao_w		date;
dt_liberacao_w		date;
nr_seq_unidade_saude_w	number(10);
cd_estabelecimento_w	number(5);
cd_pessoa_fisica_w	varchar2(10);
c_cnes_w		varchar2(7);
c_exame_w		varchar2(14);
c_id_nome_w		varchar2(70);
c_nomemae_w		varchar2(70);
c_id_apel_w		varchar2(30);
d_dtnasc_w		date;
c_id_idad_w		varchar2(2);
c_id_ident_w		varchar2(12);
c_id_emis_w		varchar2(5);
c_id_ufide_w		varchar2(2);
c_id_cic_w		varchar2(11);
c_id_esco_w		varchar2(1);
c_endereco_w		varchar2(35);
c_numero_w		varchar2(6);
c_complem_w		varchar2(15);
c_bairro_w		varchar2(15);
c_id_uf_w		varchar2(2);
c_ibge_w			varchar2(7);
c_id_cep_w		varchar2(8);
c_id_refe_w		varchar2(35);
c_id_fone_w		varchar2(11);
c_id_sus_w		varchar2(15);
c_us_uf_w		varchar2(2);
c_us_ibge_w		varchar2(7);
c_us_cnes_w		varchar2(7);
c_us_nome_w		varchar2(40);
c_pron_w			varchar2(12);
c_cit_esc_w		varchar2(1);
c_cit_gla_w		varchar2(1);
c_cit_ind_w		varchar2(1);
c_cit_esca_w		varchar2(1);
c_cit_glan_w		varchar2(1);
c_cit_outr_w		varchar2(40);
c_col_colp_w		varchar2(1);
c_col_pnic_w		varchar2(1);
c_col_pinv_w		varchar2(1);
c_col_frio_w		varchar2(1);
c_col_cure_w		varchar2(1);
c_col_caf_w		varchar2(1);
c_col_exer_w		varchar2(1);
c_col_rcan_w		varchar2(1);
c_col_biop_w		varchar2(1);
c_col_adic_w		varchar2(50);
c_res_tipo_w		varchar2(1);
c_res_macr_w		varchar2(100);
c_res_biop_w		varchar2(1);
c_res_frag_w		varchar2(2);
c_res_peca_w		varchar2(1);
c_res_tam1_w		varchar2(4);
c_res_tam2_w		varchar2(4);
c_res_marg_w		varchar2(4);
c_res_loca_w		varchar2(1);
c_ben_meta_w		varchar2(1);
c_ben_poli_w		varchar2(1);
c_ben_cerv_w		varchar2(1);
c_ben_alte_w		varchar2(1);
c_neo_nica_w		varchar2(1);
c_neo_aden_w		varchar2(1);
c_neo_outr_w		varchar2(40);
c_dif_grau_w		varchar2(1);
c_ext_prof_w		varchar2(4);
c_ext_vasc_w		varchar2(1);
c_ext_peri_w		varchar2(1);
c_ext_para_w		varchar2(1);
c_ext_corp_w		varchar2(1);
c_ext_vagi_w		varchar2(1);
c_ext_lexa_w		varchar2(2);
c_ext_lcom_w		varchar2(2);
c_mar_marg_w		varchar2(1);
c_diag_des_w		varchar2(100);
c_com_frag_w		varchar2(2);
c_com_bloc_w		varchar2(2);
c_mat_ins_w		varchar2(1);
c_mat_insa_w		varchar2(70);
d_exame_w		date;
d_recebe_w		date;
d_libera_w		date;
c_pat_cns_w		varchar2(15);
c_pat_cic_w		varchar2(11);
c_erro_w			varchar2(100);
c_id_raca_w		varchar2(2);
c_etnia_w		varchar2(3);
c_nacional_w		varchar2(3);

cursor c01 is
	select	a.nr_sequencia,
		substr(a.cd_cnpj_unidade_saude,1,14),
		a.cd_estabelecimento,
		substr(a.cd_profissional,1,10),
		trunc(a.dt_emissao),
		trunc(a.dt_liberacao),
		a.nr_seq_unidade_saude,
		a.nr_atendimento,
		b.nr_interno_conta
	from	conta_paciente b,
		siscolo_atendimento a
	where	a.nr_atendimento 	= b.nr_atendimento
	and	a.dt_liberacao   	= (	select 	max(x.dt_liberacao)
					from	siscolo_atendimento x
					where	x.nr_atendimento = a.nr_atendimento)
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and	(((select	count(*)
		from	siscolo_histo_result_cito t
		where	t.nr_seq_siscolo = a.nr_sequencia) > 0) and
		((select	count(*)
		from	siscolo_histo_result r
		where	r.nr_seq_siscolo = a.nr_sequencia) > 0) and
		((select	count(*)
		from	siscolo_histo_continuacao n
		where	n.nr_seq_siscolo = a.nr_sequencia) > 0))
	order by nr_sequencia;

begin

delete 	from w_interf_hiscolo
where	nr_seq_protocolo = nr_seq_protocolo_p;

open c01;
loop
fetch c01 into
	nr_seq_siscolo_w,
	cd_cnpj_unidade_saude_w,
	cd_estabelecimento_w,
	cd_profissional_w,
	dt_emissao_w,
	dt_liberacao_w,
	nr_seq_unidade_saude_w,
	nr_atendimento_w,
	nr_interno_conta_w;
exit when c01%notfound;
	begin

	begin
	select	lpad(max(substr(sus_obter_cnes_estab_unif(a.cd_estabelecimento,'B'),1,7)),7,'0') c_cnes,
		max(a.nr_sequencia) c_exame,
		max(upper(substr(obter_nome_pf(c.cd_pessoa_fisica),1,70))) c_id_nome,
		max(upper(substr(obter_compl_pf(c.cd_pessoa_fisica,5,'N'),1,60))) c_nomemae,
		' ' c_id_apel,
		max(to_date(obter_dados_pf(c.cd_pessoa_fisica,'DN'),'dd/mm/yyyy')) d_dtnasc,
		max(substr(obter_dados_pf(c.cd_pessoa_fisica,'I'),1,2)) c_id_idad,
		max(substr(obter_dados_pf(c.cd_pessoa_fisica,'RG'),1,12)) c_id_ident,
		max(substr(obter_dados_pf(c.cd_pessoa_fisica,'O'),1,5)) c_id_emis,
		max(decode(obter_dados_pf(c.cd_pessoa_fisica,'RG'),'','',upper(substr(sus_obter_codigo_uf(obter_compl_pf(c.cd_pessoa_fisica,1,'UF')),1,2)))) c_id_ufide,
		max(substr(obter_dados_pf(c.cd_pessoa_fisica,'CPF'),1,11)) c_id_cic,
		max(decode(obter_dados_pf(c.cd_pessoa_fisica,'GI'),'Analfabeto','1','Primeiro  Grau  Incompleto','2','Primeiro Grau','3','Segundo Grau','4','Superior','5','0')) c_id_esco,
		max(upper(substr(elimina_caractere_especial(obter_compl_pf(c.cd_pessoa_fisica,1,'EN')),1,35))) c_endereco,
		max(substr(elimina_caractere_especial(obter_compl_pf(c.cd_pessoa_fisica,1,'NR')),1,6)) c_id_numero,
		max(upper(substr(elimina_caractere_especial(obter_compl_pf(c.cd_pessoa_fisica,1,'CO')),1,15))) c_complem,
		max(upper(substr(obter_compl_pf(c.cd_pessoa_fisica,1,'B'),1,15))) c_bairro,
		max(upper(substr(sus_obter_codigo_uf(obter_compl_pf(c.cd_pessoa_fisica,1,'UF')),1,2))) c_id_uf,
		max(substr(obter_compl_pf(c.cd_pessoa_fisica,1,'CDM')||calcula_digito('MODULO10',obter_compl_pf(c.cd_pessoa_fisica,1,'CDM')),1,7)) c_ibge,
		max(substr(obter_compl_pf(c.cd_pessoa_fisica,1,'CEP'),1,8)) c_id_cep,
		' ' c_id_refe,
		max(substr(obter_compl_pf(c.cd_pessoa_fisica,1,'T'),1,11)) c_id_fone,
		max(substr(obter_dados_pf(c.cd_pessoa_fisica,'CNS'),1,15)) c_id_sus,
		max(substr(sus_obter_codigo_uf(obter_dados_unid_saude_colo(a.nr_sequencia,'UF')),1,2)) c_us_uf,
		max(substr(obter_dados_unid_saude_colo(a.nr_sequencia,'CDM'),1,7)) c_us_ibge,
		lpad(max(substr(obter_dados_unid_saude_colo(a.nr_sequencia,'CNES'),1,7)),7,'0') c_us_cnes,
		max(substr(obter_dados_unid_saude_colo(a.nr_sequencia,'NM'),1,40)) c_us_nome,
		max(obter_prontuario_paciente(c.cd_pessoa_fisica)) c_pron,
		max(c.cd_pessoa_fisica),
		max(nvl(decode(obter_dados_pf(c.cd_pessoa_fisica,'CP'),'Amarela','04','Branca','01','Indio','05','Negra','02','Parda','03','Sem informação','99'),'99')) c_id_raca,
		nvl(max(substr(decode(obter_dados_pf(c.cd_pessoa_fisica,'CP'),'5',sus_obter_etnia(c.cd_pessoa_fisica, 'C'),'0'),2,3)),'0') c_etnia,
		lpad(max(obter_dados_pf(c.cd_pessoa_fisica,'NC')),3,'0') c_nacional
	into	c_cnes_w,
		c_exame_w,
		c_id_nome_w,
		c_nomemae_w,
		c_id_apel_w,
		d_dtnasc_w,
		c_id_idad_w,
		c_id_ident_w,
		c_id_emis_w,
		c_id_ufide_w,
		c_id_cic_w,
		c_id_esco_w,
		c_endereco_w,
		c_numero_w,
		c_complem_w,
		c_bairro_w,
		c_id_uf_w,
		c_ibge_w,
		c_id_cep_w,
		c_id_refe_w,
		c_id_fone_w,
		c_id_sus_w,
		c_us_uf_w,
		c_us_ibge_w,
		c_us_cnes_w,
		c_us_nome_w,
		c_pron_w,
		cd_pessoa_fisica_w,
		c_id_raca_w,
		c_etnia_w,
		c_nacional_w
	from	atendimento_paciente c,
		conta_paciente b,
		siscolo_atendimento a
	where	a.nr_atendimento = b.nr_atendimento
	and	b.nr_atendimento = c.nr_atendimento
	and	a.nr_sequencia   = nr_seq_siscolo_w;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(223246,'NR_SEQ_SISCOLO='||nr_seq_siscolo_w||';NR_ATENDIMENTO='||nr_atendimento_w);
		/*Problemas em obter os dados do paciente do laudo: '||nr_seq_siscolo_w||' no antedimento: '||nr_atendimento_w*/
	end;

	begin
	select	decode(ie_atipicas_escamosas,'P','1','N','2','0'),
		decode(ie_atipicas_glandulares,'P','1','N','2','0'),
		decode(ie_atipicas_indefinidas,'P','1','N','2','0'),
		decode(ie_atipias_escamosas,'H','1','N','2','M','3','C','4','0'),
		decode(ie_atipias_gla_adeno,'A','1','C','2','E','3','S','4','0'),
		substr(ds_outro_diagnostico,1,40),
		decode(ie_coloscopia,'N','3','A','2','I','1','0'),
		decode(ie_sugestiva_nic,'S','3','0'),
		decode(ie_sugestiva_inv,'S','3','0'),
		decode(ie_biopsia_fio,'S','3','0'),
		decode(ie_curetagem_endo,'S','3','0'),
		decode(ie_proc_caf,'S','3','0'),
		decode(ie_exerese_alargada,'S','3','0'),
		decode(ie_retirada_canal,'S','3','0'),
		decode(ie_caf_biopsia,'S','3','0'),
		substr(ds_info_adicionais,1,50),
		trunc(dt_exame_histo)
	into	c_cit_esc_w,
		c_cit_gla_w,
		c_cit_ind_w,
		c_cit_esca_w,
		c_cit_glan_w,
		c_cit_outr_w,
		c_col_colp_w,
		c_col_pnic_w,
		c_col_pinv_w,
		c_col_frio_w,
		c_col_cure_w,
		c_col_caf_w,
		c_col_exer_w,
		c_col_rcan_w,
		c_col_biop_w,
		c_col_adic_w,
		d_exame_w
	from	siscolo_histo_result_cito
	where	nr_seq_siscolo = nr_seq_siscolo_w;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(223247,'NR_SEQ_SISCOLO='||nr_seq_siscolo_w||';NR_ATENDIMENTO='||nr_atendimento_w);
		/*Problemas em obter os dados do resultado citopatológico do laudo: '||nr_seq_siscolo_w||' no antedimento: '||nr_atendimento_w*/
	end;

	begin
	select	decode(ie_biopsia,'S','1'),
		substr(ds_macroscopia,1,100),
		decode(ie_biopsia_fragmento,'S','3','0'),
		substr(nr_fragmento_biopsia,1,2),
		decode(ie_cicurgia_tumor,'S','3','0'),
		substr(qt_tamanho_tumor1,1,4),
		substr(qt_tamanho_tumor2,1,4),
		substr(qt_distancia_margem,1,4),
		decode(ie_localizacao,'T','1','D','2','J','3','0'),
		decode(ie_metaplasia_esc,'S','3','0'),
		decode(ie_polipo_endocer,'S','3','0'),
		decode(ie_cervicite_cro,'S','3','0'),
		decode(ie_citoarquiteturais,'S','3','0'),
		decode(ie_lesao_carater,'L','1','M','3','A','3','E','4','I','5','C','6','V','7','Z','8','0'),
		decode(ie_adenocarcionoma,'S','1','M','2','V','3','0'),
		substr(ds_neoplasia_maligna,1,40)
	into	c_res_tipo_w,
		c_res_macr_w,
		c_res_biop_w,
		c_res_frag_w,
		c_res_peca_w,
		c_res_tam1_w,
		c_res_tam2_w,
		c_res_marg_w,
		c_res_loca_w,
		c_ben_meta_w,
		c_ben_poli_w,
		c_ben_cerv_w,
		c_ben_alte_w,
		c_neo_nica_w,
		c_neo_aden_w,
		c_neo_outr_w
	from	siscolo_histo_result
	where	nr_seq_siscolo = nr_seq_siscolo_w;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(223248,'NR_SEQ_SISCOLO='||nr_seq_siscolo_w||';NR_ATENDIMENTO='||nr_atendimento_w);
		/*Problemas em obter os dados do resultado histopatológico do laudo: '||nr_seq_siscolo_w||' no antedimento: '||nr_atendimento_w*/
	end;

	begin
	select	decode(ie_grau_deferenciacao,'N','0','B','1','M','2','P','3','I','4','5'),
		substr(nr_profundidade,1,4),
		decode(ie_vascular,'S','3','N','1','0'),
		decode(ie_peri_neural,'S','3','N','1','0'),
		decode(ie_parametrial,'S','3','N','1','0'),
		decode(ie_corpo_uterino,'S','3','N','1','0'),
		decode(ie_vagina,'S','3','N','1','0'),
		substr(nr_linf_examinados,1,2),
		substr(nr_linf_comprometidos,1,2),
		decode(ie_margem_cirurgica,'L','1','C','2','I','3','0'),
		substr(ds_desc_diagnostico,1,100),
		substr(nr_fragmentos,1,2),
		substr(nr_blocos,1,2),
		decode(ie_mat_insatisfatorio,'S','3','0'),
		substr(DS_MOTIVO_INSTISF,1,70),
		trunc(dt_liberacao),
		substr(cd_cns_resp_result,1,15),
		substr(cd_cpf_resp_result,1,11),
		'' c_erro
	into	c_dif_grau_w,
		c_ext_prof_w,
		c_ext_vasc_w,
		c_ext_peri_w,
		c_ext_para_w,
		c_ext_corp_w,
		c_ext_vagi_w,
		c_ext_lexa_w,
		c_ext_lcom_w,
		c_mar_marg_w,
		c_diag_des_w,
		c_com_frag_w,
		c_com_bloc_w,
		c_mat_ins_w,
		c_mat_insa_w,
		d_libera_w,
		c_pat_cns_w,
		c_pat_cic_w,
		c_erro_w
	from	siscolo_histo_continuacao
	where	nr_seq_siscolo = nr_seq_siscolo_w;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(223250,'NR_SEQ_SISCOLO='||nr_seq_siscolo_w||';NR_ATENDIMENTO='||nr_atendimento_w);
		/*Problemas em obter os dados da continuação I do resultado histopatológico do laudo: '||nr_seq_siscolo_w||' no antedimento: '||nr_atendimento_w*/
	end;

	insert into w_interf_hiscolo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		nr_seq_protocolo,
		nr_seq_siscolo,
		nr_interno_conta,
		nr_atendimento,
		cd_profissional,
		cd_cnpj_unidade_saude,
		dt_emissao,
		dt_liberacao,
		nr_seq_unidade_saude,
		c_cnes,
		c_exame,
		c_id_nome,
		c_nomemae,
		c_id_apel,
		d_dtnasc,
		c_id_idad,
		c_id_ident,
		c_id_emis,
		c_id_ufide,
		c_id_cic,
		c_id_esco,
		c_endereco,
		c_numero,
		c_complem,
		c_bairro,
		c_id_uf,
		c_ibge,
		c_id_cep,
		c_id_refe,
		c_id_fone,
		c_id_sus,
		c_us_uf,
		c_us_ibge,
		c_us_cnes,
		c_us_nome,
		c_pron,
		c_cit_esc,
		c_cit_gla,
		c_cit_ind,
		c_cit_esca,
		c_cit_glan,
		c_cit_outr,
		c_col_colp,
		c_col_pnic,
		c_col_pinv,
		c_col_frio,
		c_col_cure,
		c_col_caf,
		c_col_exer,
		c_col_rcan,
		c_col_biop,
		c_col_adic,
		c_res_tipo,
		c_res_macr,
		c_res_biop,
		c_res_frag,
		c_res_peca,
		c_res_tam1,
		c_res_tam2,
		c_res_marg,
		c_res_loca,
		c_ben_meta,
		c_ben_poli,
		c_ben_cerv,
		c_ben_alte,
		c_neo_nica,
		c_neo_aden,
		c_neo_outr,
		c_dif_grau,
		c_ext_prof,
		c_ext_vasc,
		c_ext_peri,
		c_ext_para,
		c_ext_corp,
		c_ext_vagi,
		c_ext_lexa,
		c_ext_lcom,
		c_mar_marg,
		c_diag_des,
		c_com_frag,
		c_com_bloc,
		c_mat_ins,
		c_mat_insa,
		d_exame,
		d_recebe,
		d_libera,
		c_pat_cns,
		c_pat_cic,
		c_erro,
		c_id_raca,
		c_etnia,
		c_nacional)
	values(	w_interf_hiscolo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_w,
		nr_seq_protocolo_p,
		nr_seq_siscolo_w,
		nr_interno_conta_w,
		nr_atendimento_w,
		cd_profissional_w,
		cd_cnpj_unidade_saude_w,
		dt_emissao_w,
		dt_liberacao_w,
		nr_seq_unidade_saude_w,
		c_cnes_w,
		c_exame_w,
		c_id_nome_w,
		c_nomemae_w,
		c_id_apel_w,
		d_dtnasc_w,
		c_id_idad_w,
		c_id_ident_w,
		c_id_emis_w,
		c_id_ufide_w,
		c_id_cic_w,
		c_id_esco_w,
		c_endereco_w,
		c_numero_w,
		c_complem_w,
		c_bairro_w,
		c_id_uf_w,
		c_ibge_w,
		c_id_cep_w,
		c_id_refe_w,
		c_id_fone_w,
		c_id_sus_w,
		c_us_uf_w,
		c_us_ibge_w,
		c_us_cnes_w,
		c_us_nome_w,
		c_pron_w,
		c_cit_esc_w,
		c_cit_gla_w,
		c_cit_ind_w,
		c_cit_esca_w,
		c_cit_glan_w,
		c_cit_outr_w,
		c_col_colp_w,
		c_col_pnic_w,
		c_col_pinv_w,
		c_col_frio_w,
		c_col_cure_w,
		c_col_caf_w,
		c_col_exer_w,
		c_col_rcan_w,
		c_col_biop_w,
		c_col_adic_w,
		c_res_tipo_w,
		c_res_macr_w,
		c_res_biop_w,
		c_res_frag_w,
		c_res_peca_w,
		c_res_tam1_w,
		c_res_tam2_w,
		c_res_marg_w,
		c_res_loca_w,
		c_ben_meta_w,
		c_ben_poli_w,
		c_ben_cerv_w,
		c_ben_alte_w,
		c_neo_nica_w,
		c_neo_aden_w,
		c_neo_outr_w,
		c_dif_grau_w,
		c_ext_prof_w,
		c_ext_vasc_w,
		c_ext_peri_w,
		c_ext_para_w,
		c_ext_corp_w,
		c_ext_vagi_w,
		c_ext_lexa_w,
		c_ext_lcom_w,
		c_mar_marg_w,
		c_diag_des_w,
		c_com_frag_w,
		c_com_bloc_w,
		c_mat_ins_w,
		c_mat_insa_w,
		d_exame_w,
		dt_emissao_w,
		d_libera_w,
		c_pat_cns_w,
		c_pat_cic_w,
		c_erro_w,
		c_id_raca_w,
		c_etnia_w,
		c_nacional_w);
	end;
end loop;
close c01;

commit;

end sus_gerar_interf_histo_siscolo;
/
