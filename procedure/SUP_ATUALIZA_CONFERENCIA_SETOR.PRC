create or replace
procedure sup_atualiza_conferencia_setor(
			nr_sequencia_p		number,
			ie_resultado_p		varchar2,
			nm_usuario_p		varchar2) is 

begin

update	ordem_compra_item_conf
set	ie_resultado_conf_setor = ie_resultado_p,
	nm_usuario_conf_setor = nm_usuario_p,
	dt_conf_setor = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end sup_atualiza_conferencia_setor;
/