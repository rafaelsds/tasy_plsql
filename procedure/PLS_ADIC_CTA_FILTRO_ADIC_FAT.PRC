create or replace
procedure pls_adic_cta_filtro_adic_fat(	nr_seq_lote_p		pls_lote_faturamento.nr_sequencia%type,
					nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

Cursor C01 (	nr_seq_lote_pc		pls_lote_faturamento.nr_sequencia%type,
		nr_seq_fatura_pc	pls_fatura.nr_sequencia%type) is
	select	c.nr_seq_conta,
		f.nr_seq_lote
	from	pls_fatura		f,
		pls_fatura_evento	e,
		pls_fatura_conta	c
	where	f.nr_sequencia	= e.nr_seq_fatura
	and	e.nr_sequencia	= c.nr_seq_fatura_evento
	and	f.nr_seq_lote	= nr_seq_lote_pc
	and	nr_seq_fatura_pc is null
	union
	select	c.nr_seq_conta,
		f.nr_seq_lote
	from	pls_fatura		f,
		pls_fatura_evento	e,
		pls_fatura_conta	c
	where	f.nr_sequencia	= e.nr_seq_fatura
	and	e.nr_sequencia	= c.nr_seq_fatura_evento
	and	f.nr_sequencia	= nr_seq_fatura_pc
	and	nr_seq_lote_pc is null;
					
begin
for r_c01_w in c01( nr_seq_lote_p , nr_seq_fatura_p ) loop
	insert into pls_lote_fat_adic
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_lote,
		nr_contrato,
		nr_seq_contrato,
		nr_seq_cooperativa,
		nr_seq_conta,
		nr_seq_intercambio,
		nr_seq_protocolo)
	values	(pls_lote_fat_adic_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		r_c01_w.nr_seq_lote,
		null,
		null,
		null,
		r_c01_w.nr_seq_conta,
		null,
		null);
end loop

commit;

end pls_adic_cta_filtro_adic_fat;
/