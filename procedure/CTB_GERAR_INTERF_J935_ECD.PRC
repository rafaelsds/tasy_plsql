create or replace
procedure ctb_gerar_interf_J935_ecd
			(	nr_seq_controle_p		number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		number,
				dt_inicio_p			date,
				dt_fim_p			date,
				sep_p				Varchar2,
				cd_empresa_p			number,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number	) is 

sep_w				varchar2(1)	:= sep_p;
tp_registro_w			varchar2(15)	:= 'J935';
nr_linha_w			number(10)	:= qt_linha_p;
nr_seq_registro_w		number(10)	:= nr_sequencia_p;		
ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			varchar2(8000);
cd_versao_w			ctb_regra_sped.cd_versao%type;

cd_pf_cnpj_w			varchar2(50) 	:= '';
cd_pf_auditor_w			empresa.cd_pf_auditor%type;
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
nr_cpf_w			pessoa_fisica.nr_cpf%type;
ds_codigo_prof_w		pessoa_fisica.ds_codigo_prof%type;
cd_cnpj_auditor_w		empresa.cd_cnpj_auditor%type;
ds_razao_social_auditoria_w	pessoa_juridica.ds_razao_social%type;
cd_cvm_w			pessoa_juridica.cd_cvm%type;
cd_cgc_w			pessoa_juridica.cd_cgc%type;
ie_porte_empresa_w		empresa.ie_porte_empresa%type;


Cursor C01 is
	select	nvl(cd_pf_auditor,cd_pf_auditor_w) cd_pf_auditor,
		nvl(cd_cnpj_auditor, cd_cnpj_auditor_w) cd_cnpj_auditor
	from	estabelecimento
	where   cd_estabelecimento	= cd_estabelecimento_p
	and 	ie_porte_empresa_w	= 'G';
begin
select	max(cd_pf_auditor),
	max(cd_cnpj_auditor),
	max(ie_porte_empresa)
into	cd_pf_auditor_w,
	cd_cnpj_auditor_w,
	ie_porte_empresa_w
from	empresa
where	cd_empresa	= cd_empresa_p;

select	nvl(max(a.cd_versao), '9.0')
into	cd_versao_w
from	ctb_sped_controle	b,
	ctb_regra_sped		a
where	a.nr_sequencia	= b.nr_seq_regra_sped
and	b.nr_sequencia	= nr_seq_controle_p;

open C01;
loop
fetch C01 into		
	cd_pf_auditor_w,
	cd_cnpj_auditor_w;
exit when C01%notfound;
	begin
	if	(cd_pf_auditor_w is not null) then
		select	a.nm_pessoa_fisica,
			a.ds_codigo_prof,
			a.nr_cpf
		into	nm_pessoa_fisica_w,
			ds_codigo_prof_w,
			nr_cpf_w
		from	pessoa_fisica	a
		where	a.cd_pessoa_fisica	= cd_pf_auditor_w;
		cd_pf_cnpj_w := nr_cpf_w;
	elsif	(cd_cnpj_auditor_w is not null) then
		begin
		select	ds_razao_social,
			cd_cvm
		into	ds_razao_social_auditoria_w,
			cd_cvm_w
		from	pessoa_juridica a
		where	cd_cgc	= cd_cnpj_auditor_w;
		exception when others then
			ds_razao_social_auditoria_w	:= '';
		end;
		cd_pf_cnpj_w := cd_cnpj_auditor_w;
	end if;
	
	if (cd_versao_w in ('7.0','8.0','9.0')) then
		ds_linha_w	:= substr(	sep_w || tp_registro_w ||
						sep_w || cd_pf_cnpj_w ||
						sep_w || nvl(nm_pessoa_fisica_w,ds_razao_social_auditoria_w) ||
						sep_w || nvl(ds_codigo_prof_w,cd_cvm_w) ||
						sep_w ,1,8000);	
	else
		ds_linha_w	:= substr(	sep_w || tp_registro_w ||
						sep_w || nvl(nm_pessoa_fisica_w,ds_razao_social_auditoria_w) ||
						sep_w || nvl(ds_codigo_prof_w,cd_cvm_w) ||
						sep_w ,1,8000);
	end if;
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;	
	
	insert into ctb_sped_registro
		(nr_sequencia,
		ds_arquivo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_controle_sped,
		ds_arquivo_compl,
		cd_registro,
		nr_linha)
	values	(nr_seq_registro_w,
		ds_arquivo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		ds_compl_arquivo_w,
		tp_registro_w,
		nr_linha_w);
	end;
end loop;
close C01;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

commit;

end ctb_gerar_interf_J935_ecd;
/
