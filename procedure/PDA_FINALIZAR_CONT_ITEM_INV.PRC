create or replace
procedure pda_finalizar_cont_item_inv(	nr_sequencia_p		number,
				nr_seq_inventario_p	number,
				qt_contagem_p		number,
				nm_usuario_p		varchar2,
				nr_seq_lote_p number default null) is

cd_estabelecimento_w		number(5);
ie_contagem_atual_w		varchar2(15);
nm_usuario_contagem_atual_w	varchar2(15);
ds_consiste_inv_w			varchar2(5);
qt_inventario_w			number(13,4);
cd_material_w			number(6);
ds_erro_w			varchar2(255);
 
begin

ds_consiste_inv_w := substr(nvl(obter_valor_param_usuario(88,80, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'N'),1,1);

select	nvl(max(ie_contagem_atual), '1'),
	max(cd_material)
into	ie_contagem_atual_w,
	cd_material_w
from	inventario_material
where	nr_seq_inventario = nr_seq_inventario_p
and	nr_sequencia = nr_sequencia_p;

select	nvl(decode(ie_contagem_atual_w,
	1,	nm_usuario_contagem,
	2,	nm_usuario_recontagem,
	3,	nm_usuario_seg_recontagem,
	4,	nm_usuario_terc_recontagem),nm_usuario_p),
	qt_inventario
into	nm_usuario_contagem_atual_w,
	qt_inventario_w
from	inventario_material
where	nr_sequencia  = nr_sequencia_p;

if	(qt_inventario_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(198942);
	-- A quantidade deste item no invent�rio j� est� definida. Esta leitura ser� desconsiderada.
end if;

Inventario_barras(nr_seq_inventario_p, cd_material_w, qt_contagem_p, nr_seq_lote_p, ie_contagem_atual_w, nm_usuario_p, ds_erro_w);

if	(nvl(ds_erro_w,'X') <> 'X') then
	wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);
end if;

update	inventario_material
set	ie_status_inventario	= 'C'
where	nr_sequencia		= nr_sequencia_p; 


/* Retira o item do bloqueio */
delete	inventario_bloqueio
where	nr_seq_inventario  	= nr_seq_inventario_p
and	nr_seq_item 	= nr_sequencia_p;

commit;

end pda_finalizar_cont_item_inv;
/