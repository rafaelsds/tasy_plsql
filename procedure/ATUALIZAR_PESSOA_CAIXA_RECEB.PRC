create or replace
procedure atualizar_pessoa_caixa_receb(
			cd_pessoa_fisica_p		varchar2,
			cd_cgc_p 		varchar2,
			nr_sequencia_p 		number) is 
begin
update	caixa_receb
set	cd_pessoa_fisica 		= cd_pessoa_fisica_p,
	cd_cgc		 	= cd_cgc_p
where	nr_seq_saldo_caixa		= nr_sequencia_p
and 	nvl(ie_tipo_receb, 'R') 	= 'R';
commit;
end atualizar_pessoa_caixa_receb;
/