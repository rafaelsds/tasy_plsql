Create or replace
procedure Hcc_gerar_interf_real_400	(nr_seq_envio_p		number,
									nm_usuario_p		varchar2) is
ie_ordem_arq_w			number(10);
nr_seq_envio_w			number(10);
ie_forma_lancamento_w		varchar(5);
ie_ordem_lote_w			number(10);
tp_registro_w			number(10);
nr_inscricao_w			varchar(20);
cd_agencia_w			varchar(8);
cd_conta_w			varchar(20);
nm_empresa_w			varchar(255);
nm_banco_w			varchar(255);
dt_arquivo_w			date;
hr_arquivo_w			varchar(255);
ie_tipo_servico_w		number(3);
ds_endereco_w			varchar(255);
nr_endereco_w			varchar(255);
ds_complemento_w		varchar(255);
ds_bairro_w			varchar(255);
ds_cidade_w			varchar(255);
cd_cep_w			varchar(255);
sg_estado_w			varchar(255);
cd_banco_w			varchar(10);
cd_compensacao_w		number(10);
nm_favorecido_w			varchar(255);
nr_documento_w			number(20);
dt_lancamento_w			date;
vl_escritural_w			number(15,2);
qt_registros_w			number(10);
vl_total_pagto_w		number(15,2);
dt_vencimento_w			date;
vl_titulo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_acrescimo_w			number(15,2);
dt_pagamento_w			date;	
qt_moeda_w			number(7,6);
cd_sacado_w			varchar(20);
qt_lotes_arquivos_w		number(10);
qt_total_registros_w		number(10);
tp_pagamento_w			varchar(10);
vl_mora_w			number(15,2);
cd_carteira_w			varchar(15);
ds_brancos_w			varchar(255);
ie_registro_w			varchar(255);
nr_barras_w			varchar(100);
ie_tipo_inscricao_w		varchar(20);
cd_agencia_fornec_w		varchar(8);
nr_conta_fornecedor_w		varchar(20);
ie_digito_conta_w		varchar(5);
ds_dac_agencia_w		varchar(2);
nr_seq_apres_w			number(10)	:= 0;
nr_lote_w			number(10)	:= 1;
nr_registro_w			number(10)	:= 1;
cd_camara_compensacao_w		varchar(20);

cursor c01 is
select	ie_ordem_arq,
	nr_seq_envio,
	ie_forma_lancamento,
	ie_ordem_lote,
	tp_registro,
	nr_inscricao,
	cd_agencia,
	cd_conta,
	nm_empresa,
	nm_banco,
	dt_arquivo,
	hr_arquivo,
	ie_tipo_servico,
	ds_endereco,
	nr_endereco,
	ds_complemento,
	ds_bairro,
	ds_cidade,
	cd_cep,
	sg_estado,
	cd_banco,
	cd_compensacao,
	nm_favorecido,
	nr_documento,
	dt_lancamento,
	vl_escritural,
	qt_registros,
	vl_total_pagto,
	dt_vencimento,
	vl_titulo,
	vl_desconto,
	vl_acrescimo,
	dt_pagamento,
	qt_moeda,
	cd_sacado,
	qt_lotes_arquivos,
	qt_total_registros,
	tp_pagamento,
	vl_mora,
	cd_carteira,
	ds_brancos,
	ie_registro,
	nr_barras,
	ie_tipo_inscricao,
	cd_agencia_fornec,
	nr_conta_fornecedor,
	ie_digito_conta,
	ds_dac_agencia
from
(SELECT	0										ie_ordem_arq,
	e.nr_sequencia								nr_seq_envio,
	'0'											ie_forma_lancamento,
	0 											ie_ordem_lote,
	0											tp_registro,
	a.cd_cgc									nr_inscricao,
	TO_CHAR(g.cd_agencia_bancaria)				cd_agencia,
	substr(TO_NUMBER(g.cd_conta) || g.ie_digito_conta,1,15)					cd_conta,
	UPPER(p.ds_razao_social)					nm_empresa,
	g.ds_banco									nm_banco,
	e.dt_remessa_retorno						dt_arquivo,
	e.dt_remessa_retorno						hr_arquivo,
	0											ie_tipo_servico,
	' '											ds_endereco,
	' '											nr_endereco,
	' '											ds_complemento,
	' '											ds_bairro,
	' '											ds_cidade,
	' '											cd_cep,
	' '											sg_estado,
	'0'											cd_banco,
	0											cd_compensacao,
	' '											nm_favorecido,
	0											nr_documento,
	e.dt_remessa_retorno									dt_lancamento,
	0											vl_escritural,
	0											qt_registros,
	0											vl_total_pagto,
	SYSDATE											dt_vencimento,
	0											vl_titulo,
	0											vl_desconto,
	0											vl_acrescimo,
	SYSDATE											dt_pagamento,
	0											qt_moeda,
	' '											cd_sacado,
	0											qt_lotes_arquivos,
	0											qt_total_registros,
	' '											tp_pagamento,
	0											vl_mora,
	g.cd_carteira										cd_carteira,
	' '											ds_brancos,
	'0' 											ie_registro,
	'0'											nr_barras,
	' '											ie_tipo_inscricao,
	' '											cd_agencia_fornec,
	' '											nr_conta_fornecedor,
	' '											ie_digito_conta,
	' '											ds_dac_agencia
FROM												estabelecimento a,
												banco_estabelecimento_v g,
												pessoa_juridica p,
												banco_escritural e
WHERE												e.cd_estabelecimento   	= a.cd_estabelecimento
AND												a.cd_cgc				= p.cd_cgc
AND												g.nr_sequencia			= e.nr_seq_conta_banco
AND												g.ie_tipo_relacao      	IN ('EP','ECC')
AND												e.nr_sequencia			= nr_seq_envio_p
UNION
/*	Detalhe - Pagamento a Fornecedores	*/
SELECT	1										ie_ordem_arq,
		a.nr_sequencia								nr_seq_envio,
		DECODE(c.ie_tipo_pagamento,'CC','2','BLQ','6','DOC','4','TED','4') 	ie_forma_lancamento,
		c.nr_titulo								ie_ordem_lote,
		1									tp_registro,
		d.cd_favorecido								nr_inscricao,
		TO_CHAR(b.cd_agencia_bancaria)						cd_agencia,
		substr(b.cd_conta || b.ie_digito_conta,1,15)				cd_conta,
		' '									nm_empresa,
		' '									nm_banco,
		SYSDATE									dt_arquivo,
		SYSDATE 								hr_arquivo,
		0									ie_tipo_servico,
		d.ds_endereco								ds_endereco,
		TO_CHAR(d.nr_endereco)							nr_endereco,
		' '									ds_complemento,
		d.ds_bairro								ds_bairro,
		d.ds_cidade								ds_cidade,
		d.cd_cep								cd_cep,
		d.ds_estado								sg_estado,
		lpad(c.cd_banco,3,'0')							cd_banco,
		decode(c.ie_tipo_pagamento,'DOC',700,'TED',018) 			cd_compensacao,
		d.nm_favorecido								nm_favorecido,
		d.nr_titulo								nr_documento,
		a.dt_remessa_retorno							dt_lancamento,
		NVL(c.vl_escritural,0) - NVL(c.vl_desconto,0)				vl_escritural,
		0									qt_registros,
		0									vl_total_pagto,
		SYSDATE									dt_vencimento,
		0									vl_titulo,
		0									vl_desconto,
		0									vl_acrescimo,
		SYSDATE									dt_pagamento,
		0									qt_moeda,
		' '									cd_sacado,
		0									qt_lotes_arquivos,
		0									qt_total_registros,
		c.ie_tipo_pagamento							tp_pagamento,
		0									vl_mora,
		b.cd_carteira								cd_carteira,
		' '									ds_brancos,
		'3' 									ie_registro,
		'0'									nr_barras,
		d.ie_favorecido								ie_tipo_inscricao,
		lpad(c.cd_agencia_bancaria,5,'0')					cd_agencia_fornec,
		lpad(c.nr_conta,10,'0')	 						nr_conta_fornecedor,
		decode(length(c.ie_digito_conta),1,c.ie_digito_conta,' ') 		ie_digito_conta,
		decode(length(c.ie_digito_conta),1,' ',c.ie_digito_conta) 		ds_dac_agencia
FROM		estabelecimento e,
		banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c
WHERE		c.nr_titulo								= d.nr_titulo
AND		a.nr_sequencia								= c.nr_seq_escrit
AND		a.cd_estabelecimento							= e.cd_estabelecimento
AND 		b.ie_tipo_relacao						IN ('EP','ECC')
AND		b.nr_sequencia							= a.nr_seq_conta_banco
AND		c.ie_tipo_pagamento						IN ('DOC','TED','CCP','CC','BLQ')
AND		a.nr_sequencia							= nr_seq_envio_p
UNION
/*	Liquida��o de t�tulos - Detalhe 	*/
SELECT	1										ie_ordem_arq,
	a.nr_sequencia								nr_seq_envio,
	DECODE(c.ie_tipo_pagamento,'CC','2','BLQ','6','DOC','4','TED','4') ie_forma_lancamento,
	c.nr_titulo									ie_ordem_lote,
	2											tp_registro,
	' '											nr_inscricao,
	TO_CHAR(b.cd_agencia_bancaria)				cd_agencia,
	substr(b.cd_conta || b.ie_digito_conta,1,15)						cd_conta,
	' '											nm_empresa,
	' '											nm_banco,
	SYSDATE										dt_arquivo,
	SYSDATE 									hr_arquivo,
	0											ie_tipo_servico,
	' '											ds_endereco,
	' '											nr_endereco,
	' '											ds_complemento,
	' '											ds_bairro,
	' '											ds_cidade,
	' '											cd_cep,
	' '											sg_estado,
	'0'											cd_banco,
	decode(c.ie_tipo_pagamento,'DOC',700,'TED',018) cd_compensacao,
	d.nm_favorecido								nm_favorecido,
	d.nr_titulo									nr_documento,
	a.dt_remessa_retorno									dt_lancamento,
	NVL(c.vl_escritural,0) - NVL(c.vl_desconto,0) vl_escritural,
	0											qt_registros,
	0											vl_total_pagto,
	d.dt_vencimento_atual						dt_vencimento,
	0						 					vl_titulo,
	0											vl_desconto,
	0											vl_acrescimo,
	SYSDATE										dt_pagamento,
	0											qt_moeda,
	' '											cd_sacado,
	0											qt_lotes_arquivos,
	0											qt_total_registros,
	c.ie_tipo_pagamento							tp_pagamento,
	NVL(d.vl_saldo_multa,0) + NVL(d.vl_saldo_juros,0) vl_mora,
	b.cd_carteira								cd_carteira,
	' '											ds_brancos,
	'3' 										ie_registro,
	d.nr_bloqueto								nr_barras,
	d.ie_favorecido								ie_tipo_inscricao,
	' '											cd_agencia_fornec,
	' '											nr_conta_fornecedor,
	' '											ie_digito_conta,
	' '											ds_dac_agencia
FROM											estabelecimento e,
												titulo_pagar_v2 d,
												titulo_pagar_escrit c,
												banco_estabelecimento_v b,
												banco_escritural a
WHERE	c.nr_titulo								= d.nr_titulo
AND		a.nr_sequencia							= c.nr_seq_escrit
AND		a.cd_estabelecimento					= e.cd_estabelecimento
AND 	b.ie_tipo_relacao						IN ('EP','ECC')
AND		b.nr_sequencia							= a.nr_seq_conta_banco
AND 	c.ie_tipo_pagamento						= 'BLQ'
AND		a.nr_sequencia							= nr_seq_envio_p
UNION
/*	Trailler do Arquivo	*/
SELECT	9										ie_ordem_arq,
	e.nr_sequencia								nr_seq_envio,
	'0'											ie_forma_lancamento,
	999999										ie_ordem_lote,
	9											tp_registro,
	' '											nr_inscricao,
	'0'											cd_agencia,
	'0'											cd_conta,
	' '											nm_empresa,
	' '											nm_banco,
	SYSDATE										dt_arquivo,
	SYSDATE 									hr_arquivo,
	0											ie_tipo_servico,
	' '											ds_endereco,
	' '											nr_endereco,
	' '											ds_complemento,
	' '											ds_bairro,
	' '											ds_cidade,
	' '											cd_cep,
	' '											sg_estado,
	'0'											cd_banco,
	0											cd_compensacao,
	' '											nm_favorecido,
	0											nr_documento,
	e.dt_remessa_retorno									dt_lancamento,
	sum(NVL(c.vl_escritural,0) - NVL(c.vl_desconto,0))						vl_escritural,
	0											qt_registros,
	SUM(nvl(c.vl_escritural,0) - nvl(c.vl_desconto,0) + nvl(c.vl_acrescimo,0) + nvl(C.VL_JUROS,0) + nvl(c.vl_multa,0)) 	vl_total_pagto,
	SYSDATE										dt_vencimento,
	0											vl_titulo,
	0											vl_desconto,
	0											vl_acrescimo,
	SYSDATE										dt_pagamento,
	0											qt_moeda,
	' '											cd_sacado,
	COUNT(*)									qt_lotes_arquivos,
	COUNT(*)									qt_total_registros,
	' '											tp_pagamento,
	0											vl_mora,
	' '											cd_carteira,
	' '											ds_brancos,
	'9' 										ie_registro,
	'0'											nr_barras,
	' '											ie_tipo_inscricao,
	' '											cd_agencia_fornec,
	' '											nr_conta_fornecedor,
	' '											ie_digito_conta,
	' '											ds_dac_agencia
FROM											estabelecimento a,
												banco_escritural e,
												banco_estabelecimento_v b,
												titulo_pagar_escrit c
WHERE	e.nr_sequencia							= c.nr_seq_escrit
AND		e.cd_estabelecimento    				= a.cd_estabelecimento
AND		b.ie_tipo_relacao       				IN ('EP','ECC')
AND		b.nr_sequencia							= e.nr_seq_conta_banco
AND		e.nr_sequencia							= nr_seq_envio_p
GROUP BY e.nr_sequencia,
	e.dt_remessa_retorno
)
order by		ie_ordem_arq,
		nr_seq_envio,
		nr_documento,
		ie_forma_lancamento,
		tp_pagamento,
		ie_ordem_lote,
		tp_registro;


begin

delete	from w_interf_itau;
commit;

open c01;
loop
fetch c01 into
	ie_ordem_arq_w,	
	nr_seq_envio_w,	
	ie_forma_lancamento_w,
	ie_ordem_lote_w,
	tp_registro_w,
	nr_inscricao_w,	
	cd_agencia_w,
	cd_conta_w,
	nm_empresa_w,
	nm_banco_w,
	dt_arquivo_w,	
	hr_arquivo_w,
	ie_tipo_servico_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_bairro_w,
	ds_cidade_w,
	cd_cep_w,
	sg_estado_w,
	cd_banco_w,
	cd_camara_compensacao_w,
	nm_favorecido_w,
	nr_documento_w,	
	dt_lancamento_w,
	vl_escritural_w,
	qt_registros_w,	
	vl_total_pagto_w,
	dt_vencimento_w,
	vl_titulo_w,
	vl_desconto_w,
	vl_acrescimo_w,	
	dt_pagamento_w,
	qt_moeda_w,
	cd_sacado_w,
	qt_lotes_arquivos_w,
	qt_total_registros_w,
	tp_pagamento_w,
	vl_mora_w,
	cd_carteira_w,
	ds_brancos_w,
	ie_registro_w,
	nr_barras_w,
	ie_tipo_inscricao_w,
	cd_agencia_fornec_w,
	nr_conta_fornecedor_w,
	ie_digito_conta_w,
	ds_dac_agencia_w;
exit when c01%notfound;

nr_seq_apres_w	:= nr_seq_apres_w + 1;	

--raise_application_error(-20011,	vl_escritural_w);
insert	into	w_interf_itau
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_apres,
		nr_seq_envio,
		ie_forma_pagto,
		nr_seq_reg_lote,
		ie_tipo_registro,
		nr_inscricao,
		cd_agencia_bancaria,
		cd_conta,
		nm_empresa,
		ds_banco,
		dt_geracao,
		ds_endereco,
		nr_endereco,
		ds_bairro,
		ds_cidade,
		cd_cep,
		sg_estado,
		cd_banco_fornec,
		cd_camara_compensacao,
		nm_fornecedor,
		nr_titulo,
		dt_pagto,
		vl_pagto,
		qt_registros,
		vl_total_pagto,
		cd_barras,
		dt_vencimento,
		vl_titulo,
		vl_desconto,
		vl_acrescimo,
		qt_lote_arquivo,
		qt_tot_reg,
		vl_juros,
		ie_registro,
		ie_tipo_servico,
		ds_complemento,
		qt_moeda,
		ie_forma_lancamento,
		cd_carteira,
		ds_brancos,
		nr_lote,
		nr_registro,
		ie_tipo_inscricao,
		cd_agencia_fornec,
		nr_conta_fornecedor,
		ie_digito_conta,
		ds_dac_agencia)
	values	(w_interf_itau_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_apres_w,--ie_ordem_arq_w,
		nr_seq_envio_w,
		tp_pagamento_w,
		ie_ordem_lote_w,
		tp_registro_w,
		nr_inscricao_w,
		cd_agencia_w,
		cd_conta_w,
		nm_empresa_w,
		nm_banco_w,
		dt_arquivo_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_bairro_w,
		ds_cidade_w,
		cd_cep_w,
		sg_estado_w,
		cd_banco_w,
		cd_camara_compensacao_w,
		nm_favorecido_w,
		nr_documento_w,
		dt_lancamento_w,
		vl_escritural_w,
		qt_registros_w,
		vl_total_pagto_w,
		nr_barras_w,
		dt_vencimento_w,
		vl_titulo_w,
		vl_desconto_w,
		vl_acrescimo_w,
		qt_lotes_arquivos_w,
		qt_total_registros_w,
		vl_mora_w,
		ie_registro_w,
		ie_tipo_servico_w,
		ds_complemento_w,
		qt_moeda_w,
		ie_forma_lancamento_w,
		cd_carteira_w,
		ds_brancos_w,
		nr_lote_w,
		nr_registro_w,
		ie_tipo_inscricao_w,
		cd_agencia_fornec_w,
		nr_conta_fornecedor_w,
		ie_digito_conta_w,
		ds_dac_agencia_w);

	/* So incrementar o numero do registro quando detalhe */
	if	(tp_registro_w in ('2','3','6','7','8')) then
		nr_registro_w	:= nr_registro_w + 1;
	end if;

	/* Quando passou do Trailler muda o numero do lote */
	if	(tp_registro_w in ('4','9')) then
		nr_lote_w	:= nr_lote_w + 1;
		nr_registro_w	:= 1;
	end if;	
end loop;
close c01;

commit;

end Hcc_Gerar_interf_real_400;
/
