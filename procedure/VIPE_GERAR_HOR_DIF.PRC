create or replace
procedure VIPE_Gerar_Hor_Dif(	ds_dose_diferenciada_p		varchar2,
				ds_horarios_entrada_p		varchar2,
				nr_prescricoes_p			varchar2,
				cd_intervalo_p			varchar2,
				hr_prim_horario_p			varchar2,
				nr_prescricao_p			number,
				cd_item_p			number,
				qt_dose_total_mod_item_p	in out	number,
				ds_horarios_p		out	varchar2,
				ds_mensagem_p		out	varchar2,
				qt_operacao_p		out	number) is
						
begin

VIPE_Gerar_Horario_Dif_PCK.VIPE_Gerar_Horario_Dif(ds_dose_diferenciada_p, ds_horarios_entrada_p, nr_prescricoes_p, cd_intervalo_p, hr_prim_horario_p, nr_prescricao_p, cd_item_p, qt_dose_total_mod_item_p, ds_horarios_p, ds_mensagem_p, qt_operacao_p);

end VIPE_Gerar_Hor_Dif;
/