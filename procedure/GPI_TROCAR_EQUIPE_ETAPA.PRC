create or replace
procedure gpi_trocar_equipe_etapa(	nr_seq_etapa_p		number,
				cd_pessoa_fisica_atual_p	number,
				cd_pessoa_fisica_novo_p	number,
				nm_usuario_p		Varchar2) is 

begin

update	gpi_cron_etapa_equipe
set	cd_pessoa_fisica	= cd_pessoa_fisica_novo_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_seq_etapa	= nr_seq_etapa_p
and	cd_pessoa_fisica	= cd_pessoa_fisica_atual_p;		

commit;

end gpi_trocar_equipe_etapa;
/