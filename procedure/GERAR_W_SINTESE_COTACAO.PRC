create or replace
procedure gerar_w_sintese_cotacao(
			tp_movimento_p		varchar2,
			dt_gravacao_p		varchar2,
			cd_cotacao_p		varchar2,
			cd_requisicao_p		varchar2,
			cd_condicao_pagamento_p	varchar2,
			dt_inicio_cotacao_p	varchar2,
			dt_fim_cotacao_p		varchar2,
			dt_validade_preco_p	varchar2,
			dt_cadastro_p		varchar2,
			ds_cotacao_p		varchar2,
			nm_usuario_cot_p		varchar2,
			ds_observacao_p		varchar2,
			ie_lido_p			varchar2,
			nm_usuario_p		varchar2,
			nr_seq_cotacao_p out	varchar2) is 

nr_seq_cotacao_w	number(10);
dt_gravacao_w		date;
dt_inicio_cotacao_w	date;
dt_fim_cotacao_w	date;
dt_validade_preco_w	date;
dt_cadastro_w		date;	
			
begin

select	w_sintese_cotacao_seq.nextval
into	nr_seq_cotacao_w
from	dual;

dt_gravacao_w := to_date(dt_gravacao_p, 'dd/mm/yyyy hh24:mi:ss');
dt_inicio_cotacao_w := to_date(dt_inicio_cotacao_p, 'dd/mm/yyyy hh24:mi:ss');
dt_fim_cotacao_w := to_date(dt_fim_cotacao_p, 'dd/mm/yyyy hh24:mi:ss');
dt_validade_preco_w := to_date(dt_validade_preco_p, 'dd/mm/yyyy hh24:mi:ss');
dt_cadastro_w := to_date(dt_cadastro_p, 'dd/mm/yyyy hh24:mi:ss');

insert into w_sintese_cotacao(	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	tp_movimento,
	dt_gravacao,
	cd_cotacao,
	cd_requisicao,
	cd_condicao_pagamento,
	dt_inicio_cotacao,
	dt_fim_cotacao,
	dt_validade_preco,
	dt_cadastro,
	ds_cotacao,
	nm_usuario_cot,
	ds_observacao,
	ie_lido)
values(	nr_seq_cotacao_w,           
	sysdate,      
	nm_usuario_p,             
	sysdate,    
	nm_usuario_p,        
	tp_movimento_p,
	dt_gravacao_w,
	cd_cotacao_p,
	cd_requisicao_p,
	cd_condicao_pagamento_p,
	dt_inicio_cotacao_w,
	dt_fim_cotacao_w,
	dt_validade_preco_w,
	dt_cadastro_w,
	substr(ds_cotacao_p,1,50),
	nm_usuario_cot_p,
	ds_observacao_p,
	ie_lido_p);
	
nr_seq_cotacao_p := nr_seq_cotacao_w;

commit;

end gerar_w_sintese_cotacao;
/
