create or replace 
procedure obter_regra_subst_farm	(cd_material_p 			number,
									cd_perfil_p				number,
									nm_usuario_p			varchar2,
									ie_substituir_p			out varchar2,
									ie_alterar_qt_p			out varchar2,
									ie_quantidade_adic_p	out varchar2) is


ie_substituir_w				regra_subst_farm.ie_substituir%type;
ie_alterar_quantidade_w		regra_subst_farm.ie_alterar_quantidade%type;
ie_quantidade_adicional_w	regra_subst_farm.ie_quantidade_adicional%type;
ie_controlado_w				varchar2(1);
cd_grupo_w					number(3);
cd_subgrupo_w				number(3);
cd_classe_material_w		number(5);

begin

select	max(obter_estrutura_material(a.cd_material,'G')),
		max(obter_estrutura_material(a.cd_material,'S')),
		max(obter_estrutura_material(a.cd_material,'C')),
		max(obter_se_medic_controlado(a.cd_material))
into	cd_grupo_w,
		cd_subgrupo_w,
		cd_classe_material_w,
		ie_controlado_w
from	material a
where 	a.cd_material = cd_material_p;

select	max(a.ie_substituir),
		max(a.ie_alterar_quantidade),
		max(a.ie_quantidade_adicional)
into	ie_substituir_w,
		ie_alterar_quantidade_w,
		ie_quantidade_adicional_w
from	regra_subst_farm a
where 	nvl(ie_situacao,'I') = 'A'
and 	(a.cd_perfil is null or a.cd_perfil = cd_perfil_p)
and 	(a.cd_material is null or a.cd_material = cd_material_p)
and 	(a.cd_grupo_material is null or a.cd_grupo_material = cd_grupo_w)
and 	(a.cd_subgrupo_material is null or a.cd_subgrupo_material = cd_subgrupo_w)
and 	(a.cd_classe_material is null or a.cd_classe_material = cd_classe_material_w)
and 	(a.ie_receita = 'A' 
		or (a.ie_receita = 'S' and ie_controlado_w = 'S')
		or (a.ie_receita = 'N' and ie_controlado_w = 'N'));

ie_substituir_p 		:=	nvl(ie_substituir_w,'S');
ie_alterar_qt_p 		:=	nvl(ie_alterar_quantidade_w,'S');
ie_quantidade_adic_p	:=	nvl(ie_quantidade_adicional_w,'S');

end obter_regra_subst_farm;
/