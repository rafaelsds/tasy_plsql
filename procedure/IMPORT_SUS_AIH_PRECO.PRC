create or replace
procedure import_sus_aih_preco(vl_sa_p		number,
			vl_sh_p			number,
			vl_sp_p			number,
			qt_pontos_ato_p		number,
			dt_vigencia_inicial_p	date,
			dt_vigencia_final_p		date,
			vl_total_hospitalar_p	number,
			ie_versao_aih_p		varchar2,
			cd_procedimento_p		number,
			dt_comp_aih_p		date,
			ie_novo_p		varchar2,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar dados AIH para a tabela sus_preco
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

if	(cd_procedimento_p is not null) and 
	(vl_total_hospitalar_p is not null) and 
	(vl_sp_p is not null) and 
	(vl_sh_p is not null) and 
	(vl_sa_p is not null) then 
		if	(upper(ie_novo_p) = 'S')then
			insert into sus_preco 
						( vl_sadt, 
						vl_sa, 
						vl_sh, 
						vl_sp, 
						qt_pontos_ato, 
						dt_vigencia_inicial, 
						dt_vigencia_final, 
						vl_total_hospitalar, 
						dt_atualizacao, 
						nm_usuario, 
						ie_versao_aih, 
						dt_atualizacao_nrec, 
						nm_usuario_nrec, 
						cd_procedimento, 
						ie_origem_proced, 
						vl_total_amb, 
						dt_competencia, 
						ie_aih) 
			 values 	(0, 
						vl_sa_p, 
						vl_sh_p, 
						vl_sp_p, 
						qt_pontos_ato_p, 
						dt_vigencia_inicial_p, 
						dt_vigencia_final_p, 
						vl_total_hospitalar_p, 
						sysdate, 
						nm_usuario_p, 
						ie_versao_aih_p, 
						sysdate, 
						nm_usuario_p, 
						cd_procedimento_p, 
						7, 
						0, 
						dt_comp_aih_p,
						'S');		
		else
			update	sus_preco 
			set	ie_aih = 'S', 
				vl_sh = vl_sh_p, 
				vl_sp = vl_sp_p, 
				vl_total_hospitalar = vl_total_hospitalar_p, 
				qt_pontos_ato = qt_pontos_ato_p, 
				ie_versao_aih = ie_versao_aih_p, 
				dt_atualizacao = sysdate 
			where	cd_procedimento = cd_procedimento_p 
			and	ie_origem_proced = 7 
			and	dt_competencia = dt_comp_aih_p; 
			
		end if;
	commit;
end if;

end import_sus_aih_preco;
/