create or replace procedure gerar_prescr_mat_hor_compl(	nr_seq_mat_hor_p		number,
						nm_usuario_p			Varchar2,
						ie_susp_duplicidade_p	varchar2) is

nr_seq_mat_compl_w		prescr_mat_hor.nr_seq_mat_compl%type;

begin

select	max(nr_seq_mat_compl)
into	nr_seq_mat_compl_w
from	prescr_mat_hor
where	nr_sequencia = nr_seq_mat_hor_p;

if (nr_seq_mat_compl_w	is not null) then

	update	prescr_mat_hor_compl
	set		dt_atualizacao	= sysdate,
			nm_usuario		= nm_usuario_p,
			ie_susp_duplicidade	= ie_susp_duplicidade_p
	where	nr_sequencia	= nr_seq_mat_compl_w;

else

	$if dbms_db_version.version >= 11 $then 
		nr_seq_mat_compl_w	:= prescr_mat_hor_compl_seq.nextval;
	$else                                   
		select	prescr_mat_hor_compl_seq.nextval
		into	nr_seq_mat_compl_w
		from	dual;
	$end

	insert into prescr_mat_hor_compl (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_susp_duplicidade	)
	values (
		nr_seq_mat_compl_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_susp_duplicidade_p);					

	update	prescr_mat_hor
	set		nr_seq_mat_compl = nr_seq_mat_compl_w
	where	nr_sequencia	= nr_seq_mat_hor_p;

end if;	

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_prescr_mat_hor_compl;
/
