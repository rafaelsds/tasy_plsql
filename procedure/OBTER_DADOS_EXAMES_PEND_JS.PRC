create or replace
procedure obter_dados_exames_pend_js(	nr_prescricao_p		number,
					qt_exame_susp_p		out number,
					ie_consiste_conv_p	out varchar2,
					ie_consiste_part_p	out varchar2,
					ie_evento_12_p		out varchar2) is 


qt_exame_susp_w		number(10);
ie_consiste_conv_w	varchar2(1);
ie_consiste_part_w	varchar2(1);
ie_evento_12_w		varchar2(1);

begin

select	count(*) 
into	qt_exame_susp_w
from 	prescr_procedimento
Where 	ie_suspenso = 'S'
and   	nr_prescricao = nr_prescricao_p;

ie_consiste_conv_w 	:= obter_prescr_guia_conv(nr_prescricao_p);

ie_consiste_part_w 	:= obter_prescr_part_fat(nr_prescricao_p);
			
ie_evento_12_w		:= obter_se_existe_evento(12);

qt_exame_susp_p		:= qt_exame_susp_w;	
ie_consiste_conv_p 	:= ie_consiste_conv_w;
ie_consiste_part_p 	:= ie_consiste_part_w;
ie_evento_12_p		:= ie_evento_12_w;

end obter_dados_exames_pend_js;
/