create or replace
procedure atualiza_retorno_lote_ext_js is 

begin

update 	lab_lote_externo a 
set 	dt_retorno = sysdate
where 	not exists (	select 1
			from prescr_procedimento b
			where b.ie_status_atend < 30
			and b.nr_seq_lote_externo = a.nr_sequencia )
and 	a.dt_envio is not null
and 	a.dt_retorno is null;

commit;

end atualiza_retorno_lote_ext_js;
/