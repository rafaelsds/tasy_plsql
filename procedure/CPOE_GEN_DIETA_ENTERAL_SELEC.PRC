create or replace
procedure CPOE_GEN_DIETA_ENTERAL_SELEC(	nr_atendimento_p			atendimento_paciente.nr_atendimento%type,
										cd_paciente_p				pessoa_fisica.cd_pessoa_fisica%type,
										cd_material_p        		cpoe_dieta.cd_material%type,
										cd_estabelecimento_p    	estabelecimento.cd_estabelecimento%type,
										cd_perfil_p             	perfil.cd_perfil%type,
										nm_usuario_p				usuario.nm_usuario%type,
										nr_seq_item_gerado_p		out number,
										ie_prescritor_aux_p			varchar2 default 'N',
										cd_medico_p					number default null,
										ie_continuo_p				cpoe_dieta.ie_continuo%type default null,
                                        nr_seq_cpoe_order_unit_p	cpoe_anatomia_patologica.nr_seq_cpoe_order_unit%type) is


ie_duracao_w			cpoe_dieta.ie_duracao%type := 'C';

begin

select	cpoe_dieta_seq.nextval
into	nr_seq_item_gerado_p
from	dual;

insert into cpoe_dieta(
				nr_sequencia,
				cd_material,
				ie_duracao,
				ie_continuo,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nr_atendimento,
				cd_pessoa_fisica,
				ie_tipo_dieta,
				cd_perfil_ativo,
				cd_funcao_origem,
				ie_prescritor_aux,
				cd_medico,
                 nr_seq_cpoe_order_unit)
			values (
				nr_seq_item_gerado_p,
				cd_material_p,
				ie_duracao_w,
				ie_continuo_p,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				nr_atendimento_p,
				cd_paciente_p,
				'E',
				cd_perfil_p,
				2314,
				ie_prescritor_aux_p,
				cd_medico_p,
                 nr_seq_cpoe_order_unit_p);			
commit;

end CPOE_GEN_DIETA_ENTERAL_SELEC;
/
