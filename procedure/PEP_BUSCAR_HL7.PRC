create or replace
procedure PEP_BUSCAR_HL7(nr_atendimento_p    number,
                         nr_intervalo_p      integer,
                         cd_tipo_intervalo_p varchar2) is
  ConstanteMinuto_w  varchar2(3) := 'MIN';
  ConstanteDia_w     varchar2(3) := 'DIA';
  msg_hl7_texto_w    varchar(2000);

  msg_hl7_long_w        long;
  cd_pessoa_fisica_w    number;
  w_log_integr_sv_seq_w number;

  dt_atualizacao_w   date;
  dt_inicio_busca_w  date;

cursor c01 is
  select c.ds_hl7,
         c.dt_atualizacao
  from   informacao_integracao a,
         log_integracao        b,
         log_integracao_hl7    c
  where  a.nr_sequencia = 152
  and    nr_seq_evento = 55
  and    a.nr_sequencia = b.nr_seq_informacao
  and    b.nr_sequencia = c.nr_seq_log
  and    c.dt_atualizacao >= dt_inicio_busca_w;


begin
  Exec_sql_Dinamico(wheb_usuario_pck.get_nm_usuario, 'truncate table w_log_integr_sv');
  
  if (ConstanteMinuto_w = cd_tipo_intervalo_p) then

    dt_inicio_busca_w := sysdate - ((1/24/60)*nr_intervalo_p);

  elsif (ConstanteDia_w = cd_tipo_intervalo_p) then

    dt_inicio_busca_w :=  sysdate - nr_intervalo_p;

  end if;

  select nvl(max(cd_pessoa_fisica), 0)
  into   cd_pessoa_fisica_w
  from   atendimento_paciente
  where  nr_atendimento = nr_atendimento_p;

  open C01;
  loop
  fetch C01 into
      msg_hl7_long_w,
      dt_atualizacao_w;
  exit when C01%notfound;

    msg_hl7_texto_w := substr(TO_CHAR(msg_hl7_long_w), 1, 2000);

    if (instr(msg_hl7_texto_w, cd_pessoa_fisica_w) > 0)  then
        
        select w_log_integr_sv_seq.nextval
        into   w_log_integr_sv_seq_w
        from   dual;
        
        insert into w_log_integr_sv(nr_sequencia         , dt_atualizacao   , nm_usuario                     , ds_mensagem)
                             values(w_log_integr_sv_seq_w, dt_atualizacao_w , wheb_usuario_pck.get_nm_usuario, msg_hl7_texto_w);
        commit;

    end if;

  end loop;
  close C01;

end PEP_BUSCAR_HL7;

/
