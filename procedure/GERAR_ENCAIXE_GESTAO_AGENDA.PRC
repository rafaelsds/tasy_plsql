create or replace
procedure gerar_encaixe_gestao_agenda(	dt_agenda_p		date,
					qt_duracao_p		number,
					cd_pessoa_fisica_p	varchar2,
					nm_paciente_p		varchar2,
					cd_medico_p		varchar2,
					nr_seq_proc_interno_p	number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					ie_lado_p		varchar2,
					cd_convenio_p		number,
					cd_agenda_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					ds_observacao_p		varchar2,
					ie_carater_cirurgia_p	varchar2,
					nr_tefefone_p		varchar2,
					qt_idade_p		number,
					qt_idade_mes_p		number,
					nm_pessoa_contato_p	varchar2,
					cd_anestesista_p	varchar2,
					nr_seq_agenda_p		out number,
					cd_procedencia_p	number default null,
					ie_tipo_atendimento_p	number default null,
					nr_seq_motivo_encaixe_p number,
					ds_erro_2_p		out varchar2,
					nr_seq_classif_agenda_p	number default null)
					is


cd_pessoa_fisica_w	varchar2(10);
nr_seq_classif_w	number(10,0);
qt_min_cirugia_w	number(3,0):= null;
nr_sequencia_W		number(10,0);
nr_telefone_w		varchar2(40);
nr_celular_w		varchar2(40);
dt_ultima_consulta_w	date;
cd_usuario_convenio_w	varchar2(30);
qt_dias_atend_w		number(5,0);
ie_reserva_leito_w	varchar2(3);
cd_tipo_anestesia_w	varchar2(2);
qt_idade_paciente_w	number(3);
ie_agenda_cirurgiao_w	varchar2(1);
ds_erro_w		varchar2(255);
ds_erro_2_w		varchar2(255);
qt_agendas_w		number(5);

ds_status_agenda_w	varchar2(100);
nm_pessoa_fisica_w	varchar2(50);
ds_agenda_w		varchar2(50);
hr_inicio_w		varchar2(21);
nm_pessoa_contato_w	varchar2(50);
ie_consiste_w		varchar2(01); 
ie_forma_convenio_w	Varchar2(2);

cd_convenio_w		number(5,0);
cd_categoria_w		varchar2(10);
cd_plano_w		varchar2(10);
dt_validade_w		date;
nr_doc_convenio_w	varchar2(20);
cd_tipo_acomodacao_w	number(4,0);
ie_Gera_dados_conv_w	Varchar2(1);
ie_gerar_dias_prev_w	Varchar2(1);
qt_diaria_prev_w	number(3,0);
ie_status_autor_w	Varchar2(10);
ie_gerar_equip_w	Varchar2(1);
ie_gerar_cme_w		Varchar2(1);
ie_gerar_opme_w		Varchar2(1);
ie_gerar_servico_w	Varchar2(1);
ie_gerar_caixa_opme_w	Varchar2(1);
ds_observacao_aux	varchar2(4000);
ie_erro_w		Varchar2(1);
ds_orientacoes_w varchar2(4000);

begin

if (nr_seq_classif_agenda_p is not null) then
	nr_seq_classif_w := nr_seq_classif_agenda_p;
else 
	Obter_Classif_Encaixe_Agenda(nr_seq_classif_w);
end if;

begin
select	obter_idade(dt_nascimento, sysdate, 'A')
into	qt_idade_paciente_w	
from	pessoa_fisica
where	cd_pessoa_fisica		=	cd_pessoa_fisica_p;
exception
	when no_data_found then
	qt_idade_paciente_w	:= null;
end;

select	obter_compl_pf(cd_pessoa_fisica_p, 1, 'T'),
	substr(nm_pessoa_contato_p,1,50)
into	nr_telefone_w,
	nm_pessoa_contato_w
from	dual;

select	obter_dados_pf(cd_pessoa_fisica_p,'TC')
into	nr_celular_w
from	dual;

if	(nr_celular_w <> null) then
	nr_telefone_w := nr_telefone_w || ' ' || nr_celular_w;
end if;

if	(nr_seq_proc_interno_p is not null) then
	select	ie_reserva_leito,
		cd_tipo_anestesia,
		QT_DIAS_PREV_INTER
	into	ie_reserva_leito_w,
		cd_tipo_anestesia_w,
		qt_diaria_prev_w
	from	proc_interno
	where	nr_sequencia			= nr_seq_proc_interno_p;

end if;

select	nvl(max(to_number(obter_valor_param_usuario(871,25,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p))) , 1)	
into	qt_dias_atend_w
from	dual;

obter_param_usuario(871, 24, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_agenda_cirurgiao_w);
obter_param_usuario(871, 142, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_w);
obter_param_usuario(871, 184, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_dias_prev_w);
obter_param_usuario(871, 33, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_status_autor_w);
obter_param_usuario(871, 40, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_equip_w);
obter_param_usuario(871, 200, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_cme_w);
obter_param_usuario(871, 201, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_opme_w);
obter_param_usuario(871, 202, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_servico_w);
obter_param_usuario(871, 203, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_caixa_opme_w);


begin						
select	dt_ultima_consulta,
	cd_usuario_convenio
into	dt_ultima_consulta_w,
	cd_usuario_convenio_w
from	Med_Cliente
where	cd_pessoa_fisica		= cd_pessoa_fisica_p
and	trunc(dt_ultima_consulta,'dd')	>= trunc(sysdate-qt_dias_atend_w,'dd')
and	cd_convenio			= cd_convenio_p;
exception
	when others then
	cd_usuario_convenio_w	:= null;
end;

if	(cd_usuario_convenio_w is null) then
	begin
	select	max(cd_usuario_convenio)
	into	cd_usuario_convenio_w	
	from    atendimento_paciente a,
		atend_categoria_convenio b
	where   a.nr_atendimento	= b.nr_atendimento
	and     a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and     b.cd_convenio		= cd_convenio_p
	and     dt_entrada >=		sysdate - qt_dias_atend_w;
	exception
		when others then
		cd_usuario_convenio_w	:= null;
	end;
end if;

select 	count(*)
into	qt_agendas_w
from 	agenda_paciente
where	cd_agenda	 	= cd_agenda_p
and	dt_agenda		= trunc(dt_agenda_p,'dd')
and	hr_inicio		= trunc(dt_agenda_p,'mi')
and	(((ie_consiste_w = 'N') and (ie_status_agenda  = decode(cd_pessoa_fisica_p, null, 'R', 'N'))) or
	 ((ie_consiste_w = 'S') and (ie_status_agenda not in ('C', 'L', 'B'))));

if 	(qt_agendas_w > 0) then

	ds_agenda_w		:= substr(obter_nome_agenda(cd_agenda_p),1,50);
	hr_inicio_w		:= substr(to_char(trunc(dt_agenda_p,'mi'),'dd/mm/yyyy hh24:mi'),1,21);

	select	substr(obter_valor_dominio(83, decode(cd_pessoa_fisica_p, null, 'R', 'N')),1,100)
	into	ds_status_agenda_w
	from 	dual;
	
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(182974,'HR_INICIO_W='||to_char(hr_inicio_w)||';DS_AGENDA_W='||to_char(ds_agenda_w)||';DS_STATUS_AGENDA_W='||to_char(ds_status_agenda_w));
end if;


select	agenda_paciente_seq.nextval
into	nr_sequencia_W
from	dual;

nr_seq_agenda_p	:= nr_sequencia_W; 

select 	nvl(max('N'),'S')
into	ie_erro_w
from	procedimento
where	cd_procedimento = cd_procedimento_p
and	ie_origem_proced= ie_origem_proced_p;

if	(ie_erro_w = 'S') then

	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(182975);
end if;					
	
	
--begin Retirado tfferretti OS 470990
insert into agenda_paciente(
	nr_sequencia,
	cd_agenda,
	cd_pessoa_fisica,
	dt_agenda,
	hr_inicio,
	nr_minuto_duracao,
	nm_usuario,
	dt_atualizacao,
	cd_medico,
	ie_status_agenda,
	nr_seq_classif_agenda,
	dt_agendamento,
	nr_seq_proc_interno,
	ie_equipamento,
	cd_procedimento,
	ie_origem_proced,
	nr_telefone,
	ie_reserva_leito,
	cd_tipo_anestesia,
	cd_usuario_convenio,
	ie_lado,
	nm_paciente,
	cd_convenio,
	nm_usuario_orig,
	qt_idade_paciente,
	ds_observacao,
	ie_carater_cirurgia,
	ie_autorizacao,
	nm_pessoa_contato,
	qt_diaria_prev,
	cd_anestesista,
	hr_revisada,
	dt_nascimento_pac,
	qt_idade_mes,
	cd_procedencia,
	ie_tipo_atendimento,
	nr_seq_motivo_encaixe)
values(
	nr_sequencia_w,
	cd_agenda_p,
	cd_pessoa_fisica_p,
	trunc(dt_agenda_p,'dd'),
	trunc(dt_agenda_p,'mi'),
	qt_duracao_p,
	nm_usuario_p,
	sysdate,
	cd_medico_p,
	decode(cd_pessoa_fisica_p, null, 'R', 'N'),
	nr_seq_classif_w,
	sysdate,
	nr_seq_proc_interno_p,
	'N',
	cd_procedimento_p,
	ie_origem_proced_p,
	substr(nvl(nr_telefone_w, nr_tefefone_p),1,254),
	ie_reserva_leito_w,
	cd_tipo_anestesia_w,
	cd_usuario_convenio_w,
	ie_lado_p,
	decode(cd_pessoa_fisica_p, null, nm_paciente_p, substr(obter_nome_pf(cd_pessoa_fisica_p),1,100)),
	cd_convenio_p,
	nm_usuario_p,
	decode(qt_idade_p,0,qt_idade_paciente_w,qt_idade_p),
	ds_observacao_p,
	ie_carater_cirurgia_p,
	nvl(ie_status_autor_w,'PA'),
	nm_pessoa_contato_w,
	decode(ie_gerar_dias_prev_w,'S',qt_diaria_prev_w,null),
	cd_anestesista_p,
	trunc(dt_agenda_p,'mi'),
	to_date(obter_dados_pf(cd_pessoa_fisica_p,'DN'),'dd/mm/yyyy hh24:mi'),
	DECODE(qt_idade_mes_p,0,obter_idade(obter_data_nascto_pf(cd_pessoa_fisica_p),SYSDATE,'MM'),qt_idade_mes_p),
	cd_procedencia_p,
	ie_tipo_atendimento_p,
	nr_seq_motivo_encaixe_p);
--exception
--	when others then
--	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(182976);
--end;

commit;
gerar_orientacao(nr_seq_proc_interno_p, cd_convenio_p, nm_usuario_p,  ds_orientacoes_w);

select	nvl(max(obter_valor_param_usuario(871, 46, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N'),
	nvl(max(obter_valor_param_usuario(871, 156, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
into	ie_forma_convenio_w,
	ie_Gera_dados_conv_w
from	dual;

if	(ie_gera_dados_conv_w = 'S') and
	(cd_pessoa_fisica_p is not null) and
	(ie_forma_convenio_w <> 'N') then
	gerar_convenio_agendamento(cd_pessoa_fisica_p, 1, nr_sequencia_w, ie_forma_convenio_w, cd_convenio_w, cd_categoria_w, cd_usuario_convenio_w, dt_validade_w, nr_doc_convenio_w, cd_tipo_acomodacao_w, cd_plano_w, nm_usuario_p, ds_observacao_aux);
	if	(cd_convenio_w is not null) and
		(cd_convenio_p	= cd_convenio_w) then
		update	agenda_paciente
		set	cd_convenio		= cd_convenio_w,
			cd_categoria		= cd_categoria_w,
			cd_usuario_convenio	= cd_usuario_convenio_w,
			dt_validade_carteira	= dt_validade_w,
			nr_doc_convenio		= nr_doc_convenio_w, 
			cd_tipo_acomodacao	= cd_tipo_acomodacao_w,
			cd_plano		= cd_plano_w
		where	nr_sequencia		= nr_sequencia_w;
	end if;
end if;

gerar_dados_pre_agenda(cd_procedimento_p,
						ie_origem_proced_p,
						nr_sequencia_W, 
						nr_seq_proc_interno_p, 
						cd_medico_p, 
						cd_pessoa_fisica_p,
						nm_usuario_p, 
						cd_convenio_p, 
						null,
						ds_orientacoes_w,
						cd_estabelecimento_p, 
						ie_gerar_equip_w, 
						ie_gerar_cme_w, 
						ie_gerar_opme_w, 
						ie_gerar_servico_w, 
						ie_gerar_caixa_opme_w, 
						ds_erro_w,
						ds_erro_2_w); 
						
ds_erro_2_p	:= substr(ds_erro_2_w,1,255);

if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	Gerar_Autor_Regra(null,null,null,null,null,null,'AP',nm_usuario_p,nr_sequencia_W,nr_seq_proc_interno_p,null,null,null,null,null,null,null);
end if;	

end gerar_encaixe_gestao_agenda;
/