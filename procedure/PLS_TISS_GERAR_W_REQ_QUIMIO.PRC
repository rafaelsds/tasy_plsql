create or replace
procedure pls_tiss_gerar_w_req_quimio	(	nr_seq_requisicao_p		number,
						nr_seq_lote_guia_p		number,
						nm_usuario_p			varchar2) is

type 	rc_med is record	(dt_prev_w		date,
				cd_tabela_w		varchar2(2),
				cd_med_w		varchar2(10),
				ds_med_w		varchar2(150),
				qt_doses_w		number(7,2),
				via_adm_w		varchar2(2),
				freq_w			varchar2(2),
				nr_seq_apre_w		number(10),
				qt_unidade_medida_w	varchar2(3));

type 	rc_cid is record	(dt_diag_w		date,
				cd_cid_w		varchar2(4),
				cd_diag_imag_w		varchar2(1),
				ie_est_tumor_w		varchar2(1),
				ie_cap_func_w		varchar2(1),
				cd_fin_trat_w		varchar2(1),
				ds_diag_w		varchar2(1000),
				ds_obs_w		varchar2(4000));
				
type 	tb_med is table of rc_med index by pls_integer;
type 	tb_cid is table of rc_cid index by pls_integer;

tb_cid_w			tb_cid;
tb_med_w			tb_med;
nr_cid_w			number(10) := 0;
cd_doenca_w			varchar2(4);

dt_prev_realizacao_w		date;
cd_tipo_tabela_w		varchar2(2);
cd_medicamento_w		varchar2(10);
ds_medicamento_w		varchar2(150);
qt_doses_w			number(7,2);
via_adm_w			varchar2(2);
frequencia_w			varchar2(2);
qt_unidade_medida_w		varchar2(3);

cd_ans_w			varchar2(20);
dt_radioterapia_w		date;
ds_procedimento_cirurgico_w	varchar2(40);
dt_real_proc_cirurgico_w	date;
ds_area_irradiada_w		varchar2(40);

nr_sequencia_req_w			number(10);
nr_seq_apresentacao_w		number(10);
nr_med_w			number(10);
i				number(10);

dt_diagnostico_w		date;
cd_diagnostico_imagem_w		varchar2(1);
ie_estadia_tumor_w		varchar2(1);
ie_capacidade_funcional_w	varchar2(1);
cd_finalidade_tratamento_w	varchar2(1);
ds_diagnostico_w		varchar2(1000);
ds_observacao_w			varchar2(4000);
nr_seq_segurado_w		varchar2(15);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
dt_requisicao_w			pls_requisicao.dt_requisicao%type;

cursor c01 is
	select	b.dt_prevista,
		pls_obter_cod_tabela_tiss(null,null, c.nr_seq_material) cd_tabela,
		substr(pls_obter_seq_codigo_material(c.nr_seq_material,null),1,10),
		substr(pls_obter_desc_material(c.nr_seq_material),1,150),
		b.qt_dosagem_total,		
		b.ie_via_administracao,
		b.ie_frequencia_dose,
		b.qt_unidade_medida
	from 	pls_lote_anexo_guias_aut a,
		pls_lote_anexo_mat_aut b,
		pls_requisicao_mat c
	where	b.nr_seq_lote_anexo_guia 	= a.nr_sequencia
	and	c.nr_sequencia 			= b.nr_seq_req_mat
	and	a.nr_seq_requisicao 		= nr_seq_requisicao_p
	and	a.ie_tipo_anexo = 'QU';
	
cursor c02 is	--Diagn�stico oncol�gico
	select	b.dt_diagnostico,
		b.cd_doenca,
		b.cd_diagnostico_imagem,
		b.ie_estadia_tumor,
		b.ie_capacidade_funcional,
		b.cd_finalidade_tratamento,
		b.ds_diagnostico,
		b.ds_observacao
	from	pls_lote_anexo_diag_aut		b,
		pls_lote_anexo_guias_aut	a
	where	b.nr_seq_lote_anexo_guia 	= a.nr_sequencia
	and	a.nr_seq_requisicao 		= nr_seq_requisicao_p
	and	(a.nr_sequencia 		= nr_seq_lote_guia_p
	or	0 = nvl(nr_seq_lote_guia_p, 0))
	and	a.ie_tipo_anexo 		= 'QU'
	order by ie_tipo_diagnostico desc, b.nr_sequencia asc;
	/*N�o mudar a ordena��o, � necess�rio trazer primeiro os dados do atendimento e depois os diagn�sticos */

begin

delete	from w_tiss_proc_paciente
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_guia
where	nm_usuario		= nm_usuario_p;

commit;

if	(nr_seq_requisicao_p is not null) then
	begin
		select	a.dt_radioterapia,
			a.ds_procedimento_cirurgico,
			a.dt_real_proc_cirurgico,
			a.ds_area_irradiada,
			nvl(b.dt_fim_processo_req, b.dt_requisicao)
		into	dt_radioterapia_w,
			ds_procedimento_cirurgico_w,
			dt_real_proc_cirurgico_w,
			ds_area_irradiada_w,
			dt_requisicao_w
		from 	pls_lote_anexo_guias_aut a,
			pls_requisicao b
		where	a.nr_seq_requisicao = b.nr_sequencia
		and	a.nr_seq_requisicao = nr_seq_requisicao_p
		and	a.ie_tipo_anexo = 'QU';
		
	exception
	when others then
		dt_radioterapia_w		:= null;
		ds_procedimento_cirurgico_w	:= null;
		dt_real_proc_cirurgico_w	:= null;
		ds_area_irradiada_w		:= null;
		cd_estabelecimento_w		:= null;
	end;
	
	
	
	begin
		select	cd_estabelecimento
		into	cd_estabelecimento_w
		from	pls_requisicao
		where	nr_sequencia = nr_seq_requisicao_p;
	exception
	when others then
		cd_estabelecimento_w := null;
	end;
	
	cd_ans_w	:= pls_obter_dados_outorgante(cd_estabelecimento_w,'ANS');
	
	if(cd_ans_w is null) then		
		select	max(cd_ans)
		into	cd_ans_w
		from	pls_outorgante
		where	cd_ans	is not null;
	end if;

	nr_sequencia_req_w := nr_seq_requisicao_p;
	
	insert into w_tiss_guia
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_entrada,		--dt_radioterapia
		ds_especific_mat,	--ds_procedimento_cirurgico
		dt_autorizacao,		--dt_real_proc_cirurgico
		ds_observacao,		--ds_area_irradiada
		nr_seq_guia,
		cd_ans)
	values	(w_tiss_guia_seq.nextval,
		sysdate,
		nm_usuario_p,
		dt_radioterapia_w,
		ds_procedimento_cirurgico_w,
		dt_requisicao_w,
		ds_area_irradiada_w,
		nr_sequencia_req_w,
		cd_ans_w);
	
	nr_seq_apresentacao_w := 1;

	nr_med_w := 1;

	open c01;
	loop
	fetch c01 into	dt_prev_realizacao_w,
			cd_tipo_tabela_w,
			cd_medicamento_w,
			ds_medicamento_w,
			qt_doses_w,
			via_adm_w,
			frequencia_w,
			qt_unidade_medida_w;
	exit when c01%notfound;

	begin
		tb_med_w(nr_med_w).dt_prev_w		:= dt_prev_realizacao_w;
		tb_med_w(nr_med_w).cd_tabela_w		:= cd_tipo_tabela_w;
		tb_med_w(nr_med_w).cd_med_w		:= cd_medicamento_w;
		tb_med_w(nr_med_w).ds_med_w		:= ds_medicamento_w;
		tb_med_w(nr_med_w).qt_doses_w		:= qt_doses_w;
		tb_med_w(nr_med_w).via_adm_w		:= via_adm_w;
		tb_med_w(nr_med_w).freq_w		:= frequencia_w;
		tb_med_w(nr_med_w).nr_seq_apre_w	:= nr_seq_apresentacao_w;
		tb_med_w(nr_med_w).qt_unidade_medida_w	:= qt_unidade_medida_w;
		nr_med_w := nr_med_w + 1;
		nr_seq_apresentacao_w := nr_seq_apresentacao_w + 1;
	end;
	end loop;
	close c01;
	
	if (nr_med_w < 9) then
		for i in 1.. 8 loop
			begin
				if	( i = nr_med_w ) then
					tb_med_w(nr_med_w).dt_prev_w		:= null;
					tb_med_w(nr_med_w).cd_tabela_w		:= null;
					tb_med_w(nr_med_w).cd_med_w		:= null;
					tb_med_w(nr_med_w).ds_med_w		:= null;
					tb_med_w(nr_med_w).qt_doses_w		:= null;
					tb_med_w(nr_med_w).via_adm_w		:= null;
					tb_med_w(nr_med_w).freq_w		:= null;
					tb_med_w(nr_med_w).nr_seq_apre_w	:= nr_seq_apresentacao_w;
					tb_med_w(nr_med_w).qt_unidade_medida_w	:= null;
					nr_med_w := nr_med_w + 1;
					nr_seq_apresentacao_w := nr_seq_apresentacao_w + 1;
				end if;
			end;
		end loop;
	end if;	

	for i in 1.. 8  loop
		begin
		insert into w_tiss_proc_paciente
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_procedimento,
			cd_edicao_amb,
			cd_procedimento,
			ds_procedimento,
			qt_procedimento,
			ie_via_acesso,
			ie_funcao_medico,
			nr_seq_apresentacao,
			nr_seq_guia,
			ds_face_dente)
		values	(w_tiss_proc_paciente_seq.nextval,
			sysdate,
			nm_usuario_p,
			tb_med_w(i).dt_prev_w,
			tb_med_w(i).cd_tabela_w,
			tb_med_w(i).cd_med_w,
			tb_med_w(i).ds_med_w,
			tb_med_w(i).qt_doses_w,
			tb_med_w(i).via_adm_w,
			tb_med_w(i).freq_w,
			tb_med_w(i).nr_seq_apre_w,
			nr_sequencia_req_w,
			tb_med_w(i).qt_unidade_medida_w);
		end;
	end loop;
	
	
	open c02;
	loop
	fetch c02 into	dt_diagnostico_w,
			cd_doenca_w,
			cd_diagnostico_imagem_w,
			ie_estadia_tumor_w,
			ie_capacidade_funcional_w,
			cd_finalidade_tratamento_w,
			ds_diagnostico_w,
			ds_observacao_w;
	exit when C02%notfound;				
		begin
			tb_cid_w(nr_cid_w).dt_diag_w		:= dt_diagnostico_w;
			tb_cid_w(nr_cid_w).cd_cid_w		:= cd_doenca_w;
			tb_cid_w(nr_cid_w).cd_diag_imag_w	:= cd_diagnostico_imagem_w;
			tb_cid_w(nr_cid_w).ie_est_tumor_w	:= ie_estadia_tumor_w;
			tb_cid_w(nr_cid_w).ie_cap_func_w	:= ie_capacidade_funcional_w;
			tb_cid_w(nr_cid_w).cd_fin_trat_w	:= cd_finalidade_tratamento_w;
			tb_cid_w(nr_cid_w).ds_diag_w		:= ds_diagnostico_w;
			tb_cid_w(nr_cid_w).ds_obs_w		:= ds_observacao_w;
			nr_cid_w := nr_cid_w + 1;
		end;
	end loop;
	close c02;

	if(nr_cid_w = 1 )then
		insert into w_tiss_solicitacao
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_diagnostico,	cd_cid,	cd_diagnostico_imagem,
			cd_estadiamento_tumor,	ie_capacidade_funcional,cd_finalidade_tratamento,
			ds_diagnostico,	ds_observacao,nr_seq_guia)
		values	(w_tiss_solicitacao_seq.nextval, sysdate,nm_usuario_p,
			tb_cid_w(0).dt_diag_w,	tb_cid_w(0).cd_cid_w,	tb_cid_w(0).cd_diag_imag_w,
			tb_cid_w(0).ie_est_tumor_w, tb_cid_w(0).ie_cap_func_w, tb_cid_w(0).cd_fin_trat_w,
			tb_cid_w(0).ds_diag_w, tb_cid_w(0).ds_obs_w, nr_seq_requisicao_p);
	elsif(nr_cid_w = 2 )then
		insert into w_tiss_solicitacao
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_diagnostico, cd_cid,	cd_cid2,
			cd_diagnostico_imagem, cd_estadiamento_tumor, ie_capacidade_funcional,
			cd_finalidade_tratamento, ds_diagnostico, ds_observacao,
			nr_seq_guia)
		values	(w_tiss_solicitacao_seq.nextval, sysdate, nm_usuario_p,
			tb_cid_w(0).dt_diag_w,	tb_cid_w(1).cd_cid_w, tb_cid_w(0).cd_cid_w,
			tb_cid_w(0).cd_diag_imag_w, tb_cid_w(0).ie_est_tumor_w,	tb_cid_w(0).ie_cap_func_w,
			tb_cid_w(0).cd_fin_trat_w, tb_cid_w(0).ds_diag_w, tb_cid_w(0).ds_obs_w,
			nr_seq_requisicao_p);
	elsif(nr_cid_w = 3 )then
		insert into w_tiss_solicitacao
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_diagnostico, cd_cid, cd_cid2,
			cd_cid3, cd_diagnostico_imagem, cd_estadiamento_tumor,
			ie_capacidade_funcional, cd_finalidade_tratamento, ds_diagnostico,
			ds_observacao, nr_seq_guia)
		values	(w_tiss_solicitacao_seq.nextval, sysdate, nm_usuario_p,
			tb_cid_w(0).dt_diag_w, tb_cid_w(1).cd_cid_w, tb_cid_w(2).cd_cid_w,
			tb_cid_w(0).cd_cid_w, tb_cid_w(0).cd_diag_imag_w, tb_cid_w(0).ie_est_tumor_w,
			tb_cid_w(0).ie_cap_func_w, tb_cid_w(0).cd_fin_trat_w, tb_cid_w(0).ds_diag_w,
			tb_cid_w(0).ds_obs_w,nr_seq_requisicao_p);
	elsif(nr_cid_w = 4 )then
		insert into w_tiss_solicitacao
			(nr_sequencia, dt_atualizacao,nm_usuario,
			dt_diagnostico, cd_cid, cd_cid2,
			cd_cid3, cd_cid4, cd_diagnostico_imagem, cd_estadiamento_tumor,
			ie_capacidade_funcional, cd_finalidade_tratamento, ds_diagnostico,
			ds_observacao, nr_seq_guia)	
		values	(w_tiss_solicitacao_seq.nextval, sysdate,
			nm_usuario_p, tb_cid_w(0).dt_diag_w, tb_cid_w(1).cd_cid_w,
			tb_cid_w(2).cd_cid_w, tb_cid_w(3).cd_cid_w, tb_cid_w(0).cd_cid_w,
			tb_cid_w(0).cd_diag_imag_w, tb_cid_w(0).ie_est_tumor_w,	tb_cid_w(0).ie_cap_func_w,
			tb_cid_w(0).cd_fin_trat_w, tb_cid_w(0).ds_diag_w, tb_cid_w(0).ds_obs_w,
			nr_seq_requisicao_p);
	elsif(nr_cid_w = 5 )then
		insert into w_tiss_solicitacao
			(nr_sequencia,dt_atualizacao,nm_usuario,
			dt_diagnostico,cd_cid,cd_cid2,
			cd_cid3,cd_cid4,cd_diagnostico_imagem,
			cd_estadiamento_tumor,ie_capacidade_funcional,cd_finalidade_tratamento,
			ds_diagnostico, ds_observacao, nr_seq_guia)	
		values	(w_tiss_solicitacao_seq.nextval, sysdate,nm_usuario_p,
			tb_cid_w(0).dt_diag_w, tb_cid_w(1).cd_cid_w,tb_cid_w(2).cd_cid_w,
			tb_cid_w(3).cd_cid_w,tb_cid_w(4).cd_cid_w,tb_cid_w(0).cd_diag_imag_w,
			tb_cid_w(0).ie_est_tumor_w,tb_cid_w(0).ie_cap_func_w,tb_cid_w(0).cd_fin_trat_w,
			tb_cid_w(0).ds_diag_w,tb_cid_w(0).ds_obs_w,nr_seq_requisicao_p);
	end if;
end if;

commit;

end pls_tiss_gerar_w_req_quimio;
/
