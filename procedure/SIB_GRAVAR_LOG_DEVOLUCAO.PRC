create or replace
procedure sib_gravar_log_devolucao
		(	nr_seq_lote_sib_p	number,
			ie_tipo_arquivo_p	varchar2,
			nr_interno_p		number,
			ds_observacao_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 
			
ie_tipo_envio_sib_w	varchar2(10);
nr_seq_lote_envio_w	number(10);
nr_seq_lote_reenvio_w	number(10);

begin

select	max(ie_tipo_envio)
into	ie_tipo_envio_sib_w
from	pls_lote_sib
where	nr_sequencia	= nr_seq_lote_sib_p;

if	(ie_tipo_envio_sib_w	= 'E') then
	nr_seq_lote_envio_w	:= nr_seq_lote_sib_p;
	nr_seq_lote_reenvio_w	:= null;
elsif	(ie_tipo_envio_sib_w	= 'R') then
	nr_seq_lote_envio_w	:= null;
	nr_seq_lote_reenvio_w	:= nr_seq_lote_sib_p;
end if;

insert into sib_devolucao_log
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		cd_estabelecimento,nr_seq_lote_sib_envio,nr_seq_lote_sib_reenvio,ds_log,ie_tipo_arquivo,
		nr_interno)
values	(	sib_devolucao_log_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		cd_estabelecimento_p,nr_seq_lote_envio_w,nr_seq_lote_reenvio_w,ds_observacao_p,ie_tipo_arquivo_p,
		nr_interno_p);

end sib_gravar_log_devolucao;
/
