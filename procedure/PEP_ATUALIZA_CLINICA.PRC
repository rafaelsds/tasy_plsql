create or replace 
procedure pep_atualiza_clinica(	
		ie_clinica_p		number,	
		nr_seq_queixa_p		number,
		nr_atendimento_p	number,
		nr_seq_classificacao_p	number,
		nm_usuario_p		varchar2) is

begin

if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then

	if (ie_clinica_p is not null) and (nr_seq_queixa_p > 0) then
		begin
		update	atendimento_paciente
		set	ie_clinica 		= ie_clinica_p,
			nr_seq_queixa 	= nr_seq_queixa_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where 	nr_atendimento 		= nr_atendimento_p;
		end;
	elsif (ie_clinica_p is not null) then
		begin
		update	atendimento_paciente
		set	ie_clinica 		= ie_clinica_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao	= sysdate
		where 	nr_atendimento 		= nr_atendimento_p;
		end;
	end if;	

	if (nr_seq_classificacao_p is not null) and (nr_seq_classificacao_p > 0) then
		begin
		update	atendimento_paciente
		set	nr_seq_classificacao 	= nr_seq_classificacao_p,
			nm_usuario				= nm_usuario_p,
			dt_atualizacao			= sysdate
		where 	nr_atendimento 		= nr_atendimento_p;	
		end;
	end if;
	
	commit;

END IF;
end pep_atualiza_clinica;
/
