create or replace
procedure pls_dados_prestador_web(cd_prestador_p		Varchar2,
				      nr_seq_prestador_p   out	Number,
				      nm_prestador_p   	   out	Varchar2,
				      cd_cnes_p 	   out	Varchar2) is
				
nm_prestador_w		Varchar2(80) := null;
nr_seq_prestador_w	Number(10) := null;
cd_prestador_w		Varchar2(30) := null;
cd_cnes_w		VARCHAR2(20) := null;

begin

nr_seq_prestador_w 	:= nr_seq_prestador_p;
cd_prestador_w		:= cd_prestador_p;

if	(cd_prestador_w is not null) then
	select	max(a.nr_sequencia)
	into	nr_seq_prestador_w
	from	pls_prestador a
	where	a.cd_prestador = cd_prestador_w;
	
	if	(nr_seq_prestador_w is not null) then
		select	max(nm_interno)
		into	nm_prestador_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prestador_w;
		
		select 	substr(pls_obter_cnes_prestador(nr_seq_prestador_w),1,40)
		into	cd_cnes_w
		from 	dual;
		
		if	(nm_prestador_w is null) then
			select	substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc),1,255)
			into 	nm_prestador_w
			from	pls_prestador
			where	nr_sequencia = nr_seq_prestador_w;	
		end if;
	end if;
end if;
	
nm_prestador_p 		:= nm_prestador_w;
nr_seq_prestador_p 	:= nr_seq_prestador_w;
cd_cnes_p		:= cd_cnes_w;

end pls_dados_prestador_web;
/
