create or replace PROCEDURE CLASSIFY_SERVICE_PACK(IE_CLASSIFICACAO_P varchar2,
                                                  NR_SERVICE_PACK_P number,
                                                  NR_PACOTE_P number,
                                                  NR_OLD_BUILD_P number,
												  NM_USUARIO_P varchar2) is

log_w varchar2(4000);
old_classf_w varchar2(30);
cd_versao_w varchar2(10);

BEGIN
    begin
      select sp.ie_classificacao, sp.cd_versao
      into old_classf_w, cd_versao_w
      from man_service_pack_versao sp
      where sp.NR_SERVICE_PACK = NR_SERVICE_PACK_P
      and sp.NR_PACOTE = NR_PACOTE_P
      and sp.NR_OLD_BUILD = NR_OLD_BUILD_P;
      exception 
      when no_data_found then
      begin
        old_classf_w := null;
      end;
    end;

    if(old_classf_w is null or old_classf_w = '') then
    begin
      old_classf_w := Expressao_Pck.Obter_Desc_Expressao(327236);
    end;
    else
      old_classf_w := obter_valor_dominio(9905, old_classf_w);
    end if;

    log_w := Expressao_Pck.Obter_Desc_Expressao(1043525);

    log_w := REPLACE(log_w, '@#OLD_CLASSF@#', old_classf_w);
    log_w := REPLACE(log_w, '@#NEW_CLASSF@#', obter_valor_dominio(9905, IE_CLASSIFICACAO_P));

    insert into log_classificacao_sp
    (
      nr_sequencia,
      nm_usuario,
      dt_atualizacao,
      cd_versao,
      nr_service_pack,
      ds_alteracao
    ) values (
      log_classificacao_sp_seq.nextval,
      nm_usuario_p,
      sysdate,
      cd_versao_w,
      nr_service_pack_p,
      log_w
    );

    update man_service_pack_versao sp
    set sp.IE_CLASSIFICACAO = IE_CLASSIFICACAO_P,
    sp.nm_usuario = nm_usuario_p
    where sp.NR_SERVICE_PACK = NR_SERVICE_PACK_P
    and sp.NR_PACOTE = NR_PACOTE_P
    and sp.NR_OLD_BUILD = NR_OLD_BUILD_P;
    
    commit;
END CLASSIFY_SERVICE_PACK;
/