create or replace
procedure pls_iniciar_imp_fila_web(	nr_seq_xml_arquivo_p		Number) is

ds_xml_w		long;
nr_seq_lote_w		Number(10);


begin

if	(nr_seq_xml_arquivo_p is not null) then
	update	pls_xml_arquivo
	set	ie_status = 'IMP',
		dt_inicio_importacao = sysdate
	where	nr_sequencia = nr_seq_xml_arquivo_p
	and	ie_status = 'VAL'
	and	ie_tipo_processo in (1,2);
	
	commit;
end if; 

end pls_iniciar_imp_fila_web;
/
