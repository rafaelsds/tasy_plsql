create or replace
procedure gerar_alt_status_regulacao(	nr_seq_parecer_int_w	number,
										ie_status_p				varchar2) is 
									
	nr_seq_regulacao_w	regulacao_atend.nr_sequencia%type;
	
begin

	if (nr_seq_parecer_int_w is not null and ie_status_p is not null) then
		
		select	max(nr_sequencia)
		into	nr_seq_regulacao_w	
		from	regulacao_atend 
		where	nr_seq_resp_parecer_ori = nr_seq_parecer_int_w;
		
		if (nr_seq_regulacao_w is not null) then
		
			update	PARECER_MEDICO
			set		dt_liberacao = sysdate,
					nm_usuario = wheb_usuario_pck.get_nm_usuario,
					dt_atualizacao = sysdate
			where	NR_SEQ_INTERNO = nr_seq_parecer_int_w;
		
			update	WL_WORKLIST
			set		dt_final_real = sysdate,
					nm_usuario = wheb_usuario_pck.get_nm_usuario,
					dt_atualizacao = sysdate
			where	nr_seq_regulacao_atend = nr_seq_regulacao_w;
								
			Alterar_status_regulacao(nr_seq_regulacao_w, ie_status_p, null);			
			
			commit;
			
		end if;

	end if;

end gerar_alt_status_regulacao;
/
