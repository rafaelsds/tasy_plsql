create or replace
procedure HS_verifica_revalic_doc(	
				cd_estabelecimento_p		number,
				ie_comunic_interna_p		varchar2,
				ie_email_p			varchar2,
				nm_usuario_p			varchar2) is
				
dt_aprovacao_w				date;
dt_revalidacao_w				date;
nm_usuario_elaboracao_w			varchar2(15);
nm_usuario_validacao_w			varchar2(15);
nm_usuario_aprov_w			varchar2(15);
nm_usuario_revalic_w			varchar2(15);
qt_dias_entre_datas_w			number(05,0);
nm_usuario_destino_w			varchar2(2000);
ds_comunicado_w				varchar2(2000);
ds_email_destino_w				varchar2(2000);
ds_email_w				varchar2(255);
nr_seq_classif_w				number(10,0);
qt_dias_revisao_w				number(05,0);
nr_sequencia_w				number(10,0);

Cursor C01 is
	select	nr_sequencia,
		dt_aprovacao,
		dt_revalidacao,
		substr(obter_usuario_pessoa(cd_pessoa_elaboracao),1,15) nm_usuario_elaboracao,
		substr(obter_usuario_pessoa(cd_pessoa_validacao),1,15) nm_usuario_validacao,
		substr(obter_usuario_pessoa(cd_pessoa_aprov),1,15) nm_pessoa_aprov,
		substr('Senhor gestor, favor atualizar ou revalidar o documento: ' || chr(13) || chr(10) ||
			cd_documento || ' - ' || nm_documento || chr(13) || chr(10) ||
			'Orienta��es gerais ' || chr(13) || chr(10) ||
			'1. Orienta��es para revalida��o de documentos no TASY: ' || chr(13) || chr(10) ||
			'1.1. Abrir o Tasy com seu usu�rio e senha. ' || chr(13) || chr(10) ||
			'1.2. Acessar o perfil Qualidade Aprov. ' || chr(13) || chr(10) ||
			'1.3. Localizar o documento a ser revalidado. ' || chr(13) || chr(10) ||
			'1.4. Posicionar o cursor no documento a ser revalidado. ' || chr(13) || chr(10) ||
			'1.5. Clicar no bot�o Detalhe (� direita na tela), clicar com o bot�o direito e selecionar a op��o Revalidar documento.' || chr(13) || chr(10) ||
			'2. Se n�o houver altera��o no conte�do, n�o ser� necess�rio imprimir o documento.' || chr(13) || chr(10) ||
			'Coordena��o da Qualidade',1,2000),
		qt_dias_revisao		
	from	qua_documento
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_status 		= 'D'
	and	ie_situacao		= 'A'
	and	qt_dias_revisao > 0
	and	dt_aprovacao is not null
	and	trunc(obter_dias_entre_datas(nvl(dt_revalidacao, dt_aprovacao) + qt_dias_revisao,sysdate)) in (60,30,15,0);

Cursor C02 is
	select	distinct
		substr(obter_usuario_pessoa(cd_pessoa_revalidacao),1,15)
	from	qua_doc_revalidacao
	where	nr_seq_documento = nr_sequencia_w
	order by 1;

begin
select	min(nr_sequencia)
into	nr_seq_classif_w
from	comunic_interna_classif
where	ie_tipo = 'F';

open C01;
loop
fetch C01 into
	nr_sequencia_w,
	dt_aprovacao_w,
	dt_revalidacao_w,
	nm_usuario_elaboracao_w,
	nm_usuario_validacao_w,
	nm_usuario_aprov_w,
	ds_comunicado_w,
	qt_dias_revisao_w;
exit when C01%notfound;
	begin
	nm_usuario_destino_w	:= '';
	ds_email_destino_w	:= 'qualidade@samaritano.org.br,';
	
	select	trunc(obter_dias_entre_datas(nvl(dt_revalidacao_w, dt_aprovacao_w) + qt_dias_revisao_w,sysdate))
	into	qt_dias_entre_datas_w
	from	dual;
	
	if	(nvl(nm_usuario_elaboracao_w,'X') <> 'X') and
		(instr(nvl(nm_usuario_destino_w,'X'), nm_usuario_elaboracao_w) = 0) then
		begin
		nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_elaboracao_w || ', ',1,2000);
		
		select	substr(obter_dados_usuario_opcao(nm_usuario_elaboracao_w,'E'),1,255)
		into	ds_email_w
		from	dual;
		
		if	(nvl(ds_email_w,'X') <> 'X') and
			(instr(nvl(ds_email_destino_w,'X'), ds_email_w) = 0) then
			ds_email_destino_w := substr(ds_email_destino_w || ds_email_w || ',',1,2000);
		end if;
		end;
	end if;
	
	if	(nvl(nm_usuario_validacao_w,'X') <> 'X') and
		(instr(nvl(nm_usuario_destino_w,'X'), nm_usuario_validacao_w) = 0) then
		begin
		nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_validacao_w || ', ',1,2000);
		
		select	substr(obter_dados_usuario_opcao(nm_usuario_validacao_w,'E'),1,255)
		into	ds_email_w
		from	dual;
		
		if	(nvl(ds_email_w,'X') <> 'X') and
			(instr(nvl(ds_email_destino_w,'X'), ds_email_w) = 0) then
			ds_email_destino_w := substr(ds_email_destino_w || ds_email_w || ',',1,2000);
		end if;
		end;
	end if;
	
	if	(nvl(nm_usuario_aprov_w,'X') <> 'X') and
		(instr(nvl(nm_usuario_destino_w,'X'), nm_usuario_aprov_w) = 0) then
		begin
		nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_aprov_w || ', ',1,2000);
		
		select	substr(obter_dados_usuario_opcao(nm_usuario_aprov_w,'E'),1,255)
		into	ds_email_w
		from	dual;
		
		if	(nvl(ds_email_w,'X') <> 'X') and
			(instr(nvl(ds_email_destino_w,'X'), ds_email_w) = 0) then
			ds_email_destino_w := substr(ds_email_destino_w || ds_email_w || ',',1,2000);
		end if;
		end;
	end if;
	
	open C02;
	loop
	fetch C02 into	
		nm_usuario_revalic_w;
	exit when C02%notfound;
		begin
		if	(nvl(nm_usuario_revalic_w,'X') <> 'X') and
			(instr(nvl(nm_usuario_destino_w,'X'), nm_usuario_revalic_w) = 0) then
			begin
			nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_revalic_w || ', ',1,2000);
			
			select	substr(obter_dados_usuario_opcao(nm_usuario_revalic_w,'E'),1,255)
			into	ds_email_w
			from	dual;
			
			if	(nvl(ds_email_w,'X') <> 'X') and
				(instr(nvl(ds_email_destino_w,'X'), ds_email_w) = 0) then
				ds_email_destino_w := substr(ds_email_destino_w || ds_email_w || ',',1,2000);
			end if;
			end;
		end if;
		end;
	end loop;
	close C02;
	
	if	(nvl(ie_comunic_interna_p,'S') = 'S') then
		gerar_comunic_padrao(	sysdate,
					'Revalida��o de Documentos da Qualidade',
					ds_comunicado_w,
					nm_usuario_p,
					'N',
					nm_usuario_destino_w,
					'N',
					nr_seq_classif_w,
					'',
					cd_estabelecimento_p,
					'68,', --setor da qualidade
					sysdate,
					'',
					'');
	end if;
	
	if	(nvl(ie_email_p,'S') = 'S') and
		(nvl(ds_email_destino_w,'X') <> 'X') then
		begin
		enviar_email(	'Revalida��o de Documentos da Qualidade',
				ds_comunicado_w,
				null,
				ds_email_destino_w,
				nm_usuario_p,
				'M');
		exception
		when others then
			null;
		end;
	end if;
	
	if	(qt_dias_entre_datas_w = 0) then
		update	qua_documento
		set	ie_status	= 'R'
		where	nr_sequencia	= nr_sequencia_w;
	end if;
	end;
end loop;
close C01;

commit;

end HS_verifica_revalic_doc;
/