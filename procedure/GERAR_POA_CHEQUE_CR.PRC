create or replace
procedure GERAR_POA_CHEQUE_CR is

/*
create	table poa_cheque_cr
	(cd_agencia_bancaria	varchar2(8),
	cd_banco		number(5),
	cd_pessoa_fisica	varchar2(10),
	ds_observacao		varchar(4000),
	dt_contabil		date,
	dt_registro		date,
	dt_vencimento		date,
	nr_cheque		varchar2(20),
	nr_conta		varchar2(20),
	vl_cheque		number(15,2),
	vl_terceiro		number(15,2),
	vl_saldo_negociado	number(15,2));
*/

cd_agencia_bancaria_w	varchar2(8);
cd_banco_w		number(5);
cd_pessoa_fisica_w	varchar2(10);
ds_observacao_w		varchar(4000);
dt_contabil_w		date;
dt_registro_w		date;
dt_vencimento_w		date;
nr_cheque_w		varchar2(20);
nr_conta_w		varchar2(20);
vl_cheque_w		number(15,2);
vl_terceiro_w		number(15,2);
vl_saldo_negociado_w	number(15,2);

qt_cont_w		number(10);
cd_estabelecimento_w	number(4);
cd_moeda_w		number(5);

cursor	c01 is
select	a.cd_agencia_bancaria,
	a.cd_banco,
	a.cd_pessoa_fisica,
	a.ds_observacao,
	a.dt_contabil,
	a.dt_registro,
	a.dt_vencimento,
	a.nr_cheque,
	a.nr_conta,
	nvl(a.vl_cheque,0),
	nvl(a.vl_terceiro,0),
	nvl(a.vl_saldo_negociado,0)
from	poa_cheque_cr a;

cursor	c02 is
select	distinct
	a.cd_pessoa_fisica
from	poa_cheque_cr a
where	a.cd_pessoa_fisica	is not null;

cursor	c03 is
select	distinct
	a.cd_banco
from	poa_cheque_cr a;

begin

delete	from log_tasy
where	cd_log	= 99891;

open	c02;
loop
fetch	c02 into
	cd_pessoa_fisica_w;
exit	when c02%notfound;

	select	count(*)
	into	qt_cont_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;

	if	(qt_cont_w	= 0) then
		insert	into log_tasy
			(cd_log,
			ds_log,
			dt_atualizacao,
			nm_usuario)
		values	(99891,
			'A pessoa f�sica de c�digo ' || cd_pessoa_fisica_w || ' n�o foi localizada no Tasy!',
			sysdate,
			'Tasy');
	end if;

end	loop;
close	c02;

open	c03;
loop
fetch	c03 into
	cd_banco_w;
exit	when c03%notfound;

	select	count(*)
	into	qt_cont_w
	from	banco a
	where	a.cd_banco	= cd_banco_w;

	if	(qt_cont_w	= 0) then
		insert	into log_tasy
			(cd_log,
			ds_log,
			dt_atualizacao,
			nm_usuario)
		values	(99891,
			'O banco de c�digo ' || cd_banco_w || ' n�o foi localizado no Tasy!',
			sysdate,
			'Tasy');
	end if;

end	loop;
close	c03;

select	count(*)
into	qt_cont_w
from	log_tasy a
where	a.cd_log	= 99891;

if	(qt_cont_w	> 0) then
	delete	from poa_cheque_cr;
	commit;
	/* Foram encontradas inconsist�ncias no arquivo! Verifique o log de c�digo 99891! */
	wheb_mensagem_pck.exibir_mensagem_abort(262743);
end if;

select	min(a.cd_estabelecimento)
into	cd_estabelecimento_w
from	estabelecimento a;

select	max(a.cd_moeda_padrao)
into	cd_moeda_w
from	parametro_contas_receber a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

open	c01;
loop
fetch	c01 into
	cd_agencia_bancaria_w,
	cd_banco_w,
	cd_pessoa_fisica_w,
	ds_observacao_w,
	dt_contabil_w,
	dt_registro_w,
	dt_vencimento_w,
	nr_cheque_w,
	nr_conta_w,
	vl_cheque_w,
	vl_terceiro_w,
	vl_saldo_negociado_w;
exit	when c01%notfound;

	begin
	insert	into cheque_cr
		(cd_agencia_bancaria,
		cd_banco,
		cd_estabelecimento,
		cd_moeda,
		cd_pessoa_fisica,
		ds_observacao,
		dt_atualizacao,
		dt_contabil,
		dt_registro,
		dt_vencimento,
		ie_lib_caixa,
		nm_usuario,
		nr_cheque,
		nr_conta,
		nr_seq_cheque,
		vl_cheque,
		vl_terceiro,
		vl_saldo_negociado)
	values	(cd_agencia_bancaria_w,
		cd_banco_w,
		cd_estabelecimento_w,
		cd_moeda_w,
		cd_pessoa_fisica_w,
		ds_observacao_w,
		sysdate,
		dt_contabil_w,
		dt_registro_w,
		dt_vencimento_w,
		'S',
		'Tasy',
		nr_cheque_w,
		nr_conta_w,
		cheque_cr_seq.nextval,
		vl_cheque_w,
		vl_terceiro_w,
		vl_saldo_negociado_w);
	exception
	when others then
		delete	from poa_cheque_cr;
		commit;
		/* Ocorreu um problema ao copiar o cheque para o Tasy!
		sqlerrm */
		wheb_mensagem_pck.exibir_mensagem_abort(262745,'SQLERRM_W='||sqlerrm);
	end;

end	loop;
close	c01;

delete	from poa_cheque_cr;

commit;

end GERAR_POA_CHEQUE_CR;
/