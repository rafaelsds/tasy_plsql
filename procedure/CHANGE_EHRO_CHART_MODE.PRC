create or replace 
PROCEDURE change_ehro_chart_mode (ie_agrupado_p		VARCHAR2,
						nm_usuario_p	VARCHAR2,
						ie_apos_leitura_p VARCHAR2,
						ie_ocultar_item_p VARCHAR2) IS

record_count_w NUMBER(1);
						
BEGIN

SELECT COUNT(1)
INTO record_count_w
FROM PEPO_GRAFICO_USUARIO
WHERE nm_usuario = nm_usuario_p;

IF(record_count_w > 0) THEN 

	IF(ie_apos_leitura_p = 'N') THEN
		update PEPO_GRAFICO_USUARIO
		set ie_agrupado = ie_agrupado_p,
			ie_ocultar_item = ie_ocultar_item_p,
			dt_atualizacao = sysdate
		where nm_usuario = nm_usuario_p;
	END IF;
	
ELSE
	
	INSERT INTO PEPO_GRAFICO_USUARIO
	VALUES(SYSDATE, nm_usuario_p, SYSDATE, nm_usuario_p, ie_agrupado_p, ie_ocultar_item_p);

END IF;
	
COMMIT;

END change_ehro_chart_mode;
/
