create or replace
procedure gerar_remessa_caixa_cnab_240(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2)  is 

/*UTL File*/
arq_texto_w			utl_file.file_type;
ds_conteudo_w			varchar2(240);
ds_erro_w			varchar2(255);
ds_local_w			varchar2(255);
nm_arquivo_w			varchar2(255);
ds_mensagem_w			varchar2(255);

/*	Brancos		*/
ds_brancos_211_w		varchar2(211);
ds_brancos_148_w		varchar2(148);
ds_brancos_80_w			varchar2(80);
ds_brancos_40_w			varchar2(40);
ds_brancos_33_w			varchar2(33);
ds_brancos_25_w			varchar2(25);
ds_brancos_20_w			varchar2(20);
ds_brancos_10_w			varchar2(10);
ds_brancos_9_w			varchar2(9);
ds_brancos_8_w			varchar2(8);
ds_brancos_4_w			varchar2(4);

/*	Header do arquivo	*/
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(30);
cd_cgc_w			varchar2(14);
cd_agencia_bancaria_w		varchar2(5);
dt_remessa_retorno_w		date;

/*	Segmento P, Segmento Q, Segmento R	 */
nm_pessoa_w			varchar2(40);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(15);
ds_cidade_w			varchar2(15);
nr_inscricao_w			varchar2(15);
ds_nosso_numero_w		varchar2(15);
vl_titulo_w			varchar2(15);
vl_desconto_w			varchar2(15);
vl_juros_mora_w			varchar2(15);
nr_titulo_w			varchar2(11);
cd_cep_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_vencimento_w			varchar2(8);	
ds_uf_w				varchar2(2);
ie_tipo_inscricao_w		varchar2(1);

/*	Trailer de Lote	*/
vl_titulos_cobr_w		varchar2(15);
qt_titulos_cobr_w		number(5);
qt_registro_lote_w		number(4)	:= 0;

/*	Trailer do Arquivo	*/
qt_registro_w			number(5)	:= 0;	
qt_registro_P_w			number(6)	:= 0;
qt_registro_Q_w			number(6)	:= 0;

Cursor C01 is
	select	lpad(c.cd_agencia_bancaria,5,'0'),
		lpad(82||somente_numero(lpad(b.nr_titulo,8,'0')||'-'|| calcula_digito('Modulo11',(82||lpad(b.nr_titulo,8,'0')))),15,'0') ds_nosso_numero,
		lpad(b.nr_titulo,11,0) nr_titulo,
		to_char(nvl(b.dt_pagamento_previsto,sysdate),'DDMMYYYY') dt_vencimento,
		lpad(replace(to_char(b.vl_titulo, 'fm0000000000000.00'),'.',''),15,'0') vl_titulo,
		to_char(nvl(b.dt_emissao,sysdate),'DDMMYYYY') dt_emissao,
		lpad(replace(to_char(b.vl_titulo * b.tx_juros / 100 / 30, 'fm0000000000000.00'),'.',''),15,'0') vl_juros_mora,
		lpad(replace(to_char(b.TX_DESC_ANTECIPACAO, 'fm0000000000000.00'),'.',''),15,'0') vl_desconto,
		nvl(decode(b.cd_pessoa_fisica, null, '2', '1'),'0') ie_tipo_inscricao,
		lpad(decode(decode(b.cd_pessoa_fisica, null, 2, 1),2,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,(substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,9) || '0000' || substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),10,2)),'000000000000000'),15,'0') nr_inscricao,
		rpad(substr(nvl(elimina_caractere_especial(obter_nome_pf_pj(b.cd_pessoa_fisica, b.cd_cgc)),' '),1,40),40,' ') nm_pessoa,
		rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'E')),' '),1,40),40,' ') ds_endereco,
		rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'B')),' '),1,15),15,' ') ds_bairro,
		lpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),' '),1,8),8,'0') cd_cep,
		rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),' '),1,15),15,' ') ds_cidade,
		rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),' '),1,2),2,' ') ds_uf
	from	pls_mensalidade d,
		titulo_receber b,
		titulo_receber_cobr c,
		cobranca_escritural a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	d.nr_sequencia(+)	= b.nr_seq_mensalidade
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
begin
nm_arquivo_w	:= to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '.rem';

obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W'); --arq_texto_w := utl_file.fopen('/srvfs03/FINANCEIRO/TASY/',nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;	
	wheb_mensagem_pck.exibir_mensagem_abort(186768,'DS_ERRO_W=' || ds_erro_w);
end;
--arq_texto_w := utl_file.fopen('/oraprd03/utlfile/',nm_arquivo_w,'W');
--\\192.168.0.230\UTLFILE

update	cobranca_escritural
set	ds_arquivo	= ds_local_w || nm_arquivo_w
where	nr_sequencia	= nr_seq_cobr_escrit_p;

/*	Brancos		*/
select	lpad(' ',20,' '),
	lpad(' ',25,' '),
	lpad(' ',10,' '),
	lpad(' ',4,' '),
	lpad(' ',9,' '),
	lpad(' ',80,' '),
	lpad(' ',33,' '),
	lpad(' ',8,' '),
	lpad(' ',211,' '),
	lpad(' ',148,' '),
	lpad(' ',40,' ')
into	ds_brancos_20_w,
	ds_brancos_25_w,
	ds_brancos_10_w,
	ds_brancos_4_w,
	ds_brancos_9_w,
	ds_brancos_80_w,
	ds_brancos_33_w,
	ds_brancos_8_w,
	ds_brancos_211_w,
	ds_brancos_148_w,
	ds_brancos_40_w
from	dual;
/*	Fim - Brancos	*/

/*	Header do Arquivo	*/
select	lpad(b.cd_cgc,14,'0'),
	lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0') cd_agencia_bancaria,
	rpad(substr(nvl(elimina_caractere_especial(obter_razao_social(b.cd_cgc)),' '),1,30),30,' ') nm_empresa,
	rpad(substr(nvl(elimina_caractere_especial(obter_nome_banco(c.cd_banco)),' '),1,30),30,' ') nm_banco,
	nvl(a.dt_remessa_retorno,sysdate)
into	cd_cgc_w,
	cd_agencia_bancaria_w,
	nm_empresa_w,
	nm_banco_w,
	dt_remessa_retorno_w
from	banco_estabelecimento c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:= 	'104' ||
			'0000' || 
			'0' || 
			ds_brancos_9_w ||
			'2' ||
			cd_cgc_w ||
			'00000000000000000000' ||
			cd_agencia_bancaria_w ||
			'5' ||
			'001804' ||
			'0000000' ||
			'0' ||
			nm_empresa_w ||
			nm_banco_w ||
			ds_brancos_10_w ||
			'1' ||
			lpad(to_char(dt_remessa_retorno_w,'DDMMYYYY'),8,'0') ||
			lpad(to_char(dt_remessa_retorno_w,'hh24miss'),6,'0') ||
			'000000' ||
			'050' ||
			'00000' ||
			ds_brancos_20_w ||
			rpad('REMESSA-TESTE',20,' ') ||
			ds_brancos_4_w ||
			ds_brancos_25_w;
			
utl_file.put_line(arq_texto_w,ds_conteudo_w);
utl_file.fflush(arq_texto_w);
/*	Fim - Header do Arquivo	*/

/*	Header do Lote	*/
qt_registro_lote_w	:= qt_registro_lote_w + 1;
qt_registro_w		:= qt_registro_w + 1;
ds_conteudo_w		:= 	'104' ||
				lpad(qt_registro_lote_w,4,'0') ||
				'1' ||
				'R' ||
				'01' ||
				'00' ||
				'030' || 
				' ' ||
				'2' ||
				lpad(cd_cgc_w,15,'0') ||
				'001804' ||
				'00000000000000' ||
				cd_agencia_bancaria_w ||
				'5' ||
				'001804' ||
				'0000000' ||
				'0' ||
				nm_empresa_w ||
				ds_brancos_80_w ||
				'00000001' ||
				to_char(dt_remessa_retorno_w,'DDMMYYYY') ||
				'00000000' ||
				ds_brancos_33_w;		
				
utl_file.put_line(arq_texto_w,ds_conteudo_w);
utl_file.fflush(arq_texto_w);
/*	Fim - Header do Lote	*/

/*	Segmento P, Segmento Q, Segmento R	 */
open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	ds_nosso_numero_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_mora_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w;
exit when C01%notfound;
	begin
	/* 	Segmento P		*/
	--qt_registro_lote_w	:= qt_registro_lote_w + 1;
	qt_registro_w		:= qt_registro_w + 1;
	qt_registro_P_w		:= qt_registro_P_w + 1;
	ds_conteudo_w		:=	'104' ||
					lpad(qt_registro_lote_w,4,'0') ||
					'3' ||
					lpad(qt_registro_P_w,5,'0') ||
					'P' ||
					' ' ||
					'01' ||
					cd_agencia_bancaria_w ||
					'5' ||
					'001804' ||
					'00000000000' ||
					'14' ||
					ds_nosso_numero_w ||
					'1' ||
					'1' ||
					'2' ||
					'2' ||
					'0' ||
					nr_titulo_w ||
					ds_brancos_4_w ||
					dt_vencimento_w ||
					vl_titulo_w ||
					cd_agencia_bancaria_w ||
					'4' ||
					'99' ||
					'A' ||
					dt_emissao_w ||
					'2' ||
					'00000000' ||
					vl_juros_mora_w ||
					'0' ||
					'00000000' ||
					vl_desconto_w ||
					'000000000000000' ||
					'000000000000000' ||
					lpad(nr_titulo_w,25,'0') ||
					'3' ||
					'00' ||
					'1' ||
					'060' ||
					'09' ||
					'0000000000' ||
					' ';
	
	utl_file.put_line(arq_texto_w,ds_conteudo_w);
	utl_file.fflush(arq_texto_w);
	/*	Fim - Segmento P	*/
	
	/*	Segmento Q	*/
	--qt_registro_lote_w	:= qt_registro_lote_w + 1;
	qt_registro_w		:= qt_registro_w + 1;
	--qt_registro_Q_w		:= qt_registro_Q_w + 1;
	qt_registro_P_w		:= qt_registro_P_w + 1;
	
	ds_conteudo_w		:=	'104' ||
					lpad(qt_registro_lote_w,4,'0') ||
					'3' ||
					lpad(qt_registro_P_w,5,'0') ||
					'Q' ||
					' ' ||
					'01' ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					nm_pessoa_w ||
					ds_endereco_w ||
					ds_bairro_w ||
					cd_cep_w ||
					ds_cidade_w ||
					ds_uf_w ||
					'0' ||
					'000000000000000' ||
					ds_brancos_40_w ||
					'000' ||
					'00000000000000000000' ||
					ds_brancos_8_w;
						
	utl_file.put_line(arq_texto_w,ds_conteudo_w);
	utl_file.fflush(arq_texto_w);
	/*	Fim - Segmento Q	*/
	end;
end loop;
close C01;

/*	Fim - Segmento P, Segmento Q, Segmento R	 */

/*	Trailer de Lote	*/
select	lpad(count(1),6,'0'),
	replace(to_char(sum(c.vl_titulo), 'fm0000000000000.00'),'.','')
into	qt_titulos_cobr_w,
	vl_titulos_cobr_w
from	titulo_receber		c,
	titulo_receber_cobr	b,
	cobranca_escritural 	a
where	b.nr_titulo	= c.nr_titulo
and	a.nr_sequencia	= b.nr_seq_cobranca
and	a.nr_sequencia	= nr_seq_cobr_escrit_p;

qt_registro_lote_w	:= qt_registro_lote_w + 1;
ds_conteudo_w		:=	'104' ||
				'0001' || 
				'5' ||
				ds_brancos_9_w ||
				lpad(qt_registro_w,6,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				ds_brancos_148_w;

qt_registro_w		:= qt_registro_w + 1;				
				
utl_file.put_line(arq_texto_w,ds_conteudo_w);
utl_file.fflush(arq_texto_w);
/*	Fim - Trailer de Lote	*/

/*	Trailer do Arquivo	*/
qt_registro_w	:= qt_registro_w + 1;
ds_conteudo_w	:=	'104' ||
			'9999' ||
			'9' ||
			ds_brancos_9_w ||
			'000001' ||
			lpad(qt_registro_w,6,'0') ||
			ds_brancos_211_w;
			
			
utl_file.put_line(arq_texto_w,ds_conteudo_w);
utl_file.fflush(arq_texto_w);
/*	Fim - Trailer do Arquivo	*/

end gerar_remessa_caixa_cnab_240;
/