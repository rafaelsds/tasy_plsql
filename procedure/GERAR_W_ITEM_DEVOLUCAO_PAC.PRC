create or replace
procedure gerar_w_item_devolucao_pac(
			nr_devolucao_p		number,
			cd_material_p		number,
			ie_aborta_p			varchar2,
			ds_titulo_p			varchar2,
			ds_observacao_p		varchar2,
			ie_etapa_p			varchar2,
			nm_usuario_p		Varchar2) is 

begin

if	(nr_devolucao_p is not null) then
	
	insert into w_item_devolucao_pac(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_devolucao,
		ds_observacao,
		ie_aborta,
		ds_titulo,
		cd_material)
	values(
		w_item_devolucao_pac_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_devolucao_p,
		ds_observacao_p,
		ie_aborta_p,
		ds_titulo_p,
		cd_material_p);

end if;

commit;

end gerar_w_item_devolucao_pac;
/
