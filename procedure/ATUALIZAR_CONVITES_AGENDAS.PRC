create or replace
procedure atualizar_convites_agendas(
		ds_lista_sequencias_p	varchar2,
		nm_usuario_p		varchar2) is

nr_sequencia_w		number(10,0);
ds_lista_w		varchar2(1000);
ie_pos_virgula_w	number(3,0);
nr_seq_agenda_w		number(10,0);
dt_agenda_w		date;
ie_confirmado_w		varchar2(1);
ds_motivo_ausencia_w	varchar2(255);

begin

if	(ds_lista_sequencias_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ds_lista_w	:= ds_lista_sequencias_p;
	
	while	(ds_lista_w is not null) loop
		begin
		
		ie_pos_virgula_w 	:= instr(ds_lista_w,',');
		
		if	(ie_pos_virgula_w <> 0) then
			begin
			nr_sequencia_w		:= to_number(substr(ds_lista_w,1,(ie_pos_virgula_w - 1)));
			ds_lista_w		:= substr(ds_lista_w,(ie_pos_virgula_w + 1),length(ds_lista_w));
			end;
		else
			begin
			nr_sequencia_w		:= to_number(ds_lista_w);
			ds_lista_w		:= null;
			end;
		end if;
		
		if	(nr_sequencia_w is not null) then
			begin
			select	b.nr_seq_agenda,
				b.ie_confirmado,
				a.dt_agenda,
				b.ds_motivo_ausencia
			into	nr_seq_agenda_w,
				ie_confirmado_w,
				dt_agenda_w,
				ds_motivo_ausencia_w
			from	agenda_tasy_convite b,
				agenda_tasy a
			where	a.nr_sequencia = b.nr_seq_agenda
			and	b.nr_sequencia = nr_sequencia_w;
		
			if	(nr_seq_agenda_w is not null) and
				(ie_confirmado_w is not null) and
				(dt_agenda_w is not null) then
				begin
				atualizar_agenda_tasy_convite(
					nr_seq_agenda_w,
					dt_agenda_w,
					ie_confirmado_w,
					ds_motivo_ausencia_w,
					nm_usuario_p);
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;
end atualizar_convites_agendas;
/