create or replace
procedure atualizar_data_impressao(dt_atual_p date,
				nr_sequencia_p	number,
				nm_usuario_p	Varchar2) is 

begin

if	(dt_atual_p is not null) and
	(nr_sequencia_p is not null) then
	update	can_ordem_prod
	set 	dt_impressao =	dt_atual_p
	where 	nr_sequencia =	nr_sequencia_p;
	commit;
end if;
end atualizar_data_impressao;
/