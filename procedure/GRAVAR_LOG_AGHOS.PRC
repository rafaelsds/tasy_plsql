create or replace
procedure Gravar_Log_Aghos( nr_sequencia_p		number,
							ie_situacao_p		varchar2) is 
			
nr_seq_log_w		number(10);
nm_usuario_w		varchar2(15);

begin
begin
	
	select	aghos_log_situacao_seq.nextval
	into	nr_seq_log_w
	from	dual;

	begin
		nm_usuario_w := nvl(wheb_usuario_pck.get_nm_usuario, 'Aghos');
	exception
		when	others then
			nm_usuario_w := 'Aghos';
	end;

	insert into AGHOS_LOG_SITUACAO (
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		nr_seq_solicitacao)
	values (nr_seq_log_w,
		sysdate,
		nm_usuario_w,
		sysdate,
		nm_usuario_w,
		ie_situacao_p,
		nr_sequencia_p);
		
exception
	when	others then
		null;
end;

commit;

end Gravar_Log_Aghos;
/