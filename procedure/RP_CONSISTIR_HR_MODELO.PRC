create or replace
procedure rp_consistir_hr_modelo(hr_consistencia_p	date,
			      nr_seq_pac_reab_p		number,
			      nr_seq_modelo_p		number,
			      ie_dia_semana_p		number,
			      ie_tipo_p			varchar2,
			      nm_usuario_p			varchar2) is 

qt_consistencia_w	number(10,0);
qt_modelo_exec_w	number(10,0);	
ie_nao_permite_iguais_w Varchar2(1);		      
			      
begin

	/* 
	Tipo
	
	CM - Cadastro Modelo
	MI - Modelo Individual 
	IM - Inser��o de Modelo
	
	*/
	
Obter_Param_Usuario(9091,48,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_nao_permite_iguais_w);	

if	(ie_tipo_p = 'CM') and (ie_nao_permite_iguais_w = 'S') then
	
	select	count(*)
	into	qt_consistencia_w
	from	rp_modelo_agendamento a,
		rp_item_modelo_agenda b
	where	b.nr_seq_modelo	= a.nr_sequencia
	and	to_char(b.hr_horario,'hh24:mi') = to_char(hr_consistencia_p,'hh24:mi')
	and	b.ie_dia_semana	= ie_dia_semana_p
	and	a.nr_sequencia 	= nr_seq_modelo_p
	and	nvl(b.ie_situacao,'A') = 'A';
	
	
	if	(qt_consistencia_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(225186);
	end if;
	
elsif	(ie_tipo_p = 'MI') and (ie_nao_permite_iguais_w = 'S') then

	select	count(*)
	into	qt_consistencia_w
	from	rp_pac_agend_individual
	where	nr_seq_pac_reab = nr_seq_pac_reab_p
	and	to_char(dt_horario,'hh24:mi') 	= to_char(hr_consistencia_p,'hh24:mi')
	and	ie_dia_semana			= ie_dia_semana_p
	and	dt_fim_tratamento is null;
	
	select	count(*)
	into	qt_modelo_exec_w
	from	rp_pac_modelo_agendamento a,
		rp_pac_modelo_agend_item b
	where	b.nr_seq_modelo_pac		= a.nr_sequencia
	and	to_char(b.dt_horario,'hh24:mi') = to_char(hr_consistencia_p,'hh24:mi')
	and	a.nr_seq_pac_reab		= nr_seq_pac_reab_p
	and	b.ie_dia_semana			= ie_dia_semana_p
	and	b.dt_fim_tratamento is null
	and	a.dt_inativacao is null;
	
	if	(qt_consistencia_w > 0) or (qt_modelo_exec_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(225188);
	end if;

elsif	(ie_tipo_p = 'IM') and (ie_nao_permite_iguais_w = 'S') then
	
	select	count(*)
	into	qt_consistencia_w
	from	rp_pac_agend_individual
	where	nr_seq_pac_reab = nr_seq_pac_reab_p
	and	to_char(dt_horario,'hh24:mi') 	= to_char(hr_consistencia_p,'hh24:mi')
	and	ie_dia_semana			= ie_dia_semana_p
	and	dt_fim_tratamento is null;
	
	if	(qt_consistencia_w > 0) or (qt_modelo_exec_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(225189);
	end if;
				
end if;
	
commit;

end rp_consistir_hr_modelo;
/
