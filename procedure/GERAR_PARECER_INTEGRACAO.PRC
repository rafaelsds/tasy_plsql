create or replace
procedure Gerar_Parecer_Integracao(
			nr_cpf_p	varchar2 default null,
			cd_nacionalidade_p	varchar2 default null,
			dt_nascimento_p	varchar2 default null,
			ie_sexo_p	varchar2 default null,
			nm_pessoa_fisica_p	varchar2 default null,
			nm_mae_p	varchar2 default null,
			cd_cnpj_p	varchar2 default null,
			ie_tipo_p	varchar2 default null,
			ie_status_p	varchar2 default null,
			nr_seq_regulacao_p	varchar2 default null,
			ds_parecer_p	varchar2 default null,
			qt_solicitado_p	varchar2 default null,
			ie_origem_proced_p	varchar2 default null,
			cd_medico_dest_p	varchar2 default null,
			cd_especialidade_p	varchar2 default null,
			ds_cod_prof_person_med_p	varchar2 default null, --medico destino
			nr_seq_cons_person_med_p	varchar2 default null, --medico destino
			nr_cpf_person_med_p	varchar2 default null, --medico destino
			nm_pessoa_fisica_med_p	varchar2 default null,  --medico destino
			cd_pessoa_fisica_med_p	varchar2 default null, --medico destino
			ds_codigo_prof_prof_p	varchar2 default null, --profissional solicitante
			nr_seq_cons_prof_p	varchar2 default null, --profissional solicitante
			nr_cpf_prof_p	varchar2 default null, --profissional solicitante
			nm_pessoa_fisica_prof_p	varchar2 default null, --profissional solicitante
			cd_pessoa_fisica_prof_p	varchar2 default null, --profissional solicitante
			nr_seq_cbo_saude_p	varchar2 default null,
			ds_especialidade_p	varchar2 default null,
			cd_ptu_p	varchar2 default null,
			cd_especialidade_ext_p	varchar2 default null,
			cd_ptu_ori_p	varchar2 default null,
			nr_seq_paracer_origem_p	varchar2 default null,
			nm_usuario_p	varchar2 default null) is 

			
cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_fisica_med_dest_w		varchar2(10);
nr_seq_parecer_w			number(10);
cd_especialidade_dest_w		number(5);
cd_especialidade_ori_w		number(5);
nr_atendimento_w			number(10);

cd_pessoa_fisica_prof_w  varchar2(10);

nm_usuario_w		  varchar2(15);			

begin

Select  max('T.I.E') -- Integração Bifrost
into	nm_usuario_w
from 	dual;


select	max(cd_pessoa_fisica)
into 	cd_pessoa_fisica_w		
from	pessoa_fisica
where	nr_cpf = nr_cpf_p;

if( cd_pessoa_fisica_w is null)then

select	max(cd_pessoa_fisica)
into 	cd_pessoa_fisica_w		
from	pessoa_fisica
where	dt_nascimento = dt_nascimento_p
and		ie_sexo	= ie_sexo_p
and 	UPPER(nm_pessoa_fisica) = UPPER(nm_pessoa_fisica_p)
and 	UPPER(obter_nome_mae_pf(cd_pessoa_fisica)) like UPPER(nm_mae_p);

	
	if( cd_pessoa_fisica_w is null)then

		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;
		

		insert into pessoa_fisica(
			cd_pessoa_fisica,
			ie_tipo_pessoa,
			nm_pessoa_fisica,
			nr_cpf,
			ie_sexo,
			dt_nascimento,
			dt_atualizacao,
			nm_usuario)
		values(	to_char( cd_pessoa_fisica_w ),
			1,
			nm_pessoa_fisica_p,
			nr_cpf_p,
			ie_sexo_p,
			dt_nascimento_p,
			sysdate,
			nm_usuario_w);
			
			commit;
	end if;
end if;


--Medico Destino


select	max(cd_pessoa_fisica)
into 	cd_pessoa_fisica_med_dest_w		
from	pessoa_fisica
where	nr_cpf = nr_cpf_person_med_p;


if( cd_pessoa_fisica_med_dest_w is null)then

	select	max(cd_pessoa_fisica)
	into 	cd_pessoa_fisica_med_dest_w	
	from	pessoa_fisica
	where	ds_codigo_prof = ds_cod_prof_person_med_p
	and		nr_seq_conselho	= nr_seq_cons_person_med_p;
	

end if;

--Medico  Origem
select	max(cd_pessoa_fisica)
into 	cd_pessoa_fisica_prof_w		
from	pessoa_fisica
where	nr_cpf = nr_cpf_prof_p;


if( cd_pessoa_fisica_prof_w is null)then

	select	max(cd_pessoa_fisica)
	into 	cd_pessoa_fisica_prof_w	
	from	pessoa_fisica
	where	ds_codigo_prof = ds_codigo_prof_prof_p
	and		nr_seq_conselho	= nr_seq_cons_prof_p;
	

end if;

-- Especialidade destino
select	max(cd_especialidade)
into	cd_especialidade_dest_w 
from	especialidade_medica
where	cd_ptu = cd_ptu_p; 

-- Especialidade  origem
select	max(cd_especialidade)
into	cd_especialidade_ori_w 
from	especialidade_medica
where	cd_ptu = cd_ptu_ori_p;  

if(( cd_pessoa_fisica_med_dest_w is not null or cd_especialidade_dest_w is not null) and cd_pessoa_fisica_w is not null )then

	select	parecer_medico_req_seq.nextval
	into	nr_seq_parecer_w
	from	dual;
	
	

	insert into parecer_medico_req (
						nr_parecer,
						cd_medico,
						cd_pessoa_fisica,
						cd_especialidade_dest,
						cd_pessoa_parecer,
						ie_integracao,
						ie_situacao,
						nm_usuario_nrec,
						nm_usuario,
						dt_atualizacao_nrec,
						dt_atualizacao,
						nr_seq_regulacao_ori,
						cd_cgc,
						ds_motivo_consulta,
						dt_liberacao,
						cd_especialidade,
						nr_seq_origem
					) values (
						nr_seq_parecer_w,
						nvl(cd_pessoa_fisica_prof_w, cd_pessoa_fisica_prof_p),
						cd_pessoa_fisica_w,
						cd_especialidade_dest_w,
						cd_pessoa_fisica_med_dest_w,
						'S',
						'A',
						nm_usuario_w,
						nm_usuario_w,
						sysdate,
						sysdate,
						nr_seq_regulacao_p,
						cd_cnpj_p,
						ds_parecer_p,
						sysdate,
						cd_especialidade_ori_w,
						nr_seq_paracer_origem_p
					);

		commit;

		inserir_worklist_integracao(cd_especialidade_dest_w,cd_pessoa_fisica_med_dest_w,cd_pessoa_fisica_w);
		
end if;

end Gerar_Parecer_Integracao;
/