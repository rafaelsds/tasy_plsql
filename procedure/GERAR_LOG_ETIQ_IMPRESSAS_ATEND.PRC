create or replace
procedure gerar_log_etiq_impressas_atend(	nr_atendimento_p number,
					qt_etiqueta_p number,
					nm_usuario_p varchar2)
is 	
ie_existe_atendimento_w  	number(10,1) 	:= 0;		
nr_etiqueta_w		 number(10,1) 	:= 0;
nr_sequencia_w		 number(15,1) 	:= 0;
begin
	
	if ((nr_atendimento_p is not null) and (nm_usuario_p is not null)) then	
	select 	count(*)
	into	ie_existe_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;

	if ie_existe_atendimento_w > 0 then
	begin
	select 	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from   	etiqueta_controle_atend;
	
	select 	nvl(max(nr_etiqueta),0)
	into	nr_etiqueta_w
	from	etiqueta_controle_atend
	where	nr_atendimento = nr_atendimento_p;
	end;	
					
	for i in 1..qt_etiqueta_p
	loop
		begin
		nr_etiqueta_w := nr_etiqueta_w + 1;
		nr_sequencia_w := nr_sequencia_w +1;	
		insert into etiqueta_controle_atend values (	nr_sequencia_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_etiqueta_w,
							nr_atendimento_p);
		end;
	end loop;	
	end if;
	end if;
	commit;	
end ;

/