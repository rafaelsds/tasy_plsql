create or replace
procedure san_doacao_triagem_fis_bp_js(	cd_pessoa_fisica_p	Varchar2,
				cd_estabelecimento_p	Varchar2,
				nm_usuario_p		Varchar2,
				pr_hemat_min_fem_p out	Number,
				pr_hemat_max_fem_p out	Number,
				pr_hemat_min_mas_p out  Number,
				pr_hemat_max_mas_p out	Number,
				qt_hemoglobina_min_fem_p out Number,
				qt_hemoglobina_max_fem_p out Number,
				qt_hemoglobina_min_mas_p out Number,
				qt_hemoglobina_max_mas_p out Number,
				ie_sexo_p out Varchar2) is 

begin

select 	max(ie_sexo)
into	ie_sexo_p 
from 	pessoa_fisica 
where 	cd_pessoa_fisica = cd_pessoa_fisica_p;

select 	max(pr_hemat_min_fem) pr_hemat_min_fem, 
	max(pr_hemat_max_fem) pr_hemat_max_fem,
	max(pr_hemat_min_mas) pr_hemat_min_mas,
	max(pr_hemat_max_mas) pr_hemat_max_mas
into	pr_hemat_min_fem_p,
	pr_hemat_max_fem_p,
	pr_hemat_min_mas_p,
	pr_hemat_max_mas_p
from   	san_parametro 
where  	cd_estabelecimento = cd_estabelecimento_p;

select 	max(qt_hemoglobina_min_fem) qt_hemoglobina_min_fem,
	max(qt_hemoglobina_max_fem) qt_hemoglobina_max_fem, 
	max(qt_hemoglobina_min_mas) qt_hemoglobina_min_mas, 
	max(qt_hemoglobina_max_mas) qt_hemoglobina_max_mas
into	qt_hemoglobina_min_fem_p,
	qt_hemoglobina_max_fem_p,
	qt_hemoglobina_min_mas_p,
	qt_hemoglobina_max_mas_p
from   san_parametro 
where  cd_estabelecimento = cd_estabelecimento_p;

end san_doacao_triagem_fis_bp_js;
/