create or replace PROCEDURE UPDATE_MALIGNANT_TUMOR
IS  
CURSOR c01 IS
    select CD_ICD from ICD_MALIGNANT_TUMOR_JPN WHERE IE_SITUACAO='A';
    r01_w   c01%rowtype;

BEGIN 

        OPEN C01;
        LOOP
            FETCH c01 INTO r01_w;
            EXIT WHEN C01%notfound;
            BEGIN

            UPDATE 	CID_DOENCA
            SET	SI_MALIGNANT_TUMOR = 'S'
            WHERE	CD_DOENCA_CID = r01_w.CD_ICD;
            END;
        END LOOP;

    COMMIT;
END UPDATE_MALIGNANT_TUMOR;
/
