create or replace
procedure pls_gerar_grupos_contr_interc
			(	nr_seq_intercambio_p	number,
				ie_tipo_repasse_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_intercambio_w		number(10);
nr_seq_congenere_w		number(10);
nr_seq_oper_congenere_w		number(10);
nr_seq_regra_grupo_w		number(10);
cd_operadora_empresa_w		number(10);
cd_cgc_empresa_w		varchar2(14);
				
Cursor C01 is
	select	b.nr_sequencia
	from	pls_regra_grupo_inter	b,
		pls_regra_benef_grupo	a
	where	a.nr_seq_regra		= b.nr_sequencia
	and	b.ie_situacao		= 'A'
	and	((a.nr_seq_cooperativa	= nr_seq_congenere_w and a.nr_seq_cooperativa is not null) or (a.nr_seq_cooperativa is null))
	and	((a.nr_seq_congenere	= nr_seq_oper_congenere_w and a.nr_seq_congenere is not null) or (a.nr_seq_congenere is null))
	and	((a.cd_cgc_empresa	= cd_cgc_empresa_w and a.cd_cgc_empresa is not null) or (a.cd_cgc_empresa is null))
	and	((a.ie_tipo_repasse	= ie_tipo_repasse_p and a.ie_tipo_repasse is not null) or (a.ie_tipo_repasse is null))
	and	((a.cd_operadora_empresa = cd_operadora_empresa_w and a.cd_operadora_empresa is not null) or (a.cd_operadora_empresa is null))
	order by nvl(a.nr_seq_cooperativa,0),
		nvl(a.nr_seq_congenere,0),
		nvl(a.ie_tipo_repasse,0),
		nvl(a.cd_cgc_empresa,0),
		nvl(a.cd_operadora_empresa,0);
			
begin

select	nr_seq_congenere,
	nr_seq_oper_congenere,
	cd_cgc,
	CD_OPERADORA_EMPRESA
into	nr_seq_congenere_w,
	nr_seq_oper_congenere_w,
	cd_cgc_empresa_w,
	cd_operadora_empresa_w
from	pls_intercambio
where	nr_sequencia	= nr_seq_intercambio_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_grupo_w;
exit when C01%notfound;
end loop;
close C01;

if	(nr_seq_regra_grupo_w is not null) then
	update	PLS_INTERCAMBIO
	set	NR_SEQ_GRUPO_INTERCAMBIO	= nr_seq_regra_grupo_w
	where	nr_sequencia			= nr_seq_intercambio_p;
end if;	

end pls_gerar_grupos_contr_interc;
/