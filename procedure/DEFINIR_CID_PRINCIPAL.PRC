create or replace
procedure Definir_CID_principal
		(nr_atendimento_p		Number,
		dt_diagnostico_p		Date,
		cd_doenca_p			varchar2,
		nm_usuario_p			varchar2) is

begin
update	diagnostico_doenca
set	ie_classificacao_doenca	= 'P',
	dt_atualizacao		= sysdate, 
	nm_usuario		= nm_usuario_p 
where	nr_atendimento		= nr_atendimento_p 
and	dt_diagnostico		= dt_diagnostico_p
and	cd_doenca 		= cd_doenca_p;

commit;
end Definir_CID_principal;
/