create or replace
procedure devolver_mat_barras_atender(		nr_prescricao_p			number,
						nr_cirurgia_p			number,
						cd_material_p			number,
						qt_saldo_p			number,
						nr_seq_lote_fornec_p		number,
						cd_local_estoque_p		number,
						nm_usuario_p			varchar2)
						is
						
cd_material_w			number(6);
cd_kit_material_w		number(6);
nr_seq_kit_estoque_w		number(10);
nr_seq_interno_w		number(10);	
cd_motivo_baixa_w		number(3);
cd_motivo_baixa_original_w	number(3);
					
						
cursor	c01 is
	select	cd_material,
		cd_kit_material,
		nr_seq_kit_estoque,
		nr_seq_interno
	from	prescr_material
	where	nr_prescricao 	= nr_prescricao_p
	and	dt_baixa is null
	and	cd_motivo_baixa = 0
	and	qt_material > 0;


begin
obter_param_usuario(36, 21, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, 0, cd_motivo_baixa_w);

cd_motivo_baixa_original_w	:= cd_motivo_baixa_w; 

update	prescr_material
set	qt_material 		= nvl(qt_atendido,0),
	qt_total_dispensar	= nvl(qt_atendido,0),
	qt_dose			= nvl(qt_atendido,0),
	qt_unitaria		= nvl(qt_atendido,0)
where	nr_prescricao		= nr_prescricao_p
and	cd_motivo_baixa		= 0
and	dt_baixa is null;

open C01;
loop
fetch C01 into	
	cd_material_w,
	cd_kit_material_w,
	nr_seq_kit_estoque_w,
	nr_seq_interno_w;
exit when C01%notfound;	
	if	(obter_se_mat_baixa_estoque_cir(cd_material_w, cd_local_estoque_p, nr_cirurgia_p,cd_kit_material_w,nr_seq_kit_estoque_w,nr_seq_interno_w) is not null) then
		cd_motivo_baixa_w := obter_se_mat_baixa_estoque_cir(cd_material_w, cd_local_estoque_p, nr_cirurgia_p,cd_kit_material_w,nr_seq_kit_estoque_w,nr_seq_interno_w);
	else
		cd_motivo_baixa_w := cd_motivo_baixa_original_w;
	end if;
	
	update	prescr_material
	set	dt_baixa 		= sysdate,
		cd_motivo_baixa 	= cd_motivo_baixa_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		cd_local_estoque	= cd_local_estoque_p
	where	nr_seq_interno		= nr_seq_interno_w;
	commit;
end loop;
close C01;

commit;

end devolver_mat_barras_atender;
/