create or replace
procedure liberar_hist_doc_imp_oc(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

begin

update	ordem_doc_imp_historico
set	dt_liberacao	= sysdate,
	nm_usuario_lib	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

commit;

end liberar_hist_doc_imp_oc;
/
