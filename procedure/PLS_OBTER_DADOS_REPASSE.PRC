create or replace
procedure pls_obter_dados_repasse
			(	dt_referencia_p					date,
				nr_seq_segurado_p				number,
				nr_seq_congenere_p				number,
				ie_tipo_repasse_p		out nocopy	varchar2,
				ie_tipo_compartilhamento_p	out		number,
				dt_repasse_p			out		date,
				dt_fim_repasse_p		out		date) is 

/* 	Utilizada nas rotinas de atualização das contas contábeis e registros auxiliares, 
	para obter os dados de compartilhamento de beneficiários				*/

nr_seq_repasse_w		pls_segurado_repasse.nr_sequencia%type;

begin

select 	max(a.nr_sequencia)
into	nr_seq_repasse_w
from	pls_segurado_repasse a
where	a.nr_seq_segurado	= nr_seq_segurado_p
and	dt_referencia_p between a.dt_repasse and nvl(a.dt_fim_repasse,dt_referencia_p)
and	nvl(a.nr_seq_congenere_atend,0) = nvl(nr_seq_congenere_p,0);

if	(nr_seq_repasse_w is not null) then
	select 	ie_tipo_compartilhamento,
		ie_tipo_repasse,
		dt_repasse,
		dt_fim_repasse
	into	ie_tipo_compartilhamento_p,
		ie_tipo_repasse_p,
		dt_repasse_p,
		dt_fim_repasse_p
	from 	pls_segurado_repasse
	where	nr_sequencia = nr_seq_repasse_w;
end if;

end pls_obter_dados_repasse;
/