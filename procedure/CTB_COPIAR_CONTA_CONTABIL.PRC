create or replace procedure ctb_copiar_conta_contabil(	cd_empresa_origem_p  number,
							cd_empresa_destino_p  number,
							nm_usuario_p    Varchar2) is

	/*Variaveis de controle*/
	cd_conta_contabil_w      varchar2(20);
	cd_conta_referencia_w      varchar2(20);
	cd_conta_contabil_new_w      varchar2(20);
	ds_critica_w        varchar2(2000);
	nr_sequencia_w        number(10);

	/*Cursor com dados filtrados */
	cursor c1 is
	select	a.cd_conta_contabil,
		a.ds_conta_contabil,
		a.ie_situacao,
		sysdate dt_atualizacao,
		nm_usuario_p nm_usuario,
		a.ie_centro_custo,
		a.ie_tipo,
		ctb_obter_classif_conta(a.cd_conta_contabil,a.cd_classificacao,null)	cd_classificacao,
		(	select	max(aa.cd_grupo)
			from	ctb_grupo_conta aa
			where	aa.cd_grupo_ref	= a.cd_grupo
			and	aa.cd_empresa	= cd_empresa_destino_p)	cd_grupo,
		cd_empresa_destino_p cd_empresa,
		(	select	max(bb.cd_centro_custo)
			from	centro_custo bb
			where	bb.cd_centro_custo_ref  =  a.cd_centro_custo
			and	bb.ie_situacao = 'A')	cd_centro_custo,
		a.cd_sistema_contabil,
		a.cd_plano_ans,
		a.ds_orientacao,
		a.ie_compensacao,
		a.ie_versao_ans,
		a.dt_inicio_vigencia,
		a.dt_fim_vigencia,
		ctb_obter_classif_conta_sup(ctb_obter_classif_conta(a.cd_conta_contabil,a.cd_classificacao,null),sysdate,cd_empresa_origem_p)	cd_classif_superior,
		a.cd_classif_ecd,
		sysdate dt_atualizacao_nrec,
		nm_usuario_p nm_usuario_nrec,
		a.ie_ecd_reg_dre,
		a.ie_ecd_reg_bp,
		a.ie_diops,
		ctb_obter_classif_conta(a.cd_conta_contabil,a.cd_classificacao,sysdate) cd_classificacao_atual,
		ctb_obter_classif_conta_sup(ctb_obter_classif_conta(a.cd_conta_contabil,a.cd_classificacao,null),sysdate,cd_empresa_origem_p)   cd_classif_superior_atual,
		a.ie_natureza_sped,
		a.ie_deducao_acomp_orc,
		a.cd_conta_referencia
	from	conta_contabil a
	where	a.cd_empresa	=	cd_empresa_origem_p;

	r1 c1%rowtype;

	function gera_pk_valida return varchar2 is
		cd_conta_contabil_pk_w varchar2(20);
	begin

		nr_sequencia_w := nr_sequencia_w + 1;

		cd_conta_contabil_pk_w := substr(cd_empresa_destino_p || '.' || nr_sequencia_w,1,20);

		select	max(cd_conta_contabil)
		into	cd_conta_contabil_new_w
		from	conta_contabil
		where	cd_conta_contabil	= cd_conta_contabil_pk_w;

		/* Se achou PK na conta contabil, chama function novamente */
		if (cd_conta_contabil_new_w is not null) then
			/* recusividade */
			cd_conta_contabil_pk_w := gera_pk_valida();
		end if;

	return(cd_conta_contabil_pk_w);

	end gera_pk_valida;
begin

/* Inicia sequenciador interno para conten��o de duplicidades de PK*/
nr_sequencia_w := 0;

/* Chamar a copida do grupo conta_contabil , pois � referenciada na conta contabil*/
ctb_copiar_ctb_grupo_conta(	cd_empresa_origem_p,
				cd_empresa_destino_p,
				nm_usuario_p);

open c1;
loop
fetch c1 into r1;
exit when c1%notfound;
	/* Valida conta ja referenciada */
	begin
	select	max(cd_conta_contabil)
	into	cd_conta_referencia_w
	from	conta_contabil
	where	cd_conta_referencia  =  r1.cd_conta_contabil
	and	cd_empresa    =  cd_empresa_destino_p;
	end;

	/* Evita copia duplicada */
	if (cd_conta_referencia_w is null) then

		if (length(cd_empresa_destino_p || '.' || r1.cd_conta_contabil) > 20 ) then
			cd_conta_contabil_w := substr(cd_empresa_destino_p || '.' || somente_numero(r1.cd_conta_contabil),1,20);
		else
			cd_conta_contabil_w := substr(cd_empresa_destino_p || '.' || r1.cd_conta_contabil,1,20);
		end if;

		select	max(cd_conta_contabil)
		into	cd_conta_contabil_new_w
		from	conta_contabil
		where	cd_conta_contabil = cd_conta_contabil_w;

		if (cd_conta_contabil_new_w is not null) then
			/* Chama fun��o recursiva para garantir PK da nova Conta Contabil*/
			cd_conta_contabil_w := gera_pk_valida();
		end if;

		insert into conta_contabil (	cd_conta_contabil,
						ds_conta_contabil,
						ie_situacao,
						dt_atualizacao,
						nm_usuario,
						ie_centro_custo,
						ie_tipo,
						cd_classificacao,
						cd_grupo,
						cd_empresa,
						cd_centro_custo,
						cd_sistema_contabil,
						cd_plano_ans,
						ds_orientacao,
						ie_compensacao,
						ie_versao_ans,
						dt_inicio_vigencia,
						dt_fim_vigencia,
						cd_classif_superior,
						cd_classif_ecd,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_ecd_reg_dre,
						ie_ecd_reg_bp,
						ie_diops,
						cd_classificacao_atual,
						cd_classif_superior_atual,
						ie_natureza_sped,
						ie_deducao_acomp_orc,
						cd_conta_referencia)
		values	(			cd_conta_contabil_w,
						r1.ds_conta_contabil,
						r1.ie_situacao,
						r1.dt_atualizacao,
						r1.nm_usuario,
						r1.ie_centro_custo,
						r1.ie_tipo,
						r1.cd_classificacao,
						r1.cd_grupo,
						r1.cd_empresa,
						r1.cd_centro_custo,
						r1.cd_sistema_contabil,
						r1.cd_plano_ans,
						r1.ds_orientacao,
						r1.ie_compensacao,
						r1.ie_versao_ans,
						r1.dt_inicio_vigencia,
						r1.dt_fim_vigencia,
						r1.cd_classif_superior,
						r1.cd_classif_ecd,
						r1.dt_atualizacao_nrec,
						r1.nm_usuario_nrec,
						r1.ie_ecd_reg_dre,
						r1.ie_ecd_reg_bp,
						r1.ie_diops,
						r1.cd_classificacao_atual,
						r1.cd_classif_superior_atual,
						r1.ie_natureza_sped,
						r1.ie_deducao_acomp_orc,
						r1.cd_conta_contabil);
	end if;
	
	end loop;
	close c1;

	commit;

end ctb_copiar_conta_contabil;
/
