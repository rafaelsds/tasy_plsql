CREATE OR REPLACE PROCEDURE insert_diagnosis_icd_barthel (
    nr_atendimento_p        NUMBER,
    cd_doenca_p             VARCHAR2,
    cd_doenca_superior_p    VARCHAR2 default null,
    cd_pessoa_fisica_p      VARCHAR2,
    ds_classifications_p    VARCHAR2,
    nr_cirurgia_p           NUMBER default null,
    ie_lado_p               VARCHAR2 default null,
    ie_classificacao_doenca_p VARCHAR2 default null,
    ie_tipo_diagnostico_p     VARCHAR2 default null,
    dt_diagnostico_p        OUT DATE)IS

BEGIN

    -- Insere o diagnóstico
    gerar_diagnostico_atend(
        nr_atendimento_p,
        cd_doenca_p,        
        cd_pessoa_fisica_p,
        Wheb_usuario_pck.get_nm_usuario());
        
    COMMIT;
    
    -- Guarda a data do diagnóstico
    select max(dt_diagnostico) 
    into dt_diagnostico_p
    from	diagnostico_medico
    where	pkg_date_utils.start_of(dt_diagnostico,'DD',0)	= pkg_date_utils.start_of(sysdate,'DD',0)
        and		nr_atendimento		= nr_atendimento_p
        and		cd_medico		    = cd_pessoa_fisica_p;
                            
    -- Atualiza o diagnóstico que acabou de ser inserido
    update  diagnostico_doenca
    set     cd_doenca_superior  = cd_doenca_superior_p,
            nr_cirurgia         = nr_cirurgia_p,
            ie_lado             = ie_lado_p,
            ie_classificacao_doenca = ie_classificacao_doenca_p,
            ie_tipo_diagnostico = ie_tipo_diagnostico_p,
            dt_liberacao		= sysdate
    where   dt_diagnostico      = dt_diagnostico_p
    and     cd_doenca           = cd_doenca_p;
    COMMIT;
    
    -- Insere as classificações do diagnóstico
    if(ds_classifications_p is not null)then
        inserir_classif_diagnostico(
            ds_classifications_p, 
            wheb_usuario_pck.get_cd_estabelecimento(), 
            Wheb_usuario_pck.get_nm_usuario(), 
            nr_atendimento_p, 
            cd_pessoa_fisica_p, 
            cd_doenca_p, 
            dt_diagnostico_p);
    end if;
    
    COMMIT;
END insert_diagnosis_icd_barthel;
/