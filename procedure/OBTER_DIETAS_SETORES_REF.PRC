create or replace
procedure obter_dietas_setores_ref(
		cd_setor_p		number,
		cd_dominio_p		number,
		qt_hora_p		number,
		dt_dieta_p		date,
		nm_usuario_p		varchar2) is 

cd_refeicao_w		varchar(15);

Cursor C01 is
	select	vl_dominio cd_refeicao
	from	valor_dominio 
	where	cd_dominio = cd_dominio_p;

begin

open C01;
loop
fetch C01 into	
	cd_refeicao_w;
	exit when C01%notfound;
	begin
	obter_dieta_prescricao( cd_setor_p,
				qt_hora_p, 
				dt_dieta_p, 
				cd_refeicao_w,
				nm_usuario_p);
	end;
end loop;
close C01;

end  obter_dietas_setores_ref;
/