CREATE OR REPLACE
PROCEDURE cancela_ordem_nova_cotacao(	nr_ordem_compra_p	number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					dt_entrega_p		date,
					nr_seq_motivo_cancel_p	number,
					ie_cancel_apenas_pend_p	varchar2,
					nr_cot_compra_p	out	number) is

cd_pessoa_solicitante_w		varchar2(10);
cd_comprador_w			varchar2(10);
qt_dias_param_w			number(5);
nr_item_w			number(5);
cd_material_w			number(6);
cd_unidade_medida_compra_w	varchar2(30);
qt_material_w			number(13,4);
nr_cot_compra_w			number(10);
ds_retorno_w			varchar2(255);
nr_solic_compra_w			number(10);
nr_item_solic_compra_w		number(05);

cursor C01 is
select	nr_item_oci,
	cd_material,
	cd_unidade_medida_compra,
	nvl(qt_material,0) - nvl(qt_material_entregue,0) qt_material,
	nr_solic_compra,
	nr_item_solic_compra	
from	ordem_compra_item
where	nr_ordem_compra	= nr_ordem_compra_p
and	nvl(qt_material,0) > nvl(qt_material_entregue,0);


BEGIN

Obter_Param_Usuario(915, 2, cd_perfil_p, nm_usuario_p, 1, qt_dias_param_w);

select	cd_pessoa_solicitante,
	cd_comprador
into	cd_pessoa_solicitante_w,
	cd_comprador_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

select	cot_compra_seq.nextval
into	nr_cot_compra_w
from	dual;

nr_cot_compra_p := nr_cot_compra_w;

insert into cot_compra(
	nr_cot_compra,
	dt_cot_compra,
	dt_atualizacao,
	cd_comprador,
	nm_usuario,
	ds_observacao,
	cd_pessoa_solicitante,
	cd_estabelecimento,
	dt_geracao_ordem_compra,
	dt_retorno_prev,
	ie_finalidade_cotacao)
values(	nr_cot_compra_w,
	sysdate,
	sysdate,
	cd_comprador_w,
	nm_usuario_p,
	'',
	cd_pessoa_solicitante_w,
	cd_estabelecimento_p,
	null,
	sysdate + qt_dias_param_w,
	'C');


open c01;
loop
fetch c01 into
	nr_item_w,
	cd_material_w,
	cd_unidade_medida_compra_w,
	qt_material_w,
	nr_solic_compra_w,
	nr_item_solic_compra_w;	
Exit when c01%notfound;
	begin
	insert into cot_compra_item(
			nr_cot_compra,
			nr_item_cot_compra,
			cd_material,
			qt_material,
			cd_unidade_medida_compra,
			dt_atualizacao,
			dt_limite_entrega,
			nm_usuario,
			ie_situacao,
			nr_cot_venc_sis,
			nr_item_cot_venc_sis,
			cd_cgc_fornecedor_venc_sis,
			nr_cot_venc_alt,
			nr_item_cot_venc_alt,
			cd_cgc_fornecedor_venc_alt,
			ds_material_direto_w,
			nr_seq_cot_item_forn,
			cd_estab_item,
			nr_solic_compra,
			nr_item_solic_compra)
		values(	nr_cot_compra_w,
			nr_item_w,
			cd_material_w,
			qt_material_w,
			cd_unidade_medida_compra_w,
			sysdate,
			dt_entrega_p,
			nm_usuario_p,
			'A',
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			nr_solic_compra_w,
			nr_item_solic_compra_w);
			
	gerar_cot_compra_item_entrega(nr_cot_compra_w,nr_item_w,nm_usuario_p);
			
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(210870);
			/*(-20011,'erro ao gerar cot_compra_item');*/
	end;

if	(ie_cancel_apenas_pend_p = 'S') then
	cancelar_entregas_pendentes_oc(nr_ordem_compra_p,nm_usuario_p);
else
	cancelar_ordem_compra(nr_ordem_compra_p, nr_seq_motivo_cancel_p, 'N', 'N', 'N', nm_usuario_p,'','N',ds_retorno_w);
end if;

commit;
end loop;
close c01;

END cancela_ordem_nova_cotacao;
/