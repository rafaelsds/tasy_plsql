create or replace
procedure pep_atualiza_dt_fim_medic(	nr_sequencia_p		number,
					dt_fim_p		date,
					ds_observacao_p		varchar2,
					ie_nao_sabe_fim_p	varchar2,
					ie_atualiza_tudo_p	varchar2,
					nm_usuario_p		varchar2
					) is

begin

if	(ie_atualiza_tudo_p = 'S') then
	begin
	
	update   paciente_medic_uso       
	set      dt_fim         = dt_fim_p
        where    nr_sequencia   = nr_sequencia_p;
	
	update   paciente_medic_uso         
	set      ie_nao_sabe_fim = ie_nao_sabe_fim_p,   
                 ds_observacao   = substr(ds_observacao||decode(ds_observacao,null,null,chr(13)||chr(10))||ds_observacao_p,1,255)
        where    nr_sequencia    = nr_sequencia_p;	
	
	end;
else
	begin
	
	update   paciente_medic_uso         
	set      ie_nao_sabe_fim = ie_nao_sabe_fim_p,   
                 ds_observacao   = substr(ds_observacao||decode(ds_observacao,null,null,chr(13)||chr(10))||ds_observacao_p,1,255)
        where    nr_sequencia    = nr_sequencia_p;
	
	end;
end if;

commit;

end pep_atualiza_dt_fim_medic;
/