create or replace
procedure VINCULAR_ADIANT_ATEND(nr_interno_conta_p	in number,
				nr_adiantamento_p	in number)
				is
	
nr_adiantamento_w	number(10);	
	
begin

select	max(a.nr_atendimento)
into	nr_adiantamento_w
from	conta_paciente a
where	a.nr_interno_conta   = nr_interno_conta_p;

update	adiantamento
set     nr_atendimento    = nr_adiantamento_w
where   nr_adiantamento   = nr_adiantamento_p;

commit;

end VINCULAR_ADIANT_ATEND;
/

