create or replace
procedure envia_email_atraso_aprovacao(	nr_documento_p		varchar2,
				     	ie_tipo_documento_p	varchar2,
				    	ds_justificativa_p		varchar2,
					nm_usuario_p		Varchar2) is


nr_seq_regra_email_w		number(10);

ds_email_origem_w			varchar2(255);
ds_email_destino_w			varchar2(2000);
ds_email_adicional_w		varchar2(2000);
ds_email_comprador_w		varchar2(255);
ds_email_remetente_w		varchar2(255);

ds_tipo_documento_w		varchar2(255);
cd_perfil_disparar_w		number(10);

ie_usuario_w			varchar2(3);
ds_usuario_origem_w		varchar2(255);
ds_usuario_comprador_w		varchar2(255);

ds_assunto_w			varchar2(255);
ds_mensagem_w			varchar2(2000);

cd_estabelecimento_w		number(5);
cd_pessoa_solicitante_w		varchar2(10);
ds_cond_pagto_w			condicao_pagamento.ds_condicao_pagamento%type;

Cursor C01 is
	/*Procedimento de envio de email*/
	select	nr_sequencia,
		nvl(ds_email_remetente,'X'),
		replace(ds_email_adicional,',',';')
	from	regra_envio_email_compra
	where	ie_tipo_mensagem = 62
	and	ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_w;

begin

ds_cond_pagto_w := '';

if	(ie_tipo_documento_p = 'C') then

	select	cd_pessoa_solicitante,
		substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante), 'E'),1,2000) ds_email_destino,
		cd_estabelecimento
	into	cd_pessoa_solicitante_w,
		ds_email_destino_w,
		cd_estabelecimento_w
	from    	cot_compra
	where   	nr_cot_compra = nr_documento_p;

elsif	(ie_tipo_documento_p = 'S') then

	select	cd_pessoa_solicitante,
		substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante), 'E'),1,2000) ds_email_destino,
		cd_estabelecimento
	into	cd_pessoa_solicitante_w,
		ds_email_destino_w,
		cd_estabelecimento_w
	from    	solic_compra
	where   	nr_solic_compra = nr_documento_p;

elsif	(ie_tipo_documento_p = 'O') then

	select	cd_pessoa_solicitante,
		substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante), 'E'),1,2000) ds_email_destino,
		cd_estabelecimento,
		substr(obter_desc_cond_pagto(cd_condicao_pagamento),1,255)
	into	cd_pessoa_solicitante_w,
		ds_email_destino_w,
		cd_estabelecimento_w,
		ds_cond_pagto_w
	from    	ordem_compra
	where   	nr_ordem_compra = nr_documento_p;

elsif	(ie_tipo_documento_p = 'R') then

	select	cd_pessoa_requisitante,
		substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante), 'E'),1,2000) ds_email_destino,
		cd_estabelecimento
	into	cd_pessoa_solicitante_w,
		ds_email_destino_w,
		cd_estabelecimento_w
	from    	requisicao_material
	where   	nr_requisicao = nr_documento_p;
end if;

select 	decode(nvl(ie_tipo_documento_p,'X'),	'C', wheb_mensagem_pck.get_texto(299881),
					'S', wheb_mensagem_pck.get_texto(299882),
					'O', wheb_mensagem_pck.get_texto(299883),
					'R', wheb_mensagem_pck.get_texto(299885), wheb_mensagem_pck.get_texto(299886)) ds_tipo_documento
into	ds_tipo_documento_w
from	dual;

open C01;
loop
fetch C01 into	
	nr_seq_regra_email_w,
	ds_email_remetente_w,
	ds_email_adicional_w;
exit when C01%notfound;
	begin

	select	substr(
		replace_macro(
		replace_macro(
		replace_macro(
		replace_macro(ds_assunto,
			'@nr_documento',nr_documento_p),
			'@tipo_documento',ds_tipo_documento_w),
			'@ds_cond_pagto',ds_cond_pagto_w),
			'@ds_justificativa',ds_justificativa_p),1,255),
		substr(
		replace_macro(
		replace_macro(
		replace_macro(
		replace_macro(ds_mensagem_padrao,
			'@nr_documento',nr_documento_p),
			'@tipo_documento',ds_tipo_documento_w),
			'@ds_cond_pagto',ds_cond_pagto_w),
			'@ds_justificativa',ds_justificativa_p),1,2000)
		into	ds_assunto_w,
			ds_mensagem_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_email_w;

		select	nvl(max(ie_usuario),'U'),
			max(cd_perfil_disparar)
		into	ie_usuario_w,
			cd_perfil_disparar_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_email_w;

		if	(ie_usuario_w = 'U') then --Usuario
			select	ds_email,
				nm_usuario
			into	ds_email_origem_w,
				ds_usuario_origem_w
			from	usuario
			where	nm_usuario = nm_usuario_p;
		elsif	(ie_usuario_w = 'C') then --Setor compras
			select	ds_email
			into	ds_email_origem_w
			from	parametro_compras
			where	cd_estabelecimento = cd_estabelecimento_w;

			select	nvl(ds_fantasia,ds_razao_social)
			into	ds_usuario_origem_w
			from	estabelecimento_v
			where	cd_estabelecimento = cd_estabelecimento_w;
		elsif	(ie_usuario_w = 'O') then --Comprador
			ds_email_origem_w		:= ds_email_comprador_w;
			ds_usuario_origem_w	:= ds_usuario_comprador_w;
		end if;

		if	(ds_email_remetente_w <> 'X') then
			ds_email_origem_w	:= ds_email_remetente_w;
		end if;

		if	(ds_email_destino_w is not null) and
			(nvl(ds_email_destino_w,'X') <> 'X') then
			ds_email_destino_w := substr(ds_email_destino_w || ',',1,2000);
		end if;

		if	(ds_email_adicional_w is not null) then
			ds_email_destino_w := substr(ds_email_destino_w || ds_email_adicional_w,1,2000);
		end if;

		if	(ds_email_destino_w is not null) and
			((cd_perfil_disparar_w is null) or
			((cd_perfil_disparar_w is not null) and (cd_perfil_disparar_w = obter_perfil_ativo))) then

			begin
			enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_origem_w,ds_email_destino_w,ds_usuario_origem_w,'M');
			exception when others then
				ds_assunto_w := '';
			end;
		end if;
	end;
end loop;
close C01;

commit;

end envia_email_atraso_aprovacao;
/