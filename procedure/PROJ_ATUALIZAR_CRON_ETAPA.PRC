create or replace
procedure proj_atualizar_cron_etapa(nr_sequencia_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_interno_w	number(10);
nr_predecessora_w	number(10);
dt_inicio_prev_w	date;
dt_fim_prev_w		date;
dt_fim_prev_ww		date;
nr_sequencia_sup_w	number(10); --  Sequ�ncia da atividade posterior
nr_seq_cronograma_w	number(10);
qt_hora_prev_w		number(15,2);
qt_hora_prev_etapa_w	number(15,2);
nr_seq_sup_fase_w	number(10);
qt_calendario_w		number(10);
ie_regra_ini_fim_w	varchar2(1);
nr_sequencia_w		number(10);

dt_inicio_ativ_w	date;
dt_fim_ativ_w		date;

Cursor C01 is
	select	nr_sequencia
	from	proj_cron_etapa
	where	nr_predecessora = nr_seq_interno_w
	and	nr_seq_cronograma = nr_seq_cronograma_w
	order by 1;
	
TYPE 		fetch_array IS TABLE OF c01%ROWTYPE;
s_array 	fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_c01_w	Vetor;

begin

-- Buscar os dados da atividade atualizada 

select	nr_seq_interno,
	dt_inicio_prev,
	dt_fim_prev,
	nr_seq_cronograma,
	nr_seq_superior,
	substr(obter_horas_etapa_recurso(nr_sequencia),1,255),
	--qt_hora_prev,
	nr_predecessora,
	ie_regra_ini_fim
into	nr_seq_interno_w,
	dt_inicio_prev_w,
	dt_fim_prev_w,
	nr_seq_cronograma_w,
	nr_seq_sup_fase_w,
	qt_hora_prev_etapa_w,
	nr_predecessora_w,
	ie_regra_ini_fim_w
from	proj_cron_etapa
where	nr_sequencia = nr_sequencia_p
order by 1;

select	count(*)
into	qt_calendario_w
from	proj_cron_calendario
where	nr_seq_cronograma = nr_seq_cronograma_w;

if (qt_calendario_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(173564);
end if;

if (nr_predecessora_w is not null or dt_inicio_prev_w is not null) then
	
	if (nr_predecessora_w is not null) then
		select	max(a.dt_fim_prev)
		into	dt_inicio_prev_w
		from	proj_cron_etapa a
		where	nr_seq_interno = nr_predecessora_w
		and	nr_seq_cronograma = nr_seq_cronograma_w;
	end if;
	
	if (dt_inicio_prev_w is not null) then

		if (ie_regra_ini_fim_w = 'B') then
		
			proj_verifica_calendario_cron(dt_inicio_prev_w, qt_hora_prev_etapa_w, dt_inicio_ativ_w, dt_fim_ativ_w, nr_seq_cronograma_w, nm_usuario_p,nr_sequencia_p);
			
			update	proj_cron_etapa
			set	dt_fim_prev = dt_fim_ativ_w,
				dt_inicio_prev = dt_inicio_ativ_w
			where	nr_sequencia = nr_sequencia_p;
		
		else
			select	dt_inicio_prev
			into	dt_inicio_prev_w
			from	proj_cron_etapa
			where	nr_sequencia = nr_sequencia_p;
		
			proj_verifica_calendario_cron(dt_inicio_prev_w, qt_hora_prev_etapa_w, dt_inicio_ativ_w, dt_fim_ativ_w, nr_seq_cronograma_w, nm_usuario_p,nr_sequencia_p);
			
			update	proj_cron_etapa
			set	dt_fim_prev = dt_fim_ativ_w
			where	nr_sequencia = nr_sequencia_p;
		
		end if;
		
		commit;

	end if;
--  Abre cursor que ir� varrer as atividades posteriores (com o NR_SEQ_INTERNO da atividade informado na NR_PREDECESSORA)

	Proj_atualizar_tempo_fase(nr_sequencia_p, nm_usuario_p);

	open c01;
	loop
	FETCH C01 BULK COLLECT INTO s_array LIMIT 1000;
		Vetor_c01_w(i) := s_array;
		i := i + 1;
	EXIT WHEN C01%NOTFOUND;
	END LOOP;
	CLOSE C01;

	for i in 1..Vetor_c01_w.COUNT loop
		s_array := Vetor_c01_w(i);
		for z in 1..s_array.COUNT loop
		nr_sequencia_sup_w := s_array(z).nr_sequencia;
			begin
			proj_atual_hr_etapa_superior(nr_sequencia_p);
			Proj_atualizar_tempo_fase(nr_sequencia_p, nm_usuario_p);
			proj_atualizar_cron_etapa(nr_sequencia_sup_w ,nm_usuario_p);
			commit;
		
			end;
		end loop;
	end loop;
	
	commit;
	
end if;

end proj_atualizar_cron_etapa;
/
