create or replace
procedure pls_imp_conta_proc
			(	ds_versao_tiss_p		Varchar2,
				nr_seq_conta_proc_p		Number,
				ie_tipo_guia_p			Varchar2,
				ie_tipo_despesa_p		Number,
				nr_seq_conta_p			Number,
				cd_procedimento_p		Number,
				qt_procedimento_p		Number,
				ds_procedimento_p		Varchar2,
				vl_unitario_p			Number,
				vl_procedimento_p		Number,
				cd_tipo_tabela_p		Varchar2,
				tx_reducao_acrescimo_p		Number,
				dt_procedimento_p		Date,
				ie_via_acesso_p			Varchar2,
				ds_justificativa_p		Varchar2,
				nm_usuario_p			Varchar2,
				tx_item_p			Number,
				dt_inicio_proc_p		Date,
				dt_fim_proc_p			Date,
				cd_grau_partic_imp_p		Varchar2,
				ie_tecnica_utilizada_p		Varchar2,
				nr_seq_item_tiss_p		in pls_conta_proc_regra.nr_seq_item_tiss%type,
				nr_seq_item_tiss_vinculo_p	in pls_conta_mat_regra.nr_seq_item_tiss_vinculo%type,
				cd_dente_p			varchar2 default null,
				cd_regiao_boca_p		varchar2 default null,
				cd_face_dente_p			varchar2 default null,
				qt_unidade_serv_p		number default null,
				vl_proc_copartic_p		number default null,
				ie_autorizado_p			varchar2 default null) is  

dt_inicio_proc_w		Date;
dt_procedimento_w		Varchar2(50);
ds_mascara_w			Varchar2(50);
nr_seq_conta_proc_w		Number(10,0);
ie_tipo_guia_w			Varchar2(10);
dt_atendimento_imp_w		Date;
qt_procedimento_imp_w		pls_conta_proc.qt_procedimento_imp%type;
qt_proc_regra_w			pls_integer;

begin

/*
if	(ds_versao_tiss_p  = '2.01.02') then
	ds_mascara_w	:= 'yyyy-mm-dd';
elsif	(ds_versao_tiss_p  = '2.01.03') then
	ds_mascara_w	:= 'dd/mm/yyyy';
elsif	(ds_versao_tiss_p  = '2.02.01') then
	ds_mascara_w	:= 'yyyy-mm-dd';
end if;
*/

dt_procedimento_w	:= to_char(dt_procedimento_p);
dt_inicio_proc_w	:= dt_inicio_proc_p;
nr_seq_conta_proc_w 	:= nr_seq_conta_proc_p;

if	(nr_seq_conta_proc_p is null) then
	select  pls_conta_proc_seq.nextval
	into	nr_seq_conta_proc_w
	from	dual;	
end if;

begin
select	ie_tipo_guia,
	dt_atendimento_imp
into	ie_tipo_guia_w,
	dt_atendimento_imp_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_p;
exception
when others then
	ie_tipo_guia_w		:= '';
	dt_atendimento_imp_w	:= null;
end;

/* Felipe - 10/05/2011 - OS 314559 - Para as guias de consulta ser� inserido a data do ATENDIMENTO na data do procedimento*/
/*Robson OS - 361886
if	(ie_tipo_guia_w	= '3') then	
	dt_inicio_proc_w	:= dt_procedimento_p;
end if;
*/

/* No TISS 3 n�o tem mais quantidade do procedimento na guia de consulta,
por�m precisamos dessa informa��o para valoriza��o, etc */
if	(ie_tipo_guia_w = '3') and
	(qt_procedimento_p is null) then
	qt_procedimento_imp_w	:= 1;
else
	qt_procedimento_imp_w	:= qt_procedimento_p;
end if;

insert into pls_conta_proc(
	nr_sequencia, dt_atualizacao, nm_usuario,
	nr_seq_conta, cd_procedimento_imp, qt_procedimento_imp,
	ds_procedimento_imp, vl_unitario_imp, vl_procedimento_imp,
	cd_tipo_tabela_imp, tx_reducao_acrescimo_imp, ie_tipo_despesa_imp,
	ie_status, ie_situacao, dt_procedimento_imp,
	dt_atualizacao_nrec, nm_usuario_nrec, ie_via_acesso_imp,
	ds_justificativa, tx_item, dt_procedimento,
	dt_inicio_proc_imp, dt_fim_proc_imp, vl_apresentado_xml,
	cd_grau_partic_imp, ie_tecnica_utilizada_imp,  CD_DENTE_IMP,
	cd_regiao_boca_imp, cd_face_dente_imp, qt_unidade_serv_imp,
	vl_proc_copartic_imp, ie_autorizado_imp)
values(	nr_seq_conta_proc_w, sysdate, nm_usuario_p,
	nr_seq_conta_p, cd_procedimento_p, qt_procedimento_imp_w,
	ds_procedimento_p, nvl(vl_unitario_p,dividir(nvl(vl_procedimento_p,0),qt_procedimento_p)), nvl(vl_procedimento_p,0),
	cd_tipo_tabela_p, tx_reducao_acrescimo_p, ie_tipo_despesa_p,
	'U', 'I', dt_procedimento_p,
	sysdate, nm_usuario_p, ie_via_acesso_p,
	ds_justificativa_p, tx_item_p, dt_procedimento_w,
	dt_inicio_proc_w, dt_fim_proc_p, vl_procedimento_p,
	cd_grau_partic_imp_p, ie_tecnica_utilizada_p,cd_dente_p,
	cd_regiao_boca_p, cd_face_dente_p, qt_unidade_serv_p,
	vl_proc_copartic_p, ie_autorizado_p);	
commit;

-- so gera neste momento quando possui informa��o
if	(nr_seq_item_tiss_p is not null) then


	pls_cta_proc_mat_regra_pck.cria_registro_regra_proc(nr_seq_conta_proc_w, nm_usuario_p);
	
	pls_cta_proc_mat_regra_pck.atualiza_seq_tiss_proc(nr_seq_conta_proc_w, nr_seq_item_tiss_p, nr_seq_item_tiss_vinculo_p, nm_usuario_p);
	
	commit;
	
end if;

end pls_imp_conta_proc;
/
