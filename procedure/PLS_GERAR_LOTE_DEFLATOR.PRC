create or replace
procedure pls_gerar_lote_deflator
			(	nr_seq_lote_ref_p		number,
				nr_seq_reajuste_p		number,
				tx_deflator_p			number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				nr_seq_motivo_def_p		number,
				ds_observacao_p			varchar2,
				ie_commit_p			varchar2,
				tx_reajuste_correto_p		number,
				tx_reajuste_copart_p		number,
				tx_reajuste_copart_max_p	number,
				tx_deflator_copartic_p		number,
				tx_deflator_copartic_max_p	number,
				nr_seq_lote_novo_p 	out	number) is

nr_seq_reajuste_w		number(10);
nr_seq_motivo_def_w		number(10);
nr_seq_reaj_tab_w		number(10);
nr_seq_contrato_ww		number(10);
dt_reajuste_w			date;
ds_observacao_w			varchar2(2000);
nr_seq_lote_reaj_copart_w	number(10);
tx_reajuste_copartic_w		number(7,4);
tx_reajuste_copartic_max_w	number(7,4);
qt_regra_copartic_w		number(10);
ie_vinculo_coparticipacao_w	varchar2(10);
ie_reajustar_copartic_w		varchar2(10);
nr_seq_lote_reaj_inscricao_w	number(10);
tx_reajuste_inscricao_w		number(10);
qt_regra_inscricao_w		number(10);
ie_reajustar_inscricao_w	varchar2(10);
tx_reajuste_inscricao_aux_w	number(7,4);
tx_reajuste_copartic_aux_w	number(7,4);
tx_reaj_copartic_max_aux_w	number(7,4);
ie_vinculo_copar_aux_w		varchar2(10);
nr_seq_tipo_reajuste_w		pls_tipo_reajuste.nr_sequencia%type;
nr_seq_regra_gerada_w		pls_reajuste_copartic.nr_seq_regra_gerada%type;
nr_seq_regra_atual_w		pls_reajuste_copartic.nr_seq_regra_atual%type;
tx_reajuste_w 			number(7,4);
tx_reajuste_vl_maximo_w 	pls_lote_reaj_copartic.tx_reajuste_vl_maximo%type;
vl_maximo_w			pls_regra_coparticipacao.vl_maximo%type;
vl_maximo_atual_w		pls_regra_coparticipacao.vl_maximo%type;
vl_coparticipacao_w		pls_regra_coparticipacao.vl_coparticipacao%type;
vl_reajuste_w			number(15,2);
vl_reajuste_atual_w		number(15,2);
nr_seq_copartic_reajustada_w	pls_reajuste_copartic.nr_seq_copartic_reajustada%type;
nr_seq_copartic_ant_w		pls_reajuste_copartic.nr_seq_copartic_ant%type;
qt_processo_reaj_w		number(10);
tx_reajuste_processo_w		number(7,4);
nr_seq_processo_w		number(10);
tx_reajuste_lote_w		number(7,4);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_processo,
		tx_reajuste
	from	pls_reajuste_tabela
	where	nr_seq_reajuste	= nr_seq_reajuste_p;

cursor c02 is
	select	tx_reajuste,
		tx_reajuste_vl_maximo,
		ie_vinculo_coparticipacao,
		nr_seq_contrato,
		nr_sequencia
	from	pls_lote_reaj_copartic
	where	nr_seq_reajuste = nr_seq_reajuste_p;

cursor c03 is
	select	nr_seq_contrato,
		tx_reajuste
	from	pls_lote_reaj_inscricao
	where	nr_seq_reajuste = nr_seq_reajuste_p;

begin

if	(nvl(tx_reajuste_correto_p,0) <> 0) then
	select	count(1)
	into	qt_processo_reaj_w
	from	pls_reajuste_tabela a,
		pls_reajuste b
	where	b.nr_sequencia = a.nr_seq_reajuste
	and	b.nr_sequencia = nr_seq_reajuste_p
	and	a.nr_seq_processo is not null;
	
	if	(qt_processo_reaj_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1029738);
	end if;
end if;

select	pls_reajuste_seq.NextVal
into	nr_seq_reajuste_w
from	dual;

if	(nvl(nr_seq_motivo_def_p,0) = 0) then
	nr_seq_motivo_def_w	:= null;
else
	nr_seq_motivo_def_w	:= nr_seq_motivo_def_p;
end if;

select	dt_reajuste,
	tx_reajuste_inscricao,
	tx_reajuste_copartic,
	tx_reajuste_copartic_max,
	ie_vinculo_coparticipacao,
	tx_reajuste
into	dt_reajuste_w,
	tx_reajuste_inscricao_aux_w,
	tx_reajuste_copartic_aux_w,
	tx_reaj_copartic_max_aux_w,
	ie_vinculo_copar_aux_w,
	tx_reajuste_lote_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;

ds_observacao_w := substr('Reajuste deflator referente ao lote ' || to_char(nr_seq_reajuste_p) || chr(10) || ds_observacao_p,1,2000);

select	max(nr_sequencia)
into	nr_seq_tipo_reajuste_w
from	pls_tipo_reajuste
where	ie_deflator = 'S';

insert	into	pls_reajuste
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_reajuste,
		tx_reajuste,
		ie_indice_reajuste,
		ds_oficio_ans,
		nr_protocolo_ans,
		ie_tipo_reajuste,
		ds_observacao,
		ie_status,
		nr_seq_contrato,
		cd_estabelecimento,
		ie_reajuste_tabela,
		dt_autorizacao_ans,
		tx_deflator,
		nr_seq_lote_referencia,
		nr_seq_motivo_deflator,
		nr_seq_intercambio,
		nr_contrato,
		ie_aplicacao_reajuste,
		nr_seq_plano,
		ie_tipo_contrato,
		dt_reajuste_aux,
		dt_aplicacao_reajuste,
		nr_seq_tipo_reajuste,
		tx_reajuste_correto,
		tx_reajuste_copart_correto,
		tx_reajuste_copart_max_correto,
		tx_deflator_copartic,
		tx_deflator_copartic_max)
	(select	nr_seq_reajuste_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		dt_reajuste,
		0,
		ie_indice_reajuste,
		ds_oficio_ans,
		nr_protocolo_ans,
		ie_tipo_reajuste,
		ds_observacao_w,
		'1',
		nr_seq_contrato,
		cd_estabelecimento,
		ie_reajuste_tabela,
		dt_autorizacao_ans,
		tx_deflator_p,
		nvl(nr_seq_lote_ref_p,nr_sequencia),
		nr_seq_motivo_def_w,
		nr_seq_intercambio,
		nr_contrato,
		ie_aplicacao_reajuste,
		nr_seq_plano,
		ie_tipo_contrato,
		to_char(dt_reajuste,'mm/yyyy'),
		dt_aplicacao_reajuste,
		nr_seq_tipo_reajuste_w,
		tx_reajuste_correto_p,
		tx_reajuste_copart_p,
		tx_reajuste_copart_max_p,
		tx_deflator_copartic_p,
		tx_deflator_copartic_max_p
	from	pls_reajuste
	where	nr_sequencia = nr_seq_reajuste_p);

/*pls_inserir_tabela_reajuste(nr_seq_reajuste_w,null,nm_usuario_p);
LEPINSKi - Comentada a linha acima e adicionado o cursor abaixo, para inserir no deflator todas as tabelas do reajuste */
open C01;
loop
fetch C01 into
	nr_seq_reaj_tab_w,
	nr_seq_processo_w,
	tx_reajuste_w;
exit when C01%notfound;
	begin
	
	if	((nr_seq_processo_w is not null) and (tx_reajuste_lote_w <> tx_reajuste_w)) then
		tx_reajuste_processo_w	:= (((10 / (10 + ((10 * tx_reajuste_w) / 100))) - 1)* 100);
	else
		tx_reajuste_processo_w	:= null;
	end if;
	
	insert	into	pls_reajuste_tabela
		(	nr_sequencia,
			nr_seq_reajuste,
			nr_seq_tabela,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_plano,
			dt_inicio_vigencia,
			dt_contrato,
			nr_seq_contrato,
			ie_origem_tabela,
			nr_seq_processo,
			tx_reajuste)
	(	select	pls_reajuste_tabela_seq.nextval,
			nr_seq_reajuste_w,
			nr_seq_tabela,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_plano,
			dt_inicio_vigencia,
			dt_contrato,
			nr_seq_contrato,
			ie_origem_tabela,
			nr_seq_processo,
			tx_reajuste_processo_w
		from	pls_reajuste_tabela
		where	nr_sequencia	= nr_seq_reaj_tab_w);
	end;
end loop;
close C01;

ie_reajustar_copartic_w	:= 'N';

open C02;
loop
fetch C02 into
	tx_reajuste_copartic_w,
	tx_reajuste_copartic_max_w,
	ie_vinculo_coparticipacao_w,
	nr_seq_contrato_ww,
	nr_seq_lote_reaj_copart_w;
exit when c02%notfound;
	begin
	vl_maximo_w := null;		
	vl_reajuste_w := null;

	select	max(nr_seq_regra_gerada),
		max(nr_seq_copartic_reajustada)
	into	nr_seq_regra_gerada_w,
		nr_seq_copartic_reajustada_w
	from	pls_reajuste_copartic
	where	nr_seq_lote = nr_seq_lote_reaj_copart_w;

	if	(nr_seq_regra_gerada_w is not null) then	
		select	nr_seq_regra_gerada,
			nr_seq_regra_atual
		into	nr_seq_regra_gerada_w,
			nr_seq_regra_atual_w
		from	pls_reajuste_copartic
		where	nr_seq_lote = nr_seq_lote_reaj_copart_w
		and	nr_seq_regra_gerada is not null
		and	rownum = 1;
	
		select	max(vl_maximo),
			max(vl_coparticipacao)
		into	vl_maximo_w,
			vl_reajuste_w
		from	pls_regra_coparticipacao
		where	nr_sequencia = nr_seq_regra_gerada_w;
				
		select	max(vl_maximo),
			max(vl_coparticipacao)
		into	vl_maximo_atual_w,
			vl_reajuste_atual_w
		from	pls_regra_coparticipacao
		where	nr_sequencia = nr_seq_regra_atual_w;
				
		if	(nvl(vl_reajuste_w,0) <> 0) then
			tx_reajuste_copartic_w		:= (((vl_reajuste_w - vl_reajuste_atual_w) * 100) / vl_reajuste_w) * - 1;
		end if;
		
		if	(nvl(vl_maximo_w,0) <> 0) then
			tx_reajuste_copartic_max_w 	:= (((vl_maximo_w - vl_maximo_atual_w) * 100) / vl_maximo_w) * -1;
		else
			tx_reajuste_copartic_max_w 	:= tx_reajuste_copartic_w;
		end if;
	elsif	(nr_seq_copartic_reajustada_w is not null) then
		select	nr_seq_copartic_reajustada,
			nr_seq_copartic_ant
		into	nr_seq_copartic_reajustada_w,
			nr_seq_copartic_ant_w
		from	pls_reajuste_copartic
		where	nr_seq_lote = nr_seq_lote_reaj_copart_w
		and	nr_seq_copartic_reajustada is not null
		and	rownum = 1;
	
		select	max(a.vl_apropriacao),
			max(b.vl_maximo_copartic)
		into	vl_reajuste_w,
			vl_maximo_w
		from	pls_regra_copartic_aprop a,
			pls_regra_copartic 	 b
		where	b.nr_sequencia = a.nr_seq_regra
		and	b.nr_sequencia = nr_seq_copartic_reajustada_w;
		
		select	max(a.vl_apropriacao),
			max(b.vl_maximo_copartic)
		into	vl_reajuste_atual_w,
			vl_maximo_atual_w
		from	pls_regra_copartic_aprop a,
			pls_regra_copartic 	 b
		where	b.nr_sequencia = a.nr_seq_regra
		and	b.nr_sequencia = nr_seq_copartic_ant_w;
		
		if	(vl_reajuste_w is not null) then
			tx_reajuste_copartic_w		:= (((vl_reajuste_w - vl_reajuste_atual_w) * 100) / vl_reajuste_w) * -1;
		end if;
		
		if	(vl_maximo_w is not null) then
			tx_reajuste_copartic_max_w 	:= (((vl_maximo_w - vl_maximo_atual_w) * 100) / vl_maximo_w) * -1;
		else
			tx_reajuste_copartic_max_w := tx_reajuste_copartic_w;
		end if;
	end if;
	
	if	(vl_reajuste_w is null) then
	
		select	max(pr_reajustado)
		into	tx_reajuste_copartic_w
		from	pls_reajuste_tabela	e,
			pls_tabela_preco	c,
			pls_reajuste		b,	
			pls_plano_preco		f,
			pls_reajuste_preco	g
		where	e.nr_seq_tabela		= c.nr_sequencia
		and	e.nr_seq_reajuste	= b.nr_sequencia
		and	f.nr_seq_tabela 	= c.nr_sequencia
		and	g.nr_seq_tabela		= e.nr_sequencia
		and	g.nr_seq_preco		= f.nr_sequencia
		and	c.nr_contrato 		= nr_seq_contrato_ww
		and	b.nr_sequencia		= nr_seq_reajuste_w;
		
		tx_reajuste_copartic_max_w := tx_reajuste_copartic_w;
	end if;
	
	select	pls_lote_reaj_copartic_seq.nextval
	into	nr_seq_lote_reaj_copart_w
	from	dual;
	
	insert	into	pls_lote_reaj_copartic
	(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		cd_estabelecimento, nr_seq_contrato, dt_referencia,  tx_reajuste, tx_reajuste_vl_maximo,
		nr_seq_reajuste, ie_vinculo_coparticipacao)
	values
	(	nr_seq_lote_reaj_copart_w, sysdate, nm_usuario_p,sysdate, nm_usuario_p,
		cd_estabelecimento_p, nr_seq_contrato_ww,dt_reajuste_w, tx_reajuste_copartic_w,tx_reajuste_copartic_max_w,
		nr_seq_reajuste_w, ie_vinculo_coparticipacao_w);
	
	pls_incluir_copartic_lote_reaj(nr_seq_lote_reaj_copart_w, cd_estabelecimento_p, 'N', nm_usuario_p);
	
	select	count(1)
	into	qt_regra_copartic_w
	from	pls_reajuste_copartic
	where	nr_seq_lote	= nr_seq_lote_reaj_copart_w;
	
	if	(qt_regra_copartic_w = 0) then
		delete	pls_lote_reaj_copartic
		where	nr_sequencia	= nr_seq_lote_reaj_copart_w;
	end if;
	
	ie_reajustar_copartic_w	:= 'S';
	end;
end loop;
close c02;

ie_reajustar_inscricao_w	:= 'N';

open C03;
loop
fetch C03 into
	nr_seq_contrato_ww,
	tx_reajuste_inscricao_w;
exit when C03%notfound;
	begin
	if	(nvl(tx_reajuste_inscricao_w,0) <> 0) then
		tx_reajuste_inscricao_w	:= tx_reajuste_inscricao_w * -1;
	end if;
	
	select	pls_lote_reaj_inscricao_seq.nextval
	into	nr_seq_lote_reaj_inscricao_w
	from	dual;
	
	insert	into	pls_lote_reaj_inscricao
		(	nr_sequencia, dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec,
			cd_estabelecimento,nr_seq_contrato, dt_referencia,  tx_reajuste, nr_seq_reajuste)
	values	(	nr_seq_lote_reaj_inscricao_w, sysdate, nm_usuario_p,sysdate, nm_usuario_p,
			cd_estabelecimento_p, nr_seq_contrato_ww, dt_reajuste_w,  tx_reajuste_inscricao_w,nr_seq_reajuste_w);
	
	pls_incluir_inscricao_reaj(nr_seq_lote_reaj_inscricao_w, cd_estabelecimento_p, nm_usuario_p);
	
	select	count(1)
	into	qt_regra_inscricao_w
	from	pls_reajuste_inscricao
	where	nr_seq_lote	= nr_seq_lote_reaj_inscricao_w;
	
	if	(qt_regra_inscricao_w = 0) then
		delete	pls_lote_reaj_inscricao
		where	nr_sequencia	= nr_seq_lote_reaj_inscricao_w;
	end if;
	
	ie_reajustar_inscricao_w	:= 'S';
	end;
end loop;
close C03;

update	pls_reajuste
set	nr_seq_lote_deflator	= nr_seq_reajuste_w
where	nr_sequencia		= nr_seq_reajuste_p;

update	pls_reajuste
set	ie_reajustar_inscricao		= ie_reajustar_inscricao_w,
	ie_reajustar_copartic		= ie_reajustar_copartic_w,
	tx_reajuste_inscricao		= tx_reajuste_inscricao_aux_w,
	tx_reajuste_copartic		= tx_reajuste_copartic_aux_w,
	tx_reajuste_copartic_max	= tx_reaj_copartic_max_aux_w,
	ie_vinculo_coparticipacao	= ie_vinculo_copar_aux_w
where	nr_sequencia			= nr_seq_reajuste_w;

pls_gerar_reajuste_preco(nr_seq_reajuste_w, 'D', 'N', cd_estabelecimento_p, nm_usuario_p);

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_gerar_lote_deflator;
/
