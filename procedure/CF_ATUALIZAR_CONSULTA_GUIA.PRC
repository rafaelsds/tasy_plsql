create or replace
procedure CF_ATUALIZAR_CONSULTA_GUIA(
									cd_empresa_p					number,									
									cd_estabelecimento_p			number,
									cd_convenio_parametro_p			number,
									cd_categoria_parametro_p		varchar2,
									cd_plano_convenio_p				varchar2,									
									dt_periodo_inicial_p			date,
									dt_periodo_final_p				date,
									nr_seq_status_p					number,
									nr_doc_convenio_p				varchar2,
									nr_interno_conta_p				number,
									nr_remessa_p					number,
									nr_seq_protocolo_p				number,
									cd_pessoa_fisica_p				varchar2,
									cd_medico_executor_p			varchar2,
									nr_seq_proc_interno_p			number,
									cd_grupo_proc_p					number,
									cd_especialidade_p				number,
									cd_area_procedimento_p			number,									
									ie_guia_protocolo_p				number,
									ie_desconsidera_status_fat_p	varchar2,
									ie_opcao_data_p					number,									
									nm_usuario_p					varchar2,
									ie_status_execucao_p     varchar2,
									ie_somente_remessa_p	varchar2) is 									
									
									
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir registros na tabela w_cf_consulta_guia para o usu�rio que atualizar a consulta na pasta Consulta Guia da Central de Faturamento.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_fisica_w		atendimento_paciente.cd_pessoa_fisica%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
nr_doc_convenio_w		procedimento_paciente.nr_doc_convenio%type;
nr_interno_conta_w		procedimento_paciente.nr_interno_conta%type;
dt_procedimento_w		procedimento_paciente.dt_procedimento%type;
nr_sequencia_w			procedimento_paciente.nr_sequencia%type;
nr_seq_exame_w			procedimento_paciente.nr_seq_exame%type;
cd_procedimento_w		procedimento_paciente.cd_procedimento%type;
ie_origem_proced_w		procedimento_paciente.ie_origem_proced%type;
vl_procedimento_w		procedimento_paciente.vl_procedimento%type;
cd_senha_w				procedimento_paciente.cd_senha%type;
nr_seq_proc_interno_w	procedimento_paciente.nr_seq_proc_interno%type;
nr_seq_atepacu_w		procedimento_paciente.nr_seq_atepacu%type;
cd_setor_atendimento_w	procedimento_paciente.cd_setor_atendimento%type;
dt_entrada_unidade_w	procedimento_paciente.dt_entrada_unidade%type;
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_estabelecimento_w	conta_paciente.cd_estabelecimento%type;			
nr_seq_status_fat_w		conta_paciente.nr_seq_status_fat%type;
nr_seq_status_mob_w		conta_paciente.nr_seq_status_mob%type;
nr_seq_protocolo_w		conta_paciente.nr_seq_protocolo%type;
cd_convenio_parametro_w	conta_paciente.cd_convenio_parametro%type;
nm_paciente_w			pessoa_fisica.nm_pessoa_fisica%type;
qt_altura_w				pessoa_fisica.qt_altura_cm%type;
qt_peso_w				pessoa_fisica.qt_peso%type;
cd_plano_convenio_w		atend_categoria_convenio.cd_plano_convenio%type;
cd_usuario_convenio_w	atend_categoria_convenio.cd_usuario_convenio%type;
ds_status_fat_w			cf_status_faturamento.ds_status_fat%type;
ds_status_mob_w			cf_status_mobilizacao.ds_status_mob%type;
ds_procedimento_w		procedimento.ds_procedimento%type;
ds_convenio_w			convenio.ds_convenio%type;
ds_plano_w				convenio_plano.ds_plano%type;
cd_empresa_w			estabelecimento.cd_empresa%type;
ie_insere_w 			varchar2(1);
ds_estagio_w			varchar2(120);
ds_classificacao_w		varchar2(120);
qt_pendencia_total_w	number(10,0);
qt_pendencia_aberta_w	number(10,0);
nr_seq_regra_resp_w		number(10,0);
nr_prescricao_w			procedimento_paciente.nr_prescricao%type;
nr_sequencia_prescricao_w       procedimento_paciente.nr_sequencia_prescricao%type;      
ie_status_execucao_w		prescr_procedimento.ie_status_execucao%type;
dt_periodo_inicial_w	date;
dt_periodo_final_w		date;

-- L� OS PROCEDIMENTOS CONFORME OS FILTROS 
Cursor C01 is
	select	a.nr_doc_convenio,
			a.nr_interno_conta,
			d.nr_atendimento,
			a.dt_procedimento,
			a.nr_seq_exame,
			a.cd_procedimento,
			a.ie_origem_proced,               
			nvl(a.vl_procedimento, 0),
			a.cd_senha,
			d.cd_pessoa_fisica,
			b.cd_estabelecimento,
			b.nr_seq_status_fat,
			b.nr_seq_status_mob,
			b.nr_seq_protocolo,
			b.cd_convenio_parametro,
			a.nr_seq_proc_interno,
			a.nr_seq_atepacu,
			a.cd_setor_atendimento,
			a.dt_entrada_unidade,
			a.nr_sequencia,	
			a.nr_prescricao, 
			a.nr_sequencia_prescricao
	from	estrutura_procedimento_v	c,
			procedimento_paciente		a,
			conta_paciente				b,
			atendimento_paciente		d
	where	a.dt_procedimento between dt_periodo_inicial_w and dt_periodo_final_w	
	and		((ie_desconsidera_status_fat_p = 'N' and b.nr_seq_status_fat = nr_seq_status_p) or	(ie_desconsidera_status_fat_p = 'S'))
	and		((nr_seq_proc_interno_p is null) or (a.nr_seq_proc_interno = nr_seq_proc_interno_p))
	and		((cd_grupo_proc_p is null) or (c.cd_grupo_proc = cd_grupo_proc_p))
	and		((cd_especialidade_p is null) or (c.cd_especialidade = cd_especialidade_p))
	and		((cd_area_procedimento_p is null) or (c.cd_area_procedimento = cd_area_procedimento_p))
	and		a.nr_interno_conta = b.nr_interno_conta
	and		d.nr_atendimento   = b.nr_atendimento
	and		c.cd_procedimento  = a.cd_procedimento
	and		c.ie_origem_proced = a.ie_origem_proced	
	and		((cd_convenio_parametro_p is null) or (b.cd_convenio_parametro = cd_convenio_parametro_p))
	and		((cd_categoria_parametro_p is null) or (b.cd_categoria_parametro = cd_categoria_parametro_p))
	and		((cd_pessoa_fisica_p is null) or (d.cd_pessoa_fisica = cd_pessoa_fisica_p))
	and		((nr_doc_convenio_p is null) or (a.nr_doc_convenio = nr_doc_convenio_p))
	and		((nr_interno_conta_p is null) or (a.nr_interno_conta = nr_interno_conta_p))
	and		((ie_guia_protocolo_p = 0 and b.nr_seq_protocolo is not null) or
			(ie_guia_protocolo_p = 1 and b.nr_seq_protocolo is null) or
			(ie_guia_protocolo_p = 2))
	and		((cd_medico_executor_p is null) or (a.cd_medico_executor = cd_medico_executor_p))
	and		a.nr_doc_convenio is not null
	and 	((nr_seq_protocolo_p is null) or (b.nr_seq_protocolo = nr_seq_protocolo_p))
	and		c.ie_classificacao = 1	
	and   	b.nr_seq_status_fat is not null
	and 	b.nr_seq_status_mob is not null
	and 	b.nr_seq_regra_fluxo is not null
	and 	((nr_remessa_p is null) or (exists (select 1
												from	protocolo_documento x,
														protocolo_doc_item y
												where	x.nr_sequencia = y.nr_sequencia
														and x.nr_sequencia = nr_remessa_p
														and y.nr_seq_interno = b.nr_interno_conta
														and y.nr_documento = b.nr_atendimento
														and	x.ie_destino_remessa is not null)))
	and		((ie_somente_remessa_p = 'N') or (nr_remessa_p is not null) or (exists (select 1
												from	protocolo_documento x,
														protocolo_doc_item y
												where	x.nr_sequencia = y.nr_sequencia
														and y.nr_seq_interno = b.nr_interno_conta
														and y.nr_documento = b.nr_atendimento
														and	x.ie_destino_remessa is not null)))
	order by a.nr_interno_conta;

begin

/* Deleta os registros do usu�rio */
delete	from w_cf_consulta_guia
where	nm_usuario = nm_usuario_p;

if	(ie_opcao_data_p = 0) then
	dt_periodo_inicial_w	:= pkg_date_utils.start_of(sysdate,'DAY');
	dt_periodo_final_w		:= pkg_date_utils.end_of(sysdate,'DAY');
elsif	(ie_opcao_data_p = 1) then
	dt_periodo_inicial_w	:= pkg_date_utils.start_of(dt_periodo_inicial_p,'DAY');
	dt_periodo_final_w		:= pkg_date_utils.end_of(dt_periodo_final_p,'DAY');
elsif	(ie_opcao_data_p = 2) then
	dt_periodo_inicial_w	:= sysdate - 360;
	dt_periodo_final_w		:= pkg_date_utils.end_of(sysdate,'DAY');
end if;

open C01;
loop
fetch C01 into	
	nr_doc_convenio_w,
	nr_interno_conta_w,
	nr_atendimento_w,
	dt_procedimento_w,
	nr_seq_exame_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	vl_procedimento_w,
	cd_senha_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	nr_seq_status_fat_w,
	nr_seq_status_mob_w,
	nr_seq_protocolo_w,
	cd_convenio_parametro_w,
	nr_seq_proc_interno_w,
	nr_seq_atepacu_w,
	cd_setor_atendimento_w,
	dt_entrada_unidade_w,
	nr_sequencia_w,
	nr_prescricao_w, 
	nr_sequencia_prescricao_w;
exit when C01%notfound;
		begin
		ie_insere_w 		:= 'S';
		ds_estagio_w		:= '';
		ds_classificacao_w	:= '';
		
		select 		count(*),
					sum(decode(dt_fechamento, null, 1, 0))
		into		qt_pendencia_total_w,
					qt_pendencia_aberta_w
		from 		cta_pendencia 
		where 		nr_interno_conta = nr_interno_conta_w;		
		
		if			(qt_pendencia_total_w > 0) then

					select 		max(nr_seq_regra_resp)
					into		nr_seq_regra_resp_w
					from 		cta_pendencia 
					where 		nr_interno_conta = nr_interno_conta_w
					and 		dt_fechamento is null;
		
					select 		max(ds_regra)
					into		ds_classificacao_w
					from 		cta_regra_resp_pend
					where 		nr_sequencia = nr_seq_regra_resp_w;
										
					if			(qt_pendencia_aberta_w > 0) then
								
								--ds_estagio_w		:= 'Pend�ncia Ativa';   
								ds_estagio_w		:= WHEB_MENSAGEM_PCK.get_texto(297981);
					else

								--ds_estagio_w		:= 'Pend�ncia Resolvida';
								ds_estagio_w		:= WHEB_MENSAGEM_PCK.get_texto(297982);
							
					end if;
					
		else
					ds_estagio_w		:= '';
					ds_classificacao_w	:= '';
		end if;
		
		
		select		max(substr(obter_nome_pf(a.cd_pessoa_fisica),1,200)),
					max(a.qt_altura_cm),
					max(a.qt_peso)
		into		nm_paciente_w,
					qt_altura_w,
					qt_peso_w
		from		pessoa_fisica a
		where		a.cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		select		max(obter_dados_categ_conv(nr_atendimento_w, 'P')),
					max(obter_dados_categ_conv(nr_atendimento_w, 'U'))
		into		cd_plano_convenio_w,
					cd_usuario_convenio_w
		from		dual;
		
		select		max(a.ds_plano)
		into		ds_plano_w
		from		convenio_plano a
		where		a.cd_plano = cd_plano_convenio_w
		and			a.cd_convenio = cd_convenio_parametro_w;
		
		select		max(a.ds_status_fat)
		into		ds_status_fat_w
		from		cf_status_faturamento a
		where		a.nr_sequencia = nr_seq_status_fat_w;
		
		select		max(b.ds_status_mob)
		into		ds_status_mob_w
		from		cf_status_mobilizacao b
		where		b.nr_sequencia = nr_seq_status_mob_w;
		
		select		max(a.ds_procedimento)
		into		ds_procedimento_w
		from		procedimento a
		where		a.cd_procedimento = cd_procedimento_w
		and			a.ie_origem_proced = ie_origem_proced_w;
		
		select		max(a.ds_convenio)
		into		ds_convenio_w
		from		convenio a
		where		a.cd_convenio = cd_convenio_parametro_w;
		
		if	(ie_insere_w = 'S') and
			(((cd_estabelecimento_p is not null) and(cd_estabelecimento_w = cd_estabelecimento_p)) or
			(cd_estabelecimento_p is null)) then
			ie_insere_w:= 'S';
		else
			ie_insere_w:= 'N';
		end if;
						
		select	max(cd_empresa)
		into	cd_empresa_w
		from	estabelecimento
		where	cd_estabelecimento = cd_estabelecimento_w;
		
		if	(ie_insere_w = 'S') and
			(((cd_empresa_p is not null) and	(cd_empresa_p = cd_empresa_w)) or
			(cd_empresa_p is null)) then
			ie_insere_w:= 'S';
		else
			ie_insere_w:= 'N';
		end if;
			
		if	(ie_insere_w = 'S') and
			(((cd_plano_convenio_p is not null) and (cd_plano_convenio_p = cd_plano_convenio_w)) or
			(cd_plano_convenio_p is null)) then
			ie_insere_w:= 'S';
		else
			ie_insere_w:= 'N';
		end if;
		
		begin
		select  ie_status_execucao
		into 	ie_status_execucao_w
		from 	prescr_procedimento
		where 	nr_prescricao  = nr_prescricao_w 
		and 	nr_Sequencia = nr_sequencia_prescricao_w; 		
		exception
		when others then
			ie_status_execucao_w:= null;
			end;
		
		if	(ie_insere_w = 'S') and
			(((ie_status_execucao_p is not null) and (ie_status_execucao_p = ie_status_execucao_w)) or
			(ie_status_execucao_p is null)) then
			ie_insere_w:= 'S';
		else
			ie_insere_w:= 'N';
		end if;
		
		
		if	(ie_insere_w = 'S') then
			insert	into w_cf_consulta_guia(nr_guia,
											nr_interno_conta,
											nr_seq_status_fat,
											nr_seq_protocolo,
											nr_seq_status_mob,											
											dt_procedimento,
											cd_paciente,
											nm_paciente,
											qt_altura,
											qt_peso,
											nr_seq_exame,
											cd_procedimento,
											ie_origem_proced,
											vl_procedimento,
											cd_convenio,
											cd_plano_convenio,
											cd_senha,
											cd_usuario_convenio,
											ds_status_fat,
											ds_status_mob,
											ds_estagio,
											ds_classificacao,
											ds_procedimento,
											ds_convenio,
											ds_plano,
											dt_atualizacao,
											nm_usuario,
											nr_seq_proc_interno,
											nr_seq_atepacu,
											cd_setor_atendimento,
											dt_entrada_unidade,
											nr_atendimento,
											nr_seq_propaci,
											ie_status_execucao)
			values							(nr_doc_convenio_w,
											nr_interno_conta_w,
											nr_seq_status_fat_w,
											nr_seq_protocolo_w,
											nr_seq_status_mob_w,
											dt_procedimento_w,
											cd_pessoa_fisica_w,
											nm_paciente_w,
											qt_altura_w,
											qt_peso_w,
											nr_seq_exame_w,
											cd_procedimento_w,
											ie_origem_proced_w,
											vl_procedimento_w,
											cd_convenio_parametro_w,
											cd_plano_convenio_w,
											cd_senha_w,
											cd_usuario_convenio_w,
											ds_status_fat_w,
											ds_status_mob_w,
											ds_estagio_w,	
											ds_classificacao_w,
											ds_procedimento_w,
											ds_convenio_w,
											ds_plano_w,
											sysdate,
											nm_usuario_p,
											nr_seq_proc_interno_w,
											nr_seq_atepacu_w,
											cd_setor_atendimento_w,
											dt_entrada_unidade_w,
											nr_atendimento_w,
											nr_sequencia_w,
											ie_status_execucao_w);
		end if;
		end;
end loop;
close C01;


commit;

end CF_ATUALIZAR_CONSULTA_GUIA;
/
