CREATE OR REPLACE
PROCEDURE GRAVAR_PROC_ADICIONAL_SUS(nr_interno_conta_p	number,
						nr_atendimento_p		number,
						nm_usuario_p		varchar2) IS

dt_atualizacao_w			date		:= sysdate;
qt_dia_acompanhante_w		number(3);
nr_aih_w				number(13);
cd_procedimento_w			number(15);
qt_procedimento_w			number(10);
nr_sequencia_w			number(10);

dt_entrada_unidade_w		date;
dt_procedimento_w			date;
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w			varchar2(20);
cd_setor_atendimento_w		number(5);
cd_setor_receita_w		number(5);
nr_interno_conta_w		number(10);
cd_cgc_prestador_w		varchar2(14);
ie_tipo_ato_sus_w			number(3);
ie_tipo_servico_sus_w		number(3);
ie_responsavel_credito_w	varchar2(5);
ie_emite_conta_w			varchar2(3);
nr_seq_atepacu_w			Number(10,0);
ie_auditoria_w			varchar2(1);
qt_anos_w				Number(05);
qt_diarias_digitadas_w		Number(05);

BEGIN

begin
select	nvl(qt_dia_acompanhante,0),
		nr_aih
into		qt_dia_acompanhante_w,
		nr_aih_w
from		sus_aih
where		nr_atendimento	= nr_atendimento_p
and		nr_interno_conta	= nr_interno_conta_p;
exception
		when others then
		qt_dia_acompanhante_w := 0;
end;

qt_anos_w	:= 0;
if	(qt_dia_acompanhante_w	> 0) then
	begin
	select Somente_Numero(obter_idade(b.dt_nascimento, null,'A'))
	into	 qt_anos_w
	from	 pessoa_fisica b,
	 	 atendimento_paciente a
	where	 a.nr_atendimento		= nr_atendimento_p
	and	 a.cd_pessoa_fisica	= b.cd_pessoa_fisica;
	exception
     		when others then
           		qt_anos_w := 0;
	end;
end if;

qt_diarias_digitadas_w	:= 0;
if	(qt_dia_acompanhante_w	> 0) then
	begin
	select count(*)
	into	 qt_diarias_digitadas_w
	from	 procedimento_paciente
	where	 nr_interno_conta	= nr_interno_conta_p
	and	 cd_motivo_exc_conta is null
	and	 ie_origem_proced	= 2
	and	 cd_procedimento in(99080010,99081016);
	end;
end if;



/* Incluir procedimento 99080011-Diaria de acompanhante especial */
if	(qt_dia_acompanhante_w	> 0)		and
/*	(qt_anos_w < 18 or qt_anos_w >= 60)	and excluido pode ter acompanhante tipo excepcional os HST */
	(qt_diarias_digitadas_w	= 0)		then
	BEGIN
	/* Excluir anterior */
	begin
	delete from procedimento_paciente
	where		nr_interno_conta	= nr_interno_conta_p
	and		cd_procedimento	= 99080011;
	exception
			when others then
			qt_dia_acompanhante_w := qt_dia_acompanhante_w;
	end;	

	/* Buscar dados complementares */
	begin
	select	a.dt_entrada_unidade,
			a.dt_procedimento,
			a.cd_convenio,
			a.cd_categoria,
			a.nr_doc_convenio,
			a.cd_setor_atendimento,
			a.cd_setor_receita,
			a.nr_interno_conta,
			a.cd_cgc_prestador,
			a.ie_tipo_ato_sus,
			a.ie_tipo_servico_sus,
			a.ie_responsavel_credito,
			a.ie_emite_conta,
			a.nr_seq_atepacu,
			a.ie_auditoria
	into		dt_entrada_unidade_w,
			dt_procedimento_w,
			cd_convenio_w,
			cd_categoria_w,
			nr_doc_convenio_w,
			cd_setor_atendimento_w,
			cd_setor_receita_w,
			nr_interno_conta_w,
			cd_cgc_prestador_w,
			ie_tipo_ato_sus_w,
			ie_tipo_servico_sus_w,
			ie_responsavel_credito_w,
			ie_emite_conta_w,
			nr_seq_atepacu_w,
			ie_auditoria_w
	from		procedimento_paciente a,
			sus_laudo_paciente b
	where		a.cd_procedimento	= b.cd_procedimento_solic
	and		a.ie_origem_proced	= b.ie_origem_proced
	and		a.nr_atendimento	= b.nr_atendimento
	and		a.nr_interno_conta	= nr_interno_conta_p
	and		b.ie_tipo_laudo_sus	in (0,1)
	and		b.nr_atendimento	= nr_atendimento_p
	and		b.nr_interno_conta	= nr_interno_conta_p
	and		a.cd_motivo_exc_conta	is null;
	exception
			when others then
			select	dt_entrada_unidade,
				cd_setor_atendimento,
				nr_seq_atepacu,
				dt_procedimento
			into	dt_entrada_unidade_w,
				cd_setor_atendimento_w,
				nr_seq_atepacu_W,
				dt_procedimento_w
			from	procedimento_paciente
			where	nr_sequencia	= (	select	max(b.nr_sequencia)
							from	sus_laudo_paciente	c,
								procedimento_paciente	b
							where	b.cd_procedimento	= c.cd_procedimento_solic
							and	b.ie_origem_proced	= c.ie_origem_proced
							and	b.nr_atendimento	= c.nr_atendimento
							and	b.nr_interno_conta	= nr_interno_conta_p
							and	c.ie_tipo_laudo_sus	in (0,1)
							and	c.nr_atendimento	= nr_atendimento_p
							and	c.nr_interno_conta	= nr_interno_conta_p
							and	b.cd_motivo_exc_conta	is null);
	end;

	begin
    	select 	Procedimento_Paciente_Seq.nextval
     	into 		nr_sequencia_w
     	from 		dual;
	end;
	/* Incluir na procedimento_paciente */
	begin
	INSERT INTO PROCEDIMENTO_PACIENTE
			(NR_SEQUENCIA,
			NR_ATENDIMENTO,
			DT_ENTRADA_UNIDADE,
			CD_PROCEDIMENTO,
			DT_PROCEDIMENTO,
			QT_PROCEDIMENTO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			CD_MEDICO,                              
			CD_CONVENIO,                            
			CD_CATEGORIA,                           
			CD_PESSOA_FISICA,                       
			DT_PRESCRICAO,                          
			DS_OBSERVACAO,                          
			VL_PROCEDIMENTO,                        
			VL_MEDICO,                              
			VL_ANESTESISTA,                         
			VL_MATERIAIS,                           
			CD_EDICAO_AMB,                          
			CD_TABELA_SERVICO,                      
			DT_VIGENCIA_PRECO,                      
			CD_PROCEDIMENTO_PRINC,                  
			DT_PROCEDIMENTO_PRINC,                  
			DT_ACERTO_CONTA,                        
			DT_ACERTO_CONVENIO,                     
			DT_ACERTO_MEDICO,                       
			VL_AUXILIARES,                          
			VL_CUSTO_OPERACIONAL,                   
			TX_MEDICO,                              
			TX_ANESTESIA,                           
			NR_PRESCRICAO,                          
			NR_SEQUENCIA_PRESCRICAO,                
			CD_MOTIVO_EXC_CONTA,                    
			DS_COMPL_MOTIVO_EXCON,                  
			CD_ACAO,                                
			QT_DEVOLVIDA,                           
			CD_MOTIVO_DEVOLUCAO,                    
			NR_CIRURGIA,                            
			NR_DOC_CONVENIO,                        
			CD_MEDICO_EXECUTOR,                     
			IE_COBRA_PF_PJ,                         
			NR_LAUDO,                               
			DT_CONTA,                               
			CD_SETOR_ATENDIMENTO,                   
			CD_CONTA_CONTABIL,                      
			CD_PROCEDIMENTO_AIH,                    
			IE_ORIGEM_PROCED,                       
			NR_AIH,                                 
			IE_RESPONSAVEL_CREDITO,                 
			TX_PROCEDIMENTO,                        
			CD_EQUIPAMENTO,                         
			IE_VALOR_INFORMADO,                     
			CD_ESTABELECIMENTO_CUSTO,               
			CD_TABELA_CUSTO,                        
			CD_SITUACAO_GLOSA,                      
			NR_LOTE_CONTABIL,                       
			CD_PROCEDIMENTO_CONVENIO,               
			NR_SEQ_AUTORIZACAO,                     
			IE_TIPO_SERVICO_SUS,                    
			IE_TIPO_ATO_SUS,                        
			CD_CGC_PRESTADOR,                       
			NR_NF_PRESTADOR,                        
			CD_ATIVIDADE_PROF_BPA,                  
			NR_INTERNO_CONTA,                       
			NR_SEQ_PROC_PRINC,                      
			IE_GUIA_INFORMADA,                     
			DT_INICIO_PROCEDIMENTO,                 
			IE_EMITE_CONTA,                         
			IE_FUNCAO_MEDICO,                       
			IE_CLASSIF_SUS,                         
			CD_ESPECIALIDADE,                       
			NM_USUARIO_ORIGINAL,                    
			NR_SEQ_PROC_PACOTE,                     
			IE_TIPO_PROC_SUS,                       
			CD_SETOR_RECEITA,                       
			VL_ADIC_PLANT,
			nr_seq_atepacu, 
			ie_auditoria)
	VALUES
			(nr_sequencia_w,
			nr_atendimento_p,
			dt_entrada_unidade_w,
			99080011,
			dt_procedimento_w,
			qt_dia_acompanhante_w,
			dt_atualizacao_w,
			nm_usuario_p,
			null,
			cd_convenio_w,
			cd_categoria_w,
			null,
			null,
			null,
			0,
			0,
			0,
			0,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			0,
			0,
			100,
			100,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			nr_doc_convenio_w,
			null,
			null,
			null,
			dt_procedimento_w,
			cd_setor_atendimento_w,
			null,
			null,
			2,
			nr_aih_w,
			ie_responsavel_credito_w,
			100,
			null,
			'N',
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			cd_cgc_prestador_w,
			null,
			null,
			nr_interno_conta_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			cd_setor_receita_w,
			null,
			nr_seq_atepacu_w,
			ie_auditoria_w);
	exception
			when others then
			qt_dia_acompanhante_w := 0;
	end;
	
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

	if	(qt_dia_acompanhante_w > 0) then
		Atualiza_Preco_Proc_Sus(NR_SEQUENCIA_W,NM_USUARIO_P);
	end if;

	END;
end if;

END GRAVAR_PROC_ADICIONAL_SUS;
/
