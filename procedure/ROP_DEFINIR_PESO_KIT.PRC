create or replace
procedure rop_definir_peso_kit(	nr_seq_kit_rop_p		number,
				nm_usuario_p		Varchar2) is

nr_seq_roupa_w	number(10);
qt_roupa_w	number(15,2);

/*Retorna peso do KIT*/
qt_peso_w	number(13,4) := 0;

Cursor C01 is
	select	nr_seq_roupa,
		qt_roupa
	from	rop_componente_kit
	where	nr_seq_kit = nr_seq_kit_rop_p;

begin

open C01;
loop
fetch C01 into
	nr_seq_roupa_w,
	qt_roupa_w;
exit when C01%notfound;
	begin

	qt_peso_w := (qt_peso_w + (nvl(rop_obter_peso_roupa(nr_seq_roupa_w),0) * qt_roupa_w));

	end;
end loop;
close C01;

if	(qt_peso_w >= 0) then
	begin

	update	rop_kit_rouparia
	set	qt_peso 	= qt_peso_w
	where	nr_sequencia	= nr_seq_kit_rop_p;

	end;
end if;

commit;

end rop_definir_peso_kit;
/