create or replace
procedure pls_gerar_ocorr_glosa_aud_req
			(	nr_seq_auditoria_p	number,				
				nm_usuario_p		Varchar2) is 
				
nr_seq_requisicao_w		number(10);
nr_seq_proc_origem_w		number(10);
nr_seq_mat_origem_w		number(10);
nr_seq_proc_w 			number(10) := 0;
nr_seq_mat_w			number(10) := 0;
nr_seq_glosa_w			number(10);
nr_seq_aud_item_w		number(10);
nr_seq_ocorr_benef_w		number(10);
ie_tipo_w			varchar2(10);
nr_nivel_liberacao_w		number(2);
nr_seq_ocorrencia_w		number(10);
ie_auditoria_w			varchar2(1);
nr_seq_motivo_glosa_w		number(10);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_proc_origem,
		nr_seq_mat_origem
	from	pls_auditoria_item
	where	nr_seq_auditoria = nr_seq_auditoria_p
	order by nr_sequencia;
	
Cursor C02 is
	select	ie_tipo,
		nr_seq_glosa,
		nr_seq_ocorr_benef,
		nr_nivel_liberacao,
		nr_seq_ocorrencia
	from	(select	'G' ie_tipo,
			nr_sequencia nr_seq_glosa,
			null nr_seq_ocorr_benef,
			nr_seq_ocorrencia nr_ocorrencia,
			nr_seq_requisicao,
			nr_seq_req_proc nr_seq_proc,
			nr_seq_req_mat nr_seq_mat,
			0 nr_nivel_liberacao,
			null nr_seq_ocorrencia
		from	pls_requisicao_glosa
		where	nr_seq_execucao is null
		and	((nr_seq_requisicao	= nr_seq_requisicao_w)
		or	(nr_seq_req_proc	= nr_seq_proc_w)
		or	(nr_seq_req_mat		= nr_seq_mat_w))
		union
		select	'O' ie_tipo,
			null nr_seq_glosa,
			nr_sequencia nr_seq_ocorr_benef,
			null nr_ocorrencia,
			nr_seq_requisicao,
			nr_seq_proc nr_seq_proc,
			nr_seq_mat nr_seq_mat,
			nr_nivel_liberacao,
			nr_seq_ocorrencia
		from	pls_ocorrencia_benef
		where	nr_seq_conta is null
		and	nr_seq_guia_plano is null
		and	nr_seq_execucao is null
		and	nr_seq_requisicao	= nr_seq_requisicao_w
		and	((nr_seq_proc		= nr_seq_proc_w)
		or	(nr_seq_mat		= nr_seq_mat_w)
		or	((nr_seq_proc		is null)
		and	(nr_seq_mat		is null))))
	where	(nr_seq_requisicao	= nr_seq_requisicao_w or nr_seq_requisicao is null)
	and	((nr_seq_proc	= nr_seq_proc_w and nr_seq_proc is not null) or (nr_seq_proc_w = 0 and nr_seq_proc is null))
	and	((nr_seq_mat	= nr_seq_mat_w and nr_seq_mat is not null) or (nr_seq_mat_w = 0 and nr_seq_mat is null));

begin

select	nr_seq_requisicao
into	nr_seq_requisicao_w
from	pls_auditoria
where	nr_sequencia = nr_seq_auditoria_p;

open C02;
loop
fetch C02 into	
	ie_tipo_w,
	nr_seq_glosa_w,
	nr_seq_ocorr_benef_w,
	nr_nivel_liberacao_w,
	nr_seq_ocorrencia_w;
exit when C02%notfound;
	begin
	
	begin
		select	ie_auditoria,
			nr_seq_motivo_glosa
		into	ie_auditoria_w,
			nr_seq_motivo_glosa_w
		from	pls_ocorrencia
		where	nr_sequencia	= nr_seq_ocorrencia_w;
	exception
	when others then
		ie_auditoria_w	:= 'N';
	end;
	
	if	((ie_auditoria_w	= 'S') or ((ie_auditoria_w	= 'N')	and (nr_seq_motivo_glosa_w	is not null))) then
		nr_nivel_liberacao_w := nvl(nr_nivel_liberacao_w,0);
		
		insert into pls_analise_ocor_glosa_aut
			(nr_sequencia, ie_status, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_auditoria, nr_seq_aud_item, nr_seq_glosa,
			nr_seq_ocorrencia_benef, ie_tipo, nr_nivel_liberacao,
			nr_seq_ocorrencia)
		values	(pls_analise_ocor_glosa_aut_seq.nextval, 'P', sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_auditoria_p, null, nr_seq_glosa_w,
			nr_seq_ocorr_benef_w, ie_tipo_w, nr_nivel_liberacao_w,
			nr_seq_ocorrencia_w);
		
	elsif	(ie_auditoria_w = 	'N') then
		nr_nivel_liberacao_w := nvl(nr_nivel_liberacao_w,0);
	
		insert into pls_analise_ocor_glosa_aut
			(nr_sequencia, ie_status, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_auditoria, nr_seq_aud_item, nr_seq_glosa,
			nr_seq_ocorrencia_benef, ie_tipo, nr_nivel_liberacao,
			nr_seq_ocorrencia)
		values	(pls_analise_ocor_glosa_aut_seq.nextval, 'L', sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_auditoria_p, nr_seq_aud_item_w, nr_seq_glosa_w,
			nr_seq_ocorr_benef_w, ie_tipo_w, nr_nivel_liberacao_w,
			nr_seq_ocorrencia_w);			
	end if;
	end;
end loop;
close C02;

open C01;
loop
fetch C01 into	
	nr_seq_aud_item_w,
	nr_seq_proc_origem_w,
	nr_seq_mat_origem_w;
exit when C01%notfound;
	begin
	
	if	(nvl(nr_seq_proc_origem_w,0) > 0) then
		nr_seq_proc_w := nr_seq_proc_origem_w;
		nr_seq_mat_w := 0;	
		
	elsif	(nvl(nr_seq_mat_origem_w,0) > 0) then
		nr_seq_mat_w := nr_seq_mat_origem_w;
		nr_seq_proc_w := 0;
	
	end if;
	
	open C02;
	loop
	fetch C02 into
		ie_tipo_w,
		nr_seq_glosa_w,
		nr_seq_ocorr_benef_w,
		nr_nivel_liberacao_w,
		nr_seq_ocorrencia_w;
	exit when C02%notfound;
		begin
		begin
			select	ie_auditoria,
				nr_seq_motivo_glosa
			into	ie_auditoria_w,
				nr_seq_motivo_glosa_w
			from	pls_ocorrencia
			where	nr_sequencia	= nr_seq_ocorrencia_w;
		exception
		when others then
			ie_auditoria_w	:= 'N';
		end;
		
		if	((ie_auditoria_w	= 'S') or ((ie_auditoria_w	= 'N')	and (nr_seq_motivo_glosa_w	is not null))) then
			nr_nivel_liberacao_w := nvl(nr_nivel_liberacao_w,0);
			
			insert into pls_analise_ocor_glosa_aut
				(nr_sequencia, ie_status, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_auditoria, nr_seq_aud_item, nr_seq_glosa,
				nr_seq_ocorrencia_benef, ie_tipo, nr_nivel_liberacao,
				nr_seq_ocorrencia)
			values	(pls_analise_ocor_glosa_aut_seq.nextval, 'P', sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_auditoria_p, nr_seq_aud_item_w, nr_seq_glosa_w,
				nr_seq_ocorr_benef_w, ie_tipo_w, nr_nivel_liberacao_w,
				nr_seq_ocorrencia_w);
				
		elsif	(ie_auditoria_w = 	'N') then
			nr_nivel_liberacao_w := nvl(nr_nivel_liberacao_w,0);
		
			insert into pls_analise_ocor_glosa_aut
				(nr_sequencia, ie_status, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_auditoria, nr_seq_aud_item, nr_seq_glosa,
				nr_seq_ocorrencia_benef, ie_tipo, nr_nivel_liberacao,
				nr_seq_ocorrencia)
			values	(pls_analise_ocor_glosa_aut_seq.nextval, 'L', sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_auditoria_p, nr_seq_aud_item_w, nr_seq_glosa_w,
				nr_seq_ocorr_benef_w, ie_tipo_w, nr_nivel_liberacao_w,
				nr_seq_ocorrencia_w);		
		end if;		
		end;
	end loop;
	close C02;		
	
	end;
end loop;
close C01;	

end pls_gerar_ocorr_glosa_aud_req;
/
