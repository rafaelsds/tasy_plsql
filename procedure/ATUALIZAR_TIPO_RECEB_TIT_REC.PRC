create or replace 
procedure ATUALIZAR_TIPO_RECEB_TIT_REC
		(nr_interno_conta_p	in	number,
		nr_seq_protocolo_p	in	number,
		cd_tipo_recebimento_p	in	varchar2,
		nm_usuario_p		in	varchar2,
		nr_seq_lote_prot_p	in 	number default 0) is

cd_estabelecimento_w			number(4);
nr_titulo_w				numeric(10,0)		:= 0;
vl_vencimento_w				Number(15,2)		:= 0;
dt_emissao_p				date;	
dt_vencimento_w				date;
cd_pessoa_fisica_w			varchar2(10);
cd_cgc_w				varchar2(14);
ie_tipo_titulo_w			varchar2(5);
ie_situacao_w				varchar2(1);
nr_nota_fiscal_w			varchar2(255);
ie_integracao_nota_w			varchar2(1);
cd_estab_usuario_w			number(5) := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);

cursor c01 is
	select	cd_estabelecimento,
		nr_titulo,
		dt_emissao,
		dt_vencimento,
		cd_pessoa_fisica,
		cd_cgc,
		ie_tipo_titulo, 
		ie_situacao,
		nr_nota_fiscal,
		nvl(vl_titulo,0)
	from	titulo_receber
	where	((nr_interno_conta	= nr_interno_conta_p) or
		(nr_seq_protocolo	= nr_seq_protocolo_p and nr_seq_lote_prot_p = 0) or
		(nr_seq_lote_prot	= nr_seq_lote_prot_p));

begin
				
update	 titulo_receber
set	cd_tipo_recebimento	= cd_tipo_recebimento_p
where	cd_tipo_recebimento	is null
and	((nr_interno_conta	= nr_interno_conta_p) or
	(nr_seq_protocolo	= nr_seq_protocolo_p and nr_seq_lote_prot_p = 0) or
	(nr_seq_lote_prot	= nr_seq_lote_prot_p));

select 	nvl(max(ie_integracao_titulo),'X')
into 	ie_integracao_nota_w
from 	parametro_contas_receber
where	cd_estabelecimento = cd_estab_usuario_w;

if	(ie_integracao_nota_w = 'P') then
	begin
	
	open c01;
	loop
	fetch c01 into
		cd_estabelecimento_w,
		nr_titulo_w,
		dt_emissao_p,
		dt_vencimento_w,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		ie_tipo_titulo_w,
		ie_situacao_w,
		nr_nota_fiscal_w,
		vl_vencimento_w;
	exit when c01%notfound;
		
		exec_sql_dinamico('Tasy','begin pir_importar_titulo_receber(' 	
				|| nvl(nr_interno_conta_p,0) || ','
				|| nvl(nr_seq_protocolo_p,0) || ','
				|| cd_estabelecimento_w || ','
				|| nr_titulo_w || ','
				|| chr(39) || vl_vencimento_w || chr(39) || ','
				|| chr(39) || dt_emissao_p || chr(39) || ','
				|| chr(39) || dt_vencimento_w || chr(39) || ','
				|| chr(39) || nvl(cd_pessoa_fisica_w,'X')  || chr(39) || ','
				|| chr(39) || nvl(cd_cgc_w,'X') || chr(39) || ','
				|| chr(39) || ie_tipo_titulo_w || chr(39) || ','
				|| chr(39) || ie_situacao_w || chr(39) || ','
				|| chr(39) || nr_nota_fiscal_w || chr(39) || ','
				|| chr(39) || cd_tipo_recebimento_p || chr(39) || ','
				|| chr(39) || nm_usuario_p || chr(39) || '); end;');

	end loop;
	close c01;
	
	end;
end if;

commit;

end ATUALIZAR_TIPO_RECEB_TIT_REC;
/
