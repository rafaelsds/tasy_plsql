create or replace
procedure aprovar_proj_rat_mes
			(	nr_sequencia_p		number,
				ie_opcao_p		Varchar2,
				nm_usuario_p		Varchar2) is 

begin

if	(ie_opcao_p	= 'F') then
	update	proj_rat_mes
	set	dt_aprovacao_fin	= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p;
elsif	(ie_opcao_p	= 'C') then
	update	proj_rat_mes
	set	dt_aprov_consultor	= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p;
end if;

commit;

end aprovar_proj_rat_mes;
/