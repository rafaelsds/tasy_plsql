create or replace
procedure pac_validar_envio_sms_ccih(	nr_atendimento_p		number,
				nr_seq_evento_p		out number,
				cd_medico_resp_p		out varchar2,
				nm_medico_resp_p		out varchar2,
				ds_titulo_p		out varchar2,
				ds_mensagem_p		out varchar2) is 

nr_seq_evento_w		number(10);
cd_medico_resp_w		varchar2(10)	:= '';
nm_medico_resp_w		varchar2(255)	:= '';
ds_titulo_w		varchar2(100)	:= '';
ds_mensagem_w		varchar2(4000)	:= '';
			

begin
/*
Obs: Procedure criada especificamente para utiliza��o no Tasy Swing, para evitar  acessos desnecess�rios ao Servidor
*/
nr_seq_evento_w		:= obter_evento_regra_envio('GCIH');
cd_medico_resp_w		:= obter_medico_resp_atend(nr_atendimento_p, 'C'); --Buscar o m�dico respons�vel pelo atendimento
nm_medico_resp_w		:= obter_nome_pf(cd_medico_resp_w); --Buscar o nome do m�dico

select	max(a.ds_titulo),
	max(a.ds_mensagem)
into	ds_titulo_w,
	ds_mensagem_w
from	ev_evento a
where	a.nr_sequencia	= nr_seq_evento_w;

nr_seq_evento_p		:= nr_seq_evento_w;
cd_medico_resp_p		:= cd_medico_resp_w;
nm_medico_resp_p		:= nm_medico_resp_w;
ds_titulo_p		:= ds_titulo_w;
ds_mensagem_p		:= ds_mensagem_w;

end pac_validar_envio_sms_ccih;
/