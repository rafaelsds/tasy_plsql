create or replace
procedure atualizar_man_equip_parado(
		nr_seq_equip_p	number,
		nr_seq_motivo_p	number,
		nr_seq_motivo_parado_p number,
		dt_inicial_p	date,
		dt_final_p	date,
		nm_usuario_p	varchar2) is
begin
if	(nm_usuario_p is not null) then
	begin

                         insert into man_equip_periodo_parado(         
                                nr_sequencia,                          
                                nr_seq_equipamento,                    
                                dt_periodo_inicial,                    
                                dt_periodo_final,                      
                                dt_atualizacao,                        
                                dt_atualizacao_nrec,                   
                                nm_usuario,                            
                                nm_usuario_nrec,                       
                                nr_seq_motivo,                         
                                nr_seq_reg_parada_os)                  
                         values(man_equip_periodo_parado_seq.nextval,   
                                nr_seq_equip_p,                          
                                dt_inicial_p,             
                                dt_final_p,           
                                sysdate,                                
				sysdate,                                
                                nm_usuario_p,                            
                                nm_usuario_p,                            
                                nr_seq_motivo_p,                         
                                nr_seq_motivo_parado_p);
	end;
end if;
commit;
end atualizar_man_equip_parado;
/