create or replace
procedure gerar_cobr_febra_bb_240_v084
		(	nr_seq_cobr_escrit_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is 

-- OS 400914
-- Layout Padr�o Febraban 240 posi��es V08.4  
-- http://www.febraban.org.br
-- BB
			
ds_conteudo_w			varchar2(240);
nr_remessa_w			varchar2(8);
qt_lote_arquivo_w		varchar2(6) := 0;
qt_reg_lote_w			varchar2(6) := 0;
nr_seq_arquivo_w		varchar2(6) := 0;
nr_seq_registro_w		varchar2(5) := 0;
nm_empresa_w			varchar2(30);
cd_cgc_w			varchar2(15);
nm_banco_w			varchar2(30);
dt_geracao_w			varchar2(8);
nr_lote_w_w			varchar2(4) := 0;
nr_lote_w			varchar2(4);
cd_convenio_banco_w		varchar2(20);
cd_agencia_bancaria_w		varchar2(5);
nr_digito_agencia_w		varchar2(1);
nr_conta_corrente_w		varchar2(13);
cd_banco_w			varchar2(3);

/* Segmentos */
nr_nosso_numero_w		varchar2(20);
ie_documento_w			varchar2(1);
dt_vencimento_w			varchar2(8);
vl_titulo_w			varchar2(15);
ie_titulo_w			varchar2(2);
dt_emissao_w			varchar2(8);
cd_juros_mora_w			varchar2(1);
dt_juros_mora_w			varchar2(8);
vl_juros_diario_w		varchar2(15);
cd_desconto_w			varchar2(1);
dt_desconto_w			varchar2(8);
vl_desconto_dia_w		varchar2(15);
vl_iof_w			varchar2(15);
vl_abatimento_w			varchar2(15);
ds_ident_titulo_emp_w		varchar2(25);
cd_protesto_w			varchar2(1);
qt_dias_protestos_w		varchar2(2);
cd_moeda_w			varchar2(2);
cd_agencia_w			varchar2(5);
ie_digito_agencia_w		varchar2(1);
nr_conta_w			varchar2(12);
ie_digito_conta_w		varchar2(1);
cd_conta_cobr_w			varchar2(9);
ie_conta_cobr_w			varchar2(1);
ie_tipo_cobranca_w		varchar2(1);
ie_forma_cad_w			varchar2(1);
nr_seu_numero_w			varchar2(15);
cd_agencia_cobr_w		varchar2(5);
ie_agencia_cobr_w		varchar2(1);
ie_ident_titulo_w		varchar2(1);
cd_baixa_w			varchar2(1);
nr_dias_baixa_w			varchar2(3);
ie_tipo_inscricao_w		varchar2(1);
nr_inscricao_w			varchar2(15);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
ds_bairro_sacado_w		varchar2(15);
cd_cep_sacado_w			varchar2(8);
ds_municipio_sacado_w		varchar2(15);
ds_estado_sacado_w		varchar2(2);
ds_avalista_w			varchar2(40);
nr_avalista_w			varchar2(16);
cd_mov_remessa_w		varchar2(2);
ie_carne_w			varchar2(3);
nr_parcela_w			varchar2(3);
qt_total_parcela_w		varchar2(3);
nr_plano_w			varchar2(3);

/* Ds_Brancos */
ds_brancos_8_w			varchar2(8);
ds_brancos_25_w			varchar2(25);
ds_brancos_10_w			varchar2(10);
ds_brancos_6_w			varchar2(6);
ds_brancos_74_w			varchar2(74);
ds_brancos_2_w			varchar2(2);
ds_brancos_1_w			varchar2(1);
ds_brancos_20_w			varchar2(20);
ds_brancos_5_w			varchar2(5);
ds_brancos_41_w			varchar2(41);
ds_brancos_9_w			varchar2(9);
ds_brancos_217_w		varchar2(217);
ds_brancos_211_w		varchar2(211);
ds_brancos_19_w			varchar2(19);
ds_brancos_11_w			varchar2(11);
ds_brancos_23_w			varchar2(23);
ds_mensagen_1_w			varchar2(40);
ds_mensagen_2_w			varchar2(40);

/* Segmento P */
cursor C01 is
	select	lpad(x.cd_banco,3,'0') cd_banco,
		rpad(nvl(substr(c.cd_ocorrencia,1,2),'0'),2,'0') cd_mov_remessa, 
		lpad(to_char(c.cd_agencia_bancaria),5,'0') cd_agencia,
		rpad(substr(nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),'0'),1,1),1,'0') ie_digito_agencia,
		lpad(substr(c.nr_conta,1,12),12,'0') nr_conta,
		lpad(nvl(substr(c.ie_digito_conta,1,1),'0'),1,'0') ie_digito_conta,
		lpad(substr(x.cd_conta,1,9),9,'0') cd_conta_cobr,
		rpad(nvl(substr(x.ie_digito_conta,1,1),'0'),1,'0') ie_conta_cobr,
		lpad(nvl(substr(obter_nosso_numero_interf(x.cd_banco,b.nr_titulo),1,20),'0'),20,'0') nr_nosso_numero,		
		'5' ie_tipo_cobranca,
		'1' ie_forma_cad,
		'2' ie_documento,
		rpad(substr(b.nr_titulo,1,15),15,'0') nr_seu_numero,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyyyy') dt_vencimento,
		lpad(nvl(elimina_caracteres_especiais(b.vl_titulo),0),15,'0') vl_titulo,
		'00000' cd_agencia_cobr,
		'0' ie_agencia_cobr,
		'04' ie_titulo,
		'N' ie_ident_titulo,
		to_char(b.dt_emissao,'ddmmyyyy') dt_emissao,
		'0' cd_juros_mora,
		'00000000' dt_juros_mora,
		lpad(nvl(elimina_caracteres_especiais(obter_vl_juros_diario_tit(null,b.nr_titulo)),0),15,'0') vl_juros_diario,
		'0' cd_desconto,
		to_char(sysdate,'ddmmyyyy') dt_desconto,
		lpad('0',15,'0') vl_desconto_dia,
		lpad('0',15,'0') vl_iof,
		lpad('0',15,'0') vl_abatimento,
		lpad(' ',25,' ') ds_ident_titulo_emp,
		'0' cd_protesto,
		'00' qt_dias_protestos,
		'3' cd_baixa,
		'000' nr_dias_baixa,
		lpad(b.cd_moeda,2,'0') cd_moeda,
/* Segmento Q */	decode(b.cd_cgc, null,'1','2') ie_tipo_inscricao, 
		lpad(nvl(b.cd_cgc_cpf,'0'),15,'0') nr_inscricao,
		rpad(upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))),40,' ') nm_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'E')),1,40),40,' ') ds_endereco_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,15),15,' ') ds_bairro_sacado,
		lpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8),8,'0') cd_cep_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15),15,' ') ds_municipio_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),2,' ') ds_estado_sacado,
		lpad('0',16,'0') nr_avalista,
		rpad(' ',40,' ') ds_avalista,
		'000' ie_carne,
		'000' nr_parcela,
		'000' qt_total_parcela,
		'000' nr_plano	
	from	banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	rpad(' ',8,' '),
	rpad(' ',25,' '),
	rpad(' ',10,' '),
	rpad(' ',6,' '),
	rpad(' ',74,' '),
	rpad(' ',2,' '),
	rpad(' ',1,' '),
	rpad(' ',20,' '),
	rpad(' ',5,' '),
	rpad(' ',41,' '),
	lpad(' ',9,' '),
	rpad(' ',217,' '),
	rpad(' ',211,' '),
	rpad(' ',19,' '),
	rpad(' ',11,' '),
	rpad(' ',40,' '),
	rpad(' ',40,' '),
	lpad('0',23,'0')
into	ds_brancos_8_w,
	ds_brancos_25_w,
	ds_brancos_10_w,
	ds_brancos_6_w,
	ds_brancos_74_w,
	ds_brancos_2_w,
	ds_brancos_1_w,
	ds_brancos_20_w,
	ds_brancos_5_w,
	ds_brancos_41_w,
	ds_brancos_9_w,
	ds_brancos_217_w,
	ds_brancos_211_w,
	ds_brancos_19_w,
	ds_brancos_11_w,
	ds_mensagen_1_w,
	ds_mensagen_2_w,
	ds_brancos_23_w
from	dual;

/* Header Lote */
nr_lote_w_w	:= nr_lote_w_w + 1;
nr_lote_w	:= lpad(nr_lote_w_w,4,'0');

select	lpad(b.cd_cgc,15,'0') cd_cgc,
	rpad(substr(obter_nome_banco(c.cd_banco),1,30),30,' ') nm_banco,
	lpad(substr(a.nr_remessa,1,8),8,'0') nr_remessa,
	to_char(sysdate, 'ddmmyyyy') dt_geracao,
	rpad(substr(elimina_acentuacao(obter_nome_pf_pj(null,b.cd_cgc)),1,30),30,' ') nm_empresa,
	rpad(nvl(d.cd_convenio_banco,c.cd_convenio_banco),20,'0') cd_convenio_banco,
	lpad(nvl(c.cd_agencia_bancaria,'0'),5,0) cd_agencia_bancaria,
	rpad(nvl(calcula_digito('Modulo11',c.cd_agencia_bancaria),'0'),1,'0') nr_digito_agencia,
	lpad(nvl(lpad(substr(c.cd_conta,1,11),11,'0') || rpad(c.ie_digito_conta,2,'0'),'0'),13,'0') nr_conta_corrente,
	lpad(c.cd_banco,3,'0') cd_banco
into	cd_cgc_w,
	nm_banco_w,
	nr_remessa_w,
	dt_geracao_w,
	nm_empresa_w,
	cd_convenio_banco_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	cd_banco_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	cd_banco_w || nr_lote_w || '1' || 'R' || '01' || ds_brancos_2_w || '043' || ds_brancos_1_w || '2' || cd_cgc_w ||
			cd_convenio_banco_w || cd_agencia_bancaria_w || nr_digito_agencia_w || nr_conta_corrente_w || ' ' || 
			nm_empresa_w || ds_mensagen_1_w || ds_mensagen_2_w || nr_remessa_w || dt_geracao_w || ds_brancos_41_w;

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	2,
	2);

qt_reg_lote_w	:= qt_reg_lote_w + 1;
/* Fim Header Lote */

/* Inicio Segmentos */
open C01;
loop
fetch C01 into	
	cd_banco_w,
	cd_mov_remessa_w,
	cd_agencia_w,
	ie_digito_agencia_w,
	nr_conta_w,
	ie_digito_conta_w,
	cd_conta_cobr_w,
	ie_conta_cobr_w,
	nr_nosso_numero_w,
	ie_tipo_cobranca_w,
	ie_forma_cad_w,
	ie_documento_w,
	nr_seu_numero_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_agencia_cobr_w,
	ie_agencia_cobr_w,
	ie_titulo_w,
	ie_ident_titulo_w,
	dt_emissao_w,
	cd_juros_mora_w,
	dt_juros_mora_w,
	vl_juros_diario_w,
	cd_desconto_w,
	dt_desconto_w,
	vl_desconto_dia_w,
	vl_iof_w,
	vl_abatimento_w,
	ds_ident_titulo_emp_w,
	cd_protesto_w,
	qt_dias_protestos_w,
	cd_baixa_w,
	nr_dias_baixa_w,
	cd_moeda_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	ds_bairro_sacado_w,
	cd_cep_sacado_w,
	ds_municipio_sacado_w,
	ds_estado_sacado_w,
	nr_avalista_w,
	ds_avalista_w,
	ie_carne_w,
	nr_parcela_w,
	qt_total_parcela_w,
	nr_plano_w;
exit when C01%notfound;
	begin
	/* Segmento P*/
	nr_seq_registro_w:= nr_seq_registro_w + 1;

	ds_conteudo_w	:=	cd_banco_w || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'P' || ds_brancos_1_w || cd_mov_remessa_w || 
				cd_agencia_w || ie_digito_agencia_w || nr_conta_w || ie_digito_conta_w || ie_conta_cobr_w || nr_nosso_numero_w || 
				ie_tipo_cobranca_w || ie_forma_cad_w || ie_documento_w || ds_brancos_2_w || nr_seu_numero_w || dt_vencimento_w || 
				vl_titulo_w || cd_agencia_cobr_w || ie_agencia_cobr_w || ie_titulo_w || ie_ident_titulo_w || 
				dt_emissao_w || cd_juros_mora_w || dt_juros_mora_w || vl_juros_diario_w || cd_desconto_w || dt_desconto_w || 
				vl_desconto_dia_w || vl_iof_w || vl_abatimento_w || ds_ident_titulo_emp_w || cd_protesto_w || qt_dias_protestos_w ||
				cd_baixa_w || nr_dias_baixa_w || cd_moeda_w || ds_brancos_11_w; 
	
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);
	/* Fim segmento P*/

	/* Segmento Q*/
	ds_conteudo_w	 :=	null;
	nr_seq_registro_w:= 	nr_seq_registro_w + 1;
	
	ds_conteudo_w	:=	cd_banco_w || nr_lote_w || '3' || lpad(nr_seq_registro_w,5,'0') || 'Q' || ds_brancos_1_w || cd_mov_remessa_w || 
				ie_tipo_inscricao_w || nr_inscricao_w || nm_sacado_w || ds_endereco_sacado_w || ds_bairro_sacado_w ||
				cd_cep_sacado_w || ds_municipio_sacado_w || ds_estado_sacado_w || nr_avalista_w || ds_avalista_w || '000' ||
				ds_brancos_20_w || ds_brancos_8_w;
			
				
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);
		
	/* Fim segmento Q*/
	qt_reg_lote_w	:= qt_reg_lote_w + 1;
	end;	
				
end loop;
close C01;
/*Fim Segmentos */

/* Trailler Lote*/
begin
select	lpad(c.cd_banco,3,'0') cd_banco
into	cd_banco_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
ds_conteudo_w	:= cd_banco_w || nr_lote_w || '5' || ds_brancos_9_w || lpad(qt_reg_lote_w,6,'0') || ds_brancos_217_w; 

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		5,
		5);
		
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_reg_lote_w 		:= qt_reg_lote_w + 1;
end;
/* Fim Trailler Lote*/

commit;

end gerar_cobr_febra_bb_240_v084;
/