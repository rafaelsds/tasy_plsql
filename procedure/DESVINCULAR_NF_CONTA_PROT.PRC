create or replace 
procedure desvincular_nf_conta_prot(	nr_seq_protocolo_p	Number,
				nm_usuario_p	 Varchar2) is

nr_seq_nf_w		number(10,0);
nr_interno_conta_w	number(10,0);

cursor c01 is
	select	nr_sequencia,
		nr_interno_conta
	from	nota_fiscal
	where	nr_seq_protocolo = nr_seq_protocolo_p
	and 	ie_situacao in ('2','3') -- Estorno e Estornadas
	order by 1;

begin

open C01;
loop
fetch C01 into	
	nr_seq_nf_w,
	nr_interno_conta_w;
	exit when C01%notfound;
	begin
		
	update	nota_fiscal
	set	nr_seq_protocolo 	= null,
		nr_interno_conta	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia 		= nr_seq_nf_w;	
		
	gerar_historico_nota_fiscal(nr_seq_nf_w, nm_usuario_p, '57', wheb_mensagem_pck.get_texto(311441,'NR_SEQ_PROTOCOLO_P='|| NR_SEQ_PROTOCOLO_P ||';NR_INTERNO_CONTA_W='|| NR_INTERNO_CONTA_W)); /*'Protocolo= ' || nr_seq_protocolo_p || '; Conta= ' || nr_interno_conta_w);*/
				
	end;
	end loop;
close C01;


commit;

end desvincular_nf_conta_prot;
/
