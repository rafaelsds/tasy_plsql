create or replace
procedure gerar_menu_cron_migr_dic_obj4 (
		cd_funcao_p	number,
		nr_seq_menu_p	number,
		nm_usuario_p	varchar2) is
		
nr_seq_cronograma_w	number(10,0);
nr_seq_atividade_w	number(10,0);
nr_seq_apresent_w	number(10,0);
nr_seq_atividade_ww	number(10,0);
nr_seq_atividade_www	number(10,0);
nm_menu_w		varchar2(255);
nr_seq_menu_w		number(10,0);
nm_item_w		varchar2(255);
nr_seq_item_w		number(10,0);
		
cursor c01 is		
select	a.nm_objeto,
	a.nr_sequencia
from	dic_objeto a
where	a.cd_funcao = cd_funcao_p
and	a.ie_tipo_objeto = 'M'
and	a.nr_sequencia = nvl(nr_seq_menu_p,a.nr_sequencia)
group by
	a.nm_objeto,
	a.nr_sequencia
order by
	a.nm_objeto,
	a.nr_sequencia;
	
cursor c02 is		
select	a.nm_objeto,
	a.nr_sequencia
from	dic_objeto a
where	a.cd_funcao = cd_funcao_p
and	a.nr_seq_obj_sup = nr_seq_menu_w
and	a.ie_tipo_objeto = 'MI'
group by
	a.nm_objeto,
	a.nr_sequencia
order by
	a.nm_objeto,
	a.nr_sequencia;
	
begin
if	(cd_funcao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	nvl(max(c.nr_sequencia),0)
	into	nr_seq_cronograma_w
	from	proj_cronograma c,
		proj_projeto p
	where	c.nr_seq_proj = p.nr_sequencia
	and	c.ie_tipo_cronograma = 'E'
	and	p.cd_funcao = cd_funcao_p;
	
	if	(nr_seq_cronograma_w > 0) then
		begin
		select	nvl(min(a.nr_sequencia),0),
			nvl(max(a.nr_seq_apres),0)
		into	nr_seq_atividade_w,
			nr_seq_apresent_w
		from	proj_cron_etapa a
		where	a.nr_seq_cronograma = nr_seq_cronograma_w
		and	a.ie_tipo_obj_proj_migr = 'TMenuItem';
		
		if	(nr_seq_atividade_w > 0) then
			begin
			open c01;
			loop
			fetch c01 into	nm_menu_w,
					nr_seq_menu_w;
			exit when c01%notfound;
				begin
				nr_seq_apresent_w := nr_seq_apresent_w + 10;
				
				select	proj_cron_etapa_seq.nextval
				into	nr_seq_atividade_ww
				from	dual;
				
				insert into proj_cron_etapa (
					nr_seq_cronograma,
					nr_sequencia,
					ds_atividade,
					ie_fase,
					qt_hora_prev,
					pr_etapa,
					nr_seq_apres,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_modulo,
					ie_tipo_obj_proj_migr,
					nr_seq_superior,
					ie_atividade_adicional)
				values (
					nr_seq_cronograma_w,
					nr_seq_atividade_ww,
					nm_menu_w,
					'S',
					0,
					0,
					nr_seq_apresent_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					'N',
					'TMenuItem',
					nr_seq_atividade_w,
					'S');
					
				open c02;
				loop
				fetch c02 into	nm_item_w,
						nr_seq_item_w;
				exit when c02%notfound;
					begin
					nr_seq_apresent_w := nr_seq_apresent_w + 10;
					
					select	proj_cron_etapa_seq.nextval
					into	nr_seq_atividade_www
					from	dual;
					
					insert into proj_cron_etapa (
						nr_seq_cronograma,
						nr_sequencia,
						ds_atividade,
						ie_fase,
						qt_hora_prev,
						pr_etapa,
						nr_seq_apres,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_modulo,
						ie_tipo_obj_proj_migr,
						nr_seq_superior,
						ie_atividade_adicional)
					values (
						nr_seq_cronograma_w,
						nr_seq_atividade_www,
						nm_item_w,
						'N',
						1,
						0,
						nr_seq_apresent_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						'N',
						'TMenuItem',
						nr_seq_atividade_ww,
						'S');						
					atualizar_horas_etapa_cron(nr_seq_atividade_www);		
					end;
				end loop;
				close c02;					
				atualizar_horas_etapa_cron(nr_seq_atividade_ww);		
				end;
			end loop;
			close c01;
			atualizar_horas_etapa_cron(nr_seq_atividade_w);
			end;
		end if;
		atualizar_total_horas_cron(nr_seq_cronograma_w);
		gerar_classif_etapa_proj(nr_seq_cronograma_w, nm_usuario_p);			
		end;
	end if;
	end;
end if;
commit;
end gerar_menu_cron_migr_dic_obj4;
/