create or replace
procedure sus_calcula_valor_sh	(	nr_interno_conta_p	number,
					nm_usuario_p		varchar2) is

nr_sequencia_w		number(10);
nr_interno_conta_w	number(10);
cd_item_w		number(15);
ie_origem_proced_w	number(10);
vl_preco_w		number(15,2);
vl_receita_w		number(15,2);
qt_item_w		number(15,4);
nr_seq_propaci_w	number(10);
nr_seq_matpaci_w	number(10);

cd_estabelecimento_w	number(4);
cd_setor_atendimento_w	number(5);
cd_setor_atend_ant_w	number(5);
cd_item_ant_w		number(15);
dt_item_w		date;

vl_fixo_w		number(15,4);
cd_convenio_w		number(5);
cd_categoria_w		number(5);
ie_custo_medio_mat_w	varchar2(15) := 'N';
nr_seq_proc_interno_w	w_conta_sus_sh.nr_seq_proc_interno%type;
vl_proc_interno_w	number(15,4) := 0;
vl_proc_interno_ww	number(15,4) := 0;
cd_proc_princ_w		procedimento.cd_procedimento%type;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
nr_seq_interno_w	sus_laudo_paciente.nr_seq_interno%type;
qt_proc_int_w		number(10) := 0;

cursor c01 is
	select	a.nr_sequencia,
		a.cd_item,
		a.qt_item,
		a.nr_seq_matpaci,
		a.dt_item,
		a.cd_setor_atendimento
	from	w_conta_sus_sh a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	a.nr_seq_matpaci	is not null
	order by
		a.cd_item,
		a.cd_setor_atendimento;

cursor c02 is
	select	a.nr_sequencia,
		a.cd_item,
		a.qt_item,
		a.nr_seq_propaci,
		a.dt_item,
		a.cd_setor_atendimento,
		a.ie_origem_proced,
		a.nr_seq_proc_interno
	from	w_conta_sus_sh a
	where	a.nr_interno_conta	= nr_interno_conta_p
	and	a.nr_seq_propaci 	is not null
	and	nvl(a.cd_registro,0)	<> 3
	order by
		a.cd_item,
		a.cd_setor_atendimento;

Cursor C03 is
	select	b.cd_procedimento,
		b.ie_origem_proced
	from	proc_interno a,
		proc_interno_conv b
	where	a.nr_sequencia = b.nr_seq_proc_interno
	and	a.nr_sequencia = nr_seq_proc_interno_w;
	
type 		fetch_array is table of c01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c01_w			vetor;

type 		fetch_array02 is table of c02%rowtype;
w_array 	fetch_array02;
n		integer := 1;
type vetor02 is table of fetch_array02 index by binary_integer;
vetor_c02_w			vetor02;
		
begin

/* obter estabelecimento da conta paciente */
begin
select	cd_estabelecimento,
	nr_atendimento
into	cd_estabelecimento_w,
	nr_atendimento_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;
exception
	when others then
	--R.aise_application_error(-20011,'Problama ao obter o estabelecimento da conta: '||nr_interno_conta_p||' #@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263437,'nr_interno_conta_p='||nr_interno_conta_p);
	end;

ie_custo_medio_mat_w	:= nvl(obter_valor_param_usuario(1123,165,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w),'N');	
	
/* Atualizar dados matmed */
cd_setor_atend_ant_w	:= 0;
cd_item_ant_w		:= 0;

open c01;
loop
fetch c01 bulk collect into s_array limit 100000;
	vetor_c01_w(i) := s_array;
	i := i + 1;
exit when c01%notfound;
end loop;
close c01;

for i in 1..vetor_c01_w.count loop
	begin
	s_array := vetor_c01_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_sequencia_w			:= s_array(z).nr_sequencia;
		cd_item_w			:= s_array(z).cd_item;
		qt_item_w			:= s_array(z).qt_item;
		nr_seq_matpaci_w		:= s_array(z).nr_seq_matpaci;
		dt_item_w			:= s_array(z).dt_item;
		cd_setor_atendimento_w		:= s_array(z).cd_setor_atendimento;
		
		if	(cd_setor_atendimento_w <> cd_setor_atend_ant_w) or
			(cd_item_w		<> cd_item_ant_w)	then
			begin
			
			cd_setor_atend_ant_w	:= cd_setor_atendimento_w;
			cd_item_ant_w		:= cd_item_w;
			
			sus_obter_rateio_sh(cd_estabelecimento_w, cd_item_w, null, null, cd_setor_atendimento_w, cd_convenio_w, cd_categoria_w,vl_fixo_w,null);
			
			if	(vl_fixo_w	is null) or
				(vl_fixo_w	= '') then
				begin			
				if	(nvl(ie_custo_medio_mat_w,'N') = 'S') then					
					vl_fixo_w	:= nvl(Obter_custo_medio_material(cd_estabelecimento_w, ESTABLISHMENT_TIMEZONE_UTILS.startOfmonth(dt_item_w), cd_item_w),0);					
				else
					vl_fixo_w	:= Obter_Preco_Material(cd_estabelecimento_w,cd_convenio_w,cd_categoria_w,dt_item_w,cd_item_w,0,0,cd_setor_atendimento_w,null,0,0);
				end if;
				end;
			end if;
			end;
		end if;
	
		if	(vl_fixo_w	> 0) then
			begin
			update 	w_conta_sus_sh
			set	vl_preco = (vl_fixo_w * qt_item_w)
			where	nr_sequencia	= nr_sequencia_w;
			end;
		end if;
	
		end;
	end loop;
	end;
end loop;

/* atualizar dados propaci */
cd_setor_atend_ant_w	:= 0;
cd_item_ant_w		:= 0;

open c02;
loop
fetch c02 bulk collect into w_array limit 100000;
	vetor_c02_w(n) := w_array;
	n := n + 1;
exit when c02%notfound;
end loop;
close c02;

vl_fixo_w := 0;

for j in 1..vetor_c02_w.count loop
	begin
	w_array := vetor_c02_w(j);
	for w in 1..w_array.count loop
		begin
		
		nr_sequencia_w			:= w_array(w).nr_sequencia;
		cd_item_w			:= w_array(w).cd_item;
		qt_item_w			:= w_array(w).qt_item;
		nr_seq_propaci_w			:= w_array(w).nr_seq_propaci;
		dt_item_w			:= w_array(w).dt_item;
		cd_setor_atendimento_w		:= w_array(w).cd_setor_atendimento;
		ie_origem_proced_w		:= w_array(w).ie_origem_proced;
		nr_seq_proc_interno_w		:= w_array(w).nr_seq_proc_interno;
	
		if	(cd_setor_atendimento_w <> cd_setor_atend_ant_w) or
			(cd_item_w		<> cd_item_ant_w)	then
			begin
			
			cd_setor_atend_ant_w	:= cd_setor_atendimento_w;
			cd_item_ant_w		:= cd_item_w;
			
			if	(nvl(nr_seq_proc_interno_w,0) <> 0) then
				begin

				select	count(1)
				into	qt_proc_int_w
				from	proc_interno a,
					proc_interno_conv b
				where	a.nr_sequencia = b.nr_seq_proc_interno
				and	a.nr_sequencia = nr_seq_proc_interno_w
				and	rownum = 1;
					
				if (qt_proc_int_w > 0) then
					begin
					vl_proc_interno_w:=0;
					open C03;
					loop
					fetch C03 into	
						cd_item_w,
						ie_origem_proced_w;
					exit when C03%notfound;
						begin
						
						cd_convenio_w := null;
						cd_categoria_w := null;
						
						sus_obter_rateio_sh(cd_estabelecimento_w, null, cd_item_w, ie_origem_proced_w, cd_setor_atendimento_w, cd_convenio_w, cd_categoria_w, vl_fixo_w, null);
						
						if	((vl_fixo_w	is null) or
							(vl_fixo_w	= '')) and
							(cd_convenio_w is not null) then			
							vl_proc_interno_ww	:= obter_preco_procedimento(cd_estabelecimento_w,
													cd_convenio_w,
													cd_categoria_w,
													dt_item_w,
													cd_item_w,
													ie_origem_proced_w,
													0,0,0,0,0,
													null,null,null,null,'P');
								
							if	(vl_proc_interno_ww > vl_proc_interno_w) then
								vl_proc_interno_w := vl_proc_interno_ww;
							end if;					
						end if;
						
						end;
					end loop;
					close C03;
				
					if	(vl_proc_interno_w > 0) then
						vl_fixo_w := vl_proc_interno_w;
					end if;
				
					end;
				else
					sus_obter_rateio_sh(cd_estabelecimento_w, null, null, null, cd_setor_atendimento_w, cd_convenio_w, cd_categoria_w, vl_fixo_w, nr_seq_proc_interno_w);
					if 	(vl_fixo_w is null) or
						(vl_fixo_w = '') then
						begin
						vl_fixo_w := 0;
						end;
					end if;
					
				end if;	
				
				end;
			else
				begin
		
				sus_obter_rateio_sh(cd_estabelecimento_w, null, cd_item_w, ie_origem_proced_w, cd_setor_atendimento_w, cd_convenio_w, cd_categoria_w, vl_fixo_w, null);
			
				if	(vl_fixo_w	is null) or
					(vl_fixo_w	= '') then			
					vl_fixo_w	:= obter_preco_procedimento(cd_estabelecimento_w,
										cd_convenio_w,
										cd_categoria_w,
										dt_item_w,
										cd_item_w,
										ie_origem_proced_w,
										0,0,0,0,0,
										null,null,null,null,'P');
				end if;
				end;
			end if;
				
			end;
		end if;
		
		if	(vl_fixo_w	> 0) then
			begin		
			update 	w_conta_sus_sh
			set	vl_preco = (vl_fixo_w * qt_item_w)
			where	nr_sequencia	= nr_sequencia_w;
			end;
		end if;
	
		end;
	end loop;
	end;
end loop;

end sus_calcula_valor_sh;
/