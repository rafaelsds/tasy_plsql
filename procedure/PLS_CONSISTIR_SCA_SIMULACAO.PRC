create or replace
procedure pls_consistir_sca_simulacao
		(	nr_seq_pessoa_simul_p	number,
			nr_seq_plano_sca_p	number,
			nr_seq_tabela_p		number,
			ds_erro_p	out	varchar2) is 

ie_proposta_adesao_w	varchar2(2);
qt_idade_sca_w		number(10);
qt_idade_benef_w	number(10);
ds_erro_w		varchar2(4000);
qt_retorno_plano_w	number(10);
ds_plano_w		varchar2(255);
ie_grau_dependencia_w	varchar2(10);
ds_dependencia_w	varchar2(255);
ie_titularidade_seg_w	varchar2(10);
ie_utilizar_tab_prop_w	varchar2(10);
qt_idade_min_sca_w	number(10);

begin

/*Dados do beneficiário*/
select	QT_IDADE,
	IE_TIPO_BENEF
into	qt_idade_benef_w,
	ie_titularidade_seg_w
from	PLS_SIMULPRECO_INDIVIDUAL	a
where	a.nr_sequencia			= nr_seq_pessoa_simul_p;

/*Dados do SCA*/
select	qt_idade_sca,
	nvl(ie_grau_dependencia,'A'),
	ds_plano,
	qt_idade_min_sca
into	qt_idade_sca_w,
	ie_grau_dependencia_w,
	ds_plano_w,
	qt_idade_min_sca_w
from	pls_plano
where	nr_sequencia	= nr_seq_plano_sca_p;


select	count(*)
into	qt_retorno_plano_w
from	pls_plano
where	nr_sequencia 		= nr_seq_plano_sca_p
and	ie_tipo_operacao	<> 'A';

select	decode(ie_grau_dependencia_w,'T','titular','dependente')
into	ds_dependencia_w
from	dual;

if	((ie_grau_dependencia_w <> 'A') and (ie_grau_dependencia_w <> ie_titularidade_seg_w)) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(279248, 'DS_PLANO_P=' || ds_plano_w ||';DS_DEPENDENCIA_P=' || ds_dependencia_w);
end if;

if	(nvl(qt_retorno_plano_w,0) > 0) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(279251);
end if;

if	((qt_idade_benef_w > nvl(qt_idade_sca_w,qt_idade_benef_w)) or
	(qt_idade_benef_w < nvl(qt_idade_min_sca_w,qt_idade_benef_w))) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(279253);
end if;	

ds_erro_p	:= ds_erro_w;	

end pls_consistir_sca_simulacao;
/