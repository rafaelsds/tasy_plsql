create or replace
procedure proj_gerar_escore_os_desenv
			(	nr_seq_gerencia_p		Number,
				ie_regra_definicao_p	Number,
				nr_seq_item_p			number,
				dt_referencia_p			date,
				qt_escore_p				Number,
				nm_usuario_p			Varchar2) is 

qt_ponto_w			Number(15,0);
qt_peso_w			Number(15,0);
qt_escore_w			Number(15,0);
qt_escore_real_w	Number(15,2);
pr_escore_real_w	Number(15,2);
nr_seq_cliente_w	Number(10,0);
dt_inicio_real_w	Date;
dt_fim_real_w		Date;
cd_cnpj_w			Varchar2(14);
nr_seq_loc_w		Number(15,0);
qt_os_recebida_w	Number(10)	:= 0;
qt_registro_w		Number(10)	:= 0;
nr_seq_proj_w		number(10);

begin

select	max(pe.nr_seq_proj)
into	nr_seq_proj_w
from	proj_escore_item pei,
		proj_escore pe
where	pei.nr_seq_escore = pe.nr_sequencia
and		pei.nr_sequencia = nr_seq_item_p;

/* N�mero de OS recebida */
select  count(*)
into	qt_os_recebida_w
from    grupo_desenvolvimento b, 
		man_ordem_servico a
where   trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
and		a.nr_seq_grupo_des 	= b.nr_sequencia
and		b.nr_seq_gerencia	= nr_seq_gerencia_p
and 	exists	(
					select	1
					from	man_ordem_serv_estagio b,
							man_estagio_processo c
					where	a.nr_sequencia = b.nr_seq_ordem
					and		b.nr_seq_estagio = c.nr_sequencia 
					and		c.ie_desenv = 'S'
				);
				
if	(ie_regra_definicao_p = 26) then
	begin
		select  count(*)
		into	qt_os_recebida_w
		from    grupo_desenvolvimento b, 
				man_ordem_servico a,
				proj_projeto p
		where   trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
		and		a.nr_seq_grupo_des = b.nr_sequencia
		and		b.nr_seq_gerencia = nr_seq_gerencia_p
		and		proj_obter_projeto_ordem_serv(a.nr_sequencia) = p.nr_sequencia
		and		p.nr_sequencia = nr_seq_proj_w
		and 	exists	(
							select	1
							from	man_ordem_serv_estagio b,
									man_estagio_processo c
							where	a.nr_sequencia = b.nr_seq_ordem
							and		b.nr_seq_estagio = c.nr_sequencia 
							and		c.ie_desenv = 'S'
						);
	end;
end if;

if	(ie_regra_definicao_p = 8) or
	(ie_regra_definicao_p = 12) or
	(ie_regra_definicao_p = 13) or
	(ie_regra_definicao_p = 26) then
	begin
	
	qt_peso_w	:= qt_os_recebida_w;
	
	/* N�mero de OS's executadas */
	if	(ie_regra_definicao_p	= 8) then
		select  count(*)
		into	qt_ponto_w
		from    grupo_desenvolvimento b, 
				man_ordem_servico a
		where   trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
		and		a.nr_seq_grupo_des 	= b.nr_sequencia
		and		b.nr_seq_gerencia	= nr_seq_gerencia_p
		and		((a.nr_seq_estagio in (2,1511)) or (a.dt_fim_real is not null))
		and 	exists	(
							select	1
							from	man_ordem_serv_estagio b,
									man_estagio_processo c
							where	a.nr_sequencia = b.nr_seq_ordem
							and		b.nr_seq_estagio = c.nr_sequencia 
							and		c.ie_desenv = 'S'
						);
						
	/* N�mero de OS's de erro no m�s */
	elsif	(ie_regra_definicao_p = 12) then
	
		select	sum(decode(m.ie_origem_erro, 'W', 1, 0)) qt_ordem_wheb
		into	qt_ponto_w
		from	grupo_desenvolvimento c, 
				man_ordem_servico_v a, 
				man_doc_erro m
		where 	m.nr_seq_ordem = a.nr_sequencia
		and   	m.nr_seq_grupo_des = c.nr_sequencia
		and   	a.ie_classificacao = 'E'
		and   	c.nr_seq_gerencia = nr_seq_gerencia_p
		and   	trunc(obter_data_estagio_os(a.nr_sequencia,'D'), 'month') >= trunc(dt_referencia_p,'mm')
		and		trunc(a.dt_ordem_servico, 'month') >= trunc(dt_referencia_p,'mm');
		
	/* N�mero de OS's com data de acordo ultrapassado */
	elsif	(ie_regra_definicao_p	= 13) or
		(ie_regra_definicao_p   = 26) then
		
		select  count(*)
		into	qt_ponto_w
		from    man_estagio_processo z,
				grupo_desenvolvimento b, 
				man_ordem_servico a
		where   a.nr_seq_grupo_des = b.nr_sequencia
		and		b.nr_seq_gerencia = nr_seq_gerencia_p
		and		a.nr_seq_estagio = z.nr_sequencia 
		and		z.ie_desenv = 'S'
		and		trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
		and		proj_obter_projeto_ordem_serv(a.nr_sequencia) = nr_seq_proj_w
		and		a.dt_interna_acordo	<= sysdate;
	end if;
	end;
/* N�mero de OS's com tempo maior que o definido para grupo */
elsif	(ie_regra_definicao_p	= 10) then
	begin
	
		select	count(*)
		into	qt_peso_w
		from	man_ordem_servico_v x,
				man_ordem_servico a
		where	a.nr_sequencia = x.nr_sequencia
		and		a.nr_seq_meta_pe is null
		and		a.nr_seq_obj_bsc is null
		and		x.nr_seq_gerencia_des = nr_seq_gerencia_p
		and		a.nr_seq_estagio in	(
										select	x.nr_sequencia
										from	man_estagio_processo x
										where	x.ie_desenv = 'S'
									)
		and		a.ie_status_ordem in ('1','2');

		select	count(*)
		into	qt_ponto_w
		from	grupo_desenvolvimento b,
				man_ordem_servico_v  a
		where	obter_se_os_pend_cliente(a.nr_sequencia) = 'N'
		and		Obter_Se_OS_Desenv(a.nr_sequencia) = 'S'
		and		a.nr_seq_gerencia_des = nr_seq_gerencia_p
		and		(ie_status_ordem <> 3)
		and		a.nr_seq_grupo_des    = b.nr_sequencia
		and		a.dt_ordem_servico < (sysdate - b.qt_dia_prev_os);

	end;
	
/* N�mero de OS's duplas */
elsif	(ie_regra_definicao_p	= 11) then
	
	qt_peso_w	:= qt_os_recebida_w;

	select	count(*)
	into	qt_ponto_w
	from	grupo_desenvolvimento	b, 
			man_ordem_servico 	a
	where	trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
	and		a.nr_seq_grupo_des	= b.nr_sequencia
	and		b.nr_seq_gerencia	= nr_seq_gerencia_p
	and		exists	(
						select	count(m.nr_seq_ordem)
						from	man_ordem_serv_estagio m
						where	a.nr_sequencia = m.nr_seq_ordem
						and		m.ie_nivel_solucao = 'SD'
						having	count(m.nr_seq_ordem) > 1
					);

/* N�mero de OS's com tempo de SLA ultrapassado */
elsif	(ie_regra_definicao_p	= 14) then
	select	count(*)
	into	qt_peso_w
	from	man_ordem_serv_sla	c,
			grupo_desenvolvimento	b, 
			man_ordem_servico 	a
	where	trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
	and		a.nr_seq_grupo_des = b.nr_sequencia
	and		a.nr_sequencia = c.nr_seq_ordem
	and		b.nr_seq_gerencia = nr_seq_gerencia_p;
	
	select	count(*)
	into	qt_ponto_w
	from	man_ordem_serv_sla	c,
			grupo_desenvolvimento	b, 
			man_ordem_servico 	a
	where	trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
	and		a.nr_seq_grupo_des = b.nr_sequencia
	and		a.nr_sequencia = c.nr_seq_ordem
	and		b.nr_seq_gerencia = nr_seq_gerencia_p
	and		c.qt_desvio > 0;
	
/* Meta mensal atingida */
elsif	(ie_regra_definicao_p	= 16) then
	
	select	count(*)
	into	qt_peso_w
	from 	grupo_desenvolvimento b,
			desenvolvimento_meta_os a
	where	a.nr_seq_grupo_des = b.nr_sequencia
	and 	trunc(dt_meta,'month') = dt_referencia_p
	and		b.nr_seq_gerencia = nr_seq_gerencia_p
	and		qt_meta_prev > 0;
	
	select	count(*)
	into	qt_ponto_w
	from	grupo_desenvolvimento b,
			desenvolvimento_meta_os	a
	where	a.nr_seq_grupo_des = b.nr_sequencia
	and 	trunc(dt_meta,'month') = dt_referencia_p
	and		b.nr_seq_gerencia = nr_seq_gerencia_p
	and		nvl(a.qt_meta_real,0) < a.qt_meta_prev
	and		qt_meta_prev > 0;
	
/* Nota do NET */
elsif	(ie_regra_definicao_p = 17) then
	select	sum(c.qt_nota),
			count(*)
	into	qt_ponto_w,
			qt_registro_w
	from	alteracao_tasy		c,
			grupo_desenvolvimento	b, 
			man_ordem_servico 	a
	where   trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
	and		a.nr_seq_grupo_des = b.nr_sequencia
	and		a.nr_sequencia = c.nr_seq_ordem_serv
	and		b.nr_seq_gerencia = nr_seq_gerencia_p
	and		nvl(c.qt_nota,0) > 0;
	
	qt_peso_w	:= qt_registro_w * 10;
	
elsif	(ie_regra_definicao_p = 18) then
/* Grau de satisfa��o 
	E -  Excelente	(5)
	O - Otimo		(3)
	B- Bom		(2)
	R - Regular		(1)
	N - N�o se aplica	(0)
	P - Ruim ou P�ssimo	(-1)
*/
	select	sum(decode(a.ie_grau_satisfacao,'E',5,'O',3,'B',2,'R',1,'N',0,'P',-1)),
			count(*)
	into	qt_ponto_w,
			qt_registro_w
	from	grupo_desenvolvimento b, 
			man_ordem_servico  a
	where   trunc(a.dt_ordem_servico, 'month') = dt_referencia_p
	and		a.nr_seq_grupo_des = b.nr_sequencia
	and		b.nr_seq_gerencia = nr_seq_gerencia_p
	and		a.ie_grau_satisfacao is not null;
	
	qt_peso_w := qt_registro_w * 5;
	
/* Horas extras ultrapassadas */	
elsif	(ie_regra_definicao_p = 19) then
	/* Count(*) -1 pois desconta a soma das horas do gerente */
	select	count(*) -1
	into	qt_registro_w
	from	usuario_grupo_des c,
			grupo_desenvolvimento b
	where	b.nr_sequencia = c.nr_seq_grupo
	and		b.nr_seq_gerencia = nr_seq_gerencia_p;
	
	qt_peso_w := qt_registro_w * 5;
end if;

if	(qt_peso_w > 0)  then
	if	(qt_ponto_w > qt_peso_w) then
		qt_escore_real_w := 0;
	else
		select	qt_escore
		into	qt_escore_w
		from	proj_escore_item
		where	nr_sequencia = nr_seq_item_p;
		
		if	(ie_regra_definicao_p in (8,16,17,18)) then
			qt_escore_real_w := qt_escore_w - (qt_escore_w * ((qt_peso_w - qt_ponto_w) / qt_peso_w));
		else
			qt_escore_real_w := qt_escore_w * ((qt_peso_w - qt_ponto_w) / qt_peso_w);
		end if;
	end if;
	pr_escore_real_w := trunc(dividir(qt_escore_real_w * 100, qt_escore_p),0);
else
	qt_escore_real_w := qt_escore_p;
	pr_escore_real_w := 100;
end if;

update	proj_escore_item
set		qt_nota_maxima	= qt_peso_w,
		qt_nota			= qt_ponto_w,
		qt_escore_real	= qt_escore_real_w,
		pr_escore_real	= pr_escore_real_w
where	nr_sequencia	= nr_seq_item_p;

end proj_gerar_escore_OS_Desenv;
/
