CREATE OR REPLACE PROCEDURE calcular_pontuacao_auditoria_b(
    nr_sequencia_p NUMBER,
    nm_usuario_p   VARCHAR2)
IS
  qt_total_ponto_w     NUMBER(15,2) := 0;
  qt_ponto_grupo_w   NUMBER(10)   := 0;
  ds_estrutura_w         VARCHAR2(255);
  CURSOR C01
  IS
    SELECT MIN(DECODE(c.ie_resultado,'PC', 5,DECODE(c.ie_resultado,'NC', 0,10))) vl_total,
    SUBSTR(obter_desc_expressao(CD_EXP_ESTRUTURA,DS_ESTRUTURA),1,255) ds_estrutura
    FROM qua_auditoria_result c,
      qua_auditoria_item b,
      qua_auditoria_estrut a,
      qua_auditoria d
    WHERE b.nr_seq_estrutura = a.nr_sequencia
    AND d.nr_sequencia       = c.nr_seq_auditoria
    AND d.nr_seq_tipo        = a.nr_seq_tipo
    AND c.nr_seq_item        = b.nr_sequencia
    AND d.nr_sequencia       = nr_sequencia_p
    AND c.ie_resultado NOT  IN ('NA')
    GROUP BY SUBSTR(obter_desc_expressao(CD_EXP_ESTRUTURA,DS_ESTRUTURA),1,255)
    ORDER BY 2;
BEGIN
  qt_total_ponto_w := 0;
  OPEN C01;
  LOOP
    FETCH C01 INTO qt_ponto_grupo_w,ds_estrutura_w;
    EXIT
  WHEN C01%NOTFOUND;
    qt_total_ponto_w := qt_total_ponto_w + qt_ponto_grupo_w;
  END LOOP;
  CLOSE C01;
  UPDATE qua_auditoria
  SET vl_total       = qt_total_ponto_w
  WHERE nr_sequencia = nr_sequencia_p;
  COMMIT;
END calcular_pontuacao_auditoria_b;
/