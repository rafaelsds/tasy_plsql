create or replace procedure finalizar_transfusao_hemot(	ie_opcao_p				varchar2,
										nr_seq_exame_lote_p		number,
										nr_seq_transfusao_p		number,
										cd_estabelecimento_p	number,
										cd_perfil_p				number,
										nm_usuario_p			varchar2,
										ds_erro_p			out	varchar2) is 

/*	ie_opcao_p
	F - Finalizar
	D - Desfazer	*/

ie_consistir_exames_nao_lib_w	varchar2(1);
ie_consistir_data_transf_w		varchar2(1);
ie_atualiza_data_utilizacao_w	varchar2(1);
ie_possuir_exames_nao_lib_w		varchar2(1);
ie_data_utilizacao_w			varchar2(1);
ie_desf_dt_fim_transf_w			varchar2(1);
ie_dt_fim_ADEP_w				varchar2(1);
qt_horas_consistir_w			number(10);
nr_seq_prescr_w					number(10);
nr_prescricao_w					number(10);
nr_atendimento_w				number(15);
nr_seq_reserva_w				number(10);
ds_msg_w						varchar2(255);

Cursor C01 is
	select	nr_sequencia
	from	prescr_procedimento
	where	dt_suspensao is null
	and		nr_seq_solic_sangue is not null
	and		nr_prescricao = nr_prescricao_w
	order by 1;

begin

if	(ie_opcao_p = 'F') then
	begin
	/* Hemoterapia - Parametro [93] - Ao finalizar transfusao, caso os exames nao estejam liberados consistir a mesma */
	obter_param_usuario(450, 93, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_consistir_exames_nao_lib_w);
	obter_param_usuario(450, 442, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, qt_horas_consistir_w);

	if	(ie_consistir_exames_nao_lib_w = 'S') then
		begin		
		select 	decode(sum(qt_exames_nao_lib), 0, 'N', 'S')
		into	ie_possuir_exames_nao_lib_w
		from   (SELECT 	COUNT(*) qt_exames_nao_lib
			FROM 	san_exame_realizado a,
				san_exame_lote b,
				san_transfusao d
			WHERE 	a.nr_seq_exame_lote = b.nr_sequencia
			AND	b.nr_seq_transfusao = d.nr_sequencia
			and	d.nr_sequencia = nr_seq_transfusao_p
			AND	a.dt_liberacao IS NULL
			and not exists(	select	1
					from	san_transfusao g
					where	g.nr_atendimento = d.nr_atendimento
					and	g.nr_sequencia <> d.nr_sequencia
					and	g.ie_status = 'F'
					and	g.dt_transfusao between d.dt_transfusao-(nvl(qt_horas_consistir_w,0)/24) and d.dt_transfusao)
			UNION
			SELECT 	COUNT(*) qt_exames_nao_lib
			FROM 	san_exame_realizado a,
				san_exame_lote b,
				san_producao c,
				san_transfusao d
			WHERE 	a.nr_seq_exame_lote = b.nr_sequencia
			AND	b.nr_seq_producao = c.nr_sequencia
			AND	b.ie_origem = 'T'
			AND	c.nr_seq_transfusao = d.nr_sequencia
			and	d.nr_sequencia = nr_seq_transfusao_p
			AND	a.dt_liberacao IS NULL
			and not exists(	select	1
					from	san_transfusao g
					where	g.nr_atendimento = d.nr_atendimento
					and	g.nr_sequencia <> d.nr_sequencia
					and	g.ie_status = 'F'
					and	g.dt_transfusao between d.dt_transfusao-(nvl(qt_horas_consistir_w,0)/24) and d.dt_transfusao));

		if	(ie_possuir_exames_nao_lib_w = 'S') then
			ds_erro_p	:= obter_texto_tasy(83474, wheb_usuario_pck.get_nr_seq_idioma);
		end if;
		end;
	end if;

	if	(ds_erro_p is null) then
		begin
		/* Hemoterapia - Parametro [61] - Consistir data de utilizacao/fim utilizacao ao finalizar a transfusao */
		obter_param_usuario(450, 61, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consistir_data_transf_w);

		if	(ie_consistir_data_transf_w = 'S') then
			begin
			select	decode(count(*), 0, 'N', 'S')
			into	ie_data_utilizacao_w
			from	san_producao
			where	nr_seq_transfusao	= nr_seq_transfusao_p
			and	(dt_utilizacao is null or dt_termino_util is null);

			if	(ie_data_utilizacao_w = 'S') then
				ds_erro_p	:= obter_texto_tasy(83475, wheb_usuario_pck.get_nr_seq_idioma);
			end if;
			end;
		end if;
		end;
	end if;

	if	(ds_erro_p is null) then
		begin
		update	san_transfusao
		set	ie_status		= 'F',
			dt_fim_transfusao	= sysdate
		where	nr_sequencia	= nr_seq_transfusao_p;

		/* Hemoterapia - Parametro [31] - Atualizar data de utilizacao ao gerar/finalizar transfusao */
		obter_param_usuario(450, 31, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_data_utilizacao_w);

		if	(ie_atualiza_data_utilizacao_w = 'S') then
			begin
			update	san_producao
			set	dt_termino_util	= sysdate
			where	nr_seq_transfusao	= nr_seq_transfusao_p
			and	dt_termino_util	is null;
			end;
		end if;

		select	max(a.nr_prescricao)
		into	nr_prescricao_w
		from	san_transfusao a
		where	a.nr_sequencia = nr_seq_transfusao_p;
		
		if	(nr_prescricao_w is null) then
			select	max(c.nr_prescricao),
					max(c.nr_seq_prescr)
			into	nr_prescricao_w,
					nr_seq_prescr_w
			from	san_reserva_item c,
					san_reserva b,
					san_transfusao a
			where	c.nr_seq_reserva = b.nr_sequencia
			and		b.nr_sequencia = a.nr_seq_reserva
			and		a.nr_sequencia = nr_seq_transfusao_p;
		end if;
		
		-- Conforme acordo com cliente, sera realizado somente tratamento para termino no ADEP. Caso seja cancelada transfusao na funcao Hemoterapia, nao ira atualizar no ADEP. OS 572335
		obter_param_usuario(450, 452, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_dt_fim_ADEP_w);
		
		if	(ie_dt_fim_ADEP_w = 'S') and
			(nr_prescricao_w is not null) then
			
			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_w;
			
			if	(nr_seq_prescr_w is null) then
				select	max(c.nr_seq_prescr)
				into	nr_seq_prescr_w
				from	san_reserva_item c,
						san_reserva b,
						san_transfusao a
				where	c.nr_seq_reserva = b.nr_sequencia
				and		b.nr_sequencia = a.nr_seq_reserva
				and		a.nr_sequencia = nr_seq_transfusao_p;
			end if;
			
			if	(nr_seq_prescr_w is not null) then
				update	prescr_proc_hor
				set		dt_fim_horario = sysdate,
						nm_usuario_adm = nm_usuario_p
				where	dt_fim_horario is null
				and		nr_seq_procedimento = nr_seq_prescr_w
				and		nr_prescricao		= nr_prescricao_w;
				
				Gerar_alteracao_solucao_prescr(	cd_estabelecimento_p, nr_atendimento_w, 3, nr_prescricao_w, nr_seq_prescr_w, 4, sysdate, null, null, null, null, null, null, null, null, null,
												wheb_mensagem_pck.get_texto(279024), nm_usuario_p, null, sysdate, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ds_msg_w);
			else
				open C01;
				loop
				fetch C01 into
					nr_seq_prescr_w;
				exit when C01%notfound;
					begin
					update	prescr_proc_hor
					set		dt_fim_horario = sysdate,
							nm_usuario_adm = nm_usuario_p
					where	dt_fim_horario is null
					and		nr_seq_procedimento = nr_seq_prescr_w
					and		nr_prescricao		= nr_prescricao_w;
		
					Gerar_alteracao_solucao_prescr(	cd_estabelecimento_p, nr_atendimento_w, 3, nr_prescricao_w, nr_seq_prescr_w, 4, sysdate, null, null, null, null, null, null, null, null, null,
													wheb_mensagem_pck.get_texto(279024), nm_usuario_p, null, sysdate, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ds_msg_w);
					end;
				end loop;
				close C01;
			end if;
		end if;
		end; 
	end if;
	
	if(pkg_i18n.get_user_locale = 'es_BO') then
		gerar_sangue_pend_pac(nr_seq_transfusao_p,null);
	end if;
	
	end;

elsif	(ie_opcao_p = 'D') then
	begin
	update	san_transfusao
	set	ie_status		= 'A'
	where	nr_sequencia	=  nr_seq_transfusao_p;

	/* Hemoterapia - Parametro [95] - Limpar a data de utilizacao ao desfazer final de transfusao */
	obter_param_usuario(450, 95, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_desf_dt_fim_transf_w);

	if	(ie_desf_dt_fim_transf_w = 'S') then
		begin
		update	san_producao
		set	dt_termino_util	= null
		where	nr_seq_transfusao	= nr_seq_transfusao_p
		and	dt_termino_util is not null;
		end;
	end if;

	select	max(a.nr_prescricao)
	into	nr_prescricao_w
	from	san_transfusao a
	where	a.nr_sequencia = nr_seq_transfusao_p;
	
	if	(nr_prescricao_w is null) then
		select	max(c.nr_prescricao),
				max(c.nr_seq_prescr)
		into	nr_prescricao_w,
				nr_seq_prescr_w
		from	san_reserva_item c,
				san_reserva b,
				san_transfusao a
		where	c.nr_seq_reserva = b.nr_sequencia
		and		b.nr_sequencia = a.nr_seq_reserva
		and		a.nr_sequencia = nr_seq_transfusao_p;
	end if;
		
	obter_param_usuario(450, 452, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_dt_fim_ADEP_w);

	if	(ie_dt_fim_ADEP_w = 'S') and
		(nr_prescricao_w is not null) then
			
		select	max(nr_atendimento)
		into	nr_atendimento_w
		from	prescr_medica
		where	nr_prescricao = nr_prescricao_w;
		
		if	(nr_seq_prescr_w is null) then
			select	max(c.nr_seq_prescr),
					max(a.nr_seq_reserva)
			into	nr_seq_prescr_w,
					nr_seq_reserva_w
			from	san_reserva_item c,
					san_reserva b,
					san_transfusao a
			where	c.nr_seq_reserva = b.nr_sequencia
			and		b.nr_sequencia = a.nr_seq_reserva
			and		a.nr_sequencia = nr_seq_transfusao_p;

		end if;
	
		if	(nr_seq_prescr_w is not null) then
			update	prescr_proc_hor
			set		dt_fim_horario = null,
					nm_usuario_adm = null
			where	dt_fim_horario is not null
			and		nr_seq_procedimento = nr_seq_prescr_w
			and		nr_prescricao		= nr_prescricao_w;
			
			reverter_alteracao_solucao(cd_estabelecimento_p, nr_atendimento_w, 3, nr_prescricao_w, nr_seq_prescr_w, null, null, nm_usuario_p, nr_seq_reserva_w, null, null);

		else
			open C01;
			loop
			fetch C01 into
				nr_seq_prescr_w;
			exit when C01%notfound;
				begin
				update	prescr_proc_hor
				set		dt_fim_horario = null,
						nm_usuario_adm = null
				where	dt_fim_horario is not null
				and		nr_seq_procedimento = nr_seq_prescr_w
				and		nr_prescricao		= nr_prescricao_w;
				
				reverter_alteracao_solucao(cd_estabelecimento_p, nr_atendimento_w, 3, nr_prescricao_w, nr_seq_prescr_w, null, null, nm_usuario_p, nr_seq_reserva_w, null, null);

				end;
			end loop;
			close C01;
		end if;
	end if;
	end;
end if;

commit;

end finalizar_transfusao_hemot;
/
