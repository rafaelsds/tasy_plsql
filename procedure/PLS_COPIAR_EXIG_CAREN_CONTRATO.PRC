create or replace
procedure pls_copiar_exig_caren_contrato
		(	nr_seq_contrato_p	number,
			nr_seq_regra_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

nr_seq_regra_carencia_w	number(10);

begin

select	pls_regra_carencia_isencao_seq.nextval
into	nr_seq_regra_carencia_w
from	dual;

insert into pls_regra_carencia_isencao
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		cd_estabelecimento,nr_seq_contrato,qt_dias_inicial,qt_dias_final,ie_tipo_data,
		ie_tipo_contratacao,qt_vidas_inicial,qt_vidas_final,ie_situacao,ie_exige_carencia,
		ie_mae_beneficiario,ie_acao_contrato,ie_responsavel_plano,qt_tempo_resp_plano,
		ie_regulamentacao,ie_referencia_vidas, qt_idade_minima, qt_idade_maxima)
	(select	nr_seq_regra_carencia_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		cd_estabelecimento_p,nr_seq_contrato_p,qt_dias_inicial,qt_dias_final,ie_tipo_data,
		ie_tipo_contratacao,qt_vidas_inicial,qt_vidas_final,'A',ie_exige_carencia,
		ie_mae_beneficiario,ie_acao_contrato,ie_responsavel_plano,qt_tempo_resp_plano,
		ie_regulamentacao,ie_referencia_vidas, qt_idade_minima, qt_idade_maxima
	from	pls_regra_carencia_isencao
	where	nr_sequencia = nr_seq_regra_p);

insert into pls_regra_carencia_dias
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		nr_seq_regra,nr_seq_tipo_carencia,qt_dias)
	(select	pls_regra_carencia_dias_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		nr_seq_regra_carencia_w,nr_seq_tipo_carencia,qt_dias
	from	pls_regra_carencia_dias
	where	nr_seq_regra	= nr_seq_regra_p);

commit;

end pls_copiar_exig_caren_contrato;
/