create or replace
procedure GERAR_PERDAS_REPASSE
			(nr_interno_conta_p	number,
	 		nm_usuario_p		varchar2,
			ie_operacao_p		varchar2,
			vl_titulo_p		number DEFAULT 0,
			vl_baixa_p		number DEFAULT 0,
			vl_saldo_titulo_p		number DEFAULT 0) is

/* N�O DAR COMMIT NESTA PROCEDURE */

nr_seq_repasse_w		number(10);
ie_forma_pagto_w		varchar2(1);
ie_status_w		varchar2(1);
ie_tipo_item_w		varchar2(1);
vl_porcentagem_perda_w		number(7,4);
vl_procmat_repasse_w		number(10,2) := 0;
nr_seq_novo_proc_w		procedimento_repasse.nr_sequencia%type;
nr_seq_novo_mat_w		material_repasse.nr_sequencia%type;

cursor	c01 is
select	'P' ie_tipo_item,
	b.nr_sequencia,
	c.ie_forma_pagto
from	terceiro c,
	procedimento_repasse b,
	procedimento_paciente a
where	b.nr_seq_terceiro		= c.nr_sequencia
and	b.nr_repasse_terceiro	is null
and	a.nr_sequencia		= b.nr_seq_procedimento
and	a.nr_interno_conta		= nr_interno_conta_p
and	b.ie_status = 'A'
union all
select	'M' ie_tipo_item,
	b.nr_sequencia,
	c.ie_forma_pagto
from	terceiro c,
	material_repasse b,
	material_atend_paciente a
where	b.nr_seq_terceiro		= c.nr_sequencia
and	b.nr_repasse_terceiro	is null
and	a.nr_sequencia		= b.nr_seq_material
and	a.nr_interno_conta		= nr_interno_conta_p
and	b.ie_status = 'A';

begin

open	c01;
loop
fetch	c01 into
	ie_tipo_item_w,
	nr_seq_repasse_w,
	ie_forma_pagto_w;
exit	when c01%notfound;

	if	(ie_operacao_p = 'B') then
		ie_status_w	:= 'P';
	elsif	(ie_forma_pagto_w = 'P') then
		ie_status_w	:= 'U';
	else
		ie_status_w	:= 'A';
	end if;
	
	if (vl_titulo_p = 0 and vl_baixa_p = 0) then
		vl_porcentagem_perda_w	:= 1;
	else 
		vl_porcentagem_perda_w := dividir_sem_round(vl_baixa_p,vl_titulo_p);
		select decode(vl_porcentagem_perda_w,0,1,vl_porcentagem_perda_w)
		into vl_porcentagem_perda_w from dual;
	end if;
	
	if	(ie_tipo_item_w	= 'P') then

		select	vl_original_repasse * (1 - vl_porcentagem_perda_w)
		into	vl_procmat_repasse_w
		from	procedimento_repasse
		where	nr_sequencia	= nr_seq_repasse_w;
		
		update	procedimento_repasse a
		set	a.ie_status	= ie_status_w,
			a.nm_usuario	= nm_usuario_p,
			vl_repasse		= vl_original_repasse * vl_porcentagem_perda_w
		where	a.nr_sequencia	= nr_seq_repasse_w;

		if (vl_procmat_repasse_w > 0) and (vl_saldo_titulo_p > 0) then
			Desdobrar_ProcMat_Repasse(nr_seq_repasse_w, null, 'A', vl_procmat_repasse_w, nm_usuario_p,nr_seq_novo_proc_w,nr_seq_novo_mat_w);
		end if;

	else

		select	vl_original_repasse * (1 - vl_porcentagem_perda_w)
		into		vl_procmat_repasse_w
		from		material_repasse
		where	nr_sequencia	= nr_seq_repasse_w;

		update	material_repasse a
		set	a.ie_status	= ie_status_w,
			a.nm_usuario	= nm_usuario_p,
			vl_repasse		= vl_original_repasse * vl_porcentagem_perda_w
		where	a.nr_sequencia	= nr_seq_repasse_w;

		if (vl_procmat_repasse_w > 0) and (vl_saldo_titulo_p > 0)  then
			Desdobrar_ProcMat_Repasse(null, nr_seq_repasse_w, 'A', vl_procmat_repasse_w, nm_usuario_p,nr_seq_novo_proc_w,nr_seq_novo_mat_w);
		end if;

	end if;

end	loop;
close	c01;

end GERAR_PERDAS_REPASSE;
/
