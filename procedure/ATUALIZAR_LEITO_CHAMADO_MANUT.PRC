create or replace
procedure atualizar_leito_chamado_manut(
			nm_usuario_p		Varchar2,
			cd_unidade_basica_p	varchar2,
			cd_unidade_compl_p	varchar2,
			cd_setor_atendimento_p	number,
			ie_opcao_p		varchar2) is

/* C chamado manutencao
*/			

begin

if	(cd_setor_atendimento_p is not null) and 
	(cd_unidade_basica_p is not null) and
	(cd_unidade_compl_p is not null) then
	
	if	(ie_opcao_p = 'C') then

		update	unidade_atendimento
		set	ie_status_unidade    = 'C',
			nm_usuario           = nm_usuario_p,
			dt_atualizacao       = sysdate
		where	cd_unidade_basica    = cd_unidade_basica_p
		and	cd_unidade_compl     = cd_unidade_compl_p
		and 	cd_setor_atendimento = cd_setor_atendimento_p;
		
	end if;
end if;
commit;

end atualizar_leito_chamado_manut;
/