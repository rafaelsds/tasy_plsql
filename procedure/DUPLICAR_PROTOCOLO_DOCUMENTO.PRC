create or replace
procedure DUPLICAR_PROTOCOLO_DOCUMENTO	(nr_seq_protocolo_p	number,
						 nm_usuario_p		varchar2) is

dt_envio_w		date;
dt_fechamento_w		date;
dt_rec_destino_w		date;
nm_usuario_envio_w	varchar2(15);
ie_tipo_protocolo_w		varchar2(3);
cd_pessoa_origem_w	varchar2(10);
cd_pessoa_envio_w	varchar(10); /* afstringari - OS151895 03/07/2009 */
cd_pessoa_destino_w	varchar2(10);
cd_cgc_destino_w		varchar2(14);
nm_usuario_receb_w	varchar2(15);
ds_obs_w		varchar2(4000);
cd_setor_destino_w		number(5);
cd_setor_origem_w		number(5);
nr_seq_prot_novo_w	number(10);

ds_documento_w		varchar2(2000);
nr_seq_interno_w		number(10);
nr_seq_tipo_item_w		number(10);
nr_documento_w		number(10);
nr_seq_item_w		number(5);
dt_conferencia_w		date;
dt_recebimento_w		date;
ie_envio_atual_w		varchar2(15);
ie_pessoa_vazio_w		varchar2(15);
cd_estabelecimento_w		protocolo_documento.cd_estabelecimento%type;
nr_seq_tipo_doc_item_w		protocolo_documento.nr_seq_tipo_doc_item%type;

cursor c01 is
select	nr_seq_item,
	nr_documento,
	ds_documento,
	nr_seq_interno,
	nr_seq_tipo_item,
	dt_conferencia,
	dt_recebimento
from	protocolo_doc_item
where	nr_sequencia	= nr_seq_protocolo_p;

begin
ie_pessoa_vazio_w := nvl(obter_valor_param_usuario(290,54,obter_perfil_ativo,nm_usuario_p,Wheb_usuario_pck.get_cd_estabelecimento),'S');
ie_envio_atual_w  := nvl(obter_valor_param_usuario(290,55,obter_perfil_ativo,nm_usuario_p,Wheb_usuario_pck.get_cd_estabelecimento),'N');


select	obter_pessoa_fisica_usuario(nm_usuario_p,'C') /* afstringari - OS151895 03/07/2009 */
into	cd_pessoa_envio_w
from 	dual;

if	(nvl(nr_seq_protocolo_p, 0) > 0) then

	select	dt_envio,
		nm_usuario_envio,
		ie_tipo_protocolo,
		cd_pessoa_origem,
		cd_setor_origem,
		cd_pessoa_destino,
		cd_setor_destino,
		cd_cgc_destino,
		dt_rec_destino,
		nm_usuario_receb,
		ds_obs,
		dt_fechamento,
		cd_estabelecimento,
		nr_seq_tipo_doc_item
	into	dt_envio_w,
		nm_usuario_envio_w,
		ie_tipo_protocolo_w,
		cd_pessoa_origem_w,
		cd_setor_origem_w,
		cd_pessoa_destino_w,
		cd_setor_destino_w,
		cd_cgc_destino_w,
		dt_rec_destino_w,
		nm_usuario_receb_w,
		ds_obs_w,
		dt_fechamento_w,
		cd_estabelecimento_w,
		nr_seq_tipo_doc_item_w
	from	protocolo_documento
	where	nr_sequencia	= nr_seq_protocolo_p;

	select	protocolo_documento_seq.nextval
	into	nr_seq_prot_novo_w
	from	dual;
	--'Protocolo documento duplicado a partir do protocolo: '||nr_seq_protocolo_p
	ds_obs_w	:= substr(wheb_mensagem_pck.get_texto(305447,'NR_SEQ_PROTOCOLO_P='||nr_seq_protocolo_p)||chr(10)||chr(13)||ds_obs_w,1,4000); /* jcaraujo 25/03/11 - OS304210 */
	
	if	(upper(ie_envio_atual_w) = 'S') then
		dt_envio_w := sysdate;
	elsif (upper(ie_envio_atual_w) = 'X') then
		dt_envio_w := null;
	end if;
	
	if	(ie_pessoa_vazio_w = 'N') then
		cd_pessoa_origem_w := null;
	end if;	

	insert into protocolo_documento
		(nr_sequencia,
		dt_envio,
		nm_usuario_envio,
		ie_tipo_protocolo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_origem,
		cd_setor_origem,
		cd_pessoa_destino,
		cd_setor_destino,
		cd_cgc_destino,
		dt_rec_destino,
		nm_usuario_receb,
		ds_obs,
		dt_fechamento,
		nr_seq_protocolo_origem,
		cd_estabelecimento,
		nr_seq_tipo_doc_item)
	values(	nr_seq_prot_novo_w,
		dt_envio_w,
		nm_usuario_p,		/* nm_usuario_envio_w,	 afstringari - OS151895 03/07/2009 */
		ie_tipo_protocolo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_envio_w,	/* cd_pessoa_origem_w,	 afstringari - OS151895 03/07/2009 */
		cd_setor_origem_w,
		cd_pessoa_destino_w,
		cd_setor_destino_w,
		cd_cgc_destino_w,
		null,			/* dt_rec_destino_w,	ahoffelder - OS154829 20/07/2009 */
		null,			/* nm_usuario_receb_w,	ahoffelder - OS154829 20/07/2009 */
		ds_obs_w,
		null,			/* dt_fechamento_w	ahoffelder - OS154829 20/07/2009 */
		nr_seq_protocolo_p,
		cd_estabelecimento_w,
		nr_seq_tipo_doc_item_w);

	nr_seq_item_w	:= 0;

	open c01;
	loop
	fetch c01 into
		nr_seq_item_w,
		nr_documento_w,
		ds_documento_w,
		nr_seq_interno_w,
		nr_seq_tipo_item_w,
		dt_conferencia_w,
		dt_recebimento_w;
	exit when c01%notfound;

		insert into protocolo_doc_item
			(nr_seq_item,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_documento,
			ds_documento,
			nr_seq_interno,
			nr_seq_tipo_item,
			dt_conferencia,
			dt_recebimento,
			nm_usuario_receb)		/* ahoffelder - OS154829 20/07/2009 */
		values (nr_seq_item_w,
			nr_seq_prot_novo_w,
			sysdate,
			nm_usuario_p,
			nr_documento_w,
			ds_documento_w,
			nr_seq_interno_w,
			nr_seq_tipo_item_w,
			dt_conferencia_w,
			null,			/* dt_recebimento_w	ahoffelder - OS154829 20/07/2009 */
			null);

	end loop;
	close c01;

commit;	

end if;

end DUPLICAR_PROTOCOLO_DOCUMENTO;
/