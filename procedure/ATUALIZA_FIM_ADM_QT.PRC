create or replace
procedure atualiza_fim_adm_qt( 	nr_seq_atendimento_p number,
								nm_usuario_p		Varchar2,
								dt_fim_adm_p date) is 
								
cd_estabelecimento_w	number(5) := wheb_usuario_pck.get_cd_estabelecimento;
nr_seq_estagio_param_w	varchar2(20);
qt_existe_estagio_w	number(5) := 0;
nr_sequencia_autor_w	autorizacao_convenio.nr_sequencia%type;
begin

obter_param_usuario(3130,471,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,nr_seq_estagio_param_w);

if 	(nr_seq_atendimento_p is not null) and
	(dt_fim_adm_p is not null) then
	
	update 	paciente_atendimento 
    set 	dt_fim_adm = dt_fim_adm_p,
			nm_usuario = nm_usuario_p
    where 	nr_seq_atendimento =  nr_seq_atendimento_p;

elsif (nr_seq_atendimento_p is not null) and
	(dt_fim_adm_p is null) then 
	
	update 	paciente_atendimento 
    set 	dt_fim_adm = sysdate,
			nm_usuario = nm_usuario_p
    where 	nr_seq_atendimento =  nr_seq_atendimento_p;

end if;

if	(nr_seq_estagio_param_w is not null) then

	select	count(*)
	into	qt_existe_estagio_w
	from	estagio_autorizacao
	where	nr_sequencia = to_number(nr_seq_estagio_param_w);

	select	max(a.nr_sequencia)
	into	nr_sequencia_autor_w
	from	autorizacao_convenio a,
		estagio_autorizacao e
	where	e.nr_sequencia = a.nr_seq_estagio
	and	nvl(qt_existe_estagio_w,0) > 0
	and	a.nr_seq_paciente = nr_seq_atendimento_p;

	if	(nr_sequencia_autor_w is not null) then
		atualizar_autorizacao_convenio(nr_sequencia_autor_w,nm_usuario_p, nr_seq_estagio_param_w,'N','N','S');
	end if;
end if;

if	(nr_seq_atendimento_p is not null) then
	integracao_dispensario_pck.cubixx_termino_administracao(nr_seq_atendimento_p);
end if;

commit;

end Atualiza_fim_adm_qt;
/