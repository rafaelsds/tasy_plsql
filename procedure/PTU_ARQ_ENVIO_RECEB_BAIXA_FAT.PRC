create or replace
procedure ptu_arq_envio_receb_baixa_fat
		(	nr_seq_baixa_fatura_p	ptu_fat_baixa_interc.nr_sequencia%type,
			ds_arquivo_p		ptu_fat_baixa_interc.ds_conteudo_a510%type,
			ie_tipo_arquivo_p	varchar2,
			nm_usuario_p		Varchar2) is 

begin

if	(ie_tipo_arquivo_p = 'E') then
	update	ptu_fat_baixa_interc
	set	ds_conteudo_a510	= ds_arquivo_p
	where	nr_sequencia		= nr_seq_baixa_fatura_p;
elsif	(ie_tipo_arquivo_p = 'R') then
	update	ptu_fat_baixa_interc
	set	ds_conteudo_a515	= ds_arquivo_p
	where	nr_sequencia		= nr_seq_baixa_fatura_p;
end if;

commit;

end ptu_arq_envio_receb_baixa_fat;
/
