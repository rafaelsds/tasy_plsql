create or replace
procedure gerar_escritural_gps(	nr_seq_gps_p		number,
				cd_banco_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				nr_seq_conta_p          number default null) is 

dt_vencimento_w		date;
nr_seq_escritural_w	number(10);
nr_titulo_w			number(10);

vl_juros_gps_w		gps.vl_juros%type;
vl_multa_gps_w		gps.vl_multa%type;

cursor	c01 is
select	a.nr_titulo
from 	gps_titulo a
where	a.nr_seq_gps	= nr_seq_gps_p;

begin

select	banco_escritural_seq.nextval
into	nr_seq_escritural_w
from	dual;

select	dt_vencimento
into	dt_vencimento_w
from	gps
where	nr_sequencia	= nr_seq_gps_p;

insert	into banco_escritural
	(nr_sequencia,
	cd_banco,
	cd_estabelecimento,
	ie_cobranca_pagto,
	ie_remessa_retorno,
	dt_remessa_retorno,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_observacao,
	nr_seq_conta_banco)
values	(nr_seq_escritural_w,
	cd_banco_p,
	cd_estabelecimento_p,
	'C',
	'R',
	dt_vencimento_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	substr(wheb_mensagem_pck.get_texto(304959)|| nr_seq_gps_p,1,4000),
	nr_seq_conta_p);

open	c01;
loop
fetch	c01 into
	nr_titulo_w;
exit	when c01%notfound;

	gerar_titulo_escritural(nr_titulo_w,nr_seq_escritural_w,nm_usuario_p);

end	loop;
close	c01;

--OS 1750569 in�cio
if (nr_seq_gps_p is not null) then

	select	max(a.vl_juros),
			max(a.vl_multa)
	into	vl_juros_gps_w,
			vl_multa_gps_w
	from	gps a
	where	a.nr_sequencia = nr_seq_gps_p;
	
	/*Pegar o titulo de maior valor no lote escritural para inserir os juros e multa do lote darf.*/
	nr_titulo_w := null;
	select	max(a.nr_titulo)
	into	nr_titulo_w
	from	titulo_pagar_escrit a
	where	a.nr_seq_escrit = nr_seq_escritural_w
	and		a.vl_escritural = (select max(x.vl_escritural) 
							   from   titulo_pagar_escrit x
							   where  x.nr_seq_escrit = nr_seq_escritural_w);	
					   
	if (nr_titulo_w is not null) then

		update	titulo_pagar_escrit
		set		vl_juros = nvl(vl_juros,0) + nvl(vl_juros_gps_w,0),
				vl_multa = nvl(vl_multa,0) + nvl(vl_multa_gps_w,0)
		where	nr_seq_escrit 	= nr_seq_escritural_w
		and		nr_titulo 		= nr_titulo_w;
	
	end if;	
							   
							   

end if;
--OS 1750569 fim.

update 	gps 
set 	nr_seq_escritural	= nr_seq_escritural_w 
where 	nr_sequencia		= nr_seq_gps_p;

commit;

end gerar_escritural_gps;
/