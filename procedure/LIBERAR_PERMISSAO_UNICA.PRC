create or replace
procedure liberar_permissao_unica(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2,
			ie_liberar_inativar_p		Varchar2) is 

begin

/* 	Objeto utilizado na fun��o Cadastro M�dico, 
	para libera��o ou inativa��o do registro de permiss�o �nica do m�dico (autoriz�-lo a realizar atendimento m�dico na institui��o).*/

if	(ie_liberar_inativar_p = 'L') then

update 	medico_permissao_unica
set	dt_liberacao = sysdate,
	nm_usuario_liberacao = nm_usuario_p,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where 	dt_inativacao is null
and	dt_liberacao is null
and	nr_sequencia = nr_sequencia_p;

commit;

/*Envio de CI ap�s libera��o da permiss�o �nica*/
envia_comunic_permissao_unica(nr_sequencia_p, nm_usuario_p);

elsif	(ie_liberar_inativar_p = 'I') then

update 	medico_permissao_unica
set	dt_inativacao = sysdate,
	nm_usuario_inativacao = nm_usuario_p,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where 	dt_inativacao is null
and	dt_liberacao is not null
and	nr_sequencia = nr_sequencia_p;

commit;

end if;

end liberar_permissao_unica;
/