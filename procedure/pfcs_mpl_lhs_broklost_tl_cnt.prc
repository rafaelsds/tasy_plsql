create or replace procedure pfcs_mpl_lhs_broklost_tl_cnt
(
    nr_seq_indicator_p		  number,
	cd_estabelecimento_p	  number,
	nm_usuario_p			  varchar2) is

qt_total_brkn_pfcs_w        	pfcs_panel.vl_indicator%type := 0;
qt_total_aux                	pfcs_panel.vl_indicator_aux%type := 0;
nr_seq_operational_level_w  	number(15) := 0;
nr_seq_panel_w					pfcs_panel_detail.nr_seq_panel%type;
qt_available_tl_w             number(15) := 0;
qt_isUse_device_w               number(15) := 0;
  
begin
    qt_available_tl_w := pfcs_func_tl_device_usage('Monitor', 'A', cd_estabelecimento_p);
   	qt_isUse_device_w := pfcs_func_tl_device_usage('Monitor', 'U', cd_estabelecimento_p);
   	qt_total_brkn_pfcs_w :=  pfcs_func_tl_device_usage('Monitor', 'B_L_D', cd_estabelecimento_p);
   	qt_total_aux :=  qt_available_tl_w + qt_isUse_device_w + qt_total_brkn_pfcs_w;

	pfcs_pck_v2.pfcs_generate_results(
				vl_indicator_p => qt_total_brkn_pfcs_w,
				vl_indicator_aux_p => qt_total_aux,
				ds_reference_value_p => '',
				nr_seq_indicator_p => nr_seq_indicator_p,
				nr_seq_operational_level_p => cd_estabelecimento_p,
				nm_usuario_p => nm_usuario_p,
				nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck_v2.pfcs_update_detail(
				nr_seq_indicator_p => nr_seq_indicator_p,
				nr_seq_panel_p => nr_seq_panel_w,
				nr_seq_operational_level_p => cd_estabelecimento_p,
				nm_usuario_p => nm_usuario_p);
	commit;

	pfcs_pck_v2.pfcs_activate_records(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p);

end pfcs_mpl_lhs_broklost_tl_cnt;
/
