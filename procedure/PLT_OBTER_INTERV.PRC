create or replace
procedure plt_obter_interv (
				cd_estabelecimento_p		number,
				cd_setor_usuario_p		number,
				cd_perfil_p			number,
				nm_usuario_p			varchar2,
				nr_atendimento_p		number,
				dt_inicial_horarios_p		date,
				dt_final_horarios_p		date,					
				dt_validade_limite_p		date,					
				ie_agrupar_acm_sn_p		varchar2,
				ie_prescr_usuario_p		varchar2,
				nr_seq_regra_p			number) is

nr_seq_wadep_w			number(10,0);
nr_seq_pe_prescr_w		number(14,0);
nr_seq_pe_proc_w		number(10,0);
cd_intervencao_w		varchar2(255);
ds_intervencao_w		varchar2(255);
ie_acm_sn_w			varchar2(1);
cd_intervalo_w			varchar2(7);
ds_intervalo_w			varchar2(255);
ie_status_w			varchar2(1);
ds_interv_prescr_w		varchar2(15);
ie_liberado_w			varchar2(1);
ie_copiar_w			varchar2(1);
ds_cor_titulo_w			varchar2(20);
					
cursor c01 is
select	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_seq_pe_prescr,nr_seq_pe_proc,ie_agrupar_acm_sn_p), 'N', nr_seq_pe_prescr, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_seq_pe_prescr,nr_seq_pe_proc,ie_agrupar_acm_sn_p), 'N', nr_seq_pe_proc, null), null),	
	cd_intervencao,
	ds_intervencao,
	ie_acm_sn,	
	cd_intervalo,
	ds_prescricao,
	--decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_seq_pe_prescr,nr_seq_pe_proc,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null) ie_status
	null,
	ds_intervalo,
	ie_liberado
from	(
	select	a.nr_sequencia nr_seq_pe_prescr,
		c.nr_seq_pe_proc,
		c.nr_seq_proc cd_intervencao,
		substr(y.ds_procedimento,1,240) ds_intervencao,
		obter_se_acm_sn(null,x.ie_se_necessario) ie_acm_sn,		
		x.cd_intervalo,
		substr(obter_desc_inf_sae_adep(x.ie_se_necessario,w.ds_prescricao),1,240) ds_prescricao,
		nvl(x.ie_suspenso,'N') ie_suspenso,
		substr(obter_desc_intervalo_prescr(x.cd_intervalo),1,15) ds_intervalo,
		decode(a.dt_liberacao,null,'N','S') ie_liberado
	from	intervalo_prescricao w,
		pe_procedimento y,
		pe_prescr_proc x,
		pe_prescr_proc_hor c,
		pe_prescricao a
	where	w.cd_intervalo(+) = x.cd_intervalo
	and	y.nr_sequencia = x.nr_seq_proc
	and	y.nr_sequencia = c.nr_seq_proc
	and	x.nr_seq_prescr = c.nr_seq_pe_prescr
	and	x.nr_sequencia = c.nr_seq_pe_proc
	and	x.nr_seq_prescr = a.nr_sequencia
	and	c.nr_seq_pe_prescr = a.nr_sequencia
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	a.dt_liberacao is not null
	and	nvl(x.ie_adep,'S') = 'S'
	and	nvl(c.ie_situacao,'A') = 'A'
	and	(((obter_se_acm_sn(null,x.ie_se_necessario) = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 ((obter_se_acm_sn(null,x.ie_se_necessario) = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	(((a.dt_liberacao is not null) and
		  (ie_prescr_usuario_p = 'N')) or
		 (a.nm_usuario_nrec = nm_usuario_p))
	group by
		a.nr_sequencia,
		c.nr_seq_pe_proc,
		c.nr_seq_proc,
		y.ds_procedimento,
		x.ie_se_necessario,
		x.cd_intervalo,
		w.ds_prescricao,
		x.ie_suspenso,
		a.dt_liberacao
	)
group by
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_seq_pe_prescr,nr_seq_pe_proc,ie_agrupar_acm_sn_p), 'N', nr_seq_pe_prescr, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_seq_pe_prescr,nr_seq_pe_proc,ie_agrupar_acm_sn_p), 'N', nr_seq_pe_proc, null), null),	
	cd_intervencao,
	ds_intervencao,
	ie_acm_sn,	
	cd_intervalo,
	ds_prescricao,
	--decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_seq_pe_prescr,nr_seq_pe_proc,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null);
	null,
	ds_intervalo,
	ie_liberado;

begin

ie_copiar_w 	:= plt_obter_se_item_marcado('E', nr_seq_regra_p);
ds_cor_titulo_w	:= plt_obter_se_item_marcado('E', nr_seq_regra_p);

open c01;
loop
fetch c01 into	nr_seq_pe_prescr_w,
		nr_seq_pe_proc_w,
		cd_intervencao_w,
		ds_intervencao_w,
		ie_acm_sn_w,		
		cd_intervalo_w,
		ds_intervalo_w,
		ie_status_w,
		ds_interv_prescr_w,
		ie_liberado_w;
exit when c01%notfound;
	begin
	select	w_rep_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_rep_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,		
		cd_intervalo,
		ds_prescricao,
		ie_status_item,
		nr_agrupamento,
		ie_diferenciado,
		nr_seq_proc_interno,
		ds_interv_prescr,
		ie_liberado,
		ie_copiar,
		ds_cor_titulo)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'E',
		nr_seq_pe_prescr_w,
		nr_seq_pe_proc_w,
		cd_intervencao_w,
		ds_intervencao_w,
		ie_acm_sn_w,
		cd_intervalo_w,
		ds_intervalo_w,
		ie_status_w,
		0,
		'N',
		0,
		ds_interv_prescr_w,
		ie_liberado_w,
		ie_copiar_w,
		ds_cor_titulo_w);
	end;
end loop;
close c01;

end plt_obter_interv;
/