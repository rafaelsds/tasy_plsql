CREATE OR REPLACE
PROCEDURE Gerar_Horario_Escala_gestao(
			nr_seq_escala_p		Number,
			dt_inicial_p		Date,
			dt_final_p			Date,
			ie_forma_tratar_horario_p	Number,
			nm_usuario_p		Varchar2) is

cont_fds_w		number(1) := 0;				
ie_dia_semana_ww		number(2,0);
ie_gerar_escala_w		boolean := false;
ie_alternar_dias_w		varchar2(1);
ie_dia_semana_w		Number(3,0);
cd_estabelecimento_w	Number(5,0);
dt_inicial_w		Date;
dt_final_w		Date;
dt_escala_w		Date;
dt_escala_aux_w		Date; 
hr_atual_w		Date;
hr_inicial_w		Date;
dt_atual_ww		Date;
hr_final_w		Date;
qt_escala_w		Number(5,0);
nr_sequencia_w		Number(10,0)    := 0;
ie_completa_w		Varchar2(1);
ie_feriado_w		Varchar2(1); 
ie_gerar_todos_hor_w	Varchar2(1);
cd_pessoa_fisica_w	Varchar2(10);
qt_escala_dup_w		Number(5,0);
qt_dia_semana_w		Number(5,0);
ie_dia_semana_mes_w	Varchar2(5);
cd_cbo_sus_w		number(6);
cd_profissional_w		varchar2(10);
nr_seq_escala_horario_w	number(10);
qt_dias_periodo_p		number(10);

CURSOR C01 IS
Select 	a.hr_inicial,
	a.hr_final,
	a.cd_pessoa_fisica,
	a.ie_dia_semana_mes,
	a.cd_cbo_sus,
	a.nr_sequencia,
	nvl(ie_alternar_dias,'N'),
	ie_dia_semana
From 	Escala_Horario a
where 	a.nr_seq_escala	= nr_seq_escala_p
and	(((a.ie_dia_semana	= ie_dia_Semana_w) or (a.ie_dia_semana = 0)) or
	(a.ie_dia_semana = 10) and ie_dia_semana_w in (7,1))
and	((a.ie_dia_semana_mes is null) or
	 (a.ie_dia_semana_mes = ie_dia_semana_mes_w))
and	decode(ie_feriado,'S',decode(ie_feriado_w,'S','S','N'),'F',decode(ie_feriado_w,'S','N','S'),'S') = 'S'
and	trunc(nvl(dt_inicio_vigencia,dt_escala_w-1),'dd') <= dt_escala_w 
Union
Select	hr_inicial,
	hr_final,
	cd_pessoa_fisica,
	ie_dia_semana_mes,
	cd_cbo_sus,
	nr_sequencia,
	nvl(ie_alternar_dias,'N'),
	ie_dia_semana
From 	Escala_Horario
Where 	nr_seq_escala	= nr_seq_escala_p
and	ie_dia_semana	= 9
and	ie_dia_semana_w between 2 and 6
and	decode(ie_feriado,'S',decode(ie_feriado_w,'S','S','N'),'F',decode(ie_feriado_w,'S','N','S'),'S') = 'S'
and	trunc(nvl(dt_inicio_vigencia,dt_escala_w-1),'dd') <= dt_escala_w
and	(not exists (
	select 1
	from 	Escala_horario
	where 	nr_seq_escala	= nr_seq_escala_p
	and 	((ie_dia_semana	= ie_dia_Semana_w) or (ie_dia_Semana = '0'))) or (ie_gerar_todos_hor_w = 'S'))
order by 1,2;

CURSOR C02 IS
Select	dt_inicio,
	dt_fim
From 	Escala_Diaria
where 	nr_seq_escala		= nr_seq_escala_p
and	dt_inicio between dt_inicial_w and dt_final_w
and	ie_completa_w		= 'S'
order by 1,2;

Cursor C03 is
	select	cd_profissional
	from	escala_horario_adic
	where	nr_seq_escala = nr_seq_escala_horario_w
	order by cd_profissional;

BEGIN

dt_inicial_w		:= trunc(dt_inicial_p,'dd');
dt_final_w		:= fim_dia(dt_final_p);

select	nvl(max(ie_completa),'N'),
	nvl(max(ie_gerar_todos_hor),'N')
into	ie_completa_w,
	ie_gerar_todos_hor_w
from	escala
where	nr_sequencia	= nr_seq_escala_p;

select	c.cd_estabelecimento
into	cd_estabelecimento_w
from	escala_classif c,
	escala_grupo b,
	escala a
where	a.nr_sequencia	= nr_seq_escala_p
and	a.nr_seq_grupo	= b.nr_sequencia
and	b.nr_seq_classif	= c.nr_sequencia;

delete	from escala_diaria
where	nr_seq_escala	= nr_seq_escala_p
and	cd_pessoa_fisica	is null
and	dt_inicio		>= dt_inicial_w
and	dt_fim		<= dt_final_w
and	ie_forma_tratar_horario_p = 1;
commit;

dt_escala_w	:= dt_inicial_w;

WHILE dt_escala_w <= dt_final_w LOOP

	select	pkg_date_utils.get_WeekDay(dt_escala_w)
	into	ie_dia_semana_w
	from	dual;

	select	decode(obter_se_feriado(cd_estabelecimento_w, trunc(dt_escala_w,'dd')),0,'N','S')
	into	ie_feriado_w
	from	dual;

	qt_dia_semana_w		:= 0;
	ie_dia_semana_mes_w	:= 0;
	dt_escala_aux_w		:= trunc(dt_final_w,'mm');

	while	dt_escala_aux_w <= trunc(dt_escala_w, 'dd') loop

		if	(pkg_date_utils.get_WeekDay(dt_escala_aux_w) = ie_dia_semana_w) then
			qt_dia_semana_w		:= qt_dia_semana_w + 1;
			ie_dia_semana_mes_w	:= qt_dia_semana_w;
		end if;

		if	(qt_dia_semana_w = 6) then
			qt_dia_semana_w		:= 1;
			ie_dia_semana_mes_w	:= 1;
		end if;

		dt_escala_aux_w	:= dt_escala_aux_w + 1;

	end loop;


	OPEN C01;
	LOOP
	FETCH C01 into
		hr_inicial_w,
		hr_final_w,
		cd_pessoa_fisica_w,
		ie_dia_semana_mes_w,
		cd_cbo_sus_w,
		nr_seq_escala_horario_w,
		ie_alternar_dias_w,
		ie_dia_semana_ww;
	EXIT 	when c01%notfound;
		
		select	nvl(qt_dias_periodo_escala,0)
		into	qt_dias_periodo_p
		from	escala_horario
		where	nr_sequencia = nr_seq_escala_horario_w;
		
		hr_inicial_w	:= to_date(to_char(dt_escala_w,'dd/mm/yyyy') || ' ' ||
				to_char(hr_inicial_w,'hh24:mi:ss'),
				'dd/mm/yyyy hh24:mi:ss');
		
		hr_final_w	:= to_date(to_char(dt_escala_w,'dd/mm/yyyy') || ' ' ||
				to_char(hr_final_w,'hh24:mi:ss'),
				'dd/mm/yyyy hh24:mi:ss');
				
		if (qt_dias_periodo_p > 0) then
			hr_final_w := hr_final_w + qt_dias_periodo_p;
		end if;
			
		select	count(*)
		into	qt_escala_w
		from	escala_diaria a
		where	nr_seq_escala		= nr_seq_escala_p
		and		dt_inicio		= hr_inicial_w;

		if	(hr_final_w < hr_inicial_w) then
			hr_final_w	:= hr_final_w + 1;
		end if;

		qt_escala_dup_w := 0;
		
		if 	(ie_forma_tratar_horario_p = 0) then

			select	count(*)
			into	qt_escala_dup_w
			from	escala_diaria a
			where	nr_seq_escala		= nr_seq_escala_p
			and	dt_inicio		= hr_inicial_w
			and	dt_fim			= hr_final_w
			and	ie_feriado		= nvl(ie_feriado_w,'N');

		end if;
				
		if	(qt_escala_w = 0) and 
			(qt_escala_dup_w = 0) then

			if		(ie_alternar_dias_w = 'S') then
					ie_gerar_escala_w := not ie_gerar_escala_w;
					if		(ie_dia_semana_ww = 10) then
							ie_gerar_escala_w := true;
							cont_fds_w := cont_fds_w + 1;
							if	(cont_fds_w > 2) then
								ie_gerar_escala_w := false;
								if	(cont_fds_w = 4) then
									cont_fds_w := 0;
								end if;
							end if;
					end if;
			end if;
			
			if		((ie_alternar_dias_w = 'S') and (ie_gerar_escala_w)) or
					(ie_alternar_dias_w = 'N') then
			begin
			
			select	escala_diaria_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into escala_diaria(
				nr_sequencia,
				nr_seq_escala,
				dt_atualizacao,
				nm_usuario,
				dt_inicio,
				dt_fim,
				ie_feriado,
				cd_pessoa_fisica,
				cd_cbo_sus,
				ie_profis_aleatorio,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(
				nr_sequencia_w,
				nr_seq_escala_p,
				sysdate,
				nm_usuario_p,
				hr_inicial_w,
				hr_final_w,
				nvl(ie_feriado_w,'N'),
				cd_pessoa_fisica_w,
				cd_cbo_sus_w,
				'N',
				sysdate,
				nm_usuario_p);
				
			open C03;
			loop
			fetch C03 into	
				cd_profissional_w;
			exit when C03%notfound;
				begin
								
				insert	into escala_diaria_adic(
					cd_pessoa_fisica,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_escala_diaria,
					nr_sequencia)
				values(	cd_profissional_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nr_sequencia_w,
					escala_diaria_adic_seq.nextval);
				
				end;
			end loop;
			close C03;
			end;
			end if;
		end if;
	END LOOP;
	CLOSE C01;
dt_escala_w		:= dt_escala_w + 1;
END LOOP;
commit;

hr_atual_w		:= trunc(dt_inicial_w,'dd');
OPEN C02;
LOOP
FETCH C02 into
	hr_inicial_w,
	hr_final_w;		
EXIT 	when c02%notfound;

	qt_escala_dup_w := 0;

	if 	(ie_forma_tratar_horario_p = 0) then

		select	count(*)
		into	qt_escala_dup_w
		from	escala_diaria a
		where	nr_seq_escala	= nr_seq_escala_p
		and	dt_inicio		= nvl(dt_atual_ww,hr_atual_w)
		and	dt_fim		= hr_inicial_w
		and	ie_feriado		= nvl(ie_feriado_w,'N');
	end if;

	if	(hr_atual_w < hr_inicial_w)and 
		(qt_escala_dup_w = 0)  then

		select	escala_diaria_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into escala_diaria(
			nr_sequencia,
			nr_seq_escala,
			dt_atualizacao,
			nm_usuario,
			dt_inicio,
			dt_fim,
			ie_feriado,
			ie_profis_aleatorio,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	nr_sequencia_w,
			nr_seq_escala_p,
			sysdate,
			nm_usuario_p,
			nvl(dt_atual_ww,hr_atual_w),
			hr_inicial_w,
			nvl(ie_feriado_w,'N'),
			'N',
			sysdate,
			nm_usuario_p);
		
	end if;
	
	hr_atual_w			  := hr_inicial_w;
	dt_atual_ww			  := to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' ||	to_char(hr_final_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

END LOOP;
CLOSE C02;
commit;
END Gerar_Horario_Escala_Gestao;
/
