create or replace
procedure Suspender_receita_trat_onc(	nr_seq_paciente_p	Number,
						nr_ciclo_p		Number,
						nm_usuario_p		Varchar2,
						ds_retorno_p	out	Varchar2) is

ds_retorno_w		Varchar2(4000) := '';
ds_receitas_w		Varchar2(4000) := '';
nr_sequencia_w		Number(10);
nr_receita_w		Varchar2(15);

Cursor C01 is
	select	nr_sequencia,
		nr_receita
	from	fa_receita_farmacia
	where	nr_seq_paciente = nr_seq_paciente_p
	and	nr_ciclo = nr_ciclo_p
	and	dt_liberacao is null
	and	dt_cancelamento is null
	order by nr_receita;						

begin

if	(nr_seq_paciente_p is not null) and
	(nr_ciclo_p is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		nr_receita_w;
	exit when C01%notfound;
		begin		
		begin
		fa_cancelar_receita(nr_sequencia_w,nm_usuario_p);		
		exception when others then
			if	(ds_receitas_w is null) then
				ds_receitas_w := nr_receita_w || chr(29);
			else
				ds_receitas_w := ds_receitas_w ||' , '||nr_receita_w || chr(29);
			end if;
		end;		
		end;
	end loop;
	close C01;

	If	(ds_receitas_w is not null) then
	
		ds_retorno_w := Wheb_mensagem_pck.get_texto(307857, 'DS_RECEITAS_W='||ds_receitas_w); --'As seguinte receitas n�o foram canceladas: '|| chr(10)||ds_receitas_w;
	
	end if;
end if;

ds_retorno_p := ds_retorno_w;

end Suspender_receita_trat_onc;
/