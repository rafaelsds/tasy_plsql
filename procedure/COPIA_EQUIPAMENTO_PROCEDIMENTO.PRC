Create or Replace 
PROCEDURE Copia_equipamento_procedimento(
				cd_procedimento_origem_p		Number,
				ie_origem_proced_origem_p		Number,
				cd_procedimento_destino_p		Number,
				ie_origem_proced_destino_p		Number,
				nm_usuario_p				Varchar2) IS

cd_equipamento_w	Number(10);
qt_existe_w		Number(10);

cursor C01 is
	select	cd_equipamento
	from	procedimento_equip
	where	cd_procedimento = cd_procedimento_origem_p
	and	ie_origem_proced = ie_origem_proced_origem_p;

BEGIN
select	count(*)
into	qt_existe_w
from	procedimento_equip
where	cd_procedimento = cd_procedimento_destino_p
and	ie_origem_proced = ie_origem_proced_destino_p;

if	(qt_existe_w = 0) then
	begin
	open c01;
	loop
	fetch c01 into
		cd_equipamento_w;
		exit when c01%notfound;
		begin
		insert into procedimento_equip(
			nr_sequencia,
			cd_procedimento,
			ie_origem_proced,
			cd_equipamento,
			dt_atualizacao,
			nm_usuario)
		values(	procedimento_equip_seq.nextval,
			cd_procedimento_destino_p,
			ie_origem_proced_destino_p,
			cd_equipamento_w,
			sysdate,
			nm_usuario_p);		
		end;
	end loop;
	close c01;
	commit;
	end;
end if;

end Copia_equipamento_procedimento;
/