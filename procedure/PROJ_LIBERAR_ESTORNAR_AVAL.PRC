create or replace procedure proj_liberar_estornar_aval(
		nr_sequencia_p 	number,
		ie_tipo_p 		varchar2,
		nm_usuario_p 	varchar2
) is
begin

if 	( nr_sequencia_p 	is not null ) and
	( ie_tipo_p 	in ('DL', 'A') ) then
	
	if 	( ie_tipo_p = 'DL' ) then
		update 	proj_projeto_aval
		set 	dt_liberacao 	= null,
			nm_usuario_lib 	= null,
			dt_atualizacao 	= sysdate,
			nm_usuario 	= nm_usuario_p
		where 	nr_sequencia = nr_sequencia_p;
	else
		update 	proj_projeto_aval
		set 	ie_situacao 	= 'A',
			dt_aprovacao 	= sysdate,
			nm_usuario_aprov 	= nm_usuario_p,
			dt_atualizacao 	= sysdate,
			nm_usuario 	= nm_usuario_p
		where 	nr_sequencia = nr_sequencia_p;
	end if;

	commit;
end if;

end proj_liberar_estornar_aval;
/