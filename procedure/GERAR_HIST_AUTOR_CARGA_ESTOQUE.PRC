create or replace
procedure gerar_hist_autor_carga_estoque(	nr_sequencia_p		number,
					ie_origem_p		varchar2,
					ds_titulo_p		varchar2,
					ds_historico_p		varchar2,
					nm_usuario_p		varchar2) is 

begin

insert into sup_autor_carga_est_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_autor,
		ie_origem,
		ds_titulo,
		dt_historico,
		ds_historico,
		dt_liberacao,
		nm_usuario_lib)
values(		sup_autor_carga_est_hist_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p,
		ie_origem_p,
		ds_titulo_p,
		sysdate,
		ds_historico_p,
		sysdate,
		nm_usuario_p);


commit;

end gerar_hist_autor_carga_estoque;
/