create or replace
procedure cp_release_care_plan(nr_seq_prescr_p		number,
							nm_usuario_p		Varchar2) is 

nr_atendimento_w	patient_care_plan.nr_atendimento%type;
ie_tipo_w		pe_prescricao.ie_tipo%type;

Cursor c_care_plan is
select	a.nr_sequencia
from	patient_care_plan a
where	a.nr_seq_prescr = nr_seq_prescr_p;
						
begin

if	(nr_seq_prescr_p is not null) then
	begin
	select	ie_tipo
	into	ie_tipo_w
	from	pe_prescricao a
	where	a.nr_sequencia = nr_seq_prescr_p;
	exception
		when no_data_found then
		ie_tipo_w	:= null;
	end;
	
	if	(ie_tipo_w = 'CP') then
		/* update all ie_situacao where ie_situacao = 'A' */
			
		for r_c_care_plan in c_care_plan loop
			/* update all ie_situacao of current prescription to 'A'  */
			UPDATE	patient_care_plan
			SET 	ie_situacao = 'A'
			WHERE	nr_sequencia = r_c_care_plan.nr_sequencia;

			/* update dt_start if this field is null */ 
			UPDATE	patient_care_plan
			SET 	dt_start = sysdate
			WHERE	dt_start IS NULL
			and		si_selected = 'Y'
			and		nr_sequencia = r_c_care_plan.nr_sequencia;

			UPDATE	patient_cp_problem
			SET 	dt_start = sysdate
			WHERE	dt_start IS NULL
			and		si_selected = 'Y'
			and		nr_seq_pat_care_plan = r_c_care_plan.nr_sequencia;	

			UPDATE	patient_cp_goal
			SET		dt_start = sysdate
			WHERE	dt_start IS NULL
			and		si_selected = 'Y'
			and		nr_seq_pat_care_plan = r_c_care_plan.nr_sequencia;
		end loop;
		
		UPDATE 	patient_care_plan
		SET 	ie_situacao = 'I'
		WHERE 	ie_situacao = 'A'
		and		nr_atendimento = nr_atendimento_w
		and		nr_seq_prescr  <> nr_seq_prescr_p;
		
		/* update dt_liberacao_plano */
		UPDATE	pe_prescricao
		SET 	dt_liberacao_plano = sysdate
		WHERE	nr_sequencia = nr_seq_prescr_p;
	end if;
end if;

end cp_release_care_plan;
/