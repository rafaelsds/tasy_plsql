create or replace
procedure fis_atualizar_dt_envio_rf(
			nm_usuario_p		varchar2,
			nr_sequencia_p		number) is 

begin

update	siscoserv_fatura
set	dt_atualizacao = sysdate,
	dt_geracao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

commit;

end fis_atualizar_dt_envio_rf;
/