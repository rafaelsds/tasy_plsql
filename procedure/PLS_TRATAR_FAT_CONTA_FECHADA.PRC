create or replace
procedure pls_tratar_fat_conta_fechada(	nr_seq_lote_p			pls_lote_faturamento.nr_sequencia%type,
					nr_seq_fatura_p			pls_fatura.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_vinculo_manual_p		varchar2 default 'N') is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:		OS 666875 / 45952

1) Vinculacao de atendimento com guia SADT:
Neste exemplo temos duas contas e na base oficial as contas entraram no mesmo lote de faturamento, mas, em faturas separadas, pois, existia regra de divisao de valor.
Nesta situacao, mesmo que exista regra de divisao de valor, manter as contas/notas em mesma fatura.
Importante: Se uma das contas tivesse alguma pendencia de faturamento (glosa/ocorrencia) a outra conta tambem nao poderia ser faturada.
As duas contas precisavam faturar juntas.

2) Vinculacao de guias com atendimento de Internacao:
Neste exemplo temos uma guia de resumo de internacao, guias de SADT e guias de Honorario Individual.
Nesta situacao, mesmo que exista regra de divisao de valor, manter as contas/notas em mesma fatura.
Importante: Se uma das contas tivesse alguma pendencia de faturamento (glosa/ocorrencia) as outras contas tambem nao poderiam ser faturadas. 
As contas precisavam faturar juntas.

ie_vinculo_manual_p - Identifica se a conta foi vinculada de forma manual ou nao, somente deve ser utilizada  na rotina de vinculo manual pois vai contra o conceito de conta fechada
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_fatura_ref_w		pls_fatura.nr_sequencia%type;
nr_seq_fat_evento_ref_w		pls_fatura_evento.nr_sequencia%type;
nr_seq_fat_conta_ref_w		pls_fatura_conta.nr_sequencia%type;
nr_seq_conta_ref_w		pls_conta.nr_sequencia%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
cd_guia_referencia_w		pls_conta.cd_guia_ok%type;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
nr_seq_conta_pos_w		pls_conta_pos_estabelecido.nr_sequencia%type;
nr_seq_regra_fat_w		pls_lote_faturamento.nr_seq_regra_fat%type;
ie_conta_fechada_w		pls_regra_faturamento.ie_conta_fechada%type;
qt_conta_fora_w			pls_integer;
nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_mat.nr_sequencia%type;
vl_beneficiario_w		pls_conta_pos_estabelecido.vl_beneficiario%type;
vl_administracao_w		pls_conta_pos_estabelecido.vl_administracao%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_conta_nova_w		pls_conta.nr_sequencia%type;
nr_seq_fatura_conta_w		pls_fatura_conta.nr_sequencia%type;
nr_seq_evento_ref_w		pls_evento_faturamento.nr_sequencia%type;
nr_seq_segurado_ref_w		pls_segurado.nr_sequencia%type;
qt_guias_w			pls_integer;
qt_conta_fechada_w		pls_integer;
nr_seq_cong_w			pls_segurado.nr_seq_congenere%type;
ie_contas_faturar_w		pls_parametro_faturamento.ie_contas_faturar%type;
nr_seq_pagador_ref_w		pls_fatura.nr_seq_pagador%type;
nr_seq_pos_contab_w		pls_conta_pos_estab_contab.nr_sequencia%type;
ie_conta_referencia_w		varchar2(2) := 'N';
nr_seq_fat_proc_w		pls_fatura_proc.nr_sequencia%type;
nr_seq_fat_mat_w		pls_fatura_mat.nr_sequencia%type;
ie_apresentacao_w		pls_protocolo_conta.ie_apresentacao%type;
nr_seq_fat_evento_novo_w	pls_fatura_evento.nr_sequencia%type;
ie_tipo_atendimento_w		pls_tipo_atendimento.cd_tiss%type;
ie_somente_resumo_w		pls_regra_faturamento.ie_somente_resumo%type;
qt_conta_resumo_w		pls_integer;
qt_guia_resumo_w		pls_integer;
ie_gerar_fat_contab_w		pls_parametro_faturamento.ie_gerar_fat_contab%type;
qt_conta_contest_w		pls_integer;
ie_apresentacao_prot_w		pls_lote_faturamento.ie_apresentacao_prot%type;
ie_tipo_cobranca_w		pls_fatura_proc.ie_tipo_cobranca%type;
nr_seq_pos_estab_taxa_w		pls_conta_pos_estab_taxa.nr_sequencia%type;
nr_seq_pos_taxa_contab_w	pls_conta_pos_taxa_contab.nr_sequencia%type;

Cursor C01 is -- Cursor para obter as contas medicas que estao dentro do lote de faturamento
	-- A nivel de lote
	select	a.nr_sequencia nr_seq_fatura_ref,
		b.nr_sequencia nr_seq_fat_evento_ref,
		c.nr_sequencia nr_seq_fat_conta_ref,
		c.nr_seq_conta,
		b.nr_seq_evento,
		a.nr_seq_pagador
	from	pls_fatura_conta c,
		pls_fatura_evento b,
		pls_fatura a
	where	a.nr_sequencia		= b.nr_seq_fatura
	and	b.nr_sequencia		= c.nr_seq_fatura_evento
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	(c.ie_tipo_cobranca	<> '5' or c.ie_tipo_cobranca is null)
	and	nr_seq_fatura_p is null
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido x
				where	x.nr_seq_conta = c.nr_seq_conta
				and	x.nr_seq_lote_fat = nr_seq_lote_p
				and	x.nr_seq_conta_rec is not null)
	union all
	-- A nivel de fatura
	select	a.nr_sequencia nr_seq_fatura_ref,
		b.nr_sequencia nr_seq_fat_evento_ref,
		c.nr_sequencia nr_seq_fat_conta_ref,
		c.nr_seq_conta,
		b.nr_seq_evento,
		a.nr_seq_pagador
	from	pls_fatura_conta c,
		pls_fatura_evento b,
		pls_fatura a
	where	a.nr_sequencia		= b.nr_seq_fatura
	and	b.nr_sequencia		= c.nr_seq_fatura_evento
	and	a.nr_sequencia		= nr_seq_fatura_p
	and	(c.ie_tipo_cobranca	<> '5' or c.ie_tipo_cobranca is null)
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido x
				where	x.nr_seq_conta = c.nr_seq_conta
				and	x.nr_seq_lote_fat = nr_seq_lote_p
				and	x.nr_seq_conta_rec is not null)
	order by nr_seq_conta;

Cursor C02 is
	select	x.nr_sequencia nr_seq_conta,
		a.nr_sequencia nr_seq_fatura,
		b.nr_sequencia nr_seq_evento_fat,
		c.nr_sequencia nr_seq_fat_conta,
		a.nr_seq_pagador,
		b.nr_seq_evento
	from	pls_conta 		x,
		pls_fatura_conta 	c,
		pls_fatura_evento 	b,
		pls_fatura 		a
	where	a.nr_sequencia		= b.nr_seq_fatura
	and	b.nr_sequencia		= c.nr_seq_fatura_evento
	and	x.nr_sequencia		= c.nr_seq_conta
	and	x.cd_guia_ok		= cd_guia_referencia_w
	and	x.nr_seq_segurado	= nr_seq_segurado_ref_w
	and	a.nr_seq_lote		= nr_seq_lote_p	
	and 	not exists(	select	1
				from	pls_fatura_motivo_imp_cob	z
				where	z.nr_seq_fatura			= a.nr_sequencia
				and	z.nr_seq_conta			= x.nr_sequencia)
	union all
	select	x.nr_sequencia,
		a.nr_sequencia,
		b.nr_sequencia,
		c.nr_sequencia,
		a.nr_seq_pagador,
		b.nr_seq_evento
	from	pls_conta 		x,
		pls_fatura_conta 	c,
		pls_fatura_evento 	b,
		pls_fatura 		a
	where	a.nr_sequencia		= b.nr_seq_fatura
	and	b.nr_sequencia		= c.nr_seq_fatura_evento
	and	x.nr_sequencia		= c.nr_seq_conta
	and	x.cd_guia_ok		= cd_guia_referencia_w
	and	x.nr_seq_segurado	= nr_seq_segurado_ref_w
	and	a.nr_seq_lote		= nr_seq_lote_p	
	and	nr_seq_fatura_p is not null;

Cursor C03 is
	select	x.nr_sequencia,
		x.nr_seq_conta,
		x.nr_seq_conta_proc,
		x.nr_seq_conta_mat,
		nvl(w.vl_custo_operacional,0) - nvl(w.vl_administracao,0) vl_beneficiario,
		decode(nvl(x.vl_administracao,0),0,0,nvl(w.vl_administracao,0)) vl_administracao,
		c.nr_seq_segurado,
		w.nr_sequencia nr_seq_pos_contab,
		'1' ie_tipo_cobranca,
		null nr_seq_pos_estab_taxa,
		null nr_seq_pos_taxa_contab
	from	pls_conta_pos_estab_contab	w,
		pls_conta_pos_estabelecido	x,
		pls_protocolo_conta		b,
		pls_conta			c
	where	c.nr_sequencia			= x.nr_seq_conta 
	and	c.nr_seq_protocolo		= b.nr_sequencia
	and	x.nr_sequencia			= w.nr_seq_conta_pos
	and	c.cd_guia_ok			= cd_guia_referencia_w
	and	c.nr_seq_segurado		= nr_seq_segurado_ref_w
	and	x.ie_status_faturamento		= 'L'
	and	c.ie_status			= 'F'
	and	x.vl_beneficiario > 0
	and	w.nr_seq_lote_fat is null
	and	w.nr_seq_evento_fat is null
	and	x.nr_seq_lote_fat is null
	and	x.nr_seq_evento_fat is null
	and	((x.ie_situacao	= 'A') or (x.ie_situacao is null))
	and	(ie_contas_faturar_w = 'CF' or b.ie_status in ('3', '6'))
	and not exists(	select	1
			from	pls_conta z
			where	z.nr_seq_conta_referencia = c.nr_sequencia
			and	z.nr_seq_ajuste_fat is not null)
	union all
	select	x.nr_sequencia,
		x.nr_seq_conta,
		x.nr_seq_conta_proc,
		x.nr_seq_conta_mat,
		b.vl_taxa vl_beneficiario,
		0 vl_administracao,
		c.nr_seq_segurado,
		null nr_seq_pos_contab,
		decode(a.ie_tipo_taxa,'1','4','2','3') ie_tipo_cobranca,
		a.nr_sequencia nr_seq_pos_estab_taxa,
		b.nr_sequencia nr_seq_pos_taxa_contab
	from	pls_conta_pos_estab_taxa 	a,
		pls_conta_pos_taxa_contab	b,
		pls_conta_pos_estabelecido	x,
		pls_protocolo_conta		p,
		pls_conta			c
	where	c.nr_sequencia			= x.nr_seq_conta 
	and	c.nr_seq_protocolo		= p.nr_sequencia
	and	x.nr_sequencia			= a.nr_seq_conta_pos_estab
	and	a.nr_Sequencia			= b.nr_seq_pos_estab_taxa
	and	c.cd_guia_ok			= cd_guia_referencia_w
	and	c.nr_seq_segurado		= nr_seq_segurado_ref_w
	and	x.ie_status_faturamento		= 'L'
	and	c.ie_status			= 'F'
	and	x.vl_beneficiario > 0
	and	x.nr_seq_lote_fat is null
	and	x.nr_seq_evento_fat is null
	and	((x.ie_situacao	= 'A') or (x.ie_situacao is null))
	and	(ie_contas_faturar_w = 'CF' or p.ie_status in ('3', '6'))
	and not exists(	select	1
			from	pls_conta z
			where	z.nr_seq_conta_referencia = c.nr_sequencia
			and	z.nr_seq_ajuste_fat is not null)
	order by 2;

--cursor utilizado para retirar as contas abertas da fatura nao alterar 	
Cursor C04 is
	select	x.nr_sequencia nr_seq_conta,
		a.nr_sequencia nr_seq_fatura,
		c.nr_sequencia nr_seq_fat_conta
	from	pls_conta x,
		pls_fatura_conta c,
		pls_fatura_evento b,
		pls_fatura a
	where	a.nr_sequencia = b.nr_seq_fatura
	and	b.nr_sequencia = c.nr_seq_fatura_evento
	and	x.nr_sequencia = c.nr_seq_conta
	and	x.cd_guia_ok = cd_guia_referencia_w
	and	x.nr_seq_segurado = nr_seq_segurado_ref_w
	and	a.nr_seq_lote = nr_seq_lote_p;
	
Cursor C05 is
	select	max(c.nr_sequencia) nr_seq_fat_conta_ref,
		d.cd_guia_ok,
		d.nr_seq_segurado
	from	pls_conta		d,
		pls_fatura_conta 	c,
		pls_fatura_evento 	b,
		pls_fatura 		a
	where	a.nr_sequencia		= b.nr_seq_fatura
	and	b.nr_sequencia		= c.nr_seq_fatura_evento
	and	d.nr_sequencia		= c.nr_seq_conta
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	nr_seq_fatura_p is null
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido x
				where	x.nr_seq_conta = c.nr_seq_conta
				and	x.nr_seq_lote_fat = nr_seq_lote_p
				and	x.nr_seq_conta_rec is not null)
	and exists (	select	1
			from	pls_conta_pos_estab_contab	w,
				pls_conta_pos_estabelecido	x,
				pls_protocolo_conta		y,
				pls_conta			j
			where	j.nr_sequencia			= x.nr_seq_conta 
			and	j.nr_seq_protocolo		= y.nr_sequencia
			and	x.nr_sequencia			= w.nr_seq_conta_pos
			and	j.cd_guia_ok			= d.cd_guia_ok
			and	j.nr_seq_segurado		= d.nr_seq_segurado
			and	x.ie_status_faturamento		= 'L'
			and	j.ie_status			= 'F'
			and	x.vl_beneficiario > 0
			and	w.nr_seq_lote_fat is null
			and	w.nr_seq_evento_fat is null
			and	x.nr_seq_lote_fat is null
			and	x.nr_seq_evento_fat is null
			and	((x.ie_situacao	= 'A') or (x.ie_situacao is null))
			and	(ie_contas_faturar_w = 'CF' or y.ie_status in ('3', '6'))
			and not exists(	select	1
					from	pls_conta z
					where	z.nr_seq_conta_referencia = j.nr_sequencia
					and	z.nr_seq_ajuste_fat is not null)
			union
			select	1
			from	pls_conta_pos_estab_taxa 	r,
				pls_conta_pos_taxa_contab	y,
				pls_conta_pos_estabelecido	x,
				pls_protocolo_conta		p,
				pls_conta			j
			where	j.nr_sequencia			= x.nr_seq_conta 
			and	j.nr_seq_protocolo		= p.nr_sequencia
			and	x.nr_sequencia			= r.nr_seq_conta_pos_estab
			and	r.nr_sequencia			= y.nr_seq_pos_estab_taxa
			and	j.cd_guia_ok			= d.cd_guia_ok
			and	j.nr_seq_segurado		= d.nr_seq_segurado
			and	x.ie_status_faturamento		= 'L'
			and	j.ie_status			= 'F'
			and	x.vl_beneficiario > 0
			and	x.nr_seq_lote_fat is null
			and	x.nr_seq_evento_fat is null
			and	((x.ie_situacao	= 'A') or (x.ie_situacao is null))
			and	(ie_contas_faturar_w = 'CF' or p.ie_status in ('3', '6'))
			and not exists(	select	1
					from	pls_conta z
					where	z.nr_seq_conta_referencia = j.nr_sequencia
					and	z.nr_seq_ajuste_fat is not null))
	group by d.cd_guia_ok,
		d.nr_seq_segurado;
					
begin
select	nr_seq_regra_fat,
	nvl(ie_apresentacao_prot,'T')
into	nr_seq_regra_fat_w,
	ie_apresentacao_prot_w
from	pls_lote_faturamento
where	nr_sequencia = nr_seq_lote_p;

select	nvl(max(a.ie_contas_faturar), 'PF'),
	nvl(max(a.ie_gerar_fat_contab),'N')
into	ie_contas_faturar_w,
	ie_gerar_fat_contab_w
from	pls_parametro_faturamento	a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

select	nvl(ie_conta_fechada,'N'),
	nvl(ie_somente_resumo,'N')
into	ie_conta_fechada_w,
	ie_somente_resumo_w
from	pls_regra_faturamento
where	nr_sequencia = nr_seq_regra_fat_w;

--tratamento que ira verificar nas guias vinculadas a uma guia de resumo de internacao se existe ao menos uma conta de resumo de internacao integrada
if	(ie_somente_resumo_w	= 'S') then
	for r_c01_w in C01 loop

		select	cd_guia_ok,
			nr_seq_segurado,
			ie_tipo_guia
		into	cd_guia_referencia_w,
			nr_seq_segurado_ref_w,
			ie_tipo_guia_w
		from	pls_conta
		where	nr_sequencia	= r_c01_w.nr_seq_conta;
		
		--Verificar se o tipo de guia e resumo de internacao neste caso nao precisa verificar mais nada
		if	(ie_tipo_guia_w	!= '5') and	
			(nvl(ie_vinculo_manual_p,'N') = 'N')then
			--Verificar se existe ao menos uma conta de resumo de internacao integrada
			select	count(1)
			into	qt_conta_resumo_w
			from	pls_conta		c,
				pls_protocolo_conta	p
			where	c.cd_guia_ok		= cd_guia_referencia_w
			and	c.nr_seq_segurado	= nr_seq_segurado_ref_w
			and	c.ie_tipo_guia		= '5'
			and	c.ie_status		!= 'C'
			and	p.nr_sequencia		= c.nr_seq_protocolo
			and	p.ie_situacao		in ('D','T');

			if	(qt_conta_resumo_w	= 0) then
				--Verificar se e filha de uma guia solicitacao de internacao
				select	count(1)
				into	qt_guia_resumo_w
				from	pls_guia_plano
				where	cd_guia		= cd_guia_referencia_w
				and	nr_seq_segurado	= nr_seq_segurado_ref_w
				and	ie_tipo_guia	= '1';
				
				if	(qt_guia_resumo_w > 0) then
					
					for r_c04_w in c04 loop
						-- Gravar log de exclusao
						pls_gerar_fatura_log(	nr_seq_lote_p, null, r_c04_w.nr_seq_conta, '1. PLS_TRATAR_FAT_CONTA_FECHADA - Conta removida do faturamento', 'TC', 'N', nm_usuario_p);
					
						delete	pls_fatura_proc
						where	nr_seq_fatura_conta = r_c04_w.nr_seq_fat_conta;
						
						delete	pls_fatura_mat
						where	nr_seq_fatura_conta = r_c04_w.nr_seq_fat_conta;
						
						delete	pls_fatura_conta
						where	nr_sequencia = r_c04_w.nr_seq_fat_conta;
						
						update	pls_conta_pos_estab_contab x
						set	x.nr_seq_evento_fat 		= null,
							x.nr_seq_regra_evento_fat	= null,
							x.nr_seq_lote_fat		= null,
							x.dt_atualizacao		= sysdate,
							x.nm_usuario			= nm_usuario_p
						where	x.nr_seq_conta_pos in (	select	w.nr_sequencia
										from	pls_conta_pos_estabelecido w
										where	w.nr_seq_conta = r_c04_w.nr_seq_conta);

						update	pls_conta_pos_estabelecido
						set	nr_seq_lote_fat		= null,
							nr_seq_regra_evento_fat	= null,
							nr_seq_evento_fat	= null,
							dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p
						where	nr_seq_conta		= r_c04_w.nr_seq_conta;
						
						delete	pls_fatura_motivo_imp_cob z
						where	z.nr_seq_fatura = r_c04_w.nr_seq_fatura
						and	z.nr_seq_conta = r_c04_w.nr_seq_conta;
					end loop;
					commit;
				end if;
			end if;
		end if;	
	end loop;
end if;

if	(ie_conta_fechada_w = 'S') and
	(ie_apresentacao_prot_w != 'R') then
	
	-- anteriormente as contas da guia que nao foram faturadas entravam na forca, agora passamos a remover a conta, caso haja outras contas que nao foram faturadas
	for r_c05_w in c05 loop
		pls_remover_conta_fatura(r_c05_w.nr_seq_fat_conta_ref, 'S', cd_estabelecimento_p, nm_usuario_p);
	end loop;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_fatura_ref_w,
		nr_seq_fat_evento_ref_w,
		nr_seq_fat_conta_ref_w,
		nr_seq_conta_ref_w,
		nr_seq_evento_ref_w,
		nr_seq_pagador_ref_w;
	exit when C01%notfound;
		begin
					
		qt_conta_fechada_w	:= 0;
		qt_conta_fora_w		:= 0;
		ie_apresentacao_w	:= 'A';
		
		begin
		select	x.cd_guia_ok,
			x.ie_tipo_guia,
			x.nr_seq_segurado,
			(select	z.cd_tiss
			from	pls_tipo_atendimento	z
			where	z.ie_situacao		= 'A'
			and	z.nr_sequencia		= x.nr_seq_tipo_atendimento) ie_tipo_atendimento
		into	cd_guia_referencia_w,
			ie_tipo_guia_w,
			nr_seq_segurado_ref_w,
			ie_tipo_atendimento_w
		from	pls_conta		x,
			pls_protocolo_conta	y
		where	y.nr_sequencia			= x.nr_seq_protocolo
		and	x.nr_sequencia			= nr_seq_conta_ref_w
		and	nvl(y.ie_apresentacao,'A') 	!= 'R';
		exception
		when others then
			cd_guia_referencia_w	:= null;
			ie_tipo_guia_w		:= null;
			nr_seq_segurado_ref_w	:= null;
			ie_tipo_atendimento_w	:= null;
		end;
		
		if	(ie_tipo_guia_w is null) and
			(nr_seq_fatura_p is not null) then
			
			select	nvl(max(y.ie_apresentacao),'A')
			into	ie_apresentacao_w
			from	pls_conta		x,
				pls_protocolo_conta	y
			where	y.nr_sequencia		= x.nr_seq_protocolo
			and	x.nr_sequencia		= nr_seq_conta_ref_w;
			
			if	(ie_apresentacao_w = 'R') then
				select	max(x.cd_guia_ok),
					max(x.ie_tipo_guia),
					max(x.nr_seq_segurado)
				into	cd_guia_referencia_w,
					ie_tipo_guia_w,
					nr_seq_segurado_ref_w
				from	pls_conta		x,
					pls_protocolo_conta	y
				where	y.nr_sequencia			= x.nr_seq_protocolo
				and	x.nr_sequencia			= nr_seq_conta_ref_w
				and	y.ie_apresentacao	 	= ie_apresentacao_w;
			end if;

		-- O tipo de guia so sere nulo se a conta for de protocolo reapresentado e a rotina for chamado por lote e nao por fatura
		elsif	(ie_tipo_guia_w is null) then
			select	max(c.cd_guia_ok),
				max(c.ie_tipo_guia),
				max(c.nr_seq_segurado),
				nvl(max(p.ie_apresentacao),'A')
			into	cd_guia_referencia_w,
				ie_tipo_guia_w,
				nr_seq_segurado_ref_w,
				ie_apresentacao_w
			from	pls_conta		c,
				pls_protocolo_conta	p
			where	c.nr_sequencia	   = nr_seq_conta_ref_w
			and	c.nr_seq_protocolo = p.nr_sequencia;
		
		end if;
		
		-- Mesmo se for internacao, tem que verificar se a conta ja teve itens faturados
		if	(ie_tipo_guia_w = '5') and
			(qt_conta_fora_w = 0) then
			select	count(1)
			into	qt_conta_fora_w
			from	pls_conta_pos_estabelecido
			where	nr_seq_conta	= nr_seq_conta_ref_w
			and	nr_seq_lote_fat	!= nr_seq_lote_p;
		end if;
		
		-- Verificar se a conta esta em lote de contestacao que nao esteja concluida, 
		--Esta alteracao e necessario onde em caso como a  Litoral que usa o check box atendimento fechado, ou seja, nao pode fatuar nenhum item da conta enquanto a conta estiver em contestacao e esta nao estiver encerrada. 
		--Precisa faturar tudo junto mesmo que estoure o prazo de faturamento conforme orientacao da Unimed Litoral documentado em OS.
		-- se estiver no lote nao concluido, nao pode entrar em faturamento todo atendimento
		qt_conta_contest_w := 0;
		if	(qt_conta_fora_w = 0) then
			select	count(1)
			into	qt_conta_contest_w
			from	pls_contestacao  c,
				pls_lote_contestacao l
			where	l.nr_sequencia	= c.nr_seq_lote
			and	l.nr_seq_ptu_fatura is not null
			and	l.ie_status	!= 'C'
			and	c.nr_seq_conta	in (	select	a.nr_sequencia
							from	pls_conta a
							where	a.cd_guia_ok = cd_guia_referencia_w
							and	a.nr_seq_segurado = nr_seq_segurado_ref_w);
			
			-- Atribuido valor para nao precisar tratar os proximos "ifs" abaixo com "or", pois se tiver tendo contas ou contestacao vai excluir de qualquer jeito
			qt_conta_fora_w := qt_conta_contest_w;
		end if;		
		
		select	count(1)
		into	qt_guias_w
		from	pls_conta_v
		where	cd_guia_referencia	= cd_guia_referencia_w
		and	nr_seq_segurado		= nr_seq_segurado_ref_w
		and	ie_apresentacao		= ie_apresentacao_w
		and	ie_tipo_guia 		in ('4','5'); -- 4 = Guia de SP/SADT | 5 = Guia de Resumo de Internacao
		
		if	(qt_guias_w > 0) or
			(qt_conta_fora_w > 0) then
			select	decode(count(1),0,'N','S')
			into	ie_conta_referencia_w
			from	pls_conta	x
			where	x.nr_sequencia	= nr_seq_conta_ref_w
			and	x.nr_seq_ajuste_fat is not null
			and	x.nr_seq_conta_referencia is not null
			and	rownum = 1;

			-- unidos os counts que precisam ser validados, nao inserir counts com custo alto abaixo
			if	(qt_conta_fora_w = 0) then
				select sum(qtd)
				into	qt_conta_fora_w
				from	(select	count(1) qtd
					from	pls_conta_v a
					where	a.cd_guia_referencia	= cd_guia_referencia_w
					and	a.nr_seq_segurado 	= nr_seq_segurado_ref_w
					and	a.ie_apresentacao	= ie_apresentacao_w
					and	a.ie_status		= 'F'
					and	not exists(	select	1
								from	pls_conta_pos_estabelecido b
								where	a.nr_sequencia = b.nr_seq_conta
								and	(b.ie_situacao	= 'A'
									or b.ie_situacao is null))
					union all
					select	count(1) qtd
					from	pls_conta_v a
					where	a.cd_guia_referencia	= cd_guia_referencia_w
					and	a.nr_seq_segurado 	= nr_seq_segurado_ref_w
					and	a.ie_status		= 'F'
					and	a.ie_apresentacao	= ie_apresentacao_w
					and	exists(	select	1
							from	pls_conta_pos_estabelecido b
							where	b.ie_status_faturamento	in ('U','P')
							and	a.nr_sequencia		= b.nr_seq_conta
							and	(b.ie_situacao	= 'A'
								or b.ie_situacao is null))
					union all
					select	count(1) qtd
					from	pls_conta		c,
						pls_lote_fat_excecao	e
					where	c.nr_sequencia		= e.nr_seq_conta
					and	c.cd_guia_ok		= cd_guia_referencia_w
					and	c.nr_seq_segurado	= nr_seq_segurado_ref_w
					and	e.nr_seq_lote		= nr_seq_lote_p
					and 	c.ie_status		!= 'C'
					union all
					select	count(1) qtd
					from	pls_conta_v a
					where	a.cd_guia_referencia	= cd_guia_referencia_w
					and	a.nr_seq_segurado 	= nr_seq_segurado_ref_w
					and	a.ie_apresentacao	= ie_apresentacao_w
					and 	a.ie_status		!= 'C'
					and	nvl(a.ie_status_fat, 'N') 	!= 'L');
			end if;

			if	(qt_conta_fora_w = 0) and
				(ie_conta_referencia_w = 'N') and
				(ie_apresentacao_w != 'R') then -- Unimed Litoral (23/11) - Nao faz tratamento de conta fechada para reapresentacao

				select	count(1)
				into	qt_conta_fora_w
				from	pls_protocolo_conta		p,
					pls_conta 			x,
					pls_fatura_conta 		c,
					pls_fatura_evento 		b,
					pls_fatura 			a
				where	a.nr_sequencia			= b.nr_seq_fatura
				and	b.nr_sequencia			= c.nr_seq_fatura_evento
				and	x.nr_sequencia			= c.nr_seq_conta
				and	p.nr_sequencia			= x.nr_seq_protocolo
				and	nvl(p.ie_apresentacao,'A') 	= ie_apresentacao_w
				and	a.ie_cancelamento		is null
				and	x.cd_guia_ok			= cd_guia_referencia_w
				and	x.nr_seq_segurado		= nr_seq_segurado_ref_w
				and	a.nr_seq_lote			<> nr_seq_lote_p;
			end if;
			
			-- Verifica se e 'Guia de SP/SADT' e o tipo de atendimento 'SADT Internado'. Se a guia referencia desta conta nao for 'Guia de Resumo de Internacao' entao nao entrara no faturamento
			if	(ie_tipo_guia_w	= '4') and
				(ie_tipo_atendimento_w = '07') and
				(qt_conta_fora_w = 0) then
				
				select	count(1)
				into	qt_conta_fora_w
				from	pls_conta	x
				where	x.nr_sequencia	= nr_seq_conta_ref_w
				and 	not exists(	select	1
							from	pls_conta		y
							where	y.cd_guia_ok		= x.cd_guia_ok
							and	y.nr_seq_segurado	= x.nr_seq_segurado
							and	y.ie_tipo_guia		= '5');
							
				if	(qt_conta_fora_w > 0) then
					select	count(1)
					into	qt_conta_fora_w
					from	pls_conta	x
					where	x.nr_sequencia	= nr_seq_conta_ref_w
					and 	not exists(	select	1
								from	pls_conta		y
								where	y.cd_guia		= x.cd_guia_ok
								and	y.nr_seq_segurado	= x.nr_seq_segurado
								and	y.ie_tipo_guia		= '5');
				end if;
			end if;
			
			-- Verificar se o parametro nao for por conta fechada, e tiver conta fora de protocolo nao tiver "Liberado para pagamento" e "Pago"
			if	(qt_conta_fora_w = 0) and
				(ie_contas_faturar_w != 'CF') then
				select	count(1)
				into	qt_conta_fora_w
				from	pls_conta_pos_estab_contab	w,
					pls_conta_pos_estabelecido	x,
					pls_protocolo_conta		b,
					pls_conta			c
				where	c.nr_sequencia			= x.nr_seq_conta 
				and	c.nr_seq_protocolo		= b.nr_sequencia
				and	x.nr_sequencia			= w.nr_seq_conta_pos
				and	c.cd_guia_ok			= cd_guia_referencia_w
				and	c.nr_seq_segurado		= nr_seq_segurado_ref_w
				and	x.ie_status_faturamento		= 'L'
				and	c.ie_status			= 'F'
				and	nvl(b.ie_apresentacao,'A') 	= ie_apresentacao_w
				and	x.vl_beneficiario > 0
				and	w.nr_seq_lote_fat is null
				and	w.nr_seq_evento_fat is null
				and	x.nr_seq_lote_fat is null
				and	x.nr_seq_evento_fat is null
				and	((x.ie_situacao	= 'A') or (x.ie_situacao is null))
				and	b.ie_status not in ('3', '6');
			end if;
			
			--  SE PARA A GUIA DE REFERENCIA TIVER CONTA QUE NAO TEM POS-ESTABELECIDO OU ESTEJA PENDENTE DE ACAO DE USUARIO, EXCLUIR AS CONTAS DO FATURAMENTO
			if	(qt_conta_fora_w > 0) and
				(nvl(ie_vinculo_manual_p,'N') = 'N')then

				for r_c04_w in c04 loop	
					-- Gravar log de exclusao
					pls_gerar_fatura_log(	nr_seq_lote_p, null, r_c04_w.nr_seq_conta, '2. PLS_TRATAR_FAT_CONTA_FECHADA - Conta removida do faturamento (2)', 'TC', 'N', nm_usuario_p);
										
					delete	pls_fatura_proc
					where	nr_seq_fatura_conta = r_c04_w.nr_seq_fat_conta;
					
					delete	pls_fatura_mat
					where	nr_seq_fatura_conta = r_c04_w.nr_seq_fat_conta;
					
					delete	pls_fatura_conta
					where	nr_sequencia = r_c04_w.nr_seq_fat_conta;
					
					update	pls_conta_pos_estab_contab x
					set	x.nr_seq_evento_fat		= null,
						x.nr_seq_regra_evento_fat	= null,
						x.nr_seq_lote_fat		= null,
						x.dt_atualizacao		= sysdate,
						x.nm_usuario			= nm_usuario_p
					where	x.nr_seq_conta_pos in (	select	w.nr_sequencia
									from	pls_conta_pos_estabelecido w
									where	w.nr_seq_conta = r_c04_w.nr_seq_conta);
					update	pls_conta_pos_estabelecido
					set	nr_seq_lote_fat		= null,
						nr_seq_regra_evento_fat	= null,
						nr_seq_evento_fat	= null,
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p
					where	nr_seq_conta		= r_c04_w.nr_seq_conta;
					
					delete	pls_fatura_motivo_imp_cob z
					where	z.nr_seq_fatura	= r_c04_w.nr_seq_fatura
					and	z.nr_seq_conta	= r_c04_w.nr_seq_conta;
				end loop;
				commit;
			else
				
				-- PEGAR TODAS AS CONTAS QUE ESTAO EM OUTRA FATURA DO MESMO LOTE DE FATURAMENTO
				for r_c02_w in C02 loop
				
					if	(nr_seq_fatura_ref_w != r_c02_w.nr_seq_fatura) and
						(nr_seq_fat_evento_ref_w != r_c02_w.nr_seq_evento_fat) and
						(nr_seq_pagador_ref_w = r_c02_w.nr_seq_pagador) then
						
						select	max(nr_sequencia)
						into	nr_seq_fat_evento_novo_w
						from	pls_fatura_evento
						where	nr_seq_evento	= r_c02_w.nr_seq_evento
						and	nr_seq_fatura	= nr_seq_fatura_ref_w;
						
						if	(nr_seq_fat_evento_novo_w is null) then
							insert into pls_fatura_evento
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_fatura,
								nr_seq_evento,
								vl_evento)
							values	(pls_fatura_evento_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_fatura_ref_w,
								r_c02_w.nr_seq_evento,
								0) returning nr_sequencia into nr_seq_fat_evento_novo_w;
						end if;
						
						pls_gerar_fatura_log(	nr_seq_lote_p, null, r_c02_w.nr_seq_conta, '3. PLS_TRATAR_FAT_CONTA_FECHADA - Conta movida para outro evento faturamento', 'TC', 'N', nm_usuario_p);
						
						update	pls_fatura_conta
						set	nr_seq_fatura_evento	= nr_seq_fat_evento_novo_w,
							cd_guia_referencia	= cd_guia_referencia_w
						where	nr_sequencia		= r_c02_w.nr_seq_fat_conta;
					end if;
				end loop;

				if (nvl(ie_vinculo_manual_p,'N') = 'N') then
					-- FORCAR AS CONTAS DA GUIA QUE NAO FORAM FATURADAS ENTREM NA FORCA
					open C03;
					loop
					fetch C03 into	
						nr_seq_conta_pos_w,
						nr_seq_conta_w,
						nr_seq_conta_proc_w,
						nr_seq_conta_mat_w,
						vl_beneficiario_w,
						vl_administracao_w,
						nr_seq_segurado_w,
						nr_seq_pos_contab_w,
						ie_tipo_cobranca_w,
						nr_seq_pos_estab_taxa_w,
						nr_seq_pos_taxa_contab_w;
					exit when C03%notfound;
						begin
						-- Gravar log de inclusao
						pls_gerar_fatura_log(	nr_seq_lote_p, null, nr_seq_conta_w, '4. PLS_TRATAR_FAT_CONTA_FECHADA - Conta inserida no faturamento (3)', 'TC', 'N', nm_usuario_p);
						
						if	(nvl(vl_beneficiario_w,0) > nvl(vl_administracao_w,0)) and
							(nr_seq_pos_contab_w is null) then

							vl_beneficiario_w := vl_beneficiario_w - vl_administracao_w;
						end if;
						
						update	pls_conta_pos_estabelecido
						set	nr_seq_lote_fat		= nr_seq_lote_p,
							nr_seq_regra_evento_fat	= nr_seq_regra_fat_w,
							nr_seq_evento_fat	= nr_seq_evento_ref_w,
							dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p
						where	nr_sequencia		= nr_seq_conta_pos_w;
						
						if	(nr_seq_pos_contab_w is not null) then
							update	pls_conta_pos_estab_contab
							set	nr_seq_evento_fat	= nr_seq_evento_ref_w,
								nr_seq_regra_evento_fat	= nr_seq_regra_fat_w,
								nr_seq_lote_fat		= nr_seq_lote_p,
								dt_atualizacao		= sysdate,
								nm_usuario		= nm_usuario_p
							where	nr_sequencia		= nr_seq_pos_contab_w;
							
						else
							update	pls_conta_pos_estab_contab
							set	nr_seq_evento_fat	= nr_seq_evento_ref_w,
								nr_seq_regra_evento_fat	= nr_seq_regra_fat_w,
								nr_seq_lote_fat		= nr_seq_lote_p,
								dt_atualizacao		= sysdate,
								nm_usuario		= nm_usuario_p
							where	nr_seq_conta_pos	= nr_seq_conta_pos_w;
							
						end if;
						
						
						if	(nvl(nr_seq_conta_nova_w,0) <> nr_seq_conta_w) then

							nr_seq_conta_nova_w := nr_seq_conta_w;
							
							select	max(nr_sequencia)
							into	nr_seq_fatura_conta_w
							from	pls_fatura_conta
							where	nr_seq_conta		= nr_seq_conta_nova_w
							and	nr_seq_fatura_evento	= nr_seq_fat_evento_ref_w;
							
							if	(nr_seq_fatura_conta_w is null) then
																
								select	max(a.nr_seq_congenere)
								into	nr_seq_cong_w
								from	pls_segurado	a
								where	a.nr_sequencia	= nr_seq_segurado_w;
								
								insert into pls_fatura_conta (
									nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_fatura_evento,
									nr_seq_conta,
									vl_faturado,
									nr_seq_segurado,
									ie_tipo_cobranca,
									cd_conta_debito,
									cd_conta_credito,
									nr_lote_contabil,
									vl_faturado_ndc,
									cd_guia_referencia,
									ie_tipo_vinculacao,
									nr_seq_congenere)
								values(	pls_fatura_conta_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_fat_evento_ref_w,
									nr_seq_conta_w,
									0,
									nr_seq_segurado_w,
									'1',
									null,
									null,
									0,
									0,
									cd_guia_referencia_w,
									'A',
									nr_seq_cong_w) returning nr_sequencia into nr_seq_fatura_conta_w;
							end if;
						end if;
						
						if	(nr_seq_conta_proc_w is not null) then
							if	(ie_gerar_fat_contab_w = 'S') then
								select	max(nr_sequencia)
								into	nr_seq_fat_proc_w
								from	pls_fatura_proc
								where	nr_seq_conta_pos_estab	= nr_seq_conta_pos_w
								and	nr_seq_conta_pos_contab	= nr_seq_pos_contab_w
								and	nr_seq_fatura_conta	= nr_seq_fatura_conta_w;
							else
								select	max(nr_sequencia)
								into	nr_seq_fat_proc_w
								from	pls_fatura_proc
								where	nr_seq_conta_pos_estab	= nr_seq_conta_pos_w
								and	nr_seq_fatura_conta	= nr_seq_fatura_conta_w;
							end if;
							
							if	(nr_seq_fat_proc_w is null) then
							
								insert into pls_fatura_proc (	
									nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_fatura_conta,
									nr_seq_conta_proc,
									vl_faturado,
									cd_conta_debito,
									cd_conta_credito,
									nr_lote_contabil,
									cd_historico,
									nr_seq_esquema,
									cd_classif_cred,
									cd_classif_deb,
									cd_historico_baixa,
									cd_historico_estorno,
									ie_tipo_cobranca,
									ie_liberado,
									nr_seq_conta_pos_estab,
									nr_seq_fat_proc_cancel,
									vl_faturado_ndc,
									cd_conta_debito_ndc,
									cd_conta_credito_ndc,
									cd_classif_cred_ndc,
									cd_classif_deb_ndc,
									nr_seq_esquema_ndc,
									cd_historico_ndc,
									nr_seq_conta_pos_contab,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_taxa_contab)
								values(	pls_fatura_proc_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_fatura_conta_w,
									nr_seq_conta_proc_w,
									nvl(vl_administracao_w,0),
									null,
									null,
									0,
									null,
									null,
									null,
									null,
									null,
									null,
									ie_tipo_cobranca_w,
									null,
									nr_seq_conta_pos_w,
									null,
									nvl(vl_beneficiario_w,0),
									null,
									null,
									null,
									null,
									null,
									null,
									nr_seq_pos_contab_w,
									nr_seq_pos_estab_taxa_w,
									nr_seq_pos_taxa_contab_w);
							else
								update	pls_fatura_proc
								set	vl_faturado	= vl_faturado + nvl(vl_administracao_w,0),
									vl_faturado_ndc	= vl_faturado_ndc + nvl(vl_beneficiario_w,0)
								where	nr_sequencia	= nr_seq_fat_proc_w;
							end if;
							
						elsif	(nr_seq_conta_mat_w is not null) then
							if	(ie_gerar_fat_contab_w = 'S') then
								select	max(nr_sequencia)
								into	nr_seq_fat_mat_w
								from	pls_fatura_mat
								where	nr_seq_conta_pos_estab	= nr_seq_conta_pos_w
								and	nr_seq_conta_pos_contab	= nr_seq_pos_contab_w
								and	nr_seq_fatura_conta	= nr_seq_fatura_conta_w;
							else
								select	max(nr_sequencia)
								into	nr_seq_fat_mat_w
								from	pls_fatura_mat
								where	nr_seq_conta_pos_estab	= nr_seq_conta_pos_w
								and	nr_seq_fatura_conta	= nr_seq_fatura_conta_w;
							end if;
							
							if	(nr_seq_fat_mat_w is null) then
							
								insert into pls_fatura_mat (	
									nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_fatura_conta,
									nr_seq_conta_mat,
									vl_faturado,
									cd_conta_debito,
									cd_conta_credito,
									nr_lote_contabil,
									cd_historico,
									nr_seq_esquema,
									cd_classif_cred,
									cd_classif_deb,
									cd_historico_baixa,
									cd_historico_estorno,
									ie_tipo_cobranca,
									ie_liberado,
									nr_seq_conta_pos_estab,
									nr_seq_fat_mat_cancel,
									vl_faturado_ndc,
									cd_conta_debito_ndc,
									cd_conta_credito_ndc,
									cd_classif_cred_ndc,
									cd_classif_deb_ndc,
									nr_seq_esquema_ndc,
									cd_historico_ndc,
									nr_seq_conta_pos_contab,
									nr_seq_pos_estab_taxa,
									nr_seq_pos_taxa_contab)
								values(	pls_fatura_mat_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_fatura_conta_w,
									nr_seq_conta_mat_w,
									nvl(vl_administracao_w,0),
									null,
									null,
									0,
									null,
									null,
									null,
									null,
									null,
									null,
									ie_tipo_cobranca_w,
									null,
									nr_seq_conta_pos_w,
									null,
									nvl(vl_beneficiario_w,0),
									null,
									null,
									null,
									null,
									null,
									null,
									nr_seq_pos_contab_w,
									nr_seq_pos_estab_taxa_w,
									nr_seq_pos_taxa_contab_w);
							else
								update	pls_fatura_mat
								set	vl_faturado	= vl_faturado + nvl(vl_administracao_w,0),
									vl_faturado_ndc	= vl_faturado_ndc + nvl(vl_beneficiario_w,0)
								where	nr_sequencia	= nr_seq_fat_mat_w;
							end if;
						end if;
						
						pls_atualiza_fat_vl_adic(null,nr_seq_fatura_ref_w);
						
						end;
					end loop;
					close C03;
					commit;
				end if;
				
				select	count(1)
				into	qt_conta_fechada_w
				from	pls_conta
				where	cd_guia_ok	= cd_guia_referencia_w
				and	nr_seq_segurado	= nr_seq_segurado_ref_w
				and	ie_tipo_guia in ('4','5');
				
				if	(qt_conta_fechada_w > 1) then
					update	pls_fatura_conta
					set	ie_conta_fechada	= 'S'
					where	nr_seq_conta in (	select	nr_sequencia
									from	pls_conta
									where	cd_guia_ok	= cd_guia_referencia_w
									and	nr_seq_segurado	= nr_seq_segurado_ref_w)
					and	nr_seq_fatura_evento in	(select	b.nr_sequencia
									from	pls_fatura_evento b,
										pls_fatura a
									where	a.nr_sequencia	= b.nr_seq_fatura
									and	a.nr_seq_lote	= nr_seq_lote_p);
					commit;
				end if;
			end if;
		end if;
		
		qt_guias_w := 0;
		qt_conta_fora_w := 0;
		end;
	end loop;
	close c01;
	
	for r_c01_w in C01 loop
	
		-- Tratamento para acegurar que todas as contas do atendimento estarao dentro do lote de faturamento
		select	cd_guia_ok,
			nr_seq_segurado,
			ie_tipo_guia
		into	cd_guia_referencia_w,
			nr_seq_segurado_ref_w,
			ie_tipo_guia_w
		from	pls_conta
		where	nr_sequencia = r_c01_w.nr_seq_conta;
		
		-- Verificar se o tipo de guia e resumo de internacao neste caso nao precisa verificar mais nada
		if	(ie_tipo_guia_w	!= '5') and	
			(nvl(ie_vinculo_manual_p,'N') = 'N')then
			
			-- Verificar se existe ao menos uma conta de resumo de internacao integrada
			select	count(1)
			into	qt_conta_resumo_w
			from	pls_conta			c,
				pls_conta_pos_estabelecido	p
			where	c.cd_guia_ok		= cd_guia_referencia_w
			and	c.nr_seq_segurado	= nr_seq_segurado_ref_w
			and	c.ie_status		!= 'C'
			and	p.nr_seq_conta		= c.nr_sequencia
			and	p.nr_seq_lote_fat	is null
			and	p.ie_status_faturamento	= 'L';
			
			if	(qt_conta_resumo_w > 0) then
				pls_gerar_fatura_log(	nr_seq_lote_p, null, r_c01_w.nr_seq_conta, '5. PLS_TRATAR_FAT_CONTA_FECHADA - Atendimento nao vinculado corretamente','TC', 'N', nm_usuario_p);
				commit;
			end if;
		end if;
	end loop;
end if;

if	(ie_conta_fechada_w 	= 'S') or
	(ie_somente_resumo_w	= 'S') then
	pls_atualizar_vl_lote_fatura(	nr_seq_lote_p, nm_usuario_p, 'N', 'S');
end if;

end pls_tratar_fat_conta_fechada;
/