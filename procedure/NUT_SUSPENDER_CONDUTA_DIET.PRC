create or replace
procedure Nut_suspender_conduta_diet(	nr_seq_dieta_p		Number,
					nm_usuario_p		Varchar2) is 

begin

if(nr_seq_dieta_p is not null) then

	update	nut_atend_serv_dia_dieta
	set	dt_suspensao = sysdate,
		nm_usuario_suspensao = nm_usuario_p
	where	nr_sequencia = nr_seq_dieta_p;

end if;

commit;

end Nut_suspender_conduta_diet;
/