create or replace
procedure obter_regra_atualizar_barras(
				cd_material_p			in	NUMBER,
				cd_estabelecimento_p		in	Number,
				ie_gera_p			out	varchar2) is

cd_grupo_material_w		NUMBER(3);
cd_subgrupo_w			NUMBER(3);
cd_classe_material_w		NUMBER(5);
ie_barras_material_w		VARCHAR2(1);
nr_regras_w			NUMBER(5);
cd_unidade_w			varchar2(30);
cd_empresa_w			number(4);
ie_tipo_material_w		VARCHAR2(3);
nr_seq_familia_w		number(10);

CURSOR	c01 IS
	select	ie_barras_material
	from	Regra_nf_lote_fornec
	where	nvl(cd_unidade_medida, cd_unidade_w)		= cd_unidade_w
	and	NVL(cd_grupo_material, cd_grupo_material_w)	= cd_grupo_material_w
	and	NVL(cd_subgrupo_material, cd_subgrupo_w)		= cd_subgrupo_w
	and	NVL(cd_classe_material, cd_classe_material_w)	= cd_classe_material_w
	and	NVL(ie_tipo_material, ie_tipo_material_w)		= ie_tipo_material_w
	and	NVL(cd_material, cd_material_p)			= cd_material_p
	and	NVL(cd_estab_regra, cd_estabelecimento_p) = cd_estabelecimento_p
	and	((nvl(nr_seq_familia, nr_seq_familia_w)			= nr_seq_familia_w) or (nr_seq_familia is null))
	and	(cd_empresa = cd_empresa_w or cd_empresa_w = '')
	order by
		nvl(cd_material, 0),	
		nvl(cd_classe_material, 0),
		nvl(cd_subgrupo_material, 0),
		nvl(cd_grupo_material, 0);

BEGIN

select	nvl(cd_empresa,'')
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

select	count(*)
into	nr_regras_w
from	Regra_nf_lote_fornec
where	cd_empresa	= cd_empresa_w
and	NVL(cd_estab_regra, cd_estabelecimento_p) = cd_estabelecimento_p;

if	(nr_regras_w	= 0) then
	ie_barras_material_w	:= 'S';
else
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		nr_seq_familia
	into	cd_grupo_material_w,
		cd_subgrupo_w,
		cd_classe_material_w,
		nr_seq_familia_w
	from	estrutura_material_v
	where	cd_material	= cd_material_p;

	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,255) cd_unidade_medida_compra,
		ie_tipo_material
	into	cd_unidade_w,
		ie_tipo_material_w
	from	material
	where	cd_material = cd_material_p;

	OPEN	c01;
	LOOP
	FETCH	c01 INTO
		ie_barras_material_w;
	EXIT WHEN c01%NOTFOUND;
	END LOOP;
	CLOSE 	c01;
	end;
END IF;

ie_gera_p	:= ie_barras_material_w;

END obter_regra_atualizar_barras;
/
