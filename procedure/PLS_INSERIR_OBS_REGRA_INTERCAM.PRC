create or replace
procedure pls_inserir_obs_regra_intercam
			(	nr_seq_req_item_p	Number,
				nr_seq_guia_item_p	Number,
				ie_tipo_item_p		Varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_segurado_w		Number(10);
ie_tipo_guia_w			Varchar2(4);
nr_seq_congenere_w		Number(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
nr_seq_material_w		Number(10);
ds_observacao_w			Varchar2(4000);
ds_observ_regra_w		Varchar2(4000);
nr_seq_regra_w			Number(10);
nr_seq_requisicao_w		Number(10);
cd_material_ops_w		Varchar2(20);
ds_observacao_itens_w		pls_requisicao_proc.ds_observacao%type;
ie_envia_obs_item_w		pls_regra_observ_interc.ie_envia_obs_item%type;
sg_estado_w			pessoa_juridica.sg_estado%type;
sg_estado_int_w			pessoa_juridica.sg_estado%type;
ie_nacional_w			pls_congenere.ie_nacional%type;
nr_seq_uni_exec_w		pls_requisicao.nr_seq_uni_exec%type;
cd_usuario_plano_w		pls_segurado_carteira.cd_usuario_plano%type;
cd_unimed_benef_w		Varchar2(4);
nr_seq_uni_benef_w		pls_requisicao.nr_seq_uni_exec%type;
ie_tipo_intercambio_w		Varchar2(1);
cd_estabelecimento_w		pls_requisicao.cd_estabelecimento%type;

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		ds_observacao
	from	pls_requisicao_proc
	where	nr_sequencia	= nr_seq_req_item_p;

Cursor C02 is
	select	nr_seq_material,
		ds_observacao
	from	pls_requisicao_mat
	where	nr_sequencia	= nr_seq_req_item_p;

begin

if	(nr_seq_req_item_p	is not null) then
	if	(ie_tipo_item_p	= 'P') then
		begin
			select	a.nr_sequencia,
				a.nr_seq_segurado,
				a.ie_tipo_guia,
				substr(a.ds_observacao,1,4000),
				pls_obter_dados_segurado(a.nr_seq_segurado, 'CR'),
				nr_seq_uni_exec,
				cd_estabelecimento
			into	nr_seq_requisicao_w,
				nr_seq_segurado_w,
				ie_tipo_guia_w,
				ds_observacao_w,
				cd_usuario_plano_w,
				nr_seq_uni_exec_w,
				cd_estabelecimento_w
			from	pls_requisicao_proc	b,
				pls_requisicao		a
			where	a.nr_sequencia	= b.nr_seq_requisicao
			and	b.nr_sequencia	= nr_seq_req_item_p;
		exception
		when others then
			nr_seq_requisicao_w	:= null;
			nr_seq_segurado_w	:= null;
			ie_tipo_guia_w		:= null;
			ds_observacao_w		:= null;
			cd_usuario_plano_w	:= null;
			nr_seq_uni_exec_w	:= null;
		end;
	elsif	(ie_tipo_item_p	= 'M') then
		begin
			select	a.nr_sequencia,
				a.nr_seq_segurado,
				a.ie_tipo_guia,
				substr(a.ds_observacao,1,4000),
				pls_obter_dados_segurado(a.nr_seq_segurado, 'CR'),
				nr_seq_uni_exec,
				cd_estabelecimento
			into	nr_seq_requisicao_w,
				nr_seq_segurado_w,
				ie_tipo_guia_w,
				ds_observacao_w,
				cd_usuario_plano_w,
				nr_seq_uni_exec_w,
				cd_estabelecimento_w
			from	pls_requisicao_mat	b,
				pls_requisicao		a
			where	a.nr_sequencia	= b.nr_seq_requisicao
			and	b.nr_sequencia	= nr_seq_req_item_p;
		exception
		when others then
			nr_seq_requisicao_w	:= null;
			nr_seq_segurado_w	:= null;
			ie_tipo_guia_w		:= null;
			ds_observacao_w		:= null;
			cd_usuario_plano_w	:= null;
			nr_seq_uni_exec_w	:= null;
		end;
	end if;

	begin
		select	nvl(nr_seq_congenere,0)
		into	nr_seq_congenere_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
	exception
	when others then
		nr_seq_congenere_w	:= null;
	end;
	
	
	/* Obter a UF da operadora  - Tasy*/
	select	nvl(max(sg_estado),'X')
	into	sg_estado_w
	from	pessoa_juridica
	where	cd_cgc	= (	select	max(cd_cgc_outorgante)
				from	pls_outorgante
				where	cd_estabelecimento	= nvl(cd_estabelecimento_w, wheb_usuario_pck.get_cd_estabelecimento));

	/* Obter a UF da operadora do beneficiario*/
	select	nvl(max(a.sg_estado),'X'),
		nvl(max(ie_nacional), 'N')
	into	sg_estado_int_w,
		ie_nacional_w
	from	pessoa_juridica	a,
		pls_congenere	b
	where	a.cd_cgc	= b.cd_cgc
	and	b.nr_sequencia	= nr_seq_congenere_w;
		

	if	(ie_nacional_w	= 'S') then
		ie_tipo_intercambio_w := 'N';	-- Nacional
	elsif	(sg_estado_w <> 'X') then
		if	(sg_estado_int_w <> 'X') then
			if	(sg_estado_w	= sg_estado_int_w) then
				ie_tipo_intercambio_w	:= 'E';
			else
				ie_tipo_intercambio_w	:= 'N';
			end if;
		elsif (cd_usuario_plano_w is not null) then
					
			cd_unimed_benef_w	:= lpad(substr(cd_usuario_plano_w,1,4),4,'0');
			
			begin
				select	pls_obter_seq_cooperativa(max(b.cd_cooperativa))
				into	nr_seq_uni_benef_w
				from	pessoa_juridica a,
					pls_congenere	b
				where	b.cd_cgc = a.cd_cgc
				and	length(b.cd_cooperativa) >= 4
				and	b.cd_cooperativa = cd_unimed_benef_w;
			exception
			when others then
				nr_seq_uni_benef_w := nr_seq_uni_exec_w;
			end;
			
			
			if ((nr_seq_uni_exec_w is not null) and (nr_seq_uni_exec_w <> nr_seq_uni_benef_w)) then
				ie_tipo_intercambio_w := pls_obter_tipo_intercambio(nr_seq_uni_benef_w, nvl(cd_estabelecimento_w, wheb_usuario_pck.get_cd_estabelecimento));
			else
				ie_tipo_intercambio_w	:= 'A';
			end if;
		else
			ie_tipo_intercambio_w	:= 'A';
		end if;
	else
		ie_tipo_intercambio_w	:= 'A';
	end if;
end if;

if	(ie_tipo_item_p	= 'P') then
	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		ds_observacao_itens_w;
	exit when C01%notfound;
		begin
		if 	(pls_obter_se_controle_estab('RE') = 'S') then
			select	max(nr_sequencia)
			into	nr_seq_regra_w
			from	pls_regra_observ_interc
			where	(nr_seq_congenere	is null or	nr_seq_congenere	= nr_seq_congenere_w)
			and	(ie_tipo_guia		is null or	ie_tipo_guia		= ie_tipo_guia_w)
			and	(cd_procedimento	is null	or	cd_procedimento		= cd_procedimento_w)
			and	(ie_origem_proced	is null	or	ie_origem_proced	= ie_origem_proced_w)
			and	nr_seq_material		is null
			and	ie_situacao		= 'A'
			and 	cd_estabelecimento 	= wheb_usuario_pck.get_cd_estabelecimento
			and	((ie_tipo_intercambio	is null or	ie_tipo_intercambio	= 'A')
			or	(ie_tipo_intercambio	= ie_tipo_intercambio_w));
		else
			select	max(nr_sequencia)
			into	nr_seq_regra_w
			from	pls_regra_observ_interc
			where	(nr_seq_congenere	is null or	nr_seq_congenere	= nr_seq_congenere_w)
			and	(ie_tipo_guia		is null or	ie_tipo_guia		= ie_tipo_guia_w)
			and	(cd_procedimento	is null	or	cd_procedimento		= cd_procedimento_w)
			and	(ie_origem_proced	is null	or	ie_origem_proced	= ie_origem_proced_w)
			and	nr_seq_material		is null
			and	ie_situacao		= 'A'
			and	((ie_tipo_intercambio	is null or	ie_tipo_intercambio	= 'A')
			or	(ie_tipo_intercambio	= ie_tipo_intercambio_w));
		end if;

		if	(nvl(nr_seq_regra_w,0)	<> 0) then
			if	(pls_obter_se_controle_estab('RE') = 'S') then
				select	substr(ds_observacao,1,4000),
					nvl(ie_envia_obs_item, 'N')
				into	ds_observ_regra_w,
					ie_envia_obs_item_w
				from	pls_regra_observ_interc
				where	nr_sequencia	= nr_seq_regra_w
				and 	cd_estabelecimento 	= wheb_usuario_pck.get_cd_estabelecimento;
			else
				select	substr(ds_observacao,1,4000),
					nvl(ie_envia_obs_item, 'N')
				into	ds_observ_regra_w,
					ie_envia_obs_item_w
				from	pls_regra_observ_interc
				where	nr_sequencia	= nr_seq_regra_w;
			end if;
			
			if	(ie_envia_obs_item_w = 'S') then
				ds_observ_regra_w := ds_observ_regra_w || ds_observacao_itens_w;
			end if;

			if	(ds_observ_regra_w	is not null) then
				if	(ds_observacao_w	is null) then
					ds_observacao_w	:= substr(cd_procedimento_w||' - '||ds_observ_regra_w,1,4000);
				else
					ds_observacao_w	:= substr(ds_observacao_w||' '||cd_procedimento_w||' - '||ds_observ_regra_w,1,4000);
				end if;
				
				update	pls_requisicao
				set	ds_observacao	= substr(ds_observacao_w,1,4000)
				where	nr_sequencia	= nr_seq_requisicao_w;
			end if;
		end if;
		end;
	end loop;
	close C01;
elsif	(ie_tipo_item_p	= 'M') then
	open C02;
	loop
	fetch C02 into	
		nr_seq_material_w,
		ds_observacao_itens_w;
	exit when C02%notfound;
		begin
		if	(pls_obter_se_controle_estab('RE') = 'S') then
			select	max(nr_sequencia)
			into	nr_seq_regra_w
			from	pls_regra_observ_interc
			where	(nr_seq_congenere	is null or	nr_seq_congenere	= nr_seq_congenere_w)
			and	(ie_tipo_guia		is null or	ie_tipo_guia		= ie_tipo_guia_w)
			and	(nr_seq_material	is null	or	nr_seq_material		= nr_seq_material_w)
			and	cd_procedimento		is null
			and	ie_situacao		= 'A'
			and 	cd_estabelecimento 	= wheb_usuario_pck.get_cd_estabelecimento
			and	((ie_tipo_intercambio	is null or	ie_tipo_intercambio	= 'A')
			or	(ie_tipo_intercambio	= ie_tipo_intercambio_w));
		else
			select	max(nr_sequencia)
			into	nr_seq_regra_w
			from	pls_regra_observ_interc
			where	(nr_seq_congenere	is null or	nr_seq_congenere	= nr_seq_congenere_w)
			and	(ie_tipo_guia		is null or	ie_tipo_guia		= ie_tipo_guia_w)
			and	(nr_seq_material	is null	or	nr_seq_material		= nr_seq_material_w)
			and	cd_procedimento		is null
			and	ie_situacao		= 'A'
			and	((ie_tipo_intercambio	is null or	ie_tipo_intercambio	= 'A')
			or	(ie_tipo_intercambio	= ie_tipo_intercambio_w));
		end if;
		
		if	(nvl(nr_seq_regra_w,0)	<> 0) then
			if	(pls_obter_se_controle_estab('RE') = 'S') then
				select	substr(ds_observacao,1,4000),
					nvl(ie_envia_obs_item, 'N')
				into	ds_observ_regra_w,
					ie_envia_obs_item_w
				from	pls_regra_observ_interc
				where	nr_sequencia	= nr_seq_regra_w
				and 	cd_estabelecimento 	= wheb_usuario_pck.get_cd_estabelecimento;
			else
				select	substr(ds_observacao,1,4000),
					nvl(ie_envia_obs_item, 'N')
				into	ds_observ_regra_w,
					ie_envia_obs_item_w
				from	pls_regra_observ_interc
				where	nr_sequencia	= nr_seq_regra_w;
			end if;

			if	(ie_envia_obs_item_w = 'S') then
				ds_observ_regra_w := ds_observ_regra_w || ds_observacao_itens_w;
			end if;

			if	(ds_observ_regra_w	is not null) then
				begin
					select	cd_material_ops
					into	cd_material_ops_w
					from	pls_material
					where	nr_sequencia	= nr_seq_material_w;
				exception
				when others then
					cd_material_ops_w	:= null;
				end;
			
				if	(ds_observacao_w	is null) then
					ds_observacao_w	:= substr(nvl(cd_material_ops_w,nr_seq_material_w)||' - '||ds_observ_regra_w,1,4000);
				else
					ds_observacao_w	:= substr(ds_observacao_w||' '||nvl(cd_material_ops_w,nr_seq_material_w)||' - '||ds_observ_regra_w,1,4000);
				end if;
				
				update	pls_requisicao
				set	ds_observacao	= substr(ds_observacao_w,1,4000)
				where	nr_sequencia	= nr_seq_requisicao_w;
			end if;
		end if;
		end;
	end loop;
	close C02;
end if;

commit;

end pls_inserir_obs_regra_intercam;
/
