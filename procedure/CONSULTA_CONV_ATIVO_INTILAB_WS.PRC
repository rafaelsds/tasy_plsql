CREATE OR REPLACE PROCEDURE consulta_conv_ativo_intilab_ws(
          nr_prontuario_p      number,
          cd_setor_atendimento_p    number,
          cd_convenio_p    out  number,
          cd_plano_convenio_p  out  varchar2,
          ds_erro_p      out  varchar2) is

  cd_pessoa_fisica_w    pessoa_fisica.cd_pessoa_fisica%type;
  nr_atendimento_w      atendimento_paciente.nr_atendimento%type;
  cd_convenio_w         atend_categoria_convenio.cd_convenio%type;
  cd_plano_convenio_w   atend_categoria_convenio.cd_plano_convenio%type;
  cd_setor_atendimento_w  setor_atendimento.cd_setor_atendimento%type;


  ds_retorno_w        varchar2(2000);
  ds_erro_w            varchar2(2000);

  begin


  begin
        select   cd_pessoa_fisica
        into  cd_pessoa_fisica_w
       from     pessoa_fisica
       where    nr_prontuario = nr_prontuario_p;

        exception
        when others then
        ds_erro_w  := obter_desc_expressao(513083,'');
        goto final_ds_erro;
  end;

  begin
  select    cd_setor_atendimento
  into     cd_setor_atendimento_w
  from      setor_atendimento
  where     cd_setor_externo = cd_setor_atendimento_p;

  exception
       when others then
        ds_erro_w  := obter_desc_expressao(619365,'');
      goto final_ds_erro;

  end;

  begin
    select  nvl(max(a.nr_atendimento),0) nr_atendimento
        into      nr_atendimento_w
       from      pessoa_fisica b,
                  atendimento_paciente_v a
        where     a.cd_pessoa_fisica = b.cd_pessoa_fisica
       and       b.nr_prontuario = nr_prontuario_p;

    exception
        when others then
        ds_erro_w  := obter_desc_expressao(894542,'') || ' ( ' || nr_prontuario_p ||' ) ' ||
           obter_desc_expressao(298466,'')  || ' ( '|| cd_setor_atendimento_w ||' ) !';
        goto final_ds_erro;

  end;

  select  lpad(max(cd_convenio),5,0) cd_convenio,
            max(cd_plano_convenio) cd_plano_convenio
  into      cd_convenio_w,
              cd_plano_convenio_w
  from      atend_categoria_convenio a
  where     a.nr_atendimento    = nr_atendimento_w
  and       a.dt_inicio_vigencia  =
              (  select  max(dt_inicio_vigencia)
                from  Atend_categoria_convenio b
                where   nr_atendimento  = nr_atendimento_w
              );

  <<final_ds_erro>>
  cd_convenio_p    := cd_convenio_w;
  cd_plano_convenio_p   := cd_plano_convenio_w;
  ds_erro_p               := substr(ds_erro_w,1,255);

end consulta_conv_ativo_intilab_ws;
/