create or replace
procedure alterar_arquivo_laudo(nr_seq_prescr_proc_p	number) is 

begin

update 	prescr_proc_ditado 
set 	ie_excluido = 'S' 
where 	nr_seq_prescr_proc = nr_seq_prescr_proc_p;

commit;

end alterar_arquivo_laudo;
/