create or replace
function lab_obter_microorg_ant(nr_prescricao_p		number,
				nr_seq_prescr_p		number,
				ie_opcao_p		varchar2)
				return varchar2 is			

ds_retorno_w		varchar2(4000);
nr_seq_resultado_w	exame_lab_resultado.nr_seq_resultado%type;	
qt_microorganismo_w	w_exame_lab_result_antib.qt_microorganismo%type;	
ds_microorganismo_w	cih_microorganismo.ds_microorganismo%type;
				
cursor c01 is
	select	distinct
		a.qt_microorganismo,
		b.ds_microorganismo
	from	w_exame_lab_result_antib a,
		cih_microorganismo b
	where	a.cd_microorganismo = b.cd_microorganismo
	  and	a.nr_seq_resultado = nr_seq_resultado_w
	  and	a.nr_seq_prescr = nr_seq_prescr_p
	  order by 2;
				
begin
ds_retorno_w := '';

select	nvl(max(nr_seq_resultado), 0)
into	nr_seq_resultado_w
from	exame_lab_resultado
where	nr_prescricao = nr_prescricao_p;

if nvl(ie_opcao_p, 'A') = 'D' then
	for r_c01 in c01 loop
		ds_retorno_w := ds_retorno_w || r_c01.ds_microorganismo  || chr(13) || chr(10);
	end loop;
elsif nvl(ie_opcao_p, 'A') = 'Q' then
	for r_c01 in c01 loop
		ds_retorno_w := ds_retorno_w || r_c01.qt_microorganismo  || chr(13) || chr(10);
	end loop;
elsif nvl(ie_opcao_p, 'A') = 'A' then
	for r_c01 in c01 loop
		ds_retorno_w := ds_retorno_w || rpad(r_c01.ds_microorganismo, 50, ' ') || ' ' || r_c01.qt_microorganismo || chr(13) || chr(10);
	end loop;
end if;

return trim(ds_retorno_w);

end lab_obter_microorg_ant;
/