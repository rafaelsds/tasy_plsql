create or replace
procedure grava_avisos_oft(cd_pessoa_fisica_p 	varchar2,
			   ds_observacao_p 	varchar2,
			   nm_usuario_p  	varchar2) is

qt_registro_w 	NUMBER(10);
nr_sequencia_w 	NUMBER(10);

begin
if (cd_pessoa_fisica_p is not null) then
	 select  count(*)
	 into qt_registro_w
	 from oft_observacao_paciente
	 where cd_pessoa_fisica = cd_pessoa_fisica_p;

	if (nvl(qt_registro_w,0) = 0) then
		select oft_observacao_paciente_seq.nextval
		into nr_sequencia_w
		from  dual;

		insert into oft_observacao_paciente(
			nr_sequencia,
			cd_pessoa_fisica,
			ds_observacao,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec)
		values (nr_sequencia_w,
			cd_pessoa_fisica_p,
			ds_observacao_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p);
	else
		update  oft_observacao_paciente
		set 	ds_observacao = ds_observacao_p
		where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		commit;
	end if;
end if;

end grava_avisos_oft;
/