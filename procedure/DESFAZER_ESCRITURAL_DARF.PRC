create or replace
procedure DESFAZER_ESCRITURAL_DARF(	nr_seq_darf_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is

ie_remover_titulo_w	varchar2(1);
nr_seq_escritural_w	number(10);
ds_arquivo_w		varchar2(255);
ie_lib_banco_escrit_w	varchar2(1);
dt_liberacao_w		date;
ie_remessa_retorno_w	varchar2(1);
dt_baixa_w		date;

begin

obter_param_usuario(857,38,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_remover_titulo_w);

select	max(a.nr_seq_escritural)
into	nr_seq_escritural_w
from	darf a
where	a.nr_sequencia	= nr_seq_darf_p;

select	max(a.ds_arquivo),
	max(a.dt_liberacao),
	nvl(max(a.ie_remessa_retorno),'R'),
	max(a.dt_baixa)
into	ds_arquivo_w,
	dt_liberacao_w,
	ie_remessa_retorno_w,
	dt_baixa_w
from	banco_escritural a
where	a.nr_sequencia	= nr_seq_escritural_w;

if	(dt_baixa_w	is not null) then

	/* O pagamento escritural j� foi baixado. N�o pode ser desfeito! */
	wheb_mensagem_pck.exibir_mensagem_abort(228262);

end if;

if	(nvl(ie_remover_titulo_w,'S') = 'N') and
	(ie_remessa_retorno_w = 'R') and
	(ds_arquivo_w is not null) then

	/* O pagamento escritural j� teve arquivo remessa gerado. N�o pode ser desfeito! Par�metro [38] */
	wheb_mensagem_pck.exibir_mensagem_abort(228256);

end if;

select	nvl(max(a.ie_lib_banco_escrit),'N')
into	ie_lib_banco_escrit_w
from	parametros_contas_pagar a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_lib_banco_escrit_w	= 'S') and
	(dt_liberacao_w		is not null) then

	/* Este pagamento j� foi liberado, n�o � poss�vel retirar os t�tulos! */
	wheb_mensagem_pck.exibir_mensagem_abort(228261);

end if;

update	darf
set	nr_seq_escritural	= null,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_darf_p;

delete	from titulo_pagar_escrit
where	nr_seq_escrit	= nr_seq_escritural_w;

delete	from banco_escritural
where	nr_sequencia	= nr_seq_escritural_w;

commit;

end DESFAZER_ESCRITURAL_DARF;
/