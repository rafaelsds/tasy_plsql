create or replace
procedure inserir_conta_atendimento(
			nm_usuario_p		Varchar2,
			nr_seq_interno_p	Number,
			nr_sequencia_p	number,
			nr_seq_item_p	number,
			nr_atendimento_p	number) is 

begin
insert into protocolo_doc_item(	nr_sequencia,
			       	nr_seq_item,
			       	dt_atualizacao,
			       	nm_usuario,
			      	nr_seq_interno,
			       	dt_inclusao_item,
				nr_documento)
		values	(	nr_sequencia_p,
				nr_seq_item_p,									
				sysdate,
				nm_usuario_p,
				nr_seq_interno_p,
				sysdate,
				nr_atendimento_p);
commit;

end inserir_conta_atendimento;
/