create or replace
procedure gerar_protocolo_curativo(	cd_protocolo_p			number,
					nr_seq_medicacao_p		number,
					nr_seq_curativo_p		number,
					nm_usuario_p			varchar2) is 

begin

insert into cur_material(
	nr_sequencia,
	cd_material,
	nr_seq_curativo,
	dt_atualizacao,
	nm_usuario,
	qt_material,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_intervalo)
select	cur_material_seq.nextval,
	cd_material,
	nr_seq_curativo_p,
	sysdate,
	nm_usuario_p,
	nvl(qt_dose,1),
	sysdate,
	nm_usuario_p,
	cd_intervalo
from	protocolo_medic_material
where	cd_protocolo	= cd_protocolo_p
and	nr_sequencia	= nr_seq_medicacao_p
and	ie_agrupador	= 2;
	
commit;

end gerar_Protocolo_Curativo;
/
