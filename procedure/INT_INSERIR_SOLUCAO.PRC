create or replace
procedure int_inserir_solucao(cd_intervalo_p			varchar2,
			cd_perfil_ativo_p		number,
			cd_unidade_medida_p		varchar2,
			cd_unid_med_conc_p		varchar2,
			ds_horarios_p			varchar2,
			ds_justif_troca_disp_p		varchar2,
			ds_motivo_susp_p		varchar2,
			ds_orientacao_p			varchar2,
			ds_orientacao_enf_p		varchar2,
			ds_solucao_p			varchar2,
			dt_atualizacao_p		date,
			dt_extensao_p			date,
			dt_status_p			date,
			dt_suspensao_p			date,
			hr_prim_horario_p		varchar2,
			ie_acm_p			varchar2,
			ie_bomba_infusao_p		varchar2,
			ie_calc_aut_p			varchar2,
			ie_se_necessario_p		varchar2,
			ie_solucao_especial_p		varchar2,
			ie_solucao_pca_p		varchar2,
			ie_status_p			varchar2,
			ie_suspender_p			varchar2,
			ie_suspenso_p			varchar2,
			ie_tipo_dosagem_p		varchar2,
			ie_unid_vel_inf_p		varchar2,
			ie_urgencia_p			varchar2,
			ie_via_aplicacao_p		varchar2,
			nm_usuario_susp_p		varchar2,
			nr_etapas_p			number,
			nr_prescricao_p			number,
			nr_seq_solucao_p		number,
			qt_dosagem_p			number,
			qt_hora_fase_p			number,
			qt_hora_infusao_p		number,
			qt_solucao_total_p		number,
			qt_tempo_aplicacao_p		number,
			nm_usuario_p			varchar2,
			nr_sequencia_p			out number,
			ds_erro_p			out varchar2) is 
			
nr_sequencia_w		number(15);

begin

begin

select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p;

insert into prescr_solucao (cd_intervalo,
			cd_perfil_ativo,
			cd_unidade_medida,
			cd_unid_med_conc,
			ds_horarios,
			ds_justif_troca_disp,
			ds_motivo_susp,
			ds_orientacao,
			ds_orientacao_enf,
			ds_solucao,
			dt_atualizacao,
			dt_extensao,
			dt_status,
			dt_suspensao,
			hr_prim_horario,
			ie_acm,
			ie_bomba_infusao,
			ie_calc_aut,
			ie_se_necessario,
			ie_solucao_especial,
			ie_solucao_pca,
			ie_status,
			ie_suspender,
			ie_suspenso,
			ie_tipo_dosagem,
			ie_unid_vel_inf,
			ie_urgencia,
			ie_via_aplicacao,
			nm_usuario_susp,
			nr_etapas,
			nr_prescricao,
			nr_seq_solucao,
			qt_dosagem,
			qt_hora_fase,
			qt_hora_infusao,
			qt_solucao_total,
			qt_tempo_aplicacao,
			nm_usuario)
			values (
			cd_intervalo_p,
			cd_perfil_ativo_p,
			cd_unidade_medida_p,
			cd_unid_med_conc_p,
			ds_horarios_p,
			ds_justif_troca_disp_p,
			ds_motivo_susp_p,
			ds_orientacao_p,
			ds_orientacao_enf_p,
			ds_solucao_p,
			dt_atualizacao_p,
			dt_extensao_p,
			dt_status_p,
			dt_suspensao_p,
			hr_prim_horario_p,
			ie_acm_p,
			ie_bomba_infusao_p,
			ie_calc_aut_p,
			ie_se_necessario_p,
			ie_solucao_especial_p,
			ie_solucao_pca_p,
			ie_status_p,
			ie_suspender_p,
			ie_suspenso_p,
			ie_tipo_dosagem_p,
			ie_unid_vel_inf_p,
			ie_urgencia_p,
			ie_via_aplicacao_p,
			nm_usuario_susp_p,
			nr_etapas_p,
			nr_prescricao_p,
			nr_seq_solucao_p,
			qt_dosagem_p,
			qt_hora_fase_p,
			qt_hora_infusao_p,
			qt_solucao_total_p,
			qt_tempo_aplicacao_p,
			nm_usuario_p);

commit;

nr_sequencia_p		:= nr_sequencia_w;

exception
when others then
	ds_erro_p	:= substr(SQLERRM,1,255);
end;

end int_inserir_solucao;
/