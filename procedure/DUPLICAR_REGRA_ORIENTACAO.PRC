create or replace
procedure duplicar_regra_orientacao(	nr_sequencia_p		number,
					nm_usuario_p		varchar2) is

qt_existe_w		number(10);
nr_seq_doc_atend_w	number(10);

begin

select	convenio_doc_atend_seq.nextval
into	nr_seq_doc_atend_w
from	dual;

insert into convenio_doc_atend
	(nr_sequencia,
	cd_convenio,
	ie_documento,
	dt_atualizacao,
	nm_usuario,
	cd_estabelecimento,
	cd_plano,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_arquivo,
	cd_categoria,
	ds_documento)
select	nr_seq_doc_atend_w,
	cd_convenio,
	ie_documento,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento,
	cd_plano,
	sysdate,
	nm_usuario_p,
	ds_titulo,
	ds_arquivo,
	cd_categoria,
	' '
from	convenio_doc_atend
where	nr_sequencia = nr_sequencia_p;

copia_campo_long('CONVENIO_DOC_ATEND','DS_DOCUMENTO','WHERE NR_SEQUENCIA = :NR_SEQUENCIA',
								 'NR_SEQUENCIA=' || nr_sequencia_p,'NR_SEQUENCIA=' || nr_seq_doc_atend_w);

select	count(*)
into	qt_existe_w
from	convenio_doc_atend_regra
where	nr_seq_doc_atend = nr_sequencia_p;

if	(qt_existe_w > 0) then
	
	insert into convenio_doc_atend_regra
		(nr_sequencia,
		cd_estabelecimento,
		nr_seq_doc_atend,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_perfil,
		ie_tipo_atendimento,
		ie_situacao,
		nr_seq_classificacao,
		cd_funcao,
		dt_vigencia_inicial,
		dt_vigencia_final,
		cd_empresa,
		hr_inicial,
		hr_final)
	select	convenio_doc_atend_regra_seq.nextval,
		cd_estabelecimento,
		nr_seq_doc_atend_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_perfil,
		ie_tipo_atendimento,
		ie_situacao,
		nr_seq_classificacao,
		cd_funcao,
		dt_vigencia_inicial,
		dt_vigencia_final,
		cd_empresa,
		hr_inicial,
		hr_final
	from	convenio_doc_atend_regra
	where	nr_seq_doc_atend = nr_sequencia_p;

end if;

commit;

end duplicar_regra_orientacao;
/
