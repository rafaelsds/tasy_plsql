create or replace
procedure intpd_gerar_devol_proced(
			cd_procedimento_p 		number,
			nr_seq_lote_fornec_p	number,
			nr_atendimento_p		number,
			nr_seq_matpaci_p		number,
			nr_seq_propaci_p		number,
			qt_devolucao_p 			number,
			cd_setor_atendimento_p	number,
			nm_usuario_p			varchar2) is

nr_sequencia_w				intpd_devol_mat_proc.nr_sequencia%type;
nr_seq_proc_atend_w			procedimento_paciente.nr_sequencia%type;
nr_seq_original_w				intpd_devol_mat_proc.nr_seq_original%type;
qt_procedimento_w				procedimento_paciente.qt_procedimento%type;
qt_total_desejado_devolucao_w		intpd_devol_mat_proc.qt_devolucao%type;
qt_devolvido_proc_w			intpd_devol_mat_proc.qt_devolucao%type;
qt_devolver_w				intpd_devol_mat_proc.qt_devolucao%type;
qt_disponivel_devolucao_w 			intpd_devol_mat_proc.qt_devolucao%type;
nr_atendimento_w				intpd_devol_mat_proc.nr_atendimento%type;
nr_sequencia_log_w		NUMBER(15);
ie_liberado_w			varchar2(1) := 'N';
ds_entrada_w			varchar2(10);

cursor c01 is	
	select 	a.nr_sequencia,
		a.nr_atendimento,
		a.qt_procedimento, 
		(select	nvl(sum(qt_devolucao), 0) 
		from	intpd_devol_mat_proc 
		where	nr_seq_original = a.nr_sequencia
		and	nr_seq_propaci is not null) qt_ja_devolvidos
	from 	procedimento_paciente a
	where 	a.cd_procedimento = cd_procedimento_p
	and 	a.nr_atendimento = nr_atendimento_p
	and		a.cd_setor_atendimento = cd_setor_atendimento_p
	and 	a.qt_procedimento > 0
	and 	a.qt_procedimento - (
		select	nvl(sum(qt_devolucao), 0) 
		from	intpd_devol_mat_proc 
		where	nr_seq_original = a.nr_sequencia
		and	nr_seq_propaci is not null) > 0
	order by 1;

begin

ds_entrada_w := '1';
select log_gera_devol_mat_proc_seq.nextval
into	nr_sequencia_log_w
from dual;

insert into log_gera_devol_mat_proc (
	nr_sequencia,
	nm_usuario,
	dt_inicio)
values	(nr_sequencia_log_w,
	nm_usuario_p,
	sysdate);

commit;

while 	(ie_liberado_w <> 'S')  loop
	begin

	select 	nvl(max('N'),'S')
	into 	ie_liberado_w
	from 	log_gera_devol_mat_proc
	where 	nr_sequencia < nr_sequencia_log_w
	and 	(dt_inicio +1/288) > sysdate
	and 	dt_final is null;

	end;
end loop;

if	(cd_procedimento_p is not null) and
	(nr_atendimento_p is not null) and
	(cd_setor_atendimento_p is not null) and
	(qt_devolucao_p is not null) then
	begin
	
	qt_total_desejado_devolucao_w := qt_devolucao_p * -1;
	
	if (qt_total_desejado_devolucao_w > 0) then
		begin
		
		select nvl(sum(x.qt_disponivel_devolucao),0)
		into qt_disponivel_devolucao_w
		from (select 	a.qt_procedimento - (	select nvl(sum(b.qt_devolucao), 0) 
							from intpd_devol_mat_proc   b
							where b.nr_seq_original = a.nr_sequencia
							and	nr_seq_propaci is not null) qt_disponivel_devolucao
			from 	procedimento_paciente a
			where 	a.cd_procedimento = cd_procedimento_p
			and 	a.nr_atendimento = nr_atendimento_p
			and		a.cd_setor_atendimento = cd_setor_atendimento_p
			and 	a.qt_procedimento > 0
			and 	a.qt_procedimento - (
					select	nvl(sum(qt_devolucao), 0) 
					from	intpd_devol_mat_proc 
					where	nr_seq_original = a.nr_sequencia
					and	nr_seq_propaci is not null) > 0) x;

		if 	(qt_disponivel_devolucao_w > 0) and
			((qt_disponivel_devolucao_w - qt_total_desejado_devolucao_w) >= 0) then
			begin
				open c01;
				loop
				fetch c01 into	
					nr_seq_proc_atend_w,
					nr_atendimento_w,
					qt_procedimento_w,
					qt_devolvido_proc_w;
				exit when c01%notfound or qt_total_desejado_devolucao_w = 0;
					begin
					if (qt_total_desejado_devolucao_w <= (qt_procedimento_w - qt_devolvido_proc_w)) then
						begin
						qt_devolver_w := qt_total_desejado_devolucao_w;
						end;
					else
						begin
						qt_devolver_w := (qt_procedimento_w - qt_devolvido_proc_w);
						end;
					end if;
					
					insert into intpd_devol_mat_proc(	
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_matpaci,	
						nr_seq_propaci,	
						nr_seq_original,
						qt_devolucao, 
						dt_devolucao,
						nr_atendimento)
					values(	intpd_devol_mat_proc_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_matpaci_p,
						nr_seq_propaci_p,
						nr_seq_proc_atend_w,
						qt_devolver_w,
						sysdate,
						nr_atendimento_w);
					
					qt_total_desejado_devolucao_w := qt_total_desejado_devolucao_w - qt_devolver_w;
					end;
				end loop;
				close c01;
			end;
		end if;
		end;
	end if;
	end;
end if;

update  log_gera_devol_mat_proc
set 	dt_final = sysdate
where 	nr_sequencia = nr_sequencia_log_w;

commit;

end intpd_gerar_devol_proced;
/