create or replace
procedure 	atualiza_carteira_selecionada(	cd_usuario_convenio_p	varchar2,
					dt_validade_carteira_p	date,
					nr_atendimento_p		number,
					cd_convenio_p		number,
					cd_categoria_p		varchar2,
					dt_inicio_vigencia_p      	date,
					nm_usuario_p		varchar2) is

begin

update	atend_categoria_convenio
set		cd_usuario_convenio	= cd_usuario_convenio_p,
		dt_validade_carteira	= dt_validade_carteira_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
where	nr_atendimento			= nr_atendimento_p
and		cd_convenio		= cd_convenio_p
and		cd_categoria		= cd_categoria_p
and		dt_inicio_vigencia		= dt_inicio_vigencia_p;


commit;

end 	atualiza_carteira_selecionada;
/