create or replace
function obter_valores_diot(nr_sequencia_p		number,
                                            ie_opcao_p                  varchar2)
 		    	    return number is


vl_retorno_w		number(13,2);
			    
begin

if (ie_opcao_p = '1') then

	select  b.vl_liquido
	into    vl_retorno_w
	from 	nota_fiscal a,
		nota_fiscal_item b,
		nota_fiscal_item_trib c,
		tributo d
	where   a.nr_sequencia = b.nr_sequencia
	and     a.nr_sequencia = nr_sequencia_p
	and     c.cd_tributo = d.cd_tributo
	and     b.nr_item_nf = c.nr_item_nf
	and     c.nr_sequencia = a.nr_sequencia
	and     d.ie_tipo_tributo  =  'IVA'
	and     c.vl_tributo = 0;	

elsif (ie_opcao_p = '2') then

select  sum(vl_liquido)
into    vl_retorno_w  
from    (select b.vl_liquido vl_liquido
	from    nota_fiscal a,
	        nota_fiscal_item b,
	        nota_fiscal_item_trib c,
	        tributo d
	where   a.nr_sequencia = b.nr_sequencia
	and     a.nr_sequencia = nr_sequencia_p
	and     c.cd_tributo = d.cd_tributo
	and     b.nr_item_nf = c.nr_item_nf
	and     c.nr_sequencia = a.nr_sequencia
	and     d.ie_tipo_tributo  <>  'IVA'
	union
                 select  b.vl_liquido vl_liquido
	from 	nota_fiscal a,
		nota_fiscal_item b
	where   a.nr_sequencia = b.nr_sequencia
	and     a.nr_sequencia = nr_sequencia_p
	and     nvl(obter_se_trib_item(a.nr_sequencia, b.nr_item_nf),'N') = 'N'); 
	

end if;

return	vl_retorno_w;

end obter_valores_diot;
/