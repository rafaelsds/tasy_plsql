create or replace procedure enviar_email_avaliacao_precad(
				nr_seq_processo_p  number,
				nr_tipo_avaliacao_p	number,
				nr_nivel_p	number,				
				nm_usuario_p		varchar2
				) is 

ds_remetente_w			varchar2(255);
ds_destinatarios_w		varchar2(1000);
ie_primeiro_w 			varchar2(1) := 'S';
nm_usuario_w			usuario_processo_aval.nm_usuario_avaliacao%type;
ds_destinatario_w		varchar2(255);	
ds_adress_w				varchar2(255);
nr_seq_avaliacao_w 		pre_cad_avaliacao.nr_sequencia%type;
nr_atendimento_w		number;
ds_texto_w				varchar2(1000);
ie_tipo_pro_w 			processo_avaliacao.ie_tipo_processo%type;
cd_tipo_pessoa_w		processo_avaliacao.cd_tipo_pessoa%type := null;
			
cursor c_destinatarios is 
select nm_usuario_avaliacao from usuario_processo_aval usu, processo_avaliacao pa 
where pa.nr_sequencia = usu.nr_processo_aval 
and pa.nr_tipo_avaliacao = nr_tipo_avaliacao_p
and pa.nr_nivel 		 = nr_nivel_p
and pa.ie_tipo_processo	 = ie_tipo_pro_w
and nvl(pa.cd_tipo_pessoa, 0)	 = nvl(cd_tipo_pessoa_w, 0);

begin

	select 
		pro.ie_tipo_processo
	into
		ie_tipo_pro_w
	from	
		processo_pre_cadastro pro
	where	
		pro.nr_sequencia = nr_seq_processo_p;
		
	if (ie_tipo_pro_w = 'FPF') then
		select 
			pf.cd_tipo_pj
		into
			cd_tipo_pessoa_w
		from 
			pessoa_fisica_precad pf
		where pf.nr_seq_processo = nr_seq_processo_p;
			
	elsif(ie_tipo_pro_w = 'FPJ') then
		select 
			pj.cd_tipo_pessoa
		into
			cd_tipo_pessoa_w
		from 
			pessoa_juridica_precad pj
		where pj.nr_seq_processo = nr_seq_processo_p;
		
	elsif(ie_tipo_pro_w = 'MAT') then
		cd_tipo_pessoa_w := 0;
		
	end if;
		

	open c_destinatarios;
	loop
	fetch c_destinatarios into	nm_usuario_w;
	exit when c_destinatarios%notfound;
		begin
			select max(obter_compl_pf(usu.cd_pessoa_fisica, 1, 'M'))
			into ds_destinatario_w
			from usuario usu
			where nm_usuario = nm_usuario_w;
			
			if (ds_destinatario_w is not null) then 
				if (ie_primeiro_w = 'N') then
					ds_destinatarios_w := ds_destinatarios_w || ','  || ds_destinatario_w;
				else 
					ds_destinatarios_w := ds_destinatario_w;
					ie_primeiro_w := 'N';
				end if;
			end if;
		end;
	end loop;
	close c_destinatarios;
	
	select max(obter_compl_pf(usu.cd_pessoa_fisica, 1, 'M'))
	into ds_remetente_w
	from usuario usu where nm_usuario = nm_usuario_p;

	if ((ds_destinatarios_w is not null) and (ds_remetente_w is not null)) then 
		select max(nr_sequencia) 
		into nr_seq_avaliacao_w
		from PRE_CAD_AVALIACAO
		order by nr_sequencia desc;
		ds_texto_w	:= 'Foi gerada uma avalia��o do tipo pre-cadastro para os seguintes e-mails: ' || ds_destinatarios_w;
		ds_adress_w := Wheb_assist_pck.obterParametroFuncao(0,216);

		enviar_email('PRE-CAD',
				ds_texto_w || ' ' ||ds_adress_w||'/#/comcadpr/'||nr_seq_processo_p||'/'||nr_seq_avaliacao_w,
				ds_remetente_w,
				ds_destinatarios_w,
				nm_usuario_p,
				'M');

	end if;

	

end enviar_email_avaliacao_precad;
/
