create or replace procedure gerar_prescr_solu_esquema(nr_seq_cirur_agente_p cirurgia_agente_anestesico.nr_sequencia%type,
													  nr_prescricao_p prescr_medica.nr_prescricao%type,
													  nr_seq_solucao_p prescr_solucao.nr_seq_solucao%type) is
																
qt_total_solucao_w			prescr_material.qt_solucao%type;															
nr_etapas_w					prescr_solucao.nr_etapas%type;
qt_hora_fase_w				prescr_solucao.qt_hora_fase%type;
qt_volume_fase_w			prescr_solucao_esquema.qt_volume%type;
nr_cirurgia_w				cirurgia.nr_cirurgia%type;
nr_seq_pepo_w				cirurgia_agente_anestesico.nr_seq_pepo%type;
ie_modo_registro_w			cirurgia_agente_anest_ocor.ie_modo_registro%type;
nr_seq_mat_cpoe_w			cpoe_material.nr_sequencia%type;
qt_tempo_total_w			number(15,3);
ie_prescr_esquema_w			varchar2(1);

Cursor C02 is
select	a.*, rownum nr_etapa
from 	cirurgia_agente_anest_ocor a
where	nr_seq_cirur_agente = nr_seq_cirur_agente_p
and dt_final_adm is not null
order by nr_sequencia;
	
begin	

select max(nr_cirurgia), 
	max(nr_seq_pepo),
	max(nr_seq_mat_cpoe)
into nr_cirurgia_w,
	nr_seq_pepo_w,
	nr_seq_mat_cpoe_w
from cirurgia_agente_anestesico
where	nr_sequencia = nr_seq_cirur_agente_p;

gerar_resumo_agente(nr_cirurgia_w, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nvl(nr_seq_pepo_w,0));

for c02_w in C02 loop
	ie_modo_registro_w := c02_w.ie_modo_registro;
	if (c02_w.ie_modo_registro = 'V' and c02_w.dt_final_adm is not null) then				
			qt_tempo_total_w :=  round(nvl(qt_tempo_total_w,0) + ((c02_w.dt_final_adm - c02_w.dt_inicio_adm) * 1440) / 60, 3);
	end if;

	nr_etapas_w := nvl(nr_etapas_w, 0) + 1;
	
end loop;

if (nvl(ie_modo_registro_w, 'XPTO') = 'V') then

	select	sum(Obter_conversao_ml(cd_material, qt_dose, cd_unidade_medida))
	into	qt_total_solucao_w
	from	w_agente_adm 
	where 	nr_seq_cir_agente_anest = nr_seq_cirur_agente_p;
	
	if (nvl(qt_total_solucao_w,0) > 0) then
	
		update 	prescr_solucao
		set		qt_solucao_total = nvl(qt_total_solucao_w, 0),
				qt_volume = nvl(qt_total_solucao_w, 0),
				nr_etapas = nr_etapas_w
		where 	nr_seq_solucao = nr_seq_solucao_p
		and		nr_prescricao = nr_prescricao_p;
		
		update    prescr_material
		set    qt_dose = Obter_dose_convertida(cd_material, qt_total_solucao_w, 'ml', cd_unidade_medida_dose),
			   qt_solucao = qt_total_solucao_w
		where nr_prescricao = nr_prescricao_p
		and    nr_sequencia_solucao = nr_seq_solucao_p;
		
		commit;
		
	end if;
else

	select	sum(qt_solucao)
	into	qt_total_solucao_w
	from 	prescr_material
	where	nr_sequencia_solucao = nr_seq_solucao_p
	and		nr_prescricao = nr_prescricao_p;

	qt_total_solucao_w := nvl(qt_total_solucao_w, 0) * nvl(nr_etapas_w, 0);

	update	prescr_solucao
	set	qt_solucao_total = nvl(qt_total_solucao_w, 0),
		qt_volume = nvl(qt_total_solucao_w, 0),
		nr_etapas = nr_etapas_w
	where	nr_seq_solucao = nr_seq_solucao_p
	and	nr_prescricao = nr_prescricao_p;
end if;

for c02_w in C02 loop
	
	select nvl(max('S'),'N')
	into ie_prescr_esquema_w
	from prescr_solucao_esquema
	where nr_prescricao = nr_prescricao_p
	and nr_seq_solucao = nr_seq_solucao_p
	and ds_horario = to_char(c02_w.dt_inicio_adm, 'hh24:mi');
	
	if(ie_prescr_esquema_w = 'N') then
		if (c02_w.ie_modo_registro = 'V') then
			if (c02_w.dt_final_adm is not null) then
				qt_hora_fase_w := round(((c02_w.dt_final_adm - c02_w.dt_inicio_adm) * 1440) / 60,3);
				qt_volume_fase_w := round((qt_total_solucao_w * ((qt_hora_fase_w * 100) / qt_tempo_total_w)) / 100, 3);
			else
				qt_hora_fase_w := 0;
				qt_volume_fase_w := 0;
			end if;
		else 
			qt_volume_fase_w := nvl(c02_w.qt_dose, c02_w.qt_dose_ataque);
		end if;
		
		if (nvl(qt_volume_fase_w, 0 ) > 0) then
			insert into prescr_solucao_esquema (
					nr_sequencia,
					nr_prescricao,
					nr_seq_solucao,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					ds_horario,
					nr_etapa,
					qt_volume,
					qt_dosagem,
					qt_hora_fase
				) values(
					prescr_solucao_esquema_seq.nextval,
					nr_prescricao_p,
					nr_seq_solucao_p,
					sysdate,
					sysdate,
					wheb_usuario_pck.get_nm_usuario,
					wheb_usuario_pck.get_nm_usuario,
					to_char(c02_w.dt_inicio_adm, 'hh24:mi'),
					c02_w.nr_etapa,
					nvl(qt_volume_fase_w, 0),
					c02_w.qt_velocidade_inf,
					qt_hora_fase_w);
		end if;

	
		if (nvl(nr_seq_mat_cpoe_w, 0) > 0) then
			update cpoe_material
			set ds_dose_diferenciada = 	replace(ds_dose_diferenciada || decode(ds_dose_diferenciada,null, '' , '-') || campo_mascara_virgula(Obter_dose_convertida(cd_material, qt_volume_fase_w, 'ml', cd_unidade_medida)), '--', '-'),
				nr_etapas = nr_etapas_w,
				qt_solucao_total = nvl(qt_total_solucao_w, 0),
				qt_dosagem_diferenciada = replace(qt_dosagem_diferenciada || decode(qt_dosagem_diferenciada,null, '' , '-') || campo_mascara_virgula(c02_w.qt_velocidade_inf), '--', '-'),
				ds_horarios = replace(ds_horarios || decode(ds_horarios, null, '' , ' ') || to_char(c02_w.dt_inicio_adm, 'hh24:mi'), '  ', ' '),
				qt_hora_fase_diferenciada = replace(qt_hora_fase_diferenciada || decode(qt_hora_fase_diferenciada, null, '' , '-') || obter_horas_minutos(qt_hora_fase_w*60), '--', '-'),
				ds_volume_diferenciado = replace(ds_volume_diferenciado || decode(ds_volume_diferenciado,null, '' , '-') ||campo_mascara_virgula(qt_volume_fase_w), '--', '-')
			where nr_sequencia = nr_seq_mat_cpoe_w;
			
		end if;
	end if;
end loop;
end gerar_prescr_solu_esquema;
/
