create or replace
procedure ctb_desdobrar_lote_mes(	
		nr_lote_contabil_p	in	number,
		nm_usuario_p	in	varchar2,
		ds_erro_p		out	varchar2) is

ds_erro_w			varchar2(2000);
nr_sequencia_w			number(10,0) := 0;
dt_movimento_w			date;
cd_estabelecimento_w		number(05,0);
nr_seq_mes_ref_w			number(10,0);
nr_lote_contabil_w			number(10,0);
nr_lote_contabil_novo_w		number(10,0);
cd_empresa_w			number(5);
cd_tipo_lote_contabil_w		number(10);
ie_tipo_lote_integrar_w		varchar2(01);

cursor c01 is
select	distinct trunc(dt_movimento,'month')
from	ctb_mes_ref b,
	ctb_movimento a
where	a.nr_lote_contabil 		= nr_lote_contabil_p
and	a.nr_seq_mes_ref		= b.nr_sequencia
and	trunc(a.dt_movimento,'month')	<> b.dt_referencia;

begin

ds_erro_w	:= '';

select 	cd_estabelecimento,
	cd_tipo_lote_contabil
into	cd_estabelecimento_w,
	cd_tipo_lote_contabil_w
from	lote_contabil
where	nr_lote_contabil = nr_lote_contabil_p;

select	cd_empresa
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_w;

select	nvl(max(obter_valor_param_usuario(923, 47, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)), 'N')
into	ie_tipo_lote_integrar_w
from	dual;

/* identificar se existem lanšamentos com data diferente do mes de referencia */
select	min(dt_movimento)
into	dt_movimento_w
from	ctb_movimento a
where	nr_lote_contabil = nr_lote_contabil_p
and	not exists (
	select	1
	from	ctb_mes_ref b
	where	b.dt_referencia = trunc(a.dt_movimento,'month')
	and	b.dt_abertura is not null
	and	b.dt_fechamento is null 
	and     b.cd_empresa = cd_empresa_w);
	

if	(dt_movimento_w is not null)	then
	ds_erro_w := ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(280171) || ' ' ||
		WHEB_MENSAGEM_PCK.get_texto(280172) || chr(13) || chr(10);	
else
	open c01;
	loop
	fetch c01 into
		dt_movimento_w;
	exit when c01%notfound;
		select 	min(nr_sequencia)
		into	nr_seq_mes_ref_w
		from	ctb_mes_ref
		where	dt_referencia	= dt_movimento_w
		and	cd_empresa = cd_empresa_w;

		ctb_gerar_lote_digitacao(nr_seq_mes_ref_w, cd_estabelecimento_w, nm_usuario_p,nr_lote_contabil_novo_w);

		nr_lote_contabil_w	:= nr_lote_contabil_novo_w;
		
		update	lote_contabil
		set	nr_lote_origem = nr_lote_contabil_p
		where	nr_lote_contabil = nr_lote_contabil_w;

		update	ctb_movimento
		set	nr_seq_mes_ref	= nr_seq_mes_ref_w,
			nr_lote_contabil	= nr_lote_contabil_w
		where	nr_lote_contabil	= nr_lote_contabil_p
		and	trunc(dt_movimento,'month') = dt_movimento_w;
	end loop;
	close c01;
	
	if	(ie_tipo_lote_integrar_w = 'S') then	
		update	lote_contabil
		set	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_w,
			dt_integracao		= sysdate
		where	nr_lote_contabil		= nr_lote_contabil_w;
	end if;
	commit;
end if;

ds_erro_p	:= ds_erro_w;

end ctb_desdobrar_lote_mes;
/