create or replace
procedure Fleury_Executar_Gerar_Laudo(
		nr_ficha_fleury_p    		number,
		nr_prescricao_p		number,
		nr_seq_prescr_p		number,
		ds_laudo_p		varchar2,
		nm_usuario_p		varchar2) is

nr_seq_propaci_w	number(10,0)	:= 0;
ie_status_execucao_w	varchar2(3);
dt_exec_proc_w		date;
dt_entrada_unidade_w	date;
cd_setor_atendimento_w	number(5);
nr_atendimento_w	number(10);
nr_seq_laudo_w		number(10);
nr_laudo_w		number(10);
nr_sequencia_w		number(10);
dt_resultado_w		date;
ds_titulo_laudo_w	varchar2(255);
dt_baixa_w		date;
cd_protocolo_w        	number(10);
dt_prev_execucao_w	date;
nr_controle_w		number(15);
nr_seq_log_w		number(10);
nr_prescricao_w		number(15);
cd_cgc_laboratorio_w	varchar2(14);
cd_medico_exec_lab_w	varchar2(10);

begin

nr_prescricao_w	:= nr_prescricao_p;

if    (nvl(nr_prescricao_w,0) = 0) then

    select	Obter_Prescr_Controle(nr_ficha_fleury_p)
    into	nr_prescricao_w
    from	dual;

end if;

if 	(nvl(nr_prescricao_w,0) > 0) and
	(nr_seq_prescr_p is not null) then
	--executa procedimento
	Gerar_Proced_Paciente_Pendente(nr_prescricao_w, nr_seq_prescr_p, nm_usuario_p, null, 942, null, null, nr_seq_propaci_w);
	
	--verifica se gerou procedimento_paciente
	if (nvl(nr_seq_propaci_w,0) > 0) then
		--se procedimento executado, gera laudo
		select	max(b.ie_status_execucao),
			max(b.dt_resultado),
			max(substr(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno),1,255)),
			max(b.dt_baixa),
			max(b.cd_protocolo),
			max(b.dt_prev_execucao),
			max(b.nr_controle),
			max(b.cd_cgc_laboratorio)
		into	ie_status_execucao_w,
			dt_resultado_w,
			ds_titulo_laudo_w,
			dt_baixa_w,
			cd_protocolo_w,
			dt_prev_execucao_w,
			nr_controle_w,
			cd_cgc_laboratorio_w
		from	prescr_procedimento b
		where	b.nr_prescricao = nr_prescricao_w
		and	b.nr_sequencia = nr_seq_prescr_p;
		
		if (somente_numero(ie_status_execucao_w) <> 0) then--para baixa especial (BE)
			if (somente_numero(ie_status_execucao_w) < 20) then--quando procedimento n�o est� executado n�o deve integrar
				--'Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. O procedimento n�o est� executado. � necess�rio executar o mesmo para gerar laudo'||'#@#@');
				wheb_mensagem_pck.exibir_mensagem_abort(264096,'NR_PRESCRICAO='||nr_prescricao_w||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
			end if;
		end if;
		
		select  nvl(max(cd_setor_atendimento), null),
			nvl(max(dt_entrada_unidade), dt_entrada_unidade_w),
			nvl(max(dt_procedimento),sysdate),
			nvl(max(nr_atendimento),0)
		into    cd_setor_atendimento_w,
			dt_entrada_unidade_w,
			dt_exec_proc_w,
			nr_atendimento_w
		from    procedimento_paciente
		where   nr_prescricao = nr_prescricao_w
		and     nr_sequencia_prescricao = nr_seq_prescr_p;
		
		if (nr_atendimento_w = 0) then
			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_w;
		end if;
		
		/*rotina para buscar o m�dico respons�vel do laborat�rio executante*/
		select	max(cd_pessoa_fisica)
		into	cd_medico_exec_lab_w
		from	medico
		where	cd_cgc = '60840055000131'; --cgc lab. fleury
		
		if (cd_medico_exec_lab_w is null) and (cd_cgc_laboratorio_w is not null) then
			
			select	max(cd_pessoa_fisica)
			into	cd_medico_exec_lab_w
			from	medico
			where	cd_cgc = cd_cgc_laboratorio_w;
		
		end if;
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_laudo_w
		from	laudo_paciente
		where	nr_atendimento	= nr_atendimento_w
		and	nr_seq_proc	= nr_seq_propaci_w;
		
		if	(nr_seq_laudo_w > 0) then
			--'Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. J� existe laudo para o exame em quest�o.'||'#@#@');
			wheb_mensagem_pck.exibir_mensagem_abort(264097,'NR_PRESCRICAO='||nr_prescricao_w||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
		end if;
		
		select  	nvl(max(nr_laudo),0) + 1
		into    	nr_laudo_w    
		from    	laudo_paciente
		where   	nr_atendimento = nr_atendimento_w;
		
		if	(nr_laudo_w > 0) then
		
			if (dt_resultado_w > sysdate) then
				dt_resultado_w	:= sysdate;
			end if;
				
			select 	laudo_paciente_seq.NextVal
			into	nr_sequencia_w 
			from	dual;
			
			begin
			insert into laudo_paciente (	
				nr_sequencia,        
				nr_atendimento,
				dt_entrada_unidade,    		  
				nr_laudo,
				nm_usuario,        			  
				dt_atualizacao,
				cd_medico_resp,        		  
				ds_titulo_laudo,
				dt_laudo,        			  
				ie_normal,        			  
				dt_exame,
				nr_prescricao,        		  
				ds_laudo,
				dt_aprovacao,        		  
				nm_usuario_aprovacao,
				cd_protocolo,        		  
				nr_seq_proc,        		  
				nr_seq_prescricao,
				dt_liberacao,        		  
				dt_prev_entrega,
				dt_real_entrega,    	      
				qt_imagem,
				nr_controle,        		  
				nm_usuario_seg_aprov,    	  
				cd_setor_atendimento,
				dt_integracao,
				nm_usuario_liberacao,
				ie_formato)
			values (
				nr_sequencia_w,   	  
				nr_atendimento_w,
				dt_entrada_unidade_w,         	  
				nr_laudo_w,
				nm_usuario_p,            	  
				sysdate,
				cd_medico_exec_lab_w,
				ds_titulo_laudo_w,
				nvl(dt_resultado_w,sysdate),        	  		  
				'S',                		  
				nvl(dt_baixa_w,dt_exec_proc_w),
				nr_prescricao_w,        	  
				ds_laudo_p,
				sysdate,    			  
				nm_usuario_p,
				cd_protocolo_w,            	  
				nr_seq_propaci_w,        	  
				nr_seq_prescr_p,
				sysdate,    			  
				dt_prev_execucao_w,
				null,        	  
				0,
				nr_controle_w,            	  
				nm_usuario_p,            	  
				cd_setor_atendimento_w,
				sysdate,
				nm_usuario_p,
				'2');
			exception
			when others then
				--'Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. Ocorreu o seguinte problema ao tentar inserir o laudo no Tasy: '||'#@#@');
				wheb_mensagem_pck.exibir_mensagem_abort(264098,'NR_PRESCRICAO='||nr_prescricao_w||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
			end;
			
			begin
			Vincular_Procedimento_Laudo(nr_sequencia_w,'N',nm_usuario_p);
			Atualizar_Propaci_Medico_Laudo(nr_sequencia_w, 'EX', nm_usuario_p);
			Atualizar_Propaci_Medico_Laudo(nr_sequencia_w, 'EXC', nm_usuario_p);
			exception
			when others then
				--'Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. Ocorreu o seguinte problema ao atualizar procedimentos do laudo: '||'#@#@');
				wheb_mensagem_pck.exibir_mensagem_abort(264099,'NR_PRESCRICAO='||nr_prescricao_w||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
			end;
			
			if	(dt_baixa_w is null) then
				
				dt_baixa_w := sysdate;
				
				begin
				update	prescr_procedimento
				set	cd_motivo_baixa = 1,
					dt_baixa	= dt_baixa_w
				where   	nr_prescricao	= nr_prescricao_w
				and     	nr_sequencia 	= nr_seq_prescr_p;
				
				exception
				when others then
					--'Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. Ocorreu o seguinte problema ao baixar o procedimento: '||'#@#@');
					wheb_mensagem_pck.exibir_mensagem_abort(264100,'NR_PRESCRICAO='||nr_prescricao_w||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
				end;
			
			end if;
			
			LAB_Gravar_Fleury_Log('Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. Executado procedimento e gerado laudo pela procedure Fleury_Exec_Gerar_Laudo com sucesso',null,null,'S',nm_usuario_p,nr_prescricao_w,'1',nr_seq_log_w);
			commit;
		end if;	
	else
		--'Prescri��o: '||nr_prescricao_w||' Seq.: '||nr_seq_prescr_p||'. N�o foram gerados dados de execu��o para este exame.'||'#@#@'); 
		wheb_mensagem_pck.exibir_mensagem_abort(264101,'NR_PRESCRICAO='||nr_prescricao_w||';NR_SEQ_PRESCR='||nr_seq_prescr_p);
	end if;
end if;
	
end Fleury_Executar_Gerar_Laudo;
/
