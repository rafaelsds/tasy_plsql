create or replace
procedure desfazer_cancelamento_atend(	nr_atendimento_p	number,
					nm_usuario_p		Varchar2) is 


ds_senha_w				atend_senha_internet.ds_senha%type;
ie_tipo_atendimento_w 	atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_episodio_w		atendimento_paciente.nr_seq_episodio%type;
nr_seq_case_type_w		tipo_episodio.nr_sequencia%type;

begin

select 	nvl(max(ds_senha_internet),0)
into	ds_senha_w
from	atendimento_paciente
where	nr_atendimento 	= nr_atendimento_p;

update	atendimento_paciente
set	dt_cancelamento = null,
	nm_usuario_cancelamento = null
where	nr_atendimento	= nr_atendimento_p;

select 	max(a.ds_senha_internet)
into	ds_senha_w
from	atendimento_paciente a,
	atend_senha_internet b
where	a.nr_atendimento 	= nr_atendimento_p
and	a.nr_atendimento	= b.nr_atendimento;

if 	((ds_senha_w is not null) or (ds_senha_w  > 0 ))then	
	gerar_senha_internet(nr_atendimento_p,	null, null, null,ds_senha_w,'');
end if;	

delete	
from	atendimento_cancelamento
where	nr_atendimento = nr_atendimento_p;

if (nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_DE', 'de_AT')) then
	select 	max(ie_tipo_atendimento),
			max(nr_seq_episodio)
	into	ie_tipo_atendimento_w,
			nr_seq_episodio_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_p;
	
	if (nr_seq_episodio_w is not null
	and ie_tipo_atendimento_w = '1') then	
		select 	max(nr_sequencia)
		into 	nr_seq_case_type_w
		from 	tipo_episodio
		where 	ie_tipo = '1'
		and 	ie_situacao = 'A';
		
		if (nr_seq_case_type_w > 0) then
			update 	episodio_paciente
			set 	nr_seq_tipo_episodio = nr_seq_case_type_w
			where 	nr_sequencia = nr_seq_episodio_w;
		end if;
	end if;
end if;

commit;

end desfazer_cancelamento_atend;
/