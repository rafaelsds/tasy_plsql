create or replace
procedure enviar_email_aprov(
			nr_solic_compra_p			number,
			ds_assunto_padrao_p		Varchar2,
			ds_mensagem_p			Varchar2,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number) is


cd_centro_custo_w	number(8);
cd_pessoa_resp_w	varchar2(10);
nm_usuario_origem_w	varchar2(255);
ds_email_origem_w	varchar2(255);
ds_email_w		varchar2(255);
ds_email_remetente_w	varchar2(255);
ie_tipo_mensagem_w	number(5);
ie_momento_envio_w	varchar2(1);
ds_centro_custo_w	varchar2(100);
vl_rateio_w		varchar(60);
ds_mensagem_padrao_w	varchar2(4000);
ds_assunto_w		varchar2(255);
nr_sequencia_w		number(10,0);
ds_fornecedor_w		varchar2(255);
ds_lista_macro_w        varchar2(4000);

Cursor C01 is
	select	distinct nvl(c.cd_pessoa_resp,0)
	from	solic_compra a,
		solic_compra_item_rateio b,
		centro_custo_resp c
	where	a.nr_solic_compra = b.nr_solic_compra
	and	b.cd_centro_custo = c.cd_centro_custo
	and	b.vl_rateio is not null
	and	b.cd_centro_custo is not null
	and	a.nr_solic_compra = nr_solic_compra_p
	and 	ie_tipo_servico in ('SP','SR')
	and	a.dt_autorizacao is not null;

Cursor C02 is
	select	distinct nvl(b.cd_centro_custo,0) cd_centro_custo,
		substr(obter_desc_centro_custo(b.cd_centro_custo),1,100) ds_centro_custo,
		campo_mascara_virgula_casas(nvl(b.vl_rateio,0),2) vl_rateio,
		substr(obter_nome_pf_pj(null,a.cd_fornec_sugerido),1,100) ds_fornecedor
	from	solic_compra a,
		solic_compra_item_rateio b,
		centro_custo_resp c
	where	a.nr_solic_compra = b.nr_solic_compra
	and	b.cd_centro_custo = c.cd_centro_custo
	and	b.vl_rateio is not null
	and	b.cd_centro_custo is not null
	and	a.nr_solic_compra = nr_solic_compra_p
	and	c.cd_pessoa_resp = cd_pessoa_resp_w
	and 	ie_tipo_servico in ('SP','SR')
	and	a.dt_autorizacao is not null;

begin

select	nvl(ie_momento_envio,'I'),
	ie_tipo_mensagem,
	ds_email_remetente,
	nr_sequencia
into	ie_momento_envio_w,
	ie_tipo_mensagem_w,
	ds_email_remetente_w,
	nr_sequencia_w
from	regra_envio_email_compra
where	ie_tipo_mensagem in (75)
and 	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;


if (nr_sequencia_w > 0) then
begin	
	select	max(ds_email),
		nm_usuario
	into	ds_email_origem_w,
		nm_usuario_origem_w
	from	usuario
	where	nm_usuario = nm_usuario_p
	group by nm_usuario;
	
	select	substr(replace_macro(ds_assunto,'@solicitacao',nr_solic_compra_p),1,255)
	into	ds_assunto_w
	from	regra_envio_email_compra
	where	nr_sequencia = nr_sequencia_w;
	
	if (nvl(nr_solic_compra_p,0) <> 0) then
	
	open C01;
	loop
	fetch C01 into
		cd_pessoa_resp_w;
	exit when C01%notfound;
		begin
		select	max(ds_email)
		into	ds_email_w
		from	usuario
		where	cd_pessoa_fisica = cd_pessoa_resp_w
		and	ds_email is not null;
	
		cd_centro_custo_w := null;
		ds_centro_custo_w := null;
		vl_rateio_w := null;
		ds_mensagem_padrao_w:= null;
		ds_lista_macro_w := null;
		
		
		open C02;
		loop
		fetch C02 into			
			cd_centro_custo_w,
			ds_centro_custo_w,
			vl_rateio_w,
			ds_fornecedor_w;			
		exit when C02%notfound;
			begin								
			ds_lista_macro_w := WHEB_MENSAGEM_PCK.get_texto(300298,'DS_LISTA_MACRO_W='|| DS_LISTA_MACRO_W ||';CD_CENTRO_CUSTO_W='|| CD_CENTRO_CUSTO_W ||';DS_CENTRO_CUSTO_W='|| DS_CENTRO_CUSTO_W ||';VL_RATEIO_W='|| VL_RATEIO_W ||';DS_FORNECEDOR_W='|| DS_FORNECEDOR_W);
					/*#@DS_LISTA_MACRO_W#@ Centro de custo: #@CD_CENTRO_CUSTO_W#@  - #@DS_CENTRO_CUSTO_W#@
					Rateio: #@VL_RATEIO_W#@
					Fornecedor: #@DS_FORNECEDOR_W#@*/						
			end;
		end loop;
		close C02;

			select	substr( 
				replace_macro(
				ds_mensagem_padrao,
				'@lista_aprovacao_rateio',ds_lista_macro_w),1,4000)
			into	ds_mensagem_padrao_w
			from	regra_envio_email_compra
			where	nr_sequencia = nr_sequencia_w;			

		if	(ds_email_remetente_w is null) then
			ds_email_remetente_w := ds_email_origem_w;
		end if;
	
		if	(ie_momento_envio_w = 'A') and
			(ds_email_w is not null) then
			begin
			--insert into mhbarth values(ds_email_w || ds_mensagem_padrao_w);
			sup_grava_envio_email(
				'AP',
				ie_tipo_mensagem_w,
				nr_solic_compra_p,
				null,
				null,
				ds_email_w,
				nm_usuario_p,
				ds_email_remetente_w,
				ds_assunto_w,
				ds_mensagem_padrao_w,
				cd_estabelecimento_p,
				nm_usuario_p);
			end;
		elsif (ds_email_w is not null) then
			begin
			--insert into mhbarth values(ds_email_w || ds_mensagem_padrao_w);
			enviar_email(ds_assunto_w,ds_mensagem_padrao_w,ds_email_remetente_w,ds_email_w,nm_usuario_origem_w,'M');
			end;
		end if;
		end;
	end loop;
	close C01;	
		
	end if;
end;
end if;

commit;

end enviar_email_aprov;
/