create or replace
procedure pls_consistir_program_canal
			(	nr_seq_vendedor_p	number,
				dt_programacao_p	varchar2,
				ie_opcao_p		varchar2,
				ds_erro_p	out	varchar2) is 
				
/*
ie_opcao_p
S - Solicitação de lead
P - Cliente
*/				
			
qt_regras_programac_canal_w	number(10);
qt_tempo_min_w			number(10);
qt_visitas_programas_w		number(10);
dt_programacao_w		date;
dt_hora_programada_w		varchar2(20);


begin
qt_visitas_programas_w	:= 0;
dt_programacao_w	:= to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss');

select	count(*)
into	qt_regras_programac_canal_w
from	pls_regra_programacao_vend
where	nr_seq_vendedor	= nr_seq_vendedor_p
and	sysdate between dt_inicio_vigencia and fim_dia(dt_fim_vigencia);

if	(qt_regras_programac_canal_w	> 0) then
	select	max(qt_tempo_min)
	into	qt_tempo_min_w
	from	pls_regra_programacao_vend
	where	nr_seq_vendedor	= nr_seq_vendedor_p
	and	sysdate between dt_inicio_vigencia and fim_dia(dt_fim_vigencia);
	
	if	(qt_tempo_min_w <> 0) then
		if	(ie_opcao_p	= 'S') then
		
			begin
			select	count(*)
			into	qt_visitas_programas_w
			from	pls_tipo_atividade		d,
				pls_solicitacao_historico	c,
				pls_solicitacao_vendedor	b,
				pls_solicitacao_comercial	a
			where	a.nr_sequencia 			= b.nr_seq_solicitacao
			and	a.nr_sequencia			= c.nr_seq_solicitacao
			and	c.ie_tipo_atividade		= d.nr_sequencia
			and	d.ie_programacao		= 'S'
			and	d.ie_situacao			= 'A'
			and	trunc(dt_programacao_w,'dd')	= trunc(c.dt_programacao,'dd')
			and	c.dt_programacao > to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') - qt_tempo_min_w/1440 
			and 	c.dt_programacao < to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') + qt_tempo_min_w/1440
			and	b.nr_seq_vendedor_canal	= nr_seq_vendedor_p
			and	c.dt_liberacao is not null
			and	c.dt_cancelamento_programacao is null;
			exception
			when others then
				qt_visitas_programas_w	:= 0;
			end;
			
			if	(qt_visitas_programas_w > 0) then
				begin
				select	max(to_char(c.dt_programacao,'dd/mm/yyyy hh24:mi:ss'))
				into	dt_hora_programada_w
				from	pls_tipo_atividade		d,
					pls_solicitacao_historico	c,
					pls_solicitacao_vendedor	b,
					pls_solicitacao_comercial	a
				where	a.nr_sequencia 			= b.nr_seq_solicitacao
				and	a.nr_sequencia			= c.nr_seq_solicitacao
				and	c.ie_tipo_atividade		= d.nr_sequencia
				and	d.ie_programacao		= 'S'
				and	d.ie_situacao			= 'A'
				and	trunc(dt_programacao_w,'dd')	= trunc(c.dt_programacao,'dd')
				and	c.dt_programacao > to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') - qt_tempo_min_w/1440 
				and 	c.dt_programacao < to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') + qt_tempo_min_w/1440
				and	b.nr_seq_vendedor_canal	= nr_seq_vendedor_p
				and	c.dt_liberacao is not null
				and	c.dt_cancelamento_programacao is null;
				exception
				when others then
					qt_visitas_programas_w	:= 0;
				end;
			end if;	
			
		elsif	(ie_opcao_p	= 'C') then
			begin
			select	count(*)
			into	qt_visitas_programas_w
			from	pls_comercial_historico	b,
				pls_tipo_atividade	a
			where	b.ie_tipo_atividade		= a.nr_sequencia
			and	a.ie_programacao		= 'S'
			and	a.ie_situacao			= 'A'
			and	trunc(dt_programacao_w,'dd')	= trunc(b.dt_programacao,'dd')
			and	b.dt_programacao > to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') - qt_tempo_min_w/1440 
			and 	b.dt_programacao < to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') + qt_tempo_min_w/1440
			and	exists	(	select	1
						from	pls_solicitacao_vendedor	x
						where	x.nr_seq_vendedor_canal	= nr_seq_vendedor_p
						and	x.nr_seq_cliente is not null	)
			and	b.dt_liberacao is not null;
			exception
			when others then
				qt_visitas_programas_w	:= 0;
			end;
			
			if	(qt_visitas_programas_w > 0) then
				begin
				select	max(to_char(b.dt_programacao,'dd/mm/yyyy hh24:mi:ss'))
				into	dt_hora_programada_w
				from	pls_comercial_historico	b,
					pls_tipo_atividade	a
				where	b.ie_tipo_atividade		= a.nr_sequencia
				and	a.ie_programacao		= 'S'
				and	a.ie_situacao			= 'A'
				and	trunc(dt_programacao_w,'dd')	= trunc(b.dt_programacao,'dd')
				and	b.dt_programacao > to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') - qt_tempo_min_w/1440 
				and 	b.dt_programacao < to_date(dt_programacao_p,'dd/mm/yyyy hh24:mi:ss') + qt_tempo_min_w/1440
				and	exists	(	select	1
							from	pls_solicitacao_vendedor	x
							where	x.nr_seq_vendedor_canal	= nr_seq_vendedor_p
							and	x.nr_seq_cliente is not null	)
				and	b.dt_liberacao is not null;
				exception
				when others then
					qt_visitas_programas_w	:= 0;
				end;
			
			end if;	
		end if;
	end if;
end if;	

if	(qt_visitas_programas_w	<> 0) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(280658, 'DT_HORA_PROGRAMADA_P=' || dt_hora_programada_w);
end if;	

end pls_consistir_program_canal;
/