create or replace
procedure mp_atualizar_unid_org(nr_seq_proc_objeto_p	number,
				nm_usuario_p		varchar2) is

qt_topo_objeto_w	number(10);
qt_topo_raia_w		number(10);
qt_altura_raia_w	number(10);
nr_seq_processo_w	number(10);
nr_seq_processo_ref_w	number(10);
qt_registro_w		number(10);
nr_seq_unid_org_w	number(10);


Cursor c01 is
select	qt_topo,
	qt_altura,
	nr_seq_unid_org
from	mp_processo_raia
where	nr_seq_processo	= nr_seq_processo_w
order by
	qt_topo;

begin

if	(nr_seq_proc_objeto_p is not null) then
	select	qt_topo,
		nr_seq_processo
	into	qt_topo_objeto_w,
		nr_seq_processo_w
	from	mp_processo_objeto
	where	nr_sequencia	= nr_seq_proc_objeto_p;

	open c01;
	loop
	fetch c01 into
		qt_topo_raia_w,
		qt_altura_raia_w,
		nr_seq_unid_org_w;
	exit when c01%notfound;
		if	(qt_topo_objeto_w > qt_topo_raia_w) and
			(qt_topo_objeto_w < qt_topo_raia_w + qt_altura_raia_w) then
			
			select	max(nr_seq_processo_ref)
			into	nr_seq_processo_ref_w
			from	mp_processo_objeto
			where	nr_sequencia = nr_seq_proc_objeto_p;

			if	(nr_seq_processo_ref_w is not null) then
				select	count(*)
				into	qt_registro_w
				from	mp_processo_unid_org
				where	nr_seq_unid_org	= nr_seq_unid_org_w
				and	nr_seq_processo	= nr_seq_processo_ref_w;

				if	(qt_registro_w = 0) then
					delete	from	mp_processo_unid_org
					where	nr_seq_processo	= nr_seq_processo_ref_w;

					insert	into	mp_processo_unid_org
						(nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						nr_seq_unid_org,
						nr_seq_processo)
					values	(mp_processo_unid_org_seq.nextval,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nr_seq_unid_org_w,
						nr_seq_processo_ref_w);
				end if;
			else
				select	count(*)
				into	qt_registro_w
				from	mp_proc_obj_unid_org
				where	nr_seq_unid_org	= nr_seq_unid_org_w
				and	nr_seq_proc_obj = nr_seq_proc_objeto_p;
			

				if	(qt_registro_w = 0) then
					delete	from	mp_proc_obj_unid_org
					where	nr_seq_proc_obj	= nr_seq_proc_objeto_p;

					insert	into	mp_proc_obj_unid_org
						(nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						nr_seq_unid_org,
						nr_seq_proc_obj)
					values	(mp_processo_unid_org_seq.nextval,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nr_seq_unid_org_w,
						nr_seq_proc_objeto_p);
				end if;

			end if;
		else
			select	max(nr_seq_processo_ref)
			into	nr_seq_processo_ref_w
			from	mp_processo_objeto
			where	nr_sequencia = nr_seq_proc_objeto_p;

			if	(nr_seq_processo_ref_w is not null) then
				delete 	from 	mp_processo_unid_org
				where	nr_seq_unid_org	= nr_seq_unid_org_w
				and	nr_seq_processo	= nr_seq_processo_ref_w;
			else
				delete	from	mp_proc_obj_unid_org
				where	nr_seq_unid_org	= nr_seq_unid_org_w
				and	nr_seq_proc_obj = nr_seq_proc_objeto_p;
			end if;
		end if;	

	end loop;
	close c01;
end if;

commit;

end mp_atualizar_unid_org;
/