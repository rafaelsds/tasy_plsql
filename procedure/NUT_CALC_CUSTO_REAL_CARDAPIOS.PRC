create or replace
procedure nut_calc_custo_real_cardapios	(nr_sequencia_p		number) is 

/* AO ALTERAR ESTA PROCEDURE, DEVE SER VERIFICADO SE A PROCEDURE Calcular_Custo_Real_Comp_Rec PRECISA SER ALTERADA TAMB�M */

nr_sequencia_comp_w		number(10);
qt_componente_w			number(15,4);
qt_conversao_w			number(15,4);
vl_custo_real_w			number(15,4);
vl_custo_total_w		number(15,4);
cd_material_w			number(6,0);
qt_pessoa_atend_real_w		number(15,0);
qt_refeicao_w			number(15,0);
cd_estabelecimento_w		number(05,0);
nr_seq_gen_alim_w		number(10);
qt_dias_ult_compra_w		number(15);
ie_considerar_pessoas_w		varchar2(1);
ie_considerar_comp_w		varchar2(1);

Cursor C01 is
	select	n.nr_sequencia,
	nvl(nr_seq_gen_alim_sub,n.nr_seq_gen_alim),
	decode(nr_seq_gen_alim_sub,null,nvl(n.qt_componente_real,decode(ie_considerar_comp_w,'S', n.qt_componente, nvl(j.qt_componente_calc,j.qt_componente))),nvl(n.qt_componente_real,n.qt_componente) * nvl(i.qt_refeicao,1) / nvl(h.qt_refeicao,1)),
	nvl(i.qt_refeicao,1),
	qt_pessoa_atend_real
from	nut_genero_alim g,
	nut_rec_real_comp n,
	nut_receita_real h,
	nut_receita i,
	nut_receita_comp j,
	nut_cardapio d,
    	nut_cardapio_dia e
where	n.nr_seq_gen_alim	= g.nr_sequencia
and	h.nr_sequencia		= n.nr_seq_rec_real
and	i.nr_sequencia		= h.nr_seq_receita
and	j.nr_seq_receita	= i.nr_sequencia
and	j.nr_seq_gen_alim	= g.nr_sequencia
and	e.nr_sequencia		= d.nr_seq_card_dia
and	d.nr_sequencia		= h.nr_seq_cardapio
and	e.nr_sequencia		= nr_sequencia_p
and	nvl(ie_comp_adicional,'N') = 'N'
union all
select	n.nr_sequencia,
	nvl(nr_seq_gen_alim_sub,n.nr_seq_gen_alim),
	nvl(n.qt_componente_real,n.qt_componente) * nvl(i.qt_refeicao,1) / nvl(h.qt_refeicao,1),
	nvl(i.qt_refeicao,1),
	qt_pessoa_atend_real
from	nut_genero_alim g,
	nut_rec_real_comp n,
	nut_receita_real h,
	nut_receita i,
	nut_cardapio d,
    	nut_cardapio_dia e
where	n.nr_seq_gen_alim	= g.nr_sequencia
and	h.nr_sequencia		= n.nr_seq_rec_real
and	i.nr_sequencia		= h.nr_seq_receita
and	e.nr_sequencia		= d.nr_seq_card_dia
and	d.nr_sequencia		= h.nr_seq_cardapio
and	e.nr_sequencia		= nr_sequencia_p
and	nvl(ie_comp_adicional,'N') = 'S';

begin
if	(nr_sequencia_p is not null) then

	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
	qt_dias_ult_compra_w	:= obter_valor_param_usuario(912, 27, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w);
	ie_considerar_pessoas_w := obter_valor_param_usuario(912,31,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w);
	ie_considerar_comp_w	:= obter_valor_param_usuario(912,32,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w);

	open c01;
	loop
	fetch c01 into
		nr_sequencia_comp_w,
		nr_seq_gen_alim_w,
		qt_componente_w,
		qt_refeicao_w,
		qt_pessoa_atend_real_w;
	exit when c01%notfound;
		begin

		select 	cd_material,
			nvl(qt_conversao,1)
		into	cd_material_w,
			qt_conversao_w
		from	nut_genero_alim
		where 	nr_sequencia = nr_seq_gen_alim_w;

		-- Retirado na OS 642832 - 02/10/2013
		/*select	nvl(max(c.vl_custo),0)
		into	vl_custo_real_w
		from	custo_mensal_material c
		where 	c.cd_estabelecimento	= cd_estabelecimento_w	
		and	c.cd_material		= cd_material_w
		and	c.ie_tipo_custo		= 'R'
		and	c.dt_referencia		=	(select	max(dt_referencia)
							from	custo_mensal_material c
							where	c.cd_estabelecimento	= cd_estabelecimento_w
							and	c.cd_material		= cd_material_w
							and	c.ie_tipo_custo		= 'R');*/

		select	nvl(obter_dados_ultima_compra(cd_estabelecimento_w, cd_material_w, 'VE'),0)
		into	vl_custo_real_w
		from	dual;
		
		if (nvl(qt_refeicao_w,0) = 0 ) then
			qt_componente_w  := 0;
		else
			if (ie_considerar_pessoas_w = 'S') then
				qt_componente_w		:= qt_componente_w * qt_pessoa_atend_real_w / qt_refeicao_w;
			else
				qt_componente_w		:= qt_componente_w / qt_refeicao_w;
			end if;
		end if;
			
		vl_custo_total_w	:= vl_custo_real_w * qt_componente_w / qt_conversao_w;
			
		update	nut_rec_real_comp
		set	vl_custo_real	=  vl_custo_total_w
		where	nr_sequencia	= nr_sequencia_comp_w;

		end;
	end loop;
	close c01;
end if;

commit;

end nut_calc_custo_real_cardapios;
/