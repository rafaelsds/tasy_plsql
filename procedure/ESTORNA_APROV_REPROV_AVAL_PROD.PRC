create or replace
procedure estorna_aprov_reprov_aval_prod(	nr_Sequencia_p		number,
					nm_usuario_p		Varchar2) is 

begin


update	mat_avaliacao
set	dt_aprovacao		= null,
	nm_usuario_aprov		= null,
	dt_reprovacao		= null,
	nm_usuario_reprov		= null
where	nr_sequencia		= nr_Sequencia_p;

commit;

end estorna_aprov_reprov_aval_prod;
/