create or replace
procedure tiss_gerar_dem_fatura(nr_seq_demonstrativo_p	number,
				nr_fatura_p	varchar2,
				vl_processado_p	number,
				vl_liberado_p	number,
				vl_glosado_p	number,
				nm_usuario_p	Varchar2,
				nr_seq_fatura_p	out number) is


nr_sequencia_w	number(10);

begin

select	tiss_dem_fatura_seq.nextval
into	nr_sequencia_w
from	dual;

insert into tiss_dem_fatura(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_demonstrativo,
	nr_fatura,
	vl_processado,
	vl_liberado,
	vl_glosado)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	nr_seq_demonstrativo_p,
	nr_fatura_p,
	vl_processado_p,
        vl_liberado_p,
	vl_glosado_p);

nr_seq_fatura_p	:= nr_sequencia_w;

commit;

end tiss_gerar_dem_fatura;
/
