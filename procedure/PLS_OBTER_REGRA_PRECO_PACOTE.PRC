create or replace
procedure pls_obter_regra_preco_pacote(	cd_procedimento_p	number,
					ie_origem_proced_p		number,
					ie_origem_p			varchar2,
					nr_seq_item_p			number,
					nm_usuario_p			varchar2,
					nr_seq_pacote_p		out 	varchar2,
					nr_seq_regra_preco_pac_p	out 	varchar2) is

/* IE_ORIGEM_P
C -> Procedimento da conta -> pls_conta_proc
PI -> Procedimento da conta de Importa��o XML (novo) -> pls_conta_item_imp
AA -> Procedimento da auditoria da autoriza��o -> pls_auditoria_item
AR -> Procedimento da auditoria da requisi��o -> pls_auditoria_item
E -> Execu��o da Requisi��o -> pls_execucao_req_item
G -> Guia/Autoriza��o -> pls_guia_plano_proc
R -> Requisi��o -> pls_requisicao_proc
pls_regra_lanc_aut_item
COMPL -> Complemento de guia -> ptu_ped_compl_aut_serv	
*/

nr_seq_regra_preco_pac_w		number(10)	:= null;
nr_seq_pacote_w			number(10)	:= null;
ie_regra_preco_w			varchar2(3)	:= 'N';

cd_estabelecimento_w		number(4);
nr_seq_prestador_w		number(10);
nr_seq_tipo_acomod_w		number(10);
dt_pacote_w			date;
ie_internado_w			varchar2(3);
nr_seq_plano_w			number(10);
nr_contrato_w			number(10);
nr_seq_congenere_w		number(10);
ie_origem_conta_w		varchar2(5);
ie_tipo_intercambio_w		varchar2(5);
nr_seq_pacote_out_w		number(10);
nr_seq_regra_pacote_out_w	number(10);
cd_proc_pacote_out_w		number(15)	:= null;
ie_origem_pacote_out_w		number(15)	:= null;
vl_pacote_out_w			number(15,2)	:= 0;
vl_medico_out_w			number(15,2)	:= 0;
vl_anestesista_out_w		number(15,2)	:= 0;
vl_auxiliares_out_w		number(15,2)	:= 0;
vl_custo_operacional_out_w	number(15,2)	:= 0;
vl_materiais_out_w		number(15,2)	:= 0;
nr_seq_intercambio_w		number(10);
nr_seq_classificacao_prest_w	number(10);
nr_seq_procedimento_w		number(10);

nr_seq_conta_w			number(10);
dt_procedimento_w		date;
nr_seq_prestador_conta_w	number(10);
nr_seq_protocolo_w		number(10);
dt_conta_guia_w			date;
nr_seq_segurado_w		number(10);
cd_guia_w			varchar2(30);
sg_estado_w			pessoa_juridica.sg_estado%type;
sg_estado_int_w			pessoa_juridica.sg_estado%type;
nr_seq_prestador_prot_w		number(10);
nr_seq_guia_w			number(10);
nr_seq_requisicao_w		number(10);
ie_conta_medica_w		Varchar2(1);
dt_atendimento_referencia_w	date;

begin
ie_conta_medica_w := 'N';
/* Se n�o passar o item, ir� verificar apenas pelo c�digo do procedimento, sem considerar regra de pre�o */
if	(nr_seq_item_p is not null) then
	if	(ie_origem_p = 'C') then
		ie_conta_medica_w := 'S';
		select	a.nr_seq_conta,
			a.dt_procedimento
		into	nr_seq_conta_w,
			dt_procedimento_w
		from	pls_conta_proc a
		where	a.nr_sequencia	= nr_seq_item_p;
		
		dt_pacote_w := pls_obter_data_preco_item(nr_seq_item_p,'P');
		
		select	nvl(nr_seq_prestador_exec,0),
			nr_seq_protocolo,
			cd_estabelecimento,
			nr_seq_tipo_acomodacao,
			nvl(dt_atendimento_referencia, nvl(dt_autorizacao, nvl(dt_entrada, sysdate))),
			nr_seq_plano,
			substr(pls_obter_se_internado(nr_sequencia,'X'),1,1),
			nr_seq_segurado,
			nvl(cd_guia,'X'),
			ie_origem_conta,
			dt_atendimento_referencia
		into	nr_seq_prestador_conta_w,
			nr_seq_protocolo_w,
			cd_estabelecimento_w,
			nr_seq_tipo_acomod_w,
			dt_conta_guia_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w,
			cd_guia_w,
			ie_origem_conta_w,
			dt_atendimento_referencia_w
		from	pls_conta
		where	nr_sequencia	= nr_seq_conta_w;
		
		/* Obter o prestador do protocolo */
		select	nr_seq_prestador
		into	nr_seq_prestador_prot_w
		from	pls_protocolo_conta
		where	nr_sequencia	= nr_seq_protocolo_w;

		if	(nr_seq_prestador_conta_w	> 0) then
			nr_seq_prestador_w	:= nr_seq_prestador_conta_w;
		else
			nr_seq_prestador_w	:= nr_seq_prestador_prot_w;
		end if;
		
	elsif	(ie_origem_p = 'PI') then 
		-- Aqui obt�m os dados da importa��o nova pls_conta_item_imp e pls_conta_imp
		
		ie_conta_medica_w := 'S';
		
		-- no ie_origem_conta passo E por se tratar apenas de importa��o XML / TISS quando entrar aqui
		-- pega os campos do procedimento e da conta, caso o prestador da conta n�o esteja 
		-- identificado busca do protocolo
		-- pls_obter_se_internado tem alguns valores passados como NULL aqui porque ainda n�o temos uma identifica��o completa
		-- e n�o s�o valores utilizados neste momento
		select	c.dt_execucao,
			nvl(a.nr_seq_prest_exec_conv, b.nr_seq_prestador_conv) nr_seq_prestador,
			a.cd_estabelecimento,
			null,
			
			pls_obter_produto_benef(
			(select max(x.nr_seq_plano)
			from	pls_segurado x
			where	x.nr_sequencia = a.nr_seq_segurado_conv),dt_atendimento_referencia_w) as nr_seq_plano,
			
--			(select max(x.nr_seq_plano)
--			from	pls_segurado x
--			where	x.nr_sequencia = a.nr_seq_segurado_conv) nr_seq_plano,
			
			pls_obter_se_internado(	null, 'X', b.ie_tipo_guia, a.nr_seq_tipo_atend_conv, null, null, 'N'),
			a.nr_seq_segurado_conv,
			'E' ie_origem_conta
		into	dt_pacote_w,
			nr_seq_prestador_w,
			cd_estabelecimento_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w,
			ie_origem_conta_w
		from	pls_conta_item_imp c,
			pls_conta_imp a,
			pls_protocolo_conta_imp b
		where	c.nr_sequencia = nr_seq_item_p
		and	a.nr_sequencia = c.nr_seq_conta
		and	b.nr_sequencia = a.nr_seq_protocolo;
	
	elsif	(ie_origem_p = 'AA') then
		/*Obter dados do procedimento*/
		select	a.nr_seq_guia
		into	nr_seq_guia_w
		from	pls_auditoria_item	b,
			pls_auditoria		a			
		where	b.nr_sequencia	= nr_seq_item_p
		and	a.nr_sequencia	= b.nr_seq_auditoria;
		
		/*Obter dados da guia*/
		select	nr_seq_prestador,
			cd_estabelecimento,
			dt_solicitacao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'A'),
			nr_seq_segurado
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_w;
	elsif	(ie_origem_p = 'AR') then
		/*Obter dados do procedimento*/
		select	a.nr_seq_requisicao
		into	nr_seq_requisicao_w
		from	pls_auditoria_item	b,
			pls_auditoria		a			
		where	b.nr_sequencia	= nr_seq_item_p
		and	a.nr_sequencia	= b.nr_seq_auditoria;
		
		/*Obter dados da requisicao*/
		select	nr_seq_prestador,
			cd_estabelecimento,
			dt_requisicao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'R'),
			nr_seq_segurado
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
		
		if (nr_seq_tipo_acomod_w is null) then
			nr_seq_tipo_acomod_w := pls_obter_dados_produto(nr_seq_plano_w,'AP');
		end if;
	elsif	(ie_origem_p = 'E') then
		/*Obter dados do procedimento*/
		select	nr_seq_requisicao
		into	nr_seq_requisicao_w
		from	pls_execucao_req_item
		where	nr_sequencia	= nr_seq_item_p;
		
		/*Obter dados da guia*/
		select	nr_seq_prestador,
			cd_estabelecimento,
			dt_requisicao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'R'),
			nr_seq_segurado
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
		
		if (nr_seq_tipo_acomod_w is null) then
			nr_seq_tipo_acomod_w := pls_obter_dados_produto(nr_seq_plano_w,'AP');
		end if;
	elsif	(ie_origem_p = 'G') then
		/*Obter dados do procedimento*/
		select	nr_seq_guia
		into	nr_seq_guia_w
		from	pls_guia_plano_proc
		where	nr_sequencia	= nr_seq_item_p;
		
		/*Obter dados da guia*/
		select	nr_seq_prestador,
			cd_estabelecimento,
			dt_solicitacao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'A'),
			nr_seq_segurado
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_w;
		
	elsif	(ie_origem_p = 'R') then
		/*Obter dados do procedimento*/
		select	nr_seq_requisicao
		into	nr_seq_requisicao_w
		from	pls_requisicao_proc
		where	nr_sequencia	= nr_seq_item_p;
		
		/*Obter dados da guia*/
		select	nr_seq_prestador,
			cd_estabelecimento,
			dt_requisicao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'R'),
			nr_seq_segurado
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
		
		if (nr_seq_tipo_acomod_w is null) then
			nr_seq_tipo_acomod_w := pls_obter_dados_produto(nr_seq_plano_w,'AP');
		end if;		
		
	elsif	(ie_origem_p = 'COMPL') then
		/*Obter dados do procedimento*/
		select	a.nr_seq_guia
		into	nr_seq_guia_w
		from	ptu_pedido_compl_aut_serv	b,
			ptu_pedido_compl_aut		a
		where	b.nr_sequencia	= nr_seq_item_p
		and	b.nr_seq_pedido	= a.nr_sequencia;
		
		/*Obter dados da guia*/
		select	nr_seq_prestador,
			cd_estabelecimento,
			dt_solicitacao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'A'),
			nr_seq_segurado
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_w;
	end if;
	
	/* Dados v�lidos para todos os tipos */
	if	(nvl(nr_seq_plano_w,0) = 0) then		
		begin
		select	a.nr_seq_plano
		into	nr_seq_plano_w
		from	pls_segurado			a	
		where	a.nr_sequencia = nr_seq_segurado_w;	
		exception
		when others then
			nr_seq_plano_w := null;
		end;
	end if;
	
	begin
	select	nvl(a.nr_contrato,0),
		b.nr_seq_congenere
	into	nr_contrato_w,
		nr_seq_congenere_w
	from	pls_contrato	a,
		pls_segurado	b
	where	a.nr_sequencia	= b.nr_seq_contrato
	and	b.nr_sequencia	= nr_seq_segurado_w;
	exception
	when others then
		nr_contrato_w	:= 0;
	end;
	
	if	(nr_seq_congenere_w is null) then
		begin
		select	b.nr_seq_congenere
		into	nr_seq_congenere_w
		from	pls_segurado	b
		where	b.nr_sequencia	= nr_seq_segurado_w;
		exception
		when others then
			nr_seq_congenere_w	:= null;
		end;
	end if;
	
	select	max(nr_seq_intercambio)
	into	nr_seq_intercambio_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_w;
	
	select	max(nr_seq_classificacao)
	into	nr_seq_classificacao_prest_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_w;
	
	select	nvl(max(sg_estado),'X')
	into	sg_estado_w
	from	pessoa_juridica
	where	cd_cgc	= (	select	max(cd_cgc_outorgante)
				from	pls_outorgante
				where	cd_estabelecimento	= cd_estabelecimento_w);
	select	nvl(max(a.sg_estado),'X')
	into	sg_estado_int_w
	from	pessoa_juridica	a,
		pls_congenere	b
	where	a.cd_cgc	= b.cd_cgc
	and	b.nr_sequencia	= nr_seq_congenere_w;

	if	(sg_estado_w <> 'X') and
		(sg_estado_int_w <> 'X') then
		if	(sg_estado_w	= sg_estado_int_w) then
			ie_tipo_intercambio_w	:= 'E';
		else	
			ie_tipo_intercambio_w	:= 'N';
		end if;
	else
		ie_tipo_intercambio_w	:= 'A';
	end if;

	pls_define_preco_pacote(cd_estabelecimento_w,
				nr_seq_prestador_w,
				nr_seq_tipo_acomod_w,
				dt_pacote_w,
				cd_procedimento_p,
				ie_origem_proced_p,
				ie_internado_w,
				nr_seq_plano_w,
				nr_contrato_w,
				nr_seq_congenere_w,
				nm_usuario_p,
				ie_origem_conta_w,
				ie_tipo_intercambio_w,
				nr_seq_pacote_out_w,
				nr_seq_regra_pacote_out_w,
				cd_proc_pacote_out_w,
				ie_origem_pacote_out_w,
				vl_pacote_out_w,
				vl_medico_out_w,
				vl_anestesista_out_w,
				vl_auxiliares_out_w,
				vl_custo_operacional_out_w,
				vl_materiais_out_w,
				nr_seq_intercambio_w,
				nr_seq_classificacao_prest_w,
				nr_seq_procedimento_w,
				nr_seq_segurado_w,
				null,
				ie_conta_medica_w,
				null,
				1);
				
	nr_seq_regra_preco_pac_w	:= nr_seq_regra_pacote_out_w;
	
	/* Obter pacote */
	select	max(a.nr_sequencia)
	into	nr_seq_pacote_w
	from	pls_pacote a
	where	a.ie_situacao	= 'A'
	and	a.cd_procedimento	= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p;
	
	if	(nr_seq_pacote_w is not null) then
		/* Verificar se � considerado como pacote apenas atrav�s da regra de pre�o */
		select	nvl(ie_regra_preco,'N')
		into	ie_regra_preco_w
		from	pls_pacote a
		where	a.nr_sequencia	= nr_seq_pacote_w;
		
		if	(ie_regra_preco_w = 'S') and
			(nr_seq_regra_pacote_out_w is null) then
			nr_seq_pacote_w		:= null;
		end if;
	end if;
end if;

nr_seq_pacote_p			:= nr_seq_pacote_w;
nr_seq_regra_preco_pac_p	:= nr_seq_regra_preco_pac_w;

end pls_obter_regra_preco_pacote;
/