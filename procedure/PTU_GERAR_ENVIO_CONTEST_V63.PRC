create or replace 
procedure ptu_gerar_envio_contest_v63 (	nr_seq_camara_p			ptu_camara_contestacao.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar o arquivo A550 V6.3
-------------------------------------------------------------------------------------------------------------------
OPS - Controle de Contestações
Locais de chamada direta: 
[X]  Objetos do dicionário [ ]  Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

-- Conteudo
ds_conteudo_w			varchar2(700);
ds_conteudo_aux_w		varchar2(700);
ds_conteudo_553_w		varchar2(500);
ie_registro_w			varchar2(3);
nr_seq_questionamento_w		ptu_questionamento.nr_sequencia%type; -- 552
cd_motivo_w			ptu_motivo_questionamento.cd_motivo%type; -- 553	
--nr_seq_quest_reemb_w		ptu_questionamento_reem b.nr_sequencia%type; -- 557
nr_seq_registro_w		pls_integer	:= 1;
vl_reconhecido_w		number(15,2) 	:= 0;
vl_cobrado_w			number(15,2) 	:= 0;
vl_contestacao_w		number(15,2) 	:= 0;

-- 559
qt_552_w			pls_integer	:= 0;
qt_557_w			pls_integer	:= 0;

Cursor C00 is
	select	'552' ||
		lpad(nvl(nr_lote,'0'),8,'0') ||
		lpad(' ',15,' ') ||
		lpad(nvl(cd_unimed,'0'),4,'0') ||
		lpad(nvl(cd_usuario_plano,' '),13,' ') ||
		rpad(nvl(upper(substr(nm_beneficiario,1,25)),' '),25,' ') ||
		lpad(nvl(to_char(dt_atendimento,'yyyy/mm/dd') || to_char(dt_atendimento,'hh24:mi:ss') ||'-03',' '),21,' ') ||
		lpad(' ',124,' ') ||
		nvl(to_char(ie_tipo_tabela),' ') ||
		lpad(nvl(cd_servico,'0'),8,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobrado,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconhecido,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo,0)),'.',''),',',''),14,'0') ||
		lpad(nvl(to_char(dt_acordo,'yyyymmdd'),' '),8,' ') ||
		lpad(nvl(ie_tipo_acordo,' '),2,' ') ||
		lpad(substr(replace(replace(nvl(qt_cobrada*10000,'0'),'.',''),',',''),1,8),8,'0') ||
		rpad(nvl(elimina_acentuacao(ds_servico),' '),80,' ') ||
		lpad(nvl(nr_seq_a500,'0'),8,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_adic_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_adic_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_adic_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_adic_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_adic_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_adic_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_adic_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_adic_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_adic_filme,0)),'.',''),',',''),14,'0') ||
		lpad(substr(replace(replace(nvl(qt_reconh*10000,'0'),'.',''),',',''),1,8),8,'0') ||
		nvl(ie_pacote,'N') ||
		lpad(nvl(cd_pacote,'0'),8,'0') ||
		lpad(nvl(to_char(dt_servico,'yyyymmdd'),' '),8,' ') ||
		lpad(nvl(hr_realiz,' '),8,' ') ||
		lpad(nvl(qt_acordada,'0'),8,'0') ||
		lpad(nvl(fat_mult_serv*100,'0'),3,'0') ||
		rpad(nvl(to_char(nr_nota),' '),20,' '),
		nr_sequencia
	from	ptu_questionamento
	where	nr_seq_contestacao	= nr_seq_camara_p
	and	ie_tipo_acordo not in ('11');

Cursor C01 is
	select	lpad((substr(a.cd_motivo,1,4)),4,'0'),
		ptu_somente_caracter_permitido(substr(decode(b.ds_parecer_glosa, null, a.ds_motivo, b.ds_parecer_glosa),1,500), 'ANS')
	from	ptu_questionamento_codigo	b,
		ptu_motivo_questionamento	a
	where	a.nr_sequencia		= b.nr_seq_mot_questionamento
	and	b.nr_seq_registro	= nr_seq_questionamento_w
	/*union all
	select	lpad((substr(a.cd_motivo,1,4)),4,'0'),
		ptu_somente_caracter_permitido(substr(decode(b.ds_parecer_glosa, null, a.ds_motivo, b.ds_parecer_glosa),1,500), 'ANS')
	from	ptu_questionamento_codigo	b,
		ptu_motivo_questionamento	a
	where	a.nr_sequencia		= b.nr_seq_mot_questionamento
	and	b.nr_seq_registro_reemb	= nr_seq_quest_reemb_w*/;
	
/*Cursor C13 is
	select	'557' ||
		lpad(nvl(nr_lote,'0'),8,'0') ||
		rpad(nvl(to_char(nr_nota),' '),20,' ') ||
		lpad(nvl(nr_seq_a500,'0'),8,'0') ||
		lpad(nvl(cd_unimed,'0'),4,'0') ||
		lpad(nvl(id_benef,' '),13,' ') ||
		rpad(nvl(upper(substr(nm_benef,1,25)),' '),25,' ') ||
		lpad(nvl(to_char(dt_servico,'yyyymmdd'),' '),8,' ') ||
		lpad(nvl(to_char(dt_reembolso,'yyyymmdd'),' '),8,' ') ||
		nvl(tp_particip,' ') ||
		nvl(tp_tabela,' ') ||
		lpad(nvl(cd_servico,'0'),8,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_dif_vl_inter,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_serv,0)),'.',''),',',''),14,'0') ||
		lpad(nvl(to_char(dt_acordo,'yyyymmdd'),' '),8,' ') ||
		lpad(nvl(tp_acordo,' '),2,' ') ||
		lpad(substr(replace(replace(nvl(qt_cobrada*10000,'0'),'.',''),',',''),1,8),8,'0') ||
		lpad(substr(replace(replace(nvl(qt_reconh*10000,'0'),'.',''),',',''),1,8),8,'0') ||
		lpad(substr(replace(replace(nvl(qt_acordada*10000,'0'),'.',''),',',''),1,8),8,'0') ||
		lpad(nvl(nr_cnpj_cpf,' '),14,' ') ||
		rpad(nvl(nm_prestador,' '),40,' ') ||
		rpad(nvl(nm_profissional,' '),40,' ') ||
		rpad(nvl(sg_cons_prof,' '),12,' ') ||
		rpad(nvl(nr_cons_prof,' '),15,' ') ||
		rpad(nvl(sg_uf_cons_prof,' '),2,' ') ||
		lpad(nvl(nr_autoriz,'0'),10,'0'),
		nr_sequencia
	from	ptu_questionamento_reem b
	where	nr_seq_contestacao	= nr_seq_camara_p
	and	tp_acordo not in ('11');*/

begin
delete	w_ptu_envio_arq 
where	nm_usuario = nm_usuario_p;

select	decode(count(1),0,'557','552')
into	ie_registro_w
from	ptu_questionamento
where	nr_seq_contestacao = nr_seq_camara_p;

select	sum(nvl(x.vl_reconhecido,0) + nvl(x.vl_reconh_adic_co,0) + 
	nvl(x.vl_reconh_adic_filme,0) + nvl(x.vl_reconh_adic_serv,0) + 
	nvl(x.vl_reconh_co,0) + nvl(x.vl_reconh_filme,0)) vl_rec,
	sum(nvl(x.vl_cobrado,0) + nvl(x.vl_cobr_adic_co,0) +
	nvl(x.vl_cobr_adic_filme,0) + nvl(x.vl_cobr_adic_serv,0) +
	nvl(x.vl_cobr_co,0) + nvl(x.vl_cobr_filme,0)) vl_cobr
into	vl_reconhecido_w,
	vl_cobrado_w
from	ptu_questionamento x
where	x.nr_seq_contestacao = nr_seq_camara_p
and	x.ie_tipo_acordo not in ('11');

vl_contestacao_w := vl_cobrado_w - vl_reconhecido_w;

select	lpad(nr_seq_registro_w,8,'0') ||
	'551' ||
	lpad(nvl(cd_unimed_destino,'0'),4,'0') ||
	lpad(nvl(cd_unimed_origem,'0'),4,'0') ||
	rpad(nvl(to_char(dt_geracao,'yyyymmdd'),' '),8,' ') ||
	lpad(nvl(cd_unimed_credora,'0'),4,'0') ||
	lpad(' ',11,' ') ||
	lpad(' ',11,' ') ||
	rpad(nvl(to_char(dt_venc_fatura,'yyyymmdd'),' '),8,' ') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_fatura,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_contestacao,vl_contestacao_w)),'.',''),',',''),14,'0') ||
	lpad(' ',14,' ') ||
	nvl(to_char(ie_tipo_arquivo),' ') ||
	'12' ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_pago,0)),'.',''),',',''),14,'0') ||
	lpad(nvl(to_char(nr_documento),'0'),11,'0') ||
	lpad(nvl(to_char(dt_venc_doc,'yyyymmdd'),' '),8,' ') ||
	nvl(to_char(ie_conclusao),'0') ||
	nvl(ie_classif_cobranca_a500,'2') ||	
	lpad('0',11,'0') ||	
	lpad(nvl(to_char(dt_vencimento_ndc_a500,'yyyymmdd'),' '),8,' ') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_ndc_a500,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_contest_ndc,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_pago_ndc,0)),'.',''),',',''),14,'0') ||
	lpad(nvl(to_char(nr_documento2),'0'),11,'0') ||
	lpad(nvl(to_char(dt_venc_doc2,'yyyymmdd'),' '),8,' ') ||
	rpad(nvl(nr_fatura,' '),20,' ') ||
	rpad(nvl(nr_nota_credito_debito_a500,' '),20,' ')	
into	ds_conteudo_w
from	ptu_camara_contestacao
where	nr_sequencia	= nr_seq_camara_p;

insert into w_ptu_envio_arq
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_conteudo)
values	(w_ptu_envio_arq_seq.nextval,
	sysdate,
	nm_usuario_p,
	ds_conteudo_w);

nr_seq_registro_w	:= nr_seq_registro_w + 1;

-- REGISTRO 552
if	(ie_registro_w = '552') then
	open C00;
	loop
	fetch C00 into
		ds_conteudo_aux_w,
		nr_seq_questionamento_w;
	exit when C00%notfound;
		begin
		ds_conteudo_w	:= lpad(nr_seq_registro_w,8,'0') || ds_conteudo_aux_w;

		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);

		update	ptu_questionamento
		set	nr_seq_arquivo	= nr_seq_registro_w
		where	nr_sequencia	= nr_seq_questionamento_w;

		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		qt_552_w		:= qt_552_w + 1;

		open C01;
		loop
		fetch C01 into
			cd_motivo_w,
			ds_conteudo_553_w;
		exit when C01%notfound;
			begin

			ds_conteudo_553_w :=	replace(replace(replace(ptu_somente_caracter_permitido(ds_conteudo_553_w,'ANS'),chr(13),' '),chr(10),' '),chr(9),' ');
			
			ds_conteudo_w :=	lpad(nr_seq_registro_w, 8, '0') ||
						'553' ||
						cd_motivo_w ||
						rpad(trim(ds_conteudo_553_w),500,' ');

			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w);

			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			end;
		end loop;
		close C01;
		end;
	end loop;
	close C00;
	
-- REGISTRO 557
/*elsif	(ie_registro_w = '557') then
	open C13;
	loop
	fetch C13 into	
		ds_conteudo_aux_w,
		nr_seq_quest_reemb_w;
	exit when C13%notfound;
		begin
		
		-- INSERE PRIMEIRO O REGISTRO 553
		open C01;
		loop
		fetch C01 into
			cd_motivo_w,
			ds_conteudo_553_w;
		exit when C01%notfound;
			begin
			
			ds_conteudo_553_w :=	replace(replace(replace(ptu_somente_caracter_permitido(ds_conteudo_553_w,'ANS'),chr(13),' '),chr(10),' '),chr(9),' ');
			
			ds_conteudo_w :=	lpad(nr_seq_registro_w, 8, '0') ||
						'553' ||
						cd_motivo_w ||
						rpad(trim(ds_conteudo_553_w),500,' ');

			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w);

			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			end;
		end loop;
		close C01;
		
		ds_conteudo_w	:= lpad(nr_seq_registro_w,8,'0') || ds_conteudo_aux_w;

		-- REGISTRO 557
		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);

		update	ptu_questionamento_reem b
		set	nr_seq_arquivo	= nr_seq_registro_w
		where	nr_sequencia	= nr_seq_quest_reemb_w;

		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		qt_557_w		:= qt_557_w + 1;
		
		end;
	end loop;
	close C13;*/
end if;

select	lpad(nr_seq_registro_w,8,'0') ||
	'559' ||
	lpad(qt_552_w,5,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobrado_w,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconhecido_w,0)),'.',''),',',''),14,'0') ||
	lpad('0',14,'0') ||
	lpad('0',14,'0') ||
	lpad(qt_557_w,5,'0')
into	ds_conteudo_w
from	ptu_camara_contestacao
where	nr_sequencia	= nr_seq_camara_p;

insert into w_ptu_envio_arq
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_conteudo)
values	(w_ptu_envio_arq_seq.nextval,
	sysdate,
	nm_usuario_p,
	ds_conteudo_w);

commit;

end ptu_gerar_envio_contest_v63;
/