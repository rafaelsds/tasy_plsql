create or replace procedure get_ihc_ccg (
											nr_seq_acs_p			in ihc_acs.nr_sequencia%type, 
											nm_usuario_p	     	in ihc_acs.nm_usuario%type) is
			

vl_charge_w				ihc_acd.vl_charge%type;
vl_day_w				ihc_acd.vl_day%type;
ie_charge_raised_w		ihc_acd.ie_charge_raised%type;
ie_level_w				ihc_acd.ie_level%type;
ie_service_type_w		ihc_acd.ie_service_type%type;
nr_account_w			ihc_claim.nr_account%type;
nr_records_w			ihc_claim.nr_sequencia%type;
nr_seq_claim_w			ihc_claim.nr_sequencia%type;
nr_seq_classif_niss_w	Varchar2(10);

cursor c01 is	
select		a.nr_seq_interno  nr_seq_interno,
			b.nr_seq_classif_niss,
			a.dt_entrada_unidade dt_entrada_unidade,
			a.dt_saida_unidade dt_saida_unidade,
			obter_dias_entre(a.dt_entrada_unidade, nvl(a.dt_saida_unidade, sysdate)) qt_days,
			round(obter_hora_entre_datas(a.dt_entrada_unidade, nvl(a.dt_saida_unidade, sysdate))) qt_hours,
			c.vl_procedimento vl_procedimento,
			(c.vl_procedimento / c.qt_procedimento) vl_day,
			d.cd_procedimento_loc cd_procedimento,
			c.ie_origem_proced
from		atend_paciente_unidade a,
			setor_atendimento b,
			procedimento_paciente c,
			procedimento d
where		c.nr_interno_conta = nr_account_w
and   		c.nr_atendimento = a.nr_atendimento
and 		d.cd_procedimento = c.cd_procedimento
and 		d.ie_origem_proced = c.ie_origem_proced
and   		a.nr_seq_interno = c.nr_seq_atepacu
and			a.cd_setor_atendimento = b.cd_setor_atendimento
and			b.cd_classif_setor = '4';

c01_w		c01%rowtype;

begin

select nr_seq_claim
into   nr_seq_claim_w
from   ihc_acs 
where  nr_sequencia = nr_seq_acs_p;

select	max(nr_account)
into	nr_account_w
from	ihc_claim
where	nr_sequencia = nr_seq_claim_w;

if (nr_account_w is not null) then
	
	begin
	open c01;
	loop
	fetch c01 into c01_w;		
	exit when c01%notfound;
			
		vl_charge_w := (nvl(c01_w.vl_procedimento ,0) * 100);
		vl_day_w := ROUND(dividir_sem_round(nvl(vl_charge_w,0),c01_w.qt_days));

		if (vl_charge_w = 0) then
			ie_charge_raised_w := 'I';
		else
			ie_charge_raised_w := 'C';
		end if;
		
		if (c01_w.nr_seq_classif_niss is null) then
			generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || c01_w.cd_procedimento || '. ' || obter_desc_expressao(840785), nm_usuario_p);
		else
			get_eclipse_conversion('NR_SEQ_CLASSIF_NISS', c01_w.nr_seq_classif_niss, 'IHC', null, nr_account_w, nr_seq_classif_niss_w);			
			if	(nr_seq_classif_niss_w in ('ICU','CCU')) then
				ie_level_w := '1';
			end if;
		end if;
		
		if (c01_w.dt_saida_unidade is null) then
			generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || c01_w.cd_procedimento || '. ' || obter_desc_expressao(570828), nm_usuario_p);
		end if;
		
		if (c01_w.ie_origem_proced is not null) then
			get_eclipse_conversion('IE_ORIGEM_PROCED', c01_w.ie_origem_proced, 'IHC', 'CCG', nr_account_w, ie_service_type_w);
		end if;

		select  count(nr_sequencia)
		into    nr_records_w
		from    ECLIPSE_INCO_ACCOUNT
		where   NR_INTERNO_CONTA = nr_account_w;

		if (BILLING_I18N_PCK.GET_VALIDATE_ECLIPSE() = 'N') and 
		   (nr_records_w = 0) then
		
			insert into ihc_acd(
					nr_sequencia,       
					nr_seq_claim,
					nr_seq_acs,
					ie_type,              
					dt_atualizacao,       
					nm_usuario,           
					dt_atualizacao_nrec,  
					nm_usuario_nrec,      
					ie_bed_band,          
					ie_addon,             
					ie_level,             
					vl_charge,            
					ie_charge_raised,     
					vl_day,               
					dt_from,              
					qt_days,              
					cd_classification,    
					cd_program,           
					cd_service,           
					ie_service_type,      
					dt_end,               
					ie_critical_care,     
					qt_hours)
			values (
					ihc_acd_seq.nextval,
					nr_seq_claim_w,
					nr_seq_acs_p,
					'CCG',
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					'Y',
					ie_level_w,
					vl_charge_w,
					ie_charge_raised_w,
					vl_day_w,
					c01_w.dt_entrada_unidade,
					decode(c01_w.qt_days, 0, 1,null,1, c01_w.qt_days),
					null,
					null,
					c01_w.cd_procedimento,
					ie_service_type_w,
					c01_w.dt_saida_unidade,
					nr_seq_classif_niss_w,
					c01_w.qt_hours);
		
			update ihc_acs
			set ie_situation = 'A'
			where nr_sequencia = nr_seq_acs_p;
		end if;
			
	end loop;
	close c01;
	end;
	
end if;

end get_ihc_ccg;
/