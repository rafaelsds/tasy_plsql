CREATE OR REPLACE
PROCEDURE duplicar_prescr_enfer_html(	qt_horas_futura_p	number,
											nr_sequencia_p		number,
											cd_prescritor_p		varchar2,
											nr_atendimento_p		number,
											dt_sae_p			date,
											ie_intervensao_p		varchar2,
											ie_agrupa_dia_p		varchar2,
											nm_usuario_p		varchar2,
											nr_seq_modelo_p		number,
											nr_sequencia_sae_p	out number,
											ie_tipo_p			varchar2 default 'SAE') IS 

BEGIN

if	((sysdate + nvl(qt_horas_futura_p,0) / 24) < dt_SAE_p) then
	wheb_mensagem_pck.exibir_mensagem_abort(281986,'QT_HORAS_FUTURA='||qt_horas_futura_p);
end if;

duplicar_prescricao_enfemagem(nr_sequencia_p,cd_prescritor_p,nr_atendimento_p,dt_sae_p,ie_intervensao_p,ie_agrupa_dia_p,nm_usuario_p,nr_seq_modelo_p,nr_sequencia_sae_p,ie_tipo_p);


END duplicar_prescr_enfer_html;
/
