create or replace
procedure ajustar_prescr_material_html5(	nr_prescricao_p		in number,
											nr_seq_cpoe_p		in number,
											cd_material_p		in number) is

cursor c01 is
select	a.nr_sequencia
from	prescr_material a,
		cpoe_material b
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_mat_cpoe = b.nr_sequencia
and		a.nr_seq_mat_cpoe = nr_seq_cpoe_p
and		b.cd_material = cd_material_p
and		a.ie_agrupador in (1, 5)
and		a.cd_material in (	b.cd_material,
							b.cd_mat_comp1,
							b.cd_mat_comp2,
							b.cd_mat_comp3,
							b.cd_mat_comp4,
							b.cd_mat_comp5,
							b.cd_mat_comp6,
							b.cd_mat_comp7);

begin

	for r_c01_w in c01
	loop

		ajustar_prescr_material(nr_prescricao_p => nr_prescricao_p,
								nr_sequencia_p => r_c01_w.nr_sequencia,
								ie_atualiza_regra_disp_p => 'N');

	end loop;

end ajustar_prescr_material_html5;
/
