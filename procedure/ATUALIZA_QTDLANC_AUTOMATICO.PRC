CREATE OR REPLACE
PROCEDURE Atualiza_QtdLanc_Automatico
			(	nr_seq_procedimento_p		Number) is


nr_seq_regra_lanc_w		Number(10);
qt_procedimento_w		Number(9,3)	:= 0;
nr_seq_proc_regra_w		Number(10);
cd_proc_regra_w			Number(15);
nr_seq_lanc_acao_w		Number(6,0);
ie_quantidade_w			Varchar2(1)	:= 'I';
cd_material_w			Number(6);
nr_seq_material_w		Number(10);

dt_procedimento_w		Date;
dt_conta_w			Date;

qt_lancamento_w			regra_lanc_aut_pac.qt_lancamento%type;
qt_proc_rla_w			procedimento_paciente.qt_procedimento%type;
qt_mat_rla_w			material_atend_paciente.qt_material%type;
cd_convenio_w			convenio.cd_convenio%type;


CURSOR C01 IS
select	nr_sequencia,
	cd_procedimento,
	nr_seq_lanc_acao,
	nr_seq_regra_lanc,
	qt_procedimento,
	cd_convenio
from	procedimento_paciente
where	nr_seq_proc_princ	= nr_seq_procedimento_p
and	nr_seq_regra_lanc	is not null;

CURSOR C02 IS
select	nr_sequencia,
	cd_material,
	nr_seq_regra_lanc,
	qt_material
from	material_atend_paciente
where	nr_seq_proc_princ	= nr_seq_procedimento_p
and	nr_seq_regra_lanc	is not null;

CURSOR C03 IS
select	nr_sequencia,
	cd_procedimento,
	nr_seq_regra_lanc
from	procedimento_paciente
where	nr_seq_proc_princ	= nr_seq_procedimento_p
and	((dt_procedimento	<> dt_procedimento_w) or (dt_conta <> dt_conta_w))
and	nr_seq_regra_lanc	is not null;

CURSOR C04 IS
select	nr_sequencia,
	nr_seq_regra_lanc
from	material_Atend_paciente
where	nr_seq_proc_princ	= nr_seq_procedimento_p
and	((dt_atendimento	<> dt_procedimento_w) or (dt_conta <> dt_conta_w))
and	nr_seq_regra_lanc	is not null;

BEGIN

select	qt_procedimento,
	dt_procedimento,
	dt_conta
into	qt_procedimento_w,
	dt_procedimento_w,
	dt_conta_w
from	procedimento_paciente
where	nr_sequencia	= nr_seq_procedimento_p;

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_proc_regra_w,
	cd_proc_regra_w,
	nr_seq_lanc_acao_w,
	nr_seq_regra_lanc_w,
	qt_proc_rla_w,
	cd_convenio_w;
exit when c01%notfound;
	begin
	if	(nvl(nr_seq_lanc_acao_w,0) > 0) then
		select	nvl(max(ie_quantidade),'I'),
			nvl(max(qt_lancamento),1)
		into	ie_quantidade_w,
			qt_lancamento_w
		from	regra_lanc_aut_pac
		where	nr_seq_lanc	= nr_seq_lanc_acao_w 
		and	nr_seq_regra	= nr_seq_regra_lanc_w;
	else
		select	nvl(max(ie_quantidade),'I'),
			nvl(max(qt_lancamento),1)
		into	ie_quantidade_w,
			qt_lancamento_w
		from	regra_lanc_aut_pac
		where	cd_procedimento	= cd_proc_regra_w
		and 	nvl(ie_adic_orcamento,'N') = 'N'
		and	nr_seq_regra	= nr_seq_regra_lanc_w;
	end if;

	if	(ie_quantidade_w	= 'M') and
		((qt_procedimento_w * nvl(qt_lancamento_w,1)) <> qt_proc_rla_w) then
		update	procedimento_paciente
		set	qt_procedimento	= qt_procedimento_w * nvl(qt_lancamento_w,1)
		where	nr_sequencia	= nr_seq_proc_regra_w;
		
		atualiza_preco_procedimento(nr_seq_proc_regra_w, cd_convenio_w, wheb_usuario_pck.get_nm_usuario);
	end if;
	end;
END LOOP;
CLOSE C01;

OPEN C02;
LOOP
FETCH C02 into
	nr_seq_material_w,
	cd_material_w,
	nr_seq_regra_lanc_w,
	qt_mat_rla_w;
exit when c02%notfound;
	begin
	select	nvl(max(ie_quantidade),'I'),
		nvl(max(qt_lancamento),1)
	into	ie_quantidade_w,
		qt_lancamento_w
	from	regra_lanc_aut_pac
	where	cd_material	= cd_material_w
	and 	nvl(ie_adic_orcamento,'N') = 'N'
	and	nr_seq_regra	= nr_seq_regra_lanc_w;

	if	(ie_quantidade_w	= 'M') and
		((qt_procedimento_w * nvl(qt_lancamento_w,1)) <> qt_mat_rla_w) then
		update	material_atend_paciente
		set	qt_material	= qt_procedimento_w * nvl(qt_lancamento_w,1)
		where	nr_sequencia	= nr_seq_material_w;
		
		atualiza_preco_material(nr_seq_material_w, wheb_usuario_pck.get_nm_usuario);
	end if;
	end;
END LOOP;
CLOSE C02;

OPEN C03;
LOOP
FETCH C03 into
	nr_seq_proc_regra_w,
	cd_proc_regra_w,
	nr_seq_regra_lanc_w;
exit when c03%notfound;
	begin

	update	procedimento_paciente
	set	dt_procedimento	= dt_procedimento_w,
		dt_conta	= dt_conta_w
	where	nr_sequencia	= nr_seq_proc_regra_w;
	
	end;
END LOOP;
CLOSE C03;

OPEN C04;
LOOP
FETCH C04 into
	nr_seq_material_w,
	nr_seq_regra_lanc_w;
exit when c04%notfound;
	begin

	update	material_atend_paciente
	set	dt_atendimento	= dt_procedimento_w,
		dt_conta	= dt_conta_w
	where	nr_sequencia	= nr_seq_material_w;
	
	end;
END LOOP;
CLOSE C04;

commit;

END Atualiza_QtdLanc_Automatico;
/
