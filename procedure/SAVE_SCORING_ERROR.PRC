create or replace procedure save_scoring_error(  ds_justificativa_p varchar2,
                                                 nm_usuario_p varchar2, 
                                                 nr_seq_scoring_error_warning_p number) is

nr_seq_scoring_w number(10);
begin
    select scoring_error_warning_just_SEQ.nextval
      into nr_seq_scoring_w
      from dual;

    insert into scoring_error_warning_just(
        nr_sequencia,
        dt_atualizacao, 
        nm_usuario, 
        ds_justificativa,
        nr_seq_scoring_error_warning
		
    ) values (
        nr_seq_scoring_w,
        sysdate,
        nm_usuario_p,
        ds_justificativa_p, 
        nr_seq_scoring_error_warning_p
    );

end save_scoring_error;
/