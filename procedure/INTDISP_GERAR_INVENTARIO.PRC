create or replace 
procedure intdisp_gerar_inventario ( cd_local_estoque_p		number,
				cd_material_p		number,
				qt_contagem_p		number,
				cd_fornecedor_p 		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ds_erro_p		out 	varchar2) is


ds_erro_w					varchar2(2000);
ie_tipo_entrada_w			varchar2(1);
cd_operacao_estoque_w		number(10);
cd_op_est_consig_w			number(10);
dt_mesano_referencia_w		date;
nr_movimento_w				number(10);
qt_estoque_w				number(15,4) := 0;
qt_estoque_consig_w			number(15,4) := 0;
qt_mvto_consig_w			number(15,4) := 0;
qt_material_estoque_w		number(15,4);
ie_consignado_w				material.ie_consignado%type;
ie_regra_saldo_consig_w		parametro_estoque.ie_regra_saldo_consig%type;
cd_cgc_fornec_w				pessoa_juridica.cd_cgc%type;
ie_movto_consignado_w		movimento_estoque.ie_movto_consignado%type;
cd_material_w				material.cd_material%type;

begin

dt_mesano_referencia_w := PKG_DATE_UTILS.start_of(sysdate,'MONTH',0);

select	nvl(ie_consignado,'0'),
	nvl(cd_material_estoque,cd_material)
into	ie_consignado_w,
	cd_material_w
from	material
where	cd_material = cd_material_p;

select nvl(ie_regra_saldo_consig,0)
into ie_regra_saldo_consig_w
from parametro_estoque
where cd_estabelecimento = cd_estabelecimento_p;

if	(ie_consignado_w = '2') and (ie_regra_saldo_consig_w > 0) then

	begin
	select	nvl(sum(f.qt_estoque),0)
	into	qt_estoque_consig_w
	from	fornecedor_mat_consignado f
	where	f.cd_estabelecimento = cd_estabelecimento_p
	and		f.cd_material = cd_material_w
	and		f.cd_local_estoque = nvl(cd_local_estoque_p, f.cd_local_estoque)
	and		f.dt_mesano_referencia = dt_mesano_referencia_w;
	exception
		when others then
			qt_estoque_consig_w := 0;
	end;

end if;

Obter_Saldo_Estoque(cd_estabelecimento_p,cd_material_p,cd_local_estoque_p,dt_mesano_referencia_w, qt_estoque_w);

select	(qt_contagem_p - (nvl(qt_estoque_w,0) + qt_estoque_consig_w))
into	qt_material_estoque_w
from	dual;

if	(nvl(qt_material_estoque_w,0) > 0) then
	ie_tipo_entrada_w := 'E';
else
	ie_tipo_entrada_w := 'S';
	qt_material_estoque_w := (qt_material_estoque_w * -1);
end if;

if	(nvl(ie_tipo_entrada_w,'X') = 'E') or
	(nvl(ie_tipo_entrada_w,'X') = 'S' and nvl(qt_estoque_w,0) > 0) then

	select	min(cd_operacao_estoque)
	into	cd_operacao_estoque_w
	from	operacao_estoque
	where	ie_tipo_requisicao = 5
	and	ie_entrada_saida = ie_tipo_entrada_w
	and	ie_consignado	= '0' -- Proprio
	and	ie_situacao	= 'A';

	ie_movto_consignado_w := 'N';

	if	(nvl(ie_tipo_entrada_w,'X') = 'S' and nvl(qt_material_estoque_w,0) > nvl(qt_estoque_w,0)) and
		((ie_consignado_w = '2') and (ie_regra_saldo_consig_w > 0)) then

		qt_mvto_consig_w := nvl(qt_material_estoque_w,0) - nvl(qt_estoque_w,0);
		qt_material_estoque_w := nvl(qt_estoque_w,0);

		select	min(cd_operacao_estoque)
		into	cd_op_est_consig_w
		from	operacao_estoque
		where	ie_tipo_requisicao = 5
		and	ie_entrada_saida = ie_tipo_entrada_w
		and	ie_consignado = '7' -- Consignado
		and	ie_situacao	= 'A';

	end if;

elsif (nvl(ie_tipo_entrada_w,'X') = 'S') and
	((ie_consignado_w = '2') and (ie_regra_saldo_consig_w > 0)) then

	select	min(cd_operacao_estoque)
	into	cd_op_est_consig_w
	from	operacao_estoque
	where	ie_tipo_requisicao = 5
	and	ie_entrada_saida = ie_tipo_entrada_w
	and	ie_consignado = '7' -- Consignado
	and	ie_situacao	= 'A';

	ie_movto_consignado_w := 'S';
	qt_mvto_consig_w := qt_material_estoque_w;

end if;

if	(nvl(cd_operacao_estoque_w,0) <> 0) and (nvl(qt_material_estoque_w,0) > 0) and (ie_movto_consignado_w = 'N') then
	begin

	select	movimento_estoque_seq.nextval
	into	nr_movimento_w
	from	dual;

	begin
	insert into movimento_estoque(
		nr_movimento_estoque,
		cd_estabelecimento,
		cd_local_estoque,
		dt_movimento_estoque,
		cd_operacao_estoque,
		cd_acao,
		cd_material,
		dt_mesano_referencia,
		qt_movimento,
		dt_atualizacao,
		nm_usuario,
		ie_origem_documento,
		ie_origem_proced,
		qt_estoque,
		dt_processo,
		cd_material_estoque,
		qt_inventario,
		ie_movto_consignado,
		cd_fornecedor,
		ds_observacao)
	values( nr_movimento_w,
		cd_estabelecimento_p,
		cd_local_estoque_p,
		sysdate,
		cd_operacao_estoque_w,
		1,
		cd_material_p,
		dt_mesano_referencia_w,
		qt_material_estoque_w,
		sysdate,
		nm_usuario_p,
		5,
		1,
		qt_material_estoque_w,
		null,
		cd_material_w,
		qt_material_estoque_w,
		'N',
		null,
		WHEB_MENSAGEM_PCK.get_texto(278957));
	exception when others then
		ds_erro_w := substr(sqlerrm,1,2000);
	end;

	end;
end if;

if (nvl(cd_op_est_consig_w,0) <> 0) and (nvl(qt_mvto_consig_w,0) > 0) then
	begin

	while (qt_mvto_consig_w > 0) loop

		cd_cgc_fornec_w	:= substr(obter_fornecedor_regra_consig(cd_estabelecimento_p, cd_material_p, '1', cd_local_estoque_p),1,14);
		obter_saldo_estoque_consignado(cd_estabelecimento_p, cd_cgc_fornec_w, cd_material_p, cd_local_estoque_p, sysdate, qt_estoque_consig_w);

		if (qt_estoque_consig_w >= qt_mvto_consig_w) then

			qt_material_estoque_w := qt_mvto_consig_w;
			qt_mvto_consig_w := 0;

		elsif (qt_estoque_consig_w > 0) then

			qt_material_estoque_w := qt_estoque_consig_w;
			qt_mvto_consig_w := qt_mvto_consig_w - qt_estoque_consig_w;

		elsif (qt_estoque_consig_w <= 0) then

			qt_mvto_consig_w := 0;
			qt_material_estoque_w := 0;

		end if;

		if (qt_material_estoque_w > 0) then

			select	movimento_estoque_seq.nextval
			into	nr_movimento_w
			from	dual;

			begin
			insert into movimento_estoque(
				nr_movimento_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				cd_operacao_estoque,
				cd_acao,
				cd_material,
				dt_mesano_referencia,
				qt_movimento,
				dt_atualizacao,
				nm_usuario,
				ie_origem_documento,
				ie_origem_proced,
				qt_estoque,
				dt_processo,
				cd_material_estoque,
				qt_inventario,
				ie_movto_consignado,
				cd_fornecedor,
				ds_observacao)
			values( nr_movimento_w,
				cd_estabelecimento_p,
				cd_local_estoque_p,
				sysdate,
				cd_op_est_consig_w,
				1,
				cd_material_p,
				dt_mesano_referencia_w,
				qt_material_estoque_w,
				sysdate,
				nm_usuario_p,
				5,
				1,
				qt_material_estoque_w,
				null,
				cd_material_w,
				qt_material_estoque_w,
				'S',
				cd_cgc_fornec_w,
				WHEB_MENSAGEM_PCK.get_texto(278957));

				commit;

			exception when others then
				ds_erro_w := substr(sqlerrm,1,2000);
			end;

		end if;

	end loop;

	end;
end if;

ds_erro_p := ds_erro_w;

commit;

end intdisp_gerar_inventario;
/