create or replace
procedure inserir_metodo_pagamento(	cd_tipo_p		varchar2,
				nr_sequencia_p	number,
				nm_usuario_p          varchar2) is 


cd_tipo_recebimento_w		tipo_recebimento.cd_tipo_recebimento%type;

begin

if	(cd_tipo_p is not null) then
	

	insert into fis_metodo_pagamento (
		nr_sequencia,	
		cd_tipo_recebimento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_nota 
		)values(
		fis_metodo_pagamento_seq.nextval,
		cd_tipo_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p);	

	end if;


commit;
end inserir_metodo_pagamento;
/
