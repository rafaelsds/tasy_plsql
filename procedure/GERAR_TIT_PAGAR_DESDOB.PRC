CREATE OR REPLACE 
PROCEDURE GERAR_TIT_PAGAR_DESDOB ( 	nr_titulo_p		NUMBER,
					nr_parcelas_p		NUMBER,
					dt_vencimento_p		DATE,
					nm_usuario_p		VARCHAR2,
					qt_dias_intervalo_p	NUMBER) IS

vl_saldo_titulo_w		NUMBER(15,2);
vl_parcela_w			NUMBER(15,2);
vl_parcela_original_w		NUMBER(15,2);
nr_sequencia_w			NUMBER(10,0);
vl_resto_w			NUMBER(15,2);
dt_parcela_w			DATE;
vl_parcela_juros_orig_w		NUMBER(15,2);
vl_parcela_multa_orig_w		NUMBER(15,2);
vl_parcela_juros_w		NUMBER(15,2);
vl_parcela_multa_w		NUMBER(15,2);
vl_resto_juros_w		NUMBER(15,2);
vl_resto_multa_w		NUMBER(15,2);
vl_saldo_juros_w		NUMBER(15,2);
vl_saldo_multa_w		NUMBER(15,2);
nr_dia_parcela_w		number(10);
nr_dia_venc_w			number(10);

BEGIN

select	vl_saldo_titulo,
	vl_saldo_juros,
	vl_saldo_multa
into	vl_saldo_titulo_w,
	vl_saldo_juros_w,
	vl_saldo_multa_w
from	titulo_pagar
where	nr_titulo = nr_titulo_p;

dt_parcela_w 		:= dt_vencimento_p;
vl_parcela_w		:= dividir(trunc(dividir(vl_saldo_titulo_w * 100,nr_parcelas_p)), 100); 
vl_parcela_juros_w	:= dividir(trunc(dividir(vl_saldo_juros_w * 100,nr_parcelas_p)), 100); 
vl_parcela_multa_w	:= dividir(trunc(dividir(vl_saldo_multa_w * 100,nr_parcelas_p)), 100); 
vl_resto_w		:= vl_saldo_titulo_w - (vl_parcela_w * nr_parcelas_p);
vl_resto_juros_w	:= vl_saldo_juros_w - (vl_parcela_juros_w * nr_parcelas_p);
vl_resto_multa_w	:= vl_saldo_multa_w - (vl_parcela_multa_w * nr_parcelas_p);
vl_parcela_original_w	:= vl_parcela_w;
vl_parcela_juros_orig_w	:= vl_parcela_juros_w;
vl_parcela_multa_orig_w	:= vl_parcela_multa_w;

nr_dia_venc_w		:= PKG_DATE_UTILS.extract_field('DAY', dt_vencimento_p);

FOR i in 1..nr_parcelas_p LOOP

	IF 	(i = 1) THEN
		vl_parcela_w 		:= vl_parcela_w + vl_resto_w;
		vl_parcela_juros_w	:= vl_parcela_juros_w + vl_resto_juros_w;
		vl_parcela_multa_w	:= vl_parcela_multa_w + vl_resto_multa_w;
	END IF;

	select	titulo_pagar_desdob_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into titulo_pagar_desdob
		(NR_SEQUENCIA,
		 NR_TITULO,
		 DT_VENCIMENTO,
		 VL_TITULO,
		 DT_ATUALIZACAO,
		 NM_USUARIO,	
		 NR_TITULO_DEST,
		 vl_saldo_juros,
		 vl_saldo_multa)
	values				
		(nr_sequencia_w,
		 nr_titulo_p,
		 dt_parcela_w,
		 vl_parcela_w,
		 SYSDATE,
		 nm_usuario_p,
		 Null,
		 vl_parcela_juros_w,
		 vl_parcela_multa_w);

	vl_parcela_w 		:= vl_parcela_original_w;
	vl_parcela_juros_w	:= vl_parcela_juros_orig_w;
	vl_parcela_multa_w	:= vl_parcela_multa_orig_w;
	
	if	(qt_dias_intervalo_p > 0) then
		dt_parcela_w	:= dt_parcela_w + qt_dias_intervalo_p;
	else
		/* esse tratamento � para o caso de as parcelas passarem pelo m�s de fevereiro */
		nr_dia_parcela_w	:= PKG_DATE_UTILS.extract_field('DAY', PKG_DATE_UTILS.END_OF(PKG_DATE_UTILS.ADD_MONTH(dt_parcela_w, 1, 0), 'MONTH', 0));

		if	(nr_dia_venc_w	> nr_dia_parcela_w) then

			dt_parcela_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_parcela_w,1,0);

		else
			if (nr_dia_venc_w > PKG_DATE_UTILS.EXTRACT_FIELD('DAY', PKG_DATE_UTILS.END_OF(PKG_DATE_UTILS.add_month(dt_parcela_w,1),'MONTH') )) then 
				nr_dia_venc_w := PKG_DATE_UTILS.EXTRACT_FIELD('DAY', PKG_DATE_UTILS.END_OF(PKG_DATE_UTILS.add_month(dt_parcela_w,1),'MONTH'));
			end if;
			dt_parcela_w	:= PKG_DATE_UTILS.get_date(nr_dia_venc_w, PKG_DATE_UTILS.add_month(dt_parcela_w,1,0));

		end if;
	end if;

end loop;
COMMIT;
END GERAR_TIT_PAGAR_DESDOB;
/