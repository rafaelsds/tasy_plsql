create or replace
procedure pls_valida_imp_contest(nr_seq_lote_p	pls_lote_contestacao.nr_sequencia%type) is

nr_seq_mensagem_w	number(10) :=0;
nr_seq_lote_p_w		pls_lote_contestacao.nr_sequencia%type;

-- Valida se existe algum questionamento que n�o possui o nr_seq_conta informado, caracterizando que o servi�o importado
cursor c01 (	nr_seq_lote_pc	 pls_lote_contestacao.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_camara_contest
	from	pls_lote_contestacao	a,
		ptu_camara_contestacao	b,
		ptu_questionamento	c
	where	a.nr_sequencia		= nr_seq_lote_pc
	and	a.nr_seq_pls_fatura is not null
	and	c.nr_seq_conta is null
	and	b.nr_seq_lote_contest	= a.nr_sequencia
	and	c.nr_seq_contestacao	= b.nr_sequencia;
begin
	nr_seq_lote_p_w := nr_seq_lote_p;

	-- Apenas realiza o teste se n�o possuir nehuma mensagem.
	if	(nr_seq_mensagem_w = 0) then
	
		-- Se n�o gerou nenhum lote, quer dizer que n�o foi localizada nenhuma fatura para  contesta��o
		if	(nr_seq_lote_p_w is null) then
			nr_seq_mensagem_w := 391497; -- 391497 - N�o foi localizada a fatura para esta contesta��o importada.
		end if;
	end if;
	
	-- Apenas realiza o teste se n�o possuir nehuma mensagem.
	if	(nr_seq_mensagem_w = 0) and (nr_seq_lote_p_w is not null) then
		-- Valida se existe algum questionamento que n�o possui o nr_seq_conta informado, caracterizando que o servi�o importado
		-- n�o foi faturado pelo PTU
		for r_c01_w in c01(nr_seq_lote_p_w) loop
			-- atualiza o status para "IN" - Inv�lido
			ptu_atualizar_status_imp_a550(r_c01_w.nr_seq_camara_contest, 'IN', 'S');			
			nr_seq_mensagem_w := 391498;
		end loop;	
	end if;

	-- Se possui alguma inconsist�ncia, gera a mensagem
	if	(nr_seq_mensagem_w <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(nr_seq_mensagem_w);		
	end if;		

end pls_valida_imp_contest;
/