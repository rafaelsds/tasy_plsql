create or replace
procedure 	alterar_convenio_proced(nr_prescricao_p		number,
					cd_convenio_p		number,
					cd_procedimento_p	number,
					cd_categoria_p		Varchar2,
					nm_usuario_p		Varchar2) is 

begin

if 	(nr_prescricao_p is not null) and 
	(cd_procedimento_p is not null) then 
	update 	prescr_procedimento
	set 	cd_convenio 	= cd_convenio_p,
		cd_categoria 	= cd_categoria_p
	where 	nr_prescricao 	= nr_prescricao_p 
	and 	cd_procedimento = cd_procedimento_p;
	
	commit;
end if;

end alterar_convenio_proced;
/