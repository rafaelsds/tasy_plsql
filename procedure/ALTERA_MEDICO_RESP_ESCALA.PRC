create or replace
procedure altera_medico_resp_escala(
					cd_estabelecimento_p	number,
					ie_clinica_p		number,
					nr_atendimento_p	number) is 

cd_medico_resp_w	varchar2(10);	
dt_referencia_w		date;
ie_tipo_atendimento_w	number(3);
begin

select	max(dt_entrada),
	max(ie_tipo_atendimento)
into	dt_referencia_w,
	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select	obter_pessoa_escala_atend(0,cd_estabelecimento_p,ie_tipo_atendimento_w,ie_clinica_p,dt_referencia_w)
into	cd_medico_resp_w
from 	dual;

if (nvl(cd_medico_resp_w,0) > 0) then

	update	atendimento_paciente
	set	cd_medico_resp = cd_medico_resp_w
	where	nr_atendimento = nr_atendimento_p;

end if;

commit;

end altera_medico_resp_escala;
/