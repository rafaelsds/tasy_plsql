create or replace
procedure pls_gerar_lote_pessoa_inc
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2,
				ie_acao_p		varchar2,
				cd_estabelecimento_p	number) is

-- ie_acao_p
-- 'G' - Gerar
-- 'D' - Desfazer gera��o

ie_contrato_ativo_w	varchar2(1);
ie_tipo_pessoa_w	varchar2(1);
ie_pagador_w		varchar2(1);
ie_estipulante_w	varchar2(1);
ie_beneficiario_w	varchar2(1);
ie_prestador_w		varchar2(1);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);
nr_seq_inconsistencia_w	number(10);
qt_inconsistencia_w	number(10);
nr_seq_contrato_w	pls_contrato.nr_sequencia%type;
nr_contrato_w		pls_contrato.nr_contrato%type	:= null;
ie_tipo_segurado_w	pls_lote_pessoa_inconsist.ie_tipo_segurado%type;
nr_seq_intercambio_w	pls_intercambio.nr_sequencia%type := null;

Cursor c01 is
	select	cd_pessoa_fisica,
		cd_cgc
	from	(select	a.cd_pessoa_fisica,
			a.cd_cgc
		from	pls_contrato_pagador a,
			pls_contrato b
		where	a.nr_seq_contrato	= b.nr_sequencia
		and	ie_pagador_w	= 'S'
		and	((a.cd_cgc is not null and ie_tipo_pessoa_w = 'J') or
			(a.cd_pessoa_fisica is not null and ie_tipo_pessoa_w = 'F') or
			ie_tipo_pessoa_w = 'A')
		and	(((ie_contrato_ativo_w = 'A' and b.dt_aprovacao is not null and a.dt_rescisao is null) or (ie_contrato_ativo_w = 'I' and a.dt_rescisao is not null) or (ie_contrato_ativo_w = 'T')))
		and	((b.nr_contrato	= nr_contrato_w) or (nr_contrato_w is null))
		union
		select	a.cd_pessoa_fisica,
			null cd_cgc
		from	pls_segurado a,
			pls_contrato_pagador c,
			pls_contrato b
		where	c.nr_seq_contrato	= b.nr_sequencia
		and	c.nr_sequencia		= a.nr_seq_pagador
		and	ie_beneficiario_w	= 'S'
		and	(((ie_contrato_ativo_w = 'A' and b.dt_aprovacao is not null and a.dt_rescisao is null) or (ie_contrato_ativo_w = 'I' and a.dt_rescisao is not null) or (ie_contrato_ativo_w = 'T')))
		and	((b.nr_contrato	= nr_contrato_w) or (nr_contrato_w is null))
		and	((ie_tipo_pessoa_w ='A') or (ie_tipo_pessoa_w='F'))
		and	((a.ie_tipo_segurado = ie_tipo_segurado_w) or (ie_tipo_segurado_w is null))
		union
		select	a.cd_pf_estipulante cd_pessoa_fisica,
			a.cd_cgc_estipulante cd_cgc
		from	pls_contrato a
		where	ie_estipulante_w	= 'S'
		and	((a.cd_cgc_estipulante is not null and ie_tipo_pessoa_w = 'J') or
			(a.cd_pf_estipulante is not null and ie_tipo_pessoa_w = 'F') or
			ie_tipo_pessoa_w = 'A')
		and	(((ie_contrato_ativo_w = 'A' and a.dt_aprovacao is not null and a.dt_rescisao_contrato is null) or (ie_contrato_ativo_w = 'I' and a.dt_rescisao_contrato is not null) or (ie_contrato_ativo_w = 'T')))
		and	((a.nr_contrato	= nr_contrato_w) or (nr_contrato_w is null))
		union
		select	a.cd_pessoa_fisica,
			a.cd_cgc
		from	pls_prestador a
		where	ie_prestador_w		= 'S'
		and	((a.cd_cgc is not null and ie_tipo_pessoa_w = 'J') or
			(a.cd_pessoa_fisica is not null and ie_tipo_pessoa_w = 'F') or
			ie_tipo_pessoa_w = 'A'));

Cursor c02 is
	select	a.nr_seq_inconsistencia
	from	pls_inconsistencia_pessoa a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
	union all
	select	a.nr_seq_inconsistencia
	from	pls_inconsistencia_pessoa a
	where	a.cd_cgc		= cd_cgc_w;

Cursor c03 is
	select	cd_pessoa_fisica,
		cd_cgc
	from	(select	a.cd_pessoa_fisica,
			a.cd_cgc
		from	pls_contrato_pagador a,
			pls_intercambio b
		where	a.nr_seq_pagador_intercambio	= b.nr_sequencia
		and	ie_pagador_w	= 'S'
		and	((a.cd_cgc is not null and ie_tipo_pessoa_w = 'J') or
			(a.cd_pessoa_fisica is not null and ie_tipo_pessoa_w = 'F') or
			ie_tipo_pessoa_w = 'A')
		and	(((ie_contrato_ativo_w = 'A' and b.dt_aprovacao is not null and a.dt_rescisao is null) or (ie_contrato_ativo_w = 'I' and a.dt_rescisao is not null) or (ie_contrato_ativo_w = 'T')))
		and	((b.nr_sequencia = nr_seq_intercambio_w) or (nr_seq_intercambio_w is null))
		union
		select	a.cd_pessoa_fisica,
			null cd_cgc
		from	pls_segurado a,
			pls_contrato_pagador c,
			pls_intercambio b
		where	c.nr_seq_pagador_intercambio = b.nr_sequencia
		and	c.nr_sequencia		= a.nr_seq_pagador
		and	ie_beneficiario_w	= 'S'
		and	(((ie_contrato_ativo_w = 'A' and b.dt_aprovacao is not null and a.dt_rescisao is null) or (ie_contrato_ativo_w = 'I' and a.dt_rescisao is not null) or (ie_contrato_ativo_w = 'T')))
		and	((b.nr_sequencia = nr_seq_intercambio_w) or (nr_seq_intercambio_w is null))
		and	((ie_tipo_pessoa_w ='A') or (ie_tipo_pessoa_w='F'))
		and	((a.ie_tipo_segurado = ie_tipo_segurado_w) or (ie_tipo_segurado_w is null))
		union
		select	a.cd_pessoa_fisica cd_pessoa_fisica,
			a.cd_cgc cd_cgc
		from	pls_intercambio a
		where	ie_estipulante_w	= 'S'
		and	((a.cd_cgc is not null and ie_tipo_pessoa_w = 'J') or
			(a.cd_pessoa_fisica is not null and ie_tipo_pessoa_w = 'F') or
			ie_tipo_pessoa_w = 'A')
		and	(((ie_contrato_ativo_w = 'A' and a.dt_aprovacao is not null and a.dt_exclusao is null) or (ie_contrato_ativo_w = 'I' and a.dt_exclusao is not null) or (ie_contrato_ativo_w = 'T')))
		and	((a.nr_sequencia= nr_seq_intercambio_w) or (nr_seq_intercambio_w is null))
		union
		select	a.cd_pessoa_fisica,
			a.cd_cgc
		from	pls_prestador a
		where	ie_prestador_w		= 'S'
		and	((a.cd_cgc is not null and ie_tipo_pessoa_w = 'J') or
			(a.cd_pessoa_fisica is not null and ie_tipo_pessoa_w = 'F') or
			ie_tipo_pessoa_w = 'A'));

begin

if	(nr_seq_lote_p is not null) then
	if	(ie_acao_p	= 'G') then
		select	ie_contrato_ativo,
			ie_tipo_pessoa,
			ie_pagador,
			ie_beneficiario,
			ie_estipulante,
			ie_prestador,
			nr_seq_contrato,
			ie_tipo_segurado,
			nr_seq_intercambio
		into	ie_contrato_ativo_w,
			ie_tipo_pessoa_w,
			ie_pagador_w,
			ie_beneficiario_w,
			ie_estipulante_w,
			ie_prestador_w,
			nr_seq_contrato_w,
			ie_tipo_segurado_w,
			nr_seq_intercambio_w
		from	pls_lote_pessoa_inconsist 
		where	nr_sequencia	= nr_seq_lote_p;
		
		if	(nr_seq_contrato_w is not null) then
			select	a.nr_contrato
			into	nr_contrato_w
			from	pls_contrato a
			where	a.nr_sequencia	= nr_seq_contrato_w;
		end if;
		
		if	(nr_seq_intercambio_w is not null) or
			(ie_tipo_segurado_w in('T','C') and nr_contrato_w is null) then--T = Interc�mbio cooperativa ; C = Interc�mbio Cong�nere
			
				open c03;
				loop
				fetch c03 into
					cd_pessoa_fisica_w,
					cd_cgc_w;
				exit when c03%notfound;
					pls_consistir_dados_pessoa(cd_pessoa_fisica_w,
								cd_cgc_w,
								nr_seq_lote_p,
								cd_estabelecimento_p,
								nm_usuario_p);
					
					open c02;
					loop
					fetch c02 into
						nr_seq_inconsistencia_w;
					exit when c02%notfound;
						select	count(1)
						into	qt_inconsistencia_w
						from	pls_pessoa_inconsistente	a,
							pls_lote_pessoa_inconsist	b
						where	a.nr_seq_lote	= b.nr_sequencia
						and	b.ie_status	= 'I'
						and	((a.cd_pessoa_fisica = cd_pessoa_fisica_w) or (a.cd_cgc = cd_cgc_w))
						and	a.nr_seq_inconsistencia = nr_seq_inconsistencia_w
						and	rownum = 1;
						
						if	(qt_inconsistencia_w = 0) then
							insert	into	pls_pessoa_inconsistente
								(nr_sequencia,
								nm_usuario,
								dt_atualizacao,
								nm_usuario_nrec,
								dt_atualizacao_nrec,
								nr_seq_lote,
								cd_pessoa_fisica,
								cd_cgc,
								nr_seq_inconsistencia)
							values	(pls_pessoa_inconsistente_seq.nextval,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								sysdate,
								nr_seq_lote_p,
								cd_pessoa_fisica_w,
								cd_cgc_w,
								nr_seq_inconsistencia_w);
						end if;
					end loop;
					close c02;
				end loop;
				close c03;
		elsif	(nr_contrato_w is not null) or
			(ie_tipo_segurado_w not in('T','C')) then
			
			open c01;
			loop
			fetch c01 into
				cd_pessoa_fisica_w,
				cd_cgc_w;
			exit when c01%notfound;
				pls_consistir_dados_pessoa(cd_pessoa_fisica_w,
							cd_cgc_w,
							nr_seq_lote_p,
							cd_estabelecimento_p,
							nm_usuario_p);
				
				open c02;
				loop
				fetch c02 into
					nr_seq_inconsistencia_w;
				exit when c02%notfound;
					select	count(1)
					into	qt_inconsistencia_w
					from	pls_pessoa_inconsistente	a,
						pls_lote_pessoa_inconsist	b
					where	a.nr_seq_lote	= b.nr_sequencia
					and	b.ie_status	= 'I'
					and	((a.cd_pessoa_fisica = cd_pessoa_fisica_w) or (a.cd_cgc = cd_cgc_w))
					and	a.nr_seq_inconsistencia = nr_seq_inconsistencia_w
					and	rownum = 1;
					
					if	(qt_inconsistencia_w = 0) then
						insert	into	pls_pessoa_inconsistente
							(nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nr_seq_lote,
							cd_pessoa_fisica,
							cd_cgc,
							nr_seq_inconsistencia)
						values	(pls_pessoa_inconsistente_seq.nextval,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nr_seq_lote_p,
							cd_pessoa_fisica_w,
							cd_cgc_w,
							nr_seq_inconsistencia_w);
					end if;
				end loop;
				close c02;
			end loop;
			close c01;
			
		end if;
		
		update	pls_lote_pessoa_inconsist
		set	dt_geracao_lote	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_lote_p;
	elsif	(ie_acao_p	= 'D') then
		delete	pls_pessoa_inconsistente
		where	nr_seq_lote	= nr_seq_lote_p;
		
		update	pls_lote_pessoa_inconsist
		set	dt_geracao_lote	= null,
			dt_atualizacao	= sysdate,
			ie_status	= 'I',
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_lote_p;
	end if;
end if;

commit;

end pls_gerar_lote_pessoa_inc;
/