create or replace 
procedure GERAR_W_TRIBUTO_FORNEC(	cd_estabelecimento_p	number,
					dt_inicial_p		date,
					dt_final_p			date,
					cd_cgc_p		varchar2,
					cd_pessoa_fisica_p		varchar2,
					ie_tipo_pessoa_p		varchar2,
					cd_tipo_pessoa_p		number,
					nm_usuario_p		varchar2) is

ie_origem_w		varchar2(5);
nr_documento_w		number(20,0);
dt_referencia_w		date;
dt_liquidacao_w		date;
vl_documento_w		number(15,2);
vl_tributo_w		number(15,2);
ie_tipo_tributo_w		varchar2(255);
cd_cgc_w		varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
vl_ir_w			number(15,2);
vl_pis_w			number(15,2);
vl_inss_w			number(15,2);
vl_iss_w			number(15,2);
VL_PIS_COFINS_W	number(15,2);
cont_w			number(5,0);
cd_tipo_pessoa_w		number(10,0);


cursor c01 is
select	ie_origem,
	nr_documento,
	dt_referencia,
	dt_liquidacao,
	vl_documento,
	vl_tributo,
	ie_tipo_tributo,
	cd_cgc,
	cd_pessoa_fisica,
	cd_tipo_pessoa
from	(
	select	'T' ie_origem,
		a.nr_titulo nr_documento,
		a.dt_vencimento_atual dt_referencia,
		a.dt_liquidacao,
		a.vl_titulo vl_documento,
		b.vl_imposto vl_tributo,
		c.ie_tipo_tributo,
		a.cd_cgc,
		a.cd_pessoa_fisica,
		d.cd_tipo_pessoa
	from	pessoa_juridica d,
		tributo c,
		titulo_pagar_imposto b,
		titulo_pagar a
	where	a.nr_titulo			= b.nr_titulo(+)
	and	b.cd_tributo		= c.cd_tributo(+)
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_situacao		= 'L'
	and	a.cd_cgc			= d.cd_cgc(+)
	and	(d.cd_tipo_pessoa		= cd_tipo_pessoa_p or cd_tipo_pessoa_p is null)
	and	a.nr_seq_nota_fiscal	is null
	and	a.nr_repasse_terceiro	is null
	union all
	select	'R' ie_origem,
		b.nr_repasse_terceiro nr_documento,
		c.dt_vencimento dt_referencia,
		null dt_liquidacao,
		c.vl_vencimento vl_documento,
		d.vl_imposto vl_tributo,
		e.ie_tipo_tributo,
		a.cd_cgc,
		a.cd_pessoa_fisica,
		f.cd_tipo_pessoa
	from	pessoa_juridica f,
		tributo e,
		repasse_terc_venc_trib d,
		repasse_terceiro_venc c,
		repasse_terceiro b,
		terceiro a
	where	a.nr_sequencia		= b.nr_seq_terceiro
	and	b.nr_repasse_terceiro	= c.nr_repasse_terceiro
	and	c.nr_sequencia		= d.nr_seq_rep_venc(+)
	and	d.cd_tributo		= e.cd_tributo(+)
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_cgc			= f.cd_cgc(+)
	and	(f.cd_tipo_pessoa		= cd_tipo_pessoa_p  or cd_tipo_pessoa_p is null)
	and	b.ie_status		= 'F'
	union all
	select	'NF' ie_origem,
		somente_numero(a.nr_nota_fiscal) nr_documento,
		a.dt_emissao  dt_referencia,
		null dt_liquidacao,
		a.vl_mercadoria vl_documento,
		b.vl_tributo,
		c.ie_tipo_tributo,
		a.cd_cgc,
		a.cd_pessoa_fisica,
		d.cd_tipo_pessoa
	from	pessoa_juridica d,
		tributo c,
		nota_fiscal_trib b,
		nota_fiscal a
	where	a.nr_sequencia		= b.nr_sequencia(+)
	and	b.cd_tributo		= c.cd_tributo(+)
	and	a.ie_situacao		= '1'
	and	a.dt_atualizacao_estoque	is not null
	and	a.cd_cgc			= d.cd_cgc(+)
	and	(d.cd_tipo_pessoa		= cd_tipo_pessoa_p or cd_tipo_pessoa_p is null)
	and	a.cd_estabelecimento	= cd_estabelecimento_p)
where	(cd_cgc				= cd_cgc_p or cd_cgc_p is null)
and	(cd_pessoa_fisica			= cd_pessoa_fisica_p or cd_pessoa_fisica_p is null)
and	((ie_tipo_pessoa_p = 'A') or
	 ((ie_tipo_pessoa_p = 'PF') and (cd_pessoa_fisica is not null)) or
	 ((ie_tipo_pessoa_p = 'PJ') and (cd_cgc is not null)))
and	(trunc(dt_referencia, 'dd') 	between dt_inicial_p and dt_final_p);

begin

delete	w_tributo_fornec
where	nm_usuario = nm_usuario_p 
or	dt_atualizacao < sysdate - 1;

open c01;
loop
fetch c01 into 
	ie_origem_w,
	nr_documento_w,
	dt_referencia_w,
	dt_liquidacao_w,
	vl_documento_w,
	vl_tributo_w,
	ie_tipo_tributo_w,
	cd_cgc_w,
	cd_pessoa_fisica_w,
	cd_tipo_pessoa_w;
exit when c01%notfound;

	VL_IR_w			:= 0;
	VL_PIS_w		:= 0;
	VL_INSS_w		:= 0;
	VL_ISS_w		:= 0;
	VL_PIS_COFINS_W	:= 0;

	if	(ie_tipo_tributo_w = 'IR') then
		VL_IR_w	:= vl_tributo_w;
	elsif	(ie_tipo_tributo_w = 'PIS') then
		VL_PIS_w	:= vl_tributo_w;
	elsif	(ie_tipo_tributo_w = 'INSS') then
		VL_INSS_w	:= vl_tributo_w;
	elsif	(ie_tipo_tributo_w = 'ISS') then
		VL_ISS_w	:= vl_tributo_w;
	elsif	(ie_tipo_tributo_w = 'PISCOFINSCSLL') then
		VL_PIS_COFINS_W	:= vl_tributo_w;		
	end if;

	select	count(*)
	into	cont_w
	from	w_tributo_fornec
	where	nm_usuario	= nm_usuario_p
	and	ie_origem	= ie_origem_w
	and	nr_documento	= nr_documento_w
	and	(cd_cgc	= cd_cgc_w or cd_cgc_w is null)
	and	(cd_pessoa_fisica = cd_pessoa_fisica_w or cd_pessoa_fisica_w is null);

	if	(cont_w = 0) then
		insert into w_tributo_fornec
			(NR_SEQUENCIA,
			DT_ATUALIZACAO,
			NM_USUARIO,
			IE_ORIGEM,
			NR_DOCUMENTO,
			CD_CGC,
			CD_PESSOA_FISICA,
			DT_REFERENCIA,
			DT_LIQUIDACAO,
			VL_DOCUMENTO,
			VL_IR,
			VL_PIS,
			VL_INSS,
			VL_ISS,
			CD_TIPO_PESSOA,
			VL_PIS_COFINS)
		values	(w_tributo_fornec_seq.nextval,
			sysdate,
			NM_USUARIO_p,
			IE_ORIGEM_w,
			NR_DOCUMENTO_w,
			CD_CGC_w,
			CD_PESSOA_FISICA_w,
			DT_REFERENCIA_w,
			DT_LIQUIDACAO_w,
			VL_DOCUMENTO_w,
			VL_IR_w,
			VL_PIS_w,
			VL_INSS_w,
			VL_ISS_w,
			CD_TIPO_PESSOA_W,
			VL_PIS_COFINS_W);
	else
		update	w_tributo_fornec
		set	VL_IR		= VL_IR + VL_IR_w,
			VL_PIS		= VL_PIS + VL_PIS_w,
			VL_INSS		= VL_INSS + VL_INSS_w,
			VL_ISS		= VL_ISS + VL_ISS_w,
			VL_PIS_COFINS	= VL_PIS_COFINS + VL_PIS_COFINS_w
		where	nm_usuario	= nm_usuario_p
		and	ie_origem	= ie_origem_w
		and	nr_documento	= nr_documento_w
		and	(cd_cgc	= cd_cgc_w or cd_cgc_w is null)
		and	(cd_pessoa_fisica = cd_pessoa_fisica_w or cd_pessoa_fisica_w is null);
	end if;
end loop;
close c01;

commit;

end GERAR_W_TRIBUTO_FORNEC;
/