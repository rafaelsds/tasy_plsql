create or replace
procedure pls_define_valor_partic
		(	nr_seq_proc_p		in	Number,
			nr_seq_partic_p		in 	Number,
			vl_anestesista_p	in	Number,
			vl_medico_p		in	Number,
			vl_auxiliares_p		in	Number,
			nm_usuario_p		in	Varchar2,
			ie_tipo_tabela_p	in	Varchar2,
			nr_seq_plano_p		in	pls_segurado.nr_seq_plano%type,
			vl_participante_p	out	Number) is
			
tx_valor_medico_w	Number(15,4);
tx_valor_anestesista_w	Number(15,4);
tx_valor_auxiliar_w	Number(15,4);
vl_participante_w	Number(15,4);
cd_procedimento_w	Number(15);
nr_seq_grau_partic_w	Number(10);
cd_area_w		Number(15);
cd_especialidade_w	Number(15);
cd_grupo_w		Number(15);
ie_origem_w		Number(10);

Cursor C01 is
	select	nvl(tx_valor_medico,0) tx_valor_medico,
		nvl(tx_valor_anestesista,0) tx_valor_anestesista,
		nvl(tx_valor_auxiliar,0) tx_valor_auxiliar
	from 	pls_grau_partic_taxa
	where	nr_seq_grau_partic	= nr_seq_grau_partic_w
	and	((cd_area_procedimento	is null) or (cd_area_procedimento 	= cd_area_w))
	and 	((cd_especialidade	is null) or (cd_especialidade 		= cd_especialidade_w))
	and 	((cd_grupo_proc		is null) or (cd_grupo_proc 		= cd_grupo_w))
	and 	((cd_procedimento	is null) or (cd_procedimento 		= cd_procedimento_w and ie_origem_proced = ie_origem_w))
	and	((nr_seq_plano		is null) or (nr_seq_plano 		= nr_seq_plano_p))
	order by	
		cd_area_procedimento desc, 
		cd_especialidade desc, 
		cd_grupo_proc desc, 
		cd_procedimento desc,
		nr_seq_plano desc;

begin

if	(ie_tipo_tabela_p	= 'P') then
	
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_grau_partic,
		cd_grupo_proc,
		cd_especialidade,
		cd_area_procedimento
	into	cd_procedimento_w,
		ie_origem_w,
		nr_seq_grau_partic_w,
		cd_grupo_w,
		cd_especialidade_w,
		cd_area_w
	from	pls_proc_participante_v
	where	nr_sequencia	= nr_seq_partic_p;
	
elsif	(ie_tipo_tabela_p	= 'C') then

	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_grau_partic_conta,
		cd_grupo_proc,
		cd_especialidade,
		cd_area_procedimento
	into	cd_procedimento_w,
		ie_origem_w,
		nr_seq_grau_partic_w,
		cd_grupo_w,
		cd_especialidade_w,
		cd_area_w
	from	pls_conta_proc_v
	where	nr_sequencia	= nr_seq_proc_p;
end if;

/* Se n�o tem o grau de participan��o informado, o sistema deve tratar como sendo m�dico */
if	(nvl(nr_seq_grau_partic_w,0) > 0) then
	for r_c01_w in C01() loop
		begin
		tx_valor_medico_w	:= r_c01_w.tx_valor_medico;
		tx_valor_anestesista_w	:= r_c01_w.tx_valor_anestesista;
		tx_valor_auxiliar_w	:= r_c01_w.tx_valor_auxiliar;
		end;
	end loop;
else
	tx_valor_medico_w	:= 1;
end if;

if	(tx_valor_medico_w > 0) then
	vl_participante_w 	:= vl_medico_p 		* tx_valor_medico_w;
elsif	((tx_valor_auxiliar_w > 0) and (vl_auxiliares_p is not null)) then
	vl_participante_w 	:= vl_auxiliares_p	* tx_valor_auxiliar_w;
else
	vl_participante_w 	:= vl_anestesista_p	* tx_valor_anestesista_w;
end if;


vl_participante_p	:= vl_participante_w;

end pls_define_valor_partic;
/