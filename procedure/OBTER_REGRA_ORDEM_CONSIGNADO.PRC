create or replace
procedure obter_regra_ordem_consignado(
				cd_estabelecimento_p	in	number,
				cd_local_estoque_p	in	number,
				cd_operacao_estoque_p	in	number,
				cd_material_p		in	number,
				cd_cgc_p		in	varchar2,
				cd_convenio_p		in	number,
				cd_setor_prescricao_p	in	number,
				ie_gera_ordem_p		out	varchar2,
				cd_local_entrega_p	out	number,
				cd_centro_custo_p	out	number,
				pr_desconto_p		out	number,
				nm_usuario_dest_p	out	varchar2,
				cd_perfil_comunic_p	out	number,
				cd_pessoa_solicitante_p	out	varchar2,
				ie_gera_oc_reposicao_p	out	varchar2) is

cd_grupo_material_w		number(03,0);
cd_subgrupo_w			number(03,0);
cd_classe_material_w		number(05,0);
ie_gera_ordem_w			varchar2(01);
nr_regras_w			number(05,0);
cd_local_entrega_w		Number(15,0);
cd_centro_custo_w		Number(15,0);
pr_desconto_w			Number(15,0);
nm_usuario_dest_w		Varchar2(255);
cd_pessoa_solicitante_w		Varchar2(10);
ie_regra_oc_consig_vazia_w	varchar2(15);
cd_perfil_comunic_w		number(5);
ie_gera_oc_reposicao_w		varchar2(1);

cursor	c01 is
	select	ie_gera_ordem,
		cd_local_entrega,
		cd_centro_custo,
		pr_desconto,
		nm_usuario_dest,
		cd_pessoa_solicitante,
		cd_perfil_comunic,
		nvl(ie_gera_oc_reposicao,'N')
	from	regra_ordem_consignado
	where	cd_estabelecimento = cd_estabelecimento_p
	and	cd_local_estoque = cd_local_estoque_p
	and	cd_operacao_estoque = cd_operacao_estoque_p
	and	nvl(cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
	and	nvl(cd_subgrupo_material, cd_subgrupo_w) = cd_subgrupo_w
	and	nvl(cd_classe_material, cd_classe_material_w) = cd_classe_material_w
	and	nvl(cd_material, cd_material_p) = cd_material_p
	and	nvl(cd_fornecedor, cd_cgc_p) = cd_cgc_p
	and	nvl(cd_convenio, cd_convenio_p) = cd_convenio_p
	and	nvl(cd_setor_prescricao, cd_setor_prescricao_p) = cd_setor_prescricao_p
	order by
		nvl(cd_fornecedor, '0'),
		nvl(cd_convenio, 0),
		nvl(cd_setor_prescricao, 0),
		nvl(cd_material, 0),
		nvl(cd_classe_material, 0),
		nvl(cd_subgrupo_material, 0),
		nvl(cd_grupo_material, 0);

begin

select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_w,
	cd_classe_material_w
from	estrutura_material_v
where 	cd_material = cd_material_p;

select	nvl(ie_regra_oc_consig_vazia,'S')
into	ie_regra_oc_consig_vazia_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

select	count(*)
into	nr_regras_w
from	regra_ordem_consignado
where	cd_estabelecimento = cd_estabelecimento_p;

if	(nr_regras_w = 0) then	
	if	(ie_regra_oc_consig_vazia_w = 'N') then
		ie_gera_ordem_w	:= 'N';
	else
		ie_gera_ordem_w	:= 'S';
	end if;
else	
	begin
	open	c01;
	loop
	fetch	c01 into 
		ie_gera_ordem_w,
		cd_local_entrega_w,
		cd_centro_custo_w,
		pr_desconto_w,
		nm_usuario_dest_w,
		cd_pessoa_solicitante_w,
		cd_perfil_comunic_w,
		ie_gera_oc_reposicao_w;
	exit when c01%notfound;
	end loop;
	close 	c01;

	end;
end if;

ie_gera_ordem_p		:= ie_gera_ordem_w;
cd_local_entrega_p	:= cd_local_entrega_w;
cd_centro_custo_p	:= cd_centro_custo_w;
pr_desconto_p		:= pr_desconto_w;
nm_usuario_dest_p	:= nm_usuario_dest_w;
cd_pessoa_solicitante_p	:= cd_pessoa_solicitante_w;
cd_perfil_comunic_p	:= cd_perfil_comunic_w;
ie_gera_oc_reposicao_p	:= ie_gera_oc_reposicao_w;

end obter_regra_ordem_consignado;
/
