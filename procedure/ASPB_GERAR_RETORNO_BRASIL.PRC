create or replace
procedure aspb_gerar_retorno_brasil(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p		varchar2) is 

nr_seq_reg_t_w			number(10);
nr_seq_reg_u_w			number(10);
nr_titulo_w			number(10);
vl_titulo_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
vl_outras_despesas_w		number(15,2);
dt_liquidacao_w			date;
ds_titulo_w			varchar2(255);
vl_cobranca_w			number(15,2);
vl_alterar_w			number(15,2);
cd_ocorrencia_w			number(10);
nr_seq_ocorrencia_ret_w		number(10);
vl_saldo_inclusao_w		number(15,2);
qt_titulo_cobr_w		number(10);
qt_titulo_arq_w			number(10);
ds_titulo_externo_w		varchar2(255);
ds_titulo_number_w		number(15);
vl_juros_w			number(15,2);
ie_somente_liquid_w		varchar2(1);
ie_rejeitado_w			varchar2(1);
cd_estabelecimento_w		number(4);
ie_rejeitado_princ_w		varchar2(1);
nr_seq_ocorrencia_ret_princ_w	number(10);
dt_remessa_retorno_w		date;
dt_remessa_retorno_ww		varchar2(8);

cursor c01 is
	select	nr_sequencia,
		trim(substr(ds_string,106,25)),
		to_number(substr(ds_string,82,15))/100,
		trim(substr(ds_string,59,15))
	from	w_retorno_banco
	where	substr(ds_string,8,1)	= '3'
	and	substr(ds_string,14,1)	= 'T'
	and	substr(ds_string,16,2)	<> '28'
	and	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p;

begin

select	MAX(SUBSTR(ds_string,146,8))
into	dt_remessa_retorno_ww
from	w_retorno_banco
where	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'U'
and	substr(ds_string,16,2)	<> '28'
and	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p;

if	(dt_remessa_retorno_ww is not null) then
	
	dt_remessa_retorno_w := to_date(dt_remessa_retorno_ww,'dd/mm/yyyy');
	
	update	cobranca_escritural
	set	dt_credito_bancario		= dt_remessa_retorno_w
	where	nr_sequencia		= 	nr_seq_cobr_escrit_p;
end if;

open C01;
loop
fetch C01 into	
	nr_seq_reg_T_w,
	ds_titulo_w,
	vl_cobranca_w,
	ds_titulo_externo_w;
exit when C01%notfound;
	begin
	vl_alterar_w	:= 0;

	ds_titulo_number_w	:= somente_numero(ds_titulo_w);

	select	max(nr_titulo),
		max(cd_estabelecimento)
	into	nr_titulo_w,
		cd_estabelecimento_w
	from	titulo_receber
	where	nr_titulo	= ds_titulo_number_w;

	if	(nr_titulo_w is not null) then -- Se encontrou o t�tulo importa, sen�o grava no log
		obter_param_usuario(815,11,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_somente_liquid_w);

		select	count(1)
		into	qt_titulo_cobr_w
		from	titulo_receber_cobr a
		where	a.nr_titulo		= nr_titulo_w
		and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

		if	(nvl(qt_titulo_cobr_w,0) = 0) then

			select	max(vl_titulo),
				max(vl_saldo_titulo)
			into	vl_titulo_w,
				vl_saldo_inclusao_w
			from	titulo_receber
			where	nr_titulo	= nr_titulo_w;

			nr_seq_reg_U_w := nr_seq_reg_T_w + 1;

			select	nvl(to_number(substr(ds_string,123,15))/100,0),
				nvl(to_number(substr(ds_string,33,15))/100,0),
				nvl(to_number(substr(ds_string,48,15))/100,0),
				nvl(to_number(substr(ds_string,93,15))/100,0),
				nvl(to_number(substr(ds_string,108,15))/100,0),
				to_date(decode(substr(ds_string,138,8),'00000000',null,substr(ds_string,138,8)),'dd/mm/yyyy'), -- antes era 146
				to_number(substr(ds_string,16,2)),
				nvl(to_number(substr(ds_string,18,15))/100,0)
			into	vl_acrescimo_w,
				vl_desconto_w,
				vl_abatimento_w,
				vl_liquido_w,
				vl_outras_despesas_w,
				dt_liquidacao_w,
				cd_ocorrencia_w,
				vl_juros_w
			from	w_retorno_banco
			where	nr_sequencia	= nr_seq_reg_U_w;

			select 	max(a.nr_sequencia),
				max(a.ie_rejeitado)
			into	nr_seq_ocorrencia_ret_w,
				ie_rejeitado_w
			from	banco_ocorr_escrit_ret a
			where	a.cd_banco	= 001
			and	somente_numero(a.cd_ocorrencia) = cd_ocorrencia_w;

			if	(nvl(vl_liquido_w,0)	= 0) then
				vl_liquido_w	:= nvl(vl_cobranca_w,0);
			end if;

			insert	into titulo_receber_cobr 
				(nr_sequencia,
				nr_titulo,
				cd_banco,
				vl_cobranca,
				vl_desconto,
				vl_acrescimo,
				vl_despesa_bancaria,
				vl_liquidacao,
				dt_liquidacao,
				dt_atualizacao,
				nm_usuario,
				nr_seq_cobranca,
				nr_seq_ocorrencia_ret,
				vl_saldo_inclusao,
				vl_juros_calc,
				vl_juros)
			values	(titulo_receber_cobr_seq.nextval,
				nr_titulo_w,
				001,
				vl_titulo_w,
				vl_desconto_w,
				vl_acrescimo_w,
				vl_outras_despesas_w,
				nvl(vl_liquido_w,0),
				dt_liquidacao_w,
				sysdate,
				nm_usuario_p,
				nr_seq_cobr_escrit_p,
				nr_seq_ocorrencia_ret_w,
				vl_saldo_inclusao_w,
				0,
				vl_juros_w);

			if	(nvl(ie_somente_liquid_w,'N')	= 'S') and
				(nvl(ie_rejeitado_w,'N') not in ('L','N')) then

				insert	into cobranca_escrit_log
					(ds_log,
					dt_atualizacao,
					dt_atualizacao_nrec,
					dt_credito,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_cobranca,
					nr_sequencia,
					nr_titulo,
					vl_saldo_titulo)
				values	('O t�tulo ' || nr_titulo_w || ' n�o pode ser baixado porque n�o possui ocorr�ncia de liquida��o. Par�metro [11]' || chr(13) || chr(10) ||
					'Verifique se a ocorr�ncia est� cadastrada corretamente em Cadastros Financeiros / Banco / Contas a Receber / Ocorr�ncia retorno',
					sysdate,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nr_seq_cobr_escrit_p,
					cobranca_escrit_log_seq.nextval,
					nr_titulo_w,
					nvl(vl_liquido_w,0));
			end if;
		else
			select	count(1)
			into	qt_titulo_arq_w
			from	w_retorno_banco a
			where	substr(a.ds_string,8,1)			= '3'
			and	substr(a.ds_string,14,1)		= 'T'
			and	substr(a.ds_string,16,2)		<> '28'
			and	trim(substr(a.ds_string,106,25))	= trim(ds_titulo_w)
			and	a.nr_seq_cobr_escrit			= nr_seq_cobr_escrit_p;

			if	(nvl(qt_titulo_arq_w,0) > 1) then -- se o t�tulo aparecer mais de uma vez no arquivo
				nr_seq_reg_U_w := nr_seq_reg_T_w + 1; -- somar os valores do segmento U

				select	nvl(to_number(substr(ds_string,18,15))/100,0),
					nvl(to_number(substr(ds_string,33,15))/100,0),
					nvl(to_number(substr(ds_string,93,15))/100,0),
					nvl(to_number(substr(ds_string,108,15))/100,0),
					to_date(decode(substr(ds_string,138,8),'00000000',null,substr(ds_string,138,8)),'dd/mm/yyyy'),
					to_number(substr(ds_string,16,2))
				into	vl_acrescimo_w,
					vl_desconto_w,
					vl_liquido_w,
					vl_outras_despesas_w,
					dt_liquidacao_w,
					cd_ocorrencia_w
				from	w_retorno_banco
				where	nr_sequencia	= nr_seq_reg_U_w;

				select 	max(a.nr_sequencia)
				into	nr_seq_ocorrencia_ret_w
				from	banco_ocorr_escrit_ret a
				where	somente_numero(a.cd_ocorrencia) = cd_ocorrencia_w
				and	a.cd_banco	= 001;
				
				select	max(b.ie_rejeitado),
					max(b.nr_sequencia)
				into	ie_rejeitado_princ_w,
					nr_seq_ocorrencia_ret_princ_w
				from	titulo_receber_cobr a,
					banco_ocorr_escrit_ret b					
				where	a.nr_seq_ocorrencia_ret = b.nr_sequencia
				and	a.nr_titulo		= nr_titulo_w
				and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;
				
				if	(ie_rejeitado_princ_w = 'L') and
					(nr_seq_ocorrencia_ret_princ_w is not null) then
					nr_seq_ocorrencia_ret_w := nr_seq_ocorrencia_ret_princ_w;
				end if;
				
				-- IE_REJEITADO - (N,S,B,L)(Outros,Ocorr�ncia de rejei��o,Ocorr�ncia de bloqueio,Liquida��o)
				if	(ie_rejeitado_princ_w <> 'L') then
					dt_liquidacao_w := null;
					nr_seq_ocorrencia_ret_w := null;
					vl_acrescimo_w := 0;
					vl_desconto_w := 0;
					vl_outras_despesas_w := 0;
					vl_liquido_w := 0;
				end if;

				update	titulo_receber_cobr
				set	dt_liquidacao		= nvl(dt_liquidacao_w,dt_liquidacao),
					nr_seq_ocorrencia_ret	= nvl(nr_seq_ocorrencia_ret_w,nr_seq_ocorrencia_ret),
					vl_acrescimo		= nvl(vl_acrescimo,0) + vl_acrescimo_w,
					vl_desconto		= nvl(vl_desconto,0) + vl_desconto_w,
					vl_despesa_bancaria	= nvl(vl_despesa_bancaria,0) + vl_outras_despesas_w,
					vl_liquidacao		= nvl(vl_liquidacao,0) + vl_liquido_w
				where	nr_titulo		= nr_titulo_w
				and	nr_seq_cobranca		= nr_seq_cobr_escrit_p;
			else

				insert	into	log_tasy
					(nm_usuario,
					dt_atualizacao,
					cd_log,
					ds_log)
				values	(nm_usuario_p,
					sysdate,
					55760,
					'O t�tulo ' || ds_titulo_w || ' j� est� nessa cobran�a!');
			end if;

		end if;
	else
		insert	into cobranca_escrit_log
			(ds_log,
			dt_atualizacao,
			dt_atualizacao_nrec,
			dt_credito,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_cobranca,
			nr_sequencia)
		values	('O t�tulo ' || ds_titulo_w || ' n�o foi importado porque n�o foi encontrado no Tasy.' || chr(13) || chr(10) ||
			'Verifique na fun��o Consulta de T�tulos a Receber se este t�tulo existe.',
			sysdate,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_cobr_escrit_p,
			cobranca_escrit_log_seq.nextval);

		insert	into	log_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');

	end if;
	
	end;
end loop;
close c01;

commit;

end aspb_gerar_retorno_brasil;
/