create or replace 
procedure ageint_gerar_consulta_agecons2(cd_estabelecimento_p	number,
					cd_agenda_p     	number,
					dt_agenda_p	date,
					nm_usuario_p	varchar2) is 


cd_especialidade_w		number(10);
cd_medico_exec_w	varchar2(10);
cd_pessoa_fisica_w	varchar2(10);
nm_paciente_w		varchar2(60);
ie_status_Agenda_w	varchar2(3);
nr_minuto_duracao_w	number(10);
dt_agenda_w		date;
nr_seq_agenda_w		number(10,0);
nr_seq_status_pac_w	number(10,0);
nr_atendimento_w		number(10);
cd_convenio_w		number(5);
hr_inicial_intervalo_w	date;
hr_final_intervalo_w		date;
cd_turno_ww		varchar2(1);
ie_encaixe_w		varchar2(1);
ie_classif_Agenda_w	Varchar2(0005);
qt_hor_w		number(10);
ie_feriado_w		varchar2(1);
ie_sobra_horario_w	varchar2(1);
ds_horarios_w		varchar2(255);
ds_motivo_w			Varchar2(255);

Cursor C03 is
	select	cd_pessoa_fisica,
			substr(nm_paciente,1,60),
			cd_medico_req,
			ie_status_agenda,
			nr_minuto_duracao,
			dt_Agenda,
			nr_sequencia,
			nr_seq_status_pac,
			nr_atendimento,
			cd_convenio,
			cd_turno,
			nvl(ie_encaixe,'N'),
			ie_classif_agenda,
			SUBSTR(obter_motivo_agecons(nr_sequencia,ie_status_agenda,NVL(nr_seq_motivo_transf,somente_numero(cd_motivo_cancelamento))),1,255)
	from	agenda_consulta
	where	cd_agenda		= cd_agenda_p
	and		trunc(dt_agenda)	= trunc(dt_agenda_p)
	order by dt_agenda;
	
BEGIN

/*gerar_horario_agenda_consulta(nm_usuario_p, cd_estabelecimento_p, cd_agenda_p, dt_agenda_p, null, null, 0);*/
select	nvl(ie_feriado,'N'),
	nvl(ie_gerar_sobra_horario,'N')
into	ie_feriado_w,
	ie_sobra_horario_w
from	agenda
where	cd_agenda = cd_agenda_p;

horario_livre_consulta(cd_estabelecimento_p, cd_agenda_p, ie_feriado_w, dt_agenda_p, nm_usuario_p, 'S', ie_sobra_horario_w, 'N', 0,ds_horarios_w);	

delete	ageint_consulta_horarios
where	cd_agenda	= cd_agenda_p
and	nm_usuario	= nm_usuario_p
and	dt_agenda	between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 83699/86400;

select	max(cd_especialidade)
into	cd_especialidade_w
from	agenda
where	cd_agenda	= cd_agenda_p;

open C03;
loop
fetch C03 into	
	cd_pessoa_fisica_w,
	nm_paciente_w,
	cd_medico_exec_w,
	ie_status_agenda_w,
	nr_minuto_duracao_w,
	dt_agenda_w,
	nr_seq_agenda_w,
	nr_seq_status_pac_w,
	nr_atendimento_w,
	cd_convenio_w,
	cd_turno_ww,
	ie_encaixe_w,
	ie_classif_agenda_w,
	ds_motivo_w;
exit when C03%notfound;
	begin
	
	insert into ageint_consulta_horarios
		(dt_Atualizacao,
		nm_usuario,
		cd_agenda,
		cd_medico_exec,
		cd_paciente,
		nm_paciente,
		ie_status_agenda,
		nr_minuto_duracao,
		dt_agenda,
		nr_seq_agenda,
		nr_seq_status_pac,
		nr_atendimento,
		cd_convenio,
		cd_turno,
		ie_encaixe,
		cd_especialidade,
		ie_classif_agenda,
		ds_motivo)
	values
		(sysdate,
		nm_usuario_p,
		cd_Agenda_p,
		cd_medico_exec_w,
		cd_pessoa_fisica_w,
		nm_paciente_w,
		ie_status_Agenda_w,
		nr_minuto_duracao_w,
		dt_agenda_w,
		nr_seq_agenda_w,
		nr_seq_status_pac_w,
		nr_atendimento_w,
		cd_convenio_w,
		cd_turno_ww,
		ie_encaixe_w,
		cd_especialidade_w,
		ie_classif_agenda_w,
		ds_motivo_w);
	end;
end loop;
close C03;

commit;

end ageint_gerar_consulta_agecons2;
/
