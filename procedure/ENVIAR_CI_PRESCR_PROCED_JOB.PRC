create or replace
procedure Enviar_CI_prescr_proced_job (nm_usuario_p		varchar2) is

ds_titulo_w		varchar2(255);
nm_paciente_w		varchar2(255);
nm_medico_w		varchar2(255);
nm_medico_exec_w	varchar2(255);
ds_comunicado_w		Long;
ds_comunic_w		varchar2(4000);
ds_procedimento_w	varchar2(4000);
nm_usuario_destino_w	varchar2(255);
ds_lista_usuario_destino_w	varchar2(1000);
ds_observacao_w		varchar2(255);
ds_dado_clinico_w	varchar2(255);
ds_erro_w		varchar2(255);
ds_unidade_w		varchar2(255);
cd_procedimento_w	number(15,0);
nr_atendimento_w	number(15,0);
cd_perfil_w			varchar2(4000);
cd_setor_envio_w    varchar2(4000);
cd_setor_destino_w	number(15,0);
nr_sequencia_w		number(15,0);
nr_sequencia_ww		number(15,0);
nr_sequencia_www	number(15,0);
cd_area_procedimento_w	number(15,0);
cd_perfil_destino_w	number(15,0);
cd_especialidade_w	number(15,0);
cd_grupo_proc_w		number(15,0);
cd_setor_w		number(15,0);
ie_origem_proced_w	number(15,0);
ie_tipo_atendimento_w	number(15,0);
nr_seq_regra_w		number(15,0);
ie_enviar_prescritor_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(50);
ie_enviar_executor_w	varchar2(1);
nr_seq_comunic_w	number(15,0);
nr_seq_proc_interno_w	number(15,0);
ds_quebra_w		varchar2(2) := chr(13) || chr(10);

cd_intervalo_w		varchar2(30);
ds_horarios_w		varchar2(255);
ds_intervalo_w		varchar2(255);
ds_setor_exec_w		varchar2(255);
ds_setor_prescr_w	varchar2(255);	
dt_liberacao_w		date;
dt_prescricao_w		date;
dt_prev_exec_w		date;
qt_procedimento_w	number(8,3);
ds_convenio_w		varchar2(255);
cd_protocolo_w		number(10);
nr_seq_protocolo_w	number(10);
cd_medico_exec_w	number(15,0);
ie_gerar_comunic_w	varchar2(1);
ie_aplica_prescr_eup_w	varchar2(1);
cd_estabelecimento_w	number(10)	:= obter_estab_usuario(nm_usuario_p);


nr_prescricao_w		number(15);

cursor c01 is
select	a.nr_sequencia,
	a.ds_titulo,
	a.ds_comunicado,
	a.cd_area_procedimento,
	a.cd_especialidade,
	a.cd_grupo_proc,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.ie_tipo_atendimento,
	nvl(a.ie_enviar_prescritor,'S'),
	a.nr_seq_proc_interno,
	a.cd_protocolo,
	a.nr_seq_protocolo,
	nvl(a.ie_enviar_executor,'S'),
	a.cd_pessoa_fisica,
	nvl(a.ie_aplica_prescr_eup,'S')
from	rep_regra_envio_ci_proced a
where	nvl(ie_job,'N') = 'S'
and		((cd_perfil is null) or (cd_perfil = obter_Perfil_ativo))
and		nvl(cd_estabelecimento,nvl(cd_estabelecimento_w,0))	= nvl(cd_estabelecimento_w,0);

cursor c001 is
select	distinct
	p.nr_prescricao,
	obter_nome_pf_pj(p.cd_pessoa_fisica,null),
	obter_nome_pf_pj(p.cd_prescritor,null),
	p.cd_setor_atendimento,
	p.nr_atendimento,
	obter_unidade_atendimento(p.nr_atendimento,'A','U'),
	p.dt_prescricao,
	nvl(p.dt_liberacao_medico,p.dt_liberacao),
	obter_nome_setor(p.cd_setor_atendimento),
	substr(obter_desc_convenio(obter_convenio_atendimento(p.nr_atendimento)),1,100)
from	prescr_medica p,
	estrutura_procedimento_v a,
	prescr_procedimento c
where	a.cd_procedimento	= c.cd_procedimento
and	a.ie_origem_proced	= c.ie_origem_proced
and	c.nr_prescricao		= p.nr_prescricao
and	trunc(p.dt_prescricao,'dd') = trunc(sysdate -1,'dd')
and	((cd_procedimento_w	is null) or
	 (c.cd_procedimento	= cd_procedimento_w))
and	((ie_origem_proced_w	is null) or
	 (c.ie_origem_proced	= ie_origem_proced_w) or (cd_procedimento_w is null))
and	((cd_grupo_proc_w is null) or
	 (a.cd_grupo_proc = cd_grupo_proc_w))
and	((cd_area_procedimento_w is null) or
	 (a.cd_area_procedimento = cd_area_procedimento_w))
and	((p.cd_prescritor = cd_pessoa_fisica_w) or (cd_pessoa_fisica_w is null))
and	((cd_especialidade_w is null) or
	 (a.cd_especialidade = cd_especialidade_w))
and	((nr_seq_proc_interno_w is null) or
	 (c.nr_seq_proc_interno	= nr_seq_proc_interno_w))
and	((ie_tipo_atendimento_w is null) or
	 (obter_tipo_atendimento(obter_atendimento_prescr(p.nr_prescricao)) = ie_tipo_atendimento_w))
and	((ie_aplica_prescr_eup_w = 'S') or
	 (nvl(p.cd_funcao_origem,924) <> 916))
and	((cd_protocolo_w is null) or
	 (p.cd_protocolo = cd_protocolo_w))	 
and	((nr_seq_protocolo_w is null) or
	 (p.nr_seq_protocolo = nr_seq_protocolo_w))
order by p.nr_atendimento, p.nr_prescricao;
	
cursor c02 is
select	b.cd_perfil
from	rep_perfil_ci_proced b
where	b.nr_seq_regra	= nr_sequencia_ww;

cursor c03 is
select	b.cd_setor_recebimento 
from	rep_regra_ci_proced_setor b
where	b.nr_seq_regra	= nr_sequencia_ww
and    b.cd_setor_recebimento is not null;

cursor c14 is
select	substr(obter_descricao_procedimento(c.cd_procedimento, c.ie_origem_proced),1,80),
	c.ds_observacao,
	c.ds_dado_clinico,
	c.cd_procedimento,
	nvl(obter_desc_intervalo_prescr(c.cd_intervalo),c.cd_intervalo),
	obter_desc_intervalo(c.cd_intervalo),
	c.ds_horarios,
	c.dt_prev_execucao,
	c.qt_procedimento,
	obter_nome_setor(c.cd_setor_atendimento),
	c.cd_medico_exec
from	prescr_medica p,
	estrutura_procedimento_v a,
	prescr_procedimento c
where	a.cd_procedimento	= c.cd_procedimento
and	a.ie_origem_proced	= c.ie_origem_proced
and	c.nr_prescricao		= p.nr_prescricao
and	c.nr_prescricao		= nr_prescricao_w;
	 
cursor	c15 is
select	b.nm_usuario_regra
from	rep_regra_envio_ci_proced a,
	rep_usuario_ci_proced b
where	a.nr_sequencia	= b.nr_seq_regra
and	b.nr_seq_regra	= nr_sequencia_ww
and	obter_se_envia_ci_setor(a.nr_sequencia, cd_setor_w) = 'S';

begin

select	min(nr_sequencia) 
into	nr_sequencia_w
from	comunic_interna_classif 
where	ie_tipo	= 'F';

open c01;
loop
fetch c01 into
	nr_sequencia_ww,
	ds_titulo_w,
	ds_comunic_w,
	cd_area_procedimento_w,
	cd_especialidade_w,
	cd_grupo_proc_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_tipo_atendimento_w,
	ie_enviar_prescritor_w,
	nr_seq_proc_interno_w,
	cd_protocolo_w,
	nr_seq_protocolo_w,
	ie_enviar_executor_w,
	cd_pessoa_fisica_w,
	ie_aplica_prescr_eup_w;
exit when c01%notfound;

	ds_comunicado_w := null;

	open C001;
	loop
	fetch C001 into	
		nr_prescricao_w,
		nm_paciente_w,
		nm_medico_w,
		cd_setor_w,
		nr_atendimento_w,
		ds_unidade_w,
		dt_prescricao_w,
		dt_liberacao_w,
		ds_setor_prescr_w,
		ds_convenio_w;
	exit when C001%notfound;
	
		ie_gerar_comunic_w := obter_se_envia_ci_setor(nr_sequencia_www, cd_setor_w);
	
		if	(ie_gerar_comunic_w = 'S') then
		
			if	(ds_comunicado_w is not null) then
				ds_comunicado_w	:=	ds_comunicado_w || ds_quebra_W || ds_quebra_W||
							obter_desc_expressao(325814)/*'Prescri��o: '*/ || nr_prescricao_w || ds_quebra_W ||
							obter_desc_expressao(775048)/*'Atendimento: '*/ || nr_atendimento_w || ds_quebra_W ||
							obter_desc_expressao(325811)/*'Paciente: '*/ || nm_paciente_w || ds_quebra_W ||
							obter_desc_expressao(298386)||': ' /*'Setor da prescri��o:' */ || ds_setor_prescr_w || ds_quebra_W ||
							obter_desc_expressao(345228)||': ' /*'Leito do paciente:' */ || ds_unidade_w || ds_quebra_W ||
							obter_desc_expressao(326148)/*'Conv�nio: '*/|| ds_convenio_w || ds_quebra_w ||
							obter_desc_expressao(335718)||':' ||to_char(dt_prescricao_W,'dd/mm/yyyy hh24:mi:ss') || ds_quebra_W ||
							obter_desc_expressao(595025)/*'Dt Libera��o:'*/ ||to_char(dt_liberacao_w,'dd/mm/yyyy hh24:mi:ss') || ds_quebra_W ||
							obter_desc_expressao(296217)||': ' /*'Prescritor: '*/|| nm_medico_w || ds_quebra_W|| ds_quebra_W;
			else
				ds_comunicado_w	:=	obter_desc_expressao(325814)||': ' || nr_prescricao_w || ds_quebra_W ||
							obter_desc_expressao(775048)/*'Atendimento: '*/ || nr_atendimento_w || ds_quebra_W ||
							obter_desc_expressao(325811)/*'Paciente: '*/ || nm_paciente_w || ds_quebra_W ||
							obter_desc_expressao(298386)||': ' || ds_setor_prescr_w || ds_quebra_W ||
							obter_desc_expressao(345228)||': ' /*'Leito do paciente:' */ || ds_unidade_w || ds_quebra_W ||
							obter_desc_expressao(326148)/*'Conv�nio: '*/|| ds_convenio_w || ds_quebra_w ||
							obter_desc_expressao(335718)||':' ||to_char(dt_prescricao_W,'dd/mm/yyyy hh24:mi:ss') || ds_quebra_W ||
							obter_desc_expressao(595025)/*'Dt Libera��o:'*/ ||to_char(dt_liberacao_w,'dd/mm/yyyy hh24:mi:ss') || ds_quebra_W ||
							obter_desc_expressao(296217)||': ' /*'Prescritor: '*/ || nm_medico_w || ds_quebra_W|| ds_quebra_W;
			end if;
			
			open c14;
			loop
			fetch c14 into	
				ds_procedimento_w,
				ds_observacao_w,
				ds_dado_clinico_w,
				cd_procedimento_w,
				cd_intervalo_w,
				ds_intervalo_w,
				ds_horarios_w,
				dt_prev_exec_w,
				qt_procedimento_w,
				ds_setor_exec_w,
				cd_medico_exec_w;
			exit when c14%notfound;
		
				if	(ds_comunicado_w is not null) then
					ds_comunicado_w := ds_comunicado_w || ds_quebra_w ;
				end if;
				
				ds_comunicado_w := ds_comunicado_w || obter_desc_expressao(296423)/*'    Procedimento: '*/ || cd_procedimento_w ||  ' - ' || ds_procedimento_w || ds_quebra_w;
				ds_comunicado_w := ds_comunicado_w || obter_desc_expressao(614525)/*'    Quantidade:   '*/ || qt_procedimento_w || ds_quebra_w; 
				
				if	(dt_prev_exec_w is not null) then
					ds_comunicado_w := ds_comunicado_w || obter_desc_expressao(287132) ||':' /*'    Data Prevista execu��o:   '*/ || to_char(dt_prev_exec_w,'dd/mm/yyyy hh24:mi:ss') || ds_quebra_w; 
				end if;
				
				if	(nvl(cd_intervalo_w, ds_intervalo_w) is not null) then
					ds_comunicado_w := ds_comunicado_w || obter_desc_expressao(326105)/*'    Intervalo:   '*/ || nvl(cd_intervalo_w, ds_intervalo_w) || ds_quebra_w; 
				end if;
				
				if	(ds_horarios_w is not null) then
					ds_comunicado_w := ds_comunicado_w || obter_desc_expressao(771970)||': '/*'    Horarios:   '*/  || ds_horarios_w || ds_quebra_w; 
				end if;
				
				if	(ds_setor_exec_w is not null) then
					ds_comunicado_w := ds_comunicado_w || obter_desc_expressao(325813)/*'    Setor de execu��o:   '*/ || ds_setor_exec_w || ds_quebra_w; 
				end if;
				
				if	(ds_observacao_w is not null) then
					ds_comunicado_w	:= ds_comunicado_w || obter_desc_expressao(329252)/*'    Observa��o: '*/ || ds_observacao_w|| ds_quebra_w;		
				end if;
				
				if	(ds_dado_clinico_w is not null) then
					ds_comunicado_w	:= ds_comunicado_w || obter_desc_expressao(291839)/*'    Indica��o cl�nica: '*/ || ds_dado_clinico_w|| ds_quebra_w;		
				end if;
				
				if	(ie_enviar_executor_w = 'S') and
					(cd_medico_exec_w is not null) then
					select	max(nm_usuario)
					into	nm_usuario_destino_w
					from	usuario
					where	cd_pessoa_fisica = cd_medico_exec_w;
					
					if	(ds_lista_usuario_destino_w is not null) then
						ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
					else
						ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_destino_w,1,1000);
					end if;
					
					
				end if;
			end loop;
			close C14;
		end if;
	end loop;
	close C001;
	
	cd_perfil_w := null;
	open c02;
	loop
	fetch c02 into
		cd_perfil_destino_w;
	exit when c02%notfound;
		if	(cd_perfil_w is not null) or
			(cd_perfil_w <> '') then
			cd_perfil_w := cd_perfil_w || ',';
		end if;
		
		cd_perfil_w := substr(cd_perfil_w || cd_perfil_destino_w,1,1000);	
		
	end loop;
	close c02;
	
	cd_setor_envio_w := null;
	open c03;
	loop
	fetch c03 into
		cd_setor_destino_w;
	exit when c03%notfound;
		if	(cd_setor_envio_w is not null) or
			(cd_setor_envio_w <> '') then
			cd_setor_envio_w := cd_setor_envio_w || ',';
		end if;
		
		cd_setor_envio_w := substr(cd_setor_envio_w || cd_setor_destino_w,1,1000);	
		
	end loop;
	close c03;
	
		
	if	(ds_comunicado_w is not null) then
	
		open C15;
		loop
		fetch C15 into
			nm_usuario_destino_w;
		exit when C15%notfound;
			
			if	(ds_lista_usuario_destino_w is not null) then
				ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
			end if;
			
			ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_destino_w,1,1000);
		end loop;
		close C15;
		
		if	(ds_titulo_w is not null) then
			begin
			select	comunic_interna_seq.nextval
			into	nr_seq_comunic_w
			from	dual;
			
			insert	into comunic_interna(	
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				nm_usuario_destino,
				dt_atualizacao,
				ie_geral,
				ie_gerencial,
				ds_perfil_adicional,
				nr_sequencia,
				nr_seq_classif,
				ds_setor_adicional,
				dt_liberacao)
			values(
				sysdate,
				ds_titulo_w,
				ds_comunic_w||ds_quebra_w ||ds_quebra_w||ds_comunicado_w, 
				nm_usuario_p,
				ds_lista_usuario_destino_w,
				sysdate,
				'N',
				'N',
				cd_perfil_w,
				nr_seq_comunic_w,
				nr_sequencia_w,
				cd_setor_envio_w,
				sysdate);	
				
			if	(ie_enviar_prescritor_w = 'N') then
			
				insert into comunic_interna_lida 
				values(	nr_seq_comunic_w,
					nm_usuario_p,
					sysdate);
			end if;
			exception when others then
				ds_erro_w	:= ds_erro_w;
			end;
			commit;
	
		end if;
		ds_lista_usuario_destino_w := null;
	end if;
	
end loop;
close C01; 

END Enviar_CI_prescr_proced_job;
/