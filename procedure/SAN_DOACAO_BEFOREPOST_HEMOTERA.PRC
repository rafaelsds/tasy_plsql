create or replace
procedure san_doacao_beforepost_hemotera(
		ie_cadastro_doacao_p		varchar2,
		ie_modo_edicao_ou_novo_p	varchar2,
		ie_novo_registro_p		varchar2,
		ie_tipo_coleta_p		varchar2,
		ie_gravar_log_p			varchar2,
		cd_pessoa_fisica_p		varchar2,
		nr_seq_doacao_p			number,
		nr_seq_derivado_p		number,
		nr_sec_saude_p			varchar2,
		nr_sangue_p			varchar2,
		pr_hematocrito_p		number,
		qt_peso_p			number,
		cd_estabelecimento_p		number,
		nm_usuario_p			varchar2,
		qt_peso_out_p		out	number,
		nr_seq_doacao_amostra_p	out	number,
		ds_atributo_focus_p	out	varchar2,
		ds_msg_hemat_p		out	varchar2,
		ds_msg_peso_p		out	varchar2,
		ds_erro_p		out	varchar2) is 

ie_imped_sangue_dup_doacao_w	varchar2(1);
ie_duplic_sangue_w		varchar2(1);
ie_bloquear_pr_hematocrito_w	varchar2(1);
qt_peso_minimo_doacao_w		number(3);
ie_consistir_peso_paciente_w	varchar2(1);
ie_atualizar_campo_peso_w	varchar2(1);
ie_cons_se_houver_amostra_w	varchar2(1);
cd_pessoa_fisica_w		varchar2(10);
ds_inconsistencia_soro_w	varchar2(255);
ds_comando_w			Varchar2(2000);
ds_parametros_w			varchar2(2000);
ds_sep_bv_w			varchar2(10);
qt_san_producao_w		number(10);
ie_sus_w			varchar2(1);
ie_sexo_w			varchar2(1);
pr_hemat_min_fem_w		number(10,4);
pr_hemat_max_fem_w		number(10,4);
pr_hemat_min_mas_w		number(10,4);
pr_hemat_max_mas_w		number(10,4);
ie_gerar_sangue_w		varchar2(1);
nr_seq_doacao_w			number(10);
qt_dias_inaptidao_w		san_regra_sinal_vital.qt_dias_inapto%type;

begin

if	(ie_cadastro_doacao_p = 'S') then
	begin
	if	(ie_gravar_log_p = 'S') then
		begin
		gravar_log_tasy(99876,
			obter_texto_dic_objeto(84907, wheb_usuario_pck.get_nr_seq_idioma,
				'NR_SEQ_DOACAO=' || nr_seq_doacao_p || ';'
				|| 'CD_PESSOA_FISICA=' || cd_pessoa_fisica_p),
			nm_usuario_p);
		end;
	end if;

	ds_erro_p	:=  san_obter_consistencia_soro(cd_pessoa_fisica_p, nr_seq_doacao_p, ie_tipo_coleta_p);

	if	(ds_erro_p is null) and
		(ie_novo_registro_p = 'S') then
		begin
		/* Hemoterapia - Parametro [325] - Ao gerar uma nova doacao identificar se o doador possui uma nova amostra pendente */
		obter_param_usuario(450, 325, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_cons_se_houver_amostra_w);

		if	(ie_cons_se_houver_amostra_w = 'S') then
			begin
			select	nvl(max(nr_sequencia), 0)
			into	nr_seq_doacao_amostra_p
			from	san_doacao
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	ie_status		= 4;

			if	(nr_seq_doacao_amostra_p <> 0) then
				ds_erro_p	:= obter_texto_tasy(93526, wheb_usuario_pck.get_nr_seq_idioma);
			end if;
			end;
		end if;
		end;
	end if;

	if	(ds_erro_p is null) then
		begin
		/* Hemoterapia - Parametro [134] - Impedir o lancamento de sangues com o mesmo numero na pasta Doacao */
		obter_param_usuario(450, 134, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_imped_sangue_dup_doacao_w);
		obter_param_usuario(450, 252, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_sangue_w);
		if	(ie_imped_sangue_dup_doacao_w = 'S') and
			(ie_gerar_sangue_w = 'N')then
			begin
			ds_comando_w	:=
				' select	count(*) ' ||
				' from	san_producao ' ||
				' where	nr_sangue	= :nr_sangue_p ' ||
				' and	nr_seq_doacao <> :nr_sequencia_p ';
			ds_sep_bv_w	:= obter_separador_bv;
			ds_parametros_w	:= 'nr_sangue_p=' || nr_sangue_p || ds_sep_bv_w
					|| 'nr_sequencia_p=' || nr_seq_doacao_p || ds_sep_bv_w;

			/* Hemoterapia - Parametro [24] - Forma de tratar a duplicidade de hemocomponente */
			obter_param_usuario(450, 24, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_duplic_sangue_w);

			if	(ie_duplic_sangue_w = 'SH') then
				begin
				ds_comando_w	:= ds_comando_w || ' and nr_seq_derivado = :nr_seq_derivado_p ';
				ds_parametros_w	:= ds_parametros_w || 'nr_seq_derivado_p=' || nr_seq_derivado_p || ds_sep_bv_w;
				end;
			end if;

			qt_san_producao_w	:= obter_select_concatenado_bv(ds_comando_w, ds_parametros_w, ds_sep_bv_w);

			if	(qt_san_producao_w > 0) then
				ds_erro_p	:= obter_texto_tasy(75481, wheb_usuario_pck.get_nr_seq_idioma);
			else
				ds_comando_w	:=
					' select	count(*) ' ||
					' from		san_doacao ' ||
					' where		nr_sangue	= :nr_sangue_p ' ||
					' and		nr_sequencia	<> :nr_sequencia_p ';
				ds_sep_bv_w	:= obter_separador_bv;
				ds_parametros_w	:= 'nr_sangue_p=' || nr_sangue_p || ds_sep_bv_w
						|| 'nr_sequencia_p=' || nr_seq_doacao_p || ds_sep_bv_w;
						
				qt_san_producao_w	:= obter_select_concatenado_bv(ds_comando_w, ds_parametros_w, ds_sep_bv_w);
				
				if	(qt_san_producao_w > 0) then
					ds_erro_p	:= obter_texto_tasy(75481, wheb_usuario_pck.get_nr_seq_idioma);
				end if;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;

if	(ds_erro_p is null) then
	begin
	consistir_hematocritos_hemot(
		pr_hematocrito_p,
		cd_pessoa_fisica_p,
		cd_estabelecimento_p,
		nm_usuario_p,
		ds_atributo_focus_p,
		ds_msg_hemat_p,
		ds_erro_p,
		qt_dias_inaptidao_w);
	end;
end if;

if	(ds_erro_p is null) and
	(ie_modo_edicao_ou_novo_p = 'S') and
	(qt_peso_p > 0) then
	begin
	/* Hemoterapia - Parametro [145] - Peso minimo para a doacao */
	obter_param_usuario(450, 145, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, qt_peso_minimo_doacao_w);

	if	(qt_peso_minimo_doacao_w > 0) then
		begin
		if	(qt_peso_p < qt_peso_minimo_doacao_w) then
			begin
			/* Hemoterapia - Parametro [146] - Permite a doacao de paciente com peso inferior ao minimo */
			obter_param_usuario(450, 146, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consistir_peso_paciente_w);

			if	(ie_consistir_peso_paciente_w = 'N') then
				ds_erro_p		:= obter_texto_dic_objeto(75501, wheb_usuario_pck.get_nr_seq_idioma, 'PESO_MINIMO_DOACAO=' || qt_peso_minimo_doacao_w);
			elsif	(ie_consistir_peso_paciente_w = 'A') then
				ds_msg_peso_p	:= obter_texto_dic_objeto(75501, wheb_usuario_pck.get_nr_seq_idioma, 'PESO_MINIMO_DOACAO=' || qt_peso_minimo_doacao_w);
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;

if	(ds_erro_p is null) then
	begin
	if	(ie_cadastro_doacao_p = 'S') and
		(ie_novo_registro_p = 'S') then
		begin
		/* Hemoterapia - Parametro [186] - Atualizar o campo peso do paciente conforme a ultima doacao */
		obter_param_usuario(450, 186, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualizar_campo_peso_w);

		if	(ie_atualizar_campo_peso_w = 'S') then
			begin
			
			select	max(nr_sequencia)
			into	nr_seq_doacao_w
			from	san_doacao
			where	cd_pessoa_fisica = cd_pessoa_fisica_p
			and	nr_sequencia	<> nr_seq_doacao_p;
			
			if	(nvl(nr_seq_doacao_w,0) > 0) then
				
				select	nvl(qt_peso,0)
				into	qt_peso_out_p
				from	san_doacao
				where	nr_sequencia = nr_seq_doacao_w;
				
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;

end san_doacao_beforepost_hemotera;
/
