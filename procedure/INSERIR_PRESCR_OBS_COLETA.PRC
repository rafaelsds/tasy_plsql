create or replace
procedure inserir_prescr_obs_coleta(	nm_usuario_p		Varchar2,
				ds_observacao_coleta_p	varchar2,
				nr_prescricao_p		number,
				nr_sequencia_p		number) is 

begin

update	prescr_procedimento
set	ds_observacao_coleta = ds_observacao_coleta_p
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia = nr_sequencia_p;

insert into	prescr_proc_obs_coleta(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_prescricao,
			nr_seq_prescr,
			ds_observacao)
values   		(
			prescr_proc_obs_coleta_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_prescricao_p,
			nr_sequencia_p,
			ds_observacao_coleta_p);

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end inserir_prescr_obs_coleta;
/
