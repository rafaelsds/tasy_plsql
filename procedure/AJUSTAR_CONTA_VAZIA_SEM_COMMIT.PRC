create or replace procedure ajustar_conta_vazia_sem_commit(	nr_atendimento_p	Number,
						nm_usuario_p		varchar2) is
						
/* ATEN��O !!!!!!! N�O COLOCAR COMMIT DENTRO DESSA PROCEDURE.  UTILIZADA EM OBJETOS QUE N�O PODER�O COMMIT. ATEN��O 

!!!!!!!!!! */

ie_existe_registro_w		char(1);
nr_interno_conta_w			number(10,0);
ie_excluir_conta_vazia_w	varchar2(1):= 'S';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

Cursor C01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_atendimento = nr_atendimento_p
	and 	ie_status_acerto = 1
	order by nr_interno_conta;
			  
begin

begin
select	nvl(a.cd_estabelecimento, c.cd_estabelecimento)
into	cd_estabelecimento_w
from	conta_paciente a,
	atendimento_paciente c,
	convenio b
where	nvl(a.cd_convenio_calculo,a.cd_convenio_parametro) 	= b.cd_convenio
and	a.nr_atendimento	= nr_atendimento_p
and	a.nr_atendimento	= c.nr_atendimento;
exception
	when others then
	cd_estabelecimento_w := 0;
end;

ie_excluir_conta_vazia_w	:= nvl(obter_valor_param_usuario(67, 675, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'S');

if	(nvl(ie_excluir_conta_vazia_w,'S') = 'S') then

	open C01;
	loop
	fetch C01 into	
		nr_interno_conta_w;
	exit when C01%notfound;
		begin
		
		/* Deletar contas sem procedimentos e materiais */
		select 	nvl(max('S'),'N')
		into	ie_existe_registro_w
		from 	procedimento_paciente
		where	rownum = 1
		and		nr_interno_conta = nr_interno_conta_w;
	
		if	(ie_existe_registro_w = 'N') then
			select 	nvl(max('S'),'N')
			into	ie_existe_registro_w
			from 	material_atend_paciente
			where	rownum = 1
			and		nr_interno_conta		= nr_interno_conta_w;
			
			if	(ie_existe_registro_w = 'N') then
				begin
				delete from conta_paciente
				where nr_interno_conta = nr_interno_conta_w
				and ((Obter_Tipo_Convenio(cd_convenio_parametro) <> 3) or
			--'-Reinterna��o AIH 72 horas-'
					(obter_se_contido_char(ds_observacao,obter_desc_expressao(342126)) = 'N'));
				exception when others then
					null;
				end;
			end if;
		end if;
		
		end;
	end loop;
	close C01;
	
end if;

end ajustar_conta_vazia_sem_commit;
/