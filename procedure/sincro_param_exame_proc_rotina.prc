create or replace
procedure sincro_param_exame_proc_rotina (cd_estabelecimento_p 	estabelecimento.cd_estabelecimento%type,
										  cd_perfil_p			perfil.cd_perfil%type,
										  nm_usuario_p			usuario.nm_usuario%type)is 




Cursor C01 is
	select  a.nr_sequencia nr_seq_origem,
			a.cd_funcao cd_funcao_origem,
			b.nr_sequencia   nr_seq_destino,
			a.vl_parametro vl_parametro_origem
	from 	funcao_parametro a,
			funcao_parametro b
	where 	a.cd_funcao in (924,916)
	and 	b.cd_funcao = -397
	and 	a.ds_parametro = b.ds_parametro
	order by 3,1;

Cursor C02 is
	select  a.nr_sequencia 	 nr_seq_origem,
			a.cd_funcao cd_funcao_origem,
			b.nr_sequencia   nr_seq_destino,
			c.vl_parametro   vl_parametro_origem,
			c.cd_estabelecimento
	from 	funcao_parametro a,
			funcao_parametro b,
			funcao_param_estab c
	where 	a.cd_funcao in (924,916)
	and 	b.cd_funcao = -397
	and 	a.ds_parametro = b.ds_parametro
	and 	c.CD_FUNCAO = a.CD_FUNCAO
	and 	c.NR_SEQ_PARAM = a.NR_SEQUENCIA
	and		c.cd_estabelecimento = nvl(cd_estabelecimento_p,c.cd_estabelecimento)
	order by 3,1;
			
Cursor C03 is
	select  a.nr_sequencia nr_seq_origem,
			a.cd_funcao cd_funcao_origem,
			c.cd_perfil,
			b.nr_sequencia   nr_seq_destino,
			c.vl_parametro vl_parametro_origem,
			c.cd_estabelecimento
	from 	funcao_parametro a,
			funcao_parametro b,
			funcao_param_perfil c
	where a.cd_funcao in (924,916)
	and b.cd_funcao = -397
	and a.ds_parametro = b.ds_parametro
	and c.CD_FUNCAO = a.CD_FUNCAO
	and c.NR_SEQuencia = a.NR_SEQUENCIA
	and c.cd_perfil = nvl(cd_perfil_p, c.cd_perfil)
	order by 4,1;
			
Cursor C04 is
	select  a.nr_sequencia nr_seq_origem,
			a.cd_funcao cd_funcao_origem,
			c.nm_usuario_param,
			b.nr_sequencia   nr_seq_destino,
			c.vl_parametro vl_parametro_origem,
			c.cd_estabelecimento
	from    funcao_parametro a,
			funcao_parametro b,
			funcao_param_usuario c
	where 	a.cd_funcao in (924,916)
	and 	b.cd_funcao = -397
	and 	a.ds_parametro = b.ds_parametro
	and 	c.CD_FUNCAO = a.CD_FUNCAO
	and 	c.NR_SEQuencia = a.NR_SEQUENCIA
	and 	c.nm_usuario_param = nvl(nm_usuario_p, c.nm_usuario_param)
	order by 4,1;
	
	
cMainParam 		c01%Rowtype;
cEstabParam 	c02%Rowtype;
cPerfParam 		c03%Rowtype;
cUserParam 		c04%Rowtype;
ieNovoRegistro_w	varchar2(1) := 'S';

begin

open C01;
loop
fetch C01 into	
	cMainParam;
exit when C01%notfound;
	begin
	
	update funcao_parametro 
	set vl_parametro = cMainParam.vl_parametro_origem, 
		ds_observacao = obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cMainParam.cd_funcao_origem||' '||cMainParam.nr_seq_origem,
		nm_usuario = obter_desc_expressao_br(314318),
		dt_atualizacao = sysdate
	where cd_funcao = -397
	and  nr_sequencia = cMainParam.nr_seq_destino;
	
	commit;
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	cEstabParam;
exit when C02%notfound;
	begin
	
	select  decode(count(*),0,'S','N')
	into 	ieNovoRegistro_w
	from    funcao_param_estab
	where   cd_estabelecimento = cEstabParam.cd_estabelecimento
	and		cd_funcao = -397
	and     nr_seq_param = cEstabParam.nr_seq_destino;
	
	if (ieNovoRegistro_w = 'S') then
		insert into funcao_param_estab (nr_sequencia, nr_seq_param, ds_observacao, cd_estabelecimento, vl_parametro,nm_usuario,cd_funcao,dt_atualizacao)
		values (funcao_param_estab_seq.nextVal,
				cEstabParam.nr_seq_destino,
				obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cEstabParam.cd_funcao_origem||' '||cEstabParam.nr_seq_origem||' '||cEstabParam.cd_estabelecimento,
				cEstabParam.cd_estabelecimento,
				cEstabParam.vl_parametro_origem,
				obter_desc_expressao_br(314318),
				-397,
				sysdate
				);
		
	else
		update funcao_param_estab
		set vl_parametro = cEstabParam.vl_parametro_origem, 
			ds_observacao = obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cEstabParam.cd_funcao_origem||' '||cEstabParam.nr_seq_origem,
			nm_usuario = obter_desc_expressao_br(314318)  
		where cd_funcao = -397
		and   nr_sequencia = cEstabParam.nr_seq_destino
		and   cd_estabelecimento = cEstabParam.cd_estabelecimento;	
	
	end if;
	
	
	commit;
	
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	cPerfParam;
exit when C03%notfound;
	begin
	select  decode(count(*),0,'S','N')
	into 	ieNovoRegistro_w
	from    funcao_param_perfil
	where   cd_perfil = cPerfParam.cd_perfil
	and		cd_funcao = -397
	and     nr_sequencia = cPerfParam.nr_seq_destino;
	
	if (ieNovoRegistro_w = 'S') then
		insert into funcao_param_perfil (nr_seq_interno, nr_sequencia, ds_observacao, cd_perfil, vl_parametro,nm_usuario,cd_funcao,dt_atualizacao)
		values (funcao_param_perfil_seq.nextVal,
				cPerfParam.nr_seq_destino,
				obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cPerfParam.cd_funcao_origem||' '||cPerfParam.nr_seq_origem||' '||cPerfParam.cd_estabelecimento,
				cPerfParam.cd_perfil,
				cPerfParam.vl_parametro_origem,
				obter_desc_expressao_br(314318),
				-397,
				sysdate
				);
		
	else
		update funcao_param_perfil
		set vl_parametro = cPerfParam.vl_parametro_origem, 
			ds_observacao = obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cPerfParam.cd_funcao_origem||' '||cPerfParam.nr_seq_origem,
			nm_usuario = obter_desc_expressao_br(314318)  
		where cd_funcao = -397
		and   nr_sequencia = cPerfParam.nr_seq_destino
		and   cd_perfil = cPerfParam.cd_perfil;	
	
	end if;
	
	commit;
	
	end;
end loop;
close C03;

open C04;
loop
fetch C04 into	
	cUserParam;
exit when C04%notfound;
	begin
	
	select  decode(count(*),0,'S','N')
	into 	ieNovoRegistro_w
	from    funcao_param_usuario
	where   nm_usuario_param = cUserParam.nm_usuario_param
	and		cd_funcao = -397
	and     nr_sequencia = cUserParam.nr_seq_destino;
	
	
	if (ieNovoRegistro_w = 'S') then
		insert into funcao_param_usuario (nr_seq_interno, nr_sequencia, ds_observacao, nm_usuario_param, vl_parametro,nm_usuario,cd_funcao,dt_atualizacao)
		values (funcao_param_usuario_seq.nextVal,
				cUserParam.nr_seq_destino,
				obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cUserParam.cd_funcao_origem||' '||cUserParam.nr_seq_origem||' '||cUserParam.cd_estabelecimento,
				cUserParam.nm_usuario_param,
				cUserParam.vl_parametro_origem,
				obter_desc_expressao_br(314318),
				-397,
				sysdate
				);
		
	else
		update funcao_param_usuario
		set vl_parametro = cUserParam.vl_parametro_origem, 
			ds_observacao = obter_desc_expressao_br(929155)||' '||obter_desc_expressao_br(955603)||' '||cUserParam.cd_funcao_origem||' '||cUserParam.nr_seq_origem,
			nm_usuario = obter_desc_expressao_br(314318)  
		where cd_funcao = -397
		and   nr_sequencia = cUserParam.nr_seq_destino
		and   nm_usuario_param = cUserParam.nm_usuario_param;	
	
	end if;
	
	commit;
	
	end;
end loop;
close C04;			


commit;

end sincro_param_exame_proc_rotina;
/	