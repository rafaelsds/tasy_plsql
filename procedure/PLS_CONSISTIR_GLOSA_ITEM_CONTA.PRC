create or replace
procedure pls_consistir_glosa_item_conta
			(	nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nm_usuario_p			varchar2) is
				
ie_permite_glosar_w	varchar2(1)	:= 'N';

begin

if	( nr_seq_conta_p is not null ) then
	select		decode(count(1), 0, 'N', 'S')
	into		ie_permite_glosar_w
	from		pls_conta_glosa_ocorrencia_v
	where		nr_seq_conta	= nr_seq_conta_p
	order by	nr_sequencia;
elsif	( nr_seq_conta_proc_p is not null ) then
	select		decode(count(1), 0, 'N', 'S')
	into		ie_permite_glosar_w
	from		pls_conta_glosa_ocorrencia_v
	where		nr_seq_proc	= nr_seq_conta_proc_p
	order by	nr_sequencia;
elsif	( nr_seq_conta_mat_p is not null ) then
	select		decode(count(1), 0, 'N', 'S')
	into		ie_permite_glosar_w
	from		pls_conta_glosa_ocorrencia_v
	where		nr_seq_mat	= nr_seq_conta_mat_p
	order by	nr_sequencia;
end if;

if	(ie_permite_glosar_w = 'N') then
	Wheb_mensagem_pck.exibir_mensagem_abort(324951);
end if;

end pls_consistir_glosa_item_conta;
/