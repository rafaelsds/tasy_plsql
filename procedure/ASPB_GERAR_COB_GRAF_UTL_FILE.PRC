create or replace
procedure aspb_gerar_cob_graf_utl_file( 	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is 

dt_solicitacao_w	varchar2(4000);
dt_resposta_w		varchar2(4000);
ie_primeiro_loop_w	varchar2(4000);
ds_servico_w		varchar2(4000);
dt_servico_w		varchar2(4000);
nm_segurado_w		varchar2(4000);
nm_responsavel_w	varchar2(4000);
ds_conteudo_w		varchar2(150);
ds_bairro_w		varchar2(50);
nm_beneficiario_w	varchar2(26);
cd_ans_w		varchar2(7);
vl_mensalidade_w	number(15,2);	
vl_coparticipacao_w	number(15,2);
vl_opc_w		number(15,2);
nr_seq_segurado_w	number(15);
nr_seq_apres_w		number(10);
nr_titulo_w		number(10);
nr_protocolo_ans_w	number(10);
i 			number(2);
vl_servico_w		date;
dt_contratacao_w	date;

/*UTL File*/
arq_texto_w			utl_file.file_type;
ds_erro_w			varchar2(255);
ds_local_w			varchar2(255);
nm_arquivo_w			varchar2(255);
ds_mensagem_w			varchar2(255);
				
Cursor C01 is
	select	a.nr_titulo,
		rpad(nvl(substr(obter_endereco_cobr_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),1,50),' '),50,' '),
		rpad(nvl(g.cd_ans,' '),7,' '),
		f.nr_protocolo_ans,
		rpad(nvl(substr(obter_dados_titulo_receber(b.nr_titulo,'N'),1,25),' '),26,' '),
		sum(decode(i.ie_tipo_item,3,0,15,0,i.vl_item)) vl_mens,
		sum(decode(i.ie_tipo_item,3,i.vl_item,0))vl_copart,
		sum(decode(i.ie_tipo_item,15,i.vl_item,0)) vl_opc,
		nvl(e.dt_contratacao,sysdate),
		e.nr_sequencia
	from	pls_outorgante			g,
		pls_plano			f,
		pls_segurado 			e,
		pls_mensalidade_seg_item	i,
		pls_mensalidade_segurado	d,
		pls_mensalidade			c,
		titulo_receber			b,
		titulo_receber_cobr		a
	where	a.nr_seq_cobranca 		= nr_seq_cobr_escrit_p
	and	a.nr_titulo 			= b.nr_titulo
	and	b.nr_seq_mensalidade 		= c.nr_sequencia
	and	c.nr_sequencia			= d.nr_seq_mensalidade
	and	d.nr_sequencia			= i.nr_seq_mensalidade_seg
	and	d.nr_seq_segurado		= e.nr_sequencia
	and	e.nr_seq_plano			= f.nr_sequencia
	and	f.nr_seq_outorgante		= g.nr_sequencia
	group by 	a.nr_titulo,
			obter_endereco_cobr_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),
			g.cd_ans,
			f.nr_protocolo_ans,
			substr(obter_dados_titulo_receber(b.nr_titulo,'N'),1,25),
			e.dt_contratacao,
			e.nr_sequencia
	order by 	a.nr_titulo;
	
Cursor C02 is
	select	rpad(substr(obter_descricao_procedimento(f.cd_procedimento,f.ie_origem_proced),1,10),11,' ') ds_servico,
		rpad(f.dt_procedimento,9,' ') dt_servico,  
		rpad(pls_obter_dados_segurado(nr_seq_segurado_w,'N'),20,' ') nm_segurado,
		rpad(pls_obter_dados_prestador(e.nr_seq_prestador_exec,'N'),20,' ') nm_responsavel,
		lpad(campo_mascara_virgula(h.vl_coparticipacao),8,' ') vl_servico
	from  	pls_conta_coparticipacao  	h,
		pls_conta_proc      		f,
		pls_conta      			e,
		pls_mensalidade_seg_item  	a,
		pls_mensalidade_segurado  	b,
		pls_mensalidade      		c,
		titulo_receber      		d	
	where  	a.nr_seq_mensalidade_seg  	= b.nr_sequencia
	and  	b.nr_seq_mensalidade    	= c.nr_sequencia
	and  	d.nr_seq_mensalidade    	= c.nr_sequencia
	and  	a.nr_seq_conta      		= e.nr_sequencia
	and  	e.nr_sequencia      		= f.nr_seq_conta
	and  	h.nr_seq_conta_proc    		= f.nr_sequencia
	and  	d.nr_titulo      		= nr_titulo_w
	and  	a.nr_seq_conta is not null
	order by e.nr_sequencia;
	
begin
nm_arquivo_w	:= to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '.rem';

obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;	
	wheb_mensagem_pck.exibir_mensagem_abort(186768,'DS_ERRO_W=' || ds_erro_w);
end;

update	cobranca_escritural
set	ds_arquivo	= ds_local_w || nm_arquivo_w
where	nr_sequencia	= nr_seq_cobr_escrit_p;

open C01;
loop
fetch C01 into	
	nr_titulo_w,
	ds_bairro_w,
	cd_ans_w,
	nr_protocolo_ans_w,
	nm_beneficiario_w,
	vl_mensalidade_w,
	vl_coparticipacao_w,
	vl_opc_w,
	dt_contratacao_w,
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	i := 1;
	while (i <= 5) loop
		if	(i = 1) then
			ds_conteudo_w	:= 	lpad(nr_titulo_w,15,'0') || ds_bairro_w || 'Registro na ANS: ' || cd_ans_w || 'Produto na ANS: ' || nr_protocolo_ans_w;
		elsif	(i = 2) then
			ds_conteudo_w 	:= 	lpad(nr_titulo_w,15,'0') || ds_bairro_w || 'Usuario                    Inclusao     Vlr Taxa    Vlr Opc   Vlr Cota';
		elsif 	(i = 3) then
			ds_conteudo_w 	:= 	lpad(nr_titulo_w,15,'0') || ds_bairro_w || nm_beneficiario_w || to_char(dt_contratacao_w,'dd/mm/yyyy') ||
						lpad(vl_mensalidade_w,12,' ') || lpad(vl_opc_w,11,' ') || lpad(vl_coparticipacao_w,11,' ');
		elsif 	(i = 4) then
			select	to_char(max(b.dt_solicitacao),'dd/mm/yyyy'),
				to_char(nvl(max(b.dt_resposta),sysdate),'dd/mm/yyyy')
			into	dt_solicitacao_w,
				dt_resposta_w
			from	pls_portab_pessoa	b,
				pls_segurado		a
			where	b.nr_sequencia = a.nr_seq_portabilidade
			and	a.nr_sequencia = nr_seq_segurado_w;
			
			if	(dt_solicitacao_w is not null) and (dt_resposta_w is not null)then
			ds_conteudo_w	:= lpad(nr_titulo_w,15,'0') || ds_bairro_w || 'PERIODO PARA EXERCER A PORTABILIDADE ' || dt_solicitacao_w || ' A ' || dt_resposta_w;
			end if;
			
		elsif	(i = 5) then
			ie_primeiro_loop_w := 'S';
			open C02;
			loop
			fetch C02 into
				ds_servico_w,
				dt_servico_w,
				nm_segurado_w,
				nm_responsavel_w,
				vl_servico_w;
			exit when C02%notfound;
				begin				
				if	(ie_primeiro_loop_w = 'S') then
					ds_conteudo_w 		:= lpad(nr_titulo_w,15,'0') || ds_bairro_w || 'Usuario                    Inclusao     Vlr Taxa    Vlr Opc   Vlr Cota';
					ie_primeiro_loop_w 	:= 'N';
				end if;
				ds_conteudo_w	:= lpad(nr_titulo_w,15,'0') || ds_bairro_w  || ds_servico_w || dt_servico_w || nm_segurado_w || nm_responsavel_w || vl_servico_w;
				end;
			end loop;
			close c02;
		end if;
		
		nr_seq_apres_w := nr_seq_apres_w + 1;
		
		utl_file.put_line(arq_texto_w,ds_conteudo_w);
		utl_file.fflush(arq_texto_w);
		
		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_titulo)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w,
			0,
			nr_titulo_w);
		
		i := i + 1;
	end loop;
	end;
end loop;
close C01;

commit;

end aspb_gerar_cob_graf_utl_file;
/