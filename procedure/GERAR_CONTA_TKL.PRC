create or replace
procedure gerar_conta_tkl(NR_ACESSO_DICOM_P	VARCHAR2,
				 CD_MEDICO_EXEC_P	VARCHAR2) is 
			
			
nr_prescricao_w			prescr_medica.nr_prescricao%type;

nr_seq_prescricao_w		prescr_procedimento.nr_sequencia%type;
nr_seq_proc_interno_w	prescr_procedimento.nr_seq_proc_interno%type;
cd_procedimento_w		prescr_procedimento.cd_procedimento%type;
ie_origem_proced_w		prescr_procedimento.ie_origem_proced%type;
qt_procedimento_w		prescr_procedimento.qt_procedimento%type;
cd_setor_atendimento_w	prescr_procedimento.cd_setor_atendimento%type;
--cd_medico_exec_w		prescr_procedimento.cd_medico_exec%type;
ie_lado_w				prescr_procedimento.ie_lado%type;
dt_prev_execucao_w		prescr_procedimento.dt_prev_execucao%type;
nr_seq_propaci_w		procedimento_paciente.nr_sequencia%type;
cd_agenda_w				agenda_paciente.cd_agenda%type;
nr_seq_agenda_w			agenda_paciente.nr_sequencia%type;

begin


begin
	
select	a.nr_prescricao,
	a.nr_sequencia,
	a.nr_seq_proc_interno,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.qt_procedimento,
	a.cd_setor_atendimento,
	--a.cd_medico_exec,
	nvl(a.ie_lado,'A'),
	a.dt_prev_execucao,
	nvl(b.nr_seq_agenda, nvl(a.nr_seq_agenda,0))	
into	nr_prescricao_w,
	nr_seq_prescricao_w,
	nr_seq_proc_interno_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	cd_setor_atendimento_w,
	--cd_medico_exec_w,
	ie_lado_w,
	dt_prev_execucao_w,
	nr_seq_agenda_w
from	prescr_procedimento a,
		prescr_medica b
where	a.nr_acesso_dicom	= nr_acesso_dicom_p
and 	a.nr_prescricao = b.nr_prescricao
and	a.dt_atualizacao_nrec > (sysdate - 60);

exception
when no_data_found then
	begin
	
	select	a.nr_prescricao,
		a.nr_sequencia,
		a.nr_seq_proc_interno,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_procedimento,
		a.cd_setor_atendimento,
		--a.cd_medico_exec,
		nvl(a.ie_lado,'A'),
		a.dt_prev_execucao,
		nvl(b.nr_seq_agenda, nvl(a.nr_seq_agenda,0))
	into	nr_prescricao_w,
		nr_seq_prescricao_w,
		nr_seq_proc_interno_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		cd_setor_atendimento_w,
		--cd_medico_exec_w,
		ie_lado_w,
		dt_prev_execucao_w,
		nr_seq_agenda_w	
	from	prescr_procedimento a,
			prescr_medica b
	where	a.nr_acesso_dicom	= nr_acesso_dicom_p
	and		a.nr_prescricao = b.nr_prescricao;		

	exception
	when no_data_found then
		Wheb_mensagem_pck.exibir_mensagem_abort(192827,'NR_ACESSO_DICOM_P='|| nr_acesso_dicom_p);
	end;
end;


if	(nvl(nr_prescricao_w,0) > 0) and
	(nvl(nr_seq_prescricao_w,0) > 0) then

	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_propaci_w		
	from	procedimento_paciente
	where	nr_prescricao		= nr_prescricao_w
	and	nr_sequencia_prescricao	= nr_seq_prescricao_w;
	
	if	(nr_seq_propaci_w = 0) then
		begin
		
		Gerar_Proc_Pac_item_Prescr(	
						nr_prescricao_w, 
						nr_seq_prescricao_w, 
						null, 
						null,
						nr_seq_proc_interno_w,
						cd_procedimento_w, 
						ie_origem_proced_w,
						qt_procedimento_w, 
						cd_setor_atendimento_w,
						9, 
						dt_prev_execucao_w,
						'TKL', 
						cd_medico_exec_p, 
						null,
						ie_lado_w, 
						null);

			update	prescr_procedimento
			set		ie_status_execucao = 20
			where 	nr_prescricao = nr_prescricao_w
			and		nr_sequencia = nr_seq_prescricao_w
			and		nvl(ie_status_execucao,0) < 20;

		end;
	end if;
	
	
end if;

commit;

end gerar_conta_tkl;
/
