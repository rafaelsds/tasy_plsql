create or replace
procedure pep_gerar_protocolo(	cd_protocolo_p		number,
				nr_seq_medicacao_p	number,
				nr_atendimento_p		number,
				ie_prescricao_p		varchar2,
				ie_evolucao_p		varchar2,
				ie_receita_p		varchar2,
				ie_diagnostico_p		varchar2,
				ie_orientacao_alta_p	varchar2,
				ie_orientacao_geral_p	varchar2,
				ie_atestado_p		varchar2,
				ie_justificativa_p		varchar2,
				nm_usuario_p		varchar2,
				ie_atualizar_data_p		varchar2,
				ie_anamnese_p		varchar2					
				) is

nr_prescricao_w	number(15);
begin

Gerar_Protocolo_PA(cd_protocolo_p, nr_seq_medicacao_p, nr_atendimento_p, ie_prescricao_p, ie_evolucao_p, ie_receita_p, ie_diagnostico_p, ie_orientacao_alta_p,
				ie_orientacao_geral_p, ie_atestado_p, ie_justificativa_p, ie_anamnese_p,nm_usuario_p,nr_prescricao_w);

if	(nvl(ie_atualizar_data_p,'N') = 'S') then
	begin
	Atualizar_data_atend_medico(nr_atendimento_p);
	end;
end if;


end pep_gerar_protocolo;
/