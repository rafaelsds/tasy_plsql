create or replace
procedure Inserir_Regra_Alocacao(nr_seq_origem_p		number,
				cd_estabelecimento_p	number,
				cd_setor_atendimento_p	number,
				nm_usuario_p		Varchar2) is 

nr_sequencia_w				number(10);
ie_calculo_custo_w				varchar2(1);
pr_aplicar_w				number(9,4);
vl_custo_w				number(15,2);
cd_area_procedimento_w			number(15);
cd_especialidade_w			number(15);
cd_grupo_proc_w				number(15);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(10);
cd_convenio_w				number(8);
cd_proc_custo_w				number(15);
ie_origem_proc_custo_w			number(10);
cd_grupo_material_w			number(3);
cd_subgrupo_material_w			number(3);
cd_classe_material_w			number(5);
cd_material_w				number(6);
pr_custo_oper_w				number(9,4);
pr_medico_w				number(9,4);
pr_material_w				number(9,4);
ds_criterio_aloc_mat_w			varchar2(3);
ie_custo_hm_w				varchar2(1);
nr_seq_exame_w				number(10);
pr_imposto_w				number(9,4);
nr_seq_grupo_w				number(10);
nr_seq_subgrupo_w			number(10);
nr_seq_forma_org_w			number(10);
cd_edicao_amb_w				number(6);
ie_tipo_atendimento_w			number(3);
dt_inicio_vigencia_w			date;
dt_fim_vigencia_w			date;
ds_observacao_w				varchar2(255);
nr_seq_proc_interno_w			number(10);
begin

select	ie_calculo_custo,
		pr_aplicar,
		vl_custo,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		cd_procedimento,
		ie_origem_proced,
		cd_convenio,
		cd_proc_custo,
		ie_origem_proc_custo,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		pr_custo_oper,
		pr_medico,
		pr_material,
		ds_criterio_aloc_mat,
		ie_custo_hm,
		nr_seq_exame,
		pr_imposto,
		nr_seq_grupo,
		nr_seq_subgrupo,
		nr_seq_forma_org,
		cd_edicao_amb,
		ie_tipo_atendimento,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		ds_observacao,
		nr_seq_proc_interno
into	ie_calculo_custo_w,
		pr_aplicar_w,
		vl_custo_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_convenio_w,
		cd_proc_custo_w,
		ie_origem_proc_custo_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		cd_material_w,
		pr_custo_oper_w,
		pr_medico_w,
		pr_material_w,
		ds_criterio_aloc_mat_w,
		ie_custo_hm_w,
		nr_seq_exame_w,
		pr_imposto_w,
		nr_seq_grupo_w,
		nr_seq_subgrupo_w,
		nr_seq_forma_org_w,
		cd_edicao_amb_w,
		ie_tipo_atendimento_w,
		dt_inicio_vigencia_w,
		dt_fim_vigencia_w,
		ds_observacao_w,
		nr_seq_proc_interno_w
from	REGRA_ALOC_CUSTO_SETOR
where	nr_sequencia = nr_seq_origem_p;

select	regra_aloc_custo_setor_seq.nextval
into	nr_sequencia_w
from	dual;

insert into	REGRA_ALOC_CUSTO_SETOR (	nr_sequencia,
				cd_setor_atendimento,
				cd_estabelecimento,
				ie_calculo_custo,
				pr_aplicar,
				vl_custo,
				cd_area_procedimento,
				cd_especialidade,
				cd_grupo_proc,
				cd_procedimento,
				ie_origem_proced,
				cd_convenio,
				cd_proc_custo,
				ie_origem_proc_custo,
				cd_grupo_material,
				cd_subgrupo_material,
				cd_classe_material,
				cd_material,
				pr_custo_oper,
				pr_medico,
				pr_material,
				ds_criterio_aloc_mat,
				ie_custo_hm,
				nr_seq_exame,
				pr_imposto,
				nr_seq_grupo,
				nr_seq_subgrupo,
				nr_seq_forma_org,
				cd_edicao_amb,
				ie_tipo_atendimento,
				dt_atualizacao,
				nm_usuario,
				dt_inicio_vigencia,
				dt_fim_vigencia,
				ds_observacao,
				nr_seq_proc_interno)
		values (
				nr_sequencia_w,
				cd_setor_atendimento_p,
				cd_estabelecimento_p,
				ie_calculo_custo_w,
				pr_aplicar_w,
				vl_custo_w,
				cd_area_procedimento_w,
				cd_especialidade_w,
				cd_grupo_proc_w,
				cd_procedimento_w,
				ie_origem_proced_w,
				cd_convenio_w,
				cd_proc_custo_w,
				ie_origem_proc_custo_w,
				cd_grupo_material_w,
				cd_subgrupo_material_w,
				cd_classe_material_w,
				cd_material_w,
				pr_custo_oper_w,
				pr_medico_w,
				pr_material_w,
				ds_criterio_aloc_mat_w,
				ie_custo_hm_w,
				nr_seq_exame_w,
				pr_imposto_w,
				nr_seq_grupo_w,
				nr_seq_subgrupo_w,
				nr_seq_forma_org_w,
				cd_edicao_amb_w,
				ie_tipo_atendimento_w,
				sysdate,
				nm_usuario_p,
				dt_inicio_vigencia_w,
				dt_fim_vigencia_w,
				ds_observacao_w,
				nr_seq_proc_interno_w);	
		
commit;

end Inserir_Regra_Alocacao;
/
