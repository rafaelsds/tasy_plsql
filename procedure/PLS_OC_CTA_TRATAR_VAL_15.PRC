create or replace
procedure pls_oc_cta_tratar_val_15(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de coparticipa��o das ocorr�ncias combinadas de conta m�dica
Ex: Coparticipa��o n�o gerada, Coparticipa��o gerada zerada..
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:		
-------------------------------------------------------------------------------------------------------------------
jjung OS 602020 - Cria��o da procedure.
-------------------------------------------------------------------------------------------------------------------
jjung 29/06/2013 

Altera��o:	Adicionado parametro nos m�todos de atualiza��o dos campos IE_VALIDO e IE_VALIDO_TEMP 
	da PLS_TIPOS_OCOR_PCK
	
Motivo:	Se tornou necess�rio diferenciar os filtros das valida��es na hora de realizar esta opera��o
	para que os filtros de exce��o funcionem corretamente.
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_filtro_w		pls_tipos_ocor_pck.dados_filtro;

ds_select_w		varchar2(4000);
ds_restricao_proc_w	varchar2(1000);
ds_restricao_mat_w	varchar2(1000);

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

nr_seq_conta_w		pls_conta.nr_sequencia%type;
nr_seq_item_w		number(10);
ie_tipo_item_w		pls_oc_cta_selecao_ocor_v.ie_tipo_registro%type;
ie_registro_valido_w	varchar2(1);
v_cur			pls_util_pck.t_cursor;
nr_seq_selecao_w	dbms_sql.number_table;
ds_observacao_w		dbms_sql.varchar2_table;
ie_valido_w		dbms_sql.varchar2_table;


-- Informa��es da valida��o de coparticipacao
cursor C02 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
select	a.nr_sequencia	nr_seq_validacao,
	a.ie_coparticipacao
from	pls_oc_cta_val_copartic a
where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

begin
-- Deve haver informa��o da regra para que a valida��o seja aplicada
if	(dados_regra_p.nr_sequencia is not null) then
	ie_registro_valido_w := 'S';
	
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
	
	-- Varrer as informa��es parametrizadas para esta valida��o. No caso desta valida��o ser� apenas uma linha.
	for	r_C02_w in C02 (dados_regra_p.nr_sequencia) loop
		
		--Verificar o tipo de situa��o conforme a parametriza��o do usu�rio.
		-- Se for para verificar itens com coparticipa��o zerada
		if	(r_C02_w.ie_coparticipacao = 'CZ') then
			
			-- Deve ser retornado no select os itens quais foi gerado registro de coparticipa��o por�m o valor � zerado ou n�o informado
			ds_restricao_proc_w :=	'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 ' || pls_tipos_ocor_pck.enter_w ||
						'		from	pls_conta_coparticipacao cop ' || pls_tipos_ocor_pck.enter_w ||
						'		where	cop.nr_seq_conta = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'		and	cop.nr_seq_conta_proc = sel.nr_seq_conta_proc ' || pls_tipos_ocor_pck.enter_w ||
						'		and	(cop.vl_coparticipacao <= 0 or vl_coparticipacao is null)' || pls_tipos_ocor_pck.enter_w ||
						'	) ';
			
			ds_restricao_mat_w :=	'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 ' || pls_tipos_ocor_pck.enter_w ||
						'		from	pls_conta_coparticipacao cop ' || pls_tipos_ocor_pck.enter_w ||
						'		where	cop.nr_seq_conta = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'		and	cop.nr_seq_conta_mat = sel.nr_seq_conta_mat ' || pls_tipos_ocor_pck.enter_w ||
						'		and	(cop.vl_coparticipacao <= 0 or vl_coparticipacao is null) ' || pls_tipos_ocor_pck.enter_w ||
						'	) ';
		-- Se for para verificar itens sem regra de coparticipacao
		elsif	(r_C02_w.ie_coparticipacao = 'SR') then
		
			-- Deve ser retornado no select os itens quais deveria ter sido gerada a coparticipa��o por�m n�o existe registro de coparticipacao gerado.
			ds_restricao_proc_w :=	'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 ' || pls_tipos_ocor_pck.enter_w ||
						'		from	pls_conta_proc_ocor_v proc ' || pls_tipos_ocor_pck.enter_w ||
						'		where	proc.nr_seq_conta = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'		and	proc.nr_sequencia = sel.nr_seq_conta_proc ' || pls_tipos_ocor_pck.enter_w ||
						'		and	proc.ie_coparticipacao = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
						'		and	not exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'					select	1 ' || pls_tipos_ocor_pck.enter_w ||
						'					from	pls_conta_coparticipacao cop ' || pls_tipos_ocor_pck.enter_w ||
						'					where	cop.nr_seq_conta = proc.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'					and	cop.nr_seq_conta_proc = proc.nr_sequencia ' || pls_tipos_ocor_pck.enter_w ||
						'				) ' || pls_tipos_ocor_pck.enter_w ||
						'	) ';
						
			ds_restricao_mat_w :=	'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 ' || pls_tipos_ocor_pck.enter_w ||
						'		from	pls_conta_mat_ocor_v mat ' || pls_tipos_ocor_pck.enter_w ||
						'		where	mat.nr_seq_conta = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'		and	mat.nr_sequencia = sel.nr_seq_conta_mat ' || pls_tipos_ocor_pck.enter_w ||
						'		and	mat.ie_coparticipacao = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
						'		and	not exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'					select	1 ' || pls_tipos_ocor_pck.enter_w ||
						'					from	pls_conta_coparticipacao cop ' || pls_tipos_ocor_pck.enter_w ||
						'					where	cop.nr_seq_conta = mat.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'					and	cop.nr_seq_conta_mat = mat.nr_sequencia ' || pls_tipos_ocor_pck.enter_w ||
						'				) ' || pls_tipos_ocor_pck.enter_w ||
						'	) ';
		end if;
		
		ds_select_w	:=	'select	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w ||
					'	'''||ie_registro_valido_w||''' ie_registro_valido, ' ||pls_tipos_ocor_pck.enter_w||
					' 	null ds_obervacao ' ||pls_tipos_ocor_pck.enter_w||
					'from	pls_oc_cta_selecao_ocor_v sel ' || pls_tipos_ocor_pck.enter_w ||
					'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_tipo_registro = ''P'' ' || pls_tipos_ocor_pck.enter_w ||
					ds_restricao_proc_w || pls_tipos_ocor_pck.enter_w ||
					'union all ' || pls_tipos_ocor_pck.enter_w ||
					'select	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w ||
					'	'''||ie_registro_valido_w||''' ie_registro_valido, ' ||pls_tipos_ocor_pck.enter_w||
					' 	null ds_obervacao ' ||pls_tipos_ocor_pck.enter_w||
					'from	pls_oc_cta_selecao_ocor_v sel ' || pls_tipos_ocor_pck.enter_w ||
					'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_tipo_registro = ''M'' ' || pls_tipos_ocor_pck.enter_w ||
					ds_restricao_mat_w;
		begin	
			open v_cur for ds_select_w using 	nr_id_transacao_p, nr_id_transacao_p;
			loop
				nr_seq_selecao_w	:= pls_util_cta_pck.num_table_vazia_w;
				ie_valido_w		:= pls_util_cta_pck.vchr2_table_vazia_w;	
				ds_observacao_w		:= pls_util_cta_pck.vchr2_table_vazia_w;
				fetch v_cur bulk collect
				into  nr_seq_selecao_w, ie_valido_w, ds_observacao_w
				limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when nr_seq_selecao_w.count = 0;	
					
					pls_tipos_ocor_pck.gerencia_selecao_validacao( nr_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 'SEQ', ds_observacao_w, 
										ie_valido_w, nm_usuario_p); 					
			end loop;
			close v_cur;
		exception
			when others then
				--Fecha cursor
				close v_cur;
				-- Insere o log na tabela e aborta a opera��o
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p ,ds_select_w , nr_id_transacao_p, nm_usuario_p);
		end;	
		
	end loop; -- C02
	
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;

end pls_oc_cta_tratar_val_15;
/