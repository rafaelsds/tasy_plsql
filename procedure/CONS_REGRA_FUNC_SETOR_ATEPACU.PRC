create or replace
procedure cons_regra_func_setor_atepacu	(cd_setor_atendimento_p	number,
					nr_atendimento_p		number) is

ie_setor_regra_w		varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
ie_atepacu_ok_w		varchar2(1);
					
begin
if	(cd_setor_atendimento_p is not null) and
	(nr_atendimento_p is not null) then
		
		/* obter dados atendimento */
		select	a.cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	pessoa_fisica b,
			atendimento_paciente a
		where	b.cd_pessoa_fisica = a.cd_pessoa_fisica
		and	a.nr_atendimento = nr_atendimento_p;
		
		if	(cd_pessoa_fisica_w is not null) then

		/* consistir regra funcionamento setor */
		
		select	consistir_idade_comp_setor(cd_pessoa_fisica_w,cd_setor_atendimento_p) 
		into	ie_atepacu_ok_w	
		from	dual; 
		
				 
			if	(ie_atepacu_ok_w = 'N') then
				Wheb_mensagem_pck.exibir_mensagem_abort(262504);
			end if;
				 
		end if;
	
end if;

end cons_regra_func_setor_atepacu;
/
						