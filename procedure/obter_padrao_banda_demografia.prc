create or replace procedure obter_padrao_banda_demografia(
nr_seq_relatorio_p number,
nr_tipo_banda_p number,
nm_usuario_p varchar2,
cd_relatorio_p number)
is

contador_w number :=0;
codigo_expressao_w number;
expressao_w varchar(40);
qt_tamanho_banda_w number;
qt_altura_banda_w number;
qt_registros number;

begin

/*
1 - Patient
2 - Date of birth
3 - Sex
4 - Entry date
5 - Medical record
6 - Encounter
7 - Doctor
8 - Insurance
9 - Period
10 - Patient Diagnosis
*/

SELECT COUNT(rd.nr_sequencia)
  INTO qt_registros
  FROM relatorio_dinamico rd
 WHERE rd.nr_seq_relatorio = nr_seq_relatorio_p
   AND rd.nr_tipo_banda = nr_tipo_banda_p;
   
   IF qt_registros = 0 THEN

select max(qt_tamanho_banda),
max(qt_altura_banda)
into qt_tamanho_banda_w,
qt_altura_banda_w
from relatorio_dinamico
where nr_seq_relatorio = nr_seq_relatorio_p
and nr_tipo_banda = nr_tipo_banda_p
and cd_relatorio = cd_relatorio_p;

loop
contador_w := contador_w + 1;
if contador_w > 11 then
  exit;
end if;

	select decode
	(contador_w,1,931087,2,295156,3,759222,4,863406,5,286727,6,296549,7,283863,8,720297,9,286193,10,295503,11,325012)
	into codigo_expressao_w
	from dual;
	
	select substr(obter_desc_expressao(codigo_expressao_w),0,36) 
	into expressao_w
	from dual;
	
   IF (contador_w > 1) THEN
    
    insert into relatorio_dinamico(
    nr_sequencia,          -- 1
    dt_atualizacao,        -- 2
    nm_usuario,            -- 3
    dt_atualizacao_nrec,   -- 4     
    nm_usuario_nrec,       -- 5
    cd_relatorio,          -- 6
    cd_tipo,               -- 7
    ds_label,              -- 8
		nr_eixo_y,             -- 9
		nr_eixo_x,             -- 10
		nr_altura,             -- 11
		nr_comprimento,        -- 12
		qt_rotacao,            -- 13
		nm_label,              -- 14
		ie_ajustar_tamanho,    -- 15
		nr_seq_apres,          -- 16
		nr_tipo_banda,         -- 17
		nr_seq_relatorio,      -- 18
		qt_tamanho_banda,      -- 19
		qt_altura_banda,       -- 20
    ds_cor_fundo_descricao,-- 21
    ds_cor_fundo_label,    -- 22
    ie_alinhamento         -- 23
    )
	values(
    relatorio_dinamico_seq.nextval, -- 1
		sysdate,                        -- 2
		nm_usuario_p,                   -- 3
		sysdate,                        -- 4
		nm_usuario_p,                   -- 5
		cd_relatorio_p,                 -- 6
		null,                        -- 7
		expressao_w || ': ',                    -- 8
		'1',                            -- 9
		'1',                            -- 10
		'20',                           -- 11
		CASE WHEN contador_w = 1 THEN '700' ELSE '40' END,                           -- 12
		'0',                            -- 13
		expressao_w || ': ',                    -- 14
		'S',                            -- 15
		contador_w,                     -- 16
		'3', -- 17
		nr_seq_relatorio_p,             -- 18
		nvl(qt_tamanho_banda_w,'200'),  -- 19
		nvl(qt_altura_banda_w,'500'),   -- 20
    '#bebebe', -- 21
    'clWhite', -- 22
    'D' -- 23
    );
    
    END IF;

  insert into relatorio_dinamico(
    nr_sequencia,          -- 1
		dt_atualizacao,        -- 2
		nm_usuario,            -- 3
		dt_atualizacao_nrec,   -- 4     
		nm_usuario_nrec,       -- 5
		cd_relatorio,          -- 6
		cd_tipo,               -- 7
		ds_label,              -- 8
		nr_eixo_y,             -- 9
		nr_eixo_x,             -- 10
		nr_altura,             -- 11
		nr_comprimento,        -- 12
		qt_rotacao,            -- 13
		nm_label,              -- 14
		ie_ajustar_tamanho,    -- 15
		nr_seq_apres,          -- 16
		nr_tipo_banda,         -- 17
		nr_seq_relatorio,      -- 18
		qt_tamanho_banda,      -- 19
		qt_altura_banda,       -- 20
    ds_cor_fundo_descricao,-- 21
    ds_cor_fundo_label,    -- 22
    ie_alinhamento,        -- 23
    nr_tam_fonte           -- 24
    )
	values(
    relatorio_dinamico_seq.nextval, -- 1
		sysdate,                        -- 2
		nm_usuario_p,                   -- 3
		sysdate,                        -- 4
		nm_usuario_p,                   -- 5
		cd_relatorio_p,                 -- 6
		DECODE(codigo_expressao_w, 
           295156, 'NP', -- NM_PACIENTE
           759222, 'DN', -- DT_NASCIMENTO
           863406, 'DS', -- DS_SEXO
           286727, 'DE', -- DT_ENTRADA
           296549, 'MR', -- CD_MED_RECORD
           283863, 'NA', -- NR_ATENDIMENTO
           720297, 'NM', -- NM_MEDICO_RESP
           286193, 'DI', -- DS_INSURANCE
           295503, 'DP', -- DS_PERIODO
           325012, 'DD'  -- DS_DIAG_PRINCIPAL
          ),                        -- 7
		expressao_w,                    -- 8
		'1',                            -- 9
		'1',                            -- 10
		'20',                           -- 11
		CASE WHEN contador_w = 1 THEN '1000' ELSE '40' END,                           -- 12
		'0',                            -- 13
		expressao_w,                    -- 14
		'S',                            -- 15
		contador_w,                     -- 16
		'3', -- 17
		nr_seq_relatorio_p,             -- 18
		nvl(qt_tamanho_banda_w,'200'),  -- 19
		nvl(qt_altura_banda_w,'500'),   -- 20
    CASE WHEN contador_w = 1 THEN '#2596be' ELSE 'clWhite' END, -- 21
    CASE WHEN contador_w = 1 THEN '#2596be' ELSE 'clWhite' END, -- 22
    CASE WHEN contador_w = 1 THEN 'C' ELSE 'E' END,         -- 23
    CASE WHEN contador_w = 1 THEN 12 ELSE 8 END   -- 24
    );

end loop;

	ajustar_posicao_campos(nr_seq_relatorio_p, nr_tipo_banda_p);
  
  DELETE
    FROM PARAMETRO_RELAT_DINAMICO prd
   WHERE prd.nr_seq_relatorio = nr_seq_relatorio_p
     AND prd.Nm_Atributo = 'NR_ATEND';
  COMMIT;
  
  INSERT INTO PARAMETRO_RELAT_DINAMICO (
    NR_SEQUENCIA,              -- 1
    DT_ATUALIZACAO,            -- 2
    NM_USUARIO,                -- 3
    DT_ATUALIZACAO_NREC,       -- 4
    NM_USUARIO_NREC,           -- 5
    CD_RELATORIO,              -- 6
    NR_SEQ_RELATORIO,          -- 7
    NM_ATRIBUTO,               -- 8
    IE_TIPO_COMPARACAO,        -- 9
    IE_TIPO_ATRIBUTO,          -- 10
    DS_MASCARA,                -- 11
    IE_FORMATO_CAMPO,          -- 12
    NR_TAMANHO_OBRIGATORIO,    -- 13
    NR_LINHA,                  -- 14
    DS_VALOR_PADRAO,           -- 15
    DS_PARAMETRO,              -- 16
    NR_SEQ_RELATORIO_PARAMETRO -- 17
  )
  VALUES
  (
    PARAMETRO_RELAT_DINAMICO_SEQ.NEXTVAL, -- 1
    SYSDATE,                              -- 2
    nm_usuario_p,                         -- 3
    SYSDATE,                              -- 4
    nm_usuario_p,                         -- 5
    cd_relatorio_p,                       -- 6
    nr_seq_relatorio_p,                   -- 7
    'NR_ATEND',                           -- 8
    '=',                                  -- 9
    'NUMBER',                             -- 10
    NULL,                                 -- 11
    'S',                                  -- 12
    200,                                  -- 13
    1,                                    -- 14
    NULL,                                 -- 15
    'PATIENT ID',                         -- 16
    NULL                                  -- 17
  );
  COMMIT;

  END IF;

end obter_padrao_banda_demografia;
/
