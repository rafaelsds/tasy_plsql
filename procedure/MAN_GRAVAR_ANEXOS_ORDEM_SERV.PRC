create or replace
procedure man_gravar_anexos_ordem_serv (
		nr_seq_faq_p		number,
		nr_ordem_servico_p	number,
		nm_usuario_p		varchar2) is

ds_arquivo_w 	varchar2(255);
nm_arquivo_w	varchar2(255);
cursor	c01 is
	select	ds_arquivo
	from	com_faq_anexo
	where	nr_seq_faq = nr_seq_faq_p;

begin
if	(nr_seq_faq_p is not null) and
	(nr_ordem_servico_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
		fetch C01 into	
			ds_arquivo_w;
		exit when c01%notfound;
		begin
		nm_arquivo_w := '\\whebs05\ordem_servico\OS'|| nr_ordem_servico_p || '_';
		nm_arquivo_w := nm_arquivo_w || to_char(sysdate, 'dd') || '_' || to_char(sysdate, 'MM') || '_' || ds_arquivo_w;
		
		man_gravar_anexo_ordem(nr_ordem_servico_p,nm_arquivo_w,nm_usuario_p);
		
		end;
	end loop;
	close c01;
	end;
end if;
commit;
end man_gravar_anexos_ordem_serv;
/