create or replace procedure reg_include_test_plan_ignored(nr_seq_integrated_test_p number,
                                                          ie_tipo_operacao_p       varchar2,
                                                          ds_version_p             varchar2,
                                                          nm_usuario_p             varchar2) is
begin
  if (nr_seq_integrated_test_p is not null) then
  
    insert into w_test_plan_ignored_int
      (nm_usuario,
       nm_usuario_nrec,
       nr_seq_integrated_test,
       ie_tipo_operacao,
       dt_atualizacao_nrec,
       dt_atualizacao,
       ds_version,
       nr_sequencia)
    values
      (nm_usuario_p,
       nm_usuario_p,
       nr_seq_integrated_test_p,
       ie_tipo_operacao_p,
       sysdate,
       sysdate,
       ds_version_p,
       w_test_plan_ignored_int_seq.nextval);
  
  end if;

end reg_include_test_plan_ignored;
/
