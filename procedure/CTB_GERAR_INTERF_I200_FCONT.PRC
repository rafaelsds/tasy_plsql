create or replace
procedure ctb_gerar_interf_I200_fcont
			(	nr_seq_controle_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				dt_inicio_p			date,
				dt_fim_p			date,
				cd_empresa_p			number,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 


cd_centro_custo_w		varchar2(8);
cd_classificacao_w		varchar2(40);
cd_conta_contabil_w		varchar2(40);
cd_historico_w			number(10);
ds_historico_w			varchar2(255);
dt_movimento_w			date;
dt_referencia_w			date;
ie_apres_conta_ctb_w		varchar2(15);
ie_debito_credito_w		varchar2(1);
ie_tipo_lancamento_w		varchar2(2);
ie_tipo_lancamento_ww		varchar2(2);
nr_documento_w			varchar2(20);
nr_sequencia_w			number(10);
qt_commit_w			number(10)	:= 0;
vl_movimento_w			number(15,2);

nr_linha_w			number(10)	:= qt_linha_p;
nr_seq_registro_w		number(10)	:= nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= '|';
tp_registro_w			varchar2(15);
ie_consolida_empresa_w		ctb_regra_sped.ie_consolida_empresa%type;

cursor C01 is
	select	a.nr_sequencia,
		a.dt_movimento,
		a.vl_movimento,
		a.ie_tipo_lancamento,
		a.dt_referencia
	from	ecd_movimento_contabil_v a
	where	a.tp_registro		= 1
	and	a.cd_empresa		= cd_empresa_p
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	ie_consolida_empresa_w	= 'N'
	and	a.dt_referencia	between dt_inicio_p and dt_fim_p
	union
	select	a.nr_sequencia,
		a.dt_movimento,
		a.vl_movimento,
		a.ie_tipo_lancamento,
		a.dt_referencia
	from	ecd_movimento_contabil_v a,
		estabelecimento b
	where	a.tp_registro		= 1
	and	a.cd_empresa		= cd_empresa_p
	and	a.cd_estabelecimento	= b.cd_estabelecimento
	and	nvl(ie_gerar_sped,'S')	=	'S'
	and	ie_consolida_empresa_w	= 'S'
	and	a.dt_referencia	between dt_inicio_p and dt_fim_p;	
	
cursor C02 is
	select	a.cd_conta_contabil,
		a.cd_classificacao,
		a.cd_centro_custo,
		a.vl_movimento,
		a.ie_debito_credito,
		a.nr_documento,
		a.cd_historico,
		a.ds_historico
	from	ecd_movimento_contabil_v a
	where	a.tp_registro		= 2
	and	a.cd_empresa		= cd_empresa_p
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	ie_consolida_empresa_w	= 'N'
	and	a.dt_referencia		= dt_referencia_w
	and	a.dt_movimento		= dt_movimento_w
	and	a.ie_tipo_lancamento	= ie_tipo_lancamento_ww
	union
	select	a.cd_conta_contabil,
		a.cd_classificacao,
		a.cd_centro_custo,
		a.vl_movimento,
		a.ie_debito_credito,
		a.nr_documento,
		a.cd_historico,
		a.ds_historico
	from	ecd_movimento_contabil_v a,
		estabelecimento b
	where	b.cd_estabelecimento	= a.cd_estabelecimento
	and	b.cd_empresa		= a.cd_empresa
	and	a.tp_registro		= 2
	and	b.cd_empresa		= cd_empresa_p
	and	nvl(b.ie_gerar_sped,'S') = 'S'
	and	ie_consolida_empresa_w	= 'S'
	and	a.dt_referencia		= dt_referencia_w
	and	a.dt_movimento		= dt_movimento_w
	and	a.ie_tipo_lancamento	= ie_tipo_lancamento_ww;

begin
select	nvl(max(a.ie_apres_conta_ctb), 'CD'),
	nvl(max(a.ie_consolida_empresa), 'N')
into	ie_apres_conta_ctb_w,
	ie_consolida_empresa_w
from	ctb_regra_sped		a,
	ctb_sped_controle	b
where	b.nr_seq_regra_sped 	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_controle_p;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	dt_movimento_w,
	vl_movimento_w,
	ie_tipo_lancamento_w,
	dt_referencia_w;
exit when C01%notfound;
	begin
	ie_tipo_lancamento_ww	:= ie_tipo_lancamento_w;
	
	if	(ie_tipo_lancamento_w = 'N') then
		ie_tipo_lancamento_w	:= 'X';
	elsif	(ie_tipo_lancamento_w = 'E') then
		ie_tipo_lancamento_w	:= 'EF';
	end if;
	
	tp_registro_w		:= 'I200';
	ds_linha_w		:= substr(	sep_w || tp_registro_w				||
					sep_w || nr_sequencia_w 				|| 
					sep_w || to_char(dt_movimento_w,'ddmmyyyy') 		|| 
					sep_w || sped_obter_campo_valor(vl_movimento_w)	|| 
					sep_w || ie_tipo_lancamento_w 			|| sep_w,1,8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;		
	qt_commit_w		:= qt_commit_w + 1;
	
	insert into ctb_sped_registro
		(nr_sequencia,                   
		ds_arquivo,                     
		dt_atualizacao,                 
		nm_usuario,                     
		dt_atualizacao_nrec,            
		nm_usuario_nrec,                
		nr_seq_controle_sped,           
		ds_arquivo_compl,               
		cd_registro,
		nr_linha,
		nr_doc_origem)
	values	(nr_seq_registro_w,
		ds_arquivo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		ds_compl_arquivo_w,
		tp_registro_w,
		nr_linha_w,
		nr_sequencia_w);
	
	if	(qt_commit_w >= 1000) then
		commit;
		qt_commit_w	:= 0;
	end if;
	
	ie_tipo_lancamento_w	:= 'N';
	
	open C02;
	loop
	fetch C02 into	
		cd_conta_contabil_w,
		cd_classificacao_w,
		cd_centro_custo_w,
		vl_movimento_w,
		ie_debito_credito_w,
		nr_documento_w,
		cd_historico_w,
		ds_historico_w;	
	exit when C02%notfound;
		begin
		tp_registro_w	:= 'I250';
		ds_historico_w	:= replace(ds_historico_w,chr(9),'');
		
		if	(ie_apres_conta_ctb_w = 'CL') then
			cd_conta_contabil_w	:= cd_classificacao_w;
		elsif	(ie_apres_conta_ctb_w = 'CP') then
			cd_conta_contabil_w	:= substr(replace(cd_classificacao_w,'.',''),1,40);
		end if;
		
		
		ds_linha_w	:= substr(	sep_w || tp_registro_w				||
						sep_w || cd_conta_contabil_w 			|| 
						sep_w || cd_centro_custo_w 				|| 
						sep_w || sped_obter_campo_valor(vl_movimento_w)	|| 
						sep_w || ie_debito_credito_w 				|| 
						sep_w || nr_documento_w 				|| 
						sep_w || cd_historico_w 				|| 
						sep_w || ds_historico_w 				|| 
						sep_w || ''						|| sep_w, 1,8000);
	
		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;
		qt_commit_w		:= qt_commit_w + 1;
		
		insert into ctb_sped_registro
			(nr_sequencia,                   
			ds_arquivo,                     
			dt_atualizacao,                 
			nm_usuario,                     
			dt_atualizacao_nrec,            
			nm_usuario_nrec,                
			nr_seq_controle_sped,           
			ds_arquivo_compl,               
			cd_registro,
			nr_linha)
		values	(nr_seq_registro_w,
			ds_arquivo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_controle_p,
			ds_compl_arquivo_w,
			tp_registro_w,
			nr_linha_w);
	
		if	(qt_commit_w >= 1000) then
			commit;
			qt_commit_w	:= 0;
		end if;
		
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end ctb_gerar_interf_I200_fcont;
/