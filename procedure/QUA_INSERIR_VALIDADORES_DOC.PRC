 create or replace
procedure Qua_Inserir_Validadores_Doc(
			is_doc_validacao	Varchar2,
			nm_usuario_p		Varchar2,
			nr_seq_rev_p			number,
			nr_seq_doc_p		number) is 

cd_pessoa_vali_w number;
			
cursor c01 is
select  distinct cd_participante
from    qua_doc_participante
where 	nr_seq_doc = nr_seq_doc_p;


begin
if('S' != is_doc_validacao)then

open c01;
loop
fetch c01 into	cd_pessoa_vali_w;
exit when c01%notfound;
begin
insert into qua_doc_validacao(
                    nr_sequencia,
                    nr_seq_doc,
                    dt_atualizacao,
                    nm_usuario,
					cd_pessoa_validacao)
	select qua_doc_validacao_seq.nextval,
					nr_seq_doc_p,                                				
                    sysdate,
                    nm_usuario_p,
					cd_pessoa_vali_w
					from dual where not exists (select 1 from qua_doc_validacao
	where 			nr_seq_doc = nr_seq_doc_p
	and				cd_pessoa_validacao = cd_pessoa_vali_w);
end;
end loop;
close c01;
end if;

if('S' = is_doc_validacao)then
open c01;
loop
fetch c01 into	cd_pessoa_vali_w;
exit when c01%notfound;
begin

insert into qua_doc_revisao_validacao(
                    nr_sequencia,
                    nr_seq_doc_revisao,
                    dt_atualizacao,
                    nm_usuario,
					cd_pessoa_validacao)
	select qua_doc_revisao_validacao_seq.nextval,
					nr_seq_rev_p,                                				
                    sysdate,
                    nm_usuario_p,
					cd_pessoa_vali_w
					from dual where not exists (select 1 from qua_doc_revisao_validacao
	where 			nr_seq_doc_revisao = nr_seq_rev_p
	and 			cd_pessoa_validacao = cd_pessoa_vali_w);			
end;
end loop;
close c01;
end if;
					
commit;

end Qua_Inserir_Validadores_Doc;
/  