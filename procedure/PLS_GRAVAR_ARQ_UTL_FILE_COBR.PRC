create or replace
procedure pls_gravar_arq_utl_file_cobr(	nr_seq_cobranca_p	number,
					ds_arq_rede_p		varchar2,
					nm_usuario_p		varchar2) is 

begin
if	(nr_seq_cobranca_p is not null) and
	(ds_arq_rede_p is not null) then
	
	insert into cobranca_escrit_arq 
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_cobranca,
		ds_arquivo)
	values(	cobranca_escrit_arq_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_cobranca_p,
		ds_arq_rede_p);
end if;

end pls_gravar_arq_utl_file_cobr;
/