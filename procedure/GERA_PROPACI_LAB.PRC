create or replace 
procedure Gera_Propaci_Lab (nr_prescricao_p		number,
				    nr_seq_prescr_p		number,
				    nm_usuario_p		varchar2) is

nr_sequencia_w			Number(10,0);

CURSOR C01 IS
select a.nr_sequencia	
from   prescr_procedimento a
where  not exists (select z.nr_sequencia 
			 from procedimento_paciente z
			 where z.nr_prescricao = nr_prescricao_p
			   and z.nr_sequencia_prescricao = a.nr_sequencia)	
  and  a.nr_prescricao		= nr_prescricao_p
  and  ((a.nr_sequencia		= nr_seq_prescr_p) or
	  (nr_seq_prescr_p		= 0));

BEGIN

OPEN C01;
LOOP
FETCH C01 	into nr_sequencia_w;
	EXIT WHEN 	C01%NOTFOUND;
	Gerar_Proc_Pac_Prescricao(nr_prescricao_p, nr_sequencia_w, 0, 0, nm_usuario_p, null, null,null);
	update prescr_procedimento
	set dt_baixa = null,
	    cd_motivo_baixa = 0
	where nr_prescricao = nr_prescricao_p
	  and nr_sequencia = nr_sequencia_w;
END LOOP;
CLOSE C01;
commit;

END;
/