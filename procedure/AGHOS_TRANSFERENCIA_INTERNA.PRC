create or replace
procedure Aghos_transferencia_interna(	nr_atendimento_p	number,
					nr_seq_unid_destino_p	number,
					nm_usuario_p		varchar2,
					nr_seq_contig_p		number default 0) is

cd_setor_atendimento_w		number(10);
ds_motivo_cancelamento_w	varchar2(1000);
cd_cnes_solicitante_w		varchar2(1000);
ds_sep_bv_w			varchar2(50);
ds_comando_w			varchar2(2000);
cd_setor_destino_w		number(10);
nm_setor_integracao_w		varchar2(255);
nr_internacao_w			number(10);
cd_enfermaria_w			varchar2(3);
cd_leito_w			varchar2(10);
cd_dig_leito_w			varchar2(1);
ds_usuario_w			varchar2(15);
nr_seq_unidade_w		number(10);
cd_pessoa_fisica_w		varchar2(10);

pragma autonomous_transaction;

begin

/*  
IDF_GSH_I_INTERN_TRAINT_ENT 	NUMBER 		not null
TIP_STATUS 			VARCHAR2 1 	not null
DES_ERRO 			VARCHAR2 500 	null
DAT_INTEGRACAO 		DATE 		not null
DES_USUARIO 			VARCHAR2 15 	not null
COD_CNES_SOLICITANTE 		NUMBER 7 	not null
IDF_INTERNACAO 		NUMBER 9 	not null
COD_POSTO_ENFERMAGEM 		NUMBER 3 	not null
COD_ENFERMARIA			NUMBER 3 	not null
COD_LEITO			NUMBER 10 	not null
COD_DIGITO_LEITO 		VARCHAR2 1 	null
DES_LAUDO 			VARCHAR2 1000 	null
IDF_TRANSFERENCIA_ESTADO 	NUMBER 1 	not null
*/

select	max(substr(obter_cnes_estab(b.cd_estabelecimento), 1, 7)),
	max(a.nr_internacao),
	nvl(max(substr(obter_dados_param_atend(b.cd_estabelecimento, 'US'), 1, 15)), 'TREINA'),
	max(nr_seq_unidade)
into	cd_cnes_solicitante_w,
	nr_internacao_w,
	ds_usuario_w,
	nr_seq_unidade_w
from	solicitacao_tasy_aghos a,
	atendimento_paciente b
where	a.nr_atendimento = nr_atendimento_p
and	b.nr_atendimento = a.nr_atendimento;

if	(nr_internacao_w is not null) and
	(nvl(nr_seq_unidade_w, 0) <> nr_seq_unid_destino_p) then

	

	select	max(nm_setor_integracao),
		max(cd_setor_atendimento)
	into	nm_setor_integracao_w,
		cd_setor_atendimento_w
	from	unidade_atendimento
	where	nr_seq_interno = nr_seq_unid_destino_p;

	cd_enfermaria_w := trim(substr(nm_setor_integracao_w, 1, instr(nm_setor_integracao_w, ';') - 1));	
	nm_setor_integracao_w := substr(nm_setor_integracao_w, instr(nm_setor_integracao_w, ';') + 1, length(nm_setor_integracao_w));
	cd_leito_w 	:= trim(substr(nm_setor_integracao_w, 1, instr(nm_setor_integracao_w, ';')- 1));
	if	(instr(nm_setor_integracao_w, ';') > 0) then
		nm_setor_integracao_w := substr(nm_setor_integracao_w, instr(nm_setor_integracao_w, ';') + 1, length(nm_setor_integracao_w));
		cd_dig_leito_w	:= trim(nm_setor_integracao_w);
	else
		cd_dig_leito_w	:= null;
	end if;
	

	select	max(cd_setor_externo)
	into	cd_setor_destino_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_w;

	if	(cd_setor_destino_w is not null) and
		(cd_enfermaria_w is not null) and
		(cd_leito_w is not null) then

		ds_sep_bv_w := obter_separador_bv;

		ds_comando_w :=	'declare '||
				'	traint_sai gsh_i_pkg_integra_sai.traint_sai@tasy_aghos; '||
				'	ds_erro_ww	varchar2(500); '||
				'begin '||
				' '||
				'   begin ' ||
				'	gsh_i_prc_intern_traint_ent@tasy_aghos(	0, '||
				'						''A'',  '||
				'						ds_erro_ww, '||
				'						sysdate, '||
				'						:ds_usuario_p, '||
				'						:cd_cnes_solicitante_p, '||
				'						:nr_internacao_p, '||
				'						:cd_setor_destino_p, '||
				'						:cd_enfermaria_p, '||
				'						:cd_leito_p, '||
				'						:cd_dig_leito_p, '||
				'						null, '||
				'						1 '|| -- transferêncie entra sempre como "realizada"
				'						); '||
				' '||
				'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_traint_sai@tasy_aghos(traint_sai); '||
				' '||
				'	if	(traint_sai.first is not null) then '||
				'		for i in traint_sai.first..traint_sai.last loop '||
				' '||
				'			if	(traint_sai(i).idf_internacao = :nr_internacao_p) then '||
				' '||
				'				gsh_i_prc_intern_traint_s_u@tasy_aghos(	traint_sai(i).idf_gsh_i_intern_traint_sai,  '||
				'									''P'', '||
				'									ds_erro_ww); '||				
				'			end if; '||
				'		end loop; '||
				'	end if; '||				
				' commit; '||		
				' DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||			
				'   exception '||
				'   when others then '||
				'      rollback; ' ||
				'      DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||				
				'   end; '||				
				'end;';
	
		exec_sql_dinamico_bv('AGHOS', 	ds_comando_w,	'nr_internacao_p='	|| nr_internacao_w 		|| ds_sep_bv_w ||
								'cd_cnes_solicitante_p='|| cd_cnes_solicitante_w 	|| ds_sep_bv_w ||
								'cd_setor_destino_p='	|| cd_setor_destino_w	 	|| ds_sep_bv_w ||
								'cd_enfermaria_p='	|| cd_enfermaria_w	 	|| ds_sep_bv_w ||
								'cd_leito_p='		|| cd_leito_w		 	|| ds_sep_bv_w ||
								'cd_dig_leito_p='	|| cd_dig_leito_w		|| ds_sep_bv_w ||
								'ds_usuario_p='		|| ds_usuario_w);
	end if;
end if;

commit;

end Aghos_transferencia_interna;
/