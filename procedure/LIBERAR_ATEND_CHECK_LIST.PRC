create or replace
procedure liberar_atend_check_list(
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is
	
begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	update	atend_check_list
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;
commit;
end liberar_atend_check_list;
/