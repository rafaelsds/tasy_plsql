create or replace
procedure ajustar_valor_tributos_nf(	nr_seq_nf_p		number,
					nr_item_nf_p		number,
					cd_tributo_p		number,
					vl_ajuste_p		number,
					ie_tipo_alt_p		number,
					dt_venc_tributo date default null) is 
/* ie_tipo_alt_p =
1 = nota_fiscal_item_trib
2 = nota_fiscal_trib
3 = nota_fiscal_venc_trib
*/

vl_tributo_w			number(13,2);
vl_base_calculo_w		number(13,2);
tx_tributo_w			number(15,4);

begin
if	(nr_seq_nf_p > 0) then
	if	(ie_tipo_alt_p = 1) and
		(nr_item_nf_p > 0) then

		select	vl_base_calculo,
			vl_tributo
		into	vl_base_calculo_w,
			vl_tributo_w
		from	nota_fiscal_item_trib
		where	nr_sequencia = nr_seq_nf_p
		and	nr_item_nf = nr_item_nf_p
		and	cd_tributo = cd_tributo_p;

		vl_tributo_w	:= (vl_tributo_w + vl_ajuste_p);
		tx_tributo_w 	:= (vl_tributo_w * 100) / vl_base_calculo_w;

		update	nota_fiscal_item_trib
		set	vl_tributo = vl_tributo_w,
			tx_tributo = tx_tributo_w
		where	nr_sequencia = nr_seq_nf_p
		and	nr_item_nf = nr_item_nf_p
		and	cd_tributo = cd_tributo_p;

	elsif	(ie_tipo_alt_p = 2) then
		
		select	vl_base_calculo,
			vl_tributo
		into	vl_base_calculo_w,
			vl_tributo_w
		from	nota_fiscal_trib
		where	nr_sequencia = nr_seq_nf_p
		and	cd_tributo = cd_tributo_p;

		vl_tributo_w	:= (vl_tributo_w + vl_ajuste_p);
		tx_tributo_w 	:= (vl_tributo_w * 100) / vl_base_calculo_w;

		update	nota_fiscal_trib
		set	vl_tributo = vl_tributo_w,
			tx_tributo = tx_tributo_w
		where	nr_sequencia = nr_seq_nf_p
		and	cd_tributo = cd_tributo_p;
		
	elsif	(ie_tipo_alt_p = 3) then

		select	vl_base_calculo,
			vl_tributo
		into	vl_base_calculo_w,
			vl_tributo_w
		from	nota_fiscal_venc_trib
		where	nr_sequencia = nr_seq_nf_p
		and	cd_tributo = cd_tributo_p
	               and           dt_vencimento = dt_venc_tributo;

		vl_tributo_w	:= (vl_tributo_w + vl_ajuste_p);
		tx_tributo_w 	:= (vl_tributo_w * 100) / vl_base_calculo_w;

		update	nota_fiscal_venc_trib
		set	vl_tributo = vl_tributo_w,
			tx_tributo = tx_tributo_w
		where	nr_sequencia = nr_seq_nf_p
		and	cd_tributo = cd_tributo_p
     		and 	dt_vencimento = dt_venc_tributo;

	end if;
end if;
commit;

end ajustar_valor_tributos_nf;
/