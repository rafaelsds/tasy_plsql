create or replace
procedure Desfazer_Baixa_Item_Acha_Perd(
			                nm_usuario_p		Varchar2,
					nr_seq_item_p		number,
					nr_seq_atend_achado_p	number) is 

dt_baixa_w	date;					
					
begin

if	(nvl(nr_seq_item_p,0) > 0) then

	update	atend_achado_perdido_item
	set	dt_baixa                     = null,
		nm_usuario                   = nm_usuario_p,
		dt_atualizacao		     = sysdate,	
		nr_seq_status_achado_perdido = null,
		nr_seq_tipo_baixa            = null,
		cd_pessoa_retirada           = null,
		nm_pessoa_retirada           = null,
		cd_cnpj_doacao               = null,
		nm_pessoa_juridica_doacao    = null,
		cd_pessoa_doacao             = null,
		nm_pessoa_doacao             = null,
		ds_obs_baixa		     = null
	where	nr_sequencia  = nr_seq_item_p;
	
	select	dt_baixa
	into	dt_baixa_w
	from	atend_achado_perdido
	where	nr_sequencia = nr_seq_atend_achado_p;

	if	(dt_baixa_w is not null) then
		
		update	atend_achado_perdido
		set	dt_baixa = null,
			nm_usuario = nm_usuario_p
		where	nr_sequencia = nr_seq_atend_achado_p;
		
	end if;
	

end if;

commit;

end Desfazer_Baixa_Item_Acha_Perd;
/