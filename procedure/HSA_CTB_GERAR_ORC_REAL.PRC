create or replace
procedure hsa_ctb_gerar_orc_real (	cd_empresa_p		number,
					cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_fim_p		date,
					cd_centro_custo_p		number,
					cd_conta_contabil_p	varchar2,
					cd_classif_conta_p		varchar2,
					cd_classif_centro_p		varchar2,
					ie_valor_p		number,
					nm_usuario_p		varchar2,
					ie_relatorio_p		varchar2,
					ie_apurar_result_p		varchar2,
					ie_orc_empresa_p		varchar2) is

cd_empresa_w			number(4);
cd_estab_w			number(4)	:= cd_estabelecimento_p;
cd_estabelecimento_w		number(4);
cd_conta_contabil_w		varchar2(40);
cd_centro_custo_w		number(08,0);
cd_classificacao_w		varchar2(40);
cd_classificacao_ww		varchar2(40);
cd_classif_w			varchar2(255);
cd_classif_ww			varchar2(255);
ds_conta_contabil_w		varchar2(255);
ds_centro_custo_w		varchar2(255);
k				integer;
j				integer;
ds_erro_w			varchar2(2000);
ie_apurar_result_w		varchar2(01);
ie_permite_w			varchar2(1);
ie_pos_w			number(10);
ie_gerar_w			varchar2(1)	:= 'S';
ie_debito_credito_w		varchar2(01);
dt_inicio_w			date;
dt_fim_w			date;
qt_reg_w			number(10);
vl_total_orc_w			number(10);
vl_total_real_w			number(10);

cursor c01 is
select	y.cd_empresa,
	y.cd_estabelecimento,
	y.cd_conta_contabil,
	y.cd_centro_custo,
	y.cd_classificacao,
	y.ds_conta_contabil,
	y.ds_centro_custo,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_1,'D',y.vl_orc_1 * -1),y.vl_orc_1) vl_orc_1,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_2,'D',y.vl_orc_2 * -1), y.vl_orc_2) vl_orc_2,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_3,'D',y.vl_orc_3 * -1), y.vl_orc_3) vl_orc_3,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_4,'D',y.vl_orc_4 * -1), y.vl_orc_4) vl_orc_4,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_5,'D',y.vl_orc_5 * -1), y.vl_orc_5) vl_orc_5,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_6,'D',y.vl_orc_6 * -1), y.vl_orc_6) vl_orc_6,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_7,'D',y.vl_orc_7 * -1), y.vl_orc_7) vl_orc_7,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_8,'D',y.vl_orc_8 * -1), y.vl_orc_8) vl_orc_8,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_9,'D',y.vl_orc_9 * -1), y.vl_orc_9) vl_orc_9,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_10,'D',y.vl_orc_10 * -1), y.vl_orc_10) vl_orc_10,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_11,'D',y.vl_orc_11 * -1), y.vl_orc_11) vl_orc_11,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_orc_12,'D',y.vl_orc_12 * -1), y.vl_orc_12) vl_orc_12,	
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_1,'D',y.vl_real_1 * -1), y.vl_real_1) vl_real_1,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_2,'D',y.vl_real_2 * -1), y.vl_real_2) vl_real_2,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_3,'D',y.vl_real_3 * -1), y.vl_real_3) vl_real_3,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_4,'D',y.vl_real_1 * -1), y.vl_real_4) vl_real_4,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_5,'D',y.vl_real_5 * -1), y.vl_real_5) vl_real_5,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_6,'D',y.vl_real_6 * -1), y.vl_real_6) vl_real_6,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_7,'D',y.vl_real_7 * -1), y.vl_real_7) vl_real_7,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_8,'D',y.vl_real_8 * -1), y.vl_real_8) vl_real_8,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_9,'D',y.vl_real_9 * -1), y.vl_real_9) vl_real_9,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_10,'D',y.vl_real_10 * -1), y.vl_real_10) vl_real_10,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_11,'D',y.vl_real_11 * -1), y.vl_real_11) vl_real_11,
	decode(ie_apurar_result_p,'S',decode(y.ie_debito_credito,'C',y.vl_real_12,'D',y.vl_real_12 * -1), y.vl_real_12) vl_real_12,
	y.ie_debito_credito
from(
select	m.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	decode(ie_relatorio_p, 'A', a.cd_centro_custo, 0) cd_centro_custo,
	c.cd_classificacao_atual cd_classificacao,
	substr(c.ds_conta_apres,1,80) ds_conta_contabil,
	decode(ie_relatorio_p, 'A', substr(obter_desc_centro_custo(a.cd_centro_custo),1,80), 'Total') ds_centro_custo,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,0), nvl(a.vl_orcado,0),0)),0) vl_orc_1,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,1), nvl(a.vl_orcado,0),0)),0) vl_orc_2,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,2), nvl(a.vl_orcado,0),0)),0) vl_orc_3,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,3), nvl(a.vl_orcado,0),0)),0) vl_orc_4,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,4), nvl(a.vl_orcado,0),0)),0) vl_orc_5,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,5), nvl(a.vl_orcado,0),0)),0) vl_orc_6,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,6), nvl(a.vl_orcado,0),0)),0) vl_orc_7,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,7), nvl(a.vl_orcado,0),0)),0) vl_orc_8,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,8), nvl(a.vl_orcado,0),0)),0) vl_orc_9,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,9), nvl(a.vl_orcado,0),0)),0) vl_orc_10,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,10), nvl(a.vl_orcado,0),0)),0) vl_orc_11,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,11), nvl(a.vl_orcado,0),0)),0) vl_orc_12,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,0), nvl(a.vl_realizado,0),0)),0) vl_real_1,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,1), nvl(a.vl_realizado,0),0)),0) vl_real_2,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,2), nvl(a.vl_realizado,0),0)),0) vl_real_3,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,3), nvl(a.vl_realizado,0),0)),0) vl_real_4,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,4), nvl(a.vl_realizado,0),0)),0) vl_real_5,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,5), nvl(a.vl_realizado,0),0)),0) vl_real_6,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,6), nvl(a.vl_realizado,0),0)),0) vl_real_7,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,7), nvl(a.vl_realizado,0),0)),0) vl_real_8,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,8), nvl(a.vl_realizado,0),0)),0) vl_real_9,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,9), nvl(a.vl_realizado,0),0)),0) vl_real_10,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,10), nvl(a.vl_realizado,0),0)),0) vl_real_11,
	nvl(sum(decode(m.dt_referencia, add_months(dt_inicio_w,11), nvl(a.vl_realizado,0),0)),0) vl_real_12,	
	e.ie_debito_credito
from	ctb_grupo_conta e,
	conta_contabil_v c,
	ctb_orcamento a,
	ctb_mes_ref m
where	m.dt_referencia between dt_inicio_w and dt_fim_p
and	a.nr_seq_mes_ref		= m.nr_sequencia
and	a.cd_conta_contabil	= c.cd_conta_contabil
and	c.cd_grupo		= e.cd_grupo
and	m.cd_empresa		= cd_empresa_p
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_centro_custo		= nvl(cd_centro_custo_p, a.cd_centro_custo)
and	a.cd_conta_contabil	= nvl(cd_conta_contabil_p, a.cd_conta_contabil)
/*and	((nvl(cd_classif_centro_p,'0') = '0') or
		(ctb_obter_se_centro_classif(a.cd_centro_custo, cd_classif_centro_p) = 'S'))*/
and	ctb_obter_se_centro_usuario(a.cd_centro_custo, m.cd_empresa, nm_usuario_p) = 'S'
group by m.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	decode(ie_relatorio_p, 'A', a.cd_centro_custo, 0),
	c.cd_classificacao_atual,
	c.ds_conta_apres,
	decode(ie_relatorio_p, 'A', substr(obter_desc_centro_custo(a.cd_centro_custo),1,80), 'Total'),
	e.ie_debito_credito) y
where	1 = 1
order by y.cd_centro_custo,
	 y.cd_conta_contabil;
	
vet01 c01%rowtype;

begin
dt_inicio_w		:= trunc(dt_inicio_p,'mm');
ie_apurar_result_w	:= ie_apurar_result_p;

if	(ie_orc_empresa_p = 'S') then
	cd_estab_w	:= null;
end if;

delete from w_hsa_ctb_orcamento;
commit;

if	(instr(cd_classif_conta_p, '-') > 0) then
	
	ie_permite_w	:= 'N';
else
	ie_permite_w	:= 'S';
	
end if;

open c01;
loop
fetch c01 into
	vet01;
exit when c01%notfound;

	ie_gerar_w	:= 'S';

	if  	(ie_gerar_w = 'S') and
		(nvl(cd_classif_conta_p,'0') <> '0' ) then
		begin
		
		ie_gerar_w	:= 'N';
		cd_classif_w	:= cd_classif_conta_p;
		cd_classif_w	:= replace(cd_classif_w,'(','');
		cd_classif_w	:= replace(cd_classif_w,')','');
		cd_classif_w	:= replace(cd_classif_w,'-','');
		cd_classif_w	:= replace(cd_classif_w,' ','');
		
		while (ie_gerar_w = 'N') and (length(cd_classif_w) > 0)  loop
			begin
		
			ie_pos_w 	:= instr(cd_classif_w,',');
			if	(ie_pos_w = 0) then
				cd_classif_ww	:= cd_classif_w;
				cd_classif_w	:= '';
			else
				cd_classif_ww	:= substr(cd_classif_w,1, ie_pos_w - 1);
				cd_classif_w	:= substr(cd_classif_w, ie_pos_w + 1, 255);
			end if;
			
			ie_gerar_w := CTB_Obter_Se_Conta_Classif_Sup(vet01.cd_conta_contabil, cd_classif_ww);
			
			end;
		end loop;
		
		if	(ie_permite_w = 'N') and
			(ie_gerar_w = 'S') then
			
			ie_gerar_w	:= 'N';
		elsif	(ie_permite_w = 'N') and
			(ie_gerar_w = 'N') then
			
			ie_gerar_w	:= 'S';				
		end if;
		end;
	end if;

	if	(ie_gerar_w = 'S') then
		begin
	
		/*TOTALIZA VL_ORCADO*/
		vet01.vl_orc_1		:= nvl(vet01.vl_orc_1,0);
		vet01.vl_orc_2		:= nvl(vet01.vl_orc_2,0);
		vet01.vl_orc_3		:= nvl(vet01.vl_orc_3,0);
		vet01.vl_orc_4		:= nvl(vet01.vl_orc_4,0);
		vet01.vl_orc_5		:= nvl(vet01.vl_orc_5,0);
		vet01.vl_orc_6		:= nvl(vet01.vl_orc_6,0);
		vet01.vl_orc_7		:= nvl(vet01.vl_orc_7,0);
		vet01.vl_orc_8		:= nvl(vet01.vl_orc_8,0);
		vet01.vl_orc_9		:= nvl(vet01.vl_orc_9,0);
		vet01.vl_orc_10		:= nvl(vet01.vl_orc_10,0);
		vet01.vl_orc_11		:= nvl(vet01.vl_orc_11,0);
		vet01.vl_orc_12		:= nvl(vet01.vl_orc_12,0);
		vl_total_orc_w 	:= nvl(vet01.vl_orc_1 + vet01.vl_orc_2 + vet01.vl_orc_3 + vet01.vl_orc_4 + vet01.vl_orc_5 + vet01.vl_orc_6 +
				vet01.vl_orc_7 + vet01.vl_orc_8 + vet01.vl_orc_9 + vet01.vl_orc_10 + vet01.vl_orc_11 + vet01.vl_orc_12,0);
		
		/*TOTALIZA VL_REALIZADO*/
		vet01.vl_real_1		:= nvl(vet01.vl_real_1,0);
		vet01.vl_real_2		:= nvl(vet01.vl_real_2,0);
		vet01.vl_real_3		:= nvl(vet01.vl_real_3,0);
		vet01.vl_real_4		:= nvl(vet01.vl_real_4,0);
		vet01.vl_real_5		:= nvl(vet01.vl_real_5,0);
		vet01.vl_real_6		:= nvl(vet01.vl_real_6,0);
		vet01.vl_real_7		:= nvl(vet01.vl_real_7,0);
		vet01.vl_real_8		:= nvl(vet01.vl_real_8,0);
		vet01.vl_real_9		:= nvl(vet01.vl_real_9,0);
		vet01.vl_real_10	:= nvl(vet01.vl_real_10,0);
		vet01.vl_real_11	:= nvl(vet01.vl_real_11,0);
		vet01.vl_real_12	:= nvl(vet01.vl_real_12,0);
		vl_total_real_w 	:= nvl(vet01.vl_real_1 + vet01.vl_real_2 + vet01.vl_real_3 + vet01.vl_real_4 + vet01.vl_real_5 + vet01.vl_real_6 +
				vet01.vl_real_7 + vet01.vl_real_8 + vet01.vl_real_9 + vet01.vl_real_10 + vet01.vl_real_11 + vet01.vl_real_12,0);
		
		select	count(*)
		into	qt_reg_w
		from	w_hsa_ctb_orcamento
		where	cd_estabelecimento	= vet01.cd_estabelecimento
		and	cd_empresa		= vet01.cd_empresa
		and	cd_centro_custo	= vet01.cd_centro_custo
		and	cd_conta_contabil	= vet01.cd_conta_contabil
		and	nm_usuario		= nm_usuario_p;
		
		if	(qt_reg_w = 0) then
			begin
			insert into w_hsa_ctb_orcamento(
					 cd_estabelecimento,
					 cd_conta_contabil,
					 cd_centro_custo,
					 ds_conta_contabil,
					 ds_centro_custo,
					 vl_orc_1,
					 vl_orc_2,
					 vl_orc_3,
					 vl_orc_4,
					 vl_orc_5,
					 vl_orc_6,
					 vl_orc_7,
					 vl_orc_8,
					 vl_orc_9,
					 vl_orc_10,
					 vl_orc_11,
					 vl_orc_12,
					 vl_total,
					 cd_empresa,
					 nm_usuario,
					 cd_classificacao,
					 vl_referencia,
					 vl_real_1,
					 vl_real_2,
					 vl_real_3,
					 vl_real_4,
					 vl_real_5,
					 vl_real_12,
					 vl_real_11,
					 vl_real_10,
					 vl_real_7,
					 vl_real_9,
					 vl_real_8,
					 vl_real_6,
					 nr_sequencia,
					 vl_total_real)
				values(	 vet01.cd_estabelecimento,
					 vet01.cd_conta_contabil,
					 vet01.cd_centro_custo,
					 vet01.ds_conta_contabil,
					 vet01.ds_centro_custo,
					 vet01.vl_orc_1,
					 vet01.vl_orc_2,
					 vet01.vl_orc_3,
					 vet01.vl_orc_4,
					 vet01.vl_orc_5,
					 vet01.vl_orc_6,
					 vet01.vl_orc_7,
					 vet01.vl_orc_8,
					 vet01.vl_orc_9,
					 vet01.vl_orc_10,
					 vet01.vl_orc_11,
					 vet01.vl_orc_12,
					 vl_total_orc_w,
					 vet01.cd_empresa,
					 nm_usuario_p,
					 vet01.cd_classificacao,
					 0,
					 vet01.vl_real_1,
					 vet01.vl_real_2,
					 vet01.vl_real_3,
					 vet01.vl_real_4,
					 vet01.vl_real_5,
					 vet01.vl_real_12,
					 vet01.vl_real_11,
					 vet01.vl_real_10,
					 vet01.vl_real_7,
					 vet01.vl_real_9,
					 vet01.vl_real_8,
					 vet01.vl_real_6,
					 1,
					 vl_total_real_w);
			end;
			else
				begin
				update	w_hsa_ctb_orcamento
				set	vl_total		= vl_total   + vl_total_orc_w,
					vl_orc_1		= vl_orc_1   + vet01.vl_orc_1,
					vl_orc_2		= vl_orc_2   + vet01.vl_orc_2,
					vl_orc_3		= vl_orc_3   + vet01.vl_orc_3,
					vl_orc_4		= vl_orc_4   + vet01.vl_orc_4,
					vl_orc_5		= vl_orc_5   + vet01.vl_orc_5,
					vl_orc_6		= vl_orc_6   + vet01.vl_orc_6,
					vl_orc_7		= vl_orc_7   + vet01.vl_orc_7,
					vl_orc_8		= vl_orc_8   + vet01.vl_orc_8,
					vl_orc_9		= vl_orc_9   + vet01.vl_orc_9,
					vl_orc_10		= vl_orc_10  + vet01.vl_orc_10,
					vl_orc_11		= vl_orc_11  + vet01.vl_orc_11,
					vl_orc_12		= vl_orc_12  + vet01.vl_orc_12,
					vl_real_1		= vl_real_1  + vet01.vl_real_1,
					vl_real_2		= vl_real_2  + vet01.vl_real_2,
					vl_real_3		= vl_real_3  + vet01.vl_real_3,
					vl_real_4		= vl_real_4  + vet01.vl_real_4,
					vl_real_5		= vl_real_5  + vet01.vl_real_5,
					vl_real_6		= vl_real_6  + vet01.vl_real_6,
					vl_real_7		= vl_real_7  + vet01.vl_real_7,
					vl_real_8		= vl_real_8  + vet01.vl_real_8,
					vl_real_9		= vl_real_9  + vet01.vl_real_9,
					vl_real_10		= vl_real_10 + vet01.vl_real_10,
					vl_real_11		= vl_real_11 + vet01.vl_real_11,
					vl_real_12		= vl_real_12 + vet01.vl_real_12,
					vl_total_real		= vl_total_real + vl_total_real_w
				where	cd_estabelecimento	= vet01.cd_estabelecimento
				and	cd_conta_contabil	= vet01.cd_conta_contabil
				and	cd_centro_custo		= vet01.cd_centro_custo
				and	cd_empresa		= vet01.cd_empresa;
				exception when others then
				ds_erro_w		:= sqlerrm(sqlcode);
				raise_application_error(-20011,'Erro Insert! Centro:' || vet01.cd_centro_custo || ' Conta: ' || vet01.cd_conta_contabil ||
							chr(13) || 'Usu�rio: ' || nm_usuario_p || ' Estab: ' || vet01.cd_estabelecimento || chr(13) ||
							'Empresa: ' || vet01.cd_empresa || ' REG: ' || qt_reg_w || chr(13) || ds_erro_w);
				end;
			end if;
		

		select	campo_numerico(ctb_obter_nivel_classif_conta(vet01.cd_classificacao))
		into	k
		from 	dual;
	
		cd_classificacao_w	:= vet01.cd_classificacao;
		
		while k > 1 loop
			
			cd_classificacao_ww	:= cd_classificacao_w;
			
			select	nvl(ctb_obter_classif_conta_sup(cd_classificacao_w, dt_inicio_w, vet01.cd_empresa), cd_classificacao_ww)
			into	cd_classificacao_w
			from	dual;

			
			cd_conta_contabil_w	:= 	substr(ctb_obter_codigo_classif_vig(vet01.cd_empresa, cd_classificacao_w,dt_inicio_w), 1, 20);
			
			select	substr(max(ds_conta_apres),1,255)
			into	ds_conta_contabil_w
			from	conta_contabil_v
			where 	cd_conta_contabil	= cd_conta_contabil_w
			and	cd_empresa		= vet01.cd_empresa;

		
			if	(nvl(ds_conta_contabil_w,'X') = 'X') then
				raise_application_error(-20011,'A conta/Classif: ' || cd_conta_contabil_w || ' / ' || cd_classificacao_w || ' n�o tem apresenta��o cadastrada!');
			end if;
			if	(cd_conta_contabil_w is null) then
				raise_application_error(-20011,'A classifica��o ' || cd_classificacao_w || ' n�o tem conta cadastrada');
			else
				begin
				commit;
				select	count(*)
				into	qt_reg_w
				from	w_hsa_ctb_orcamento
				where	cd_estabelecimento	= vet01.cd_estabelecimento
				and	cd_empresa		= vet01.cd_empresa
				and	cd_centro_custo	= vet01.cd_centro_custo
				and	cd_conta_contabil	= cd_conta_contabil_w
				and	cd_classificacao	= cd_classificacao_w
				and	nm_usuario		= nm_usuario_p;
					
				if	(qt_reg_w = 0) then
					begin
					insert into w_hsa_ctb_orcamento(
						cd_estabelecimento,
						cd_conta_contabil,
						cd_centro_custo,
						ds_conta_contabil,
						ds_centro_custo,
						vl_orc_1,
						vl_orc_2,
						vl_orc_3,
						vl_orc_4,
						vl_orc_5,
						vl_orc_6,
						vl_orc_7,
						vl_orc_8,
						vl_orc_9,
						vl_orc_10,
						vl_orc_11,
						vl_orc_12,
						vl_total,
						cd_empresa,
						nm_usuario,
						cd_classificacao,
						vl_referencia,
						vl_real_1,
						vl_real_2,
						vl_real_3,
						vl_real_4,
						vl_real_5,
						vl_real_12,
						vl_real_11,
						vl_real_10,
						vl_real_7,
						vl_real_9,
						vl_real_8,
						vl_real_6,
						nr_sequencia,
						vl_total_real)
					values(	vet01.cd_estabelecimento,
						cd_conta_contabil_w,
						vet01.cd_centro_custo,
						ds_conta_contabil_w,
						vet01.ds_centro_custo,
						vet01.vl_orc_1,
						vet01.vl_orc_2,
						vet01.vl_orc_3,
						vet01.vl_orc_4,
						vet01.vl_orc_5,
						vet01.vl_orc_6,
						vet01.vl_orc_7,
						vet01.vl_orc_8,
						vet01.vl_orc_9,
						vet01.vl_orc_10,
						vet01.vl_orc_11,
						vet01.vl_orc_12,
						vl_total_orc_w,
						vet01.cd_empresa,
						nm_usuario_p,
						cd_classificacao_w,
						0,
						vet01.vl_real_1,
						vet01.vl_real_2,
						vet01.vl_real_3,
						vet01.vl_real_4,
						vet01.vl_real_5,
						vet01.vl_real_12,
						vet01.vl_real_11,
						vet01.vl_real_10,
						vet01.vl_real_7,
						vet01.vl_real_9,
						vet01.vl_real_8,
						vet01.vl_real_6,
						1,
						vl_total_real_w);
					exception when others then
						ds_erro_w		:= sqlerrm(sqlcode);
				raise_application_error(-20011,'Erro Insert! Centro:' || vet01.cd_centro_custo || ' Conta: ' || cd_conta_contabil_w ||
							chr(13) || 'Usu�rio: ' || nm_usuario_p || ' Estab: ' || vet01.cd_estabelecimento || chr(13) ||
							'Empresa: ' || vet01.cd_empresa || ' REG: ' || qt_reg_w || chr(13) || ds_erro_w);
					end;
				else
					begin
					
					update	w_hsa_ctb_orcamento
					set	vl_total		= vl_total   + vl_total_orc_w,
						vl_orc_1		= vl_orc_1   + vet01.vl_orc_1,
						vl_orc_2		= vl_orc_2   + vet01.vl_orc_2,
						vl_orc_3		= vl_orc_3   + vet01.vl_orc_3,
						vl_orc_4		= vl_orc_4   + vet01.vl_orc_4,
						vl_orc_5		= vl_orc_5   + vet01.vl_orc_5,
						vl_orc_6		= vl_orc_6   + vet01.vl_orc_6,
						vl_orc_7		= vl_orc_7   + vet01.vl_orc_7,
						vl_orc_8		= vl_orc_8   + vet01.vl_orc_8,
						vl_orc_9		= vl_orc_9   + vet01.vl_orc_9,
						vl_orc_10		= vl_orc_10  + vet01.vl_orc_10,
						vl_orc_11		= vl_orc_11  + vet01.vl_orc_11,
						vl_orc_12		= vl_orc_12  + vet01.vl_orc_12,
						vl_real_1		= vl_real_1  + vet01.vl_real_1,
						vl_real_2		= vl_real_2  + vet01.vl_real_2,
						vl_real_3		= vl_real_3  + vet01.vl_real_3,
						vl_real_4		= vl_real_4  + vet01.vl_real_4,
						vl_real_5		= vl_real_5  + vet01.vl_real_5,
						vl_real_6		= vl_real_6  + vet01.vl_real_6,
						vl_real_7		= vl_real_7  + vet01.vl_real_7,
						vl_real_8		= vl_real_8  + vet01.vl_real_8,
						vl_real_9		= vl_real_9  + vet01.vl_real_9,
						vl_real_10		= vl_real_10 + vet01.vl_real_10,
						vl_real_11		= vl_real_11 + vet01.vl_real_11,
						vl_real_12		= vl_real_12 + vet01.vl_real_12,
						vl_total_real		= vl_total_real + vl_total_real_w
					where	cd_estabelecimento	= vet01.cd_estabelecimento
					and	cd_conta_contabil	= cd_conta_contabil_w
					and	cd_centro_custo		= vet01.cd_centro_custo
					and	cd_classificacao	= cd_classificacao_w
					and	cd_empresa		= vet01.cd_empresa;
					exception when others then
						insert into log_tasy values(sysdate, nm_usuario_p, 301, vet01.cd_conta_contabil);
					end;
				end if;
				end;
			end if;
		select	campo_numerico(ctb_obter_nivel_classif_conta(cd_classificacao_w))
		into	j
		from 	dual;
		
		if	((K - J) > 1) then /* Tratar casos onde a diferen�a de niveis entre a proxima conta � maior do que um (ANS) */
			k := j + 1;
		end if;
		
		k	:= k - 1;
		end loop;
		end;
	end if;

end loop;
close c01;
commit;

end hsa_ctb_gerar_orc_real;
/