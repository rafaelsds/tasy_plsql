create or replace
procedure gerar_nr_sequencial_nf_item(
			nr_sequencia_p		Number) is 

nr_item_nf_w	Number(5);
nr_item_seq_w	Number(5) := 0;
			
Cursor C01 is
	select	 nr_item_nf	
	from	 nota_fiscal_item
	where	 nr_sequencia = nr_sequencia_p
	order by nr_item_nf;
			
begin

open C01;
loop
fetch C01 into	
	nr_item_nf_w;
exit when C01%notfound;
	begin
	
	nr_item_seq_w := nr_item_seq_w + 1;
	
	update	nota_fiscal_item
	set	nr_item_sequencial = nr_item_seq_w
	where	nr_sequencia = nr_sequencia_p
	and	nr_item_nf = nr_item_nf_w;
	
	end;
end loop;
close C01;

end gerar_nr_sequencial_nf_item;
/
