create or replace
procedure copiar_dic_objeto_filtro (
		cd_funcao_p	number,
		nr_seq_objeto_p	number,
		nm_usuario_p	varchar2,
		ie_dinamic_form_p	varchar2 default 'N',
		nr_seq_criado_p		out number,
		ie_html_p		varchar2 default 'N') is
		
nr_seq_objeto_w	number(10,0);

ds_mascara_w	varchar2(255);

nr_seq_obj_lookup_novo_w	dic_objeto.nr_sequencia%type;

Cursor c_campo_filtro is
	select	nr_sequencia,
			nr_seq_dic_objeto,
			ie_componente
	from	dic_objeto_filtro
	where	nr_seq_objeto	= nr_seq_objeto_p;


Cursor C04 is
	select 	a.nr_sequencia,
			trim(upper(a.ds_mascara)) ds_mascara,
			a.nm_atributo
	from 	dic_objeto_filtro a
	where 	a.nr_seq_objeto = nr_seq_objeto_w
	and		substr(a.NM_ATRIBUTO,1,2) = 'DT'
	and		a.ds_mascara is not null;
	
	
c04_w c04%rowtype;
		
begin
if	(cd_funcao_p is not null) and
	(nr_seq_objeto_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	dic_objeto_seq.nextval
	into	nr_seq_objeto_w
	from	dual;
	
	insert into dic_objeto (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_objeto,
		nm_objeto,
		cd_funcao,
		ie_layout_xy,
		ds_texto,
		qt_altura,
		qt_compensacao_x,
		qt_compensacao_y,
		cd_exp_texto)
	select	nr_seq_objeto_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		decode(nvl(ie_dinamic_form_p, 'N'), 'N', ie_tipo_objeto, 'WDF'),
		nm_objeto,
		cd_funcao_p,
		ie_layout_xy,
		ds_texto,
		qt_altura,
		qt_compensacao_x,
		qt_compensacao_y,
		cd_exp_texto
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_p;
	
	for r_c_campo_filtro in c_campo_filtro loop
	
		nr_seq_obj_lookup_novo_w	:= r_c_campo_filtro.nr_seq_dic_objeto;
		if	(r_c_campo_filtro.nr_seq_dic_objeto is not null) and
			(r_c_campo_filtro.ie_componente = 'WDBLCB') and
			(ie_dinamic_form_p = 'S' or ie_html_p = 'S') then
			COPIAR_DIC_OBJETO_LOOKUP(cd_funcao_p,r_c_campo_filtro.nr_seq_dic_objeto,nm_usuario_p,nr_seq_obj_lookup_novo_w);
		end if;
		
		insert into dic_objeto_filtro (
			nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			nr_seq_objeto,
			ie_componente,
			qt_tamanho,
			ds_label,
			nr_seq_apresent,
			qt_deslocamento,
			nr_seq_dic_objeto,
			cd_dominio,
			ds_opcoes,
			nm_atributo,
			ie_tipo_dado_objeto,
			nr_seq_localizador,
			cd_funcao,
			nm_funcao,
			ds_mascara,
			vl_default,
			qt_tam_cd_localizar,
			ie_botao_hoje,
			ie_botao_flecha,
			ie_obrigatorio,
			ie_desc_loc_edit,
			ie_deslocamento,
			qt_limite_inferior,
			qt_limite_superior,
			qt_coluna,
			nm_atributo_pai,
			ie_tipo_botao,
			qt_pos_x,
			qt_pos_y,
			qt_altura,
			ie_cod_proc_loc_edit,
			ds_separador,
			ie_tab_stop,
			ds_cor_fundo,
			ds_cor_fonte,
			ds_cor_fundo_label,
			ds_cor_fonte_label,
			ie_estilo_fonte,
			qt_tamanho_fonte,
			ie_estilo_fonte_label,
			qt_tamanho_fonte_label,
			nm_tabela_ref,
			ie_orientacao_dia_semana,
			ie_read_only,
			vl_default_dic_obj,
			ie_forma_passagem_valor_sql,
			cd_exp_label,
			cd_exp_opcoes,
			ie_manter_cor_readonly,
			ie_charcase,
			ie_fundo_transparente,
			ie_borda,
			nr_seq_diobfil,
			ie_atalho_at_fv,
			ie_atalho_li_fv,
			ie_popup_campo_desabilitado,
			ie_orientacao,
			nr_seq_apres_lista,
			ds_label_lista,
			ie_tipo_teclado,
			ds_sql_action,
			ie_tipo_action,
			ds_comando,
			ie_regra_incremento,
			ie_atalho,
			ie_alinhamento,
			ds_documentacao,
			nm_campo_ret_loc,
			ie_auto_expand_width,
			qt_pos_x_java,
			qt_pos_y_java,
			ie_primeiro_registro,
			ie_dia_semana,
			ie_posicao_dia_semana,
			qt_length,
			nm_comp_orig,
			ie_salvar,
			nm_imagem)
		select	dic_objeto_filtro_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_objeto_w,
			ie_componente,
			qt_tamanho,
			ds_label,
			nr_seq_apresent,
			qt_deslocamento,
			nr_seq_obj_lookup_novo_w,
			cd_dominio,
			ds_opcoes,
			nm_atributo,
			ie_tipo_dado_objeto,
			nr_seq_localizador,
			cd_funcao,
			nm_funcao,
			ds_mascara,
			vl_default,
			qt_tam_cd_localizar,
			ie_botao_hoje,
			ie_botao_flecha,
			ie_obrigatorio,
			ie_desc_loc_edit,
			ie_deslocamento,
			qt_limite_inferior,
			qt_limite_superior,
			qt_coluna,
			nm_atributo_pai,
			ie_tipo_botao,
			qt_pos_x,
			qt_pos_y,
			qt_altura,
			ie_cod_proc_loc_edit,
			ds_separador,
			ie_tab_stop,
			ds_cor_fundo,
			ds_cor_fonte,
			ds_cor_fundo_label,
			ds_cor_fonte_label,
			ie_estilo_fonte,
			qt_tamanho_fonte,
			ie_estilo_fonte_label,
			qt_tamanho_fonte_label,
			nm_tabela_ref,
			ie_orientacao_dia_semana,
			ie_read_only,
			vl_default_dic_obj,
			ie_forma_passagem_valor_sql,
			cd_exp_label,
			cd_exp_opcoes,
			ie_manter_cor_readonly,
			ie_charcase,
			ie_fundo_transparente,
			ie_borda,
			nr_seq_diobfil,
			ie_atalho_at_fv,
			ie_atalho_li_fv,
			ie_popup_campo_desabilitado,
			ie_orientacao,
			nr_seq_apres_lista,
			ds_label_lista,
			ie_tipo_teclado,
			ds_sql_action,
			ie_tipo_action,
			ds_comando,
			ie_regra_incremento,
			ie_atalho,
			ie_alinhamento,
			ds_documentacao,
			nm_campo_ret_loc,
			ie_auto_expand_width,
			qt_pos_x_java,
			qt_pos_y_java,
			ie_primeiro_registro,
			ie_dia_semana,
			ie_posicao_dia_semana,
			qt_length,
			nm_comp_orig,
			ie_salvar,
			nm_imagem
		from	dic_objeto_filtro
		where	nr_sequencia = r_c_campo_filtro.nr_sequencia;
	end loop;
	
	if	(ie_html_p	= 'S') then
		Ajustar_Dic_Objeto_filtro_Html(nr_seq_objeto_w,nm_usuario_p);
	end if;
	
	open c04;
	loop
	fetch c04 into 
		c04_w;
	exit when c04%notfound;
	begin
	
		ds_mascara_w := Obter_Mascara_Convertida(upper(c04_w.ds_mascara),c04_w.nm_atributo);
	 		 
		if 	(ds_mascara_w is not null) then
			update dic_objeto_filtro
			set  	ds_mascara = ds_mascara_w,
				dt_atualizacao = sysdate
			where  nr_sequencia = c04_w.nr_sequencia;
		end if;
	 
	 end;
	end loop;
	close c04;
	
	end;
end if;
nr_seq_criado_p	:= nr_seq_objeto_w;


commit;
end copiar_dic_objeto_filtro;
/