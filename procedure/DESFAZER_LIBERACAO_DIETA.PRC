create or replace
procedure desfazer_liberacao_dieta(	nr_seq_dieta_p	number,
				nm_usuario_p	varchar2) is 

begin

if	(nr_seq_dieta_p > 0) then
	update	mapa_dieta
	set	dt_liberacao		= null,
		nm_usuario_lib		= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_dieta_p
	and	dt_liberacao is not null;
	commit;
end if;

end desfazer_liberacao_dieta;
/
