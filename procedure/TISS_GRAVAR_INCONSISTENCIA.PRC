create or replace
procedure TISS_GRAVAR_INCONSISTENCIA
	(nr_seq_inconsistencia_p	in	number,
	nr_sequencia_autor_p		in	number,
	nr_interno_conta_p		in	number,
	nr_seq_protocolo_p		in	number,
	nm_usuario_p			in	varchar2,
	ds_complemento_p		in	varchar2) is

cont_w		number(10,0);

begin

if	(nvl(nr_sequencia_autor_p,0) > 0) then
	begin
	select	1
	into	cont_w
	from	w_tiss_inconsistencia
	where	nr_seq_inconsistencia		= nr_seq_inconsistencia_p
	and	nr_sequencia_autor		= nr_sequencia_autor_p
	and	ds_complemento			= ds_complemento_p
	and	rownum = 1;
	exception
	when others then
		cont_w	:= 0;
	end;
elsif	(nvl(nr_interno_conta_p,0) > 0) then
	begin
	select	1 
	into	cont_w
	from	w_tiss_inconsistencia
	where	nr_seq_inconsistencia		= nr_seq_inconsistencia_p
	and	nr_interno_conta		= nr_interno_conta_p
	and	ds_complemento			= ds_complemento_p
	and	rownum = 1;	
	exception
	when others then
		cont_w	:= 0;
	end;
elsif	(nvl(nr_seq_protocolo_p,0) > 0) then
	begin
	select	1
	into	cont_w
	from	w_tiss_inconsistencia
	where	nr_seq_inconsistencia		= nr_seq_inconsistencia_p
	and	nr_seq_protocolo		= nr_seq_protocolo_p
	and	ds_complemento			= ds_complemento_p
	and	rownum = 1;
	exception
	when others then
		cont_w	:= 0;
	end;
end if;

if	(cont_w = 0) then
	insert into w_tiss_inconsistencia
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_inconsistencia,
		nr_sequencia_autor,
		nr_interno_conta,
		nr_seq_protocolo,
		ds_complemento)
	select	w_tiss_inconsistencia_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_inconsistencia_p,
		nr_sequencia_autor_p,
		nr_interno_conta_p,
		nr_seq_protocolo_p,
		ds_complemento_p
	from	dual;	
	
	if	(nvl(tiss_convenio_pck.get_gravar_log_inconsist,'N') = 'S') then
		tiss_convenio_pck.gravar_log_inconsist(nm_usuario_p, nr_seq_inconsistencia_p, nr_sequencia_autor_p, nr_interno_conta_p, nr_seq_protocolo_p);
	end if;
	
	commit;
	
end if;

end TISS_GRAVAR_INCONSISTENCIA;
/