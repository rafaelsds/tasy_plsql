create or replace
procedure gerar_gant_control_projetos (
		nr_seq_gerencia_p	number,
		nr_seq_classificacao_p	number,
		dt_analise_p		date,
		nm_usuario_p		varchar2) is
		
nr_seq_estagio_w	number(10,0) := 12;

nr_semana_w		number(2,0);
dt_alocacao_w		date;

cd_pessoa_recurso_w	varchar2(10);
nr_seq_projeto_w	number(10,0);

nr_seq_alocacao_w	number(10,0);

cursor c01 is
select	p.nr_sequencia
from	proj_projeto p
where	p.nr_seq_gerencia = nr_seq_gerencia_p
and	p.nr_seq_classif = nvl(nr_seq_classificacao_p, p.nr_seq_classif)
and	p.nr_seq_estagio = nr_seq_estagio_w
and 	p.ie_posicao_externa = 'S'
and	((trunc(dt_alocacao_w,'dd') between p.dt_inicio_prev and p.dt_fim_prev) or
	 (p.dt_inicio_prev between trunc(dt_alocacao_w,'dd') and p.dt_fim_prev)) /* casos onde a semana come�a antes do in�cio do projeto */
and	not exists (
		select	1
		from	w_alocacao_recurso w
		where	w.nr_seq_projeto = p.nr_sequencia)
group by
	p.nr_sequencia
order by
	p.nr_sequencia;
	
cursor c02 is
select	p.nr_sequencia
from	proj_projeto p
where	p.nr_seq_gerencia = nr_seq_gerencia_p
and	p.nr_seq_classif = nvl(nr_seq_classificacao_p, p.nr_seq_classif)
and	p.nr_seq_estagio = nr_seq_estagio_w
and 	p.ie_posicao_externa = 'S'
and	((trunc(dt_alocacao_w,'dd') between p.dt_inicio_prev and p.dt_fim_prev) or
	 (p.dt_inicio_prev between trunc(dt_alocacao_w,'dd') and p.dt_fim_prev)) /* casos onde a semana come�a antes do in�cio do projeto */
group by
	p.nr_sequencia
order by
	p.nr_sequencia;	
		
begin
if	(nr_seq_gerencia_p is not null) and
	--(nr_seq_classificacao_p is not null) and
	(dt_analise_p is not null) and
	(nm_usuario_p is not null) then
	begin
	exec_sql_dinamico(nm_usuario_p, 'truncate table w_alocacao_recurso');
	
	nr_semana_w	:= 0;
	dt_alocacao_w	:= obter_inicio_fim_semana(dt_analise_p,'I');	
	
	while (nr_semana_w < 11) loop
		begin		
		open c01;
		loop
		fetch c01 into nr_seq_projeto_w;
		exit when c01%notfound;
			begin
			insert into w_alocacao_recurso (
				nr_sequencia,
				nm_usuario,
				cd_pessoa_recurso,
				nr_seq_projeto,
				ds_semana_00,
				ds_semana_01,
				ds_semana_02,
				ds_semana_03,
				ds_semana_04,
				ds_semana_05,
				ds_semana_06,
				ds_semana_07,
				ds_semana_08,
				ds_semana_09,
				ds_semana_10)
			values (
				w_alocacao_recurso_seq.nextval,
				nm_usuario_p,
				'0',
				nr_seq_projeto_w,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);			
			end;
		end loop;
		close c01;		
		nr_semana_w	:= nr_semana_w + 1;
		dt_alocacao_w	:= adicionar_semanas(dt_analise_p, nr_semana_w);
		end;
	end loop;
	
	nr_semana_w	:= 0;
	dt_alocacao_w	:= obter_inicio_fim_semana(dt_analise_p,'I');	
	
	while (nr_semana_w < 11) loop
		begin		
		open c02;
		loop
		fetch c02 into nr_seq_projeto_w;
		exit when c02%notfound;
			begin
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_alocacao_w
			from	w_alocacao_recurso
			where	nr_seq_projeto = nr_seq_projeto_w;
			
			if	(nr_seq_alocacao_w > 0) then
				begin
				update	w_alocacao_recurso
				set	ds_semana_00 = decode(nr_semana_w, 0, 'S', ds_semana_00),
					ds_semana_01 = decode(nr_semana_w, 1, 'S', ds_semana_01),
					ds_semana_02 = decode(nr_semana_w, 2, 'S', ds_semana_02),
					ds_semana_03 = decode(nr_semana_w, 3, 'S', ds_semana_03),
					ds_semana_04 = decode(nr_semana_w, 4, 'S', ds_semana_04),
					ds_semana_05 = decode(nr_semana_w, 5, 'S', ds_semana_05),
					ds_semana_06 = decode(nr_semana_w, 6, 'S', ds_semana_06),
					ds_semana_07 = decode(nr_semana_w, 7, 'S', ds_semana_07),
					ds_semana_08 = decode(nr_semana_w, 8, 'S', ds_semana_08),
					ds_semana_09 = decode(nr_semana_w, 9, 'S', ds_semana_09),
					ds_semana_10 = decode(nr_semana_w, 10, 'S', ds_semana_10)
				where	nr_sequencia = nr_seq_alocacao_w;
				end;
			end if;
			end;
		end loop;
		close c02;		
		nr_semana_w	:= nr_semana_w + 1;
		dt_alocacao_w	:= adicionar_semanas(dt_analise_p, nr_semana_w);
		end;
	end loop;	
	end;
end if;
commit;
end gerar_gant_control_projetos;
/