create or replace PROCEDURE Gerar_Solucao_Rotina_prescr(
			nr_seq_interno_p	varchar2,
			nr_prescricao_p		number,
			nm_usuario_p        	varchar2,
			cd_intervalo_p		varchar2 default null) is

nr_sequencia_w				Number(10);
nr_seq_solucao_w			Number(6);
ie_tipo_dosagem_w			Varchar2(3);
qt_dosagem_w				Number(15,4);
qt_solucao_total_w			Number(15,4);
qt_tempo_aplicacao_w		Number(15,4);
qt_volume_w					Number(15,4);
nr_etapas_w					Number(3);
ds_material_w				varchar2(255);
ds_unid_med_w				varchar2(40);
ie_bomba_infusao_w			Varchar2(1);
nr_agrupamento_w			Number(07,1);
nr_agrup_solucao_w			Number(07,1);
ie_esquema_alternado_w		Varchar2(1);
ie_calculo_aut_w			Varchar2(1);
ie_acm_w					Varchar2(1);
ie_solucao_especial_w		Varchar2(1);
ie_limpar_ACM_w				Varchar2(5);
qt_hora_fase_w				Number(15,4);
ds_horarios_w				Varchar2(100);
ds_horarios_2w				Varchar2(255);
dt_prescricao_w				Date;
dt_primeiro_horario_w		Date;
hr_primeiro_hoario_w		Varchar2(5);
ie_tipo_sol_w				Varchar2(5);
nr_seq_material_w			Number(10);
qt_limite_uma_hora_w		Number(10,1);
qt_conversao_dose_w	   		Number(15,4);
qt_dose_ataque_w			Number(15,2);
qt_conversao_ml_w			Number(15,4);
qt_unitaria_w   			Number(18,6);
qt_dispensar_w				Number(18,6);
qt_total_etapa_w			Number(15,4);
qt_dosagem_final_w			Number(15,4);
qt_material_w   			Number(15,3);
ie_se_necessario_w			Varchar2(1);
nr_seq_material_ww			Number(10);
CD_MATERIAL_w				Number(6);
CD_UNIDADE_MEDIDA_w			Varchar2(30);
QT_DOSE_w					Number(15,3);
qt_dose_ml_w				Number(15,3);
QT_SOLUCAO_w				Number(15,4);
IE_AGRUPADOR_w				Number(2);
DS_DOSE_DIFERENCIADA_w		Varchar2(50);
cd_unid_med_consumo_w		Varchar2(30);
QT_MINIMO_MULTIPLO_SOLIC_w	Number(13,4);
ds_erro_w					Varchar2(255);
cd_intervalo_w              Varchar2(7);
cd_intervalo_ww              Varchar2(7);
nr_seq_solucao_ww			Number(10,0);
cd_estabelecimento_w		Number(4);
cd_setor_atendimento_w		Number(10);
ie_via_aplicacao_w			Varchar2(5);
ds_recomendacao_w			Varchar2(2000);
ds_solucao_W				varchar2(100);
vl_retorno_w				Number(10);
qt_dose_peso_w				Number(15,4);
qt_peso_w					number(6,3);
ie_regra_disp_w				varchar2(1);
cd_protocolo_w				number(10);
nr_seq_medic_w				number(10);
nr_seq_sol_w				number(10);
ie_etapa_sol_acm_w			varchar2(1);
ie_gerar_etapa_w			varchar2(1);
ie_solucao_pca_w			varchar2(1);
ie_tipo_analgesia_w			varchar2(15);
ie_pca_modo_prog_w			varchar2(15);
qt_dose_inicial_pca_w		number(10,1);
qt_vol_infusao_pca_w		number(10,1);
qt_bolus_pca_w				number(10,1);
qt_intervalo_bloqueio_w		number(10,1);
qt_limite_quatro_hora_w		number(10,1);
ie_arredondar_w				varchar2(1);
ie_um_dose_inicio_pca_w		varchar2(15);
ie_um_limite_pca_w			varchar2(15);
ie_um_limite_hora_pca_w		varchar2(15);
ie_um_fluxo_pca_w			varchar2(15);
ie_um_bolus_pca_w			varchar2(15);
ds_justificativa_w			varchar2(2000);
ie_intervalo_setor_w		varchar2(1);
nr_atendimento_w			number(10);
ie_aberta_w					varchar2(1);
ie_urgencia_w				prescr_solucao.ie_urgencia%type;

CURSOR C01 IS
Select	a.nr_seq_material,
		a.CD_MATERIAL,
		nvl(obter_UM_w_item_prescr_sol(a.cd_protocolo, a.nr_sequencia, -1,  a.nr_seq_material, a.nr_seq_solucao, 'SOL', nm_usuario_p),a.cd_unidade_medida),
		nvl(obter_dose_w_item_prescr_sol(a.cd_protocolo, a.nr_sequencia, -1,  a.nr_seq_material, a.nr_seq_solucao, 'SOL', nm_usuario_p),a.qt_dose),
		a.QT_SOLUCAO,
		a.IE_AGRUPADOR,
		a.DS_DOSE_DIFERENCIADA,
		substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_w,'UMS'),1,30) CD_UNIDADE_MEDIDA_CONSUMO,
		b.QT_MINIMO_MULTIPLO_SOLIC,
		nvl(cd_intervalo_p, a.cd_intervalo),
		nvl(a.ie_se_necessario,'N'),
		nvl(a.ie_via_aplicacao,nvl(ie_via_aplicacao_w, upper(obter_via_usuario('IV')))),
		a.ds_recomendacao,
		nvl(a.qt_dose_peso,0),
		a.ds_justificativa,
		nvl(ie_acm,'N')
from 	material b,
		Protocolo_medic_material a
where 	a.cd_protocolo	 	= cd_protocolo_w
  and 	a.nr_sequencia		= nr_seq_medic_w
  and 	a.nr_seq_solucao		= nr_seq_sol_w
  and 	b.ie_situacao		= 'A'
  and 	a.cd_material 		= b.cd_material
  order by a.nr_seq_apresentacao;

CURSOR C02 IS
select	nr_seq_solucao
from		prescr_solucao
where	nr_prescricao	= nr_prescricao_p;

BEGIN

select	nvl(max(cd_estabelecimento),1),
		nvl(max(qt_peso),0),
		nvl(max(cd_setor_atendimento),1),
		max(nr_atendimento)
into		cd_estabelecimento_w,
		qt_peso_w,
		cd_setor_atendimento_w,
		nr_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

obter_param_usuario(924, 226, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_etapa_sol_acm_w);
obter_param_usuario(924, 560, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_arredondar_w);
obter_param_usuario(924, 935, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_limpar_ACM_w);

if	(NR_PRESCRICAO_P > 0) then
	begin
	select	nvl(max(nr_seq_solucao),0),
			nvl(max(nr_agrupamento),0)
	into		nr_sequencia_w,
			nr_agrup_solucao_w
	from		prescr_solucao
	where	nr_prescricao = NR_PRESCRICAO_P;

	Select	dt_prescricao,
			nvl(dt_inicio_prescr,dt_primeiro_horario)
	into		dt_prescricao_w,
			dt_primeiro_horario_w
	from		prescr_medica
	where	nr_prescricao = NR_PRESCRICAO_P;

	select	nvl(max(nr_sequencia),1)
	into		nr_seq_material_w
	from		prescr_material
	where	nr_prescricao = NR_PRESCRICAO_P;

	hr_primeiro_hoario_w	:= to_char(dt_primeiro_horario_w,'hh24:mi');
	end;
end if;

select	max(NR_SEQ_SOLUCAO),
		nvl(max(IE_TIPO_DOSAGEM),'gtm'),
		max(QT_DOSAGEM),
		max(QT_SOLUCAO_TOTAL),
		max(QT_TEMPO_APLICACAO),
		nvl(max(QT_VOLUME),0),
		max(NR_ETAPAS),
		max(IE_BOMBA_INFUSAO),
		nvl(max(NR_AGRUPAMENTO),0),
		nvl(max(ie_esquema_alternado),'N'),
		nvl(max(ie_calc_aut),'N'),
		nvl(max(ie_acm),'N'),
		max(qt_hora_fase),
		max(ds_solucao),
		max(cd_protocolo),
		max(nr_sequencia),
		max(nr_seq_solucao),
		max(ie_solucao_pca),
		max(ie_tipo_analgesia),
		max(ie_pca_modo_prog),
		max(qt_dose_inicial_pca),
		max(qt_vol_infusao_pca),
		max(qt_bolus_pca),
		max(qt_intervalo_bloqueio),
		max(qt_limite_quatro_hora),
		max(qt_dose_ataque),
		max(ie_tipo_sol),
		max(qt_limite_uma_hora),
		max(ie_um_dose_inicio_pca),
		max(ie_um_limite_pca),
		max(ie_um_limite_hora_pca),
		max(ie_um_fluxo_pca),
		max(ie_um_bolus_pca),
		max(ie_via_aplicacao),
		max(ie_sol_especial),
		max(ie_aberta),
		nvl(max(ie_urgencia),'N'),
		max(cd_intervalo)
into		nr_seq_solucao_w,
		ie_tipo_dosagem_w,
		qt_dosagem_w,
		qt_solucao_total_w,
		qt_tempo_aplicacao_w,
		qt_volume_w,
		nr_etapas_w,
		ie_bomba_infusao_w,
		nr_agrupamento_w,
		ie_esquema_alternado_w,
		ie_calculo_aut_w,
		ie_acm_w,
		qt_hora_fase_w,
		ds_solucao_w,
		cd_protocolo_w,
		nr_seq_medic_w,
		nr_seq_sol_w,
		ie_solucao_pca_w,
		ie_tipo_analgesia_w,
		ie_pca_modo_prog_w,
		qt_dose_inicial_pca_w,
		qt_vol_infusao_pca_w,
		qt_bolus_pca_w,
		qt_intervalo_bloqueio_w,
		qt_limite_quatro_hora_w,
		qt_dose_ataque_w,
		ie_tipo_sol_w,
		qt_limite_uma_hora_w,
		ie_um_dose_inicio_pca_w,
		ie_um_limite_pca_w,
		ie_um_limite_hora_pca_w,
		ie_um_fluxo_pca_w,
		ie_um_bolus_pca_w,
		ie_via_aplicacao_w,
		ie_solucao_especial_w,
		ie_aberta_w,
		ie_urgencia_w,
		cd_intervalo_ww
from		protocolo_medic_solucao
where		nr_seq_interno	= nr_seq_interno_p
and		nvl(ie_solucao_rotina, 'S') = 'S';

-- andre arbigaus

if	(ie_etapa_sol_acm_w = 'N') and
	(ie_acm_w = 'S') then
	ie_gerar_etapa_w := 'N';
else
	ie_gerar_etapa_w := 'S';
end if;

if	(ie_acm_w = 'S') and
	(ie_limpar_ACM_w = 'N') then
	ie_gerar_etapa_w 	:= 'N';
	ie_calculo_aut_w	:= 'N';
end if;

Select	Obter_prim_horario_Sol_regra(decode(ie_gerar_etapa_w, 'N', null, nr_etapas_w), dt_prescricao_w, Hr_primeiro_hoario_w)
into		hr_primeiro_hoario_w
from		dual;

select	Obter_se_setor_intervalo(cd_intervalo_w,cd_setor_atendimento_w,cd_estabelecimento_w,null, ie_via_aplicacao_w,obter_unid_atend_setor_atual(nr_atendimento_w,cd_setor_atendimento_w,'UB'))
into		ie_intervalo_setor_w
from		dual;

if	(ie_intervalo_setor_w = 'S') then
	hr_primeiro_hoario_w	:= obter_primeiro_horario(cd_intervalo_w,nr_prescricao_p,null,null);
end if;

if	(ie_calculo_aut_w = 'S') then
	Calcula_Horarios_Etapas(to_date(to_char(dt_primeiro_horario_w,'dd/mm/yyyy ')||hr_primeiro_hoario_w,'dd/mm/yyyy hh24:mi')
				,nr_etapas_w,dividir(qt_tempo_aplicacao_w,nr_etapas_w),nm_usuario_p,'N',qt_tempo_aplicacao_w,ds_horarios_w,ds_horarios_2w,null,'N');
end if;

if	(ie_bomba_infusao_w is null) then
	ie_bomba_infusao_w := Obter_dispositivo_solucao(cd_setor_atendimento_w, obter_perfil_ativo);
end if;

insert into prescr_solucao (
	nr_prescricao,
	nr_seq_solucao,
	ie_via_aplicacao,
	dt_atualizacao,
	nm_usuario,
	cd_unidade_medida,
	ie_tipo_dosagem,
	qt_dosagem,
	qt_solucao_total,
	qt_tempo_aplicacao,
	qt_volume,
	nr_etapas,
	ie_bomba_infusao,
	ie_suspenso,
	nr_agrupamento,
	ie_esquema_alternado,
	ie_calc_aut,
	ie_acm,
	qt_hora_fase,
	ds_horarios,
	hr_prim_horario,
	ie_urgencia,
	ds_solucao,
	ie_solucao_especial,
	ie_solucao_pca,
	ie_tipo_analgesia,
	ie_pca_modo_prog,
	qt_dose_inicial_pca,
	qt_vol_infusao_pca,
	qt_bolus_pca,
	qt_intervalo_bloqueio,
	qt_limite_quatro_hora,
	qt_dose_ataque,
	ie_tipo_sol,
	qt_limite_uma_hora,
	IE_UM_LIMITE_HORA_PCA,
	ie_um_dose_inicio_pca,
	ie_um_limite_pca,
	ie_um_fluxo_pca,
	ie_um_bolus_pca,
	ie_aberto,
	cd_intervalo)
Values	(
	NR_PRESCRICAO_P,
	nr_seq_solucao_w + nr_sequencia_w,
	nvl(ie_via_aplicacao_w,upper(obter_via_usuario('IV'))),
	sysdate,
	nm_usuario_p,
	lower(obter_unid_med_usua('ml')),
	ie_tipo_dosagem_w,
	qt_dosagem_w,
	qt_solucao_total_w,
	qt_tempo_aplicacao_w,
	qt_volume_w,
	decode(ie_gerar_etapa_w, 'N', null, nr_etapas_w),
	ie_bomba_infusao_w,
	'N',
	nr_agrupamento_w + nr_agrup_solucao_w,
	ie_esquema_alternado_w,
	ie_calculo_aut_w,
	ie_acm_w,
	decode(ie_gerar_etapa_w, 'N', null, qt_hora_fase_w),
	ds_horarios_w,
	hr_primeiro_hoario_w,
	ie_urgencia_w,
	ds_solucao_W,
	nvl(ie_solucao_especial_w,'N'),
	ie_solucao_pca_w,
	ie_tipo_analgesia_w,
	ie_pca_modo_prog_w,
	qt_dose_inicial_pca_w,
	qt_vol_infusao_pca_w,
	qt_bolus_pca_w,
	qt_intervalo_bloqueio_w,
	qt_limite_quatro_hora_w,
	qt_dose_ataque_w,
	ie_tipo_sol_w,
	qt_limite_uma_hora_w,
	nvl(ie_um_limite_hora_pca_w,'ml'),
	ie_um_dose_inicio_pca_w,
	ie_um_limite_pca_w,
	ie_um_fluxo_pca_w,
	ie_um_bolus_pca_w,
	nvl(ie_aberta_w,'N'),
	cd_intervalo_ww);

-- andre arbigaus

insert into prescr_solucao_esquema(
	nr_sequencia,
	nr_prescricao,
	nr_seq_solucao,
	dt_atualizacao,
	nm_usuario,
	qt_volume,
	qt_dosagem,
	ds_horario)
select	prescr_solucao_esquema_seq.nextval,
	nr_prescricao_p,
	nr_seq_solucao_w + nr_sequencia_w,
	sysdate,
	NM_USUARIO_P,
	qt_volume,
	qt_dosagem,
	ds_horario
from	protocolo_medic_sol_esq
where	cd_protocolo	= cd_protocolo_w
and	nr_seq_subitem	= nr_seq_medic_w
and	nr_seq_solucao	= nr_seq_sol_w;

qt_total_etapa_w	:= 0;

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_material_ww,
	CD_MATERIAL_w,
	CD_UNIDADE_MEDIDA_w,
	QT_DOSE_w,
	QT_SOLUCAO_w,
	IE_AGRUPADOR_w,
	DS_DOSE_DIFERENCIADA_w,
	cd_unid_med_consumo_w,
	QT_MINIMO_MULTIPLO_SOLIC_w,
	cd_intervalo_w,
	ie_se_necessario_w,
	ie_via_aplicacao_w,
	ds_recomendacao_w,
	qt_dose_peso_w,
	ds_justificativa_w,
	ie_acm_w;
exit when C01%notfound;
	BEGIN

	if	(qt_dose_peso_w > 0) and
		(qt_peso_w > 0) then /* Oraci em 21/09/2007 OS69262 */
		qt_dose_w:= qt_dose_peso_w * qt_peso_w;
	end if;

	if	(ie_esquema_alternado_w = 'S') then
		obter_valor_dinamico('select ('|| replace(DS_DOSE_DIFERENCIADA_w,'-','+')||') from dual ',QT_DOSE_w);
	end if;
	qt_conversao_dose_w	:= obter_conversao_unid_med(cd_material_w,cd_unidade_medida_w);
	if (nvl(qt_conversao_dose_w,0) = 0) then
		select	substr(max(obter_desc_material(cd_material_w)),1,255),
				substr(max(Obter_Unidade_Medida(cd_unidade_medida_w)),1,40)
		into	ds_material_w,
				ds_unid_med_w
		from	dual;
		--'O medicamento '||ds_material_w||chr(13)||' n�o possui valor cadastrado para  convers�o em '||ds_unid_med_w);
		Wheb_mensagem_pck.exibir_mensagem_abort(181330,'DS_MATERIAL='||ds_material_w || ';DS_UNID='||ds_unid_med_w);
	end if;
	qt_unitaria_w		:= (trunc(qt_dose_w * 1000 / qt_conversao_dose_w)/ 1000);

	select	Obter_se_setor_intervalo(cd_intervalo_w,cd_setor_atendimento_w,cd_estabelecimento_w, cd_material_w)
	into	ie_intervalo_setor_w
	from	dual;

	if	(ie_intervalo_setor_w = 'S') then
		hr_primeiro_hoario_w	:= obter_primeiro_horario(cd_intervalo_w,nr_prescricao_p,cd_material_w,ie_via_aplicacao_w);
	end if;

	/*
	Obter_Quant_Dispensar(1,cd_material_w, NR_PRESCRICAO_P, nr_seq_material_w + nr_seq_material_ww,
				cd_intervalo_w,	ie_via_aplicacao_w, qt_unitaria_w, 0, nvl(nr_etapas_w,1),
				'', '1', CD_UNIDADE_MEDIDA_w, 1, qt_material_w, qt_dispensar_w, ie_regra_disp_w, ds_erro_w);

	Dalcastagne em 19/03/2009 OS 133289 */

	Obter_Quant_Dispensar(1,cd_material_w, NR_PRESCRICAO_P, nr_seq_material_w + nr_seq_material_ww,
				cd_intervalo_w,	ie_via_aplicacao_w, qt_unitaria_w, 0, 1,
				'', '1', CD_UNIDADE_MEDIDA_w, 1, qt_material_w, qt_dispensar_w, ie_regra_disp_w, ds_erro_w,ie_se_necessario_w,ie_acm_w);

	qt_dose_ml_w		:= obter_conversao_ml(cd_material_w,qt_dose_w,cd_unidade_medida_w);
	qt_solucao_w		:= qt_dose_ml_w;
	qt_total_etapa_w	:= qt_total_etapa_w + qt_dose_ml_w;

	Insert into prescr_material(
		nr_prescricao,
		nr_sequencia,
		ie_origem_inf,
		cd_material,
		cd_unidade_medida,
		cd_unidade_medida_dose,
		qt_dose,
		qt_unitaria,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		cd_intervalo,
		nr_ocorrencia,
		qt_total_dispensar,
		nr_sequencia_solucao,
		qt_solucao,
		ie_agrupador,
		ds_dose_diferenciada,
		qt_conversao_dose,
		cd_motivo_baixa,
		ie_utiliza_kit,
		ie_via_aplicacao,
		ie_se_necessario,
		ds_observacao,
		ie_recons_diluente_fixo,
		cd_protocolo,
		nr_seq_protocolo,
		nr_seq_mat_protocolo,
		ie_regra_disp,
		ds_justificativa)
	values(
		nr_prescricao_p,
		nr_seq_material_w + nr_seq_material_ww,
		'1',
		cd_material_w,
		cd_unid_med_consumo_w,
		cd_unidade_medida_w,
		qt_dose_w,
		nvl(qt_unitaria_w,1),
		nvl(qt_material_w,1),
		sysdate,
		nm_usuario_p,
		cd_intervalo_w,
		1,
		qt_dispensar_w,
		nr_seq_solucao_w + nr_sequencia_w,
		QT_SOLUCAO_w,
		IE_AGRUPADOR_w,
		DS_DOSE_DIFERENCIADA_w,
		qt_conversao_dose_w,
		0,
		'N',
		ie_via_aplicacao_w,
		ie_se_necessario_w,
		ds_recomendacao_w,
		'N',
		cd_protocolo_w,
		nr_seq_medic_w,
		nr_seq_material_ww,
		ie_regra_disp_w,
		ds_justificativa_w);
	END;
END LOOP;
CLOSE C01;

update	prescr_solucao
set	qt_volume	= qt_total_etapa_w
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_solucao	= nr_seq_solucao_w + nr_sequencia_w;

if	(ie_calculo_aut_w = 'S') then
	begin
	update	prescr_solucao
	set	qt_volume		= qt_total_etapa_w,
		qt_solucao_total	= decode(ie_esquema_alternado_w,'S',qt_total_etapa_w,qt_total_etapa_w * nr_etapas_w),
		qt_hora_fase		= decode(ie_gerar_etapa_w, 'N', null, dividir(qt_tempo_aplicacao_w,nr_etapas_w))
	where	nr_prescricao 		= nr_prescricao_p
	and	nr_seq_solucao		= nr_seq_solucao_w + nr_sequencia_w;

	Calcular_volume_total_solucao(upper(ie_tipo_dosagem_w),qt_tempo_aplicacao_w,qt_total_etapa_w * nr_etapas_w,
					qt_dosagem_w,nr_prescricao_p,nr_seq_solucao_w + nr_sequencia_w,1,qt_dosagem_final_w,ie_arredondar_w);
	update	prescr_solucao
	set	qt_dosagem		= qt_dosagem_final_w
	where	nr_prescricao 		= nr_prescricao_p
	and	nr_seq_solucao		= nr_seq_solucao_w + nr_sequencia_w;
	end;
end if;


if	(nvl(ie_aberta_w,'N') = 'S') then
	update	prescr_solucao
	set	nr_etapas		     = 1,
		qt_tempo_aplicacao	= '',
		qt_hora_fase		= ''
	where	nr_prescricao 		= nr_prescricao_p
	and	nr_seq_solucao		= nr_seq_solucao_w + nr_sequencia_w;
end if;

Reordenar_Solucoes(nr_prescricao_p);

OPEN C02;
LOOP
FETCH C02 into
	nr_seq_solucao_ww;
exit when c02%notfound;
	Ajustar_Prescr_Mat_Solucao(nr_prescricao_p, nr_seq_solucao_ww);
END LOOP;
CLOSE C02;

COMMIT;

END Gerar_Solucao_Rotina_prescr;
/