create or replace
procedure pls_gravar_inconsist_pessoa
			(	cd_pessoa_fisica_p	Varchar2,
				cd_cgc_p		Varchar2,
				cd_inconsistencia_p	Number,
				nr_seq_tipo_doc_pf_p	Number,
				nr_seq_tipo_doc_pj_p	Number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_inconsistencia_w		Number(10);
ds_acao_usuario_w		Varchar2(4000);
ie_situacao_w			Varchar2(1);
ie_tipo_pessoa_w		Varchar2(2);
ie_pagador_w			Varchar2(1);
ie_beneficiario_w		Varchar2(1);
ie_estipulante_w		Varchar2(1);
ie_prestador_w			Varchar2(1);
ie_pessoa_fisica_w		Varchar2(1);
ie_operadoras_w			varchar2(1);
qt_pagador_w			Number(5)	:= 0;
qt_beneficiario_w		Number(5)	:= 0;
qt_estipulante_w		Number(5)	:= 0;
qt_prestador_w			Number(5)	:= 0;
qt_pessoa_fisica_w		Number(5)	:= 0;
qt_operadora_w			number(5)	:= 0;
qt_congenere_w			number(5)	:= 0;


begin
select	nr_sequencia,
	nvl(ds_acao_usuario,''),
	nvl(ie_situacao,'N'),
	nvl(ie_tipo_pessoa,'N'),
	nvl(ie_pagador,'N'),
	nvl(ie_beneficiario,'N'),
	nvl(ie_estipulante,'N'),
	nvl(ie_prestador,'N'),
	nvl(ie_pessoa_fisica,'N'),
	nvl(ie_operadora,'N')
into	nr_seq_inconsistencia_w,
	ds_acao_usuario_w,
	ie_situacao_w,
	ie_tipo_pessoa_w,
	ie_pagador_w,
	ie_beneficiario_w,
	ie_estipulante_w,
	ie_prestador_w,
	ie_pessoa_fisica_w,
	ie_operadoras_w
from	pls_cad_inconsist_pessoa
where	cd_inconsistencia	= cd_inconsistencia_p;

if	(cd_pessoa_fisica_p is not null) then
	if	(ie_tipo_pessoa_w in ('A','PF')) then
		select	count(1)
		into	qt_pagador_w
		from	pls_contrato_pagador
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	rownum = 1;

		select	count(1)
		into	qt_beneficiario_w
		from	pls_segurado
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	rownum = 1;

		select	count(1)
		into	qt_estipulante_w
		from	pls_contrato
		where	cd_pf_estipulante	= cd_pessoa_fisica_p
		and	rownum = 1;

		select	count(1)
		into	qt_prestador_w
		from	pls_prestador
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	rownum = 1;
		
		qt_pessoa_fisica_w		:= 1;
	end if;
elsif	(cd_cgc_p is not null) then
	if	(ie_tipo_pessoa_w in ('A','PJ')) then
		select	count(1)
		into	qt_pagador_w
		from	pls_contrato_pagador
		where	cd_cgc	= cd_cgc_p
		and	rownum = 1;

		select	count(1)
		into	qt_estipulante_w
		from	pls_contrato
		where	cd_cgc_estipulante	= cd_cgc_p
		and	rownum = 1;

		select	count(1)
		into	qt_prestador_w
		from	pls_prestador
		where	cd_cgc	= cd_cgc_p
		and	rownum = 1;
		
		select	count(1)
		into	qt_operadora_w
		from	pls_outorgante
		where	cd_cgc_outorgante	= cd_cgc_p
		and	rownum = 1;
		
		select	count(1)
		into	qt_congenere_w
		from	pls_congenere
		where	cd_cgc	= cd_cgc_p
		and	rownum = 1;
	end if;
end if;

if	(ie_situacao_w = 'A') and
	(((ie_pagador_w = 'S') and (qt_pagador_w > 0)) or
	((ie_beneficiario_w = 'S') and (qt_beneficiario_w > 0)) or
	((ie_estipulante_w = 'S') and (qt_estipulante_w > 0)) or
	((ie_prestador_w = 'S') and (qt_prestador_w > 0)) or
	((ie_pessoa_fisica_w = 'S') and (qt_pessoa_fisica_w > 0)) or
	((ie_operadoras_w = 'S') and ((qt_operadora_w > 0) or (qt_congenere_w > 0)))) then

	insert into pls_inconsistencia_pessoa
		(nr_sequencia, nr_seq_inconsistencia, dt_atualizacao, 
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		ds_acao_usuario, cd_pessoa_fisica, cd_cgc,
		nr_seq_tipo_doc, nr_seq_pls_tipo_doc)
	values(	pls_inconsistencia_pessoa_seq.nextval, nr_seq_inconsistencia_w, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		ds_acao_usuario_w, cd_pessoa_fisica_p, cd_cgc_p,
		nr_seq_tipo_doc_pf_p, nr_seq_tipo_doc_pj_p);
	commit;
end if;

end pls_gravar_inconsist_pessoa;
/