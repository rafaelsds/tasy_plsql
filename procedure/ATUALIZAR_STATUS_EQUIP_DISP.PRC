create or replace
procedure atualizar_status_equip_disp(	nr_seq_equipamento_p	number,
											ie_status_p			varchar2,
											ds_motivo_p			varchar2,
											nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

if	(nr_seq_equipamento_p is not null) and
	(ie_status_p is not null) then
	insert	into	equip_disp_elet_status
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_equipamento,
		dt_status,
		ie_status,
		ds_motivo)
	values
		(equip_disp_elet_status_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_equipamento_p,
		sysdate,
		ie_status_p,
		ds_motivo_p);
		
		commit;

end if;

end atualizar_status_equip_disp;
/
