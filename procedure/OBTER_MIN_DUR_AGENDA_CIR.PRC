create or replace
procedure obter_min_dur_agenda_cir	(	nr_seq_agenda_p		number,
						cd_perfil_p		number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number,
						qt_minutos_p		out number,
						ds_mensagem_p		out varchar) is

/* variaveis */
dt_agenda_w		date;
cd_medico_exec_w	varchar2(10);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_minuto_duracao_w	number(10,0);
cd_agenda_w		number(10,0);	

qt_min_proc_w		number(10,0) 	:= 0;
qt_min_agenda_w		number(10,0) 	:= 0;
qt_min_agenda_reaj_w	number(10,0) 	:= 0;
hr_inicio_w		date;

ie_param_sobreposicao_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_proc_adic_w	varchar2(10);
hr_prox_agenda_w	date;
ie_atualiza_tempo_agenda_w		varchar2(1);
ie_atualiza_tempo_proc_w		varchar2(1); 
ie_modo_sobreposicao_w	varchar2(1);
cd_estab_agenda_w	number(4);
ie_estab_agenda_w	varchar2(1) := 'S';
ds_lista_status_w varchar2(255)	:= null;
ie_atual_maior_tempo_proc_w		varchar2(1); 

cursor c01 is
select	cd_agenda,
	hr_inicio,
	cd_medico,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	nr_minuto_duracao,
	cd_pessoa_fisica
	cd_pessoa_fisica
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p
and	nr_seq_proc_interno is not null;

cursor c02 is
select	cd_medico,
	nr_seq_proc_interno,
	qt_min_proc
from	agenda_paciente_proc
where	nr_sequencia = nr_seq_agenda_p;

begin

obter_param_usuario(871, 162, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_agenda_w);
obter_param_usuario(871, 163, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_proc_w);
obter_param_usuario(871, 642, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atual_maior_tempo_proc_w);
obter_param_usuario(871, 774, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_estab_agenda_w);

if	(wheb_usuario_pck.get_cd_funcao = 871) then /*s� ir� buscar o parametro se estiver na fun��o Gest�o da Agenda cirurgica*/
	Obter_Param_Usuario(871, 820, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ds_lista_status_w);	
end if;

if	(cd_estabelecimento_p is not null) and
	(nr_seq_agenda_p is not null) then
	
	select	nvl(a.ie_modo_sobreposicao,'R'),
		a.cd_estabelecimento
	into	ie_modo_sobreposicao_w,
		cd_estab_agenda_w
	from	agenda a,
		agenda_paciente b
	where	a.cd_agenda 	= b.cd_agenda
	and	b.nr_sequencia 	= nr_seq_agenda_p;

	select	obter_parametro_agenda(cd_estabelecimento_p, 'IE_CONSISTE_DURACAO', 'N')
	into	ie_param_sobreposicao_w
	from	dual;	
	
	open c01;
	loop
	fetch c01 into	cd_agenda_w,
			dt_agenda_w,
			cd_medico_exec_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_minuto_duracao_w,
			cd_pessoa_fisica_w;
	exit when c01%notfound;
		begin
		
		if	(ie_atualiza_tempo_agenda_w in ('S','Q')) then		

			select	obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,decode(ie_estab_agenda_w,'S',cd_estab_agenda_w,0))
			into	qt_min_proc_w
			from	dual;
			
			qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
			
		end if;
			
		end;
	end loop;
	close c01;
	
	if	(ie_atualiza_tempo_proc_w = 'S') then
		open c02;
		loop
		fetch c02 into	cd_medico_proc_adic_w,
				nr_seq_proc_interno_w,
				qt_min_proc_w;
		exit when c02%notfound;
			begin
			if	(nvl(qt_min_proc_w,0) = 0) then
				if	(ie_estab_agenda_w = 'S') then
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,cd_estab_agenda_w);
				else
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,0);
				end if;
			end if;
			
			if (ie_atual_maior_tempo_proc_w = 'S') then
				if (qt_min_proc_w > qt_min_agenda_w) then
					 qt_min_agenda_w := qt_min_proc_w;
				end if;
			else
				qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
			end if;
			
			
			end;
		end loop;
		close c02;
	end if;
	
	if	(qt_min_agenda_w > 0) and (qt_min_agenda_w < nr_minuto_duracao_w) then
		qt_min_agenda_w := nr_minuto_duracao_w;
	end if;	
	
	
	if	(ie_modo_sobreposicao_w = 'D') and (qt_min_agenda_w	> 0) then
		select	cd_agenda,
			hr_inicio
		into	cd_agenda_w,
			hr_inicio_w	
		from	agenda_paciente
		where	nr_sequencia = nr_seq_agenda_p;
		
		if (nvl(ds_lista_status_w,'XPTO') = 'XPTO') then
			select	min(hr_inicio)
			into	hr_prox_agenda_w
			from	agenda_paciente
			where	cd_agenda = cd_agenda_w
			and	dt_agenda = trunc(hr_inicio_w,'dd')
			and	hr_inicio > hr_inicio_w
			and	ie_status_agenda not in ('C','L','II');
		else
			select	min(hr_inicio)
			into	hr_prox_agenda_w
			from	agenda_paciente
			where	cd_agenda = cd_agenda_w
			and	dt_agenda = trunc(hr_inicio_w,'dd')
			and	hr_inicio > hr_inicio_w
			and obter_se_contido_char(ie_status_agenda,ds_lista_status_w || ',L') = 'N';
		end if;
		
		if	((hr_inicio_w + qt_min_agenda_w / 1440) > hr_prox_agenda_w) then
			qt_min_agenda_reaj_w	:= 	Obter_Min_Entre_Datas(hr_inicio_w,hr_prox_agenda_w,null);
			ds_mensagem_p		:= 	wheb_mensagem_pck.get_texto(308661, 'QT_MIN_AGENDA_W=' || qt_min_agenda_w || ';' ||
																		'QT_MIN_AGENDA_REAJ_W=' || qt_min_agenda_reaj_w);
								/*
									Este agendamento possui um tempo de dura��o superior que ultrapassa o hor�rio do pr�ximo agendamento!
									Tempo previsto pelo m�dico/procedimento/estabelecimento: #@QT_MIN_AGENDA_W#@
									Tempo at� o pr�ximo agendamento: #@QT_MIN_AGENDA_REAJ_W#@
									Deseja continuar?
								*/
			qt_min_agenda_w		:= 	qt_min_agenda_reaj_w;	
		else
			ds_mensagem_p	:= wheb_mensagem_pck.get_texto(308660, 'QT=' || qt_min_agenda_w);
								-- Confirma a atualiza��o da dura��o do agendamento para #@QT#@ minutos?
		end if;	
	elsif	(qt_min_agenda_w > 0) then
		ds_mensagem_p	:= wheb_mensagem_pck.get_texto(308660, 'QT=' || qt_min_agenda_w);
							-- Confirma a atualiza��o da dura��o do agendamento para #@QT#@ minutos?
	end if;	
	qt_minutos_p 	:= qt_min_agenda_w;
end if;

end obter_min_dur_agenda_cir;
/