create or replace
procedure diops_cad_gerar_emp_depend
			(	nr_seq_operadora_p	number,
				nr_seq_periodo_p	number,
				nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar as informações cadastrais do DIOPS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
-------------------------------------------------------------------------------------------------------------------
Referências:
	PLS_GERAR_DIOPS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_email_um_w			varchar2(255);
ds_email_dois_w			varchar2(255);
ds_razao_social_w		varchar2(80);
ds_tipo_empresa_w		varchar2(50);
cd_cgc_w			varchar2(14);
ie_tipo_empresa_w		varchar2(2);
cd_estabelecimento_w		number(4);
	
Cursor C01 is
	select	ie_tipo_empresa,
		cd_cgc,
		substr(pls_diops_caracteres_nome(obter_razao_social(cd_cgc)),1,80),
		substr(obter_dados_pf_pj('', cd_cgc, 'M'),1,255)
	from	pls_empresa_dependente
	where	nr_seq_operadora	= nr_seq_operadora_p;

begin
begin /* Obter dados da operadora */
select	cd_estabelecimento
into	cd_estabelecimento_w
from	pls_outorgante
where	nr_sequencia	= nr_seq_operadora_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(243450, 'NR_SEQ_OPERADORA=' || nr_seq_operadora_p);
end;

open C01;
loop
fetch C01 into	
	ie_tipo_empresa_w,
	cd_cgc_w,
	ds_razao_social_w,
	ds_email_um_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_empresa_w = 'F') then
		ds_tipo_empresa_w	:= 'Filial';
	elsif	(ie_tipo_empresa_w = 'S') then
		ds_tipo_empresa_w	:= 'Sucursal';
	elsif	(ie_tipo_empresa_w = 'S') then
		ds_tipo_empresa_w	:= 'Outra';
	end if;
	insert into w_diops_cad_emp_depend
		(nr_sequencia,
		nr_seq_operadora,
		ie_tipo_empresa,
		cd_cgc,
		ds_razao_social,
		ds_email_um,
		ds_email_dois,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_tipo_empresa,
		nr_seq_periodo)
	values	(w_diops_cad_emp_depend_seq.nextval,
		nr_seq_operadora_p,
		ie_tipo_empresa_w,
		cd_cgc_w,
		ds_razao_social_w,
		ds_email_um_w,
		ds_email_dois_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_tipo_empresa_w,
		nr_seq_periodo_p);
		
	diops_cadastral_gravar_end(nr_seq_operadora_p, nr_seq_periodo_p, '', cd_cgc_w, 'E', cd_estabelecimento_w, nm_usuario_p);
	
	diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, '', cd_cgc_w, 'TE', cd_estabelecimento_w, nm_usuario_p);
	
	diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, '', cd_cgc_w, 'FE', cd_estabelecimento_w, nm_usuario_p);	
	end;
end loop;
close C01;

end diops_cad_gerar_emp_depend;
/