create or replace
procedure vincular_senha_agendamento(
			nr_seq_senha_p		number,
			nr_seq_agenda_p		number,
			ie_tipo_agenda_p		varchar2) is 


cd_pessoa_fisica_w			varchar2(60);	
ie_status_padrao_w			varchar2(15) := null;
		
begin

if	(ie_tipo_agenda_p = 'E') then
	begin
	if	(nr_seq_agenda_p is not null) then
		begin
				
		select	CD_PESSOA_FISICA
		into	cd_pessoa_fisica_w
		from	agenda_paciente
		where	nr_sequencia		= nr_seq_agenda_p;	
		
		
		if	(cd_pessoa_fisica_w is not null) then
			begin
			update	agenda_paciente
			set		nr_seq_pac_senha_fila	= 	nr_seq_senha_p
			where	nr_sequencia		= nr_seq_agenda_p
			and		cd_pessoa_fisica	=	cd_pessoa_fisica_w;
			
			if	(obter_funcao_ativa = 871) then
				obter_param_usuario(871,778,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_status_padrao_w);
				if	(ie_status_padrao_w is  not null) then
					update	agenda_paciente
					set		ie_status_agenda	= ie_status_padrao_w
					where	nr_sequencia		= nr_seq_agenda_p
					and		cd_pessoa_fisica	=cd_pessoa_fisica_w;
				end if;
			end if;
			
			end;
		end if;
		end;
	end if;
	end;
elsif	(ie_tipo_agenda_p = 'S') then
	begin
	if	(nr_seq_agenda_p is not null) then
		begin
		select	CD_PESSOA_FISICA
		into	cd_pessoa_fisica_w
		from	agenda_consulta
		where	nr_sequencia		= nr_seq_agenda_p;	
		
		
		if	(cd_pessoa_fisica_w is not null) then
			begin
			update	agenda_consulta
			set	nr_seq_pac_senha_fila	= 	nr_seq_senha_p
			where	nr_sequencia		= nr_seq_agenda_p
			and	cd_pessoa_fisica	=	cd_pessoa_fisica_w;
			end;
		end if;
		end;
	end if;	
	end;
end if;
commit;

end vincular_senha_agendamento;
/