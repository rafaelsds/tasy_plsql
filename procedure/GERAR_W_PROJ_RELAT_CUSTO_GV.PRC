create or replace
procedure gerar_w_proj_relat_custo_gv(	nm_usuario_p	varchar2,
				dt_ano_p		varchar2,
				nr_seq_projeto_p	number) is

nr_seq_viagem_w		number(10);

cursor	c01 is
select	distinct b.nr_sequencia
from	via_destino a,
	via_viagem b
where	b.nr_sequencia = a.nr_seq_viagem
and	a.nr_seq_proj is not null
and	b.IE_ETAPA_VIAGEM <> 7
and	trunc(to_date('01/01/' || dt_ano_p ,'dd/mm/yyyy'),'yyyy') between trunc(b.dt_saida_prev,'yyyy') and trunc(b.dt_retorno_prev,'yyyy');

begin
delete
from	w_proj_relat_custo_via
where	nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into
	nr_seq_viagem_w;
exit when C01%notfound;
	begin
	insert	into w_proj_relat_custo_via (
		nr_seq_viagem,
		nr_seq_proj,
		cd_setor,
		nr_classif,
		ie_tipo_recurso,
		ie_tipo_despesa,
		ie_resp_custo,
		nm_usuario,
		vl_mes01,
		vl_mes02,
		vl_mes03,
		vl_mes04,
		vl_mes05,
		vl_mes06,
		vl_mes07,
		vl_mes08,
		vl_mes09,
		vl_mes10,
		vl_mes11,
		vl_mes12,
		vl_total)
	(select	nr_seq_viagem_w,
		proj_obter_proj_via(nr_seq_viagem_w),
		proj_obter_setor_atend_via(nr_seq_viagem_w),
		proj_obter_classif_proj(nr_seq_viagem_w),
		proj_obter_tipo_recurso(nr_seq_viagem_w),
		'D',
		'N',
		nm_usuario_p,
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/01/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/02/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/02/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/03/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/03/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/04/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/04/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/05/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/05/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/06/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/06/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/07/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/07/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/08/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/08/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/09/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/09/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/10/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/10/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/11/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/11/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/12/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'N','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'N','D')
		from	dual
		where	proj_obter_proj_via(nr_seq_viagem_w) > 0);

	insert	into w_proj_relat_custo_via(	
		nr_seq_viagem,
		nr_seq_proj,
		cd_setor,
		nr_classif,
		ie_tipo_recurso,
		ie_tipo_despesa,
		ie_resp_custo,
		nm_usuario,
		vl_mes01,
		vl_mes02,
		vl_mes03,
		vl_mes04,
		vl_mes05,
		vl_mes06,
		vl_mes07,
		vl_mes08,
		vl_mes09,
		vl_mes10,
		vl_mes11,
		vl_mes12,
		vl_total)
	(select	nr_seq_viagem_w,
		proj_obter_proj_via(nr_seq_viagem_w),
		proj_obter_setor_atend_via(nr_seq_viagem_w),
		proj_obter_classif_proj(nr_seq_viagem_w),
		proj_obter_tipo_recurso(nr_seq_viagem_w),
		'D',
		'T',
		nm_usuario_p,
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/01/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/02/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/02/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/03/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/03/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/04/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/04/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/05/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/05/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/06/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/06/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/07/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/07/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/08/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/08/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/09/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/09/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/10/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/10/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/11/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/11/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/12/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'T','D'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'T','D')
		from	dual
		where	proj_obter_proj_via(nr_seq_viagem_w) > 0);

	insert	into w_proj_relat_custo_via (
		nr_seq_viagem,
		nr_seq_proj,
		cd_setor,
		nr_classif,
		ie_tipo_recurso,
		ie_tipo_despesa,
		ie_resp_custo,
		nm_usuario,
		vl_mes01,
		vl_mes02,
		vl_mes03,
		vl_mes04,
		vl_mes05,
		vl_mes06,
		vl_mes07,
		vl_mes08,
		vl_mes09,
		vl_mes10,
		vl_mes11,
		vl_mes12,
		vl_total)
	(select	nr_seq_viagem_w,
		proj_obter_proj_via(nr_seq_viagem_w),
		proj_obter_setor_atend_via(nr_seq_viagem_w),
		proj_obter_classif_proj(nr_seq_viagem_w),
		proj_obter_tipo_recurso(nr_seq_viagem_w),
		'T',
		'N',
		nm_usuario_p,
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/01/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/02/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/02/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/03/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/03/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/04/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/04/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/05/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/05/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/06/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/06/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/07/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/07/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/08/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/08/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/09/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/09/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/10/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/10/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/11/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/11/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/12/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'N','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'N','T')
		from	dual
		where	proj_obter_proj_via(nr_seq_viagem_w) > 0);

	insert	into w_proj_relat_custo_via(	
		nr_seq_viagem,
		nr_seq_proj,
		cd_setor,
		nr_classif,
		ie_tipo_recurso,
		ie_tipo_despesa,
		ie_resp_custo,
		nm_usuario,
		vl_mes01,
		vl_mes02,
		vl_mes03,
		vl_mes04,
		vl_mes05,
		vl_mes06,
		vl_mes07,
		vl_mes08,
		vl_mes09,
		vl_mes10,
		vl_mes11,
		vl_mes12,
		vl_total)
	(select	nr_seq_viagem_w,
		proj_obter_proj_via(nr_seq_viagem_w),
		proj_obter_setor_atend_via(nr_seq_viagem_w),
		proj_obter_classif_proj(nr_seq_viagem_w),
		proj_obter_tipo_recurso(nr_seq_viagem_w),
		'T',
		'T',
		nm_usuario_p,
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/01/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/02/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/02/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/03/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/03/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/04/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/04/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/05/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/05/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/06/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/06/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/07/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/07/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/08/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/08/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/09/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/09/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/10/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/10/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/11/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/11/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/12/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'T','T'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'T','T')
		from	dual
		where	proj_obter_proj_via(nr_seq_viagem_w) > 0);

	insert	into w_proj_relat_custo_via (
		nr_seq_viagem,
		nr_seq_proj,
		cd_setor,
		nr_classif,
		ie_tipo_recurso,
		ie_tipo_despesa,
		ie_resp_custo,
		nm_usuario,
		vl_mes01,
		vl_mes02,
		vl_mes03,
		vl_mes04,
		vl_mes05,
		vl_mes06,
		vl_mes07,
		vl_mes08,
		vl_mes09,
		vl_mes10,
		vl_mes11,
		vl_mes12,
		vl_total)
	(select	nr_seq_viagem_w,
		proj_obter_proj_via(nr_seq_viagem_w),
		proj_obter_setor_atend_via(nr_seq_viagem_w),
		proj_obter_classif_proj(nr_seq_viagem_w),
		proj_obter_tipo_recurso(nr_seq_viagem_w),
		'H',
		'N',
		nm_usuario_p,
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/01/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/02/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/02/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/03/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/03/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/04/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/04/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/05/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/05/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/06/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/06/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/07/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/07/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/08/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/08/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/09/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/09/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/10/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/10/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/11/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/11/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/12/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'N','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'N','H')
		from	dual
		where	proj_obter_proj_via(nr_seq_viagem_w) > 0);

	insert	into w_proj_relat_custo_via(	
		nr_seq_viagem,
		nr_seq_proj,
		cd_setor,
		nr_classif,
		ie_tipo_recurso,
		ie_tipo_despesa,
		ie_resp_custo,
		nm_usuario,
		vl_mes01,
		vl_mes02,
		vl_mes03,
		vl_mes04,
		vl_mes05,
		vl_mes06,
		vl_mes07,
		vl_mes08,
		vl_mes09,
		vl_mes10,
		vl_mes11,
		vl_mes12,
		vl_total)
	(select	nr_seq_viagem_w,
		proj_obter_proj_via(nr_seq_viagem_w),
		proj_obter_setor_atend_via(nr_seq_viagem_w),
		proj_obter_classif_proj(nr_seq_viagem_w),
		proj_obter_tipo_recurso(nr_seq_viagem_w),
		'H',
		'T',
		nm_usuario_p,
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/01/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/02/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/02/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/03/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/03/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/04/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/04/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/05/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/05/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/06/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/06/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/07/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/07/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/08/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/08/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/09/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/09/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/10/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/10/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/11/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/11/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/12/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'T','H'),
		proj_obter_tot_via_geral(nr_seq_viagem_w,to_date('01/01/' || dt_ano_p,'dd/mm/yyyy'),fim_mes(to_date('01/12/' || dt_ano_p,'dd/mm/yyyy')),'T','H')
		from	dual
		where	proj_obter_proj_via(nr_seq_viagem_w) > 0);
	end;
end loop;
close C01;

commit;

end gerar_w_proj_relat_custo_gv;
/
