create or replace
procedure consistir_hor_ausencia(	dt_agenda_p		date,
					nr_minuto_duracao_p	number,
					ds_erro_p		out varchar2,
					nm_usuario_ausente_p	varchar2 default null) is

ie_existe_w	Number(10);
					
begin

SELECT  COUNT(*)
into	ie_existe_w
FROM    ausencia_tasy
WHERE   ((dt_agenda_p BETWEEN dt_inicio AND dt_fim) OR
	(dt_agenda_p + (nr_minuto_duracao_p / 1440)  - (1/86400) BETWEEN dt_inicio AND dt_fim) OR 
	((dt_agenda_p < dt_inicio) AND (dt_agenda_p + (nr_minuto_duracao_p / 1440) - (1/86400) > dt_fim)))
and	nm_usuario_ausente = nm_usuario_ausente_p;


if	(ie_existe_w > 0) then
	ds_erro_p :=	wheb_mensagem_pck.get_texto(278506);
			
end if;

end consistir_hor_ausencia;
/