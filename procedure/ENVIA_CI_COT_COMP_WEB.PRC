create or replace
procedure envia_ci_cot_comp_web(	nr_cot_compra_p		number,	
				cd_material_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_regra_w			number(10);
nm_usuarios_adic_w		varchar2(255);

cd_setor_regra_usuario_w		number(5);
ds_perfil_adicional_w		varchar2(4000) := '';
ds_setor_adicional_w		varchar2(2000) := '';
cd_perfil_usuario_w			number(5);
ds_material_w			varchar2(255);

ds_titulo_w			varchar2(80);
ds_comunicado_w			varchar2(4000);
cd_perfil_w			varchar2(10);
ie_ci_lida_w			varchar2(1);
nm_usuario_destino_w		varchar2(255);
nr_seq_classif_w			number(10);
nr_seq_comunic_w			number(10);
nr_atendimento_w			material_atend_paciente.nr_atendimento%type;
nr_seq_autorizacao_w		autorizacao_cirurgia.nr_sequencia%type;
nm_paciente_w			pessoa_fisica.nm_pessoa_fisica%type;
cd_cnpj_cadastrado_w		material.cd_cnpj_cadastro%type;
ds_razao_social_w		pessoa_juridica.ds_razao_social%type;

Cursor C01 is
	select	b.nr_sequencia,
		b.nm_usuarios_adic
	from	regra_envio_comunic_compra a,
		regra_envio_comunic_evento b
	where	a.nr_sequencia = b.nr_seq_regra
	and	a.cd_funcao = 915
	and	b.cd_evento = 77
	and	b.ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_p
	and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,null,'CC',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';

Cursor C02 is
	select	nvl(a.cd_setor_atendimento,0),
		nvl(a.cd_perfil,0)
	from	regra_envio_comunic_usu a
	where	a.nr_seq_evento = nr_seq_regra_w;

begin

open C01;
loop
fetch C01 into
	nr_seq_regra_w,
	nm_usuarios_adic_w;
exit when C01%notfound;
	begin

	open C02;
	loop
	fetch C02 into
		cd_setor_regra_usuario_w,
		ds_perfil_adicional_w;
	exit when C02%notfound;
		begin

		if	(cd_setor_regra_usuario_w <> 0) and
			(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
			ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
		end if;
		if	(cd_perfil_usuario_w <> 0) and
			(obter_se_contido_char(cd_perfil_usuario_w, ds_perfil_adicional_w) = 'N') then
			ds_perfil_adicional_w := substr(ds_perfil_adicional_w || cd_perfil_usuario_w || ',',1,4000);
		end if;
		end;
	end loop;
	close C02;
	
	begin
	select	max(a.nr_atendimento),
		max(a.nr_sequencia),
		substr(max(Obter_Dados_Atendimento(a.nr_atendimento,'NP')),1,255) nm_paciente
	into	nr_atendimento_w,
		nr_seq_autorizacao_w,
		nm_paciente_w
	from	autorizacao_cirurgia a,
		material_autor_cirurgia b
	where	a.nr_sequencia = b.nr_seq_autorizacao
	and	b.nr_cot_compra = nr_cot_compra_p
	and	b.cd_material = cd_material_p;
	exception
	when others then
		nm_paciente_w := '';
	end;

	select	cd_cnpj_cadastro,
		ds_material
	into	cd_cnpj_cadastrado_w,
		ds_material_w
	from	material
	where	cd_material = cd_material_p;

	select	nvl(max(ds_razao_social),'')
	into	ds_razao_social_w
	from	pessoa_juridica
	where	cd_cgc = cd_cnpj_cadastrado_w;
	
	select	max(substr(ds_titulo,1,80)),
		max(substr(
			replace_macro(
			replace_macro(
			replace_macro(
			replace_macro(
			replace_macro(
			replace_macro(ds_comunicacao,
					'@cd_material',cd_material_p),
					'@nr_atendimento',nr_atendimento_w),
					'@nr_seq_autorizacao',nr_seq_autorizacao_w),
					'@nm_paciente',nm_paciente_w),
					'@ds_material',ds_material_w),
					'@fornecedor', cd_cnpj_cadastrado_w || ds_razao_social_w),1,4000)) ds_comunicacao,
					
		max(cd_perfil)
	into	ds_titulo_w,
		ds_comunicado_w,
		cd_perfil_w
	from	regra_envio_comunic_evento
	where	nr_sequencia = nr_seq_regra_w;

	select	nvl(ie_ci_lida,'N')
	into	ie_ci_lida_w
	from 	regra_envio_comunic_evento
	where 	nr_sequencia = nr_seq_regra_w;

	nm_usuario_destino_w := nm_usuarios_adic_w;

	if	(nm_usuario_destino_w is not null) then

		select	obter_classif_comunic('F')
		into	nr_seq_classif_w
		from	dual;

		select	comunic_interna_seq.nextval
		into	nr_seq_comunic_w
		from	dual;

		if	(cd_perfil_w is not null) then
			cd_perfil_w := cd_perfil_w ||',';
		end if;

		insert into comunic_interna(
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			dt_liberacao,
			nr_seq_resultado,
			ds_perfil_adicional,
			ds_setor_adicional)
		values(	sysdate,
			ds_titulo_w,
			ds_comunicado_w,
			nm_usuario_p,
			sysdate,
			'N',
			nm_usuario_destino_w,
			nr_seq_comunic_w,
			'N',
			nr_seq_classif_w,
			sysdate,
			null,
			cd_perfil_w,
			ds_setor_adicional_w);

		if	(ie_ci_lida_w = 'S') then
			insert into comunic_interna_lida(nr_sequencia,nm_usuario,dt_atualizacao)values(nr_seq_comunic_w,nm_usuario_p,sysdate);
		end if;

	end if;
	end;
end loop;
close C01;

commit;

end envia_ci_cot_comp_web;
/