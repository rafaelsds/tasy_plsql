create or replace
procedure pls_liberar_lanc_automatico
			(	nr_seq_lancamento_p	Number,
				nm_usuario_p		Varchar2) is 

begin

update	pls_lancamento_mensalidade
set	dt_liberacao	= sysdate,
	nm_usuario_liberacao	= nm_usuario_p
where	nr_sequencia	= nr_seq_lancamento_p;

commit;

end pls_liberar_lanc_automatico;
/