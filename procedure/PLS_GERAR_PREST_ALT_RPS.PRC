create or replace
procedure pls_gerar_prest_alt_rps(	nr_seq_lote_p		pls_lote_rps.nr_sequencia%type,
					dt_alt_inicial_p	pls_lote_rps.dt_alt_inicial%type,
					dt_alt_final_p		pls_lote_rps.dt_alt_final%type,
					nr_seq_solicitacao_p	pls_rps_solicitacao.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: N�O PODE TER COMMIT;

Altera��es:
-------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
dt_exclusao_w			date;
dt_inicio_servico_w		date;
dt_inicio_contrato_w		date;
dt_fim_contrato_w		date;
cd_classificacao_w  		varchar2(1);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
sg_uf_w				varchar2(2);
ie_tipo_relacao_w		varchar2(1);
ie_disp_serv_prest_w		varchar2(3);
ie_urgencia_emergencia_w 	varchar2(1);
ie_ptu_a400_w			varchar2(1);			
nr_seq_tipo_prestador_w		number(10);
cd_cpf_cnpj_w			varchar2(14);
ds_razao_social_w		varchar2(60);
ie_tipo_contratualizacao_w	varchar2(1);
ie_relacao_operadora_w		varchar2(1);
ie_disponibilidade_serv_w	varchar2(1);
cd_cnes_w			varchar2(7);
cd_municipio_ibge_w		varchar2(6);
dt_lote_w			date;
dt_alt_inicial_w		date;
dt_alt_final_w			date;
nr_registro_plano_w		varchar2(20);
cd_plano_w			varchar2(10);	
nr_seq_plano_w			number(10);
ie_regulamentacao_w		varchar2(2);
qt_registro_w			number(10);
qt_vida_ativa_w			number(10);
nr_seq_rps_prest_w		number(10);
nr_seq_alt_prest_w		pls_alt_prest_rps.nr_sequencia%type;
ie_insere_plano_w		varchar2(1);
cd_cgc_cpf_old_w		pls_alt_prest_rps.cd_cgc_cpf_old%type;
cd_municipio_ibge_old_w		pls_alt_prest_rps.cd_municipio_ibge_old%type;
cd_cnes_old_w			pls_alt_prest_rps.cd_cnes_old%type;
ie_processa_w			varchar2(1);
ie_A400_lote_w			pls_lote_rps.ie_ptu_a400%type;

cursor c01 is

	select	distinct a.nr_seq_prestador,
		a.nr_sequencia	
	from	pls_alt_prest_rps a
	where	a.dt_atualizacao between trunc(dt_alt_inicial_p,'dd') and fim_dia(dt_alt_final_p)
	and not exists(	select	1
			from	pls_rps_prest_alt x
			where	x.nr_seq_alt_prest = a.nr_sequencia); 
			
cursor c02 is
	select	c.nr_protocolo_ans,
		substr(c.cd_scpa, 1, 10),
		c.nr_sequencia,
		c.ie_regulamentacao
	from	pls_plano_rede_atend	b,
		pls_plano		c,
		pls_prestador_plano	d
	where	c.nr_sequencia		= b.nr_seq_plano	
	and	c.nr_sequencia		= d.nr_seq_plano
	and	d.nr_seq_prestador	= nr_seq_prestador_w
	and	(dt_lote_w between nvl(d.dt_inicio_vigencia, dt_lote_w) and nvl(d.dt_fim_vigencia, dt_lote_w))
	and	substr(pls_obter_se_prest_rede_atend(nr_seq_prestador_w,b.nr_seq_rede),1,255) = 'S'
	group by c.nr_protocolo_ans,
		substr(c.cd_scpa, 1, 10),
		c.nr_sequencia,
		c.ie_regulamentacao;

begin

select	nvl(max(a.ie_ptu_a400),'N')
into	ie_A400_lote_w
from	pls_lote_rps	a
where	a.nr_sequencia	= nr_seq_lote_p;

open c01;
loop
fetch c01 into
	--cursor 1 trazer a sequencia dos prestadores que sofreram altera��es
	nr_seq_prestador_w,
	nr_seq_alt_prest_w;
exit when c01%notfound;
	begin
	ie_insere_plano_w := 'N';
	
	if ( nr_seq_prestador_w is not null ) then
		-- pego as informa��es do prestador alterado

		select	trunc(a.dt_exclusao, 'dd') dt_exclusao,
			trunc(a.dt_inicio_servico, 'dd') dt_inicio_servico,
			trunc(a.dt_inicio_contrato, 'dd') dt_inicio_contrato,
			trunc(a.dt_fim_contrato, 'dd') dt_fim_contrato,
			a.ie_tipo_classif_ptu,
			a.cd_cgc,
			a.cd_pessoa_fisica,
			a.sg_uf_sip,
			a.ie_tipo_relacao,
			a.ie_disponibilidade_serv,
			a.ie_urgencia_emergencia,
			a.ie_ptu_a400,
			a.nr_seq_tipo_prestador
		into
			dt_exclusao_w,
			dt_inicio_servico_w,
			dt_inicio_contrato_w,
			dt_fim_contrato_w,
			cd_classificacao_w,
			cd_cgc_w,
			cd_pessoa_fisica_w,
			sg_uf_w,
			ie_tipo_relacao_w,
			ie_disp_serv_prest_w,
			ie_urgencia_emergencia_w,
			ie_ptu_a400_w,
			nr_seq_tipo_prestador_w
		from	pls_prestador	a
		where	a.nr_sequencia = nr_seq_prestador_w;
		
		--Faz as valida��es iniciais que indicam se o prestador deve continuar o processo ou n�o
		-- Inicializa sempre com o padr�o 'S' - Sim
		ie_processa_w := 'S';
		
		-- Verifica a regra do lote sobre A400
		if	((nvl(ie_A400_lote_w, 'N') = 'S') and (nvl(ie_ptu_a400_w, 'N') = 'N')) then
		
			ie_processa_w := 'N';
		end if;

		if	(nvl(ie_processa_w, 'S') = 'S') then
		
			if	(cd_cgc_w is not null) then
				cd_cpf_cnpj_w		:= elimina_caractere_especial(cd_cgc_w);
				ds_razao_social_w	:= substr(elimina_caractere_especial(obter_dados_pf_pj(null, cd_cgc_w, 'N')), 1, 60);
			else
				cd_cpf_cnpj_w		:= substr(elimina_caractere_especial(obter_dados_pf(cd_pessoa_fisica_w, 'CPF')), 1, 11);
				ds_razao_social_w	:= substr(elimina_caractere_especial(obter_nome_pf(cd_pessoa_fisica_w)), 1, 60);
			end if;
			
			if	(ie_tipo_relacao_w not in ('P','C')) then
				if	(ie_tipo_relacao_w <> 'I') then
					ie_tipo_contratualizacao_w	:= 'D';
				else
					ie_tipo_contratualizacao_w	:= 'I';
				end if;
				
				ie_relacao_operadora_w	:= 'C';
			else
				ie_tipo_contratualizacao_w	:= null;
				ie_relacao_operadora_w		:= 'P';
			end if;

			select	decode(ie_disp_serv_prest_w, '1', 'P', 'T')
			into	ie_disponibilidade_serv_w
			from	dual;

			begin
			cd_cnes_w		:= lpad(substr(pls_obter_cnes_prestador(nr_seq_prestador_w), 1, 7),7,'0');
			exception
			when others then
				cd_cnes_w := null;
			end;

			cd_municipio_ibge_w	:= lpad(substr(elimina_caractere_especial(pls_obter_dados_prest_end(	nr_seq_prestador_w, null, null,'MI')), 1, 6), 6, '0');
			
			select	nvl(max(nr_sequencia), 0)
			into	nr_seq_rps_prest_w
			from	pls_rps_prestador
			where	nr_seq_lote = nr_seq_lote_p
			and	nr_seq_prestador = nr_seq_prestador_w;
			
			if (nr_seq_rps_prest_w = 0) then
				ie_insere_plano_w := 'S';
				--pego a proxima sequencia aqui pois vou precisar informar nos planos													
				select	pls_rps_prestador_seq.nextval
				into	nr_seq_rps_prest_w
				from	dual;

				begin
				select	dt_alt_inicial,
					dt_alt_final
				into	dt_alt_inicial_w,
					dt_alt_final_w
				from	pls_lote_rps
				where	nr_sequencia	= nr_seq_lote_p;
				exception
				when others then
					dt_alt_inicial_w	:= null;
					dt_alt_final_w		:= null;
				end;

				begin
				select	substr(a.cd_cgc_cpf_old, 1, 14),
					substr(a.cd_cnes_old, 1, 7),
					substr(a.cd_municipio_ibge_old, 1, 6)
				into	cd_cgc_cpf_old_w,
					cd_cnes_old_w,
					cd_municipio_ibge_old_w
				from	pls_alt_prest_rps	a
				where	a.nr_sequencia		= (	select	min(d.nr_sequencia)
									from	pls_alt_prest_rps	d,
										pls_rps_prestador	c
									where	c.nr_seq_prestador	= d.nr_seq_prestador
									and	c.nr_seq_prestador	= nr_seq_prestador_w
									and	d.dt_atualizacao between nvl(dt_alt_inicial_w, trunc(sysdate, 'month')) and fim_dia(last_day(nvl(dt_alt_final_w, sysdate))) );
				exception
				when others then
					cd_cgc_cpf_old_w	:= null;
					cd_cnes_old_w		:= null;
					cd_municipio_ibge_old_w	:= null;
				end;

				if	(cd_cgc_cpf_old_w is null) then
					begin
					select	decode(a.cd_cgc, null, (select	b.NR_CPF
									from	pessoa_fisica b
									where	b.cd_pessoa_fisica = a.cd_pessoa_fisica), a.cd_cgc)
					into	cd_cgc_cpf_old_w
					from	pls_prestador a
					where	a.nr_sequencia = nr_seq_prestador_w;
					exception
					when others then
						cd_cgc_cpf_old_w	:= null;
					end;
				end if;
				
				if	(cd_cnes_old_w is null) then
					begin
					select	nvl(pls_obter_cnes_prestador(a.nr_sequencia), '')
					into	cd_cnes_old_w
					from	pls_prestador a
					where	a.nr_sequencia = nr_seq_prestador_w;
					exception
					when others then
						cd_cnes_old_w		:= '';
					end;
				end if;
				
				if	(cd_municipio_ibge_old_w is null) then
					begin
					cd_municipio_ibge_old_w	:= lpad(substr(elimina_caractere_especial(pls_obter_dados_prest_end(	nr_seq_prestador_w, null, null,'MI')), 1, 6), 6, '0');
					exception
					when others then
						cd_municipio_ibge_old_w	:= null;
					end;
				end if;

				--insiro as informa��es dos prestadores .
				insert into pls_rps_prestador
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_lote,
					nr_seq_prestador,
					nr_seq_solicitacao,
					cd_classificacao,
					cd_cpf_cnpj,
					cd_cnes,
					sg_uf,
					cd_municipio_ibge,
					ds_razao_social,
					ie_relacao_operadora,
					ie_tipo_contratualizacao,
					cd_ans_int,
					dt_contratualizacao,
					dt_inicio_servico,
					ie_disponibilidade_serv,
					ie_urgencia_emergencia,
					cd_cnes_old,
					cd_cpf_cnpj_old,
					cd_municipio_ibge_old)
				values	(nr_seq_rps_prest_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_lote_p,
					nr_seq_prestador_w,
					nr_seq_solicitacao_p,
					cd_classificacao_w,
					cd_cpf_cnpj_w,
					cd_cnes_w,
					sg_uf_w,
					cd_municipio_ibge_w,
					ds_razao_social_w,
					ie_relacao_operadora_w,
					ie_tipo_contratualizacao_w,
					null,--cd_ans_int,
					dt_inicio_contrato_w,
					dt_inicio_servico_w,
					ie_disponibilidade_serv_w,
					ie_urgencia_emergencia_w,
					cd_cnes_old_w,
					cd_cgc_cpf_old_w,
					cd_municipio_ibge_old_w);
			end if;	
			--insiri os dados na pls_rps_prest_alt
			insert into pls_rps_prest_alt
				(nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_alt_prest,
				nr_seq_rps_prest)
			values(	pls_rps_prest_alt_seq.nextval,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_alt_prest_w,
				nr_seq_rps_prest_w);
				
			--prestadores inseridos agora come�o a pegar as informa��es necessarias para os planos
			
			select	dt_lote
			into	dt_lote_w
			from	pls_lote_rps
			where	nr_sequencia = nr_seq_lote_p;

			open c02;
			loop
			fetch c02 into
				nr_registro_plano_w,
				cd_plano_w,
				nr_seq_plano_w,
				ie_regulamentacao_w;
			exit when c02%notfound;
				begin
				qt_registro_w := 0;
				qt_vida_ativa_w := 0;
				
				if	(cd_classificacao_w = '3') and
					(ie_urgencia_emergencia_w <> 'S') then
					cd_plano_w		:= null;
					nr_registro_plano_w	:= null;
				else
					if	(ie_regulamentacao_w = 'R') then
						nr_registro_plano_w	:= null;
					else
						cd_plano_w		:= null;
					end if;
				end if;
				
				select	count(1)
				into	qt_vida_ativa_w
				from	pls_segurado
				where	nr_seq_plano = nr_seq_plano_w
				and	nr_seq_contrato is not null
				and	dt_liberacao is not null
				and	(coalesce(dt_rescisao, dt_limite_utilizacao, dt_cancelamento) is null 
				or	coalesce(dt_rescisao, dt_limite_utilizacao, dt_cancelamento) >= sysdate)
				and	ie_tipo_segurado in ('B','R');
				
				select	count(1)
				into	qt_registro_w
				from	pls_rps_prest_plano
				where	nr_seq_prestador_rps	= nr_seq_rps_prest_w
				and	(nr_registro_plano	= nr_registro_plano_w
				or	cd_plano		= cd_plano_w);
				
				if	(qt_registro_w = 0) and (qt_vida_ativa_w > 0) and (ie_insere_plano_w = 'S')then
					insert into pls_rps_prest_plano
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_prestador_rps,
						nr_seq_plano,
						nr_registro_plano,
						cd_plano)
					values	(pls_rps_prest_plano_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_rps_prest_w,
						nr_seq_plano_w,
						nr_registro_plano_w,
						cd_plano_w);
				end if;
				end;
			end loop;
			close c02;	
		end if;
	end if;
	end;	
end loop;
close c01;

commit;

end pls_gerar_prest_alt_rps;
/
