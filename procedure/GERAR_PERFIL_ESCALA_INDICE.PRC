Create or Replace 
Procedure gerar_perfil_escala_indice(	
						cd_perfil_p		number, 
						nm_usuario_p		Varchar2,
						nr_seq_grupo_p	Number) is
nr_sequencia_w		Number(10);

begin

select	perfil_escala_indice_seq.nextval
into	nr_sequencia_w
from 	dual;

insert into perfil_escala_indice (
	nr_sequencia,
	ie_atualizar,
	nm_usuario, 
	cd_perfil, 
	dt_atualizacao, 
	nr_seq_grupo, 
	nr_seq_apres)
values	(
 	nr_sequencia_w,
	'S',
	nm_usuario_p,
	cd_perfil_p, 
	sysdate, 
	nr_seq_grupo_p,
	999);

commit;
end gerar_perfil_escala_indice;
/
