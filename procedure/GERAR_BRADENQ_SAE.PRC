create or replace
procedure gerar_bradenq_sae(nr_sequencia_p		number,
			nm_usuario_p		varchar2) is

nr_atendimento_w	number(10);
qt_reg_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_item_escala_w	number(10);

/* Atributos da escala */
ie_mobilidade_w		  number(1);
ie_atividade_fisica_w	  number(1);
ie_percepcao_sensorial_w  number(1);
ie_umidade_w		  number(1);
ie_friccao_cisalhamento_w number(1);
ie_nutricao_w	    	  number(1);
ie_perfusao_w	    	  number(1);

	function obter_valor_escala( nr_seq_item_escala_p	number) return varchar2 is

	ie_retorno_w	varchar2(15);
	nr_seq_result_escala_w	varchar2(15);
	begin
	
	select	max(f.nr_seq_result_escala)
	into	nr_seq_result_escala_w
	from	pe_prescricao a,
		pe_prescr_item_result b,
		pe_regra_item_escala c,
		pe_regra_result_escala f
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_sequencia = b.nr_seq_prescr
	and	b.nr_seq_item = c.nr_seq_item_sae
	and	c.nr_seq_item_escala = nr_seq_item_escala_p
	and	f.nr_seq_result_sae = b.nr_seq_result
	and	f.nr_seq_regra = c.nr_sequencia;
	
	select 	max (x.vl_result)
	into	ie_retorno_w
	from	pe_result_item x,
		pe_regra_result_escala f
	where	x.nr_sequencia = nr_seq_result_escala_w;
	

	
	return ie_retorno_w;
	end;

begin

select	max(a.cd_prescritor),
	max(a.nr_atendimento)
into	cd_pessoa_fisica_w,
	nr_atendimento_w
from	pe_prescricao a,
	pe_prescr_item_result b
where	a.nr_sequencia = b.nr_seq_prescr
and	a.nr_sequencia = nr_sequencia_p;

select	count(*)
into	qt_reg_w
from 	pe_prescricao a,
	pe_prescr_item_result b,
	pe_regra_item_escala c,
	pe_regra_result_escala d
where	a.nr_sequencia	= b.nr_seq_prescr
and	b.nr_seq_result = d.nr_seq_result_sae
and	b.nr_seq_item	= c.nr_seq_item_sae
and	a.nr_sequencia	= nr_sequencia_p
and	c.nr_seq_item_escala	is not null
and 	c.ie_escala = 67;

if 	(qt_reg_w > 0) then
	begin
		ie_mobilidade_w		  := obter_valor_escala(26);
		ie_atividade_fisica_w	  := obter_valor_escala(27);
		ie_percepcao_sensorial_w  := obter_valor_escala(28);
		ie_umidade_w		  := obter_valor_escala(29);
		ie_friccao_cisalhamento_w := obter_valor_escala(30);
		ie_nutricao_w	    	  := obter_valor_escala(31);
		ie_perfusao_w	    	  := obter_valor_escala(32);			
	end;
	
	insert into atend_escala_braden_q (nr_sequencia,
					cd_pessoa_fisica,
					dt_atualizacao,
					dt_avaliacao,
					ie_mobilidade,
					ie_atividade_fisica,
					ie_percepcao_sensorial,
					ie_umidade,
					ie_friccao_cisalhamento,
					ie_nutricao,
					ie_perfusao,
					nm_usuario,
					nr_atendimento,
					ie_situacao,
					dt_liberacao,
					nr_seq_prescr)
		values	(	atend_escala_braden_q_SEQ.nextval,
					cd_pessoa_fisica_w,
					sysdate,
					sysdate,
					nvl(ie_mobilidade_w,1),
					nvl(ie_atividade_fisica_w,1),
					nvl(ie_percepcao_sensorial_w,1),
					nvl(ie_umidade_w,1),
					nvl(ie_friccao_cisalhamento_w,1),
					nvl(ie_nutricao_w,1),
					nvl(ie_perfusao_w,1),
					nm_usuario_p,
					nr_atendimento_w,
					'A',
					sysdate,
					nr_sequencia_p);
	
end if;	

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_bradenq_sae;
/
