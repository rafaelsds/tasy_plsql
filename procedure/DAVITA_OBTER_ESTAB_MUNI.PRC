CREATE OR REPLACE PROCEDURE DAVITA_OBTER_ESTAB_MUNI(NR_SEQUENCIA_P NUMBER) IS

  JSON_MUNICIPIOS_W     PHILIPS_JSON;
  JSON_VALUE_AUX_W      PHILIPS_JSON_VALUE;
  JSON_PARAM_W          PHILIPS_JSON_LIST;
  JSON_UF        PHILIPS_JSON;
  JSON_RETORNO_LISTA     PHILIPS_JSON_LIST;
  JSON_RETORNO    PHILIPS_JSON;
  VR_UF_W          PESSOA_JURIDICA.SG_ESTADO%TYPE;
  DS_MESSAGE_W          INTPD_FILA_TRANSMISSAO.DS_MESSAGE%TYPE;
  MY_ARRAY INTPD_REST_UTILS_PCK.TB_VARCHAR;
  CD_CONVENIO_W REGRA_LIB_CONV_AGENDA.CD_CONVENIO%TYPE;

BEGIN

    BEGIN
    -- Verifica se existe requisi��o na Fila
        SELECT IFT.DS_MESSAGE
            INTO DS_MESSAGE_W
            FROM INTPD_FILA_TRANSMISSAO IFT
          WHERE IFT.NR_SEQUENCIA = NR_SEQUENCIA_P;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
       -- Caso n�o exista, retorna exce��o de consulta sem retorno
             RAISE INTPD_REST_UTILS_PCK.RECORD_NOT_FOUND_E;
    END;

    -- Converte objeto do JSON para valida��o de estrutura
    JSON_VALUE_AUX_W := PHILIPS_JSON(DS_MESSAGE_W).GET('listaUF');

    -- Valida se estrutura de JSON est� correta, caso n�o estiver retorna exce��o
    IF (JSON_VALUE_AUX_W IS NULL) THEN
      RAISE INTPD_REST_UTILS_PCK.ERROR_ESTRUTURA_E;
    END IF;

    -- Converte o JSON para lista
    JSON_PARAM_W := PHILIPS_JSON_LIST(JSON_VALUE_AUX_W);
    -- Inicializa lista de JSON
    JSON_RETORNO_LISTA := PHILIPS_JSON_LIST();

    -- Percorre os par�metros do JSON
    FOR IND IN 1 .. JSON_PARAM_W.COUNT LOOP

        -- Converte objeto do JSON para char
        JSON_UF := PHILIPS_JSON(JSON_PARAM_W.GET(IND).TO_CHAR());

        -- Verifica se objeto existe, caso n�o existir retorna erro de par�metro
        IF (JSON_UF.GET('UF') IS NOT NULL) THEN
          VR_UF_W := JSON_UF.GET('UF').GET_STRING();
      MY_ARRAY(IND) := JSON_UF.GET('UF').GET_STRING();
        ELSE
          RAISE_APPLICATION_ERROR(-20101, 'UF');          
        END IF;

     END LOOP;
	 
	IF (PHILIPS_JSON(DS_MESSAGE_W).GET('codConvenio') IS NOT NULL) THEN
		CD_CONVENIO_W := PHILIPS_JSON(DS_MESSAGE_W).GET('codConvenio').GET_NUMBER();
	END IF;

   -- Cursor para buscar o c�digo do estabelecimento e o m�nicipio com base na UF passada como par�metro (VR_UF_W)
  FOR C IN (SELECT DISTINCT e.cd_estabelecimento cd_estabelecimento, p.ds_municipio ds_municipio
        FROM agenda a
           , estabelecimento e
           , pessoa_juridica p
         WHERE a.cd_tipo_agenda in (3,4)
         AND a.ie_situacao = 'A' -- Status da agenda (Ativa)
         AND a.cd_estabelecimento = e.cd_estabelecimento -- Liga��o do estabelecimento com agenda
         AND e.cd_cgc IS NOT NULL
         AND p.cd_cgc = e.cd_cgc
         AND p.sg_estado IN (SELECT * FROM TABLE(MY_ARRAY))
		 AND (CD_CONVENIO_W IN (SELECT rlca.cd_convenio FROM regra_lib_conv_agenda rlca WHERE rlca.cd_agenda = a.cd_agenda) OR CD_CONVENIO_W IS NULL)) LOOP

  -- Inicializa objeto JSON
      JSON_MUNICIPIOS_W := PHILIPS_JSON();
    -- Insere c�digo do estabelecimento no Objeto
      JSON_MUNICIPIOS_W.PUT('codEstabelecimento', c.cd_estabelecimento);
  -- Insere M�nicipio do estabelecimento no Objeto
      JSON_MUNICIPIOS_W.PUT('descMunicipio', c.ds_municipio);
  -- Adiciona objeto JSON para a lista JSON
      JSON_RETORNO_LISTA.APPEND(JSON_MUNICIPIOS_W.TO_JSON_VALUE());
  END LOOP;

    -- Inicializa objeto JSON
    JSON_RETORNO := PHILIPS_JSON();
    -- Insere a lista de objetos para o objeto JSON
    JSON_RETORNO.PUT('listaMunicipios', JSON_RETORNO_LISTA);

    -- Cria um clob tempor�rio
    DBMS_LOB.CREATETEMPORARY(DS_MESSAGE_W, TRUE);
    -- Converte objeto JSON para clob
    JSON_RETORNO.TO_CLOB(DS_MESSAGE_W);
    -- Cria retorno JSON
    UPDATE INTPD_FILA_TRANSMISSAO IFT
       SET IFT.IE_STATUS_HTTP      = 200,
           IFT.IE_STATUS           = 'S',
           IFT.DS_MESSAGE_RESPONSE = DS_MESSAGE_W
     WHERE IFT.NR_SEQUENCIA = NR_SEQUENCIA_P;
    COMMIT;
    DBMS_LOB.FREETEMPORARY(DS_MESSAGE_W);
  EXCEPTION
       WHEN INTPD_REST_UTILS_PCK.RECORD_NOT_FOUND_E THEN
       INTPD_REST_UTILS_PCK.UPDATE_RESPONSE_JSON_ERROR_CUS(NR_SEQUENCIA_P, 'Sequ�ncia da fila de integra��o n�o encontrado.', 400, 'codRetorno', 'descRetorno');
       WHEN INTPD_REST_UTILS_PCK.ERROR_PARAMETER_E THEN
       INTPD_REST_UTILS_PCK.UPDATE_RESPONSE_JSON_ERROR_CUS(NR_SEQUENCIA_P, 'Atributo' || SUBSTR(SQLERRM, INSTR(SQLERRM, ':') + 1) || ' obrigat�rio n�o encontrado!', 400, 'codRetorno', 'descRetorno');
       WHEN INTPD_REST_UTILS_PCK.ERROR_CUSTOM_E THEN
       INTPD_REST_UTILS_PCK.UPDATE_RESPONSE_JSON_ERROR_CUS(NR_SEQUENCIA_P, TRIM(SUBSTR(SQLERRM, INSTR(SQLERRM, ':') + 1)), 400, 'codRetorno', 'descRetorno');
       WHEN INTPD_REST_UTILS_PCK.ERROR_ESTRUTURA_E THEN
       INTPD_REST_UTILS_PCK.UPDATE_RESPONSE_JSON_ERROR_CUS(NR_SEQUENCIA_P, 'Estrutura JSON � inv�lida.', 404, 'codRetorno', 'descRetorno');
  WHEN OTHERS THEN
       INTPD_REST_UTILS_PCK.UPDATE_RESPONSE_JSON_ERROR_CUS(NR_SEQUENCIA_P, SQLERRM, 404, 'codRetorno', 'descRetorno');
END davita_obter_estab_muni;
/
