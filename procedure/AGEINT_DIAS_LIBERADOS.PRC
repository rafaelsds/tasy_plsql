CREATE OR REPLACE PROCEDURE ageint_dias_liberados(nr_seq_agenda_int_p number,
                                dt_agenda_p         date,
                                ds_retorno_p        out varchar2) is

begin

  ds_retorno_p := ageint_sugerir_horarios_pck.get_agendas_dias_liberados(nr_seq_agenda_int_p => nr_seq_agenda_int_p,
                                                                         dt_agenda_p         => dt_agenda_p);
end;
/