CREATE OR REPLACE PROCEDURE HC_CANCELAR_HOME_CARE 
(
	NR_SEQUENCIA_P NUMBER,
	NR_SEQ_MOTIVO_CANC_P NUMBER,
	CD_PESSOA_FISICA_P NUMBER,
	NM_USUARIO_P VARCHAR2
) AS 
BEGIN
	if(NR_SEQUENCIA_P is not null and NR_SEQ_MOTIVO_CANC_P is not null) then
		begin
			update PACIENTE_HOME_CARE 
			set DT_CANCELAMENTO = SYSDATE, 
			NM_USUARIO_CANC = NM_USUARIO_P, 
			NR_SEQ_MOTIVO_CANC = NR_SEQ_MOTIVO_CANC_P, 
			DT_FINAL = sysdate
			where nr_sequencia = NR_SEQUENCIA_P;

			CANC_AGEND_FUTUROS(CD_PESSOA_FISICA_P, NR_SEQ_MOTIVO_CANC_P); 

		end;
	end if;

END HC_CANCELAR_HOME_CARE;
/