Create or Replace PROCEDURE 
Gerar_Conta_Resumo_Provisoria(	dt_referencia_p	Date,
						ds_parametros_p	Varchar2,
						nm_usuario_p	Varchar2) IS

C01					Integer;
Ignore				Integer;
nr_interno_conta_w		Number(10,0);
dt_inicio_w				Date;
dt_final_w				Date;

BEGIN

dt_inicio_w	:= trunc(dt_referencia_p, 'month');
dt_final_w	:= trunc(last_day(dt_referencia_p),'dd') + 86399/86400;

delete from conta_paciente_resumo
where nr_interno_conta in (	select nr_interno_conta 
					from conta_paciente
					where dt_mesano_referencia between dt_inicio_w and dt_final_w
					  and ie_status_acerto = 1);

C01 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C01,	'select a.nr_interno_conta ' ||
				'from	atendimento_paciente c, conta_paciente a ' ||
				'where a.nr_atendimento = c.nr_atendimento ' ||
				'  and a.ie_status_acerto = 1 ' ||
	  			'  and a.dt_mesano_referencia between sysdate - 41 and sysdate - 10 ' ||
				ds_parametros_p, DBMS_SQL.Native); 
dbms_output.put_line(ds_parametros_p);
DBMS_SQL.DEFINE_COLUMN(C01, 1, nr_interno_conta_w); 
ignore := DBMS_SQL.EXECUTE(C01);

LOOP 
	IF (DBMS_SQL.FETCH_ROWS(C01) > 0) THEN 
		DBMS_SQL.COLUMN_VALUE(C01, 1, nr_interno_conta_w);
		Atualizar_Resumo_Conta(nr_interno_conta_w, 2);
	ELSE
		EXIT; 
	END IF; 
END LOOP;

COMMIT; 
DBMS_SQL.CLOSE_CURSOR(C01);

EXCEPTION 
	WHEN OTHERS THEN 
		IF DBMS_SQL.IS_OPEN(C01) THEN 
			DBMS_SQL.CLOSE_CURSOR(C01); 
		END IF; 
		RAISE; 

END Gerar_Conta_Resumo_Provisoria;
/