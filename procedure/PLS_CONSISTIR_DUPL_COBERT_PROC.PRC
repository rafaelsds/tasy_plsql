create or replace
procedure pls_consistir_dupl_cobert_proc
		(	nr_seq_tipo_cobert_p		number,
			nr_seq_cobertura_proc_p		number,
			cd_area_procedimento_p		number,
			cd_especialidade_p		number,
			cd_grupo_proc_p			number,
			cd_procedimento_p		number,
			ie_origem_proced_p		number,
			nr_seq_grupo_servico_p		number,
			ds_erro_p		out	varchar2) is

qt_duplicada_cober_w	number(10);
ds_erro_w		varchar2(400);

begin

ds_erro_w := '';

select	count(*)
into	qt_duplicada_cober_w
from	pls_cobertura_proc
where	nr_seq_tipo_cobertura		= nr_seq_tipo_cobert_p
and	nvl(cd_procedimento,0)		= nvl(cd_procedimento_p,nvl(cd_procedimento,0))
and	nvl(ie_origem_proced_p,0)	= nvl(ie_origem_proced,nvl(ie_origem_proced_p,0))
and	nvl(cd_grupo_proc,0)		= nvl(cd_grupo_proc_p,nvl(cd_grupo_proc,0))
and	nvl(cd_especialidade,0)		= nvl(cd_especialidade_p,nvl(cd_especialidade,0))
and	nvl(cd_area_procedimento,0)	= nvl(cd_area_procedimento_p,nvl(cd_area_procedimento,0))
and	nvl(nr_seq_grupo_servico,0)	= nvl(nr_seq_grupo_servico_p,nvl(nr_seq_grupo_servico,0))
and	nr_sequencia			<> nr_seq_cobertura_proc_p
and	IE_COBERTURA			= 'S';

if	(qt_duplicada_cober_w > 0) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280365);
end if;

ds_erro_p := ds_erro_p || ds_erro_w;

end pls_consistir_dupl_cobert_proc;
/