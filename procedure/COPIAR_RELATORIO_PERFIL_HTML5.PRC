create or replace procedure copiar_relatorio_perfil_html5
			(cd_perfil_origem_p			number,
			cd_perfil_destino_p			number,
			nr_seq_relatorio_p			varchar2,
			nm_usuario_p				varchar2) as

nr_seq_antiga_w		number(10,0);
nr_sequencia_w		number(10,0);
QT_COPIA_w        number(10,0);
IE_LOGO_w         varchar2(1);
IE_CONFIGURAR_w   varchar2(1);
IE_IMPRIMIR_w     varchar2(1);
IE_OUTPUTBIN_w    number(3, 0);
DS_IMPRESSORA_w   varchar2(60);
NR_SEQ_APRESENT_w number(10, 0);
IE_GRAVAR_LOG_w   varchar2(1);
IE_VISUALIZAR_w   varchar2(1);
IE_APRESENTA_w    varchar2(1);
IE_FORMA_IMPRESSAO_w  varchar2(5);
CD_ESTAB_W        number(4, 0);
cursor C01 is
select
    RELATORIO_PERFIL_seq.nextval,
		QT_COPIA,
		IE_LOGO,
		IE_CONFIGURAR,
		IE_IMPRIMIR,
		IE_OUTPUTBIN,
		DS_IMPRESSORA,
		NR_SEQ_APRESENT,
		IE_GRAVAR_LOG,
		IE_VISUALIZAR,
		IE_APRESENTA,
		IE_FORMA_IMPRESSAO,
        CD_ESTAB
from	RELATORIO_PERFIL
where	cd_perfil		 = cd_perfil_origem_p
and		nr_seq_relatorio = nr_seq_relatorio_p;

begin

select 	max(nr_sequencia)
into	nr_seq_antiga_w
from	RELATORIO_PERFIL
where	cd_perfil		 = cd_perfil_origem_p
and		nr_seq_relatorio = nr_seq_relatorio_p;

open C01;
loop
fetch C01 into
	nr_sequencia_w,
	QT_COPIA_w,
  IE_LOGO_w,
	IE_CONFIGURAR_w,
	IE_IMPRIMIR_w,
	IE_OUTPUTBIN_w,
	DS_IMPRESSORA_w,
	NR_SEQ_APRESENT_w,
	IE_GRAVAR_LOG_w,
	IE_VISUALIZAR_w,
	IE_APRESENTA_w,
	IE_FORMA_IMPRESSAO_w,
    CD_ESTAB_W;
exit when C01%notfound;
Begin
insert into RELATORIO_PERFIL
		(NR_SEQUENCIA,
		CD_PERFIL,
		NR_SEQ_RELATORIO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		QT_COPIA,
		IE_LOGO,
		IE_CONFIGURAR,
		IE_IMPRIMIR,
		IE_OUTPUTBIN,
		DS_IMPRESSORA,
		NR_SEQ_APRESENT,
		IE_GRAVAR_LOG,
		IE_VISUALIZAR,
		IE_APRESENTA,
		IE_FORMA_IMPRESSAO,
        CD_ESTAB)
    values(nr_sequencia_w,
    cd_perfil_destino_p,
    nr_seq_relatorio_p,
    sysdate,
    nm_usuario_p,
    QT_COPIA_w,
    IE_LOGO_w,
    IE_CONFIGURAR_w,
    IE_IMPRIMIR_w,
    IE_OUTPUTBIN_w,
    DS_IMPRESSORA_w,
    NR_SEQ_APRESENT_w,
    IE_GRAVAR_LOG_w,
    IE_VISUALIZAR_w,
    IE_APRESENTA_w,
    IE_FORMA_IMPRESSAO_w,
    cd_estab_w);

insert into relatorio_perfil_param(
			nr_seq_perfil_relat,
			nr_seq_parametro,
			dt_atualizacao,
			nm_usuario,
			ds_valor,
			ds_valor_exec)
(select	nr_sequencia_w,
		nr_seq_parametro,
		sysdate,
		nm_usuario_p,
		ds_valor,
		ds_valor_exec
from	relatorio_perfil_param a
where	a.nr_seq_perfil_relat = nr_seq_antiga_w);


	end;
end loop;
close C01;

commit;

end copiar_relatorio_perfil_html5;
/