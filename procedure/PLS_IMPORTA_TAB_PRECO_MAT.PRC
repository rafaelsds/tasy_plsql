create or replace
procedure pls_importa_tab_preco_mat(
			nr_seq_tabela_preco_p in out	pls_material_preco.nr_sequencia%type,
			cd_tiss_brasindice_p		varchar2,
			vl_material_p			number,
			vl_percentual_p			number,	
			dt_inicio_vigencia_p		date,
			cd_moeda_p			moeda.cd_moeda%type,
			nm_usuario_p			usuario.nm_usuario%type,
			cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
			ie_tipo_preco_p			pls_material_valor_item.ie_tipo_preco%type,
			qt_convercao_p			pls_material_valor_item.qt_convercao%type,
			cd_tuss_p			pls_material_preco_imp_log.cd_tuss%type,
			ie_item_criado_atual_p out	varchar2,
			ds_retorno_p	       out	Varchar2) is 

			
qt_registro_w		number(1);
dt_inicio_vigencia_w	date;
vl_material_w		number(15,2);
vl_material_calc_w	number(15,2);
ie_importado_w		varchar2(2);

ie_situacao_w		pls_material_preco.ie_situacao%type;
ds_tabela_w		pls_material_preco.ds_tabela%type;
cd_tiss_brasindice_w	pls_material.cd_tiss_brasindice%type;
nr_seq_tabela_preco_w	pls_material_preco.nr_sequencia%Type;

cursor C01 (cd_tiss_brasindice_p	pls_material.cd_tiss_brasindice%type) is
select	nr_sequencia nr_seq_material,
	cd_material_ops cd_material,
	qt_conversao
from	pls_material 
where	cd_tiss_brasindice = cd_tiss_brasindice_p
and	ie_situacao = 'A';

begin
ie_importado_w := 'N';
--Verificar se existe a tabela de preco, se n�o existir criar uma nova
select	max(ie_situacao),
	max(nr_sequencia)
into	ie_situacao_w,
	nr_seq_tabela_preco_w
from	pls_material_preco
where	nr_sequencia = nr_seq_tabela_preco_p;	

ds_retorno_p := null;
if	(ie_situacao_w <> 'A' and ie_situacao_w is not null) then
	qt_registro_w := -1;
	ds_retorno_p := 'A tabela selecionada n�o est� ativa.';
else
	if	(nr_seq_tabela_preco_w is not null) then
		qt_registro_w := 1;
	else
		qt_registro_w := 0;
	end if;
	
end if;

if	(qt_registro_w <> -1) then
	if	(qt_registro_w = 0) then

		select	pls_material_preco_seq.nextval
		into	nr_seq_tabela_preco_w
		from	dual;
		
		ds_tabela_w := 'Tabela '||nr_seq_tabela_preco_w||' - Importa��o de arquivo de pre�o ('
						||sysdate||')'||' - '||nm_usuario_p;
		
		insert into pls_material_preco
			(nr_sequencia, cd_estabelecimento, dt_atualizacao, nm_usuario, ds_tabela, ie_situacao)
		values	(nr_seq_tabela_preco_w, cd_estabelecimento_p,sysdate,nm_usuario_p,ds_tabela_w,'A');
		
		-- a partir do primeiro registro que encontrar, todos os demais far�o parte da mesma tabela
		nr_seq_tabela_preco_p := nr_seq_tabela_preco_w;
	end if;

	if	(nr_seq_tabela_preco_w is not null) then
		
		-- Fa�o os tratamentos com exception porque pode ser que exista lixo no arquivo.
		cd_tiss_brasindice_w := null;
		begin
			if	(cd_tiss_brasindice_p is not null) then
				cd_tiss_brasindice_w := cd_tiss_brasindice_p;--Atributo cd_tiss_brasindice registro 101 interface 2393		
			end if;
		exception
		when value_error then
			null;
		end;
		
		vl_material_w := 0;
		begin
			if	(vl_material_p is not null) then
				vl_material_w := vl_material_p;--Atributo vl_material registro 101 interface 2393
			end if;
		exception
		when value_error then
			null;
		end;

		vl_material_calc_w := 0;
		begin
			vl_material_calc_w := (vl_material_w * vl_percentual_p) + vl_material_w;
		exception
		when value_error then
			null;
		end;

		dt_inicio_vigencia_w := sysdate;
		begin	
			dt_inicio_vigencia_w := trunc(dt_inicio_vigencia_p);	
		exception
		when value_error then
			null;
		end;

		-- Materiais com o c�digo brasindice informado
		for	r_C01_w in C01 (cd_tiss_brasindice_w) loop
		
			if	(r_C01_w.nr_seq_material is not null) then
							
				-- jjung OS 523315 -	Inserir os valores para cada item que estiver cadastrado na tabela para o material selecionado.
				pls_inserir_valor_tab_mat(
								nr_seq_tabela_preco_w,r_C01_w.nr_seq_material,vl_material_calc_w,
								dt_inicio_vigencia_w,'S',cd_moeda_p,
								ie_item_criado_atual_p,	nm_usuario_p, ie_tipo_preco_p,
								qt_convercao_p);
				ie_importado_w := 'S';
				
				commit;
				
				pls_gera_log_imp_tab_mat(
								cd_moeda_p,cd_tiss_brasindice_w,dt_inicio_vigencia_w,   
								ie_item_criado_atual_p,ie_tipo_preco_p,r_C01_w.nr_seq_material,       
								nr_seq_tabela_preco_w,qt_convercao_p,vl_material_calc_w,           
								cd_tuss_p,nm_usuario_p);	
			end if;
		end loop;-- C01
		
		if	(ie_importado_w = 'N') then
			pls_gera_log_imp_tab_mat(
							cd_moeda_p,cd_tiss_brasindice_w,dt_inicio_vigencia_w,   
							'N',ie_tipo_preco_p,null,       
							nr_seq_tabela_preco_w,qt_convercao_p,vl_material_calc_w,           
							cd_tuss_p,nm_usuario_p);
		end if;
						
	end if;
end if;
end pls_importa_tab_preco_mat;
/
