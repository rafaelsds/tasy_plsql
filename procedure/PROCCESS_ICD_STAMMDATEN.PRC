create or replace PROCEDURE PROCCESS_ICD_STAMMDATEN (import_p NUMBER, nm_usuario_p  VARCHAR2 default 'TASYLOAD') IS 
  
  -- default values for table CID_ESPECIALIDADE  
  CD_DEFAULT_ESP_W 	cid_especialidade.cd_especialidade_cid%type := '999';
  DS_DEFAULT_ESP_W 	cid_especialidade.ds_especialidade_cid%type := 'Other ICD Codes';

  -- default values for table CID_CATEGORIA
  CD_DEFAULT_CAT_W 		cid_categoria.cd_categoria_cid%type := 'ZZZ999.999';
  DS_DEFAULT_CAT_W 		cid_categoria.ds_categoria_cid%type := 'Other ICD Codes';
  -- table icd_category_aut
  CD_BEGIN_CATEGORY_W   icd_category_aut.cd_begin_category%type;
  CD_END_CATEGORY_W     icd_category_aut.cd_end_category%type;
  CD_CHAPTER_FK_W       icd_category_aut.cd_chapter%type;
  DS_CATEGORY_W         icd_category_aut.ds_category%type;
  CD_CATEGORY_W         cid_categoria.cd_categoria_cid%type;
  CD_CATEGORY_RANGE_BEGIN_W   NUMBER(10); 
  CD_CATEGORY_RANGE_END_W     NUMBER(10);
  CD_CATEGORY_NUM_W     NUMBER(10); 
  CD_CATEGORY_VAR_W     VARCHAR(10);


  qt_exist_chapter_w     NUMBER(10);
  qt_exist_category_w    NUMBER(10);

  CURSOR c_category IS
    SELECT CD_BEGIN_CATEGORY, CD_END_CATEGORY, CD_CHAPTER, DS_CATEGORY
    FROM icd_category_aut;
BEGIN

    -- BEGIN CID_ESPECIALIDADE 
    BEGIN    
        -- INSERT NEW DATA ON CID_ESPECIALIDADE
        INSERT INTO cid_especialidade ( 
                     cd_especialidade_cid,
                     ds_especialidade_cid,
                     dt_atualizacao,
                     dt_atualizacao_nrec,
                     nm_usuario,
                     nm_usuario_nrec,
                     ie_situacao,
                     ds_especialidade_compl
                  )  
        SELECT 
            cd_chapter,                 -- cd_especialidade_cid
            substr(ds_chapter, 1, 100), -- ds_especialidade_cid
            sysdate,                    -- dt_atualizacao
            sysdate,                    -- dt_atualizacao_nrec
            nm_usuario_p,               -- nm_usuario
            nm_usuario_p,               -- nm_usuario_nrec
            'A',                        -- ie_situacao
            ds_chapter                  -- ds_especialidade_compl
        FROM icd_chapter_aut cha 
            LEFT JOIN cid_especialidade cid ON cha.cd_chapter = cid.cd_especialidade_cid
        WHERE cid.cd_especialidade_cid is null;

        -- UPDATE EXISTENTS RECORDS FROM CID_ESPECIALIDADE
        UPDATE cid_especialidade esp
        SET (ds_especialidade_cid,
             dt_atualizacao,
             nm_usuario,
             ie_situacao) = (
                                SELECT 
                                    substr(cha.ds_chapter, 1, 100), 
                                    SYSDATE,
                                    NM_USUARIO_P,
                                    'A'
                                FROM icd_chapter_aut cha 
                                WHERE cha.cd_chapter = esp.cd_especialidade_cid 
                            )
        WHERE EXISTS (SELECT 1 
                      FROM icd_chapter_aut cha 
                      WHERE cha.cd_chapter = esp.cd_especialidade_cid );     

        -- CHECK IF DEFAULT VALUE FROM CID_ESPECIALIDADE ALREADY EXISTS
        SELECT COUNT(*) 
        INTO qt_exist_chapter_w
        FROM cid_especialidade cid
        WHERE cid.cd_especialidade_cid = CD_DEFAULT_ESP_W;

        IF (qt_exist_chapter_w = 0) THEN
            -- INSERT IF DON'T EXIST
            INSERT INTO cid_especialidade ( 
                         cd_especialidade_cid,
                         ds_especialidade_cid,
                         dt_atualizacao,
                         dt_atualizacao_nrec,
                         nm_usuario,
                         nm_usuario_nrec,
                         ie_situacao
                      )  
            VALUES ( 
                CD_DEFAULT_ESP_W, 
                DS_DEFAULT_ESP_W, 
                SYSDATE, 
                SYSDATE,
                NM_USUARIO_P, 
                NM_USUARIO_P,
                'A'
                );
        ELSE
            -- UPDATE DEFAULT VALUE
            UPDATE cid_especialidade 
            SET ds_especialidade_cid   = DS_DEFAULT_ESP_W,
                dt_atualizacao_nrec    = SYSDATE,
                nm_usuario_nrec        = NM_USUARIO_P,
                ie_situacao            = 'A'
            WHERE cd_especialidade_cid  = CD_DEFAULT_ESP_W;
        END IF;  

    COMMIT;
    END;
    -- END CID_ESPECIALIDADE 

    --BEGIN CID_CATEGORIA
    BEGIN
        OPEN c_category; 
            LOOP
                FETCH c_category into CD_BEGIN_CATEGORY_W, CD_END_CATEGORY_W, CD_CHAPTER_FK_W, DS_CATEGORY_W;
                EXIT WHEN  c_category%NOTFOUND;
                BEGIN
                    -- CREATE RANGE FOR ICD CODES
                    CD_CATEGORY_RANGE_BEGIN_W := TO_NUMBER(SUBSTR(CD_BEGIN_CATEGORY_W,2,LENGTH(CD_BEGIN_CATEGORY_W)));
                    CD_CATEGORY_RANGE_END_W := TO_NUMBER(SUBSTR(CD_END_CATEGORY_W,2,LENGTH(CD_END_CATEGORY_W)));

                    FOR CD_CATEGORY_NUM_W in  CD_CATEGORY_RANGE_BEGIN_W .. CD_CATEGORY_RANGE_END_W
                        LOOP  
                            IF ( LENGTH(CD_CATEGORY_NUM_W) =  1) THEN  
                              CD_CATEGORY_VAR_W := concat('0',CD_CATEGORY_NUM_W);
                            ELSE
                              CD_CATEGORY_VAR_W := CD_CATEGORY_NUM_W;
                            END IF;
                            -- CREATE ICD CODE 
                            CD_CATEGORY_W := CONCAT(SUBSTR(CD_BEGIN_CATEGORY_W,1,1),CD_CATEGORY_VAR_W); 

                            SELECT COUNT(*)
                            INTO QT_EXIST_CATEGORY_W
                            FROM cid_categoria 
                            WHERE cd_categoria_cid  = CD_CATEGORY_W;

                            IF ( QT_EXIST_CATEGORY_W = 0 or QT_EXIST_CATEGORY_W is null) THEN
                                INSERT INTO cid_categoria ( 
                                    cd_categoria_cid,
                                    ds_categoria_cid,
                                    dt_atualizacao,
                                    dt_atualizacao_nrec,
                                    nm_usuario,
                                    nm_usuario_nrec,
                                    cd_especialidade,
                                    ie_situacao
                                ) VALUES (
                                    CD_CATEGORY_W,  -- CD_CATEGORIA_CID
                                    DS_CATEGORY_W,  -- DS_CATEGORIA_CID
                                    SYSDATE,        -- DT_ATUALIZACAO
                                    SYSDATE,        -- DT_ATUALIZACAO_NREC
                                    NM_USUARIO_P,   -- NM_USUARIO
                                    NM_USUARIO_P,   -- NM_USUARIO_NREC
                                    CD_CHAPTER_FK_W,-- CD_ESPECIALIDADE
                                    'A'             -- IE_SITUACAO
                               );
                            ELSE 
                                UPDATE cid_categoria 
                                SET ds_categoria_cid     = DS_CATEGORY_W,
                                    dt_atualizacao       = SYSDATE,
                                    nm_usuario           = NM_USUARIO_P,
                                    cd_especialidade     = CD_CHAPTER_FK_W,
                                    ie_situacao          = 'A'
                                WHERE cd_categoria_cid = CD_CATEGORY_W;
                            END IF;
                     END LOOP;
                END;
            END LOOP;
        CLOSE c_category;

        -- CHECK IF DEFAULT VALUE FROM CID_CATEGORIA ALREADY EXISTS
        SELECT COUNT(*)
        INTO qt_exist_category_w
        FROM cid_categoria 
        WHERE cd_categoria_cid   = CD_DEFAULT_CAT_W;


        IF ( qt_exist_category_w = 0 or qt_exist_category_w is null) THEN
            INSERT INTO cid_categoria ( 
                cd_categoria_cid,
                ds_categoria_cid,
                dt_atualizacao,
                dt_atualizacao_nrec,
                nm_usuario,
                nm_usuario_nrec,
                cd_especialidade,
                ie_situacao
            ) VALUES (
                CD_DEFAULT_CAT_W,   -- CD_CATEGORIA_CID
                DS_DEFAULT_CAT_W,  -- DS_CATEGORIA_CID
                SYSDATE,            -- DT_ATUALIZACAO
                SYSDATE,            -- DT_ATUALIZACAO_NREC
                NM_USUARIO_P,       -- NM_USUARIO
                NM_USUARIO_P,       -- NM_USUARIO_NREC
                CD_DEFAULT_ESP_W,   -- CD_ESPECIALIDADE
                'A'                 -- IE_SITUACAO
            );
        ELSE   
            UPDATE cid_categoria 
            SET ds_categoria_cid     = DS_DEFAULT_CAT_W,
                dt_atualizacao       = SYSDATE,
                nm_usuario           = NM_USUARIO_P,
                cd_especialidade     = CD_DEFAULT_ESP_W,
                ie_situacao          = 'A'
            WHERE cd_categoria_cid = CD_DEFAULT_CAT_W;
        END IF;	  

    COMMIT;
    END;
    -- END CID_CATEGORIA


    -- BEGIN CID_DOENCA
    BEGIN
        -- INSERT NEW DATA
        INSERT INTO cid_doenca 
                    (
                      cd_doenca_cid, 
                      ds_doenca_cid, 
                      cd_categoria_cid, 
                      dt_atualizacao,
                      nm_usuario, 
                      ie_sexo, 
                      ie_cad_interno, 
                      ie_situacao, 
                      qt_idade_min,
                      qt_idade_max, 
                      ds_descricao_original, 
                      cd_versao,        
                      cd_doenca, 
                      dt_atualizacao_nrec, 
                      nm_usuario_nrec,
                      ie_codificacao
                    )
                    SELECT 
                        cid_doenca_seq.NEXTVAL,                                     -- cd_doenca_cid
                        diag.kurzbezeichnung,                                       -- ds_doenca_cid
                        COALESCE(cat.cd_categoria_cid, CD_DEFAULT_CAT_W),           -- cd_categoria_cid
                        SYSDATE,                                                    -- dt_atualizacao
                        NM_USUARIO_P,                                               -- nm_usuario
                        DECODE(diag.geschlecht, 
                            'M', 'M', 
                            'W', 'F', 
                                 'A'),                                              -- ie_sexo
                        'N',                                                        -- ie_cad_interno
                        'A',                                                        -- ie_situacao
                        TO_NUMBER(diag.mindestalter),                               -- qt_idade_min
                        TO_NUMBER(diag.hochstalter),                                -- qt_idade_max
                        diag.kurzbezeichnung,                                       -- ds_descricao_original
                        'ICD-10',                                                   -- cd_versao
                        diag.diagnose,                                              -- cd_doenca
                        SYSDATE,                                                    -- dt_atualizacao_nrec
                        NM_USUARIO_P,                                               -- nm_usuario_nrec
                        DECODE(diag.kennzeichen, 
                            '!', 'E', 
                            '#', 'I', 
                            '+', 'P',
                            '*', 'S',
                                NULL)                                               -- ie_codificacao
                    FROM w_xdok_diaglist diag  
                        LEFT JOIN cid_doenca doe ON diag.diagnose = doe.cd_doenca 
                        LEFT JOIN cid_categoria cat ON SUBSTR(diag.diagnose,1,3) = cat.cd_categoria_cid
                    WHERE doe.cd_doenca_cid IS NULL
                          AND diag.nr_seq_import  = IMPORT_P;

        -- UPDATE DATA IF IT ALREADY EXISTS
        UPDATE cid_doenca doe 
        SET (
                ds_doenca_cid,
                cd_categoria_cid,
                dt_atualizacao,
                nm_usuario,
                ie_sexo,
                qt_idade_min,
                qt_idade_max, 
                ie_situacao,
                cd_versao,
                ie_codificacao) = (
                                    SELECT DISTINCT
                                        diag.kurzbezeichnung,                                       -- ds_doenca_cid
                                        COALESCE(cat.cd_categoria_cid, CD_DEFAULT_CAT_W) as cd_cat, -- cd_categoria_cid
                                        SYSDATE,                                                    -- dt_atualizacao
                                        NM_USUARIO_P,                                               -- nm_usuario
                                        DECODE(diag.geschlecht, 
                                            'M', 'M', 
                                            'W', 'F', 
                                                 'A') as genero,                                    -- ie_sexo
                                        TO_NUMBER(diag.mindestalter) as min_age,                    -- qt_idade_min
                                        TO_NUMBER(diag.hochstalter) as max_age,                     -- qt_idade_max
                                        'A',                                                        -- ie_situacao
                                        'ICD-10',                                                   -- cd_versao
                                        DECODE(diag.kennzeichen, 
                                                       '!', 'E', 
                                                       '#', 'I', 
                                                       '+', 'P',
                                                       '*', 'S',
                                                            NULL)                                  -- ie_codificacao
                                    FROM (
                                            SELECT DISTINCT diagnose, kurzbezeichnung, geschlecht, mindestalter, hochstalter, kennzeichen
                                            FROM w_xdok_diaglist
                                            WHERE nr_seq_import = IMPORT_P
                                        ) diag 
                                        LEFT JOIN cid_categoria cat ON SUBSTR(diag.diagnose,1,3) = cat.cd_categoria_cid 
                                    WHERE diag.diagnose = doe.cd_doenca
                                    )          
        WHERE EXISTS (SELECT 1 
                      FROM  (
                                SELECT DISTINCT diagnose, kurzbezeichnung, geschlecht, mindestalter, hochstalter, kennzeichen
                                FROM w_xdok_diaglist
                                WHERE nr_seq_import = IMPORT_P
                            ) diag 
                      WHERE diag.diagnose = doe.cd_doenca );   

        -- END THE VIGENCY OF THE OLD DISEASES ON TABLE CID_DOENCA_VERSAO
        UPDATE cid_doenca_versao doe_ver
        SET dt_vigencia_final = trunc(SYSDATE)
        WHERE EXISTS (  SELECT 1
                        FROM cid_doenca doe 
                        WHERE 
                                doe_ver.cd_doenca_cid = doe.cd_doenca_cid
                            AND (dt_vigencia_final > TRUNC(current_date) OR dt_vigencia_final IS NULL)
                            AND dt_vigencia_inicial IS NOT NULL);

        -- INSERT THE NEW RECORDS ON TABLE CID_DOENCA_VERSAO
        INSERT INTO cid_doenca_versao
          (
            nr_sequencia,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            cd_doenca_cid,
            dt_versao,
            dt_vigencia_inicial,
            cd_versao
          )
        SELECT 
            cid_doenca_versao_seq.NEXTVAL,                             	-- nr_sequencia
            SYSDATE,                                                   	-- dt_atualizacao
            NM_USUARIO_P,                                               -- nm_usuario
            SYSDATE,                                                   	-- dt_atualizacao_nrec
            NM_USUARIO_P,                                               -- nm_usuario_nrec
            doe.cd_doenca_cid,                                         	-- cd_doenca_cid
            trunc(SYSDATE, 'yyyy'), 									-- dt_versao
            SYSDATE,                                                    -- dt_vigencia_inicial
            'ICD-10'                                                    -- cd_versao
        FROM cid_doenca_versao doe_ver 
            INNER JOIN cid_doenca doe ON doe_ver.cd_doenca_cid = doe.cd_doenca_cid; 

    COMMIT;
    END;
    -- END CID_DOENCA

execute immediate 'truncate table TASY.icd_chapter_aut';
execute immediate 'truncate table TASY.icd_category_aut';

COMMIT;

END PROCCESS_ICD_STAMMDATEN;
/