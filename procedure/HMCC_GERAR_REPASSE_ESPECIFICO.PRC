create or replace
procedure HMCC_GERAR_REPASSE_ESPECIFICO(nr_repasse_terceiro_p	number) is

nr_sequencia_item_w	number(18);
nr_seq_terceiro_w		number(18);
cont_w			number(18);
vl_desconto_w		number(18,4);
vl_repasse_desc_w	number(18,4);
dt_mesano_referencia_w	date;
cd_centro_custo_w	number(8);
cd_medico_w		varchar2(10);

cursor	c01 is
select	distinct
	c.cd_centro_custo
from	procedimento d,
	setor_atendimento c,
	procedimento_paciente b,
	procedimento_repasse a
where	(d.cd_grupo_proc in (10107,50010,31601) or b.cd_procedimento = 10014 or b.cd_procedimento = 10106146 or b.cd_procedimento = 31601014)
and	b.ie_origem_proced	= d.ie_origem_proced
and	b.cd_procedimento	= d.cd_procedimento
and	b.cd_setor_atendimento	= c.cd_setor_atendimento
and	(b.cd_setor_atendimento	in (181,76,174,125) or b.cd_setor_atendimento = 116 and b.cd_procedimento = 31601014)
and	obter_tipo_convenio(b.cd_convenio)	= 2
--and	b.cd_procedimento	<> 10057
and	a.nr_seq_procedimento	= b.nr_sequencia
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
and	b.cd_especialidade	not in (66)
and	b.dt_procedimento	> to_date('01/09/2013','dd/mm/yyyy');


cursor	c02 is
select	distinct
	b.cd_medico_executor
from	procedimento d,
	setor_atendimento c,
	procedimento_paciente b,
	procedimento_repasse a
where	(d.cd_grupo_proc in (10107,50010,31601) or b.cd_procedimento = 10014 or b.cd_procedimento = 10106146 or b.cd_procedimento = 31601014)
and	b.ie_origem_proced	= d.ie_origem_proced
and	b.cd_procedimento	= d.cd_procedimento
and	nvl(c.cd_centro_custo,0)	= nvl(cd_centro_custo_w,0)
and	b.cd_setor_atendimento	= c.cd_setor_atendimento
and	(b.cd_setor_atendimento	in (181,76,174,125) or b.cd_setor_atendimento = 116 and b.cd_procedimento = 31601014)
and	obter_tipo_convenio(b.cd_convenio)	= 2
--and	b.cd_procedimento	<> 10057
and	a.nr_seq_procedimento	= b.nr_sequencia
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
and	b.cd_especialidade	not in (66)
and	b.dt_procedimento	> to_date('01/09/2013','dd/mm/yyyy');

begin
vl_repasse_desc_w	:= 0;
select	max(a.dt_mesano_referencia),
	max(nr_seq_terceiro)
into	dt_mesano_referencia_w,
	nr_seq_terceiro_w
from	repasse_terceiro a
where	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;

open	c01;
loop
fetch	c01 into
	cd_centro_custo_w;
exit	when c01%notfound;

	open	c02;
	loop
	fetch	c02 into
		cd_medico_w;
	exit	when c02%notfound;

		select	sum(b.qt_procedimento) -- quantidade lan�ada
		into	cont_w
		from	procedimento d,
			setor_atendimento c,
			procedimento_paciente b,
			procedimento_repasse a
		where	(d.cd_grupo_proc in (10107,50010) or b.cd_procedimento = 10014 or b.cd_procedimento = 10106146)
		and	b.ie_origem_proced	= d.ie_origem_proced
		and	b.cd_procedimento	= d.cd_procedimento
		and	nvl(c.cd_centro_custo,0)	= nvl(cd_centro_custo_w,0)
		and	b.cd_setor_atendimento	= c.cd_setor_atendimento
		and	b.cd_setor_atendimento	in (181,76,174,125)
		and	obter_tipo_convenio(b.cd_convenio)	= 2
		and	nvl(b.cd_medico_executor,'X')	= nvl(cd_medico_w,'X')
		--and	b.cd_procedimento	<> 10057
		and	a.nr_seq_procedimento	= b.nr_sequencia
		and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
		and	b.cd_especialidade	not in (66)
		and	b.dt_procedimento	between to_date('01/09/2014','dd/mm/yyyy') and to_date('31/08/2015','dd/mm/yyyy');

		if	(cont_w > 0 and cont_w < 121) then
			vl_desconto_w	:= 11.89;
		elsif	(cont_w > 120) then
			vl_desconto_w	:=  9.51;
		end if;
		vl_repasse_desc_w	:= (vl_desconto_w * cont_w) * -1;
		
		if	(vl_repasse_desc_w	<> 0) then
			select	nvl(max(nr_sequencia_item),0) + 1
			into	nr_sequencia_item_w
			from	repasse_terceiro_item
			where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

			insert into repasse_terceiro_item
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				vl_repasse,
				nr_repasse_terceiro,
				nr_sequencia_item,
				ds_observacao,
				dt_contabil,
				cd_centro_custo_prov,
				nr_seq_trans_fin_prov,
				cd_medico,
				dt_lancamento,
				nr_seq_terceiro)
			values	(repasse_terceiro_item_seq.nextval,
				sysdate,
				'Tasy',
				vl_repasse_desc_w,
				nr_repasse_terceiro_p,
				nr_sequencia_item_w,
				'Desconto gerado pela regra de repasse espec�fico. Procedure: HMCC_GERAR_REPASSE_ESPECIFICO',
				dt_mesano_referencia_w,
				cd_centro_custo_w,
				5081,
				cd_medico_w,
				dt_mesano_referencia_w,
				nr_seq_terceiro_w);
		end if;		

		select	sum(b.qt_procedimento) -- quantidade lan�ada
		into	cont_w
		from	procedimento d,
			setor_atendimento c,
			procedimento_paciente b,
			procedimento_repasse a
		where	(d.cd_grupo_proc in (10107,50010,31601) or b.cd_procedimento = 10014 or b.cd_procedimento = 10106146 or b.cd_procedimento = 31601014)
		and	b.ie_origem_proced	= d.ie_origem_proced
		and	b.cd_procedimento	= d.cd_procedimento
		and	nvl(c.cd_centro_custo,0)	= nvl(cd_centro_custo_w,0)
		and	b.cd_setor_atendimento	= c.cd_setor_atendimento
		and	(b.cd_setor_atendimento	in (181,76,174,125) or b.cd_setor_atendimento = 116 and b.cd_procedimento = 31601014)
		and	obter_tipo_convenio(b.cd_convenio)	= 2
		and	nvl(b.cd_medico_executor,'X')	= nvl(cd_medico_w,'X')
		--and	b.cd_procedimento	<> 10057
		and	a.nr_seq_procedimento	= b.nr_sequencia
		and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
		and	b.cd_especialidade	not in (66)
		and	b.dt_procedimento	>= to_date('01/09/2015','dd/mm/yyyy');
				
		if	(cont_w > 0 and cont_w < 121) then
			vl_desconto_w	:= 13.02;
		elsif	(cont_w > 120) then
			vl_desconto_w	:= 10.41;
		end if;
		
		vl_repasse_desc_w	:= (vl_desconto_w * cont_w) * -1;
		
		if	(vl_repasse_desc_w <> 0) then
		
			select	nvl(max(nr_sequencia_item),0) + 1
			into	nr_sequencia_item_w
			from	repasse_terceiro_item
			where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

			insert into repasse_terceiro_item
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				vl_repasse,
				nr_repasse_terceiro,
				nr_sequencia_item,
				ds_observacao,
				dt_contabil,
				cd_centro_custo_prov,
				nr_seq_trans_fin_prov,
				cd_medico,
				dt_lancamento,
				nr_seq_terceiro)
			values	(repasse_terceiro_item_seq.nextval,
				sysdate,
				'Tasy',
				vl_repasse_desc_w,
				nr_repasse_terceiro_p,
				nr_sequencia_item_w,
				'Desconto gerado pela regra de repasse espec�fico. Procedure: HMCC_GERAR_REPASSE_ESPECIFICO',
				dt_mesano_referencia_w,
				cd_centro_custo_w,
				5081,
				cd_medico_w,
				dt_mesano_referencia_w,
				nr_seq_terceiro_w);
		end if;		

	end	loop;
	close	c02;

end	loop;
close	c01;

end HMCC_GERAR_REPASSE_ESPECIFICO;
/