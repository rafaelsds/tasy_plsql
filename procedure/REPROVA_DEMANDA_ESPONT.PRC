create or replace
procedure REPROVA_DEMANDA_ESPONT(	nr_seq_demand_espont_p 	number,
									ds_motivo_reprovacao_p	varchar2
									) is 

nm_usuario_w mprev_demanda_espont.nm_usuario_reprovacao%type;

begin

nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

update mprev_demanda_espont
set dt_reprovacao = sysdate,
nm_usuario_reprovacao = nm_usuario_w,
ds_motivo_reprovacao = ds_motivo_reprovacao_p
where nr_sequencia = nr_seq_demand_espont_p;

commit;

end REPROVA_DEMANDA_ESPONT;
/