create or replace
procedure unificar_titulos_bloqueto(nr_titulo_p			number,
				ds_lista_titulo_p		varchar2,
				nr_bloqueto_p			varchar2,
				nm_usuario_p			varchar2,
				dt_emissao_p			date,
				dt_contabil_p			date,
				dt_vencto_atual_p		date,
				dt_vencto_orig_p		date,
				nr_seq_classe_p			number,
				nr_titulo_gerado_p		out	number) is

nr_titulo_w			number(10);
nr_titulo_dest_w		number(10);
vl_saldo_titulo_w		number(15,2);
vl_saldo_juros_w		number(15,2);
vl_saldo_multa_w		number(15,2);
vl_dia_antecipacao_w		number(15,2);
nr_seq_trans_financ_w		number(10);
cd_estabelecimento_w		number(4);
vl_total_w			number(15,2);
vl_total_juros_w		number(15,2);
vl_total_multa_w		number(15,2);
vl_total_desconto_w		number(15,2);
cd_moeda_padrao_w		number(5);
cd_tipo_baixa_w			number(5);
qt_classe_titulo_w		number(10);
nr_seq_classe_w			number(10);
ie_consiste_classe_w		varchar2(1);
ie_origem_tit_pagar_w		varchar2(10);
cd_tributo_w			titulo_pagar.cd_tributo%type;
qt_tributo_titulo_w		number(10);
ie_tipo_data_baixa_w	varchar2(5);
dt_baixa_tit_unif_w		date;

cursor c01 is
select	nr_titulo,
	vl_saldo_titulo,
	vl_saldo_juros,
	vl_saldo_multa,
	vl_dia_antecipacao
from	titulo_pagar
where	' ' || ds_lista_titulo_p || ' ' like '% ' || nr_titulo || ',%'
and	ds_lista_titulo_p is not null;

begin

if	(nr_titulo_p is not null) and
	(ds_lista_titulo_p is not null) then
	
	ie_consiste_classe_w	:= obter_valor_param_usuario(851, 179, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);

	if	(ie_consiste_classe_w = 'S') then
		select	max(nr_seq_classe),
			count(distinct nvl(nr_seq_classe,0))
		into	nr_seq_classe_w,
			qt_classe_titulo_w
		from	titulo_pagar
		where	' ' || ds_lista_titulo_p || ' ' like '% ' || nr_titulo || ',%';

		if	(qt_classe_titulo_w > 1) then
			Wheb_mensagem_pck.exibir_mensagem_abort(209914);
		end if;
	end if;
	

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	titulo_pagar
	where	nr_titulo	= nr_titulo_p;

	select	max(nr_seq_trans_fin_transf),
		max(cd_moeda_padrao)
	into	nr_seq_trans_financ_w,
		cd_moeda_padrao_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= cd_estabelecimento_w;

	select	nvl(sum(vl_saldo_titulo),0),
		nvl(sum(vl_saldo_juros),0),
		nvl(sum(vl_saldo_multa),0),
		nvl(sum(nvl(vl_dia_antecipacao,0)),0),
		max(cd_tributo)
	into	vl_total_w,
		vl_total_juros_w,
		vl_total_multa_w,
		vl_total_desconto_w,
		cd_tributo_w
	from	titulo_pagar
	where	' ' || ds_lista_titulo_p || ' ' like '% ' || nr_titulo || ',%'
	and	ds_lista_titulo_p is not null;

	cd_tipo_baixa_w	:= obter_valor_param_usuario(851, 101, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);

	if	(nvl(cd_tipo_baixa_w,0) = 0) then
		cd_tipo_baixa_w	:= 4;
	end if;
	
	obter_param_usuario(851, 263, obter_perfil_ativo, nm_usuario_p,obter_estabelecimento_ativo, ie_tipo_data_baixa_w);
	if (ie_tipo_data_baixa_w = 'DA') then
		dt_baixa_tit_unif_w := sysdate;
	elsif (ie_tipo_data_baixa_w = 'DE')  then
		dt_baixa_tit_unif_w := dt_emissao_p;
	elsif (ie_tipo_data_baixa_w = 'DC')  then
		dt_baixa_tit_unif_w := dt_contabil_p;
	elsif (ie_tipo_data_baixa_w = 'DVO')  then
		dt_baixa_tit_unif_w := dt_vencto_orig_p;
	elsif (ie_tipo_data_baixa_w = 'DVA')  then
		dt_baixa_tit_unif_w := dt_vencto_atual_p;
	end if;

	open c01;
	loop
	fetch c01 into
		nr_titulo_w,	
		vl_saldo_titulo_w,
		vl_saldo_juros_w,
		vl_saldo_multa_w,
		vl_dia_antecipacao_w;
	exit when c01%notfound;
	
		update	titulo_pagar
		set	ie_tipo_titulo		= 1,
			nr_bloqueto		= nr_bloqueto_p,
			nr_seq_trans_fin_baixa	= nr_seq_trans_financ_w
		where	nr_titulo		= nr_titulo_w;

		baixa_titulo_pagar(cd_estabelecimento_w,
				cd_tipo_baixa_w,
				nr_titulo_w,
				vl_saldo_titulo_w,
				nm_usuario_p,
				nr_seq_trans_financ_w,
				null,
				null,
				dt_baixa_tit_unif_w,
				null);

		atualizar_saldo_tit_pagar(nr_titulo_w,nm_usuario_p);
		Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
	
	end loop;
	close c01;

	if	(vl_total_w > 0) then	
	
		select count(*)
		into qt_tributo_titulo_w
		from (
			select 	distinct cd_tributo 
			from 	titulo_pagar
			where	' ' || ds_lista_titulo_p || ' ' like '% ' || nr_titulo || ',%'
			and	ds_lista_titulo_p is not null		
			);
			
		-- Se haver tributos diferentes nos titulos unificados nao leva pro novo titulo
		if (qt_tributo_titulo_w > 1) and (cd_tributo_w is not null) then
			cd_tributo_w := null;
		end if;	
	
		ie_origem_tit_pagar_w := obter_valor_param_usuario(851, 183, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);
		
		select	titulo_pagar_seq.nextval
		into	nr_titulo_dest_w
		from	dual;

		insert	into	titulo_pagar
			(nr_titulo,
			nm_usuario,
			dt_atualizacao,
			ie_situacao,
			ie_origem_titulo,
			ie_tipo_titulo,
			cd_estabelecimento,
			cd_tipo_taxa_multa,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_antecipacao,
			tx_multa,
			tx_juros,
			cd_moeda,
			vl_titulo,
			vl_saldo_titulo,
			dt_emissao,
			dt_contabil,
			dt_vencimento_original,
			dt_vencimento_atual,
			cd_pessoa_fisica,
			cd_cgc,
			nr_bloqueto,
			vl_saldo_juros,
			vl_saldo_multa,
			vl_dia_antecipacao,
			ds_observacao_titulo,
			cd_estab_financeiro,
			nr_seq_classe,
			cd_tributo)
		select	nr_titulo_dest_w,
			nm_usuario_p,
			sysdate,
			'A',
			ie_origem_tit_pagar_w,
			1,
			cd_estabelecimento,
			1,
			1,
			1,
			0,
			0,
			cd_moeda_padrao_w,
			vl_total_w,
			vl_total_w,
			nvl(dt_emissao_p, dt_emissao),
			trunc(nvl(dt_contabil_p, sysdate),'dd'),
			nvl(dt_vencto_orig_p, dt_vencimento_original),
			nvl(dt_vencto_atual_p, dt_vencimento_atual),
			cd_pessoa_fisica,
			cd_cgc,
			nr_bloqueto_p,
			vl_total_juros_w,
			vl_total_multa_w,
			vl_total_desconto_w,
			--'Unificacao dos titulos: ' || ds_lista_titulo_p,
			wheb_mensagem_pck.get_texto(303988,'DS_LISTA_TITULO_P='||ds_lista_titulo_p),
			cd_estab_financeiro,
			nvl(nr_seq_classe_p, nr_seq_classe_w),
			cd_tributo_w
		from	titulo_pagar
		where	nr_titulo	= nr_titulo_p;
		
		ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_dest_w, nm_usuario_p);
		gerar_titulo_pagar_classif(ds_lista_titulo_p,nr_titulo_dest_w,nm_usuario_p);

	end if;

	nr_titulo_gerado_p	:= nr_titulo_w;

end if;

commit;

end unificar_titulos_bloqueto;
/