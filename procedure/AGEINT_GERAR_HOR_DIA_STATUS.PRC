create or replace
procedure ageint_gerar_hor_dia_status(
		cd_agenda_p		number,
		cd_pessoa_fisica_p	varchar2,
		dt_agenda_p		date,
		ie_forma_apres_p	varchar2,
		ie_tipo_agendamento_p	varchar2,
		dt_inicial_p		date,
		dt_final_p		date,
		nr_mes_p		number,
		ds_mes_extenso_p out	varchar2,
		ie_status_p	out	varchar2,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2) is 

begin
	ageint_gerar_consulta_horarios(cd_agenda_p,cd_pessoa_fisica_p,dt_agenda_p,ie_forma_apres_p,ie_tipo_agendamento_p,null,cd_estabelecimento_p, null, null, nm_usuario_p, null);
	ageint_dia_status(cd_estabelecimento_p,cd_agenda_p,dt_inicial_p,dt_final_p,nm_usuario_p,ie_status_p);
	ds_mes_extenso_p := obter_mes_extenso(nr_mes_p, 'C');
commit;
end ageint_gerar_hor_dia_status;
/
