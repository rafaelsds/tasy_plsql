create or replace
procedure fis_gerar_reg_0900_efd(	nr_seq_controle_p	number,
									nm_usuario_p		varchar2,
									ds_separador_p		varchar2,
									nr_linha_p			number,
									nr_sequencia_p		in out	number) is 
									
nr_seq_registro_w	number(10) 	:= nr_sequencia_p;
sep_w				varchar2(1) := ds_separador_p;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w			varchar2(8000);	
vl_reg_A_w			fis_efd_0900.vl_registro%type;
vl_reg_F_w			fis_efd_0900.vl_registro%type;
vl_reg_I_w			fis_efd_0900.vl_registro%type;
vl_reg_total_w		fis_efd_0900.vl_registro%type;

function obter_somatorio_reg(ds_registro_p varchar2)
	return number 
as
	vl_registro_w fis_efd_0900.vl_registro%type;
begin

	select 	sum(nvl(vl_registro, 0))
	into	vl_registro_w
	from	fis_efd_0900
	where	((ds_registro = 'F100' and cd_ind_oper in ('1', '2'))
	or		ds_registro <> 'F100')
	and		cd_cst in ('01', '02', '03', '04', '05', '06', '07', '08', '09')
	and		nr_seq_controle = nr_seq_controle_p
	and		nm_usuario = nm_usuario_p
	and 	ds_registro = ds_registro_p;
	
	return nvl(vl_registro_w, 0);
	
end;

begin

	vl_reg_A_w := obter_somatorio_reg('A170');
	vl_reg_F_w := obter_somatorio_reg('F100') + obter_somatorio_reg('F500') + obter_somatorio_reg('F550');
	vl_reg_I_w := obter_somatorio_reg('I100');
	vl_reg_total_w := vl_reg_A_w + vl_reg_F_w + vl_reg_I_w;
	
	ds_linha_w	:= substr(	sep_w || '0900'	|| 		/*Texto fixo contendo 0900. */
							sep_w || sped_formatar_numericos(vl_reg_A_w, 2, ',') || 	/*Receita total do Bloco A*/
							sep_w || '' ||	/*Parcela da receita escriturada no Bloco A */
							sep_w || '0,00' || /*Receita total do Bloco C*/
							sep_w || '' || /*Parcela da receita escriturada no Bloco C*/
							sep_w || '0,00' || /*Receita total do Bloco D*/
							sep_w || '' || /*Parcela da receita escriturada no Bloco D*/
							sep_w || sped_formatar_numericos(vl_reg_F_w, 2, ',') || 	/*Receita total do Bloco F*/
							sep_w || '' ||	/*Parcela da receita escriturada no Bloco F*/
							sep_w || sped_formatar_numericos(vl_reg_I_w, 2, ',') || 	/*Receita total do Bloco I*/
							sep_w || '' ||	/*Parcela da receita escriturada no Bloco I*/
							sep_w || '0,00' || /*Receita total do Bloco 1*/
							sep_w || '' || /*Parcela da receita escriturada no Bloco 1*/
							sep_w || sped_formatar_numericos(vl_reg_total_w, 2, ',') || /*Receita bruta total*/
							sep_w || '' || /*Parcela da receita total escriturada */
							sep_w, 1, 8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	
	insert into fis_efd_arquivo	(	nr_sequencia,
									nm_usuario,
									dt_atualizacao,
									nm_usuario_nrec,
									dt_atualizacao_nrec,
									nr_seq_controle_efd,
									nr_linha,
									cd_registro,
									ds_arquivo,
									ds_arquivo_compl)
								values	(
									nr_seq_registro_w,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									sysdate,
									nr_seq_controle_p,
									nr_linha_p,
									'0900',
									ds_arquivo_w,
									ds_arquivo_compl_w);
									
	commit;
	
	nr_sequencia_p := nr_seq_registro_w;
	
end fis_gerar_reg_0900_efd;
/