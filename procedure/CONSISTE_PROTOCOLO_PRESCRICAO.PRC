create or Replace
PROCEDURE Consiste_Protocolo_prescricao(
			cd_estabelecimento_p	number,
			cd_protocolo_p		number,
			nr_sequencia_p		number,
			ie_consiste_medic_p	varchar2) IS


ie_situacao_mat_w		varchar2(1);
ie_padronizado_w		varchar2(1);
cd_unidade_medida_w	varchar2(30);
ie_situacao_unid_med_w	varchar2(1);
ds_material_w		varchar2(255);
cd_material_w		number(6);
ds_inconsitencia_W	varchar2(2000) := '';
VarPadronizado_w		Varchar2(15);

Cursor C01 is
	select	nvl(b.ie_situacao,'A'),
		nvl(substr(obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material),1,1),'S'),
		a.cd_unidade_medida,
		nvl(c.ie_situacao,'A'),
		b.ds_material,
		b.cd_material
	from	unidade_medida c,
		material b,
		protocolo_medic_material a
	where	a.cd_material	= b.cd_material
	and	a.cd_protocolo	= cd_protocolo_p
	and	a.nr_sequencia	= nr_sequencia_p
	and	a.ie_agrupador not in (20,21)
	and	c.cd_unidade_medida	= a.cd_unidade_medida (+)
	order by b.cd_material;

BEGIN
/* Virg�lio 24/12/2009 N�o l� valor do usu�rio/perfil "hohoho"
select	nvl(vl_parametro, vl_parametro_padrao)
into	VarPadronizado_w
from	funcao_parametro
where	cd_funcao = 924
and	nr_sequencia = 18;
*/

OPEN C01;
LOOP
FETCH C01 into
	ie_situacao_mat_w,
	ie_padronizado_w,
	cd_unidade_medida_w,
	ie_situacao_unid_med_w,
	ds_material_w,
	cd_material_w;
exit when c01%notfound;
	if	(ie_situacao_mat_w = 'I') then
		ds_inconsitencia_W	:= substr(ds_inconsitencia_W || 
										-- Mat/Med inativo: #@CD_MATERIAL#@ #@DS_MATERIAL#@
										obter_texto_dic_objeto( 261354 , Wheb_usuario_pck.get_nr_seq_idioma, 'CD_MATERIAL=' ||cd_material_w|| ';DS_MATERIAL=' ||ds_material_w ) 
											|| chr(13) || chr(10),1,2000);
	end if;
	if	(ie_padronizado_w = 'N') and
		(ie_consiste_medic_p = 'O') then
		ds_inconsitencia_W	:= substr(ds_inconsitencia_W ||
										-- Mat/Med n�o padronizado: #@CD_MATERIAL#@ #@DS_MATERIAL#@
										obter_texto_dic_objeto( 261356 , Wheb_usuario_pck.get_nr_seq_idioma, 'CD_MATERIAL=' ||cd_material_w|| ';DS_MATERIAL=' ||ds_material_w ) 
											|| chr(13) || chr(10),1,2000);
	end if;
	if	(ie_situacao_unid_med_w = 'I') then
		ds_inconsitencia_W	:= substr(ds_inconsitencia_W ||
										-- Mat/Med com unidade medida inativa: #@CD_MATERIAL#@ #@DS_MATERIAL#@
										obter_texto_dic_objeto( 261357 , Wheb_usuario_pck.get_nr_seq_idioma, 'CD_MATERIAL=' ||cd_material_w|| ';DS_MATERIAL=' ||ds_material_w ) 
											|| chr(13) || chr(10),1,2000);
	end if;
END LOOP;
Close C01;

if	(ds_inconsitencia_W is not null) then
	-- #@DS_INCONSISTENCIA#@ Confira o protocolo!!!
	Wheb_mensagem_pck.exibir_mensagem_abort( 261359 , 'DS_INCONSISTENCIA='||ds_inconsitencia_W);
end if;

END Consiste_Protocolo_prescricao;
/