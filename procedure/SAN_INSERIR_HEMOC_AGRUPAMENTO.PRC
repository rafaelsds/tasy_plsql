create or replace
procedure San_Inserir_Hemoc_Agrupamento(nr_seq_agrupamento_p	Number,
					nr_seq_producao_p	Number,
					nm_usuario_p		Varchar2) is 

begin

if (nr_seq_agrupamento_p is not null) and (nr_seq_producao_p is not null) then

	insert into san_agrupamento_derivado (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_agrupamento,
		nr_seq_producao)
	values(	san_agrupamento_derivado_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_agrupamento_p,
		nr_seq_producao_p);
end if;
		
commit;

end San_Inserir_Hemoc_Agrupamento;
/