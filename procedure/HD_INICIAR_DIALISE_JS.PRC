create or replace
procedure hd_iniciar_dialise_js(nr_seq_dialise_p	number,
				nr_seq_dialisador_p	number,
				dt_inicio_p		date,
				nm_usuario_p		varchar2,
				nr_atendimento_p	number,
				ie_forma_geracao_p	varchar2,
				cd_setor_atendimento_p	number,
				nr_seq_dialise_atual_p	number,
				ie_inserir_conta_p	varchar2,
				ds_erro_p		out Varchar2) is 
				
ie_tipo_dialise_w	varchar2(15);

begin

hd_iniciar_dialise(nr_seq_dialise_p, nr_seq_dialisador_p, dt_inicio_p, nm_usuario_p,ds_erro_p);

if	(ie_inserir_conta_p = 'S') then

	select	max(ie_tipo_dialise) 
	into	ie_tipo_dialise_w
	from 	hd_dialise 
	where 	nr_sequencia = nr_seq_dialise_p;

	hd_inserir_proced_conta(nr_atendimento_p, ie_forma_geracao_p, nm_usuario_p, cd_setor_atendimento_p, ie_tipo_dialise_w);
end if;

end hd_iniciar_dialise_js;
/