create or replace
procedure pls_alterar_dt_fim_comunic_web
			(	dt_nova_data_fim_p	date,
				nr_seq_comunic_p	number,
				nm_usuario_p		Varchar2) is 

begin

update	pls_comunic_externa_web
set	dt_fim_liberacao = dt_nova_data_fim_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_comunic_p;

commit;

end pls_alterar_dt_fim_comunic_web;
/
