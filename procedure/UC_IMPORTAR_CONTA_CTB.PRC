create or replace
procedure uc_importar_conta_ctb is 

cd_conta_w	varchar2(20);
ds_conta_w	varchar2(80);
ie_centro_w	varchar2(1);
ie_tipo_w	varchar2(1);
cd_classif_w	varchar2(40);
cd_grupo_w	varchar2(10);
cd_empresa_w	varchar2(1)	:= 1;
qt_registro_w	number(10);
qt_commit_w	number(10)	:= 0;
ds_erro_w	varchar2(300);

Cursor C01 is
select	substr(a.cd_conta,1,20) cd_conta,
	substr(a.ds_conta,1,80) ds_conta,
	substr(a.ie_centro,1,1) ie_centro,
	substr(a.ie_tipo,1,1) ie_tipo,
	substr(a.cd_classif,1,40) cd_classif,
	substr(a.cd_grupo,1,10) cd_grupo	
from	w_uc_plano_conta_2011 a;

begin

open c01;
loop
fetch c01 into
	cd_conta_w,
	ds_conta_w,
	ie_centro_w,
	ie_tipo_w,
	cd_classif_w,
	cd_grupo_w;
exit when C01%notfound;

	qt_commit_w	:= qt_commit_w + 1;
	
	if	(cd_conta_w is not null) then
		select	count(*)
		into	qt_registro_w
		from	conta_contabil
		where	cd_conta_contabil = cd_conta_w;
	else
		qt_registro_w	:= 0;
		select	max(somente_numero(cd_conta_contabil)) + 1
		into	cd_conta_w
		from	conta_contabil;
	end if;
	
	
	if	(qt_registro_w = 0) then
		begin
		insert into conta_contabil(
			cd_conta_contabil,
			ds_conta_contabil,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			ie_centro_custo,
			ie_tipo,
			cd_classificacao,
			cd_grupo,
			cd_empresa,
			ie_compensacao,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	cd_conta_w,
			ds_conta_w,
			'A',
			sysdate,
			'Importacao',
			ie_centro_w,
			ie_tipo_w,
			cd_classif_w,
			cd_grupo_w,
			cd_empresa_w,
			'N',
			null,
			null,
			sysdate,
			'Importacao');
		exception when others then 	
			ds_erro_w	:= sqlerrm(sqlcode);
			insert into log_tasy(
				cd_log,
				ds_log,
				nm_usuario,
				dt_atualizacao)
			values(	1225,
				'Conta n�o importada: ' || cd_conta_w || ' ERRO: ' || ds_erro_w,
				'Importacao',
				sysdate);
		end;
	end if;
	if	(qt_commit_w >= 3000) then
		commit;
		qt_commit_w	:= 0;
	end if;
end loop;
close c01;
commit;
end uc_importar_conta_ctb;
/