create or replace
procedure informar_exame_refeito ( 	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					nm_usuario_p		Varchar2 ) is 


nr_seq_prescr_proced_hist_w		number(10);
					
Cursor C01 is
	select 	a.nr_sequencia
	from   	prescr_procedimento_hist a,
		prescr_proc_hist_status b
	where  	a.nr_seq_prescr_status_hist = b.nr_sequencia   
	and	a.nr_prescricao = nr_prescricao_p
	and	a.nr_seq_procedimento = nr_seq_prescr_p
	and	b.ie_refazer_exame = 'S'
	and	a.ie_exame_refeito = 'N';
begin

open C01;
loop
fetch C01 into	
	nr_seq_prescr_proced_hist_w;
exit when C01%notfound;
	begin
		
	update	prescr_procedimento_hist
	set	ie_exame_refeito = 'S'
	where	nr_sequencia = nr_seq_prescr_proced_hist_w;
	
	end;
end loop;
close C01;

commit;

end informar_exame_refeito;
/