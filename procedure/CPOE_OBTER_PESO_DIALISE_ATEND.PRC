create or replace 
procedure cpoe_obter_peso_dialise_atend(
							nr_atendimento_p		number,
							qt_peso_atual_p			out number,
							qt_peso_ideal_p			out number,
							qt_peso_pos_p			out number) is

qt_peso_pos_w			cpoe_dialise.qt_peso_pos%type;
qt_peso_ideal_w			cpoe_dialise.qt_peso_ideal%type;
qt_peso_atual_w			cpoe_dialise.qt_peso_atual%type;
dt_atualizacao_w		cpoe_dialise.dt_atualizacao%type;
dt_atualizacao_ww		cpoe_dialise.dt_atualizacao%type;
nr_seq_dialise_w		cpoe_dialise.nr_sequencia%type;
qt_sinal_vital_w		number(15);

cursor c01 is
select	qt_peso_pre,
		dt_dialise
from	hd_dialise
where	nr_atendimento = nr_atendimento_p
and		trunc(dt_dialise) = trunc(sysdate)
and		qt_peso_pre is not null
order by dt_dialise;

cursor c02 is 
select	qt_peso
from	atendimento_sinal_vital
where	nr_atendimento	= nr_atendimento_p
and		qt_peso is not null
and		ie_situacao = 'A'
and		dt_liberacao is not null
and		trunc(dt_sinal_vital) = trunc(sysdate)
and		nvl(ie_rn,'N')	= 'N'
order by dt_sinal_vital;

begin

select	max(qt_peso_pos),
		max(qt_peso_ideal),
		max(dt_dialise)
into	qt_peso_pos_w,
		qt_peso_ideal_w,
		dt_atualizacao_w
from	hd_dialise
where	nr_sequencia = (	select	max(nr_sequencia)
							from	hd_dialise
							where	nr_atendimento = nr_atendimento_p);
							
select	max(nr_sequencia)
into	nr_seq_dialise_w
from	cpoe_dialise
where	nr_atendimento = nr_atendimento_p
and		ie_tipo_dialise = 'DI'
and		dt_liberacao is not null
and		dt_atualizacao_nrec > dt_atualizacao_w;

if (nvl(nr_seq_dialise_w,0) > 0) then
select	qt_peso_pos,
		qt_peso_ideal
into	qt_peso_pos_w,
		qt_peso_ideal_w
from	cpoe_dialise
where	nr_sequencia = nr_seq_dialise_w;
end if;

open c01;
loop
fetch c01 into 	qt_peso_atual_w,
				dt_atualizacao_ww;
exit when c01%notfound;
end loop;
close c01;

if (dt_atualizacao_ww is not null) then 
	select	count(*)
	into	qt_sinal_vital_w
	from	atendimento_sinal_vital
	where	nr_atendimento	= nr_atendimento_p
	and		qt_peso is not null
	and		ie_situacao = 'A'
	and		dt_liberacao is not null
	and		dt_sinal_vital > dt_atualizacao_ww
	and		nvl(ie_rn,'N')	= 'N'
	order by dt_sinal_vital;
end if;

if	(nvl(qt_peso_atual_w,0) = 0) or
	(qt_sinal_vital_w > 0) then
open c02;
loop
fetch c02 into qt_peso_atual_w;
exit when c02%notfound;
end loop;
close c02;
end if;

qt_peso_pos_p	:= qt_peso_pos_w;
qt_peso_ideal_p	:= qt_peso_ideal_w;
qt_peso_atual_p	:= qt_peso_atual_w;

end cpoe_obter_peso_dialise_atend;
/
