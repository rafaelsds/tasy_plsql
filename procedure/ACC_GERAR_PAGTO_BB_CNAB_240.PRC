create or replace
procedure ACC_GERAR_PAGTO_BB_CNAB_240
		(	nr_seq_envio_p		number,
			nm_usuario_p		varchar2) is


ds_conteudo_w		varchar2(240);
cd_estabelecimento_w	number(4);
nr_seq_apres_w		number(10)	:= 0;
cd_banco_hosp_w		number(3);

/* header de arquivo */
cd_agencia_estab_w	varchar2(6);
cd_cgc_estab_w		varchar2(14);
cd_convenio_banco_w	varchar2(20);
dt_geracao_w		varchar2(14);
nm_empresa_w		varchar2(30);
nr_conta_estab_w	varchar2(12);
nr_remessa_w		number(10);
ie_digito_estab_w	varchar2(1);

/* header de lote - DOC */
nr_lote_servico_w	number(10);
ie_forma_lanc_w		varchar2(2);
ie_tipo_pagamento_w	varchar2(3);
ie_finalidade_w		varchar2(2);
ds_endereco_w		varchar2(30);
nr_endereco_w		varchar2(5);
ds_complemento_w	varchar2(15);
ds_municipio_w		varchar2(20);
nr_cep_w		varchar2(8);
sg_estado_w		varchar2(15);

cursor	c01 is
select	distinct
	decode(b.ie_tipo_pagamento,'CC','01','DOC',decode(b.cd_banco,cd_banco_hosp_w,'01','03'),'TED','41','OP',30),
	b.ie_tipo_pagamento,
	decode(b.ie_tipo_pagamento,'CC','01','99')
from	titulo_pagar_escrit b,
	banco_escritural a
where	b.ie_tipo_pagamento	in ('DOC','CC','OP','TED')
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - DOC */
nr_sequencia_w		number(10);
cd_camara_compensacao_w	varchar2(3);
cd_banco_w		varchar2(3);
cd_agencia_bancaria_w	varchar2(6);
nr_conta_w		varchar2(20);
nm_pessoa_w		varchar2(30);
nr_titulo_w		number(10);
dt_remessa_retorno_w	date;
vl_escritural_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_despesa_w		number(15,2);
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(14);
dt_vencimento_w		date;
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
cd_agencia_conta_w	varchar2(20);
ie_digito_conta_w	varchar2(1);
ds_endereco_det_w	varchar2(30);
nr_endereco_det_w	varchar2(5);
ds_complemento_det_w	varchar2(15);
ds_municipio_det_w	varchar2(20);
nr_cep_det_w		varchar2(8);
sg_estado_det_w		varchar2(2);
ds_bairro_det_w		varchar2(15);

cursor	c02 is
select	nvl(nvl((select	x.cd_banco_externo
	from	banco x
	where	x.cd_banco	= b.cd_banco),to_char(b.cd_banco)),'000') cd_banco,
	lpad(substr(somente_numero(nvl(b.cd_agencia_bancaria,'0')),1,5) || substr(trim(nvl(b.ie_digito_agencia,'0')),1,1),6,'0'),
	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30))),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	lpad(nvl(d.nr_cpf,c.cd_cgc),14,'0'),
	nvl(b.nr_conta,'0'),
	substr(nvl(b.ie_digito_conta,'0'),1,1),
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	decode(c.cd_pessoa_fisica,null,'2','1') ie_tipo_inscricao,
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'R'),1,30))),' '),30,' '),
	lpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'NR'),1,5),'0'),5,'0'),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CO'),1,15))),' '),15,' '),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CI'),1,20))),' '),20,' '),
	lpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CEP'),1,8),'0'),8,'0'),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'UF'),1,2))),' '),2,' '),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'B'),1,15))),' '),15,' ')
from	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	ie_forma_lanc_w		= decode(b.ie_tipo_pagamento,'CC','01','DOC',decode(b.cd_banco,cd_banco_hosp_w,'01','03'),'TED','41','OP',30)
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	= ie_tipo_pagamento_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer lote - DOC */
vl_total_w		number(15,2);

/* header de lote - BLQ */
cursor	c03 is
select	distinct
	decode(b.cd_banco,cd_banco_hosp_w,'30','31'),
	b.ie_tipo_pagamento
from	titulo_pagar_escrit b,
	banco_escritural a
where	b.ie_tipo_pagamento	= 'BLQ'
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - BLQ */
nr_bloqueto_w		varchar2(44);
nr_nosso_numero_w	varchar2(20);

cursor	c04 is
select	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30))),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	rpad(substr(c.nr_bloqueto,1,44),44,' '),
	rpad(substr(c.nr_nosso_numero,1,20),20,' ')
from	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	ie_forma_lanc_w		= decode(b.cd_banco,cd_banco_hosp_w,'30','31')
and	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	= ie_tipo_pagamento_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer do arquivo */
qt_registro_w		number(10);

begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* header de arquivo */
nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

select	nvl((select	somente_numero(nvl(max(x.cd_banco_externo),to_char(c.cd_banco)))
	from	banco x
	where	x.cd_banco	= c.cd_banco), c.cd_banco),
	lpad(b.cd_cgc,14,'0'),
	lpad(substr(nvl(c.cd_convenio_banco,' '),1,9),9,'0'),
	lpad(substr(elimina_caractere_especial(c.cd_agencia_bancaria),1,5) || substr(elimina_caractere_especial(d.ie_digito),1,1),6,'0'),
	lpad(somente_numero(substr(c.cd_conta,1,12)),12,'0'),
	rpad(upper(elimina_acentuacao(substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30))),30,' '),
	to_char(sysdate,'ddmmyyyyhhmiss'),
	nvl(a.nr_remessa,a.nr_sequencia),
	b.cd_estabelecimento,
	a.dt_remessa_retorno,
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(null,b.cd_cgc,'R'),1,30))),' '),30,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'NR'),1,5),'0'),5,'0'),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(null,b.cd_cgc,'CO'),1,15))),' '),15,' '),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(null,b.cd_cgc,'CI'),1,20))),' '),20,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CEP'),1,8),'0'),8,'0'),
	rpad(nvl(upper(elimina_acentuacao(substr(obter_dados_pf_pj(null,b.cd_cgc,'UF'),1,2))),' '),2,' '),
	nvl(c.ie_digito_conta,'0')
into	cd_banco_hosp_w,
	cd_cgc_estab_w,
	cd_convenio_banco_w,
	cd_agencia_estab_w,
	nr_conta_estab_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_remessa_w,
	cd_estabelecimento_w,
	dt_remessa_retorno_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_municipio_w,
	nr_cep_w,
	sg_estado_w,
	ie_digito_estab_w
from	agencia_bancaria d,
	banco_estabelecimento c,
	estabelecimento b,
	banco_escritural a
where	c.cd_banco			= d.cd_banco
and	c.cd_agencia_bancaria	= d.cd_agencia_bancaria
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia			= nr_seq_envio_p;

cd_convenio_banco_w := cd_convenio_banco_w ||
						'0126' ||
						rpad(' ',7,' ');

ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') || --1 3
			'0000' || -- 4 7
			'0' || -- 8 8
			rpad(' ',9,' ') || -- 9 17
			'2' || --  18 18
			cd_cgc_estab_w || --19 32
			cd_convenio_banco_w || -- 33 52
			cd_agencia_estab_w ||
			nr_conta_estab_w ||
			ie_digito_estab_w ||
			' ' ||
			nm_empresa_w ||
			rpad('BANCO DO BRASIL',30,' ') ||
			rpad(' ',10,' ') ||
			'1' ||
			dt_geracao_w ||
			lpad(nr_remessa_w,6,'0') ||
			'084' ||
			'00000' ||
			rpad(' ',69,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

open	c01;
loop
fetch	c01 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w,
	ie_finalidade_w;
exit	when c01%notfound;

	/* header de lote */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w			:= 0;

	ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') || -- 1 3
				lpad(nr_lote_servico_w,4,'0') || -- 4 7
				'1' || -- 8 8
				'C' || -- 9 9
				'20' || -- 10 11
				ie_forma_lanc_w || -- 12 13
				'043' || -- 14 16
				' ' || -- 17 17 
				'2' || -- 18 18
				cd_cgc_estab_w || -- 19 32
				cd_convenio_banco_w || -- 33 52
				cd_agencia_estab_w ||
				nr_conta_estab_w ||
				ie_digito_estab_w ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c02;
	loop
	fetch	c02 into
		cd_banco_w,
		cd_agencia_bancaria_w,
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		nr_inscricao_w,
		nr_conta_w,
		ie_digito_conta_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		ie_tipo_inscricao_w,
		ds_endereco_det_w,
		nr_endereco_det_w,
		ds_complemento_det_w,
		ds_municipio_det_w,
		nr_cep_det_w,
		sg_estado_det_w,
		ds_bairro_det_w;
	exit	when c02%notfound;

		/* segmento A */
		nr_sequencia_w		:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w		:= nvl(qt_registro_w,0) + 1;
		vl_total_w			:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0));

		ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||
					'A' ||
					'000' ||
					'000' ||
					lpad(cd_banco_w,3,'0') ||
					lpad(cd_agencia_bancaria_w,6,'0') ||
					lpad(substr(nr_conta_w,1,12),12,'0') ||
					ie_digito_conta_w ||
					' ' ||
					nm_pessoa_w ||
					lpad(nr_titulo_w,20,' ') || --74 93
					to_char(dt_remessa_retorno_w,'ddmmyyyy') || --94 101
					'BRL' || --102 104
					'000000000000000' || -- 105 119
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') || -- 120 134
					rpad(' ',20,' ') || -- 135 154
					'00000000' ||
					'000000000000000' ||
					rpad(' ',52,' ') ||
					'0' ||
					rpad(' ',10,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

		/* segmento B */
		nr_sequencia_w		:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w		:= nvl(qt_registro_w,0) + 1;

		ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||
					'B' ||
					rpad(' ',3,' ') ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					ds_endereco_det_w ||
					nr_endereco_det_w ||
					ds_complemento_det_w ||
					ds_bairro_det_w ||
					ds_municipio_det_w ||
					nr_cep_det_w ||
					sg_estado_det_w ||
					to_char(dt_vencimento_w,'ddmmyyyy') ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
					'000000000000000' ||
					lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
					rpad(' ',15,' ') ||
					' ' ||
					rpad(' ',14,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

	end	loop;
	close	c02;

	/* trailer de lote */
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				'000000' ||
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c01;

open	c03;
loop
fetch	c03 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w;
exit	when c03%notfound;

	/* header de lote - BLQ */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w			:= 0;

	ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				'98' ||
				ie_forma_lanc_w ||
				'030' ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				cd_convenio_banco_w ||
				cd_agencia_estab_w ||
				nr_conta_estab_w ||
				ie_digito_estab_w ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c04;
	loop
	fetch	c04 into
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		nr_bloqueto_w,
		nr_nosso_numero_w;
	exit	when c04%notfound;

		/* segmento J */
		nr_sequencia_w		:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w		:= nvl(qt_registro_w,0) + 1;
		vl_total_w			:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0));

		ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||
					'J' ||
					'000' ||
					nr_bloqueto_w ||
					nm_pessoa_w ||
					to_char(dt_vencimento_w,'ddmmyyyy') ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_multa_w,0) + nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
					to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||
					'00000' ||
					rpad(nr_titulo_w,20,' ') ||
					rpad(nr_nosso_numero_w,20,' ') ||
					'00' ||
					rpad(' ',16,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);
			
				
		/* Para t�tulos com valor igual ou superior a 250.000,00 o segmento J52 � obrigat�rio */
		if (nvl(vl_escritural_w,0) >= 250000) then

			nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
			nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
			qt_registro_w	:= nvl(qt_registro_w,0) + 1;

			ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'J' ||
						' ' ||
						rpad(' ',2,' ') ||
						'52' ||
						'2' ||
						lpad(cd_cgc_estab_w,15,'0') ||
						rpad(nm_empresa_w,40,' ') ||
						ie_tipo_inscricao_w ||
						lpad(nr_inscricao_w,15,'0') ||
						rpad(nm_pessoa_w,40,' ') ||
						'0' ||
						rpad('0',15,'0') ||
						rpad(' ',40,' ') ||
						rpad(' ',53,' ');
						
		
	        insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values (cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);			
		end if;
	end	loop;
	close	c04;
	
				

	/* trailer de lote */
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				'000000' ||
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c03;

/* trailer de arquivo */
nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w	:=	lpad(cd_banco_hosp_w,3,'0') ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			lpad(nr_lote_servico_w,6,'0') ||
			lpad(nvl(qt_registro_w,0) + (nvl(nr_lote_servico_w,0) * 2) + 2,6,'0') ||	/* registros detalhe + header e trailer dos lotes + header e trailer do arquivo */
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

commit;

end ACC_GERAR_PAGTO_BB_CNAB_240;
/
