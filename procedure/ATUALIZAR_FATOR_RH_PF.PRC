create or replace
procedure atualizar_fator_rh_pf(cd_pessoa_fisica_p 	Varchar2,
								ie_exame_pac_p		Varchar2,
								ds_resultado_p		Varchar2,
								nm_usuario_p		Varchar2) is 

ds_resultado_w	Varchar2(255);					
					
begin

if (cd_pessoa_fisica_p is not null) and (ds_resultado_p is not null) then

	if (upper(ds_resultado_p) = 'POSITIVO') or (ds_resultado_p = '+') then
		ds_resultado_w := '+';
	elsif (upper(ds_resultado_p) = 'NEGATIVO') or (ds_resultado_p = '-') then
		ds_resultado_w := '-';
	elsif (upper(ds_resultado_p) = 'A') then
		ds_resultado_w := 'A';
	elsif (upper(ds_resultado_p) = 'B') then
		ds_resultado_w := 'B';
	elsif (upper(ds_resultado_p) = 'AB') then
		ds_resultado_w := 'AB';
	elsif (upper(ds_resultado_p) = 'O') then
		ds_resultado_w := 'O';
	end if;

	if (ie_exame_pac_p = 'GS') then
		update 	pessoa_fisica
		set		ie_tipo_sangue = ds_resultado_w
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	elsif (ie_exame_pac_p = 'FR') then
		update 	pessoa_fisica
		set		ie_fator_rh = ds_resultado_w
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	end if;
end if;
	
commit;

end atualizar_fator_rh_pf;
/