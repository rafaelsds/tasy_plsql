create or replace
procedure enviar_comunic_consumo_lote(
			nr_seq_lote_fornec_p	number,
			qt_saldo_p		number,
			qt_atendimento_p		number,
			cd_local_estoque_p	number,
			nr_documento_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is 

/*informações da regra*/
ds_titulo_w		varchar2(80);
ds_comunicacao_w		varchar2(2000);
cd_setor_atendimento_w	number(5);
nm_usuarios_adic_w	varchar2(255);
cd_perfil_w		number(5);
ie_ci_lida_w		varchar2(1);
nr_seq_comunic_w		number(10);

/*macros*/
nr_lote_fornec_w		number(10);
ds_lote_fornec_w		varchar2(20);
dt_validade_w		varchar2(20);
qt_saldo_w		number(13,4);
qt_atendimento_w		number(13,4);
nr_documento_w		varchar2(255);
nm_usuario_w		varchar2(15);
nm_pessoa_usuario_w	varchar2(60);
ds_local_estoque_w	varchar2(40);
ds_setor_atendimento_w	varchar2(100);
ds_perfil_ativo_w		varchar2(40);
ds_funcao_ativa_w		varchar2(80);
ds_estacao_w		varchar2(80);

cd_material_w		number(6);
ds_material_w		varchar2(255);
dt_atual_w		varchar(20) := PKG_DATE_FORMATERS.TO_VARCHAR(SYSDATE, 'timestamp', cd_estabelecimento_p, nm_usuario_p);
	
cursor c01 is
	select	ds_titulo,
		ds_comunicacao,
		cd_setor_destino,
		nm_usuarios_adic,
		cd_perfil,
		ie_ci_lida
	from	regra_envio_comunic_compra a,
		regra_envio_comunic_evento b
	where	a.nr_sequencia = b.nr_seq_regra
	and	b.ie_situacao = 'A'
	and	cd_evento = 61
	and	a.cd_funcao = 143
	and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,nr_seq_lote_fornec_p,'LF',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';
	
begin
nr_lote_fornec_w := null;

open c01;
loop
fetch c01 into	
	ds_titulo_w,
	ds_comunicacao_w,
	cd_setor_atendimento_w,
	nm_usuarios_adic_w,
	cd_perfil_w,
	ie_ci_lida_w;
exit when c01%notfound;
	begin
	if	(nr_lote_fornec_w is null) then
		begin
		nm_usuario_w := nm_usuario_p;
		begin
		nm_pessoa_usuario_w := substr(obter_pessoa_fisica_usuario(nm_usuario_w,'D'),1,60);
		exception
		when others then
			nm_pessoa_usuario_w := null;
		end;
		begin
		ds_estacao_w := substr(wheb_usuario_pck.get_nm_estacao,1,80);
		exception
		when others then
			ds_estacao_w := null;
		end;
		begin
		ds_funcao_ativa_w := substr(obter_desc_funcao(obter_funcao_ativa),1,80);
		exception
		when others then
				ds_funcao_ativa_w := null;
		end;
		begin
		ds_perfil_ativo_w := substr(obter_desc_perfil(obter_perfil_ativo),1,40);
		exception
		when others then
			ds_perfil_ativo_w := null;
		end;
		begin
		ds_setor_atendimento_w	:= substr(obter_nome_setor(obter_setor_ativo),1,100);
		exception
		when others then
			ds_setor_atendimento_w	:= null;
		end;
		
		
		select	max(ds_local_estoque)
		into	ds_local_estoque_w
		from	local_estoque
		where	cd_local_estoque = cd_local_estoque_p;
		
		
		
		nr_documento_w		:= nr_documento_p;
		qt_saldo_w		:= qt_saldo_p;
		qt_atendimento_w	:= qt_atendimento_p;
		
		begin
		select	a.nr_sequencia,
			a.ds_lote_fornec,					/*'Indeterminada'*/
			substr(nvl(PKG_DATE_FORMATERS.TO_VARCHAR(a.dt_validade, 'shortDate', cd_estabelecimento_p, nm_usuario_p), wheb_mensagem_pck.get_Texto(312731)),1,20),
			a.cd_material,
			b.ds_material
		into	nr_lote_fornec_w,
			ds_lote_fornec_w,
			dt_validade_w,
			cd_material_w,
			ds_material_w
		from	material b,
			material_lote_fornec a
		where	a.cd_material = b.cd_material
		and	a.nr_sequencia = nr_seq_lote_fornec_p;		
		exception
		when others then
			nr_lote_fornec_w := null;
		end;
		
		ds_titulo_w := replace_macro(ds_titulo_w, '@nr_lote_fornec', to_char(nr_lote_fornec_w));
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_lote_fornec', ds_lote_fornec_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@dt_validade', dt_validade_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@qt_saldo', to_char(qt_saldo_w));
		ds_titulo_w := replace_macro(ds_titulo_w, '@qt_atendimento', qt_atendimento_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@nr_documento', nr_documento_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@nm_usuario', nm_usuario_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@nm_pessoa_usuario', nm_pessoa_usuario_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_local_estoque', ds_local_estoque_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_setor_atendimento', ds_setor_atendimento_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_perfil_ativo', ds_perfil_ativo_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_funcao_ativa', ds_funcao_ativa_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_estacao', ds_estacao_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@cd_material', cd_material_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@ds_material', ds_material_w);
		ds_titulo_w := replace_macro(ds_titulo_w, '@dt_atual', dt_atual_w);
		
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@nr_lote_fornec', to_char(nr_lote_fornec_w));
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_lote_fornec', ds_lote_fornec_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@dt_validade', dt_validade_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@qt_saldo', to_char(qt_saldo_w));
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@qt_atendimento', qt_atendimento_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@nr_documento', nr_documento_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@nm_usuario', nm_usuario_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@nm_pessoa_usuario', nm_pessoa_usuario_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_local_estoque', ds_local_estoque_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_setor_atendimento', ds_setor_atendimento_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_perfil_ativo', ds_perfil_ativo_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_funcao_ativa', ds_funcao_ativa_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_estacao', ds_estacao_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@cd_material', cd_material_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@ds_material', ds_material_w);
		ds_comunicacao_w := replace_macro(ds_comunicacao_w, '@dt_atual', dt_atual_w);
		end;
	end if;
	
	if	(nr_lote_fornec_w is not null) then
		begin
		select	comunic_interna_seq.nextval
		into	nr_seq_comunic_w
		from	dual;
		
		insert	into comunic_interna(
			cd_estab_destino,
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			dt_liberacao,
			cd_perfil)
		values(	cd_estabelecimento_p,
			sysdate,
			ds_titulo_w,
			ds_comunicacao_w,
			nm_usuario_p,
			sysdate,
			'N',
			nm_usuarios_adic_w,
			nr_seq_comunic_w,
			'N',
			sysdate,
			cd_perfil_w);
			
		if	(ie_ci_lida_w = 'S') then
			insert into comunic_interna_lida(
				nr_sequencia,
				nm_usuario,
				dt_atualizacao)
			values (nr_seq_comunic_w,
				nm_usuario_p,
				sysdate);
		end if;
		end;
	end if;
	end;
end loop;
close c01;
	
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end enviar_comunic_consumo_lote;
/
