create or replace 
procedure liberar_paciente_setor(	cd_pessoa_fisica_p varchar2,
				nr_seq_paciente_p number) is

begin
       
update	paciente_setor 
set	ie_status = 'I' 
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	nr_seq_paciente <> nr_seq_paciente_p;

end	liberar_paciente_setor;
/