create or replace PROCEDURE  grava_log_tasy(
                        cd_log_p NUMBER,
                        ds_log_p VARCHAR2,
                        nm_usuario_p VARCHAR2) IS
dt_fim_geracao_w        DATE;
PRAGMA autonomous_transaction;
BEGIN
BEGIN
 SELECT  NVL(DT_FIM_GERACAO, SYSDATE)
 INTO    dt_fim_geracao_w
 FROM    tipo_log_tasy
 WHERE   cd_log = cd_log_p;
EXCEPTION
        WHEN NO_DATA_FOUND THEN
        dt_fim_geracao_w := SYSDATE;
END;
IF      (pkg_date_utils.start_of(dt_fim_geracao_w,'DD',0) >= pkg_date_utils.start_of(SYSDATE,'DD',0)) THEN
        INSERT INTO log_tasy (  dt_atualizacao,
                                nm_usuario,
                                cd_log,
                                ds_log)
                VALUES       (  SYSDATE,
                                nm_usuario_p,
                                cd_log_p,
                                SUBSTR(ds_log_p,1,2000));
END IF;
COMMIT;
END  grava_log_tasy;
/