create or replace
procedure BL_vincular_parto(	nr_seq_banco_leite_p	Number,
				nr_atendimento_p	Number,
				nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_banco_leite_p is not null) and (nr_atendimento_p is not null) then

	insert into bl_parto (	
		nr_sequencia,
		nr_seq_banco_leite,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_atendimento,
		ds_tratamento_clinico,
		dt_ultimo_parto,
		cd_medico,
		qt_gestacoes,
		qt_abortos,
		qt_sem_ig_ini_pre_natal,
		qt_sem_ig_cronologica,
		qt_dia_ig_cronologica,
		ds_uso_medic,
		ie_aids,
		ie_rubeola,
		ie_toxoplasmose,
		ds_sorologias,
		ie_sifilis,
		ie_laqueadura,
		ie_parto_normal,
		ie_parto_forceps,
		ie_parto_episio,
		ie_parto_cesaria,
		ie_imun_hepatite_c,
		ie_imun_hepatite_b,
		ie_trauma_mamilar,
		ie_mamilos,
		ie_mamas,
		ds_diagnostico,
		ie_origem,
		ie_aids_positivo,
		ie_sifilis_positivo,
		ie_rubeola_positivo,
		ie_toxoplasmose_positivo,
		dt_parto)
	select	bl_parto_seq.nextval,
		nr_seq_banco_leite_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_atendimento,
		'',
		dt_ult_parto,
		cd_medico,
		qt_gestacoes,
		qt_abortos,
		qt_sem_ig_ini_pre_natal,		
		qt_sem_ig_cronologica,
		qt_dia_ig_cronologica,		
		ds_uso_medic,
		ie_aids,
		ie_rubeola,
		ie_toxoplasmose,
		ds_sorologias,
		ie_sifilis,
		ie_laqueadura,
		ie_parto_normal,
		ie_parto_forceps,
		ie_parto_episio,
		ie_parto_cesaria,
		'N',
		ie_imun_hepatite_b,
		'N',
		ie_mamilos,
		null,
		'',
		'P',
		ie_aids_positivo,
		ie_sifilis_positivo,
		ie_rubeola_positivo,
		ie_toxoplasmose_positivo,
		dt_fim_parto
	from	parto
	where	nr_atendimento = nr_atendimento_p;

end if;

commit;

end BL_vincular_parto;
/
