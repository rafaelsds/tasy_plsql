create or replace
procedure contabiliza_perda_pre_fatur(  nr_lote_contabil_p      number,
                                nm_usuario_p    varchar2,
                                ie_exclusao_p   varchar2,
                                ds_retorno_p    out varchar2) is


cd_centro_custo_debito_w        number(10);
cd_centro_custo_w               number(10);
cd_conta_credito_w              varchar2(20);
cd_conta_debito_w               varchar2(20);
cd_convenio_w                   number(10);
cd_estabelecimento_w            number(10);
cd_historico_w                  number(10);
cd_tipo_lote_contabil_w         number(10);
ds_compl_historico_w            varchar2(255);
ds_conteudo_w                   varchar2(4000);
ds_convenio_w                   varchar2(255);
ds_titulo_w                     varchar2(120);
dt_inicial_w                    date;
dt_final_w                      date;
dt_referencia_w                 date;
ie_centro_custo_w               varchar2(1);
ie_debito_credito_w             varchar2(1);
nr_interno_conta_w              number(10);
nm_paciente_w                   varchar2(60);
nr_sequencia_w                  number(10)      := 0;
nr_seq_perda_w                  number(10);
qt_dias_conta_w                 number(10);
vl_movimento_w                  number(15,2);
nm_agrupador_w                  varchar2(255);
nr_seq_agrupamento_w            w_movimento_contabil.nr_seq_agrupamento%type;
ds_mesano_w                     varchar2(10);
nr_seq_info_ctb_w               informacao_contabil.nr_sequencia%type   := 77;
cd_procedimento_w               conta_paciente_resumo.cd_procedimento%type;
ie_origem_proced_w              conta_paciente_resumo.ie_origem_proced%type;
cd_material_w                   conta_paciente_resumo.cd_material%type;
cd_setor_atendimento_w          setor_atendimento.cd_setor_atendimento%type;
nr_seq_proc_interno_w           procedimento_paciente.nr_seq_proc_interno%type;

cursor c01 is
select  a.nr_sequencia,
        a.ds_titulo,
        a.cd_convenio,
        b.nr_interno_conta,
        b.qt_dias_conta,
        c.cd_conta_contabil,
        c.cd_procedimento,
        c.ie_origem_proced,
        c.cd_material,
        d.cd_centro_custo,
        d.cd_centro_custo_receita,
        d.cd_setor_atendimento,
        nvl(sum(c.vl_procedimento) + sum(c.vl_material),0) vl_movimento,
        c.nr_seq_proc_interno
from    setor_atendimento d,
        conta_paciente_resumo c,
        pre_fatur_perda_conta b,
        pre_fatur_perda a
where   a.nr_sequencia          = b.nr_seq_perda
and     b.nr_interno_conta      = c.nr_interno_conta
and     c.cd_setor_atendimento  = d.cd_setor_atendimento
and     b.nr_lote_contabil      = nr_lote_contabil_p
group by a.nr_sequencia,
        a.ds_titulo,
        a.cd_convenio,
        b.nr_interno_conta,
        b.qt_dias_conta,
        c.cd_conta_contabil,
        c.cd_procedimento,
        c.ie_origem_proced,
        c.cd_material,
        d.cd_centro_custo,
        d.cd_centro_custo_receita,
        d.cd_setor_atendimento,
        c.nr_seq_proc_interno;

begin

select  dt_referencia,
        cd_estabelecimento,
        cd_tipo_lote_contabil
into    dt_referencia_w,
        cd_estabelecimento_w,
        cd_tipo_lote_contabil_w
from    lote_contabil
where   nr_lote_contabil        = nr_lote_contabil_p;

dt_inicial_w    := trunc(dt_referencia_w, 'mm');
dt_final_w      := fim_mes(dt_inicial_w);

delete  w_movimento_contabil
where   nr_lote_contabil        = nr_lote_contabil_p;

if      (ie_exclusao_p = 'S') then

        delete  movimento_contabil
        where   nr_lote_contabil        = nr_lote_contabil_p;

        update  pre_fatur_perda_conta
        set     nr_lote_contabil        = null
        where   nr_lote_contabil        = nr_lote_contabil_p;

        update  lote_contabil
        set     vl_debito               = 0,
                vl_credito              = 0
        where   nr_lote_contabil        = nr_lote_contabil_p;
else
        update  pre_fatur_perda_conta a
        set     nr_lote_contabil        = nr_lote_contabil_p
        where   a.nr_lote_contabil is null
        and     exists (        select  1
                                from    pre_fatur_perda b
                                where   b.dt_referencia         between dt_inicial_w and dt_final_w
                                and     b.nr_sequencia          = a.nr_seq_perda
                                and     b.cd_estabelecimento    = cd_estabelecimento_w
                                and     b.dt_baixa is not null);
end if;

nm_agrupador_w  := nvl(trim(obter_agrupador_contabil(cd_tipo_lote_contabil_w)),'DS_MES_ANO');
ds_mesano_w             :=      to_char(dt_referencia_w,'mmyyyy');
open c01;
loop
fetch c01 into
        nr_seq_perda_w,
        ds_titulo_w,
        cd_convenio_w,
        nr_interno_conta_w,
        qt_dias_conta_w,
        cd_conta_credito_w,
        cd_procedimento_w,
        ie_origem_proced_w,
        cd_material_w,
        cd_centro_custo_debito_w,
        cd_centro_custo_w,
        cd_setor_atendimento_w,
        vl_movimento_w,
        nr_seq_proc_interno_w;
exit when c01%notfound;

        if        (nm_agrupador_w = 'NR_INTERNO_CONTA')then
                   nr_seq_agrupamento_w :=      nr_interno_conta_w;
        elsif (nm_agrupador_w = 'DS_MES_ANO')then
                   nr_seq_agrupamento_w :=      somente_numero(ds_mesano_w);
        end if;

        if (nvl(nr_seq_agrupamento_w,0) = 0)then
                nr_seq_agrupamento_w    :=      ds_mesano_w;
        end if;

        ie_debito_credito_w     := 'D';
        cd_conta_debito_w       := obter_conta_contabil_idade(dt_referencia_w, qt_dias_conta_w);
        cd_historico_w          := obter_hist_idade_conpaci(dt_referencia_w, qt_dias_conta_w);
        nr_sequencia_w          := nr_sequencia_w + 1;
        ds_compl_historico_w    := '';

        ds_convenio_w           := substr(obter_nome_convenio(cd_convenio_w),1,255);
        nm_paciente_w           := substr(obter_paciente_conta(nr_interno_conta_w, 'D'),1,60);

        ds_conteudo_w           := substr(      nr_interno_conta_w      || '#@' ||
                                                nr_seq_perda_w          || '#@' ||
                                                nm_paciente_w           || '#@' ||
                                                ds_convenio_w, 1, 4000);

        select  obter_compl_historico(cd_tipo_lote_contabil_w, cd_historico_w, ds_conteudo_w)
        into    ds_compl_historico_w
        from    dual;

        select  nvl(max(ie_centro_custo),'N')
        into    ie_centro_custo_w
        from    conta_contabil
        where   cd_conta_contabil = cd_conta_debito_w;

        if      (ie_centro_custo_w = 'N') then
                cd_centro_custo_debito_w        := null;
        end if;

        insert into w_movimento_contabil(
                nr_lote_contabil,
                nr_sequencia,
                dt_movimento,
                cd_conta_contabil,
                ie_debito_credito,
                vl_movimento,
                cd_historico,
                cd_centro_custo,
                ds_compl_historico,
                nr_documento,
                nr_seq_agrupamento,
                ie_transitorio,
                nm_tabela,
                nm_atributo,
                nr_seq_tab_orig,
                nr_seq_tab_compl,
                nr_seq_info)
        values( nr_lote_contabil_p,
                nr_sequencia_w,
                dt_referencia_w,
                cd_conta_debito_w,
                ie_debito_credito_w,
                vl_movimento_w,
                cd_historico_w,
                cd_centro_custo_debito_w,
                ds_compl_historico_w,
                null,
                nr_seq_agrupamento_w,
                'N',
                'CONTA_PACIENTE_RESUMO',
                'VL_PROCEDIMENTO+VL_MATERIAL',
                nr_seq_perda_w,
                nr_interno_conta_w,
                nr_seq_info_ctb_w);

        ie_debito_credito_w     := 'C';
        nr_sequencia_w          := nr_sequencia_w + 1;

        select  nvl(max(ie_centro_custo),'N')
        into    ie_centro_custo_w
        from    conta_contabil
        where   cd_conta_contabil = cd_conta_credito_w;

        if      (ie_centro_custo_w = 'N') then
                cd_centro_custo_w       := null;
        end if;

        if      (nvl(cd_conta_credito_w, '0') = '0') then

                if      (nvl(cd_procedimento_w,0) != 0) then
                        define_conta_procedimento(      cd_estabelecimento_w,
                                                        cd_procedimento_w,
                                                        ie_origem_proced_w,
                                                        8,
                                                        null,
                                                        cd_setor_atendimento_w,
                                                        null,
                                                        null,
                                                        null,
                                                        cd_convenio_w,
                                                        null,
                                                        dt_referencia_w,
                                                        cd_conta_credito_w,
                                                        cd_centro_custo_w,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        nr_seq_proc_interno_w);
                elsif   (nvl(cd_material_w,0) != 0) then
                        define_conta_material(  cd_estabelecimento_w,
                                                cd_material_w,
                                                8,
                                                null,
                                                cd_setor_atendimento_w,
                                                '0',
                                                0,
                                                null,
                                                cd_convenio_w,
                                                null,
                                                0,
                                                null,
                                                dt_referencia_w,
                                                cd_conta_credito_w,
                                                cd_centro_custo_w,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);
                end if;
        end if;

        insert into w_movimento_contabil(
                nr_lote_contabil,
                nr_sequencia,
                dt_movimento,
                cd_conta_contabil,
                ie_debito_credito,
                vl_movimento,
                cd_historico,
                cd_centro_custo,
                ds_compl_historico,
                nr_documento,
                nr_seq_agrupamento,
                ie_transitorio,
                nm_tabela,
                nm_atributo,
                nr_seq_tab_orig,
                nr_seq_tab_compl,
                nr_seq_info)
        values( nr_lote_contabil_p,
                nr_sequencia_w,
                dt_referencia_w,
                cd_conta_credito_w,
                ie_debito_credito_w,
                vl_movimento_w,
                cd_historico_w,
                cd_centro_custo_w,
                ds_compl_historico_w,
                null,
                nr_seq_agrupamento_w,
                'N',
                'CONTA_PACIENTE_RESUMO',
                'VL_PROCEDIMENTO+VL_MATERIAL',
                nr_seq_perda_w,
                nr_interno_conta_w,
                nr_seq_info_ctb_w);
end loop;
close c01;

if      (nr_sequencia_w > 0) then
        agrupa_movimento_contabil(nr_lote_contabil_p, nm_usuario_p);
end if;

if   (ds_retorno_p is null) then

        update  lote_contabil
        set     ie_situacao     = 'A',
                dt_geracao_lote = sysdate
        where   nr_lote_contabil        = nr_lote_contabil_p;

        if      (ie_exclusao_p = 'S') then
                ds_retorno_p            := wheb_mensagem_pck.get_texto(298780); -- Exclusao do Lote Ok
                ctb_gravar_log_lote( nr_lote_contabil_p, 2, '', nm_usuario_p);
        else
                ds_retorno_p            := wheb_mensagem_pck.get_texto(298781); -- Geracao do Lote Ok
                ctb_gravar_log_lote( nr_lote_contabil_p, 1, '', nm_usuario_p);
        end if;
        commit;
else
        rollback;
end if;

end contabiliza_perda_pre_fatur;
/
