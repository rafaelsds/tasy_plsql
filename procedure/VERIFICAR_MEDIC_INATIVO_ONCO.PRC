create or replace
procedure verificar_medic_inativo_onco	(	nr_seq_paciente_p	number,
						ds_retorno_p out	varchar2) is

cd_protocolo_out_w	varchar2(2000) := null;

nr_seq_paciente_w	number(10);
cd_protocolo_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
ie_existe_w		char(1);
ds_protocolos_w		varchar2(2000) := ',';


cursor c01 is
	select	nr_seq_paciente,
				cd_protocolo
	from	paciente_setor
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_seq_paciente		<> nr_seq_paciente_p
	and	((ie_status		= 'I') or (ie_status		= 'F'))
	order by
		dt_protocolo;


begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	paciente_setor
where	nr_seq_paciente	= nr_seq_paciente_p;


open c01;
loop
fetch c01 into
	nr_seq_paciente_W,
	cd_protocolo_w;
exit when c01%notfound;
	begin
	
		select	nvl(max('S'),'N')
		into	ie_existe_w
		from	paciente_atendimento a,
			paciente_atend_medic b
		where	a.nr_seq_atendimento	= b.nr_seq_atendimento
		and	trunc(a.dt_prevista)	>= trunc(sysdate)
		and	a.nr_seq_paciente	= nr_seq_paciente_w
		and	b.ie_cancelada		= 'N'
		and	b.nr_seq_diluicao 	is null;

						
		if	(ie_existe_w = 'S') and
			(instr(ds_protocolos_w, ','||cd_protocolo_w||',') = 0 )then
			cd_protocolo_out_w :=  '- ' || substr(obter_desc_protocolo(cd_protocolo_w),1,80)  || chr(13) || chr(10) || cd_protocolo_out_w;
			ds_protocolos_w := ds_protocolos_w||','||cd_protocolo_w||',';
			
		end if;

		end;
end loop;
close C01;


if	(cd_protocolo_out_w is not null) then
	cd_protocolo_out_w := 	/*'Os protocolos abaixo est�o inativos e possuem ciclos gerados:'*/obter_desc_expressao(782122) || chr(13) || chr(10) || cd_protocolo_out_w;
end if;


ds_retorno_p := cd_protocolo_out_w;


end	verificar_medic_inativo_onco;
/