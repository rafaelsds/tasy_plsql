CREATE OR REPLACE PROCEDURE INTEGRAR_SINAIS_VITAIS_BB   
    (nr_sequencia_p	      atendimento_sinal_vital.nr_sequencia%type default null,
    nr_atendimento_p      atendimento_sinal_vital.nr_atendimento%type default null,
    dt_sinal_vital_p      atendimento_sinal_vital.dt_sinal_vital%type default null,
    qt_freq_cardiaca_p    atendimento_sinal_vital.qt_freq_cardiaca%type default null,
    qt_freq_resp_p        atendimento_sinal_vital.qt_freq_resp%type default null,
    qt_saturacao_o2_p     atendimento_sinal_vital.qt_saturacao_o2%type default null,
    qt_temp_p             atendimento_sinal_vital.qt_temp%type default null,
    qt_pam_p              atendimento_sinal_vital.qt_pam%type default null,
    qt_pa_sistolica_p     atendimento_sinal_vital.qt_pa_sistolica%type default null,
    qt_pa_diastolica_p    atendimento_sinal_vital.qt_pa_diastolica%type default null,
    ie_aparelho_pa_p      atendimento_sinal_vital.ie_aparelho_pa%type default null,
    qt_glasgow_p          atend_escala_indice.qt_glasgow%type default null,
    qt_ramsay_p           escala_ramsay.ie_ramsay%type default null,
    ie_rass_p             escala_richmond.ie_rass%type default null) IS

json_smart_alert      philips_json;
json_cells            philips_json_list;
json_cell             philips_json;
json_cell_attributes  philips_json_list;
json_cell_attribute   philips_json;
envio_integracao_bb   clob;
retorno_integracao_bb clob;

flowsheet_column_id_v VARCHAR2(32);

BEGIN

    json_smart_alert := philips_json();
    json_cells := philips_json_list();
    json_cell := philips_json();
    json_cell_attribute := philips_json();
    
    json_smart_alert.put('typeID', 'VSIFS');
    json_smart_alert.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'MM/DD/YYYY HH24:MI:SS.SSSSS'));
    
    IF (qt_glasgow_p IS NOT NULL) THEN
      flowsheet_column_id_v := 'B' || nr_sequencia_p;
    ELSIF (qt_ramsay_p IS NOT NULL) THEN
      flowsheet_column_id_v := 'C' || nr_sequencia_p;
    ELSIF (ie_rass_p IS NOT NULL) THEN
      flowsheet_column_id_v := 'F' || nr_sequencia_p;
    ELSE
      flowsheet_column_id_v := 'A' || nr_sequencia_p;
    END IF;

    json_smart_alert.put('objectID', 'FC' || LPAD(flowsheet_column_id_v, 30, 0));
    json_smart_alert.put('patientHealthSystemStayID', LPAD(nr_atendimento_p, 32, 0));
    json_smart_alert.put('columnDate', TO_CHAR(f_extract_utc_bb(dt_sinal_vital_p), 'YYYY-MM-DD"T"HH24:MI'));
    json_smart_alert.put('columnDateGMTOffset', 0);
    
    json_cell.put('typeCatID', '09e33845beb0f595f4b624fcf4666cef');
    json_cell.put('resultStatusID', '39d8f36dc43045c8b04a0a06f20b6351');
    
    json_cell_attribute.put('resultStatusID', '39d8f36dc43045c8b04a0a06f20b6351');

    IF (qt_freq_cardiaca_p is not null) THEN
        json_cell.put('labelID', '01409271445d415b867dded0fbcf2459');
        json_cell.put('objectID', 'C1' || LPAD(flowsheet_column_id_v, 30, 0));
        
        json_cell_attribute.put('typeValueID', 'c51b1ead9ff94248acdc9986d9858f0b');
        json_cell_attribute.put('value', qt_freq_cardiaca_p);
        
        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;

    IF (qt_freq_resp_p is not null) THEN
        json_cell.put('labelID', 'a9b6b51c9eb248728f4112ddd4cc762c');
        json_cell.put('objectID', 'C2' || LPAD(flowsheet_column_id_v, 30, 0));

        json_cell_attribute.put('typeValueID', 'dc100b48923c4555869cea761a3f58e7');
        json_cell_attribute.put('value', qt_freq_resp_p);

        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;
    
    IF (qt_saturacao_o2_p is not null) THEN
        json_cell.put('labelID', 'db69f4fbcda3429e9a4a47e16c5ab013');
        json_cell.put('objectID', 'C3' || LPAD(flowsheet_column_id_v, 30, 0));
        
        json_cell_attribute.put('typeValueID', '3c64a7630640464a8f9da89f5213c8b4');
        json_cell_attribute.put('value', qt_saturacao_o2_p);
        
        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;
    
    IF (qt_temp_p is not null) THEN
        json_cell.put('labelID', '27f0b49471144b658212d3b56673943a');
        json_cell.put('objectID', 'C4' || LPAD(flowsheet_column_id_v, 30, 0));
        
        json_cell_attribute.put('typeValueID', '5ed47254daad4b74b70e1637a142a451');
        json_cell_attribute.put('value', qt_temp_p);

        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;
    
    IF (qt_pam_p is not null OR qt_pa_sistolica_p is not null OR qt_pa_diastolica_p is not null) THEN 
      IF (ie_aparelho_pa_p is not null AND ie_aparelho_pa_p = 'I') THEN
          json_cell.put('labelID', '8126628c8d8f402188e5ff7c5ae1e6ce');
      ELSE
        json_cell.put('labelID', 'f7687733f870403aa11c44ca735c93da');
      END IF;
      
      json_cell.put('objectID', 'C5' || LPAD(flowsheet_column_id_v, 30, 0));
      json_cell_attributes := philips_json_list();
      
      IF (qt_pam_p is not null) THEN
        IF (ie_aparelho_pa_p is not null AND ie_aparelho_pa_p = 'I') THEN
          json_cell_attribute.put('typeValueID', '29b35d7461a9494694d4119d89736081');
        ELSE
          json_cell_attribute.put('typeValueID', '1d17edd4cb194c26bdd3826909f96cce');
        END IF;
  
        json_cell_attribute.put('value', qt_pam_p);
        json_cell_attributes.append(json_cell_attribute.to_json_value());
      END IF;
      
      IF (qt_pa_sistolica_p is not null) THEN
        IF (ie_aparelho_pa_p is not null AND ie_aparelho_pa_p = 'I') THEN
          json_cell_attribute.put('typeValueID', '988aac12a502431cb500fde5b34a5a5a');
        ELSE
          json_cell_attribute.put('typeValueID', '0cd403629bcf4cbaabfb8d95da5c2ce1');
        END IF;
  
        json_cell_attribute.put('value', qt_pa_sistolica_p);
        json_cell_attributes.append(json_cell_attribute.to_json_value());
      END IF;
      
      IF (qt_pa_diastolica_p is not null) THEN
        IF (ie_aparelho_pa_p is not null AND ie_aparelho_pa_p = 'I') THEN
          json_cell_attribute.put('typeValueID', 'a115738cbcf5410dbad4bec040d7853f');
        ELSE
          json_cell_attribute.put('typeValueID', 'dcd6536206d5472d9f7816c03c24d165');
        END IF;
  
        json_cell_attribute.put('value', qt_pa_diastolica_p);
        json_cell_attributes.append(json_cell_attribute.to_json_value());
      END IF;
      
      json_cell.put('attributes', json_cell_attributes);
      json_cells.append(json_cell.to_json_value());
    
     END IF;
     
    IF (qt_glasgow_p is not null) THEN
        json_cell.put('labelID', 'a83599934b6e47bbb1f0159f06015e81');
        json_cell.put('objectID', 'C1' || LPAD(flowsheet_column_id_v, 30, 0));
        json_cell.put('typeCatID', 'a2eb36f5217d611cbbd4c180d720bb39');
        
        json_cell_attribute.put('typeValueID', '492f2179d6f04e938859eb0e0d2db3b0');
        json_cell_attribute.put('gcsTotalPresent', '1');
        
        CASE 
            WHEN qt_glasgow_p = 3  THEN
                json_cell_attribute.put('value', '6f622fef3c764876adb8247f828d30b9'); 
            WHEN qt_glasgow_p = 4  THEN
                json_cell_attribute.put('value', 'a51f07408320458987e69719a59dbf35'); 
            WHEN qt_glasgow_p = 5  THEN
                json_cell_attribute.put('value', 'b39aefb50f7e4deba7fa28896a9dda4d'); 
            WHEN qt_glasgow_p = 6  THEN
                json_cell_attribute.put('value', '4eeea401d002499aaf115ca7edbdbb2f'); 
            WHEN qt_glasgow_p = 7  THEN
                json_cell_attribute.put('value', 'd9a14200e474426f862da12bef2686e9'); 
            WHEN qt_glasgow_p = 8  THEN
                json_cell_attribute.put('value', '63122c185bde42ef87f95688a003fda4'); 
            WHEN qt_glasgow_p = 9  THEN
                json_cell_attribute.put('value', '1a13503896cd4e28b09f0acd88447461'); 
            WHEN qt_glasgow_p = 10  THEN
                json_cell_attribute.put('value', '0c2a3113fc1848efae43aebd5110d562'); 
            WHEN qt_glasgow_p = 11  THEN
                json_cell_attribute.put('value', '359975c6059c42eda3c6562af776ebde'); 
            WHEN qt_glasgow_p = 12  THEN
                json_cell_attribute.put('value', 'f25adee99b554638bcce87dad690f5b2'); 
            WHEN qt_glasgow_p = 13  THEN
                json_cell_attribute.put('value', 'c73d60a66ae8434c96826fb00bbfa82b'); 
            WHEN qt_glasgow_p = 14  THEN
                json_cell_attribute.put('value', 'c73d5d43408b4ada949006edfef7b255'); 
            WHEN qt_glasgow_p = 15  THEN
                json_cell_attribute.put('value', 'ab02ee26fca549d194d83426a1c4c06b'); 
            ELSE
                json_cell_attribute.put('value', '878b1e084b62463a9dd01467c1cf6353'); 
        END CASE;
        
        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;
    
    IF (qt_ramsay_p is not null) THEN
        json_cell.put('labelID', '4cc58794678b4a5e9cd9de50d55dc21e');
        json_cell.put('objectID', 'C1' || LPAD(flowsheet_column_id_v, 30, 0));
        json_cell.put('typeCatID', 'a2eb36f5217d611cbbd4c180d720bb39');

        json_cell_attribute.put('typeValueID', '9012e760fb3241869e5eeceb1138c612');
        json_cell_attribute.put('value', 'Ramsay');
        
        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        
        json_cell_attribute := philips_json();
        
        json_cell_attribute.put('typeValueID', 'a8e88afe18b6471ca1fd24ec5930324e');
        json_cell_attribute.put('value', qt_ramsay_p);
        
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;

    IF (ie_rass_p is not null) THEN
        json_cell.put('labelID', '4cc58794678b4a5e9cd9de50d55dc21e');
        json_cell.put('objectID', 'C1' || LPAD(flowsheet_column_id_v, 30, 0));
        json_cell.put('typeCatID', 'a2eb36f5217d611cbbd4c180d720bb39');

        json_cell_attribute.put('typeValueID', '9012e760fb3241869e5eeceb1138c612');
        json_cell_attribute.put('value', 'RASS');
        
        json_cell_attributes := philips_json_list();
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        
        json_cell_attribute := philips_json();
        
        json_cell_attribute.put('typeValueID', 'a8e88afe18b6471ca1fd24ec5930324e');
        json_cell_attribute.put('value', ie_rass_p);
        
        json_cell_attributes.append(json_cell_attribute.to_json_value());
        
        json_cell.put('attributes', json_cell_attributes);
        json_cells.append(json_cell.to_json_value());
    END IF;
    
    json_smart_alert.put('cells', json_cells);
    
    dbms_lob.createtemporary(envio_integracao_bb, TRUE);
    json_smart_alert.to_clob(envio_integracao_bb);
    
    SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Vitals_Aperiodic',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;

END;
/