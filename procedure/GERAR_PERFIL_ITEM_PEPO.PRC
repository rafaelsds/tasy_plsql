CREATE OR REPLACE
PROCEDURE Gerar_Perfil_item_pepo(	cd_perfil_p		Number,
					nr_seq_item_pepo_p	Number,
					nm_usuario_p		Varchar2) IS 

nr_sequencia_w		Number(10);

BEGIN

select	perfil_item_pepo_seq.nextval
into	nr_sequencia_w
from	dual;

insert into perfil_item_pepo(
	nr_sequencia,           
	dt_atualizacao,
	nm_usuario,
	cd_perfil,
	nr_seq_item_pepo,
	ie_atualizar,
	dt_atualizacao_nrec,
	nm_usuario_nrec)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	cd_perfil_p,
	nr_seq_item_pepo_p,
	'S',
	sysdate,
	nm_usuario_p);

commit;

END Gerar_Perfil_item_pepo;
/