create or replace
procedure TISS_OBTER_SE_ENVIA_PARTIC (	cd_convenio_p		number,
					cd_estabelecimento_p	number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					ie_tiss_tipo_guia_p	varchar2,
					ie_envio_p	OUT	varchar2,
					ie_responsavel_credito_p	varchar2,
					ie_proc_pacote_p	varchar2,
					cd_funcao_medico_p	number,
					cd_medico_p		varchar2,
					cd_setor_atendimento_p	number,
					nr_seq_proc_interno_p	number,
					dt_referencia_p		date) is

ie_enviar_w		varchar2(5);
cd_grupo_proc_w		number(15,0);
cd_especialidade_w	number(15,0);
cd_area_procedimento_w	number(15,0);

cursor c01 is
select	ie_enviar
from	tiss_regra_partic
where	cd_convenio							= cd_convenio_p
and	cd_estabelecimento						= cd_estabelecimento_p
and	nvl(ie_tiss_tipo_guia, nvl(ie_tiss_tipo_guia_p,'X'))		= nvl(ie_tiss_tipo_guia_p,'X')
and	nvl(cd_procedimento, nvl(cd_procedimento_p,0))			= nvl(cd_procedimento_p,0)
and	nvl(cd_area_procedimento, nvl(cd_area_procedimento_w,0))	= nvl(cd_area_procedimento_w,0)
and	nvl(cd_especialidade, nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
and	nvl(cd_grupo_proc, nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
and	nvl(ie_responsavel_credito, nvl(ie_responsavel_credito_p, 'X'))	= nvl(ie_responsavel_credito_p, 'X')
and	nvl(cd_funcao_medico, nvl(cd_funcao_medico_p,0))		= nvl(cd_funcao_medico_p,0)
and	((nvl(ie_pacote, 'T') = 'T') or (nvl(ie_pacote, 'T') 		= ie_proc_pacote_p))
and	nvl(cd_medico,nvl(cd_medico_p,'0'))				= nvl(cd_medico_p,'0')
and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_p,0))	= nvl(cd_setor_atendimento_p,0) 
and	nvl(nr_seq_proc_interno, nvl(nr_seq_proc_interno_p,0))		= nvl(nr_seq_proc_interno_p, 0)
and	nvl(dt_referencia_p,sysdate) between nvl(dt_inicio_vigencia,sysdate - 99999) and nvl(dt_fim_vigencia,sysdate + 99999)
order by nvl(cd_procedimento, 0),
	nvl(cd_grupo_proc, 0),
	nvl(cd_especialidade,0),
	nvl(cd_area_procedimento, 0),
	nvl(ie_responsavel_credito,0),
	nvl(cd_funcao_medico,0),
	nvl(cd_medico,'0'),
	nvl(cd_setor_atendimento,0),
	nvl(nr_seq_proc_interno,0),
	dt_inicio_vigencia;

begin

select	max(cd_grupo_proc),
	max(cd_especialidade),
	max(cd_area_procedimento)
into	cd_grupo_proc_w,
	cd_especialidade_w,
	cd_area_procedimento_w
from	estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

open c01;
loop
fetch c01 into
	ie_enviar_w;
exit when c01%notfound;
	ie_enviar_w	:= ie_enviar_w;
end loop;
close c01;

ie_envio_p	:= nvl(ie_enviar_w,'S');

end TISS_OBTER_SE_ENVIA_PARTIC;
/