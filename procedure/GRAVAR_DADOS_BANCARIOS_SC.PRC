create or replace
procedure gravar_dados_bancarios_sc(		nr_solic_compra_p		number,
					nm_usuario_p		Varchar2) is 

cd_banco_w		pessoa_juridica_conta.cd_banco%type;
cd_agencia_bancaria_w	pessoa_juridica_conta.cd_agencia_bancaria%type;
nr_conta_w		pessoa_juridica_conta.nr_conta%type;
nr_digito_conta_w	pessoa_juridica_conta.nr_digito_conta%type;
ie_tipo_servico_w	solic_compra.ie_tipo_servico%type;
cd_pessoa_fisica_w	solic_compra.cd_pessoa_fisica%type;
cd_fornec_sugerido_w	solic_compra.cd_fornec_sugerido%type;

cursor c01 is
select	cd_banco,
	cd_agencia_bancaria,
	nr_conta,
	nr_digito_conta
from	pessoa_fisica_conta
where	cd_pessoa_fisica = cd_pessoa_fisica_w;
					

cursor c02 is
select	cd_banco,
	cd_agencia_bancaria,
	nr_conta,
	nr_digito_conta
from	pessoa_juridica_conta
where	cd_cgc = cd_fornec_sugerido_w;
					
begin

cd_banco_w			:= null;
cd_agencia_bancaria_w		:= '';
nr_conta_w			:= '';
nr_digito_conta_w			:= '';

select	ie_tipo_servico,
	cd_pessoa_fisica,
	cd_fornec_sugerido
into	ie_tipo_servico_w,
	cd_pessoa_fisica_w,
	cd_fornec_sugerido_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

if	(ie_tipo_servico_w in ('SP','SR','CD')) then
	
	if	(cd_pessoa_fisica_w is not null) then
	
		open C01;
		loop
		fetch C01 into	
			cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			nr_digito_conta_w;
		exit when C01%notfound;
			begin
			cd_banco_w			:= cd_banco_w;
			cd_agencia_bancaria_w		:= cd_agencia_bancaria_w;
			nr_conta_w			:= nr_conta_w;
			nr_digito_conta_w			:= nr_digito_conta_w;
			end;
		end loop;
		close C01;
		
		update	solic_compra
		set	cd_banco		= cd_banco_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	cd_banco is null;
		
		update	solic_compra
		set	cd_agencia_bancaria	= cd_agencia_bancaria_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	cd_agencia_bancaria is null;
		
		
		update	solic_compra
		set	nr_conta		= nr_conta_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	nr_conta is null;
		
		update	solic_compra
		set	nr_digito_conta		= nr_digito_conta_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	nr_digito_conta is null;
		
	
	elsif	(cd_fornec_sugerido_w is not null) then
		open C02;
		loop
		fetch C02 into	
			cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			nr_digito_conta_w;
		exit when C02%notfound;
			begin
			cd_banco_w			:= cd_banco_w;
			cd_agencia_bancaria_w		:= cd_agencia_bancaria_w;
			nr_conta_w			:= nr_conta_w;
			nr_digito_conta_w			:= nr_digito_conta_w;
			end;
		end loop;
		close C02;
		
		update	solic_compra
		set	cd_banco		= cd_banco_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	cd_banco is null;
		
		update	solic_compra
		set	cd_agencia_bancaria	= cd_agencia_bancaria_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	cd_agencia_bancaria is null;
		
		
		update	solic_compra
		set	nr_conta		= nr_conta_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	nr_conta is null;
		
		update	solic_compra
		set	nr_digito_conta		= nr_digito_conta_w
		where	nr_solic_compra		= nr_solic_compra_p
		and	nr_digito_conta is null;
	end if;
	
end if;
	
commit;

end gravar_dados_bancarios_SC;
/