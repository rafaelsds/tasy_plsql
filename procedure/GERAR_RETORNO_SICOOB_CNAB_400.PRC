create or replace
procedure gerar_retorno_sicoob_cnab_400(nr_seq_cobr_escrit_p	number,
					nm_usuario_p		varchar2) is 

nr_titulo_w			number(10);
vl_juros_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
dt_liquidacao_w			varchar2(6);
ie_tipo_carteira_w		varchar2(255);
ds_titulo_w			varchar2(255);
vl_cobranca_w			number(15,2);
cd_ocorrencia_w			number(10);
cd_motivo_w			varchar2(10);
nr_seq_ocorrencia_ret_w		number(10);
vl_tarifa_cobranca_w		number(15,2);
nr_seq_ocorr_motivo_w		number(10);
vl_saldo_titulo_w		number(15,2);
cd_banco_w			number(3);
cd_banco_cobr_w			number(5);
cd_centro_custo_desc_w		number(8);
nr_seq_motivo_desc_w		number(10);
nr_seq_tit_rec_cobr_w		number(10);
cd_pessoa_fisica_desc_w		varchar2(10);
cd_cgc_desc_w			varchar2(14);
ds_nosso_numero_w		varchar2(255);
ds_nosso_numero_sep_w		varchar2(20);
cd_movimento_w 			varchar2(02);

cursor c01 is
	select	trim(substr(ds_string,109,2)),
		trim(substr(ds_string,117,10)),
		to_number(substr(ds_string,153,13))/100,
		to_number(substr(ds_string,267,13))/100,
		to_number(substr(ds_string,241,13))/100,
		to_number(substr(ds_string,228,13))/100,
		to_number(substr(ds_string,254,13))/100,
		to_number(substr(ds_string,189,13))/100,
		substr(ds_string,111,6)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= '1';
begin

select	to_number(substr(ds_string,77,3))
into	cd_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,1,1)	= '0';

select	max(a.cd_banco)
into	cd_banco_cobr_w
from	cobranca_escritural a
where	a.nr_sequencia	= nr_seq_cobr_escrit_p;

if	(cd_banco_w <> cd_banco_cobr_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(184490);
end if;

open C01;
loop
fetch C01 into	
	cd_movimento_w,
	ds_titulo_w,
	vl_cobranca_w,
	vl_juros_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_tarifa_cobranca_w,
	dt_liquidacao_w;
exit when C01%notfound;
	begin			
	select	max(nr_titulo),
		nvl(max(vl_saldo_titulo),0)
	into	nr_titulo_w,
		vl_saldo_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	/* Ou importa o t�tulo, ou grava no log */
	if	(nr_titulo_w is not null) then

		ie_tipo_carteira_w	:= obter_tipo_carteira_regra(nr_titulo_w);
		
		if cd_movimento_w = '02' then
			update	titulo_receber
			set	ie_tipo_carteira	= ie_tipo_carteira_w,
			        ie_entrada_confirmada	= 'C'
			where	nr_titulo		= nr_titulo_w
			and	ie_tipo_carteira	is null;
		else
			update	titulo_receber
			set	ie_tipo_carteira	= ie_tipo_carteira_w
			where	nr_titulo		= nr_titulo_w
			and	ie_tipo_carteira	is null;
		end if;

		select	max(a.cd_centro_custo),
			max(a.nr_seq_motivo_desc),
			max(a.cd_pessoa_fisica),
			max(a.cd_cgc)
		into	cd_centro_custo_desc_w,
			nr_seq_motivo_desc_w,
			cd_pessoa_fisica_desc_w,
			cd_cgc_desc_w
		from	titulo_receber_liq_desc a
		where	a.nr_bordero	is null
		and	a.nr_seq_liq	is null
		and	a.nr_titulo	= nr_titulo_w;

		select	titulo_receber_cobr_seq.nextval
		into	nr_seq_tit_rec_cobr_w
		from	dual;

		insert	into titulo_receber_cobr 
			(nr_sequencia,
			nr_titulo,
			cd_banco,
			vl_cobranca,
			vl_desconto,
			vl_acrescimo,
			vl_despesa_bancaria,
			vl_liquidacao,
			vl_juros,
			dt_liquidacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_cobranca,
			vl_saldo_inclusao,
			vl_multa,
			nr_seq_motivo_desc,
			cd_centro_custo_desc)
		values	(nr_seq_tit_rec_cobr_w,
			nr_titulo_w,
			cd_banco_cobr_w,
			nvl(vl_cobranca_w,0),
			nvl(vl_desconto_w,0) + nvl(vl_abatimento_w,0),
			0,
			nvl(vl_tarifa_cobranca_w,0),
			nvl(vl_liquido_w,0),
			nvl(vl_juros_w,0),
			nvl(trim(dt_liquidacao_w),sysdate),
			sysdate,
			nm_usuario_p,
			nr_seq_cobr_escrit_p,
			nvl(vl_saldo_titulo_w,0),
			0,
			nr_seq_motivo_desc_w,
			cd_centro_custo_desc_w);
			
		insert	into titulo_rec_cobr_desc
			(cd_cgc,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			nr_seq_tit_rec_cobr,
			nr_sequencia)
		values	(cd_cgc_desc_w,
			cd_pessoa_fisica_desc_w,
			sysdate,
			nm_usuario_p,
			nr_seq_tit_rec_cobr_w,
			titulo_rec_cobr_desc_seq.nextval);

	else
		fin_gerar_log_controle_banco(3,substr('N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy',1,4000),nm_usuario_p,'N');
	end if;
	
	end;
end loop;
close C01;

commit;

end gerar_retorno_sicoob_cnab_400;
/
