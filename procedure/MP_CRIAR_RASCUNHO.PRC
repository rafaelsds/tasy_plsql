create or replace
procedure mp_criar_rascunho(nr_seq_processo_p	in	number,
			nm_usuario_p		in	varchar2,
			nr_seq_novo_processo_p	out	number) is

nr_sequencia_w			number(10);
qt_registro_w			number(10);
ie_status_versao_w		varchar2(2);
ds_processo_w			long;
ds_objeto_w			long;
nr_seq_proc_objeto_w		number(10);
nr_seq_proc_objeto_novo_w	number(10);
nr_seq_proc_raia_w		number(10);
nr_seq_processo_doc_w		number(10);
nr_seq_proc_unid_org_w		number(10);
nr_seq_proc_regra_equip_w	number(10);
nr_seq_recurso_w		number(10);
nr_seq_obj_ref_w		number(10);
ie_tipo_ref_w			varchar2(1);
nr_seq_proc_obj_unid_org_w	number(10);
nr_seq_tarefa_w			number(10);
nr_seq_proc_objeto_pai_w	number(10);
nr_seq_processo_ref_w		number(10);

/* Objetos em tela */

Cursor c01 is
select	a.nr_sequencia
from	mp_processo_objeto a
where	nr_seq_processo	= nr_seq_processo_p
order by
	decode(nr_seq_obj_origem,null,2,1);

Cursor c02 is
select	a.nr_sequencia
from	mp_processo_raia a
where	nr_seq_processo	= nr_seq_processo_p;

/* Tabelas filhas do processo */

Cursor c03 is
select	a.nr_sequencia
from	mp_processo_doc a
where	a.nr_seq_processo	= nr_seq_processo_p;

Cursor c04 is
select	a.nr_sequencia
from	mp_processo_unid_org a
where	a.nr_seq_processo	= nr_seq_processo_p;

Cursor c05 is
select	a.nr_sequencia
from	mp_proc_regra_equip a
where	a.nr_seq_proc		= nr_seq_processo_p;

/* Tabelas filhas da atividade */

Cursor c06 is
select	a.nr_sequencia
from	mp_processo_doc a
where	a.nr_seq_proc_obj	= nr_seq_proc_objeto_w;

Cursor c07 is
select	a.nr_sequencia
from	mp_processo_obj_rec a
where	a.nr_seq_obj_proc	= nr_seq_proc_objeto_w;

Cursor c08 is
select	a.nr_sequencia,
	'O'
from	mp_proc_obj_ref a
where	a.nr_seq_proc_obj_origem	= nr_seq_proc_objeto_w
union all
select	a.nr_sequencia,
	'D'
from	mp_proc_obj_ref a
where	a.nr_seq_proc_obj_destino	= nr_seq_proc_objeto_w;	

Cursor c09 is
select	a.nr_sequencia
from	mp_proc_obj_unid_org a
where	a.nr_seq_proc_obj		= nr_seq_proc_objeto_w;

Cursor c10 is
select	a.nr_sequencia
from	mp_tarefa a
where	a.nr_seq_proc_objeto		= nr_seq_proc_objeto_w;

begin

if	(nr_seq_processo_p is not null) then

	select	count(*)
	into	qt_registro_w
	from	mp_processo
	where	ie_status_versao	in ('R','E')
	and	nr_seq_origem		= nr_seq_processo_p;

	if	(qt_registro_w > 0) then
		-- J� existe uma revis�o para esse processo!
		wheb_mensagem_pck.exibir_mensagem_abort(267095);
	end if;

	select	ie_status_versao,
		ds_processo
	into	ie_status_versao_w,
		ds_processo_w
	from	mp_processo
	where	nr_sequencia	= nr_seq_processo_p;
		
	if	(ie_status_versao_w in ('R','E')) then
		-- O processo atual j� � uma revis�o, n�o � permitido criar outra revis�o a partir dele.
		wheb_mensagem_pck.exibir_mensagem_abort(267096);
	end if;

	select	mp_processo_seq.nextval
	into	nr_sequencia_w
	from	dual;


	insert	into	mp_processo
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_empresa,
		cd_estabelecimento,
		nm_processo,
		ie_status_versao,
		ie_tipo_mapeamento,
		nr_seq_apres,
		nr_seq_objeto,
		nr_seq_origem,
		nr_seq_superior)
	select	nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_empresa,
		cd_estabelecimento,
		nm_processo,
		'E',
		ie_tipo_mapeamento,
		nr_seq_apres,
		nr_seq_objeto,
		nr_seq_processo_p,
		nr_seq_superior
	from	mp_processo
	where	nr_sequencia	= nr_seq_processo_p;
		
	update	mp_processo
	set	ds_processo	= ds_processo_w
	where	nr_sequencia	= nr_sequencia_w;

	/* Atualizar referencias superiores */
	
	insert	into	mp_processo_objeto
		(ds_cor,
		ds_lista_pontos,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_objeto,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_apres,
		nr_seq_objeto,
		nr_seq_processo,
		nr_sequencia,
		qt_altura,
		qt_esquerda,
		qt_largura,
		qt_topo,
		nr_seq_obj_origem,
		nr_seq_obj_destino,
		nr_seq_processo_ref)
	select	ds_cor,
		ds_lista_pontos,
		sysdate,
		sysdate,
		nm_objeto,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_apres,
		nr_seq_objeto,
		nr_seq_processo,
		mp_processo_objeto_seq.nextval,
		qt_altura,
		qt_esquerda,
		qt_largura,
		qt_topo,
		nr_seq_obj_origem,
		nr_seq_obj_destino,
		nr_sequencia_w
	from	mp_processo_objeto
	where	nr_seq_processo_ref	= nr_seq_processo_p;


	/*Copiar tabelas filhas do processo */
	open c03;
	loop
	fetch c03 into
		nr_seq_processo_doc_w;
	exit when c03%notfound;
		insert	into	mp_processo_doc
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_processo,
			nr_seq_documento)
		select	mp_processo_doc_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_sequencia_w,
			nr_seq_documento
		from	mp_processo_doc
		where	nr_sequencia	= nr_seq_processo_doc_w;	
	end loop;
	close c03;

	open c04;
	loop
	fetch c04 into
		nr_seq_proc_unid_org_w;
	exit when c04%notfound;
		insert	into	mp_processo_unid_org
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_processo,
			nr_seq_unid_org)
		select	mp_processo_unid_org_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_sequencia_w,
			nr_seq_unid_org
		from	mp_processo_unid_org
		where	nr_sequencia	= nr_seq_proc_unid_org_w;	
	end loop;
	close c04;

	open c05;
	loop
	fetch c05 into
		nr_seq_proc_regra_equip_w;
	exit when c05%notfound;
		insert	into	mp_proc_regra_equip
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_proc,
			cd_setor_atendimento,
			cd_cargo,
			nr_seq_equipe,
			cd_pessoa_fisica)
		select	mp_proc_regra_equip_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_sequencia_w,
			cd_setor_atendimento,
			cd_cargo,
			nr_seq_equipe,
			cd_pessoa_fisica
		from	mp_proc_regra_equip
		where	nr_sequencia	= nr_seq_proc_regra_equip_w;	
	end loop;
	close c05;


	/* Copiar objetos */
	open c01;
	loop
	fetch c01 into
		nr_seq_proc_objeto_w;
	exit when c01%notfound;

		select	ds_objeto
		into	ds_objeto_w
		from	mp_processo_objeto
		where	nr_sequencia	= nr_seq_proc_objeto_w;

		select	mp_processo_objeto_seq.nextval
		into	nr_seq_proc_objeto_novo_w
		from	dual;	

		insert	into	mp_processo_objeto
			(ds_cor,
			ds_lista_pontos,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_objeto,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_apres,
			nr_seq_objeto,
			nr_seq_processo,
			nr_sequencia,
			qt_altura,
			qt_esquerda,
			qt_largura,
			qt_topo,
			nr_seq_obj_origem,
			nr_seq_obj_destino,
			nr_seq_processo_ref)
		select	ds_cor,
			ds_lista_pontos,
			sysdate,
			sysdate,
			nm_objeto,
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_apres,
			nr_seq_objeto,
			nr_sequencia_w,
			nr_seq_proc_objeto_novo_w,
			qt_altura,
			qt_esquerda,
			qt_largura,
			qt_topo,
			nr_seq_obj_origem,
			nr_seq_obj_destino,
			nr_seq_processo_ref
		from	mp_processo_objeto
		where	nr_sequencia	= nr_seq_proc_objeto_w;

		update	mp_processo_objeto
		set	ds_objeto	= ds_objeto_w
		where	nr_sequencia	= nr_seq_proc_objeto_novo_w;

		update	mp_processo_objeto a
		set	nr_seq_obj_origem	= nr_seq_proc_objeto_novo_w
		where	a.nr_seq_obj_origem 	= nr_seq_proc_objeto_w
		and	a.nr_seq_processo	= nr_sequencia_w;

		update	mp_processo_objeto a
		set	nr_seq_obj_destino	= nr_seq_proc_objeto_novo_w
		where	a.nr_seq_obj_destino 	= nr_seq_proc_objeto_w
		and	a.nr_seq_processo	= nr_sequencia_w;

		/* Copiar filhas da atividade */

		open c06;
		loop
		fetch c06 into
			nr_seq_processo_doc_w;
		exit when c06%notfound;
			insert	into	mp_processo_doc
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_proc_obj,
				nr_seq_documento)
			select	mp_processo_doc_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_proc_objeto_novo_w,
				nr_seq_documento
			from	mp_processo_doc
			where	nr_sequencia	= nr_seq_processo_doc_w;	
		end loop;
		close c06;

		open c07;
		loop
		fetch c07 into
			nr_seq_recurso_w;
		exit when c07%notfound;
			insert	into	mp_processo_obj_rec
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_obj_proc,
				nr_seq_recurso,
				cd_cargo,
				nr_seq_tipo_equip)
			select	mp_processo_obj_rec_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_proc_objeto_novo_w,
				nr_seq_recurso,
				cd_cargo,
				nr_seq_tipo_equip
			from	mp_processo_obj_rec
			where	nr_sequencia	= nr_seq_recurso_w;	
		end loop;
		close c07;

		open c08;
		loop
		fetch c08 into
			nr_seq_obj_ref_w,
			ie_tipo_ref_w;
		exit when c08%notfound;
			insert	into	mp_proc_obj_ref
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_proc_obj_origem,
				nr_seq_proc_obj_destino)
			select	mp_proc_obj_ref_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				decode(ie_tipo_ref_w,'O',nr_seq_proc_objeto_novo_w,nr_seq_proc_obj_origem),
				decode(ie_tipo_ref_w,'D',nr_seq_proc_objeto_novo_w,nr_seq_proc_obj_destino)
			from	mp_proc_obj_ref
			where	nr_sequencia	= nr_seq_obj_ref_w;	
		end loop;
		close c08;

		open c09;
		loop
		fetch c09 into
			nr_seq_proc_obj_unid_org_w;
		exit when c09%notfound;
			insert	into	mp_proc_obj_unid_org
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_proc_obj,
				nr_seq_unid_org)
			select	mp_proc_obj_unid_org_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_proc_objeto_novo_w,
				nr_seq_unid_org
			from	mp_proc_obj_unid_org
			where	nr_sequencia	= nr_seq_proc_obj_unid_org_w;	
		end loop;
		close c09;

		open c10;
		loop
		fetch c10 into
			nr_seq_tarefa_w;
		exit when c10%notfound;
			insert	into	mp_tarefa
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_proc_objeto,
				nr_seq_apres,
				ds_tarefa)
			select	mp_tarefa_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_proc_objeto_novo_w,
				nr_seq_apres,
				ds_tarefa
			from	mp_tarefa
			where	nr_sequencia	= nr_seq_tarefa_w;	
		end loop;
		close c10;
	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into
		nr_seq_proc_raia_w;
	exit when c02%notfound;

		insert	into	mp_processo_raia
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_processo,
			nr_seq_unid_org,
			qt_topo,
			qt_esquerda,
			ie_orientacao,
			qt_altura,
			qt_largura)
		select	mp_processo_unid_org_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_sequencia_w,
			nr_seq_unid_org,
			qt_topo,
			qt_esquerda,
			ie_orientacao,
			qt_altura,
			qt_largura
		from	mp_processo_raia
		where	nr_sequencia	= nr_seq_proc_raia_w;
	end loop;
	close c02;
end if;

nr_seq_novo_processo_p	:= nr_sequencia_w;

commit;

end mp_criar_rascunho;
/