create or replace
procedure fa_gerar_estorno_entrega( 	nr_seq_entrega_p	number,
					nm_usuario_p		Varchar2) is 

nr_seq_entrega_item_w	number(10);

Cursor C01 is
	select 	nr_sequencia
	from	fa_entrega_medicacao_item
	where	nr_seq_fa_entrega = nr_seq_entrega_p
	and	dt_cancelamento is null;
begin
open C01;
loop
fetch C01 into	
	nr_seq_entrega_item_w;
exit when C01%notfound;
	begin
	fa_gerar_estorno_entrega_item(nr_seq_entrega_item_w, nm_usuario_p);	
	end;
end loop;
close C01;

commit;

end fa_gerar_estorno_entrega;
/