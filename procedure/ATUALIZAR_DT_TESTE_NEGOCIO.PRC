create or replace
procedure atualizar_dt_teste_negocio(nr_seq_p		number) is 

begin
update	man_ordem_servico
set     	dt_teste_negocio = sysdate
where   	nr_sequencia  = nr_seq_p;

commit;

end atualizar_dt_teste_negocio;
/