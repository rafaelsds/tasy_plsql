create or replace
procedure qua_atual_dt_envio_nota_trein (	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	qua_doc_trein_pessoa
set	dt_envio_nota	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end qua_atual_dt_envio_nota_trein;
/