create or replace
procedure pls_atualizar_aud_req_guia (	nr_seq_auditoria_p	pls_auditoria.nr_sequencia%type,
					nr_seq_requisicao_p	pls_requisicao.nr_sequencia%type,
					nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Ao finalizar a auditoria o sistema deve atualizar as informa��es da guia que 
foi gerada via Webservice
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [X  ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


nr_seq_guia_plano_w		pls_guia_plano.nr_sequencia%type;
ie_tipo_processo_w		pls_requisicao.ie_tipo_processo%type;


Cursor C01 is
	select 	a.ie_anexo_guia,
		a.ie_anexo_quimioterapia,
		a.ie_anexo_radioterapia,
		a.ie_anexo_opme,
		a.ie_estagio,
		a.dt_validade_senha,
		a.cd_senha
	from	pls_requisicao a
	where	a.nr_sequencia = nr_seq_requisicao_p;
		

Cursor C02 is
	select	a.nr_seq_proc_origem,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_ajuste,
		a.nr_seq_motivo_exc,
		a.ie_status,
		a.vl_item,
		a.ie_pacote_ptu
	from	pls_auditoria_item	a
	where	a.nr_seq_auditoria = nr_seq_auditoria_p
	and	a.nr_seq_proc_origem is not null	
	order by nr_sequencia;
	
Cursor C03 is
	select	a.nr_seq_mat_origem,
		a.nr_seq_material,
		a.qt_ajuste,
		a.nr_seq_motivo_exc,
		a.ie_status,
		a.vl_item,
		a.nr_seq_regra_vl_mat
	from	pls_auditoria_item	a
	where	a.nr_seq_auditoria = nr_seq_auditoria_p
	and	a.nr_seq_mat_origem is not null
	order by nr_sequencia;	
	
begin		

--Garante que a guia gerada � devido ao pedido recebido via Webservice	
select	max(nr_seq_guia_plano)
into	nr_seq_guia_plano_w
from	pls_guia_plano_imp
where	nr_seq_requisicao = nr_seq_requisicao_p;

if	( nr_seq_guia_plano_w is not null ) then

	select	ie_tipo_processo
	into	ie_tipo_processo_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_p;

	for C01_w in C01 loop
		begin	
			update	pls_guia_plano
			set	ie_anexo_guia 		= C01_w.ie_anexo_guia,
				ie_anexo_quimioterapia 	= C01_w.ie_anexo_quimioterapia,
				ie_anexo_radioterapia 	= C01_w.ie_anexo_radioterapia,
				ie_anexo_opme 		= C01_w.ie_anexo_opme,
				ie_status	= decode(C01_w.ie_estagio,
							1, 2,	-- Aberta				- Em an�lise
							2, 1,	-- Aprovada				- Autorizado
							3, 3,	-- Cancelada				- Negado
							4, 2,	-- Auditoria				- Em an�lise
							5, 2,	-- Auditoria Interc�mbio		- Em an�lise
							6, 1,	-- Parcialmente Aprovada		- Autorizado
							7, 3,	-- Reprovada				- Negado
							8, 2,	-- Aguardando autoriza��o do contratante- Em an�lise
							9, 1,	-- Autorizada pelo contratante		- Autorizado
							10, 2,	-- Aguardando envio Interc�mbio		- Em an�lise
							C01_w.ie_estagio),
				ie_estagio	= decode(C01_w.ie_estagio,
							1, 7,	-- Aberta				- Usu�rio (aguardando a��o)
							2, 6,	-- Aprovada				- Autorizado sem glosa
							3, 8,	-- Cancelada				- Cancelada
							4, 11,	-- Auditoria				- Auditoria interc�mbio
							5, 11,	-- Auditoria Interc�mbio		- Auditoria interc�mbio
							6, 10,	-- Parcialmente Aprovada		- Autorizado sem glosa
							7, 4,	-- Reprovada				- Negado
							8, 9,	-- Aguardando autoriza��o do contratante- Aguardando autoriza��o do contratante
							9, 6,	-- Autorizada				- Autorizado sem glosa
							10, 12,	-- Aguardando envio Interc�mbio		- Aguardando autoriza��o interc�mbio
							C01_w.ie_estagio),
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate,
				dt_validade_senha	= C01_w.dt_validade_senha,
				cd_senha		= C01_w.cd_senha
			where 	nr_sequencia 		= nr_seq_guia_plano_w;
			
			--Atualizar as informa��es dos procedimentos na guia liberados na OPS - Gest�o de an�lise de autoriza��o
			for C02_w in C02 loop	

				update	pls_guia_plano_proc
				set	qt_solicitada		= decode(ie_tipo_processo_w, 'I', 0, C02_w.qt_ajuste),
					qt_autorizada		= decode(C02_w.ie_status,'N',0, C02_w.qt_ajuste),
					ie_status		= decode(C02_w.ie_status,'A',decode(C01_w.ie_estagio, 10, 'I','S'),'N'),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_seq_guia		= nr_seq_guia_plano_w
				and	cd_procedimento		= C02_w.cd_procedimento
				and	ie_origem_proced	= C02_w.ie_origem_proced;
				
				update	pls_itens_lote_execucao
				set	qt_aprovado 		= C02_w.qt_ajuste,
					qt_pendente 		= C02_w.qt_ajuste,
					dt_validade_senha	= C01_w.dt_validade_senha
				where	nr_seq_req_proc 	= C02_w.nr_seq_proc_origem;
				
				update	pls_execucao_req_item
				set	qt_item			= decode(C02_w.ie_status,'N',0, C02_w.qt_ajuste)
				where	nr_seq_req_proc		= C02_w.nr_seq_proc_origem;
				
				update	pls_requisicao_proc
				set	qt_proc_executado 	= decode(C02_w.ie_status,'N',0, C02_w.qt_ajuste),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= C02_w.nr_seq_proc_origem;

			end loop;
			
			--Atualizar as informa��es dos materiais na guia liberados na OPS - Gest�o de an�lise de autoriza��o
			for C03_w in C03 loop					
				update	pls_guia_plano_mat
				set	qt_solicitada		= decode(ie_tipo_processo_w, 'I', 0, C03_w.qt_ajuste),
					qt_autorizada		= decode(C03_w.ie_status,'N',0, C03_w.qt_ajuste),
					ie_status		= decode(C03_w.ie_status,'A',decode(C01_w.ie_estagio, 10, 'I','S'),'N'),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_seq_guia	 	= nr_seq_guia_plano_w
				and	nr_seq_material		= C03_w.nr_seq_material;
				
				update	pls_itens_lote_execucao
				set	qt_aprovado 		= C03_w.qt_ajuste,
					qt_pendente 		= C03_w.qt_ajuste,
					dt_validade_senha	= C01_w.dt_validade_senha
				where	nr_seq_req_mat 		= C03_w.nr_seq_mat_origem;
				
				update	pls_execucao_req_item
				set	qt_item			= decode(C03_w.ie_status,'N',0, C03_w.qt_ajuste)
				where	nr_seq_req_mat 		= C03_w.nr_seq_mat_origem;
				
				update	pls_requisicao_mat
				set	qt_mat_executado 	= decode(C03_w.ie_status,'N',0, C03_w.qt_ajuste),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= C03_w.nr_seq_mat_origem;
				
			end loop;
		end;
		
		--Gerar as glosas da requisi��o na guia, quando for gerada negativa para o pedido de autoriza��o
		pls_gravar_glosa_req_guia_imp(nr_seq_requisicao_p, nr_seq_guia_plano_w, nm_usuario_p);
	end loop;	

end if;

commit;

end pls_atualizar_aud_req_guia;
/
