create or replace
procedure Lab_copiar_antibiotico(cd_microorganismo_origem_p		number,
				cd_microorganismo_destino_p		number,
				nr_seq_material_origem_p		number,
				nr_seq_material_destino_p		number,
				cd_medicamentos_p			varchar2,
				nm_usuario_p				varchar2) is

cd_medicamento_w	number(10);
nr_seq_material_w	number(10);
nr_seq_grupo_micro_w	number(10);

CURSOR C01 IS

select	cd_medicamento,
	nr_seq_material,
	nr_seq_grupo_micro
from	cih_microorg_medic
where	cd_microorganismo = cd_microorganismo_origem_p
and	obter_se_contido_char(cd_medicamento, cd_medicamentos_p) = 'S'
and	nr_seq_material	  = decode(nvl(nr_seq_material_origem_p, 0), 0, nr_seq_material, nr_seq_material_origem_p);

begin

delete	from cih_microorg_medic
where	cd_microorganismo = cd_microorganismo_destino_p
and	nr_seq_material	  = decode(nvl(nr_seq_material_destino_p, 0), 0, nr_seq_material, nr_seq_material_destino_p);

open c01;
loop
fetch c01 into
	cd_medicamento_w,
	nr_seq_material_w,
	nr_seq_grupo_micro_w;
	
	exit when c01%notfound;
	begin
		insert into cih_microorg_medic(
		cd_microorganismo,
		nr_seq_material,
		dt_atualizacao,
		nm_usuario,
		cd_medicamento,
		nr_seq_grupo_micro)
	values	(cd_microorganismo_destino_p,
		decode(nvl(nr_seq_material_destino_p, 0), 0, nr_seq_material_w, nr_seq_material_destino_p),
		sysdate,
		nm_usuario_p,
		cd_medicamento_w,
		nr_seq_grupo_micro_w);
	end;
end loop;
close c01;

commit;

end Lab_copiar_antibiotico;
/
