create or replace
procedure cpoe_fa_liberar_receita(
				nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 
			     
dt_inicio_w						date;
dt_validade_w					date;

begin

update	fa_receita_farmacia
set		dt_liberacao    = sysdate,	
		nm_usuario_lib	= nm_usuario_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao	= sysdate
where 	nr_sequencia 	= nr_sequencia_p;

select 	max(dt_inicio_receita),
		max(dt_validade_receita)
into	dt_inicio_w,
		dt_validade_w
from	fa_receita_farmacia
where	nr_sequencia = nr_sequencia_p;

if (dt_inicio_w is not null) and (dt_validade_w is not null) then
	preparar_medic_dias(nr_sequencia_p,nm_usuario_p);
end if;		

commit;

end cpoe_fa_liberar_receita;
/