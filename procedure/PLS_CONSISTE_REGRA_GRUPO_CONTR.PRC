create or replace
procedure pls_consiste_regra_grupo_contr
			(	nr_seq_regra_p		Number,
				nm_usuario_p		Varchar2) is 

nr_contrato_w			number(10);
nr_seq_contrato_w		number(10);

begin

select	max(nr_contrato)
into	nr_contrato_w
from	pls_mens_grupo_contrato
where	nr_sequencia	= nr_seq_regra_p;

if	(nvl(nr_contrato_w,0) <> 0) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato	= nr_contrato_w;
	
	if	(nvl(nr_seq_contrato_w,0) <> 0) then
		update	pls_mens_grupo_contrato
		set	nr_seq_contrato	= nr_seq_contrato_w
		where	nr_sequencia	= nr_seq_regra_p;
	end if;
else
	update	pls_mens_grupo_contrato
	set	nr_seq_contrato	= null
	where	nr_sequencia	= nr_seq_regra_p;
end if;

commit;

end pls_consiste_regra_grupo_contr;
/