CREATE OR REPLACE
PROCEDURE Gravar_Log_Processo(	cd_log_p			Number,
						dt_Inicio_P   		DATE,
  			           		NM_USUARIO_P      	VARCHAR2,
                                    ds_processo_P     	VARCHAR2) IS 
ds_tempo_w			Varchar2(10);
qt_dia_w			Number(15,5);
qt_seg_w			Number(15,0);

BEGIN
qt_dia_w			:= (sysdate - dt_inicio_p);
qt_seg_w			:= qt_dia_w * 24 * 60 * 60;

ds_tempo_w 			:= to_char(qt_seg_w);
					
/*insert into log_tasy (	dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
	values
			(	sysdate,
				nm_usuario_p,
				cd_log_p,
				ds_processo_p || '=' || ds_tempo_w);
commit;*/
END Gravar_Log_Processo;
/
