create or replace
procedure EXPORTAR_REGRAS_CARTA_MEDICA(	NR_SEQ_MODELO_P				NUMBER,
										NR_SEQ_MODELO_DESTINO_P		NUMBER,
										IE_SECAO_REGRA_P			VARCHAR2,
										NM_USUARIO_P				VARCHAR2) is 

nr_regra_carta_med_w		number(10);
nr_regra_carta_med_old_w	number(10);
qt_regra_destino_w			number(10);
										
cursor C01 is
	select  IE_SECAO,
			NR_SEQUENCIA nr_seq_regra
	from    CARTA_MEDICA_REGRA
	where   NR_SEQ_MODELO = nr_seq_modelo_p
	and		(ie_secao_regra_p = 'TD' or ie_secao = ie_secao_regra_p);
	
cursor C02 is
	select  CD_AREA_PROCEDIMENTO,
			CD_CLASSE_MATERIAL,
			CD_GRUPO_MATERIAL,
			CD_GRUPO_PROC,
			CD_MATERIAL,
			CD_SUBGRUPO_MATERIAL,
			IE_CLASSIFICACAO_DOENCA,
			IE_EVOLUCAO_CLINICA,
			IE_FORA_FAIXA,
			IE_INSERIR_AUT,
			IE_TIPO_DIAGNOSTICO,
			IE_TODOS,
			NR_PROC_INTERNO,
			NR_SEQ_EXAME,
			CD_TIPO_RECOMENDACAO,
			IE_TIPO_REGRA,
			NR_SEQ_CLASSIF_RECOMENDACAO
	from    CARTA_MEDICA_REGRA_ITEM
	where   NR_SEQ_REGRA = nr_regra_carta_med_old_w;

begin
	select 	count(*)
	into	qt_regra_destino_w
	from    CARTA_MEDICA_REGRA
	where   NR_SEQ_MODELO = nr_seq_modelo_destino_p
	and		(ie_secao_regra_p = 'TD' or ie_secao = ie_secao_regra_p);
	
	if (qt_regra_destino_w = 0) then
		for c01_w in C01 loop
			begin
				nr_regra_carta_med_old_w := c01_w.nr_seq_regra;
				
				select 	carta_medica_regra_seq.nextval
				into	nr_regra_carta_med_w
				from	dual;
			
				insert into CARTA_MEDICA_REGRA(
					NR_SEQUENCIA,
					NR_SEQ_MODELO,
					DT_ATUALIZACAO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO,
					NM_USUARIO_NREC,
					IE_SECAO
				) values (
					nr_regra_carta_med_w,
					nr_seq_modelo_destino_p,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					c01_w.IE_SECAO);
					
				commit;
				
				for c02_w in C02 loop
					begin
						insert into CARTA_MEDICA_REGRA_ITEM(
							NR_SEQUENCIA,
							NM_USUARIO,
							NM_USUARIO_NREC,
							DT_ATUALIZACAO,
							DT_ATUALIZACAO_NREC,
							CD_AREA_PROCEDIMENTO,
							CD_CLASSE_MATERIAL,
							CD_GRUPO_MATERIAL,
							CD_GRUPO_PROC,
							CD_MATERIAL,
							CD_SUBGRUPO_MATERIAL,
							IE_CLASSIFICACAO_DOENCA,
							IE_EVOLUCAO_CLINICA,
							IE_FORA_FAIXA,
							IE_INSERIR_AUT,
							IE_TIPO_DIAGNOSTICO,
							IE_TODOS,
							NR_PROC_INTERNO,
							NR_SEQ_EXAME,
							NR_SEQ_REGRA,
							CD_TIPO_RECOMENDACAO,
							IE_TIPO_REGRA,
							NR_SEQ_CLASSIF_RECOMENDACAO
						) values (
							carta_medica_regra_item_seq.nextval,
							nm_usuario_p,
							nm_usuario_p,
							sysdate,
							sysdate,
							c02_w.CD_AREA_PROCEDIMENTO,
							c02_w.CD_CLASSE_MATERIAL,
							c02_w.CD_GRUPO_MATERIAL,
							c02_w.CD_GRUPO_PROC,
							c02_w.CD_MATERIAL,
							c02_w.CD_SUBGRUPO_MATERIAL,
							c02_w.IE_CLASSIFICACAO_DOENCA,
							c02_w.IE_EVOLUCAO_CLINICA,
							c02_w.IE_FORA_FAIXA,
							c02_w.IE_INSERIR_AUT,
							c02_w.IE_TIPO_DIAGNOSTICO,
							c02_w.IE_TODOS,
							c02_w.NR_PROC_INTERNO,
							c02_w.NR_SEQ_EXAME,
							nr_regra_carta_med_w,
							c02_w.CD_TIPO_RECOMENDACAO,
							c02_w.IE_TIPO_REGRA,
							c02_w.NR_SEQ_CLASSIF_RECOMENDACAO);
						
						commit;
					end;
				end loop;
			end;
		end loop;
	end if;
end exportar_regras_carta_medica;
/