create or replace
procedure EHR_Inativar_Template(nr_sequencia_p		number,
			ie_opcao_p		varchar2,
			nm_usuario_p		Varchar2) is 

qt_nao_lib_w		number(10);

begin

if	(ie_opcao_p = 'I') then /*Inativar*/
	begin
	update	ehr_template
	set	dt_inativacao		= sysdate,
		nm_usuario_inativ	= nm_usuario_p,
		nm_usuario		= nm_usuario_p 
	where	nr_sequencia		= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 'RI') then /*Reverter inativação*/
	begin
	update	ehr_template
	set	dt_inativacao		= null,
		nm_usuario_inativ	= null,
		nm_usuario		= nm_usuario_p 
	where	nr_sequencia		= nr_sequencia_p;
	end;
end if;

commit;

end EHR_Inativar_Template;
/