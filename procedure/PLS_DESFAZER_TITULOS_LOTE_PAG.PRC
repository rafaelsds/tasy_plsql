create or replace 
procedure pls_desfazer_titulos_lote_pag(nr_seq_lote_p			number,
					nm_usuario_p			varchar2,
					dt_cancelamento_p		date) is 

nr_titulo_w			number(10);
nr_seq_pag_prestador_w		number(10);
nr_seq_prestador_w		number(10);
nr_titulo_receber_w		number(10);
qt_copartic_w			pls_integer;
ie_desfaz_lote_pag_copartic_w	pls_parametros.ie_desfaz_lote_pag_copartic%type;
cd_estabelecimento_w		pls_lote_pagamento.cd_estabelecimento%type;
nr_seq_escrit_w			titulo_pagar_escrit.nr_seq_escrit%type;
nr_bordero_w			bordero_tit_pagar.nr_bordero%type;
ie_data_lote_prod_med_w		pls_parametro_contabil.ie_data_lote_prod_med%type;
nr_lote_contabilizado_w		lote_contabil.nr_lote_contabil%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;
ie_status_origem_w		lote_contabil.ie_status_origem%type;

Cursor C01 is
	select	c.nr_titulo nr_titulo_pagar,
		b.nr_sequencia,
		b.nr_seq_prestador,
		null nr_titulo_receber
	from	titulo_pagar		c,
		pls_pagamento_prestador	b,
		pls_lote_pagamento	a
	where	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_pls_pag_prest
	and	a.nr_sequencia 	= nr_seq_lote_p
	union
	select	d.nr_titulo nr_titulo_pagar,
		a.nr_sequencia,
		a.nr_seq_prestador,
		null nr_titulo_receber
	from	titulo_pagar			d,
		pls_pag_prest_venc_trib		c,
		pls_pag_prest_vencimento 	b,
		pls_pagamento_prestador 	a
	where	d.nr_seq_pls_venc_trib 	= c.nr_sequencia
	and	b.nr_sequencia		= c.nr_seq_vencimento
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	a.nr_seq_lote 		= nr_seq_lote_p
	union all
	select	null nr_titulo_pagar,
		a.nr_sequencia,
		a.nr_seq_prestador,
		c.nr_titulo nr_titulo_receber
	from	titulo_receber			c,
		pls_pag_prest_vencimento 	b,
		pls_pagamento_prestador 	a
	where	c.nr_titulo 		= b.nr_titulo_receber
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	a.nr_seq_lote 		= nr_seq_lote_p;
	
Cursor C02 (	nr_seq_lote_pc		pls_lote_pagamento.nr_sequencia%type) is
	select	b.nr_sequencia,
		b.nr_adiant_pago_origem,
		b.vl_item
	from	pls_pagamento_item	b,
		pls_pagamento_prestador	a
	where	a.nr_sequencia		= b.nr_seq_pagamento
	and	a.nr_seq_lote		= nr_seq_lote_pc
	and	a.ie_cancelamento is null
	and	b.nr_adiant_pago_origem is not null;
	
begin

if	(nr_seq_lote_p is not null) then
	
	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	pls_lote_pagamento
	where	nr_sequencia	= nr_seq_lote_p;
	
	select	max(nvl(ie_data_lote_prod_med,'C'))
	into	ie_data_lote_prod_med_w
	from	pls_parametro_contabil
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(ie_data_lote_prod_med_w = 'T') then	
		select	max(nvl(nr_lote_contabil,0)) nr_lote_contabil
		into	nr_lote_contabilizado_w
		from	pls_lote_pagamento
		where	nr_sequencia = nr_seq_lote_p
		and	nvl(nr_lote_contabil,0) <> 0;
		
		if 	(nr_lote_contabilizado_w <> 0) then
			select	nvl(max(ie_status_origem), 'M')
			into	ie_status_origem_w
			from	lote_contabil
			where	nr_lote_contabil = nr_lote_contabilizado_w;

			if	(ie_status_origem_w <> 'SO') then		
				/* O pagamento de produ��o j� foi contabilizado no lote #@NR_LOTE_CONTABILIZADO#@. */
				wheb_mensagem_pck.exibir_mensagem_abort(326581, 'NR_LOTE_CONTABILIZADO=' || nr_lote_contabilizado_w);	
			end if;
		end if;
	end if;
	
	select	nvl(ie_desfaz_lote_pag_copartic,'S')
	into	ie_desfaz_lote_pag_copartic_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(ie_desfaz_lote_pag_copartic_w = 'N') then
		select	count(1)
		into	qt_copartic_w
		from	pls_conta_coparticipacao	a
		where	a.nr_seq_mensalidade_seg is not null
		and	a.nr_seq_conta	in	(select	x.nr_seq_conta
						from	pls_conta_medica_resumo	x
						where	x.nr_seq_lote_pgto	= nr_seq_lote_p);
		if	(qt_copartic_w	> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(249681);
		end if;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		nr_titulo_w,
		nr_seq_pag_prestador_w,
		nr_seq_prestador_w,
		nr_titulo_receber_w;
	exit when C01%notfound;
		begin
		if	(nr_titulo_w is not null) then
		
			select	max(nr_bordero)
			into	nr_bordero_w
			from	titulo_pagar_bordero_v
			where	nr_titulo	= nr_titulo_w;
			
			if	(nr_bordero_w is not null) then
				wheb_mensagem_pck.exibir_mensagem_abort(283640, 'NR_TITULO=' || nr_titulo_w || ';NR_BORDERO=' || nr_bordero_w);
				-- O t�tulo #@NR_TITULO#@ j� est� no border� de pagamento #@NR_BORDERO#@!
				-- N�o � poss�vel cancelar este pagamento.
			end if;

			select	max(nr_seq_escrit)
			into	nr_seq_escrit_w
			from	titulo_pagar_escrit
			where	nr_titulo	= nr_titulo_w;

			if	(nr_seq_escrit_w is not null) then
				wheb_mensagem_pck.exibir_mensagem_abort(283646, 'NR_TITULO=' || nr_titulo_w || ';NR_SEQ_ESCRIT=' || nr_bordero_w);
				-- O t�tulo #@NR_TITULO#@ j� est� no pagamento_escritural #@NR_SEQ_ESCRIT#@!
				-- N�o � poss�vel cancelar este pagamento.
			end if;
			
			cancelar_titulo_pagar(nr_titulo_w, nm_usuario_p, nvl(dt_cancelamento_p, sysdate));
			
			update	titulo_pagar
			set	nr_seq_pls_pag_prest	= null,
				nr_seq_pls_venc_trib	= null,
				ds_observacao_titulo	= 	('Este t�tulo havia sido gerado pelo lote de pagamento ' || nr_seq_lote_p || ' e o prestador ' || nr_seq_prestador_w || '.')	
			where	nr_titulo		= nr_titulo_w;
			
			update 	pls_pag_prest_vencimento
			set	nr_titulo 		= null
			where	nr_seq_pag_prestador 	= nr_seq_pag_prestador_w
			and	nr_titulo		= nr_titulo_w;	
			
			update	TITULO_PAGAR_IMPOSTO
			set	NR_SEQ_PLS_VENC_TRIB	= null
			where	nr_titulo		= nr_titulo_w;
			
		elsif	(nr_titulo_receber_w is not null) then
			cancelar_titulo_receber(nr_titulo_receber_w, nm_usuario_p, 'N',sysdate);
			
			update	titulo_receber
			set	ds_observacao_titulo	= 	('Este t�tulo havia sido gerado pelo lote de pagamento ' || nr_seq_lote_p || ' e o prestador ' || nr_seq_prestador_w || '.')	
			where	nr_titulo		= nr_titulo_receber_w;
			
			update 	pls_pag_prest_vencimento
			set	nr_titulo_receber	= null
			where	nr_seq_pag_prestador 	= nr_seq_pag_prestador_w
			and	nr_titulo_receber	= nr_titulo_receber_w;	
		end if;	
		end;
	end loop;
	close C01;
	
	-- Varre todos os itens que s�o provenientes de adiantamentos pagos e gera a devolu��o no adiantamento pago
	for r_C02_w in C02 (nr_seq_lote_p) loop
		pls_devolver_adiant_evento(	r_C02_w.nr_sequencia,
						r_C02_w.nr_adiant_pago_origem,
						r_C02_w.vl_item,
						nm_usuario_p);
	end loop;
	
	update	pls_lote_pagamento
	set	dt_geracao_titulos	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia 		= nr_seq_lote_p;
	
	--Necess�rio retornar o status do protocolo para liberado para pagamento quando os titulos s�o cancelados
	update	pls_protocolo_conta	prot
	set	prot.ie_status		= '3'
	where	1 = 1
	and	ie_status <> '4'
	and	exists	(select 1 
			from 	pls_conta_medica_resumo resumo
			where	resumo.nr_seq_protocolo = prot.nr_sequencia
			and	resumo.nr_seq_lote_pgto	= nr_seq_lote_p);
			
	update	pls_evento_movimento	c
	set	nr_titulo_pagar = null,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao 	= sysdate
	where 	exists (select 	1
			from	pls_pagamento_item	b,
				pls_pagamento_prestador	a
			where	b.nr_sequencia 	= c.nr_seq_pagamento_item
			and	a.nr_sequencia 	= b.nr_seq_pagamento
			and	a.nr_seq_lote 	= nr_seq_lote_p
			and	((c.cd_pf_titulo_pagar is not null) or (c.cd_cgc_titulo_pagar is not null))
			and	c.nr_titulo_pagar is not null);

	select 	nvl(max(ie_concil_contab), 'N')
	into	ie_concil_contab_w
	from	pls_visible_false
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(ie_concil_contab_w = 'S') then
		pls_ctb_onl_gravar_movto_pck.gravar_movto_canc_desvinc_tit(nr_seq_lote_p, cd_estabelecimento_w, nm_usuario_p, dt_cancelamento_p);
	end if;
	commit;
end if;

end pls_desfazer_titulos_lote_pag;
/
