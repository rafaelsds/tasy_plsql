create or replace
procedure pls_oc_cta_val_27_mat_imp (	nr_seq_combinada_p		pls_oc_cta_combinada.nr_sequencia%type,
										nr_id_transacao_p		pls_oc_cta_selecao_imp.nr_id_transacao%type,
										nm_usuario_p			usuario.nm_usuario%type) is

dados_segurado_w			pls_ocor_imp_pck.dados_benef;
dados_limitacao_w			pls_ocor_imp_pck.dados_limitacao;
dados_periodo_exec_w		pls_ocor_imp_pck.t_datas_periodo_limit_row;
dados_mat_limitacao_w		pls_ocor_imp_pck.t_pls_limitacao_mat_row;
dados_restricao_w			pls_ocor_imp_pck.dados_restricao_select;
dados_restricao_imp_w		pls_ocor_imp_pck.dados_restricao_select;
dados_conta_mat_w			pls_ocor_imp_pck.dados_conta_mat;
dados_tabela_aux_w			pls_ocor_imp_pck.dados_tabela_aux_val_limit;
dados_tb_selecao_w			pls_ocor_imp_pck.dados_table_selecao_ocor;
dado_bind_w					sql_pck.t_dado_bind;
cursor_w					sql_pck.t_cursor;
qt_permitida_w			pls_limitacao.qt_permitida%type;
nr_seq_titular_w		pls_segurado.nr_seq_titular%type;
qt_pessoas_w			pls_integer := 0;

i				pls_integer;
qt_mat_selecao_w		pls_integer;
ds_itens_util_w			varchar2(4000);
ds_sql_w			varchar2(4000);
qt_mat_util_total_w		pls_conta_mat_imp.qt_executado%type;
					
-- Obter os benefici�rios e seus dados das contas que est�o na tabela de sele��o. Est� sendo utilizado o distinct para que seja passado uma vez apenas para cada benefici�rio pois o processo
-- da valida��o � caro e quanto menos for executado melhor
cursor C01 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	distinct
		b.nr_seq_segurado_conv,
		c.nr_seq_plano,
		c.nr_seq_contrato,
		c.nr_seq_intercambio,
		d.dt_execucao_conv,
		c.dt_contrato,
		c.dt_contratacao,
		c.dt_primeira_utilizacao,
		c.nr_seq_titular
	from	pls_oc_cta_selecao_imp		a,
		pls_conta_imp			b,
		pls_segurado_conta_imp_v	c,
		pls_conta_mat_imp		d
	where	a.nr_id_transacao	= nr_id_transacao_pc
	and	a.ie_valido		= 'S'
	and	a.ie_tipo_registro	= 'M'
	and	b.nr_sequencia		= a.nr_seq_conta
	and	c.nr_sequencia		= b.nr_seq_segurado_conv
	and	d.nr_seq_conta		= a.nr_seq_conta
	and	d.nr_sequencia		= a.nr_seq_conta_mat;
	
-- Obter as limita��es a qual o benefici�rio est� atrelado, seja pelo plano ou pelo contrato de interc�mbio. 
-- Est� sendo utilizado o union e buscado todas as limita��es do benefici�rio para evitar replica��o de c�digo
-- e para que passe e verifique cada limita��o apenas uma vez.	
cursor C02 (	nr_seq_contrato_pc	pls_segurado_conta_v.nr_seq_contrato%type,
		nr_seq_plano_pc		pls_segurado_conta_v.nr_seq_plano%type,
		nr_seq_intercambio_pc	pls_segurado_conta_v.nr_seq_intercambio%type) is
	select	a.nr_sequencia	nr_seq_limitacao,
		a.nr_seq_tipo_limitacao,
		a.qt_permitida,
		a.ie_periodo,
		a.qt_meses_intervalo,
		a.ie_tipo_periodo,
		b.ie_tipo_incidencia,
		nvl(a.ie_qt_familia,'N') ie_qt_familia
	from	pls_limitacao		a,
		pls_tipo_limitacao	b
	where	a.nr_seq_contrato	= nr_seq_contrato_pc
	and	((a.nr_seq_plano_contrato is null) or (a.nr_seq_plano_contrato = nr_seq_plano_pc))
	and	a.qt_permitida		is not null
	and	b.nr_sequencia		= a.nr_seq_tipo_limitacao
	and	b.ie_situacao		= 'A'
	union
	select	a.nr_sequencia	nr_seq_limitacao,
		a.nr_seq_tipo_limitacao,
		a.qt_permitida,
		a.ie_periodo,
		a.qt_meses_intervalo,
		a.ie_tipo_periodo,
		b.ie_tipo_incidencia,
		nvl(a.ie_qt_familia,'N') ie_qt_familia
	from	pls_limitacao		a,
		pls_tipo_limitacao	b
	where	a.nr_seq_plano		= nr_seq_plano_pc
	and	a.qt_permitida		is not null
	and	b.nr_sequencia		= a.nr_seq_tipo_limitacao
	and	b.ie_situacao		= 'A'
	union
	select	a.nr_sequencia	nr_seq_limitacao,
		a.nr_seq_tipo_limitacao,
		a.qt_permitida,
		a.ie_periodo,
		a.qt_meses_intervalo,
		a.ie_tipo_periodo,
		b.ie_tipo_incidencia,
		nvl(a.ie_qt_familia,'N') ie_qt_familia
	from	pls_limitacao		a,
		pls_tipo_limitacao	b
	where	a.nr_seq_intercambio	= nr_seq_intercambio_pc
	and	a.qt_permitida		is not null
	and	b.nr_sequencia		= a.nr_seq_tipo_limitacao
	and	b.ie_situacao		= 'A';
	
-- Obter os procedimentos para determinado tipo de limita��o. Ser� passado por �ltimo para os procedimentos que n�o est�o liberados para que seja removido da tabela 
-- auxiliar os itens que n�o est�o liberadas.
cursor C03 (	nr_seq_tipo_limitacao_pc	pls_limitacao.nr_seq_tipo_limitacao%type) is
	select	a.nr_seq_material,
		a.ie_tipo_guia,
		a.cd_doenca_cid,
		a.ie_liberado
	from	table(pls_grupos_pck.obter_mats_limitacao(nr_seq_tipo_limitacao_pc)) a
	order by a.ie_liberado desc;
	
-- Obter os itens que est�o na tabela auxiliar. Ser� passado por �ltimo para os itens que est�o na tabela de sele��o para que seja inserida ocorr�ncia apenas para estes itens.	
cursor C04 (	nr_id_transacao_pc		pls_oc_cta_val_limit_aux.nr_id_transacao%type,
		nr_seq_segurado_pc		pls_oc_cta_val_limit_aux.nr_seq_segurado%type,
		nr_seq_tipo_limitacao_pc	pls_oc_cta_val_limit_aux.nr_seq_tipo_limitacao%type) is
	select	a.nr_seq_conta, 
		a.nr_seq_conta_mat,
		a.qt_item,
		a.ie_selecao,
		b.nr_sequencia nr_seq_selecao
	from	pls_oc_cta_val_limit_aux	a,
		pls_oc_cta_selecao_imp		b
	where	a.nr_id_transacao	= b.nr_id_transacao
	and	a.nr_seq_conta		= b.nr_seq_conta
	and	a.nr_seq_conta_mat	= b.nr_seq_conta_mat
	and	a.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_seq_segurado	= nr_seq_segurado_pc
	and	a.nr_seq_tipo_limitacao	= nr_seq_tipo_limitacao_pc
	and	a.ie_liberado		= 'S'
	order by a.ie_selecao;

begin

-- Aqui ser�o buscados os benefici�rios das contas que est�o na tabela de sele��o.
for	r_C01_w in C01 (nr_id_transacao_p) loop
	
	-- Atualizar os dados do benefici�rio atual.
	dados_segurado_w.nr_sequencia		:= r_C01_w.nr_seq_segurado_conv;
	dados_segurado_w.nr_seq_contrato	:= r_C01_w.nr_seq_contrato;
	dados_segurado_w.nr_seq_plano		:= r_C01_w.nr_seq_plano;
	dados_segurado_w.nr_seq_intercambio	:= r_C01_w.nr_seq_intercambio;
	dados_segurado_w.dt_adesao		:= r_C01_w.dt_contratacao;
	dados_segurado_w.dt_contrato		:= r_C01_w.dt_contrato;
	dados_segurado_w.dt_primeira_utilizacao	:= r_C01_w.dt_primeira_utilizacao;
	dados_segurado_w.nr_seq_titular		:= r_C01_w.nr_seq_titular;
	
	
	-- Para cada benefici�rio ser�o buscadas as limita��es contratuais do plano e do contrato do benefici�rio que for benefici�rio da operadora, para interc�mbio
	-- ser� feito de outra forma pois devido ao campo ser diferente ent�o temos mais facilidade buscando o interc�mbio em outro momento, apenas quando ele for 
	-- informado.
	for	r_C02_w in C02 (r_C01_w.nr_seq_contrato, r_C01_w.nr_seq_plano, r_C01_w.nr_seq_intercambio) loop
	
		-- Obter os dados da limita��o atual para realizar as consist�ncias devidas.
		dados_limitacao_w.nr_sequencia			:= r_C02_w.nr_seq_limitacao;		
		dados_limitacao_w.nr_seq_tipo_limitacao	:= r_C02_w.nr_seq_tipo_limitacao;	
		dados_limitacao_w.qt_permitida			:= r_C02_w.qt_permitida;	
		dados_limitacao_w.ie_periodo			:= r_C02_w.ie_periodo;		
		dados_limitacao_w.qt_meses_intervalo	:= r_C02_w.qt_meses_intervalo;	
		dados_limitacao_w.ie_tipo_periodo		:= r_C02_w.ie_tipo_periodo;
		dados_limitacao_w.ie_tipo_incidencia	:= r_C02_w.ie_tipo_incidencia;
		dados_limitacao_w.ie_qt_familia			:= r_C02_w.ie_qt_familia;
		
		-- Verificar se o tipo de per�odo informado � referente a datas.
		if	(dados_limitacao_w.ie_tipo_periodo in ('D','S','M')) then
		
			-- Verificar o tipo de per�odo informado na regra para obter o per�odo de verifica��o da regra. Atrav�s das datas obtidas nesta se��o ser�o buscados os
			-- materiais utilizados pelo benenfici�rio para verificar se ultrapassa a quantidade permitida pelo contrato.
			dados_periodo_exec_w := pls_ocor_imp_pck.obter_periodo_exec_limit(	dados_limitacao_w,
												dados_segurado_w, 
												r_C01_w.dt_execucao_conv,
												nm_usuario_p);
												
			-- Atualiza os dados da limitacao com a informa��o de come�o e fim de per�odo.
			dados_limitacao_w.dt_inicio_periodo	:= dados_periodo_exec_w.dt_inicio;
			dados_limitacao_w.dt_fim_periodo	:= dados_periodo_exec_w.dt_fim;
			
			-- Se n�o tiver encontrado as datas verifica materiais apenas na mesma data do material.
			if	(dados_limitacao_w.dt_inicio_periodo is null) then
				
				dados_limitacao_w.dt_inicio_periodo := trunc(r_C01_w.dt_execucao_conv);
			end if;
			
			if	(dados_limitacao_w.dt_fim_periodo is null) then
			
				dados_limitacao_w.dt_fim_periodo := fim_dia(r_C01_w.dt_execucao_conv);
			end if;
		end if;
		
		-- Ap�s encontrar as limita��es contratuais verificar os materiais e\ou tipo de guia e\ou CID que cada uma delas ir� bloquear o uso. 
		-- Para cada regra obtida ser�o buscados os registros que se encaixam para o benefici�rio e somadas as quantidades utilizadas para verificar se 
		-- deve ser gerada a ocorr�ncia ou n�o. Para isso ser� populada a tabela auxiliar com os materiais que foram utilizacos e 
		-- se encaixam nos materiais para a limita��o, ap�s ser�o percorridos os registros passando para os materiais que n�o est�o 
		-- na tabela de sele��o e ap�s para os que est�o para que seja glosado apenas o excedentee inserir ocorr�ncia apenas em registros da 
		-- consist�ncia atual. Ser� passado inicialmente pelos materiais que est�o liberados e posteriormente para os que n�o est�o liberados, 
		-- para que sejam removidos. A cada loop dos materiais selecionados, para os materiais que j� haviam sido selecionados anteriormente ser�
		-- atualizado o campo IE_LIBERADO da tabela auxiliar, para que sejam removidos os materiais n�o liberados, e ao final do processo ser�o 
		-- considerados apenas os materiais que est�o com o IE_LIBERADO como 'S' na tabela de sele��o.
		
		for r_C03_w in C03 (dados_limitacao_w.nr_seq_tipo_limitacao) loop
		
			-- Alimentar os dados da regra da limita��o. Ser�o usados para montar a restri��o do select que ser� montado para buscar os materiais 
			-- a que a regra est� bloqueando.
			dados_mat_limitacao_w.nr_seq_material	:= r_C03_w.nr_seq_material;
			dados_mat_limitacao_w.ie_tipo_guia	:= r_C03_w.ie_tipo_guia;
			dados_mat_limitacao_w.cd_doenca_cid	:= r_C03_w.cd_doenca_cid;
			dados_mat_limitacao_w.ie_liberado	:= r_C03_w.ie_liberado;
			dado_bind_w.delete;
			
			-- Montar a restri��o da regra utilizando os campos IMP
			dados_restricao_imp_w := pls_obter_restr_val_27_imp (	dados_segurado_w, nr_id_transacao_p, dados_limitacao_w, 
										'M', null, dados_mat_limitacao_w, 
										'IMP', dado_bind_w);
			
			-- Montar a restri��o da regra. 
			dados_restricao_w := pls_obter_restr_val_27_imp (	dados_segurado_w, nr_id_transacao_p, dados_limitacao_w, 
										'M', null, dados_mat_limitacao_w, 
										'N', dado_bind_w);
			
			-- S� executa a verifica��o se alguma restri��o for informada. Se estiverem todas nulas pode ser que o tipo de inicid�ncia do tipo 
			-- de limita��o n�o esteja informado, este campo � obrigat�rio e sem ele n�o ser� aplicada a valida��o. Se tiver uma das restri��es informadas ent�o ser�
			-- aplicada a informa��o.
			if	(dados_restricao_w.qt_restricao > 0) and
				(dados_restricao_imp_w.qt_restricao > 0) then
				
				-- Montar o select que busca os mateiriais utilizados.
				ds_sql_w :=	'select	conta.nr_sequencia nr_seq_conta,			' || pls_util_pck.enter_w || 
						'	mat.nr_sequencia nr_seq_conta_mat,			' || pls_util_pck.enter_w || 
						'	mat.qt_executado qt_mat, 				' || pls_util_pck.enter_w || 
						'	mat.dt_execucao_conv dt_atendimento,			' || pls_util_pck.enter_w || 
						'	(select	count(1)					' || pls_util_pck.enter_w || 
						'	 from	pls_oc_cta_selecao_imp x			' || pls_util_pck.enter_w || 
						'	 where	x.nr_id_transacao	= :nr_id_transacao_p	' || pls_util_pck.enter_w || 
						'	 and	x.ie_valido = ''S''				' || pls_util_pck.enter_w || 
						'	 and	x.ie_tipo_registro = ''M''			' || pls_util_pck.enter_w || 
						'	 and	x.nr_seq_conta = conta.nr_sequencia		' || pls_util_pck.enter_w || 
						'	 and	x.nr_seq_conta_mat = mat.nr_sequencia)	qt_tran	' || pls_util_pck.enter_w || 
						'from	pls_conta_imp conta,	 				' || pls_util_pck.enter_w || 
						'	pls_conta_mat_imp mat 					' || pls_util_pck.enter_w || 
						'where	1 = 1 							' || pls_util_pck.enter_w || 
						dados_restricao_imp_w.ds_restricao_conta 			  || pls_util_pck.enter_w || 
						'and	mat.nr_seq_conta = conta.nr_sequencia 			' || pls_util_pck.enter_w || 
						dados_restricao_imp_w.ds_restricao_mat 				  || pls_util_pck.enter_w ||
						'union all							' || pls_util_pck.enter_w || 
						'select	conta.nr_sequencia nr_seq_conta, 			' || pls_util_pck.enter_w || 
						'	mat.nr_sequencia nr_seq_conta_mat, 			' || pls_util_pck.enter_w || 
						'	mat.qt_ok qt_mat, 					' || pls_util_pck.enter_w || 
						'	mat.dt_atendimento,					' || pls_util_pck.enter_w || 
						'	(select	count(1)					' || pls_util_pck.enter_w || 
						'	 from	pls_selecao_ocor_cta x				' || pls_util_pck.enter_w || 
						'	 where	x.nr_id_transacao	= :nr_id_transacao_p	' || pls_util_pck.enter_w || 
						'	 and	x.ie_valido = ''S''				' || pls_util_pck.enter_w || 
						'	 and	x.ie_tipo_registro = ''M''			' || pls_util_pck.enter_w || 
						'	 and	x.nr_seq_conta = conta.nr_sequencia		' || pls_util_pck.enter_w || 
						'	 and	x.nr_seq_conta_mat = mat.nr_sequencia) qt_tran	' || pls_util_pck.enter_w || 
						'from	pls_conta_ocor_v conta, 				' || pls_util_pck.enter_w || 
						'	pls_conta_mat_ocor_v mat 				' || pls_util_pck.enter_w || 
						'where	1 = 1 							' || pls_util_pck.enter_w || 
						dados_restricao_w.ds_restricao_conta 			 	  || pls_util_pck.enter_w || 
						'and	mat.nr_seq_conta = conta.nr_sequencia 			' || pls_util_pck.enter_w || 
						dados_restricao_w.ds_restricao_mat 				  || pls_util_pck.enter_w ||
						'order by dt_atendimento ';
						
				sql_pck.bind_variable(':nr_id_transacao_p' , nr_id_transacao_p, dado_bind_w);
				
				begin
					
					-- Abrir um novo cursor
					cursor_w := sql_pck.executa_sql_cursor(ds_sql_w, dado_bind_w);
					
					loop
						fetch cursor_w into 
							dados_conta_mat_w.nr_seq_conta,
							dados_conta_mat_w.nr_seq_conta_mat,
							dados_conta_mat_w.qt_material,
							dados_conta_mat_w.dt_atendimento,
							qt_mat_selecao_w;
						exit when cursor_w%notfound;
						
						-- Verificar se est� na tabela de selecao para atualizar o campo IE_SELECAO da tabela auxiliar.
						if	(qt_mat_selecao_w = 0) then
							
							dados_tabela_aux_w.ie_selecao := 'N';
						else
							dados_tabela_aux_w.ie_selecao := 'S';
						end if;
						
						-- Gravar os dados selecionados para gravar na tabela auxiliar. Todo o processo de grava��o ou atualiza��o ser� feito na procedure
						-- GERENCIA_TABELA_AUX_VAL_LIMIT da PLS_OCOR_IMP_PCK.
						dados_tabela_aux_w.nr_seq_segurado		:= r_C01_w.nr_seq_segurado_conv;
						dados_tabela_aux_w.nr_seq_tipo_limitacao	:= dados_limitacao_w.nr_seq_tipo_limitacao;
						dados_tabela_aux_w.nr_id_transacao		:= nr_id_transacao_p;
						dados_tabela_aux_w.ie_liberado			:= r_C03_w.ie_liberado;
						dados_tabela_aux_w.qt_item			:= dados_conta_mat_w.qt_material;
						dados_tabela_aux_w.nr_seq_conta			:= dados_conta_mat_w.nr_seq_conta;
						dados_tabela_aux_w.nr_seq_conta_mat		:= dados_conta_mat_w.nr_seq_conta_mat;
						
						pls_ocor_imp_pck.gerencia_tabela_aux_val_limit( 'I', 'M', nr_id_transacao_p, dados_tabela_aux_w, nm_usuario_p);
					end loop; -- Contas filtradas
					close cursor_w;
				exception
				when others then
					if	(cursor_w%isopen) then	
						-- Fechar os cursores que continuam abertos, os cursores que utilizam FOR - LOOP n�o necessitam serem fechados, ser�o fechados automaticamente.
						close cursor_w;
					end if;
					-- Insere o log na tabela e aborta a opera��o
					pls_ocor_imp_pck.trata_erro_sql_dinamico (	nr_seq_combinada_p, null, ds_sql_w, 
											nr_id_transacao_p, nm_usuario_p, 'N');
				end;
			end if;
		end loop;
		
		--Determina a quantidade permitida.
		if (dados_limitacao_w.ie_qt_familia = 'S')  then
			
			select	max(nr_seq_titular)
			into	nr_seq_titular_w
			from	pls_segurado
			where	nr_sequencia	= dados_segurado_w.nr_sequencia
			and	ie_situacao_atend	= 'A'
			and	substr(pls_obter_dados_segurado(nr_sequencia,'CS'),1,10) = 'A';
			
			if	(nr_seq_titular_w	is null) then
				select	count(1)
				into	qt_pessoas_w
				from	pls_segurado
				where	nr_seq_titular	= dados_segurado_w.nr_sequencia
				and	ie_situacao_atend	= 'A'
				and	substr(pls_obter_dados_segurado(nr_sequencia,'CS'),1,10) = 'A';
				
				-- Deve ser adicionado mais 1 para que seja considerado o titular.
				qt_pessoas_w 	:= qt_pessoas_w + 1;
			else
				select	count(1)
				into	qt_pessoas_w
				from	pls_segurado
				where	nr_seq_titular	= nr_seq_titular_w
				and	ie_situacao_atend	= 'A'
				and	substr(pls_obter_dados_segurado(nr_sequencia,'CS'),1,10) = 'A';
				
				-- Deve ser adicionado mais 1 para que seja considerado o titular.
				qt_pessoas_w 	:= qt_pessoas_w + 1;
			end if;
			
			qt_permitida_w	:=	qt_pessoas_w * qt_permitida_w;
		else

			qt_permitida_w := dados_limitacao_w.qt_permitida;
		
		end if;
		
		-- Zerar o totalizador para executar a soma dos materiais que foram utilizados pelo benefici�rio.
		i := 0;
		qt_mat_util_total_w := 0;
		ds_itens_util_w := '';
		pls_ocor_imp_pck.limpar_nested_tables(	dados_tb_selecao_w.nr_seq_selecao, 
							dados_tb_selecao_w.ie_valido, 
							dados_tb_selecao_w.ds_observacao);
							
		for	r_C04_w	in C04 (	nr_id_transacao_p, r_C01_w.nr_seq_segurado_conv, 
						dados_limitacao_w.nr_seq_tipo_limitacao) loop
						
			qt_mat_util_total_w := qt_mat_util_total_w + r_C04_w.qt_item;
			
			ds_itens_util_w	:= substr(ds_itens_util_w || pls_util_pck.enter_w  ||  wheb_mensagem_pck.get_texto(791834/*Conta:*/) || ' ' || r_C04_w.nr_seq_conta || ' | ' || 
						wheb_mensagem_pck.get_texto(791832/*Material:*/) || ' ' || r_C04_w.nr_seq_conta_mat || ' | ' || 
						wheb_mensagem_pck.get_texto(791833/*Quantidade:*/) || ' ' || r_C04_w.qt_item, 1, 4000);
						
			
			-- Ir� gerar a ocorr�ncia apenas quando a quantidade utilizada ultrapassar a permitida e quando o material estiver na tabela de 
			-- sele��o.			
			if	(qt_mat_util_total_w > qt_permitida_w) and
				(r_C04_w.ie_selecao = 'S') then
				
				dados_tb_selecao_w.nr_seq_selecao(i)	:= r_C04_w.nr_seq_selecao;
				dados_tb_selecao_w.ie_valido(i)		:= 'S';
				dados_tb_selecao_w.ds_observacao(i)	:= substr(	wheb_mensagem_pck.get_texto(791835) || ' ' || pls_util_pck.enter_w ||
											wheb_mensagem_pck.get_texto(791826) || ' ' || qt_permitida_w || ' ' ||
											wheb_mensagem_pck.get_texto(791836) || ' ' || dados_limitacao_w.qt_meses_intervalo || ' ' || 
											obter_valor_dominio(4171, dados_limitacao_w.ie_tipo_periodo)|| pls_util_pck.enter_w || pls_util_pck.enter_w ||	
											wheb_mensagem_pck.get_texto(791837) || ' ' || pls_util_pck.enter_w ||
											ds_itens_util_w, 1, 4000);
	
				/*
				791835 - O material ultrapassa a quantidade permitida pelas limita��es contratuais do benefici�rio.
				791826 - Este benefici�rio possui libera��o para realizar apenas
				791836 -  material(is) neste mesmo contexto em um per�odo de
				791837 - Materiais utilizados no per�odo:
				*/
				
				-- Gravar na tabela de sele��o as contas selecionadas 
				if	(dados_tb_selecao_w.nr_seq_selecao.count >= pls_util_cta_pck.qt_registro_transacao_w) then
					
					pls_ocor_imp_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao,
											dados_tb_selecao_w.ie_valido,
											dados_tb_selecao_w.ds_observacao,
											nr_id_transacao_p,
											'SEQ');		
											
					i := 0;
					pls_ocor_imp_pck.limpar_nested_tables(	dados_tb_selecao_w.nr_seq_selecao, 
										dados_tb_selecao_w.ie_valido, 
										dados_tb_selecao_w.ds_observacao);
				else
					i := i + 1;
				end if;
			end if;
		end loop;
		
		if	(dados_tb_selecao_w.nr_seq_selecao.count >= 0) then
					
			pls_ocor_imp_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao,
									dados_tb_selecao_w.ie_valido,
									dados_tb_selecao_w.ds_observacao,
									nr_id_transacao_p,
									'SEQ');		
									
			pls_ocor_imp_pck.limpar_nested_tables(	dados_tb_selecao_w.nr_seq_selecao, 
								dados_tb_selecao_w.ie_valido, 
								dados_tb_selecao_w.ds_observacao);
		end if;
		
		pls_ocor_imp_pck.gerencia_tabela_aux_val_limit( 'T', 'P', nr_id_transacao_p, dados_tabela_aux_w, nm_usuario_p);
	end loop;
end loop;

end pls_oc_cta_val_27_mat_imp;
/