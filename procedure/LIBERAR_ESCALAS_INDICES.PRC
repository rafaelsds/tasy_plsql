create or replace
procedure liberar_escalas_indices(	nm_tabela_p		varchar2,
				nm_chave_p		varchar2,
				qt_chave_p		number,
				nr_atendimento_p	number,
				nr_seq_evento_p		number,
				nr_escala_p		varchar2,
				nm_usuario_p	varchar2) is 

ds_sep_bv_w		varchar2(10);
ds_comando_w	varchar2(255);
dt_avaliacao_w	date;
				
begin

select 	obter_separador_bv
into	ds_sep_bv_w
from 	dual;

if	((upper(nm_tabela_p)	= 'ESCALA_ASG_PPP') or
	(upper(nm_tabela_p)	= 'ESCALA_STRONG_KIDS') or 
	(upper(nm_tabela_p)	= 'AVAL_NUTRIC_SUBJETIVA')) then
	begin
	ds_comando_w	:=	' update ' || nm_tabela_p || 
				' set dt_liberacao = sysdate ' ||
				' where ' || nm_chave_p || ' = :qt_valor ' ||
				' and dt_liberacao is null ';				
				
	exec_sql_dinamico_bv(obter_desc_expressao(726973), ds_comando_w, 'qt_valor='|| to_char(qt_chave_p) || ds_sep_bv_w);	
	end;
else
	begin
	ds_comando_w	:=	' update ' || nm_tabela_p || 
				' set dt_liberacao = sysdate ' ||
				' where ' || nm_chave_p || ' = :qt_valor ' ||
				' and nm_usuario = :nm_usuario ' ||
				' and dt_liberacao is null ';
				
	exec_sql_dinamico_bv(obter_desc_expressao(726973), ds_comando_w, 'qt_valor='|| to_char(qt_chave_p) || ds_sep_bv_w || 
								  'nm_usuario='|| nm_usuario_p || ds_sep_bv_w);
	end;
end if;

gerar_lancamento_automatico (nr_atendimento_p,0,nr_seq_evento_p,nm_usuario_p,0,nr_escala_p,null,null,null,null);
	
ds_comando_w	:=	'Select max(dt_avaliacao) ' ||
			' from ' || nm_tabela_p || 
			' where ' || nm_chave_p || ' = :qt_valor';				
Obter_valor_Dinamico_date_bv(ds_comando_w, 'qt_valor='||qt_chave_p,dt_avaliacao_w);

wl_gerar_finalizar_tarefa('S','F',nr_atendimento_p,null,nm_usuario_p,null,'S',null,null,null,null,null,null,nr_escala_p,null,null,null,null,dt_avaliacao_w, qt_chave_p);

commit;	

if	(upper(nm_tabela_p)	= 'ESCALA_EIF') then
	gerar_evolucao_score_flex(qt_chave_p,nm_usuario_p);
end if;

end liberar_escalas_indices;
/
