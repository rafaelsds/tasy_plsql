create or replace
procedure ptu_imp_resp_serv_ordem_v60(	cd_transacao_p		ptu_resposta_req_ord_serv.cd_transacao%type,
					ie_tipo_tabela_p	ptu_resposta_req_servico.ie_tipo_tabela%type,
					nr_seq_item_p		ptu_resposta_req_servico.nr_seq_item%type,
					cd_servico_p		ptu_resposta_req_servico.cd_servico%type,
					qt_autorizada_p		ptu_resposta_req_servico.qt_servico_aut%type,
					ie_status_req_p		ptu_resposta_req_servico.ie_status_requisicao%type,
					cd_mens_erro_1_p	ptu_inconsistencia.cd_inconsistencia%type,
					cd_mens_erro_2_p	ptu_inconsistencia.cd_inconsistencia%type,
					cd_mens_erro_3_p	ptu_inconsistencia.cd_inconsistencia%type,
					cd_mens_erro_4_p	ptu_inconsistencia.cd_inconsistencia%type,
					cd_mens_erro_5_p	ptu_inconsistencia.cd_inconsistencia%type,
					ds_mens_erro_p		ptu_intercambio_consist.ds_observacao%type,
					nr_seq_resposta_p	ptu_resposta_req_ord_serv.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		Usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importar dados do bloco de servi�os da resposta de ordem de servi�o 00807 - Resposta de Ordem de Servi�o

Rotina utilizada nas transa��es ptu via scs homologadas com a unimed brasil.
quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Performance
---------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_autorizada_ww	ptu_resposta_req_servico.qt_servico_aut%type;
nr_seq_resp_req_ser_w	ptu_resposta_req_servico.nr_sequencia%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
begin

if	(cd_estabelecimento_p is null) then
	cd_estabelecimento_w := ptu_val_scs_ws_pck.ptu_obter_estab_padrao;
else
	cd_estabelecimento_w := cd_estabelecimento_p;
end if;

begin
	qt_autorizada_ww  := to_number(ptu_obter_qtd_env_itens_scs(null, qt_autorizada_p, 'R'));
exception
when others then
	qt_autorizada_ww   := to_number(replace(ptu_obter_qtd_env_itens_scs(null, qt_autorizada_p, 'R'),',','.'));
end;

insert	into ptu_resposta_req_servico
	(nr_sequencia, nr_seq_resp_req_ord, cd_servico,
	qt_servico_aut, ie_status_requisicao, ie_tipo_tabela,
	nm_usuario, dt_atualizacao, nm_usuario_nrec,
	dt_atualizacao_nrec, nr_seq_item)
values	(ptu_resposta_req_servico_seq.NextVal, nr_seq_resposta_p, cd_servico_p,
	qt_autorizada_ww, ie_status_req_p, ie_tipo_tabela_p,
	nm_usuario_p, sysdate, nm_usuario_p,
	sysdate, nr_seq_item_p) returning nr_sequencia into nr_seq_resp_req_ser_w;

if	(ie_tipo_tabela_p	in ('0','1','4')) then
	if	(cd_mens_erro_1_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_1_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
	end if;

	if	(cd_mens_erro_2_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_2_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
	end if;

	if	(cd_mens_erro_3_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_3_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
	end if;

	if	(cd_mens_erro_4_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_4_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
	end if;

	if	(cd_mens_erro_5_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_5_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
	end if;
elsif	(ie_tipo_tabela_p	in ('2','3')) then
	if	(cd_mens_erro_1_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_1_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
	end if;
	
	if	(cd_mens_erro_2_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_2_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
	end if;
	
	if	(cd_mens_erro_3_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_3_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
	end if;
	
	if	(cd_mens_erro_4_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_4_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
	end if;
	
	if	(cd_mens_erro_5_p	<> 0) then
		ptu_inserir_inconsistencia(	null, null, cd_mens_erro_5_p,ds_mens_erro_p,cd_estabelecimento_w, nr_seq_resposta_p, 'OR',
						cd_transacao_p, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
	end if;
end if;

commit;

end ptu_imp_resp_serv_ordem_v60;
/