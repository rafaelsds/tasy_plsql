create or replace
procedure pls_proposta_mig_gerar_benef
			(	nr_seq_proposta_p	Number,
				nr_seq_beneficiario_p	Number,
				nm_usuario_p		Varchar2) is

cd_pessoa_fisica_w		Varchar2(10);
nr_seq_plano_w			number(10);
nr_seq_tabela_w			number(10);
nr_seq_pagador_w		number(10);
ie_preco_w			number(10);
ds_plano_w			Varchar2(255);
nr_seq_titular_w		number(10);
cd_pessoa_fisica_tit_w		varchar2(10);
nr_seq_titular_proposta_w	number(10);
nr_seq_motivo_inclusao_w	number(10);
cd_estabelecimento_w		number(10);
qt_registros_w			number(10);
qt_benef_titular_w		number(10);
nr_contrato_existente_w		number(10);
nr_seq_titular_contrato_w	number(10);
nr_seq_plano_sca_w		number(10);
nr_seq_tabela_sca_w		number(10);
nr_seq_vendedor_canal_w		number(10);
nr_seq_vendedor_pf_w		number(10);
nr_seq_proposta_benef_w		number(10);
ds_erro_sca_w			varchar2(4000);
nr_seq_parentesco_w		number(10);
ie_tipo_parentesco_w		varchar2(3);
nr_seq_causa_rescisao_w		number(10);
nr_seq_benef_depend_w		number(10);
nr_seq_benef_prop_depen_w	number(10);
dt_inicio_proposta_w		date;

Cursor C01 is
	select	nr_seq_plano,
		nr_seq_tabela,
		nr_seq_vendedor_canal,
		nr_seq_vendedor_pf
	from	pls_sca_regra_contrato	b,
		pls_contrato		a
	where	b.nr_seq_contrato	= a.nr_sequencia
	and	a.nr_contrato		= nr_contrato_existente_w
	and	((b.nr_seq_plano_benef	= nr_seq_plano_w) or (nr_seq_plano_benef is null))
	and	dt_inicio_proposta_w between nvl(dt_inicio_vigencia,dt_inicio_proposta_w) and fim_dia(nvl(dt_fim_vigencia,dt_inicio_proposta_w));

Cursor C02 is
	select	b.nr_seq_beneficiario,
		b.nr_sequencia
	from	pls_proposta_beneficiario	b
	where	b.nr_seq_proposta	= nr_seq_proposta_p
	and	b.cd_beneficiario	in	(	select	X.CD_PESSOA_FISICA
							from	pls_segurado	x
							where	x.nr_seq_titular	= nr_seq_beneficiario_p);

begin

select	cd_pessoa_fisica,
	nr_seq_titular
into	cd_pessoa_fisica_w,
	nr_seq_titular_w
from	pls_segurado
where	nr_sequencia	= nr_seq_beneficiario_p;

select	cd_estabelecimento,
	nr_seq_contrato,
	dt_inicio_proposta
into	cd_estabelecimento_w,
	nr_contrato_existente_w,
	dt_inicio_proposta_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

begin
nr_seq_causa_rescisao_w	:= nvl(obter_valor_param_usuario(1232, 81, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w), 'N');
exception
when others then
	nr_seq_causa_rescisao_w	:= null;
end;

--Buscar o titular do benefici�rio da proposta
if	(nr_seq_titular_w is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_tit_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_titular_w;

	if	(cd_pessoa_fisica_tit_w is not null) then
		select	max(nr_sequencia)
		into	nr_seq_titular_proposta_w
		from	pls_proposta_beneficiario
		where	nr_seq_proposta 	= nr_seq_proposta_p
		and	cd_beneficiario 	= cd_pessoa_fisica_tit_w;
	end if;
end if;

/*aaschlote 01/10/2011 OS - 368210*/
if	(nr_seq_titular_proposta_w is null) and
	(nr_contrato_existente_w is not null) then

	select	count(*)
	into	qt_benef_titular_w
	from	pls_segurado		b,
		pls_contrato		a
	where	b.nr_seq_contrato	= a.nr_sequencia
	and	a.nr_contrato		= nr_contrato_existente_w
	and 	a.cd_pf_estipulante is not null /* acstapassoli 23/12/2014 - validar somente para contratos PF - OS - 827423 */
	and	b.nr_seq_titular is null
	and	b.dt_liberacao is not null;

	if	(qt_benef_titular_w = 1) then
		select	max(b.nr_sequencia)
		into	nr_seq_titular_contrato_w
		from	pls_segurado		b,
			pls_contrato		a
		where	b.nr_seq_contrato	= a.nr_sequencia
		and	a.nr_contrato		= nr_contrato_existente_w		
		and	b.nr_seq_titular is null
		and	b.dt_liberacao is not null;
	end if;
end if;

/*aaschlote 27/02/2012 OS - 416505*/
if	(nr_seq_titular_proposta_w is not null) or
	(nr_seq_titular_contrato_w is not null) then
	select	max(nr_seq_parentesco)
	into	nr_seq_parentesco_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_beneficiario_p;

	if	(nr_seq_parentesco_w is not null) then
		select	ie_tipo_parentesco
		into	ie_tipo_parentesco_w
		from	grau_parentesco
		where	nr_sequencia	= nr_seq_parentesco_w;
	end if;
end if;

/*OS 295470 - Diego - Se haver somente um plano ou uma tabela  ou pagador inseri-los com o beneficiario*/

--Se houver somente um  pagador � inserido este com o benefi. Sen�o entra no excpetion.
begin
select	nr_sequencia
into	nr_seq_pagador_w
from	pls_proposta_pagador
where	nr_seq_proposta = nr_seq_proposta_p;
exception
when others then
	nr_seq_pagador_w := null;
end;

--Se houver somente um  plano � inserido este com o benefi. Sen�o entra no excpetion.
begin
select	a.nr_seq_plano,
	b.ie_preco,
	b.ds_plano
into	nr_seq_plano_w,
	ie_preco_w,
	ds_plano_w
from	pls_proposta_plano a,
	pls_plano b
where	a.nr_seq_plano  = b.nr_sequencia
and	a.nr_seq_proposta = nr_seq_proposta_p
group by a.nr_seq_plano,
	 b.ie_preco,
	 b.ds_plano;
exception
when others then
	nr_seq_plano_w	:= null;
	ie_preco_w	:= null;
end;

--Realizado a verifica��o separadamente pois existindo somente um produto mas duas ou mais tabelas o produto ainda pode ser inserido
if (nvl(nr_seq_plano_w,0) <> 0) then
	begin
	select	nr_seq_tabela
	into	nr_seq_tabela_w
	from	pls_proposta_plano
	where	nr_seq_proposta = nr_seq_proposta_p
	group by nr_seq_tabela;
	exception
	when others then
		nr_seq_tabela_w	:= null;
	end;
end if;

select	nvl(max(nr_seq_motivo_migracao), max(nr_seq_motivo_inclusao))
into	nr_seq_motivo_inclusao_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_w;

--Se o produto for preestabelecido e n�o houver tabela n�o � inserido
if	(nvl(ie_preco_w,0) = 1) and
	(nvl(nr_seq_tabela_w,0) = 0) then
	WHEB_MENSAGEM_PCK.exibir_mensagem_abort(176934,'DS_PLANO='||ds_plano_w);
end if;

select	count(*)
into	qt_registros_w
from	pls_proposta_beneficiario
where	nr_seq_proposta	= nr_seq_proposta_p
and	cd_beneficiario	= cd_pessoa_fisica_w;

if	(qt_registros_w = 0) then

	select	pls_proposta_beneficiario_seq.nextval
	into	nr_seq_proposta_benef_w
	from	dual;

	
	insert into pls_proposta_beneficiario
		(nr_sequencia, nr_seq_proposta, cd_beneficiario,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
		nm_usuario_nrec, nr_seq_beneficiario, ie_nascido_plano,
		nr_seq_plano, nr_seq_tabela, nr_seq_pagador,nr_seq_titular,ie_taxa_inscricao,
		nr_seq_motivo_inclusao,nr_seq_titular_contrato,nr_seq_parentesco,
		ie_tipo_parentesco,nr_seq_causa_rescisao, ie_copiar_sca_plano)
	values(	nr_seq_proposta_benef_w, nr_seq_proposta_p, cd_pessoa_fisica_w,
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, nr_seq_beneficiario_p, 'N',
		nr_seq_plano_w, nr_seq_tabela_w, nr_seq_pagador_w,nr_seq_titular_proposta_w,'S',
		nr_seq_motivo_inclusao_w,nr_seq_titular_contrato_w,nr_seq_parentesco_w,
		ie_tipo_parentesco_w,nr_seq_causa_rescisao_w, 'S');

	open C01;
	loop
	fetch C01 into
		nr_seq_plano_sca_w,
		nr_seq_tabela_sca_w,
		nr_seq_vendedor_canal_w,
		nr_seq_vendedor_pf_w;
	exit when C01%notfound;
		begin

		pls_consistir_sca_proposta(nr_seq_proposta_benef_w,nr_seq_plano_sca_w,nr_seq_tabela_sca_w,cd_estabelecimento_w,ds_erro_sca_w,nm_usuario_p);

		if	(ds_erro_sca_w is null) then
			insert into pls_sca_vinculo
				(	nr_sequencia,dt_atualizacao, nm_usuario, dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_benef_proposta,nr_seq_plano,nr_seq_tabela,nr_seq_vendedor_canal,nr_seq_vendedor_pf)
			values	(	pls_sca_vinculo_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p,
					nr_seq_proposta_benef_w,nr_seq_plano_sca_w,nr_seq_tabela_sca_w,nr_seq_vendedor_canal_w,nr_seq_vendedor_pf_w);
		end if;
		end;
	end loop;
	close C01;

	/*aaschlote 30/11/2012 - OS - 523648 - Caso os dependentes j� est�o na proposta, ent�o atualiza seu titular*/
	if	(nr_seq_titular_w is null) then
		open C02;
		loop
		fetch C02 into
			nr_seq_benef_depend_w,
			nr_seq_benef_prop_depen_w;
		exit when C02%notfound;
			begin

			select	max(nr_seq_parentesco)
			into	nr_seq_parentesco_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_benef_depend_w;

			if	(nr_seq_parentesco_w is not null) then
				select	ie_tipo_parentesco
				into	ie_tipo_parentesco_w
				from	grau_parentesco
				where	nr_sequencia	= nr_seq_parentesco_w;
			end if;

			update	pls_proposta_beneficiario
			set	nr_seq_titular		= nr_seq_proposta_benef_w,
				nr_seq_parentesco	= nr_seq_parentesco_w,
				ie_tipo_parentesco	= ie_tipo_parentesco_w
			where	nr_sequencia		= nr_seq_benef_prop_depen_w;

			end;
		end loop;
		close C02;
	end if;
end if;

commit;

end pls_proposta_mig_gerar_benef;
/