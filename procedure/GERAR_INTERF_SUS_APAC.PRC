create or replace 
procedure gerar_interf_sus_apac(	nr_seq_protocolo_p		number,
				nm_usuario_p		varchar2) is


nr_sequencia_w		number(10);
nr_interno_conta_w		number(10);
nr_atendimento_w		number(10);
nr_apac_w		number(13);
nr_seq_protocolo_w	number(10);
dt_emissao_w		date;
dt_inicio_validade_w	date;
dt_fim_validade_w		date;
ie_tipo_apac_w		number(2);
nr_cpf_medico_w		varchar2(11);
cd_motivo_cobranca_w	number(2);
dt_ocorrencia_w		date;
cd_cep_w		varchar2(15);
nm_paciente_w		varchar2(60);
nm_mae_pac_w		varchar2(60);
ds_endereco_w		varchar2(100);
ds_complemento_w		varchar2(2000);
dt_nascimento_w		date;
ie_sexo_w		varchar2(1);
nm_medico_w		varchar2(60);
dt_competencia_w		date;
nr_cns_medico_w		number(15);
uf_nasc_w		varchar2(3);
cd_municipio_ibge_w	varchar2(6);
nr_cpf_w			varchar2(11);
nr_endereco_w		varchar2(5);
cd_cid_causa_assoc_w	varchar2(4);
dt_solicitacao_w		date;
dt_autorizacao_w		date;
cd_carater_internacao_w	varchar2(2);
nr_apac_anterior_w		number(13);
cd_raca_cor_sus_w	number(2);
cd_etnia_w		varchar2(4);
nr_prontuario_w		number(10);
cd_orgao_emissor_apac_w	varchar2(10);
nr_cns_diretor_w		number(15);
nr_cpf_diretor_w		varchar2(11);
nm_diretor_w		varchar2(60);
nr_cns_w			number(15);

qt_proc_w		number(5) := 0;
cd_procedimento_apac_w	number(15);
cd_procedimento1_w	number(15);
qt_procedimento1_w	number(9,3);
cd_cgc_prestador1_w	varchar2(14);
nr_nf_prestador1_w		number(10);
cd_procedimento2_w	number(15);
qt_procedimento2_w	number(9,3);
cd_cgc_prestador2_w	varchar2(14);
nr_nf_prestador2_w		number(10);
cd_procedimento3_w	number(15);
qt_procedimento3_w	number(9,3);
cd_cgc_prestador3_w	varchar2(14);
nr_nf_prestador3_w		number(10);
cd_procedimento4_w	number(15);
qt_procedimento4_w	number(9,3);
cd_cgc_prestador4_w	varchar2(14);
nr_nf_prestador4_w		number(10);
cd_procedimento5_w	number(15);
qt_procedimento5_w	number(9,3);
cd_cgc_prestador5_w	varchar2(14);
nr_nf_prestador5_w		number(10);
cd_procedimento6_w	number(15);
qt_procedimento6_w	number(9,3);
cd_cgc_prestador6_w	varchar2(14);
nr_nf_prestador6_w		number(10);
cd_procedimento7_w	number(15);
qt_procedimento7_w	number(9,3);
cd_cgc_prestador7_w	varchar2(14);
nr_nf_prestador7_w		number(10);
cd_procedimento8_w	number(15);
qt_procedimento8_w	number(9,3);
cd_cgc_prestador8_w	varchar2(14);
nr_nf_prestador8_w		number(10);
cd_procedimento9_w	number(15);
qt_procedimento9_w	number(9,3);
cd_cgc_prestador9_w	varchar2(14);
nr_nf_prestador9_w		number(10);
cd_procedimento10_w	number(15);
qt_procedimento10_w	number(9,3);
cd_cgc_prestador10_w	varchar2(14);
nr_nf_prestador10_w	number(10);
cd_procedimento_w	number(15);
qt_procedimento_w		number(9,3);
cd_cgc_prestador_w	varchar2(14);
nr_nf_prestador_w		number(10);
cd_cbo1_w		varchar2(10);
cd_cbo2_w		varchar2(6);
cd_cbo3_w		varchar2(6);
cd_cbo4_w		varchar2(6);
cd_cbo5_w		varchar2(6);
cd_cbo6_w		varchar2(6);
cd_cbo7_w		varchar2(6);
cd_cbo8_w		varchar2(6);
cd_cbo9_w		varchar2(6);
cd_cbo10_w		varchar2(6);
cd_cidpri1_w		varchar2(4);
cd_cidpri2_w		varchar2(4);
cd_cidpri3_w		varchar2(4);
cd_cidpri4_w		varchar2(4);
cd_cidpri5_w		varchar2(4);
cd_cidpri6_w		varchar2(4);
cd_cidpri7_w		varchar2(4);
cd_cidpri8_w		varchar2(4);
cd_cidpri9_w		varchar2(4);
cd_cidpri10_w		varchar2(4);
cd_cidsec1_w		varchar2(4);
cd_cidsec2_w		varchar2(4);
cd_cidsec3_w		varchar2(4);
cd_cidsec4_w		varchar2(4);
cd_cidsec5_w		varchar2(4);
cd_cidsec6_w		varchar2(4);
cd_cidsec7_w		varchar2(4);
cd_cidsec8_w		varchar2(4);
cd_cidsec9_w		varchar2(4);
cd_cidsec10_w		varchar2(4);

cd_unidade_ibge_w	varchar2(2);
cd_unidade_apac_w	varchar2(7);
cd_unidade_solic_apac_w	varchar2(7);
nm_responsavel_w		varchar2(60);
cd_estabelecimento_w	number(4);
ds_varia_w		varchar2(141);
cd_medico_executor_w	varchar2(10);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_autorizador_w	varchar2(10);
cd_medico_responsavel_w	varchar2(10);
cd_cbo_w		varchar2(6);
ie_exporta_resp_w		varchar2(1);
nr_seq_apac_w		number(10);
nr_ordem_w		number(5);
ie_continuacao_w		number(1);
ie_tipo_laudo_apac_w	varchar2(2);
ie_tpcc_w		varchar2(3);
cd_exec_exp_w		varchar2(10);
cd_cbo_exec_exp_w	varchar2(6);
qt_registro_w		number(5) := 0;
nr_regra_exp_par_w	number(5) := 0;
cd_cgc_prestador_exp_w	varchar2(14);
ie_exp_cnes_prest_w	varchar2(15) := 'N';
cd_prest_proc_apac_w	varchar2(14);
cd_cid_principal_w		varchar2(4);
cd_cid_secundario_w	varchar2(4);
cd_grupo_w		number(2);
ie_exc_dt_sem_cid_w	varchar2(15) := 'N';
cd_estab_usuario_w	number(4);
ie_dados_apac_cont_w	varchar2(15) := 'N';
cd_servico_w		varchar2(3);
cd_serv_classif_w	varchar2(3);
nr_seq_sus_equipe_w	number(10);
cd_equipe1_w		varchar2(12);
cd_equipe2_w		varchar2(12);
cd_equipe3_w		varchar2(12);
cd_equipe4_w		varchar2(12);
cd_equipe5_w		varchar2(12);
cd_equipe6_w		varchar2(12);
cd_equipe7_w		varchar2(12);
cd_equipe8_w		varchar2(12);
cd_equipe9_w		varchar2(12);
cd_equipe10_w		varchar2(12);
tp_logradouro_w		varchar2(3);
ds_bairro_w		varchar2(30);
nr_ddd_pac_w		varchar2(2);
nr_telefone_pac_w	varchar2(9);
ds_email_w		varchar2(40);
nr_cns_med_exec_w	varchar2(15);
ie_considerar_cns_ant_w	parametro_faturamento.ie_considerar_cns_ant%type;
nr_cartao_nac_sus_ant_w cnes_profissional.nr_cartao_nac_sus_ant%type;
cd_localidade_w		cep_localidade.cd_localidade%type;
ds_desc_exp_w           varchar2(1);

/* Obter dados do detalhe */
cursor c01 is
	select	a.nr_sequencia,
		a.cd_medico_responsavel,
		d.cd_pessoa_fisica cd_pf,
		a.cd_medico_autorizador,
		b.nr_seq_protocolo,
		b.nr_atendimento,
		b.nr_interno_conta,
		a.nr_apac,
		a.dt_emissao,
		a.dt_inicio_validade,
		a.dt_fim_validade,
		a.ie_tipo_apac,
		d.nr_cpf,
		substr(obter_nome_pf_pj(c.cd_pessoa_fisica, null),1,60),
		d.cd_nacionalidade,
		nvl(substr(obter_compl_pf(c.cd_pessoa_fisica, 5,'N'),1,60),substr(obter_nome_pf(d.cd_pessoa_mae),1,60)),
		substr(obter_compl_pf(c.cd_pessoa_fisica, 1,'EN'),1,80),
		substr(nvl(obter_compl_pf(c.cd_pessoa_fisica, 1,'NR'),'S/N'),1,20),
		substr(obter_compl_pf(c.cd_pessoa_fisica, 1,'CO'),1,80),
		substr(obter_compl_pf(c.cd_pessoa_fisica, 1,'CEP'),1,15),
		substr(obter_compl_pf(c.cd_pessoa_fisica, 1,'CDM'),1,6),
		d.dt_nascimento,
		d.ie_sexo,
		substr(obter_cpf_pessoa_fisica(a.cd_medico_responsavel),1,11),
		substr(obter_nome_pf_pj(a.cd_medico_responsavel,null),1,60),
		a.cd_motivo_cobranca,
		a.dt_ocorrencia,
		a.dt_competencia,
		substr(obter_dados_pf(a.cd_medico_responsavel,'CNS'),1,15),
		a.cd_cid_causa_assoc,
		a.dt_solicitacao,
		a.dt_autorizacao,
		a.cd_carater_internacao,
		a.nr_apac_anterior,
		substr(sus_obter_cor_pele(d.cd_pessoa_fisica, 'C'),1,2),
		decode(sus_obter_cor_pele(d.cd_pessoa_fisica, 'C'),'5',sus_obter_etnia(d.cd_pessoa_fisica, 'C'),'    '),
		substr(obter_dados_pf(d.cd_pessoa_fisica,'CNS'),1,15),
nvl(decode(ie_exporta_resp_w, 'C',substr(obter_compl_pf(c.cd_pessoa_fisica, 3,'N'),1,60), substr(obter_nome_pf(c.cd_pessoa_responsavel),1,60)), nvl(substr(obter_compl_pf(c.cd_pessoa_fisica, 5,'N'),1,60),substr(obter_compl_pf(c.cd_pessoa_fisica, 4,'N'),1,60))),
		substr(obter_dados_pf(a.cd_medico_autorizador,'CNS'),1,15),
		substr(obter_cpf_pessoa_fisica(a.cd_medico_autorizador),1,11),
		substr(obter_nome_pf_pj(a.cd_medico_autorizador, null),1,60),
		a.cd_procedimento,
		sus_obter_tipo_laudo_apac_proc(a.cd_procedimento, a.ie_origem_proced),
		a.cd_cid_principal,
		a.cd_cid_secundario,
		lpad(sus_obter_serv_classif_apac(a.nr_sequencia,'CS'),3,'0') cd_servico,
		lpad(sus_obter_serv_classif_apac(a.nr_sequencia,'CC'),3,'0') cd_serv_classif,
		substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'TLS'),1,3) tp_logradouro,
		replace(elimina_caractere_especial(upper(substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'B'),1,30))),ds_desc_exp_w,'C') ds_bairro,
		substr(sus_obter_telefone_export(c.cd_pessoa_fisica),1,2) nr_ddd,
		substr(sus_obter_telefone_export(c.cd_pessoa_fisica),3,9) nr_telefone,
		substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'M'),1,40) ds_email
	from	pessoa_fisica d,
		atendimento_paciente c,
		conta_paciente b,
		sus_apac_unif a
	where	a.nr_interno_conta	= b.nr_interno_conta
	and	b.nr_atendimento	= c.nr_atendimento
	and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	order by a.nr_sequencia;

cursor c02 is
	select	cd_procedimento,
		sum(qt_procedimento),
		cd_cgc_prestador,
		nr_nf_prestador,
		nvl(sus_obter_regra_exp_exec (cd_procedimento,ie_origem_proced,nvl(cd_medico_executor,cd_pessoa_fisica),
		nvl(cd_cbo, sus_obter_cbo_medico(nvl(cd_medico_executor,cd_pessoa_fisica), cd_procedimento, sysdate, 0 )),
		cd_estabelecimento_w,'N','N','S','C'),nvl(cd_cbo, sus_obter_cbo_medico(nvl(cd_medico_executor,cd_pessoa_fisica), cd_procedimento, sysdate, 0 ))),
		sus_ordenar_proc_apac(nr_interno_conta_w, cd_procedimento) nr_ordem,
		nr_seq_sus_equipe
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_w
	and	ie_origem_proced	= 7
	and	cd_motivo_exc_conta is null
	group by cd_procedimento,
		cd_cgc_prestador,
		nr_nf_prestador,
		nvl(sus_obter_regra_exp_exec (cd_procedimento,ie_origem_proced,nvl(cd_medico_executor,cd_pessoa_fisica),
		nvl(cd_cbo, sus_obter_cbo_medico(nvl(cd_medico_executor,cd_pessoa_fisica), cd_procedimento, sysdate, 0 )),
		cd_estabelecimento_w,'N','N','S','C'),nvl(cd_cbo, sus_obter_cbo_medico(nvl(cd_medico_executor,cd_pessoa_fisica), cd_procedimento, sysdate, 0 ))),
		nr_seq_sus_equipe
	order by nr_ordem;

begin

begin
cd_estab_usuario_w := wheb_usuario_pck.get_cd_estabelecimento;
exception
when others then
	cd_estab_usuario_w := 0;
end;

nr_regra_exp_par_w	:= nvl(to_number(obter_valor_param_usuario(1124,73,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w)),0);
ie_exc_dt_sem_cid_w	:= nvl(obter_valor_param_usuario(1124,95,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');
ie_dados_apac_cont_w	:= nvl(obter_valor_param_usuario(1124,96,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');
ds_desc_exp_w           := upper(expressao_pck.obter_desc_expressao(960618));

/* Limpar tabela */
delete	from w_susapac_interf
where	nr_seq_protocolo	= nr_seq_protocolo_p;

begin
if	(nvl(nr_regra_exp_par_w,0) <> 0) then
	begin
	select	b.cd_orgao_emissor_apac,
		b.cd_estabelecimento,
		b.cd_unidade_apac,
		nvl(b.cd_unidade_solic_apac,''),
		nvl(b.ie_exporta_resp,'C'),
		nvl(ie_exp_cnes_prest_apac,'N')
	into	cd_orgao_emissor_apac_w,
		cd_estabelecimento_w,
		cd_unidade_apac_w,
		cd_unidade_solic_apac_w,
		ie_exporta_resp_w,
		ie_exp_cnes_prest_w
	from	sus_regra_exp_param_apac	b,
		protocolo_convenio		a
	where	a.nr_seq_protocolo		= nr_seq_protocolo_p
	and	a.cd_estabelecimento	= b.cd_estabelecimento
	and	b.nr_sequencia		= nr_regra_exp_par_w;
	end;
else
	begin
	select	b.cd_orgao_emissor_apac,
		b.cd_estabelecimento,
		b.cd_unidade_apac,
		nvl(b.cd_unidade_solic_apac,''),
		nvl(b.ie_exporta_resp,'C'),
		nvl(ie_exp_cnes_prest_apac,'N')
	into	cd_orgao_emissor_apac_w,
		cd_estabelecimento_w,
		cd_unidade_apac_w,
		cd_unidade_solic_apac_w,
		ie_exporta_resp_w,
		ie_exp_cnes_prest_w
	from	sus_parametros_apac	b,
		protocolo_convenio		a
	where	a.nr_seq_protocolo		= nr_seq_protocolo_p
	and	a.cd_estabelecimento	= b.cd_estabelecimento;
	end;
end if;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(174189);
	/*'Dados dos parametros APAC nao encontrado'*/
end;

begin
select	lpad(c.cd_localidade,8,'0')
into	cd_localidade_w
from	estabelecimento c
where	c.cd_estabelecimento	= cd_estabelecimento_w;
exception
when others then
	cd_localidade_w := '0';
end;

begin
select 	a.cd_unidade_ibge
into	cd_unidade_ibge_w
from   	cep_localidade b,
	unidade_federacao a
where 	b.cd_localidade		= cd_localidade_w
and	b.cd_unidade_federacao 	= a.cd_unidade_federacao;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(174191);
	/*'Falta a informacao da localidade do Hospital. ' || chr(13) ||
				'Verificar a funcao Empresas/Estabelecimento/Conta/CC, pasta estabelecimento, campo Localidade.'*/
end;

begin
select	nvl(ie_considerar_cns_ant, 'N')
into	ie_considerar_cns_ant_w
from	parametro_faturamento
where	cd_estabelecimento=wheb_usuario_pck.get_cd_estabelecimento;
exception
	when others then
	ie_considerar_cns_ant_w :='N';
end;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_medico_responsavel_w,
	cd_pessoa_fisica_w,
	cd_medico_autorizador_w,
	nr_seq_protocolo_w,
	nr_atendimento_w,
	nr_interno_conta_w,
	nr_apac_w,
	dt_emissao_w,
	dt_inicio_validade_w,
	dt_fim_validade_w,
	ie_tipo_apac_w,
	nr_cpf_w,
	nm_paciente_w,
	uf_nasc_w,
	nm_mae_pac_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	cd_cep_w,
	cd_municipio_ibge_w,
	dt_nascimento_w,
	ie_sexo_w,
	nr_cpf_medico_w,
	nm_medico_w,
	cd_motivo_cobranca_w,
	dt_ocorrencia_w,
	dt_competencia_w,
	nr_cns_medico_w,
	cd_cid_causa_assoc_w,
	dt_solicitacao_w,
	dt_autorizacao_w,
	cd_carater_internacao_w,
	nr_apac_anterior_w,
	cd_raca_cor_sus_w,
	cd_etnia_w,
	nr_cns_w,
	nm_responsavel_w,
	nr_cns_diretor_w,
	nr_cpf_diretor_w,
	nm_diretor_w,
	cd_procedimento_apac_w,
	ie_tipo_laudo_apac_w,
	cd_cid_principal_w,
	cd_cid_secundario_w,
	cd_servico_w,
	cd_serv_classif_w,
	tp_logradouro_w,
	ds_bairro_w,
	nr_ddd_pac_w,
	nr_telefone_pac_w,
	ds_email_w;
exit when c01%notfound;
	begin
	qt_proc_w	:= 0;
	cd_procedimento1_w	:= null;
	cd_procedimento2_w	:= null;
	cd_procedimento3_w	:= null;
	cd_procedimento4_w	:= null;
	cd_procedimento5_w	:= null;
	cd_procedimento6_w	:= null;
	cd_procedimento7_w	:= null;
	cd_procedimento8_w	:= null;
	cd_procedimento9_w	:= null;
	cd_procedimento10_w	:= null;
	qt_procedimento1_w	:= null;
	qt_procedimento2_w	:= null;
	qt_procedimento3_w	:= null;
	qt_procedimento4_w	:= null;
	qt_procedimento5_w	:= null;
	qt_procedimento6_w	:= null;
	qt_procedimento7_w	:= null;
	qt_procedimento8_w	:= null;
	qt_procedimento9_w	:= null;
	qt_procedimento10_w	:= null;
	cd_cgc_prestador1_w	:= null;
	cd_cgc_prestador2_w	:= null;
	cd_cgc_prestador3_w	:= null;
	cd_cgc_prestador4_w	:= null;
	cd_cgc_prestador5_w	:= null;
	cd_cgc_prestador6_w	:= null;
	cd_cgc_prestador7_w	:= null;
	cd_cgc_prestador8_w	:= null;
	cd_cgc_prestador9_w	:= null;
	cd_cgc_prestador10_w	:= null;
	nr_nf_prestador1_w	:= null;
	nr_nf_prestador2_w	:= null;
	nr_nf_prestador3_w	:= null;
	nr_nf_prestador4_w	:= null;
	nr_nf_prestador5_w	:= null;
	nr_nf_prestador6_w	:= null;
	nr_nf_prestador7_w	:= null;
	nr_nf_prestador8_w	:= null;
	nr_nf_prestador9_w	:= null;
	nr_nf_prestador10_w	:= null;
	cd_cbo1_w	:= '000000';
	cd_cbo2_w	:= '000000';
	cd_cbo3_w	:= '000000';
	cd_cbo4_w	:= '000000';
	cd_cbo5_w	:= '000000';
	cd_cbo6_w	:= '000000';
	cd_cbo7_w	:= '000000';
	cd_cbo8_w	:= '000000';
	cd_cbo9_w	:= '000000';
	cd_cbo10_w	:= '000000';
	cd_cidpri1_w	:= '';
	cd_cidpri2_w	:= '';
	cd_cidpri3_w	:= '';
	cd_cidpri4_w	:= '';
	cd_cidpri5_w	:= '';
	cd_cidpri6_w	:= '';
	cd_cidpri7_w	:= '';
	cd_cidpri8_w	:= '';
	cd_cidpri9_w	:= '';
	cd_cidpri10_w	:= '';
	cd_cidsec1_w	:= '';
	cd_cidsec2_w	:= '';
	cd_cidsec3_w	:= '';
	cd_cidsec4_w	:= '';
	cd_cidsec5_w	:= '';
	cd_cidsec6_w	:= '';
	cd_cidsec7_w	:= '';
	cd_cidsec8_w	:= '';
	cd_cidsec9_w	:= '';
	cd_cidsec10_w	:= '';
	cd_equipe1_w	:= '';
	cd_equipe2_w	:= '';
	cd_equipe3_w	:= '';
	cd_equipe4_w	:= '';
	cd_equipe5_w	:= '';
	cd_equipe6_w	:= '';
	cd_equipe7_w	:= '';
	cd_equipe8_w	:= '';
	cd_equipe9_w	:= '';
	cd_equipe10_w	:= '';

	nr_prontuario_w	:= obter_prontuario_pf(cd_estab_usuario_w,cd_pessoa_fisica_w);

	begin

	select
		max(nvl(sus_obter_regra_exp_exec(cd_procedimento,ie_origem_proced,nvl(cd_medico_executor,cd_pessoa_fisica),
			nvl(cd_cbo, sus_obter_cbo_medico(nvl(cd_medico_executor,cd_pessoa_fisica), cd_procedimento, sysdate, 0 )),
			cd_estabelecimento_w,'N','N','S','M'),nvl(cd_medico_executor,cd_pessoa_fisica))),

		nvl(max(cd_cgc_prestador),'0'),
		nvl(max(substr(obter_dados_pf(nvl(sus_obter_regra_exp_exec(cd_procedimento,ie_origem_proced,nvl(cd_medico_executor,cd_pessoa_fisica),
			nvl(cd_cbo, sus_obter_cbo_medico(nvl(cd_medico_executor,cd_pessoa_fisica), cd_procedimento, sysdate, 0 )),
			cd_estabelecimento_w,'N','N','S','M'),nvl(cd_medico_executor,cd_pessoa_fisica)),'CNS'),1,15)),'')
	into	cd_medico_executor_w,
		cd_prest_proc_apac_w,
		nr_cns_med_exec_w
	from	procedimento_paciente
	where	cd_procedimento 	= cd_procedimento_apac_w
	and	ie_origem_proced	= 7
	and	nr_interno_conta	= nr_interno_conta_w;
	exception
	when others then
		nr_cns_med_exec_w := '0';
		cd_prest_proc_apac_w := '0';
		cd_medico_executor_w := '0';
	end;

	if	(nvl(ie_exp_cnes_prest_w,'N') = 'S') and
		(cd_prest_proc_apac_w <> '0') then
		cd_unidade_apac_w := nvl(substr(obter_dados_pf_pj(null,cd_prest_proc_apac_w,'CNES'),1,7),cd_unidade_apac_w);
	end if;


	if	(nvl(ie_dados_apac_cont_w,'N') = 'S') and
		(ie_tipo_apac_w = 2) and
		(nvl(nr_apac_w,0) <> 0) then
		begin
		/*Campos removidos para a OS 580334*/
		begin
		select	a.nr_cpf,
			a.nm_paciente,
			a.uf_nasc,
			a.nm_mae_pac,
			--a.ds_endereco,
			--a.nr_endereco,
			--a.ds_complemento,
			--a.cd_cep,
			a.cd_municipio_ibge,
			a.dt_nascimento,
			a.ie_sexo,
			a.nr_prontuario,
			a.cd_raca_cor_sus,
			a.cd_etnia,
			a.nr_cns
		into	nr_cpf_w,
			nm_paciente_w,
			uf_nasc_w,
			nm_mae_pac_w,
			--ds_endereco_w,
			--nr_endereco_w,
			--ds_complemento_w,
			--cd_cep_w,
			cd_municipio_ibge_w,
			dt_nascimento_w,
			ie_sexo_w,
			nr_prontuario_w,
			cd_raca_cor_sus_w,
			cd_etnia_w,
			nr_cns_w
		from	w_susapac_interf a
		where	a.nr_sequencia = (select	max(x.nr_sequencia)
					from	w_susapac_interf x
					where	x.nr_apac = nr_apac_w);
		exception
		when others then
			nr_cpf_w		:= nr_cpf_w;
			nm_paciente_w		:= nm_paciente_w;
			uf_nasc_w		:= uf_nasc_w;
			nm_mae_pac_w		:= nm_mae_pac_w;
			--ds_endereco_w		:= ds_endereco_w;
			--nr_endereco_w		:= nr_endereco_w;
			--ds_complemento_w	:= ds_complemento_w;
			--cd_cep_w		:= cd_cep_w;
			cd_municipio_ibge_w	:= cd_municipio_ibge_w;
			dt_nascimento_w		:= dt_nascimento_w;
			ie_sexo_w		:= ie_sexo_w;
			nr_prontuario_w		:= nr_prontuario_w;
			cd_raca_cor_sus_w	:= cd_raca_cor_sus_w;
			cd_etnia_w		:= cd_etnia_w;
			nr_cns_w		:= nr_cns_w;
		end;
		end;
	end if;

	if (ie_considerar_cns_ant_w = 'S') then
		begin
			begin
				select	nvl(b.nr_cartao_nac_sus_ant, ' ')
				into	nr_cartao_nac_sus_ant_w
				from 	cnes_profissional 	b
				where	b.cd_pessoa_fisica = cd_pessoa_fisica_w
				and 	rownum = 1;
				exception
			when others then
				nr_cartao_nac_sus_ant_w := ' ';
			end;

			if (nr_cartao_nac_sus_ant_w <> ' ') then
				nr_cns_w := nr_cartao_nac_sus_ant_w;
			end if;

			begin
				select	nvl(b.nr_cartao_nac_sus_ant, ' ')
				into	nr_cartao_nac_sus_ant_w
				from 	cnes_profissional 	b
				where	b.cd_pessoa_fisica = cd_medico_autorizador_w
				and 	rownum = 1;
				exception
			when others then
				nr_cartao_nac_sus_ant_w := ' ';
			end;

			if (nr_cartao_nac_sus_ant_w <> ' ') then
				nr_cns_diretor_w := nr_cartao_nac_sus_ant_w;
			end if;

			begin
				select	nvl(b.nr_cartao_nac_sus_ant, ' ')
				into	nr_cartao_nac_sus_ant_w
				from 	cnes_profissional 	b
				where	b.cd_pessoa_fisica = cd_medico_responsavel_w
				and 	rownum = 1;
				exception
			when others then
				nr_cartao_nac_sus_ant_w := ' ';
			end;

			if (nr_cartao_nac_sus_ant_w <> ' ') then
				nr_cns_medico_w := nr_cartao_nac_sus_ant_w;
			end if;

			begin
				select	nvl(b.nr_cartao_nac_sus_ant, ' ')
				into	nr_cartao_nac_sus_ant_w
				from 	cnes_profissional 	b
				where	b.cd_pessoa_fisica = cd_medico_executor_w
				and 	rownum = 1;
				exception
			when others then
				nr_cartao_nac_sus_ant_w := ' ';
			end;

			if (nr_cartao_nac_sus_ant_w <> ' ') then
				nr_cns_med_exec_w := nr_cartao_nac_sus_ant_w;
			end if;
		end;
	end if;

	if	(ie_tipo_apac_w <> 4) then
		begin

		open c02;
		loop
		fetch c02 into
			cd_procedimento_w,
			qt_procedimento_w,
			cd_cgc_prestador_w,
			nr_nf_prestador_w,
		--	cd_medico_executor_w,
		--	cd_pessoa_fisica_w,
			cd_cbo_w,
			nr_ordem_w,
			nr_seq_sus_equipe_w;
		exit when c02%notfound;
			begin
			/*select	substr(sus_obter_cbo_medico(nvl(cd_medico_executor_w, cd_pessoa_fisica_w), cd_procedimento_w, 0),1,6)
			into	cd_cbo_w
			from	dual;*/
			/*sus_obter_exec_exp_aih(cd_procedimento_w, 7, nvl(cd_medico_executor_w, cd_pessoa_fisica_w), cd_cbo_w, cd_estabelecimento_w, 'N', 'N', 'S', cd_exec_exp_w, cd_cbo_exec_exp_w);

			if	(cd_cbo_exec_exp_w is not null) and
				(cd_exec_exp_w is not null) then
				begin
				cd_medico_executor_w	:= cd_exec_exp_w;
				cd_cbo_w		:= cd_cbo_exec_exp_w;
				end;
			end if;*/

			sus_obter_prest_exp(cd_procedimento_w, 7,cd_cgc_prestador_w,cd_estabelecimento_w,'N','N','S',cd_cgc_prestador_exp_w);

			if	(cd_cgc_prestador_exp_w	is not null) then
				cd_cgc_prestador_w	:= cd_cgc_prestador_exp_w;
			end if;

			select	nvl(max(a.ie_tpcc),'1'),
				nvl(max(d.cd_grupo),'0')
			into	ie_tpcc_w,
				cd_grupo_w
			from 	sus_procedimento a,
				sus_forma_organizacao b,
				sus_subgrupo c,
				sus_grupo d
			where	a.cd_procedimento 	= cd_procedimento_w
			and	a.ie_origem_proced 	= 7
			and	a.nr_seq_forma_org	= b.nr_sequencia
			and	b.nr_seq_subgrupo	= c.nr_sequencia
			and	c.nr_seq_grupo		= d.nr_sequencia;

			if	(ie_tpcc_w <> '2') then
				begin
				cd_cgc_prestador_w	:= null;
				nr_nf_prestador_w	:= null;
				end;
			end if;

			if	(cd_grupo_w <> '6') then
				begin
				cd_cid_principal_w	:= '';
				cd_cid_secundario_w	:= '';
				end;
			end if;

			begin
			select	count(*)
			into	qt_registro_w
			from	sus_regra_prestador_envio
			where	cd_procedimento 	= cd_procedimento_w
			and	ie_origem_proced 	= 7;
			exception
				when others then
				qt_registro_w := 0;
				end;

			if	(qt_registro_w > 0) then
				cd_cgc_prestador_w	:= null;
				nr_nf_prestador_w		:= null;
			end if;

			if	(qt_proc_w = 10) then
				begin
				select	substr(obter_campo_varia_apac_unif(nr_sequencia_w,nvl(ie_exc_dt_sem_cid_w,'N')),1,141)
				into	ds_varia_w
				from	dual;

				select	w_susapac_interf_seq.nextval
				into	nr_seq_apac_w
				from	dual;

				insert into w_susapac_interf
					(nr_sequencia,
					nr_seq_protocolo,
					nr_atendimento,
					nr_interno_conta,
					dt_atualizacao,
					nm_usuario,
					nr_apac,
					dt_inicio_validade,
					dt_fim_validade,
					ie_tipo_apac,
					nr_cpf,
					nm_paciente,
					uf_nasc,
					nm_mae_pac,
					ds_endereco,
					nr_endereco,
					ds_complemento,
					cd_cep,
					cd_municipio_ibge,
					dt_nascimento,
					ie_sexo,
					nr_cpf_medico,
					nm_medico,
					cd_motivo_cobranca,
					dt_ocorrencia,
					dt_competencia,
					nr_cns_medico,
					cd_cid_causa_assoc,
					dt_solicitacao,
					dt_autorizacao,
					cd_carater_internacao,
					nr_apac_anterior,
					cd_orgao_emissor_apac,
					nr_cns_diretor,
					nr_cpf_diretor,
					nm_diretor,
					nr_prontuario,
					cd_raca_cor_sus,
					cd_etnia,
					dt_emissao,
					nr_cns,
					cd_servico,
					cd_serv_classif,
					cd_procedimento1,
					cd_procedimento2,
					cd_procedimento3,
					cd_procedimento4,
					cd_procedimento5,
					cd_procedimento6,
					cd_procedimento7,
					cd_procedimento8,
					cd_procedimento9,
					cd_procedimento10,
					qt_procedimento1,
					qt_procedimento2,
					qt_procedimento3,
					qt_procedimento4,
					qt_procedimento5,
					qt_procedimento6,
					qt_procedimento7,
					qt_procedimento8,
					qt_procedimento9,
					qt_procedimento10,
					cd_cgc_prestador1,
					cd_cgc_prestador2,
					cd_cgc_prestador3,
					cd_cgc_prestador4,
					cd_cgc_prestador5,
					cd_cgc_prestador6,
					cd_cgc_prestador7,
					cd_cgc_prestador8,
					cd_cgc_prestador9,
					cd_cgc_prestador10,
					nr_nf_prestador1,
					nr_nf_prestador2,
					nr_nf_prestador3,
					nr_nf_prestador4,
					nr_nf_prestador5,
					nr_nf_prestador6,
					nr_nf_prestador7,
					nr_nf_prestador8,
					nr_nf_prestador9,
					nr_nf_prestador10,
					cd_unidade_ibge,
					cd_unidade_apac,
					cd_unidade_solic_apac,
					nm_responsavel,
					ds_varia,
					cd_cbo1,
					cd_cbo2,
					cd_cbo3,
					cd_cbo4,
					cd_cbo5,
					cd_cbo6,
					cd_cbo7,
					cd_cbo8,
					cd_cbo9,
					cd_cbo10,
					cd_cid_pri1,
					cd_cid_pri2,
					cd_cid_pri3,
					cd_cid_pri4,
					cd_cid_pri5,
					cd_cid_pri6,
					cd_cid_pri7,
					cd_cid_pri8,
					cd_cid_pri9,
					cd_cid_pri10,
					cd_cid_sec1,
					cd_cid_sec2,
					cd_cid_sec3,
					cd_cid_sec4,
					cd_cid_sec5,
					cd_cid_sec6,
					cd_cid_sec7,
					cd_cid_sec8,
					cd_cid_sec9,
					cd_cid_sec10,
					cd_equipe1,
					cd_equipe2,
					cd_equipe3,
					cd_equipe4,
					cd_equipe5,
					cd_equipe6,
					cd_equipe7,
					cd_equipe8,
					cd_equipe9,
					cd_equipe10,
					ie_continuacao,
					ie_tipo_atendimento,
					tp_logradouro,
					ds_bairro,
					nr_ddd_pac,
					nr_telefone_pac,
					ds_email,
					nr_cns_med_exec)
				values
					(nr_seq_apac_w,
					nr_seq_protocolo_w,
					nr_atendimento_w,
					nr_interno_conta_w,
					sysdate,
					nm_usuario_p,
					nr_apac_w,
					dt_inicio_validade_w,
					dt_fim_validade_w,
					ie_tipo_apac_w,
					nr_cpf_w,
					nm_paciente_w,
					uf_nasc_w,
					nm_mae_pac_w,
					ds_endereco_w,
					nr_endereco_w,
					ds_complemento_w,
					cd_cep_w,
					cd_municipio_ibge_w,
					dt_nascimento_w,
					ie_sexo_w,
					nr_cpf_medico_w,
					nm_medico_w,
					cd_motivo_cobranca_w,
					dt_ocorrencia_w,
					dt_competencia_w,
					nr_cns_medico_w,
					cd_cid_causa_assoc_w,
					dt_solicitacao_w,
					dt_autorizacao_w,
					cd_carater_internacao_w,
					nr_apac_anterior_w,
					cd_orgao_emissor_apac_w,
					nr_cns_diretor_w,
					nr_cpf_diretor_w,
					nm_diretor_w,
					nr_prontuario_w,
					cd_raca_cor_sus_w,
					cd_etnia_w,
					dt_emissao_w,
					nr_cns_w,
					cd_servico_w,
					cd_serv_classif_w,
					cd_procedimento1_w,
					cd_procedimento2_w,
					cd_procedimento3_w,
					cd_procedimento4_w,
					cd_procedimento5_w,
					cd_procedimento6_w,
					cd_procedimento7_w,
					cd_procedimento8_w,
					cd_procedimento9_w,
					cd_procedimento10_w,
					qt_procedimento1_w,
					qt_procedimento2_w,
					qt_procedimento3_w,
					qt_procedimento4_w,
					qt_procedimento5_w,
					qt_procedimento6_w,
					qt_procedimento7_w,
					qt_procedimento8_w,
					qt_procedimento9_w,
					qt_procedimento10_w,
					cd_cgc_prestador1_w,
					cd_cgc_prestador2_w,
					cd_cgc_prestador3_w,
					cd_cgc_prestador4_w,
					cd_cgc_prestador5_w,
					cd_cgc_prestador6_w,
					cd_cgc_prestador7_w,
					cd_cgc_prestador8_w,
					cd_cgc_prestador9_w,
					cd_cgc_prestador10_w,
					nr_nf_prestador1_w,
					nr_nf_prestador2_w,
					nr_nf_prestador3_w,
					nr_nf_prestador4_w,
					nr_nf_prestador5_w,
					nr_nf_prestador6_w,
					nr_nf_prestador7_w,
					nr_nf_prestador8_w,
					nr_nf_prestador9_w,
					nr_nf_prestador10_w,
					cd_unidade_ibge_w,
					cd_unidade_apac_w,
					nvl(cd_unidade_solic_apac_w,cd_unidade_apac_w),
					nm_responsavel_w,
					ds_varia_w,
					cd_cbo1_w,
					cd_cbo2_w,
					cd_cbo3_w,
					cd_cbo4_w,
					cd_cbo5_w,
					cd_cbo6_w,
					cd_cbo7_w,
					cd_cbo8_w,
					cd_cbo9_w,
					cd_cbo10_w,
					cd_cidpri1_w,
					cd_cidpri2_w,
					cd_cidpri3_w,
					cd_cidpri4_w,
					cd_cidpri5_w,
					cd_cidpri6_w,
					cd_cidpri7_w,
					cd_cidpri8_w,
					cd_cidpri9_w,
					cd_cidpri10_w,
					cd_cidsec1_w,
					cd_cidsec2_w,
					cd_cidsec3_w,
					cd_cidsec4_w,
					cd_cidsec5_w,
					cd_cidsec6_w,
					cd_cidsec7_w,
					cd_cidsec8_w,
					cd_cidsec9_w,
					cd_cidsec10_w,
					cd_equipe1_w,
					cd_equipe2_w,
					cd_equipe3_w,
					cd_equipe4_w,
					cd_equipe5_w,
					cd_equipe6_w,
					cd_equipe7_w,
					cd_equipe8_w,
					cd_equipe9_w,
					cd_equipe10_w,
					ie_continuacao_w,
					ie_tipo_laudo_apac_w,
					tp_logradouro_w,
					ds_bairro_w,
					nr_ddd_pac_w,
					nr_telefone_pac_w,
					ds_email_w,
					nr_cns_med_exec_w);
				ie_continuacao_w	:= ie_continuacao_w + 1;
				qt_proc_w		:= 0;
				cd_procedimento1_w	:= null;
				cd_procedimento2_w	:= null;
				cd_procedimento3_w	:= null;
				cd_procedimento4_w	:= null;
				cd_procedimento5_w	:= null;
				cd_procedimento6_w	:= null;
				cd_procedimento7_w	:= null;
				cd_procedimento8_w	:= null;
				cd_procedimento9_w	:= null;
				cd_procedimento10_w	:= null;
				qt_procedimento1_w	:= null;
				qt_procedimento2_w	:= null;
				qt_procedimento3_w	:= null;
				qt_procedimento4_w	:= null;
				qt_procedimento5_w	:= null;
				qt_procedimento6_w	:= null;
				qt_procedimento7_w	:= null;
				qt_procedimento8_w	:= null;
				qt_procedimento9_w	:= null;
				qt_procedimento10_w	:= null;
				cd_cgc_prestador1_w	:= null;
				cd_cgc_prestador2_w	:= null;
				cd_cgc_prestador3_w	:= null;
				cd_cgc_prestador4_w	:= null;
				cd_cgc_prestador5_w	:= null;
				cd_cgc_prestador6_w	:= null;
				cd_cgc_prestador7_w	:= null;
				cd_cgc_prestador8_w	:= null;
				cd_cgc_prestador9_w	:= null;
				cd_cgc_prestador10_w	:= null;
				nr_nf_prestador1_w	:= null;
				nr_nf_prestador2_w	:= null;
				nr_nf_prestador3_w	:= null;
				nr_nf_prestador4_w	:= null;
				nr_nf_prestador5_w	:= null;
				nr_nf_prestador6_w	:= null;
				nr_nf_prestador7_w	:= null;
				nr_nf_prestador8_w	:= null;
				nr_nf_prestador9_w	:= null;
				nr_nf_prestador10_w	:= null;
				cd_cbo1_w	:= '000000';
				cd_cbo2_w	:= '000000';
				cd_cbo3_w	:= '000000';
				cd_cbo4_w	:= '000000';
				cd_cbo5_w	:= '000000';
				cd_cbo6_w	:= '000000';
				cd_cbo7_w	:= '000000';
				cd_cbo8_w	:= '000000';
				cd_cbo9_w	:= '000000';
				cd_cbo10_w	:= '000000';
				cd_cidpri1_w	:= '';
				cd_cidpri2_w	:= '';
				cd_cidpri3_w	:= '';
				cd_cidpri4_w	:= '';
				cd_cidpri5_w	:= '';
				cd_cidpri6_w	:= '';
				cd_cidpri7_w	:= '';
				cd_cidpri8_w	:= '';
				cd_cidpri9_w	:= '';
				cd_cidpri10_w	:= '';
				cd_cidsec1_w	:= '';
				cd_cidsec2_w	:= '';
				cd_cidsec3_w	:= '';
				cd_cidsec4_w	:= '';
				cd_cidsec5_w	:= '';
				cd_cidsec6_w	:= '';
				cd_cidsec7_w	:= '';
				cd_cidsec8_w	:= '';
				cd_cidsec9_w	:= '';
				cd_cidsec10_w	:= '';
				cd_equipe1_w	:= '';
				cd_equipe2_w	:= '';
				cd_equipe3_w	:= '';
				cd_equipe4_w	:= '';
				cd_equipe5_w	:= '';
				cd_equipe6_w	:= '';
				cd_equipe7_w	:= '';
				cd_equipe8_w	:= '';
				cd_equipe9_w	:= '';
				cd_equipe10_w	:= '';
				end;
			end if;

			qt_proc_w := qt_proc_w + 1;
			if	(qt_proc_w = 1) then
				cd_procedimento1_w 	:= cd_procedimento_w;
				qt_procedimento1_w	:= qt_procedimento_w;
				cd_cgc_prestador1_w	:= cd_cgc_prestador_w;
				nr_nf_prestador1_w	:= nr_nf_prestador_w;
				cd_cbo1_w		:= cd_cbo_w;
				cd_cidpri1_w		:= cd_cid_principal_w;
				cd_cidsec1_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 2) then
				cd_procedimento2_w 	:= cd_procedimento_w;
				qt_procedimento2_w	:= qt_procedimento_w;
				cd_cgc_prestador2_w	:= cd_cgc_prestador_w;
				nr_nf_prestador2_w	:= nr_nf_prestador_w;
				cd_cbo2_w		:= cd_cbo_w;
				cd_cidpri2_w		:= cd_cid_principal_w;
				cd_cidsec2_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 3) then
				cd_procedimento3_w 	:= cd_procedimento_w;
				qt_procedimento3_w	:= qt_procedimento_w;
				cd_cgc_prestador3_w	:= cd_cgc_prestador_w;
				nr_nf_prestador3_w	:= nr_nf_prestador_w;
				cd_cbo3_w		:= cd_cbo_w;
				cd_cidpri3_w		:= cd_cid_principal_w;
				cd_cidsec3_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 4) then
				cd_procedimento4_w	:= cd_procedimento_w;
				qt_procedimento4_w	:= qt_procedimento_w;
				cd_cgc_prestador4_w	:= cd_cgc_prestador_w;
				nr_nf_prestador4_w	:= nr_nf_prestador_w;
				cd_cbo4_w		:= cd_cbo_w;
				cd_cidpri4_w		:= cd_cid_principal_w;
				cd_cidsec4_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 5) then
				cd_procedimento5_w 	:= cd_procedimento_w;
				qt_procedimento5_w	:= qt_procedimento_w;
				cd_cgc_prestador5_w	:= cd_cgc_prestador_w;
				nr_nf_prestador5_w	:= nr_nf_prestador_w;
				cd_cbo5_w		:= cd_cbo_w;
				cd_cidpri5_w		:= cd_cid_principal_w;
				cd_cidsec5_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 6) then
				cd_procedimento6_w 	:= cd_procedimento_w;
				qt_procedimento6_w	:= qt_procedimento_w;
				cd_cgc_prestador6_w	:= cd_cgc_prestador_w;
				nr_nf_prestador6_w	:= nr_nf_prestador_w;
				cd_cbo6_w		:= cd_cbo_w;
				cd_cidpri6_w		:= cd_cid_principal_w;
				cd_cidsec6_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 7) then
				cd_procedimento7_w 	:= cd_procedimento_w;
				qt_procedimento7_w	:= qt_procedimento_w;
				cd_cgc_prestador7_w	:= cd_cgc_prestador_w;
				nr_nf_prestador7_w	:= nr_nf_prestador_w;
				cd_cbo7_w		:= cd_cbo_w;
				cd_cidpri7_w		:= cd_cid_principal_w;
				cd_cidsec7_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 8) then
				cd_procedimento8_w 	:= cd_procedimento_w;
				qt_procedimento8_w	:= qt_procedimento_w;
				cd_cgc_prestador8_w	:= cd_cgc_prestador_w;
				nr_nf_prestador8_w	:= nr_nf_prestador_w;
				cd_cbo8_w		:= cd_cbo_w;
				cd_cidpri8_w		:= cd_cid_principal_w;
				cd_cidsec8_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 9) then
				cd_procedimento9_w 	:= cd_procedimento_w;
				qt_procedimento9_w	:= qt_procedimento_w;
				cd_cgc_prestador9_w	:= cd_cgc_prestador_w;
				nr_nf_prestador9_w	:= nr_nf_prestador_w;
				cd_cbo9_w		:= cd_cbo_w;
				cd_cidpri9_w		:= cd_cid_principal_w;
				cd_cidsec9_w		:= cd_cid_secundario_w;
			elsif	(qt_proc_w = 10) then
				cd_procedimento10_w 	:= cd_procedimento_w;
				qt_procedimento10_w	:= qt_procedimento_w;
				cd_cgc_prestador10_w	:= cd_cgc_prestador_w;
				nr_nf_prestador10_w	:= nr_nf_prestador_w;
				cd_cbo10_w		:= cd_cbo_w;
				cd_cidpri10_w		:= cd_cid_principal_w;
				cd_cidsec10_w		:= cd_cid_secundario_w;
			end if;
			end;
		end loop;
		close c02;

		select	substr(obter_campo_varia_apac_unif(nr_sequencia_w,nvl(ie_exc_dt_sem_cid_w,'N')),1,141)
		into	ds_varia_w
		from	dual;

		end;
	elsif	(ie_tipo_apac_w = 4) then
		begin
		cd_procedimento1_w	:= '0000000000';
		cd_procedimento2_w	:= '0000000000';
		cd_procedimento3_w	:= '0000000000';
		cd_procedimento4_w	:= '0000000000';
		cd_procedimento5_w	:= '0000000000';
		cd_procedimento6_w	:= '0000000000';
		cd_procedimento7_w	:= '0000000000';
		cd_procedimento8_w	:= '0000000000';
		cd_procedimento9_w	:= '0000000000';
		cd_procedimento10_w	:= '0000000000';
		qt_procedimento1_w	:= '0000000';
		qt_procedimento2_w	:= '0000000';
		qt_procedimento3_w	:= '0000000';
		qt_procedimento4_w	:= '0000000';
		qt_procedimento5_w	:= '0000000';
		qt_procedimento6_w	:= '0000000';
		qt_procedimento7_w	:= '0000000';
		qt_procedimento8_w	:= '0000000';
		qt_procedimento9_w	:= '0000000';
		qt_procedimento10_w	:= '0000000';
		cd_servico_w		:= null;
		cd_serv_classif_w	:= null;
                if (cd_carater_internacao_w <> '01') then
        		nr_cns_medico_w		:= null;
                end if;
		nr_apac_anterior_w	:= '0000000000000';
		ds_varia_w		:= '9';
		end;
	end if;

	select	w_susapac_interf_seq.nextval
	into	nr_seq_apac_w
	from	dual;

	insert into w_susapac_interf
		(nr_sequencia,
		nr_seq_protocolo,
		nr_atendimento,
		nr_interno_conta,
		dt_atualizacao,
		nm_usuario,
		nr_apac,
		dt_inicio_validade,
		dt_fim_validade,
		ie_tipo_apac,
		nr_cpf,
		nm_paciente,
		uf_nasc,
		nm_mae_pac,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		cd_cep,
		cd_municipio_ibge,
		dt_nascimento,
		ie_sexo,
		nr_cpf_medico,
		nm_medico,
		cd_motivo_cobranca,
		dt_ocorrencia,
		dt_competencia,
		nr_cns_medico,
		cd_cid_causa_assoc,
		dt_solicitacao,
		dt_autorizacao,
		cd_carater_internacao,
		nr_apac_anterior,
		cd_orgao_emissor_apac,
		nr_cns_diretor,
		nr_cpf_diretor,
		nm_diretor,
		nr_prontuario,
		cd_raca_cor_sus,
		cd_etnia,
		dt_emissao,
		nr_cns,
		cd_servico,
		cd_serv_classif,
		cd_procedimento1,
		cd_procedimento2,
		cd_procedimento3,
		cd_procedimento4,
		cd_procedimento5,
		cd_procedimento6,
		cd_procedimento7,
		cd_procedimento8,
		cd_procedimento9,
		cd_procedimento10,
		qt_procedimento1,
		qt_procedimento2,
		qt_procedimento3,
		qt_procedimento4,
		qt_procedimento5,
		qt_procedimento6,
		qt_procedimento7,
		qt_procedimento8,
		qt_procedimento9,
		qt_procedimento10,
		cd_cgc_prestador1,
		cd_cgc_prestador2,
		cd_cgc_prestador3,
		cd_cgc_prestador4,
		cd_cgc_prestador5,
		cd_cgc_prestador6,
		cd_cgc_prestador7,
		cd_cgc_prestador8,
		cd_cgc_prestador9,
		cd_cgc_prestador10,
		nr_nf_prestador1,
		nr_nf_prestador2,
		nr_nf_prestador3,
		nr_nf_prestador4,
		nr_nf_prestador5,
		nr_nf_prestador6,
		nr_nf_prestador7,
		nr_nf_prestador8,
		nr_nf_prestador9,
		nr_nf_prestador10,
		cd_unidade_ibge,
		cd_unidade_apac,
		cd_unidade_solic_apac,
		nm_responsavel,
		ds_varia,
		cd_cbo1,
		cd_cbo2,
		cd_cbo3,
		cd_cbo4,
		cd_cbo5,
		cd_cbo6,
		cd_cbo7,
		cd_cbo8,
		cd_cbo9,
		cd_cbo10,
		cd_cid_pri1,
		cd_cid_pri2,
		cd_cid_pri3,
		cd_cid_pri4,
		cd_cid_pri5,
		cd_cid_pri6,
		cd_cid_pri7,
		cd_cid_pri8,
		cd_cid_pri9,
		cd_cid_pri10,
		cd_cid_sec1,
		cd_cid_sec2,
		cd_cid_sec3,
		cd_cid_sec4,
		cd_cid_sec5,
		cd_cid_sec6,
		cd_cid_sec7,
		cd_cid_sec8,
		cd_cid_sec9,
		cd_cid_sec10,
		cd_equipe1,
		cd_equipe2,
		cd_equipe3,
		cd_equipe4,
		cd_equipe5,
		cd_equipe6,
		cd_equipe7,
		cd_equipe8,
		cd_equipe9,
		cd_equipe10,
		ie_continuacao,
		ie_tipo_atendimento,
		tp_logradouro,
		ds_bairro,
		nr_ddd_pac,
		nr_telefone_pac,
		ds_email,
		nr_cns_med_exec)
	values
		(nr_seq_apac_w,
		nr_seq_protocolo_w,
		nr_atendimento_w,
		nr_interno_conta_w,
		sysdate,
		nm_usuario_p,
		nr_apac_w,
		dt_inicio_validade_w,
		dt_fim_validade_w,
		ie_tipo_apac_w,
		nr_cpf_w,
		nm_paciente_w,
		uf_nasc_w,
		nm_mae_pac_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		cd_cep_w,
		cd_municipio_ibge_w,
		dt_nascimento_w,
		ie_sexo_w,
		nr_cpf_medico_w,
		nm_medico_w,
		cd_motivo_cobranca_w,
		dt_ocorrencia_w,
		dt_competencia_w,
		nr_cns_medico_w,
		cd_cid_causa_assoc_w,
		dt_solicitacao_w,
		dt_autorizacao_w,
		cd_carater_internacao_w,
		nr_apac_anterior_w,
		cd_orgao_emissor_apac_w,
		nr_cns_diretor_w,
		nr_cpf_diretor_w,
		nm_diretor_w,
		nr_prontuario_w,
		cd_raca_cor_sus_w,
		cd_etnia_w,
		dt_emissao_w,
		nr_cns_w,
		cd_servico_w,
		cd_serv_classif_w,
		cd_procedimento1_w,
		cd_procedimento2_w,
		cd_procedimento3_w,
		cd_procedimento4_w,
		cd_procedimento5_w,
		cd_procedimento6_w,
		cd_procedimento7_w,
		cd_procedimento8_w,
		cd_procedimento9_w,
		cd_procedimento10_w,
		qt_procedimento1_w,
		qt_procedimento2_w,
		qt_procedimento3_w,
		qt_procedimento4_w,
		qt_procedimento5_w,
		qt_procedimento6_w,
		qt_procedimento7_w,
		qt_procedimento8_w,
		qt_procedimento9_w,
		qt_procedimento10_w,
		cd_cgc_prestador1_w,
		cd_cgc_prestador2_w,
		cd_cgc_prestador3_w,
		cd_cgc_prestador4_w,
		cd_cgc_prestador5_w,
		cd_cgc_prestador6_w,
		cd_cgc_prestador7_w,
		cd_cgc_prestador8_w,
		cd_cgc_prestador9_w,
		cd_cgc_prestador10_w,
		nr_nf_prestador1_w,
		nr_nf_prestador2_w,
		nr_nf_prestador3_w,
		nr_nf_prestador4_w,
		nr_nf_prestador5_w,
		nr_nf_prestador6_w,
		nr_nf_prestador7_w,
		nr_nf_prestador8_w,
		nr_nf_prestador9_w,
		nr_nf_prestador10_w,
		cd_unidade_ibge_w,
		cd_unidade_apac_w,
		nvl(cd_unidade_solic_apac_w,cd_unidade_apac_w),
		nm_responsavel_w,
		ds_varia_w,
		cd_cbo1_w,
		cd_cbo2_w,
		cd_cbo3_w,
		cd_cbo4_w,
		cd_cbo5_w,
		cd_cbo6_w,
		cd_cbo7_w,
		cd_cbo8_w,
		cd_cbo9_w,
		cd_cbo10_w,
		cd_cidpri1_w,
		cd_cidpri2_w,
		cd_cidpri3_w,
		cd_cidpri4_w,
		cd_cidpri5_w,
		cd_cidpri6_w,
		cd_cidpri7_w,
		cd_cidpri8_w,
		cd_cidpri9_w,
		cd_cidpri10_w,
		cd_cidsec1_w,
		cd_cidsec2_w,
		cd_cidsec3_w,
		cd_cidsec4_w,
		cd_cidsec5_w,
		cd_cidsec6_w,
		cd_cidsec7_w,
		cd_cidsec8_w,
		cd_cidsec9_w,
		cd_cidsec10_w,
		cd_equipe1_w,
		cd_equipe2_w,
		cd_equipe3_w,
		cd_equipe4_w,
		cd_equipe5_w,
		cd_equipe6_w,
		cd_equipe7_w,
		cd_equipe8_w,
		cd_equipe9_w,
		cd_equipe10_w,
		ie_continuacao_w,
		ie_tipo_laudo_apac_w,
		tp_logradouro_w,
		ds_bairro_w,
		nr_ddd_pac_w,
		nr_telefone_pac_w,
		ds_email_w,
		nr_cns_med_exec_w);
	end;
	ie_continuacao_w	:= 0;
end loop;
close c01;
commit;

end gerar_interf_sus_apac;
/