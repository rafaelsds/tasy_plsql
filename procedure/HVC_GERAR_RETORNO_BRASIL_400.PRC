create or replace
procedure HVC_GERAR_RETORNO_BRASIL_400
			(	nr_seq_cobr_escrit_p	number,
				nm_usuario_p		varchar2) is 

ds_titulo_w			varchar2(255);
dt_liquidacao_w			varchar2(6);
vl_titulo_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
vl_outras_despesas_w		number(15,2);
vl_cobranca_w			number(15,2);
vl_alterar_w			number(15,2);
nr_seq_reg_T_w			number(10);
nr_seq_reg_U_w			number(10);
nr_titulo_w			number(10);
cd_ocorrencia_w			number(10);
nr_seq_ocorrencia_ret_w		number(10);
cd_banco_w			number(3);
cd_banco_cobr_w			number(3);
dt_pagto_real_w			varchar2(6);

cursor C01 is
	select	trim(substr(ds_string,117,10)),
		to_number(substr(ds_string,153,13))/100,
		to_number(substr(ds_string,267,13))/100,
		to_number(substr(ds_string,241,13))/100,
		to_number(substr(ds_string,228,13))/100,
		to_number(substr(ds_string,254,13))/100,
		to_number(substr(ds_string,182,7))/100,
		substr(ds_string,111,6),
		substr(ds_string,111,6),
		substr(ds_string,109,2)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= '7';

begin
select	max(to_number(substr(ds_string,77,3)))
into	cd_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,1,1)	= '0';

select	max(to_number(a.cd_banco))
into	cd_banco_cobr_w
from	banco_carteira b,
	cobranca_escritural a
where	a.nr_sequencia		= nr_seq_cobr_escrit_p
and	a.nr_seq_carteira_cobr	= b.nr_sequencia(+);

if	(cd_banco_w <> cd_banco_cobr_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(184490);
end if;

open C01;
loop
fetch C01 into	
	ds_titulo_w,
	vl_cobranca_w,
	vl_acrescimo_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_outras_despesas_w,
	dt_liquidacao_w,
	dt_pagto_real_w,
	cd_ocorrencia_w;
exit when C01%notfound;
	begin
	vl_alterar_w	:= 0;
	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	/* Se encontrou o t�tulo importa, sen�o grava no log */
	if	(nr_titulo_w is not null) then
		select	vl_titulo
		into	vl_titulo_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;


		select 	nvl(to_char(max(a.nr_sequencia)),0)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco 			= 001
		and	a.cd_ocorrencia 		= cd_ocorrencia_w
		and	nvl(a.ie_forma_cobranca,1)	= 1;

		/* Tratar acrescimos/descontos */
		if	(vl_titulo_w <> vl_liquido_w) then
			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;	
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;
		end if;
		
		insert into titulo_receber_cobr
			(nr_sequencia,
			nr_titulo,
			cd_banco,
			vl_cobranca,
			vl_desconto,
			vl_acrescimo,
			vl_despesa_bancaria,
			vl_liquidacao,
			dt_liquidacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_ocorrencia_ret,
			dt_pagamento_real)
		values	(titulo_receber_cobr_seq.nextval,
			nr_titulo_w,
			001,
			vl_titulo_w,
			vl_desconto_w,
			vl_acrescimo_w,
			vl_outras_despesas_w,
			vl_liquido_w,
			nvl(to_Date(dt_liquidacao_w,'ddmmyy'),sysdate),
			sysdate,
			nm_usuario_p,
			nr_seq_cobr_escrit_p,
			nr_seq_ocorrencia_ret_w,
			nvl(to_Date(dt_pagto_real_w,'ddmmyy'),sysdate));
	else
		/*insert into logxxx_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');*/
			
		insert into cobranca_escrit_log
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_cobranca,
			ds_log)
		values	(cobranca_escrit_log_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_cobr_escrit_p,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');
	end if;
	end;
end loop;
close C01;

commit;

end HVC_GERAR_RETORNO_BRASIL_400;
/
