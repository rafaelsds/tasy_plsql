create or replace
procedure consistir_bef_post_orcamento(
		cd_conta_contabil_p	varchar2,
		cd_centro_custo_p		number,
		nr_seq_mes_ref_p		number,
		cd_empresa_p		number,
		ds_erro_p out		varchar2,
		ie_null_cd_conta_p		varchar2,
		ie_null_centro_p		varchar2,
		ie_permite_alterar_p		varchar2,
		ie_tipo_p			varchar2,
		nm_usuario_p		varchar2) is 

ie_qtd_w		number(10,0);
ie_obter_conta_w	varchar2(1);
ie_obter_centro_w	varchar2(1);
ie_tipo_w		ctb_grupo_conta.ie_tipo%type;
ie_situacao_w		conta_contabil.ie_situacao%type;

begin
if 	(nm_usuario_p is not null) then
	begin
	
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	conta_contabil
	where	cd_conta_contabil = cd_conta_contabil_p;
	exception when others then
		ie_situacao_w	:= '';
	end;

	if	(ie_situacao_w = 'I') then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || obter_texto_tasy(44259, wheb_usuario_pck.get_nr_seq_idioma));
		end;
	end if;

	select	count(*)
	into	ie_qtd_w
	from	ctb_mes_ref
	where	trunc(dt_referencia,'month') < trunc(sysdate,'month')
	and	nr_sequencia = nr_seq_mes_ref_p;

	if	(ie_qtd_w <> 0) and
		(ie_permite_alterar_p <> 'S') then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || obter_texto_tasy(44260, wheb_usuario_pck.get_nr_seq_idioma));
		end;
	end if;

	consiste_grupo_centro_conta(cd_conta_contabil_p, cd_centro_custo_p, ie_tipo_p, ds_erro_p);

	ie_obter_conta_w := ctb_obter_se_conta_usuario(cd_empresa_p, cd_conta_contabil_p, nm_usuario_p);

	if	(ie_obter_conta_w <> 'S') then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || obter_texto_tasy(44261, wheb_usuario_pck.get_nr_seq_idioma));
		end;
	end if;

	if	(ie_null_cd_conta_p = 'S') then
		begin
			select	max(b.ie_tipo)
			into	ie_tipo_w
			from	ctb_grupo_conta b,
				conta_contabil a
			where	a.cd_grupo		= b.cd_grupo
			and	a.cd_empresa		= b.cd_empresa
			and	a.cd_conta_contabil	= cd_conta_contabil_p;
			
			if	(ie_tipo_w <> 'R') and
				(ie_tipo_w <> 'C') and
				(ie_tipo_w <> 'D') then
				begin
				wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || obter_texto_tasy(44263, wheb_usuario_pck.get_nr_seq_idioma));
				end;
			end if;
		end;
	end if;

	if	(ie_null_centro_p = 'S') then
		begin
			ie_obter_centro_w := ctb_obter_se_centro_usuario(cd_centro_custo_p, cd_empresa_p, nm_usuario_p);
			
			if	(ie_obter_centro_w in('V','N')) then
				begin
				wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || obter_texto_tasy(92455, wheb_usuario_pck.get_nr_seq_idioma));
				end;
			end if;
		end;
	end if;

	end;
end if;
commit;
end consistir_bef_post_orcamento;
/