create or replace
procedure alterar_prestador_solicitante	(nr_prescricao_p	number,
				cd_cgc_solic_p	varchar2,
				nm_usuario_p	varchar2) is

begin
if	(nr_prescricao_p is not null) and	
	(nm_usuario_p is not null) then

	update	prescr_medica
	set	cd_cgc_solic	= cd_cgc_solic_p,
		nm_usuario	= nm_usuario_p
	where	nr_prescricao	= nr_prescricao_p;

end if;

commit;

end alterar_prestador_solicitante;
/
