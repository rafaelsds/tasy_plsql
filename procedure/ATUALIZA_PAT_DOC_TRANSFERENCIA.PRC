create or replace
procedure atualiza_pat_doc_transferencia(nr_seq_motivo_baixa_p number,
			nr_sequencia_p number) is 

begin

if (nr_sequencia_p is not null) then
	begin
	update  pat_doc_transferencia 
	set 	nr_seq_motivo_baixa = nr_seq_motivo_baixa_p, 
		dt_transferencia = sysdate 
	where	nr_sequencia = nr_sequencia_p;
	commit;
	end;	
end if;
end atualiza_pat_doc_transferencia;
/