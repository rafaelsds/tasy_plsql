CREATE OR REPLACE PROCEDURE carga_custo_procedimento(cd_procedimento_p NUMBER,
                                                     ie_origem_proced_p NUMBER,
                                                     cd_tabela_custo_p NUMBER,
                                                     nr_seq_proc_interno_p NUMBER,
                                                     qt_dias_prazo_p NUMBER,
                                                     nr_seq_exame_p NUMBER,
                                                     vl_presente_p NUMBER,
                                                     vl_cotado_p NUMBER,
                                                     cd_estabelecimento_p NUMBER,
                                                     nm_usuario_p VARCHAR2,
                                                     nr_seq_tabela_p NUMBER,
                                                     ds_observacao_p VARCHAR2) IS

begin

    insert into CUSTO_PROCEDIMENTO (
        nr_sequencia,
        nr_seq_proc_interno,
        nr_seq_tabela,
        cd_estabelecimento,
        cd_tabela_custo,
        cd_procedimento,
        ie_origem_proced,
        nm_usuario,
        dt_atualizacao,
        qt_dias_prazo,
        vl_cotado,
        vl_presente,
        ds_observacao,
        nr_seq_exame,
        dt_atualizacao_nrec,
        nm_usuario_nrec
    ) VALUES (
        custo_procedimento_seq.nextval,
        nr_seq_proc_interno_p,
        nr_seq_tabela_p,
        cd_estabelecimento_p,
        cd_tabela_custo_p,
        cd_procedimento_p,
        ie_origem_proced_p,
        nm_usuario_p,
        sysdate,
        qt_dias_prazo_p,
        vl_cotado_p,
        vl_presente_p,
        ds_observacao_p,
        nr_seq_exame_p,
        sysdate,
        nm_usuario_p
    );

    commit;

end carga_custo_procedimento;
/
