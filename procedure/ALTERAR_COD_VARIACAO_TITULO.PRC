create or replace
procedure alterar_cod_variacao_titulo(cd_variacao_p		Varchar2,
				      nm_usuario_p		Varchar2,
				      nr_titulo_p		Varchar2) is 

begin

update 	titulo_pagar
set	cd_variacao = cd_variacao_p,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_titulo = nr_titulo_P;

commit;

end alterar_cod_variacao_titulo;
/