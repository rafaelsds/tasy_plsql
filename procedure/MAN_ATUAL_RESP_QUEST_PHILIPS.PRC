create or replace
procedure man_atual_resp_quest_philips( nr_seq_restricao_p	in	Number,
				nr_seq_pergunta_p		in 	Number,
				ds_resultado_p		in 	Varchar2,
				vl_resultado_p		in 	Number,
				dt_resultado_p		in 	Date,
				nm_usuario_p		in	Varchar2,
				nr_sequencia_p		out 	Number) is 
				
qt_reg_w			Number(3);

begin

select	count(*)
into	qt_reg_w
from	man_os_resp_quest_philips
where	nr_seq_ordem		= nr_seq_restricao_p
and	nr_seq_pergunta		= nr_seq_pergunta_p;

if	(qt_reg_w > 0) then
	update	man_os_resp_quest_philips
	set	ds_resultado	= ds_resultado_p,
		vl_resultado	= vl_resultado_p,
		dt_resultado	= dt_resultado_p
	where	nr_seq_ordem	= nr_seq_restricao_p
	and	nr_seq_pergunta	= nr_seq_pergunta_p;
	
	nr_sequencia_p	:= 0;
else
	select	man_os_resp_quest_philips_seq.nextval	
	into	nr_sequencia_p
	from	dual;

	insert into man_os_resp_quest_philips(
		nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		nr_seq_pergunta,
		ds_resultado,
		vl_resultado,
		dt_resultado)
	values(	nr_sequencia_p,
		nr_seq_restricao_p,
		sysdate,		
		nm_usuario_p,
		nr_seq_pergunta_p,
		ds_resultado_p, 
		vl_resultado_p,
		dt_resultado_p);
end if;

commit;

end man_atual_resp_quest_philips;
/
