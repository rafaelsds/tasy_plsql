create or replace
procedure pls_desfazer_calculo_peona
			(	nr_seq_peona_p		Number,
				nm_usuario_p		Varchar2) is 

ie_concil_contab_w	pls_visible_false.ie_concil_contab%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin

cd_estabelecimento_w 	:= Obter_estab_usuario(nm_usuario_p);

select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_w;

if 	(ie_concil_contab_w = 'S') then
	pls_ctb_onl_gravar_movto_pck.gravar_movto_desf_calc_peona(nr_seq_peona_p, cd_estabelecimento_w, nm_usuario_p);
end if;

delete from pls_valores_peona
where	nr_seq_peona	= nr_seq_peona_p;

update	pls_peona	
set	nr_parcela		= null,
	vl_total_receita	= 0,
	vl_total_despesa	= 0,
	vl_base_receita		= 0,
	vl_base_despesa		= 0,
	vl_base			= 0,
	vl_estimativa_mes	= 0,
	vl_estimativa_parcela	= 0,
	vl_acumulado		= 0,
	vl_base_parcela		= 0,
	vl_acumulado_estimativa	= 0,
	vl_peona		= 0
where	nr_sequencia		= nr_seq_peona_p;

commit;

end pls_desfazer_calculo_peona;
/