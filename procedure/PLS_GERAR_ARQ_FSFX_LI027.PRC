CREATE OR REPLACE PROCEDURE PLS_GERAR_ARQ_FSFX_LI027
            (    nr_sequencia_p        number,
                ds_local_p        varchar2,
                nm_arquivo_p         varchar2) is

ds_erro_w            varchar2(255);
ds_conteudo_w            varchar2(4000);
arq_texto_w            utl_file.file_type;
vazio_w                varchar2(30) := '                ';
ie_liberacao_debito_w        pls_ame_regra_geracao.ie_liberacao_debito%type;
nr_seq_ame_empresa_w        pls_ame_lote_rem_destino.nr_seq_ame_empresa%type;
nr_seq_ame_subsidiaria_w    pls_ame_lote_rem_destino.nr_seq_ame_subsidiaria%type;

Cursor C01 is
    select    lpad(nvl(c.CD_MATRICULA_ESTIPULANTE,'0'),8,'0') CD_MATRICULA_ESTIPULANTE,
        rpad(d.nm_pessoa_fisica,40,' ') nm_pessoa_fisica,
        rpad(decode(c.nr_seq_titular,null,' ',nvl(d.nr_cpf,' ')),11,' ') nr_cpf_dependente,
        decode(c.nr_seq_titular,null,'00', ( select    x.CD_SISTEMA_ANTERIOR
                                from    GRAU_PARENTESCO    x
                                where    x.nr_sequencia    = c.nr_seq_parentesco)) ie_tipo,
        decode(c.dt_contratacao,null,'        ',to_char(c.dt_contratacao,'ddmmyyyy')) dt_contratacao,
        decode(a.DT_RESCISAO_LIB_DEB,null,'        ',to_char(a.DT_RESCISAO_LIB_DEB,'ddmmyyyy')) dt_rescisao,
        lpad(b.CD_TARJA_MAGNETICA,2,'0') ie_tipo_plano,
        lpad(nvl(c.IE_TITULARIDADE,'0'),2,'00') IE_TITULARIDADE,
        sum(nvl(e.vl_detalhe,0)) vl_detalhe,
        e.CD_COBRANCA,
        to_char(e.qt_dias_pro_rata) qt_dias_pro_rata,
        (    select    max(x.CD_CODIGO)
            from    PLS_CONTRATO_CODIGO x
            where    x.nr_seq_contrato = c.nr_seq_contrato
            and    x.NR_SEQ_TIPO_CODIGO = 2) cd_company,
        (    select    max(x.CD_CODIGO)
            from    PLS_CONTRATO_CODIGO x
            where    x.nr_seq_contrato = c.nr_seq_contrato
            and    x.NR_SEQ_TIPO_CODIGO = 1) cd_grupo_unisaude
    from    pessoa_fisica        d,
        pls_segurado        c,
        pls_plano        b,
        PLS_AME_LOTE_REM_VALOR    a,
        PLS_AME_LOTE_REM_VALOR_DET e
    where    d.cd_pessoa_fisica    = c.cd_pessoa_fisica
    and    a.nr_seq_segurado    = c.nr_sequencia
    and    c.nr_seq_plano        = b.nr_sequencia
    and    e.NR_SEQ_LOTE_REM_VALOR    = a.nr_sequencia
    and    a.NR_SEQ_LOTE_REM_ARQ    = nr_sequencia_p
    group by c.CD_MATRICULA_ESTIPULANTE,d.nm_pessoa_fisica,c.nr_seq_titular,d.nr_cpf,c.dt_contratacao,a.DT_RESCISAO_LIB_DEB,
        b.CD_TARJA_MAGNETICA,c.IE_TITULARIDADE,e.CD_COBRANCA,c.nr_sequencia,c.nr_seq_parentesco,e.qt_dias_pro_rata,c.nr_seq_contrato
    order by decode(c.nr_seq_titular,null,c.nr_sequencia,c.nr_seq_titular),
        decode(c.nr_seq_titular,null,-1,0);

begin

select    nvl(a.IE_LIBERACAO_DEBITO,'N'),
    c.NR_SEQ_AME_EMPRESA,
    c.NR_SEQ_AME_SUBSIDIARIA
into    ie_liberacao_debito_w,
    nr_seq_ame_empresa_w,
    nr_seq_ame_subsidiaria_w
from    PLS_AME_LOTE_REM_ARQUIVO    d,
    PLS_AME_LOTE_REM_DESTINO    c,
    PLS_AME_LOTE_REMESSA        b,
    PLS_AME_REGRA_GERACAO        a
where    d.NR_SEQ_LOTE_REM_DEST        = c.nr_sequencia
and    c.NR_SEQ_LOTE_REM        = b.nr_sequencia
and    b.NR_SEQ_REGRA_GERACAO        = a.nr_sequencia
and    d.nr_sequencia            = nr_sequencia_p;

begin
arq_texto_w := utl_file.fopen(ds_local_p,nm_arquivo_p,'W');
exception
when others then
    if (sqlcode = -29289) then
        ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
    elsif (sqlcode = -29298) then
        ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
    elsif (sqlcode = -29291) then
        ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
    elsif (sqlcode = -29286) then
        ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
    elsif (sqlcode = -29282) then
        ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
    elsif (sqlcode = -29288) then
        ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
    elsif (sqlcode = -29287) then
        ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
    elsif (sqlcode = -29281) then
        ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
    elsif (sqlcode = -29290) then
        ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
    elsif (sqlcode = -29283) then
        ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
    elsif (sqlcode = -29280) then
        ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
    elsif (sqlcode = -29284) then
        ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
    elsif (sqlcode = -29292) then
        ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
    elsif (sqlcode = -29285) then
        ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
    else
        ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
    end if;
end;

if    (ds_erro_w is not null) then
    Raise_application_error(-20011,ds_erro_w);
end if;

utl_file.put_line(arq_texto_w,'    00000000' || chr(13));
utl_file.fflush(arq_texto_w);

for r_c01_w in C01 loop
    begin

    if    (r_c01_w.vl_detalhe < 0) then
        r_c01_w.vl_detalhe    := r_c01_w.vl_detalhe * -1;
    end if;

    ds_conteudo_w    := r_c01_w.cd_company|| r_c01_w.CD_MATRICULA_ESTIPULANTE||rpad(fsfx_elimina_acentos(UPPER(r_c01_w.nm_pessoa_fisica),'S'),40,' ')||  r_c01_w.nr_cpf_dependente||
                lpad(nvl(r_c01_w.ie_tipo,'0'),2,'0')||r_c01_w.dt_contratacao||r_c01_w.dt_rescisao||r_c01_w.CD_COBRANCA||
                replace(lpad(campo_mascara(r_c01_w.vl_detalhe,2),13,'0'),'.',',')||r_c01_w.ie_tipo_plano||
                lpad(nvl(r_c01_w.QT_DIAS_PRO_RATA,' '),2,0) ||ie_liberacao_debito_w||r_c01_w.cd_grupo_unisaude||r_c01_w.IE_TITULARIDADE||
                to_char(sysdate,'ddmmyyyy')||vazio_w;

    utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
    utl_file.fflush(arq_texto_w);

    end;
end loop;

utl_file.fclose(arq_texto_w);

end PLS_GERAR_ARQ_FSFX_LI027;
/