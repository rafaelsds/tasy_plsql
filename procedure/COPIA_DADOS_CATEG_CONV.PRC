create or replace procedure copia_dados_categ_conv(	cd_estabelecimento_p 	Number,
							nm_usuario_p		Varchar2,
							cd_convenio_p		Number,
							cd_categoria_orig_p	Varchar2,
							cd_categoria_dest_p	Varchar2,
							nm_tabela_p		Varchar2,
							ie_excluir_p		Varchar2) is

cd_tipo_acomodacao_w	number(15,0);
cd_estabelecimento_w	number(4,0);
cd_plano_w              	varchar2(10);
dt_inicio_vigencia_w    	date;
dt_final_vigencia_w     	date;
ie_situacao_w           	varchar2(1);
ie_tipo_atendimento_w   	number(3,0);
cd_usuario_padrao_w     	varchar2(30);
ds_usuario_padrao_w     	varchar2(255);
cd_plano_padrao_w       	varchar2(10);
ds_reduzida_w           	varchar2(30);
ie_empresa_w            	varchar2(1);
ie_idade_dieta_w       	varchar2(1);
ie_ident_atend_w        	varchar2(1);
ie_lib_dieta_w          	varchar2(15);
ie_permite_gerar_pacote_w    varchar2(1);
ie_preco_custo_w        	varchar2(1);
nr_acompanhante_w       	number(3,0);
nr_seq_regra_acomp_w    	number(10,0);
qt_dieta_acomp_w        	number(3,0);
qt_idade_maiores_w      	number(3,0);
qt_idade_menores_w      	number(3,0);
qt_existe_w             		number(10,0) := 0;
nr_seq_w			number(10,0);
nr_sequencia_w		number(10,0);


Cursor C01 is
	select	cd_tipo_acomodacao
	from	cat_conv_tipo_acomodacao
	where	cd_categoria = cd_categoria_orig_p
	and	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = cd_estabelecimento_p
	order by	cd_tipo_acomodacao;

Cursor C02 is
	select	cd_estabelecimento
	from	categoria_convenio_estab
	where	cd_categoria = cd_categoria_orig_p
	and	cd_convenio = cd_convenio_p
	order by	cd_estabelecimento;

Cursor C03 is
	select	cd_plano,
		cd_estabelecimento,
		dt_inicio_vigencia,
		dt_final_vigencia,
		ie_situacao
	from	categoria_plano
	where	cd_categoria = cd_categoria_orig_p
	and	cd_convenio = cd_convenio_p
	order by 	cd_plano;

Cursor C04 is
	select	ie_tipo_atendimento,
		ie_situacao,
		cd_estabelecimento
	from	categoria_tipo_atend
	where	cd_categoria = cd_categoria_orig_p
	and	cd_convenio = cd_convenio_p
	order by	ie_tipo_atendimento;

Cursor C05 is
	select	cd_usuario_padrao,
		ds_usuario_padrao
	from	cat_conv_usuario_padrao
	where	cd_categoria = cd_categoria_orig_p
	and	cd_convenio = cd_convenio_p
	and	cd_estabelecimento = cd_estabelecimento_p
	order by 	cd_usuario_padrao;

Cursor C06 is
	select	cd_plano_padrao,
		ds_reduzida,
		ie_empresa,
		ie_idade_dieta,
		ie_ident_atend,
		ie_lib_dieta,
		ie_permite_gerar_pacote,
		ie_preco_custo,
		ie_situacao,
		nr_acompanhante,
		nr_seq_regra_acomp,
		qt_dieta_acomp,
		qt_idade_maiores,
		qt_idade_menores
	from	categoria_convenio
	where	cd_categoria = cd_categoria_orig_p
	and	cd_convenio = cd_convenio_p
	order by 	cd_categoria;

Cursor C07 is
	select	nr_sequencia
	from	regra_ajuste_proc
	where	cd_categoria		= cd_categoria_orig_p
	and	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento		= cd_estabelecimento_p;

Cursor C08 is
	select	nr_sequencia
	from	regra_ajuste_material
	where	cd_categoria		= cd_categoria_orig_p
	and	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento		= cd_estabelecimento_p;

begin

if (nm_tabela_p = 'CAT_CONV_TIPO_ACOMODACAO') then
	if (ie_excluir_p = 'S') then
		delete from CAT_CONV_TIPO_ACOMODACAO
		where	cd_categoria		= cd_categoria_dest_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento		= cd_estabelecimento_p;
	end if;

	open C01;
	loop
	fetch C01 into
		cd_tipo_acomodacao_w;
	exit when C01%notfound;
		begin
		insert into CAT_CONV_TIPO_ACOMODACAO(
			nr_sequencia,
			cd_estabelecimento,
			cd_convenio,
			cd_categoria,
			dt_atualizacao,
			nm_usuario,
			cd_tipo_acomodacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values  (CAT_CONV_TIPO_ACOMODACAO_seq.NextVal,
			cd_estabelecimento_p,
			cd_convenio_p,
			cd_categoria_dest_p,
			sysdate,
			nm_usuario_p,
			cd_tipo_acomodacao_w,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C01;

elsif (nm_tabela_p = 'CATEGORIA_CONVENIO_ESTAB') then
	if (ie_excluir_p = 'S') then
		delete from CATEGORIA_CONVENIO_ESTAB
		where	cd_categoria	= cd_categoria_dest_p
		and	cd_convenio	= cd_convenio_p;
	end if;

	open C02;
	loop
	fetch C02 into
		cd_estabelecimento_w;
	exit when C02%notfound;
		begin
		insert into CATEGORIA_CONVENIO_ESTAB(
			nr_sequencia,
			cd_convenio,
			cd_categoria,
			dt_atualizacao,
			nm_usuario,
			cd_estabelecimento,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values (CATEGORIA_CONVENIO_ESTAB_seq.NextVal,
			cd_convenio_p,
			cd_categoria_dest_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_w,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C02;

elsif (nm_tabela_p = 'CATEGORIA_PLANO') then
	if (ie_excluir_p = 'S') then
		delete from CATEGORIA_PLANO
		where	cd_categoria	= cd_categoria_dest_p
		and	cd_convenio	= cd_convenio_p;
	end if;

	open C03;
	loop
	fetch C03 into
		cd_plano_w,
		cd_estabelecimento_w,
		dt_inicio_vigencia_w,
		dt_final_vigencia_w,
		ie_situacao_w;
	exit when C03%notfound;
		begin
		insert into CATEGORIA_PLANO(
			nr_sequencia,
			cd_convenio,
			cd_categoria,
			dt_atualizacao,
			nm_usuario,
			cd_plano,
			cd_estabelecimento,
			dt_inicio_vigencia,
			dt_final_vigencia,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values (CATEGORIA_PLANO_seq.NextVal,
			cd_convenio_p,
			cd_categoria_dest_p,
			sysdate,
			nm_usuario_p,
			cd_plano_w,
			cd_estabelecimento_w,
			dt_inicio_vigencia_w,
			dt_final_vigencia_w,
			ie_situacao_w,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C03;

elsif (nm_tabela_p = 'CATEGORIA_TIPO_ATEND') then
	if (ie_excluir_p = 'S') then
		delete from CATEGORIA_TIPO_ATEND
		where	cd_categoria	= cd_categoria_dest_p
		and	cd_convenio	= cd_convenio_p;
	end if;

	open C04;
	loop
	fetch C04 into
		ie_tipo_atendimento_w,
		ie_situacao_w,
		cd_estabelecimento_w;
	exit when C04%notfound;
		begin
		insert into CATEGORIA_TIPO_ATEND(
			nr_sequencia,
			cd_convenio,
			cd_categoria,
			dt_atualizacao,
			nm_usuario,
			ie_tipo_atendimento,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento)
		values (CATEGORIA_TIPO_ATEND_seq.NextVal,
			cd_convenio_p,
			cd_categoria_dest_p,
			sysdate,
			nm_usuario_p,
			ie_tipo_atendimento_w,
			ie_situacao_w,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_w);
		end;
	end loop;
	close C04;

elsif (nm_tabela_p = 'CAT_CONV_USUARIO_PADRAO') then
	if (ie_excluir_p = 'S') then
		delete from CAT_CONV_USUARIO_PADRAO
		where	cd_categoria		= cd_categoria_dest_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
	end if;

	open C05;
	loop
	fetch C05 into
		cd_usuario_padrao_w,
		ds_usuario_padrao_w;
	exit when C05%notfound;
		begin
		insert into CAT_CONV_USUARIO_PADRAO(
			nr_sequencia,
			cd_estabelecimento,
			cd_convenio,
			cd_categoria,
			dt_atualizacao,
			nm_usuario,
			cd_usuario_padrao,
			ds_usuario_padrao,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values (CAT_CONV_USUARIO_PADRAO_seq.NextVal,
			cd_estabelecimento_p,
			cd_convenio_p,
			cd_categoria_dest_p,
			sysdate,
			nm_usuario_p,
			cd_usuario_padrao_w,
			ds_usuario_padrao_w,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C05;

elsif (nm_tabela_p = 'CATEGORIA_CONVENIO') then
	open C06;
	loop
	fetch C06 into
		cd_plano_padrao_w,
		ds_reduzida_w,
		ie_empresa_w,
		ie_idade_dieta_w,
		ie_ident_atend_w,
		ie_lib_dieta_w,
		ie_permite_gerar_pacote_w,
		ie_preco_custo_w,
		ie_situacao_w,
		nr_acompanhante_w,
		nr_seq_regra_acomp_w,
		qt_dieta_acomp_w,
		qt_idade_maiores_w,
		qt_idade_menores_w;
	exit when C06%notfound;
		begin
		update	CATEGORIA_CONVENIO
		set	cd_plano_padrao = cd_plano_padrao_w,
			ds_reduzida = ds_reduzida_w,
			dt_atualizacao = sysdate,
			ie_empresa = ie_empresa_w,
			ie_idade_dieta = ie_idade_dieta_w,
			ie_ident_atend = ie_ident_atend_w,
			ie_lib_dieta = ie_lib_dieta_w,
			ie_permite_gerar_pacote = ie_permite_gerar_pacote_w,
			ie_preco_custo = ie_preco_custo_w,
			ie_situacao = ie_situacao_w,
			nm_usuario = nm_usuario_p,
			nr_acompanhante = nr_acompanhante_w,
			nr_seq_regra_acomp = nr_seq_regra_acomp_w,
			qt_dieta_acomp = qt_dieta_acomp_w,
			qt_idade_maiores = qt_idade_maiores_w,
			qt_idade_menores = qt_idade_menores_w
		where	cd_categoria = cd_categoria_dest_p
		and	cd_convenio = cd_convenio_p;
		end;
	end loop;
	close C06;

elsif (nm_tabela_p = 'CONVENIO_AMB') then
	if (ie_excluir_p = 'S') then
		delete from CONVENIO_AMB
		where	cd_categoria 		= cd_categoria_dest_p
		and	cd_convenio 		= cd_convenio_p
		and	cd_estabelecimento 	= cd_estabelecimento_p;
	end if;

	insert into CONVENIO_AMB(
			cd_estabelecimento, cd_convenio, cd_categoria, dt_inicio_vigencia,
			ie_prioridade, cd_edicao_amb, tx_ajuste_geral, dt_atualizacao,
			nm_usuario, vl_ch_honorarios, vl_ch_custo_oper, vl_filme,
			ie_situacao, dt_final_vigencia, nr_seq_tiss_tabela, nr_seq_cbhpm_edicao)
		select	cd_estabelecimento_p, cd_convenio_p, cd_categoria_dest_p,
			dt_inicio_vigencia, ie_prioridade, cd_edicao_amb,
			tx_ajuste_geral, sysdate, nm_usuario_p, vl_ch_honorarios,
			vl_ch_custo_oper, vl_filme, ie_situacao, dt_final_vigencia, nr_seq_tiss_tabela, nr_seq_cbhpm_edicao
		from 	convenio_amb
		where 	cd_categoria		= cd_categoria_orig_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;

elsif (nm_tabela_p = 'CONVENIO_SERVICO') then
	if	(ie_excluir_p = 'S') then
		delete 	from CONVENIO_SERVICO
		where 	cd_categoria 		= cd_categoria_dest_p
		and	cd_convenio 		= cd_convenio_p
		and	cd_estabelecimento 	= cd_estabelecimento_p;
	end if;

	insert into CONVENIO_SERVICO(
			nr_sequencia, cd_estabelecimento, cd_convenio, cd_categoria, dt_liberacao_tabela,
			cd_tabela_servico, dt_atualizacao, nm_usuario, ie_situacao, nr_prioridade, nr_seq_tiss_tabela, tx_ajuste_geral,
			dt_termino)
		select	convenio_servico_seq.NextVal, cd_estabelecimento_p, cd_convenio_p, cd_categoria_dest_p, dt_liberacao_tabela,
			cd_tabela_servico, sysdate, nm_usuario_p, ie_situacao, nr_prioridade, nr_seq_tiss_tabela, tx_ajuste_geral,
			dt_termino
		from	convenio_servico
		where	cd_categoria		= cd_categoria_orig_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;

elsif (nm_tabela_p = 'CONVENIO_PRECO_MAT') then
	if	(ie_excluir_p = 'S') then
		delete 	from CONVENIO_PRECO_MAT
		where 	cd_categoria 		= cd_categoria_dest_p
		and	cd_convenio 		= cd_convenio_p
		and	cd_estabelecimento 	= cd_estabelecimento_p;
	end if;

	insert into CONVENIO_PRECO_MAT(
			cd_estabelecimento, cd_convenio, cd_categoria, dt_liberacao_tabela, cd_tab_preco_mat,
			dt_atualizacao, nm_usuario, tx_ajuste_tabela_mat, ie_situacao, nr_prioridade,
			dt_inicio_vigencia, dt_final_vigencia, ie_tab_adicional)
		select	cd_estabelecimento_p, cd_convenio_p, cd_categoria_dest_p, dt_liberacao_tabela, cd_tab_preco_mat,
			sysdate, nm_usuario_p, tx_ajuste_tabela_mat, ie_situacao, nr_prioridade,
			dt_inicio_vigencia, dt_final_vigencia, ie_tab_adicional
		from	convenio_preco_mat
		where	cd_categoria		= cd_categoria_orig_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;

elsif (nm_tabela_p = 'CONVENIO_BRASINDICE') then
	if	(ie_excluir_p = 'S') then
		delete from CONVENIO_BRASINDICE
		where 	cd_categoria		= cd_categoria_dest_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
	end if;

	insert into CONVENIO_BRASINDICE (
		cd_convenio, cd_categoria, dt_inicio_vigencia, tx_preco_fabrica, dt_atualizacao,
		nm_usuario, tx_brasindice_pmc, ie_situacao, qt_dia_fixo, dt_base_fixo, cd_estabelecimento,
		tx_pmc_neg, tx_pmc_pos, nr_sequencia, cd_grupo_material, cd_classe_material, cd_subgrupo_material,
		nr_seq_estrutura, ie_tipo_material, tx_pfb_neg, tx_pfb_pos, ie_dividir_indice_pmc,
		ie_dividir_indice_pfb, ie_tipo_atendimento, cd_plano, vl_minimo, vl_maximo)
	select 	cd_convenio_p, cd_categoria_dest_p, dt_inicio_vigencia, tx_preco_fabrica, sysdate,
		nm_usuario_p, tx_brasindice_pmc, ie_situacao, qt_dia_fixo, dt_base_fixo, cd_estabelecimento_p,
		tx_pmc_neg, tx_pmc_pos, convenio_brasindice_seq.NextVal, cd_grupo_material, cd_classe_material, cd_subgrupo_material,
		nr_seq_estrutura, ie_tipo_material, tx_pfb_neg, tx_pfb_pos, ie_dividir_indice_pmc,
		ie_dividir_indice_pfb , ie_tipo_atendimento, cd_plano, vl_minimo, vl_maximo
	from 	convenio_brasindice
	where 	cd_categoria		= cd_categoria_orig_p
	and	cd_convenio 		= cd_convenio_p
	and	cd_estabelecimento	= cd_estabelecimento_p;

elsif (nm_tabela_p = 'CONVENIO_SIMPRO') then
	if	(ie_excluir_p = 'S') then
		delete from CONVENIO_SIMPRO
		where 	cd_categoria	= cd_categoria_dest_p
		and	cd_convenio	= cd_convenio_p;
	end if;

	insert into CONVENIO_SIMPRO (
		nr_sequencia, cd_convenio, cd_categoria, dt_inicio_vigencia, dt_atualizacao, nm_usuario,
		tx_preco_fabrica, tx_pmc, ie_situacao, qt_dia_fixo, dt_base_fixo, dt_atualizacao_nrec,
		nm_usuario_nrec, ie_tipo_preco, cd_estabelecimento, cd_grupo_material, cd_classe_material, cd_subgrupo_material,
		TX_PFB_POS, TX_PMC_POS, TX_PFB_NEG, TX_PMC_NEG, vl_minimo, vl_maximo, ie_primeira_versao)
	select 	convenio_simpro_seq.nextval, cd_convenio_p, cd_categoria_dest_p, dt_inicio_vigencia, sysdate,
		nm_usuario_p, tx_preco_fabrica, tx_pmc, ie_situacao, qt_dia_fixo, dt_base_fixo, sysdate,
		nm_usuario_p, ie_tipo_preco, cd_estabelecimento, cd_grupo_material, cd_classe_material, cd_subgrupo_material,
		TX_PFB_POS, TX_PMC_POS, TX_PFB_NEG, TX_PMC_NEG, vl_minimo, vl_maximo, ie_primeira_versao
	from 	convenio_simpro
	where 	cd_categoria	= cd_categoria_orig_p
	and	cd_convenio	= cd_convenio_p;

elsif (nm_tabela_p = 'CONVENIO_TAXA_EXAME') then
	if	(ie_excluir_p = 'S') then
		delete 	from CONVENIO_TAXA_EXAME
		where 	cd_categoria		= cd_categoria_dest_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
	end if;
	insert into CONVENIO_TAXA_EXAME (
		cd_estabelecimento, cd_convenio, cd_categoria, cd_procedimento, cd_taxa_exame, dt_atualizacao,
		nm_usuario, ie_origem_proced, tx_procedimento, ie_criterio_taxa, ie_evento_calculo, ie_origem_taxa,
		ie_regra_filme, nr_sequencia, cd_especialidade, cd_grupo_proc, cd_area_procedimento,
		nr_seq_proc_interno, ie_situacao)
	select 	cd_estabelecimento_p, cd_convenio_p,  cd_categoria_dest_p, cd_procedimento, cd_taxa_exame, sysdate,
		nm_usuario_p, ie_origem_proced, tx_procedimento, ie_criterio_taxa, ie_evento_calculo, ie_origem_taxa,
		ie_regra_filme, convenio_taxa_exame_seq.nextval, cd_especialidade, cd_grupo_proc, cd_area_procedimento,
		nr_seq_proc_interno, ie_situacao
	from 	convenio_taxa_exame
	where 	cd_categoria		= cd_categoria_orig_p
	and	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento	= cd_estabelecimento_p;

elsif (nm_tabela_p = 'REGRA_AJUSTE_PROC') then
	if (ie_excluir_p = 'S') then
		delete	from 	REGRA_AJUSTE_PROC_AVISO
		where	nr_seq_regra in (select nr_sequencia
       					 from   regra_ajuste_proc
				         where 	cd_categoria = cd_categoria_dest_p
					 and	cd_convenio = cd_convenio_p
					 and	cd_estabelecimento = cd_estabelecimento_p);

		delete 	from REGRA_AJUSTE_PROC
		where	cd_categoria		= cd_categoria_dest_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
	end if;

	open C07;
	loop
	fetch C07 into
		nr_seq_w;
	exit when C07%notfound;
		begin
		select	REGRA_AJUSTE_PROC_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into REGRA_AJUSTE_PROC(
			nr_sequencia,		cd_estabelecimento, 		cd_convenio, 		dt_inicio_vigencia,
			ie_situacao, 		dt_atualizacao, 		nm_usuario, 		cd_categoria,
			cd_edicao_amb,		cd_area_procedimento, 		cd_especialidade,	cd_grupo_proc,
			cd_procedimento,	ie_origem_proced, 		ie_preco_informado, 	vl_proc_ajustado,
			ie_glosa,		cd_procedimento_esp, 		tx_ajuste, 		nr_seq_regra_preco,
			dt_final_vigencia,	cd_tipo_acomodacao, 		ie_tipo_atendimento, 	cd_setor_atendimento,
			vl_medico, 		vl_custo_operacional, 		qt_filme, 		nr_auxiliares,
			qt_porte_anestesico,	tx_ajuste_custo_oper, 		tx_ajuste_medico, 	tx_ajuste_filme,
			ie_credenciado,		pr_glosa, 			cd_cid, 		qt_idade_min,
			qt_idade_max, 		nr_seq_proc_interno,		nr_seq_exame, 		cd_plano,
			ie_clinica, 		cd_empresa_ref, 		cd_motivo_exc_conta,	cd_medico,
			ds_observacao, 		tx_ajuste_partic, 		vl_glosa, 		ie_sexo,
			ie_atend_retorno,	ie_autor_particular, 		cd_equipamento, 	ie_proc_qt_zero_sus,
			nr_seq_tipo_acidente,	ie_gerar_sem_proc_ref_sus,	cd_setor_atend_prescr, 	ie_ignora_ch_edicao_amb,
			ie_consiste_prescr,	ie_utiliza_video,		ie_spect,		cd_usuario_convenio,
			qt_pos_inicial,		qt_pos_final,                   cd_tabela_servico,	NR_SEQ_AREA_INT,
			NR_SEQ_ESPEC_INT, 	NR_SEQ_GRUPO_INT,		cd_dependente,		nr_seq_origem,
			cd_convenio_glosa,	cd_categoria_glosa,		nr_seq_estrutura)
		select	nr_sequencia_w, 	cd_estabelecimento_p, 		cd_convenio_p, 		dt_inicio_vigencia,
			ie_situacao, 		sysdate, 			nm_usuario_p, 		cd_categoria_dest_p,
			cd_edicao_amb, 		cd_area_procedimento,		cd_especialidade,  	cd_grupo_proc,
			cd_procedimento, 	ie_origem_proced,		ie_preco_informado, 	vl_proc_ajustado,
			ie_glosa, 		cd_procedimento_esp,		tx_ajuste, 		nr_seq_regra_preco,
			dt_final_vigencia, 	cd_tipo_acomodacao,		ie_tipo_atendimento, 	cd_setor_atendimento,
			vl_medico, 		vl_custo_operacional,		qt_filme, 		nr_auxiliares,
			qt_porte_anestesico, 	tx_ajuste_custo_oper,		tx_ajuste_medico, 	tx_ajuste_filme,
			ie_credenciado, 	pr_glosa,			cd_cid, 		qt_idade_min,
			qt_idade_max, 		nr_seq_proc_interno, 		nr_seq_exame,		null,
			ie_clinica,  		cd_empresa_ref, 		cd_motivo_exc_conta, 	cd_medico,
			ds_observacao, 		tx_ajuste_partic, 		vl_glosa, 		ie_sexo,
			ie_atend_retorno,	ie_autor_particular, 		cd_equipamento, 	ie_proc_qt_zero_sus,
			nr_seq_tipo_acidente,	ie_gerar_sem_proc_ref_sus,	cd_setor_atend_prescr, 	ie_ignora_ch_edicao_amb,
			ie_consiste_prescr,	ie_utiliza_video,		ie_spect,		cd_usuario_convenio,
			qt_pos_inicial,		qt_pos_final,			cd_tabela_servico,	NR_SEQ_AREA_INT,
			NR_SEQ_ESPEC_INT, 	NR_SEQ_GRUPO_INT,		cd_dependente,		nr_seq_origem,
			cd_convenio_glosa,	cd_categoria_glosa,		nr_seq_estrutura
		from 	regra_ajuste_proc
		where 	nr_sequencia = nr_seq_w
		and	cd_categoria = cd_categoria_orig_p
		and	cd_convenio = cd_convenio_p
		and	cd_estabelecimento = cd_estabelecimento_p;

		insert into regra_ajuste_proc_aviso(
			nr_sequencia, nr_seq_regra, dt_atualizacao, nm_usuario, nm_usuario_aviso,
			cd_perfil_aviso, dt_atualizacao_nrec, nm_usuario_nrec)
		select 	regra_ajuste_proc_aviso_seq.nextval, nr_sequencia_w, sysdate,
			nm_usuario_p, nm_usuario_aviso, cd_perfil_aviso, sysdate, nm_usuario_p
		from	regra_ajuste_proc_aviso
		where	nr_seq_regra = nr_seq_w;
		end;
	end loop;
	close C07;

elsif (nm_tabela_p = 'REGRA_AJUSTE_MATERIAL') then
	if	(ie_excluir_p = 'S') then
		delete	from 	REGRA_AJUSTE_MAT_AVISO
		where	nr_seq_regra in (select nr_sequencia
       					 from   regra_ajuste_material
				         where 	cd_categoria = cd_categoria_dest_p
					 and	cd_convenio = cd_convenio_p
					 and	cd_estabelecimento = cd_estabelecimento_p);

		delete	from REGRA_AJUSTE_MATERIAL
		where	cd_categoria	= cd_categoria_dest_p
		and	cd_convenio	= cd_convenio_p;
	end if;

	open C08;
	loop
	fetch C08 into
		nr_seq_w;
	exit when C08%notfound;
		begin

		select	REGRA_AJUSTE_MATERIAL_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into REGRA_AJUSTE_MATERIAL(
			nr_sequencia, dt_inicio_vigencia, ie_situacao, 	cd_categoria, cd_grupo_material,
			cd_subgrupo_material, cd_classe_material, cd_material, tx_ajuste, vl_negociado,
			ie_preco_informado, ie_glosa, tx_brasindice_pfb, tx_brasindice_pmc,
			tx_afaturar, dt_final_vigencia, cd_tipo_acomodacao, ie_tipo_atendimento,
			cd_setor_atendimento, qt_idade_min, qt_idade_max, tx_simpro_pfb, tx_simpro_pmc,
			ie_origem_preco, ie_precedencia_preco, pr_glosa, ie_tipo_material, tx_pmc_neg,
			tx_pmc_pos, cd_cid, cd_tabela_preco, cd_motivo_exc_conta, cd_estabelecimento,
			cd_convenio,dt_atualizacao,nm_usuario, ie_sexo, ie_autor_particular, ie_kit_material, cd_kit_material,
			tx_pfb_neg, tx_pfb_pos, nr_seq_estrutura, ie_clinica, cd_convenio_glosa, cd_categoria_glosa, ie_tipo_preco_autor)
		select	nr_sequencia_w, dt_inicio_vigencia, ie_situacao, cd_categoria_dest_p, cd_grupo_material,
			cd_subgrupo_material, cd_classe_material, cd_material, tx_ajuste, vl_negociado,
			ie_preco_informado, ie_glosa, tx_brasindice_pfb, tx_brasindice_pmc, tx_afaturar,
			dt_final_vigencia, cd_tipo_acomodacao, ie_tipo_atendimento, cd_setor_atendimento,
			qt_idade_min, qt_idade_max,  tx_simpro_pfb, tx_simpro_pmc, ie_origem_preco,
			ie_precedencia_preco, pr_glosa, ie_tipo_material, tx_pmc_neg, tx_pmc_pos,
			cd_cid, cd_tabela_preco, cd_motivo_exc_conta, cd_estabelecimento_p, cd_convenio_p,
			sysdate, nm_usuario_p, ie_sexo, ie_autor_particular, ie_kit_material, cd_kit_material,
			tx_pfb_neg, tx_pfb_pos, nr_seq_estrutura, ie_clinica, cd_convenio_glosa, cd_categoria_glosa, ie_tipo_preco_autor
		from	regra_ajuste_material
		where	cd_categoria		= cd_categoria_orig_p
		and	cd_convenio		= cd_convenio_p
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	nr_sequencia		= nr_seq_w;

		insert into REGRA_AJUSTE_INDICE_DIF(
			nr_sequencia, vl_inicial, vl_final, tx_ajuste, nr_seq_regra, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, ie_origem_preco)
		select	regra_ajuste_indice_dif_seq.nextval, vl_inicial, vl_final, tx_ajuste, nr_sequencia_w,
			sysdate, nm_usuario_p, sysdate, nm_usuario_p, ie_origem_preco
		from	regra_ajuste_indice_dif
		where	nr_seq_regra = nr_seq_w;

		insert into REGRA_AJUSTE_MAT_AVISO(
			nr_sequencia, nr_seq_regra, dt_atualizacao, nm_usuario, nm_usuario_aviso,
			cd_perfil_aviso, dt_atualizacao_nrec, nm_usuario_nrec)
		select 	regra_ajuste_mat_aviso_seq.nextval, nr_sequencia_w, sysdate,
			nm_usuario_p, nm_usuario_aviso, cd_perfil_aviso, sysdate, nm_usuario_p
		from	regra_ajuste_mat_aviso
		where	nr_seq_regra = nr_seq_w;

		end;
	end loop;
	close C08;

end if;

commit;

end copia_dados_categ_conv;
/