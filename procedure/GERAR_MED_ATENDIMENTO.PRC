create or replace
procedure Gerar_Med_Atendimento	
		(nr_seq_agenda_p			number,
		cd_pessoa_fisica_p		varchar2,
		cd_medico_p				varchar2,
		nm_usuario_p			varchar2,
		ie_chegada_consulta_p		varchar2,
		ds_erro_p			out 	varchar2) is


/*	C - Consulta
	E - Entrada/Chegada */


nr_seq_cliente_w			number(10,0)	:= Null;
nr_atendimento_w			number(10,0)	:= Null;
cd_convenio_w			number(05,0)	:= Null;
nr_seq_plano_w			number(10,0)	:= Null;
cd_usuario_convenio_w		varchar2(30);
ie_classif_agenda_w		varchar2(05);
dt_validade_carteira_w		date;
ie_tipo_consulta_w		number(3,0);

begin

select	ie_classif_agenda
into		ie_classif_agenda_w
from		agenda_consulta
where		nr_sequencia	= nr_seq_agenda_p;


if	(ie_classif_agenda_w <> 'M') then
	begin
	select		nvl(max(a.nr_sequencia),0),
			max(a.cd_convenio),
			max(a.nr_seq_plano),
			max(a.cd_usuario_convenio),
			max(a.dt_validade_carteira)
	into		nr_seq_cliente_w,
			cd_convenio_w,
			nr_seq_plano_w,
			cd_usuario_convenio_w,
			dt_validade_carteira_w
	from		med_cliente a
	where		a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and		a.cd_medico		= cd_medico_p
	and             a.nr_sequencia          in
		           (select x.nr_sequencia
	        	   from    med_cliente x
	        	   where   x.cd_pessoa_fisica      = a.cd_pessoa_fisica
        		   and     x.cd_medico             = a.cd_medico
		           and     x.cd_pessoa_sist_orig   is not null
		           union
		           select  max(x.nr_sequencia)
		           from    med_cliente x
		           where   x.cd_pessoa_fisica      = a.cd_pessoa_fisica
		           and     x.cd_medico             = a.cd_medico
		           and     x.cd_pessoa_sist_orig   is null
		           and     not exists
        	       	   (select 1
		           from    med_cliente y
		           where   y.cd_pessoa_fisica      = a.cd_pessoa_fisica
                	   and     y.cd_medico             = a.cd_medico
	                   and     y.cd_pessoa_sist_orig   is not null));

	select	nvl(max(nr_atendimento),0)
	into		nr_atendimento_w
	from		med_atendimento
	where		nr_seq_agenda		= nr_seq_agenda_p;

	if	(nr_atendimento_w > 0) then
		begin
		if	(ie_chegada_consulta_p = 'E') then
			update	med_atendimento
			set		dt_entrada	= sysdate
			where		nr_atendimento	= nr_atendimento_w;	
		elsif	(ie_chegada_consulta_p = 'C') then
			update	med_atendimento
			set		dt_inicio_consulta = sysdate
			where		nr_atendimento	   = nr_atendimento_w
			and		dt_inicio_consulta is null;
		end if;
		end;
	else
		begin
		
		if	(nr_seq_cliente_w <> 0) then
			begin

			if	(cd_convenio_w is null) then
				select	cd_convenio
				into		cd_convenio_w
				from		agenda_consulta
				where		nr_sequencia	= nr_seq_agenda_p;
			end if;	
	
			if	(nr_seq_plano_w is null) then
				select	nr_seq_plano
				into		nr_seq_plano_w
				from		agenda_consulta
				where		nr_sequencia	= nr_seq_agenda_p;
			end if;

			/* Rafael em 12/04/2007 classificacao TISS 2007 */
			select	obter_classif_tiss_agenda(nr_seq_agenda_p, 'C')
			into	ie_tipo_consulta_w
			from	dual;

			select	med_atendimento_seq.nextval
			into		nr_atendimento_w
			from		dual;
		
			insert into med_atendimento 
					(nr_atendimento,
					nr_seq_cliente,
					dt_atualizacao,
					nm_usuario,
					dt_entrada,
					cd_convenio,
					dt_saida,
					nr_atend_original,
					cd_usuario_convenio,
					dt_validade_carteira,
					nr_seq_agenda,
					dt_inicio_consulta,
					ie_grau_satisfacao,
					nr_seq_plano,
					ie_tipo_consulta)
			values	(nr_atendimento_w,
					nr_seq_cliente_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_convenio_w,
					null,
					null,
					cd_usuario_convenio_w,
					dt_validade_carteira_w,
					nr_seq_agenda_p,
					null,
					null,
					nr_seq_plano_w,
					ie_tipo_consulta_w);

			Atualizar_Convenio_Med_Cliente 
				(nr_seq_cliente_w,
				cd_convenio_w,
				cd_usuario_convenio_w,
				'S', nr_seq_plano_w);

			end;
		end if;

		end;

	end if;
	end;
end if;

if	(ie_chegada_consulta_p = 'E') then
	update		agenda_consulta a
	set		a.ie_status_agenda	= 'A'
	where		a.nr_sequencia		= nr_seq_agenda_p
/*	and		a.ie_status_agenda	in ('N', 'LF') Linha comentada e trocada pela abaixo por Oraci em 05/11/2007 OS72299 */
	and		a.ie_status_agenda	not in ('B', 'C', 'E', 'L')
	and		(exists	(	select 1 
					from	med_atendimento x
					where	x.nr_seq_agenda	= a.nr_sequencia) or
					(a.ie_classif_agenda	= 'M') or (a.nr_atendimento is not null));

elsif	(ie_chegada_consulta_p = 'C') then
	update		agenda_consulta a
	set		a.ie_status_agenda	= 'O'
	where		a.nr_sequencia		= nr_seq_agenda_p
	and		a.ie_status_agenda	= 'A'
	and		(exists	(select 1 
				from	med_atendimento x
				where	x.nr_seq_agenda	= a.nr_sequencia) or
			(a.ie_classif_agenda	= 'M'));
end if;

commit;

end Gerar_Med_Atendimento;
/
