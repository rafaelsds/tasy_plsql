create or replace procedure pls_carregar_regra_copar_contr
			(	nr_seq_contrato_p	number,
				nr_seq_plano_p		pls_plano.nr_sequencia%type,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_plano_w			number(10);
nr_seq_regra_coparticipacao_w	number(10);
nr_seq_tipo_coparticipacao_w	number(10);
qt_registros_w			number(10);
nr_seq_regra_retorno_w		number(10);
nr_seq_regra_copart_w		number(10);

-------------------------------------------------------------------------------------------
qt_eventos_minimo_w			pls_regra_coparticipacao.qt_eventos_minimo%type;
nr_seq_prestador_w			pls_regra_coparticipacao.nr_seq_prestador%type;
nr_seq_tipo_prestador_w			pls_regra_coparticipacao.nr_seq_tipo_prestador%type;
ie_tipo_data_consistencia_w		pls_regra_coparticipacao.ie_tipo_data_consistencia%type;
ie_forma_cobr_internacao_w		pls_regra_coparticipacao.ie_forma_cobr_internacao%type;
ie_titularidade_w			pls_regra_coparticipacao.ie_titularidade%type;
ie_tipo_parentesco_w			pls_regra_coparticipacao.ie_tipo_parentesco%type;
qt_ocorrencias_w			pls_regra_coparticipacao.qt_ocorrencias%type;
qt_ocorrencia_grupo_serv_w		pls_regra_coparticipacao.qt_ocorrencia_grupo_serv%type;
qt_periodo_ocor_w			pls_regra_coparticipacao.qt_periodo_ocor%type;
nr_seq_grupo_serv_w			pls_regra_coparticipacao.nr_seq_grupo_serv%type;
cd_procedimento_w			pls_regra_coparticipacao.cd_procedimento%type;
qt_idade_min_w				pls_regra_coparticipacao.qt_idade_min%type;
qt_idade_max_w				pls_regra_coparticipacao.qt_idade_max%type;
ie_incidencia_proc_mat_w		pls_regra_coparticipacao.ie_incidencia_proc_mat%type;
ie_considera_outra_ocor_w		pls_regra_coparticipacao.ie_considera_outra_ocor_regra%type;
ie_origem_proced_w			pls_regra_coparticipacao.ie_origem_proced%type;
qt_regra_exe_w				number(10);
dt_inicio_vigencia_w			pls_regra_copartic_exce.dt_inicio_vigencia%type;
dt_fim_vigencia_w			pls_regra_copartic_exce.dt_fim_vigencia%type;
nr_seq_grupo_servico_w			pls_regra_copartic_exce.nr_seq_grupo_servico%type;
nr_seq_grupo_prestador_w		pls_regra_copartic_exce.nr_seq_grupo_prestador%type;
ie_tipo_guia_w				pls_regra_copartic_exce.ie_tipo_guia%type;
nr_seq_saida_int_w			pls_regra_copartic_exce.nr_seq_saida_int%type;
nr_seq_saida_int_princ_w		pls_regra_copartic_exce.nr_seq_saida_int_princ%type;
ie_tipo_protocolo_w			pls_regra_copartic_exce.ie_tipo_protocolo%type;
dt_fim_vigencia_ww			pls_regra_copartic_exce.dt_fim_vigencia%type;
dt_inicio_vigencia_ww			pls_regra_copartic_exce.dt_inicio_vigencia%type;
dt_inicio_vigencia_prod_w		pls_contrato_plano.dt_inicio_vigencia%type;
ie_tipo_atendimento_w			pls_regra_coparticipacao.ie_tipo_atendimento%type;
dt_contrato_de_w			pls_regra_coparticipacao.dt_contrato_de%type;		
vl_base_min_w				pls_regra_coparticipacao.vl_base_min%type;
vl_base_max_w				pls_regra_coparticipacao.vl_base_max%type;
ie_tipo_ocorrencia_w			pls_regra_coparticipacao.ie_tipo_ocorrencia%type;
qt_diaria_inicial_w			pls_regra_coparticipacao.qt_diaria_inicial%type;
qt_diaria_final_w			pls_regra_coparticipacao.qt_diaria_final%type;
ie_incidencia_psiquiatria_w 		pls_regra_coparticipacao.ie_incidencia_psiquiatria%type;

Cursor C01 is
	select	nr_seq_plano,
		dt_inicio_vigencia
	from	pls_contrato_plano
	where	nr_seq_contrato	= nr_seq_contrato_p
	and     ((nr_seq_plano = nr_seq_plano_p) or (nr_seq_plano_p is null))
	and	ie_situacao	= 'A'
	group by 
		nr_seq_plano,
		dt_inicio_vigencia;

Cursor C02 is
	select	nr_sequencia,
		nr_seq_tipo_coparticipacao,
		nvl(qt_eventos_minimo,0),
		nvl(ie_tipo_data_consistencia,'N'),
		nvl(ie_forma_cobr_internacao,'C'),
		nvl(ie_titularidade,'A'),
		nvl(ie_tipo_parentesco,'X'),
		nvl(qt_ocorrencias,0),
		nvl(qt_ocorrencia_grupo_serv,0),
		nvl(qt_periodo_ocor,0),
		nvl(nr_seq_grupo_serv,0),
		nvl(cd_procedimento,0),
		nvl(qt_idade_min,0),
		nvl(qt_idade_max,0),
		nvl(nr_seq_prestador,0),
		nvl(nr_seq_tipo_prestador,0),
		nvl(ie_incidencia_proc_mat,0),
		nvl(ie_considera_outra_ocor_regra,'N'),
		nvl(ie_origem_proced, 1),
		trunc(dt_fim_vigencia,'dd'),
		trunc(dt_inicio_vigencia,'dd'),
		nvl(ie_tipo_atendimento, 'E'),
		trunc(dt_contrato_de, 'dd'),
		nvl(vl_base_min,0),
		nvl(vl_base_max,0),
		nvl(ie_tipo_ocorrencia,'X'),
		nvl(qt_diaria_inicial, 0),
		nvl(qt_diaria_final, 0),
		nvl(ie_incidencia_psiquiatria, 'S')
	from	pls_regra_coparticipacao
	where	nr_seq_plano	= nr_seq_plano_w
	and	dt_inicio_vigencia_prod_w between trunc(nvl(dt_inicio_vigencia,dt_inicio_vigencia_prod_w),'dd') and fim_dia(nvl(dt_fim_vigencia,dt_inicio_vigencia_prod_w))
	and	nr_seq_contrato is null
	and	nr_seq_proposta is null;

Cursor C03 is
	select	nr_sequencia
	from	pls_regra_copartic_retorno
	where	nr_seq_regra_copartic	= nr_seq_regra_coparticipacao_w;

Cursor C04 is
	select	nr_sequencia
	from	pls_regra_copartic_exce
	where	nr_seq_regra_copartic = nr_seq_regra_coparticipacao_w
	and	dt_inicio_vigencia_prod_w between trunc(nvl(dt_inicio_vigencia,dt_inicio_vigencia_prod_w),'dd') and fim_dia(nvl(dt_fim_vigencia,dt_inicio_vigencia_prod_w));

begin

open C01;
loop
fetch C01 into
	nr_seq_plano_w,
	dt_inicio_vigencia_prod_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into
		nr_seq_regra_coparticipacao_w,
		nr_seq_tipo_coparticipacao_w,
		qt_eventos_minimo_w,
		ie_tipo_data_consistencia_w,
		ie_forma_cobr_internacao_w,
		ie_titularidade_w,
		ie_tipo_parentesco_w,
		qt_ocorrencias_w,
		qt_ocorrencia_grupo_serv_w,
		qt_periodo_ocor_w,
		nr_seq_grupo_serv_w,
		cd_procedimento_w,
		qt_idade_min_w,
		qt_idade_max_w,
		nr_seq_prestador_w,
		nr_seq_tipo_prestador_w,
		ie_incidencia_proc_mat_w,
		ie_considera_outra_ocor_w,
		ie_origem_proced_w,
		dt_fim_vigencia_ww,
		dt_inicio_vigencia_ww,
		ie_tipo_atendimento_w,
		dt_contrato_de_w,
		vl_base_min_w,
		vl_base_max_w,
		ie_tipo_ocorrencia_w,
		qt_diaria_inicial_w,
		qt_diaria_final_w,
		ie_incidencia_psiquiatria_w;
	exit when C02%notfound;
		begin
		select	count(1)
		into	qt_registros_w
		from	pls_regra_coparticipacao
		where	nr_seq_contrato				= nr_seq_contrato_p
		and	nr_seq_tipo_coparticipacao		= nr_seq_tipo_coparticipacao_w
		and	nr_seq_plano				= nr_seq_plano_w
		and	nvl(qt_eventos_minimo,0)		= qt_eventos_minimo_w
		and	nvl(ie_tipo_data_consistencia,'N')	= ie_tipo_data_consistencia_w
		and	nvl(ie_forma_cobr_internacao,'C')	= ie_forma_cobr_internacao_w
		and	nvl(ie_titularidade,'A')		= ie_titularidade_w
		and	nvl(ie_tipo_parentesco,'X')		= ie_tipo_parentesco_w
		and	nvl(qt_ocorrencias,0)			= qt_ocorrencias_w
		and	nvl(qt_ocorrencia_grupo_serv,0)		= qt_ocorrencia_grupo_serv_w
		and	nvl(qt_periodo_ocor,0)			= qt_periodo_ocor_w
		and	nvl(nr_seq_grupo_serv,0)		= nr_seq_grupo_serv_w
		and	nvl(cd_procedimento,0)			= cd_procedimento_w
		and	nvl(qt_idade_min,0)			= qt_idade_min_w
		and	nvl(qt_idade_max,0)			= qt_idade_max_w
		and	nvl(nr_seq_prestador,0)			= nr_seq_prestador_w
		and	nvl(nr_seq_tipo_prestador,0)		= nr_seq_tipo_prestador_w
		and	nvl(ie_incidencia_proc_mat,0)		= ie_incidencia_proc_mat_w
		and	nvl(ie_considera_outra_ocor_regra, 'N') = ie_considera_outra_ocor_w
		and	nvl(ie_origem_proced, 1)		= ie_origem_proced_w
		and	nvl(ie_tipo_atendimento, 'E')		= ie_tipo_atendimento_w
		and	trunc(dt_contrato_de, 'dd')		= dt_contrato_de_w
		and	nvl(vl_base_min,0)			= vl_base_min_w
		and	nvl(vl_base_max,0)			= vl_base_max_w
		and	nvl(ie_tipo_ocorrencia,'X')		= ie_tipo_ocorrencia_w
		and	nvl(qt_diaria_inicial,0)		= qt_diaria_inicial_w
		and	nvl(qt_diaria_final,0) 			= qt_diaria_final_w
		and	nvl(ie_incidencia_psiquiatria,'S')	= ie_incidencia_psiquiatria_w
		and	rownum					= 1;
		
		if	(qt_registros_w = 0) then
			select	pls_regra_coparticipacao_seq.nextval
			into	nr_seq_regra_copart_w
			from	dual;
			
			insert	into	pls_regra_coparticipacao
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_tipo_coparticipacao,qt_eventos_minimo,qt_meses_intervalo,ie_tipo_data_consistencia,ie_tipo_atendimento,
					ie_prestador_cooperado,dt_inicio_vigencia,dt_fim_vigencia,dt_contrato_de,dt_contrato_ate,qt_idade_min,
					qt_idade_max,ie_titularidade,ie_tipo_parentesco,qt_ocorrencias,ie_tipo_ocorrencia,
					qt_diaria_inicial,qt_diaria_final,qt_ocorrencia_grupo_serv,qt_periodo_ocor,ie_tipo_periodo_ocor,
					nr_seq_grupo_serv,cd_procedimento,tx_coparticipacao,vl_maximo,vl_coparticipacao,
					ie_reajuste,nr_seq_contrato,ie_situacao, nr_seq_plano,ie_incidencia_valor_maximo,
					nr_seq_prestador, nr_seq_tipo_prestador,ie_periodo_valor_maximo,ie_forma_cobr_internacao,ie_ano_calendario_outras_ocor,
					ie_tipo_incidencia, ie_incidencia_proc_mat, ie_incidencia_valor_fixo,
					ie_considera_outra_ocor_regra, ie_origem_proced, cd_sistema_anterior,               
					ie_incide_vl_fixo_cta, ie_inc_demais_itens, vl_base_min,
					vl_base_max, ie_incidencia_psiquiatria)
				(select	nr_seq_regra_copart_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_tipo_coparticipacao,qt_eventos_minimo,qt_meses_intervalo,ie_tipo_data_consistencia,ie_tipo_atendimento,
					ie_prestador_cooperado,trunc(dt_inicio_vigencia_prod_w,'dd'),null,dt_contrato_de,dt_contrato_ate,qt_idade_min,
					qt_idade_max,ie_titularidade,ie_tipo_parentesco,qt_ocorrencias,ie_tipo_ocorrencia,
					qt_diaria_inicial,qt_diaria_final,qt_ocorrencia_grupo_serv,qt_periodo_ocor,ie_tipo_periodo_ocor,
					nr_seq_grupo_serv,cd_procedimento,tx_coparticipacao,vl_maximo,vl_coparticipacao,
					ie_reajuste,nr_seq_contrato_p,'A', nr_seq_plano_w,ie_incidencia_valor_maximo,
					nr_seq_prestador, nr_seq_tipo_prestador,ie_periodo_valor_maximo,ie_forma_cobr_internacao,ie_ano_calendario_outras_ocor,
					nvl(ie_tipo_incidencia,'B'),ie_incidencia_proc_mat, ie_incidencia_valor_fixo,
					ie_considera_outra_ocor_regra, ie_origem_proced, cd_sistema_anterior,               
					ie_incide_vl_fixo_cta, ie_inc_demais_itens, vl_base_min,
					vl_base_max, ie_incidencia_psiquiatria
				from	pls_regra_coparticipacao
				where	nr_sequencia	= nr_seq_regra_coparticipacao_w);
			
			select	count(1)
			into	qt_regra_exe_w
			from	pls_regra_copartic_exce
			where	nr_seq_regra_copartic = nr_seq_regra_coparticipacao_w;
			
			if	(qt_regra_exe_w > 0) then
			
				pls_copiar_exce_copartic( nr_seq_regra_coparticipacao_w, nr_seq_regra_copart_w, dt_inicio_vigencia_prod_w, nm_usuario_p);
			
			end if;
			
			open C03;
			loop
			fetch C03 into
				nr_seq_regra_retorno_w;
			exit when C03%notfound;
				begin
				insert	into	pls_regra_copartic_retorno
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						nr_seq_regra_copartic, qt_dias_retorno)
					(select	pls_regra_copartic_retorno_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
						nr_seq_regra_copart_w, qt_dias_retorno
					from	pls_regra_copartic_retorno
					where	nr_sequencia	= nr_seq_regra_retorno_w);
				end;
			end loop;
			close C03;
		end if;
		end; 
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

end pls_carregar_regra_copar_contr;
/
