create or replace
procedure gerar_consulta_oft_atend(	nr_atendimento_p		number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number) is 
	
cd_medico_w		varchar2(10);
nr_sequencia_w		number(10,0);
nr_seq_status_w		number(10,0);
cd_pessoa_fisica_w	varchar2(10);
ie_possui_consulta_w	varchar2(1);
nr_seq_classificacao_w	atendimento_paciente.nr_seq_classificacao%type;
nr_seq_tipo_consulta_w	classificacao_atendimento.nr_seq_tipo_consulta%type;
	
begin

obter_param_usuario(3010, 18, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, nr_seq_status_w);

if	(nvl(nr_atendimento_p,0) > 0) then
	select	nvl(max('S'),'N')
	into	ie_possui_consulta_w
	from	oft_consulta
	where	nr_atendimento = nr_atendimento_p
	and	dt_cancelamento is null;
	
	if	(ie_possui_consulta_w = 'N') then
		select	max(cd_medico_resp),
			max(cd_pessoa_fisica)
		into	cd_medico_w,
			cd_pessoa_fisica_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p;
			
		consiste_geracao_oftalmologia(nr_atendimento_p,nm_usuario_p,cd_estabelecimento_p);	
	
		if (nr_atendimento_p is not null) and (nr_atendimento_p > 0) then
			select  max(nr_seq_classificacao)
			into	nr_seq_classificacao_w
			from    atendimento_paciente
			where   nr_atendimento = nr_atendimento_p
			and	cd_estabelecimento = cd_estabelecimento_p;
			
			if (nr_seq_classificacao_w is not null) then
				select max(nr_seq_tipo_consulta)
				into   nr_seq_tipo_consulta_w	
				from   classificacao_atendimento
				where  nr_sequencia = nr_seq_classificacao_w
				and    nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
				and    nvl(ie_situacao,'A') = 'A';
			end if;
		end if;

	
		select	oft_consulta_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into oft_consulta(
			nr_sequencia,           
			dt_atualizacao,         
			nm_usuario,             
			dt_atualizacao_nrec,    
			nm_usuario_nrec,        
			nr_atendimento,         
			nr_atend_consultorio,   
			dt_consulta,            
			cd_medico,              
			dt_fim_consulta,
			nr_seq_status,
			nr_seq_tipo_consulta,
			cd_pessoa_fisica)
		values(
			nr_sequencia_w,
			sysdate,         
			nm_usuario_p,
			sysdate,    
			nm_usuario_p,
			nr_atendimento_p,
			null,
			sysdate,
			cd_medico_w,
			null,
			nr_seq_status_w,
			nr_seq_tipo_consulta_w,
			cd_pessoa_fisica_w);
		commit;
	
		update	atendimento_paciente
		set	nr_seq_oftalmo	=	nr_sequencia_w
		where	nr_atendimento	=	nr_atendimento_p;		
		commit;
	
	end if;
end if;

end gerar_consulta_oft_atend;
/