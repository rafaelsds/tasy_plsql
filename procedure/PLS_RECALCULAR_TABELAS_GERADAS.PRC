create or replace
procedure pls_recalcular_tabelas_geradas
			(	nr_seq_simul_perfil_p	number,
				nm_usuario_p		Varchar2) is

nr_seq_simul_plano_w		number(10);
qt_idade_inicial_w		number(10);
qt_idade_final_w		number(10);
qt_beneficiario_w		number(10);
nr_seq_tabela_w			number(10);
vl_preco_faixa_etaria_w		number(10,2);
nr_seq_tabela_ww		number(10);
vl_tot_preco_faixa_w		number(15,2) := 0;
vl_preco_faixa_w		number(10,2) := 0;
qt_total_beneficiarios_w	number(10) := 0;
qt_vidas_w			number(10) := 0;
tx_indice_perfil_w		number(7,4);
vl_preco_original_w		number(10,2) := 0;
vl_preco_tot_original_w		number(10,2) := 0;

Cursor C01 is
	select	qt_idade_inicial,
		qt_idade_final,
		qt_beneficiario
	from	pls_simulpreco_coletivo
	where	nr_seq_simul_perfil	= nr_seq_simul_perfil_p;

Cursor C02 is
	select	b.nr_sequencia
	from	pls_simulacao_plano b,
		pls_simulacao_perfil a
	where	b.nr_seq_simul_perfil	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_simul_perfil_p
	and	b.qt_idade_inicial	= qt_idade_inicial_w
	and	b.qt_idade_final	= qt_idade_final_w;

Cursor C03 is
	select	b.nr_seq_tabela
	from	pls_simulacao_plano b,
		pls_simulacao_perfil a
	where	b.nr_seq_simul_perfil	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_simul_perfil_p
	group by b.nr_seq_tabela;

Cursor C04 is
	select	b.nr_sequencia
	from	pls_simulacao_plano b,
		pls_simulacao_perfil a
	where	b.nr_seq_simul_perfil	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_simul_perfil_p
	and	b.nr_seq_tabela		= nr_seq_tabela_ww
	and	b.qt_idade_final	<> 0;

begin

select	max(tx_indice)
into	tx_indice_perfil_w
from	pls_simulacao_perfil
where	nr_sequencia		= nr_seq_simul_perfil_p;

if	(tx_indice_perfil_w is null) then
	tx_indice_perfil_w	:= 1;
end if;	

open C01;
loop
fetch C01 into
	qt_idade_inicial_w,
	qt_idade_final_w,
	qt_beneficiario_w;
exit when C01%notfound;
	begin

	open C02;
	loop
	fetch C02 into
		nr_seq_simul_plano_w;
	exit when C02%notfound;
		begin

		select	max(nr_seq_tabela)
		into	nr_seq_tabela_w
		from	pls_simulacao_plano
		where	nr_sequencia	= nr_seq_simul_plano_w;

		if	(nr_seq_tabela_w is not null) then
			begin
			select	max(vl_preco_atual)
			into	vl_preco_faixa_etaria_w
			from	pls_plano_preco
			where	nr_seq_tabela		= nr_seq_tabela_w
			and	qt_idade_inicial	= qt_idade_inicial_w
			and	qt_idade_final		= qt_idade_final_w;
			exception
			when others then
				vl_preco_faixa_etaria_w	:= 0;
			end;

			if	(vl_preco_faixa_etaria_w is null) then
				vl_preco_faixa_etaria_w	:= 0;
			end if;
			
			vl_preco_original_w	:= vl_preco_original_w;
			
			vl_preco_faixa_etaria_w	:= vl_preco_faixa_etaria_w * tx_indice_perfil_w;

			update	pls_simulacao_plano
			set	vl_preco_faixa		= vl_preco_faixa_etaria_w,
				vl_preco_original	= vl_preco_original_w,
				qt_vidas		= qt_beneficiario_w
			where	nr_sequencia		= nr_seq_simul_plano_w;
		end if;
		end;
	end loop;
	close C02;
	qt_total_beneficiarios_w	:= qt_total_beneficiarios_w + qt_beneficiario_w;
	end;
end loop;
close C01;

open C03;
loop
fetch C03 into
	nr_seq_tabela_ww;
exit when C03%notfound;
	begin
	vl_tot_preco_faixa_w	:= 0;
	vl_preco_tot_original_w	:= 0;
	open C04;
	loop
	fetch C04 into
		nr_seq_simul_plano_w;
	exit when C04%notfound;
		begin

		begin
		select	vl_preco_faixa,
			qt_vidas,
			vl_preco_original
		into	vl_preco_faixa_w,
			qt_vidas_w,
			vl_preco_original_w
		from	pls_simulacao_plano
		where	nr_sequencia	= nr_seq_simul_plano_w;
		exception
		when others then
			vl_preco_faixa_w	:= 0;
			qt_vidas_w		:= 0;
			vl_preco_original_w	:= 0;
		end;

		if	(vl_preco_faixa_w is null) then
			vl_preco_faixa_w	:= 0;
		end if;
		
		if	(qt_vidas_w is null) then
			qt_vidas_w	:= 0;
		end if;	
		
		vl_tot_preco_faixa_w	:= vl_tot_preco_faixa_w + (vl_preco_faixa_w  * qt_vidas_w);
		vl_preco_tot_original_w	:= vl_preco_tot_original_w + vl_preco_original_w;

		end;
	end loop;
	close C04;

	update	pls_simulacao_plano
	set	vl_preco_faixa		= vl_tot_preco_faixa_w,
		vl_preco_original	= vl_preco_tot_original_w
	where	nr_seq_simul_perfil	= nr_seq_simul_perfil_p
	and	nr_seq_tabela		= nr_seq_tabela_ww
	and	qt_idade_inicial	= 0
	and	qt_idade_final		= 0;

	end;
end loop;
close C03;

update	pls_simulacao_plano
set	qt_vidas			= qt_total_beneficiarios_w
where	nr_seq_simul_perfil		= nr_seq_simul_perfil_p
and	qt_idade_inicial		= 0
and	qt_idade_final			= 0;

commit;

end pls_recalcular_tabelas_geradas;
/
