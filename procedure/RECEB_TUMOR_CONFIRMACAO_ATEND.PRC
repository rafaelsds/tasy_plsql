create or replace
procedure receb_tumor_confirmacao_atend(nr_sequencia_p number) is 

ds_mensagem_w clob;
ds_json_w 			philips_json;
ds_mensagem_json_w 	philips_json;
ds_lugar_w 			philips_json;
nr_seq_documento_w	intpd_fila_transmissao.nr_seq_documento%type;
ds_erro_w 			varchar2(255);
nr_atendimento_w	varchar2(255);
nr_prontuario_w		varchar2(255);
nr_atend_w			atendimento_paciente.nr_atendimento%type;
nr_pront_w			pessoa_fisica.nr_prontuario%type;
begin
select	ds_message,
		nr_seq_documento
into	ds_mensagem_w,
		nr_seq_documento_w
from	intpd_fila_transmissao a
where	a.nr_sequencia = nr_sequencia_p;


ds_json_w := philips_json(ds_mensagem_w);
ds_mensagem_json_w := philips_json(ds_json_w.get('mensagem'));
nr_atendimento_w := ds_mensagem_json_w.get('atendimento').get_string();
nr_prontuario_w := ds_mensagem_json_w.get('prontuario').get_string();


if (nr_atendimento_w is not null and nr_prontuario_w is not null) then
			
	begin
		select 	a.nr_atendimento,
				b.nr_prontuario
		into	nr_atend_w,
				nr_pront_w
		from	atendimento_paciente a,
				pessoa_fisica b
		where 	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
		and     a.nr_atendimento 	= nr_atendimento_w
		and     b.nr_prontuario 	= nr_prontuario_w;
				
		update 	intpd_fila_transmissao	
		set		ie_status_http = 200,
				ds_message_response =		'{ "Mensagem" : {'
										||	'"DataHora": "'  || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
										||	'"Status": "Sucesso",'
										||	'"Atendimento": "'||nr_atend_w||'",'
										||	'"Prontuario": "' ||nr_pront_w||'" '
										||	'}}',
				ie_status = 'S'
		where 	nr_sequencia = nr_sequencia_p;		
		
	exception
	when others then
		if (ds_erro_w is null) then
			ds_erro_w := DBMS_UTILITY.FORMAT_ERROR_STACK();
		end if;
		update 	intpd_fila_transmissao	
		set		ie_status_http = 400,
				ds_message_response =	'{ "Mensagem" : {' 
									||	'"DataHora": "' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
									||	'"Status": "Erro",'
									||	'"Descricao": "' || ds_erro_w|| '"}}',
				ie_status = 'E'
		where 	nr_sequencia = nr_sequencia_p;			
	end;
else
		
		update 	intpd_fila_transmissao	
		set		ie_status_http = 400,
				ds_message_response =	'{ "Mensagem" : { ' 
									||	'"DataHora": "' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
									||	'"Status": "Erro",'
									||	'"Descricao": " Favor informar todos os par�metros(Atendimento + Prontu�rio) "}}',
				ie_status = 'E'
		where 	nr_sequencia = nr_sequencia_p;		
end if;

commit;

end receb_tumor_confirmacao_atend;
/