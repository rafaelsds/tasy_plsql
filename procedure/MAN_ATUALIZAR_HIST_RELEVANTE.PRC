create or replace
procedure man_atualizar_hist_relevante(		nr_sequencia_p		number, 
					ie_relevante_teste_p	varchar2) is		
begin
if 	(nr_sequencia_p is not null) and
	(ie_relevante_teste_p is not null) then
	begin
	update	man_ordem_serv_tecnico
	set	ie_relevante_teste	= ie_relevante_teste_p
	where	nr_sequencia	= nr_sequencia_p;
	commit;
	end;
end if;
end man_atualizar_hist_relevante;
/