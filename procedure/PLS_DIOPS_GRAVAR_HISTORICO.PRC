create or replace
procedure pls_diops_gravar_historico
			(	nr_seq_periodo_p		Integer,
				ie_tipo_historico_p		Integer,
				ds_observacao_p			Varchar,
				ds_parametro_p 			Varchar,
				nm_usuario_p			Varchar2) is

begin

insert into diops_historico
	(nr_sequencia, nr_seq_periodo, ie_tipo_historico,
	dt_historico, dt_atualizacao, nm_usuario,
	dt_atualizacao_nrec, nm_usuario_nrec, ds_observacao)
values	(diops_historico_seq.nextval, nr_seq_periodo_p, ie_tipo_historico_p,
	sysdate, sysdate, nm_usuario_p,
	sysdate, nm_usuario_p, ds_observacao_p);

end pls_diops_gravar_historico;
/