create or replace
procedure GERAR_W_IMP_FATURA
			(nr_seq_lote_p	number,
			ds_conteudo_p	varchar2,
	 		nm_usuario_p	varchar2) is

nr_sequencia_w	w_imp_fatura.nr_sequencia%type;

begin

select	nvl(max(a.nr_sequencia),0) + 1
into	nr_sequencia_w
from	w_imp_fatura a
where	a.nr_seq_lote	= nr_seq_lote_p;

insert	into w_imp_fatura
	(ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_sequencia,
	nr_seq_lote)
values	(ds_conteudo_p,
	sysdate,
	nm_usuario_p,
	nr_sequencia_w,
	nr_seq_lote_p);

commit;

end GERAR_W_IMP_FATURA;
/