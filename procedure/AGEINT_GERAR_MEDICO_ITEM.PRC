create or replace
procedure Ageint_Gerar_Medico_Item(
			nr_seq_ageint_p		number,
			nr_seq_item_p		number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number) is 

nr_seq_item_w		number(10);			
nr_seq_proc_interno_w	number(10);
cd_especialidade_w	number(5);
cd_medico_w		varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_idade_w		number(3);
cd_paciente_w		varchar2(10);
dt_nascimento_w		date;
ie_sexo_w		varchar2(1);
cd_medico_item_w	varchar2(10);
cd_estabelecimento_w	number(5);
ie_estab_usuario_w	varchar2(1);
cd_estab_agenda_w	number(5);
ie_perm_agenda_w	varchar2(1)	:= 'S';
qt_estab_user_w		number(10);
cd_agenda_w		number(10,0);
nr_seq_agrupamento_w	number(10,0);
qt_agrupamento_w	number(10,0);
nr_seq_agenda_int_w	number(10);
qt_reg_w		number(10);
ie_rodizio_especialidade_w	agenda_integrada_item.ie_rodizio_especialidade%type;

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_proc_interno,
		a.cd_especialidade,
		a.cd_medico,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_seq_agrupamento,
		nvl(a.ie_rodizio_especialidade, 'N')
	from	agenda_integrada_item a
	where	a.nr_seq_agenda_int	= nr_seq_Ageint_p
	--and	a.nr_sequencia		= nvl(nr_seq_item_p, a.nr_sequencia)
	and	a.ie_tipo_agendamento	= 'E'
	--and	not exists (select 1 from ageint_medico_item b where b.nr_seq_item = a.nr_sequencia)
	order by 1;
	
Cursor C02 is
	select	a.cd_medico,
		c.cd_estabelecimento,
		c.cd_agenda
	from    agenda c,
		pessoa_fisica b,
		agenda_medico a 
	where   Obter_Se_Ageint_Item(a.cd_agenda, nr_Seq_Ageint_p, c.cd_estabelecimento, nr_seq_proc_interno_w, cd_Especialidade_w, cd_medico_w, a.cd_medico, cd_procedimento_w, ie_origem_proced_w,nr_seq_item_p, qt_idade_w, ie_rodizio_especialidade_w) = 'S'
	and     a.cd_medico    			= b.cd_pessoa_fisica
	and	c.cd_agenda			= a.cd_agenda
	and	nvl(c.ie_agenda_integrada,'N')	= 'S'
	and     a.ie_situacao  			= 'A'
	and	c.ie_situacao			= 'A'
	and	c.cd_tipo_Agenda = 2
	and	((qt_idade_w	>= nvl(qt_idade_min,0)
	and	qt_idade_w	<= nvl(qt_idade_max,999))
	or	qt_idade_w = 0);
			
begin

ie_estab_usuario_w	:= nvl(Obter_Valor_Param_Usuario(869, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S');

ageint_excluir_medico_item(nr_seq_ageint_p, nm_usuario_p);

select	max(cd_pessoa_fisica),
	max(cd_estabelecimento)
into	cd_paciente_w,
	cd_estabelecimento_w
from	agenda_integrada
where	nr_sequencia	= nr_seq_Ageint_p;

if	(cd_paciente_w is not null) then
	select	max(ie_Sexo),
		max(dt_nascimento)
	into	ie_Sexo_w,
		dt_nascimento_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_paciente_w;
end if;

qt_idade_w	:= obter_idade(dt_nascimento_w, sysdate, 'A');


open C01;
loop
fetch C01 into	
	nr_seq_item_w,
	nr_seq_proc_interno_w,
	cd_especialidade_w,
	cd_medico_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_agrupamento_w,
	ie_rodizio_especialidade_w;
exit when C01%notfound;
	begin
	if (nr_seq_item_w = nvl(nr_seq_item_p, nr_seq_item_w)) then
		select 	nvl(max(1),0)
		into	qt_reg_w
		from 	ageint_medico_item b 
		where 	b.nr_seq_item = nr_seq_item_w;
		if (qt_reg_w = 0) then
			open C02;
			loop
			fetch C02 into	
				cd_medico_item_w,
				cd_estab_agenda_w,
				cd_agenda_w;
			exit when C02%notfound;
				begin
				ie_perm_agenda_w	:= 'S';
				if	(ie_estab_usuario_w	= 'S') or 
					(ie_estab_usuario_w	= 'C') then
					if	(cd_estab_agenda_w	<> cd_estabelecimento_p) then
						ie_perm_agenda_w	:= 'N';
					end if;
				elsif	(ie_estab_usuario_w	= 'N') or
					(ie_estab_usuario_w	= 'T') then
					select	count(*)
					into	qt_estab_user_w
					from	usuario_estabelecimento
					where	nm_usuario_param	= nm_usuario_p
					and	cd_estabelecimento	= cd_estab_agenda_w;
					if	(qt_estab_user_w	= 0) then
						ie_perm_agenda_w	:= 'N';
					end if;
				end if; 
				
				if	(nr_seq_agrupamento_w is not null) then
					select	count(*)
					into	qt_agrupamento_w
					from	agenda
					where	cd_agenda = cd_agenda_w
					and	nr_seq_agrupamento = nr_seq_agrupamento_w;					
					if	(qt_agrupamento_w = 0) then
						select	count(*)
						into	qt_agrupamento_w
						from	agenda_agrupamento
						where	cd_agenda = cd_agenda_w
						and	nr_seq_agrupamento = nr_seq_agrupamento_w;
						if	(qt_agrupamento_w = 0) then
							ie_perm_agenda_w := 'N';
						end if;
					end if;
				end if;
				
				if	(ie_perm_agenda_w	= 'S') then
					insert into ageint_medico_item
						(nr_sequencia,
						nr_seq_item,
						cd_pessoa_fisica,
						nm_usuario,
						dt_Atualizacao,
						nm_usuario_nrec,
						dt_Atualizacao_nrec)
					values
						(ageint_medico_item_seq.nextval,
						nr_seq_item_w,
						cd_medico_item_w,
						nm_usuario_p,
						sysdate,
						nm_usuario_p, 
						sysdate); 
				end if;
				
				end;
			end loop;
			close C02;
		end if;
	end if;
	end;
end loop;
close C01;

commit;

end Ageint_Gerar_Medico_Item;
/