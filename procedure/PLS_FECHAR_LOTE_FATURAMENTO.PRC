create or replace
procedure pls_fechar_lote_faturamento(	nr_seq_lote_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		varchar2) is
					
/*ie_opcao_p
'F ' -> Fechar lote
'D' -> Desfazer fechamento lote
*/

ie_novo_pos_estab_w		pls_visible_false.ie_novo_pos_estab%type := 'N';
dt_referencia_w			date;
dt_referencia_month_w           date;
dt_ref_inicial_w                date;
dt_ref_final_w                  date;
ie_data_rec_faturamento_w	pls_parametro_contabil.ie_data_rec_faturamento%type;
cd_estabelecimento_w		number(4);
nr_seq_fatura_w			number(10);
nr_seq_atualizacao_w		number(10);
qt_movimento_w			number(10) := 0;
qt_fatura_proc_w		number(10);
qt_fatura_mat_w			number(10);
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;

Cursor c_contas_faturamento is
	select	b.nr_sequencia
	from	pls_fatura		b,
		pls_lote_faturamento	a
	where	b.nr_seq_lote		= a.nr_sequencia
	and	b.nr_seq_cancel_fat is null
	and	nvl(a.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	nvl(b.nr_lote_contabil,0) = 0
	and	a.nr_sequencia = nr_seq_lote_p
	union
	select	b.nr_sequencia
	from	pls_fatura		b,
		pls_lote_faturamento	a
	where	b.nr_seq_lote		= a.nr_sequencia
	and	b.nr_seq_cancel_fat is null
	and	nvl(a.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	nvl(b.nr_lote_contabil,0) = 0
	and	a.nr_sequencia = nr_seq_lote_p;

begin

select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from 	pls_visible_false;

if	(nr_seq_lote_p is not null) then
	select	nvl(max(ie_novo_pos_estab),'N')
	into	ie_novo_pos_estab_w
	from	pls_visible_false;
	
	if	(ie_novo_pos_estab_w = 'S') then
		if	(ie_opcao_p = 'F') then
			pls_faturamento_pck.fechar_lote_faturamento(nr_seq_lote_p, null, nm_usuario_p);
			
		elsif	(ie_opcao_p = 'D') then
			pls_faturamento_pck.desfazer_fechar_lote_fat(nr_seq_lote_p, null, nm_usuario_p);
		end if;
	else
		if	(ie_opcao_p = 'F') then
			update	pls_lote_faturamento
			set	dt_fechamento 	= sysdate,
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia 	= nr_seq_lote_p;
			
		elsif	(ie_opcao_p = 'D') then		
			update	pls_lote_faturamento
			set	dt_fechamento 	= null,
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia 	= nr_seq_lote_p;
		end if;
	end if;
	
	commit;
end if;

if (ie_opcao_p = 'F') then

	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	pls_lote_faturamento a
	where	a.nr_sequencia	= nr_seq_lote_p;

	select	count(*)
	into	qt_fatura_proc_w
	from	pls_conta 			a,
		pls_conta_pos_estabelecido 	b,
		pls_conta_pos_estab_contab 	c,
		pls_fatura 			d,
		pls_fatura_proc 		e,
		pls_fatura_conta 		f,
		pls_fatura_evento 		g,
		pls_lote_faturamento 		h,
		pls_plano			i,
		pls_segurado			j
	where	a.nr_sequencia 		= b.nr_seq_conta
	and	b.nr_sequencia 		= c.nr_seq_conta_pos
	and	b.nr_sequencia 		= e.nr_seq_conta_pos_estab
	and	d.nr_sequencia 		= g.nr_seq_fatura
	and	f.nr_sequencia 		= e.nr_seq_fatura_conta
	and	g.nr_sequencia 		= f.nr_seq_fatura_evento
	and	h.nr_sequencia 		= d.nr_seq_lote
	and	i.nr_sequencia 		= a.nr_seq_plano
	and	j.nr_sequencia		= a.nr_seq_segurado
	and	a.nr_seq_plano is not null
	and	d.nr_seq_cancel_fat is null
	and	nvl(h.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	h.nr_sequencia		= nr_seq_lote_p;

	select	count(*)
	into	qt_fatura_mat_w
	from	pls_conta 			a,
		pls_conta_pos_estabelecido 	b,
		pls_conta_pos_estab_contab 	c,
		pls_fatura 			d,
		pls_fatura_mat 			e,
		pls_fatura_conta 		f,
		pls_fatura_evento 		g,
		pls_lote_faturamento 		h,
		pls_plano			i,
		pls_segurado			j
	where	a.nr_sequencia 		= b.nr_seq_conta
	and	b.nr_sequencia 		= c.nr_seq_conta_pos
	and	b.nr_sequencia 		= e.nr_seq_conta_pos_estab
	and	d.nr_sequencia 		= g.nr_seq_fatura
	and	f.nr_sequencia 		= e.nr_seq_fatura_conta
	and	g.nr_sequencia 		= f.nr_seq_fatura_evento
	and	h.nr_sequencia 		= d.nr_seq_lote
	and	i.nr_sequencia 		= a.nr_seq_plano
	and	j.nr_sequencia		= a.nr_seq_segurado
	and	a.nr_seq_plano is not null
	and	d.nr_seq_cancel_fat is null
	and	nvl(h.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	h.nr_sequencia		= nr_seq_lote_p;

	qt_movimento_w	:= qt_fatura_proc_w + qt_fatura_mat_w;

	select	nvl(max(a.ie_data_rec_faturamento),'MC')
	into	ie_data_rec_faturamento_w
	from	pls_parametro_contabil	a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

	select	trunc(a.dt_mesano_referencia,'month')
	into	dt_referencia_w
	from	pls_lote_faturamento	a
	where	a.nr_sequencia	= nr_seq_lote_p;

	dt_referencia_month_w	:= trunc(dt_referencia_w,'month');

	dt_ref_inicial_w	:= dt_referencia_month_w;
	dt_ref_final_w		:= fim_dia(fim_mes(dt_referencia_month_w));


	if	(ie_concil_contab_w = 'N') then
		pls_gerar_atualizacao_contabil(	dt_referencia_w,
						nm_usuario_p,
						cd_estabelecimento_w,
						44,
						'G',
						qt_movimento_w,
						nr_seq_atualizacao_w);

		open c_contas_faturamento;
		loop
		fetch c_contas_faturamento into
			nr_seq_fatura_w;
		exit when c_contas_faturamento%notfound;
			begin
										
			ctb_pls_atualizar_faturamento(	nr_seq_fatura_w,
							null,
							null,
							nr_seq_atualizacao_w,
							nm_usuario_p,
							cd_estabelecimento_w);
										
			commit;

			pls_atualizar_tributo_fat(	nm_usuario_p,
							cd_estabelecimento_w,
							nr_seq_fatura_w,
							nr_seq_atualizacao_w);


			commit;

			end;
		end loop;
		close c_contas_faturamento;

		pls_gerar_atualizacao_contabil(	dt_referencia_w,
						nm_usuario_p,
						cd_estabelecimento_w,
						44,
						'A',
						qt_movimento_w,
						nr_seq_atualizacao_w);
	end if;
end if;


end pls_fechar_lote_faturamento;
/