create or replace
procedure san_desfazer_liberacao_exame( nr_seq_exame_lote_p	number,      
					nr_seq_exame_p		number,           
					nm_usuario_p		Varchar2) is 

begin

update 	san_exame_realizado
set	dt_liberacao		= null,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	nm_usuario_lib		= null
where	nr_seq_exame_lote	= nr_seq_exame_lote_p
and	nr_seq_exame		= nr_seq_exame_p;

commit;

end san_desfazer_liberacao_exame;
/