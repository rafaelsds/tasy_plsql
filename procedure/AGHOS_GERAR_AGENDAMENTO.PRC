create or replace
procedure Aghos_gerar_agendamento(nr_internacao_p	number,
				dt_agendamento_p	date,
				ie_tipo_acomodacao_p	number,
				nm_usuario_p		varchar2) is

cd_cnes_solicitante_w		varchar2(1000);
ds_sep_bv_w			varchar2(50);
ds_comando_w			varchar2(2000);
ie_tipo_acomodacao_w		number(10);
ds_erro_ww			varchar2(500);
ds_usuario_w			varchar2(15);

begin
/*  
IDF_GSH_I_INTERN_AGENDA_ENT 	NUMBER 		not null
TIP_STATUS 			VARCHAR2 1 	not null
DES_ERRO 			VARCHAR2 500 	null
DAT_INTEGRACAO 		DATE 		not null
DES_USUARIO 			VARCHAR2 15 	not null
COD_CNES_SOLICITANTE 		NUMBER 7 	not null
IDF_INTERNACAO 		NUMBER 9 	not null
DAT_AGENDAMENTO 		DATE 		not null
IDF_TIPO_ACOMODACAO_LEITO 	NUMBER 2	null
*/

select	max(cd_cnes_solicitante),
	max(ds_usuario)
into	cd_cnes_solicitante_w,
	ds_usuario_w
from	(select	substr(obter_cnes_estab(a.cd_estabelecimento), 1, 7) cd_cnes_solicitante,
		nvl(substr(obter_dados_param_atend(a.cd_estabelecimento, 'US'), 1, 15), 'TREINA') ds_usuario
	from	atendimento_paciente a,
		solicitacao_tasy_aghos b
	where	((b.nr_atend_original = a.nr_atendimento) or (b.nr_atendimento = a.nr_atendimento))
	and	b.nr_internacao = nr_internacao_p
	union
	select	substr(obter_cnes_estab(1), 1, 7),
		nvl(substr(obter_dados_param_atend(1, 'US'), 1, 15), 'TREINA') nm_usuario
	from	dual);
	
ds_sep_bv_w := obter_separador_bv;

select	decode(ie_tipo_acomodacao_p, 0, null, ie_tipo_acomodacao_p)
into	ie_tipo_acomodacao_w
from	dual;

gsh_i_prc_intern_agenda_ent@tasy_aghos( 0,
					'A',
					ds_erro_ww,
					sysdate,
					ds_usuario_w,
					cd_cnes_solicitante_w,
					nr_internacao_p,
					dt_agendamento_p, 
					ie_tipo_acomodacao_p);

ds_comando_w :=	'declare '||
		'	agenda_sai gsh_i_pkg_integra_sai.agenda_sai@tasy_aghos; '||
		'	ds_erro_ww	varchar2(500); '||
		'begin '||
		' '||
		'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_agenda_sai@tasy_aghos(agenda_sai); '||
		' '||
		'	if	(agenda_sai.first is not null) then '||
		'		for i in agenda_sai.first..agenda_sai.last loop '||
		' '||
		'			if	(agenda_sai(i).idf_internacao = :nr_internacao_p) then '||
		' '||
		'				update	solicitacao_tasy_aghos '||
		'				set	ie_situacao = ''AG'', '||
		'					ds_motivo_situacao = ''Agendamento feito no Aghos'' '||
		'				where	nr_internacao = :nr_internacao_p; '||
		' '||
		'				commit; '||
		' '||
		'				ds_erro_ww := null; '||	
		'				gsh_i_prc_intern_agenda_s_u@tasy_aghos(	agenda_sai(i).idf_gsh_i_intern_agenda_sai,  '||
		'									''P'', '||
		'									ds_erro_ww); '||			
		'			end if; '||
		'		end loop; '||
		'	end if; '||
		'end;';
		
exec_sql_dinamico_bv('AGHOS', ds_comando_w, 	'nr_internacao_p='|| nr_internacao_p);

end Aghos_gerar_agendamento;
/