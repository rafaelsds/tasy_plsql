create or replace
procedure Ajustar_Valor_Auditoria_mat(		nr_sequencia_p			number,
						vl_material_p			number,
						nr_seq_motivo_p			number,
						ie_tipo_ajuste_p		number,
						vl_unitario_p			number,
						nm_usuario_p			varchar2) is 	
											
vl_material_w			number(15,2);
nr_seq_matpaci_w		number(10);
qt_material_w			number(10,3);
ie_Gerar_Pendencia_w		varchar2(1);
vl_unitario_w			number(15,4);

ie_altera_item_repasse_w	varchar2(1) := 'S';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.Get_cd_estabelecimento;
begin

ie_Gerar_Pendencia_w 	 := obter_valor_param_usuario(1116, 136, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);
ie_altera_item_repasse_w := obter_valor_param_usuario(1116, 189, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);

select	nr_seq_matpaci
into	nr_seq_matpaci_w
from	auditoria_matpaci
where	nr_sequencia	= nr_sequencia_p;

select	qt_material,
	vl_material,
	vl_unitario
into	qt_material_w,
	vl_material_w,
	vl_unitario_w
from	material_atend_paciente
where	nr_sequencia		= nr_seq_matpaci_w;


if	(ie_Gerar_Pendencia_w = 'V') then

	update	auditoria_matpaci a
	set	a.vl_material_ajuste	= decode(ie_tipo_ajuste_p, 1, (nvl(vl_unitario_p,0) * qt_material_w), vl_material_p),
		a.ie_tipo_auditoria	= 'V',
		a.nr_seq_motivo		= decode(nr_seq_motivo_p, -1, a.nr_seq_motivo, nr_seq_motivo_p),
		a.nm_usuario = nm_usuario_p,
		a.dt_atualizacao = sysdate		
	where	a.nr_sequencia		= nr_sequencia_p
	and	not exists(	select	1
				from	material_repasse x
				where	x.nr_seq_material = a.nr_seq_matpaci
				and	nvl(ie_altera_item_repasse_w,'S') = 'N');

else

	update	auditoria_matpaci a
	set	a.vl_material		= vl_material_w,
		a.vl_material_ajuste	= decode(ie_tipo_ajuste_p, 1, (nvl(vl_unitario_p,0) * qt_material_w), vl_material_p),
		a.ie_tipo_auditoria	= 'V',
		a.nr_seq_motivo		= decode(nr_seq_motivo_p, -1, a.nr_seq_motivo, nr_seq_motivo_p),
		a.nm_usuario = nm_usuario_p,
		a.dt_atualizacao = sysdate
	where	a.nr_sequencia		= nr_sequencia_p
	and	not exists(	select	1
				from	material_repasse x
				where	x.nr_seq_material = a.nr_seq_matpaci
				and	nvl(ie_altera_item_repasse_w,'S') = 'N');

end if;


if	(ie_tipo_ajuste_p = 1) then

	update	material_Atend_Paciente a
	set	a.vl_material	= (nvl(vl_unitario_p,0) * qt_material_w),
		a.vl_unitario   = nvl(vl_unitario_p,0),
		a.ie_valor_informado	= 'S',
		a.nm_usuario = nm_usuario_p,
		a.dt_atualizacao = sysdate
	where	a.nr_sequencia	= nr_seq_matpaci_w
	and	not exists(	select	1
				from	material_repasse x
				where	x.nr_seq_material = a.nr_sequencia
				and	nvl(ie_altera_item_repasse_w,'S') = 'N');
				
else

	update	material_Atend_Paciente a
	set	a.vl_material	= vl_material_p,
		a.vl_unitario = dividir(vl_material_p,qt_material_w),
		a.ie_valor_informado	= 'S',
		a.nm_usuario = nm_usuario_p,
		a.dt_atualizacao = sysdate
	where	a.nr_sequencia	= nr_seq_matpaci_w
	and	not exists(	select	1
				from	material_repasse x
				where	x.nr_seq_material = a.nr_sequencia
				and	nvl(ie_altera_item_repasse_w,'S') = 'N');
				
end if;				

commit;

end Ajustar_Valor_Auditoria_Mat;
/