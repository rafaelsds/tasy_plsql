create or replace
procedure pls_gerar_w_analise_pos_tit(	nr_seq_analise_p	pls_analise_conta.nr_sequencia%type,
					nr_seq_conta_p		pls_conta.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					nr_id_transacao_p	w_pls_analise_item.nr_id_transacao%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar os t�tulos dos itens da an�lise (Tipos de despesa) na tabela tempor�ria
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_tipo_despesa_w	varchar2(255);
ie_tipo_despesa_w	varchar2(10);
ie_tipo_item_w		varchar2(3);
ie_selecionado_w	varchar2(1)	:= 'N';
qt_registro_w		pls_integer;
nr_sequencia_w		number(10)	:= 0;
qt_selecao_w		pls_integer;

Cursor C01 is
	select	'P',
		a.ie_tipo_despesa,
		obter_valor_dominio(3796,a.ie_tipo_despesa)
	from	pls_conta_proc a,
		pls_conta_pos_estabelecido b
	where	a.nr_sequencia	= b.nr_seq_conta_proc
	and	b.nr_seq_conta	= nr_seq_conta_p
	and	b.nr_seq_analise = nr_seq_analise_p
	and	(b.ie_situacao = 'A' or b.ie_situacao is null)
	and	b.ie_status_faturamento	!= 'A'
	and	b.ie_cobrar_mensalidade	!= 'A'
	group by
		a.ie_tipo_despesa
	union all
	select	'M',
		a.ie_tipo_despesa,
		obter_valor_dominio(1854,a.ie_tipo_despesa)
	from	pls_conta_mat a,
		pls_conta_pos_estabelecido b
	where	a.nr_sequencia	= b.nr_seq_conta_mat
	and	b.nr_seq_conta	= nr_seq_conta_p
	and	b.nr_seq_analise = nr_seq_analise_p
	and	(b.ie_situacao = 'A' or b.ie_situacao is null)
	and	b.ie_status_faturamento	!= 'A'
	and	b.ie_cobrar_mensalidade	!= 'A'
	group by
		a.ie_tipo_despesa;

begin
open C01;
loop
fetch C01 into
	ie_tipo_item_w,
	ie_tipo_despesa_w,
	ds_tipo_despesa_w;
exit when C01%notfound;
	begin
	/* Ver se o t�tulo ainda n�o foi gerado */
	select	count(1)
	into	qt_registro_w
	from	w_pls_analise_item
	where	nr_seq_analise	= nr_seq_analise_p
	and	ie_tipo_linha	= 'T'
	and	nr_id_transacao = nr_id_transacao_p
	and	ds_item		= ds_tipo_despesa_w
	
	and	rownum <= 1;
	
	if	(qt_registro_w = 0) then
		nr_sequencia_w	:= pls_consulta_analise_pos_pck.get_nr_seq_item;
		
		select	count(1)
		into	qt_selecao_w
		from	w_pls_analise_selecao_item a
		where	a.nr_seq_analise = nr_seq_analise_p
		and	a.nr_seq_w_item = nr_sequencia_w
		and	rownum <= 1;
		
		if	(qt_selecao_w > 0) then
			ie_selecionado_w	:= 'S';
		else
			ie_selecionado_w	:= 'N';
		end if;
		
		insert into w_pls_analise_item(
			nr_sequencia, nr_seq_analise, nm_usuario,
			dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec,
			ie_tipo_linha, ie_tipo_despesa, ds_item,
			ie_tipo_item, ie_selecionado, nr_id_transacao
		) values (
			nr_sequencia_w, nr_seq_analise_p, nm_usuario_p,
			sysdate, nm_usuario_p, sysdate,
			'T', ie_tipo_despesa_w, ds_tipo_despesa_w,
			ie_tipo_item_w, ie_selecionado_w, nr_id_transacao_p
		);
			
		pls_consulta_analise_pos_pck.set_nr_seq_item(nr_sequencia_w + 1);
	end if;
	end;
end loop;
close C01;

end pls_gerar_w_analise_pos_tit;
/
