create or replace procedure GERAR_LOTE_AUDITORIA
			(nr_seq_retorno_p		number,
			ie_gerar_itens_p		varchar2,
			nm_usuario_p		varchar2,
			nr_seq_lote_audit_p	number,
			ie_guia_sem_saldo_p	varchar2,
			ie_gerar_lotes_p		varchar2 default 'N',
			ds_retorno_p		out varchar2) is

cd_convenio_w		number(5);
cd_estabelecimento_w	number(4);
nr_seq_lote_audit_w	number(10);
nr_seq_lote_hist_w	number(10);
nr_analise_w		number(10);
dt_fechamento_w		date;
nr_reg_atual_w		number(10);
nr_registro_w		varchar2(255);
ie_adicional_w		varchar2(5);
dt_envio_w		date;
ie_data_retorno_w	varchar2(1);
dt_retorno_w		date;
ds_retorno_w		varchar2(4000);

begin

if	(nr_seq_retorno_p is not null) then

	select	max(a.cd_convenio),
		max(a.cd_estabelecimento),
		max(a.dt_retorno)
	into	cd_convenio_w,
		cd_estabelecimento_w,
		dt_retorno_w
	from	convenio_retorno a
	where	a.nr_sequencia	= nr_seq_retorno_p;

	if	(nr_seq_lote_audit_p	is null) then	/* se nenhum lote foi selecionado, gera um novo lote */

		Criar_Lote_Auditoria(	cd_convenio_w,
					cd_estabelecimento_w,
					nm_usuario_p,
					nr_seq_lote_audit_w);
	else
		select	a.dt_fechamento
		into	dt_fechamento_w
		from	lote_auditoria a
		where	a.nr_sequencia	= nr_seq_lote_audit_p;

		if	(dt_fechamento_w	is not null) then
			/* Esta opera��o n�o pode ser realizada. O lote de recurso nr_seq_lote_audit_p j� est� fechado! */
			wheb_mensagem_pck.exibir_mensagem_abort(199212,'NR_SEQ_LOTE_AUDIT_P=' || nr_seq_lote_audit_p);
		end if;

		nr_seq_lote_audit_w	:= nr_seq_lote_audit_p;
	end if;

	Criar_Lote_Audit_Hist(	nr_seq_lote_audit_w,
				cd_estabelecimento_w,
				nm_usuario_p,
				nr_seq_lote_hist_w,
        nr_seq_retorno_p);

	if	(nvl(ie_gerar_itens_p,'N')	= 'S') then
		gerar_lote_audit_hist(nr_seq_lote_hist_w, nm_usuario_p, nr_seq_retorno_p, 'S', nvl(ie_guia_sem_saldo_p,'S'), ie_gerar_lotes_p, ds_retorno_w);
		ds_retorno_p	:= substr(ds_retorno_w,1,255);
	else	/* na GERAR_LOTE_AUDIT_HIST j� tem commit */
		commit;
	end if;

end if;

end GERAR_LOTE_AUDITORIA;
/
