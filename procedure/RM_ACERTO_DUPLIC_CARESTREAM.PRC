create or replace procedure rm_acerto_duplic_carestream(nr_seq_pessoa_dupli_p varchar2,
                                                        CD_STATUS_P           varchar2,
                                                        dt_unificacao_p       date) is

  nr_sequencia_w    pessoa_fisica_duplic_cares.nr_sequencia%type;
  registro_existe_w varchar2(1);

begin

  select decode(count(*), 0, 'N', 'S')
  into   registro_existe_w
  from   pessoa_fisica_duplic_cares
  where  nr_seq_pessoa_duplic = nr_seq_pessoa_dupli_p;

  if (registro_existe_w = 'N') then
    begin
      select pessoa_fisica_duplic_cares_seq.nextval
      into   nr_sequencia_w
      from   dual;
    
      insert into pessoa_fisica_duplic_cares
        (nr_sequencia, nr_seq_pessoa_duplic, dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec, cd_status)
      values
        (nr_sequencia_w, nr_seq_pessoa_dupli_p, dt_unificacao_p, dt_unificacao_p, 'CARESTREAM', 'CARESTREAM', CD_STATUS_P);
    
      commit;
    end;
  elsif (registro_existe_w = 'S') then
    begin
      update pessoa_fisica_duplic_cares
      set    cd_status      = cd_status_p,
             dt_atualizacao = dt_unificacao_p
      where  nr_seq_pessoa_duplic = nr_seq_pessoa_dupli_p;
      commit;
    end;
  end if;
end RM_ACERTO_DUPLIC_CARESTREAM;
/
