create or replace
procedure consiste_lib_solic_kit (nr_sequencia_p		number,
					ds_erro_p	out	varchar2) is
					
cd_material_w			number(6);
qt_disponivel_w			number(13,4);
cd_local_estoque_w		number(4);
cd_estabelecimento_w		number(4);
qt_solicitada_w			number(13);
ds_erro_w			varchar2(1000);
qt_material_requisicao_w		number(13,4);

Cursor C01 is
select	a.cd_material,
	a.qt_solicitada,
	a.qt_disponivel
from	solic_kit_mat_comp a
where	nr_seq_solic_kit = nr_sequencia_p
and	nvl(qt_disponivel,0) < qt_solicitada;

begin

select	cd_estabelecimento,
	cd_local_estoque
into	cd_estabelecimento_w,
	cd_local_estoque_w
from	solic_kit_material
where	nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01 into	
	cd_material_w,
	qt_solicitada_w,
	qt_disponivel_w;
exit when C01%notfound;
	begin
	select	nvl(sum(qt_material_requisitada - nvl(qt_material_atendida,0)),0)
	into	qt_material_requisicao_w
	from	requisicao_material a,
		item_requisicao_material b
	where	a.nr_requisicao = b.nr_requisicao
	and	a.dt_baixa is null
	and	nvl(b.cd_motivo_baixa,0) = 0
	and	a.nr_seq_solic_kit = nr_sequencia_p
	and	b.cd_material = cd_material_w;
	
	qt_disponivel_w	:= qt_disponivel_w + qt_material_requisicao_w;
	
	if	(qt_disponivel_w < qt_solicitada_w) then
		ds_erro_w := substr(ds_erro_w || cd_material_w || ' - ' || obter_desc_material(cd_material_w) || chr(10),1,1000);
	end if;
	end;
end loop;
close C01;

commit;

if	(ds_erro_w is not null) then
	ds_erro_p := substr(WHEB_MENSAGEM_PCK.get_texto(280396) || chr(10) || ds_erro_w,1,255);
end if;

end consiste_lib_solic_kit;
/