create or replace
procedure Med_Excluir_Lista_Espera(	nr_seq_lista_p	number) is

begin

if	(nr_seq_lista_p	is not null) then
	delete from med_lista_espera where nr_sequencia = nr_seq_lista_p;
end if;

commit;

end Med_Excluir_Lista_Espera;
/