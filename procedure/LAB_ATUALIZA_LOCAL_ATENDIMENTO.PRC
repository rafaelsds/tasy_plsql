create or replace
procedure lab_atualiza_local_atendimento (nr_prescricao_p			number,
				    nr_seq_local_pa_p		number,
				    nm_usuario_p			Varchar2) is 

										  
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;

begin

select 	Max(nr_atendimento)
into 	nr_atendimento_w
from	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

if 	((nr_atendimento_w is not null) and 	
	(nr_seq_local_pa_p is not null)) then
	
	update  atendimento_paciente
	set 	nr_seq_local_pa      = nr_seq_local_pa_p,
		dt_atualizacao 	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_atendimento = nr_atendimento_w;

end if;

commit;

end lab_atualiza_local_atendimento;
/