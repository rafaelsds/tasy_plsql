/*---------------------------------------------------------------------------------------------------------------------------------------
		           N�O  DEVE SER UTILIZADA
		UTILIZAR A PLS_GERAR_CONTA_PARTIC
    ---------------------------------------------------------------------------------------------------------------------------------------*/
create or replace
procedure pls_conta_gerar_hi
			(	nr_seq_conta_p			Varchar2,
				cd_estabelecimento_p		Number,
				nm_usuario_p			Varchar2,
				ie_commit_p			Varchar2,
				nr_seq_protocolo_gerado_p out	Number) is
	

nr_seq_conta_proc_w		Number(10);	
nr_seq_conta_w			Number(10);	
nr_seq_procedimento_w		Number(10);
nr_seq_procedimento_ww		Number(10);
nr_seq_participante_w		Number(10);
nr_seq_participante_ww		Number(10);
cd_medico_w			Number(10);
nr_seq_grau_partic_w		Number(10);
nr_seq_cbo_saude_w		Number(10);
nr_seq_conta_referencia_w	Number(10);
ie_existe_w			Number(1);
dt_procedimento_w		date;
cd_procedimento_w 		Number(10);
ie_origem_proced_w 		Number(10);
qt_procedimento_w		number(10);
vl_unitario_w			number(10,4); 
vl_procedimento_w 		number(10,4); 
ie_via_acesso_w			Varchar2(5);
dt_procedimento_imp_w		date; 
cd_procedimento_imp_w		number(10);
qt_procedimento_imp_w 		number(10);
vl_unitario_imp_w 		number(10,4);
vl_procedimento_imp_w		number(10,4);
ie_via_acesso_imp_w		Varchar2(5);
dt_inicio_proc_w		date; 
dt_fim_proc_w			date;
dt_inicio_proc_imp_w 		date;
dt_fim_proc_imp_w 		date;
tx_participacao_w		number(10,4);
vl_participacao_w 		number(10,4);
ds_procedimento_imp_w 		Varchar2(255);
cd_tipo_tabela_imp_w		Number(10);
tx_reducao_acrescimo_imp_w 	number(10,4);
ie_tipo_despesa_imp_w 		Varchar2(5);
ie_tecnica_utilizada_w		Varchar2(5);
vl_liberado_w 			number(10,4);
vl_glosa_w 			number(10,4);
vl_saldo_w			number(10,4);
nr_seq_regra_w 			number(10);
ie_tipo_despesa_w 		Varchar2(5);
ie_situacao_w			Varchar2(5);
ie_status_w 			Varchar2(5);
dt_liberacao_w 			date;
nm_usuario_liberacao_w		Varchar2(255);
tx_item_w			number(10,4); 
nr_seq_tiss_tabela_w 		number(10);
nr_seq_regra_horario_w		number(10);
vl_custo_operacional_w 		number(10,4);
vl_anestesista_w 		number(10,4);
vl_materiais_w			number(10,4);
vl_medico_w 			number(10,4);
vl_auxiliares_w 		number(10,4);
nr_seq_regra_liberacao_w	number(10);
ds_log_w 			Varchar2(255);
cd_conta_cred_w			number(10); 
cd_conta_deb_w			number(10);
cd_historico_w 			number(10);
cd_conta_glosa_cred_w 		number(10);
cd_conta_glosa_deb_w		number(10);
cd_historico_glosa_w 		number(10);
nr_seq_regra_ctb_deb_w 		number(10);	
nr_seq_regra_ctb_cred_w		number(10);
nr_seq_grupo_ans_w		number(10);
cd_guia_w			Varchar2(20);
cd_guia_referencia_w		Varchar2(20);
cd_guia_prestador_w		Varchar2(20);
dt_emissao_w			date;
cd_senha_w			Varchar2(20);
nr_seq_segurado_w		number(10);
nr_seq_prestador_exec_w		number(10);
cd_cnes_w			number(10);
cd_medico_executor_w		number(10);
nr_seq_tipo_acomodacao_w	number(10);
cd_usuario_plano_imp_w		Varchar2(30);
nr_seq_protocolo_w		Number(10);
nr_seq_conta_exclusao_w		number(10);
cd_guia_proc_w			Varchar2(20);
nr_seq_protocolo_conta_w	number(10);
nr_seq_prestador_w		number(10);
dt_competencia_w		date;
nr_protocolo_prestador_w	number(10);
dt_protocolo_w			date;
ie_existe_participante_w	number(10);
ie_origem_conta_w		Varchar2(5);
ie_tipo_guia_w			Varchar2(5);
qt_procedimentos_w		number(10);
nr_seq_lote_conta_w		number(10);
nr_seq_outorgante_w		number(10);
qt_participante_w		Number(10);
qt_medico_credenciado_w		Number(10);
nr_seq_glosa_w			Number(10);
dt_recebimento_w		Date;
qt_procedimento_ww		Number(10) := 	0;
qt_conta_w			Number(10);
ie_credenciado_w		varchar2(1);
vl_total_partic_w		Number(15,2);
vl_calculado_partic_w		Number(15,2);
qt_atualizacoes_w		Number(10)	:= 0;
ie_tipo_importacao_w		varchar2(2)	:= 'UP';
ie_guia_fisica_w		Varchar2(1);
ie_origem_protocolo_w		varchar2(1);

Cursor C01 is
	select	c.nr_sequencia,
		b.cd_medico,
		b.nr_seq_grau_partic,
		b.nr_seq_cbo_saude,
		c.cd_guia,
		b.cd_guia,
		pls_obter_se_credenciado_prest(b.cd_medico, c.nr_seq_prestador_exec) 
	from	pls_proc_participante	b,
		pls_conta		c,
		pls_conta_proc		a
	where	a.nr_seq_conta		= nr_seq_conta_p
	and	c.nr_sequencia		= a.nr_seq_conta
	and	a.nr_sequencia		= b.nr_seq_conta_proc		
	group by
		c.nr_sequencia,
		b.cd_medico,
		c.nr_seq_prestador_exec,
		b.nr_seq_grau_partic,
		b.nr_seq_cbo_saude,
		c.cd_guia,
		b.cd_guia;

Cursor C02 is
	select	a.cd_procedimento,
		a.ie_origem_proced,
		a.tx_item,
		a.nr_sequencia,
		b.nr_sequencia,
		b.cd_medico,
		b.nr_seq_cbo_saude,
		b.cd_guia,
		b.vl_calculado,
		a.vl_total_partic
	from	pls_proc_participante	b,
		pls_conta_proc		a
	where	a.nr_seq_conta		= nr_seq_conta_p
	and	b.cd_medico		= cd_medico_w
	and	a.nr_sequencia		= b.nr_seq_conta_proc
	and	b.nr_seq_grau_partic	= nr_seq_grau_partic_w
	and	nvl(b.cd_guia,cd_guia_proc_w)		= cd_guia_proc_w
	order by 
		cd_procedimento,
		ie_origem_proced;
		
Cursor C03 is
	select	nr_sequencia
	from	pls_conta_glosa
	where   nr_seq_conta_proc = nr_seq_procedimento_w;

begin

begin
	select	ie_origem_conta,
		ie_tipo_guia
	into	ie_origem_conta_w,
		ie_tipo_guia_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_p;
exception
when others then
	ie_origem_conta_w	:= null;
	ie_tipo_guia_w		:= null;
end;

select	count(c.nr_sequencia)
into	qt_medico_credenciado_w
from	pls_proc_participante	b,
	pls_conta_proc		a,
	pls_conta		c
where	a.nr_seq_conta		= nr_seq_conta_p
and	c.nr_sequencia		= a.nr_seq_conta
and	a.nr_sequencia		= b.nr_seq_conta_proc		
and	pls_obter_se_credenciado_prest(b.cd_medico, c.nr_seq_prestador_exec) = 'S';
/*S� � gerado as guias de honor�rio individual de froam autom�tica quando a conta fori digitada e de interna��o*/
/* Foi alterado na linha abaixo para a rotina tamb�m validar as contas digitadas pelo portal */

if	(nvl(ie_origem_conta_w,'X') in ('D','P')) and  
	(nvl(ie_tipo_guia_w,0) = 5) and
	(nvl(qt_medico_credenciado_w,0) <> 0) then
	
	select	nr_seq_protocolo
	into	nr_seq_protocolo_conta_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_p;

	/*Verifica se j� existe um protocolo de honor�rio individual para este protocolo*/
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_protocolo_w
	from	pls_protocolo_conta
	where	nr_seq_prot_referencia	= nr_seq_protocolo_conta_w
	and	ie_status in (1, 2)
	and	ie_tipo_guia = 6;
	
	if	(nr_seq_protocolo_w = 0) then /*Se n�o existir cria-se um novo protocolo de Honorario individual*/

		select	nr_seq_prestador,
			dt_mes_competencia,
			nr_protocolo_prestador,
			dt_protocolo,
			nr_seq_lote_conta,
			dt_recebimento,
			ie_origem_protocolo,
			nvl(ie_guia_fisica,'N'),
			ie_tipo_importacao
		into	nr_seq_prestador_w	,
			dt_competencia_w,
			nr_protocolo_prestador_w,
			dt_protocolo_w,
			nr_seq_lote_conta_w,
			dt_recebimento_w,
			ie_origem_protocolo_w,
			ie_guia_fisica_w,
			ie_tipo_importacao_w
		from	pls_protocolo_conta
		where	nr_sequencia	= nr_seq_protocolo_conta_w;
				
		select  pls_protocolo_conta_seq.nextval
		into    nr_seq_protocolo_w
		from    dual;
		
		nr_seq_protocolo_gerado_p	:= nr_seq_protocolo_w;

		select	max(nr_sequencia)
		into	nr_seq_outorgante_w
		from    pls_outorgante
		where   cd_estabelecimento = cd_estabelecimento_p;		

		insert into pls_protocolo_conta
			(nr_sequencia, nm_usuario, dt_atualizacao,
			nm_usuario_nrec, dt_atualizacao_nrec, ie_situacao,
			ie_status, dt_mes_competencia, cd_estabelecimento,
			ie_tipo_guia, ie_apresentacao, dt_protocolo,
			nr_seq_prestador, nr_protocolo_prestador, qt_contas_informadas,
			dt_base_venc, ie_tipo_protocolo, nr_seq_prot_referencia,
			nr_seq_outorgante, ie_guia_fisica, nr_seq_lote_conta,
			dt_recebimento,ie_tipo_importacao, ie_origem_protocolo)
		values  (nr_seq_protocolo_w, nm_usuario_p, sysdate,
			nm_usuario_p, sysdate, 'D',
			1, dt_competencia_w, cd_estabelecimento_p,
			6, 'A', dt_protocolo_w,
			nr_seq_prestador_w, nr_protocolo_prestador_w, null,
			sysdate, 'C', nr_seq_protocolo_conta_w,
			nr_seq_outorgante_w, 'N', nr_seq_lote_conta_w,
			dt_recebimento_w,ie_tipo_importacao_w, ie_origem_protocolo_w);
	end if;
	
	open C01; /*Obtem os procedimentos e seus respectivos participantes*/
	loop
	fetch C01 into	
		nr_seq_conta_w,
		cd_medico_w,
		nr_seq_grau_partic_w,
		nr_seq_cbo_saude_w,	
		cd_guia_w,
		cd_guia_proc_w,
		ie_credenciado_w;
	exit when C01%notfound;
		begin
		if	(ie_credenciado_w = 'S') then
			qt_procedimento_ww := 0;
			
			select	max(nvl(nr_sequencia,0)) /*Verifica se j� foi gerada uma conta de Hi para este m�dico, grau de participa��o e guia nesta conta*/
			into	nr_seq_conta_w
			from	pls_conta
			where	nr_seq_protocolo 	= nr_seq_protocolo_w
			and	cd_medico_executor 	= cd_medico_w
			and	cd_guia 		= cd_guia_proc_w
			and	nr_seq_grau_partic 	= nr_seq_grau_partic_w
			and	nr_seq_conta_referencia	= nr_seq_conta_p;
			
			if	(nvl(nr_seq_conta_w,0) = 0) then /*Se n�o existe cria-se a conta*/
				select	pls_conta_seq.nextval
				into	nr_seq_conta_w
				from	dual;
				
				insert into pls_conta
					(nr_seq_protocolo, cd_guia, cd_guia_referencia,      
					cd_guia_prestador, dt_emissao, dt_atendimento_referencia, cd_senha,                
					ie_tipo_guia, nr_seq_segurado, nr_seq_prestador_exec,   
					cd_cnes, cd_medico_executor, nr_seq_grau_partic,      
					nr_seq_cbo_saude, nr_seq_tipo_acomodacao,  
					ie_status, cd_usuario_plano_imp, dt_atualizacao, 
					nm_usuario, cd_estabelecimento, nr_seq_conta_referencia, 
					nr_sequencia, cd_cooperativa, nr_crm_exec, 
					nr_seq_conselho_exec, uf_crm_exec, ie_origem_conta,
					nr_seq_clinica)  
				select	nr_seq_protocolo_w, cd_guia_proc_w, cd_guia_referencia,      
					cd_guia_prestador, dt_emissao, dt_atendimento_referencia, cd_senha,                
					6, nr_seq_segurado, pls_obter_seq_prestador_pf(cd_medico_w, cd_estabelecimento_p),   
					cd_cnes, cd_medico_w, nr_seq_grau_partic_w,      
					nr_seq_cbo_saude_w, nr_seq_tipo_acomodacao,  
					'U', cd_usuario_plano_imp, sysdate, 
					nm_usuario_p, cd_estabelecimento, nr_sequencia, 
					nr_seq_conta_w, cd_cooperativa, obter_crm_medico(cd_medico_w), 
					pls_obter_seq_conselho_prof(cd_medico_w), obter_dados_medico(cd_medico_w, 'UFCRM'),ie_origem_conta, 
					nr_seq_clinica
				from	pls_conta
				where	nr_sequencia = nr_seq_conta_p;			
			end if;
			
			open C02;
			loop
			fetch C02 into	
				cd_procedimento_w,
				ie_origem_proced_w,
				tx_item_w,
				nr_seq_procedimento_w,
				nr_seq_participante_w,
				cd_medico_w,
				nr_seq_cbo_saude_w,
				cd_guia_proc_w,
				vl_calculado_partic_w,
				vl_total_partic_w;
			exit when C02%notfound;
				begin
				
				qt_procedimento_ww := qt_procedimento_ww + 1;
				/*verifica se nesta conta existe o procedimento*/
				select	count(nr_sequencia)
				into	qt_procedimentos_w
				from	pls_conta_proc
				where	nr_seq_conta 		= nr_seq_conta_w
				and	cd_procedimento		= cd_procedimento_w
				and	ie_origem_proced	= ie_origem_proced_w
				and	tx_item			= tx_item_w;
				
				if	(qt_procedimentos_w = 0) then
					/*Insere-se o procedimento nesta conta*/
					insert into pls_conta_proc
						(nr_sequencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, dt_procedimento,
						cd_procedimento, ie_origem_proced, qt_procedimento,
						vl_unitario, vl_procedimento, ie_via_acesso,
						nr_seq_conta, dt_procedimento_imp, cd_procedimento_imp,
						qt_procedimento_imp, vl_unitario_imp, vl_procedimento_imp,
						ie_via_acesso_imp, dt_inicio_proc, dt_fim_proc,
						dt_inicio_proc_imp, dt_fim_proc_imp, tx_participacao,
						vl_participacao, ds_procedimento_imp, cd_tipo_tabela_imp,
						tx_reducao_acrescimo_imp, ie_tipo_despesa_imp, ie_tecnica_utilizada,
						vl_liberado, vl_glosa, vl_saldo,
						nr_seq_regra, ie_tipo_despesa, ie_situacao,
						ie_status, dt_liberacao, nm_usuario_liberacao,
						tx_item, nr_seq_tiss_tabela, nr_seq_regra_horario,
						vl_custo_operacional, vl_anestesista, vl_materiais,
						vl_medico, vl_auxiliares, nr_seq_regra_liberacao,
						ds_log, cd_conta_cred, cd_conta_deb,
						cd_historico, cd_conta_glosa_cred, cd_conta_glosa_deb,
						cd_historico_glosa, nr_seq_regra_ctb_deb, nr_seq_regra_ctb_cred,
						nr_seq_grupo_ans, nr_auxiliares, nr_seq_participante_hi)
					select	pls_conta_proc_seq.nextval, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, dt_procedimento,
						cd_procedimento, ie_origem_proced, qt_procedimento,
						vl_unitario, vl_procedimento, ie_via_acesso,
						nr_seq_conta_w, dt_procedimento_imp, cd_procedimento_imp,
						qt_procedimento_imp, vl_unitario_imp, vl_procedimento_imp,
						ie_via_acesso_imp, dt_inicio_proc, dt_fim_proc,
						dt_inicio_proc_imp, dt_fim_proc_imp, tx_participacao,
						vl_participacao, ds_procedimento_imp, cd_tipo_tabela_imp,
						tx_reducao_acrescimo_imp, ie_tipo_despesa_imp, ie_tecnica_utilizada,
						vl_liberado, vl_glosa, vl_saldo,
						nr_seq_regra, ie_tipo_despesa, 'U',
						ie_status, dt_liberacao, nm_usuario_liberacao,
						tx_item, nr_seq_tiss_tabela, nr_seq_regra_horario,
						vl_custo_operacional, vl_anestesista, vl_materiais,
						vl_medico, vl_auxiliares, nr_seq_regra_liberacao,
						ds_log, cd_conta_cred, cd_conta_deb,
						cd_historico, cd_conta_glosa_cred, cd_conta_glosa_deb,
						cd_historico_glosa, nr_seq_regra_ctb_deb, nr_seq_regra_ctb_cred,
						nr_seq_grupo_ans, nr_auxiliares, nr_seq_participante_w
					from	pls_conta_proc
					where	nr_sequencia	= nr_seq_procedimento_w;
				end if;
				vl_total_partic_w := vl_total_partic_w - vl_calculado_partic_w;
				
				update	pls_conta
				set	cd_guia		= cd_guia_proc_w
				where	nr_sequencia	= nr_seq_conta_w;
				
				/*alterado o status do participante para evitar cobran�a duplicada OS 451709*/
				update	pls_proc_participante
				set	ie_status = 'C',
					vl_participante = 0
				where	nr_sequencia = nr_seq_participante_w;
				
				/*Altera o situa��o das Glosas e Ocorrencias que possam existir para participante*/
				update	pls_ocorrencia_benef
				set	ie_situacao = 'I',
					ie_forma_inativacao	= decode(ie_forma_inativacao,'S','US','US','US','U')
				where	nr_seq_proc_partic = nr_seq_participante_w;
				
				update	pls_conta_glosa
				set	ie_situacao = 'I',
					ie_forma_inativacao	= decode(ie_forma_inativacao,'S','US','US','US','U')
				where	nr_seq_proc_partic = nr_seq_participante_w;
				
				end;
			end loop;
			close C02;
			
			/*Necess�rio manter este tratamento para que n�o sejam criadas contas sem procedimento*/
			if	(nvl(qt_procedimento_ww,0) = 0) then
				delete	pls_conta
				where	nr_sequencia = nr_seq_conta_w;
			end if;

		end if;
		end;		
	end loop;
	close C01;
	
	/*Necess�rio manter este tratamento para que n�o sejam criadas contas sem procedimento*/
	select	count(1)
	into	qt_conta_w
	from	pls_conta
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	
	if	(nvl(qt_conta_w,0) = 0) then
		delete	pls_protocolo_conta
		where	nr_sequencia	= nr_seq_protocolo_w;
	end if;
	
	if	(nvl(ie_commit_p,'S') = 'S') then	
		commit; 
	end if;
end if; 

end pls_conta_gerar_hi;
/