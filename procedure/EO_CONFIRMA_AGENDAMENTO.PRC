create or replace
procedure Eo_confirma_agendamento(
			nr_seq_paciente_vacina_p 	Number,
			nm_usuario_p		Varchar2) is 

begin

update 	paciente_vacina
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_confirmacao = trunc(sysdate),
	ie_confirmado = 'S'
where	nr_sequencia = nr_seq_paciente_vacina_p;

commit;

end Eo_confirma_agendamento;
/