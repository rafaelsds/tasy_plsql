create or replace
procedure alterar_atend_menor_dt_agenda(	ds_lista_agendas_p	varchar2,
						nr_atendimento_p	number,
						nm_usuario_p		varchar2) is 
dt_menor_agenda_w	date;

begin

dt_menor_agenda_w	:= obter_menor_data_agenda(ds_lista_agendas_p);

if	(dt_menor_agenda_w is not null) then
	update	atendimento_paciente
	set	dt_entrada = dt_menor_agenda_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_atendimento = nr_atendimento_p;
end if;

commit;

end alterar_atend_menor_dt_agenda;
/