create or replace
procedure gc_gerar_kit_estoque_agrup_js(
					nr_seq_kit_estoque_p	number,
					nr_cirurgia_p		number,
					nr_prescricao_p		number,
					ie_origem_inf_p 	varchar2,
					cd_local_estoque_p	number,
					cd_perfil_p		number,
					cd_medico_p		varchar2,
					nr_doc_interno_p	number,
					nm_usuario_p		varchar,
					nr_doc_interno_aux_p	number,
					ie_tipo_doc_interno_p	varchar2,
					ie_tipo_doc_interno_aux_p varchar2) is

qt_kit_estoque_w		number(10);
begin


if	(nr_seq_kit_estoque_p is not null) then
	begin	

	select	count(*) 
	into 	qt_kit_estoque_w
	from	kit_estoque_reg 
	where	nr_sequencia = nr_seq_kit_estoque_p
	and 	dt_liberacao is null;

	if	(qt_kit_estoque_w > 0) then
		begin
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(51025);
		end;
	end if;
	end;
end if;

gerar_kit_estoque_agrup(nr_seq_kit_estoque_p,
						nr_cirurgia_p,
						nr_prescricao_p,
						ie_origem_inf_p,
						cd_local_estoque_p,
						cd_perfil_p,
						cd_medico_p,
						nr_doc_interno_p,
						nm_usuario_p,
						nr_doc_interno_aux_p,
						ie_tipo_doc_interno_p,
						ie_tipo_doc_interno_aux_p);


end gc_gerar_kit_estoque_agrup_js;
/
