create or replace
procedure excluir_mat_receb_disp_elet(	nr_seq_p number ,
				nm_usuario_p       varchar2 ) is

begin

update	w_int_disp_auto
set	ie_lido = 'S'
where	nr_sequencia = nr_seq_p;

commit;

end excluir_mat_receb_disp_elet;
/
