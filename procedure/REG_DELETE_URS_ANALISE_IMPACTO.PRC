create or replace procedure REG_DELETE_URS_ANALISE_IMPACTO(nr_sequencia_p number) is

begin

  delete from man_os_imp_cr_quest where nr_seq_imp_cr = nr_sequencia_p;

  delete from man_ordem_serv_imp_cr where nr_sequencia = nr_sequencia_p;

  commit;

end REG_DELETE_URS_ANALISE_IMPACTO;
/
