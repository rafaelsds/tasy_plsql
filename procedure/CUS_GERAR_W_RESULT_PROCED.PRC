create or replace
procedure cus_gerar_w_result_proced(	cd_estabelecimento_p		number,
						cd_area_procedimento_p	number,
						cd_grupo_proc_p		number,
						cd_especialidade_p		number,
						cd_convenio_p			number,
						cd_setor_atendimento_p	number,
						nr_seq_grupo_exame_p		number,
						dt_referencia_p		date,
						nm_usuario_p			varchar2) is

/* procedure criada por matheus para o relat�rio wcus 6019*/

cd_procedimento_w				number(15);
cd_tabela_custo_proc_w			number(10);
dt_mes_referencia_w				date;
ie_origem_proced_w				number(10);
nr_interno_conta_w				number(15);
pr_resultado_w				number(15,2);
qt_parametro_w				number(15,4);
qt_resumo_w					number(15,4);
vl_custo_w					number(15,2);
vl_custo_dir_apoio_w				number(15,4);
vl_custo_dir_fixo_w				number(15,4);
vl_custo_hm_w					number(15,2);
vl_custo_indireto_w				number(15,2);
vl_custo_mao_obra_w				number(15,2);
vl_custo_total_w				number(15,4);
vl_custo_var_w				number(15,4);
vl_despesa_w					number(15,2);
vl_preco_calculado_w				number(15,4);
vl_preco_tabela_w				number(15,4);
vl_procedimento_w				number(15,2);
vl_resultado_w				number(15,2);

/*Contas do M�s*/
cursor c01 is
select	distinct
	a.nr_interno_conta
from	conta_paciente_resumo_v3 a
where	a.dt_referencia = dt_mes_referencia_w
and	a.cd_convenio	= nvl(cd_convenio_p, a.cd_convenio);

/* Procedimentos executados no m�s */
cursor c02 is
select	c.cd_procedimento,
	c.ie_origem_proced,
	c.qt_resumo,
	nvl(c.vl_procedimento,0),
	nvl(c.vl_custo,0),
	nvl(c.vl_custo_variavel,0),
	nvl(c.vl_custo_dir_apoio,0),
	nvl(c.vl_custo_mao_obra,0),
	nvl(c.vl_custo_direto,0),
	nvl(c.vl_custo_indireto,0),
	nvl(c.vl_despesa,0),
	nvl(c.vl_custo_hm,0)
from	conta_paciente_resumo_v3 c
where	c.nr_interno_conta			= nr_interno_conta_w
and	c.dt_referencia 			= dt_mes_referencia_w
and	c.cd_area_procedimento			= nvl(cd_area_procedimento_p, c.cd_area_procedimento)
and	c.cd_grupo_proc				= nvl(cd_grupo_proc_p, c.cd_grupo_proc)
and	c.cd_especialidade			= nvl(cd_especialidade_p, c.cd_especialidade)
and	c.cd_setor_atendimento			= nvl(cd_setor_atendimento_p, c.cd_setor_atendimento)
and	c.nr_seq_grupo_exame			= nvl(nr_seq_grupo_exame_p, c.nr_seq_grupo_exame)
and	c.cd_procedimento is not null;

BEGIN

delete	from w_cus_result_proced;
commit;

dt_mes_referencia_w	:= trunc(dt_referencia_p,'mm');

open c01;
loop
fetch c01 into
	nr_interno_conta_w;
exit when c01%notfound;

	open c02;
	loop
	fetch c02 into
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_resumo_w,
		vl_procedimento_w,
		vl_custo_w,
		vl_custo_var_w,
		vl_custo_dir_apoio_w,
		vl_custo_mao_obra_w,
		vl_custo_dir_fixo_w,
		vl_custo_indireto_w,
		vl_despesa_w,
		vl_custo_hm_w;
	exit when c02%notfound;
		
		vl_resultado_w			:= vl_procedimento_w - vl_custo_w;
		pr_resultado_w			:= dividir(vl_resultado_w, vl_procedimento_w) *100;

		insert into w_cus_result_proced(
			cd_procedimento,
			ie_origem_proced,
			qt_resumo,
			vl_procedimento,
			vl_preco_calculado,
			vl_custo_variavel,
			vl_custo_mao_obra,
			vl_custo_direto_fixo,
			vl_custo_direto_apoio,
			vl_custo_indireto,
			vl_despesa,
			vl_custo_hm,
			vl_resultado,
			pr_resultado,
			dt_atualizacao,
			nm_usuario)
		values(cd_procedimento_w,
			ie_origem_proced_w,
			qt_resumo_w,
			vl_procedimento_w,
			vl_custo_w,
			vl_custo_var_w,
			vl_custo_mao_obra_w,
			vl_custo_dir_fixo_w,
			vl_custo_dir_apoio_w,
			vl_custo_indireto_w,
			vl_despesa_w,
			vl_custo_hm_w,
			vl_resultado_w,
			pr_resultado_w,
			sysdate,
			nm_usuario_p);
	end loop;
	close c02;

end loop;
close c01;

commit;	
END cus_gerar_w_result_proced;
/
