create or replace
procedure VINCULAR_AUTOR_CIR_AGENDA_PAC
			(nr_seq_autorizacao_p	in number,
			nr_seq_agenda_p		in number,			
			nm_usuario_p		in varchar2) is

ie_estagio_autor_w	varchar2(1);
dt_agenda_w		date;
begin

update	autorizacao_cirurgia
set	nr_seq_agenda	= nr_seq_agenda_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_autorizacao_p;

begin
select	dt_agenda
into	dt_agenda_w
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p;
exception
when others then
	dt_agenda_w := null;
end;

update	autorizacao_convenio
set	nr_seq_agenda		= Nvl(nr_seq_agenda,nr_seq_agenda_p),
	dt_agenda		= Nvl(Dt_agenda,dt_agenda_w),
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_seq_autor_cirurgia	= nr_seq_autorizacao_p;

select	max(ie_estagio_autor)
into	ie_estagio_autor_w
from	autorizacao_cirurgia
where	nr_sequencia	= nr_seq_autorizacao_p;

if	(ie_estagio_autor_w in ('3','8','7')) then --Autorizado, e atualizar o est�gio no OPME
	atualizar_agenda_pac_opme(nr_seq_autorizacao_p, nr_seq_agenda_p, nm_usuario_p);
end if;

commit;

end VINCULAR_AUTOR_CIR_AGENDA_PAC;
/