create or replace
procedure ple_alterar_meta_acao(	nr_seq_meta_p	number,
				nr_sequencia_p	number,
				nm_usuario_p	varchar2) is 

begin

update	man_ordem_servico
set	nr_seq_meta_pe	= nr_seq_meta_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where  nr_sequencia	= nr_sequencia_p;

commit;

end ple_alterar_meta_acao;
/