create or replace
procedure REP_copia_suplemento(	nr_prescricao_orig_p	number,
								nr_prescricao_p			number,
								dt_prescricao_p			date,
								nr_seq_regra_p			number,
								nr_seq_material_p		number,
								nm_usuario_p			Varchar2) is 

nr_agrup_acum_w			number(10,0);
ie_copia_w				varchar2(5);
ie_padronizado_w		varchar2(1);
nr_seq_material_w		number(15);
ie_loop_w				varchar2(1);
qt_itens_w				number(15);
ie_copia_valid_igual_w	varchar2(1);
dt_prescricao_www		date;
dt_inicio_w				date;
dt_final_w				date;
dt_inicio_item_w		date;
dt_inicio_prescr_w		date;
dt_prescricao_w			date;
nr_horas_validade_w		number(5);
ie_prim_horario_setor_w	varchar2(10);
hr_setor_w				varchar2(10);
cd_setor_atendimento_w	number(10);
cd_estabelecimento_w	Number(4,0);
nr_atendimento_w		number(15,0);
dt_validade_origem_w	date;
dt_validade_nova_w		date;
ie_regra_geral_w		varchar2(15);
nr_sequencia_w			number(15);
nr_seq_regra_copia_w	number(15);
ie_manter_intervalo_w	varchar2(1);
qt_operacao_w			number(15,4);
ie_operacao_w			varchar2(15);
ds_horarios_w			varchar2(2000);
ds_horarios2_w			varchar2(2000);
hr_prim_horario_w		varchar2(5);
ds_erro_w				Varchar2(255);
nr_ocorrencia_w			number(15,4);
dt_prim_horario_ant_w	date;
dt_ultimo_horario_w	date;

cursor c01 is
	select  a.nr_sequencia,
			a.ie_origem_inf,
			a.cd_material,
			a.cd_unidade_medida,
			a.qt_dose,
			a.qt_unitaria,
			a.qt_material,
			dt_prescricao_p dt_prescricao,
			nm_usuario_p,
			a.cd_intervalo,
			a.ds_horarios,
			a.ds_observacao,
			a.ds_observacao_enf,
			a.ie_via_aplicacao,
			a.nr_agrupamento  nr_agrupamento,
			nvl(a.ie_cobra_paciente,'S') ie_cobra_paciente,
			decode(nvl(a.ie_regra_disp,'X'), 'D', a.cd_motivo_baixa, decode(nvl(a.ie_cobra_paciente,'S'), 'S', 0, a.cd_motivo_baixa)) cd_motivo_baixa,
			decode(nvl(a.ie_regra_disp,'X'), 'D', sysdate, decode(nvl(a.ie_cobra_paciente,'S'), 'S', null, sysdate)) dt_baixa,
			a.ie_utiliza_kit,
			a.cd_unidade_medida_dose,
			a.qt_conversao_dose,
			'N' ie_urgencia,
			a.ie_urgencia ie_urgencia_rep,
			a.nr_ocorrencia,
			a.qt_total_dispensar,
			a.cd_fornec_consignado,
			a.nr_sequencia_solucao,
			a.nr_sequencia_proc,
			a.qt_solucao,
			null hr_dose_especial,
			null qt_dose_especial,
			a.ds_dose_diferenciada,
			a.ie_medicacao_paciente,
			a.nr_sequencia_diluicao,
			decode(a.ie_se_necessario, 'S', null, a.hr_prim_horario) hr_prim_horario,
			decode(a.nr_sequencia_dieta, null,null,a.nr_sequencia_dieta) nr_sequencia_dieta, 
			a.ie_agrupador,
			a.nr_dia_util,
			decode(b.ie_situacao, 'A', 'N', 'S')  ie_suspenso,
			a.ie_se_necessario,
			a.qt_min_aplicacao,
			a.ie_bomba_infusao,
			nvl(a.ie_aplic_bolus,'N') ie_aplic_bolus,
			nvl(a.ie_aplic_lenta,'N') ie_aplic_lenta,
			nvl(a.ie_acm,'N') ie_acm,
			a.ie_objetivo,
			a.cd_topografia_cih,
			a.ie_origem_infeccao,
			a.cd_amostra_cih,
			a.cd_microorganismo_cih,
			nvl(a.ie_uso_antimicrobiano,'N') ie_uso_antimicrobiano,
			a.cd_protocolo,
			a.nr_seq_protocolo,
			a.nr_seq_mat_protocolo,
			a.qt_hora_aplicacao,
			'N' ie_recons_diluente_fixo,
			a.qt_vel_infusao,
			a.ds_justificativa,
			a.ie_sem_aprazamento,
			a.ie_indicacao,
			a.dt_proxima_dose,
			a.qt_total_dias_lib,
			a.nr_seq_substituto,
			a.ie_lado,
			a.dt_inicio_medic,
			a.qt_dia_prim_hor,
			decode(nvl(a.ie_regra_disp,'X'), 'D', a.ie_regra_disp, null) ie_regra_disp,
			a.qt_vol_adic_reconst,
			a.qt_hora_intervalo,
			a.qt_min_intervalo,
			a.nr_sequencia nr_seq_anterior
	From	Material b,
			Prescr_Material a
	where	a.nr_sequencia_diluicao is null
	and		a.ie_suspenso 	<> 'S'
	and		a.ie_origem_inf	<> 'K'
	and		verifica_medic_padronizado(a.nr_prescricao, a.cd_material, ie_padronizado_w) = 'S'
	and		b.ie_situacao	= 'A'
	and		a.cd_material 	= b.cd_material
	and		a.ie_agrupador = 12
	and		a.nr_prescricao = nr_prescricao_orig_p;

c01_w	c01%rowtype;

cursor c02 is
select	nvl(ie_copiar, 'N'),
		ie_regra_geral,
		nr_sequencia,
		nvl(ie_manter_intervalo, 'N')
from	rep_regra_copia_crit
where	(nvl(ie_se_necessario,'S') = 'S'        or nvl(c01_w.ie_se_necessario,'N') <> 'S')
and		(nvl(ie_acm,'S') = 'S'                  or nvl(c01_w.ie_acm,'N') <> 'S')
and		(nvl(ie_agora,'S') = 'S'                or nvl(c01_w.ie_urgencia_rep,'N') <> 'S')
and		ie_tipo_item    = 'SUP'
and		nr_seq_regra    = nr_seq_regra_p
order by nr_seq_apres;

begin

select	nvl(max(cd_estabelecimento),1),
		max(cd_setor_atendimento),
		max(nr_atendimento),
		max(dt_prescricao),
		max(dt_inicio_prescr),
		nvl(max(nr_horas_validade), 24)
into	cd_estabelecimento_w,
		cd_setor_atendimento_w,
		nr_atendimento_w,
		dt_prescricao_w,
		dt_inicio_prescr_w,
		nr_horas_validade_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

Obter_Param_Usuario(924,18,Obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_padronizado_w);

nr_seq_material_w := 1;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	ie_copia_w := 'S';
	
	open C02; 
	loop
	fetch C02 into	
		ie_copia_w,
		ie_regra_geral_w,
		nr_seq_regra_copia_w,
		ie_manter_intervalo_w;
	exit when C02%notfound;
		begin
		ie_copia_w := ie_copia_w;
		end;
	end loop; 
	close C02;
	
	if	(ie_copia_w	= 'S') then
		select	nvl(max(nr_agrupamento),0) + 1
		into	nr_agrup_acum_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p;
		
		ie_loop_w	:= 'S';
		while	(ie_loop_w = 'S') loop
			select	count(*)
			into	qt_itens_w
			from	prescr_material
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nvl(nr_seq_material_w,1);
			
			if	(qt_itens_w	= 0) then
				ie_loop_w	:= 'N';
			else	
				nr_seq_material_w	:= nvl(nr_seq_material_w,0) + 1;
			end if;
		end loop;
		
		if (ie_regra_geral_w = 'H') then
			if	(ie_manter_intervalo_w = 'S') and
				(c01_w.cd_intervalo is not null) then
				
				select	max(ie_operacao),
						max(qt_operacao)
				into	ie_operacao_w,
						qt_operacao_w
				from	intervalo_prescricao
				where	cd_intervalo = c01_w.cd_intervalo;

				select	max(ie_operacao),
						max(qt_operacao)
				into	ie_operacao_w,
						qt_operacao_w
				from	intervalo_prescricao
				where	cd_intervalo = c01_w.cd_intervalo;
				
				--qt_operacao_w	:= obter_ocorrencia_intervalo(c01_w.cd_intervalo, nr_horas_validade_w, 'H');

				if	(ie_operacao_w in ('F','V')) then
					hr_prim_horario_w := obter_primeiro_horario(c01_w.cd_intervalo, nr_prescricao_p, c01_w.cd_material, c01_w.ie_via_aplicacao);
				end if;

				dt_inicio_w := dt_inicio_prescr_w - 5;
				dt_final_w := sysdate + 5;

				dt_inicio_item_w := dt_inicio_prescr_w - qt_operacao_w / 24;

				select	max(c.dt_horario)
				into	dt_inicio_item_w
				from	prescr_mat_hor c,
						prescr_material b,
						prescr_medica a
				where	c.nr_seq_material = b.nr_sequencia
				and		c.nr_prescricao = b.nr_prescricao
				and		nvl(c.ie_situacao,'A') = 'A'
				and		c.dt_horario >= dt_inicio_item_w - 220/24
				and		b.dt_suspensao is null
				and		b.ie_agrupador = 12
				and		nvl(c.ie_dose_especial,'N') <> 'S'
				and		b.cd_intervalo = nvl(c01_w.cd_intervalo, b.cd_intervalo)
				and		nvl(b.qt_dose,0) = nvl(c01_w.qt_dose, nvl(b.qt_dose,0))
				and		b.cd_material = c01_w.cd_material
				and		b.nr_prescricao = a.nr_prescricao
				and		a.dt_suspensao is null
				and		nvl(a.dt_liberacao_medico, a.dt_liberacao) is not null
				--and		Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
				and		a.dt_inicio_prescr between dt_inicio_w and dt_final_w
				and		a.nr_atendimento = nr_atendimento_w;

				if	(dt_inicio_item_w is not null) then
					dt_inicio_item_w := dt_inicio_item_w + qt_operacao_w / 24;
				end if;

				dt_inicio_item_w := nvl(dt_inicio_item_w,dt_inicio_prescr_w);

				if	(nvl(qt_operacao_w,0) > 0) and
					(dt_inicio_item_w is not null) then
					
					if	(ie_operacao_w in ('H')) and
						(dt_inicio_item_w not between dt_inicio_prescr_w and (dt_inicio_item_w + qt_operacao_w)) then

						while (dt_inicio_item_w not between dt_inicio_prescr_w and (dt_inicio_item_w + qt_operacao_w)) loop
							dt_inicio_item_w := dt_inicio_item_w + qt_operacao_w/24;
						end loop;
					end if;
				else
					if	(c01_w.ie_acm = 'S') then
						c01_w.ds_horarios := 'ACM';
					end if;

					if	(c01_w.ie_se_necessario = 'S') then
						c01_w.ds_horarios := 'SN';
					end if;
				end if;
				
				if	(c01_w.hr_prim_horario is not null) then
					c01_w.hr_prim_horario	:= to_char(dt_inicio_item_w, 'hh24:mi');
				end if;

				c01_w.nr_ocorrencia := 0;
				Calcular_Horario_Prescricao(nr_prescricao_p, c01_w.cd_intervalo, dt_inicio_prescr_w, dt_inicio_item_w, nr_horas_validade_w, c01_w.cd_material, 0, 0, c01_w.nr_ocorrencia, ds_horarios_w, ds_horarios2_w, 'N', c01_w.ds_dose_diferenciada);
				c01_w.ds_horarios := ds_horarios_w || ds_horarios2_w;
				
				c01_w.qt_dia_prim_hor := 0;
				
				if	(hr_prim_horario_w <> '') and
					(hr_prim_horario_w <> ' : ') and
					(hr_prim_horario_w < to_char(dt_inicio_prescr_w,'hh24:mi')) then
					c01_w.qt_dia_prim_hor := 1;
				end if;
				
			elsif (ie_manter_intervalo_w = 'N') then
			
				dt_ultimo_horario_w	:= PLT_obter_ultimo_horario(nr_prescricao_orig_p, c01_w.nr_seq_anterior, 'S', nm_usuario_p);

				if	(dt_ultimo_horario_w is not null) and
					(dt_ultimo_horario_w > (sysdate - 1)) then

					hr_prim_horario_w	:= to_char(dt_ultimo_horario_w + /*nvl(*/Obter_ocorrencia_intervalo(c01_w.cd_intervalo, nvl(nr_horas_validade_w,24),'H')/ 24,'hh24:mi');
				end if;
					
				if	(hr_prim_horario_w <> '  :  ') and
					(hr_prim_horario_w is not null) then
					c01_w.ds_horarios	:= '';
					
					if	(dt_prim_horario_ant_w is null) then
						dt_prim_horario_ant_w	:= dt_inicio_prescr_w;
					end if;
						
					Calcular_Horario_Prescricao(nr_prescricao_p,
												c01_w.cd_intervalo,
												dt_inicio_prescr_w,
												dt_prim_horario_ant_w,
												nr_horas_validade_w,
												null,
												0,
												0,
												c01_w.nr_ocorrencia,
												ds_horarios_w,
												ds_horarios2_w,
												'N',
												null);
												
					c01_w.ds_horarios := ds_horarios_w||ds_horarios2_w;
				else
					c01_w.ds_horarios	:= '';
				end if;
			
			end if;
		end if;
		
		insert  into prescr_material (
				nr_prescricao,
				nr_sequencia,
				ie_origem_inf,
				cd_material,
				cd_unidade_medida,
				qt_dose,
				qt_unitaria,
				qt_material,
				dt_atualizacao,
				nm_usuario,
				cd_intervalo,
				ds_horarios,
				ds_observacao,
				ds_observacao_enf,
				ie_via_aplicacao,
				nr_agrupamento,
				ie_cobra_paciente,
				cd_motivo_baixa,
				dt_baixa,
				ie_utiliza_kit,
				cd_unidade_medida_dose,
				qt_conversao_dose,
				ie_urgencia,
				nr_ocorrencia,
				qt_total_dispensar,
				cd_fornec_consignado,
				nr_sequencia_solucao,
				nr_sequencia_proc,
				qt_solucao,
				hr_dose_especial,
				qt_dose_especial,
				ds_dose_diferenciada,
				ie_medicacao_paciente,
				nr_sequencia_diluicao,
				hr_prim_horario,
				nr_sequencia_dieta,
				ie_agrupador,
				nr_dia_util,
				ie_suspenso,
				ie_se_necessario,
				qt_min_aplicacao,
				ie_bomba_infusao,
				ie_aplic_bolus,
				ie_aplic_lenta,
				ie_acm,
				ie_objetivo,
				cd_topografia_cih,
				ie_origem_infeccao,
				cd_amostra_cih,
				cd_microorganismo_cih,
				ie_uso_antimicrobiano,
				cd_protocolo,
				nr_seq_protocolo,
				nr_seq_mat_protocolo,
				qt_hora_aplicacao,
				ie_recons_diluente_fixo,
				qt_vel_infusao,
				ds_justificativa,
				ie_sem_aprazamento,
				ie_indicacao,
				dt_proxima_dose,
				qt_total_dias_lib,
				nr_seq_substituto,
				ie_lado,
				dt_inicio_medic,
				qt_dia_prim_hor,
				ie_regra_disp,
				qt_vol_adic_reconst,
				qt_hora_intervalo,
				qt_min_intervalo,
				nr_seq_anterior,
				nr_prescricao_original)
		values(		nr_prescricao_p,
				nr_seq_material_w,
				c01_w.ie_origem_inf,
				c01_w.cd_material,
				c01_w.cd_unidade_medida,
				c01_w.qt_dose,
				c01_w.qt_unitaria,
				c01_w.qt_material,
				sysdate,
				nm_usuario_p,
				c01_w.cd_intervalo,
				c01_w.ds_horarios,
				c01_w.ds_observacao,
				c01_w.ds_observacao_enf,
				c01_w.ie_via_aplicacao,
				nr_agrup_acum_w,
				c01_w.ie_cobra_paciente,
				c01_w.cd_motivo_baixa,
				c01_w.dt_baixa,
				c01_w.ie_utiliza_kit,
				c01_w.cd_unidade_medida_dose,
				c01_w.qt_conversao_dose,
				c01_w.ie_urgencia,
				c01_w.nr_ocorrencia,
				c01_w.qt_total_dispensar,
				c01_w.cd_fornec_consignado,
				c01_w.nr_sequencia_solucao,
				c01_w.nr_sequencia_proc,
				c01_w.qt_solucao,
				c01_w.hr_dose_especial,
				c01_w.qt_dose_especial,
				c01_w.ds_dose_diferenciada,
				c01_w.ie_medicacao_paciente,
				c01_w.nr_sequencia_diluicao,
				c01_w.hr_prim_horario,
				c01_w.nr_sequencia_dieta,
				c01_w.ie_agrupador,
				c01_w.nr_dia_util,
				c01_w.ie_suspenso,
				c01_w.ie_se_necessario,
				c01_w.qt_min_aplicacao,
				c01_w.ie_bomba_infusao,
				c01_w.ie_aplic_bolus,
				c01_w.ie_aplic_lenta,
				c01_w.ie_acm,
				c01_w.ie_objetivo,
				c01_w.cd_topografia_cih,
				c01_w.ie_origem_infeccao,
				c01_w.cd_amostra_cih,
				c01_w.cd_microorganismo_cih,
				c01_w.ie_uso_antimicrobiano,
				c01_w.cd_protocolo,
				c01_w.nr_seq_protocolo,
				c01_w.nr_seq_mat_protocolo,
				c01_w.qt_hora_aplicacao,
				c01_w.ie_recons_diluente_fixo,
				c01_w.qt_vel_infusao,
				c01_w.ds_justificativa,
				c01_w.ie_sem_aprazamento,
				c01_w.ie_indicacao,
				c01_w.dt_proxima_dose,
				c01_w.qt_total_dias_lib,
				c01_w.nr_seq_substituto,
				c01_w.ie_lado,
				c01_w.dt_inicio_medic,
				c01_w.qt_dia_prim_hor,
				c01_w.ie_regra_disp,
				c01_w.qt_vol_adic_reconst,
				c01_w.qt_hora_intervalo,
				c01_w.qt_min_intervalo,
				c01_w.nr_seq_anterior,
				nr_prescricao_orig_p);
				
	Obter_Quant_Dispensar(cd_estabelecimento_w, c01_w.cd_material, nr_prescricao_p, nr_seq_material_w, c01_w.cd_intervalo,c01_w.ie_via_aplicacao, c01_w.qt_unitaria, 0,c01_w.nr_ocorrencia, c01_w.ds_dose_diferenciada,c01_w.ie_origem_inf, c01_w.cd_unidade_medida_dose, 1, c01_w.qt_material, c01_w.qt_total_dispensar, c01_w.ie_regra_disp, ds_erro_w,c01_w.ie_se_necessario,c01_w.ie_acm);
	
	update	prescr_material
	set 	nr_ocorrencia	= c01_w.nr_ocorrencia,
			qt_total_dispensar	= c01_w.qt_total_dispensar,
			qt_material		= nvl(c01_w.qt_material,1),		
			ie_regra_disp	= decode(nvl(ie_regra_disp,'X'), 'D', ie_regra_disp, c01_w.ie_regra_disp)
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia		= nr_seq_material_w;			
				
				
	end if;
	end;
end loop;
close C01;

commit;

end REP_copia_suplemento;
/