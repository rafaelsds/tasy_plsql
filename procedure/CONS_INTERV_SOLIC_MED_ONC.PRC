create or replace 
procedure Cons_interv_solic_med_Onc(
				nr_prescricao_p			Number,
				cd_pessoa_fisica_p		varchar2,
				nr_seq_atendimento_p	number,
				cd_material_p			varchar2,
				ds_erro_p				Out		Varchar2) is


ds_erro_w			Varchar2(2000) := '';
cd_material_w			Number(6);
qt_dias_intervalo_w		Number(15);
cd_grupo_material_w		Number(3);
cd_subgrupo_material_w		Number(3);
cd_classe_material_w		Number(5);
cd_material_regra_w		Number(6);
cd_estabelecimento_w		Number(4);
nr_atendimento_w			Number(10);
cd_grupo_material_prescr_w		Number(3);
cd_subgrupo_material_prescr_w	Number(3);
cd_classe_material_prescr_w		Number(5);
dt_ultima_prescricao_w		Date;
dt_prescricao_w			Date;
ds_material_w			Varchar2(255);
ie_considera_kit_w		Varchar2(2);
ie_todas_atend_w		Varchar2(2);
ds_material_prob_w		Varchar2(2000) := '';
cd_convenio_w			Number(5);
ds_justificativa_w			Varchar2(255);
qt_permitida_w			Number(9,3)	:= 1;
qt_executada_periodo_w		Number(15,3);
cd_setor_atendimento_w		Number(10,0);
cd_protocolo_w			number(10,0);
nr_seq_medicacao_w		number(10,0);	

Cursor C01 is
	Select	distinct cd_material,
			ds_material,
			null
	from	material
	where	cd_material	= cd_material_p;

Cursor C02 is
	select	qt_dias_intervalo,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		nvl(qt_permitida,1),
		nvl(ie_considera_kit,'S'),
		nvl(ie_todas_atend,'N')
	from	REGRA_INTERV_MED_ONC
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	nvl(cd_grupo_material,nvl(cd_grupo_material_prescr_w,0))	= nvl(cd_grupo_material_prescr_w,0)
	and	nvl(cd_subgrupo_material,nvl(cd_subgrupo_material_prescr_w,0))	= nvl(cd_subgrupo_material_prescr_w,0)
	and	nvl(cd_classe_material,nvl(cd_classe_material_prescr_w,0))	= nvl(cd_classe_material_prescr_w,0)
	and	nvl(cd_setor_atendimento,nvl(cd_setor_atendimento_w,0))		= nvl(cd_setor_atendimento_w,0)
	and	nvl(cd_protocolo,nvl(cd_protocolo_w,0))				= nvl(cd_protocolo_w,0)
	and	nvl(nr_seq_medicacao,nvl(nr_seq_medicacao_w,0))		 	= nvl(nr_seq_medicacao_w,0)
	and	((cd_convenio is null) or (cd_convenio = cd_convenio_w))
	order by
		nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_convenio,0);

BEGIN



select	b.cd_estabelecimento,
	b.cd_setor_atendimento,
	a.dt_real,
	cd_protocolo,
	nr_seq_medicacao
into	cd_estabelecimento_w,
	cd_setor_atendimento_w,
	dt_prescricao_w,
	cd_protocolo_w,
	nr_seq_medicacao_w
from	paciente_atendimento a,
	paciente_setor b
where	b.nr_seq_paciente	= a.nr_seq_paciente
and	a.nr_seq_atendimento	= nr_seq_atendimento_p;

nr_atendimento_w	:= obter_ultimo_atendimento(cd_pessoa_fisica_p);
cd_convenio_w		:= 0;

if	(nr_atendimento_w	is not null) then
	cd_convenio_w		:= obter_convenio_atendimento(nr_atendimento_w);

end if;

open C01;
loop
fetch C01 into	
	cd_material_w,
	ds_material_w,
	ds_justificativa_w;
exit when C01%notfound;	
	
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_prescr_w,
		cd_subgrupo_material_prescr_w,
		cd_classe_material_prescr_w
	from	estrutura_material_v
	where	cd_material	= cd_material_w;

	open c02;
	loop
	fetch c02 into	
		qt_dias_intervalo_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		cd_material_regra_w,
		qt_permitida_w,
		ie_considera_kit_w,
		ie_todas_atend_w;
	exit when c02%notfound;
		qt_dias_intervalo_w	:= qt_dias_intervalo_w;
	end loop;
	close c02;
	
	dt_ultima_prescricao_w	:= null;
	
	if	(cd_material_regra_w is not null)  then
		begin
		select	max(a.dt_prescricao)
		into	dt_ultima_prescricao_w
		from	prescr_material b,
			prescr_medica a
		where	a.nr_prescricao		= b.nr_prescricao
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.nr_prescricao		<> nr_prescricao_p
		and	b.cd_material		= cd_material_w
		and	((ie_considera_kit_w	= 'S') or
			 (b.nr_seq_kit	is null))
		and	((a.dt_liberacao is not null) or
			 (a.dt_liberacao_medico is not null));
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;
		
		begin
		if	(ie_todas_atend_w = 'N') then
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	prescr_material	b,
				prescr_medica 	a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= cd_material_w
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	a.dt_prescricao between (dt_prescricao_w - (qt_dias_intervalo_w )) and dt_prescricao_w
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		else
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	prescr_material	b,
				prescr_medica 	a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= cd_material_w
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;		
		end if;
		exception
			when others then
				qt_executada_periodo_w	:= 0;
		end;
		
	elsif	(cd_classe_material_w is not null) then
		begin
		select	max(a.dt_prescricao)
		into	dt_ultima_prescricao_w
		from	estrutura_material_v c,
			prescr_material b,
			prescr_medica a
		where	a.nr_prescricao		= b.nr_prescricao
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.nr_prescricao		<> nr_prescricao_p
		and	b.cd_material		= c.cd_material
		and	c.cd_classe_material	= cd_classe_material_w
		and	((ie_considera_kit_w	= 'S') or
			 (b.nr_seq_kit	is null))
		and	a.dt_suspensao is null
		and	nvl(b.ie_suspenso,'N')	<> 'S'
		and	((a.dt_liberacao is not null) or
			 (a.dt_liberacao_medico is not null));
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;

		begin
		
		if	(ie_todas_atend_w = 'N') then
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	estrutura_material_v c,
				prescr_material b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= c.cd_material
			and	c.cd_classe_material	= cd_classe_material_w
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	a.dt_suspensao is null
			and	nvl(b.ie_suspenso,'N')	<> 'S'
			and	a.dt_prescricao between (dt_prescricao_w - (qt_dias_intervalo_w)) and dt_prescricao_w
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		else
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	estrutura_material_v c,
				prescr_material b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= c.cd_material
			and	c.cd_classe_material	= cd_classe_material_w
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	a.dt_suspensao is null
			and	nvl(b.ie_suspenso,'N')	<> 'S'
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		end if;
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;
	elsif	(cd_subgrupo_material_w is not null) then
		begin
		select	max(a.dt_prescricao)
		into	dt_ultima_prescricao_w
		from	estrutura_material_v c,
			prescr_material b,
			prescr_medica a
		where	a.nr_prescricao		= b.nr_prescricao
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.nr_prescricao		<> nr_prescricao_p
		and	b.cd_material		= c.cd_material
		and	c.cd_subgrupo_material	= cd_subgrupo_material_w
		and	((ie_considera_kit_w	= 'S') or
			 (b.nr_seq_kit	is null))
		and	a.dt_suspensao is null
		and	nvl(b.ie_suspenso,'N')	<> 'S'
		and	((a.dt_liberacao is not null) or
			 (a.dt_liberacao_medico is not null));
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;

		begin
		
		if	(ie_todas_atend_w = 'N') then
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	estrutura_material_v c,
				prescr_material b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= c.cd_material
			and	c.cd_subgrupo_material	= cd_subgrupo_material_w
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	a.dt_suspensao is null
			and	nvl(b.ie_suspenso,'N')	<> 'S'
			and	a.dt_prescricao between (dt_prescricao_w - (qt_dias_intervalo_w)) and dt_prescricao_w
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		else
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	estrutura_material_v c,
				prescr_material b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= c.cd_material
			and	c.cd_subgrupo_material	= cd_subgrupo_material_w
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	a.dt_suspensao is null
			and	nvl(b.ie_suspenso,'N')	<> 'S'
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		end if;
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;
	elsif	(cd_grupo_material_w is not null) then
		begin
		select	max(a.dt_prescricao)
		into	dt_ultima_prescricao_w
		from	estrutura_material_v c,
			prescr_material b,
			prescr_medica a
		where	a.nr_prescricao		= b.nr_prescricao
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
		and	a.nr_prescricao		<> nr_prescricao_p
		and	b.cd_material		= c.cd_material
		and	c.cd_grupo_material	= cd_grupo_material_w
		and	((ie_considera_kit_w	= 'S') or
			 (b.nr_seq_kit	is null))
		and	a.dt_suspensao is null
		and	nvl(b.ie_suspenso,'N')	<> 'S'
		and	((a.dt_liberacao is not null) or
			 (a.dt_liberacao_medico is not null));
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;

		begin
		if	(ie_todas_atend_w = 'N') then
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	estrutura_material_v c,
				prescr_material b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= c.cd_material
			and	c.cd_grupo_material	= cd_grupo_material_w
			and	a.dt_suspensao is null
			and	nvl(b.ie_suspenso,'N')	<> 'S'
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	a.dt_prescricao between (dt_prescricao_w - (qt_dias_intervalo_w)) and dt_prescricao_w
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		else
			select	decode(b.ie_agrupador,1,nvl(sum(b.qt_dose),0),2,nvl(sum(b.qt_unitaria),0))
			into	qt_executada_periodo_w
			from	estrutura_material_v c,
				prescr_material b,
				prescr_medica a
			where	a.nr_prescricao		= b.nr_prescricao
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			and	b.cd_material		= c.cd_material
			and	c.cd_grupo_material	= cd_grupo_material_w
			and	a.dt_suspensao is null
			and	nvl(b.ie_suspenso,'N')	<> 'S'
			and	((ie_considera_kit_w	= 'S') or
				 (b.nr_seq_kit	is null))
			and	(((a.dt_liberacao is not null) or
				  (a.dt_liberacao_medico is not null)) or
				 (a.nr_prescricao = nr_prescricao_p))
			group by
				b.ie_agrupador;
		end if;
		exception
			when others then
				dt_ultima_prescricao_w	:= null;
		end;
	end if;
			
	if	(dt_ultima_prescricao_w is not null) and
		(((dt_prescricao_w - dt_ultima_prescricao_w) * 24) < qt_dias_intervalo_w) then	
		ds_material_prob_w	:= substr(ds_material_prob_w ||ds_material_w||' - '||qt_dias_intervalo_w||'h / ',1,2000);
	end if;
	if	(qt_executada_periodo_w > qt_permitida_w) then
		ds_material_prob_w	:= substr(ds_material_prob_w ||ds_material_w||' - '||qt_dias_intervalo_w||'h / ',1,2000);
	end if;
end loop;
close C01;

if	(ds_material_prob_w is not null) and
	(ds_justificativa_w is null) then
	ds_erro_w	:= qt_dias_intervalo_w||'h ';
end if;

ds_erro_p	:= ds_erro_w;

END Cons_interv_solic_med_Onc;
/