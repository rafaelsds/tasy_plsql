create or replace
procedure pls_imp_nv_diagnostico_conta(	nr_seq_conta_p		in pls_conta_imp.nr_sequencia%type,
					ie_indicacao_acidente_p	in pls_diagnostico_conta.ie_indicacao_acidente%type,
					cd_doenca_p		in pls_diagnostico_conta_imp.cd_doenca%type,
					cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		in usuario.nm_usuario%type) is

begin

-- se for para usar a nova forma de importa��o XML chama da package, caso contr�rio chama a rotina antiga
if	(pls_imp_xml_cta_pck.usar_nova_imp_xml(cd_estabelecimento_p) = 'S') then
	
	pls_imp_xml_cta_pck.pls_imp_nv_diagnostico_conta(	nr_seq_conta_p, cd_doenca_p, nm_usuario_p);
else
	-- rotinas da estrutura antiga
	-- com o tempo a mesma deve sair daqui e ficar s� o novo m�todo de implementa��o
	pls_imp_diagnostico_conta
			(	null, cd_doenca_p,
				null, nr_seq_conta_p,
				null, ie_indicacao_acidente_p,
				null, null,
				nm_usuario_p);
end if;

end pls_imp_nv_diagnostico_conta;
/
