create or replace 
procedure Calcular_Agua_Nut_Pac_Ped(	
				nr_sequencia_p     		number,
				nm_usuario_p         	varchar2) is

qt_peso_w					number(15,3);	
nr_seq_elem_agua_w			number(10);
qt_vol_total_w				number(15,4);
qt_aporte_hidrico_diario_w	number(15,4);
qt_elem_kg_dia_w			number(15,4);
ie_arredonda_npt_w			parametro_medico.ie_arredondar_npt%type := 'N';

begin

select	qt_peso,
		qt_aporte_hidrico_diario
into	qt_peso_w,
		qt_aporte_hidrico_diario_w
from	Nut_pac
where	nr_sequencia		= nr_sequencia_p;

select 	nvl(max(ie_arredondar_npt),'N')
into	ie_arredonda_npt_w
from	parametro_medico
where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

/*Rotina para atualizar o volume do elemento do tipo �gua para ficar compat�vel com Aporte h�drico */
select	max(a.nr_sequencia)
into	nr_seq_elem_agua_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'A';

if	(nr_seq_elem_agua_w > 0) then
	begin
			
		if (ie_arredonda_npt_w = 'N') then
		
			select	nvl(sum(qt_volume_final),0)
			into	qt_vol_total_w	
			from	nut_pac_elemento
			where	nr_seq_nut_pac	= nr_sequencia_p;	
					
			if	(qt_aporte_hidrico_diario_w > qt_vol_total_w) then
				begin
					
					select	sum(qt_elem_kg_dia)
					into	qt_elem_kg_dia_w
					from	nut_pac_elemento
					where	nr_seq_nut_pac	= nr_sequencia_p
					and		nr_sequencia	= nr_seq_elem_agua_w;
							
					qt_elem_kg_dia_w	:= qt_elem_kg_dia_w + (dividir((qt_aporte_hidrico_diario_w - qt_vol_total_w),qt_peso_w));
					qt_elem_kg_dia_w	:= ceil(qt_elem_kg_dia_w * 100) / 100;
					
					update	nut_pac_elemento
					set		qt_elem_kg_dia	=  nvl(qt_elem_kg_dia_w,0)
					where	nr_seq_nut_pac	= nr_sequencia_p
					and		nr_sequencia	= nr_seq_elem_agua_w;

				end;
			elsif	(qt_aporte_hidrico_diario_w < qt_vol_total_w) then
				begin
				
					select	sum(qt_elem_kg_dia)
					into	qt_elem_kg_dia_w
					from	nut_pac_elemento
					where	nr_seq_nut_pac	= nr_sequencia_p
					and		nr_sequencia	= nr_seq_elem_agua_w;
					
					qt_elem_kg_dia_w	:= qt_elem_kg_dia_w - (dividir((qt_vol_total_w - qt_aporte_hidrico_diario_w),qt_peso_w));
					qt_elem_kg_dia_w	:= ceil(qt_elem_kg_dia_w * 100) / 100;
					
					update	nut_pac_elemento
					set		qt_elem_kg_dia	= nvl(qt_elem_kg_dia_w,0)
					where	nr_seq_nut_pac	= nr_sequencia_p
					and		nr_sequencia	= nr_seq_elem_agua_w;
			
				end;
			end if;		
		else
			
			select	nvl(sum(qt_volume_final),0)
			into	qt_vol_total_w	
			from	nut_pac_elemento
			where	nr_seq_nut_pac	= nr_sequencia_p
			and		nr_sequencia	<> nr_seq_elem_agua_w;
			
			update	nut_pac_elemento
			set		qt_elem_kg_dia	= (ceil((qt_aporte_hidrico_diario_w - qt_vol_total_w) * 100) / 100) / qt_peso_w,
					qt_diaria		= ceil((qt_aporte_hidrico_diario_w - qt_vol_total_w) * 100) / 100,
					qt_volume_final	= ceil((qt_aporte_hidrico_diario_w - qt_vol_total_w) * 100) / 100
			where 	nr_seq_nut_pac	= nr_sequencia_p
			and		nr_sequencia	= nr_seq_elem_agua_w;
			
		end if;
	end;
end if;

commit;

end Calcular_Agua_Nut_Pac_Ped;
/