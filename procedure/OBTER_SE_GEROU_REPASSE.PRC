create or replace
procedure Obter_Se_Gerou_Repasse(
			nr_seq_protocolo_p		number,
			ie_erro_p		out	varchar2) is 
			
ie_erro_w		varchar2(1) := 'N';
qt_reg_w		number;

begin

if	(nr_seq_protocolo_p is not null) and (nr_seq_protocolo_p <> 0) then

	select	sum(nvl(qt_reg,0))
	into	qt_reg_w	
	from	(
		select	count(*) qt_reg
		from	procedimento_repasse	a,
			procedimento_paciente	b,
			conta_paciente		c
		where	a.nr_seq_procedimento(+)= b.nr_sequencia
		and	b.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_p
		and	nvl(a.vl_repasse,0) 	= 0
		and	b.ie_responsavel_credito= 'RM'
		union all
		select	count(*) qt_reg
		from	material_repasse	a,
			material_atend_paciente	b,
			conta_paciente		c
		where	a.nr_seq_material(+) 	= b.nr_sequencia
		and	b.nr_interno_conta	= c.nr_interno_conta
		and	c.nr_seq_protocolo	= nr_seq_protocolo_p
		and	nvl(a.vl_repasse,0) 	= 0
		and	b.ie_responsavel_credito= 'RM'
		);
	
end if;

if	(qt_reg_w > 0) then
	ie_erro_w	:= 'S';
end if;

ie_erro_p	:= ie_erro_w;

end Obter_Se_Gerou_Repasse;
/