create or replace
procedure alterar_prescr_medica(dt_entrada_unidade_p	date,
				nr_atendimento_p	number,
				nr_prescricao_p		number,			
				nm_usuario_p		varchar2) is 

begin
if	(dt_entrada_unidade_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	prescr_medica
	set	dt_entrada_unidade	= dt_entrada_unidade_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_prescricao		= nr_prescricao_p
	and	nr_atendimento		= nr_atendimento_p;
	end;
end if;



commit;

end alterar_prescr_medica;
/			