create or replace
procedure pls_lote_imp_exclusao_benef
			(	nr_contrato_p			Number,
				nr_intercambio_p		Number,
				cd_usuario_plano_p		Varchar2,
				nr_seq_motivo_rescisao_p	Number,
				dt_inicial_rescisao_p		Varchar2,
				ie_rescisao_tipo_p		Varchar2,
				ie_devolucao_carteira_p		Varchar2,
				ds_observacao_p			Varchar2,
				nr_seq_lote_p			Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number) is


/*  Procedure utilizada no portal do plano de sa�de */

cd_usuario_plano_imp_w		Varchar2(30);
nr_seq_segurado_w		Number(10);
nr_seq_contrato_w		Number(10);
nr_seq_intercambio_w		Number(10);
cd_pessoa_fisica_w		Varchar(15);
nm_pessoa_fisica_w		Varchar(255);
nr_seq_resc_prog_gerada_w	Number(10);

begin

if	(cd_usuario_plano_p is not null) then
	cd_usuario_plano_imp_w	:= elimina_caracteres_especiais(cd_usuario_plano_p);
	cd_usuario_plano_imp_w	:= somente_numero_char(cd_usuario_plano_imp_w);
	
	select	nvl(max(nr_seq_segurado),0)
	into	nr_seq_segurado_w
	from	pls_segurado_carteira
	where	cd_usuario_plano	= cd_usuario_plano_imp_w;
	
	if	(nr_seq_segurado_w > 0 ) then
		select	nr_seq_contrato,
			cd_pessoa_fisica,
			pls_obter_dados_segurado(nr_sequencia, 'N')
		into	nr_seq_contrato_w,
			cd_pessoa_fisica_w,
			nm_pessoa_fisica_w
		from	pls_segurado
		where	nr_sequencia = nr_seq_segurado_w;
		
		pls_gerar_rescisao_programada(	null, nr_seq_contrato_w, nr_seq_segurado_w,
						nr_seq_motivo_rescisao_p, null, to_date(dt_inicial_rescisao_p,'dd/mm/yyyy'),
						ie_rescisao_tipo_p, ie_devolucao_carteira_p, ds_observacao_p,
						nm_usuario_p, null, null,
						0,null,'S',nr_seq_resc_prog_gerada_w,null);
		
		insert into pls_inclusao_beneficiario(
			nr_sequencia, nm_pessoa_fisica, cd_estabelecimento,
			ie_status, dt_solicitacao, nr_seq_contrato,
			ie_tipo_inclusao, ie_tipo_operacao, dt_atualizacao,
			nr_seq_lote_inclusao, ie_processo, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_segurado,
			nr_seq_motivo_cancelamento, ie_status_mov)
		values(	pls_inclusao_beneficiario_seq.nextval, nm_pessoa_fisica_w, cd_estabelecimento_p,
			'P', sysdate, nr_seq_contrato_w,
			'P', 'E', sysdate,
			nr_seq_lote_p, 'W', nm_usuario_p,
			sysdate, nm_usuario_p, nr_seq_segurado_w,
			nr_seq_motivo_rescisao_p, '1');
	end if;
end if;

commit;

end pls_lote_imp_exclusao_benef;
/
