create or replace
procedure atualizar_mesano_protocolo_js(	nr_interno_conta_p	number,
						nr_protocolo_p		varchar2,
						nr_seq_protocolo_p		number,
						dt_mesano_referencia_p	date,
						nm_usuario_p		varchar2) is 

qt_fatur_etapa_w	number(10,0);
				
begin

if	(nr_interno_conta_p is not null)then
	begin
	
	update 	conta_paciente
	set    	dt_atualizacao 		= sysdate ,
		nm_usuario 		= nm_usuario_p,
		nr_protocolo 		= nr_protocolo_p,
		nr_seq_protocolo 	= nr_seq_protocolo_p,
		dt_mesano_referencia 	= dt_mesano_referencia_p
	where  	nr_interno_conta 	= nr_interno_conta_p;
	
	select 	count(*)
	into	qt_fatur_etapa_w
	from 	fatur_etapa_alta 
	where 	ie_evento = 'B';
	
	if	(qt_fatur_etapa_w > 0)then
		begin
		
		gerar_etapa_inserir_protocolo(nr_interno_conta_p, nm_usuario_p);
		
		end;
	end if;
	
	end;
end if;

commit;

end atualizar_mesano_protocolo_js;
/