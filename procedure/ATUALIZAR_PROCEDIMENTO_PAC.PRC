CREATE OR REPLACE PROCEDURE atualizar_procedimento_pac (
                                                       nr_sequencia_p            IN   procedimento_paciente.nr_sequencia%TYPE,
                                                       nr_prescricao_p           IN   procedimento_paciente.nr_prescricao%TYPE,
                                                       nr_atendimento_p          IN   procedimento_paciente.nr_atendimento%TYPE,
                                                       ds_justificativa_exec_p   IN   procedimento_paciente_dado.ds_justificativa_exec%TYPE DEFAULT NULL
) IS

    ds_indicacao_w            procedimento_paciente.ds_indicacao%TYPE;
    ds_observacao_w           procedimento_paciente.ds_observacao%TYPE;
    cd_setor_atendimento_w    procedimento_paciente.cd_setor_atendimento%TYPE;
    ie_tipo_guia_w            procedimento_paciente.ie_tipo_guia%TYPE;
    nr_doc_convenio_w         procedimento_paciente.nr_doc_convenio%TYPE;
    ie_spect_w                procedimento_paciente.ie_spect%TYPE;
    cd_cgc_prestador_w        procedimento_paciente.cd_cgc_prestador%TYPE;
    cd_medico_req_w           procedimento_paciente.cd_medico_req%TYPE;
    cd_equipamento_w          procedimento_paciente.cd_equipamento%TYPE;
    cd_especialidade_w        procedimento_paciente.cd_especialidade%TYPE;
    ie_funcao_medico_w        procedimento_paciente.ie_funcao_medico%TYPE;
    cd_medico_prev_laudo_w    procedimento_paciente.cd_medico_prev_laudo%TYPE;
    cd_medico_executor_w      procedimento_paciente.cd_medico_executor%TYPE;
    cd_pessoa_fisica_w        procedimento_paciente.cd_pessoa_fisica%TYPE;
    dt_conta_w                procedimento_paciente.dt_conta%TYPE;
    dt_procedimento_w         procedimento_paciente.dt_procedimento%TYPE;
    ie_video_w                procedimento_paciente.ie_video%TYPE;
    nr_sequencia_w            procedimento_paciente_dado.nr_sequencia%TYPE;
    is_inserted_w             VARCHAR2(1);
	
CURSOR c01 IS
SELECT
	nr_sequencia nr_seq_proc
FROM
	procedimento_paciente
WHERE
	nr_prescricao = nr_prescricao_p
	AND nr_atendimento = nr_atendimento_p
	AND nr_sequencia NOT IN (
		SELECT
			nr_seq_proc nr_sequencia
		FROM
			procedimento_paciente proc_pac,
			procedimento_paciente_dado proc_dado
		WHERE
			proc_pac.nr_sequencia = proc_dado.nr_seq_proc
			AND proc_pac.nr_prescricao = nr_prescricao_p
			AND proc_pac.nr_atendimento = nr_atendimento_p);
BEGIN
    SELECT
        MAX(ds_indicacao),
        MAX(ds_observacao),
        MAX(cd_setor_atendimento),
        MAX(ie_tipo_guia),
        MAX(nr_doc_convenio),
        MAX(ie_spect),
        MAX(cd_cgc_prestador),
        MAX(cd_medico_req),
        MAX(cd_equipamento),
        MAX(cd_especialidade),
        MAX(ie_funcao_medico),
        MAX(cd_medico_prev_laudo),
        MAX(cd_medico_executor),
        MAX(cd_pessoa_fisica),
        MAX(dt_conta),
        MAX(dt_procedimento),
        MAX(ie_video)
    INTO
        ds_indicacao_w,
        ds_observacao_w,
        cd_setor_atendimento_w,
        ie_tipo_guia_w,
        nr_doc_convenio_w,
        ie_spect_w,
        cd_cgc_prestador_w,
        cd_medico_req_w,
        cd_equipamento_w,
        cd_especialidade_w,
        ie_funcao_medico_w,
        cd_medico_prev_laudo_w,
        cd_medico_executor_w,
        cd_pessoa_fisica_w,
        dt_conta_w,
        dt_procedimento_w,
        ie_video_w
    FROM
        procedimento_paciente
    WHERE
        nr_sequencia = nr_sequencia_p;

    UPDATE procedimento_paciente
    SET
        ds_indicacao = ds_indicacao_w,
        ds_observacao = ds_observacao_w,
        cd_setor_atendimento = cd_setor_atendimento_w,
        ie_tipo_guia = ie_tipo_guia_w,
        nr_doc_convenio = nr_doc_convenio_w,
        ie_spect = ie_spect_w,
        cd_cgc_prestador = cd_cgc_prestador_w,
        cd_medico_req = cd_medico_req_w,
        cd_equipamento = cd_equipamento_w,
        cd_especialidade = cd_especialidade_w,
        ie_funcao_medico = ie_funcao_medico_w,
        cd_medico_prev_laudo = cd_medico_prev_laudo_w,
        cd_medico_executor = cd_medico_executor_w,
        cd_pessoa_fisica = cd_pessoa_fisica_w,
        dt_conta = dt_conta_w,
        dt_procedimento = dt_procedimento_w,
        ie_video = ie_video_w
    WHERE
        nr_prescricao = nr_prescricao_p
        AND nr_atendimento = nr_atendimento_p
		AND nr_sequencia <> nr_sequencia_p;

	is_inserted_w := 'N';
	
	FOR c01_w IN c01 LOOP
		SELECT
			procedimento_paciente_dado_seq.NEXTVAL
		INTO
			nr_sequencia_w
		FROM
			dual;

		INSERT INTO procedimento_paciente_dado
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nr_seq_proc,
			ds_justificativa_exec)
		VALUES(
			nr_sequencia_w,
			obter_usuario_ativo,
			SYSDATE,
			c01_w.nr_seq_proc,
			ds_justificativa_exec_p);
		
		is_inserted_w := 'S';
	END LOOP;


	IF (ds_justificativa_exec_p IS NOT NULL AND is_inserted_w <> 'S') THEN
		UPDATE procedimento_paciente_dado
		SET
			ds_justificativa_exec = ds_justificativa_exec_p
		WHERE
			nr_seq_proc IN (
				SELECT
					nr_sequencia
				FROM
					procedimento_paciente
				WHERE
					nr_prescricao = nr_prescricao_p
					AND nr_atendimento = nr_atendimento_p
			);
	END IF;
	COMMIT;

END atualizar_procedimento_pac;
/
