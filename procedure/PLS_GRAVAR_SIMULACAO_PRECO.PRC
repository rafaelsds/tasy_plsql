create or replace
procedure pls_gravar_simulacao_preco
			(	ie_tipo_simulacao_p	in	varchar2,
				nm_benef_p		in	varchar2,
				dt_nasc_p		in	varchar2,
				nr_telefone_p		in	varchar2,
				ds_email_p		in	varchar2,
				nr_ddi_p		in	varchar2,
				nr_ddd_p		in	varchar2,
				sg_estado_p		in	varchar2,
				cd_municipio_ibge_p	in	varchar2,
				nr_seq_simulacao_p	in out	number,
				cd_estabelecimento_p	in	estabelecimento.cd_estabelecimento%type) is

nr_seq_simulacao_w      number(10):= 0;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin
nr_seq_simulacao_w 	:= nr_seq_simulacao_p;
cd_estabelecimento_w	:= nvl(cd_estabelecimento_p, pls_parametro_operadora_web('EST'));

if( nr_seq_simulacao_w is null) then
	select	pls_simulacao_preco_seq.nextval
	into	nr_seq_simulacao_w
	from	dual;

	insert into pls_simulacao_preco(
			    nr_sequencia,
			    dt_simulacao,
			    ie_tipo_contratacao,
			    ie_tipo_processo,
			    nm_pessoa,
			    dt_nascimento,
			    nr_telefone,
			    ds_email,
			    cd_estabelecimento,
			    nm_usuario,
			    dt_atualizacao,
			    nm_usuario_nrec,
			    dt_atualizacao_nrec,
			    nr_ddi,
			    nr_ddd,
			    sg_estado,
			    cd_municipio_ibge
	) values (
			    nr_seq_simulacao_w,
			    sysdate,
			    ie_tipo_simulacao_p,
			    'W',
			    nm_benef_p,
			    dt_nasc_p,
			    nr_telefone_p,
			    ds_email_p,
			    nvl(cd_estabelecimento_w,1),
			    'OPS - Portal',
			    sysdate,
			    'OPS - Portal',
			    sysdate,
			    nr_ddi_p,
			    nr_ddd_p,
			    sg_estado_p,
			    cd_municipio_ibge_p
	);
else
	update  pls_simulacao_preco set
		    nm_pessoa 		= nm_benef_p,
		    dt_nascimento 		= dt_nasc_p,
		    nr_telefone 		= nr_telefone_p,
		    ds_email 		= ds_email_p,
		    nr_ddi 			= nr_ddi_p,
		    nr_ddd 		= nr_ddd_p,
		    sg_estado 		= sg_estado_p,
		    cd_municipio_ibge 	= cd_municipio_ibge_p
	where   nr_sequencia = nr_seq_simulacao_w;
end if;       

commit;

nr_seq_simulacao_p := nr_seq_simulacao_w;

end pls_gravar_simulacao_preco;
/
