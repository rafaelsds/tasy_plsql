create or replace
procedure atualizar_obs_farm(	ds_observacao_p		Varchar2,
				nr_prescr_p		number,
				nr_seq_p		number,
				ie_gravar_obs_prescr_p	varchar2,
				nr_prescricao_p		number,
				nr_sequencia_p		number,
				nr_seq_obs_p number default 0) is 

begin

if	(nr_prescr_p is not null) and
	(nr_seq_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) then
	begin

	update 	prescr_material
	set 	ds_observacao_far 	= ds_observacao_p,
			nr_seq_obs 			= nr_seq_obs_p
	where 	nr_prescricao   	= nr_prescr_p
	and 	nr_sequencia      	= nr_seq_p; 

	if	(ie_gravar_obs_prescr_p = 'S') then
		begin
		
		atualizar_obs_medicamentos_rep(nr_prescricao_p, nr_sequencia_p, nr_seq_obs_p);
		
		end;
	end if;
	
	end;
end if;

commit;

end atualizar_obs_farm;
/
