create or replace
procedure calcular_min_dur_lista_espera	(cd_estabelecimento_p	number,
					nr_seq_lista_espera_p		number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2) is

/* PROC CRIADA BASEADA NA PROC   ---- CALCULAR_MIN_DUR_AGENDA_CIR ----*/				
					
/* variaveis */
cd_agenda_w		number(10,0);
cd_medico_exec_w		varchar2(10);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_minuto_duracao_w	number(10,0);

qt_min_proc_w		number(10,0) 	:= 0;
qt_min_agenda_w		number(10,0) 	:= 0;

ie_param_sobreposicao_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_proc_adic_w	varchar2(10);
ie_atualiza_tempo_agenda_w		varchar2(1);
ie_atualiza_tempo_proc_w		varchar2(1);
ie_atual_maior_tempo_proc_w		varchar2(1); 
cd_estab_agenda_w	number(4);
ie_estab_agenda_w	varchar2(1) := 'S';
ds_lista_status_w varchar2(255)	:= null;

expressao1_w	varchar2(255) := substr(obter_desc_expressao_idioma(773926, null, wheb_usuario_pck.get_nr_seq_idioma),1,255);
expressao2_w	varchar2(255) := substr(obter_desc_expressao_idioma(773939, null, wheb_usuario_pck.get_nr_seq_idioma),1,255);
expressao3_w	varchar2(255) := substr(obter_desc_expressao_idioma(773940, null, wheb_usuario_pck.get_nr_seq_idioma),1,255);
 		
/* obter exame agenda */
cursor c01 is
select	cd_agenda,
	cd_medico,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	nr_minuto_duracao,
	cd_pessoa_fisica
from	agenda_lista_espera
where	nr_sequencia = nr_seq_lista_espera_p
and	nr_seq_proc_interno is not null;

/* obter exames adicionais */
cursor c02 is
select	cd_medico,
	nr_seq_proc_interno,
	qt_min_proc
from	AGENDA_LISTA_ESPERA_PROC
where	NR_SEQ_LISTA = nr_seq_lista_espera_p;

begin

obter_param_usuario(871, 162, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_agenda_w);
obter_param_usuario(871, 163, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_proc_w); 	
obter_param_usuario(871, 642, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atual_maior_tempo_proc_w);
obter_param_usuario(871, 774, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_estab_agenda_w);
if	(wheb_usuario_pck.get_cd_funcao = 871) then /*s� ir� buscar o parametro se estiver na fun��o Gest�o da Agenda cirurgica*/
	Obter_Param_Usuario(871, 820, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ds_lista_status_w);	
end if;
	
if	(cd_estabelecimento_p is not null) and
	(nr_seq_lista_espera_p is not null) then

	select 	max(a.cd_estabelecimento)
	into 	cd_estab_agenda_w
	from	agenda a,
			agenda_lista_espera b
	where	a.cd_agenda 	= b.cd_agenda
	and		b.nr_sequencia 	= nr_seq_lista_espera_p;	

	/* obter parametros */
	select	obter_parametro_agenda(cd_estabelecimento_p, 'IE_CONSISTE_DURACAO', 'N')
	into	ie_param_sobreposicao_w
	from	dual;	
	
	
	/* obter duracao exame agenda */
	open c01;
	loop
	fetch c01 into	cd_agenda_w,
			cd_medico_exec_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_minuto_duracao_w,
			cd_pessoa_fisica_w;
	exit when c01%notfound;
		begin
    
		if	(ie_atualiza_tempo_agenda_w in ('S','Q')) then		

			select	obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,decode(ie_estab_agenda_w,'S',cd_estab_agenda_w,0))
			into	qt_min_proc_w
			from	dual;
			
			qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
    
		end if;
			
		end;
	end loop;
	close c01;
	

	/* obter duracao exames adicionais */
	if	(ie_atualiza_tempo_proc_w = 'S') then
		
		open c02;
		loop
		fetch c02 into	cd_medico_proc_adic_w,
				nr_seq_proc_interno_w,
				qt_min_proc_w;
		exit when c02%notfound;
			begin
      
			if	(nvl(qt_min_proc_w,0) = 0) then
				if	(ie_estab_agenda_w = 'S') then
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,cd_estab_agenda_w);
				else
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,0);
				end if;
			end if;
      
			if (ie_atual_maior_tempo_proc_w = 'S') then
				if (qt_min_proc_w > qt_min_agenda_w) then
					 qt_min_agenda_w := qt_min_proc_w;
				end if;
			else
				qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
			end if;		
			end;
      
		end loop;
		close c02;
    
    
		update	agenda_lista_espera
			set	nr_minuto_duracao	= qt_min_agenda_w
			where	nr_sequencia		= nr_seq_lista_espera_p;		
			commit;
	
	end if;
	
end if;

end calcular_min_dur_lista_espera;
/