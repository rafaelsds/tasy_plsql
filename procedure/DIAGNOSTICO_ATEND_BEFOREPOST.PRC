create or replace
procedure diagnostico_atend_beforepost(	dt_diagnostico_p	date,
			nr_atendimento_p	number,
			cd_doenca_p		varchar2,
			ie_lado_p		varchar2,			
			nm_usuario_p		varchar2) is 
			
qt_diagnosticos_w	number(10) := 0;	
ie_lado_w		varchar2(1);					

begin

select 	count(*)  
into	qt_diagnosticos_w
from 	diagnostico_doenca  
where 	dt_diagnostico  = dt_diagnostico_p
and   	nr_atendimento  = nr_atendimento_p
and   	cd_doenca       = cd_doenca_p;

if (qt_diagnosticos_w > 0) then
	--O CID #@CD_DOENCA#@ j� foi cadastrado para este atendimento !
	wheb_mensagem_pck.exibir_mensagem_abort(163130, cd_doenca_p);
end if;

select 	ie_exige_lado 
into	ie_lado_w
from 	cid_doenca 
where 	cd_doenca_cid = cd_doenca_p;

if 	(ie_lado_p is null  and
	ie_lado_w = 'S') then
	--Este CID exige a informa��o do Lado.
	wheb_mensagem_pck.exibir_mensagem_abort(163137);
end if;

end diagnostico_atend_beforepost;
/