CREATE OR REPLACE
PROCEDURE Excluir_Prescr_vazia_CPOE(qt_hora_adicional_p		Number) IS

/* o motivo de baixa deve ser o mesmo utilizado na procedure de baixa das prescrições */
nr_prescricao_w		Number(14,0);
ds_erro_w			varchar2(2000);
i					number(10);

Cursor c020 is
select	a.nr_prescricao
from	prescr_medica a
where	a.dt_prescricao < (sysdate - qt_hora_adicional_p / 24)
and		a.dt_prescricao > sysdate - 5
and		obter_itens_prescr(a.nr_prescricao, a.ds_itens_prescr) IS NULL
and not exists(
		select	1
		from	cirurgia b
		where	b.nr_prescricao = a.nr_prescricao);
		
BEGIN
i := 0;
open C020;
loop
fetch C020 into	
	nr_prescricao_w;
exit when C020%notfound;
	begin
	
	begin
	update	pe_prescricao
	set		nr_prescricao = null
	where	nr_prescricao = nr_prescricao_w;
	commit;
	exception
		when others then
			ds_erro_w := substr(sqlerrm,1,200)||obter_desc_expressao(325814)||nr_prescricao_w; --' Prescricao: '
			dbms_output.put_line('SAE: '|| ds_erro_w);
	end;
	
	begin
	update	prescr_medica
	set		dt_liberacao = null,
			dt_liberacao_medico = null,
			dt_liberacao_farmacia = null
	where	nr_prescricao = nr_prescricao_w;
	commit;
	exception
		when others then
			ds_erro_w := substr(sqlerrm,1,200)||obter_desc_expressao(325814)||nr_prescricao_w; --' Prescricao: '
			dbms_output.put_line(obter_desc_expressao(724442)/*'Update: '*/|| ds_erro_w);
	end;
	begin
	delete	from prescr_medica 
	where 	nr_prescricao	= nr_prescricao_w;
	exception
		when others then
			ds_erro_w := substr(sqlerrm,1,200)||obter_desc_expressao(325814)||nr_prescricao_w; --' Prescricao: '
			dbms_output.put_line('Delete: '|| ds_erro_w);
	end;
	
	i  := i + 1;
	if	(i > 500) then
		i := 0;
		commit;
	end if;
	
	end;
end loop;
close C020;

commit;

END Excluir_Prescr_vazia_CPOE; 
/