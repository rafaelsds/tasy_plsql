create or replace
procedure ajusta_lote_baixa_js(	nr_seq_lote_fornec_p	number,
				nr_devolucao_p		number,
				nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

begin

update	item_devolucao_material_pac
set	nr_seq_lote_fornec	= nr_seq_lote_fornec_p
where	nr_devolucao	= nr_devolucao_p
and	nr_sequencia	= nr_sequencia_p;

commit;

end ajusta_lote_baixa_js;
/