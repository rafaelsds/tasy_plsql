create or replace
procedure qua_gerar_valid_doc(	nr_seq_doc_p		number,
				cd_pessoa_validacao_p	varchar2,
				nm_usuario_p		Varchar2) is 

qt_existe_w	number(5,0);

begin

if	(nvl(nr_seq_doc_p,0) > 0) and
	(cd_pessoa_validacao_p is not null) then
	select	count(*)
	into	qt_existe_w
	from	qua_doc_validacao
	where	nr_seq_doc		= nr_seq_doc_p
	and	cd_pessoa_validacao	= cd_pessoa_validacao_p;
	
	if	(qt_existe_w = 0) then
		insert into qua_doc_validacao(
			nr_sequencia,
			nr_seq_doc,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_validacao,
			dt_validacao)
		values(	qua_doc_validacao_seq.nextval,
			nr_seq_doc_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_validacao_p,
			null);
		commit;
	end if;
end if;

end qua_gerar_valid_doc;
/