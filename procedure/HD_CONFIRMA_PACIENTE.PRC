CREATE OR REPLACE procedure HD_CONFIRMA_PACIENTE(cd_pessoa_fisica_p		varchar2,
				                                         nm_usuario_p			varchar2,
                                                 ie_status_p			varchar2) is

/* ie_status
	CO - Confirmado
  N - N�o Confirmado
*/
ie_status_w  varchar2(10);

BEGIN

  select max(ie_status)
    into ie_status_w
    from hd_status_recepcao
   where cd_pessoa_fisica = cd_pessoa_fisica_p
     and ie_situacao = 'A'
	 and	trunc(dt_status) = trunc(sysdate);

  if (ie_status_w is null) then
     insert into HD_STATUS_RECEPCAO (
                  nr_sequencia,
                  dt_atualizacao,
                  nm_usuario,
                  dt_atualizacao_nrec,
                  nm_usuario_nrec,
                  cd_pessoa_fisica,
                  ie_status,
                  dt_status,
                  ie_situacao
                  ) values (
                  hd_status_recepcao_seq.nextval,
                  sysdate,
                  nm_usuario_p,
                  sysdate,
                  nm_usuario_p,
                  cd_pessoa_fisica_p,
                  ie_status_p,
                  SYSDATE,
                  'A'
                );
  end if;

commit;

end HD_CONFIRMA_PACIENTE;
/
