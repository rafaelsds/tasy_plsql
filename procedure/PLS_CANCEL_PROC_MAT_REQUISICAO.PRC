create or replace
procedure pls_cancel_proc_mat_requisicao
			(	nr_seq_proc_mat_p	Number,
				ie_tipo_cancel_p	Varchar2,
				nr_seq_motivo_cancel_p	Number,
				ds_observacao_p		Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

cd_procedimento_w		procedimento.cd_procedimento%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;
nr_seq_material_w		pls_material.nr_sequencia%type;
nr_seq_auditoria_w		pls_auditoria.nr_sequencia%type;
ds_motivo_cancel_w		Varchar2(255);
qt_reg_proc_w			Number(10)	:= 0;
qt_reg_mat_w			Number(10)	:= 0;
qt_reg_mat_aud_w		Number(10)	:= 0;
qt_reg_proc_aud_w		Number(10)	:= 0;
qt_reg_aud_w			Number(10)	:= 0;
ds_estagio_w			Varchar2(255);

begin

begin
select	substr(ds_motivo_cancelamento,1,255)
into	ds_motivo_cancel_w
from	pls_guia_motivo_cancel
where	ie_situacao 		= 'A'
and	cd_estabelecimento 	= cd_estabelecimento_p
and	nr_sequencia		= nr_seq_motivo_cancel_p;
exception
when others then
	ds_motivo_cancel_w	:= '';
end;

if	(ie_tipo_cancel_p	= 'P') then
	select	cd_procedimento,
		nr_seq_requisicao
	into	cd_procedimento_w,
		nr_seq_requisicao_w
	from	pls_requisicao_proc
	where	nr_sequencia	= nr_seq_proc_mat_p;
	
	begin
		select	substr(obter_valor_dominio(3431, ie_estagio),1,255)
		into	ds_estagio_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
	exception
	when others then
		ds_estagio_w	:= '';
	end;
	
	update	pls_requisicao_proc
	set	ie_status	= 'C',
		ie_estagio	= 'N',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_proc_mat_p;
	
	pls_requisicao_gravar_hist(	nr_seq_requisicao_w, 'L', 
					substr('Procedimento '||cd_procedimento_w||' cancelado em '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||' pelo usu�rio '||
					nm_usuario_p||'.'||chr(13)||'Motivo: '||ds_motivo_cancel_w||'.'||chr(13)||'Est�gio anterior da requisi��o: '||ds_estagio_w
					||chr(13)||'Observa��o: '||ds_observacao_p,1,4000),
					'',nm_usuario_p);

	select	count(1)
	into	qt_reg_proc_aud_w
	from	pls_auditoria		b,
		pls_auditoria_item	a
	where	a.nr_seq_proc_origem	= nr_seq_proc_mat_p
	and	a.nr_seq_auditoria	= b.nr_sequencia
	and	b.dt_liberacao		is null;
	
	if	(qt_reg_proc_aud_w	> 0) then
		update	pls_auditoria_item
		set	ie_status		= 'C',
			ie_status_solicitacao	= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_proc_origem	= nr_seq_proc_mat_p;
	end if;
elsif	(ie_tipo_cancel_p		= 'M') then
	select	nr_seq_material,
		nr_seq_requisicao
	into	nr_seq_material_w,
		nr_seq_requisicao_w
	from	pls_requisicao_mat
	where	nr_sequencia	= nr_seq_proc_mat_p;
	
	begin
		select	substr(obter_valor_dominio(3431, ie_estagio),1,255)
		into	ds_estagio_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
	exception
	when others then
		ds_estagio_w	:= '';
	end;
	
	update	pls_requisicao_mat
	set	ie_status	= 'C',
		ie_estagio	= 'N',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_proc_mat_p;
	
	pls_requisicao_gravar_hist(	nr_seq_requisicao_w, 'L', 
					substr('Material '||nr_seq_material_w||' cancelado em '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||' pelo usu�rio '||
					nm_usuario_p||'.'||chr(13)||'Motivo: '||ds_motivo_cancel_w||'.'||chr(13)||'Est�gio anterior da requisi��o: '||ds_estagio_w
					||chr(13)||'Observa��o: '||ds_observacao_p,1,4000),
					'',nm_usuario_p);

	select	count(1)
	into	qt_reg_mat_aud_w
	from	pls_auditoria		b,
		pls_auditoria_item	a
	where	a.nr_seq_mat_origem	= nr_seq_proc_mat_p
	and	a.nr_seq_auditoria	= b.nr_sequencia
	and	b.dt_liberacao		is null;
	
	if	(qt_reg_mat_aud_w	> 0) then
		update	pls_auditoria_item
		set	ie_status		= 'C',
			ie_status_solicitacao	= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_mat_origem	= nr_seq_proc_mat_p;
	end if;
end if;

begin
	select	nr_sequencia
	into	nr_seq_auditoria_w
	from	pls_auditoria
	where	nr_seq_requisicao	= nr_seq_requisicao_w
	and	ie_status		not in('F', 'C');
exception
when others then
	nr_seq_auditoria_w	:= null;
end;

if	(nr_seq_auditoria_w	is not null) then	
	select 	sum(qt_itens)
	into	qt_reg_aud_w
	from	(	select	count(1) qt_itens	
			from	pls_requisicao_proc
			where	nr_seq_requisicao 	= nr_seq_requisicao_w
			and	ie_status		= 'A'
			union 	all
			select	count(1) qt_itens
			from	pls_requisicao_mat
			where	nr_seq_requisicao	= nr_seq_requisicao_w
			and	ie_status		= 'A');

	if	(qt_reg_aud_w	= 0) then
		update	pls_auditoria_item
		set	ie_status		= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_auditoria	= nr_seq_auditoria_w;
				
		update	pls_auditoria
		set	ie_status		= 'C',
			dt_liberacao		= sysdate,
			nr_seq_proc_interno	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_auditoria_w;
		
		pls_atualiza_estagio_req(nr_seq_requisicao_w, nm_usuario_p);
	end if;
end if;

select	count(1)
into	qt_reg_proc_w
from	pls_requisicao_proc
where	nr_seq_requisicao	= nr_seq_requisicao_w
and	ie_status		<> 'C';

select	count(1)
into	qt_reg_mat_w
from	pls_requisicao_mat
where	nr_seq_requisicao	= nr_seq_requisicao_w
and	ie_status		<> 'C';

if	((qt_reg_proc_w + qt_reg_mat_w)	= 0) then
	update	pls_requisicao
	set	ie_status		= 'C',
		ie_estagio		= 3,
		nr_seq_motivo_cancel    = nr_seq_motivo_cancel_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_fim_processo_req	= sysdate
	where	nr_sequencia		= nr_seq_requisicao_w;
end if;

commit;

end pls_cancel_proc_mat_requisicao;
/
