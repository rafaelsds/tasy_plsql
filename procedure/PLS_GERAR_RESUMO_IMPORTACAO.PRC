create or replace 
procedure pls_gerar_resumo_importacao(nr_seq_protocolo_p 	pls_protocolo_conta.nr_sequencia%type, 
				      nm_usuario_p 		usuario.nm_usuario%type) is

qt_contas_w 		pls_resumo_importacao.qt_contas%type;
qt_contas_glosa_w 	pls_resumo_importacao.qt_contas_criticadas%type;
qt_itens_w 		pls_resumo_importacao.qt_itens_protocolo%type;
qt_itens_glosa_w 	pls_resumo_importacao.qt_itens_criticados%type;
vl_total_w 		pls_resumo_importacao.qt_valor_total%type;
vl_tot_crit_w 		pls_resumo_importacao.qt_valor_tot_crit%type;
qt_mensagem_aviso_w 	pls_resumo_importacao.qt_mensagens_aviso%type;
qt_mensagem_bloqueio_w	pls_resumo_importacao.qt_mensagens_bloqueio%type;
nr_seq_usu_prestador_w  pls_protocolo_conta.nr_seq_usu_prestador%type;

begin
-- quantidade de contas no protocolo
select	count(1)
into	qt_contas_w
from	pls_conta
where	nr_seq_protocolo = nr_seq_protocolo_p;

-- quantidade de contas com glosa e valor total glosado
select	count(1),
	nvl(sum(a.vl_total_imp),0)
into	qt_contas_glosa_w,
	vl_tot_crit_w
from	pls_conta a,
	pls_conta_glosa b
where	a.nr_seq_protocolo = nr_seq_protocolo_p
and	b.nr_seq_conta = a.nr_sequencia;

-- quantidade de itens
select	sum(qt_item)
into	qt_itens_w
from	(select	count(1) qt_item
	from	pls_conta_proc a,
		pls_conta b
	where	b.nr_seq_protocolo = nr_seq_protocolo_p
	and	a.nr_seq_conta = b.nr_sequencia
	union all
	select	count(1) qt_item
	from	pls_conta_mat a,
		pls_conta b
	where	b.nr_seq_protocolo = nr_seq_protocolo_p
	and	a.nr_seq_conta = b.nr_sequencia);

-- quantidade de itens glosados
select 	sum(qt)
into	qt_itens_glosa_w
from	(select	count(1) qt 
	from	pls_conta		b,
		pls_conta_glosa 	a
	where	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and	a.nr_seq_conta		= b.nr_sequencia
	and	a.nr_seq_conta_proc	is null
	and	a.nr_seq_conta_mat	is null
	union all
	select	count(1) qt
	from	pls_conta 		c,
		pls_conta_mat 		d,
		pls_conta_glosa 	e
	where	c.nr_seq_protocolo	= nr_seq_protocolo_p
	and	d.nr_seq_conta		= c.nr_sequencia
	and	e.nr_seq_conta_mat	= d.nr_sequencia
	union all
	select	count(1) qt
	from	pls_conta 		f,
		pls_conta_proc 		g,
		pls_conta_glosa 	h
	where	f.nr_seq_protocolo	= nr_seq_protocolo_p
	and	g.nr_seq_conta		= f.nr_sequencia
	and	h.nr_seq_conta_proc	= g.nr_sequencia);

-- valor total do protocolo
select	nvl(pls_obter_valor_protocolo(nr_sequencia,'TP'), 0)
into	vl_total_w
from	pls_protocolo_conta
where	nr_sequencia = nr_seq_protocolo_p;

-- quantidade total de avisos
select	count(1)
into	qt_mensagem_aviso_w
from	pls_mensagem_importacao
where	nr_seq_protocolo = nr_seq_protocolo_p
and	nr_seq_mensagem in (5013, 5011);

-- quantidade total de mensagens de bloqueio
select	count(1)
into	qt_mensagem_bloqueio_w
from	pls_mensagem_importacao
where	nr_seq_protocolo = nr_seq_protocolo_p
and	nr_seq_mensagem in (5014, 5001, 5002, 5012, 5010, 5007);

insert into pls_resumo_importacao (
	nr_sequencia,
	nm_usuario,
	nr_seq_protocolo,
	qt_contas,
	qt_contas_criticadas,
	qt_itens_protocolo,
	qt_itens_criticados,
	qt_mensagens_aviso,
	qt_mensagens_bloqueio,
	dt_atualizacao,
	qt_valor_total,
	qt_valor_tot_crit
) values (
	pls_resumo_importacao_seq.nextval,
	nm_usuario_p,
	nr_seq_protocolo_p,
	qt_contas_w,
	qt_contas_glosa_w,
	qt_itens_w,
	qt_itens_glosa_w,
	qt_mensagem_aviso_w,
	qt_mensagem_bloqueio_w,
	sysdate,
	vl_total_w,
	vl_tot_crit_w
);

-- log de acesso ao portal
if	(nm_usuario_p <> 'WebService') then

	select	max(nr_seq_usu_prestador)
	into	nr_seq_usu_prestador_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_p;
	
	if	(nr_seq_usu_prestador_w is not null) then
	
		pls_gravar_log_acesso_portal( null, null, nr_seq_usu_prestador_w,
					      null, null, null,
					      null, null, '18',
					      'O usu�rio '||nm_usuario_p||' importou um arquivo TISS.', 'Importou arquivo TISS.', 'N'); 
	end if;
end if;

commit;

end pls_gerar_resumo_importacao;
/