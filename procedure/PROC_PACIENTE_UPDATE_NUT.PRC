create or replace
procedure proc_paciente_update_nut(nr_sequencia_p in number,
				   dt_procedimento_p date) is

begin

if(nvl(nr_sequencia_p,0) > 0)then

update	procedimento_paciente
set	ds_observacao	= obter_texto_tasy(381702, wheb_usuario_pck.get_nr_seq_idioma),
	dt_procedimento = dt_procedimento_p
where	nr_sequencia	= nr_sequencia_p;

commit;

end if;

end proc_paciente_update_nut;
/
