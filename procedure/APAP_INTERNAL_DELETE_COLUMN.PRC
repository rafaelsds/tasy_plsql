create or replace PROCEDURE apap_internal_delete_column ( nr_seq_string in varchar2
        )is
begin

if(nr_seq_string is not null) THEN    

    execute immediate 'DELETE w_apap_pac_registro
    WHERE
        nr_sequencia in (' || nr_seq_string || ')
        AND nr_seq_origem IS NULL';
    
    DELETE FROM w_apap_pac_registro_hist A
    WHERE
        EXISTS(SELECT
                1
            FROM w_apap_pac_registro B
            INNER JOIN w_apap_pac_informacao C ON
                B.nr_seq_apap_inf = C.nr_sequencia
            INNER JOIN w_apap_pac_grupo D ON
                C.nr_seq_apap_grupo = D.nr_sequencia
            INNER JOIN w_apap_pac E ON
                D.nr_seq_mod_apap = E.nr_sequencia
            WHERE
                (B.nr_seq_origem IS NULL OR B.ie_status_houdini = 'L') AND
                A.dt_apap = B.dt_registro AND
                A.nm_atributo = C.nm_atributo_linked AND
                A.nr_atendimento = E.nr_atendimento AND
                A.nr_seq_documento = E.nr_seq_documento AND
                B.nr_sequencia in
                  (SELECT
                  regexp_substr(nr_seq_string, '[^,]+', 1, LEVEL) token
                  FROM dual
                  CONNECT BY LEVEL <= length(nr_seq_string) - length(REPLACE(nr_seq_string, ',')) + 1)
        );
  commit;
END IF;

END apap_internal_delete_column;
/