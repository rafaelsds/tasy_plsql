create or replace
procedure zerar_controle_lab_prescricao is

ie_regra_w		varchar2(1);
dt_sequencia_w		date;


BEGIN

select	nvl(max(dt_sequencia),sysdate)
into	dt_sequencia_w
from	lab_seq_geracao;

select	max(ie_regra_seq_prescr)
into	ie_regra_w
from	lab_parametro;

if	((ie_regra_w = 'D') and
	 (trunc(dt_sequencia_w,'dd') < trunc(sysdate,'dd'))) or
	((ie_regra_w = 'S') and
	 (trunc(dt_sequencia_w,'day') < trunc(sysdate,'day'))) or
	((ie_regra_w = 'M') and
	 (trunc(dt_sequencia_w,'month') < trunc(sysdate,'month'))) or
	((ie_regra_w = 'A') and
	 (trunc(dt_sequencia_w,'year') < trunc(sysdate,'year'))) then

	insert into lab_seq_geracao (nr_sequencia, dt_sequencia,
			 nr_valor_seq, dt_atualizacao, nm_usuario)
	values (lab_seq_geracao_seq.NextVal, trunc(sysdate,'dd'),
			0, sysdate, 'Tasy');
end if;

commit;

END zerar_controle_lab_prescricao;
/

