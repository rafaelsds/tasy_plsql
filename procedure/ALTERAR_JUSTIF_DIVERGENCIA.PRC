create or replace
procedure alterar_justif_divergencia(
			ds_justif_divergencia_p		varchar2,
			nr_cot_compra_p		varchar2) is


begin

update	cot_compra
set	ds_justif_divergencia = ds_justif_divergencia_p
where	nr_cot_compra = nr_cot_compra_p;

commit;

END alterar_justif_divergencia;
/