create or replace
procedure Ageint_Obter_Se_Cria_Grupos(
								dt_parametro_p		date,
								ds_retorno_p out	varchar2,
								nm_usuario_p		Varchar2) is 

qt_registro_w	number(10);								
								
begin

begin
select	1
into	qt_registro_w
from	agenda_int_area
where	dt_atualizacao	> dt_parametro_p
and		rownum	= 1;
exception
when no_data_found then
	begin
	select	1
	into	qt_registro_w
	from	agenda_int_grupo
	where	dt_atualizacao	> dt_parametro_p
	and		rownum	= 1;
	exception
	when no_data_found then
		begin
		select	1
		into	qt_registro_w
		from	agenda_int_grupo_item
		where	dt_atualizacao	> dt_parametro_p
		and		rownum	= 1;
		exception
		when no_data_found then
			begin
			select	1
			into	qt_registro_w
			from	ageint_ex_adic_grupo_item
			where	dt_atualizacao	> dt_parametro_p
			and		rownum	= 1;			
			exception
			when no_data_found then
				qt_registro_w	:= 0;
			end;
		end;
	end;
end;

if	(qt_registro_w	> 0) then
	ds_retorno_p	:= 'S';
else
	ds_retorno_p	:= 'N';
end if;

commit;

end Ageint_Obter_Se_Cria_Grupos;
/