create or replace
procedure pls_lanc_bonificacao_simulacao
			(	nr_seq_simul_item_p	number,
				nr_seq_simulacao_p	number,
				nm_usuario_p		Varchar2) is 
			
nr_seq_plano_w			number(10);
nr_seq_lanc_automotico_w	number(10);
nr_seq_regra_item_w		number(10);
nr_qtde_bonif_w			number(10);
nr_seq_bonificacao_vinculo_w	pls_bonificacao_vinculo.nr_sequencia%type;
ie_tipo_contratacao_w		pls_plano.ie_tipo_contratacao%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_regra_lanc_automatico
	where	ie_acao_regra 	= '6'
	and	((nr_seq_grupo_produto is not null and substr(pls_se_grupo_preco_produto(nr_seq_grupo_produto,nr_seq_plano_w),1,255) = 'S') or
		 (nr_seq_grupo_produto is null))
	and	((ie_tipo_contratacao = ie_tipo_contratacao_w) or (ie_tipo_contratacao is null))
	and	((nr_seq_plano	is not null and	nr_seq_plano = nr_seq_plano_w) or
		 (nr_seq_plano	is null))
	and	ie_situacao	= 'A'
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);
	
Cursor C02 is
	select	nr_sequencia
	from	pls_regra_lanc_aut_item
	where	nr_seq_regra	= nr_seq_lanc_automotico_w
	and	nr_seq_bonificacao is not null	
	and	ie_situacao	= 'A';
	
Cursor C03 is
	select	nr_sequencia	
	from	pls_bonificacao_vinculo
	where	nr_seq_segurado_simul	= nr_seq_simul_item_p;	
	
begin
	open C03;
	loop
	fetch C03 into	
		nr_seq_bonificacao_vinculo_w;
	exit when C03%notfound;
		begin
		delete	pls_simulacao_resumo 
		where 	nr_seq_vinc_bonificacao	= nr_seq_bonificacao_vinculo_w; 
	
		delete	pls_bonificacao_vinculo
		where	nr_sequencia	= nr_seq_bonificacao_vinculo_w;		
		end;
	end loop;
	close C03;
	
	select	nr_seq_produto
	into	nr_seq_plano_w
	from	pls_simulpreco_individual
	where	nr_sequencia	= nr_seq_simul_item_p;
	
	if	(nr_seq_plano_w is not null) then
		select	max(ie_tipo_contratacao)
		into	ie_tipo_contratacao_w
		from	pls_plano
		where	nr_sequencia = nr_seq_plano_w;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_lanc_automotico_w;
		exit when C01%notfound;
			begin
			open C02;
			loop
			fetch C02 into	
				nr_seq_regra_item_w;
			exit when C02%notfound;
				begin	
				select	count(*)
				into	nr_qtde_bonif_w
				from 	pls_bonificacao_vinculo a
				where 	a.nr_seq_segurado_simul = nr_seq_simul_item_p;
				
				if 	(nvl(nr_qtde_bonif_w,0) = 0 ) then				
					pls_gerar_bonificacao_sca(nr_seq_simul_item_p, nr_seq_plano_w,6,nr_seq_simulacao_p,nm_usuario_p);				
				end if;
				end;
			end loop;
			close C02;
			end;
		end loop;
		close C01;
	end if;	
commit;

end pls_lanc_bonificacao_simulacao;
/