create or replace
procedure deletar_caixa_filtro_fluxo(	nr_seq_caixa_p	number,
					nm_usuario_p		varchar2) is 

begin
if	(nr_seq_caixa_p is not null) then
	begin
	delete	from W_FILTRO_FLUXO
	where	nr_seq_caixa = nr_seq_caixa_p;
	end;
end if;	

commit;

end deletar_caixa_filtro_fluxo;
/
