create or replace
procedure HD_Transfere_Reproc (	nr_seq_reproc_p			number,
				nr_seq_unid_origem_p		number,
				nr_seq_unid_destino_p		number,
				dt_transferencia_p		date,
				nm_usuario_p			varchar2,
				ds_erro_p		out	varchar2) is

begin

if	(nr_seq_unid_origem_p = nr_seq_unid_destino_p) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(279796);
else
	begin
	
		insert into hd_reproc_transf (
			nr_sequencia,
			dt_transferencia,
			nr_seq_reproc,
			nr_seq_unid_destino,
			nr_seq_unid_origem,
			cd_pf_transf,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec
		) values (
			hd_reproc_transf_seq.NextVal,
			dt_transferencia_p,
			nr_seq_reproc_p,
			nr_seq_unid_destino_p,
			nr_seq_unid_origem_p,
			substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		);
		
		commit;
	end;
end if;		
	
end HD_Transfere_Reproc;
/