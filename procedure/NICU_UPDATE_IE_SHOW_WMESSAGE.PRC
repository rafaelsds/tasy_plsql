create or replace procedure nicu_update_ie_show_wmessage(nm_usuario_p       in nicu_parent.nm_usuario%type, 
                                                         ie_show_wmessage_p in nicu_parent.ie_show_wmessage%type) as 
begin
  begin
    update nicu_parent
       set ie_show_wmessage = ie_show_wmessage_p,
           dt_atualizacao   = sysdate
     where nm_usuario       = nm_usuario_p;
  end;
  commit;
end nicu_update_ie_show_wmessage;
/
