create or replace procedure INSERT_ENVIO_EMAIL_TERC_LOG (
	nr_seq_terceiro_p		number,
	ds_email_dest_p			varchar2,
	ds_observacao_p			varchar2,
	ds_relatorio_p			varchar2) is

begin

    -- Insere o log do e-mail enviado
	insert into ENVIO_EMAIL_TERCEIRO_LOG( nr_sequencia,
											dt_atualizacao,
											nm_usuario,
											nr_seq_terceiro,
											dt_envio,
											ds_email_dest,
											nm_usuario_envio,
											ds_observacao,
											ds_relatorio)
	values ( ENVIO_EMAIL_TERCEIRO_LOG_seq.nextval,
			sysdate,
			obter_usuario_ativo,
			nr_seq_terceiro_p,
			sysdate,
			substr(ds_email_dest_p, 1, 255),
			obter_usuario_ativo,
			substr(ds_observacao_p, 1, 4000),
			substr(ds_relatorio_p, 1, 255)
	);
    
    commit;
	
end INSERT_ENVIO_EMAIL_TERC_LOG;
/
