create or replace procedure pfcs_db_tl_bat_low_n_emp_stat(
					nr_seq_indicator_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p	varchar2) is

pfcs_panel_detail_seq_w		pfcs_panel_detail.nr_sequencia%type;
nr_seq_panel_w				pfcs_panel_detail.nr_seq_panel%type;

cursor c01 is
  select ds_unit,
		 sum(qt_low_battery_status) low_battery_status,
		 sum(qt_empty_battery_status) empty_battery_status
	from(
		select 	sa.ds_setor_atendimento ds_unit,
			count(ppf.nr_sequencia) qt_low_battery_status,
			0 qt_empty_battery_status
			from pfcs_service_request sr,
				pfcs_patient_flag ppf,
				pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_encounter_location el,
				pfcs_device pd,
				unidade_atendimento uni,
				setor_atendimento sa
			where ((sr.si_status = 'COMPLETED' and sr.cd_service = 'E0404') 
				 or(sr.si_status = 'ACTIVE' and sr.cd_service = 'E0403'))
			and ppf.nr_seq_patient = pat.nr_sequencia
			and ppf.si_status = 'ACTIVE'
			and sr.nr_seq_encounter = enc.nr_sequencia
			and el.nr_seq_encounter = enc.nr_sequencia
			and ppf.cd_flag = 'D0401'
			and pat.nr_sequencia = pd.nr_seq_patient
			and pd.si_status = 'ACTIVE'
			and pd.ds_device_type = 'Monitor'
			and enc.nr_seq_patient = pat.nr_sequencia
			and ppf.period_end is null			
			and uni.nr_seq_location = el.nr_seq_location
			and uni.ie_situacao = 'A'
			and uni.cd_setor_atendimento = sa.cd_setor_atendimento
			and sa.ie_situacao = 'A'
			and sa.cd_classif_setor in ('1','3','4','9','11','12')
			and sa.cd_estabelecimento_base = cd_estabelecimento_p			
			group by sa.ds_setor_atendimento, 0
		union
			select sa.ds_setor_atendimento ds_unit,
			0 qt_low_battery_status,
			count(ppf.nr_sequencia) qt_empty_battery_status
			from pfcs_service_request sr,
				pfcs_patient_flag ppf,
				pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_encounter_location el,
				pfcs_device pd,
				unidade_atendimento uni,
				setor_atendimento sa
			where ((sr.si_status = 'COMPLETED' and sr.cd_service = 'E0404') 
				or (sr.si_status = 'ACTIVE' and sr.cd_service = 'E0403'))
			and ppf.nr_seq_patient = pat.nr_sequencia
			and ppf.si_status = 'ACTIVE'
			and sr.nr_seq_encounter = enc.nr_sequencia
			and el.nr_seq_encounter = enc.nr_sequencia
			and ppf.cd_flag = 'D0402'
			and pat.nr_sequencia = pd.nr_seq_patient
			and pd.si_status = 'ACTIVE'
			and pd.ds_device_type = 'Monitor'
			and enc.nr_seq_patient = pat.nr_sequencia
			and ppf.period_end is null
			and uni.nr_seq_location = el.nr_seq_location
			and uni.ie_situacao = 'A'
			and uni.cd_setor_atendimento = sa.cd_setor_atendimento
			and sa.ie_situacao = 'A'
			and sa.cd_classif_setor in ('1','3','4','9','11','12')
			and sa.cd_estabelecimento_base = cd_estabelecimento_p			
			group by sa.ds_setor_atendimento, 0)
		group by ds_unit;

begin

for r_c01 in c01 loop

	select 	pfcs_panel_detail_seq.nextval into 	pfcs_panel_detail_seq_w from 	dual;

     pfcs_pck_v2.pfcs_generate_results (
        vl_indicator_p => r_c01.low_battery_status,
        vl_indicator_aux_p => r_c01.empty_battery_status,
        ds_reference_value_p => r_c01.ds_unit,
        nr_seq_indicator_p => nr_seq_indicator_p,
        nr_seq_operational_level_p => cd_estabelecimento_p,
        nm_usuario_p => nm_usuario_p,
        nr_seq_panel_p => nr_seq_panel_w );

end loop;

commit;

pfcs_pck_v2.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p);

end pfcs_db_tl_bat_low_n_emp_stat;
/
