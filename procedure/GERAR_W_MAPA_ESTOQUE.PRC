create or replace
procedure gerar_w_mapa_estoque(nr_seq_mapa_p		number,
				nm_usuario_p			varchar2,
				ie_qt_vl_p			varchar2,
				ie_agrupa_p			varchar2,
				cd_local_estoque_p		number,
				ie_disp_estoque_p		varchar2,
				dt_mesano_referencia_p	date) is

ie_tipo_w			varchar2(1);
cd_estabelecimento_w		number(4);
cd_codigo_w			number(6);
ds_descricao_w		varchar2(100);
cd_operacao_estoque_w	number(3);
qt_estoque_w			number(13,4);
mes_anterior_w		date;
qt_saldo_anterior_w		number(13,4);
qt_saldo_atual_w		number(13,4);
qt_saldo_atual_w2		number(13,4);
qt_saldo_disponivel_w	number(13,4);
qt_emprestimo_ent_w		number(13,4);
qt_emprestimo_sai_w		number(13,4);
nr_seq_coluna_w		number(2);
ie_entrada_saida_w		varchar2(1);
qt_entrada1_w			number(13,4) := 0;
qt_entrada2_w			number(13,4) := 0;
qt_entrada3_w			number(13,4) := 0;
qt_entrada4_w			number(13,4) := 0;
qt_entrada5_w			number(13,4) := 0;
qt_saida1_w			number(13,4) := 0;
qt_saida2_w			number(13,4) := 0;
qt_saida3_w			number(13,4) := 0;
qt_saida4_w			number(13,4) := 0;
qt_saida5_w			number(13,4);
qt_existe_w			number(10);

cursor c01 is
	/*Busca o saldo anterior*/
	select	0 ie_tipo,
		decode(ie_agrupa_p,	'G', e.cd_grupo_material,
					'S', e.cd_subgrupo_material,
					'C', e.cd_classe_material,
					'M', a.cd_material) cd_codigo,
		0,
		decode(ie_qt_vl_p, 'Q', sum(a.qt_estoque), sum(a.vl_estoque)) qt_estoque_w,
		0 qt_saldo_atual_w2
	from	estrutura_material_v e,
		saldo_estoque a
	where	a.cd_material = e.cd_material
	and	a.dt_mesano_referencia	= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p, -1, 0),'month',0)
	and	a.cd_estabelecimento	= cd_estabelecimento_w
	and	(a.cd_local_estoque	= cd_local_estoque_p or nvl(cd_local_estoque_p,0) = 0)
	group by
		decode(ie_agrupa_p,	'G', e.cd_grupo_material,
					'S', e.cd_subgrupo_material,
					'C', e.cd_classe_material,
					'M', a.cd_material)
	union all
	/*Busca o saldo atual*/
	select	1 ie_tipo,
		decode(ie_agrupa_p,	'G', e.cd_grupo_material,
					'S', e.cd_subgrupo_material,
					'C', e.cd_classe_material,
					'M', a.cd_material) cd_codigo,
		0,
		decode(ie_qt_vl_p, 'Q', sum(a.qt_estoque), sum(a.vl_estoque)),
		sum(a.qt_estoque) /*necessario para depois obter o valor do disponivel*/
	from	estrutura_material_v e,
		saldo_estoque a
	where	a.cd_material = e.cd_material
	and	a.dt_mesano_referencia	= PKG_DATE_UTILS.start_of(dt_mesano_referencia_p,'month',0)
	and	a.cd_estabelecimento	= cd_estabelecimento_w
	and	(a.cd_local_estoque	= cd_local_estoque_p or nvl(cd_local_estoque_p,0) = 0)
	group by
		decode(ie_agrupa_p,	'G', e.cd_grupo_material,
					'S', e.cd_subgrupo_material,
					'C', e.cd_classe_material,
					'M', a.cd_material)
		/*Busca os movimentos atuais*/
	union all
	select	9 ie_tipo,
		decode(ie_agrupa_p,	'G', e.cd_grupo_material,
					'S', e.cd_subgrupo_material,
					'C', e.cd_classe_material,
					'M', a.cd_material) cd_codigo,
		a.cd_operacao_estoque,
		decode(ie_qt_vl_p, 'Q', sum(a.qt_estoque), sum(a.vl_estoque)),
		0
	from	estrutura_material_v e,
		operacao_estoque o,
		movto_estoque_operacao_v a
	where	a.dt_mesano_referencia	= dt_mesano_referencia_p
	and	a.cd_estabelecimento	= cd_estabelecimento_w
	and	(a.cd_local_estoque	= cd_local_estoque_p or nvl(cd_local_estoque_p,0) = 0)
	and	a.cd_operacao_estoque	= o.cd_operacao_estoque
	and	a.cd_material		= e.cd_material
	group by
		decode(ie_agrupa_p,	'G', e.cd_grupo_material,
					'S', e.cd_subgrupo_material,
					'C', e.cd_classe_material,
					'M', a.cd_material),
		a.cd_operacao_estoque
	order by ie_tipo;

Cursor c02 is
	select	b.nr_seq_coluna,
		b.ie_entrada_saida
	from	mapa_est_item b,
		mapa_est_item_oper a
	where	a.nr_seq_item		= b.nr_sequencia
	and	a.cd_operacao_estoque	= cd_operacao_estoque_w
	and	b.nr_seq_mapa		= nr_seq_mapa_p;

BEGIN
delete from w_mapa_estoque;
commit;

mes_anterior_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p, -1, 0);

select	cd_estabelecimento
into	cd_estabelecimento_w
from	mapa_estoque
where	nr_sequencia = nr_seq_mapa_p;

open c01;
LOOP
FETCH C01 INTO
	ie_tipo_w,
	cd_codigo_w,
	cd_operacao_estoque_w,
	qt_estoque_w,
	qt_saldo_atual_w2;
EXIT WHEN C01%NOTFOUND;	
	begin
	qt_saldo_anterior_w	:= 0;
	qt_saldo_atual_w	:= 0;
	qt_saldo_disponivel_w	:= 0;
	qt_emprestimo_ent_w	:= 0;
	qt_emprestimo_sai_w	:= 0;

	if	(ie_tipo_w = 9) then
		begin

		select	/*+ INDEX(b empmate_i1) index (c emprest_pk) */
			nvl(sum(b.qt_material),0)
		into	qt_emprestimo_ent_w
		from	emprestimo c,
			emprestimo_material b
		where	b.nr_emprestimo	= c.nr_emprestimo
		and	b.cd_material	= cd_codigo_w
		and	b.qt_material	> 0
		and	c.ie_tipo	= 'E'
		and	ie_qt_vl_p	= 'Q';

		select	/*+ INDEX(b empmate_i1) index (c emprest_pk) */
			nvl(sum(b.qt_material),0)
		into	qt_emprestimo_sai_w
		from	emprestimo c,
			emprestimo_material b
		where	b.nr_emprestimo	= c.nr_emprestimo
		and	b.cd_material 	= cd_codigo_w
		and	b.qt_material	> 0
		and	c.ie_tipo 	= 'S'
		and	ie_qt_vl_p	= 'Q';

		/* Verifica a coluna a ser incluido a quantidade
		e insere a quantidade no campo que deve ser somado*/
		qt_entrada1_w	:= 0;
		qt_entrada2_w	:= 0;
		qt_entrada3_w	:= 0;
		qt_entrada4_w	:= 0;
		qt_entrada5_w	:= 0;
		qt_saida1_w	:= 0;
		qt_saida2_w	:= 0;
		qt_saida3_w	:= 0;
		qt_saida4_w	:= 0;
		qt_saida5_w	:= 0;
		open c02;
		LOOP
		FETCH C02 INTO
			nr_seq_coluna_w,
			ie_entrada_saida_w;
		EXIT WHEN C02%NOTFOUND;	
			begin
			if	(nr_seq_coluna_w = 1) then
				qt_entrada1_w	:= qt_entrada1_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 2) then
				qt_entrada2_w	:= qt_entrada2_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 3) then
				qt_entrada3_w	:= qt_entrada3_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 4) then
				qt_entrada4_w	:= qt_entrada4_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 5) then
				qt_entrada5_w	:= qt_entrada5_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 6) then
				qt_saida1_w	:= qt_saida1_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 7) then
				qt_saida2_w	:= qt_saida2_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 8) then
				qt_saida3_w	:= qt_saida3_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 9) then
				qt_saida4_w	:= qt_saida4_w + qt_estoque_w;
			elsif	(nr_seq_coluna_w = 10) then
				qt_saida5_w	:= qt_saida5_w + qt_estoque_w;
			end if;
			end;
		END LOOP;
		CLOSE C02;
		end;
	end if;	/* Fim do tipo = 9 */

	if	(ie_tipo_w = 0) then
		qt_saldo_anterior_w	:= qt_estoque_w;
	end if; /* Fim do tipo = 0 */

	if	(ie_tipo_w = 1) then
		qt_saldo_atual_w	:= qt_estoque_w;

		if	(ie_disp_estoque_p = 'S') then
			select	nvl(sum(Obter_Saldo_Disp_Estoque(cd_estabelecimento_w, cd_material, nvl(cd_local_estoque_p,0), PKG_DATE_UTILS.start_of(dt_mesano_referencia_p,'month',0))),0)
			into	qt_saldo_disponivel_w
			from	estrutura_material_v
			where	cd_codigo_w = decode(ie_agrupa_p,
							'G', cd_grupo_material,
							'S', cd_subgrupo_material,
							'C', cd_classe_material,
							'M', cd_material);

			/* Se for valor, divide o valor pela quantidade estoque e multiplica pelo disponivel*/
			if	(ie_qt_vl_p = 'V') then
				qt_saldo_disponivel_w	:= dividir(qt_saldo_atual_w, qt_saldo_atual_w2) * qt_saldo_disponivel_w;
			end if;
		end if;
	end if; /* Fim do tipo = 1 */

	select	count(*)
	into	qt_existe_w
	from	w_mapa_estoque
	where	cd_codigo = cd_codigo_w;

	if	(qt_existe_w = 0) then
		select	obter_desc_estrut_mat(
					decode(ie_agrupa_p, 'G', cd_codigo_w, null),
					decode(ie_agrupa_p, 'S', cd_codigo_w, null),
					decode(ie_agrupa_p, 'C', cd_codigo_w, null),
					decode(ie_agrupa_p, 'M', cd_codigo_w, null))
		into	ds_descricao_w
		from	dual;

		insert into w_mapa_estoque(
			nr_sequencia,
			ds_descricao,
			dt_atualizacao,
			nm_usuario,
			cd_codigo,
			qt_saldo_anterior,
			qt_entrada1,
			qt_entrada2,
			qt_entrada3,
			qt_entrada4,
			qt_entrada5,
			qt_saida1,
			qt_saida2,
			qt_saida3,
			qt_saida4,
			qt_saida5,
			qt_saldo_atual,
			qt_emprestimo_ent,
			qt_emprestimo_sai,
			qt_saldo_disponivel)
		values(w_mapa_estoque_seq.nextval,
			ds_descricao_w,
			sysdate,
			nm_usuario_p,
			cd_codigo_w,
			qt_saldo_anterior_w,
			qt_entrada1_w,
			qt_entrada2_w,
			qt_entrada3_w,
			qt_entrada4_w,
			qt_entrada5_w,
			qt_saida1_w,
			qt_saida2_w,
			qt_saida3_w,
			qt_saida4_w,
			qt_saida5_w,
			qt_saldo_atual_w,
			qt_emprestimo_ent_w,
			qt_emprestimo_sai_w,
			qt_saldo_disponivel_w);
	else
		update	w_mapa_estoque
		set	qt_saldo_disponivel = qt_saldo_disponivel + qt_saldo_disponivel_w,
			qt_saldo_anterior = qt_saldo_anterior + qt_saldo_anterior_w,
			qt_saldo_atual  = qt_saldo_atual + qt_saldo_atual_w,
			qt_emprestimo_ent = qt_emprestimo_ent + qt_emprestimo_ent_w,
			qt_emprestimo_sai = qt_emprestimo_sai + qt_emprestimo_sai_w,
			qt_entrada1	= qt_entrada1 + qt_entrada1_w,
			qt_entrada2	= qt_entrada2 + qt_entrada2_w,
			qt_entrada3	= qt_entrada3 + qt_entrada3_w,
			qt_entrada4	= qt_entrada4 + qt_entrada4_w,
			qt_entrada5	= qt_entrada5 + qt_entrada5_w,
			qt_saida1	= qt_saida1 + qt_saida1_w,
			qt_saida2	= qt_saida2 + qt_saida2_w,
			qt_saida3	= qt_saida3 + qt_saida3_w,
			qt_saida4	= qt_saida4 + qt_saida4_w,
			qt_saida5	= qt_saida5 + qt_saida5_w
		where	cd_codigo	= cd_codigo_w;
	end if;
	end;
END LOOP;
CLOSE C01;

commit;
	
END gerar_w_mapa_estoque;
/