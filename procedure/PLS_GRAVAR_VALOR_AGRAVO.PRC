create or replace
procedure pls_gravar_valor_agravo
			(	nr_seq_seg_agravo_p	Number,
				vl_agravo_p		Number,
				qt_parcelas_p		Number,
				tx_agravo_p		number,
				nm_usuario_p		Varchar2) is 

begin

begin
update	pls_segurado_agravo
set	vl_agravo	= vl_agravo_p,
	qt_parcelas	= qt_parcelas_p,
	tx_agravo	= tx_agravo_p
where	nr_sequencia	= nr_seq_seg_agravo_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(262325);
	/* Mensagem: Ocorreu um problema ao salvar o valor do agravo! */
end;

commit;

end pls_gravar_valor_agravo;
/