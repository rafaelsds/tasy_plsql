create or replace
procedure consiste_ordem_aliq_paf (	nr_seq_aliquota_p		number,
				nr_seq_ecf_p		number,
				nr_ordem_cadastro_p	number,
				ds_retorno_p		in out varchar2) is

qt_registros_w	number(10);

begin

select	max(nr_sequencia)
into	qt_registros_w
from	paf_aliquota_ecf
where	nr_seq_aliquota = nr_seq_aliquota_p
and	nr_seq_ecf = nr_seq_ecf_p
and	nr_ordem_cadastro = nr_ordem_cadastro_p;

if (qt_registros_w > 0) then
	ds_retorno_p := 'F';
else
	ds_retorno_p := 'V';
end if;

commit;

end consiste_ordem_aliq_paf;
/