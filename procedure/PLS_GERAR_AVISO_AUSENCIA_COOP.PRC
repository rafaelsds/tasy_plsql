create or replace
procedure pls_gerar_aviso_ausencia_coop(dt_referencia_p		date,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

nr_seq_cooperado_w	number(10);
nr_seq_ausencia_w	number(10);
dt_inicio_w		date;
dt_fim_w		date;
dt_referencia_w		date;
nr_seq_motivo_w		number(10);
nr_seq_regra_w		number(10);
qt_dias_inicio_w	number(10);
qt_dias_fim_w		number(10);
ds_aviso_w		varchar2(4000);
ie_avisar_inicio_w	varchar2(1);
ie_avisar_termino_w	varchar2(1);
cd_perfil_w		number(10);
nm_usuario_dest_w	varchar2(15);
ie_email_w		varchar2(1);
ie_comunic_interna_w	varchar2(1);
ds_email_origem_w	varchar2(255);
ds_email_destino_w	varchar2(255);
ds_titulo_w		varchar2(255);
qt_dias_w		number(10);
nm_cooperado_w		varchar2(255);
dt_inicio_aus_w		date;
dt_fim_aus_w		date;
ie_aviso_w		varchar2(1) := 'N';
ie_email_cooperado_w	varchar2(1) := 'N';
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);
ds_email_w		varchar2(255);
					
cursor c01 is
	select	b.nr_seq_cooperado,
		b.nr_sequencia,
		b.dt_inicio,
		b.dt_fim,
		b.nr_seq_motivo 
	from	pls_cooperado_ausencia	b,
		pls_cooperado		a
	where	a.nr_sequencia	= b.nr_seq_cooperado;

Cursor C02 is
	select	a.nr_sequencia,
		a.qt_dias_inicio,
		a.qt_dias_fim,
		a.ds_aviso,
		a.ie_avisar_inicio,
		a.ie_avisar_termino,
		a.ie_email_cooperado
	from	pls_regra_aviso_aus_coop a
	where	nvl(a.nr_seq_motivo,nr_seq_motivo_w) = nr_seq_motivo_w
	and	dt_referencia_w	between trunc(a.dt_inicio_vigencia,'dd') and fim_dia(nvl(a.dt_fim_vigencia,dt_referencia_w));

cursor c03 is
	select	a.cd_perfil,
		a.nm_usuario_dest,
		a.ie_email,
		a.ie_comunic_interna
	from	pls_regra_aviso_aus_dest a
	where	a.nr_seq_regra_aviso = nr_seq_regra_w;
	
begin
if	(dt_referencia_p is not null) then

	-- Cooperado - Aus�ncia 
	open C01;
	loop
	fetch C01 into	
		nr_seq_cooperado_w,
		nr_seq_ausencia_w,
		dt_inicio_aus_w,
		dt_fim_aus_w,
		nr_seq_motivo_w;
	exit when C01%notfound;
		begin
		dt_referencia_w	:= trunc(dt_referencia_p,'dd');
		dt_inicio_aus_w	:= trunc(dt_inicio_aus_w,'dd');
		dt_fim_aus_w	:= trunc(dt_fim_aus_w,'dd');
		ds_titulo_w	:= 'Aviso de aus�ncia de cooperado';
		nm_cooperado_w	:= pls_obter_dados_cooperado(nr_seq_cooperado_w,'N');
		
		-- Regras sobre aus�ncia
		open C02;
		loop
		fetch C02 into	
			nr_seq_regra_w,
			qt_dias_inicio_w,
			qt_dias_fim_w,
			ds_aviso_w,
			ie_avisar_inicio_w,
			ie_avisar_termino_w,
			ie_email_cooperado_w;
		exit when C02%notfound;
			begin			
			ie_aviso_w	:= 'N';
			ds_aviso_w	:=	'Aus�ncia do cooperado ' || nm_cooperado_w ||'.'|| chr(13) || chr(10) ||
						'Data in�cio: ' || dt_inicio_aus_w || chr(13) || chr(10) ||
						'Data final: ' || dt_fim_aus_w || chr(13) || chr(10) ||
						'Motivo: ' || pls_obter_desc_motivo_afast(nr_seq_motivo_w) || chr(13) ||  chr(10) || 
						chr(13) || chr(10) || ds_aviso_w;
			
			if	(ds_aviso_w is not null) then
				ds_aviso_w := ds_aviso_w || chr(13) || chr(10) || chr(13) || chr(10) ||'';
			end if;
			
			-- Avisar in�cio de aus�ncia
			if	(nvl(ie_avisar_inicio_w,'N') = 'S') and
				(qt_dias_inicio_w is not null) then
				select	max(dt_inicio)
				into	dt_inicio_w
				from	pls_cooperado_ausencia a
				where	a.nr_sequencia = nr_seq_ausencia_w
				and	dt_referencia_w between a.dt_inicio - qt_dias_inicio_w and a.dt_inicio -1;
				
				if	(dt_inicio_w is not null) then
					qt_dias_w 	:= trunc(dt_inicio_w - dt_referencia_p);
					ds_titulo_w	:= ds_titulo_w || ' - In�cio da aus�ncia.';
					if	(qt_dias_w > 1) then
						ds_aviso_w	:= 	ds_aviso_w || 'Faltam ' || qt_dias_w || 
									' dias para o in�cio da aus�ncia do cooperado ' || nm_cooperado_w ||'.';
					else
						ds_aviso_w	:= 	ds_aviso_w || 'Falta ' || qt_dias_w || 
									' dia para o in�cio da aus�ncia do cooperado ' || nm_cooperado_w ||'.';
					end if;
					ie_aviso_w	:= 'S';
				end if;
			end if;
			
			-- Avisar fim de aus�ncia
			if	(nvl(ie_avisar_termino_w,'N') = 'S') and
				(qt_dias_fim_w is not null) then
				select	max(dt_fim)
				into	dt_fim_w
				from	pls_cooperado_ausencia a
				where	a.nr_sequencia = nr_seq_ausencia_w
				and	dt_referencia_w between a.dt_fim - qt_dias_fim_w and a.dt_fim -1;
				
				if	(dt_fim_w is not null) then
					qt_dias_w 	:= trunc(dt_fim_w - dt_referencia_p);
					ds_titulo_w	:= ds_titulo_w || ' - Final da aus�ncia.';
					if	(qt_dias_w > 1) then
						ds_aviso_w	:= 	ds_aviso_w || 'Faltam ' || qt_dias_w || 
									' dias para o fim da aus�ncia do cooperado '|| nm_cooperado_w ||'.';
					else
						ds_aviso_w	:= 	ds_aviso_w || 'Falta ' || qt_dias_w || 
									' dia para o in�cio da aus�ncia do cooperado ' || nm_cooperado_w ||'.';
					end if;
					ie_aviso_w	:= 'S';
				end if;
			end if;
			
			-- Se n�o houver qt_dias_inicio informado
			if	(qt_dias_inicio_w is null) and
				(dt_inicio_aus_w = dt_referencia_w) then
				ds_titulo_w 	:= 'Aviso - In�cio da aus�ncia do cooperado ' || nm_cooperado_w ||'.'; 
				ie_aviso_w	:= 'S';
			end if;
			
			-- Se n�o houver qt_dias_fim informado
			if	(qt_dias_inicio_w is null) and
				(dt_fim_aus_w = dt_referencia_w) then
				ds_titulo_w 	:= 'Aviso - Fim da aus�ncia do cooperado ' || nm_cooperado_w ||'.';
				ie_aviso_w	:= 'S';
			end if;			
			
			-- Destinat�rios
			if	(ie_aviso_w = 'S') then
				select	ds_email
				into	ds_email_origem_w
				from	usuario
				where	nm_usuario = nm_usuario_p;
				
				-- Enviar email ao cooperado
				if	(nvl(ie_email_cooperado_w,'N') = 'S') then
					select	max(cd_pessoa_fisica),
						max(cd_cgc)
					into	cd_pessoa_fisica_w,
						cd_cgc_w
					from	pls_cooperado
					where	nr_sequencia = nr_seq_cooperado_w;
					
					ds_email_w := substr(obter_dados_pf_pj(cd_pessoa_fisica_w,cd_cgc_w,'M'),1,255);
					
					if	(trim(ds_email_w) is not null) then
						enviar_email(ds_titulo_w,ds_aviso_w,ds_email_origem_w,ds_email_w,nm_usuario_p,'M');
					end if;
				end if;
			
				-- Enviar email aos destinat�rios
				open C03;
				loop
				fetch C03 into	
					cd_perfil_w,
					nm_usuario_dest_w,
					ie_email_w,
					ie_comunic_interna_w;
				exit when C03%notfound;
					begin
					-- Gera email
					if	(ie_email_w = 'S') then
						select	ds_email
						into	ds_email_destino_w
						from	usuario
						where	nm_usuario = nm_usuario_dest_w;
						
						enviar_email(ds_titulo_w,ds_aviso_w,ds_email_origem_w,ds_email_destino_w,nm_usuario_p,'M');
					end if;	
					
					-- Gera comunica��o interna
					if	(ie_comunic_interna_w = 'S') then
						gerar_comunic_padrao(	sysdate,ds_titulo_w,ds_aviso_w,'Tasy','N',nm_usuario_dest_w,'N',null,
									cd_perfil_w,null,null,sysdate,null,null);
					end if;	
					end;
				end loop;
				close C03;
			end if;

			end;
		end loop;
		close C02;
			
		end;
	end loop;
	close C01;
	
	commit;
end if;

end pls_gerar_aviso_ausencia_coop;
/