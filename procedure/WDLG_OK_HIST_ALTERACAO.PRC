create or replace
procedure WDLG_Ok_Hist_Alteracao(cd_pessoa_fisica_p number,
   nm_usuario_p  Varchar2) is 

qt_w number(10);
ie_func_sem_local_w varchar2(1);

begin

ie_func_sem_local_w := obter_valor_param_usuario(6001, 130, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

select  count(*) qt 
into qt_w
from  pessoa_fisica_loc_trab 
where  IE_LOCAL_PRINCIPAL = 'S'
and  cd_pessoa_fisica = cd_pessoa_fisica_p;

if  (qt_w = 0) and 
 (ie_func_sem_local_w <> 'S') then
 Wheb_mensagem_pck.exibir_mensagem_abort(201026);
end if;

end WDLG_Ok_Hist_Alteracao;
/