create or replace
procedure gerar_pls_pessoas_ag_integrada (	cd_pessoa_fisica_p	varchar2,
						ie_tipo_pessoa_p	varchar2,
						nm_usuario_p		varchar2) is 

begin

if (cd_pessoa_fisica_p is not null) then
	insert into pls_pessoas_ag_integrada
		(cd_pessoa_fisica,
		ie_tipo_pessoa,
		dt_atualizacao,
		nm_usuario)
	values
		(cd_pessoa_fisica_p,
		ie_tipo_pessoa_p,
		sysdate,
		nm_usuario_p);
end if;

commit;

end gerar_pls_pessoas_ag_integrada ;
/