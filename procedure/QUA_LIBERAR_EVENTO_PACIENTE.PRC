create or replace
procedure qua_liberar_evento_paciente(
	nr_sequencia_p		number,
	nm_usuario_p		Varchar2) is 

begin

update	QUA_EVENTO_PACIENTE
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_liberacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end qua_liberar_evento_paciente;
/