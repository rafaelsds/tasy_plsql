create or replace
procedure hc_w_ipasgo_dados_prof_trat(
			nr_interno_conta_p		number,
			dt_mesano_referencia_p	date,
			nm_usuario_p		Varchar2,
			nr_seq_tipo_fatura_p	number,
			qt_linha_arq_p in out	number,
			qt_linha_atend_p in out	number) is

-- Utilizada pelo Hospital Coracao de Goias

nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_funcao_medico_w	procedimento_paciente.ie_funcao_medico%type;
ds_funcao_medico_w	varchar2(10);
nr_conselho_w		varchar2(20);
cd_medico_executor_w	procedimento_paciente.cd_medico_executor%type;
nr_seq_conselho_w		pessoa_fisica.nr_seq_conselho%type;
nr_crm_w			medico.nr_crm%type;
vl_procedimento_w		number(15,4);--Nao alterar as casas decimais
cd_convenio_parametro_w	conta_paciente.cd_convenio_parametro%type;
cd_cnpj_w		convenio.cd_cgc%type;
qt_procedimento_w		procedimento_paciente.qt_procedimento%type;
qt_ato_w			number(10,0) := 0;
qt_ato_aux_w		number(10,0) := 0;
qt_ato_ww		number(10,0) := 0;
dt_procedimento_w		procedimento_paciente.dt_procedimento%type;
dt_procedimento_ww	procedimento_paciente.dt_procedimento%type;
ie_w			number(01,0);
nr_matricula_prestador_w	varchar2(08) := '00000000';
cd_cgc_prestador_w	procedimento_paciente.cd_cgc_prestador%type;
cd_cnpj_estab_w		varchar2(14);
cd_tipo_fatura_w		fatur_tipo_fatura.cd_tipo_fatura%type;
cd_estabelecimento_w	conta_paciente.cd_estabelecimento%type;
cd_tipo_procedimento_w	number(3);
cd_tipo_procedimento_ww	number(3);
qt_reg_w			number(10) := 1;
ie_controle_w		varchar2(15) := 'N';
ie_ordem_w		number(3);
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;
ie_somente_vl_medico_w	varchar2(15) := 'N';
ie_gerar_anestesista_w	varchar2(15) := 'S';
nr_ato_ipasgo_w		procedimento_paciente.nr_ato_ipasgo%type;
qt_ato_informado_w	number(10) := 0;
qt_ato_parecer_w	number(10) := 0;
ie_responsavel_credito_w	procedimento_paciente.ie_responsavel_credito%type;
cd_tipo_prestador_w	w_ipasgo_dados_prof_trat.cd_tipo_prestador%type;



type lista is RECORD (
		dt_procedimento date,
		ato number(10));
type myArray is table of lista index by binary_integer;

ds_controle_ato_w myArray;

cursor c01 is
select	sum(decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) *
				(b.tx_procedimento/100)),0))) vl_procedimento,
	b.ie_funcao_medico,
	b.cd_medico_executor,
	sum(b.qt_procedimento) qt_procedimento,
	b.dt_procedimento,
	1 ie,
	b.cd_cgc_prestador,
	a.cd_tipo_procedimento,
	0 ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	b.ie_responsavel_credito

from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	b.cd_medico_executor is not null
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	a.cd_tipo_procedimento not in (1,2,3,4,5,12,13,14,15,16,20,25,26,30,31,34,39,74,75,80,91,92,94,96,97)
and	cd_tipo_fatura_w not in (3,4)
and	somente_numero(b.cd_procedimento_convenio) not in (70033)
--and	qt_ato_informado_w = 0
having	sum(decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) *
				(b.tx_procedimento/100)),0))) > 0
group by b.ie_funcao_medico,
	 b.cd_medico_executor,
	 b.cd_cgc_prestador,
	 b.dt_procedimento,
	 a.cd_tipo_procedimento,0,
	 nvl(b.nr_ato_ipasgo,0),
	b.ie_responsavel_credito

union all
select	sum(a.vl_participante) vl_procedimento,	
	a.ie_funcao,
	a.cd_pessoa_fisica,
	sum(b.qt_procedimento),
	b.dt_procedimento,
	2 ie,
	b.cd_cgc_prestador,
	c.cd_tipo_procedimento,
	0 ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	a.ie_responsavel_credito

from	procedimento c,
	procedimento_participante a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.nr_sequencia	= a.nr_sequencia
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	a.cd_pessoa_fisica is not null
and	c.cd_procedimento	= b.cd_procedimento
and	c.ie_origem_proced	= b.ie_origem_proced
and	c.cd_tipo_procedimento not in (1,2,3,4,5,8,12,13,14,15,16,20,25,26,30,31,33,34,39,74,75,80,91,92,93,94,96,97,103)
and	cd_tipo_fatura_w	<> 3
and	somente_numero(b.cd_procedimento_convenio) not in (70033)
and	obter_se_exp_funcao_ipasgo (a.ie_funcao,nr_seq_tipo_fatura_p) = 'N'
and	nvl(ie_gerar_anestesista_w,'S') = 'S'
--and	qt_ato_informado_w = 0
group by	a.ie_funcao,
	a.cd_pessoa_fisica,
	b.dt_procedimento,
	b.cd_cgc_prestador,
	c.cd_tipo_procedimento,0,
	nvl(b.nr_ato_ipasgo,0),
	a.ie_responsavel_credito

union all
/*93 e especifico pra gerar uma linha so - OS416225*/
select	sum(decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) *
				(b.tx_procedimento/100)),0))) vl_procedimento,
	b.ie_funcao_medico,
	b.cd_medico_executor,
	sum(b.qt_procedimento),
	max(b.dt_procedimento) dt_procedimento,
	3 ie,
	b.cd_cgc_prestador,
	a.cd_tipo_procedimento,
	0 ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	b.ie_responsavel_credito

from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	b.cd_medico_executor is not null
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	((a.cd_tipo_procedimento = 93) or (b.cd_procedimento = 91020010))
and	somente_numero(b.cd_procedimento_convenio) not in (70033)
and	cd_tipo_fatura_w	= 3
--and	qt_ato_informado_w = 0
having	sum(decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) *
				(b.tx_procedimento/100)),0))) > 0
group by b.ie_funcao_medico,
	 b.cd_medico_executor,
	 b.cd_cgc_prestador,
	 a.cd_tipo_procedimento,0,
	 nvl(b.nr_ato_ipasgo,0),
	b.ie_responsavel_credito

union all
select	sum(decode(ie_somente_vl_medico_w,'S',
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)))) vl_procedimento,
	b.ie_funcao_medico,
	b.cd_medico_executor,
	sum(b.qt_procedimento),
	min(b.dt_procedimento) dt_procedimento,
	4 ie,
	b.cd_cgc_prestador,
	a.cd_tipo_procedimento,
	a.cd_tipo_procedimento ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	b.ie_responsavel_credito

from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	b.cd_medico_executor is not null
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	a.cd_tipo_procedimento not in (1,2,3,4,5,8,12,13,14,15,16,20,25,26,30,31,33,34,39,74,75,80,91,92,94,96,97,103)
and	cd_tipo_fatura_w	= 4
and	somente_numero(b.cd_procedimento_convenio) not in (70033)
and	obter_se_gera_ato_ipasgo(b.cd_procedimento,b.ie_origem_proced,cd_estabelecimento_w,cd_tipo_fatura_w,ie_tipo_atendimento_w,b.cd_setor_atendimento) = 'N'
--and	qt_ato_informado_w = 0
having	sum(decode(ie_somente_vl_medico_w,'S',
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)))) > 0
group by b.ie_funcao_medico,
	 b.cd_medico_executor,
	 b.cd_cgc_prestador,
	 a.cd_tipo_procedimento,
	 nvl(b.nr_ato_ipasgo,0),
	b.ie_responsavel_credito

union all
select	sum(decode(ie_somente_vl_medico_w,'S',
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)))) vl_procedimento,
	b.ie_funcao_medico,
	b.cd_medico_executor,
	sum(b.qt_procedimento),
	min(b.dt_procedimento) dt_procedimento,
	5 ie,
	b.cd_cgc_prestador,
	a.cd_tipo_procedimento,
	a.cd_tipo_procedimento ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	b.ie_responsavel_credito

from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	b.cd_medico_executor is not null
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	obter_se_gera_ato_ipasgo(b.cd_procedimento,b.ie_origem_proced,cd_estabelecimento_w,cd_tipo_fatura_w,ie_tipo_atendimento_w,b.cd_setor_atendimento) = 'S'
--and	qt_ato_informado_w = 0
having	sum(decode(ie_somente_vl_medico_w,'S',
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)))) > 0
group by b.ie_funcao_medico,
	 b.cd_medico_executor,
	 b.cd_cgc_prestador,
	 a.cd_tipo_procedimento,
	 nvl(b.nr_ato_ipasgo,0),
	b.ie_responsavel_credito

union all	 
select	sum(decode(Obter_Conversao_Externa(null,'W_IPASGO_DADOS_PROF_TRAT','CD_FUNCAO',a.ie_funcao),3,
    	(nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)*0.3),
        (nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)*0.2))) vl_procedimento,	
	a.ie_funcao,
	a.cd_pessoa_fisica,
	sum(b.qt_procedimento),
	b.dt_procedimento,
	6 ie,
	b.cd_cgc_prestador,
	c.cd_tipo_procedimento,
	0 ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	a.ie_responsavel_credito

from	procedimento c,
	procedimento_participante a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.nr_sequencia	= a.nr_sequencia
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	a.cd_pessoa_fisica is not null
and	c.cd_procedimento	= b.cd_procedimento
and	c.ie_origem_proced	= b.ie_origem_proced
and	c.cd_tipo_procedimento not in (1,2,3,4,5,8,12,13,14,15,16,20,25,26,30,31,33,34,39,74,75,80,91,92,93,94,96,97,103)
and	somente_numero(b.cd_procedimento_convenio) not in (70033)
and	obter_se_exp_funcao_ipasgo (a.ie_funcao,nr_seq_tipo_fatura_p) = 'S'
group by	a.ie_funcao,
	a.cd_pessoa_fisica,
	b.dt_procedimento,
	b.cd_cgc_prestador,
	c.cd_tipo_procedimento,0,
	nvl(b.nr_ato_ipasgo,0),
	a.ie_responsavel_credito
/*
union all
select	sum(decode(ie_somente_vl_medico_w,'S',
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)))) vl_procedimento,
	b.ie_funcao_medico,
	b.cd_medico_executor,
	sum(b.qt_procedimento),
	min(b.dt_procedimento) dt_procedimento,
	7 ie,
	b.cd_cgc_prestador,
	null,
	null ie_ordem,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	b.ie_responsavel_credito

from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) <> 2
and	b.cd_medico_executor is not null
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	obter_se_gera_ato_ipasgo(b.cd_procedimento,b.ie_origem_proced,cd_estabelecimento_w,cd_tipo_fatura_w,ie_tipo_atendimento_w,b.cd_setor_atendimento) = 'S'
and	qt_ato_informado_w > 0
and	nvl(b.nr_ato_ipasgo,0) > 0
having	sum(decode(ie_somente_vl_medico_w,'S',
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		decode(somente_numero(b.cd_procedimento_convenio),70025, 
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')) * 
				(b.tx_procedimento/100)),0),
		nvl(((b.qt_procedimento * 
			Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'IPDE')) * 
				(b.tx_procedimento/100)),0)))) > 0
group by b.ie_funcao_medico,
	 b.cd_medico_executor,
	 b.cd_cgc_prestador,
	 nvl(b.nr_ato_ipasgo,0),
	b.ie_responsavel_credito
*/

order by 	nr_ato_ipasgo, ie_ordem desc, dt_procedimento, ie;

begin

obter_param_usuario(999, 80, obter_perfil_Ativo, nm_usuario_p, Wheb_Usuario_pck.get_cd_estabelecimento, ie_somente_vl_medico_w);
obter_param_usuario(999, 81, obter_perfil_Ativo, nm_usuario_p, Wheb_Usuario_pck.get_cd_estabelecimento, ie_gerar_anestesista_w);

begin
select	a.nr_atendimento,
	a.cd_convenio_parametro,
	a.cd_estabelecimento,
	b.ie_tipo_atendimento
into	nr_atendimento_w,
	cd_convenio_parametro_w,
	cd_estabelecimento_w,
	ie_tipo_atendimento_w
from	conta_paciente a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_interno_conta = nr_interno_conta_p;
exception
when others then
	nr_atendimento_w		:= 0;
	cd_convenio_parametro_w		:= 0;
	cd_estabelecimento_w		:= 0;
	ie_tipo_atendimento_w		:= 0;
end;

begin
select	cd_cgc
into	cd_cnpj_w
from	convenio
where	cd_convenio = cd_convenio_parametro_w;
exception
when others then
	cd_cnpj_w := '0';
end;

select	cd_cgc
into	cd_cnpj_estab_w
from	estabelecimento
where	cd_estabelecimento 	= cd_estabelecimento_w;

begin
select	cd_tipo_fatura
into	cd_tipo_fatura_w
from	fatur_tipo_fatura
where	nr_sequencia = nr_seq_tipo_fatura_p;
exception
when others then
	cd_tipo_fatura_w := 0;
end;

begin
select	count(*)
into	qt_ato_informado_w
from	procedimento_paciente
where	nr_atendimento = nr_atendimento_w
and	nr_interno_conta = nr_interno_conta_p
and	nvl(nr_ato_ipasgo,0) > 0;
exception
when others then
	qt_ato_informado_w := 0;
end;

for C01_w in C01 loop
	begin
	vl_procedimento_w       := C01_w.vl_procedimento;
	ie_funcao_medico_w      := C01_w.ie_funcao_medico;
	cd_medico_executor_w    := C01_w.cd_medico_executor;
	qt_procedimento_w       := C01_w.qt_procedimento;
	dt_procedimento_w       := C01_w.dt_procedimento;
	ie_w                    := C01_w.ie;
	cd_cgc_prestador_w      := C01_w.cd_cgc_prestador;
	cd_tipo_procedimento_w  := C01_w.cd_tipo_procedimento;
	ie_ordem_w              := C01_w.ie_ordem;
	nr_ato_ipasgo_w         := C01_w.nr_ato_ipasgo;
	ie_responsavel_credito_w        := C01_w.ie_responsavel_credito;
	
	nr_matricula_prestador_w := '';
	
	if 	(ie_responsavel_credito_w = 'M') then
		cd_tipo_prestador_w := 1;
	else
		cd_tipo_prestador_w := 2;		
	
		/*if	(cd_cnpj_estab_w <> cd_cgc_prestador_w) and	/*Alteracao solicitada na OS288985 - Quando o prestador for o proprio hospital, deve ir zeros 
			(nvl(cd_cgc_prestador_w,'0') <> '0') then*/ 
			--Comentado na OS 1077134
			select	nvl(max(cd_prestador_convenio),'00000000')
			into	nr_matricula_prestador_w
			from	convenio_prestador
			where	cd_convenio 	= cd_convenio_parametro_w
			and	cd_cgc 		= cd_cgc_prestador_w;
		--end if;
	end if;	
	
	if	(cd_tipo_fatura_w <> 4) and
		((dt_procedimento_w <> dt_procedimento_ww) or
		(dt_procedimento_ww is null)) then
		qt_ato_w	:= qt_ato_w + 1;
		qt_ato_aux_w	:= qt_ato_w;		
	elsif	(cd_tipo_fatura_w = 4) then
		begin		
		if	((dt_procedimento_ww <> dt_procedimento_w) or
			(dt_procedimento_ww is null)) then
			qt_ato_w	:= qt_ato_w + 1;
			qt_ato_aux_w	:= qt_ato_w;		
		end if;	

		if	(dt_procedimento_ww is null) then
			begin
			ds_controle_ato_w(qt_reg_w).dt_procedimento	:= dt_procedimento_w;
			ds_controle_ato_w(qt_reg_w).ato		:= qt_ato_w;
			end;
		else
			begin
			for i in 1..ds_controle_ato_w.count loop
				begin
				if	(ds_controle_ato_w(i).dt_procedimento = dt_procedimento_w) then
					begin
					qt_ato_aux_w := qt_ato_w - 1;
					qt_ato_w := ds_controle_ato_w(i).ato;
					ie_controle_w := 'S';
					end;
				end if;
				end;
			end loop;
			if	(ie_controle_w = 'N') then
				begin
				ds_controle_ato_w(qt_reg_w).dt_procedimento	:= dt_procedimento_w;
				ds_controle_ato_w(qt_reg_w).ato		:= qt_ato_w;
				qt_reg_w := qt_reg_w + 1;
				end;
			end if;			
			end;
		end if;	
				
		end;
	end if;
	
	select	coalesce(max(nr_seq_conselho),0)
	into	nr_seq_conselho_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_medico_executor_w;
	
	select	substr(max(nr_crm),1,7)
	into	nr_crm_w
	from	medico
	where	cd_pessoa_fisica	= cd_medico_executor_w;	
	
	if	(cd_cnpj_estab_w = '00418954000119') and
		(cd_tipo_fatura_w = 4) and
		(ie_funcao_medico_w = '2') then
		qt_ato_w	:= 2;			
	end if;
	/*Buscar as conversoes meio externo*/
	begin
	select	coalesce(max(cd_externo), to_char(nr_seq_conselho_w))
	into	nr_seq_conselho_w
	from	conversao_meio_externo
	where	cd_cgc 		= cd_cnpj_w
	and	upper(nm_tabela) 	= 'W_IPASGO_DADOS_PROF_TRAT'
	and	upper(nm_atributo)	= 'CD_CONSELHO'
	and	cd_interno	= to_char(nr_seq_conselho_w);
	exception
	when others then
		nr_seq_conselho_w := 0;
	end;

	begin
	select	coalesce(max(cd_externo), ie_funcao_medico_w)
	into	ie_funcao_medico_w
	from	conversao_meio_externo
	where	cd_cgc = cd_cnpj_w
	and	upper(nm_tabela) 	= 'W_IPASGO_DADOS_PROF_TRAT'
	and	upper(nm_atributo)	= 'CD_FUNCAO'
	and	cd_interno	= ie_funcao_medico_w;
	exception
	when others then
		ie_funcao_medico_w := 0;
	end;	
	
	qt_linha_arq_p := qt_linha_arq_p + 1;
	
	if	(nr_ato_ipasgo_w > 0) then
		qt_ato_w := nr_ato_ipasgo_w;
	end if;
	
	if	(qt_ato_w < 0) then
		qt_ato_w := 1;
	end if;
	
	if	(ie_funcao_medico_w = '2') then 	
		qt_ato_parecer_w := qt_ato_w;	
	end if;
	
	if	(ie_funcao_medico_w = '8') and (qt_ato_parecer_w > 0) then 	
		qt_ato_w := qt_ato_parecer_w;
		qt_ato_aux_w := qt_ato_aux_w - 1;
	end if;
	
	insert into w_ipasgo_dados_prof_trat(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_linha,
		tp_registro,
		nr_linha_atend,
		nr_linha_ato,
		cd_tipo_prestador,
		cd_conselho,
		nr_conselho,
		cd_funcao,
		cd_pediatria,
		qt_parecer,
		vl_honorario,
		nr_matricula_prestador,
		dt_mesano_referencia,
		nr_interno_conta,
		ds_linha,
		nr_seq_tipo_fatura)
	values(	w_ipasgo_dados_prof_trat_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		qt_linha_arq_p,
		7,
		qt_linha_atend_p,
		qt_ato_w,
		cd_tipo_prestador_w,
		nr_seq_conselho_w,
		somente_numero(nr_crm_w),
		ie_funcao_medico_w,
		0,
		qt_procedimento_w,
		vl_procedimento_w,
		nr_matricula_prestador_w,
		dt_mesano_referencia_p,
		nr_interno_conta_p,
		qt_linha_arq_p || '|' ||
		'7' || '|' ||
		qt_linha_atend_p || '|' ||
		qt_ato_w || '|' ||
		cd_tipo_prestador_w || '|' ||
		nr_seq_conselho_w || '|' ||
		nr_crm_w || '|' ||
		ie_funcao_medico_w || '|' ||
		'' || '|' ||
		decode(ie_funcao_medico_w, 	'7', qt_procedimento_w, 
						'8', qt_procedimento_w, '') || '|' ||
		replace(replace(Campo_Mascara_virgula_casas(vl_procedimento_w,4),'.',''),',','.') || '|' ||
		nr_matricula_prestador_w || '|' ||
		'|',
		nr_seq_tipo_fatura_p);

	dt_procedimento_ww 	:= dt_procedimento_w;
	cd_tipo_procedimento_ww	:= cd_tipo_procedimento_w;
	qt_ato_w 		:= qt_ato_aux_w;

	end;
end loop;

commit;

end hc_w_ipasgo_dados_prof_trat;
/
