create or replace
procedure atualiza_banco_extrato_js(
		nr_sequencia_p	number) is 

begin
if	(nr_sequencia_p is not null) then
	begin
	update 	banco_extrato 
	set 	dt_importacao = sysdate 
	where 	nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;
end atualiza_banco_extrato_js;
/