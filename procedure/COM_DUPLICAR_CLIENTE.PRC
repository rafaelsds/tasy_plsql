create or replace
procedure com_duplicar_cliente(	ie_produto_p		varchar,
				nr_seq_cliente_p	number,
				nm_usuario_p		Varchar2,
				cd_canal_p		number,
				cd_gestor_p		number,
				ds_historico_p		varchar,
				dt_fechamento_p		date) is 
				
nr_seq_gestor_w		number(10);
nr_seq_cliente_w	number(10);
nr_seq_canal_w		number(10);
nr_seq_canal_ww		number(10);
cd_pessoa_fisica_w	number(10);
ds_email_w		varchar2(250);

Cursor C01 is
	select	nr_sequencia,
		cd_pessoa_fisica
	from	com_cliente_gestor
	where	nr_seq_cliente = nr_seq_cliente_p
	and	dt_final is null
	order by 1;
	
Cursor C02 is
	select	nr_sequencia
	from	com_canal_cliente
	where	nr_seq_cliente = nr_seq_cliente_p
	and	dt_fim_atuacao is null
	and	ie_situacao = 'A'
	order by 1;	
begin
if	(ie_produto_p is not null) and
	(nr_seq_cliente_p is not null) then
	
	select	com_cliente_seq.nextVal
	into	nr_seq_cliente_w
	from	dual;
	
	insert into com_cliente (	
		nr_sequencia,
		cd_cnpj,
		ie_natureza,
		ie_status_neg,
		ie_fase_venda,
		ie_tipo,
		ie_classificacao,
		ie_resp_atend,
		ie_resp_implantacao,
		ie_situacao,
		ie_referencia,
		ie_recebe_visita,
		ie_hosp_escola,
		ie_acompanha_implantacao,
		ie_produto,
		cd_empresa,
		dt_atualizacao,
		nm_usuario,
		nr_seq_ativ,
		nr_seq_forma_conhec,
		qt_leito,
		qt_leito_uti,
		vl_fat_anual,
		dt_revisao_prevista,
		qt_consultores,
		dt_oficializacao_uso,
		ie_resp_coordenacao,
		qt_mes_estimado,
		qt_vidas,
		ie_revisado,
		nr_visita_pos_venda,
		cd_estabelecimento,
		pr_possib_fecham,
		dt_atualizacao_nrec,
		ie_inclusao_manual,
		nm_usuario_nrec,
		nr_seq_lead,
		ie_forma_aquisicao,
		dt_revisao_prevista_orig,
		dt_cliente,
		ie_migracao,
		dt_migrado,
		ie_etapa_duplic,
		dt_aprov_duplic)
	select	nr_seq_cliente_w,
		cd_cnpj,
		ie_natureza,
		ie_status_neg,
		ie_fase_venda,
		ie_tipo,
		ie_classificacao,
		ie_resp_atend,
		ie_resp_implantacao,
		ie_situacao,
		ie_referencia,
		ie_recebe_visita,
		ie_hosp_escola,
		ie_acompanha_implantacao,
		ie_produto_p,
		cd_empresa,
		sysdate,
		nm_usuario_p,
		nr_seq_ativ,
		nr_seq_forma_conhec,
		qt_leito,
		qt_leito_uti,
		vl_fat_anual,
		dt_fechamento_p,
		qt_consultores,
		dt_oficializacao_uso,
		ie_resp_coordenacao,
		qt_mes_estimado,
		qt_vidas,
		ie_revisado,
		nr_visita_pos_venda,
		cd_estabelecimento,
		pr_possib_fecham,
		sysdate,
		ie_inclusao_manual,
		nm_usuario_p,
		nr_seq_lead,
		ie_forma_aquisicao,
		dt_revisao_prevista_orig,
		dt_cliente,
		ie_migracao,
		dt_migrado,
		'P',
		null
	from	com_cliente 
	where	nr_sequencia = nr_seq_cliente_p;
	
	
	if (cd_gestor_p is null) and
	   (nr_seq_gestor_w is not null) then
		open C01;
		loop
		fetch C01 into	
			nr_seq_gestor_w,
			cd_pessoa_fisica_w;
		exit when C01%notfound;
			begin
			insert into com_cliente_gestor (	
				nr_sequencia,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				nr_seq_cliente,
				dt_inicial,
				dt_final,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			select	com_cliente_gestor_seq.nextVal,
				cd_pessoa_fisica,
				sysdate,
				nm_usuario_p,
				nr_seq_cliente_w,
				dt_inicial,
				dt_final,
				sysdate,
				nm_usuario_p
			from	com_cliente_gestor
			where	nr_sequencia = nr_seq_gestor_w;
			
			select obter_dados_pf_pj(cd_pessoa_fisica_w, null, 'M')
			into ds_email_w	
			from dual;
			end;
		end loop;
		close C01;
	else	insert into com_cliente_gestor (	
				nr_sequencia,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				nr_seq_cliente,
				dt_inicial,
				dt_final,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
		values		(com_cliente_gestor_seq.nextval,
				cd_gestor_p,
				sysdate,
				nm_usuario_p,
				nr_seq_cliente_w,
				null,
				null,
				sysdate,
				nm_usuario_p);
	end if;
	
	if (cd_canal_p is null) and
	   (nr_seq_canal_w is not null)	then
		open C02;
		loop
		fetch C02 into	
			nr_seq_canal_w;
		exit when C02%notfound;
			begin
			insert into com_canal_cliente (	
				nr_sequencia,
				nr_seq_canal,
				nr_seq_cliente,
				ie_tipo_atuacao,
				dt_inicio_atuacao,
				ie_situacao,
				dt_atualizacao,
				nm_usuario,
				dt_fim_atuacao,
				pr_esforco,
				cd_vendedor,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			select	com_canal_cliente_seq.nextVal,
				nr_seq_canal,
				nr_seq_cliente_w,
				ie_tipo_atuacao,
				dt_inicio_atuacao,
				ie_situacao,
				sysdate,
				nm_usuario_p,
				dt_fim_atuacao,
				pr_esforco,
				cd_vendedor,
				sysdate,
				nm_usuario_p
			from	com_canal_cliente
			where	nr_sequencia = nr_seq_canal_w;
			end;
		end loop;
		close C02;
	else 	insert into com_canal_cliente (	
			nr_sequencia,
			nr_seq_canal,
			nr_seq_cliente,
			ie_tipo_atuacao,
			dt_inicio_atuacao,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			dt_fim_atuacao,
			pr_esforco,
			cd_vendedor,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values (com_canal_cliente_seq.nextval,
			cd_canal_p,
			nr_seq_cliente_w,
			'V',
			sysdate,
			'A',
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			sysdate,
			nm_usuario_p);	
	end if;
	
	select	max(nr_seq_canal)
	into	nr_seq_canal_ww
	from	com_canal_cliente
	where	nr_seq_cliente = nr_seq_cliente_w
	and	nr_sequencia = (select	max(nr_sequencia)
				from	com_canal_cliente
				where	nr_seq_cliente = nr_seq_cliente_w);
				
	insert into com_cliente_hist (	
		nr_sequencia,
		nr_seq_cliente,
		nr_seq_tipo,
		dt_historico,
		nm_usuario,		
		dt_atualizacao,
		ds_titulo,
		nr_seq_canal,
		nr_seq_ativ,
		nr_seq_classif,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_historico)
	values(	com_cliente_hist_seq.nextVal,
		nr_seq_cliente_w,
		2,
		sysdate,
		nm_usuario_p,
		sysdate,
		'Abertura de conta',
		cd_canal_p,
		31,
		21,
		sysdate,
		nm_usuario_p,
		'Duplicado a partir do cliente '||nr_seq_cliente_p || chr(13) || ds_historico_p);

enviar_email('Duplicação de Cliente', 'Duplicado a partir do cliente ' || nr_seq_cliente_p, 'comercial@wheb.com.br', 'comercial@wheb.com.br;' || ds_email_w, nm_usuario_p, 'A');		
			
end if;

commit;

end com_duplicar_cliente;
/
