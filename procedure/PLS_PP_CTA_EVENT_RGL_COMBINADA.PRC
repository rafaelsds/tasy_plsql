create or replace
procedure pls_pp_cta_event_rgl_combinada(	nr_seq_analise_rec_p	pls_analise_conta.nr_sequencia%type,
						nr_seq_prot_rec_p	pls_rec_glosa_protocolo.nr_sequencia%type,
						nr_seq_cta_rec_p	pls_rec_glosa_conta.nr_sequencia%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type) is

qt_registro_w		pls_integer;

begin
-- puxar se tem regra de evento produ��o m�dica
select	count(1)
into	qt_registro_w
from	pls_pp_cta_combinada
where	rownum <= 1;

-- verificar se existe regra de evento cadastrado na base
if	(qt_registro_w > 0) then
	-- verifica se tem algo para atualizar nas tabelas
	pls_gerencia_upd_obj_pck.atualizar_objetos('Tasy', 'PLS_FILTRO_REGRA_EVENT_RGL_PCK.GERENCIA_REGRA_FILTRO', 'PLS_GRUPO_SERVICO_TM');
	pls_gerencia_upd_obj_pck.atualizar_objetos('Tasy', 'PLS_FILTRO_REGRA_EVENT_RGL_PCK.GERENCIA_REGRA_FILTRO', 'PLS_ESTRUTURA_MATERIAL_TM');

	-- Faz a chamada para os filtros dos eventos combinados, respons�vel por vincular um item da pls_conta_rec_resumo_item a um evento
	pls_filtro_regra_event_rgl_pck.gerencia_regra_filtro(	nr_seq_analise_rec_p, nr_seq_prot_rec_p, nr_seq_cta_rec_p,
								cd_estabelecimento_p, nm_usuario_p);
end if;

end pls_pp_cta_event_rgl_combinada;
/