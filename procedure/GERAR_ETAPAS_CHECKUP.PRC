CREATE OR REPLACE
PROCEDURE Gerar_Etapas_Checkup(
				nr_sequencia_p		Number,
				NR_SEQ_TIPO_CHECKUP_P	number default 0,
				nm_usuario_p		Varchar2) is


cd_estabelecimento_w		Number(4);
cd_setor_atendimento_w		Number(5);
dt_prevista_w			Date;
ie_sexo_w			Varchar2(1);
ie_etapa_liberada_w		Varchar2(1);
qt_idade_w			Number(3);
dt_prev_aux_w			Date;
nr_sequencia_w			Number(10);
nr_seq_tipo_checkup_w		Number(10);
qt_min_prev_w			Number(15);
nr_seq_etapa_w			Number(10);
ie_tipo_etapa_w			Varchar2(15);
dt_retorno_w			Date;
cd_empresa_w			number(10);
QT_REG_W			NUMBER(10);
cd_profissional_w		varchar2(15);
CURSOR C01 IS
	select	nr_sequencia,
		qt_min_prev,
		ie_tipo_etapa,
		cd_profissional	
	from	etapa_checkup
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w
	and	ie_situacao		= 'A'
	and	(ie_dia_semana is null or ie_dia_semana = pkg_date_utils.get_WeekDay(sysdate))
	and	((ie_sexo 		= ie_sexo_w) or
		(ie_sexo 		is null))
	and	((qt_idade_w		between qt_idade_min and qt_idade_max) or
		(qt_idade_min		is null or qt_idade_max is null))
	order by nr_seq_apres;

cursor C02 is 
	select a.nr_seq_tipo_checkup
	from   contrato_tipo_checkup_pf a,
	       empresa_pessoa_checkup b,
	       checkup c
	where  a.nr_seq_pessoa_checkup  = b.nr_sequencia
	and    b.cd_pessoa_fisica       = c.cd_pessoa_fisica
	and    c.nr_sequencia           = nr_sequencia_p;

cursor C03 is
	select 	a.nr_sequencia, 
		a.qt_min_prev, 
		a.ie_tipo_etapa,
		a.cd_profissional
	from   	etapa_checkup a,
		tipo_checkup_etapa b
	where  	a.nr_sequencia         	= b.nr_seq_etapa
	and    	b.nr_seq_tipo_checkup	= nr_seq_tipo_checkup_w
	and    	cd_estabelecimento	= cd_estabelecimento_w
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
	and	ie_situacao		= 'A'
	and	(ie_dia_semana is null or ie_dia_semana = pkg_date_utils.get_WeekDay(sysdate))
	and	((ie_sexo 		= ie_sexo_w) or
		(ie_sexo 		is null))
	and	qt_idade_w	        between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,999)
	and	Obter_Se_Gerar_etapa_ck(a.nr_sequencia,cd_empresa_w,ie_sexo_w,qt_idade_w,nr_seq_tipo_checkup_w)	= 'S'
	order by nr_seq_apres;

	
BEGIN
select	cd_estabelecimento,
	cd_setor_atendimento,
	dt_previsto,
	substr(obter_sexo_pf(cd_pessoa_fisica, 'C'),1,1),
	nvl(to_number(substr(obter_idade(obter_dados_pf_dt(cd_pessoa_fisica, 'DN'), sysdate, 'A'),1,3)),0),
	cd_empresa_REF
into	cd_estabelecimento_w,
	cd_setor_atendimento_w,
	dt_prevista_w,
	ie_sexo_w,
	qt_idade_w,
	cd_empresa_w
from	checkup
where	nr_sequencia	= nr_sequencia_p;

dt_retorno_w	:= dt_prevista_w;

if (nr_seq_tipo_checkup_p = 0) then
	begin
	
	select 	decode(max(a.nr_seq_tipo_checkup),'','N','S')
	into	ie_etapa_liberada_w
	from 	contrato_tipo_checkup_pf a,
		empresa_pessoa_checkup b,
		checkup c
	where  	a.nr_seq_pessoa_checkup  = b.nr_sequencia
	and    	b.cd_pessoa_fisica       = c.cd_pessoa_fisica
	and    	c.nr_sequencia           = nr_sequencia_p;
	
	if	(ie_etapa_liberada_w = 'S') then
		begin
		open C02;
		loop
		fetch C02 into
			nr_seq_tipo_checkup_w;
		exit when C02%notfound;
			begin
			
			open C03;
			loop
			fetch C03 into
				nr_sequencia_w,
				qt_min_prev_w,
				ie_tipo_etapa_w,
				cd_profissional_w;
			exit when C03%notfound;
				begin
				if	(ie_tipo_etapa_w = 'R') then
					dt_prevista_w	:= dt_retorno_w + (qt_min_prev_w / 1440);
				end if;
				
				select	checkup_etapa_seq.nextval
				into	nr_seq_etapa_w
				from	dual;

				dt_prev_aux_w	:= dt_prevista_w;
				if	(qt_min_prev_w < 0) then
					dt_prevista_w	:= dt_prevista_w + (qt_min_prev_w / 1440);
				end if;
				
					insert into checkup_etapa(
						nr_sequencia,
						nr_seq_checkup,
						dt_atualizacao,
						nm_usuario,
						nr_seq_etapa,
						dt_prevista,
						cd_pessoa_fisica)
					Values(	nr_seq_etapa_w,
						nr_sequencia_p,
						sysdate,
						nm_usuario_p,
						nr_sequencia_w,
						dt_prevista_w,
						decode(cd_profissional_w,'',null,cd_profissional_w));
					commit;

				dt_prevista_w	:= dt_prev_aux_w;

				if	(qt_min_prev_w > 0) then
					dt_prevista_w := dt_prevista_w + (qt_min_prev_w / 1440);	 
				end if;
				end;
			END LOOP;
			CLOSE C03;
			
			end;
		end loop;
		close C02;
	  end;	
	else
		begin
		OPEN C01;
		LOOP
		FETCH C01 into
			nr_sequencia_w,
			qt_min_prev_w,
			ie_tipo_etapa_w,
			cd_profissional_w;
		exit when c01%notfound;
			if	(ie_tipo_etapa_w = 'R') then
				dt_prevista_w	:= dt_retorno_w + (qt_min_prev_w / 1440);
			end if;
			
			select	checkup_etapa_seq.nextval
			into	nr_seq_etapa_w
			from	dual;

			dt_prev_aux_w	:= dt_prevista_w;
			if	(qt_min_prev_w < 0) then
				dt_prevista_w	:= dt_prevista_w + (qt_min_prev_w / 1440);
			end if;

			insert into checkup_etapa(
				nr_sequencia,
				nr_seq_checkup,
				dt_atualizacao,
				nm_usuario,
				nr_seq_etapa,
				dt_prevista,
				cd_pessoa_fisica)
			Values(	nr_seq_etapa_w,
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_w,
				dt_prevista_w,
				decode(cd_profissional_w,'',null,cd_profissional_w));
			commit;

			dt_prevista_w	:= dt_prev_aux_w;

			if	(qt_min_prev_w > 0) then
				dt_prevista_w := dt_prevista_w + (qt_min_prev_w / 1440);	 
			end if;

		END LOOP;
		CLOSE C01;
		end;
	end if;
	end;
elsif 	(nr_seq_tipo_checkup_p > 0) then
	begin
	nr_seq_tipo_checkup_w := nr_seq_tipo_checkup_p;

	
	select  COUNT(*)
	INTO	QT_REG_W
	from   tipo_checkup a,
	   empresa_tipo_checkup b,
	   tipo_checkup_lib c,
	   TIPO_CHECKUP_LIB d
	where  a.nr_sequencia = b.nr_seq_tipo_checkup
	and    a.nr_sequencia = c.nr_seq_tipo_checkup
	and   b.cd_empresa    = cd_empresa_w
	and   a.nr_sequencia  = d.nr_seq_tipo_checkup
	and   d.cd_estabelecimento  = cd_estabelecimento_w
	and   a.ie_situacao  = 'A'
	and	a.nr_sequencia	= nr_seq_tipo_checkup_w;
	
	

	
	IF	(QT_REG_W	= 0) THEN
		wheb_mensagem_pck.exibir_mensagem_abort(175931);
	END IF;
	
	
	select  COUNT(*)
	INTO	QT_REG_W
	from   tipo_checkup a,
	   empresa_tipo_checkup b,
	   tipo_checkup_lib c,
	   TIPO_CHECKUP_LIB d
	where  a.nr_sequencia = b.nr_seq_tipo_checkup
	and    a.nr_sequencia = c.nr_seq_tipo_checkup
	and   b.cd_empresa    = cd_empresa_w
	and   a.nr_sequencia  = d.nr_seq_tipo_checkup
	and   d.cd_estabelecimento  = cd_estabelecimento_w
	and   a.ie_situacao  = 'A'
	and	a.nr_sequencia	= nr_seq_tipo_checkup_w
	and	(b.cd_estabelecimento is null or b.cd_estabelecimento  = cd_estabelecimento_w);
	
	if	(QT_REG_W	= 0) then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(175933);
		end;
	end if;
	
	update	checkup
	set	nr_seq_tipo_checkup 	= nr_seq_tipo_checkup_p
	where	nr_sequencia		= nr_sequencia_p;		
	commit;
	
	open C03;
	loop
	fetch C03 into
		nr_sequencia_w,
		qt_min_prev_w,
		ie_tipo_etapa_w,
		cd_profissional_w;
	exit when C03%notfound;
		if	(ie_tipo_etapa_w = 'R') then
			dt_prevista_w	:= dt_retorno_w + (qt_min_prev_w / 1440);
		end if;
		
		select	checkup_etapa_seq.nextval
		into	nr_seq_etapa_w
		from	dual;

		dt_prev_aux_w	:= dt_prevista_w;
		if	(qt_min_prev_w < 0) then
			dt_prevista_w	:= dt_prevista_w + (qt_min_prev_w / 1440);
		end if;

		insert into checkup_etapa(
			nr_sequencia,
			nr_seq_checkup,
			dt_atualizacao,
			nm_usuario,
			nr_seq_etapa,
			dt_prevista,
			cd_pessoa_fisica)
		Values(	nr_seq_etapa_w,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			nr_sequencia_w,
			dt_prevista_w,
			decode(cd_profissional_w,'',null,cd_profissional_w));
		commit;

		dt_prevista_w	:= dt_prev_aux_w;

		if	(qt_min_prev_w > 0) then
			dt_prevista_w := dt_prevista_w + (qt_min_prev_w / 1440);	 
		end if;

	END LOOP;
	CLOSE C03;
	end;
end if;

END Gerar_Etapas_Checkup;
/
