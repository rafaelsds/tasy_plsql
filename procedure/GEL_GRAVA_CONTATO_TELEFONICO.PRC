create or replace
procedure GEL_grava_contato_telefonico(	nr_seq_prescricao_p		number,
					nr_prescricao_p			number,
					cd_pessoa_fisica_p			varchar2,
					ds_observacao_p			varchar2,
					nm_contato_p			varchar2,
					nm_usuario_p			Varchar2) is 

nr_sequencia_w		number(10);					
					
begin

select	prescr_proc_contato_seq.nextval
into	nr_sequencia_w
from	dual;

insert into prescr_proc_contato (nr_sequencia,
			 dt_atualizacao,
			 nm_usuario,
			 dt_atualizacao_nrec,
			 nm_usuario_nrec,
			 nr_prescricao,
			 nr_seq_prescricao,
			 cd_pessoa_contato,
			 ds_observacao,
			 nm_contato)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_prescricao_p,
			nr_seq_prescricao_p,
			cd_pessoa_fisica_p,
			substr(ds_observacao_p,1,255),
			substr(nm_contato_p,1,60));

commit;

end GEL_grava_contato_telefonico;
/