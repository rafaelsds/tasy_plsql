create or replace 
procedure gerar_evento_paciente_ciclo(	nr_seq_evento_p		number,
					nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					nr_seq_paciente_p	number,
					ds_dias_gerados_p	varchar2,
					nm_usuario_p		varchar2) is

ie_forma_ev_w		varchar2(15);
ie_pessoa_destino_w	varchar2(15);
cd_pf_destino_w		varchar2(10);
ds_mensagem_w		varchar2(4000);
ds_titulo_w		varchar2(100);
cd_pessoa_destino_w	varchar2(10);
nr_sequencia_w		number(10);
ds_maquina_w		varchar2(80);
nm_paciente_w		varchar2(60);
ds_unidade_w		varchar2(60);
ds_setor_atendimento_w	varchar2(60);
ie_usuario_aceite_w	varchar2(1);
qt_corresp_w		number(5);
cd_setor_atendimento_w	number(5,0);
cd_perfil_w		number(5,0);
cd_pessoa_regra_w	varchar2(10);
nr_ramal_w		varchar2(10);
nr_telefone_w		varchar2(40);
cd_convenio_w		number(10,0);
dt_liberacao_w		date	:= sysdate;
nm_usuario_destino_w	varchar2(15);
cd_setor_atend_pac_w	number(5,0);
ds_cid_w			varchar2(240);
cd_cid_w			varchar2(10);
cd_protocolo_w			number(10);
nr_seq_medicamento_w		number(6);
cursor C01 is
	select	ie_forma_ev,
		ie_pessoa_destino,
		cd_pf_destino,
		nvl(ie_usuario_aceite,'N'),
		cd_setor_atendimento,
		cd_perfil
	from	ev_evento_regra_dest
	where	nr_seq_evento	= nr_seq_evento_p
	and	nvl(cd_convenio, nvl(cd_convenio_w,0))	= nvl(cd_convenio_w,0)
	and	nvl(cd_setor_atend_pac, nvl(cd_setor_atend_pac_w,0))	= nvl(cd_setor_atend_pac_w,0)	
	order by ie_forma_ev;

cursor C02 is
	select	obter_dados_usuario_opcao(a.nm_usuario,'C')
	from	usuario_setor_v a,
		usuario b
	where	a.nm_usuario = b.nm_usuario
	and	b.ie_situacao = 'A'
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	ie_forma_ev_w in (2,3)
	and	obter_dados_usuario_opcao(a.nm_usuario,'C') is not null;

cursor C03 is
	select	obter_dados_usuario_opcao(a.nm_usuario,'C'),
			a.nm_usuario
	from	usuario_perfil a,
		usuario b
	where	a.nm_usuario = b.nm_usuario
	and	b.ie_situacao = 'A'
	and	a.cd_perfil = cd_perfil_w
	and	ie_forma_ev_w in (2,3)
	and	obter_dados_usuario_opcao(a.nm_usuario,'C') is not null;

begin
select	substr(obter_inf_sessao(0),1,80)
into	ds_maquina_w
from	dual;



select	ds_titulo,
	ds_mensagem
into	ds_titulo_w,
	ds_mensagem_w
from	ev_evento
where	nr_sequencia	= nr_seq_evento_p;

select	nvl(max(obter_convenio_atendimento(nr_atendimento_p)), 0)
into	cd_convenio_w
from 	dual;

select	substr(obter_cid_atendimento(nr_atendimento_p, 'P'),1,10),
	substr(obter_desc_cid_doenca(obter_cid_atendimento(nr_atendimento_p, 'P')),1,240)
into	cd_cid_w,
	ds_cid_w
from 	dual;

select	substr(obter_nome_pf(cd_pessoa_fisica_p),1,60),
	substr(obter_unidade_atendimento(nr_atendimento_p,'A','U'),1,60),
	substr(obter_unidade_atendimento(nr_atendimento_p,'A','RA'),1,60),
	substr(obter_unidade_atendimento(nr_atendimento_p,'A','TL'),1,60),
	substr(obter_unidade_atendimento(nr_atendimento_p,'A','S'),1,60),
	substr(obter_unidade_atendimento(nr_atendimento_p,'A','CS'),1,60)
into	nm_paciente_w,
	ds_unidade_w,
	nr_ramal_w,
	nr_telefone_w,
	ds_setor_atendimento_w,
	cd_setor_atend_pac_w
from	dual;


select	cd_protocolo,
	nr_seq_medicacao
into	cd_protocolo_w,
	nr_seq_medicamento_w
from	paciente_setor
where	nr_seq_paciente	= nr_seq_paciente_p;


ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@paciente',nm_paciente_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@quarto',ds_unidade_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@setor',ds_setor_atendimento_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@atendimento',nr_atendimento_p),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ramal',nr_ramal_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@telefone',nr_telefone_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@convenio',cd_convenio_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@cd_cid',cd_cid_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ds_cid',ds_cid_w),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ds_protocolo',substr(obter_desc_protocolo(cd_protocolo_w),1,255)),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ds_medicacao',substr(obter_desc_protocolo_medic(nr_seq_medicamento_w,cd_protocolo_w),1,50)),1,4000);
ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ds_dias_gerados',ds_dias_gerados_p),1,4000);

select	ev_evento_paciente_seq.nextval
into	nr_sequencia_w
from	dual;

insert into ev_evento_paciente(
	nr_sequencia,
	nr_seq_evento,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_pessoa_fisica,
	nr_atendimento,
	ds_titulo,
	ds_mensagem,
	ie_status,
	ds_maquina,
	dt_evento,
	dt_liberacao,
	ie_situacao)
values(	nr_sequencia_w,
	nr_seq_evento_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_pessoa_fisica_p,
	nr_atendimento_p,
	ds_titulo_w,
	ds_mensagem_w,
	'G',
	ds_maquina_w,
	dt_liberacao_w,
	dt_liberacao_w,
	'A');

open C01;
loop
fetch C01 into
	ie_forma_ev_w,
	ie_pessoa_destino_w,
	cd_pf_destino_w,
	ie_usuario_aceite_w,
	cd_setor_atendimento_w,
	cd_perfil_w;
exit when C01%notfound;
	begin
	qt_corresp_w	:= 1;
	cd_pessoa_destino_w	:= null;
	if	(ie_pessoa_destino_w = '1') then /* M�dico do atendimento */
		begin
		select	max(cd_medico_atendimento)
		into	cd_pessoa_destino_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p;
		end;
	elsif	(ie_pessoa_destino_w = '2') then /*M�dico respons�vel pelo paciente*/
		begin
		select	max(cd_medico_resp)
		into	cd_pessoa_destino_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p;
		end;
	elsif	(ie_pessoa_destino_w = '4') then /*M�dico referido*/
		begin
		select	max(cd_medico_referido)
		into	cd_pessoa_destino_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p;
		end;
	elsif	(ie_pessoa_destino_w = '5') or (ie_pessoa_destino_w = '12') then /*Pessoa fixa ou Usu�rio fixo*/
		cd_pessoa_destino_w	:= cd_pf_destino_w;
	end if;

	if	(ie_usuario_aceite_w = 'S') and
		(cd_pessoa_destino_w is not null) and
		(ie_forma_ev_w = '1') then
		begin
		select	count(*)
		into	qt_corresp_w
		from	pessoa_fisica_corresp
		where	cd_pessoa_fisica	= cd_pessoa_destino_w
		and	ie_tipo_corresp		= 'MCel'
		and	ie_tipo_doc		= 'AE';
		end;
	elsif	(ie_usuario_aceite_w = 'S') and
		(cd_pessoa_destino_w is not null) and
		(ie_forma_ev_w = '3') then
		begin
		select	count(*)
		into	qt_corresp_w
		from	pessoa_fisica_corresp
		where	cd_pessoa_fisica	= cd_pessoa_destino_w
		and	ie_tipo_corresp		= 'CI'
		and	ie_tipo_doc		= 'AE';
		end;
	elsif	(ie_usuario_aceite_w = 'S') and
		(cd_pessoa_destino_w is not null) and
		(ie_forma_ev_w = '4') then
		begin
		select	count(*)
		into	qt_corresp_w
		from	pessoa_fisica_corresp
		where	cd_pessoa_fisica	= cd_pessoa_destino_w
		and	ie_tipo_corresp		= 'Email'
		and	ie_tipo_doc		= 'AE';
		end;
	end if;

	if	(cd_pessoa_destino_w is not null) and
		(qt_corresp_w > 0) then
		begin

		insert into ev_evento_pac_destino(
			nr_sequencia,
			nr_seq_ev_pac,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			ie_forma_ev,
			ie_status,
			dt_ciencia,
			ie_pessoa_destino,
			dt_evento)
		values(	ev_evento_pac_destino_seq.nextval,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_destino_w,
			ie_forma_ev_w,
			'G',
			null,
			ie_pessoa_destino_w,
			sysdate);
		end;
	end if;

	open C02;
	loop
	fetch C02 into
		cd_pessoa_regra_w;
	exit when C02%notfound;
		begin
		if	(cd_pessoa_regra_w is not null) then
			insert into ev_evento_pac_destino(
				nr_sequencia,
				nr_seq_ev_pac,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				ie_forma_ev,
				ie_status,
				dt_ciencia,
				ie_pessoa_destino,
				dt_evento)
			values(	ev_evento_pac_destino_seq.nextval,
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_pessoa_regra_w,
				ie_forma_ev_w,
				'G',
				null,
				ie_pessoa_destino_w,
				sysdate);
		end if;
		end;
	end loop;
	close C02;

	open C03;
	loop
	fetch C03 into
		cd_pessoa_regra_w,
		nm_usuario_destino_w;
	exit when C03%notfound;
		begin

		if	(cd_pessoa_regra_w is not null) then

			insert into ev_evento_pac_destino(
				nr_sequencia,
				nr_seq_ev_pac,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				ie_forma_ev,
				ie_status,
				dt_ciencia,
				nm_usuario_DEst,
				ie_pessoa_destino,
				dt_evento)
			values(	ev_evento_pac_destino_seq.nextval,
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,	
				nm_usuario_p,
				cd_pessoa_regra_w,
				ie_forma_ev_w,
				'G',
				null,
				nm_usuario_Destino_w,
				ie_pessoa_destino_w,
				sysdate);	
		end if;
		end;
	end loop;
	close C03;

	end;
end loop;
close C01;

commit;

end gerar_evento_paciente_ciclo;
/