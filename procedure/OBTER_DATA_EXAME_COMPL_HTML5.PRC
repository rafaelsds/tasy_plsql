create or replace
function obter_data_exame_compl_html5 (nr_seq_exame_compl_p		number)
				       return Date is

ds_retorno_w 	Date;
			       
begin
if (nr_seq_exame_compl_p is not null) then
	select 	MAX(r.dt_atualizacao)
	into 	ds_retorno_w 
	from   	prescr_proc_exame_compl r,
		patologia_exame_compl p
	where  	r.nr_seq_pato_exame = p.nr_sequencia
	and	r.nr_sequencia = nr_seq_exame_compl_p;
end if;

return	ds_retorno_w;

end obter_data_exame_compl_html5;
/
