create or replace procedure pls_gerar_alerta_analise_aut
			(	ie_tipo_evento_p		number,
				nr_seq_auditoria_p		number,
				nm_usuario_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	IE_FORMA_ENVIO

	EM - Email
	CI - Comunica��o interna
	SMS -  SMS
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	PLS_GERAR_ALERTA_EVENTO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
type  regras_validas_r is record(
  nr_seq_regra_evento	pls_regra_geracao_evento.nr_sequencia%type,
  nr_seq_evento_mens	pls_regra_geracao_evento.nr_seq_evento_mens%type
);

type vetor is table of regras_validas_r index by binary_integer;
vetor_w				vetor;

nr_vetor_w			pls_integer;
ie_existe_w			varchar2(1) := 'N';
regras_validas_w		regras_validas_r;
ds_mensagem_regra_w		pls_alerta_evento_mensagem.ds_mensagem%type;
ds_mensagem_formatada_w		varchar2(4000)	:= '';
ds_atributo_w			varchar2(4000)	:= '';
nm_usuario_destino_definit_w	varchar2(4000);
nm_usuarios_lista_w		varchar2(4000);
ds_procedimento_w		varchar2(255);
ds_titulo_w			pls_alerta_evento_mensagem.ds_titulo%type;
ds_remetente_w			pls_alerta_evento_mensagem.ds_remetente_sms%type;
ds_destinatario_w		varchar2(255);
nm_beneficiario_w		varchar2(70);
cd_usuario_plano_w		varchar2(30);
cd_guia_w			pls_auditoria.cd_guia%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nm_usuario_destino_w		varchar2(15);
nr_telef_celular_benef_w	pls_auditoria.nr_telef_celular_benef%type := '';
dt_validade_senha_w		varchar2(10)	:= null;
ie_forma_envio_w		pls_alerta_evento_destino.ie_forma_envio%type;
ie_tipo_guia_w			pls_auditoria.ie_tipo_guia%type;
ie_tipo_envio_w			pls_alerta_evento_mensagem.ie_tipo_envio%type;
ie_quantidade_igual_w		pls_regra_geracao_evento.ie_quantidade_igual%type;
ie_todos_itens_w		pls_regra_geracao_evento.ie_todos_itens%type;
ie_status_item_regra_w		pls_regra_geracao_evento.ie_status_item%type;
ie_status_regra_w		varchar2(1)	:= 'N';
ie_item_regra_w			varchar2(1)	:= 'N';
ie_status_auditoria_w		pls_auditoria_item.ie_status%type;
ie_quantidade_regra_w		varchar2(1)	:= 'N';
ie_regra_sem_item_w		varchar2(1)	:= 'N';
ie_tipo_item_w			varchar2(1)	:= '';
ie_todos_itens_regra_w		varchar2(1)	:= 'N';
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
nr_seq_evento_mens_w		pls_regra_geracao_evento.nr_seq_evento_mens%type;
cd_procedimento_w		procedimento.cd_procedimento%type;
nr_seq_material_w		pls_material.nr_sequencia%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;
nr_seq_evento_dest_w		pls_alerta_evento_destino.nr_sequencia%type;
nr_seq_tipo_evento_w		pls_tipo_alerta_evento.nr_sequencia%type;
nr_seq_evento_controle_w	number(10)	:= null;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
qt_item_regra_w			number(10);
nr_seq_regra_evento_w		pls_regra_geracao_evento.nr_sequencia%type;
qt_tamanho_atributo_w		number(10)	:= 0;
id_sms_w			number(10)	:= null;
qt_proc_w			number(10);
qt_ajuste_ww			varchar2(255);
qt_original_ww			varchar2(255);
qt_ajuste_w			number(8,4);
qt_original_w			number(8,4);
ie_origem_proced_w		procedimento.ie_origem_proced%type;
nr_pos_virgula_w		number(5);
qt_contador_w			number(5)	:= 0;
qt_qtde_itens_regra_proc_w	number(5)	:= 0;
qt_qtde_itens_regra_mat_w	number(5)	:= 0;
qt_item_w			number(5)	:= 0;
qt_aud_status_aprovada_w	number(5)	:= 0;
qt_aud_status_negada_w		number(5)	:= 0;
qt_aud_status_cancelado_w	number(5)	:= 0;
qt_item_igual_w			number(5)	:= 0;
qt_macro_regra_w		number(5)	:= 0;
qt_macro_regra_proc_w		number(5)	:= 0;
qt_macro_regra_mat_w		number(5)	:= 0;
qt_aud_status_w			number(5)	:= 0;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_estagio_req_w		pls_requisicao.ie_estagio%type;
dt_liberacao_w			pls_auditoria.dt_liberacao%type;
ie_tipo_pessoa_dest_w		pls_alerta_evento_destino.ie_tipo_pessoa_dest%type;
ds_email_remetente_w		pls_alerta_evento_mensagem.ds_remetente_email%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
ds_email_dest_w			compl_pessoa_fisica.ds_email%type;
nr_seq_grupo_servico_w		number(10);
nr_seq_grupo_material_w		number(10);
cd_proc_regra_w			pls_regra_evento_proc.cd_procedimento%type;
ie_origem_proc_regra_w		pls_regra_evento_proc.ie_origem_proced%type;
nr_seq_material_regra_w		pls_regra_evento_mat.nr_seq_material%type;
ie_proc_mat_regra_w		varchar2(1);
ie_tipo_processo_w		pls_regra_geracao_evento.ie_tipo_processo%type;
cd_lista_proc_w  		varchar2(510) := '';
ie_lista_orig_proc_w		varchar2(255) := '';
cd_lista_mat_w			varchar2(4000) := '';
ie_lista_status_proc_w		varchar2(4000) := '';
qt_mat_w			pls_integer := 0;
ie_status_proc_w		varchar2(255) := '';
ie_stauts_req_w			pls_requisicao.ie_estagio%type;
ie_status_guia_w			pls_guia_plano.ie_status%type;
ie_status_analise_w			pls_regra_geracao_evento.ie_status_requisicao%type;

Cursor C01 is
	select	nr_seq_evento_mens,
		ie_status_item,
		ie_todos_itens,
		ie_quantidade_igual,
		nr_sequencia,
		ie_status_requisicao
	from	pls_regra_geracao_evento
	where	ie_situacao 		= 'A'
	and	ie_evento_disparo 	= ie_tipo_evento_p
	and	(ie_tipo_guia is null  or ie_tipo_guia	= ie_tipo_guia_w)
        and	(ie_tipo_processo is null or ie_tipo_processo = ie_tipo_processo_w);

Cursor C02 (nr_seq_evento_mens_p pls_regra_geracao_evento.nr_seq_evento_mens%type) is
	select	nr_sequencia,
		ie_forma_envio,
		ie_tipo_pessoa_dest
	from	pls_alerta_evento_destino
	where	nr_seq_evento_mens	= nr_seq_evento_mens_p
	and	ie_situacao 		= 'A';

Cursor C03 is
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_material,
		ie_status,
		ptu_obter_qtd_env_itens_scs(null, qt_ajuste, 'R'),
		ptu_obter_qtd_env_itens_scs(null, qt_original, 'R')
	from	pls_auditoria_item
	where	nr_seq_auditoria	= nr_seq_auditoria_p
	order by nr_sequencia;

Cursor C05 is
	select	cd_procedimento,
		ie_origem_proced,
		decode(ie_status,'A','Aprovado','N','Negado','Pendente')
	from	pls_auditoria_item
	where	nr_seq_auditoria	= nr_seq_auditoria_p
	and	ie_status <> 'C'
	and	cd_procedimento is not null;

Cursor C06 is
	select	'P' ie_tipo,
		a.nr_seq_evento_mens,
		a.ie_status_item,
		a.ie_todos_itens,
		a.ie_quantidade_igual,
		a.nr_sequencia,
		b.cd_procedimento cd_procedimento,
		b.ie_origem_proced ie_origem_proced,
		null nr_seq_material,
		b.nr_seq_grupo_servico nr_seq_grupo_servico,
		null nr_seq_grupo_material,
		a.ie_status_requisicao
	from	pls_regra_geracao_evento a,
		pls_regra_evento_proc b
	where	a.nr_sequencia		= b.nr_seq_regra_evento
	and	a.ie_situacao 		= 'A'
	and	a.ie_evento_disparo 	= ie_tipo_evento_p
	and	(a.ie_tipo_guia is null  or a.ie_tipo_guia = ie_tipo_guia_w)
        	and	(a.ie_tipo_processo is null  or a.ie_tipo_processo = ie_tipo_processo_w)
	union
	select 	'M' ie_tipo,
		a.nr_seq_evento_mens,
		a.ie_status_item,
		a.ie_todos_itens,
		a.ie_quantidade_igual,
		a.nr_sequencia,
		null cd_procedimento,
		null ie_origem_proced,
		nr_seq_material nr_seq_material,
		null nr_seq_grupo_servico,
		b.nr_seq_grupo_material nr_seq_grupo_material,
		a.ie_status_requisicao
	from	pls_regra_geracao_evento a,
		pls_regra_evento_mat b
	where	a.nr_sequencia		= b.nr_seq_regra_evento
	and	a.ie_situacao 		= 'A'
	and	a.ie_evento_disparo 	= ie_tipo_evento_p
	and	(a.ie_tipo_guia is null  or a.ie_tipo_guia = ie_tipo_guia_w)
        	and	(a.ie_tipo_processo is null  or a.ie_tipo_processo = ie_tipo_processo_w);


Cursor C07 is
	select	nm_usuario_destino
	from	pls_alerta_event_dest_fixo
	where	nr_seq_alerta_event_dest	= nr_seq_evento_dest_w;

Cursor C08 is
	select	nr_sequencia
	from	pls_auditoria_item
	where	nr_seq_auditoria	= nr_seq_auditoria_p
	and	ie_status <> 'C'
	and	nr_seq_material is not null;

begin

select	nr_seq_guia,
	nr_seq_requisicao,
	cd_guia,
	dt_liberacao,
	ie_tipo_guia,
	nr_telef_celular_benef,
	pls_obter_dados_segurado(nr_seq_segurado, 'PF'),
	cd_estabelecimento,
	nr_seq_segurado,
	nr_seq_prestador
into	nr_seq_guia_w,
	nr_seq_requisicao_w,
	cd_guia_w,
	dt_liberacao_w,
	ie_tipo_guia_w,
	nr_telef_celular_benef_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	nr_seq_segurado_w,
	nr_seq_prestador_w
from	pls_auditoria
where	nr_sequencia	= nr_seq_auditoria_p;

begin
nm_beneficiario_w	:= substr(pls_obter_dados_segurado(nr_seq_segurado_w, 'N'),1,70);
exception
when others then
	nm_beneficiario_w	:= null;
end;

begin
cd_usuario_plano_w	:= substr(pls_obter_dados_segurado(nr_seq_segurado_w,'CR'),1,20);
exception
when others then
	cd_usuario_plano_w	:= null;
end;

select	count(1)
into	qt_item_w
from	pls_auditoria_item
where	nr_seq_auditoria	= nr_seq_auditoria_p;

select	count(1)
into	qt_aud_status_aprovada_w
from	pls_auditoria_item
where	nr_seq_auditoria	= nr_seq_auditoria_p
and	ie_status = 'A';

select	count(1)
into	qt_aud_status_negada_w
from	pls_auditoria_item
where	nr_seq_auditoria	= nr_seq_auditoria_p
and	ie_status = 'N';

select	count(1)
into	qt_aud_status_cancelado_w
from	pls_auditoria_item
where	nr_seq_auditoria	= nr_seq_auditoria_p
and	ie_status = 'C';

select	count(1)
into	qt_item_igual_w
from	pls_auditoria_item
where	nr_seq_auditoria	= nr_seq_auditoria_p
and	qt_original		= qt_ajuste;

if	(nr_seq_requisicao_w is not null) then
	begin
		select	ie_estagio,
				to_char(dt_validade_senha,'dd/mm/yyyy'),
				ie_tipo_processo,
				ie_estagio
		into	ie_estagio_req_w,
				dt_validade_senha_w,
				ie_tipo_processo_w,
				ie_stauts_req_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
	exception
	when others then
		ie_estagio_req_w	:= null;
		dt_validade_senha_w	:= null;
		ie_tipo_processo_w	:= null;
		ie_stauts_req_w		:= null;
	end;
elsif   	(nr_seq_guia_w is not null) then
        	select  ie_tipo_processo,
					ie_status
        	into    ie_tipo_processo_w,
					ie_status_guia_w
        	from    pls_guia_plano
        	where   nr_sequencia = nr_seq_guia_w;
end if;

ds_destinatario_w	:= nr_telef_celular_benef_w;

open C03;
loop
fetch C03 into
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_material_w,
	ie_status_auditoria_w,
	qt_ajuste_ww,
	qt_original_ww;
exit when C03%notfound;
	begin

	begin
		qt_ajuste_w	:= to_number(qt_ajuste_ww);
	exception
	when others then
		qt_ajuste_w 	:= to_number(replace(qt_ajuste_ww,',','.'));
	end;

	begin
		qt_original_w	:= to_number(qt_original_w);
	exception
	when others then
		qt_original_w 	:= to_number(replace(qt_original_w,',','.'));
	end;

	open C06;
	loop
	fetch C06 into
		ie_tipo_item_w,
		nr_seq_evento_mens_w,
		ie_status_item_regra_w,
		ie_todos_itens_w,
		ie_quantidade_igual_w,
		nr_seq_regra_evento_w,
		cd_proc_regra_w,
		ie_origem_proc_regra_w,
		nr_seq_material_regra_w,
		nr_seq_grupo_servico_w,
		nr_seq_grupo_material_w,
		ie_status_analise_w;
	exit when C06%notfound;
		begin
		ie_status_regra_w 	:= 'N';
		ie_todos_itens_regra_w	:= 'N';
		ie_proc_mat_regra_w	:= 'N';
		qt_aud_status_w		:= 0;

		if (ie_status_analise_w is not null) then
			if (ie_status_analise_w = 1) then
				if (ie_status_guia_w is not null) and (ie_status_guia_w = 1) then
					ie_status_regra_w := 'S';
				elsif (ie_stauts_req_w is not null) and (ie_stauts_req_w in (2,6)) then
					ie_status_regra_w := 'S';
				end if;
			elsif (ie_status_analise_w = 3) then
				if (ie_status_guia_w is not null) and (ie_status_guia_w = 3) then
					ie_status_regra_w := 'S';
				elsif (ie_stauts_req_w is not null) and (ie_stauts_req_w in (7)) then
					ie_status_regra_w := 'S';
				end if;
			end if;

			if	(ie_status_regra_w = 'S') then

				if	(vetor_w.count	= 0) then
					nr_vetor_w	:= vetor_w.count+1;
					vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
					vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
				else
					for i in 1..vetor_w.count loop
						if	(vetor_w(i).nr_seq_regra_evento = nr_seq_regra_evento_w) then
							ie_existe_w	:= 'S';
						end if;
					end loop;

					if	(ie_existe_w = 'N') then
						nr_vetor_w	:= vetor_w.count+1;
						vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
						vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
					end if;

					ie_existe_w := 'N';
				end if;
			end if;
		else
			qt_item_regra_w	:= qt_item_regra_w + 1;
			if	(ie_status_item_regra_w is not null) then
				if	(ie_status_auditoria_w = ie_status_item_regra_w) then
					ie_status_regra_w := 'S';
				end if;
			else
				ie_status_regra_w	:= 'S';
			end if;

			if	(ie_quantidade_igual_w = 'S') then
				if	(qt_ajuste_w  = qt_original_w) then
					ie_quantidade_regra_w	:= 'S';
				end if;
			else
				ie_quantidade_regra_w	:= 'S';
			end if;

			if	(ie_status_item_regra_w is not null) and
				(ie_todos_itens_w = 'S') then
				if	(ie_status_item_regra_w = 'A') and
					(qt_item_igual_w = qt_item_w)  then
					qt_aud_status_w	:= qt_aud_status_aprovada_w;
				elsif	(ie_status_item_regra_w = 'N') then
					qt_aud_status_w := qt_aud_status_negada_w;
				elsif	(ie_status_item_regra_w = 'C') then
					qt_aud_status_w := qt_aud_status_cancelado_w;
				elsif	(ie_status_item_regra_w = 'R') then
					if	(qt_item_igual_w <> qt_item_w) then
						qt_aud_status_w	:= qt_item_w - qt_item_igual_w;
					end if;
				end if;

				if	(qt_aud_status_w = qt_item_w) then
					ie_todos_itens_regra_w	:= 'S';
				end if;
			else
				ie_todos_itens_regra_w	:= 'S';
			end if;

			if	(cd_procedimento_w is not null) and (ie_origem_proced_w is not null) then
				if	(nvl(cd_proc_regra_w,0) =  cd_procedimento_w) and (nvl(ie_origem_proc_regra_w,0) = ie_origem_proced_w) then
					ie_proc_mat_regra_w	:= 'S';

				elsif	(nr_seq_grupo_servico_w is not null) then
					ie_proc_mat_regra_w := pls_se_grupo_preco_servico(nr_seq_grupo_servico_w, cd_procedimento_w, ie_origem_proced_w);

				end if;

			elsif	(nr_seq_material_w is not null) then
				if	(nvl(nr_seq_material_regra_w,0) = nr_seq_material_w) then
					ie_proc_mat_regra_w	:= 'S';

				elsif	(nr_seq_grupo_material_w is not null) then
					ie_proc_mat_regra_w := pls_se_grupo_preco_material(nr_seq_grupo_material_w, nr_seq_material_w);

				end if;
			end if;

			if	(ie_status_regra_w = 'S') and
				(ie_quantidade_regra_w = 'S') and
				(ie_todos_itens_regra_w = 'S') and
				(ie_proc_mat_regra_w = 'S') then

				if	(vetor_w.count	= 0) then
					nr_vetor_w	:= vetor_w.count+1;
					vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
					vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
				else
					for i in 1..vetor_w.count loop
						if	(vetor_w(i).nr_seq_regra_evento = nr_seq_regra_evento_w) then
							ie_existe_w	:= 'S';
						end if;
					end loop;

					if	(ie_existe_w = 'N') then
						nr_vetor_w	:= vetor_w.count+1;
						vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
						vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
					end if;

					ie_existe_w := 'N';
				end if;
			end if;
		end if;
		end;
	end loop;
	close C06;
	end;
end loop;
close C03;
if	(vetor_w.count	= 0) then
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_mens_w,
		ie_status_item_regra_w,
		ie_todos_itens_w,
		ie_quantidade_igual_w,
		nr_seq_regra_evento_w,
		ie_status_analise_w;
	exit when C01%notfound;
		/* Valida��es da regra */
		ie_status_regra_w 	:= 'N';
		ie_quantidade_regra_w	:= 'N';
		qt_aud_status_w		:= 0;

		if (ie_status_analise_w is not null) then
			if (ie_status_analise_w = 1) then
				if (ie_status_guia_w is not null) and (ie_status_guia_w = 1) then
					ie_status_regra_w := 'S';
				elsif (ie_stauts_req_w is not null) and (ie_stauts_req_w in (2,6)) then
					ie_status_regra_w := 'S';
				end if;

			elsif (ie_status_analise_w = 3) then
				if (ie_status_guia_w is not null) and (ie_status_guia_w = 3) then
					ie_status_regra_w := 'S';
				elsif (ie_stauts_req_w is not null) and (ie_stauts_req_w in (7)) then
					ie_status_regra_w := 'S';
				end if;
			end if;

			if	(ie_status_regra_w = 'S') then
				if	(vetor_w.count	= 0) then
					nr_vetor_w	:= vetor_w.count+1;
					vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
					vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
				else
					for i in 1..vetor_w.count loop
						if	(vetor_w(i).nr_seq_regra_evento = nr_seq_regra_evento_w) then
							ie_existe_w	:= 'S';
						end if;
					end loop;

					if	(ie_existe_w = 'N') then
						nr_vetor_w	:= vetor_w.count+1;
						vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
						vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
					end if;

					ie_existe_w := 'N';
				end if;
			end if;
		else
			if	(ie_status_item_regra_w is not null) then
				if	(ie_status_item_regra_w = 'A') and
					(qt_item_igual_w = qt_item_w)  then
					qt_aud_status_w := qt_aud_status_aprovada_w;
				elsif	(ie_status_item_regra_w = 'N') then
					qt_aud_status_w	:= qt_aud_status_negada_w;
				elsif	(ie_status_item_regra_w = 'C') then
					qt_aud_status_w := qt_aud_status_cancelado_w;
				elsif	(ie_status_item_regra_w = 'R') then
					if	(qt_item_igual_w <> qt_item_w) then
						qt_aud_status_w	:= qt_item_w - qt_item_igual_w;
					end if;
				end if;

				if	(ie_todos_itens_w = 'S') then
					if	(qt_aud_status_w = qt_item_w) then
						ie_status_regra_w	:= 'S';
					end if;
				else
					if	(qt_aud_status_w > 0) then
						ie_status_regra_w	:= 'S';
					end if;
				end if;
			else
				ie_status_regra_w	:= 'S';
			end if;

			ie_quantidade_regra_w	:= 'S';

			if	(ie_quantidade_igual_w = 'S') then
				ie_quantidade_regra_w	:= 'N';

				if	(qt_item_igual_w = qt_item_w) then
					ie_quantidade_regra_w	:= 'S';
				end if;
			end if;

			select	count(1)
			into	qt_qtde_itens_regra_proc_w
			from	pls_regra_evento_proc
			where	nr_seq_regra_evento 	= nr_seq_regra_evento_w;

			select	count(1)
			into	qt_qtde_itens_regra_mat_w
			from	pls_regra_evento_mat
			where	nr_seq_regra_evento 	= nr_seq_regra_evento_w;

			if	(ie_quantidade_regra_w = 'S') and
				(qt_qtde_itens_regra_proc_w = 0) and
				(qt_qtde_itens_regra_mat_w = 0) and
				(ie_status_regra_w = 'S') then

				if	(vetor_w.count	= 0) then
					nr_vetor_w	:= vetor_w.count+1;
					vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
					vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
				else
					for i in 1..vetor_w.count loop
						if	(vetor_w(i).nr_seq_regra_evento = nr_seq_regra_evento_w) then
							ie_existe_w	:= 'S';
						end if;
					end loop;

					if	(ie_existe_w = 'N') then
						nr_vetor_w	:= vetor_w.count+1;
						vetor_w(nr_vetor_w).nr_seq_regra_evento	:= nr_seq_regra_evento_w;
						vetor_w(nr_vetor_w).nr_seq_evento_mens	:= nr_seq_evento_mens_w;
					end if;

					ie_existe_w := 'N';
				end if;
			end if;
		end if;
	end loop;
	close C01;
end if;

if	(vetor_w.count	> 0) and
	/*Francisco - 22/07/2012 - Se o est�gio for Aguardando envio intercambio, ir� gerar quando finalizar a auditoria do SCS*/
	(nr_seq_requisicao_w is null or ie_estagio_req_w <> 10) then
	for i in 1..vetor_w.count loop
		begin

			select	ds_mensagem,
				ie_tipo_envio,
				ds_titulo,
				nr_seq_tipo_evento,
				ds_remetente_sms,
				ds_remetente_email
			into	ds_mensagem_regra_w,
				ie_tipo_envio_w,
				ds_titulo_w,
				nr_seq_tipo_evento_w,
				ds_remetente_w,
				ds_email_remetente_w
			from	pls_alerta_evento_mensagem
			where 	nr_sequencia	= vetor_w(i).nr_seq_evento_mens;
		exception
		when others then
			ds_mensagem_regra_w	:= null;
			ie_tipo_envio_w		:= null;
			ds_titulo_w		:= null;
			nr_seq_tipo_evento_w	:= null;
			ds_remetente_w		:= null;
			nr_seq_tipo_evento_w	:= null;
		end;
		if	(nr_seq_tipo_evento_w is not null) then

			select	pls_alerta_evento_controle_seq.nextval
			into	nr_seq_evento_controle_w
			from	dual;

			insert into pls_alerta_evento_controle
				(nr_sequencia,
				dt_geracao_evento,
				ie_evento_disparo,
				nr_seq_tipo_evento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_mensagem,
				ds_titulo,
				ie_tipo_envio,
				nr_seq_auditoria)
			values	(nr_seq_evento_controle_w,
				sysdate,
				ie_tipo_evento_p,
				nr_seq_tipo_evento_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_mensagem_regra_w,
				ds_titulo_w,
				ie_tipo_envio_w,
				nr_seq_auditoria_p);

			open C02 (vetor_w(i).nr_seq_evento_mens);
			loop
			fetch C02 into
				nr_seq_evento_dest_w,
				ie_forma_envio_w,
				ie_tipo_pessoa_dest_w;
			exit when C02%notfound;
				begin

				cd_lista_mat_w := '';
				if	(nr_seq_evento_controle_w is null) then
					select	pls_alerta_evento_controle_seq.nextval
					into	nr_seq_evento_controle_w
					from	dual;

					insert into pls_alerta_evento_controle
						(nr_sequencia,
						dt_geracao_evento,
						ie_evento_disparo,
						nr_seq_tipo_evento,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ds_mensagem,
						ds_titulo,
						ie_tipo_envio,
						nr_seq_auditoria)
					values	(nr_seq_evento_controle_w,
						sysdate,
						ie_tipo_evento_p,
						nr_seq_tipo_evento_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						ds_mensagem_regra_w,
						ds_titulo_w,
						ie_tipo_envio_w,
						nr_seq_auditoria_p);
				end if;

				if	(ie_forma_envio_w = 'SMS') then
					ds_remetente_w		:= substr(ds_remetente_w,0,40);
					ds_destinatario_w	:= substr(ds_destinatario_w,0,40);

					if	(ds_remetente_w is not null) and
						(ds_destinatario_w is not null) and
						(ds_mensagem_regra_w is not null) then

						select	count(1)
						into	qt_macro_regra_proc_w
						from	dual
						where	ds_mensagem_regra_w like '%@DS_PROCEDIMENTO%'
						or	ds_mensagem_regra_w like '%@DS_PROC_INTERNO%'
						or	ds_mensagem_regra_w like '%@DS_PROC_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_PROC_INT_QUEBRA%';

						ds_destinatario_w	:= replace(ds_destinatario_w,'(','');
						ds_destinatario_w	:= replace(ds_destinatario_w,')','');
						ds_destinatario_w	:= replace(ds_destinatario_w,'-','');

						select	count(1)
						into	qt_macro_regra_mat_w
						from	dual
						where	ds_mensagem_regra_w like '%@DS_MATERIAL_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MATERIAL_PTU_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_OU_ORIG_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_MARCA_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_MARCA_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_ORIG_MARCA_QUEBRA%';

						if 	(qt_macro_regra_proc_w > 0) or (qt_macro_regra_mat_w > 0) then

							qt_contador_w := 0;

							if 	(qt_macro_regra_proc_w > 0) then
								select	count(1)
								into	qt_proc_w
								from	pls_auditoria_item
								where	nr_seq_auditoria = nr_seq_auditoria_p
								and	cd_procedimento is not null
								and	ie_status <> 'C';

								open C05;
								loop
								fetch C05 into
									cd_procedimento_w,
									ie_origem_proced_w,
									ie_status_proc_w;
								exit when C05%notfound;
									begin

									cd_lista_proc_w	:= substr(cd_lista_proc_w || cd_procedimento_w || ',',1,510);
									ie_lista_orig_proc_w := substr(ie_lista_orig_proc_w || ie_origem_proced_w || ',',1,255);
									ie_lista_status_proc_w := substr(ie_lista_status_proc_w || ie_status_proc_w || ',',1,4000);
									qt_contador_w	:= qt_contador_w + 1;
									if	(qt_contador_w = 3) or (( qt_proc_w < 3) and (qt_proc_w = qt_contador_w))then
										ds_mensagem_formatada_w := substr(pls_substituir_macros(ds_mensagem_regra_w, null, nr_seq_auditoria_p, null, cd_lista_proc_w, ie_lista_orig_proc_w, null, ie_lista_status_proc_w),1,4000);

										/*
										IE_STATUS_ENVIO_P
										E - Mensagem enviada
										A - Aguardando envio

										IE_FORMA_ENVIO_P
										EM - Email
										CI - Comunica��o interna
										SMS - SMS
										*/

										pls_enviar_sms_alerta_evento(	ds_remetente_w,
														ds_destinatario_w,
														substr(ds_mensagem_formatada_w,1,150),
														nr_seq_tipo_evento_w,
														nm_usuario_p,
														id_sms_w);

										pls_gerar_destino_evento(nr_seq_evento_controle_w,
														'E',
														ie_forma_envio_w,
														nm_usuario_p,
														cd_pessoa_fisica_w,
														nm_usuario_destino_w,
														ds_mensagem_formatada_w,
														id_sms_w,ds_destinatario_w);

										if	(nr_seq_requisicao_w is not null) then
											pls_requisicao_gravar_hist(	nr_seq_requisicao_w,
															'AG',
															'Remetente: ' || ds_remetente_w || chr(13) || chr(10) ||
															'Destino: ' || ds_destinatario_w || chr(13) || chr(10) ||
															'Mensagem: ' || chr(13) || chr(10) ||
															substr(ds_mensagem_formatada_w,1,150),
															null,
															nm_usuario_p);
										end if;

										qt_proc_w	:= qt_proc_w - qt_contador_w;
										qt_contador_w	:= 0;
										cd_lista_proc_w	:= '';
										ie_lista_orig_proc_w := '';
									end if;
									end;
								end loop;
								close c05;
							end if;

							qt_contador_w := 0;

							if 	(qt_macro_regra_mat_w > 0) then
								select	count(1)
								into	qt_mat_w
								from	pls_auditoria_item
								where	nr_seq_auditoria = nr_seq_auditoria_p
								and	nr_seq_material is not null
								and	ie_status <> 'C';

								for	r_C08_w in C08 loop
									cd_lista_mat_w 	:= substr(cd_lista_mat_w || r_C08_w.nr_sequencia || ',',1,4000);
									qt_contador_w	:= qt_contador_w + 1;
									if	(qt_contador_w = 3) or (( qt_mat_w < 3) and (qt_mat_w = qt_contador_w))then
										ds_mensagem_formatada_w := substr(pls_substituir_macros(ds_mensagem_regra_w, null, nr_seq_auditoria_p, null, null, null, cd_lista_mat_w, null),1,4000);

										pls_enviar_sms_alerta_evento(	ds_remetente_w,
														ds_destinatario_w,
														substr(ds_mensagem_formatada_w,1,150),
														nr_seq_tipo_evento_w,
														nm_usuario_p,
														id_sms_w);

										pls_gerar_destino_evento(	nr_seq_evento_controle_w,
														'E',
														ie_forma_envio_w,
														nm_usuario_p,
														cd_pessoa_fisica_w,
														nm_usuario_destino_w,
														ds_mensagem_formatada_w,
														id_sms_w,
														ds_destinatario_w);

										if	(nr_seq_requisicao_w is not null) then
											pls_requisicao_gravar_hist(	nr_seq_requisicao_w,
															'AG',
															'Remetente: ' || ds_remetente_w || chr(13) || chr(10) ||
															'Destino: ' || ds_destinatario_w || chr(13) || chr(10) ||
															'Mensagem: ' || chr(13) || chr(10) ||
															substr(ds_mensagem_formatada_w,1,150),
															null,
															nm_usuario_p);
										end if;

										qt_mat_w	:= qt_mat_w - qt_contador_w;
										qt_contador_w	:= 0;
										cd_lista_mat_w	:= '';
									end if;
								end loop;
							end if;
						else
							select	count(1)
							into	qt_macro_regra_w
							from	dual
							where	ds_mensagem_regra_w like '%@%';

							if	(qt_macro_regra_w > 0) then
								ds_mensagem_regra_w := substr(pls_substituir_macros(ds_mensagem_regra_w, null, nr_seq_auditoria_p, null, null, null),1,4000);
							end if;

							pls_enviar_sms_alerta_evento(	ds_remetente_w,
										ds_destinatario_w,
										substr(ds_mensagem_regra_w,1,150),
										nr_seq_tipo_evento_w,
										nm_usuario_p,
										id_sms_w);

							pls_gerar_destino_evento(nr_seq_evento_controle_w,
										'E',
										ie_forma_envio_w,
										nm_usuario_p,
										cd_pessoa_fisica_w,
										nm_usuario_destino_w,
										ds_mensagem_regra_w,
										id_sms_w,
										ds_destinatario_w);

							if	(nr_seq_requisicao_w is not null) then
								pls_requisicao_gravar_hist(	nr_seq_requisicao_w,
												'AG',
												'Remetente: ' || ds_remetente_w || chr(13) || chr(10) ||
												'Destino: ' || ds_destinatario_w || chr(13) || chr(10) ||
												'Mensagem: ' || chr(13) || chr(10) ||
												substr(ds_mensagem_regra_w,1,150),
												null,
												nm_usuario_p);
							end if;
						end if;
					end if;
				end if;

				if	(ie_forma_envio_w = 'CI') then
					select	count(1)
					into	qt_macro_regra_w
					from	dual
					where	ds_mensagem_regra_w like '%@%';

					if	(qt_macro_regra_w > 0) then

						select	count(1)
						into	qt_macro_regra_proc_w
						from	dual
						where	ds_mensagem_regra_w like '%@DS_PROCEDIMENTO%'
						or	ds_mensagem_regra_w like '%@DS_PROC_INTERNO%'
						or	ds_mensagem_regra_w like '%@DS_PROC_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_PROC_INT_QUEBRA%';

						if 	(qt_macro_regra_proc_w > 0) then
							open C05;
							loop
							fetch C05 into
								cd_procedimento_w,
								ie_origem_proced_w,
								ie_status_proc_w;
							exit when C05%notfound;
								begin
								cd_lista_proc_w  := substr(cd_lista_proc_w || cd_procedimento_w || ',',1,510);
								ie_lista_orig_proc_w := substr(ie_lista_orig_proc_w || ie_origem_proced_w || ',',1,255);
								ie_lista_status_proc_w := substr(ie_lista_status_proc_w || ie_status_proc_w || ',',1,4000);
								end;
							end loop;
							close C05;
						end if;

						select	count(1)
						into	qt_macro_regra_mat_w
						from	dual
						where	ds_mensagem_regra_w like '%@DS_MATERIAL_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MATERIAL_PTU_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_OU_ORIG_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_MARCA_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_MARCA_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_ORIG_MARCA_QUEBRA%';

						if 	(qt_macro_regra_mat_w > 0) then
							for	r_C08_w in C08 loop
								cd_lista_mat_w  := substr(cd_lista_mat_w || r_C08_w.nr_sequencia || ',',1,4000);
							end loop;
						end if;

						ds_mensagem_regra_w := substr(pls_substituir_macros(ds_mensagem_regra_w, null, nr_seq_auditoria_p, null, cd_lista_proc_w, ie_lista_orig_proc_w, cd_lista_mat_w, ie_lista_status_proc_w),1,4000);
					end if;

					open C07;
					loop
					fetch C07 into
						nm_usuario_destino_definit_w;
					exit when C07%notfound;
						begin

						if	(nm_usuario_destino_definit_w is not null) and
							(ds_mensagem_regra_w is not null) then
							gerar_comunic_padrao(	sysdate,
										ds_titulo_w,
										ds_mensagem_regra_w,
										nm_usuario_p,
										'N',
										nm_usuario_destino_definit_w,
										'N',
										null,
										null,
										cd_estabelecimento_w,
										null,
										sysdate,
										null,
										null);
						end if;

						nm_usuarios_lista_w	:= nm_usuario_destino_definit_w || ',';

						while	(nm_usuarios_lista_w is not null) loop
							begin
							nr_pos_virgula_w	:= instr(nm_usuarios_lista_w, ',');
							if	(nr_pos_virgula_w > 0) then
								nm_usuario_destino_w	:= substr(nm_usuarios_lista_w,1,nr_pos_virgula_w-1);
								nm_usuarios_lista_w	:= substr(nm_usuarios_lista_w,nr_pos_virgula_w+1,length(nm_usuarios_lista_w));

								if	(nm_usuario_destino_w is not null) and
									(ds_mensagem_regra_w is not null) and
									(nvl(nr_seq_evento_controle_w, 0) > 0) then
									pls_gerar_destino_evento(nr_seq_evento_controle_w,
												'E',
												ie_forma_envio_w,
												nm_usuario_p,
												cd_pessoa_fisica_w,
												nm_usuario_destino_w,
												ds_mensagem_regra_w,
												null,
												null);
								end if;
							end if;
							end;
						end loop;
						end;
					end loop;
					close C07;
				end if;

				if	(ie_forma_envio_w = 'EM') and (nvl(ds_email_remetente_w,'X') <> 'X') then

					select	count(1)
					into	qt_macro_regra_w
					from	dual
					where	ds_mensagem_regra_w like '%@%';

					if	(qt_macro_regra_w > 0) then

						select	count(1)
						into	qt_macro_regra_proc_w
						from	dual
						where	ds_mensagem_regra_w like '%@DS_PROCEDIMENTO%'
						or	ds_mensagem_regra_w like '%@DS_PROC_INTERNO%'
						or	ds_mensagem_regra_w like '%@DS_PROC_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_PROC_INT_QUEBRA%';

						if 	(qt_macro_regra_proc_w > 0) then
							open C05;
							loop
							fetch C05 into
								cd_procedimento_w,
								ie_origem_proced_w,
								ie_status_proc_w;
							exit when C05%notfound;
								begin
								cd_lista_proc_w  := substr(cd_lista_proc_w || cd_procedimento_w || ',',1,510);
								ie_lista_orig_proc_w := substr(ie_lista_orig_proc_w || ie_origem_proced_w || ',',1,255);
								ie_lista_status_proc_w := substr(ie_lista_status_proc_w || ie_status_proc_w || ',',1,4000);
								end;
							end loop;
							close C05;
						end if;

						select	count(1)
						into	qt_macro_regra_mat_w
						from	dual
						where	ds_mensagem_regra_w like '%@DS_MATERIAL_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MATERIAL_PTU_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_OU_ORIG_MARCA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_MARCA_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_MARCA_QUEBRA%'
						or	ds_mensagem_regra_w like '%@DS_MAT_PTU_ORIG_MARCA_QUEBRA%';

						if 	(qt_macro_regra_mat_w > 0) then
							for	r_C08_w in C08 loop
								cd_lista_mat_w  := substr(cd_lista_mat_w || r_C08_w.nr_sequencia || ',',1,4000);
							end loop;
						end if;

						ds_mensagem_regra_w := substr(pls_substituir_macros(ds_mensagem_regra_w, null, nr_seq_auditoria_p, null, cd_lista_proc_w, ie_lista_orig_proc_w, cd_lista_mat_w, ie_lista_status_proc_w),1,4000);
					end if;

					if	(ie_tipo_pessoa_dest_w = 'BE') then
						ds_email_dest_w := substr(pls_obter_dados_segurado(nr_seq_segurado_w,'E'), 0, 255);

						if	(nvl(ds_email_dest_w,'X') <> 'X') then
							enviar_email(	ds_titulo_w, ds_mensagem_regra_w, ds_email_remetente_w,
									ds_email_dest_w, nm_usuario_p, 'M');
						end if;
					elsif	(ie_tipo_pessoa_dest_w = 'PR') then
						ds_email_dest_w := substr(pls_obter_dados_prestador(nr_seq_prestador_w,'M'), 0, 255);

						if	(nvl(ds_email_dest_w,'X') <> 'X') then
							enviar_email(	ds_titulo_w, ds_mensagem_regra_w, ds_email_remetente_w,
									ds_email_dest_w, nm_usuario_p, 'M');
						end if;
					end if;

					pls_gerar_destino_evento(nr_seq_evento_controle_w, 'E', ie_forma_envio_w,
						nm_usuario_p, cd_pessoa_fisica_w, '',
						ds_mensagem_regra_w, null, null);
        end if;
        end;
      end loop;
      close C02;
    end if;
  end loop;
end if;

end pls_gerar_alerta_analise_aut;
/