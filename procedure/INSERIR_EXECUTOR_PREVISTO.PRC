create or replace
procedure inserir_executor_previsto(	cd_profissional_p		varchar2,
				nr_sequencia_p		number,
				nr_prescricao_p		number,
				nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p > 0) then
	update	prescr_procedimento
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		cd_profissional		= cd_profissional_p
	where	nr_sequencia		= nr_sequencia_p
	and	nr_prescricao		= nr_prescricao_p;
	commit;
end if;

end inserir_executor_previsto;
/
