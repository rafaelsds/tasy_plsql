CREATE OR REPLACE
procedure Gerar_w_ctb_bal_periodo_mes(	cd_empresa_p		number,
					cd_estab_p		number,
					cd_classificacao_p		varchar2,
					dt_inicio_p		date,
					dt_final_p			date,
					ie_normal_encerramento_p	varchar2,
					cd_conta_contabil_p	varchar2,
					nr_nivel_p		varchar2,
					ie_diario_mensal_p		varchar2,
					cd_centro_custo_p	varchar2,
					ie_tipo_conta_p		varchar2,
					nm_usuario_p		varchar2) is


nr_seq_mes_ref_w			number(10);
ie_normal_encerramento_w		varchar2(1);
cd_empresa_w			number(4);
cd_conta_contabil_w		varchar2(20);
cd_classificacao_w			varchar2(40);
cd_grupo_w			number(10);
ie_tipo_w				varchar2(1);
ie_nivel_w			number(10);
ie_centro_custo_w			varchar2(1);
dt_referencia_w			date;
ds_conta_apres_w			varchar2(100);
vl_debito_w			number(15,2);
vl_credito_w			number(15,2);
vl_saldo_w			number(15,2);
vl_movimento_w			number(15,2);
vl_saldo_ant_w			number(15,2);
qt_conta_w			number(10);
ie_diario_mensal_w			varchar2(01);
cd_estabelecimento_w		number(15,0);
dt_inicio_w			date;
dt_final_w			date;
cd_conta_debito_w			Varchar(20);
cd_conta_credito_w		Varchar(20);
dt_movimento_w			Date;

vl_debito_per_w			number(15,2);
vl_credito_per_w			number(15,2);
vl_movto_per_w			number(15,2);
vl_movto_ant_w			number(15,2);
ie_deb_cre_cta_w			Varchar2(01);
ie_deb_cre_mov_w			Varchar2(01);
cd_centro_custo_w		number(10);
ie_tipo_conta_w			varchar2(1);

cursor c01 is
select	x.dt_referencia,
	a.cd_estabelecimento,
	nvl(a.cd_centro_custo,0),
	a.cd_conta_contabil,
	a.cd_classificacao,
	a.cd_grupo,
	a.ie_tipo,
	ctb_obter_nivel_classif_conta(a.cd_classificacao),
	a.ie_centro_custo,
	substr(a.ds_conta_apres,1,100),
	sum(decode(x.dt_referencia, dt_inicio_w, a.vl_saldo_ant, 0)),
	nvl(sum(a.vl_saldo),0) vl_saldo,
	nvl(sum(a.vl_debito),0) vl_debito,
	nvl(sum(a.vl_credito),0) vl_credito,
	nvl(sum(a.vl_movimento),0) vl_movimento
from	ctb_balancete_v2 a,
	ctb_mes_ref x
where	a.nr_seq_mes_ref =			x.nr_sequencia
and	x.dt_referencia between dt_inicio_w and dt_final_w
and	((a.cd_estabelecimento		= cd_estab_p) or (nvl(cd_estab_p,0) = 0))
and	((nvl(cd_classificacao_p,'0') = '0') or (ctb_obter_se_conta_classif_sup(a.cd_conta_contabil,cd_classificacao_p) = 'S'))
and	(ctb_obter_nivel_classif_conta(a.cd_classificacao) <= nr_nivel_p)
and	((nvl(cd_conta_contabil_p,'0') = '0') or (ctb_obter_se_conta_contida(a.cd_conta_contabil, cd_conta_contabil_p) = 'S'))
and	((nvl(cd_centro_custo_p,'0') = '0') or (ctb_obter_se_centro_contido(a.cd_centro_custo, cd_centro_custo_p) = 'S'))
and	a.ie_normal_encerramento			= ie_normal_encerramento_p
and	a.cd_empresa				= cd_empresa_p
and	((ie_tipo_conta_w = 'X') or (a.ie_tipo	= ie_tipo_conta_w))
and	((cd_centro_custo_w = 0) or (nvl(a.cd_centro_custo, cd_centro_custo_w)	= nvl(cd_centro_custo_w,a.cd_centro_custo)))
group by x.dt_referencia,
	a.cd_estabelecimento,
	nvl(a.cd_centro_custo,0),
	a.cd_conta_contabil,
	a.cd_classificacao,
	a.cd_grupo,
	a.ie_tipo,
	ctb_obter_nivel_classif_conta(a.cd_classificacao),
	a.ie_centro_custo,
	a.ds_conta_apres;

BEGIN

dt_inicio_w		:= trunc(dt_inicio_p, 'month');
dt_final_w		:= trunc(nvl(dt_final_p,dt_inicio_p), 'month');
ie_diario_mensal_w	:= 'M';
qt_conta_w		:= 0;
ie_tipo_conta_w		:= nvl(ie_tipo_conta_p,'X');

delete from w_ctb_balancete
where nm_usuario = nm_usuario_p;
commit;

if	(nvl(cd_centro_custo_p,'0') = '0') then
	cd_centro_custo_w := 0;
end if;
	
OPEN  c01;
LOOP
FETCH c01 into
	dt_referencia_w,
	cd_estabelecimento_w,
	cd_centro_custo_w,
	cd_conta_contabil_w,
	cd_classificacao_w,
	cd_grupo_w,
	ie_tipo_w,
	ie_nivel_w,
	ie_centro_custo_w,
	ds_conta_apres_w,
	vl_saldo_ant_w,
	vl_saldo_w,
	vl_debito_w,
	vl_credito_w,
	vl_movimento_w;
EXIT when c01%notfound;

	insert	into w_ctb_balancete(
		ie_normal_encerramento,
		cd_empresa,
		cd_estabelecimento,
		cd_conta_contabil,
		cd_classificacao,
		cd_grupo,
		ie_tipo,
		ie_nivel,
		ie_centro_custo,
		ds_conta_apres,
		vl_debito,
		vl_credito,
		vl_saldo,
		vl_movimento,
		vl_saldo_ant,
		nm_usuario,
		dt_atualizacao,
		dt_referencia,
		cd_centro_custo)
	values(	ie_normal_encerramento_p,
		cd_empresa_p,
		cd_estab_p,
		cd_conta_contabil_w,
		cd_classificacao_w,
		cd_grupo_w,
		ie_tipo_w,
		ie_nivel_w,
		ie_centro_custo_w,
		ds_conta_apres_w,
		vl_debito_w,
		vl_credito_w,
		vl_saldo_w,
		vl_movimento_w,
		vl_saldo_ant_w,
		nm_usuario_p,
		sysdate,
		dt_referencia_w,
		cd_centro_custo_w);
END LOOP;
CLOSE c01;
commit;

END Gerar_w_ctb_bal_periodo_mes;
/