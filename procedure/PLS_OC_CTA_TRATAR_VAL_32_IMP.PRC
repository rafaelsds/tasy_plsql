create or replace
procedure pls_oc_cta_tratar_val_32_imp(	nr_seq_combinada_p	in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p	in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p	in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

qt_registro_valido_w	pls_integer;
nr_indice_w				pls_integer;
dados_cab_regra_w		pls_simult_concor_pck.t_dado_cab_regra;
ie_gera_ocor_item_w		pls_combinacao_item_tm.ie_gera_ocorrencia%type;
ds_sql_w				varchar2(12000);
ds_observacao_w			varchar2(4000);
dt_item_w				date;
ie_proc_e_w				varchar2(1);
ie_proc_ou_w			varchar2(1);
ie_mat_e_w				varchar2(1);
ie_mat_ou_w				varchar2(1);
dados_regra_w			pls_tipos_ocor_pck.dados_regra;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
bind_sql_w				sql_pck.t_dado_bind;
cursor_w				sql_pck.t_cursor;
ie_tipo_registro_w		pls_selecao_ocor_cta.ie_tipo_registro%type;
nr_seq_selecao_w		pls_selecao_ocor_cta.nr_sequencia%type;
tb_seq_selecao_w		pls_util_cta_pck.t_number_table;
tb_valido_w				pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w				pls_util_cta_pck.t_varchar2_table_4000;
qt_tot_agrup_proc_and_w 	pls_integer;
qt_tot_agrup_mat_and_w		pls_integer;
nr_seq_combinacao_item_w	pls_oc_cta_val_comb_item.nr_seq_combinacao_item%type;
cd_guia_referencia_w		pls_conta_v.cd_guia_referencia%type;
nr_seq_segurado_w			pls_conta_v.nr_seq_segurado%type;
qt_agrup_item_w				pls_integer;
qt_casou_and_w				pls_integer;
qt_casou_ou_w				pls_integer;
qt_casou_pacote_and_w		pls_integer;
qt_casou_pacote_ou_w		pls_integer;
dt_item_ini_w				date;
dt_item_fim_w				date;
qt_total_item_guia_w		pls_integer;
ie_atendeu_regra_w			boolean;
qt_dif_dias_w				pls_integer;
qt_tot_casou_proc_and_w			pls_integer;
qt_tot_casou_proc_ou_w			pls_integer;
qt_tot_casou_proc_pacote_and_w	pls_integer;
qt_tot_casou_proc_pacote_or_w	pls_integer;
qt_tot_casou_mat_and_w			pls_integer;
qt_tot_casou_mat_ou_w			pls_integer;
qt_tot_item_guia_proc_w			pls_integer;
qt_tot_item_guia_mat_w			pls_integer;
nr_seq_prestador_exec_princ_w	pls_conta.nr_seq_prestador_exec_princ%type;

-- regras de combinacao de itens que serao processadas na validacao
cursor c_somente_regra (	nr_seq_oc_cta_comb_p	pls_oc_cta_combinada.nr_sequencia%type) is
	select	a.nr_seq_combinacao_item
	from	pls_oc_cta_val_comb_item a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

-- todos os procedimentos da selecao que existem na regra de combinacao de itens que esta sendo processada
cursor c_reg_existe_proc(	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type,
				nr_seq_comb_item_p	pls_oc_cta_val_comb_item.nr_seq_combinacao_item%type) is
	select	a.nr_sequencia,
			'S' ie_valido,
			a.ds_observacao
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		exists (select	1
					from	pls_combinacao_item_tm z
					where	z.nr_seq_combinacao = nr_seq_comb_item_p
					and	z.ie_tipo_registro = 'P'
					and	z.ie_origem_proced = b.ie_origem_proced_conv
					and	z.cd_procedimento = b.cd_procedimento_conv)
	union all
	-- pacotes nao existem dentro de pls_combinacao_item_tm
	select	a.nr_sequencia,
			'S' ie_valido,
			a.ds_observacao
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		b.ie_tipo_despesa_conv = '4'
	and		exists (select	1
					from	pls_comb_cta_item_proc z
					where	z.nr_seq_combinacao = nr_seq_comb_item_p
					and		z.ie_tipo_desp_proc = '4');	

-- todos os materiais da selecao que existem na regra de combinacao de itens que esta sendo processada
cursor c_reg_existe_mat(nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type,
						nr_seq_comb_item_p	pls_oc_cta_val_comb_item.nr_seq_combinacao_item%type) is
	select	a.nr_sequencia,
			'S' ie_valido,
			a.ds_observacao
	from	pls_oc_cta_selecao_imp a,
			pls_conta_mat_imp b
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		b.nr_sequencia = a.nr_seq_conta_mat
	and		exists (select	1
					from	pls_combinacao_item_tm z
					where	z.nr_seq_combinacao = nr_seq_comb_item_p
					and		z.ie_tipo_registro = 'M'
					and		z.nr_seq_material = b.nr_seq_material_conv);
			
-- Informacoes da validacao de itens simultaneos/concorrentes
cursor c_regra_comb (	nr_seq_oc_cta_comb_p	pls_oc_cta_combinada.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
			a.nr_seq_combinacao_item,
			b.nr_sequencia nr_seq_combinada,
			b.dt_inicio_vigencia,
			b.dt_fim_vigencia,
			nvl(b.qt_dias_considerar, 0) qt_dias_considerar,
			nvl(b.ie_conta_igual_comb, 'N') ie_conta_igual_comb,
			nvl(b.ie_ou_excecao, 'N') ie_ou_excecao,
			nvl(b.ie_ocorrencia_conta, 'N') ie_ocorrencia_conta,
			nvl(b.ie_tipo_verificacao, '1') ie_tipo_verificacao,
			   (select	count(1)
			from	pls_combinacao_item_tm x
			where  	x.nr_seq_combinacao = b.nr_sequencia
			and	x.ie_tipo_registro  = 'P' ) qt_regra_proc,
			(	select	count(1)
				from	pls_comb_cta_item_proc z
				where   z.nr_seq_combinacao = b.nr_sequencia
				and	z.ie_tipo_desp_proc = '4') qt_regra_pacote,
			(	select	count(1)
				from	pls_combinacao_item_tm x
				where  	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'P'
				and	x.ie_and_or = 'AND') qt_regra_proc_and,
			(	select	count(1)
				from	pls_comb_cta_item_proc z
				where  	z.nr_seq_combinacao = b.nr_sequencia
				and	z.ie_tipo_desp_proc = '4'
				and	z.ie_and_or = 'AND') qt_regra_pacote_and, 
			(   	select	count(1)
				from	pls_comb_cta_item_proc z
				where  	z.nr_seq_combinacao = b.nr_sequencia
				and	z.ie_tipo_desp_proc = '4'
				and	z.ie_and_or = 'OR') qt_regra_pacote_or,
			(   	select	count(1)
				from	pls_combinacao_item_tm x
				where  	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'P'
				and	x.ie_and_or = 'OR') qt_regra_proc_or,
			(   	select	count(1)
				from	pls_combinacao_item_tm x
				where  	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'M') qt_regra_mat,
			(   	select	count(1)
				from	pls_combinacao_item_tm x
				where  	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'M'
				and	x.ie_and_or = 'AND') qt_regra_mat_and,
			(   	select	count(1)
				from	pls_combinacao_item_tm x
				where  	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'M'
				and	x.ie_and_or = 'OR') qt_regra_mat_or,
			(   	select	count(distinct x.ie_agrupamento_item)
				from	pls_combinacao_item_tm x
				where	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'P'
				and	x.ie_and_or = 'AND') qt_grupos_distintos_proc,
			(	select	count(distinct x.ie_agrupamento_item)
				from	pls_combinacao_item_tm x
				where	x.nr_seq_combinacao = b.nr_sequencia
				and	x.ie_tipo_registro  = 'M'
				and	x.ie_and_or = 'AND') qt_grupos_distintos_mat
	from	pls_oc_cta_val_comb_item a,
			pls_combinacao_item_cta b
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p
	and		b.nr_sequencia 		= a.nr_seq_combinacao_item;

cursor c_guia(	nr_id_transacao_pc			pls_oc_cta_selecao_imp.nr_id_transacao%type,
				nr_seq_combinacao_item_pc	pls_oc_cta_val_comb_item.nr_seq_combinacao_item%type) is
	select	distinct sel.nr_seq_segurado,
			sel.cd_guia_referencia,
			(select max(x.nr_seq_prestador_exec_princ) from pls_conta x where x.nr_sequencia = sel.nr_seq_conta) nr_seq_prestador_exec_princ
	from	pls_oc_cta_selecao_imp sel
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and		sel.ie_valido = 'S'
	and	exists (select 	1
			from   	pls_conta_proc_imp p,
					pls_combinacao_item_tm a
			where   p.nr_sequencia      = sel.nr_seq_conta_proc
			and   	a.cd_procedimento   = p.cd_procedimento_conv
			and     a.ie_origem_proced  = p.ie_origem_proced_conv
			and		a.nr_seq_combinacao = nr_seq_combinacao_item_pc
			union all
			select  1
			from    pls_conta_mat_imp m,
					pls_combinacao_item_tm a
			where   m.nr_sequencia 	   = sel.nr_seq_conta_mat
			and     a.nr_seq_material  = m.nr_seq_material_conv
			and		a.nr_seq_combinacao = nr_seq_combinacao_item_pc);
	
cursor c_dt_item(	nr_id_transacao_pc			pls_oc_cta_selecao_imp.nr_id_transacao%type,
					nr_seq_combinacao_item_pc	pls_oc_cta_val_comb_item.nr_seq_combinacao_item%type) is
	select	distinct sel.nr_seq_segurado,
			sel.dt_item,
			(select max(x.nr_seq_prestador_exec_princ) from pls_conta x where x.nr_sequencia = sel.nr_seq_conta) nr_seq_prestador_exec_princ
	from	pls_oc_cta_selecao_imp sel
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and	sel.ie_valido = 'S'
	and	exists (select 	1
      			from   	pls_conta_proc_imp p,
				pls_combinacao_item_tm a
       			where   p.nr_sequencia      = sel.nr_seq_conta_proc
       			and   	a.cd_procedimento   = p.cd_procedimento_conv
       			and     a.ie_origem_proced  = p.ie_origem_proced_conv
				and	a.nr_seq_combinacao = nr_seq_combinacao_item_pc
       			union all
                        select  1
       			from    pls_conta_mat_imp m,
               			pls_combinacao_item_tm a
       			where   m.nr_sequencia 	   = sel.nr_seq_conta_mat
       			and     a.nr_seq_material  = m.nr_seq_material_conv
				and	a.nr_seq_combinacao = nr_seq_combinacao_item_pc);
			
cursor c_nr_seq_conta(	nr_id_transacao_pc		pls_oc_cta_selecao_imp.nr_id_transacao%type,
						nr_seq_combinacao_item_pc	pls_oc_cta_val_comb_item.nr_seq_combinacao_item%type) is
	select	distinct sel.nr_seq_segurado,
			sel.nr_seq_conta,
			(select max(x.nr_seq_prestador_exec_princ) from pls_conta x where x.nr_sequencia = sel.nr_seq_conta) nr_seq_prestador_exec_princ
	from	pls_oc_cta_selecao_imp sel
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and		sel.ie_valido = 'S'
	and	exists	 (select 1
      			 from   pls_conta_proc_imp p,
               			pls_combinacao_item_tm a
       			where   p.nr_sequencia      = sel.nr_seq_conta_proc
       			and   	a.cd_procedimento   = p.cd_procedimento_conv
       			and     a.ie_origem_proced  = p.ie_origem_proced_conv
				and		a.nr_seq_combinacao = nr_seq_combinacao_item_pc
       			union all
                        select  1
       			from    pls_conta_mat_imp m,
               			pls_combinacao_item_tm a
       			where   m.nr_sequencia 	   = sel.nr_seq_conta_mat
       			and     a.nr_seq_material  = m.nr_seq_material_conv
				and		a.nr_seq_combinacao = nr_seq_combinacao_item_pc);
			


-- todos os itens para futuro lancamento de ocorrencia quando processamento for por guia	
cursor c_lanc_it_guia(	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type,
			nr_seq_segurado_pc	pls_conta.nr_seq_segurado%type,
			cd_guia_referencia_pc	pls_conta.cd_guia_referencia%type,
			nr_seq_comb_item_pc	pls_combinacao_item_cta.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_selecao,
		nvl(z.ie_gera_ocorrencia, 'N') ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
		pls_conta_proc_imp b,
		pls_combinacao_item_tm z
	where	a.nr_id_transacao = nr_id_transacao_pc
	and	a.ie_valido = 'S'
	and	a.nr_seq_segurado = nr_seq_segurado_pc
	and	a.cd_guia_referencia = cd_guia_referencia_pc
	and	b.nr_sequencia = a.nr_seq_conta_proc
	and	z.nr_seq_combinacao = nr_seq_comb_item_pc
	and	z.ie_tipo_registro = 'P'
	and	z.ie_origem_proced = b.ie_origem_proced_conv
	and	z.cd_procedimento = b.cd_procedimento_conv
	union all
	-- pacotes nao existem na pls_combinacao_item_tm e sao definidos durante a consistencia da conta
	-- por isso o tratamento e feito desta maneira
	select	a.nr_sequencia nr_seq_selecao,
			'S' ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.cd_guia_referencia = cd_guia_referencia_pc
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		b.ie_tipo_despesa_conv = '4'
	and		exists (select	1
					from	pls_comb_cta_item_proc z
					where	z.nr_seq_combinacao = nr_seq_comb_item_pc
					and		z.ie_tipo_desp_proc = '4')
	union all
	select	a.nr_sequencia nr_seq_selecao,
			nvl(z.ie_gera_ocorrencia, 'N') ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_mat_imp b,
			pls_combinacao_item_tm z
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.cd_guia_referencia = cd_guia_referencia_pc
	and		b.nr_sequencia = a.nr_seq_conta_mat
	and		z.nr_seq_combinacao = nr_seq_comb_item_pc
	and		z.ie_tipo_registro = 'M'
	and		z.nr_seq_material = b.nr_seq_material_conv;
	
-- todos os itens para futuro lancamento de ocorrencia quando processamento for por data do item	
cursor c_lanc_it_data(	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type,
			nr_seq_segurado_pc	pls_conta.nr_seq_segurado%type,
			dt_item_pc		pls_oc_cta_selecao_imp.dt_item%type,
			nr_seq_comb_item_pc	pls_combinacao_item_cta.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_selecao,
			nvl(z.ie_gera_ocorrencia, 'N') ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b,
			pls_combinacao_item_tm z
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.dt_item = dt_item_pc
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		z.nr_seq_combinacao = nr_seq_comb_item_pc
	and		z.ie_tipo_registro = 'P'
	and		z.ie_origem_proced = b.ie_origem_proced_conv
	and		z.cd_procedimento = b.cd_procedimento_conv
	union all
	-- pacotes nao existem na pls_combinacao_item_tm e sao definidos durante a consistencia da conta
	-- por isso o tratamento e feito desta maneira
	select	a.nr_sequencia nr_seq_selecao,
			'S' ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.dt_item = dt_item_pc
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		b.ie_tipo_despesa_conv = '4'
	and		exists (select	1
					from	pls_comb_cta_item_proc z
					where	z.nr_seq_combinacao = nr_seq_comb_item_pc
					and		z.ie_tipo_desp_proc = '4')
	union all
	select	a.nr_sequencia nr_seq_selecao,
			nvl(z.ie_gera_ocorrencia, 'N') ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_mat_imp b,
			pls_combinacao_item_tm z
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.dt_item = dt_item_pc
	and		b.nr_sequencia = a.nr_seq_conta_mat
	and		z.nr_seq_combinacao = nr_seq_comb_item_pc
	and		z.ie_tipo_registro = 'M'
	and		z.nr_seq_material = b.nr_seq_material_conv;

	
cursor c_lanc_it_seq_conta (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type,
				nr_seq_segurado_pc	pls_conta.nr_seq_segurado%type,
				nr_seq_conta_pc		pls_oc_cta_selecao_imp.nr_seq_conta%type,
				nr_seq_comb_item_pc	pls_combinacao_item_cta.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_selecao,
			nvl(z.ie_gera_ocorrencia, 'N') ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b,
			pls_combinacao_item_tm z
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.nr_seq_conta = nr_seq_conta_pc
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		z.nr_seq_combinacao = nr_seq_comb_item_pc
	and		z.ie_tipo_registro = 'P'
	and		z.ie_origem_proced = b.ie_origem_proced_conv
	and		z.cd_procedimento = b.cd_procedimento_conv
	union all
	-- pacotes nao existem na pls_combinacao_item_tm e sao definidos durante a consistencia da conta
	-- por isso o tratamento e feito desta maneira
	select	a.nr_sequencia nr_seq_selecao,
			'S' ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_proc_imp b
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.nr_seq_conta = nr_seq_conta_pc
	and		b.nr_sequencia = a.nr_seq_conta_proc
	and		b.ie_tipo_despesa_conv = '4'
	and		exists (select	1
					from	pls_comb_cta_item_proc z
					where	z.nr_seq_combinacao = nr_seq_comb_item_pc
					and		z.ie_tipo_desp_proc = '4')
	union all
	select	a.nr_sequencia nr_seq_selecao,
			nvl(z.ie_gera_ocorrencia, 'N') ie_gera_ocorrencia
	from	pls_oc_cta_selecao_imp a,
			pls_conta_mat_imp b,
			pls_combinacao_item_tm z
	where	a.nr_id_transacao = nr_id_transacao_pc
	and		a.ie_valido = 'S'
	and		a.nr_seq_segurado = nr_seq_segurado_pc
	and		a.nr_seq_conta = nr_seq_conta_pc
	and		b.nr_sequencia = a.nr_seq_conta_mat
	and		z.nr_seq_combinacao = nr_seq_comb_item_pc
	and		z.ie_tipo_registro = 'M'
	and		z.nr_seq_material = b.nr_seq_material_conv;
	 
begin	
	
-- Deve exisitr informacao da regra para executar a validacao
if	(nr_seq_combinada_p is not null) and
	(nr_id_transacao_p is not null)  then
	
	
	--Apenas para passar de parametro para a pls_simult_concor_pck.obter_obs_val_32 pois apenas o campo ie_evento e utilizado, entao crio essa estrutura vazia apenas
	--com a informacao de evento de importacao, de modo a nao precisar reescrever algumas rotinas auxilizares naquela package.
	dados_regra_w.nr_sequencia				:= null;
	dados_regra_w.nr_seq_ocorrencia			:= null;
	dados_regra_w.cd_estabelecimento		:= null;
	dados_regra_w.ie_validacao				:= null;
	dados_regra_w.ie_evento					:= 'IMP';
	dados_regra_w.ie_portal_web				:= null;
	dados_regra_w.ie_primeira_verificacao	:= null;
	dados_regra_w.ie_utiliza_filtro			:= null;
	dados_regra_w.ie_aplicacao_ocorrencia	:= null;
	dados_regra_w.dt_inicio_vigencia		:= null;
	dados_regra_w.dt_fim_vigencia			:= null;
	dados_regra_w.ie_excecao				:= null;
	dados_regra_w.ie_incidencia_selecao		:= null;
	dados_regra_w.ie_conta_sem_item			:= null;
		
	-- #to do: necessario ajustar rotina de simultaneo e concorrente para que os itens que estao sendo importados(e portanto nas novas tabelas de importacao) sejam considerados
	
	-- usa uma tabela para armazenar as combinacoes de itens.
	-- no futuro isso deve ser feito atraves de uma view materializada, nao foi feito ainda por motivo de nao existir suporte para este tipo de objetos no TASY
	pls_gerencia_upd_obj_pck.atualizar_objetos(wheb_usuario_pck.get_nm_usuario, 'PLS_OC_CTA_TRATAR_VAL_32', 'PLS_COMBINACAO_ITEM_TM');
	
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null);
	
	--limpa as variaveis
	pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
		
	-- combinacoes que serao processadas por essa ocorrencia
	for	r_c_somente_regra_w in c_somente_regra (nr_seq_combinada_p) loop
		
		-- retorna todos os procedimentos que deverao ficar/continuar validos
		open c_reg_existe_proc(	nr_id_transacao_p, r_c_somente_regra_w.nr_seq_combinacao_item);
		loop
			fetch c_reg_existe_proc bulk collect into 	tb_seq_selecao_w, tb_valido_w, tb_observacao_w
			limit pls_util_pck.qt_registro_transacao_w;
			exit when tb_seq_selecao_w.count = 0;
										
			--Grava o que restar nas variaveis na tabela
			pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
									tb_observacao_w, nr_id_transacao_p, 
									'SEQ');
			--limpa as variaveis	
			pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
								tb_valido_w,
								tb_observacao_w);	
								
		end loop;
		close c_reg_existe_proc;
		
		-- retorna todos os materiais que deverao ficar/continuar validos
		open c_reg_existe_mat(	nr_id_transacao_p, r_c_somente_regra_w.nr_seq_combinacao_item);
		loop
			fetch c_reg_existe_mat bulk collect into 	tb_seq_selecao_w, tb_valido_w, tb_observacao_w
			limit pls_util_pck.qt_registro_transacao_w;
			exit when tb_seq_selecao_w.count = 0;
			
			--Grava o que restar nas variaveis na tabela
			pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
									tb_observacao_w, nr_id_transacao_p, 
									'SEQ');
			--limpa as variaveis	
			pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
								tb_valido_w,
								tb_observacao_w);	
		end loop;
		close c_reg_existe_mat;
	
	end loop;
	
	-- seta os registros que serao validos ou invalidos apos o processamento 
	-- seta os registros que serao validos ou invalidos apos o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', 
						ie_regra_excecao_p, null,
						nr_id_transacao_p, null);
	
	-- fim do processo que invalida todos os registros que nao tem regra de combinacao de itens
	------------------------------------------------------------------------------------------------------------------------------------------
	
	-- daqui em diante processa as regras normalmente para decidir se gera ou nao ocorrencia 
	-- nos registros que sobraram (se sobraram registros validos)
	select	count(1)
	into	qt_registro_valido_w
	from	pls_oc_cta_selecao_imp a
	where	a.nr_id_transacao = nr_id_transacao_p
	and		a.ie_valido = 'S';
	
	-- se ainda existem registros validos processa as regras normalmente
	if	(qt_registro_valido_w > 0) then
	
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		-- e feito novamente para reiniciar o "ciclo" da validacao e marcar corretamente o que vai ser processado novamente
		pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null);
		
		-- Buscar os dados da regra de validacao conforme montado pelo usuario.
		for	r_c_regra_comb_w in c_regra_comb (nr_seq_combinada_p) loop
			
			-- se tiver alguma regra cadastrada processa
			if	(r_c_regra_comb_w.qt_regra_proc > 0 or r_c_regra_comb_w.qt_regra_mat > 0) then
			
				dados_cab_regra_w.nr_sequencia			:= r_c_regra_comb_w.nr_seq_combinada;
				dados_cab_regra_w.dt_inicio_vigencia	:= r_c_regra_comb_w.dt_inicio_vigencia;
				dados_cab_regra_w.dt_fim_vigencia		:= r_c_regra_comb_w.dt_fim_vigencia;
				dados_cab_regra_w.qt_dias_considerar	:= r_c_regra_comb_w.qt_dias_considerar;
				dados_cab_regra_w.ie_conta_igual_comb	:= r_c_regra_comb_w.ie_conta_igual_comb;
				dados_cab_regra_w.ie_ocorrencia_conta	:= r_c_regra_comb_w.ie_ocorrencia_conta;
				dados_cab_regra_w.ie_ou_excecao			:= r_c_regra_comb_w.ie_ou_excecao;
				dados_cab_regra_w.qt_regra_proc			:= r_c_regra_comb_w.qt_regra_proc + r_c_regra_comb_w.qt_regra_pacote ;
				dados_cab_regra_w.qt_regra_mat			:= r_c_regra_comb_w.qt_regra_mat;
				dados_cab_regra_w.qt_regra_proc_and		:= r_c_regra_comb_w.qt_regra_proc_and;
				dados_cab_regra_w.qt_regra_proc_or		:= r_c_regra_comb_w.qt_regra_proc_or;
				dados_cab_regra_w.qt_regra_mat_and		:= r_c_regra_comb_w.qt_regra_mat_and;
				dados_cab_regra_w.qt_regra_mat_or		:= r_c_regra_comb_w.qt_regra_mat_or;
				dados_cab_regra_w.ie_tipo_verificacao	:= r_c_regra_comb_w.ie_tipo_verificacao;
				nr_seq_combinacao_item_w				:= r_c_regra_comb_w.nr_seq_combinacao_item;

				
				-- por atendimento considera o segurado e a guia
				if	(dados_cab_regra_w.ie_tipo_verificacao = '1') then
					open c_guia(nr_id_transacao_p, nr_seq_combinacao_item_w);
				elsif	(dados_cab_regra_w.ie_tipo_verificacao = '2') then
					--  busca por segurado e data do item
					open c_dt_item(nr_id_transacao_p, nr_seq_combinacao_item_w);
				else
					-- busca por segurado e conta  
					open c_nr_seq_conta(nr_id_transacao_p, nr_seq_combinacao_item_w);
				end if;
				
				loop
					-- faz o loop de acordo com o tipo de validacao
					if	(dados_cab_regra_w.ie_tipo_verificacao = '1') then
					
						fetch c_guia into nr_seq_segurado_w, cd_guia_referencia_w, nr_seq_prestador_exec_princ_w;
						exit when c_guia%notfound;
						
					elsif	(dados_cab_regra_w.ie_tipo_verificacao = '2') then
					
						fetch c_dt_item into nr_seq_segurado_w, dt_item_w, nr_seq_prestador_exec_princ_w;
						exit when c_dt_item%notfound;
					else
						fetch c_nr_seq_conta into nr_seq_segurado_w, nr_seq_conta_w, nr_seq_prestador_exec_princ_w;
						exit when c_nr_seq_conta%notfound;
					end if;
					
					-- busca o select que trara os dados dos itens que precisam ser analisados

					
				ds_sql_w := pls_simult_concor_pck.obter_select_val_32_nova_imp(	dados_cab_regra_w,
												dados_regra_w,
												nr_seq_segurado_w,
												cd_guia_referencia_w,
												dt_item_w,
												nr_seq_conta_w,
												nr_seq_prestador_exec_princ_w,
												nr_id_transacao_p,
												bind_sql_w);
												
					
					-- inicializa as variaveis que conterao os totais
					qt_tot_casou_proc_and_w	:= 0;
					qt_tot_casou_proc_ou_w	:= 0;
					qt_tot_casou_proc_pacote_and_w := 0;
					qt_tot_casou_proc_pacote_or_w := 0;
					qt_tot_casou_mat_and_w	:= 0;
					qt_tot_casou_mat_ou_w	:= 0;
					qt_tot_item_guia_proc_w := 0;
					qt_tot_item_guia_mat_w := 0;
					qt_tot_agrup_proc_and_w := 0;
					qt_tot_agrup_mat_and_w := 0;
					
					-- cursor que le as quantidades de itens para guias ou datas
					-- sao armazenados dados em variaveis e mais abaixo e feito o processamento para decidir se lanca ou nao a ocorrencia
					cursor_w := sql_pck.executa_sql_cursor(	ds_sql_w, bind_sql_w);
					loop
						fetch cursor_w 
						into 	ie_tipo_registro_w, qt_casou_and_w, qt_casou_ou_w,
							qt_casou_pacote_and_w, qt_casou_pacote_ou_w,
							qt_agrup_item_w, dt_item_ini_w, dt_item_fim_w,
							qt_total_item_guia_w;
						exit when cursor_w%notfound;
						
						ie_atendeu_regra_w := true;
						
						-- se foi informada a diferenca entre datas so faz a verificacao caso a validacao seja por guia
						-- pois se for por data todas as datas serao iguais e nao fara sentido algum
						if	(dados_cab_regra_w.qt_dias_considerar > 0 and dados_cab_regra_w.ie_tipo_verificacao = '1') then
						
							qt_dif_dias_w	:= round(dt_item_fim_w - dt_item_ini_w);
			
							-- Se a quantidade de dias nao se enquadrar, nao pode aplicar a regra 
							if	(qt_dif_dias_w > dados_cab_regra_w.qt_dias_considerar) then
								ie_atendeu_regra_w := false;
							end if;
						end if;
						
						-- os totais existentes devem ser alimentados sempre, independente de ter atendido ou nao a regra
						-- pois se a regra nao foi atendida, o total deve ser armazenado, porque ele nao obedece as regras
						if	(ie_tipo_registro_w = 'P') then
							qt_tot_item_guia_proc_w := qt_tot_item_guia_proc_w + qt_total_item_guia_w;
							
						elsif	(ie_tipo_registro_w = 'M') then
							qt_tot_item_guia_mat_w := qt_tot_item_guia_mat_w + qt_total_item_guia_w;
						end if;
						
						-- se a regra de data foi atendida armazena os totalizadores
						if	(ie_atendeu_regra_w) then
							
							if	(ie_tipo_registro_w = 'P') then
								
								qt_tot_casou_proc_and_w := qt_tot_casou_proc_and_w + qt_casou_and_w;
								qt_tot_casou_proc_ou_w := qt_tot_casou_proc_ou_w + qt_casou_ou_w;
								qt_tot_agrup_proc_and_w := qt_tot_agrup_proc_and_w + qt_agrup_item_w;
								qt_tot_casou_proc_pacote_and_w := qt_tot_casou_proc_pacote_and_w + qt_casou_pacote_and_w;
								qt_tot_casou_proc_pacote_or_w := qt_tot_casou_proc_pacote_or_w + qt_casou_pacote_ou_w;

							elsif	(ie_tipo_registro_w = 'M') then
								
								qt_tot_casou_mat_and_w := qt_tot_casou_mat_and_w + qt_casou_and_w;
								qt_tot_casou_mat_ou_w := qt_tot_casou_mat_ou_w + qt_casou_ou_w;
								qt_tot_agrup_mat_and_w := qt_tot_agrup_mat_and_w + qt_agrup_item_w;
							end if;
						end if;
					end loop;
					close cursor_w;

					ie_proc_e_w := 'N';
					ie_mat_e_w := 'N';
					ie_proc_ou_w := 'N';
					ie_mat_ou_w := 'N';
					
					-- regra AND procedimento
					if	(dados_cab_regra_w.ie_conta_igual_comb = 'S') then

						-- se for para regra AND considerar tudo o que esta na regra e somente o que esta na regra
						-- verifica somente se todos os itens da regra existem na guia e 
						-- se nao existem outros itens na guia
						-- nesse caso a quantidade de itens da guia deve ser igual a que casou
						if	((qt_tot_casou_proc_and_w = r_c_regra_comb_w.qt_regra_proc_and) and
							(qt_tot_casou_proc_and_w = qt_tot_item_guia_proc_w)) then
							
							ie_proc_e_w := 'S';
							
						-- caso os totais dos grupos sejam iguais tambem valida
						-- resolve a situacao de regras em branco ou cadastro por grupos que acabam tendo varios materiais na tabela TM
						-- e nem todos precisam ter o casamento feito, apenas todos os materiais existentes na conta devem existir na tabela TM
						-- e o que casou deve ser igual ao que existe na guia
						elsif	((qt_tot_agrup_proc_and_w = r_c_regra_comb_w.qt_grupos_distintos_proc) and 
							 (qt_tot_casou_proc_and_w = qt_tot_item_guia_proc_w))then
					
							ie_proc_e_w := 'S';
						end if;
					-- verifica somente se todos os itens da regra existem na guia
					elsif	(qt_tot_casou_proc_and_w = r_c_regra_comb_w.qt_regra_proc_and) then
						 
						ie_proc_e_w := 'S';
					-- Para o caso de grupos na regra. sera valido quando os grupos que tiveram casamento forem iguais ao total da regra.	
					-- resolve a situacao de regras em branco ou cadastro por grupos que acabam tendo varios materiais na tabela TM
					-- e nem todos precisam ter o casamento feito, apenas todos os procedimentos existentes na conta devem existir na tabela TM
					elsif	(qt_tot_agrup_proc_and_w = r_c_regra_comb_w.qt_grupos_distintos_proc) then				
					
						ie_proc_e_w := 'S';
					else
						ie_proc_e_w := 'N';
					end if;
					
					-- se tiver regra para pacote precisa existir pelo menos um pacote
					-- em toda a selecao de guia ou data
					if	(r_c_regra_comb_w.qt_regra_pacote_and > 0) then
						-- se nao tiver nenhuma outra regra, apenas precisa ter pelo menos um pacote
						if	(r_c_regra_comb_w.qt_regra_proc_and = 0) then
							-- pelo menos um pacote
							if	(qt_tot_casou_proc_pacote_and_w > 0) then
								ie_proc_e_w := 'S';
							else
								ie_proc_e_w := 'N';
							end if;
						else
							if	(ie_proc_e_w = 'S' and qt_tot_casou_proc_pacote_and_w > 0) then
								ie_proc_e_w := 'S';
							else
								ie_proc_e_w := 'N';
							end if;
						end if;
					end if;
					
					-- regra AND material
					if	(dados_cab_regra_w.ie_conta_igual_comb = 'S') then

						-- se for para regra AND considerar tudo o que esta na regra e somente o que esta na regra
						-- verifica somente se todos os itens da regra existem na guia e 
						-- se nao existem outros itens na guia
						if	((qt_tot_casou_mat_and_w = r_c_regra_comb_w.qt_regra_mat_and) and
							(qt_tot_casou_mat_and_w = qt_tot_item_guia_mat_w)) then
							
							ie_mat_e_w := 'S';
							
						-- caso os totais dos grupos sejam iguais valida
						-- resolve a situacao de regras em branco ou cadastro por grupos que acabam tendo varios materiais na tabela TM
						-- e nem todos precisam ter o casamento feito, apenas todos os materiais existentes na conta devem existir na tabela TM
						-- e o que casou deve ser igual ao que existe na guia
						elsif	((qt_tot_agrup_mat_and_w = r_c_regra_comb_w.qt_grupos_distintos_mat) and
							 (qt_tot_casou_mat_and_w = qt_tot_item_guia_mat_w)) then
					
							ie_mat_e_w := 'S';
						end if;
					-- verifica somente se todos os itens da regra existem na guia ou
					-- se todos os itens da guia foram casados com itens (previnir regra em branco)
					elsif	(qt_tot_casou_mat_and_w = r_c_regra_comb_w.qt_regra_mat_and) then
						 
						ie_mat_e_w := 'S';
						
					-- Para o caso de grupos na regra. sera valido quando os grupos que tiveram casamento forem iguais ao total da regra.	
					-- resolve a situacao de regras em branco ou cadastro por grupos que acabam tendo varios materiais na tabela TM
					-- e nem todos precisam ter o casamento feito, apenas todos os materiais existentes na conta devem existir na tabela TM
					elsif	(qt_tot_agrup_mat_and_w = r_c_regra_comb_w.qt_grupos_distintos_mat) then				
					
						ie_mat_e_w := 'S';
					else
						ie_mat_e_w := 'N';
					end if;
					
					-- regra OU procedimento e material
					-- nao existe processamento de regra OU se o ie_conta_igual_comb = S
					if	(dados_cab_regra_w.ie_conta_igual_comb = 'N') then
						
						-- regra OU procedimento
						-- se casou alguma coisa ou se nao tem regra cadastrada
						if	(qt_tot_casou_proc_ou_w > 0 or r_c_regra_comb_w.qt_regra_proc_or = 0) then
							ie_proc_ou_w := 'S';
						else
							ie_proc_ou_w := 'N';
						end if;
						
						-- regra OU material
						-- se casou alguma coisa ou se nao tem regra cadastrada
						if	(qt_tot_casou_mat_ou_w > 0 or r_c_regra_comb_w.qt_regra_mat_or = 0) then
							ie_mat_ou_w := 'S';
						else
							ie_mat_ou_w := 'N';
						end if;
						
						-- se tiver regra para pacote precisa existir pelo menos um pacote
						-- em toda a selecao de guia ou data
						if	(r_c_regra_comb_w.qt_regra_pacote_or > 0) then
							-- se nao tiver nenhuma outra regra, apenas precisa ter pelo menos um pacote
							if	(r_c_regra_comb_w.qt_regra_proc_or = 0) then
								-- pelo menos um pacote
								if	(qt_tot_casou_proc_pacote_or_w > 0) then
									ie_proc_ou_w := 'S';
								else
									ie_proc_ou_w := 'N';
								end if;
							else
								if	(ie_proc_ou_w = 'S' and qt_tot_casou_proc_pacote_or_w > 0) then
									ie_proc_ou_w := 'S';
								else
									ie_proc_ou_w := 'N';
								end if;
							end if;
						end if;
						
						-- se alguma situacao acima for verdade
						-- feito isso para previnir os casos em que exista uma regra OU cadastrada, por exemplo, para procedimento
						-- e a mesma nao se encaixe e nao tenha nada para material. 
						-- acima, quando nao existe regra para item diz que a regra e valida, 
						-- entretanto como e um OU se a que tem nao casar entao nada pode ser valido
						if	(ie_proc_ou_w = 'S' or ie_mat_ou_w = 'S') then
						
							-- se o procedimento tem S porque nao tinha regra cadastrada
							if	(ie_proc_ou_w = 'S' and r_c_regra_comb_w.qt_regra_proc_or = 0) then
								-- caso o ie_mat for N, esse vira N tambem
								if	(ie_mat_ou_w = 'N') then
									ie_proc_ou_w := 'N';
								end if;
							end if;
							
							-- se o material tem S porque nao tinha regra cadastrada
							if	(ie_mat_ou_w = 'S' and r_c_regra_comb_w.qt_regra_mat_or = 0) then
								-- caso o ie_proc for N, esse vira N tambem
								if	(ie_proc_ou_w = 'N') then
									ie_mat_ou_w := 'N';
								end if;
							end if;
						end if;
					
					else
						ie_proc_ou_w := 'S';
						ie_mat_ou_w := 'S';
					end if;
					
					-- se regra ou for considerada excecao
					if	(dados_cab_regra_w.ie_ou_excecao = 'S') then
						
						-- se casou alguma coisa nao gera nada
						if	(qt_tot_casou_proc_ou_w > 0 or qt_tot_casou_mat_ou_w > 0 or
							(r_c_regra_comb_w.qt_regra_pacote_or > 0 and qt_tot_casou_proc_pacote_or_w > 0)) then
							ie_proc_e_w := 'N';
							ie_mat_e_w := 'N';
							ie_proc_ou_w := 'N';
							ie_mat_ou_w := 'N';
						end if;
					
					end if;
					
					

					-- manda para o banco se casou
					-- regra AND deve casar para proc e mat e regra OU apenas um dois dois casando ja atende
					-- se nao casar nenhuma regra OU e o checkbox de regra OU ser excecao estiver marcado, gera ocorrencia
					if	((ie_proc_e_w = 'S' and ie_mat_e_w = 'S') and
						(
						(ie_proc_ou_w = 'S' or ie_mat_ou_w = 'S') or 
						 (ie_proc_ou_w = 'N' and ie_mat_ou_w = 'N' and dados_cab_regra_w.ie_ou_excecao = 'S'))
						) then
						
						
						-- #lrp - adaptar pls_simult_concor_pck.obter_obs_val_32 para nova importacao de xml
						ds_observacao_w := substr(pls_simult_concor_pck.obter_obs_val_32_nova_imp(	dados_cab_regra_w,
															cd_guia_referencia_w,
															nr_seq_segurado_w,
															dt_item_w,
															nr_seq_conta_w,
															'IMP'), 1, 2000);
						-- se entrou aqui significa que a ocorrencia deve ser lancada.
						-- entao, busca-se tudo o que esta na tabela de selecao e na estrutura de combinacao e lanca ocorrencia

						-- por atendimento considera o segurado e a guia
						if	(dados_cab_regra_w.ie_tipo_verificacao = '1') then
							open c_lanc_it_guia(	nr_id_transacao_p, nr_seq_segurado_w,
										cd_guia_referencia_w, dados_cab_regra_w.nr_sequencia);
						elsif	(dados_cab_regra_w.ie_tipo_verificacao = '2') then
							--  busca por segurado e data do item
							open c_lanc_it_data(	nr_id_transacao_p, nr_seq_segurado_w,
										dt_item_w, dados_cab_regra_w.nr_sequencia);
						else
							--busca por segurado e sequencia da conta
							open c_lanc_it_seq_conta(	nr_id_transacao_p, nr_seq_segurado_w,
											nr_seq_conta_w, dados_cab_regra_w.nr_sequencia);
							
						end if;
						
						nr_indice_w := 0;
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
						loop
							-- abre o cursor de acordo com o tipo de validacao
							if	(dados_cab_regra_w.ie_tipo_verificacao = '1') then
							
								fetch c_lanc_it_guia into nr_seq_selecao_w, ie_gera_ocor_item_w;
								exit when c_lanc_it_guia%notfound;
								
							elsif	(dados_cab_regra_w.ie_tipo_verificacao = '2') then
							
								fetch c_lanc_it_data into nr_seq_selecao_w, ie_gera_ocor_item_w;
								exit when c_lanc_it_data%notfound;
								
							else
								
								fetch c_lanc_it_seq_conta into nr_seq_selecao_w, ie_gera_ocor_item_w;
								exit when c_lanc_it_seq_conta%notfound;
								
							end if;
							
							-- se for para gerar ocorrencia no item ou na conta, sinaliza para gerar
							if	(ie_gera_ocor_item_w = 'S' or dados_cab_regra_w.ie_ocorrencia_conta = 'S') then
								
								tb_valido_w(nr_indice_w) := 'S';
								tb_seq_selecao_w(nr_indice_w) := nr_seq_selecao_w;
								tb_observacao_w(nr_indice_w) := ds_observacao_w ;
							
								if	(nr_indice_w >= pls_cta_consistir_pck.qt_registro_transacao_w) then
									pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
																					tb_observacao_w, nr_id_transacao_p, 
																					'SEQ');
									
									pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
									nr_indice_w := 0;
								else
									nr_indice_w := nr_indice_w + 1;
								end if;
							end if;
						end loop;

						-- se sobrou alguma coisa manda para o banco
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
																		tb_observacao_w, nr_id_transacao_p, 
																		'SEQ');
						
						-- fecha o cursor de acordo com o tipo de validacao
						if	(dados_cab_regra_w.ie_tipo_verificacao = '1') then
							close c_lanc_it_guia;
						elsif	(dados_cab_regra_w.ie_tipo_verificacao = '2') then
							close c_lanc_it_data;
						else
							close c_lanc_it_seq_conta;
						end if;
						
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
					end if;

				end loop;
				
				-- faz o fechamento do cursor de acordo com o tipo de validacao
				if	(dados_cab_regra_w.ie_tipo_verificacao = '1') then
					close c_guia;
				elsif	(dados_cab_regra_w.ie_tipo_verificacao = '2') then
					close c_dt_item;
				else
					close c_nr_seq_conta;
				end if;
			end if;
		end loop;
	end if;
	
		--Grava o que restar nas variaveis na tabela
	pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
							tb_observacao_w, nr_id_transacao_p, 
							'SEQ');
	-- seta os registros que serao validos ou invalidos apos o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', 
						ie_regra_excecao_p, null,
						nr_id_transacao_p, null);
						
end if;
	
end pls_oc_cta_tratar_val_32_imp;
/
