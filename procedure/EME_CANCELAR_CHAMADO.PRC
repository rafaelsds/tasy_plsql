create or replace
procedure eme_cancelar_chamado(
			nr_seq_chamado_p	Number,
			nr_seq_mot_cancel_p	Number,
			nm_usuario_p		Varchar2) is 

begin

update	eme_chamado
set		nr_seq_mot_cancel	= nr_seq_mot_cancel_p,
		dt_cancel_chamado	= sysdate,
		nm_usuario_cancel	= nm_usuario_p
where	nr_sequencia		= nr_seq_chamado_p;

commit;

end eme_cancelar_chamado;
/