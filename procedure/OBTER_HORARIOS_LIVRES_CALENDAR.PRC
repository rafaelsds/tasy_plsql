create or replace
procedure Obter_Horarios_Livres_calendar( cd_agenda_p     	   number,
                                          dt_referencia_p		date) is
				
ds_horarios_w		   varchar2(4000);
dt_inicial_w         date;
dt_final_w           date;
qt_livres_antigos_w  number(10);
qt_registros_w       number(10);

begin
dt_inicial_w	:= PKG_DATE_UTILS.start_of(dt_referencia_p,'month',0);
dt_final_w	   := pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_referencia_p, 'MONTH', 0), dt_referencia_p);

WHILE (dt_inicial_w <= TRUNC(dt_final_w,'dd')) LOOP
   BEGIN
   select   sum(decode(ie_status_agenda,'L',1,0)),
            count(*)
   into     qt_livres_antigos_w,
            qt_registros_w
   from     agenda_paciente
   where    trunc(dt_agenda) =  trunc(dt_inicial_w)
   and      cd_agenda = cd_agenda_p
   and      obter_tipo_agenda(cd_agenda) = 1;
   
   if (((trunc(dt_inicial_w) < trunc(sysdate)) and (qt_livres_antigos_w > 0)) or
      ((trunc(dt_inicial_w) >= trunc(sysdate)) and (qt_registros_w = 0))) then
      obter_horarios_livres_cirurgia(	wheb_usuario_pck.get_cd_estabelecimento,
                                       cd_agenda_p,
                                       dt_inicial_w,
                                       dt_inicial_w,
                                       0,
                                       null,
                                       null,
                                       null,
                                       wheb_usuario_pck.get_nm_usuario,
                                       'S',
                                       ds_horarios_w,
                                       'N',
                                       null);
   end if;                                       
   dt_inicial_w := dt_inicial_w + 1;                                    
   END;
END LOOP;
   
end Obter_Horarios_Livres_calendar;
/