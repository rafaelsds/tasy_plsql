create or replace
procedure Gerar_desdobramento_item_audit(nr_sequencia_p		Number,
					qt_item_p		number,
					ie_proc_mat_p		varchar2,
					nr_seq_motivo_audit_p	Number,
					nm_usuario_p		Varchar2) is 
	
nr_seq_gerada_w		Number(10,0);
nr_seq_item_w		Number(10,0);
qt_diferenca_w		Number(15,2):=0;
qt_original_w		Number(15,2):=0;
cd_convenio_w		Number(5,0);
cd_local_estoque_w		Number(4,0);
dt_atualizacao_estoque_w	Date;
	
begin

if	(ie_proc_mat_p = 'M') then

	select 	max(nr_seq_matpaci)
	into	nr_seq_item_w
	from 	auditoria_matpaci
	where 	nr_sequencia = nr_sequencia_p;

	Duplicar_mat_paciente(nr_seq_item_w, nm_usuario_p, 'N', nr_seq_gerada_w);
	
	select 	nvl(max(qt_material),0),
		nvl(max(cd_convenio),0),
		max(cd_local_estoque),
		max(dt_atualizacao_estoque)
	into	qt_original_w,
		cd_convenio_w,
		cd_local_estoque_w,
		dt_atualizacao_estoque_w
	from 	material_atend_paciente
	where 	nr_sequencia = nr_seq_item_w;
	
	qt_diferenca_w:=  qt_original_w - qt_item_p;
	
	update	material_atend_paciente
	set 	qt_material = qt_item_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,		
		ds_observacao = Wheb_mensagem_pck.get_Texto(305949) /*'Desdobramento Item -> Auditoria Conta Paciente'*/
	where 	nr_sequencia = nr_seq_item_w;
	
	atualiza_preco_material(nr_seq_item_w, nm_usuario_p);
	
	update	material_atend_paciente
	set 	qt_material = qt_diferenca_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_auditoria = 'S',
		nr_seq_mat_desdob = nr_seq_item_w,
		cd_local_estoque = decode(cd_local_estoque_w, null, cd_local_estoque, cd_local_estoque_w),
		dt_atualizacao_estoque = decode(cd_local_estoque_w, null, dt_atualizacao_estoque, dt_atualizacao_estoque_w),
		ds_observacao = Wheb_mensagem_pck.get_Texto(305949) /*'Desdobramento Item -> Auditoria Conta Paciente'*/
	where 	nr_sequencia = nr_seq_gerada_w;
	
	atualiza_preco_material(nr_seq_gerada_w, nm_usuario_p);
	
	commit;
	
	Atualizar_Lista_Itens_Audit(null, nr_seq_gerada_w, 1, qt_diferenca_w, nm_usuario_p, nr_seq_motivo_audit_p, null, 'N');
	
	update  auditoria_matpaci
	set 	qt_original = qt_item_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
else

	select 	max(nr_seq_propaci)
	into	nr_seq_item_w
	from 	auditoria_propaci
	where 	nr_sequencia = nr_sequencia_p;

	Duplicar_proc_paciente(nr_seq_item_w, nm_usuario_p, nr_seq_gerada_w);
	
	select 	nvl(max(qt_procedimento),0),
		nvl(max(cd_convenio),0)
	into	qt_original_w,
		cd_convenio_w
	from 	procedimento_paciente
	where 	nr_sequencia = nr_seq_item_w;
	
	qt_diferenca_w:=  qt_original_w - qt_item_p;
	
	update	procedimento_paciente
	set 	qt_procedimento = qt_item_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ds_observacao = Wheb_mensagem_pck.get_Texto(305949) /*'Desdobramento Item -> Auditoria Conta Paciente'*/
	where 	nr_sequencia = nr_seq_item_w;
	
	atualiza_preco_procedimento(nr_seq_item_w, cd_convenio_w, nm_usuario_p);
	
	update	procedimento_paciente
	set 	qt_procedimento = qt_diferenca_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_auditoria = 'S',
		ds_observacao = Wheb_mensagem_pck.get_Texto(305949) /*'Desdobramento Item -> Auditoria Conta Paciente'*/
	where 	nr_sequencia = nr_seq_gerada_w;
	
	atualiza_preco_procedimento(nr_seq_gerada_w, cd_convenio_w, nm_usuario_p);
	
	commit;
	
	Atualizar_Lista_Itens_Audit(null, nr_seq_gerada_w, 2, qt_diferenca_w, nm_usuario_p, nr_seq_motivo_audit_p, null, 'N');
	
	update  auditoria_propaci
	set 	qt_original = qt_item_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end Gerar_desdobramento_item_audit;
/
