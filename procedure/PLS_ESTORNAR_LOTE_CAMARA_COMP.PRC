create or replace
procedure pls_estornar_lote_camara_comp	(	nr_seq_lote_p			pls_lote_camara_comp.nr_sequencia%type,
						dt_estorno_p			date,
						nm_usuario_p			varchar2,
						cd_estabelecimento_p		number) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Estonar a procedure PLS_BAIXAR_LOTE_CAMARA_COMP
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

nr_titulo_receber_w		titulo_receber.nr_titulo%type;
nr_titulo_pagar_w		pls_lote_camara_comp.nr_tit_pagar_taxa%type;
nr_seq_baixa_w			titulo_pagar_baixa.nr_sequencia%type;
nr_seq_tit_lote_cam_w		pls_titulo_lote_camara.nr_sequencia%type;
vl_titulo_w			titulo_receber.vl_titulo%type;
vl_saldo_titulo_w		titulo_receber.vl_saldo_titulo%type;

Cursor C01 (nr_seq_lote_pc	pls_lote_camara_comp.nr_sequencia%type)is
	select	a.nr_titulo_receber,
		a.nr_titulo_pagar,
		a.nr_sequencia nr_seq_tit_lote_cam
	from	pls_titulo_lote_camara a
	where	a.nr_seq_lote_camara	= nr_seq_lote_pc;

begin

-- CANCELAR OS T�TULOS GERADOS
select	max(nr_tit_pagar_taxa)
into	nr_titulo_pagar_w
from	pls_lote_camara_comp
where	nr_sequencia = nr_seq_lote_p;

if	(nr_titulo_pagar_w is not null) then
	delete	pls_titulo_lote_camara
	where	nr_titulo_pagar	= nr_titulo_pagar_w;
	
	select	max(nr_sequencia)
	into	nr_seq_baixa_w
	from	titulo_pagar_baixa
	where	nr_titulo		= nr_titulo_pagar_w;
	
	if	(nr_seq_baixa_w is not null) then
		estornar_tit_pagar_baixa(nr_titulo_pagar_w,nr_seq_baixa_w,dt_estorno_p,nm_usuario_p,'N');
	end if;

	cancelar_titulo_pagar(nr_titulo_pagar_w,nm_usuario_p,sysdate);
end if;

select	max(nr_titulo)
into	nr_titulo_pagar_w
from	titulo_pagar
where	nr_seq_pls_lote_camara = nr_seq_lote_p;

if	(nr_titulo_pagar_w is not null) then
	select	max(nr_sequencia)
	into	nr_seq_baixa_w
	from	titulo_pagar_baixa
	where	nr_titulo		= nr_titulo_pagar_w;
	
	if	(nr_seq_baixa_w is not null) then
		estornar_tit_pagar_baixa(nr_titulo_pagar_w,nr_seq_baixa_w,dt_estorno_p,nm_usuario_p,'N');
	end if;

	cancelar_titulo_pagar(nr_titulo_pagar_w,nm_usuario_p,sysdate);
end if;

select	max(nr_titulo)
into	nr_titulo_receber_w
from	titulo_receber
where	nr_seq_pls_lote_camara = nr_seq_lote_p;

if	(nr_titulo_receber_w is not null) then
	select	nvl(max(vl_titulo), 0),
		nvl(max(vl_saldo_titulo), 0)
	into	vl_titulo_w,
		vl_saldo_titulo_w
	from	titulo_receber
	where	nr_titulo = nr_titulo_receber_w;
	
	if	(vl_titulo_w <> vl_saldo_titulo_w) then
		select	max(nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_receber_liq
		where	nr_titulo = nr_titulo_receber_w;
		
		if	(nr_seq_baixa_w is not null) then
			estornar_tit_receber_liq(nr_titulo_receber_w,nr_seq_baixa_w,dt_estorno_p,nm_usuario_p,'N',null,'P');
		end if;
	end if;
	
	cancelar_titulo_receber(nr_titulo_receber_w,nm_usuario_p,'N',sysdate);
end if;

-- ESTORNAR OS T�TULOS
for r_C01_w in C01 (nr_seq_lote_p) loop
	begin
	if	(r_C01_w.nr_titulo_receber is not null) then
		select	max(nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_receber_liq
		where	nr_titulo		= r_C01_w.nr_titulo_receber
		and	nr_seq_pls_lote_camara	= nr_seq_lote_p;
		
		if	(nr_seq_baixa_w is not null) then
			dbms_application_info.SET_ACTION('PLS_ESTORNAR_LOTE_CAMARA_COMP');
			estornar_tit_receber_liq(r_C01_w.nr_titulo_receber,nr_seq_baixa_w,dt_estorno_p,nm_usuario_p,'N',null,'P');
			
			update	pls_titulo_lote_camara
			set	vl_baixado	= 0
			where	nr_sequencia	= r_C01_w.nr_seq_tit_lote_cam;
		end if;
	elsif	(r_C01_w.nr_titulo_pagar is not null) then
		select	max(nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_pagar_baixa
		where	nr_titulo		= r_C01_w.nr_titulo_pagar
		and	nr_seq_pls_lote_camara	= nr_seq_lote_p;
		
		if	(nr_seq_baixa_w is not null) then
			dbms_application_info.SET_ACTION('PLS_ESTORNAR_LOTE_CAMARA_COMP');
			estornar_tit_pagar_baixa(r_C01_w.nr_titulo_pagar,nr_seq_baixa_w,dt_estorno_p,nm_usuario_p,'S');
			
			update	pls_titulo_lote_camara
			set	vl_baixado	= 0
			where	nr_sequencia	= r_C01_w.nr_seq_tit_lote_cam;
		end if;
	end if;
	end;
end loop;

-- ATUALIZAR AS INFORMA��ES DO LOTE
update	pls_lote_camara_comp
set	nr_tit_pagar_taxa	= null,
	dt_baixa		= null,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_lote_p;

commit;

end pls_estornar_lote_camara_comp;
/