create or replace 
procedure gerar_evento_isolamento(	nr_atendimento_p	number,
					nm_usuario_p		varchar2) is

nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w			number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and	ie_evento_disp		= 'ISO'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)	
	and	nvl(ie_situacao,'A') = 'A';
	
pragma autonomous_transaction;

begin

select	cd_pessoa_fisica,
	cd_estabelecimento
into	cd_pessoa_fisica_w,
	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;
qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
open C01;
loop
fetch C01 into	
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_fisica_w,null,nm_usuario_p,null,'S');
	end;
end loop;
close C01;

end gerar_evento_isolamento;
/