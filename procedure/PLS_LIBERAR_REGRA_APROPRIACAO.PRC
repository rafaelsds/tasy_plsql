create or replace
procedure pls_liberar_regra_apropriacao
			(	nr_sequencia_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_regra_w			pls_regra_apropriacao.nr_sequencia%type;
nr_seq_tabela_w			pls_tabela_preco.nr_sequencia%type;
nr_seq_plano_w			pls_plano.nr_sequencia%type;
nr_seq_classif_dependencia_w	pls_classif_dependencia.nr_sequencia%type;
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
dt_aprovacao_contrato_w		pls_contrato.dt_aprovacao%type;
dt_inicio_vigencia_w		pls_regra_apropriacao.dt_inicio_vigencia%type;
vl_soma_w			pls_regra_apropriacao_item.vl_apropriacao%type;
tx_soma_aprop_w			pls_regra_apropriacao_item.tx_apropriacao%type;
dt_contrato_w			pls_contrato.dt_contrato%type;
nr_contrato_w			pls_contrato.nr_contrato%type;
qt_registros_w			number(10);
qt_valor_w			number(10);
qt_taxa_w			number(10);

Cursor C01 is
	select	nr_sequencia,
		vl_preco_atual,
		qt_idade_inicial,
		qt_idade_final
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_w;

c01_w	C01%RowType;

Cursor C02 is
	select	nr_sequencia
	from	pls_segurado
	where	nr_seq_contrato = nr_seq_contrato_w
	and	nr_seq_plano	= nr_seq_plano_w
	and	nr_seq_tabela	= nr_seq_tabela_w
	and	((nr_seq_classif_dependencia = nr_seq_classif_dependencia_w) or (nr_seq_classif_dependencia_w is null))
	and	dt_liberacao is not null;

c02_w	C02%RowType;

begin
select	nr_seq_contrato,
	nr_seq_plano,
	nr_seq_tabela,
	nr_seq_classif_dependencia,
	dt_inicio_vigencia
into	nr_seq_contrato_w,
	nr_seq_plano_w,
	nr_seq_tabela_w,
	nr_seq_classif_dependencia_w,
	dt_inicio_vigencia_w
from	pls_regra_apropriacao
where	nr_sequencia	= nr_sequencia_p;

select	nr_contrato,
	dt_aprovacao,
	dt_contrato
into	nr_contrato_w,
	dt_aprovacao_contrato_w,
	dt_contrato_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_w;

if	(dt_inicio_vigencia_w < trunc(dt_contrato_w,'dd')) then
	--A data de in�cio vig�ncia n�o pode ser menor que a data do contrato.
	wheb_mensagem_pck.exibir_mensagem_abort(296420);
end if;

if	(nr_seq_plano_w is null) then
	--Favor informar o produto para a regra.
	wheb_mensagem_pck.exibir_mensagem_abort(296457);
end if;

if	(nr_seq_tabela_w is null) then
	--Favor informar a tabela de pre�o para a regra.
	wheb_mensagem_pck.exibir_mensagem_abort(296458);
end if;

select	count(1)
into	qt_registros_w
from	pls_contrato_plano
where	nr_seq_contrato	= nr_seq_contrato_w
and	nr_seq_plano	= nr_seq_plano_w;

if	(qt_registros_w = 0) then
	-- O produto #@NR_SEQ_PLANO#@ n�o est� vinculado ao contrato #@NR_CONTRATO#@. Favor verifique.
	wheb_mensagem_pck.exibir_mensagem_abort(296454,'NR_SEQ_PLANO='||nr_seq_plano_w||';NR_CONTRATO='||nr_contrato_w);
end if;

select	max(nr_sequencia)
into	nr_seq_regra_w
from	pls_regra_apropriacao
where	nr_sequencia			<> nr_sequencia_p
and	nr_seq_contrato			= nr_seq_contrato_w
and	nr_seq_plano			= nr_seq_plano_w
and	nr_seq_tabela			= nr_seq_tabela_w
and	nr_seq_classif_dependencia	= nr_seq_classif_dependencia_w
and	dt_fim_vigencia is null;

if	(nr_seq_regra_w is not null) then
	/*N�o � poss�vel liberar a regra.
	J� existe a regra #@NR_SEQ_REGRA#@ ativa para a mesma tabela de pre�o, produto, contrato e classifica��o de depend�ncia. Favor verificar!*/
	Wheb_mensagem_pck.exibir_mensagem_abort(295195,'NR_SEQ_REGRA=' || to_char(nr_seq_regra_w));
end if;

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
	begin
	select	count(1)
	into	qt_valor_w
	from	pls_regra_apropriacao_item
	where	nr_seq_apropriacao	= nr_sequencia_p
	and	nr_seq_preco		= c01_w.nr_sequencia
	and	vl_apropriacao is not null;
	
	select	count(1)
	into	qt_taxa_w
	from	pls_regra_apropriacao_item
	where	nr_seq_apropriacao	= nr_sequencia_p
	and	nr_seq_preco		= c01_w.nr_sequencia
	and	tx_apropriacao is not null;
	
	if	(qt_valor_w = 0) and (qt_taxa_w = 0) then
		--Favor informar os valores para faixa et�ria de #@QT_IDADE_INICIAL#@ � #@QT_IDADE_FINAL#@ anos.
		Wheb_mensagem_pck.exibir_mensagem_abort(296406,'QT_IDADE_INICIAL=' || to_char(c01_w.qt_idade_inicial) || ';QT_IDADE_FINAL=' || to_char(c01_w.qt_idade_final));
	elsif	(qt_valor_w <> 0) and (qt_taxa_w <> 0) then
		--Favor informar apenas a taxa ou o valor para faixa et�ria de #@QT_IDADE_INICIAL#@ � #@QT_IDADE_FINAL#@ anos.
		Wheb_mensagem_pck.exibir_mensagem_abort(296407,'QT_IDADE_INICIAL=' || to_char(c01_w.qt_idade_inicial) || ';QT_IDADE_FINAL=' || to_char(c01_w.qt_idade_final));
	elsif	(qt_valor_w > 0) then
		select	sum(vl_apropriacao)
		into	vl_soma_w
		from	pls_regra_apropriacao_item
		where	nr_seq_apropriacao	= nr_sequencia_p
		and	nr_seq_preco		= c01_w.nr_sequencia;
		
		if	(vl_soma_w <> c01_w.vl_preco_atual) then
			--O valor da apropria��o n�o confere com o valor da faixa et�ria de #@QT_IDADE_INICIAL#@ � #@QT_IDADE_FINAL#@ anos.
			Wheb_mensagem_pck.exibir_mensagem_abort(296408,'QT_IDADE_INICIAL=' || to_char(c01_w.qt_idade_inicial) || ';QT_IDADE_FINAL=' || to_char(c01_w.qt_idade_final));
		end if;
		
		select	count(1)
		into	qt_registros_w
		from	pls_regra_apropriacao_item
		where	nr_seq_apropriacao	= nr_sequencia_p
		and	nr_seq_preco		= c01_w.nr_sequencia
		and	ie_permite_reajustar = 'S';
		
		if	(qt_registros_w = 0) then
			--A op��o Permite reajustar apropria��o deve estar marcada em pelo menos uma apropria��o da faixa et�ria de #@QT_IDADE_INICIAL#@ � #@QT_IDADE_FINAL#@ anos.
			Wheb_mensagem_pck.exibir_mensagem_abort(295205,'QT_IDADE_INICIAL=' || to_char(c01_w.qt_idade_inicial) || ';QT_IDADE_FINAL=' || to_char(c01_w.qt_idade_final));
		end if;
	elsif	(qt_taxa_w > 0) then
		select	sum(tx_apropriacao)
		into	tx_soma_aprop_w
		from	pls_regra_apropriacao_item
		where	nr_seq_apropriacao	= nr_sequencia_p
		and	nr_seq_preco		= c01_w.nr_sequencia;
		
		if	(tx_soma_aprop_w <> 100) then
			--A taxa da apropria��o deve ser 100%. Verificar faixa et�ria de #@QT_IDADE_INICIAL#@ � #@QT_IDADE_FINAL#@ anos.
			Wheb_mensagem_pck.exibir_mensagem_abort(296410,'QT_IDADE_INICIAL=' || to_char(c01_w.qt_idade_inicial) || ';QT_IDADE_FINAL=' || to_char(c01_w.qt_idade_final));
		end if;
	end if;
	end;
end loop;
close C01;

update	pls_regra_apropriacao
set	dt_liberacao		= sysdate,
	nm_usuario_liberacao	= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

if	(dt_aprovacao_contrato_w is not null) then
	open C02;
	loop
	fetch C02 into
		c02_w;
	exit when C02%notfound;
		begin
		pls_preco_beneficiario_pck.gravar_preco_benef(c02_w.nr_sequencia, 'AR', 'S', dt_inicio_vigencia_w, 'N', null, nm_usuario_p, cd_estabelecimento_p);
		end;
	end loop;
	close C02;
end if;

commit;

end pls_liberar_regra_apropriacao;
/
