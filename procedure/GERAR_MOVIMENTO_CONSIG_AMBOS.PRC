create or replace
procedure gerar_movimento_consig_ambos(	cd_material_p		in number,
					qt_movimento_p		in number,
					cd_unidade_medida_p	in varchar2,
					ie_acao_p		in varchar2,
					cd_local_estoque_p	in number,
					cd_estabelecimento_p	in number,
					ie_rotina_movimento_p	in varchar2,
					nm_usuario_p		in varchar2,
					nr_seq_lote_fornec_p	in number default null,
					nr_sequencia_p		in number default null,
					nr_seq_item_p		in number default null,
					cd_setor_atendimento_p	in number default null,
					dt_mesano_referencia_p	in date default sysdate) is

/* qt_movimento_p - Sempre passar a quantidade de estoque, pois para verificar saldo se utiliza a quantidade de estoque. */

/* nr_sequencia_p - Geralmente utilizado para sequencia da tabela superior a tabela do item. */

/* ie_rotina_movimento_p - Utilizado para definir qual rotina sera utilizada para gerar o movimento

MEP -	Utilizado no objeto gerar_movto_estoque_producao que gera o movimento de estoque ao confirmar lote producao na funcao Administracao de Estoques, aba Lote producao.
	Para realizar a baixa, chama a rotina gerar_movto_estoque_prod_ambos.

GPI -	Utilizado nas rotinas da funcao Producao de Medicamentos ao separar os materiais para producao.
	Para realizar a baixa, chama a rotina gerar_prescricao_estoque.	
	
GPE -	Utilizado nas rotinas da funcao Producao de Medicamentos ao alterar etapa e status do lote, estornando o movimento.
	Para realizar o estorno, chama a rotina gerar_prescricao_estoque.
*/

ie_existe_saldo_w	varchar2(1) := 'S';
qt_saldo_movimento_w	number(15,4) := qt_movimento_p;
qt_movimento_w		number(15,4);
qt_saldo_disponivel_w	number(15,4);
ie_tipo_saldo_w		varchar2(1);
cd_cgc_fornecedor_w	fornecedor_mat_consignado.cd_fornecedor%type;
cd_un_medida_estoque_w	material.cd_unidade_medida_estoque%type;

begin

while	(qt_saldo_movimento_w > 0 and ie_existe_saldo_w = 'S') loop
	begin
	obter_fornec_consig_ambos(cd_estabelecimento_p, cd_material_p, nr_seq_lote_fornec_p, cd_local_estoque_p, ie_tipo_saldo_w, cd_cgc_fornecedor_w);
	
	if	('C' = ie_tipo_saldo_w) then
		obter_saldo_estoque_consignado(
			cd_estabelecimento_p    => cd_estabelecimento_p,
			cd_cgc_fornec_p		=> cd_cgc_fornecedor_w,
			cd_material_p         	=> cd_material_p,
			cd_local_estoque_p      => cd_local_estoque_p,
			dt_mesano_referencia_p	=> dt_mesano_referencia_p,
			qt_estoque_p  		=> qt_saldo_disponivel_w);
	else
		qt_saldo_disponivel_w := obter_saldo_disp_estoque(
						cd_estabelecimento_p	=> cd_estabelecimento_p,
						cd_material_p		=> cd_material_p,
						cd_local_estoque_p	=> cd_local_estoque_p,
						dt_mesano_referencia_p	=> dt_mesano_referencia_p,
						nr_seq_lote_p		=> nr_seq_lote_fornec_p);
	end if;
	
	if	(qt_saldo_disponivel_w >= qt_saldo_movimento_w) then
		qt_movimento_w := qt_saldo_movimento_w;
		qt_saldo_movimento_w := 0;
	else
		qt_movimento_w := qt_saldo_disponivel_w;
		qt_saldo_movimento_w := qt_saldo_movimento_w - qt_saldo_disponivel_w;
	end if;
	
	if	(qt_movimento_w > 0) then
		if	('MEP' = ie_rotina_movimento_p) then
			gerar_movto_estoque_prod_ambos(
				cd_material_p		=> cd_material_p,
				qt_movimento_p		=> qt_movimento_w,
				cd_unidade_medida_p	=> cd_unidade_medida_p,
				cd_cgc_fornecedor_p	=> cd_cgc_fornecedor_w,
				nr_seq_lote_fornec_p	=> nr_seq_lote_fornec_p,
				ie_acao_p		=> ie_acao_p,
				dt_mesano_referencia_p	=> dt_mesano_referencia_p,
				cd_local_estoque_p	=> cd_local_estoque_p,
				cd_estabelecimento_p	=> cd_estabelecimento_p,
				nr_lote_producao_p	=> nr_sequencia_p,
				nr_seq_componente_p	=> nr_seq_item_p,
				nm_usuario_p		=> nm_usuario_p);
		elsif	('GPI' = ie_rotina_movimento_p) then
			/* Necessario converter a quantidade pois a gerar_prescricao_estoque trata como quantidade de consumo */
			cd_un_medida_estoque_w := substr(obter_dados_material_estab(cd_material_p, cd_estabelecimento_p ,'UME'),1,30);
			
			qt_movimento_w := obter_quantidade_convertida(
						cd_material_p			=> cd_material_p,
						qt_material_p			=> qt_movimento_w,
						cd_unidade_medida_p		=> cd_un_medida_estoque_w,
						cd_unidade_medida_retorno_p	=> 'UMC');

			gerar_prescricao_estoque(cd_estabelecimento_p, null, null, cd_material_p, sysdate, ie_acao_p, cd_local_estoque_p,
						qt_movimento_w, cd_setor_atendimento_p, cd_unidade_medida_p, nm_usuario_p, 'I',
						null, null, null, null, cd_cgc_fornecedor_w, null, nr_seq_lote_fornec_p,
						null, null, null, 'X', null, nr_sequencia_p);
		elsif	('GPE' = ie_rotina_movimento_p) then
			/* Necessario converter a quantidade pois a gerar_prescricao_estoque trata como quantidade de consumo */
			cd_un_medida_estoque_w := substr(obter_dados_material_estab(cd_material_p, cd_estabelecimento_p ,'UME'),1,30);
			
			qt_movimento_w := obter_quantidade_convertida(
						cd_material_p			=> cd_material_p,
						qt_material_p			=> qt_movimento_w,
						cd_unidade_medida_p		=> cd_un_medida_estoque_w,
						cd_unidade_medida_retorno_p	=> 'UMC');
			
			gerar_prescricao_estoque(cd_estabelecimento_p, null, null, cd_material_p, sysdate, ie_acao_p, cd_local_estoque_p,
						qt_movimento_w, cd_setor_atendimento_p, cd_unidade_medida_p, nm_usuario_p, 'E',
						null, null, null, null, cd_cgc_fornecedor_w, null, nr_seq_lote_fornec_p,
						null, null, null, 'X', null, nr_sequencia_p);
		end if;
	else
		ie_existe_saldo_w := 'N';
	end if;
	
	end;
end loop;

end gerar_movimento_consig_ambos;
/