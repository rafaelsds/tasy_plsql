create or replace
procedure	enviar_comunic_resp_cih(nr_atendimento_p	number,
						cd_pessoa_fisica_p	varchar2,
						ds_perfil_p		varchar2,
						qt_dia_p		number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number) is
qt_reg_w		number(5);
ds_motivo_w		varchar2(150);
nr_atend_anterior_w	number(10);
ie_gerar_alerta_w	varchar2(1);

begin

select	count(*)
into	qt_reg_w
from	atend_paciente_hist
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	ie_evento		= 'I'
and		dt_inativacao is null
and	nvl(dt_final, dt_inicial) >= (sysdate - qt_dia_p);
	
	
	
	
nr_atend_anterior_w :=	obter_ultimo_atend_CIH(cd_pessoa_fisica_p,nm_usuario_p);
ds_motivo_w := nvl(substr(obter_desc_motivo_isol_cih(nr_atend_anterior_w),1,150),wheb_mensagem_pck.get_texto(793287));
ie_gerar_alerta_w := nvl(substr(obter_se_isol_gera_alerta(nr_atend_anterior_w),1,1),'S');

if	(qt_reg_w >= 1)	and	
	(ie_gerar_alerta_w = 'S') then

	insert	into comunic_interna (
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		nr_sequencia,
		ie_gerencial,
		nr_seq_classif,
		ds_perfil_adicional,
		cd_setor_destino,
		cd_estab_destino,
		ds_setor_adicional,
		dt_liberacao)
	values (sysdate,
		wheb_mensagem_pck.get_texto(801995),
		wheb_mensagem_pck.get_texto(791350) || ': '	|| obter_nome_pf(cd_pessoa_fisica_p) || chr(13) || chr(10) ||
		wheb_mensagem_pck.get_texto(307420) || ': '	|| nr_atendimento_p || chr(13) || chr(10) || chr(13) || chr(10) ||
		wheb_mensagem_pck.get_texto(801996, 'QT_DIA='||qt_dia_p) || chr(13) || chr(10) ||
		wheb_mensagem_pck.get_texto(801997) || ': '  || ds_motivo_w ,
		nm_usuario_p,
		sysdate,
		'N',
		null,
		comunic_interna_seq.nextval,
		'N',
		null,
		ds_perfil_p,
		null,
		cd_estabelecimento_p,
		null,
		sysdate);
		end	if;
	
end	enviar_comunic_resp_cih;
/
