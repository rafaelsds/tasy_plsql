create or replace
procedure Atualiza_prescr_agendamento(nr_seq_agenda_p		number,
				      nr_atendimento_p		number,
			              nm_usuario_p		Varchar2) is 

ie_gerar_passagem_w	varchar2(1);
ie_lote_sem_atendimento_w	varchar2(1);
cd_setor_atendimento_w	number(5);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
				      
begin
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

Obter_param_Usuario(916,1079, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_passagem_w);

update	prescr_medica
set	nr_atendimento 	= nr_atendimento_p,
	dt_atualizacao 	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_seq_agenda  	= nr_seq_agenda_p
and	nr_atendimento is null;


if	(ie_gerar_passagem_w = 'S') then

	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	prescr_medica
	where 	nr_seq_agenda  	= nr_seq_agenda_p;
	
	if	(nvl(cd_setor_atendimento_w,0) > 0) then
	
		gerar_passagem_setor_atend(nr_atendimento_p,cd_setor_atendimento_w,sysdate,'S',nm_usuario_p);
	
	end if;
end if;

Obter_param_Usuario(7029,110, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_lote_sem_atendimento_w);
if	(ie_lote_sem_atendimento_w = 'S') then
	baixar_ap_lote_consistido(nr_atendimento_p,cd_estabelecimento_w,nm_usuario_p);
end if;

commit;

end Atualiza_prescr_agendamento;
/