create or replace
procedure pls_copiar_carencia_sca_benef
			(	nr_seq_segurado_p	number,
				qt_dias_p		number,
				ie_isentar_carencia_p	varchar2,
				ie_commit_p		varchar2,
				nm_usuario_p		varchar2) is

nr_seq_plano_sca_w		number(10);
nr_seq_tipo_carencia_w		number(10);
qt_dias_w			number(10);
dt_inicio_vigencia_w		date;
qt_registros_w			number(10);
nr_seq_grupo_carencia_w		number(10);
ie_permite_isencao_carencia_w	pls_plano.ie_permite_isencao_carencia%type;
ie_copiar_carencias_sca_w	varchar2(1);

Cursor C01 is
	select	a.nr_seq_plano,
		nvl(a.dt_inclusao_benef,a.dt_inicio_vigencia),
		nvl(ie_permite_isencao_carencia,'S')
	from	pls_sca_vinculo	a,
		pls_plano	b
	where	a.nr_seq_plano		= b.nr_sequencia
	and	a.nr_seq_segurado	= nr_seq_segurado_p;

Cursor C02 is
	select	nr_seq_tipo_carencia,
		nr_seq_grupo_carencia,
		qt_dias
	from	pls_carencia
	where	nr_seq_plano = nr_seq_plano_sca_w
	and	dt_inicio_vigencia_w between nvl(dt_inicio_vig_plano,dt_inicio_vigencia_w) and nvl(dt_fim_vig_plano,dt_inicio_vigencia_w);

begin

open C01;
loop
fetch C01 into
	nr_seq_plano_sca_w,
	dt_inicio_vigencia_w,
	ie_permite_isencao_carencia_w;
exit when C01%notfound;
	begin
	ie_copiar_carencias_sca_w	:= 'S';
	
	if	(ie_isentar_carencia_p = 'S') and
		(ie_permite_isencao_carencia_w = 'N') then
		ie_copiar_carencias_sca_w	:= 'N';
	end if;
	
	if	(ie_copiar_carencias_sca_w = 'S') then
		open C02;
		loop
		fetch C02 into
			nr_seq_tipo_carencia_w,
			nr_seq_grupo_carencia_w,
			qt_dias_w;
		exit when C02%notfound;
			begin
			if	(nr_seq_tipo_carencia_w is not null) then
				select	count(1)
				into	qt_registros_w
				from	pls_carencia
				where	nr_seq_segurado		= nr_seq_segurado_p
				and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w;
			elsif	(nr_seq_grupo_carencia_w is not null) then
				select	count(1)
				into	qt_registros_w
				from	pls_carencia
				where	nr_seq_segurado		= nr_seq_segurado_p
				and	nr_seq_grupo_carencia	= nr_seq_grupo_carencia_w;
			end if;
			
			/*aaschlote OS - 279915*/
			if	(pls_consistir_sexo_carencia(nr_seq_segurado_p,nr_seq_tipo_carencia_w) = 'S') then
				if	(qt_registros_w = 0) then
					insert	into pls_carencia
						(	nr_sequencia,dt_atualizacao, nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							nr_seq_segurado, dt_inicio_vigencia, qt_dias,nr_seq_grupo_carencia,
							nr_seq_tipo_carencia,ds_observacao,ie_origem_carencia_benef,ie_carencia_anterior)
					values	(	pls_carencia_seq.NextVal, sysdate, nm_usuario_p,sysdate, nm_usuario_p,
							nr_seq_segurado_p, dt_inicio_vigencia_w, decode(qt_dias_p,-1,qt_dias_w,qt_dias_p),nr_seq_grupo_carencia_w,
							nr_seq_tipo_carencia_w,'Origem: Copia da car�ncia na isen��o de car�ncia','S','N');
				end if;
			end if;
			end;
		end loop;
		close C02;
	end if;
	end;
end loop;
close C01;

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_copiar_carencia_sca_benef;
/