create or replace
procedure palm_finalizar_adm_por_medic(nr_sequencia_p number,
			nm_usuario_p		Varchar2) is 
			
cd_profissional_w	varchar2(10);			

begin

select 	cd_pessoa_fisica
into	cd_profissional_w
from	usuario
where	nm_usuario = nm_usuario_p;

update	paciente_atend_medic_adm
set		dt_fim_administracao 	= sysdate,
		cd_profissional_termino	= cd_profissional_w
where	nr_sequencia 			= nr_sequencia_p
and		dt_fim_administracao	is null;

commit;

end palm_finalizar_adm_por_medic;
/