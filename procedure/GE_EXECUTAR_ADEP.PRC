create or replace
procedure ge_executar_adep	(	nr_prescricao_p		number,
					nr_seq_proced_p		number,
					nm_usuario_p		varchar2) is

nr_atendimento_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_proc_interno_w		number(10);
dt_prev_execucao_w		date;
nr_seq_proced_w			number(6);
nr_seq_horario_w		number(10);
ie_horario_valido_w		varchar2(1);
nr_seq_alteracao_w		number(10);
qt_prescr_proc_hor		number(5,0);
ie_alteracao_w			number(5);
ds_texto_aux_w			varchar2(2000);
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;
ie_param8_cpoe_w		varchar2(1);
nr_seq_proc_cpoe_w		cpoe_procedimento.nr_sequencia%type;
ie_proc_w				varchar2(1);
nr_seq_proc_cont_w		pls_integer;
dt_fim_w				cpoe_procedimento.dt_fim%type;
cd_gestao_exame_w		number(3) := 942;
ie_lado_w				prescr_procedimento.ie_lado%type;

cursor c01 is
select	a.nr_sequencia,
	    b.nr_sequencia,
		nvl(a.nr_seq_proc_cpoe, 99999) nr_seq_proc_cpoe,
		a.cd_funcao_origem
from	prescr_proc_hor b,
		prescr_procedimento a
where	b.nr_seq_procedimento = a.nr_sequencia
and		b.nr_prescricao = a.nr_prescricao
and		a.nr_prescricao = nr_prescricao_p
and		a.cd_procedimento = cd_procedimento_w
and		a.ie_origem_proced = ie_origem_proced_w
and		nvl(a.nr_seq_proc_interno,0) = nvl(nr_seq_proc_interno_w,0)
and		b.dt_horario = dt_prev_execucao_w
and		(a.ie_lado = ie_lado_w or a.ie_lado is null)
order by 3,1,2;

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_proced_p is not null) and
	(nm_usuario_p is not null) then
	
	obter_param_usuario(2314, 8, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_param8_cpoe_w);
	
	nr_seq_proc_cont_w := 0;
	
	select	max(obter_atendimento_prescr(a.nr_prescricao)),
			max(a.cd_procedimento),
			max(a.ie_origem_proced),
			max(a.nr_seq_proc_interno),
			max(trunc(a.dt_prev_execucao, 'mi')),
			decode(max(a.nr_seq_exame), null, 3, 8),
			max(a.ie_lado)
	into	nr_atendimento_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			dt_prev_execucao_w,
			ie_alteracao_w,
			ie_lado_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.nr_sequencia = nr_seq_proced_p;
	
	
	select	count(*)
	into	qt_prescr_proc_hor
	from	prescr_proc_hor
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_procedimento = nr_seq_proced_p
	and	dt_horario = dt_prev_execucao_w
	and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
	
	if	(qt_prescr_proc_hor = 0) then
		begin
		
		select	nvl(min(dt_horario),dt_prev_execucao_w)
		into	dt_prev_execucao_w
		from	prescr_proc_hor
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_procedimento = nr_seq_proced_p
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
		
		end;
	end if;

	open c01;
	loop
	fetch c01 into	nr_seq_proced_w,
			        nr_seq_horario_w,
					nr_seq_proc_cpoe_w,
					cd_funcao_origem_w;
	exit when c01%notfound;
		begin
		
		select	decode(count(*),0,'N','S')
		into	ie_horario_valido_w
		from	prescr_proc_hor
		where	nr_sequencia	= nr_seq_horario_w
		and	dt_fim_horario	is null
		and	dt_suspensao	is null
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
		
		if	(ie_horario_valido_w = 'S') then
	
			if (obter_funcao_ativa = cd_gestao_exame_w) then
				ds_texto_aux_w := wheb_mensagem_pck.get_texto(300571); --Administracao realizada via Gestao de Exames conforme parametro 78.
			else
				ds_texto_aux_w := wheb_mensagem_pck.get_texto(1120850); --Administracao realizada via "Exames pendentes", conforme parametro 110.
			end if;
			
			update	prescr_proc_hor
			set	dt_fim_horario	= sysdate,
				nm_usuario_adm	= nm_usuario_p
			where	nr_sequencia	= nr_seq_horario_w
			and	dt_fim_horario	is null
			and	dt_suspensao	is null;
			
			if ((nvl(nr_seq_proc_cpoe_w, 0) <> 99999) and 
				(nvl(cd_funcao_origem_w, 0) = 2314) and 
				(nr_seq_proc_cont_w <> nr_seq_proc_cpoe_w) and 
				(ie_param8_cpoe_w = 'A')) then			
					
				dt_fim_w := sysdate;
				
				if ((dt_prev_execucao_w > sysdate) and (dt_prev_execucao_w is not null)) then
					dt_fim_w := dt_prev_execucao_w;
				end if;
				
				update	cpoe_procedimento
				set 	dt_fim =  trunc(dt_fim_w + 1/24,'hh24') - 1/1440,
						nm_usuario = nm_usuario_p,
						dt_atualizacao = sysdate
				where	nr_sequencia = nr_seq_proc_cpoe_w
				and 	nvl(ie_evento_unico,'N') = 'S'
				and		ie_administracao = 'P';	
					
				nr_seq_proc_cont_w := nr_seq_proc_cpoe_w;					
							
			end if;

			select	prescr_mat_alteracao_seq.nextval
			into	nr_seq_alteracao_w
			from	dual;

			insert into prescr_mat_alteracao (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_prescricao,
				dt_alteracao,
				cd_pessoa_fisica,
				ie_alteracao,
				nr_seq_horario,
				ds_observacao,
				ds_justificativa,
				nr_seq_procedimento,
				nr_seq_recomendacao,
				nr_seq_horario_proc,
				nr_seq_horario_rec,
				ie_agrupador,
				ie_tipo_item,
				nr_seq_horario_sae,
				nr_seq_item_sae,
				dt_hor_acm_sn,
				nr_seq_horario_dieta,
				nr_seq_qualidade,
				nr_seq_satisfacao,
				dt_horario,
				nr_atendimento,
				cd_item,
				qt_dose_adm,
				cd_um_dose_adm,
				nr_seq_motivo,
				nr_seq_proc_interno)
			values (
				nr_seq_alteracao_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				null,
				sysdate,
				substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10),
				ie_alteracao_w,
				null,
				ds_texto_aux_w,
				null,
				nr_seq_proced_w,
				null,
				nr_seq_horario_w,
				null,
				null,
				'P',
				null,
				null,
				dt_prev_execucao_w,
				null,
				null,
				null,
				dt_prev_execucao_w,
				nr_atendimento_w,
				cd_procedimento_w,
				null,
				null,
				null,
				nr_seq_proc_interno_w);

		end if;

		end;
	end loop;
	close c01;


end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end ge_executar_adep;
/
