create or replace
procedure pls_liberar_item_pend(	nr_seq_analise_p	pls_analise_conta.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		Varchar2) is 

Cursor C01 is
	select	nr_sequencia	nr_seq_conta_proc,
		null		nr_seq_conta_mat,
		nr_seq_conta
	from	pls_conta_proc	a
	where	a.ie_status not in ('D','L','S','M')
	and	a.nr_seq_conta	in (	select	x.nr_sequencia
					from	pls_conta	x
					where	x.nr_seq_analise	= nr_seq_analise_p)
	union all
	select	null		nr_seq_conta_proc,
		nr_sequencia	nr_seq_conta_mat,
		nr_seq_conta
	from	pls_conta_mat	a
	where	a.ie_status not in ('D','L','S','M')
	and	a.nr_seq_conta	in (	select	x.nr_sequencia
					from	pls_conta	x
					where	x.nr_seq_analise	= nr_seq_analise_p);
begin


for r_c01_w in C01() loop
	begin
	
	if	(r_c01_w.nr_seq_conta_proc is not null) then
		update	pls_conta_proc	a
		set	vl_unitario		= 0,
			vl_liberado		= 0,
			qt_procedimento		= 0,
			ie_status		= 'L',
			ie_glosa		= 'S',
			vl_glosa		= vl_procedimento_imp,
			vl_lib_taxa_co   	= 0,       
			vl_lib_taxa_material	= 0,
			vl_lib_taxa_servico     = 0,
			vl_liberado_co 		= 0,         
			vl_liberado_hi          = 0,
			vl_liberado_material    = 0,
			vl_glosa_co         	= vl_co_ptu_imp,    
			vl_glosa_hi             = vl_procedimento_ptu_imp,
			vl_glosa_material       = vl_material_ptu_imp,
			vl_glosa_taxa_co        = vl_taxa_co_imp,
			vl_glosa_taxa_material	= vl_taxa_material_imp,
			vl_glosa_taxa_servico   = vl_taxa_servico_imp,
			ie_status_pagamento	= 'G'
		where	a.nr_sequencia		= r_c01_w.nr_seq_conta_proc;
		pls_inserir_hist_analise(r_c01_w.nr_seq_conta,  nr_seq_analise_p, 3, r_c01_w.nr_seq_conta_proc, 'P', null, null,'Item glosado ao finalizar a an�lise', null, nm_usuario_p,cd_estabelecimento_p);
	else
		update	pls_conta_mat	a
		set	vl_unitario		= 0,
			vl_liberado		= 0,
			qt_material		= 0,
			ie_status		= 'L',
			ie_glosa		= 'S',
			vl_glosa		= vl_material_imp,
			vl_lib_taxa_material 	= 0,
			vl_glosa_taxa_material  = vl_taxa_material_imp,
			ie_status_pagamento	= 'G'
		where	a.nr_sequencia		= r_c01_w.nr_seq_conta_mat;
		pls_inserir_hist_analise(r_c01_w.nr_seq_conta,  nr_seq_analise_p, 3, r_c01_w.nr_seq_conta_mat, 'M', null, null,'Item glosado ao finalizar a an�lise',null, nm_usuario_p,cd_estabelecimento_p);
	end if;
	
	end;
end loop;

commit;

end pls_liberar_item_pend;
/