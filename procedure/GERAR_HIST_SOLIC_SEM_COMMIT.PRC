create or replace
procedure gerar_hist_solic_sem_commit(
				nr_solic_compra_p	Number,
				ds_titulo_p	varchar2,
				ds_historico_p	varchar2,
				cd_evento_p	varchar2,
				nm_usuario_p	varchar2) as

begin

insert into solic_compra_hist(
	nr_sequencia,
	nr_solic_compra,
	dt_atualizacao,
	nm_usuario,
	dt_historico,
	ds_titulo,
	ds_historico,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_tipo,
	cd_evento,
	dt_liberacao)
values	(solic_compra_hist_seq.nextval,
	nr_solic_compra_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	ds_titulo_p,
	substr(ds_historico_p,1,4000),
	sysdate,
	nm_usuario_p,
	'S',
	cd_evento_p,
	sysdate);

end gerar_hist_solic_sem_commit;
/
