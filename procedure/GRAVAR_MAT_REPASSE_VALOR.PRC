create or replace
procedure GRAVAR_MAT_REPASSE_VALOR(	ds_observacao_p		in	varchar2,
					nm_usuario_p		in	varchar2,
					ie_status_ant_p		in	varchar2,
					ie_status_atual_p		in	varchar2,
					vl_repasse_ant_p		in	number,
					vl_repasse_atual_p		in	number,
					nr_seq_retorno_p		in	number,
					nr_seq_lote_audit_hist_p	in	number,
					nr_seq_mat_repasse_p	in	number,
					nr_seq_regra_rep_ret_p	in	number) is

begin

insert into mat_repasse_valor
	(ds_obs_interna,
	dt_atualizacao,
	dt_atualizacao_nrec,
	ie_status_ant,
	ie_status_atual,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_lote_audit_hist,
	nr_seq_mat_repasse,
	nr_seq_regra_rep_ret,
	nr_seq_retorno,
	nr_sequencia,
	vl_repasse_ant,
	vl_repasse_atual)
values	(ds_observacao_p,
	sysdate,
	sysdate,
	ie_status_ant_p,
	ie_status_atual_p,
	nm_usuario_p,
	nm_usuario_p,
	nr_seq_lote_audit_hist_p,
	nr_seq_mat_repasse_p,
	nr_seq_regra_rep_ret_p,
	nr_seq_retorno_p,
	proc_repasse_valor_seq.nextval,
	vl_repasse_ant_p,
	vl_repasse_atual_p);

commit;

end	GRAVAR_MAT_REPASSE_VALOR;
/