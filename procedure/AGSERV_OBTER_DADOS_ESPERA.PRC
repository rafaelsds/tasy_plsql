create or replace
procedure AgServ_Obter_Dados_Espera(cd_pessoa_fisica_p		varchar2,
									cd_estabelecimento_p	number,
									nr_seq_agecons_p		number,
									nr_seq_agenda_destino_p	number,
									nm_usuario_p			varchar2) is 


cd_senha_w		varchar2(20);
cd_usuario_conv_w	varchar2(30);
cd_medico_solic_w	varchar2(10);
cd_medico_exec_w	varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
nr_seq_proc_interno_w	number(10);
qt_total_sessoes_w	number(3);
nr_secao_atual_w	number(3);
cd_especialidade_w	number(10,0);
cd_procedencia_w	number(5,0);
cd_cid_w		varchar2(10);
ie_tipo_atendimento_w	number(3,0);
ie_carater_inter_sus_w	varchar2(2);
nr_telefone_w		varchar2(80);
ds_erro_w			varchar2(255);

Cursor C01 is
	select	max(a.cd_senha),
			max(a.cd_usuario_convenio),
			max(a.cd_medico_solic),
			max(a.cd_medico),
			max(a.cd_procedimento),
			max(a.ie_origem_proced),
			max(a.nr_seq_proc_interno),
			max(a.qt_total_secao),
			max(a.nr_secao),
			max(a.cd_especialidade),
			max(a.cd_procedencia),
			max(a.cd_cid),
			max(a.ie_tipo_atendimento),
			max(a.ie_carater_inter_sus),
			max(a.nr_telefone)
	from	agenda_consulta a,
			agenda b
	where	a.cd_agenda		= b.cd_agenda
	and		b.cd_tipo_agenda	= 5
	and		b.cd_estabelecimento	= cd_estabelecimento_p
	and		b.ie_situacao		= 'A'	
	and		a.nr_sequencia		= nr_seq_agecons_p
	order by 1;
	
begin

if	(cd_pessoa_fisica_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nr_seq_agecons_p is not null) and
	(nr_seq_agenda_destino_p is not null)then
	begin
	
	open C01;
	loop
	fetch C01 into	
		cd_senha_w,
		cd_usuario_conv_w,
		cd_medico_solic_w,	
		cd_medico_exec_w,	
		cd_procedimento_w,	
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		qt_total_sessoes_w,	
		nr_secao_atual_w,	
		cd_especialidade_w,
		cd_procedencia_w,	
		cd_cid_w,	
		ie_tipo_atendimento_w,
		ie_carater_inter_sus_w,
		nr_telefone_w;
	exit when C01%notfound;
	
	begin
	
	if	(nr_secao_atual_w is not null) and
		(nr_secao_atual_w < qt_total_sessoes_w)then
		nr_secao_atual_w	:= nr_secao_atual_w + 1;
	end if;

		
	update	agenda_consulta
	set		cd_senha				= cd_senha_w,
			cd_usuario_convenio		= cd_usuario_conv_w,
			cd_medico_solic 		=	cd_medico_solic_w,
			cd_medico				= cd_medico_exec_w,
			cd_procedimento			= cd_procedimento_w,
			ie_origem_proced		= ie_origem_proced_w,
			nr_seq_proc_interno		= nr_seq_proc_interno_w,
			qt_total_secao			= qt_total_sessoes_w,
			nr_secao				= nr_secao_atual_w,
			cd_especialidade		= cd_especialidade_w,
			cd_procedencia			= cd_procedencia_w,
			cd_cid					= cd_cid_w,
			ie_tipo_atendimento		= ie_tipo_atendimento_w,
			ie_carater_inter_sus	= ie_carater_inter_sus_w,
			nr_telefone				= nr_telefone_w				
	where	nr_sequencia			= nr_seq_agenda_destino_p;
	
	commit;	
	exception
	when others then
		ds_erro_w	:= '';
	end;
		
	end loop;
	close C01;
	
	end;

end if;

end AgServ_Obter_Dados_Espera;
/