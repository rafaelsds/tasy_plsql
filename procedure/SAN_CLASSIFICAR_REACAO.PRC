create or replace
procedure san_classificar_reacao (nr_seq_reacao_p number,
				  ie_classificacao_p number,
				  ie_gravidade_p number,
				  ie_correlacao_p number,
				  ie_diagnostico_p number,
				  cd_pf_classif_p varchar2,
				  ds_retorno_p out varchar2) is
				  
dt_lib_medico_w 		date;
dt_classificacao_w		date;

begin

select	dt_lib_medico,
	dt_classificacao
into	dt_lib_medico_w,
	dt_classificacao_w
from 	san_trans_reacao
where	nr_sequencia = nr_seq_reacao_p;

if (dt_lib_medico_w is not null) and (dt_classificacao_w is null) then
	update	san_trans_reacao
	set	ie_classificacao = ie_classificacao_p,
		ie_gravidade = ie_gravidade_p,
		ie_correlacao = ie_correlacao_p,
		ie_diagnostico = ie_diagnostico_p,
		cd_pf_classificacao = cd_pf_classif_p,
		dt_classificacao = SYSDATE
	where	nr_sequencia = nr_seq_reacao_p;
	
	commit;
	
	ds_retorno_p := substr(obter_texto_tasy(454493, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	
elsif (dt_lib_medico_w is null) then
	ds_retorno_p := substr(obter_texto_tasy(454491, wheb_usuario_pck.get_nr_seq_idioma),1,255);
        	 
elsif (dt_classificacao_w is not null) then
	ds_retorno_p := substr(obter_texto_tasy(454492, wheb_usuario_pck.get_nr_seq_idioma),1,255);
end if;

end san_classificar_reacao;
/