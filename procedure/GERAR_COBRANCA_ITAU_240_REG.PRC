create or replace
procedure GERAR_COBRANCA_ITAU_240_REG(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2)  is
					
					
/*===========================================================
	             =>>>>>	A T E N � � O        <<<<<<=

Esta procedure � uma c�pia da GERAR_COBRANCA_ITAU_240
com altera��o apenas na carteira para contemplar o projeto
de cobran�as registradas, OS 1170310.

Como se trata de um projeto e n�o possu�mos cliente para validar junto 
ao banco, os defeitos devem ser verificados com Peterson antes
de serem documentados. 
============================================================*/					

ds_conteudo_w			varchar2(240);

/* Header do arquivo */
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(30);
cd_cgc_w			varchar2(14);
cd_agencia_bancaria_w		varchar2(4);
dt_remessa_retorno_w		date;

/* Detalhe */
nm_pessoa_w			varchar2(40);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(15);
ds_cidade_w			varchar2(15);
nr_inscricao_w			varchar2(15);
ds_nosso_numero_w		varchar2(15);
vl_titulo_w			varchar2(15);
vl_desconto_w			varchar2(15);
vl_juros_mora_w			varchar2(15);
nr_titulo_w			varchar2(11);
cd_cep_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_vencimento_w			varchar2(8);
ds_uf_w				varchar2(2);
ie_tipo_inscricao_w		varchar2(1);

/* Trailer de Lote */
vl_titulos_cobr_w		varchar2(15);
qt_titulos_cobr_w		number(5);
qt_registro_lote_w		number(4)	:= 0;

/* Trailer do Arquivo */
qt_registro_w			number(5)	:= 0;
qt_registro_P_w			number(6)	:= 0;
qt_registro_Q_w			number(6)	:= 0;

nr_conta_w				varchar2(5);
nr_dac_w				varchar2(1);
nr_carteira_w			varchar2(3);

Cursor	C01 is
select	lpad(nvl(c.cd_agencia_bancaria,'0'),4,'0'),
	lpad(82||somente_numero(lpad(b.nr_titulo,8,'0')||'-'|| calcula_digito('Modulo11',(82||lpad(b.nr_titulo,8,'0')))),15,'0') ds_nosso_numero,
	lpad(b.nr_titulo,10,0) nr_titulo,
	to_char(nvl(b.dt_pagamento_previsto,sysdate),'DDMMYYYY') dt_vencimento,
	lpad(replace(to_char(b.vl_titulo, 'fm0000000000000.00'),'.',''),15,'0') vl_titulo,
	to_char(nvl(b.dt_emissao,sysdate),'DDMMYYYY') dt_emissao,
	lpad(replace(to_char(b.vl_titulo * b.tx_juros / 100 / 30, 'fm0000000000000.00'),'.',''),15,'0') vl_juros_mora,
	lpad(replace(to_char(b.TX_DESC_ANTECIPACAO, 'fm0000000000000.00'),'.',''),15,'0') vl_desconto,
	nvl(decode(b.cd_pessoa_fisica, null, '2', '1'),'0') ie_tipo_inscricao,
	lpad(decode(decode(b.cd_pessoa_fisica, null, 2, 1),2,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,(substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,9) || '0000' || substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),10,2)),'000000000000000'),15,'0') nr_inscricao,
	rpad(substr(nvl(elimina_caractere_especial(obter_nome_pf_pj(b.cd_pessoa_fisica, b.cd_cgc)),' '),1,30),30,' ') nm_pessoa,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'E')),' '),1,40),40,' ') ds_endereco,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'B')),' '),1,15),15,' ') ds_bairro,
	lpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),' '),1,8),8,'0') cd_cep,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),' '),1,15),15,' ') ds_cidade,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),' '),1,2),2,' ') ds_uf,
	lpad((select substr(nvl(max(x.cd_conta),'0'),1,5) from banco_estabelecimento x where x.nr_sequencia = a.nr_seq_conta_banco),5,'0') nr_conta,
	lpad((select substr(nvl(max(x.ie_digito_conta),'0'),1,1) from banco_estabelecimento x where x.nr_sequencia = a.nr_seq_conta_banco),1,'0') nr_dac,
	lpad((select substr(nvl(max(cd_carteira),'0'),1,3) from banco_carteira x where x.nr_sequencia =  nvl(a.nr_seq_carteira_cobr,b.nr_seq_carteira_cobr)),3,'0')
from	pls_mensalidade d,
	titulo_receber b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo		= b.nr_titulo
and	d.nr_sequencia(+)	= b.nr_seq_mensalidade
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
begin

delete 
from	w_envio_banco
where	nm_usuario = nm_usuario_p;

/* Header do Arquivo */

select	lpad(b.cd_cgc,14,'0'),
	lpad(nvl(c.cd_agencia_bancaria,'0'),4,'0') cd_agencia_bancaria,
	rpad(substr(nvl(elimina_caractere_especial(obter_razao_social(b.cd_cgc)),' '),1,30),30,' ') nm_empresa,
	rpad('BANCO ITA� SA', 30, ' ') nm_banco,
	nvl(a.dt_remessa_retorno,sysdate)
into	cd_cgc_w,
	cd_agencia_bancaria_w,
	nm_empresa_w,
	nm_banco_w,
	dt_remessa_retorno_w
from	banco_estabelecimento c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:= 	'341' ||
			'0000' || 
			'0' || 
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_w ||
			rpad(' ',20,' ') ||
			cd_agencia_bancaria_w ||
			' ' ||
			'0000000' ||
			'00000' ||
			' ' ||
			'0' ||
			nm_empresa_w ||
			nm_banco_w ||
			rpad(' ',10,' ') ||
			'1' ||
			lpad(to_char(dt_remessa_retorno_w,'DDMMYYYY'),8,'0') ||
			lpad(to_char(dt_remessa_retorno_w,'hh24miss'),6,'0') ||
			'000001' ||
			'040' ||
			'00000' ||
			rpad(' ',54,' ') ||
			'000' ||
			rpad(' ',12,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);

/*	Header do Lote	*/

qt_registro_lote_w	:= qt_registro_lote_w + 1;
qt_registro_w		:= qt_registro_w + 1;

ds_conteudo_w		:= 	'341' ||
				lpad(qt_registro_lote_w,4,'0') ||
				'1' ||
				'R' ||
				'01' ||
				'00' ||
				'030' || 
				' ' ||
				'2' ||
				lpad(cd_cgc_w,15,'0') ||
				rpad(' ',20,' ') ||
				cd_agencia_bancaria_w ||
				' ' ||
				'0000000' ||
				'00000' ||
				' ' ||
				'0' ||
				nm_empresa_w ||
				rpad(' ',80,' ') ||
				'00000001' ||
				to_char(dt_remessa_retorno_w,'DDMMYYYY') ||
				'00000000' ||
				rpad(' ',33,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);
				
/* Segmento P, Segmento Q, Segmento R */

open	C01;
loop
fetch	C01 into
	cd_agencia_bancaria_w,
	ds_nosso_numero_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_mora_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w,
	nr_conta_w,
	nr_dac_w,
	nr_carteira_w;
exit	when C01%notfound;

	/* Segmento P */

	qt_registro_w		:= qt_registro_w + 1;
	qt_registro_P_w		:= qt_registro_P_w + 1;

	ds_conteudo_w		:=	'341' 						|| /* Pos 1 a 3 */
						lpad(qt_registro_lote_w,4,'0') 	|| /* Pos 4 a 7 */
						'3' 							|| /*Pos 8*/ 
						lpad(qt_registro_P_w,5,'0') 	|| /*Pos 9 a 13*/
						'P' 							|| /*Pos 14*/ 
						' ' 							|| /*Pos 15*/ 
						'01' 							|| /*Pos 16 a 17*/
						'0'								|| /*Pos 18*/
						cd_agencia_bancaria_w 			|| /*Pos 19 a 22*/
						' ' 							|| /*Pos 23*/
						'0000000' 						|| /*Pos 24 a 30*/
						nr_conta_w						|| /*Pos 31 a 35*/
						' ' 							|| /*Pos 36*/
						nr_dac_w						|| /*Pos 37*/
						nr_carteira_w					|| /*Pos 38 a 40*/ 
						'00000000' 						|| /*Pos 41  a 48 - Vai zerado conforme nota no layout.*/
						'0' 							|| /*Pos 49 - Dac do nosso numero*/
						rpad(' ',8,' ') 				|| /*Pos 50 a 57*/	
						'00000' 						|| /*Pos 58 a 62*/
						nr_titulo_w 					|| /*Pos 63 a 72*/
						rpad(' ',5,' ') 				|| /*Pos 73a 77*/
						dt_vencimento_w					|| /*Pos 78 a 85*/
						vl_titulo_w 					|| /*Pos 86 a 100*/
						'00000' 						|| /*Pos 101 a 105*/
						'0' 							|| /*Pos 106*/
						'99' 							|| /*Pos 107 a 108*/
						'A' 							|| /*Pos 109*/
						dt_emissao_w 					|| /*Pos 110 a 117*/
						'0' 							|| /*Pos 118*/
						'00000000' 						|| /*Pos 119 a 126*/
						vl_juros_mora_w 				|| /*Pos 127 a  141*/
						'0' 							|| /*Pos 142*/
						'00000000' 						|| /*Pos 143 a 150*/		
						vl_desconto_w 					|| /*Pos 151 a 165*/
						'000000000000000' 				|| /*Pos 166 a 180 */
						'000000000000000' 				|| /*Pos 181 a 195*/
						lpad(nr_titulo_w, 25, ' ') 		|| /*Pos 196 a 220*/
						'0' 							|| /*Pos 221*/
						'00' 							|| /*Pos 222 a 223*/
						'0' 							|| /*Pos 224*/
						'00' 							|| /*Pos 225 a 226*/
						'0000000000000' 				|| /*Pos *227 a 239*/
						' ';							   /*Pos 240*/
					
	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval);

	/* Segmento Q */

	qt_registro_w		:= qt_registro_w + 1;
	qt_registro_P_w		:= qt_registro_P_w + 1;
	
	ds_conteudo_w		:=	'341' ||
					lpad(qt_registro_lote_w, 4, '0') ||
					'3' ||
					lpad(qt_registro_P_w, 5, '0') ||
					'Q' ||
					' ' ||
					'01' ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					nm_pessoa_w ||
					rpad(' ',10,' ') ||
					ds_endereco_w ||
					ds_bairro_w ||
					cd_cep_w ||
					ds_cidade_w ||
					ds_uf_w ||
					'0' ||
					'000000000000000' ||
					rpad(' ',30,' ') ||
					rpad(' ',10,' ') ||
					'000' ||
					rpad(' ',28,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval);

end	loop;
close	C01;

/*	Trailer de Lote	*/

select	lpad(count(1),6,'0'),
	replace(to_char(sum(c.vl_titulo), 'fm0000000000000.00'),'.','')
into	qt_titulos_cobr_w,
	vl_titulos_cobr_w
from	titulo_receber c,
	titulo_receber_cobr b,
	cobranca_escritural a
where	b.nr_titulo	= c.nr_titulo
and	a.nr_sequencia	= b.nr_seq_cobranca
and	a.nr_sequencia	= nr_seq_cobr_escrit_p;

qt_registro_lote_w	:= qt_registro_lote_w + 1;

ds_conteudo_w		:=	'341' ||
				'0001' || 
				'5' ||
				rpad(' ',9,' ') ||
				lpad(qt_registro_w,6,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				rpad(' ',46,' ') ||
				rpad(' ',8,' ') ||
				rpad(' ',117,' ');

qt_registro_w		:= qt_registro_w + 1;

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);

/* Trailer do Arquivo */

qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:=	'341' ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			'000001' ||
			lpad(qt_registro_w,6,'0') ||
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);

end GERAR_COBRANCA_ITAU_240_REG;
/