create or replace
procedure PROCESSAR_XML_ALFABETA(	ds_xml_p		clob,
					cd_estabelecimento_p	number,
					cd_tab_preco_mat_p	number,
					nm_usuario_p		Varchar2) is 

xml_w	xmltype;
ds_patch_w	varchar2(255);
xml_aux_w xmltype;
nr_ultimo_log_w varchar2(56);
nr_doc_historico_w tab_preco_material_hist.nr_documento%type;
qt_reg_w	number(10)	:= 0;

Cursor c_labs is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Laborato/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');
		
Cursor c_tipo_venda is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/TipoVen/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');

Cursor c_tamanhos is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Tamanio/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');

Cursor c_formas is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Formas/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');
	
Cursor c_vias is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Via/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');

Cursor c_active_ingredients is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Monodro/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');

Cursor c_acoes is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Acciones/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');
		
Cursor c_compounds is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/NuevaDro/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');

Cursor c_potencias is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Potencia/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');
		
Cursor c_quantidades is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Cantidad/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');

Cursor c_controles is
	select	cd_registro,
		ds_registro
	from	xmltable('/respuesta/basecompleta/Control/registro' passing xml_w columns
		cd_registro number path 'id',
		ds_registro varchar(200) path 'des');
		
Cursor c_materiais is
	select	cd_registro,
		ds_registro,
		ds_apresentacao,
		dt_vigencia,
		vl_preco,
		ie_iva,
		cd_laboratorio,
		cd_tipo_venda,
		ie_situacao,
		ie_refrigerado,
		ie_importado,
		cd_tipo_controle,
		cd_tamanho,
		cd_forma_apresentacao,
		cd_via,
		cd_principio,
		cd_acao,
		cd_concentracao,
		cd_unidade,
		qt_unidade,
		bars,
		gtins,
		mdrogas
	from	xmltable('/respuesta/basecompleta/articulos/articulo' passing xml_w columns
			cd_registro 			number path 'reg',
			ds_registro 			varchar(200) path 'nom',
			ds_apresentacao 		varchar(200) path 'pres',
			dt_vigencia 			varchar(14) path 'vig',
			vl_preco 			number(12,4) path 'prc',
			ie_iva	 			varchar2(10) path 'iva',
			cd_laboratorio	 		varchar2(10) path 'labo',
			cd_tipo_venda	 		varchar2(10) path 'tipov',
			ie_situacao	 		varchar2(10) path 'est',
			ie_importado	 		varchar2(10) path 'imp',
			ie_refrigerado	 		varchar2(10) path 'hel',
			cd_tipo_controle	 	varchar2(10) path 'salud',
			cd_tamanho	 		varchar2(10) path 'tam',
			cd_forma_apresentacao 		varchar2(10) path 'for',
			cd_via 				varchar2(10) path 'via',
			cd_principio			varchar2(10) path 'dro',
			cd_acao				varchar2(10) path 'acc',
			cd_concentracao			varchar2(10) path 'upot',
			cd_unidade			varchar2(10) path 'uuni',
			qt_unidade			varchar2(10) path 'uni',
			bars				xmltype path 'bars',
			gtins				xmltype path 'gtins',
			mdrogas	            xmltype path 'mdrogas');
	
Cursor c_bars is
	select	bar
	from	xmltable('/bars/bar' passing xml_aux_w columns
		bar varchar2(255) path '/');

Cursor c_gtins is
	select	gtin
	from	xmltable('/gtins/gtin' passing xml_aux_w columns
		gtin varchar2(255) path '/');

Cursor c_compoundMaterials is
	select	mdroga
	from	xmltable('/mdrogas/mdroga' passing xml_aux_w columns
		mdroga varchar2(255) path '/');
		
begin
gravar_processo_longo('PROCESSAR_XML_ALFABETA','PROCESSAR_XML_ALFABETA',qt_reg_w);

execute immediate 'alter session set nls_numeric_characters = ''.,''';

xml_w	:= xmltype.createxml(ds_xml_p);

select	nvl(max(nr_documento),0)
into 	nr_doc_historico_w 
	from (	select	nr_documento 
		from 	tab_preco_material_hist 
		where 	cd_tab_preco_mat = cd_tab_preco_mat_p 
		order by dt_atualizacao desc)
where  ROWNUM = 1;
	
select	nvl(max(nr_ultimo_log),0)
into 	nr_ultimo_log_w
from	xmltable('/respuesta/basecompleta' passing xml_w columns
	nr_ultimo_log varchar2(56) path 'ultimolog');

if (nr_doc_historico_w != nr_ultimo_log_w) then

	for r_c01 in c_labs loop
		begin
		bft_global_price_pck.insert_laboratory(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_tipo_venda loop
		begin
		bft_global_price_pck.insert_tipo_venda(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_tamanhos loop
		begin
		bft_global_price_pck.insert_size(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_formas loop
		begin
		bft_global_price_pck.insert_form_of_presentation(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_vias loop
		begin
		bft_global_price_pck.insert_via(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_active_ingredients loop
		begin
		bft_global_price_pck.insert_active_ingredient(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_acoes loop
		begin
		bft_global_price_pck.insert_action(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_compounds loop
		begin
		bft_global_price_pck.insert_compound(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_potencias loop
		begin
		bft_global_price_pck.insert_concentration(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_quantidades loop
		begin
		bft_global_price_pck.insert_unidade_medida(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;

	for r_c01 in c_controles loop
		begin
		bft_global_price_pck.insert_tipo_controle(r_c01.cd_registro,r_c01.ds_registro,nm_usuario_p);
		end;
	end loop;
	commit;

	for r_cMateriais in c_materiais loop
		begin
		qt_reg_w	:= qt_reg_w + 1;
		gravar_processo_longo('PROCESSAR_XML_ALFABETA','PROCESSAR_XML_ALFABETA',qt_reg_w);
		bft_global_price_pck.insert_global_material(	
								cd_unidade_p		=> r_cMateriais.cd_unidade,
								cd_principio_p		=> r_cMateriais.cd_principio,
								cd_concentracao_p	=> r_cMateriais.cd_concentracao,
								cd_acao_p		=> r_cMateriais.cd_acao,
								cd_forma_apresentacao_p	=> r_cMateriais.cd_forma_apresentacao,
								cd_laboratorio_p	=> r_cMateriais.cd_laboratorio,
								cd_tamanho_p		=> r_cMateriais.cd_tamanho,
								cd_tipo_controle_p	=> r_cMateriais.cd_tipo_controle,
								cd_tipo_venda_p		=> r_cMateriais.cd_tipo_venda,
								cd_via_adm_p		=> r_cMateriais.cd_via,
								cd_apresentacao_p	=> r_cMateriais.cd_registro,	
								ds_apresentacao_p	=> r_cMateriais.ds_apresentacao,
								ie_importado_p		=> r_cMateriais.ie_importado,
								ie_refrigerado_p	=> r_cMateriais.ie_refrigerado,
								ie_iva_p		=> r_cMateriais.ie_iva,
								qt_unidade_p		=> r_cMateriais.qt_unidade,
								cd_material_glo_p	=> r_cMateriais.cd_registro,
								ds_material_glo_p	=> r_cMateriais.ds_registro,
								nm_usuario_p		=> nm_usuario_p,
								cd_estabelecimento_p => cd_estabelecimento_p);

		xml_aux_w	:= r_cMateriais.bars;

		for r_cBars in c_bars loop
			begin
			if	(r_cBars.bar is not null) then
				bft_global_price_pck.insert_barras(
										cd_barras_p			=>	r_cBars.bar,
										cd_material_glo_p	=>	r_cMateriais.cd_registro, 
										nm_usuario_p		=>	nm_usuario_p);
			end if;
			end;
		end loop;

		xml_aux_w	:= r_cMateriais.gtins;			
		for r_cGtins in c_gtins loop
			begin
			if	(r_cGtins.gtin is not null) then		
				bft_global_price_pck.insert_gtin(
										cd_gtin_p			=>	r_cGtins.gtin,
										cd_material_glo_p	=>	r_cMateriais.cd_registro, 
										nm_usuario_p		=>	nm_usuario_p);	
			end if;
			end;
		end loop;

		xml_aux_w	:= r_cMateriais.mdrogas;
		for r_cCompounds in c_compoundMaterials loop
			begin
			if	(r_cCompounds.mdroga is not null) then
				bft_global_price_pck.insert_comp_material(
										cd_composto_p		=>	r_cCompounds.mdroga,
										cd_material_glo_p	=>	r_cMateriais.cd_registro, 
										nm_usuario_p		=>	nm_usuario_p);
			end if;
			end;
		end loop;

		bft_global_price_pck.insert_material_price(
								vl_preco_p				=>	r_cMateriais.vl_preco, 
								dt_inicio_vigencia_p	=>	to_date(r_cMateriais.dt_vigencia,'dd/MM/yyyy'), 
								cd_tab_preco_mat_p		=>	cd_tab_preco_mat_p, 
								cd_material_glo_p		=>	r_cMateriais.cd_registro, 
								nm_usuario_p			=>	nm_usuario_p,
								cd_estabelecimento_p	=>	cd_estabelecimento_p);

		end;
	end loop;

	begin
		bft_global_price_pck.insert_tab_material_price_hist(
								cd_tab_preco_mat_p		=>	cd_tab_preco_mat_p,
								cd_estabelecimento_p	=>	cd_estabelecimento_p,
								nr_documento_p			=>	nr_ultimo_log_w,
								ie_tipo_p				=>	'C',
								nm_usuario_p			=>	nm_usuario_p);

	end;

commit;

gravar_processo_longo('','',0);
end if;
end processar_xml_alfabeta;
/