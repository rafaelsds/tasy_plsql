create or replace
procedure liberar_ficha_notificacao(	nr_sequencia_p		number,
				nm_usuario_p		varchar2) is


begin

update	notificacao_sinan 
set 	dt_liberacao = sysdate, 
	nm_usuario_liberacao	= nm_usuario_p
where 	nr_sequencia		= nr_sequencia_p;

commit;

end liberar_ficha_notificacao;
/