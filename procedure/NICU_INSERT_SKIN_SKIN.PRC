create or replace procedure nicu_insert_skin_skin(nr_sequencia_p      in nicu_skin_skin.nr_sequencia%type,
                                                  nr_seq_encounter_p  in nicu_skin_skin.nr_seq_encounter%type,
                                                  dt_session_date_p   in nicu_skin_skin.dt_session_date%type,
                                                  qt_duration_p       in nicu_skin_skin.qt_duration%type,
                                                  nm_usuario_p        in nicu_skin_skin.nm_usuario%type,
                                                  mensagem_erro_p     out varchar2) is
  
  nr_sequencia_skin_skin_w nicu_skin_skin.nr_sequencia%type;
  exeption_w               exception;
  exit_w                   exception;
begin
  if (qt_duration_p < 1) then
    raise exit_w;
  end if;
  
  if (nr_sequencia_p is null) then
    --Busca novo sequencial
    select nicu_skin_skin_seq.nextval
      into nr_sequencia_skin_skin_w
      from dual;
  else
    nr_sequencia_skin_skin_w := nr_sequencia_p;
  end if;

  begin
    insert into nicu_skin_skin
       (nr_sequencia,
        nr_seq_encounter,
        dt_session_date,
        qt_duration,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec)
    values
       (nr_sequencia_skin_skin_w,
        nr_seq_encounter_p,
        dt_session_date_p,
        qt_duration_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p);
  exception
    when dup_val_on_index then
      begin
        update nicu_skin_skin
           set nr_seq_encounter = nr_seq_encounter_p,
               dt_session_date = nvl(dt_session_date_p, dt_session_date),
               qt_duration = qt_duration_p,
               dt_atualizacao = sysdate,
               nm_usuario = nm_usuario_p,
               dt_atualizacao_nrec = sysdate,
               nm_usuario_nrec = nm_usuario_p
         where nr_sequencia = nr_sequencia_skin_skin_w;
      exception
        when others then
          mensagem_erro_p := expressao_pck.obter_desc_expressao(776218)||' Error: '|| sqlerrm;
          raise exeption_w;
      end;
    when others then
      mensagem_erro_p := expressao_pck.obter_desc_expressao(776218)||' Error: '|| sqlerrm;
      raise exeption_w;
  end;

  commit;
exception
  when exit_w then
    null;
  when exeption_w then
    rollback;
  when others then
    mensagem_erro_p := expressao_pck.obter_desc_expressao(776218)||' Error: '|| sqlerrm;
    rollback;
end nicu_insert_skin_skin;
/
