create or replace
procedure pls_gerar_titulo_liq_fat (	nr_seq_fatura_p		in pls_fatura.nr_sequencia%type,
					nm_usuario_p		in usuario.nm_usuario%type,
					cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type) is
					
ie_gerar_tit_liq_w		varchar2(1);
nr_titulo_fatura_w		pls_fatura.nr_titulo%type;
cd_tipo_portador_w		parametro_contas_receber.cd_tipo_portador%type;
cd_portador_w			parametro_contas_receber.cd_portador%type;
tx_juros_cr_w			parametro_contas_receber.pr_juro_padrao%type;
tx_multa_cr_w			parametro_contas_receber.pr_multa_padrao%type;
cd_tipo_taxa_juro_cr_w		parametro_contas_receber.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_cr_w		parametro_contas_receber.cd_tipo_taxa_multa%type;
cd_moeda_cr_w			parametro_contas_receber.cd_moeda_padrao%type;
vl_glosa_w			titulo_receber_liq.vl_glosa%type;
nr_titulo_w			titulo_receber.nr_titulo%type;

begin

select	decode(count(1), 0, 'N', 'S'),
	max(a.nr_titulo)
into	ie_gerar_tit_liq_w,
	nr_titulo_fatura_w
from	pessoa_juridica		d,
	titulo_receber_liq	c,
	titulo_receber		b,
	pls_fatura		a
where	a.nr_titulo		= b.nr_titulo
and	b.nr_titulo		= c.nr_titulo
and	b.cd_cgc		= d.cd_cgc
and	a.nr_sequencia		= nr_seq_fatura_p
and	c.vl_glosa > 0
and	sysdate between b.dt_pagamento_previsto + 1 and b.dt_pagamento_previsto + 16
and	a.nr_titulo_liq is null
and	b.ie_situacao		= '1'
and not exists (select	1
		from	pls_lote_contestacao	cc,
			pls_fatura		bb,
			titulo_receber		aa
		where	aa.nr_titulo		= b.nr_titulo
		and	aa.nr_titulo		= bb.nr_titulo
		and	bb.nr_sequencia		= cc.nr_seq_pls_fatura
		and	aa.ie_situacao <> '3');
		
if	(ie_gerar_tit_liq_w = 'S') then
	begin
		select	cd_tipo_portador,
			cd_portador,
			pr_juro_padrao,
			pr_multa_padrao,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			cd_moeda_padrao
		into	cd_tipo_portador_w,
			cd_portador_w,
			tx_juros_cr_w,
			tx_multa_cr_w,
			cd_tipo_taxa_juro_cr_w,
			cd_tipo_taxa_multa_cr_w,
			cd_moeda_cr_w
		from	parametro_contas_receber
		where	cd_estabelecimento = cd_estabelecimento_p;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(172762);
	end;
	
	select	sum(vl_glosa)
	into	vl_glosa_w
	from	titulo_receber_liq
	where	nr_titulo = nr_titulo_fatura_w;
	
	select	titulo_seq.nextval
	into	nr_titulo_w
	from	dual;
	
	insert into titulo_receber (	nr_titulo,			nm_usuario,			dt_atualizacao,			cd_estabelecimento,
					cd_tipo_portador,		cd_portador,			dt_emissao,			dt_contabil,
					dt_vencimento,			dt_pagamento_previsto,		vl_titulo,			vl_saldo_titulo,
					vl_saldo_juros,			vl_saldo_multa,			tx_juros,			tx_multa,
					cd_tipo_taxa_juro,		cd_tipo_taxa_multa,		tx_desc_antecipacao,		ie_tipo_titulo,
					ie_tipo_inclusao,		ie_origem_titulo,		cd_moeda,			ie_situacao,
					cd_cgc,				ie_tipo_emissao_titulo,		nr_lote_contabil,		nr_seq_conta_banco,
					cd_serie,			nr_seq_carteira_cobr,		nr_seq_classe,			nr_seq_trans_fin_baixa,
					nr_seq_trans_fin_contab,	ie_pls, 			nr_nota_fiscal,			nr_seq_pagador,
					nr_seq_pls_fatura)
				(select	nr_titulo_w,			nm_usuario_p,			sysdate,			cd_estabelecimento_p,
					cd_tipo_portador_w,		cd_portador_w,			sysdate,			sysdate,
					sysdate + 15,			sysdate + 15,			vl_glosa_w,			vl_glosa_w,
					0,				0,				tx_juros_cr_w,			tx_multa_cr_w,
					cd_tipo_taxa_juro_cr_w,		cd_tipo_taxa_multa_cr_w,	0,				'15',
					'2',				'13', 				cd_moeda_cr_w,			'1',
					cd_cgc,				'2',				0,				nr_seq_conta_banco,
					cd_serie,			nr_seq_carteira_cobr,		nr_seq_classe,			nr_seq_trans_fin_baixa,
					nr_seq_trans_fin_contab,	'S',				nr_nota_fiscal,			nr_seq_pagador,
					nr_seq_fatura_p
				from	titulo_receber
				where	nr_titulo = nr_titulo_fatura_w);
				
	update	pls_fatura
	set	nr_titulo_liq	= nr_titulo_w
	where	nr_sequencia	= nr_seq_fatura_p;
	
	liquidar_titulo_receber(nm_usuario_p, nr_titulo_fatura_w, sysdate);
end if;

commit;

end pls_gerar_titulo_liq_fat;
/