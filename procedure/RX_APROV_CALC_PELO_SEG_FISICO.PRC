create or replace
procedure RX_APROV_CALC_PELO_SEG_FISICO(
			nr_sequencia_p	number,
			ie_tipo_tratamento_p	varchar2) is
begin

if (ie_tipo_tratamento_p = 'AL') then
	update	rxt_fase_tratamento
	set	dt_lib_seg_fisico		=	sysdate,
		nm_usuario_seg_fisico	=	wheb_usuario_pck.get_nm_usuario
	where	nr_sequencia	        	=	nr_sequencia_p;
	commit;

    RXT_GRAVA_LOG_LIBERACAO_FISIC(nr_sequencia_p, null,null,'LS');

else
	update	rxt_braq_aplic_trat
	set	dt_lib_seg_fisico		=	sysdate,
		nm_usuario_seg_fisico	=	wheb_usuario_pck.get_nm_usuario
	where	nr_sequencia	        	=	nr_sequencia_p;
	commit;

    RXT_GRAVA_LOG_LIBERACAO_FISIC(null, nr_sequencia_p ,null, 'LS');
end if;
rxt_atualizar_rxt_campo(nr_sequencia_p);
/*
ie_tipo_log
LF	Libera��o do primeiro f�sico
LM	Libera��o do m�dico
LS	Libera��o do segundo f�sico
RM	Reprova��o do m�dico
RS	Reprova��o do segundo f�sico
*/
end RX_APROV_CALC_PELO_SEG_FISICO;
/ 