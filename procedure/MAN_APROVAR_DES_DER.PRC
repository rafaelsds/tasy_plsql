create or replace
procedure man_aprovar_des_der
		(	nr_seq_der_p		Number,
			nm_usuario_p		Varchar2) is 
			
begin

update	des_der
set	dt_aprovacao	= sysdate, 
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_der_p;

commit;

end man_aprovar_des_der;
/