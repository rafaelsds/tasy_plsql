create or replace
procedure gerar_requisicao_prod_medic(
					nr_lote_producao_p				number,
					cd_estabelecimento_p			number,
					nm_usuario_p					varchar2) is 

nr_requisicao_w					number(10,0);
cd_operacao_transf_setor_w		number(3,0);
cd_local_destino_w				varchar2(4) := null;
cd_local_origem_w				varchar2(4) := null;
cd_oper_estoque_w				number(3);
cd_setor_atend_w				number(5);
cd_local_estoque_w				number(4);
cd_centro_custo_w				number(8);
ie_tipo_conta_w					number(1);
cd_conta_contabil_w				varchar2(20);
nr_sequencia_w					number(10);
cd_unid_med_consumo_w			varchar2(30);
cd_unid_med_estoque_w			varchar2(30);
qt_conv_estoque_consumo_w 		number(13,4);
qt_existe_w						number(10);
cd_material_w					item_requisicao_material.cd_material%type;
qt_prevista_w					lote_producao_comp.qt_prevista%type;

cursor c01 is
	select 	cd_material,
			qt_prevista
	from 	lote_producao_comp 
	where 	nr_lote_producao = nr_lote_producao_p;

begin

Obter_Param_Usuario(9044, 27, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, cd_local_origem_w);

select	cd_local_estoque
into	cd_local_destino_w
from	lote_producao
where	nr_lote_producao = nr_lote_producao_p;

select	count(*)
into	qt_existe_w
from	local_estoque
where	cd_local_estoque = cd_local_destino_w;

if	(qt_existe_w = 0) then
	--Para gerar a requisi��o, deve ser informado o c�digo de local de estoque no Lote de Produ��o
	Wheb_mensagem_pck.exibir_mensagem_abort(793283);
end if;

select	count(*)
into	qt_existe_w
from	local_estoque
where	cd_local_estoque = cd_local_origem_w;

if	(qt_existe_w = 0) then
	--Para gerar a requisi��o, deve ser informado o c�digo de local de estoque baixa no par�metro [27]
	Wheb_mensagem_pck.exibir_mensagem_abort(793284);
end if;

if	(cd_local_origem_w = cd_local_destino_w) then
	--O local de estoque destino deve ser diferente do local de estoque origem. Veja par�metro [27] e local estoque no Lote de Produ��o
	Wheb_mensagem_pck.exibir_mensagem_abort(793285);
end if;

select	nvl(max(cd_operacao_transf_setor), 0)
into 	cd_operacao_transf_setor_w
from 	parametro_estoque
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(cd_operacao_transf_setor_w = 0) then
	--A requisi��o n�o pode ser gerada!' || chr(13) || 'Deve estar definido a opera��o de transfer�ncia de setor nos par�metros de estoque
	Wheb_mensagem_pck.exibir_mensagem_abort(261627);
end if;

select	requisicao_seq.nextval
into	nr_requisicao_w
from	dual;

begin
insert into requisicao_material(
	nr_requisicao,
	cd_estabelecimento,
	cd_local_estoque,
	dt_solicitacao_requisicao,
	dt_atualizacao,
	nm_usuario,
	cd_operacao_estoque,
	cd_pessoa_requisitante,
	cd_estabelecimento_destino,
	cd_local_estoque_destino,
	cd_setor_atendimento,
	ie_urgente,
	ie_geracao)
values(	nr_requisicao_w,
	cd_estabelecimento_p,
	decode(nvl(campo_numerico(cd_local_origem_w),0),0,null,nvl(campo_numerico(cd_local_origem_w),0)),
	sysdate,
	sysdate,
	nm_usuario_p,
	cd_operacao_transf_setor_w,
	Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C'),
	cd_estabelecimento_p,
	decode(nvl(campo_numerico(cd_local_destino_w),0),0,null,nvl(campo_numerico(cd_local_destino_w),0)),
	null,
	'N',
	'A');
exception when others then
	--Erro ao gravar requisi��o');
	Wheb_mensagem_pck.exibir_mensagem_abort(261628);
end;

open c01;
loop
fetch c01 into	
	cd_material_w,
	qt_prevista_w;
exit when c01%notfound;

	select	cd_operacao_estoque,
			cd_setor_atendimento,
			cd_local_estoque,
			cd_centro_custo
	into	cd_oper_estoque_w,
			cd_setor_atend_w,
			cd_local_estoque_w,
			cd_centro_custo_w
	from	requisicao_material
	where	nr_requisicao = nr_requisicao_w;

	select	decode(ie_tipo_requisicao,2,2,3)
	into	ie_tipo_conta_w
	from	operacao_estoque
	where	cd_operacao_estoque = cd_oper_estoque_w;

	define_conta_material(
			cd_estabelecimento_p,
			cd_material_w,
			ie_tipo_conta_w, 0,
			cd_setor_atend_w, '0', 0, 0, 0, 0,
			cd_local_estoque_w,
			Null,
			sysdate,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null);
			
	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
			substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
			qt_conv_estoque_consumo
	into	cd_unid_med_consumo_w,
			cd_unid_med_estoque_w,
			qt_conv_estoque_consumo_w
	from	material
	where	cd_material = cd_material_w;			

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_w;

	insert into item_requisicao_material(
		nr_requisicao,
		nr_sequencia,
		cd_estabelecimento,
		cd_material,
		qt_material_requisitada,
		qt_material_atendida,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		cd_unidade_medida,
		dt_atendimento,
		cd_pessoa_recebe,
		cd_pessoa_atende,
		ie_acao,
		cd_motivo_baixa,
		qt_estoque,
		cd_unidade_medida_estoque,
		cd_conta_contabil,
		cd_material_req,
		ds_observacao,
		ie_geracao)
	values(	nr_requisicao_w,
		nr_sequencia_w,
		cd_estabelecimento_p,
		cd_material_w,
		qt_prevista_w,
		0,
		0,
		sysdate,
		nm_usuario_p,
		cd_unid_med_consumo_w,
		null,
		null,
		null,
		'1',
		0,
		(qt_prevista_w / qt_conv_estoque_consumo_w),
		cd_unid_med_estoque_w,
		cd_conta_contabil_w,
		cd_material_w,
		null,
		null);

end loop;
close c01;

commit;

end gerar_requisicao_prod_medic;
/