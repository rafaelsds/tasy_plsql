create or replace
procedure Nut_atualizar_qt_real(nr_sequencia_p		Number,
				qt_real_p		Number,
				nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) and (qt_real_p is not null) then

	update	nut_rec_real_comp
	set	qt_componente_real = qt_real_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;

end if;

commit;

end Nut_atualizar_qt_real;
/