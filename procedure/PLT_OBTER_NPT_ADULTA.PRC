create or replace
procedure plt_obter_npt_adulta (
				nm_usuario_p			varchar2,
				nr_atendimento_p		number,
				dt_inicial_horarios_p		date,
				dt_final_horarios_p		date,					
				dt_validade_limite_p		date,
				ie_prescr_usuario_p		varchar2,
				nr_seq_regra_p			number) is

nr_seq_wadep_w			number(10,0);
nr_prescricao_w			number(14,0);
nr_seq_npt_w			number(10,0);
ds_npt_w			varchar2(255);
ie_status_solucao_w		varchar2(3);
ie_lib_pend_rep_w		varchar2(1);
ie_liberado_w			varchar2(1);
ie_copiar_w			varchar2(1);
ds_cor_titulo_w		varchar2(20);
					
cursor c01 is
select	nr_prescricao,
	nr_seq_npt,
	ds_npt,
	ie_status_solucao,
	ie_lib_pend_rep,
	ie_liberado
from	(
	select	a.nr_prescricao,
		x.nr_sequencia nr_seq_npt,
		decode(x.ie_forma, 'P', obter_desc_expressao(718006)|| y.ds_npt, obter_valor_dominio(1988,x.ie_forma)) ds_npt,
		substr(obter_status_solucao_prescr(6,a.nr_prescricao,x.nr_sequencia),1,3) ie_status_solucao,
		substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
		decode(nvl(nvl(a.dt_liberacao,a.dt_liberacao_medico),a.dt_liberacao_farmacia),null,'N','S') ie_liberado
	from	protocolo_npt y,
		nut_pac x,
		prescr_medica a
	where	y.nr_sequencia(+) = x.nr_seq_protocolo
	and	x.nr_prescricao = a.nr_prescricao
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	x.ie_npt_adulta = 'S'
	and	nvl(a.ie_adep,'S') = 'S'
	and	((x.ie_status in ('I','INT')) or
		 ((obter_se_acm_sn(null,null) = 'N') and (x.dt_status between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 ((obter_se_acm_sn(null,null) = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	(((a.dt_liberacao_medico is not null) and
		  (ie_prescr_usuario_p = 'N')) or
		 (a.nm_usuario_original = nm_usuario_p))
	group by
		a.nr_prescricao,
		x.nr_sequencia,
		x.ie_forma,
		y.ds_npt,
		a.dt_liberacao_medico,
		a.dt_liberacao,
		a.dt_liberacao_farmacia
	)
group by
	nr_prescricao,
	nr_seq_npt,
	ds_npt,
	ie_status_solucao,
	ie_lib_pend_rep,
	ie_liberado;

begin

ie_copiar_w 	:= plt_obter_se_item_marcado('NAN', nr_seq_regra_p);
ds_cor_titulo_w	:= plt_obter_se_item_marcado('NAN', nr_seq_regra_p);

open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_npt_w,
		ds_npt_w,
		ie_status_solucao_w,
		ie_lib_pend_rep_w,
		ie_liberado_w;
exit when c01%notfound;
	begin
	select	w_rep_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_rep_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,
		ie_diferenciado,
		nr_agrupamento,
		nr_seq_proc_interno,
		ie_status_item,
		ie_pendente_liberacao,
		nr_prescricoes,
		ie_liberado,
		ie_copiar,
		ds_cor_titulo)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'NAN',
		nr_prescricao_w,
		nr_seq_npt_w,
		nr_seq_npt_w,
		ds_npt_w,
		'N',
		'N',
		0,
		0,
		ie_status_solucao_w,
		ie_lib_pend_rep_w,
		nr_prescricao_w,
		ie_liberado_w,
		ie_copiar_w,
		ds_cor_titulo_w);
	end;
end loop;
close c01;

end plt_obter_npt_adulta;
/