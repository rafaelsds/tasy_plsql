create or replace
procedure REP_copia_sup_nutricional(	nr_prescricao_orig_p	number,
					nr_prescricao_p		number,
					nr_seq_regra_p		number,
					nm_usuario_p		Varchar2) is 

nr_seq_nut_pac_w	number(15,0);
nr_seq_nut_pac_ww	number(15,0);
ie_copiar_w		varchar2(10);
ie_copia_valid_igual_w	varchar2(1);
dt_prescricao_www	date;
dt_primeiro_horario_w	date;
dt_inicio_prescr_w	date;
nr_horas_validade_w	number(5);
ie_prim_horario_setor_w	varchar2(10);
hr_setor_w		varchar2(10);
cd_setor_atendimento_w	number(10);
cd_estabelecimento_w	Number(4,0);
nr_atendimento_w	number(15,0);
dt_validade_origem_w	date;
dt_validade_nova_w	date;

cursor	c02 is
select	nr_sequencia
from	nut_paciente
where	nr_prescricao = nr_prescricao_orig_p
and	nr_sequencia = (select	min(b.nr_sequencia)
			from	nut_paciente b
			where	b.nr_prescricao = nr_prescricao_orig_p);

begin

Select	Nut_Paciente_seq.nextval
into	nr_seq_nut_pac_w
from	dual;

Insert	into Nut_Paciente(
	nr_sequencia,
	nr_atendimento,
	dt_atualizacao,
	nm_usuario,
	qt_peso,
	qt_idade_ano,
	qt_altura_cm,
	nr_seq_fator_ativ,
	nr_seq_fator_stress,
	pr_proteina,
	pr_lipidio,
	pr_carboidrato,
	qt_kcal_total,
	qt_kcal_prot,
	qt_kcal_lipidio,
	qt_kcal_carboidrato,
	qt_kcal_kg,
	qt_grama_prot_kg_dia,
	pr_npt,
	qt_fase_npt,
	qt_gotejamento_npt,
	nr_prescricao,
	ie_bomba_infusao,
	ie_npt_adulta,
	ie_suspenso,
	nr_prescricao_original,
	IE_CALC_AUTOMATICO,
	QT_VOLUME_TOTAL)
select	nr_seq_nut_pac_w,
	nr_atendimento,
	sysdate,
	nm_usuario_p,
	qt_peso,
	qt_idade_ano,
	qt_altura_cm,
	nr_seq_fator_ativ,
	nr_seq_fator_stress,
	pr_proteina,
	pr_lipidio,
	pr_carboidrato,
	qt_kcal_total,
	qt_kcal_prot,
	qt_kcal_lipidio,
	qt_kcal_carboidrato,
	qt_kcal_kg,
	qt_grama_prot_kg_dia,
	pr_npt,
	qt_fase_npt,
	qt_gotejamento_npt,
	nr_prescricao_p,
	ie_bomba_infusao,
	ie_npt_adulta,
	'N',
	nr_prescricao_orig_p,
	IE_CALC_AUTOMATICO,
	QT_VOLUME_TOTAL
from	nut_paciente
where	nr_prescricao = nr_prescricao_orig_p
and	nr_sequencia = (select	min(b.nr_sequencia)
			from	nut_paciente b
			where	b.nr_prescricao = nr_prescricao_orig_p);

open c02;
loop
fetch c02 into
	nr_seq_nut_pac_ww;
exit when c02%notfound;
	
	Insert	into Nut_Paciente_Elemento(
		nr_sequencia,
		nr_seq_nut_pac,
		nr_seq_elemento,
		dt_atualizacao,
		nm_usuario,
		qt_kcal,
		qt_elemento,
		nr_seq_elem_unid_med,
		nr_seq_elem_mat,
		qt_volume,
		cd_unid_med_prescr,
		qt_prescricao,
		qt_dispensar,
		qt_vol_1_fase,
		qt_vol_2_fase,
		qt_vol_3_fase)
	select	nut_paciente_elemento_seq.nextval,
		nr_seq_nut_pac_w,
		nr_seq_elemento,
		sysdate,
		nm_usuario_p,
		qt_kcal,
		qt_elemento,
		nr_seq_elem_unid_med,
		nr_seq_elem_mat,
		qt_volume,
		cd_unid_med_prescr,
		qt_prescricao,
		qt_dispensar,
		qt_vol_1_fase,
		qt_vol_2_fase,
		qt_vol_3_fase
	from	nut_paciente_elemento
	where	nr_seq_nut_pac = nr_seq_nut_pac_ww;

end loop;
close c02;

commit;

end REP_copia_sup_nutricional;
/
