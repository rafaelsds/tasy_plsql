create or replace
procedure definir_fim_amamentacao_hist  	(	nr_sequencia_p     		number,
						dt_fim_p			date,
						ds_observacao_p		varchar2,
						cd_pessoa_fisica_p		varchar2,
						ie_finalizar_todos_p		varchar2) is
		
nr_sequencia_w		historico_saude_mulher.nr_sequencia%type;	

Cursor C01 is
	select	nr_sequencia
	from	historico_saude_mulher
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_inicio_amamentacao is not null
	and	dt_fim_amamentacao is null
	and	dt_inativacao is null;

begin

	if (ie_finalizar_todos_p = 'S') then

		open C01;
		loop
		fetch C01 into	
			nr_sequencia_w;
		exit when C01%notfound;
			begin
			
				update 	historico_saude_mulher
				set 	dt_fim_amamentacao 	= dt_fim_p,
					ds_observacao		= substr(ds_observacao||decode(ds_observacao,Null,Null,chr(13)||chr(10))||ds_observacao_p,1,255)
				where 	nr_sequencia 		= nr_sequencia_w;
				
			end;
		end loop;
		close C01;
		
	else

		update 	historico_saude_mulher
		set 	dt_fim_amamentacao  	= dt_fim_p,
			ds_observacao		= substr(ds_observacao||decode(ds_observacao,Null,Null,chr(13)||chr(10))||ds_observacao_p,1,255)
		where 	nr_sequencia 		= nr_sequencia_p;

	end if;

commit;

end definir_fim_amamentacao_hist;
/
