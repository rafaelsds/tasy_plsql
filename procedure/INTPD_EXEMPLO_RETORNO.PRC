create or replace 
procedure intpd_exemplo_retorno(	nr_sequencia_p	number) is

ds_log_w clob;
cd_pessoa_fisica_w	varchar2(2000);

paciente 			philips_json;
email				philips_json_list;
complemento_list	philips_json_list;
complemento			philips_json;

ds_erro_w				varchar2(2000);
ds_email_w				varchar2(2000);
nr_sequencia_compl_w	number(10);

i number(10);




/*
M�todo POST
{
	"IE_TIPO_PESSOA" 		: 2, 
	"NM_PESSOA_FISICA" 		: "Teste REST2",
	"DT_NASCIMENTO" 		: "12/06/2017",
	"IE_SEXO" 				: "M",
	"NR_CPF" 				: "24429880034",
	"NR_IDENTIDADE" 		: "278830699",
	"NR_TELEFONE_CELULAR" 	: "9999999",
	"CD_ESTABELECIMENTO" 	: 1,
	"IE_FUNCIONARIO"		: "S",
	"IE_ESTADO_CIVIL"		: "C",
	"EMAIL" : ["Email: A1@PHILIPS.COM","Email: A2@PHILIPS.COM"],
	"COMPL_PESSOA_FISICA":	
		[
			{
				"IE_TIPO_COMPLEMENTO" : 1,
				"NR_ENDERECO" : 71,
				"DS_ENDERECO" : "Rua de teste"
 			},
 			{
				"IE_TIPO_COMPLEMENTO" : 2,
				"NR_ENDERECO" : 44,
				"DS_ENDERECO" : "Avenida principal"
 			}
		]
}

*/



begin

select	ds_message
into	ds_log_w
from	intpd_fila_transmissao a
where	a.nr_sequencia = nr_sequencia_p;

paciente			:= philips_json(ds_log_w);
email				:= philips_json_list(paciente.get('EMAIL'));
complemento_list	:= philips_json_list(paciente.get('COMPL_PESSOA_FISICA'));


FOR i IN 1..email.count LOOP
	ds_email_w	:= ds_email_w||email.get(i).get_string()||';'||chr(13)||chr(10);
END LOOP;
 

select	pessoa_fisica_seq.nextval
into	cd_pessoa_fisica_w
from	dual;


begin
insert into pessoa_fisica(	CD_PESSOA_FISICA,
							IE_TIPO_PESSOA,                         
							NM_PESSOA_FISICA,
							DT_ATUALIZACAO,
							NM_USUARIO,
							DT_NASCIMENTO,
							IE_SEXO,
							NR_CPF,
							NR_IDENTIDADE,
							NR_TELEFONE_CELULAR,							
							CD_ESTABELECIMENTO,
							DT_ATUALIZACAO_NREC,
							NM_USUARIO_NREC,
							ie_funcionario,
							ie_estado_civil,
							ds_observacao
						)
				values	(
							cd_pessoa_fisica_w,
							paciente.get('IE_TIPO_PESSOA').get_number(),
							paciente.get('NM_PESSOA_FISICA').get_string(),
							sysdate,
							'teste',
							to_date(paciente.get('DT_NASCIMENTO').get_string()),
							paciente.get('IE_SEXO').get_string(),
							paciente.get('NR_CPF').get_string(),
							paciente.get('NR_IDENTIDADE').get_string(),														
							paciente.get('NR_TELEFONE_CELULAR').get_string(),							
							paciente.get('CD_ESTABELECIMENTO').get_number(),
							sysdate,
							'teste',							
							paciente.get('IE_FUNCIONARIO').get_string(),
							paciente.get('IE_ESTADO_CIVIL').get_string(),
							ds_email_w
						);

	FOR i IN 1..complemento_list.count LOOP
		complemento	:= philips_json(complemento_list.get(i));
		
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_compl_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		insert into compl_pessoa_fisica(	nr_sequencia,
											cd_pessoa_fisica,
											ie_tipo_complemento,
											ds_endereco,
											nr_endereco,
											nm_usuario,
											nm_usuario_nrec,
											dt_atualizacao,
											dt_atualizacao_nrec											
											)
										values(
											nr_sequencia_compl_w,
											cd_pessoa_fisica_w,
											complemento.get('IE_TIPO_COMPLEMENTO').get_number(),											
											complemento.get('DS_ENDERECO').get_string(),
											complemento.get('NR_ENDERECO').get_number(),
											'teste',
											'teste',
											sysdate,
											sysdate
										);
	END LOOP;
	

	update intpd_fila_transmissao
	set	ie_status_http = 200
	where nr_sequencia = nr_sequencia_p;
exception
	when others then
		begin
		ds_erro_w	:= SQLERRM(sqlcode);

		
		update 	intpd_fila_transmissao
		set		ie_status_http = 400,
				ds_message_response = '{ "erro" : "'||ds_erro_w||'"}'
		where 	nr_sequencia = nr_sequencia_p;
		end;
end;

commit;

end intpd_exemplo_retorno;
/