create or replace
procedure Alterar_obs_alta_EUP (nr_atendimento_p		in out number,
								cd_motivo_alta_p		out number,
								dt_alta_p				out date,
								nm_usuario_alta_p		out varchar2,
								dt_atualizacao_p		out date,
								ds_obs_alta_p			out varchar2,
								ie_necropsia_p			out varchar2,
								ie_tipo_atendimento_p	out varchar2,
								ie_obito_p				out varchar2,
								cd_setor_obito_p		out number,
								nr_seq_motivo_p			out number,
								cd_especialidade_p		out number,
								cd_cgc_p				out number,
								nm_usuario_p			in out varchar2) is 

begin

select	cd_motivo_alta,
		dt_alta,
		nr_atendimento,
		nm_usuario_alta,
		nm_usuario,
		dt_atualizacao,
		ds_obs_alta,
		ie_necropsia,
		ie_tipo_atendimento
into	cd_motivo_alta_p,
		dt_alta_p,
		nr_atendimento_p,
		nm_usuario_alta_p,
		nm_usuario_p,
		dt_atualizacao_p,
		ds_obs_alta_p,
		ie_necropsia_p,
		ie_tipo_atendimento_p
from    atendimento_paciente
where   nr_atendimento = nr_atendimento_p;

ie_obito_p := obter_se_alta_obito(nr_atendimento_p);

select 	nvl(max(cd_setor_obito),0)
into	cd_setor_obito_p
from 	atendimento_paciente 
where 	nr_atendimento = nr_atendimento_p;

select	nvl(max(nr_seq_motivo),0) nr_seq_motivo,
		nvl(max(cd_especialidade),0) cd_especialidade,
		max(cd_cgc) cd_cgc
into	nr_seq_motivo_p,
		cd_especialidade_p,
		cd_cgc_p
from	atendimento_transf 
where	nr_atendimento = nr_atendimento_p;

commit;

end Alterar_obs_alta_EUP;
/
