create or replace
procedure copiar_pat_grupo_tipo(	cd_empresa_p		number,
				cd_estabelecimento_p	number,
				nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

cd_estabelecimento_w		number(10);
ds_grupo_w			varchar2(255);
nr_seq_grupo_w			number(10);
nr_sequencia_w			number(10);

cursor c01 is
select	cd_estabelecimento
from	estabelecimento
where	cd_empresa	= cd_empresa_p
and	cd_estabelecimento	<> cd_estabelecimento_p
and	ie_situacao	= 'A';

cursor c02 is
select	nr_sequencia,
	ds_grupo
from	pat_grupo_tipo
where	cd_estabelecimento	= cd_estabelecimento_p
and	nr_sequencia	= nvl(nr_sequencia_p,nr_sequencia);


begin

open C01;
loop
fetch C01 into	
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_grupo_w,
		ds_grupo_w;
	exit when C02%notfound;
		begin
		
		select	pat_grupo_tipo_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert into pat_grupo_tipo(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao, 
			nm_usuario,
			ds_grupo,
			ie_situacao, 
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_empresa)
		values(	nr_Sequencia_w,
			cd_estabelecimento_w,
			sysdate,
			nm_usuario_p,
			ds_grupo_w,
			'A',
			sysdate,
			nm_usuario_p,
			cd_empresa_p);
	
		insert into pat_tipo_bem(
			nr_sequencia,
			nr_seq_grupo,
			dt_atualizacao, 
			nm_usuario,
			ds_tipo,
			ie_situacao, 
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		select	pat_tipo_bem_seq.nextval,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			ds_tipo,
			'A',
			sysdate,
			nm_usuario_p
		from	pat_tipo_bem
		where	nr_seq_grupo	= nr_seq_grupo_w;
		end;
	end loop;
	close C02;
	
		
	end;
end loop;
close C01;

commit;

end copiar_pat_grupo_tipo;
/