create or replace procedure SET_INTERV_PRESC_GRUPOSUBGRUPO(NR_SEQ_SUBGRUPO_P number, NR_SEQ_INTERVALO_P Varchar2) is 
begin
    INSERT INTO INTERV_PRESC_GRUPOSUBGRUPO 
        (DT_ATUALIZACAO,
        DT_ATUALIZACAO_NREC,
        NM_USUARIO,
        NM_USUARIO_NREC,
        NR_SEQ_INTERVALO,
        NR_SEQ_SUBGRUPO,
        NR_SEQUENCIA)
    VALUES (
        sysdate,
        sysdate,
        wheb_usuario_pck.get_nm_usuario,
        wheb_usuario_pck.get_nm_usuario,
        NR_SEQ_INTERVALO_P,
        NR_SEQ_SUBGRUPO_P,
        INTERV_PRESC_GRUPOSUBGRUPO_SEQ.nextval
    );

    commit;
end SET_INTERV_PRESC_GRUPOSUBGRUPO;
/
