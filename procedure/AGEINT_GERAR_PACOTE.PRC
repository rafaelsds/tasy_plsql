Create or Replace
procedure ageint_gerar_pacote        (
					nr_seq_ageint_p		number,
                                        ds_pacote_p 	      	varchar2, 
                                        nm_usuario_p          	varchar2,
					cd_estabelecimento_p	number) is

ie_tipo_acomod_w                varchar2(2);

cd_procedimento_w               number(15);
ie_origem_proced_w              number(10,0);
vl_honorario_w                  number(15,2);
vl_pacote_w                     number(15,2);
qt_dias_pacote_w                number(5);
vl_anestesista_w                number(15,2);
qt_reg_existe_w                 number(10,0);
cd_material_w                   number(10,0);
ds_documento_w                  long; 
ie_procedimento_regra_w         varchar2(1):= 'S';
ie_material_regra_w             varchar2(1):= 'S';
qt_limite_w                     number(10,0);
ie_qtde_limite_pacote_w         varchar2(1);

nr_sequencia_w                  number(10);
NR_SEQ_PAC_ACOMOD_w             number(10);
ie_insere_w                     varchar2(1):= 'S';

nr_seq_orc_proced_w             number(10,0);
ie_gera_todos_acomodacao_w	varchar2(1):= 'S';
pr_orcamento_w			number(15,4);

nr_seq_proc_interno_w		number(10);
nr_seq_proc_pacote_w		number(10);
nr_seq_pacote_w			number(10);

tam_lista_w		number(10);
ie_pos_virgula_w	number(3)	:= 0;
ds_pacote_w		varchar2(255);
ie_agendavel_w			varchar2(1);
ie_pacote_w		varchar2(1);
ie_tipo_agendamento_w	varchar2(15);
ie_tipo_convenio_w	number(2);

nr_seq_ageint_check_list_w	number(10,0);
ie_gerar_check_list_w		varchar2(1);

cursor c01 is
        select  a.cd_procedimento,
                a.ie_origem_proced,
                a.vl_honorario,
                a.vl_pacote, 
                a.qt_dias_pacote,
                a.vl_anestesista,
                1,
                a.nr_sequencia,
		a.nr_seq_proc_interno,
		nvl(c.ie_agendavel, 'N')
        from    pacote_tipo_acomodacao a,
		pacote c
        where   a.nr_seq_pacote   = nr_seq_pacote_w
	and	a.nr_seq_pacote	= c.nr_seq_pacote
	and	Ageint_Obter_Se_Pacote_Acomod(a.ie_tipo_acomod) = 'S'
        and     nvl(a.dt_vigencia,sysdate) = (
                                select  max(nvl(dt_vigencia,sysdate)) 
                                from    Pacote_Tipo_Acomodacao b
                                where   nr_seq_pacote                   = nr_seq_pacote_w
                                and     nvl(ie_situacao, 'A')           = 'A'
                                and     b.cd_procedimento               = a.cd_procedimento
                                and     b.ie_origem_proced              = a.ie_origem_proced);


cursor c02 is
        select  cd_material,
                nvl(qt_limite,1),
                NR_SEQ_PAC_ACOMOD,
		pr_orcamento
        from    pacote_material
        where   nr_seq_pacote   = nr_seq_pacote_w
        and     ie_inclui_exclui = 'I'
        and     nvl(ie_material_regra_w,'S') = 'S'
        and     cd_material is not null;
 
cursor c05 is
        select  cd_procedimento,
                ie_origem_proced,
                0,
                0,
                0,
                0,
                nvl(qt_limite,1),
                NR_SEQ_PAC_ACOMOD,
		nr_seq_proc_interno
        from    pacote_procedimento
        where   nr_seq_pacote   = nr_seq_pacote_w
        and     ie_inclui_exclui = 'I'
        and     (cd_procedimento is not null or nr_seq_proc_interno is not null)
	and 	nvl(ie_agendavel,'N') = 'S'
        and     nvl(ie_procedimento_regra_w,'S') = 'A';

Cursor C03 is
	select	nr_sequencia
	from	ageint_proc_pacote
	where	nr_seq_ageint	= nr_seq_ageint_p
	union
	select	nr_sequencia
	from	agenda_integrada_item
	where	nr_seq_agenda_int	= nr_seq_ageint_p
	and	nvl(ie_pacote, 'N')	= 'S'
	order by 1;
	
begin

ie_procedimento_regra_w 	:= obter_valor_param_usuario(106, 20, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_qtde_limite_pacote_w 	:= obter_valor_param_usuario(106, 38, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_material_regra_w     	:= obter_valor_param_usuario(106, 42, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_gera_todos_acomodacao_w     	:= nvl(obter_valor_param_usuario(106, 62, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'S');

select  nvl(max(ie_classificacao),'X')
into    ie_tipo_acomod_w
from    tipo_acomodacao b,
        orcamento_paciente a
where a.cd_tipo_acomodacao = b.cd_tipo_acomodacao;

select	max(b.ie_tipo_convenio)
into	ie_tipo_convenio_w
from	agenda_integrada a,
	convenio b
where	a.nr_sequencia	= nr_seq_ageint_p
and	a.cd_convenio	= b.cd_convenio;

ds_pacote_w	:= ds_pacote_p;
if	(ds_pacote_w is not null) and
	(nr_seq_ageint_p is not null) then
	begin
	while	(ds_pacote_w is not null) LOOP
		begin

		tam_lista_w		:=	length(ds_pacote_w);
		ie_pos_virgula_w	:=	instr(ds_pacote_w,',');
	
		if	(ie_pos_virgula_w <> 0) then
			nr_seq_pacote_w	:= substr(ds_pacote_w,1,(ie_pos_virgula_w - 1));
			ds_pacote_w	:= substr(ds_pacote_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;
		
		-- Pacote
		open c01;
		loop
		fetch c01 into
			cd_procedimento_w,
			ie_origem_proced_w,
			vl_honorario_w,
			vl_pacote_w,
			qt_dias_pacote_w,
			vl_anestesista_w,
			qt_limite_w,
			nr_sequencia_w,
			nr_seq_proc_interno_w,
			ie_agendavel_w;
		exit when c01%notfound;

			if      (ie_qtde_limite_pacote_w = 'N') then
				qt_limite_w:= 1;
			end if;
			
			if	(nr_seq_proc_interno_w is null) then
				if	(cd_procedimento_w is not null) and
					(ie_origem_proced_w is not null) then
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_proc_interno_w
					from	proc_interno
					where	cd_procedimento		= cd_procedimento_w
					and	ie_origem_proced	= ie_origem_proced_w;
					
					if	(nr_seq_proc_interno_w	= 0) then	
						select	max(nr_Seq_proc_interno)
						into	nr_Seq_proc_interno_w
						from	proc_interno_conv
						where	cd_procedimento		= cd_procedimento_w
						and	ie_origem_proced	= ie_origem_proced_w;
					end if;
				end if;
			elsif	(cd_procedimento_w is null) or 
				(ie_origem_proced_w is null) then
				obter_proc_tab_interno(nr_seq_proc_interno_w, null, null, null, cd_procedimento_w, ie_origem_proced_w, null, null);			
			end if;
			

			if	(ie_agendavel_w	= 'N') then
				select	ageint_proc_pacote_seq.nextval
				into	nr_seq_proc_pacote_w
				from	dual;
				
				insert into ageint_proc_pacote
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_proc_interno,
					cd_procedimento,
					ie_origem_proced,
					nr_seq_ageint,
					vl_pacote)
				values
					(nr_seq_proc_pacote_w,  
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_proc_interno_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					nr_seq_ageint_p,
					decode(ie_tipo_convenio_w, 1, vl_pacote_w, 0));
			else
				select	agenda_integrada_item_seq.nextval 
				into	nr_seq_proc_pacote_w
				from	dual;	

				ie_tipo_agendamento_w	:= Ageint_Obter_Tipo_Ag_Exame(nr_seq_proc_interno_w, cd_estabelecimento_p, nm_usuario_p);

				ie_gerar_check_list_w	:= substr(ageint_obter_se_proc_check(nr_seq_proc_interno_w,nr_seq_ageint_p, cd_estabelecimento_p),1,1);
				
				if	(ie_gerar_check_list_w = 'S') then
					select	ageint_check_list_paciente_seq.nextval
					into	nr_seq_ageint_check_list_w
					from	dual;
				
					insert into ageint_check_list_paciente (nr_sequencia,
										nr_seq_ageint,
										dt_atualizacao,
										nm_usuario)
									values (nr_seq_ageint_check_list_w,
										nr_seq_ageint_p,
										sysdate,	
										nm_usuario_p);
					commit;
								
					Ageint_Gerar_Check_List(nr_seq_ageint_check_list_w,nr_seq_proc_interno_w,nr_seq_ageint_p,nm_usuario_p, cd_estabelecimento_p);
				end if;
				
				insert into agenda_integrada_item
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec, 
					nm_usuario_nrec,
					nr_seq_proc_interno,
					cd_procedimento, 
					ie_origem_proced,
					ie_tipo_agendamento,
					--nr_seq_proc_pacote,
					vl_item,
					nr_seq_agenda_int,
					ie_pacote,
					nr_seq_pacote)
				values
					(nr_seq_proc_pacote_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_proc_interno_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					ie_tipo_agendamento_w,
					--nr_seq_proc_pacote_w,
					decode(ie_tipo_convenio_w, 1, vl_pacote_w, 0),
					nr_seq_ageint_p,
					'S',
					nr_seq_pacote_w);
					
				Ageint_gerar_anexos_proc(nr_seq_ageint_p,nr_seq_proc_interno_w,cd_estabelecimento_p,nm_usuario_p);
			end if;
					  
		end loop;
		close c01;

		open c05;
		loop
		fetch c05 into
			cd_procedimento_w,
			ie_origem_proced_w,
			vl_honorario_w,
			vl_pacote_w,
			qt_dias_pacote_w,
			vl_anestesista_w,
			qt_limite_w,
			NR_SEQ_PAC_ACOMOD_w,
			nr_seq_proc_interno_w;
		exit when c05%notfound;

			if      (ie_qtde_limite_pacote_w = 'N') then
				qt_limite_w:= 1;
			end if;

			ie_insere_w:= 'S';
			if      (nvl(nr_sequencia_w,0) > 0) and
				(nvl(NR_SEQ_PAC_ACOMOD_w,0) > 0) and
				(nr_sequencia_w <> NR_SEQ_PAC_ACOMOD_w) then
				ie_insere_w:= 'N';
			end if;
			
			if	(nr_seq_proc_interno_w is null) then
				if	(cd_procedimento_w is not null) and
					(ie_origem_proced_w is not null) then
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_proc_interno_w
					from	proc_interno
					where	cd_procedimento		= cd_procedimento_w
					and	ie_origem_proced	= ie_origem_proced_w;
					
					if	(nr_seq_proc_interno_w	= 0) then	
						select	max(nr_Seq_proc_interno)
						into	nr_Seq_proc_interno_w
						from	proc_interno_conv
						where	cd_procedimento		= cd_procedimento_w
						and	ie_origem_proced	= ie_origem_proced_w;
					end if;
				end if;
			elsif	(cd_procedimento_w is null) or 
				(ie_origem_proced_w is null) then
				obter_proc_tab_interno(nr_seq_proc_interno_w, null, null, null, cd_procedimento_w, ie_origem_proced_w, null,null);			
			end if;

			if      (nvl(ie_insere_w,'S') = 'S') then
				
				ie_gerar_check_list_w	:= substr(ageint_obter_se_proc_check(nr_seq_proc_interno_w,nr_seq_ageint_p, cd_estabelecimento_p),1,1);
				
				if	(ie_gerar_check_list_w = 'S') then
					select	ageint_check_list_paciente_seq.nextval
					into	nr_seq_ageint_check_list_w
					from	dual;
				
					insert into ageint_check_list_paciente (nr_sequencia,
										nr_seq_ageint,
										dt_atualizacao,
										nm_usuario)
									values (nr_seq_ageint_check_list_w,
										nr_seq_ageint_p,
										sysdate,	
										nm_usuario_p);
					commit;
								
					Ageint_Gerar_Check_List(nr_seq_ageint_check_list_w,nr_seq_proc_interno_w,nr_seq_ageint_p,nm_usuario_p, cd_estabelecimento_p);
				end if;
				
				insert into agenda_integrada_item
					(nr_sequencia,
					nr_seq_agenda_int,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_proc_interno,
					ie_tipo_agendamento,
					vl_item,
					vl_medico,
					vl_anestesista,
					vl_auxiliares,
					vl_custo_operacional,
					vl_materiais,
					ds_observacao,
					vl_lanc_auto,
					cd_procedimento,
					ie_origem_proced,
					nr_seq_proc_pacote,
					nr_seq_pacote)
				values
					(agenda_integrada_item_seq.nextval,
					nr_seq_ageint_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_proc_interno_w,
					'E',
					0,
					0,
					0,
					0,
					0,
					0,
					obter_desc_expressao(777040),
					0,
					cd_procedimento_w,
					ie_origem_proced_w,
					decode(ie_agendavel_w, 'N', nr_seq_proc_pacote_w, null),
					nr_seq_pacote_w);
				
				Ageint_gerar_anexos_proc(nr_seq_ageint_p,nr_seq_proc_interno_w,cd_estabelecimento_p,nm_usuario_p);
			end if;

		end loop;
		close c05;
	
		end;		
	end loop;
	end;
end if;
  
commit;

open C03;
loop
fetch C03 into	
	nr_seq_proc_pacote_w;
exit when C03%notfound;
	begin
	if	(ie_agendavel_w	= 'N') then
		ie_pacote_w	:= 'S';
	else
		ie_pacote_w	:= 'N';	
	end if;
	ageint_gerar_lanc_auto(nr_seq_ageint_p, 34, nr_seq_proc_pacote_w, ie_pacote_w, nm_usuario_p);
	end;
end loop;
close C03;

end ageint_gerar_pacote;
/