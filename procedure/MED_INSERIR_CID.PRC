create or replace
procedure Med_Inserir_CID
		(cd_medico_p		varchar2,
		nr_ficha_p		number,
		cd_cid_p		varchar2,
		dt_cid_p		date,
		ds_cid_p		varchar2,
		nm_usuario_p		varchar2) is

nr_seq_cliente_w	number(10,0);
nr_seq_CID_w		number(10,0);
cd_cid_w		varchar2(10);

begin

cd_cid_w		:= replace(replace(replace(cd_cid_p, '.', ''), '/', ''), ' ', '');


select	nvl(max(nr_sequencia),0)
into	nr_seq_cliente_w
from	med_cliente
where	cd_medico		= cd_medico_p
and	cd_pessoa_sist_orig	= nr_ficha_p;

select	nvl(max(cd_doenca_cid),'0')
into	cd_cid_w
from	cid_doenca
where	upper(cd_doenca_cid) = upper(cd_cid_w);


if	(nr_seq_cliente_w > 0) and
	(cd_cid_w <> '0') then
	begin

	select	med_pac_cid_seq.nextval
	into	nr_seq_CID_w		
	from	dual;


	insert	into med_pac_cid
		(nr_sequencia,
		nr_seq_cliente,
		cd_doenca_cid,
		dt_atualizacao,
		nm_usuario,
		ds_observacao,
		dt_diagnostico,
		ie_classificacao,
		ie_status)
	values	(nr_seq_CID_w,
		nr_seq_cliente_w,
		upper(cd_cid_w),
		sysdate, nm_usuario_p,
		ds_cid_p,
		nvl(dt_cid_p, trunc(sysdate)),
		'S',
		'A');


	end;
end if;

commit;

end Med_Inserir_CID;
/