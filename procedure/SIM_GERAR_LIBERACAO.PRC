create or replace
procedure sim_gerar_liberacao(		nr_sequencia_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

begin

update	simulacao
set	nm_usuario 	= nm_usuario_p,
	dt_atualizacao 	= sysdate,
	dt_liberacao   	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end sim_gerar_liberacao;
/