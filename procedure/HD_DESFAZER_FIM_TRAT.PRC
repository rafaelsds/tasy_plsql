create or replace
procedure hd_desfazer_fim_trat(	nr_seq_tratamento_p	number) is 
begin
if	(nr_seq_tratamento_p is not null) then
	update	paciente_tratamento
	set	dt_final_tratamento = null,
		nr_seq_motivo_fim   = null,
		nr_seq_causa_morte  = null,		
		ds_observacao       = null
	where	nr_sequencia = nr_seq_tratamento_p;
end if;

commit;

end hd_desfazer_fim_trat;
/