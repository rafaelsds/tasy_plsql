CREATE OR REPLACE
PROCEDURE qua_comunic_revisao(	nr_sequencia_p		Number,
					ds_observacao_p		Varchar2,
					cd_setor_atendimento_p	Number,
					nm_usuario_p			Varchar2) is

ds_titulo_w			Varchar2(255);
cd_documento_w		Varchar2(20);
cd_pessoa_revisao_w		Varchar2(10);
nm_usuario_w			Varchar2(15);
nr_sequencia_w		Number(10);
nr_seq_classif_w		Number(10);
nm_pessoa_w			Varchar2(100);
ds_arquivo_w			Varchar2(255);
ds_aviso_w			Varchar2(255);
nm_documento_w		Varchar2(255);

Cursor C01 IS
	select	cd_pessoa_revisao
	from	qua_doc_revisao
	where	nr_seq_doc = nr_sequencia_p
	union
	select	nvl(max(cd_pessoa_elaboracao),'')
	from	qua_documento
	where	nr_sequencia = nr_sequencia_p
	union
	select	nvl(max(cd_pessoa_validacao),'')
	from	qua_documento
	where	nr_sequencia = nr_sequencia_p
	union
	select	nvl(max(cd_pessoa_aprov),'')
	from	qua_documento
	where	nr_sequencia = nr_sequencia_p
	union
	select	cd_pessoa_fisica
	from	usuario
	where	cd_setor_atendimento = cd_setor_atendimento_p;

BEGIN 

select	cd_documento,
	ds_arquivo,
	nm_documento
into	cd_documento_w,
	ds_arquivo_w,
	nm_documento_w
from	qua_documento
where	nr_sequencia		= nr_sequencia_p;

ds_titulo_w	:= Wheb_mensagem_pck.get_texto(799461) || ' ' || cd_documento_w || ' - ' || nm_documento_w;

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

OPEN C01;
LOOP
FETCH C01 into
	cd_pessoa_revisao_w;
EXIT when C01%notfound;
	begin
	select	min(nm_usuario)
	into	nm_usuario_w
	from	usuario b
	where	cd_pessoa_fisica	= cd_pessoa_revisao_w;

	if	(nm_usuario_w is not null) then
		begin
		ds_aviso_w	:= Wheb_mensagem_pck.get_texto(799460) || chr(13) ||  chr(10) ||
				Wheb_mensagem_pck.get_texto(799461);

		if	(ds_arquivo_w is not null) then
			ds_aviso_w	:= ds_aviso_w  || chr(13) || chr(10) ||
					   Wheb_mensagem_pck.get_texto(799462) || ' ' ||  ds_arquivo_w;
		end if;

		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;
		select	substr(obter_nome_pessoa_fisica(cd_pessoa_revisao_w, null),1,100)
		into	nm_pessoa_w
		from	dual;
		insert into comunic_interna(
			dt_comunicado, ds_titulo, ds_comunicado, nm_usuario,
			dt_atualizacao, ie_geral, nm_usuario_destino, nr_sequencia,
			ie_gerencial, nr_seq_classif, dt_liberacao)
		values(
			sysdate, ds_titulo_w, ds_aviso_w || chr(13) || chr(10) ||  ds_observacao_p, nm_usuario_p,
			sysdate, 'N', nm_usuario_w, nr_sequencia_w, 'N',
			nr_seq_classif_w, sysdate);
		end;

	Insert into qua_doc_envio(
		nr_sequencia, nr_seq_doc, dt_atualizacao, nm_usuario,
		dt_envio, ie_tipo_envio, ds_destino, ds_observacao)
	values(qua_doc_envio_seq.nextval, nr_sequencia_p, sysdate, nm_usuario_p,
		sysdate, 'D', nm_pessoa_w, ds_aviso_w);
	end if;
	end;
END LOOP;
CLOSE C01;

commit;

END qua_comunic_revisao;
/
