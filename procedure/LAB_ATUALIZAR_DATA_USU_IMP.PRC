create or replace
procedure lab_atualizar_data_usu_imp (	nr_seq_lote_p	number,
					nm_usuario_p	varchar2) is 

Cursor C01 is
	select	a.nr_prescricao,
		a.nr_seq_prescr
	from	lab_lote_exame_item a
	where	a.nr_seq_lote = nr_seq_lote_p;
				
c01_w	c01%rowtype;
				
begin

open c01;
loop
fetch c01 into c01_w;
	exit when c01%notfound;
	begin
	
	update	prescr_procedimento
	set	dt_imp_lote = sysdate,
		nm_usuario_imp_lote = nm_usuario_p
	where	nr_prescricao = c01_w.nr_prescricao
	and	nr_sequencia = c01_w.nr_seq_prescr;
	
	end;
end loop;
close c01;

commit;

end lab_atualizar_data_usu_imp;
/