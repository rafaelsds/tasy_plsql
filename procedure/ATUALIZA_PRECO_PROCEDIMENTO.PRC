create or replace
procedure atualiza_preco_procedimento
		(	nr_sequencia_p	number,
			cd_convenio_p	number,
			nm_usuario_p    varchar2) is

ie_tipo_convenio_w		number(2)    := 0;
ie_classificacao_w		number(01);
cd_situacao_glosa_w	number(02);
nr_seq_autorizacao_w	number(10,0);
nr_sequencia_w		number(10);
ie_origem_proced_w	number(10);
nr_seq_proc_w		number(10,0);

nr_interno_conta_w		number(10,0);
cd_convenio_w		number(10,0);
cd_categoria_w		number(10,0);

nr_seq_material_w		number(10,0);
cd_estabelecimento_w	number(10,0);
ie_glosa_mat_proc_princ_w	varchar2(01);
nr_seq_auditoria_w		number(10,0);
nr_seq_auditoria_log_w		number(10,0);
cd_estab_conta_w		number(4,0);
ie_duplica_proc_pacote_estab_w	varchar2(1);
nr_seq_proc_orig_w		number(10,0);
qt_proc_orig_w			number(10,0);
nr_conta_orig_mat_w		conta_paciente.nr_interno_conta%type;
nr_prescricao_w			procedimento_paciente.nr_prescricao%type;
nr_sequencia_prescricao_w	procedimento_paciente.nr_sequencia_prescricao%type;
nr_seq_prescr_proc_inf_w	prescr_proced_inf_adic.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		nr_interno_conta
	from	material_atend_paciente
	where	nr_seq_proc_princ		= nr_sequencia_p
	and	cd_motivo_exc_conta		is null
	and	cd_convenio			<> cd_convenio_w
	and	ie_glosa_mat_proc_princ_w	= 'S';	

BEGIN

select	max(b.ie_classificacao),
	max(a.ie_origem_proced),
	nvl(max(nr_sequencia),0)
into	ie_classificacao_w,
	ie_origem_proced_w,
	nr_seq_proc_w
from	procedimento 		b,
	procedimento_paciente	a
where	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and 	a.nr_sequencia     	= nr_sequencia_p;


if	(nr_seq_proc_w > 0) then
	begin

	/* Obter tipo do convenio */
	begin
	SELECT	IE_TIPO_CONVENIO
	INTO	IE_TIPO_CONVENIO_W
	FROM	CONVENIO
	WHERE	CD_CONVENIO = CD_CONVENIO_P;
	exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(193010);
			/*Erro ao ler convenio*/
	end;

	if	(ie_tipo_convenio_w in (3,10)) then
		begin
		if	(ie_origem_proced_w	= 7) then
			sus_atualiza_valor_proc(nr_sequencia_p, nm_usuario_p);
		/*Adicionado este tratamento pois quando o procedimento lancado tinha origem diferente de 7
		entrava na rotina do SUS antigo mesmo nao sendo origem SUS*/			
		elsif	(ie_origem_proced_w in (2,3)) then
			atualiza_preco_proc_sus(nr_sequencia_p, nm_usuario_p);
		else
			begin
			if	(ie_classificacao_w = 1) then
	        		atualiza_preco_proc_amb(nr_sequencia_p, nm_usuario_p);
			else
				atualiza_preco_servico(nr_sequencia_p, nm_usuario_p);
			end if;
			end;
		end if;
		end;
	else
		begin
		if	(ie_classificacao_w = 1) then
        		atualiza_preco_proc_amb(nr_sequencia_p, nm_usuario_p);
		else
			atualiza_preco_servico(nr_sequencia_p, nm_usuario_p);
		end if;
		end;
	end if;

	/*	Rotina para Tratar Glosa por Valor (Convenio paga parte Exame) */
	select	nvl(cd_situacao_glosa,0),
		nvl(nr_seq_autorizacao,0),
		nr_prescricao,
		nr_sequencia_prescricao
	into	cd_situacao_glosa_w,
		nr_seq_autorizacao_w,
		nr_prescricao_w,
		nr_sequencia_prescricao_w
	from 	Procedimento_Paciente
	where 	nr_sequencia 	= nr_sequencia_p;
	if	(cd_situacao_glosa_w = 9) and
		(nr_seq_autorizacao_w > 0) then
		Glosa_Valor_Autorizado(nr_sequencia_p);
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_prescr_proc_inf_w
	from	prescr_proced_inf_adic
	where	nr_prescricao		= nr_prescricao_w
	and		nr_seq_prescricao	= nr_sequencia_prescricao_w
	and 	nvl(vl_coparticipacao,0) <> 0;

	/*	Rotina para Tratar Glosa por Percentual (outro convenio paga parte Exame) */
	if	(cd_situacao_glosa_w in (8,10,11,23)) or
		(nvl(nr_seq_prescr_proc_inf_w,0) > 0) then
		begin
		select 	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from	procedimento_paciente
		where	nr_seq_proc_princ = nr_sequencia_p;

		if	(nr_sequencia_w	> 0) then
			if   	(ie_classificacao_w = 1) then
				Atualiza_Preco_Proc_AMB(nr_sequencia_w, nm_usuario_p);
			else
				Atualiza_preco_servico(nr_sequencia_w, nm_usuario_p);
			end if;
		end if;
		end;
	end if;

	begin
	select	a.cd_situacao_glosa,
		a.nr_interno_conta,
		b.cd_convenio_parametro,
		a.cd_categoria,
		c.ie_tipo_convenio,
		b.cd_estabelecimento
	into	cd_situacao_glosa_w,
		nr_interno_conta_w,
		cd_convenio_w,
		cd_categoria_w,
		ie_tipo_convenio_w,
		cd_estab_conta_w
	from	convenio c,
		conta_paciente b, 
		procedimento_paciente a
	where	a.nr_interno_conta	= b.nr_interno_conta
	and	a.nr_sequencia		= nr_sequencia_p
	and	b.cd_convenio_parametro	= c.cd_convenio;
	exception
		when others then
			nr_interno_conta_w	:= 0;
	end;
	
	ie_duplica_proc_pacote_estab_w:= nvl(Obter_valor_param_usuario(0, 141, obter_perfil_ativo, nm_usuario_p, cd_estab_conta_w),'N');
	
	if	(ie_duplica_proc_pacote_estab_w = 'S') then
	
		select	nvl(max(nr_seq_proc_orig),0)
		into	nr_seq_proc_orig_w
		from	procedimento_paciente
		where	nr_sequencia = nr_sequencia_p;
		
		select	count(*)
		into	qt_proc_orig_w
		from	procedimento_paciente
		where	nr_sequencia = nr_seq_proc_orig_w;
		
		if	(nr_seq_proc_orig_w > 0) and (qt_proc_orig_w > 0) then
			if   	(ie_classificacao_w = 1) then
				Atualiza_Preco_Proc_AMB(nr_seq_proc_orig_w, nm_usuario_p);
			else
				Atualiza_preco_servico(nr_seq_proc_orig_w, nm_usuario_p);
			end if;
		end if;
	
	end if;
	
	if	(nr_interno_conta_w	<> 0) and
		(ie_tipo_convenio_w	= 1) then
		
		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	conta_paciente
		where	nr_interno_conta	= nr_interno_conta_w;
		
		select	nvl(max(ie_glosa_mat_proc_princ), 'N')
		into	ie_glosa_mat_proc_princ_w
		from	parametro_faturamento
		where	cd_estabelecimento	= cd_estabelecimento_w;		
		
		open C01;
		loop
		fetch 	C01 into	
			nr_seq_material_w,
			nr_conta_orig_mat_w;
		exit when C01%notfound;
			begin
			
			update	material_atend_paciente
			set	nr_interno_conta	= nr_interno_conta_w,
				cd_convenio		= cd_convenio_w,
				cd_categoria		= cd_categoria_w
			where	nr_sequencia		= nr_seq_material_w;
			
			atualiza_preco_material(nr_seq_material_w, nm_usuario_p);
			
			select 	nvl(max(a.nr_sequencia),0)
			into	nr_seq_auditoria_w
			from 	auditoria_propaci b,
				auditoria_conta_paciente a
			where 	a.nr_interno_conta = nr_interno_conta_w
			and 	a.nr_sequencia = b.nr_seq_auditoria
			and 	b.nr_seq_propaci = nr_sequencia_p;
			
			if	(nr_seq_auditoria_w > 0) then
				update	auditoria_matpaci a
				set	nr_seq_auditoria = nr_seq_auditoria_w,
					nr_conta_origem = decode(nr_conta_origem, null, nr_conta_orig_mat_w, nr_conta_origem)
				where 	nr_seq_matpaci = nr_seq_material_w
				and 	exists(	select 	1 
						from 	auditoria_conta_paciente x
						where 	x.nr_sequencia = a.nr_seq_auditoria
						and 	x.dt_liberacao is null);
			end if;
			
			end;
		end loop;
		close C01;
	
	end if;
	
	--Added below condition for DRG Billing	
	if(ie_origem_proced_w =15 and ie_classificacao_w <> 3) then 
		select	max(nr_interno_conta)
		into	nr_interno_conta_w
		from	procedimento_paciente
		where	nr_sequencia	= nr_sequencia_p;

		select	max(b.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	conta_paciente b
		where	nr_interno_conta	= nr_interno_conta_w;

        if (obtain_user_locale(nm_usuario_p) = 'en_AU') then
		    processar_conta_drg_aus(nr_interno_conta_w,cd_estabelecimento_w,'S',nm_usuario_p);
		end if;

		if(9 = philips_param_pck.get_cd_pais) then
            ctb_parametros_cc_rateio_pck.gravar_rateio_proc(nr_sequencia_p, philips_contabil_pck.get_parametro_conta_contabil, nm_usuario_p); 
        end if;
	end if;
	
	Atualiza_Doc_Conv_Partic(nr_sequencia_p, nm_usuario_p);
	
	/* Ricardo 13/12/2004 - Incluida a rotina abaixo para tratar o setor do paciente nos procedimentos */
	Atualizar_Setor_Pac_Procmat(nr_sequencia_p, 'P');

	tiss_atualizar_procedimento(nr_sequencia_p, 'N', nm_usuario_p);

	-- edgar 04/10/2007, os 70483
	tiss_atualiza_conta_proc(nr_sequencia_p, nm_usuario_p);
	
	end;
end if;
end atualiza_preco_procedimento;
/
