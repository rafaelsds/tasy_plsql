create or replace
procedure Desfazer_Rep_Terceiro_Selec
			(nr_repasse_terceiro_p	number,
			cd_estabelecimento_p	number,
	 		nm_usuario_p		varchar2,
			ie_acao_p		number) is

/*	ie_acao_p

0	Sem a��o
1	Excluir vencimentos
2	Mudar status e excluir vencimentos
3	Desfazer aprova��o, mudar status e excluir vencimentos
4	Desvincular t�tulo, desfazer aprova��o, mudar status e excluir vencimentos

*/

nr_seq_terceiro_w		number(10);
ie_desvinv_contabil_w		varchar2(1);
qt_titulo_w			number(10);
nr_repasse_terceiro_w		number(10);
qt_contabilizado_w		number(10);
nr_titulo_w			number(10);
qt_titulo_nf_w			number(10);
ie_reabrir_contabil_w		varchar2(1);
dt_aprovacao_terceiro_w		date;
ie_status_w			varchar2(1);
nm_terceiro_w			varchar2(255);
ie_excluir_rep_desfazer_w	varchar2(3);

Cursor C01 is
	select	b.nr_sequencia,
		a.nr_repasse_terceiro,
		a.dt_aprovacao_terceiro,
		a.ie_status,
		substr(obter_nome_terceiro(b.nr_sequencia),1,255) nm_terceiro
	from	terceiro b,
		repasse_terceiro a
	where	(ie_acao_p = 4 or
		(ie_acao_p = 3 and not exists	(select	1
						from	titulo_pagar x
						where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro)) or
		(ie_acao_p = 2 and a.dt_aprovacao_terceiro is null) or
		(ie_acao_p = 1 and a.ie_status = 'A') or
		(ie_acao_p = 0 and not exists	(select	1
						from	repasse_terceiro_venc x
						where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro)))
	and	b.ie_situacao		= 'A'
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	nvl(b.ie_utilizacao, 'A') in ('A','R')
	and	a.nr_seq_terceiro	= b.nr_sequencia
	and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;

begin

obter_param_usuario(89,63,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_desvinv_contabil_w);
obter_param_usuario(89,64,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_reabrir_contabil_w);

select	nvl(max(a.ie_excluir_rep_desfazer),'N')
into	ie_excluir_rep_desfazer_w
from	parametro_faturamento a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

open C01;
loop
fetch C01 into	
	nr_seq_terceiro_w,
	nr_repasse_terceiro_w,
	dt_aprovacao_terceiro_w,
	ie_status_w,
	nm_terceiro_w;
exit when C01%notfound;
	begin	

	select	count(*)
	into	qt_titulo_w
	from	titulo_pagar a
	where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	select	count(*)
	into	qt_contabilizado_w
	from	repasse_terceiro_item a
	where	nvl(a.nr_lote_contabil,0)	> 0
	and	a.nr_repasse_terceiro		= nr_repasse_terceiro_w;
	
	if	(qt_titulo_w		> 0) then
		
		if	(ie_desvinv_contabil_w	<> 'S') and
			(qt_contabilizado_w	> 0) then			
			wheb_mensagem_pck.exibir_mensagem_abort(185427);
		end if;
	
		select	count(*)
		into	qt_titulo_nf_w
		from	nota_fiscal b,
			titulo_pagar a
		where	nvl(b.ie_situacao,1)	= 1
		and	a.nr_seq_nota_fiscal	= b.nr_sequencia
		and	a.nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
		if	(qt_titulo_nf_w	> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(185428);
		end if;
	
		desvincular_titulo_repasse(nr_repasse_terceiro_w,nm_usuario_p,'N');
	
	end if;
	
	if	(dt_aprovacao_terceiro_w	is not null) then
		desfazer_aprovacao_repasse(nr_repasse_terceiro_w,nm_usuario_p,'N');
	end if;
	
	if	(ie_status_w	= 'F') then
	
		if	(ie_reabrir_contabil_w	<> 'S') and
			(qt_contabilizado_w	> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(185429);
		end if;
	
		altera_status_repasse_terceiro(nr_repasse_terceiro_w,nm_usuario_p,'N');
	
	end if;
	
	delete	from repasse_terc_venc_trib
	where	nr_seq_rep_venc	in
		(select	a.nr_sequencia
		from	repasse_terceiro_venc a
		where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w);
	
	delete	from repasse_terceiro_venc
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	delete	from repasse_terceiro_item
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	update	material_repasse
	set	nr_repasse_terceiro	= null
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	update	procedimento_repasse
	set	nr_repasse_terceiro	= null
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	if	(nvl(ie_excluir_rep_desfazer_w,'N')	= 'S') then
		delete	from repasse_terceiro
		where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	end if;

end;
end loop;
close C01;


commit;

end Desfazer_Rep_Terceiro_Selec;
/
