create or replace
procedure san_desfaz_lavar_hemo(nr_seq_producao_p		number,
				    nm_usuario_p	varchar2) is 

ie_lavado_w	varchar2(1);

begin

select      max(ie_lavado)
into	ie_lavado_w
from 	san_producao
where 	nr_sequencia = nr_seq_producao_p;

if	(nr_seq_producao_p is not null) and
	(ie_lavado_w is not null) then

	delete from san_producao_lavagem
	where  nr_seq_producao = nr_seq_producao_p;

	update	san_producao
	set	ie_lavado 		= 	'N',
		nm_usuario_lavado	=	nm_usuario_p
	where	nr_sequencia		=	nr_seq_producao_p;
end if;

commit;

end san_desfaz_lavar_hemo;
/