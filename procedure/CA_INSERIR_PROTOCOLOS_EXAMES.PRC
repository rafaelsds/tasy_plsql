create or replace
procedure ca_inserir_protocolos_exames(
			nm_usuario_p		Varchar2,
			ds_seq_protocolos_p		varchar2,
			nr_seq_analise_p		number) is 

nr_seq_exame_w		number(10);	
cd_unidade_medida_w	varchar2(30);	
ds_protocolos_w		varchar2(4000);
ds_codigo_temp_w 	varchar2(4000);
ds_codigo_aux_w  	varchar2(2);
k   				number(4);
i   				number(4);
nr_seq_protocolo_w	number(10);
qt_exame_analise_w	number(10);
			
Cursor C01 is
	select	nr_seq_exame
	from	ca_protocolo_exame
	where	nr_seq_protocolo = nr_seq_protocolo_w;
			
begin

if	(ds_seq_protocolos_p is not null) then

	ds_protocolos_w := substr(ds_seq_protocolos_p,1,4000);
	
	/* O IF abaixo � apenas utilizado para adicionar uma v�rgula no final,
	quando o m�todo n�o retornar dessa forma (caso da utiliza��o do WCheckList via Schematic)*/
	if	(SUBSTR(ds_protocolos_w,LENGTH(ds_protocolos_w),LENGTH(ds_protocolos_w)) <> ',') then
		ds_protocolos_w	:= ds_protocolos_w || ',';
	end if;		
	
	k := 1;
	for i in k..length(ds_protocolos_w) loop
		begin
		
		ds_codigo_aux_w := SUBSTR(ds_protocolos_w,i,1);
		
		if	(ds_codigo_aux_w = ',') then
		
			nr_seq_protocolo_w := obter_somente_numero(ds_codigo_temp_w);
			
			open C01;
			loop
			fetch C01 into	
				nr_seq_exame_w;
			exit when C01%notfound;
				begin
				
				select	count(*)
				into	qt_exame_analise_w
				from	ca_exame_realizado
				where	nr_seq_analise = nr_seq_analise_p
				and		nr_seq_exame = nr_seq_exame_w;
				
				if	(qt_exame_analise_w = 0) then
				
					select 	cd_unidade_medida
					into	cd_unidade_medida_w
					from 	ca_exame
					where 	nr_sequencia = nr_seq_exame_w;
					
					insert into	ca_exame_realizado(	nr_seq_analise,         
													nr_seq_exame,           
													dt_atualizacao,       
													nm_usuario,             
													dt_atualizacao_nrec,    
													nm_usuario_nrec,        
													cd_unidade_medida)
											values(nr_seq_analise_p,
													nr_seq_exame_w,
													sysdate,
													nm_usuario_p,
													sysdate,
													nm_usuario_p,
													cd_unidade_medida_w);
				end if;
				end;
			end loop;
			close C01;
		

		
			ds_codigo_temp_w := '';
		else
			ds_codigo_temp_w := ds_codigo_temp_w||ds_codigo_aux_w;
		end if;
		end;
	end loop;
	
end if;

commit;

end ca_inserir_protocolos_exames;
/