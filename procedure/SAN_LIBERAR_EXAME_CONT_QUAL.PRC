create or replace
procedure san_liberar_exame_cont_qual(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) is 

begin

if (nr_sequencia_p is not null) then

	update	san_controle_qual_exame
	set	dt_liberacao 		= sysdate,
		nm_usuario_liberacao 	= nm_usuario_p
	where	nr_sequencia 		= nr_sequencia_p; 
	
end if;

commit;

end san_liberar_exame_cont_qual;
/