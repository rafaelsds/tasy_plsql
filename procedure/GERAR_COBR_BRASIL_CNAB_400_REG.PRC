create or replace
procedure GERAR_COBR_BRASIL_CNAB_400_REG(	nr_seq_cobr_escrit_p	number,
											cd_estabelecimento_p	number,
											nm_usuario_p			varchar2) is
							
/*===========================================================
	             =>>>>>	A T E N � � O        <<<<<<=

Esta procedure � uma c�pia da GERAR_COBRANCA_BRASIL_CNAB_400, 
para atender a OS 1176196,layout anexo nesta OS.

O Layout disponibilizado no site do banco era da vers�o Abril/2012.

Como se trata de um projeto e n�o possu�mos cliente para validar junto 
ao banco, os defeitos devem ser verificados com o analista (Peterson) antes
de serem documentados. 
============================================================*/	
ds_conteudo_w					varchar2(400);	

/*Header arquivo*/
cd_agencia_banc_header_w		varchar2(4);
ie_digito_agencia_w				varchar2(1);
cd_conta_header_w				varchar2(8);
ie_digito_conta_w				varchar2(1);
nm_empresa_w					varchar2(30);
dt_geracao_w					varchar2(6);
nr_seq_cobr_w					varchar2(7);
cd_convenio_header_w			varchar2(7);
nr_seq_registro_w				number(6) := 0;
nr_seq_apres_w					number(10) := 0;

/*Detalhe*/
ie_tipo_pessoa_w				varchar2(2);
nr_inscricao_w					varchar2(14);
cd_agencia_bancaria_w			titulo_receber_cobr.cd_agencia_bancaria%type;
nr_conta_w						titulo_receber_cobr.nr_conta%type;
ie_digito_conta_detalhe_w		titulo_receber_cobr.ie_digito_conta%type;
cd_convenio_banco_w				banco_estabelecimento.cd_convenio_banco%type;
nr_titulo_w						titulo_receber.nr_titulo%type;
nr_nosso_numero_w				titulo_receber.nr_nosso_numero%type;			
cd_carteira_w					banco_carteira.cd_carteira%type;
cd_variacao_carteira_w			banco_carteira.cd_variacao_carteira%type;
dt_vencimento_w					varchar2(6);
vl_saldo_titulo_w				varchar2(13);	
cd_banco_w						banco_estabelecimento.cd_banco%type;	
dt_emissao_w					varchar2(6);
vl_juros_w						varchar2(13);
nm_pessoa_w						varchar2(37);
ds_endereco_w					varchar2(40);
ds_bairro_w						varchar2(12);
ds_cep_w						varchar2(8);
ds_cidade_w						varchar2(15);
ds_uf_w							varchar2(2);
ie_digito_agencia_detalhe_w		varchar2(1);	
cd_cgc_w						estabelecimento.cd_cgc%type;	
vl_multa_w						varchar2(12);	
dt_multa_w						varchar2(6);
ds_email_w						varchar2(136);	
cd_convenio_banco_inter_w		banco_estab_interf.cd_convenio_banco%type;										


cursor	c01 is
	select	decode(b.cd_cgc,null,'01','02') ie_tipo_pessoa,
			lpad(substr(decode(b.cd_cgc,null, nvl(obter_compl_pf(b.cd_pessoa_fisica, 1, 'C'),'0'), b.cd_cgc),1,14),14,'0') nr_inscricao,
			a.cd_agencia_bancaria,
			a.nr_conta,
			a.ie_digito_conta,
			substr(d.cd_convenio_banco,1,7),
			b.nr_titulo,
			b.nr_nosso_numero,
			nvl(e.cd_carteira, (select max(x.cd_carteira) from banco_carteira x where x.nr_sequencia = b.nr_seq_carteira_cobr)),
			nvl(e.cd_variacao_carteira, (select max(y.cd_variacao_carteira) from banco_carteira y where y.nr_sequencia = b.nr_seq_carteira_cobr)),
			to_char(b.dt_pagamento_previsto,'DDMMYY'),
			lpad(replace(to_char(b.vl_saldo_titulo, 'fm00000000000.00'),'.',''),13,'0'),
			d.cd_banco,
			to_char(b.dt_emissao,'DDMMYY'),
			lpad(replace(to_char(nvl(a.vl_juros,'0'), 'fm00000000000.00'),'.',''),13,'0'),
			upper(substr(elimina_caractere_especial(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc)),1,37)),
			upper(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'R'),1,40))ds_endereco,
			upper(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),1,12))ds_bairro,
			substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CEP'),1,8) ds_cep,
			upper(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CI'),1,15)) ds_cidade,
			upper(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'UF'),1,2)) ds_uf,
			f.cd_cgc,
			lpad(replace(to_char(a.vl_multa, 'fm0000000000.00'),'.',''),12,'0'),
			to_char(to_date(b.dt_pagamento_previsto,'DD/MM/YYYY')+1,'DDMMYY'),
			upper(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'M'),1,136))
	from	titulo_receber_cobr a,
			titulo_receber b,
			cobranca_escritural c,
			banco_estabelecimento d,
			banco_carteira e,
			estabelecimento f
	where	a.nr_titulo 			= b.nr_titulo
	and		a.nr_seq_cobranca 		= c.nr_sequencia
	and		c.nr_seq_conta_banco	= d.nr_sequencia
	and		c.nr_seq_carteira_cobr	= e.nr_sequencia(+) 
	and		c.cd_estabelecimento	= f.cd_estabelecimento
	and		c.nr_sequencia			= nr_seq_cobr_escrit_p;

begin

delete	
from 	w_envio_banco
where	nm_usuario	= nm_usuario_p;


/* HEADER DE ARQUIVO */
select	lpad(substr(nvl(c.cd_agencia_bancaria,'0'),1,4),4,'0'),
		substr(nvl(c.ie_digito,'0'),1,1),
		lpad(substr(nvl(b.cd_conta,'0'),1,8),8,'0'), 
		substr(nvl(b.ie_digito_conta,'0'),1,1),
		rpad(upper(elimina_caractere_especial(substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,30))),30,' '),
		to_char(sysdate,'DDMMYY'),
		lpad(substr(a.nr_sequencia,1,7),7,'0'),
		lpad(substr(nvl(b.cd_convenio_banco,'0'),1,7),7,'0')
into	cd_agencia_banc_header_w,
		ie_digito_agencia_w,
		cd_conta_header_w,
		ie_digito_conta_w,
		nm_empresa_w,
		dt_geracao_w,
		nr_seq_cobr_w,
		cd_convenio_header_w	
from	banco_estabelecimento b,
		cobranca_escritural a,
		agencia_bancaria c
where	a.nr_seq_conta_banco	= b.nr_sequencia
and		b.cd_agencia_bancaria 	= c.cd_agencia_bancaria
and		a.nr_sequencia			= nr_seq_cobr_escrit_p
and		b.cd_banco				= c.cd_banco -- Ag�ncias do mesmo.
and		nvl(b.ie_situacao,'A')	= 'A';

nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
nr_seq_apres_w	  := nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w := 	'0'										|| /*Pos 1*/
					'1'										|| /*Pos 2*/
					'REMESSA'								|| /*Pos 03 a 09*/
					'01'									|| /*Pos 10 a 11*/
					'COBRANCA'								|| /*Pos 12 a 19*/
					lpad(' ',7,' ')							|| /*Pos 20 a 26*/
					cd_agencia_banc_header_w				|| /*Pos 27 a 30*/
					ie_digito_agencia_w						|| /*Pos 31*/
					cd_conta_header_w						|| /*Pos 32 a 39*/
					ie_digito_conta_w						|| /*Pos 40*/
					lpad('0',6,'0')							|| /*Pos 41 a 46*/
					nm_empresa_w							|| /*Pos 47 a 76*/
					rpad('001BANCODOBRASIL',18,' ')			|| /*Pos 77 a 94*/
					dt_geracao_w							|| /*Pos 95 a 100*/
					nr_seq_cobr_w							|| /*Pos 101 a 107*/
					rpad(' ',22,' ')						|| /*Pos 108 a 129*/
					cd_convenio_header_w					|| /*Pos 130 a 136*/
					rpad(' ',258,' ')						|| /*Pos 137 394*/
					lpad(nr_seq_registro_w,6,'0');			   /*Pos 395 a 400*/	
					
insert into w_envio_banco (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
				values	 (	w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_apres_w);					
/*FIM HEADER ARQUIVO*/

				
/*Inicio Detalhe*/
open	c01;
loop
fetch	c01 into
	ie_tipo_pessoa_w,
	nr_inscricao_w,
	cd_agencia_bancaria_w,
	nr_conta_w,
	ie_digito_conta_detalhe_w,
	cd_convenio_banco_w,
	nr_titulo_w,
	nr_nosso_numero_w,			
	cd_carteira_w,
	cd_variacao_carteira_w,
	dt_vencimento_w,
	vl_saldo_titulo_w,	
	cd_banco_w,	
	dt_emissao_w,
	vl_juros_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	ds_cep_w,
	ds_cidade_w,
	ds_uf_w,
	cd_cgc_w,
	vl_multa_w,
	dt_multa_w,
	ds_email_w;
exit when c01%notfound;
	/*Inicio Segmento Obrigatorio*/
	if (cd_agencia_bancaria_w is not null) then
		ie_digito_agencia_detalhe_w	:= to_char(calcula_digito('MODULO11',cd_agencia_bancaria_w));
	end if;

	nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
	nr_seq_apres_w	  := nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w := 	'7'														|| /*Pos 1*/
						'02'													|| /*Pos 02 a 03*/
						lpad(nvl(cd_cgc_w,'0'),14,'0')							|| /*Pos 04 a 17*/
						lpad(cd_agencia_bancaria_w,4,'0')						|| /*Pos 18 a 21*/
						lpad(nvl(ie_digito_agencia_detalhe_w,'0'),1,'0')		|| /*Pos 22*/	
						lpad(nvl(substr(nr_conta_w,1,8),'0'),8,'0')				|| /*Pos 23 a 30*/
						lpad(nvl(substr(ie_digito_conta_detalhe_w,1,1),'0'),1,'0')|| /*Pos 31*/
						lpad(nvl(cd_convenio_banco_w,'0'),7,'0')				|| /*Pos 32 a 38*/
						lpad(nr_titulo_w,25,'0')								|| /*Pos 39 a 63*/
						lpad(substr(nvl(cd_convenio_header_w,'0'),1,7),7,'0')	|| /*Pos 64 a 80*/
						lpad(substr(nvl(nr_nosso_numero_w,'0'),1,17),10,'0')		|| /*Pos 64 a 80*/
						'00'													|| /*Pos 81 a 82*/
						'00'													|| /*Pos 83 a 84*/
						lpad(' ',3,' ')											|| /*Pos 85 a 87*/
						' '														|| /*Pos 88*/
						lpad(' ',3,' ')											|| /*Pos 89 a 91*/
						lpad(substr(nvl(cd_variacao_carteira_w,'0'),1,3),3,'0')	|| /*Pos 92 a 94*/
						'0'														|| /*Pos 95*/
						lpad('0',6,'0')											|| /*Pos 96 a 101*/
						lpad(' ',5,' ')											|| /*Pos 102 a 106*/
						lpad(substr(nvl(somente_numero(cd_carteira_w),'0'),1,2),2,'0')			|| /*Pos 107 a 108*/
						'01'													|| /*Pos 109 a 110*/
						lpad(nr_titulo_w,10,'0')								|| /*Pos 111 a 120*/
						dt_vencimento_w											|| /*Pos 121 a 126*/
						vl_saldo_titulo_w										|| /*Pos 127 a 139*/
						lpad(cd_banco_w,3,'0')									|| /*Pos 140 a 142*/
						lpad('0',4,'0')											|| /*Pos 143 a 146*/
						' '														|| /*Pos 147*/
						'12'													|| /*Pos 148 a 149*/
						'A'														|| /*Pos 150*/
						dt_emissao_w											|| /*Pos 151 a 156*/
						'01'													|| /*Pos 157 a 158*/
						'00'													|| /*Pos 159 a 160*/
						vl_juros_w												|| /*Pos 161 a 173*/
						lpad('0',6,'0')											|| /*Pos 174 a 179*/
						lpad('0',13,'0')										|| /*Pos 180 a 192*/
						lpad('0',13,'0')										|| /*Pos 193 a 205*/
						lpad('0',13,'0')										|| /*Pos 206 a 218*/
						lpad(nvl(ie_tipo_pessoa_w,0),2,'0')						|| /*Pos 219 a 220*/
						lpad(nvl(nr_inscricao_w,'0'),14,'0')					|| /*Pos 221 a 234*/
						rpad(nvl(nm_pessoa_w,' '),37,' ')						|| /*Pos 235 a 271*/
						lpad(' ',3,' ')											|| /*Pos 272 a 274*/
						rpad(nvl(ds_endereco_w,' '),40,' ')						|| /*Pos 275 a 314*/
						rpad(nvl(ds_bairro_w,' '),12,' ')						|| /*Pos 315 a 326*/
						lpad(nvl(ds_cep_w,' '),8,' ')							|| /*Pos 327 a 334*/
						rpad(nvl(ds_cidade_w,' '),15,' ')						|| /*Pos 335 a 349*/
						rpad(nvl(ds_uf_w,' '),2,' ')							|| /*Pos 350 a 351*/
						lpad(' ',40,' ')										|| /*Pos 352 a 391*/
						'00'													|| /*Pos 392 a 393*/
						' '														|| /*Pos 394*/
						lpad(nr_seq_registro_w,6,'0');			   				   /*Pos 395 a 400*/	
	
	insert into w_envio_banco (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
				values	 (	w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_apres_w);
	/*Fim segmento Obrigat�rio*/

	/*Inicio segmento opcional Multa*/
	if (vl_multa_w is not null) then
	
		nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
		nr_seq_apres_w	  := nvl(nr_seq_apres_w,0) + 1;
		ds_conteudo_w :='5'							|| /*Pos 01*/
						'99'						|| /*Pos 02 a 03*/
						'1'							|| /*Pos 04*/
						substr(dt_multa_w,1,6)		|| /*Pos 05 a 10*/
						substr(vl_multa_w,1,12)		|| /*Pos 11 a 22*/
						lpad(' ',372,' ')			|| /*Pos 23a a 394*/
						lpad(nr_seq_registro_w,6,'0');/*Pos 395 a 400*/	
					
		insert into w_envio_banco (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_estabelecimento,
						ds_conteudo,
						nr_seq_apres)
			values	 (	w_envio_banco_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_estabelecimento_p,
						ds_conteudo_w,
						nr_seq_apres_w);
	end if;
	/*Fim segmento segmento opcional Multa*/
	
	/*Inicio segmento opcional Email*/	
	if (ds_email_w is not null) then

		nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
		nr_seq_apres_w	  := nvl(nr_seq_apres_w,0) + 1;	
		
		ds_conteudo_w :='5'							|| /*Pos 01*/
						'01'						|| /*Pos 02 a 03*/
						rpad(ds_email_w,136,' ')	|| /*Pos 04 a 139*/
						rpad(' ',255,' ')			|| /*Pos 140 a 394*/
						lpad(nr_seq_registro_w,6,'0');/*Pos 395 a 400*/	
					
		insert into w_envio_banco (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_estabelecimento,
						ds_conteudo,
						nr_seq_apres)
			values	 (	w_envio_banco_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_estabelecimento_p,
						ds_conteudo_w,
						nr_seq_apres_w);		
	
	end if;
	/*Fim segmento opcional Email*/	
						
end	loop;
close	c01;

/*Inicio Trailer Arquivo*/
nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
nr_seq_apres_w	  := nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w := 	'9'								|| /*Pos 01*/
					rpad(' ',393,' ')				|| /*Pos 02 a 394*/
					lpad(nr_seq_registro_w,6,'0');	/*Pos 395 a 400*/	
					
insert into w_envio_banco (	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
	values	 (	w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_apres_w);			
/*Fim Trailer Arquivo*/				

commit;

end	GERAR_COBR_BRASIL_CNAB_400_REG;
/