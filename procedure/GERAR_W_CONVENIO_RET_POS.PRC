create or replace
procedure gerar_w_convenio_ret_pos
				(cd_estabelecimento_p	number,
				 nm_usuario_p		varchar2,
				 ie_tipo_data_p		varchar2,
				 dt_inicial_p		date,
				 dt_final_p		date,
				 ie_situacao_p		varchar2,
				 ds_lista_convenio_p	varchar2,
				 nr_titulo_p		number,
				 ds_tipos_atendimento_p	varchar2,
				 ie_quebra_guia_p		varchar2,
				 ie_clinica_p		varchar2) is

dt_emissao_w		date;
ds_convenio_w		varchar2(255);
nr_interno_conta_w		number(10);
nr_titulo_w		number(10);
nr_atendimento_w		number(10);
dt_vencimento_w		date;
nm_paciente_w		varchar2(60);
cd_senha_w		varchar2(20);
cd_usuario_convenio_w	varchar2(30);
vl_guia_w		number(15,2);
dt_pagamento_w		date;
vl_pago_w		number(15,2)	:= 0;
vl_glosa_w		number(15,2)	:= 0;
vl_desconto_w		number(15,2)	:= 0;
vl_variacao_pas_w		number(15,2)	:= 0;
vl_perdas_w		number(15,2)	:= 0;
vl_saldo_conta_w		number(15,2)	:= 0;
vl_variacao_ati_w		number(15,2)	:= 0;
vl_juros_w		number(15,2)	:= 0;
vl_amaior_w		number(15,2)	:= 0;
vl_pago_tit_w		number(15,2);
vl_glosa_tit_w		number(15,2);
vl_desconto_tit_w		number(15,2);
vl_perdas_tit_w		number(15,2);
vl_juros_tit_w		number(15,2);
vl_rec_maior_tit_w		number(15,2);
cd_tipo_recebimento_w	number(5);
nr_seq_trans_fin_w		number(10);
vl_saldo_ant_w		number(15,2);
nr_interno_conta_ant_w	number(10)	:= 0;
vl_ultimo_saldo_w		number(15,2);
vl_ultima_guia_w		number(15,2);
nr_seq_ult_reg_w		number(10);
ds_lista_convenio_w	varchar2(4000);
ds_lista_atendimentos_w	varchar2(4000);
ds_lista_clinica_w		varchar2(4000);
nr_seq_baixa_w		number(5);
nr_seq_protocolo_tit_w	number(10);
nr_interno_conta_tit_w	number(10);
nr_interno_conta_guia_w	number(10);
nr_seq_convret_item_w	number(10);
cd_autorizacao_guia_w	varchar2(255);
nr_doc_convenio_w	varchar2(255);
ds_clinica_w		varchar2(255);
dt_periodo_inicial_w	date;

cursor c00 is
select	/*+ USE_CONCAT */
	nr_titulo,
	nr_seq_protocolo,
	nr_interno_conta,
	obter_nome_convenio(cd_convenio),
	trunc(dt_emissao, 'dd'),
	trunc(dt_pagamento_previsto, 'dd')
from	titulo_receber_v
where	decode(ie_tipo_data_p, 'E', dt_emissao, 'P', dt_pagamento_previsto) between nvl(dt_inicial_p, decode(ie_tipo_data_p, 'E', dt_emissao, 'P', dt_pagamento_previsto)) and nvl(trunc(dt_final_p) + 89399/89400, decode(ie_tipo_data_p, 'E', dt_emissao, 'P', dt_pagamento_previsto))
and	ie_situacao		<> '3'
and	((ds_lista_convenio_p is null) or (ds_lista_convenio_w like '% ' || cd_convenio || ' %'))
and	nr_titulo		= nvl(nr_titulo_p, nr_titulo)
and	nr_interno_conta is not null
union
select	/*+ USE_CONCAT */
	nr_titulo,
	nr_seq_protocolo,
	nr_interno_conta,
	obter_nome_convenio(cd_convenio),
	trunc(dt_emissao, 'dd'),
	trunc(dt_pagamento_previsto, 'dd')
from	titulo_receber_v
where	decode(ie_tipo_data_p, 'E', dt_emissao, 'P', dt_pagamento_previsto) between nvl(dt_inicial_p, decode(ie_tipo_data_p, 'E', dt_emissao, 'P', dt_pagamento_previsto)) and nvl(trunc(dt_final_p) + 89399/89400, decode(ie_tipo_data_p, 'E', dt_emissao, 'P', dt_pagamento_previsto))
and	ie_situacao		<> '3'
and	((ds_lista_convenio_p is null) or (ds_lista_convenio_w like '% ' || cd_convenio || ' %'))
and	nr_titulo		= nvl(nr_titulo_p, nr_titulo)
and	nr_seq_protocolo is not null;


cursor c01 is
select	/*+ USE_CONCAT */
	a.nr_interno_conta,
	a.nr_atendimento,
	obter_pessoa_atendimento(a.nr_atendimento,'N'),
	nvl(f.cd_senha,substr(obter_senha_autorizacao(a.nr_atendimento, 'P'),1,254)) cd_senha,
	f.cd_usuario_convenio,
	substr(obter_valor_dominio(17, c.ie_clinica),1,254) ds_clinica,
	a.dt_periodo_inicial
from	atend_categoria_convenio f,
	conta_paciente a,
	atendimento_paciente c
where	a.nr_atendimento	= c.nr_atendimento
and	a.nr_interno_conta	= nr_interno_conta_tit_w
and	f.nr_atendimento	= a.nr_atendimento
and	f.nr_seq_interno	= obter_atecaco_atendimento(a.nr_atendimento)
and	((ds_tipos_atendimento_p is null) or (ds_lista_atendimentos_w like '% ' || to_char(c.ie_tipo_atendimento) || ' %'))
and	((ie_clinica_p is null) or ((ds_lista_clinica_w like '% ' || to_char(c.ie_clinica) || ' %') and c.ie_clinica is not null))
union
select	/*+ USE_CONCAT */
	a.nr_interno_conta,
	a.nr_atendimento,
	obter_pessoa_atendimento(a.nr_atendimento,'N'),
	nvl(f.cd_senha,substr(obter_senha_autorizacao(a.nr_atendimento, 'P'),1,254)) cd_senha,
	f.cd_usuario_convenio,
	substr(obter_valor_dominio(17, c.ie_clinica),1,254) ds_clinica,
	a.dt_periodo_inicial
from	atend_categoria_convenio f,
	conta_paciente a,
	atendimento_paciente c
where	a.nr_atendimento	= c.nr_atendimento
and	a.nr_seq_protocolo	= nr_seq_protocolo_tit_w
and	f.nr_atendimento	= a.nr_atendimento
and	f.nr_seq_interno	= obter_atecaco_atendimento(a.nr_atendimento)
and	((ds_tipos_atendimento_p is null) or (ds_lista_atendimentos_w like '% ' || to_char(c.ie_tipo_atendimento) || ' %'))
and	((ie_clinica_p is null) or ((ds_lista_clinica_w like '% ' || to_char(c.ie_clinica) || ' %') and c.ie_clinica is not null))
order	by nr_interno_conta;

cursor c02 is
select	nr_interno_conta,
	decode(ie_quebra_guia_p, 'S', cd_autorizacao, '-1'),
	sum(vl_guia)
from	conta_paciente_guia
where	nr_interno_conta	= nr_interno_conta_w
having	sum(vl_guia)		<> 0
group 	by nr_interno_conta,
	decode(ie_quebra_guia_p, 'S', cd_autorizacao, '-1')
order 	by decode(ie_quebra_guia_p, 'S', cd_autorizacao, '-1');

cursor c03 is
select	nr_sequencia
from	convenio_retorno_item
where	nr_interno_conta						= nr_interno_conta_guia_w
and	decode(ie_quebra_guia_p, 'S', cd_autorizacao, '-1')		= cd_autorizacao_guia_w
union
select	0 nr_sequencia
from	dual
where	not exists
	(select	x.nr_sequencia
	from	convenio_retorno_item x
	where	x.nr_interno_conta	= nr_interno_conta_guia_w
	and	decode(ie_quebra_guia_p, 'S', x.cd_autorizacao, '-1')	= cd_autorizacao_guia_w)
order	by nr_sequencia;

cursor c04 is
select	a.dt_recebimento,
	sum(nvl(a.vl_recebido,0)) vl_recebido,
	sum(nvl(a.vl_glosa,0)) vl_glosa,
	sum(nvl(a.vl_descontos,0)) vl_descontos,
	sum(nvl(a.vl_perdas,0)) vl_perdas,
	sum(nvl(a.vl_juros,0)) vl_juros,
	sum(nvl(a.vl_rec_maior,0)) vl_rec_maior,
	a.cd_tipo_recebimento,
	a.nr_seq_trans_fin,
	a.nr_sequencia
from	titulo_receber_liq a
where	a.nr_seq_ret_item		= nr_seq_convret_item_w
and	nr_seq_convret_item_w	<> 0
having	count(*) 		> 0
group 	by a.cd_tipo_recebimento,
	a.nr_seq_trans_fin,
	a.nr_sequencia,
	a.dt_recebimento
union
select	/*+ USE_CONCAT */
	to_date(null) dt_recebimento,
	0 vl_recebido,
	0 vl_glosa,
	0 vl_descontos,
	0 vl_perdas,
	0 vl_juros,
	0 vl_rec_maior,
	to_number(null) cd_tipo_recebimento,
	to_number(null) nr_seq_trans_fin,
	to_number(null) nr_sequencia
from	dual
where	((nr_seq_convret_item_w	= 0) or 
	not exists (	select	1
			from	titulo_receber_liq x
			where	x.nr_seq_ret_item	= nr_seq_convret_item_w))
order 	by dt_recebimento,
	nr_sequencia;

begin

ds_lista_convenio_w	:= ' ' || replace(replace(replace(ds_lista_convenio_p,'(',' '),')',' '),',',' ') || ' ';
ds_lista_atendimentos_w	:= ' ' || replace(replace(replace(ds_tipos_atendimento_p,'(',' '),')',' '),',',' ') || ' ';
ds_lista_clinica_w	:= ' ' || replace(replace(replace(ie_clinica_p,'(',' '),')',' '),',',' ') || ' ';

--raise_application_error(-20011, 'ds_lista_clinica_w = ' || ds_lista_clinica_w || chr(13) || 
--				'ds_lista_convenio_w = ' || ds_lista_convenio_w);

delete	from w_convenio_ret_pos
where	nm_usuario	= nm_usuario_p
or	dt_atualizacao	< sysdate - 1;	

commit;

open c00;
loop
fetch c00 into 
	nr_titulo_w,
	nr_seq_protocolo_tit_w,
	nr_interno_conta_tit_w,
	ds_convenio_w,
	dt_emissao_w,
	dt_vencimento_w;
exit when c00%notfound;

	vl_saldo_conta_w	:= 0;
	open c01;
	loop
	fetch c01 into
		nr_interno_conta_w,
		nr_atendimento_w,
		nm_paciente_w,
		cd_senha_w,
		cd_usuario_convenio_w,
		ds_clinica_w,
		dt_periodo_inicial_w;
	exit when c01%notfound;

		select	nvl(max(nr_doc_convenio), 'S/ Guia')
		into	nr_doc_convenio_w
		from	atend_categoria_convenio
		where	nr_atendimento	= nr_atendimento_w
		and	nr_seq_interno	= obter_atecaco_atendimento(nr_atendimento);

		select	nvl(sum(b.vl_glosa),0)
		into	vl_glosa_w
		from	lote_audit_hist_item b,
			lote_audit_hist_guia a
		where	a.nr_interno_conta	= nr_interno_conta_w
		and	a.nr_sequencia		= b.nr_seq_guia;

		vl_glosa_w	:= nvl(vl_glosa_w,0) + to_number(nvl(obter_valores_conpaci(nr_interno_conta_w,nr_seq_protocolo_tit_w,'G'),0));

		vl_saldo_conta_w	:= 0;
		open c02;
		loop
		fetch c02 into
			nr_interno_conta_guia_w,
			cd_autorizacao_guia_w,
			vl_guia_w;
		exit when c02%notfound;

			vl_saldo_conta_w		:= vl_guia_w;

			open c03;
			loop
			fetch c03 into
				nr_seq_convret_item_w;
			exit when c03%notfound;

				open c04;
				loop
				fetch c04 into
					dt_pagamento_w,
					vl_pago_tit_w,
					vl_glosa_tit_w,
					vl_desconto_tit_w,
					vl_perdas_tit_w,
					vl_juros_tit_w,
					vl_rec_maior_tit_w,
					cd_tipo_recebimento_w,
					nr_seq_trans_fin_w,
					nr_seq_baixa_w;
				exit when c04%notfound;

					vl_ultimo_saldo_w	:= null;
					vl_glosa_w		:= 0;
					vl_desconto_w		:= 0;
					vl_variacao_pas_w	:= 0;
					vl_perdas_w		:= 0;
					vl_variacao_ati_w	:= 0;
					vl_pago_w		:= vl_pago_tit_w;

					/*Glosa de convenio - ahoffelder - OS 233074 - 01/09/2010 - comentei esse comando
					if	(cd_tipo_recebimento_w = 9) or
						(nr_seq_trans_fin_w = 228) then
						vl_glosa_w	:= nvl(vl_glosa_tit_w,0); */
					/* desconto */
					if	(cd_tipo_recebimento_w = 7) then
						vl_desconto_w	:= nvl(vl_desconto_tit_w,0);
					/* Var passiva */
					elsif	(cd_tipo_recebimento_w = 15) then
						vl_variacao_pas_w	:= nvl(vl_desconto_tit_w,0);
					/* Perdas */
					elsif	(cd_tipo_recebimento_w = 14) then
						vl_perdas_w	:= nvl(vl_perdas_tit_w,0);
					/* Variacao ativa */
					elsif	(cd_tipo_recebimento_w = 17) then
						vl_variacao_ati_w	:=  nvl(vl_rec_maior_tit_w,0);
					end if;

					/* Valor a maior */
					if	(cd_tipo_recebimento_w <> 17) then
						vl_amaior_w	:= nvl(vl_rec_maior_tit_w,0);
					end if;

					vl_saldo_conta_w	:= vl_saldo_conta_w - vl_pago_tit_w - vl_desconto_w - vl_glosa_w - vl_variacao_pas_w - vl_perdas_w;
					vl_ultima_guia_w	:= vl_guia_w;
					
					select	w_convenio_ret_pos_seq.nextval
					into	nr_seq_ult_reg_w
					from	dual;

					insert	into	w_convenio_ret_pos
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_emissao,
						ds_convenio,
						nr_interno_conta,
						nr_titulo,
						nr_atendimento,
						dt_vencimento,
						nm_paciente,
						cd_senha,
						cd_guia,
						cd_usuario_convenio,
						vl_guia,
						dt_pagamento,
						vl_pago,
						vl_glosa,
						vl_desconto,
						vl_variacao_pas,
						vl_perdas,
						vl_saldo_conta,
						vl_variacao_ati,
						vl_juros,
						vl_amaior,
						nr_seq_baixa,
						ds_clinica,
						dt_periodo_inicial)
					select	nr_seq_ult_reg_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						dt_emissao_w,
						ds_convenio_w,
						nr_interno_conta_w,
						nr_titulo_w,
						nr_atendimento_w,
						dt_vencimento_w,
						nm_paciente_w,
						cd_senha_w,
						decode(ie_quebra_guia_p, 'S', cd_autorizacao_guia_w, nr_doc_convenio_w),
						cd_usuario_convenio_w,
						vl_guia_w,
						dt_pagamento_w,
						vl_pago_w,
						vl_glosa_w,
						vl_desconto_tit_w,
						vl_variacao_pas_w,
						vl_perdas_w,
						vl_saldo_conta_w,
						vl_variacao_ati_w,
						vl_juros_w,
						vl_amaior_w,
						nr_seq_baixa_w,
						ds_clinica_w,
						dt_periodo_inicial_w
					from	dual;

				end loop;
				close c04;

			end loop;
			close c03;

			/* Atualizar o saldo somente no ultimo registro de cada conta */
			/* Atualizar o valor da conta somente no ultimo registro de cada conta */

			if	(ie_quebra_guia_p = 'S') then
				update	w_convenio_ret_pos
				set	vl_ultimo_saldo			= vl_saldo_conta_w,
					vl_ultima_guia			= vl_ultima_guia_w
				where	cd_guia				= decode(ie_quebra_guia_p, 'S', cd_autorizacao_guia_w, nr_doc_convenio_w)
				and	nr_interno_conta		= nr_interno_conta_guia_w
				and	rownum				= 1;
			else
				update	w_convenio_ret_pos
				set	vl_ultimo_saldo			= vl_saldo_conta_w,
					vl_ultima_guia			= vl_ultima_guia_w
				where	nr_interno_conta		= nr_interno_conta_guia_w
				and	nr_sequencia			= nr_seq_ult_reg_w;
			end if;

		end loop;
		close c02;
	end loop;
	close c01;
end loop;
close c00;

commit;

end gerar_w_convenio_ret_pos;
/
