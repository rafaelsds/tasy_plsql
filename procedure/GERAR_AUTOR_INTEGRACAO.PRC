create or replace
procedure Gerar_Autor_Integracao(nr_seq_procedimento_p		number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				cd_proced_tuss_p		number,
				cd_proced_convenio_p		varchar2,
				qt_solicitada_p			number default 1,
				nr_sequencia_autor_p		number,
				nm_usuario_p			Varchar2,
				ie_commit_p			varchar2 default 'N') is 
				
qt_regra_integracao_w		number(5) := 0;
cd_procedimento_tuss_w		number(15);
qt_solicitada_w			number(10);
lista_procedimentos_w		varchar2(2000);

cd_area_proc_w			number(15);
cd_espec_proc_w			number(15);
cd_grupo_proc_w			number(15);

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_tipo_autorizacao_w		autorizacao_convenio.ie_tipo_autorizacao%type;
ie_resp_autor_w			autorizacao_convenio.ie_resp_autor%type;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;	
cd_convenio_w			convenio.cd_convenio%type;

nr_seq_regra_w			number(10);
qt_autor_integrada_w		number(5) := 0;
ie_tipo_regra_w			varchar2(1);

Cursor CRegra is
select	c.nr_sequencia,
	Nvl(c.ie_tipo_regra,'G')
from	regra_autor_integracao c
where	c.cd_estabelecimento 	= cd_estabelecimento_w
and	c.cd_convenio 		= cd_convenio_w
and	nvl(c.ie_situacao,'A') = 'A'
and	Nvl(c.ie_resp_autor,Nvl(ie_resp_autor_w,'0')) 		= Nvl(ie_resp_autor_w,'0')
and	nvl(c.ie_tipo_autorizacao,Nvl(ie_tipo_autorizacao_w,'0'))	= Nvl(ie_tipo_autorizacao_w,'0')
and	nvl(C.ie_tipo_atendimento,nvl(ie_tipo_atendimento_w,0))	= nvl(ie_tipo_atendimento_w,0)
and	Nvl(c.cd_procedimento,cd_procedimento_p) 		= cd_procedimento_p 
and	((c.cd_procedimento is null) or (Nvl(c.ie_origem_proced,ie_origem_proced_p) = ie_origem_proced_p))
and	Nvl(c.cd_area_procedimento,cd_area_proc_w)		= cd_area_proc_w
and	Nvl(c.cd_grupo_proc,cd_grupo_proc_w)			= cd_grupo_proc_w
and	Nvl(c.cd_especialidade,cd_espec_proc_w)			= cd_espec_proc_w
order by nvl(c.cd_procedimento,0),
	nvl(c.cd_grupo_proc,0),
	nvl(c.cd_especialidade,0),
	nvl(c.cd_area_procedimento,0),
	nvl(c.ie_tipo_autorizacao,0),
	nvl(c.ie_tipo_atendimento,0),
	c.ie_resp_autor desc;




begin

if	(qt_solicitada_p = 0) then
	qt_solicitada_w := 1;
else
	qt_solicitada_w := qt_solicitada_p;
end if;

begin
select	a.cd_estabelecimento,
	a.ie_tipo_autorizacao,
	a.ie_resp_autor,
	substr(obter_tipo_atend_autor(a.nr_atendimento,a.nr_seq_agenda,a.nr_seq_agenda_consulta,a.nr_seq_age_integ,a.nr_seq_paciente_setor,'C'),1,1),
	a.cd_convenio
into	cd_estabelecimento_w,
	ie_tipo_autorizacao_w,
	ie_resp_autor_w,
	ie_tipo_atendimento_w,
	cd_convenio_w
from	autorizacao_convenio a,
	estagio_autorizacao e
where	a.nr_sequencia = nr_sequencia_autor_p
and	a.cd_senha is null
and	a.nr_seq_estagio = e.nr_sequencia
and	e.ie_interno not in('70','10','90');
exception
when others then
	cd_convenio_w := null;
end;


select	count(*)
into	qt_regra_integracao_w
from	regra_autor_integracao
where	cd_estabelecimento = cd_estabelecimento_w
and	cd_convenio	= cd_convenio_w
and	nvl(ie_situacao,'A') = 'A';

if	(qt_regra_integracao_w > 0) then

	select	count(*)
	into	qt_autor_integrada_w
	from	autorizacao_integracao
	where	nr_sequencia_autor = nr_sequencia_autor_p;

	select	cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc
	into	cd_area_proc_w,
		cd_espec_proc_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;
	
	lista_procedimentos_w := null;
	open CRegra;
	loop
	fetch CRegra into	
		nr_seq_regra_w,
		ie_tipo_regra_w;
	exit when CRegra%notfound;
		begin
		nr_seq_regra_w := nr_seq_regra_w;
		ie_tipo_regra_w := ie_tipo_regra_w;
		end;
	end loop;
	close CRegra;
	
	
	
	if	(nr_seq_regra_w is not null) and
		(ie_tipo_regra_w = 'G')then
	
		if	(qt_autor_integrada_w = 0) then
		
			insert	into autorizacao_integracao
				(nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				cd_status_autorizacao,
				nr_sequencia_autor,
				nr_seq_regra_geracao,
				ds_lista_procedimentos,
				ds_lista_sequences)
			values	(autorizacao_integracao_seq.NEXTVAL,
				SYSDATE,
				SYSDATE,
				nm_usuario_p,
				nm_usuario_p,
				'P',
				nr_sequencia_autor_p,
				nr_seq_regra_w,
				'C'||Nvl(cd_proced_tuss_p,Nvl(cd_proced_convenio_p,cd_procedimento_p))||'Q'||Nvl(qt_solicitada_w,1),
				NR_SEQ_PROCEDIMENTO_P||',');
			
		else
		
			update	autorizacao_integracao a
			set	dt_atualizacao = sysdate,
				nm_usuario     = nm_usuario_p,
				ds_lista_procedimentos = ds_lista_procedimentos||'C'||Nvl(cd_proced_tuss_p,Nvl(cd_proced_convenio_p,cd_procedimento_p))||'Q'||Nvl(qt_solicitada_w,1),
				ds_lista_sequences     = ds_lista_sequences||NR_SEQ_PROCEDIMENTO_P||','
			where	a.nr_sequencia_autor   = nr_sequencia_autor_p;
		end if;
		
		
		
		
	end if;
	
	
end if;


if	(ie_commit_p = 'S') then
	 commit;
end if;

end Gerar_Autor_Integracao;
/