create or replace
procedure insert_imc_serv_dtls_resp( 	nr_seq_transaction_p	in imc_response.cd_transaction%type,
										cd_service_p			in imc_service_response.cd_service%type,
										ds_service_exp_p		in imc_service_details.ds_service_exp%type,
										cd_service_exp_p		in imc_service_details.cd_service_exp%type,
										cd_exp_row_p			in imc_service_details.cd_exp_row%type,
										nm_usuario_p			in imc_service_details.nm_usuario%type) is

nr_seq_w		imc_service_details.nr_sequencia%type;
nr_seq_sevice_w	imc_service_response.nr_sequencia%type;

begin

select	max(a.nr_sequencia)
into	nr_seq_sevice_w
from	imc_service_response a,
		imc_response b
where	a.cd_service = cd_service_p
and		a.nr_seq_response = b.nr_sequencia
and		b.cd_transaction = nr_seq_transaction_p;

select	imc_service_details_seq.nextval
into	nr_seq_w
from	dual;

insert into imc_service_details (
									nr_sequencia,
									nm_usuario,
									dt_atualizacao,
									nr_seq_service,
									dt_atualizacao_nrec,
									ds_service_exp,
									cd_service_exp,
									cd_exp_row,
									nm_usuario_nrec)
						values (							
									nr_seq_w,
									nm_usuario_p,
									sysdate,
									nr_seq_sevice_w,
									sysdate,
									ds_service_exp_p,
									cd_service_exp_p,
									cd_exp_row_p,
									nm_usuario_p);

commit;

end insert_imc_serv_dtls_resp;
/