CREATE OR REPLACE procedure gerar_encaixe_agenda_exame	(
		cd_estabelecimento_p	number,
		cd_agenda_p			number,
		dt_agenda_p			date,
		hr_encaixe_p			date,
		qt_duracao_p			number,
		cd_pessoa_fisica_p		varchar2,
		nm_pessoa_fisica_p		varchar2,
		cd_convenio_p			number,
		cd_categoria_p			varchar2,
		cd_medico_p			varchar2,
		cd_medico_exec_p		varchar2,
		cd_procedimento_p		number,
		ie_origem_proced_p		number,
		nr_seq_proc_interno_p		number,
		ie_lado_p			varchar2,
		ds_observacao_p			varchar2,
		nm_usuario_p			varchar2,
		cd_setor_atendimento_p		number,
		ie_forma_agendamento_p		varchar2,
		cd_plano_p			varchar2,
		nm_usuario_confirm_encaixe_p	varchar2,
		nr_seq_classif_p		number,
		nr_seq_encaixe_p out		number,
		nr_seq_segurado_p number default null,
		dt_val_carteira_p date default null,
		cd_usuario_convenio_p varchar2 default null,
		cd_empresa_ref_p number default null) is

dt_encaixe_w			date;
ds_consistencia_w		varchar2(255);
cd_turno_w				varchar2(1);
nr_seq_classif_w		number(10,0);
ie_forma_convenio_w		varchar2(2);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
cd_usuario_convenio_w	varchar2(30);
cd_plano_w				varchar2(10);
dt_validade_w			date;
nr_doc_convenio_w		varchar2(20);
cd_tipo_acomodacao_w	number(4,0);
nr_seq_agenda_w			number(10,0);
ie_proc_agenda_w		varchar2(1);
ds_observacao_aux		varchar2(4000);
cd_retorno_w			number(10,0);
ie_consist_js_w			varchar2(255) := '';
ie_atualiza_sala_turno_w varchar2(1);
nr_seq_sala_w			number(10,0);
nr_seq_turno_w			number(10,0);
qt_agendamentos_w		number(10);
qt_regra_w				number(10);
ds_mensagem_w			varchar2(255);
ie_cons_sobrep_hor_enc_w	varchar2(1);
qt_encaixe_turno_w		number(10) := 0;
qt_perm_enc_turno_w		number(10) := 0;
nr_seq_turno_val_marc_w	number(10);
hr_inicial_turno_w		date;
hr_final_turno_w		date;
ds_erro_w				varchar2(255);
ie_cons_regra_aut_w		varchar2(2) := '';
ie_regra_w			number(2,0);
nr_seq_regra_w			number(10,0);
ds_cons_erro_w			varchar2(255);
ds_procedimento_w		varchar2(255);
ds_convenio_w			varchar2(255);
qt_peso_w			pessoa_fisica.qt_peso%type;
qt_altura_cm_w			pessoa_fisica.qt_altura_cm%type;
ie_glosa_w			regra_ajuste_proc.ie_glosa%type;
nr_seq_regra_preco_w		regra_ajuste_proc.nr_sequencia%type;
ie_regra_encaixe_turno_w 	varchar2(1);
nr_seq_bloq_geral_w		agenda_bloqueio_geral.nr_sequencia%type;

begin
select	nvl(max(obter_valor_param_usuario(820,417, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'S'),
		nvl(max(obter_valor_param_usuario(820,440, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'S')
into	ie_cons_sobrep_hor_enc_w,
		ie_regra_encaixe_turno_w
from	dual;

select	nvl(max(obter_valor_param_usuario(820, 201, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'S')
into	ie_cons_regra_aut_w
from	dual;


if	(ie_cons_regra_aut_w = 'S') then
	consiste_plano_convenio(null,
			cd_convenio_p,
			cd_procedimento_p,
			ie_origem_proced_p,
			sysdate,
			1,
			null,
			cd_plano_p,
			'',
			ds_cons_erro_w,
			cd_setor_atendimento_p,
			null,
			ie_regra_w,
			null,
			nr_seq_regra_w,
			nr_seq_proc_interno_p,
			cd_categoria_p,
			cd_estabelecimento_p,
			0,
			cd_medico_exec_p,
			cd_pessoa_fisica_p,
			ie_glosa_w,
			nr_seq_regra_preco_w);
	if	(ie_regra_w in (1,2)) then
		ds_convenio_w		:= obter_nome_convenio(cd_convenio_p);
		ds_procedimento_w	:= obter_desc_procedimento(cd_procedimento_p,ie_origem_proced_p);
		wheb_mensagem_pck.exibir_mensagem_abort(290431, 'DS_PROCEDIMENTO='||ds_procedimento_w||';DS_CONVENIO='||ds_convenio_w);
	end if;
end if;

if	(cd_agenda_p is not null) and
	(dt_agenda_p is not null) and
	(hr_encaixe_p is not null) and
	(qt_duracao_p is not null) and
	((cd_pessoa_fisica_p is not null) or
	(nm_pessoa_fisica_p is not null)) and
	--(cd_convenio_p is not null) and
	--(cd_medico_p is not null) and
	--(cd_medico_exec_p is not null)
	(nm_usuario_p is not null) then

	/* obter horario agenda x encaixe */
	dt_encaixe_w := pkg_date_utils.get_DateTime(dt_agenda_p, hr_encaixe_p);

	if	(ie_cons_sobrep_hor_enc_w = 'S')then
		consistir_horario_agenda_exame(cd_agenda_p, dt_encaixe_w, qt_duracao_p, 'E', ds_consistencia_w);
		if	(ds_consistencia_w is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
		end if;
	end if;


	/* consistir regras procedimento x convenio */
	if	(cd_procedimento_p is not null) and
		(ie_origem_proced_p is not null) then
		consistir_proc_conv_agenda(cd_estabelecimento_p, cd_pessoa_fisica_p, dt_encaixe_w, cd_agenda_p, cd_convenio_p, cd_categoria_p, cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, cd_medico_exec_p,'E', cd_plano_p, ds_consistencia_w, ie_proc_agenda_w,null, cd_retorno_w, ie_consist_js_w, null, null, null, null, null, null);
		if	((ie_proc_agenda_w = 'N') or (ie_proc_agenda_w = 'H') or (ie_proc_agenda_w = 'Q')) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
		end if;
	end if;


	if	(nr_seq_proc_interno_p is not null) then
		Consistir_Regra_Agenda_Grupo(cd_agenda_p, dt_agenda_p, nr_seq_proc_interno_p, 0, nm_usuario_p, cd_estabelecimento_p, 0);
	end if;

	if	(cd_procedimento_p is not null) and
		(ie_origem_proced_p is not null) then
		consistir_proc_conv_agenda(cd_estabelecimento_p, cd_pessoa_fisica_p, dt_encaixe_w, cd_agenda_p, cd_convenio_p, cd_categoria_p, cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, cd_medico_p,'R', cd_plano_p, ds_consistencia_w, ie_proc_agenda_w,null, cd_retorno_w, ie_consist_js_w, null, null, null, null, null, null);

		if	((ie_proc_agenda_w = 'N') or (ie_proc_agenda_w = 'H') or (ie_proc_agenda_w = 'Q')) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
		end if;
	end if;

	/* obter parametros */
	select	nvl(max(obter_valor_param_usuario(820, 6, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
	into	ie_forma_convenio_w
	from	dual;

	select	nvl(max(obter_valor_param_usuario(820, 375, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
	into	ie_atualiza_sala_turno_w
	from	dual;

	/* obter turno */
	select	obter_turno_horario_agenda(cd_agenda_p, dt_encaixe_w)
	into	cd_turno_w
	from	dual;

	/*obter sequencia do turno*/
	select	obter_turno_encaixe_ageexame(cd_agenda_p, dt_encaixe_w)
	into	nr_seq_turno_w
	from	dual;


	if	(ie_atualiza_sala_turno_w = 'S') then
		select	max(nr_seq_sala)
		into	nr_seq_sala_w
		from	agenda_horario
		where	nr_sequencia = nr_seq_turno_w;
	end if;

	if ((ie_regra_encaixe_turno_w = 'N') and
		(nr_seq_turno_w is null)) then
		--Nao e permitido gerar um encaixe fora do horario do turno cadastrado. Parametro [440].
		wheb_mensagem_pck.Exibir_Mensagem_Abort(326563);
	end if;

	/* obter classificacao */
	--obter_classif_encaixe_agenda(nr_seq_classif_w);
	nr_seq_classif_w	:= nr_seq_classif_p;

	/* obter sequencia */
	select	agenda_paciente_seq.nextval
	into	nr_seq_agenda_w
	from	dual;

	Consistir_qtd_conv_regra(nr_seq_agenda_w, cd_convenio_p, dt_encaixe_w, cd_agenda_p, cd_pessoa_fisica_p, cd_categoria_w, cd_plano_w, cd_estabelecimento_p, nm_usuario_p, null, null, nr_seq_proc_interno_p, qt_agendamentos_w, qt_regra_w, ds_mensagem_w);

	select	max(nr_sequencia)
	into	nr_seq_turno_val_marc_w
	from	agenda_horario
	where	to_char(dt_encaixe_w, 'hh24:mi:ss') between to_char(hr_inicial,'hh24:mi:ss') and to_char(hr_final,'hh24:mi:ss')
	and		nvl(dt_inicio_vigencia, to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) <=
			to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	and		nvl(dt_final_vigencia, to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) >=
			to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	and		nr_minuto_intervalo > 0
	and		((dt_dia_semana = obter_cod_dia_semana(dt_agenda_p)) or ((dt_dia_semana = 9) and (obter_cod_dia_semana(dt_agenda_p) not in (7,1))))
	and		cd_agenda 		= cd_agenda_p
	and		qt_encaixe		is not null;

	if	(nr_seq_turno_val_marc_w is not null)then
		begin

		select	max(hr_inicial),
				max(hr_final),
				max(qt_encaixe)
		into	hr_inicial_turno_w,
				hr_final_turno_w,
				qt_perm_enc_turno_w
		from	agenda_horario
		where	nr_sequencia = nr_seq_turno_val_marc_w;

		if	(hr_inicial_turno_w is not null) and
			(hr_final_turno_w is not null)then
			select	count(*) + 1
			into	qt_encaixe_turno_w
			from	agenda_paciente
			where	nvl(ie_encaixe,'N')	= 'S'
			and		hr_inicio 		between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial_turno_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and		to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final_turno_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			and		ie_status_agenda	not in ('C', 'F', 'I', 'II', 'B', 'R')
			and		cd_agenda			= cd_agenda_p
			and		dt_geracao_encaixe is not null;
			--and		nr_seq_horario		= nr_seq_turno_val_marc_w;

		end if;

		exception
		when others then
			ds_erro_w := substr(sqlerrm,1,255);
		end;
	end if;

	if	(qt_perm_enc_turno_w is not null) and
		(nvl(qt_encaixe_turno_w,0) > nvl(qt_perm_enc_turno_w,0))then
		/*A quantidade limite de encaixes para este turno foi atingida! Verifique a qtd. permitida no cadastro dos horarios da agenda.*/
		wheb_mensagem_pck.exibir_mensagem_abort(260557);
	end if;

	nr_seq_bloq_geral_w := obter_se_bloq_geral_agenda (cd_estabelecimento_p,
							cd_agenda_p,
							null, --ie_classif_agenda_p
							nr_seq_classif_w,
							null, --cd_espec_agendamento_p
							cd_setor_atendimento_p,
							nr_seq_proc_interno_p,
							cd_procedimento_p,
							ie_origem_proced_p,
							cd_medico_p,
							dt_encaixe_w,
							'N',
							'N');
	if (nr_seq_bloq_geral_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(obter_mensagem_bloq_geral_age(nr_seq_bloq_geral_w));
	end if;

	select	max(qt_peso),
		max(qt_altura_cm)
	into	qt_peso_w,
		qt_altura_cm_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	/* gerar encaixe */
	insert into agenda_paciente	(
					nr_sequencia,
					cd_agenda,
					dt_agenda,
					hr_inicio,
					nr_minuto_duracao,
					ie_status_agenda,
					cd_pessoa_fisica,
					nm_paciente,
					dt_nascimento_pac,
					qt_idade_paciente,
					nr_telefone,
					cd_convenio,
					cd_medico,
					cd_procedimento,
					ie_origem_proced,
					nr_seq_proc_interno,
					ie_lado,
					ds_observacao,
					nr_seq_classif_agenda,
					cd_turno,
					ie_equipamento,
					dt_agendamento,
					nm_usuario_orig,
					cd_medico_exec,
					nm_usuario,
					dt_atualizacao,
					ie_encaixe,
					cd_setor_atendimento,
					ie_forma_agendamento,
					cd_categoria,
					nm_usuario_confirm_encaixe,
					dt_confirm_encaixe,
					qt_idade_meses,
					cd_perfil_ger_encaixe,
					nr_seq_sala,
					ds_email,
					qt_peso,
					qt_altura_cm,
					cd_plano,
					nr_seq_segurado,
					cd_usuario_convenio,
					cd_empresa_ref,
					dt_validade_carteira
					)
				values	(
					nr_seq_agenda_w,
					cd_agenda_p,
					trunc(dt_encaixe_w,'dd'),
					dt_encaixe_w,
					qt_duracao_p,
					'N',
					cd_pessoa_fisica_p,
					substr(nvl(obter_nome_pf(cd_pessoa_fisica_p), nm_pessoa_fisica_p),1,60),
					to_date(substr(obter_dados_pf(cd_pessoa_fisica_p,'DN'),1,10),'dd/mm/yyyy'),
					substr(obter_dados_pf(cd_pessoa_fisica_p,'I'),1,3),
					substr(obter_fone_pac_agenda(cd_pessoa_fisica_p),1,255),
					cd_convenio_p,
					cd_medico_p,
					cd_procedimento_p,
					ie_origem_proced_p,
					nr_seq_proc_interno_p,
					ie_lado_p,
					ds_observacao_p,
					nr_seq_classif_w,
					cd_turno_w,
					'N',
					sysdate,
					nm_usuario_p,
					cd_medico_exec_p,
					nm_usuario_p,
					sysdate,
					'S',
					cd_setor_atendimento_p,
					ie_forma_agendamento_p,
					cd_categoria_p,
					nm_usuario_confirm_encaixe_p,
					sysdate,
					obter_idade(to_date(obter_dados_pf(cd_pessoa_fisica_p,'DN'),'dd/mm/yyyy'),sysdate,'MM'),
					decode(nvl(obter_perfil_ativo,0),0,null,obter_perfil_ativo),
					nr_seq_sala_w,
					substr(obter_compl_pf(cd_pessoa_fisica_p, 1, 'M'),1,255),
					qt_peso_w,
					qt_altura_cm_w,
					cd_plano_p,
					nr_seq_segurado_p,
					cd_usuario_convenio_p,
					cd_empresa_ref_p,
					dt_val_carteira_p);

	/* obter dados convenio, caso usuario nao informar (Esta rotina devera permanecer aqui, antes do insert gera erro) */
	if	(cd_pessoa_fisica_p is not null) and
		(cd_convenio_p is null) and
		(ie_forma_convenio_w <> 'N') then
		gerar_convenio_agendamento(cd_pessoa_fisica_p, 2, nr_seq_agenda_w, ie_forma_convenio_w, cd_convenio_w, cd_categoria_w, cd_usuario_convenio_w, dt_validade_w, nr_doc_convenio_w, cd_tipo_acomodacao_w, cd_plano_w, nm_usuario_p, ds_observacao_aux);

		if (cd_convenio_w = '0') then
			cd_convenio_w := null;
		end if;

		if (cd_plano_w = '0') then
			cd_plano_w := null;
		end if;

		if (cd_categoria_w = '0') then
			cd_categoria_w := null;
		end if;



		if	(cd_convenio_w is not null) then

			update	agenda_paciente
			set	cd_convenio		= cd_convenio_w,
				cd_categoria		= cd_categoria_w,
				cd_usuario_convenio	= cd_usuario_convenio_w,
				dt_validade_carteira	= dt_validade_w,
				nr_doc_convenio	= nr_doc_convenio_w,
				cd_tipo_acomodacao	= cd_tipo_acomodacao_w,
				cd_plano		= cd_plano_w
			where	nr_sequencia		= nr_seq_agenda_w;
		end if;
	end if;
/*
else
	20011, 'Voce deve informar todos os dados para a geracao do encaixe');*/
end if;

delete 	agenda_controle_horario
where 	cd_agenda = cd_agenda_p
and 	dt_agenda = trunc(dt_agenda_p);

commit;

nr_seq_encaixe_p := nr_seq_agenda_w;

end gerar_encaixe_agenda_exame;
/