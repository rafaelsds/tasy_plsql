create or replace
procedure Ajustar_raterio_tit_pagar (
			nr_titulo_p		Varchar2) is 

nr_titulo_w		number(10);
nr_sequencia_w		number(5);
cd_conta_contabil_w	varchar2(20);
cd_centro_custo_w		number(8);
nr_seq_conta_financ_w	number(10);
nr_seq_trans_fin_w		number(10);
nr_contrato_w		number(10);
nr_seq_produto_w		number(10);
vl_desconto_w		number(15,2);
vl_original_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_titulo_w		number(15,2);

ds_observacao_w		varchar2(2000);
ds_observacao_aux_w	varchar2(2000);

cursor c01 is
select	min(nr_sequencia),
	nr_titulo,
	nvl(cd_conta_contabil,0),
	nvl(cd_centro_custo,0),
	nvl(nr_seq_conta_financ,0),
	nvl(nr_seq_trans_fin,0),
	nvl(nr_contrato,0),
	nvl(nr_seq_produto,0),
	sum(vl_desconto),
	sum(vl_original),
	sum(vl_acrescimo),
	sum(vl_titulo)
from	titulo_pagar_classif
where	nr_titulo = nr_titulo_p
group by	nr_titulo,
	cd_conta_contabil,
	cd_centro_custo,
	nr_seq_conta_financ,
	nr_seq_trans_fin,
	nr_contrato,
	nr_seq_produto;
	
cursor c02 is
select	ds_observacao
from	titulo_pagar_classif
where	nr_titulo = nr_titulo_w
and	nvl(cd_conta_contabil,0) = cd_conta_contabil_w
and	nvl(cd_centro_custo,0) = cd_centro_custo_w
and	nvl(nr_seq_conta_financ,0) = nr_seq_conta_financ_w
and	nvl(nr_seq_trans_fin,0) = nr_seq_trans_fin_w
and	nvl(nr_contrato,0) = nr_contrato_w
and	nvl(nr_seq_produto,0) = nr_seq_produto_w
and	ds_observacao is not null;	

begin

open c01;
loop
fetch c01 into	
	nr_sequencia_w,
	nr_titulo_w,
	cd_conta_contabil_w,
	cd_centro_custo_w,
	nr_seq_conta_financ_w,
	nr_seq_trans_fin_w,
	nr_contrato_w,
	nr_seq_produto_w,
	vl_desconto_w,
	vl_original_w,
	vl_acrescimo_w,
	vl_titulo_w;
exit when c01%notfound;
	begin
	ds_observacao_w := null;
	
	open c02;
	loop
	fetch c02 into	
		ds_observacao_aux_w;
	exit when c02%notfound;
		begin
		ds_observacao_w := substr(ds_observacao_w || chr(13) || chr(10) || ds_observacao_aux_w,1,2000);
		end;
	end loop;
	close c02;
	
	update	titulo_pagar_classif
	set	vl_desconto = vl_desconto_w,
		vl_original = vl_original_w,
		vl_acrescimo = vl_acrescimo_w,
		vl_titulo = vl_titulo_w,
		ds_observacao = ds_observacao_w
	where	nr_titulo = nr_titulo_w
	and	nr_sequencia = nr_sequencia_w;
	
	delete	titulo_pagar_classif
	where	nr_titulo = nr_titulo_w
	and	nvl(cd_conta_contabil,0) = cd_conta_contabil_w
	and	nvl(cd_centro_custo,0) = cd_centro_custo_w
	and	nvl(nr_seq_conta_financ,0) = nr_seq_conta_financ_w
	and	nvl(nr_seq_trans_fin,0) = nr_seq_trans_fin_w
	and	nvl(nr_contrato,0) = nr_contrato_w
	and	nvl(nr_seq_produto,0) = nr_seq_produto_w
	and	nr_sequencia > nr_sequencia_w;
	end;
end loop;
close c01;

commit;

end Ajustar_raterio_tit_pagar;
/