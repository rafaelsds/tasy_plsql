create or replace
procedure Atualiza_pf_solicitacao_aghos(nr_internacao_p		number,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2) is 

cd_pessoa_atend_w	varchar2(10);					
ds_stack_w		varchar2(4000);

begin

begin
	select	cd_pessoa_fisica
	into	cd_pessoa_atend_w
	from	solicitacao_tasy_aghos
	where	nr_atend_original =    (select	max(nr_atend_original)
					from	solicitacao_tasy_aghos
					where	nr_internacao = nr_internacao_p)
	and	rownum  = 1;
exception
when others then
	cd_pessoa_atend_w := cd_pessoa_fisica_p;
end;

update	solicitacao_tasy_aghos
set	cd_pessoa_fisica = cd_pessoa_fisica_p
where	nr_internacao = nr_internacao_p;

if (cd_pessoa_atend_w <> cd_pessoa_fisica_p) then
	ds_stack_w	:= substr('Internacao: ' || nr_internacao_p || ' --- Stack --- ',1,4000);
	ds_stack_w	:= ds_stack_w || substr(dbms_utility.format_call_stack,1,4000);
	
	insert into log_mov(DT_ATUALIZACAO, NM_USUARIO, DS_LOG, CD_LOG) values (sysdate,'LOG_PESSOA_TASY',ds_stack_w,45675);
	
end if;

commit;

end Atualiza_pf_solicitacao_aghos;
/
