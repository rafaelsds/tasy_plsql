create or replace
procedure tx_registrar_receb_soroteca(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) then
	
	update	tx_recep_controle_central
	set	dt_recebimento = sysdate,
		nm_usuario_receb = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;	
		
end if;	

commit;

end tx_registrar_receb_soroteca;
/