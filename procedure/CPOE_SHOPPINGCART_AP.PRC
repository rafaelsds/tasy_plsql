create or replace 
procedure cpoe_shoppingcart_ap(nr_sequencia_p	number,
					ie_retrogrado_p	varchar2,
					ds_retorno_p	out varchar2) is

qt_count_w	number(10);
ie_return_value_w	varchar2(1) := 'N';

cursor c01 is
select  a.*
from 	cpoe_anatomia_patologica a
where	nr_sequencia = nr_sequencia_p;

begin
ds_retorno_p := 'S';

if (nr_sequencia_p is not null) then
	for r_c01_w in C01
		loop

		if (r_c01_w.nr_seq_proc_interno is null and r_c01_w.dt_inicio is null) then
			ds_retorno_p := 'N';
			goto return_value;
		end if;
		
		if (r_c01_w.dt_inicio < sysdate) then
			ds_retorno_p := 'N';
			goto return_value;
		end if;

    select	decode(count(*), 0, 'N', 'S') ie_duplicado 
		into	ie_return_value_w
		from	cpoe_anatomia_patologica a
		where	a.nr_sequencia <> r_c01_w.nr_sequencia
    and   a.nr_seq_proc_interno = r_c01_w.nr_seq_proc_interno
		and		((nr_atendimento = r_c01_w.nr_atendimento) or (cd_pessoa_fisica = r_c01_w.cd_pessoa_fisica and nr_atendimento is null))
		and		(((dt_lib_suspensao is not null) and (dt_suspensao > sysdate)) or (dt_lib_suspensao is null))
		and 	((dt_liberacao is not null) or (nm_usuario = wheb_usuario_pck.get_nm_usuario))
		and		obter_se_cpoe_regra_duplic('AP', wheb_usuario_pck.get_cd_perfil, r_c01_w.cd_setor_atendimento) = 'S'
		and 	((a.dt_inicio between r_c01_w.dt_inicio and r_c01_w.dt_fim) or
				 (a.dt_fim between r_c01_w.dt_inicio and r_c01_w.dt_fim) or
				 (a.dt_fim is null and a.dt_inicio < r_c01_w.dt_inicio) or
				 (a.dt_fim > r_c01_w.dt_fim and a.dt_inicio < r_c01_w.dt_inicio) or	
				 ((a.dt_inicio >  r_c01_w.dt_inicio) or (r_c01_w.dt_inicio <= a.dt_inicio  and r_c01_w.dt_fim > a.dt_inicio)) or
				 (a.ie_retrogrado = 'S' and a.dt_liberacao is null));

		if (ie_return_value_w = 'S') then
			ds_retorno_p := 'N';
			goto return_value;
		end if;

	end loop;
end if;
			
<<return_value>>
 null;
exception when others then
	gravar_log_cpoe(substr('CPOE_SHOPPINGCART_AP EXCEPTION:'|| substr(to_char(sqlerrm),1,2000) || '//nr_sequencia_p: '|| nr_sequencia_p,1,40000));
	ds_retorno_p := 'N';
end cpoe_shoppingcart_ap;
/
