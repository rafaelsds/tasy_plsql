create or replace
procedure Zerar_Prescr_Proc_Seq_LAB is

nr_seq_grupo_w	 number(10);
ie_regra_w		 varchar2(1);
dt_sequencia_w	 date;
nr_seq_inic_w    lab_grupo_impressao.nr_seq_inic%type;

cursor C01 is
	select
        a.nr_sequencia,
		substr(a.ie_regra_sequencia,1,1) ie_regra_sequencia,
        nvl(decode(a.ie_cons_seq_inic_regra, 'S', nvl(max(b.nr_seq_inicial), a.nr_seq_inic), 0), 0) nr_seq_inic
	from
        lab_grupo_impressao a
        left join lab_grupo_imp_vig b on ( b.nr_seq_grupo_impressao = a.nr_sequencia and
            sysdate between b.dt_inicio and b.dt_final)
	where
        a.ie_regra_sequencia is not null
    group by
        a.nr_sequencia, a.ie_regra_sequencia, a.nr_seq_inic, a.ie_cons_seq_inic_regra
    order by
        a.nr_sequencia;

BEGIN

open C01;
loop
fetch C01 into
        nr_seq_grupo_w,
		ie_regra_w,
        nr_seq_inic_w;
	exit when C01%notfound;

	select	nvl(max(dt_sequencia),sysdate)
	into	dt_sequencia_w
	from	lab_seq_grupo_imp
	where	nr_seq_grupo_imp		= nr_seq_grupo_w;

	if	((ie_regra_w = 'D') and
		 (trunc(dt_sequencia_w,'dd') < trunc(sysdate,'dd'))) or
		((ie_regra_w = 'S') and
		 (trunc(dt_sequencia_w,'day') < trunc(sysdate,'day'))) or
		((ie_regra_w = 'M') and
		 (trunc(dt_sequencia_w,'month') < trunc(sysdate,'month'))) or
		((ie_regra_w = 'A') and
		 (trunc(dt_sequencia_w,'year') < trunc(sysdate,'year'))) then

		insert into lab_seq_grupo_imp (nr_sequencia, nr_seq_grupo_imp, dt_sequencia,
						 nr_valor_seq, dt_atualizacao, nm_usuario)
		values (lab_seq_grupo_imp_seq.NextVal, nr_seq_grupo_w, trunc(sysdate,'dd'),
			nr_seq_inic_w, sysdate, 'Tasy');
	end if;
end loop;
close c01;

commit;

END Zerar_Prescr_Proc_Seq_LAB;
/
