create or replace
procedure Atualizar_Usu_Inseriu_Result(
					cd_pf_realizou_p		Varchar2,
					nr_seq_exame_lote_p	number) is 

cd_pf_realizou_w	varchar2(10);			
	
begin
select	nvl(max(cd_pf_realizou),null)
into	cd_pf_realizou_w
from  	san_exame_lote 
where   nr_sequencia = nr_seq_exame_lote_p;

if	(cd_pf_realizou_w is null) then
	
	update	san_exame_lote
	set	cd_pf_realizou = cd_pf_realizou_p
	where	nr_sequencia = nr_seq_exame_lote_p;
	
end if;

commit;

end Atualizar_Usu_Inseriu_Result;
/