create or replace
procedure define_preco_tuss
			(CD_ESTABELECIMENTO_P			NUMBER,
			CD_CONVENIO_P				NUMBER,
			CD_CATEGORIA_P				VARCHAR2,
			DT_CONTA_P				DATE,
			CD_PROCEDIMENTO_P			NUMBER,
			IR_ORIGEM_PROCED_P			NUMBER,
			CD_TIPO_ACOMODACAO_P			NUMBER,
			IE_TIPO_ATENDIMENTO_P			NUMBER,
			CD_SETOR_ATENDIMENTO_P			NUMBER,
			CD_MEDICO_P				VARCHAR2,
			CD_FUNCAO_MEDICO_P			NUMBER,
			QT_IDADE_P				NUMBER,
			nr_seq_exame_lab_p			NUMBER,
			nr_seq_proc_interno_p			NUMBER,
			cd_usuario_convenio_p			VARCHAR2,
			cd_plano_p				VARCHAR2,
			ie_clinica_p				NUMBER,
			cd_empresa_ref_p			NUMBER,
			ie_sexo_p				VARCHAR2,			
			nr_sequencia_p				NUMBER,
			ie_atend_retorno_p			VARCHAR2,
			QT_DIAS_INTERNACAO_P			NUMBER,
			ie_video_p				VARCHAR2,
			ie_beira_leito_p			VARCHAR2,
			ie_spect_p				varchar2,
			cd_cgc_prestador_p			varchar2,
			cd_equipamento_p			number,
			nr_seq_tipo_acidente_p  		number,
			cd_especialidade_medica_p		number,			
			ie_vago_p				varchar2,
			ie_calculo_tuss_p			varchar,
			CD_EDICAO_AMB_P			IN OUT	NUMBER,
			VL_PROCEDIMENTO_P		OUT	NUMBER,
			VL_CUSTO_OPERACIONAL_P		OUT 	NUMBER,
			VL_ANESTESISTA_P		OUT 	NUMBER,
			VL_MEDICO_P			OUT	NUMBER,
			VL_AUXILIARES_P			OUT	NUMBER,
			VL_MATERIAIS_P			OUT	NUMBER,
			VL_PTO_PROCEDIMENTO_P		OUT 	NUMBER,
			VL_PTO_CUSTO_OPERAC_P		OUT 	NUMBER,
			VL_PTO_ANESTESISTA_P		OUT 	NUMBER,
			VL_PTO_MEDICO_P			OUT 	NUMBER,
			VL_PTO_AUXILIARES_P		OUT 	NUMBER,
			VL_PTO_MATERIAIS_P		OUT 	NUMBER,
			QT_PORTE_ANESTESICO_P		OUT 	NUMBER,
			ie_valor_informado_p		OUT	VARCHAR2) is 

begin

if	(ie_calculo_tuss_p = 'A') then --AMB

	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278937);

elsif	(ie_calculo_tuss_p = 'C') then --CBHPM
	
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278938);
	
end if;

WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(278939);

end define_preco_tuss;
/