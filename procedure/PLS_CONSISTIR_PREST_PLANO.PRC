create or replace
procedure pls_consistir_prest_plano
			(	ie_evento_p			varchar2,
				nr_seq_prestador_p		number,
				nr_seq_conta_p			number,
				nr_seq_guia_p			number,
				nr_seq_requisicao_p		number,
				nr_seq_execucao_p		number,
				nr_seq_exec_req_item_proc_p	number,
				nr_seq_exec_req_item_mat_p	number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is
								
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realiza a consistencia  de prestador do plano.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  x]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_lanca_glosa_w		varchar2(2)	:= 'N';
ie_permite_w			varchar2(1)	:= 'S';
ie_tipo_consistencia_w		varchar2(1);
ie_regulamentacao_w		varchar2(1);
ie_carater_internacao_w		varchar2(1);
ie_tipo_atendimento_w		varchar2(1);
nr_seq_plano_regra_w		number(10)	:= null;
nr_seq_conta_w			number(10);
nr_seq_guia_w			number(10);
nr_seq_prestador_w		number(10);
nr_seq_plano_guia_conta_w	number(10);
qt_inconsistencia_w		number(10);
nr_seq_internado_guia_w		number(10);
dt_ref_atendimento_w		date;
nr_seq_segurado_w		Number(10);
dt_inicio_proc_w		Date;
dt_procedimento_w		Date;
qt_alt_plano_w			Number(10);

/* NR_SEQ_PRESTADOR_P nao e mais utilizado */
Cursor C01 is
	select	nvl(ie_permite,'S') ie_permite,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		ie_carater_internacao,
		ie_tipo_atendimento
	from	pls_prestador_plano
	where	nr_seq_prestador	= nr_seq_prestador_w
	and	((nr_seq_plano = nvl(nr_seq_plano_guia_conta_w,nr_seq_plano)) or (nr_seq_plano is null))
	and	ie_situacao		= 'A'
	order by
		nvl(nr_seq_plano,0),
		nvl(ie_tipo_atendimento,' '),
		nvl(ie_carater_internacao,' ');
		
begin
if 	(nvl(nr_seq_guia_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'G';
elsif 	(nvl(nr_seq_conta_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'C';
elsif	(nvl(nr_seq_requisicao_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'R';
elsif	(nvl(nr_seq_execucao_p,0) > 0) then
	ie_tipo_consistencia_w	:= 'E';
end if;

if	(ie_tipo_consistencia_w in ('G','C','R','E')) then
	if	(ie_tipo_consistencia_w = 'C') then	
		--alterei para que seja obtido o produto do beneficiario direto da pls_segurado, pois o sistema so insere valor no NR_SEQ_PLANO  da pls_conta depois desta procedure. 
		begin
			if	(ie_evento_p = 'IA') then
				select	a.nr_seq_segurado,
					a.dt_atendimento_referencia,
					a.ie_carater_internacao_imp,
					pls_obter_internado_guia(a.nr_sequencia,'C')
				into	nr_seq_segurado_w,
					dt_ref_atendimento_w,
					ie_carater_internacao_w,
					ie_tipo_atendimento_w
				from	pls_conta a
				where	a.nr_sequencia	= nr_seq_conta_p;
				
				nr_seq_prestador_w:=	nr_seq_prestador_p;
			else
				select	a.nr_seq_segurado,
					a.nr_seq_prestador_exec,
					a.dt_atendimento_referencia,
					a.ie_carater_internacao,
					pls_obter_internado_guia(a.nr_sequencia,'C')
				into	nr_seq_segurado_w,
					nr_seq_prestador_w,
					dt_ref_atendimento_w,
					ie_carater_internacao_w,
					ie_tipo_atendimento_w
				from	pls_conta a
				where	a.nr_sequencia	= nr_seq_conta_p;
			end if;
		exception
		when others then
			nr_seq_segurado_w		:= null;
			nr_seq_prestador_w 		:= null;
			dt_ref_atendimento_w			:= trunc(sysdate,'dd');
		end;
		
		dt_ref_atendimento_w	:= trunc(dt_ref_atendimento_w,'dd');

		if	(nr_seq_segurado_w is not null) then
			begin
				select	max(dt_procedimento),
					max(dt_inicio_proc)
				into	dt_procedimento_w,
					dt_inicio_proc_w
				from	pls_conta_proc
				where	nr_seq_conta = nr_seq_conta_p;
			exception
			when others then
				dt_procedimento_w 	:= null;
				dt_inicio_proc_w 	:= null;
			end;

			if	(dt_procedimento_w is not null) then
				if	(dt_inicio_proc_w is not null) then
					begin
					dt_inicio_proc_w	:= to_date(to_char(dt_procedimento_w,'dd/mm/yyyy') || ' ' || to_char(dt_inicio_proc_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
					exception
					when others then
						dt_inicio_proc_w	:= dt_procedimento_w;
					end;
				else
					dt_inicio_proc_w	:= dt_procedimento_w;
				end if;
				
				select	count(1)
				into	qt_alt_plano_w
				from	pls_segurado_alt_plano
				where	nr_seq_segurado = nr_seq_segurado_w
				and	ie_situacao 	= 'A'
				and	rownum < 2;
				
				if	(qt_alt_plano_w > 0) then
					begin
						select	min(nr_seq_plano_ant)
						into	nr_seq_plano_guia_conta_w
						from	pls_segurado_alt_plano
						where	nr_seq_segurado = nr_seq_segurado_w
						and	ie_situacao = 'A'
						and	dt_alteracao > dt_inicio_proc_w;
					exception
					when others then
						nr_seq_plano_guia_conta_w := null;
					end;
				end if;
			end if;
		end if;
		
		if	(nr_seq_plano_guia_conta_w is null) then 
			begin
				select	nr_seq_plano
				into	nr_seq_plano_guia_conta_w
				from	pls_segurado
				where	nr_sequencia	= nr_seq_segurado_w;
			exception
			when others then
				nr_seq_plano_guia_conta_w			:= null;
			end;
		end if;
	elsif	(ie_tipo_consistencia_w = 'G') then	
		begin
		select	nr_seq_plano,
			nr_seq_prestador,
			trunc(dt_solicitacao,'dd'),
			ie_carater_internacao,
			pls_obter_internado_guia(nr_sequencia,'A')
		into	nr_seq_plano_guia_conta_w,
			nr_seq_prestador_w,
			dt_ref_atendimento_w,
			ie_carater_internacao_w,
			ie_tipo_atendimento_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
		exception
		when others then
			nr_seq_plano_guia_conta_w	:= null;
			nr_seq_prestador_w 		:= null;
			dt_ref_atendimento_w		:= trunc(sysdate,'dd');
		end;
	elsif	(ie_tipo_consistencia_w = 'R') then
		begin
		select	nr_seq_plano,
			nr_seq_prestador_exec,
			trunc(dt_requisicao,'dd'),
			ie_carater_atendimento,
			pls_obter_internado_guia(nr_sequencia,'R')
		into	nr_seq_plano_guia_conta_w,
			nr_seq_prestador_w,
			dt_ref_atendimento_w,
			ie_carater_internacao_w,
			ie_tipo_atendimento_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
		exception
		when others then
			nr_seq_plano_guia_conta_w	:= null;
			nr_seq_prestador_w 		:= null;
			dt_ref_atendimento_w		:= trunc(sysdate,'dd');
		end;
	elsif	(ie_tipo_consistencia_w = 'E') then				
		begin
		select	a.nr_seq_plano,
			b.nr_seq_prestador,
			trunc(b.dt_execucao,'dd'),
			a.ie_carater_atendimento,
			pls_obter_internado_guia(b.nr_seq_requisicao,'R')
		into	nr_seq_plano_guia_conta_w,
			nr_seq_prestador_w,
			dt_ref_atendimento_w,
			ie_carater_internacao_w,
			ie_tipo_atendimento_w
		from	pls_requisicao		a,
			pls_execucao_requisicao b
		where	b.nr_seq_requisicao = a.nr_sequencia
		and	b.nr_sequencia = nr_seq_execucao_p;
		exception
		when others then
			nr_seq_plano_guia_conta_w	:= null;
			nr_seq_prestador_w 		:= null;
			dt_ref_atendimento_w		:= trunc(sysdate,'dd');
		end;
	end if;		
end if;

for r_c01_w in C01 loop
	begin
	/*Faz a condicao para verificar se encaixa na regra*/
	if	(dt_ref_atendimento_w >= trunc(r_c01_w.dt_inicio_vigencia,'dd') or r_c01_w.dt_inicio_vigencia is null) and
	 	(dt_ref_atendimento_w <= trunc(r_c01_w.dt_fim_vigencia,'dd') or r_c01_w.dt_fim_vigencia is null) and
		(r_c01_w.ie_carater_internacao	= ie_carater_internacao_w	or r_c01_w.ie_carater_internacao is null) and
		(r_c01_w.ie_tipo_atendimento	= ie_tipo_atendimento_w		or nvl(r_c01_w.ie_tipo_atendimento,'T') = 'T') then
		--Caso se encaixar, entao coloca a sua permissao
		ie_permite_w	:= r_c01_w.ie_permite;		
	end if;
	
	end;
end loop;

if	(ie_permite_w = 'S') then
	ie_lanca_glosa_w	:= 'N';
else
	ie_lanca_glosa_w	:= 'S';
end if;

if	(ie_lanca_glosa_w = 'S') then
	if	(ie_tipo_consistencia_w = 'G') then
		pls_gravar_motivo_glosa('2514',nr_seq_guia_p,null,null,wheb_mensagem_pck.get_texto(1108908, 'NR_SEQ_PLANO_GUIA_CONTA='||nr_seq_plano_guia_conta_w),
					nm_usuario_p,'P',ie_evento_p,nr_seq_prestador_w,'',null);
	elsif	(ie_tipo_consistencia_w = 'C') then
		pls_gravar_conta_glosa('2514',nr_seq_conta_p, null,null,'N',wheb_mensagem_pck.get_texto(1108908, 'NR_SEQ_PLANO_GUIA_CONTA='||nr_seq_plano_guia_conta_w),nm_usuario_p, 'A',ie_evento_p,
					nr_seq_prestador_w, cd_estabelecimento_p, '', null);
	elsif	(ie_tipo_consistencia_w = 'R') then
		pls_gravar_requisicao_glosa('2514', nr_seq_requisicao_p, null,
					null, wheb_mensagem_pck.get_texto(1108908, 'NR_SEQ_PLANO_GUIA_CONTA='||nr_seq_plano_guia_conta_w), nm_usuario_p, 
					nr_seq_prestador_w, cd_estabelecimento_p, null,
					null);
	elsif	(ie_tipo_consistencia_w = 'E') then
		pls_gravar_execucao_req_glosa('2514', nr_seq_execucao_p, nr_seq_exec_req_item_proc_p,
					nr_seq_exec_req_item_mat_p, wheb_mensagem_pck.get_texto(1108908, 'NR_SEQ_PLANO_GUIA_CONTA='||nr_seq_plano_guia_conta_w), nm_usuario_p, 
					nr_seq_prestador_w, cd_estabelecimento_p, null,
					null);
	end if;	
end if;	

end pls_consistir_prest_plano;
/
