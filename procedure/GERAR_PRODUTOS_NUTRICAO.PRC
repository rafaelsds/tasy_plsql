create or replace
procedure gerar_produtos_nutricao(
		nr_prescricao_p		number,
		nr_seq_dieta_p		number,
		ds_lista_produtos_p	varchar2,
		nm_usuario_p		varchar2) is

ds_lista_w		varchar2(4000);
ds_lista_aux_w		varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_pos_hifem_w		number(10,0);
nr_seq_produto_w	number(10,0);
qt_dose_w		number(18,6);
qt_vel_infusao_w	number(15,4);
qt_hora_aplicacao_w	number(3,0);
nr_ctrl_loop_w		number(3,0) := 0;
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_dieta_p is not null) and
	(ds_lista_produtos_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ds_lista_w	:= ds_lista_produtos_p;	
	
	while	(ds_lista_w is not null) and
		(nr_ctrl_loop_w < 100) loop
		begin
		nr_ctrl_loop_w 		:= nr_ctrl_loop_w + 1;		
		nr_pos_virgula_w	:= instr(ds_lista_w,',');
		
		qt_dose_w		:= 0;
		qt_hora_aplicacao_w	:= 0;
		qt_vel_infusao_w	:= 0;
		
		if	(nvl(nr_pos_virgula_w,0) > 0) then
			begin
			ds_lista_aux_w	:= substr(ds_lista_w,0,nr_pos_virgula_w-1);
			ds_lista_w	:= substr(ds_lista_w,nr_pos_virgula_w+1,length(ds_lista_w));
			end;
		else
			begin
			ds_lista_aux_w	:= ds_lista_w;
			ds_lista_w	:= null;
			end;
		end if;
		
		if	(ds_lista_aux_w is not null) then
			begin
			nr_pos_hifem_w	:= instr(ds_lista_aux_w,'-');
		
			if	(nvl(nr_pos_hifem_w,0) > 0) then
				begin
				nr_seq_produto_w	:= substr(ds_lista_aux_w,1,nr_pos_hifem_w-1);
				ds_lista_aux_w		:= substr(ds_lista_aux_w,nr_pos_hifem_w+1,length(ds_lista_aux_w));

				if	(ds_lista_aux_w is not null) then
					begin					
					nr_pos_hifem_w	:= instr(ds_lista_aux_w,'-');

					if	(nvl(nr_pos_hifem_w,0) > 0) then
						begin									
						qt_dose_w	:= to_number(replace(substr(ds_lista_aux_w,0,nr_pos_hifem_w-1),'.',','));
						ds_lista_aux_w		:= substr(ds_lista_aux_w,nr_pos_hifem_w+1,length(ds_lista_aux_w));

						if	(ds_lista_aux_w is not null) then
							begin
							nr_pos_hifem_w	:= instr(ds_lista_aux_w,'-');

							if	(nvl(nr_pos_hifem_w,0) > 0) and
								(substr(ds_lista_aux_w,0,nr_pos_hifem_w-1) not in ('S', 'N')) then
								begin								
								qt_hora_aplicacao_w	:= substr(ds_lista_aux_w,0,nr_pos_hifem_w-1);
								ds_lista_aux_w		:= substr(ds_lista_aux_w,nr_pos_hifem_w+1,length(ds_lista_aux_w));
								end;
								
								if	(ds_lista_aux_w is not null) then
									begin
									nr_pos_hifem_w	:= instr(ds_lista_aux_w,'-');

									if	(nvl(nr_pos_hifem_w,0) > 0) then
										begin
										qt_vel_infusao_w	:= substr(ds_lista_aux_w,0,nr_pos_hifem_w-1);
										ds_lista_aux_w		:= substr(ds_lista_aux_w,nr_pos_hifem_w+1,length(ds_lista_aux_w));
										end;
									end if;
									end;
								end if;
							end if;
							end;
						end if;
						end;
					end if;
					end;
				end if;
				end;
			else
				begin
				ds_lista_aux_w	:= null;
				end;
			end if;
			end;
		end if;
		
		
		if	(nr_seq_produto_w is not null) then
			begin
			
			if	(qt_dose_w = 0) then
				begin
				qt_dose_w := null;
				end;
			end if;
			
			if	(qt_hora_aplicacao_w = 0) then
				begin
				qt_hora_aplicacao_w := null;
				end;
			end if;
			
			if	(qt_vel_infusao_w = 0) then
				begin
				qt_vel_infusao_w := null;
				end;
			end if;
			
			gerar_produto_nutricao(
				nr_prescricao_p,
				nr_seq_dieta_p,
				null,
				nm_usuario_p,
				qt_dose_w,
				qt_vel_infusao_w,
				qt_hora_aplicacao_w,
				nr_seq_produto_w,
				NULL,
				NULL,
				null,
				null,
				null);
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;
end gerar_produtos_nutricao;
/