create or replace
procedure agenda_pac_cme_proc_befor_post(
		nr_seq_agenda_p		number,
		nr_seq_conjunto_p	number,
		nr_seq_proc_interno_p	number,
		ie_novo_p		varchar2,
		nm_usuario_p		varchar2,
		ds_erro_p		out varchar2) is
qt_registro_w number(10,0) := 0;
ie_consist_lanc_w varchar2(255);
ie_disp_cme_w varchar2(255);
begin
if	(nr_seq_agenda_p is not null) and
	(nr_seq_conjunto_p is not null) then
	begin
	obter_param_usuario(871, 477, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_consist_lanc_w);	
	if	(ie_consist_lanc_w = 'S') and
		(nr_seq_proc_interno_p is not null) then
		begin
		select	count(*) 
		into	qt_registro_w
		from	agenda_pac_cme
		where	ie_origem_inf = 'I'
		and	nr_seq_agenda = nr_seq_agenda_p
		and	nr_seq_conjunto = nr_seq_conjunto_p
		and	nr_seq_proc_interno = nr_seq_proc_interno_p;
	
		if	(qt_registro_w > 0) then
			begin
			wheb_mensagem_pck.Exibir_Mensagem_Abort(68872);
			end;
		end if;
		end;
	end if;
	obter_param_usuario(871, 101, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_disp_cme_w);
	if	(ie_disp_cme_w <> 'N') then
		begin
		cme_consistir_conj_agenda(
				nr_seq_agenda_p,
				nr_seq_conjunto_p,
				ie_novo_p,
				nm_usuario_p,
				wheb_usuario_pck.get_cd_estabelecimento,					
				ds_erro_p);			
		end;
	end if;	
	end;
end if;
end agenda_pac_cme_proc_befor_post;
/
