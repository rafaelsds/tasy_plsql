create or replace
procedure Gravar_Agfa_Siu_In(cd_pessoa_fisica_p	Varchar2,
			dt_inicio_agendamento_p	Date,
			dt_fim_agendamento_p	Date,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			nr_seq_agenda_cons_p	number,
			nr_seq_agenda_exame_p	number,
			cd_medico_p		Varchar2,
			nr_seq_proc_interno_p	number,
			dt_atualizacao_p		date,
			nr_min_duracao_p		number,
			ie_lado_p			varchar2,
			cd_agenda_p		number,
			event_type_p		varchar2,
			ie_autorizacao_p		varchar2,
			ds_observacao_p		varchar2,
			dt_agenda_p		date,
			cd_medico_solic_p	varchar2) is 

cd_agenda_w		number(10);
ds_autorizacao_w		Varchar2(30);
ds_setor_w		Varchar2(30);
ds_observacao_w		Varchar2(400);
ie_lado_w		Varchar2(1);
dt_fim_exame_w		date;
cd_agenda_integracao_w	varchar2(30);
seq_agfa_siu_in_w		number(10);
nr_seq_agenda_integrada_w	number(10);

qt_anexos_w		number(10,0);
qt_total_anexo_w	number(10,0);
siu_03_archive_w	number(3,0);
siu_01_scan_current_w	number(3,0);
nr_seq_doc_foto_w	number(10,0);
nr_seq_ageint_item_w	number(10,0);
ie_externo_w		agenda_integrada.ie_externo%type;


BEGIN

select 	max(cd_agenda_integracao)
into	cd_agenda_integracao_w
from 	agenda_integracao
where 	cd_agenda = cd_agenda_p
and	ie_tipo_integracao = 'AGFA';

if	(cd_agenda_integracao_w is not null) then
	select	seq_agfa_siu_in.nextval
	into	seq_agfa_siu_in_w
	from	dual;
		
	if      (nr_seq_agenda_cons_p is not null) then--consulta/servi�o
		
		select 	max(nr_seq_agenda_int)
		into	nr_seq_agenda_integrada_w
		from	agenda_integrada_item
		where	nr_seq_agenda_cons = nr_seq_agenda_cons_p;

		select 	NVL(SUBSTR(DECODE(MAX(ie_autorizacao_p),'A', '1^Authorized',
							'AP','1^Authorized',
							'N', '2^Unauthorized',
							'PA','4^Waiting',
							'B', '3^Cancelled'),1,30),NULL),
			SUBSTR(MAX(ds_observacao_p),1,400),
			DECODE(NVL(MAX(ie_lado_p),'X'),'E','E','D','D','X')
		into	ds_autorizacao_w,
			ds_observacao_w,
			ie_lado_w
		from 	dual;
		
		if 	(nr_min_duracao_p is not null) then
			dt_fim_exame_w := dt_agenda_p + (nr_min_duracao_p / 1440);
		else
			dt_fim_exame_w := dt_agenda_p;
		end if;
			
		insert into agfa_siu_in
			(AIL_03_EXAM_ROOM,
			AIP_03_DOCTOR,
			AIS_03_DEPARTAMENT,
			AIS_03_EXAM_CODE,
			MSH_07_DATE,
			MSH_10_ID,
			SCH_02_APP_ID,
			NTE_03_NOTE,
			PID_02_ID,
			SCH_02_ACC_NUMBER,
			SCH_11_04_START_DATE,
			SCH_11_05_END_DATE,
			SCH_20_USER_HIS,
			SCH_25_APP_STATUS,
			SIU_01_HIS_DATE,
			SIU_02_RIS_STATUS,
			SIU_03_RIS_DATE,
			MSH_09_EVENT_TYPE,
			SCH_26_APP_BATCH,
			SCH_12_REQ_PHYSICIAN)
		values
			(cd_agenda_integracao_w,
			cd_medico_p,
			cd_estabelecimento_p,
			nr_seq_proc_interno_p||ie_lado_w,
			dt_atualizacao_p,
			seq_agfa_siu_in_w,
			nr_seq_agenda_cons_p,
			ds_observacao_w,
			cd_pessoa_fisica_p,
			null,
			dt_agenda_p,
			dt_fim_exame_w,
			lower(nm_usuario_p),
			ds_autorizacao_w,
			sysdate,
			'N',
			null,
			event_type_p,
			nr_seq_agenda_integrada_w,
			cd_medico_solic_p);
	end if;

	if      (nr_seq_agenda_exame_p is not null) then--exame

		select 	max(nr_seq_agenda_int),
			max(nr_sequencia)
		into	nr_seq_agenda_integrada_w,
			nr_seq_ageint_item_w
		from	agenda_integrada_item
		where	nr_seq_agenda_exame = nr_seq_agenda_exame_p;
		
		select	nvl(max(ie_externo),'N')
		into	ie_externo_w
		from	agenda_integrada
		where	nr_sequencia = nr_seq_agenda_integrada_w;
		
		
		select	count(*)
		into	qt_anexos_w
		from	pessoa_documentacao a,
			tipo_documentacao b,
			pessoa_doc_foto c		
		where	a.nr_seq_documento  	= b.nr_sequencia
		and	a.nr_sequencia		= c.nr_seq_pessoa_doc
		and	nvl(b.ie_gravar_ageint,'N') = 'S'
		and	a.nr_seq_ageint 	= nr_seq_agenda_integrada_w;
		
		if	(qt_anexos_w > 0) then
			
			qt_total_anexo_w	:= qt_anexos_w;
			
			siu_03_archive_w	:= 1;
			
			select	nvl(max(SIU_01_SCAN_CURRENT),0) + 1
			into	siu_01_scan_current_w
			from	agfa_siu_in
			where	SCH_26_APP_BATCH = nr_seq_agenda_integrada_w;
			
			select  max(c.nr_sequencia)
			into	nr_seq_doc_foto_w
			from	pessoa_documentacao a,
				tipo_documentacao b,
				pessoa_doc_foto c		
			where	a.nr_seq_documento  	= b.nr_sequencia
			and	a.nr_sequencia		= c.nr_seq_pessoa_doc
			and	nvl(b.ie_gravar_ageint,'N') = 'S'
			and	a.nr_seq_ageint 	= nr_seq_agenda_integrada_w
			and	c.nr_seq_ageint_item is null;
		end if;
		
		
		select 	NVL(SUBSTR(DECODE(MAX(ie_autorizacao_p),'A', '1^Authorized',
							'AP','1^Authorized',
							'N', '2^Unauthorized',
							'PA','4^Waiting',
							'B', '3^Cancelled'),1,30),NULL),
			SUBSTR(MAX(ds_observacao_p),1,400),
			DECODE(NVL(MAX(ie_lado_p),'X'),'E','E','D','D','X')
		into	ds_autorizacao_w,
			ds_observacao_w,
			ie_lado_w
		from 	dual;
		
		if 	(nr_min_duracao_p is not null) then
			dt_fim_exame_w := dt_agenda_p + (nr_min_duracao_p / 1440);
		else
			dt_fim_exame_w := dt_agenda_p;
		end if;
		
		if	(nr_seq_doc_foto_w is null) then
			insert into agfa_siu_in
				(AIL_03_EXAM_ROOM,
				AIP_03_DOCTOR,
				AIS_03_DEPARTAMENT,
				AIS_03_EXAM_CODE,
				MSH_07_DATE,
				MSH_10_ID,
				SCH_02_APP_ID,
				NTE_03_NOTE,
				PID_02_ID,
				SCH_02_ACC_NUMBER,
				SCH_11_04_START_DATE,
				SCH_11_05_END_DATE,
				SCH_20_USER_HIS,
				SCH_25_APP_STATUS,
				SIU_01_HIS_DATE,
				SIU_02_RIS_STATUS,
				SIU_03_RIS_DATE,
				MSH_09_EVENT_TYPE,
				SCH_26_APP_BATCH,
				SCH_12_REQ_PHYSICIAN)
			values
				(cd_agenda_integracao_w,
				cd_medico_p,
				cd_estabelecimento_p,
				nr_seq_proc_interno_p||ie_lado_w,
				dt_atualizacao_p,
				seq_agfa_siu_in_w,
				nr_seq_agenda_exame_p,
				ds_observacao_w,
				cd_pessoa_fisica_p,
				null,
				dt_agenda_p,
				dt_fim_exame_w,
				lower(nm_usuario_p),
				ds_autorizacao_w,
				sysdate,
				'N',
				null,
				event_type_p,
				nr_seq_agenda_integrada_w,
				cd_medico_solic_p);
		else
						
			insert into agfa_siu_in
				(AIL_03_EXAM_ROOM,
				AIP_03_DOCTOR,
				AIS_03_DEPARTAMENT,
				AIS_03_EXAM_CODE,
				MSH_07_DATE,
				MSH_10_ID,
				SCH_02_APP_ID,
				NTE_03_NOTE,
				PID_02_ID,
				SCH_02_ACC_NUMBER,
				SCH_11_04_START_DATE,
				SCH_11_05_END_DATE,
				SCH_20_USER_HIS,
				SCH_25_APP_STATUS,
				SIU_01_HIS_DATE,
				SIU_02_RIS_STATUS,
				SIU_03_RIS_DATE,
				MSH_09_EVENT_TYPE,
				SCH_26_APP_BATCH,
				SIU_02_SCAN_TOTAL,
				SIU_01_SCAN_CURRENT,
				SIU_03_ARCHIVE,
				OBX_05_SCAN,
				SCH_12_REQ_PHYSICIAN)
			select
				cd_agenda_integracao_w,
				cd_medico_p,
				cd_estabelecimento_p,
				nr_seq_proc_interno_p||ie_lado_w,
				dt_atualizacao_p,
				seq_agfa_siu_in_w,
				nr_seq_agenda_exame_p,
				ds_observacao_w,
				cd_pessoa_fisica_p,
				null,
				dt_agenda_p,
				dt_fim_exame_w,
				lower(nm_usuario_p),
				ds_autorizacao_w,
				sysdate,
				'N',
				null,
				event_type_p,
				nr_seq_agenda_integrada_w,
				qt_total_anexo_w,
				siu_01_scan_current_w,
				siu_03_archive_w,
				to_lob(im_documento),
				cd_medico_solic_p
			from	pessoa_doc_foto
			where	nr_sequencia = nr_seq_doc_foto_w;
			
			if	(ie_externo_w = 'S') then
				update	agfa_siu_in
				set	OBX_05_SCAN = (select IM_DOCUMENTO_BLOB from pessoa_doc_foto where nr_sequencia = nr_seq_doc_foto_w)
				where	MSH_10_ID = seq_agfa_siu_in_w;			
			end if;
				
			update 	pessoa_doc_foto
			set	nr_seq_ageint_item = nr_seq_ageint_item_w
			where	nr_sequencia = nr_seq_doc_foto_w;
		end if;
	end if;
end if;	
	
END Gravar_Agfa_Siu_In;
/