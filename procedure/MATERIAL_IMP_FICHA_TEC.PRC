create or replace procedure material_imp_ficha_tec(	cd_dcb_p		varchar2,
				ds_ficha_tecnica_p	varchar2,
				nr_seq_superior_p	number,
				nr_seq_retorno_p	out number,
				nr_externo_p		varchar2,
				cd_atc_p		varchar2,
				nm_usuario_p		varchar2) is 
nr_seq_dcb_w		number(10);
nr_seq_ficha_w		number(10);
medic_ficha_tecnica_w	medic_ficha_tecnica%rowtype;
ds_atc_w		varchar2(255);
ds_ficha_w		varchar2(255)	:= ds_ficha_tecnica_p;
nr_seq_ficha_igual_w	number(10);
nr_seq_ficha_count_w                  number(10);

begin

if	(cd_dcb_p	is not null) then
	material_imp_dcb_medic(	cd_dcb_p,
				ds_ficha_tecnica_p,
				nm_usuario_p,
				nr_seq_dcb_w); 
end if;

if	(nr_seq_dcb_w	is not null) and
	(nr_seq_superior_p is null) then
	
	select	max(nr_sequencia)
	into	nr_seq_ficha_w
	from	medic_ficha_tecnica
	where	nr_seq_dcb	= nr_seq_dcb_w
	and	nr_seq_superior is null;
		
end if;

if	(nr_seq_ficha_w	is null) then
	
	select	medic_ficha_tecnica_seq.nextval
	into	nr_seq_ficha_w
	from	dual;
	
	medic_ficha_tecnica_w.nr_sequencia		:= nr_seq_ficha_w;
	medic_ficha_tecnica_w.DS_SUBSTANCIA		:= substr(ds_ficha_w,1,80);
	medic_ficha_tecnica_w.NR_SEQ_DCB		:= nr_seq_dcb_w;
	medic_ficha_tecnica_w.dt_atualizacao		:= sysdate;
	medic_ficha_tecnica_w.dt_atualizacao_nrec	:= sysdate;
	medic_ficha_tecnica_w.nm_usuario		:= nm_usuario_p;
	medic_ficha_tecnica_w.nm_usuario_nrec		:= nm_usuario_p;
	medic_ficha_tecnica_w.nr_seq_superior		:= nr_seq_superior_p;
	medic_ficha_tecnica_w.ie_antimicrobiano		:= 'N';
	medic_ficha_tecnica_w.nr_externo		:= nvl(cd_atc_p,nr_externo_p);
	if	(cd_atc_p is not null) then
		medic_ficha_tecnica_w.ds_observacao		:= cd_atc_p||',';
	end if;
	
	insert into medic_ficha_tecnica values medic_ficha_tecnica_w;
	
     if( nr_seq_superior_p is not  null  AND nr_seq_dcb_w is not null  )then 
         SELECT count(*) into nr_seq_ficha_count_w FROM medic_ficha_tecnica 
          where NR_SEQ_DCB=nr_seq_dcb_w AND nr_seq_superior is  null  ; 
      if (nr_seq_ficha_count_w = 0 ) then
          select   medic_ficha_tecnica_seq.nextval
          into nr_seq_ficha_w
          from   dual;
           medic_ficha_tecnica_w.nr_sequencia                        := nr_seq_ficha_w;
           medic_ficha_tecnica_w.nr_seq_superior                     := null;
           insert into medic_ficha_tecnica values medic_ficha_tecnica_w;
     end if;
    end if;
	
end if; 

nr_seq_retorno_p	:= nr_seq_ficha_w;

end material_imp_ficha_tec;
/ 
