create or replace
procedure pls_gerar_alerta_rembolso_lib(ie_tipo_evento_p		number,
					nr_seq_protocolo_p		number,
					ie_status_protocolo_p		pls_protocolo_conta.ie_status%type,
					nm_usuario_p			varchar2) is
					
vl_total_w			pls_protocolo_conta.vl_total%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
ds_destinatario_w		varchar2(255);
ds_email_dest_w			usuario.ds_email%type;
ds_email_remetente_w		pls_alerta_evento_mensagem.ds_remetente_email%type;
ds_mensagem_regra_w		pls_alerta_evento_mensagem.ds_mensagem%type;
ds_remetente_w			pls_alerta_evento_mensagem.ds_remetente_sms%type;
ds_titulo_w			pls_alerta_evento_mensagem.ds_titulo%type;
id_sms_w			pls_alerta_evento_cont_des.id_sms%type;
ie_tipo_envio_w			pls_alerta_evento_mensagem.ie_tipo_envio%type;
qt_macro_regra_w		pls_integer	:= 0;
nr_seq_evento_controle_w	pls_alerta_evento_controle.nr_sequencia%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_tipo_evento_w		pls_tipo_alerta_evento.nr_sequencia%type;
nr_titulo_w			titulo_pagar.nr_titulo%type;
vl_saldo_titulo_w		titulo_pagar.vl_saldo_titulo%type;
dt_vencimento_atual_w		titulo_pagar.dt_vencimento_atual%type;
dt_emissao_w			titulo_pagar.dt_emissao%type;
nm_benef_w			varchar2(255);
cd_cart_benef_w			pls_segurado_carteira.cd_usuario_plano%type;
nm_responsavel_w		varchar2(255);
nm_banco_w			varchar2(255);
cd_agencia_bancaria_w		pls_protocolo_conta.cd_agencia_bancaria%type;
ie_agencia_w			pls_protocolo_conta.ie_digito_agencia%type;
cd_conta_w			pls_protocolo_conta.cd_conta%type;
ie_digito_conta_w		pls_protocolo_conta.ie_digito_conta%type;
vl_apres_prot_w			number(15, 2);
vl_liberado_prot_w		number(15, 2);
vl_glosa_prot_w			number(15, 2);
ds_forma_pgto_w			varchar2(100);
ds_justificativa_w		pls_protocolo_conta.ds_observacao%type;
ds_motivo_canc_w		pls_protocolo_conta.ds_motivo_canc%type;
ds_motivo_cancelamento_w	pls_motivo_cancel_conta.ds_motivo_cancelamento%type;
ie_utilizar_ddi_w			varchar2(1);

Cursor C01 is
	select	nr_seq_evento_mens
	from	pls_regra_geracao_evento
	where	ie_situacao 		= 'A'
	and	ie_evento_disparo 	= ie_tipo_evento_p
	and	((ie_status_reemb = ie_status_protocolo_p) or (ie_status_reemb is null));
	
Cursor C02 (	nr_seq_evento_mens_pc	pls_regra_geracao_evento.nr_seq_evento_mens%type) is
	select	nr_sequencia,
		ie_forma_envio
	from	pls_alerta_evento_destino
	where	ie_situacao 		= 'A'
	and	nr_seq_evento_mens	= nr_seq_evento_mens_pc;
	
begin
--pega os dados do protocolo

select	nr_seq_segurado,
	vl_total,
	substr(pls_obter_dados_segurado(nr_seq_segurado, 'N'), 1, 255),
	pls_obter_dados_segurado(nr_seq_segurado, 'CR'),
	substr(obter_nome_pf(cd_pessoa_fisica), 1, 255),
	substr(obter_nome_banco(cd_banco), 1, 255),
	cd_agencia_bancaria,
	ie_digito_agencia,
	cd_conta,
	ie_digito_conta,
	pls_obter_valor_protocolo(nr_sequencia, 'C'),
	pls_obter_valor_protocolo(nr_sequencia, 'T'),
	pls_obter_valor_protocolo(nr_sequencia, 'G'),
	substr(obter_descricao_dominio(3007, ie_forma_pagamento), 1, 100),
	ds_observacao,
	ds_motivo_canc,
	substr(( 	select max(ds_motivo_cancelamento)
		from 	pls_motivo_cancel_conta
		where	nr_sequencia = nr_seq_motivo_cancel),1,255) ds_motivo_cancelamento
into	nr_seq_segurado_w,
	vl_total_w,
	nm_benef_w,
	cd_cart_benef_w,
	nm_responsavel_w,
	nm_banco_w,
	cd_agencia_bancaria_w,
	ie_agencia_w,
	cd_conta_w,
	ie_digito_conta_w,
	vl_apres_prot_w,
	vl_liberado_prot_w,
	vl_glosa_prot_w,
	ds_forma_pgto_w,
	ds_justificativa_w,
	ds_motivo_canc_w,
	ds_motivo_cancelamento_w
from	pls_protocolo_conta
where	nr_sequencia = nr_seq_protocolo_p;

--pega o n�mero do t�tulo
select	max(nr_titulo),
	max(vl_saldo_titulo),
	max(dt_vencimento_atual),
	max(dt_emissao)
into	nr_titulo_w,
	vl_saldo_titulo_w,
	dt_vencimento_atual_w,
	dt_emissao_w
from	titulo_pagar
where	nr_seq_reembolso = nr_seq_protocolo_p;

for r_c01_w in C01 loop

	select	ds_mensagem,
		ie_tipo_envio,
		ds_titulo,
		nr_seq_tipo_evento,
		ds_remetente_email,
		ds_remetente_sms
	into	ds_mensagem_regra_w,
		ie_tipo_envio_w,
		ds_titulo_w,
		nr_seq_tipo_evento_w,
		ds_email_remetente_w,
		ds_remetente_w
	from	pls_alerta_evento_mensagem
	where 	nr_sequencia = r_c01_w.nr_seq_evento_mens;
	
	begin
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_w;
	exception
	when others then
		cd_pessoa_fisica_w := null;
	end;
	
	for r_c02_w in C02( r_c01_w.nr_seq_evento_mens ) loop	
	
		if	(nr_seq_evento_controle_w is null) then
			select	pls_alerta_evento_controle_seq.nextval
			into	nr_seq_evento_controle_w
			from	dual;
			
			insert into pls_alerta_evento_controle(
					nr_sequencia, dt_geracao_evento, ie_evento_disparo,
					nr_seq_tipo_evento, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, ds_mensagem,
					ds_titulo, ie_tipo_envio)
				values(	nr_seq_evento_controle_w, sysdate, ie_tipo_evento_p,
					nr_seq_tipo_evento_w, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, ds_mensagem_regra_w,
					ds_titulo_w,ie_tipo_envio_w);
		end if;
		
		select	count(1)
		into	qt_macro_regra_w
		from	dual
		where	ds_mensagem_regra_w like '%@%';
		
		if	( qt_macro_regra_w > 0) then
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@VL_REEMBOLSO', campo_mascara_virgula(vl_total_w)), 1, 4000);
			--macro para o n�mero do t�tulo
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NR_TITULO', nr_titulo_w), 1, 4000);
			--macro para o valor do t�tulo
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@VL_TITULO', campo_mascara_virgula(vl_saldo_titulo_w)), 1, 4000);
			--macro para a data de vencimento do titulo
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DT_VENC_TIT', dt_vencimento_atual_w), 1, 4000);
			--macro para a data de emissao do titulo
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DT_EMISSAO_TIT', dt_emissao_w), 1, 4000);
			--macro para o nome do beneficiario
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NM_BENEF', nm_benef_w), 1, 4000);
			--macro para a carteirinha do beneficiario
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@CD_CART_BENEF', cd_cart_benef_w), 1, 4000);
			--macro para o nome do respons�vel pelo credito
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NM_RESPONSAVEL', nm_responsavel_w), 1, 4000);
			--macro para o nome do banco
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NM_BANCO', nm_banco_w), 1, 4000);
			--macro para cd agencia bancaria
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NR_AGENCIA', cd_agencia_bancaria_w), 1, 4000);
			--macro para ie agencia bancaria
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@IE_AGENCIA', ie_agencia_w), 1, 4000);
			--macro para cd conta
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NR_CONTA', cd_conta_w), 1, 4000);
			--macro para digito conta
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@IE_CONTA', ie_digito_conta_w), 1, 4000);
			--macro para forma de pagamento
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@DS_FORMA_PGTO', ds_forma_pgto_w), 1, 4000);
			--macro para o n�mero do protocolo
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@NR_PROTOCOLO', nr_seq_protocolo_p), 1, 4000);
			--macro para o total apresentado
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@VL_APRES_PROT', campo_mascara_virgula(vl_apres_prot_w)), 1, 4000);
			--macro para o total liberado
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@VL_LIBERADO_PROT', campo_mascara_virgula(vl_liberado_prot_w)), 1, 4000);
			--macro para o total glosado
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@VL_GLOSA_PROT', campo_mascara_virgula(vl_glosa_prot_w)), 1, 4000);
			--macro para ds_observacao
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@JUSTIFICATIVA', ds_justificativa_w), 1, 4000);
			
			--macro para o motivo cancelamento(protocolo) -- essa aqui acaba por pegar o vlor do campo justificativa, no wdlg aberto para cancelamento do protocolo, caso essa informa��o estiver nula, manda 
			-- a descri��o a partir do motivo cancelamento
			ds_mensagem_regra_w := substr(replace_macro(ds_mensagem_regra_w, '@MOTIVO_CANC', nvl(ds_motivo_canc_w, ds_motivo_cancelamento_w)), 1, 4000);
															
		end if;
		
		if	(r_c02_w.ie_forma_envio = 'EM') and (nvl(ds_email_remetente_w,'X') <> 'X') then
			ds_email_dest_w := pls_obter_dados_segurado(nr_seq_segurado_w,'E');
			
			if	(nvl(ds_email_dest_w,'X') <> 'X') then
				enviar_email(ds_titulo_w, ds_mensagem_regra_w, ds_email_remetente_w, ds_email_dest_w, nm_usuario_p, 'M');
			end if;
		end if;
		
		if	(r_c02_w.ie_forma_envio = 'SMS') then	
		
			obter_param_usuario(0, 214, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_utilizar_ddi_w);		
		
			if	(cd_pessoa_fisica_w is not null) then
				begin
				
				select  decode(ie_utilizar_ddi_w, 'N', nr_ddi_celular ||nr_ddd_celular|| nr_telefone_celular, nr_ddd_celular || nr_telefone_celular)
				into	ds_destinatario_w
				from	pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_fisica_w;
				exception
				when others then
					ds_destinatario_w := null;
				end;
			end if;
			
			ds_remetente_w		:= substr(ds_remetente_w,0,40);
			ds_destinatario_w	:= substr(ds_destinatario_w,0,40);
			
			if	(ds_remetente_w is not null) and
				(ds_destinatario_w is not null) and
				(ds_mensagem_regra_w is not null) then
				
				ds_destinatario_w	:= replace(ds_destinatario_w,'(','');
				ds_destinatario_w	:= replace(ds_destinatario_w,')','');
				ds_destinatario_w	:= replace(ds_destinatario_w,'-','');
				
				pls_enviar_sms_alerta_evento(	ds_remetente_w, ds_destinatario_w, substr(ds_mensagem_regra_w,1,150),
								nr_seq_tipo_evento_w, nm_usuario_p, id_sms_w);
			end if;
		end if;
		
		
		pls_gerar_destino_evento(nr_seq_evento_controle_w, 'E', r_c02_w.ie_forma_envio,
					nm_usuario_p, cd_pessoa_fisica_w, '',
					ds_mensagem_regra_w, id_sms_w, ds_destinatario_w);
	end loop;
end loop;

end pls_gerar_alerta_rembolso_lib;
/
