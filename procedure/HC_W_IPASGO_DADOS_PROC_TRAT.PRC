create or replace
procedure hc_w_ipasgo_dados_proc_trat(
			nr_interno_conta_p		number,
			dt_mesano_referencia_p	date,
			nm_usuario_p		Varchar2,
			nr_seq_tipo_fatura_p	number,
			qt_linha_arq_p in out	number,
			qt_linha_atend_p in out	number) is
			
-- Utilizada pelo Hospital Cora��o de Goi�s


nr_atendimento_w			atendimento_paciente.nr_atendimento%type;
cd_procedimento_w		varchar2(15);
cd_procedimento_ww		varchar2(15);
ie_origem_proced_w		procedimento_paciente.ie_origem_proced%type;
qt_procedimento_w			procedimento_paciente.qt_procedimento%type;
ie_via_acesso_w			procedimento_paciente.ie_via_acesso%type;
ds_via_acesso_w			varchar2(01);
qt_ato_w				number(10,0) := 0;
qt_ato_ww			number(10,0) := 0;
dt_procedimento_w			procedimento_paciente.dt_procedimento%type;
dt_procedimento_ww		procedimento_paciente.dt_procedimento%type;
nr_seq_proc_w			number(10,0) := -1;
cd_convenio_parametro_w		conta_paciente.cd_convenio_parametro%type;
nr_seq_proc_interno_w		procedimento_paciente.nr_seq_proc_interno%type;
cd_cnpj_w			convenio.cd_cgc%type;
ie_funcao_medico_w		procedimento_paciente.ie_funcao_medico%type;
vl_medico_w			number(15,4);
ie_w				number(01,0);
nr_seq_proc_hemot_w		number(10,0);
cd_tipo_fatura_w			fatur_tipo_fatura.cd_tipo_fatura%type;
ie_considera_proc_interno_w		varchar2(01);
cd_tipo_procedimento_w		number(3);
cd_tipo_procedimento_ww		number(3);
cd_estabelecimento_w		conta_paciente.cd_estabelecimento%type;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
ie_proc_princ_atend_w		procedimento_paciente.ie_proc_princ_atend%type;
nr_ato_ipasgo_w			procedimento_paciente.nr_ato_ipasgo%type;
vl_ch_medico_w			procedimento_paciente.vl_medico%type;
qt_ato_informado_w		number(10) := 0;
qt_procedimento_ww		procedimento_paciente.qt_procedimento%type;

cursor c01 is
select	1,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	a.cd_tipo_procedimento not in (1,2,3,4,5,12,13,14,15,16,20,25,26,30,31,34,39,74,75,80,91,92,94,96,97,115)
and	somente_numero(b.cd_procedimento_convenio) not in (70025,70033)	--Estes s�o alto custo devem sair no union 5
and	nvl(b.qt_procedimento,0) <> 0
and	cd_tipo_fatura_w not in (3,4,17)
--and	qt_ato_informado_w = 0
union all
/*Anderson em 10/03/2011 - Gerar uma unica linha de Hemoterapia (80) com 20010 quando a fatura for 17 (conferencia)*/
select	2,
	'00020010' cd_procedimento,
	0,
	1,
	'',
	b.dt_procedimento,
	0,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	a.cd_tipo_procedimento in (80)
and	rownum < 2
and	cd_tipo_fatura_w = 17
--and	qt_ato_informado_w = 0
union all
select	3,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	a.cd_tipo_procedimento in (80)
and	nvl(b.ie_proc_princ_atend,'N') = 'S'
and	rownum < 2
and	cd_tipo_fatura_w <> 17
--and	qt_ato_informado_w = 0
union all
/*Anderson 15/03/2011 - Gerar taxa de box hora ao inv�s de proc ou honorario quando tipo de fatura for 8 (box hora)*/
select	4,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 2
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.ie_proc_princ_atend,'S') = 'S'
and	cd_tipo_fatura_w = 8
--and	qt_ato_informado_w = 0
union all
/*Anderson 31/03/2011 - Gerar o proc princ independente se for taxa ou procedimento quando for 3 (Alto custo)*/
select	5,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	cd_tipo_fatura_w 	= 3
and	nvl(b.ie_proc_princ_atend,'S') = 'S'
and	somente_numero(b.cd_procedimento_convenio) in (70025,70033)
--and	qt_ato_informado_w = 0
union all
/*Geliard 24/10/2012 para Hemolabor - Agrupar os procedimentos e ordenar pelo c�digo*/
select	6,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	sum(b.qt_procedimento) qt_procedimento,
	b.ie_via_acesso,
	min(b.dt_procedimento) dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl(sum(b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	decode(nvl(b.cd_procedimento_convenio,b.cd_procedimento),20010,'0',nvl(b.ie_proc_princ_atend,'N')) ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	b.ie_funcao_medico <> '12'
and	a.cd_tipo_procedimento not in (1,2,3,4,5,8,12,13,14,15,16,20,25,26,30,31,33,34,39,74,75,80,91,92,94,96,97,103)
and	somente_numero(b.cd_procedimento_convenio) not in (70025,70033)	--Estes s�o alto custo devem sair no union 5
and	obter_se_gera_ato_ipasgo(b.cd_procedimento,b.ie_origem_proced,cd_estabelecimento_w,cd_tipo_fatura_w,ie_tipo_atendimento_w,b.cd_setor_atendimento) = 'N'
and	nvl(b.qt_procedimento,0) <> 0
and	cd_tipo_fatura_w = 4
--and	qt_ato_informado_w = 0
group by	6,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento),
	b.ie_origem_proced,
	b.ie_via_acesso,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	a.cd_tipo_procedimento,
	decode(nvl(b.cd_procedimento_convenio,b.cd_procedimento),20010,'0',nvl(b.ie_proc_princ_atend,'N')),
	nvl(b.nr_ato_ipasgo,0),
	nvl(b.vl_medico,0)
union all
select	7,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	obter_se_gera_ato_ipasgo(b.cd_procedimento,b.ie_origem_proced,cd_estabelecimento_w,cd_tipo_fatura_w,ie_tipo_atendimento_w,b.cd_setor_atendimento) = 'S'
and	nvl(b.qt_procedimento,0) <> 0
--and	qt_ato_informado_w = 0
union all
/*
select	8,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	obter_se_gera_ato_ipasgo(b.cd_procedimento,b.ie_origem_proced,cd_estabelecimento_w,cd_tipo_fatura_w,ie_tipo_atendimento_w,b.cd_setor_atendimento) = 'S'
and	nvl(b.qt_procedimento,0) <> 0
and	qt_ato_informado_w > 0
and	nvl(b.nr_ato_ipasgo,0) > 0
union all*/
select	9,
	nvl(b.cd_procedimento_convenio,b.cd_procedimento) cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.ie_via_acesso,
	b.dt_procedimento,
	b.nr_seq_proc_interno,
	b.ie_funcao_medico,
	nvl((b.qt_procedimento * Obter_preco_proc_ipasgo_atend(b.nr_atendimento,b.dt_conta,b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno,b.ie_responsavel_credito,'M')),0) vl_medico,
	a.cd_tipo_procedimento,
	nvl(b.ie_proc_princ_atend,'N') ie_proc_princ,
	nvl(b.nr_ato_ipasgo,0) nr_ato_ipasgo,
	nvl(b.vl_medico,0) vl_ch_medico
from	procedimento a,
	procedimento_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	b.cd_motivo_exc_conta is null
and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
and	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced
and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
and	a.cd_tipo_procedimento not in (1,2,3,4,5,12,13,14,15,16,20,25,26,30,31,34,39,74,75,80,91,92,94,96,97,115)
and	somente_numero(b.cd_procedimento_convenio) not in (70025,70033)	--Estes s�o alto custo devem sair no union 5
and	nvl(b.qt_procedimento,0) <> 0
and	cd_tipo_fatura_w <> 4
--and	qt_ato_informado_w = 0
order by	nr_ato_ipasgo,
	dt_procedimento,
	ie_proc_princ desc,	/*Foi ordenado desta maneira para que o procedimento principal fosse gerado no arquivo*/
	vl_ch_medico desc,
	cd_procedimento,	/*como via �nica de acesso e os seguintes como mesma via do principal. OS597773 afreinert 27/05/2013*/
	vl_medico desc;

begin

obter_param_usuario(999, 59, obter_perfil_Ativo, nm_usuario_p, Wheb_Usuario_pck.get_cd_estabelecimento, ie_considera_proc_interno_w);

begin
select	a.nr_atendimento,
	a.cd_convenio_parametro,
	a.cd_estabelecimento,
	b.ie_tipo_atendimento
into	nr_atendimento_w,
	cd_convenio_parametro_w,
	cd_estabelecimento_w,
	ie_tipo_atendimento_w
from	conta_paciente a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_interno_conta = nr_interno_conta_p;
exception
when others then
	nr_atendimento_w		:= 0;
	cd_convenio_parametro_w		:= 0;
	cd_estabelecimento_w		:= 0;
	ie_tipo_atendimento_w		:= 0;
end;

begin
select	cd_cgc
into	cd_cnpj_w
from	convenio
where	cd_convenio = cd_convenio_parametro_w;
exception
when others then
	cd_cnpj_w := '0';
end;

begin
select	cd_tipo_fatura
into	cd_tipo_fatura_w
from	fatur_tipo_fatura
where	nr_sequencia = nr_seq_tipo_fatura_p;
exception
when others then
	cd_tipo_fatura_w := 0;
end;

begin
select	count(*)
into	qt_ato_informado_w
from	procedimento_paciente
where	nr_atendimento = nr_atendimento_w
and	nr_interno_conta = nr_interno_conta_p
and	nvl(nr_ato_ipasgo,0) > 0;
exception
when others then
	qt_ato_informado_w := 0;
end;

open C01;
loop
fetch C01 into	
	ie_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	ie_via_acesso_w,
	dt_procedimento_w,
	nr_seq_proc_interno_w,
	ie_funcao_medico_w,
	vl_medico_w,
	cd_tipo_procedimento_w,
	ie_proc_princ_atend_w,
	nr_ato_ipasgo_w,
	vl_ch_medico_w;
exit when C01%notfound;
	begin	
	
	if	(nvl(ie_considera_proc_interno_w,'S') = 'N') then
		nr_seq_proc_interno_w := 0;
	end if;
	
	if	(ie_w = 2) then
		select	max(b.nr_sequencia)
		into	nr_seq_proc_hemot_w
		from	procedimento a,
			procedimento_paciente b
		where	b.nr_atendimento 	= nr_atendimento_w
		and	b.nr_interno_conta	= nr_interno_conta_p
		and	b.cd_motivo_exc_conta is null
		and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
		and	a.cd_procedimento	= b.cd_procedimento
		and	a.ie_origem_proced	= b.ie_origem_proced
		and	somente_numero(nvl(b.cd_procedimento_convenio,b.cd_procedimento)) = 20010
		and	a.cd_tipo_procedimento in (80);

		if	(nvl(nr_seq_proc_hemot_w,0) > 0) then
			select	dt_procedimento,
				ie_funcao_medico,
				vl_medico
			into	dt_procedimento_w,
				ie_funcao_medico_w,
				vl_medico_w
			from	procedimento_paciente
			where	nr_sequencia = nr_seq_proc_hemot_w;
		else
			select	max(b.nr_sequencia)
			into	nr_seq_proc_hemot_w
			from	procedimento_paciente b
			where	b.nr_atendimento 	= nr_atendimento_w
			and	b.nr_interno_conta	= nr_interno_conta_p
			and	b.cd_motivo_exc_conta is null
			and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
			and	nvl(b.ie_proc_princ_atend,'S') = 'S';
			
			if	(nvl(nr_seq_proc_hemot_w,0) > 0) then
				select	dt_procedimento,
					ie_funcao_medico,
					vl_medico
				into	dt_procedimento_w,
					ie_funcao_medico_w,
					vl_medico_w
				from	procedimento_paciente
				where	nr_sequencia = nr_seq_proc_hemot_w;
			end if;
		end if;
	end if;
	
	begin
	select	nvl(max(cd_externo), ie_funcao_medico_w)
	into	ie_funcao_medico_w
	from	conversao_meio_externo
	where	cd_cgc = cd_cnpj_w
	and	upper(nm_tabela) 	= 'W_IPASGO_DADOS_PROF_TRAT'
	and	upper(nm_atributo)	= 'CD_FUNCAO'
	and	cd_interno	= ie_funcao_medico_w;
	exception
	when others then
		ie_funcao_medico_w := 0;
	end;

	if	(cd_tipo_fatura_w = 3) then	/*Altera��o para o Hemolabor 416225 - gerar mesmo ato para gerar mesma via de acesso, ent�o tem que ser a mesma data*/
		dt_procedimento_w := dt_mesano_referencia_p;
	end if;
		
	if	(nvl(ie_funcao_medico_w,'0') <> '8') then
		begin
		nr_seq_proc_w 	:= nr_seq_proc_w + 1;
		
		if	(nvl(nr_ato_ipasgo_w,0) = 0) then
			begin
			--if	(cd_tipo_fatura_w <> 4) and
			if	((dt_procedimento_w <> dt_procedimento_ww) or
				(dt_procedimento_ww is null)) then
				qt_ato_w	:= qt_ato_w + 1;
				nr_seq_proc_w	:= 0;
				
			else
				begin			
				if	((cd_tipo_procedimento_ww <> cd_tipo_procedimento_w) or
					(cd_tipo_procedimento_ww is null)) then
					qt_ato_w	:= qt_ato_w + 1;
					nr_seq_proc_w	:= 0;
					
				end if;			
				end;
			end if;			
			end;
		end if;
		
		if	(qt_ato_ww <> qt_ato_w) or	/*Alterado de and para OR, para que quando altere de ato, a primeira linha sempre venha vazio - Encore OS370027*/
			(qt_ato_ww = 0) then
			ie_via_acesso_w := '';
			
			if	(nvl(nr_ato_ipasgo_w,0) <> 0) then
				nr_seq_proc_w	:= 0;
				
			end if;
		end if;		
		
		/*Buscar as convers�es meio externo - procedimento ou proc interno*/
		begin
		select	nvl(max(cd_proc_convenio), cd_procedimento_w)
		into	cd_procedimento_w
		from	conversao_proc_convenio
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced		= ie_origem_proced_w
		and	cd_convenio		= cd_convenio_parametro_w
		and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
		and	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w;

		if	(nvl(nr_seq_proc_interno_w,0) > 0) then
			/*Buscar as convers�es meio externo - proc interno*/
			select	nvl(max(cd_proc_convenio), nr_seq_proc_interno_w)
			into	cd_procedimento_w
			from	conversao_proc_convenio
			where	nr_seq_proc_interno	= nr_seq_proc_interno_w
			and	cd_convenio		= cd_convenio_parametro_w
			and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
			and	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w;
		end if;
		exception
		when others then
			cd_procedimento_w := nvl(cd_procedimento_w,0);
		end;
		
		qt_linha_arq_p := qt_linha_arq_p + 1;
		
		if	(nr_ato_ipasgo_w > 0) then
			qt_ato_w := nr_ato_ipasgo_w;
			
			if	(qt_ato_ww <> qt_ato_w) or	/*Alterado de and para OR, para que quando altere de ato, a primeira linha sempre venha vazio - Encore OS370027*/
				(qt_ato_ww = 0) then
				ie_via_acesso_w := '';
				nr_seq_proc_w	:= 0;
				
			end if;
		end if;
		
		select	decode(ie_via_acesso_w,'U','M','B','M',ie_via_acesso_w)
		into	ds_via_acesso_w
		from	dual;
		
		if	(cd_procedimento_w = 20010) then
		
			begin
			select	sum(b.qt_procedimento)
			into	qt_procedimento_ww
			from	procedimento a,
				procedimento_paciente b
			where	b.nr_atendimento 	= nr_atendimento_w
			and	b.nr_interno_conta	= nr_interno_conta_p
			and	b.cd_motivo_exc_conta is null
			and	obter_classif_material_proced(null, b.cd_procedimento, b.ie_origem_proced) = 1
			and	a.cd_procedimento	= b.cd_procedimento
			and	a.ie_origem_proced	= b.ie_origem_proced
			and	nvl(b.nr_seq_proc_pacote,0) <> b.nr_sequencia
			and	b.cd_procedimento	= 20010;
			exception
			when others then
				qt_procedimento_w 	:= qt_procedimento_w;
			end;
			
			if	(qt_procedimento_ww > 1) then
				qt_procedimento_w 	:= qt_procedimento_ww;
			end if;		
		end if;
		
		insert into w_ipasgo_dados_proc_trat(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_linha,
			tp_registro,
			nr_linha_atend,
			nr_linha_ato,
			nr_linha_proc,
			cd_procedimento,
			ie_origem_proced,
			qt_procedimento,
			ie_via_acesso,
			dt_mesano_referencia,
			nr_interno_conta,
			ds_linha,
			nr_seq_tipo_fatura,
			dt_procedimento)
		values(	w_ipasgo_dados_proc_trat_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			qt_linha_arq_p,
			6,
			qt_linha_atend_p,
			qt_ato_w,
			nr_seq_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_procedimento_w,
			ie_via_acesso_w,
			dt_mesano_referencia_p,
			nr_interno_conta_p,
			qt_linha_arq_p || '|' ||
			'6'|| '|' ||
			qt_linha_atend_p|| '|' ||
			qt_ato_w|| '|' ||
			nr_seq_proc_w|| '|' ||
			cd_procedimento_w || '|' ||
			qt_procedimento_w || '|' ||
			ds_via_acesso_w || '|' ||
			'|||||',
			nr_seq_tipo_fatura_p,
			dt_procedimento_w);
		end;
	end if;
	dt_procedimento_ww 	:= dt_procedimento_w;
	cd_procedimento_ww	:= cd_procedimento_w;
	cd_tipo_procedimento_ww	:= cd_tipo_procedimento_w;
	qt_ato_ww		:= qt_ato_w;
	end;
end loop;
close C01;

commit;

end hc_w_ipasgo_dados_proc_trat;
/