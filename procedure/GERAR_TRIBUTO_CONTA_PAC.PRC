create or replace
procedure gerar_tributo_conta_pac(
			nr_interno_conta_p	number,
			nr_seq_proc_mat_p	number,
			ie_proc_mat_p	varchar2,
			nm_usuario_p	varchar2) is 

qt_registros_w		number(5);
qt_registros_pacote_w	number(5);

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

cd_convenio_w		convenio.cd_convenio%type;

cd_procedimento_w	estrutura_procedimento_v.cd_procedimento%type;
ie_origem_proced_w	estrutura_procedimento_v.ie_origem_proced%type;
cd_area_procedimento_w	estrutura_procedimento_v.cd_area_procedimento%type;
cd_especialidade_w	estrutura_procedimento_v.cd_especialidade%type;
cd_grupo_proc_w		estrutura_procedimento_v.cd_grupo_proc%type;

cd_grupo_material_w	estrutura_material_v.cd_grupo_material%type;
cd_subgrupo_material_w	estrutura_material_v.cd_subgrupo_material%type;
cd_classe_material_w	estrutura_material_v.cd_classe_material%type;
cd_material_w		estrutura_material_v.cd_material%type;

cd_tributo_w		tributo.cd_tributo%type;

pr_aliquota_w		regra_calculo_imposto.pr_imposto%type;
nr_seq_regra_w		regra_calculo_imposto.nr_sequencia%type;

nr_seq_propaci_w		propaci_imposto.nr_sequencia%type;
nr_seq_matpaci_w		matpaci_imposto.nr_sequencia%type;

vl_material_w		material_atend_paciente.vl_material%type;
vl_procedimento_w		procedimento_paciente.vl_procedimento%type;
nr_seq_proc_princ_w	procedimento_paciente.nr_seq_proc_princ%type;
ie_item_excluido_w	varchar2(1) := 'N';

Cursor C01 is --Cursor dos procedimentos para gera��o dos impostos quando o item for do pacote
	select	b.cd_procedimento,
		b.ie_origem_proced,
		a.cd_estabelecimento,
		a.cd_convenio_parametro,
		b.vl_procedimento,
		b.nr_seq_proc_princ,
		b.nr_sequencia
	from	conta_paciente a,
		procedimento_paciente b
	where	a.nr_interno_conta = b.nr_interno_conta
	and 	b.nr_seq_proc_pacote is not null
	and 	b.nr_sequencia = b.nr_seq_proc_pacote
	and 	a.nr_interno_conta = nr_interno_conta_p;
c01_w	c01%rowtype;

Cursor C02 is --Cursor dos procedimentos e mat/med para deletar os impostos quando o item  n�o for do pacote
	select	'P' ie_proc_mat,
		b.nr_sequencia
	from	conta_paciente a,
		procedimento_paciente b
	where	a.nr_interno_conta = b.nr_interno_conta
	and 	b.nr_sequencia <> b.nr_seq_proc_pacote
	and 	a.nr_interno_conta = nr_interno_conta_p
	union
	select	'M' ie_proc_mat,
		b.nr_sequencia
	from	conta_paciente a,
		material_atend_paciente b
	where	a.nr_interno_conta = b.nr_interno_conta
	and 	b.nr_sequencia <> b.nr_seq_proc_pacote
	and 	a.nr_interno_conta = nr_interno_conta_p;
c02_w	c02%rowtype;

begin

if	(nr_seq_proc_mat_p <> 0) and
	(ie_proc_mat_p is not null) then
	
	if	(ie_proc_mat_p = 'P') then
		select	nvl(max('N'),'S')
		into	ie_item_excluido_w
		from	procedimento_paciente
		where	nr_sequencia = nr_seq_proc_mat_p
		and	nr_interno_conta is not null;
	elsif	(ie_proc_mat_p = 'M') then
		select	nvl(max('N'),'S')
		into	ie_item_excluido_w
		from	material_atend_paciente
		where	nr_sequencia = nr_seq_proc_mat_p
		and	nr_interno_conta is not null;
	end if;
	
end if;

if	(nr_interno_conta_p is not null) and
	(nr_seq_proc_mat_p <> 0) and
	(ie_proc_mat_p is not null) and
	(nvl(ie_item_excluido_w,'N') = 'N') then
		
	select	sum(qt_registros) qt_registros 
	into	qt_registros_pacote_w
	from
		(select 	count(*) qt_registros
		from 	procedimento_paciente
		where	nr_sequencia = nr_seq_proc_mat_p
		and	ie_proc_mat_p = 'P'
		and 	nr_seq_proc_pacote is null
		union all
		select 	count(*) qt_registros
		from 	material_atend_paciente
		where	nr_sequencia = nr_seq_proc_mat_p
		and	ie_proc_mat_p = 'M'
		and 	nr_seq_proc_pacote is null);
		
	if	(ie_proc_mat_p = 'P') then
	
		select	count(*)
		into	qt_registros_w
		from	propaci_imposto
		where	nr_seq_propaci = nr_seq_proc_mat_p;
	
		if	(qt_registros_w > 0) then
			delete from propaci_imposto where nr_seq_propaci = nr_seq_proc_mat_p;
		end if;
	elsif	(ie_proc_mat_p = 'M') then
		
		select	count(*)
		into	qt_registros_w
		from	matpaci_imposto
		where	nr_seq_matpaci = nr_seq_proc_mat_p;
	
		if	(qt_registros_w > 0) then
			delete from matpaci_imposto where nr_seq_matpaci = nr_seq_proc_mat_p;
		end if;
		
	end if;
	
		
	if	(qt_registros_pacote_w > 0) then
	
		if	(ie_proc_mat_p = 'P') then
			
			begin
			select	b.cd_procedimento,
				b.ie_origem_proced,
				a.cd_estabelecimento,
				a.cd_convenio_parametro,
				b.vl_procedimento,
				b.nr_seq_proc_princ
			into	cd_procedimento_w,
				ie_origem_proced_w,
				cd_estabelecimento_w,
				cd_convenio_w,
				vl_procedimento_w,
				nr_seq_proc_princ_w
			from	conta_paciente a,
				procedimento_paciente b
			where	a.nr_interno_conta = b.nr_interno_conta
			and 	a.nr_interno_conta = nr_interno_conta_p
			and	b.nr_sequencia = nr_seq_proc_mat_p;
			exception
				when others then
				cd_procedimento_w	:= 0;
				ie_origem_proced_w	:= 0;
				cd_estabelecimento_w	:= 1;
				cd_convenio_w		:= 0;
				vl_procedimento_w		:= 0;
			end;
	
			select	nvl(max(a.cd_grupo_proc),0),
				nvl(max(a.cd_especialidade),0),
				nvl(max(a.cd_area_procedimento),0)
			into	cd_grupo_proc_w,
				cd_especialidade_w,
				cd_area_procedimento_w
			from	estrutura_procedimento_v a
			where	a.cd_procedimento = cd_procedimento_w
			and	a.ie_origem_proced = ie_origem_proced_w;
	
		elsif	(ie_proc_mat_p = 'M') then			
	
			begin
			select	b.cd_material,
				a.cd_estabelecimento,
				a.cd_convenio_parametro,
				b.vl_material
			into	cd_material_w,
				cd_estabelecimento_w,
				cd_convenio_w,
				vl_material_w
			from	conta_paciente a,
				material_atend_paciente b
			where	a.nr_interno_conta = b.nr_interno_conta
			and	a.nr_interno_conta = nr_interno_conta_p
			and	b.nr_sequencia = nr_seq_proc_mat_p;
			exception 
				when others then
				cd_material_w		:= 0;
				cd_estabelecimento_w	:= 1;
				cd_convenio_w		:= 0;
				vl_material_w		:= 0;
			end;
	
			select	a.cd_classe_material,
				a.cd_subgrupo_material,
				a.cd_grupo_material
			into	cd_classe_material_w,
				cd_subgrupo_material_w,
				cd_grupo_material_w
			from	estrutura_material_v a
			where	a.cd_material = cd_material_w;		
	
		end if;
	
		nr_seq_regra_w	:= null;
		cd_tributo_w	:= null;
		pr_aliquota_w	:= 0;
	
		/*select	max(cd_tributo)
		into	cd_tributo_w
		from	tributo
		where	ie_situacao = 'A'
		and	ie_tipo_tributo = 'IVA'
		and	(nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w);*/
					
		obter_dados_trib_conta_pac(	cd_estabelecimento_w,
					cd_convenio_w,
					sysdate,
					nvl(cd_area_procedimento_w,0),
					nvl(cd_especialidade_w,0),
					nvl(cd_grupo_proc_w,0),
					nvl(cd_procedimento_w,0),
					nvl(ie_origem_proced_w,0),
					nvl(cd_grupo_material_w,0),
					nvl(cd_subgrupo_material_w,0),
					nvl(cd_classe_material_w,0),
					nvl(cd_material_w,0),
					cd_tributo_w,
					pr_aliquota_w,
					nr_seq_regra_w);
	
		if	(ie_proc_mat_p = 'P') and (cd_tributo_w is not null) and (pr_aliquota_w is not null) then
	
			select	propaci_imposto_seq.nextval
			into	nr_seq_propaci_w
			from	dual;
	
			insert into propaci_imposto (
				nr_sequencia,
				nr_seq_propaci,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_tributo,
				pr_imposto,
				vl_imposto,
				nr_seq_regra
			) values (
				nr_seq_propaci_w,				--nr_sequencia
				nr_seq_proc_mat_p,			--nr_seq_propaci
				sysdate,					--dt_atualizacao
				nm_usuario_p,				--nm_usuario
				sysdate,					--dt_atualizacao_nrec
				nm_usuario_p,				--nm_usuario_nrec
				cd_tributo_w,				--cd_tributo
				pr_aliquota_w,				--pr_imposto
				(vl_procedimento_w * (pr_aliquota_w / 100)),	--vl_imposto
				nr_seq_regra_w);				--nr_seq_regra
	
		elsif	(ie_proc_mat_p = 'M') and (cd_tributo_w is not null) and (pr_aliquota_w is not null) then
	
			select	matpaci_imposto_seq.nextval
			into	nr_seq_matpaci_w
			from	dual;
	
			insert into matpaci_imposto (
				nr_sequencia,
				nr_seq_matpaci,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_tributo,
				pr_imposto,
				vl_imposto,
				nr_seq_regra
			) values (
				nr_seq_matpaci_w,			--nr_sequencia
				nr_seq_proc_mat_p,		--nr_seq_matpaci
				sysdate,				--dt_atualizacao
				nm_usuario_p,			--nm_usuario
				sysdate,				--dt_atualizacao_nrec
				nm_usuario_p,			--nm_usuario_nrec
				cd_tributo_w,			--cd_tributo
				pr_aliquota_w,			--pr_imposto
				(vl_material_w * (pr_aliquota_w / 100)),	--vl_imposto
				nr_seq_regra_w);			--nr_seq_regra
	
		elsif	(ie_proc_mat_p = 'P') and (nr_seq_proc_princ_w is not null) then
		
			select	count(*)
			into	qt_registros_w
			from	propaci_imposto
			where	nr_seq_propaci = nr_seq_proc_princ_w;
			
			if	(qt_registros_w > 0) then
			
				select	propaci_imposto_seq.nextval
				into	nr_seq_propaci_w
				from	dual;
			
				insert into propaci_imposto (
					nr_sequencia,
					nr_seq_propaci,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_tributo,
					pr_imposto,
					vl_imposto,
					nr_seq_regra)
				select	nr_seq_propaci_w,
					nr_seq_proc_mat_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_tributo,
					pr_imposto,
					decode(sign(vl_imposto),-1, vl_imposto * -1, vl_imposto),
					nr_seq_regra
				from	propaci_imposto
				where	nr_seq_propaci = nr_seq_proc_princ_w;
	
			end if;
		
		end if;
	end if;
end if;

--Esta rotina � executada quando na gera��o do pacote em Calcular_Pacote
--Seu objetivo � calcular os impostos sobre os itens rateados do pacote
if	(nr_interno_conta_p is not null) and
	(nr_seq_proc_mat_p = 0) and
	(ie_proc_mat_p = 'P') then
	
	open C02;
	loop
	fetch C02 into	
		c02_w;
	exit when C02%notfound;
		begin
			
		if	(c02_w.ie_proc_mat = 'P') then
		
			select	count(*)
			into	qt_registros_w
			from	propaci_imposto
			where	nr_seq_propaci = c02_w.nr_sequencia;
		
			if	(qt_registros_w > 0) then
				delete from propaci_imposto where nr_seq_propaci = c02_w.nr_sequencia;
			end if;
		elsif	(c02_w.ie_proc_mat = 'M') then
			
			select	count(*)
			into	qt_registros_w
			from	matpaci_imposto
			where	nr_seq_matpaci = c02_w.nr_sequencia;
		
			if	(qt_registros_w > 0) then
				delete from matpaci_imposto where nr_seq_matpaci = c02_w.nr_sequencia;
			end if;
			
		end if;
		end;
	end loop;
	close C02;
	
	
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin
				
		select	count(*)
		into	qt_registros_w
		from	propaci_imposto
		where	nr_seq_propaci = c01_w.nr_sequencia;
	
		if	(qt_registros_w > 0) then
			delete from propaci_imposto where nr_seq_propaci = c01_w.nr_sequencia;
		end if;
		
		select	nvl(max(a.cd_grupo_proc),0),
			nvl(max(a.cd_especialidade),0),
			nvl(max(a.cd_area_procedimento),0)
		into	cd_grupo_proc_w,
			cd_especialidade_w,
			cd_area_procedimento_w
		from	estrutura_procedimento_v a
		where	a.cd_procedimento = c01_w.cd_procedimento
		and	a.ie_origem_proced = c01_w.ie_origem_proced;		
	
		nr_seq_regra_w	:= null;
		cd_tributo_w	:= null;
		pr_aliquota_w	:= 0;
	
		/*select	max(cd_tributo)
		into	cd_tributo_w
		from	tributo
		where	ie_situacao = 'A'
		and	ie_tipo_tributo = 'IVA'
		and	(nvl(cd_estabelecimento, c01_w.cd_estabelecimento) = c01_w.cd_estabelecimento);*/
					
		obter_dados_trib_conta_pac(	c01_w.cd_estabelecimento,
					c01_w.cd_convenio_parametro,
					sysdate,
					nvl(cd_area_procedimento_w,0),
					nvl(cd_especialidade_w,0),
					nvl(cd_grupo_proc_w,0),
					nvl(c01_w.cd_procedimento,0),
					nvl(c01_w.ie_origem_proced,0),
					0,
					0,
					0,
					0,
					cd_tributo_w,
					pr_aliquota_w,
					nr_seq_regra_w);
	
		if	(cd_tributo_w is not null) and (pr_aliquota_w is not null) then
	
			select	propaci_imposto_seq.nextval
			into	nr_seq_propaci_w
			from	dual;
	
			insert into propaci_imposto (
				nr_sequencia,
				nr_seq_propaci,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_tributo,
				pr_imposto,
				vl_imposto,
				nr_seq_regra
			) values (
				nr_seq_propaci_w,				--nr_sequencia
				c01_w.nr_sequencia,				--nr_seq_propaci
				sysdate,					--dt_atualizacao
				nm_usuario_p,					--nm_usuario
				sysdate,					--dt_atualizacao_nrec
				nm_usuario_p,					--nm_usuario_nrec
				cd_tributo_w,					--cd_tributo
				pr_aliquota_w,					--pr_imposto
				(c01_w.vl_procedimento * (pr_aliquota_w / 100)),--vl_imposto
				nr_seq_regra_w);				--nr_seq_regra				
					
		elsif	(c01_w.nr_seq_proc_princ is not null) then
		
			select	count(*)
			into	qt_registros_w
			from	propaci_imposto
			where	nr_seq_propaci = c01_w.nr_seq_proc_princ;
			
			if	(qt_registros_w > 0) then
			
				select	propaci_imposto_seq.nextval
				into	nr_seq_propaci_w
				from	dual;
			
				insert into propaci_imposto (
					nr_sequencia,
					nr_seq_propaci,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_tributo,
					pr_imposto,
					vl_imposto,
					nr_seq_regra)
				select	nr_seq_propaci_w,
					c01_w.nr_sequencia,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_tributo,
					pr_imposto,
					decode(sign(vl_imposto),-1, vl_imposto * -1, vl_imposto),
					nr_seq_regra
				from	propaci_imposto
				where	nr_seq_propaci = c01_w.nr_seq_proc_princ;
	
			end if;
		
		end if;
		end;
	end loop;
	close C01;
end if;

end gerar_tributo_conta_pac;
/