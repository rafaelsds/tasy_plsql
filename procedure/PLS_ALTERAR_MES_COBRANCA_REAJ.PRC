create or replace
procedure pls_alterar_mes_cobranca_reaj
		(	nr_seq_contrato_p	number,
			ie_mes_cobranca_reaj_p	varchar2,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number) is 
			
ds_mes_cobranca_antigo_w	varchar2(255);
ds_mes_cobranca_novo_w		varchar2(255);

begin

if	(nvl(nr_seq_contrato_p,0) <> 0) then

	select	decode(ie_mes_cobranca_reaj, 'R', 'Conforme regra', 'M', 'M�s de anivers�rio', 'P', 'M�s posterior ao anivers�rio', null),
		decode(ie_mes_cobranca_reaj_p, 'R', 'Conforme regra', 'M', 'M�s de anivers�rio', 'P', 'M�s posterior ao anivers�rio', null)
	into	ds_mes_cobranca_antigo_w,
		ds_mes_cobranca_novo_w
	from	pls_contrato
	where	nr_sequencia = nr_seq_contrato_p;

	update	pls_contrato
	set	ie_mes_cobranca_reaj 	= ie_mes_cobranca_reaj_p,
		dt_atualizacao 		= sysdate,
		nm_usuario 		= nm_usuario_p
	where	nr_sequencia		= nr_seq_contrato_p;
	
	insert into pls_contrato_historico 
		      (	nr_sequencia, cd_estabelecimento, nr_seq_contrato, dt_historico, ie_tipo_historico, 
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			ds_historico,ds_observacao)
		values(	pls_contrato_historico_seq.NextVal, cd_estabelecimento_p, nr_seq_contrato_p, sysdate, '63', 
			sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			'De: ' || ds_mes_cobranca_antigo_w || '. Para: ' || ds_mes_cobranca_novo_w,
			'pls_alterar_mes_cobranca_reaj');

end if;

commit;

end pls_alterar_mes_cobranca_reaj;
/
