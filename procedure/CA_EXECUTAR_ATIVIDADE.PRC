create or replace
procedure ca_executar_atividade(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		number) is 

begin

if	(nvl(nr_sequencia_p,0) > 0) then

	update	ca_controle_atividade
	set		dt_execucao = sysdate,
			ie_status = 'R',
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p,
			nm_usuario_exec = nm_usuario_p,
			cd_pessoa_fisica = decode(cd_pessoa_fisica,'',Obter_Pf_Usuario(nm_usuario_p,'C'),cd_pessoa_fisica)
	where	nr_sequencia = nr_sequencia_p;
	
	ca_mov_estoque_materiais(nr_sequencia_p,nm_usuario_p);

end if;

commit;

end ca_executar_atividade;
/
