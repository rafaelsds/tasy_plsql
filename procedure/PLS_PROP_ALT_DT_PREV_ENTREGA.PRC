create or replace
procedure pls_prop_alt_dt_prev_entrega
			(	nr_seq_proposta_p	Number,
				dt_prev_entrega_contrato_p	Date,
				nm_usuario_p		Varchar2) is 

dt_prev_entrega_contrato_w	date;
ds_historico_w		varchar2(255);

begin

select	dt_prev_entrega_contrato
into	dt_prev_entrega_contrato_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;


update	pls_proposta_adesao
set	dt_prev_entrega_contrato	= dt_prev_entrega_contrato_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_proposta_p;

if (dt_prev_entrega_contrato_w is not null) then
	ds_historico_w	:= 'Data prevista de entrega da proposta alterada de '|| to_char(dt_prev_entrega_contrato_w, 'DD/MM/YYYY HH24:MI')||' para '||to_char(dt_prev_entrega_contrato_p, 'DD/MM/YYYY HH24:MI');
else
	ds_historico_w	:= 'Data prevista de entrega da proposta alterada de EM BRANCO para '||to_char(dt_prev_entrega_contrato_p, 'DD/MM/YYYY HH24:MI');
end if;

insert	into	pls_proposta_historico
	(	nr_sequencia, nr_seq_proposta, dt_historico,
		ds_historico, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, ie_origem_historico,
		dt_liberacao_historico,nm_usuario_liberacao)
	values
	(	pls_proposta_historico_seq.nextval, nr_seq_proposta_p, sysdate,
		ds_historico_w, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, 1, sysdate, nm_usuario_p);

commit;

end pls_prop_alt_dt_prev_entrega;
/
