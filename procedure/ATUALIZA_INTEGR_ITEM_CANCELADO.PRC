create or replace
procedure atualiza_integr_item_cancelado(	nr_solic_compra_p		number,
					nr_item_solic_compra_p	number,
					nm_usuario_p		Varchar2) is 

begin

if	(nr_item_solic_compra_p > 0) then

	update	solic_compra_item
	set	ie_cancelamento_integr = 'S'
	where	nr_solic_compra = nr_solic_compra_p
	and	nr_item_solic_compra = nr_item_solic_compra_p
	and	dt_baixa is not null;
else
	update	solic_compra_item
	set	ie_cancelamento_integr = 'S'
	where	nr_solic_compra = nr_solic_compra_p
	and	dt_baixa is not null;
end if;

commit;

end atualiza_integr_item_cancelado;
/