create or replace procedure pie_practitioner_prof (nr_seq_pie_message_p	in pie_message.nr_sequencia%type,
						dt_name_edit_p		in pie_practitioner.dt_name_edit%type,
						ds_name_title_p		in pie_practitioner.ds_name_title%type,
						ds_name_family_p	in pie_practitioner.ds_name_family%type,
						ds_name_given_p		in pie_practitioner.ds_name_given%type,
						ds_name_middle_p	in pie_practitioner.ds_name_middle%type,
						tp_gender_p		in pie_practitioner.tp_gender%type,
						nr_profession_p		in pie_practitioner.nr_profession%type,
						tp_profession_p		in pie_practitioner.tp_profession%type,
						dt_profession_start_p	in pie_practitioner.dt_profession_start%type,
						nr_seq_pie_pract_p		out pie_practitioner.nr_sequencia%type)
is
  nr_sequencia_w	pie_practitioner.nr_sequencia%type;
begin

	if(( dt_name_edit_p is not null )
	and( ds_name_family_p is not null )
	and( tp_gender_p is not null )
	and( nr_profession_p is not null )
	and( tp_profession_p is not null )
	and ( nr_seq_pie_message_p is not null)) then

		select pie_practitioner_seq.nextval
		into   nr_sequencia_w
		from   dual;

		insert into pie_practitioner
		(nr_sequencia,
		dt_name_edit,
		ds_name_title,
		ds_name_family,
		ds_name_given,
		ds_name_middle,
		tp_gender,
		nr_profession,
		tp_profession,
		dt_profession_start,
		nr_seq_pie_message
		)
		values
		(
		 nr_sequencia_w,
		 dt_name_edit_p,
		 ds_name_title_p,
		 ds_name_family_p,
		 ds_name_given_p,
		 ds_name_middle_p,
		 tp_gender_p,
		 nr_profession_p,
		 tp_profession_p,
		 dt_profession_start_p,
		 nr_seq_pie_message_p
		);
	else
		wheb_mensagem_pck.exibir_mensagem_abort(1088701);
	end if;
      nr_seq_pie_pract_p:= nr_sequencia_w;
end pie_practitioner_prof;
/