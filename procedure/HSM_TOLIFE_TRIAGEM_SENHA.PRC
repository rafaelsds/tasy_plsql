CREATE OR REPLACE procedure hsm_tolife_triagem_senha(	nr_atendimento_p            out number,
                                dt_inicio_triagem_p         varchar2,
                                dt_fim_triagem_p            varchar2,
                                ds_prioridade_p             varchar2,
                                ie_prioridade_p             varchar2,
                                ds_fluxograma_p             varchar2,
                                ds_queixa_p                 varchar2,
                                ds_descriminador_p          varchar2,
                                nm_usuario_p                varchar2,
                                nr_seq_id_triagem_p         number,
                                cd_pessoa_fisica_p          number,
                                cd_estabelecimento_toLife_p varchar2,
                                nr_seq_pac_senha_fila_p     number,
                                ds_porta_entrada_p          varchar2,
                                ds_encaminhamentos_p        varchar2 default '',
                                ie_suspeita_covid_p 		varchar2 default 'N',
                                ie_suicidio_1_p varchar2 default 'N',
                                ie_suicidio_2_p varchar2 default 'N',
                                ie_suicidio_3_p varchar2 default 'N')

 is

cd_estabelecimento_philips_w    number(15);
ie_clinica_w                    number(15);
cd_procedencia_w                number(10) := 1;
cd_medico_resp_w                number(10) := 387835;
tipo_atendimento_ps_w           number(10) := 3;
nr_atendimento_w                number(10) ;

begin

  select max(cd_estabelecimento)
  into   cd_estabelecimento_philips_w
  from   to_life_estabelecimento;

  select nvl(max(ie_clinica ), 0)
  into   ie_clinica_w
  from   to_life_clinica
  where  upper(ds_porta_entrada) = upper(ds_porta_entrada_p);  


  ToLife_Gerar_atendimento(nr_atendimento_w, cd_pessoa_fisica_p, cd_estabelecimento_philips_w, cd_procedencia_w, tipo_atendimento_ps_w, cd_medico_resp_w, nm_usuario_p, nr_seq_pac_senha_fila_p, ie_clinica_w);

  ToLife_Triagem_Paciente(nr_atendimento_w,
                          dt_inicio_triagem_p,
                          dt_fim_triagem_p,
                          ds_prioridade_p,
                          ie_prioridade_p,
                          ds_fluxograma_p,
                          ds_queixa_p,
                          ds_descriminador_p,
                          nm_usuario_p,
                          nr_seq_id_triagem_p,
                          ds_encaminhamentos_p,
                          ie_suspeita_covid_p,
                          ie_suicidio_1_p,
                          ie_suicidio_2_p,
                          ie_suicidio_3_p);

  Vincula_Atendimento_Senha(nr_seq_pac_senha_fila_p,
                            cd_pessoa_fisica_p,
                            cd_estabelecimento_philips_w,
                            'V',
                            nr_atendimento_w,
                            nm_usuario_p);


  commit;
  
  nr_atendimento_p := nr_atendimento_w;
  
end hsm_tolife_triagem_senha;
/
