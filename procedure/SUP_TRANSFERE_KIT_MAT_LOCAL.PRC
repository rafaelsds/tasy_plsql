create or replace
procedure	sup_transfere_kit_mat_local(
			nr_seq_kit_estoque_p		number,
			nr_seq_reg_kit_p		number,
			cd_local_destino_p		number,
			cd_operacao_estoque_p		number,
			cd_setor_atendimento_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

cd_local_origem_w		number(4);
nr_seq_log_w			number(10);
cd_operacao_correspondente_w	number(3);
cd_material_w			number(6);
qt_material_w			number(11,4);
nr_seq_lote_w			number(10);
nr_sequencia_comp_w		number(10);

nr_movimento_estoque_w		number(10);
nr_movimento_estoque_ww		number(10);
dt_mesano_referencia_w		date;
cd_unidade_medida_consumo_w	varchar2(30);
cd_material_estoque_w		number(6);
ds_lote_w			varchar2(20);
dt_validade_w			date;
ie_local_valido_w		varchar2(1);
ds_material_w			material.ds_material%type;
cd_fornec_consignado_w  kit_estoque_comp.cd_fornec_consignado%type;

nr_seq_regra_w			number(10);
qt_diferenca_w			number (10) := 0;
ie_continua				Varchar2(1)	:= 'S';
qt_baixado_proprio_w	number(15,4);
qt_baixado_consignado_w	number(15,4);
qt_material_baixar		number(15,4);
qt_estoque_w			number(15,4);
ie_consignado_w			varchar2(1);
ie_tipo_saldo_w			varchar2(1);
cd_operacao_estoque_w	number(3) := null;

cursor	c01 is
select	cd_material,
	qt_material,
	nvl(nr_seq_lote_fornec,0),
	nr_sequencia,
	cd_fornec_consignado
from	kit_estoque_comp
where	nr_seq_kit_estoque = nr_seq_kit_estoque_p
and	dt_exclusao is null;

begin

select	cd_local_estoque
into	cd_local_origem_w
from	kit_estoque
where	nr_sequencia = nr_seq_kit_estoque_p;

select	nvl(max(cd_operacao_correspondente),0)
into	cd_operacao_correspondente_w
from	operacao_estoque
where	cd_operacao_estoque = cd_operacao_estoque_p;

select	nvl(max(IE_REGRA_SALDO_CONSIG),0)
into	nr_seq_regra_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;

open c01;
loop
fetch c01 into
	cd_material_w,
	qt_material_w,
	nr_seq_lote_w,
	nr_sequencia_comp_w,
	cd_fornec_consignado_w;
exit when c01%notfound;
	begin
	Obter_Local_Valido(cd_estabelecimento_p, cd_local_destino_p, cd_material_w, null, ie_local_valido_w);
	qt_baixado_proprio_w 	:= 0;
	qt_baixado_consignado_w := 0;
	qt_material_baixar := qt_material_w;
	qt_diferenca_w := 0;
	ie_continua := 'S';
	
	select	max(ie_consignado)
	into	ie_consignado_w
	from	material
	where	cd_material = cd_material_w;
			
	if	(ie_local_valido_w = 'N') then
		begin
		select	ds_material
		into	ds_material_w
		from	material
		where	cd_material = cd_material_w;
		
		wheb_mensagem_pck.exibir_mensagem_abort(896471,'CD_MATERIAL=' || cd_material_w || ';DS_MATERIAL=' || ds_material_w);
		-- Material: #@CD_MATERIAL#@ - #@DS_MATERIAL#@
		-- n�o liberado para o local de estoque!
		end;
	end if;
	
	while (qt_diferenca_w > 0 or ie_continua = 'S')loop
	begin	
		select	movimento_estoque_seq.nextval
		into	nr_movimento_estoque_w
		from	dual;

		select	dt_mesano_vigente
		into	dt_mesano_referencia_w
		from	parametro_estoque
		where	cd_estabelecimento = cd_estabelecimento_p;

		select	cd_material_estoque
		into	cd_material_estoque_w
		from	material
		where	cd_material = cd_material_w;
		
		cd_unidade_medida_consumo_w	:=	obter_dados_material_estab(cd_material_w, cd_estabelecimento_p, 'UMS');

		ds_lote_w	:= '';
		dt_validade_w	:= null;
		if	(nvl(nr_seq_lote_w,0) > 0) then

			select	ds_lote_fornec,
				dt_validade
			into	ds_lote_w,
				dt_validade_w
			from	material_lote_fornec
			where	nr_sequencia = nr_seq_lote_w;
		end if;	
		
		if((ie_consignado_w = '2') and (nr_seq_regra_w > 0))then				
		
			obter_fornec_consig_ambos(cd_estabelecimento_p, cd_material_w, nvl(nr_seq_lote_w,0), cd_local_origem_w, ie_tipo_saldo_w, cd_fornec_consignado_w);
			
			if (ie_tipo_saldo_w = 'N')then
				obter_saldo_estoque(cd_estabelecimento_p, cd_material_w, cd_local_origem_w, PKG_DATE_UTILS.start_of(sysdate, 'month', 0), qt_estoque_w);
			elsif (ie_tipo_saldo_w = 'C')then
			begin
				select	nvl(sum(qt_estoque),0)
				into	qt_estoque_w
				from	fornecedor_mat_consignado
				where	cd_estabelecimento	= cd_estabelecimento_p
				and	cd_local_estoque		= cd_local_origem_w
				and	cd_fornecedor			= cd_fornec_consignado_w
				and	cd_material				= cd_material_w
				and	dt_mesano_referencia	= trunc(sysdate,'mm');	

				select	cd_operacao_transf_setor_consi
				into	cd_operacao_estoque_w
				from	parametro_estoque
				where	cd_estabelecimento = cd_estabelecimento_p;
				
				select	cd_operacao_correspondente
				into	cd_operacao_correspondente_w
				from	operacao_estoque
				where	cd_operacao_estoque  = cd_operacao_estoque_w;
			end;
			end if;

			if ((ie_tipo_saldo_w = 'N') or (ie_tipo_saldo_w = 'C'))then
			begin
				if (qt_diferenca_w > 0)then
					qt_material_w := qt_diferenca_w;
				end if;
										
				if (qt_estoque_w >= qt_material_w)then
				begin
					qt_diferenca_w := 0;
					ie_continua    := 'N';
					
					if (ie_tipo_saldo_w = 'N')then
						qt_baixado_proprio_w := qt_baixado_proprio_w + qt_material_w;
					else									
						qt_baixado_consignado_w := qt_baixado_consignado_w + qt_material_w;
					end if;									
				end;
				else
				begin
					qt_diferenca_w := qt_material_w - qt_estoque_w;
					qt_material_w  := qt_estoque_w;
					ie_continua    := 'S';
					
					if (ie_tipo_saldo_w = 'N')then
						qt_baixado_proprio_w := qt_baixado_proprio_w + qt_material_w;
					else									
						qt_baixado_consignado_w := qt_baixado_consignado_w + qt_material_w;
					end if;
				end;
				end if;
			end;
			else
			begin
				qt_diferenca_w := 0;
				ie_continua    := 'N';
			end;
			end if;
		else
		begin
			qt_diferenca_w := 0;
			ie_continua    := 'N';
		end;
		end if;
			
		insert into movimento_estoque(
			nr_movimento_estoque,		cd_estabelecimento,
			cd_local_estoque,		dt_movimento_estoque,
			cd_operacao_estoque, 		cd_acao,
			cd_material,	 		dt_mesano_referencia,
			qt_movimento,	 		dt_atualizacao,
			nm_usuario,	 		ie_origem_documento,
			nr_documento,	 		nr_sequencia_item_docto,
			cd_cgc_emitente,		cd_serie_nf,
			nr_sequencia_documento,		vl_movimento,
			cd_unidade_medida_estoque,	cd_procedimento,
			cd_setor_atendimento, 		cd_conta,
			dt_contabil,	 		cd_lote_fabricacao,
			dt_validade,			qt_estoque,
			dt_processo,	 		cd_local_estoque_destino,
			cd_centro_custo,		cd_unidade_med_mov,
			nr_movimento_estoque_corresp,	cd_conta_contabil,
			cd_material_estoque,		ie_origem_proced,
			cd_fornecedor,	 		nr_lote_contabil,
			nr_seq_tab_orig,		nr_seq_lote_fornec)
				values(
				nr_movimento_estoque_w,		cd_estabelecimento_p,
				cd_local_origem_w,		sysdate,
				nvl(cd_operacao_estoque_w,cd_operacao_estoque_p),		'1',
				cd_material_w,			dt_mesano_referencia_w,
				qt_material_w,			sysdate,
				nm_usuario_p,			'2',
				to_char(nr_seq_kit_estoque_p),	nr_sequencia_comp_w,
				null,				null,
				nr_seq_kit_estoque_p,		null,
				cd_unidade_medida_consumo_w,	null,
				cd_setor_atendimento_p,		null,
				null,				ds_lote_w,
				dt_validade_w,			qt_material_w,
				null,				cd_local_destino_p,
				null,				cd_unidade_medida_consumo_w,
				null,				null,
				cd_material_estoque_w,		null,
				cd_fornec_consignado_w,				null,
				nr_sequencia_comp_w,		decode(nr_seq_lote_w, 0, null, nr_seq_lote_w));

		if	(cd_operacao_correspondente_w > 0) then
			begin

			select	movimento_estoque_seq.nextval
			into	nr_movimento_estoque_ww
			from	dual;

			insert into movimento_estoque(
				nr_movimento_estoque,		cd_estabelecimento,
				cd_local_estoque,		dt_movimento_estoque,
				cd_operacao_estoque, 		cd_acao,
				cd_material,	 		dt_mesano_referencia,
				qt_movimento,	 		dt_atualizacao,
				nm_usuario,	 		ie_origem_documento,
				nr_documento,	 		nr_sequencia_item_docto,
				cd_cgc_emitente,		cd_serie_nf,
				nr_sequencia_documento,		vl_movimento,
				cd_unidade_medida_estoque,	cd_procedimento,
				cd_setor_atendimento, 		cd_conta,
				dt_contabil,	 		cd_lote_fabricacao,
				dt_validade,			qt_estoque,
				dt_processo,	 		cd_local_estoque_destino,
				cd_centro_custo,		cd_unidade_med_mov,
				nr_movimento_estoque_corresp,	cd_conta_contabil,
				cd_material_estoque,		ie_origem_proced,
				cd_fornecedor,	 		nr_lote_contabil,
				nr_seq_tab_orig,		nr_seq_lote_fornec)
					values(
					nr_movimento_estoque_ww,	cd_estabelecimento_p,
					cd_local_destino_p,		sysdate,
					cd_operacao_correspondente_w,	'1',
					cd_material_w,			dt_mesano_referencia_w,
					qt_material_w,			sysdate,
					nm_usuario_p,			'2',
					to_char(nr_seq_kit_estoque_p),	nr_sequencia_comp_w,
					null,				null,
					nr_seq_kit_estoque_p,		null,
					cd_unidade_medida_consumo_w,	null,
					cd_setor_atendimento_p,		null,
					null,				ds_lote_w,
					dt_validade_w,			qt_material_w,
					null,				cd_local_origem_w,
					null,				cd_unidade_medida_consumo_w,
					nr_movimento_estoque_w,		null,
					cd_material_estoque_w,		null,
					cd_fornec_consignado_w,				null,
					nr_sequencia_comp_w,		decode(nr_seq_lote_w, 0, null, nr_seq_lote_w));
			end;
		end if;
		
		
		if (((qt_diferenca_w > 0) and (qt_material_baixar = (qt_baixado_consignado_w + qt_baixado_proprio_w))) or (qt_diferenca_w = 0) or (ie_continua = 'N') and ((ie_consignado_w = '2')and (nr_seq_regra_w > 0)))then
		begin
			qt_diferenca_w := 0;
			ie_continua := 'N';
		end;
		elsif ((ie_consignado_w = '2') and (nr_seq_regra_w > 0))then
		begin
			ie_continua := 'S';
			qt_material_w := qt_diferenca_w;
		end;
		else
			ie_continua := 'N';
			qt_diferenca_w := 0;
		end if;
	end;												
	end loop;
	end;
end loop;
close c01;

select	kit_estoque_log_transf_seq.nextval
into	nr_seq_log_w
from	dual;

insert into kit_estoque_log_transf(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_kit_estoque,
	nr_seq_reg_kit,
	cd_local_origem,
	cd_local_destino) values (
		nr_seq_log_w,
		sysdate,
		nm_usuario_p,
		nr_seq_kit_estoque_p,
		nr_seq_reg_kit_p,
		cd_local_origem_w,
		cd_local_destino_p);

update	kit_estoque
set	cd_local_estoque = cd_local_destino_p
where	nr_sequencia = nr_seq_kit_estoque_p;

commit;
		
end sup_transfere_kit_mat_local;
/
