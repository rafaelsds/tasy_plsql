create or replace
procedure Consiste_Liberacao_Escala(	ie_escala_p	number) is 

qt_reg_w	number(10);
cd_cgc_w	varchar2(14);

begin

select	count(*)
into	qt_reg_w
from	ESCALA_LIBERACAO
where	ie_escala	= ie_escala_p;

if	(qt_reg_w	> 0) then

	cd_cgc_w	:= obter_cgc_estabelecimento(wheb_usuario_pck.get_cd_estabelecimento);
	
		select sum(qt_liberacao)
		into	qt_reg_w	    
        from   (
                select	nvl(max(1), 0) qt_liberacao
                from	escala_liberacao
                where	IE_ESCALA	= ie_escala_p
                and		CD_CGC_CLIENTE = cd_cgc_w
                and     nvl(IE_LIBERACAO_FILIAL,'S') = 'S'
                union all 
                select	nvl(max(1), 0) qt_liberacao
                from	escala_liberacao
                where	IE_ESCALA	= ie_escala_p
                and     nvl(IE_LIBERACAO_FILIAL,'S') = 'N'              
                and		substr(CD_CGC_CLIENTE,1, 8) = substr(cd_cgc_w,1,8)
                ) x; 
				
	if	(qt_reg_w	= 0) then
		-- Esta escala necessita de uma autorizacao por escrito do autor da escala para permitir sua liberacao.
		Wheb_mensagem_pck.exibir_mensagem_abort(263515);
	end if;
end if;

end Consiste_Liberacao_Escala;
/
