create or replace
procedure REGISTRA_DATA_FIM_EQUIPAMENTO(	nr_sequencia_p		number,
											nr_atendimento_p	number,
											dt_fim_p 			date) is

qt_existe_w number(2);

begin

if (dt_fim_p is not null) then
	Select	count(*)
	into	qt_existe_w
	from	equipamento_cirurgia
	where	nr_atendimento	= nr_atendimento_p
	and		nr_sequencia 	= nr_sequencia_p;

	if	(qt_existe_w > 0) then
		update  equipamento_cirurgia
		set 	dt_fim 		   = dt_fim_p,
				dt_atualizacao = sysdate
		where 	nr_atendimento = nr_atendimento_p
		and 	nr_sequencia = nr_sequencia_p;
		commit;
	end if;
end if;

end REGISTRA_DATA_FIM_EQUIPAMENTO;
/