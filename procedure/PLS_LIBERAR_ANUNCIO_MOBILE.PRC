create or replace
procedure pls_liberar_anuncio_mobile
			(	nr_seq_anuncio_p	number ) is 

begin

update	pls_anuncio_mobile
set		ie_status = 'S'
where	nr_sequencia	= nr_seq_anuncio_p;

commit;

end pls_liberar_anuncio_mobile;
/
