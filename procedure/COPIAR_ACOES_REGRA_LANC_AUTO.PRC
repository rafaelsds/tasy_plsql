create or replace
procedure copiar_acoes_regra_lanc_auto( nr_seq_regra_origem_p	number,
					nr_seq_regra_destino_p	number,
					ie_apagar_todos_p	varchar2,
					nm_usuario_p		varchar2) is 
	
nr_seq_lanc_w	Number(10,0);
	
Cursor c10 is
	select	*
	from	regra_lanc_aut_pac
	where	nr_seq_regra = nr_seq_regra_origem_p;
	
c10_w	c10%rowtype;

begin

if	(ie_apagar_todos_p = 'S') then
	delete from regra_lanc_aut_pac
	where nr_seq_regra = nr_seq_regra_destino_p;
end if;

open c10;
loop
fetch c10 into	
	c10_w;
exit when c10%notfound;
	begin
	
	select 	nvl(max(nr_seq_lanc),0) + 1
	into	nr_seq_lanc_w
	from 	regra_lanc_aut_pac
	where 	nr_seq_regra = nr_seq_regra_destino_p;
	
	insert into regra_lanc_aut_pac (
			nr_seq_regra,
			nr_seq_lanc,
			dt_atualizacao,
			nm_usuario,
			cd_procedimento,
			ie_origem_proced,
			cd_material,
			qt_lancamento,
			ie_medico_atendimento,
			ie_local_estoque,
			ie_quantidade,
			ie_retorno,
			nr_seq_exame,
			ie_funcao_medico,
			tx_procedimento,
			qt_ano_min,
			qt_ano_max,
			ie_regra_guia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_forma_func_medico,
			nr_seq_proc_interno,
			ie_consiste_item,
			ie_proc_princ_atend,
			cd_estab_item,
			cd_setor_item,
			cd_cnpj_prestador,
			cd_cid_doenca,
			ie_adic_orcamento,
			cd_medico)
		values 	(nr_seq_regra_destino_p,
			nr_seq_lanc_w,
			sysdate,
			nm_usuario_p,
			c10_w.cd_procedimento,
			c10_w.ie_origem_proced,
			c10_w.cd_material,
			c10_w.qt_lancamento,
			c10_w.ie_medico_atendimento,
			c10_w.ie_local_estoque,
			c10_w.ie_quantidade,
			c10_w.ie_retorno,
			c10_w.nr_seq_exame,
			c10_w.ie_funcao_medico,
			c10_w.tx_procedimento,
			c10_w.qt_ano_min,
			c10_w.qt_ano_max,
			c10_w.ie_regra_guia,
			sysdate,
			nm_usuario_p,
			c10_w.ie_forma_func_medico,
			c10_w.nr_seq_proc_interno,
			c10_w.ie_consiste_item,
			c10_w.ie_proc_princ_atend,
			c10_w.cd_estab_item,
			c10_w.cd_setor_item,
			c10_w.cd_cnpj_prestador,
			c10_w.cd_cid_doenca, /*Geliard OS178910*/
			nvl(c10_w.ie_adic_orcamento,'N'),
			c10_w.cd_medico);
	
--	insert into log_XXtasy (cd_log, ds_log, nm_usuario, dt_atualizacao)
--		values (99555, 'C�pia de regra, origem: ' || nr_seq_regra_origem_p || ', destino: ' || nr_seq_regra_destino_p ,nm_usuario_p, sysdate);
		
	end;
end loop;
close c10;

commit;

end copiar_acoes_regra_lanc_auto;
/