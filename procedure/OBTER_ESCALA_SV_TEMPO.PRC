create or replace
procedure obter_escala_sv_tempo(	cd_pessoa_fisica_p	varchar2,
					qt_minutos_p		number,
					cd_escala_dor_p		in out varchar2,
					qt_escala_dor_p		out number) is 

nr_seq_sinal_vital_w	number(10);
cd_escala_dor_w		varchar2(5);
qt_escala_dor_w		number(3,1);

begin

cd_escala_dor_w := cd_escala_dor_p;
cd_escala_dor_p := null;

if (nvl(cd_pessoa_fisica_p,'0') <> '0') then
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_sinal_vital_w
	from	atendimento_sinal_vital
	where	cd_paciente = cd_pessoa_fisica_p
	and	ie_situacao = 'A'
	and	nvl(ie_rn,'N') = 'N'
	and	((cd_escala_dor_w is null) or (cd_escala_dor = cd_escala_dor_w))
	and	sysdate between dt_sinal_vital and (dt_sinal_vital + (qt_minutos_p/1440));

	if (nr_seq_sinal_vital_w > 0) then
		select	cd_escala_dor,
			qt_escala_dor
		into	cd_escala_dor_w,
			qt_escala_dor_w
		from	atendimento_sinal_vital a
		where	a.nr_sequencia = nr_seq_sinal_vital_w;
	end if;

	cd_escala_dor_p := cd_escala_dor_w;
	qt_escala_dor_p := qt_escala_dor_w;
	end;
end if;

end obter_escala_sv_tempo;
/