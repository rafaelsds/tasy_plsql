create or replace
procedure LATAM_atualizar_aderencia (nr_seq_modulo_p	number) is 

qt_maximo_pontos_item_w		number(10);
qt_pontos_obtidos_w			number(10);
pr_aderencia_w				number(15,4);
qt_tempo_horas_w			number(10);
vl_custo_w					number(15,4);

begin

select	sum(qt_maximo_pontos),
		sum(qt_pontuacao_obtida),
		sum(qt_tempo_horas),
		sum(vl_custo)
into	qt_maximo_pontos_item_w,
		qt_pontos_obtidos_w,
		qt_tempo_horas_w,
		vl_custo_w
from	latam_requisito
where	nr_seq_modulo	= nr_seq_modulo_p;

pr_aderencia_w	:= qt_pontos_obtidos_w * 100 / qt_maximo_pontos_item_w;

update	latam_modulo
set		qt_pontos_obtidos	= nvl(qt_pontos_obtidos_w,0),
		qt_maximo_pontos	= nvl(qt_maximo_pontos_item_w,0),
		pr_aderencia		= pr_aderencia_w,
		qt_tempo_horas		= qt_tempo_horas_w,
		vl_custo			= vl_custo_w
where	nr_sequencia		= nr_seq_modulo_p;

commit;

end LATAM_atualizar_aderencia;
/
