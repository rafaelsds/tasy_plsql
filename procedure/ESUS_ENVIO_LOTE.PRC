create or replace
procedure esus_envio_lote(nm_usuario_p varchar2,
				nr_seq_lote_p varchar2,
				ds_arquivo_p varchar2,
				ie_acao_p varchar2) is
begin

if (ie_acao_p = 'E') then
	update esus_lote_envio 
	set dt_envio_lote = sysdate,
	ds_arquivo_envio = ds_arquivo_p,
	nm_usuario_envio = nm_usuario_p,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
	where
	nr_sequencia = nr_seq_lote_p;
else
	update esus_lote_envio 
	set dt_envio_lote = null,
	ds_arquivo_envio = null,
	nm_usuario_envio = null,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
	where
	nr_sequencia = nr_seq_lote_p;
end if;

end esus_envio_lote;
/