create or replace
procedure consistir_todas_prescr(	ds_var_consistir_toda_prescr_p	varchar2,
				nr_prescricao_p			number,
				cd_perfil_p			number,
				nm_usuario_p			varchar2,
				ie_erro_p				out varchar2,
				qt_material_p			out number) is 

ds_erro_w		varchar2(255);
ds_usuario_w		varchar2(255);
ds_perfil_w		varchar2(255);
ds_proced_glosa_w		varchar2(255);
ie_erro_w			varchar2(15)	:= '';
qt_material_w		number(10,0)	:= 0;
				
begin

if	(ds_var_consistir_toda_prescr_p = 'S')then

	consistir_prescricao(nr_prescricao_p, cd_perfil_p, nm_usuario_p, ds_erro_w, ds_usuario_w, ds_perfil_w, ds_proced_glosa_w);

end if;

ie_erro_w	:= obter_se_prescr_conformidade(nr_prescricao_p);

if	(ie_erro_w = 'S')then

	select 	count(*)
	into	qt_material_w
	from 	prescr_material 
	where 	nr_prescricao = nr_prescricao_p 
	and 	ie_erro = 125 
	and 	ie_suspenso = 'N';

end if;

ie_erro_p		:= ie_erro_w;
qt_material_p	:= qt_material_w;

commit;

end consistir_todas_prescr;
/