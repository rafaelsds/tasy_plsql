create or replace
procedure gerar_cobranca_hsbc_240_reg(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2)  is

ds_conteudo_w			varchar2(240);

/* Header do arquivo */
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(30);
cd_cgc_w			varchar2(14);
cd_agencia_bancaria_w		varchar2(6);
dt_remessa_retorno_w		date;
cd_conta_w			varchar2(13);
cd_carteira_w			varchar2(10);

/* Detalhe */
nm_pessoa_w			varchar2(40);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(15);
ds_cidade_w			varchar2(15);
nr_inscricao_w			varchar2(15);
ds_nosso_numero_w		varchar2(15);
vl_titulo_w			varchar2(15);
vl_desconto_w			varchar2(15);
vl_juros_mora_w			varchar2(15);
nr_titulo_w			varchar2(16);
cd_cep_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_vencimento_w			varchar2(8);
ds_uf_w				varchar2(2);
ie_tipo_inscricao_w		varchar2(1);
cd_convenio_banco_w		banco_estabelecimento.cd_convenio_banco%type;
nr_seq_carteira_cobr_w	cobranca_escritural.nr_seq_carteira_cobr%type;
dt_emissao_bloqueto_w	titulo_receber.dt_emissao_bloqueto%type;

/* Trailer de Lote */
vl_titulos_cobr_w		varchar2(15);
qt_titulos_cobr_w		number(5);
qt_registro_lote_w		number(4)	:= 0;

/* Trailer do Arquivo */
qt_registro_w			number(5)	:= 0;
qt_registro_P_w			number(6)	:= 0;
qt_registro_Q_w			number(6)	:= 0;

Cursor	C01 is
select	lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0'),
	lpad(82||somente_numero(lpad(b.nr_titulo,8,'0')||'-'|| calcula_digito('Modulo11',(82||lpad(b.nr_titulo,8,'0')))),15,'0') ds_nosso_numero,
	lpad(b.nr_titulo,16,0) nr_titulo,
	to_char(nvl(b.dt_pagamento_previsto,sysdate),'DDMMYYYY') dt_vencimento,
	lpad(replace(to_char(b.vl_titulo, 'fm0000000000000.00'),'.',''),15,'0') vl_titulo,
	to_char(nvl(b.dt_emissao,sysdate),'DDMMYYYY') dt_emissao,
	lpad(replace(to_char(b.vl_titulo * b.tx_juros / 100 / 30, 'fm0000000000000.00'),'.',''),15,'0') vl_juros_mora,
	lpad(replace(to_char(b.TX_DESC_ANTECIPACAO, 'fm0000000000000.00'),'.',''),15,'0') vl_desconto,
	nvl(decode(b.cd_pessoa_fisica, null, '2', '1'),'0') ie_tipo_inscricao,
	lpad(decode(decode(b.cd_pessoa_fisica, null, 2, 1),2,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,(substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,9) || '0000' || substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),10,2)),'000000000000000'),15,'0') nr_inscricao,
	rpad(substr(nvl(elimina_caractere_especial(obter_nome_pf_pj(b.cd_pessoa_fisica, b.cd_cgc)),' '),1,40),40,' ') nm_pessoa,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'E')),' '),1,38),38,' ') ds_endereco,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'B')),' '),1,15),15,' ') ds_bairro,
	lpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),' '),1,8),8,'0') cd_cep,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),' '),1,15),15,' ') ds_cidade,
	rpad(substr(nvl(elimina_caractere_especial(pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),' '),1,2),2,' ') ds_uf,
	to_char(nvl(dt_emissao_bloqueto, sysdate),'DDMMYYYY') dt_emissao_bloqueto
from	pls_mensalidade d,
	titulo_receber b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo		= b.nr_titulo
and	d.nr_sequencia(+)	= b.nr_seq_mensalidade
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* Header do Arquivo */

select	lpad(b.cd_cgc,14,'0'),
	lpad(nvl(c.cd_agencia_bancaria,'0'),6,'0') cd_agencia_bancaria,
	lpad(nvl(c.cd_conta,'0'),12,'0') || lpad(nvl(c.ie_digito_conta,'0'),1,'0') cd_conta,
	rpad(substr(nvl(elimina_caractere_especial(obter_razao_social(b.cd_cgc)),' '),1,30),30,' ') nm_empresa,
	rpad('BANCO HSBC SA', 30, ' ') nm_banco,
	nvl(a.dt_remessa_retorno,sysdate),
	substr(nvl(c.cd_carteira,'0'),1,1) cd_carteira,
	c.cd_convenio_banco,
	a.nr_seq_carteira_cobr
into	cd_cgc_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	nm_empresa_w,
	nm_banco_w,
	dt_remessa_retorno_w,
	cd_carteira_w,
	cd_convenio_banco_w,
	nr_seq_carteira_cobr_w
from banco_estabelecimento c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:= 	'399' ||
			'0000' || 
			'0' || 
			rpad(' ',9,' ') ||
			'2' || -- TIPO DE INSCRI��O DA EMPRESA
			cd_cgc_w || -- N� DE INSCRI��O DA EMPRESA
			'COB'|| --Verificar 20
			'CNAB' || 
			lpad(nvl(nr_seq_carteira_cobr_w,cd_convenio_banco_w),13,'0') || -- CODIGO CONTRATO COB/DESC 13		
			cd_agencia_bancaria_w ||
			cd_conta_w ||
			'0' ||
			nm_empresa_w || -- NOME DA EMPRESA
			nm_banco_w || -- NOME DO BANCO 
			rpad(' ',10,' ') || -- USO EXCLUSIVO FEBRABAN/CNAB
			'1' || -- C�DIGO REMESSA / RETORNO --- fixo retorno 
			lpad(to_char(dt_remessa_retorno_w,'DDMMYYYY'),8,'0') ||
			lpad(to_char(dt_remessa_retorno_w,'hh24miss'),6,'0') ||
			'000000' ||
			'010' || --N� DA VERS�O DO LAYOUT ARQUIVO
			'00000' || -- DENSIDADE DE GRAVA��O DO ARQUIVO
			'S' || --ENVIO PARA CART COBRAN�A SIMPLES
			rpad(' ',11,' ') || -- NUM CONTRATO LIMITE 
			'S' || -- LIBERACAO AUTOMATICA OPER.DESC.
			rpad(' ',7,' ') || -- PARA USO RESERVADO DO BANCO
			rpad(' ',20,' ') || -- PARA USO RESERVADO DA EMPRESA
			rpad(' ',29,' '); --USO EXCLUSIVO FEBRABAN/CNAB
					

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);

/*	Header do Lote	*/

qt_registro_lote_w	:= qt_registro_lote_w + 1;
qt_registro_w		:= qt_registro_w + 1;

ds_conteudo_w		:= 	'399' || -- C�DIGO DO BANCO NA COMPENSA��O
				lpad(qt_registro_lote_w,4,'0') || -- LOTE DE SERVI�O
				'1' || -- REGISTRO HEADER DE LOTE
				'T' || -- TIPO DE OPERA��O
				'01' || --TIPO DE SERVI�O
				'00' || --FORMA DE LAN�AMENTO
				'010' || --N� DA VERS�O DO LAYOUT DO LOTE
				' ' || --USO EXCLUSIVO FEBRABAN/CNAB
				'2' || --TIPO DE INSCRI��O DA EMPRESA
				lpad(cd_cgc_w,15,'0') || --N� DE INSCRI��O DA EMPRESA
				'COB' || -- C�DIGO DO APLICATIVO NO BANCO
				rpad(' ',4,' ') ||
				'0000000000000' ||
				cd_agencia_bancaria_w || -- AG�NCIA MANTENEDORA DA CONTA
				cd_conta_w || --N�MERO DA CONTA CORRENTE
				'0' || -- D�GITO VERIFICADOR DA AG/CONTA
				nm_empresa_w || -- NOME DA EMPRESA
				rpad(' ',80,' ') || --MENSAGEM 1 e 2
				'00000001' || --N�MERO REMESSA/RETORNO
				to_char(dt_remessa_retorno_w,'DDMMYYYY') || --DATA DE GRAVA��O REMESSA/RETORNO
				to_char(dt_remessa_retorno_w,'DDMMYYYY') || --DATA DO CR�DITO
				rpad(' ',33,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);
				
/* Segmento P, SegmentoQ  */

open	C01;
loop
fetch	C01 into
	cd_agencia_bancaria_w,
	ds_nosso_numero_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_mora_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w,
	dt_emissao_bloqueto_w;
exit	when C01%notfound;

	/* Segmento P */

	qt_registro_w		:= qt_registro_w + 1;
	qt_registro_P_w		:= qt_registro_P_w + 1;

	ds_conteudo_w		:=	'399' || -- C�DIGO DO BANCO NA COMPENSA��O
					lpad(qt_registro_lote_w,4,'0') || --LOTE DE SERVI�O
					'3' || --TIPO DE REGISTRO
					lpad(qt_registro_P_w,5,'0') || --N� SEQUENCIAL DO REGISTRO NO LOTE
					'P' || --C�D. SEGMENTO DO REGISTRO DETALHE
					' ' || --USO EXCLUSIVO DA FEBRABAN/CNAB
					'01' || --C�DIGO DE MOVIMENTO
					cd_agencia_bancaria_w || --AG�NCIA MANTENEDORA DA CONTA 6
					cd_conta_w || --N�MERO DA CONTA CORRENTE 13
					'0' || 
					nr_titulo_w ||--IDENTIFICA��O DO BOLETO NO BANCO
					rpad(' ',4,' ') ||
					cd_carteira_w || --C�DIGO DA CARTEIRA
					'1' || --COM CADASTRAMENTO
					'2' || --ESCRITURAL
					'9' || --Banco Emite (Boleto Auto-Envelopado)
					'1' || --BANCO DISTRIBUI
					rpad(' ',15,' ') || --N�MERO DO DOCUMENTO DE COBRAN�A
					dt_vencimento_w || --DATA DO VENCIMENTO DO BOLETO
					vl_titulo_w || --VALOR NOMINAL DO BOLETO
					cd_agencia_bancaria_w || --AG�NCIA COBRADORA/RECEBEDORA 6
					'04' || --ESP�CIE DO BOLETO
					'A' ||
					dt_emissao_bloqueto_w||
					'1'||
					to_char(sysdate,'DDMMYYYY')||
					vl_juros_mora_w|| -- 15
					'1'||
					to_char(sysdate,'DDMMYYYY') ||
					vl_desconto_w || --15
					'000000000000000' || --15
					vl_titulo_w|| --15
					rpad(' ',25,' ') || --IDENTIF. DO BOLETO NA EMPRESA
					'3' || --N�o protestar
					'00' || --N�MERO DE DIAS PARA PROTESTO E SERASA
					'1' || --BAIXAR/DEVOLVER
					'000' || 
					'09' || --C�DIGO DA MOEDA
					'0000000000' || --N. DO CONTR. DA OPERA��O DE CR�D. 10
					' '; --USO EXCLUSIVO FEBRABAN/CNAB

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval);

	/* Segmento Q */

	qt_registro_w		:= qt_registro_w + 1;
	qt_registro_P_w		:= qt_registro_P_w + 1;
	
	ds_conteudo_w		:=	'399' || --C�DIGO DO BANCO NA COMPENSA��O
					lpad(qt_registro_lote_w, 4, '0') || --LOTE DE SERVI�O
					'3' || --REGISTRO DETALHE
					lpad(qt_registro_P_w, 5, '0') || --N� SEQUENCIAL DO REGISTRO NO LOTE
					'Q' || --C�D. SEGMENTO DO REGISTRO DETALHE
					' ' || --USO EXCLUSIVO DA FEBRABAN/CNAB
					'06' || --C�DIGO DE MOVIMENTO
					ie_tipo_inscricao_w ||
					nr_inscricao_w||
					nm_pessoa_w||
					ds_endereco_w||
					'  '||
					ds_bairro_w||
					cd_cep_w||
					ds_cidade_w||
					ds_uf_w||
					ie_tipo_inscricao_w||
					nr_inscricao_w||
					rpad(' ',71,' ');


	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval);

end	loop;
close	C01;

/*	Trailer de Lote	*/

select	lpad(count(1),6,'0'),
	replace(to_char(sum(c.vl_titulo), 'fm0000000000000.00'),'.','')
into	qt_titulos_cobr_w,
	vl_titulos_cobr_w
from	titulo_receber c,
	titulo_receber_cobr b,
	cobranca_escritural a
where	b.nr_titulo	= c.nr_titulo
and	a.nr_sequencia	= b.nr_seq_cobranca
and	a.nr_sequencia	= nr_seq_cobr_escrit_p;

qt_registro_lote_w	:= qt_registro_lote_w + 1;

ds_conteudo_w		:=	'399' ||
				'0001' || 
				'5' ||
				rpad(' ',9,' ') ||
				lpad(qt_registro_w,6,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				'000000' ||
				'00000000000000000' ||
				'000000' ||
				'00000000000000000' ||
				'000000' ||
				'00000000000000000' ||
				rpad(' ',8,' ') ||
				rpad(' ',117,' ');

qt_registro_w		:= qt_registro_w + 1;

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);

/* Trailer do Arquivo */

qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:=	'399' ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			'000001' ||
			lpad(qt_registro_w,6,'0') ||
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval);

end gerar_cobranca_hsbc_240_reg;
/

