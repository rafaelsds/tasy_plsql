create or replace procedure calcular_diaria
			(nm_usuario_p      	varchar2,
                        dt_parametro_p    	date) is
nr_atendimento_w				Number(10);
cd_estabelecimento_w			Number(4);
dt_entrada_w				Date;
ie_tipo_convenio_w			Number(2);
ie_tipo_atendimento_w			Number(3);
cd_proc_longa_perm_w			Number(15);
qt_dia_longa_perm_w			Number(2);

qt_perm_sus_calc_w			number(3);
qt_perm_real_calc_w			number(3);
qt_longa_perm_calc_w			number(3);
nr_aih_calc_w				number(13);
qt_dispensa_w				number(15,0);

qt_dispensa_periodo_w			number(10,0);
cd_convenio_atend_w			number(5,0);
hr_inicio_w				date;
hr_final_w				date;
ie_dispensa_diaria_w			varchar2(1):='N';
qt_disp_w				number(10,0);
qt_regra_evento_w			number(10,0);

cursor c00 is
      select 	e.nr_atendimento,
      	     	e.dt_entrada,
		e.cd_estabelecimento,
		nvl(e.ie_tipo_convenio,0),
		e.ie_tipo_atendimento
	from 	atendimento_paciente e
       where 	e.dt_entrada    		<= dt_parametro_p
         and 	e.dt_fim_conta 		is null
	 and 	e.dt_cancelamento 	is null
	 and 	nvl(e.dt_alta,to_date('2999','yyyy')) > dt_parametro_p
	 AND 	e.dt_alta_interno BETWEEN dt_parametro_p AND TO_DATE('30/12/2999 23:59:59','dd/mm/yyyy hh24:mi:ss');

Cursor C01 is
	select	hr_inicio,
		hr_final
	from	dispensa_diaria_periodo
	where	ie_situacao = 'A'
	and 	nvl(cd_convenio, nvl(cd_convenio_atend_w,0)) = nvl(cd_convenio_atend_w,0)
	and 	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	order by nvl(cd_convenio,0),
		nvl(cd_estabelecimento,0);

BEGIN

/* Calculo de diarias */
OPEN C00;
LOOP
   	FETCH C00 into
          	nr_atendimento_w,
		dt_entrada_w,
		cd_estabelecimento_w,
		ie_tipo_convenio_w,
		ie_tipo_atendimento_w;
   	exit when c00%notfound;
	BEGIN

	-- Evento Gera��o di�ria de itens por atendimento
	-- Retirado esse tratamento e criado a JOB GERACAO_DIARIA_ITENS_ATEND_J. OS 658750
	/*select	count(*)
	into	qt_regra_evento_w
	from 	regra_lanc_automatico
	where 	nr_seq_evento = 544
	and 	ie_situacao = 'A';

	if	(qt_regra_evento_w > 0) then
		Gerar_lancamento_automatico(nr_atendimento_w, null, 544, nm_usuario_p, null, null,null,null,null,null);
	end if;*/
	ie_dispensa_diaria_w:= 'N';

	select 	count(*)
	into	qt_dispensa_periodo_w
	from 	dispensa_diaria_periodo
	where 	ie_situacao = 'A';

	if	(qt_dispensa_periodo_w > 0) then

		select 	obter_convenio_atendimento(nr_atendimento_w)
		into	cd_convenio_atend_w
		from 	dual;

		open C01;
		loop
		fetch C01 into
			hr_inicio_w,
			hr_final_w;
		exit when C01%notfound;
			begin
			hr_inicio_w	:= hr_inicio_w;
			hr_final_w	:= hr_final_w;
			end;
		end loop;
		close C01;

		select 	count(*)
		into	qt_disp_w
		from 	dual
		where 	dt_parametro_p between to_date((to_char(dt_parametro_p, 'dd/mm/yyyy') || to_char(hr_inicio_w, 'hh24:mi:ss')), 'dd/mm/yyyy hh24:mi:ss') and
						to_date((to_char(dt_parametro_p, 'dd/mm/yyyy') || to_char(hr_final_w, 'hh24:mi:ss')), 'dd/mm/yyyy hh24:mi:ss');

		if	(qt_disp_w > 0) then
			ie_dispensa_diaria_w:= 'S';
		end if;

	end if;

	select	count(*)
	into	qt_dispensa_w
	from	DISPENSA_COB_DIARIA
	where	nr_atendimento		= nr_atendimento_w
	and	dt_liberacao		is not null
	and	dt_parametro_p between dt_inicio and dt_fim;

	if	(qt_dispensa_w 	= 0) and (ie_dispensa_diaria_w = 'N') then
		begin

		if	(ie_tipo_convenio_w	<> 3) and
			(ie_tipo_convenio_w	<> 10) then
			begin
			CALCULAR_DIARIA_ATENDIMENTO
					(cd_estabelecimento_w,
					nr_atendimento_w,
					dt_entrada_w,
					dt_parametro_p,
					nm_usuario_p,'S','S','D');
			end;
		else
			begin
			Gerar_lancamento_automatico(
				nr_atendimento_w,
				null,
				39,
				nm_usuario_p,
				null, null,null,dt_parametro_p,null,null);
			/*Adicionada a data para poder verificar os acompanhantes,
			sem esta informa��o n�o � poss�vel verificar se tem acompanhante
			momento da execu��o da JOB */
			end;
		end if;

		if	(ie_tipo_convenio_w	in (3,10)) 	and
			(ie_tipo_atendimento_w	= 1) 	then
			begin
			qt_dia_longa_perm_w := 0;
			calcular_longa_permanencia (	nr_atendimento_w,
							0,
							cd_proc_longa_perm_w,
							qt_perm_sus_calc_w,
							qt_perm_real_calc_w,
							qt_longa_perm_calc_w,
							nr_aih_calc_w);
			if	(cd_proc_longa_perm_w is not null)	then
				qt_dia_longa_perm_w	:= substr(cd_proc_longa_perm_w,6,2);
			end if;

			update	atendimento_paciente
			set		qt_dia_longa_perm	= qt_dia_longa_perm_w
			where		nr_atendimento	= nr_atendimento_w;
			exception
					when others then
					qt_dia_longa_perm_w := 0;

			end;
		end if;
		end;
	end if;

	GERACAO_DIARIA_ITENS_CONTA(nr_atendimento_w);
	
	END;
END LOOP;
CLOSE C00;

commit;
END Calcular_Diaria;
/