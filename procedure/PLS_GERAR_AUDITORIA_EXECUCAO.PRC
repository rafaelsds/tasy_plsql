create or replace
procedure pls_gerar_auditoria_execucao
			(	nr_seq_execucao_p	number,
				nm_usuario_p		Varchar2,
				ie_origem_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar auditoria na execu��o da requisi��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
				
/*
IE_ORIGEM_P 
  'A' - Automatico ( gerar a auditoria quando  for consistido uma requisi��o)
  'M' - Manual ( gerar a auditoria quando a requisi��o s� encaminhada para a auditoria manualmente)
*/
				
nr_seq_requisicao_w		Number(10);
nr_seq_segurado_w		Number(10);
ie_tipo_guia_w			Number(10);
dt_requisicao_w			Date;
cd_medico_solicitante_w		Varchar2(10); 
nr_seq_auditoria_w		Number(10);
nr_seq_exec_proc_w		Number(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
qt_solicitado_w			Number(10);
ie_status_w			Varchar2(2);
ie_status_item_w		Varchar2(2);
nr_seq_auditoria_item_w		Number(10);
nr_seq_material_w		Number(10);
qt_material_w			Number(10);
nr_seq_exec_mat_w		Number(10);
nr_seq_prestador_w		Number(10);
ie_tipo_despesa_w		varchar2(10);
nr_seq_grupo_w			Number(10);
nr_seq_fluxo_w			Number(10);
qt_grupo_w			Number(10);
qt_fluxo_w			Number(10);
nr_seq_ordem_atual_w		Number(10);
qt_auditoria_w			Number(10);
cd_estabelecimento_w		number(10);
ie_utiliza_nivel_w		varchar2(1);
ie_regra_preco_w		varchar2(3);
nr_seq_regra_preco_pac_w	number(10);
nr_seq_pacote_w			number(10);
nr_seq_ocorrencia_w		number(10);
ie_auditoria_w			varchar2(1);
nr_seq_motivo_glosa_w		number(10);
ie_agrupa_grupo_aud_w		pls_param_analise_aut.ie_agrupa_grupo_aud%type;
ie_recem_nascido_w		pls_requisicao.ie_recem_nascido%type;
nm_recem_nascido_w		pls_requisicao.nm_recem_nascido%type;
dt_nasc_recem_nascido_w		pls_requisicao.dt_nasc_recem_nascido%type;
ie_excluir_proc_pacote_w	pls_pacote_tipo_acomodacao.ie_excluir_proc_pacote%type;
qt_proc_composicao_w		number(10);
ie_tipo_anexo_w			pls_execucao_req_item.ie_tipo_anexo%type;
ie_tipo_processo_w		pls_requisicao.ie_tipo_processo%type;

Cursor C01 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_item,
		ie_situacao,
		ie_tipo_anexo
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	cd_procedimento		is not null
	and	ie_origem_proced	is not null
	and	ie_situacao		<> 'C';

Cursor C02 is
	select	nr_sequencia,
		nr_seq_material,
		qt_item,
		ie_situacao,
		ie_tipo_anexo
	from	pls_execucao_req_item
	where	nr_seq_execucao	= nr_seq_execucao_p
	and	nr_seq_material	is not null
	and	ie_situacao	<> 'C';
	
Cursor C03 is
	select	nr_seq_grupo,
		nr_seq_fluxo,
		nr_seq_ocorrencia
	from	pls_ocorrencia_grupo
	where	nr_seq_ocorrencia in (	select	nr_seq_ocorrencia
					from	pls_ocorrencia_benef
					where	nr_seq_execucao	= nr_seq_execucao_p)
	and	ie_requisicao	= 'S'
	group by nr_seq_fluxo, nr_seq_grupo, nr_seq_ocorrencia
	order by nr_seq_fluxo;

begin

select	count(1)
into	qt_auditoria_w
from	pls_auditoria
where	nr_seq_execucao = nr_seq_execucao_p;

if	(qt_auditoria_w = 0) then
	select	nr_seq_requisicao,
		nr_seq_prestador
	into	nr_seq_requisicao_w,
		nr_seq_prestador_w
	from	pls_execucao_requisicao
	where	nr_sequencia = nr_seq_execucao_p;

	select	nr_seq_segurado,
		ie_tipo_guia,
		dt_requisicao,
		cd_medico_solicitante,
		cd_estabelecimento,
		ie_recem_nascido, 
		nm_recem_nascido,
		dt_nasc_recem_nascido,
		ie_tipo_processo
	into	nr_seq_segurado_w,
		ie_tipo_guia_w,
		dt_requisicao_w,
		cd_medico_solicitante_w,
		cd_estabelecimento_w,
		ie_recem_nascido_w, 
		nm_recem_nascido_w,
		dt_nasc_recem_nascido_w,
		ie_tipo_processo_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_w;
	
	select 	pls_auditoria_seq.nextval
	into	nr_seq_auditoria_w
	from	dual;

	insert into pls_auditoria
		(nr_sequencia, dt_auditoria, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_guia, nr_seq_requisicao, nr_seq_execucao,
		ie_tipo_auditoria, nr_seq_segurado, ie_tipo_guia, 
		dt_solicitacao, cd_medico_solicitante, ie_status,
		nr_seq_prestador, cd_estabelecimento, ie_recem_nascido, 
		nm_recem_nascido, dt_nasc_recem_nascido, ie_retorno_justific, ie_tipo_processo)
	values(	nr_seq_auditoria_w, sysdate, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		null, null, nr_seq_execucao_p,
		'E', nr_seq_segurado_w, ie_tipo_guia_w, 
		dt_requisicao_w, cd_medico_solicitante_w,'A',
		nr_seq_prestador_w, cd_estabelecimento_w, ie_recem_nascido_w, 
		nm_recem_nascido_w, dt_nasc_recem_nascido_w, 'N', ie_tipo_processo_w);

	update	pls_execucao_requisicao
	set	ie_situacao	= 2
	where	nr_sequencia	= nr_seq_execucao_p
	and	ie_situacao	<> 1;

	open C01;
	loop
	fetch C01 into	
		nr_seq_exec_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_solicitado_w,
		ie_status_w,
		ie_tipo_anexo_w;
	exit when C01%notfound;
		begin
		ie_excluir_proc_pacote_w	:= 'N';

		select	pls_auditoria_item_seq.nextval
		into	nr_seq_auditoria_item_w
		from	dual;
		
		if	(ie_status_w = 'S') then
			ie_status_item_w := 'A';
		elsif	(ie_status_w = 'A') then
			ie_status_item_w := 'P';
		else
			ie_status_item_w := ie_status_w;
		end if;

		begin
			select 	max(ie_classificacao)
			into	ie_tipo_despesa_w
			from	procedimento
			where	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w;
		exception
		when others then
			ie_tipo_despesa_w	:= 1;
		end;

		if	(nr_seq_prestador_w	> 0) then
			select	max(nr_sequencia)
			into	nr_seq_pacote_w
			from	pls_pacote
			where	nr_seq_prestador	= nr_seq_prestador_w
			and	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w
			and	ie_situacao		= 'A';
			
			-- Caso n�o exista um pacote cadastrado na fun��o OPS - Prestadores / Prestadores / Pacote, deve procurar pacotes sem a restri��o por prestador
			if	(nr_seq_pacote_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_pacote_w
				from	pls_pacote
				where	cd_procedimento		= cd_procedimento_w
				and	ie_origem_proced	= ie_origem_proced_w
				and	ie_situacao		= 'A';
			end if;

			/* Francisco - 16/05/2012 - OS 447352 */
			if	(nr_seq_pacote_w is not null) then
				select	nvl(ie_regra_preco,'N')
				into	ie_regra_preco_w
				from	pls_pacote a
				where	a.nr_sequencia = nr_seq_pacote_w;
				
				if	(ie_regra_preco_w = 'S') then
					pls_obter_regra_preco_pacote(cd_procedimento_w,
								ie_origem_proced_w,
								'E',
								nr_seq_exec_proc_w,
								nm_usuario_p,
								nr_seq_pacote_w,
								nr_seq_regra_preco_pac_w);
				end if;
			end if;

			if	(nr_seq_pacote_w is not null) then			
				ie_tipo_despesa_w	:= '4';
				
				-- Verificar se deve excluir o procedimento do pacote na execu��o da requisi��o
				begin
					select	ie_excluir_proc_pacote
					into	ie_excluir_proc_pacote_w
					from	pls_pacote_tipo_acomodacao
					where	nr_sequencia	= nr_seq_regra_preco_pac_w;
				exception
				when others then
					ie_excluir_proc_pacote_w	:= 'N';
				end;
			end if;
		end if;

		-- Verificar se o procedimento est� na composi��o do pacote que foi exclu�do, caso esteja, este procedimento deve ser colocado em an�lise.
		select	count(1)
		into	qt_proc_composicao_w
		from	pls_pacote_tipo_acomodacao a,
			pls_pacote_composicao b,
			pls_pacote_procedimento c
		where	a.nr_sequencia		= nr_seq_regra_preco_pac_w
		and	a.nr_seq_composicao	= b.nr_sequencia
		and	c.nr_seq_composicao	= b.nr_sequencia
		and	c.cd_procedimento	= cd_procedimento_w
		and	c.ie_origem_proced	= ie_origem_proced_w
		and	c.ie_situacao		= 'A'
		and	b.ie_situacao		= 'A'
		and	sysdate between (nvl(b.dt_inicio_vigencia,sysdate)) and (nvl(b.dt_fim_vigencia,sysdate));
		
		if	(qt_proc_composicao_w > 0) then
			ie_status_item_w	:= 'P';
			
			update	pls_execucao_req_item
			set	ie_situacao	= 'A'
			where	nr_sequencia	= nr_seq_exec_proc_w;
		end if;

		-- Se na regra de pre�o do pacote estiver marcado para excluir o procedimento do pacote, ent�o n�o pode ser inclu�do este procedimento na an�lise.
		if	(nvl(ie_excluir_proc_pacote_w,'N')	= 'N') then
			insert into pls_auditoria_item
				(nr_sequencia, nr_seq_auditoria, cd_procedimento,
				ie_origem_proced, qt_original, nr_seq_proc_origem,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
				nm_usuario_nrec, qt_ajuste, ie_status_solicitacao,
				ie_status, ie_tipo_despesa)
			values	(nr_seq_auditoria_item_w, nr_seq_auditoria_w, cd_procedimento_w,
				ie_origem_proced_w, qt_solicitado_w, nr_seq_exec_proc_w,
				sysdate, nm_usuario_p, sysdate,
				nm_usuario_p, qt_solicitado_w, ie_status_item_w,
				ie_status_item_w, ie_tipo_despesa_w);
		end if;
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into
		nr_seq_exec_mat_w,
		nr_seq_material_w,
		qt_material_w,
		ie_status_w,
		ie_tipo_anexo_w;
	exit when C02%notfound;
		begin
		select	pls_auditoria_item_seq.nextval
		into	nr_seq_auditoria_item_w
		from	dual;
		
		if	(ie_status_w = 'S') then
			ie_status_item_w := 'A';
		elsif	(ie_status_w = 'A') then
			ie_status_item_w := 'P';
		else
			ie_status_item_w := ie_status_w;
		end if;

		if	(nr_seq_regra_preco_pac_w is not null) then
			-- Verificar se o procedimento est� na composi��o do pacote que foi exclu�do, caso esteja, este procedimento deve ser colocado em an�lise.
			select	count(1)
			into	qt_proc_composicao_w
			from	pls_pacote_tipo_acomodacao a,
				pls_pacote_composicao b,
				pls_pacote_material c
			where	a.nr_sequencia		= nr_seq_regra_preco_pac_w
			and	a.nr_seq_composicao	= b.nr_sequencia
			and	c.nr_seq_composicao	= b.nr_sequencia
			and	c.nr_seq_material	= nr_seq_material_w
			and	c.ie_situacao		= 'A'
			and	b.ie_situacao		= 'A'
			and	sysdate between (nvl(b.dt_inicio_vigencia,sysdate)) and (nvl(b.dt_fim_vigencia,sysdate));
			
			if	(qt_proc_composicao_w > 0) then
				ie_status_item_w	:= 'P';
				
				update	pls_execucao_req_item
				set	ie_situacao	= 'A'
				where	nr_sequencia	= nr_seq_exec_mat_w;
			end if;
		end if;
		
		select 	max(ie_tipo_despesa)
		into	ie_tipo_despesa_w
		from	pls_material
		where	nr_sequencia = nr_seq_material_w;
		
		insert into pls_auditoria_item
			(nr_sequencia, nr_seq_auditoria, qt_original,
			nr_seq_mat_origem, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, qt_ajuste,
			nr_seq_material, ie_status_solicitacao, ie_status,
			ie_tipo_despesa, ie_tipo_anexo)
		values(	nr_seq_auditoria_item_w, nr_seq_auditoria_w, qt_material_w,
			nr_seq_exec_mat_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, qt_material_w,
			nr_seq_material_w, ie_status_item_w, ie_status_item_w,
			ie_tipo_despesa_w, ie_tipo_anexo_w);
		end;
	end loop;
	close C02;
	
	if	(ie_origem_p = 'A') then
		begin
			select	nvl(ie_agrupa_grupo_aud, 'S')
			into	ie_agrupa_grupo_aud_w
			from	pls_param_analise_aut;		
		exception
		when others then
			ie_agrupa_grupo_aud_w	:= 'S';
		end;

		open C03;
		loop
		fetch C03 into	
			nr_seq_grupo_w,
			nr_seq_fluxo_w,
			nr_seq_ocorrencia_w;
		exit when C03%notfound;
			begin
			
			begin
				select	ie_auditoria,
					nr_seq_motivo_glosa
				into	ie_auditoria_w,
					nr_seq_motivo_glosa_w
				from	pls_ocorrencia
				where	nr_sequencia	= nr_seq_ocorrencia_w;
			exception
			when others then
				ie_auditoria_w	:= 'N';
			end;
			
			if	((ie_auditoria_w	= 'S') or ((ie_auditoria_w	= 'N')	and (nr_seq_motivo_glosa_w	is not null))) then
				if	(ie_agrupa_grupo_aud_w <> 'N') then
					select	count(1)
					into	qt_grupo_w
					from	pls_auditoria_grupo
					where	nr_seq_auditoria 	= nr_seq_auditoria_w
					and	nr_seq_grupo 		= nr_seq_grupo_w;
					
					if	(qt_grupo_w = 0) then
						select	count(1)
						into	qt_fluxo_w
						from	pls_auditoria_grupo
						where	nr_seq_auditoria 	= nr_seq_auditoria_w
						and	nr_seq_ordem 		= nr_seq_fluxo_w;
						
						if	(qt_fluxo_w	> 0) then
							select	max(nr_seq_ordem) + 1
							into	nr_seq_ordem_atual_w
							from	pls_auditoria_grupo
							where	nr_seq_auditoria 	= nr_seq_auditoria_w;
							
							nr_seq_fluxo_w	:= nr_seq_ordem_atual_w;
						end if;
						
						insert into pls_auditoria_grupo
							(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
							dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
							nm_usuario_nrec, nr_seq_ordem, ie_status,
							ie_manual)
						values	(pls_auditoria_grupo_seq.nextval, nr_seq_auditoria_w, nr_seq_grupo_w,
							sysdate, nm_usuario_p, sysdate,
							nm_usuario_p, nr_seq_fluxo_w, 'U',
							'N');
					end if;
				elsif	(ie_agrupa_grupo_aud_w = 'N') then
					select	count(1)
					into	qt_fluxo_w
					from	pls_auditoria_grupo
					where	nr_seq_auditoria 	= nr_seq_auditoria_w
					and	nr_seq_ordem 		= nr_seq_fluxo_w;
					
					if	(qt_fluxo_w	> 0) then
						select	max(nr_seq_ordem) + 1
						into	nr_seq_ordem_atual_w
						from	pls_auditoria_grupo
						where	nr_seq_auditoria 	= nr_seq_auditoria_w;
						
						nr_seq_fluxo_w	:= nr_seq_ordem_atual_w;
					end if;
					
					insert into pls_auditoria_grupo
						(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, nr_seq_ordem, ie_status,
						ie_manual)
					values	(pls_auditoria_grupo_seq.nextval, nr_seq_auditoria_w, nr_seq_grupo_w,
						sysdate, nm_usuario_p, sysdate,
						nm_usuario_p, nr_seq_fluxo_w, 'U',
						'N');
				end if;
			end if;
			end;
		end loop;
		close C03;
	end if;
	
	/* OS - 421626 - Rotina para gerar as ocorr�ncias e glosas na tabela "pls_analise_ocor_glosa_aut" */
	ie_utiliza_nivel_w := pls_obter_se_uti_nivel_lib_aut(cd_estabelecimento_w); 
	if	(ie_utiliza_nivel_w = 'S') then
		pls_gerar_ocorr_glosa_aud_exec(nr_seq_auditoria_w,nm_usuario_p);
	end if;
	
end if;

commit;

end pls_gerar_auditoria_execucao;
/
