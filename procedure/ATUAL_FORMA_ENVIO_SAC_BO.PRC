create or replace
procedure atual_forma_envio_sac_bo(	nr_sequencia_p		number,
				ie_forma_envio_p		varchar2) is 

begin

if	(nr_sequencia_p is not null) and
	(ie_forma_envio_p is not null) then
	begin	
	update	sac_boletim_ocorrencia
	set	ie_forma_envio = ie_forma_envio_p
	where	nr_sequencia = nr_sequencia_p;
	commit;
	
	end;
end if;

end atual_forma_envio_sac_bo;
/