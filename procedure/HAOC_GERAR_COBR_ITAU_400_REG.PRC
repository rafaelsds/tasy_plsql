create or replace
procedure HAOC_GERAR_COBR_ITAU_400_REG(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2,
					nr_seq_grupo_inst_p		number) is 
					
/*===========================================================
	             =>>>>>	A T E N � � O        <<<<<<=

Esta procedure � uma c�pia da GERAR_COBRANCA_ITAU_400, 
por�m sem as tratativas de mensagem, para atender a OS 1175672,
layout anexo nesta OS.

Como se trata de um projeto e n�o possu�mos cliente para validar junto 
ao banco, os defeitos devem ser verificados com o analista (Peterson) antes
de serem documentados. 
============================================================*/						
	
ds_conteudo_w			varchar2(400)	:= null;
nr_seq_reg_arquivo_w		number(10)	:= 1;
cd_conta_cobr_w			varchar2(5);
cd_agencia_cobr_w		varchar2(4);
nm_empresa_w			varchar2(30);
dt_geracao_w			varchar2(6);
ie_digito_conta_cobr_w		varchar2(1);
cd_banco_w			number(3);
nr_seq_carteira_cobr_w		number(10);
nr_carteira_w			varchar2(3);

/* detalhe */
ie_tipo_inscricao_w			varchar2(2);
nr_inscricao_empresa_w		varchar2(14);
cd_agencia_bancaria_w		varchar2(4);
cd_conta_w			varchar2(6);
nr_nosso_numero_w		varchar2(8);
vl_multa_w			number(15,2);
nr_titulo_w			number(10);
dt_vencimento_w			date;
vl_titulo_w			number(15,2);
dt_emissao_w			date;
vl_juros_w			number(15,2);
vl_desconto_w			number(15,2);
nm_pessoa_w			varchar2(30);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(12);
cd_cep_w			varchar2(8);
ds_cidade_w			varchar2(15);
sg_estado_w			varchar2(15);
tx_juros_w			number(7,4);
tx_multa_w			number(7,4);
ds_tipo_juros_w			varchar2(255);
ds_tipo_multa_w			varchar2(255);
ie_digito_nosso_num_w		varchar2(1);
nr_telefone_w			varchar2(15);
nr_seq_carteira_cobr_tit_w		titulo_receber.nr_seq_carteira_cobr%type;
cd_flash_w			banco_estabelecimento.cd_flash%type;
nr_inscricao_w			varchar2(14);
cd_especie_titulo_banco_w 		especie_titulos_remessa.cd_especie_titulo_banco%type;

nr_seq_grupo_inst_w		number(10);
cd_ocorrencia_w			titulo_receber_cobr.cd_ocorrencia%type;
cd_conta_lote_w			banco_estabelecimento.cd_conta%type;
cd_instr_w				varchar2(4) := '0505';
ds_instrucao_w 			varchar2(30) := rpad(' ',30,' ');
ds_email_w				varchar2(120);

cursor	c01 is
select	decode(b.cd_pessoa_fisica,null,'02','01') ie_tipo_inscricao,
	nvl(c.nr_cpf,b.cd_cgc) nr_inscricao_empresa,
	lpad(a.cd_agencia_bancaria,4,'0') cd_agencia_bancaria,
	lpad(substr(a.nr_conta,1,5) || substr(a.ie_digito_conta,1,1),6,'0') cd_conta,
	lpad(substr(nvl(b.nr_nosso_numero,'0'),1,8),8,'0') nr_nosso_numero,
	decode(nvl(a.vl_multa,0),0,obter_juros_multa_titulo(b.nr_titulo,sysdate,'R','M'),a.vl_multa),
	a.nr_titulo,
	b.dt_pagamento_previsto,
	b.vl_saldo_titulo,
	b.dt_emissao,
	decode(nvl(a.vl_juros,0),0,obter_juros_multa_titulo(b.nr_titulo,sysdate,'R','J'),a.vl_juros),
	a.vl_desconto,
	substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,30) nm_pessoa,
	rpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'R'),1,40),' '),40,' ') ds_endereco,
	rpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),1,12),' '),12,' ') ds_bairro,
	lpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CEP'),1,8),'0'),8,'0') cd_cep,
	rpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CI'),1,15),' '),15,' ') ds_cidade,
	rpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'UF'),1,2),' '),2,' ') sg_estado,
	b.tx_juros,
	b.tx_multa,
	substr(obter_valor_dominio(707,b.cd_tipo_taxa_juro),1,255) ds_tipo_juros,
	substr(obter_valor_dominio(707,b.cd_tipo_taxa_multa),1,255) ds_tipo_multa,
	b.nr_seq_carteira_cobr,
	obter_especie_titulo_banco(cd_banco_w, b.ie_tipo_titulo) cd_especie_titulo_banco,
	substr(nvl(a.cd_ocorrencia,'01'),1,2) cd_ocorrencia,
	obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'M') ds_email
from	pessoa_fisica c,
	titulo_receber b,
	titulo_receber_cobr a
where	b.cd_pessoa_fisica	= c.cd_pessoa_fisica(+)
and	a.nr_titulo		= b.nr_titulo
and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

begin

delete	w_envio_banco
where	nm_usuario = nm_usuario_p;

if	(nvl(nr_seq_grupo_inst_p,0)	= 0) then

	nr_seq_grupo_inst_w	:= null;

else

	nr_seq_grupo_inst_w	:= nr_seq_grupo_inst_p;

end if;

/* header */
select	lpad(nvl(b.cd_conta,'0'),5,'0'),
	lpad(nvl(b.cd_agencia_bancaria,'0'),4,'0'),
	substr(nvl(b.ie_digito_conta,'0'),1,1),
	rpad(substr(obter_nome_estabelecimento(cd_estabelecimento_p),1,30),30,' ') nm_empresa,
	to_char(sysdate,'DDMMYY'),
	a.nr_seq_carteira_cobr,
	b.cd_banco,
	rpad(nvl(substr(b.cd_flash,1,3),'LWZ'),3,' ') cd_flash,
	c.cd_cgc nr_inscricao,
	b.cd_conta
into	cd_conta_cobr_w,
	cd_agencia_cobr_w,
	ie_digito_conta_cobr_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_carteira_cobr_w,
	cd_banco_w,
	cd_flash_w,
	nr_inscricao_w,
	cd_conta_lote_w
from	estabelecimento c,
	banco_estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= c.cd_estabelecimento
and	a.nr_seq_conta_banco	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

if (cd_conta_lote_w = '40105') then
	cd_instr_w := '0594';
	ds_instrucao_w := rpad('N�O RECEBER AP�S O VENCIMENTO',30,' ');
elsif (cd_conta_lote_w = '28145') then
	cd_instr_w := '0539';
	ds_instrucao_w := rpad(substr(nm_empresa_w,1,30),30,' ');
else
	cd_instr_w := '0505';	
	ds_instrucao_w := rpad('N�O RECEBER AP�S O VENCIMENTO',30,' ');
end if;

select	substr(max(a.cd_carteira),1,3) nr_carteira
into	nr_carteira_w
from	banco_carteira a
where	a.nr_sequencia 	= nr_seq_carteira_cobr_w
and	a.cd_banco	= cd_banco_w;

select	max(b.nr_telefone)
into	nr_telefone_w
from	pessoa_juridica b,
	estabelecimento a
where	a.cd_cgc		= b.cd_cgc
and	a.cd_estabelecimento	= cd_estabelecimento_p;

ds_conteudo_w	:= 	'0' 								|| /*Pos 01*/
					'1' 								|| /*Pos 02 */
					'REMESSA' 							|| /*Pos 03 a 09*/
					'01' 								|| /*Pos 10 a 11*/
					rpad('COBRANCA',15,' ') 			|| /*Pos 12 a 26*/
					cd_agencia_cobr_w 					|| /*Pos 27 a 30*/
					'00' 								|| /*Pos 31 a 32*/
					cd_conta_cobr_w 					|| /*Pos 33 a 37*/
					ie_digito_conta_cobr_w 				|| /*Pos 38*/
					rpad(' ',8,' ') 					|| /*Pos 39 a 46*/
					nm_empresa_w 						|| /*Pos 47 a 76*/
					lpad(cd_banco_w,3,'0') 				|| /*Pos 77 a 79*/
					rpad('BANCO ITAU SA',15,' ') 		|| /*Pos 80 a 94*/
					dt_geracao_w 						|| /*Pos 95 a 100*/
					rpad(' ',294,' ') 					|| /*Pos 101 a 394*/
					lpad(nr_seq_reg_arquivo_w,6,'0');      /*Pos 395 a 400*/

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_reg_arquivo_w);

/* detalhe */
open	C01;
loop
fetch	C01 into	
	ie_tipo_inscricao_w,
	nr_inscricao_empresa_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	nr_nosso_numero_w,
	vl_multa_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_w,
	vl_desconto_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	sg_estado_w,
	tx_juros_w,
	tx_multa_w,
	ds_tipo_juros_w,
	ds_tipo_multa_w,
	nr_seq_carteira_cobr_tit_w,
	cd_especie_titulo_banco_w,
	cd_ocorrencia_w,
	ds_email_w;
exit	when C01%notfound;

	if	(nr_carteira_w	is null) then

		select	substr(max(a.cd_carteira),1,3) nr_carteira
		into	nr_carteira_w
		from	banco_carteira a
		where	a.nr_sequencia 	= nr_seq_carteira_cobr_tit_w;

	end if;
	
	/*
	(3) NOSSO N�MERO 
	Para carteiras com registro: 
	Escriturais: � enviado zerado pela empresa e retornado pelo  Ita�  na confirma��o  do registro, com 
	exce��o da carteira 115 cuja faixa de Nosso N�mero � de livre utiliza��o pelo benefici�rio; */
	if (nr_carteira_w in ('112','104','147','188')) then 
		nr_nosso_numero_w := lpad('0',8,'0'); 
	end if;

	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

	/* Registro Detalhe (obrigat�rio) */
	ds_conteudo_w	:=	'1' 																		|| /*Pos 01*/
						'02' 																		|| /*Pos 02 a 03*/
						lpad(nvl(nr_inscricao_w,'0'),14,'0') 										|| /*Pos 04 a 17*/
						lpad(cd_agencia_bancaria_w,4,'0') 											|| /*Pos 18 a 21*/
						'00' 																		|| /*Pos 22 a 23*/
						lpad(cd_conta_w,6,'0') 														|| /*Pos 24 a 28 e 28 e 29 - Codigo conta e DAC conta*/
						rpad(' ',4,' ')	 															|| /*Pos 30 a 33*/
						'0000' 																		|| /*Pos 34 a 37*/
						lpad(nr_titulo_w,25,'0') 													|| /*Pos 38 a 62*/
						lpad(nr_nosso_numero_w,8,'0') 												|| /*Pos 63 a 70*/
						'0000000000000' 															|| /*Pos 71 a 83*/
						lpad(nvl(nr_carteira_w,'0'),3,'0') 											|| /*Pos 84 a 86*/
						rpad(' ',21,' ') 															|| /*Pos 87 a 107*/
						'I' 																		|| /*Pos 108*/
						lpad(nvl(cd_ocorrencia_w,'0'), 2, '0') /*'01'*/								|| /*Pos 109 a 110*/
						rpad(nvl(substr(nr_titulo_w,1,10),' '),10,' ') 								|| /*Pos 111 a 120*/
						to_char(dt_vencimento_w,'ddmmyy') 											|| /*Pos 121 a 126*/
						lpad(somente_numero(to_char(nvl(vl_titulo_w,0),'99999999990.00')),13,'0') 	|| /*Pos 127 a 139*/
						lpad(cd_banco_w,3,'0') 														|| /*Pos 140 a 142*/
						'00000' 																	|| /*Pos 143 a 147*/
						nvl(lpad(substr(cd_especie_titulo_banco_w,1,2),2,'0'),'99') 				|| /*Pos 148 a 149*/
						'A' 																		|| /*Pos 150*/
						to_char(dt_emissao_w,'ddmmyy') 												|| /*Pos 151 a 156*/
						cd_instr_w 																	|| /*Pos 157 a 158 a 159 a 160*/
						lpad(somente_numero(to_char(nvl(vl_juros_w,0),'99999999990.00')),13,'0') 	|| /*Pos 161 a 173*/
						to_char(dt_vencimento_w,'ddmmyy') 											|| /*Pos 174 a 179*/
						lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'99999999990.00')),13,'0') || /*Pos 180 a 192*/
						lpad('0',26,'0') 															|| /*Pos 193 a 205 a 206 a 218*/
						ie_tipo_inscricao_w 														|| /*Pos 219 a 220*/
						lpad(nvl(nr_inscricao_empresa_w,'0'),14,'0') 								|| /*Pos 221 a 234*/
						rpad(nm_pessoa_w,30,' ')											 		|| /*Pos 235 a 264*/
						lpad(' ',10,' ')															|| /*Pos 265 a 274*/
						ds_endereco_w 																|| /*Pos 275 a 314*/
						ds_bairro_w 																|| /*Pos 315 a 326*/
						cd_cep_w 																	|| /*Pos 327 a 334*/
						ds_cidade_w 																|| /*Pos 335 a 349*/
						substr(sg_estado_w,1,2) 													|| /*Pos 350 a 351*/
						rpad(substr(ds_instrucao_w,1,30),30,' ')									|| /*Pos 352 a 381*/
						rpad(' ',4,' ') 															|| /*Pos 382 a 385*/
						'000000' 																	|| /*Pos 386 a 391*/
						'00' 																		|| /*Pos 392 a 393*/
						' ' 																		|| /*Pos 394*/
						lpad(nr_seq_reg_arquivo_w,6,'0');											   /*Pos 395 a 400*/	

	insert	into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_reg_arquivo_w);
		
	/*Registro detalhe opcional*/	
	if (ds_email_w is not null) then
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		ds_conteudo_w	:=	'5' 											|| /*Pos 01*/
							rpad(nvl(ds_email_w,' '),120,' ')				|| /*Pos 02 a 121 */
							ie_tipo_inscricao_w 							|| /*Pos 122 a 123*/
							lpad(nvl(nr_inscricao_empresa_w,'0'),14,'0') 	|| /*Pos 124 a 137*/
							ds_endereco_w									|| /*Pos 138 a 177*/		
							ds_bairro_w										|| /*Pos 178 a 189*/
							cd_cep_w										|| /*Pos 190 a 197*/
							ds_cidade_w										|| /*Pos 198 a 212*/
							substr(sg_estado_w,1,2) 						|| /*Pos 213 a 214*/
							lpad(' ',180,' ')								|| /*Pos 215 a 394*/
							lpad(nr_seq_reg_arquivo_w,6,'0');				   /*Pos 395 a 400*/
							
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);	
	end if;			
	
end	loop;
close	C01;

/* trailer */
nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

ds_conteudo_w	:= 	'9' 								|| /*Pos 01*/
					rpad(' ',393,' ')	 				|| /*Pos 02 a 394*/
					lpad(nr_seq_reg_arquivo_w,6,'0');      /*Pos 395 a 400*/

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_reg_arquivo_w);

commit;

end HAOC_GERAR_COBR_ITAU_400_REG;
/