create or replace procedure
man_definir_impacto_requisitos (	nr_requisitos_p	varchar2,
					ie_impacto_p	varchar2,
					nm_usuario_p	Varchar2,
					ds_mensagem_p	out nocopy number)
is

qt_nao_aprovado_w	number;

cursor c_requisitos is
	select	mosip.nr_sequencia
	from	man_ordem_serv_imp_pr mosip,
		reg_product_requirement rpr
	where	rpr.nr_sequencia	= mosip.nr_product_requirement
	and	rpr.ie_situacao		= 'A'
	and	ie_impacto_p		= 'I'
	and	mosip.nr_sequencia in	(
						select	regexp_substr(nr_requisitos_p, '[^,]+', 1, level)
						from	dual
						connect by regexp_substr(nr_requisitos_p, '[^,]+', 1, level) is not null
					)
	union
	select	mosip.nr_sequencia
	from	man_ordem_serv_imp_pr mosip,
		reg_product_requirement rpr
	where	rpr.nr_sequencia	= mosip.nr_product_requirement
	and	rpr.ie_situacao		= 'A'
	and	ie_impacto_p		<> 'I'
	and	rpr.dt_aprovacao	is not null
	and	mosip.nr_sequencia in	(
						select	regexp_substr(nr_requisitos_p, '[^,]+', 1, level)
						from	dual
						connect by regexp_substr(nr_requisitos_p, '[^,]+', 1, level) is not null
					);

begin

	for	r_c01 in c_requisitos loop
		update	man_ordem_serv_imp_pr
		set	ie_impacto_requisito	= ie_impacto_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia	= r_c01.nr_sequencia;
	end loop;

	select	count(mosip.nr_sequencia)
	into	qt_nao_aprovado_w
	from	man_ordem_serv_imp_pr mosip,
		reg_product_requirement rpr
	where	rpr.nr_sequencia	= mosip.nr_product_requirement
	and	rpr.ie_situacao		= 'A'
	and	ie_impacto_p		<> 'I'
	and	rpr.dt_aprovacao	is null
	and	mosip.nr_sequencia in	(
						select	regexp_substr(nr_requisitos_p, '[^,]+', 1, level)
						from	dual
						connect by regexp_substr(nr_requisitos_p, '[^,]+', 1, level) is not null
					);

	if	(qt_nao_aprovado_w > 0) then
		begin
			ds_mensagem_p := 1073527; -- Existem PRS's n�o aprovados. Ser� necess�rio informar o tipo de impacto como 'Inclus�o'
		end;
	end if;

	commit;

end man_definir_impacto_requisitos;
/
