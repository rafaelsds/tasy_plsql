create or replace
procedure atualizar_desc_bordero_rec(nr_bordero_p		number,
					nr_titulo_p		number,
					cd_centro_custo_desc_p	number,
					nr_seq_motivo_desc_p	number,
					cd_pf_solic_desc_p	varchar2,
					cd_cgc_solic_desc_p	varchar2,
					nm_usuario_p		varchar2) is

/* Nao colocar commit nessa procedure, ela � chamada varias vezes em um la�o */

begin

if	(nr_bordero_p is not null) and
	(nr_titulo_p is not null) then

	if	(cd_pf_solic_desc_p is not null) and
		(cd_cgc_solic_desc_p is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(265464,'');
		--O solicitante deve ser uma pessoa f�sica ou uma pessoa jur�dica!
	end if;

	update	bordero_tit_rec
	set	cd_centro_custo_desc	= cd_centro_custo_desc_p,
		nr_seq_motivo_desc	= nr_seq_motivo_desc_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_bordero		= nr_bordero_p
	and	nr_titulo		= nr_titulo_p;

	if	(cd_pf_solic_desc_p is not null) or
		(cd_cgc_solic_desc_p is not null) then
		delete	from titulo_receber_liq_desc
		where	nr_bordero		= nr_bordero_p
		and	nr_titulo		= nr_titulo_p;

		insert	into	titulo_receber_liq_desc
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_bordero,
			nr_titulo,
			cd_pessoa_fisica,
			cd_cgc,
			nr_seq_motivo_desc,
			cd_centro_custo)
		values	(titulo_receber_liq_desc_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_bordero_p,
			nr_titulo_p,
			cd_pf_solic_desc_p,
			cd_cgc_solic_desc_p,
			nr_seq_motivo_desc_p,
			cd_centro_custo_desc_p);	
	end if;
end if;

end atualizar_desc_bordero_rec;
/
