create or replace
procedure fis_gerar_arq_C425_icmsipi(	nr_seq_controle_p	number,
					nr_seq_superior_p	number, 
					nr_linha_p		in out number	) is 

-- VARIABLES
ds_linha_w  varchar2(8000);
ds_sep_w    varchar2(1) := '|';
qt_cursor_w number(10) := 0;
nr_vetor_w  number(10) := 0;
nr_sequencia_w number(10) := 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

-- FIS_EFD_ICMSIPI_ARQUIVO
nr_linha_w fis_efd_icmsipi_arquivo.nr_linha%type;

cursor c_reg_C425 is
select	*
from	fis_efd_icmsipi_C425
where	nr_seq_controle = nr_seq_controle_p
and 	nr_seq_superior = nr_seq_superior_p;

/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_C425*/
type reg_c_reg_C425 is table of fis_efd_icmsipi_C425%RowType;
vetRegC425 reg_c_reg_C425;

/*Cria��o do array com o tipo sendo da tabela eespecificada - fis_efd_icmsipi_arquivo */
type registro is table of fis_efd_icmsipi_arquivo%rowtype index by pls_integer;
fis_registros_w registro;

begin

nm_usuario_w := Obter_Usuario_Ativo;
nr_linha_w   := nr_linha_p;

open c_reg_C425;
loop
fetch c_reg_C425 bulk collect into vetRegC425 limit 1000;
	for i in 1 .. vetRegC425.Count loop
	begin

	/*Incrementa a variavel para o array*/
	qt_cursor_w := qt_cursor_w + 1;
	
	nr_linha_w := nr_linha_w + 1;
	ds_linha_w := substr(ds_sep_w || vetRegC425(i).cd_reg ||
			     ds_sep_w || vetRegC425(i).cd_item ||
			     ds_sep_w || vetRegC425(i).qt_item ||
			     ds_sep_w || vetRegC425(i).cd_unid ||
			     ds_sep_w || substr(sped_obter_campo_valor(vetRegC425(i).vl_item ), 1, 30) ||
			     ds_sep_w || substr(sped_obter_campo_valor(vetRegC425(i).vl_pis ), 1, 30) ||
			     ds_sep_w || substr(sped_obter_campo_valor(vetRegC425(i).vl_cofins ), 1, 30) || ds_sep_w,
			     1,
			     8000);
			     
	/*   Atualiza o sequencial de ordena��o de linhas do arquivo */
	select fis_efd_icmsipi_arquivo_seq.nextval into nr_sequencia_w from dual;  

	fis_registros_w(qt_cursor_w).nr_sequencia		:= nr_sequencia_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao_nrec	:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario_nrec		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao		:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario			:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).nr_seq_controle		:= nr_seq_controle_p;
	fis_registros_w(qt_cursor_w).nr_linha			:= nr_linha_w;
	fis_registros_w(qt_cursor_w).ds_arquivo			:= substr(ds_linha_w,1,4000);
	fis_registros_w(qt_cursor_w).ds_arquivo_compl		:= substr(ds_linha_w,4001,4000);
	fis_registros_w(qt_cursor_w).cd_registro		:=  vetRegC425(i).cd_reg;

	if (nr_vetor_w >= 1000) then
	begin
	/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_C425 */
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_arquivo values fis_registros_w(i);

		nr_vetor_w := 0;
		fis_registros_w.delete;

		commit;

	end;
	end if;

	/*incrementa variavel para realizar o forall quando chegar no valor limite*/
	nr_vetor_w := nr_vetor_w + 1;

	end;
	end loop;
exit when c_reg_C425%notfound;
end loop;
close c_reg_C425;

if (fis_registros_w.count > 0) then
begin
/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
forall i in fis_registros_w.first .. fis_registros_w.last
	insert into fis_efd_icmsipi_arquivo values fis_registros_w (i);

	fis_registros_w.delete;

	commit;

end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

nr_linha_p := nr_linha_w;

end fis_gerar_arq_C425_icmsipi;
/
