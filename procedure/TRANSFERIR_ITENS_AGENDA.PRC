create or replace
procedure transferir_itens_agenda(	nr_seq_origem_p		number,
					nr_seq_destino_p	number,
					nm_usuario_p		varchar2)
					is

begin

update	AGENDA_PAC_EQUIP
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;

update	AGENDA_PAC_CME
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;

update	AGENDA_PAC_OPME
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;

update	AGENDA_PAC_SERVICO
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;

update	AGENDA_PAC_EQUIP
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;

update	AGENDA_PAC_HIST
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;	

update	GESTAO_VAGA
set	nr_seq_agenda	=	nr_seq_destino_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_origem_p;	
		
commit;

end transferir_itens_agenda;
/