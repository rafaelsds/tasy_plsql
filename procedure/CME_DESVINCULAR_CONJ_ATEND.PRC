create or replace
procedure CME_Desvincular_Conj_Atend(  cd_barras_p       number,
                                       nr_atendimento_p  number,
                                       nm_usuario_p      varchar2) is

nm_paciente_w            varchar2(100); 
nr_atendimento_w        number(10,0);
ds_observacao_w        varchar2(255);
nr_sequencia_w           number(10);
ie_tipo_w               	     varchar2(10);
nr_interno_conta_w      number(10);
cd_motivo_exc_conta_w   number(10);
cd_estabelecimento_w      number(10);
ds_regra_bloq_conj_w      varchar2(1);
nm_usuario_origem_w      usuario.nm_usuario%type;
nm_usuario_w             usuario.nm_usuario%type;
ie_lib_registro_vinc_conj_w varchar2(2);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(320459, null, wheb_usuario_pck.get_nr_seq_idioma);--Desfazer Desfecho

cursor   c02 is
   select   'P' ie_tipo,
            a.nr_sequencia,
            a.nr_interno_conta
   from     procedimento_paciente a,
            conta_paciente b,
            regra_lanc_automatico c
   where a.nr_interno_conta   = b.nr_interno_conta
   and   b.nr_Atendimento     = nr_atendimento_p
   and   c.nr_sequencia       = a.nr_seq_regra_lanc
   and   b.ie_status_acerto   = 1
   and   c.NR_SEQ_EVENTO      = 332
   and   a.cd_motivo_exc_conta is null
   and   a.ds_observacao like '%'||cd_barras_p||'%'
   union all
   select   'M' ie_tipo,
            a.nr_sequencia,
            a.nr_interno_conta
   from     material_Atend_paciente a,
            conta_paciente b,
            regra_lanc_automatico c
   where a.nr_interno_conta   = b.nr_interno_conta
   and   b.nr_Atendimento     = nr_atendimento_p
   and   c.nr_sequencia       = a.nr_seq_regra_lanc
   and   b.ie_status_acerto   = 1
   and   c.NR_SEQ_EVENTO      = 332
   and   a.cd_motivo_exc_conta is null
   and   a.ds_observacao like '%'||cd_barras_p||'%';


begin

select   substr(obter_pessoa_atendimento(nr_atendimento_p,'N'),1,100)
into     nm_paciente_w
from     dual;

ds_observacao_w   := WHEB_MENSAGEM_PCK.get_texto(300048,'NM_PACIENTE_W='|| NM_PACIENTE_W ||';NR_ATENDIMENTO_P='|| NR_ATENDIMENTO_P);
         /*Paciente: #@NM_PACIENTE_W#@ Num Atendimento: #@NR_ATENDIMENTO_P#@.*/

begin

select 	  nr_atendimento,
       	  nm_usuario_origem,
       	  nm_usuario
into   	  nr_atendimento_w,
         	nm_usuario_origem_w,
         	nm_usuario_w
from     cm_conjunto_cont
where    nr_sequencia   = cd_barras_p
and       nvl(ie_situacao,'A') = 'A';
exception
   when others then
      -- Conjunto nao encontrado!
      Wheb_mensagem_pck.exibir_mensagem_abort(153617);
end;

if (nr_atendimento_w is not null) then
   
   select   cd_estabelecimento
   into     cd_estabelecimento_w
   from     atendimento_paciente
   where    nr_atendimento = nr_atendimento_p;

   Obter_Param_Usuario(281,408,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ds_regra_bloq_conj_w);
   Obter_param_usuario(88,355,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_lib_registro_vinc_conj_w);

   if (( ds_regra_bloq_conj_w  = 'S' and 
         nm_usuario_p          <> nm_usuario_origem_w) or 
      (  ds_regra_bloq_conj_w = 'U' and 
         nm_usuario_p         <> nm_usuario_w)) then
      --Somente o usuario original pode desvincular o conjunto!
      Wheb_mensagem_pck.exibir_mensagem_abort(22007);
   else
      update   cm_conjunto_cont
      set   nr_atendimento    = null,
            ds_observacao     = null,
            dt_atualizacao    = sysdate,
            nm_usuario        = nm_usuario_p,
            NM_USUARIO_VINC   = null,
            DT_VINCULO_CONJ   = null,
			dt_liberacao = decode(ie_lib_registro_vinc_conj_w, 'S', dt_liberacao)
      where nr_sequencia      = cd_barras_p;
      
      select   max(cd_motivo_exc_conta)
      into     cd_motivo_exc_conta_w
      from     parametro_faturamento
      where    cd_estabelecimento   = cd_estabelecimento_w;
      
      open C02;
      loop
      fetch C02 into 
         ie_tipo_w,
         nr_sequencia_w,
         nr_interno_conta_w;
      exit when C02%notfound;
         begin
         excluir_matproc_conta(nr_sequencia_w,nr_interno_conta_w,cd_motivo_exc_conta_w,expressao1_w,ie_tipo_w,nm_usuario_p);
         end;
      end loop;
      close C02;
                 
      commit;
   end if;
else
   -- Este conjunto nao pertence ao atendimento.
   Wheb_mensagem_pck.exibir_mensagem_abort(264837);
end if;

end CME_Desvincular_Conj_Atend;
/
