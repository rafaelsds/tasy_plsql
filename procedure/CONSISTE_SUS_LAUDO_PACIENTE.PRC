create or replace
procedure consiste_sus_laudo_paciente(
		nr_sequencia_p			number,
		nm_usuario_p			varchar2,
		ie_laudo_inconsistente_p out	varchar2) is
		
ie_laudo_inconsistente_w	varchar2(1) := 'N';

begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p	is not null) then
	begin
	sus_consiste_laudo_sismama(nr_sequencia_p,nm_usuario_p);
	
	select	decode(nvl(count(*),0),0,'N','S')
	into	ie_laudo_inconsistente_w
	from	sus_inconsistencia_laudo b,
		sus_inco_reg_laudo	 a
	where	a.nr_seq_inconsistencia = b.nr_sequencia
	and	a.nr_seq_laudo		= nr_sequencia_p
	and	b.ie_consiste		= 'S';
	end;
end if;
commit;
ie_laudo_inconsistente_p := ie_laudo_inconsistente_w;
end consiste_sus_laudo_paciente;
/