create or replace
procedure Obter_se_regra_item_escala(	nr_seq_grupo_item_p	number,
									    ie_permite_p 		out varchar2) is
										
qt_reg_w				number(10);
cd_setor_atual_w		number(10);
cd_perfil_w				number(10);
cd_estabelecimento_w	number(10);
cd_especialidade_w		number(5,0) := null;
ie_permite_w			varchar2(10) := 'S';
nm_usuario_w			varchar2(15);
cd_pessoa_fisica_w		varchar2(15);

Cursor C01 is
	select	nvl(ie_permite,'S')
	from	grupo_escala_item_regra
	where	nr_seq_grupo_item	= nr_seq_grupo_item_p
	and		ie_situacao	='A'
	and		nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	and		nvl(cd_perfil, cd_perfil_w) = cd_perfil_w		
	and		nvl(cd_setor_atendimento,cd_setor_atual_w)	= cd_setor_atual_w
	and		nvl(cd_especialidade,cd_especialidade_w) = cd_especialidade_w
	and		nvl(nm_usuario_param,nm_usuario_w) = nm_usuario_w
	order by nvl(nm_usuario_param,'a'),
			nvl(cd_especialidade,0),
			nvl(cd_setor_atendimento,0),
			nvl(cd_perfil,0),
			nvl(cd_estabelecimento,0);

begin
nm_usuario_w			:= nvl(wheb_usuario_pck.get_nm_usuario,0);

Select 	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario = nm_usuario_w;

cd_setor_atual_w		:= nvl(wheb_usuario_pck.get_cd_setor_atendimento,0);
cd_perfil_w				:= nvl(wheb_usuario_pck.get_cd_perfil,0);
cd_estabelecimento_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
cd_especialidade_w		:= nvl(obter_especialidade_pf(cd_pessoa_fisica_w),0);

select	count(*)
into	qt_reg_w
from	grupo_escala_item_regra
where	nr_seq_grupo_item	= nr_seq_grupo_item_p
and		ie_situacao	='A';

if	(qt_reg_w	> 0) then

	open C01;
	loop
	fetch C01 into	
		ie_permite_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
end if;

ie_permite_p := ie_permite_w;

end Obter_se_regra_item_escala;
/
