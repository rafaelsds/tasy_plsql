create or replace
procedure sug_sugestao_envio_js(	ds_destino_p		varchar2,
				nr_seq_sugestao_p		number,
				nm_usuario_p		varchar2) is

begin

if	(nvl(ds_destino_p,'X') <> 'X') and
	(nr_seq_sugestao_p > 0) then
	insert into sug_sugestao_envio(	nr_sequencia,
					nr_seq_sugestao,
					ds_destino,
					dt_atualizacao,
					dt_envio,
					nm_usuario)
				values(	sug_sugestao_envio_seq.nextval,
					nr_seq_sugestao_p,
					ds_destino_p,
					sysdate,
					sysdate, 
					nm_usuario_p);
	commit;
end if;

end sug_sugestao_envio_js;
/
