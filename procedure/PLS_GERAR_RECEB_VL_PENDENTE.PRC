create or replace
procedure pls_gerar_receb_vl_pendente
			(	nr_seq_caixa_p		number,
				nr_seq_trans_financ_p	number,
				nr_titulo_p		varchar2,
				cd_estabelecimento_p	number,
				nr_seq_pagador_p	number,
				nm_usuario_p		varchar2) is

nr_seq_saldo_w		number(10);
nr_seq_caixa_rec_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);
ds_msg_retorno_w	varchar(4000);					

begin
if	(nr_seq_caixa_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(188951);
elsif	(nr_seq_trans_financ_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(188952);
elsif	(nr_titulo_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(188954);
else
	select	max(a.nr_sequencia)
	into	nr_seq_saldo_w
	from	caixa_saldo_diario a
	where	a.nr_seq_caixa	= nr_seq_caixa_p
	and	trunc(a.dt_saldo,'dd') = trunc(sysdate,'dd')
	and	a.dt_fechamento is null;
	
	if	(nr_seq_saldo_w is not null) then
		select	caixa_receb_seq.nextval
		into	nr_seq_caixa_rec_w
		from	dual;
		
		select	cd_pessoa_fisica,
			cd_cgc
		into	cd_pessoa_fisica_w,
			cd_cgc_w
		from	pls_contrato_pagador a
		where	a.nr_sequencia	= nr_seq_pagador_p;
		
		insert into caixa_receb
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_saldo_caixa,
			vl_especie,
			nr_seq_trans_financ,
			cd_pessoa_fisica,
			cd_cgc,
			ie_tipo_receb,
			ds_observacao,
			dt_recebimento)
		values	(nr_seq_caixa_rec_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_saldo_w,
			0,
			nr_seq_trans_financ_p,
			cd_pessoa_fisica_w,
			cd_cgc_w,
			'R',
			substr(wheb_mensagem_pck.get_texto(303938),1,255),
			sysdate);
		
		
		baixar_titulos_caixa_rec(	nr_seq_caixa_rec_w,
						nr_titulo_p,
						null,
						nm_usuario_p,
						ds_msg_retorno_w);
	else	
		wheb_mensagem_pck.exibir_mensagem_abort(188955);
	end if;
end if;

commit;

end pls_gerar_receb_vl_pendente;
/
