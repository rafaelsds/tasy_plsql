create or replace
procedure Ajustar_Prescr_mat_assoc_gas(	nr_prescricao_p		NUMBER,
										nr_sequencia_p		NUMBER,
										cd_perfil_p			number,
										nr_seq_material_p	number) IS

nr_ocorrencia_w			Number(19);
nr_sequencia_w			Number(6);
cd_material_w			Number(6);
qt_unitaria_w			Number(18,6);
qt_material_w			Number(15,3);
qt_dose_especial_w		Number(18,6);
ds_dose_diferenciada_w	Varchar2(50);
ie_origem_inf_w			Varchar2(1);
qt_dispensar_w			Number(18,6);
qt_dose_w				Number(18,6);
ds_erro_w				Varchar2(255);
cd_intervalo_w			Varchar2(7);
ie_se_necessario_w		Varchar2(1);
ie_urgencia_w			Varchar2(1);
ie_suspenso_w			Varchar2(1);
ie_via_aplicacao_w		Varchar2(5);
cd_unidade_medida_dose_w	Varchar2(30);
dt_primeiro_horario_w	Date;
nr_horas_validade_w		Number(5);
ds_horarios_w			Varchar2(255);
ds_horarios2_w			Varchar2(255);
ds_horarios3_w			Varchar2(255);
dt_prev_execucao_w		Date;
ie_regra_disp_w			varchar2(1);
qt_hora_intervalo_w		number(2);
qt_min_intervalo_w		number(5);
dt_prev_execucao_proc_w	date;
ie_se_necessario_mat_w	Varchar2(1);
	
cursor C01 is
Select	nvl(b.cd_intervalo, a.cd_intervalo),
	nvl(b.nr_sequencia,0),
	nvl(b.ie_se_necessario,'N'),
	a.dt_prev_execucao,
	a.qt_min_intervalo,
	c.dt_primeiro_horario,
	c.nr_horas_validade,
	nvl(b.cd_material,0),
 	nvl(b.qt_unitaria,0),
	nvl(b.qt_material,0),
	nvl(b.qt_dose_especial,0),
	nvl(b.ds_dose_diferenciada,''),
	nvl(b.ie_origem_inf,'1'),
	b.ie_via_aplicacao,
	b.cd_unidade_medida_dose,
	decode(nvl(a.ie_suspenso,'N'),'N',nvl(b.ie_suspenso,'N'),nvl(a.ie_suspenso,'N')),
	substr(a.ds_horarios,1,255),
	b.qt_dose
from	prescr_medica c,
		prescr_material b,
		prescr_gasoterapia a	
where	a.nr_prescricao = b.nr_prescricao
and		c.nr_prescricao	= a.nr_prescricao
and		b.nr_prescricao = c.nr_prescricao
and		b.nr_seq_gasoterapia = a.nr_sequencia
and		a.nr_sequencia  = nr_sequencia_p
and		a.nr_prescricao = nr_prescricao_p
and		b.ie_agrupador  = 15
and		((b.nr_sequencia = nr_seq_material_p) or
         (nvl(nr_seq_material_p,0) = 0));	

begin

open C01;
loop
fetch C01 into
	cd_intervalo_w,
	nr_sequencia_w,
	ie_se_necessario_mat_w,
	dt_prev_execucao_w,
	qt_min_intervalo_w,
	dt_primeiro_horario_w,
	nr_horas_validade_w,
	cd_material_w,
	qt_unitaria_w,
	qt_material_w,
	qt_dose_especial_w,
	ds_dose_diferenciada_w,
	ie_origem_inf_w,
	ie_via_aplicacao_w,
	cd_unidade_medida_dose_w,
	ie_suspenso_w,
	ds_horarios3_w,
	qt_dose_w;
exit when C01%notfound;

	qt_material_w	:= 0;
	nr_ocorrencia_w	:= 0;
	Calcular_Horario_Prescricao(nr_prescricao_p,cd_intervalo_w,dt_primeiro_horario_w,to_date(to_char(dt_prev_execucao_w,'hh24:mi'),'hh24:mi'),
				nr_horas_validade_w,cd_material_w,qt_hora_intervalo_w,qt_min_intervalo_w,nr_ocorrencia_w,ds_horarios_w,ds_horarios2_w,'N', null);
	
	ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;

	Obter_Quant_Dispensar(wheb_usuario_pck.get_cd_estabelecimento,cd_material_w, nr_prescricao_p, nr_sequencia_w, cd_intervalo_w, ie_via_aplicacao_w,
				qt_unitaria_w, nvl( qt_dose_especial_w,0), nr_ocorrencia_w, ds_dose_diferenciada_w,
				ie_origem_inf_w, cd_unidade_medida_dose_w, 1, qt_material_w, qt_dispensar_w, ie_regra_disp_w, ds_erro_w, 'N', 'N');
	
	Update	prescr_material
	set	cd_intervalo		= cd_intervalo_w,
		ie_acm				= 'N',
		ie_se_necessario	= 'N',
		ie_urgencia			= 'N',
		ie_suspenso			= ie_suspenso_w,
		ie_utiliza_kit		= 'N',
		ie_medicacao_paciente	= 'N',
		hr_prim_horario		= to_char(dt_prev_execucao_w,'hh24:mi'),
		qt_hora_intervalo	= qt_hora_intervalo_w,
		qt_min_intervalo	= qt_min_intervalo_w,
		ds_horarios			= nvl(ds_horarios3_w,ds_horarios_w),
		nr_ocorrencia		= nr_ocorrencia_w,
		qt_total_dispensar	= qt_dispensar_w,
		qt_material			= qt_material_w,
		ie_regra_disp		= decode(nvl(ie_regra_disp,'X'), 'D', ie_regra_disp, ie_regra_disp_w),
		qt_dose				= qt_dose_w
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia		= nr_sequencia_w
	and	nvl(ie_intervalo_dif,'N')	<> 'S';

end loop;
close C01;
commit;

end Ajustar_Prescr_mat_assoc_gas;
/
