create or replace
procedure pls_gerar_w_itens_glosa
			(	nr_seq_analise_p		number,
				nr_seq_grupo_p			number,
				nm_usuario_p			varchar2,
				nr_id_transacao_anali_p		number,
				nr_id_transacao_p	in out 	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a visualiza��o (tabela tempor�ria) do cabe�alho da an�lise de contas
m�dicas.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */  

Cursor C01 is
	select	a.nr_seq_conta,
		a.nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		a.qt_apresentada,          
		a.qt_liberado,             
		a.vl_calculado_co,         
		a.vl_calculado_hi,         
		a.vl_calculado_material,
		a.vl_liberado,             
		a.vl_liberado_co,          
		a.vl_liberado_hi,          
		a.vl_liberado_material,
		( select max(vl_co_ptu_imp) from pls_conta_proc where nr_sequencia = a.nr_seq_conta_proc) vl_co_imp,
		( select max(vl_procedimento_ptu_imp) from pls_conta_proc where nr_sequencia = a.nr_seq_conta_proc) vl_hi_imp,
		( select max(vl_material_ptu_imp) from pls_conta_proc where nr_sequencia = a.nr_seq_conta_proc) vl_material_imp,
		( select max(vl_material_ptu) from pls_conta_mat where nr_sequencia = a.nr_seq_conta_mat) vl_material_imp_1
	from	w_pls_analise_item a,
		w_pls_analise_selecao_item b
	where	nr_id_transacao 	= nr_id_transacao_anali_p
	and	b.nr_seq_analise	= nr_seq_analise_p
	and	b.nm_usuario		= nm_usuario_p
	and	a.nr_sequencia 		= b.nr_seq_w_item;
begin

select 	pls_id_glosa_seq.nextval
into	nr_id_transacao_p 
from 	dual;

for r_c01_w in C01() loop
	begin
	insert into w_pls_itens_glosa_analise
		(dt_atualizacao,          
		dt_atualizacao_nrec,
		nm_usuario,              
		nm_usuario_nrec,         
		nr_id_transacao,         
		nr_seq_conta_mat,        
		nr_seq_conta_proc,       
		nr_sequencia,            
		qt_apresentada,          
		qt_liberado,             
		vl_calculado_co,         
		vl_calculado_hi,         
		vl_calculado_material,
		vl_liberado,             
		vl_liberado_co,          
		vl_liberado_hi,          
		vl_liberado_material,
		vl_apres_co,
		vl_apres_hi,
		vl_apres_material)
	values	(sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_id_transacao_p,
		r_c01_w.nr_seq_conta_mat,
		r_c01_w.nr_seq_conta_proc,
		w_pls_itens_glosa_analise_seq.nextval,
		r_c01_w.qt_apresentada,          
		r_c01_w.qt_liberado,             
		r_c01_w.vl_calculado_co,         
		r_c01_w.vl_calculado_hi,         
		r_c01_w.vl_calculado_material,
		r_c01_w.vl_liberado,             
		r_c01_w.vl_liberado_co,          
		r_c01_w.vl_liberado_hi,          
		r_c01_w.vl_liberado_material,
		r_c01_w.vl_co_imp,
		r_c01_w.vl_hi_imp,
		nvl(r_c01_w.vl_material_imp, r_c01_w.vl_material_imp_1)); 
	end;
end loop;

end pls_gerar_w_itens_glosa;
/
