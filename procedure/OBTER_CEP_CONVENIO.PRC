create or replace
procedure Obter_cep_convenio(cd_pessoa_fisica_p varchar2,
			     ds_retorno_p	out varchar2,
			     ds_retorno_p2	out varchar2) is 


cd_cep_pessoa_w		varchar2(40);
cd_cep_pessoa_Int_w	varchar2(40);
cd_cdm_pessoa_w		varchar2(40);
cd_cdm_pessoa_Int_w	varchar2(40);
ie_forma_consistir_w	varchar2(5);
ds_retorno_w		varchar2(255);
ds_retorno_w2		varchar2(255);

begin

Obter_Param_Usuario(916,86,obter_perfil_ativo,obter_usuario_ativo,obter_estabelecimento_ativo,ie_forma_consistir_w);

if	(ie_forma_consistir_w <> 'N') then
	begin
		select	nvl(substr(obter_compl_pf(cd_pessoa_fisica_p, 1, 'CEP'),1,40),0),
			to_char(somente_numero(nvl(substr(obter_compl_pf(cd_pessoa_fisica_p, 1, 'CDM'),1,40),0)))
		into	cd_cep_pessoa_w,
			cd_cdm_pessoa_w
		from	dual;
		
		cd_cep_pessoa_Int_w := cd_cep_pessoa_w;
		cd_cdm_pessoa_Int_w := cd_cdm_pessoa_w;

		if	(substr(cd_cep_pessoa_w,1,1) = '0') then
			cd_cep_pessoa_Int_w := substr(cd_cep_pessoa_w,2,40);
		end if;

		if	(substr(cd_cdm_pessoa_w,1,1) = '0') then
			cd_cep_pessoa_Int_w := substr(cd_cdm_pessoa_w,2,40);
		end if;
		
		if (cd_cep_pessoa_w <> '0') and
		(Length(cd_cep_pessoa_w) <= 8) and
		(cd_cep_pessoa_int_w <> '0') and
		(Length(cd_cep_pessoa_int_w) <= 8) and
		(ie_forma_consistir_w = 'S') then
			begin
				ds_retorno_w	:= cd_cep_pessoa_w;
				ds_retorno_w2	:= cd_cep_pessoa_Int_w;
			end;
		end if;
		
		if (cd_cdm_pessoa_w <> '0') and
		(Length(cd_cdm_pessoa_w) <= 8) and
		(cd_cdm_pessoa_int_w <> '0') and
		(Length(cd_cdm_pessoa_int_w) <= 8) and
		(ie_forma_consistir_w = 'M') then
			begin
				ds_retorno_w	:= cd_cdm_pessoa_w;
				ds_retorno_w2	:= cd_cdm_pessoa_Int_w;
			end;
		end if;	
		
	ds_retorno_p   	:= ds_retorno_w;
	ds_retorno_p2	:= ds_retorno_w2;
	end;
end if;
	
commit;

end Obter_cep_convenio;
/
