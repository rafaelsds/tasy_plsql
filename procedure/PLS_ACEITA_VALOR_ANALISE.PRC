create or replace
procedure pls_aceita_valor_analise
			(	nr_sequencia_p		Number,
				ie_tipo_p		Number,
				nr_seq_grupo_p		Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is
				
vl_calculado_w		Number(10,2);
vl_total_apres_w	Number(10,2);
vl_liberado_w		Number(10,2);
qt_liberada_w		Number(10);
vl_unit_lib_w		Number(10,2);
qt_apresentado_w	Number(10);
vl_unitario_apres_w	Number(10,2);
nr_seq_conta_w		Number(10);
nr_seq_analise_w	Number(10);
nr_seq_item_w		Number(10);
ie_tipo_item_w		varchar2(1);

begin

select	nr_seq_conta,
	nr_seq_analise,
	nr_seq_item,
	ie_tipo_item
into	nr_seq_conta_w,
	nr_seq_analise_w,
	nr_seq_item_w,
	ie_tipo_item_w
from	w_pls_resumo_conta
where 	nr_sequencia = nr_sequencia_p;

if	(ie_tipo_p = 1) then
/*Aceitar melhor valor*/

	select	vl_calculado,
		vl_total_apres,
		nvl(qt_apresentado,1),
		vl_unitario_apres
	into	vl_calculado_w,
		vl_total_apres_w,
		qt_apresentado_w,
		vl_unitario_apres_w
	from	w_pls_resumo_conta
	where	nr_sequencia = nr_sequencia_p;

	if	(vl_calculado_w < vl_total_apres_w) then		
		vl_liberado_w	:= vl_calculado_w;
		qt_liberada_w	:= qt_apresentado_w;
		vl_unit_lib_w	:= dividir_sem_round(vl_calculado_w, qt_apresentado_w);
	else
		vl_liberado_w	:= vl_total_apres_w;
		qt_liberada_w	:= qt_apresentado_w;
		vl_unit_lib_w	:= vl_unitario_apres_w;
	end if;
	
	update	w_pls_resumo_conta
	set	vl_total = vl_liberado_w,
		qt_liberado = qt_liberada_w,
		vl_unitario = vl_unit_lib_w
	where	nr_sequencia = nr_sequencia_p;
	
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w, 13,
				 nr_seq_item_w, ie_tipo_item_w, null,
				 null, '', nr_seq_grupo_p,
				 nm_usuario_p, cd_estabelecimento_p);

elsif	(ie_tipo_p = 2) then
/*Aceitar valor calculado*/

	update	w_pls_resumo_conta
	set	vl_total = vl_calculado,
		qt_liberado = nvl(qt_apresentado,1),
		vl_unitario = dividir_sem_round(vl_calculado, qt_apresentado)
	where	nr_sequencia = nr_sequencia_p;
	
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w, 12,
				 nr_seq_item_w, ie_tipo_item_w, null,
				 null, '', nr_seq_grupo_p, 
				 nm_usuario_p, cd_estabelecimento_p);
	

elsif	(ie_tipo_p = 3) then
/*Aceitar valor apresentado*/
	
	update	w_pls_resumo_conta
	set	vl_total = vl_total_apres,
		qt_liberado = nvl(qt_apresentado,1),
		vl_unitario = vl_unitario_apres
	where	nr_sequencia = nr_sequencia_p;
	
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w, 11,
				 nr_seq_item_w, ie_tipo_item_w, null,
				 null, '', nr_seq_grupo_p,
				 nm_usuario_p, cd_estabelecimento_p);

end if;

commit;

end pls_aceita_valor_analise;
/