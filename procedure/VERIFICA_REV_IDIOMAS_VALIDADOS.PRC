create or replace
procedure verifica_rev_idiomas_validados(  nr_sequencia_p		number) is 

nr_seq_filho_w			number(10);
qt_revisao_pendente_w		number(10);

cursor c01 is
	select	nr_sequencia
	from	qua_documento
	where	nr_seq_superior = nr_sequencia_p;

begin

	open c01;
	loop
	fetch c01 into	
		nr_seq_filho_w;
	exit when c01%notfound;
		begin
			select	count(nr_sequencia)
			into	qt_revisao_pendente_w
			from	qua_doc_revisao
			where	nr_seq_doc = nr_seq_filho_w
			and	dt_validacao is null
			and	nvl(ie_situacao, 'A') = 'A';

			if qt_revisao_pendente_w > 0 then
				wheb_mensagem_pck.exibir_mensagem_abort(306880);			
			end if;
		end;
	end loop;
	close c01;

end verifica_rev_idiomas_validados;
/
