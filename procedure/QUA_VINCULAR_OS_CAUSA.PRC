create or replace
procedure qua_vincular_os_causa (
	nr_sequencia_p		number,
	nr_causa_p		number,
	nm_usuario_p		Varchar2) is 

begin

update	man_ordem_servico
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	nr_seq_causa_rnc = nr_causa_p
where	nr_sequencia = nr_sequencia_p ;
commit;

end qua_vincular_os_causa;
/