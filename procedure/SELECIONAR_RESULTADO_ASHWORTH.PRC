CREATE OR REPLACE
PROCEDURE Selecionar_resultado_Ashworth(
			nr_seq_ashworth_p	 Number,
			nr_seq_art_mov_musculo_p Number,
			ie_ashwort_d_p		 varchar2,
			ie_ashwort_e_p		 varchar2,
			nm_usuario_p		 Varchar2) IS 

BEGIN

insert into escala_ashworth_item(
	NR_SEQUENCIA,
	DT_ATUALIZACAO,
	NM_USUARIO,
	DT_ATUALIZACAO_NREC,
	NM_USUARIO_NREC,
	NR_SEQ_ASHWORTH,
	NR_SEQ_ART_MOV_MUSCULO,
	IE_ASHWORTH_D,
	IE_ASHWORTH_E)
	values (
	escala_ashworth_item_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_ashworth_p,
	nr_seq_art_mov_musculo_p,
	ie_ashwort_d_p,
	ie_ashwort_e_p
	);

commit;

END Selecionar_resultado_Ashworth;
/
