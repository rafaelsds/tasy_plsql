create or replace
procedure pls_gerar_tit_receber_discuss(	nr_seq_lote_discussao_p		pls_lote_discussao.nr_sequencia%type,
						nr_seq_acao_p			pls_processo_interc_acao.nr_sequencia%type,
						dt_vencimento_p			date,
						nm_usuario_p			usuario.nm_usuario%type) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OPS - Controle de Contestacoes
Finalidade: Gerar titulo a receber para o lote de discussao.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

ds_camara_w			varchar2(255);
nr_nota_credito_debito_a500_w	varchar2(30);
cd_cgc_w			varchar2(14);
cd_unimed_destino_w		varchar2(4);
ie_gerar_titulo_w		varchar2(2);
ie_tipo_titulo_receber_w	varchar2(2);
ie_tit_pagar_lib_pagamento_w	varchar2(1);
ie_envio_recebimento_w		varchar2(1);
ie_classif_cobranca_w		varchar2(1);
ie_acao_w			varchar2(3);
vl_contestado_w			number(15,2) := 0;
vl_procedimento_w		number(15,2) := 0;
vl_material_w			number(15,2) := 0;
vl_tit_disc_w			number(15,2) := 0;
vl_tit_disc_ndr_w		number(15,2) := 0;
vl_reconhecido_w		number(15,2) := 0;
vl_negado_w			number(15,2) := 0;
vl_ndc_w			number(15,2) := 0;
vl_tot_negado_w			number(15,2) := 0;
vl_tot_acordo_w			number(15,2) := 0;
nr_titulo_w			number(10);
nr_seq_ptu_fatura_w		number(10);
nr_seq_lote_contest_w		number(10);
cd_portador_w			number(10);
cd_tipo_taxa_juro_w		number(10);
cd_tipo_taxa_multa_w		number(10);
nr_seq_trans_fin_contab_w	number(10);
nr_seq_trans_fin_baixa_w	number(10);
nr_seq_trans_fin_cancel_w	number(10);
nr_seq_camara_contest_w		number(10);
nr_seq_pls_fatura_w		number(10);
nr_seq_congenere_w		number(10);
nr_seq_pagador_w		number(10);
nr_seq_pagador_fat_w		number(10);
nr_seq_conta_banco_w		number(10);
qt_dias_antes_a500_w		number(10);
nr_seq_camara_w			number(10);
nr_seq_periodo_w		number(10);
pr_juro_padrao_w		number(7,4);
pr_multa_padrao_w		number(7,4);
cd_tipo_portador_w		number(5);
cd_moeda_w			number(5);
qt_dias_vencimento_w		number(5);
cd_estabelecimento_w		number(4);
nr_seq_classe_rec_w		pls_processo_interc_acao.nr_seq_classe_rec%type;
dt_vencimento_w			date;
nr_titulo_pag_w			titulo_pagar.nr_titulo%type;
nr_titulo_pag_ndr_w		titulo_pagar.nr_titulo%type;
qt_registro_w			pls_integer;
vl_glosa_w			titulo_pagar_baixa.vl_glosa%type;
ie_tipo_arquivo_w		ptu_camara_contestacao.ie_tipo_arquivo%type;
ie_liquidado_w			varchar2(1);
ie_gerar_titulo_receber_w	varchar2(1)	:= 'S';
nr_seq_carteira_cobr_w		pls_processo_interc_acao.nr_seq_carteira_cobr%type;
ie_dt_vencto_w			pls_processo_interc_acao.ie_data_vencimento%type;
qt_camara_w			pls_integer;
ie_bloqueto_w			pls_processo_interc_acao.ie_bloqueto%type;
ie_tipo_camara_w		pls_camara_compensacao.ie_tipo_camara%type;
vl_apresentado_w		pls_lote_discussao.vl_apresentado%type;
vl_tot_aco_servico_w		pls_lote_discussao.vl_tot_aco_servico%type;
vl_tot_aco_taxa_w		pls_lote_discussao.vl_tot_aco_taxa%type;
vl_tot_aco_ult_serv_w		pls_lote_discussao.vl_tot_aco_servico%type;
vl_tot_aco_ult_taxa_w		pls_lote_discussao.vl_tot_aco_taxa%type;
qt_questionamento_w		pls_integer	:= 0;
qt_questionamento_rrs_w		pls_integer	:= 0;

begin
-- Obter o lote de contestacao do lote da discussao
select	max(a.nr_seq_lote_contest),
	nvl(max(a.vl_tot_aco_servico),0),
	nvl(max(a.vl_tot_aco_taxa),0),
	nvl(max(a.vl_apresentado),0)
into	nr_seq_lote_contest_w,
	vl_tot_aco_servico_w,
	vl_tot_aco_taxa_w,
	vl_apresentado_w
from	pls_lote_discussao	a
where	a.nr_sequencia	= nr_seq_lote_discussao_p;

select	nvl(max(a.vl_tot_aco_servico),0),
	nvl(max(a.vl_tot_aco_taxa),0)
into	vl_tot_aco_ult_serv_w,
	vl_tot_aco_ult_taxa_w
from	pls_lote_discussao	a
where	a.ie_status 	= 'F'
and	a.nr_sequencia	= (	select	max(l.nr_sequencia)
				from	pls_lote_discussao l
				where	l.nr_seq_lote_contest	= nr_seq_lote_contest_w
				and	l.ie_status 		= 'F'
				and	l.nr_sequencia		!= nr_seq_lote_discussao_p);

-- Retirar o valor dos lotes anteriores
vl_tot_aco_servico_w	:= vl_tot_aco_servico_w - vl_tot_aco_ult_serv_w;
vl_tot_aco_taxa_w	:= vl_tot_aco_taxa_w - vl_tot_aco_ult_taxa_w;

--verifico se existe camara cadastrada
select	count(1)
into	qt_camara_w
from	pls_camara_compensacao;

-- Obter fatura PTU
select	max(nr_seq_ptu_fatura),
	max(nr_seq_pls_fatura),
	max(ie_envio_recebimento),
	max(cd_estabelecimento)
into	nr_seq_ptu_fatura_w,
	nr_seq_pls_fatura_w,
	ie_envio_recebimento_w,
	cd_estabelecimento_w
from	pls_lote_contestacao
where	nr_sequencia	= nr_seq_lote_contest_w;

ie_liquidado_w := pls_obter_dados_lote_contest(nr_seq_lote_contest_w,'PC');

if	(nr_seq_ptu_fatura_w is not null) then -- Origem Importacao A500
	select	max(nr_sequencia),
		max(ie_tipo_arquivo)
	into	nr_seq_camara_contest_w,
		ie_tipo_arquivo_w
	from	ptu_camara_contestacao
	where	nr_seq_lote_contest 	= nr_seq_lote_contest_w;

	select	max(ie_classif_cobranca),
		max(nr_titulo),
		max(nr_titulo_ndc)
	into	ie_classif_cobranca_w,
		nr_titulo_pag_w,
		nr_titulo_pag_ndr_w
	from	ptu_fatura
	where	nr_sequencia = nr_seq_ptu_fatura_w;
	
	if	(nr_titulo_pag_w is not null) then
		select	nvl(sum(nvl(x.vl_glosa,0)),0)
		into	vl_glosa_w
		from	titulo_pagar_baixa x,
			titulo_pagar c
		where	x.nr_titulo	= c.nr_titulo
		and	x.nr_titulo	= nr_titulo_pag_w;
		
		if	(vl_glosa_w = 0) then
			select	nvl(sum(nvl(x.vl_glosa,0)),0)
			into	vl_glosa_w
			from	titulo_pagar_baixa x,
				titulo_pagar c
			where	x.nr_titulo	= c.nr_titulo
			and	x.nr_titulo	= nr_titulo_pag_ndr_w;
		end if;
	end if;	

elsif	(nr_seq_pls_fatura_w is not null) then -- Origem Faturamento
	select	max(nr_sequencia),
		max(ie_tipo_arquivo)
	into	nr_seq_camara_contest_w,
		ie_tipo_arquivo_w
	from	ptu_camara_contestacao
	where	nr_seq_lote_contest 	= nr_seq_lote_contest_w;

	if	((ie_liquidado_w = 'I') and 
		(ie_envio_recebimento_w = 'R') and
		(nr_seq_camara_contest_w is not null)) then
		ie_gerar_titulo_receber_w := 'N';
	end if;
	
	-- Arquivo do Tipo 1 noo gera titulo (recebe/pagar)
	if	(ie_tipo_arquivo_w = '1') then
		ie_gerar_titulo_receber_w := 'N';
	end if;
	
	if	(ie_gerar_titulo_receber_w = 'S') then
		select	max(ie_classif_cobranca)
		into	ie_classif_cobranca_w
		from	ptu_fatura
		where	nr_seq_pls_fatura = nr_seq_pls_fatura_w;
	end if;
end if;
		
if	(nvl(ie_gerar_titulo_receber_w,'S') = 'S') then

	if	(nr_seq_ptu_fatura_w is not null) or
		(nr_seq_pls_fatura_w is not null) then

		if	(nr_seq_ptu_fatura_w is not null) then -- Pagamento
			select	max(cd_unimed_origem)
			into	cd_unimed_destino_w
			from	ptu_fatura
			where	nr_sequencia	= nr_seq_ptu_fatura_w;
		
		elsif	(nr_seq_pls_fatura_w is not null) then -- Faturamento
			select	max(a.cd_cooperativa),
				max(b.nr_seq_pagador)
			into	cd_unimed_destino_w,
				nr_seq_pagador_fat_w
			from	pls_congenere	a,
				pls_fatura	b
			where	a.nr_sequencia	= b.nr_seq_congenere
			and	b.nr_sequencia	= nr_seq_pls_fatura_w;
		end if;

		-- Priorizar para pegar o CNPJ do pagador do titulo a receber de faturamento
		if      (nr_seq_pagador_fat_w is not null) then
			select	max(cd_cgc)
			into	cd_cgc_w
			from	pls_contrato_pagador
			where	nr_sequencia = nr_seq_pagador_fat_w;
		end if;
		
		-- Pegar o CNPJ do da cooperativa do A500
		if	(cd_cgc_w is null) then
			select	max(b.cd_cgc)
			into	cd_cgc_w
			from	pessoa_juridica	b,
				pls_congenere	a
			where	a.cd_cgc	 = b.cd_cgc
			and	to_number(a.cd_cooperativa) = to_number(cd_unimed_destino_w);
		end if;
		
		select	max(nr_sequencia)
		into	nr_seq_congenere_w
		from	pls_congenere
		where	cd_cgc	= cd_cgc_w;

		if	(nr_seq_congenere_w is not null) then
			select	max(nr_sequencia)
			into	nr_seq_pagador_w
			from	pls_contrato_pagador
			where	nr_seq_congenere	= nr_seq_congenere_w;
			
			select	max(c.nr_seq_camara)
			into	nr_seq_camara_w
			from	pls_congenere_camara c,
				pessoa_juridica	b,
				pls_congenere	a
			where	a.cd_cgc	= b.cd_cgc
			and	a.nr_sequencia	= c.nr_seq_congenere
			and	sysdate between c.dt_inicio_vigencia_ref and c.dt_fim_vigencia_ref
			and	to_number(a.cd_cooperativa) = to_number(cd_unimed_destino_w);
			
			if	(nr_seq_camara_w is not null) then
				select	max(ie_tipo_camara)
				into	ie_tipo_camara_w
				from	pls_camara_compensacao
				where	nr_sequencia	= nr_seq_camara_w;
			end if;
		end if;
		
		if	(nr_seq_camara_w is not null) then
			select	sum(nvl(vl_ndc,0)),
				sum(nvl(vl_negado,0))
			into	vl_ndc_w,
				vl_tot_negado_w
			from	(select	sum(nvl(x.vl_ndc,0)) vl_ndc,
					sum(nvl(x.vl_negado,0)) vl_negado
				from	pls_discussao_proc x,
					pls_contestacao_discussao a
				where	a.nr_sequencia	= x.nr_seq_discussao
				and	a.nr_seq_lote	= nr_seq_lote_discussao_p
				union all
				select	sum(nvl(x.vl_ndc,0)) vl_ndc,
					sum(nvl(x.vl_negado,0)) vl_negado
				from	pls_discussao_mat x,
					pls_contestacao_discussao a
				where	a.nr_sequencia	= x.nr_seq_discussao
				and	a.nr_seq_lote	= nr_seq_lote_discussao_p);
		end if;
		
		begin
		select	nvl(ie_tipo_titulo_receber,'1'),
			nr_seq_trans_fin_contab,
			nr_seq_trans_fin_baixa,
			nr_seq_trans_fin_cancel,
			qt_dias_vencimento,
			decode(ie_tit_pagar_lib_pagamento,'S','D',decode(ie_tit_pagar_lib_pagamento,'N','P','P')),
			nvl(qt_dias_antes_a500,0),
			ie_acao,
			nr_seq_conta_banco,
			nr_seq_classe_rec,
			nr_seq_carteira_cobr,
			nvl(ie_data_vencimento, 'DC'),
			nvl(ie_bloqueto,'N')
		into	ie_tipo_titulo_receber_w,
			nr_seq_trans_fin_contab_w,
			nr_seq_trans_fin_baixa_w,
			nr_seq_trans_fin_cancel_w,
			qt_dias_vencimento_w,
			ie_tit_pagar_lib_pagamento_w,
			qt_dias_antes_a500_w,
			ie_acao_w,
			nr_seq_conta_banco_w,
			nr_seq_classe_rec_w,
			nr_seq_carteira_cobr_w,
			ie_dt_vencto_w,
			ie_bloqueto_w
		from	pls_processo_interc_acao
		where	nr_sequencia	= nr_seq_acao_p;
		exception
		when no_data_found then
			ie_tipo_titulo_receber_w	:= '1';
			nr_seq_trans_fin_contab_w	:= null;
			nr_seq_trans_fin_baixa_w	:= null;
			nr_seq_trans_fin_cancel_w	:= null;
			qt_dias_vencimento_w		:= 0;
			ie_tit_pagar_lib_pagamento_w	:= 'P';
			qt_dias_antes_a500_w		:= 0;
			ie_acao_w			:= '1';
			nr_seq_conta_banco_w		:= null;
			nr_seq_carteira_cobr_w		:= null;
		end;
		
		-- OBTER VALOR QUE JA ESTA SENDO COBRADOS, PARA NAO COBRAR NOVAMENTE
		select	nvl(sum(nvl(vl_titulo,0)),0)
		into	vl_tit_disc_w
		from	titulo_receber
		where	nr_titulo in (	select	nr_titulo_receber
					from	pls_lote_discussao
					where	nr_seq_lote_contest = nr_seq_lote_contest_w
					and	ie_status	 <> 'C'
					and	nr_titulo_receber is not null);
					
		select	nvl(sum(nvl(vl_titulo,0)),0)
		into	vl_tit_disc_ndr_w
		from	titulo_receber
		where	nr_titulo in (	select	nr_titulo_receber_ndr
					from	pls_lote_discussao
					where	nr_seq_lote_contest = nr_seq_lote_contest_w
					and	ie_status	 <> 'C'
					and	nr_titulo_receber_ndr is not null);
		
		vl_tit_disc_w := nvl(vl_tit_disc_w,0) + nvl(vl_tit_disc_ndr_w,0);
		
		-- IE_TIPO_ACORDO
		-- 00 - Questionamento em negociacao
		-- 01 - Questionamento encerrado com Acordo
		-- 02 - Questionamento encerrado com Acordo sem emissao de nota
		-- 03 - Ignorado
		-- 04 - Excluido pelo autor
		-- 05 - Remetido para Camara Tecnica
		-- 06 - Remetido para a Camara Arbitral
		-- 10 - Encerrado pelo administrador
		-- 11 - Questionamento nao contestado

		-- OU processo MANUAL

		select	nvl(sum(b.vl_negado),0)	-- Valor total NEGADO procedimento
		into	vl_procedimento_w
		from	pls_discussao_proc 		b,
			pls_contestacao_discussao 	a
		where	a.nr_sequencia	= b.nr_seq_discussao
		and	a.nr_seq_lote	= nr_seq_lote_discussao_p
		and	(exists	(select	1
				from	ptu_questionamento x
				where	x.nr_seq_conta_proc = b.nr_seq_conta_proc
				and	x.ie_tipo_acordo not in ('02','00'))
		or	not exists	(select	1
					from	ptu_questionamento x
					where	x.nr_seq_conta_proc	= b.nr_seq_conta_proc));
				
			
		select	nvl(sum(b.vl_negado),0)	-- Valor total NEGADO material
		into	vl_material_w
		from	pls_discussao_mat 		b,
			pls_contestacao_discussao 	a
		where	a.nr_sequencia	= b.nr_seq_discussao
		and	a.nr_seq_lote	= nr_seq_lote_discussao_p
		and	(exists	(select	1
				from	ptu_questionamento x
				where	x.nr_seq_conta_mat = b.nr_seq_conta_mat
				and	x.ie_tipo_acordo not in ('02','00'))
		or	not exists	(select	1
					from	ptu_questionamento x
					where	x.nr_seq_conta_mat = b.nr_seq_conta_mat));

		vl_contestado_w := nvl(vl_procedimento_w,0) + nvl(vl_material_w,0);
		
		-- Se o valor negado da discussao nao for maior que os titulos, deve ser acomulado o valor negado de todas as discussoes da contestacao
		if	(vl_contestado_w < vl_tit_disc_w) or
			(ie_tipo_arquivo_w in ('7','8')) then
			select	nvl(sum(b.vl_negado),0)	-- Valor total NEGADO procedimento
			into	vl_procedimento_w
			from	pls_discussao_proc 		b,
				pls_contestacao_discussao 	a,
				pls_lote_discussao		x
			where	a.nr_sequencia		= b.nr_seq_discussao
			and	x.nr_sequencia		= a.nr_seq_lote
			and	x.nr_seq_lote_contest	= nr_seq_lote_contest_w
			and	x.ie_status		<> 'C'
			and	b.ie_tipo_acordo not in ('00','02','12','13')
			and	(exists	(select	1
					from	ptu_questionamento x
					where	x.nr_seq_conta_proc = b.nr_seq_conta_proc
					and	x.ie_tipo_acordo not in ('02','00'))
			or	not exists	(select	1
						from	ptu_questionamento x
						where	x.nr_seq_conta_proc	= b.nr_seq_conta_proc));

			select	nvl(sum(b.vl_negado),0)	-- Valor total NEGADO material
			into	vl_material_w
			from	pls_discussao_mat 		b,
				pls_contestacao_discussao 	a,
				pls_lote_discussao		x
			where	a.nr_sequencia		= b.nr_seq_discussao
			and	x.nr_sequencia		= a.nr_seq_lote
			and	x.nr_seq_lote_contest	= nr_seq_lote_contest_w
			and	x.ie_status		<> 'C'
			and	b.ie_tipo_acordo not in ('00','02','12','13')
			and	(exists	(select	1
					from	ptu_questionamento x
					where	x.nr_seq_conta_mat = b.nr_seq_conta_mat
					and	x.ie_tipo_acordo not in ('02','00'))
			or	not exists	(select	1
						from	ptu_questionamento x
						where	x.nr_seq_conta_mat = b.nr_seq_conta_mat));

			vl_contestado_w := nvl(vl_procedimento_w,0) + nvl(vl_material_w,0);
		end if;
		
		-- DESCONTAR VALOR QUE JA ESTA SENDO COBRADO
		vl_contestado_w := abs(nvl(vl_contestado_w,0) - nvl(vl_tit_disc_w,0)); -- Os valores sempre devem retornar coerentes. Quando nao for pra gerar titulo, o resultado sera ZERO
		
		if	(ie_acao_w = '1') and -- Fatura BR
			(nr_seq_pls_fatura_w is not null) and
			(nr_seq_camara_contest_w is not null) then
			--(nr_seq_camara_w is null) then	-- Enviar email para Ronaldo para verificar se sera assim em definitivo. Estava gerando titulo porque nao era uma unimed de Fatura BR
			
			select	count(1)
			into	qt_questionamento_w
			from	ptu_questionamento
			where	nr_seq_contestacao = nr_seq_camara_contest_w
			and	ie_tipo_acordo not in ('02');
			
			if	(qt_questionamento_w > 0) then
				select	sum(nvl(vl_acordo,0) +
					nvl(vl_acordo_co,0) + 
					nvl(vl_acordo_filme,0) +
					nvl(vl_acordo_adic_co,0) +
					nvl(vl_acordo_adic_filme,0) +
					nvl(vl_acordo_adic_serv,0))
				into	vl_tot_acordo_w
				from	ptu_questionamento
				where	nr_seq_contestacao = nr_seq_camara_contest_w
				and	ie_tipo_acordo not in ('02');
			else
				select	count(1)
				into	qt_questionamento_rrs_w
				from	ptu_quest_serv_rrs	b,
					ptu_questionamento_rrs	a
				where	a.nr_sequencia		= b.nr_seq_quest_rrs
				and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
				and	b.tp_acordo not in ('02');
				
				if	(qt_questionamento_rrs_w > 0) then
					select	sum(nvl(b.vl_acordo_serv, 0))
					into	vl_tot_acordo_w
					from	ptu_quest_serv_rrs	b,
						ptu_questionamento_rrs	a
					where	a.nr_sequencia		= b.nr_seq_quest_rrs
					and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
					and	b.tp_acordo not in ('02');
				end if;
			end if;
			
			select	nvl(sum(nvl(vl_titulo,0)),0)
			into	vl_tit_disc_w
			from	titulo_receber
			where	nr_titulo in (	select	nr_titulo_receber
						from	pls_lote_discussao
						where	nr_seq_lote_contest = nr_seq_lote_contest_w
						and	ie_status	 <> 'C'
						and	nr_titulo_receber is not null);
						
			-- DESCONTAR VALOR QUE JA ESTA SENDO COBRADO
			vl_contestado_w := abs(nvl(vl_tot_acordo_w,0) - nvl(vl_tit_disc_w,0)); -- Os valores sempre devem retornar coerentes. Quando nao for pra gerar ttulo, o resultado sera ZERO
		end if;
			
		-- CASO TENHA QUE GERAR DOIS TITULOS		FATURA - NDR
		if	(ie_classif_cobranca_w = '3') then
			if	(ie_acao_w = '15') and
				(nr_seq_camara_contest_w is not null) then -- NDR
				
				select	count(1)
				into	qt_questionamento_w
				from	ptu_questionamento
				where	nr_seq_contestacao = nr_seq_camara_contest_w
				and	ie_tipo_acordo not in ('02','05');
				
				if	(qt_questionamento_w > 0) then
					select	sum(nvl(vl_cobrado,0) +
						nvl(vl_cobr_co,0) + 
						nvl(vl_cobr_filme,0)),
						sum(nvl(vl_reconhecido,0) +
						nvl(vl_reconh_co,0) +
						nvl(vl_reconh_filme,0))
					into	vl_negado_w,
						vl_reconhecido_w
					from	ptu_questionamento
					where	nr_seq_contestacao = nr_seq_camara_contest_w
					and	ie_tipo_acordo not in ('02','05');
				else
					select	count(1)
					into	qt_questionamento_rrs_w
					from	ptu_quest_serv_rrs	b,
						ptu_questionamento_rrs	a
					where	a.nr_sequencia		= b.nr_seq_quest_rrs
					and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
					and	b.tp_acordo not in ('02','05');
					
					if	(qt_questionamento_rrs_w > 0) then
						select	sum(nvl(b.vl_serv, 0)),
							sum(nvl(b.vl_reconh_serv, 0))
						into	vl_negado_w,
							vl_reconhecido_w
						from	ptu_quest_serv_rrs	b,
							ptu_questionamento_rrs	a
						where	a.nr_sequencia		= b.nr_seq_quest_rrs
						and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
						and	b.tp_acordo not in ('02','05');
					end if;
				end if;
				
				vl_negado_w := vl_negado_w - vl_reconhecido_w;
				
				select	nvl(sum(nvl(vl_titulo,0)),0)
				into	vl_tit_disc_w
				from	titulo_receber
				where	nr_titulo in (	select	nr_titulo_receber_ndr
							from	pls_lote_discussao
							where	nr_seq_lote_contest = nr_seq_lote_contest_w
							and	ie_status	 <> 'C'
							and	nr_titulo_receber_ndr is not null);
							
				-- DESCONTAR VALOR QUE JA ESTA SENDO COBRADO
				vl_contestado_w := abs(nvl(vl_negado_w,0) - nvl(vl_tit_disc_w,0)); -- Os valores sempre devem retornar coerentes. Quando nao for pra gerar titulo, o resultado sera ZERO
				
				if	(nr_seq_camara_w is not null) and -- Caso tenha pagamento integral  CAMARA(faturamento), e tenha Vl NDC, gera titulo, senao, nao gera..
					(nr_seq_pls_fatura_w is not null) and
					(vl_ndc_w = vl_tot_negado_w) then
					vl_contestado_w := 0;
				end if;
				
				if	(nr_seq_pls_fatura_w is not null) and -- Fatura BR
					(nr_seq_camara_contest_w is not null) and
					(nr_seq_camara_w is null) then		
					
					select	count(1)
					into	qt_questionamento_w
					from	ptu_questionamento
					where	nr_seq_contestacao = nr_seq_camara_contest_w
					and	ie_tipo_acordo not in ('02','05');
					
					if	(qt_questionamento_w > 0) then
						select	sum(nvl(vl_acordo,0) +
							nvl(vl_acordo_co,0) + 
							nvl(vl_acordo_filme,0))
						into	vl_tot_acordo_w
						from	ptu_questionamento
						where	nr_seq_contestacao = nr_seq_camara_contest_w
						and	ie_tipo_acordo not in ('02','05');
					else
						select	count(1)
						into	qt_questionamento_rrs_w
						from	ptu_quest_serv_rrs	b,
							ptu_questionamento_rrs	a
						where	a.nr_sequencia		= b.nr_seq_quest_rrs
						and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
						and	b.tp_acordo not in ('02','05');
						
						if	(qt_questionamento_rrs_w > 0) then
							select	sum(nvl(b.vl_acordo_serv, 0))
							into	vl_tot_acordo_w
							from	ptu_quest_serv_rrs	b,
								ptu_questionamento_rrs	a
							where	a.nr_sequencia		= b.nr_seq_quest_rrs
							and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
							and	b.tp_acordo not in ('02','05');
						end if;
					end if;
					
					-- DESCONTAR VALOR QUE JA ESTA SENDO COBRADO
					vl_contestado_w := abs(nvl(vl_tot_acordo_w,0) - nvl(vl_tit_disc_w,0)); -- Os valores sempre devem retornar coerentes. Quando nao for pra gerar titulo, o resultado sera ZERO
				end if;
			
			elsif	(ie_acao_w = '14') and -- FATURA
				(nr_seq_camara_contest_w is not null) then
				
				select	count(1)
				into	qt_questionamento_w
				from	ptu_questionamento
				where	nr_seq_contestacao = nr_seq_camara_contest_w
				and	ie_tipo_acordo not in ('02','05');
				
				if	(qt_questionamento_w > 0) then
					select	sum(nvl(vl_cobr_adic_co,0) +
						nvl(vl_cobr_adic_filme,0) +
						nvl(vl_cobr_adic_serv,0)),
						sum(nvl(vl_reconh_adic_co,0) +
						nvl(vl_reconh_adic_filme,0) +
						nvl(vl_reconh_adic_serv,0))
					into	vl_negado_w,
						vl_reconhecido_w
					from	ptu_questionamento
					where	nr_seq_contestacao = nr_seq_camara_contest_w
					and	ie_tipo_acordo not in ('02','05');
				else
					select	count(1)
					into	qt_questionamento_rrs_w
					from	ptu_quest_serv_rrs	b,
						ptu_questionamento_rrs	a
					where	a.nr_sequencia		= b.nr_seq_quest_rrs
					and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
					and	b.tp_acordo not in ('02','05');
					
					if	(qt_questionamento_rrs_w > 0) then
						select	sum(nvl(b.vl_serv, 0)),
							sum(nvl(b.vl_reconh_serv, 0))
						into	vl_negado_w,
							vl_reconhecido_w
						from	ptu_quest_serv_rrs	b,
							ptu_questionamento_rrs	a
						where	a.nr_sequencia		= b.nr_seq_quest_rrs
						and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
						and	b.tp_acordo not in ('02','05');
					end if;
				end if;
				
				vl_negado_w := vl_negado_w - vl_reconhecido_w;
				
				select	nvl(sum(nvl(vl_titulo,0)),0)
				into	vl_tit_disc_w
				from	titulo_receber
				where	nr_titulo in (	select	nr_titulo_receber
							from	pls_lote_discussao
							where	nr_seq_lote_contest = nr_seq_lote_contest_w
							and	ie_status	 <> 'C'
							and	nr_titulo_receber is not null);
							
				-- DESCONTAR VALOR QUE JA ESTA SENDO COBRADO
				vl_contestado_w := abs(nvl(vl_negado_w,0) - nvl(vl_tit_disc_w,0)); -- Os valores sempre devem retornar coerentes. Quando nao for pra gerar titulo, o resultado sera ZERO
								
				if	(nr_seq_camara_w is not null) and -- Caso tenha pagamento integral  CAMARA(faturamento), e tenha Vl NDC, gera titulo, senao, nao gera..
					(nr_seq_pls_fatura_w is not null) and
					(vl_ndc_w = vl_tot_negado_w) then
					vl_contestado_w := 0;
				end if;
				
				if	(nr_seq_pls_fatura_w is not null) and -- Fatura BR
					(nr_seq_camara_contest_w is not null) and
					(nr_seq_camara_w is null) then		
					
					select	count(1)
					into	qt_questionamento_w
					from	ptu_questionamento
					where	nr_seq_contestacao = nr_seq_camara_contest_w
					and	ie_tipo_acordo not in ('02');
					
					if	(qt_questionamento_w > 0) then
						select	sum(nvl(vl_acordo_adic_co,0) +
							nvl(vl_acordo_adic_filme,0) +
							nvl(vl_acordo_adic_serv,0))
						into	vl_tot_acordo_w
						from	ptu_questionamento
						where	nr_seq_contestacao = nr_seq_camara_contest_w
						and	ie_tipo_acordo not in ('02');
					else
						select	count(1)
						into	qt_questionamento_rrs_w
						from	ptu_quest_serv_rrs	b,
							ptu_questionamento_rrs	a
						where	a.nr_sequencia		= b.nr_seq_quest_rrs
						and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
						and	b.tp_acordo not in ('02');
						
						if	(qt_questionamento_rrs_w > 0) then
							select	sum(nvl(b.vl_acordo_serv, 0))
							into	vl_tot_acordo_w
							from	ptu_quest_serv_rrs	b,
								ptu_questionamento_rrs	a
							where	a.nr_sequencia		= b.nr_seq_quest_rrs
							and	a.nr_seq_contestacao	= nr_seq_camara_contest_w
							and	b.tp_acordo not in ('02');
						end if;
					end if;
					
					-- DESCONTAR VALOR QUE JA ESTA SENDO COBRADO
					vl_contestado_w := abs(nvl(vl_tot_acordo_w,0) - nvl(vl_tit_disc_w,0)); -- Os valores sempre devem retornar coerentes. Quando nao for pra gerar titulo, o resultado sera ZERO					
				end if;
			end if;
		end if;
		
		-- ESSE TRECHO PODE SUBSTITUIR BOA PARTE DOS SELECTS DE CIMA, CASO NAO DE PROBLEMAS, REMOVER OS SELECTS ACIMA NAS PROXIMAS VERSOES
		-- Se estar vindo do A550 tem que ter os valores ja no lote de discussao
		if	(nr_seq_camara_contest_w is not null) then
			vl_contestado_w := 0;
		
			-- Gerar titulo a receber
			if	(ie_classif_cobranca_w in ('1','2')) and
				(ie_acao_w in ('1','14')) then
				vl_contestado_w := vl_apresentado_w;
			end if;			

			-- Se o A500 foi dividido em 2
			if	(ie_classif_cobranca_w = '3') then
				if	(ie_acao_w = '15') then -- Gerar titulo a receber (NDR)
					vl_contestado_w := vl_tot_aco_servico_w;
				elsif	(ie_acao_w = '14') then -- Gerar titulo a receber (Fatura)
					vl_contestado_w := vl_tot_aco_taxa_w;
				end if;
			end if;
		end if;
		
		if	(nr_seq_ptu_fatura_w is not null) and
			((nr_seq_camara_w is null) and (qt_camara_w > 0)) and
			(vl_glosa_w > 0) then
			vl_contestado_w := 0;
		end if;
		
		-- Camara Nacional
		if	(ie_tipo_camara_w = 'N') then
			-- Processo Pagamento
			-- Pagamento parcial gera ndc a pagar e pagamento integral gera ndc a receber
			if	(nr_seq_ptu_fatura_w is not null) and
				(ie_liquidado_w = 'P') then
				vl_contestado_w := 0;

			-- Processo faturamento
			-- Pagamento parcial gera ndc a receber e pagamento integral gera ndc a pagar
			elsif	(nr_seq_pls_fatura_w is not null) and
				(ie_liquidado_w = 'I') then
				vl_contestado_w := 0;
			end if;
		end if;

		-- So gerar se teve valor contestado
		if	(vl_contestado_w > 0) then	
			if	(nr_seq_pagador_w is not null) and
				(nr_seq_conta_banco_w is null) then
				nr_seq_conta_banco_w := pls_obter_dados_pagador_fin(nr_seq_pagador_w, 'NS');
			end if;
			
			select	max(cd_moeda_padrao),
				max(cd_portador),
				max(cd_tipo_portador),
				max(cd_tipo_taxa_juro),
				max(cd_tipo_taxa_multa),
				max(pr_juro_padrao),
				max(pr_multa_padrao)
			into	cd_moeda_w,
				cd_portador_w,
				cd_tipo_portador_w,
				cd_tipo_taxa_juro_w,
				cd_tipo_taxa_multa_w,
				pr_juro_padrao_w,
				pr_multa_padrao_w
			from	parametro_contas_receber
			where	cd_estabelecimento	= cd_estabelecimento_w;
			
			if	(nr_seq_camara_w is not null) then -- Caso tenha camara definido
				select	max(nr_sequencia)
				into	nr_seq_periodo_w
				from	(select	a.nr_sequencia
					from	pls_camara_calendario b,
						pls_camara_calend_periodo a
					where	a.nr_seq_calendario	= b.nr_sequencia
					and	b.nr_seq_camara		= nr_seq_camara_w
					and	trunc(sysdate + qt_dias_antes_a500_w) <= trunc(a.dt_limite_a500)
					order by a.dt_limite_a500)
				where	rownum <= 1;
				
				if	(nr_seq_periodo_w is null) then
					select	max(ds_camara)
					into	ds_camara_w
					from	pls_camara_compensacao
					where	nr_sequencia	= nr_seq_camara_w;
					
					wheb_mensagem_pck.exibir_mensagem_abort(221158,'DS_CAMARA='||ds_camara_w);
				else
					if	(ie_dt_vencto_w = 'DD') then
						select	max(a.dt_saldo_devedor)
						into	dt_vencimento_w
						from	pls_camara_calendario b,
							pls_camara_calend_periodo a
						where	b.nr_sequencia	= a.nr_seq_calendario
						and	a.nr_sequencia	= nr_seq_periodo_w;
					elsif	(ie_dt_vencto_w = 'NA') and -- nao se aplica
						(qt_dias_vencimento_w > 0) then
						dt_vencimento_w	:= trunc((dt_vencimento_p + qt_dias_vencimento_w), 'dd');
					else
						select	max(a.dt_saldo_credor)
						into	dt_vencimento_w
						from	pls_camara_calendario b,
							pls_camara_calend_periodo a
						where	b.nr_sequencia	= a.nr_seq_calendario
						and	a.nr_sequencia	= nr_seq_periodo_w;
					end if;
				end if;					
			else					
				if	(qt_dias_vencimento_w > 0) then
					dt_vencimento_w	:= trunc((dt_vencimento_p + qt_dias_vencimento_w), 'dd');
				else
					dt_vencimento_w	:= trunc(dt_vencimento_p, 'dd');
				end if;
			end if;	
			
			select	titulo_seq.nextval
			into	nr_titulo_w
			from	dual;
			
			insert	into titulo_receber
				(cd_cgc,
				cd_estabelecimento,
				cd_moeda,
				cd_portador,
				cd_tipo_portador,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				dt_atualizacao,
				dt_emissao,
				dt_pagamento_previsto,
				dt_vencimento,
				ie_origem_titulo,
				ie_situacao,
				ie_tipo_emissao_titulo,
				ie_tipo_inclusao,
				ie_tipo_titulo,
				nm_usuario,
				nr_titulo,
				tx_desc_antecipacao,
				tx_juros,
				tx_multa,
				vl_saldo_juros,
				vl_saldo_multa,
				vl_saldo_titulo,
				vl_titulo,
				nr_seq_pls_lote_contest,
				nr_seq_pls_lote_disc,
				ds_observacao_titulo,
				nr_documento,
				nr_lote_contabil,
				nm_usuario_orig,
				dt_inclusao,
				nr_seq_ptu_fatura,
				nr_seq_trans_fin_contab,
				nr_seq_trans_fin_baixa,
				nr_seq_conta_banco,
				nr_seq_classe,
				nr_seq_carteira_cobr)
			values	(cd_cgc_w,
				cd_estabelecimento_w,
				cd_moeda_w,
				cd_portador_w,
				cd_tipo_portador_w,
				cd_tipo_taxa_juro_w,
				cd_tipo_taxa_multa_w,
				sysdate,
				trunc(sysdate,'dd'),
				dt_vencimento_w,
				dt_vencimento_w,
				'11', --Contestacao
				'1',
				1,
				'2',
				ie_tipo_titulo_receber_w,
				nm_usuario_p,
				nr_titulo_w,
				0,
				pr_juro_padrao_w,
				pr_multa_padrao_w,
				0,
				0,
				vl_contestado_w,
				vl_contestado_w,
				nr_seq_lote_contest_w,
				nr_seq_lote_discussao_p,
				obter_expressao_dic_objeto(997439),
				nr_titulo_w,
				0,
				nm_usuario_p,
				sysdate,
				nr_seq_ptu_fatura_w,
				nr_seq_trans_fin_contab_w,
				nr_seq_trans_fin_baixa_w,
				nr_seq_conta_banco_w,
				nr_seq_classe_rec_w,
				nr_seq_carteira_cobr_w);
								
			if	(ie_acao_w = '1') then	-- FATURA - NDR
				update	pls_lote_discussao
				set	nr_titulo_receber	= nr_titulo_w
				where	nr_sequencia		= nr_seq_lote_discussao_p;
			
			elsif	(ie_acao_w = '15') or
				(ie_classif_cobranca_w = '1') then -- NDR
				update	pls_lote_discussao
				set	nr_titulo_receber_ndr	= nr_titulo_w
				where	nr_sequencia		= nr_seq_lote_discussao_p;
			
			elsif	(ie_acao_w = '14') or
				(ie_classif_cobranca_w = '2') then -- FATURA
				update	pls_lote_discussao
				set	nr_titulo_receber	= nr_titulo_w
				where	nr_sequencia		= nr_seq_lote_discussao_p;
				
			end if;
			
			if	(ie_bloqueto_w = 'S') then
				gerar_bloqueto_tit_rec(nr_titulo_w,'OPSCC');
			end if;
			
			-- Inserir o titulo em camara de compensacao
			pls_inserir_tit_camara(	null, nr_titulo_w, nr_seq_camara_w, sysdate, qt_dias_antes_a500_w, cd_estabelecimento_w, nm_usuario_p);
			
			-- Gerar classificacao do titulo
			pls_gerar_tit_rec_class_disc( nr_titulo_w, nm_usuario_p, 'N');
		end if;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(174945);
	end if;
end if;

end pls_gerar_tit_receber_discuss;
/