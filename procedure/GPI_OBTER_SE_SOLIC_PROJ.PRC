create or replace FUNCTION gpi_obter_se_solic_proj(
   nr_seq_proj_gpi_p  NUMBER,
   nr_seq_solic_p NUMBER)
   RETURN VARCHAR2 IS
   
ie_vinculo_solic_proj_w VARCHAR2(1);

BEGIN
SELECT CASE
         WHEN Count(1) > 0 THEN 'S'
         ELSE 'N'
       END AS ie_possui
INTO   ie_vinculo_solic_proj_w
FROM   solic_compra
WHERE  nr_seq_proj_gpi = nr_seq_proj_gpi_p
AND    nr_solic_compra = nr_seq_solic_p; 

RETURN ie_vinculo_solic_proj_w;

END gpi_obter_se_solic_proj;
/