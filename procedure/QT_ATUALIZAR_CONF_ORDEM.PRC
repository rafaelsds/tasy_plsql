create or replace
procedure QT_ATUALIZAR_CONF_ORDEM(nr_seq_ordem_p		number,
						nr_prescricao_p		number,
						nm_usuario_p		Varchar2) is 

ie_perm_nao_liberado_w		varchar2(1);
ie_liberado_w				varchar2(1) := 'S';
begin

Obter_param_Usuario(3130, 242, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_perm_nao_liberado_w);

if (ie_perm_nao_liberado_w = 'N') then
	select  nvl(max('S'),'N')   
	into 	ie_liberado_w
    from    prescr_medica                                                   
    where   nr_prescricao = nr_prescricao_p 
	and     ((dt_liberacao is not null) or (dt_liberacao_medico is not null));
end if;

if (ie_liberado_w = 'S') then
	update	can_ordem_prod
	set		ie_conferido 	=	'S',
			nm_usuario		=	nm_usuario_p,
			dt_atualizacao	=	sysdate
	where 	nr_sequencia 	=	nr_seq_ordem_p;
end if;

commit;

end qt_atualizar_conf_ordem;
/