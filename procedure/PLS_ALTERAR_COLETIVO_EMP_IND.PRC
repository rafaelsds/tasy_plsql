create or replace
procedure pls_alterar_coletivo_emp_ind(	nr_seq_contrato_p	pls_contrato.nr_sequencia%type) is 

ie_empresario_individual_w	pls_contrato.ie_empresario_individual%type;
ie_alterar_w			varchar2(1);

begin

select	nvl(max(ie_empresario_individual),'N')
into	ie_empresario_individual_w
from	pls_contrato
where	nr_sequencia = nr_seq_contrato_p;

if	(ie_empresario_individual_w = 'N') then
	ie_alterar_w	:= 'S';
else
	ie_alterar_w	:= 'N';
end if;

update	pls_contrato
set	ie_empresario_individual = ie_alterar_w
where	nr_sequencia = nr_seq_contrato_p;

insert into pls_contrato_historico
	(nr_sequencia,
	cd_estabelecimento,
	nr_seq_contrato,
	nm_usuario,
	nm_usuario_nrec,
	dt_atualizacao,
	dt_historico,
	dt_atualizacao_nrec,
	ie_tipo_historico,
	ds_historico)
values	(pls_contrato_historico_seq.nextval,
	wheb_usuario_pck.get_cd_estabelecimento,
	nr_seq_contrato_p,
	wheb_usuario_pck.get_nm_usuario,
	wheb_usuario_pck.get_nm_usuario,
	sysdate,
	sysdate,
	sysdate,
	'100',
	wheb_mensagem_pck.get_texto(1036201,'IE_ALTERAR='||ie_alterar_w)); --Alterado o campo Coletivo empresarial individual para: #@IE_ALTERAR#@.

commit;

end pls_alterar_coletivo_emp_ind;
/
