create or replace
procedure seisa_gerar_ret_cobr_sant_400
			(	nr_seq_cobr_escrit_p	number,
				nm_usuario_p		varchar2) is 

ie_tipo_carteira_w		varchar2(255);
ds_titulo_w			varchar2(255);
cd_carteira_w			varchar2(40);
cd_cgc_desc_w			varchar2(14);
cd_pessoa_fisica_desc_w		varchar2(10);
nr_nosso_numero_w		varchar2(7);
ie_separa_carteira_w		varchar2(1);
vl_juros_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
vl_cobranca_w			number(15,2);
vl_tarifa_cobranca_w		number(15,2);
vl_saldo_titulo_w		number(15,2);
vl_acrescimo_w			number(15,2);
nr_titulo_w			number(10);
nr_dt_liquidacao_w		number(10);
cont_w				number(10);
cd_ocorrencia_w			number(10);
nr_seq_ocorrencia_ret_w		number(10);
nr_seq_motivo_desc_w		number(10);
nr_seq_tit_rec_cobr_w		number(10);
nr_seq_tf_cobr_desp_w		number(10);
nr_seq_trans_desp_banco_w	number(10);
cd_centro_custo_desc_w		number(8);
cd_banco_cobr_w			number(5);
cd_estabelecimento_w		number(4);
cd_banco_w			number(3);
dt_liquidacao_w			date;

cursor C01 is
	select	trim(substr(ds_string,38,25)),			-- ds_titulo_w
		to_number(substr(ds_string,153,13))/100,	-- vl_cobranca_w	VL Titulo e Vl Cobran�a
		to_number(substr(ds_string,267,13))/100,	-- vl_juros_w
		to_number(substr(ds_string,241,13))/100,	-- vl_desconto_w
		to_number(substr(ds_string,228,13))/100,	-- vl_abatimento_w
		to_number(substr(ds_string,254,13))/100,	-- vl_liquido_w	valor calculado e Vl Credito
		to_number(substr(ds_string,176,13))/100,	-- vl_tarifa_cobranca_w
		nvl(somente_numero(substr(ds_string,296,6)),0),	-- nr_dt_liquidacao_w
		substr(ds_string,109,2),			-- cd_ocorrencia_w
		trim(substr(ds_string,63,7)) nr_nosso_numero,	-- nr_nosso_numero_w
		to_number(substr(ds_string,280,13))/100		-- vl_acrescimo_w	Acrescimos
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= '1'
	and	(nvl(ie_separa_carteira_w,'N') = 'N' or cd_carteira_w is null or trim(substr(ds_string,83,3)) = cd_carteira_w);

begin
select	max(to_number(substr(ds_string, 77, 3)))
into	cd_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string, 1, 1)	= '0';

select	max(a.cd_banco),
	max(b.cd_carteira),
	max(a.cd_estabelecimento)
into	cd_banco_cobr_w,
	cd_carteira_w,
	cd_estabelecimento_w
from	banco_carteira		b,
	cobranca_escritural	a
where	a.nr_sequencia		= nr_seq_cobr_escrit_p
and	a.nr_seq_carteira_cobr	= b.nr_sequencia(+);

if	(cd_banco_w <> cd_banco_cobr_w) then
	/* O banco do arquivo � diferente do banco da cobran�a no sistema!
	Posi��o do c�digo do banco no arquivo: 77 - 79 */
	wheb_mensagem_pck.exibir_mensagem_abort(226931);
end if;

select	max(a.nr_seq_tf_cobr_desp),
	max(a.nr_seq_trans_desp_banco)
into	nr_seq_tf_cobr_desp_w,
	nr_seq_trans_desp_banco_w
from	parametro_contas_receber	a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

obter_param_usuario(815,5,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_separa_carteira_w);

open C01;
loop
fetch C01 into	
	ds_titulo_w,
	vl_cobranca_w,
	vl_juros_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_tarifa_cobranca_w,
	nr_dt_liquidacao_w,
	cd_ocorrencia_w,
	nr_nosso_numero_w,
	vl_acrescimo_w;
exit when C01%notfound;
	begin
	select	max(nr_titulo),
		nvl(max(vl_saldo_titulo), 0)
	into	nr_titulo_w,
		vl_saldo_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	if	(nr_titulo_w is null) then
		if	(nr_nosso_numero_w is not null) then
			select	max(a.nr_titulo),
				nvl(max(a.vl_saldo_titulo), 0)
			into	nr_titulo_w,
				vl_saldo_titulo_w
			from	titulo_receber a
			where	somente_numero(a.nr_nosso_numero)	= somente_numero(nr_nosso_numero_w);
		else
			select	max(a.nr_titulo),
				nvl(max(a.vl_saldo_titulo), 0)
			into	nr_titulo_w,
				vl_saldo_titulo_w
			from	titulo_receber a
			where	trim(a.nr_nosso_numero)	= ds_titulo_w;
		end if;
	end if;

	/* Se encontrou o t�tulo importa, sen�o grava no log */
	if	(nr_titulo_w is not null) then
		ie_tipo_carteira_w	:= obter_tipo_carteira_regra(nr_titulo_w);

		update	titulo_receber
		set	ie_tipo_carteira	= ie_tipo_carteira_w
		where	nr_titulo		= nr_titulo_w
		and	ie_tipo_carteira	is null;

		select 	max(a.nr_sequencia)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret	a
		where	a.cd_banco	= cd_banco_cobr_w
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		select	max(a.cd_centro_custo),
			max(a.nr_seq_motivo_desc),
			max(a.cd_pessoa_fisica),
			max(a.cd_cgc)
		into	cd_centro_custo_desc_w,
			nr_seq_motivo_desc_w,
			cd_pessoa_fisica_desc_w,
			cd_cgc_desc_w
		from	titulo_receber_liq_desc	a
		where	a.nr_titulo	= nr_titulo_w
		and	a.nr_bordero	is null
		and	a.nr_seq_liq	is null;

		if	(nvl(trim(nr_dt_liquidacao_w),0) <> 0) then
			dt_liquidacao_w	:= to_date(to_char(nr_dt_liquidacao_w, '000000'),'ddmmyy');
		end if;

		select	count(1)
		into	cont_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w
		and	rownum		= 1;

		if	(cont_w = 0) then
			/* O t�tulo a receber nr_titulo_w retornado no arquivo n�o existe na base! */
			wheb_mensagem_pck.exibir_mensagem_abort(226932,'NR_TITULO_W='||nr_titulo_w);
		end if;

		if	(nr_seq_tf_cobr_desp_w		is null) and
			(nr_seq_trans_desp_banco_w	is null) then
			vl_tarifa_cobranca_w	:= 0;
		end if;

		/*if	(nvl(vl_liquido_w,0) > nvl(vl_cobranca_w,0)) then
			vl_acrescimo_w	:= nvl(vl_liquido_w,0) - nvl(vl_cobranca_w,0);
			vl_liquido_w	:= nvl(vl_cobranca_w,0);
		end if;*/
		
		if	(nvl(vl_acrescimo_w,0) = 0) and
			(nvl(vl_liquido_w,0) > nvl(vl_cobranca_w,0)) then
			vl_acrescimo_w	:= abs(nvl(vl_liquido_w,0) - nvl(vl_cobranca_w,0));
			
		elsif	(nvl(vl_acrescimo_w,0) = 0) and
			(nvl(vl_liquido_w,0) > nvl(vl_saldo_titulo_w,0)) then
			vl_acrescimo_w	:= abs(nvl(vl_liquido_w,0) - nvl(vl_saldo_titulo_w,0));
		end if;

		select	titulo_receber_cobr_seq.nextval
		into	nr_seq_tit_rec_cobr_w
		from	dual;

		insert into titulo_receber_cobr
			(nr_sequencia,
			nr_titulo,
			cd_banco,
			vl_cobranca,
			vl_desconto,
			vl_acrescimo,
			vl_despesa_bancaria,
			vl_liquidacao,
			vl_juros,
			dt_liquidacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_ocorrencia_ret,
			vl_saldo_inclusao,
			vl_multa,
			nr_seq_motivo_desc,
			cd_centro_custo_desc)
		values	(nr_seq_tit_rec_cobr_w,
			nr_titulo_w,
			cd_banco_cobr_w,
			nvl(vl_cobranca_w,0),
			nvl(vl_desconto_w,0) + nvl(vl_abatimento_w,0),
			nvl(vl_acrescimo_w,0),
			nvl(vl_tarifa_cobranca_w,0),
			nvl(vl_liquido_w,0),
			nvl(vl_juros_W,0),
			nvl(dt_liquidacao_w,sysdate),
			sysdate,
			nm_usuario_p,
			nr_seq_cobr_escrit_p,
			nr_seq_ocorrencia_ret_w,
			nvl(vl_saldo_titulo_w,0),
			0,
			nr_seq_motivo_desc_w,
			cd_centro_custo_desc_w);

		insert into titulo_rec_cobr_desc
			(cd_cgc,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			nr_seq_tit_rec_cobr,
			nr_sequencia)
		values	(cd_cgc_desc_w,
			cd_pessoa_fisica_desc_w,
			sysdate,
			nm_usuario_p,
			nr_seq_tit_rec_cobr_w,
			titulo_rec_cobr_desc_seq.nextval);

	else
		insert into log_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');
		
		if	(nr_seq_cobr_escrit_p is not null) then
			insert into cobranca_escrit_log
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_cobranca,
				nr_titulo,
				nr_seq_ocorrencia_ret,
				nr_nosso_numero,
				vl_saldo_titulo,
				ds_log)
			values	(cobranca_escrit_log_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_cobr_escrit_p,
				nr_titulo_w,
				nr_seq_ocorrencia_ret_w,
				nr_nosso_numero_w,
				vl_saldo_titulo_w,
				'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');
		end if;
	end if;
	end;
end loop;
close C01;

commit;

end seisa_gerar_ret_cobr_sant_400;
/