create or replace
procedure gerar_rel_nota_fiscal_venc(
			nm_usuario_p		varchar2,
			ie_tipo_data_p		varchar2,
			dt_inicio_p		date,
			dt_fim_p			date,
			cd_operacao_p		varchar2,
			cd_estabelecimento_p	number
			) is

nr_count_w		number(10,0);
cd_cgc_emitente_w	varchar2(14);
ds_razao_social_w		pessoa_juridica.ds_razao_social%type;
nr_nota_fiscal_w		varchar2(255);
nr_sequencia_nf_w		number(10,0);
dt_vencimento_w		varchar2(20);
vl_vencimento_w		number(13,2);
nr_sequencia_w		number(10,0);
nr_titulo_pagar_w		number(10,0);

Cursor C01 is
select	a.cd_cgc_emitente,
	b.nr_titulo_pagar,
	c.ds_razao_social,
	a.nr_nota_fiscal,
	a.nr_sequencia_nf,
	to_char(b.dt_vencimento,'dd/mm/yyyy hh24:mi:ss'),
	b.vl_vencimento,
	a.nr_sequencia
from	nota_fiscal_venc b,
	pessoa_juridica c,
	nota_fiscal a
where	c.cd_cgc = a.cd_cgc_emitente
and	a.nr_sequencia = b.nr_sequencia
and	a.ie_situacao  = 1
and	decode(ie_tipo_data_p,'0',b.dt_vencimento,'1',a.dt_atualizacao,'2',a.dt_emissao,'3',a.dt_entrada_saida,'4',a.dt_atualizacao_estoque) between dt_inicio_p and fim_dia(dt_fim_p)
and	((a.cd_operacao_nf = cd_operacao_p) or (nvl(cd_operacao_p,'0') = '0'))
and	a.cd_estabelecimento = cd_estabelecimento_p
order by	b.dt_vencimento;

begin

delete from w_gerar_rel_nf_venc where nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into
	cd_cgc_emitente_w,
	nr_titulo_pagar_w,
	ds_razao_social_w,
	nr_nota_fiscal_w,
	nr_sequencia_nf_w,
	dt_vencimento_w,
	vl_vencimento_w,
	nr_sequencia_w;	
exit when C01%notfound;
	begin
	
	insert into w_gerar_rel_nf_venc(nm_usuario,
			cd_cgc_emitente,
			nr_titulo_pagar,
			ds_razao_social,
			nr_nota_fiscal,
			nr_sequencia_nf,
			dt_vencimento,
			vl_vencimento,
			nr_sequencia)
		values(nm_usuario_p,
			cd_cgc_emitente_w,
			nr_titulo_pagar_w,
			ds_razao_social_w,
			nr_nota_fiscal_w,
			nr_sequencia_nf_w,
			to_date(dt_vencimento_w,'dd/mm/yyyy hh24:mi:ss'),
			vl_vencimento_w,
			nr_sequencia_w);
						
	end;
end loop;
close C01;

commit;

end gerar_rel_nota_fiscal_venc;
/
