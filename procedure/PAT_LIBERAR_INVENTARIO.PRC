create or replace
procedure pat_liberar_inventario(	nr_sequencia_p	number,
				nm_usuario_p	varchar2) is
begin

update	pat_inventario
set	dt_liberacao	= sysdate,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;


commit;

end pat_liberar_inventario;
/