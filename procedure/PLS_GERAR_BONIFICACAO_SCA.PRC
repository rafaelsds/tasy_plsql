create or replace
procedure pls_gerar_bonificacao_sca
			(	nr_seq_segurado_p	number,
				nr_seq_plano_benef_p	number,
				ie_acao_p		varchar2,
				nr_seq_simulacao_p	number,
				nm_usuario_p		varchar2) is

ds_regra_w		Varchar2(150);
nr_seq_plano_w		number(10);
nr_seq_bonificacao_w	number(10);
ie_tipo_contratacao_w	pls_plano.ie_tipo_contratacao%type;

Cursor C01 is
	select	ds_regra,
		a.nr_seq_plano,
		b.nr_seq_bonificacao
	from	pls_regra_lanc_automatico	a,
		pls_regra_lanc_aut_item		b
	where	b.nr_seq_regra	= a.nr_sequencia
	and	a.ie_evento	= 2
	and	a.ie_situacao	= 'A'
	and	b.ie_situacao	= 'A'
	and	(a.nr_seq_plano= nr_seq_plano_benef_p or a.nr_seq_plano is null)
	and	((a.ie_tipo_contratacao = ie_tipo_contratacao_w) or (a.ie_tipo_contratacao is null))
	and	a.ie_acao_regra	= ie_acao_p
	and	sysdate between nvl(b.dt_inicio_vigencia,sysdate) and nvl(b.dt_fim_vigencia,sysdate)
	and	sysdate between nvl(a.dt_inicio_vigencia,sysdate) and nvl(a.dt_fim_vigencia,sysdate);

begin

select	max(ie_tipo_contratacao)
into	ie_tipo_contratacao_w
from	pls_plano
where	nr_sequencia = nr_seq_plano_benef_p;

open C01;
loop
fetch C01 into	
	ds_regra_w,
	nr_seq_plano_w,
	nr_seq_bonificacao_w;
exit when C01%notfound;
	begin	
	if	(ie_acao_p = 1) then
		insert	into	pls_bonificacao_vinculo
		(	nr_sequencia, nr_seq_bonificacao, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_segurado)
		values (pls_bonificacao_vinculo_seq.nextval, nr_seq_bonificacao_w, sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_segurado_p);
        elsif	(ie_acao_p = 6) then		
		insert	into	pls_bonificacao_vinculo
		(	nr_sequencia,nr_seq_bonificacao,dt_atualizacao,
			nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			nr_seq_segurado_simul)
		values (pls_bonificacao_vinculo_seq.nextval,nr_seq_bonificacao_w,sysdate,
			nm_usuario_p,sysdate,nm_usuario_p,
			nr_seq_segurado_p);	
	end if;
	end;
end loop;
close C01;

commit;

end pls_gerar_bonificacao_sca;
/