create or replace 
procedure inserir_protocolo_conta_bpa(
				nr_protocolo_p			varchar2,
				nr_seq_protocolo_p			number,
				nr_interno_conta_p			number,
				dt_mesano_referencia_p		date,
				mesano_p			varchar2,
				nm_usuario_p			varchar2) is
begin
	if	(nr_interno_conta_p is not null) and 
		(nr_seq_protocolo_p is not null) and 
		(nr_protocolo_p is not null) then
		if (mesano_p = 'S') then
			update	conta_paciente 
			set	dt_atualizacao = sysdate, 
				nm_usuario = nm_usuario_p, 
				nr_protocolo = nr_protocolo_p, 
				nr_seq_protocolo = nr_seq_protocolo_p,
				dt_mesano_referencia = dt_mesano_referencia_p
			where	nr_interno_conta = nr_interno_conta_p;
			commit;
		else
			update	conta_paciente 
			set	dt_atualizacao = sysdate, 
				nm_usuario = nm_usuario_p, 
				nr_protocolo = nr_protocolo_p, 
				nr_seq_protocolo = nr_seq_protocolo_p 
			where	nr_interno_conta = nr_interno_conta_p;
			commit;
		end if;
	end if;
end inserir_protocolo_conta_bpa;
/