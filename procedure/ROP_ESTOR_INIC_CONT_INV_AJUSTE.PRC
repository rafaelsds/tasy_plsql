create or replace
procedure rop_estor_inic_cont_inv_ajuste(	nr_sequencia_p		number,
						nm_usuario_p		Varchar2) is 

begin

delete from rop_inv_ajuste_roupa
where nr_seq_inv_setor = nr_sequencia_p;

update	rop_inv_ajuste_setor
set	dt_inicio_contagem	= null,
	nm_usuario_inicio_cont	= null
where	nr_sequencia		= nr_sequencia_p;


commit;

end rop_estor_inic_cont_inv_ajuste;
/