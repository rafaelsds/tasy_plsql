create or replace
procedure atualizar_atend_paciente(
		nr_atendimento_p	number,
		ie_opcao_p		varchar2,
		nm_usuario_p		varchar2) is
/*
ie_opcao:

VPA - DT_VER_PREV_ALTA
*/
begin
if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	if	(ie_opcao_p = 'VPA') then
		begin
		update	atendimento_paciente
		set	dt_ver_prev_alta	= sysdate,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_atendimento		= nr_atendimento_p;
		end;
	end if;
	end;
end if;
commit;
end atualizar_atend_paciente;
/