create or replace
procedure Gedipa_Gerar_Mensagem(  nr_seq_processo_p	number,
				nr_seq_area_prep_p	number,
				ie_evento_p		varchar2,
				nm_usuario_p		Varchar2) is 

ds_mensagem_w		varchar2(4000);
ie_condicao_w		varchar2(2);
ie_evento_w		varchar2(2);
nr_seq_mensagem_w	number(10,0);
qt_kit_w		number(10,0);
cd_kit_material_w	number(10,0);

cursor c01 is
select	ds_mensagem,
	ie_condicao,
	cd_kit_material
from	adep_proc_regra_mensagem
where	((ie_evento		= ie_evento_p) or
	 (ie_evento		is null));
	 
cursor c02 is
select	ds_mensagem,
	ie_condicao,
	cd_kit_material,
	ie_evento
from	adep_proc_regra_mensagem;

begin

if	(nvl(ie_evento_p,'XX') <> 'XX') then

	delete from adep_processo_mensagem
	where	nm_usuario	= nm_usuario_p
	and	ie_evento	= ie_evento_p;

	open c01;
	loop
	fetch c01 into
		ds_mensagem_w,
		ie_condicao_w,
		cd_kit_material_w;
	exit when c01%notfound;

		if	(ie_condicao_w	= 'K') then
			select	count(*)
			into	qt_kit_w
			from	prescr_material c,
				prescr_mat_hor b,
				adep_processo a
			where	a.nr_sequencia		= b.nr_seq_processo
			and	c.nr_sequencia		= b.nr_seq_material
			and	c.nr_prescricao		= b.nr_prescricao
			and	nvl(b.nr_seq_area_prep,0)	= nvl(nr_seq_area_prep_p,nvl(b.nr_seq_area_prep,0))
			and	c.cd_kit_material	= cd_kit_material_w
			and	a.nr_sequencia		= nr_seq_processo_p
			and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';
		
			if	(nvl(qt_kit_w,0)	> 0) then
				select	adep_processo_mensagem_seq.nextval
				into	nr_seq_mensagem_w
				from	dual;

				insert into adep_processo_mensagem
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_processo,
					ds_mensagem,
					ie_evento)
				values	(nr_seq_mensagem_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_processo_p,
					ds_mensagem_w,
					ie_evento_p);
			end if;
			
		end if;

	end loop;
	close c01;
else
	delete from adep_processo_mensagem
	where	nm_usuario	= nm_usuario_p
	and	ie_evento	is null;
	
	open c02;
	loop
	fetch c02 into
		ds_mensagem_w,
		ie_condicao_w,
		cd_kit_material_w,
		ie_evento_w;
	exit when c02%notfound;
	
		if	(ie_condicao_w	= 'K') then
			select	count(*)
			into	qt_kit_w
			from	prescr_material c,
				prescr_mat_hor b,
				adep_processo a
			where	a.nr_sequencia		= b.nr_seq_processo
			and	c.nr_sequencia		= b.nr_seq_material
			and	c.nr_prescricao		= b.nr_prescricao
			and	c.cd_kit_material	= cd_kit_material_w
			and	a.nr_sequencia		= nr_seq_processo_p
			and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';
			
			if	(nvl(qt_kit_w,0)	> 0) then
				select	adep_processo_mensagem_seq.nextval
				into	nr_seq_mensagem_w
				from	dual;

				insert into adep_processo_mensagem
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_processo,
					ds_mensagem,
					ie_evento)
				values	(nr_seq_mensagem_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_processo_p,
					ds_mensagem_w,
					null);
			end if;
		end if;
		
	end loop;
	close c02;

end if;

commit;

end Gedipa_Gerar_Mensagem;
/