create or replace
procedure sug_comunic_historico(
			nr_sequencia_p			number,
			ds_perfil_adicional_p		varchar2,
			ds_setor_adicional_p		varchar2,
			nm_usuario_destino_p		varchar2,
			nr_seq_grupo_usuario_p		varchar2,
			nm_usuario_p			varchar2,
			nr_seq_grupo_resposta_p		number default 0) is
			
/* Campos C.I */
nr_seq_classif_w		number(10,0);
nm_usuario_grupo_w		varchar2(15);
ds_usuarios_destino_w		varchar2(4000) := '';
ds_titulo_w			varchar2(255);
ds_comunicado_w			varchar2(2000);

/* Campos Atividade */
nr_sequencia_w			number(10);
nr_seq_sugestao_w		number(10);
ds_historico_w			varchar2(32000);
texto_comunic_w			varchar2(32000);
ds_classif_w			varchar2(255);
ds_pos_inicio_rtf_w		number(10);
cd_estabelecimento_w		number(4);
	
cursor c01 is
	select	distinct 
		nm_usuario_grupo
	from	usuario_grupo
	where	nr_seq_grupo	= nr_seq_grupo_usuario_p
	and	ie_situacao	= 'A';

cursor	c02 is
	select	distinct nm_usuario_grupo
	from	sug_grupo_usuario
	where	nr_seq_grupo = nr_seq_grupo_resposta_p;
	
begin
ds_usuarios_destino_w := nm_usuario_destino_p;

if	(nvl(nr_seq_grupo_usuario_p,0) > 0) then
	open C01;
	loop
	fetch C01 into	
		nm_usuario_grupo_w;
	exit when C01%notfound;
		begin
		ds_usuarios_destino_w := substr(ds_usuarios_destino_w || nm_usuario_grupo_w || ', ',1,4000);
		end;
	end loop;
	close C01;
end if;

if (nvl(nr_seq_grupo_resposta_p,0) <> 0) then
	open C02;
	loop
	fetch C02 into	
		nm_usuario_grupo_w;
	exit when C02%notfound;
		begin
		if (ds_usuarios_destino_w is not null) then
			ds_usuarios_destino_w := substr(ds_usuarios_destino_w || ', ' || nm_usuario_grupo_w || ', ',1,4000);
		else
			ds_usuarios_destino_w := substr(ds_usuarios_destino_w || nm_usuario_grupo_w || ', ',1,4000);
		end if;
		end;
	end loop;
	close C02;
end if;

if	(nvl(nr_sequencia_p, 0) > 0) then
	begin
	
	select	ds_historico,
		nr_seq_sugestao
	into	ds_historico_w,
		nr_seq_sugestao_w
	from	sug_sugestao_hist
	where	nr_sequencia = nr_sequencia_p;
	
	select	nr_sequencia,
		obter_descricao_padrao('SUG_CLASSIFICACAO','DS_CLASSIFICACAO',nr_seq_classif) ds_clssif,
		cd_estabelecimento
	into	nr_sequencia_w,
		ds_classif_w,
		cd_estabelecimento_w
	from	sug_sugestao
	where	nr_sequencia = nr_seq_sugestao_w;
	
	ds_titulo_w	:= substr(wheb_mensagem_pck.get_texto(305675),1,255);
	
	ds_pos_inicio_rtf_w	:= instr(ds_historico_w,'lang1046')+8;
	texto_comunic_w 	:= substr(ds_historico_w,1,ds_pos_inicio_rtf_w) || 'fs20 ';

	texto_comunic_w	:=	substr(texto_comunic_w	|| substr(wheb_mensagem_pck.get_texto(305675),1,100) || ' \par ' ||
						substr(wheb_mensagem_pck.get_texto(318442),1,30) || nr_sequencia_w ||' \par '||
						substr(wheb_mensagem_pck.get_texto(318443),1,30) || ds_classif_w,1,32000);

	texto_comunic_w := texto_comunic_w || '\par \par '|| substr(wheb_mensagem_pck.get_texto(318444),1,30) || substr(ds_historico_w,ds_pos_inicio_rtf_w,length(ds_historico_w));

	if	((nvl(ds_usuarios_destino_w, 'X') <> 'X') or
		(nvl(ds_perfil_adicional_p, 'X') <> 'X') or
		(nvl(ds_setor_adicional_p, 'X') <> 'X')) then
		begin
		
		Obter_Param_Usuario(2500, 12, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, nr_seq_classif_w);

		insert into comunic_interna(
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			dt_liberacao,
			ds_perfil_adicional,
			ds_setor_adicional)
		values(	sysdate,
			ds_titulo_w,
			texto_comunic_w,
			nm_usuario_p,
			sysdate,
			'N',
			ds_usuarios_destino_w,
			comunic_interna_seq.nextval,
			'N',
			nr_seq_classif_w,
			sysdate,
			ds_perfil_adicional_p,
			ds_setor_adicional_p);
		commit;
		end;
	end if;
	end;
end if;

end sug_comunic_historico;
/