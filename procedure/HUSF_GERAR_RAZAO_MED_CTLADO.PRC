create or replace
PROCEDURE HUSF_Gerar_Razao_Med_Ctlado(               
				dt_inicial_p  		date,                                       
				dt_final_p			date,                                                               
				dt_movto_inic_p		Date,                         
				dt_movto_fim_p		Date,                                
				cd_estabelecimento_p	number,                                
				cd_setor_atendimento_p	number,
				ie_gerar_saldo_p		Varchar2,
				ie_imprime_local_p		Varchar2,                    
				ie_imprime_endereco_p	Varchar2,                                                                         
				ie_imprime_setor_p		Varchar2,                                
				ie_imprime_lote_p		Varchar2,                                
				ie_imprime_movimento_p	varchar2,                                
				ie_analitico_p		Varchar2,
				ie_desconsidera_transfer_p	varchar2,                                                                 
				nm_usuario_p		Varchar2,
				cd_local_estoque_p	Varchar2,                     
				cd_medicamento_p		Number,                                                                 
				ie_imprime_centro_p	varchar2,
				cd_centro_custo_p		varchar2)   IS                                                                                                                                                    
nr_sequencia_w			number(10,0);
nr_documento_w			varchar2(22);    
nr_sequencia_item_docto_w	number(22,0);          
nr_sequencia_docto_w		movimento_estoque.nr_sequencia_documento%type;
nr_crm_w			varchar2(80);                    
cd_medico_w			varchar2(10);                 
cd_material_w			number(22,0);               
cd_material_ww			number(22,0) := 0;           
dt_movimento_w			date;                      
dt_movimento_ant_w		date;                    
cd_classificacao_w		varchar2(80);               
cd_dcb_w			varchar2(80);                      
ds_dcb_w			varchar2(80);                      
ds_local_estoque_w		varchar2(80);             
ie_tipo_w			varchar2(10);                        
ds_observacao_w			varchar2(255);                
qt_entrada_w			number(22,4);                     
qt_saida_w			number(22,4);                       
qt_perda_w			number(22,4);                        
qt_transf_w			number(22,4);                     
ie_primeira_vez_w		varchar2(1) := 'N';                                                  
qt_estoque_w			number(22,4);                       
qt_saldo_w			number(22,4);                          
ds_historico_w			Varchar2(250);                      
ie_origem_documento_w		varchar2(25);                
dt_inicio_w			Date;                                 
dt_fim_w			Date;                                     
dt_saldo_w			Date;                                   
ie_entrada_saida_w		Varchar2(10);                  
ie_tipo_requisicao_w		varchar2(30);                  
nr_coluna_w			Number(10,0);                           
dt_movto_inic_w			Date;                            
dt_movto_fim_w			Date;                             
qt_nota_estorno_w		number(5);                      
ds_situacao_nota_w		varchar2(50);                   
dt_inicio_compara_w		Date;                            
dt_anterior_w			date;                                
dt_saldo_dia_w			date;                               
qt_acumulado_dia_w		number(22,4) := 0;               
dt_validade_w			Date;                                
cd_lote_w			varchar2(20);                           
cd_operacao_estoque_w		number(3);                    
ie_analitico_w			varchar2(1);                        
nr_receita_w			number(15,0);                         
cd_centro_custo_w		number(08);                      
ds_centro_custo_w		varchar2(80);                     
cd_local_estoque_destino_w	number(5);               
cd_local_estoque_w		number(5);                      
dt_fim_movto_w			date;                              
dt_processo_w			date;                               
nr_movimento_estoque_w		number(10);                 
dt_atualizacao_w		date;   
cd_acao_w varchar2(1);	
            
Cursor C00 is                      
	select	distinct                           
		m.cd_classificacao,                         
		c.cd_dcb,                                     
		c.ds_dcb,                                     
		a.cd_material                                 
	from	dcb_medic_controlado c,                    
		medic_controlado m,                            
		saldo_estoque a                              
	where	m.cd_material		= a.cd_material                 
	and	m.nr_seq_dcb		= c.nr_sequencia               
	and	a.dt_mesano_referencia between  trunc(dt_inicio_w,'month') and dt_fim_w        
	and	a.cd_estabelecimento	= cd_estabelecimento_p                                                                   
	and	((cd_medicamento_p is null) or (cd_medicamento_p = a.cd_material))        
	order by	c.ds_dcb;                                         
cursor C01  is                           
	select	/*+ index(a MOVESTO_I4) */       
		'B' ie_tipo,                        
		trunc(a.dt_processo) dt_processo,         
		a.nr_movimento_estoque,                 
		a.dt_atualizacao,                         
		trunc(a.dt_movimento_estoque) dt_movimento_estoque,                
		0 cd_local_estoque,  
		'' local_estoque,   
		sum(decode(a.cd_acao,'1', qt_estoque, qt_estoque * -1)) qt_estoque,                                               
		'Sa�das' ds_historico,           
		'',                                 
		'',                             
		'0',                                  
		0,                                      
		0,                                      
		'',                                        
		'',                                           
		sum(obter_qt_coluna_controlado(1, b.ie_entrada_saida, b.ie_tipo_requisicao,                
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_entrada,                                                       
		sum(obter_qt_coluna_controlado(2, b.ie_entrada_saida, b.ie_tipo_requisicao,                                
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_saida,                                                         
		sum(obter_qt_coluna_controlado(3, b.ie_entrada_saida, b.ie_tipo_requisicao,                                  
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_perda,                                                         
		sum(obter_qt_coluna_controlado(4, b.ie_entrada_saida, b.ie_tipo_requisicao,                                  
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_transf,                                                        
		trunc(a.dt_validade) dt_validade,                
		a.cd_lote_fabricacao,                
		a.cd_operacao_estoque,                
		0,                
		0	cd_centro_custo,                
		0 cd_local_estoque_destino,
		max(a.cd_acao)
	from	operacao_estoque b,        
		movimento_estoque a                
	where	a.dt_mesano_referencia between dt_inicio_w and dt_fim_w        
	and	a.cd_estabelecimento		= cd_estabelecimento_p        
	and	a.cd_material_estoque	= cd_material_w        
	and	a.cd_operacao_estoque	= b.cd_operacao_estoque        
	and	(nvl(ie_desconsidera_transfer_p,'N') = 'N' or        
			ie_desconsidera_transfer_p = 'S'                        
			and b.ie_tipo_requisicao <> '2')                        
	and	b.ie_entrada_saida		= 'S'        
	and	((ie_analitico_w = 'N') and (b.ie_tipo_requisicao <> 3))        
	and	(a.cd_setor_atendimento = cd_setor_atendimento_p or nvl(cd_setor_atendimento_p,0) = 0)                                       
	and	((cd_local_estoque_p is null) or (obter_se_contido(a.cd_local_estoque, cd_local_estoque_p) = 'S'))        
	and	((cd_centro_custo_p is null) or (obter_se_contido(a.cd_centro_custo, cd_centro_custo_p) = 'S'))        
	and	((dt_movto_inic_p is null) or (a.dt_movimento_estoque between dt_movto_inic_w and dt_movto_fim_w))        
	and	a.dt_processo is not null        
	group by	trunc(a.dt_processo), trunc(a.dt_movimento_estoque), a.cd_operacao_estoque, trunc(a.dt_validade), a.cd_lote_fabricacao, a.nr_movimento_estoque, a.dt_atualizacao        
	union all        
	select	/*+ index(a MOVESTO_I4) */        
		'B' ie_tipo,                
		a.dt_processo,                
		a.nr_movimento_estoque, 
		a.dt_atualizacao,     		
		a.dt_movimento_estoque,                		              
		a.cd_local_estoque,                
		substr(obter_desc_local_estoque(a.cd_local_estoque),1,100),                
		decode(a.cd_acao,'1', qt_estoque, qt_estoque * -1) qt_estoque,                                                               
		substr(obter_hist_medic_controlado(a.nr_movimento_estoque,                
				nvl(ie_imprime_endereco_p,'N'), nvl(ie_imprime_setor_p,'N'),nvl(ie_imprime_movimento_p,'N')),1,250) ds_historico,                               
		b.ie_entrada_saida,                
		b.ie_tipo_requisicao,                
		to_char(a.nr_documento),                
		a.nr_sequencia_item_docto,                
		a.nr_sequencia_documento,                
		a.ie_origem_documento,                
		b.ie_tipo_requisicao,                
		obter_qt_coluna_controlado(1, b.ie_entrada_saida, b.ie_tipo_requisicao,                
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_entrada,                                                            
		obter_qt_coluna_controlado(2, b.ie_entrada_saida, b.ie_tipo_requisicao,                
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_saida,                                                              
		obter_qt_coluna_controlado(3, b.ie_entrada_saida, b.ie_tipo_requisicao,                                       
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_perda,                                                              
		obter_qt_coluna_controlado(4, b.ie_entrada_saida, b.ie_tipo_requisicao,                                       
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_transf,                                                            
		trunc(a.dt_validade) dt_validade,                
		a.cd_lote_fabricacao,                
		b.cd_operacao_estoque,                
		a.nr_receita,                
		a.cd_centro_custo,                
		a.cd_local_estoque_destino,
		a.cd_acao                
	from	operacao_estoque b,        
		movimento_estoque a                
	where	a.dt_mesano_referencia between  dt_inicio_w and dt_fim_w        
	and	a.cd_estabelecimento	= cd_estabelecimento_p        
	and	a.cd_material_estoque	= cd_material_w        
	and	a.cd_operacao_estoque	= b.cd_operacao_estoque        
	and	(nvl(ie_desconsidera_transfer_p,'N') = 'N' or        
			ie_desconsidera_transfer_p = 'S'                        
			and b.ie_tipo_requisicao <> '2')                        
	and	((ie_analitico_w = 'S') or (b.ie_tipo_requisicao = 3) or (b.ie_entrada_saida = 'E'))                                         
	and	(a.cd_setor_atendimento = cd_setor_atendimento_p or nvl(cd_setor_atendimento_p,0) = 0)           
	and	((cd_local_estoque_p is null) or (obter_se_contido(a.cd_local_estoque, cd_local_estoque_p) = 'S'))        
	and	((cd_centro_custo_p is null) or (obter_se_contido(a.cd_centro_custo, cd_centro_custo_p) = 'S'))        
	and	((dt_movto_inic_p is null) or (a.dt_movimento_estoque between dt_movto_inic_w and dt_movto_fim_w))        
	and	a.dt_processo is not null        
	union all                         
	select distinct                         
		'A',        
		decode(dt_movto_inic_p, null, last_Day(dt_saldo_w), trunc(dt_movto_inic_w,'dd')-1) dt_processo,                
		0,                                                 
		decode(dt_movto_inic_p, null, last_Day(dt_saldo_w), trunc(dt_movto_inic_w,'dd')-1) dt_atualizacao,                
		decode(dt_movto_inic_p, null, last_Day(dt_saldo_w), trunc(dt_movto_inic_w,'dd')-1) dt_movimento_estoque,                
		0,                
		'',                              
		0,                              
		'Saldo anterior',                
		'',                
		'',                		
		'0',                            
		0,                             
		0,                               
		'',                              
		'',                             
		0,                                  
		0,                               
		0,                               
		0,                               
		to_date(null),                   
		'',                  
		0,                              
		0,                               
		0 cd_centro_custo,                    
		0,
		'0'
	from	material a        
	where	nvl(ie_gerar_saldo_p,'S') = 'S'        
	and	cd_material = cd_material_w        
	order by dt_processo,dt_movimento_estoque, 1, nr_movimento_estoque, dt_atualizacao;  
BEGIN                                                                                 
if 	(dt_movto_fim_p is not null) then
	dt_fim_movto_w := fim_dia(fim_mes(dt_movto_fim_p));        
end if;
ie_analitico_w	:= nvl(ie_analitico_p, 'S');                                              
dt_inicio_w	:= trunc(dt_inicial_p, 'dd');
dt_fim_w		:= trunc(dt_final_p, 'dd') + 86399 / 86400;
dt_saldo_w	:= add_months(dt_inicio_w, -1);
dt_movto_inic_w	:= trunc(dt_movto_inic_p, 'dd');
dt_movto_fim_w	:= trunc(dt_movto_fim_p, 'dd') + 86399 / 86400;                                                                                            
/*Para truncar somente se nao passou param com hora*/                                                                                                                                                     
if	(dt_movto_inic_p is not null) and (to_char(dt_movto_inic_p,'hh24:mi:ss') <> '00:00:00') then
	dt_movto_inic_w	:= dt_movto_inic_p;        
	dt_movto_fim_w	:= dt_movto_fim_p;        
end if;

Delete W_Razao_Medic_Controlado;
commit;                     
OPEN C00;                                     
LOOP                                        
FETCH C00 into                                   
	cd_classificacao_w,        
	cd_dcb_w,                
	ds_dcb_w,                          
	cd_material_w;                     
EXIT when C00%notfound;
	select	sum(qt_estoque)               
	into	qt_saldo_w        
	from	saldo_Estoque        
	where	cd_estabelecimento		= cd_estabelecimento_p        
	and	cd_material 		= cd_material_w        
	and	dt_mesano_referencia 	= dt_saldo_w        
	and	((cd_local_estoque_p is null) or (obter_se_contido(cd_local_estoque, cd_local_estoque_p) = 'S'));
	ie_primeira_vez_w	:= 'S';                                        
	OPEN C01;        
	LOOP                               
	FETCH C01 into                          
		ie_tipo_w,                           
		dt_processo_w,                
		nr_movimento_estoque_w,                
		dt_atualizacao_w,                
		dt_movimento_w,                
		cd_local_estoque_w,                
		ds_local_estoque_w,                
		qt_estoque_w,                
		ds_historico_w,                
		ie_entrada_saida_w,                
		ie_tipo_requisicao_w,                
		nr_documento_w,                
		nr_sequencia_item_docto_W,                
		nr_sequencia_docto_w,                
		ie_origem_documento_w,                
		ie_tipo_requisicao_w,                
		qt_entrada_w,                
		qt_saida_w,                
		qt_perda_w,                
		qt_transf_w,                
		dt_validade_w,                
		cd_lote_w,                
		cd_operacao_estoque_w,                
		nr_receita_w,                
		cd_centro_custo_w,                
		cd_local_estoque_destino_w,
		cd_acao_w;		
	EXIT when C01%notfound;        
		select W_Razao_Medic_Controlado_seq.nextval            
		into	nr_sequencia_w           
		from	dual;                         
		nr_crm_w			:= '';                     
		ds_observacao_w			:= '';                                       
		if	(ie_origem_documento_w	= '1') then                 
			begin                                 
			select	substr(nvl(max(ds_observacao),''),1,255)                     
			into	ds_observacao_w                        
			from	nota_fiscal_item                        
			where	nr_nota_fiscal	= nr_documento_w                           
			and	nr_item_nf	= nr_sequencia_item_docto_w                           
			and	nr_sequencia_nf	= nr_sequencia_docto_w;                                              
			select	substr(obter_valor_dominio(1056, nvl(max(ie_situacao),'')),1,20)                        
			into	ds_situacao_nota_w                          
			from	nota_fiscal                             
			where	nr_nota_fiscal	= nr_documento_w                             
			and	nr_sequencia_nf	= nr_sequencia_docto_w;                                  
			ds_observacao_w	:= substr(ds_observacao_w || ' ' || ds_situacao_nota_w,1,255);                                                                                                       
			end;                              
		elsif	(ie_origem_documento_w	= '2') then                         
			select	substr(nvl(max(b.ds_observacao),max(a.ds_observacao)),1,255)                        
			into	ds_observacao_w                  
			from	item_requisicao_material b,                         
				requisicao_material a                 
			where	a.nr_requisicao = b.nr_requisicao                    
			and	a.nr_requisicao	= nr_documento_w               
			and	b.nr_sequencia	= nr_sequencia_item_docto_w;                
		elsif	(ie_origem_documento_w	= '3') and                
			(nvl(nr_documento_w,'0') <> '0') then               
			begin                      
			if	(nr_sequencia_item_docto_w 	is null) or              
				(nr_sequencia_item_docto_w	= 999) then             
				begin                     
				select	nvl(max(cd_medico_resp), ' ')                       
				into	cd_medico_w                           
				from	atendimento_paciente                          
				where	nr_atendimento	= nr_documento_w;                        
				end;                           
			else                            
				begin                          
				select	nvl(max(cd_medico), ' ')                 
				into	cd_medico_w                 
				from	prescr_medica                     
				where	nr_prescricao	= nr_documento_w;                    
				end;                          
			end if;                
			
			if	(cd_medico_w <> ' ') then                               
				select	substr(obter_crm_medico(cd_medico_w),1,20)                                
				into	nr_Crm_w                         
				from	dual;              
			end if;                         
			end;                             
		elsif	(ie_origem_documento_w	= '5') then                     
			select	substr(nvl(max(ds_observacao),''),1,255)                  
			into	ds_observacao_w                
			from  	movimento_estoque             
			where	nr_documento = nr_documento_w              
			and	cd_material = cd_material_w;            
		end if;                             
		if	(dt_movto_inic_p is null) then                       
			qt_saldo_w		:= nvl(qt_saldo_w,0) +                      
						qt_entrada_w -                                  
						qt_saida_w -                                       
						qt_perda_w -                                        
						qt_transf_w;                                         
		elsif	(dt_movto_inic_p is not null) then                                       
			begin                           
			if	(trunc(dt_movimento_w,'dd') = trunc(dt_movimento_ant_w,'dd')) then                        
				qt_acumulado_dia_w	:= qt_acumulado_dia_w +                                      
							qt_entrada_w - qt_saida_w - qt_perda_w - qt_transf_w;                                                       
			else                           
				qt_acumulado_dia_w	:= 0;                      
			end if;                                
			if	(ie_primeira_vez_w = 'S') then                      
				ie_primeira_vez_w := 'N';                                     
				qt_saldo_w	:= Obter_saldo_Diario_medic(                                           
							dt_movto_inic_p - 1,                                               
							cd_estabelecimento_p,                                                
							cd_local_estoque_p,                                                 
							cd_material_w);                              
			else                 
				qt_saldo_w		:= nvl(qt_saldo_w,0) +                                        
							qt_entrada_w -                                       
							qt_saida_w -                                               
							qt_perda_w -                                                 
							qt_transf_w;                                                        
			end if;              
			dt_movimento_ant_w	:= dt_movimento_w;        
			end;                               
		end if;                    
		if	(ie_tipo_w <> 'A') then              
			ds_historico_w	:= substr(' (' || nr_documento_w || ') ' || ds_historico_w,1,250);                                                                                                          
			if	(ie_imprime_local_p = 'S') then                
				ds_historico_w	:= substr(ds_historico_w || ' Local: ' || ds_local_estoque_w,1,250);                                                                                                     
			elsif	(ie_imprime_lote_p = 'S') and                       
				((cd_lote_w is not null) or                     
				(dt_validade_w is not null)) then                          
				ds_historico_w	:= substr(ds_historico_w || ' Lote: ' || cd_lote_w || ' - ' || 'Validade: ' || dt_validade_w,1,250);                                                                 
			end if;                             
			if	(ie_imprime_centro_p = 'S') and                                       
				(cd_centro_custo_w is not null) then                                       
				select	substr(obter_desc_centro_custo(cd_centro_custo_w),1,80)                                
				into	ds_centro_custo_w            
				from	dual;                 
				ds_historico_w	:= substr(ds_historico_w || ' Centro custo: ' || ds_centro_custo_w,1,250);                                
				end if;                                
		end if;          

		if (cd_acao_w = '2') then
		   ds_historico_w	:= substr('ESTORNO - ' || ds_historico_w,1,250);
		end if;                                
                           
		insert into w_Razao_Medic_Controlado (             
			ie_tipo,                             
			cd_estabelecimento,                        
			nr_sequencia,                                
			cd_material,                               
			cd_classificacao,                            
			cd_dcb,                                  
			ds_dcb,                                   
			dt_movimento,                             
			ds_historico,                           
			qt_entrada,                               
			qt_saida,                                 
			qt_perda,                                   
			qt_transf,                              
			qt_saldo,                                   
			nr_crm,                                      
			ds_observacao,                             
			cd_operacao_estoque,                       
			nr_receita,                       
			cd_medico_presc,                         
			cd_local_estoque,                          
			cd_local_estoque_destino,
			dt_processo)                        
		values(	ie_tipo_w,                         
			cd_estabelecimento_p,                       
			nr_sequencia_w,                          
			cd_material_w,                          
			cd_classificacao_w,                         
			cd_dcb_w,                                   
			ds_dcb_w,                                 
			dt_movimento_w,                            
			ds_historico_w,                     
			qt_entrada_w,                          
			qt_saida_w,                            
			qt_perda_w,                             
			qt_transf_w,                           
			qt_saldo_w,                               
			nr_crm_w,                       
			nvl(ds_observacao_w,''),                         
			cd_operacao_estoque_w,                        
			nr_receita_w,                          
			cd_medico_w,                        
			cd_local_estoque_w,                        
			cd_local_estoque_destino_w,
			dt_processo_w);          
	END LOOP;                             
	CLOSE C01;                                
END LOOP;                                    
CLOSE C00;                                                                                                                                                                                             
commit;                                         
END HUSF_Gerar_Razao_Med_Ctlado;
/