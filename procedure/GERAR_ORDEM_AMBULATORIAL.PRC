create or replace
PROCEDURE Gerar_ordem_ambulatorial(
          dt_referencia_p     Date,
          cd_estabelecimento_p    Number,
          ie_gerar_ordem_pac_chegada_p  varchar2,
          nm_usuario_p      Varchar2,
          ie_gera_inconsistencia_p  varchar2 default 'N',
          ds_retorno_p OUT varchar2) IS

nr_seq_atendimento_w    Number(10);
nr_seq_paciente_w   Number(10);
cd_medico_resp_w    Varchar2(10);
nr_prescricao_w     Number(14);
cd_estabelecimento_w    Number(4);
nm_paciente_w     Varchar2(60 char);
ie_gerar_prescr_w   varchar2(1);
ie_restringir_ordem_w   varchar2(1);
nr_sequencia_log_w    number(15);
nr_sequencia_inc_w    number(10);
ie_liberado_w     varchar2(1);
ie_consiste_ordem_w   varchar2(1);
ie_exibe_apto_w     varchar2(1);
ie_permite_acm_w    varchar2(1);
ds_retorno_ww     varchar2(4000);
ie_exige_lib_farma_w  varchar2(1);
nr_atendimento_w  number(10);
ds_possui_med_inativo_w varchar2(4000);

Cursor C01 is
  select  a.nr_seq_atendimento,
    a.nr_seq_paciente,
    a.nr_prescricao,
    a.nr_atendimento
  from  paciente_atendimento a
  where trunc(nvl(a.dt_real,a.dt_prevista)) = trunc(dt_referencia_p)
  and obter_se_prescr_liberada(a.nr_seq_atendimento, cd_estabelecimento_p, nm_usuario_p) = 'S'
  AND   ((a.cd_estabelecimento = cd_estabelecimento_p) OR (ie_restringir_ordem_w = 'N'))
  and ((a.dt_chegada is not null) or (ie_gerar_ordem_pac_chegada_p = 'N'))
  and   ((a.dt_apto is not null) or (nvl(ie_exibe_apto_w,'N') = 'N'))
  and   a.dt_suspensao is null
  and a.dt_cancelamento is null
  and obter_se_paciente_obito(obter_pessoa_atendimento(a.nr_atendimento,'C')) <> 'A'
  and ((ie_gera_inconsistencia_p = 'N') or
     (not exists (select 1 from w_inconsistencias_onco d  where d.nr_seq_atendimento = a.nr_seq_atendimento) ))
  union
  select  a.nr_seq_atendimento,
    a.nr_seq_paciente,
    b.nr_prescricao,
    a.nr_atendimento
  from  paciente_atendimento a,
    prescr_medica b
  where trunc(nvl(a.dt_real,a.dt_prevista)) = trunc(dt_referencia_p)
  and   b.nr_prescricao_anterior = a.nr_prescricao
  and   ((a.cd_estabelecimento = cd_estabelecimento_p) or (ie_restringir_ordem_w = 'N'))
  and ((a.dt_chegada is not null) or (ie_gerar_ordem_pac_chegada_p = 'N'))
  and   ((a.dt_apto is not null) or (nvl(ie_exibe_apto_w,'N') = 'N'))
  and   a.dt_suspensao is null
  and (obter_se_paciente_obito(obter_pessoa_atendimento(a.nr_atendimento,'C')) <> 'A')
  and ((ie_gera_inconsistencia_p = 'N') or
     (not exists (select 1 from w_inconsistencias_onco d  where d.nr_seq_atendimento = a.nr_seq_atendimento) ))
  and a.dt_cancelamento is null
	and	((b.dt_liberacao_farmacia is not null) or (nvl(ie_exige_lib_farma_w,'N') = 	'N'));

begin

select log_gera_ordem_seq.nextval
into  nr_sequencia_log_w
from dual;

insert into log_gera_ordem (
  nr_sequencia,
  nm_usuario,
  dt_inicio)
values  (nr_sequencia_log_w,
  nm_usuario_p,
  sysdate);

commit;

delete from w_inconsistencias_onco;

commit;

ie_liberado_w := 'N';

Obter_Param_Usuario(3130,55,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_prescr_w);
Obter_Param_Usuario(3130,165,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_restringir_ordem_w);
Obter_Param_Usuario(3130,169,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_ordem_w);
Obter_Param_Usuario(3130,204,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_exibe_apto_w);
Obter_Param_Usuario(3130,429,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_exige_lib_farma_w);

if  (ie_consiste_ordem_w = 'S') then
  while   (ie_liberado_w <> 'S')  loop
    begin
    select  nvl(max('N'),'S')
    into  ie_liberado_w
    from  log_gera_ordem
    where   nr_sequencia < nr_sequencia_log_w
    and   (dt_inicio +1/288) > sysdate
    and   dt_final is null;
    end;
  end loop;
end if;

begin

  OPEN C01;
  LOOP
  FETCH C01 into
    nr_seq_atendimento_w,
    nr_seq_paciente_w,
    nr_prescricao_w,
    nr_atendimento_w;
  exit when C01%notfound;
    begin

    select  cd_medico_resp,
      cd_estabelecimento,
      substr(obter_nome_pf(cd_pessoa_fisica),1,60)
    into  cd_medico_resp_w,
      cd_estabelecimento_w,
      nm_paciente_w
    from  paciente_setor
    where nr_seq_paciente = nr_seq_paciente_w;

    if  (cd_medico_resp_w is null) then
      if (ie_gera_inconsistencia_p = 'N') then
        wheb_mensagem_pck.Exibir_Mensagem_Abort(184696, 'NM_PACIENTE_W='||nm_paciente_w);
      else
        select w_inconsistencias_onco_seq.nextval
        into  nr_sequencia_inc_w
        from dual;

        insert into w_inconsistencias_onco (
          nr_seq_atendimento,
          nr_sequencia,
          nm_usuario,
          nr_atendimento,
          ds_informacao)
        values  (nr_seq_atendimento_w,
          nr_sequencia_inc_w,
          nm_usuario_p,
          nr_atendimento_w,
          substr(wheb_mensagem_pck.get_texto (184696, 'NM_PACIENTE_W='||nm_paciente_w),1,2000));
        commit;
      end if;

    end if;

    if  (nr_prescricao_w is null) and (ie_gerar_prescr_w = 'S') then
      Gerar_Prescricao_Paciente(nr_seq_atendimento_w, nm_usuario_p, cd_estabelecimento_w,null,null,'N',ds_retorno_ww);
      if (ds_retorno_ww is not null) then
        ds_possui_med_inativo_w := ds_possui_med_inativo_w || ' - ' || ds_retorno_ww;
      end if;

    end if;
    end;
  END LOOP;
  Close C01;

	Gerar_ordem_ambulatorial_Impl(dt_referencia_p, cd_estabelecimento_p, ie_gerar_ordem_pac_chegada_p, nm_usuario_p, ie_gera_inconsistencia_p);

  update  log_gera_ordem
  set   dt_final = sysdate
  where   nr_sequencia = nr_sequencia_log_w;
  commit;
exception
when others then
  update  log_gera_ordem
  set   dt_final = sysdate
  where   nr_sequencia = nr_sequencia_log_w;
  commit;
  
  wheb_mensagem_pck.Exibir_Mensagem_Abort(184695, 'ERRO='||sqlerrm);
end;

if (ds_possui_med_inativo_w is not null) then
  ds_retorno_p := substr(wheb_mensagem_pck.get_texto(214974,'DS_MED_W=' || ds_possui_med_inativo_w),1,2000);
end if;

commit;
end Gerar_ordem_ambulatorial;
/
