create or replace
procedure HD_Transfere_Maquina (nr_seq_maquina_p		number,
			nr_seq_unid_origem_p	number,
			nr_seq_unid_destino_p	number,
			nr_seq_ponto_dialise_p	number,
			dt_transferencia_p		date,
			nm_usuario_p		varchar2,
			ds_observacao_p		varchar2,
			ds_erro_p			out	varchar2) is

qt_registro_w		number(5,0);
nr_seq_ponto_atual_w	number(10,0);
nr_seq_maquina_w		number(10,0);
ie_ponto_livre_w		varchar2(1);
qt_maquina_ponto_w 	number(3);
begin

if	(nr_seq_ponto_dialise_p = 0)	 then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(281792,null);
else
	begin

	/* Guarda o ponto atual da m�quina */
	select	nr_seq_ponto
	into	nr_seq_ponto_atual_w
	from	hd_maquina_dialise
	where	nr_sequencia	= nr_seq_maquina_p;	
	
	/* Obtem se o ponto onde quer transferir est� livre */
	select	substr(hd_obter_se_ponto_livre(nr_seq_ponto_dialise_p),1,1)
	into	ie_ponto_livre_w
	from	dual;
	
	/* Limpa o ponto atual da m�quina */
	update	hd_maquina_dialise
	set	nr_seq_ponto	= null
	where	nr_sequencia	= nr_seq_maquina_p;
	
	/* Se transfer�ncia interna onde o ponto j� est� ocupado realiza a troca */
	if	(nr_seq_unid_origem_p = nr_seq_unid_destino_p) and
		(ie_ponto_livre_w = 'N') then
		
		select	count(*)
		into	qt_maquina_ponto_w
		from	hd_maquina_dialise
		where	nr_seq_ponto	= nr_seq_ponto_dialise_p;
	
		if (qt_maquina_ponto_w > 1) then			
			wheb_mensagem_pck.exibir_mensagem_abort(203100);
		end if;
		
		/* Pega a m�quina que est� no ponto atual */
		select	nr_sequencia
		into	nr_seq_maquina_w
		from	hd_maquina_dialise
		where	nr_seq_ponto	= nr_seq_ponto_dialise_p;
		
		/* Insere o ponto de di�lise */
		insert into hd_maquina_transf (
			nr_sequencia,
			dt_transferencia,
			nr_seq_maquina,
			nr_seq_unid_destino,
			nr_seq_unid_origem,
			nr_seq_ponto_origem,
			nr_seq_ponto_destino,
			cd_pf_transf,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_observacao
		) values (
			hd_maquina_transf_seq.NextVal,
			dt_transferencia_p,
			nr_seq_maquina_w,
			nr_seq_unid_destino_p,
			nr_seq_unid_origem_p,
			nr_seq_ponto_dialise_p,
			nr_seq_ponto_atual_w,
			substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ds_observacao_p
		);
		
		/* Atualiza o ponto na m�quina */
		update	hd_maquina_dialise
		set	nr_seq_ponto	= nr_seq_ponto_atual_w
		where	nr_sequencia	= nr_seq_maquina_w;
		
	end if;
	
	/* Insere o ponto de di�lise */
	insert into hd_maquina_transf (
		nr_sequencia,
		dt_transferencia,
		nr_seq_maquina,
		nr_seq_unid_destino,
		nr_seq_unid_origem,
		nr_seq_ponto_origem,
		nr_seq_ponto_destino,
		cd_pf_transf,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_observacao
	) values (
		hd_maquina_transf_seq.NextVal,
		dt_transferencia_p,
		nr_seq_maquina_p,
		nr_seq_unid_destino_p,
		nr_seq_unid_origem_p,
		nr_seq_ponto_atual_w,
		nr_seq_ponto_dialise_p,
		substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_observacao_p
	);

	/* Atualiza o ponto na m�quina */
	update	hd_maquina_dialise
	set	nr_seq_ponto	= nr_seq_ponto_dialise_p
	where	nr_sequencia	= nr_seq_maquina_p;
		
	commit;
		
	end;
	
end if;		
	
end HD_Transfere_Maquina;
/
