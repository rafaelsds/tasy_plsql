create or replace
procedure Gerar_Integracao_Conexa(nr_seq_agenda_p	number,
				  cd_paciente_p		varchar2,
				  cd_tipo_agenda_p	number,
				  cd_unidade_p		out number,
				  ds_unidade_p		out varchar2,
				  cd_codigo_pac_p	out varchar2,
				  dt_recepcao_p		out varchar2) is 

cd_estabelecimento_w	number(4,0);
cd_unidade_conexa_w	number(11,0);
nm_fantasia_estab_w	varchar2(50);
cd_pessoa_fisica_externo_w	varchar2(20);
dt_recepcao_w		varchar2(50);
cd_estabelecimento_usuario_w estabelecimento.cd_estabelecimento%type;
				  
begin

select wheb_usuario_pck.get_cd_estabelecimento
into cd_estabelecimento_usuario_w
from dual;

if	(cd_tipo_agenda_p = 3) then
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	agenda a,
		agenda_consulta b
	where	a.cd_agenda = b.cd_agenda
	and	b.nr_sequencia = nr_seq_agenda_p;
else
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	agenda a,
		agenda_paciente b
	where	a.cd_agenda = b.cd_agenda
	and	b.nr_sequencia = nr_seq_agenda_p;
end if;

if	(cd_estabelecimento_w is not null) then

	select	max(cd_unidade_conexa),
		max(nm_fantasia_estab)
	into	cd_unidade_conexa_w,
		nm_fantasia_estab_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;
	
end if;

select	max(cd_pessoa_fisica_externo)
into	cd_pessoa_fisica_externo_w
from	pessoa_fisica a,
	pf_codigo_externo b
where	b.cd_pessoa_fisica 		= a.cd_pessoa_fisica
and	b.ie_tipo_codigo_externo	= 'CO'
and (nvl(b.cd_estabelecimento,cd_estabelecimento_usuario_w) = cd_estabelecimento_usuario_w)
and	a.cd_pessoa_fisica		= cd_paciente_p;

select 	to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')
into	dt_recepcao_w
from 	dual;

cd_unidade_p		:= cd_unidade_conexa_w;	
ds_unidade_p		:= nm_fantasia_estab_w;
cd_codigo_pac_p		:= cd_pessoa_fisica_externo_w;
dt_recepcao_p		:= dt_recepcao_w;


end Gerar_Integracao_Conexa;
/