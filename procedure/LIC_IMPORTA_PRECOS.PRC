create or replace
procedure lic_importa_precos(		nr_seq_licitacao_p			number,				
				ie_opcao_material_p		varchar2,				
				nm_usuario_p			varchar2) is

/*ie_opcao_material_p
0 = C�digo do Tasy
1 = C�digo do fornecedor, dai tem que utilizar a tabela preco_pj*/				

cd_cgc_fornec_w				varchar2(14);
cd_pessoa_fisica_w			varchar2(10);
cd_estabelecimento_w			number(4);
nr_seq_lic_item_w				number(10);
nr_seq_lic_item_ww				number(10);
vl_item_w					number(15,4);
ds_marca_w				varchar2(255);
nr_seq_marca_w				number(10);
cd_material_w				number(10);
nr_seq_lote_w				number(10);
nr_seq_fornec_w				number(10);
qt_item_w				number(15,4);
nr_seq_lic_item_fornec_w			number(10);
qt_existe_w				number(10);
nr_seq_item_fornec_w			number(10);
nr_lote_compra_w				varchar2(80);
vl_original_w				number(15,4) := 0;

cursor c01 is
select	distinct
	a.cd_cgc_fornec,
	a.cd_pessoa_fisica
from	lic_w_importa_precos a
where	a.nr_seq_licitacao = nr_seq_licitacao_p
and	a.cd_cgc_fornec is not null;

cursor c02 is
select	distinct
	a.nr_seq_lic_item,
	a.vl_item,
	a.ds_marca,
	b.nr_seq_lote,
	b.nr_lote_compra
from	lic_w_importa_precos a,
	reg_lic_item b
where	a.nr_seq_lic_item	= b.nr_seq_lic_item
and	a.nr_seq_licitacao	= b.nr_seq_licitacao
and	a.nr_seq_licitacao	= nr_seq_licitacao_p
and	a.cd_material is not null
and	a.vl_item > 0
and	a.nr_seq_lic_item > 0;

begin

open C01;
loop
fetch C01 into	
	cd_cgc_fornec_w,
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_fornec_w
	from	reg_lic_fornec
	where	nr_seq_licitacao = nr_seq_licitacao_p
	and	cd_cgc_fornec = cd_cgc_fornec_w;	
	
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	reg_licitacao
	where	nr_sequencia = nr_seq_licitacao_p;
	
	if	(nr_seq_fornec_w = 0) then
	
		select	reg_lic_fornec_seq.nextval
		into	nr_seq_fornec_w
		from	dual;

		insert into reg_lic_fornec (
			nr_sequencia,
			nr_seq_licitacao,
			dt_atualizacao,
			nm_usuario,
			cd_cgc_fornec,
			cd_pessoa_fisica,
			ie_situacao)
		values(	nr_seq_fornec_w,
			nr_seq_licitacao_p,
			sysdate,
			nm_usuario_p,
			cd_cgc_fornec_w,
			cd_pessoa_fisica_w,
			'A');
	end if;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_lic_item_w,
		vl_item_w,
		ds_marca_w,
		nr_seq_lote_w,
		nr_lote_compra_w;
	exit when C02%notfound;
		begin
		
		select	nvl(max(cd_material),0)
		into	cd_material_w
		from	reg_lic_item
		where	nr_seq_licitacao = nr_seq_licitacao_p
		and	nr_seq_lic_item = nr_seq_lic_item_w;
		
		if	(cd_material_w > 0) then
		
			if	(ie_opcao_material_p = 1) then

				select	nvl(max(cd_material),0)
				into	cd_material_w
				from	preco_pj
				where	cd_cgc	= cd_cgc_fornec_w
				and	cd_item	= cd_material_w
				and	cd_estabelecimento = cd_estabelecimento_w;

				if	(cd_material_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(266226,'CD_MATERIAL=' || cd_material_w);
					--'O material ' || cd_material_w || ' do arquivo, n�o foi encontrado no v�nculado com o fornecedor. ' ||
					--'Verifique na fun��o Pessoa Jur�dica, pasta Regras, subpasta Pre�o fornecedor.');
				end if;
			end if;
			
			select	nvl(max(nr_seq_lic_item),0)
			into	nr_seq_lic_item_ww
			from	reg_lic_item
			where	cd_material		= cd_material_w
			and	nr_seq_licitacao	= nr_seq_licitacao_p;
	
			if	(nr_seq_lic_item_ww = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(266228,'CD_MATERIAL=' || cd_material_w);
				--'O material com c�digo ' || cd_material_w || ' n�o existe nesta licita��o. Verifique na pasta Itens.');
			end if;
			
			
			if	(ds_marca_w is not null) then
			
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_marca_w
				from	marca
				where	elimina_acentuacao(upper(ds_marca)) = elimina_acentuacao(upper(ds_marca_w));
	
				if	(nr_seq_marca_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(266229,'DS_MARCA=' || ds_marca_w);
					--'N�o existe nenhuma marca cadastrada no sistema com o nome ' || ds_marca_w || '.' || chr(13) || chr(10) || 
					--'Favor cadastra-la e em seguida importar novamente o arquivo.');
				else
					select	count(*)
					into	qt_existe_w
					from	material_marca
					where	cd_material = cd_material_w
					and	nr_sequencia = nr_seq_marca_w;

					if	(qt_existe_w = 0) then
				
						insert into material_marca(
							cd_material,
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							qt_prioridade,
							ie_situacao)
						values(	cd_material_w,
							nr_seq_marca_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							1,
							'A');
					end if;
				end if;
			end if;
			
			qt_item_w	:= lic_obter_qt_item(nr_seq_licitacao_p, nr_seq_lic_item_w);
			
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_lic_item_fornec_w
			from	reg_lic_item_fornec
			where	nr_seq_fornec	= nr_seq_fornec_w
			and	cd_material	= cd_material_w;
			
			if	(nr_seq_lote_w is null) then
				begin				
				if	(nr_seq_lic_item_fornec_w = 0) then				
					insert into reg_lic_item_fornec(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_fornec,
						nr_seq_lic_item,
						qt_item,
						ie_vencedor,
						vl_item,
						vl_original,
						cd_material,
						nr_seq_licitacao,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_qualificado,
						ie_qualif_lance_fornec,
						ds_marca)
					values(	reg_lic_item_fornec_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_fornec_w,
						nr_seq_lic_item_w,
						qt_item_w,
						'N',
						0,
						vl_item_w,
						cd_material_w,
						nr_seq_licitacao_p,
						sysdate,
						nm_usuario_p,
						'S',
						'S',
						ds_marca_w);		
				else
					update	reg_lic_item_fornec
					set	vl_item = vl_item_w
					where	nr_seq_fornec		= nr_seq_fornec_w
					and	nr_seq_lic_item		= nr_seq_lic_item_w;
				end if;
				end;
			else
				begin
				
				nr_seq_item_fornec_w := 0;
				
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_item_fornec_w
				from	reg_lic_item_fornec
				where	nr_seq_fornec	= nr_seq_fornec_w
				and	nr_Seq_lic_item = nr_seq_lote_w;
				
				if	(nr_seq_item_fornec_w = 0) then
					
					select	reg_lic_item_fornec_seq.nextval
					into	nr_seq_item_fornec_w
					from	dual;					
					
					insert into reg_lic_item_fornec(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_fornec,
						nr_seq_lic_item,
						qt_item,
						ie_vencedor,
						vl_item,
						vl_original,
						cd_material,
						nr_seq_licitacao,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_qualificado,
						ie_qualif_lance_fornec,
						ds_marca)
					values(	nr_seq_item_fornec_w,
						sysdate,
						nm_usuario_p,
						nr_seq_fornec_w,
						nr_seq_lote_w,
						qt_item_w,
						'N',
						0,
						vl_item_w,
						null,
						nr_seq_licitacao_p,
						sysdate,
						nm_usuario_p,
						'S',
						'S',
						ds_marca_w);
				else
					update	reg_lic_item_fornec
					set	qt_item		= qt_item + qt_item_w
					where	nr_sequencia	= nr_seq_item_fornec_w;
				end if;
				
				insert into reg_lic_item_fornec_lote(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_lic_item_fornec,
					nr_lote_compra,
					cd_material,
					qt_item,
					vl_item,
					nr_seq_licitacao,
					nr_seq_lic_item,
					ds_marca)
				values(	reg_lic_item_fornec_lote_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_item_fornec_w,
					nr_lote_compra_w,
					cd_material_w,
					qt_item_w,
					vl_item_w,
					nr_seq_licitacao_p,
					nr_seq_lic_item_w,
					ds_marca_w);
					
				select	sum(qt_item * vl_item)
				into	vl_original_w
				from	reg_lic_item_fornec_lote
				where	nr_seq_lic_item_fornec = nr_seq_item_fornec_w;
				
				update	reg_lic_item_fornec
				set	vl_original		= vl_original_w
				where	nr_sequencia	= nr_seq_item_fornec_w;
				
				end;			
			end if;
		end if;		
		end;
	end loop;
	close C02;	
	end;
end loop;
close C01;

commit;

end lic_importa_precos;
/