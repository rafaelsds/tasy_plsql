create or replace
procedure gerar_prescr_dieta_composicoes(
		nr_prescricao_p		number,
		ds_lista_check_p	varchar2,
		nm_usuario_p		varchar2,
		ds_erros_p		out varchar2) is

cd_intervalo_w		varchar2(7);
ie_agora_w		varchar2(1);
ie_dose_esp_w		varchar2(1);
ds_erro_w		varchar2(2000);
ds_lista_check_w	varchar2(2000);
ds_item_atual_w		varchar2(255);
nr_pos_virgula_w	number(10,0);
nr_pos_separador_w	number(10,0);

begin
if	(nr_prescricao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ds_lista_check_w	:= ds_lista_check_p;
	
	while	(ds_lista_check_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(ds_lista_check_w, ',');
		
		if	(nr_pos_virgula_w > 0) then
			begin
			ds_item_atual_w		:= substr(ds_lista_check_w,0,nr_pos_virgula_w-1);
			ds_lista_check_w	:= substr(ds_lista_check_w,nr_pos_virgula_w+1,length(ds_lista_check_w));
			end;
		else
			begin
			ds_item_atual_w		:= ds_lista_check_w;
			ds_lista_check_w	:= null;
			end;
		end if;
		
		if	(ds_item_atual_w is not null) then
			begin
			nr_pos_separador_w	:= instr(ds_item_atual_w, '-');
			
			if	(nr_pos_separador_w > 0) then
				begin
				ds_item_atual_w	:= substr(ds_item_atual_w,nr_pos_separador_w+1,length(ds_item_atual_w));
				
				if	(ds_item_atual_w is not null) then
					begin
					nr_pos_separador_w	:= instr(ds_item_atual_w, '-');
					
					if	(nr_pos_separador_w > 0) then
						begin
						cd_intervalo_w 	:= substr(ds_item_atual_w,0,nr_pos_separador_w-1);
						ds_item_atual_w	:= substr(ds_item_atual_w,nr_pos_separador_w+1,length(ds_item_atual_w));
						end;
					else
						begin
						cd_intervalo_w	:= ds_item_atual_w;
						ds_item_atual_w	:= null;
						end;
					end if;
					
					if	(ds_item_atual_w is not null) then
						begin
						nr_pos_separador_w	:= instr(ds_item_atual_w, '-');
					
						if	(nr_pos_separador_w > 0) then
							begin
							ie_agora_w 	:= substr(ds_item_atual_w,0,nr_pos_separador_w-1);
							ds_item_atual_w	:= substr(ds_item_atual_w,nr_pos_separador_w+1,length(ds_item_atual_w));
							end;
						else
							begin
							ie_agora_w	:= ds_item_atual_w;
							ds_item_atual_w	:= null;
							end;
						end if;
						
						if	(ds_item_atual_w is not null) then
							begin
							nr_pos_separador_w	:= instr(ds_item_atual_w, '-');
						
							if	(nr_pos_separador_w > 0) then
								begin
								ie_dose_esp_w 	:= substr(ds_item_atual_w,0,nr_pos_separador_w-1);
								ds_item_atual_w	:= substr(ds_item_atual_w,nr_pos_separador_w+1,length(ds_item_atual_w));
								end;
							else
								begin
								ie_dose_esp_w	:= ds_item_atual_w;
								ds_item_atual_w	:= null;
								end;
							end if;
							end;
						end if;						
						end;
					end if;
					end;
				end if;
				end;
			end if;
			
			if	(cd_intervalo_w = 'null') then
				cd_intervalo_w := '';
			end if;
			
			gerar_prescr_dieta_composicao(
				nr_prescricao_p,
				nm_usuario_p,
				cd_intervalo_w,
				ie_agora_w,
				ie_dose_esp_w,
				null,
				'A',
				null,
				ds_erro_w);
			end;
		end if;
		end;
		ds_erros_p := null;
		if	(ds_erro_w is not null or
			ds_erro_w <> '') then
			ds_erros_p := ds_erro_w;
		end if;
	end loop;
	end;
end if;
commit;
end gerar_prescr_dieta_composicoes;
/
