create or replace
procedure fechar_saldo_diario(
        nr_sequencia_p  number,
        nm_usuario_p    varchar2) is 

nr_lote_contabil_w      lote_contabil.nr_lote_contabil%type;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;
dt_saldo_w              caixa_saldo_diario.dt_saldo%type;

begin
if  (nm_usuario_p is not null) and
    (nr_sequencia_p is not null)    then
    begin
    update  caixa_saldo_diario
    set     dt_fechamento = sysdate,
            nm_usuario_fechamento =  nm_usuario_p,
            nm_usuario = nm_usuario_p,
            dt_atualizacao = sysdate
    where   nr_sequencia = nr_sequencia_p;
    
    begin
    select  b.cd_estabelecimento,
            a.dt_saldo
    into    cd_estabelecimento_w,
            dt_saldo_w
    from    caixa_saldo_diario a,
            caixa b
    where   a.nr_seq_caixa = b.nr_sequencia
    and     a.nr_sequencia = nr_sequencia_p;
    exception
        when others then
            cd_estabelecimento_w := null;
            dt_saldo_w := null;
    end;
    
    if (ctb_online_pck.get_modo_lote(10, cd_estabelecimento_w) = 'S') then
        begin
        nr_lote_contabil_w := ctb_online_pck.get_lote_contabil(10, cd_estabelecimento_w, dt_saldo_w, nm_usuario_p);
        
        if (nvl(nr_lote_contabil_w, 0) <> 0) then
            update  caixa_saldo_diario
            set     nr_lote_contabil = nr_lote_contabil_w
            where   nr_sequencia = nr_sequencia_p;
        end if;
        
        end;
    end if;
    
    commit;
    end;
end if;
end fechar_saldo_diario;
/
