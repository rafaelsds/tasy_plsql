create or replace
procedure consistir_inativar_material(
			cd_estabelecimento_p	number,
			cd_material_p     		number,
			nm_usuario_p		varchar2) is

vl_disp_estoque_total_w		number(15,2)	:= 0;
vl_disp_estoque_estab_w		number(15,2)	:= 0;
qt_estoque_w			number(15,2)	:= 0;
qt_regra_w			number(10,0);
cd_estabelecimento_w		number(4);
ie_permite_w			varchar2(1);
ds_estabelecimento_w		varchar2(255)	:= '';
ds_lista_estab_saldo_w		varchar2(255)	:= '';
ds_lista_estab_movto_w		varchar2(255)	:= '';
ds_lista_estab_saldo_consig_w	varchar2(255)	:= '';
qt_movimento_w			number(10);
cd_material_estoque_w		number(6);
qt_mat_conta_w			number(10);
cd_material_w			number(6);
ds_material_w			varchar2(255);
ds_regras_w			varchar2(255)	:= '';
ds_protocolo_w			varchar2(255);
ie_consiste_protocolo_w		varchar2(15);
ds_lista_protocolo_w		varchar2(800);
ds_lista_kit_w			varchar2(2000);
nr_documento_w			number(10);
ds_documento_w			varchar2(255);
ds_documento_ww			varchar2(255);
ds_lista_pendentes_w		varchar2(4000);
ie_inativa_mat_pend_compra_w	varchar2(15);
ie_consiste_diluente_w		varchar2(1)	:= 'A';
ie_consiste_comp_kit_w		varchar2(1)	:= 'A';
qt_mat_conta_convenio_w		number(10);
ie_consiste_mat_conta_w		varchar2(1)	:= 'N';
cd_material_conta_w		number(6);
ds_lista_materiais_conv_w	varchar2(2000);
ds_material_conta_w		varchar2(255);
qt_material_convenio_w		number(10,0);

cursor c01 is
select	count(*),
	cd_estabelecimento
from	movimento_estoque
where	cd_material = cd_material_estoque_w
and	dt_movimento_estoque > (sysdate - 30)
group by cd_estabelecimento;

cursor c02 is
select	sum(qt_estoque),
	cd_estabelecimento
from	saldo_estoque
where	cd_material = cd_material_estoque_w
and	dt_mesano_referencia = trunc(sysdate,'mm')
group by cd_estabelecimento;

cursor c03 is
select	sum(qt_estoque),
	cd_estabelecimento
from	fornecedor_mat_consignado
where	cd_material = cd_material_estoque_w
and	dt_mesano_referencia = trunc(sysdate,'mm')
group by cd_estabelecimento;

cursor c04 is
select	a.cd_protocolo || ' - ' || b.nm_protocolo ds_protocolo
from	protocolo_medic_material a,
		protocolo b
where	a.cd_material = cd_material_p
and		b.cd_protocolo = a.cd_protocolo;

cursor c05 is
select	nr_documento,
	ds_documento
from	compra_pendente_v2
where	cd_material = cd_material_p
ORDER BY 2;

Cursor C06 is
select	distinct 
	cd_kit_material
from	componente_kit
where	cd_material = cd_material_p
and	((cd_estab_regra is null) or (cd_estab_regra = cd_estabelecimento_p))
ORDER BY 1;

Cursor C07 is
select	distinct 	
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,255)
from	material_convenio a,
	material b
where	a.cd_material = b.cd_material
and	a.cd_material_conta = cd_material_p
and	b.ie_situacao = 'A'
order by 1;

begin

delete
from	w_material_inativo
where	cd_material = cd_material_p;

select	nvl(max(ie_inativa_mat_pend_compra),'S')
into	ie_inativa_mat_pend_compra_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;

select	cd_material_estoque
into	cd_material_estoque_w
from	material
where	cd_material = cd_material_p;

open c01;
loop
fetch c01 into
	qt_movimento_w,
	cd_estabelecimento_w;
exit when c01%notfound;
	begin
	if	(qt_movimento_w > 0) then
		
		select	substr(obter_nome_estabelecimento(cd_estabelecimento_w),1,100)
		into	ds_estabelecimento_w
		from	dual;

/*		ie_permite_w	:= 'S'; */
		ds_lista_estab_movto_w	:= substr(ds_lista_estab_movto_w || '* ' || ds_estabelecimento_w || chr(13),1,255);
	end if;
	end;
end loop;
close c01;

if	(ds_lista_estab_movto_w is not null) then
/*	ds_retorno_w := substr(	ds_retorno_w ||	'Este material possui movimentos nos �ltimos 30 dias para os estabelecimentos abaixo:' || chr(13) ||
				ds_lista_estab_movto_w || chr(13), 1,255); */

	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		'S',
		wheb_mensagem_pck.get_texto(313618),
		wheb_mensagem_pck.get_texto(313625,'DS_LISTA_ESTAB_MOVTO='||ds_lista_estab_movto_w));		

end if;

/* Verifica se h� saldo do material em algum estabelecimento */

open c02;
loop
fetch c02 into
	qt_estoque_w,
	cd_estabelecimento_w;
exit when c02%notfound;
	begin

	select	sum(obter_saldo_disp_estoque(cd_estabelecimento_w,cd_material_estoque_w,null,trunc(sysdate,'mm'))),
		substr(obter_nome_estabelecimento(cd_estabelecimento_w),1,100)
	into	vl_disp_estoque_estab_w,
		ds_estabelecimento_w
	from	dual
	order by 2;

	if	(vl_disp_estoque_estab_w <> 0 or qt_estoque_w <> 0) then

/*		ie_permite_w	:= 'N'; */
		ds_lista_estab_saldo_w	:= substr(ds_lista_estab_saldo_w || '* ' || ds_estabelecimento_w || chr(13),1,255);
	end if;
	end;

end loop;
close c02;

if	(ds_lista_estab_saldo_w is not null) then
/*	ds_retorno_w := substr(ds_retorno_w || 'Este material n�o pode ser inativado, possui saldo no(s) seguinte(s) estabelecimento(s):' || chr(13) || 
			ds_lista_estab_saldo_w || chr(13), 1,255); */
	
	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		'N',
		wheb_mensagem_pck.get_texto(313626),
		wheb_mensagem_pck.get_texto(313627,'DS_LISTA_ESTAB_SALDO='||ds_lista_estab_saldo_w));
			
end if;

/* Verifica se h� saldo consignado do material em algum estabelecimento */

open c03;
loop
fetch c03 into
	qt_estoque_w,
	cd_estabelecimento_w;
exit when c03%notfound;
	begin

	if	(qt_estoque_w <> 0) then

/*		ie_permite_w	:= 'N';	*/
		ds_lista_estab_saldo_consig_w	:= substr(ds_lista_estab_saldo_consig_w || '* ' || ds_estabelecimento_w || chr(13),1,255);
	end if;
	end;

end loop;
close c03;

if	(ds_lista_estab_saldo_consig_w is not null) then
/*	ds_retorno_w := substr( ds_retorno_w || 'Este material n�o pode ser inativado, possui saldo consignado no(s) seguinte(s) estabelecimento(s):' || chr(13) || 
			ds_lista_estab_saldo_consig_w || chr(13), 1,255); */
			
	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		'N',
		wheb_mensagem_pck.get_texto(313629),
		wheb_mensagem_pck.get_texto(313630,'DS_LISTA_ESTAB_SALDO_CONSIG='||ds_lista_estab_saldo_consig_w));
		
end if;

/* Verifica se o material est� presente em alguma regra de lan�amento autom�tico */

select	count(*)
into	qt_regra_w
from	regra_lanc_automatico a,
	regra_lanc_aut_pac b
where	a.nr_sequencia = b.nr_seq_regra
and	a.ie_situacao = 'A'
and	b.cd_material = cd_material_p;

if	(qt_regra_w > 0) then
	begin
	select	substr(obter_select_concatenado_bv('select b.nr_seq_regra from regra_lanc_automatico a, regra_lanc_aut_pac b where a.nr_sequencia = b.nr_seq_regra and a.ie_situacao = ''A'' and b.cd_material = :cd_material', 'cd_material=' || cd_material_p, ', '),1,255)
	into	ds_regras_w
	from	dual;

/*	ie_permite_w	:= 'N';
	ds_retorno_w	:= substr(ds_retorno_w || 'Este material consta em alguma regra de lan�amento autom�tico.' || chr(13) || 'Regras: ' || ds_regras_w|| chr(13), 1,255); */
	
	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		'N',
		wheb_mensagem_pck.get_texto(313631),
		wheb_mensagem_pck.get_texto(313632,'DS_REGRAS='||ds_regras_w));
	
	end;
end if;

/* Verifica se � material conta de algum outro material */

select	count(*)
into	qt_mat_conta_w
from	material_estab b,
	material a
where	a.cd_material = b.cd_material
and	b.cd_material_conta = cd_material_p
and	b.cd_estabelecimento = cd_estabelecimento_p
and	b.cd_material_conta <> a.cd_material;

if	(qt_mat_conta_w > 0) then
	select	max(a.cd_material),
		max(a.ds_material)
	into	cd_material_w,
		ds_material_w
	from	material_estab b,
		material a
	where	a.cd_material = b.cd_material
	and	b.cd_material_conta = cd_material_p
	and	b.cd_estabelecimento = cd_estabelecimento_p
	and	b.cd_material_conta <> a.cd_material;
	
/*	ie_permite_w	:= 'N';
	ds_retorno_w	:= substr(ds_retorno_w || 'Este material n�o pode ser inativado.' || chr(13) ||
				'� material conta do material ' || cd_material_w || ' ' || ds_material_w || '.' || chr(13), 1,255); */
				
	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		'N',
		wheb_mensagem_pck.get_texto(313633),
		wheb_mensagem_pck.get_texto(313634,'CD_MATERIAL='||cd_material_w||';DS_MATERIAL='||ds_material_w));				

end if;

/* Verifica se � diluente de algum material */
begin
Obter_Param_Usuario(132,134,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_diluente_w);

if	(ie_consiste_diluente_w <> 'N') then
	select	count(*)
	into	qt_regra_w
	from	material_diluicao
	where	cd_diluente = cd_material_p;

	if	(qt_regra_w > 0) then
		select	substr(obter_materiais_mat_diluicao(cd_material_p,'C'),1,255)
		into	ds_material_w
		from	dual;

		ie_permite_w		:= 'S';
		if	(ie_consiste_diluente_w = 'S') then
			ie_permite_w	:= 'N';
		end if;

		insert into w_material_inativo(
			cd_material,
			ie_permite,
			ds_titulo,
			ds_mensagem)
		values( cd_material_p,
			ie_permite_w,
			wheb_mensagem_pck.get_texto(313636),
			wheb_mensagem_pck.get_texto(313637,'DS_MATERIAL='||ds_material_w));		
	end if;
end if;
end;

/* Verifica se o material est� relacionado a algum protocolo */
begin
ie_consiste_protocolo_w	:= obter_valor_param_usuario(132, 89, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);

if	(ie_consiste_protocolo_w <> 'N') then
	begin
	select	max(cd_protocolo)
	into	qt_regra_w
	from	protocolo_medic_material
	where	cd_material = cd_material_p;
	exception when others then
		qt_regra_w	:= 0;
	end;

	if	(qt_regra_w > 0) then
		open c04;
		loop
		fetch c04 into
			ds_protocolo_w;
		exit when c04%notfound;
			ds_lista_protocolo_w := substr(ds_lista_protocolo_w || '* ' || ds_protocolo_w || chr(13),1,800);
		end loop;
		close c04;
	
		ie_permite_w		:= 'S';
		if	(ie_consiste_protocolo_w = 'S') then
			ie_permite_w	:= 'N';
		end if;

		insert into w_material_inativo(
			cd_material,
			ie_permite,
			ds_titulo,
			ds_mensagem)
		values( cd_material_p,
			ie_permite_w,
			wheb_mensagem_pck.get_texto(313638),
			wheb_mensagem_pck.get_texto(313639,'DS_LISTA_PROTOCOLO='||ds_lista_protocolo_w));
	end if;
end if;
end;

/*Consiste se o material tem compras pendentes
Retirada restri��o por cd_estabelecimento para trazer  todos os documentos de compras pendentes*/
begin
select	count(*)
into	qt_regra_w
from	compra_pendente_v2
where	cd_material = cd_material_p;
exception when others then
	qt_regra_w	:= 0;
end;
if	(qt_regra_w > 0) then
	begin
	
	open c05;
	loop
	fetch c05 into
		nr_documento_w,
		ds_documento_w;
	exit when c05%notfound;
		begin		
				
		if	(ds_documento_ww is null) then
			ds_documento_ww	:= 'X';
		end if;
		
		if	(ds_documento_ww <> ds_documento_w) then
			ds_lista_pendentes_w := substr(ds_lista_pendentes_w || ds_documento_w || ':' || chr(13) || '* ' || nr_documento_w || chr(13),1,4000);		
		else
			ds_lista_pendentes_w := substr(ds_lista_pendentes_w || '* ' || nr_documento_w || chr(13),1,4000);
		end if;
		ds_documento_ww	:= ds_documento_w;
		end;
	end loop;
	close c05;
	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		ie_inativa_mat_pend_compra_w,
		wheb_mensagem_pck.get_texto(313640),
		ds_lista_pendentes_w);
	
	end;
end if;

/*Consiste a exist�ncia de KIT material*/
begin
Obter_Param_Usuario(132,135,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_comp_kit_w);

if	(ie_consiste_comp_kit_w <> 'N') then
	begin -- Begin do exception
	select	max(cd_kit_material)
	into	qt_regra_w
	from	componente_kit
	where	cd_material = cd_material_p
	and	((cd_estab_regra is null) or (cd_estab_regra = cd_estabelecimento_p));
	exception when others then
		qt_regra_w	:= 0;
	end;
	if	(qt_regra_w > 0) then
		open C06;
		loop
		fetch C06 into
			qt_regra_w;
		exit when C06%notfound;
			begin
			select	qt_regra_w || ' - ' || ds_kit_material
			into	ds_material_w
			from	kit_material
			where	cd_kit_material = qt_regra_w;

			ds_lista_kit_w := substr(ds_lista_kit_w || '* ' || ds_material_w || chr(13),1,2000);
			end;
		end loop;
		close C06;

		ie_permite_w		:= 'S';
		if	(ie_consiste_comp_kit_w = 'S') then
			ie_permite_w	:= 'N';
		end if;

		insert into w_material_inativo(
			cd_material,
			ie_permite,
			ds_titulo,
			ds_mensagem)
		values( cd_material_p,
			ie_permite_w,
			wheb_mensagem_pck.get_texto(313641),
			wheb_mensagem_pck.get_texto(313642,'DS_LISTA_KIT='||ds_lista_kit_w));
	end if;
end if;
end;	
	
/*Consiste se o material possui v�nculo na pasta conv�nio (Material conta) de outros materiais */
begin
Obter_Param_Usuario(132,154,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_mat_conta_w);

select	count(*)
into	qt_material_convenio_w
from	material_convenio a,
	material b
where	a.cd_material = b.cd_material
and	a.cd_material_conta = cd_material_p
and	b.ie_situacao = 'A'
order by 1;


if	(nvl(ie_consiste_mat_conta_w,'N') <> 'S') and
	(nvl(qt_material_convenio_w,0) > 0) then	
	open C07;
	loop
	fetch C07 into	
		cd_material_w,
		ds_material_w;
	exit when C07%notfound;
		begin		
		ds_lista_materiais_conv_w := substr(ds_lista_materiais_conv_w || '* ' || cd_material_w || ' - ' || ds_material_w || chr(13),1,2000);		
		end;
	end loop;
	close C07;
	
	insert into w_material_inativo(
		cd_material,
		ie_permite,
		ds_titulo,
		ds_mensagem)
	values( cd_material_p,
		ie_consiste_mat_conta_w,
		wheb_mensagem_pck.get_texto(313643),
		wheb_mensagem_pck.get_texto(313644,'DS_LISTA_MATERIAIS_CONV='||ds_lista_materiais_conv_w));				
end if;			
end;			

end  consistir_inativar_material;
/