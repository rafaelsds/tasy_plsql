create or replace
procedure import_apac_cid_procedimento(cd_procedimento_p	number,
			cd_doenca_cid_p		varchar2,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: insere dado na tabela sus_procedimento_cid
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
if	(cd_procedimento_p is not null) and
	(cd_doenca_cid_p is not null) then
		insert	into sus_procedimento_cid 
		(cd_procedimento, 
		ie_origem_proced, 
		cd_doenca_cid, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_sequencia) 
values	(cd_procedimento_p, 
		7, 
		cd_doenca_cid_p, 
		sysdate, 
		nm_usuario_p, 
		sysdate, 
		nm_usuario_p, 
		sus_procedimento_cid_seq.nextval);
		commit;
end if;
end import_apac_cid_procedimento;
/