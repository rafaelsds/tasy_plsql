create or replace
procedure pls_gerar_w_benef_mens_sca
			(	dt_ano_p		Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

dt_mes_w			date;
dt_mes_final_w			date;
nr_seq_segurado_w		number(10);
dt_fim_mes_atual_w		date;
dt_rescisao_w			date;
nr_seq_w_pls_benef_w		number(10);
nr_seq_plano_w			number(10);
dt_mes_atual_w			date;
qt_idade_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
i_w				number(10);

Cursor C01 is
	select	a.nr_sequencia,
		b.nr_seq_plano
	from	pls_sca_vinculo	b,
		pls_segurado 	a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	trunc(b.dt_inicio_vigencia,'MOnth') = trunc(dt_mes_w,'MOnth')
	and	not exists (	select	1
				from	w_pls_benef_movto_mes_sca x
				where	a.nr_sequencia	= x.nr_seq_segurado
				and	x.dt_referencia	= dt_mes_w)
	and	((dt_fim_vigencia >= dt_mes_w) or (dt_fim_vigencia is null));
	
begin

dt_mes_w	:= dt_ano_p;
dt_mes_final_w	:= add_months(dt_ano_p,11);
dt_mes_atual_w	:= trunc(sysdate,'month');
i_w		:= 0;

if	(dt_mes_final_w >= sysdate) then
	dt_mes_final_w := dt_mes_atual_w;
end if;

WHILE (dt_mes_w <= dt_mes_final_w) LOOP
	BEGIN
	dt_fim_mes_atual_w	:= last_day(dt_mes_w);
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_segurado_w,
		nr_seq_plano_w;
	exit when C01%notfound;
		begin
		
		i_w	:= i_w +1;
		
		select	w_pls_benef_movto_mes_sca_seq.NextVal
		into	nr_seq_w_pls_benef_w
		from	dual;
		
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		qt_idade_w	:= obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
	
		insert into w_pls_benef_movto_mes_sca (	nr_sequencia, dt_referencia, cd_estabelecimento, nr_seq_segurado, dt_atualizacao, 
							nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_sca,qt_idade)
						values(	nr_seq_w_pls_benef_w, dt_mes_w, cd_estabelecimento_p, nr_seq_segurado_w, sysdate,
							nm_usuario_p, sysdate, nm_usuario_p, nr_seq_plano_w,qt_idade_w);
		
		if	(i_w = 1000) then
			i_w := 0;
			commit;
		end if;
		end;
	end loop;
	close C01;
	
	dt_mes_w	:= add_months(dt_mes_w,1);
	END;
END LOOP;

commit;

end pls_gerar_w_benef_mens_sca;
/
