create or replace
procedure med_excluir_inicio_consulta(nr_seq_agenda_p	number) is

begin

if	(nr_seq_agenda_p is not null) then	
	update	med_atendimento
	set	dt_inicio_consulta	= null
	where	nr_seq_agenda	= nr_seq_agenda_p;
end if;

commit;

end med_excluir_inicio_consulta;
/
