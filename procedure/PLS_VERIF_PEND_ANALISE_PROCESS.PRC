create or replace
procedure pls_verif_pend_analise_process(	cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		in usuario.nm_usuario%type,
						qt_pendencias_p		out number) is 

qt_job_em_exec_w	pls_integer;
ds_comando_job_w	varchar2(2000);
jobno 			number;

-- s�o verificados todos os regristros que n�o estejam como finalizado (feito in por quest�es de performance)
-- sessao_ativa retorna N caso n�o exista sess�o
cursor c01 is
	select	pls_util_pck.obter_se_sessao_ativa(a.nm_id_sid, a.nm_id_serial) sessao_ativa,
		a.ie_status,
		a.nr_sequencia
	from	pls_cta_analise_cons a
	where 	a.ie_status in ('X', 'I');

begin

for r_c01_w in C01 loop

	-- s� � necess�rio verificar caso a sess�o n�o estiver ativa, se estiver ativa n�o precisa verificar
	if	(r_c01_w.sessao_ativa = 'N') then
		
		-- se o status est� como Iniciado por�m a sess�o n�o est� ativa, � feito um update para P
		-- desta forma a JOB que est� em excecu��o ir� pegar este registro novamente para processar.
		if 	(r_c01_w.ie_status = 'I') then
		
			update	pls_cta_analise_cons
			set	ie_status = 'P'
			where	nr_sequencia = r_c01_w.nr_sequencia;
		end if;
	end if;
	
	-- se existe algum registro com problema, retorna 1 para que seja apresentado o bot�o 
	-- para o usu�rio verificar o que aconteceu
	if	(r_c01_w.ie_status = 'X') then

		qt_pendencias_p := 1;
	end if;
end loop;

commit;

--Job de processo de pend�ncia de an�lise
select	count(1)
into	qt_job_em_exec_w
from	job_v
where 	comando like '%PLS_PROCESSAR_ANALISE_CTA_PEND%';

if	(qt_job_em_exec_w = 0) then

	begin
		ds_comando_job_w := 'PLS_PROCESSAR_ANALISE_CTA_PEND(' || pls_util_pck.aspas_w  || 'TASY'|| pls_util_pck.aspas_w|| ',null);';
		
		pls_cta_gerar_job_regra(	'U', 'PLS_PROCESSAR_ANALISE_CTA_PEND', ds_comando_job_w,
						sysdate + 0.00001, '(SYSDATE) + 0.1/24','A');
		commit;
	exception
	when others then
		null;
	end;
end if;

end pls_verif_pend_analise_process;
/