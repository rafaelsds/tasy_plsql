create or replace
procedure pls_excluir_reembolso
			(	nr_seq_reembolso_p	number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number) is 

nr_seq_reemb_proc_w		number(10);
nr_seq_reemb_mat_w		number(10);
nr_titulo_w			number(10);
nr_lote_contabil_w		number(10);
ie_mes_fechado_w		varchar2(1);

Cursor C01 is
	select	nr_sequencia
	from	pls_reembolso_proc
	where	nr_seq_reembolso = nr_seq_reembolso_p;
	
Cursor C02 is
	select	nr_titulo
	from	titulo_pagar
	where	nr_seq_reembolso = nr_seq_reembolso_p;

begin

select	nvl(max(nr_lote_contabil),0)
into	nr_lote_contabil_w
from	pls_reembolso
where	nr_sequencia	= nr_seq_reembolso_p;

select	pls_obter_se_mes_fechado(dt_referencia,'T', cd_estabelecimento_p)
into	ie_mes_fechado_w
from	pls_reembolso
where	nr_sequencia	= nr_seq_reembolso_p;

if	(ie_mes_fechado_w	= 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort( 267002, null); /* O m�s de compet�ncia ou contabilidade do m�s est� fechado. Verifique! */
end if;

if (nr_lote_contabil_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort( 267003, null); /* N�o � poss�vel excluir o reembolso pois o mesmo j� foi contabilizado. */
end if;

delete	from pls_reembolso_hist
where	nr_seq_reembolso	= nr_seq_reembolso_p;

delete	from pls_reemb_coparticipacao
where	nr_seq_reembolso	= nr_seq_reembolso_p;

open C01;
loop
fetch C01 into	
	nr_seq_reemb_proc_w;
exit when C01%notfound;
	begin
	delete	from pls_reembolso_proc_partic
	where	nr_seq_proc_reembolso = nr_seq_reemb_proc_w;
	
	delete	from pls_reembolso_glosa
	where	nr_seq_reemb_proc = nr_seq_reemb_proc_w;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_titulo_w;
exit when C02%notfound;
	begin
	Cancelar_Titulo_Pagar(nr_titulo_w,nm_usuario_p, sysdate);
	end;
end loop;
close C02;

delete	from pls_reembolso_proc
where	nr_seq_reembolso	= nr_seq_reembolso_p;

delete	from pls_reembolso_mat
where	nr_seq_reembolso	= nr_seq_reembolso_p;

delete	from pls_reembolso
where	nr_sequencia = nr_seq_reembolso_p;

commit;

end pls_excluir_reembolso;
/