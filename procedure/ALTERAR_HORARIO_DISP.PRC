create or replace
procedure alterar_horario_disp(	nr_seq_disp_pac_p	number,
				dt_ret_prevista_p	date,
				ds_justificativa_p	varchar2,
				nm_usuario_p		varchar2) is 

dt_retirada_prev_w	date;
				
begin

if	(nr_seq_disp_pac_p > 0) and (dt_ret_prevista_p is not null) then

	select	dt_retirada_prev
	into	dt_retirada_prev_w
	from	atend_pac_dispositivo
	where	nr_sequencia		= nr_seq_disp_pac_p; 	

	update	atend_pac_dispositivo
	set	dt_retirada_prev	= dt_ret_prevista_p,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_disp_pac_p; 
	
	insert into log_dispositivo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_disp_pac,
		ds_justificativa,
		dt_vencimento_anterior)
	values(	
		log_dispositivo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_disp_pac_p,
		substr(ds_justificativa_p,1,255),
		dt_retirada_prev_w);
	
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end if;

end alterar_horario_disp;
/
