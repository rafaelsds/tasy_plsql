create or replace
procedure limpar_tabela_log_lab is

qt_dia_log_tasy_w	number(10,0);
nm_tabela_w		varchar2(50);
ds_sql_limpeza_w	varchar2(255);
ie_opcao_w		varchar2(1);
nm_tabela_destino_w	varchar2(50);
nm_campos_w		varchar2(2000);
s_array 		LT_DATE;
i			Integer := 1;
type Vetor is table of LT_DATE index by binary_integer;
Vetor_c01_w		Vetor;
dt_inicial_w		date := to_Date('01/01/1900','dd/mm/yyyy');
dt_final_w		date;
qt_limite_w		number(6);

cursor c01 is
	select	a.qt_dia_log_tasy,
		a.nm_tabela,		
		b.ds_sql_limpeza,
		a.ie_opcao,
		a.nm_tabela_destino,
		nvl(a.qt_limite, 100000)
	from	regra_limpeza_tabela a,
		tabela_sistema b
	where	a.ie_situacao = 'A'
	and	a.nm_tabela = b.nm_tabela
	order by nr_seq_delecao;

Cursor C02 is
	select *
	from	(
		select	a.dt_atualizacao
		from	log_lab a
		where	a.dt_atualizacao between dt_inicial_w and dt_final_w
	)
	where rownum < qt_limite_w;
	
begin

gravar_processo_longo('Vetor Principal', 'LIMPAR_TABELA_LOG_LAB', -1);

open c01;
loop
fetch c01 into 
	qt_dia_log_tasy_w,
	nm_tabela_w,
	ds_sql_limpeza_w,
	ie_opcao_w,
	nm_tabela_destino_w,
	qt_limite_w;
exit when c01%notfound;
	begin
	dt_final_w := sysdate - qt_dia_log_tasy_w;
	gravar_processo_longo(nm_tabela_w,'LIMPAR_TABELA_LOG_LAB',-1);
	
	/*TRATAMENTO ESPEC�FICO DEVIDO AO VOLUME DE DADOS*/
	if	(nm_tabela_w = 'LOG_LAB') then
		begin
			exec_sql_dinamico('Tasy','ALTER TABLE LOG_LAB NOLOGGING');
			open c02;
			loop
			FETCH C02 BULK COLLECT INTO s_array LIMIT 100;
				Vetor_c01_w(i) := s_array;
				i := i + 1;
			EXIT WHEN C02%NOTFOUND;
			END LOOP;
			CLOSE C02;
			
			for i in 1..Vetor_c01_w.COUNT loop
				s_array := Vetor_c01_w(i);
			
				gravar_processo_longo('Removendo LOG_LAB -> ' || i || '/' || Vetor_c01_w.COUNT,'LIMPAR_TABELA_LOG_LAB',-1);
				
				delete	from	log_lab
				where	dt_atualizacao in ( select column_value from table(cast(s_array as LT_DATE)));

				commit;
			end LOOP;
			exec_sql_dinamico('Tasy','ALTER TABLE LOG_LAB LOGGING');
		exception
		when others then
			exec_sql_dinamico('Tasy','ALTER TABLE LOG_LAB LOGGING');
		end;
		
	else
		limpar_tabela(nm_tabela_w, ds_sql_limpeza_w, qt_dia_log_tasy_w, ie_opcao_w, nm_tabela_destino_w);
	end if;
	end;
end loop;
close c01;

end limpar_tabela_log_lab;
/