create or replace
procedure enviar_frasco_pato_loc ( cd_setor_atendimento_p	number,
				   ie_itens_p			varchar2,
			           nm_usuario_p			varchar2) is 
				   
nr_seq_frasco_pato_loc_w	number;
				   
Cursor C01 is
	select	b.nr_sequencia
	from	frasco_pato_loc b
	where	b.ie_status in (10,30)
	and  	obter_se_contido(b.nr_sequencia,ie_itens_p) = 'S'
	order 	by 1;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_frasco_pato_loc_w;
exit when C01%notfound;
	begin
	
	insert into frasco_pato_loc_status (
		nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		ie_status, 
		nr_seq_frasco_pato_loc,
		cd_setor_atendimento)
	values(	frasco_pato_loc_status_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		20,
		nr_seq_frasco_pato_loc_w,
		cd_setor_atendimento_p);
		
	update	frasco_pato_loc
	set	ie_status      = 20,
		dt_atualizacao = sysdate,
		nm_usuario     = nm_usuario_p
	where	nr_sequencia   = nr_seq_frasco_pato_loc_w;
	
	end;
end loop;
close C01;

commit;

end enviar_frasco_pato_loc;
/
