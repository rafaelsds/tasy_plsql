create or replace 
procedure checkup_finalizar_etapa (
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is
 
begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	checkup_etapa
	set	dt_fim_etapa	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;
commit;
end checkup_finalizar_etapa;
/