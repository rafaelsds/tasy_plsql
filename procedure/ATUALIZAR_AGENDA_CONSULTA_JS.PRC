create or replace
procedure atualizar_agenda_consulta_js(	ds_lista_agenda_p	varchar2,
					nr_atendimento_p	number,
					nr_seq_agenda_p		number,
					nr_acao_executada_p	number,
					cd_tipo_agenda_p	number,
					ie_agenda_cons_nova_p	varchar2,
					ie_parametro_externo_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					nr_prescricao_p		out number) is 

ie_evento_w	varchar2(1);
nr_prescricao_w	number(14,0);
				
begin

if	(ds_lista_agenda_p <> '')then
	begin
	
	atualizar_lista_ag_cons_eup(nr_atendimento_p, ds_lista_agenda_p, nm_usuario_p);
	
	end;
else
	begin
	
	atualizar_atend_ag_cons_eup(nr_atendimento_p, nr_seq_agenda_p, nm_usuario_p);
	
	end;
end if;

if	(nr_acao_executada_p = 1)then
	begin
	
	atualizar_espec_med_eup_agenda	(nr_atendimento_p, nr_seq_agenda_p, nm_usuario_p);
	
	end;
end if;

if	(cd_tipo_agenda_p in(3,4))and
	(ie_agenda_cons_nova_p <> 'S')then
	begin
	
	ie_evento_w	:= obter_se_existe_evento_agenda(cd_estabelecimento_p,'GA','C');
	
	if	(ie_evento_w = 'N')and
		(ie_parametro_externo_p = 'N')then
		begin
		
		Atualizar_Status_Ag_Cons_EUP(nr_seq_agenda_p, 'O', nm_usuario_p);
		
		end;
	end if;
	
	end;
end if;

gerar_prescr_agenda_consulta(nr_seq_agenda_p, nm_usuario_p);

select  nvl(max(nr_prescricao),0) 
into	nr_prescricao_w 
from  	prescr_medica  
where 	nr_seq_agecons = nr_seq_agenda_p;

nr_prescricao_p	:= nr_prescricao_w;

commit;

end atualizar_agenda_consulta_js;
/