create or replace
procedure  inserir_prescr_mat_lib_cih(	ie_tipo_liberacao_p	varchar,
				nr_prescricao_p	number,
				nr_seq_material_p	number,
				qt_liberacao_p	number,
				nm_usuario_p	varchar
			    	) is 
begin

insert into prescr_material_lib_cih
	   		(nr_sequencia,
			 dt_atualizacao,
			 nm_usuario,
			 dt_atualizacao_nrec,
			 nm_usuario_nrec,
			 nr_prescricao,
			 nr_seq_material,
			 ie_tipo_liberacao,
			 qt_liberacao
			)
values
	  		(prescr_material_lib_cih_seq.nextval,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 nr_prescricao_p,
			 nr_seq_material_p,
			 ie_tipo_liberacao_p,
			 qt_liberacao_p
			); 			  
commit;

Atualizar_qt_total_dias_lib(nr_prescricao_p,nr_seq_material_p);

end inserir_prescr_mat_lib_cih;
/