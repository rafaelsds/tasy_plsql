create or replace
procedure Agecons_atual_vig_final_turno (	cd_agenda_p		number,
						dt_fim_vigencia_p	date,
						nm_usuario_p		Varchar2) is 
begin

if	(cd_agenda_p is not null) and
	(dt_fim_vigencia_p is not null)then
	update 	agenda_turno        
	set    	dt_final_vigencia 	= dt_fim_vigencia_p,
		nm_usuario        	= nm_usuario_p,
		dt_atualizacao    	= sysdate
	where  	cd_agenda         	= cd_agenda_p;
end if;

commit;

end Agecons_atual_vig_final_turno;
/