create or replace
procedure HD_Excluir_Dialise (  nr_seq_dialise_p        number,
                                nm_usuario_p            varchar2,
                                ds_retorno_p    out     varchar2) is

ds_erro_w                       varchar2(2000);
ie_status_preparar_w		varchar2(1);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_paciente_w		number(10);
ie_retirar_concentrado_w	varchar2(1);
nr_sequencia_dializador_w	number(10):=0;
qt_dialise_w			number(10):=0;

begin

begin
obter_param_usuario(7009, 166, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_status_preparar_w);
obter_param_usuario(7009, 199, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_retirar_concentrado_w);

/* Excluir os testes */
delete from     hd_dialisador_teste
where           nr_seq_dialise  = nr_seq_dialise_p;

if (ie_status_preparar_w = 'S') then
	/* Deixar dialisadores como preparados */
	update		hd_dializador
	set		ie_status	= 'O'
	where		nr_sequencia	in (
		select	nr_seq_dialisador
		from	hd_dialise_dialisador
		where	nr_seq_dialise	= nr_seq_dialise_p
		);
else
	/* Armazenar dialisadores */
	update		hd_dializador
	set		ie_status	= 'A'
	where		nr_sequencia	in (
		select	nr_seq_dialisador
		from	hd_dialise_dialisador
		where	nr_seq_dialise	= nr_seq_dialise_p
		);
end if;

/* Retirar concentrado */
if (ie_retirar_concentrado_w = 'S') then
	hd_retirar_concentrado_dialise(nr_seq_dialise_p, nm_usuario_p);
end if;

select 	max(cd_pessoa_fisica),
	max(nr_seq_paciente)
into	cd_pessoa_fisica_w,
	nr_seq_paciente_w
from	hd_dialise
where	nr_sequencia    = nr_seq_dialise_p;

select	nvl(max(nr_sequencia),0)  
	into	nr_sequencia_dializador_w
from	hd_dializador
where	nr_sequencia	in (
	select	nr_seq_dialisador
	from	hd_dialise_dialisador
	where	nr_seq_dialise	= nr_seq_dialise_p		
	)
and 	qt_reuso > 1; 

/* Excluir os dialisadores */
delete from     hd_dialise_dialisador
where           nr_seq_dialise  = nr_seq_dialise_p;

/* Excluir os acessos */
delete from     hd_dialise_acesso
where           nr_seq_dialise  = nr_seq_dialise_p;

/* Excluir os controles */
delete from     hd_controle
where           nr_seq_dialise  = nr_seq_dialise_p;

/* Excluir os eventos */
delete from     hd_dialise_evento
where           nr_seq_dialise  = nr_seq_dialise_p;

/* Excluir a avaliacao */
delete from     hd_prc_avaliacao
where           nr_seq_dialise  = nr_seq_dialise_p;

/*Exclurir os horarios dos medicamentos*/
delete from	prescr_mat_hor
where           nr_seq_dialise  = nr_seq_dialise_p;

/* Excluir medicamentos externos*/
delete from     hd_dialise_med_ext
where           nr_seq_dialise  = nr_seq_dialise_p;

/*Excluir concentrado da dialise*/
delete from	hd_dialise_concentrado
where		nr_seq_dialise	= nr_seq_dialise_p;

/*Excluir dialise retroativa*/
delete from	hd_dialise_retroativa
where		nr_seq_dialise	= nr_seq_dialise_p;

/* Excluir a escala de dor atribuido ao paciente */
delete from	HD_ESCALA_DOR
where		nr_seq_dialise = nr_seq_dialise_p;

update	hd_agenda_dialise
set		nr_seq_dialise = null
where	nr_seq_dialise = nr_seq_dialise_p;

/* Excluir a hemodialise */
delete from     hd_dialise
where           nr_sequencia    = nr_seq_dialise_p;

HD_atualizar_nr_hemodialise(cd_pessoa_fisica_w,nr_seq_paciente_w,nm_usuario_p);

select	count(*) 
into 	qt_dialise_w
from	hd_dialise
where	nr_sequencia = nr_seq_dialise_p;

if	(qt_dialise_w = 0 ) and
	(nr_sequencia_dializador_w is not null) then
	update	hd_dializador
	set	qt_reuso = qt_reuso - 1
	where 	nr_sequencia	= nr_sequencia_dializador_w;
	
	update	hd_dialisador_reproc 
	set		qt_reuso = qt_reuso -1
	where	nr_sequencia = (select max(x.nr_sequencia)
							from	hd_dialisador_reproc x
							where	x.nr_seq_dialisador = nr_sequencia_dializador_w);
end if;

/* Incluir regitro nos logs de exclusao do Tasy */
insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	('NR_SEQUENCIA = '||nr_seq_dialise_p,
	sysdate,
	'HD_DIALISE',
	nm_usuario_p);
	
exception
        when others then
        rollback;
	--Wheb_mensagem_pck.exibir_mensagem_abort( 193870 , 'DS_ERRO= ' || sqlerrm );
end;

commit;

end HD_Excluir_Dialise;
/
