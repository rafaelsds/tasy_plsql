create or replace
procedure HSVP_gerar_arquivo_sefip
					(	dt_inicial_p		DATE,
						dt_final_p		date,
						cd_estabelecimento_p	number) is 

ds_conteudo_w			varchar2(4000);
ie_tipo_registro_w		varchar2(2);
ds_brancos_51_w			varchar2(51);
ds_brancos_18_w			varchar2(18);
ie_final_linha			varchar2(1) := '*';
ie_tipo_remessa_w		varchar2(1) := '1';
ie_tipo_inscr_w			varchar2(1) := '1';
cd_inscr_resp_w			varchar2(14);
ds_razao_social_w		varchar2(30);
ie_competencia_w		varchar2(2);
dt_competencia_w		varchar2(6);
ie_ind_rec_fgts_w		varchar2(1) := '1';
ie_mod_arquivo_w		varchar2(1) := '1';
dt_recolhimento_w		varchar2(8);
ie_ind_rec_prev_w		varchar2(1) := '1';
dt_rec_prev_soc_w		varchar2(8);
ie_ind_rec_at_prev_w		varchar2(7);
ie_tipo_insc_forn_w		varchar2(1) := '1';
nm_pessoa_contato_w		varchar2(20);
ds_logradouro_w			varchar2(50);
ds_bairro_w			varchar2(20);
cd_cep_w			varchar2(8);
sg_uf_w				varchar2(2);
nr_telefone_w			varchar2(12);
ds_email_w			varchar2(60);
ds_zeros_36_w			varchar2(36);
ds_brancos_4_w			varchar2(4);
ds_cidade_w			varchar2(20);
cd_cnae_w			varchar2(7);
cd_centralizacao_w		varchar2(1) := '1';
ie_simples_w			varchar2(1) := '1';
ds_zeros_21_w			varchar2(21);
nr_titulo_w			number(10);
nr_seq_prestador_w		number(10);
cd_cgc_estip_w			varchar2(14);
cd_inscr_resp_estip_w		varchar2(14);
ds_razao_social_estip_w		varchar2(30);
ds_logradouro_estip_w		varchar2(50);
ds_bairro_estip_w		varchar2(20);
cd_cep_estip_w			varchar2(8);
sg_uf_estip_w			varchar2(2);
ds_cidade_estip_w		varchar2(20);
ds_zeros_45_w			varchar2(45);
ds_brancos_42_w			varchar2(42);
ds_brancos_98_w			varchar2(98);
ds_brancos_306_w		varchar2(306);
ds_marca_51_w			varchar2(51);
ie_fpas_w			varchar2(3) := '515';
cd_outra_entidade_w		varchar2(4) := '0000';
cd_pag_gps_w			varchar2(4) := '0000';
pr_isencao_filan_w		varchar2(5) := '00000';
vl_salario_familia_w		varchar2(15);
vl_salario_maternidade_w	varchar2(15);
vl_contr_desc_emp_13_w		varchar2(15);
ie_ind_neg_pos_w		varchar2(1) := '0';
vl_desc_prev_13_w		varchar2(14) := '00000000000000';
cd_banco_w			varchar2(3) := '   ';
cd_agencia_w			varchar2(4) := '    ';
cd_conta_w			varchar2(9) := '         ';
cd_pag_gps_estip_w		varchar2(4) := '    ';
vl_salario_fam_estip_w		varchar2(15) := '000000000000000';
vl_contr_desc_emp_estip_13_w	varchar2(15) := '000000000000000';
ie_ind_neg_pos_w_estip_w	varchar2(1) := '0';
vl_dev_prev_13_estip_w		varchar2(14) := '00000000000000';
vl_retencao_estip_w		varchar2(15);
vl_faturas_estip_w		varchar2(15);
cd_pessoa_fisica_w		varchar2(10);
nr_pis_pasep_w			varchar2(11);
dt_admissao_w			varchar2(8);
cd_categoria_trabalhador_w	varchar2(2);
nm_pessoa_fisica_w		varchar2(70);
cd_funcionario_w		varchar2(11);
nr_ctps_w			varchar2(7);
nr_serie_ctps_w			varchar2(5);
dt_opcao_w			varchar2(8) := '        ';
dt_nascimento_w			varchar2(8);
nr_seq_cbo_saude_w		varchar2(5);
vl_remuneracao_sem_13_w		varchar2(15);
cd_classe_contrib_w		varchar2(2) := '  ';
vl_remuneracao_w		varchar2(15);
vl_remuneracao_13_w		varchar2(15);
vl_desconto_w			varchar2(15);
cd_ocorrencia_w			varchar2(2) := '  ';	
vl_base_13_w			varchar2(15);
vl_base_13_mov_w		varchar2(15);
dt_lote_w			date;

Cursor C01 is

	select	d.cd_pessoa_fisica,
		b.nr_titulo
	from	titulo_pagar_v b,
		nota_fiscal a,
        	usuario c,
        	pessoa_fisica d              
	where	a.dt_emissao between dt_inicial_p and dt_final_p
	and	a.nr_sequencia	= b.nr_seq_nota_fiscal
	and 	b.ds_situacao <> ('Cancelado')
	and	a.nm_usuario = c.nm_usuario
	and	d.cd_pessoa_fisica is not null
	/*and	A.CD_OPERACAO_NF in (29, 33)*/
	and	a.cd_pessoa_fisica = d.cd_pessoa_fisica(+);
	
Cursor C02 is
	
	select	rpad(b.vl_titulo,15,'0'),
		rpad(0,15,'0'),
		rpad(0,15,'0'),
		rpad(0,15,'0'),
		rpad(0,15,'0')
	from	titulo_pagar_v b,
		nota_fiscal a,
        	usuario c,
        	pessoa_fisica d              
	where	a.dt_emissao between dt_inicial_p and dt_final_p
	and	b.nr_titulo = nr_titulo_w
	and	a.nr_sequencia	= b.nr_seq_nota_fiscal
	and 	b.ds_situacao <> ('Cancelado')
	and	a.nm_usuario = c.nm_usuario
	and	d.cd_pessoa_fisica is not null
	/*and	A.CD_OPERACAO_NF in (29, 33)*/
	and	a.cd_pessoa_fisica = d.cd_pessoa_fisica(+);

begin

delete from w_interf_sefip where nm_usuario = wheb_usuario_pck.get_nm_usuario;


/*Registro - 00*/
ie_tipo_registro_w	:= '00';
ds_brancos_51_w		:= lpad(' ',51,' ');
ds_brancos_18_w		:= lpad(' ',18,' ');
ds_zeros_36_w		:= lpad('0',36,'0');
ds_brancos_4_w		:= lpad(' ',4,' ');
ds_zeros_45_w		:= lpad('0',45,'0');

select	a.cd_cgc,
	rpad(substr(b.ds_razao_social,1,30),30,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'CTT'),1,20),20,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'E'),1,50),50,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'B'),1,20),20,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'CEP'),1,8),8,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'UF'),1,8),2,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'DDT')||obter_dados_pf_pj(null, a.cd_cgc, 'T'),1,12),12,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'M'),1,60),60,' '),
	rpad(substr(obter_dados_pf_pj(null, a.cd_cgc, 'CI'),1,20),20,' '),
	rpad(b.nr_seq_cnae,7,' ')
into	cd_inscr_resp_w,
	ds_razao_social_w,
	nm_pessoa_contato_w,
	ds_logradouro_w,
	ds_bairro_w,
	cd_cep_w,
	sg_uf_w,
	nr_telefone_w,
	ds_email_w,
	ds_cidade_w,
	cd_cnae_w
from	pessoa_juridica	b,
	ESTABELECIMENTO	a
where	a.cd_cgc		= b.cd_cgc
and	cd_estabelecimento	= cd_estabelecimento_p;

ie_competencia_w	:= 1;

dt_competencia_w	:= to_char(TRUNC(DT_INICIAL_P, 'MONTH'),'yyyymm');

select	rpad(' ',8,' ')
into	dt_recolhimento_w
from	dual;

select	rpad(' ',8,' ')
into	dt_rec_prev_soc_w
from	dual;

select	rpad(' ',7,' ')
into	ie_ind_rec_at_prev_w
from	dual;

ds_conteudo_w	:= ie_tipo_registro_w ||  ds_brancos_51_w ||  ie_tipo_remessa_w  || ie_tipo_inscr_w  || cd_inscr_resp_w  || ds_razao_social_w  || 
		ds_logradouro_w	 || ds_bairro_w  || cd_cep_w  || ds_cidade_w  || sg_uf_w  || nr_telefone_w  || ds_email_w  ||
		dt_competencia_w|| '211' || ie_ind_rec_fgts_w  || ie_mod_arquivo_w || dt_recolhimento_w  || ie_ind_rec_prev_w  || 
		dt_rec_prev_soc_w  || ie_ind_rec_at_prev_w  || ie_tipo_insc_forn_w  || cd_inscr_resp_w  || ds_brancos_18_w  ||
		ie_final_linha;


insert into w_interf_sefip
				(	nr_sequencia, dt_atualizacao, nm_usuario,
					nm_usuario_nrec, dt_atualizacao_nrec, ds_conteudo)
			values	(	w_interf_sefip_seq.nextval, sysdate, wheb_usuario_pck.get_nm_usuario,
					wheb_usuario_pck.get_nm_usuario, sysdate, ds_conteudo_w);
/*Registro - 00 Fim*/
				
/*Registro - 10*/					
ie_tipo_registro_w	:= '10';

select	lpad('0',15,'0')
into	vl_salario_familia_w
from	dual;

select	lpad('0',15,'0')
into	vl_salario_maternidade_w
from	dual;

select	lpad('0',15,'0')
into	vl_contr_desc_emp_13_w
from	dual;

ds_conteudo_w	:= ie_tipo_registro_w  || ie_tipo_inscr_w  || cd_inscr_resp_w || ds_razao_social_w  ||  ds_logradouro_w	 || ds_bairro_w  ||
		cd_cep_w  || ds_cidade_w  || sg_uf_w  || nr_telefone_w  || ds_email_w  || 'N'  || cd_cnae_w  || 'N'  || cd_centralizacao_w || ie_simples_w || 
		ie_fpas_w || cd_outra_entidade_w || cd_pag_gps_w || pr_isencao_filan_w || vl_salario_familia_w || vl_salario_maternidade_w || vl_contr_desc_emp_13_w ||
		ie_ind_neg_pos_w || vl_desc_prev_13_w || cd_banco_w || cd_agencia_w || cd_conta_w || 
		ds_zeros_45_w || ds_brancos_4_w || ie_final_linha;

insert into w_interf_sefip
				(	nr_sequencia, dt_atualizacao, nm_usuario,
					nm_usuario_nrec, dt_atualizacao_nrec, ds_conteudo)
			values	(	w_interf_sefip_seq.nextval, sysdate, wheb_usuario_pck.get_nm_usuario,
					wheb_usuario_pck.get_nm_usuario, sysdate, ds_conteudo_w);
/*Registro - 10 Fim*/

/*Registro - 20*/ 
open C01;
loop
fetch C01 into	
	cd_pessoa_fisica_w,
	nr_titulo_w;
exit when C01%notfound;
	begin

	/*Registro - 30*/
	open C02;
	loop
	fetch C02 into	
		vl_remuneracao_w,
		vl_remuneracao_13_w,
		vl_desconto_w,
		vl_base_13_mov_w,
		vl_base_13_w;
	exit when C02%notfound;
		begin
		ie_tipo_registro_w	:= '30';
		ds_brancos_98_w		:= lpad(' ',98,' ');
		
		select	lpad(nvl(d.nr_pis_pasep,0),11,'0'),
			nvl(to_char(d.dt_primeira_admissao,'ddmmyyyy'),null),
			lpad(d.nm_pessoa_fisica,70,' '),
			lpad(nvl(d.cd_funcionario,' '),11,' '),
			lpad(decode(nvl(substr(nr_ctps,1,7),'0'),'0',' ',substr(nr_ctps,1,7)),7,' '),
			lpad(decode(nvl(nr_serie_ctps,0),0,' ',nr_serie_ctps),5,' '),
			lpad(decode(nvl(dt_nascimento,null),null,' ',dt_nascimento),8,' '),
			lpad(decode(nvl(nr_seq_cbo_saude,0),0,' ',nr_seq_cbo_saude),5,' ')
		into	nr_pis_pasep_w,
			dt_admissao_w,
			nm_pessoa_fisica_w,
			cd_funcionario_w,
			nr_ctps_w,
			nr_serie_ctps_w,
			dt_nascimento_w,
			nr_seq_cbo_saude_w
		from	titulo_pagar_v b,
			nota_fiscal a,
        		usuario c,
        		pessoa_fisica d              
		where	a.dt_emissao between dt_inicial_p and dt_final_p
		and	d.cd_pessoa_fisica = cd_pessoa_fisica_w
		and b.nr_titulo = nr_titulo_w


		and	a.nr_sequencia	= b.nr_seq_nota_fiscal

		and   	a.nm_usuario = c.nm_usuario
		and   	a.cd_pessoa_fisica = d.cd_pessoa_fisica(+);
	

		ds_conteudo_w	:= ie_tipo_registro_w || ie_tipo_inscr_w  || cd_inscr_resp_w  || ie_tipo_inscr_w  || cd_inscr_resp_w  || 
			nr_pis_pasep_w  || rpad(nvl(dt_admissao_w,' '),8,' ')  || cd_categoria_trabalhador_w  || nm_pessoa_fisica_w  || cd_funcionario_w  || nr_ctps_w  ||
			nr_serie_ctps_w || dt_opcao_w || dt_nascimento_w || nr_seq_cbo_saude_w || vl_remuneracao_w || vl_remuneracao_13_w || 
			cd_classe_contrib_w || cd_ocorrencia_w || vl_desconto_w || vl_base_13_w || vl_base_13_mov_w || vl_base_13_w || ds_brancos_98_w || 
			ie_final_linha;
		
		insert into w_interf_sefip
					(	nr_sequencia, dt_atualizacao, nm_usuario,
						nm_usuario_nrec, dt_atualizacao_nrec, ds_conteudo)
				values	(	w_interf_sefip_seq.nextval, sysdate, wheb_usuario_pck.get_nm_usuario,
						wheb_usuario_pck.get_nm_usuario, sysdate, ds_conteudo_w);
		end;
	end loop;
	close C02;
	/*Registro - 30 Fim*/
	
	end;
end loop;
close C01;
/*Registro - 20 Fim*/

/*Registro - 90*/
ie_tipo_registro_w	:= '90';
ds_brancos_306_w	:= lpad(' ',306, ' ');
ds_marca_51_w		:= lpad('3',51,'3');

ds_conteudo_w	:= ie_tipo_registro_w ||  ds_marca_51_w ||  ds_brancos_306_w  || ie_final_linha;

insert into w_interf_sefip
					(	nr_sequencia, dt_atualizacao, nm_usuario,
						nm_usuario_nrec, dt_atualizacao_nrec, ds_conteudo)
				values	(	w_interf_sefip_seq.nextval, sysdate, wheb_usuario_pck.get_nm_usuario,
						wheb_usuario_pck.get_nm_usuario, sysdate, ds_conteudo_w);
/*Registro - 90 Fim*/
					
commit;

end HSVP_gerar_arquivo_sefip;
/