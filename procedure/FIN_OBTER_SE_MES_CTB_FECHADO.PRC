create or replace
procedure fin_obter_se_mes_ctb_fechado(	cd_estab_p		integer,
					dt_referencia_p		date) is 

nr_sequencia_w		ctb_mes_ref.nr_sequencia%type;	
ie_fechado_w		varchar2(1);			
				
begin

select	max(nvl(nr_sequencia,0))
into	nr_sequencia_w
from	ctb_mes_ref r
where	r.cd_empresa = obter_empresa_estab(cd_estab_p)
and	r.dt_referencia = trunc(dt_referencia_p,'mm');

if	(nr_sequencia_w > 0) then
	select	ctb_obter_se_mes_fechado(nr_sequencia_w, cd_estab_p)
	into	ie_fechado_w
	from	dual;
	
	if	(ie_fechado_w = 'F') then
		wheb_mensagem_pck.exibir_mensagem_abort(296872);
	end if;
end if;

commit;

end fin_obter_se_mes_ctb_fechado;
/

