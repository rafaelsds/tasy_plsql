create or replace
procedure pls_obter_regra_arq_rps(	ie_tipo_lote_p		in	pls_regra_arquivo_rps.ie_tipo_lote%type,
					nr_seq_lote_p		in	pls_lote_rps.nr_sequencia%type,
					ds_caminho_arquivo_p	out	pls_regra_arquivo_rps.ds_caminho_arquivo%type,
					ie_extensao_p		out	pls_regra_arquivo_rps.ds_extensao_arq%type,
					nm_arquivo_p		out	pls_regra_arquivo_rps.nm_arquivo%type ) is 

ie_tipo_lote_w		pls_regra_arquivo_rps.ie_tipo_lote%type := upper(ie_tipo_lote_p);
					
cursor c01 (	ie_tipo_lote_pc		pls_regra_arquivo_rps.ie_tipo_lote%type) is
	select	ds_caminho_arquivo,
		ds_extensao_arq,
		nm_arquivo
	from	pls_regra_arquivo_rps
	where	(ie_tipo_lote = ie_tipo_lote_pc or ie_tipo_lote is null)
	order by nvl(ie_tipo_lote,'X') desc;
	
begin

for r_c01_w in c01( ie_tipo_lote_w) loop
	ds_caminho_arquivo_p	:= r_c01_w.ds_caminho_arquivo;
	ie_extensao_p		:= r_c01_w.ds_extensao_arq;
	nm_arquivo_p		:= substr(replace_macro(r_c01_w.nm_arquivo,'@NR_SEQ_LOTE', nr_seq_lote_p),1,255);
	nm_arquivo_p		:= substr(replace_macro(nm_arquivo_p,'@DT_GERACAO_ARQ', to_char(sysdate, 'ddmmyyyy')),1,255);
end loop;

end pls_obter_regra_arq_rps;
/