
create or replace
procedure prescr_proc_material_bp(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					nr_seq_material_p	number,
					ie_coleta_exame_p	varchar2,
					dt_coleta_p		date,
					nm_usuario_p		varchar2) is 

ie_param_funcao_w varchar2(2);
					
begin

ie_param_funcao_w := obter_valor_param_usuario(724, 2 ,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento);

if	(ie_coleta_exame_p = 'S') and
	(nr_seq_prescr_p > 0) then

	update	prescr_procedimento 
	set 	dt_integracao = null, 
		dt_coleta = dt_coleta_p
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia  = nr_seq_prescr_p;
	
	commit;
elsif (ie_param_funcao_w <> 'A') then
	update 	prescr_procedimento 
	set 	dt_coleta = dt_coleta_p
	where 	nr_prescricao = nr_prescricao_p
	and 	cd_material_exame = obter_material_exame_lab(nr_seq_material_p,'',2);
	
	commit;
end if;

end prescr_proc_material_bp;
/
