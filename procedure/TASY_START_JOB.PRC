create or replace
procedure tasy_start_job(
			nm_proc_p		Varchar2) is 

nr_job_w	number(10);
			
begin

if	(nm_proc_p is not null) then
	begin

	select	job
	into	nr_job_w
	from	job_v
	where	upper(comando) like upper('%'||nm_proc_p||'%');
	
	dbms_job.run(nr_job_w, false);
	
	end;
end if;

end tasy_start_job;
/