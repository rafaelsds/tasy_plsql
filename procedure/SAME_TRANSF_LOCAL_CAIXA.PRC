create or replace
procedure same_transf_local_caixa(	nr_seq_caixa_p		number,
				nr_seq_local_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_same_w		number(10);	
nr_seq_agrup_w		number(10);			
ie_permite_util_pront_w	varchar2(1);				
qt_prontuarios_w	number(5);	
nr_seq_caixa_w		number(10);	
		
				
Cursor C01 is
	select	nr_sequencia,
		nr_seq_agrup
	from	same_prontuario a
	where	nr_seq_caixa = nr_seq_caixa_p;	

Cursor C02 is
	select	nr_sequencia
	from	same_caixa
	where	nr_seq_caixa_superior = nr_seq_caixa_p;	
	
Cursor C03 is
	select	nr_sequencia,
		nr_seq_agrup
	from	same_prontuario a
	where	nr_seq_caixa = nr_seq_caixa_w;	
begin
open C01;
loop
fetch C01 into	
	nr_seq_same_w,
	nr_seq_agrup_w;
exit when C01%notfound;
	begin
	Consistir_prontuario_local(nr_seq_same_w,nr_seq_local_p,cd_estabelecimento_p,nm_usuario_p);
	end;
end loop;
close C01;

select	max(nvl(ie_permite_util_pront,'N'))
into	ie_permite_util_pront_w
from	same_local
where	nr_sequencia = nr_seq_local_p;

if	(ie_permite_util_pront_w = 'S') then
	select	count(*)
	into	qt_prontuarios_w
	from	same_prontuario
	where	nr_seq_caixa = nr_seq_caixa_p;
	
	if	(qt_prontuarios_w > 1) then
		Wheb_mensagem_pck.exibir_mensagem_abort(215117);
	end if;

end if;

update	same_caixa
set	nr_seq_local = nr_seq_local_p
where	nr_sequencia = nr_seq_caixa_p;

open C01;
loop
fetch C01 into	
	nr_seq_same_w,
	nr_seq_agrup_w;
exit when C01%notfound;
	begin
	Gestao_Prontuario_Same(nr_seq_same_w,nm_usuario_p,'','',nr_seq_local_p,nr_seq_caixa_p,nr_seq_agrup_w,'',9,'','','',cd_estabelecimento_p);
	end;
end loop;
close C01;	

/*Altera as caixas que possuem a caixa como superior*/
open C02;
loop
fetch C02 into	
	nr_seq_caixa_w;
exit when C02%notfound;
	begin
			
	open C03;
	loop
	fetch C03 into	
		nr_seq_same_w,
		nr_seq_agrup_w;
	exit when C03%notfound;
		begin
		Consistir_prontuario_local(nr_seq_same_w,nr_seq_local_p,cd_estabelecimento_p,nm_usuario_p);
		end;
	end loop;
	close C03;
	
	update	same_caixa
	set	nr_seq_local = nr_seq_local_p
	where	nr_sequencia = nr_seq_caixa_w;
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_same_w,
		nr_seq_agrup_w;
	exit when C03%notfound;
		begin
		Gestao_Prontuario_Same(nr_seq_same_w,nm_usuario_p,'','',nr_seq_local_p,nr_seq_caixa_p,nr_seq_agrup_w,'',9,'','','',cd_estabelecimento_p);
		end;
	end loop;
	close C03;
	
	end;
end loop;
close C02;


commit;

end same_transf_local_caixa;
/
