create or replace
procedure dl_gerar_ajuste_distribuicao(
			nr_seq_lote_p		number,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number) is 

/*	Projeto SDL - Distribuição de Lucros SMA
	Os calculos da procedure dl_gerar_ajuste_distribuicao sao baseados no Metodo de Elaboracao da Distribuicao de Lucros, OS 171346.
*/			
			
vl_mont_variavel_f1_w	number(15,4);	
vl_mont_variavel_f2_w	number(15,4);			
vl_mont_invariavel_w	number(15,4);			
vl_distribuicao_w	number(15,4);
vl_disp_variavel_w	number(15,4);			
vl_fator_correcao_1_w	number(15,4);	
vl_fator_correcao_2_w	number(15,4);	
vl_total_extra_jetons_w	number(15,4);			
vl_distribuir_w		number(15,4);	
vl_item_atual_w		number(15,4);
vl_calculado_atual_w	number(15,4);
vl_total_distrib_w	number(15,4);
nr_seq_tipo_jetons_w	number(10);	
nr_seq_tipo_extras_w	number(10);	
nr_seq_distrib_item_w	number(10);
nr_seq_distribuicao_w	number(10);
dt_mesano_lote_w	date;

Cursor C01 is
	select	a.nr_sequencia
	from	dl_distribuicao a
	where	a.nr_seq_lote	= nr_seq_lote_p
	order by a.nr_sequencia;
	
Cursor C02 is
	select	b.nr_sequencia,
		nvl(b.vl_item,0),
		nvl(b.vl_calculado,0)
	from	dl_distribuicao a,
		dl_distribuicao_item b 
	where	a.nr_seq_lote	= nr_seq_lote_p
	and	a.nr_sequencia	= b.nr_seq_distribuicao
	and	b.nr_seq_item	in (nr_seq_tipo_extras_w,nr_seq_tipo_jetons_w);
	
Cursor C03 is
	select	b.nr_sequencia,
		nvl(b.vl_item,0),
		nvl(b.vl_calculado,0)
	from	dl_distribuicao a,
		dl_distribuicao_item b
	where	a.nr_seq_lote	= nr_seq_lote_p
	and	a.nr_sequencia	= b.nr_seq_distribuicao
	and	b.nr_seq_item	not in (nr_seq_tipo_extras_w,nr_seq_tipo_jetons_w);	
		
begin

vl_mont_variavel_f1_w	:= 0;
vl_mont_variavel_f2_w	:= 0;
vl_mont_invariavel_w	:= 0;
vl_distribuicao_w	:= 0;
vl_disp_variavel_w	:= 0;
vl_fator_correcao_1_w	:= 0;
vl_fator_correcao_2_w	:= 0;
vl_total_extra_jetons_w	:= 0;
vl_distribuir_w		:= 0;
vl_item_atual_w		:= 0;
vl_calculado_atual_w	:= 0;
vl_total_distrib_w	:= 0;

select	nvl(a.vl_distribuicao,0),
	a.dt_mesano_ref	
into	vl_distribuicao_w,
	dt_mesano_lote_w
from	dl_lote_distribuicao a
where	a.nr_sequencia	= nr_seq_lote_p;

/* 
	Calculo do fator de correcao 1	
	Fator de correcao 1 = Total da Distribuicao / Soma(valores teoricos da parte variavel)	
	
	Valor definitivo a distribuir = Total distribuicao - ((Total extras  + Total jetons) * Fator de correcao 1)
	
	Recalcular todos os valores da estapas 1 e 2 baseado no Valor definido a distribuir
*/
-- a) I, II - montante teorico do pagamento da parte variavel
select	sum(nvl(b.vl_calculado,0))
into	vl_mont_variavel_f1_w
from	dl_distribuicao a,
	dl_distribuicao_item b
where	a.nr_seq_lote	= nr_seq_lote_p
and	a.nr_sequencia	= b.nr_seq_distribuicao;
-- a) III - subtrair da distribuicao a soma dos valores da renda media
/* renda media nao estou calculando ainda.... verificar com antonio em conexao como deve ser feito o calculo
select	(sum(nvl(a.vl_calculado,0)) / 6)
into	vl_mont_invariavel_w
from	dl_lote_distribuicao a
where	a.dt_mesano_ref	between (dt_mesano_lote_w - 6 * 30) and (dt_mesano_lote_w - 30);*/

vl_disp_variavel_w	:= vl_distribuicao_w - nvl(vl_mont_invariavel_w,0);
-- a) IV - dividir o disponivel pela soma do mantante teorico variavel
vl_fator_correcao_1_w	:= vl_disp_variavel_w / vl_mont_variavel_f1_w;

/* dominio DL - Tipo de item para calculo da distribuicao / Anestesias em hora extra */
select	max(a.nr_sequencia)
into	nr_seq_tipo_extras_w
from	dl_tipo_item a
where	a.ie_tipo_item		= 9
and	a.cd_estabelecimento	= cd_estabelecimento_p;
/* dominio DL - Tipo de item para calculo da distribuicao / Reunioes (Jetons) */
select	max(a.nr_sequencia)
into	nr_seq_tipo_jetons_w
from	dl_tipo_item a
where	a.ie_tipo_item		= 10
and	a.cd_estabelecimento	= cd_estabelecimento_p;

select	sum(nvl(b.vl_calculado,0))
into	vl_total_extra_jetons_w
from	dl_distribuicao a,
	dl_distribuicao_item b
where	a.nr_seq_lote	= nr_seq_lote_p
and	a.nr_sequencia	= b.nr_seq_distribuicao
and	b.nr_seq_item	in (nr_seq_tipo_extras_w,nr_seq_tipo_jetons_w);

-- valor definitivo a distribuir
vl_distribuir_w	:= vl_distribuicao_w - ((vl_total_extra_jetons_w) * vl_fator_correcao_1_w);

open C01;
loop
fetch C01 into	
	nr_seq_distribuicao_w;
exit when C01%notfound;
	begin
	dl_gerar_itens_distribuicao(nr_seq_distribuicao_w,nm_usuario_p,'S','S','S','S','S','S','S','S','S','S','S','S','S','S',cd_estabelecimento_p,vl_distribuir_w);
	--dl_gerar_itens_distribuicao(nr_seq_distribuicao_w,nm_usuario_p,'N','N','N','S','N','N','N','S','S','N','N',cd_estabelecimento_p,vl_distribuir_w);
	end;
end loop;
close C01;
/* 
	Calculo do fator de correcao 2	
	Fator de correcao 2 = Total da Distribuicao / Soma(valores teoricos da parte variavel)	
*/
-- b) I, II - montante teorico do pagamento da parte variavel exceto extras e jetons
select	sum(nvl(b.vl_calculado,0))
into	vl_mont_variavel_f2_w
from	dl_distribuicao a,
	dl_distribuicao_item b
where	a.nr_seq_lote	= nr_seq_lote_p
and	a.nr_sequencia	= b.nr_seq_distribuicao
and	b.nr_seq_item	not in (nr_seq_tipo_extras_w,nr_seq_tipo_jetons_w);
-- b) III - subtrair da distribuicao a soma dos valores da renda media
-- b) IV - dividir o disponivel pela soma do mantante teorico variavel
vl_fator_correcao_2_w	:= vl_distribuir_w / vl_mont_variavel_f2_w;

open C02;
loop
fetch C02 into	
	nr_seq_distrib_item_w,
	vl_item_atual_w,
	vl_calculado_atual_w;
exit when C02%notfound;
	begin
		update 	dl_distribuicao_item
		set	dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			vl_item		= vl_calculado_atual_w * vl_fator_correcao_1_w
		where	nr_sequencia	= nr_seq_distrib_item_w;
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_distrib_item_w,
	vl_item_atual_w,
	vl_calculado_atual_w;
exit when C03%notfound;
	begin
		update 	dl_distribuicao_item
		set	dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			vl_item		= vl_calculado_atual_w * vl_fator_correcao_2_w
		where	nr_sequencia	= nr_seq_distrib_item_w;
	end;
end loop;
close C03;

open C01;
loop
fetch C01 into	
	nr_seq_distribuicao_w;
exit when C01%notfound;
	begin
		select	nvl(sum(a.vl_item),0)
		into	vl_total_distrib_w
		from	dl_distribuicao_item a
		where	a.nr_seq_distribuicao	= nr_seq_distribuicao_w;

		update	dl_distribuicao
		set	vl_distribuicao	= vl_total_distrib_w
		where	nr_sequencia	= nr_seq_distribuicao_w;
	end;
end loop;
close C01;

commit;

end dl_gerar_ajuste_distribuicao;
/
