create or replace
procedure eis_gerar_gestao_exame(dt_referencia_p	date,
				ie_opcao_p		varchar2) is 


ie_opcao_w	varchar2(2);

begin

if (dt_referencia_p is not null) then

	if (ie_opcao_p = 'D') then
		ie_opcao_w	:= 'dd';
	else
		ie_opcao_w	:= 'mm';
	end if;

	delete	eis_gestao_exame
	where	trunc(dt_prescricao, ie_opcao_w) = trunc(dt_referencia_p, ie_opcao_w);
	
	insert into eis_gestao_exame(	nr_sequencia,
					nr_prescricao,
					dt_prescricao,
					cd_medico,
					dt_liberacao_medico,
					nm_usuario_original,
					nm_usuario,
					dt_emissao_setor_atend,
					dt_liberacao,
					cd_pessoa_fisica,
					dt_prev_execucao,
					nr_seq_laudo,
					dt_liberacao_laudo,
					ie_status_execucao,
					cd_setor_atendimento,
					cd_procedimento,
					ie_origem_proced,
					nm_usuario_laudo,
					nm_usuario_aprovacao,
					nm_usuario_seg_aprov,
					cd_medico_exec,
					cd_medico_executor,
					cd_medico_resp,
					ie_autorizacao,
					ds_observacao,
					nr_seq_proc_interno,
					nr_seq_proced,
					nr_seq_propaci,
					ds_dado_clinico,
					nr_seq_motivo_parada,
					nr_atendimento,
					cd_material_exame,
					nr_prontuario,
					cd_setor_entrega,
					dt_baixa,
					dt_procedimento,
					cd_motivo_baixa,
					nr_seq_interno,
					cd_procedencia,
					nr_seq_agenda,
					ie_lado,
					cd_estabelecimento,
					nm_usuario_exec,
					ie_urgencia,
					ie_tipo_atendimento,
					ie_executar_leito,
					dt_inicio_exame,
					nr_seq_exame,
					cd_tipo_procedimento,
					cd_grupo_proc,
					cd_empresa,
					cd_setor_prescr_proced,
					cd_setor_prescricao,
					cd_proc_exec,
					ie_origem_exec,
					nr_seq_proc_interno_exec,
					nr_seq_tamanho_filme,
					qt_filme)
				select	eis_gestao_exame_seq.NextVal,
					a.nr_prescricao,
					a.dt_prescricao,
					a.cd_medico,
					a.dt_liberacao_medico,
					a.nm_usuario_original,
					a.nm_usuario,
					b.dt_emissao_setor_atend,
					a.dt_liberacao,
					a.cd_pessoa_fisica,				
					b.dt_prev_execucao,
					d.nr_sequencia,
					d.dt_liberacao,
					b.ie_status_execucao,
					nvl(b.cd_setor_atendimento, nvl(a.cd_setor_entrega, a.cd_setor_atendimento)),
					b.cd_procedimento,
					b.ie_origem_proced,
					d.nm_usuario,
					d.nm_usuario_aprovacao,
					d.nm_usuario_seg_aprov,
					b.cd_medico_exec,
					c.cd_medico_executor,
					d.cd_medico_resp,
					b.ie_autorizacao,
					substr(b.ds_observacao,1,255),
					b.nr_seq_proc_interno,
					b.nr_sequencia,
					c.nr_sequencia,
					b.ds_dado_clinico,
					d.nr_seq_motivo_parada,
					a.nr_atendimento,
					b.cd_material_exame,
					g.nr_prontuario,
					a.cd_setor_entrega,
					b.dt_baixa,
					c.dt_procedimento,
					b.cd_motivo_baixa,
					b.nr_seq_interno,
					z.cd_procedencia,
					a.nr_seq_agenda,
					b.ie_lado,
					a.cd_estabelecimento,
					c.nm_usuario_original,
					b.ie_urgencia,
					z.ie_tipo_atendimento,
					nvl(b.ie_executar_leito,'N'),
					b.dt_inicio_exame,
					b.nr_seq_exame,
					w.cd_tipo_procedimento,
					w.cd_grupo_proc,
					e.cd_empresa,
					b.cd_setor_atendimento,
					a.cd_setor_atendimento,
					c.cd_procedimento,
					c.ie_origem_proced,
					c.nr_seq_proc_interno,
					c.nr_seq_tamanho_filme,
					c.qt_filme
				from    estabelecimento e,
					procedimento w,
					pessoa_fisica g,
					atendimento_paciente z,
					laudo_paciente d,
					conta_paciente f,
					procedimento_paciente c,
					prescr_procedimento b,
					prescr_medica a
				where   w.cd_procedimento = b.cd_procedimento
				and     w.ie_origem_proced = b.ie_origem_proced
				and	a.cd_pessoa_fisica = g.cd_pessoa_fisica
				and     a.nr_atendimento = z.nr_atendimento(+)
				and     a.nr_prescricao = b.nr_prescricao
				and	c.nr_laudo		 = d.nr_sequencia (+)
				and	b.nr_prescricao = c.nr_prescricao (+)
				and	c.nr_interno_conta	= f.nr_interno_conta(+)
				and	z.cd_estabelecimento = e.cd_estabelecimento(+)
				and	b.nr_sequencia = c.nr_sequencia_prescricao (+)
				and     ((b.cd_motivo_baixa = 0) or (b.cd_motivo_baixa is null) or (b.ie_status_execucao <> '10'))
				and     f.ie_cancelamento is null
				and     b.ie_suspenso  <> 'S'
				and     a.dt_suspensao is null  
				and	trunc(a.dt_prescricao, ie_opcao_w) = trunc(dt_referencia_p, ie_opcao_w)
				and a.dt_liberacao is not null;				
	
end if;

commit;

end eis_gerar_gestao_exame;
/