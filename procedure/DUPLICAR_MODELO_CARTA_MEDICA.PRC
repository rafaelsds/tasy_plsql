create or replace
procedure Duplicar_modelo_carta_medica(
			nr_seq_modelo_p		number,
			nm_usuario_p		Varchar2) is 
			
nr_seq_modelo_w		number(10);
nr_seq_carta_med_mod_lib_old_w 	carta_medica_modelo_lib.nr_sequencia%type;
nr_seq_carta_med_mod_lib_w 		carta_medica_modelo_lib.nr_sequencia%type;

cursor C01 is
	select  nr_sequencia,
			nm_usuario_regra,
			ie_permite_utilizar,
			ie_permite_editar,
			cd_setor_atendimento,
			cd_perfil,
			cd_departamento			
	from    carta_medica_modelo_lib
	where   nr_seq_modelo = nr_seq_modelo_p;
	
cursor C02 is
	select  ie_status,
			ie_permissao
	from    carta_medica_lib_status
	where   nr_seq_carta_med_mod_lib = nr_seq_carta_med_mod_lib_old_w;

begin

select	carta_medica_modelo_seq.nextval
into	nr_seq_modelo_w
from	dual;

insert into carta_medica_modelo(
		nr_sequencia,
		cd_estabelecimento,
		cd_perfil,
		nm_modelo,
		ds_modelo,
		ie_preliminar,
		qt_divisor,
		ie_situacao,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
        ds_titulo_definitivo,
		ds_titulo_adicional,
		ie_metodo_busca_info)
select	nr_seq_modelo_w,
		cd_estabelecimento,
		cd_perfil,
		substr(Wheb_mensagem_pck.get_texto(1021458, 'DS_MODELO_W='||nm_modelo),1,255),
		ds_modelo,
		ie_preliminar,
		qt_divisor,
		ie_situacao,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
        ds_titulo_definitivo,
		ds_titulo_adicional,
		ie_metodo_busca_info
from	carta_medica_modelo
where	nr_sequencia = nr_seq_modelo_p;

insert into carta_medica_modelo_macro(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_chave,
		nr_seq_modelo,
		ie_macro_pf)
select	carta_medica_modelo_macro_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_chave,
		nr_seq_modelo_w,
		ie_macro_pf
from	carta_medica_modelo_macro
where	nr_seq_modelo = nr_seq_modelo_p;

insert into participante_carta_medica (	
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nm_usuario_resp,
		nm_usuario_assinat,
		ie_deve_assinar,
		nr_seq_apresent,
		nm_destinatario,
		nm_usuario,
		nr_seq_modelo,
		nr_seq_assinatura)
select	participante_carta_medica_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_resp,
		nm_usuario_assinat,
		ie_deve_assinar,
		nr_seq_apresent,
		nm_destinatario,
		nm_usuario_p,
		nr_seq_modelo_w,
		nr_seq_assinatura
from	participante_carta_medica
where	nr_seq_modelo = nr_seq_modelo_p;

commit;

EXPORTAR_REGRAS_CARTA_MEDICA(nr_seq_modelo_p, nr_seq_modelo_w, 'TD', nm_usuario_p);

for c01_w in C01 loop
	begin			
		nr_seq_carta_med_mod_lib_old_w := c01_w.nr_sequencia;
		
		select 	carta_medica_modelo_lib_seq.nextval
		into	nr_seq_carta_med_mod_lib_w
		from	dual;
		
		insert into carta_medica_modelo_lib(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_regra,
			nm_usuario_nrec,
			ie_permite_utilizar,
			cd_setor_atendimento,
			cd_perfil,
			cd_departamento,
			ie_permite_editar,
			nr_seq_modelo
		) values (
			nr_seq_carta_med_mod_lib_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			c01_w.NM_USUARIO_REGRA,
			nm_usuario_p,			
			c01_w.ie_permite_utilizar,			
			c01_w.cd_setor_atendimento,
			c01_w.cd_perfil,
			c01_w.cd_departamento,
			c01_w.ie_permite_editar,
			nr_seq_modelo_w);
		
		for c02_w in C02 loop
			begin
				insert into carta_medica_lib_status(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_status,
					ie_permissao,
					nr_seq_carta_med_mod_lib
				) values (
					carta_medica_lib_status_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,					
					c02_w.ie_status,
					c02_w.ie_permissao,
					nr_seq_carta_med_mod_lib_w);
			end;
		end loop;
	end;
end loop;

commit;

end Duplicar_modelo_carta_medica;
/