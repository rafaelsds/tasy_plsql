exec Exec_sql_Dinamico('Maicon', 'alter table W_RAZAO_MEDIC_CONTROLADO add IE_ORIGEM_DOCUMENTO varchar2(3)');

create or replace 
procedure hmcg_gerar_razao_dcb(
				dt_inicial_p  		date,
				dt_final_p			date,
				dt_movto_inic_p		date,
				dt_movto_fim_p		date,
				cd_estabelecimento_p	number,
				cd_setor_atendimento_p	number,
				nm_usuario_p		varchar2,
				cd_local_estoque_p	varchar2,
				cd_medicamento_p		number) is

nr_sequencia_w			number(10,0);
nr_documento_w			varchar2(22);
nr_sequencia_item_docto_w	number(22,0);
nr_sequencia_docto_w		movimento_estoque.nr_sequencia_documento%type;
nr_crm_w			varchar2(80);
cd_medico_w			varchar2(10);
cd_material_w			number(22,0);
cd_material_ww			number(22,0) := 0;
dt_movimento_w			date;
cd_classificacao_w		varchar2(80);
cd_dcb_w			varchar2(80);
ds_dcb_w			varchar2(80);
ie_tipo_w			varchar2(10);
ds_observacao_w			varchar2(255);
qt_entrada_w			number(22,4);
qt_saida_w			number(22,4);
qt_total_entrada_w		number(22,4);
qt_total_saida_w		number(22,4);
qt_perda_w			number(22,4);
qt_transf_w			number(22,4);
ie_primeira_vez_w		varchar2(1) := 'N';
qt_estoque_w			number(22,4);
qt_saldo_w			number(22,4);
ds_historico_w			Varchar2(250);
ie_origem_documento_w		varchar2(25);
dt_inicio_w			Date;
dt_fim_w			Date;
dt_saldo_w			Date;
ie_entrada_saida_w		Varchar2(10);
ie_tipo_requisicao_w		varchar2(30);
nr_coluna_w			Number(10,0);
dt_movto_inic_w			Date;
dt_movto_fim_w			Date;
dt_anterior_w			date;
dt_saldo_dia_w			date;
qt_acumulado_dia_w		number(22,4) := 0;
cd_operacao_estoque_w		number(3);
ie_analitico_w			varchar2(1);
nr_receita_w			number(15,0);

Cursor C00 is
select	distinct
	m.cd_classificacao,
	c.cd_dcb,
	c.ds_dcb,
	a.cd_material
from	dcb_medic_controlado c,
	medic_controlado m,
	saldo_estoque a
where	m.cd_material		= a.cd_material
and	m.nr_seq_dcb		= c.nr_sequencia
and	a.dt_mesano_referencia between  trunc(dt_inicio_w,'month') and dt_fim_w
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	((cd_medicamento_p is null) or (cd_medicamento_p = a.cd_material))
order by	c.ds_dcb;

cursor C01  is
	select	/*+ index(a MOVESTO_I4) */
		'B' ie_tipo,
		trunc(a.dt_movimento_estoque) dt_movimento_estoque,
		sum(decode(a.cd_acao,'1', qt_estoque, qt_estoque * -1)) qt_estoque,
		'Sa�das' ds_historico,
		'',
		'',
		'0',
		0,
		0,
		'',
		'',
		sum(obter_qt_coluna_controlado(1, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_entrada,
		sum(obter_qt_coluna_controlado(2, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_saida,
		sum(obter_qt_coluna_controlado(3, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_perda,
		sum(obter_qt_coluna_controlado(4, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque)) qt_transf,
		a.cd_operacao_estoque,
		0
	from	operacao_estoque b,
		movimento_estoque a
	where	a.dt_mesano_referencia between dt_inicio_w and dt_fim_w
	and	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.cd_material_estoque	= cd_material_w
	and	a.cd_operacao_estoque	= b.cd_operacao_estoque
	and	b.ie_entrada_saida		= 'S'
	and	((ie_analitico_w = 'N') and (b.ie_tipo_requisicao <> 3))
	and	(a.cd_setor_atendimento = cd_setor_atendimento_p or nvl(cd_setor_atendimento_p,0) = 0)
	and	((cd_local_estoque_p is null) or (obter_se_contido(a.cd_local_estoque, cd_local_estoque_p) = 'S'))
	and	((dt_movto_inic_p is null) or (a.dt_movimento_estoque between dt_movto_inic_w and fim_dia(dt_movto_fim_w)))
	group by	trunc(a.dt_movimento_estoque), a.cd_operacao_estoque, trunc(a.dt_validade), a.cd_lote_fabricacao
	union all
	select	/*+ index(a MOVESTO_I4) */
		'B' ie_tipo,
		a.dt_movimento_estoque,
		decode(a.cd_acao,'1', qt_estoque, qt_estoque * -1) qt_estoque,
		substr(decode(a.ie_origem_documento,
			3,est_obter_paciente_movto(a.nr_movimento_estoque),
			1,obter_nome_pf_pj(null,cd_fornecedor),
			obter_desc_local_estoque(a.cd_local_estoque)),1,250),
		b.ie_entrada_saida,
		b.ie_tipo_requisicao,
		to_char(a.nr_documento),
		a.nr_sequencia_item_docto,
		a.nr_sequencia_documento,
		a.ie_origem_documento,
		b.ie_tipo_requisicao,
		obter_qt_coluna_controlado(1, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_entrada,
		obter_qt_coluna_controlado(2, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_saida,
		obter_qt_coluna_controlado(3, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_perda,
		obter_qt_coluna_controlado(4, b.ie_entrada_saida, b.ie_tipo_requisicao,
			b.ie_coluna_controlado, a.cd_acao, a.qt_estoque) qt_transf,
		b.cd_operacao_estoque,
		a.nr_receita
	from	operacao_estoque b,
		movimento_estoque a
	where	a.dt_mesano_referencia between  dt_inicio_w and dt_fim_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_material_estoque	= cd_material_w
	and	a.cd_operacao_estoque	= b.cd_operacao_estoque
	and	((ie_analitico_w = 'S') or (b.ie_tipo_requisicao = 3) or (b.ie_entrada_saida = 'E'))
	and	(a.cd_setor_atendimento = cd_setor_atendimento_p or nvl(cd_setor_atendimento_p,0) = 0)
	and	((cd_local_estoque_p is null) or (obter_se_contido(a.cd_local_estoque, cd_local_estoque_p) = 'S'))
	and	((dt_movto_inic_p is null) or (a.dt_movimento_estoque between dt_movto_inic_w and dt_movto_fim_w))
	union all
	select distinct	
		'A',
		decode(dt_movto_inic_p, null, last_Day(dt_saldo_w), trunc(dt_movto_inic_w,'dd')-1),
		0,
		'Saldo anterior',
		'',
		'',
		'0',
		0,
		0,
		'',
		'',
		0,
		0,
		0,
		0,
		0,
		0
	from	material a
	where	cd_material = cd_material_w
	order by 1,2;

begin

ie_analitico_w	:= 'S';

dt_inicio_w	:= trunc(dt_inicial_p, 'dd');
dt_fim_w	:= trunc(dt_final_p, 'dd') + 86399 / 86400;
dt_saldo_w	:= add_months(dt_inicio_w, -1);
dt_movto_inic_w	:= trunc(dt_movto_inic_p, 'dd');
dt_movto_fim_w	:= trunc(dt_movto_fim_p, 'dd') + 86399 / 86400;

/*if	(dt_movto_inic_p = to_date(to_char(dt_movto_inic_p,'dd/mm/yyyy hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')) then
	dt_movto_inic_w	:= dt_movto_inic_p;
	dt_movto_fim_w	:= dt_movto_fim_p;
end if;*/

delete w_razao_medic_controlado;
commit;

open c00;
loop
fetch c00 into
	cd_classificacao_w,
	cd_dcb_w,
	ds_dcb_w,
	cd_material_w;
exit when c00%notfound;
	select	sum(qt_estoque)
	into	qt_saldo_w
	from	saldo_estoque
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_material 		= cd_material_w
	and	dt_mesano_referencia 	= dt_saldo_w
	and	((cd_local_estoque_p is null) or (obter_se_contido(cd_local_estoque, cd_local_estoque_p) = 'S'));
	
	ie_primeira_vez_w	:= 'S';

	OPEN C01;
	LOOP
	FETCH C01 into
		ie_tipo_w,
		dt_movimento_w,
		qt_estoque_w,
		ds_historico_w,
		ie_entrada_saida_w,
		ie_tipo_requisicao_w,
		nr_documento_w,
		nr_sequencia_item_docto_W,
		nr_sequencia_docto_w,
		ie_origem_documento_w,
		ie_tipo_requisicao_w,
		qt_entrada_w,
		qt_saida_w,
		qt_perda_w,
		qt_transf_w,
		cd_operacao_estoque_w,
		nr_receita_w;
	exit when c01%notfound;

		select w_razao_medic_controlado_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		nr_crm_w			:= '';
		ds_observacao_w			:= '';
			
		if	(ie_origem_documento_w	= '3') and
			(nvl(nr_documento_w,'0') <> '0') then
			begin
			if	(nr_sequencia_item_docto_w is null) or (nr_sequencia_item_docto_w = 999) then
				begin
				select	nvl(max(cd_medico_resp), ' ')
				into	cd_medico_w
				from	atendimento_paciente
				where	nr_atendimento	= nr_documento_w;
				end;
			else
				begin
				select	nvl(max(cd_medico), ' ')
				into	cd_medico_w
				from	prescr_medica
				where	nr_prescricao	= nr_documento_w;
				end;
			end if;
			if	(cd_medico_w <> ' ') then
				select	substr(obter_nome_medico(cd_medico_w,'NC'),1,80)
				into	ds_observacao_w
				from	dual;
			end if;			
			end;
			
		elsif	(ie_origem_documento_w = '2') and
			(nvl(nr_documento_w,'0') <> '0') then
			
			select	nvl(max(substr(obter_nome_pf_pj(cd_pessoa_requisitante,null) || ' (' || cd_pessoa_requisitante || ')',1,255)),'Sem Respons�vel')
			into	ds_observacao_w
			from	requisicao_material
			where	nr_requisicao = nr_documento_w;
			
		elsif	(ie_origem_documento_w = '1') and
			(nvl(nr_documento_w,'0') <> '0') then			
			select	max(nr_nota_fiscal || ' - U ' || dt_emissao)
			into	ds_observacao_w
			from	nota_fiscal
			where	nr_nota_fiscal = nr_documento_w;
			
		end if;
		
		if	(dt_movto_inic_p is null) then
			qt_saldo_w		:= nvl(qt_saldo_w,0) +
						qt_entrada_w -
						qt_saida_w -
						qt_perda_w -
						qt_transf_w;					
		elsif	(dt_movto_inic_p is not null) then
			begin
			if	(trunc(dt_movimento_w) = trunc(dt_anterior_w)) then
				qt_acumulado_dia_w	:= qt_acumulado_dia_w +
						qt_entrada_w - qt_saida_w - qt_perda_w - qt_transf_w;
			else
				qt_acumulado_dia_w	:= 0;
			end if;
			

			/* � preciso buscar o saldo diario com parametros diferentes quando for o primeiro(Saldo 	anterior), ou os saldos dos dias durante o mes*/
			dt_saldo_dia_w	:= dt_movimento_w;
			
			if	(ie_primeira_vez_w = 'S') then
				dt_saldo_dia_w	:= dt_movto_inic_w;
				ie_primeira_vez_w := 'N';
				qt_saldo_w	:= Obter_saldo_Diario_medic(
							dt_movto_inic_p - 1,
							cd_estabelecimento_p,
							cd_local_estoque_p,
							cd_material_w);
			else
				qt_saldo_w	:= nvl(qt_saldo_w,0) +
							qt_entrada_w -
							qt_saida_w -
							qt_perda_w -
							qt_transf_w;
			end if;
			
			dt_anterior_w	:= dt_movimento_w;
			end;
		end if;

		insert into w_Razao_Medic_Controlado (
			ie_tipo,			cd_estabelecimento,			nr_sequencia,
			cd_material,			cd_classificacao,			cd_dcb,
			ds_dcb,				dt_movimento,				ds_historico,
			qt_entrada,			qt_saida,				qt_perda,
			qt_transf,			qt_saldo,				nr_crm,
			ds_observacao,			cd_operacao_estoque,			nr_receita,
			ie_origem_documento)
		values(	ie_tipo_w,			cd_estabelecimento_p,			nr_sequencia_w,
			cd_material_w,			cd_classificacao_w,			cd_dcb_w,
			ds_dcb_w,			dt_movimento_w,				ds_historico_w,
			qt_entrada_w,			qt_saida_w,				qt_perda_w,
			qt_transf_w,			qt_saldo_w,				nr_crm_w,
			nvl(ds_observacao_w,' - '),	cd_operacao_estoque_w,			nr_documento_w,
			ie_origem_documento_w);
	end loop;
	close c01;
	
	select W_Razao_Medic_Controlado_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	select	sum(qt_entrada),
		sum(qt_saida)
	into	qt_total_entrada_w,
		qt_total_saida_w
	from	W_Razao_Medic_Controlado
	where	cd_material = cd_material_w;
	
	insert into w_Razao_Medic_Controlado (
			ie_tipo,			cd_estabelecimento,			nr_sequencia,
			cd_material,			cd_classificacao,			cd_dcb,
			ds_dcb,				ds_historico,				qt_entrada,
			qt_saida,			qt_perda,				qt_transf,
			qt_saldo,			nr_crm)
		values(	'T',				cd_estabelecimento_p,			nr_sequencia_w,
			cd_material_w,			cd_classificacao_w,			cd_dcb_w,
			ds_dcb_w,			'Total',				qt_total_entrada_w,
			qt_total_saida_w,		0,					0,
			qt_saldo_w,			nr_crm_w);
end loop;
close c00;

commit;

end hmcg_gerar_razao_dcb;
/