create or replace 
procedure sup_palm_imprime_requisicao(	nr_seq_impressora_p			Number,
					nr_requisicao_p				Number,
					nr_seq_relatorio_p			Number,
					nm_usuario_p				Varchar2,
					ds_erro_p		out 		varchar2) is 
					

ds_endereco_rede_w      Varchar2(255);
ds_endereco_ip_w	Varchar2(255);
nr_prescricao_w         Number(14);
 
begin

select 	max(ds_endereco_rede),
	max(ds_endereco_ip)
into	ds_endereco_rede_w,
	ds_endereco_ip_w
from	impressora      
where	nr_sequencia = nr_seq_impressora_p;


if	(ds_endereco_rede_w is null or ds_endereco_ip_w is null) then
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(280643);
else
	
	 insert into requisicao_impressao_pda (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,             
            		dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_requisicao,            
            		nr_seq_impressora,
			ds_endereco_rede,
			nr_seq_relatorio,       
            		ds_endereco_ip,
			ie_pendente)         
             values (	requisicao_impressao_pda_seq.nextval,
			sysdate,
			nm_usuario_p,
            		sysdate,
			nm_usuario_p,
			nr_requisicao_p,
            		nr_seq_impressora_p,
			ds_endereco_rede_w,
			nr_seq_relatorio_p,
            		ds_endereco_ip_w,
			'S');

	insert into fila_impressao_etiqueta(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_chave,
			nr_seq_impressora,
			nr_seq_relatorio,
			ds_endereco_rede,
			ds_endereco_ip,
			ie_pendente)
	values(		fila_impressao_etiqueta_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_requisicao_p,
            		nr_seq_impressora_p,
			nr_seq_relatorio_p,
			ds_endereco_rede_w,
            		ds_endereco_ip_w,
			'S');

	commit;
end if;

end sup_palm_imprime_requisicao;
/