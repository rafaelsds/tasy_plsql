create or replace
procedure  pls_gerar_protocolo_compl
			(	nr_seq_prestador_p		Number,
				nr_seq_guia_p			Number,
				ie_tipo_guia_p			Number,
				nr_seq_conta_orig_p		Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number,
				ie_permite_apres_parc_p		Varchar2,
				nr_seq_conta_p		out	Number,
				nr_seq_protocolo_p	out	Number,
				ie_mensagem_ret_p	out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a conta de complemento a partir da guia
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [  x] Tasy (Delphi/Java) [  x] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
	ie_permite_apres_parc_p: Parametro[27] - Permite o prestador complementar guias parcialmente apresentadas, da funcao OPSW - Contas Medicas
	ie_mensagem_ret_p: 	FIN = Conta Criada com sucesso
			APR = Conta ja apresentada
-------------------------------------------------------------------------------------------------------------------
Referencias:
pls_reabrir_complemento_conta
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


nr_seq_protocolo_w		Number(10)	:= 0;
nr_seq_conta_w			Number(10)	:= 0;
ie_tipo_guia_w			Number(1)	:= 0;
dt_mes_competencia_w		Date;
ie_situacao_prot_w      	Varchar(2);
nr_seq_segurado_w		Number(10)	:= 0;
nr_seq_protocolo_orig_w		Number(10);
cd_estab_padrao_w		pls_web_param_geral.ie_estab_padrao%type;
cd_estabelecimento_w		pls_conta.cd_estabelecimento%type;
ie_conta_existe_w		Varchar2(5)	:= 'N';
ie_mensagem_ret_w		Varchar2(3) := 'FIN';
nr_seq_conta_existente_w	pls_conta.nr_sequencia%type := 0;
nr_seq_protocolo_existente_w	pls_conta.nr_seq_protocolo%type;
qt_registros_w			number(10);
cd_medico_solicitante_w		pls_guia_plano.cd_medico_solicitante%type;
ie_vinculo_complemento_w	pls_web_param_geral.ie_vinculo_complemento%type;
ie_estagio_complemento_w	pls_guia_plano.ie_estagio_complemento%type;
nr_seq_regra_compl_w		pls_guia_plano.nr_seq_regra_compl%type;
ds_log_w			plsprco_cta.ds_log%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_conta_proc
	where	nr_seq_conta = nr_seq_conta_existente_w;

Cursor C02 is
	select	nr_sequencia
	from	pls_conta_mat
	where	nr_seq_conta = nr_seq_conta_existente_w;
begin

select	max(ie_estab_padrao)
into	cd_estab_padrao_w
from	pls_web_param_geral;

if	(cd_estab_padrao_w	is not null) then
	cd_estabelecimento_w	:= cd_estab_padrao_w;
else
	cd_estabelecimento_w	:= cd_estabelecimento_p;
end if;

if	(ie_tipo_guia_p	=	1) then
	ie_tipo_guia_w 	:=	5;
elsif	(ie_tipo_guia_p	=	2)then
	ie_tipo_guia_w 	:=	4;
else	
	ie_tipo_guia_w 	:=	ie_tipo_guia_p;
end if;

select	max(ie_vinculo_complemento)
into	ie_vinculo_complemento_w
from	pls_web_param_geral
where	cd_estabelecimento	= cd_estabelecimento_w;

ie_vinculo_complemento_w	:= nvl(ie_vinculo_complemento_w,'P');

select	max(cd_medico_solicitante),
	max(ie_estagio_complemento),
	max(nr_seq_regra_compl)
into	cd_medico_solicitante_w,
	ie_estagio_complemento_w,
	nr_seq_regra_compl_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_p;

nr_seq_protocolo_w	:= 0;

if	(ie_vinculo_complemento_w = 'PM') then
	select 	nvl(min(nr_sequencia), 0)
	into	nr_seq_protocolo_w
	from 	pls_protocolo_conta a
	where	ie_protocolo_compl 	= 'S'
	and	ie_status		= 1
	and	ie_tipo_guia		= ie_tipo_guia_w
	and	nr_seq_prestador	= nr_seq_prestador_p
	and	ie_situacao 		= 'I'
	and	(select count(1) 
		 from 	pls_conta  
		 where 	nr_seq_protocolo = a.nr_sequencia 
		 and	ie_status <> 'C') < 100
	and	exists	(select 1
			from 	pls_conta  	x
			where 	x.nr_seq_protocolo 	= a.nr_sequencia 
			and	x.cd_medico_solicitante	= cd_medico_solicitante_w)
	and	not exists (select 1
			from 	pls_conta  	x
			where 	x.nr_seq_protocolo 	= a.nr_sequencia 
			and	x.cd_medico_solicitante	!= cd_medico_solicitante_w);
elsif	(ie_vinculo_complemento_w = 'P') then
	select 	nvl(min(nr_sequencia), 0)
	into	nr_seq_protocolo_w
	from 	pls_protocolo_conta a
	where	ie_protocolo_compl 	= 'S'
	and	ie_status		= 1
	and	ie_tipo_guia		= ie_tipo_guia_w
	and	nr_seq_prestador	= nr_seq_prestador_p
	and	ie_situacao 		= 'I'
	and	(select count(1) 
		 from 	pls_conta  
		 where 	nr_seq_protocolo = a.nr_sequencia 
		 and	ie_status <> 'C') < 100;
end if;

if	(nr_seq_protocolo_w = 0 ) then
	select	pls_protocolo_conta_seq.nextval
	into	nr_seq_protocolo_w
	from	dual;

	dt_mes_competencia_w	:= sysdate;

	insert into pls_protocolo_conta(nr_sequencia, dt_atualizacao, nm_usuario,          
					ie_status, ie_situacao, cd_estabelecimento,             
					dt_mes_competencia, nr_seq_prestador, ie_apresentacao, 
					ie_protocolo_compl, ie_tipo_guia, ie_origem_protocolo,
					ie_guia_fisica, nm_usuario_nrec, dt_atualizacao_nrec,
					ie_tipo_protocolo, dt_protocolo)
				values(	nr_seq_protocolo_w, sysdate, nm_usuario_p,  
					'1', 'I', cd_estabelecimento_w,
					dt_mes_competencia_w, nr_seq_prestador_p, 'A', 
					'S', ie_tipo_guia_w, 'C',
					'N', nm_usuario_p, sysdate,
					'C', sysdate);

	
end if;
nr_seq_protocolo_p	:= 	nr_seq_protocolo_w;

begin
select	nr_sequencia
into	nr_seq_conta_existente_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_orig_p;
exception
when others then
	nr_seq_conta_existente_w:= 0;
end;

if	(nr_seq_conta_existente_w <> 0) then
	update	pls_conta
	set	nr_seq_protocolo = nr_seq_protocolo_w
	where	nr_sequencia = nr_seq_conta_existente_w;
	
	nr_seq_conta_w := nr_seq_conta_existente_w;
	
	delete from pls_analise_glo_ocor_grupo a
	where	exists (select	1
			from	pls_ocorrencia_benef x
			where	a.nr_seq_ocor_benef = x.nr_sequencia
			and	x.nr_seq_conta = nr_seq_conta_existente_w);
	
	delete  from pls_analise_fluxo_ocor a 
	where   a.nr_seq_analise in	(select	y.nr_seq_analise
					from	pls_conta y
					where	y.nr_sequencia	= nr_seq_conta_existente_w);
								
	delete	pls_ocorrencia_benef
	where	(nr_seq_conta = nr_seq_conta_existente_w) ;
	
	delete from pls_analise_glo_ocor_grupo a
	where	exists (select	1
			from	pls_conta_glosa x
			where	a.nr_seq_conta_glosa = x.nr_sequencia
			and	x.nr_seq_conta = nr_seq_conta_existente_w);
	
	for r_c01_w in C01() loop
		begin
		delete from pls_analise_glo_ocor_grupo a
		where	exists (select	1
				from	pls_ocorrencia_benef x
				where	a.nr_seq_ocor_benef = x.nr_sequencia
				and	x.nr_seq_proc = r_c01_w.nr_sequencia);
		
		delete	pls_ocorrencia_benef
		where	nr_seq_proc = r_c01_w.nr_sequencia;
		
		delete from pls_analise_glo_ocor_grupo a
		where	exists (select	1
				from	pls_conta_glosa x
				where	a.nr_seq_conta_glosa = x.nr_sequencia
				and	x.nr_seq_conta_proc = r_c01_w.nr_sequencia);
		
		delete	pls_conta_glosa
		where	nr_seq_conta_proc = r_c01_w.nr_sequencia;
		
		end;
	end loop;

	for r_c02_w in C02() loop
		begin
		delete from pls_analise_glo_ocor_grupo a
		where	exists (select	1
				from	pls_ocorrencia_benef x
				where	a.nr_seq_ocor_benef = x.nr_sequencia
				and	x.nr_seq_mat = r_c02_w.nr_sequencia);
		
		delete	pls_ocorrencia_benef
		where	nr_seq_mat = r_c02_w.nr_sequencia;
		
		delete from pls_analise_glo_ocor_grupo a
		where	exists (select	1
				from	pls_conta_glosa x
				where	a.nr_seq_conta_glosa = x.nr_sequencia
				and	x.nr_seq_conta_mat = r_c02_w.nr_sequencia);
		
		delete	pls_conta_glosa
		where	nr_seq_conta_mat = r_c02_w.nr_sequencia;
		end;
	end loop;
	
	delete	pls_conta_glosa
	where	(nr_seq_conta = nr_seq_conta_existente_w) ;

else
	if (ie_estagio_complemento_w = 3) and (nr_seq_regra_compl_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort('A guia nao pode ser complementada. Favor entrar em contato com a operadora.');
	end if;

	if	(nvl(ie_permite_apres_parc_p, 'X') = 'N' and nr_seq_guia_p is not null) then
		pls_valida_conta_existente(nr_seq_guia_p, null, ie_conta_existe_w,
					   nr_seq_conta_existente_w,nr_seq_protocolo_existente_w);

	end if;

	if	(ie_conta_existe_w <> 'S') then
		pls_gerar_conta_complemento(	nr_seq_guia_p,
						nr_seq_protocolo_w,
						cd_estabelecimento_w,
						nm_usuario_p,
						nr_seq_conta_w); 
						
		pls_consistir_conta_web(null,
								nr_seq_conta_w,
								'C',
								nm_usuario_p,
								cd_estabelecimento_p,
								null);

ds_log_w := 'Seq guia: ' || nr_seq_guia_p || ' ; Estagio compl : ' || ie_estagio_complemento_w || ' ; Regra compl : ' || nr_seq_regra_compl_w||'; ';								
				
		insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela, 
					ds_log, ds_log_call, ds_funcao_ativa, 
					ie_aplicacao_tasy, nm_maquina, ie_opcao,
					nr_seq_conta)
		values		( 	plsprco_cta_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, 'pls_gerar_protocolo_compl', 
					ds_log_w, dbms_utility.format_call_stack, obter_funcao_ativa, 
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '1',
					nr_seq_conta_w);							
	else
		rollback;
		nr_seq_conta_w	:= null;
		ie_mensagem_ret_w := 'APR';
	end if;
end if;

nr_seq_conta_p		:=	nr_seq_conta_w;

if	(nr_seq_conta_w is not null and nr_seq_prestador_p is not null) then
	update  pls_conta
	set	nr_seq_prestador = nr_seq_prestador_p
	where	nr_sequencia = nr_seq_conta_w
	and	nr_seq_prestador is null; 

	pls_consist_conta_proc_web(nr_seq_conta_w, nr_seq_prestador_p, nm_usuario_p,
				   cd_estabelecimento_w, 'C', ie_situacao_prot_w);
				   
	pls_consist_conta_mat_web(nr_seq_conta_w, nr_seq_prestador_p, nm_usuario_p,
				  cd_estabelecimento_w, 'C', ie_situacao_prot_w);
				  
end if;

if	(ie_tipo_guia_w <> 4 and nr_seq_conta_w is not null) then
	delete from pls_diagnostico_conta
	where  nr_seq_conta = nr_seq_conta_w;
end if;


ie_mensagem_ret_p := ie_mensagem_ret_w;

commit;
 
end  pls_gerar_protocolo_compl;
/