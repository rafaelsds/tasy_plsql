create or replace
procedure gerar_email_conf_geral_pac(
				qt_dias_p		number
				) is 
			

nr_seq_agenda_w		number(10);
			

cursor c01 is
	select 	a.nr_sequencia
	from 	agenda_paciente a,
			agenda g
	where 	trunc(a.dt_agenda) = ( trunc(sysdate) + qt_dias_p )
	and 	a.cd_agenda = g.cd_agenda
	and		g.cd_tipo_agenda = 2
	and 	a.cd_pessoa_fisica is not null
	and		a.ie_status_agenda <> 'C'
	and		a.nr_sequencia = (	select	min(c.nr_sequencia)
								from	agenda_paciente c
								where 	trunc(c.dt_agenda) = ( trunc(sysdate) + qt_dias_p )
								and 	c.cd_pessoa_fisica = a.cd_pessoa_fisica
								and		c.ie_status_agenda <> 'C');
	
		
cursor c02 is
	select 	a.nr_sequencia
	from 	agenda_consulta a,
			agenda	g
	where 	trunc(a.dt_agenda) = ( trunc(sysdate) + qt_dias_p )
	and 	a.cd_agenda = g.cd_agenda
	and		g.cd_tipo_agenda in (3,4)
	and 	a.cd_pessoa_fisica is not null
	and		a.ie_status_agenda <> 'C'
	and	a.nr_sequencia = (	select	min(c.nr_sequencia)
							from	agenda_consulta c
							where 	trunc(c.dt_agenda) = ( trunc(sysdate) + qt_dias_p )
							and		c.ie_status_agenda <> 'C'
							and 	c.cd_pessoa_fisica = a.cd_pessoa_fisica);

	
begin


--Agendas Exame
open c01;
loop
fetch c01 into	
	nr_seq_agenda_w;
exit when c01%notfound;
	begin
	
	gerar_email_conf_agenda(nr_seq_agenda_w,2,'TASY');
	
	end;
end loop;
close c01;



--Agendas Consulta
open c02;
loop
fetch c02 into	
	nr_seq_agenda_w;
exit when c02%notfound;
	begin

	gerar_email_conf_agenda(nr_seq_agenda_w,4,'TASY');
	
	end;
end loop;
close c02;


end gerar_email_conf_geral_pac;
/
