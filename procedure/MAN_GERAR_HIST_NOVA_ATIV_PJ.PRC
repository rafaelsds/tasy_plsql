create or replace
procedure man_gerar_hist_nova_ativ_pj(
			nr_seq_ordem_p		number,
			nr_seq_atividade_p		number,
			nm_usuario_p		varchar2,
			ds_obs_ativ_p		varchar2 default '',
			ie_tipo_hist_p		varchar2 default 'R') is

ds_historico_w		varchar2(4000);
nr_seq_estagio_w		Number(10);
begin

if	(ie_tipo_hist_p = 'R') then
	select	substr(
		substr(wheb_mensagem_pck.get_texto(304428),1,255) || chr(13) || chr(10) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304429),1,255) || to_char(a.dt_atividade,'dd/mm/yyyy hh24:mi:ss') || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304430),1,255) || substr(OBTER_NOME_PF_PJ(null,a.cd_cnpj),1,80) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304431),1,255) || substr(obter_descricao_padrao_pk('MAN_TIPO_FUNCAO_PJ','DS_FUNCAO','NR_SEQUENCIA',a.nr_seq_funcao),1,255) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304433),1,255) || a.qt_minuto || chr(13) || chr(10) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304434),1,255) || a.ds_observacao || chr(13) || chr(10) ||
		decode(ds_obs_ativ_p,'','',' ' || ds_obs_ativ_p || chr(13) || chr(10))|| chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304437),1,255) || a.ds_atividade
		,1,4000)
	into	ds_historico_w
	from	man_ordem_serv_ativ_pj a
	where	a.nr_sequencia = nr_seq_atividade_p;
elsif	(ie_tipo_hist_p = 'P') then
	select	substr(
		substr(wheb_mensagem_pck.get_texto(304439),1,255) || chr(13) || chr(10) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304440),1,255) || substr(a.ds_atividade,1,255) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304441),1,255) || substr(OBTER_NOME_PF_PJ(null,a.cd_cnpj),1,80) || ' (' || a.cd_cnpj || ')' || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304442),1,255) || obter_desc_estagio_proc(man_obter_estagio_ordem(a.NR_SEQ_ORDEM_SERV)) || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(304437),1,255) || a.ds_atividade || chr(13) || chr(10) ||	
		substr(wheb_mensagem_pck.get_texto(304434),1,255) || a.ds_observacao
		,1,4000),
		man_obter_estagio_ordem(a.NR_SEQ_ORDEM_SERV)
	into	ds_historico_w,
		nr_seq_estagio_w
	from	man_ordem_ativ_prev_pj a
	where	a.nr_sequencia = nr_seq_atividade_p;
	
	update	man_ordem_servico
	set	nr_seq_estagio = nr_seq_estagio_w,
		ie_status_ordem = 2
	where	nr_sequencia = nr_seq_ordem_p;
end if;

insert into man_ordem_serv_tecnico(
	nr_sequencia,
	nr_seq_ordem_serv,
	dt_atualizacao,
	nm_usuario,
	ds_relat_tecnico,
	dt_historico,
	dt_liberacao,
	nm_usuario_lib,
	ie_origem)
values(	man_ordem_serv_tecnico_seq.nextval,
	nr_seq_ordem_p,
	sysdate,
	nm_usuario_p,
	wheb_rtf_pck.get_texto_rtf(ds_historico_w),
	sysdate,
	sysdate,
	nm_usuario_p,
	'I');

commit;

end man_gerar_hist_nova_ativ_pj;
/