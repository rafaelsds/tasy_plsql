create or replace
procedure Salvar_adep_proced_partic(	cd_estabelecimento_p		number,
										nr_atendimento_p			number,
										nr_prescricao_p				number,
										nr_seq_procedimento_p		number,
										cd_medico_p					varchar2,
										ie_funcao_p					number,
										nm_usuario_p				varchar2,
										ie_acao_p					varchar2,
										cd_especialidade_p 	in	out	number,
										ds_erro_p				out	varchar2) is 
			
ie_anestesista_w			varchar2(1);
ie_permite_anestesistas_w	varchar2(1);
ie_permite_anestesistas_ww	varchar2(1);
ds_porte_w					varchar2(5);
ie_medico_cadastrado_w		varchar2(1);
ie_consiste_anest_porte_w	varchar2(1);
ds_valor_w					varchar2(255);
nr_seq_proc_pac_w			number(15);
cd_convenio_parametro_w		number(15);
cd_convenio_w				number(15);
cd_categoria_w				number(15);
cd_edicao_amb_w				number(15);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(15);
dt_conta_w					Date;
ds_macro_erro_w				varchar2(255) := '';
nr_erro_w					number(15) := 0;

begin

if (cd_medico_p is not null) then

	select	nvl(max('S'),'N')
	into	ie_medico_cadastrado_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_medico_p;
	
	if (ie_medico_cadastrado_w = 'N') then
		-- M�dico n�o cadastrado!
		nr_erro_w := 195849;
	end if;
end if;	

if	(nr_erro_w = 0) then
	
	select	max(cd_convenio_parametro)
	into	cd_convenio_parametro_w
	from	conta_paciente
	where	nr_atendimento = nr_atendimento_p;

	select	max(nr_sequencia)
	into	nr_seq_proc_pac_w
	from	procedimento_paciente
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = nr_seq_procedimento_p;

	select	max(cd_convenio),
			max(cd_categoria),
			nvl(max(cd_edicao_amb),0),
			max(cd_procedimento),
			max(ie_origem_proced),
			max(dt_conta)
	into	cd_convenio_w, 
			cd_categoria_w, 
			cd_edicao_amb_w, 
			cd_procedimento_w, 
			ie_origem_proced_w, 
			dt_conta_w
	from	procedimento_paciente
	where	nr_sequencia = nr_seq_proc_pac_w;

	select 	nvl(max(ie_consiste_anest_porte),'N')
	into	ie_consiste_anest_porte_w
	from 	parametro_faturamento 
	where  	cd_estabelecimento = cd_estabelecimento_p;

	if (ie_consiste_anest_porte_w = 'R') then
		select	nvl(max(obter_regra_tipo_atend_porte(nr_atendimento_p)),'N')
		into	ie_consiste_anest_porte_w
		from	dual;
	end if;
	
	if (ie_acao_p = 'I') then

		select	nvl(max(ie_anestesista),'N')
		into	ie_anestesista_w
		from	funcao_medico
		where	cd_funcao = ie_funcao_p;
		
		if	(ie_anestesista_w = 'S') and
			(ie_consiste_anest_porte_w in ('S','P')) then
			select	nvl(max(Obter_Dados_Preco_Proc(cd_estabelecimento_p, cd_convenio_w, cd_categoria_w, cd_edicao_amb_w, cd_procedimento_w, nvl(ie_origem_proced_w,0), dt_conta_w, 'P')),'N'),
					nvl(max(Obter_Dados_Preco_Proc(cd_estabelecimento_p, cd_convenio_w, cd_categoria_w, cd_edicao_amb_w, cd_procedimento_w, ie_origem_proced_w, dt_conta_w, 'PN')),'N')
			into	ie_permite_anestesistas_w,
					ie_permite_anestesistas_ww
			from	procedimento_paciente
			where	nr_sequencia = nr_seq_proc_pac_w;
			
			if	(((ie_consiste_anest_porte_w = 'S') and (ie_permite_anestesistas_w = 'N')) or
				 ((ie_consiste_anest_porte_w = 'N') and (ie_permite_anestesistas_ww = 'N'))) then
				-- N�o � permitido o lan�amento de anestesistas para procedimentos sem porte anest�sico. Verifique os par�metros do faturamento.
				nr_erro_w := 195860;
			end if;
		end if;
	end if;

	if	(nr_erro_w = 0) then
		select	max(Obter_convenio_regra_atend(cd_medico_p, cd_convenio_parametro_w, ie_tipo_atendimento, cd_estabelecimento_p, 'P', null, null))
		into	ie_medico_cadastrado_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;

		if	(ie_medico_cadastrado_w = 'N') then
			
			select	max(substr(obter_nome_convenio(cd_convenio_parametro_w),1,255))
			into	ds_valor_w
			from	dual;
			
			-- O m�dico n�o est� cadastrado para participar dos procedimentos para o conv�nio #@DS_CONVENIO#@
			nr_erro_w			:= 195842;
			ds_macro_erro_w		:= 'DS_CONVENIO=' || ds_valor_w;
		else if	(ie_acao_p in ('I','E')) and
				(nvl(cd_especialidade_p,-1) = -1) then
				
				select	max(obter_especialidade_medico(cd_medico_p,'C'))
				into	cd_especialidade_p
				from	dual;
				
			end if;
		end if;
	end if;
end if;

if	(nr_erro_w > 0) then
	ds_erro_p := obter_texto_dic_objeto(nr_erro_w, 1, ds_macro_erro_w);
end if;

end Salvar_adep_proced_partic;
/