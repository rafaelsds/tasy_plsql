create or replace
procedure	sup_liberar_relat_segmento(			
			nr_sequencia_p		number,
			nr_seq_item_p		number,
			ie_forma_liberacao_p	varchar2,
			nm_usuario_p		varchar2) is

/* Forma de Libera��o
R : Relat�rio
I : Item
*/

nr_sequencia_w			number(10);
nr_seq_status_item_w		number(10);
cd_setor_atendimento_w		number(5);
cd_pessoa_resp_w			varchar2(10);
nr_seq_parecer_w			number(10);
nr_seq_primeiro_parec_w		number(10);
qt_existe_w			number(10);
cd_pessoa_resp_lc_w		varchar2(10);

cursor	c01 is
select	nr_sequencia,
	nr_seq_status_item,
	cd_pessoa_resp_lc
from	segmento_compras_rel_it
where	nr_seq_seg_rel = nr_sequencia_p
and	dt_liberacao is null
and	((ie_forma_liberacao_p = 'R') or
	(ie_forma_liberacao_p = 'I' and nr_sequencia = nr_seq_item_p))
order by	1;

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_seq_status_item_w,
	cd_pessoa_resp_lc_w;
exit when c01%notfound;
	begin

	update	segmento_compras_rel_it
	set	dt_liberacao = sysdate,
		nm_usuario_lib = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_w;

	select	segmento_compras_rel_parec_seq.nextval
	into	nr_seq_parecer_w
	from	dual;

	insert into segmento_compras_rel_parec(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		nr_seq_item_rel,
		dt_liberacao) values (
			nr_seq_parecer_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_resp_lc_w,
			nr_sequencia_w,
			sysdate);

	sup_envia_ci_resp_parec_lc(nr_seq_parecer_w,nm_usuario_p);

	end;
end loop;
close c01;

select	count(*)
into	qt_existe_w
from	segmento_compras_rel_it
where	nr_seq_seg_rel = nr_sequencia_p
and	dt_liberacao is null;

if	(ie_forma_liberacao_p = 'R') or
	(qt_existe_w = 0) then
	begin

	update	segmento_compras_rel
	set	dt_liberacao = sysdate,
		nm_usuario_lib = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

	end;
end if;

commit;

end sup_liberar_relat_segmento;
/