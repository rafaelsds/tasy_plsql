create or replace
procedure obter_moeda_cotacao_conta(
							nr_interno_conta_p 	    	number,
							cd_moeda_p 				out number,
							vl_cotacao_p			out number							
							) is

cd_moeda_w			CONTA_PACIENTE_EXCEDENTE.CD_MOEDA%type;
vl_cotacao_w		CONTA_PACIENTE_EXCEDENTE.VL_COTACAO%type;
begin

cd_moeda_w 	:= 0;
vl_cotacao_w 	:= 0;

begin
	select 	cd_moeda,
			vl_cotacao			
	into	cd_moeda_w,
			vl_cotacao_w			
	from   	conta_paciente_excedente
	where  	nr_interno_conta = nr_interno_conta_p;
exception
when others then
	cd_moeda_w 			:= 0;
	vl_cotacao_w 		:= 0;	
end;

cd_moeda_p 			:= cd_moeda_w;
vl_cotacao_p 		:= vl_cotacao_w;

end obter_moeda_cotacao_conta;
/
