create or replace
procedure carrega_itens_cartao(	cd_material_p			Number,                         
				qt_material_p			Number,
				nr_seq_lote_fornec_p		Number,                                      
				nr_seq_cartao_p			Number,                                      
				cd_estabelecimento_p		Number,
				nm_usuario_retirada_p		varchar2,
				nm_usuario_p			Varchar2) is                                                 

nr_sequencia_w		number(10,0);                                                   
ie_consignado_w		varchar2(1);                                                   
cd_unidade_medida_w	varchar2(30);                                               
cd_cgc_fornec_w		varchar2(14);                                                  

begin                                                                           
select	substr(obter_dados_material(cd_material_p,'UMC'),1,30)                    
into	cd_unidade_medida_w                                                        
from	dual;                                                                      

select	nvl(max(ie_consignado),'0')                                                   
into	ie_consignado_w                                                            
from	material                                                                   
where	cd_material = cd_material_p;                                              


if	(ie_consignado_w <> '0') then                                                
	begin                                                                          
	if	(nvl(nr_seq_lote_fornec_p, 0) > 0) then                                     
		select	max(cd_cgc_fornec)
		into	cd_cgc_fornec_w                                                          
		from	material_lote_fornec                                                     
		where	nr_sequencia = nr_seq_lote_fornec_p;                                    
	else                                                                           
		cd_cgc_fornec_w	:= obter_fornecedor_regra_consig(cd_estabelecimento_p, cd_material_p, '1');                                                                      
	end if;                                                                        
	end;                                                                           
end if;                                                                         

if	(qt_material_p > 0) then                                                     

	select	sup_lanc_cartao_item_seq.nextval                                        
	into	nr_sequencia_w                                                            
	from	dual;                                                                     

	insert	into sup_lanc_cartao_item(                                              
			nr_sequencia,                                                                
			dt_atualizacao,                                                              
			nm_usuario,                                                                  
			dt_atualizacao_nrec,                                                         
			nm_usuario_nrec,                                                             
			nr_seq_lancamento,                                                           
			cd_material,                                                                 
			qt_material,                                                                 
			cd_unidade_medida,                                                           
			nr_seq_lote_fornec,
			nm_usuario_receb,
			ds_observacao)                                                               
	values	(	nr_sequencia_w,                                                       
			sysdate,                                                                     
			nm_usuario_p,                                                                
			sysdate,                                                                     
			'N',                                                                         
			nr_Seq_cartao_p,                                                             
			cd_material_p,                                                               
			qt_material_p,                                                               
			cd_unidade_medida_w,                                                         
			decode(nr_seq_lote_fornec_p,0,null,nr_seq_lote_fornec_p),
			nm_usuario_retirada_p,
			null);                                                                       
end if;                                                                         

end carrega_itens_cartao;                                                       
/