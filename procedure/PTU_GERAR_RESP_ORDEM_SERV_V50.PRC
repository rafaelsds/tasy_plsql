create or replace
procedure ptu_gerar_resp_ordem_serv_v50
			(	nr_seq_retorno_p	Number,
				nr_versao_ptu_p		Varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a transa��o PTU de resposta de ordem de servi�o, via SCS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ie_tipo_cliente_w		Varchar2(2);
cd_unimed_executora_w		Number(4);
cd_unimed_beneficiario_w	varchar2(4);
nr_transacao_solicitante_w	Number(10);
cd_unimed_w			Number(4);
cd_usuario_plano_w		Varchar2(17);
nr_seq_guia_w			Number(10);
cd_servico_w			Number(8);
ie_origem_proced_w		Number(2);
qt_servico_aut_w		Number(4);
ie_status_requisicao_w		Number(2);
ie_status_w			Varchar2(2);
nr_seq_resposta_w		Number(10);
ie_classificacao_w		Number(2);
ie_tipo_despesa_w		Number(2);
ie_tipo_tabela_w		Varchar2(2);
nr_seq_prestador_w		Number(10);
nm_prest_alto_custo_w		Varchar2(60)	:= '';
ie_prest_alto_custo_w		Varchar2(2);
nr_seq_requisicao_w		Number(10);
cd_unimed_solicitante_w		number(4);
nr_seq_material_w		pls_material.nr_sequencia%type;
nr_seq_resp_req_serv_w		ptu_resposta_req_servico.nr_sequencia%type;
qt_registros_w			pls_integer;
ie_aceito_w			Varchar2(1);
nr_seq_ordem_serv_w		number(10);

Cursor C01 is
	select	cd_servico,
		cd_servico_consersao,
		ie_origem_servico,
		ie_tipo_tabela,
		qt_servico_aut
	from	ptu_req_ord_serv_servico
	where	nr_seq_req_ord = nr_seq_ordem_serv_w;

begin

select	nvl(nr_seq_ordem_serv,0)
into	nr_seq_ordem_serv_w
from	ptu_controle_execucao
where	nr_sequencia	= nr_seq_retorno_p;

select	ie_tipo_cliente,
	cd_unimed_executora,
	cd_unimed_beneficiario,
	nr_transacao_solicitante,
	cd_unimed,
	cd_usuario_plano,
	nr_seq_guia,
	nr_seq_requisicao,
	nvl(nr_seq_prestador,0),
	cd_unimed_solicitante
into	ie_tipo_cliente_w,
	cd_unimed_executora_w,
	cd_unimed_beneficiario_w,
	nr_transacao_solicitante_w,
	cd_unimed_w,
	cd_usuario_plano_w,
	nr_seq_guia_w,
	nr_seq_requisicao_w,
	nr_seq_prestador_w,
	cd_unimed_solicitante_w
from	ptu_requisicao_ordem_serv
where	nr_sequencia	= nr_seq_ordem_serv_w;

select	ptu_resposta_req_ord_serv_seq.NextVal
into	nr_seq_resposta_w
from	dual;

insert	into ptu_resposta_req_ord_serv
	(nr_sequencia, cd_transacao, ie_tipo_cliente,
	 cd_unimed_executora, cd_unimed_beneficiario, cd_unimed_solicitante,
	 nr_seq_execucao, cd_unimed, cd_usuario_plano, 
	 nr_seq_origem, nm_prest_alto_custo, nr_seq_requisicao, 
	 nr_seq_guia, ds_observacao, nm_usuario, 
	 dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec, 
	 nr_versao)
values	(nr_seq_resposta_w, '00807', ie_tipo_cliente_w,
	 cd_unimed_executora_w, cd_unimed_beneficiario_w, cd_unimed_solicitante_w,
	 nr_seq_retorno_p, cd_unimed_w, cd_usuario_plano_w, 
	 nr_transacao_solicitante_w, nm_prest_alto_custo_w, nr_seq_requisicao_w, 
	 nr_seq_guia_w, '', nm_usuario_p, 
	 sysdate, nm_usuario_p, sysdate, 
	 nr_versao_ptu_p);

for	r_c01_w	in c01	loop
	if	(r_c01_w.ie_tipo_tabela	in('0','1','4')) then
		if	(cd_unimed_solicitante_w	= cd_unimed_beneficiario_w) then
			ie_aceito_w	:= ptu_obter_item_req_ord_aceito(	nvl(r_c01_w.cd_servico_consersao, r_c01_w.cd_servico), r_c01_w.ie_origem_servico, null,
										cd_unimed_beneficiario_w);
		else
			ie_aceito_w	:= 'S';
		end if;

		select	ptu_resposta_req_servico_seq.NextVal
		into	nr_seq_resp_req_serv_w
		from	dual;

		insert	into ptu_resposta_req_servico 
			(nr_sequencia, nr_seq_resp_req_ord, cd_servico,
			 qt_servico_aut, ie_status_requisicao, ie_tipo_tabela,
			 nm_usuario, dt_atualizacao, nm_usuario_nrec,
			 dt_atualizacao_nrec)
		values	(nr_seq_resp_req_serv_w, nr_seq_resposta_w, r_c01_w.cd_servico,
			 decode(ie_aceito_w,'S',r_c01_w.qt_servico_aut,0), decode(ie_aceito_w,'S',2,1), r_c01_w.ie_tipo_tabela,
			 nm_usuario_p, sysdate, nm_usuario_p,
			 sysdate);

		if	(ie_aceito_w	= 'N') then
			ptu_inserir_inconsistencia(	null, null, 2010,
							'',cd_estabelecimento_p, nr_seq_resposta_w, 
							'OR', '00807', nr_seq_resp_req_serv_w, 
							null, null, nm_usuario_p);
		end if;
	elsif	(r_c01_w.ie_tipo_tabela	in('2','3')) then
		nr_seq_material_w	:= substr(pls_obter_dados_material_a900(nvl(r_c01_w.cd_servico_consersao, r_c01_w.cd_servico),null,'NR'),1,255);
		
		if	(nvl(nr_seq_material_w,0)	= 0) then
			nr_seq_material_w	:= substr(pls_obter_seq_codigo_material('',nvl(r_c01_w.cd_servico_consersao, r_c01_w.cd_servico)),1,8);
		end if;

		if	(cd_unimed_solicitante_w	= cd_unimed_beneficiario_w) then
			ie_aceito_w	:= ptu_obter_item_req_ord_aceito(null, null, nr_seq_material_w, cd_unimed_beneficiario_w);
		else
			ie_aceito_w	:= 'S';
		end if;
		
		select	ptu_resposta_req_servico_seq.NextVal
		into	nr_seq_resp_req_serv_w
		from	dual;

		insert	into ptu_resposta_req_servico 
			(nr_sequencia, nr_seq_resp_req_ord, cd_servico,
			 qt_servico_aut, ie_status_requisicao, ie_tipo_tabela,
			 nm_usuario, dt_atualizacao, nm_usuario_nrec,
			 dt_atualizacao_nrec)
		values	(nr_seq_resp_req_serv_w, nr_seq_resposta_w, r_c01_w.cd_servico,
			 decode(ie_aceito_w,'S',r_c01_w.qt_servico_aut,0), decode(ie_aceito_w,'S',2,1), r_c01_w.ie_tipo_tabela,
			 nm_usuario_p, sysdate, nm_usuario_p,
			 sysdate);

		if	(ie_aceito_w	= 'N') then
			ptu_inserir_inconsistencia(	null, null, 2010,
							'',cd_estabelecimento_p, nr_seq_resposta_w, 
							'OR', '00807', null, 
							nr_seq_resp_req_serv_w, null, nm_usuario_p);
		end if;
	end if;
end loop;

select	count(1)
into	qt_registros_w
from	ptu_resposta_req_servico
where	ie_status_requisicao	= 2
and	nr_seq_resp_req_ord	= nr_seq_resposta_w;

if	(qt_registros_w	> 0) then
	-- Se a resposta da ordem de servi�o vier com servi�os recusados e aceitos, a ordem de servi�o fica pendente de pedido de autoriza��o
	update	ptu_requisicao_ordem_serv
	set	ie_estagio			= 8
	where	nr_sequencia		= nr_seq_ordem_serv_w;
else
	-- Se a resposta da ordem de servi�o for totalmente recusada, se encerra o processo
	update	ptu_requisicao_ordem_serv
	set	ie_estagio			= 4
	where	nr_sequencia		= nr_seq_ordem_serv_w;
end if;

commit;

end ptu_gerar_resp_ordem_serv_v50;
/
