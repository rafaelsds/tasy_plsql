create or replace
procedure verifica_regra_serv_reg	(nr_seq_tipo_servico_p	number,
					cd_estabelecimento_p 	number,
					ds_erro_p 		out varchar2) is
						
ie_regra_w		VARCHAR2(1);
ie_convenio_w		VARCHAR2(1);

begin
	if (nr_seq_tipo_servico_p is not null) then

		select 	max(a.ie_exige_pessoa_atendida),
			max(a.ie_exige_usuario_convenio)
		into	ie_regra_w,
			ie_convenio_w
		from	eme_tipo_servico a
		where	a.nr_sequencia 	     = nr_seq_tipo_servico_p
		and 	nvl(a.ie_situacao,'A') = 'A'
		and	a.cd_estabelecimento = cd_estabelecimento_p;

			if (ie_regra_w = 'S') then
			begin	      
			ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(311698);
			end;
			end if;
			
			if (ie_convenio_w = 'S') then
			begin
			ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(311699);
			end;
			end if;
	end if;	


end verifica_regra_serv_reg;
/
