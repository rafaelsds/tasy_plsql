create or replace
procedure gerar_agend_serv_pac_atend(	nr_seq_paciente_p 	number,
					nr_seq_atendimento_p	number,
					nm_usuario_p		varchar2,
					ds_erro_p	out	varchar2) is 
			
	
dt_agendamento_w	date;
dt_agenda_w		date;
cd_agenda_w		number(10);
ie_classif_agenda_w  	varchar2(5);
nr_sequencia_w		number(10);
nr_seq_agenda_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_resp_w	varchar2(10);	
ds_erro_w		varchar2(2000) := '';
nr_seq_atendimento_w	number(10);
	
cursor c01 is
	select	dt_prevista,
		nr_seq_atendimento
	from	paciente_atendimento
	where	(nr_seq_paciente	= nr_seq_paciente_p)
	and	((nr_seq_atendimento_p = nr_seq_atendimento) or ((nr_seq_atendimento_p = 0) and (dt_real is null)))
	order 	by 1;					
					
begin

ds_erro_w := null;

select	max(nr_sequencia)
into	nr_sequencia_w
from 	regra_agenda_serv_ciclo;


if (nr_sequencia_w is not null) then

	select 	max(cd_agenda),
		max(ie_classif_agenda)
	into	cd_agenda_w,
		ie_classif_agenda_w
	from	regra_agenda_serv_ciclo
	where	nr_sequencia = nr_sequencia_w;


	select	max(cd_pessoa_fisica),
		max(cd_medico_resp)
	into	cd_pessoa_fisica_w,
		cd_medico_resp_w
	from	paciente_setor
	where	nr_seq_paciente = nr_seq_paciente_p;


	if not(cd_agenda_w is null) and
	   not(ie_classif_agenda_w is null)	 then

		open c01;
		loop
		fetch c01 into	
			dt_agendamento_w,
			nr_seq_atendimento_w;
		exit when c01%notfound;
			begin
				
			--Atualiza agenda de servi�o
			Gerar_Horario_Agenda_Servico(obter_estab_agenda(cd_agenda_w), cd_agenda_w, trunc(dt_agendamento_w),nm_usuario_p );	
				
			
			select	max(dt_agenda)
			into	dt_agenda_w
			from	agenda_consulta
			where	trunc(dt_agenda) = trunc(dt_agendamento_w)
			and	ie_status_agenda = 'L'
			and	dt_agenda 	<= dt_agendamento_w
			and	cd_agenda 	= cd_agenda_w
			and	ie_classif_agenda = ie_classif_agenda_w;
			
			nr_seq_agenda_w := null;
			
			if (dt_agenda_w is not null) then
				
				select	max(nr_sequencia)
				into	nr_seq_agenda_w
				from	agenda_consulta
				where	trunc(dt_agenda) = trunc(dt_agendamento_w)
				and	ie_status_agenda = 'L'
				and	dt_agenda 	= dt_agenda_w
				and	cd_agenda 	= cd_agenda_w
				and	ie_classif_agenda = ie_classif_agenda_w;
				
			end if;	
				
				
			if (nr_seq_agenda_w is null) then
			
				ds_erro_w := ds_erro_w||' | '||to_char(dt_agendamento_w,'dd/mm/yyyy hh24:mi:ss');	
			else
			
				update 	agenda_consulta
				set	nm_usuario 	    = nm_usuario_p,
					dt_atualizacao	    = sysdate,
					ie_status_agenda    = 'N',
					ie_classif_agenda   = ie_classif_agenda_w,
					cd_agenda	    = cd_agenda_w,
					cd_pessoa_fisica    = cd_pessoa_fisica_w,
					cd_medico_solic	    = cd_medico_resp_w		
				where	nr_sequencia 	    = nr_seq_agenda_w;
				
				
				update	paciente_atendimento
				set	nr_seq_agenda_cons = nr_seq_agenda_w
				where	nr_seq_atendimento = nr_seq_atendimento_w;
				
				commit;
				
			end if;
			
			end;
		end loop;
		close c01;

	end if;

	if (ds_erro_w is not null) then
			
		select 	substr(substr(ds_erro_w,3,length(ds_erro_w)),1,255)
		into	ds_erro_w
		from	dual;
				
	end if;

	ds_erro_p := ds_erro_w;

end if;

end gerar_agend_serv_pac_atend;
/