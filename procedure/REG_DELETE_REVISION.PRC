CREATE OR REPLACE PROCEDURE reg_delete_revision(nr_seq_revision_p	reg_revisao_documento.nr_sequencia%TYPE) AS
BEGIN
	DELETE FROM	reg_aprovacao_documento
	WHERE		nr_seq_revisao = nr_seq_revision_p;
	
	DELETE FROM	reg_referencia_documento
	WHERE		nr_seq_revisao = nr_seq_revision_p;
	
	DELETE FROM	reg_repositorio_documento
	WHERE		nr_seq_revisao = nr_seq_revision_p;
	
	DELETE FROM	reg_revisao_documento
	WHERE		nr_sequencia = nr_seq_revision_p;
	
	COMMIT;
END reg_delete_revision;
/
