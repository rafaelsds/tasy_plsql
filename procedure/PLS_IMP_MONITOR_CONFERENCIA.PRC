create or replace
procedure pls_imp_monitor_conferencia (	nr_seq_lote_p		in out 	pls_lot_monit_tiss_confer.nr_sequencia%type,
					ds_conteudo_p		in	varchar2,
					nm_arquivo_p		in	varchar2,
					nm_usuario_p		in	usuario.nm_usuario%type) is 

dt_mes_competencia_w		pls_lot_monit_tiss_confer.dt_mes_competencia%type;
nr_seq_lote_w			pls_lot_monit_tiss_confer.nr_sequencia%type;
nr_seq_lote_arquivo_w	pls_lot_monit_tiss_confer.nr_sequencia%type;
ds_conteudo_w			clob;
ds_valor_w			varchar2(255);
ds_mascara_w			varchar2(10);
index_w				pls_integer;
nr_registro_ans_w		pls_monit_tiss_confer.cd_operadora_ans%type;
cd_cnes_w			pls_monit_tiss_confer.cd_cnes_prest_exec%type;
ie_identificador_w		pls_monit_tiss_confer.ie_identif_prest_exec%type;
cd_cpf_cnpj_w			pls_monit_tiss_confer.cd_cpf_cgc_prest_exec%type;
cd_municipio_prest_w		pls_monit_tiss_confer.cd_municipio_prest_exec%type;
nr_reg_oper_inter_w		pls_monit_tiss_confer.nr_registro_operadora_inter%type;
ie_tipo_atendimento_int_w	pls_monit_tiss_confer.ie_tipo_atend_oper%type;
cd_cns_w			pls_monit_tiss_confer.nr_cartao_nac_sus%type;
ie_sexo_w			pls_monit_tiss_confer.ie_sexo%type;
dt_nasc_benef_w			pls_monit_tiss_confer.dt_nascimento%type;
cd_municipio_benef_w		pls_monit_tiss_confer.cd_municipio_benef%type;
nr_plano_w			pls_monit_tiss_confer.cd_plano_ans%type;
ie_tipo_guia_w			pls_monit_tiss_confer.ie_tipo_guia%type;
ie_origem_guia_w		pls_monit_tiss_confer.ie_origem_evento%type;
cd_guia_prestador_w		pls_monit_tiss_confer.cd_guia_prestador%type;
cd_guia_operadora_w		pls_monit_tiss_confer.cd_guia_operadora%type;
nr_reembolso_w			pls_monit_tiss_confer.ie_reembolso%type;
cd_guia_referencia_w		pls_monit_tiss_confer.cd_guia_principal%type;
nr_sadt_princ_w			pls_monit_tiss_confer.cd_guia_sadt_princ%type;
dt_realizacao_w			pls_monit_tiss_confer.dt_realizacao%type;
dt_inicio_fat_w			pls_monit_tiss_confer.dt_inicio_faturamento%type;
dt_fim_fat_w			pls_monit_tiss_confer.dt_fim_internacao%type;
dt_processamento_w		pls_monit_tiss_confer.dt_processamento%type;
cd_cbo_w			pls_monit_tiss_confer.cd_cbo_executante%type;
cd_diag_princ_w			pls_monit_tiss_confer.cd_diag_principal%type;
cd_diag_sec_w			pls_monit_tiss_confer.cd_diag_secundario%type;
cd_diag_terc_w			pls_monit_tiss_confer.cd_diag_terceario%type;
cd_diag_quarto_w		pls_monit_tiss_confer.cd_diag_quartenario%type;
ie_tipo_atend_w			pls_monit_tiss_confer.ie_tipo_atendimento%type;
ie_tipo_faturamento_w		pls_monit_tiss_confer.ie_tipo_faturamento%type;
cd_motivo_encerramento_w	pls_monit_tiss_confer.ie_motivo_encerramento%type;
vl_apresentado_w		pls_monit_tiss_confer.vl_cobranca_guia%type;
vl_glosa_w			pls_monit_tiss_confer.vl_total_glosa%type;
vl_liberado_w			pls_monit_tiss_confer.vl_total_pago%type;
vl_pag_fornec_w			pls_monit_tiss_confer.vl_total_fornecedor%type;
vl_pag_tab_propria_w		pls_monit_tiss_confer.vl_total_tabela_propria%type;
vl_coparticipacao_w		pls_monit_tiss_confer.vl_total_copart%type;
					
begin

--Essa rotina � chamada m�ltiplicas vezes via aplica��o, na primeira, estar� passando nr_seq_lote_p null e ent�o verificar� 
-- j� existe um lote para a compet�ncia do arquivo sendo importado, caso j� tiver, utilizar� essa sequ�ncia de lote, caso contr�rio, 
--criar� um registro novo. Verificado ainda nessa primeira etapa, se j� foi importado arquivo com esse nome.
if	(nr_seq_lote_p is null) then
	
	dt_mes_competencia_w := to_date(substr(nm_arquivo_p, 8, 6),'YYYYMM');
	
	select	max(nr_sequencia)
	into	nr_seq_lote_w
	from	pls_lot_monit_tiss_confer
	where	dt_mes_competencia = dt_mes_competencia_w;
	
	if	(nr_seq_lote_w is null) then
	
		insert	into	pls_lot_monit_tiss_confer
			(	nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
				nm_usuario, nm_usuario_nrec, dt_mes_competencia,
				nm_arquivo)	
		values	(	pls_lot_monit_tiss_confer_seq.nextval, sysdate, sysdate,
				nm_usuario_p, nm_usuario_p, dt_mes_competencia_w,
				nm_arquivo_p)		
			returning nr_sequencia into nr_seq_lote_w;
	else
	
		--Caso tiver lote j� criado para compet�ncia, verifica ainda se h� algum que tenha mesmo nome do arquivo que est� sendo importado, 
		--barrando a importa��o e exibindo mensagem  em caso afirmativo. Essa rotina � chamada para cada linha do arquivo � partir de 
		--loop no Delphi e ent�o essa verifica��o somente deve ororrer quando n�o est� passando o nr_seq_lote_p pois quer dizer que � a primeira
		--itera��o, se passar pela primeira itera��o significa que o arquivo ainda n�o foi importado com esse nome, ent�o deve permitir a itera��o 
		-- sobre as demais linhas do arquivo.

		select	max(nr_sequencia)
		into 	nr_seq_lote_arquivo_w
		from	pls_lot_monit_tiss_confer
		where	dt_mes_competencia = dt_mes_competencia_w
		and		nm_arquivo = nm_arquivo_p;			
	
		if (nr_seq_lote_arquivo_w is not null) then
			--wheb_mensagem_pck.exibir_mensagem_abort(1029520);
			nr_seq_lote_p := -1;
		end if;
	
	end if;
else

	nr_seq_lote_w := nr_seq_lote_p;	
end if;


if ( nvl(nr_seq_lote_p,0) != -1) then
	ds_conteudo_w := ds_conteudo_p;
	index_w := 0;

	while 	(ds_conteudo_w is not null) loop
		
		if	(instr(ds_conteudo_w,';') = 0) then
			ds_valor_w	:= ds_conteudo_w;
			ds_conteudo_w	:= null;
		else
			ds_valor_w := substr(ds_conteudo_w, 1, instr(ds_conteudo_w,';') -1);
			ds_conteudo_w := substr(ds_conteudo_w,instr(ds_conteudo_w,';') + 1,length(ds_conteudo_w));
		end if;
		
		index_w := index_w + 1;
		ds_mascara_w := null;
		
		if	(instr(ds_valor_w,'/') > 0) then
			ds_mascara_w := 'dd/mm/yy';
		elsif	(instr(ds_valor_w,'-') > 0) then
			ds_mascara_w := 'yyyy-mm-dd';
		end if;
		
		if	(index_w = 1) then
			nr_registro_ans_w := ds_valor_w;
		elsif	(index_w = 2) then
			cd_cnes_w := ds_valor_w;
		elsif	(index_w = 3) then
			ie_identificador_w := ds_valor_w;
		elsif	(index_w = 4) then
			cd_cpf_cnpj_w := ds_valor_w;
		elsif	(index_w = 5) then
			cd_municipio_prest_w := ds_valor_w;
		elsif	(index_w = 6) then
			nr_reg_oper_inter_w := ds_valor_w;
		elsif	(index_w = 7) then
			ie_tipo_atendimento_int_w := ds_valor_w;
		elsif	(index_w = 8) then
			cd_cns_w := ds_valor_w;
		elsif	(index_w = 9) then
			ie_sexo_w := ds_valor_w;
		elsif	(index_w = 10) then
			dt_nasc_benef_w :=  to_date(ds_valor_w,ds_mascara_w);
		elsif	(index_w = 11) then
			cd_municipio_benef_w := ds_valor_w;
		elsif	(index_w = 12) then
			nr_plano_w := ds_valor_w;
		elsif	(index_w = 13) then
			ie_tipo_guia_w := ds_valor_w;
		elsif	(index_w = 14) then
			ie_origem_guia_w := ds_valor_w;
		elsif	(index_w = 15) then
			cd_guia_prestador_w := ds_valor_w;
		elsif	(index_w = 16) then
			cd_guia_operadora_w := ds_valor_w;
		elsif	(index_w = 17) then
			nr_reembolso_w := ds_valor_w;
		elsif	(index_w = 18) then
			cd_guia_referencia_w := ds_valor_w;
		elsif	(index_w = 19) then
			nr_sadt_princ_w := ds_valor_w;
		elsif	(index_w = 20) then
			dt_realizacao_w := to_date(ds_valor_w,ds_mascara_w);
		elsif	(index_w = 21) then
			dt_inicio_fat_w := to_date(ds_valor_w,ds_mascara_w);
		elsif	(index_w = 22) then
			dt_fim_fat_w := to_date(ds_valor_w,ds_mascara_w);
		elsif	(index_w = 23) then
			dt_processamento_w := to_date(ds_valor_w,ds_mascara_w);
		elsif	(index_w = 24) then
			cd_cbo_w := ds_valor_w;
		elsif	(index_w = 25) then
			cd_diag_princ_w := substr(ds_valor_w,1,4);
		elsif	(index_w = 26) then
			cd_diag_sec_w := substr(ds_valor_w,1,4);
		elsif	(index_w = 27) then
			cd_diag_terc_w := substr(ds_valor_w,1,4);
		elsif	(index_w = 28) then
			cd_diag_quarto_w := substr(ds_valor_w,1,4);
		elsif	(index_w = 29) then
			ie_tipo_atend_w := ds_valor_w;
		elsif	(index_w = 30) then
			ie_tipo_faturamento_w := ds_valor_w;
		elsif	(index_w = 31) then
			cd_motivo_encerramento_w := ds_valor_w;
		elsif	(index_w = 32) then
			vl_apresentado_w := ds_valor_w;
		elsif	(index_w = 33) then
			vl_glosa_w := ds_valor_w;
		elsif	(index_w = 34) then
			vl_liberado_w := ds_valor_w;
		elsif	(index_w = 35) then
			vl_pag_fornec_w := ds_valor_w;
		elsif	(index_w = 36) then
			vl_pag_tab_propria_w := ds_valor_w;
		elsif	(index_w = 37) then
			vl_coparticipacao_w := ds_valor_w;
		end if;		
	end loop;

	/*
	Tipo guia - ANS
	1 - Consulta
	2 - SADT
	3 - Interna��o
	4 - Odontol�gico
	5 - HI

	Tipo guia - Tasy
	3 - Consulta
	4 - SADT
	5 - Interna��o
	11 - Odontol�gico
	6 - HI
	*/

	select	decode(ie_tipo_guia_w,'1','3','2','4','3','5','4','11','5','6')
	into	ie_tipo_guia_w
	from	dual;

	insert	into	pls_monit_tiss_confer
		(	cd_cbo_executante, cd_cnes_prest_exec, cd_cpf_cgc_prest_exec,
			cd_guia_operadora, cd_guia_prestador, cd_guia_principal,
			cd_guia_sadt_princ, cd_municipio_benef, cd_municipio_prest_exec,
			cd_operadora_ans, cd_plano_ans, dt_atualizacao,
			dt_atualizacao_nrec, dt_fim_internacao, dt_inicio_faturamento,
			dt_nascimento, dt_processamento, dt_realizacao, 
			ie_identif_prest_exec, ie_motivo_encerramento, ie_origem_evento, 
			ie_reembolso, ie_sexo, ie_tipo_atendimento, 
			ie_tipo_atend_oper, ie_tipo_faturamento, ie_tipo_guia, 
			nm_usuario, nm_usuario_nrec, nr_cartao_nac_sus, 
			nr_registro_operadora_inter, nr_seq_lote, nr_sequencia, 
			vl_cobranca_guia, vl_total_copart, vl_total_fornecedor, 
			vl_total_glosa, vl_total_pago, vl_total_tabela_propria,
			cd_diag_principal, cd_diag_secundario, cd_diag_terceario,
			cd_diag_quartenario    )
	values	(	cd_cbo_w, cd_cnes_w, cd_cpf_cnpj_w,
			cd_guia_operadora_w, cd_guia_prestador_w, cd_guia_referencia_w,
			nr_sadt_princ_w, cd_municipio_benef_w, cd_municipio_prest_w,
			nr_registro_ans_w, nr_plano_w, sysdate,
			sysdate, dt_fim_fat_w, dt_inicio_fat_w,
			dt_nasc_benef_w, dt_processamento_w, dt_realizacao_w,
			ie_identificador_w, cd_motivo_encerramento_w, ie_origem_guia_w,
			nr_reembolso_w, ie_sexo_w, ie_tipo_atend_w,
			ie_tipo_atendimento_int_w, ie_tipo_faturamento_w, ie_tipo_guia_w,
			nm_usuario_p, nm_usuario_p, cd_cns_w,
			nr_reg_oper_inter_w, nr_seq_lote_w, pls_monit_tiss_confer_seq.nextval,
			vl_apresentado_w, vl_coparticipacao_w, vl_pag_fornec_w,
			vl_glosa_w, vl_liberado_w, vl_pag_tab_propria_w,
			cd_diag_princ_w, cd_diag_sec_w, cd_diag_terc_w,
			cd_diag_quarto_w);
			
	nr_seq_lote_p := nr_seq_lote_w;
	
end if;	

commit;

end pls_imp_monitor_conferencia;
/
