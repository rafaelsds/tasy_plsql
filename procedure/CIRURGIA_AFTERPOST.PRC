create or replace
procedure cirurgia_afterPost(	cd_estabelecimento_p number,
								nr_cirurgia_p	number,
								nr_seq_agenda_p number,								
								nm_usuario_p		varchar2 
							) is 

nr_prescricao_w		number(10,0);
GerarKit            varchar2(1);
ie_tipo_pessoa_w    varchar2(1);

begin

GerarKit 	:= obter_valor_param_usuario(281,1227,obter_perfil_ativo,nm_usuario_p,0);

if	( nr_cirurgia_p is not null ) and
	(nr_seq_agenda_p is not null) then 
	begin

	gerar_prescr_cirurgia(cd_estabelecimento_p,nr_cirurgia_p,nr_seq_agenda_p,nm_usuario_p, nr_prescricao_w);

	if	( nr_prescricao_w is not null)	then
		begin

		update	cirurgia
		set	nr_prescricao = nr_prescricao_w
		where	nr_cirurgia = nr_cirurgia_p;

		commit;
		end;
	end if;								
	end;
	
	if (GerarKit = 'S') then	
		select b.ie_tipo_pessoa
			into ie_tipo_pessoa_w
		from	usuario a,
			pessoa_fisica b
		where   a.cd_pessoa_fisica 	= 	b.cd_pessoa_fisica
		and	a.nm_usuario	=	nm_usuario_p;

		Gerar_prescricao_cirurgia(0,0,nr_cirurgia_p,0,ie_tipo_pessoa_w,nm_usuario_p,nr_prescricao_w,0,null,281,Obter_perfil_Ativo,null,null,null,null);
	end if;

end if;
end cirurgia_afterPost;
/
