create or replace
procedure san_enviar_dados_sht(ds_bifrost_event_p varchar2,
							   nr_seq_lote_p	number,
							   nm_usuario_p varchar2) is
						  
	ds_param_integ_res_w 	clob;
	json_dados_w			clob;
    data_w                  clob;
    data_auth_w             clob;	
    json_dados_envio_w      philips_json;
    json_body_w             philips_json;
	elapsed_time_w			number(10);
	
	
	cursor c_itens_lote is
		select nr_sequencia,
			cd_id_token, 
			dt_gravacao_token,
			qt_expirar_token,
			nm_pessoa_fisica, 
			nm_mae
		from san_lote_item
		where nr_seq_lote = nr_seq_lote_p
        and (ds_mensagem_retorno is null or nvl(ie_status_envio, 'N') = 'N');

begin	
    json_dados_envio_w  := philips_json();
	
	san_solicita_token_sht('integracao.sht.accesstoken', nr_seq_lote_p, nm_usuario_p);
    
    DBMS_LOCK.sleep(15);
	
	for c_item_lote_w in c_itens_lote loop
	begin
				
		select san_obter_dados_json_sht(nr_seq_lote_p, c_item_lote_w.nr_sequencia, nm_usuario_p)
		into json_dados_w
		from dual;
        
        json_body_w := philips_json(json_dados_w);        
        json_dados_envio_w.put('access_token', c_item_lote_w.cd_id_token);
        json_dados_envio_w.put('body', json_body_w);
		json_dados_envio_w.put('sequence', c_item_lote_w.nr_sequencia);
        
		dbms_lob.createtemporary(lob_loc => data_w, cache => true, dur => dbms_lob.call);
        json_dados_envio_w.to_clob(data_w);
		

		select (((sysdate) - (c_item_lote_w.dt_gravacao_token)) * 24 * 60 * 60) 
		into elapsed_time_w
		from dual;
        		
		begin
			if (elapsed_time_w <= c_item_lote_w.qt_expirar_token) then
				select bifrost.send_integration_content(ds_bifrost_event_p, data_w, nm_usuario_p)
                into ds_param_integ_res_w
                from dual;
			else
				san_solicita_token_sht('integracao.sht.accesstoken', nr_seq_lote_p, nm_usuario_p);    
				DBMS_LOCK.sleep(15);
				
				select bifrost.send_integration_content(ds_bifrost_event_p, data_w, nm_usuario_p)
                into ds_param_integ_res_w
                from dual;
            end if;			
		exception
			when others then
			rollback;
		end;
	end;
	end loop;
  	
end san_enviar_dados_sht;
/
