create or replace
procedure trans_finalizar_intervalo(
			nm_usuario_p		Varchar2) is 

cd_transportador_usuario_w	varchar2(10);
			
begin

begin
select	a.cd_pessoa_fisica
into	cd_transportador_usuario_w
from	usuario a
where	upper(a.nm_usuario_pesquisa) = upper(nm_usuario_p);
exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(197128);
		--Pessoa f�sica do usu�rio n�o encontrada. Atualize o cadastro do usu�rio
end;

update	transportador
set	IE_STATUS_RECURSO = 1,
	IE_TIPO_INTERVALO = null
where	cd_pessoa_fisica  = cd_transportador_usuario_w;


commit;

end trans_finalizar_intervalo;
/