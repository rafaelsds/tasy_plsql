create or replace
procedure pls_gerar_est_mat_uni_fed_sc(	nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is 


ds_primeiro_nivel_w		varchar2(255);
ds_segundo_nivel_w		varchar2(255);
ds_terceiro_nivel_w		varchar2(255);
cd_grupo_estoque_um_w		number(15);
cd_grupo_estoque_dois_w		number(15);
cd_grupo_estoque_tres_w		number(15);
nr_seq_nivel_um_w		number(15);
nr_seq_nivel_dois_w		number(15);
qt_registros_w			number(5);
cd_empresa_w			number(5);


Cursor C01 is
	select	ds_grupo_estoque,
		cd_grupo_estoque
	from	pls_grupo_est_fed_sc
	where	cd_pai	is null
	order by 1;

Cursor C02 is
	select	ds_grupo_estoque,
		cd_grupo_estoque
	from	pls_grupo_est_fed_sc
	where	cd_pai	is not null
	and	cd_pai	= cd_grupo_estoque_um_w
	order by 1;

Cursor C03 is
	select	ds_grupo_estoque,
		cd_grupo_estoque
	from	pls_grupo_est_fed_sc
	where	cd_pai	is not null
	and	cd_pai	= cd_grupo_estoque_dois_w
	order by 1;	
	
begin

select	substr(obter_empresa_estab(cd_estabelecimento_p),1,20)
into	cd_empresa_w
from	dual;

open C01;
loop
fetch C01 into	
	ds_primeiro_nivel_w,
	cd_grupo_estoque_um_w;
exit when C01%notfound;
	begin
	
	--Verifica se o item j� existe
	select	count(*)
	into	qt_registros_w
	from	pls_estrutura_material
	where	cd_externo	= cd_grupo_estoque_um_w;

	-- Se n�o existir, grava o item
	if	(qt_registros_w	= 0) then
	
	select	pls_estrutura_material_seq.nextval
	into	nr_seq_nivel_um_w
	from	dual;
	
	insert into	pls_estrutura_material
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_empresa,
		cd_estabelecimento,
		ds_estrutura,
		nr_seq_superior,
		cd_externo)
	values	(nr_seq_nivel_um_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_empresa_w,
		cd_estabelecimento_p,
		ds_primeiro_nivel_w,
		null,
		cd_grupo_estoque_um_w);
		
	--Se existir, pega a sequ�ncia do item para refer�ncia do segundo item (filho)
	else		
		select	nr_sequencia
		into	nr_seq_nivel_um_w
		from	pls_estrutura_material
		where	cd_externo	= cd_grupo_estoque_um_w;
		
	end if;
	
	
	open C02;
	loop
	fetch C02 into	
		ds_segundo_nivel_w,
		cd_grupo_estoque_dois_w;
	exit when C02%notfound;
		begin
		
		select	count(*)
		into	qt_registros_w
		from	pls_estrutura_material
		where	cd_externo	=  cd_grupo_estoque_dois_w;
		
		-- Se n�o existir, grava o item
		if	(qt_registros_w	= 0) then
		
			select	pls_estrutura_material_seq.nextval
			into	nr_seq_nivel_dois_w
			from	dual;
			
			insert into	pls_estrutura_material
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_empresa,
				cd_estabelecimento,
				ds_estrutura,
				nr_seq_superior,
				cd_externo)
			values	(nr_seq_nivel_dois_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_empresa_w,
				cd_estabelecimento_p,
				ds_segundo_nivel_w,
				nr_seq_nivel_um_w,
				cd_grupo_estoque_dois_w);
		
		--Se existir, pega a sequ�ncia do item para refer�ncia do segundo item (filho)
		else		
			select	nr_sequencia
			into	nr_seq_nivel_dois_w
			from	pls_estrutura_material
			where	cd_externo	= cd_grupo_estoque_dois_w;
		
		end if;
		
		open C03;
		loop
		fetch C03 into	
			ds_terceiro_nivel_w,
			cd_grupo_estoque_tres_w;
		exit when C03%notfound;
			begin
			
			select	count(*)
			into	qt_registros_w
			from	pls_estrutura_material
			where	cd_externo	=  cd_grupo_estoque_tres_w;
			
			-- Se n�o existir, grava o item
			if	(qt_registros_w	= 0) then
				
				insert into	pls_estrutura_material
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_empresa,
					cd_estabelecimento,
					ds_estrutura,
					nr_seq_superior,
					cd_externo)
				values	(pls_estrutura_material_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_empresa_w,
					cd_estabelecimento_p,
					ds_terceiro_nivel_w,
					nr_seq_nivel_dois_w,
					cd_grupo_estoque_tres_w);
			end if;
					
				
			end;
		end loop;
		close C03;	
		
		end;
	end loop;
	close C02;	
	end;
	
end loop;
close C01;


commit;

end pls_gerar_est_mat_uni_fed_sc;
/