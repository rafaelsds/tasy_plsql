create or replace
procedure Suspender_horarios_duplicados( nr_prescricao_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

ds_erro_w					varchar2(4000);
nr_atendimento_w			number(10);
cd_material_w				number(10);
nr_seq_material_w			number(6);
nr_seq_horario_w			number(10);
dt_horario_w				date;
ie_apresenta_rep_ant_w		varchar2(1);
nr_prescr_conflito_w		number(15);
ie_gravar_justif_w			varchar2(1);
ds_justificativa_w			varchar(255);
ie_agrupador_w				prescr_material.ie_agrupador%type;
ie_tipo_item_w				varchar2(1 char);

cursor c01 is
select	distinct 
	nr_seq_material,
	nr_seq_mat_hor,
	cd_material,
	dt_horario,
	nr_prescricao_conflito
from	prescr_mat_hor_conflito
where	nr_prescricao  = nr_prescricao_p
and	ie_tipo = 'D';

begin

Obter_Param_Usuario(924,890,obter_perfil_ativo,nm_usuario_p,0,ie_gravar_justif_w);

select	max(nr_atendimento)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

consiste_hor_prescr_mat_hor(nr_prescricao_p);

if	(nvl(ie_gravar_justif_w,'S') <> 'N') then
	ds_justificativa_w := obter_desc_expressao(727956);
else 
	ds_justificativa_w := null;
end if;

open C01;
loop
fetch C01 into	
	nr_seq_material_w,
	nr_seq_horario_w,
	cd_material_w,
	dt_horario_w,
	nr_prescr_conflito_w;
exit when C01%notfound;
	begin

	Consiste_susp_prescr_mat_hor(nr_prescr_conflito_w, nr_seq_horario_w, cd_estabelecimento_p, nm_usuario_p, ds_erro_w);
	if	(ds_erro_w	is not null) then
		ds_erro_w	:= '';
	else
	
		select 	nvl(max(ie_agrupador),99)
		into	ie_agrupador_w
		from 	prescr_material
		where	nr_prescricao = nr_prescr_conflito_w
		and 	nr_sequencia = nr_seq_material_w;
		
		if (ie_agrupador_w = 12) then
			ie_tipo_item_w := 'S';
		else
			ie_tipo_item_w := 'M';
		end if;

		Suspender_prescr_mat_hor(nr_seq_horario_w, nm_usuario_p, 0, null, 'N',null);
		Gerar_prescr_mat_hor_compl(nr_seq_horario_w, nm_usuario_p, 'S');
		Atualiza_ie_horario_susp(nr_prescr_conflito_w, nr_seq_material_w, 'PRESCR_MATERIAL');
		Gerar_Alter_Hor_Prescr_Adep(nr_atendimento_w, ie_tipo_item_w, cd_material_w, nr_prescr_conflito_w, nr_seq_material_w, nr_seq_horario_w, dt_horario_w, 12, ds_justificativa_w, null, null, nm_usuario_p);
	end if;	
	end;
end loop;
close C01;

commit;

end Suspender_horarios_duplicados;
/