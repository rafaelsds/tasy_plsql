create or replace
procedure	gerar_solic_compra_prescr(
			nr_prescricao_p			number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

qt_dias_obter_compra_w			number(10);
cd_perfil_w				number(5);

cd_material_w				number(6);
cd_unidade_medida_w			varchar2(30);
qt_material_w				number(15,4);
cd_setor_atendimento_w			number(5);
nr_solic_compra_w				number(10) := 0;
qt_dias_entrega_padrao_w			number(10) := 0;
cd_pessoa_fisica_w			varchar2(10);
cd_centro_custo_w			number(10);
nm_paciente_w				varchar2(100);
nr_atendimento_w				number(10);
dt_entrega_solic_w			date;
nr_item_w				number(5);
vl_ultima_compra_w			number(15,4);
nr_seq_nf_w				number(10);
qt_conv_estoque_consumo_w		number(13,4);
qt_conv_compra_estoque_w			number(13,4);
cd_unidade_medida_compra_w		varchar2(30);
cd_unidade_medida_consumo_w		varchar2(30);
qt_comprar_w				number(13,4) := 0;
qt_conversao_w				number(15,6);
qt_existe_w				number(10);
ie_tipo_solic_rep_w			varchar2(15);
ds_observacao_item_w			varchar2(255);

cursor c01 is			
select	a.cd_material,
	a.cd_unidade_medida,
	sum(a.qt_material) qt_material,
	obter_setor_prescricao(nr_prescricao_p, 'C') cd_setor_atendimento
from	prescr_material a
where 	a.nr_prescricao = nr_prescricao_p
and	a.nr_sequencia_solucao is null  
and	a.nr_sequencia_dieta is null  
and	a.nr_sequencia_diluicao is null  
and   	a.dt_suspensao is null  
and	nvl(a.ie_administrar,'S') = 'S' 
and	a.ie_agrupador = 2
--and	obter_tipo_material(a.cd_material, 'C') in (2,3)
and	a.ie_regra_disp = 'S'
and	a.qt_material > 0
and	obter_se_material_padronizado(cd_estabelecimento_p, a.cd_material) = 'N'
group by	a.cd_material,
	a.cd_unidade_medida
union
select	a.cd_material,
	a.cd_unidade_medida,
	sum(a.qt_solucao) qt_solucao,
	obter_setor_prescricao(nr_prescricao_p, 'C') cd_setor_atendimento
from 	prescr_material a
where 	nr_prescricao = nr_prescricao_p
and 	a.ie_agrupador = 4
and	a.ie_regra_disp = 'S'
and	a.qt_solucao > 0
and	obter_se_material_padronizado(cd_estabelecimento_p, a.cd_material) = 'N'
and 	a.nr_sequencia_solucao in ( 
 	select	x.nr_seq_solucao
	from    prescr_solucao x
	where   x.nr_prescricao = nr_prescricao_p)
group by	a.cd_material,
	a.cd_unidade_medida
union
select	a.cd_material,
	a.cd_unidade_medida,
	sum(a.qt_dose) qt_material,
	obter_setor_prescricao(nr_prescricao_p, 'C') cd_setor_atendimento
from	prescr_material a
where	a.nr_prescricao = nr_prescricao_p
and	a.ie_agrupador = 12		
and	a.ie_regra_disp = 'S'
and	a.qt_dose > 0
and	obter_se_material_padronizado(cd_estabelecimento_p, a.cd_material) = 'N'	
group by	a.cd_material,
	a.cd_unidade_medida
union
select	b.cd_material,
	b.cd_unidade_medida,
	sum(b.qt_dose),
	obter_setor_prescricao(nr_prescricao_p, 'C') cd_setor_atendimento
from	nut_pac_elem_mat b,
	nut_pac a
where	b.nr_seq_nut_pac  = a.nr_sequencia
and	a.nr_prescricao = nr_prescricao_p
and	obter_se_material_padronizado(cd_estabelecimento_p, b.cd_material) = 'N'
and	b.qt_dose > 0
and	'N' = 'S'
group by b.cd_material,
	b.cd_unidade_medida;

begin

Obter_Param_Usuario(913, 11,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, qt_dias_obter_compra_w);

select	obter_perfil_ativo
into	cd_perfil_w
from	dual;

open C01;
loop
fetch C01 into	
	cd_material_w,
	cd_unidade_medida_w,
	qt_material_w,
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin

	if	(nr_solic_compra_w = 0) then
		begin
	
		select	nr_atendimento,
			substr(obter_nome_pf_pj(cd_pessoa_fisica,null),1,100)
		into	nr_atendimento_w,
			nm_paciente_w
		from	prescr_medica
		where	nr_prescricao = nr_prescricao_p;
		
		select	nvl(qt_dias_entrega_padrao,2),
			nvl(ie_tipo_solic_rep,'ES')
		into	qt_dias_entrega_padrao_w,
			ie_tipo_solic_rep_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	usuario
		where	nm_usuario = nm_usuario_p;
		
		select	obter_centro_custo_setor(cd_setor_atendimento_w, 'C')
		into	cd_centro_custo_w
		from	dual;

		dt_entrega_solic_w  := ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate) + qt_dias_entrega_padrao_w;
		
		if	(cd_pessoa_fisica_w is null) then
			/*(-20011,'Erro ao gerar solicitacao de compras. O usuario ' || NM_USUARIO_P || ' nao possui nenhuma pessoa fisica vinculada.');*/
			wheb_mensagem_pck.exibir_mensagem_abort(201268,'NM_USUARIO_P='||NM_USUARIO_P);
		end if;
		
		select	solic_compra_seq.nextval
		into	nr_solic_compra_w
		from	dual;
		
		insert into solic_compra(
			nr_solic_compra,
			cd_estabelecimento,
			dt_solicitacao_compra,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			cd_pessoa_solicitante,
			cd_centro_custo,
			cd_setor_atendimento,
			ds_observacao,
			ie_aviso_chegada,
			ie_aviso_aprov_oc,
			ie_urgente,
			ie_tipo_solicitacao,
			ie_comodato,
			ie_semanal,
			nr_prescricao,
			ie_tipo_servico,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		values(	nr_solic_compra_w,
			cd_estabelecimento_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			'A',
			cd_pessoa_fisica_w,
			cd_centro_custo_w,
			cd_setor_atendimento_w,
			substr(WHEB_MENSAGEM_PCK.get_texto(302213,'NM_PACIENTE_W='|| NM_PACIENTE_W ||';NR_ATENDIMENTO_W='|| NR_ATENDIMENTO_W ||';NR_PRESCRICAO_P='|| NR_PRESCRICAO_P),1,2000), 	/*Paciente: #@NM_PACIENTE_W#@ - Atend: #@NR_ATENDIMENTO_W#@ - Prescr: #@NR_PRESCRICAO_P#@*/			
			'N',
			'N',
			'N',
			'7',
			'N',
			'N', 
			nr_prescricao_p,
			ie_tipo_solic_rep_w,
			nm_usuario_p,
			sysdate);

		ds_observacao_item_w		:= substr(WHEB_MENSAGEM_PCK.get_texto(302214,'NM_PACIENTE_W='|| NM_PACIENTE_W ||';NR_ATENDIMENTO_W='|| NR_ATENDIMENTO_W ||';NR_PRESCRICAO_P='|| NR_PRESCRICAO_P),1,255);
							/*Paciente: #@NM_PACIENTE_W#@ - Atend: #@NR_ATENDIMENTO_W#@ - Prescr: #@NR_PRESCRICAO_P#@*/
		
		end;
	end if;

	Obter_ultima_compra_material(
				cd_estabelecimento_p,
				cd_material_w,
				cd_unidade_medida_w,
				'C',
				sysdate - 365,
				nr_seq_nf_w,
				vl_ultima_compra_w);
				
				
	select	qt_conv_estoque_consumo,
		qt_conv_compra_estoque,
		substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,255) cd_unidade_medida_compra,
		substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,255) cd_unidade_medida_consumo
	into	qt_conv_estoque_consumo_w,
		qt_conv_compra_estoque_w,
		cd_unidade_medida_compra_w,
		cd_unidade_medida_consumo_w
	from	material
	where	cd_material = cd_material_w;
	
	qt_comprar_w := qt_material_w;
	
	if	(cd_unidade_medida_w <> cd_unidade_medida_consumo_w) then
		select	nvl(max(qt_conversao) ,1)
		into	qt_conversao_w
		from	material_conversao_unidade
		where	cd_material = cd_material_w
		and	cd_unidade_medida = cd_unidade_medida_w;
		qt_comprar_w := dividir(qt_comprar_w, qt_conversao_w);
	end if;
	
	qt_comprar_w := dividir(qt_comprar_w, qt_conv_estoque_consumo_w);	
	qt_comprar_w := dividir(qt_comprar_w, qt_conv_compra_estoque_w);
	
	if	((qt_comprar_w mod 1) > 0) then
		qt_comprar_w := round(qt_comprar_w) + 1;	
	end if;
	
	if	(qt_comprar_w = 0) then
		qt_comprar_w := 1;
	end if;	
	
	Gerar_Solic_Compra_Item(
				nr_solic_compra_w,
				cd_material_w,
				'C',
				qt_comprar_w,
				dt_entrega_solic_w,
				nm_usuario_p,
				'S',
				365,
				ds_observacao_item_w);


	end;
end loop;
close c01;

if	(nr_solic_compra_w <> 0) then
	gerar_aprov_solic_compra(nr_solic_compra_w,cd_perfil_w,nm_usuario_p,qt_dias_obter_compra_w,'N');
end if;

end gerar_solic_compra_prescr;
/
