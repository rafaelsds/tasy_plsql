create or replace
procedure alterar_qtde_item_nota(	nr_sequencia_p		number,
				nr_item_nf_p		number,
				qt_item_nf_p		number,
				nm_usuario_p		varchar2) is

cd_material_w			nota_fiscal_item.cd_material%type; 
ds_material_w			varchar2(255);
qt_item_nf_w			number(13,4);
historico_w			varchar2(2000);
VL_UNITARIO_ITEM_NF_w           nota_fiscal_item.VL_UNITARIO_ITEM_NF%type; 
VL_DESCONTO_w                   nota_fiscal_item.VL_DESCONTO%type;
QT_ITEM_ESTOQUE_w               nota_fiscal_item.QT_ITEM_ESTOQUE%type;
vl_liquido_w                    nota_fiscal_item.vl_liquido%type;
qt_conversao_w                  number(6);
NR_SEQ_UNIDADE_ADIC_W           nota_fiscal_item.NR_SEQ_UNIDADE_ADIC%type;
CD_UNIDADE_MEDIDA_COMPRA_w      nota_fiscal_item.CD_UNIDADE_MEDIDA_COMPRA%type;
NR_SEQ_MARCA_W                  nota_fiscal_item.NR_SEQ_MARCA%type;
cd_cgc_w                        nota_fiscal.cd_cgc%type;               
cd_estabelecimento_w            nota_fiscal.cd_estabelecimento%type; 
VL_TOTAL_ITEM_NF_w		nota_fiscal_item.VL_TOTAL_ITEM_NF%type; 
pr_desconto_w                   nota_fiscal_item.pr_desconto%type; 			
begin

select cd_cgc,
       cd_estabelecimento 
into   cd_cgc_w,      
       cd_estabelecimento_w 
from nota_fiscal 
where nr_sequencia =  nr_sequencia_p;      

select	substr(obter_desc_material(cd_material),1,255) ds_material,
	qt_item_nf,
	VL_UNITARIO_ITEM_NF,
	VL_DESCONTO,
	QT_ITEM_ESTOQUE,
	cd_material,
	NR_SEQ_UNIDADE_ADIC,
        CD_UNIDADE_MEDIDA_COMPRA,
	NR_SEQ_MARCA,
	pr_desconto
	
into	ds_material_w,
	qt_item_nf_w,
	VL_UNITARIO_ITEM_NF_w,
	VL_DESCONTO_w,
        QT_ITEM_ESTOQUE_w,
	cd_material_w,
        NR_SEQ_UNIDADE_ADIC_w,
        CD_UNIDADE_MEDIDA_COMPRA_W,
	NR_SEQ_MARCA_W,
	pr_desconto_w
from	nota_fiscal_item
where 	nr_sequencia 	= nr_sequencia_p
and	nr_item_nf	= nr_item_nf_p;

VL_TOTAL_ITEM_NF_w := qt_item_nf_p * VL_UNITARIO_ITEM_NF_W; 

if (NR_SEQ_UNIDADE_ADIC_w is not null) then
   begin
     select nvl(max(qt_conversao),1)
     into   qt_conversao_w
     from   unidade_medida_adic_compra
     where nr_sequencia  = NR_SEQ_UNIDADE_ADIC_w;
     
     QT_ITEM_ESTOQUE_w := qt_item_nf_p * qt_conversao_w; 
   end; 
else
   begin   
     QT_ITEM_ESTOQUE_w := obter_qt_convertida_compra_est(cd_material_w, qt_item_nf_p, CD_UNIDADE_MEDIDA_COMPRA_w,NR_SEQ_MARCA_W, cd_cgc_w, cd_estabelecimento_w);     
   end;
end if;

    vl_desconto_w   := (pr_desconto_w * vl_total_item_nf_w) / 100;     
    pr_desconto_w   := (vl_desconto_w * 100) / vl_total_item_nf_w;     
       
    vl_liquido_w := qt_item_nf_p * VL_UNITARIO_ITEM_NF_W - VL_DESCONTO_w;      


begin
  update 	nota_fiscal_item
  set   	qt_item_nf 	 = qt_item_nf_p,
                vl_liquido       = vl_liquido_w,
	        QT_ITEM_ESTOQUE  = QT_ITEM_ESTOQUE_w,
	        VL_TOTAL_ITEM_NF = VL_TOTAL_ITEM_NF_w,
	        vl_desconto      = vl_desconto_w,
	        pr_desconto      = pr_desconto_w
  where 	nr_sequencia 	 = nr_sequencia_p
  and	nr_item_nf	 = nr_item_nf_p;
exception
 when others then		
     historico_w :=  wheb_mensagem_pck.get_texto(347587);					
end;  

	
historico_w := historico_w ||  Wheb_mensagem_pck.get_Texto(794197,
'NR_ITEM_W='||nr_item_nf_p||
';CD_MATERIAL_W='||cd_material_w||
';DS_MATERIAL_W='||ds_material_w||
';QT_MATERIAL_W='||campo_mascara(qt_item_nf_w,4)||
';QT_MAT_W='||campo_mascara(qt_item_nf_p,4));
			

gerar_historico_nota_fiscal(
	nr_sequencia_p,
	nm_usuario_p,
	35,
	historico_w);

commit;
end alterar_qtde_item_nota;
/