create or replace
procedure pls_baixar_glosas_conta_pag(	nr_seq_conta_p			number,
					nr_seq_acao_interc_p		number,
					nr_seq_fatura_p			number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		varchar2) is

vl_glosa_proc_w		number(15,2) :=	0;
vl_glosa_mat_w		number(15,2) :=	0;
vl_total_glosa_w	number(15,2) :=	0;
vl_saldo_titulo_w	number(15,2) := 0;
nr_titulo_w		number(10);
nr_seq_baixa_w		number(5);
dt_baixa_w		date;
cd_tipo_baixa_w		number(10);
nr_seq_trans_financ_w	number(10);	
cd_moeda_padrao_w	number(5);
qt_baixas_conta_w	number(5);
ie_status_fatura_w	varchar2(4);
ds_observacao_w		varchar2(255);	
vl_glosa_w		number(15,2);
ie_ato_cooperado_w	varchar2(1);			

Cursor C01 is
	select	sum(nvl(a.vl_glosa,0)),
		a.ie_ato_cooperado
	from	pls_conta_proc	a
	where	a.nr_seq_conta = nr_seq_conta_p
	group by a.ie_ato_cooperado
	union all
	select	sum(nvl(a.vl_glosa,0)),
		a.ie_ato_cooperado
	from	pls_conta_mat	a
	where	a.nr_seq_conta = nr_seq_conta_p
	group by a.ie_ato_cooperado;

begin
/* Somar todo o valor glosado (PROCEDIMENTOS) */
select	nvl(sum(p.vl_glosa),0)
into	vl_glosa_proc_w
from	pls_conta_proc	p
where	p.nr_seq_conta = nr_seq_conta_p;

/* Somar todo o valor glosado (MATERIAIS / MEDICAMENTOS) */
select	nvl(sum(m.vl_glosa),0)
into	vl_glosa_mat_w
from	pls_conta_mat	m
where	m.nr_seq_conta = nr_seq_conta_p;

vl_total_glosa_w := vl_glosa_proc_w + vl_glosa_mat_w;

/* Verifica se h� valor de glosa */
if	(vl_total_glosa_w > 0) and
	(nr_seq_acao_interc_p is not null) then
	select	max(a.nr_titulo),
		max(a.ie_status)
	into	nr_titulo_w,
		ie_status_fatura_w
	from	ptu_fatura a
	where	a.nr_sequencia	= nr_seq_fatura_p;
	
	if	(nr_titulo_w is not null) and
		(ie_status_fatura_w <> 'CA') then
		select	a.vl_saldo_titulo
		into	vl_saldo_titulo_w
		from	titulo_pagar a
		where	a.nr_titulo = nr_titulo_w;
		
		if	(vl_saldo_titulo_w > 0) and
			(vl_total_glosa_w <= vl_saldo_titulo_w) then
			/* Gerar baixa de glosa */
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_seq_baixa_w
			from	titulo_pagar_baixa a
			where	a.nr_titulo	= nr_titulo_w;
			
			dt_baixa_w	:= trunc(sysdate,'dd');
			
			select	max(cd_tipo_baixa_cpa),
				max(nr_seq_trans_fin_baixa)
			into	cd_tipo_baixa_w,
				nr_seq_trans_financ_w
			from	pls_processo_interc_acao
			where	nr_sequencia = nr_seq_acao_interc_p;
			
			select	max(cd_moeda_padrao)
			into	cd_moeda_padrao_w
			from	parametros_contas_pagar
			where	cd_estabelecimento = cd_estabelecimento_p;			
			
			if	(nr_seq_acao_interc_p = 3) then
				ds_observacao_w	:= wheb_mensagem_pck.get_texto(302201,'NR_SEQ_CONTA_P=' || nr_seq_conta_p);
			elsif	(nr_seq_acao_interc_p = 4) then
				ds_observacao_w	:= wheb_mensagem_pck.get_texto(302208,'NR_SEQ_CONTA_P=' || nr_seq_conta_p);
			end if;
		
			update	titulo_pagar 
			set	ie_status	= 'D',
				dt_liberacao	= sysdate
			where	nr_titulo	= nr_titulo_w;
			
			insert	into titulo_pagar_baixa
				(nr_titulo,
				nr_sequencia,
				dt_baixa,
				cd_moeda,
				dt_atualizacao,
				nm_usuario,
				cd_tipo_baixa,
				ie_acao,
				vl_baixa,
				vl_glosa,
				vl_descontos,
				vl_outras_deducoes,
				vl_juros,
				vl_multa,
				vl_outros_acrescimos,
				vl_pago,
				cd_banco,
				nr_lote_contabil,
				vl_devolucao,
				nr_seq_devolucao,
				nr_bordero,
				nr_seq_trans_fin,
				vl_ir,
				nr_seq_conta_banco,
				nr_seq_escrit,
				cd_centro_custo,
				cd_conta_contabil,
				vl_cotacao_moeda,
				ds_observacao,
				nr_seq_pls_conta,
				vl_glosa_ato_coop_princ,
				vl_glosa_ato_coop_aux,
				vl_glosa_ato_nao_coop)
			values	(nr_titulo_w,
				nr_seq_baixa_w,
				dt_baixa_w,
				cd_moeda_padrao_w,
				sysdate,
				nm_usuario_p,
				cd_tipo_baixa_w,
				'I',
				vl_total_glosa_w,
				vl_total_glosa_w,
				0,
				0, 
				0, 
				0, 
				0, 
				0,
				null,
				0,
				0,
				null, 
				null, 
				nr_seq_trans_financ_w,
				null, 
				null,
				null,
				null,
				null,
				null,
				ds_observacao_w,
				nr_seq_conta_p,
				0,
				0,
				0);
				
			open C01;
			loop
			fetch C01 into	
				vl_glosa_w,
				ie_ato_cooperado_w;
			exit when C01%notfound;
				begin
				if	(ie_ato_cooperado_w = '1') then
					update	titulo_pagar_baixa
					set	vl_glosa_ato_coop_princ	= nvl(vl_glosa_ato_coop_princ,0) + vl_glosa_w
					where	nr_titulo 		= nr_titulo_w
					and	nr_sequencia 		= nr_seq_baixa_w;
				elsif	(ie_ato_cooperado_w = '2') then
					update	titulo_pagar_baixa
					set	vl_glosa_ato_coop_aux	= nvl(vl_glosa_ato_coop_aux,0) + vl_glosa_w
					where	nr_titulo 		= nr_titulo_w
					and	nr_sequencia 		= nr_seq_baixa_w;
				else
					update	titulo_pagar_baixa
					set	vl_glosa_ato_nao_coop	= nvl(vl_glosa_ato_nao_coop,0) + vl_glosa_w
					where	nr_titulo 		= nr_titulo_w
					and	nr_sequencia 		= nr_seq_baixa_w;	
				end if;
				end;
			end loop;
			close C01;			
				
			atualizar_saldo_tit_pagar(nr_titulo_w,nm_usuario_p);
			Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
		end if;
	end if;
end if;

--commit

end pls_baixar_glosas_conta_pag;
/