create or replace
procedure lab_insere_impressao_mapa(	nr_prescricao_p 	Number,
					nr_seq_prescr_p		Number,
					nr_seq_exame_p		Number,
					nm_usuario_p 		Varchar2,
					cd_setor_atendimento_p varchar2) is 
					
nr_seq_grup_imp_w		number(10);
nr_sequencia_w			number(10);
cd_estabelecimento_w 	lab_impressao_mapa.cd_estabelecimento%type;
begin

select nvl(max(cd_estabelecimento), obter_estabelecimento_ativo) 
into cd_estabelecimento_w
from setor_atendimento 
where 	cd_setor_atendimento = cd_setor_atendimento_p;

select	max(nr_seq_grupo_imp)
into	nr_seq_grup_imp_w
from	exame_laboratorio
where	nr_seq_exame = nr_seq_exame_p;

if (nr_seq_grup_imp_w > 0) then

	select	lab_impressao_mapa_seq.nextval 
	into	nr_sequencia_w
	from 	dual;

	insert into lab_impressao_mapa (
		nr_sequencia,
		nr_prescricao,
		nr_seq_prescr,
		nr_seq_grup_imp,
		cd_mapa,
		ie_status,
		dt_atualizacao,
		nm_usuario,
		cd_estabelecimento
	) values (
		nr_sequencia_w,
		nr_prescricao_p,
		nr_seq_prescr_p,
		nr_seq_grup_imp_w,
		null,
		10,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_w
	);

	lab_insere_impressao_mapa_log(nr_prescricao_p, nr_seq_prescr_p, 10, null, nm_usuario_p);
end if;

end lab_insere_impressao_mapa;
/
