create or replace
procedure atualiza_estab_financeiro_tit(
		nr_titulo_p		number,
		cd_estab_financeiro_p 	number,
		dt_contabil_p		date,
		ds_observacao_titulo_p	varchar2) is 
begin
if	(nr_titulo_p is not null) then
	begin
	update 	titulo_pagar
	set 	cd_estab_financeiro = cd_estab_financeiro_p,
		dt_contabil = dt_contabil_p,
		ds_observacao_titulo = WHEB_MENSAGEM_PCK.get_texto(212522, 'DS_LISTA_TITULOS=' || ds_observacao_titulo_p)
	where 	nr_titulo = nr_titulo_p;
	commit;
	end;
end if;
end atualiza_estab_financeiro_tit;
/