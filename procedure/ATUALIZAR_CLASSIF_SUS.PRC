CREATE OR REPLACE
PROCEDURE Atualizar_classif_sus
                         	(nr_interno_conta_p   		Number) IS

ie_erro_w		varchar2(1)		:= 'N';

BEGIN

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus 		= 4
where 	a.nr_interno_conta 	= nr_interno_conta_p
and		a.ie_classif_sus is null
and 		a.ie_origem_proced 	= 2
and		(substr(a.cd_procedimento,1,2) in ('92','97','98') 		or
		 substr(a.cd_procedimento,1,4) in ('9906','9907','9904')  	or
		 substr(a.cd_procedimento,1,5) in ('99300','99500','99800') or
		 a.cd_procedimento = 99020017);
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus 		= 9
where 	a.nr_interno_conta 	= nr_interno_conta_p
and 		a.ie_origem_proced 	= 2
and 		a.ie_tipo_servico_sus 	= 4
and 		a.ie_classif_sus 		is null;
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus 		= 5
where 	a.nr_interno_conta 	= nr_interno_conta_p
and 		a.ie_origem_proced 	= 2
and 		a.ie_tipo_servico_sus in (3,2,1)
and 		a.ie_classif_sus 		is null;
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus 		= 7
where 	a.nr_interno_conta 	= nr_interno_conta_p
and 		a.ie_origem_proced 	= 2
and 		a.ie_tipo_servico_sus 	= 7
and 		a.ie_classif_sus 		is null;
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus 		= 8
where 	a.nr_interno_conta 	= nr_interno_conta_p
and 		a.ie_origem_proced 	= 2
and 		a.ie_tipo_servico_sus 	= 8
and 		a.ie_classif_sus 		is null;
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus 		= 3
where 	a.nr_interno_conta 	= nr_interno_conta_p
and 		a.ie_origem_proced 	= 2
and 		a.ie_classif_sus is null;
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

begin
update 	procedimento_paciente a
set 		a.ie_classif_sus = 6
where 	a.nr_interno_conta = nr_interno_conta_p
and		a.ie_classif_sus is null
and 		a.ie_origem_proced = 3;
exception
		when others then
      			ie_erro_w := 'S';
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end;

END Atualizar_classif_sus;
/
