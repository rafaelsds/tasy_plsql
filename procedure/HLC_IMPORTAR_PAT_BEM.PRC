create or replace
procedure hlc_importar_pat_bem(	nm_usuario_p		varchar2)is

nr_sequencia_w			number(10);
nr_seq_bem_principal_w		number(10);
ie_tipo_valor_w			varchar2(1);
vl_residual_w			number(15,2);
cursor c01 is
select	*
from	hlc_pat_bem;	

vet01	c01%rowtype;

begin

open c01;
loop
fetch c01 into
	vet01;
exit when c01%notfound;
	begin

	select	pat_bem_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	vl_residual_w	:= vet01.vl_residual;
	if	(vet01.vl_residual >= vet01.vl_origem) then
		vl_residual_w	:= 0;
	end if;
	
	insert into pat_bem(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		cd_bem,
		ds_bem,
		dt_aquisicao,
		nr_seq_tipo,
		nr_seq_local,
		ie_imobilizado,
		ie_situacao,
		cd_conta_contabil,
		cd_centro_custo,
		ie_tipo_valor,
		tx_deprec,
		nr_seq_princ,
		vl_original,
		cd_moeda,
		dt_inicio_uso,
		nr_seq_adicao,
		ds_observacao,
		nr_seq_marca,
		ds_serie,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_bem_externo,
		ie_propriedade,
		ie_subvencao,
		tx_fiscal,
		vl_residual)
	values(	nr_sequencia_w,
		vet01.cd_estabelecimento,
		sysdate,
		'OS336271',
		vet01.cd_bem,
		substr(vet01.ds_bem,1,255),
		nvl(vet01.dt_aquisicao,vet01.dt_inicio_uso),
		vet01.nr_seq_tip,
		vet01.nr_seq_loc,
		'S',
		'A',
		vet01.cd_conta_contabil,
		vet01.cd_centro_,
		'O', /*Bem original - Depreciação zero - somente deprec fiscal*/
		0, /* Tx_deprec */
		null,
		vet01.vl_origem,
		1,
		vet01.dt_inicio_uso,
		null,
		vet01.ds_observa,
		vet01.nr_seq_mar,
		vet01.ds_serie,
		sysdate,
		'OS336271',
		vet01.CD_PLACA_A,
		'P',
		'N',
		vet01.tx_deprec_fiscal,
		vet01.vl_residual);
		
	nr_seq_bem_principal_w	:=nr_sequencia_w;
	
	vl_residual_w	:= vet01.vl_residual;
	if	(vet01.vl_residual >= vet01.vl_reavali) then
		vl_residual_w	:= 0;
	end if;
	
	select	pat_bem_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into pat_bem(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		cd_bem,
		ds_bem,
		dt_aquisicao,
		nr_seq_tipo,
		nr_seq_local,
		ie_imobilizado,
		ie_situacao,
		cd_conta_contabil,
		cd_centro_custo,
		ie_tipo_valor,
		tx_deprec,
		nr_seq_princ,
		vl_original,
		cd_moeda,
		dt_inicio_uso,
		nr_seq_adicao,
		ds_observacao,
		nr_seq_marca,
		ds_serie,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_bem_externo,
		ie_propriedade,
		ie_subvencao,
		tx_fiscal,
		vl_residual)
	values(	nr_sequencia_w,
		vet01.cd_estabelecimento,
		sysdate,
		'OS336271',
		vet01.cd_bem,
		substr(vet01.ds_bem,1,255),
		nvl(vet01.dt_aquisicao,vet01.dt_inicio_uso),
		vet01.nr_seq_tip,
		vet01.nr_seq_loc,
		'S',
		'A',
		vet01.cd_conta_contabil,
		vet01.cd_centro_,
		'C', /*Bem Reavaliado - Depreciação zero - somente deprec fiscal*/
		vet01.tx_deprec, /* Tx_deprec */
		nr_seq_bem_principal_w,
		vet01.vl_reavali,
		1,
		vet01.dt_inicio_uso,
		null,
		vet01.ds_observa,
		vet01.nr_seq_mar,
		vet01.ds_serie,
		sysdate,
		'OS336271',
		vet01.CD_PLACA_A,
		'P',
		'N',
		0,
		vet01.vl_residual);
	
	end;
end loop;
close c01;



commit;

end hlc_importar_pat_bem;
/