create or replace procedure pls_inserir_w_glosa_ocor(nr_seq_analise_p	number,
					nr_seq_ocorrencia_p	number,
					nr_seq_motivo_glosa_p	number,
					nr_seq_conta_pos_p	number,
					nm_usuario_p		varchar2,
					ie_regra_agrup_p	varchar2 default null,
					ie_tipo_item_p		varchar2 default null,
					ie_mult_itens_p		varchar2 default null) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir nova glosa/ocorr�ncia na tela de glosa do item (An�lise Nova)
Gera apenas na tabela tempor�ria, somente depois que clicar em Ok que ir� para as tabelas quentes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_ocorrencia_w			varchar2(30);
ds_tipo_w			varchar2(255);
ds_glosa_ocorrencia_w		varchar2(255);
ds_glosa_vinc_w			varchar2(255);
nr_seq_motivo_vinc_w		number(10);
nr_seq_glosa_w			number(10);
nr_sequencia_w			number(10);
qt_glo_ocor_w			number(10) := 0;
cd_mensagem_w			varchar2(30);
ie_novo_pos_estab_w		pls_visible_false.ie_novo_pos_estab%type;

begin

select 	nvl(max(ie_novo_pos_estab),'N')
into	ie_novo_pos_estab_w
from	pls_visible_false;

if	(nr_seq_analise_p is not null) and
	((nr_seq_ocorrencia_p is not null) or (nr_seq_motivo_glosa_p is not null)) then
	if	(nr_seq_ocorrencia_p is not null) then
		select	a.cd_ocorrencia,
			'Ocorr�ncia',
			a.ds_ocorrencia,
			a.nr_seq_motivo_glosa
		into	cd_ocorrencia_w,
			ds_tipo_w,
			ds_glosa_ocorrencia_w,
			nr_seq_glosa_w
		from	pls_ocorrencia a
		where	a.nr_sequencia	= nr_seq_ocorrencia_p;

		if	(nr_seq_glosa_w is not null) then
			select	b.ds_motivo_tiss
			into	ds_glosa_vinc_w
			from	tiss_motivo_glosa b
			where	b.nr_sequencia	= nr_seq_glosa_w;
		end if;
		/*retirado tratamento para que seja poss�vel inserir a mesma ocorr�ncia na an�lise OS 538829 Diogo*/
		/* Francisco - 07/02/2013 - Inclu� denovo, usu�rio ter� que reativar as ocorr�ncias/glosas */
		select	count(1)
		into	qt_glo_ocor_w
		from	w_pls_analise_glosa_ocor	a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_ocorrencia	= nr_seq_ocorrencia_p
		and	rownum			= 1;
	elsif	(nr_seq_motivo_glosa_p is not null) then
		select	a.cd_motivo_tiss,
			'Glosa',
			a.ds_motivo_tiss
		into	cd_ocorrencia_w,
			ds_tipo_w,
			ds_glosa_ocorrencia_w
		from	tiss_motivo_glosa a
		where	a.nr_sequencia	= nr_seq_motivo_glosa_p;
		/*retirado tratamento para que seja poss�vel inserir a mesma glosa na an�lise OS 538829 Diogo*/
		/* Francisco - 07/02/2013 - Inclu� denovo, usu�rio ter� que reativar as ocorr�ncias/glosas */
		if ((ie_mult_itens_p is null) or (ie_mult_itens_p = 'N')) then
			select	count(1)
			into	qt_glo_ocor_w
			from	w_pls_analise_glosa_ocor	a
			where	a.nr_seq_analise	= nr_seq_analise_p
			and	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_p
			and	rownum			= 1;		
		end if;
	end if;

	if	(qt_glo_ocor_w = 0) then
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	w_pls_analise_glosa_ocor a
		where	a.nm_usuario	= nm_usuario_p;

		insert into w_pls_analise_glosa_ocor
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_analise,
			nr_seq_ocorrencia,
			nr_seq_motivo_glosa,
			ie_situacao,
			cd_ocorrencia,
			ds_tipo,
			ds_glosa_ocorrencia,
			ds_glosa_vinc,
			ie_selecionada,
			ie_inserido_auditor,
			nr_seq_conta_pos_estab)
		values	(nr_sequencia_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_analise_p,
			nr_seq_ocorrencia_p,
			nvl(nr_seq_motivo_glosa_p,nr_seq_glosa_w),
			'A',
			cd_ocorrencia_w,
			ds_tipo_w,
			ds_glosa_ocorrencia_w,
			ds_glosa_vinc_w,
			'S',
			'S',
			nr_seq_conta_pos_p);
			
		if	(ie_novo_pos_estab_w = 'S') then
		
			if	(ie_tipo_item_p = 'P') then
				update	w_pls_analise_glosa_ocor
				set	nr_seq_conta_pos_estab = null,
					nr_seq_conta_pos_proc = nr_seq_conta_pos_p
				where	nr_sequencia = nr_sequencia_w;
			elsif	(ie_tipo_item_p = 'M') then
				update	w_pls_analise_glosa_ocor
				set	nr_seq_conta_pos_estab = null,
					nr_seq_conta_pos_mat = nr_seq_conta_pos_p
				where	nr_sequencia = nr_sequencia_w;
			end if;
		
		end if;
			
	elsif	(nvl(ie_regra_agrup_p,'N') = 'N') then
		if	(nr_seq_ocorrencia_p is not null) then
			select	a.cd_ocorrencia
			into	cd_mensagem_w
			from	pls_ocorrencia a
			where	a.nr_sequencia = nr_seq_ocorrencia_p;
			
		elsif	(nr_seq_motivo_glosa_p is not null) then
			select	a.cd_motivo_tiss
			into	cd_mensagem_w
			from	tiss_motivo_glosa a
			where	a.nr_sequencia = nr_seq_motivo_glosa_p;
		end if;

		wheb_mensagem_pck.exibir_mensagem_abort(227160,'CODIGO=' || cd_mensagem_w);
	end if;
end if;

commit;

end pls_inserir_w_glosa_ocor;
/
