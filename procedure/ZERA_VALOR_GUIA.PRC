create or replace
procedure zera_valor_guia(nr_Seq_item_p int) is

begin  
update   convenio_retorno_item 
set      vl_amenor = (vl_amenor + vl_pago + vl_glosado), vl_pago = 0, vl_adicional = 0, vl_glosado = 0
where    nr_sequencia = nr_Seq_item_p;
commit;

end	zera_valor_guia;
/