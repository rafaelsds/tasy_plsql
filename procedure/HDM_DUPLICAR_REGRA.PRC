create or replace
procedure hdm_duplicar_regra (nr_sequencia_p	varchar2) is

nr_seq_regra_cubo_w 		mprev_regra_cubo.nr_sequencia%type;	
nr_seq_regra_cubo_pessoa_w	mprev_regra_cubo_pessoa.nr_sequencia%type;
nr_seq_regra_cubo_benef_w	mprev_regra_cubo_benef.nr_sequencia%type;
nr_seq_regra_cubo_custo_w	mprev_regra_cubo_custo.nr_sequencia%type;
nr_seq_regra_cubo_atend_w	mprev_regra_cubo_atend.nr_sequencia%type;
nr_seq_regra_cubo_diag_w	mprev_regra_cubo_diag.nr_sequencia%type;
nr_seq_regra_cubo_proc_w	mprev_regra_cubo_proc.nr_sequencia%type;
nr_seq_hdm_regra_cubo_compl_w 	hdm_regra_cubo_compl.nr_sequencia%type;
hdm_regra_cubo_atend_eup_w      hdm_regra_cubo_atend_eup.nr_sequencia%type; 
hdm_regra_cubo_lista_prob_w     hdm_regra_cubo_lista_prob.nr_sequencia%type;
nr_seq_hdm_regra_cubo_trat_w    hdm_regra_cubo_trat.nr_sequencia%type;
nr_seq_hdm_regra_cubo_medic_w   hdm_regra_cubo_medic.nr_sequencia%type;



nr_sequencia_w			number(10);
		
cursor	c01 is
	select  nr_sequencia
	from 	mprev_regra_cubo_pessoa 
	where 	nr_seq_regra_cubo = nr_sequencia_p;
		
cursor  c02 is	
	select	nr_sequencia
	from 	mprev_regra_cubo_benef 
	where  	nr_seq_regra_cubo = nr_sequencia_p;
	
cursor  c03 is
	select 	nr_sequencia
	from	mprev_regra_cubo_custo a
	where	nr_seq_regra_cubo = nr_sequencia_p;
	
cursor  c04 is
	select  nr_sequencia
	from	mprev_regra_cubo_atend 
	where	nr_seq_regra_cubo = nr_sequencia_p;
	
cursor  c05 is
        select	nr_sequencia
	from 	mprev_regra_cubo_diag 
	where	nr_seq_regra_cubo = nr_sequencia_p;
	
cursor 	c06 is
	select	nr_sequencia
	from 	mprev_regra_cubo_proc 
	where	nr_seq_regra_cubo = nr_sequencia_p;
  
cursor 	c07 is
	select	nr_sequencia
	from 	hdm_regra_cubo_compl 
	where	nr_seq_regra_cubo = nr_sequencia_p;
  

cursor 	c08 is
	select	nr_sequencia
	from 	hdm_regra_cubo_atend_eup 
	where	nr_seq_regra_cubo = nr_sequencia_p;
  
cursor 	c09 is
	select	nr_sequencia
	from 	hdm_regra_cubo_lista_prob
	where	nr_seq_regra_cubo = nr_sequencia_p;
  
cursor 	c10 is
	select	nr_sequencia
	from 	hdm_regra_cubo_trat
	where	nr_seq_regra_cubo = nr_sequencia_p;
  
cursor 	c11 is
	select	nr_sequencia
	from 	hdm_regra_cubo_medic
	where	nr_seq_regra_cubo = nr_sequencia_p;
  
begin

    if	(nr_sequencia_p is not null) then
	
		select  mprev_regra_cubo_seq.nextval
		into 	nr_seq_regra_cubo_w
		from 	dual;		
		
		if	(nvl(nr_seq_regra_cubo_w,0) <> 0) then
		
			insert
			into 	mprev_regra_cubo(
				nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_regra,
				nr_seq_programa,
				nr_seq_campanha,
				dt_inicio_vigencia,
				dt_fim_vigencia,				
				qt_meses_atras,
        nr_seq_envio_mens)(
			select  nr_seq_regra_cubo_w,
				a.cd_estabelecimento,
				a.dt_atualizacao,
				a.nm_usuario,
				a.dt_atualizacao_nrec,
				a.nm_usuario_nrec,
				obter_desc_expressao(342086)||' '||a.nm_regra,
				a.nr_seq_programa,
				a.nr_seq_campanha,
				a.dt_inicio_vigencia,
				a.dt_fim_vigencia,				
				a.qt_meses_atras,
        a.nr_seq_envio_mens		   
			from	mprev_regra_cubo a
			where 	a.nr_sequencia = nr_sequencia_p);	
			
			open c01;
			loop
			fetch c01 into
				nr_sequencia_w;
			exit 	when c01%notfound;
				begin				
					select  mprev_regra_cubo_pessoa_seq.nextval
					into 	nr_seq_regra_cubo_pessoa_w
					from 	dual;
					
					if	(nvl(nr_seq_regra_cubo_pessoa_w,0)<>0) then
			
						insert
						into 	mprev_regra_cubo_pessoa(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							ie_sexo,
							qt_idade_minima,
							qt_idade_maxima,
							ie_participante,
							ie_grupo_controle,
							ie_incluir)(
						select  nr_seq_regra_cubo_pessoa_w,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							ie_sexo,
							qt_idade_minima,
							qt_idade_maxima,
							ie_participante,
							ie_grupo_controle,
							ie_incluir
						from	mprev_regra_cubo_pessoa
						where 	nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c01;
			
			open c02;
			loop
			fetch c02 into
				nr_sequencia_w;
			exit 	when c02%notfound;
				begin
			
					select  mprev_regra_cubo_benef_seq.nextval
					into 	nr_seq_regra_cubo_benef_w
					from 	dual;
					
					if	(nvl(nr_seq_regra_cubo_benef_w,0)<>0) then
			
						insert
						into 	mprev_regra_cubo_benef(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							ie_preco,
							qt_minima_dias_plano,
							ie_situacao_contrato,
							ie_atendimento_suspenso,
							ie_incluir)(
						select 	nr_seq_regra_cubo_benef_w,
							a.dt_atualizacao,
							a.nm_usuario,
							a.dt_atualizacao_nrec,
							a.nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							a.ie_preco,
							a.qt_minima_dias_plano,
							a.ie_situacao_contrato,
							a.ie_atendimento_suspenso,
							a.ie_incluir
						from	mprev_regra_cubo_benef a
						where   a.nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c02;
			
			open c03;
			loop
			fetch c03 into
				nr_sequencia_w;
			exit 	when c03%notfound;
				begin
				
					select  mprev_regra_cubo_custo_seq.nextval
					into 	nr_seq_regra_cubo_custo_w
					from 	dual;
					
					if	(nvl(nr_seq_regra_cubo_custo_w,0)<>0) then
			
						insert
						into 	mprev_regra_cubo_custo(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							vl_custo_medio_minimo,
							vl_custo_medio_maximo,
							vl_sinistralidade_minima,
							vl_sinistralidade_maxima,
							ie_incluir)(
						select	nr_seq_regra_cubo_custo_w,
							a.dt_atualizacao,
							a.nm_usuario,
							a.dt_atualizacao_nrec,
							a.nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							a.vl_custo_medio_minimo,
							a.vl_custo_medio_maximo,
							a.vl_sinistralidade_minima,
							a.vl_sinistralidade_maxima,
							a.ie_incluir
						from 	mprev_regra_cubo_custo a
						where 	a.nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c03;
			
			open c04;
			loop
			fetch c04 into
				nr_sequencia_w;
			exit 	when c04%notfound;
				begin
				
					select  mprev_regra_cubo_atend_seq.nextval
					into 	nr_seq_regra_cubo_atend_w
					from 	dual;
					
					if	(nvl(nr_seq_regra_cubo_atend_w,0)<>0) then
					
						insert 
						into	mprev_regra_cubo_atend(
							nr_sequencia,
							ie_tipo_guia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							qt_ocorrencia_minima,
							qt_ocorrencia_maxima,
							cd_medico_solicitante,
							cd_medico_executor,
							ie_carater_internacao,
							nr_seq_tipo_atendimento,
							nr_seq_prestador_exec,
							cd_procedimento,
							ie_origem_proced,
							ie_incluir,
							cd_especialidade,
							nr_seq_clinica,
							ie_busca)(
						select	nr_seq_regra_cubo_atend_w,
							a.ie_tipo_guia,
							a.dt_atualizacao,
							a.nm_usuario,
							a.dt_atualizacao_nrec,
							a.nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							a.qt_ocorrencia_minima,
							a.qt_ocorrencia_maxima,
							a.cd_medico_solicitante,
							a.cd_medico_executor,
							a.ie_carater_internacao,
							a.nr_seq_tipo_atendimento,
							a.nr_seq_prestador_exec,
							a.cd_procedimento,
							a.ie_origem_proced,
							a.ie_incluir,
							a.cd_especialidade,
							a.nr_seq_clinica,
							a.ie_busca
						from 	mprev_regra_cubo_atend a
						where  	a.nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c04;
			
			open c05;
			loop
			fetch c05 into
				nr_sequencia_w;
			exit 	when c05%notfound;
				begin
				
					select  mprev_regra_cubo_diag_seq.nextval
					into 	nr_seq_regra_cubo_diag_w
					from 	dual;
					
					if	(nvl(nr_seq_regra_cubo_diag_w,0)<>0) then
					
						insert
						into 	mprev_regra_cubo_diag(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							cd_doenca,
							cd_doenca_inicial,
							cd_doenca_final,
							qt_ocorrencia,
							ie_incluir,
							ie_origem_dados, 
							ie_status)(
						select 	nr_seq_regra_cubo_diag_w,
							a.dt_atualizacao,
							a.nm_usuario,
							a.dt_atualizacao_nrec,
							a.nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							a.cd_doenca,
							a.cd_doenca_inicial,
							a.cd_doenca_final,
							a.qt_ocorrencia,
							a.ie_incluir,
							a.ie_origem_dados, 
							a.ie_status
						from 	mprev_regra_cubo_diag a
						where 	a.nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c05;
			
			open c06;
			loop
			fetch c06 into
				nr_sequencia_w;
			exit 	when c06%notfound;
				begin
				
					select  mprev_regra_cubo_proc_seq.nextval
					into 	nr_seq_regra_cubo_proc_w
					from 	dual;
					
					if	(nvl(nr_seq_regra_cubo_proc_w,0)<>0) then
			
						insert
						into	mprev_regra_cubo_proc(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							cd_procedimento,
							ie_origem_proced,
							qt_ocorrencia,
							nr_seq_regra_proc_sup,
							ie_incluir,
							ie_busca,
							ie_origem_dados)(
						select	nr_seq_regra_cubo_proc_w,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							cd_procedimento,
							ie_origem_proced,
							qt_ocorrencia,
							nr_seq_regra_proc_sup,
							ie_incluir,
							ie_busca,
							ie_origem_dados
						from 	mprev_regra_cubo_proc a
						where	a.nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
						close c06;

			open c07;
			loop
			fetch c07 into
				nr_sequencia_w;
			exit 	when c07%notfound;
				begin				
					select  hdm_regra_cubo_compl_seq.nextval
					into 	nr_seq_hdm_regra_cubo_compl_w
					from 	dual;

					if	(nvl(nr_seq_hdm_regra_cubo_compl_w,0)<>0) then

						insert into hdm_regra_cubo_compl(
							nr_sequencia,
							dt_atualizacao,   
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							ie_tipo_complemento,
							cd_municipio_ibge,
              sg_estado,
						ie_incluir)(
							select  nr_seq_hdm_regra_cubo_compl_w,
							dt_atualizacao,   
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							ie_tipo_complemento,
							cd_municipio_ibge,
              sg_estado,
							ie_incluir
						from	hdm_regra_cubo_compl
						where 	nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c07;

			open c08;
			loop
			fetch c08 into
				nr_sequencia_w;
			exit 	when c08%notfound;
				begin				
					select  hdm_regra_cubo_atend_eup_seq.nextval
						into 	hdm_regra_cubo_atend_eup_w
						from 	dual;

					if	(nvl(hdm_regra_cubo_atend_eup_w,0)<>0) then

						insert into hdm_regra_cubo_atend_eup(
							nr_sequencia,
							dt_atualizacao,       
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							qt_ocorrencia_minima,
							qt_ocorrencia_maxima,
							ie_tipo_atendimento,
							cd_medico_resp,
							ie_incluir,
							nr_seq_regra_cubo,
							ie_tipo_data,
							ie_periodo,
							qt_data_inicial,
							qt_data_final,
							ie_un_tempo_data)(
						select  hdm_regra_cubo_atend_eup_w,
							dt_atualizacao,       
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							qt_ocorrencia_minima,
							qt_ocorrencia_maxima,
							ie_tipo_atendimento,
							cd_medico_resp,
							ie_incluir,
							nr_seq_regra_cubo_w,
							ie_tipo_data,
							ie_periodo,
							qt_data_inicial,
							qt_data_final,
							ie_un_tempo_data
						from	hdm_regra_cubo_atend_eup
							where 	nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c08;

			open c09;
			loop
			fetch c09 into
				nr_sequencia_w;
			exit 	when c09%notfound;
				begin				
					select  hdm_regra_cubo_lista_prob_seq.nextval
						into 	hdm_regra_cubo_lista_prob_w
						from 	dual;

					if	(nvl(hdm_regra_cubo_lista_prob_w,0)<>0) then

						insert into hdm_regra_cubo_lista_prob (
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,     
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							cd_ciap,
							cd_doenca,
							cd_doenca_inicial,
							cd_doenca_final,
							ie_status,
							ie_tipo_data,
							ie_periodo,
							qt_data_inicial,
							qt_data_final,
							ie_un_tempo_data,
							qt_ocorrencia,
							ie_incluir
						)(
							select  hdm_regra_cubo_lista_prob_w,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,     
							nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							cd_ciap,
							cd_doenca,
							cd_doenca_inicial,
							cd_doenca_final,
							ie_status,
							ie_tipo_data,
							ie_periodo,
							qt_data_inicial,
							qt_data_final,
							ie_un_tempo_data,
							qt_ocorrencia,
							ie_incluir
							from hdm_regra_cubo_lista_prob
							where nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c09;


			open c10;
			loop
			fetch c10 into
				nr_sequencia_w;
			exit 	when c10%notfound;
				begin				
					select  hdm_regra_cubo_trat_seq.nextval
						into 	nr_seq_hdm_regra_cubo_trat_w
						from 	dual;

					if	(nvl(nr_seq_hdm_regra_cubo_trat_w,0)<>0) then

						insert into hdm_regra_cubo_trat(nr_sequencia,
							dt_atualizacao,       
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							ie_tipo_tratamento,
							ie_incluir)(
						select  nr_seq_hdm_regra_cubo_trat_w,
							dt_atualizacao,       
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							ie_tipo_tratamento,
							ie_incluir
							from hdm_regra_cubo_trat
							where nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c10;

			open c11;
			loop
			fetch c11 into
				nr_sequencia_w;
			exit 	when c11%notfound;
				begin				
					select  hdm_regra_cubo_medic_seq.nextval
						into 	nr_seq_hdm_regra_cubo_medic_w
						from 	dual;

					if	(nvl(nr_seq_hdm_regra_cubo_medic_w,0)<>0) then

						insert into hdm_regra_cubo_medic(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo,
							cd_material,
							ie_periodo,
							qt_data_inicial,
							qt_data_final,
							ie_un_tempo_data,
							qt_dose_inicial,
							qt_dose_final,
							cd_unidade_medida,
							ie_incluir
						)(
							select  nr_seq_hdm_regra_cubo_medic_w,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_regra_cubo_w,
							cd_material,
							ie_periodo,
							qt_data_inicial,
							qt_data_final,
							ie_un_tempo_data,
							qt_dose_inicial,
							qt_dose_final,
							cd_unidade_medida,
							ie_incluir
							from hdm_regra_cubo_medic
							where nr_sequencia = nr_sequencia_w);
					end if;
				end;
			end loop;
			close c11;

		end if;			
	end if;		
commit;

end hdm_duplicar_regra;
/