create or replace procedure duv_gerar_segmento_vne(nr_seq_mensagem_p in duv_mensagem.nr_sequencia%type,
                                                   nm_usuario_p      in usuario.nm_usuario%type,
                                                   nr_seq_episodio_p in episodio_paciente.nr_sequencia%type)is
                                                   
  cursor c01 is
    select null ie_reportado, 
           null dt_reportado, 
           null ie_momento_report, 
           null dt_afast_trabalho, 
           null ie_habilitado_trab 
    from dual;                    

  c01_w c01%rowtype;                                                   
                                                   
begin 
  c01_w := null;
  open c01;
  fetch c01 into c01_w;
  close c01;
  insert into duv_vne (nr_sequencia, 
                       dt_atualizacao, 
                       nm_usuario, 
                       dt_atualizacao_nrec, 
                       nm_usuario_nrec, 
                       nr_seq_mensagem, 
                       ie_reportado, 
                       dt_reportado, 
                       ie_momento_report, 
                       dt_afast_trabalho, 
                       ie_habilitado_trab) values (duv_vne_seq.nextval,
                                                   sysdate,
                                                   nm_usuario_p,
                                                   sysdate,
                                                   nm_usuario_p,
                                                   nr_seq_mensagem_p,
                                                   c01_w.ie_reportado, 
                                                   c01_w.dt_reportado, 
                                                   c01_w.ie_momento_report, 
                                                   c01_w.dt_afast_trabalho, 
                                                   c01_w.ie_habilitado_trab);
end duv_gerar_segmento_vne;
/