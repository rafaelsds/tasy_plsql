create or replace
procedure gerar_itens_nervo_produto(	nr_seq_atend_p	number,
					nm_usuario_p	Varchar2) is 

nr_sequencia_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	nervo
	where	ie_situacao = 'A';

begin
open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	insert into atend_nervo_prod_item (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_nervo,
		nr_seq_atend)
	values(	atend_nervo_prod_item_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		nr_seq_atend_p);
	end;
end loop;
close C01;

commit;

end gerar_itens_nervo_produto;
/