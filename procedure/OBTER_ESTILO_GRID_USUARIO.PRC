create or replace
procedure obter_estilo_grid_usuario(
			cd_estabelecimento_p	in	number,
			nm_usuario_p		in	varchar2,
			nm_tabela_p		in	varchar2,
			nr_seq_visao_p		in	number,
			nr_seq_objeto_p		in	number,
			ie_negrito_p		out	varchar2,
			ie_italico_p            out	varchar2,
			ie_negrito_italico_p    out	varchar2,
			ie_sublinhado_p         out	varchar2,
			ie_riscado_p            out	varchar2,
			ds_fonte_p              out	varchar2,
			ds_cor_p                out	varchar2,
			qt_tam_fonte_p		out	number) is 
			
begin

select	nvl(max(ie_negrito),'N'),
	nvl(max(ie_italico),'N'),
	nvl(max(ie_negrito_italico),'N'),
	nvl(max(ie_sublinhado),'N'),
	nvl(max(ie_riscado),'N'),
	max(ds_fonte),
	max(ds_cor),
	max(qt_tam_fonte)
into	ie_negrito_p,
        ie_italico_p,		
        ie_negrito_italico_p,
        ie_sublinhado_p,		
        ie_riscado_p,		
        ds_fonte_p,		
        ds_cor_p,
	qt_tam_fonte_p	
from 	estilo_grid_usuario 
where	nm_usuario_ref = nm_usuario_p
and	cd_estabelecimento = cd_estabelecimento_p
and	(nr_seq_objeto_p is null or nr_seq_objeto = nr_seq_objeto_p)
and	(nr_seq_visao_p is null or nr_seq_visao = nr_seq_visao_p)
and	(nm_tabela_p is null or nm_tabela = nm_tabela_p);

end obter_estilo_grid_usuario;
/
