create or replace
procedure inserir_protocolo_conta(nm_usuario_p		Varchar2,
			nr_seq_protocolo_p	number,
			nr_interno_conta_p	number) is 

begin
update	conta_paciente
set	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	nr_seq_protocolo = nr_seq_protocolo_p
where	nr_interno_conta  = nr_interno_conta_p;

commit;

end inserir_protocolo_conta;
/