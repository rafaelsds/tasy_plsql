create or replace
PROCEDURE Consiste_Liberacao_PEPO(	nr_cirurgia_p 			NUMBER,
					nm_usuario_p  			VARCHAR,
					nr_seq_pepo_p			NUMBER,
					consistir_27_p 			VARCHAR2,
					ie_geral_permite_liberar_p	OUT VARCHAR) IS

qt_registros_w				NUMBER(10);
ds_erro_w				VARCHAR2(255);
nr_sequencia_w				NUMBER(10);
ds_consistencia_w			VARCHAR2(255);
ds_mensagem_w				VARCHAR2(255);
ie_permite_liberar_w	 		VARCHAR2(15);
ie_situacao_w				VARCHAR2(1);
qt_saldo_w				NUMBER(5,0);
ie_tipo_cirurgia_w			VARCHAR2(15);
cd_tipo_cirurgia_w			NUMBER(10);
ie_porte_w				VARCHAR2(1);
ie_asa_estado_paciente_w		VARCHAR2(3);
ie_geral_permite_liberar_w		VARCHAR2(1) := 'S';
ie_ExibeAvalLibPepPepo_w		VARCHAR2(15);
nr_prescricao_w				NUMBER(10);
qt_eventos_proced_w			NUMBER(10);
ie_lib_perfil_w				VARCHAR2(1);
dt_termino_w				DATE;
ie_cirurgia_interrompida_w		VARCHAR2(1);
dt_alta_w					DATE;

ie_consiste_w				VARCHAR2(1):='S';
cd_estabelecimento_w		NUMBER(10) := wheb_usuario_pck.get_cd_estabelecimento;

/*
nr_sequencia_w
1 - Falta do registro "eventro" saida do paciente da sala
2 - Controle de cavidade com saldo positivo
3 - Nao preenchimento da pasta cirurgia: tipo de cirurgia
4 - Nao preenchimento da pasta curativos;
5 - Nao preenchimento da pasta posicao
6 - Necessario preencher evolucao;
7 - Pasta cirurgias: o campo classificacao
8 - Pasta cirurgias: o campo porte
9 - Pasta cirurgias: o campo ASA
10 - Agentes anestesicos: ausencia de velocidade
11 - Agentes anestesicos: Inicio e termino da adminstracao;
12 - Medicamentos: ausencia do registro de dose;
13 - Medicamentos: ausencia do registro de velocidade;
14 - Sem tecnica anestesica prenchida;
15 - Necessario liberar evolucao de cirurgia
27 - Ausencia de registro de eventos obrigatorios
34 - Necessario liberar ganhos e perdas
35 - Necessario liberar eventos
36 - Necessario liberar Sinais Vitais e Monitorizacao
37 - Necessario liberar Avaliacoes
38 - Necessario liberar o SAE
40 - Necessario liberar a posicao
42 - Necessario liberar a pele
43 - Necessario liberar a Incisao Cirurgica
44 - Necessario liberar o Curativo
45 - Necessario liberar o Equipamento
46 - Necessario liberar o conjunto da CME
47 - Necessario preencher a via de acesso dos procedimento adicionais.
48 - Necessario registrar o evento que executa os procedimentos previstos
49 - E necessario preencher via de  acesso nos procedimentos previstos
50- Necessario informar o termino da  cirurgia
54 - Necessario preencher dados na pasta Avaliacoes
55 - Necessario preencher os dados da pasta apae
57 - Necessario preencher os dados da pasta pele -> ITEM INATIVO
58- Necessario preencher os dados da pasta incisao cirurgica
59 - E necessario o registro de tricotomia na pasta Pele
61 - E necessario o registro de degermacao na pasta Pele
62 - E necessario o registro de antissepsia na pasta Pele
65 - E necessario que o paciente nao tenha data de alta informada
66 - Necessario o registro de descricao cirurgica na pasta 'Relatorio cirurgia'
201- Existe um ou mais itens de documentacao cirurgica pendente*/

CURSOR C01 IS
	SELECT 	nr_sequencia,
		ie_permite_liberar,
		NVL(ie_cirurgia_interrompida,'S'),
		SUBSTR(obter_consistencia_lib_perfil(nr_sequencia,obter_perfil_ativo),1,1)
	FROM	consistencia_lib_pepo
	WHERE 	ie_situacao = 'A'
	AND 	ie_permite_liberar <> 'A';

BEGIN

Obter_Param_Usuario(872, 219, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w,ie_ExibeAvalLibPepPepo_w);

DELETE	consistencia_cirurgia
WHERE	nr_cirurgia = nr_cirurgia_p OR
	nr_seq_pepo = nr_seq_pepo_p;

IF 	NVL(nr_seq_pepo_p,0) > 0 THEN
	SELECT	MAX(ie_tipo_cirurgia),
		MAX(ie_asa_estado_paciente)
	INTO   	ie_tipo_cirurgia_w,
		ie_asa_estado_paciente_w
	FROM   	pepo_cirurgia
	WHERE  	nr_sequencia = nr_seq_pepo_p;

	SELECT	MAX(cd_tipo_cirurgia),
		MAX(ie_porte),
		MAX(dt_termino)
	INTO   	cd_tipo_cirurgia_w,
		ie_porte_w,
		dt_termino_w
	FROM   	cirurgia
	WHERE  	nr_seq_pepo = nr_seq_pepo_p;
ELSE
	SELECT	MAX(ie_tipo_cirurgia),
		MAX(cd_tipo_cirurgia),
		MAX(ie_porte),
		MAX(ie_asa_estado_paciente),
		MAX(dt_termino)
	INTO   	ie_tipo_cirurgia_w,
		cd_tipo_cirurgia_w,
		ie_porte_w,
		ie_asa_estado_paciente_w,
		dt_termino_w
	FROM   	cirurgia
	WHERE  	nr_cirurgia = nr_cirurgia_p;
END IF;

SELECT	MAX(nr_prescricao)
INTO    nr_prescricao_w
FROM	cirurgia a
WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p));

OPEN c01;
LOOP
FETCH c01 INTO
	nr_sequencia_w,
	ie_permite_liberar_w,
	ie_cirurgia_interrompida_w,
	ie_lib_perfil_w;
EXIT WHEN c01%NOTFOUND;
	BEGIN

	ie_consiste_w := 'S';
	IF	(ie_cirurgia_interrompida_w = 'N') THEN
		SELECT	NVL(MAX('S'),'N')
		INTO	ie_consiste_w
		FROM	cirurgia
		WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
		AND	ie_status_cirurgia <> 4;
	END IF;
	IF	(ie_consiste_w = 'S') AND (ie_lib_perfil_w = 'S') THEN
		IF 	(nr_sequencia_w = 1) THEN
			/* 1 - Falta do registro "eventro" saida do paciente da sala */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	evento_cirurgia a,
				evento_cirurgia_paciente b
			WHERE	a.nr_sequencia			= b.nr_seq_evento
			AND	a.ie_finaliza_cirurgia 		= 'S'
			AND	NVL(b.ie_situacao,'A') = 'A'
			AND	((a.cd_estabelecimento = cd_estabelecimento_w) OR (a.cd_estabelecimento IS NULL))
			AND	((b.nr_cirurgia 		= nr_cirurgia_p) OR (b.nr_seq_pepo = nr_seq_pepo_p));

			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 2) THEN
			/* 2 - Controle de cavidade com saldo positivo */
			SELECT	SUM(DECODE(ie_acao,1,qt_item,0) - DECODE(ie_acao,2,qt_item,3,qt_item,4,qt_item,0))
			INTO	qt_saldo_w
			FROM	pepo_item_controle b,
				cirurgia_item_controle a
			WHERE	a.nr_seq_item_controle 	= b.nr_sequencia
			AND	((nr_cirurgia 		= nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	NVL(a.ie_situacao,'A')	= 'A'
			AND	b.ie_controla_saldo 	= 'S';

			IF 	(qt_saldo_w > 0)	 THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 3) THEN
			/* 3 - Nao preenchimento da pasta cirurgia: tipo de cirurgia */
			IF 	(ie_tipo_cirurgia_w IS NULL) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 4) THEN
			/* 4 - Nao preenchimento da pasta curativos; */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pepo_curativo
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	(NVL(ie_situacao,'A') = 'A');

			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 5) THEN
			/* 5 - Nao preenchimento da pasta posicao */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	posicao_cirurgia
			WHERE	((nr_cirurgia 	= nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	(NVL(ie_situacao,'A') = 'A');

			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 6) THEN
			/* 6 - Necessario preencher evolucao; */
			SELECT 	COUNT(*)
			INTO	qt_registros_w
			FROM	evolucao_paciente
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p));

			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 7) THEN
			/* 7 - Pasta cirurgias: o campo classificacao */
			IF	(cd_tipo_cirurgia_w IS NULL) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 8) THEN
			/* 8 - Pasta cirurgias: o campo porte */
			IF	(ie_porte_w IS NULL) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 9) THEN
			/* 9 - Pasta cirurgias: o campo ASA */
			IF	(ie_asa_estado_paciente_w IS NULL) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 10) THEN
			/* 10 - Agentes anestesicos: ausencia de velocidade */
			SELECT 	COUNT(*)
			INTO	qt_registros_w
			FROM	cirurgia_agente_anestesico a,
				cirurgia_agente_anest_ocor b
			WHERE	a.nr_sequencia = b.nr_seq_cirur_agente
			AND	b.qt_velocidade_inf IS NULL
			AND	ie_modo_registro = 'V'
			AND	NVL(a.ie_situacao,'A') = 'A'
			AND	NVL(b.ie_situacao,'A') = 'A'
			AND	((nr_cirurgia 	= nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	a.ie_tipo = 1;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 11) THEN
			/* 11 - Agentes anestesicos: Inicio e termino da adminstracao; */
			SELECT 	COUNT(*)
			INTO	qt_registros_w
			FROM	cirurgia_agente_anestesico a,
				cirurgia_agente_anest_ocor b
			WHERE	a.nr_sequencia = b.nr_seq_cirur_agente
			AND	((b.dt_inicio_adm IS NULL) OR (dt_final_adm IS NULL))
			AND	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND	NVL(a.ie_situacao,'A') = 'A'
			AND	NVL(b.ie_situacao,'A') = 'A'
			AND	a.ie_tipo = 1;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 12) THEN
			/* 12 - Necessario informar a dose agente anestesico */
			SELECT 	COUNT(*)
			INTO	qt_registros_w
			FROM	cirurgia_agente_anestesico a,
				cirurgia_agente_anest_ocor b
			WHERE	a.nr_sequencia = b.nr_seq_cirur_agente
			AND	b.qt_dose IS NULL
			AND	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND	a.ie_modo_adm = 'IN'
			AND	NVL(a.ie_situacao,'A') = 'A'
			AND	NVL(b.ie_situacao,'A') = 'A'
			AND	a.ie_tipo = 1;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 13) THEN
			SELECT 	COUNT(*)
			INTO	qt_registros_w
			FROM	cirurgia_agente_anestesico a,
				cirurgia_agente_anest_ocor b
			WHERE	a.nr_sequencia = b.nr_seq_cirur_agente
			AND	b.qt_velocidade_inf IS NULL
			AND	ie_modo_registro = 'V'
			AND	NVL(a.ie_situacao,'A') = 'A'
			AND	NVL(b.ie_situacao,'A') = 'A'
			AND	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND	a.ie_tipo = 3;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 14) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	cirurgia_tec_anestesica
			WHERE 	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p));

			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 15) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	evolucao_paciente
			WHERE 	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF	(nr_sequencia_w = 27) AND
			(consistir_27_p = 'S') THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM 	evento_cirurgia a
			WHERE	a.ie_evento_obrigatorio = 'S'
			AND		((cd_estabelecimento = cd_estabelecimento_w) OR (cd_estabelecimento IS NULL))
			AND 	obter_se_evento_lib_perfil(0,0,nr_sequencia,obter_perfil_ativo) = 'S'
			AND	NOT EXISTS(	SELECT 	1
							FROM 	evento_cirurgia_paciente b
							WHERE 	b.nr_seq_evento = a.nr_sequencia
							AND	NVL(b.ie_situacao,'A') = 'A'
							AND 	((b.nr_cirurgia = nr_cirurgia_p) OR (b.nr_seq_pepo = nr_seq_pepo_p)));
			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 34) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	atendimento_perda_ganho
			WHERE 	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND		dt_liberacao IS NULL;
			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 35) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	qua_evento_paciente
			WHERE 	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	NVL(ie_tipo_evento,'E') = 'E'
			AND	dt_liberacao IS NULL;
			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 36) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	atendimento_sinal_vital
			WHERE 	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	dt_liberacao IS NULL;
			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 37) THEN
			IF	(ie_ExibeAvalLibPepPepo_w = 'S') THEN
				SELECT 	COUNT(*)
				INTO	qt_registros_w
				FROM	med_avaliacao_paciente a
				WHERE   a.nr_seq_tipo_avaliacao IN ( 	SELECT	nr_sequencia
									FROM    med_tipo_avaliacao
									WHERE   ((cd_funcao = 281) OR (cd_funcao = 872)))
				AND	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
				AND	a.dt_liberacao IS NULL;
			ELSIF (ie_ExibeAvalLibPepPepo_w = 'N') THEN
				SELECT 	COUNT(*)
				INTO	qt_registros_w
				FROM	med_avaliacao_paciente a
				WHERE 	a.nr_seq_tipo_avaliacao IN 	(SELECT nr_sequencia
									 FROM 	med_tipo_avaliacao
									 WHERE 	nr_seq_prontuario = 253)
				AND	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
				AND	a.dt_liberacao IS NULL;
			ELSIF (ie_ExibeAvalLibPepPepo_w = 'SP') THEN
				SELECT 	COUNT(*)
				INTO	qt_registros_w
				FROM	med_avaliacao_paciente a
				WHERE   a.nr_seq_tipo_avaliacao IN ( 	SELECT	nr_sequencia
									FROM    med_tipo_avaliacao
									WHERE   ((cd_funcao IS NULL) OR (cd_funcao = 872)))
				AND	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
				AND	a.dt_liberacao IS NULL;
			END IF;
			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 38) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pe_prescricao a
			WHERE 	EXISTS	(SELECT 1
					FROM  	cirurgia x
					WHERE 	((x.nr_cirurgia = nr_cirurgia_p) OR (x.nr_seq_pepo = nr_seq_pepo_p))
					AND	x.nr_atendimento = a.nr_atendimento)
			AND	Obter_se_mod_SAE_lib_vis(a.nr_seq_modelo) = 'S'
			AND	a.dt_liberacao IS NULL;
			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 40) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	posicao_cirurgia a
			WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND 	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 42) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pepo_pele a
			WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND 	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 43) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	incisao_cirurgia a
			WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND 	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 44) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pepo_curativo a
			WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND 	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 45) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	equipamento_cirurgia a
			WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND 	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 46) THEN
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	cm_conjunto_cont a
			WHERE 	((a.nr_cirurgia = nr_cirurgia_p) OR (a.nr_seq_pepo = nr_seq_pepo_p))
			AND 	dt_liberacao IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 47) THEN
				SELECT	COUNT(*)
				INTO	qt_registros_w
				FROM	prescr_procedimento a,
					procedimento b
				WHERE 	a.nr_prescricao    =  nr_prescricao_w
				AND     a.cd_procedimento  = b.cd_procedimento
				AND     a.ie_origem_proced = b.ie_origem_proced
				AND     b.ie_classificacao = 1
				and       a.dt_suspensao is null
				AND     a.ie_via_acesso IS NULL;

			IF 	(qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF (nr_sequencia_w = 48) THEN

			SELECT COUNT(*)
			INTO   qt_eventos_proced_w
			FROM   evento_cirurgia
			WHERE  NVL(ie_executa_procedimentos,'N') = 'S'
			AND ((cd_estabelecimento = cd_estabelecimento_w) OR (cd_estabelecimento IS NULL))
			AND    NVL(ie_situacao,'A') = 'A';

			IF (qt_eventos_proced_w > 0) THEN
				SELECT 	COUNT(*)
				INTO   	qt_registros_w
				FROM 	evento_cirurgia a,
					evento_cirurgia_paciente b
				WHERE	a.nr_sequencia = b.nr_seq_evento
				AND	NVL(a.ie_executa_procedimentos,'S') = 'S'
				AND	NVL(b.ie_situacao,'A') = 'A'
				AND ((a.cd_estabelecimento = cd_estabelecimento_w) OR (a.cd_estabelecimento IS NULL))
				AND 	((b.nr_cirurgia = nr_cirurgia_p) OR (b.nr_seq_pepo = nr_seq_pepo_p));

				IF 	(qt_registros_w = 0) THEN
					grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
				END IF;
			END IF;

		ELSIF 	(nr_sequencia_w = 50) THEN
			/* 50 - Necessario informar o termino da  cirurgia */
			IF 	(dt_termino_w IS NULL) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 54) THEN
			/* 54 - Necessario preencher os dados da pasta avaliacoes */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	med_avaliacao_paciente
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 55) THEN
			/* 55 - Necessario preencher os dados da pasta apae */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	aval_pre_anestesica
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 58) THEN
			/* 58 - Necessario preencher os dados da pasta incisao cirurgica */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	incisao_cirurgia
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 59) THEN
			/* 59 - E necessario o registro de tricotomia na pasta Pele */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pepo_pele
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND 	ie_trico_antis = 'T'
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 61) THEN
			/* 61 - E necessario o registro de degermacao na pasta Pele */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pepo_pele
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND 	ie_trico_antis = 'D'
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 62) THEN
			/* 62 - E necessario o registro de antissepsia na pasta Pele */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	pepo_pele
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND 	ie_trico_antis = 'A'
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 65) THEN
			/* 65 - E necessario que o paciente nao tenha data de alta informada */
			SELECT	NVL(MAX(dt_alta),NULL)
			INTO	dt_alta_w
			FROM	cirurgia a,
				atendimento_paciente b
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	a.nr_atendimento = b.nr_atendimento;
			IF 	(dt_alta_w IS NOT NULL) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		ELSIF 	(nr_sequencia_w = 66) THEN
			/* 66 - Necessario o registro de descricao cirurgica na pasta 'Relatorio cirurgia' */
			SELECT	COUNT(*)
			INTO	qt_registros_w
			FROM	cirurgia_descricao
			WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			AND	(NVL(ie_situacao,'A') = 'A');
			IF 	(qt_registros_w = 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		END IF;
	END IF;
	IF (ie_consiste_w = 'S') AND (nvl((pkg_i18n.get_user_locale), 'pt_BR') = 'de_AT') THEN
		IF (nr_sequencia_w = 201) THEN
			/* 201 - Existe um ou mais itens de documentacao cirurgica pendente */
			select	count(*)
			into	qt_registros_w
			from	pepo_doc_status
			where	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
			and cd_item <> 4
			and	ie_status <> 'C';
			IF (qt_registros_w > 0) THEN
				grava_consistencia_pepo(nr_sequencia_w,nr_cirurgia_p,ie_permite_liberar_w,nm_usuario_p,nr_seq_pepo_p);
			END IF;
		END IF;
	END IF;
	END;
END LOOP;
CLOSE c01;

SELECT	NVL(MAX('N'),'S')
INTO	ie_geral_permite_liberar_w
FROM	consistencia_cirurgia
WHERE	((nr_cirurgia = nr_cirurgia_p) OR (nr_seq_pepo = nr_seq_pepo_p))
and		obter_se_cirurgia_cancelada(nr_cirurgia) = 'N'
AND	ie_permite_liberar = 'N';

ie_geral_permite_liberar_p := ie_geral_permite_liberar_w;

COMMIT;

END Consiste_Liberacao_PEPO;
/