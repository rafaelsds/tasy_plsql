create or replace
procedure vincula_atend_agend_OPME(	nr_seq_agenda_p number,
					ie_opcao_p	varchar2,
				 	nm_usuario_p		Varchar2) is 

ie_opme_integracao_w	varchar2(1);
cd_estabelecimento_w	agenda.cd_estabelecimento%type;	
envio_auto_int_opme_w	opme_envio_automatico.ie_evento%type;

/* V - vincular
     D - Desvincular */			
begin

select	nvl(max(a.ie_opme_integracao),'N'),
        max(b.cd_estabelecimento)
into	ie_opme_integracao_w,
        cd_estabelecimento_w
from	agenda_paciente a,
        agenda b
where 	a.cd_agenda	= b.cd_agenda
and    	nr_sequencia 	= nr_seq_agenda_p;

envio_auto_int_opme_w := obter_envio_auto_int_opme(cd_estabelecimento_w, nr_seq_agenda_p, 'AA');

if	(ie_opcao_p = 'V') and (ie_opme_integracao_w = 'S') and (envio_auto_int_opme_w = 'S' or envio_auto_int_opme_w = 'P') then
	ajusta_status_agenda_int_opme(nr_seq_agenda_p, '7', '', nm_usuario_p);
end if;

if	(ie_opcao_p = 'D') and (ie_opme_integracao_w = 'S') and (envio_auto_int_opme_w = 'S' or envio_auto_int_opme_w = 'P') then
	ajusta_status_agenda_int_opme(nr_seq_agenda_p, '107', '', nm_usuario_p);
end if;

commit;

end vincula_atend_agend_OPME;
/
