create or replace
procedure gerar_relat_acessos_ops_prest (	dt_inicial_p		date,
						dt_final_p		date,
						pr_diferenca_p		number,
						nm_usuario_p		Varchar2) is 

nr_count_w		number(5,0);
nm_maquina_w		varchar2(255);
nr_seq_acesso_w		number(15,0);
qt_acesso_operadora_w	number(15,0);
qt_acesso_prestadora_w	number(15,0);
i			integer;
ds_sep_bv_w		varchar2(50);
qt_acessos_total_w	number(15);
pr_acessos_operadora_w	number(15,5);
pr_acessos_prestadora_w	number(15,5);
dt_acesso_w		date;

type dados is record (	nm_maquina		varchar2(255),
			dt_acesso		date,
			qt_operadora		number(15,0),
			qt_prestadora		number(15,0));
type Vetor is table of dados index by binary_integer;
vt_dados_w		Vetor;



Cursor C01 is
	select	a.nr_sequencia,
		a.dt_acesso,
		decode(a.cd_aplicacao_tasy, 	'AvaliacaoPacienteWeb', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						'BibliotecasWeb', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						'CotacaoCompraWeb', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						'LaboratorioWeb', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						'PalmWeb', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						'TasyWeb', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						'WhebPortal', a.ds_maquina || ':' || a.cd_aplicacao_tasy, 
						lower(decode(a.nm_maq_cliente, 'Console', a.ds_maquina, a.nm_maq_cliente))) nm_maquina
	from	tasy_log_acesso a
	where	a.dt_acesso between dt_inicial_p and fim_dia(dt_final_p);

	
	
	
	
	procedure	adiciona_registro_vetor(	nm_maquina_p	varchar2,
							qt_operadora_p	number,
							qt_prestadora_p	number,
							dt_acesso_p	date) is
	ie_encontrou_w	boolean;
	qt_total_w	number(15);
	pr_operadora_w	number(15,5);
	pr_prestadora_w	number(15,5);
	qt_operadora_w	number(1);
	qt_prestadora_w	number(1);
	begin
	
	qt_total_w	:= qt_operadora_p + qt_prestadora_p;
	pr_operadora_w	:= 0;
	pr_prestadora_w	:= 0;
	qt_operadora_w	:= 0;
	qt_prestadora_w	:= 0;
	
	if (qt_total_w > 0) then
		pr_operadora_w	:= (qt_operadora_p * 100) / qt_total_w;
		pr_prestadora_w	:= (qt_prestadora_p * 100) / qt_total_w;
	end if;

	if	(pr_operadora_w >= pr_prestadora_w) then
		qt_operadora_w	:= 1;
		if	((pr_operadora_w - pr_prestadora_w) <= pr_diferenca_p) then
			qt_prestadora_w	:= 1;
		end if;
	else
		qt_prestadora_w	:= 1;
		if	((pr_prestadora_w - pr_operadora_w) <= pr_diferenca_p) then
			qt_operadora_w	:= 1;
		end if;
	end if;
	
	ie_encontrou_w := False;
	for i in 1..vt_dados_w.Count loop
		if	(vt_dados_w(i).nm_maquina = nm_maquina_p) then
			if	(vt_dados_w(i).dt_acesso > dt_acesso_p) then
				vt_dados_w(i).dt_acesso := dt_acesso_p;
			end if;
			vt_dados_w(i).qt_operadora	:= vt_dados_w(i).qt_operadora + qt_operadora_w;
			vt_dados_w(i).qt_prestadora	:= vt_dados_w(i).qt_prestadora + qt_prestadora_w;
			ie_encontrou_w := True;
		end if;
	end loop;
	
	if not ie_encontrou_w then
		i	:= vt_dados_w.Count+1;
		vt_dados_w(i).nm_maquina	:= nm_maquina_p;
		vt_dados_w(i).dt_acesso		:= dt_acesso_p;
		vt_dados_w(i).qt_operadora	:= qt_operadora_w;
		vt_dados_w(i).qt_prestadora	:= qt_prestadora_w;
	end if;
	end;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_acesso_w,
	dt_acesso_w,
	nm_maquina_w;
exit when C01%notfound;
	begin
	select	count(1)
	into	qt_acesso_operadora_w
	from	log_acesso_funcao b,
		funcao c
	where	b.nr_seq_acesso = nr_seq_acesso_w
	and	c.cd_funcao = b.cd_funcao
	and	nvl(c.ie_classif_produto,'A') = 'P';
	--and 	c.ds_aplicacao = 'TasyPLS';

	select	count(1)
	into	qt_acesso_prestadora_w
	from	log_acesso_funcao b,
		funcao c
	where	b.nr_seq_acesso = nr_seq_acesso_w
	and	c.cd_funcao = b.cd_funcao
	and	nvl(c.ie_classif_produto,'A') = 'O';
	--and 	c.ds_aplicacao <> 'TasyPLS';
	
	if (qt_acesso_operadora_w > 0) or (qt_acesso_prestadora_w > 0) then
		adiciona_registro_vetor(nm_maquina_w, qt_acesso_operadora_w, qt_acesso_prestadora_w, dt_acesso_w);
	end if;
	end;
end loop;
close C01;

select	count(table_name)
into	nr_count_w
from	user_tables
where	upper(table_name) = 'RELAT_ACESSOS_OPS_PREST';

if (nr_count_w	= 0) then
	exec_sql_dinamico(nm_usuario_p,'create table RELAT_ACESSOS_OPS_PREST(	nm_usuario		varchar2(15),
										dt_acesso		date,
										nm_maquina		varchar2(255),
										qt_operadora		number(15,0),
										qt_prestadora		number(15,0),
										pr_operadora		number(15,5),
										pr_prestadora		number(15,5))'); 
else
	exec_sql_dinamico(nm_usuario_p,'delete from RELAT_ACESSOS_OPS_PREST where nm_usuario = ' || chr(39) || nm_usuario_p || chr(39));
end if;

ds_sep_bv_w	:= obter_separador_bv;

for i in 1..vt_dados_w.Count loop

	qt_acessos_total_w	:= vt_dados_w(i).qt_operadora + vt_dados_w(i).qt_prestadora;
	pr_acessos_operadora_w	:= 0;
	pr_acessos_prestadora_w	:= 0;

	if (qt_acessos_total_w > 0) then
		pr_acessos_operadora_w	:= round((vt_dados_w(i).qt_operadora * 100) / qt_acessos_total_w,2);
		pr_acessos_prestadora_w	:= round((vt_dados_w(i).qt_prestadora * 100) / qt_acessos_total_w,2);
	end if;

	if (pr_acessos_operadora_w > 0) or (pr_acessos_prestadora_w > 0) then	
		exec_sql_dinamico_bv (	nm_usuario_p,
					'insert into RELAT_ACESSOS_OPS_PREST (nm_usuario, dt_acesso, nm_maquina, qt_operadora, qt_prestadora, pr_operadora, pr_prestadora) '||
					'values (:nm_usuario, :dt_acesso, :nm_maquina, :qt_operadora, :qt_prestadora, :pr_operadora, :pr_prestadora)',
					'nm_usuario=' || nm_usuario_p || ds_sep_bv_w || 
					'dt_acesso=' || to_char(vt_dados_w(i).dt_acesso,'dd/mm/yyyy hh24:mi:ss') || ds_sep_bv_w || 
					'nm_maquina=' || vt_dados_w(i).nm_maquina || ds_sep_bv_w || 
					'qt_operadora=' || to_char(vt_dados_w(i).qt_operadora) || ds_sep_bv_w || 
					'qt_prestadora=' || to_char(vt_dados_w(i).qt_prestadora) || ds_sep_bv_w || 
					'pr_operadora=' || to_char(pr_acessos_operadora_w) || ds_sep_bv_w || 
					'pr_prestadora=' || to_char(pr_acessos_prestadora_w));
		commit;
	end if;
end loop;

end gerar_relat_acessos_ops_prest;
/
