create or replace
procedure GERAR_DATA_INTERV_DIFF_RP(nr_seq_cpoe_rp_p    number,
                                    nm_usuario_p        varchar2) is

nr_seq_cpoe_material_w  cpoe_material.nr_sequencia%type;
ie_segunda_w            cpoe_rp.ie_segunda%type;
ie_terca_w              cpoe_rp.ie_segunda%type;
ie_quarta_w             cpoe_rp.ie_segunda%type;
ie_quinta_w             cpoe_rp.ie_segunda%type;
ie_sexta_w              cpoe_rp.ie_segunda%type;
ie_sabado_w             cpoe_rp.ie_segunda%type;
ie_domingo_w            cpoe_rp.ie_segunda%type;
ie_inter_diff_w         varchar(1) := 'N';
qt_diff_date_w          number(10);

cursor c01 is
select  b.ie_segunda,
        b.ie_terca,
        b.ie_quarta,
        b.ie_quinta,
        b.ie_sexta,
        b.ie_sabado,
        b.ie_domingo,
        a.nr_sequencia
from    cpoe_material a,
        cpoe_rp b
where   a.nr_seq_cpoe_rp = b.nr_sequencia
and     b.nr_sequencia = nr_seq_cpoe_rp_p
and     (
            nvl(b.ie_segunda, 'S') = 'N' or
            nvl(b.ie_terca, 'S') = 'N' or
            nvl(b.ie_quarta, 'S') = 'N' or
            nvl(b.ie_quinta, 'S') = 'N' or
            nvl(b.ie_sexta, 'S') = 'N' or
            nvl(b.ie_sabado, 'S') = 'N' or
            nvl(b.ie_domingo, 'S') = 'N'
        ); -- if any of the weekdays field is N, it means that the user used differentiated interval
        
cursor c02 is
select  a.nr_sequencia
from    cpoe_material a,
        cpoe_rp b
where   a.nr_seq_cpoe_rp = b.nr_sequencia
and     b.nr_sequencia = nr_seq_cpoe_rp_p; 

begin

open c01;
loop
fetch c01 into
    ie_segunda_w,
    ie_terca_w,
    ie_quarta_w,
    ie_quinta_w,
    ie_sexta_w,
    ie_sabado_w,
    ie_domingo_w,
    nr_seq_cpoe_material_w;
exit when c01%notfound;

    ie_inter_diff_w := 'S'; -- only can check interval differentiate by date
    -- if user do not used differentiated interval
    -- only one option can be used at once

    update cpoe_material
    set ie_segunda = ie_segunda_w,
        ie_terca = ie_terca_w,
        ie_quarta = ie_quarta_w,
        ie_quinta = ie_quinta_w, 
        ie_sexta = ie_sexta_w,
        ie_sabado = ie_sabado_w,
        ie_domingo = ie_domingo_w
    where nr_sequencia = nr_seq_cpoe_material_w;

end loop;
close c01;

select  count(*)
into    qt_diff_date_w
from    cpoe_diff_date_rp a
where   ie_inter_diff_w = 'N'
and     a.nr_seq_cpoe_rp = nr_seq_cpoe_rp_p;

if     (qt_diff_date_w > 0 and ie_inter_diff_w = 'N') then -- enter here if interval differentiated by dates

    open c02;
    loop
    fetch c02 into
        nr_seq_cpoe_material_w;
    exit when c02%notfound;
    
        insert into cpoe_diff_date(
            nr_sequencia,
            dt_atualizacao,
            dt_atualizacao_nrec,
            nm_usuario,
            nm_usuario_nrec,
            nr_seq_cpoe_material,
            dt_administration)
        select  cpoe_diff_date_seq.nextval,
                sysdate,
                sysdate,
                nm_usuario_p,
                nm_usuario_p,
                nr_seq_cpoe_material_w,
                dt_administration
        from    cpoe_diff_date_rp
        where   nr_seq_cpoe_rp = nr_seq_cpoe_rp_p;
        
    end loop;
    close c02;

end if;

commit;

end GERAR_DATA_INTERV_DIFF_RP;
/