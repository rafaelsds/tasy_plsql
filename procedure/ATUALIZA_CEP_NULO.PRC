create or replace 
procedure atualiza_cep_nulo
			(nm_usuario_p	varchar2) is

/* variaveis localidade (cursor) */
nr_seq_loc_w	number(10);
nm_localidade_w	varchar2(60);
ds_uf_w		valor_dominio.vl_dominio%type;

/* variaveis tasy_versao */
cd_cep_ver_w	number(8,0);
ds_uf_ver_w	varchar2(2);

/* variaveis logradouro */
nr_seq_log_w	number(10,0);
ds_uf_log_w	valor_dominio.vl_dominio%type;
cd_cep_log_w	number(8,0);

/* obter ceps nulos */
cursor c01 is
select	nr_sequencia,
	nm_localidade,
	ds_uf
from	cep_loc
where	cd_cep is null;

c02	  	Integer;		
retorno_w	Number(5);
ds_comando_w	varchar2(2000);
ds_valor_w	varchar2(255);
begin

open c01;
loop
fetch c01 into	nr_seq_loc_w,
			nm_localidade_w,
			ds_uf_w;
exit when c01%notfound;
	begin
	/* tentar obter cep v�lido tasy_versao */

	ds_comando_w := ' select	nvl(max(cd_cep),0),' ||
			'		max(ds_uf) ' ||
			' from		tasy_versao.cep_loc_nulo ' ||
			' where		nr_sequencia = :nr_sequencia';

	C02 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C02, ds_comando_w, dbms_sql.Native);
	DBMS_SQL.DEFINE_COLUMN(C02, 1, ds_valor_w,255);
	DBMS_SQL.DEFINE_COLUMN(C02, 2, ds_valor_w,255);
	DBMS_SQL.BIND_VARIABLE(C02, 'NR_SEQUENCIA', nr_seq_loc_w,255);
	retorno_w := DBMS_SQL.execute(c02);
	retorno_w := DBMS_SQL.fetch_rows(c02);
	DBMS_SQL.COLUMN_VALUE(C02, 1, ds_valor_w);
	cd_cep_ver_w := to_number(ds_valor_w);
	DBMS_SQL.COLUMN_VALUE(C02, 2, ds_valor_w);
	ds_uf_ver_w := substr(ds_valor_w,1,2);
	DBMS_SQL.CLOSE_CURSOR(C02);

	if	(cd_cep_ver_w <> 0) then
		update	cep_loc
		set	cd_cep			= cd_cep_ver_w,
			ds_uf			= ds_uf_ver_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia		= nr_seq_loc_w;

	else	/* tentar obter cep v�lido logradouro (integridade) - Rafael em 08/02/2007 OS49509 */
		select	nvl(min(nr_sequencia),0)
		into	nr_seq_log_w
		from	cep_log
		where	nr_seq_loc = nr_seq_loc_w
		and	cd_cep is not null;

		if	(nr_seq_log_w <> 0) then
			select	nvl(max(cd_cep),0),
				max(ds_uf)
			into	cd_cep_log_w,
				ds_uf_log_w
			from	cep_log
			where	nr_sequencia = nr_seq_log_w;

			update	cep_loc
			set	cd_cep			= cd_cep_log_w,
				ds_uf			= ds_uf_log_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia		= nr_seq_loc_w;

		else	/* tentar obter cep v�lido logradouro (descri��o)  - Rafael em 08/02/2007 OS49509 */
			select	nvl(min(nr_sequencia),0)
			into	nr_seq_log_w
			from	cep_log
			where	upper(nm_logradouro) = upper(nm_localidade_w)
			and	upper(ds_uf) = upper(ds_uf_w)
			and	cd_cep is not null;

			if	(nr_seq_log_w <> 0) then
				select	nvl(max(cd_cep),0),
					max(ds_uf)
				into	cd_cep_log_w,
					ds_uf_log_w
				from	cep_log
				where	nr_sequencia = nr_seq_log_w;

				update	cep_loc
				set	cd_cep			= cd_cep_log_w,
					ds_uf			= ds_uf_log_w,
					nm_usuario		= nm_usuario_p,
					dt_atualizacao	= sysdate
				where	nr_sequencia		= nr_seq_loc_w;
			end if;			
		end if;
	end if;
	end;
end loop;
close c01;

commit;

end atualiza_cep_nulo;
/