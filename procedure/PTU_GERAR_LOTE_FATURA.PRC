create or replace
procedure ptu_gerar_lote_fatura
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

cd_cgc_fin_w			varchar2(14);				
cd_cooperativa_w		varchar2(10);
cd_unimed_origem_w		varchar2(10);
ie_sem_camara_comp_w		varchar2(10);
ie_gerar_fatura_w		varchar2(10);
ie_filha_w			varchar2(10);
cd_cooperativa_ant_w		varchar2(10);
ie_forma_pagamento_w		varchar2(3)	:= null;
ie_estrutura_cong_w		varchar2(1)	:= 'N';
vl_total_fatura_w		number(15,2);
nr_seq_fatura_w			number(10);
nr_seq_conta_w			number(10);
nr_seq_resumo_conta_w		number(10);
nr_seq_camara_w			number(10);
qt_camara_w			number(10);
nr_seq_periodo_pgto_w		number(10);
nr_seq_evento_w			number(10);
qt_periodo_w			number(10);
nr_seq_congenere_lote_w		number(10)	:= null;
nr_seq_congenere_w		number(10);
nr_seq_cong_resp_financ_w	number(10);
qt_dia_a500_w			number(10);
qt_cobrancas_w			number(10);
dt_inicio_w			date;
dt_fim_w			date;
dt_prev_envio_w			date;
dt_vencimento_w			date;
dt_emissao_w			date;

Cursor C01 is
	select	a.nr_seq_conta,
		a.cd_cooperativa_pgto,
		a.nr_sequencia,
		a.nr_seq_evento
	from	pls_conta_medica_resumo	a
	where	a.nr_seq_fatura is null
	and	a.ie_tipo_item	= 'I'
	and	(((ie_forma_pagamento_w = 'P') and (a.ie_status_protocolo = '3')) or
		((ie_forma_pagamento_w = 'C') and (a.ie_status = 'F')))
	and	a.dt_competencia between inicio_dia(dt_inicio_w) and fim_dia(dt_fim_w)
	and	((a.ie_situacao != 'I') or (a.ie_situacao is null));
	
Cursor C02 is
	select	a.nr_sequencia
	from	ptu_fatura	a
	where	a.nr_seq_lote	= nr_seq_lote_p
	and	a.dt_vencimento_fatura	is null;
	
Cursor C03 is
	select	nr_sequencia
	from	ptu_fatura
	where	nr_seq_lote	= nr_seq_lote_p;

begin
begin
select	max(ie_forma_pagamento)
into	ie_forma_pagamento_w
from	pls_parametro_pagamento
where	cd_estabelecimento	= cd_estabelecimento_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(194991);
end;

/* Obter dados do lote da fatura */
select	dt_inicio,
	fim_dia(dt_fim),
	nr_seq_camara,
	nvl(ie_sem_camara_comp,'N'),
	nr_seq_periodo_pgto,
	ie_estrutura_cong,
	nr_seq_congenere,
	nvl(dt_previsao_envio,sysdate),
	dt_vencimento,
	dt_emissao
into	dt_inicio_w,
	dt_fim_w,
	nr_seq_camara_w,
	ie_sem_camara_comp_w,
	nr_seq_periodo_pgto_w,
	ie_estrutura_cong_w,
	nr_seq_congenere_lote_w,
	dt_prev_envio_w,
	dt_vencimento_w,
	dt_emissao_w
from	ptu_lote_fatura_envio
where	nr_sequencia	= nr_seq_lote_p;

cd_unimed_origem_w	:= pls_obter_unimed_estab(cd_estabelecimento_p); /* Obter a Unimed do estabelecimento */

open C01;
loop
fetch C01 into	
	nr_seq_conta_w,
	cd_cooperativa_w,
	nr_seq_resumo_conta_w,
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	ie_gerar_fatura_w	:= 'S';
	qt_periodo_w		:= 1;
	ie_filha_w		:= 'S';

	if	(nr_seq_periodo_pgto_w is not null) then /*Caso o lote tenha periodo definido*/
		select	count(1)
		into	qt_periodo_w
		from	pls_evento_regra
		where	nr_seq_evento	= nr_seq_evento_w
		and	nr_seq_periodo	= nr_seq_periodo_pgto_w
		and	rownum 		= 1;
	end if;

	if	(nr_seq_camara_w is not null) then /*Caso tenha camara definido*/
		select	count(1)
		into	qt_camara_w
		from	pls_congenere_camara	x,
			pls_congenere		c
		where	x.nr_seq_congenere	= c.nr_sequencia
		and	x.nr_seq_camara		= nr_seq_camara_w
		and	trunc(sysdate,'dd')	<= fim_dia(x.dt_fim_vigencia)
		and	c.cd_cooperativa	= cd_cooperativa_w
		and	rownum 			= 1;
		
		if	(qt_camara_w	= 0) then
			ie_gerar_fatura_w	:= 'N';
		end if;
	end if;

	if	(ie_sem_camara_comp_w	= 'S') then /*Caso sej� para as cooperativa sem camaras*/
		select	count(1)
		into	qt_camara_w
		from	pls_congenere_camara	x,
			pls_congenere		c
		where	x.nr_seq_congenere	= c.nr_sequencia
		and	trunc(sysdate,'dd')	<= fim_dia(x.dt_fim_vigencia)
		and	c.cd_cooperativa	= cd_cooperativa_w
		and	rownum			= 1;
		
		if	(qt_camara_w	<> 0) then
			ie_gerar_fatura_w	:= 'N';
		end if;
	end if;	
	
	select	max(a.nr_sequencia) /* Obter qual o nr_seq_congenere pelo codigo da unimed */
	into	nr_seq_congenere_w
	from	pls_congenere a
	where	a.cd_cooperativa = cd_cooperativa_w
	and	(a.dt_exclusao is null or a.dt_exclusao > dt_prev_envio_w);

	if	(nr_seq_congenere_w is null) then
		select	max(a.nr_sequencia)
		into	nr_seq_congenere_w
		from	pls_congenere a
		where	somente_numero(a.cd_cooperativa) = somente_numero(cd_cooperativa_w)
		and	(a.dt_exclusao is null or a.dt_exclusao > dt_prev_envio_w);
	end if;

	if	(nvl(cd_cooperativa_ant_w,'X') <> nvl(cd_cooperativa_w,'X')) and
		(nr_seq_congenere_lote_w is not null) then
		if	(nr_seq_congenere_lote_w <>  nr_seq_congenere_w) then
			if	(ie_estrutura_cong_w = 'S') then
				ie_filha_w	:= pls_obter_se_unimed_superior(nr_seq_congenere_w, nr_seq_congenere_lote_w);
			else
				ie_filha_w	:= 'N';
			end if;
		end if;
	end if;
	
	if	(ie_gerar_fatura_w	= 'S') and
		(qt_periodo_w		<> 0) and
		(ie_filha_w		= 'S') then
		/*Verificar se j� existe fatura no lote para a unimed*/
		select	max(nr_sequencia)
		into	nr_seq_fatura_w
		from	ptu_fatura
		where	cd_unimed_destino	= cd_cooperativa_w
		and	nr_seq_lote		= nr_seq_lote_p;

		if	(nr_seq_fatura_w is null) then
			cd_cgc_fin_w	:= pls_obter_coop_pag_resp_fin(nr_seq_congenere_w,sysdate);
			
			if	(cd_cgc_fin_w is not null) then
				select	max(nr_sequencia)
				into	nr_seq_cong_resp_financ_w
				from	pls_congenere
				where	cd_cgc = cd_cgc_fin_w;
			end if;
			
			select	ptu_fatura_seq.nextval
			into	nr_seq_fatura_w
			from	dual;
			
			insert into ptu_fatura
				(nr_sequencia, cd_estabelecimento, cd_unimed_destino, 
				cd_unimed_origem, dt_geracao, nr_competencia, 
				nr_fatura,  dt_atualizacao, nm_usuario, 
				dt_atualizacao_nrec, nm_usuario_nrec, dt_emissao_fatura, 
				vl_total_fatura, nr_versao_transacao, ie_tipo_fatura, 
				ie_operacao, nr_seq_lote, dt_vencimento_fatura,
				nr_seq_cong_resp_financ, ie_doc_fisica_conferida)
			values	(nr_seq_fatura_w, cd_estabelecimento_p, cd_cooperativa_w,
				cd_unimed_origem_w, trunc(sysdate,'dd'), to_number(to_char(sysdate,'yymm')),
				null, sysdate, nm_usuario_p, 
				sysdate, nm_usuario_p, trunc(nvl(dt_emissao_w,sysdate),'dd'), 
				0, 16, 1, 
				'E', nr_seq_lote_p, dt_vencimento_w,
				nr_seq_cong_resp_financ_w, 'N');
				
			ptu_atualizar_status_fat_envio(nr_seq_fatura_w, 1, nm_usuario_p);
		end if;

		update	pls_conta_medica_resumo
		set	nr_seq_fatura	= nr_seq_fatura_w
		where	nr_sequencia	= nr_seq_resumo_conta_w
		and	nr_seq_conta	= nr_seq_conta_w
		and	ie_situacao = 'A';
		
		select	nvl(sum(a.vl_liberado),0)
		into	vl_total_fatura_w
		from	pls_conta_medica_resumo a
		where	a.nr_seq_fatura	= nr_seq_fatura_w
		and	a.ie_situacao = 'A';
		
		update	ptu_fatura
		set	vl_total	= vl_total_fatura_w
		where	nr_sequencia	= nr_seq_fatura_w;
	end if;
	
	cd_cooperativa_ant_w	:= cd_cooperativa_w;
	end;
end loop;
close C01;

update	ptu_lote_fatura_envio
set	dt_geracao_lote	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_Atualizacao	= sysdate
where	nr_sequencia	= nr_seq_lote_p;

ptu_gerar_fatura(nr_seq_lote_p,nm_usuario_p);

ptu_desdobrar_fatura_lote(nr_seq_lote_p, cd_estabelecimento_p,nm_usuario_p);

open C02;
loop
fetch C02 into	
	nr_seq_fatura_w;
exit when C02%notfound;
	begin
	ptu_obter_venc_fatura(	nr_seq_fatura_w,
				cd_estabelecimento_p,
				nm_usuario_p,
				dt_vencimento_w,
				qt_dia_a500_w);
	
	update	ptu_fatura
	set	dt_vencimento_fatura	= dt_vencimento_w
	where	nr_sequencia		= nr_seq_fatura_w;
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_fatura_w;
exit when C03%notfound;
	begin
	select	count(1)
	into	qt_cobrancas_w
	from	ptu_nota_cobranca
	where	nr_seq_fatura		= nr_seq_fatura_w
	and	ie_necessita_doc_fisico	= 'S'
	and	rownum			= 1;
	
	if	(qt_cobrancas_w > 0) then
		update 	ptu_fatura
		set	ie_necessita_doc_fisico	= 'S'
		where	nr_sequencia	= nr_seq_fatura_w;
	else
		update 	ptu_fatura
		set	ie_necessita_doc_fisico	= 'N'
		where	nr_sequencia	= nr_seq_fatura_w;
	end if;
	end;
end loop;
close C03;

commit;

end ptu_gerar_lote_fatura;
/