create or replace
procedure pls_sel_tabela_simulacao
		(	nr_seq_simul_perfil_p	number,
			nr_seq_tabela_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 
			
vl_preco_simul_tot_w		number(15,2);
nr_seq_simulacao_w		number(10);
vl_simulacao_perfil_w		number(15,2);

begin

select	max(nr_seq_simulacao)
into	nr_seq_simulacao_w
from	pls_simulacao_perfil
where	nr_sequencia		= nr_seq_simul_perfil_p;

update	pls_simulacao_plano
set	ie_tabela_selecionada	= nr_seq_tabela_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_seq_simul_perfil	= nr_seq_simul_perfil_p;

pls_lancamento_sca_simulacao(nr_seq_simul_perfil_p,2,nm_usuario_p);

pls_atualizar_valor_simul_col(nr_seq_simul_perfil_p,cd_estabelecimento_p,nm_usuario_p,'S');

commit;

end pls_sel_tabela_simulacao;
/
