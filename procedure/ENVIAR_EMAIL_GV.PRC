create or replace
procedure enviar_email_gv(nm_usuario_p	varchar2) is 

ds_assunto_w		varchar2(255);
ds_mensagem_w		varchar2(4000);
nr_seq_gv_w		number;
dt_saida_w		date;
dt_chegada_w		date;
nm_pessoa_viagem_w	varchar2(255);
ds_status_viagem_w	varchar2(40);
cd_pessoa_viagem_w	varchar2(10);
ds_email_origem_w	varchar2(255);
ds_email_destino_w	varchar2(255);

Cursor C01 is
	/* Seleciona todas as pessoas que  est�o com mais de 25 dias em status de viagem */
	select	obter_nome_pf(a.cd_pessoa_fisica),
		a.nr_sequencia,
		trunc(a.dt_saida_prev),
		trunc(a.dt_retorno_prev),
		obter_valor_dominio(1752, a.ie_etapa_viagem),
		nvl(obter_email_pf(a.cd_pessoa_fisica), (select max(ds_email) from usuario b where a.cd_pessoa_fisica = b.cd_pessoa_fisica and b.ie_situacao = 'A'))
	from	via_viagem a
	where	a.ie_etapa_viagem in (0,1,2,3,4,8)
	and	trunc(a.dt_saida_prev) >= to_date('01/01/2018','dd/mm/yyyy');
	
begin

ds_email_origem_w := 'financeiropci@philips.com';
ds_assunto_w := 'Pol�tica de viagens Philips';

open C01;
loop
fetch C01 into	
		nm_pessoa_viagem_w,
		nr_seq_gv_w,
		dt_saida_w,
		dt_chegada_w,
		ds_status_viagem_w,
		ds_email_destino_w;
exit when C01%notfound;
	begin
		/* Montagem do email para encaminhar */
		ds_mensagem_w := 'Prezado ' || nm_pessoa_viagem_w || '. '|| chr(13) || chr(10) || chr(13) || chr(10) || 'Conforme pol�tica de viagens Philips, os relat�rios de viagens precisam ser entregues dentro do prazo de 30 dias ap�s data fim da viagem. A n�o entrega dos relat�rios no prazo ao setor  financeiro afeta o fluxo de caixa da empresa, pois impacta no processo de cobran�a aos clientes bem como poder� afetar o viajante com o n�o reembolso.' || chr(13) || chr(10) || chr(13) || chr(10) || 'Levantamos que voc� possu� pendencias de viagens confomre abaixo. Pedimos que regularize as mesmas dentro do prazo (Voc� ainda possu� 5 dias) . Sendo que entende-se regularizada quando entregue ao Setor Financiero/Projetos.' || chr(13) || chr(10) || chr(13) || chr(10) || 
	'Numero da GV: ' || nr_seq_gv_w || 
	' | Data Da viagem: ' || dt_saida_w || 
	' | Data de Retorno: ' || dt_chegada_w || 
	' | Status: ' || ds_status_viagem_w || '.' || chr(13) || chr(10) || chr(13) || chr(10) ||
	'Obrigado.';

	enviar_email(	ds_assunto_w,
			ds_mensagem_w,
			ds_email_destino_w,
			ds_email_destino_w,
			nm_usuario_p,
			'A',
			'luiz.poltronieri@philips.com;anapaula.luz@philips.com;maisa.schmitt@philips.com;financeiropci@philips.com');	
	EXCEPTION
		WHEN OTHERS THEN
			null;
	end;		
end loop;
close C01;

end enviar_email_gv;
/