create or replace
procedure pls_gerar_trib_venc_prev
			(	nr_seq_vencimento_p		number,
				nm_usuario_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar os valores de tributos previstos (para baixa do t�tulo)
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_variacao_w			varchar2(50);
ie_periodicidade_w		varchar2(50);
ie_cnpj_w			varchar2(50);
cd_cnpj_raiz_w			varchar2(50);
cd_darf_w			varchar2(20);
ie_tipo_tributo_w		varchar2(15);
cd_beneficiario_w		varchar2(14);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
ie_irpf_w			varchar2(3);
ie_apuracao_piso_w		varchar2(3);
ie_tipo_contratacao_w		varchar2(2);
ie_vencimento_w			varchar2(1);
ie_acumulativo_w		varchar2(1);
ie_restringe_estab_w		varchar2(1);
pr_aliquota_w			number(15,4);
vl_tributo_w			number(15,2);
vl_base_calculo_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_base_retido_outro_w		number(15,2);
vl_base_calculo_paga_w		number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_minimo_base_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_base_adic_w			number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_trib_anterior_w		number(15,2);
vl_vencimento_w			number(15,2);
vl_reducao_w			number(15,2);
vl_desc_dependente_w		number(15,2);
vl_total_base_w			number(15,2);
vl_base_pago_adic_base_w	number(15,2);
cd_tributo_w			number(10);
nr_seq_venc_trib_prev_w		number(10);
cd_cond_pagto_w			number(10);
nr_seq_trans_reg_w		number(10);
nr_seq_trans_baixa_w		number(10);
cd_conta_financ_w		number(10);
nr_seq_regra_trib_w		number(10);
nr_seq_classe_w			number(10);
cd_estabelecimento_w		number(5);
cd_tipo_baixa_neg_w		number(5);
cd_empresa_w			number(4);
qt_dependente_w			number(2);
dt_vencimento_w			date;
dt_tributo_w			date;
dt_calculo_w			date;
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;
nr_seq_tipo_prestador_w		pls_tipo_prestador.nr_sequencia%type;
nr_seq_classificacao_w		pls_prestador.nr_seq_classificacao%type;

cursor C01 is
	select	cd_tributo,
		ie_vencimento,
		ie_tipo_tributo,
		ie_apuracao_piso,
		ie_cnpj,
		ie_restringe_estab
	from	tributo	a
	where	ie_conta_pagar	= 'S'
	and	ie_situacao	= 'A'
	and	ie_baixa_titulo	= 'S'
	and	((ie_pf_pj = 'A') or
		((ie_pf_pj = 'PF') and (cd_pessoa_fisica_w is not null)) or
		((ie_pf_pj = 'PJ') and (cd_cgc_w is not null)))
	order	by decode(ie_tipo_tributo, 'INSS', 1, 2);

begin

select	a.vl_vencimento,
	a.vl_vencimento,
	e.cd_empresa,
	d.cd_pessoa_fisica,
	d.cd_cgc,
	obter_cnpj_raiz(d.cd_cgc),
	nvl(c.dt_mes_competencia, a.dt_vencimento),
	c.cd_estabelecimento,
	nvl(c.dt_ref_tributo, c.dt_mes_competencia),
	d.nr_seq_tipo_prestador,
	d.nr_seq_classificacao
into	vl_vencimento_w,
	vl_base_calculo_w,
	cd_empresa_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	cd_cnpj_raiz_w,
	dt_tributo_w,
	cd_estabelecimento_w,
	dt_vencimento_w,
	nr_seq_tipo_prestador_w,
	nr_seq_classificacao_w
from	estabelecimento 		e,
	pls_prestador			d,
	pls_lote_pagamento		c,
	pls_pagamento_prestador		b,
	pls_pag_prest_vencimento	a
where	c.cd_estabelecimento	= e.cd_estabelecimento
and	a.nr_seq_pag_prestador	= b.nr_sequencia
and	b.nr_seq_prestador	= d.nr_sequencia
and	b.nr_seq_lote		= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_vencimento_p;

if	(nvl(vl_base_calculo_w,0) > 0) then
	open C01;
	loop
	fetch C01 into 
		cd_tributo_w,
		ie_vencimento_w,
		ie_tipo_tributo_w,
		ie_apuracao_piso_w,
		ie_cnpj_w,
		ie_restringe_estab_w;
	exit when c01%notfound;
		begin
		vl_tributo_w := 0;
		
		obter_dados_trib_tit_pagar	
			(cd_tributo_w,
			cd_estabelecimento_w,
			cd_cgc_w,
			cd_pessoa_fisica_w,
			cd_beneficiario_w,
			pr_aliquota_w,
			cd_cond_pagto_w,
			cd_conta_financ_w,
			nr_seq_trans_reg_w,
			nr_seq_trans_baixa_w,
			vl_minimo_base_w,
			vl_minimo_tributo_w,
			ie_acumulativo_w,
			vl_teto_base_w,
			vl_desc_dependente_w,
			cd_darf_w,
			dt_calculo_w,
			cd_variacao_w,
			ie_periodicidade_w,
			null,
			null,
			null,
			null,
			ie_tipo_contratacao_w,
			null,
			nr_seq_regra_trib_w,
			null,
			0,
			nr_seq_classe_w,
			cd_tipo_baixa_neg_w,
			vl_base_calculo_w,
			'S',
			null,
			null,
			nr_seq_tipo_prestador_w,
			nr_seq_classificacao_w);
					
		if	(nvl(pr_aliquota_w,0) > 0) then
		
			pls_pag_prod_obter_val_trib(	ie_apuracao_piso_w,
							ie_cnpj_w,
							cd_pessoa_fisica_w,
							cd_cgc_w,
							cd_cnpj_raiz_w,
							ie_restringe_estab_w,
							cd_empresa_w,
							cd_tributo_w,
							dt_tributo_w,
							vl_soma_trib_nao_retido_w,
							vl_soma_base_nao_retido_w,
							vl_soma_trib_adic_w,
							vl_soma_base_adic_w,
							vl_trib_anterior_w,
							vl_total_base_w,
							vl_reducao_w,
							cd_estabelecimento_w,
							nm_usuario_p);
			
			obter_valores_tributo(	ie_acumulativo_w,
						pr_aliquota_w,
						vl_minimo_base_w,
						vl_minimo_tributo_w,
						vl_soma_trib_nao_retido_w,
						vl_soma_trib_adic_w,
						vl_soma_base_nao_retido_w,
						vl_soma_base_adic_w,
						vl_base_calculo_w,
						vl_tributo_w,
						vl_trib_nao_retido_w,
						vl_trib_adic_w,
						vl_base_nao_retido_w,
						vl_base_adic_w,
						vl_teto_base_w,
						vl_trib_anterior_w,
						ie_irpf_w,
						vl_total_base_w,
						vl_reducao_w,
						vl_desc_dependente_w,
						qt_dependente_w,
						vl_base_calculo_paga_w,
						vl_base_pago_adic_base_w,
						vl_base_retido_outro_w,
						obter_outras_reducoes_irpf(cd_pessoa_fisica_w, cd_estabelecimento_w, dt_tributo_w),
						dt_calculo_w,
						nr_seq_regra_irpf_w);
						
			if	(vl_tributo_w <> 0) then
				select	pls_pag_venc_trib_prev_seq.nextval
				into	nr_seq_venc_trib_prev_w
				from	dual;
				
				insert into pls_pag_venc_trib_prev
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_vencimento,
					cd_tributo,
					dt_imposto,
					vl_base_adic,
					vl_base_calculo,
					vl_base_nao_retido,
					pr_tributo,
					vl_desc_base,
					vl_imposto,
					vl_trib_adic,
					vl_reducao,
					vl_nao_retido)
				values	(nr_seq_venc_trib_prev_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_vencimento_p,
					cd_tributo_w,
					dt_vencimento_w,
					vl_base_adic_w,
					vl_base_calculo_w,
					vl_base_nao_retido_w,
					pr_aliquota_w,
					vl_desc_dependente_w,
					vl_tributo_w,
					vl_trib_adic_w,
					vl_reducao_w,
					vl_trib_nao_retido_w);
			end if;
		end if;
				
		end;
	end loop;
	close C01;
end if;

end pls_gerar_trib_venc_prev;
/