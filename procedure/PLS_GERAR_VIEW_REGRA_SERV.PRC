create or replace 
procedure pls_gerar_view_regra_serv
			(	ie_tipo_regra_p		Varchar2,
				ie_tipo_tabela_p	Varchar2) is

ds_comando_w			varchar2(32000);
ds_tipo_tabela_w		varchar2(10);
nm_atributo_w			varchar2(255);
qt_regra_w			number(10);
ie_ordem_classificacao_w	varchar2(1);
ds_compl_order_w		varchar2(100);
ie_tipo_atributo_w  tabela_atributo.ie_tipo_atributo%type;
/*	Ie_tipo_tabela tem referencia da tabela se e prestador, co-participacao, intercambio etc
	ie_tipo_regra tem referencia no tipo de item se e material, procedimento ou servico*/
Cursor C01 is
	select	nm_atributo,
		nvl(ie_ordem_classificacao,'P')
	from	pls_regra_preco_ordem_serv
	where	ie_tipo_preco = ie_tipo_regra_p
	and	ie_tipo_tabela = ie_tipo_tabela_p
	order by nr_seq_apresentacao;
Cursor C02 is
	select	a.nm_atributo,
		a.ie_tipo_atributo
	from	tabela_atributo	a
	where	(a.nm_tabela) = 'PLS_REGRA_PRECO_SERVICO'
	and	a.NR_SEQ_APRESENT is not null
	and	not exists	(	select	1
					from	pls_regra_preco_ordem_serv	x
					where	(x.nm_atributo) 	= (a.nm_atributo)
					and	x.ie_tipo_preco		= ie_tipo_regra_p
					and	x.ie_tipo_tabela	= ie_tipo_tabela_p)
	and	a.nr_seq_apresent < (	select 	max(y.nr_seq_apresent)
					from	tabela_atributo y
					where	y.nm_tabela	= 'PLS_REGRA_PRECO_SERVICO'
					and	(y.nm_atributo) = ('DS_ENTAO'))
	and	ie_tipo_atributo not in ('FUNCTION', 'VISUAL')
	and	a.nm_atributo <> ('NR_SEQUENCIA')
	order by a.nr_seq_apresent desc;
begin


ds_tipo_tabela_w	:='prest';
if	(ie_tipo_tabela_p 	= 'P')then
	ds_tipo_tabela_w	:='prest';
elsif	((ie_tipo_tabela_p	= 'IC') or (ie_tipo_tabela_p	= 'IP') or (ie_tipo_tabela_p	= 'I')) then
	ds_tipo_tabela_w	:= 'interc';
elsif	(ie_tipo_tabela_p	= 'CO') then
	ds_tipo_tabela_w	:= 'copart';
elsif	(ie_tipo_tabela_p	= 'R') then
	ds_tipo_tabela_w	:= 'reemb';
elsif	(ie_tipo_tabela_p	= 'O') then
	ds_tipo_tabela_w	:= 'oper';
elsif	(ie_tipo_tabela_p	= 'CP') then
	ds_tipo_tabela_w	:= 'CP';
end if;

if	((ie_tipo_tabela_p = 'IC') or (ie_tipo_tabela_p = 'IP') or (ie_tipo_tabela_p = 'I')) then
	ds_comando_w	:=	'create or replace view pls_regra_preco_serv_'||ds_tipo_tabela_w||'_v as ' || chr(10) || chr(13) ||
			'	select	a.* ' || chr(10) || chr(13) ||
			'	from	pls_regra_preco_servico a ' || chr(10) || chr(13) ||
			'	where	a.ie_situacao = ' || chr(39) || 'A' || chr(39) ||
			'	and	a.ie_tipo_tabela	 in(''I'',''IC'',''IP'')';


else
	ds_comando_w	:=	'create or replace view pls_regra_preco_serv_'||ds_tipo_tabela_w||'_v as ' || chr(10) || chr(13) ||
			'	select	a.* ' || chr(10) || chr(13) ||
			'	from	pls_regra_preco_servico a ' || chr(10) || chr(13) ||
			'	where	a.ie_situacao = ' || chr(39) || 'A' || chr(39) ||
			'	and	a.ie_tipo_tabela	 = ' || chr(39) || ie_tipo_tabela_p || chr(39);
end if;


/* Verificar se existe regra para ordenacao do tipo de regra selecionado (ie_tipo_regra_p) */
select	count(1)
into	qt_regra_w
from	pls_regra_preco_ordem_serv
where	ie_tipo_preco = ie_tipo_regra_p
and	ie_tipo_tabela = ie_tipo_tabela_p
and	rownum = 1;

if	(qt_regra_w > 0) then
	ds_comando_w	:= ds_comando_w || chr(13) || chr(10) || ' order by ';
	/* Concatenar os campos para ordenacao dos registros da view */
	open C01;
	loop
	fetch C01 into
		nm_atributo_w,
		ie_ordem_classificacao_w;
	exit when C01%notfound;
		begin
		
		if (ie_ordem_classificacao_w = 'C') then
			ds_compl_order_w	:= ' Asc , ';
		elsif (ie_ordem_classificacao_w = 'D') then
			ds_compl_order_w	:= ' Desc , ';
		else
			ds_compl_order_w	:= ' , ';
		end if;

		select  max(ie_tipo_atributo)
		into    ie_tipo_atributo_w
		from    tabela_atributo
		where   nm_tabela      = 'PLS_REGRA_PRECO_SERVICO'
		and     nm_atributo    = nm_atributo_w;

		if	(substr(nm_atributo_w,1,2) = 'DT') then
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) || nm_atributo_w || ds_compl_order_w ;
		elsif	(nm_atributo_w		= 'NR_SEQUENCIA') then
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) || nm_atributo_w || ds_compl_order_w;
		elsif (ie_tipo_atributo_w       = 'NUMBER') then
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) ||'decode( '|| nm_atributo_w || ',null,0,1)' || ds_compl_order_w;
		else
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) ||'decode( '|| nm_atributo_w || ',null,''A'',''A'',''A'',''S'',''C'',''B'' )' || ds_compl_order_w;
		end if;
		end;
	end loop;
	close C01;

	/*Concatena os atributos que nao estao  na tabela pls_regra_preco_ordem para que a order by fique correto*/
	open C02;
	loop
	fetch C02 into
		nm_atributo_w, 
		ie_tipo_atributo_w;
	exit when C02%notfound;
		begin
		if	(substr(nm_atributo_w,1,2) = 'DT') then
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) || nm_atributo_w || ' , ';
		elsif (ie_tipo_atributo_w       = 'NUMBER') then
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) ||'decode( '|| nm_atributo_w || ',null,0,1) , ';
		else
			ds_comando_w	:= ds_comando_w || chr(10) || chr(13) ||'decode( '|| nm_atributo_w || ',null,''A'',''A'',''A'',''S'',''C'',''B'' ) , ';
		end if;

		end;
	end loop;
	close C02;
	/* Realizado o substr para retirar a virgula do final do comando */
end if;

exec_sql_dinamico('tasy', substr(ds_comando_w,1,length(ds_comando_w)-2)); 

end pls_gerar_view_regra_serv;
/
