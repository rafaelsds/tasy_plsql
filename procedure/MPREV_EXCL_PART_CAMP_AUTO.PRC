create or replace
procedure mprev_excl_part_camp_auto( 	nr_seq_campanha_p	number,
					dt_termino_p		date,					
					nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Ao informar data de t�rmino na campanha, preencher automaticamente a data de exclus�o 
dos participantes desta campanha
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: HDM - Controle de Participantes
[  ]  Objetos do dicion�rio [  x] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

if	(nr_seq_campanha_p is not null) then

	update	mprev_campanha_partic
	set	dt_exclusao = dt_termino_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_seq_campanha = nr_seq_campanha_p
	and	dt_exclusao is null;
		
end if;

commit;

end mprev_excl_part_camp_auto;
/
