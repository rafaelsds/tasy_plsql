create or replace
procedure ativia_gera_cobr_safra_400_msg (	nr_seq_cobr_escrit_p	number,
											cd_estabelecimento_p	number,
											nr_seq_grupo_inst_p		number,
											nm_usuario_p			Varchar2 ) is 

nr_titulo_w					titulo_receber.nr_titulo%type;	
nr_seq_mensalidade_w		titulo_receber.nr_seq_mensalidade%type;
cd_cgc_w					pls_contrato_pagador.cd_cgc%type;
ie_aplicacao_mensagem_w		banco_instrucao_boleto.ie_aplicacao_mensagem%type;
ie_posicao_w				banco_instrucao_boleto.ie_posicao%type;
nr_linha_anterior_w			banco_instrucao_boleto.nr_linha%type;
qt_msg_um_w					number(10);
qt_msg_dois_w				number(10);	
nr_linha_w					banco_instrucao_boleto.nr_linha%type;
nr_seq_grupo_inst_w			number(10);	
cd_banco_w					banco_estabelecimento.cd_banco%type;	
ds_mensagem_concat_w		varchar2(4000)	:= '';
cont_cursor_w				number(10)	:= 0;	
qt_reg_w					number(10);	
nr_seq_conta_proc_w			pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w			pls_conta_proc.nr_sequencia%type;	
ds_mensagem_w				varchar2(126);		
ds_mensagem_um_w			varchar2(126);
ds_mensagem_dois_w			varchar2(126);
ds_mensagem_tres_w			varchar2(127);
nr_seq_reg_arquivo_w		number(10)	:= 1;
nr_remessa_w				cobranca_escritural.nr_remessa%type;
ds_conteudo_w				varchar2(400);
cd_cedente_w				banco_estabelecimento.cd_cedente%type;
nm_empresa_w				varchar2(30);
dt_geracao_w				varchar2(6);
ie_mensagem_w				varchar2(1);
nr_seq_segurado_w			number(15);	
vl_outros_n_w				number(15,2);
vl_outros_w					varchar2(255)	:= null;
vl_total_w					varchar2(255)	:= null;
nr_seq_mensalidade_seg_w	pls_mensalidade_segurado.nr_sequencia%type;
vl_item_w					pls_mensalidade_seg_item.vl_item%type;	
nr_seq_plano_w				pls_segurado.nr_seq_plano%type;
ds_plano_w					pls_plano.ds_plano%type;	
nr_protocolo_ans_w			pls_plano.nr_protocolo_ans%type;
nr_count_w					number(10);
vl_mensalidade_w			varchar2(255);
vl_coparticipacao_w			varchar2(255);
nr_seq_segurado_mens_w		pls_mensalidade_segurado.nr_seq_segurado%type;
cd_usuario_plano_w			pls_segurado_carteira.cd_usuario_plano%type;
dt_contratacao_w			varchar2(10);
qt_coparticipacao_w			number(10);
				
											
/*Cursor para buscar os t�tulos da cobran�a que ter�o as msg gerada.
Deixei as clausulas where dessa forma e com todas as liga��es conforme ocorre
na rotina ativia_gerar_cobr_safra_240 que gera o arquivo para o banco, para 
garantir que ser�o obtido os mesmos t�tulos*/											
cursor C01 is
	select	b.nr_titulo,
			b.nr_seq_mensalidade
	from	titulo_receber_v 		b,
			titulo_receber_cobr 	c,
			cobranca_escritural 	a,
			banco_carteira 			d,
			banco_estabelecimento 	e,
			pls_contrato_pagador	f,
			pls_mensalidade			k
	where	a.nr_sequencia			= c.nr_seq_cobranca
	and		c.nr_titulo				= b.nr_titulo
	and		a.nr_seq_conta_banco	= e.nr_sequencia
	and		b.nr_seq_mensalidade	= k.nr_sequencia(+)
	and		k.nr_seq_pagador		= f.nr_sequencia(+)
	and		a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
	and		a.nr_sequencia			= nr_seq_cobr_escrit_p;	

cursor	C02 is
	select	distinct nvl(a.nr_linha,0) nr_linha
	from	banco_instrucao_boleto a
	where	nvl(a.ie_posicao,'F')				= nvl(ie_posicao_w,'X')
	and		nvl(a.ie_aplicacao_mensagem,'T')	= nvl(ie_aplicacao_mensagem_w,'X')
	and		a.cd_banco							= nvl(cd_banco_w,0)
	and		nvl(a.nr_seq_grupo,0)				= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0))
	order by	1;

cursor	C03 is
	select	substr(obter_instrucao_boleto_tam(nr_titulo_w,cd_banco_w,a.nr_linha,ie_posicao_w,nr_seq_conta_proc_w,nr_seq_conta_mat_w,a.nr_sequencia),1,126) ds_mensagem
	from	banco_instrucao_boleto a
	where	nvl(a.ie_posicao,'F')				= ie_posicao_w
	and		nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
	and		a.cd_banco							= cd_banco_w
	and		nvl(a.nr_linha,0)					= nvl(nr_linha_w,0)
	and		nvl(a.nr_seq_grupo,0)				= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0))
	order by	a.nr_linha;	
	
cursor	C04 is
	select	f.nr_sequencia nr_seq_conta_proc,
			null nr_seq_conta_mat
	from	pls_conta_proc f,
			pls_conta e,
			pls_mensalidade_seg_item 	a,
			pls_mensalidade_segurado	b,
			pls_mensalidade 		c,
			titulo_receber d
	where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
	and		c.nr_sequencia	= b.nr_seq_mensalidade
	and		e.nr_sequencia	= a.nr_seq_conta
	and		e.nr_sequencia	= f.nr_seq_conta
	and		a.nr_seq_conta	is not null
	and		c.nr_sequencia	= d.nr_seq_mensalidade
	and		d.nr_titulo		= nr_titulo_w
	union all
	select	null nr_seq_conta_proc,
			f.nr_sequencia nr_seq_conta_mat
	from	pls_conta_mat f,
			pls_conta e,
			pls_mensalidade_seg_item 	a,
			pls_mensalidade_segurado 	b,
			pls_mensalidade 		c,
			titulo_receber 		d
	where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
	and		c.nr_sequencia	= b.nr_seq_mensalidade
	and		e.nr_sequencia	= a.nr_seq_conta
	and		e.nr_sequencia	= f.nr_seq_conta
	and		a.nr_seq_conta	is not null
	and		c.nr_sequencia	= d.nr_seq_mensalidade
	and		d.nr_titulo		= nr_titulo_w;

Cursor C05 is
	select	campo_mascara_virgula(nvl(a.vl_mensalidade - a.vl_coparticipacao - a.vl_outros,0)) vl_mensalidade,
			campo_mascara_virgula(nvl(a.vl_coparticipacao,0)) vl_coparticipacao,
			a.nr_seq_segurado
	from	pls_mensalidade_segurado 	a
	where	a.nr_seq_mensalidade 	= nr_seq_mensalidade_w
	and	rownum 			<= 9
	order by a.nr_sequencia;	
			
											
begin

delete 
from 	w_envio_banco 
where 	nm_usuario = nm_usuario_p;

if	(nvl(nr_seq_grupo_inst_p,0)	= 0) then
	nr_seq_grupo_inst_w	:= null;
else
	nr_seq_grupo_inst_w	:= nr_seq_grupo_inst_p;
end if;

select	max(b.cd_banco),
		nvl(max(nr_remessa_w),0),
		nvl(max(cd_cedente),0),
		rpad(upper(substr(obter_razao_social(max(c.cd_cgc)),1,30)),30,' ') nm_empresa,
		to_char(sysdate,'DDMMYY')
into	cd_banco_w,
		nr_remessa_w,
		cd_cedente_w,
		nm_empresa_w,
		dt_geracao_w
from	banco_estabelecimento b,
		cobranca_escritural a,
		estabelecimento c
where	a.nr_seq_conta_banco	= b.nr_sequencia
and		a.cd_estabelecimento	= c.cd_estabelecimento
and		a.nr_sequencia			= nr_seq_cobr_escrit_p;

/*Inicio header do arquivo de mensagens*/
ds_conteudo_w := 	'0'								|| /*Pos 01*/
					'1'								|| /*Pos 02*/
					'REMESSA'						|| /*Pos 03 a 09*/
					'01'							|| /*Pos 10 a 11*/
					'Cobranca'						|| /*Pos 12 a 19*/
					lpad(' ',7, ' ')				|| /*Pos 20 a 26*/
					lpad(nvl(cd_cedente_w,0),14,'0') || /*Pos 27 a 40*/
					lpad(' ',6, ' ')				|| /*Pos 41 a 46*/
					rpad(nvl(nm_empresa_w,' '),30,' ')|| /*Pos 47 a 76*/
					'422'							|| /*Pos 77 a 79*/
					'BANCO SAFRA'					|| /*Pos 80 a 90*/
					lpad(' ',4,' ')					|| /*Pos 91 a 94*/
					lpad(dt_geracao_w,6,'0')		|| /*Pos 95 a 100*/
					lpad(' ',291,' ')				|| /*Pos 101 a 391*/
					lpad(nr_remessa_w,3,'0')		|| /*Pos 392 a 394*/	
					lpad(nr_seq_reg_arquivo_w,6,'0');/*Pos 395 a 400*/
						
insert	into w_envio_banco ( nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
				values	  ( w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_reg_arquivo_w);	
/*Fim header do arquivo de mensagens*/

open C01;
loop
fetch C01 into	
	nr_titulo_w,
	nr_seq_mensalidade_w;
exit when C01%notfound;
	begin
	
	/* Registro mensagem frente (obrigat�rio) */
	ie_aplicacao_mensagem_w	:= 'T';
	ie_posicao_w			:= 'F';
	nr_linha_anterior_w		:= null;
	qt_msg_um_w				:= 0;
	qt_msg_dois_w			:= 0;
	cd_cgc_w 				:= null;
	
	/*A tratativa de msg ocorre conforme a rotina  ATIVIA_GERAR_COBR_ITAU_400*/
	if	(nr_seq_mensalidade_w is not null) then	
	
		select	max(b.cd_cgc)
		into	cd_cgc_w
		from 	pls_mensalidade a,
				pls_contrato_pagador b,
				pls_contrato c
		where	a.nr_seq_pagador	= b.nr_sequencia
		and   	b.nr_seq_contrato	= c.nr_sequencia
		and		a.nr_sequencia		= nr_seq_mensalidade_w;	
		
	end if;
		
	if (cd_cgc_w is null) then
	
		open C02;
		loop
		fetch C02 into	
			nr_linha_w;
		exit when C02%notfound;
			begin
			
			ds_mensagem_concat_w	:= '';
			cont_cursor_w			:= 0;
			
			select	count(*)
			into	qt_reg_w
			from	banco_instrucao_boleto a
			where	nvl(a.ie_posicao,'F')				= ie_posicao_w
			and		nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
			and		a.cd_banco							= cd_banco_w
			and		a.nr_linha							= nr_linha_w
			and		nvl(a.nr_seq_grupo,0)				= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0));
			
			open C03;
			loop
			fetch C03 into	
				ds_mensagem_w;
			exit when C03%notfound;
				begin
				
				cont_cursor_w	:= nvl(cont_cursor_w,0) + 1;
				ds_mensagem_concat_w	:= ds_mensagem_concat_w || ds_mensagem_w;
				
				if	(ds_mensagem_concat_w is not null) or (ds_mensagem_concat_w <> '') then
					
					if	((ds_mensagem_um_w is null) or (nvl(nr_linha_anterior_w,nr_linha_w)	= nr_linha_w)) and (qt_msg_um_w	= 0) then
					
						ds_mensagem_um_w		:= substr(ds_mensagem_concat_w,1,126);
						
						if	(cont_cursor_w 	= qt_reg_w) and (ds_mensagem_um_w is not null) then
							qt_msg_um_w	:= 1;
						end if;	
						
					elsif	((ds_mensagem_dois_w is null) or (nvl(nr_linha_anterior_w,nr_linha_w) = nr_linha_w)) and (qt_msg_dois_w	= 0) then
						
						ds_mensagem_dois_w		:= substr(ds_mensagem_concat_w,1,126);
						
						if	(cont_cursor_w	= qt_reg_w) and (ds_mensagem_dois_w	is not null) then
							qt_msg_dois_w	:= 1;
						end if;	
					else
					
						ds_mensagem_tres_w	:= substr(ds_mensagem_concat_w,1,127);
						
					end if;

				end if;
				
				if	(ds_mensagem_um_w is not null) and (ds_mensagem_dois_w is not null) and ((ds_mensagem_tres_w is not null) and (cont_cursor_w = qt_reg_w)) then
					 
					nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
					ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
										'0' 										|| /*Pos 02*/
										lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
										rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
										rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
										rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
										lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
										lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/

					insert	into w_envio_banco ( nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												cd_estabelecimento,
												ds_conteudo,
												nr_seq_apres)
									values	  ( w_envio_banco_seq.nextval,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p,
												cd_estabelecimento_p,
												ds_conteudo_w,
												nr_seq_reg_arquivo_w);

					ds_mensagem_um_w	:= null;
					ds_mensagem_dois_w	:= null;
					ds_mensagem_tres_w	:= null;
					qt_msg_um_w			:= 0;
					qt_msg_dois_w		:= 0;
					
				end if;
				
			nr_linha_anterior_w	:= nr_linha_w;	
				
				end;
			end loop;
			close C03;
			
			end;
		end loop;
		close C02;
		
		/* Tratar as sobras */
		if	(ds_mensagem_um_w is not null) then

			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);
			
			ds_mensagem_um_w	:= null;
			ds_mensagem_dois_w	:= null;
			ds_mensagem_tres_w	:= null;
	
		end if;
		
		ie_aplicacao_mensagem_w	:= 'C';
		qt_msg_um_w				:= 0;
		qt_msg_dois_w			:= 0;
		ie_mensagem_w			:= 'N';
		
		open C04;
		loop
		fetch C04 into	
			nr_seq_conta_proc_w,
			nr_seq_conta_mat_w;
		exit when C04%notfound;
			begin
			
			nr_linha_anterior_w	:= null;	
			/* Registro mensagem frente (obrigat�rio) */
			
			open C02;
			loop
			fetch C02 into	
				nr_linha_w;
			exit when C02%notfound;
				begin
				
				ds_mensagem_concat_w	:= '';
				cont_cursor_w			:= 0;

				select	count(*)
				into	qt_reg_w
				from	banco_instrucao_boleto a
				where	nvl(a.ie_posicao,'F')				= ie_posicao_w
				and		nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
				and		a.cd_banco							= cd_banco_w
				and		a.nr_linha							= nr_linha_w
				and		nvl(a.nr_seq_grupo,0)				= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0));				
				
				open C03;
				loop
				fetch C03 into	
					ds_mensagem_w;
				exit when C03%notfound;
					begin
					
					cont_cursor_w	:= cont_cursor_w + 1;
					ds_mensagem_concat_w	:= ds_mensagem_concat_w || ds_mensagem_w;
					
					if	(ds_mensagem_concat_w is not null) or (ds_mensagem_concat_w <> '') then
					
						if	(ds_mensagem_um_w is null) or (qt_msg_um_w = 0) then
						
							ds_mensagem_um_w		:= substr(ds_mensagem_concat_w,1,126);
							
							if	(cont_cursor_w = qt_reg_w) and (ds_mensagem_um_w is not null) then
							
								qt_msg_um_w	:= 1;
								
							end if;	
							
						elsif	(ds_mensagem_dois_w	is null) or (qt_msg_dois_w = 0) then
						
							ds_mensagem_dois_w		:= substr(ds_mensagem_concat_w,1,126);
							
							if	(cont_cursor_w = qt_reg_w) and(ds_mensagem_dois_w is not null) then
							
								qt_msg_dois_w	:= 1;
								
							end if;	
							
						else
						
							ds_mensagem_tres_w	:= substr(ds_mensagem_concat_w,1,127);

						end if;

					end if;
					
					if	(ds_mensagem_um_w is not null) and (ds_mensagem_dois_w is not null) and ((ds_mensagem_tres_w is not null) and (cont_cursor_w = qt_reg_w)) then
						
						
						nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
						ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
											'0' 										|| /*Pos 02*/
											lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
											rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
											rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
											rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
											lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
											lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
						insert	into w_envio_banco ( nr_sequencia,
													dt_atualizacao,
													nm_usuario,
													dt_atualizacao_nrec,
													nm_usuario_nrec,
													cd_estabelecimento,
													ds_conteudo,
													nr_seq_apres)
										values	  ( w_envio_banco_seq.nextval,
													sysdate,
													nm_usuario_p,
													sysdate,
													nm_usuario_p,
													cd_estabelecimento_p,
													ds_conteudo_w,
													nr_seq_reg_arquivo_w);						
						
						ie_mensagem_w		:= 'S';
						ds_mensagem_um_w	:= null;
						ds_mensagem_dois_w	:= null;
						ds_mensagem_tres_w	:= null;
						qt_msg_um_w			:= 0;
						qt_msg_dois_w		:= 0;	
						
					end if;
				
					end;
				end loop;
				close C03;
				
				end;
			end loop;
			close C02;
			
			end;
		end loop;
		close C04;
		
		
		/* Tratar as sobras */
		if	(ds_mensagem_um_w is not null) then
		
			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);
				
			ie_mensagem_w			:= 'S';
			ds_mensagem_um_w	:= null;
			ds_mensagem_dois_w	:= null;
			ds_mensagem_tres_w	:= null;
	
		end if;
		
		
		if (ie_mensagem_w = 'N') and (ds_mensagem_um_w is not null) then
		
			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);
		end if;	
		
		if	(nr_seq_mensalidade_w is not null) then
		
			select	pls_obter_segurado_pagador(a.nr_seq_pagador)
			into	nr_seq_segurado_w
			from	pls_mensalidade	a
			where	a.nr_sequencia 	= nr_seq_mensalidade_w;
			
			select	sum(nvl(a.vl_outros,0)) vl_outros,
					campo_mascara_virgula(nvl(sum(a.vl_mensalidade),0)) vl_total_mens,
					max(a.nr_sequencia)
			into	vl_outros_n_w,
					vl_total_w,
					nr_seq_mensalidade_seg_w
			from  	pls_mensalidade_segurado 	a
			where 	a.nr_seq_mensalidade	= nr_seq_mensalidade_w
			and  	rownum 					<= 9
			order by 	a.nr_sequencia;
			
			select	sum(nvl(a.vl_item,0))
			into	vl_item_w
			from	pls_mensalidade_seg_item 	a,
					pls_mensalidade_segurado 	b
			where	a.nr_seq_mensalidade_seg	= b.nr_sequencia
			and		b.nr_seq_mensalidade   		= nr_seq_mensalidade_w
			and		a.ie_tipo_item				= 4
			and  	rownum 						<= 9
			order by 	a.nr_sequencia;

			if	(nvl(vl_item_w,0) <= nvl(vl_outros_n_w,0)) then
			
				vl_outros_w := campo_mascara_virgula(nvl(vl_outros_n_w,0) - nvl(vl_item_w,0));
				
			end if;		
			
		end if;	
		
		if	(nr_seq_segurado_w is not null) then
		
			select	max(a.nr_seq_plano)
			into	nr_seq_plano_w
			from	pls_segurado_carteira	b,
					pls_segurado		a
			where	a.nr_sequencia	= b.nr_seq_segurado
			and		a.nr_sequencia 	= nr_seq_segurado_w;
			
			if	(nr_seq_plano_w is not null) then
			
				select	max(ds_plano),
						max(nr_protocolo_ans)
				into	ds_plano_w,
						nr_protocolo_ans_w
				from	pls_plano
				where	nr_sequencia	= nr_seq_plano_w;
				
			end if;	
			
		end if;
		
		ds_mensagem_concat_w :=	rpad('Numero Cartao Id.    Inicio     Reg. Prod.    Mensal.      Co-Part.      Qt.CP  ',128,' ');
		
		cont_cursor_w := 0;
		nr_count_w := 1;

		select	count(*)
		into    cont_cursor_w
		from	pls_mensalidade_segurado 	a
		where	a.nr_seq_mensalidade 	= nr_seq_mensalidade_w
		and		rownum 					<= 9
		order by 	a.nr_sequencia;	
		
		cont_cursor_w := cont_cursor_w + 1;	
		
		open C05;
		loop
		fetch C05 into	
			vl_mensalidade_w,
			vl_coparticipacao_w,
			nr_seq_segurado_mens_w;
		exit when C05%notfound;
			begin
			
			select	max(b.cd_usuario_plano),
					to_char(max(a.dt_contratacao),'dd/mm/yyyy')
			into	cd_usuario_plano_w,
					dt_contratacao_w
			from	pls_segurado_carteira	b,
					pls_segurado		a
			where	a.nr_sequencia		= b.nr_seq_segurado
			and		a.nr_sequencia 		= nr_seq_segurado_mens_w;
			
			select	count(1)
			into	qt_coparticipacao_w
			from	pls_mensalidade_seg_item	b,
					pls_mensalidade_segurado	a
			where	b.nr_seq_mensalidade_seg	= a.nr_sequencia
			and		a.nr_seq_mensalidade		= nr_seq_mensalidade_w
			and		a.nr_seq_segurado			= nr_seq_segurado_mens_w
			and		b.ie_tipo_item 				= 3;
			
			ds_mensagem_concat_w := ds_mensagem_concat_w 				 	|| 
									rpad(rpad(cd_usuario_plano_w,21,' ') 	|| 
									rpad(dt_contratacao_w,11,' ') 			|| 
									rpad(nr_protocolo_ans_w,14,' ') 		||
									rpad(vl_mensalidade_w,14,' ') 			|| 
									rpad(vl_coparticipacao_w,14,' ') 		|| 
									rpad(qt_coparticipacao_w,11,' '),128,' ');	

			nr_count_w := nr_count_w + 1;

			
			if (nr_count_w mod 3 = 0) or (nr_count_w = cont_cursor_w) then
			
			
				nr_seq_reg_arquivo_w := nr_seq_reg_arquivo_w + 1;
				ds_conteudo_w	:=	'2' 															|| /*Pos 01*/
									'0' 															|| /*Pos 02*/
									lpad(nr_titulo_w,10,'0')										|| /*Pos 03 a 12*/
									rpad(nvl(substr(ds_mensagem_concat_w,1,126),' '),126,' ') 		|| /*Pos 13 a 138*/
									rpad(nvl(substr(ds_mensagem_concat_w,127,126),' '),126,' ') 	|| /*Pos 139 a 264*/
									rpad(nvl(substr(ds_mensagem_concat_w,254,127),' '),127,' ') 	|| /*Pos 265 a a 391*/
									lpad(nr_remessa_w,3,'0')										|| /*Pos 392 a 394*/		
									lpad(nr_seq_reg_arquivo_w,6,'0');			   						/*Pos 395 a 400*/
				
				insert	into w_envio_banco ( nr_sequencia,
											dt_atualizacao,
											nm_usuario,
											dt_atualizacao_nrec,
											nm_usuario_nrec,
											cd_estabelecimento,
											ds_conteudo,
											nr_seq_apres)
								values	  ( w_envio_banco_seq.nextval,
											sysdate,
											nm_usuario_p,
											sysdate,
											nm_usuario_p,
											cd_estabelecimento_p,
											ds_conteudo_w,
											nr_seq_reg_arquivo_w);			
				
				ds_mensagem_um_w	:= null;
				ds_mensagem_dois_w	:= null;
				ds_mensagem_tres_w	:= null;								
				
			end if;		
			
			end;
		end loop;
		close C05;

		ds_mensagem_um_w	:= 	rpad(' ',126,' ');		
		ds_mensagem_dois_w	:= 	rpad(rpad('Outros eventos:',20,' ')	|| nvl(vl_outros_w, '0,00'),64,' ') || rpad(nvl(Elimina_Acentos(pls_obter_mensagem_reajuste(nr_seq_mensalidade_w,3,'RA'),'S'), '0,00'),42,' ');
		ds_mensagem_tres_w	:=	rpad('Totalizacao:',20,' ') 		|| nvl(vl_total_w,' 0,00');			
		
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
							'0' 										|| /*Pos 02*/
							lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
							rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
							rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
							rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
							lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
							lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
		
		insert	into w_envio_banco ( nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									cd_estabelecimento,
									ds_conteudo,
									nr_seq_apres)
						values	  ( w_envio_banco_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									cd_estabelecimento_p,
									ds_conteudo_w,
									nr_seq_reg_arquivo_w);
									
		ds_mensagem_um_w	:= null;
		ds_mensagem_dois_w	:= null;
		ds_mensagem_tres_w	:= null;

	else

		nr_linha_w := 1; --@NM_CNPJ_CPF
		
		open C03;
		loop
		fetch C03 into	
			ds_mensagem_w;
		exit when C03%notfound;
			begin
			
			ds_mensagem_um_w	:= ds_mensagem_w;
			ds_mensagem_dois_w 	:= null;
			ds_mensagem_tres_w 	:= null;
			
			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);
										
			ds_mensagem_um_w	:= null;							
			
			end;
		end loop;
		close C03;
		
		nr_linha_w := 7; --@DS_ENDERECO_COMPL
		
		open C03;
		loop
		fetch C03 into	
			ds_mensagem_w;
		exit when C03%notfound;
			begin
			
			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);
										
			ds_mensagem_um_w	:= ds_mensagem_w;	
						
			end;
		end loop;
		close C03;
		
		
		nr_linha_w := 11; --APOS VENC. MULTA DE @TX_MULTA % E JUROS @TX_JUROS % AO DIA. MES REF. @DT_REFERENCIA
		
		open C03;
		loop
		fetch C03 into	
			ds_mensagem_w;
		exit when C03%notfound;
			begin
			
			ds_mensagem_dois_w 	:= null;
			ds_mensagem_tres_w 	:= ds_mensagem_w;
			
			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);	

			ds_mensagem_um_w	:= null;	
			ds_mensagem_tres_w 	:= null;											
			
			end;
		end loop;
		close C03;
	
	end if;	
		
	/* Registro mensagem verso (opcional) */
	ie_aplicacao_mensagem_w	:= 'T';
	ie_posicao_w			:= 'V';
	nr_linha_anterior_w		:= null;
	qt_msg_um_w				:= 0;
	
	open C02;
	loop
	fetch C02 into	
		nr_linha_w;
	exit when C02%notfound;
		begin
		
		ds_mensagem_concat_w	:= '';
		cont_cursor_w			:= 0;
		
		select	count(*)
		into	qt_reg_w
		from	banco_instrucao_boleto a
		where	nvl(a.ie_posicao,'F')				= ie_posicao_w
		and		nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
		and		a.cd_banco							= cd_banco_w
		and		a.nr_linha							= nr_linha_w
		and		nvl(a.nr_seq_grupo,0)				= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0));
		
		open C03;
		loop
		fetch C03 into	
			ds_mensagem_w;
		exit when C03%notfound;
			begin
			
			cont_cursor_w	:= cont_cursor_w + 1;
			ds_mensagem_concat_w	:= ds_mensagem_concat_w||ds_mensagem_w;
			
			if	(ds_mensagem_concat_w is not null) or (ds_mensagem_concat_w <> '') then
			
				if	((ds_mensagem_um_w		is null) or (nvl(nr_linha_anterior_w,nr_linha_w) = nr_linha_w)) and (qt_msg_um_w = 0) then

					ds_mensagem_um_w := ds_mensagem_concat_w;
				
					if	(cont_cursor_w = qt_reg_w) and (ds_mensagem_um_w is not null) then
					
						qt_msg_um_w	:= 1;
						
					end if;	
					
				else
				
					ds_mensagem_dois_w		:= ds_mensagem_concat_w;

				end if;

			end if;
			
			if	(ds_mensagem_um_w is not null) and ((ds_mensagem_dois_w is not null) and (cont_cursor_w = qt_reg_w)) then
			
				nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
				ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
									'0' 										|| /*Pos 02*/
									lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
									rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
									rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
									rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
									lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
									lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
				
				insert	into w_envio_banco ( nr_sequencia,
											dt_atualizacao,
											nm_usuario,
											dt_atualizacao_nrec,
											nm_usuario_nrec,
											cd_estabelecimento,
											ds_conteudo,
											nr_seq_apres)
								values	  ( w_envio_banco_seq.nextval,
											sysdate,
											nm_usuario_p,
											sysdate,
											nm_usuario_p,
											cd_estabelecimento_p,
											ds_conteudo_w,
											nr_seq_reg_arquivo_w);

				ds_mensagem_um_w	:= null;
				ds_mensagem_dois_w	:= null;
				qt_msg_um_w			:= 0;							
			
			end if;
			
			nr_linha_anterior_w	:= nr_linha_w;
			
			end;
		end loop;
		close C03;
		
		end;
	end loop;
	close C02;
	
	if	(ds_mensagem_um_w	is not null) then
	
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
							'0' 										|| /*Pos 02*/
							lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
							rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
							rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
							rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
							lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
							lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
		
		insert	into w_envio_banco ( nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									cd_estabelecimento,
									ds_conteudo,
									nr_seq_apres)
						values	  ( w_envio_banco_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									cd_estabelecimento_p,
									ds_conteudo_w,
									nr_seq_reg_arquivo_w);
									
		ds_mensagem_um_w	:= null;
		ds_mensagem_dois_w	:= null;						
	
	end if;
	
	
	ie_aplicacao_mensagem_w	:= 'C';
	
	open C04;
	loop
	fetch C04 into	
		nr_seq_conta_proc_w,
		nr_seq_conta_mat_w;
	exit when C04%notfound;
		begin
		
		nr_linha_anterior_w	:= null;
		/* Registro mensagem verso (opcional) */
		qt_msg_um_w	:= 0;
		
		open C02;
		loop
		fetch C02 into	
			nr_linha_w;
		exit when C02%notfound;
			begin
			
			ds_mensagem_concat_w	:= '';
			cont_cursor_w			:= 0;
			
			select	count(*)
			into	qt_reg_w
			from	banco_instrucao_boleto a
			where	nvl(a.ie_posicao,'F')				= ie_posicao_w
			and		nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
			and		a.cd_banco							= cd_banco_w
			and		a.nr_linha							= nr_linha_w
			and		nvl(a.nr_seq_grupo,0)				= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0));
		
			open C03;
			loop
			fetch C03 into	
				ds_mensagem_w;
			exit when C03%notfound;
				begin
				
				cont_cursor_w	:= cont_cursor_w + 1;
				ds_mensagem_concat_w	:= ds_mensagem_concat_w||ds_mensagem_w;
				
				if	(ds_mensagem_concat_w is not null) or (ds_mensagem_concat_w <> '') then

					if	((ds_mensagem_um_w is null) or (nvl(nr_linha_anterior_w,nr_linha_w)	= nr_linha_w)) and (qt_msg_um_w	= 0) then

						ds_mensagem_um_w		:= ds_mensagem_concat_w;
						
						if	(cont_cursor_w = qt_reg_w) and (ds_mensagem_um_w is not null) then
						
							qt_msg_um_w	:= 1;
							
						end if;
						
					else

						ds_mensagem_dois_w	:= ds_mensagem_concat_w;

					end if;

				end if;
				
				if	(ds_mensagem_um_w is not null) and ((ds_mensagem_dois_w is not null) and (cont_cursor_w = qt_reg_w)) then
				
					nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
					ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
										'0' 										|| /*Pos 02*/
										lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
										rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
										rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
										rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
										lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
										lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
					
					insert	into w_envio_banco ( nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												cd_estabelecimento,
												ds_conteudo,
												nr_seq_apres)
									values	  ( w_envio_banco_seq.nextval,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p,
												cd_estabelecimento_p,
												ds_conteudo_w,
												nr_seq_reg_arquivo_w);
												
					ds_mensagem_um_w	:= null;
					ds_mensagem_dois_w	:= null;
					qt_msg_um_w			:= 0;							
				
				end if;
				
				nr_linha_anterior_w	:= nr_linha_w;
				
				end;
			end loop;
			close C03;
					
			end;
		end loop;
		close C02;
		
		/* Tratar as sobras */
		if	(ds_mensagem_um_w	is not null) then
			
			nr_seq_reg_arquivo_w := nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'2' 										|| /*Pos 01*/
								'0' 										|| /*Pos 02*/
								lpad(nr_titulo_w,10,'0')					|| /*Pos 03 a 12*/
								rpad(nvl(ds_mensagem_um_w,' '),126,' ') 	|| /*Pos 13 a 138*/
								rpad(nvl(ds_mensagem_dois_w,' '),126,' ') 	|| /*Pos 139 a 264*/
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') 	|| /*Pos 265 a a 391*/
								lpad(nr_remessa_w,3,'0')					|| /*Pos 392 a 394*/		
								lpad(nr_seq_reg_arquivo_w,6,'0');			   /*Pos 395 a 400*/
			
			insert	into w_envio_banco ( nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										cd_estabelecimento,
										ds_conteudo,
										nr_seq_apres)
							values	  ( w_envio_banco_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										cd_estabelecimento_p,
										ds_conteudo_w,
										nr_seq_reg_arquivo_w);
										
			ds_mensagem_um_w	:= null;
			ds_mensagem_dois_w	:= null;						
		
		end if;
		
		end;
	end loop;
	close C04;
	
	nr_seq_conta_proc_w	:= null;
	nr_seq_conta_mat_w	:= null;
		
	end;
end loop;
close C01;

/*Inicio trailler*/

nr_seq_reg_arquivo_w := nr_seq_reg_arquivo_w + 1;

ds_conteudo_w	:=	'9'							|| /*Pos  01*/
					lpad(' ',390,' ')			|| /*Pos 02 a 391*/
					lpad(nr_remessa_w,3,'0')	|| /*Pos 392 a 394*/
					lpad(nr_seq_reg_arquivo_w,6,'0');/*Pos 395 a 400*/
					
insert	into w_envio_banco ( nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
				values	  ( w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_reg_arquivo_w);
																
/*Fim trailler*/

commit;

end ativia_gera_cobr_safra_400_msg;
/