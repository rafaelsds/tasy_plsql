create or replace 
PROCEDURE gerar_passagem_setor_evt_pepo
		(nr_cirurgia_p		NUMBER DEFAULT 0,
		 nr_seq_pepo_p		NUMBER DEFAULT 0,
		 cd_setor_atendimento_p        NUMBER,
		 cd_unidade_basica_p            VARCHAR2,
		 cd_unidade_compl_p             VARCHAR2,
		 dt_entrada_unidade_p          	DATE,
		 dt_saida_unidade_p             	DATE,
		 nr_seq_interno_p               	NUMBER,
		 ie_opcao_p                     	VARCHAR2,
		 nm_usuario_p                   	VARCHAR2,
		 ie_atualizar_evt_cirurgia_p	VARCHAR2,
		 nr_seq_aten_pac_unid_p	OUT NUMBER) IS

nr_atendimento_w 	NUMBER(10,0);
nr_atend_W 	NUMBER(10,0);
ie_cirurgia_w	VARCHAR2(1) := 'S';
ie_alter_status_pas_cc_w	VARCHAR2(1);
nr_seq_evt_cir_pac_w	evento_cirurgia_paciente.nr_sequencia%TYPE;

BEGIN
IF	((nr_cirurgia_p IS NOT NULL) OR (nr_seq_pepo_p IS NOT NULL)) AND
	(cd_setor_atendimento_p IS NOT NULL) AND
	(cd_unidade_basica_p    IS NOT NULL) AND
	(cd_unidade_compl_p     IS NOT NULL) AND
	(dt_entrada_unidade_p   IS NOT NULL) AND
	--(dt_saida_unidade_p     is not null) and
	--(nr_seq_interno_p       is not null) and
	(ie_opcao_p             IS NOT NULL) AND
	(nm_usuario_p           IS NOT NULL) THEN
	BEGIN

	IF	(NVL(nr_seq_pepo_p,0) > 0) THEN
		SELECT	NVL(MAX('S'),'N')
		INTO	ie_cirurgia_w
		FROM	cirurgia
		WHERE	nr_seq_pepo = nr_seq_pepo_p;
	END IF;

	IF	(ie_cirurgia_w = 'S') THEN
		SELECT	NVL(MAX(nr_atendimento), 0)
		INTO	nr_atendimento_w
		FROM	cirurgia
		WHERE	(((NVL(nr_cirurgia_p,0) > 0) AND (nr_cirurgia = 	nr_cirurgia_p)) OR
				 ((NVL(nr_seq_pepo_p,0) > 0) AND (nr_seq_pepo = 	nr_seq_pepo_p)));
	ELSE
		SELECT	NVL(MAX(nr_atendimento), 0)
		INTO	nr_atendimento_w
		FROM	pepo_cirurgia
		WHERE	nr_sequencia = nr_seq_pepo_p;
	END IF;

	IF	(nr_atendimento_w = 0) THEN
		Wheb_mensagem_pck.exibir_mensagem_abort(313473);
	END IF;

	gerar_passagem_setor_evento(	nr_atendimento_w,
					nr_cirurgia_p,
					nr_seq_pepo_p,
					cd_setor_atendimento_p,
					cd_unidade_basica_p,
					cd_unidade_compl_p,
					dt_entrada_unidade_p,
					dt_saida_unidade_p,
					nr_seq_interno_p,
					ie_opcao_p,
					nm_usuario_p,
					nr_atend_W);
					
	SELECT	MAX(nr_sequencia)
	INTO 	nr_seq_evt_cir_pac_w
	FROM 	evento_cirurgia_paciente
	WHERE 	NVL(ie_situacao,'A') = 'A'
	AND 	((nr_cirurgia = nr_cirurgia_p AND NVL(nr_cirurgia_p,0) > 0) OR
			 (nr_seq_pepo = nr_seq_pepo_p AND NVL(nr_seq_pepo_p,0) > 0));

	IF	(ie_atualizar_evt_cirurgia_p = 'S') THEN
		BEGIN
		UPDATE	evento_cirurgia_paciente
		SET		nr_seq_aten_pac_unid = nr_atend_W
		WHERE	nr_sequencia = nr_seq_evt_cir_pac_w;
		END;
	END IF;
	
	SELECT 	NVL(MAX(a.ie_alter_status_pas_cc), 'N')
	INTO 	ie_alter_status_pas_cc_w
	FROM    evento_cirurgia a,
			evento_cirurgia_paciente b
	WHERE   a.nr_sequencia = b.nr_seq_evento
	AND 	b.nr_sequencia = nr_seq_evt_cir_pac_w;
	
	IF	(NVL(ie_alter_status_pas_cc_w, 'N') = 'S') THEN
			
		UPDATE  unidade_atendimento a
		SET     a.ie_status_unidade   	= 	'P'
		WHERE	a.cd_unidade_basica 	=  	cd_unidade_basica_p
		AND 	a.cd_unidade_compl  	= 	cd_unidade_compl_p
		AND		a.cd_setor_atendimento 	= 	cd_setor_atendimento_p;
	END IF;
	END;
END IF;

COMMIT;

nr_seq_aten_pac_unid_p	:= nr_atend_w;

END gerar_passagem_setor_evt_pepo;
/
