create or replace
procedure Inserir_regra_consulta_contato( nm_usuario_p		Varchar2,
					  lista_perfil_p	varchar2,
					  cd_estabelecimento_p	number,
					  cd_pessoa_p		varchar2) is 
					  
lista_perfil_w		varchar2(4000);
ie_contador_w		number(10,0)	:= 0;
ie_pos_virgula_w	number(3,0);
cd_perfil_w		number(5);
nr_seq_regra_consul_w	number(10);
nr_sequencia_w		number(10,0);
tam_lista_w		number(10,0);
qt_perfil_existente_w   number(10,0);


begin
if 	(cd_pessoa_p is not null) then
	begin
	select  max(nr_sequencia)
	into	nr_seq_regra_consul_w
	from	regra_consulta_contato	
	where	cd_pessoa_fisica = cd_pessoa_p;
	end;
end if;

if 	(nr_seq_regra_consul_w is null) and (cd_pessoa_p is not null) then
	begin
	select	regra_consulta_contato_seq.nextval
	into	nr_seq_regra_consul_w
	from	dual;
	
	insert into regra_consulta_contato(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica)
	values	(
		nr_seq_regra_consul_w,
		sysdate,
		nm_usuario_p,
		cd_pessoa_p);
	end;
end if;
	
cd_perfil_w	:= null;
lista_perfil_w	:= lista_perfil_p;

while	lista_perfil_w is not null loop
	begin
	tam_lista_w		:= length(lista_perfil_w);
	ie_pos_virgula_w	:= instr(lista_perfil_w,',');

	if	(ie_pos_virgula_w <> 0) then
		begin
		cd_perfil_w	:= substr(lista_perfil_w,1,(ie_pos_virgula_w - 1));
		lista_perfil_w	:= substr(lista_perfil_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end;
	end if;
	
	SELECT nvl(count(*),0)
	into   qt_perfil_existente_w
	FROM   regra_consulta_contato a
	WHERE  a.nr_sequencia = nr_seq_regra_consul_w
	AND    EXISTS ( SELECT  1
	   		FROM   regra_consulta_contato_lib x
			WHERE  x.nr_seq_regra_consulta = a.nr_sequencia
			AND    x.cd_perfil = cd_perfil_w);
	
	
	if 	( nr_seq_regra_consul_w > 0) and (qt_perfil_existente_w = 0) then
		begin
		select	regra_consulta_contato_lib_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert into regra_consulta_contato_lib(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_regra_consulta,
			cd_perfil,
			cd_pessoa_fisica,
			ie_permite_visualizar_contato)
		values	(
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nr_seq_regra_consul_w,
			cd_perfil_w,
			'',
			'S');
		end;
	
	end if;

	end;
end loop;
		
		
commit;

end Inserir_regra_consulta_contato;
/
