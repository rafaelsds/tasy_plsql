create or replace
procedure LIBERAR_CONVENIO_CARGA_PRECO(	cd_convenio_p		number,
						nm_usuario_p		varchar2,
						ie_opcao_p		varchar2) is 

begin
if	((cd_convenio_p is not null) and
	(ie_opcao_p is not null) and
	(nm_usuario_p is not null)) then
	begin
	if	(ie_opcao_p = 'M') then
		begin
		update	convenio_carga_preco
		set	ie_status = 'L',
			nm_usuario = nm_usuario_p
		where	cd_material is not null
		and	cd_convenio = cd_convenio_p
		and	vl_item_convenio > vl_atual;
		end;
	elsif	(ie_opcao_p = 'D') then
		begin
		update	convenio_carga_preco
		set	ie_status = 'L',
			nm_usuario = nm_usuario_p
		where	cd_material is not null
		and	cd_convenio = cd_convenio_p
		and 	vl_item_convenio <> vl_atual;
		end;
	elsif	(ie_opcao_p = 'T') then
		begin
		update	convenio_carga_preco
		set	ie_status = 'L',
			nm_usuario = nm_usuario_p
		where	cd_material is not null
		and	cd_convenio = cd_convenio_p;	
		end;
	end if;
	end;
end if;
commit;

end LIBERAR_CONVENIO_CARGA_PRECO;
/