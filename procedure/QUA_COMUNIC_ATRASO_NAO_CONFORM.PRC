create or replace
procedure qua_comunic_atraso_nao_conform(cd_estabelecimento_p	number) is

nr_seq_classif_w				number(10);
nr_seq_nao_conform_w				number(10);
ds_nao_conformidade_w			varchar2(4000);
ds_tipo_nc_w					varchar2(255);
nm_pf_abertura_w				varchar2(60);
nm_pf_resp_ocorrencia_w			varchar2(60);
nm_pf_resp_eficacia_w			varchar2(60);
ds_titulo_w					varchar2(80);
ds_comunic_w					varchar2(4000);
nm_usuario_w					varchar2(2000);
nr_sequencia_w				number(10);
nr_seq_ordem_w				number(10);
nr_seq_nao_conformidade_w			number(10);
ds_dano_breve_w				varchar2(80);
dt_conclusao_desejada_w			Date;
ds_titulo_acao_w				varchar2(80);
nm_usuarios_executores_w			varchar2(200);
ds_comunic_acao_w				varchar2(4000);
nm_pf_abertura_acao_w			varchar2(60);
nm_pf_resp_ocorrencia_acao_w		varchar2(60);

cursor c01 is
	select	nr_sequencia,
		ds_nao_conformidade,
		substr(obter_descricao_padrao('QUA_TIPO_NAO_CONFORM','DS_TIPO_NC', nr_seq_tipo),1,255),
		substr(obter_usuario_pessoa(nvl(max(cd_pf_abertura),'')),1,60),
		substr(obter_usuario_pessoa(nvl(max(cd_pf_resp_ocorrencia),'')),1,60),
		substr(obter_usuario_pessoa(nvl(max(cd_pf_resp_eficacia),'')),1,60)
	from	qua_nao_conformidade
	where	cd_estabelecimento = cd_estabelecimento_p
	and	ie_status = 'Aberta'
	and	trunc(dt_abertura + 7) <= trunc(sysdate)
	group by	nr_sequencia,
			ds_nao_conformidade,
			nr_seq_tipo
	union
	select	a.nr_sequencia,
		a.ds_nao_conformidade,
		substr(obter_descricao_padrao('QUA_TIPO_NAO_CONFORM','DS_TIPO_NC', a.nr_seq_tipo),1,255),
		substr(obter_usuario_pessoa(nvl(max(a.cd_pf_abertura),'')),1,60),
		substr(obter_usuario_pessoa(nvl(max(a.cd_pf_resp_ocorrencia),'')),1,60),
		substr(obter_usuario_pessoa(nvl(max(a.cd_pf_resp_eficacia),'')),1,60)
	from	qua_nao_conformidade a,
		qua_tipo_nao_conform b
	where	a.nr_seq_tipo = b.nr_sequencia
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.ie_status = 'Andamento'
	and	b.ie_comunic_atraso_eficacia = 'S'
	and	trunc(a.dt_abertura + a.qr_dia_analise_eficacia) <= trunc(sysdate)
	group by	a.nr_sequencia,
			a.ds_nao_conformidade,
			a.nr_seq_tipo;

cursor c02 is
	select	distinct a.nr_sequencia
	from	qua_nao_conformidade a,
		man_ordem_servico b
	where	a.nr_sequencia = b.nr_seq_nao_conform
	and	b.ie_status_ordem = '1'
	and	b.dt_conclusao_desejada <= sysdate;

cursor c03 is
	select	nr_sequencia,
		ds_dano_breve,
		dt_conclusao_desejada
	from	man_ordem_servico
	where	nr_seq_nao_conform = nr_seq_nao_conformidade_w
	and	ie_status_ordem = '1'
	and	dt_conclusao_desejada <= sysdate;
	
begin

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

open c01;
loop
fetch c01 into
	nr_seq_nao_conform_w,
	ds_nao_conformidade_w,
	ds_tipo_nc_w,
	nm_pf_abertura_w,
	nm_pf_resp_ocorrencia_w,
	nm_pf_resp_eficacia_w;
exit when c01%notfound;
	begin
	ds_titulo_w	:= WHEB_MENSAGEM_PCK.get_texto(823492) /*'Comunicar atraso no processo da Ocorr�ncia SGQ'*/;
	ds_comunic_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(823494, 'NR_SEQ_NAO_CONFORM_W=' || nr_seq_nao_conform_w || 
																';DS_NAO_CONFORMIDADE_W=' || ds_nao_conformidade_w || 
																';DS_TIPO_NC_W=' || ds_tipo_nc_w),1,4000);

	nm_usuario_w	:= nm_pf_resp_eficacia_w || ', ' || nm_pf_resp_ocorrencia_w || ', ' || nm_pf_abertura_w || ', ' || nm_usuario_w;
	
	if	(nm_usuario_w is not null) then
		begin
		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into comunic_interna(
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			dt_liberacao)
		values(sysdate,
			ds_titulo_w,
			ds_comunic_w,
			'Tasy',
			sysdate,
			'N',
			nm_usuario_w,
			nr_sequencia_w,
			'N',
			nr_seq_classif_w,
			sysdate);
		end;
	end if;
	ds_comunic_w	:= '';
	nm_usuario_w	:= null;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into
	nr_seq_nao_conformidade_w;
exit when c02%notfound;
	begin
	ds_titulo_acao_w	:= WHEB_MENSAGEM_PCK.get_texto(823493); /* 'Comunicar atraso no processo das A��es da Ocorr�ncia SGQ'; */

	select	substr(obter_usuario_pessoa(nvl(max(cd_pf_abertura),'')),1,60),
		substr(obter_usuario_pessoa(nvl(max(cd_pf_resp_ocorrencia),'')),1,60)
	into	nm_pf_abertura_acao_w,
		nm_pf_resp_ocorrencia_acao_w
	from	qua_nao_conformidade
	where	nr_sequencia = nr_seq_nao_conformidade_w;

	nm_usuario_w	:= nm_pf_abertura_acao_w || ', ' || nm_pf_resp_ocorrencia_acao_w || ', ' || nm_usuario_w;

	open c03;
	loop
	fetch c03 into
		nr_seq_ordem_w,
		ds_dano_breve_w,
		dt_conclusao_desejada_w;
	exit when c03%notfound;
		begin
		select	substr(obter_select_concatenado_bv('select nm_usuario_exec from man_ordem_servico_exec
				where nr_seq_ordem = :nr','nr= ' || nr_seq_ordem_w,','),1,255)
		into	nm_usuarios_executores_w
		from	dual;

		ds_comunic_acao_w 	:= Substr(WHEB_MENSAGEM_PCK.get_texto(823495, 'NR_SEQ_NAO_CONFORMIDADE_W=' || nr_seq_nao_conformidade_w || 
																		';NR_SEQ_ORDEM_W=' || rpad(nr_seq_ordem_w,10) || 
																		';DT_CONCLUSAO_DESEJADA_W=' || dt_conclusao_desejada_w  ||
																		';DS_DANO_BREVE=' || substr(ds_dano_breve_w,1,40) || 
																		';NM_USUARIOS_EXECUTORES_W=' || nm_usuarios_executores_w) || chr(13) || chr(10) || 
																		ds_comunic_acao_w  || chr(13) || chr(10),1,4000);

		nm_usuario_w		:= nm_usuario_w || ', ' || nm_usuarios_executores_w;

		end;		
	end loop;
	close c03;
	if	(nm_usuario_w is not null) then
		begin
		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into comunic_interna(
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			dt_liberacao)
		values(sysdate,
			ds_titulo_acao_w,
			ds_comunic_acao_w,
			'Tasy',
			sysdate,
			'N',
			nm_usuario_w,
			nr_sequencia_w,
			'N',
			nr_seq_classif_w,
			sysdate);
		end;
	end if;
	ds_comunic_acao_w 	:= '';
	nm_usuario_w	  	:= null;
	end;
end loop;
close c02;

commit;

end qua_comunic_atraso_nao_conform;
/