create or replace
procedure deletar_ordem_compra_item_cb(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

begin
	if	(nr_sequencia_p is not null) then
		begin
			delete	from ordem_compra_item_cb 
			where 	nr_sequencia 	= nr_sequencia_p;
		end;
	end if;
	
commit;

end deletar_ordem_compra_item_cb;
/
