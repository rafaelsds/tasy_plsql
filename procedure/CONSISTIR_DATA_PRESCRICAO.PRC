CREATE OR REPLACE
PROCEDURE Consistir_data_Prescricao(	qt_horas_futura_p	number,
					dt_prescricao_p		Date) IS 

BEGIN

if	((sysdate + qt_horas_futura_p / 24) < dt_prescricao_p) then
	-- A data da prescri��o n�o pode ser superior a #@#@ horas a partir deste momento!
	Wheb_mensagem_pck.exibir_mensagem_abort( 173469 , 'QT_HORAS='||qt_horas_futura_p);
end if;

END Consistir_data_Prescricao;
/