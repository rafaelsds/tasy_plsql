create or replace
procedure GERAR_FLUXO_SALDO_BANCO(	cd_estabelecimento_p	number,
					dt_inicial_p		date,
					dt_final_p		date,
					nm_usuario_p		varchar2,
					ie_origem_p		varchar2,
					ie_periodo_p		varchar2,
					cd_empresa_p		number,
					ie_somente_ativa_p	varchar2,
					cd_moeda_p		number default null,
					ie_restringe_estab_p	varchar2 default null) is

/*--------------------------------------------------------------- ATENCAO ----------------------------------------------------------------*/
/* Cuidado ao realizar alteracoes no fluxo de caixa. Toda e qualquer alteracao realizada em qualquer uma das       */
/* procedures do fluxo de caixa deve ser cuidadosamente verificada e realizada no fluxo de caixa em lote.           */
/* Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando assim que           */
/* existam diferencas entre os fluxos de caixa.                                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA ALTERAR TAMBEM O FLUXO DE CAIXA EM LOTE ---------------*/

dt_referencia_w		date;
cd_conta_financ_inf_w	number(15);
vl_fluxo_w		number(15,4);
nr_seq_saldo_w		number(15);
nr_seq_saldo_ant_w	number(15);
ie_tratar_fim_semana_w	varchar2(255);
cont_w			number(15);
ie_conta_financ_ativa_w	varchar2(1);

dt_referencia_fim_w	date;
dt_referencia_dia_w	date;
/* Projeto Multimoeda - Variaveis */
cd_moeda_estrang_w	number(5);
cd_moeda_empresa_w	number(5);
vl_fluxo_estrang_w	fluxo_caixa.vl_fluxo%type;

cursor c01 is
select	distinct
	b.cd_conta_financ_inf
from	banco_saldo c,
	banco_estabelecimento b
where	c.nr_seq_conta		= b.nr_sequencia
and	b.cd_conta_financ_inf	is not null
and	b.cd_empresa		= cd_empresa_p
and	( (ie_restringe_estab_p = 'N') or
	  ( (ie_restringe_estab_p = 'E') and (b.cd_estabelecimento = cd_estabelecimento_p) ) or
	  ( (ie_restringe_estab_p = 'S') and ((b.cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(b.cd_estabelecimento) = cd_estabelecimento_p)) ) )
and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada

begin

select	max(ie_tratar_fim_semana)
into	ie_tratar_fim_semana_w
from	parametro_fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p;

/* Projeto Multimoeda - Busca a moeda padrao da empresa e verifica o parametro cd_moeda passado na procedure. Ele sera a base da busca dos dados
		em moeda estrangeira. Caso o parametro seja nulo, devera ser considerada a moeda padrao da empresa nas consultas,
		caso contrario ira buscar somente os dados na moeda selecionada.*/
select obter_moeda_padrao_empresa(cd_estabelecimento_p,'E')
into cd_moeda_empresa_w
from dual;
if (cd_moeda_p is null) then
	cd_moeda_estrang_w := cd_moeda_empresa_w;
else
	cd_moeda_estrang_w := cd_moeda_p;
end if;

open c01;
loop
fetch c01 into
	cd_conta_financ_inf_w;
exit when c01%notfound;

	dt_referencia_w	:= dt_inicial_p;
	
	/* Para o fluxo mensal devera ser excluado os registros de saldo da conta financeira diarios para nao serem somados ao saldo final do mes */
	if (ie_periodo_p = 'M') then
		begin
		
			delete from fluxo_caixa
			where 	dt_referencia		between dt_inicial_p and dt_final_p
			and 	ie_classif_fluxo 	= 'P'
			and	cd_empresa		= cd_empresa_p
			and	cd_conta_financ		= cd_conta_financ_inf_w
			and	(ie_restringe_estab_p = 'N' or
				(ie_restringe_estab_p = 'E' and cd_estabelecimento = cd_estabelecimento_p) or
				(ie_restringe_estab_p = 'S' and (cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(cd_estabelecimento) = cd_estabelecimento_p)))
			and	ie_origem 		<> 'D'
			and	ie_periodo = 'D' or ie_periodo = 'M'
			and	ie_integracao		= 'CB'
			and ie_origem 			<> 'I'; --OS 1610979, n entendi o motivo desse delete no fluxo, mas coloquei essa restricao para nao excluir dessa origem.
		
		end;
	end if;

	while	(dt_referencia_w <= dt_final_p) loop

		if	(dt_referencia_w between dt_inicial_p and dt_final_p) then

			if (ie_periodo_p = 'M') then
				
				select	sum(obter_saldo_banco(c.nr_seq_conta, dt_referencia_w)) vl_fluxo,
					sum(obter_saldo_banco_estrang(c.nr_seq_conta, dt_referencia_w)) vl_fluxo_estrang,
					count(*)
				into	vl_fluxo_w,
					vl_fluxo_estrang_w,
					cont_w
				from	banco_saldo c,
					banco_estabelecimento b
				where	c.nr_seq_conta		= b.nr_sequencia
				and	b.cd_empresa		= cd_empresa_p
				and	c.dt_referencia		between pkg_date_utils.start_of(dt_referencia_w, 'MONTH', 0) and fim_mes(dt_referencia_w)
				and	b.cd_conta_financ_inf	= cd_conta_financ_inf_w
				and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada
				
				if	(cont_w = 0) then --OS 1610979, Na OS onde o if ie_periodo_p = 'M'  acima foi implementado, nao foi feito essa parte aqui, que faz para o diario.
					
					dt_referencia_fim_w	:= fim_dia(dt_referencia_w);
					
					select	max(c.nr_sequencia)
					into	nr_seq_saldo_ant_w
					from	banco_saldo c,
						banco_estabelecimento b
					where	c.nr_seq_conta		= b.nr_sequencia
					and	b.cd_empresa		= cd_empresa_p
					and	b.cd_conta_financ_inf	= cd_conta_financ_inf_w
					and	c.dt_referencia		< dt_referencia_w
					and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada

					select	max(obter_saldo_banco_diario(c.nr_sequencia, dt_referencia_fim_w)) vl_fluxo,
						max(obter_saldo_banco_diario_estr(c.nr_sequencia, dt_referencia_fim_w)) vl_fluxo_estrang
					into	vl_fluxo_w,
						vl_fluxo_estrang_w
					from	banco_saldo c,
						banco_estabelecimento b
					where	c.nr_seq_conta	= b.nr_sequencia
					and	b.cd_empresa		= cd_empresa_p
					and	c.nr_sequencia	= nr_Seq_saldo_ant_w
					and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada
				end if;	
					
				
			else
				if	(ie_tratar_fim_semana_w = 'S') then
					dt_referencia_w			:= obter_proximo_dia_util(cd_estabelecimento_p, dt_referencia_w);
				end if;
	
				dt_referencia_dia_w	:= trunc(dt_referencia_w,'dd');
				dt_referencia_fim_w	:= fim_dia(dt_referencia_w);

				select	sum(obter_saldo_banco_diario(c.nr_sequencia, dt_referencia_fim_w)) vl_fluxo,
					sum(obter_saldo_banco_diario_estr(c.nr_sequencia, dt_referencia_fim_w)) vl_fluxo_estrang,
					count(*)
				into	vl_fluxo_w,
					vl_fluxo_estrang_w,
					cont_w
				from	banco_saldo c,
					banco_estabelecimento b
				where	c.nr_seq_conta		= b.nr_sequencia
				and	b.cd_empresa		= cd_empresa_p
				and	c.dt_referencia		between add_months(trunc(dt_referencia_dia_w,'month'), + 0) and dt_referencia_fim_w
				and	b.cd_conta_financ_inf	= cd_conta_financ_inf_w
				and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada

				
				if	(cont_w = 0) then

					select	max(c.nr_sequencia)
					into	nr_seq_saldo_ant_w
					from	banco_saldo c,
						banco_estabelecimento b
					where	c.nr_seq_conta		= b.nr_sequencia
					and	b.cd_empresa		= cd_empresa_p
					and	b.cd_conta_financ_inf	= cd_conta_financ_inf_w
					and	c.dt_referencia		< dt_referencia_dia_w
					and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada

					select	max(obter_saldo_banco_diario(c.nr_sequencia, dt_referencia_fim_w)) vl_fluxo,
						max(obter_saldo_banco_diario_estr(c.nr_sequencia, dt_referencia_fim_w)) vl_fluxo_estrang
					into	vl_fluxo_w,
						vl_fluxo_estrang_w
					from	banco_saldo c,
						banco_estabelecimento b
					where	c.nr_seq_conta	= b.nr_sequencia
					and	b.cd_empresa		= cd_empresa_p
					and	c.nr_sequencia	= nr_Seq_saldo_ant_w
					and	nvl(c.cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada

				end if;
			end if;

			select	decode(ie_somente_ativa_p,'S',ie_situacao,'A')
			into	ie_conta_financ_ativa_w
			from	conta_financeira
			where	cd_conta_financ	= cd_conta_financ_inf_w;
			
			/* Projeto Multimoeda - Verifica se e moeda estrangeira, caso positivo grava o valor em moeda estrangeira*/
			if (nvl(cd_moeda_estrang_w,cd_moeda_empresa_w) <> cd_moeda_empresa_w) then
				vl_fluxo_w	:= vl_fluxo_estrang_w;
			end if;
			
			if	(ie_conta_financ_ativa_w = 'A') then
			
				begin
				insert into fluxo_caixa
					(cd_estabelecimento,
					dt_referencia, 
					cd_conta_financ, 
					ie_classif_fluxo, 
					dt_atualizacao, 
					nm_usuario, 
					vl_fluxo, 
					ie_origem, 
					ie_periodo, 
					ie_integracao,
					cd_empresa,
					cd_moeda)
				values	(cd_estabelecimento_p, 
					dt_referencia_w,
					cd_conta_financ_inf_w,
					'P',
					sysdate,
					nm_usuario_p,
					nvl(vl_fluxo_w, 0),
					ie_origem_p,
					decode(ie_periodo_p,'M','X',ie_periodo_p), -- Alterado para permitir a geracao do saldo bancario no fluxo mensal, pois a rotina de acumulacao executada posteriormente eliminava os registros
					'CB',
					cd_empresa_p,
					nvl(cd_moeda_estrang_w,cd_moeda_empresa_w));		
				exception
					when dup_val_on_index then
						update	fluxo_caixa 
						set	vl_fluxo			= nvl(vl_fluxo_w, 0)
						where	cd_estabelecimento	= cd_estabelecimento_p
						and	cd_conta_financ		= cd_conta_financ_inf_w
						and	dt_referencia		= dt_referencia_w
						and	ie_periodo		= ie_periodo_p
						and	ie_integracao		= 'CB'
						and	cd_empresa		= cd_empresa_p
						and	nvl(cd_moeda,cd_moeda_empresa_w) = nvl(cd_moeda_estrang_w,cd_moeda_empresa_w);  --Projeto Multimoeda - Busca apenas os registros da moeda relacionada
				end;
			end if;
		end if;
	if (ie_periodo_p = 'M') then
		dt_referencia_w	:= add_months(dt_referencia_w,1);
	else
		dt_referencia_w	:= dt_referencia_w + 1;
	end if;
	
	end loop;
	
end loop;
close c01;

commit;

end	GERAR_FLUXO_SALDO_BANCO;
/
