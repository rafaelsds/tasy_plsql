create or replace 
procedure Gerar_Iot_Conta_Pend_Retorno(	dt_inicial_p 		date default to_date('01/01/2016','dd/mm/yyyy'),
					dt_final_p		date default sysdate,
					cd_convenio_parametro_p	number default 0,
					ie_tipo_atendimento_p	number default 0,
					nm_usuario_p		Varchar2 default 'IOT') is


cd_estabelecimento_w		number(5);
vl_pendente_w			number(15,2) := 0;
vl_pend_clinica_w		number(15,2) := 0;
vl_pend_medico_w		number(15,2) := 0;
nr_seq_retorno_w		convenio_retorno.nr_sequencia%type;
nr_seq_ret_item_w		convenio_retorno_item.nr_sequencia%type;
vl_glosa_w			number(15,2) := 0;
vl_pago_w			convenio_retorno_item.vl_pago%type;
Cursor C01 is
	SELECT	a.nr_atendimento nr_atend,
		p.nm_pessoa_fisica nm_pac,
		c.dt_entrada dt_entrada,
		d.ds_convenio ds_conv,
		substr(obter_valor_dominio(12,c.ie_tipo_atendimento),1,30) ds_tipo_atend,
		ac.cd_usuario_convenio cd_usuario,
		b.cd_autorizacao nr_guia,
		Nvl(b.vl_guia,0) VL_GUIA,
		/*r.dt_pagamento_previsto*/ null dt_pagto_previsto,
		/*Nvl(to_number(OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO)),0) */ 0 vl_pendente,
		/*Nvl(iot_obter_valor_pend_conta_2(a.nr_interno_conta,b.cd_autorizacao,'C'),0) */ 0 vl_pend_clinica,
		/*Nvl(iot_obter_valor_pend_conta_2(a.nr_interno_conta,b.cd_autorizacao,'M'),0) */ 0 vl_pend_medico,
		SUBSTR(obter_nome_medico(c.cd_medico_resp,'N'),1,80) nm_medico,
		A.NR_SEQ_PROTOCOLO NR_SEQ_PROTOCOLO,
		C.IE_TIPO_ATENDIMENTO IE_TIPO_ATEND,
		A.VL_CONTA VL_CONTA,
		A.CD_CONVENIO_PARAMETRO CD_CONV,
		A.NR_INTERNO_CONTA NR_CONTA,
		c.cd_medico_resp cd_medico_resp,
		a.DT_MESANO_REFERENCIA DT_MESANO_REFERENCIA,
		'C' ie_tipo_titulo
	FROM	/*titulo_receber r,*/
		conta_paciente a,
		conta_paciente_guia b,
		atendimento_paciente c,
		convenio d,
		atend_categoria_convenio ac,
		pessoa_fisica p
	WHERE	a.cd_estabelecimento = cd_estabelecimento_w
	--and	r.ie_situacao = '1'
	and	a.cd_convenio_parametro not in (26, 31)
	and	a.IE_CANCELAMENTO is null
	--and	r.nr_seq_protocolo is null
	--and	a.ie_status_acerto = 2 --definitivo
	--and	a.nr_interno_conta  = r.nr_interno_conta
	and	a.nr_interno_conta = b.nr_interno_conta
	and	a.nr_atendimento   = c.nr_atendimento
	and	c.cd_pessoa_fisica = p.cd_pessoa_fisica
	and	a.nr_atendimento   = b.nr_atendimento
	and	d.cd_convenio	= a.cd_convenio_parametro
	and	ac.nr_seq_interno = obter_atecaco_atendimento(c.nr_atendimento)
	and	trunc(a.dt_mesano_referencia) between dt_inicial_p and fim_dia(dt_final_p)
	and	((a.cd_convenio_parametro	= cd_convenio_parametro_p) or (0 = Nvl(cd_convenio_parametro_p,0)))
	and	(c.ie_tipo_atendimento = ie_tipo_atendimento_p or (0 = Nvl(ie_tipo_atendimento_p,0)))
	union
	SELECT	a.nr_atendimento nr_atend,
		p.nm_pessoa_fisica nm_pac,
		c.dt_entrada dt_entrada,
		d.ds_convenio ds_conv,
		substr(obter_valor_dominio(12,c.ie_tipo_atendimento),1,30) ds_tipo_atend,
		ac.cd_usuario_convenio cd_usuario,
		b.cd_autorizacao nr_guia,
		Nvl(b.vl_guia,0) VL_GUIA,
		/*r.dt_pagamento_previsto*/ null dt_pagto_previsto,
		/*nvl(to_number(OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO)),0) */ 0 vl_pendente,
		/*nvl(iot_obter_valor_pend_conta_2(a.nr_interno_conta,b.cd_autorizacao,'C'),0) */ 0 vl_pend_clinica,
		/*nvl(iot_obter_valor_pend_conta_2(a.nr_interno_conta,b.cd_autorizacao,'M'),0) */ 0 vl_pend_medico,
		SUBSTR(obter_nome_medico(c.cd_medico_resp,'N'),1,80) nm_medico,
		A.NR_SEQ_PROTOCOLO NR_SEQ_PROTOCOLO,
		C.IE_TIPO_ATENDIMENTO IE_TIPO_ATEND,
		A.VL_CONTA VL_CONTA,
		A.CD_CONVENIO_PARAMETRO CD_CONV,
		A.NR_INTERNO_CONTA NR_CONTA,
		c.cd_medico_resp cd_medico_resp,
		a.DT_MESANO_REFERENCIA DT_MESANO_REFERENCIA,
		'P' ie_tipo_titulo
	FROM	/*titulo_receber r,*/
		conta_paciente a,
		conta_paciente_guia b,
		atendimento_paciente c,
		convenio d,
		atend_categoria_convenio ac,
		pessoa_fisica p,
		protocolo_convenio pc
	WHERE	a.cd_estabelecimento = cd_estabelecimento_w
	--and	r.ie_situacao = '1'
	and	a.cd_convenio_parametro not in (26, 31)
	and	a.IE_CANCELAMENTO is null
	--and	r.nr_interno_conta is null
	--and	a.ie_status_acerto = 2 --definitivo
	and	pc.ie_status_protocolo = 2 --definitivo
	and	pc.nr_seq_protocolo = a.nr_seq_protocolo
	--and	pc.nr_seq_protocolo = r.nr_seq_protocolo
	and	a.nr_interno_conta = b.nr_interno_conta
	and	a.nr_atendimento   = c.nr_atendimento
	and	c.cd_pessoa_fisica = p.cd_pessoa_fisica
	and	d.cd_convenio	= a.cd_convenio_parametro
	and	ac.nr_seq_interno = obter_atecaco_atendimento(c.nr_atendimento)
	and	trunc(a.dt_mesano_referencia) between dt_inicial_p and fim_dia(dt_final_p)
	and	((a.cd_convenio_parametro	= cd_convenio_parametro_p) or (0 = Nvl(cd_convenio_parametro_p,0)))
	and	(c.ie_tipo_atendimento = ie_tipo_atendimento_p or (0 = Nvl(ie_tipo_atendimento_p,0))); 

c01_w	c01%rowtype;


Cursor C02 is
	select	a.nr_sequencia nr_seq_propaci,
		Nvl(a.vl_procedimento,0) vl_proc,
		obter_classif_material_proced(null, cd_procedimento, ie_origem_proced) ie_classif,
		ie_emite_conta ie_emite_conta,
		nr_seq_proc_pacote nr_seq_proc_pacote
	from	procedimento_paciente a
	where	nr_interno_conta			= c01_w.NR_CONTA
	and	nvl(nr_doc_convenio,'N�o Informada')	= c01_w.NR_GUIA
	and 	cd_motivo_exc_conta is null;
	
c02_w c02%rowtype;
	
Cursor C03 is
	select	nr_sequencia nr_seq_matpaci,
		Nvl(vl_material,0) vl_mat
	from	material_atend_paciente
	where	nr_interno_conta			= c01_w.NR_CONTA
	and	nvl(nr_doc_convenio,'N�o Informada')	= c01_w.NR_GUIA
	and	cd_motivo_exc_conta is null;
	
c03_w c03%rowtype;	
begin

delete	from IOT_CONTA_PEND_RETORNO
where	nm_usuario = nm_usuario_p;
commit;

if	(dt_inicial_p is null) then
	Raise_application_error(-20011,'� OBRIGAT�RIO INFORMAR UM PER�ODO NOS FILTROS DO RELAT�RIO');
end if;

if	(dt_final_p is null) then
	Raise_application_error(-20011,'� OBRIGAT�RIO INFORMAR UM PER�ODO NOS FILTROS DO RELAT�RIO');
end if;

cd_estabelecimento_w := Nvl(wheb_usuario_pck.get_cd_estabelecimento,1);
vl_pendente_w := 0;

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
	begin
	
	select	Nvl(sum(a.vl_pago + a.vl_glosado),0),
		max(b.nr_sequencia) nr_seq_retorno,
		max(a.nr_sequencia) nr_seq_ret_item
	into	vl_pago_w,
		nr_seq_retorno_w,
		nr_seq_ret_item_w
	from	convenio_retorno b,
		convenio_retorno_item a
	where	a.nr_seq_retorno	= b.nr_sequencia
	and	b.ie_status_retorno	= 'F'
	and 	a.nr_interno_conta	= c01_w.NR_CONTA
	and 	a.cd_autorizacao 	= c01_w.NR_GUIA;
	
	vl_pendente_w := c01_w.VL_GUIA - vl_pago_w;
	
	
	
	if	(vl_pendente_w > 0) then
		
	
		vl_pend_medico_w := 0;
		vl_pend_clinica_w := 0;
		open C02;
		loop
		fetch C02 into	
			c02_w;
		exit when C02%notfound;
			
			if	(nr_seq_retorno_w is null) then
				
				if	(c02_w.ie_classif <> 2) and
					(c02_w.nr_seq_proc_pacote is null) then
					
					vl_pend_medico_w := vl_pend_medico_w + C02_w.vl_proc;
					
				end if;
				
				if	(c02_w.ie_emite_conta <> '77') and	
					(c02_w.ie_classif = 2) then
					
					vl_pend_clinica_w := vl_pend_clinica_w + C02_w.vl_proc;
					
				end if;
				
			else
				
				SELECT	NVL(SUM(a.vl_glosa),0)
				INTO	vl_glosa_w
				FROM	motivo_glosa c,
					convenio_retorno_glosa a,
					convenio_retorno_item b
				WHERE	c.ie_acao_glosa		= 'R'
				and	a.cd_motivo_glosa	= c.cd_motivo_glosa
				and	b.nr_sequencia		= a.nr_seq_ret_item
				and	b.nr_sequencia		= nr_seq_ret_item_w
				AND	a.nr_seq_propaci	= c02_w.nr_seq_propaci
				and	b.nr_seq_retorno	= nr_seq_retorno_w;
				
				
				
				if	(c02_w.ie_classif <> 2) and
					(c02_w.nr_seq_proc_pacote is null) then

					vl_pend_medico_w := vl_pend_medico_w + vl_glosa_w;

				end if;
				
				if	(c02_w.ie_emite_conta <> '77') and	
					(c02_w.ie_classif = 2) then

					vl_pend_clinica_w := vl_pend_clinica_w + vl_glosa_w;

				end if;
			end if;
			
		end loop;
		close C02;
		
		open C03;
		loop
		fetch C03 into	
			c03_w;
		exit when C03%notfound;
			
			if	(nr_seq_retorno_w is null) then
				
				vl_pend_clinica_w := vl_pend_clinica_w + C03_w.vl_mat;
				
			else
				
				SELECT	NVL(SUM(a.vl_glosa),0)
				INTO	vl_glosa_w
				FROM	motivo_glosa c,
					convenio_retorno_glosa a,
					convenio_retorno_item b
				WHERE	c.ie_acao_glosa		= 'R'
				and	a.cd_motivo_glosa	= c.cd_motivo_glosa
				and	b.nr_sequencia		= a.nr_seq_ret_item
				and	b.nr_sequencia		= nr_seq_ret_item_w
				AND	a.nr_seq_matpaci	= c03_w.nr_seq_matpaci
				and	b.nr_seq_retorno	= nr_seq_retorno_w;
				
				
				
				vl_pend_clinica_w := vl_pend_clinica_w + vl_glosa_w;
			end if;
			
		end loop;
		close C03;	
		
		
		insert	into IOT_CONTA_PEND_RETORNO(
			NR_ATENDIMENTO,
			NM_PACIENTE,
			DT_ENTRADA,
			DS_CONVENIO,
			DS_TIPO_ATEND,
			CD_MATRICULA,
			VL_CONTA,
			VL_PENDENTE,
			VL_PEND_MEDICO,
			VL_PEND_CLINICA,
			NM_MEDICO,
			CD_CONVENIO_PARAMETRO,
			NR_INTERNO_CONTA,
			CD_ESTABELECIMENTO,
			DT_MESANO_REFERENCIA,
			VL_GUIA,
			CD_MEDICO_RESP,
			NR_SEQ_PROTOCOLO,
			IE_TIPO_ATENDIMENTO,
			CD_AUTORIZACAO,
			NM_USUARIO,
			DT_PAGAMENTO_PREVISTO,
			ie_tipo_titulo)
		values	(c01_w.nr_atend,
			c01_w.nm_pac,
			c01_w.dt_entrada,
			c01_w.ds_conv,
			c01_w.ds_tipo_atend,
			c01_w.cd_usuario,
			c01_w.vl_conta,
			vl_pendente_w,
			vl_pend_medico_w,
			vl_pend_clinica_w,
			c01_w.NM_MEDICO,
			c01_w.CD_CONV,
			c01_w.NR_CONTA,
			cd_estabelecimento_w,
			c01_w.DT_MESANO_REFERENCIA,
			c01_w.VL_GUIA,
			c01_w.CD_MEDICO_RESP,
			c01_w.NR_SEQ_PROTOCOLO,
			c01_w.IE_TIPO_ATEND,
			c01_w.NR_GUIA,
			NM_USUARIO_P,
			c01_w.dt_pagto_previsto,
			c01_w.ie_tipo_titulo);				
	end if;
	
	
	
		
		
	end;
end loop;
close C01;

commit;

end Gerar_Iot_Conta_Pend_Retorno;
/