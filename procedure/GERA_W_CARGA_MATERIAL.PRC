create or replace
procedure gera_w_carga_material(	nr_seq_estrutura_p	number,
					cd_material_p		number,
					nm_usuario_p	varchar2) is 
ie_existe_w	number(1);
begin

select	count(1)
into	ie_existe_w
from	material
where	cd_material = cd_material_p;

if(ie_existe_w > 0) then
	begin
	insert into mat_estrutura_cadastro(nr_sequencia,
		nr_seq_estrutura,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material)
	values( mat_estrutura_cadastro_seq.nextval,
		nr_seq_estrutura_p,
		sysdate,
		nm_usuario_p,
		sysdate,
    nm_usuario_p,
		cd_material_p);
    end;
	commit;
end if;

end gera_w_carga_material;
/
