create or replace
procedure vincular_lote_rec_item(
			nr_seq_conpaci_ret_hist_p	number,
			nr_sequencia_p			number,
			nm_usuario_p			Varchar2) is 
begin
update	convenio_retorno_glosa
set	nr_seq_conpaci_ret_hist	= nr_seq_conpaci_ret_hist_p
where	nr_sequencia		= nr_sequencia_p;
commit;
end vincular_lote_rec_item;
/