create or replace procedure pls_atualizar_mensalidade_web
			(	nr_titulo_p			number,
				dt_vencimento_p			date,
				cd_motivo_p			number,
				ds_observacao_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_regra_p			number,
				ds_retorno_regra_p	out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar / recalcular a mensalidade do benefici�rio, conforme data de
vencimento informada
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ X ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_mensagem_w			varchar2(2000);
ds_retorno_regra_w		varchar2(255)	:= 'OK';
cd_cgc_estab_w			varchar2(14);
nr_seq_pagador_w		number(10);
qt_max_dias_boleto_w		number(10);
cd_estabelecimento_w		number(4);
qt_max_dias_titulo_w		number(4);
ie_permite_w			varchar2(1)	:= 'S';
dt_ultimo_venc_w		date;
dt_vencimento_w			date;
dt_pagamento_previsto_w		date;
dt_venc_original_w		date;
ie_juros_multa_portal_w		varchar2(1);
ie_juros_multa_venc_orig_w	parametro_contas_receber.ie_juros_multa_venc_orig%type;
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
nr_seq_alt_venc_w		number(10);
ie_mes_ref_mensalidade_w	varchar2(2) := 'S';
nr_seq_mensalidade_w		Number(10);
dt_mesano_referencia_w		date;
dt_venc_original_util_w		date;
ie_permite_recalculo_w		varchar2(1) := 'N';
qt_dias_limite_prorrogacao_w	pls_regra_emis_seg_via_bol.qt_dias_limite_prorrogacao%type;
nr_seq_regra_w			number(10);
cd_pessoa_fisica_w		titulo_receber.cd_pessoa_fisica%type;
cd_cgc_w			titulo_receber.cd_cgc%type;
qt_max_dias_dt_atual_w		pls_regra_emis_seg_via_bol.qt_max_dias_dt_atual%type;
ie_dt_vencimento_dia_util_w	varchar2(1);
ie_recalcular_juros_multa_w	varchar2(1) := 'S';
ie_juros_multa_venc_portal_w	varchar(2);
qt_vezes_permitido_w		pls_regra_emis_seg_via_bol.qt_vezes_permitido%type;
qt_titulo_vencido_w		number(4);
dt_alt_venc_w			date;
ie_multa_portal_w		varchar2(1) := 'S';
ie_consistir_regra_w		varchar2(1) := 'S';
ie_cons_impr_sem_alt_w		pls_regra_emis_seg_via_bol.ie_consistir_impressao_sem_alt%type;
ie_tipo_titulo_w		varchar2(2);


begin
if	(nr_titulo_p is not null) then
	select	a.nr_seq_pagador,
		a.cd_estabelecimento,
		a.dt_pagamento_previsto,
		a.dt_vencimento,
		a.nr_seq_mensalidade,
		a.cd_pessoa_fisica,
		a.cd_cgc,
		a.ie_tipo_titulo
	into	nr_seq_pagador_w,
		cd_estabelecimento_w,
		dt_pagamento_previsto_w,
		dt_venc_original_w,
		nr_seq_mensalidade_w,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		ie_tipo_titulo_w
	from	titulo_receber	a
	where	a.nr_titulo	= nr_titulo_p;

	nr_seq_regra_w	:= pls_obter_regra_seg_via_boleto(	cd_pessoa_fisica_w,
								cd_cgc_w,
								nr_seq_pagador_w,
								nr_titulo_p,
								sysdate,
								'PO',
								cd_estabelecimento_w);

	dt_venc_original_util_w	:= obter_proximo_dia_util(cd_estabelecimento_w, dt_venc_original_w);
	if(dt_vencimento_p is null) then
		dt_vencimento_w := dt_venc_original_util_w;
	else
		dt_vencimento_w	:= obter_proximo_dia_util(cd_estabelecimento_w, dt_vencimento_p);
	end if;

	if	(nr_seq_regra_w is not null) and
		(nr_seq_regra_w > 0) then

		select	nvl(max(qt_max_dias_boleto), 0),
			nvl(max(qt_max_dias_dt_atual),0),
			max(ie_permite),
			nvl(max(ie_consistir_impressao_sem_alt),'S')
		into	qt_max_dias_boleto_w,
			qt_max_dias_dt_atual_w,
			ie_permite_w,
			ie_cons_impr_sem_alt_w
		from	pls_regra_emis_seg_via_bol
		where	nr_sequencia	= nr_seq_regra_w;

		if(dt_vencimento_p = dt_pagamento_previsto_w and ie_cons_impr_sem_alt_w = 'N')then
			ie_consistir_regra_w := 'N';
		end if;

		if(ie_consistir_regra_w = 'S') then

			if 	(ie_permite_w = 'N') then
				select 	ds_mensagem
				into	ds_retorno_regra_w
				from	pls_regra_emis_seg_via_bol	a
				where	a.nr_sequencia	= nr_seq_regra_w;

			end if;

			if	(qt_max_dias_boleto_w > 0) and
				(nr_seq_pagador_w is not null) then
				select	max(a.dt_vencimento)
				into	dt_ultimo_venc_w
				from	titulo_receber		a,
					pls_mensalidade		b,
					pls_lote_Mensalidade 	c
				where	a.nr_seq_mensalidade 	= b.nr_sequencia
				and	b.nr_seq_lote		= c.nr_sequencia
				and	a.nr_seq_pagador	= nr_seq_pagador_w
				and	c.ie_visualizar_portal 	= 'S'
				and	a.ie_situacao 		= '1';

				dt_ultimo_venc_w := obter_proximo_dia_util(cd_estabelecimento_w,dt_ultimo_venc_w);

				if	(trunc(dt_vencimento_p,'dd') > (trunc(dt_ultimo_venc_w,'dd') + qt_max_dias_boleto_w)) then
					ds_retorno_regra_w	:= 'A data de vencimento informada n�o pode ser superior a ' || qt_max_dias_boleto_w ||
									' dias do vencimento da �ltima parcela em aberto.';
				elsif	(dt_vencimento_p < trunc(sysdate,'dd')) then
					ds_retorno_regra_w	:= 'A data de vencimento informada n�o pode ser menor que a data atual.';
				end if;
			end if;

			if	(qt_max_dias_dt_atual_w > 0) then
				-- Considerar dias �teis . OS: 901440
				if	(trunc(dt_vencimento_p,'dd') > obter_proximo_dia_util(cd_estabelecimento_w, trunc(sysdate,'dd') + qt_max_dias_dt_atual_w)) then
				--if	(trunc(dt_vencimento_p,'dd') > trunc(sysdate,'dd') + qt_max_dias_dt_atual_w) then	-- ( TRATAMENTO ANTIGO )
					ds_retorno_regra_w	:= 'A data de vencimento informada n�o pode ser superior a ' || qt_max_dias_dt_atual_w ||
									' dia(s) da data atual.';
				end if;
			end if;

			select	max(a.ds_mensagem),
				max(a.qt_max_dias_titulo),
				max(ie_mes_ref_mensalidade),
				max(qt_dias_limite_prorrogacao),
				max(qt_vezes_permitido)
			into	ds_mensagem_w,
				qt_max_dias_titulo_w,
				ie_mes_ref_mensalidade_w,
				qt_dias_limite_prorrogacao_w,
				qt_vezes_permitido_w
			from	pls_regra_emis_seg_via_bol	a
			where	a.nr_sequencia	= nr_seq_regra_w;

			if	(nr_titulo_p is not null) and
				(qt_vezes_permitido_w is not null) then
				select	count(1)
				into	qt_titulo_vencido_w
				from	alteracao_vencimento
				where	nr_titulo = nr_titulo_p;

				if	(qt_vezes_permitido_w <= qt_titulo_vencido_w) then
					ds_retorno_regra_w	:= 'N�o � poss�vel imprimir o boleto com mais de ' || qt_vezes_permitido_w || ' altera��es de vencimento';
				end if;
			end if;

			if	(qt_max_dias_titulo_w > 0) and
				(trunc(dt_vencimento_p) > (trunc(dt_venc_original_w) + qt_max_dias_titulo_w)) then

				ds_retorno_regra_w := substr(ds_mensagem_w, 1, 255);
			end if;

			if	(trunc(dt_vencimento_w,'dd') < trunc(sysdate,'dd') and cd_motivo_p != 0) then
				ds_retorno_regra_w := 'A data de vencimento informada'||trunc(dt_vencimento_w,'dd')||' n�o pode ser menor que a data vigente '||trunc(sysdate,'dd')||'.';
			end if;

			/* Indicar se pode ser informado um vencimento fora do m�s da mensalidade */
			if	(ds_retorno_regra_w = 'OK' and nvl(ie_mes_ref_mensalidade_w, 'S') = 'N') then
				select  max(dt_mesano_referencia)
				into	dt_mesano_referencia_w
				from    pls_mensalidade a,
					pls_mensalidade_segurado b
				where   a.nr_sequencia	= b.nr_seq_mensalidade
				and     a.nr_sequencia	= nr_seq_mensalidade_w;

				if	(to_char(dt_mesano_referencia_w, 'mm/yyyy') <> to_char(dt_vencimento_p, 'mm/yyyy') and cd_motivo_p != 0) then
					ds_retorno_regra_w	:= substr(ds_mensagem_w, 1, 255);
				end if;
			end if;

			if 	(qt_dias_limite_prorrogacao_w >= 0) and
				(qt_dias_limite_prorrogacao_w is not null) and
				(ds_retorno_regra_w = 'OK') then
				if 	(obter_proximo_dia_util(cd_estabelecimento_w, trunc(sysdate,'dd') + qt_dias_limite_prorrogacao_w) < trunc(dt_vencimento_w, 'dd')) then
					ds_retorno_regra_w := 'N�o � poss�vel gerar o boleto para mais de ' || qt_dias_limite_prorrogacao_w || ' dia(s) da data atual!';
				end if;
			end if;
		end if;
	end if;

	if	(ds_retorno_regra_w = 'OK' and cd_motivo_p != 0) then

		/* Titulo prorrogado*/
		if	(dt_venc_original_w < dt_vencimento_p) then
			/* Atualiza a carteira */
			pls_alterar_carteira_cob_bol(	nr_titulo_p,'PW',nm_usuario_p);

		end if;

		/* Caso a data selecionada no portal seja menor que a data de vencimento original do t�tulo e j� exista uma prorroga��o de vencimento lan�ada,
			considerar a data de vencimento original para impress�o do boleto pelo portal - OS 901815 USJRP */
		ie_multa_portal_w := 'S';
		begin
		select	dt_vencimento
		into	dt_alt_venc_w
		from	alteracao_vencimento a
		where	a.nr_titulo	= nr_titulo_p
		and	a.nr_sequencia = (select max(x.nr_sequencia)
					from alteracao_vencimento x
					where x.nr_titulo = nr_titulo_p);
		exception when others then
			dt_alt_venc_w := dt_venc_original_w;
		end;
		if	(trunc(dt_vencimento_p,'dd') <= trunc(dt_venc_original_util_w,'dd')
			and trunc(dt_alt_venc_w,'dd') > trunc(dt_venc_original_w,'dd')) then
			dt_vencimento_w := dt_venc_original_util_w;
			ie_multa_portal_w := 'N';
		end if;

		/* Altera vencimento do t�tulo conforme altera��o da data e c�lculo de juros */
		if	(trunc(dt_vencimento_w,'dd') > trunc(dt_venc_original_util_w,'dd')) then--trunc(obter_proximo_dia_util(cd_estabelecimento_w, dt_pagamento_previsto_w),'dd')) then

			select	nvl(max(ie_juros_multa_venc_portal),'R')
			into	ie_juros_multa_venc_portal_w
			from	pls_web_param_geral
			where	cd_estabelecimento	= cd_estabelecimento_w;

			if	(ie_juros_multa_venc_portal_w = 'R') then
				select	max(ie_juros_multa_venc_orig)
				into	ie_juros_multa_venc_orig_w
				from	parametro_contas_receber
				where	cd_estabelecimento	= cd_estabelecimento_w;

				ie_juros_multa_venc_orig_w	:= nvl(ie_juros_multa_venc_orig_w,'N');

			elsif	(ie_juros_multa_venc_portal_w = 'O') then
				ie_juros_multa_venc_orig_w	:= 'S';
			elsif	(ie_juros_multa_venc_portal_w = 'P') then
				ie_juros_multa_venc_orig_w	:= 'N';
			end if;

			/*verifica se a data de vencimento do titulo � um dia util*/
			select	obter_se_dia_util(dt_venc_original_w,cd_estabelecimento_w)
			into	ie_dt_vencimento_dia_util_w
			from	dual;

			ie_recalcular_juros_multa_w := 'S';
			vl_juros_w	:= 0;
			vl_multa_w  := 0;

			/*se n�o for dia util, verificar se o proximo dia util baseado na data de vencimento original � o mesmo da nova data de vencimento, se for, nao deve calcular juros e multa*/
			if ( nvl(ie_dt_vencimento_dia_util_w,'S') = 'N' ) and ( to_date(dt_venc_original_util_w,'dd/mm/yyyy') = to_date(dt_vencimento_w,'dd/mm/yyyy') ) then
				ie_recalcular_juros_multa_w := 'N';
			end if;

			/*So recalcular se o vencimento atual nao for em dia util e o proximo dia util informado no vencimento for diferente do proximo dia util*/
			if ( nvl(ie_recalcular_juros_multa_w,'S') = 'S') then
				calcular_juro_multa_titulo(	null,
								nr_titulo_p,
								dt_vencimento_w,
								'N',
								ie_juros_multa_venc_orig_w,
								vl_juros_w,
								vl_multa_w);
			end if;

			alterar_vencimento_tit_rec(	nr_titulo_p,
							dt_vencimento_w,
							cd_motivo_p,
							'Gerado via portal',
							nm_usuario_p);

			begin
			select	ie_juros_multa_portal
			into	ie_juros_multa_portal_w
			from	pls_parametros_cr
			where	cd_estabelecimento	= cd_estabelecimento_w;
			exception
			when others then
				ie_juros_multa_portal_w		:= 'N';
			end;

			if	(nvl(ie_juros_multa_portal_w,'N') = 'S') then
				select 	max(nr_sequencia)
				into	nr_seq_alt_venc_w
				from 	alteracao_vencimento
				where	nr_titulo	= nr_titulo_p;

				update 	alteracao_vencimento
				set	vl_juros		= vl_juros_w,
					vl_multa		= vl_multa_w,
					ie_calc_juros_multa	= 'S'
				where	nr_titulo		= nr_titulo_p
				and	nr_sequencia		= nr_seq_alt_venc_w;

				atualiza_venc_jur_mult_cre(	nm_usuario_p,
								vl_juros_w,
								vl_multa_w,
								dt_vencimento_w,
								'S',
								nr_titulo_p,
								nvl(ie_multa_portal_w,'S'));
			end if;
		end if;

		update	titulo_receber
		set	ie_prorrogacao_portal	= 'S'
		where	nr_titulo		= nr_titulo_p;
		
		/*Se o tipo do t�tulo � '10' - D�bito autom�tico e n�o est� vencido, n�o gera o bloqueto OS 1357175 */
		if	(ie_tipo_titulo_w <> '10' or dt_vencimento_p > dt_venc_original_w) then
			/* Atualiza o valor de juros e multas do boleto */		
			gerar_bloqueto_tit_rec(nr_titulo_p, 'PW');
		end if;		
		
		update	titulo_receber
		set	ie_prorrogacao_portal	= 'N'
		where	nr_titulo		= nr_titulo_p;
	end if;
	
	Gerar_Via_Tit_Rec_Bloqueto ( nr_titulo_p, 'PW', nm_usuario_p);

end if;

ds_retorno_regra_p	:= ds_retorno_regra_w;

commit;

end pls_atualizar_mensalidade_web;
/