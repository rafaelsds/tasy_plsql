create or replace
procedure lab_alterar_resultado_exame 	(NR_SEQ_RESULTADO_P	in	number,
				       	 NR_SEQUENCIA_P		in 	number,
					 NM_USUARIO_P		in 	varchar2,
					 DS_RESULTADO_P         in	varchar2,
                                         DS_RETORNO_P           out     varchar2) is
nr_seq_exame_w		number(10);
ds_resultado_w		varchar2(4000);
qt_resultado_w		number(15,4);
pr_resultado_w		number(9,4);
qt_decimais_w		number(1);
ie_formato_resultado_w	varchar2(3);
dt_digitacao_w		date;
nm_usuario_prim_dig_w	varchar2(15);
ie_status_w		varchar2(1);
ie_formato_incorreto_w  varchar2(1);

begin
ie_formato_incorreto_w := 'N';

select	a.nr_seq_exame,
	nvl(qt_decimais, 0),
	dt_digitacao,
	nm_usuario_prim_dig
into	nr_seq_exame_w,
	qt_decimais_w,
	dt_digitacao_w,
	nm_usuario_prim_dig_w
from	exame_lab_result_item a
where	a.nr_seq_resultado = nr_seq_resultado_p
and	a.nr_sequencia = nr_sequencia_p;

select	a.ie_formato_resultado
into	ie_formato_resultado_w
from	exame_laboratorio a
where	a.nr_seq_exame = nr_seq_exame_w;

begin
case	ie_formato_resultado_w
	when	'D'	then	ds_resultado_w := DS_RESULTADO_P;
	when	'DL'	then	ds_resultado_w := DS_RESULTADO_P;
	when	'DD'	then	ds_resultado_w := DS_RESULTADO_P; -- alinhar a direita
	when	'DV'	then	
		if (lab_obter_se_somente_numero(DS_RESULTADO_P) = 'S') then
			begin
			--qt_resultado_w:= formata_casas_decimais(DS_RESULTADO_P, qt_decimais_w);
			
			select to_number(DS_RESULTADO_P)
			into qt_resultado_w
			from dual;
			
			qt_resultado_w:= formata_casas_decimais(DS_RESULTADO_P, qt_decimais_w);
			exception
			when others then
				begin
					select to_number(replace(DS_RESULTADO_P, '.', ','))
					into qt_resultado_w
					from dual;
				exception
				when others then
					ds_resultado_w:= DS_RESULTADO_P;
				end;
			end;
		else
			ds_resultado_w:= DS_RESULTADO_P;
		end if;
	when	'V'	then	qt_resultado_w:= formata_casas_decimais(DS_RESULTADO_P, qt_decimais_w);
	when	'P'	then	pr_resultado_w:= formata_casas_decimais(DS_RESULTADO_P, qt_decimais_w);
	when	'VP'	then	qt_resultado_w:= formata_casas_decimais(DS_RESULTADO_P, qt_decimais_w);
	when	'CV'	then	qt_resultado_w:= formata_casas_decimais(DS_RESULTADO_P, qt_decimais_w);
	else
		begin
			ie_formato_incorreto_w := 'S';
			goto FINAL;
		end;
end case;
exception
	when others then
		ie_formato_incorreto_w := 'S';
		goto FINAL;
end;

if 	(dt_digitacao_w is null) then
	dt_digitacao_w := sysdate;
end if;
	
if 	(nm_usuario_prim_dig_w is null) then
	nm_usuario_prim_dig_w := nm_usuario_p;
end if;
	
if 	not((ds_resultado_w is null) and
	(qt_resultado_w is null) and
	(pr_resultado_w is null)) then
	ie_status_w := '1';
end if;

update	exame_lab_result_item
set	ds_resultado = ds_resultado_w,
	qt_resultado = qt_resultado_w,
	pr_resultado = pr_resultado_w,
	dt_digitacao = dt_digitacao_w,
	nm_usuario_prim_dig = nm_usuario_prim_dig_w,
	ie_status = ie_status_w
where	nr_seq_resultado = nr_seq_resultado_p
and	nr_sequencia = nr_sequencia_p;

<<FINAL>>

commit;

DS_RETORNO_P := ie_formato_incorreto_w;

end lab_alterar_resultado_exame ;
/