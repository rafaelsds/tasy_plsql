create or replace 
procedure sup_iniciar_mes_estoque is

dt_mesano_vigente_ajuste_w	parametro_estoque.dt_mesano_vigente%type;
dt_mesano_vigente_w			parametro_estoque.dt_mesano_vigente%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

begin
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

begin
	select  min(dt_mesano_referencia)
	into	dt_mesano_vigente_ajuste_w
	from    saldo_estoque
	where	cd_estabelecimento = cd_estabelecimento_w;
exception
	when others	 then
		dt_mesano_vigente_ajuste_w := sysdate;
end;

if	(dt_mesano_vigente_ajuste_w is null) then
	dt_mesano_vigente_ajuste_w := sysdate;
end if;

dt_mesano_vigente_w	:= pkg_date_utils.start_of(dt_mesano_vigente_ajuste_w,'MONTH',0);

update	parametro_estoque a
set		a.dt_mesano_vigente = dt_mesano_vigente_w
where	a.cd_estabelecimento = cd_estabelecimento_w
and		not exists( select	1
					from	parametro_estoque b
					where	b.cd_estabelecimento = cd_estabelecimento_w
					and		b.dt_mesano_vigente is not null);
					
					
commit;

end sup_iniciar_mes_estoque;
/
