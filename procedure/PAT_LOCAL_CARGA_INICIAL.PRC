create or replace
procedure pat_local_carga_inicial(nm_usuario_p varchar2,	
				  cd_estabelecimento_p number) is 
				  
nr_sequencia_w 		number(10);
ds_local_w		varchar2(80);
cd_centro_custo_w	number(8);
cd_setor_w		number(8);
qt_registro_w		number(1) :=0;

cursor c01 is 
select 	nr_sequencia,
	substr(ds_localizacao,1,80),
	cd_centro_custo,
	cd_setor
from 	man_localizacao
where 	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;

begin

open c01;
loop
fetch c01 into	
	nr_sequencia_w,
	ds_local_w,
	cd_centro_custo_w,
	cd_setor_w;
exit when c01%notfound;
	begin
	
	select 	count(*)
	into	qt_registro_w
	from	pat_local
	where	nr_seq_local_manutencao = nr_sequencia_w
	and	ie_situacao = 'A'
	and	((cd_centro_custo <> cd_centro_custo_w) or 
		(cd_setor_atendimento <> cd_setor_w));
		
	if 	(qt_registro_w > 0) then
		update 	pat_local
		set	cd_centro_custo = cd_centro_custo_w,
			cd_setor_atendimento = cd_setor_w
		where	nr_seq_local_manutencao = nr_sequencia_w
		and	ie_situacao = 'A'
		and	((cd_centro_custo <> cd_centro_custo_w) or 
			(cd_setor_atendimento <> cd_setor_w));
	else
	
		man_insert_pat_local(	nm_usuario_p,		
					cd_centro_custo_w,	
					cd_estabelecimento_p,	
					cd_setor_w,		
					ds_local_w,		
					nr_sequencia_w);
	end if;
	
	end;			
end loop;
close c01;

commit;

end pat_local_carga_inicial;
/