create or replace
procedure med_pessoa_fisica_AfterPost(	cd_pessoa_fisica_p	varchar2,
					nm_pessoa_fisica_p	varchar2,
					cd_medico_p		varchar2,
					nm_usuario_p		varchar2) is 

qt_registro_w		number(10,0);
nr_seq_cliente_w	number(10,0);
vl_parametro_w		varchar2(255);
				
begin

if	(cd_pessoa_fisica_p is not null) and
	(cd_medico_p is not null) then
	begin
	select	count(*)
	into	qt_registro_w
	from	med_cliente
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	cd_medico		= cd_medico_p;
	
	if	(qt_registro_w = 0) then
		begin
		med_incluir_paciente_cliente(cd_pessoa_fisica_p, cd_medico_p, nm_usuario_p);

		select	nvl(nr_sequencia,0)
		into	nr_seq_cliente_w
		from	med_cliente
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	cd_medico		= cd_medico_p;

		if	(nr_seq_cliente_w > 0) then		
			med_ultima_atualizacao_cliente(nr_seq_cliente_w, cd_pessoa_fisica_p, cd_medico_p, 'C');
		end if;
		end; 
	end if;
	Obter_Param_Usuario(9007,59, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
	
	if (vl_parametro_w = 'S') then
		med_altera_inicial_nome_pac(cd_pessoa_fisica_p, obter_initcap(nm_pessoa_fisica_p));
	end if;
	end;
end if;

end med_pessoa_fisica_AfterPost;
/
