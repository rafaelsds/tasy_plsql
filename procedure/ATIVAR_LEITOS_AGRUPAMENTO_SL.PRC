create or replace
procedure Ativar_leitos_agrupamento_sl (	cd_setor_atendimento_p		number,
											cd_unidade_basica_p		varchar2,
											cd_unidade_compl_p		varchar2,
											nm_usuario_p			varchar2) is

nr_agrupamento_w			number(5);
qt_unidade_ocupado_w		number(5);
ie_atualizar_leitos_temp_w	varchar2(1);


begin
Obter_Param_Usuario(75, 42, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_atualizar_leitos_temp_w);

select	max(nr_agrupamento)
into	nr_agrupamento_w
from	unidade_atendimento
where	cd_setor_atendimento 	= cd_setor_atendimento_p
and	cd_unidade_basica	= cd_unidade_basica_p
and	cd_unidade_compl	= cd_unidade_compl_p
and	ie_inativar_leito_temp  = 'S'
and	ie_temporario		= 'S';


if 	(nr_agrupamento_w is not null) then

	update 	unidade_atendimento
	set	   	ie_situacao 	= 'A',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao	= sysdate
	where  	cd_setor_atendimento = cd_setor_atendimento_p
	and	   	nr_agrupamento = nr_agrupamento_w
	and	   	(cd_unidade_basica <> cd_unidade_basica_p
			or cd_unidade_compl <> cd_unidade_compl_p)
	and 	( (ie_atualizar_leitos_temp_w = 'S')
			or ((ie_atualizar_leitos_temp_w = 'T') 
				and (nvl(ie_temporario,'N') <> 'S')));
	
end if;

commit;
end Ativar_leitos_agrupamento_sl;
/
