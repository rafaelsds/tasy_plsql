create or replace procedure        
gerar_registro_problema(	nr_sequencia_p 	number,                    
							ie_main_enc_probl_p varchar2) is

nr_atendimento_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
ie_tipo_diagnostico_w	number(3);
cd_doenca_w				varchar2(10);
cd_ciap_w				varchar2(10);
cd_medico_w				varchar2(10);
nm_usuario_w			varchar2(15);
ds_problema_w			varchar2(4000);

nr_seq_mentor_w			number(10);
nr_regras_atendidas_w 	varchar2(2000);
ie_gera_prot_assist		boolean := False;

nr_seq_registro_w 		lista_problema_pac_item.nr_seq_registro%type;
nr_seq_historico_w		lista_problema_pac.nr_seq_historico%type;
nr_seq_tipo_hist_w		lista_problema_pac.nr_seq_tipo_hist%type;
nr_seq_apresen_esp_w	lista_problema_pac.nr_seq_apresen_esp%type;
nm_tabela_w				tipo_historico_saude.nm_tabela%type;
ds_erro_w				varchar2(4000);

begin


if (nvl(nr_sequencia_p,0) > 0 ) then

	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

	Select 	max(nr_atendimento),
			max(cd_pessoa_fisica),
			max(cd_doenca),
			max(cd_ciap),
        	max(nvl(ie_tipo_diagnostico,2)),
			max(ds_problema),
			max(nr_seq_historico),
			max(nr_seq_tipo_hist),
			max(nr_seq_apresen_esp)
	into	nr_atendimento_w,
			cd_pessoa_fisica_w,
			cd_doenca_w,
			cd_ciap_w,
			ie_tipo_diagnostico_w,
			ds_problema_w,
			nr_seq_historico_w,
			nr_seq_tipo_hist_w,
			nr_seq_apresen_esp_w
	from	lista_problema_pac
	where	nr_sequencia = nr_sequencia_p;

	select  Obter_Pf_Usuario(nm_usuario_w,'C')
	into	cd_medico_w
	from	dual;

	if (cd_doenca_w is not null) then
		if (cd_medico_w is not null) then
			Gerar_diagnostico_problema(nr_sequencia_p,nr_atendimento_w,cd_doenca_w,cd_medico_w,nm_usuario_w,ie_tipo_diagnostico_w,'S',sysdate,ie_main_enc_probl_p);

			select	nvl(max(nr_seq_registro),0)
			into	nr_seq_registro_w
			from	lista_problema_pac_item a
			where	a.nr_seq_problema	= nr_sequencia_p
			and		ie_tipo_registro	= 'DM';

			if (nr_seq_registro_w <> 0) then
				GQA_LIBERACAO_DIAGNOSTICO(nr_seq_registro_w, nm_usuario_w, nr_seq_mentor_w, nr_regras_atendidas_w);
				ie_gera_prot_assist := True;
			end if;
		end if;
	end if;

	if (cd_ciap_w is not null) then
		gerar_ciap_problema(nr_sequencia_p, nr_atendimento_w, cd_ciap_w, cd_medico_w, nm_usuario_w, ds_problema_w);

		select	nvl(max(nr_seq_registro),0)
		into	nr_seq_registro_w
		from	lista_problema_pac_item a
		where	a.nr_seq_problema	= nr_sequencia_p
		and		ie_tipo_registro	= 'CP';

		if (nr_seq_registro_w <> 0) then
			GQA_Liberacao_ciap(nr_seq_registro_w, nm_usuario_w, nr_seq_mentor_w, nr_regras_atendidas_w);
			ie_gera_prot_assist := True;
		end if;
	end if;

	if (nr_seq_historico_w is not null and nr_seq_tipo_hist_w is not null) then
		select	nm_tabela
		into	nm_tabela_w
		from	tipo_historico_saude
		where	nr_sequencia = nr_seq_tipo_hist_w;

		gravar_historico_saude(nm_tabela_w, nr_seq_historico_w, cd_pessoa_fisica_w, 'S', nr_atendimento_w, null, nm_usuario_w, nr_seq_registro_w, ds_erro_w);

		if (ds_erro_w is null) then
			insert into lista_problema_pac_item (	nr_sequencia,
												dt_atualizacao,
												dt_atualizacao_nrec,
												nm_usuario,
												nm_usuario_nrec,
												nr_seq_problema,
												nr_seq_registro,
												ie_tipo_registro,
												nm_tabela,
												ie_tipo_problema,
												nr_seq_tipo_hist )
									values(		lista_problema_pac_item_seq.NextVal,
												sysdate,
												sysdate,
												nm_usuario_w,
												nm_usuario_w,
												nr_sequencia_p,
												nr_seq_registro_w,
												'HS',
												nm_tabela_w,
												nr_seq_historico_w,
												nr_seq_tipo_hist_w);
		end if;
	end if;

	gerar_fatos_problema(nr_sequencia_p,nm_usuario_w);
	gerar_especialidade_problema(nr_sequencia_p,cd_pessoa_fisica_w,nm_usuario_w,nr_seq_apresen_esp_w,'S');

	if ie_gera_prot_assist then
		gera_protocolo_assistencial(nr_atendimento_w, nm_usuario_w);
	end if;

end if;


end gerar_registro_problema;
/