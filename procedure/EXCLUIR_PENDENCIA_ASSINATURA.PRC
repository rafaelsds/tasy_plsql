create or replace 
procedure EXCLUIR_PENDENCIA_ASSINATURA(	dt_parametro_p	date,
					nm_usuario_p	varchar2) is


begin

--- Avalia��o
delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.nr_seq_avaliacao is not null
and    exists ( Select 1
       	        from   MED_AVALIACAO_PACIENTE b
	        where    a.nr_seq_avaliacao = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


--- Avalia��o Analgesia
delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.NR_SEQ_ATEND_AVAL_ANALGESIA is not null
and    exists ( Select 1
       	        from   ATEND_AVAL_ANALGESIA b
	        where    a.NR_SEQ_ATEND_AVAL_ANALGESIA = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


--- SV Monit Resp
delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.nr_seq_atend_monit_resp is not null
and    exists ( Select 1
       	        from   atendimento_monit_resp b
	        where    a.nr_seq_atend_monit_resp = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;

--- SV Monit Hemo
delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.NR_SEQ_ATEND_MONIT_HEMO is not null
and    exists ( Select 1
       	        from   Atend_monit_hemod b
	        where    a.NR_SEQ_ATEND_MONIT_HEMO = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


--- Template
delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.NR_SEQ_EHR_REG is not null
and    exists ( Select 1
       	        from   ehr_registro b
	        where    a.NR_SEQ_EHR_REG = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


--- Diagnostico Tumor

delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.nr_seq_loco_reg is not null
and    exists ( Select 1
       	        from   CAN_LOCO_REGIONAL b
	        where    a.nr_seq_loco_reg = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;

--- Evolu��o

delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.cd_evolucao is not null
and    exists ( Select 1
       	        from   evolucao_paciente b
	        where    a.cd_evolucao = b.cd_evolucao
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


--- Ganhos e Perdas.

delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.NR_SEQ_PERDA_GANHO is not null
and    exists ( Select 1
       	        from   ATENDIMENTO_PERDA_GANHO b
	        where    a.NR_SEQ_PERDA_GANHO = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


--- Sinais Vitais

delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.NR_SEQ_SINAL_VITAL is not null
and    exists ( Select 1
       	        from   ATENDIMENTO_SINAL_VITAL b
	        where    a.NR_SEQ_SINAL_VITAL = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;



--- Boletins informativos

delete from   pep_item_pendente a      
where  a.ie_tipo_pendencia = 'A'
and    a.nr_seq_registro is not null
and    a.IE_TIPO_REGISTRO = 'LBI'
and    exists ( Select 1
       	        from   ATENDIMENTO_BOLETIM b
	        where    a.nr_seq_registro = b.nr_sequencia
	        and    OBTER_DATA_ASSINATURA_DIGITAL(b.nr_seq_assinatura) is not null);			
			
commit;


end;
/