create or replace
procedure gpi_liberar_estudo_viab(	nr_sequencia_p	number,
				ie_operacao_p	varchar2,
				nm_usuario_p	Varchar2) is 

/* IE_OPERACAO_P
L - Liberar - marca dt_liberacao
E - Estorna a libera��o, passa dt_liberacao para null
*/
begin

if	(ie_operacao_p = 'L') then
	update	gpi_estudo_viab
	set	dt_liberacao	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
elsif	(ie_operacao_p = 'E') then
	update	gpi_estudo_viab
	set	dt_liberacao	= null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
end if;
commit;

end gpi_liberar_estudo_viab;
/