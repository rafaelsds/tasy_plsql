create or replace procedure update_by_pass_so(	nr_sequencia_p		number,
												nr_seq_ordem_serv_p	number
											) is

begin

	begin

	update   coe_by_pass_analysis
	set      nr_seq_service_order   = nr_seq_ordem_serv_p
    where    nr_sequencia   		= nr_sequencia_p;

	end;

commit;

end update_by_pass_so;
/