create or replace
procedure oft_obter_microscopia (	nr_seq_consulta_p			number,
												nr_seq_consulta_form_p	number,
												cd_pessoa_fisica_p		varchar2,
												ie_opcao_p					varchar2,
												vListaMicroscopia			in out strRecTypeFormOft) is

dt_exame_w							oft_microscopia_especular.dt_registro%type;
ds_observacao_w					oft_microscopia_especular.ds_observacao%type;
qt_od_microscopia_w				oft_microscopia_especular.qt_od_microscopia%type;
qt_oe_microscopia_w				oft_microscopia_especular.qt_oe_microscopia%type;
qt_od_microscopia_sup_w			oft_microscopia_especular.qt_od_microscopia_sup%type;
qt_od_microscopia_temp_w		oft_microscopia_especular.qt_od_microscopia_temp%type;
qt_od_microscopia_central_w	oft_microscopia_especular.qt_od_microscopia_central%type;
qt_od_microscopia_nasal_w		oft_microscopia_especular.qt_od_microscopia_nasal%type;
qt_od_microscopia_inf_w			oft_microscopia_especular.qt_od_microscopia_inf%type;
qt_oe_microscopia_sup_w			oft_microscopia_especular.qt_oe_microscopia_sup%type;
qt_oe_microscopia_temp_w		oft_microscopia_especular.qt_oe_microscopia_temp%type;
qt_oe_microscopia_nasal_w		oft_microscopia_especular.qt_oe_microscopia_nasal%type;
qt_oe_microscopia_inf_w			oft_microscopia_especular.qt_oe_microscopia_inf%type;
qt_oe_microscopia_central_w	oft_microscopia_especular.qt_oe_microscopia_central%type;
qt_oe_espessura_sup_w			oft_microscopia_especular.qt_oe_espessura_sup%type;
qt_oe_espessura_nasal_w			oft_microscopia_especular.qt_oe_espessura_nasal%type;
qt_oe_espessura_central_w		oft_microscopia_especular.qt_oe_espessura_central%type;
qt_oe_espessura_temp_w			oft_microscopia_especular.qt_oe_espessura_temp%type;
qt_oe_espessura_inf_w			oft_microscopia_especular.qt_oe_espessura_inf%type;
qt_oe_espessura_w					oft_microscopia_especular.qt_oe_espessura%type;
qt_od_espessura_sup_w			oft_microscopia_especular.qt_od_espessura_sup%type;
qt_od_espessura_temp_w			oft_microscopia_especular.qt_od_espessura_temp%type;
qt_od_espessura_central_w		oft_microscopia_especular.qt_od_espessura_central%type;
qt_od_espessura_nasal_w			oft_microscopia_especular.qt_od_espessura_nasal%type;
qt_od_espessura_inf_w			oft_microscopia_especular.qt_od_espessura_inf%type;
qt_od_espessura_w					oft_microscopia_especular.qt_od_espessura%type;
cd_profissional_w					oft_microscopia_especular.cd_profissional%TYPE;
dt_liberacao_w						date;
cd_estabelecimento_w				estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_w						usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
ds_erro_w							varchar2(4000);

cursor	microscopia_especular_form is
	select	a.*
	from		oft_microscopia_especular a,
				oft_consulta_formulario b
	where		a.nr_seq_consulta_form 	=	b.nr_sequencia
	and		a.nr_seq_consulta_form 	=	nr_seq_consulta_form_p
	and		a.nr_seq_consulta			=	nr_seq_consulta_p
	and		((a.dt_liberacao is not null) or (a.nm_usuario = nm_usuario_w))
	and		((a.dt_inativacao is null) or (b.dt_inativacao is not null))
	order by dt_registro;
	
cursor	microscopia_especular_paciente is
	select	a.*
	from		oft_microscopia_especular a,
				oft_consulta b
	where		a.nr_seq_consulta		=	b.nr_sequencia
	and		b.cd_pessoa_fisica	=	cd_pessoa_fisica_p
	and		a.dt_liberacao 		is not null
	and		a.dt_inativacao 		is null
	and		b.nr_sequencia 		<> nr_seq_consulta_p
	order by dt_registro;
	

											
begin
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaMicroscopia.count > 0) then
	if	(ie_opcao_p = 'F') then
		FOR c_microscopia_especular IN microscopia_especular_form LOOP
			begin
			dt_exame_w							:= c_microscopia_especular.dt_registro;
			ds_observacao_w					:= c_microscopia_especular.ds_observacao;
			qt_od_microscopia_w				:= c_microscopia_especular.qt_od_microscopia;
			qt_oe_microscopia_w				:= c_microscopia_especular.qt_oe_microscopia;
			qt_od_microscopia_sup_w			:= c_microscopia_especular.qt_od_microscopia_sup;
			qt_od_microscopia_temp_w		:= c_microscopia_especular.qt_od_microscopia_temp;
			qt_od_microscopia_central_w	:= c_microscopia_especular.qt_od_microscopia_central;
			qt_od_microscopia_nasal_w		:= c_microscopia_especular.qt_od_microscopia_nasal;
			qt_od_microscopia_inf_w			:= c_microscopia_especular.qt_od_microscopia_inf;
			qt_oe_microscopia_sup_w			:= c_microscopia_especular.qt_oe_microscopia_sup;
			qt_oe_microscopia_temp_w		:= c_microscopia_especular.qt_oe_microscopia_temp;
			qt_oe_microscopia_nasal_w		:= c_microscopia_especular.qt_oe_microscopia_nasal;
			qt_oe_microscopia_inf_w			:= c_microscopia_especular.qt_oe_microscopia_inf;
			qt_oe_microscopia_central_w	:= c_microscopia_especular.qt_oe_microscopia_central;
			qt_oe_espessura_sup_w			:= c_microscopia_especular.qt_oe_espessura_sup;
			qt_oe_espessura_nasal_w			:= c_microscopia_especular.qt_oe_espessura_nasal;
			qt_oe_espessura_central_w		:= c_microscopia_especular.qt_oe_espessura_central;
			qt_oe_espessura_temp_w			:= c_microscopia_especular.qt_oe_espessura_temp;
			qt_oe_espessura_inf_w			:= c_microscopia_especular.qt_oe_espessura_inf;
			qt_oe_espessura_w					:= c_microscopia_especular.qt_oe_espessura;
			qt_od_espessura_sup_w			:= c_microscopia_especular.qt_od_espessura_sup;
			qt_od_espessura_temp_w			:= c_microscopia_especular.qt_od_espessura_temp;
			qt_od_espessura_central_w		:= c_microscopia_especular.qt_od_espessura_central;
			qt_od_espessura_nasal_w			:= c_microscopia_especular.qt_od_espessura_nasal;
			qt_od_espessura_inf_w			:= c_microscopia_especular.qt_od_espessura_inf;
			qt_od_espessura_w					:= c_microscopia_especular.qt_od_espessura;
			dt_liberacao_w						:=	c_microscopia_especular.dt_liberacao;
			cd_profissional_w					:=	c_microscopia_especular.cd_profissional;
			end;
		end loop;	
	else
		FOR c_microscopia_especular IN microscopia_especular_paciente LOOP
			begin
			ds_observacao_w					:= c_microscopia_especular.ds_observacao;
			qt_od_microscopia_w				:= c_microscopia_especular.qt_od_microscopia;
			qt_oe_microscopia_w				:= c_microscopia_especular.qt_oe_microscopia;
			qt_od_microscopia_sup_w			:= c_microscopia_especular.qt_od_microscopia_sup;
			qt_od_microscopia_temp_w		:= c_microscopia_especular.qt_od_microscopia_temp;
			qt_od_microscopia_central_w	:= c_microscopia_especular.qt_od_microscopia_central;
			qt_od_microscopia_nasal_w		:= c_microscopia_especular.qt_od_microscopia_nasal;
			qt_od_microscopia_inf_w			:= c_microscopia_especular.qt_od_microscopia_inf;
			qt_oe_microscopia_sup_w			:= c_microscopia_especular.qt_oe_microscopia_sup;
			qt_oe_microscopia_temp_w		:= c_microscopia_especular.qt_oe_microscopia_temp;
			qt_oe_microscopia_nasal_w		:= c_microscopia_especular.qt_oe_microscopia_nasal;
			qt_oe_microscopia_inf_w			:= c_microscopia_especular.qt_oe_microscopia_inf;
			qt_oe_microscopia_central_w	:= c_microscopia_especular.qt_oe_microscopia_central;
			qt_oe_espessura_sup_w			:= c_microscopia_especular.qt_oe_espessura_sup;
			qt_oe_espessura_nasal_w			:= c_microscopia_especular.qt_oe_espessura_nasal;
			qt_oe_espessura_central_w		:= c_microscopia_especular.qt_oe_espessura_central;
			qt_oe_espessura_temp_w			:= c_microscopia_especular.qt_oe_espessura_temp;
			qt_oe_espessura_inf_w			:= c_microscopia_especular.qt_oe_espessura_inf;
			qt_oe_espessura_w					:= c_microscopia_especular.qt_oe_espessura;
			qt_od_espessura_sup_w			:= c_microscopia_especular.qt_od_espessura_sup;
			qt_od_espessura_temp_w			:= c_microscopia_especular.qt_od_espessura_temp;
			qt_od_espessura_central_w		:= c_microscopia_especular.qt_od_espessura_central;
			qt_od_espessura_nasal_w			:= c_microscopia_especular.qt_od_espessura_nasal;
			qt_od_espessura_inf_w			:= c_microscopia_especular.qt_od_espessura_inf;
			qt_od_espessura_w					:= c_microscopia_especular.qt_od_espessura;
			cd_profissional_w					:=	obter_pf_usuario(nm_usuario_w,'C');
			dt_exame_w							:= sysdate;
			end;
		end loop;	
	end if;	
	
	for i in 1..vListaMicroscopia.count loop
		begin
		if	(ie_opcao_p = 'F') or (vListaMicroscopia(i).ie_obter_resultado = 'S') then
			vListaMicroscopia(i).dt_liberacao	:= dt_liberacao_w;
			case upper(vListaMicroscopia(i).nm_campo)
				WHEN 'CD_PROFISSIONAL' THEN
					vListaMicroscopia(i).ds_valor	:= cd_profissional_w;
				when 'DT_REGISTRO' then
					vListaMicroscopia(i).dt_valor	:= dt_exame_w;
				when 'DS_OBSERVACAO' then
					vListaMicroscopia(i).ds_valor	:= ds_observacao_w;
				when 'QT_OD_MICROSCOPIA' then
					vListaMicroscopia(i).nr_valor	:= qt_od_microscopia_w;
				when 'QT_OE_MICROSCOPIA' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_microscopia_w;
				when 'QT_OD_MICROSCOPIA_SUP' then
					vListaMicroscopia(i).nr_valor	:= qt_od_microscopia_sup_w;
				when 'QT_OD_MICROSCOPIA_TEMP' then
					vListaMicroscopia(i).nr_valor	:= qt_od_microscopia_temp_w;
				when 'QT_OD_MICROSCOPIA_CENTRAL' then
					vListaMicroscopia(i).nr_valor	:= qt_od_microscopia_central_w;
				when 'QT_OD_MICROSCOPIA_NASAL' then
					vListaMicroscopia(i).nr_valor	:= qt_od_microscopia_nasal_w;
				when 'QT_OD_MICROSCOPIA_INF' then
					vListaMicroscopia(i).nr_valor	:= qt_od_microscopia_inf_w;
				when 'QT_OE_MICROSCOPIA_SUP' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_microscopia_sup_w;
				when 'QT_OE_MICROSCOPIA_TEMP' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_microscopia_temp_w;
				when 'QT_OE_MICROSCOPIA_NASAL' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_microscopia_nasal_w;
				when 'QT_OE_MICROSCOPIA_INF' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_microscopia_inf_w;
				when 'QT_OE_MICROSCOPIA_CENTRAL' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_microscopia_central_w;
				when 'QT_OE_ESPESSURA_SUP' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_espessura_sup_w;
				when 'QT_OE_ESPESSURA_NASAL' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_espessura_nasal_w;
				when 'QT_OE_ESPESSURA_CENTRAL' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_espessura_central_w;
				when 'QT_OE_ESPESSURA_TEMP' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_espessura_temp_w;
				when 'QT_OE_ESPESSURA_INF' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_espessura_inf_w;
				when 'QT_OE_ESPESSURA' then
					vListaMicroscopia(i).nr_valor	:= qt_oe_espessura_w;
				when 'QT_OD_ESPESSURA_SUP' then
					vListaMicroscopia(i).nr_valor	:= qt_od_espessura_sup_w;
				when 'QT_OD_ESPESSURA_TEMP' then
					vListaMicroscopia(i).nr_valor	:= qt_od_espessura_temp_w;
				when 'QT_OD_ESPESSURA_CENTRAL' then
					vListaMicroscopia(i).nr_valor	:= qt_od_espessura_central_w;
				when 'QT_OD_ESPESSURA_NASAL' then
					vListaMicroscopia(i).nr_valor	:= qt_od_espessura_nasal_w;
				when 'QT_OD_ESPESSURA_INF' then
					vListaMicroscopia(i).nr_valor	:= qt_od_espessura_inf_w;
				when 'QT_OD_ESPESSURA' then
					vListaMicroscopia(i).nr_valor	:= qt_od_espessura_w;
				else
					null;	
			end case;	
		end if;	
	end;
	end loop;
end if;

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
end;

end oft_obter_microscopia;
/