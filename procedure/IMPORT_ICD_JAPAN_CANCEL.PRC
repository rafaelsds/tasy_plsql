create or replace PROCEDURE        IMPORT_ICD_JAPAN_CANCEL (nm_usuario_p varchar2,return_value OUT VARCHAR2)
IS
    modifier_w   NUMBER(10);
    index_w      NUMBER(10);
    icd_code_w   NUMBER(10);
begin
IF ( obtain_user_locale(nm_usuario_p) = 'ja_JP' ) THEN
obter_valor_dinamico('select count(*) from icd_main_jpn', icd_code_w);
obter_valor_dinamico('select count(*) from icd_modifier_jpn', modifier_w);
obter_valor_dinamico('select count(*) from icd_index_jpn', index_w);
END IF;

IF (modifier_w > 0) OR (index_w > 0) OR ( icd_code_w > 0) THEN
    EXECUTE IMMEDIATE 'truncate table TASY.icd_main_jpn';
    EXECUTE IMMEDIATE 'truncate table TASY.icd_modifier_jpn';
    EXECUTE IMMEDIATE 'truncate table TASY.icd_index_jpn';
     return_value := 'S';
ELSE
     return_value := 'N';
END IF;

end IMPORT_ICD_JAPAN_CANCEL; 
/
