create or replace
procedure mprev_gerar_alerta_evento(cd_estabelecimento_p 	number,
									dt_inicio_p				date,
									dt_fim_p				date,
									nm_usuario_p			varchar2) is

nr_seq_pacticipante_w		number(10);
cd_pessoa_fisica_w			varchar2(10);
nr_seq_evento_externo_w		number(10);
dt_evento_w					date;
ds_prestador_exec_w			varchar2(255);
ds_medico_solic_w			varchar2(255);
ds_tipo_atendimento_w		varchar2(100);
ds_carater_internacao_w		varchar2(100);
nr_seq_guia_plano_w			number(10);
nr_seq_requisicao_w			number(10);
ds_procedimento_w			varchar2(255);
qt_registro_regra_w			number(5);
cd_perfil_dest_w			number(5);
ds_perfis_dest_w			varchar2(4000);
nm_usuario_dest_w			varchar2(15);
nm_usuarios_dest_w			varchar2(4000);
ds_quebra_w					varchar2(10) := chr(13)||chr(10);
ds_tab_w					varchar2(10) := chr(9);
ds_comunicacao_w			varchar2(32000);
ie_email_w					mprev_regra_evento_ext_des.ie_email%type;
ie_comunic_interna_w		mprev_regra_evento_ext_des.ie_comunic_interna%type;
ds_email_origem_w			varchar2(255);
ds_email_destino_w			varchar2(255);

Cursor C01 is
	select	a.nr_sequencia,
			a.cd_pessoa_fisica
	from	mprev_participante a
	where	a.ie_situacao = 'A';

Cursor C02 is
	select	a.nr_sequencia,
			a.dt_evento,
			a.ds_prestador_exec,
			a.ds_medico_solic,
			substr(obter_valor_dominio(1761,a.ie_tipo_atend_tiss),1,100),
			substr(obter_valor_dominio(1016,a.ie_carater_internacao),1,100),
			nr_seq_guia_plano,
			nr_seq_requisicao
	from	w_evento_externo_paciente a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
	and		nm_usuario = nm_usuario_p
	order by	a.dt_evento;

Cursor C03 is	
	select	substr(nvl(pls_obter_conv_serv_extratmens(null,a.cd_procedimento,a.ie_origem_proced,null,null,null,null),
			obter_desc_procedimento(a.cd_procedimento,a.ie_origem_proced)),1,255) /* Francisco - 27/11/2014 - Ocultar procedimentos conforme mensalidade */
	from	w_evento_externo_pac_proc a
	where	a.nr_seq_evento_externo = nr_seq_evento_externo_w;

Cursor C04 is	
	select	a.cd_perfil,
			a.nm_usuario_destino,
			a.ie_email,
			a.ie_comunic_interna	
	from	mprev_regra_evento_ext_des a
	where	a.nr_seq_regra_evento in (
				select	b.nr_sequencia
				from	mprev_regra_evento_externo b,
						w_evento_externo_paciente c
				where	((nvl(b.ie_tipo_guia,'0') = '0')          	or (nvl(b.ie_tipo_guia,'0') = nvl(c.ie_tipo_guia,'0')))
				and		((nvl(b.ie_tipo_atend_tiss,'0') = '0')    	or (nvl(b.ie_tipo_atend_tiss,'0') = nvl(c.ie_tipo_atend_tiss,'0')))
				and		((nvl(b.ie_carater_internacao,'0') = '0') 	or (nvl(b.ie_carater_internacao,'0') = nvl(c.ie_carater_internacao,'0')))
				and		((nvl(b.nr_seq_clinica,'0') = '0')        	or (nvl(b.nr_seq_clinica,'0') = nvl(c.nr_seq_clinica,'0')))
				and		((nvl(b.ie_req_executada,'N') = 'N')		or (nvl(b.ie_req_executada,'N') = nvl(c.ie_req_executada,'N')))
				AND     ((NVL(b.NR_SEQ_PROGRAMA,'0') = '0')  		or (MPrev_Obter_Se_Programa(c.cd_pessoa_fisica, B.NR_SEQ_PROGRAMA) = 'S'))
				AND     ((NVL(b.NR_SEQ_PROG_MODULO,'0') = '0')  	or (MPrev_Obter_Se_Modulo(c.cd_pessoa_fisica, B.NR_SEQ_PROG_MODULO) = 'S'))
				AND     ((NVL(b.NR_SEQ_FORMA_INGRESSO,'0') = '0')  	or (MPrev_Obter_Se_Forma_Ingresso(c.cd_pessoa_fisica, B.NR_SEQ_FORMA_INGRESSO) = 'S'))
				and		c.cd_pessoa_fisica = cd_pessoa_fisica_w
				and		c.nm_usuario = nm_usuario_p);	
begin

/* Limpa tabelas W */
delete	w_evento_externo_pac_proc
where	nm_usuario = nm_usuario_p;

delete	w_evento_externo_pac_mat
where	nm_usuario = nm_usuario_p;

delete	w_evento_externo_paciente
where	nm_usuario = nm_usuario_p;

/* Abre cursor com todos particimantes ativos */
open C01;
loop
fetch C01 into
	nr_seq_pacticipante_w,
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
	obter_param_usuario(0,38,Obter_perfil_Ativo,nm_usuario_p,cd_estabelecimento_p,ds_email_origem_w);
	gerar_eventos_externos_pac(dt_inicio_p,dt_fim_p,cd_pessoa_fisica_w,'A','S',nm_usuario_p);

	ds_comunicacao_w := Wheb_mensagem_pck.get_texto(308786) || ': ' /*'Paciente: '*/ || substr(obter_nome_pf(cd_pessoa_fisica_w),1,255) || ds_quebra_w;
	ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308787) || ' '  /*'Idade: '*/ || substr(obter_dados_pf(cd_pessoa_fisica_w,'I') || ' ' || Wheb_mensagem_pck.get_texto(308788) /*' anos'*/,1,25) ||
	ds_tab_w || ds_tab_w || Wheb_mensagem_pck.get_texto(308790) || ': ' /*'Sexo: '*/ || substr(obter_valor_dominio(4,obter_dados_pf(cd_pessoa_fisica_w,'SE')),1,25) || ds_quebra_w;
	ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308791) || ' ' /*'Programas: '*/ || substr(mprev_obter_dados_partic(nr_seq_pacticipante_w,sysdate,'PR',cd_pessoa_fisica_w),1,255) || ds_quebra_w || ds_quebra_w;

	ds_comunicacao_w := ds_comunicacao_w || '--------------------------------------------------------------------------------' || ds_quebra_w;

	open C02;
	loop
	fetch C02 into	
		nr_seq_evento_externo_w,
		dt_evento_w,
		ds_prestador_exec_w,
		ds_medico_solic_w,
		ds_tipo_atendimento_w,
		ds_carater_internacao_w,
		nr_seq_guia_plano_w,
		nr_seq_requisicao_w;
	exit when C02%notfound;
		begin
		
		if (nr_seq_guia_plano_w is not null) then
			ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308792) || ': ' ||/*'Dados da autoriza��o: '*/ nr_seq_guia_plano_w || ds_quebra_w;
		elsif (nr_seq_requisicao_w is not null) then
			ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308793) || ': ' ||/*'Dados da requisi��o: '*/ nr_seq_requisicao_w || ds_quebra_w;
		end if;

		ds_comunicacao_w := ds_comunicacao_w || '--------------------------------------------------------------------------------' || ds_quebra_w;
		ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308794) || ' ' /*'Data: '*/ || to_char(dt_evento_w,'dd/mm/yyyy hh24:mi:ss') || ds_quebra_w;
		ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308795) || ' ' /*'Prestador: '*/ || ds_prestador_exec_w || ds_quebra_w;
		ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308796) || ': ' /*'M�dico solicitante: '*/ || ds_medico_solic_w || ds_quebra_w;
		ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308797) || ' ' /*'Tipo de atendimento: '*/ || ds_tipo_atendimento_w || ds_quebra_w;
		ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308798) || ': ' /*'Car�ter: '*/ || ds_carater_internacao_w || ds_quebra_w;
		ds_comunicacao_w := ds_comunicacao_w || Wheb_mensagem_pck.get_texto(308799) || ' ' /*'Procedimentos: '*/ || ds_quebra_w;

		open C03;
		loop
		fetch C03 into	
			ds_procedimento_w;
		exit when C03%notfound;
			begin
			ds_comunicacao_w := ds_comunicacao_w || '- ' || ds_procedimento_w || ds_quebra_w;
			end;
		end loop;
		close C03;

		ds_comunicacao_w := ds_comunicacao_w || '--------------------------------------------------------------------------------' || ds_quebra_w;
		end;
	end loop;
	close C02;

	qt_registro_regra_w := 0;
	ds_perfis_dest_w := '';
	nm_usuarios_dest_w := '';

	open C04;
	loop
	fetch C04 into	
		cd_perfil_dest_w,
		nm_usuario_dest_w,
		ie_email_w,
		ie_comunic_interna_w;
	exit when C04%notfound;
		begin
		if	(ie_comunic_interna_w = 'S') then
			ds_perfis_dest_w := ds_perfis_dest_w || cd_perfil_dest_w || ',';
			nm_usuarios_dest_w := nm_usuarios_dest_w || nm_usuario_dest_w || ',';
			qt_registro_regra_w := qt_registro_regra_w + 1;
		end if;
		if	(ie_email_w = 'S') then	
			ds_email_destino_w 	:= 	obter_dados_usuario_opcao(nm_usuario_dest_w,'E');
			if	(ds_email_origem_w is not null) and
				(ds_email_destino_w is not null) then
				enviar_email(	Wheb_mensagem_pck.get_texto(308800),
						ds_comunicacao_w,
						ds_email_origem_w,
						ds_email_destino_w,
						nm_usuario_p,
						'M');
			end if;
		end if;
		end;
	end loop;
	close C04;
	if 	(qt_registro_regra_w > 0) then
		insert into comunic_interna (
			nr_sequencia,
			cd_estab_destino,
			ds_titulo,
			dt_comunicado,
			ds_comunicado,
			nm_usuario,
			nm_usuario_destino,
			dt_atualizacao,
			ie_geral,
			ie_gerencial,
			ds_perfil_adicional,
			dt_liberacao)
		values (
			comunic_interna_seq.nextval,
			cd_estabelecimento_p,
			Wheb_mensagem_pck.get_texto(308800), /*'Evento externo - Paciente da Medicina Preventiva',*/
			sysdate,
			ds_comunicacao_w,
			nm_usuario_p,
			nm_usuarios_dest_w,
			sysdate,
			'N',
			'N',
			ds_perfis_dest_w,
			sysdate);
			
	end if;
	end;
end loop;
close C01;

commit;

end mprev_gerar_alerta_evento;
/