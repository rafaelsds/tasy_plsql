create or replace
procedure reverter_lote_beira_leito(
									nr_seq_lote_p		number,
									nm_usuario_p		Varchar2) is
									
nr_sequencia_w			number(10);

Cursor c01 is
	select	a.nr_sequencia
	from	prescr_mat_hor a,
			prescr_material b
	where	a.nr_prescricao 	= b.nr_prescricao
	and		a.nr_seq_material 	= b.nr_sequencia
  and   a.dt_fim_horario is null
	and		b.ie_agrupador		in (1,4,5,8,12)
	and		nr_seq_lote 		= nr_seq_lote_p;

begin

open c01;
	loop
	fetch c01 into	
		nr_sequencia_w;
	exit when c01%notfound;
		begin
		update	prescr_mat_hor
		set  	ie_checado_leito = 'N'
		where 	nr_sequencia  = nr_sequencia_w;
		
		commit;
		end;
	end loop;
close c01;

end reverter_lote_beira_leito;
/