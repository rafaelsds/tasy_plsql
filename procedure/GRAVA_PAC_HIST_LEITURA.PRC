create or replace
procedure grava_pac_hist_leitura(	nr_seq_historico_p	number,
					nm_usuario_p		varchar) 
					as
ie_insere_w	varchar2(1);					
					
begin

begin
select	decode(count(*),0,'S','N')
into	ie_insere_w
from	agenda_pac_hist_leitura
where	nr_seq_historico	= nr_seq_historico_p
and	nm_usuario_leitura	= nm_usuario_p;

if	(nr_seq_historico_p is not null) and
	(ie_insere_w = 'S') then
	insert	into agenda_pac_hist_leitura
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_historico,
		nm_usuario_leitura,
		dt_leitura)
		select agenda_pac_hist_leitura_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_seq_historico_p,
		nm_usuario_p,
		sysdate from dual;
	commit;
end if;
exception
	when others then
	ie_insere_w := 'N';
end;
	
end grava_pac_hist_leitura;
/
