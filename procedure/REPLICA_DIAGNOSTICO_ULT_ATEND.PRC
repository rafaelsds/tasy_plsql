create or replace
procedure replica_diagnostico_ult_atend(nr_atendimento_p	number,
					nm_usuario_p		Varchar2) is 

nr_atendimento_ant_w	number(10);
cd_pessoa_fisica_w	varchar2(60);

qt_registros_w		number(10);
ie_data_diag_atual	varchar2(1);
dt_atual_diagnostico_w	date;
ie_diagostico_tipo_atend_w varchar2(10);

--diagnostico medico
dt_diag_medico_w	date;
ie_tipo_diag_medico_w	number(3);
cd_medico_w		varchar2(10);
ds_diagnostico_w	varchar2(2000);
ie_tipo_doenca_med_w	varchar2(2);
ie_unidade_tempo_med_w	varchar2(2);
qt_tempo_med_w		number(15);
ie_tipo_atendimento_w	number(3);

--diagnostico doenca
dt_diag_doenca_w		date;
cd_doenca_w			varchar2(10);
ds_diagnostico_doenca_w		varchar2(2000);
ie_classificacao_doenca_w	varchar2(1);
ie_tipo_doenca_w		varchar2(2);
ie_unidade_tempo_w		varchar2(2);	
qt_tempo_w			number(15);
ie_lado_w			varchar(1);
dt_manifestacao_w		date;
nr_seq_diag_interno_w		number(20);
nr_seq_grupo_diag_w		number(10);
dt_inicio_w			date;
dt_fim_w			date;
dt_liberacao_w			date;
cd_perfil_ativo_w		number(5);
ie_gera_somente_pa_w		varchar2(1);
ie_tipo_diag_doenca_w		number(3);
ie_situacao_c02_w		varchar2(1);



Cursor C01 is
	select	dt_diagnostico,
		ie_tipo_diagnostico,
		cd_medico,
		ds_diagnostico,
		ie_tipo_doenca,
		ie_unidade_tempo,
		qt_tempo,
		ie_tipo_atendimento
	from	diagnostico_medico
	where	nr_atendimento = nr_atendimento_ant_w;
	
Cursor C02 is
	select	dt_diagnostico,
		ie_tipo_diagnostico,	
		cd_doenca,
		ds_diagnostico,
		ie_classificacao_doenca,
		ie_tipo_doenca,
		ie_unidade_tempo,
		qt_tempo,
		ie_lado,
		dt_manifestacao,
		nr_seq_diag_interno,
		nr_seq_grupo_diag,
		dt_inicio,
		dt_fim,
		dt_liberacao,
		cd_perfil_ativo,
		ie_situacao
	from	diagnostico_doenca
	where	nr_atendimento 	= nr_atendimento_ant_w
	and	dt_diagnostico = dt_diag_medico_w;
			
begin

obter_param_usuario(916, 805, Obter_perfil_ativo, nm_usuario_p, 0, ie_gera_somente_pa_w);	
ie_gera_somente_pa_w := nvl(ie_gera_somente_pa_w,'N');
obter_param_usuario(916, 1109, Obter_perfil_ativo, nm_usuario_p, 0, ie_diagostico_tipo_atend_w);

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;


select 	nvl(max(a.nr_atendimento),0)
into	nr_atendimento_ant_w
from	atendimento_paciente a
where	a.nr_atendimento 	<> nr_atendimento_p
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
and	exists	(	
		select	1
		from	diagnostico_doenca z
		where	z.nr_atendimento	= a.nr_atendimento);				
if	(nr_atendimento_ant_w > 0)				and 
	(((ie_diagostico_tipo_atend_w is null) 	and 
	 (((ie_gera_somente_pa_w = 'S') 				and  
	 (obter_tipo_atendimento(nvl(nr_atendimento_ant_w,0)) = 3)) or
	 (ie_gera_somente_pa_w = 'N'))) or
	(OBTER_SE_CONTIDO((obter_tipo_atendimento(nvl(nr_atendimento_ant_w,0))),ie_diagostico_tipo_atend_w) = 'S')) then
	
	obter_param_usuario(916, 677, Obter_perfil_ativo, nm_usuario_p, 0, ie_data_diag_atual);
	
	
	dt_atual_diagnostico_w := sysdate+(1/1440);
		
	open C01;
		loop
	fetch C01 into	
		dt_diag_medico_w,
		ie_tipo_diag_medico_w,
		cd_medico_w,
		ds_diagnostico_w,
		ie_tipo_doenca_med_w,
		ie_unidade_tempo_med_w,
		qt_tempo_med_w,
		ie_tipo_atendimento_w;
	exit when C01%notfound;
		begin
		
		dt_atual_diagnostico_w := dt_atual_diagnostico_w +(1/1440);
		
		select	count(*)
		into	qt_registros_w
		from	diagnostico_medico
		where	nr_atendimento = nr_atendimento_p
		and	dt_diagnostico = decode(ie_data_diag_atual,'S',dt_atual_diagnostico_w,dt_diag_medico_w);
			
	if 	(qt_registros_w = 0) then
		
		insert into	diagnostico_medico 
				(
				nr_atendimento,
				dt_diagnostico,
				ie_tipo_diagnostico,
				cd_medico,
				dt_atualizacao,
				nm_usuario,
				ds_diagnostico,
				ie_tipo_doenca,
				ie_unidade_tempo,
				qt_tempo,
				ie_tipo_atendimento
				)
			values (
				nr_atendimento_p,
				decode(ie_data_diag_atual,'S',dt_atual_diagnostico_w,dt_diag_medico_w),
				ie_tipo_diag_medico_w,
				cd_medico_w,
				sysdate,
				nm_usuario_p,
				ds_diagnostico_w,
				ie_tipo_doenca_med_w,
				ie_unidade_tempo_med_w,
				qt_tempo_med_w,
				ie_tipo_atendimento_w);
				
			open C02;
			loop
			fetch C02 into	
				dt_diag_doenca_w,
				ie_tipo_diag_doenca_w,
				cd_doenca_w,
				ds_diagnostico_doenca_w,
				ie_classificacao_doenca_w,
				ie_tipo_doenca_w,
				ie_unidade_tempo_w,
				qt_tempo_w,
				ie_lado_w,
				dt_manifestacao_w,
				nr_seq_diag_interno_w,
				nr_seq_grupo_diag_w,
				dt_inicio_w,
				dt_fim_w,
				dt_liberacao_w,
				cd_perfil_ativo_w,
				ie_situacao_c02_w;
			exit when C02%notfound;
				begin
								
				insert into 	diagnostico_doenca
						(
						nr_atendimento,
						dt_diagnostico,
						cd_doenca,
						dt_atualizacao,
						nm_usuario,
						ds_diagnostico,
						ie_classificacao_doenca,
						ie_tipo_doenca,
						ie_unidade_tempo,
						qt_tempo,
						ie_lado,
						dt_manifestacao,
						nr_seq_diag_interno,
						nr_seq_grupo_diag,
						dt_inicio,
						dt_fim,
						dt_liberacao,
						cd_perfil_ativo,
						ie_tipo_diagnostico,
						ie_situacao
						)
					values (
						nr_atendimento_p,
						decode(ie_data_diag_atual,'S',dt_atual_diagnostico_w,dt_diag_doenca_w),
						cd_doenca_w,
						sysdate,
						nm_usuario_p,
						ds_diagnostico_doenca_w,
						ie_classificacao_doenca_w,
						ie_tipo_doenca_w,
						ie_unidade_tempo_w,
						qt_tempo_w,
						ie_lado_w,
						dt_manifestacao_w,
						nr_seq_diag_interno_w,
						nr_seq_grupo_diag_w,
						dt_inicio_w,
						dt_fim_w,
						dt_liberacao_w,
						cd_perfil_ativo_w,
						nvl(ie_tipo_diag_doenca_w,2),
						nvl(ie_situacao_c02_w,'A'));
				end;
			end loop;
			close C02;		
				
		end if;			
		end;
		
	end loop;
	close C01;
	
end if;


commit;

end replica_diagnostico_ult_atend;
/
