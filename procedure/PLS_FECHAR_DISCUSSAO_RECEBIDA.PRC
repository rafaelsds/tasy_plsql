create or replace
procedure pls_fechar_discussao_recebida(nr_seq_lote_disc_p		number,
					ie_estorno_p			varchar2,
					nm_usuario_p			varchar2,
					ie_atualizar_vl_contest_p	varchar2,
					cd_estabelecimento_p		varchar2) is 

nr_seq_lote_contest_w		number(10);
ie_envio_recebimento_w		varchar2(3);
nr_seq_acao_w			number(10);
nr_seq_fatura_w			number(10);
nr_seq_contestacao_w		number(10);
nr_seq_discussao_w		number(10);
qt_registro_w			number(10) := 0;
qt_reg_eve_w			number(10) := 0;
nr_seq_lote_evento_w		number(10);
nr_titulo_pag_w			number(10);
nr_titulo_rec_w			number(10);
ie_status_w			varchar2(3);
nr_seq_lote_conta_w		number(10);
nr_seq_pls_fatura_w		number(10);
ie_gerar_titulo_fat_ndr_w	varchar2(1) := 'S';
nr_titulo_receber_ndr_w		number(10);
nr_titulo_pagar_ndr_w		number(10);
nr_seq_camara_contest_w		number(10);
qt_inconsistente_w		number(10);
ie_parametro_12_w		funcao_param_usuario.vl_parametro%type;
ie_tipo_arquivo_disc_w		pls_lote_discussao.ie_tipo_arquivo%type;
nr_titulo_w			pls_fatura.nr_titulo%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;
qt_baixa_w			pls_integer;

begin
select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_p;

-- Parametro [5] - Permite desfazer fechamento do lote de discussao
if	(nvl(ie_estorno_p,'N') = 'N') then
	select	count(1)
	into	qt_registro_w
	from	pls_lote_protocolo_conta	c,
		pls_lote_discussao		b,
		pls_analise_conta		a
	where	c.nr_sequencia		= b.nr_seq_lote_conta
	and	a.nr_seq_lote_protocolo = c.nr_sequencia
	and	b.nr_sequencia		= nr_seq_lote_disc_p
	and	a.ie_status not in ('L','T');
	
	if	(qt_registro_w > 0) then
		-- Este lote de discussao nao pode ser fechado, pois ainda existe analise de conta pendente.
		wheb_mensagem_pck.exibir_mensagem_abort(143677);
		
	elsif	(nr_seq_lote_disc_p is not null) then
		select	max(a.nr_seq_lote_contest),
			max(a.ie_tipo_arquivo)
		into	nr_seq_lote_contest_w,
			ie_tipo_arquivo_disc_w
		from	pls_lote_discussao a
		where	a.nr_sequencia		= nr_seq_lote_disc_p;
		
		Obter_Param_Usuario(1334, 12, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_parametro_12_w);

		if	(nvl(ie_parametro_12_w, 'S') = 'N') then
			select	count(1)
			into	qt_inconsistente_w
			from	dual
			where	exists	(select	1
					from	pls_conta_proc		x,
						pls_contestacao_proc 	c,
						pls_contestacao		b,
						pls_lote_contestacao	a
					where	a.nr_sequencia	= b.nr_seq_lote
					and	b.nr_sequencia	= c.nr_seq_contestacao
					and	x.nr_sequencia	= c.nr_seq_conta_proc
					and	x.cd_procedimento is null
					and	a.nr_sequencia	= nr_seq_lote_contest_w
					union
					select	1
					from	pls_conta_mat		x,
						pls_contestacao_mat 	c,
						pls_contestacao		b,
						pls_lote_contestacao	a
					where	a.nr_sequencia	= b.nr_seq_lote
					and	b.nr_sequencia	= c.nr_seq_contestacao
					and	x.nr_sequencia	= c.nr_seq_conta_mat
					and	x.nr_seq_material is null
					and	a.nr_sequencia	= nr_seq_lote_contest_w);
					
			if	(qt_inconsistente_w > 0) and
				(ie_tipo_arquivo_disc_w in (5,6,7,8,9)) then
				commit;
				wheb_mensagem_pck.exibir_mensagem_abort(285404);
			end if;
		end if;
		
		select	max(a.ie_envio_recebimento)
		into	ie_envio_recebimento_w
		from	pls_lote_contestacao a
		where	a.nr_sequencia		= nr_seq_lote_contest_w;
		
		select	max(nr_seq_ptu_fatura),
			max(nr_seq_pls_fatura)
		into	nr_seq_fatura_w,
			nr_seq_pls_fatura_w
		from	pls_lote_contestacao
		where	nr_sequencia = nr_seq_lote_contest_w;
				
		select	nvl(max(nr_titulo),max(nr_titulo_ndc))
		into	nr_titulo_w
		from	ptu_fatura
		where	nr_sequencia = nr_seq_fatura_w;
		
		select	max(nr_sequencia)
		into	nr_seq_camara_contest_w
		from	ptu_camara_contestacao
		where	nr_seq_lote_contest 	= nr_seq_lote_contest_w;
		
		select	nvl(max(nr_sequencia), nr_seq_camara_contest_w)
		into	nr_seq_camara_contest_w
		from	ptu_camara_contestacao
		where	nr_seq_lote_contest 	= nr_seq_lote_contest_w
		and	nr_seq_lote_discussao	= nr_seq_lote_disc_p;
		
		if	(nvl(ie_atualizar_vl_contest_p,'S') = 'S') then
			pls_atualizar_valores_contest(nr_seq_lote_contest_w,'N');
		end if;

		update	pls_lote_discussao
		set 	dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_fechamento	= sysdate,
			ie_status	= 'F'
		where	nr_sequencia 	= nr_seq_lote_disc_p;

		-- Gerar LOG
		pls_gerar_contest_log(	nr_seq_lote_contest_w, nr_seq_lote_disc_p, null, null, null, null, 'FLD', 'N', nm_usuario_p);				
		
		-- Somente tratar evento Geracco/baixa de titulo a pagar caso discussao recebida
		if	(ie_envio_recebimento_w = 'E') then
			pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
							'20', -- Validar baixa no titulo da fatura
							nr_seq_fatura_w,
							null,
							nr_seq_lote_contest_w,
							null,
							sysdate,
							'A550',
							ie_estorno_p,
							nr_seq_acao_w);
							
			if	(nr_seq_acao_w is not null) and (nr_titulo_w is not null) then
				select	count(1)
				into	qt_baixa_w
				from	titulo_pagar_baixa
				where	nr_titulo	= nr_titulo_w
				and	vl_glosa	= 0;
				
				if	(qt_baixa_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(1072101); -- O titulo da fatura nao possui baixa.
				end if;
			end if;
			
			pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
							'7', -- Baixar valor glosado no titulo
							nr_seq_fatura_w,
							null,
							nr_seq_lote_contest_w,
							null,
							sysdate,
							'A550',
							ie_estorno_p,
							nr_seq_acao_w);
							
			if	(nr_seq_acao_w is not null) then
				-- Gerar baixa no titulo a pagar com os glosas negadas
				pls_baixar_glosas_discuss_pag(nr_seq_lote_disc_p,nr_seq_acao_w,'N',nm_usuario_p,cd_estabelecimento_p);
			end if;
			
			pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
							'1', -- Gerar titulo a receber
							nr_seq_fatura_w,
							null,
							null,
							null,
							sysdate,
							'A550',
							ie_estorno_p,
							nr_seq_acao_w);		
							
			if	(nr_seq_acao_w is not null) and
				(ie_tipo_arquivo_disc_w not in (9)) then
				ie_gerar_titulo_fat_ndr_w := 'N';
				pls_gerar_tit_receber_discuss(nr_seq_lote_disc_p,nr_seq_acao_w,sysdate,nm_usuario_p);
			end if;
			
			if	(ie_gerar_titulo_fat_ndr_w = 'S') then
				pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
								'14', -- Gerar titulo a receber (Fatura)
								nr_seq_fatura_w,
								null,
								null,
								null,
								sysdate,
								'A550',
								ie_estorno_p,
								nr_seq_acao_w);		
								
				if	(nr_seq_acao_w is not null) and
					(ie_tipo_arquivo_disc_w not in (9)) then
					pls_gerar_tit_receber_discuss(nr_seq_lote_disc_p,nr_seq_acao_w,sysdate,nm_usuario_p);
				end if;
				
				pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
								'15', -- Gerar titulo a receber (NDR)
								nr_seq_fatura_w,
								null,
								null,
								null,
								sysdate,
								'A550',
								ie_estorno_p,
								nr_seq_acao_w);		
								
				if	(nr_seq_acao_w is not null) and
					(ie_tipo_arquivo_disc_w not in (9)) then
					pls_gerar_tit_receber_discuss(nr_seq_lote_disc_p,nr_seq_acao_w,sysdate,nm_usuario_p);
				end if;
			end if;
			
			pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
							'2', -- Gerar titulo a pagar
							nr_seq_fatura_w,
							null,
							null,
							null,
							sysdate,
							'A550',
							ie_estorno_p,
							nr_seq_acao_w);
							
			if	(nr_seq_acao_w is not null) and
				(ie_tipo_arquivo_disc_w not in (9)) then	
				-- Gerar titulo a pagar
				pls_gerar_tit_pagar_discussao(nr_seq_lote_disc_p,nr_seq_acao_w,nm_usuario_p,cd_estabelecimento_p);
			end if;
			
			pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
							'19', -- Encaminhar discussao para analise
							nr_seq_fatura_w,
							null,
							null,
							null,
							sysdate,
							'A550',
							ie_estorno_p,
							nr_seq_acao_w);
							
			if	(nr_seq_acao_w is not null) then
				-- Gerar o lote de analise da discussao 
				pls_gerar_lote_analise_disc(nr_seq_lote_disc_p,nm_usuario_p);
				
				update	pls_lote_discussao
				set 	ie_status	= 'F'
				where	nr_sequencia 	= nr_seq_lote_disc_p;
			end if;
			
			if	(ie_tipo_arquivo_disc_w not in (9)) then
				-- Gerar coparticipacao
				pls_gerar_coparticipacao_disc( nr_seq_lote_disc_p, cd_estabelecimento_p, nm_usuario_p);
				
				-- Gerar pos-estabalecido
				pls_gerar_pos_estab_disc( nr_seq_lote_disc_p, cd_estabelecimento_p, nm_usuario_p);
				--Gerar dados monitoramento
				pls_grava_monit_disc( nr_seq_lote_disc_p, nm_usuario_p);
			end if;
		end if;
		
		-- Gerar A560
		pls_gerar_nota_debito_a560( nr_seq_lote_disc_p, nr_seq_camara_contest_w, cd_estabelecimento_p, nm_usuario_p);
		
		-- Gerar eventos financeiros
		pls_gerar_evento_contestacao(nr_seq_lote_disc_p,'N',cd_estabelecimento_p,nm_usuario_p);
	end if;

	if 	(ie_concil_contab_w = 'S') then
		pls_ctb_onl_gravar_movto_pck.gravar_movto_fechar_disc(nr_seq_lote_disc_p, cd_estabelecimento_p, nm_usuario_p);
	end if;
end if;

-- Estorno das acoes que podem ocorrer durante o fechamento da discussao
if	(nvl(ie_estorno_p,'N') = 'S') then
	if 	(ie_concil_contab_w = 'S') then
		pls_ctb_onl_gravar_movto_pck.gravar_movto_desf_fech_disc(nr_seq_lote_disc_p, cd_estabelecimento_p, nm_usuario_p);
	end if;

	select	max(nr_seq_lote_contest),
		max(nr_seq_lote_conta)
	into	nr_seq_lote_contest_w,
		nr_seq_lote_conta_w
	from	pls_lote_discussao
	where	nr_sequencia = nr_seq_lote_disc_p;
	
	if	(nr_seq_lote_conta_w is null) then
		ie_status_w := 'A';
	else
		ie_status_w := 'AN';
	end if;

	update	pls_lote_discussao
	set 	dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_fechamento	= null,
		ie_status	= ie_status_w
	where	nr_sequencia 	= nr_seq_lote_disc_p;

	-- Gerar LOG
	pls_gerar_contest_log(	nr_seq_lote_contest_w, nr_seq_lote_disc_p, null, null, null, null, 'DFLD', 'N', nm_usuario_p);
	
	-- Gerar titulo a receber - pagar
	select	max(nr_titulo_receber),
		max(nr_titulo_receber_ndr),
		max(nr_titulo_pagar),
		max(nr_titulo_pagar_ndr)
	into	nr_titulo_rec_w,
		nr_titulo_receber_ndr_w,
		nr_titulo_pag_w,
		nr_titulo_pagar_ndr_w
	from	pls_lote_discussao
	where	nr_sequencia	= nr_seq_lote_disc_p;
	
	if	(nr_titulo_rec_w is not null) then
		cancelar_titulo_receber(nr_titulo_rec_w,nm_usuario_p,'N',sysdate);
		
		update	pls_lote_discussao
		set	nr_titulo_receber	= null
		where	nr_sequencia		= nr_seq_lote_disc_p;
		
		update	ptu_nota_debito
		set	ie_cancelamento = 'S'
		where	nr_sequencia	=	(select	max(a.nr_seq_nota_debito)
						from	ptu_nota_deb_conclusao 	a,
							titulo_receber 		b
						where	a.nr_sequencia		= b.nr_seq_nota_deb_conclusao
						and	b.nr_titulo		= nr_titulo_rec_w);
	end if;
	
	if	(nr_titulo_receber_ndr_w is not null) then
		cancelar_titulo_receber(nr_titulo_receber_ndr_w,nm_usuario_p,'N',sysdate);
		
		update	pls_lote_discussao
		set	nr_titulo_receber_ndr	= null
		where	nr_sequencia		= nr_seq_lote_disc_p;
		
		update	ptu_nota_debito
		set	ie_cancelamento = 'S'
		where	nr_sequencia	=	(select	max(a.nr_seq_nota_debito)
						from	ptu_nota_deb_conclusao 	a,
							titulo_receber 		b
						where	a.nr_sequencia		= b.nr_seq_nota_deb_conclusao
						and	b.nr_titulo		= nr_titulo_receber_ndr_w);
	end if;
	
	if	(nr_titulo_pag_w is not null) then
		cancelar_titulo_pagar(nr_titulo_pag_w,nm_usuario_p,sysdate);
	
		update	pls_lote_discussao
		set	nr_titulo_pagar		= null,
			nr_nota_credito_debito	= null
		where	nr_sequencia		= nr_seq_lote_disc_p;
	end if;
	
	if	(nr_titulo_pagar_ndr_w is not null) then
		cancelar_titulo_pagar(nr_titulo_pagar_ndr_w,nm_usuario_p,sysdate);
	
		update	pls_lote_discussao
		set	nr_titulo_pagar_ndr		= null,
			nr_nota_credito_debito_ndr	= null
		where	nr_sequencia			= nr_seq_lote_disc_p;
	end if;
	
	-- Baixar valor glosado no titulo
	select	max(b.nr_seq_ptu_fatura)
	into	nr_seq_fatura_w
	from	pls_lote_contestacao	b,
		pls_lote_discussao 	a
	where	b.nr_sequencia	= a.nr_seq_lote_contest
	and	a.nr_sequencia	= nr_seq_lote_disc_p;
	
	pls_obter_acao_intercambio(	'5', -- Fechamento de discussao
					'7', -- Baixar valor glosado no titulo
					nr_seq_fatura_w,
					null,
					null,
					null,
					sysdate,
					'A550',
					ie_estorno_p,
					nr_seq_acao_w);	
		
	if	(nr_seq_acao_w is not null) then
		pls_baixar_glosas_discuss_pag(nr_seq_lote_disc_p,nr_seq_acao_w,'S',nm_usuario_p,cd_estabelecimento_p);
	end if;
	
	-- Desfazer os eventos
	select	count(1)
	into	qt_reg_eve_w
	from	pls_lote_evento		b,
		pls_evento_movimento	a
	where	b.nr_sequencia		= a.nr_seq_lote
	and	a.nr_seq_lote_disc	= nr_seq_lote_disc_p
	and	b.dt_liberacao 		is not null
	and	a.ie_cancelamento 	is null
	and	rownum = 1;
	
	if	(qt_reg_eve_w > 0) then
		select	max(b.nr_sequencia)
		into	nr_seq_lote_evento_w
		from	pls_lote_evento		b,
			pls_evento_movimento	a
		where	b.nr_sequencia		= a.nr_seq_lote
		and	a.nr_seq_lote_disc	= nr_seq_lote_disc_p
		and	b.dt_liberacao is not null;
		
		wheb_mensagem_pck.exibir_mensagem_abort(196092,'NR_SEQ_LOTE_EVENTO=' || nr_seq_lote_evento_w);
	else
		delete	pls_evento_movimento
		where	nr_seq_lote_disc = nr_seq_lote_disc_p;
	end if;
	
	pls_delete_monit_disc(nr_seq_lote_disc_p, nm_usuario_p);
	-- Desfazer analise da discussao
	if	(nr_seq_lote_conta_w is not null) then
		pls_desfazer_analise_discussao(nr_seq_lote_conta_w, 'N', nm_usuario_p);
	end if;
end if;

commit;

end pls_fechar_discussao_recebida;
/