CREATE OR REPLACE
PROCEDURE Gerar_Fluxo_Caixa_Contrato(
			cd_estabelecimento_p		Number,
			nm_usuario_p			Varchar2,
			cd_empresa_p			number,
			ie_restringe_estab_p		varchar2,
			dt_inicial_p			date,
			dt_final_p			date,
			ie_operacao_p			varchar2,
			ie_somente_ativa_p		varchar2) IS

/*--------------------------------------------------------------- ATENCAO ----------------------------------------------------------------*/
/* Cuidado ao realizar alteracoes no fluxo de caixa. Toda e qualquer alteracao realizada em qualquer uma das       */
/* procedures do fluxo de caixa deve ser cuidadosamente verificada e realizada no fluxo de caixa em lote.           */
/* Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando assim que           */
/* existam diferencas entre os fluxos de caixa.                                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA ALTERAR TAMBEM O FLUXO DE CAIXA EM LOTE ---------------*/

cd_conta_financ_w	Number(10,0);
nr_seq_contrato_w	Number(10,0);
dt_primeiro_vencto_w	Date;

ie_classif_Fluxo_w	Varchar2(1)		:= 'R';
ie_origem_w		Varchar2(1)		:= 'I';
vl_fluxo_w		Number(17,4);
dt_referencia_w	Date;
cd_moeda_w		number(05,0)		:= 0;
cd_moeda_padrao_w	number(05,0)		:= 0;
qt_dia_w		Number(02,0);
ie_tratar_fim_semana_w	varchar2(255);
IE_SOMENTE_FUTURO_w	varchar2(255);
ie_forma_w		varchar2(255);
dt_fim_w		date;
dt_vencimento_w		date;
dt_inicio_w		date;
cont_w			number(4,0);
ie_operacao_w		varchar2(10);
ie_conta_financ_ativa_w	varchar2(1);
ie_contrato_nf_calc_w	varchar2(1);
cont_ww			number(4,0);
dia_vencimento_w	varchar2(2);
ie_ano_bisexto_w	varchar(1);
cd_moeda_empresa_w	number(5);

Cursor c010 is
select	a.nr_sequencia,
	b.dt_primeiro_vencto,
	b.vl_pagto,
	b.cd_moeda,
	b.cd_conta_financ,
	pkg_date_utils.start_of(nvl(b.dt_final_vigencia,dt_final_p), 'DD', 0),
	b.ie_forma
from	contrato_regra_pagto b,
	contrato a
where	a.nr_sequencia		= b.nr_seq_contrato
and	a.ie_situacao		= 'A'
and	nvl(b.dt_inicio_vigencia,dt_final_p)	<= dt_final_p
and	nvl(b.dt_final_vigencia,dt_final_p)	>= dt_inicial_p
and	b.dt_primeiro_vencto	between nvl(b.dt_inicio_vigencia,b.dt_primeiro_vencto)
				and	nvl(b.dt_final_vigencia,b.dt_primeiro_vencto)
and	a.ie_pagar_receber	= 'P'
and	b.ie_forma		in ('M', 'T', 'F')
and	nvl(vl_pagto,0)		> 0
and	ie_tipo_valor		= 'V'
and	cd_conta_financ	is not null
and	(cd_estabelecimento	= cd_estabelecimento_p or ie_restringe_estab_p = 'N')
and	obter_empresa_estab(a.cd_estabelecimento) = cd_empresa_p
and	(obter_se_conta_financ_estab(b.cd_conta_financ, cd_estabelecimento_p,ie_restringe_estab_p) = 'S' or ie_restringe_estab_p = 'N')
and	Obter_se_media_regra(b.cd_conta_financ) = 'N'
and not exists (
				select 1 -- se possuir titulo gerado pelo contrato nao exibe o contrato, pois ja entra no titulo.
				from titulo_pagar x
				where  x.nr_seq_contrato = a.nr_sequencia
				and	x.nr_seq_tributo is null -- so deve considerar o titulo para pagamento do fornecedor do contrato, nao pode considerar os titulos de tributos gerados a partir deste
				and  x.ie_situacao <> 'C' /* OS 2291609 - Ajuste para nao considerar contratos com titulos cancelados */
				and trunc(x.dt_vencimento_atual, 'month') = trunc(to_date(dt_inicial_p), 'month')
			);

BEGIN

select	obter_moeda_padrao(cd_estabelecimento_p, 'P')
into	cd_moeda_padrao_w
from	dual;

/* Projeto Multimoeda - Busca a moeda padrao da empresa para gravar no fluxo. */
select	obter_moeda_padrao_empresa(cd_estabelecimento_p,'E')
into	cd_moeda_empresa_w
from 	dual;


begin
	select	ie_tratar_fim_semana,
		IE_SOMENTE_FUTURO,
		nvl(ie_contrato_nf_calc,'N')
	into	ie_tratar_fim_semana_w,
		IE_SOMENTE_FUTURO_w,
		ie_contrato_nf_calc_w
	from	parametro_fluxo_caixa
	where	cd_estabelecimento	= cd_estabelecimento_p;
exception when no_data_found then
		/* Parametros do fluxo de caixa nao cadastrados! */
		wheb_mensagem_pck.exibir_mensagem_abort(197611);
end;

dt_inicio_w		:= dt_inicial_p;
if	(IE_SOMENTE_FUTURO_w = 'S') then
	dt_inicio_w	:= pkg_date_utils.start_of(sysdate, 'DD', 0);
end if;

OPEN C010;
LOOP
FETCH C010 into 
	nr_seq_contrato_w,
	dt_primeiro_vencto_w,
	vl_fluxo_w,
	cd_moeda_w,
	cd_conta_financ_w,
	dt_fim_w,
	ie_forma_w;
EXIT when c010%notfound;

	dt_vencimento_w		:= dt_primeiro_vencto_w;
	if	(ie_forma_w = 'F') then

		select	count(*) -- se possuir titulo na nota gerada pelo contrato nao exibe o contrato, pois ja entra no titulo.
		into	cont_ww
		from	nota_fiscal y,
			titulo_pagar x
		where	x.nr_seq_nota_fiscal		= y.nr_sequencia
		and	y.nr_contrato			= nr_seq_contrato_w
		and	y.dt_atualizacao_estoque	is not null
		and	x.nr_seq_tributo		is null -- nao pode considerar o titulo do tributo gerado pela nota fiscal, somente o titulo original
		and	pkg_date_utils.start_of(x.dt_vencimento_atual,'MONTH',0) = pkg_date_utils.start_of(dt_vencimento_w,'MONTH',0);
		
		if (cont_ww = 0) then
		
			select	count(*)
			into	cont_w
			from	nota_fiscal b,
				nota_fiscal_item a
			where	a.nr_sequencia			= b.nr_sequencia
			and	a.nr_contrato			= nr_seq_contrato_w
			and	b.dt_atualizacao_estoque	is not null
			and	((ie_contrato_nf_calc_w = 'S' and pkg_date_utils.start_of(TO_DATE(SUBSTR(obter_dados_nota_fiscal(b.nr_sequencia,'21'),1,60)),'MONTH',0) = pkg_date_utils.start_of(dt_vencimento_w, 'MONTH',0)) or 
				(pkg_date_utils.start_of(b.dt_atualizacao_estoque,'MONTH',0) = pkg_date_utils.start_of(dt_vencimento_w, 'MONTH',0)));
			
			if 	(cont_w = 0) and
				(cont_ww = 0)then

				select	obter_operacao_conta_financ(cd_conta_financ_w)
				into	ie_operacao_w
				from	dual;

				if	(
						((ie_operacao_p = 'S') and (ie_operacao_w <> 'D'))
					or
						((ie_operacao_p = 'D') and (ie_operacao_w <> 'S'))
					or
						(ie_operacao_p = 'A')
					) then

					select	decode(ie_somente_ativa_p,'S',ie_situacao,'A')
					into	ie_conta_financ_ativa_w
					from	conta_financeira
					where	cd_conta_financ = cd_conta_financ_w;
					
					if	(ie_conta_financ_ativa_w = 'A') then
						begin
						insert into fluxo_caixa(
							cd_estabelecimento,
							dt_referencia,
							cd_conta_financ,
							ie_classif_fluxo,
							dt_atualizacao,
							nm_usuario,
							vl_fluxo,
							ie_origem,
							ie_periodo,
							ie_integracao,
							cd_empresa,
							cd_moeda)
						values(	cd_estabelecimento_p,
							dt_vencimento_w,
							cd_conta_financ_w,
							ie_classif_fluxo_w,
							sysdate,
							nm_usuario_p,
							vl_fluxo_w,
							ie_origem_w,
							'D',
							'CO',
							cd_empresa_p,
							cd_moeda_empresa_w);
						exception
						when dup_val_on_index then
						update	fluxo_caixa 
							set	vl_fluxo		= vl_fluxo + vl_fluxo_w
							where	cd_estabelecimento	= cd_estabelecimento_p
							and	cd_conta_financ		= cd_conta_financ_w
							and	dt_referencia		= dt_vencimento_w
							and	ie_periodo		= 'D'
							and	ie_classif_fluxo	= ie_classif_fluxo_w
							and	ie_integracao		= 'CO'
							and	cd_empresa		= cd_empresa_p;
						when others then
							wheb_mensagem_pck.exibir_mensagem_abort(190199,'DS_SQLERRM_W=' || sqlerrm);
						end;
					end if;
				end if;
			end if;
		end if;
	else
		--dt_vencimento_w	:= to_date(to_char(dt_vencimento_w,'dd/mm') || '/' || to_char(dt_inicial_p,'yyyy'),'dd/mm/yyyy');
		begin
			ie_ano_bisexto_w := obter_se_ano_bisexto(dt_inicial_p);
		
			if (pkg_date_utils.extract_field('MONTH', dt_inicial_p, 0) = 2) then /*Se for fevereiro*/
			
				if (nvl(ie_ano_bisexto_w,'N') = 'S' ) and (pkg_date_utils.extract_field('DAY', dt_vencimento_w, 0) > 29 ) then /*se for ano bisexto e o dia for maior que 29, nao pode pois fevereiro em ano bisexto tem apenas 29 dias*/
					
					dt_vencimento_w	:= PKG_DATE_UTILS.START_OF(pkg_date_utils.end_of(dt_inicial_p, 'MONTH', 0), 'DD'); /*pega o utimo dia do mes 02*/
				
				elsif (nvl(ie_ano_bisexto_w,'N') = 'N' ) and (pkg_date_utils.extract_field('DAY', dt_vencimento_w, 0) > 28 ) then /*se n for bisexto e 28 dias*/
				
					dt_vencimento_w	:= PKG_DATE_UTILS.START_OF(pkg_date_utils.end_of(dt_inicial_p, 'MONTH', 0), 'DD'); /*pega o utimo dia do mes 02*/
					
				end if;
			
			else 
			
				dt_vencimento_w	:= pkg_date_utils.get_date(pkg_date_utils.extract_field('DAY', dt_vencimento_w, 0), dt_inicial_p, 0);
				
			end if;
		
			/*if (to_char(dt_inicial_p,'mm') = 02) and ( to_char(dt_vencimento_w,'dd') >= 28 ) then /*se for fevereiro  nao tem 30 dias*/
				/*dt_vencimento_w	:= to_date(to_char(last_day(dt_vencimento_w),'dd/mm/yyyy'),'dd/mm/yyyy');
			else
				dt_vencimento_w	:= to_date(to_char(dt_vencimento_w,'dd') || '/' || to_char(dt_inicial_p,'mm/yyyy'),'dd/mm/yyyy');
			end if;*/
		exception when others then
				dt_vencimento_w := pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_inicial_p,'MONTH',0), nvl(dt_inicial_p, PKG_DATE_UTILS.get_Time('00:00:00')));
				--wheb_mensagem_pck.exibir_mensagem_abort(316259,'NR_SEQ_CONTRATO_W=' || nr_seq_contrato_w);
		end;
		
		if	(dt_vencimento_w = pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_vencimento_w, 'MONTH', 0), nvl(dt_vencimento_w, PKG_DATE_UTILS.get_Time('00:00:00')))) then
			dia_vencimento_w := '31';
		else
			dia_vencimento_w := pkg_date_utils.extract_field('DAY',dt_vencimento_w,0);
		end if;
		
		
		/* Alterado para gerar somente os dados referente ao periodo solicitado no fluxo de caixa, pois em base com uma quantidade consideravel de contratos gerava muitos registros nao utilizados pelo fluxo de caixa causando lentidao no processo. */
		/* So pode gerar os registros ate a data final de vigencia da regra de pagamento, verificando sempre pelo dia do vencimento definido para a regra */
		while 	((pkg_date_utils.start_of(dt_vencimento_w, 'MONTH',0) <= pkg_date_utils.start_of(dt_final_p, 'MONTH',0))
			and (pkg_date_utils.start_of(dt_vencimento_w, 'DD',0) <= pkg_date_utils.start_of(dt_fim_w, 'DD',0))) loop

			select	count(*) -- se possuir titulo na nota gerada pelo contrato nao exibe o contrato, pois ja entra no titulo.
			into	cont_ww
			from	nota_fiscal y,
				titulo_pagar x
			where	x.nr_seq_nota_fiscal		= y.nr_sequencia
			and	y.nr_contrato			= nr_seq_contrato_w
			and	y.dt_atualizacao_estoque	is not null
			and	x.nr_seq_tributo		is null -- so deve considerar o titulo para pagamento do fornecedor do contrato, nao pode considerar os titulos de tributos gerados a partir deste
			and	pkg_date_utils.start_of(x.dt_vencimento_atual,'MONTH',0) = pkg_date_utils.start_of(dt_vencimento_w,'MONTH',0);
			
			if (cont_ww = 0) then
			
				select	count(*)
				into	cont_w
				from	nota_fiscal b,
						nota_fiscal_item a
				where	a.nr_sequencia			= b.nr_sequencia
				and	a.nr_contrato			= nr_seq_contrato_w
				and	b.dt_atualizacao_estoque	is not null
				and	((ie_contrato_nf_calc_w = 'S' and pkg_date_utils.start_of(TO_DATE(SUBSTR(obter_dados_nota_fiscal(b.nr_sequencia,'21'),1,60)),'MONTH',0) = pkg_date_utils.start_of(dt_vencimento_w, 'MONTH',0)) or 
					(pkg_date_utils.start_of(b.dt_atualizacao_estoque,'MONTH',0) = pkg_date_utils.start_of(dt_vencimento_w, 'MONTH',0)));
				
				if 	(cont_w = 0) and
					(cont_ww = 0)then

					if	(cd_moeda_padrao_w <> cd_moeda_w) then
						select	Obter_Valor_Moeda_Nac
							(cd_estabelecimento_p,
							vl_fluxo_w,
							cd_moeda_w,
							dt_vencimento_w)
						into	vl_fluxo_w
						from	dual;
					end if;

					dt_referencia_w			:= dt_vencimento_w;
					if	(ie_tratar_fim_semana_w = 'S') then
						dt_referencia_w		:= obter_proximo_dia_util(cd_estabelecimento_p, dt_vencimento_w);
					end if;

					if	(dt_referencia_w >= dt_inicio_w) then
						-- Edgar 07/12/2009, OS 182897, tratar pagamento trimestral
						if	(ie_forma_w = 'M') or
							(pkg_date_utils.start_of(dt_referencia_w, 'MONTH',0) = pkg_date_utils.start_of(dt_primeiro_vencto_w, 'MONTH',0)) or
							(mod(somente_numero(pkg_date_utils.extract_field('MONTH',dt_referencia_w, 0)) - somente_numero(pkg_date_utils.extract_field('MONTH',dt_primeiro_vencto_w, 0)), 3) = 0) then

							select	obter_operacao_conta_financ(cd_conta_financ_w)
							into	ie_operacao_w
							from	dual;

							if	(
									((ie_operacao_p = 'S') and (ie_operacao_w <> 'D'))
								or
									((ie_operacao_p = 'D') and (ie_operacao_w <> 'S'))
								or
									(ie_operacao_p = 'A')
								) then

								select	decode(ie_somente_ativa_p,'S',ie_situacao,'A')
								into	ie_conta_financ_ativa_w
								from	conta_financeira
								where	cd_conta_financ = cd_conta_financ_w;
								
								if	(ie_conta_financ_ativa_w = 'A') then
									begin
									insert into fluxo_caixa(
										cd_estabelecimento,
										dt_referencia,
										cd_conta_financ,
										ie_classif_fluxo,
										dt_atualizacao,
										nm_usuario,
										vl_fluxo,
										ie_origem,
										ie_periodo,
										ie_integracao,
										cd_empresa,
										cd_moeda)
									values(	cd_estabelecimento_p,
										dt_referencia_w,
										cd_conta_financ_w,
										ie_classif_fluxo_w,
										sysdate, 
										nm_usuario_p,
										vl_fluxo_w,
										ie_origem_w,
										'D', 
										'CO',
										cd_empresa_p,
										cd_moeda_empresa_w);
									exception
									when dup_val_on_index then
										update	fluxo_caixa 
										set	vl_fluxo		= vl_fluxo + vl_fluxo_w
											where	cd_estabelecimento	= cd_estabelecimento_p
										and	cd_conta_financ		= cd_conta_financ_w
										and	dt_referencia		= dt_referencia_w
										and	ie_periodo		= 'D'
										and	ie_classif_fluxo	= ie_classif_fluxo_w
										and	ie_integracao		= 'CO'
										and	cd_empresa		= cd_empresa_p;
									when others then
										wheb_mensagem_pck.exibir_mensagem_abort(190199,'DS_SQLERRM_W=' || sqlerrm);
									end;
								end if;
							end if;
						end if;
					end if;
				end if;
			end if;
			
			dt_vencimento_w		:= PKG_DATE_UTILS.ADD_MONTH(dt_vencimento_w, 1,0);
			begin
			dt_vencimento_w		:= pkg_date_utils.get_date(dia_vencimento_w, dt_vencimento_w,0);
			exception
			when others then
			dt_vencimento_w := dt_vencimento_w;
			end;
			
		end loop;
	end if;
END LOOP;
CLOSE C010;

commit;

END Gerar_Fluxo_Caixa_Contrato;
/
