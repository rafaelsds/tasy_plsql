create or replace
procedure importar_colunas_catalogo(
			nm_coluna_p			varchar2,
			nm_usuario_p		varchar2) is 

/*importar_colunas_catalogo*/

begin



	if (nm_coluna_p is not null) then

	begin

		insert into w_config_carga_endereco(
			nr_sequencia,      
			nm_coluna_arquivo,
			nm_usuario,        
			nm_usuario_nrec,         
			dt_atualizacao,     
			dt_atualizacao_nrec) 
		values 
			(w_config_carga_endereco_seq.nextval,
			nm_coluna_p,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate);

	end;

	end if;

commit;

end importar_colunas_catalogo;
/