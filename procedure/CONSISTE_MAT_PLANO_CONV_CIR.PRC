create or replace
procedure consiste_mat_plano_conv_cir(	nr_prescricao_p 	number,
					cd_material_p		number,
					qt_material_p		number,
					ie_regra_p		out number,
					ds_mensagem_p		out varchar2) is

cd_convenio_w		number(10);
cd_plano_w		varchar2(100);
cd_material_w		number(6);
nr_atendimento_w	number(10);
cd_setor_atendimento_w	number(5);
dt_atendimento_w	date;
nr_seq_agenda_w		number(10);
cd_estabelecimento_w	number(4);
cd_categoria_w		varchar(10);
ie_tipo_atendimento_w	number(3);
ds_retorno_w		varchar2(255)	:= '';
ie_bloqueia_agenda_w	varchar2(01)	:= 'N';
ie_regra_w		number(02,0) 	:= 0;
nr_seq_regra_w		number(10);
ie_prescr_especial_w	varchar2(01)	:= 'N';

begin

if	(nr_prescricao_p > 0) then
	
	select	nvl(max('N'),'S')
	into	ie_prescr_especial_w
	from	cirurgia
	where	nr_prescricao = nr_prescricao_p;
		
	if	(ie_prescr_especial_w = 'S') then
		select	max(a.cd_convenio),
			substr(obter_plano_atendimento(max(a.nr_atendimento),'C'),1,100) cd_plano,
			max(a.nr_atendimento),
			max(a.cd_setor_atendimento),
			to_date(obter_dados_atendimento(max(a.nr_atendimento),'DE'),'dd/mm/yyyy hh24:mi:ss') dt_atendimento,
			max(a.nr_seq_agenda),
			max(a.cd_estabelecimento),
			max(a.cd_categoria),
			obter_tipo_atendimento(max(a.nr_atendimento)) ie_tipo_atendimento
		into	cd_convenio_w,
			cd_plano_w,
			nr_atendimento_w,
			cd_setor_atendimento_w,
			dt_atendimento_w,
			nr_seq_agenda_w,
			cd_estabelecimento_w,
			cd_categoria_w,
			ie_tipo_atendimento_w
		from	cirurgia a,
			prescr_medica b
		where	a.nr_cirurgia 	= b.nr_cirurgia
		and	b.nr_prescricao = nr_prescricao_p
		and	nvl(ie_tipo_prescr_cirur,0) <> 2;
	else	
		select		max(a.cd_convenio),
				substr(obter_plano_atendimento(max(a.nr_atendimento),'C'),1,100) cd_plano,
				max(a.nr_atendimento),
				max(a.cd_setor_atendimento),
				to_date(obter_dados_atendimento(max(a.nr_atendimento),'DE'),'dd/mm/yyyy hh24:mi:ss') dt_atendimento,
				max(a.nr_seq_agenda),
				max(a.cd_estabelecimento),
				max(a.cd_categoria),
				obter_tipo_atendimento(max(a.nr_atendimento)) ie_tipo_atendimento
		into		cd_convenio_w,
				cd_plano_w,
				nr_atendimento_w,
				cd_setor_atendimento_w,
				dt_atendimento_w,
				nr_seq_agenda_w,
				cd_estabelecimento_w,
				cd_categoria_w,
				ie_tipo_atendimento_w
		from		cirurgia a
		where		a.nr_prescricao = nr_prescricao_p;
	end if;	

	if	(cd_convenio_w > 0) then
		consiste_mat_plano_convenio	(cd_convenio_w,
						cd_plano_w,
						cd_material_p,
						nr_atendimento_w,
						cd_setor_atendimento_w,
						ds_retorno_w,
						ie_bloqueia_agenda_w,
						ie_regra_w,
						nr_seq_regra_w,
						qt_material_p,
						dt_atendimento_w,
						nr_seq_agenda_w,
						cd_estabelecimento_w,
						cd_categoria_w,
						ie_tipo_atendimento_w);
		ie_regra_p	:= ie_regra_w;
		ds_mensagem_p	:= substr(ds_retorno_w,1,255);
	end if;
end if;
end consiste_mat_plano_conv_cir;
/