create or replace
procedure Consulta_inf_prim_checagem(	nr_seq_horario_p	number,
										cd_item_p			number,
										cd_procedimento_p	number,
										nm_usuario_p		varchar2,
										ie_opcao_p			varchar2,
										ie_tipo_item_p		varchar2,
										nr_prescricao_p		varchar2 default '0',
										ds_retorno_p		out 	varchar2,
										ie_acao_p			Number,
										nr_etapa_p			Number,
										nr_seq_glicemia_p	Number default null) is

ie_retorno_w			char(1);
nr_etapa_atual_sol_w	Number(10);
ie_existe_proc_w		varchar2(1);
nr_agrupamento_w		prescr_mat_hor.nr_agrupamento%type;
nr_prescricao_w			prescr_medica.nr_prescricao%type;
ie_agrupador_w			prescr_material.ie_agrupador%type;
nr_seq_processo_w		prescr_mat_hor.nr_seq_processo%type;
nr_seq_horario_w		prescr_mat_hor.nr_sequencia%type;
nr_seq_item_w			prescr_material.nr_sequencia%type;
nm_usuario_w			usuario.nm_usuario%type;
nr_etapa_atual_w		prescr_mat_hor.nr_etapa_sol%type;

/*
MPC -	Mensagem: Realizar 1a checagem // 135872
MMU - 	Mensagem: O usuario que realizou a 1a checagem nao pode administrar o horario! // 160630
*/
begin

if	(nr_seq_horario_p is not null) and
	(cd_item_p is not null) and
	(nm_usuario_p is not null) and
	(ie_opcao_p is not null) then
	
	select	coalesce(max(a.nm_usuario), nm_usuario_p) nm_usuario
	into	nm_usuario_w
	from	usuario a
	where	upper(a.ds_login) = upper(nm_usuario_p);
			
	if	((nr_seq_glicemia_p is not null) and (ie_opcao_p = 'MMU')) then
			
		select	nvl(max('S'), 'N')
		into	ie_retorno_w
		from	atend_glicemia_evento b
		where	nr_seq_glicemia = nr_seq_glicemia_p
		and		ie_evento = 13
		and		upper(b.nm_usuario) = upper(nm_usuario_p);
		
		if	(ie_retorno_w = 'S') then
			ds_retorno_p	:= Wheb_mensagem_pck.get_texto(160630);
		end if;
				
	elsif	(ie_tipo_item_p in ('IAH','M','S','IA','IAG')) then -- Verifica para medicamentos e suplemento oral
	
		ie_retorno_w	:= nvl(obter_se_medic_dupla_chec(cd_item_p, wheb_usuario_pck.get_cd_estabelecimento, nr_prescricao_p),'N');
		
		if	(ie_retorno_w = 'N') then		
			select	nvl(max(ie_dupla_checagem),'N')
			into	ie_retorno_w
			from	material
			where	cd_material = cd_item_p;
			
			if	(ie_retorno_w = 'N') then	

				select	nvl(max(b.nr_agrupamento),0),
						max(a.nr_prescricao),
						max(b.ie_agrupador),
						max(nr_seq_processo)
				into	nr_agrupamento_w,
						nr_prescricao_w,
						ie_agrupador_w,
						nr_seq_processo_w
				from	prescr_mat_hor a,
						prescr_material b
				where	a.nr_sequencia = nr_seq_horario_p
				and		a.nr_prescricao = b.nr_prescricao
				and		a.nr_seq_material = b.nr_sequencia;
			
			    select	nvl(max('S'),'N')
				into	ie_retorno_w
				from	material
				where	cd_material in (	select 	b.cd_material
											from	prescr_material b
											where	nr_prescricao = nvl(nr_prescricao_p,nr_prescricao_w)
											and		nr_agrupamento = nr_agrupamento_w
											and		ie_agrupador = ie_agrupador_w
											and		cd_material <> cd_item_p)
				and		ie_dupla_checagem = 'S';
				
				if (ie_retorno_w = 'N') and (nr_seq_processo_w is not null)	then

						select	nvl(max('S'),'N')
						into	ie_retorno_w
						from	material
						where	((ie_dupla_checagem = 'S') or (nvl(obter_se_medic_dupla_chec(cd_material, wheb_usuario_pck.get_cd_estabelecimento, nvl(nr_prescricao_p,nr_prescricao_w)),'N') = 'S'))
						and		cd_material in (	select	b.cd_material	
													from	prescr_mat_hor b
													where	b.nr_seq_processo = nr_seq_processo_w
													and		b.ie_agrupador = ie_agrupador_w)
						and		nvl(obter_funcao_ativa,-1113) not in (1113,88);
						
				end if;
				
			end if;
			
		end if;
		
		if	(ie_retorno_w = 'S') then
			if	(ie_opcao_p = 'MPC') then
			
				select  nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_mat_hor
				where	rownum = 1
				and		nr_sequencia = nr_seq_horario_p
				and		dt_primeira_checagem is null
				and		dt_fim_horario is null
				and		dt_suspensao is null;	
				
				if	(ie_retorno_w = 'S') then  -- Se encontrou horario sem primeira checagem
											   -- Verificar se o item nao esta  com o processo tendo primeira checagem	
				
					select	nvl(max('N'), 'S')
					into	ie_retorno_w
					from	prescr_mat_hor a
					where	a.nr_sequencia = nr_seq_horario_p
					and		exists	( 	select 	1
										from	adep_processo b
										where	b.nr_sequencia = a.nr_seq_processo
										and		dt_primeira_checagem is not null);
				
				end if;
				
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(135872);
				end if;
				
			elsif	(ie_opcao_p = 'MMU') then

				select	nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_mat_hor a,
						prescr_mat_alteracao b
				where	rownum = 1
				and		a.nr_sequencia = nr_seq_horario_p
				and		a.nr_sequencia = b.nr_seq_horario
				and		a.dt_primeira_checagem is not null
				and		a.dt_fim_horario is null
				and		a.dt_suspensao is null
				and		b.ie_alteracao = 47
				and		upper(b.nm_usuario) = upper(nm_usuario_p)
				and		b.nr_sequencia = (	select	max(b.nr_sequencia)
											from	prescr_mat_hor a,
													prescr_mat_alteracao b
											where	a.nr_sequencia = nr_seq_horario_p
											and		a.nr_sequencia = b.nr_seq_horario
											and		a.dt_primeira_checagem is not null
											and		a.dt_fim_horario is null
											and		a.dt_suspensao is null
											and		b.ie_alteracao = 47);
											
				if	(ie_retorno_w = 'N') then
											
					select 	max(nr_seq_processo)										
					into	nr_seq_processo_w
					from	prescr_mat_hor
					where	nr_sequencia = nr_seq_horario_p;
						

					if	(nr_seq_processo_w is not null) then
											
				        select	nvl(max('S'),'N')
						into	ie_retorno_w
						from	prescr_mat_hor a,
								prescr_mat_alteracao b
						where	rownum = 1
						and		a.nr_seq_processo = nr_seq_processo_w
						and		a.nr_sequencia = b.nr_seq_horario
						and		a.dt_primeira_checagem is not null
						and		a.dt_fim_horario is null
						and		a.dt_suspensao is null
						and		b.ie_alteracao = 47
						and		upper(b.nm_usuario) = upper(nm_usuario_p)					
						and		b.nr_sequencia = (	select	max(b.nr_sequencia)
													from	prescr_mat_hor a,
															prescr_mat_alteracao b
													where	a.nr_seq_processo = nr_seq_processo_w
													and		a.nr_sequencia = b.nr_seq_horario
													and		a.dt_primeira_checagem is not null
													and		a.dt_fim_horario is null
													and		a.dt_suspensao is null
													and		b.ie_alteracao = 47);		
					end if;
					
				end if;		

				if	((ie_agrupador_w = 5) and (ie_retorno_w = 'N')) then
				
					select	nvl(max('S'),'N')
					into	ie_retorno_w
					from	prescr_mat_hor a,
							prescr_mat_alteracao b
					where	rownum = 1
					and		a.nr_sequencia = nr_seq_horario_p
					and		a.nr_sequencia = b.nr_seq_horario
					and		a.dt_primeira_checagem is not null
					and		a.dt_fim_horario is null
					and		a.dt_suspensao is null
					and		b.ie_alteracao = 47
					and		upper(b.nm_usuario) = upper(nm_usuario_p)
					and		b.nr_sequencia = (	select	max(b.nr_sequencia)
												from	prescr_mat_hor a,
														prescr_mat_alteracao b
												where	a.nr_sequencia = nr_seq_horario_p
												and		a.nr_sequencia = b.nr_seq_horario
												and		a.dt_primeira_checagem is not null
												and		a.dt_fim_horario is null
												and		a.dt_suspensao is null
												and		b.ie_alteracao = 47);
					
				end if;				
						
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(160630);
				end if;
			end if;
		end if;
	elsif (ie_tipo_item_p in('P','L','C','G','HM')) then
	
		ie_retorno_w	:= nvl(obter_se_medic_dupla_chec(cd_item_p, wheb_usuario_pck.get_cd_estabelecimento, nr_prescricao_p),'N');

		if	(ie_retorno_w = 'N') then
			if (ie_tipo_item_p = 'HM') then
				select	nvl(max(ie_dupla_checagem),'N')
				into	ie_retorno_w
				from	procedimento
				where	cd_procedimento = cd_procedimento_p;
			else
				select	nvl(max(ie_dupla_checagem),'N')
				into	ie_retorno_w
				from	procedimento
				where	cd_procedimento = cd_item_p;
			end if;
		end if;
		
		if	(ie_retorno_w = 'S') then
			if	(ie_opcao_p = 'MPC') then
			
				select	nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_proc_hor
				where	rownum = 1
				and		nr_sequencia = nr_seq_horario_p
				and		dt_primeira_checagem is null
				and		dt_fim_horario is null
				and		dt_suspensao is null;	
				
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(135872);
				end if;
			elsif	(ie_opcao_p = 'MMU') then
				if	(ie_tipo_item_p = 'HM')	then
					select	nvl(max('S'),'N')
					into	ie_retorno_w
					from	prescr_proc_hor a,
							prescr_solucao_evento b
					where	rownum = 1
					and		a.nr_prescricao = b.nr_prescricao
					and	a.nr_sequencia = nr_seq_horario_p
					and	a.nr_prescricao = nr_prescricao_p
					and	a.nr_seq_procedimento = cd_item_p
					and	a.nr_etapa = b.nr_etapa_evento
					and	a.dt_primeira_checagem is not null
					and	a.dt_fim_horario is null
					and	a.dt_suspensao is null
					and	b.ie_alteracao = 37
					and	upper(b.nm_usuario) = upper(nm_usuario_w)
					and	b.nr_sequencia = (	select	max(b.nr_sequencia)
									from	prescr_proc_hor a,
										prescr_solucao_evento b
									where	a.nr_sequencia = nr_seq_horario_p
									and		a.nr_prescricao = b.nr_prescricao
									and	a.nr_prescricao = nr_prescricao_p
									and	a.nr_seq_procedimento = cd_item_p
									and	a.nr_etapa = b.nr_etapa_evento
									and	a.dt_primeira_checagem is not null
									and	a.dt_fim_horario is null
									and	a.dt_suspensao is null
									and	b.ie_alteracao = 37);
				else
					select	nvl(max('S'),'N')
					into	ie_retorno_w
					from	prescr_proc_hor a,
							prescr_mat_alteracao b
					where	a.nr_sequencia = nr_seq_horario_p
					and		a.nr_sequencia = b.nr_seq_horario_proc
					and		a.dt_primeira_checagem is not null
					and		a.dt_fim_horario is null
					and		a.dt_suspensao is null
					and		b.ie_alteracao = 47
					and		upper(b.nm_usuario) = upper(nm_usuario_p)
					and		b.nr_sequencia = (	select	max(b.nr_sequencia)
												from	prescr_proc_hor a,
														prescr_mat_alteracao b
												where	a.nr_sequencia = nr_seq_horario_p
												and		a.nr_sequencia = b.nr_seq_horario_proc
												and		a.dt_primeira_checagem is not null
												and		a.dt_fim_horario is null
												and		a.dt_suspensao is null
												and		b.ie_alteracao = 47);
				end if;
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(160630);
				end if;
			end if;
		end if;
	elsif	(ie_tipo_item_p in ('SOL')) then -- Verifica solucoes
	
		ie_retorno_w	:= nvl(obter_se_medic_dupla_chec(cd_item_p, wheb_usuario_pck.get_cd_estabelecimento, nr_prescricao_p),'N');
		
		if	(ie_retorno_w = 'N') then		
			select	nvl(max('S'), 'N')
			into	ie_retorno_w
			from	material
			where	cd_material in(	  select 	cd_material
						  from 		prescr_material
						  where 	nr_sequencia_solucao = cd_item_p
						  and 		nr_prescricao = nr_prescricao_p)
			and	nvl(ie_dupla_checagem,'N') = 'S';
		end if;
		
		if	(ie_retorno_w = 'S') then
			if	(ie_opcao_p = 'MPC') then
			
				select  nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_mat_hor
				where	rownum = 1
				and	nr_etapa_sol = nr_seq_horario_p
				and	nr_prescricao = nr_prescricao_p
				and	nr_seq_solucao = cd_item_p
				and	dt_primeira_checagem is null
				and	dt_fim_horario is null
				and	dt_suspensao is null;	
				
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(135872);
				end if;
				
			elsif	(ie_opcao_p = 'MMU') then
						
				nr_etapa_atual_sol_w := obter_etapa_atual(nr_prescricao_p, cd_item_p);								
				if	(ie_acao_p = 3) then
					nr_etapa_atual_sol_w	:= nr_etapa_atual_sol_w +1;
				end if;
				
				if	(nr_etapa_p > 0) then
					nr_etapa_atual_sol_w := nr_etapa_p;
				end if;

				select	coalesce(max(a.nm_usuario), nm_usuario_p) nm_usuario
				into	nm_usuario_w
				from	usuario a
				where	upper(a.ds_login) = upper(nm_usuario_p);

				select	nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_mat_hor a,
					prescr_solucao_evento b
				where	rownum = 1
				and	a.nr_etapa_sol = nr_etapa_atual_sol_w
				and	a.nr_prescricao = nr_prescricao_p
				and	a.nr_seq_solucao = cd_item_p
				and	a.nr_etapa_sol = b.nr_etapa_evento
				and	((a.dt_primeira_checagem is not null) or (ie_acao_p = 99))
				and	a.dt_fim_horario is null
				and	a.dt_suspensao is null
				and	b.ie_alteracao = 37
				and	upper(b.nm_usuario) = upper(nm_usuario_w)
				and	b.nr_sequencia = (	select	max(b.nr_sequencia)
							from	prescr_mat_hor a,
								prescr_solucao_evento b
							where	a.nr_etapa_sol = nr_etapa_atual_sol_w
							and	a.nr_prescricao = nr_prescricao_p
							and	a.nr_seq_solucao = cd_item_p
							and	a.nr_etapa_sol = b.nr_etapa_evento
							and	((a.dt_primeira_checagem is not null) or (ie_acao_p = 99))
							and	a.dt_fim_horario is null
							and	a.dt_suspensao is null
							and	b.ie_alteracao = 37);
								
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(160630);
				end if;
			end if;
		end if;	
		
	elsif	(ie_tipo_item_p = 'DI')	then -- verifica dialise
	
		ie_retorno_w	:= nvl(obter_se_medic_dupla_chec(cd_item_p, wheb_usuario_pck.get_cd_estabelecimento, nr_prescricao_p),'N');
		
		if	(ie_retorno_w = 'N') then		
			select	nvl(max('S'), 'N')
			into	ie_retorno_w
			from	material
			where	cd_material in(	  select 	cd_material
						  from 		prescr_material
						  where 	nr_sequencia_solucao = cd_item_p
						  and 		nr_prescricao = nr_prescricao_p)
			and	nvl(ie_dupla_checagem,'N') = 'S';
		end if;
		
		if	(ie_retorno_w = 'S') then
			if	(ie_opcao_p = 'MMU') then
			
				nr_etapa_atual_sol_w := obter_etapa_atual_DI(nr_prescricao_p, cd_item_p);
				
				if	(nr_etapa_p > 0) then
					nr_etapa_atual_sol_w := nr_etapa_p;
				end if;
				
				select 	nvl(max('S'),'N')
				into 	ie_retorno_w
				from 	prescr_mat_hor a,
						hd_prescricao_evento b
				where 	rownum = 1
				and 	a.nr_etapa_sol 		= nr_etapa_atual_sol_w
				and		a.nr_prescricao 	= nr_prescricao_p
				and		a.nr_seq_solucao	= cd_item_p
				and 	a.nr_etapa_sol		= b.nr_etapa_evento
				and 	a.dt_primeira_checagem is not null
				and		a.dt_fim_horario	is null
				and		a.dt_suspensao		is null
				and		b.ie_evento			= 'PC'
				and 	upper(b.nm_usuario) = upper(nm_usuario_p)
				and		b.nr_sequencia		= ( select 	max(x.nr_sequencia)
												from 	hd_prescricao_evento x
												where 	x.nr_prescricao 	= nr_prescricao_p
												and		x.nr_seq_solucao	= cd_item_p );
				
				if (ie_retorno_w = 'N') then
					select 	nvl(max('S'),'N')
					into 	ie_retorno_w 
					from 	hd_prescricao_evento
					where 	nr_prescricao 		= nr_prescricao_p  
					and		nr_seq_solucao 		= cd_item_p
					and		ie_evento			= 'PC'
					and 	upper(nm_usuario) 	= upper(nm_usuario_p)
					and		nr_sequencia		= ( select 	max(x.nr_sequencia)
													from 	hd_prescricao_evento x
													where 	x.nr_prescricao 	= nr_prescricao_p
													and		x.nr_seq_solucao	= cd_item_p );
				end if;
				
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(160630);
				end if;
			end if;
		end if;
		
	
	elsif	(ie_tipo_item_p in ('SNE')) then -- Verifica solucoes	
	
		ie_retorno_w	:= nvl(obter_se_medic_dupla_chec(cd_item_p, wheb_usuario_pck.get_cd_estabelecimento, nr_prescricao_p),'N');
		
		if	(ie_retorno_w = 'N') then		
			select	nvl(max('S'), 'N')
			into	ie_retorno_w
			from	material
			where	cd_material = cd_item_p
			and		nvl(ie_dupla_checagem,'N') = 'S';
			
		end if;
		
		if	(ie_retorno_w = 'S') then
			if	(ie_opcao_p = 'MPC') then
			
				select  nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_mat_hor
				where	rownum = 1
				and		nr_sequencia = nr_seq_horario_p				
				and		dt_primeira_checagem is null
				and		dt_fim_horario is null
				and		dt_suspensao is null;	
				
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(135872);
				end if;
				
			elsif	(ie_opcao_p = 'MMU') then
			
				select	max(nr_sequencia)
				into	nr_seq_item_w
				from	prescr_material
				where	nr_prescricao	= nr_prescricao_p
				and		cd_material		= cd_item_p;
			
				nr_etapa_atual_sol_w	:= obter_etapa_atual_sne(nr_prescricao_p,nr_seq_item_w);
	
				if	(nr_etapa_atual_sol_w = 0) then
					nr_etapa_atual_sol_w	:= nr_etapa_atual_sol_w + 1;
				end if;

				nr_etapa_atual_w	:= nr_etapa_atual_sol_w;

				select	min(nr_etapa_sol)
				into	nr_etapa_atual_w
				from	prescr_mat_hor
				where	nr_prescricao	= nr_prescricao_p
				and		nr_seq_material	= nr_seq_item_w
				and		nr_etapa_sol	>= nr_etapa_atual_sol_w
				and		dt_suspensao is null
				and		nvl(ie_horario_especial,'N') <> 'S'
				and		dt_inicio_horario is null;

				select	nvl(max('S'),'N')
				into	ie_retorno_w
				from	prescr_mat_hor a,
						prescr_solucao_evento b
				where	rownum = 1
				and		a.nr_prescricao = b.nr_prescricao
				and		a.nr_etapa_sol = b.nr_etapa_evento
				and		a.nr_prescricao = nr_prescricao_p
				and		a.cd_material = cd_item_p
				and		b.ie_evento_valido = 'S'
				and		b.nr_etapa_evento 	= nr_etapa_atual_w
				and		((a.dt_primeira_checagem is not null) or (ie_acao_p = 99))
				and		a.dt_fim_horario is null
				and		a.dt_suspensao is null
				and		b.ie_alteracao = 37
				and		upper(b.nm_usuario) = upper(nm_usuario_p);
								
				if	(ie_retorno_w = 'S') then
					ds_retorno_p	:= Wheb_mensagem_pck.get_texto(160630);
				end if;
			end if;
		end if;	
		
	end if;
end if;

end Consulta_inf_prim_checagem;
/
