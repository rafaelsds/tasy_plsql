create or replace
procedure pls_migracao_contrato_web ( 	nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
					nr_seq_contrato_atual_p		pls_contrato.nr_sequencia%type,
					nr_seq_contrato_mig_p		pls_contrato.nr_sequencia%type,
					dt_migracao_p   		pls_migracao_beneficiario.dt_migracao%type,
					dt_limite_utilizacao_p		pls_migracao_beneficiario.dt_limite_utilizacao%type,
					nr_seq_pagador_p		pls_contrato_pagador.nr_sequencia%type,
					nr_seq_plano_p			pls_plano.nr_sequencia%type,
					nr_seq_tabela_preco_p		pls_tabela_preco.nr_sequencia%type,
					ie_migrar_dependente_p		pls_migracao_beneficiario.ie_migrar_dependente%type,
					nm_usuario_p			Varchar2, 
					nr_seq_migacao_p	out	pls_migracao_beneficiario.nr_sequencia%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Solicitar a migra��o de contrato, no acesso de Relacionamento cliente.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_migacao_w	pls_migracao_beneficiario.nr_sequencia%type;

begin

begin
	select	max(nr_sequencia)
	into	nr_seq_migacao_w
	from	pls_migracao_beneficiario
	where	nr_seq_segurado = nr_seq_segurado_p
	and	ie_status = 1; --Solicita��a em aberto
exception
when others then
	nr_seq_migacao_w	:= null;
end;	

if	(nr_seq_migacao_w is not null) then
	
	update	pls_migracao_beneficiario 
	set	nr_seq_contrato_mig 	= nr_seq_contrato_mig_p,
		dt_migracao 		= dt_migracao_p,
		dt_limite_utilizacao	= dt_limite_utilizacao_p,
		nr_seq_pagador 		= nr_seq_pagador_p,
		nr_seq_plano 		= nr_seq_plano_p,
		nr_seq_tabela_preco 	= nr_seq_tabela_preco_p,
		nm_usuario 		= nm_usuario_p,
		dt_solicitacao		= sysdate,
		ie_migrar_dependente	= ie_migrar_dependente_p
	where	nr_sequencia 		= nr_seq_migacao_w;
else
	insert into pls_migracao_beneficiario 
		(	nr_seq_contrato_mig, dt_migracao, dt_limite_utilizacao,
			nr_seq_pagador, nr_seq_plano, nr_seq_tabela_preco,
			nr_seq_segurado, nm_usuario, dt_atualizacao,
			nr_sequencia, nr_seq_contrato_atual, ie_status,
			dt_solicitacao,ie_migrar_dependente)
	values	(	nr_seq_contrato_mig_p, dt_migracao_p, dt_limite_utilizacao_p,
			nr_seq_pagador_p, nr_seq_plano_p, nr_seq_tabela_preco_p,
			nr_seq_segurado_p, nm_usuario_p, sysdate,
			pls_migracao_beneficiario_seq.nextval, nr_seq_contrato_atual_p, 1,
			sysdate, ie_migrar_dependente_p) returning nr_sequencia into nr_seq_migacao_w;
			
end if;

nr_seq_migacao_p	:= nr_seq_migacao_w;

commit;

end pls_migracao_contrato_web;
/
