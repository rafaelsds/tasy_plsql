create or replace
procedure GRAVA_LOG_INTEGRA_SYSPEC(
			NM_USUARIO_P			Varchar2,
			IE_ENVIO_P				Varchar2,
			NR_ATENDIMENTO_P		Number,
			CD_USUARIO_CONVENIO_P 	Varchar2,
			NR_DOC_CONVENIO_P  		Varchar2,
			NR_CPF_P  				Varchar2,
			DS_URL_P				Varchar2,
			DS_MENSAGEM_P			Varchar2) is 

begin

insert into LOG_INTEGRA_SYSPEC(
  NR_SEQUENCIA  ,
  DT_ATUALIZACAO  ,
  NM_USUARIO  ,
  IE_ENVIO ,
  NR_ATENDIMENTO  ,
  DS_LOG
) values (
	LOG_INTEGRA_SYSPEC_seq.nextval,
	sysdate,
	NM_USUARIO_P,
	IE_ENVIO_P,
	NR_ATENDIMENTO_P,
	substr(
		'NR_ATENDIMENTO: ' || NR_ATENDIMENTO_P || CHR(10) || CHR(13) ||
		'CD_USUARIO_CONVENIO: ' || CD_USUARIO_CONVENIO_P || CHR(10) || CHR(13) ||
		'NR_DOC_CONVENIO: ' || NR_DOC_CONVENIO_P || CHR(10) || CHR(13) ||
		'NR_CPF: ' || NR_CPF_P || CHR(10) || CHR(13) ||
		'URL: ' || DS_URL_P || CHR(10) || CHR(13) ||
		'MENSAGEM_RETORNO: ' || DS_MENSAGEM_P
	,1,4000)
);  

commit;

end GRAVA_LOG_INTEGRA_SYSPEC;
/