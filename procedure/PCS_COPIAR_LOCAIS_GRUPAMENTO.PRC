create or replace
procedure pcs_copiar_locais_grupamento(	nr_seq_grup_origem_p		number,
					nr_seq_grup_destino_p		number,
					ie_substituir_regras_p		varchar2,
					nm_usuario_p			varchar2) is 

begin

if	(ie_substituir_regras_p = 'S') then
	delete from pcs_local_estoque_grup
	where nr_seq_grupamento = nr_seq_grup_destino_p;
end if;

insert into pcs_local_estoque_grup(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_grupamento,
	cd_estab_regra,
	cd_local_estoque,
	ds_grupamento)
select	pcs_local_estoque_grup_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_grup_destino_p,
	cd_estab_regra,
	cd_local_estoque,
	pcs_obter_dados_grupamento(nr_seq_grup_destino_p,'DG')
from	pcs_local_estoque_grup
where	nr_seq_grupamento = nr_seq_grup_origem_p;

commit;

end pcs_copiar_locais_grupamento;
/
