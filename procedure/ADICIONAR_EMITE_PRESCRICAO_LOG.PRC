create or replace procedure adicionar_emite_prescricao_log(
NM_USUARIO_P VARCHAR2,
DS_FILTROS_P VARCHAR2,
NR_PRESCRICAO_P NUMBER,
NR_SEQ_PRESCRICAO_P NUMBER,
DS_OBSERVACAO_ERRO_P CLOB,
DS_OBSERVACAO_P CLOB,
NM_COMPUTADOR_P VARCHAR2) is
NR_SEQUENCIA_W emite_prescricao_log.nr_sequencia%type;
begin
    select emite_prescricao_log_seq.nextval into NR_SEQUENCIA_W from dual;
	insert into emite_prescricao_log (
        NR_SEQUENCIA,
        DT_ATUALIZACAO,
        NM_USUARIO,
        DT_ATUALIZACAO_NREC,
        NM_USUARIO_NREC,
        DS_FILTROS,
        CD_PERFIL,
        NR_PRESCRICAO,
        NR_SEQ_PRESCRICAO,
        DS_OBSERVACAO_ERRO,
        DS_OBSERVACAO,
        NM_COMPUTADOR)
	values (
        NR_SEQUENCIA_W,
        sysdate,
        NM_USUARIO_P,
        sysdate,
        NM_USUARIO_P,
        DS_FILTROS_P,
        Obter_perfil_Ativo,
        NR_PRESCRICAO_P,
        NR_SEQ_PRESCRICAO_P,
        DS_OBSERVACAO_ERRO_P,
        DS_OBSERVACAO_P,
        NM_COMPUTADOR_P);
	commit;
end adicionar_emite_prescricao_log;
/