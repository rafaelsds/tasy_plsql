create or replace
procedure Gerar_prescricao_futura(	nr_prescricao_p			number,
				qt_dias_p				number,
				nm_usuario_p			varchar2,
				ds_lista_dias_p			varchar2,
				cd_estabelecimento_p		number,
				nr_prescricoes_p		out	varchar2,
				ds_erro_p		out	varchar2) is 

type Dia is record (dt_data Date);
type vetor is table of dia index by binary_integer;			
			
nr_atendimento_w		number(10);
cd_medico_w		varchar2(10);
ie_erro_w		varchar2(1);
qt_hora_ww		varchar2(100);
qt_hora_w		varchar2(100);
VarConsisteDataFutura_w	varchar2(1);
dt_prescricao_w		Date;
i			integer := 1;
x			integer := 1;
nr_nova_prescricao_w	number(14);
dt_primeiro_horario_w	Varchar2(5);
ie_gerar_kit_w		Varchar2(5);
cd_pessoa_fisica_w		Varchar2(255);
ds_erro_w		Varchar2(4000);
ds_erro_w2		Varchar2(4000);
cd_setor_atendimento_w	number(5,0);
nr_horas_validade_w	number(5,0);
ie_medico_w		Varchar2(5);
ds_lista_dias_w		varchar2(2000);
ie_calc_validade_w		Varchar2(5);
ie_estend_validade_w	Varchar2(5);
ie_prim_hor_cp_setor_w	Varchar2(5);
nr_prescricoes_w		Varchar2(1000);
ie_gerar_dia_w		boolean;
ie_setar_hor_rep_w	varchar2(1);
cd_medic_posicionar_w	number(15);
qt_hor_fut_sem_atend_w		number(10);

vt_dias_w		vetor;
/* Extrai um valor de uma string */
function pushVlString(	String_pp		in out	varchar2,
			ds_separador_pp		varchar2) 
			return varchar2 is
dsValor_w 	varchar2(30);
j		integer;

begin
dsValor_w := '';
j := 1;
while  j <= length(String_pp) loop
	if	(substr(String_pp,j,1) <> ds_separador_pp) then
		dsValor_w	:=  dsValor_w||substr(String_pp,j,1);
	else
		String_pp		:= substr(String_pp,j+1, length(String_pp));
		return dsValor_w;
	end if;
	j:= j+1;
end loop;

String_pp := null;
return dsValor_w;
end;

BEGIN
ds_lista_dias_w := ds_lista_dias_p;
while ds_lista_dias_w is not null loop 
	begin
	vt_dias_w(x).dt_data := to_date(pushVlString(ds_lista_dias_w, ','),'dd/mm/yyyy hh24:mi:ss');
	x := x + 1;
	end;
end loop;

obter_param_usuario(924,5, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_kit_w);
wheb_assist_pck.obterValorParametroREP(22,qt_hora_ww);
wheb_assist_pck.obterValorParametroREP(44,VarConsisteDataFutura_w);
obter_param_usuario(924,98, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_calc_validade_w);
obter_param_usuario(924,249, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_estend_validade_w);
obter_param_usuario(924,306, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_prim_hor_cp_setor_w);

select	max(nr_atendimento),
		max(cd_medico),
		max(dt_prescricao),
		max(cd_setor_atendimento),
		to_char(max(dt_primeiro_horario),'hh24:mi')
into	nr_atendimento_w,
		cd_medico_w,
		dt_prescricao_w,
		cd_setor_atendimento_w,
		dt_primeiro_horario_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select	max(cd_pessoa_Fisica)
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario = nm_usuario_p;

select	nvl(max('S'),'N')
into	ie_medico_w
from	medico
where	ie_situacao	= 'A'
and	cd_pessoa_fisica = cd_pessoa_fisica_w;

update	prescr_medica
set	ds_de_ate = '1/'||to_char(qt_dias_p + 1)
where	nr_prescricao = nr_prescricao_p;

select	nvl(max(ie_setar_hor_rep),'N')
into	ie_setar_hor_rep_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_w;

if	(ie_prim_hor_cp_setor_w = 'S') or
	((ie_prim_hor_cp_setor_w = 'D') and
	 (ie_setar_hor_rep_w	= 'S')) then
	dt_primeiro_horario_w	:= to_char(obter_prim_hor_setor(cd_setor_atendimento_w),'hh24:mi');
else
	if (dt_primeiro_horario_w is null) then
		dt_primeiro_horario_w	:= to_char(Obter_Prim_Horario_Prescricao(nvl(nr_atendimento_w,0),cd_setor_atendimento_w,dt_prescricao_w,nm_usuario_p,'A'),'hh24:mi');
	end if;
end if;

Select	max(dt_validade_prescr + 1/86400)
into	dt_prescricao_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

nr_horas_validade_w  := 24;	

nr_nova_prescricao_w	:= nr_prescricao_p;

while	(i <= qt_dias_p) loop
	if (vt_dias_w.count > 0) then
		ie_gerar_dia_w := false;
		for k in 1..vt_dias_w.count loop
			begin
			if (trunc(vt_dias_w(k).dt_data) = trunc(dt_prescricao_w + i-1)) then
				ie_gerar_dia_w := true;
			end if;
			end;
		end loop;
	else
		ie_gerar_dia_w := true;
	end if;
	
	if	(ie_gerar_dia_w) then

		ie_erro_w := 'N';

		qt_hora_w	:= Campo_Numerico(qt_hora_ww);

		IF	((NVL(nr_atendimento_w,0) > 0) OR
			 (VarConsisteDataFutura_w = 'S')) THEN
			wheb_assist_pck.obterValorParametroREP(1091,qt_hor_fut_sem_atend_w);
			IF	(NVL(nr_atendimento_w,0) = 0) AND
				(NVL(qt_hor_fut_sem_atend_w,0) > 0) THEN
				qt_hora_w	:=	qt_hor_fut_sem_atend_w;
			END IF;

			IF	(qt_hora_w > 0) AND
				((SYSDATE + qt_hora_w / 24) < dt_prescricao_w + i - 1) THEN
				--'A data da prescri��o n�o pode ser superior a ' ||qt_hora_w || ' horas a partir deste momento !');
				-- Wheb_mensagem_pck.exibir_mensagem_abort(178446,'QT_HORAS='||qt_hora_w);
				ie_erro_w := 'S';
			END IF;

		END IF;

		IF	(ie_erro_w <> 'S') THEN

			Copia_Prescricao(nr_nova_prescricao_w, nr_atendimento_w, nm_usuario_p, cd_medico_w,
					dt_prescricao_w + i - 1, 'S','S','S',0,'N',NULL,dt_primeiro_horario_w,
					dt_prescricao_w,NULL,'N', NULL, NULL, nr_horas_validade_w, obter_perfil_ativo,
					nr_nova_prescricao_w, nr_prescricao_p, NULL, cd_estabelecimento_p, 'N', cd_medic_posicionar_w);

			IF	(ie_gerar_kit_w = 'S') THEN
				gerar_kit_prescricao(cd_estabelecimento_p, nr_nova_prescricao_w, NULL, nm_usuario_p);
			END IF;

			nr_prescricoes_w := nr_prescricoes_w || nr_nova_prescricao_w || ',';

			liberar_prescricao(nr_nova_prescricao_w, nr_atendimento_w, ie_medico_w, Obter_perfil_ativo, nm_usuario_p, 'S', ds_erro_w);

		/* OS 130860 - trocado pelo gerar prescri��o conforme conversado com o Edilson. */
			UPDATE	prescr_medica
			SET		ds_de_ate		= TO_CHAR(i+1)||'/'||TO_CHAR(qt_dias_p + 1),
					nr_prescr_orig_fut	= nr_prescricao_p
			WHERE	nr_prescricao		= nr_nova_prescricao_w;

			COMMIT;

		ELSE

			 ds_erro_w2 := TO_CHAR(dt_prescricao_w + i - 1, 'dd/mm/yyyy hh24:mi:ss');
			exit;
		END IF;
		
	end if;
	i := i + 1;
end loop;

nr_prescricoes_p := substr(nr_prescricoes_w, 1, length(nr_prescricoes_w) - 1);
ds_erro_p := ds_erro_w2;

update	prescr_medica
set		nr_prescr_orig_fut	= nr_prescricao_p
where	nr_prescricao		= nr_prescricao_p;
commit;

END Gerar_prescricao_futura;
/
