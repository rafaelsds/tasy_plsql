create or replace procedure update_nr_seq_classif_esp( 
          nr_seq_classif_esp_p     number,  
          nr_atendimento_p         number) is          
 
begin

UPDATE 
	atendimento_paciente 
SET 
	nr_seq_classif_esp = nr_seq_classif_esp_p 
WHERE 
	nr_atendimento = nr_atendimento_p;
commit;
end update_nr_seq_classif_esp;
/