create or replace
procedure pls_copiar_item_contr_regra
		(	nr_seq_item_p		number,
			cd_perfil_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 

begin

insert into pls_item_contr_regra_lib
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		cd_estabelecimento,cd_perfil,nr_seq_item)
values	(	pls_item_contr_regra_lib_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		cd_estabelecimento_p,cd_perfil_p,nr_seq_item_p);

commit;

end pls_copiar_item_contr_regra;
/