create or replace
procedure Obter_Proc_Tab_Interno_Conv
		(nr_seq_interno_p	in number,
		cd_estabelecimento_p	in number,
		cd_convenio_p		in number,
		cd_categoria_p		in varchar2,
		cd_plano_p		in varchar2,
		cd_setor_atendimento_p	in number,
		cd_procedimento_p		out number,
		ie_origem_proced_p	out number,
		cd_setor_item_p		number default null,
		dt_vigencia_p		date default null,
		cd_tipo_acomodacao_p	number,
		ie_vago_varchar1_p	varchar2,
		ie_vago_varchar2_p	varchar2,
		ie_vago_varchar3_p	varchar2,
		ie_tipo_atendimento_p	number,
		ie_vago_number2_p	number,
		ie_vago_number3_p	number,
		ie_vago_date_p		date) is

cd_convenio_w			number(05,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_edicao_amb_w			number(06,0);
cd_estabelecimento_w		number(04,0);
ie_tipo_atendimento_w		number(03,0);
cd_categoria_w			varchar2(10);
vl_retorno_w			number(15,4);
ie_preco_informado_w		varchar2(10);
ie_glosa_w			varchar2(10);
cd_edicao_ajuste_w		number(06,0);
ie_atend_retorno_w			varchar2(01);
ie_prioridade_edicao_w		varchar2(01);	
IE_PRIORIDADE_AJUSTE_PROC_w	varchar2(01);
VL_CH_HONORARIOS_W		NUMBER(15,4)	:= 1;
VL_CH_CUSTO_OPER_W		NUMBER(15,4)	:= 1;
VL_M2_FILME_W			NUMBER(15,4)	:= 0;
tx_ajuste_geral_w		number(15,4)	:= 1;
dt_inicio_vigencia_w		date;
dt_conta_w			date := sysdate;
ie_autor_particular_w		varchar2(1);
cd_convenio_glosa_ww		number(5);
cd_categoria_glosa_ww		varchar2(10);
nr_seq_ajuste_proc_ww		number(10);
ie_tipo_convenio_w		number(2);
ie_origem_proc_filtro_w		number(10); 

nr_seq_grupo_dif_fat_w		number(10,0);
cd_edicao_amb_grupo_dif_w	number(6,0);
nr_seq_cbhpm_edicao_w		number(10,0);
dt_vigencia_w			date;

ie_proc_interno_conv_w		varchar2(1);
ie_regra_prior_edicao_w		varchar2(3);
cd_edicao_amb_prior_w		number(6,0);
qt_proc_edicao_w		number(10,0);
ie_encontrou_edicao_w		varchar2(1);
nr_seq_proc_conv_w		number(10,0);
ie_prioridade_conv_w		number(10,0);
dt_inicio_vig_conv_w		date;
nr_seq_cbhpm_edicao_regra_w	number(10,0);
qt_preco_w			number(10,0);

cd_proc_prior_w			procedimento.cd_procedimento%type;
ie_origem_proc_prior_w		procedimento.ie_origem_proced%type;
ie_origem_proced_edicao_w	edicao_amb.cd_edicao_amb%type;
nr_seq_cbhpm_edicao_prior_w	convenio_amb.nr_seq_cbhpm_edicao%type;

Cursor C01 is 
select	cd_procedimento,
	ie_origem_proced
from	proc_interno_conv
where	nr_seq_proc_interno	= nr_seq_interno_p
and	((cd_convenio		= cd_convenio_w OR cd_convenio IS NULL))
and	((cd_edicao_amb		= cd_edicao_amb_w OR cd_edicao_amb IS NULL))
and	((ie_tipo_atendimento	= ie_tipo_atendimento_w OR ie_tipo_atendimento IS NULL))
and 	((ie_origem_proc_filtro = ie_origem_proc_filtro_w) or (ie_origem_proc_filtro is null))
and 	(((cd_setor_atendimento = nvl(cd_setor_atendimento_p,0) and nvl(ie_tipo_setor,'P') = 'P') or
	 (cd_setor_atendimento = nvl(cd_setor_item_p,0) and nvl(ie_tipo_setor,'P') = 'L')) or
	 (cd_setor_atendimento is null))		
and 	((cd_estabelecimento   = nvl(cd_estabelecimento_p, wheb_usuario_pck.get_cd_estabelecimento)) or (cd_estabelecimento is null))
and	(nvl(cd_categoria,nvl(cd_categoria_w,'0')) =  nvl(cd_categoria_w,'0'))
and	(nvl(cd_tipo_acomodacao,nvl(cd_tipo_acomodacao_p,0)) =  nvl(cd_tipo_acomodacao_p,0))
and	dt_vigencia_w between nvl(dt_inicio_vigencia, dt_vigencia_w) and nvl(dt_final_vigencia, dt_vigencia_w)
and 	nvl(nr_seq_cbhpm_edicao, nvl(nr_seq_cbhpm_edicao_w,0)) = nvl(nr_seq_cbhpm_edicao_w,0)
and	nvl(ie_situacao,'A') = 'A'
order by 
	nvl(cd_edicao_amb,0),
	nvl(nr_seq_cbhpm_edicao,0),
	nvl(cd_convenio,0),	
	nvl(ie_tipo_atendimento,0),
	nvl(cd_setor_atendimento,0),
	nvl(cd_estabelecimento,0),
	nvl(ie_tipo_setor,'P'),
	nvl(cd_categoria,'0'),
	nvl(cd_tipo_acomodacao,0);


cursor	c02 is
	SELECT	CD_EDICAO_AMB
	FROM 	CONVENIO_AMB
	WHERE 	(CD_ESTABELECIMENTO     = cd_estabelecimento_w)
	AND 	(CD_CONVENIO            = cd_convenio_w)
	AND 	(nvl(IE_SITUACAO,'A')   = 'A')
	AND 	(DT_INICIO_VIGENCIA     = 
			(SELECT	MAX(DT_INICIO_VIGENCIA)
			FROM 	CONVENIO_AMB A	
			WHERE 	(A.CD_ESTABELECIMENTO  	= cd_estabelecimento_w)
			AND 	(A.CD_CONVENIO         	= CD_CONVENIO_w)
			AND 	(nvl(A.IE_SITUACAO,'A')	= 'A')
			AND 	(A.DT_INICIO_VIGENCIA 	<=  sysdate)))
	order by ie_prioridade;

	
Cursor C03 is
	select	nvl(cd_edicao_amb,0)
	from	grupo_dif_fat_edicao_regra
	where	nr_seq_grupo_dif_fat = nr_seq_grupo_dif_fat_w
	and 	(nvl(cd_convenio, nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0))
	and 	(nvl(cd_categoria, nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0'))
	and 	(nvl(cd_plano, nvl(cd_plano_p,'0')) = nvl(cd_plano_p,'0'))
	and	nvl(ie_situacao,'A') = 'A'
	order by nvl(cd_convenio,0),
		nvl(cd_categoria,'0'),
		nvl(cd_plano,'0');
		
cursor	c04 is
	select	x.ie_prioridade,
		x.dt_inicio_vigencia,
		x.cd_edicao_amb,
		x.nr_seq_cbhpm_edicao
	from	edicao_amb y,
		convenio_amb x
	where	x.cd_edicao_amb = y.cd_edicao_amb
	and	x.cd_estabelecimento = cd_estabelecimento_p
	and	x.cd_convenio = cd_convenio_p
	and	((x.cd_categoria = nvl(cd_categoria_p,cd_categoria)) or (nvl(cd_categoria_p,'0') = '0'))
	and	nvl(x.ie_situacao,'A')	= 'A'
	and	ie_regra_prior_edicao_w	= 'DT'
	and	(x.dt_inicio_vigencia =	(select	max(dt_inicio_vigencia)
					from	convenio_amb a
					where	a.cd_estabelecimento = cd_estabelecimento_p
					and	a.cd_convenio = cd_convenio_p
					and	((a.cd_categoria = nvl(cd_categoria_p,cd_categoria)) or (nvl(cd_categoria_p,'0') = '0'))
					and	nvl(a.ie_situacao,'A')= 'A'
					and	dt_vigencia_w between a.dt_inicio_vigencia and nvl(a.dt_final_vigencia,dt_vigencia_w)))
	union
	select 	x.ie_prioridade,
		x.dt_inicio_vigencia,
		x.cd_edicao_amb,
		x.nr_seq_cbhpm_edicao
	from	edicao_amb y,
		convenio_amb x
	where	x.cd_edicao_amb = y.cd_edicao_amb
	and	x.cd_estabelecimento = cd_estabelecimento_p
	and	x.cd_convenio = cd_convenio_p
	and	((x.cd_categoria = nvl(cd_categoria_p,cd_categoria)) or (nvl(cd_categoria_p,'0') = '0'))
	and 	nvl(x.ie_situacao,'A')	= 'A'
	and	ie_regra_prior_edicao_w	= 'PR'
	and	((x.dt_final_vigencia is null) or (dt_vigencia_w <= x.dt_final_vigencia))
	order by	1, 2;
	
/*Este cursor deve ser igual ao C01, porem para verificar se existem as regras para as prioridades do C04*/
Cursor C05 is 
select	cd_procedimento,
	ie_origem_proced
from	proc_interno_conv
where	nr_seq_proc_interno	= nr_seq_interno_p
and	((cd_convenio		= cd_convenio_w OR cd_convenio IS NULL))
and	(cd_edicao_amb = cd_edicao_amb_prior_w)
and	((ie_tipo_atendimento	= ie_tipo_atendimento_w OR ie_tipo_atendimento IS NULL))
and 	((ie_origem_proc_filtro = ie_origem_proc_filtro_w) or (ie_origem_proc_filtro is null))
and 	(((cd_setor_atendimento = nvl(cd_setor_atendimento_p,0) and nvl(ie_tipo_setor,'P') = 'P') or
	 (cd_setor_atendimento = nvl(cd_setor_item_p,0) and nvl(ie_tipo_setor,'P') = 'L')) or
	 (cd_setor_atendimento is null))		
and 	((cd_estabelecimento   = nvl(cd_estabelecimento_p, wheb_usuario_pck.get_cd_estabelecimento)) or (cd_estabelecimento is null))
and	(nvl(cd_categoria,nvl(cd_categoria_w,'0')) =  nvl(cd_categoria_w,'0'))
and	(nvl(cd_tipo_acomodacao,nvl(cd_tipo_acomodacao_p,0)) =  nvl(cd_tipo_acomodacao_p,0))
and	dt_vigencia_w between nvl(dt_inicio_vigencia, dt_vigencia_w) and nvl(dt_final_vigencia, dt_vigencia_w)
and 	nvl(nr_seq_cbhpm_edicao, nvl(nr_seq_cbhpm_edicao_prior_w,0)) = nvl(nr_seq_cbhpm_edicao_prior_w,0)
and	nvl(ie_situacao,'A') = 'A'
order by 
	nvl(cd_edicao_amb,0),
	nvl(nr_seq_cbhpm_edicao,0),
	nvl(cd_convenio,0),	
	nvl(ie_tipo_atendimento,0),
	nvl(cd_setor_atendimento,0),
	nvl(cd_estabelecimento,0),
	nvl(ie_tipo_setor,'P'),
	nvl(cd_categoria,'0'),
	nvl(cd_tipo_acomodacao,0);

begin

cd_convenio_w		:= cd_convenio_p;
cd_categoria_w		:= cd_categoria_p;
cd_estabelecimento_w	:= cd_estabelecimento_p;
dt_vigencia_w		:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_vigencia_p, sysdate));
nr_seq_cbhpm_edicao_regra_w	:= null;
nr_seq_cbhpm_edicao_w		:= null;

select	max(cd_procedimento),
	max(ie_origem_proced),
	max(nr_seq_grupo_dif_fat)
into	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_grupo_dif_fat_w
from	proc_interno
where	nr_sequencia		= nr_seq_interno_p;

select	nvl(max(ie_prioridade_edicao_amb), 'N'),
	nvl(max(IE_PRIORIDADE_AJUSTE_PROC), 'N'),
	nvl(max(ie_proc_interno_conv), 'N')
into	ie_prioridade_edicao_w,
	IE_PRIORIDADE_AJUSTE_PROC_w,
	ie_proc_interno_conv_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(max(ie_regra_prior_edicao), 'DT')
into	ie_regra_prior_edicao_w
from	convenio_estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p
and	cd_convenio = cd_convenio_p;

if	(nvl(ie_tipo_atendimento_p,0) > 0) then
	ie_tipo_atendimento_w	:= ie_tipo_atendimento_p;
end if;

select 	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from 	convenio
where 	cd_convenio = cd_convenio_w;

ie_origem_proc_filtro_w:= Obter_Origem_Proced_Cat(cd_estabelecimento_w, ie_tipo_atendimento_w, ie_tipo_convenio_w, cd_convenio_w, cd_categoria_w);

if	(nvl(nr_seq_grupo_dif_fat_w,0) > 0) then
		
	CD_EDICAO_AMB_W:= 0;
		
	open C03;
	loop
	fetch C03 into	
		cd_edicao_amb_grupo_dif_w;
	exit when C03%notfound;
		begin
		CD_EDICAO_AMB_W:= cd_edicao_amb_grupo_dif_w;
		end;
	end loop;
	close C03;

end if;

if	((nvl(nr_seq_grupo_dif_fat_w,0) = 0) or 
	((nvl(nr_seq_grupo_dif_fat_w,0) > 0) and (CD_EDICAO_AMB_W = 0))) then

	if	(ie_prioridade_edicao_w = 'N') then	
		SELECT	nvl(max(CD_EDICAO_AMB),0),
			max(nr_seq_cbhpm_edicao)
		INTO	CD_EDICAO_AMB_W,
			nr_seq_cbhpm_edicao_w
		FROM 	CONVENIO_AMB
		WHERE 	(CD_ESTABELECIMENTO     = cd_estabelecimento_w)
		AND 	(CD_CONVENIO            = cd_convenio_w)
		AND 	(nvl(IE_SITUACAO,'A')   = 'A')
		and	((nvl(cd_categoria_w,'0') = '0') or (cd_categoria = cd_categoria_w))
		AND 	(DT_INICIO_VIGENCIA     = 
				(SELECT	MAX(DT_INICIO_VIGENCIA)
				FROM 	CONVENIO_AMB A	
				WHERE 	(A.CD_ESTABELECIMENTO  	= cd_estabelecimento_w)
				AND 	(A.CD_CONVENIO         	= CD_CONVENIO_w)
				AND 	(nvl(A.IE_SITUACAO,'A')	= 'A')
				and	((nvl(cd_categoria_w,'0') = '0') or (a.cd_categoria = cd_categoria_w))
				AND 	(A.DT_INICIO_VIGENCIA 	<=  sysdate)));
	else
	
		if	(ie_proc_interno_conv_w = 'S') then
			
			ie_encontrou_edicao_w	:= 'N';
		
			open C04;
			loop
			fetch C04 into
				ie_prioridade_conv_w,
				dt_inicio_vig_conv_w,
				cd_edicao_amb_prior_w,
				nr_seq_cbhpm_edicao_prior_w;
			exit when C04%notfound;
				begin
				
				if	(ie_encontrou_edicao_w = 'N') then
					cd_proc_prior_w		:= null;
					ie_origem_proc_prior_w	:= null;
				
					open C05;
					loop
					fetch C05 into	
						cd_proc_prior_w,
						ie_origem_proc_prior_w;
					exit when C05%notfound;
						begin
						ie_encontrou_edicao_w	:= 'S';
						cd_proc_prior_w		:= cd_proc_prior_w;
						ie_origem_proc_prior_w	:= ie_origem_proc_prior_w;
						end;
					end loop;
					close C05;
				
					if	(nvl(cd_proc_prior_w,0) > 0) then
						
						select	nvl(max(ie_origem_proced), ie_origem_proc_prior_w)
						into	ie_origem_proced_edicao_w
						from	edicao_amb
						where	cd_edicao_amb	= cd_edicao_amb_prior_w;
						
						if	(ie_origem_proced_edicao_w = 5) then
							select	count(*)
							into	qt_preco_w
							from	cbhpm_preco
							where	cd_procedimento		= cd_proc_prior_w
							and	ie_origem_proced	= ie_origem_proc_prior_w;
						else
							select	count(*)
							into	qt_preco_w
							from	preco_amb
							where	cd_edicao_amb		= cd_edicao_amb_prior_w
							and	cd_procedimento		= cd_proc_prior_w
							and	ie_origem_proced	= nvl(ie_origem_proc_prior_w, ie_origem_proced)
							and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_vigencia_p,sysdate)) between nvl(dt_inicio_vigencia,sysdate - 3650) and nvl(dt_final_vigencia, nvl(dt_vigencia_p,sysdate));
						end if;
						
						if	(nvl(qt_preco_w,0) = 0) then
							ie_encontrou_edicao_w	:= 'N';
						else
							cd_procedimento_w	:= cd_proc_prior_w;
							ie_origem_proced_w	:= ie_origem_proc_prior_w;
						end if;

					end if;
				end if;
				
				end;
			end loop;
			close C04;
		
		end if;
	
		Obter_Edicao_Proc_Conv
			(cd_estabelecimento_w,
			CD_CONVENIO_W,
			CD_CATEGORIA_W,
			dt_conta_w,
			cd_procedimento_w,
			CD_EDICAO_AMB_W,
			VL_CH_HONORARIOS_W,
			VL_CH_CUSTO_OPER_W,
			VL_M2_FILME_W,
			dt_inicio_vigencia_w,
			tx_ajuste_geral_w,
			nr_seq_cbhpm_edicao_w);

		if	(nvl(CD_EDICAO_AMB_W, 0) = 0) then
		
			open	c02;
			loop
			fetch	c02 into CD_EDICAO_AMB_W;
			exit when c02%notfound;
				begin
	
				CD_EDICAO_AMB_W	:= CD_EDICAO_AMB_W;

				end;
			end loop;
			close c02;

		end if;

	end if;	

	begin
	obter_regra_ajuste_proc
			(cd_estabelecimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			null,
			dt_vigencia_w,0,ie_tipo_atendimento_w,0,null,
			0,0,null,nr_seq_interno_p,
			null,cd_plano_p,null,null,null,
			vl_retorno_w,vl_retorno_w,vl_retorno_w,
			vl_retorno_w,vl_retorno_w,vl_retorno_w,
			ie_preco_informado_w,ie_glosa_w,
			vl_retorno_w,vl_retorno_w,
			cd_edicao_ajuste_w,vl_retorno_w,vl_retorno_w,vl_retorno_w,
			vl_retorno_w,vl_retorno_w,vl_retorno_w,vl_retorno_w,
			vl_retorno_w, ie_atend_retorno_w, 0, ie_autor_particular_w,
			cd_convenio_glosa_ww,
			cd_categoria_glosa_ww,
			nr_seq_ajuste_proc_ww,
			null,null,
			null,null,
			null,null,
			null,
			null,
			vl_retorno_w,
			null, null, null, null,
			null, null, null,null,
			null, null);	
	exception
	when others then
		cd_edicao_ajuste_w	:= 0;
	end;
		
	if	(cd_edicao_ajuste_w is not null) and
		(cd_edicao_ajuste_w <> 0) then
		cd_edicao_amb_w		:= cd_edicao_ajuste_w;
	end if;
	
	nr_seq_cbhpm_edicao_regra_w:= null;
	if	(nvl(nr_seq_ajuste_proc_ww,0) > 0) then
		select	max(nr_seq_cbhpm_edicao)
		into	nr_seq_cbhpm_edicao_regra_w
		from	regra_ajuste_proc
		where 	nr_sequencia		= nr_seq_ajuste_proc_ww;
	end if;
	
end if;

nr_seq_cbhpm_edicao_w:= nvl(nr_seq_cbhpm_edicao_regra_w, nr_seq_cbhpm_edicao_w);
	
if	(cd_convenio_w > 0) or
	(cd_edicao_amb_w > 0) or
	(ie_tipo_atendimento_w > 0) or
	(ie_origem_proc_filtro_w > 0) then 
		
	OPEN C01;
	LOOP
	FETCH C01 into
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when c01%notfound;
		cd_procedimento_w	:= cd_procedimento_w;
		ie_origem_proced_w	:= ie_origem_proced_w;
	END LOOP;
	CLOSE C01;

end if; 

cd_procedimento_p	:= cd_procedimento_w;
ie_origem_proced_p	:= ie_origem_proced_w;

end Obter_Proc_Tab_Interno_Conv;
/
