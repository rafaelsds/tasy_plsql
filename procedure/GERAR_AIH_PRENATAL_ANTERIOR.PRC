CREATE OR REPLACE
PROCEDURE Gerar_Aih_Prenatal_Anterior(
				nr_atendimento_p		number,
				nr_prenatal_p		out	number,
				ie_sexo_p		out	varchar2,
				nm_usuario_p	varchar2,
				cd_estabelecimento_p	number) is


cd_pessoa_fisica_w	varchar2(10);
ie_local_nr_gestante_w	varchar2(15);

BEGIN

ie_local_nr_gestante_w	:= nvl(obter_valor_param_usuario(1123, 204, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'A');

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

select	ie_sexo
into	ie_sexo_p
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

if	(ie_local_nr_gestante_w = 'P') then
	begin
	select	nvl(max(nr_gestante_pre_natal),0)
	into	nr_prenatal_p
	from	parto
	where	nr_atendimento = nr_atendimento_p;
	end;
elsif	(ie_local_nr_gestante_w = 'A')	then
		begin	
		select	nvl(max(nr_gestante_pre_natal),0)
		into	nr_prenatal_p
		from	atendimento_paciente
		where	nr_atendimento	=	nr_atendimento_p;

		if	(nr_prenatal_p	= 0) then
			begin
			select	nvl(max(nr_gestante_pre_natal),0)
			into	nr_prenatal_p
			from	sus_aih
			where	nr_atendimento	= nr_atendimento_p
			and	dt_emissao	= (	select	max(dt_emissao)
							from	sus_aih
							where	nr_atendimento = nr_atendimento_p
							and	nr_gestante_pre_natal is not null);
			end;
		end if;
		end;
end if;
END Gerar_Aih_Prenatal_Anterior;
/ 
