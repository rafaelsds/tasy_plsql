create or replace
procedure gerar_nf_entrada_transf_quimio(
				nr_ordem_compra_p	number,
				cd_estabelecimento_p	number,
				cd_operacao_nf_p		number,
				cd_nat_oper_nf_p		number,
				cd_serie_nf_p		varchar2,
				nr_nota_fiscal_p		varchar2,
				cd_local_estoque_p	number,
				ds_observacao_p		varchar2,
				nm_usuario_p		varchar2,
				ie_item_nf_p		varchar2,
				cd_setor_atendimento_p	number,
				nr_sequencia_p	out	number,
				ds_erro_p		out	varchar2,
				ds_erro_item_p	out	varchar2,
				ds_erro_nota_p	out	varchar2) as

nr_sequencia_w			Number(10);
cd_cgc_destino_w			varchar2(14);
cd_estab_origem_w			number(10);
cd_cgc_origem_w			varchar2(14);
nr_nota_fiscal_w			varchar2(255);
nr_sequencia_nf_w			number(10)	:= 9;
dt_emissao_w			date;
dt_entrada_saida_w		date;
cd_condicao_pagto_w		Number(10);
vl_frete_w			number(13,2);
qt_itens_nota_w			number(10);
ds_erro_w			varchar2(255) := '';
ds_erro_item_w			varchar2(255) := '';
ds_erro_nota_w			varchar2(255) := '';

/*Itens*/
nr_item_oci_w			Number(5);
cd_material_w			Number(6);
cd_unidade_medida_compra_w	varchar2(30);
vl_unitario_item_nf_w		number(13,4);
pr_descontos_w			number(13,4);
ds_material_direto_w		varchar2(4000);
ds_observacao_item_w		varchar2(255);
cd_centro_custo_w			Number(8);
cd_conta_contabil_w		varchar2(20);
pr_desc_financ_w			Number(7,4);
dt_prevista_entrega_w		date;
vl_desconto_oci_w			Number(13,2);
vl_desconto_w			Number(13,2);
nr_seq_conta_financeira_w		Number(10);
nr_item_nf_w			Number(5);
cd_unidade_medida_estoque_w	varchar2(30);
qt_prevista_entrega_w		Number(13,4);
qt_material_w			Number(13,4);
qt_item_estoque_w			Number(20,4);
vl_total_item_nf_w			Number(13,2);
vl_liquido_w			Number(13,2);
qt_conv_compra_estoque_w		Number(13,4);
cd_material_estoque_w		Number(6);
ie_tipo_conta_w			Number(5);
cd_centro_conta_w			Number(8);
qt_nota_w			number(05,0);
nr_seq_lote_w			number(10,0);
ie_calcula_nf_w			varchar2(01);

/* Lote */
nr_seq_lote_fornec_w		number(10);
ds_lote_fornec_w			varchar2(20);
ds_validade_w			varchar2(255);
ds_barra_w			varchar2(255);

-- Ordem C�digo de  BARRAS -> Se precisar usar mais tarde, olhar a procedure GERAR_NF_SAIDA_TRANSFERENCIA;
cursor c00 is
	select  	cd_material,
		cd_unidade_medida_compra,
		nr_item_oci,
		nr_seq_lote,
		qt_material
	from	ordem_compra_item_cb
	where	nr_ordem_compra 	= nr_ordem_compra_p
	and	ie_atende_recebe	= 'A'
	and	nr_seq_nota is null;

cursor c01 is
	select	b.nr_item_oci,
		b.cd_material,
		b.cd_unidade_medida_compra,
		b.vl_unitario_item_nf,
		nvl(b.pr_desconto,0),
		b.ds_complemento,
		b.ds_observacao,
		nvl(b.pr_desc_financ,0),
		nvl(b.vl_desconto,0),
		nvl(b.qt_item_nf,0),
		b.nr_seq_lote_fornec,
		b.cd_lote_fabricacao,
		to_char(b.dt_validade,'dd/mm/yyyy') ds_validade,
		b.cd_barra_material
	from	nota_fiscal a,
		nota_fiscal_item b
	WHERE  	a.nr_sequencia = b.nr_sequencia
	AND    	a.nr_ordem_compra = nr_ordem_compra_p
	AND    	a.ie_tipo_nota in ('ST', 'SE', 'SF','SD')
	AND    	a.cd_estabelecimento = cd_estab_origem_w
	and	a.dt_atualizacao_estoque is not null
	order 	by	b.nr_item_oci;

-- Item C�digo BARRAS -> Se precisar usar mais tarde, olhar a procedure GERAR_NF_SAIDA_TRANSFERENCIA;
cursor c02 is
	select	nr_seq_lote
	from	ordem_compra_item_cb
	where	cd_material 	= cd_material_w
	and	nr_item_oci	= nr_item_oci_w
	and	nr_ordem_compra	= nr_ordem_compra_p
	and	nr_seq_nota is null;

begin

dt_emissao_w		:= trunc(sysdate, 'dd');
dt_entrada_saida_w	:= sysdate;

select	cd_estab_transf,
	cd_condicao_pagamento,
	vl_frete
into	cd_estab_origem_w,
	cd_condicao_pagto_w,
	vl_frete_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

select	nota_fiscal_seq.nextval
into	nr_sequencia_w
from	dual;

select	substr(obter_cgc_estabelecimento(cd_estab_origem_w),1,14),
	substr(obter_cgc_estabelecimento(cd_estabelecimento_p),1,14)
into	cd_cgc_origem_w, /* CNPJ Estabelecimento EMitente da NF */
	cd_cgc_destino_w /* CNPJ do Estabelecimento que est� gerando a nota de entrada */
from	dual;

ie_calcula_nf_w	:= substr(nvl(obter_valor_param_usuario(221, 14, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);

if	(nvl(nr_nota_fiscal_p, '0') = '0') then
	begin
	select	nvl(max(somente_numero(nr_ultima_nf)), nr_sequencia_w) + 1
	into	nr_nota_fiscal_w
	from	serie_nota_fiscal
	where	cd_serie_nf 		= cd_serie_nf_p
	and	cd_estabelecimento 	= cd_estabelecimento_p;

	select	count(*)
	into	qt_nota_w
	from	nota_fiscal
	where	cd_estabelecimento = cd_estabelecimento_p
	and	cd_cgc_emitente 	= cd_cgc_origem_w
	and	cd_serie_nf 	= cd_serie_nf_p
	and	nr_nota_fiscal 	= nr_nota_fiscal_w;

	if	(qt_nota_w > 0) then
		select	(nvl(max(somente_numero(nr_nota_fiscal)),'0')+1)
		into	nr_nota_fiscal_w
		from	nota_fiscal
		where	cd_estabelecimento = cd_estabelecimento_p
		and	cd_cgc_emitente = cd_cgc_origem_w
		and	cd_serie_nf = cd_serie_nf_p;
	end if;
	end;
else
	nr_nota_fiscal_w := nr_nota_fiscal_p;
end if;

insert into nota_fiscal(
	nr_sequencia,		cd_estabelecimento,
	cd_cgc_emitente,		cd_serie_nf,
	nr_nota_fiscal,		nr_sequencia_nf,
	cd_operacao_nf,		dt_emissao,
	dt_entrada_saida,		ie_acao_nf,
	ie_emissao_nf,		ie_tipo_frete,
	vl_mercadoria,		vl_total_nota,
	qt_peso_bruto,		qt_peso_liquido,
	dt_atualizacao,		nm_usuario,
	cd_condicao_pagamento,	cd_cgc,
	cd_pessoa_fisica,		vl_ipi,
	vl_descontos,		vl_frete,
	vl_seguro,		vl_despesa_acessoria,
	ds_observacao,		cd_natureza_operacao,
	vl_desconto_rateio,		ie_situacao,
	nr_interno_conta,		nr_seq_protocolo,
	ds_obs_desconto_nf,	nr_seq_classif_fiscal,
	ie_tipo_nota,		nr_ordem_compra,
	ie_entregue_bloqueto,	cd_setor_digitacao)
values( nr_sequencia_w,		cd_estabelecimento_p,
	cd_cgc_origem_w,		cd_serie_nf_p,
	nr_nota_fiscal_w,		nr_sequencia_nf_w,
	cd_operacao_nf_p,		dt_emissao_w,
	dt_entrada_saida_w,	'1',
	'0',			'0',
	0,			0,
	0,			0,
	sysdate,			nm_usuario_p,
	cd_condicao_pagto_w,	cd_cgc_destino_w,
	null,			0,
	0,			vl_frete_w,
	0,			0,
	ds_observacao_p,		cd_nat_oper_nf_p,
	0,			'1',
	null,			null,
	null,			null,
	'EN',			nr_ordem_compra_p,
	'N',			cd_Setor_atendimento_p);

gerar_historico_nota_fiscal(nr_sequencia_w, nm_usuario_p, '17', WHEB_MENSAGEM_PCK.get_texto(281661));

if (ie_item_nf_p = 0) then
	OPEN C01;
	LOOP
	FETCH C01 into
		nr_item_oci_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		vl_unitario_item_nf_w,
		pr_descontos_w,
		ds_material_direto_w,
		ds_observacao_item_w,
		pr_desc_financ_w,
		vl_desconto_oci_w,
		qt_material_w,
		nr_seq_lote_fornec_w,
		ds_lote_fornec_w,
		ds_validade_w,
		ds_barra_w;
	exit when c01%notfound;
		begin
		select	nvl(max(nr_item_nf), 0) + 1
		into	nr_item_nf_w
		from	nota_fiscal_item
		where	nr_sequencia = nr_sequencia_w;

		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
			qt_conv_compra_estoque,
			cd_material_estoque
		into	cd_unidade_medida_estoque_w,
			qt_conv_compra_estoque_w,
			cd_material_estoque_w
		from	material
		where	cd_material = cd_material_w;

		qt_item_estoque_w := qt_material_w * qt_conv_compra_estoque_w;
		if	(cd_unidade_medida_compra_w = cd_unidade_medida_estoque_w) then
			qt_item_estoque_w := qt_material_w;
		end if;

		vl_total_item_nf_w		:= nvl((qt_material_w * vl_unitario_item_nf_w),0);
		vl_unitario_item_nf_w 	:= nvl(dividir(vl_total_item_nf_w,qt_material_w),0);
		vl_desconto_w		:= nvl((dividir((vl_total_item_nf_w * pr_descontos_w), 100) + NVL(vl_desconto_oci_w,0)),0);
		vl_liquido_w		:= nvl((vl_total_item_nf_w - vl_desconto_w),0);

		ie_tipo_conta_w	:= 3;
		if	(cd_centro_custo_w is null) then
			ie_tipo_conta_w	:= 2;
		end if;

		define_conta_material(
			cd_estabelecimento_p,
			cd_material_w,
			ie_tipo_conta_w,
			0,
			0,
			0,
			0,
			0,0,0,
			cd_local_estoque_p,
			cd_operacao_nf_p,
			trunc(sysdate),
			cd_conta_contabil_w,
			cd_centro_conta_w,
			null);

	insert into nota_fiscal_item(
		nr_sequencia,			cd_estabelecimento,
		cd_cgc_emitente,			cd_serie_nf,
		nr_nota_fiscal,			nr_sequencia_nf,
		nr_item_nf,			cd_natureza_operacao,
		qt_item_nf,			vl_unitario_item_nf,
		vl_total_item_nf,			dt_atualizacao,
		nm_usuario,			vl_frete,
		vl_desconto,			vl_despesa_acessoria,
		cd_material,			cd_local_estoque,
		ds_observacao,			ds_complemento,
		cd_unidade_medida_compra,		qt_item_estoque,
		cd_unidade_medida_estoque,		cd_conta_contabil,
		vl_desconto_rateio,			vl_seguro,
		cd_material_estoque,		nr_ordem_compra,
		vl_liquido,
		pr_desconto,			nr_item_oci,
		dt_entrega_ordem,			nr_seq_conta_financ,
		pr_desc_financ,			nr_seq_lote_fornec,
		cd_lote_fabricacao,			dt_validade,
		cd_sequencia_parametro)
	values(	nr_sequencia_w,			cd_estabelecimento_p,
		cd_cgc_origem_w,			cd_serie_nf_p,
		nr_nota_fiscal_w,			nr_sequencia_nf_w,
		nr_item_nf_w,			cd_nat_oper_nf_p,
		qt_material_w,			vl_unitario_item_nf_w,
		vl_total_item_nf_w,			sysdate,
		nm_usuario_p, 			nvl(vl_frete_w,0),
 		nvl(vl_desconto_w,0),		0,
 		cd_material_w, 			cd_local_estoque_p,
		'',				ds_material_direto_w,
		cd_unidade_medida_compra_w,	qt_item_estoque_w,
		cd_unidade_medida_estoque_w,	cd_conta_contabil_w,
		0,				0,
 		cd_material_estoque_w,		nr_ordem_compra_p,
		vl_liquido_w,
		0,				nr_item_oci_w,
		dt_prevista_entrega_w,		nr_seq_conta_financeira_w,
		0,				nr_seq_lote_fornec_w,
		ds_lote_fornec_w,			ds_validade_w,
		philips_contabil_pck.get_parametro_conta_contabil);
		
	if	(ds_barra_w is not null) then
		begin
		update  nota_fiscal_item
		set	cd_barra_material = ds_barra_w
		where   nr_item_nf = nr_item_nf_w
		and	nr_sequencia = nr_sequencia_w
		and	cd_material is not null
		and     ds_barras is null;
		end;
	end if;	
	end;
	end loop;
	close c01;
end if;

if	(ie_calcula_nf_w = 'S') then
	begin
	atualiza_total_nota_fiscal(nr_sequencia_w,nm_usuario_p);
	gerar_vencimento_nota_fiscal(nr_sequencia_w,nm_usuario_p);
	consistir_nota_fiscal(nr_sequencia_w,nm_usuario_p,ds_erro_item_w,ds_erro_nota_w);
	end;
end if;

select 	count(*)
into	qt_itens_nota_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_w;

if	(qt_itens_nota_w = 0) then
	begin
	delete nota_fiscal where nr_sequencia = nr_sequencia_w;
	nr_sequencia_w	:= 0;
	ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(281662);
	end;
end if;

if	(qt_itens_nota_w > 0) and
	(ds_erro_item_w is null) and
	(ds_erro_nota_w is null) and
	(ds_erro_w is null) then  
	begin
	if	(ie_calcula_nf_w = 'S') then
		Atualizar_Nota_Fiscal(nr_sequencia_w,'I',nm_usuario_p,3);
	end if;
	end;
end if;

if	(ds_erro_item_w is not null) or
	(ds_erro_nota_w is not null) then
	begin
	delete	nota_fiscal 
	where	nr_sequencia = nr_sequencia_w;

	nr_sequencia_w	:= 0;
	end;
end if;	

nr_sequencia_p	:= nr_sequencia_w;
ds_erro_p	:= substr(ds_erro_w,1,255);
ds_erro_item_p	:= substr(ds_erro_item_w,1,255);
ds_erro_nota_p	:= substr(ds_erro_nota_w,1,255);

if 	(ds_erro_p is null) and
	(ds_erro_item_p is null) and
	(ds_erro_nota_p is null) then
	
	if	(nvl(nr_nota_fiscal_p, '0') = '0') and
		(nvl(qt_nota_w, 0) = 0) then
		update	serie_nota_fiscal
		set	nr_ultima_nf = nr_nota_fiscal_w
		where	cd_serie_nf = cd_serie_nf_p
		and	cd_estabelecimento = cd_estabelecimento_p;
	end if;
end if;

commit;
end gerar_nf_entrada_transf_quimio;
/
