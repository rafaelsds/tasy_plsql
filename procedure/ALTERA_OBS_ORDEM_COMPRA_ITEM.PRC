CREATE OR REPLACE
PROCEDURE ALTERA_OBS_ORDEM_COMPRA_ITEM(
			ds_observacao_p		VARCHAR2,
			nr_ordem_compra_p   NUMBER,
			cd_material_p       NUMBER)IS 
ds_erro_w      VARCHAR2(255);
BEGIN	
	BEGIN
		UPDATE	ordem_compra_item
		   SET	ds_observacao = ds_observacao || ds_observacao_p
		 WHERE	nr_ordem_compra = nr_ordem_compra_p  
		   AND	cd_material = cd_material_p;
	EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;		
		ds_erro_w  := substr(sqlerrm, 1,255);
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(349843);
		END;
COMMIT;
END ALTERA_OBS_ORDEM_COMPRA_ITEM;
/