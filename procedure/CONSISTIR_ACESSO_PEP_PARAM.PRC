create or replace
procedure consistir_acesso_pep_param(	nr_atendimento_p	in number,
					cd_estabelecimento_p	in number,
					nm_usuario_p		in varchar2,
					ie_bloqueia_p		out varchar2,
					ds_consistencia_p	out varchar2,
					cd_pessoa_fisica_p	in varchar2 default null) is 

ie_forma_consistir_w		varchar2(15);
cd_setor_pac_w			number(10,0);
cd_setor_usuario_w		number(10,0);
ie_setor_pac_lib_w		varchar2(1);
cd_convenio_w			number(10);
ie_registro_pep_w		varchar2(10);
ie_param_inicio_enf_w		varchar2(10);
ie_inicio_enfer_w		varchar2(10);
dt_alta_w			date;
ie_tipo_atendimento_w		number(10);
cd_classif_pf_w			varchar2(50);
qt_reg_w			number(10);
nr_seq_classif_w		number(10);
ds_classif_w			varchar2(255);
qt_min_acesso_setor_w	number(10);
qt_minuto_w				number(10);
dt_saida_unid_w			date;
				
begin
obter_param_usuario(281,347,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_forma_consistir_w);
obter_param_usuario(281,1239,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, cd_classif_pf_w);
obter_param_usuario(281,1393,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, qt_min_acesso_setor_w);
/*
if	(nr_atendimento_p	> 0) then
	begin
	cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);
	exception
	when others then
		cd_convenio_w	:= 0;
	end;
	
	if	(cd_convenio_w	> 0) then
		select	nvl(max(IE_REGISTRO_PEP),'S')
		into	ie_registro_pep_w
		from	convenio_estabelecimento
		where	cd_convenio	= cd_convenio_w
		and	cd_estabelecimento = cd_estabelecimento_p;
		
		if	(ie_registro_pep_w	= 'N') then
			ds_consistencia_p	:= 'O conv�nio '||obter_nome_convenio(cd_convenio_w)||' n�o permite a utiliza��o do PEP.';
			ie_bloqueia_p		:= 'S';
		end if;
	end if;
	
end if;
*/


if	(nr_atendimento_p > 0) and (ie_forma_consistir_w <> 'N') then
	cd_setor_usuario_w	:= nvl(obter_dados_usuario_opcao(nm_usuario_p,'S'),0);
	cd_setor_pac_w		:= nvl(nvl(obter_setor_atendimento(nr_atendimento_p),Obter_Unidade_Atendimento(nr_atendimento_p,'IA','CS')),0);
	ie_setor_pac_lib_w	:= obter_se_setor_usuario(cd_setor_pac_w, nm_usuario_p);

	if	(ie_forma_consistir_w = 'BS') and
		(cd_setor_pac_w <> cd_setor_usuario_w) then
		ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306622, null); -- Voc� n�o possui autoriza��o para acessar este prontu�rio.
		ie_bloqueia_p		:= 'S';
	elsif	(ie_forma_consistir_w = 'AS') and
		(cd_setor_pac_w <> cd_setor_usuario_w) then
		ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306623, null); -- O setor do paciente n�o � o mesmo que o seu. Suas a��es est�o sendo registradas.
		ie_bloqueia_p		:= 'N';
	elsif	(ie_forma_consistir_w = 'BL') and
		(ie_setor_pac_lib_w = 'N') then
		ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306622, null); -- Voc� n�o possui autoriza��o para acessar este prontu�rio.
		ie_bloqueia_p		:= 'S';
	elsif	(ie_forma_consistir_w = 'AL') and
		(ie_setor_pac_lib_w = 'N') then
		ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306626, null); -- O setor do paciente n�o esta liberado para voc�. Suas a��es est�o sendo registradas.
		ie_bloqueia_p		:= 'N';
	elsif 	(ie_forma_consistir_w = 'BSL') and
		(cd_setor_pac_w	<> wheb_usuario_pck.get_cd_setor_atendimento) then
		ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306627, null); -- Voc� n�o possui autoriza��o para acessar este prontu�rio. O setor logado n�o � o mesmo do paciente.
		ie_bloqueia_p		:= 'S';
	elsif	(ie_forma_consistir_w = 'BLH') and
		(ie_setor_pac_lib_w = 'N') then
		select	max(dt_saida_unidade)
		into	dt_saida_unid_w
		from	atend_paciente_unidade
		where	nr_atendimento = nr_atendimento_p;
		
		if	(nvl(qt_min_acesso_setor_w,0) > 0) and
			(dt_saida_unid_w is not null) then
			
			select	(Obter_Hora_Entre_datas(dt_saida_unid_w,sysdate) * 60)
			into	qt_minuto_w
			from	dual;
			
			if	(qt_min_acesso_setor_w < qt_minuto_w) then
				ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306622, null); -- Voc� n�o possui autoriza��o para acessar este prontu�rio.
				ie_bloqueia_p		:= 'S';
			end if;
		end if;
	end if;
end if;

obter_param_usuario(281,1103,obter_perfil_ativo,nm_usuario_p, cd_estabelecimento_p,ie_param_inicio_enf_w);


if	(nr_atendimento_p > 0) and (ie_param_inicio_enf_w = 'N') then
	select	to_char(a.dt_inicio_atendimento,'hh24:mi'),
			dt_alta,
			ie_tipo_atendimento
	into	ie_inicio_enfer_W,
			dt_alta_w,
			ie_tipo_atendimento_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_p;
		
	if 	( ie_inicio_enfer_w is null) and
		(dt_alta_w	is null) and
		(ie_tipo_atendimento_w	= 3)then
		ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306621, null); -- N�o � poss�vel acessar este prontu�rio sem o in�cio da enfermagem. Par�metro [1103].
		ie_bloqueia_p		:= 'S';
	end if;
end if;

if	(cd_classif_pf_w is not null) then
	select	count(*)
	into	qt_reg_w
	from	pessoa_classif
	where	obter_se_contido(nr_seq_classif,cd_classif_pf_w) = 'S'
	and	cd_pessoa_fisica = cd_pessoa_fisica_p;

	
	if	(qt_reg_w = 0) then
		select	nvl(max(nr_seq_classif),0)
		into	nr_seq_classif_w
		from	pessoa_classif
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		if	(nr_seq_classif_w > 0) then
			select	max(ds_classificacao)
			into	ds_classif_w
                        from	classif_pessoa
                        where	nr_sequencia = nr_seq_classif_w;
			
			if	(Trim(ds_classif_w) <> '') then
				ds_consistencia_p := wheb_mensagem_pck.get_texto(306619, 'DS_CLASSIF_W=' || ds_classif_w); -- Voc� n�o tem permiss�o para visualizar este cadastro, cuja classifica��o � #@DS_CLASSIF_W#@ !
				ie_bloqueia_p := 'S';
			end if;
		end if;
	ds_consistencia_p := wheb_mensagem_pck.get_texto(306618, null); -- Voc� n�o tem permiss�o para visualizar este cadastro!
	ie_bloqueia_p := 'S';
	end if;
end if;


end consistir_acesso_pep_param;
/
