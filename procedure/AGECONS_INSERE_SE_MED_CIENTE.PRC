create or replace
procedure Agecons_insere_se_med_ciente(	ds_observacao_p		in varchar2,
					ie_ciente_p		in varchar2,
					cd_agenda_p		in number,
					dt_agenda_p		in date,
					nm_usuario_p		in varchar2) is


nr_sequencia_w		number(10,0);						

begin	

select	Agenda_Medico_Ciente_Seq.nextval
into	nr_sequencia_w
from	dual;

	begin	
	
	insert into	agenda_medico_ciente	(
						cd_agenda,
						dt_agenda,
						dt_atualizacao,
						dt_contato,
						ie_ciente,
						nm_usuario,
						nr_sequencia,						
						ds_observacao,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_situacao
						)
					values	(
						cd_agenda_p,
						dt_agenda_p,
						sysdate,
						sysdate,
						ie_ciente_p,
						nm_usuario_p,					
						nr_sequencia_w,						
						ds_observacao_p,
						sysdate,
						nm_usuario_p,
						'A'
						);
	end;

end Agecons_insere_se_med_ciente;
/
