create or replace
procedure wint_gerar_prescr_eup(	nr_atendimento_p	number) is 

qt_param_1231                   number(8);
qt_proc_regra_w					number(8);
nr_doc_convenio_w				varchar2(20);
ds_classe_medic_excl_cir_w 		varchar2(255);  /*funcao_parametro.vl_parametro%type;*/
ds_hora_fixa_w				varchar2(255);
ie_gerar_proc_adic_w			varchar2(1);
qt_ambos_w						number(3,0);
ie_emite_mapa_w				varchar2(255);
ie_amostra_w				varchar2(1);
ie_considera_regra_med_exame_w	varchar2(1);
ie_autorizacao_w			varchar2(03);
nr_seq_agenda_w				number(10);
ie_origem_inf_w				varchar2(1);
ie_atualizar_recoleta_w 		varchar2(255);
considera_regra_quantidade_w    varchar2(1); 								  
hr_inicio_w						Date;
nr_seq_regra_preco_w			regra_ajuste_proc.nr_sequencia%type;		
cd_setor_atend_prescr_medica_w		number(10);
ie_gera_opme_w			varchar2(1);
ie_chama_fanep_w		varchar2(10);
ie_data_resultado_w			varchar2(255);
cd_setor_prescricao_w			number(5,0);
qt_min_atraso_w			number(10);
ie_duplica_prescr_w		VARCHAR2(1);
ie_acumula_w			VARCHAR2(01);
cd_intervalo_w					varchar2(255);
ie_param_459_w					varchar2(1);
ie_param_843_w					varchar2(1);
ie_gerar_med_adicional_w		varchar2(1);
ie_adep_w			varchar2(3);
ie_prescr_dt_agenda_w		varchar2(1);
ie_registra_w			varchar2(1);
nr_seq_apresentacao_w		NUMBER(10);
nr_seq_lote_fornec_w		NUMBER(10);
ie_atualizar_obs_prescr_w		varchar2(1);
ie_gerar_kit_proc_w				varchar2(1);
ie_autorizados_w				varchar2(1);
ie_setor_exec_proc_int_w 		varchar2(1);
ie_somente_exclusivo_w			varchar2(1) := 'N';
ie_dt_maior_w					varchar2(1);
ie_gerar_proc_adic_previstos_w	varchar2(1) := 'N';
nr_prescricao_old_w		prescr_medica.nr_prescricao%type;
ie_dia_semana_final_w 			number(10);
ie_forma_atual_dt_result_w		exame_lab_regra_setor.ie_atul_data_result%type;
qt_min_entrega_w			number(10);
nr_prescricao_w				number(14,0);
cd_setor_atendimento_w			number(05,0) 	:= 0;
cd_setor_entrega_w			number(05,0) 	:= 0;
cd_setor_entrega_ww			varchar2(255);
ie_glosa_w				regra_ajuste_proc.ie_glosa%type;
ie_urgente_w				varchar2(1);
ie_data_w				varchar2(1);
cd_medico_executor_w			varchar2(10);
cd_setor_atend_usuario_w		number(5);
nr_seq_regra_w				number(10,0);
cd_medico_w				varchar2(10);
qt_dia_entrega_w			number(20,0);
ds_erro_conv_w				varchar2(255);
cd_material_exame_w			varchar2(20);
cd_material_exame_ww			varchar2(20);
dt_prescricao_w				date;
dt_prevista_w				date;
cd_cgc_w				varchar2(14);
ds_erro_w				number(10,0);
ie_atualizar_result_w			varchar2(1);
dt_resultado_w				Date;
ie_medico_executor_w			varchar2(1);
ie_regra_w				number(2,0);
ie_medico_prescr_w			varchar2(15);
cd_medico_prescr_w			varchar2(10);
cd_paciente_w				pessoa_fisica.cd_pessoa_fisica%type;
ie_medico_requisitante_w		VARCHAR2(1) := 'N';
nr_sequencia_w				Number(10,0);
cd_setor_atend_w			varchar2(255);
cd_setor_coleta_w			varchar2(255);
cd_setor_entrega_270_w			varchar2(10);
cd_setor_entrega_def_270_w		varchar2(10);
ie_atualizar_entrega_w			varchar2(1);
cd_setor_proc_interno_w			number(10);
ie_atualiza_setor_entrega		varchar2(1); 
cd_tipo_agenda_w			number(10);
nr_pos_sep_itens_w			number(10);
qt_passagem_setor_w			number(10);
ie_tipo_passagem_regra_w		varchar2(1);
nr_seq_interno_w			number(10);
cd_setor_proc_w				number(10,0);
ds_aux_w				varchar2(255);
cd_senha_w				varchar2(20);
cd_procedimento_w			NUMBER(15)		:= 0;
ie_origem_proced_w			NUMBER(10,0)	:= 0; 
cd_convenio_w				NUMBER(5);
cd_categoria_w				VARCHAR2(10);
cd_setor_proced_w			NUMBER(05,0);
ie_tipo_atendimento_w			NUMBER(3);
nr_seq_forma_laudo_w			number(10);	
cd_medico_agenda_w			VARCHAR2(10);
ie_atualizar_setor_agenda_w		VARCHAR2(3);
qt_prescr_gerada_w			number(10);
ie_agendamento_pac_prescr_w		varchar2(1);
ie_gerou_agend_pac_prescr_w		varchar2(1) := 'N';
ie_exames_lab_w				varchar2(1);
ie_restringe_estab_w			varchar2(3);
ie_tipo_data_w				varchar2(3);
cd_pessoa_fisica_w			atendimento_paciente.cd_pessoa_fisica%type;
dt_entrada_w				atendimento_paciente.dt_entrada%type;
nr_seq_ageint_w				number(10);
nr_seq_ageint_lab_w			number(10);
ie_gerar_ageint_exames_lab_w		varchar2(1);
ie_gerou_prescr_w 			varchar2(1) := 'N';
ie_gerar_senha_aut_w			varchar2(1) := 'S';
cd_estabelecimento_w			number(10);
nm_usuario_w				varchar2(255) := obter_usuario_ativo;
cd_setor_logado_w			number(10)   := obter_setor_ativo;
ie_setor_47_w				varchar2(15);
cd_plano_w				varchar2(10);
nr_seq_proc_interno_w			proc_interno.nr_sequencia%type;
vl_coparticipacao_w			agenda_paciente_auxiliar.vl_coparticipacao%type;
vl_coparticipacao_adic_w		Agenda_paciente_proc.vl_coparticipacao%type;
nr_Seq_inf_add_w			prescr_proced_inf_adic.nr_sequencia%type;
cd_medico_exec_w			prescr_procedimento.cd_medico_exec%type;
nr_crm_w            procedimento_guia_wint.nr_crm%type;
nr_ano_guia_w            procedimento_guia_wint.nr_ano_guia%type;

Cursor C01 is	
	select	cd_material		,
		cd_procedimento         ,
		cd_procedimento_tuss    ,
		cd_variacao             ,
		ds_ind_clinica          ,
		ds_observacao           ,
		ds_proc_exame           ,
		ds_status               ,
		ie_gerar                ,
		ie_lado                 ,
		nr_atendimento          ,
		nr_sequencia            ,
		nr_seq_ageint           ,
		nr_seq_agepac           ,
		nr_seq_doc_convenio     ,
		nr_seq_exame            ,
		nr_seq_proc_interno     ,
		nr_seq_topografia       ,
		qt_procedimento         ,
		vl_coparticipacao       ,
		vl_procedimento			,
		nr_seq_contraste		,
		nr_prescricao			,
		nr_seq_prescr_proc		,
		nr_crm					,
		nr_ano_guia
	from	procedimento_guia_wint
	where	nr_atendimento = nr_atendimento_p
	and	ds_erro is null
	and	upper(nvl(ds_status, 'AUTORIZADO')) <> 'ERRO'
	and	ie_gerar = 'S';	
	
cursor	c02 is
	select	cd_setor_atendimento
	from	procedimento_setor_atend
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w
	and	cd_estabelecimento	= cd_estabelecimento_w
	order by ie_prioridade desc;
	
Cursor C03 is
	select	cd_setor_atendimento
	from	proc_interno_setor
	where	nr_seq_proc_interno = nr_seq_proc_interno_w
	and	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
	and	nvl(cd_perfil,obter_perfil_ativo) = obter_perfil_ativo
	and	((cd_especialidade is null) or
		 (obter_se_especialidade_medico(cd_medico_prescr_w,cd_especialidade) = 'S'))
	order by nr_prioridade desc;	

	
c01_w	c01%ROWTYPE;


begin
select	cd_estabelecimento,
	ie_tipo_atendimento,
	cd_pessoa_fisica,
	dt_entrada,
	cd_pessoa_fisica,
	cd_medico_resp
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w,
	cd_pessoa_fisica_w,
	dt_entrada_w,
	cd_paciente_w,
	cd_medico_prescr_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

Obter_Param_Usuario(820,136,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_medico_requisitante_w);
Obter_Param_Usuario(916,45,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_data_w);
Obter_Param_Usuario(916, 47,  obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_setor_47_w);
Obter_Param_Usuario(916,203,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_medico_prescr_w);
Obter_param_usuario(916,270,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_atualiza_setor_entrega);
obter_param_usuario(916,452,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_atualizar_setor_agenda_w);
Obter_Param_Usuario(916,457,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, cd_setor_entrega_w);
Obter_Param_Usuario(916,518,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_agendamento_pac_prescr_w);	
Obter_param_usuario(916,646,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, cd_setor_entrega_def_270_w);
Obter_param_usuario(916,757,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_restringe_estab_w);
Obter_param_usuario(916,1164,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_tipo_data_w);
Obter_param_usuario(916,1185,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_gerar_ageint_exames_lab_w);
obter_param_usuario(916, 931, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_atualizar_result_w);
obter_param_usuario(916, 932, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_atualizar_entrega_w);
Obter_param_usuario(924,246,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_adep_w);
Obter_param_usuario(820,342,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_chama_fanep_w);
Obter_param_usuario(820,330,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_prescr_dt_agenda_w);
Obter_param_usuario(820,315,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_gera_opme_w);
Obter_Param_Usuario(916, 200,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_somente_exclusivo_w);
Obter_Param_Usuario(916,163,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,qt_ambos_w);
Obter_Param_Usuario(916,1231,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,considera_regra_quantidade_w);																			
Obter_Param_Usuario(916, 301, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_gerar_proc_adic_w);

Obter_Param_Usuario(916, 537, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, cd_intervalo_w);
Obter_Param_Usuario(916, 619, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_setor_exec_proc_int_w);
Obter_Param_Usuario(916, 626, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_dt_maior_w);
Obter_Param_Usuario(916, 838, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_considera_regra_med_exame_w);
Obter_param_usuario(820, 332, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_autorizados_w);
Obter_param_usuario(916, 1063, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_atualizar_obs_prescr_w);
obter_param_usuario(871, 773, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_gerar_proc_adic_previstos_w);
obter_param_usuario(924, 58, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_gerar_kit_proc_w);
obter_param_usuario(871, 825, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ds_classe_medic_excl_cir_w);
Obter_param_usuario(916,1188,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w, ie_gerar_med_adicional_w);
Obter_param_usuario(820, 459, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_param_459_w);
Obter_param_usuario(871, 843, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_param_843_w);

select	max(cd_convenio),
	max(cd_categoria),
	max(cd_plano_convenio)
into 	cd_convenio_w,
	cd_categoria_w,	
	cd_plano_w
from 	Atend_categoria_convenio a
where 	a.nr_atendimento	= nr_atendimento_p
and 	a.dt_inicio_vigencia	= (	select 	max(dt_inicio_vigencia)
					from 	atend_categoria_convenio b
					where 	nr_atendimento	= nr_atendimento_p);

dt_prescricao_w	:= sysdate;

if	(ie_data_w = 'E') then
	select	dt_entrada
	into	dt_prescricao_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;
end if;

if (ie_atualiza_setor_entrega = 'P') then
	select 	nvl(max(cd_setor_atendimento),cd_setor_logado_w)
	into	cd_setor_entrega_270_w
	from 	atend_paciente_unidade
	where 	nr_seq_interno = obter_atepacu_paciente(nr_atendimento_p, 'A');
elsif (ie_atualiza_setor_entrega = 'U') then
	cd_setor_entrega_270_w	:= cd_setor_entrega_w;
elsif (ie_atualiza_setor_entrega = 'A') then
	cd_setor_entrega_270_w	:= cd_setor_entrega_def_270_w;
end if;

if	(cd_setor_entrega_270_w	= 0) then
	cd_setor_entrega_270_w	:= null;
end if;

select	nvl(max('1'),'3')
into	ie_origem_inf_w
from	Medico b,
	Usuario a
where 	a.nm_usuario		= nm_usuario_w
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

begin
	select	max(vl_default)
	into	nr_seq_forma_laudo_w
	from	tabela_atrib_regra
	where	nm_tabela					= 'PRESCR_MEDICA'
	and	nm_atributo					= 'NR_SEQ_FORMA_LAUDO'
	and	nvl(cd_estabelecimento,cd_estabelecimento_w)    = cd_estabelecimento_w
	and	nvl(cd_perfil, obter_perfil_ativo)		= obter_perfil_ativo
	and	nvl(nm_usuario_param, nm_usuario_w) 		= nm_usuario_w;
exception
when others then
	nr_seq_forma_laudo_w := null;
end;

select	Prescr_medica_seq.nextval
into	nr_prescricao_w
from	dual;

open C01;
loop
fetch C01 into	
	C01_w;
exit when C01%notfound;
	begin
	
	if(C01_w.nr_prescricao is not null and C01_w.nr_seq_prescr_proc is not null) then
	
		update	prescr_procedimento
		set		nr_seq_contraste = nvl(C01_w.nr_seq_contraste, nr_seq_contraste),
				nr_doc_convenio = nvl(C01_w.nr_seq_doc_convenio, nr_doc_convenio),
				nr_seq_topografia = nvl(C01_w.nr_seq_topografia, nr_seq_topografia)
		where	nr_sequencia = C01_w.nr_seq_prescr_proc
		and nr_prescricao = C01_w.nr_prescricao;
		
		select 	max(nr_sequencia)
		into	nr_Seq_inf_add_w
		from 	prescr_proced_inf_adic
		where 	nr_prescricao		= C01_w.nr_prescricao
		and	nr_seq_prescricao	= C01_w.nr_seq_prescr_proc;

		if (nvl(nr_Seq_inf_add_w,0) = 0) then
		
			select  max(nvl(cd_medico_exec,cd_medico_solicitante))
			into 	cd_medico_exec_w
			from 	prescr_procedimento 
			where	nr_sequencia = C01_w.nr_seq_prescr_proc
			and 	nr_prescricao = C01_w.nr_prescricao;	
		
			insert into prescr_proced_inf_adic(
						nr_sequencia, 
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_registro,
						nr_prescricao,
						nr_seq_prescricao,
						cd_pessoa_fisica, 
						ie_situacao,
						vl_coparticipacao)
					values( prescr_proced_inf_adic_seq.nextval,
						sysdate,
						nm_usuario_w,
						sysdate,
						nm_usuario_w,
						sysdate,
						C01_w.nr_prescricao,
						C01_w.nr_seq_prescr_proc,
						nvl(cd_medico_exec_w, cd_medico_prescr_w),
						'A',
						c01_w.vl_coparticipacao);
				
		else 
		
			update 	prescr_proced_inf_adic
			set	vl_coparticipacao = c01_w.vl_coparticipacao	
			where 	nr_sequencia = nr_Seq_inf_add_w;
			
		end if;					
		
	else 
	
		nr_seq_proc_interno_w := C01_w.nr_seq_proc_interno;

		 obter_proc_tab_interno_conv(
				 nr_seq_proc_interno_w,
				 cd_estabelecimento_w,
				 cd_convenio_w,
				 cd_categoria_w,
				 cd_plano_w,
				 null,
				 cd_procedimento_w,
				 ie_origem_proced_w,
				 null,
				 sysdate,
				 null,
				 null,
				 null,
				 null,
				 null,
				 null,
				 null,
				 null);
		
		IF	(REMOVER_ACENTUACAO(ie_setor_47_w) = 'Usuario') THEN
			cd_setor_atendimento_w	:= NVL(cd_setor_logado_w,cd_setor_atendimento_w);
		ELSIF	(ie_setor_47_w = 'Paciente') THEN
			SELECT NVL(MAX(cd_setor_atendimento),NVL(cd_setor_logado_w,cd_setor_atendimento_w))
			INTO cd_setor_atendimento_w
			FROM atend_paciente_unidade
			WHERE nr_seq_interno	= obter_atepacu_paciente(nr_atendimento_p, 'A');
		ELSIF	(ie_setor_47_w = 'Procedimento') THEN
			BEGIN
			open	c02;
			loop
			fetch	c02 into
				cd_setor_proc_w;
			exit	when c02%notfound;
				begin
				
				cd_setor_atendimento_w	:= cd_setor_proc_w;
				end;
			end loop;
			close c02;
			if	(nvl(cd_setor_proc_w,0) = 0) then
				begin
				cd_setor_proc_interno_w	:= null;
				open C03;
				loop
				fetch C03 into	
					cd_setor_proc_interno_w;
				exit when C03%notfound;
					begin
					cd_setor_proc_interno_w	:=	cd_setor_proc_interno_w;
					end;
				end loop;
				close C03;
				cd_setor_atendimento_w	:= cd_setor_proc_interno_w;
				end;				
			end if;
			END;
		ELSIF	(ie_setor_47_w = 'Proc Interno') THEN
			BEGIN

			SELECT	NVL(MAX(Obter_Setor_exec_proc_interno(c01_w.nr_seq_proc_interno,NULL,ie_tipo_atendimento_w,cd_convenio_w,cd_categoria_w)),cd_setor_atendimento_w)
			INTO	cd_setor_atendimento_w
			FROM	dual;

			END;
		END IF;		

		select	count(*)
		into	qt_prescr_gerada_w
		from	prescr_medica 
		where	nr_atendimento = nr_atendimento_p
		and	cd_setor_atendimento = cd_setor_atendimento_w
		and	dt_liberacao is null;	
		
		if	(qt_prescr_gerada_w = 0) then
		
			select	Prescr_medica_seq.nextval
			into	nr_prescricao_w
			from	dual;
			
			insert into prescr_medica (
				nr_prescricao,
				cd_pessoa_fisica,
				nr_atendimento,
				cd_medico,
				dt_prescricao,
				dt_atualizacao,
				nm_usuario,
				nm_usuario_original,
				nr_horas_validade,
				dt_primeiro_horario,
				dt_liberacao,
				cd_setor_atendimento,
				cd_setor_entrega,
				dt_entrega,
				ie_recem_nato,
				ie_origem_inf,
				cd_estabelecimento,
				nr_seq_agenda,
				cd_prescritor,
				ie_modo_integracao,
				nr_seq_forma_laudo)
			select 	nr_prescricao_w,
				a.cd_pessoa_fisica,
				nr_atendimento_p,
				a.cd_medico_resp,
				dt_prescricao_w,
				sysdate,
				nm_usuario_w,
				nm_usuario_w,
				24,
				b.hr_inicio_prescricao,
				null,
				cd_setor_atendimento_w,
				nvl(cd_setor_entrega_270_w,cd_setor_atendimento_w),
				dt_prescricao_w,
				'N',
				ie_origem_inf_w,
				cd_estabelecimento_w,
				null,
				obter_dados_usuario_opcao(nm_usuario_w, 'C'),
				'A',
				nr_seq_forma_laudo_w
			from	setor_atendimento b,
				atendimento_paciente a
			where	a.nr_atendimento	= nr_atendimento_p
			and	b.cd_setor_atendimento	= cd_setor_atendimento_w;
			
		else
			
			select	nvl(max(nr_prescricao),0)
			into	nr_prescricao_w
			from	prescr_medica 
			where	nr_atendimento = nr_atendimento_p		
			and	cd_setor_atendimento = cd_setor_atendimento_w
			and	dt_liberacao is null;	

		end if;
		
			if	(nr_prescricao_w > 0) then
					
				if (C01_w.nr_seq_exame is not null) then
					if (C01_w.cd_material is null) then
						select nvl(max(cd_material_exame),'')
						into	cd_material_exame_w
						from (	select 	b.cd_material_exame
							from	material_exame_lab b,
								exame_lab_material a
							where 	a.nr_seq_material	= b.nr_sequencia
							and 	a.nr_seq_exame	= c01_w.nr_seq_exame
							and 	a.ie_situacao	= 'A'
							and  	lab_obter_se_mat_lib_conv(a.nr_seq_exame,b.nr_sequencia,cd_estabelecimento_w) = 'S'
							order by 	a.ie_prioridade)
						where rownum = 1;
					end if;
					
					select	max(cd_setor_exclusivo)
					into	cd_setor_atendimento_w
					from	procedimento
					where	cd_procedimento	= cd_procedimento_w
					and	ie_origem_proced	= ie_origem_proced_w;
		
					If	(ie_atualizar_entrega_w = 'S') then
						cd_setor_atendimento_w := cd_setor_atendimento_w;
					end if;

					if	(cd_setor_atendimento_w is null) then
						cd_setor_atendimento_w := obter_setor_atend_proc(	cd_estabelecimento_w, 
													cd_procedimento_w, 
													ie_origem_proced_w, 
													null, 
													null,
													null,
													c01_w.nr_seq_proc_interno,
													nr_atendimento_p);
					end if;

									if	(cd_setor_atendimento_w is null) and
										(c01_w.nr_seq_exame is not null) then
									
										cd_setor_atendimento_w := obter_setor_atend_proc_lab(	cd_estabelecimento_w,
																	cd_procedimento_w,
																	ie_origem_proced_w, 
																	null,
																	null,
																	null,
																	c01_w.nr_seq_exame);
									end if;
										
									consiste_medico_executor(cd_estabelecimento_w,
												 cd_convenio_w,
												 cd_setor_atendimento_w,
												 cd_procedimento_w,
												 ie_origem_proced_w,
												 ie_tipo_atendimento_w,
												 c01_w.nr_seq_exame,
												 c01_w.nr_seq_proc_interno,
												 ie_medico_executor_w,
												 cd_cgc_w,
												 cd_medico_executor_w,
												 cd_pessoa_fisica_w,
												 '',
												 nvl(dt_prevista_w,sysdate),
												 Obter_Classificacao_Atend(nr_atendimento_p,'C'),
												 'N',
												 null,
												 cd_setor_prescricao_w);
										
									select	max(vl_default)
									into	ie_amostra_w
									from	tabela_atrib_regra
									where	nm_tabela = 'PRESCR_PROCEDIMENTO'
									and	nm_atributo = 'IE_AMOSTRA'
									and	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
									and	nvl(cd_perfil, obter_perfil_ativo) = obter_perfil_ativo
									and	nvl(nm_usuario_param, nm_usuario_w) = nm_usuario_w;
									
									select 	nvl(max(nr_sequencia),0) + 1
									into 	nr_sequencia_w
									from 	prescr_procedimento
									where 	nr_prescricao	= nr_prescricao_w;
										
									select	prescr_procedimento_seq.NextVal
									into	nr_seq_interno_w
									from	dual;	
									
									select	obter_se_proc_urgente(nvl(cd_setor_atend_prescr_medica_w,0), nvl(cd_procedimento_w,0), nvl(ie_origem_proced_w,0), nvl(c01_w.nr_seq_proc_interno,0), nvl(c01_w.nr_seq_exame,0), nvl(ie_tipo_atendimento_w,0), 'N', obter_perfil_ativo, nvl(cd_medico_w,0))
									into	ie_urgente_w
									from	dual;
	  
										
									consiste_plano_convenio(nr_atendimento_p,
												cd_convenio_w,
												cd_procedimento_w,
												ie_origem_proced_w,
												sysdate,
												1,
												ie_tipo_atendimento_w,
												cd_plano_w,
												'',
												ds_erro_conv_w,
												cd_setor_atendimento_w,
												c01_w.nr_seq_exame,
												ie_regra_w,
												null,
												nr_seq_regra_w,
												c01_w.nr_seq_proc_interno,
												cd_categoria_w,
												cd_estabelecimento_w,
												cd_setor_atendimento_w,
												cd_medico_executor_w,
												cd_paciente_w,
												ie_glosa_w,
												nr_seq_regra_preco_w);
												
									if	(ie_regra_w in (1,2)) then
										ie_autorizacao_w := 'B';
									elsif	(ie_regra_w in (3,6,7)) then
										ie_autorizacao_w := 'PA';
									end if;
										
									insert into Prescr_Procedimento (
											nr_prescricao, 
											nr_sequencia, 
											cd_procedimento,
											qt_procedimento, 
											dt_atualizacao, 
											nm_usuario,
											cd_motivo_baixa, 
											ie_origem_proced,
											ie_urgencia, 
											cd_setor_atendimento,
											dt_prev_execucao, 
											ie_suspenso, 
											ie_status_atend, 
											ie_origem_inf, 
											cd_medico_exec, 
											nr_seq_interno,
											ie_avisar_result, 
											nr_seq_proc_interno, 
											nr_seq_exame, 
											cd_material_exame, 
											cd_setor_coleta,
											cd_setor_entrega, 
											dt_resultado, 
											ie_autorizacao,
							ds_observacao,
							ds_dado_clinico,
							nr_seq_contraste,
							nr_doc_convenio)
										values(
											nr_prescricao_w, 
											nr_sequencia_w, 
											cd_procedimento_w,
											c01_w.qt_procedimento, 
											sysdate, 
											nm_usuario_w,
											0, 
											ie_origem_proced_w,
											ie_urgente_w, 
											cd_setor_atendimento_w,
											dt_prevista_w, 
											'N', 
											5, 
											'1', 
											cd_medico_executor_w, 
											nr_seq_interno_w,
											'N', 
											c01_w.nr_seq_proc_interno, 
											c01_w.nr_seq_exame, 
											cd_material_exame_w, 
											cd_setor_atendimento_w,
											cd_setor_atendimento_w, 
											dt_resultado_w, 
											ie_autorizacao_w,
							c01_w.ds_observacao,
							c01_w.ds_ind_clinica,
							c01_w.nr_seq_contraste,
							c01_w.nr_seq_doc_convenio);
									
					select 	max(nr_sequencia)
					into	nr_Seq_inf_add_w
					from 	prescr_proced_inf_adic
					where 	nr_prescricao		= nr_prescricao_w
					and	nr_seq_prescricao	= nr_sequencia_w;
			
					if (nvl(nr_Seq_inf_add_w,0) = 0) then
						insert into prescr_proced_inf_adic(
									nr_sequencia, 
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									dt_registro,
									nr_prescricao,
									nr_seq_prescricao,
									cd_pessoa_fisica, 
									ie_situacao,
									vl_coparticipacao,
									cd_crm_medico,
									nr_ano_guia)
								values( prescr_proced_inf_adic_seq.nextval,
									sysdate,
									nm_usuario_w,
									sysdate,
									nm_usuario_w,
									sysdate,
									nr_prescricao_w,
									nr_sequencia_w,
									nvl(cd_medico_executor_w,cd_medico_prescr_w),
									'A',
									c01_w.vl_coparticipacao,
									c01_w.nr_crm					,
									c01_w.nr_ano_guia);
							
					else 
						update 	prescr_proced_inf_adic
						set	vl_coparticipacao = c01_w.vl_coparticipacao	
						where 	nr_sequencia = nr_Seq_inf_add_w;
					end if;						
					
									if	(ie_atualizar_result_w= 'S') then
										begin
											dt_resultado_w := Obter_data_resultado_exame(nr_sequencia_w, nr_prescricao_w);
										exception
											when 	no_data_found then
												dt_resultado_w := null;
										end;
										
										if	(dt_resultado_w is not null) then
											update	prescr_procedimento
											set	dt_resultado = dt_resultado_w
											where	nr_prescricao = nr_prescricao_w
											and	nr_sequencia = nr_sequencia_w;
										end if;		
									End if;
										
									cd_setor_coleta_w := null;
									cd_setor_entrega_ww := null;
									cd_setor_atend_w := null;
			   
										cd_setor_atend_usuario_w := cd_setor_atendimento_w;
									
										obter_setor_exame_lab(	nr_prescricao_w, 
													c01_w.nr_seq_exame, 
													cd_setor_atend_usuario_w, 
													cd_material_exame_w,
													null,
													'S', 
													cd_setor_atend_w, 
													cd_setor_coleta_w, 
													cd_setor_entrega_ww, 
													qt_dia_entrega_w, 
													ie_emite_mapa_w, 
													ds_hora_fixa_w, 
													ie_data_resultado_w, 
													qt_min_entrega_w, 
													ie_atualizar_recoleta_w, 
													'N',
													ie_dia_semana_final_w,
													ie_forma_atual_dt_result_w,
													qt_min_atraso_w);
										
										if	(cd_setor_coleta_w is not null) or
											(cd_setor_entrega_ww is not null) or 
											(cd_setor_atend_w is not null) then
											update	prescr_procedimento
											set	cd_setor_coleta =  nvl(to_number(replace(replace(cd_setor_coleta_w,'(',''), ')','')), cd_setor_coleta),
												cd_setor_entrega = nvl(nvl(to_number(replace(replace(cd_setor_entrega_ww,'(',''), ')','')), cd_setor_entrega_w), cd_setor_entrega)
											where	nr_prescricao = nr_prescricao_w
											and	nr_sequencia = nr_sequencia_w;
										end if;
								   
									Consistir_Prescr_Procedimento (nr_prescricao_w,nr_sequencia_w,nm_usuario_w,obter_perfil_ativo,ds_erro_w);	
				else
				 
									consiste_medico_executor(cd_estabelecimento_w,
												 cd_convenio_w,
												 cd_setor_atendimento_w,
												 cd_procedimento_w,
												 ie_origem_proced_w,
												 ie_tipo_atendimento_w,
												 c01_w.nr_seq_exame,
												 c01_w.nr_seq_proc_interno,
												 ie_medico_executor_w,
												 cd_cgc_w,
												 cd_medico_executor_w,
												 cd_pessoa_fisica_w,
												 '',
												 nvl(dt_prevista_w,sysdate),
												 Obter_Classificacao_Atend(nr_atendimento_p,'C'),
												 'N',
												 null,
												 cd_setor_prescricao_w);

					
					select nvl(max(nr_sequencia),0) + 1
					into nr_sequencia_w
					from prescr_procedimento
					where nr_prescricao	= nr_prescricao_w;
		
					obter_entrega_laudo
						(nr_prescricao_w,
						nvl(hr_inicio_w,sysdate),
						nr_seq_proc_interno_w,
						cd_procedimento_w,
						ie_origem_proced_w,
						cd_setor_atendimento_w,
						nm_usuario_w,
						dt_resultado_w);

					select	prescr_procedimento_seq.NextVal
					into	nr_seq_interno_w
					from	dual;
					
					qt_proc_regra_w:=	nvl(Obter_Quantidade_prescr_proced(cd_intervalo_w,
										c01_w.ie_lado,
										cd_procedimento_w,
										ie_origem_proced_w,
										nr_seq_proc_interno_w,
										nr_atendimento_p),1);
							   
		
						insert into Prescr_Procedimento (
										nr_prescricao, 
										nr_sequencia, 
										cd_procedimento,
										qt_procedimento, 
										dt_atualizacao, 
										nm_usuario,
										cd_motivo_baixa, 
										ie_origem_proced,
										ie_urgencia, 
										cd_setor_atendimento,
										dt_prev_execucao, 
										ie_suspenso, 
										ie_status_atend, 
										ie_origem_inf, 
										dt_resultado, 			
										cd_medico_exec, 
										ie_autorizacao, 
										nr_seq_interno, 			
										ie_lado,
										ie_avisar_result, 
										nr_seq_proc_interno,									
										nr_seq_topografia,
										ds_dado_clinico,
										ds_observacao,
										nr_doc_convenio,
										nr_seq_contraste)
									values(
										nr_prescricao_w, 
										nr_sequencia_w, 
										cd_procedimento_w,
										decode(c01_w.ie_lado,'A',qt_param_1231,qt_proc_regra_w), 
										sysdate, 
										nm_usuario_w,
										0, 
										ie_origem_proced_w,
										'N', 
										cd_setor_atendimento_w,
										nvl(hr_inicio_w,sysdate), 
										'N', 
										5, 
										'1', 
										dt_resultado_w, 
										cd_medico_agenda_w, 
										ie_autorizacao_w, 
										nr_seq_interno_w, 
										c01_w.ie_lado,
										'N', 
										c01_w.nr_seq_proc_interno,
										c01_w.nr_seq_topografia,
										c01_w.ds_ind_clinica,
										c01_w.ds_observacao,
										c01_w.nr_seq_doc_convenio,
										c01_w.nr_seq_contraste);
						select 	max(nr_sequencia)
						into	nr_Seq_inf_add_w
						from 	prescr_proced_inf_adic
						where 	nr_prescricao		= nr_prescricao_w
						and	nr_seq_prescricao	= nr_sequencia_w;
			
						if (nvl(nr_Seq_inf_add_w,0) = 0) then
							insert into prescr_proced_inf_adic(
										nr_sequencia, 
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										dt_registro,
										nr_prescricao,
										nr_seq_prescricao,
										cd_pessoa_fisica, 
										ie_situacao,
										vl_coparticipacao,
										cd_crm_medico,
										nr_ano_guia)
									values( prescr_proced_inf_adic_seq.nextval,
										sysdate,
										nm_usuario_w,
										sysdate,
										nm_usuario_w,
										sysdate,
										nr_prescricao_w,
										nr_sequencia_w,
										nvl(cd_medico_executor_w,cd_medico_prescr_w),
										'A',
										c01_w.vl_coparticipacao,
										c01_w.nr_crm					,
										c01_w.nr_ano_guia);
								
						else 
							update 	prescr_proced_inf_adic
							set	vl_coparticipacao = c01_w.vl_coparticipacao	
							where 	nr_sequencia = nr_Seq_inf_add_w;
					end if;
				end if;
			end if;
	end if;
	
	update_status_proc_wint(to_char(c01_w.nr_sequencia), 'G');
	consistir_prescricao(nr_prescricao_w, obter_perfil_Ativo, nm_usuario_w, ds_aux_w, ds_aux_w, ds_aux_w, ds_aux_w);
	
	end;
end loop;
close C01;



select  max(cd_senha)
into    cd_senha_w
from    prescr_procedimento
where   nr_prescricao   = nr_prescricao_w;


if (cd_senha_w is not null) and 
   (ie_gerar_senha_aut_w = 'S') then
	update  atend_categoria_convenio
	set     cd_senha        = cd_senha_w
	where   nr_atendimento  = nr_atendimento_p;
end if;

if	(cd_setor_atendimento_w <> 0) then
	begin
	select	count(*)
	into	qt_passagem_setor_w
	from	atend_paciente_unidade
	where	nr_atendimento	= nr_atendimento_p;	

	if	(qt_passagem_setor_w = 0) then
		begin

		select 	obter_tipo_passagem_regra(cd_setor_atendimento_w)
		into	ie_tipo_passagem_regra_w
		from 	dual;


		if	(ie_tipo_passagem_regra_w = 'T') then

			gerar_entrada_setor_prescr(nr_atendimento_p, cd_setor_atendimento_w, dt_prescricao_w,nm_usuario_w);

		else

			gerar_passagem_setor_atend(nr_atendimento_p, cd_setor_atendimento_w, dt_prescricao_w, 'S', nm_usuario_w);

			select	nvl(max(nr_seq_interno),0)
			into	nr_seq_interno_w
			from 	atend_paciente_unidade
			where 	nr_atendimento	= nr_atendimento_p; 

			ATEND_PACIENTE_UNID_AFTERPOST(nr_seq_interno_w,'I',nm_usuario_w);

		end if;

		end;
	end if;
	end;
end if;


commit;

end wint_gerar_prescr_eup;
/
