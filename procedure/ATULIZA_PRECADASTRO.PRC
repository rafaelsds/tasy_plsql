create or replace procedure atuliza_precadastro(nr_seq_processo_p in number,
                                                ie_tipo_processo_p in varchar2,
                                                ie_tipo_pre_cadastro_p in varchar2,
												cd_estabelecimento_p in number,
                                                nm_usuario_p in varchar2) is

dt_liberacao_w date := null;
dt_aprovacao_w date := null;
nm_usuario_w varchar(255) := null;														 
nm_tabela_pre_cadastro_w varchar2(255) := null;													
nr_seq_pre_cad_w number(10) := null;
ie_tipo_processo_w varchar(255);
													
begin							 

	if (ie_tipo_pre_cadastro_p = 'FPF') then
		nm_tabela_pre_cadastro_w := 'PESSOA_FISICA_PRECAD';
	elsif (ie_tipo_pre_cadastro_p = 'FPJ') then
		nm_tabela_pre_cadastro_w := 'PESSOA_JURIDICA_PRECAD';
	elsif (ie_tipo_pre_cadastro_p = 'MAT') then
		nm_tabela_pre_cadastro_w := 'MATERIAL_PRECAD';
	end if;

	if (ie_tipo_processo_p = 'LIBERAR') then 
		dt_liberacao_w := sysdate;
		nm_usuario_w := nm_usuario_p;
		ie_tipo_processo_w := wheb_mensagem_pck.get_texto(1150958);
	elsif (ie_tipo_processo_p = 'APROVAR') then
		dt_aprovacao_w := sysdate;
		nm_usuario_w := nm_usuario_p;
		ie_tipo_processo_w := wheb_mensagem_pck.get_texto(1150959);
	elsif (ie_tipo_processo_p = 'ESTORNAR') then
		dt_liberacao_w := null;
		nm_usuario_w := null;
		ie_tipo_processo_w := wheb_mensagem_pck.get_texto(1150960);
	elsif (ie_tipo_processo_p = 'ESTORNAR_APROVACAO') then
		dt_aprovacao_w := null;
		nm_usuario_w := null;
		ie_tipo_processo_w := wheb_mensagem_pck.get_texto(1150961);
	end if;
	
	if (nm_tabela_pre_cadastro_w is not null) then

		execute immediate 'select a.nr_sequencia from ' || nm_tabela_pre_cadastro_w || ' a where nr_seq_processo = :1' into nr_seq_pre_cad_w using nr_seq_processo_p;

		if (ie_tipo_processo_p = 'LIBERAR') then
			execute immediate 'update ' || nm_tabela_pre_cadastro_w || ' set nm_usuario_lib = :1 , dt_liberacao = :2 where nr_sequencia = :3' using nm_usuario_w, dt_liberacao_w, nr_seq_pre_cad_w;
		elsif (ie_tipo_processo_p = 'APROVAR') then
			execute immediate 'update ' || nm_tabela_pre_cadastro_w || ' set nm_usuario_aprov = :1 , dt_aprovacao = :2 where nr_sequencia = :3' using nm_usuario_w, dt_aprovacao_w, nr_seq_pre_cad_w;
			criar_avaliacao_pre_cadastro(nr_seq_pre_cad_w,ie_tipo_pre_cadastro_p, 'S', cd_estabelecimento_p, nm_usuario_p);
		elsif (ie_tipo_processo_p = 'ESTORNAR') then
			execute immediate 'update ' || nm_tabela_pre_cadastro_w || ' set nm_usuario_lib = :1 , dt_liberacao = :2 where nr_sequencia = :3' using nm_usuario_w, dt_liberacao_w, nr_seq_pre_cad_w;
			execute immediate 'update ' || 'PROCESSO_PRE_CADASTRO' || ' set dt_liberacao = null, nm_usuario_lib = null where nr_sequencia = :1' using nr_seq_processo_p;
		elsif (ie_tipo_processo_p = 'ESTORNAR_APROVACAO') then
			execute immediate 'update ' || nm_tabela_pre_cadastro_w || ' set nm_usuario_aprov = :1 , dt_aprovacao = :2 where nr_sequencia = :3' using nm_usuario_w, dt_aprovacao_w, nr_seq_pre_cad_w;
			execute immediate 'update ' || 'PROCESSO_PRE_CADASTRO' || ' set dt_aprovacao = null, nm_usuario_aprov = null where nr_sequencia = :1' using nr_seq_processo_p;
		end if;
		
		insert into processo_precad_hist(nr_sequencia,
								 dt_atualizacao,
								 nm_usuario,
								 dt_atualizacao_nrec,
								 nm_usuario_nrec,
								 dt_liberacao,
								 nm_usuario_lib,
								 nr_seq_processo,
								 ds_historico
								 ) values(
								 processo_precad_hist_seq.nextval,
								 sysdate,
								 nm_usuario_p,
								 sysdate,
								 nm_usuario_p,
								 sysdate,
								 nm_usuario_p,
								 nr_seq_processo_p,
								 ie_tipo_processo_w);					 
		
		commit;

	end if;

end atuliza_precadastro;
/
