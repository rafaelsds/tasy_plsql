create or replace
procedure Obter_horarios_eliminados	(cd_estabelecimento_p		number,
					nm_usuario_p		varchar2,
					nr_atendimento_p		number,
					ie_tipo_item_p		varchar2,
					cd_item_p		varchar2,
					cd_intervalo_p		varchar2,
					ie_laboratorio_p		varchar2,
					nr_prescricao_p		number,
					nr_seq_item_p		number,
					qt_minutos_p		number,
					ds_lista_eliminados_p out	varchar2,
					ie_reaprazar_seguintes_p	varchar2,
					nr_seq_horario_p		number) is

ds_lista_eliminados_w	varchar2(255);
dt_horario_w		varchar2(20);
dt_validade_prescr_w	date;
dt_inicio_prescr_w	date;
ie_data_proc_w		varchar2(15);
ie_data_lib_prescr_w	varchar2(15);
ie_exibe_suspenso_w	varchar2(15);
cd_setor_pac_w		number(15);
cd_funcao_origem_w 	prescr_medica.cd_funcao_origem%type;
dt_validade_cpoe_w	date;
nr_seq_mat_cpoe_w	prescr_material.nr_seq_mat_cpoe%type;
dt_hor_selecionado_w date;

cursor c01 is
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* dietas orais */
from	prescr_dieta_hor c,
	prescr_medica a
where	c.nr_prescricao = a.nr_prescricao
--and	a.dt_liberacao is not null
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	nvl(c.ie_situacao,'A') = 'A'
--and	nvl(c.ie_horario_especial,'N') = 'N'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	c.cd_refeicao = cd_item_p
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'D'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi')
from	prescr_medica a,
	prescr_dieta b,
	prescr_dieta_hor c
where	c.nr_prescricao = a.nr_prescricao
and	c.nr_prescricao = b.nr_prescricao
and	c.nr_seq_dieta	= b.nr_sequencia
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	nvl(c.ie_situacao,'A') = 'A'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	to_char(b.cd_dieta) = cd_item_p
and	c.cd_refeicao is null
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'D'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* suplementos orais */
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_prescricao = b.nr_prescricao
and	c.nr_seq_material = b.nr_sequencia
and	b.nr_prescricao = a.nr_prescricao
--and	a.dt_liberacao is not null
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	b.ie_agrupador in (12)
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	b.nr_sequencia = nr_seq_item_p
and	b.cd_material = cd_item_p
and	nvl(b.cd_intervalo,'XPTO') = nvl(cd_intervalo_p,'XPTO')
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* medicamentos */
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_prescricao = b.nr_prescricao
and	c.nr_seq_material = b.nr_sequencia
and	b.nr_prescricao = a.nr_prescricao
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	b.ie_agrupador in (1)
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(c.ie_adep,'S') = 'S'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	b.nr_sequencia = nr_seq_item_p
and	b.cd_material = cd_item_p
and	nvl(b.cd_intervalo,'XPTO') = nvl(cd_intervalo_p,'XPTO')
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'M'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* materiais */
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_prescricao = b.nr_prescricao
and	c.nr_seq_material = b.nr_sequencia
and	b.nr_prescricao = a.nr_prescricao
--and	a.dt_liberacao is not null
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	b.ie_agrupador in (2)
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	b.nr_sequencia = nr_seq_item_p
and	b.cd_material = cd_item_p
and	nvl(b.cd_intervalo,'XPTO') = nvl(cd_intervalo_p,'XPTO')
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'MAT'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* procedimentos e controles de glicemia */
from	prescr_proc_hor c,
	prescr_procedimento b,
	prescr_medica a
where	c.nr_prescricao = b.nr_prescricao
and	c.nr_seq_procedimento = b.nr_sequencia
and	b.nr_prescricao = a.nr_prescricao
--and	a.dt_liberacao is not null
and	obter_data_lib_proc_adep(a.dt_liberacao, a.dt_liberacao_medico, ie_data_proc_w) is not null
and	obter_se_exibir_adep_suspensos(b.dt_suspensao, ie_exibe_suspenso_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	((b.nr_seq_exame is null and ie_laboratorio_p = 'N') or
	(b.nr_seq_exame is not null and ie_laboratorio_p = 'S'))
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	b.nr_sequencia = nr_seq_item_p
and	b.cd_procedimento = cd_item_p
and	nvl(b.cd_intervalo,'XPTO') = nvl(cd_intervalo_p,'XPTO')
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	(((nvl(ie_reaprazar_seguintes_p,'N') = 'N') and (c.nr_sequencia = nr_seq_horario_p)) or
	(nvl(ie_reaprazar_seguintes_p,'N') = 'S'))
and	ie_tipo_item_p in ('P','G','C')
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* recomenda��es */
from	prescr_rec_hor c,
	prescr_recomendacao b,
	prescr_medica a
where	c.nr_prescricao = b.nr_prescricao
and	c.nr_seq_recomendacao = b.nr_sequencia
and	b.nr_prescricao = a.nr_prescricao
--and	a.dt_liberacao is not null
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_w) = 'S'
and	obter_se_exibir_rep_adep_setor(cd_setor_pac_w,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_prescricao = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	b.nr_sequencia = nr_seq_item_p
and	nvl(to_char(b.cd_recomendacao),b.ds_recomendacao) = cd_item_p
and	nvl(b.cd_intervalo,'XPTO') = nvl(cd_intervalo_p,'XPTO')
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'R'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	to_char((c.dt_horario + (qt_minutos_p / 1440)),'dd/mm/yy hh24:mi') /* sae */
from	pe_prescr_proc_hor c,
	pe_prescr_proc b,
	pe_prescricao a
where	c.nr_seq_pe_proc = b.nr_sequencia
and	b.nr_seq_prescr = a.nr_sequencia
and	a.dt_liberacao is not null
and	nvl(b.ie_adep,'N') = 'S'
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	a.nr_atendimento = nr_atendimento_p
and	a.nr_sequencia = nr_prescricao_p
and	a.dt_validade_prescr > sysdate
and	b.nr_sequencia = nr_seq_item_p
and	b.nr_seq_proc = cd_item_p
and	nvl(b.cd_intervalo,'XPTO') = nvl(cd_intervalo_p,'XPTO')
and c.dt_horario >= dt_hor_selecionado_w
and	((((c.dt_horario + (qt_minutos_p / 1440)) >= dt_validade_prescr_w) and
	   (qt_minutos_p > 0)) or
	 (((c.dt_horario + (qt_minutos_p / 1440)) < dt_inicio_prescr_w) and
	   (qt_minutos_p < 0)))
and	ie_tipo_item_p = 'E'
order by
	1;

begin
obter_param_usuario(924, 223, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_data_proc_w);
obter_param_usuario(1113, 115, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_data_lib_prescr_w);
obter_param_usuario(1113, 117, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_exibe_suspenso_w);

cd_setor_pac_w	:= Obter_Unidade_Atendimento(nr_atendimento_p, 'IA', 'CS');

if	(nr_atendimento_p is not null) and
	(ie_tipo_item_p is not null) and
	(cd_item_p is not null) and
	(ie_laboratorio_p is not null) and
	(nr_prescricao_p is not null) then
	/* atualizar lista eliminados */
	ds_lista_eliminados_w := ds_lista_eliminados_p;
	
	/* obter validade prescri��o */
	if	(ie_tipo_item_p <> 'E') then
		select	dt_validade_prescr,
				dt_inicio_prescr,
				cd_funcao_origem
		into	dt_validade_prescr_w,
				dt_inicio_prescr_w,
				cd_funcao_origem_w
		from	prescr_medica
		where 	nr_prescricao = nr_prescricao_p;
		
		if	((ie_tipo_item_p = 'M') and (cd_funcao_origem_w = 2314)) then
		
			select	nvl(max(nr_seq_mat_cpoe),0)
			into	nr_seq_mat_cpoe_w
			from	prescr_material
			where	nr_prescricao = nr_prescricao_p
			and		nr_sequencia = nr_seq_item_p;
			
			select 	max(decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)))
			into	dt_validade_cpoe_w
			from	cpoe_material a
			where	nr_sequencia = nr_seq_mat_cpoe_w;
			
			if	(dt_validade_cpoe_w is not null) and (dt_validade_cpoe_w < dt_validade_prescr_w) then
				dt_validade_prescr_w	:= dt_validade_cpoe_w;
			end if;
			
		end if;
		
	else
		select	dt_validade_prescr,
			dt_inicio_prescr
		into	dt_validade_prescr_w,
			dt_inicio_prescr_w
		from	pe_prescricao
		where	nr_sequencia = nr_prescricao_p;
	end if;
	
	if (ie_tipo_item_p = 'D') then
		select	max(dt_horario)
		into	dt_hor_selecionado_w
		from	prescr_dieta_hor
		where	nr_sequencia = nr_seq_horario_p;
	elsif (ie_tipo_item_p in ('S','M','MAT')) then
		select	max(dt_horario)
		into	dt_hor_selecionado_w
		from	prescr_mat_hor
		where	nr_sequencia = nr_seq_horario_p;
	elsif (ie_tipo_item_p in ('P','G','C')) then
		select	max(dt_horario)
		into	dt_hor_selecionado_w
		from	prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p;
	elsif (ie_tipo_item_p = 'R') then
		select	max(dt_horario)
		into	dt_hor_selecionado_w
		from	prescr_rec_hor
		where	nr_sequencia = nr_seq_horario_p;
	elsif (ie_tipo_item_p = 'E') then
		select	max(dt_horario)
		into	dt_hor_selecionado_w
		from	pe_prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p;
	end if;
	
	/* eliminar hor�rios */
	open c01;
	loop
	fetch c01 into
		dt_horario_w;
	exit when c01%notfound;	
		begin
		if	(ie_tipo_item_p = 'D') then /* dietas orais*/
			/* inserir hor�rio lista */
			if	(ds_lista_eliminados_w is null) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			elsif	(nvl(length(ds_lista_eliminados_w),0) <= 240) and
				(instr(ds_lista_eliminados_w, dt_horario_w) = 0) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			end if;

		elsif	(ie_tipo_item_p in ('S','M','MAT')) then /* suplementos orais e medicamentos */
			/* inserir hor�rio lista */
			if	(ds_lista_eliminados_w is null) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			elsif	(nvl(length(ds_lista_eliminados_w),0) <= 240) and
				(instr(ds_lista_eliminados_w, dt_horario_w) = 0) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			end if;

		elsif	(ie_tipo_item_p in ('P','G','C')) then /* procedimentos e controles de glicemia */
			/* inserir hor�rio lista */
			if	(ds_lista_eliminados_w is null) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			elsif	(nvl(length(ds_lista_eliminados_w),0) <= 240) and
				(instr(ds_lista_eliminados_w, dt_horario_w) = 0) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			end if;

		elsif	(ie_tipo_item_p = 'R') then /* recomenda��es */
			/* inserir hor�rio lista */
			if	(ds_lista_eliminados_w is null) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			elsif	(nvl(length(ds_lista_eliminados_w),0) <= 240) and
				(instr(ds_lista_eliminados_w, dt_horario_w) = 0) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			end if;

		elsif	(ie_tipo_item_p = 'E') then /* sae */
			/* inserir hor�rio lista */
			if	(ds_lista_eliminados_w is null) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			elsif	(nvl(length(ds_lista_eliminados_w),0) <= 240) and
				(instr(ds_lista_eliminados_w, dt_horario_w) = 0) then
				ds_lista_eliminados_w := ds_lista_eliminados_w || dt_horario_w || ',';
			end if;
		end if;
		end;
	end loop;
	close c01;	
end if;

ds_lista_eliminados_p := substr(ds_lista_eliminados_w,1,length(ds_lista_eliminados_w)-1);

commit;

end Obter_horarios_eliminados;
/