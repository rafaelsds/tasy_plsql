create or replace
procedure hd_atualiza_saida_unidade(nr_atendimento_p	number,
									nr_seq_dialise_p	number,
									dt_final_p			date) is 

cd_setor_atendimento_w	hd_ponto_acesso.cd_setor_atendimento%type;
cd_unidade_basica_w		hd_ponto_acesso.cd_unidade_basica%type;
cd_unidade_compl_w		hd_ponto_acesso.cd_unidade_compl%type;
dt_entrada_unidade_w	atend_paciente_unidade.dt_entrada_unidade%type;

begin
SELECT max(b.cd_setor_atendimento),
	   max(b.cd_unidade_basica),
	   max(b.cd_unidade_compl)
into	cd_setor_atendimento_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w
FROM   HD_DIALISE_DIALISADOR a,
	   hd_ponto_acesso b
WHERE  a.nr_sequencia = (SELECT MAX(x.nr_sequencia)
	   				  FROM	HD_DIALISE_DIALISADOR x
					  WHERE	x.nr_seq_dialise = nr_seq_dialise_p)
AND	   a.nr_seq_ponto_acesso = b.nr_sequencia;

SELECT max(dt_entrada_unidade)
into	dt_entrada_unidade_w
FROM   atend_paciente_unidade
WHERE  nr_atendimento = nr_atendimento_p
AND	   dt_saida_unidade IS NULL
and		cd_setor_atendimento = cd_setor_atendimento_w
and		cd_unidade_basica = cd_unidade_basica_w
and		cd_unidade_compl = cd_unidade_compl_w;

if	(dt_entrada_unidade_w is not null) then
	update	atend_paciente_unidade
	set		dt_saida_unidade = nvl(dt_final_p,sysdate)
	where	nr_atendimento = nr_atendimento_p
	and		dt_entrada_unidade = dt_entrada_unidade_w;
end if;

commit;

end hd_atualiza_saida_unidade;
/