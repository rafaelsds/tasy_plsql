create or replace
function html5_obter_se_data_periodo
			(	cd_funcao_p		number,
				dt_inicial_p		date,
				dt_final_p		date,
				ie_tipo_data_p		varchar2,
				ie_data_vazia_p		varchar2 default 'N')
				return varchar2 is

ds_retorno_w			varchar2(255) := 'S';

begin

-- 1- In�cio prev an�lise = dt_prev_inicio_analise
if	(ie_tipo_data_p = '1') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_inicio_analise between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_inicio_analise is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;	
-- 2- Prev checkpoint = dt_prev_checkpoint
elsif	(ie_tipo_data_p = '2') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_checkpoint between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_checkpoint is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 3- In�cio prev programa��o = dt_prev_inicio_prog
elsif	(ie_tipo_data_p = '3') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_inicio_prog between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_inicio_prog is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 4- In�cio prev teste = dt_prev_inicio_teste
elsif	(ie_tipo_data_p = '4') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_inicio_teste between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_inicio_teste is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 5- Prev aprova��o gerente = dt_prev_aprov_gerente
elsif	(ie_tipo_data_p = '5') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_aprov_gerente between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_aprov_gerente is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 6- Fim prev programa��o = dt_prev_fim_prog
elsif	(ie_tipo_data_p = '6') then	
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_fim_prog between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_fim_prog is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
--7- Fim prev an�lise = dt_prev_fim_analise
elsif	(ie_tipo_data_p = '7') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_fim_analise between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_fim_analise is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 8- Fim prev teste = dt_prev_fim_teste
elsif	(ie_tipo_data_p = '8') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_prev_fim_teste between dt_inicial_p and fim_dia(dt_final_p)
			and		((nvl(ie_data_vazia_p,'N') = 'N') or ((nvl(ie_data_vazia_p,'N') = 'S') and ( a.dt_real_fim_teste is null)))
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 9- In�cio real programa��o = dt_real_inicio_prog
elsif	(ie_tipo_data_p = '9') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_inicio_prog between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 10- Fim real programa��o = dt_real_fim_prog
elsif	(ie_tipo_data_p = '10') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_fim_prog between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 11- In�cio real an�lise = dt_real_inicio_analise
elsif	(ie_tipo_data_p = '11') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_inicio_analise between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 12- Fim real an�lise = dt_real_fim_analise
elsif	(ie_tipo_data_p = '12') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_fim_analise between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 13- In�cio real teste = dt_real_inicio_teste
elsif	(ie_tipo_data_p = '13') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_inicio_teste between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 14- Fim real teste = dt_real_fim_teste
elsif	(ie_tipo_data_p = '14') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_fim_teste between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 15- Aprova��o do Gerente = dt_real_aprov_gerente
elsif	(ie_tipo_data_p = '15') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_aprov_gerente between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
-- 16- Real checkpoint = dt_real_checkpoint
elsif	(ie_tipo_data_p = '16') then
	ds_retorno_w	:= 'N';
	
	begin
	select	'S'
	into	ds_retorno_w
	from	dual
	where	exists (select	1
			from	funcoes_html5 a
			where	a.dt_real_checkpoint between dt_inicial_p and fim_dia(dt_final_p)
			and	a.cd_funcao	= cd_funcao_p);
	exception
	when others then
		ds_retorno_w	:= 'N';
	end;
end if;

return	

ds_retorno_w;

end html5_obter_se_data_periodo;
/