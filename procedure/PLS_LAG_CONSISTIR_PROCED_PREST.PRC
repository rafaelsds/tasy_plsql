create or replace
procedure pls_lag_consistir_proced_prest
				(	nr_seq_lote_guia_imp_p 	number,
					nr_seq_lote_proc_imp_p 	number,
					nr_seq_guia_plano_p	number,
					nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Consistir se o 'Credenciado n�o � habilitado a realizar o procedimento'.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
					
ie_tipo_guia_w			varchar2(2);
ie_carater_internacao_w		varchar2(1);
ie_internacao_w			varchar2(1);
ie_liberar_w			varchar2(1)	:= 'N';
cd_procedimento_w		number(15);
nr_seq_prestador_exec_w		number(10)	:= null;
nr_seq_tipo_atend_w		number(10)	:= null;
ie_origem_proced_w		number(10);
nr_seq_prestador_w		number(10);
nr_seq_segurado_w		number(10);
dt_procedimento_w		date;

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		nvl(dt_prev_realizacao, sysdate)
	from	pls_lote_anexo_proc_imp
	where	nr_sequencia = nr_seq_lote_proc_imp_p;
	
begin	
if	((nr_seq_guia_plano_p is not null) and (nr_seq_guia_plano_p > 0)) then
	begin
		select	nr_seq_prestador,
			ie_tipo_guia,
			ie_carater_internacao,
			nr_seq_segurado
		into	nr_seq_prestador_w,
			ie_tipo_guia_w,
			ie_carater_internacao_w,
			nr_seq_segurado_w
		from	pls_guia_plano
		where	nr_sequencia 		= nr_seq_guia_plano_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
		ie_tipo_guia_w		:= null;
		ie_carater_internacao_w	:= null;
		nr_seq_segurado_w	:= null;
	end;
	
	ie_internacao_w := pls_obter_se_internado_guia(nr_seq_guia_plano_p);
	
	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_procedimento_w;
	exit when C01%notfound;
		begin
		ie_liberar_w	:= pls_obter_se_proc_prestador(	nr_seq_prestador_w,
								nr_seq_prestador_exec_w, 
								cd_procedimento_w,
								ie_origem_proced_w,
								ie_tipo_guia_w,
								nvl(dt_procedimento_w, sysdate),
								null,
								ie_carater_internacao_w,
								nr_seq_tipo_atend_w,
								ie_internacao_w,
								nr_seq_guia_plano_p);
		if	(ie_liberar_w = 'N') then		
			pls_inserir_anexo_glosa_aut('1214', nr_seq_lote_guia_imp_p, nr_seq_lote_proc_imp_p, null, '', nm_usuario_p);
		end if;
	
		end;
	end loop;
	close C01;
end if;

end pls_lag_consistir_proced_prest;
/