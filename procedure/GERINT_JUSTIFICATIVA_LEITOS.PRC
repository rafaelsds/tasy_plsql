create or replace 
procedure gerint_justificativa_leitos(
nr_sequencia_evento_p number default 0,
		dt_internacao_p date default null,
		ds_justif_data_retroativa_p varchar default null
		) is
		
qt_registros_w integer;

begin

if	nr_sequencia_evento_p <> 0 then
	
	select	count(*)
	into	qt_registros_w
	from	GERINT_EVENTO_INT_DADOS
	where	nr_seq_evento = nr_sequencia_evento_p;
	
	if	qt_registros_w > 0 then
		update	GERINT_EVENTO_INT_DADOS
		set		dt_internacao = dt_internacao_p,
				ds_justif_data_retroativa = ds_justif_data_retroativa_p
		where	NR_SEQ_EVENTO = nr_sequencia_evento_p;									
	else
		insert into GERINT_EVENTO_INT_DADOS(	NR_SEQ_EVENTO,
												dt_internacao,
												ds_justif_data_retroativa
												)
									values (	nr_sequencia_evento_p,
												dt_internacao_p,
												ds_justif_data_retroativa_p
												);
	end if;
end if;

end;
/