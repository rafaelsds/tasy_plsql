create or replace
procedure pls_oc_cta_tratar_val_79_imp(	nr_seq_combinada_p	pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p	pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p	pls_oc_cta_selecao_imp.nr_id_transacao%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Aplicar a valida��o de senha de itens na importa��o de XML de conta m�dca
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;
nr_seq_guia_w		pls_guia_plano.nr_sequencia%type;
ie_status_w		pls_guia_plano.ie_status%type;
dt_validade_w		pls_guia_plano.dt_validade_senha%type;
cd_senha_w 		pls_guia_plano.cd_senha%type;
nr_idx_w		pls_integer;
ie_valido_w		pls_oc_cta_selecao_imp.ie_valido%type;
ie_verificar_w		varchar2(1);
qt_item_autorizado_w	pls_guia_plano_proc.qt_autorizada%type;

-- Cursor das regras
cursor C01 (	nr_seq_oc_cta_comb_p	pls_oc_cta_combinada.nr_sequencia%type) is
	select	a.ie_valida_senha
	from	pls_oc_cta_val_aut_senha a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

-- Cursor das contas com os campos necess�rio para encontrar a autoriza��o pela senha 
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_sequencia%type)	is
	select distinct	a.nr_sequencia,
			a.nr_seq_guia_conv 	nr_seq_guia,
			a.cd_senha,
			a.nr_seq_segurado_conv 	nr_seq_segurado
	from		pls_oc_cta_selecao_imp	x,
			pls_conta_imp		a
	where		x.ie_valido		= 'S'
	and		x.nr_id_transacao	= nr_id_transacao_pc
	and		a.nr_sequencia		= x.nr_seq_conta;

cursor C03 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_sequencia%type,
		nr_seq_conta_pc		pls_conta_proc_imp.nr_seq_conta%type) is
	select 	x.nr_sequencia		nr_seq_selecao,
		a.dt_execucao_conv	dt_item,
		a.qt_executado		qt_item,
		a.ie_origem_proced_conv	ie_origem_proced,
		a.cd_procedimento_conv	cd_procedimento,
		null			nr_seq_material
	from	pls_oc_cta_selecao_imp	x,
		pls_conta_proc_imp	a
	where	x.ie_valido		= 'S'
	and	x.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_sequencia 		= x.nr_seq_conta_proc
	and	x.nr_seq_conta		= nr_seq_conta_pc
	and	a.cd_procedimento_conv	is not null
	and	a.ie_origem_proced_conv	is not null
	union all
	select 	x.nr_sequencia		nr_seq_selecao,
		a.dt_execucao_conv	dt_item,
		a.qt_executado		qt_item,
		null			ie_origem_proced,
		null			cd_procedimento,
		a.nr_seq_material_conv	nr_seq_material
	from	pls_oc_cta_selecao_imp	x,
		pls_conta_mat_imp	a
	where	x.ie_valido		= 'S'
	and	x.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_sequencia 		= x.nr_seq_conta_mat
	and	x.nr_seq_conta		= nr_seq_conta_pc
	and	a.nr_seq_material_conv	is not null;

begin
if	(nr_seq_combinada_p is not null)  then
	for r_C01_w in C01(nr_seq_combinada_p) loop
		if	(r_C01_w.ie_valida_senha = 'S')	then

			pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', ie_regra_excecao_p, nr_id_transacao_p, null);

			for r_C02_w in C02(nr_id_transacao_p) loop
				nr_seq_guia_w:= 	null;
				ie_status_w:= 		null;
				dt_validade_w:= 	null;
				cd_senha_w:= 		null;

				ie_verificar_w:= 'N';

				if (r_C02_w.cd_senha is not null) then
					ie_verificar_w:= 'S';
					
					select 	max(nr_sequencia),
						max(ie_status),
						max(dt_validade_senha),
						max(cd_senha)
					into	nr_seq_guia_w,
						ie_status_w,
						dt_validade_w,
						cd_senha_w
					from	pls_guia_plano
					where	cd_senha	= r_C02_w.cd_senha
					and	nr_seq_segurado	= r_C02_w.nr_seq_segurado;
				end if;

				-- Incializar as listas para cada regra.
				pls_ocor_imp_pck.limpar_nested_tables(tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
				nr_idx_w:=	0;

				-- S� faz valida��o se tiver cd_senha ou nr_seq_guia informado
				if (ie_verificar_w = 'S') then
					for r_C03_w in	C03( nr_id_transacao_p, r_C02_w.nr_sequencia) loop
						ie_valido_w:= 'N';
						tb_observacao_w(nr_idx_w):=	'';
						if	(nr_seq_guia_w is null) then
							-- Vai gerar ocorrencia
							ie_valido_w:= 'S';
							tb_observacao_w(nr_idx_w):=	'Item n�o autorizado, n�o foi encontrada autoriza��o.';
						elsif	(nr_seq_guia_w is not null and ie_status_w <> '1') then
							-- Vai gerar ocorrencia
							ie_valido_w:= 'S';
							tb_observacao_w(nr_idx_w):=	'Item n�o autorizado.';
						else
							if	(r_C03_w.dt_item > dt_validade_w) then
								ie_valido_w:= 'S';
								tb_observacao_w(nr_idx_w):=	'Item n�o autorizado, data superior a validade da senha';
							end if;
								
							if	(ie_valido_w = 'N') then
								if	(r_C03_w.cd_procedimento is not null) then
									select 	sum(a.qt_autorizada)
									into	qt_item_autorizado_w
									from	pls_guia_plano_proc a
									where	a.nr_seq_guia 		= nr_seq_guia_w
									and	a.ie_origem_proced	= r_c03_w.ie_origem_proced
									and	a.cd_procedimento	= r_c03_w.cd_procedimento
									and	a.ie_status in ('S','P','L');
								elsif	(r_C03_w.nr_seq_material is not null) then
									select 	sum(a.qt_autorizada)
									into	qt_item_autorizado_w
									from	pls_guia_plano_mat a
									where	a.nr_seq_guia 		= nr_seq_guia_w
									and	a.nr_seq_material	= r_c03_w.nr_seq_material
									and	a.ie_status in ('S','P','L');
								end if;
								if	(r_C03_w.qt_item > nvl(qt_item_autorizado_w,0)) then
									ie_valido_w:= 'S';
									tb_observacao_w(nr_idx_w):=	'Item n�o autorizado, quantidade apresentada maior que a autorizada.';
								end if;
							end if;
						end if;
						tb_seq_selecao_w(nr_idx_w):=	r_C03_w.nr_seq_selecao;
						tb_valido_w(nr_idx_w):=	ie_valido_w;
						nr_idx_w := nr_idx_w + 1;

						if	(nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
							pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w, nr_id_transacao_p,'SEQ');
							
							nr_idx_w := 0;
							-- Incializar as listas para cada regra.
							pls_ocor_imp_pck.limpar_nested_tables(tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
							
						end if;
					end loop;
					if	(nr_idx_w > 0)	then
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w, nr_id_transacao_p,'SEQ');
					end if;
				end if;
			end loop;
			-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
			pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', ie_regra_excecao_p, null, nr_id_transacao_p, null);
		end if;
	end loop;
end if;
end pls_oc_cta_tratar_val_79_imp;
/
