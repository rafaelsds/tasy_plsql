create or replace procedure gerar_w_int_local_estoque(
					cd_centro_custo_p		varchar2,                 
					cd_comprador_consig_p		varchar2,             
					cd_estabelecimento_p		varchar2,              
					cd_pessoa_solic_consig_p	varchar2,          
					ds_complemento_p		varchar2,                  
					ds_local_estoque_p		varchar2,                           
					ie_baixa_disp_p			varchar2,                   
					ie_centro_custo_nf_p		varchar2,              
					ie_centro_custo_ordem_p		varchar2,           
					ie_centro_inventario_p		varchar2,            
					ie_considera_transf_padrao_p	varchar2,      
					ie_consiste_saldo_rep_p		varchar2,           
					ie_externo_p			varchar2,                      
					ie_gera_lote_p			varchar2,                    
					ie_limpa_requisicao_p		varchar2,             
					ie_local_entrega_oc_p		varchar2,             
					ie_permite_digitacao_p		varchar2,            
					ie_permite_emprestimo_p		varchar2,           
					ie_permite_estoque_negativo_p	varchar2,     
					ie_permite_oc_p			varchar2,                   
					ie_ponto_pedido_p		varchar2,                 
					ie_proprio_p			varchar2,                      
					ie_req_automatica_p		varchar2,               
					ie_req_mat_estoque_p		varchar2,              
					ie_situacao_p			varchar2,                     
					ie_tipo_local_p			varchar2,                                 
					qt_dia_ajuste_maximo_p		varchar2,            
					qt_dia_ajuste_minimo_p		varchar2,            
					qt_dia_consumo_p		varchar2, 
					nm_usuario_p			varchar2,
					nr_seq_local_estoque_p		out varchar2) is
					
				
  					
                 					
				
				
nr_seq_local_estoque_w	number(10);	
ds_erro_w	varchar2(4000);			

begin

delete w_int_local_estoque
where nm_usuario = nm_usuario_p;

select	w_int_local_estoque_seq.nextval
into	nr_seq_local_estoque_w
from	dual;

insert into w_int_local_estoque(
			nr_sequencia,           
			cd_centro_custo,                 
			cd_comprador_consig,             
			cd_estabelecimento,              
			cd_pessoa_solic_consig,          
			ds_complemento,                  
			ds_local_estoque,                           
			ie_baixa_disp,                   
			ie_centro_custo_nf,              
			ie_centro_custo_ordem,           
			ie_centro_inventario,            
			ie_considera_transf_padrao,      
			ie_consiste_saldo_rep,           
			ie_externo,                      
			ie_gera_lote,                    
			ie_limpa_requisicao,             
			ie_local_entrega_oc,             
			ie_permite_digitacao,            
			ie_permite_emprestimo,           
			ie_permite_estoque_negativo,     
			ie_permite_oc,                   
			ie_ponto_pedido,                 
			ie_proprio,                      
			ie_req_automatica,               
			ie_req_mat_estoque,              
			ie_situacao,                     
			ie_tipo_local,                                 
			qt_dia_ajuste_maximo,            
			qt_dia_ajuste_minimo,            
			qt_dia_consumo, 
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,          
			dt_atualizacao_nrec)
values( nr_seq_local_estoque_w,
	cd_centro_custo_p,                 
	cd_comprador_consig_p,             
	cd_estabelecimento_p,              
	cd_pessoa_solic_consig_p,          
	ds_complemento_p,                  
	ds_local_estoque_p,                           
	ie_baixa_disp_p,                   
	ie_centro_custo_nf_p,              
	ie_centro_custo_ordem_p,           
	ie_centro_inventario_p,            
	ie_considera_transf_padrao_p,      
	ie_consiste_saldo_rep_p,           
	ie_externo_p,                      
	ie_gera_lote_p,                    
	ie_limpa_requisicao_p,             
	ie_local_entrega_oc_p,             
	ie_permite_digitacao_p,            
	ie_permite_emprestimo_p,           
	ie_permite_estoque_negativo_p,     
	ie_permite_oc_p,                   
	ie_ponto_pedido_p,                 
	ie_proprio_p,                      
	ie_req_automatica_p,               
	ie_req_mat_estoque_p,              
	ie_situacao_p,                     
	ie_tipo_local_p,                                 
	to_number(qt_dia_ajuste_maximo_p),            
	to_number(qt_dia_ajuste_minimo_p),            
	to_number(qt_dia_consumo_p),      
	nm_usuario_p,
	nm_usuario_p,
	sysdate,
	sysdate); 
		
nr_seq_local_estoque_p := nr_seq_local_estoque_w;	

commit;


end gerar_w_int_local_estoque;
/