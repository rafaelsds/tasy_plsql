create or replace
procedure obter_tx_juros_multa_cre(
				cd_estabelecimento_p	in	number,
				ie_origem_titulo_p		in	varchar2,
				ie_tipo_titulo_p	in	varchar2,
				tx_juros_p		out	number,
				tx_multa_p		out	number) is 

qt_registros_w	number(5);
cd_empresa_w	number(4);


Cursor C01 is
select	tx_juros,
	tx_multa
from	taxa_titulo_receber
where	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
and	nvl(cd_empresa,cd_empresa_w)			= cd_empresa_w
and	nvl(ie_origem_titulo,ie_origem_titulo_p)	= ie_origem_titulo_p
and	nvl(ie_tipo_titulo, ie_tipo_titulo_p)		= ie_tipo_titulo_p
and	sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate)
order by dt_inicio_vigencia,
	ie_origem_titulo,
	ie_tipo_titulo;

begin

select	cd_empresa
into	cd_empresa_w
from	estabelecimento 
where	cd_estabelecimento = cd_estabelecimento_p;

select	count(*)
into	qt_registros_w
from	taxa_titulo_receber;

if	(qt_registros_w > 0) then
	open C01;
	loop
	fetch C01 into	
		tx_juros_p,
		tx_multa_p;
	exit when C01%notfound;
	end loop;
	close C01;
end if;

end obter_tx_juros_multa_cre;
/