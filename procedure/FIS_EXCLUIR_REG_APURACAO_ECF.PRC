create or replace
procedure fis_excluir_reg_apuracao_ecf(	nr_sequencia_p	number,
						                            nm_usuario_p		varchar2) is 
begin
  begin
    delete	from fis_reg_apuracao_ecf
    where	nr_seq_lote  = nr_sequencia_p;

    delete	from fis_lote_ecf
    where	nr_sequencia = nr_sequencia_p;
    commit;

  exception when others then
    wheb_mensagem_pck.exibir_mensagem_abort(326208, 'DS_ERRO=' || SQLERRM);
  end;
end fis_excluir_reg_apuracao_ecf;
/
