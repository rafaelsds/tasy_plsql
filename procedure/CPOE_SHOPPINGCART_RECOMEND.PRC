create or replace 
procedure cpoe_shoppingcart_recomend(nr_sequencia_p	number,
					ie_retrogrado_p	varchar2,
					ds_retorno_p	out varchar2) is

ie_continuo_w		varchar2(1) := 'N';
ie_return_value_w	varchar2(1);
ie_alterar_interv_w	tipo_recomendacao.ie_alterar_interv%type;
					
cursor c01 is
select  a.*, obter_setor_atendimento(nr_atendimento) cd_setor_atend
from 	cpoe_recomendacao a
where	nr_sequencia = nr_sequencia_p;

begin

ds_retorno_p := 'S';

if (nr_sequencia_p is not null) then

	for r_c01_w in C01
	loop
	
		/* CAMPOS OBRIGATORIOS  */

		if (r_c01_w.cd_recomendacao is null or r_c01_w.dt_inicio is null 
			or r_c01_w.ie_administracao is null or r_c01_w.ie_duracao is null) then
			ds_retorno_p := 'N';
			goto return_value;
		end if;
		
		select	nvl(max(ie_alterar_interv),'S') ie_alterar_interv
		into	ie_alterar_interv_w
		from	tipo_recomendacao
		where	cd_tipo_recomendacao = r_c01_w.cd_recomendacao;
		
		if (r_c01_w.cd_intervalo is null and ie_alterar_interv_w = 'S') then
			ds_retorno_p := 'N';
			goto return_value;
		end if;
		
		if (cpoe_shoppingcart_vigencia(r_c01_w.cd_intervalo, r_c01_w.ie_administracao, r_c01_w.dt_inicio,
								r_c01_w.ie_urgencia, r_c01_w.ie_duracao,r_c01_w.dt_fim, r_c01_w.ie_evento_unico, ie_retrogrado_p) = 'S') then
			ds_retorno_p := 'N';
			goto return_value;
		end if;
				
		/* FIM CAMPOS OBRIGATORIOS  */

		/* INCONSISTÊNCIA DA CPOE (BARRA VERMELHA) */
	
		if (r_c01_w.dt_fim is not null) then
			ie_continuo_w := 'S';
		end if;
		
		/*DUPLICIDADE*/
		select	decode(count(*), 0, 'N', 'S') ie_duplicado 
		into	ie_return_value_w
		from	cpoe_recomendacao a
		where	a.nr_sequencia <> r_c01_w.nr_sequencia
		and		obter_se_cpoe_regra_duplic('R', wheb_usuario_pck.get_cd_perfil, r_c01_w.cd_setor_atend) = 'S' 
		and		((nr_atendimento = r_c01_w.nr_atendimento) or (cd_pessoa_fisica = r_c01_w.cd_pessoa_fisica and nr_atendimento is null))
		and		((cpoe_reg_valido_ativacao( decode(dt_lib_suspensao, null, dt_fim, nvl(dt_suspensao,dt_fim)), dt_inicio, sysdate) = 'S') or (ie_retrogrado = 'S' and dt_liberacao is null))
		and		a.cd_recomendacao = r_c01_w.cd_recomendacao
		and		(((dt_lib_suspensao is not null) and (dt_suspensao > r_c01_w.dt_inicio)) or (dt_lib_suspensao is null))
		and 	((dt_liberacao is not null) or (nm_usuario = wheb_usuario_pck.get_nm_usuario))
		and 	((a.dt_inicio between r_c01_w.dt_inicio and r_c01_w.dt_fim) or
				 (a.dt_fim between r_c01_w.dt_inicio and r_c01_w.dt_fim) or
				 (a.dt_fim is null and a.dt_inicio < r_c01_w.dt_inicio) or
				 (a.dt_fim > r_c01_w.dt_fim and a.dt_inicio < r_c01_w.dt_inicio) or	
				 (((a.dt_inicio >  r_c01_w.dt_inicio) and (nvl(ie_continuo_w,'N') = 'S')) or ((nvl(ie_continuo_w,'N') = 'N') and (r_c01_w.dt_inicio <= a.dt_inicio)  and r_c01_w.dt_fim > a.dt_inicio)) or
				 (a.ie_retrogrado = 'S' and a.dt_liberacao is null));

		if (ie_return_value_w = 'S') then
			ds_retorno_p := 'N';
			goto return_value;
		end if;
		
		/* FIM INCONSISTÊNCIA DA CPOE (BARRA VERMELHA) */
	

	end loop;
end if;
 
<<return_value>>		
 null;

exception when others then
	gravar_log_cpoe(substr('CPOE_SHOPPINGCART_RECOMEND EXCEPTION:'|| substr(to_char(sqlerrm),1,2000) || '//nr_sequencia_p: '|| nr_sequencia_p,1,40000));
	ds_retorno_p := 'N';
end cpoe_shoppingcart_recomend;
/
