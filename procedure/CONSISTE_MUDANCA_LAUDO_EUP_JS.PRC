create or replace
procedure consiste_mudanca_laudo_eup_js(
					CD_PROC_ORIGINAL_P	varchar2,
					ds_msg_erro_p		 out varchar2) is 
cd_retorno_w	number(10);
ds_msg_erro_w	varchar2(2000);
					
begin

cd_retorno_w := sus_obter_se_detalhe_proc(cd_proc_original_p,7,'008');

if (nvl(cd_retorno_w,0) > 0) then

	ds_msg_erro_w := obter_texto_dic_objeto(289794, 1, 'CD_PROC_ORIGINAL_P=' || cd_proc_original_p) || chr(10) ||
					  obter_texto_tasy(289795, 1);
end if;

ds_msg_erro_p := ds_msg_erro_w;

commit;

end consiste_mudanca_laudo_eup_js;
/