create or replace
procedure reservar_leito_unidade_atend(
		ie_consiste_idade_p		varchar2,
		cd_paciente_reserva_p		varchar2,
		nr_seq_interno_p		number,
		cd_convenio_reserva_p		varchar2,
		nr_seq_motivo_reserva_p		varchar2,					
		ie_tipo_reserva_p		varchar2,
		ie_status_unidade_p		varchar2,
		ds_observacao_p			varchar2,
		cd_unidade_basica_p		varchar2,
		cd_unidade_compl_p		varchar2,
		cd_setor_atendimento_p		number,
		ie_necessita_isol_reserva_p	varchar2,
		ds_retorno_p 	  out		varchar2,
		ds_consistencia_p out		varchar2,
		cd_estabelecimento_p		number,
		nm_usuario_p			varchar2) is
		
ds_retorno_w		varchar2(255);
ds_consistencia_w	varchar2(255);

ie_consiste_sexo_unid_w varchar2(1) := '';

begin

obter_param_usuario(44, 188, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_sexo_unid_w);

if	(cd_paciente_reserva_p is not null) and
	(nr_seq_interno_p is not null) and
	(cd_convenio_reserva_p is not null) and 
	(nr_seq_motivo_reserva_p is not null) and 			
	(nm_usuario_p is not null) and 
	(ie_tipo_reserva_p is not null) and 
	(ie_status_unidade_p is not null) and		
	(cd_unidade_basica_p is not null) and
	(cd_unidade_compl_p is not null) and
	(cd_setor_atendimento_p	is not null) and
	(ie_necessita_isol_reserva_p is not null) and
	(nm_usuario_p is not null) then
	begin
	if	(ie_consiste_idade_p = 'S') or
		(ie_consiste_idade_p = 'A') then
		begin	
		consistir_idade_unidade(cd_paciente_reserva_p, nr_seq_interno_p, ds_retorno_w);
	
		if 	(ie_consiste_idade_p = 'S') and
			(ds_retorno_w is not null) then
			begin		
			Wheb_mensagem_pck.exibir_mensagem_abort(264607, 'DS_RETORNO_W=' || ds_retorno_w);
			end;
		end if;
		end;
	end if;
	
	if	(ie_consiste_sexo_unid_w <> 'N') then
		begin
		
		consistir_sexo_unid_reserva(nr_seq_interno_p,cd_paciente_reserva_p,ds_consistencia_w);
		
		if	(ie_consiste_sexo_unid_w = 'S') and
			(ds_consistencia_w is not null) and
			((obter_se_quarto_familiar(cd_setor_atendimento_p,cd_unidade_basica_p,cd_unidade_compl_p,cd_paciente_reserva_p) = 'N'))then
			begin
			Wheb_mensagem_pck.exibir_mensagem_abort(279278, 'DS_CONSISTENCIA_P=' || ds_consistencia_w);		
			end;
		end if;
		
		end;
	end if;
	
	
	if	(ie_consiste_sexo_unid_w <> 'J') then
		begin
		update	unidade_atendimento
		set	cd_convenio_reserva 		= cd_convenio_reserva_p,
			nr_seq_motivo_reserva 		= nr_seq_motivo_reserva_p,
			cd_paciente_reserva		= cd_paciente_reserva_p,
			nr_seq_interno			= nr_seq_interno_p,
			nm_usuario_reserva		= nm_usuario_p,
			ie_tipo_reserva			= ie_tipo_reserva_p,
			ie_status_unidade		= ie_status_unidade_p,
			ds_observacao			= ds_observacao_p || wheb_mensagem_pck.get_texto(279279, 'NM_USUARIO_P=' || nm_usuario_p || ';DATA_P=' || to_char(sysdate,'dd/MM/yyyy hh24:mi:ss')),
			ie_necessita_isol_reserva	= ie_necessita_isol_reserva_p,
			dt_atualizacao			= sysdate,
			nm_usuario			= nm_usuario_p
		where	cd_unidade_basica		= cd_unidade_basica_p
		and	cd_unidade_compl		= cd_unidade_compl_p
		and	cd_setor_atendimento		= cd_setor_atendimento_p;
			end;
	end if;

	
	end;
end if;
ds_retorno_p		:= ds_retorno_w;
ds_consistencia_p	:= ds_consistencia_w;
commit;
end reservar_leito_unidade_atend;
/