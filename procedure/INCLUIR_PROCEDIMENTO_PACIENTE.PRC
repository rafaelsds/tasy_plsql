create or replace
procedure incluir_procedimento_paciente(
			cd_procedimento_p		in number,
			qt_procedimento_p		in number,
			nr_seq_exame_p	       	in number,
			nr_seq_proc_interno_p 	in number,
			ie_origem_proced_p	in number,
			cd_setor_atendimento_p	in number,
			nr_atendimento_p		in number,
			cd_estabelecimento_p	in number,
			nm_usuario_p		in varchar2,
			ie_tipo_baixa_p		in varchar2,
			dt_execucao_p		date,
			nr_sequencia_p		out number) is

/** juliane menin - faz a inser��o dos procedimentos - (execu��o da prescri��o )*/

nr_sequencia_w			number(10);
dt_entrada_unidade_w		date;
nr_seq_interno_w			number(10);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w		varchar2(20);
ie_tipo_guia_w			varchar2(2);
cd_senha_w			varchar2(20);
ie_emite_conta_w  			varchar2(3)	:= null; --no delphi 81(conta paciente-antiga) parametro 18 
cd_cgc_prestador_w 		varchar2(14);
ie_origem_proced_w		number(10);
ie_tipo_atendimento_w  		varchar2(2)	:=  null; 
ie_medico_executor_w		varchar2(2);
cd_cgc_w			varchar2(14);
cd_medico_executor_w		varchar2(10);


ie_funcao_medico_w		varchar2(10)	:= 0;
dt_procedimento_w			date;
cd_pessoa_fisica_w		varchar2(10);

cd_tipo_procedimento_w		number(3,0);
ie_classificacao_w			varchar2(1);
ie_tipo_lancto_w			varchar2(10);
cd_procedimento_w		number(15,0);
cd_profissional_w			varchar2(10);
nr_seq_cor_exec_w		number(15)	:= 96;
ds_observacao_w			varchar(255);
nr_seq_classificacao_w		number(10);

begin

	obter_param_usuario(24, 44, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_tipo_lancto_w);

	if( nr_seq_proc_interno_p is not null) then
		obter_proc_tab_interno( nr_seq_proc_interno_p, 0, nr_atendimento_p, 0, cd_procedimento_w, ie_origem_proced_w,null,null,null); 	
	end if;

	cd_procedimento_w := NVL(cd_procedimento_w, cd_procedimento_p);
	ie_origem_proced_w := NVL(ie_origem_proced_w, ie_origem_proced_p);

	-- obter o c�digo da pessoa f�sica
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atendimento_paciente 
	where	nr_atendimento 		= nr_atendimento_p;

	-- obter a data do procedimento( se a data da alta for igual a null ent�o dt_atendimento = dt_alta sen�o dt_atendimento = sysdate)
	begin
		select	dt_alta
		into	dt_procedimento_w
		from	atendimento_paciente
		where	cd_pessoa_fisica    = cd_pessoa_fisica_w
		and	nr_atendimento      = nr_atendimento_p;
	exception
		when others then
		dt_procedimento_w	:=	sysdate;
	end;

	if ( dt_procedimento_w is null) then
		dt_procedimento_w	:=	sysdate;
	end if;

	select  max(a.dt_entrada_unidade), 
		max(a.nr_seq_interno)
	into	dt_entrada_unidade_w,
		nr_seq_interno_w
	from	tipo_acomodacao b,
		setor_atendimento c,
		atend_paciente_unidade a
	where	a.cd_tipo_acomodacao	= b.cd_tipo_acomodacao
	and	a.cd_setor_atendimento  = c.cd_setor_atendimento
	and	nr_atendimento		= nr_atendimento_p
	and	a.cd_setor_atendimento  = cd_setor_atendimento_p;

	

	select	a.cd_cgc 
	into	cd_cgc_prestador_w 
	from 	estabelecimento a,
		atendimento_paciente b
	where	a.cd_estabelecimento 	= b.cd_estabelecimento
	and	b.nr_atendimento 	= nr_atendimento_p;
	
	select	nr_seq_classificacao
	into	nr_seq_classificacao_w
	from	atendimento_paciente 
	where	nr_atendimento 		= nr_atendimento_p;

	-- obter o conv�nio de execucao 
	obter_convenio_execucao(nr_atendimento_p, dt_entrada_unidade_w, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);

	-- obter m�dico executor 
	consiste_medico_executor(cd_estabelecimento_p, cd_convenio_w, cd_setor_atendimento_p, cd_procedimento_w, ie_origem_proced_w, 
			ie_tipo_atendimento_w, nr_seq_exame_p,nr_seq_proc_interno_p,
			ie_medico_executor_w,cd_cgc_w, cd_medico_executor_w, cd_profissional_w, null, dt_procedimento_w,
			nr_seq_classificacao_w,'N',null, null); 

	-- obter a m�xima sequencia da procedimento_paciente 
	select		procedimento_paciente_seq.nextval
	into		nr_sequencia_w
	from 		dual;

	if	( cd_medico_executor_w is null or cd_medico_executor_w = '') then
		ie_funcao_medico_w	:=	null;
	end if;

	if	( ie_tipo_baixa_p = 'P') then --via palmweb
		nr_seq_cor_exec_w := 1013;
		ds_observacao_w	  := 'PalmWeb';

	end if;

	-- inserir na tabela procedimento_paciente
	insert into procedimento_paciente( 
				nr_sequencia,
				nr_atendimento,
				dt_entrada_unidade,
				cd_procedimento,
				dt_procedimento,
				cd_convenio,
				cd_categoria,
				nr_doc_convenio,
				ie_tipo_guia,
				cd_senha,
				ie_auditoria,
				ie_emite_conta,
				cd_cgc_prestador,
				ie_origem_proced,
				nr_seq_exame,
				nr_seq_proc_interno,
				qt_procedimento,
				cd_setor_atendimento,
				nr_seq_atepacu,
				nr_seq_cor_exec,
				ie_funcao_medico,
				vl_procedimento,
				ie_proc_princ_atend,
				ie_video,
				tx_medico,
				tx_anestesia,
				tx_procedimento,
				ie_valor_informado,
				ie_guia_informada,
				cd_situacao_glosa,
				nm_usuario_original,
				ds_observacao,
				dt_atualizacao,
				nm_usuario,
				cd_pessoa_fisica)
			values( 
				nr_sequencia_w,
				nr_atendimento_p,
				dt_entrada_unidade_w,
				cd_procedimento_w,
				dt_procedimento_w,
				cd_convenio_w,
				cd_categoria_w,
				nr_doc_convenio_w,
				ie_tipo_guia_w,
				cd_senha_w,
				'N',
				ie_emite_conta_w,
				cd_cgc_prestador_w,
				ie_origem_proced_w,
				nr_seq_exame_p,
				nr_seq_proc_interno_p,
				qt_procedimento_p,
				cd_setor_atendimento_p,
				nr_seq_interno_w,
				nr_seq_cor_exec_w,
				ie_funcao_medico_w,
				100,
				'N',
				'N',
				100,
				100,
				100,
				'N',
				'N',
				0,
				nm_usuario_p,
				ds_observacao_w,
				sysdate,
				nm_usuario_p,
				cd_profissional_w);


			commit;


			select	max(ie_classificacao),
			max(cd_tipo_procedimento)
			into	ie_classificacao_w,
				cd_tipo_procedimento_w
			from	procedimento
			where	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced;

			if	(ie_classificacao_w in (1,8)) then
	
				atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_w, nm_usuario_p);
		
				gerar_taxa_sala_porte(nr_atendimento_p, dt_entrada_unidade_w, dt_procedimento_w, cd_procedimento_w, nr_sequencia_w, nm_usuario_p);
			else
				atualiza_preco_servico(nr_sequencia_w,nm_usuario_p);
			end if;
			
			atualizar_agenda_propaci(nr_sequencia_w);

			if	(nvl(ie_tipo_lancto_w,'0') = '0') then				
				gerar_lancamento_automatico(nr_atendimento_p, null, 34, nm_usuario_p, nr_sequencia_w, null,null,null,null,null);
			end if;

			gerar_autor_regra(nr_atendimento_p, null, nr_sequencia_w, null, null, nr_seq_proc_interno_p,
					'EP',nm_usuario_p,null,null,null,null,null,null,'','','');

nr_sequencia_p	:= nr_sequencia_w;

commit;

end incluir_procedimento_paciente;
/