CREATE OR REPLACE
PROCEDURE estornar_liberacao_devolucao(	nr_devolucao_p	in	Number,
						ds_consistencia_p	out	varchar2) IS

qt_item_w		number(1);
ds_consistencia_w	varchar2(255);

BEGIN 
qt_item_w		:= 0;

select	count(*)
into	qt_item_w
from	item_devolucao_material_pac
where	nr_devolucao = nr_devolucao_p
and	dt_recebimento is not null;

if	(qt_item_w > 0) then
	ds_consistencia_w	:= obter_desc_expressao(616248); --'Existem materiais j� recebidos, imposs�vel Estornar';
else
	update devolucao_material_pac 
	set	 dt_liberacao_baixa = null 
	where	 nr_devolucao = nr_devolucao_p;
end if;

if	(ds_consistencia_w is not null) then
	ds_consistencia_p	:= substr(ds_consistencia_w,1,254);
end if;

END estornar_liberacao_devolucao;
/