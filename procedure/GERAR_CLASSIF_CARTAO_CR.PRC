create or replace
procedure GERAR_CLASSIF_CARTAO_CR(	nr_seq_movto_p	number,
					nm_usuario_p	varchar,
					ie_commit_p		varchar default 'S') is

cd_conta_financ_w		number(10);
vl_classificacao_w		number(15,2);
cont_w			number(10);
nr_seq_movto_w		number(10);
cd_estabelecimento_w	number(10);
cd_cgc_w		varchar2(14);
nr_seq_produto_w		number(10);
nr_seq_grupo_w		number(10);
vl_classificacao_mccc_w	number(15,2);

begin
cd_conta_financ_w	:= 0;

select	nvl(max(nr_sequencia),0)
into	nr_seq_movto_w
from	movto_cartao_cr
where	nr_sequencia	= nvl(nr_seq_movto_p,0);

if	(nr_seq_movto_w > 0) then

	select	max(obter_conta_financ_cartao_cr(a.nr_sequencia)),
		max(vl_transacao),
		max(cd_estabelecimento),
		max(cd_cgc)
	into	cd_conta_financ_w,
		vl_classificacao_w,
		cd_estabelecimento_w,
		cd_cgc_w
	from	movto_cartao_cr a
	where	nr_sequencia	= nr_seq_movto_p;

	if	(vl_classificacao_w = 0) then
		--r.aise_application_error(-20011, 'O valor da classifica��o da movimenta��o de cart�o n�o pode ser zero!');
		wheb_mensagem_pck.exibir_mensagem_abort(215515);
	end if;

	select	count(*)
	into	cont_w
	from	movto_cartao_cr_classif
	where	nr_seq_movto	= nr_seq_movto_p;
	
	obter_produto_financeiro(cd_estabelecimento_w,null,cd_cgc_w,nr_seq_produto_w,null,null,nr_seq_grupo_w);
	
	if	(nr_seq_grupo_w is null) and
		(nr_seq_produto_w is not null) then
		
		select	max(b.nr_seq_grupo_sup)	
		into	nr_seq_grupo_w
		from	produto_financeiro a,
			grupo_prod_financ b
		where	b.nr_sequencia	= a.nr_seq_grupo
		and	a.nr_sequencia	= nr_seq_produto_w;
		
	end if;		
		
	if	(nr_seq_grupo_w is not null) then
	
		update	movto_cartao_cr
		set	nr_seq_grupo_prod	= nr_seq_grupo_w
		where	nr_sequencia		= nr_seq_movto_p;
	
	end if;	
	
	if 	(nvl(cd_conta_financ_w,0) = 0) then
		select 	max(a.cd_conta_financ)
		into	cd_conta_financ_w
		from 	transacao_financeira a,
			movto_cartao_cr b
		where	b.nr_seq_trans_caixa = a.nr_sequencia
		and	b.nr_sequencia = nr_seq_movto_p;
	end if;
		
	if	(cont_w = 0) and
		(cd_conta_financ_w is not null) then
		insert into movto_cartao_cr_classif
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_movto,
				cd_conta_financ,
				vl_classificacao,
				nr_seq_produto)
		values		(movto_cartao_cr_classif_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_movto_p,
				cd_conta_financ_w,
				vl_classificacao_w,
				nr_seq_produto_w);
	end if;
	
	if	(nvl(cont_w, 0) > 0) then
		begin
	
		select 	max(vl_classificacao)		
		into 	vl_classificacao_mccc_w	
		from	movto_cartao_cr_classif
		where	nr_seq_movto		= nr_seq_movto_p;
	
		if nvl(vl_classificacao_mccc_w, 0) <> nvl(vl_classificacao_w, 0) then
			update movto_cartao_cr_classif
			set vl_classificacao		 = vl_classificacao_w 
			where nr_seq_movto	 = nr_seq_movto_p;			
		end if;
		
		end;
	end if;
	
end if;

/*OS 1754739 - Essa rotina eh chamada dentro da baixa_titulo_cartao_integracao, e nao pode ter commit aqui, por isso criado esse parametro.*/
if (nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end GERAR_CLASSIF_CARTAO_CR;
/