create or replace
procedure alt_status_net_cot_compra_forn(
			ie_status_p		varchar2,
			ie_liberada_internet_p	varchar2,
			nr_sequencia_p		varchar2) is

begin

update	cot_compra_forn
set	ie_status = ie_status_p,
	ie_liberada_internet = ie_liberada_internet_p
where	nr_sequencia = nr_sequencia_p;

commit;

end alt_status_net_cot_compra_forn;
/