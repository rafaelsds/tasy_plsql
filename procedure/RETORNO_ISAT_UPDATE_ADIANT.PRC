create or replace
procedure retorno_ISAT_update_adiant(
			ds_obs_p		varchar2,
			nr_seq_caixa_p		varchar2,
			nr_adiantamento_p	varchar2) is 

begin

update	adiantamento a
set	a.ds_observacao = ds_obs_p
where	a.nr_seq_caixa_rec = nr_seq_caixa_p
and	a.nr_adiantamento = nr_adiantamento_p;

commit;

end retorno_ISAT_update_adiant;
/