create or replace procedure
enviar_ci_gestor_conta (
	nr_seq_ordem_serv_p	number,
	nm_usuario_p		varchar2
) is

ds_macro_w		varchar2(255 char);
ds_titulo_w		comunic_interna.ds_titulo%type;
ds_comunic_w		comunic_interna.ds_comunicado%type;
nm_usuario_gestor_w	comunic_interna.nm_usuario_destino%type;
nr_seq_cliente_W	com_cliente.nr_sequencia%type;
nr_seq_rtf_srtring_w	number(10);
ds_historico_os_w	tasy_conversao_rtf.ds_texto%type;

begin

select	obter_usuario_gestor_conta(nr_seq_ordem_serv_p, 'S')
into	nm_usuario_gestor_w
from	dual;

converte_rtf_string('select ds_relat_tecnico from man_ordem_serv_tecnico where nr_seq_ordem_serv = :nr_sequencia_p', nr_seq_ordem_serv_p, nm_usuario_p, nr_seq_rtf_srtring_w);

ds_macro_w	:= 'NR_SEQ_ORDEM=' || nr_seq_ordem_serv_p;

select	nvl(dbms_lob.substr(ds_texto_clob, 4000, 1), obter_desc_exp_idioma(950391, wheb_usuario_pck.get_nr_seq_idioma, ds_macro_w))
into	ds_historico_os_w
from	tasy_conversao_rtf
where	nr_sequencia = nr_seq_rtf_srtring_w;

ds_titulo_w	:= obter_desc_exp_idioma(100329035, wheb_usuario_pck.get_nr_seq_idioma, ds_macro_w);
ds_comunic_w	:= substr(obter_desc_exp_idioma(100329037, wheb_usuario_pck.get_nr_seq_idioma, ds_macro_w)
			|| chr(10) || chr(13) || chr(10) || chr(13) ||
			obter_desc_expressao(508099)/*'�ltimo hist�rico: '*/
			|| chr(10) || chr(13) || ds_historico_os_w,1,4000);


	gerar_comunic_padrao ( sysdate, ds_titulo_w, ds_comunic_w,
			nm_usuario_p, 'N', nm_usuario_gestor_w || ',',
			'N', 5, null,
			null, null, sysdate,
			null, null);

end enviar_ci_gestor_conta;
/
