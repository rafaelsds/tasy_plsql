create or replace PROCEDURE validate_batch_hcp (nr_seq_batch_p NUMBER, 
                                                   nm_usuario_p   VARCHAR2,
                                                   nr_atendimento_p NUMBER) 
IS 
  qt_erro_w    NUMBER(10); 
  qt_alerta_w  NUMBER(10); 
  qt_arquivo_w NUMBER(10); 
  CURSOR c00 IS 
    SELECT a.nr_sequencia 
    FROM   hcp_files a 
    WHERE  a.nr_file_batch_seq = nr_seq_batch_p; 
  c00_w        c00%ROWTYPE; 
  CURSOR c01 IS 
    SELECT a.nr_sequencia, a.nr_atendimento,
           a.ie_dataset 
    FROM   hcp_dataset_send a 
    WHERE  a.nr_seq_file = c00_w.nr_sequencia; 
  c01_w        c01%ROWTYPE; 
BEGIN 
    UPDATE hcp_batch_sending 
    SET    dt_intial_consistency = SYSDATE 
    WHERE  nr_sequencia = nr_seq_batch_p; 
              
    OPEN c00; 
    LOOP 
        FETCH c00 INTO c00_w;  
        exit WHEN c00%NOTFOUND; 
        OPEN c01; 
        LOOP 
            FETCH c01 INTO c01_w; 

            exit WHEN c01%NOTFOUND; 
    Validate_dataset_hcp(c01_w.nr_sequencia, c01_w.ie_dataset, nm_usuario_p, c01_w.nr_atendimento); 
    END LOOP; 
    CLOSE c01; 
    BEGIN 
    SELECT 1 
    INTO   qt_erro_w 
    FROM   hcp_dataset_send 
    WHERE  nr_seq_file = c00_w.nr_sequencia 
           AND ie_validation_status = 'E' 
           AND ROWNUM = 1; 
    EXCEPTION 
    WHEN OTHERS THEN 
      qt_erro_w := 0; 
    END; 
	
    BEGIN 
    SELECT 1 
    INTO   qt_alerta_w 
    FROM   hcp_dataset_send 
    WHERE  nr_seq_file = c00_w.nr_sequencia 
           AND ie_validation_status = 'A' 
           AND ROWNUM = 1; 
    EXCEPTION 
    WHEN OTHERS THEN 
      qt_alerta_w := 0; 
    END; 
	
    IF ( qt_erro_w > 0 ) THEN 
    UPDATE hcp_files 
    SET    ie_validity_status = 'E' 
    WHERE  nr_sequencia = c00_w.nr_sequencia; 
    ELSIF ( qt_alerta_w > 0 ) THEN 
    UPDATE hcp_files 
    SET    ie_validity_status = 'A' 
    WHERE  nr_sequencia = c00_w.nr_sequencia; 
    ELSE 
    UPDATE hcp_files 
    SET    ie_validity_status = 'V', 
         ie_sending_status = 'P' 
    WHERE  nr_sequencia = c00_w.nr_sequencia; 
    END IF; 
    END LOOP; 
      
    CLOSE c00; 

    qt_alerta_w := 0; 

    qt_erro_w := 0; 

    BEGIN 
        SELECT 1 
        INTO   qt_arquivo_w 
        FROM   hcp_files 
        WHERE  nr_file_batch_seq = nr_seq_batch_p 
               AND ROWNUM = 1; 
    EXCEPTION 
        WHEN OTHERS THEN 
          qt_arquivo_w := 0; 
    END; 

    BEGIN 
        SELECT 1 
        INTO   qt_erro_w 
        FROM   hcp_files 
        WHERE  nr_file_batch_seq = nr_seq_batch_p 
               AND ie_validity_status = 'E' 
               AND ROWNUM = 1; 
    EXCEPTION 
        WHEN OTHERS THEN 
          qt_erro_w := 0; 
    END; 

    BEGIN 
        SELECT 1 
        INTO   qt_alerta_w 
        FROM   hcp_files 
        WHERE  nr_file_batch_seq = nr_seq_batch_p 
               AND ie_validity_status = 'A' 
               AND ROWNUM = 1; 
    EXCEPTION 
        WHEN OTHERS THEN 
          qt_alerta_w := 0; 
    END; 

    IF ( qt_erro_w > 0 ) THEN 
      UPDATE hcp_batch_sending 
      SET    ie_validation_status = 'E' 
      WHERE  nr_sequencia = nr_seq_batch_p; 
    ELSIF ( qt_alerta_w > 0 ) THEN 
      UPDATE hcp_batch_sending 
      SET    ie_validation_status = 'A' 
      WHERE  nr_sequencia = nr_seq_batch_p; 
    ELSIF ( qt_arquivo_w > 0 ) THEN 
      UPDATE hcp_batch_sending 
      SET    ie_validation_status = 'V', 
             ie_sending_status = 'P' 
      WHERE  nr_sequencia = nr_seq_batch_p; 
    ELSE 
      UPDATE hcp_batch_sending 
      SET    ie_validation_status = 'P' 
      WHERE  nr_sequencia = nr_seq_batch_p; 
    END IF; 

    UPDATE hcp_batch_sending 
    SET    dt_final_consistency = SYSDATE 
    WHERE  nr_sequencia = nr_seq_batch_p; 

   
END validate_batch_hcp;
/