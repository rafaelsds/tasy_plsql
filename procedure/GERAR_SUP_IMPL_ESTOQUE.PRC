create or replace
procedure gerar_sup_impl_estoque(
		ie_tipo_virada_p		number,
		ie_deleta_p		varchar2,
		ie_custo_medio_p		Number,
		cd_estabelecimento_p	Number,
		cd_local_estoque_p	Number,
		cd_centro_custo_p		Number,
		cd_material_planilha_p	varchar2,
		cd_fornecedor_p		varchar2,
		qt_estoque_p		Number,
		vl_custo_medio_p		Number,
		vl_ultima_compra_p		Number,
		ds_lote_fornec_p	varchar2,
		ds_barras_p		varchar2,
		dt_validade_p		date,
		dt_movimento_estoque_p	date) as


/*ie_tipo_virada_p
0	Saldo normal
2	CM
1	Saldo consignado
5	Consumos
6	Lote fornecedor*/


nr_sequencia_w		number(5);
vl_custo_medio_w	number(13,4);

begin


if	(ie_deleta_p = 'S') then
	begin
	delete from sup_impl_estoque;
	delete from sup_impl_consistencia;
	commit;
	end;
end if;

if	(nvl(qt_estoque_p,0) > 0) or (ie_tipo_virada_p = 2) then
	begin
	
	if	(ie_custo_medio_p = 0) then
		vl_custo_medio_w	:= nvl(vl_custo_medio_p, 0);

	elsif	(ie_custo_medio_p = 1) then

		select	nvl(max(vl_custo_medio), 0)
		into	vl_custo_medio_w
		from	sup_impl_cm
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_material_planilha	= cd_material_planilha_p;

	elsif	(ie_custo_medio_p = 2) then

		vl_custo_medio_w	:= nvl(vl_custo_medio_p, 0);
		if	(vl_custo_medio_w = 0) then
			select	nvl(max(vl_custo_medio), 0)
			into	vl_custo_medio_w
			from	sup_impl_cm
			where	cd_estabelecimento	= cd_estabelecimento_p
			and	cd_material_planilha	= cd_material_planilha_p;
		end if;

	elsif	(ie_custo_medio_p = 3) then

		select	nvl(max(vl_custo_medio), 0)
		into	vl_custo_medio_w
		from	sup_impl_cm
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_material_planilha	= cd_material_planilha_p;

		if	(vl_custo_medio_w = 0) then
			vl_custo_medio_w	:= nvl(vl_custo_medio_p, 0);
		end if;
	end if;

	select	nvl(max(nr_sequencia), 0) + 1
	into	nr_sequencia_w
	from	sup_impl_estoque;

	insert into sup_impl_estoque(
		nr_sequencia,
		cd_estabelecimento,
		cd_local_estoque,
		cd_centro_custo,
		cd_material_planilha,
		cd_fornecedor,
		qt_estoque,
		vl_custo_medio,
		vl_ultima_compra,
		ds_lote_fornec,
		dt_validade,
		ds_barras,
		dt_movimento_estoque)
	values( nr_sequencia_w,
		cd_estabelecimento_p,
		cd_local_estoque_p,
		cd_centro_custo_p,
		cd_material_planilha_p,
		substr(cd_fornecedor_p,1,14),
		qt_estoque_p,
		vl_custo_medio_w,
		vl_ultima_compra_p,
		substr(trim(ds_lote_fornec_p),1,20),
		dt_validade_p,
		trim(ds_barras_p),
		dt_movimento_estoque_p);

	end;
end if;

commit;

end gerar_sup_impl_estoque;
/