create or replace
procedure Atualiza_Vinculo_Mat_TUSS(	Nr_Seq_Carga_Novo_p			Number,
										Nr_Seq_Carga_Atual_p		Number,
										Nr_Seq_Material_TUSS_p		Number,
										Dt_Inicio_Vigencia_p		Date,
										Nm_usuario_p			Varchar2) is 

Cursor C01 Is
		Select  cd_material,
				cd_convenio,
				cd_categoria,
				ie_tipo_atendimento,
				ie_origem_preco,                
				nr_seq_tuss_mat,               
				nr_seq_tuss_mat_item,
				cd_material_tuss,
				cd_estabelecimento,
				nr_seq_marca,
				cd_cgc_fornecedor,
				ie_situacao,
				nr_sequencia
		From	material_tuss 
		Where	ie_situacao = 'A'
		And		nr_seq_tuss_mat = Nr_Seq_Carga_Atual_p
		And		nr_seq_tuss_mat_item = Nr_Seq_Material_TUSS_p
		And		dt_vigencia_final Is Null;

nr_seq_tuss_mat_item_w	tuss_material_item.nr_sequencia%Type;
		
begin

For	r1 In C01 Loop

	
	Select	Max(Nr_Sequencia)
	Into	nr_seq_tuss_mat_item_w
	From 	tuss_material_item b
	Where	b.Nr_Seq_Carga_Tuss = Nr_Seq_Carga_Novo_p
	And		Cd_Material_Tuss = r1.Cd_Material_Tuss;
	
	If Nvl(nr_seq_tuss_mat_item_w,0) > 0 Then
		Insert Into material_tuss (	nr_sequencia,
									cd_material,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									cd_convenio,
									cd_categoria,
									ie_tipo_atendimento,
									dt_vigencia_inicial,
									ie_origem_preco,
									nr_seq_tuss_mat,
									nr_seq_tuss_mat_item,
									cd_material_tuss,
									ie_situacao,
									cd_estabelecimento,
									nr_seq_marca,
									cd_cgc_fornecedor)
						Values	(	material_tuss_seq.nextval,
									r1.cd_material,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									r1.cd_convenio,
									r1.cd_categoria,
									r1.ie_tipo_atendimento,
									Dt_Inicio_Vigencia_p,
									r1.ie_origem_preco,
									Nr_Seq_Carga_Novo_p,
									nr_seq_tuss_mat_item_w,
									r1.cd_material_tuss,
									r1.ie_situacao,
									r1.cd_estabelecimento,
									r1.nr_seq_marca,
									r1.cd_cgc_fornecedor);
		Update	material_tuss
		Set		dt_vigencia_final = Dt_Inicio_Vigencia_p - 1
		Where	nr_Sequencia = r1.nr_sequencia
		And		dt_vigencia_final Is Null;
	End If;	
End Loop;

Commit;

end Atualiza_Vinculo_Mat_TUSS;
/	