CREATE OR REPLACE PROCEDURE GENERATE_EVO_PACIENTE_REQ_TIE(CD_EVOLUCAO_P   NUMBER,
                                                          IE_EVENT_TYPE_P VARCHAR2) IS
  /*
    - IE_EVENT_TYPE_P
    A - TIE event name: clinical.notes.import
    I - TIE event name: clinical.notes.inactive
  */

  TIE_EVENT_NAME_W VARCHAR2(255);
  DS_JSON_W        CLOB;
  DS_EVOLUCAO_W    CLOB;
  NM_USUARIO_W     EVOLUCAO_PACIENTE.NM_USUARIO%TYPE;
BEGIN
  IF (NVL(CD_EVOLUCAO_P, 0) > 0 AND UPPER(IE_EVENT_TYPE_P) IN ('A', 'I')) THEN
    IF (UPPER(IE_EVENT_TYPE_P) = 'A') THEN
      TIE_EVENT_NAME_W := 'clinical.notes.import';
    ELSIF (UPPER(IE_EVENT_TYPE_P) = 'I') THEN
      TIE_EVENT_NAME_W := 'clinical.notes.inactive';
    END IF;
  
    SELECT CONVERT_LONG_TO_CLOB('EVOLUCAO_PACIENTE',
                                'DS_EVOLUCAO',
                                'CD_EVOLUCAO = ' || EP.CD_EVOLUCAO) DS_EVOLUCAO_CLOB,
           EP.NM_USUARIO
      INTO DS_EVOLUCAO_W, NM_USUARIO_W
      FROM EVOLUCAO_PACIENTE EP
     WHERE EP.CD_EVOLUCAO = CD_EVOLUCAO_P;
  
    IF (UPPER(DS_EVOLUCAO_W) LIKE '%HTML%') THEN
      DS_JSON_W := BIFROST.SEND_INTEGRATION(TIE_EVENT_NAME_W,
                                            'com.philips.tasy.integration.telehealth.clinicalnotes.callback.ClinicalNotesCallback',
                                            '{"id": ' || CD_EVOLUCAO_P || ', "eventType" : "' || IE_EVENT_TYPE_P || '"}',
                                            NM_USUARIO_W);
    END IF;
  END IF;
END GENERATE_EVO_PACIENTE_REQ_TIE;
/
