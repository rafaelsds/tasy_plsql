create or replace
procedure ctb_pls_atualizar_aviso_cobr
			(	nr_seq_lote_p			number,
				nr_seq_arquivo_p		number,
				nr_seq_aviso_proc_p		number,
				nr_seq_aviso_mat_p		number,
				nr_seq_aviso_proc_item_p	number,
				nr_seq_aviso_mat_item_p		number,
				nr_seq_lote_faturamento_p	number,
				nr_seq_ptu_fatura_p		number,
				nr_seq_nota_servico_p		number,
				nr_seq_atualizacao_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				qt_movimento_p		in out	number) is 

qt_movimento_w			number(10)	:= 0;
i				number(10);
qt_registros_w 			number(10);
qt_cooperado_w			number(10);
ie_tipo_contrato_w		varchar2(2);
ie_classif_grupo_w		varchar2(5);
ie_classif_grupo_ww		varchar2(5);
cd_classificacao_item_w		varchar2(30);
dt_mesano_ref_w			date;
cd_empresa_w			empresa.cd_empresa%type;
nr_seq_esquema_w		pls_esquema_contabil.nr_sequencia%type;
cd_historico_padrao_w		pls_esquema_contabil.cd_historico_padrao%type;
nr_seq_esquema_ww		pls_esquema_contabil.nr_sequencia%type;
cd_historico_padrao_ww		pls_esquema_contabil.cd_historico_padrao%type;
cd_conta_credito_w		conta_contabil.cd_conta_contabil%type;
cd_conta_debito_w		conta_contabil.cd_conta_contabil%type;
cd_classificacao_credito_w	conta_contabil.cd_classificacao%type;
cd_classificacao_debito_w	conta_contabil.cd_classificacao%type;
nr_seq_grupo_ans_w		ans_grupo_despesa.nr_seq_grupo_superior%type;
nr_seq_grupo_superior_w		ans_grupo_despesa.nr_seq_grupo_superior%type;
ie_grupo_despesa_ans_w		ans_grupo_despesa.ie_tipo_grupo_ans%type;
ie_tipo_repasse_w		pls_segurado_repasse.ie_tipo_repasse%type;
ie_tipo_prestador_w		pls_tipo_prestador.ie_tipo_ptu%type;
ie_codificacao_w		pls_esquema_contabil_seg.ie_codificacao%type;
vl_fixo_w			pls_esquema_contabil_seg.vl_fixo%type;
cd_conta_contabil_w		pls_esquema_contabil_seg.cd_conta_contabil%type;
ie_debito_credito_w		pls_esquema_contabil_seg.ie_debito_credito%type;
ds_mascara_w			pls_esquema_contabil_seg.ds_mascara%type;
cd_cgc_w			pls_congenere.cd_cgc%type;
cd_pessoa_fisica_prest_w	pls_prestador.cd_pessoa_fisica%type;
cd_cgc_prest_w			pls_prestador.cd_cgc%type;
ie_tipo_relacao_w		pls_prestador.ie_tipo_relacao%type;
nr_seq_tipo_prestador_w		pls_prestador.nr_seq_tipo_prestador%type;
ie_prestador_codificacao_w	pls_esquema_contabil.ie_prestador_codificacao%type;
ie_tipo_compartilhamento_w	pls_esquema_contabil.ie_tipo_compartilhamento%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
dt_repasse_w			date;
dt_fim_repasse_w		date;

cursor c_itens is
	select	'A500' ie_tipo_processo,
		'E' ie_tipo_movimento,
		'P' ie_proc_mat,
		a.rowid nr_id,
		pp.nr_sequencia nr_seq_conta_proc,
		null nr_seq_conta_mat,
		pp.nr_seq_grupo_ans nr_seq_grupo_ans,
		pp.ie_ato_cooperado ie_ato_cooperado,
		null nr_seq_aviso_proc,
		null nr_seq_aviso_mat,
		null nr_seq_aviso_proc_item,
		null nr_seq_aviso_mat_item,
		p.ie_tipo_contratacao ie_tipo_contratacao,
		p.ie_preco ie_preco,
		p.ie_segmentacao ie_segmentacao,
		p.ie_regulamentacao ie_regulamentacao,
		g.dt_mesano_referencia dt_referencia,
		nvl(nvl(c.ie_tipo_segurado,s.ie_tipo_segurado),'B') ie_tipo_segurado,
		s.ie_tipo_vinculo_operadora ie_tipo_vinculo_operadora,
		s.nr_seq_contrato nr_seq_contrato,
		s.nr_seq_intercambio nr_seq_intercambio,
		s.nr_sequencia nr_seq_segurado,
		obter_estabelecimento_base(s.cd_pessoa_fisica) cd_estabelecimento_setor,
		decode(ie_prestador_codificacao_w,'E',c.nr_seq_prestador_exec,'S',c.nr_seq_prestador,t.nr_seq_prestador) nr_seq_prestador,
		nvl(s.ie_pcmso,'N') ie_pcmso,
		t.nr_seq_congenere nr_seq_congenere,
		pls_obter_se_benef_remido(s.nr_sequencia,g.dt_mesano_referencia) ie_benef_remido,
		pp.dt_procedimento dt_ref_repasse,
		p.nr_seq_classificacao nr_seq_classif_sca,
		null cd_usuario_plano
	from 	pls_conta c,
		pls_segurado s,
		pls_plano p,
		pls_protocolo_conta t,
		pls_conta_proc pp,
		ptu_nota_servico a,
		ptu_nota_cobranca b,
		ptu_fatura f,
		pls_lote_faturamento g,
		pls_fatura h
	where	a.nr_seq_nota_cobr 		= b.nr_sequencia
	and	b.nr_seq_fatura 		= f.nr_sequencia
	and	pp.nr_seq_conta 		= c.nr_sequencia
	and	c.nr_seq_segurado 		= s.nr_sequencia
	and	s.nr_seq_plano 			= p.nr_sequencia
	and	pp.nr_sequencia 		= a.nr_seq_conta_proc
	and	f.nr_seq_pls_fatura		= h.nr_sequencia
	and	h.nr_seq_lote			= g.nr_sequencia
	and	t.nr_sequencia			= c.nr_seq_protocolo
	and	f.ie_tipo_cobranca_fatura 	= 'A'
	and	f.ie_operacao			= 'E'
	and	(
			a.nr_sequencia 		= nr_seq_nota_servico_p
			or
			nr_seq_nota_servico_p 	is null
		)
	and	g.nr_sequencia 			= nr_seq_lote_faturamento_p
	and	nvl(a.nr_lote_contabil, 0)	= 0
	union all
	select	'A500' ie_tipo_processo,
		'E' ie_tipo_movimento,
		'M' ie_proc_mat,
		a.rowid nr_id,
		null nr_seq_conta_proc,
		mm.nr_sequencia nr_seq_conta_mat,
		mm.nr_seq_grupo_ans nr_seq_grupo_ans,
		mm.ie_ato_cooperado ie_ato_cooperado,
		null nr_seq_aviso_proc,
		null nr_seq_aviso_mat,
		null nr_seq_aviso_proc_item,
		null nr_seq_aviso_mat_item,
		p.ie_tipo_contratacao ie_tipo_contratacao,
		p.ie_preco ie_preco,
		p.ie_segmentacao ie_segmentacao,
		p.ie_regulamentacao ie_regulamentacao,
		g.dt_mesano_referencia dt_referencia,
		nvl(nvl(c.ie_tipo_segurado,s.ie_tipo_segurado),'B') ie_tipo_segurado,
		s.ie_tipo_vinculo_operadora ie_tipo_vinculo_operadora,
		s.nr_seq_contrato nr_seq_contrato,
		s.nr_seq_intercambio nr_seq_intercambio,
		s.nr_sequencia nr_seq_segurado,
		obter_estabelecimento_base(s.cd_pessoa_fisica) cd_estabelecimento_setor,
		decode(ie_prestador_codificacao_w,'E',c.nr_seq_prestador_exec,'S',c.nr_seq_prestador,t.nr_seq_prestador) nr_seq_prestador,
		nvl(s.ie_pcmso,'N') ie_pcmso,
		t.nr_seq_congenere nr_seq_congenere,
		pls_obter_se_benef_remido(s.nr_sequencia,g.dt_mesano_referencia) ie_benef_remido,
		mm.dt_atendimento dt_ref_repasse,
		p.nr_seq_classificacao nr_seq_classif_sca,
		null cd_usuario_plano
	from 	pls_conta c,
		pls_segurado s,
		pls_plano p,
		pls_protocolo_conta t,
		pls_conta_mat mm,
		ptu_nota_servico a,
		ptu_nota_cobranca b,
		ptu_fatura f,
		pls_lote_faturamento g,
		pls_fatura h
	where	a.nr_seq_nota_cobr 		= b.nr_sequencia
	and	b.nr_seq_fatura 		= f.nr_sequencia
	and	mm.nr_seq_conta 		= c.nr_sequencia
	and	c.nr_seq_segurado 		= s.nr_sequencia
	and	s.nr_seq_plano 			= p.nr_sequencia
	and	mm.nr_sequencia 		= a.nr_seq_conta_mat
	and	f.nr_seq_pls_fatura		= h.nr_sequencia
	and	h.nr_seq_lote			= g.nr_sequencia
	and	t.nr_sequencia			= c.nr_seq_protocolo
	and	f.ie_tipo_cobranca_fatura 	= 'A'
	and	f.ie_operacao			= 'E'
	and	(
			a.nr_sequencia 		= nr_seq_nota_servico_p
			or
			nr_seq_nota_servico_p 	is null
		)
	and	g.nr_sequencia 			= nr_seq_lote_faturamento_p
	and	nvl(a.nr_lote_contabil, 0)	= 0
	union all
	select	'A500' ie_tipo_processo,
		'R' ie_tipo_movimento,
		'P' ie_proc_mat,
		a.rowid nr_id,
		null nr_seq_conta_proc,
		null nr_seq_conta_mat,
		null nr_seq_grupo_ans,
		null ie_ato_cooperado,
		null nr_seq_aviso_proc,
		null nr_seq_aviso_mat,
		null nr_seq_aviso_proc_item,
		null nr_seq_aviso_mat_item,
		null ie_tipo_contratacao,
		null ie_preco,
		null ie_segmentacao,
		null ie_regulamentacao,
		f.dt_mes_competencia dt_referencia,
		null ie_tipo_segurado,
		null ie_tipo_vinculo_operadora,
		null nr_seq_contrato,
		null nr_seq_intercambio,
		null nr_seq_segurado,
		null cd_estabelecimento_setor,
		null nr_seq_prestador,
		null ie_pcmso,
		null nr_seq_congenere,
		null ie_benef_remido,
		a.dt_procedimento dt_ref_repasse,
		null nr_seq_classif_sca,
		b.cd_usuario_plano cd_usuario_plano
	from 	ptu_nota_servico a,
		ptu_nota_cobranca b,
		ptu_fatura f
	where	a.nr_seq_nota_cobr 		= b.nr_sequencia
	and	b.nr_seq_fatura 		= f.nr_sequencia
	and	f.ie_tipo_cobranca_fatura 	= 'A'
	and	f.ie_operacao			= 'R'
	and	(
			a.nr_sequencia 		= nr_seq_nota_servico_p
			or
			nr_seq_nota_servico_p 	is null
		)
	and	f.nr_sequencia 			= nr_seq_ptu_fatura_p
	and	nvl(a.nr_lote_contabil, 0)	= 0
	union all
	select	'A520' ie_tipo_processo,
		'E' ie_tipo_movimento,
		x.ie_proc_mat,
		x.nr_id,		
		x.nr_seq_conta_proc,
		x.nr_seq_conta_mat,
		x.nr_seq_grupo_ans,
		x.ie_ato_cooperado,
		x.nr_seq_aviso_proc,
		x.nr_seq_aviso_mat,
		x.nr_seq_aviso_proc_item,
		x.nr_seq_aviso_mat_item,
		d.ie_tipo_contratacao,
		d.ie_preco,
		d.ie_segmentacao,
		d.ie_regulamentacao,
		a.dt_competencia dt_referencia,
		nvl(nvl(c.ie_tipo_segurado,s.ie_tipo_segurado),'B') ie_tipo_segurado,
		s.ie_tipo_vinculo_operadora,
		s.nr_seq_contrato,
		s.nr_seq_intercambio,
		s.nr_sequencia nr_seq_segurado,
		obter_estabelecimento_base(s.cd_pessoa_fisica) cd_estabelecimento_setor,
		decode(ie_prestador_codificacao_w,'E',c.nr_seq_prestador_exec,'S',c.nr_seq_prestador,p.nr_seq_prestador) nr_seq_prestador,
		nvl(s.ie_pcmso,'N') ie_pcmso,
		p.nr_seq_congenere,
		pls_obter_se_benef_remido(s.nr_sequencia,l.dt_referencia_inicio) ie_benef_remido,
		nvl(x.dt_ref_repasse, a.dt_competencia) dt_ref_repasse,
		d.nr_seq_classificacao nr_seq_classif_sca,
		null cd_usuario_plano
	from	(select	'P' ie_proc_mat,
			i.rowid nr_id,
			b.nr_seq_conta_proc,
			null nr_seq_conta_mat,
			b.nr_seq_aviso_conta,
			(select	max(cp.nr_seq_grupo_ans)
			from	pls_conta_proc	cp
			where	cp.nr_sequencia  = b.nr_seq_conta_proc) nr_seq_grupo_ans,
			(select	max(cp.ie_ato_cooperado)
			from	pls_conta_proc	cp
			where	cp.nr_sequencia	= b.nr_seq_conta_proc) ie_ato_cooperado,
			b.nr_sequencia nr_seq_aviso_proc,
			null nr_seq_aviso_mat,
			i.nr_sequencia nr_seq_aviso_proc_item,
			null nr_seq_aviso_mat_item,
			(select y.dt_procedimento
			from    pls_conta_proc y
			where   b.nr_seq_conta_proc = y.nr_sequencia) dt_ref_repasse
		from	ptu_aviso_procedimento	b,
			ptu_aviso_proc_item	i
		where	b.nr_sequencia		= i.nr_seq_aviso_procedimento
		and	((i.nr_sequencia = nr_seq_aviso_proc_item_p) or (nr_seq_aviso_proc_item_p is null))
		union all
		select	'M' ie_proc_mat,
			i.rowid nr_id,
			null nr_seq_conta_proc,
			b.nr_seq_conta_mat,
			b.nr_seq_aviso_conta,
			(select	max(cm.nr_seq_grupo_ans)
			from	pls_conta_mat	cm
			where	cm.nr_sequencia	= b.nr_seq_conta_mat) nr_seq_grupo_ans,
			(select	max(cm.ie_ato_cooperado)
			from	pls_conta_proc	cm
			where	cm.nr_sequencia	= b.nr_seq_conta_mat) ie_ato_cooperado,
			null nr_seq_aviso_proc,
			b.nr_sequencia nr_seq_aviso_mat,
			null nr_seq_aviso_proc_item,
			i.nr_sequencia nr_seq_aviso_mat_item,
			(select  y.dt_atendimento
			from    pls_conta_mat y
			where   b.nr_seq_conta_mat = y.nr_sequencia) dt_ref_repasse
		from	ptu_aviso_material	b,
			ptu_aviso_mat_item	i
		where	b.nr_sequencia		= i.nr_seq_aviso_material
		and	((i.nr_sequencia = nr_seq_aviso_mat_item_p) or (nr_seq_aviso_mat_item_p is null))) x,
		ptu_lote_aviso		l,
		ptu_aviso_arquivo	a,
		ptu_aviso_protocolo	ap,
		ptu_aviso_conta		ac,
		pls_protocolo_conta	p,
		pls_conta 		c,
		pls_segurado		s,
		pls_plano 		d
	where	l.nr_sequencia		= a.nr_seq_lote
	and	a.nr_sequencia		= ap.nr_seq_arquivo
	and	ap.nr_sequencia		= ac.nr_seq_aviso_protocolo
	and	ac.nr_sequencia		= x.nr_seq_aviso_conta
	and	p.nr_sequencia		= ap.nr_seq_protocolo
	and	c.nr_sequencia		= ac.nr_seq_conta
	and	c.nr_seq_segurado	= s.nr_sequencia(+)
	and	c.nr_seq_plano		= d.nr_sequencia(+)
	and	l.ie_tipo_lote		= 'E'
	and	l.nr_sequencia		= nr_seq_lote_p
	and	a.nr_sequencia		= nr_seq_arquivo_p
	union all
	select	'A520' ie_tipo_processo,
		'R' ie_tipo_movimento,
		x.ie_proc_mat,
		x.nr_id,
		null nr_seq_conta_proc,
		null nr_seq_conta_mat,
		x.nr_seq_grupo_ans,
		x.ie_ato_cooperado,
		x.nr_seq_aviso_proc,
		x.nr_seq_aviso_mat,
		null nr_seq_aviso_proc_item,
		null nr_seq_aviso_mat_item,
		d.ie_tipo_contratacao,
		d.ie_preco,
		d.ie_segmentacao,
		d.ie_regulamentacao,
		a.dt_transacao dt_referencia,
		nvl(s.ie_tipo_segurado,'B') ie_tipo_segurado,
		null ie_tipo_vinculo_operadora,
		null nr_seq_contrato,
		null nr_seq_intercambio,
		s.nr_sequencia nr_seq_segurado,
		null cd_estabelecimento_setor,
		null nr_seq_prestador,
		null ie_pcmso,
		null nr_seq_congenere,
		pls_obter_se_benef_remido(s.nr_sequencia,l.dt_referencia_inicio) ie_benef_remido,
		nvl(x.dt_execucao, a.dt_transacao) dt_ref_repasse,
		d.nr_seq_classificacao nr_seq_classif_sca,
		null cd_usuario_plano
	from	(select	'P' ie_proc_mat,
			b.rowid nr_id,
			b.nr_seq_aviso_conta,
			b.nr_sequencia nr_seq_aviso_proc,
			null nr_seq_aviso_mat,
			b.dt_execucao,
			b.ie_ato_cooperado,
			b.nr_seq_grupo_ans
		from	ptu_aviso_procedimento	b
		where	((b.nr_sequencia = nr_seq_aviso_proc_p) or (nr_seq_aviso_proc_p is null))
		union all
		select	'M' ie_proc_mat,
			b.rowid nr_id,
			b.nr_seq_aviso_conta,
			null nr_seq_aviso_proc,
			b.nr_sequencia nr_seq_aviso_mat,
			b.dt_execucao,
			b.ie_ato_cooperado,
			b.nr_seq_grupo_ans
		from	ptu_aviso_material	b
		where	((b.nr_sequencia = nr_seq_aviso_mat_p) or (nr_seq_aviso_mat_p is null))) x,
		ptu_lote_aviso		l,
		ptu_aviso_arquivo	a,
		ptu_aviso_protocolo	ap,
		ptu_aviso_conta		ac,
		pls_segurado		s,
		pls_plano		d
	where	l.nr_sequencia		= a.nr_seq_lote
	and	a.nr_sequencia		= ap.nr_seq_arquivo
	and	ap.nr_sequencia		= ac.nr_seq_aviso_protocolo
	and	ac.nr_sequencia		= x.nr_seq_aviso_conta
	and	ac.nr_seq_segurado	= s.nr_sequencia(+)
	and	s.nr_seq_plano		= d.nr_sequencia(+)
	and	l.ie_tipo_lote		= 'R'
	and	l.nr_sequencia		= nr_seq_lote_p
	and	a.nr_sequencia		= nr_seq_arquivo_p;

c_itens_w	c_itens%rowtype;

cursor c_esquema is
	select	nr_sequencia,
		cd_historico_padrao
	from	pls_esquema_contabil
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_regra		= 'AC'
	and	ie_tipo_movimentacao	= c_itens_w.ie_tipo_movimento
	and	c_itens_w.dt_referencia between dt_inicio_vigencia and nvl(dt_fim_vigencia,c_itens_w.dt_referencia)
	and	((ie_tipo_segurado = c_itens_w.ie_tipo_segurado) or (ie_tipo_segurado is null))
	and	((nr_seq_prestador = c_itens_w.nr_seq_prestador) or (nr_seq_prestador is null))
	and	((nr_seq_tipo_prestador = nr_seq_tipo_prestador_w) or (nr_seq_tipo_prestador is null))
	and	((ie_tipo_relacao = ie_tipo_relacao_w) or (ie_tipo_relacao is null))
	and	((nvl(ie_tipo_relacao,'X') <> 'C') or ((ie_tipo_relacao = 'C') and (qt_cooperado_w > 0)))
	and	((nvl(ie_pcmso,'N') = c_itens_w.ie_pcmso) or (nvl(ie_pcmso,'A') = 'A'))
	and	((nr_seq_congenere = c_itens_w.nr_seq_congenere) or (nr_seq_congenere is null))
	and	((nr_seq_contrato = c_itens_w.nr_seq_contrato) or (nr_seq_contrato is null))
	and	((ie_tipo_vinculo_operadora = c_itens_w.ie_tipo_vinculo_operadora) or (ie_tipo_vinculo_operadora is null))
	and	((cd_estab_setor_pessoa = c_itens_w.cd_estabelecimento_setor) or (cd_estab_setor_pessoa is null))
	and	((ie_tipo_repasse = ie_tipo_repasse_w) or (ie_tipo_repasse is null))
	and	((ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (ie_tipo_compartilhamento is null))
	and	((ie_benef_remido	= c_itens_w.ie_benef_remido) or (ie_benef_remido is null))
	and	((nr_seq_classif_sca	= c_itens_w.nr_seq_classif_sca) or (nr_seq_classif_sca is null))
	order by
		nvl(ie_tipo_repasse,' '),
		nvl(ie_pcmso,' '),
		nvl(nr_seq_congenere,0),
		nvl(nr_seq_contrato,0),
		nvl(ie_tipo_vinculo_operadora,' '),
		nvl(cd_estab_setor_pessoa,0),
		nvl(ie_tipo_relacao,' '),
		nvl(nr_seq_tipo_prestador,0),
		nvl(nr_seq_prestador,0),
		nvl(ie_tipo_segurado,' '),
		nvl(ie_tipo_compartilhamento,0),
		nvl(ie_benef_remido,' '),
		nvl(dt_inicio_vigencia,sysdate);

cursor c_segmentacao is
	select	ie_codificacao,
		vl_fixo,
		cd_conta_contabil,
		ie_debito_credito,
		ds_mascara
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema = nr_seq_esquema_w
	order by	ie_debito_credito,
			nr_seq_apresentacao;

begin

qt_movimento_w	:= qt_movimento_p;

select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(pkg_date_utils.start_of(l.dt_geracao, 'MONTH', 0))
into	dt_mesano_ref_w
from	ptu_lote_aviso	l
where	l.nr_sequencia	= nr_seq_lote_p;

select	max(a.ie_prestador_codificacao)
into	ie_prestador_codificacao_w
from	pls_esquema_contabil	a
where	a.ie_tipo_regra		= 'AC'
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	dt_mesano_ref_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_mesano_ref_w);

open c_itens;
loop
fetch c_itens into
	c_itens_w;
exit when c_itens%notfound;
	begin
	nr_seq_esquema_w		:= null;
	cd_conta_credito_w		:= null;
	cd_conta_debito_w		:= null;
	cd_historico_padrao_w		:= null;
	cd_classificacao_credito_w	:= null;
	cd_classificacao_debito_w	:= null;
	nr_seq_grupo_ans_w		:= null;
	nr_seq_grupo_superior_w		:= null;
	
	-- Quando eh recebimento do processo do A500, pode haver mais de um registro vinculado ao cd_usuario_plano, portanto o select eh feito fora do cursor
	if	(c_itens_w.cd_usuario_plano is not null) then

		select	max(nr_seq_segurado)
		into	nr_seq_segurado_w
		from	pls_segurado_carteira
		where	cd_usuario_plano = c_itens_w.cd_usuario_plano;

		if	(nr_seq_segurado_w is not null) then
			begin
			select	s.ie_tipo_segurado	ie_tipo_segurado,
				s.nr_sequencia 		nr_seq_segurado,
				p.ie_tipo_contratacao 	ie_tipo_contratacao,
				p.ie_preco 		ie_preco,
				p.ie_segmentacao 	ie_segmentacao,
				p.ie_regulamentacao 	ie_regulamentacao,
				pls_obter_se_benef_remido(s.nr_sequencia, c_itens_w.dt_referencia) ie_benef_remido,
				p.nr_seq_classificacao nr_seq_classif_sca
			into	c_itens_w.ie_tipo_segurado,
				c_itens_w.nr_seq_segurado,
				c_itens_w.ie_tipo_contratacao,
				c_itens_w.ie_preco,
				c_itens_w.ie_segmentacao,
				c_itens_w.ie_regulamentacao,
				c_itens_w.ie_benef_remido,
				c_itens_w.nr_seq_classif_sca
			from	pls_segurado s,
				pls_plano p
			where	s.nr_sequencia		= nr_seq_segurado_w
			and	p.nr_sequencia(+)	= s.nr_seq_plano;
			exception when no_data_found then
				c_itens_w.ie_tipo_segurado 	:= null;
				c_itens_w.nr_seq_segurado 	:= null;
				c_itens_w.ie_tipo_contratacao 	:= null;
				c_itens_w.ie_preco 		:= null;
				c_itens_w.ie_segmentacao 	:= null;
				c_itens_w.ie_regulamentacao 	:= null;
				c_itens_w.ie_benef_remido 	:= null;
				c_itens_w.nr_seq_classif_sca 	:= null;
			end;
		end if;
	
	end if;
	if	(c_itens_w.nr_seq_prestador is not null) then
		select	ie_tipo_relacao,
			nr_seq_tipo_prestador,
			cd_pessoa_fisica,
			cd_cgc
		into	ie_tipo_relacao_w,
			nr_seq_tipo_prestador_w,
			cd_pessoa_fisica_prest_w,
			cd_cgc_prest_w
		from	pls_prestador
		where	nr_sequencia	= c_itens_w.nr_seq_prestador
		and	rownum		<= 1;

		if	(nr_seq_tipo_prestador_w is not null) then
			select	max(ie_tipo_ptu)
			into	ie_tipo_prestador_w
			from	pls_tipo_prestador
			where	nr_sequencia	= nr_seq_tipo_prestador_w
			and	rownum		<= 1;
		end if;
	end if;
	
	select	count(1)
	into	qt_cooperado_w
	from	pls_cooperado
	where	((cd_cgc = cd_cgc_prest_w) or (cd_cgc_prest_w is null))
	and	((cd_pessoa_fisica = cd_pessoa_fisica_prest_w) or (cd_pessoa_fisica_prest_w is null))
	and	c_itens_w.dt_referencia between nvl(dt_inclusao,c_itens_w.dt_referencia) and nvl(dt_exclusao,c_itens_w.dt_referencia)
	and	rownum		<= 1;

	pls_obter_dados_repasse(	c_itens_w.dt_ref_repasse,
					c_itens_w.nr_seq_segurado,
					c_itens_w.nr_seq_congenere,
					ie_tipo_repasse_w,
					ie_tipo_compartilhamento_w,
					dt_repasse_w,
					dt_fim_repasse_w);

	if	(nvl(c_itens_w.nr_seq_contrato,0) <> 0) then
		select	decode(cd_pf_estipulante, null, 'PJ', 'PF')
		into	ie_tipo_contrato_w
		from	pls_contrato
		where	nr_sequencia	= c_itens_w.nr_seq_contrato
		and	rownum		<= 1;
		
	elsif	(nvl(c_itens_w.nr_seq_intercambio,0) <> 0) then
		select	decode(cd_pessoa_fisica, null, 'PJ', 'PF')
		into	ie_tipo_contrato_w
		from	pls_intercambio
		where	nr_sequencia	= c_itens_w.nr_seq_intercambio
		and	rownum		<= 1;
	end if;
		
	open c_esquema;
	loop
	fetch c_esquema into	
		nr_seq_esquema_ww,
		cd_historico_padrao_ww;
	exit when c_esquema%notfound;
		begin
		nr_seq_esquema_w                 := nr_seq_esquema_ww;
		cd_historico_padrao_w            := cd_historico_padrao_ww;
		
		end;
	end loop;
	close c_esquema;

	open c_segmentacao;
	loop
	fetch c_segmentacao into
		ie_codificacao_w,
		vl_fixo_w,
		cd_conta_contabil_w,
		ie_debito_credito_w,
		ds_mascara_w;
	exit when c_segmentacao%notfound;
		begin
		cd_classificacao_item_w	:= null;

		if	(ie_codificacao_w in ('GA','CG')) then
			if	(nvl(c_itens_w.nr_seq_grupo_ans,0) > 0) then
				select	max(nr_seq_grupo_superior)
				into	nr_seq_grupo_superior_w
				from	ans_grupo_despesa
				where	nr_sequencia	= c_itens_w.nr_seq_grupo_ans;
			end if;

			if	(nvl(nr_seq_grupo_superior_w,0) = 0) then
				nr_seq_grupo_ans_w	:= c_itens_w.nr_seq_grupo_ans;
			else
				nr_seq_grupo_ans_w	:= nr_seq_grupo_superior_w;
			end if;
		
			select	max(ie_tipo_grupo_ans)
			into	ie_grupo_despesa_ans_w
			from	ans_grupo_despesa
			where	nr_sequencia	= nr_seq_grupo_ans_w
			and	rownum		<= 1;

			if	(ie_grupo_despesa_ans_w = 10) then /* 1 - Consultas */
				ie_classif_grupo_w	:= '1';
				ie_classif_grupo_ww	:= '0';
			elsif	(ie_grupo_despesa_ans_w = 20) then /* 49 - Exames */
				ie_classif_grupo_w	:= '2';
				ie_classif_grupo_ww	:= '0';
			elsif	(ie_grupo_despesa_ans_w = 30) then /* 51 - Terapias */
				ie_classif_grupo_w	:= '3';
				ie_classif_grupo_ww	:= '0';
			elsif	(ie_grupo_despesa_ans_w = 41) then /* 7 - Internacao - Honorario medico */
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '1';
			elsif	(ie_grupo_despesa_ans_w = 42) then /* 8 - Internacao - Exames */
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '2';
			elsif	(ie_grupo_despesa_ans_w = 43) then /* 9 - Internacao - Terapias*/
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '3';
			elsif	(ie_grupo_despesa_ans_w = 44) then /* 10 - Internacao - Materiais medicos */
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '4';
			elsif	(ie_grupo_despesa_ans_w = 45) then /* 11 - Internacao - Medicamentos */
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '5';
			elsif	(ie_grupo_despesa_ans_w = 46) then /* 11 - Internacao - Procedimentos com preco fixo */
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '6';
			elsif	(ie_grupo_despesa_ans_w = 49) then /* 12 - Internacao - Outras despesas */
				ie_classif_grupo_w	:= '4';
				ie_classif_grupo_ww	:= '9';
			elsif	(ie_grupo_despesa_ans_w = 50) then /* 6 - Outros atendimentos - Ambulatoriais */
				ie_classif_grupo_w	:= '5';
				ie_classif_grupo_ww	:= '0';
			elsif	(ie_grupo_despesa_ans_w = 60) then /* 16 - Demais despesas assistenciais */
				ie_classif_grupo_w	:= '6';
				ie_classif_grupo_ww	:= '0';
			elsif	(ie_grupo_despesa_ans_w = 70) then /* 70 - Demais despesas assistenciais */
				ie_classif_grupo_w	:= '7';
				ie_classif_grupo_ww	:= '0';
			end if;
		end if;

		if	(ie_debito_credito_w = 'C') then /* Classificacao CREDITO */
			if	(ie_codificacao_w = 'CR') then /* Codigo reduzido */
				select	max(cd_classificacao_atual)
				into	cd_classificacao_credito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;

				cd_conta_credito_w	:= cd_conta_contabil_w;
			elsif	(ie_codificacao_w = 'FX') then /* Fixo */
				cd_classificacao_item_w	:= vl_fixo_w;
			elsif	(ie_codificacao_w = 'FP') then /* Formacao de Preco - Obs: Apenas regra de Envio */
				if	(c_itens_w.ie_preco in ('1','2','3')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_formacao_preco(c_itens_w.ie_preco);
				else
					cd_classificacao_item_w	:= 'FP';
				end if;
			elsif	(ie_codificacao_w = 'R') then /* Regulamentacao - Obs: Apenas regra de Envio */
				cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(c_itens_w.ie_regulamentacao);
			elsif	(ie_codificacao_w = 'TC') then /* Tipo de contratacao - Obs: Apenas regra de Envio */
				if	(c_itens_w.ie_tipo_contratacao in ('I','CE','CA')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_contratacao(c_itens_w.ie_tipo_contratacao);
				else
					cd_classificacao_item_w	:= 'TC';
				end if;
			elsif	(ie_codificacao_w = 'TP') then /* Tipo de Contrato (Pessoa fisica ou Juridica)  -  Obs: Apenas regra de Envio */
				if	(ie_tipo_contrato_w in ('PF','PJ')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
				else
					cd_classificacao_item_w	:= 'TP';
				end if;
			elsif	(ie_codificacao_w = 'S') then /* Segmentacao - Obs: Apenas regra de Envio */
				cd_classificacao_item_w	:= lpad(c_itens_w.ie_segmentacao,2,'0');
			elsif	(ie_codificacao_w = 'TA') then /* Tipo de ato cooperado - Obs: Apenas regra de Envio */
				if	(c_itens_w.ie_ato_cooperado in ('1','2','3')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_ato_cooperado(c_itens_w.ie_ato_cooperado);
				else
					cd_classificacao_item_w	:= 'TA';
				end if;
			elsif	(ie_codificacao_w = 'TR') then /* Tipo de relacao com a OPS */
				cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_relacao(ie_tipo_relacao_w);
			elsif	(ie_codificacao_w = 'RC') then /* Tipo de contratacao / Regulamentacao - Obs: Apenas regra de Envio */
				cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(c_itens_w.ie_tipo_contratacao,c_itens_w.ie_regulamentacao);
			elsif	(ie_codificacao_w = 'GA') then /* Grupo ANS */
				if	(ie_classif_grupo_w is not null) then
					cd_classificacao_item_w	:= ie_classif_grupo_w;
				else
					cd_classificacao_item_w	:= 'GA';
				end if;
			elsif	(ie_codificacao_w = 'CG') then /* Complemento grupo ANS */
				if	(ie_classif_grupo_ww is not null) then
					cd_classificacao_item_w	:= ie_classif_grupo_ww;
				else
					cd_classificacao_item_w	:= 'CG';
				end if;
			elsif	(ie_codificacao_w = 'CP') then /* Conta de pagamento cooperativa origem */
				select	max(a.cd_cgc)
				into	cd_cgc_w
				from	pls_congenere	a
				where	a.cd_cooperativa	= c_itens_w.nr_seq_congenere;
				
				if	(nvl(cd_cgc_w,0) <> 0) then
					select	max(cd_conta_contabil)
					into	cd_conta_contabil_w
					from	pessoa_jur_conta_cont a
					where	a.cd_cgc	= cd_cgc_w
					and	a.cd_empresa	= cd_empresa_w
					and	a.ie_tipo_conta	= 'P'
					and	c_itens_w.dt_referencia between nvl(a.dt_inicio_vigencia,c_itens_w.dt_referencia) and nvl(a.dt_fim_vigencia,c_itens_w.dt_referencia);
				end if;
				
				cd_conta_credito_w	:= cd_conta_contabil_w;
				
				select	max(cd_classificacao_atual)
				into	cd_classificacao_credito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;
			end if;
			
			if	(cd_classificacao_item_w is not null) then
				if	(ds_mascara_w = '00') then
					cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
				elsif	(ds_mascara_w = '0.0') then
					cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
				elsif	(ds_mascara_w = '0_') then
					cd_classificacao_item_w	:= cd_classificacao_item_w;
				else
					cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
				end if;

				cd_classificacao_credito_w	:= cd_classificacao_credito_w || cd_classificacao_item_w;
			end if;
		elsif	(ie_debito_credito_w = 'D') then /* Classificacao DEBITO */
			if	(ie_codificacao_w = 'CR') then /* Codigo reduzido */
				select	max(cd_classificacao_atual)
				into	cd_classificacao_debito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;

				cd_conta_debito_w	:= cd_conta_contabil_w;
			elsif	(ie_codificacao_w = 'FX') then /* Fixo */
				cd_classificacao_item_w	:= vl_fixo_w;
			elsif	(ie_codificacao_w = 'FP') then /* Formacao de Preco - Obs: Apenas regra de Envio */
				if	(c_itens_w.ie_preco in ('1','2','3')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_formacao_preco(c_itens_w.ie_preco);
				else
					cd_classificacao_item_w	:= 'FP';
				end if;
			elsif	(ie_codificacao_w = 'R') then /* Regulamentacao - Obs: Apenas regra de Envio */
				cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_regulamentacao(c_itens_w.ie_regulamentacao);
			elsif	(ie_codificacao_w = 'TC') then /* Tipo de contratacao - Obs: Apenas regra de Envio */
				if	(c_itens_w.ie_tipo_contratacao in ('I','CE','CA')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_contratacao(c_itens_w.ie_tipo_contratacao);
				else
					cd_classificacao_item_w	:= 'TC';
				end if;
			elsif	(ie_codificacao_w = 'S') then /* Segmentacao - Obs: Apenas regra de Envio */
				cd_classificacao_item_w	:= lpad(c_itens_w.ie_segmentacao,2,'0');
			elsif	(ie_codificacao_w = 'TP') then /* Tipo de Contrato (Pessoa fisica ou Juridica)  -  Obs: Apenas regra de Envio */
				if	(ie_tipo_contrato_w in ('PF','PJ')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
				else
					cd_classificacao_item_w	:= 'TP';
				end if;
			elsif	(ie_codificacao_w = 'TA') then /* Tipo de ato cooperado - Obs: Apenas regra de Envio */
				if	(c_itens_w.ie_ato_cooperado in ('1','2','3')) then
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_ato_cooperado(c_itens_w.ie_ato_cooperado);
				else
					cd_classificacao_item_w	:= 'TA';
				end if;
			elsif	(ie_codificacao_w = 'TR') then /* Tipo de relacao com a OPS - Obs: Apenas regra de Envio */
				cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_relacao(ie_tipo_relacao_w);
			elsif	(ie_codificacao_w = 'RC') then /* Tipo de contratacao / Regulamentacao - Obs: Apenas regra de Envio */
				cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(c_itens_w.ie_tipo_contratacao,c_itens_w.ie_regulamentacao);
			elsif	(ie_codificacao_w = 'GA') then /* Grupo ANS */
				if	(ie_classif_grupo_w is not null) then
					cd_classificacao_item_w	:= ie_classif_grupo_w;
				else
					cd_classificacao_item_w	:= 'GA';
				end if;
			elsif	(ie_codificacao_w = 'CG') then /* Complemento grupo ANS */
				if	(ie_classif_grupo_ww is not null) then
					cd_classificacao_item_w	:= ie_classif_grupo_ww;
				else
					cd_classificacao_item_w	:= 'CG';
				end if;
			elsif	(ie_codificacao_w = 'CP') then /* Conta de pagamento cooperativa origem */
				select	max(a.cd_cgc)
				into	cd_cgc_w
				from	pls_congenere	a
				where	a.cd_cooperativa	= c_itens_w.nr_seq_congenere;
				
				if	(nvl(cd_cgc_w,0) <> 0) then
					select	max(cd_conta_contabil)
					into	cd_conta_contabil_w
					from	pessoa_jur_conta_cont a
					where	a.cd_cgc	= cd_cgc_w
					and	a.cd_empresa	= cd_empresa_w
					and	a.ie_tipo_conta	= 'P'
					and	c_itens_w.dt_referencia between nvl(a.dt_inicio_vigencia,c_itens_w.dt_referencia) and nvl(a.dt_fim_vigencia,c_itens_w.dt_referencia);
				end if;
				
				cd_conta_credito_w	:= cd_conta_contabil_w;
				
				select	max(cd_classificacao_atual)
				into	cd_classificacao_credito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;
			end if;

			if	(cd_classificacao_item_w is not null) then
				if	(ds_mascara_w = '00') then
					cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
				elsif	(ds_mascara_w = '0.0') then
					cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
				elsif	(ds_mascara_w = '0_') then
					cd_classificacao_item_w	:= cd_classificacao_item_w;
				else
					cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
				end if;

				cd_classificacao_debito_w	:= cd_classificacao_debito_w || cd_classificacao_item_w;
			end if;
		end if;
		end;
	end loop;
	close c_segmentacao;
	
	/* Remover o ultimo ponto da classificacao */
	if	(substr(cd_classificacao_credito_w,length(cd_classificacao_credito_w),length(cd_classificacao_credito_w)) = '.') then
		cd_classificacao_credito_w	:= substr(cd_classificacao_credito_w,1,length(cd_classificacao_credito_w)-1);
	end if;

	if	(substr(cd_classificacao_debito_w,length(cd_classificacao_debito_w),length(cd_classificacao_debito_w)) = '.') then
		cd_classificacao_debito_w	:= substr(cd_classificacao_debito_w,1,length(cd_classificacao_debito_w)-1);
	end if;

	if	(cd_conta_credito_w is null) then
		cd_conta_credito_w	:= ctb_obter_conta_classif(cd_classificacao_credito_w,c_itens_w.dt_referencia,cd_estabelecimento_p);
	end if;

	if	(cd_conta_debito_w is null) then
		cd_conta_debito_w	:= ctb_obter_conta_classif(cd_classificacao_debito_w,c_itens_w.dt_referencia,cd_estabelecimento_p);
	end if;

	if 	(((c_itens_w.nr_seq_conta_proc is not null) or (c_itens_w.nr_seq_conta_mat is not null)) or
		((c_itens_w.nr_seq_aviso_proc is not null) or (c_itens_w.nr_seq_aviso_mat is not null))) then
		if	(nr_seq_atualizacao_p is not null) then
			if	(nr_seq_esquema_w is null) then
				pls_gravar_mov_contabil(	nr_seq_atualizacao_p,
								1,
								null,
								c_itens_w.nr_seq_conta_proc,
								c_itens_w.nr_seq_conta_mat,
								'C',
								null, null, null, null, null, null, null, null, null, null, null,
								nm_usuario_p,
								nr_seq_esquema_w,
								null, null, null, null, null, null, null,
								c_itens_w.nr_seq_aviso_proc,
								c_itens_w.nr_seq_aviso_mat,
								c_itens_w.nr_seq_aviso_proc_item,
								c_itens_w.nr_seq_aviso_mat_item);
			elsif	((cd_conta_credito_w is null) or (cd_conta_debito_w is null)) then
				pls_gravar_mov_contabil(	nr_seq_atualizacao_p,
								2,
								null,
								c_itens_w.nr_seq_conta_proc,
								c_itens_w.nr_seq_conta_mat,
								'C',
								null, null, null, null, null, null, null, null, null, null, null,
								nm_usuario_p,
								nr_seq_esquema_w,
								null, null, null, null, null, null, null,
								c_itens_w.nr_seq_aviso_proc,
								c_itens_w.nr_seq_aviso_mat,
								c_itens_w.nr_seq_aviso_proc_item,
								c_itens_w.nr_seq_aviso_mat_item);
			end if;
		end if;
	end if;

	if	(c_itens_w.ie_tipo_processo = 'A520') then
		if	(c_itens_w.ie_tipo_movimento = 'E') then
			--insert into knoch_log (ds_valor) values ();
			if	(c_itens_w.ie_proc_mat = 'P') then
				update	ptu_aviso_proc_item
				set	cd_conta_debito			= cd_conta_debito_w,
					cd_conta_credito		= cd_conta_credito_w,
					cd_classif_debito		= cd_classificacao_debito_w,
					cd_classif_credito		= cd_classificacao_credito_w,
					nr_seq_esquema			= nr_seq_esquema_w,
					cd_historico	       		= cd_historico_padrao_w
				where	rowid				= c_itens_w.nr_id;
			
			elsif	(c_itens_w.ie_proc_mat = 'M') then
				update	ptu_aviso_mat_item
				set	cd_conta_debito			= cd_conta_debito_w,
					cd_conta_credito		= cd_conta_credito_w,
					cd_classif_debito		= cd_classificacao_debito_w,
					cd_classif_credito		= cd_classificacao_credito_w,
					nr_seq_esquema			= nr_seq_esquema_w,
					cd_historico	       		= cd_historico_padrao_w
				where	rowid				= c_itens_w.nr_id;
			end if;
		elsif	(c_itens_w.ie_tipo_movimento = 'R') then
			
			if	(c_itens_w.ie_proc_mat = 'P') then
				update	ptu_aviso_procedimento
				set	cd_conta_debito			= cd_conta_debito_w,
					cd_conta_credito		= cd_conta_credito_w,
					cd_classif_debito		= cd_classificacao_debito_w,
					cd_classif_credito		= cd_classificacao_credito_w,
					nr_seq_esquema			= nr_seq_esquema_w,
					cd_historico	       		= cd_historico_padrao_w
				where	rowid				= c_itens_w.nr_id;
			
			elsif	(c_itens_w.ie_proc_mat = 'M') then
				update	ptu_aviso_material
				set	cd_conta_debito			= cd_conta_debito_w,
					cd_conta_credito		= cd_conta_credito_w,
					cd_classif_debito		= cd_classificacao_debito_w,
					cd_classif_credito		= cd_classificacao_credito_w,
					nr_seq_esquema			= nr_seq_esquema_w,
					cd_historico	       		= cd_historico_padrao_w
				where	rowid				= c_itens_w.nr_id;
			end if;
		end if;

	elsif	(c_itens_w.ie_tipo_processo = 'A500') then
		update	ptu_nota_servico
		set	cd_conta_deb			= cd_conta_debito_w,
			cd_conta_cred			= cd_conta_credito_w,
			cd_classif_deb			= cd_classificacao_debito_w,
			cd_classif_cred			= cd_classificacao_credito_w,
			nr_seq_esquema			= nr_seq_esquema_w,
			cd_historico	       		= cd_historico_padrao_w
		where	rowid				= c_itens_w.nr_id;
	end if;
	
	qt_movimento_w	:= qt_movimento_w + 1;
	
	if	(mod(i,1000) = 0) then
		commit;
	end if;
	
	end;
end loop;
close c_itens;

commit;

qt_movimento_p	:= qt_movimento_w;

end ctb_pls_atualizar_aviso_cobr;
/
