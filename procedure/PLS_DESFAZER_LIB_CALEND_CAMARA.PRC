create or replace
procedure pls_desfazer_lib_calend_camara(	nr_seq_calendario_p	number,
						nm_usuario_p		varchar2) is 

begin

if	(nr_seq_calendario_p is not null) then
	update	pls_camara_calendario
	set	dt_liberacao	= null,
		nm_usuario_lib	= null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_calendario_p;

	commit;
end if;

end pls_desfazer_lib_calend_camara;
/
