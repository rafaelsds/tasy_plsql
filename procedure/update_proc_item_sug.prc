create or replace procedure update_proc_item_sug(nr_seq_proc_interno_p	number,
                                                  nr_atendimento_p	number,
                                                  nm_usuario_p	varchar2) is 
												  
qt_register_w         number(5);

Cursor c01 is
select    * 
from      proc_int_nec_item
where     nr_seq_proc_interno = nr_seq_proc_interno_p
and       cd_departamento = obter_departamento_atual_atend(nr_atendimento_p)
or        cd_departamento is null
and       ie_situacao = 'A'
order by  nr_seq_proc_interno;

c01_w c01%rowtype;		

begin

open c01;

loop

fetch c01 into	

	c01_w;

exit when c01%notfound;

	begin
	
	select	count(1)
	into	qt_register_w
	from	prescr_proc_item_sug
	where	nr_atendimento = nr_atendimento_p 
	and		nr_seq_proc_interno = nr_seq_proc_interno_p;
	
	if(qt_register_w = 0)then
	
		insert	into prescr_proc_item_sug 
		  (nr_sequencia, 
		  dt_atualizacao,
		  nm_usuario,
		  dt_atualizacao_nrec,
		  nm_usuario_nrec,
		  cd_unidade_medida,
		  qt_dose,
		  cd_intervalo,
		  nr_seq_proc_interno,
		  ie_via_aplicacao,
		  cd_material,
		  nr_atendimento,
      si_status) 
		values
		  (prescr_proc_item_sug_seq.nextval, 
		  sysdate,     
		  nm_usuario_p,
		  sysdate,        
		  nm_usuario_p,
		  c01_w.cd_unidade_medida,
		  c01_w.qt_dose,
		  null,
		  c01_w.nr_seq_proc_interno,
		  null,
		  c01_w.cd_material,
		  nr_atendimento_p,
      c01_w.si_status);


	end if;

end;

end loop;

close c01;

commit;

end update_proc_item_sug;
/
