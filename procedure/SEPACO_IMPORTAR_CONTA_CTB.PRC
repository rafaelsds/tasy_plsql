create or replace
procedure sepaco_importar_conta_ctb is 

cd_conta_w		varchar2(20);
cd_sistema_contabil_w	varchar2(140);
ds_conta_w		varchar2(80);
ie_centro_w		varchar2(1);
ie_tipo_w		varchar2(1);
cd_classif_w		varchar2(40);
cd_grupo_w		varchar2(10);
cd_empresa_w		varchar2(1)	:= 1;
qt_registro_w		number(10);
qt_commit_w		number(10)	:= 0;
ds_erro_w		varchar2(300);
ie_grupo_w		varchar2(1);

cursor C01 is
select	a.cd_conta,
	a.ds_conta,
	a.ie_tipo,
	a.cd_classif,
	a.ie_centro
from	w_sepaco_plano_conta a;

vet01	C01%RowType;

begin

open c01;
loop
fetch c01 into
	vet01;
exit when C01%notfound;

	cd_conta_w	:= vet01.cd_conta;
	qt_commit_w	:= qt_commit_w + 1;
	
	
	select	count(*)
	into	qt_registro_w
	from	conta_contabil
	where	cd_conta_contabil 	= vet01.cd_conta;
		
	if	(qt_registro_w > 0) then	
		select	max(somente_numero(cd_conta_contabil)) + 1
		into	cd_conta_w
		from	conta_contabil;		
	end if;
	
	
	cd_grupo_w	:= substr(vet01.cd_classif,1,1);
	select	max(cd_grupo)
	into	cd_grupo_w
	from	ctb_grupo_conta
	where	cd_grupo		= cd_grupo_w;
	
		begin
		insert into conta_contabil(
			cd_conta_contabil,
			ds_conta_contabil,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			ie_centro_custo,
			ie_tipo,
			cd_classificacao,
			cd_grupo,
			cd_empresa,
			ie_compensacao,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_sistema_contabil)
		values(	cd_conta_w,
			vet01.ds_conta,
			'I',
			sysdate,
			'Importacao',
			vet01.ie_centro,
			vet01.ie_tipo,
			vet01.cd_classif,
			cd_grupo_w,
			cd_empresa_w,
			'N',
			null,
			null,
			sysdate,
			'Importacao',
			cd_sistema_contabil_w);
		exception when others then 	
			ds_erro_w	:= sqlerrm(sqlcode);
			insert into log_tasy(
				cd_log,
				ds_log,
				nm_usuario,
				dt_atualizacao)
			values(	1225,
				'Conta n�o importada: ' || cd_conta_w || ' ERRO: ' || ds_erro_w,
				'Importacao',
				sysdate);
		end;
	if	(qt_commit_w >= 100) then
		commit;
		qt_commit_w	:= 0;
	end if;
end loop;
close c01;
commit;
end sepaco_importar_conta_ctb;
/