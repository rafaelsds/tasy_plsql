create or replace
procedure mprev_agendar_w_horario(nr_sequencia_p		w_mprev_agendamento.nr_sequencia%type,
				nr_seq_aprovacao_turma_p	number,
				ie_confirmacao_p		varchar2,
				qt_mensagens_erro_p		out	number,
				qt_mensagens_atencao_p		out	number,
				qt_mensagens_confirm_p		out	number,
				nr_seq_gerada_p			out	number) is 

qt_consist_w			number(10);
nr_seq_agendamento_w		w_mprev_agendamento.nr_sequencia%type;
cd_agenda_w			w_mprev_agendamento.cd_agenda%type;
nr_seq_participante_w		w_mprev_agendamento.nr_seq_participante%type;
nr_seq_turma_w			w_mprev_agendamento.nr_seq_turma%type;
ds_utilizacao_w			w_mprev_agendamento.ds_utilizacao%type;
cd_agenda_profissional_w	w_mprev_agendamento.cd_agenda_profissional%type;
cd_agenda_local_w		w_mprev_agendamento.cd_agenda_local%type;
dt_agendamento_w		w_mprev_agendamento.dt_agenda%type;
dt_final_agendamento_w		w_mprev_agendamento.dt_agenda%type;
nr_minutos_w			w_mprev_agendamento.nr_minuto_duracao%type;
ds_observacao_w			w_mprev_agendamento.ds_observacao%type;  
nm_usuario_w			w_mprev_agendamento.nm_usuario%type;
nr_seq_aprovacao_turma_w	number;
nr_seq_captacao_w		number;
ie_previsto_w			w_mprev_agendamento.ie_previsto%type;
nr_seq_partic_ciclo_item_w	w_mprev_agendamento.nr_seq_partic_ciclo_item%type;
ie_tipo_agendamento_w		w_mprev_agendamento.ie_tipo_agendamento%type;
ie_forma_atendimento_w		w_mprev_agendamento.ie_forma_atendimento%type;
nr_seq_ativ_extra_w		w_mprev_agendamento.nr_seq_ativ_extra%type;
ie_tipo_atendimento_w		w_mprev_agendamento.ie_tipo_atendimento%type;
nr_seq_grupo_tema_w		mprev_grupo_tema_encontro.nr_sequencia%type;
nr_seq_forma_atend_w	mprev_forma_atendimento.nr_sequencia%type;
nr_seq_pac_senha_fila_w		w_mprev_agendamento.nr_seq_pac_senha_fila%type;

begin

select	nr_seq_agendamento,	
        cd_agenda,			
        nr_seq_participante,		
        nr_seq_turma,		
        ds_utilizacao,		
        cd_agenda_profissional,
        cd_agenda_local,	
        dt_agenda,		
        nr_minuto_duracao,		
        ds_observacao,		
        nm_usuario,
	ie_tipo_agendamento,
        nr_seq_captacao,	
        ie_previsto,
        nr_seq_partic_ciclo_item,
        ie_forma_atendimento,
	nr_seq_ativ_extra,
	ie_tipo_atendimento,
	nr_seq_grupo_tema,
	nr_seq_forma_atend,
	nr_seq_pac_senha_fila
into	
	nr_seq_agendamento_w,		
	cd_agenda_w,			
	nr_seq_participante_w,		
	nr_seq_turma_w,			
	ds_utilizacao_w,			
	cd_agenda_profissional_w,	
	cd_agenda_local_w,		
	dt_agendamento_w,		
	nr_minutos_w,			
	ds_observacao_w,			
	nm_usuario_w,
	ie_tipo_agendamento_w,
	nr_seq_captacao_w,		
	ie_previsto_w,			
	nr_seq_partic_ciclo_item_w,
	ie_forma_atendimento_w,
	nr_seq_ativ_extra_w,
	ie_tipo_atendimento_w,
	nr_seq_grupo_tema_w,
	nr_seq_forma_atend_w,
	nr_seq_pac_senha_fila_w
from	w_mprev_agendamento
where	nr_sequencia = nr_sequencia_p;

dt_final_agendamento_w		:= dt_agendamento_w + (nr_minutos_w * (1/24/60));

mprev_agendar_horario(	nr_seq_agendamento_w,		
			cd_agenda_w,		
			nr_seq_participante_w,	
			nr_seq_turma_w,	
			null,--nr_seq_horario_turma_p
			ds_utilizacao_w,		
			cd_agenda_profissional_w,
			cd_agenda_local_w,	
			dt_agendamento_w,	
			dt_final_agendamento_w,	
			nr_minutos_w,		
			ds_observacao_w,
			'S',--ie_marcar_horario_p
			nm_usuario_w,
			'N',
			ie_confirmacao_p,	
			nr_seq_aprovacao_turma_p,
			nr_seq_captacao_w,	
			ie_previsto_w,		
			nr_seq_partic_ciclo_item_w,
			ie_forma_atendimento_w,	
			nr_seq_ativ_extra_w,
			ie_tipo_agendamento_w,
			ie_tipo_atendimento_w,
			nr_sequencia_p,
			nr_seq_grupo_tema_w,
			nr_seq_forma_atend_w,
			qt_mensagens_erro_p,
			qt_mensagens_atencao_p,
			qt_mensagens_confirm_p,
			nr_seq_gerada_p,
			nr_seq_pac_senha_fila_w);

end mprev_agendar_w_horario;
/
