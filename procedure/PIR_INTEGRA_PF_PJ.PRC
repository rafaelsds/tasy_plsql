CREATE OR REPLACE procedure Pir_Integra_PF_pj(	cd_pessoa_fisica_p	varchar2,
								cd_cnpj_p		varchar2,
								cd_Estabelecimento_p	number,
								nm_usuario_p		Varchar2) is
qt_registro_w		number(10,0);
cd_municipio_ibge_w	varchar2(6);
nm_pessoa_w		varchar2(100);
nr_cpf_cnpj_w		varchar2(20);
dt_cadastro_original_w	date;
ds_bairro_w		varchar2(50);
ds_email_w		varchar2(255);
ds_endereco_w		varchar2(60);
cd_cep_w		varchar2(15);
ds_fax_w		varchar2(80);
nr_telefone_w		varchar2(15);
nr_endereco_w		number(5);
sg_emissora_ci_w	varchar2(2);
ds_complemento_w	varchar2(40);
ds_municipio_w		varchar2(40);
ds_erro_w		varchar2(255):= null;
cd_empresa_w		number(4,0);
ds_observacao_w		varchar2(2000);
cd_cliente_w		varchar2(20);
ie_tipo_pessoa_w	varchar2(1);
sql_errm_w		varchar2(4000);
begin
if	(nvl(cd_pessoa_fisica_p,'X') <> 'X') then
	cd_cliente_w	:= cd_pessoa_fisica_p;
elsif	(nvl(cd_cnpj_p,'X') <> 'X') then
	cd_cliente_w	:= cd_cnpj_p;
end if;
select	cd_empresa
into	cd_empresa_w
from	estabelecimento_v
where	cd_estabelecimento = cd_estabelecimento_p;
-- Verifica se a pessoa j existe na tabela TI_CLIENTE
select	count(*)
into	qt_registro_w
from 	ti_cliente
where 	cod_sistema_origem = 'TAS'
and 	cod_cliente_origem = cd_cliente_w;
if	(qt_registro_w = 0) then
	if	(nvl(cd_pessoa_fisica_p,'X') <> 'X') then
		ie_tipo_pessoa_w	:= 'F';
		select 	substr(nvl(max(nm_pessoa_fisica),'Pirmide'),1,100),
			nvl(max(nr_cpf),'00000000000'),
			nvl(max(dt_cadastro_original),sysdate),
			decode(max(CD_NACIONALIDADE),'10',max(sg_emissora_ci),'EX'),
			max(ds_observacao)
		into	nm_pessoa_w,
			nr_cpf_cnpj_w,
			dt_cadastro_original_w,
			sg_emissora_ci_w,
			ds_observacao_w
		from 	pessoa_fisica
		where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
		select 	substr(nvl(max(ds_bairro),'No Informado'),1,50),
			substr(nvl(max(ds_endereco),'No Informado'),1,60),
			nvl(max(cd_cep),'00000000'),
			nvl(max(nr_telefone),'00000000'),
			nvl(max(ds_fax),'00000000'),
			nvl(max(ds_email),'No Informado'),
			nvl(max(nr_endereco),'00000000'),
			nvl(max(ds_complemento),'No Informado'),
			nvl(max(ds_municipio),'No Informado'),
			nvl(max(cd_municipio_ibge),'0')
		into	ds_bairro_w,
			ds_endereco_w,
			cd_cep_w,
			nr_telefone_w,
			ds_fax_w,
			ds_email_w,
			nr_endereco_w,
			ds_complemento_w,
			ds_municipio_w,
			cd_municipio_ibge_w
		from 	compl_pessoa_fisica
		where 	cd_pessoa_fisica = cd_pessoa_fisica_p
		and 	ie_tipo_complemento = 1;
	end if;
	if	(nvl(cd_cnpj_p,'X') <> 'X') then
		ie_tipo_pessoa_w := 'J';
		select	nvl(dt_atualizacao_nrec, sysdate),
			substr(ds_razao_social,1,100),
			cd_cgc,
			substr(ds_endereco,1,60),
			substr(nvl(ds_bairro,'No informado'),1,50),
			cd_cep,
			substr(nvl(nr_telefone,'0'),1,15),
			substr(nvl(nr_fax,'0'),1,15),
			substr(ds_email,1,100),
			substr(nvl(nr_endereco,'0'),1,10),
			sg_estado,
			nvl(cd_municipio_ibge,'0'),
			ds_complemento,
			NVL(ds_municipio,'No Informado')
		into	dt_cadastro_original_w,
			nm_pessoa_w,
			nr_cpf_cnpj_w,
			ds_endereco_w,
			ds_bairro_w,
			cd_cep_w,
			nr_telefone_w,
			ds_fax_w,
			ds_email_w,
			nr_endereco_w,
			sg_emissora_ci_w,
			cd_municipio_ibge_w,
			ds_complemento_w,
			ds_municipio_w
		from	pessoa_juridica
		where	cd_cgc = cd_cnpj_p;
	end if;
	if	(cd_municipio_ibge_w = '0') then
		insert into ERROS_INTEGRACAO_PIRAMIDE values (cd_estabelecimento_p, nm_usuario_p, 'CLI', 'Falta informar municpio IBGE no complemento do cadastro da pessoa fsica ' ||cd_pessoa_fisica_p || '.' );
	end if;
	begin
	insert into ti_cliente(
		        COD_CLIENTE_ORIGEM,
		 	COD_SISTEMA_ORIGEM,	
			COD_SEGMENTO_MERCADO_ORIGEM,
			COD_MUNICIPIO_COB_ORIGEM,
		     	COD_MUNICIPIO_FAT_ORIGEM, 
			COD_AREA_ORIGEM,
			COD_GRUPO_EMPRESARIAL_ORIGEM,
			COD_CLIENTE_DESTINO,
		        NOM_CLIENTE,
			COD_TIPO_PESSOA,
			COD_CNPJ_CPF,
			DAT_CADASTRAMENTO,
		        DSC_ENDERECO_FAT,
			DSC_BAIRRO_FAT,	
			COD_CEP_FAT,
			COD_FONE_FAT,
		        COD_FAX_FAT,
			IND_CONTRIBUINTE_ICMS,	
			NOM_FANTASIA_CLIENTE,
			COD_INSCRICAO_ESTADUAL,
		        DSC_EMAIL,	
			DSC_ENDERECO_COB,
			DSC_BAIRRO_COB,	
			COD_CEP_COB,
		        COD_FONE_COB,	
			COD_FAX_COB,	
			IND_USA_CEI,	
			COD_CEI,
		        NUM_INSCR_SUFRAMA,
			IND_PRODUTOR_RURAL,	
			END_NUMERO_COB,	
			END_NUMERO_FAT,
		        COD_UF_FAT,
			COD_PAIS_FAT,	
			DSC_COMPLEMENTO_FAT,
			DSC_COMPLEMENTO_COB,
		        COD_INSCRICAO_MUNICIPAL,
			DAT_ATUALIZACAO,
			COD_USU_SISTEMA_ORIGEM,	
			DSC_MUNICIPIO_FAT_ORIGEM,
		        COD_BANCO_DEB_ORIGEM,
			COD_AGENCIA_DEB_ORIGEM,	
			COD_CCORRENTE_DEB_ORIGEM,
			DSC_OBSERVACAO,
		        COD_OPERACAO_REGISTRO,
			COD_STATUS_REGISTRO,
			DSC_ERRO_REGISTRO)
	values	    (    cd_cliente_w,
			'TAS',
			'C996',	
         		 null,
		         cd_municipio_ibge_w,
			'C996',	
			'C9996',
  			 null,
		     	 nm_pessoa_w,
	 		 ie_tipo_pessoa_w,
			 nr_cpf_cnpj_w,	
			 dt_cadastro_original_w,
		     	 ds_endereco_w,	
     		 	 ds_bairro_w,	
			 cd_cep_w,
			 nr_telefone_w,
		         ds_fax_w,
 			 'N',
			 null,
 			 null,
		     	 ds_email_w,
			 ds_endereco_w,
			 ds_bairro_w,
			 cd_cep_w,
	  	         nr_telefone_w,
			 ds_fax_w,
			 'N',
			 null,
		         null,	
			 'N',
			 nr_endereco_w,
 			 nr_endereco_w,
		         sg_emissora_ci_w,
     	     		 null,
			 ds_complemento_w,
			 ds_complemento_w,
		    	 null,	
  			 dt_cadastro_original_w,
			 nm_usuario_p,
			 ds_municipio_w,
		         null,	
			 null,
			 null,
			 null,
		         'I',
			'NP',
			ds_erro_w);
	 exception
                        when others then
                        sql_errm_w      := sqlerrm;
			gravar_log_tasy(1, 'impPiramide: ' || sql_errm_w,'TASY');
         end;
         commit;
	 begin
	insert into ti_cliente_empresa(
		     		COD_CLIENTE_ORIGEM,
				COD_SISTEMA_ORIGEM,
				COD_EMPRESA_ORIGEM,
				COD_BLOQUEIO_ORIGEM,
		    		COD_VENDEDOR_ORIGEM,
				COD_CARGO_CONTATO_ORIGEM,
				COD_FUNCAO_CONTATO_ORIGEM,
				DAT_CADASTRAMENTO,
		     		IND_SITUACAO,
				PCT_MORA,
				PCT_MULTA,
				NUM_DIASMULTA,
		    		COD_BANCO_COMP,	
				COD_AGENCIA_ORIGEM,
				VAL_LIMITE_CREDITO,
				DAT_LIMITE_CREDITO,
		     		NUM_PRAZO,
				NOM_CONTATO,
				COD_FONE_CONTATO,
				COD_RAMAL_CONTATO,
		     	 	COD_FAX_CONTATO,
				DSC_EMAIL_CONTATO,
				DAT_ANIVERSARIO_CONTATO,
				DSC_OBSERVACAO_CONTATO,
		    	        DSC_OBSERVACAO_CLIENTE,	
				NUM_DIAS_ATRASO,
				COD_CONTA_CONTABIL_ORIGEM,
				COD_CONTA_AUXILIAR_ORIGEM,
		     		COD_CONTATO_ORIGEM,
				COD_DOC_PAGAMENTO_ORIGEM,
				COD_CONDPAG_ORIGEM,	
				COD_TIPO_PESSOA_CONTATO,
		     		IND_PENDENTE_CREDITO,
				COD_OPERACAO_REGISTRO,
				COD_STATUS_REGISTRO,	
				DSC_ERRO_REGISTRO)
	values	    (		cd_cliente_w,
				'TAS',	
				'025',
				'00',
		  		'8888',	
				null,	
				'0',
				sysdate,
		   		'A',
				0,
				0,
				0,
		   	 	null,
				null,	
				0,
				null,
		  		null,
				null,
				null,	
				null,
		 		null,	
				null,
				null,	
				null,
		    		ds_observacao_w,
				0,
				null,
				null,
		   		null,
				'NSN',	
				null,	
				null,
		   		'N',
				'I',
				'NP',	
				ds_erro_w);
	 exception
                                when others then
                                sql_errm_w      := sqlerrm;
				gravar_log_tasy(1, 'impPiramide: ' || sql_errm_w,'TASY');
         end;
	commit;
end if;
end Pir_Integra_PF_pj;
/