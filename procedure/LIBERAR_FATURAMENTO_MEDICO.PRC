create or replace
procedure Liberar_Faturamento_Medico(nr_sequencia_p	number,
				     ie_opcao_p		varchar2,
				     nm_usuario_p	Varchar2) is 

begin

if	(ie_opcao_p = 'L') then

	update	Pep_med_fatur
	set 	dt_liberacao = sysdate,
		nm_usuario   = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
	
else

	update	Pep_med_fatur
	set 	dt_liberacao = null,
		nm_usuario   = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end Liberar_Faturamento_Medico;
/