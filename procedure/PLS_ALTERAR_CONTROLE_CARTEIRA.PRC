create or replace
procedure pls_alterar_controle_carteira
		(	nr_seq_contrato_p	number,
			ie_controle_carteira_p	varchar2,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number) is 

ds_controle_carteira_w		varchar2(15);
ds_controle_carteira_novo_w	varchar2(15);

begin

select	decode(ie_controle_carteira,'V','Vencimento','E','Emiss�o','N','Nenhum','Ambos'),
	decode(ie_controle_carteira_p,'V','Vencimento','E','Emiss�o','N','Nenhum','Ambos')
into	ds_controle_carteira_w,
	ds_controle_carteira_novo_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_p;

update	pls_contrato
set	ie_controle_carteira	= ie_controle_carteira_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_contrato_p;

insert into pls_contrato_historico
	      (	nr_sequencia, cd_estabelecimento, nr_seq_contrato, dt_historico, ie_tipo_historico,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, 
		ds_historico,ds_observacao)
	values(	pls_contrato_historico_seq.NextVal, cd_estabelecimento_p, nr_seq_contrato_p, sysdate, '24',
		sysdate, nm_usuario_p, sysdate, nm_usuario_p, 
		'Alterado o controle de emiss�o da carteira de ' || ds_controle_carteira_w || ' para ' || ds_controle_carteira_novo_w,'pls_alterar_controle_carteira');

commit;

end pls_alterar_controle_carteira;
/
