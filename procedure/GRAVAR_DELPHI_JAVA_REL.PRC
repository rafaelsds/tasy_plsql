create or replace 
procedure gravar_delphi_java_rel(	
			nr_sequencia_p		number,		
			nm_usuario_p		varchar2,
			cd_funcao_p			number,
			cd_gerencia_p		number,
			ie_pasta_p			varchar2,
			ie_pop_up_comp_p 	varchar2,
			ie_wcpanel_p		varchar2,
			ie_wdb_coluna_p		varchar2,
			ie_wselecao_p		varchar2,
			ie_wdlg_p			varchar2 default 'N')	is
		
Cursor C01 is
	select	a.*
	from	w_analise_migr_proj a
	where	a.nm_usuario = nm_usuario_p
	and		a.cd_funcao = cd_funcao_p
	and		a.nr_sequencia = nr_sequencia_p
	and		(((ie_pasta_p = 'S') and (a.ie_objeto = 'TTabSheet')) or
			((ie_pasta_p = 'S') and (a.ie_objeto = 'TS')) or
			((ie_pasta_p = 'S') and (a.ie_objeto = 'TPageControl')) or
			((ie_pop_up_comp_p = 'S') and (a.ie_objeto = 'TMenuItem')) or
			((ie_pop_up_comp_p = 'S') and (a.ie_objeto = 'TPopUpMenu')) or
			((ie_pop_up_comp_p = 'S') and (a.ie_objeto = 'PM')) or
			((ie_pop_up_comp_p = 'S') and (a.ie_objeto = 'MI'))	or
			((ie_wcpanel_p = 'S') and (a.ie_objeto = 'TWCPanel')) or
			((ie_wdb_coluna_p = 'S') and (a.ie_objeto = 'TDBPanel')) or
			((ie_wselecao_p = 'S') and (a.ie_objeto = 'TWSelecaoCB')) or
			((ie_wdlg_p = 'S') and (a.ie_objeto = 'TWDLGPanel')))	
	order by 1;
	
c01_w c01%rowtype;

Cursor C02 is
	select	a.*
	from	w_colunas_grid_wcpanel a
	where	a.nm_usuario = nm_usuario_p
	and		a.nr_sequencia = nr_sequencia_p
	and		a.cd_funcao = cd_funcao_p
	and		(((ie_wcpanel_p = 'S') and (a.ie_tipo = 'P')) or
			((ie_wdb_coluna_p = 'S') and (a.ie_tipo = 'B')))
	order by 1;
	
c02_w c02%rowtype;
		
		
begin

if	(nr_sequencia_p is not null) then

	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin
		
		insert into W_DELPHI_JAVA(  nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									cd_funcao,
									cd_gerencia,
									ie_objeto,
									nm_objeto,
									nm_objeto_pai,
									ds_objeto,
									QT_TEMPO_DESENV) 	
						values 		(w_delphi_java_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									cd_funcao_p,
									cd_gerencia_p,
									c01_w.ie_objeto,
									c01_w.nm_objeto,
									c01_w.nm_objeto_pai,
									c01_w.ds_objeto,
									c01_w.QT_TEMPO_DESENV);
								
		commit;
		
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into	
		c02_w;
	exit when C02%notfound;
		begin
		
		insert into W_DELPHI_JAVA(  nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									cd_funcao,
									cd_gerencia,
									ie_objeto,
									nm_coluna,
									ds_titulo_coluna,
									nm_componente,
									cd_codigo,
									nr_seq_objeto,
									nr_seq_visao,
									ie_tipo,
									nm_tabela,
									ie_tipo_coluna,
									qt_tempo_desenv)	
						values 		(w_delphi_java_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									cd_funcao_p,
									cd_gerencia_p,
									decode(c02_w.ie_tipo,'B','WDBPColuna','P','WCPColuna',''),
									c02_w.nm_coluna,
									c02_w.ds_titulo_coluna,
									c02_w.nm_componente,
									c02_w.cd_codigo,
									c02_w.nr_seq_objeto,
									c02_w.nr_seq_visao,
									c02_w.ie_tipo,
									c02_w.nm_tabela,
									c02_w.ie_tipo_coluna,
									5);
								
		commit;
		
		end;
	end loop;
	close C02;
end if;

end gravar_delphi_java_rel;
/