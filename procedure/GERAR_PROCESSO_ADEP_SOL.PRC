create or replace
procedure gerar_processo_adep_sol    (    nr_atendimento_p                number,
                                        nr_prescricao_p                    number,
                                        nr_seq_solucao_p                number,
                                        nr_etapa_p                        number,
                                        dt_horario_processo_p            date,
                                        nm_usuario_p                    varchar2,
                                        nr_seq_processo_p         out        number,
                                        ie_somente_gedipa_p                varchar2,
                                        cd_funcao_origem_p                number,
                                        ie_origem_processo_p            varchar2,
                                        ie_gedipa_p                        varchar2,
                                        nr_seq_material_p                number,
                                        nr_seq_procedimento_p            number) is
                    
nr_seq_processo_w            number(10,0);
cd_setor_atend_w            number(5,0);
cd_local_estoque_w            number(4,0);
ds_erro_w                    varchar2(1800);
ie_urgente_w                varchar2(2);
cd_estabelecimento_w        number(4,0);
ie_local_estoque_proc_w        varchar2(15);
cd_intervalo_w                varchar2(7);
ie_acm_w                    varchar2(1);
ie_se_necessario_w            varchar2(1);
qt_tempo_min_w                number(10,0);
nr_seq_regra_w                number(10,0);
ie_gerar_urgente_w            varchar2(1);
ie_grava_log_gedipa_w        varchar2(1);
ie_hemodialise_w            varchar2(1);
ie_possui_ancora_w            varchar(1);
nr_seq_material_w            number(10);
ie_vincula_processo_pharm_w	varchar2(1);
ie_agrupador_w				prescr_material.ie_agrupador%type;


Cursor C01 is
    select    nr_sequencia
    from    regra_etapa_gedipa
    where    nvl(cd_setor_atendimento,nvl(cd_setor_atend_w,0))     = nvl(cd_setor_atend_w,0)
    and    nvl(cd_intervalo,nvl(cd_intervalo_w,'0'))        = nvl(cd_intervalo_w,'0')
    and    nvl(ie_acm,nvl(ie_acm_w,'N'))                = nvl(ie_acm_w,'N')
    and    nvl(ie_se_necessario,nvl(ie_se_necessario_w,'N'))    = nvl(ie_se_necessario_w,'N')
    and    nvl(qt_tempo_min_w,0) between nvl(qt_tempo_min,-99999) and nvl(qt_tempo_max,99999)
    and    ie_situacao = 'A'
    and    (((nvl(ie_hemodialise,'S') = 'S') and (nvl(ie_hemodialise_w,'X') = 'H')) or
        ((nvl(ie_hemodialise,'N') = 'N') and (nvl(ie_hemodialise_w,'X') <> 'H')))
    order by 
        nvl(cd_setor_atendimento,000000),
        nvl(cd_intervalo,'AAAAAAA'),
        nvl(ie_acm,'AAAAAAA'),
        nvl(ie_se_necessario,'AAAAAAA'),
        nvl(qt_tempo_min,000000);


begin

if (nr_atendimento_p is not null) then
    select    max(cd_estabelecimento)
    into    cd_estabelecimento_w
    from    atendimento_paciente
    where    nr_atendimento = nr_atendimento_p;

    select    nvl(max(ie_local_estoque_proc),'P'),
            nvl(max(ie_grava_log_gedipa),'S')
    into    ie_local_estoque_proc_w,
            ie_grava_log_gedipa_w
    from    parametros_farmacia
    where    cd_estabelecimento = cd_estabelecimento_w;
end if;

if (ie_grava_log_gedipa_w = 'S') then
    insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
    values (obter_nextval_sequence('log_gedipa'), sysdate, 705, 'GERAR_PROCESSO_ADEP_SOL', null, 'nr_atendimento_p='||to_char(nr_atendimento_p)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_solucao_p='||to_char(nr_seq_solucao_p)||'; nr_etapa_p='||to_char(nr_etapa_p)||'; dt_horario_processo_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_processo_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone) ||'; nm_usuario_p='||nm_usuario_p||'; nr_seq_processo_p='||to_char(nr_seq_processo_p), 'Inicio da execucao da procedure GERAR_PROCESSO_ADEP_SOL');
end if;

obter_param_usuario(1113, 617, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w, ie_vincula_processo_pharm_w);

if    (nr_atendimento_p is not null) and
    (nr_prescricao_p is not null) and
    (nr_seq_solucao_p is not null) and
    (nr_seq_procedimento_p is null) and
    (nr_etapa_p is not null) and    
    (nm_usuario_p is not null) then
    begin

    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 710, 'GERAR_PROCESSO_ADEP_SOL', null, null, obter_desc_expressao(781334) || '01'/*'Execucao do comando SQL 01'*/);
    end if;
    
    cd_setor_atend_w    := Obter_Unidade_Atendimento(nr_atendimento_p,'IAA','CS');
    if    (ie_local_estoque_proc_w = 'P') then
        begin
        cd_local_estoque_w    := obter_local_estoque_setor(cd_setor_atend_w, cd_estabelecimento_w);
        end;
    elsif    (ie_local_estoque_proc_w = 'S') then
        begin
        select    obter_local_estoque_setor(cd_setor_atendimento, cd_estabelecimento_w)
        into    cd_local_estoque_w
        from    prescr_medica
        where    nr_prescricao = nr_prescricao_p;
        end;
    ELSIF    (ie_local_estoque_proc_w in ('R', 'L')) THEN

        select     min(nr_sequencia)
        into    nr_seq_material_w
        from    material b,
                prescr_material a
        where    a.cd_material        =    b.cd_material
        and        nvl(b.ie_ancora_solucao,'S') = 'S'
        and        a.nr_sequencia_solucao     =    nr_seq_solucao_p
        and        a.nr_prescricao            =    nr_prescricao_p;
        
        if         (nr_seq_material_w is null) then
                select     min(nr_sequencia)
                into    nr_seq_material_w
                from    material b,
                        prescr_material a
                where    a.cd_material        =    b.cd_material
                and        a.nr_sequencia_solucao    =    nr_seq_solucao_p
                and        a.nr_prescricao            =    nr_prescricao_p;
        end if;
        
        if    (ie_local_estoque_proc_w = 'L') then
        
            select    max(cd_local_estoque_proc)
            into    cd_local_estoque_w
            from    regra_local_dispensacao
            where    nr_sequencia in     (    select    max(nr_regra_local_disp)
                                            from    prescr_mat_hor a,
                                                    prescr_material b
                                            where    a.nr_prescricao = b.nr_prescricao
                                            and        a.nr_seq_material = b.nr_sequencia
                                            and        b.nr_sequencia_solucao    =    nr_seq_solucao_p
                                            and        b.nr_prescricao            =    nr_prescricao_p
                                            and        b.nr_sequencia             =   nr_seq_material_w);
        else    
            cd_local_estoque_w    := obter_local_estoque_regra_ged(cd_setor_atend_w,nr_prescricao_p,nvl(nr_seq_material_p,nr_seq_material_w),null,dt_horario_processo_p,'N');
        end if;
        
    end if;
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 715, 'GERAR_PROCESSO_ADEP_SOL', null, null, obter_desc_expressao(781334) || '02'/*'Execucao do comando SQL 02'*/);        
    end if;
    
    select    adep_processo_seq.nextval
    into    nr_seq_processo_w
    from    dual;
    	
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 720, 'GERAR_PROCESSO_ADEP_SOL', null, null, 'Insert na tabela ADEP_PROCESSO; SEQ='||to_char(nr_seq_processo_w));    
    end if;
    
    select     max(nvl(ie_urgencia,'N')),
        max(ie_classif_agora), --max(cd_intervalo),
        max(ie_acm),
        max(ie_se_necessario),
        max(trunc((dt_horario_processo_p - sysdate) * 1440))
    into    ie_urgente_w,
        cd_intervalo_w,
        ie_acm_w,
        ie_se_necessario_w,
        qt_tempo_min_w
    from    prescr_solucao
    where    nr_prescricao  = nr_prescricao_p
    and    nr_seq_solucao = nr_seq_solucao_p;
    
    select    max('H')
    into    ie_hemodialise_w
    from    prescr_material a,
            prescr_solucao b
    where    a.nr_prescricao         = b.nr_prescricao
    and        a.nr_sequencia_solucao    = b.nr_seq_solucao
    and        b.nr_prescricao          = nr_prescricao_p
    and        b.nr_seq_solucao         = nr_seq_solucao_p
    and        a.ie_agrupador            = 13;
    
    open C01;
    loop
    fetch C01 into    
        nr_seq_regra_w;
    exit when C01%notfound;
    begin
        nr_seq_regra_w := nr_seq_regra_w;
    end;
    end loop;
    close C01;

    if    (nr_seq_regra_w is not null) then
        select    nvl(ie_gerar_urgente,'N')
        into    ie_gerar_urgente_w
        from    regra_etapa_gedipa
        where    nr_sequencia    = nr_seq_regra_w;
    end if;

    insert into adep_processo (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_atendimento,
        nr_prescricao,
        nr_seq_solucao,
        nr_etapa,
        dt_processo,
        nm_usuario_processo,
        dt_leitura,
        nm_usuario_leitura,
        dt_preparo,
        nm_usuario_preparo,
        dt_paciente,
        nm_usuario_paciente,
        dt_horario_processo,
        cd_local_estoque,
        cd_setor_atendimento,
        cd_funcao_origem,
        ie_origem_processo,
        ie_gedipa,
        ie_urgente,
        cd_intervalo,
        ie_acm,
        ie_se_necessario,
        nr_seq_regra,
        ie_tipo_processo)
    values (
        nr_seq_processo_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_atendimento_p,
        nr_prescricao_p,
        nr_seq_solucao_p,
        nr_etapa_p,
        sysdate,
        nm_usuario_p,
        null,
        null,
        null,
        null,
        null,
        null,
        dt_horario_processo_p,
        cd_local_estoque_w,
        cd_setor_atend_w,
        cd_funcao_origem_p,
        ie_origem_processo_p,
        ie_gedipa_p,
        decode(ie_gerar_urgente_w,'S','S',ie_urgente_w),
        cd_intervalo_w,
        ie_acm_w,
        ie_se_necessario_w,
        nr_seq_regra_w,
        ie_hemodialise_w);
		
	if (nvl(ie_vincula_processo_pharm_w,'N') = 'L') then	
		preparar_processo_adep(nr_seq_processo_w,'S',nm_usuario_p);
	end if;	
    
    nr_seq_processo_p := nr_seq_processo_w;
    end;
elsif     (nr_atendimento_p is not null) and
    (nr_prescricao_p is not null) and
    (nr_seq_solucao_p is null) and
    (nr_seq_material_p is not null) and
    (nr_seq_procedimento_p is null) and
    (nr_etapa_p is not null) and    
    (nm_usuario_p is not null) then
    begin
    
    select    max(cd_estabelecimento)
    into    cd_estabelecimento_w
    from    atendimento_paciente
    where    nr_atendimento = nr_atendimento_p;

    select    nvl(max(ie_local_estoque_proc),'P')
    into    ie_local_estoque_proc_w
    from    parametros_farmacia
    where    cd_estabelecimento = cd_estabelecimento_w;    
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 710, 'GERAR_PROCESSO_ADEP_SOL', null, null, obter_desc_expressao(781334) || '01-2'/*'Execucao do comando SQL 01-2'*/);
    end if;
        
    cd_setor_atend_w    := Obter_Unidade_Atendimento(nr_atendimento_p,'IAA','CS');
    if    (ie_local_estoque_proc_w = 'P') then
        begin
        cd_local_estoque_w    := obter_local_estoque_setor(cd_setor_atend_w, cd_estabelecimento_w);
        end;
    elsif    (ie_local_estoque_proc_w = 'S') then
        begin
        select    obter_local_estoque_setor(cd_setor_atendimento, cd_estabelecimento_w)
        into    cd_local_estoque_w
        from    prescr_medica
        where    nr_prescricao = nr_prescricao_p;
        end;
    ELSIF    (ie_local_estoque_proc_w in ('R', 'L')) THEN
    
        if    (ie_local_estoque_proc_w = 'L') then
        
            select    max(cd_local_estoque_proc)
            into    cd_local_estoque_w
            from    regra_local_dispensacao
            where    nr_sequencia in     (    select    max(nr_regra_local_disp)
                                            from    prescr_mat_hor a,
                                                    prescr_material b
                                            where    a.nr_prescricao = b.nr_prescricao
                                            and        a.nr_seq_material = b.nr_sequencia
                                            and        b.nr_prescricao            =    nr_prescricao_p
                                            and        b.nr_sequencia             =   nr_seq_material_p);
        else
            cd_local_estoque_w    := obter_local_estoque_regra_ged(cd_setor_atend_w,nr_prescricao_p,nr_seq_material_p,null,dt_horario_processo_p,'N');
        end if;
        
    end if;
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 715, 'GERAR_PROCESSO_ADEP_SOL', null, null, obter_desc_expressao(781334) || '02-2'/*'Execucao do comando SQL 02-2'*/);        
    end if;
    
    select    adep_processo_seq.nextval
    into    nr_seq_processo_w
    from    dual;
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 720, 'GERAR_PROCESSO_ADEP_SOL', null, null, 'Insert na tabela ADEP_PROCESSO-2; SEQ='||to_char(nr_seq_processo_w));    
    end if;
    
    select  max(nvl(ie_urgencia,'N')),
        	max(cd_intervalo),
	        max(ie_acm),
	        max(ie_se_necessario),
	        max(trunc((dt_horario_processo_p - sysdate) * 1440)),
	        max(decode(ie_agrupador,13,'H',null)),
			max(ie_agrupador)
    into    ie_urgente_w,
	        cd_intervalo_w,
	        ie_acm_w,
	        ie_se_necessario_w,
	        qt_tempo_min_w,
	        ie_hemodialise_w,
			ie_agrupador_w
    from    prescr_material
    where   nr_prescricao  = nr_prescricao_p
    and    nr_sequencia = nr_seq_material_p;
    
    open C01;
    loop
    fetch C01 into    
        nr_seq_regra_w;
    exit when C01%notfound;
    begin
        nr_seq_regra_w := nr_seq_regra_w;
    end;
    end loop;
    close C01;

    if    (nr_seq_regra_w is not null) then
        select    nvl(ie_gerar_urgente,'N')
        into    ie_gerar_urgente_w
        from    regra_etapa_gedipa
        where    nr_sequencia    = nr_seq_regra_w;
    end if;
    

    insert into adep_processo (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_atendimento,
        nr_prescricao,
        nr_seq_material,
        nr_etapa,
        dt_processo,
        nm_usuario_processo,
        dt_leitura,
        nm_usuario_leitura,
        dt_preparo,
        nm_usuario_preparo,
        dt_paciente,
        nm_usuario_paciente,
        dt_horario_processo,
        cd_local_estoque,
        cd_setor_atendimento,
        cd_funcao_origem,
        ie_origem_processo,
        ie_gedipa,
        ie_urgente,
        cd_intervalo,
        ie_acm,
        ie_se_necessario,
        nr_seq_regra,
        ie_tipo_processo)
    values (
        nr_seq_processo_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_atendimento_p,
        nr_prescricao_p,
        nr_seq_material_p,
        nr_etapa_p,
        sysdate,
        nm_usuario_p,
        null,
        null,
        null,
        null,
        null,
        null,
        dt_horario_processo_p,
        cd_local_estoque_w,
        cd_setor_atend_w,
        cd_funcao_origem_p,
        ie_origem_processo_p,
        ie_gedipa_p,
        decode(ie_gerar_urgente_w,'S','S',ie_urgente_w),
        cd_intervalo_w,
        ie_acm_w,
        ie_se_necessario_w,
        nr_seq_regra_w,
        ie_hemodialise_w);
		
		if (((nvl(ie_vincula_processo_pharm_w,'N') = 'X') and (ie_agrupador_w <> 11)) or
            (nvl(ie_vincula_processo_pharm_w,'N') in ('T', 'S'))) then
			preparar_processo_adep(nr_seq_processo_w,'S',nm_usuario_p);
		end if;
        
    nr_seq_processo_p := nr_seq_processo_w;
    end;
elsif     (nr_atendimento_p is not null) and
    (nr_prescricao_p is not null) and
    (nr_seq_solucao_p is null) and
    (nr_seq_material_p is null) and
    (nr_seq_procedimento_p is not null) and
    (nr_etapa_p is not null) and    
    (nm_usuario_p is not null) then
    begin
    
    select    max(cd_estabelecimento)
    into    cd_estabelecimento_w
    from    atendimento_paciente
    where    nr_atendimento = nr_atendimento_p;

    select    nvl(max(ie_local_estoque_proc),'P')
    into    ie_local_estoque_proc_w
    from    parametros_farmacia
    where    cd_estabelecimento = cd_estabelecimento_w;    
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 710, 'GERAR_PROCESSO_ADEP_SOL', null, null, obter_desc_expressao(781334) || '01-3'/*'Execucao do comando SQL 01-3'*/);
    end if;
    
    cd_setor_atend_w    := Obter_Unidade_Atendimento(nr_atendimento_p,'IAA','CS');
    if    (ie_local_estoque_proc_w = 'P') then
        begin
        cd_local_estoque_w    := obter_local_estoque_setor(cd_setor_atend_w, cd_estabelecimento_w);
        end;
    elsif    (ie_local_estoque_proc_w = 'S') then
        begin
        select    obter_local_estoque_setor(cd_setor_atendimento, cd_estabelecimento_w)
        into    cd_local_estoque_w
        from    prescr_medica
        where    nr_prescricao = nr_prescricao_p;
        end;
    ELSIF    (ie_local_estoque_proc_w = 'R') THEN
        cd_local_estoque_w    := obter_local_estoque_regra_ged(cd_setor_atend_w);    
    end if;
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 715, 'GERAR_PROCESSO_ADEP_SOL', null, null, obter_desc_expressao(781334) || '02-3'/*'Execucao do comando SQL 02-3'*/);        
    end if;
    
    select    adep_processo_seq.nextval
    into    nr_seq_processo_w
    from    dual;
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 720, 'GERAR_PROCESSO_ADEP_SOL', null, null, 'Insert na tabela ADEP_PROCESSO-3; SEQ='||to_char(nr_seq_processo_w));    
    end if;
    
    select     max(nvl(ie_urgencia,'N')),
        max(cd_intervalo),
        max(ie_acm),
        max(ie_se_necessario)
    into    ie_urgente_w,
        cd_intervalo_w,
        ie_acm_w,
        ie_se_necessario_w
    from    prescr_procedimento
    where    nr_prescricao  = nr_prescricao_p
    and    nr_sequencia = nr_seq_procedimento_p;
    
    insert into adep_processo (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_atendimento,
        nr_prescricao,
        nr_seq_procedimento,
        nr_etapa,
        dt_processo,
        nm_usuario_processo,
        dt_leitura,
        nm_usuario_leitura,
        dt_preparo,
        nm_usuario_preparo,
        dt_paciente,
        nm_usuario_paciente,
        dt_horario_processo,
        cd_local_estoque,
        cd_setor_atendimento,
        cd_funcao_origem,
        ie_origem_processo,
        ie_gedipa,
        ie_urgente,
        cd_intervalo,
        ie_acm,
        ie_se_necessario,
        nr_seq_regra)
    values (
        nr_seq_processo_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_atendimento_p,
        nr_prescricao_p,
        nr_seq_procedimento_p,
        nr_etapa_p,
        sysdate,
        nm_usuario_p,
        null,
        null,
        null,
        null,
        null,
        null,
        sysdate,--dt_horario_processo_p,
        cd_local_estoque_w,
        cd_setor_atend_w,
        cd_funcao_origem_p,
        ie_origem_processo_p,
        ie_gedipa_p,
        ie_urgente_w,
        cd_intervalo_w,
        ie_acm_w,
        ie_se_necessario_w,
        nr_seq_regra_w);
        
    nr_seq_processo_p := nr_seq_processo_w;
    end;
end if;

if (ie_grava_log_gedipa_w = 'S') then
    insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
    values (obter_nextval_sequence('log_gedipa'), sysdate, 725, 'GERAR_PROCESSO_ADEP_SOL', null, 'nr_atendimento_p='||to_char(nr_atendimento_p)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_solucao_p='||to_char(nr_seq_solucao_p)||'; nr_etapa_p='||to_char(nr_etapa_p)||'; dt_horario_processo_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_processo_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone) ||'; nm_usuario_p='||nm_usuario_p||'; nr_seq_processo_p='||to_char(nr_seq_processo_p), 'Termino da execucao da procedure GERAR_PROCESSO_ADEP_SOL');
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

exception 
when others then
    ds_erro_w    := substr(SQLERRM(sqlcode),1,1800);
	
	gravar_log_tasy(7892, 'Prescricao='||nr_prescricao_p||';nr_seq_solucao_p='||nr_seq_solucao_p||';Erro='||ds_erro_w, nm_usuario_p);
    
    if (ie_grava_log_gedipa_w = 'S') then
        insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
        values (obter_nextval_sequence('log_gedipa'), sysdate, 730, 'GERAR_PROCESSO_ADEP_SOL', null, 'nr_atendimento_p='||to_char(nr_atendimento_p)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_solucao_p='||to_char(nr_seq_solucao_p)||'; nr_etapa_p='||to_char(nr_etapa_p)||'; dt_horario_processo_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_processo_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone) ||'; nm_usuario_p='||nm_usuario_p||'; nr_seq_processo_p='||to_char(nr_seq_processo_p), 'Excecao na execucao da procedure GERAR_PROCESSO_ADEP_SOL; E='||ds_erro_w);
    end if;
    
    if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_processo_adep_sol;
/