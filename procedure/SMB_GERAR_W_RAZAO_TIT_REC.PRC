create or replace
PROCEDURE SMB_Gerar_W_Razao_Tit_rec
		(dt_parametro_p   		DATE,
		nm_usuario_p			varchar2,
		ie_considera_cancel_p		varchar2,
		cd_estabelecimento_p		number,
		dt_inicial_p			date,
		dt_final_p			date,
		ie_situacao_p			varchar2,
		cd_empresa_p			number,
		cd_conta_contabil_p		varchar2) IS 

dt_inicio_w		date;
dt_final_w		date;
cd_cgc_w		varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
vl_saldo_ant_w		number(15,2);
vl_saldo_atual_w	number(15,2);
ie_tipo_w		integer;
dt_movimento_w		date;
nr_documento_w		varchar2(255);
vl_movimento_w		number(15,2);
vl_credito_w		number(15,2);
vl_debito_w		number(15,2);
nr_sequencia_w		Number(10,0);
cd_conta_contabil_ww	varchar2(2000);
nr_titulo_ant_w	number(10,0);	

cursor C01 is
	select	distinct 
		cd_pessoa_fisica,
		cd_cgc
	from	titulo_receber
	where	dt_contabil			< fim_dia(dt_final_w)
	and	nvl(dt_liquidacao,sysdate)	>= dt_inicio_w
	and	cd_estabelecimento		= cd_estabelecimento_p;

cursor C02 is
	select	1,
		b.dt_recebimento,
		SUBSTR('Baixa T�tulo: ' || c.nr_titulo || ' / ' || c.nr_nota_fiscal,1,255),
		nvl(vl_recebido + vl_glosa + vl_descontos + nvl(vl_perdas,0),0) vl_movimento
	from 	titulo_receber_liq b,
		titulo_receber c
	where	b.nr_titulo		= c.nr_titulo
	and	(c.cd_cgc		= cd_cgc_w OR 
		c.cd_pessoa_fisica 	= cd_pessoa_fisica_w)
	and	b.dt_recebimento	BETWEEN dt_inicio_w AND fim_dia(dt_final_w)
	and	(c.cd_estabelecimento	= cd_estabelecimento_p)
	and	nvl(c.dt_contabil, c.dt_emissao) 	<= fim_dia(dt_final_w)
	and	((c.ie_situacao not in('5','3') and 'N'	= ie_considera_cancel_p) OR 
	 	 (c.ie_situacao <> '5' and 'S'		= ie_considera_cancel_p))
	and	c.ie_situacao = nvl(ie_situacao_p,c.ie_situacao)
	AND	((cd_conta_contabil_ww LIKE '% ' || (DECODE(c.cd_cgc,NULL,obter_conta_contab_pf(cd_empresa_p,c.cd_pessoa_fisica,'R',dt_final_p), obter_conta_contab_pj(cd_empresa_p,c.cd_estabelecimento,c.cd_cgc,'R',dt_final_p)))|| ' %') OR cd_conta_contabil_p IS NULL)
	union all
	SELECT	3,
		b.dt_alteracao,
		SUBSTR('Altera��o p/ Menor: ' || TO_CHAR(b.nr_titulo),1,255),
		nvl(VL_ALTERACAO,0)
	FROM	alteracao_valor b,
		titulo_receber a
	WHERE	b.dt_alteracao			BETWEEN dt_inicio_w AND fim_dia(dt_final_w)
	and	nvl(a.dt_contabil, a.dt_emissao) 	<= fim_dia(dt_final_w)
	AND	b.ie_aumenta_diminui = 'D'
	AND	a.nr_titulo			= b.nr_titulo
	AND	(a.cd_estabelecimento		= cd_estabelecimento_p)
	AND	(a.cd_cgc 			= cd_cgc_w OR 
		a.cd_pessoa_fisica		= cd_pessoa_fisica_w)
	AND	((a.ie_situacao not in('5','3') AND 'N' = ie_considera_cancel_p) OR 
		 (a.ie_situacao <> '5' AND 'S' = ie_considera_cancel_p))
	and	a.ie_situacao = nvl(ie_situacao_p,a.ie_situacao)	
	AND	((cd_conta_contabil_ww LIKE '% ' || (DECODE(cd_cgc,NULL,obter_conta_contab_pf(cd_empresa_p,cd_pessoa_fisica,'R',dt_final_p), obter_conta_contab_pj(cd_empresa_p,a.cd_estabelecimento,cd_cgc,'R',dt_final_p)))|| ' %') OR cd_conta_contabil_p IS NULL)
	union all
	SELECT	4,
		dt_alteracao,
		SUBSTR('Altera��o p/ Maior: ' || TO_CHAR(b.nr_titulo),1,255),
		nvl((VL_ALTERACAO),0)
	FROM	alteracao_valor b,
		titulo_receber a
	WHERE	b.dt_alteracao			BETWEEN dt_inicio_w AND fim_dia(dt_final_w)
	and	nvl(a.dt_contabil, a.dt_emissao) 	<= fim_dia(dt_final_w)
	AND	b.ie_aumenta_diminui = 'A'
	AND	a.nr_titulo			= b.nr_titulo
	AND	(a.cd_cgc			= cd_cgc_w OR 
		a.cd_pessoa_fisica		= cd_pessoa_fisica_w)
	AND	(a.cd_estabelecimento		= cd_estabelecimento_p)
	AND	((a.ie_situacao not in('3','5') AND 'N' = ie_considera_cancel_p) OR 
		 (a.ie_situacao <> '5' AND 'S' = ie_considera_cancel_p))
	and	a.ie_situacao = nvl(ie_situacao_p,a.ie_situacao)
	AND	((cd_conta_contabil_ww LIKE '% ' || (DECODE(cd_cgc,NULL,obter_conta_contab_pf(cd_empresa_p,cd_pessoa_fisica,'R',dt_final_p), obter_conta_contab_pj(cd_empresa_p,a.cd_estabelecimento,cd_cgc,'R',dt_final_p)))|| ' %') OR cd_conta_contabil_p IS NULL)
	union all
	select	5,
		NVL(dt_contabil, dt_emissao),
		SUBSTR('Inclus�o de T�tulo: ' || nr_titulo || ' / ' || NVL(to_char(nr_documento),nr_nota_fiscal),1,255),
		to_number(nvl(obter_dados_titulo_receber(nr_titulo,'VLL'),0))
	from	titulo_receber a
	where	NVL(dt_contabil, dt_emissao)	BETWEEN dt_inicio_w AND fim_dia(dt_final_w)
	and	(cd_cgc				= cd_cgc_w OR 
		cd_pessoa_fisica		= cd_pessoa_fisica_w)
	and	(a.cd_estabelecimento		= cd_estabelecimento_p)
	and	((ie_situacao not in('5','3') and 'N' = ie_considera_cancel_p) OR 
		 (ie_situacao <> '5' and 'S' = ie_considera_cancel_p))
	and	ie_situacao = nvl(ie_situacao_p,ie_situacao)
	AND	((cd_conta_contabil_ww LIKE '% ' || (DECODE(cd_cgc,NULL,obter_conta_contab_pf(cd_empresa_p,cd_pessoa_fisica,'R',dt_final_p), obter_conta_contab_pj(cd_empresa_p,a.cd_estabelecimento,cd_cgc,'R',dt_final_p)))|| ' %') OR cd_conta_contabil_p IS NULL);

BEGIN

dt_inicio_w	:=  nvl(dt_inicial_p,trunc(dt_parametro_p, 'month'));
dt_final_w	:=  fim_dia(nvl(dt_final_p,trunc(last_day(dt_parametro_p),'dd')));
cd_conta_contabil_ww 	:= ' ' || replace(replace(replace(cd_conta_contabil_p,'(',''),')',''),',',' ') || ' ';

/*select	max(nr_titulo)
into	nr_titulo_ant_w
from	titulo_receber a
where	dt_contabil			< fim_dia(dt_final_w)
and	nvl(dt_liquidacao,sysdate)	>= dt_inicio_w
and	cd_estabelecimento		= cd_estabelecimento_p
and		exists (select	1 
				from 	titulo_receber_liq x 
				where	a.nr_titulo = x.nr_titulo
				and		x.dt_recebimento < trunc(nvl(a.dt_contabil, a.dt_emissao), 'dd'));

if	(nr_titulo_ant_w is not null) then
	--O t�tulo #@NR_TITULO#@ possui baixas com data inferior a data de liquida��o/emiss�o do t�tulo
	Wheb_mensagem_pck.exibir_mensagem_abort(200303,'NR_TITULO='||nr_titulo_ant_w);
end if;*/
	

delete from w_razao_tit_rec;

open C01;
loop
fetch C01 into
	cd_pessoa_fisica_w,
	cd_cgc_w;
exit when C01%notfound;

	select  w_razao_tit_rec_seq.nextval
	into	nr_sequencia_w
	from	dual;

	/* obter saldo anterior */ 
	insert into w_razao_tit_rec 
		(nr_sequencia, 
		cd_pessoa_fisica, 
		cd_cgc, 
		ie_tipo, 
		dt_atualizacao,
		nm_usuario, 
		dt_movimento,
		nr_documento, 
		vl_credito, 
		vl_debito, 
		cd_estabelecimento)
	select	nr_sequencia_w,
		cd_pessoa_fisica_w,
		cd_cgc_w, 
		0,
		sysdate,
		nm_usuario_p,
		dt_inicio_w,
		'Saldo Anterior',
		nvl(sum(obter_saldo_titulo_receber(nr_titulo,dt_inicio_w - 1/86400)),0),
		0,
		cd_estabelecimento_p
	from	titulo_receber
	where	(cd_cgc			= cd_cgc_w or 
		cd_pessoa_fisica	= cd_pessoa_fisica_w)
	and	(cd_estabelecimento	= cd_estabelecimento_p)
	and	nvl(dt_contabil, dt_emissao) 	< dt_inicio_w
	and	((ie_situacao not in('5','3') and 'N' = ie_considera_cancel_p) or 
		 (ie_situacao <> '5' and 'S' = ie_considera_cancel_p))
	and	ie_situacao = nvl(ie_situacao_p,ie_situacao)
	AND	((cd_conta_contabil_ww LIKE '% ' || (DECODE(cd_cgc,NULL,obter_conta_contab_pf(cd_empresa_p,cd_pessoa_fisica,'R',dt_final_p), obter_conta_contab_pj(cd_empresa_p,cd_estabelecimento,cd_cgc,'R',dt_final_p)))|| ' %') OR cd_conta_contabil_p IS NULL);

	open C02;
	loop
	fetch C02 into
		ie_tipo_w,
		dt_movimento_w,
		nr_documento_w,
		vl_movimento_w;
	exit when C02%notfound;

		vl_credito_w := 0;
		vl_debito_w  := 0;

		if (ie_tipo_w <= 3) then	
			vl_debito_w	:= vl_movimento_w;
		else
			vl_credito_w	:= vl_movimento_w;
		end if;

		select  w_razao_tit_rec_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into w_razao_tit_rec 
			(nr_sequencia, 
			cd_pessoa_fisica, 
			cd_cgc, 
			ie_tipo, 
			dt_atualizacao,
			nm_usuario, 
			dt_movimento, 
			nr_documento, 
			vl_credito, 
			vl_debito, 
			cd_estabelecimento)
		values 	(nr_sequencia_w, 
			cd_pessoa_fisica_w, 
			cd_cgc_w, 
			ie_tipo_w, 
			sysdate, 
			nm_usuario_p, 
			dt_movimento_w,
			nr_documento_w, 
			vl_credito_w, 
			vl_debito_w, 
			cd_estabelecimento_p);
	end loop;
	close C02;
	
	select	w_razao_tit_rec_seq.nextval
	into	nr_sequencia_w
	from	dual;

	/* obter saldo atual */
	insert into w_razao_tit_rec
		(nr_sequencia, 
		cd_pessoa_fisica, 
		cd_cgc, 
		ie_tipo, 
		dt_atualizacao,
		nm_usuario, 
		dt_movimento,
		nr_documento, 
		vl_credito, 
		vl_debito, 
		cd_estabelecimento)
	select	nr_sequencia_w,
		cd_pessoa_fisica_w,
		cd_cgc_w, 
		9,
		sysdate,
		nm_usuario_p,
		dt_final_w,
		'Saldo Atual',
		nvl(sum(obter_saldo_titulo_receber(nr_titulo,fim_dia(dt_final_w))),0),
		0,
		cd_estabelecimento_p		
	from 	titulo_receber
	where	(cd_cgc			= cd_cgc_w or 
		cd_pessoa_fisica	= cd_pessoa_fisica_w)
	and	(cd_estabelecimento	= cd_estabelecimento_p)
	and	nvl(dt_contabil, dt_emissao) 	<= fim_dia(dt_final_w)
	and	((ie_situacao not in('5','3') and 'N' = ie_considera_cancel_p) or 
		 (ie_situacao <> '5' and 'S' = ie_considera_cancel_p))
	and	ie_situacao = nvl(ie_situacao_p,ie_situacao)
	AND	((cd_conta_contabil_ww LIKE '% ' || (DECODE(cd_cgc,NULL,obter_conta_contab_pf(cd_empresa_p,cd_pessoa_fisica,'R',dt_final_p), obter_conta_contab_pj(cd_empresa_p,cd_estabelecimento,cd_cgc,'R',dt_final_p)))|| ' %') OR cd_conta_contabil_p IS NULL);
end loop;
close C01;

commit;

END SMB_Gerar_W_Razao_Tit_rec;
/