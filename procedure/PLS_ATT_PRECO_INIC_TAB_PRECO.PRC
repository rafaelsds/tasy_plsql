create or replace
procedure pls_att_preco_inic_tab_preco(	nr_seq_tabela_p		pls_tabela_preco.nr_sequencia%type) is 

Cursor C01 (	nr_seq_tabela_pc	pls_tabela_preco.nr_sequencia%type) is
	select	vl_preco_atual,
		nr_sequencia
	from	pls_plano_preco
	where	nr_seq_tabela = nr_seq_tabela_pc;

begin

if	(nr_seq_tabela_p is not null) then
	for r_c01_w in c01 (nr_seq_tabela_p) loop
		begin
		update	pls_plano_preco
		set	vl_preco_inicial = r_c01_w.vl_preco_atual
		where	nr_sequencia = r_c01_w.nr_sequencia;
		end;
	end loop;
end if;

commit;

end pls_att_preco_inic_tab_preco;
/