create or replace
procedure gravar_painel_perfil(	nr_seq_indicador_p		number,
				ds_indicador_p		varchar2,
				qt_topo_p		number,
				qt_esquerda_p		number,
				qt_largura_p		number,
				qt_altura_p		number,
				ds_cor_p			varchar2,
				ds_tam_campo_p		varchar2,
				nm_atributo_p		varchar2,
				ds_regra_analise_p		varchar2) is 

qt_esquerda_w		number(5);
qt_topo_w		number(5);
				
begin

if (qt_topo_p < 0) then
	qt_topo_w := 0;
else
	qt_topo_w := qt_topo_p;
end if;

if (qt_esquerda_p < 0) then
	qt_esquerda_w := 0;
else
	qt_esquerda_w := qt_esquerda_p;
end if;

insert into regra_painel_controle	(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_indicador,
					ds_indicador,
					qt_topo,
					qt_esquerda,
					qt_largura,
					qt_altura,
					ds_cor)
			values	(	regra_painel_controle_seq.Nextval,
					sysdate,
					'Tasy',
					sysdate,
					'Tasy',
					nr_seq_indicador_p,
					substr(ds_indicador_p,1,80),
					qt_topo_w,
					qt_esquerda_w,
					qt_largura_p,
					qt_altura_p,
					ds_cor_p);

commit;

end gravar_painel_perfil;
/