create or replace
procedure desfazer_liberacao_noms(
						nr_sequencia_p	number,
						nm_tabela_p		varchar2) is 

begin

	if	(upper(nm_tabela_p) = 'GUIA_MORTE_FETAL_NOMS') then
	
		update 	GUIA_MORTE_FETAL_NOMS
		set		dt_liberacao = null,
				nm_usuario_lib = null
		where 	nr_sequencia = nr_sequencia_p;

	elsif	(upper(nm_tabela_p)	= 'GUIA_MORTE_NOMS') then
		
		update 	GUIA_MORTE_NOMS
		set		dt_liberacao = null,
				nm_usuario_lib = null
		where 	nr_sequencia = nr_sequencia_p;
		
	elsif	(upper(nm_tabela_p)	= 'NOM_NASCIMENTO') then
		
		update 	NOM_NASCIMENTO
		set		dt_liberacao = null,
				nm_usuario_lib = null
		where 	nr_sequencia = nr_sequencia_p;
		
	end if;

commit;

end desfazer_liberacao_noms;
/
