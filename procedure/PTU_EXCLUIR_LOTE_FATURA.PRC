create or replace
procedure ptu_excluir_lote_fatura
			(	nr_seq_lote_p	number,
				nm_usuario_p	varchar2) is


nr_seq_lote_w                   number(10);
nr_seq_fatura_w                 number(10);
nr_seq_cobranca_w               number(10);
nr_seq_hospital_w               number(10);

cursor  C02 is
        select  nr_sequencia
        from    ptu_fatura
        where   nr_seq_lote = nr_seq_lote_w;

cursor  C03 is
        select  nr_sequencia
        from    ptu_nota_cobranca
        where   nr_seq_fatura = nr_seq_fatura_w;

cursor  C04 is
        select  nr_sequencia
        from    ptu_nota_hospitalar
        where   nr_seq_nota_cobr = nr_seq_cobranca_w;

begin
select  nr_sequencia
into    nr_seq_lote_w
from    ptu_lote_fatura_envio
where   nr_sequencia = nr_seq_lote_p;

open C02;
loop
fetch C02 into
	nr_seq_fatura_w;
exit when C02%notfound;
	begin
	delete from ptu_fatura_historico
	where	nr_seq_fatura = nr_seq_fatura_w;
	
	update	pls_conta_medica_resumo
	set     nr_seq_fatura = null
	where   nr_seq_fatura = nr_seq_fatura_w
	and	ie_situacao = 'A';
	
	update	pls_conta
	set     nr_seq_fatura = null
	where   nr_seq_fatura = nr_seq_fatura_w;
	open C03;
	loop
	fetch C03 into
		nr_seq_cobranca_w;
	exit when C03%notfound;
		begin
		delete from ptu_nota_servico where nr_seq_nota_cobr = nr_seq_cobranca_w;

		delete from ptu_nota_complemento where nr_seq_nota_cobr = nr_seq_cobranca_w;

		delete from ptu_nota_fiscal where nr_seq_nota_cobr = nr_seq_cobranca_w;

		open C04;
		loop
		fetch C04 into
			nr_seq_hospital_w;
		exit when C04%notfound;
			begin
			delete from ptu_nota_hosp_compl where nr_seq_nota_hosp = nr_seq_hospital_w;
			end;
		end loop;
		close C04;
		end;
		delete from ptu_nota_hospitalar where nr_seq_nota_cobr = nr_seq_cobranca_w;
	end loop;
	close C03;
	delete from ptu_nota_cobranca where nr_seq_fatura = nr_seq_fatura_w;

	delete from ptu_fatura_boleto where nr_seq_fatura = nr_seq_fatura_w;

	delete from ptu_fatura_boleto where nr_seq_fatura = nr_seq_fatura_w;

	delete from ptu_fatura_corpo where nr_seq_fatura = nr_seq_fatura_w;

	delete from ptu_fatura_cedente where nr_seq_fatura = nr_seq_fatura_w;
	
	delete from ptu_fatura_conta_exc where nr_seq_fatura = nr_seq_fatura_w;
	
	update  pls_conta
	set     cd_cooperativa = null
	where   cd_cooperativa = '0';
	end;
end loop;
close C02;

delete from ptu_a500_historico where nr_seq_lote = nr_seq_lote_w;

delete from ptu_fatura where nr_seq_lote = nr_seq_lote_w;

update	ptu_lote_fatura_envio
set	dt_geracao_lote		= null,
	dt_geracao_titulo	= null,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_lote_p;

commit;

end ptu_excluir_lote_fatura;
/