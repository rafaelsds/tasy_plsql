create or replace
procedure copiar_autor_cirurgia(nr_sequencia_p		number,
				nr_seq_agenda_nova_p	number,
				nm_usuario_p		varchar2) is

nr_sequencia_w		number(10);
nr_seq_mat_autor_w	number(10);
nr_seq_mat_novo_w	number(10);
ie_estagio_autor_transf_w 	varchar2(2);

Cursor c01 is
select	nr_sequencia
from	material_autor_cirurgia
where	nr_seq_autorizacao	= nr_sequencia_p;

begin

select	nvl(max(ie_estagio_autor_transf),'1')
into	ie_estagio_autor_transf_w
from	parametro_faturamento
where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;

select	autorizacao_cirurgia_seq.nextval
into	nr_sequencia_w
from	dual;


insert	into	autorizacao_cirurgia
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_pedido,
	nm_usuario_pedido,
	ds_justificativa,
	dt_autorizacao,
	nm_autorizante,
	nm_usuario_autorizacao,
	nr_seq_agenda,
	cd_requisitante,
	ds_observacao,
	nr_atendimento,
	cd_procedimento,
	ie_estagio_autor,
	ie_origem_proced,
	dt_previsao,
	ds_motivo_negativa,
	dt_liberacao,
	dt_fim_cotacao,
	nr_prescricao,
	nr_doc_convenio,
	cd_estabelecimento,
	cd_comprador,
	cd_senha,
	dt_liberacao_autor,
	cd_pessoa_fisica)
select	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	dt_pedido,
	nm_usuario_pedido,
	ds_justificativa,
	decode(ie_estagio_autor_transf_w,'3',dt_autorizacao,'7',dt_autorizacao,null),
	decode(ie_estagio_autor_transf_w,'3',nm_autorizante,'7',nm_autorizante,null),
	decode(ie_estagio_autor_transf_w,'3',nm_usuario_autorizacao,'7',nm_usuario_autorizacao,null),
	nr_seq_agenda_nova_p,
	cd_requisitante,
	ds_observacao,
	nr_atendimento,
	cd_procedimento,
	ie_estagio_autor_transf_w,
	ie_origem_proced,
	dt_previsao,
	decode(ie_estagio_autor_transf_w,'5',ds_motivo_negativa,null),
	decode(ie_estagio_autor_transf_w,'3',dt_liberacao,'7',dt_liberacao,null),
	dt_fim_cotacao,
	nr_prescricao,
	nr_doc_convenio,
	cd_estabelecimento,
	cd_comprador,
	cd_senha,
	decode(ie_estagio_autor_transf_w,'3',dt_liberacao_autor,'7',dt_liberacao_autor,null),
	cd_pessoa_fisica
from	autorizacao_cirurgia
where	nr_sequencia	= nr_sequencia_p;

insert	into	proced_autor_cirurgia
	(nr_sequencia,
	nr_seq_autorizacao,
	cd_procedimento,
	ie_origem_proced,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	qt_procedimento)
select	proced_autor_cirurgia_seq.nextval,
	nr_sequencia_w,
	cd_procedimento,
	ie_origem_proced,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	qt_procedimento
from	proced_autor_cirurgia
where	nr_seq_autorizacao	= nr_sequencia_p;


open c01;
loop
fetch c01 into
	nr_seq_mat_autor_w;
exit when c01%notfound;
	
	select	material_autor_cirurgia_seq.nextval
	into	nr_seq_mat_novo_w
	from	dual;

	insert	into	material_autor_cirurgia
		(nr_sequencia,
		nr_seq_autorizacao,
		dt_atualizacao,
		nm_usuario,
		cd_material,
		qt_material,
		ds_observacao,
		vl_material,
		ie_aprovacao,
		vl_unitario_material,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_cgc_fornec,
		qt_solicitada,
		nr_seq_fabricante,
		ie_origem_preco,
		nr_seq_opme)
	select	nr_seq_mat_novo_w,
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		a.cd_material,
		a.qt_material,
		a.ds_observacao,
		a.vl_material,
		a.ie_aprovacao,
		a.vl_unitario_material,
		sysdate,
		nm_usuario_p,
		a.cd_cgc_fornec,
		a.qt_solicitada,
		a.nr_seq_fabricante,
		a.ie_origem_preco,
		nvl((	select	max(x.nr_sequencia)
			from	agenda_pac_opme x
			where	x.nr_seq_agenda = nr_seq_agenda_nova_p
			and	x.cd_material = a.cd_material
			and	not exists(	select	1
						from	material_autor_cirurgia w
						where	w.nr_seq_opme = x.nr_sequencia)),a.nr_seq_opme)
	from	material_autor_cirurgia a
	where	a.nr_sequencia	= nr_seq_mat_autor_w;

	insert	into	material_autor_cir_cot
		(nr_sequencia,
		cd_cgc,
		dt_atualizacao,
		nm_usuario,
		vl_cotado,
		cd_condicao_pagamento,
		vl_unitario_cotado,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_aprovacao)
	select	nr_seq_mat_novo_w,
		cd_cgc,
		sysdate,
		nm_usuario_p,
		vl_cotado,
		cd_condicao_pagamento,
		vl_unitario_cotado,
		sysdate,
		nm_usuario_p,
		ie_aprovacao
	from	material_autor_cir_cot
	where	nr_sequencia	= nr_seq_mat_autor_w;
end loop;
close c01;

commit;

end;
/