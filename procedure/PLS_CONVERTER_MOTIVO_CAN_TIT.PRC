/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a convers�o do motivo de cancelmaneto
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_converter_motivo_can_tit
			(	nr_seq_motivo_can_tit_p		number,
				nr_seq_motivo_can_conver_p out	number,
				nm_usuario_p			Varchar2) is 
			
nr_seq_motivo_can_conver_w	number(10);

begin

select	max(NR_SEQ_MOTIVO_RESCISAO)
into	nr_seq_motivo_can_conver_w
from	PLS_REGRA_MOTIVO_RESCISAO
where	NR_SEQ_MOTIVO_RESCISAO_TIT	= nr_seq_motivo_can_tit_p
and	ie_situacao			= 'A';

nr_seq_motivo_can_conver_p	:= nr_seq_motivo_can_conver_w;

end pls_converter_motivo_can_tit;
/
