create or replace
procedure obter_valores_cpoe_suep(
			nr_atendimento_p	Varchar2,
			qt_total_p			out	number,
			qt_nutricao_p		out	number,
			qt_medicamentos_p	out	number,
			qt_procedimentos_p	out	number,
			qt_gaso_p			out	number,
			qt_recomendacao_p	out	number,
			qt_hemoterapia_p	out	number,
			qt_dialise_p		out	number,
			qt_intervencoes_p	out	number,
			qt_materiais_p		out	number,
			qt_proc_anatomo_p	out	number,
			nr_seq_item_p number default null) is 
			
begin

select	count(*)
into	qt_nutricao_p
from	prescr_medica a,
		prescr_dieta b,
		cpoe_dieta c
where	a.nr_prescricao = b.nr_prescricao
and		b.nr_seq_dieta_cpoe = c.nr_sequencia
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		a.dt_suspensao 		is null
and		c.DT_LIB_SUSPENSAO 	is null
and		c.DT_SUSPENSAO 		is null
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_medicamentos_p
from	prescr_medica a,
		prescr_material b,
		cpoe_material c left join informacao_suep s on nr_seq_item = nr_seq_item_p
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_mat_cpoe 	= c.nr_sequencia
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		nvl(ie_material,'N') = 'N'
and		b.ie_agrupador in (1,4)
and		a.dt_suspensao 		is null
and		c.DT_LIB_SUSPENSAO 	is null
and		c.DT_SUSPENSAO 		is null
and (c.cd_material = s.cd_material or s.cd_material is null)
and (obter_familia_material(c.cd_material) = s.nr_seq_familia or s.nr_seq_familia is null)
and (obter_dados_material(c.cd_material, 'CGRU') = s.cd_grupo_material or s.cd_grupo_material is null)
and (obter_dados_material(c.cd_material, 'CSUB') = s.cd_subgrupo_material or s.cd_subgrupo_material is null)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_procedimentos_p
from	prescr_medica a,
		prescr_procedimento b,
		cpoe_procedimento c
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_proc_cpoe 	= c.nr_sequencia
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_gaso_p
from	prescr_medica a,
		prescr_gasoterapia b,
		cpoe_gasoterapia c
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_gas_cpoe 	= c.nr_sequencia
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_recomendacao_p
from	prescr_medica a,
		prescr_recomendacao b,
		cpoe_recomendacao c
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_rec_cpoe 	= c.nr_sequencia
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_hemoterapia_p
from	prescr_medica a,
		prescr_solic_bco_sangue b,
		cpoe_hemoterapia c
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_hemo_cpoe 	= c.nr_sequencia
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_dialise_p
from	prescr_medica a,
		hd_prescricao b,
		cpoe_dialise c
where	a.nr_prescricao 	= b.nr_prescricao
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1)
and		b.nr_seq_dialise_cpoe = c.nr_sequencia;

select	count(*)
into	qt_intervencoes_p
from	pe_prescricao a,
		pe_prescr_proc b,
		intervalo_prescricao x,
		cpoe_intervencao c
where	a.nr_sequencia 		= b.nr_seq_prescr
and		b.nr_seq_cpoe_interv = c.nr_sequencia
and		b.cd_intervalo 		= x.cd_intervalo(+)
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= NVL(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_materiais_p
from	prescr_medica a,
		prescr_material b,
		cpoe_material c left join informacao_suep s on nr_seq_item = nr_seq_item_p
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_mat_cpoe 	= c.nr_sequencia
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and (c.cd_material = s.cd_material or s.cd_material is null)
and (obter_familia_material(c.cd_material) = s.nr_seq_familia or s.nr_seq_familia is null)
and (obter_dados_material(c.cd_material, 'CGRU') = s.cd_grupo_material or s.cd_grupo_material is null)
and (obter_dados_material(c.cd_material, 'CSUB') = s.cd_subgrupo_material or s.cd_subgrupo_material is null)
and		a.nr_atendimento 	= nvl(nr_atendimento_p,0)
and		ie_material 		= 'S'
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

select	count(*)
into	qt_proc_anatomo_p
from	prescr_medica a,
		prescr_procedimento b,
		cpoe_anatomia_patologica c
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_proc_cpoe 	= c.nr_sequencia
and		a.dt_suspensao 		is null
and		c.dt_lib_suspensao 	is null
and		c.dt_suspensao 		is null
and		a.nr_atendimento 	= nvl(nr_atendimento_p,0)
and		sysdate between a.dt_inicio_prescr and nvl(a.dt_validade_prescr,sysdate + 1);

qt_total_p := qt_nutricao_p + qt_medicamentos_p + qt_procedimentos_p + qt_gaso_p + qt_recomendacao_p + qt_hemoterapia_p + qt_dialise_p + qt_intervencoes_p + qt_materiais_p + qt_proc_anatomo_p;


commit;

end obter_valores_cpoe_suep;
/
