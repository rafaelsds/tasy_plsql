create or replace
procedure ingoh_san_gerar_codigo_int	(nr_seq_doacao_p	number)
				is 

cd_barras_w		varchar2(20);
nr_seq_derivado_w	number(10);

Cursor C01 is
	select	replace('4'||to_char(0,'000')||to_char(x.nr_sequencia ,'0000000')||to_char(x.nr_seq_derivado,'00'),' ',''),
		x.nr_sequencia
	from	san_producao x
	where	x.nr_seq_doacao = nr_seq_doacao_p;

begin
if (nr_seq_doacao_p is not null) then
	
	open C01;
	loop
	fetch C01 into	
		cd_barras_w,
		nr_seq_derivado_w;
	exit when C01%notfound;
		begin
		update	san_producao
		set	cd_barras	= cd_barras_w
		where	nr_seq_doacao	= nr_seq_doacao_p
		and	nr_sequencia	= nr_seq_derivado_w;
		end;
	end loop;
	close C01;	
 	
end if;

commit;

end ingoh_san_gerar_codigo_int;
/