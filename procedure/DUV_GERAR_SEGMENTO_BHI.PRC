create or replace procedure duv_gerar_segmento_bhi(nr_seq_mensagem_p in duv_mensagem.nr_sequencia%type,
                                                   nm_usuario_p      in usuario.nm_usuario%type,
                                                   nr_seq_episodio_p in episodio_paciente.nr_sequencia%type)is
                                                   
  cursor c01 is
   select null nr_seq_episodio,
          null ie_necessita_teste_estresse, 
          null dt_esperada, 
          null ie_necessita_medicao_futura, 
          null ds_medicao_futura, 
          null ie_doencas_psicologicas, 
          null ds_doencas_psicologicas, 
          null ie_medidas_especiais, 
          null ds_medidas_especiais, 
          null ie_precaucoes_retorno_atidade, 
          null ds_motivo_precaucoes, 
          null ie_julgamento_parcial
   from dual;

  c01_w c01%rowtype;                                                   
                                                   
begin 
  c01_w := null;
  open c01;
  fetch c01 into c01_w;
  close c01;
    insert into duv_bhi (nr_sequencia, 
                         dt_atualizacao, 
                         nm_usuario, 
                         dt_atualizacao_nrec, 
                         nm_usuario_nrec, 
                         nr_seq_mensagem, 
                         ie_necessita_teste_estresse, 
                         dt_esperada, 
                         ie_necessita_medicao_futura, 
                         ds_medicao_futura, 
                         ie_doencas_psicologicas, 
                         ds_doencas_psicologicas, 
                         ie_medidas_especiais, 
                         ds_medidas_especiais, 
                         ie_precaucoes_retorno_atidade, 
                         ds_motivo_precaucoes, 
                         ie_julgamento_parcial) values (duv_bhi_seq.nextval,
                                                        sysdate,
                                                        nm_usuario_p,
                                                        sysdate,
                                                        nm_usuario_p,
                                                        nr_seq_mensagem_p,
                                                        c01_w.ie_necessita_teste_estresse, 
                                                        c01_w.dt_esperada, 
                                                        c01_w.ie_necessita_medicao_futura, 
                                                        c01_w.ds_medicao_futura, 
                                                        c01_w.ie_doencas_psicologicas, 
                                                        c01_w.ds_doencas_psicologicas, 
                                                        c01_w.ie_medidas_especiais, 
                                                        c01_w.ds_medidas_especiais, 
                                                        c01_w.ie_precaucoes_retorno_atidade, 
                                                        c01_w.ds_motivo_precaucoes, 
                                                        c01_w.ie_julgamento_parcial);
end duv_gerar_segmento_bhi;
/