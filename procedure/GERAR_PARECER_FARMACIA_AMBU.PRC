create or replace
procedure gerar_parecer_farmacia_ambu	(	nr_seq_encaminhamento_p	number,
											ds_conteudo_p			varchar2,
											nr_seq_regulacao_p		number,
											nr_seq_parecer_p 	out number) is 


cd_pessoa_fisica_w	cpoe_procedimento.cd_pessoa_fisica%type;
nr_atendimento_w	cpoe_procedimento.nr_atendimento%type;
dt_avaliacao_w		cpoe_procedimento.dt_liberacao%type;
ds_titulo_w			varchar2(255);

nr_seq_parecer_w	parecer_medico_req.nr_parecer%type;

nm_usuario_w		usuario.nm_usuario%type;
cd_pf_usuario_w		usuario.cd_pessoa_fisica%type;

ds_fonte_w			varchar2(100);
ds_tam_fonte_w		varchar2(10);
nr_tam_fonte_w		number(5,0);

ds_cabecalho_w		varchar2(32000);
ds_conteudo_w		varchar2(32000);
ds_rodape_w		 	varchar2(32000);
ds_titulo_rft_w 	varchar2(255) := '';
ds_enter_w			varchar2(10)	:=  '\par';

ds_parecer_w		varchar2(32000);

enviar_CI_w			varchar2(1);
enviar_email_w		varchar2(1);

					
begin

	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
	cd_pf_usuario_w := obter_pessoa_fisica_usuario(nm_usuario_w, 'C');

	select	max(b.cd_pessoa_fisica),
			max(b.nr_atendimento),
			max(b.dt_liberacao)
	into	cd_pessoa_fisica_w,
			nr_atendimento_w,
			dt_avaliacao_w
	from	fa_receita_farmacia_item a,
			fa_receita_farmacia b
	where 	a.nr_seq_receita = b.nr_sequencia
	and     a.nr_sequencia = nr_seq_encaminhamento_p;

	if (cd_pessoa_fisica_w is not null) then
	
		ds_conteudo_w := ds_conteudo_p;	

		Select 	obter_desc_expressao(941912) --Solicitação de Medicamento
		into	ds_titulo_w
		from	dual;
		
		
		if (ds_titulo_w is not null) then			
			ds_titulo_w := '<b>' ||  '<center>' || ds_titulo_w || '</b>'||'</center>' || '<br>'|| '<br>';				
		end if;
		

		ds_parecer_w	:= ds_titulo_w ||ds_conteudo_w;	
		
		select	parecer_medico_req_seq.nextval
		into	nr_seq_parecer_w
		from	dual;
		
		insert into parecer_medico_req (
							nr_parecer,
							nr_atendimento,
							cd_medico,
							cd_especialidade,
							dt_atualizacao,
							nm_usuario,
							ds_motivo_consulta,
							dt_liberacao,
							cd_perfil_ativo,
							ie_situacao,
							cd_pessoa_fisica,
							ie_tipo_parecer,
							nm_usuario_nrec,
							dt_atualizacao_nrec
						) values (
							nr_seq_parecer_w,
							nr_atendimento_w,
							cd_pf_usuario_w,
							obter_especialidade_medico(cd_pf_usuario_w, 'C'),
							sysdate,
							nm_usuario_w,
							ds_parecer_w,
							sysdate,
							wheb_usuario_pck.get_cd_perfil,
							'A',
							cd_pessoa_fisica_w,
							'M',
							nm_usuario_w,
							sysdate
						);
						
		nr_seq_parecer_p := nr_seq_parecer_w;
		
		commit;
	

		Obter_Param_Usuario(281, 361, obter_perfil_ativo, nm_usuario_w, wheb_usuario_pck.get_cd_estabelecimento, enviar_CI_w); 
		if (enviar_CI_w <> 'N') then
			begin
				enviar_comunic_parecer_html(nr_seq_parecer_w,1, wheb_usuario_pck.get_cd_estabelecimento, 0, nm_usuario_w);
			Exception when others then
				null;
				
			end;
		end if;
		
		Obter_Param_Usuario(281, 1167, obter_perfil_ativo, nm_usuario_w, wheb_usuario_pck.get_cd_estabelecimento, enviar_email_w);
		if (enviar_email_w <> 'N') then
			begin
				enviar_email_parecer_med(nr_seq_parecer_w,1, wheb_usuario_pck.get_cd_estabelecimento, 0, nm_usuario_w);
		Exception when others then
				null;
			end;
		end if;
		
		
	end if;
	
end gerar_parecer_farmacia_ambu;
/