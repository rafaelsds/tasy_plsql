create or replace
procedure pls_atualisa_estipulante_web(	nr_seq_estipulante_p	pls_estipulante_web.nr_sequencia%type,
				ds_tec_p		pls_estipulante_web.ds_tec%type,
				ds_senha_p		pls_estipulante_web.ds_senha%type) is 

begin

update 	pls_estipulante_web
set	ds_senha = ds_senha_p,
	ds_tec = ds_tec_p
where	nr_sequencia = nr_seq_estipulante_p;

commit;

end pls_atualisa_estipulante_web;
/