create or replace
procedure OHKIS_DEFINIR_ESTRATEGIA_ATRIB(
			nm_tabela_p			Varchar2,
			nm_atributo_p		Varchar2,
			ie_informacao_sensivel_p	varchar2,
			nr_sequencia_p	out	number) is 

nm_tabela_w			tabela_atributo.nm_tabela%type;
nm_atributo_w		tabela_atributo.nm_atributo%type;
ie_obrigatorio_w	tabela_atributo.ie_obrigatorio%type;
ie_tipo_atributo_w	tabela_atributo.ie_tipo_atributo%type;
qt_tamanho_w		tabela_atributo.qt_tamanho%type;
ie_componente_w		tabela_atributo.IE_COMPONENTE%type;
ie_condicao_w		varchar2(4);
ie_integridade_w	number(1);
ie_pk_w			number(1);
ie_uk_w			number(1);
ie_flag_w		varchar2(1) := 'N';
nr_sequencia_w		number(10);	



ds_valor_w 			varchar2(2000);
ds_sep_bv_w			varchar2(20);
qt_verdade_w		number(10);
ds_comando_w		varchar2(2000);
ds_parametro_w  	varchar2(2000);



Cursor C01 is
select  a.nm_tabela,
	    a.nm_atributo,
	    a.ie_obrigatorio,
	    a.ie_tipo_atributo,
		a.qt_tamanho,
		nvl((select max(1) from integridade_atributo c where c.nm_tabela = a.nm_tabela and c.nm_atributo = a.nm_atributo),0) ie_integridade,
		NVL((SELECT MAX(1) FROM INDICE_ATRIBUTO b, INDICE d WHERE d.nm_tabela = b.nm_tabela AND d.nm_indice = b.nm_indice AND d.nm_tabela = a.nm_tabela AND b.nm_atributo = a.nm_atributo AND d.IE_TIPO in ('PK')),0) ie_pk,
		NVL((SELECT MAX(1) FROM INDICE_ATRIBUTO b, INDICE d WHERE d.nm_tabela = b.nm_tabela AND d.nm_indice = b.nm_indice AND d.nm_tabela = a.nm_tabela AND b.nm_atributo = a.nm_atributo AND d.IE_TIPO in ('UK')),0) ie_uk,
		ie_componente		
from	tabela_atributo a,
	tabela_sistema b
where	a.nm_tabela = b.nm_tabela
and	b.ie_temporaria not in ('G','GD')
and	(a.ie_informacao_sensivel = 'S' or ie_informacao_sensivel_p = 'S')
and	a.nm_tabela = nm_tabela_p
and	a.nm_atributo = nm_atributo_p
order	by a.nm_tabela, a.nm_atributo;

begin

open C01;
loop
fetch C01 into	
	nm_tabela_w,
	nm_atributo_w,
	ie_obrigatorio_w,
	ie_tipo_atributo_w,
	qt_tamanho_w,
	ie_integridade_w,
	ie_pk_w,
	ie_uk_w,
	ie_componente_w;
exit when C01%notfound;
	begin	
	/*
	SUB_RD /  Substitui��o - Datas Aleat�rias
	*/	
	if	(ie_tipo_atributo_w = 'DATE') and -- Para campos DATE que n�o sejam FK ou PK da tabela
		(ie_flag_w = 'N') and 
		(ie_pk_w = 0) and
		(ie_integridade_w = 0) then
		begin
		
		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	decharacterization_rule
		where	nr_strategy = 15
		and 	ie_situacao = 'A';
		
		ie_flag_w := 'S';
		
		end;
	end if;	
	/*
	NULL / Anula��o		
	*/
	--verifica_regra_descaracte(nm_tabela_p,nm_atributo_p,23,ie_componente_w,ie_condicao_w);	
	OHKIS_obter_regra_estrategia(nm_tabela_p,nm_atributo_p,23,ie_componente_w,ie_condicao_w);	
	
	if	(ie_tipo_atributo_w in ('VARCHAR2','NUMBER','LONG RAW','BLOB','CLOB','LONG')) and
		(ie_obrigatorio_w = 'N') and -- N�o seta nulo para campos obrigat�rios
		(ie_pk_w = 0) and -- N�o seta nulo para campos que s�o pks
		(ie_uk_w = 0) and -- N�o seta nulo para campos que s�o uks
		(ie_integridade_w = 0) and -- N�o seta nulo para campo que s�o integridade
		(ie_condicao_w = 'S') and -- Verifica se o atributo est� na regra
		(ie_flag_w = 'N')	then 		  
		begin
		
		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	decharacterization_rule
		where	nr_strategy = 23
		and 	ie_situacao = 'A';
				
		ie_flag_w := 'S';
		ie_condicao_w := 'N';
		
		end;
	end if;
	/*
	SHUFFLE / Embaralhamento / Normal
	*/
	OHKIS_obter_regra_estrategia(nm_tabela_p,nm_atributo_p,24,ie_componente_w,ie_condicao_w);
	
	if  ((ie_tipo_atributo_w = 'VARCHAR2') or
		(ie_tipo_atributo_w = 'NUMBER') or
		(ie_tipo_atributo_w = 'DATE')) and
		((ie_condicao_w = 'S') or
		((ie_tipo_atributo_w = 'VARCHAR2') and (qt_tamanho_w < 50))) and
		(ie_integridade_w = 0) and -- Para integridades,  manter o sistema usual
		(ie_pk_w = 0) and
		(ie_uk_w = 0) and
		(ie_flag_w = 'N')  then
		begin
		
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	decharacterization_rule a
		where	a.nr_strategy = 24
		and 	a.ie_situacao = 'A'
		and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'cascadeReferences' and DS_VALUE = 'false');
		
		ie_flag_w := 'S';
		ie_condicao_w := 'N';
		
		end;
	end if;	
	/*
	SHUFFLE / Embaralhamento / Cascade
	*/
	
	if	((ie_tipo_atributo_w = 'VARCHAR2') or
		(ie_tipo_atributo_w = 'NUMBER') or
		(ie_tipo_atributo_w = 'DATE')) and
		((ie_pk_w = 1) or 
		((ie_uk_w = 1) and (ie_tipo_atributo_w <> 'DATE'))or 
		(ie_integridade_w = 1)) and -- Somente para PKS e FKs
		(ie_flag_w = 'N')  then
		begin
		
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	decharacterization_rule a
		where	a.nr_strategy = 24
		and 	a.ie_situacao = 'A'
		and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'cascadeReferences' and DS_VALUE = 'true');
		
		ie_flag_w := 'S';
		
		end;	
	end if;			
	/*
	SUB_RB / Substitui��o - Boleanos Aleat�rios
	*/
	
	/*
	SUB_RN / Substitui��o - N�meros Aleat�rios
	*/
	if 	(ie_tipo_atributo_w = 'NUMBER') AND 
		(ie_pk_w = 0) and -- N�o pode primary key
		(ie_uk_w = 0) and -- N�o pode primary key
		(ie_integridade_w = 0) and -- N�o pode gerar para integridades
		(ie_flag_w = 'N')  then --N�o pode campo que tem integridade
		begin
			
		
		if	 (qt_tamanho_w < 9) then
		
			select	max(a.nr_sequencia)
			into	nr_sequencia_w
			from	decharacterization_rule a
			where	a.nr_strategy = 14
			and 	a.ie_situacao = 'A'
			and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'maxValue' and DS_VALUE = '9');
		
		else
			
			select	max(a.nr_sequencia)
			into	nr_sequencia_w
			from	decharacterization_rule a
			where	a.nr_strategy = 14
			and 	a.ie_situacao = 'A'
			and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'maxValue' and DS_VALUE = '99999');
		
		end if;

		
		ie_flag_w := 'S';
		
		
		end;
	end if;		
	/*
	CUSTOM / Algoritmo customizado
	*/		
	OHKIS_obter_regra_estrategia(nm_tabela_p,nm_atributo_p,16,ie_componente_w,ie_condicao_w);
	
	if	(ie_tipo_atributo_w in ('LONG RAW','BLOB')) and
		(ie_pk_w = 0) and -- N�o pode gerar para PKs
		(ie_uk_w = 0) and -- N�o pode gerar para UKs
		(ie_integridade_w = 0) and -- N�o pode gerar para integridades
		(ie_obrigatorio_w = 'S') and
		(ie_condicao_w = 'S') and
		(ie_flag_w = 'N')  then		
		begin
		
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	decharacterization_rule a
		where	a.nr_strategy = 16
		and 	a.ie_situacao = 'A';
		
		end;
	end if;	
	/*
	SUB_RC / Substitui��o - Caracteres Aleat�rios
	*/
	if	(ie_tipo_atributo_w in ('VARCHAR2','LONG','CLOB')) and
		(ie_pk_w = 0) and -- N�o pode gerar para PKs
		(ie_uk_w = 0) and -- N�o pode gerar para UKs
		(ie_integridade_w = 0) and -- N�o pode gerar para integridades
		(ie_flag_w = 'N')  then		
		begin

		
		if	 (qt_tamanho_w <= 255) then
		
			select	max(a.nr_sequencia)
			into	nr_sequencia_w
			from	decharacterization_rule a
			where	a.nr_strategy = 13
			and 	a.ie_situacao = 'A'
			and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'maxChars' and DS_VALUE = '50');
		
		elsif	 (qt_tamanho_w <= 1000) then
			
			select	max(a.nr_sequencia)
			into	nr_sequencia_w
			from	decharacterization_rule a
			where	a.nr_strategy = 13
			and 	a.ie_situacao = 'A'
			and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'maxChars' and DS_VALUE = '255');

		elsif	 (qt_tamanho_w <= 2000) then
			
			select	max(a.nr_sequencia)
			into	nr_sequencia_w
			from	decharacterization_rule a
			where	a.nr_strategy = 13
			and 	a.ie_situacao = 'A'
			and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'maxChars' and DS_VALUE = '1000');

		elsif	((qt_tamanho_w <= 4000) or 
				(ie_tipo_atributo_w in ('LONG','CLOB'))) then
			
			select	max(a.nr_sequencia)
			into	nr_sequencia_w
			from	decharacterization_rule a
			where	a.nr_strategy = 13
			and 	a.ie_situacao = 'A'
			and 	exists (select 1 from decharacterization_param b where a.nr_sequencia = b.nr_dechar_rule and b.DS_NAME = 'maxChars' and DS_VALUE = '4000');
		
		end if;		
		
		ie_flag_w := 'S';
				
		end;
	end if;
		
	ie_flag_w := 'N';	
	end;		
end loop;
close C01;

nr_sequencia_p :=  nr_sequencia_w;


end OHKIS_definir_estrategia_atrib;
/
