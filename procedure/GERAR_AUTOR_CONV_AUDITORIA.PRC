create or replace 
procedure gerar_autor_conv_auditoria(	nr_interno_conta_p	in number,
				nr_seq_auditoria_p	in number,
				nr_seq_procpaci_p	in number,
				nr_seq_matpaci_p	in number,
				nm_usuario_p	in varchar2) is

cd_convenio_w		number(5);
cd_estabelecimento_w	number(4);
cd_material_w		number(6);
cd_procedimento_w	number(15);
cd_tipo_acomodacao_w	number(10,0);
ds_observacao_w		varchar2(4000);
ie_carater_int_tiss_w	varchar2(2);
ie_origem_proced_w	number(10);
ie_tipo_autorizacao_w	varchar2(2);
ie_tipo_guia_w		varchar2(2);
nr_atendimento_w		number(10);
nr_seq_autorizacao_w	number(10);
nr_seq_estagio_w		number(10);
nr_seq_proc_interno_w	number(10);	
nr_sequencia_autor_w	number(10);
nr_sequencia_proc_w	number(10);
nr_sequencia_mat_w	number(10);
qt_procedimento_w		number(15,3);
qt_material_w		number(15,3);
vl_unit_material_w		number(17,4);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_autor_nova_w	number(10);


begin

nr_sequencia_autor_w	:= null;
nr_seq_autorizacao_w	:= null;
nr_sequencia_proc_w	:= null;
nr_sequencia_mat_w	:= null;

select	a.cd_convenio_parametro,
	a.nr_atendimento,
	a.cd_estabelecimento,
	b.cd_pessoa_fisica
into	cd_convenio_w,
	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w
from 	conta_paciente a,
	atendimento_paciente b
where	nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento;

select	decode(max(a.ie_carater_inter_sus),'1','E','U'),
	max(b.cd_tipo_acomodacao)
into	ie_carater_int_tiss_w,
	cd_tipo_acomodacao_w
from	atend_categoria_convenio b,
	atendimento_paciente a	
where	b.nr_atendimento	= a.nr_atendimento
and	b.nr_seq_interno	= obter_atecaco_atendimento(a.nr_atendimento)
and	a.nr_atendimento	= nr_atendimento_w;


if	(nvl(nr_seq_procpaci_p,0) > 0) then

	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,		
		qt_procedimento
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		qt_procedimento_w
	from 	procedimento_paciente
	where	nr_sequencia		= nr_seq_procpaci_p;
		
	ie_tipo_autorizacao_w	:= '3';

elsif	(nvl(nr_seq_matpaci_p,0) > 0) then

	select	cd_material,
		qt_material
	into	cd_material_w,
		qt_material_w
	from 	material_atend_paciente
	where	nr_sequencia	= nr_seq_matpaci_p;
	
	ie_tipo_autorizacao_w	:= '4';
	
end if;

select	max(ie_tipo_guia_padrao),
	max(nr_seq_estagio_autor)
into	ie_tipo_guia_w,
	nr_seq_estagio_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(nr_seq_estagio_w is null) then
	begin
	select	min(nr_sequencia)
	into	nr_seq_estagio_w
	from	estagio_autorizacao
	where	ie_situacao	= 'A'
	and	ie_interno		= '1'
	and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
	exception
	when no_data_found then
		/*r.aise_application_error(-20011, 'N�o existe um est�gio de necessidade de autoriza��o cadastrado!' ||
				 chr(13) || 'Verfique o cadastro de est�gio da autoriza��o do conv�nio.');*/
		wheb_mensagem_pck.exibir_mensagem_abort(263327);
	end;
end if;

select	max(a.nr_sequencia),
	max(a.nr_seq_autorizacao)
into	nr_sequencia_autor_w,
	nr_seq_autorizacao_w
from 	autorizacao_convenio a,
	estagio_autorizacao b
where	a.nr_seq_estagio	= b.nr_sequencia
and	a.nr_atendimento	= nr_atendimento_w
and	a.nr_seq_auditoria	= nr_seq_auditoria_p
and	b.ie_interno		= '1'
and 	a.ie_tipo_autorizacao	= ie_tipo_autorizacao_w;


if	(nr_sequencia_autor_w is null) then
	--Autoriza��o gerada pela Auditoria Conta Paciente (Seq: #@NR_SEQ_AUDITORIA_P#@ ).
	ds_observacao_w	:= substr(wheb_mensagem_pck.get_texto(305887,'NR_SEQ_AUDITORIA_P='||nr_seq_auditoria_p),1,2000);

	select 	nvl(max(nr_seq_autorizacao),0) + 1
	into	nr_seq_autorizacao_w
	from	autorizacao_convenio
	where	nr_atendimento	= nr_atendimento_w;

	select	autorizacao_convenio_seq.nextval
	into	nr_sequencia_autor_w
	from	dual;
	
	insert into autorizacao_convenio(
		nr_sequencia,
		nr_atendimento,
		nr_seq_autorizacao,
		cd_convenio,
		cd_autorizacao,
		dt_autorizacao,	
		dt_inicio_vigencia,
		dt_atualizacao,
		nm_usuario,		
		ds_observacao,
		cd_procedimento_principal,
		ie_origem_proced,
		ie_tipo_dia,
		ie_tipo_guia,
		nr_seq_estagio,
		ie_tipo_autorizacao,
		nr_seq_auditoria,
		ie_carater_int_tiss,
		cd_tipo_acomodacao,
		cd_pessoa_fisica,
		cd_estabelecimento,
    dt_atualizacao_nrec,
    nm_usuario_nrec)
	values	(nr_sequencia_autor_w,
		nr_atendimento_w,
		nr_seq_autorizacao_w,
		cd_convenio_w,
		null,
		sysdate,
		sysdate,
		sysdate,
		nm_usuario_p,
		ds_observacao_w,
		null,
		null,
		'C',
		ie_tipo_guia_w,
		nr_seq_estagio_w,
		ie_tipo_autorizacao_w,
		nr_seq_auditoria_p,
		ie_carater_int_tiss_w,	
		cd_tipo_acomodacao_w,
		cd_pessoa_fisica_w,
		cd_estabelecimento_w,
    sysdate,
    nm_usuario_p);
		
	nr_seq_autor_nova_w	:= nr_sequencia_autor_w;
end if;

if	(nvl(nr_seq_procpaci_p,0) > 0) then
	
	select	max(a.nr_sequencia)
	into	nr_sequencia_proc_w
	from 	procedimento_autorizado a,
		autorizacao_convenio b,
		estagio_autorizacao c
	where	a.nr_sequencia_autor	= b.nr_sequencia
	and	b.nr_seq_estagio		= c.nr_sequencia
	and	b.nr_sequencia		= nr_sequencia_autor_w
	and	c.ie_interno		= '1'
	and	a.cd_procedimento		= cd_procedimento_w
	and	a.ie_origem_proced		= ie_origem_proced_w;
	
	if	(nr_sequencia_proc_w is null) then
		
		select	procedimento_autorizado_seq.nextval
		into	nr_sequencia_proc_w
		from	dual;
		
		insert	into procedimento_autorizado
			(nr_atendimento,
			nr_seq_autorizacao,
			cd_procedimento,
			ie_origem_proced,
			qt_solicitada,
			qt_autorizada,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia_autor,
			nr_sequencia,
			nr_seq_proc_interno,
			ds_observacao)
		values( nr_atendimento_w,
			nr_seq_autorizacao_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_procedimento_w,
			0,
			sysdate,
			nm_usuario_p,
			nr_sequencia_autor_w,
			nr_sequencia_proc_w,
			nr_seq_proc_interno_w,
			null);
		
		update	procedimento_paciente
		set	nr_seq_proc_autor	= nr_sequencia_proc_w
		where	nr_sequencia	= nr_seq_procpaci_p; 
		
	else
		update	procedimento_autorizado
		set	qt_solicitada	= qt_solicitada + qt_procedimento_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_proc_w;
	
		update	procedimento_paciente
		set	nr_seq_proc_autor	= nr_sequencia_proc_w
		where	nr_sequencia	= nr_seq_procpaci_p;
		
	end if;
	
end if;

if	(nvl(nr_seq_matpaci_p,0) > 0) then

	select	max(a.nr_sequencia)
	into	nr_sequencia_mat_w
	from 	material_autorizado a,
		autorizacao_convenio b,
		estagio_autorizacao c
	where	a.nr_sequencia_autor	= b.nr_sequencia
	and	b.nr_seq_estagio		= c.nr_sequencia
	and	b.nr_sequencia		= nr_sequencia_autor_w
	and	c.ie_interno		= '1'
	and	a.cd_material		= cd_material_w;

	select	trunc(vl_unitario,2)
	into	vl_unit_material_w
	from	material_atend_paciente
	where	nr_sequencia = nr_seq_matpaci_p;
	
	if	(nr_sequencia_mat_w is null) then
		select	material_autorizado_seq.nextval
		into	nr_sequencia_mat_w
		from	dual;	

		insert	into material_autorizado
			(nr_atendimento,
			nr_seq_autorizacao,
			cd_material,
			qt_solicitada,
			qt_autorizada,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_sequencia_autor,
			nr_sequencia,			
			ds_observacao,
			vl_unitario)
		values( nr_atendimento_w,
			nr_seq_autorizacao_w,
			cd_material_w,
			qt_material_w,
			0,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_sequencia_autor_w,
			nr_sequencia_mat_w,			
			null,
			vl_unit_material_w);
			
		update	material_atend_paciente
		set	nr_seq_mat_autor	= nr_sequencia_mat_w
		where	nr_sequencia	= nr_seq_matpaci_p;
		
	else
		update	material_autorizado
		set	qt_solicitada	= qt_solicitada + qt_material_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			vl_unitario	= vl_unit_material_w
		where	nr_sequencia	= nr_sequencia_mat_w;

		update	material_atend_paciente
		set	nr_seq_mat_autor	= nr_sequencia_mat_w
		where	nr_sequencia	= nr_seq_matpaci_p;
	end if;	
end if;

gerar_comunic_autor_conv(nr_seq_autor_nova_w,nm_usuario_p);

commit;

end gerar_autor_conv_auditoria;
/
