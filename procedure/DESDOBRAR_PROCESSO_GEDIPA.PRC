create or replace 
procedure desdobrar_processo_gedipa(	nr_processo_p			number,
					nr_etiquetas_desdobrar_p	number,
					nr_seq_area_prep_p		number,
					nr_processo_desdobrado_p out	number,
					nm_usuario_p			varchar2,
					ie_atualizar_processo_p	varchar2 default 'S') is

nr_processo_desdobrado_w	number(15);					
					
begin

if 	(nr_processo_p is not null) then

	Adep_Desdobrar_Processo(nr_processo_p, nr_etiquetas_desdobrar_p, nr_seq_area_prep_p, nm_usuario_p, nr_processo_desdobrado_w);
	
	if (ie_atualizar_processo_p = 'S') then
	
		if 	(nvl(nr_processo_desdobrado_w,0)  > 0) then
		
			Atualiza_Status_Processo_Adep(nr_processo_desdobrado_w, nr_seq_area_prep_p, 'G', 'P', sysdate, nm_usuario_p);
			
			update  adep_processo_area a                                           
			set     a.dt_preparo    = sysdate,                                
					a.nm_usuario_preparo 	= nm_usuario_p                                     
					where   a.nr_seq_processo    = nr_processo_desdobrado_w
					and     not exists (                                                   
						select   1                                            
											from     adep_processo_area x                         
											where    x.nr_seq_processo    = nr_processo_desdobrado_w
											and      x.nr_seq_area_prep   <> a.nr_seq_area_prep);
		
		end if;
	
	end if;

end if;

commit;

nr_processo_desdobrado_p := nr_processo_desdobrado_w;

end desdobrar_processo_gedipa;
/
