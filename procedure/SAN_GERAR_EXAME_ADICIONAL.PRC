create or replace
procedure san_gerar_exame_adicional(	nr_seq_exame_lote_p	number,
					nr_seq_exame_p		number,
					nm_usuario_p		varchar2) is 

nr_seq_exame_w		number(10);
ds_resultado_w		varchar2(255);
nr_seq_exame_adic_w	number(10);
qt_reg_w		number(10);
qt_exame_adic_w		number(10);

cursor c01 is
select	nr_seq_exame_adic
from	san_exame_adicional
where	nr_seq_exame = nr_seq_exame_w
and	ds_resultado = ds_resultado_w;

begin

select	max(nr_seq_exame),
	max(ds_resultado)
into	nr_seq_exame_w,
	ds_resultado_w
from	san_exame_realizado
where	nr_seq_exame_lote = nr_seq_exame_lote_p
and	nr_seq_exame = nr_seq_exame_p;

-- Inserir exames adicionais
open C01;
loop
fetch C01 into	
	nr_seq_exame_adic_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_reg_w
	from	san_exame_realizado
	where	nr_seq_exame_lote = nr_seq_exame_lote_p
	and	nr_seq_exame = nr_seq_exame_adic_w;
	
	if (qt_reg_w = 0) then
	
		insert into san_exame_realizado 
			(nr_seq_exame_lote, 
			nr_seq_exame, 
			dt_atualizacao, 
			nm_usuario, 
			dt_realizado,
			nr_seq_exame_origem)
		values (nr_seq_exame_lote_p,
			nr_seq_exame_adic_w,
			sysdate, 
			nm_usuario_p, 
			sysdate,
			nr_seq_exame_p);
		commit;
	else if	(qt_reg_w > 0) then
		
		update	san_exame_realizado
		set	nr_seq_exame_origem = nr_seq_exame_p
		where	nr_seq_exame_lote = nr_seq_exame_lote_p
		and	nr_seq_exame = nr_seq_exame_adic_w;

	end if;
	end if;
	
	end;
end loop;
close C01;

-- Excluir exames adicionais
delete 	from san_exame_realizado a
where 	a.nr_seq_exame_lote = nr_seq_exame_lote_p
and	a.dt_liberacao is null
and	a.ds_resultado is not null
and	exists (select	1
		from	san_exame_adicional b
		where	b.nr_seq_exame = nr_seq_exame_p
		and	b.ie_situacao = 'A'
		and	nvl(b.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
		and	b.nr_seq_exame_adic = a.nr_seq_exame
		and	b.ds_resultado <> ds_resultado_w);

end san_gerar_exame_adicional;
/