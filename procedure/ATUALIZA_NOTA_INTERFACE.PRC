create or replace
procedure atualiza_nota_interface(
				nm_usuario_p		varchar2,
				ds_arquivo_p		varchar2,
				cd_interface_txt_p	number,
				cd_interface_xml_p	number
				) is


nr_seq_nota_fiscal_w		number(10,0);
nr_seq_lote_w			number(10,0);
nr_sequencia_w			number(10,0);

cursor c01 is
	select	nr_seq_nota_fiscal,
		nr_seq_lote
	from	w_nota_fiscal;

begin

select	max(a.nr_seq_lote)
into	nr_sequencia_w
from	w_nota_fiscal a
where not exists(
	select	1
	from	nota_fiscal_exportada x
	where	x.nr_sequencia = a.nr_seq_lote)
and a.nm_usuario = nm_usuario_p;

if	(nvl(nr_sequencia_w,0) = 0) then
	select	nota_fiscal_exportada_seq.nextval
	into	nr_sequencia_w
	from	dual;
end if;

insert into nota_fiscal_exportada(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_arquivo,
	cd_interface_txt,
	cd_interface_xml)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	substr(ds_arquivo_p,1,80),
	cd_interface_txt_p,
	cd_interface_xml_p);

open C01;
loop
fetch	C01 into
	nr_seq_nota_fiscal_w,
	nr_seq_lote_w;
exit when C01%notfound;
	begin

	update	nota_fiscal_exportada
	set	nr_sequencia = nr_seq_lote_w
	where	nr_sequencia = nr_sequencia_w;

	update	nota_fiscal
	set	ie_status_envio = 'E',
		nr_seq_exportada = nr_seq_lote_w
	where	nr_sequencia = nr_seq_nota_fiscal_w;

	insert into nota_fiscal_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_nota,
			cd_evento,
			ds_historico)
		values(	nota_fiscal_hist_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_nota_fiscal_w,
			'21',
			substr(wheb_mensagem_pck.get_texto(312254)|| ': ' || ds_arquivo_p,1,255));
	end;
end loop;
close c01;

end atualiza_nota_interface;
/