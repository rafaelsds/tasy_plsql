create or replace
procedure pls_atualiza_valor_mat_orc
			(		nr_seq_orcamento_mat_p	Number,
					ie_gravar_log_p		Varchar2,
					nm_usuario_p		Varchar2) is

cd_estabelecimento_w		Number(10);
nr_seq_prestador_w		Number(10);
dt_solicitacao_w		Date;
nr_seq_material_w		Number(10);
ie_tipo_despesa_w		Varchar2(1);
nr_seq_outorgante_w		Number(10);
nr_seq_segurado_w		Number(10);
vl_material_w			Number(15,4);
dt_ult_vigencia_w		Date;
nr_seq_material_preco_w		Number(10);
vl_material_simpro_w		Number(15,4);
vl_material_brasindice_w	Number(15,2);
vl_material_tabela_w		Number(15,2);
nr_seq_plano_w			Number(10);
nr_seq_tipo_acomodacao_w	Number(10);
nr_seq_categoria_w		Number(10);
nr_seq_tipo_atendimento_w	Number(10);
ie_tipo_atend_tiss_w		Varchar2(2);
nr_seq_clinica_w		Number(10);
ie_tipo_guia_w			Varchar2(2);
nr_seq_req_mat_w		Number(10);
dados_regra_preco_material_w	pls_cta_valorizacao_pck.dados_regra_preco_material;

begin

/*Obter dados dos procedimentos do orçamento*/
select	nr_seq_req_mat,
	nr_seq_material
into	nr_seq_req_mat_w,
	nr_seq_material_w
from	pls_orcamento_mat
where	nr_sequencia	= nr_seq_orcamento_mat_p;

/* Obter dados da requisição */
select	a.cd_estabelecimento,
	a.nr_seq_prestador,
	a.dt_requisicao,
	a.nr_seq_segurado,
	a.nr_seq_plano,
	a.nr_seq_tipo_acomodacao,
	a.ie_tipo_atendimento,
	a.nr_seq_clinica,
	a.ie_tipo_guia
into	cd_estabelecimento_w,
	nr_seq_prestador_w,
	dt_solicitacao_w,
	nr_seq_segurado_w,
	nr_seq_plano_w,
	nr_seq_tipo_acomodacao_w,
	ie_tipo_atend_tiss_w,
	nr_seq_clinica_w,
	ie_tipo_guia_w
from	pls_requisicao		a,
	pls_requisicao_mat	b
where	a.nr_sequencia		= b.nr_seq_requisicao
and	b.nr_sequencia		= nr_seq_req_mat_w;

/* Obter a categoria do tipo de acomodação */ 
if	(nr_seq_tipo_acomodacao_w is not null) then
	select	max(nr_seq_categoria) 
	into	nr_seq_categoria_w
	from	pls_regra_categoria 
	where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w; 
end if; 

begin
	select	nr_sequencia
	into	nr_seq_tipo_atendimento_w
	from	pls_tipo_atendimento
	where	somente_numero(cd_tiss)	= ie_tipo_atend_tiss_w;
exception
when others then
	nr_seq_tipo_atendimento_w	:= null;
end;


begin
	select	nr_seq_outorgante
	into	nr_seq_outorgante_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_w;
exception
	when others then
	nr_seq_outorgante_w	:= 0;
end;

select	ie_tipo_despesa
into	ie_tipo_despesa_w
from	pls_material
where	nr_sequencia	= nr_seq_material_w;

pls_define_preco_material(	cd_estabelecimento_w, nr_seq_prestador_w, dt_solicitacao_w,
				nr_seq_material_w, 4, ie_tipo_despesa_w,
				null, 'OR', nr_seq_outorgante_w,
				nr_seq_segurado_w, '', null, 
				nr_seq_categoria_w , nr_seq_tipo_acomodacao_w, nr_seq_tipo_atendimento_w,
				nr_seq_clinica_w, ie_tipo_guia_w, null,
				null, ie_gravar_log_p, nm_usuario_p, '', '',
				null,null,null,null,null,
				null,null,null,null,null,
				null,null,null,null,null,
				vl_material_w, dt_ult_vigencia_w, nr_seq_material_preco_w, 
				vl_material_simpro_w, vl_material_brasindice_w, vl_material_tabela_w, 
				dados_regra_preco_material_w);

update	pls_orcamento_mat
set	vl_material	= vl_material_w
where	nr_sequencia	= nr_seq_orcamento_mat_p;

commit;

end pls_atualiza_valor_mat_orc;
/