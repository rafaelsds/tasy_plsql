create or replace
procedure rxt_insere_trat_tecnica_lista(
		nr_seq_tecnica_lista_p		varchar2,
		nr_seq_tratamento_p		number,
		nm_usuario_p			varchar2) is 

nr_seq_tecnica_w		rxt_tecnica_modalidade.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	rxt_tecnica_modalidade
	where	obter_se_contido(nr_sequencia, nr_seq_tecnica_lista_p) = 'S'
	order by nr_sequencia;

begin

open C01;
loop
fetch C01 into	
	nr_seq_tecnica_w;
exit when C01%notfound;
	begin
	rxt_insere_trat_tecnica(nr_seq_tratamento_p, nr_seq_tecnica_w, nm_usuario_p);
	end;
end loop;
close C01;

end rxt_insere_trat_tecnica_lista;
/
