create or replace
procedure EXCLUIR_INF_SENSIVEL_OS(
							ie_informacao_p		varchar2,
							nr_ordem_servico_p	number,
							nr_seq_informacao_p number,
							nr_seq_log_acesso_p	number) is 

/*    ie_informacao_p
H - Hist�rico
-- Grava no aftersroll do grid

D - Detalhamento
-- Grava ao acessar a OS, ap�s ativar o wdbpanel da MAN_ORDEM_SERVICO

A - Anexo
-- Grava ao realizar o duplo clique para abrir o arquivo, fazer o download  ou visualizar
*/	

/*    nr_seq_informacao_p
Grava  a sequencia /PK  visualizada da tabela 
*/		

nm_usuario_w			varchar2(15);		
ds_informacao_w			varchar2(255);
nr_seq_idioma_w			number(5);
			
begin


nr_seq_idioma_w :=	nvl(man_obter_idioma_os_local(nr_ordem_servico_p),1);

nm_usuario_w	:= wheb_usuario_pck.get_nm_usuario;


if	(ie_informacao_p = 'H') then
	begin
	
		
	ds_informacao_w := obter_desc_exp_idioma(852477,nr_seq_idioma_w);
	
	
	update	man_ordem_serv_tecnico
	set		ds_relat_tecnico	= obter_desc_exp_idioma(910633,nr_seq_idioma_w,'DS_INFORMACAO=' || ds_informacao_w)
	where	nr_seq_ordem_serv	= nr_ordem_servico_p
	and		nr_sequencia		= nr_seq_informacao_p;

	
	end;
elsif (ie_informacao_p = 'D') then
	begin
	
	ds_informacao_w := obter_desc_exp_idioma(287639,nr_seq_idioma_w);
	

	
	update	man_ordem_servico
	set		ds_dano			= obter_desc_exp_idioma(910633,nr_seq_idioma_w,'DS_INFORMACAO=' || ds_informacao_w)
	where 	nr_sequencia	= nr_ordem_servico_p;
		

	end;
elsif (ie_informacao_p = 'A') then
	begin

  	update man_ordem_serv_arq
  	set ie_deletar = 'S'
	where 	nr_seq_ordem	= nr_ordem_servico_p
	and	nr_sequencia	= nr_seq_informacao_p;
	
	delete	man_ordem_serv_arq
	where 	nr_seq_ordem	= nr_ordem_servico_p
	and		nr_sequencia	= nr_seq_informacao_p;
	
	ds_informacao_w :=  obter_desc_exp_idioma(283478,nr_seq_idioma_w,NULL);

	end;
end if;

-- Gravar log

update  log_inf_sensivel_os
set		dt_exclusao 		= sysdate,
		nm_usuario_exclusao = nm_usuario_w
where	nr_sequencia		= nr_seq_log_acesso_p;


begin
insert into 	man_ordem_serv_tecnico (nr_sequencia, 
					nr_seq_ordem_serv, 
					dt_atualizacao, 
					dt_historico, 
					dt_liberacao,
					nm_usuario, 
					nr_seq_tipo,
					ie_origem,
					ds_relat_tecnico) 
			values (man_ordem_serv_tecnico_seq.nextval, 
					nr_ordem_servico_p, 
					sysdate,
					sysdate, 
					sysdate, 
					wheb_usuario_pck.get_nm_usuario,
					198, 
					'I',
					obter_desc_exp_idioma(910635,nr_seq_idioma_w,'DS_INFORMACAO=' || ds_informacao_w));
exception
when others then
	null;
end;

commit;

end excluir_inf_sensivel_os;
/
