create or replace
procedure ptu_gestao_imp_resp_cons_prest
			(	ds_arquivo_p		Varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

nr_versao_ptu_w			Varchar2(255);

begin

--dbms_application_info.SET_ACTION('TASY_SCS');

nr_versao_ptu_w	:= pls_obter_versao_scs;

if	(nr_versao_ptu_w	= '035') then
	ptu_imp_resp_consulta_prest(ds_arquivo_p, cd_estabelecimento_p, nm_usuario_p);
	
elsif	(nr_versao_ptu_w	= '040') then
	ptu_imp_resp_consult_prest_v40(ds_arquivo_p, cd_estabelecimento_p, nm_usuario_p);
	
elsif	(nr_versao_ptu_w	= '050') then
	ptu_imp_resp_consult_prest_v50(ds_arquivo_p, cd_estabelecimento_p, nm_usuario_p);	
end if;

--dbms_application_info.SET_ACTION('');

end ptu_gestao_imp_resp_cons_prest;
/