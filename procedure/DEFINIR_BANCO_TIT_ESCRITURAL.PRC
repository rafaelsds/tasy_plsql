create or replace
procedure definir_banco_tit_escritural	(ie_tipo_p	varchar2,
					nr_titulo_p	number,
					nr_seq_escrit_p	number,
					nm_usuario_p	varchar2,
					ie_commit_p	varchar2) is

/*	ie_tipo_p

CP	- Contas a pagar
CR	- Contas a receber

*/

nr_bloqueto_w			Varchar2(255);
cd_banco_w			Number(3);
cd_agencia_bancaria_w		Varchar2(8);
ie_digito_agencia_w		varchar2(5);
nr_conta_w			Varchar2(20);
ie_digito_conta_w		Varchar2(5);
cd_camara_compensacao_w		Number(3);
cd_pessoa_fisica_titulo_w	Varchar2(25);
cd_cgc_titulo_w			Varchar2(25);
nr_conta_pagto_w		Varchar2(20);
ie_tipo_pagamento_w		Varchar2(3);
ie_dados_banco_escrit_blq_w	varchar2(1);
ie_tipo_conta_w			varchar2(3);
qt_barras_w			number(10);
ie_forma_importacao_w		varchar2(1);
cd_estabelecimento_w		number(4);
ie_pessoa_w			varchar2(10);
nr_seq_pls_pag_prest_w		number(10);
nr_seq_prestador_w		number(10);
qt_conta_prest_w		number(10)	:= 0;
qt_banco_w			number(10);
qt_titulo_favorecido_w			number(10);
ie_tipo_pagto_baixa_w		varchar2(3);
cd_pessoa_fisica_w		pls_prestador.cd_pessoa_fisica%type;
cd_cgc_w			pls_prestador.cd_cgc%type;
dt_emissao_w			titulo_pagar.dt_emissao%type;

cursor c01 is
	select	cd_banco,
		cd_agencia_bancaria,
		ie_digito_agencia,
		nr_conta,
		nr_digito_conta,
		cd_camara_compensacao,
		ie_tipo_conta
	from 	pessoa_fisica_conta a
	where	cd_pessoa_fisica 	=
		(select	cd_pessoa_fisica
		from	titulo_pagar
		where	nr_titulo 	= nr_titulo_p)
	and	nr_conta		= nvl(nr_conta_pagto_w, nr_conta)
	and	nvl(ie_situacao,'A')	= 'A'
	union
	select	cd_banco,
		cd_agencia_bancaria,
		ie_digito_agencia,
		nr_conta,
		nr_digito_conta,
		cd_camara_compensacao,
		'CC' ie_tipo_conta
	from 	pessoa_juridica_conta a
	where	cd_cgc 			=
		(select	cd_cgc
		from	titulo_pagar
		where	nr_titulo 	= nr_titulo_p)
	and	nr_conta		= nvl(nr_conta_pagto_w, nr_conta)
	and	nvl(ie_situacao,'A') 	= 'A';
	
/* William - OS 400968 - Adicionado o cursor para ter as informacoes dos prestadores */
Cursor C02 is
	select	a.cd_banco,
		a.cd_agencia_bancaria,
		a.ie_digito_agencia,
		a.nr_conta,
		a.nr_digito_conta,
		a.cd_camara_compensacao,
		a.ie_tipo_conta
	from 	pessoa_fisica_conta	a,
		pls_prestador_pagto	b
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	a.cd_banco		= b.cd_banco
	and	a.cd_agencia_bancaria	= b.cd_agencia_bancaria
	and	a.nr_conta		= b.nr_conta
	and	nvl(a.ie_situacao,'A')	= 'A'
	and	b.nr_seq_prestador	= nr_seq_prestador_w
	and	dt_emissao_w between b.dt_inicio_vigencia_ref and b.dt_fim_vigencia_ref
	union
	select	c.cd_banco,
		c.cd_agencia_bancaria,
		c.ie_digito_agencia,
		c.nr_conta,
		c.nr_digito_conta,
		c.cd_camara_compensacao,
		'CC' ie_tipo_conta
	from 	pessoa_juridica_conta	c,
		pls_prestador_pagto	d
	where	c.cd_cgc 		= d.cd_cgc
	and	c.cd_banco		= d.cd_banco
	and	c.cd_agencia_bancaria	= d.cd_agencia_bancaria
	and	c.nr_conta		= d.nr_conta
	and	nvl(c.ie_situacao,'A') 	= 'A'
	and	d.nr_seq_prestador	= nr_seq_prestador_w
	and	dt_emissao_w between d.dt_inicio_vigencia_ref and d.dt_fim_vigencia_ref;

BEGIN

if	(ie_tipo_p = 'CP') then

	if	(nr_seq_escrit_p	is not null) then

		select	max(a.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	banco_escritural a
		where	a.nr_sequencia	= nr_seq_escrit_p;

		select	max(a.ie_tipo_pagamento)
		into	ie_tipo_pagamento_w
		from	titulo_pagar_escrit a
		where	a.nr_titulo	= nr_titulo_p
		and	a.nr_seq_escrit	= nr_seq_escrit_p;

	else

		select	max(a.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	titulo_pagar a
		where	a.nr_titulo	= nr_titulo_p;

	end if;

	select	max(a.cd_pessoa_fisica),
		max(a.cd_cgc),
		max(a.nr_conta),
		max(a.nr_seq_pls_pag_prest),
		max(a.nr_bloqueto),
		max(a.dt_emissao)
	into	cd_pessoa_fisica_titulo_w,
		cd_cgc_titulo_w,
		nr_conta_pagto_w,
		nr_seq_pls_pag_prest_w,
		nr_bloqueto_w,
		dt_emissao_w
	from	titulo_pagar a
	where	a.nr_titulo 	= nr_titulo_p;
	
	/* William - OS 400968 - Verifica o prestador */
	nr_seq_prestador_w	:= null;
	cd_pessoa_fisica_w	:= null;
	cd_cgc_w		:= null;
	select	max(b.nr_seq_prestador),
		max(a.cd_pessoa_fisica),
		max(cd_cgc)
	into	nr_seq_prestador_w,
		cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_pagamento_prestador	b,
		pls_prestador		a
	where	a.nr_sequencia		= b.nr_seq_prestador
	and	b.nr_sequencia 		= nr_seq_pls_pag_prest_w;

	if	(nr_conta_pagto_w is null) then
		nr_conta_pagto_w	:= obter_conta_pag_pessoa(cd_pessoa_fisica_titulo_w, cd_cgc_titulo_w, cd_estabelecimento_w);
	end if;

	obter_param_usuario(857,32,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_forma_importacao_w);

	select	count(*)
	into	qt_barras_w
	from	banco_escrit_barras a
	where	(ie_forma_importacao_w = '2' or (ie_forma_importacao_w = '1' and a.nr_seq_banco_escrit = nr_seq_escrit_p))
	and	a.nr_titulo	= nr_titulo_p;

	if	(qt_barras_w = 1) then
		ie_tipo_pagamento_w	:= 'DDA';
	else
		/* OS79423 - rfcaldas - 21/02/2008 - Buscar a forma de pagamento do tipo de baixa do contas a pagar. */
		select	max(a.ie_tipo_pagamento)
		into	ie_tipo_pagto_baixa_w
		from	tipo_baixa_cpa a,
			titulo_pagar b
		where	b.cd_tipo_baixa_neg	= a.cd_tipo_baixa
		and	b.nr_titulo		= nr_titulo_p;

		if	(ie_tipo_pagto_baixa_w is null) then

			begin

			if	(nr_bloqueto_w	is null) then

				ie_tipo_pagamento_w	:= 'DOC';

			else

				if	(ie_tipo_pagamento_w	is null) then

					ie_tipo_pagamento_w	:= 'BLQ';

				else

					select	count(*)
					into	qt_banco_w
					from	banco a
					where	a.cd_banco	= somente_numero(substr(nr_bloqueto_w,1,3));

					if	(nvl(qt_banco_w,0)	> 0) then

						ie_tipo_pagamento_w	:= 'BLQ';

					end if;

				end if;

			end if;

			exception
			when	no_data_found then
				/* O titulo nr_titulo_p nao existe no sistema!
				Verifique o arquivo de importacao.
				sqlerrm */
				wheb_mensagem_pck.exibir_mensagem_abort(210688,	'NR_TITULO_P' || nr_titulo_p ||
										';SQL_ERRM=' || sqlerrm);
			end;

		else

			ie_tipo_pagamento_w	:= ie_tipo_pagto_baixa_w;

		end if;

	end if;

	if	(cd_camara_compensacao_w is null) then
		if	(ie_tipo_pagamento_w = 'DOC') then
			cd_camara_compensacao_w	:= 18;
		else
			cd_camara_compensacao_w	:= 0;
		end if;
	end if;
	
	if	(nr_seq_prestador_w is not null) then
		select	count(1)
		into	qt_conta_prest_w
		from	pls_prestador_pagto a
		where	nr_seq_prestador	= nr_seq_prestador_w
		and	a.cd_banco is not null
		and	a.cd_agencia_bancaria is not null
		and	a.nr_conta is not null;
	end if;
	
	if	(nr_seq_pls_pag_prest_w is not null) and
		(qt_conta_prest_w > 0) and
		((cd_pessoa_fisica_titulo_w = cd_pessoa_fisica_w) or (cd_cgc_titulo_w = cd_cgc_w)) then
		/* William - OS 400968 - Caso seja titulo de pagamento de prestador */
		open C02;
		loop
		fetch C02 into	
			cd_banco_w,
			cd_agencia_bancaria_w,
			ie_digito_agencia_w,
			nr_conta_w,
			ie_digito_conta_w,
			cd_camara_compensacao_w,
			ie_tipo_conta_w;
		exit when C02%notfound;
			begin			
			if	(ie_tipo_conta_w = 'CP') then
				ie_tipo_pagamento_w	:= 'CCP';
			end if;
			end;
		end loop;
		close C02;
	else
		open	c01;
		loop
		fetch	c01 into
			cd_banco_w,
			cd_agencia_bancaria_w,
			ie_digito_agencia_w,
			nr_conta_w,
			ie_digito_conta_w,
			cd_camara_compensacao_w,
			ie_tipo_conta_w;
		exit 	when c01%notfound;

			if	(ie_tipo_conta_w = 'CP') then
				ie_tipo_pagamento_w	:= 'CCP';
			end if;

		end	loop;
		close	c01;
	end if;
	

	if 	((ie_tipo_pagamento_w = 'BLQ') or (ie_tipo_pagamento_w = 'DDA')) and
		(nr_bloqueto_w is not null) then

		select	somente_numero(substr(nr_bloqueto_w,1,3))
		into	cd_banco_w
		from	dual;

		select	count(*)
		into	qt_banco_w
		from	banco a
		where	a.cd_banco	= cd_banco_w;

		select	nvl(max(ie_dados_banco_escrit_blq),'S')
		into	ie_dados_banco_escrit_blq_w
		from	banco
		where	somente_numero(cd_banco)	= nvl(cd_banco_w,0);

		if	(qt_banco_w	= 0) then
			cd_banco_w	:= null;
		end if;

		if	((nvl(ie_dados_banco_escrit_blq_w,'S') = 'S') or
			(nvl(ie_dados_banco_escrit_blq_w,'S') = 'V'))and
			(cd_banco_w in (399, 341, 1, 237, 291, 104, 409, 33, 41)) then

			select	substr(obter_dados_cod_barras(nr_bloqueto_w,'A'),1,255) cd_agencia_bancaria,
				substr(obter_dados_cod_barras(nr_bloqueto_w,'DA'),1,255) ie_digito_agencia,
				substr(obter_dados_cod_barras(nr_bloqueto_w,'C'),1,255) nr_conta,
				substr(obter_dados_cod_barras(nr_bloqueto_w,'DC'),1,255) ie_digito_conta
			into	cd_agencia_bancaria_w,
				ie_digito_agencia_w,
				nr_conta_w,
				ie_digito_conta_w
			from	dual;

		end if;

	end if;

	if (cd_banco_w is null and cd_agencia_bancaria_w is null and nr_conta_w is null and ie_digito_agencia_w is null and ie_digito_conta_w is null) then
		select	count(*)
		into	qt_titulo_favorecido_w
		from	titulo_pagar_favorecido
		where	nr_titulo	= nr_titulo_p;

		if (qt_titulo_favorecido_w > 0) then
			select distinct	max(nvl(cd_banco,obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'B'))) cd_banco,
				max(nvl(cd_agencia_bancaria,obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'A'))) cd_agencia_bancaria,
				max(nvl(ie_digito_agencia,obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DA'))) ie_digito_agencia,
				max(nvl(nr_conta,obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'C'))) nr_conta,
				max(nvl(nr_digito_conta,obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DC'))) nr_digito_conta
			into	cd_banco_w,
				cd_agencia_bancaria_w,
				ie_digito_agencia_w,
				nr_conta_w,
				ie_digito_conta_w
			from	titulo_pagar_favorecido a
			where	nr_titulo	= nr_titulo_p;
		end if;
		
	end if;
	
	update	titulo_pagar_escrit
	set	cd_banco		= nvl(cd_banco_w,cd_banco),
		cd_agencia_bancaria	= nvl(cd_agencia_bancaria_w,cd_agencia_bancaria),
		nr_conta		= nvl(nr_conta_w,nr_conta),
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_digito_conta		= decode(nvl(ie_digito_conta_w,ie_digito_conta),'V',ie_digito_conta_w,nvl(ie_digito_conta_w,ie_digito_conta)),
		ie_digito_agencia	= decode(nvl(ie_digito_agencia_w,ie_digito_agencia),'V',ie_digito_agencia_w,nvl(ie_digito_agencia_w,ie_digito_agencia)),
		cd_camara_compensacao	= nvl(cd_camara_compensacao_w,cd_camara_compensacao),
		ie_tipo_pagamento	= ie_tipo_pagamento_w
	where	nr_seq_escrit		= nvl(nr_seq_escrit_p,nr_seq_escrit)
	and	nr_titulo		= nr_titulo_p;

	if	(cd_pessoa_fisica_titulo_w is not null) then
		ie_pessoa_w	:= 'PF';
	else
		ie_pessoa_w	:= 'PJ';
	end if;

	/* atualiza o tipo de pagamento de acordo com a regra cadastrada */
	if	(ie_tipo_pagamento_w <> 'BLQ') and
		(ie_tipo_pagamento_w <> 'DDA') then
		Atualiza_regra_tipo_pag_escrit(nr_seq_escrit_p,nr_titulo_p, ie_pessoa_w);
	end if;

end if;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end definir_banco_tit_escritural;
/
