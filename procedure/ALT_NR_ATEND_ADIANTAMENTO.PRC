create or replace
procedure alt_nr_atend_adiantamento(	nr_atendimento_p	number,
				nm_usuario_p	varchar2,
				nr_adiantamento_p	number) is 

begin

update  adiantamento
set	nr_atendimento	= nr_atendimento_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_adiantamento	= nr_adiantamento_p;

commit;

end alt_nr_atend_adiantamento;
/