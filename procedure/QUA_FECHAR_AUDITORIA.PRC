create or replace
procedure qua_fechar_auditoria(	
	nr_sequencia_p		number) is 

begin

update	Qua_auditoria
set	dt_fechamento = sysdate
where	nr_sequencia = nr_sequencia_p;
commit;

end qua_fechar_auditoria;
/

