create or replace
Procedure Gerar_itens_kit_requisicao(
			nr_requisicao_p		number,
			nm_usuario_p		varchar2) is


cd_estabelecimento_w		Number(4);
cd_kit_material_w			Number(10);
nr_sequencia_w			Number(5);
nr_sequencia_ww			Number(5);
cd_material_w			Number(6);
cd_material_ww			Number(6);
qt_material_requisitada_w	Number(13,4);
qt_material_requisitada_ww	Number(13,4);
cd_unidade_medida_consumo_w	varchar2(30);
cd_unidade_medida_estoque_w	varchar2(30);
qt_conv_estoque_consumo_w	Number(13,4);
qt_material_w			Number(13,4);
qt_estoque_w			Number(13,4);
qt_estoque_ww			Number(13,4);
nr_seq_aprovacao_w		number(10);
nr_seq_proc_aprov_w		number(10);
dt_aprovacao_w			date;
nm_usuario_aprov_w		varchar2(15);
dt_reprovacao_w			date;
ie_qt_estoque_mult_w		varchar2(1);

cursor c00 is
select	a.cd_material,
	a.qt_material_requisitada,
	a.qt_estoque,
	a.nr_sequencia,
	a.nr_seq_aprovacao,
	a.nr_seq_proc_aprov,
	a.dt_aprovacao,
	a.nm_usuario_aprov,
	a.dt_reprovacao
from	item_requisicao_material a,
	material b
where	a.nr_requisicao	= nr_requisicao_p
and	a.cd_material = b.cd_material;

cursor c01 is
select	b.cd_material,
	b.qt_material,
	substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
	substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_w,'UME'),1,30) cd_unidade_medida_estoque,
	qt_conv_estoque_consumo
from	componente_kit b,
	material c
where	b.cd_kit_material		= cd_kit_material_w
and	c.cd_material		= b.cd_material
and	(((nvl(b.ie_duplica_req,'S') = 'S') and (b.cd_material = cd_material_w)) or
	(b.cd_material <> cd_material_w))
and	((b.cd_estab_regra is null) or (b.cd_estab_regra = cd_estabelecimento_w));

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

ie_qt_estoque_mult_w := substr(nvl(obter_valor_param_usuario(919, 119, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N'),1,1);

OPEN C00;
LOOP
FETCH C00 INTO
	cd_material_w,
	qt_material_requisitada_w,
	qt_estoque_ww,
	nr_sequencia_w,
	nr_seq_aprovacao_w,
	nr_seq_proc_aprov_w,
	dt_aprovacao_w,
	nm_usuario_aprov_w,
	dt_reprovacao_w;
exit when c00%notfound;
	begin
	select nvl(max(Obter_mat_estabelecimento(
				cd_estabelecimento_w,
				0,
				cd_material_w,
				'KT')), 0)
	into	cd_kit_material_w
	from	dual;

	if	(cd_kit_material_w > 0) then
		begin
		OPEN C01;
		LOOP
		FETCH C01 INTO
			cd_material_ww,
			qt_material_w,
			cd_unidade_medida_consumo_w,
			cd_unidade_medida_estoque_w,
			qt_conv_estoque_consumo_w;
		exit when c01%notfound;
			begin

			select	nvl(max(nr_sequencia), 0) + 1
			into	nr_sequencia_ww
			from	item_requisicao_material
			where	nr_requisicao = nr_requisicao_p;

			if	(ie_qt_estoque_mult_w = 'S') then
				qt_material_requisitada_w := qt_estoque_ww;
			end if;
			
			qt_material_requisitada_ww	:= qt_material_w * qt_material_requisitada_w;
			qt_estoque_w			:= dividir(qt_material_requisitada_ww, qt_conv_estoque_consumo_w);
				
			insert into item_requisicao_material(
				nr_requisicao,
				nr_sequencia,
				cd_estabelecimento,
				cd_material,
				qt_material_requisitada,
				vl_material,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				cd_motivo_baixa,
				qt_estoque,
				cd_unidade_medida_estoque,
				cd_material_req,
				ie_geracao,
				cd_kit_material,
				nr_seq_aprovacao,
				nr_seq_proc_aprov,
				dt_aprovacao,
				nm_usuario_aprov,
				dt_reprovacao)
			values( nr_requisicao_p,
				nr_sequencia_ww,
				cd_estabelecimento_w,
				cd_material_ww,
				qt_material_requisitada_ww,
				0,
				sysdate,
				nm_usuario_p,
				cd_unidade_medida_consumo_w,
				0,
				qt_estoque_w,
				cd_unidade_medida_estoque_w,
				cd_material_ww,
				'S',
				cd_kit_material_w,
				nr_seq_aprovacao_w,
				nr_seq_proc_aprov_w,
				dt_aprovacao_w,
				nm_usuario_aprov_w,
				dt_reprovacao_w);
			end;
		end loop;
		close c01;

		/*Identifica que o item � um KIT e j� foi desdobrado na libera��o da requisi��o*/
		update	item_requisicao_material
		set	ie_geracao = 'D'
		where	nr_requisicao = nr_requisicao_p
		and	nr_sequencia = nr_sequencia_w;

		end;
	end if;
	end;
end loop;
close c00;

commit;

end Gerar_itens_kit_requisicao;
/
