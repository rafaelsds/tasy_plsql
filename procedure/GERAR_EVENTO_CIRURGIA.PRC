create or replace 
procedure gerar_evento_cirurgia(	cd_profissional_p		varchar2,
				dt_evento_p		date,
				nr_cirurgia_p		number,
				nr_seq_evento_p		number,
				cd_local_estoque_p	number,
				ds_observacao_p		varchar2,
				nm_usuario_p		varchar2,
				nr_sequencia_p	out	number,
				nr_seq_pepo_p		number,
				dt_inicio_evento_p	date default null) is

ie_finaliza_cirurgia_w	varchar2(1);
ie_lanc_automatico_w	varchar2(1);
dt_inicio_real_w	date;
qt_mintuos_w		number(10,0);
ie_evento_painel_w	varchar2(15);
nr_sequencia_w		number(10,0);
nr_sequencia_ev_w	number(10,0) := 0;
nr_seq_evento_dep_w	number(10,0);
dt_registro_w		date;
nr_prescricao_w		number(10,0);
nr_prescr_mat_esp_w	number(10,0);
ie_trocar_sala_w		varchar2(1);
nr_seq_interno_w	number(10,0) := null;
nr_cirurgia_w		number(10);
nr_cirurgia_ww		number(10);
ie_permite_registrar_w	number(2);
nr_atendimento_w	number(10);
nr_atendimento_ww	number(10);
qt_proc_hemot_w		number(10);
ie_gerar_proc_hemo_w	Varchar2(1);
ie_momento_integracao_w	varchar2(15);
ie_inicia_integracao_w	varchar2(1);
ie_finaliza_integracao_w varchar2(1);
cd_setor_atendimento_w		number(5,0);
ie_gera_mat_autorizacao_w	varchar2(1);
nr_seq_agenda_w			number(10);
nr_seq_proc_interno_w		number(10,0); --Alteracoes feitas pelo Fabricio Theiss conforem historico do dia 02/12/15 da OS 823663
cd_perfil_w             perfil.cd_perfil%type   := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w            usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type:= wheb_usuario_pck.get_cd_estabelecimento;
nr_seq_evento_eritel_w  number(10);
cd_unidade_basica_w  atend_paciente_unidade.cd_unidade_basica%type;
cd_unidade_compl_w   atend_paciente_unidade.cd_unidade_compl%type;
ie_alter_status_leito_w varchar2(1);
qt_existe_regra_setor_w	number(5);

cursor c01 is
	select	nr_cirurgia
	from	cirurgia
	where	nr_seq_pepo = nr_seq_pepo_p;
	
cursor c02 is
	select	a.nr_sequencia
	from	prescr_material a
	where	a.nr_prescricao 	= nr_prescricao_w
	and	a.qt_material 		> 0
	and	not exists (select 1 from material_autorizado b where a.nr_prescricao = b.nr_prescricao and a.nr_sequencia = b.nr_seq_prescricao);
	
cursor c03 is
	select	a.nr_prescricao,
		b.nr_sequencia
	from	prescr_medica a,
		prescr_material b
	where	a.nr_prescricao = b.nr_prescricao
	and	a.nr_cirurgia 	= nr_cirurgia_p
	and 	nvl(a.ie_tipo_prescr_cirur,0) <> 2
	and	b.qt_material 	> 0
	and	not exists (select 1 from material_autorizado c where b.nr_prescricao = c.nr_prescricao and b.nr_sequencia = c.nr_seq_prescricao)
	order by 1;
	
cursor c04 is --Alteracoes feitas pelo Fabricio Theiss conforem historico do dia 02/12/15 da OS 823663
	select	b.nr_sequencia,
		b.nr_seq_proc_interno
	from	prescr_procedimento b
	where	b.nr_prescricao = nr_prescricao_w
	and	not exists (select 1 from procedimento_autorizado c where b.nr_prescricao = c.nr_prescricao and b.nr_sequencia = c.nr_seq_prescricao)
	order by 1;
	
				
begin

obter_param_usuario(872, 534, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, nr_seq_evento_eritel_w);

select	nvl(max(ie_finaliza_cirurgia),'N'),
	nvl(max(ie_lanc_automatico),'N'),
	nvl(max(ie_trocar_sala),'N'),
	nvl(max(ie_gerar_proc_hemo),'N'),
	nvl(max(ie_inicia_integracao),'N'),
	nvl(max(ie_finaliza_integracao),'N'),
	nvl(max(ie_gera_mat_autorizacao),'N'),
	nvl(max(ie_alter_status_leito),'N')
into	ie_finaliza_cirurgia_w,
	ie_lanc_automatico_w,
	ie_trocar_sala_w,
	ie_gerar_proc_hemo_w,
	ie_inicia_integracao_w,
	ie_finaliza_integracao_w,
	ie_gera_mat_autorizacao_w,
	ie_alter_status_leito_w
from	evento_cirurgia
where	nr_sequencia = nr_seq_evento_p;

select	nvl(max(ie_momento_integracao),'IF')
into	ie_momento_integracao_w
from	parametros_pepo
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_finaliza_cirurgia_w = 'S') then
	select	max(dt_inicio_real)
	into	dt_inicio_real_w
	from	cirurgia
	where	nr_cirurgia = nr_cirurgia_p;		
	
	select	obter_minutos_espera(dt_inicio_real_w, dt_evento_p) 
	into	qt_mintuos_w
	from	dual;
	if 	(nr_cirurgia_p > 0) then 
		finalizar_cirurgia(nr_cirurgia_p, dt_evento_p, qt_mintuos_w, nm_usuario_p);
	end if;
end if;

select	max(nr_prescricao),
		max(nr_atendimento)
into	nr_prescricao_w,
		nr_atendimento_w
from	cirurgia
where	nr_cirurgia = nr_cirurgia_p;

if	(ie_lanc_automatico_w = 'S') then
	gerar_lanc_automatico(nr_cirurgia_p, nr_prescricao_w, cd_local_estoque_p, nm_usuario_p);
end if;

---Teste

select	max(nr_seq_evento_dep)
into	nr_seq_evento_dep_w
from	evento_cirurgia
where	nr_sequencia = nr_seq_evento_p;

if	(nr_seq_evento_dep_w is not null)	then

	select  max(dt_registro) 
	into	dt_registro_w
	from	evento_cirurgia_paciente
	where	nr_cirurgia = nr_cirurgia_p
	and 	nvl(ie_situacao,'A') = 'A'
	and	nr_seq_evento = nr_seq_evento_dep_w;	
		
	if 	(dt_registro_w is not null)	and
		(dt_evento_p < dt_registro_w)	then
		wheb_mensagem_pck.exibir_mensagem_abort(189748,'DS_EVENTO_CIRURGIA_W='||to_char(obter_desc_evento_cirurgia(nr_seq_evento_dep_w)));
	
	end if;
end if;

---Fim Teste

select	evento_cirurgia_paciente_seq.nextval
into	nr_sequencia_ev_w
from	dual;

if	(ie_trocar_sala_w = 'S') and (nr_cirurgia_p > 0) then
	nr_seq_interno_w := obter_unidade_cirurgia(nr_cirurgia_p);
	if	(nr_seq_interno_w = 0) then
		nr_seq_interno_w := null;
	end if;
end if;

ie_permite_registrar_w := 0;

SELECT	COUNT(*)
INTO	ie_permite_registrar_w
FROM  	evento_cirurgia_paciente a
WHERE	a.nr_cirurgia 		= nr_cirurgia_p	
AND	a.nr_seq_evento  	= nr_seq_evento_p		
AND	dt_registro		= dt_evento_p
and	nvl(a.ie_situacao,'A')	= 'A';

if  	(ie_permite_registrar_w = 0) then
		SELECT	COUNT(*)
		INTO	ie_permite_registrar_w
		FROM  	evento_cirurgia_paciente a
		WHERE	a.nr_seq_pepo 		= nr_seq_pepo_p	
		AND	a.nr_seq_evento  	= nr_seq_evento_p		
		AND	dt_registro		= dt_evento_p
		and	nvl(a.ie_situacao,'A')	= 'A';
		
end if;

/*insert into logx_tasy values(sysdate,nm_usuario_p,68955,'Procedure = gerar_evento_cirurgia; Sequencia = '||nr_sequencia_ev_w||' Evento = '||nr_seq_evento_p||' Cirurgia = '||nr_cirurgia_p||'  Numero do processo ='||nr_seq_pepo_p||' data do evento = '||dt_evento_p); */

if	(ie_permite_registrar_w = 0) then

	insert into evento_cirurgia_paciente(
		nr_sequencia,           
		nr_seq_evento,
		nr_cirurgia,            
		ds_observacao,          
		dt_registro,            
		cd_profissional,        
		dt_atualizacao,         
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_pepo,
		nr_seq_interno,
		ie_situacao,
		dt_inicio_evento)
	values(
		nr_sequencia_ev_w,
		nr_seq_evento_p,
		decode(nr_cirurgia_p,0,null,nr_cirurgia_p),
		ds_observacao_p,
		dt_evento_p,
		cd_profissional_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		decode(nr_seq_pepo_p,0,null,nr_seq_pepo_p),
		nr_seq_interno_w,
		'A',
		dt_inicio_evento_p);
	commit;

	nr_sequencia_p	:=	nr_sequencia_ev_w;
	
	nr_atendimento_ww	:= 0;
	nr_cirurgia_ww 		:= 0;
	
	if (nvl(nr_atendimento_w,0) = 0) and (nvl(nr_seq_pepo_p,0) > 0)  then
		
		select	nvl(max(nr_cirurgia_principal),0)
		into	nr_cirurgia_ww
		from	pepo_cirurgia
		where	nr_sequencia = nr_seq_pepo_p;

		if (nvl(nr_cirurgia_ww,0) = 0) then
			select	nvl(max(nr_cirurgia),0)
			into	nr_cirurgia_ww
			from	cirurgia
			where	nr_seq_pepo = nr_seq_pepo_p;
		end if;
		
		select	nvl(max(nr_atendimento),0)
		into	nr_atendimento_ww
		from	cirurgia
		where	nr_cirurgia = nr_cirurgia_p;

	end if;
	
	if (nvl(nr_atendimento_ww,0) = 0) then
				
		select	nvl(max(nr_atendimento),0)
		into	nr_atendimento_ww
		from 	pepo_cirurgia
		where	nr_sequencia = nr_seq_pepo_p;	
	end if;
	
	if (nvl(nr_atendimento_w,nr_atendimento_ww) is not null) then
		gerar_hig_leito_cc(sysdate, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, 'EC', null, nvl(nr_atendimento_w,nr_atendimento_ww) , null,nr_seq_evento_p, ie_alter_status_leito_w);
	end if;
end if;

if (ie_permite_registrar_w > 0 and ie_finaliza_cirurgia_w = 'S' and nr_sequencia_p is null) then
	SELECT 
		  max(a.nr_sequencia)
	INTO  
		  nr_sequencia_p
	FROM  
		  evento_cirurgia_paciente a,
		  evento_cirurgia b
	WHERE
		  a.nr_seq_evento   				= b.nr_sequencia
	AND   a.nr_cirurgia 					= nr_cirurgia_p	
	AND	  a.nr_seq_evento  					= nr_seq_evento_p		
	AND	  a.dt_registro		  				= dt_evento_p
	AND   nvl(b.ie_finaliza_cirurgia,'N') 	= 'S'
	AND	  nvl(a.ie_situacao,'A')			= 'A';
end if;

if (nvl(nr_seq_evento_eritel_w,0) > 0) and (nr_seq_evento_eritel_w = nr_seq_evento_p) then
   begin
   if (nvl(nr_cirurgia_p,0) > 0) then
      select   max(a.cd_setor_atendimento),
               max(a.cd_unidade_basica),
               max(a.cd_unidade_compl),
               max(a.nr_seq_interno),
               max(a.nr_atendimento)
      into     cd_setor_atendimento_w,
               cd_unidade_basica_w,
               cd_unidade_compl_w,
               nr_seq_interno_w,
               nr_atendimento_w
      from     atend_paciente_unidade a,
               cirurgia b
      where    a.NR_ATENDIMENTO     = b.NR_ATENDIMENTO
      and      a.DT_ENTRADA_UNIDADE = b.DT_ENTRADA_UNIDADE
      and      b.nr_cirurgia        = nr_cirurgia_p;
   elsif (nvl(nr_seq_pepo_p,0) > 0) then
      select   max(a.cd_setor_atendimento),
               max(a.cd_unidade_basica),
               max(a.cd_unidade_compl),
               max(a.nr_seq_interno),
               max(a.nr_atendimento)
      into     cd_setor_atendimento_w,
               cd_unidade_basica_w,
               cd_unidade_compl_w,
               nr_seq_interno_w,
               nr_atendimento_w
      from     atend_paciente_unidade a,
               cirurgia b
      where    a.NR_ATENDIMENTO     = b.NR_ATENDIMENTO
      and      a.DT_ENTRADA_UNIDADE = b.DT_ENTRADA_UNIDADE
      and      b.nr_seq_pepo        = nr_seq_pepo_p;
   end if;  
   if (nr_seq_interno_w is not null) then
      insere_w_integracao_eritel(cd_setor_atendimento_w,cd_unidade_basica_w,cd_unidade_compl_w,nr_atendimento_w,'N',nm_usuario_w,cd_estabelecimento_w);
      commit;
   end if;   
   exception
   when others then
      null;
   end;
end if;   


if	(ie_momento_integracao_w = 'TM') then

	select	max(nr_atendimento),
		nvl(max(obter_unid_setor_cirurgia(nr_cirurgia_p,'NI')),0)
	into	nr_atendimento_w,
		nr_seq_interno_w
	from 	cirurgia
	where   nr_cirurgia = nr_cirurgia_p;
	
	select	max(cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno = nr_seq_interno_w
	and		dt_saida_unidade is null; 

	if	(ie_inicia_integracao_w = 'S') and
		(nr_atendimento_w is not null) and
		(nvl(cd_setor_atendimento_w,0) > 0) and
		(nr_seq_interno_w > 0) then
		gerar_cirurgia_hl7(nr_atendimento_w,nr_seq_interno_w,cd_setor_atendimento_w,'I');	
	end if;
	
	if	(ie_finaliza_integracao_w = 'S') and
		(nr_atendimento_w is not null) and
		(nvl(cd_setor_atendimento_w,0) > 0) and		
		(nr_seq_interno_w > 0) then
		gerar_cirurgia_hl7(nr_atendimento_w,nr_seq_interno_w,cd_setor_atendimento_w,'F');	
	end if;
	
end if;

select	ie_evento_painel
into	ie_evento_painel_w
from	evento_cirurgia
where	nr_sequencia	=	nr_seq_evento_p;	

if	(ie_evento_painel_w is not null) then
	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	agenda_paciente
	where	nr_cirurgia	=	nr_cirurgia_p;
	
	if	(nr_sequencia_w > 0) then
		gerar_dados_painel_cirurgia(ie_evento_painel_w, nr_sequencia_w, 'P', nm_usuario_p);
	end if;	
	
	if	(nr_seq_pepo_p is not null) and
		(nr_cirurgia_p is null) then
		open c01;
		loop
		fetch c01 into	
			nr_cirurgia_w;
		exit when c01%notfound;
			begin
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	agenda_paciente
			where	nr_cirurgia = nr_cirurgia_w;
			if	(nr_sequencia_w > 0) then
				gerar_dados_painel_cirurgia(ie_evento_painel_w, nr_sequencia_w, 'P', nm_usuario_p);
			end if;	
			end;
		end loop;
		close c01;
	end if;
end if;

if	(ie_gerar_proc_hemo_w = 'S') then
	hem_gerar_dados_cirurgia(nr_cirurgia_p, nm_usuario_p);
end if;

if	(nr_cirurgia_p > 0) and
	(nr_seq_evento_p > 0) then
	exec_regra_fim_evento(nr_cirurgia_p, nr_seq_evento_p, nm_usuario_p);
end if;

select	nvl(max(nr_sequencia),0)
into	nr_seq_agenda_w
from	agenda_paciente
where	nr_cirurgia = nr_cirurgia_p;


if	(ie_gera_mat_autorizacao_w = 'S') then
	--Materiais e medicamentos previstos
	open C02;
	loop
	fetch C02 into	
		nr_sequencia_w;
	exit when C02%notfound;
		begin
		if	(nr_seq_agenda_w > 0) then
			gerar_autor_regra(null, null, null,  nr_prescricao_w,  nr_sequencia_w,  null, 'RTM',  nm_usuario_p,nr_seq_agenda_w,  null,  null,  null, null, null,  '', '',  '');	
		else	
			gerar_autor_regra(nr_atendimento_w, null, null,  nr_prescricao_w,  nr_sequencia_w,  null, 'RTM',  nm_usuario_p,null,  null,  null,  null, null, null,  '', '',  '');	
		end if;	
		end;
	end loop;
	close C02;
		
	open C03;
	loop
	fetch C03 into	
		nr_prescr_mat_esp_w,
		nr_sequencia_w;
	exit when C03%notfound;
		begin
		--Materiais especiais
		if	(nr_seq_agenda_w > 0) then
			gerar_autor_regra(null, null, null,  nr_prescr_mat_esp_w,  nr_sequencia_w,  null, 'RTM',  nm_usuario_p,  nr_seq_agenda_w,  null,  null,  null, null, null,  '', '',  '');
		else
			gerar_autor_regra(nr_atendimento_w, null, null,  nr_prescr_mat_esp_w,  nr_sequencia_w,  null, 'RTM',  nm_usuario_p,  null,  null,  null,  null, null, null,  '', '',  '');
		end if;	
		end;
	end loop;
	close C03;
	
	open C04;
	loop
	fetch C04 into	--Alteracoes feitas pelo Fabricio Theiss conforem historico do dia 02/12/15 da OS 823663
		nr_sequencia_w,
		nr_seq_proc_interno_w;
	exit when C04%notfound;
		begin
		--Procedimentos adicionais previstos
		--if	(nr_seq_agenda_w > 0) then
		--	gerar_autor_regra(null, null, null,  nr_prescricao_w,  null,nr_sequencia_w, 'RTM',  nm_usuario_p,  nr_seq_agenda_w,  null,  null,  null, null, null,  '', '',  '');
		--else
		if	(nr_atendimento_w > 0) then
			gerar_autor_regra(nr_atendimento_w, null, null,  nr_prescricao_w,  null,  nr_sequencia_w, 'RTM',  nm_usuario_p,  null,  nr_seq_proc_interno_w,  null,  null, null, null,  '', '',  '');
		end if;	
		end;
	end loop;
	close C04;
end if;


begin
select	1
into	qt_existe_regra_setor_w
from	dis_regra_setor
where	cd_setor_atendimento = cd_setor_atendimento_w
and	rownum = 1;
exception
when others then
	qt_existe_regra_setor_w := 0;
end;

if	(qt_existe_regra_setor_w > 0) and
	(ie_finaliza_cirurgia_w = 'S') then
	intdisp_gerar_movimento(nr_atendimento_w, 'EPD', cd_setor_atendimento_w, nr_cirurgia_p);
end if;	

end  gerar_evento_cirurgia;
/
