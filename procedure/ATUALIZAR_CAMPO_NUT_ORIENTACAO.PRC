create or replace
procedure atualizar_campo_nut_orientacao(
		nr_sequencia_p	number,
		ds_campo_p	varchar2,
		ds_valor_p	varchar2,
		ie_campo_data_p	varchar2,
		ie_opcao_p	varchar2) is

/*	ie_opcao_p
	A - Atualizar
	D - Desfazer
	*/

ds_sql_w		varchar2(2000);
ds_parametros_w	varchar2(2000);
ds_sep_bv_w	varchar2(10);


begin
ds_parametros_w	:= '';
ds_sep_bv_w	:= obter_separador_bv;


if	(ie_opcao_p = 'A') then
	begin
	if	(ie_campo_data_p = 'S') then
		begin
		ds_sql_w	:=
			' update	nut_orientacao ' ||
			' set	'||ds_campo_p||'	= sysdate, ' ||
			' dt_atualizacao = sysdate, ' || 
			' nm_usuario = :nm_usuario ' ||
			' where	nr_sequencia	= :nr_sequencia ';
		end;
	else
		begin
		ds_sql_w	:=
			' update	nut_orientacao ' ||
			' set	'||ds_campo_p||'	= :ds_valor, ' ||
			' dt_atualizacao = sysdate, ' || 
			' nm_usuario = :nm_usuario ' || 
			' where	nr_sequencia	= :nr_sequencia ';
		ds_parametros_w	:= 'ds_valor=' || ds_valor_p || ds_sep_bv_w;
		end;
	end if;
	end;

elsif	(ie_opcao_p = 'D') then
	begin
	ds_sql_w	:=
		' update	nut_orientacao ' ||
		' set	'||ds_campo_p||'	= null, ' ||
		' dt_atualizacao = sysdate, ' || 
		' nm_usuario = :nm_usuario ' ||
		' where	nr_sequencia	= :nr_sequencia ';
	end;
end if;

ds_parametros_w	:= ds_parametros_w || 'nr_sequencia=' || nr_sequencia_p || ds_sep_bv_w
				|| 'nm_usuario=' || wheb_usuario_pck.get_nm_usuario || ds_sep_bv_w;

exec_sql_dinamico_bv('', ds_sql_w, ds_parametros_w);


commit;

end atualizar_campo_nut_orientacao;
/