create or replace
procedure pls_imp_monitor_guias_pend (	nr_seq_lote_p		in out 	pls_lot_monit_tiss_confer.nr_sequencia%type,
										ds_conteudo_p		in	varchar2,
										nm_arquivo_p		in	varchar2,
										nm_usuario_p		in	usuario.nm_usuario%type) is 


ds_conteudo_w			clob;
ds_valor_w				varchar2(255);
index_w					pls_integer;
nr_seq_lote_w			pls_lot_monit_tiss_confer.nr_sequencia%type;
nr_seq_lote_arquivo_w	pls_lot_monit_tiss_confer.nr_sequencia%type;
cd_operadora_ans_w 		pls_monit_tiss_cta_pen.cd_operadora_ans%type;
cd_cpf_cgc_prest_exec_w pls_monit_tiss_cta_pen.cd_cpf_cgc_prest_exec%type;
cd_guia_prestador_w 	pls_monit_tiss_cta_pen.cd_guia_prestador%type;
cd_guia_operadora_w 	pls_monit_tiss_cta_pen.cd_guia_operadora%type;
cd_reembolso_w 			pls_monit_tiss_cta_pen.cd_reembolso%type;	
cd_cnes_prest_w			pls_monit_tiss_cta_pen.cd_cnes_prest_exec%type;
					

					
begin

--Essa rotina � chamada m�ltiplas vezes via aplica��o, na primeira, estar� passando nr_seq_lote_p null e ent�o verificar� 
-- Verificado ainda nessa primeira etapa, se j� foi importado arquivo com esse nome.
if	(nr_seq_lote_p is null) then
		
	if	(nr_seq_lote_w is null) then

		-- verifica  se h� algum que tenha mesmo nome do arquivo que est� sendo importado, 
		--barrando a importa��o e exibindo mensagem  em caso afirmativo. Essa rotina � chamada para cada linha do arquivo � partir de 
		--loop no Delphi e ent�o essa verifica��o somente deve ororrer quando n�o est� passando o nr_seq_lote_p pois quer dizer que � a primeira
		--itera��o, se passar pela primeira itera��o significa que o arquivo ainda n�o foi importado com esse nome, ent�o deve permitir a itera��o 
		-- sobre as demais linhas do arquivo.
		select	max(nr_sequencia)
		into 	nr_seq_lote_arquivo_w
		from	pls_lot_monit_cta_pe
		where	nm_arquivo = nm_arquivo_p;			
	
		if (nr_seq_lote_arquivo_w is not null) then
			--wheb_mensagem_pck.exibir_mensagem_abort(1029520);
			nr_seq_lote_p := -1;
		else
			insert	into	pls_lot_monit_cta_pe
					(	nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
						nm_usuario, nm_usuario_nrec, nm_arquivo)	
			values	(	pls_lot_monit_cta_pe_seq.nextval, sysdate, sysdate, 
						nm_usuario_p, nm_usuario_p, nm_arquivo_p)		
			returning nr_sequencia into nr_seq_lote_w;
		end if;
	
	end if;
else
	nr_seq_lote_w := nr_seq_lote_p;	
end if;


if ( nvl(nr_seq_lote_p,0) != -1) then
	ds_conteudo_w := ds_conteudo_p;
	index_w := 0;

	while 	(ds_conteudo_w is not null) loop
		
		if	(instr(ds_conteudo_w,',') = 0) then
			ds_valor_w	:= ds_conteudo_w;
			ds_conteudo_w	:= null;
		else
			ds_valor_w := substr(ds_conteudo_w, 1, instr(ds_conteudo_w,',') -1);
			ds_conteudo_w := substr(ds_conteudo_w,instr(ds_conteudo_w,',') + 1,length(ds_conteudo_w));
		end if;
		
		index_w := index_w + 1;
		
		--CNES Prestador, CNPJ/CPF Prestador, Guia Prestador, Guia Operadora, Reembolso
		if	(index_w = 1) then
			cd_operadora_ans_w := ds_valor_w;
		elsif	(index_w = 2) then
			cd_cnes_prest_w := ds_valor_w;
		elsif	(index_w = 3) then
			cd_cpf_cgc_prest_exec_w := ds_valor_w;
		elsif	(index_w = 4) then
			cd_guia_prestador_w := ds_valor_w;
		elsif	(index_w = 5) then
			cd_guia_operadora_w := ds_valor_w;
		elsif	(index_w = 6) then
			cd_reembolso_w := ds_valor_w;
		end if;		
		
	end loop;

	insert	into	pls_monit_tiss_cta_pen
		(	cd_cnes_prest_exec, cd_cpf_cgc_prest_exec, cd_guia_operadora,    
		    cd_guia_prestador, cd_operadora_ans, cd_reembolso,         
		    dt_atualizacao, dt_atualizacao_nrec, nm_usuario,           
		    nm_usuario_nrec, nr_seq_lote, nr_sequencia
		)
	values (cd_cnes_prest_w, cd_cpf_cgc_prest_exec_w, cd_guia_operadora_w,
			cd_guia_prestador_w, cd_operadora_ans_w, cd_reembolso_w,
			sysdate, sysdate, nm_usuario_p,
			nm_usuario_p, nr_seq_lote_w, pls_monit_tiss_cta_pen_seq.nextval);
			
	nr_seq_lote_p := nr_seq_lote_w;
	
end if;	

commit;

end pls_imp_monitor_guias_pend;
/
