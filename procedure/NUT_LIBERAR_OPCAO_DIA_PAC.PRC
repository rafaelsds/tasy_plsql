create or replace
procedure Nut_Liberar_Opcao_Dia_Pac
		(nr_atendimento_p		number,
		cd_setor_atendimento_p		number,
		dt_referencia_p			date,
		nr_seq_servico_p		number,
		cd_acompanhante_p		varchar2,
		nr_seq_acompanhante_p		number,
		nm_usuario_p			varchar2,
		ie_opcao_p			varchar2 default 'L') is

ie_status_w		varchar2(5);		
nr_seq_opcao_w		number(10,0);	
dt_liberacao_w		date;
nr_sequencia_w		number(10);

Cursor C01 is
	SELECT	a.nr_sequencia
	FROM	nut_pac_opcao_rec a,
		nut_atend_serv_dia b
	WHERE	a.nr_seq_servico_dia		= b.nr_sequencia
	AND	b.cd_setor_atendimento	    	= cd_setor_atendimento_p
	AND	b.nr_atendimento		= nr_atendimento_p
	AND	TRUNC(b.DT_SERVICO)		= TRUNC(dt_referencia_p)
	AND	b.nr_seq_servico		= nr_seq_servico_p
	AND	(((cd_acompanhante_p = 0) 	AND (nvl(cd_acompanhante,0) = 0))     OR ((cd_acompanhante_p <> 0)     AND  (nvl(cd_acompanhante,0)     = cd_acompanhante_p)))
	AND	(((nr_seq_acompanhante_p = 0)   AND (nvl(nr_seq_acompanhante,0) = 0)) OR ((nr_seq_acompanhante_p <> 0) AND  (nvl(nr_seq_acompanhante,0) = nr_seq_acompanhante_p)));
	
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	if	(ie_opcao_p = 'D') then
		update 	nut_pac_opcao_rec
		set	dt_liberacao	= null,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_w;
	else
		update 	nut_pac_opcao_rec
		set	dt_liberacao	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_w;
	end if;
	end;
end loop;
close C01;


commit;

end Nut_Liberar_Opcao_Dia_Pac;
/