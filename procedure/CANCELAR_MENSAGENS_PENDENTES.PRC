create or replace
procedure	cancelar_mensagens_pendentes(
			ie_tipo_mensagem_p		number,
			nr_documento_p			number,
			ie_tipo_documento_p		varchar2,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

begin

sup_cancela_email_pendente(ie_tipo_mensagem_p,nr_documento_p,ie_tipo_documento_p,cd_estabelecimento_p,nm_usuario_p);

commit;

end cancelar_mensagens_pendentes;
/