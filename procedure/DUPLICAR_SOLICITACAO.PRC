create or replace
procedure	Duplicar_Solicitacao(nr_sequencia_p number) is

cd_pessoa_usuario_w varchar(10);
nm_usuario_w varchar(15);


begin

nm_usuario_w := obter_usuario_ativo;
cd_pessoa_usuario_w := obter_dados_usuario_opcao(nm_usuario_w,'C');

insert into trans_solicitacao(
				nr_sequencia, dt_atualizacao, nm_usuario, nr_atendimento, cd_setor_origem, cd_pessoa_fisica, dt_atualizacao_nrec, nm_usuario_nrec,
				cd_transportador, cd_equipamento, ie_status, ie_regra_prioridade, cd_solicitante, nr_ramal, nr_prontuario, ie_oxigenio, 
				nr_seq_tipo_remocao, ds_condicao_clinica_pac, ie_med_bomba_infusao, ds_observacao, dt_prevista, nr_seq_local_externo, nm_contato, nr_telefone_contato, nm_acomp_pac, 
				ds_localidade, ie_tipo_solicitacao, ie_pac_vulneravel, qt_peso_pac)
select	trans_solicitacao_seq.nextval, sysdate, nm_usuario_w, nr_atendimento, cd_setor_origem, cd_pessoa_fisica, sysdate, nm_usuario_w, 
		cd_transportador, cd_equipamento,1, ie_regra_prioridade, cd_pessoa_usuario_w, nr_ramal, nr_prontuario, ie_oxigenio, 
		nr_seq_tipo_remocao, ds_condicao_clinica_pac, ie_med_bomba_infusao, ds_observacao, dt_prevista, nr_seq_local_externo, nm_contato, nr_telefone_contato, nm_acomp_pac, 
		ds_localidade, ie_tipo_solicitacao, ie_pac_vulneravel, qt_peso_pac
from	trans_solicitacao
where 	nr_sequencia = nr_sequencia_p;		
			
commit;

end Duplicar_Solicitacao;
/

