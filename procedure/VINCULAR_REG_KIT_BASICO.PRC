create or replace
procedure vincular_reg_kit_basico(	cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				nr_seq_reg_kit_p		number,
				nr_seq_kit_p		number) is 

ie_ordem_w		varchar2(1);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_kit_material_w		number(5);
nr_sequencia_w		number(10);
nr_seq_kit_estoque_w	number(10);	
nr_seq_pedido_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
dt_montagem_w		date;		
nm_usuario_montagem_w	varchar2(15);
cd_local_estoque_w	number(4);
ie_status_w		varchar2(1);
nr_sequencia_comp_w	number(10);
cd_material_w		number(6);
qt_material_w		number(11,4);
ie_gerado_barras_w	varchar2(1);
nr_seq_lote_fornec_w	number(10);
nr_seq_motivo_exclusao_w 	number(10);
nr_seq_item_kit_w		number(5);
dt_exclusao_w		date;
nm_usuario_exclusao_w 	varchar2(15);
					
				
Cursor C01 is
	select 	nr_sequencia,
		cd_kit_material, 
		dt_montagem, 
		nm_usuario_montagem, 
		cd_local_estoque,
		ie_status
	from	kit_estoque
	where  	nr_seq_reg_kit = nr_seq_reg_kit_p;

Cursor C02 is
	select	nr_sequencia, 
		cd_material, 
		qt_material, 
		ie_gerado_barras, 
		nr_seq_lote_fornec,
		nr_seq_motivo_exclusao,
		nr_seq_item_kit, 
		dt_exclusao,
		nm_usuario_exclusao 
	from   kit_estoque_comp
	where  nr_seq_kit_estoque = nr_sequencia_w;		

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	cd_kit_material_w,
	dt_montagem_w,
	nm_usuario_montagem_w,
	cd_local_estoque_w,
	ie_status_w;
exit when C01%notfound;
	begin
	
	select	kit_estoque_seq.nextval
	into	nr_seq_kit_estoque_w
	from	dual;
	
	insert 	into kit_estoque(
			nr_sequencia, 
			cd_kit_material,
			dt_atualizacao,
			nm_usuario,
			dt_montagem,
			nm_usuario_montagem,
			dt_utilizacao,           
			nm_usuario_util,
			nr_cirurgia,
			cd_estabelecimento,
			cd_medico,
			nr_prescricao,
			nr_atendimento,
			cd_local_estoque,
			nr_seq_reg_kit,
			ie_status,
			nr_seq_reg_kit_basico)
	values	(	nr_seq_kit_estoque_w,
			cd_kit_material_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			cd_estabelecimento_p,
			null,
			null,
			null,
			cd_local_estoque_w,
			nr_seq_kit_p,
			null,
			nr_seq_reg_kit_p);
	end;

	open C02;
	loop
	fetch C02 into	
		nr_sequencia_comp_w, 
		cd_material_w, 
		qt_material_w, 
		ie_gerado_barras_w, 
		nr_seq_lote_fornec_w,
		nr_seq_motivo_exclusao_w,
		nr_seq_item_kit_w, 
		dt_exclusao_w,
		nm_usuario_exclusao_w;
	exit when C02%notfound;
		begin
		
		insert into kit_estoque_comp (
				nr_seq_kit_estoque, 
				nr_sequencia, 
				cd_material, 
				dt_atualizacao,
				nm_usuario, 
				qt_material, 
				ie_gerado_barras, 
				nr_seq_lote_fornec, 
				nr_seq_motivo_exclusao, 
				nr_seq_item_kit, 
				dt_exclusao, 
				nm_usuario_exclusao,
				cd_material_troca, 
				nm_usuario_troca, 
				ds_motivo_troca)
			values(	nr_seq_kit_estoque_w,
				nr_sequencia_comp_w,
				cd_material_w, 
				sysdate,
				nm_usuario_p,
				qt_material_w, 
				ie_gerado_barras_w, 
				nr_seq_lote_fornec_w,
				nr_seq_motivo_exclusao_w,
				nr_seq_item_kit_w, 
				dt_exclusao_w,	
				nm_usuario_exclusao_w,
				null,
				null,
				null);
		end;
	end loop;
	close C02;	
		
end loop;
close C01;

update	kit_estoque_reg
set	dt_utilizacao = sysdate,
	nm_usuario_util = nm_usuario_p
where	nr_sequencia = 	nr_seq_reg_kit_p;

commit;

end VINCULAR_REG_KIT_BASICO;
/