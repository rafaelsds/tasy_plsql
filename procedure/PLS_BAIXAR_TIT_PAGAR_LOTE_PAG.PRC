create or replace
procedure pls_baixar_tit_pagar_lote_pag
		(nr_seq_lote_p		pls_lote_pagamento.nr_sequencia%type,
		 nr_seq_pagamento_prest_p	pls_pagamento_prestador.nr_sequencia%type,
		 ie_acao_p		varchar2,
		 nm_usuario_p		usuario.nm_usuario%type) is

/*
ie_acao_p
	'F' - Fechamento do lote
	'A' - Reabertura do Lote
*/

cd_tipo_baixa_pgto_w			pls_parametro_pagamento.cd_tipo_baixa_pgto%type;
nr_seq_trans_fin_pag_pgto_w		pls_parametro_pagamento.nr_seq_trans_fin_pag_pgto%type;
cd_estabelecimento_w			pls_parametro_pagamento.cd_estabelecimento%type;
nr_seq_baixa_w				titulo_pagar_baixa.nr_sequencia%type;

Cursor C01 (	nr_seq_lote_cp			pls_lote_pagamento.nr_sequencia%type) is
select	b.nr_titulo,
	decode(ie_acao_p, 'F', abs(a.vl_movimento), abs(a.vl_movimento) * -1) vl_saldo_titulo
from	pls_pagamento_prestador d,
	pls_pagamento_item c,
	titulo_pagar	b,
	pls_evento_movimento a
where	a.nr_tit_pagar_vinculado	= b.nr_titulo
and	a.nr_seq_pagamento_item		= c.nr_sequencia
and	c.nr_seq_pagamento		= d.nr_sequencia
and	d.nr_sequencia			= nvl(nr_seq_pagamento_prest_p, d.nr_sequencia)
and	d.ie_cancelamento		is null
and	a.nr_seq_lote_pgto		= nr_seq_lote_cp
union all
select	c.nr_titulo,
        decode(ie_acao_p, 'F', abs(b.vl_item), abs(b.vl_item) * -1) vl_saldo_titulo
from	pls_pagamento_prestador a,
	pls_pagamento_item b,
	titulo_pagar c
where 	a.nr_sequencia 		= b.nr_seq_pagamento
and   	c.nr_titulo 		= b.nr_tit_pagar_origem
and   	a.nr_sequencia 		= nvl(nr_seq_pagamento_prest_p, a.nr_sequencia)
and   	a.ie_cancelamento is null
and not exists(	select 	1
                from   	pls_evento_movimento x
                where  	x.nr_seq_pagamento_item = b.nr_sequencia)
and	a.nr_seq_lote 		= nr_seq_lote_cp;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	pls_lote_pagamento
where	nr_sequencia		= nr_seq_lote_p;

select	max(cd_tipo_baixa_pgto),
	max(nr_seq_trans_fin_pag_pgto)
into	cd_tipo_baixa_pgto_w,
	nr_seq_trans_fin_pag_pgto_w
from	pls_parametro_pagamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(cd_tipo_baixa_pgto_w is null) or 
	(nr_seq_trans_fin_pag_pgto_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(312374);
	-- N�o � poss�vel realizar este processo!
	-- Verifique se os campos "Trans fin pagar pag produ��o" e "Tipo baixa pag produ��o" est�o informados nos par�metros do pagamento de produ��o (OPS - Gest�o de Operadoras -> Par�metros OPS -> Pagamento produ��o m�dica).
end if;

for r_c01_w in C01(nr_seq_lote_p) loop

	baixa_titulo_pagar
		(cd_estabelecimento_w,
		cd_tipo_baixa_pgto_w,
		r_c01_w.nr_titulo,
		r_c01_w.vl_saldo_titulo,
		nm_usuario_p,
		nr_seq_trans_fin_pag_pgto_w,
		null,
		null,
		sysdate,
		null,
		null);

	-- Atualiza a observa��o da baixa informando o lote de pagamento que gerou a mesma
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_baixa_w
	from	titulo_pagar_baixa
	where	nr_titulo	= r_c01_w.nr_titulo;

	update	titulo_pagar_baixa
	set	ds_observacao		= 'Baixa gerada atrav�s do lote, ' || nr_seq_lote_p || ', de pagamento de produ��o m�dica.'
	where	nr_sequencia		= nr_seq_baixa_w
	and	nr_titulo		= r_c01_w.nr_titulo;
	exception
	when others then
		null;
	end;

	Atualizar_Saldo_Tit_pagar(r_c01_w.nr_titulo, nm_usuario_p);

end loop;

end pls_baixar_tit_pagar_lote_pag;
/