create or replace
procedure Same_Desfazer_Devolver_Solic (	nr_seq_solic_p		number,
					nm_usuario_p		varchar2) is

qt_devolvido_w	number(10,0);

begin

/* ATEN��O: ESTA PROCEDURE N�O TEM COMMIT, POIS SOMENTE � CHAMADA ATRAV�S DE OUTRA PROCEDURE.
 *          CASO SEJA NECESS�RIO IMPLEMENTAR O COMMIT, FAVOR VERIFICAR OS PONTOS ONDE PODEM
 *          OCORRER ROLLBACKS E TRATAR OS MESMOS DE OUTRA FORMA.
 */

select	count(*)
into	qt_devolvido_w
from	same_solic_pront_envelope
where	nr_seq_solic	= nr_seq_solic_p
and	dt_devolucao	is not null;

if	(qt_devolvido_w = 0) then
	update	same_solic_pront
	set	ie_status	= 'E',
		dt_devolucao	= null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_solic_p;
end if;

end Same_Desfazer_Devolver_Solic;
/