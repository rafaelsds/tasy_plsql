create or replace
procedure atualizar_auditoria_corrente	(	nr_seq_grupo_atual_p	number,
					nr_seq_analise_p		number,
					nm_usuario_p		varchar2,
					ds_auditoria_cor_p	out varchar2) is

nm_auditor_atual_w	varchar2(255);

begin

ds_auditoria_cor_p	:= '';

if	(nr_seq_grupo_atual_p > 0) then
	pls_obter_se_auditor_dif(nr_seq_grupo_atual_p, nr_seq_analise_p, nm_usuario_p, nm_auditor_atual_w);
	if	(nvl(nm_auditor_atual_w,'') = '') then
		pls_atualizar_auditor_grupo(nr_seq_grupo_atual_p, nr_seq_analise_p, nm_usuario_p);
	elsif	(nm_usuario_p <> nm_auditor_atual_w) then
		ds_auditoria_cor_p := obter_texto_dic_objeto(92699, wheb_usuario_pck.get_nr_seq_idioma, 'NM_AUDITOR=' || substr(obter_nome_usuario(nm_auditor_atual_w),1,100) || ';');
	end if;
end if;

commit;

end atualizar_auditoria_corrente;
/