create or replace
procedure inativar_atend_tipo_ser_terc(
			nm_usuario_p		Varchar2,
			nr_sequencia_p	number) is 

begin
update	atend_tipo_servico_ter
set	nm_usuario_inativacao = nm_usuario_p,
	dt_inativacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end inativar_atend_tipo_ser_terc;
/ 