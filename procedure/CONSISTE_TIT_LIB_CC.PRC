create or replace
procedure consiste_tit_lib_cc(nr_titulo_p		in number,
			      cd_estabelecimento_p	in number,
			      nm_usuario_p		in varchar2) is 

			      
cd_centro_custo_w		titulo_pagar_classif.cd_centro_custo%type;
ds_retorno_w			varchar2(1) := 'N';

Cursor C01 is /*Buscar todos os centro de custo do t�tulo*/
select	cd_centro_custo
from	titulo_pagar_classif
where	nr_titulo  = nr_titulo_p;
		      			      
begin

open C01;
loop
fetch C01 into	
	cd_centro_custo_w;
exit when C01%notfound or ds_retorno_w = 'S';
	begin
		select  substr(obter_cc_setor_usuario_lib(nm_usuario_p,cd_estabelecimento_p,cd_centro_custo_w),1,1)
		into	ds_retorno_w
		from 	dual;
	end;
end loop;
close C01;

if (ds_retorno_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(296118);
end if;

commit;

end consiste_tit_lib_cc;
/