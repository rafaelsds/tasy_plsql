create or replace
procedure pls_oc_cta_val_calc_zero_item(dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_selecao_ocor_cta.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Consistir a regra da glosa 9919, verificando se existe regra de pre�o para o procedimento,
por�m seu valor est� zerado. Tamb�m foi tratada a situa��o aonde dever� desconsiderar a regra de libera��o
"Valor calculado zerado", para que a mesma n�o seja considerada.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Altera��es:
------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
null;
end pls_oc_cta_val_calc_zero_item;
/
