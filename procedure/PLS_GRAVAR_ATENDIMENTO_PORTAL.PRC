create or replace
procedure pls_gravar_atendimento_portal
			(	nr_seq_atendimento_p	Number,
				nr_seq_evento_p		Number,
				nm_usuario_envio_p	Varchar2,
				dt_envio_p		Date,
				ds_mensagem_p		Varchar2,
				ds_jid_usuario_envio_p	Varchar2,
				ds_jid_consulta_p	Varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_atendimento_w	Number(10);

begin

nr_seq_atendimento_w	:= nr_seq_atendimento_p;

if	(nr_seq_atendimento_w is null and ds_jid_consulta_p is not null) then
	select	max(nr_seq_atendimento)
	into	nr_seq_atendimento_w
	from	pls_atendimento_chat
	where	ds_jid_usuario_envio = ds_jid_consulta_p;
end if;

if	(nr_seq_atendimento_w is not null ) then
	insert into pls_atendimento_chat(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, ds_mensagem,
		ie_status_atendimento, nr_seq_atendimento, nr_seq_solicitacao,
		dt_envio_mensagem, nm_usuario_envio, ds_jid_usuario_envio)
	values(	pls_atendimento_chat_seq.nextval, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, ds_mensagem_p,
		'A', nr_seq_atendimento_w, null, 
		dt_envio_p, nm_usuario_envio_p, ds_jid_usuario_envio_p);
end if;

commit;

end pls_gravar_atendimento_portal;
/