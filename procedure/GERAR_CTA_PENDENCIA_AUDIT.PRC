create or replace
procedure gerar_cta_pendencia_audit(	nr_seq_pendencia_p	Number,
				nr_seq_auditoria_p	Number,
				nm_usuario_p		Varchar2) is 

qt_audit_pend_w	number(10,0);				
				
begin

if	(nvl(nr_seq_pendencia_p,0) <> 0) and
	(nvl(nr_seq_auditoria_p,0) <> 0) then
	
	select	count(*)
	into	qt_audit_pend_w
	from	cta_pendencia_audit
	where	nr_seq_pendencia = nr_seq_pendencia_p
	and	nr_seq_auditoria = nr_seq_auditoria_p;
	
	if	(qt_audit_pend_w = 0) then
	
		insert into cta_pendencia_audit (
			nr_sequencia,
			nr_seq_pendencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_auditoria
		) values (
			cta_pendencia_audit_seq.NextVal,
			nr_seq_pendencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_auditoria_p
		);
		
	end if;
	
end if;

commit;

end gerar_cta_pendencia_audit;
/