create or replace
procedure update_chosen_departments(
				nm_usuario_p 			varchar2,
				cd_novo_setor_atendimento_p	number) 	is
				
nr_seq_ocorrencia_w		ocorrencias_transf_atend.nr_sequencia%type;
qt_times_selected_w		number;
nm_records_count_w		number;
dt_record_update_w		date;

begin

	select	SYSDATE
	into 	dt_record_update_w
	from	dual;
	
	select	count(*)
	into	nm_records_count_w
	from	chosen_departments
	where	nm_user = nm_usuario_p;

	select	qt_times_selected
	into	qt_times_selected_w
	from	chosen_departments
	where	nm_user = nm_usuario_p
	and	cd_department = cd_novo_setor_atendimento_p;
	
	if( qt_times_selected_w > 0 ) then 

		update	CHOSEN_DEPARTMENTS
		set	DT_RECORD_UPDATE = dt_record_update_w,
			QT_TIMES_SELECTED = (qt_times_selected_w + 1)
		where	nm_user = nm_usuario_p
		and	cd_department = cd_novo_setor_atendimento_p;
		
	else 
		
		if ( nm_records_count_w >= 10 ) then
			delete 	from 	CHOSEN_DEPARTMENTS
			where 	NR_SEQUENCIA in 
					(select	*
					from	(select NR_SEQUENCIA 
							from	CHOSEN_DEPARTMENTS
							where	nm_user = nm_usuario_p
							order by QT_TIMES_SELECTED, DT_RECORD_CREATION )
					where	ROWNUM <= 1);					
					
		end if;

		select	ocorrencias_transf_atend_seq.nextval
		into	nr_seq_ocorrencia_w
		from	dual;

		insert into CHOSEN_DEPARTMENTS(NR_SEQUENCIA, DT_RECORD_CREATION, DT_RECORD_UPDATE, NM_USER, CD_DEPARTMENT, QT_TIMES_SELECTED)
		values(nr_seq_ocorrencia_w, dt_record_update_w, dt_record_update_w, nm_usuario_p, cd_novo_setor_atendimento_p, 1 );

	end if;
	
	exception
		when no_data_found then
		
			if ( nm_records_count_w >= 10 ) then
				delete 	from 	CHOSEN_DEPARTMENTS
				where 	NR_SEQUENCIA in 
						(select	*
						from	(select NR_SEQUENCIA 
								from	CHOSEN_DEPARTMENTS
								where	nm_user = nm_usuario_p
								order by QT_TIMES_SELECTED, DT_RECORD_CREATION )
						where	ROWNUM <= 1);					
					
			end if;
		
			select	ocorrencias_transf_atend_seq.nextval
			into	nr_seq_ocorrencia_w
			from	dual;

			insert into CHOSEN_DEPARTMENTS(NR_SEQUENCIA, DT_RECORD_CREATION, DT_RECORD_UPDATE, NM_USER, CD_DEPARTMENT, QT_TIMES_SELECTED)
			values(nr_seq_ocorrencia_w, dt_record_update_w, dt_record_update_w, nm_usuario_p, cd_novo_setor_atendimento_p, 1 );

	commit;

end update_chosen_departments;
/