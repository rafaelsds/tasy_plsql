create or replace
procedure pls_oc_cta_tratar_val_43_imp(	nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

nr_indice_w				pls_integer;
qt_conta_dif_w				pls_integer;
ie_gerar_ocorrencia_w			varchar2(1);				
ds_observacao_w				varchar2(4000);
ds_observacao_1_w			varchar2(255);
ds_observacao_2_w			varchar2(255);
ds_observacao_3_w			varchar2(255);
ds_observacao_4_w			varchar2(255);
ds_observacao_5_w			varchar2(255);
ds_observacao_6_w			varchar2(255);
ds_observacao_7_w			varchar2(255);
ds_observacao_8_w			varchar2(255);
ds_observacao_9_w			varchar2(255);
ds_observacao_10_w			varchar2(255);
ds_observacao_11_w			varchar2(255);
tb_seq_selecao_w			pls_util_cta_pck.t_number_table;
tb_valido_w				pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w				pls_util_cta_pck.t_varchar2_table_4000;

--Validar autoriza��o do item
cursor C01 (	nr_seq_oc_cta_comb_pc		pls_oc_cta_combinada.nr_sequencia%type) is
	select	nvl(ie_valida_aut_item,'N')	ie_valida_aut_item,
		nvl(ie_valida_qtd_util,'N')	ie_valida_qtd_util,
		nvl(ie_somente_item_autor,'N')	ie_somente_item_autor,
		qt_minima			qt_minima
	from	pls_oc_cta_val_aut_item
	where	nr_seq_oc_cta_comb		= nr_seq_oc_cta_comb_pc;
	
--Procedimentos e Materiais selecionados
cursor C02 (	nr_id_transacao_pc		pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	sel.nr_sequencia						nr_seq_selecao,
		'P'								ie_tipo_registro,
		conta.nr_seq_guia_conv						nr_seq_guia,	
		proc.ie_tipo_despesa_conv					ie_tipo_despesa, 
		proc.ie_origem_proced_conv					ie_origem_proced, 
		proc.cd_procedimento_conv					cd_procedimento, 
		null								nr_seq_material,
		conta.cd_estabelecimento					cd_estabelecimento,
		conta.nr_sequencia						nr_seq_conta,
		conta.cd_guia_ok_conv						cd_guia_ok,
		conta.nr_seq_segurado_conv					nr_seq_segurado,
		(	select	max(guia.ie_status)
			from	pls_guia_plano	guia
			where	guia.nr_sequencia = conta.nr_seq_guia_conv)	ie_status_guia,
		(	select	sum(qt_procedimento)
			from	pls_conta_autor_utilizada_v
			where	nr_seq_guia	 = conta.nr_seq_guia_conv
			and	cd_procedimento  = proc.cd_procedimento_conv
			and	ie_origem_proced = proc.ie_origem_proced_conv)	qt_dif_proc_mat
	from	pls_oc_cta_selecao_imp		sel, 
		pls_conta_proc_imp		proc,
		pls_conta_imp			conta
	where	sel.nr_id_transacao		= nr_id_transacao_pc 
	and	sel.ie_valido			= 'S'
	and	proc.nr_sequencia		= sel.nr_seq_conta_proc
	and	conta.nr_sequencia		= proc.nr_seq_conta		
	union all
	select	sel.nr_sequencia						nr_seq_selecao, 
		'M'								ie_tipo_registro,
		conta.nr_seq_guia_conv						nr_seq_guia, 
		mat.ie_tipo_despesa_conv					ie_tipo_despesa,
		null								ie_origem_proced, 
		null								cd_procedimento, 
		mat.nr_seq_material_conv					nr_seq_material,
		conta.cd_estabelecimento					cd_estabelecimento,
		conta.nr_sequencia						nr_seq_conta,
		conta.cd_guia_ok_conv						cd_guia_ok,
		conta.nr_seq_segurado_conv					nr_seq_segurado,
		(	select	max(guia.ie_status)
			from	pls_guia_plano	guia
			where	guia.nr_sequencia = conta.nr_seq_guia_conv)	ie_status_guia,
		(	select	sum(qt_procedimento)
			from	pls_conta_autor_utilizada_v
			where	nr_seq_guia	= conta.nr_seq_guia_conv
			and	nr_seq_material	= mat.nr_seq_material_conv)	qt_dif_proc_mat
	from	pls_oc_cta_selecao_imp		sel, 
		pls_conta_mat_imp		mat,
		pls_conta_imp			conta
	where	sel.nr_id_transacao		= nr_id_transacao_pc 
	and	sel.ie_valido			= 'S'
	and	mat.nr_sequencia		= sel.nr_seq_conta_mat
	and	conta.nr_sequencia		= mat.nr_seq_conta;
	
-- Obter o saldo referente a utiliza��o de di�rias para a guia especificada.
cursor C03 (	nr_seq_guia_pc				pls_guia_plano.nr_sequencia%type,
		ie_origem_proced_pc			pls_conta_proc_imp.ie_origem_proced_conv%type,
		cd_procedimento_pc			pls_conta_proc_imp.cd_procedimento_conv%type,
		cd_estabelecimento_pc			estabelecimento.cd_estabelecimento%type) is
	select	nvl(max(conta_autor.qt_saldo),0)	qt_saldo,
		nvl(max(conta_autor.qt_utilizada),0)	qt_utilizada,
		nvl(max(conta_autor.qt_autorizada),0)	qt_autorizada,
		count(0)				qt_registro
	from	table(pls_conta_autor_pck.obter_dados(nr_seq_guia_pc, 'D', cd_estabelecimento_pc, ie_origem_proced_pc, cd_procedimento_pc)) conta_autor;
	
-- Obter o saldo restante para o procedimento indicado
cursor C04 (	nr_seq_guia_pc				pls_guia_plano.nr_sequencia%type,
		ie_origem_proced_pc			pls_conta_proc_imp.ie_origem_proced_conv%type,
		cd_procedimento_pc			pls_conta_proc_imp.cd_procedimento_conv%type,
		cd_estabelecimento_pc			estabelecimento.cd_estabelecimento%type) is 
	select	nvl(max(conta_autor.qt_saldo),0)	qt_saldo,
		nvl(max(conta_autor.qt_utilizada),0)	qt_utilizada,
		nvl(max(conta_autor.qt_autorizada),0)	qt_autorizada,
		count(0)				qt_registro
	from	table(pls_conta_autor_pck.obter_dados(nr_seq_guia_pc, 'P', cd_estabelecimento_pc, ie_origem_proced_pc, cd_procedimento_pc)) conta_autor;

-- Obter o saldo referente ao material especificado.
cursor C05 (	nr_seq_guia_pc				pls_guia_plano.nr_sequencia%type,
		nr_seq_material_pc			pls_conta_mat_imp.nr_seq_material_conv%type,
		cd_estabelecimento_pc			estabelecimento.cd_estabelecimento%type) is
	select	nvl(max(conta_autor.qt_saldo),0)	qt_saldo,
		nvl(max(conta_autor.qt_utilizada),0)	qt_utilizada,
		nvl(max(conta_autor.qt_autorizada),0)	qt_autorizada,
		count(0)				qt_registro
	from	table(pls_conta_autor_pck.obter_dados(nr_seq_guia_pc, 'M', cd_estabelecimento_pc, null, null, nr_seq_material_pc)) conta_autor;

begin
--- Deve exisitr informa��o da regra para executar a valida��o
if	(nr_seq_combinada_p is not null) and
	(nr_id_transacao_p is not null)  then
	--N�o foi encontrada uma autoriza��o para este atendimento.
	ds_observacao_1_w  := wheb_mensagem_pck.get_texto(380883);
	--A solicita��o de autoriza��o para este atendimento n�o foi autorizada pela operadora. Status da solicita��o: 
	ds_observacao_2_w  := wheb_mensagem_pck.get_texto(380884);
	--A quantidade de di�rias utilizadas ultrapassa a quantidade de di�rias autorizadas para esta guia.
	ds_observacao_3_w  := wheb_mensagem_pck.get_texto(380901);
	--Quantidade autorizada:
	ds_observacao_4_w  := wheb_mensagem_pck.get_texto(380902);
	-- | Quantidade utilizada: 
	ds_observacao_5_w  := wheb_mensagem_pck.get_texto(380903);
	-- | Saldo: 
	ds_observacao_6_w  := wheb_mensagem_pck.get_texto(380905);
	--Contas deste atendimento que utilizaram di�rias:
	ds_observacao_7_w  := wheb_mensagem_pck.get_texto(380906);
	--O sistema est� considerando mais de um grupo de autoriza��es v�lidas para este item, sendo que apenas um grupo deveria estar sendo considerado. 
	ds_observacao_8_w  := wheb_mensagem_pck.get_texto(381336);
	--Existe uma autoriza��o v�lida para esta guia, por�m a cobran�a desta di�ria n�o foi autorizada.
	ds_observacao_9_w  := wheb_mensagem_pck.get_texto(381338);
	--Existe uma autoriza��o v�lida para esta guia, por�m a cobran�a deste procedimento n�o foi autorizada.
	ds_observacao_10_w := wheb_mensagem_pck.get_texto(381417);
	--Existe uma autoriza��o v�lida para esta guia, por�m a cobran�a deste material n�o foi autorizada.
	ds_observacao_11_w := wheb_mensagem_pck.get_texto(381426);
	
	-- Informa��es da regra de valida��o da carteira.
	for	r_C01_w in C01(nr_seq_combinada_p) loop
		-- Verificar se a regra ir� validar os itens n�o autorizados ou a quantidade.
		if	(r_C01_w.ie_valida_aut_item = 'S') then
			-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
			pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null);
					
			--limpa as vari�veis
			pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
								tb_valido_w,
								tb_observacao_w);	
			nr_indice_w := 0;
			--Cursor com os materiais e procedimentos selecionados
			for	r_C02_w in C02(nr_id_transacao_p) loop
				
				ie_gerar_ocorrencia_w := 'S';
				ds_observacao_w	      := '';
				-- Verificar se a conta em quest�o teve uma guia v�ilida, se n�o tiver a ocorr�ncia ser� gerada					
				if	(r_C02_w.nr_seq_guia is null and r_C01_w.ie_somente_item_autor = 'N')then
					
					ie_gerar_ocorrencia_w := 'S';
					--N�o foi encontrada uma autoriza��o para este atendimento.
					ds_observacao_w       := ds_observacao_1_w;
				elsif	(r_C02_w.nr_seq_guia is null and r_C01_w.ie_somente_item_autor = 'S') then	
				
					ie_gerar_ocorrencia_w := 'N';
				-- Se a guia n�o estiver autorizada tamb�m gera ocorr�ncia.
				elsif	(r_C02_w.ie_status_guia != '1' and r_C01_w.ie_somente_item_autor = 'N') then
					
					ie_gerar_ocorrencia_w := 'S';
					--A solicita��o de autoriza��o para este atendimento n�o foi autorizada pela operadora. Status da solicita��o: 
					ds_observacao_w       := ds_observacao_2_w || obter_valor_dominio(1747, r_C02_w.ie_status_guia);
					
				elsif	(r_C02_w.ie_status_guia != '1' and r_C01_w.ie_somente_item_autor = 'S') then
					
					ie_gerar_ocorrencia_w := 'N';
				elsif	(r_C02_w.ie_tipo_registro = 'P') then
				
					if	(r_C02_w.ie_tipo_despesa = '3') then
						for	r_C03_w in C03(	r_C02_w.nr_seq_guia, r_C02_w.ie_origem_proced, 
									r_C02_w.cd_procedimento, r_C02_w.cd_estabelecimento) loop
							-- A partir do momento em que o item est� na atuoriza��o 
							-- consideramos que a ocorr�ncia n�o deva ser gerada.
							-- A ocorr�ncia portanto s� ser� gerada caso seja para validar tamb�m a quantidade
							-- e o item em quest�o j� ultrapassou a quantidade autorizada.		

							-- Verificar se deve ser verificada a quantidade apresentada para gera��o da ocorr�ncia.
							if	(r_C03_w.qt_registro = 1) then
								ie_gerar_ocorrencia_w := 'N';
								-- Verificar se deve ser verificada a quantidade apresentada para gera��o da ocorr�ncia.
								if	(r_C01_w.ie_valida_qtd_util = 'S') then
									-- Se o saldo j� tive sido esgotado ent�o ser� gerada a ocorr�ncia, caso contr�rio n�o, ent�o 
									-- � setado como inv�lido para a tabela de sele��o.
									if	(r_C03_w.qt_saldo < 0) then
									
										ie_gerar_ocorrencia_w	:= 'S';
										--A quantidade de di�rias utilizadas ultrapassa a quantidade de di�rias autorizadas para esta guia.
										--Quantidade autorizada: | Quantidade autorizada: | Saldo:
										--Contas deste atendimento que utilizaram di�rias: 
										ds_observacao_w	      	:= substr(	ds_observacao_3_w || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
															ds_observacao_4_w || r_C03_w.qt_autorizada || ds_observacao_5_w || r_C03_w.qt_utilizada || ds_observacao_6_w || 
															r_C03_w.qt_saldo || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
															ds_observacao_7_w || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
															pls_util_cta_pck.obter_ds_itens_utilizados( 	r_C02_w.nr_seq_guia, r_C02_w.nr_seq_conta, null, 
																					null, null, 'D', 
																					chr(13) || chr(10)), 1, 4000);
									end if;
								end if;
							elsif	(r_C03_w.qt_registro > 1) then
								
								ie_gerar_ocorrencia_w 	:= 'S';
								--O sistema est� considerando mais de um grupo de autoriza��es v�lidas para este item, sendo que apenas um grupo deveria estar sendo considerado. 
								ds_observacao_w		:= substr(ds_observacao_8_w ,1,4000);
								
							elsif	(r_C01_w.ie_somente_item_autor = 'N') then
								
								ie_gerar_ocorrencia_w 	:= 'S';
								--Existe uma autoriza��o v�lida para esta guia, por�m a cobran�a desta di�ria n�o foi autorizada.
								ds_observacao_w		:= substr(ds_observacao_9_w,1,4000); 	
							else
								ie_gerar_ocorrencia_w   := 'N';
							end if;
						end loop;
					else
						for	r_C04_w in C04(	r_C02_w.nr_seq_guia, r_C02_w.ie_origem_proced, 
									r_C02_w.cd_procedimento, r_C02_w.cd_estabelecimento) loop
							-- A partir do momento em que o item est� na atuoriza��o 
							-- consideramos que a ocorr�ncia n�o deva ser gerada.
							-- A ocorr�ncia portanto s� ser� gerada caso seja para validar tamb�m a quantidade
							-- e o item em quest�o j� ultrapassou a quantidade autorizada.
							if	(r_C04_w.qt_registro = 1) then
								ie_gerar_ocorrencia_w := 'N';
								if	(r_C01_w.ie_valida_qtd_util = 'S') then
									-- Se o saldo j� tive sido esgotado ent�o ser� gerada a ocorr�ncia, caso contr�rio n�o, ent�o 
									-- � setado como inv�lido para a tabela de sele��o.
									if	(r_C04_w.qt_saldo < 0) then
									
										ie_gerar_ocorrencia_w	:= 'S';
										--A quantidade de di�rias utilizadas ultrapassa a quantidade de di�rias autorizadas para esta guia.
										--Quantidade autorizada: | Quantidade autorizada: | Saldo:
										--Contas deste atendimento que utilizaram di�rias: 
										ds_observacao_w		:= substr(	ds_observacao_3_w || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
															ds_observacao_4_w || r_C04_w.qt_autorizada || ds_observacao_5_w || r_C04_w.qt_utilizada || ds_observacao_6_w || 
															r_C04_w.qt_saldo || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
															ds_observacao_7_w || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
															pls_util_cta_pck.obter_ds_itens_utilizados(	r_C02_w.nr_seq_guia, r_C02_w.nr_seq_conta, r_C02_w.ie_origem_proced, 
																					r_C02_w.cd_procedimento, null, 'P', 
																					chr(13) || chr(10)), 1, 4000);
									end if;
								end if;
							elsif	(r_C04_w.qt_registro > 1) then
							
								ie_gerar_ocorrencia_w 	:= 'S';
								--O sistema est� considerando mais de um grupo de autoriza��es v�lidas para este item, sendo que apenas um grupo deveria estar sendo considerado. 
								ds_observacao_w		:= substr(ds_observacao_8_w ,1,4000);
								
							elsif	(r_C01_w.ie_somente_item_autor = 'N') then
								
								ie_gerar_ocorrencia_w	:= 'S';
								--Existe uma autoriza��o v�lida para esta guia, por�m a cobran�a deste procedimento n�o foi autorizada.
								ds_observacao_w		:= substr(ds_observacao_10_w,1,4000);
							else
								ie_gerar_ocorrencia_w   := 'N';
							end if;
						end loop;
					end if;
					
				elsif	(r_C02_w.ie_tipo_registro = 'M') then
					
					for	r_C05_w	in C05(r_C02_w.nr_seq_guia, r_C02_w.nr_seq_material, r_C02_w.cd_estabelecimento) loop
						
						-- A partir do momento em que o item est� na atuoriza��o 
						-- consideramos que a ocorr�ncia n�o deva ser gerada.
						-- A ocorr�ncia portanto s� ser� gerada caso seja para validar tamb�m a quantidade
						-- e o item em quest�o j� ultrapassou a quantidade autorizada.
						if	(r_C05_w.qt_registro = 1) then
							ie_gerar_ocorrencia_w := 'N';
							if	(r_C01_w.ie_valida_qtd_util = 'S') then
								
								-- Se o saldo j� tive sido esgotado ent�o ser� gerada a ocorr�ncia, caso contr�rio n�o, ent�o 
								-- � setado como inv�lido para a tabela de sele��o.
								if	(r_C05_w.qt_saldo < 0) then
								
									ie_gerar_ocorrencia_w 	:= 'S';
									--A quantidade de di�rias utilizadas ultrapassa a quantidade de di�rias autorizadas para esta guia.
									--Quantidade autorizada: | Quantidade autorizada: | Saldo:
									--Contas deste atendimento que utilizaram di�rias: 
									ds_observacao_w		:= substr(	ds_observacao_3_w || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
														ds_observacao_4_w || r_C05_w.qt_autorizada || ds_observacao_5_w || r_C05_w.qt_utilizada || ds_observacao_6_w || 
														r_C05_w.qt_saldo || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
														ds_observacao_7_w || pls_tipos_ocor_pck.enter_w || pls_tipos_ocor_pck.enter_w || 
														pls_util_cta_pck.obter_ds_itens_utilizados( 	r_C02_w.nr_seq_guia, r_C02_w.nr_seq_conta, null, 
																				null, r_C02_w.nr_seq_material, 'M', 
																				chr(13) || chr(10)), 1, 4000);
								end if;
							end if;
						elsif	(r_C05_w.qt_registro > 1) then
						
							ie_gerar_ocorrencia_w 	:= 'S';
							--O sistema est� considerando mais de um grupo de autoriza��es v�lidas para este item, sendo que apenas um grupo deveria estar sendo considerado. 
							ds_observacao_w		:= substr(ds_observacao_8_w ,1,4000);
							
						elsif	(r_C01_w.ie_somente_item_autor = 'N') then
							
							ie_gerar_ocorrencia_w 	:= 'S';
							--Existe uma autoriza��o v�lida para esta guia, por�m a cobran�a deste material n�o foi autorizada.
							ds_observacao_w		:= substr(ds_observacao_11_w,1,4000);
						else
							ie_gerar_ocorrencia_w   := 'N';
						end if;
					end loop;
				end if;
				
				--Quantidade m�nima , ela pode invalidar alguma ocorr�ncia j� gerada
				if	(r_C01_w.qt_minima is not null) then
					if	(r_C02_w.nr_seq_guia is not null) then
						--Caso a quantidade m�nima seja maior que a quantidade, ent�o n�o gera a ocorr�ncia
						if	(r_C02_w.qt_dif_proc_mat is not null and r_C01_w.qt_minima > r_C02_w.qt_dif_proc_mat) then
							ie_gerar_ocorrencia_w 		:= 'N';
							ds_observacao_w			:= '';
						end if;
					else
						--Proc
						if	(r_C02_w.ie_tipo_registro = 'P') then
							--Quantidade dos procedimentos de importa��o somados
							select	sum(val)
							into	qt_conta_dif_w
							from	(	select	qt_executado 		val
									from	pls_conta_proc_imp	proc,
										pls_conta_imp		conta,
										pls_protocolo_conta_imp prot
									where	proc.ie_origem_proced_conv	= r_C02_w.ie_origem_proced
									and	proc.cd_procedimento_conv	= r_C02_w.cd_procedimento
									and	conta.nr_sequencia		= proc.nr_seq_conta
									and	conta.cd_guia_ok_conv		= r_C02_w.cd_guia_ok
									and	conta.nr_seq_segurado_conv	= r_C02_w.nr_seq_segurado
									and	conta.nr_seq_protocolo		= prot.nr_sequencia
									and	prot.ie_situacao in('A','D','I')
									union all
									--Soma dos procedimentos da tabela quente
									select	qt_procedimento 		val
									from	pls_conta_proc			proc,
										pls_conta			conta
									where	proc.cd_procedimento		= r_C02_w.cd_procedimento
									and	proc.ie_origem_proced		= r_C02_w.ie_origem_proced
									and	proc.ie_status in('S','L','P','C','A')
									and	proc.ie_glosa			= 'N'
									and	conta.nr_sequencia		= proc.nr_seq_conta
									and	conta.cd_guia_referencia	= r_C02_w.cd_guia_ok
									and	conta.nr_seq_segurado		= r_C02_w.nr_seq_segurado
									and	not exists (	select	1
												from	pls_conta_glosa x
												where	x.nr_seq_conta_proc = proc.nr_sequencia
												and	x.ie_situacao = 'A'));
												
							--Caso a quantidade m�nima seja maior que a quantidade, ent�o n�o gera a ocorr�ncia
							if	(qt_conta_dif_w is not null and r_C01_w.qt_minima > qt_conta_dif_w) then
								ie_gerar_ocorrencia_w 		:= 'N';
								ds_observacao_w			:= '';
							end if;
						--Mat
						elsif	(r_C02_w.ie_tipo_registro = 'M') then
							--Quantidade dos materiais de importa��o somados
							select	sum(val)
							into	qt_conta_dif_w
							from	(	select	mat.qt_executado		val
									from	pls_conta_mat_imp 		mat,
										pls_conta_imp 			conta,
										pls_protocolo_conta_imp 	prot
									where	mat.nr_seq_material_conv	= r_C02_w.nr_seq_material
									and	conta.cd_guia_ok_conv		= r_C02_w.cd_guia_ok
									and	conta.nr_seq_segurado_conv	= r_C02_w.nr_seq_segurado
									and	conta.nr_sequencia		= mat.nr_seq_conta
									and	conta.nr_seq_protocolo		= prot.nr_sequencia
									and	prot.ie_situacao in('A','D','I')
									union all
									select	qt_material			val
									from	pls_conta_mat			mat,
										pls_conta			conta
									where	mat.nr_seq_material		= r_C02_w.nr_seq_material
									and	conta.nr_sequencia		= mat.nr_seq_conta
									and	conta.cd_guia_referencia	= r_C02_w.cd_guia_ok
									and	conta.nr_seq_segurado		= r_C02_w.nr_seq_segurado
									and	conta.ie_status in('S','L','P','C','A')
									and	mat.ie_glosa 			= 'N'
									and	not exists (	select 1
												from 	pls_conta_glosa x
												where 	x.nr_seq_conta_proc 	= mat.nr_sequencia
												and 	x.ie_situacao 		= 'A'));
								
							--Caso a quantidade m�nima seja maior que a quantidade, ent�o n�o gera a ocorr�ncia
							if	(qt_conta_dif_w is not null and r_C01_w.qt_minima > qt_conta_dif_w) then
								ie_gerar_ocorrencia_w 		:= 'N';
								ds_observacao_w			:= '';
							end if;
						end if;
					end if;				
				end if;
				
				if 	(ie_gerar_ocorrencia_w = 'S') then
					tb_seq_selecao_w(nr_indice_w) := r_C02_w.nr_seq_selecao;
					tb_valido_w(nr_indice_w)      := 'S';
					tb_observacao_w(nr_indice_w)  := ds_observacao_w;
													
					if	(nr_indice_w >= pls_cta_consistir_pck.qt_registro_transacao_w ) then
						--Grava o que restar nas vari�veis na tabela
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
												tb_observacao_w, nr_id_transacao_p, 
												'SEQ');
						--limpa as vari�veis	
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);				
						nr_indice_w := 0;
					else
						nr_indice_w := nr_indice_w + 1;	
					end if;	
				end if;	
			end loop;--C02
		end if;	
		--Grava o que restar nas vari�veis na tabela
		pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
								tb_observacao_w, nr_id_transacao_p, 
								'SEQ');
		-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
		pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', 
							ie_regra_excecao_p, null,
							nr_id_transacao_p, null);
	end loop; -- C01 
end if;
end pls_oc_cta_tratar_val_43_imp;
/
