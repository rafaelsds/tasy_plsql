create or replace
Procedure	Gerar_W_HMV_PS (dt_estatistica_p	date) is

dt_ultimo_dia_w	date;
dt_estatistica_w	date;
qt_clinica4_w		number(5,0);
qt_psa_w		number(5,0);
qt_psi_w		number(5,0);
qt_pso_w		number(5,0);
qt_psgo_w		number(5,0);
qt_total_ps_w		number(5,0);
qt_cc_w			number(5,0);
qt_cti_w		number(5,0);
qt_uco_w		number(5,0);
qt_utin_w		number(5,0);
qt_utip_w		number(5,0);
qt_meio_dia_w		number(5,0); /* 6 horas */
qt_admit_w		number(5,0);
qt_alta_w		number(5,0);
qt_dia_w		number(5,0);
qt_media_diaria_w	number(5,0);
qt_media_cirurgia_w	number(5,0);
qt_media_ps_w		number(5,0);

BEGIN

dt_ultimo_dia_w		:= trunc(LAST_DAY(dt_estatistica_p),'dd');
dt_estatistica_w	:= trunc(dt_estatistica_p,'month');

While	(dt_estatistica_w <= dt_ultimo_dia_w) and
	(trunc(dt_estatistica_w,'dd') <= trunc(sysdate,'dd')) loop

	dt_estatistica_w := trunc(dt_estatistica_w,'dd');
	
	delete	w_hmv_ps
	where	dt_estatistica = dt_estatistica_w;

	select	sum(qt_clinica4),
		sum(qt_psa),
		sum(qt_psi),
		sum(qt_pso),
		sum(qt_psgo),
		sum(qt_total_ps),
		sum(qt_cc),
		sum(qt_cti),
		sum(qt_uco),
		sum(qt_utin),
		sum(qt_utip),
		sum(qt_meio_dia),
		sum(qt_admit),
		sum(qt_alta),
		sum(qt_dia)
		
	into
		qt_clinica4_w,
		qt_psa_w,
		qt_psi_w,
		qt_pso_w,
		qt_psgo_w,
		qt_total_ps_w,
		qt_cc_w,
		qt_cti_w,
		qt_uco_w,
		qt_utin_w,
		qt_utip_w,
		qt_meio_dia_w,
		qt_admit_w,
		qt_alta_w,
		qt_dia_w		
	from	(

	/* Item - 1 	Clinica 4, PSA, PSI, PSO, PSGO e Total P.S. */

	select	sum(decode(cd_setor_atendimento,70,1,0)) qt_clinica4,
		sum(decode(cd_setor_atendimento,49,1,0)) qt_psa,
		sum(decode(cd_setor_atendimento,64,1,0)) qt_psi,
		sum(decode(cd_setor_atendimento,66,1,0)) qt_pso,
		sum(decode(cd_setor_atendimento,167,1,0)) qt_psgo,
		sum(decode(cd_setor_atendimento,70,1,0)) + sum(decode(cd_setor_atendimento,49,1,0)) + sum(decode(cd_setor_atendimento,64,1,0)) +
			sum(decode(cd_setor_atendimento,66,1,0)) + sum(decode(cd_setor_atendimento,167,1,0)) qt_total_ps,
		0 qt_cc,
		0 qt_cti,
		0 qt_uco,
		0 qt_utin,
		0 qt_utip, 
		0 qt_meio_dia,
		0 qt_admit,
		0 qt_alta,
		0 qt_dia		
	from	admissao_hospitalar_ps_v
	where	dt_entrada between dt_estatistica_w AND dt_estatistica_w + 86399/86400

	union

	/* Item - 2 	Centro Cirurgico */

	select	0,0,0,0,0,0,
		count(*),
		0,0,0,0,0,0,0,0		
	from	agenda_paciente
	where	dt_agenda between dt_estatistica_w and dt_estatistica_w + 86399/86400
	and	ie_status_agenda = 'E'

	/* Item - 3 	CTI, UCO, UTIN, UTIP e Total da 0h �s 23:59 */

	union
	select	0,0,0,0,0,0,0,
		sum(decode(cd_setor_atendimento,67,1,0)),	
		sum(decode(cd_setor_atendimento,201,1,0)),
		sum(decode(cd_setor_atendimento,86,1,0)),
		sum(decode(cd_setor_atendimento,68,1,0)),	
		0,0,0,
		count(*)		
	from	paciente_internado_v2
	where	dt_saida_interno	>  dt_estatistica_w
	and	dt_entrada_unidade	<= dt_estatistica_w
	and	nr_seq_atepacu	=  obter_atepacu_data(nr_atendimento,'IA',dt_estatistica_w)
	union

	/* Item - 4 	6 horas */

	select	0,0,0,0,0,0,
		0,0,0,0,0,
		count(*),
		0,0,0
	from	paciente_internado_v2
	WHERE	(DT_SAIDA_INTERNO	>	dt_estatistica_w + 21601/86400)
	AND	(DT_ENTRADA_UNIDADE	<=	dt_estatistica_w + 21601/86400)
	AND	nr_seq_atepacu	= 	obter_atepacu_data(nr_atendimento,'IA',dt_estatistica_w + 21601/86400)
	union

	/* Item - 5	ADMIT */

	select	0,0,0,0,0,0,
		0,0,0,0,0,
		0,
		count(*),
		0,0
	from	admissao_hospitalar_v
	where	dt_entrada 		between dt_estatistica_w AND dt_estatistica_w + 86399/86400
	and	dt_entrada_unidade	between dt_estatistica_w AND dt_estatistica_w + 86399/86400
	union

	/* Item - 6	ALTA */

	select	0,0,0,0,0,0,
		0,0,0,0,0,
		0,
		0,
		count(*),
		0
	from	alta_hospitalar_v
	where	dt_alta_interno		between dt_estatistica_w AND dt_estatistica_w + 86399/86400);

	insert	into W_HMV_PS(
		dt_estatistica,
		qt_clinica4,
		qt_psa,
		qt_psi,
		qt_pso,
		qt_psgo,
		qt_total_ps,
		qt_cc,
		qt_cti,
		qt_uco,
		qt_utin,
		qt_utip,
		qt_meio_dia,
		qt_admit,
		qt_alta,
		qt_dia,
		qt_media_diaria,
		qt_media_cirurgia,
		qt_media_ps)
	values(	
		dt_estatistica_w,
		qt_clinica4_w,
		qt_psa_w,
		qt_psi_w,
		qt_pso_w,
		qt_psgo_w,
		qt_total_ps_w,
		qt_cc_w,
		qt_cti_w,
		qt_uco_w,
		qt_utin_w,
		qt_utip_w,
		qt_meio_dia_w,
		qt_admit_w,
		qt_alta_w,
		qt_dia_w,
		qt_media_diaria_w,
		qt_media_cirurgia_w,
		qt_media_ps_w);
	dt_estatistica_w	:= dt_estatistica_w + 1;
end	loop;


commit;


END	Gerar_W_HMV_PS;
/
