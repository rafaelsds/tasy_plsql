create or replace
procedure HMSL_rop_gerar_w_resumo_setor(	dt_inicio_p		date,
					dt_final_p			date,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

qt_dias_periodo_w			number(10);					
cd_setor_atendimento_w		number(4);
nr_seq_roupa_w			number(10);
qt_peso_roll_w			number(13,4);
qt_peso_roll_ww			number(13,4) := 0;
qt_roll_w				number(13,4);
qt_roll_ww			number(13,4) := 0;
qt_reposicao_w			number(13,4);
qt_reposicao_ww			number(13,4) := 0;
pr_dif_reposicao_w			number(13,4) := 0;
qt_peso_repor_w			number(13,4);
qt_peso_repor_ww			number(13,4) := 0;
pr_dif_peso_repor_w		number(13,4) := 0;
vl_despesas_lavagem_w		number(13,4);
vl_despesas_compra_w		number(13,4);
qt_paciente_dia_w			number(10);
qt_peso_pac_dia_w		number(13,4);
pr_utilizacao_w			number(13,4);
qt_rateio_reposicao_w		number(13,4);
qt_rateio_lavagem_w		number(13,4);
qt_maximo_w			number(5);
qt_itens_kit_w			number(13,4);
qt_total_reposicao_w		number(13,4);

cursor c01 is
select	distinct
	cd_setor_atendimento
from	rop_regra_roupa_setor
where	cd_estabelecimento = cd_estabelecimento_p;

cursor c02 is
select	nr_seq_roupa,
	qt_itens_kit,
	qt_maximo
from (	select	nr_seq_roupa,
		1 qt_itens_kit,
		qt_maximo
	from	rop_regra_roupa_setor
	where	cd_setor_atendimento = cd_setor_atendimento_w
	and	cd_estabelecimento = cd_estabelecimento_p
	and	nr_seq_roupa is not null
	union all
	select	c.nr_seq_roupa,
		c.qt_roupa,
		a.qt_maximo
	from	rop_regra_roupa_setor a,
		rop_kit_rouparia b,
		rop_componente_kit c
	where	a.nr_seq_kit = b.nr_sequencia
	and	c.nr_seq_kit = b.nr_sequencia
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.nr_seq_kit is not null);


cursor c03 is
select	qt_repor,
	qt_peso
from (	select	nvl(b.qt_repor,0) qt_repor,
		(nvl(rop_obter_peso_roupa(b.nr_seq_roupa),0) * nvl(b.qt_repor,0)) qt_peso
	from	rop_inv_reposicao a,
		rop_inv_reposicao_item b
	where	a.nr_Sequencia = b.nr_seq_inventario
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(a.dt_fechamento,'dd') between dt_inicio_p and dt_final_p
	and	b.nr_seq_roupa is not null
	union all
	select	(nvl(b.qt_repor,0) * d.qt_roupa) qt_repor,
		(nvl(rop_obter_peso_roupa(d.nr_seq_roupa),2) *  nvl(b.qt_repor,0) * d.qt_roupa) qt_peso
	from	rop_inv_reposicao a,
		rop_inv_reposicao_item b,
		rop_kit_rouparia c,
		rop_componente_kit d
	where	a.nr_sequencia = b.nr_seq_inventario
	and	b.nr_seq_kit = c.nr_sequencia
	and	d.nr_seq_kit = c.nr_sequencia
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(a.dt_fechamento,'dd') between dt_inicio_p and dt_final_p
	and	b.nr_seq_kit is not null);
	
begin

delete from w_rop_resumo_setor
where nm_usuario = nm_usuario_p;

select	obter_dias_entre_datas(dt_inicio_p, dt_final_p +1)
into	qt_dias_periodo_w
from	dual;

open C01;
loop
fetch C01 into	
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
	
	qt_peso_roll_w		:= 0;
	qt_peso_roll_ww 	:= 0;
	qt_reposicao_w		:= 0;
	qt_reposicao_ww		:= 0;
	qt_peso_repor_ww	:= 0;
	qt_peso_repor_w		:= 0;
	qt_roll_ww		:= 0;
	qt_paciente_dia_w	:= 0;
	qt_peso_pac_dia_w	:= 0;
		
	select	nvl(sum(qt_maximo),0)
	into	qt_roll_w
	from	rop_regra_roupa_setor
	where	cd_setor_atendimento = cd_setor_atendimento_w
	and	cd_estabelecimento = cd_estabelecimento_p;	
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_roupa_w,
		qt_itens_kit_w,
		qt_maximo_w;
	exit when C02%notfound;
		begin
		
		select	((nvl(a.qt_peso,0) * qt_itens_kit_w) * qt_maximo_w)
		into	qt_peso_roll_w
		from	rop_lote_roupa a,
			rop_roupa b
		where	a.nr_sequencia = b.nr_seq_lote_roupa
		and	b.nr_sequencia = nr_seq_roupa_w;
		
		qt_peso_roll_ww := qt_peso_roll_ww + qt_peso_roll_w;		
		
		end;
	end loop;
	close C02;
	
	qt_roll_ww	:= qt_roll_w * qt_dias_periodo_w;
	qt_peso_roll_ww	:= qt_peso_roll_ww  * qt_dias_periodo_w;
	
	open C03;
	loop
	fetch C03 into	
		qt_reposicao_w,
		qt_peso_repor_w;
	exit when C03%notfound;
		begin
		qt_reposicao_ww 	:= qt_reposicao_ww + qt_reposicao_w;
		qt_peso_repor_ww	:= qt_peso_repor_ww + qt_peso_repor_w;
		end;
	end loop;
	close C03;
	
	pr_dif_reposicao_w	:= round(dividir((qt_reposicao_ww * 100), qt_roll_ww));
	pr_dif_peso_repor_w	:= round(dividir((qt_peso_repor_ww * 100), qt_peso_roll_ww));
	
	select	nvl(sum(a.vl_total_nota),0)
	into	vl_despesas_lavagem_w
	from	nota_fiscal a
	where	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.ie_situacao = '1'
	and	trunc(a.dt_atualizacao_estoque,'dd') between dt_inicio_p and dt_final_p
	and	a.cd_operacao_nf = 3
	and exists(
		select	1
		from	nota_fiscal_item x
		where	x.nr_Sequencia = a.nr_Sequencia
		and	x.cd_material = 54699);
	
	select	nvl(sum(a.vl_total_nota),0)
	into	vl_despesas_compra_w
	from	nota_fiscal a
	where	a.cd_estabelecimento = cd_estabelecimento_p
	and	trunc(a.dt_atualizacao_estoque,'dd') between dt_inicio_p and dt_final_p
	and	a.ie_situacao = '1'	
	and	a.cd_operacao_nf = 1
	and exists(
		select	1
		from	nota_fiscal_item x,
			rop_lote_roupa y
		where	x.nr_sequencia = a.nr_sequencia
		and	x.cd_material = y.cd_material);

		
	select	obter_qt_pacientes_dia_setor(dt_inicio_p, dt_final_p, cd_setor_atendimento_w)
	into	qt_paciente_dia_w
	from	dual;
	
	qt_peso_pac_dia_w	:= dividir(qt_peso_repor_ww, qt_paciente_dia_w);	

	insert into w_rop_resumo_setor(		
		cd_setor_atendimento,
		qt_roll,
		qt_peso_roll,
		qt_reposicao,
		pr_dif_reposicao,
		qt_peso_repor,
		pr_dif_peso_repor,
		qt_pacientes,
		qt_quilo_pac_dia,
		pr_utilizacao,
		qt_rateio_reposicao,
		qt_rateio_lavagem,
		vl_despesas_lavagem,
		vl_despesas_compra,
		nm_usuario,
		dt_atualizacao)
	values(	cd_setor_atendimento_w,
		qt_roll_ww,
		qt_peso_roll_ww,
		qt_reposicao_ww,
		pr_dif_reposicao_w,
		qt_peso_repor_ww,
		pr_dif_peso_repor_w,
		qt_paciente_dia_w,
		qt_peso_pac_dia_w,
		0,
		0,
		0,
		vl_despesas_lavagem_w,
		vl_despesas_compra_w,
		nm_usuario_p,
		sysdate);
	end;
end loop;
close C01;

select	nvl(sum(qt_reposicao),0),
	nvl(max(vl_despesas_compra),0),
	nvl(max(vl_despesas_lavagem),0)
into	qt_total_reposicao_w,
	vl_despesas_compra_w,
	vl_despesas_lavagem_w
from	w_rop_resumo_setor
where	nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
		
	select	max(qt_reposicao)
	into	qt_reposicao_w
	from	w_rop_resumo_setor
	where	nm_usuario = nm_usuario_p
	and	cd_setor_atendimento = cd_setor_atendimento_w;
	
	pr_utilizacao_w		:= dividir((qt_reposicao_w * 100 ), qt_total_reposicao_w);
	qt_rateio_reposicao_w	:= dividir((vl_despesas_compra_w * pr_utilizacao_w),100);
	qt_rateio_lavagem_w	:= dividir((vl_despesas_lavagem_w * pr_utilizacao_w),100);
	
	update	w_rop_resumo_setor
	set	pr_utilizacao		= pr_utilizacao_w,
		qt_rateio_reposicao		= qt_rateio_reposicao_w,
		qt_rateio_lavagem		= qt_rateio_lavagem_w
	where	nm_usuario		= nm_usuario_p
	and	cd_setor_atendimento	= cd_setor_atendimento_w;
	
	
	end;
end loop;
close C01;


commit;

end HMSL_rop_gerar_w_resumo_setor;
/
