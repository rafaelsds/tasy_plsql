create or replace procedure inserir_laudo_pac_recipient(nm_usuario_p  laudo_paciente_recipient.nm_usuario%type,                
                                     nr_atendimento_p    laudo_paciente_recipient.nr_atendimento%type,            
                                     nr_seq_laudo_p      laudo_paciente_recipient.nr_seq_laudo%type,              
                                     cd_medico_p         laudo_paciente_recipient.cd_medico%type,
                                     ds_justificativa_p laudo_paciente_recipient.ds_justificativa_alteracao%type)
is    
    
begin   

    insert into  laudo_paciente_recipient(nr_sequencia,               
					  dt_atualizacao,
					  nm_usuario,
					  dt_atualizacao_nrec,        
					  nm_usuario_nrec,            
					  ie_situacao,                
					  nr_atendimento,             
					  nr_seq_laudo,               
					  nm_usuario_lib,             
					  cd_medico,                  
					  dt_liberacao,               
					  dt_inativacao,              
					  nm_usuario_inativacao,      
					  ds_justificativa,           
					  ie_main_recipient,          
					  nm_destinatario,            
					  ds_justificativa_alteracao, 
					  ie_envia_medicalnet )
    values(laudo_paciente_recipient_seq.nextval,
           sysdate,          
	   nm_usuario_p,                 
	   sysdate,        
	   nm_usuario_p,            
	   'A',                
	   nr_atendimento_p,             
	   nr_seq_laudo_p,               
	   null,             
	   cd_medico_p,                  
	   null,               
	   null,              
	   null,      
	   null,           
	   'N',          
	   null,            
	   ds_justificativa_p, 
	   'S');

	commit;

end inserir_laudo_pac_recipient;
/