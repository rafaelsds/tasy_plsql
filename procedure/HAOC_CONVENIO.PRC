CREATE OR REPLACE 
procedure HAOC_Convenio
		(cd_convenio_p		in 	number,
		ds_convenio_p		in	varchar2,
		cd_cnpj_p		in	varchar2,
		ds_razao_social_p	in	varchar2,
		cd_cep_p		in	varchar2,
		ds_endereco_p		in	varchar2,
		ds_municipio_p		in	varchar2,
		sg_uf_p			in	varchar2,
		ie_erro_p		out	number,
		ds_mensagem_p		out	varchar2) is
		

cd_convenio_w			number(10,0);
qt_reg_w			number(10,0);
ds_razao_social_w		pessoa_juridica.ds_razao_social%type;
ds_endereco_w			varchar2(40);
ds_municipio_w			varchar2(40);
sg_uf_w				valor_dominio.vl_dominio%type;
cd_cep_w			varchar2(15);
ds_erro_w			varchar2(2000);
cd_cnpj_w			varchar2(14);
cd_cgc_estab_w			varchar2(14);
cd_cep_estab_w			varchar2(15);
cd_estabelecimento_w		number(03,0);
nr_seq_convenio_estab_w		number(10,0);
ds_convenio_w			varchar2(40);

begin

ie_erro_p	:= 0;
ds_mensagem_p	:= null;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	Usuario
where	nm_usuario	= 'INTEGRACAO';

select	a.cd_cgc,
	b.cd_cep
into	cd_cgc_estab_w,
	cd_cep_estab_w
from	estabelecimento a,
	pessoa_juridica b
where	a.cd_estabelecimento	= 1
and	a.cd_cgc		= b.cd_cgc;

select	decode(cd_cnpj_p, '00000000000000', cd_cgc_estab_w, cd_cnpj_p)
into	cd_cnpj_w
from	dual;

ds_razao_social_w	:= substr(nvl(ds_razao_social_p, ds_convenio_p),1,80);
cd_cep_w		:= nvl(cd_cep_p, cd_cep_estab_w);
ds_endereco_w		:= substr(nvl(ds_endereco_p, 'Nao informado'),1,40);        		
ds_municipio_w		:= substr(nvl(ds_municipio_p, 'Nao informado'),1,40);        		
sg_uf_w			:= substr(sg_uf_p,1,2);
ds_convenio_w		:= substr(DS_CONVENIO_p,1,40);
	
select	padronizar_nome(ds_razao_social_w),
	padronizar_nome(ds_endereco_w),
	padronizar_nome(ds_municipio_w),
	padronizar_nome(ds_convenio_w)
into	ds_razao_social_w,
	ds_endereco_w,
	ds_municipio_w,
	ds_convenio_w
from 	dual;
	


select	count(*)
into	qt_reg_w
from	pessoa_juridica
where	cd_cgc		= cd_cnpj_w;

if	(qt_reg_w	= 0) then
	begin

	insert	into pessoa_juridica
		(CD_CGC                ,
		DS_RAZAO_SOCIAL        ,
		NM_FANTASIA            ,
		CD_CEP                 ,
		DS_ENDERECO            ,
		DS_MUNICIPIO           ,
		SG_ESTADO              ,
		DT_ATUALIZACAO         ,
		NM_USUARIO             ,
		CD_TIPO_PESSOA,
		IE_PROD_FABRIC,
		ie_situacao,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC)
	values
		(cd_cnpj_w,
		ds_razao_social_w,
		ds_razao_social_w,
		cd_cep_w,
		ds_endereco_w,
		ds_municipio_w,
		sg_uf_w,
		sysdate,
		'INTEGRACAO',
		2,
		Obter_Valor_Padrao('PESSOA_JURIDICA','IE_PROD_FABRIC'),
		'A',
		sysdate,
		'INTEGRACAO');
	exception
		when others then
			ie_erro_p	:= -1;
			ds_erro_w	:= SQLERRM(sqlcode);
			ds_mensagem_p	:= substr('Erro na inclusao da pessoa juridica ' || ds_razao_social_w || chr(10) || ds_erro_w,1,255);	
	end;
else
	begin
	update	pessoa_juridica
	set	ds_razao_social		= ds_razao_social_w,
		nm_fantasia		= ds_razao_social_w,
		ds_endereco		= ds_endereco_w,
		cd_cep			= cd_cep_w,
		ds_municipio		= ds_municipio_w,
		SG_ESTADO		= sg_uf_w,
		dt_atualizacao		= sysdate
	where	cd_cgc			= cd_cnpj_w
	and	cd_cgc			<> cd_cgc_estab_w;

	exception
		when others then
			ie_erro_p	:= -1;
			ds_erro_w	:= SQLERRM(sqlcode);
			ds_mensagem_p	:= substr('Erro na alteracao da pessoa juridica ' || ds_razao_social_w || chr(10) || ds_erro_w,1,255);

	end;
end if;


if	(ie_erro_p	= 0) then
	begin
	select	nvl(max(cd_convenio),0)
	into	cd_convenio_w
	from	convenio
	where	cd_convenio		= cd_convenio_p;

	if	(cd_convenio_w	= 0) then
		begin

		insert	into convenio
			(CD_CONVENIO         		     ,                           
			DS_CONVENIO                          ,          
			DT_INCLUSAO                          ,          
			IE_TIPO_CONVENIO                     ,          
			IE_SITUACAO                          ,          
			CD_CGC                               ,          
			DT_ATUALIZACAO                       ,          
			NM_USUARIO                           ,          
			DT_DIA_VENCIMENTO                    ,          
			IE_FORMA_CALCULO_DIARIA              ,          
			IE_EXIGE_GUIA                        ,          
			IE_SEPARA_CONTA                      ,          
			IE_GLOSA_ATENDIMENTO                 ,          
			IE_VALOR_CONTABIL                    ,          
			IE_TIPO_ACOMODACAO                   ,          
			IE_PRECO_MEDIO_MATERIAL              ,          
			QT_CONTA_PROTOCOLO                   ,          
			IE_TITULO_RECEBER                    ,          
			IE_AGENDA_CONSULTA                   ,          
			IE_EXIGE_DATA_ULT_PAGTO              ,          
			IE_DOC_CONVENIO                      ,          
			IE_ORIGEM_PRECO                      ,          
			IE_PRECEDENCIA_PRECO                 ,          
			IE_AGRUP_ITEM_INTERF                 ,          
			IE_CONVERSAO_MAT                     ,          
			IE_REP_COD_USUARIO                   ,          
			IE_EXIGE_ORC_ATEND                   ,          
			IE_CALC_PORTE                        ,          
			IE_EXIGE_SENHA_ATEND                 ,          
			NR_MULTIPLO_ENVIO                    ,          
			IE_EXIGE_PLANO                       ,          
			IE_PARTIC_CIRURGIA                   ,          
			IE_EXIGE_CARTEIRA_ATEND              ,          
			IE_EXIGE_VALIDADE_ATEND              ,         
			IE_SOLIC_EXAME_TASYMED               ,          
			DT_ATUALIZACAO_NREC                  ,          
			NM_USUARIO_NREC                      ,          
			IE_CONSISTE_AUTOR                    ,          
			IE_GUIA_UNICA_CONTA                  ,          
			IE_EXIGE_TIPO_GUIA                   )
		values
			(CD_CONVENIO_p,                           
			ds_convenio_w,
			sysdate,          
			2                     ,          
			'A',          
			cd_cnpj_w,          
			sysdate,          
			'INTEGRACAO',          
			Obter_Valor_Padrao('CONVENIO', 'DT_DIA_VENCIMENTO'),          
			Obter_Valor_Padrao('CONVENIO', 'IE_FORMA_CALCULO_DIARIA'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_GUIA'),
			Obter_Valor_Padrao('CONVENIO', 'IE_SEPARA_CONTA'),
			Obter_Valor_Padrao('CONVENIO', 'IE_GLOSA_ATENDIMENTO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_VALOR_CONTABIL'),
			Obter_Valor_Padrao('CONVENIO', 'IE_TIPO_ACOMODACAO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_PRECO_MEDIO_MATERIAL'),
			Obter_Valor_Padrao('CONVENIO', 'QT_CONTA_PROTOCOLO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_TITULO_RECEBER'),
			Obter_Valor_Padrao('CONVENIO', 'IE_AGENDA_CONSULTA'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_DATA_ULT_PAGTO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_DOC_CONVENIO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_ORIGEM_PRECO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_PRECEDENCIA_PRECO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_AGRUP_ITEM_INTERF'),
			Obter_Valor_Padrao('CONVENIO', 'IE_CONVERSAO_MAT'),
			Obter_Valor_Padrao('CONVENIO', 'IE_REP_COD_USUARIO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_ORC_ATEND'),
			Obter_Valor_Padrao('CONVENIO', 'IE_CALC_PORTE'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_SENHA_ATEND'),
			Obter_Valor_Padrao('CONVENIO', 'NR_MULTIPLO_ENVIO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_PLANO'),
			Obter_Valor_Padrao('CONVENIO', 'IE_PARTIC_CIRURGIA'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_CARTEIRA_ATEND'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_VALIDADE_ATEND'),
			Obter_Valor_Padrao('CONVENIO', 'IE_SOLIC_EXAME_TASYMED'),
			sysdate,
			'INTEGRACAO',
			Obter_Valor_Padrao('CONVENIO', 'IE_CONSISTE_AUTOR'),
			Obter_Valor_Padrao('CONVENIO', 'IE_GUIA_UNICA_CONTA'),
			Obter_Valor_Padrao('CONVENIO', 'IE_EXIGE_TIPO_GUIA'));	

		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na inclusao do convenio ' || cd_cnpj_w || ' ' || cd_cnpj_p || ' ' || ds_convenio_p || chr(10) || ds_erro_w,1,255);		

		end;
	else
		begin
	
		update	convenio
		set	DS_CONVENIO		= ds_convenio_w,
			CD_CGC                  = cd_cnpj_w
		where	cd_convenio		= cd_convenio_p;

		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na alteracao do convenio ' || ds_convenio_p || chr(10) || ds_erro_w,1,255);			

		end;

	end if;

	select	count(*)
	into	qt_reg_w
	from	convenio_estabelecimento
	where	cd_convenio		= cd_convenio_p
	and	cd_estabelecimento	= cd_estabelecimento_w;

	if	(qt_reg_w	= 0) then
		begin

		select	convenio_estabelecimento_seq.nextval
		into	nr_seq_convenio_estab_w
		from	dual;

		insert	into convenio_estabelecimento
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_convenio,
			cd_estabelecimento,
			ie_exige_data_ult_pagto,
			ie_exige_guia,
			ie_exige_orc_atend,
			ie_glosa_atendimento,
			ie_preco_medio_material,
			ie_agenda_consulta,
			ie_rep_cod_usuario,
			ie_exige_carteira_atend,
			ie_exige_validade_atend,
			ie_exige_plano,
			ie_separa_conta,
			ie_guia_unica_conta,
			ie_valor_contabil,
			ie_titulo_receber,
			ie_conversao_mat,
			ie_doc_convenio,
			ie_doc_retorno,
			qt_conta_protocolo,
			qt_dia_fim_conta,
			ie_exige_senha_atend,
			ie_partic_cirurgia,
			ie_exige_origem,
			cd_interno,
			cd_regional,
			ie_protocolo_conta,
			ie_gerar_nf_titulo,
			ie_cancelar_conta,
			IE_MANTER_ZERADO_EDICAO)
		values	(nr_seq_convenio_estab_w,
			sysdate,
			'INTEGRACAO',
			cd_convenio_p,
			cd_estabelecimento_w,
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_data_ult_pagto'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_guia'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_orc_atend'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_glosa_atendimento'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_preco_medio_material'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_agenda_consulta'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_rep_cod_usuario'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_carteira_atend'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_validade_atend'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_plano'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_separa_conta'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_guia_unica_conta'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_valor_contabil'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_titulo_receber'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_conversao_mat'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_doc_convenio'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_doc_retorno'),'N'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','qt_conta_protocolo'),0),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','qt_dia_fim_conta'),0),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_senha_atend'),'N'),
			Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_partic_cirurgia'),
			nvl(Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','ie_exige_origem'),'N'),
			Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','cd_interno'),
			Obter_Valor_Padrao('CONVENIO_ESTABELECIMENTO','cd_regional'),
			'T',
			'S',
			'S', 'N');

		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na inclusao do estab do convenio ' || ds_convenio_p || chr(10) || ds_erro_w,1,255);			

		end;

	end if;

	
	end;
end if;

end HAOC_Convenio;
/
