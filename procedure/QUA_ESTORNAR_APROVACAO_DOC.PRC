create or replace
procedure qua_estornar_aprovacao_doc (
			nr_sequencia_p	number,
			ie_status_p	varchar2, 
			nm_usuario_p	varchar2) is 

begin

update	qua_documento
set	dt_aprovacao = null,
	nm_usuario_aprov = null,
	ie_status   = ie_status_p
where	nr_sequencia = nr_sequencia_p;

commit;

update	qua_doc_revisao
set	dt_aprovacao = null,
	nm_usuario_aprovacao = null
where	nr_seq_doc = nr_sequencia_p;

commit;

update	qua_doc_aprov
set	dt_aprovacao = null,
dt_atualizacao = sysdate,
nm_usuario = nm_usuario_p
where	nr_seq_doc = nr_sequencia_p
and (cd_pessoa_aprov = obter_pessoa_fisica_usuario(nm_usuario_p,'C')
    or cd_cargo = obter_cargo_pf(nm_usuario_p,'C')
    or cd_setor_atendimento = obter_setor_usuario_pf_ativa(obter_pessoa_fisica_usuario(nm_usuario_p,'C')));
commit;

end qua_estornar_aprovacao_doc;
/