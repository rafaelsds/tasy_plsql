create or replace
procedure obter_custo_medio_mat(	cd_estabelecimento_p		number,
					dt_mesano_referencia_p	date,
					cd_material_p			number,
					vl_custo_medio_p out		number) is

begin

if	(nvl(cd_material_p,0) > 0) then
	select	nvl(Obter_custo_medio_material(cd_estabelecimento_p, dt_mesano_referencia_p, cd_material_p),0)
	into	vl_custo_medio_p
	from	dual;
end if;

end obter_custo_medio_mat;
/