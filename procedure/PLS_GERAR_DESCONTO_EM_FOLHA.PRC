create or replace
procedure pls_gerar_desconto_em_folha
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_empresa_w		number(10);
nr_seq_cobranca_w		number(10);
nr_titulo_w			number(10);
nr_seq_mensalidade_w		number(10);
nr_seq_conta_banco_w		number(10);
cd_banco_w			number(5);
cd_conta_w			varchar2(20);
ie_digito_conta_w		varchar2(1);
cd_moeda_w			number(5);
cd_agencia_bancaria_w		varchar2(8);
qt_mensalidade_w		varchar2(10);
qt_tit_cobranca_w		number(10);
nr_seq_empresa_superior_w	number(10);
dt_referencia_w			date;
nr_seq_empresa_ww		number(10);
nr_seq_cobranca_procempa_w	number(10);
nr_seq_cobranca_cmpa_w		number(10);
cd_vinculo_w			varchar2(30);
nr_seq_vinculo_empresa_w	pls_empresa_vinculo.nr_sequencia%type;
nr_seq_vinculo_empresa_lote_w	pls_empresa_vinculo.nr_sequencia%type;

Cursor C01 is
	select	c.nr_seq_empresa,
		c.nr_seq_conta_banco,
		c.cd_banco,
		d.dt_mesano_referencia,
		decode(e.ie_quebra_vinculo,'S',c.nr_seq_vinculo_empresa,null) nr_seq_vinculo_empresa
	from	pls_mensalidade		a,
		pls_contrato_pagador	b,
		pls_contrato_pagador_fin c,
		pls_lote_mensalidade	d,
		pls_desc_empresa	e
	where	a.nr_seq_pagador	= b.nr_sequencia
	and	b.nr_sequencia		= c.nr_seq_pagador
	and	c.nr_seq_empresa	= e.nr_sequencia
	and	a.nr_seq_lote		= d.nr_sequencia
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	c.nr_seq_forma_cobranca	= 4
	and	a.ie_cancelamento is null
	and	trunc(a.dt_referencia,'dd') between trunc(c.dt_inicio_vigencia,'dd') and fim_dia(nvl(c.dt_fim_vigencia,a.dt_referencia))
	group by	c.nr_seq_empresa,
			c.nr_seq_conta_banco,
			c.cd_banco,
			c.cd_conta,
			c.ie_digito_conta,
			d.dt_mesano_referencia,
			decode(e.ie_quebra_vinculo,'S',c.nr_seq_vinculo_empresa,null);

Cursor C02 is
	select	a.nr_titulo,
		b.nr_sequencia,
		c.cd_conta,
		d.ie_digito_conta,
		d.cd_agencia_bancaria,
		d.nr_seq_vinculo_empresa
	from	titulo_receber a,
		pls_mensalidade b,
		pls_contrato_pagador c,
		pls_contrato_pagador_fin d
	where	a.nr_seq_mensalidade	= b.nr_sequencia
	and	b.nr_seq_pagador	= c.nr_sequencia
	and	c.nr_sequencia		= d.nr_seq_pagador
	and	d.nr_seq_forma_cobranca	= 4
	and	b.nr_seq_lote		= nr_seq_lote_p
	and	b.ie_cancelamento is null
	and	a.dt_liquidacao is null
	and	d.nr_seq_empresa = nr_seq_empresa_w
	and	trunc(b.dt_referencia,'dd') between trunc(d.dt_inicio_vigencia,'dd') and fim_dia(nvl(d.dt_fim_vigencia,b.dt_referencia))
	and	(d.nr_seq_conta_banco = nr_seq_conta_banco_w or d.nr_seq_conta_banco is null)
	and	(d.cd_banco = cd_banco_w or d.cd_banco is null)
	and	((d.nr_seq_vinculo_empresa = nr_seq_vinculo_empresa_lote_w) or (nr_seq_vinculo_empresa_lote_w is null));

begin

select	nr_seq_empresa
into	nr_seq_empresa_ww
from	pls_lote_mensalidade
where	nr_sequencia	= nr_seq_lote_p;

if	(nr_seq_empresa_ww = 80) then
	open C01;
	loop
	fetch C01 into
		nr_seq_empresa_w,
		nr_seq_conta_banco_w,
		cd_banco_w,
		dt_referencia_w,
		nr_seq_vinculo_empresa_lote_w;
	exit when C01%notfound;
		begin
		select	cd_moeda_padrao
		into	cd_moeda_w
		from 	parametro_contas_receber
		where	cd_estabelecimento	= cd_estabelecimento_p;
		
		select	max(nr_seq_empresa_superior)
		into	nr_seq_empresa_superior_w
		from	pls_desc_empresa
		where	nr_sequencia = nr_seq_empresa_w;
		
		select	cobranca_escritural_seq.NextVal
		into	nr_seq_cobranca_procempa_w
		from	dual;
		
		insert into	cobranca_escritural(	nr_sequencia,
							cd_estabelecimento,
							cd_banco,
							ie_remessa_retorno,
							dt_remessa_retorno,
							dt_atualizacao,
							nm_usuario,
							nr_seq_conta_banco,
							ie_emissao_bloqueto,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_arquivo,
							nr_seq_forma_cobranca,
							nr_seq_empresa,
							nr_seq_lote_mensalidade,
							nr_seq_vinculo_empresa)
					values(		nr_seq_cobranca_procempa_w,
							cd_estabelecimento_p,
							cd_banco_w,
							'R',
							dt_referencia_w,
							sysdate,
							nm_usuario_p,
							nr_seq_conta_banco_w,
							2,
							sysdate,
							nm_usuario_p,
							null,
							4,
							nvl(nr_seq_empresa_superior_w,nr_seq_empresa_w),
							nr_seq_lote_p,
							nr_seq_vinculo_empresa_lote_w);
		
		select	cobranca_escritural_seq.NextVal
		into	nr_seq_cobranca_cmpa_w
		from	dual;
		
		insert into	cobranca_escritural(	nr_sequencia,
							cd_estabelecimento,
							cd_banco,
							ie_remessa_retorno,
							dt_remessa_retorno,
							dt_atualizacao,
							nm_usuario,
							nr_seq_conta_banco,
							ie_emissao_bloqueto,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_arquivo,
							nr_seq_forma_cobranca,
							nr_seq_empresa,
							nr_seq_lote_mensalidade,
							nr_seq_vinculo_empresa)
					values(		nr_seq_cobranca_cmpa_w,
							cd_estabelecimento_p,
							cd_banco_w,
							'R',
							dt_referencia_w,
							sysdate,
							nm_usuario_p,
							nr_seq_conta_banco_w,
							2,
							sysdate,
							nm_usuario_p,
							null,
							4,
							nr_seq_empresa_w,
							nr_seq_lote_p,
							nr_seq_vinculo_empresa_lote_w);
		open C02;
		loop
		fetch C02 into
			nr_titulo_w,
			nr_seq_mensalidade_w,
			cd_conta_w,
			ie_digito_conta_w,
			cd_agencia_bancaria_w,
			nr_seq_vinculo_empresa_w;
		exit when C02%notfound;
			begin
			/* Gravar a Empresa no titulo */
			update	titulo_receber
			set	nr_seq_empresa	= nr_seq_empresa_w,
				ie_tipo_titulo	= '9'
			where	nr_titulo = nr_titulo_w;
			
			select	count(1)
			into	qt_mensalidade_w
			from	titulo_receber_cobr
			where	nr_seq_mensalidade = nr_seq_mensalidade_w;
			
			select	max(cd_vinculo)
			into	cd_vinculo_w
			from	pls_empresa_vinculo
			where	nr_sequencia =	nr_seq_vinculo_empresa_w;
			
			if	(qt_mensalidade_w = 0) then
				if	(cd_vinculo_w = '99') then
					insert	into	titulo_receber_cobr
						(nr_seq_cobranca,
						nr_titulo,
						vl_cobranca,
						dt_atualizacao,
						nm_usuario,
						cd_banco,
						cd_agencia_bancaria,
						nr_conta,
						cd_moeda,
						ie_digito_conta,
						vl_desconto,
						vl_acrescimo,
						nr_sequencia,
						vl_despesa_bancaria,
						vl_desc_previsto,
						nr_seq_mensalidade)
					(select	nr_seq_cobranca_procempa_w,
						nr_titulo,
						vl_saldo_titulo,
						sysdate,
						nm_usuario_p,
						cd_banco_w,
						cd_agencia_bancaria_w,
						cd_conta_w,
						cd_moeda_w,
						ie_digito_conta_w,
						0, 
						0,
						titulo_receber_cobr_seq.nextval,
						0,
						vl_desc_previsto,
						nr_seq_mensalidade_w
					from	titulo_receber
					where	nr_titulo	= nr_titulo_w);
				else
					insert	into	titulo_receber_cobr
						(nr_seq_cobranca,
						nr_titulo,
						vl_cobranca,
						dt_atualizacao,
						nm_usuario,
						cd_banco,
						cd_agencia_bancaria,
						nr_conta,
						cd_moeda,
						ie_digito_conta,
						vl_desconto,
						vl_acrescimo,
						nr_sequencia,
						vl_despesa_bancaria,
						vl_desc_previsto,
						nr_seq_mensalidade)
					(select	nr_seq_cobranca_cmpa_w,
						nr_titulo,
						vl_saldo_titulo,
						sysdate,
						nm_usuario_p,
						cd_banco_w,
						cd_agencia_bancaria_w,
						cd_conta_w,
						cd_moeda_w,
						ie_digito_conta_w,
						0, 
						0,
						titulo_receber_cobr_seq.nextval,
						0,
						vl_desc_previsto,
						nr_seq_mensalidade_w
					from	titulo_receber
					where	nr_titulo	= nr_titulo_w);
				end if;
				/* Gravar hist�rico da mensalidade referente a gera��o do lote de desconto em folha */
				pls_gravar_historico_mens(nr_seq_mensalidade_w,'DF','N',cd_estabelecimento_p, nm_usuario_p);
			end if;
			end;
		end loop;
		close C02;
		
		/* Verifica se gerou t�tulos para procempa - t�tulos de pagadores inativos */
		select	count(1)
		into	qt_tit_cobranca_w
		from	cobranca_escritural	a,
			titulo_receber_cobr	b
		where	a.nr_sequencia = b.nr_seq_cobranca
		and	a.nr_sequencia = nr_seq_cobranca_procempa_w;
		
		if	(qt_tit_cobranca_w = 0) then
			delete from cobranca_escritural
			where	nr_sequencia = nr_seq_cobranca_procempa_w;
		end if;
		
		/* Verifica se gerou t�tulos para cmpa - t�tulos de pagadores ativos */
		select	count(1)
		into	qt_tit_cobranca_w
		from	cobranca_escritural	a,
			titulo_receber_cobr	b
		where	a.nr_sequencia = b.nr_seq_cobranca
		and	a.nr_sequencia = nr_seq_cobranca_cmpa_w;
		
		if	(qt_tit_cobranca_w = 0) then
			delete from cobranca_escritural
			where	nr_sequencia = nr_seq_cobranca_cmpa_w;
		end if;
		end;
	end loop;
	close C01;
else
	open C01;
	loop
	fetch C01 into
		nr_seq_empresa_w,
		nr_seq_conta_banco_w,
		cd_banco_w,
		dt_referencia_w,
		nr_seq_vinculo_empresa_lote_w;
	exit when C01%notfound;
		begin
		select	cobranca_escritural_seq.NextVal
		into	nr_seq_cobranca_w
		from	dual;
		
		select	cd_moeda_padrao
		into	cd_moeda_w
		from 	parametro_contas_receber
		where	cd_estabelecimento	= cd_estabelecimento_p;
		
		select	max(nr_seq_empresa_superior)
		into	nr_seq_empresa_superior_w
		from	pls_desc_empresa
		where	nr_sequencia = nr_seq_empresa_w;
		
		insert into	cobranca_escritural(	nr_sequencia,
							cd_estabelecimento,
							cd_banco,
							ie_remessa_retorno,
							dt_remessa_retorno,
							dt_atualizacao,
							nm_usuario,
							nr_seq_conta_banco,
							ie_emissao_bloqueto,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_arquivo,
							nr_seq_forma_cobranca,
							nr_seq_empresa,
							nr_seq_lote_mensalidade,
							nr_seq_vinculo_empresa)
					values(		nr_seq_cobranca_w,
							cd_estabelecimento_p,
							cd_banco_w,
							'R',
							dt_referencia_w,
							sysdate,
							nm_usuario_p,
							nr_seq_conta_banco_w,
							2,
							sysdate,
							nm_usuario_p,
							null,
							4,
							nvl(nr_seq_empresa_superior_w,nr_seq_empresa_w),
							nr_seq_lote_p,
							nr_seq_vinculo_empresa_lote_w);
		open C02;
		loop
		fetch C02 into
			nr_titulo_w,
			nr_seq_mensalidade_w,
			cd_conta_w,
			ie_digito_conta_w,
			cd_agencia_bancaria_w,
			nr_seq_vinculo_empresa_w;
		exit when C02%notfound;
			begin
			/* Gravar a Empresa no titulo */
			update	titulo_receber
			set	nr_seq_empresa = nr_seq_empresa_w,
				ie_tipo_titulo	= '9'
			where	nr_titulo = nr_titulo_w;
			
			select	count(1)
			into	qt_mensalidade_w
			from	titulo_receber_cobr
			where	nr_seq_mensalidade = nr_seq_mensalidade_w;
			
			if	(qt_mensalidade_w = 0) then
				insert	into	titulo_receber_cobr
					(nr_seq_cobranca,
					nr_titulo,
					vl_cobranca,
					dt_atualizacao,
					nm_usuario,
					cd_banco,
					cd_agencia_bancaria,
					nr_conta,
					cd_moeda,
					ie_digito_conta,
					vl_desconto,
					vl_acrescimo,
					nr_sequencia,
					vl_despesa_bancaria,
					vl_desc_previsto,
					nr_seq_mensalidade)
				(select	nr_seq_cobranca_w,
					nr_titulo,
					vl_saldo_titulo,
					sysdate,
					nm_usuario_p,
					cd_banco_w,
					cd_agencia_bancaria_w,
					cd_conta_w,
					cd_moeda_w,
					ie_digito_conta_w,
					0,
					0,
					titulo_receber_cobr_seq.nextval,
					0,
					vl_desc_previsto,
					nr_seq_mensalidade_w
				from	titulo_receber
				where	nr_titulo	= nr_titulo_w);
				
				/* Gravar hist�rico da mensalidade referente a gera��o do lote de desconto em folha */
				pls_gravar_historico_mens(nr_seq_mensalidade_w,'DF','N',cd_estabelecimento_p, nm_usuario_p);
			end if;
			end;
		end loop;
		close C02;
		
		select	count(1)
		into	qt_tit_cobranca_w
		from	cobranca_escritural	a,
			titulo_receber_cobr	b
		where	a.nr_sequencia = b.nr_seq_cobranca
		and	a.nr_sequencia = nr_seq_cobranca_w;
		
		if	(qt_tit_cobranca_w = 0) then
			delete	from cobranca_escritural
			where	nr_sequencia = nr_seq_cobranca_w;
		end if;
		end;
	end loop;
	close C01;
end if;

commit;

end pls_gerar_desconto_em_folha;
/