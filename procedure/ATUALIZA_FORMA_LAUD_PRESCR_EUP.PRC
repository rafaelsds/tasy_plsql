create or replace
procedure Atualiza_Forma_Laud_Prescr_EUP	(	nr_atendimento_p	number,
						nr_prescricao_p	number,
						nr_seq_forma_laudo_p	number,
						nm_usuario_p	varchar2) is
nr_seq_forma_laudo_old_w	number;
ie_atualiza_dt_entrega_w	varchar2(1);

begin

Obter_Param_Usuario(916, 645, obter_perfil_ativo, nm_usuario_p, 0, ie_atualiza_dt_entrega_w);


select	nvl(max(nr_seq_forma_laudo),0)
into	nr_seq_forma_laudo_old_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

	
update	prescr_medica           
set	nr_seq_forma_laudo	= nr_seq_forma_laudo_p,
	nm_usuario		= nm_usuario_p
where	nr_atendimento		= nr_atendimento_p
and	nr_prescricao		= nr_prescricao_p;

if	(nvl(ie_atualiza_dt_entrega_w,'N') = 'S') and 
	(nr_seq_forma_laudo_old_w <> nr_seq_forma_laudo_p) then
	
	alterar_dt_entrega_prescr(nr_prescricao_p,obter_perfil_ativo,nm_usuario_p);
	
end if;

commit;

end Atualiza_Forma_Laud_Prescr_EUP;
/