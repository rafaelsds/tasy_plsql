create or replace
procedure Obter_Regra_Valid_Usuario_Conv
		(cd_convenio_p		in	number,
		cd_categoria_p		in	varchar2,
		ie_tipo_atendimento_p	in	number,
		ds_procedure_p		out nocopy	varchar2) is

ds_procedure_w		varchar2(255)	:= null;
cd_categoria_w		varchar2(10);
ie_tipo_atendimento_w	number(3);
cd_estabelecimento_w	number(4);

cursor	c01 is
	select	ds_procedure_validacao
	from	regra_validacao_usuario
	where	cd_convenio					= cd_convenio_p
	and	nvl(cd_categoria, cd_categoria_w)		= cd_categoria_w
	and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
	and	nvl(cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w;

begin

cd_estabelecimento_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
cd_categoria_w		:= nvl(cd_categoria_p, '0');
ie_tipo_atendimento_w	:= nvl(ie_tipo_atendimento_p, 0);

open	c01;
loop
fetch	c01 into
	ds_procedure_w;
exit	when c01%notfound;
	begin

	ds_procedure_w	:= ds_procedure_w;

	end;
end loop;
close c01;

ds_procedure_p		:= ds_procedure_w;

end Obter_Regra_Valid_Usuario_Conv;
/