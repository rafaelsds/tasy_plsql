create or replace
procedure gerar_ressup_fornecedor(	cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

nr_sequencia_w				ressup_fornec.nr_sequencia%type;
nr_solic_compra_w			solic_compra.nr_solic_compra%type;
cd_fornecedor_w				pessoa_juridica.cd_cgc%type;
cd_local_estoque_w			local_estoque.cd_local_estoque%type;
cd_pessoa_solic_padrao_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_comprador_padrao_w			pessoa_fisica.cd_pessoa_fisica%type;
cd_condicao_pagamento_padrao_w		condicao_pagamento.cd_condicao_pagamento%type;
cd_moeda_padrao_w			moeda.cd_moeda%type;
cd_material_w				material.cd_material%type;
nr_item_solic_compra_w			solic_compra_item.nr_item_solic_compra%type;
qt_material_w				solic_compra_item.qt_material%type := 0;
vl_unitario_material_w			ordem_compra_item.vl_unitario_material%type;
vl_item_liquido_w			ordem_compra_item.vl_item_liquido%type;
nr_seq_nf_w				nota_fiscal.nr_sequencia%type;
dt_entrega_w				ordem_compra.dt_entrega%type;
nr_ordem_compra_w			ordem_compra.nr_ordem_compra%type;
cd_unidade_medida_compra_w		unidade_medida.cd_unidade_medida%type;
qt_ponto_pedido_w			ressup_fornec_mat.qt_ponto_pedido%type;
qt_estoque_maximo_w			ressup_fornec_mat.qt_estoque_maximo%type;
qt_conv_compra_estoque_w		material.qt_conv_compra_estoque%type;
qt_saldo_estoque_w			saldo_estoque.qt_estoque%type;
cd_unid_med_compra_w			unidade_medida.cd_unidade_medida%type;
cd_unid_med_estoque_w			unidade_medida.cd_unidade_medida%type;
qt_existe_w				number(10);
ie_gerou_w				varchar2(1) := 'N';
ie_libera_ordem_compra_w		varchar2(1) := 'A';
dt_liberacao_w				ordem_compra.dt_liberacao%type;
nm_usuario_lib_w			usuario.nm_usuario%type;
dt_aprovacao_w				ordem_compra.dt_aprovacao%type;
nm_usuario_aprov_w			usuario.nm_usuario%type;
nr_item_oci_w				ordem_compra_item.nr_item_oci%type;
cd_conta_contabil_w			conta_contabil.cd_conta_contabil%type;
cd_centro_custo_w			centro_custo.cd_centro_custo%type;

cursor c01 is
select	nr_sequencia,
	cd_fornecedor,
	cd_local_estoque
from	ressup_fornec	
where	cd_estabelecimento = cd_estabelecimento_p
and	ie_situacao = 'A';

cursor c02 is
SELECT	cd_material,
	qt_ponto_pedido,
	qt_estoque_maximo,
	obter_dados_material(cd_material,'QCE'),
	obter_dados_material(cd_material,'UMP'),
	obter_dados_material(cd_material,'UME'),
	DECODE(	a.ie_consignado,
		'C', 	NVL( obter_saldo_estoque_consig(cd_estabelecimento_p, cd_fornecedor_w, b.cd_material, a.cd_local_estoque),0),
			NVL(obter_saldo_disp_estoque(cd_estabelecimento_p, b.cd_material,a.cd_local_estoque,TRUNC(SYSDATE,'mm')),0))
FROM	ressup_fornec a,
	ressup_fornec_mat b
WHERE	a.nr_sequencia = b.nr_seq_ressup
AND	a.nr_sequencia = nr_sequencia_w;
--AND	qt_ponto_pedido > 0
--AND	qt_estoque_maximo > 0;

cursor c03 is
select	nr_item_solic_compra,
	cd_material,
	cd_unidade_medida_compra,
	qt_material,
	nvl(obter_preco_vigente_mat_forn(cd_fornecedor_w,cd_material,cd_estabelecimento_p),0) vl_unitario
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_w;

begin

select	cd_pessoa_solic_padrao,
	cd_comprador_padrao,
	cd_condicao_pagamento_padrao,
	cd_moeda_padrao
into	cd_pessoa_solic_padrao_w,
	cd_comprador_padrao_w,
	cd_condicao_pagamento_padrao_w,
	cd_moeda_padrao_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

select	(max(obter_valor_param_usuario(913, 281, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)))
into	ie_libera_ordem_compra_w
from	dual;

if	(ie_libera_ordem_compra_w = 'A') then /*Libera a aprova automaticamente*/
	dt_liberacao_w		:= sysdate;
	nm_usuario_lib_w	:= nm_usuario_p;
	dt_aprovacao_w		:= sysdate;
	nm_usuario_aprov_w	:= nm_usuario_p;
end if;	

if	(nvl(cd_pessoa_solic_padrao_w,'X') = 'X') then

	select	substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10)
	into	cd_pessoa_solic_padrao_w
	from	dual;

end if;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	cd_fornecedor_w,
	cd_local_estoque_w;
exit when C01%notfound;
	begin
	ie_gerou_w := 'N';
	select	solic_compra_seq.nextval
	into	nr_solic_compra_w
	from	dual;
	
	insert into solic_compra(
		nr_solic_compra,
		cd_estabelecimento,
		dt_solicitacao_compra,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		cd_pessoa_solicitante,
		cd_local_estoque,
		dt_liberacao,
		nm_usuario_lib,
		dt_autorizacao,
		ie_aviso_chegada,
		ie_aviso_aprov_oc,
		ie_urgente,
		ie_tipo_solicitacao,
		nr_seq_ressup_fornec,
		ie_comodato,
		ie_semanal,
		nm_usuario_nrec,
		dt_atualizacao_nrec)
	values(	nr_solic_compra_w,
		cd_estabelecimento_p,
		sysdate,
		sysdate,
		nm_usuario_p,
		'A',
		cd_pessoa_solic_padrao_w,
		cd_local_estoque_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		'N',
		'N',
		'N',
		5,
		nr_sequencia_w,
		'N',
		'N',
		nm_usuario_p,
		sysdate);
		
	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		qt_ponto_pedido_w,
		qt_estoque_maximo_w,
		qt_conv_compra_estoque_w,
		cd_unid_med_compra_w,
		cd_unid_med_estoque_w,
		qt_saldo_estoque_w;
	exit when C02%notfound;
		begin
		qt_material_w := 0;
		if	(qt_saldo_estoque_w <= qt_ponto_pedido_w) then
			qt_material_w := qt_estoque_maximo_w - qt_saldo_estoque_w;
		end if;
		
		if	(cd_unid_med_compra_w <> cd_unid_med_estoque_w) then
			qt_material_w := round(qt_material_w / qt_conv_compra_estoque_w);
		end if;
		
		if	(qt_material_w > 0) then
			ie_gerou_w := 'S';
			dias_entrega_solic_compra(nr_solic_compra_w,7,'S',dt_entrega_w);		
			Gerar_Solic_Compra_Item(nr_solic_compra_w,cd_material_w,'C',qt_material_w,dt_entrega_w,nm_usuario_p,'S',90,null);
		end if;
		end;
	end loop;
	close C02;

	if	(ie_gerou_w = 'S') then
	
		select	ordem_compra_seq.nextval
		into	nr_ordem_compra_w
		from	dual;
		
		insert into ordem_compra(
			nr_ordem_compra,
			cd_estabelecimento,
			cd_cgc_fornecedor,
			cd_condicao_pagamento,
			cd_comprador,
			dt_ordem_compra,
			dt_atualizacao,
			nm_usuario,
			cd_moeda,
			ie_situacao,
			dt_inclusao,
			cd_pessoa_solicitante,
			ie_frete,
			vl_frete,
			pr_desc_pgto_antec,
			pr_juros_negociado,
			pr_desc_financeiro,
			vl_desconto,
			ds_pessoa_contato,
			dt_entrega,
			dt_aprovacao,
			nm_usuario_aprov,
			dt_liberacao,
			nm_usuario_lib,
			ie_aviso_chegada,
			ie_emite_obs,
			ie_somente_pagto,
			ie_urgente,
			ie_tipo_ordem,
			nr_seq_ressup_fornec,
			ie_forma_exportar)
		values(	nr_ordem_compra_w,
			cd_estabelecimento_p,
			cd_fornecedor_w,
			cd_condicao_pagamento_padrao_w,
			cd_comprador_padrao_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			cd_moeda_padrao_w,
			'A',
			sysdate,
			cd_pessoa_solic_padrao_w,
			'C',
			0,
			0,
			0,
			0,
			0,
			substr(obter_dados_pf_pj_estab(cd_estabelecimento_p,null,cd_fornecedor_w,'ENC'),1,255),
			dt_entrega_w,
			dt_aprovacao_w,
			nm_usuario_aprov_w,
			dt_liberacao_w,
			nm_usuario_lib_w,
			'N',
			'S',
			'N',
			'N',
			'F',
			nr_sequencia_w,
			'EXC');
		
			
			open C03;
			loop
			fetch C03 into	
				nr_item_solic_compra_w,
				cd_material_w,
				cd_unidade_medida_compra_w,
				qt_material_w,
				vl_unitario_material_w;
			exit when C03%notfound;
				begin
				
				vl_item_liquido_w	:= vl_unitario_material_w * qt_material_w;
				
				define_conta_material(	cd_estabelecimento_p,
							cd_material_w,
							2,
							null, null, null, null, null, null, null,
							cd_local_estoque_w,
							null, sysdate,
							cd_conta_contabil_w,
							cd_centro_custo_w,
							null);
				
				select	nvl(max(nr_item_oci),0) + 1
				into	nr_item_oci_w
				from	ordem_compra_item
				where	nr_ordem_compra = nr_ordem_compra_w;
				
				insert into ordem_compra_item(	
					nr_ordem_compra,
					nr_item_oci,
					cd_material,
					cd_unidade_medida_compra,
					vl_unitario_material,
					qt_material,
					qt_original,
					vl_item_liquido,
					vl_desconto,
					pr_descontos,
					pr_desc_financ,
					dt_atualizacao,
					nm_usuario,
					ie_situacao,
					cd_pessoa_solicitante,
					cd_local_estoque,
					dt_aprovacao,
					nr_solic_compra,
					nr_item_solic_compra,
					ie_geracao_solic,
					cd_conta_contabil,
					vl_total_item)
				values(	nr_ordem_compra_w,
					nr_item_oci_w,
					cd_material_w,
					cd_unidade_medida_compra_w,
					vl_unitario_material_w,
					qt_material_w,
					qt_material_w,
					vl_item_liquido_w,
					0,
					0,
					0,
					sysdate,
					nm_usuario_p,
					'A',
					cd_pessoa_solic_padrao_w,
					cd_local_estoque_w,
					dt_aprovacao_w,
					nr_solic_compra_w,
					nr_item_solic_compra_w,
					'S',
					cd_conta_contabil_w,
					round((qt_material_w * vl_unitario_material_w),4));
					
				insert into ordem_compra_item_entrega(
					nr_sequencia,
					nr_ordem_compra,
					nr_item_oci,
					dt_prevista_entrega,
					dt_real_entrega,
					dt_entrega_original,
					dt_entrega_limite,
					qt_prevista_entrega,
					qt_real_entrega,
					dt_atualizacao,
					nm_usuario,
					ds_observacao)
				values(	ordem_compra_item_entrega_seq.nextval,
					nr_ordem_compra_w,
					nr_item_oci_w,
					dt_entrega_w,
					null,
					dt_entrega_w,
					dt_entrega_w,
					qt_material_w,
					null,
					sysdate,
					nm_usuario_p,
					null);	
				end;
			end loop;
			close C03;
			
		if	(ie_libera_ordem_compra_w = 'L') then /* Liberar automaticamente e cai no processo de aprova��o*/
			Gerar_Aprov_Ordem_Compra(nr_ordem_compra_w, null, 'S', nm_usuario_p);
		end if;
	else
		delete 	from solic_compra
		where	nr_solic_compra = nr_solic_compra_w;
	end if;
	end;
	
end loop;
close C01;

commit;
end gerar_ressup_fornecedor;
/
