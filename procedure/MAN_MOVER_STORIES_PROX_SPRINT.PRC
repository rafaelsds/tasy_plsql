create or replace
procedure man_mover_stories_prox_sprint(
			nr_sprint_p		number,
			nr_time_p		number,
			nm_usuario_p		varchar2,
			ds_erro_p	out	varchar2) is 

nr_sprint_mover_w	desenv_sprint.nr_sequencia%type;
			
cursor c01 is
	select	a.nr_story,
		a.nr_sprint
	from	desenv_story_sprint a,
		desenv_story b
	where	a.nr_sprint = nr_sprint_p
	and	a.nr_story = b.nr_sequencia
	and	b.nr_team = nr_time_p
	and 	not exists(	select	1
				from	desenv_story_sprint
				where	nr_sprint = nr_sprint_mover_w
				and	nr_story = b.nr_sequencia)
	and	a.cd_status <> 4;

v01			c01%rowtype;
dt_final_w		desenv_sprint.dt_final%type;
nr_art_w		desenv_release.nr_art%type;
	
begin

select	a.dt_final,
	b.nr_art
into	dt_final_w,
	nr_art_w
from	desenv_sprint a,
	desenv_release b
where	a.nr_release = b.nr_sequencia
and	a.nr_sequencia = nr_sprint_p;

select	nr_sequencia
into	nr_sprint_mover_w
from	(select	a.nr_sequencia
	from	desenv_sprint a,
		desenv_release b
	where	a.nr_release = b.nr_sequencia
	and	b.nr_art = nr_art_w
	and	a.dt_inicial > dt_final_w
	order by a.dt_inicial)
where	rownum = 1;

open c01;
loop
fetch c01 into	
	v01;
exit when c01%notfound;
	begin
	
	man_atribuir_story_sprint(
			v01.nr_story,
			nr_sprint_mover_w,
			nr_sprint_p,
			nm_usuario_p,
			ds_erro_p);
			
	if	(ds_erro_p is not null) then
		rollback;
		return;
	end if;
	
	end;
end loop;
close c01;

commit;

end man_mover_stories_prox_sprint;
/
