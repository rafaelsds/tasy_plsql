create or replace
procedure atualizar_crm_medico(	cd_pessoa_fisica_p		varchar2,
				nm_usuario_p		Varchar2) is 

cd_cargo_w		number(10);
nr_conselho_w		varchar2(20);

begin

if	(cd_pessoa_fisica_p is not null) then
	
	select	max(a.cd_cargo)
	into	cd_cargo_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica		= cd_pessoa_fisica_p;
	
	if	(cd_cargo_w is not null) then
		
		select	max(a.nr_conselho)
		into	nr_conselho_w
		from	cargo a
		where	a.cd_cargo	= cd_cargo_w;
		
		if	(nr_conselho_w is not null) then
			
			update	medico
			set	nr_crm			= nr_conselho_w,
				nm_usuario		= nm_usuario_p
			where	cd_pessoa_fisica		= cd_pessoa_fisica_p;
		end if;
		
	end if;
end if;
commit;

end atualizar_crm_medico;
/