create or replace
procedure pls_atualiza_etapa_solicitacao
			(	nr_seq_solicitacao_p	number,
				nm_usuario_p		varchar2) is

ie_status_w		varchar2(2);
qt_vendedor_atual_w	number(5);
qt_historicos_w		number(5);
cd_estabelecimento_w	number(5);
ie_etapa_solicitacao_w	varchar2(1);

begin

select	nvl(ie_status,'PE'),
	cd_estabelecimento
into	ie_status_w,
	cd_estabelecimento_w
from	pls_solicitacao_comercial
where	nr_sequencia	= nr_seq_solicitacao_p;

if	(ie_status_w = 'A') then
	update	pls_solicitacao_comercial
	set	ie_etapa_solicitacao = 'O'
	where	nr_sequencia	= nr_seq_solicitacao_p;
elsif	(ie_status_w = 'R') then
	update	pls_solicitacao_comercial
	set	ie_etapa_solicitacao = 'E'
	where	nr_sequencia	= nr_seq_solicitacao_p;
else
	select	count(*)
	into	qt_vendedor_atual_w
	from	pls_solicitacao_vendedor
	where	nr_seq_solicitacao	= nr_seq_solicitacao_p
	and	dt_fim_vigencia	is null;
	
	if	(qt_vendedor_atual_w > 0) then
		select	count(*)
		into	qt_historicos_w
		from	pls_solicitacao_historico
		where	nr_seq_solicitacao	= nr_seq_solicitacao_p;
		
		if	(qt_historicos_w > 0) then
			ie_etapa_solicitacao_w	:= 'C';
			select	count(*)
			into	qt_historicos_w
			from	pls_solicitacao_historico a,
				pls_tipo_atividade b
			where	b.nr_sequencia		= a.ie_tipo_atividade
			and	a.nr_seq_solicitacao	= nr_seq_solicitacao_p
			and	a.dt_liberacao is not null
			and	nvl(b.ie_etapa_solicitacao,'C') = 'C';
			
			if	(qt_historicos_w = 0) then --Se n�o existir hist�rico para alterar a etapa para "Em negocia��o"
				select	count(*)
				into	qt_historicos_w
				from	pls_solicitacao_historico a,
					pls_tipo_atividade b
				where	b.nr_sequencia		= a.ie_tipo_atividade
				and	a.nr_seq_solicitacao	= nr_seq_solicitacao_p
				and	a.dt_liberacao is not null
				and	nvl(b.ie_etapa_solicitacao,'C') = 'D';
				
				if	(qt_historicos_w > 0) then
					ie_etapa_solicitacao_w := 'D';
				end if;
			end if;
			
			update	pls_solicitacao_comercial
			set	ie_etapa_solicitacao = ie_etapa_solicitacao_w
			where	nr_sequencia	= nr_seq_solicitacao_p;
		else
			update	pls_solicitacao_comercial
			set	ie_etapa_solicitacao = 'R'
			where	nr_sequencia	= nr_seq_solicitacao_p;
		end if;
	else
		update	pls_solicitacao_comercial
		set	ie_etapa_solicitacao = 'T'
		where	nr_sequencia	= nr_seq_solicitacao_p;
	end if;
end if;

end pls_atualiza_etapa_solicitacao;
/