create or replace
procedure pls_guia_plano_atribfocuslost(	cd_usuario_plano_p	varchar2,
					ds_mensagem_um_p	out varchar2,
					ds_mensagem_dois_p	out varchar2,
					ds_mensagem_tres_p	out varchar2,
					ie_existe_benef_p		out number,
					dt_validade_cart_p		out varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is

nr_seq_segurado_w	number(10) := 0;
dt_validade_carteira_w	varchar2(50);

begin

select	nvl(max(pls_obter_beneficiario_existe(cd_usuario_plano_p, nm_usuario_p, cd_estabelecimento_p)),0)
into	nr_seq_segurado_w
from	dual;

ie_existe_benef_p	:= nr_seq_segurado_w;

ds_mensagem_um_p := obter_texto_dic_objeto(48859,null,null);

select	to_char(nvl(max(dt_validade_carteira),sysdate),'dd/mm/yyyy hh24:mi:ss') dt_validade
into	dt_validade_carteira_w
from	pls_segurado_carteira
where	nr_seq_segurado = nr_seq_segurado_w
and	cd_usuario_plano = cd_usuario_plano_p;

dt_validade_cart_p := dt_validade_carteira_w;

ds_mensagem_dois_p := obter_texto_dic_objeto(48860,null,null);

ds_mensagem_tres_p := obter_texto_dic_objeto(48860,null,null);

end pls_guia_plano_atribfocuslost;
/