create or replace
procedure GSJS_gerar_w_analise_compra(	dt_mesano_referencia_p		date,
					cd_estabelecimento_p		number) is 

/*create table GSJS_analise_compra(
CD_MATERIAL  number(6),
IE_MES		 number(10),
QT_ITEM_NF	 number(13,4),
vl_item_nf	 number(13,4),
VL_TOTAL_ITEM_NF  number(13,4),
QT_ITEM_NF_ACUM number(13,4),
VL_TOTAL_ITEM_NF_ACUM number(13,4))*/


					
dt_mes_w			date;
ie_mes_w			number(10) := 0;
cd_material_w			number(6);
qt_item_nf_w			number(13,4) := 0;
vl_item_nf_w			number(13,4) := 0;
vl_total_item_nf_w		number(13,4) := 0;
qt_existe_w			number(10);
vl_total_w			number(13,4) := 0;
vl_soma_qt_item_nf_w		number(13,4) := 0;
qt_item_nf_ww			number(13,4) := 0;
vl_custo_medio_w		number(13,4) := 0;
	
cursor c01 is
select	dt_mes
from	mes_v
where	dt_mes between add_months(dt_mesano_referencia_p,-4) and dt_mesano_referencia_p
order by 1 desc;
				
cursor c02 is
select	distinct
	b.cd_material
from	nota_fiscal_item b,
	nota_fiscal a
where	a.nr_sequencia = b.nr_sequencia
and	b.cd_material is not null
and	a.ie_tipo_nota in ('EN','EF')
and a.cd_operacao_nf <> 22
and	trunc(a.dt_atualizacao_estoque,'mm') = dt_mes_w
and	a.dt_atualizacao_estoque is not null
and	a.ie_situacao = '1'
and	a.cd_estabelecimento = cd_estabelecimento_p;

cursor c03 is
select	b.qt_item_nf,
	dividir(b.vl_liquido, b.qt_item_estoque)
from	nota_fiscal_item b,
	nota_fiscal a
where	a.nr_sequencia = b.nr_sequencia
and	b.cd_material is not null
and	a.ie_tipo_nota in ('EN','EF')
and a.cd_operacao_nf <> 22
and	trunc(a.dt_atualizacao_estoque,'mm') = dt_mes_w
and	a.dt_atualizacao_estoque is not null
and	a.ie_situacao = '1'
and	a.cd_estabelecimento = cd_estabelecimento_p
and	b.cd_material = cd_material_w;

begin

delete from GSJS_analise_compra;

open C01;
loop
fetch C01 into	
	dt_mes_w;
exit when C01%notfound;
	begin
	
	ie_mes_w := ie_mes_w + 1;
	
	open C02;
	loop
	fetch C02 into	
		cd_material_w;
	exit when C02%notfound;
		begin
		qt_item_nf_ww 		:= 0;
		vl_item_nf_w		:= 0;
		vl_total_w		:= 0;
		vl_soma_qt_item_nf_w	:= 0;
		open C03;
		loop
		fetch C03 into	
			qt_item_nf_w,
			vl_custo_medio_w;
		exit when C03%notfound;
			begin
			
			qt_item_nf_ww		:= qt_item_nf_ww + qt_item_nf_w;
			vl_item_nf_w		:= vl_item_nf_w + vl_custo_medio_w;
			vl_total_w 		:= vl_total_w + (qt_item_nf_w * vl_custo_medio_w);
			vl_soma_qt_item_nf_w	:= vl_soma_qt_item_nf_w + qt_item_nf_w;
			
			end;
		end loop;
		close C03;	
		
		vl_total_item_nf_w	:= dividir(vl_total_w,vl_soma_qt_item_nf_w);
	
		insert into GSJS_analise_compra(
				cd_material,
				ie_mes,
				qt_item_nf,
				vl_item_nf,
				vl_total_item_nf,
				qt_item_nf_acum,
				vl_total_item_nf_acum)
			values	(cd_material_w,
				ie_mes_w,
				qt_item_nf_ww,
				vl_total_item_nf_w,
				vl_total_item_nf_w,
				0,
				0);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

end GSJS_gerar_w_analise_compra;
/