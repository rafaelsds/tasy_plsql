create or replace
procedure consistir_exclusao_escala(	nr_seq_classif_p		number,
					nr_seq_grupo_p			number,
					nr_seq_escala_p			number,
					nr_seq_escala_horario_p		number,
					nr_seq_escala_diaria_p		number,
					nr_seq_escala_diaria_adic_p	number,
					nm_usuario_p			varchar2,
					ds_erro_p		out	varchar2)
										is
qt_itens_w	number(5);
nr_seq_escala_w	number(10);
nr_seq_grupo_w	number(10);
ds_erro_w	varchar2(255);


cursor c01 is
	select 	b.nr_sequencia,
		a.nr_sequencia
	from	escala_grupo a,
		escala b
	where	a.nr_sequencia 		= b.nr_seq_grupo
	and	a.nr_seq_classif 	= nr_seq_classif_p;


cursor c02 is
	select 	b.nr_sequencia
	from	escala b	
	where	b.nr_seq_grupo		= nr_seq_grupo_p;

begin

ds_erro_w	:= '';

if 	(nr_seq_classif_p > 0) then

	select 	count(*)
	into	qt_itens_w
	from	agenda_paciente_escala a,
		escala b,
		escala_grupo c
	where	a.nr_seq_escala = b.nr_sequencia
	and	b.nr_seq_grupo	= c.nr_sequencia
	and	nr_seq_classif 	= nr_seq_classif_p;

	if (qt_itens_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279359,null);
	end if;
end if;


if 	(nr_seq_grupo_p > 0) then

	select 	count(*)
	into	qt_itens_w
	from	agenda_paciente_escala a,
		escala b
	where	a.nr_seq_escala = b.nr_sequencia
	and	b.nr_seq_grupo	= nr_seq_grupo_p;

	if (qt_itens_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279362,null);
	end if;
end if;

if 	(nr_seq_escala_p > 0) then

	select 	count(*)
	into	qt_itens_w
	from	agenda_paciente_escala a		
	where	a.nr_seq_escala = nr_seq_escala_p;

	if	(qt_itens_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279363,null);
	end if;
end if;

if	(nr_seq_escala_horario_p > 0) then

	select 	count(*)
	into	qt_itens_w
	from	agenda_paciente_escala a,
		escala_horario b		
	where	b.nr_Seq_escala = a.nr_seq_escala
	and	b.nr_sequencia  = nr_seq_escala_horario_p;

	if (qt_itens_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279365,null);
	end if;
end if;

if 	(nr_seq_escala_diaria_p > 0) then

	select 	count(*)
	into	qt_itens_w
	from	agenda_paciente_escala a,
		escala_diaria b		
	where	b.nr_Seq_escala = a.nr_seq_escala
	and	b.nr_sequencia  = nr_seq_escala_diaria_p
	and	trunc(b.dt_inicio,'dd')	= trunc(a.dt_atualizacao,'dd');

	if (qt_itens_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279367,null);
	end if;
end if;


if 	(nr_seq_escala_diaria_adic_p > 0) then

	select 	count(*)
	into	qt_itens_w
	from	agenda_paciente_escala a,
		escala_diaria_adic c,
		escala_diaria b
	where	c.nr_seq_escala_diaria 	= b.nr_sequencia
	and	b.nr_Seq_escala 	= a.nr_seq_escala
	and	c.nr_sequencia 		= nr_seq_escala_diaria_adic_p
	AND	c.cd_pessoa_fisica	= a.cd_anestesista
	and	trunc(b.dt_inicio,'dd')	= trunc(a.dt_atualizacao,'dd');

	if (qt_itens_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279367,null);
	end if;
end if;


ds_erro_p	:= ds_erro_w;

end;
/
