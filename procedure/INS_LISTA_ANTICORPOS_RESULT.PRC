create or replace
procedure ins_lista_anticorpos_result(nr_seq_exame_p		number,
				      ds_lista_p		varchar2,
				      nr_seq_peca_p		number,
				      nm_usuario_p		varchar2) is 

ds_lista_w		varchar2(255);
tam_lista_w		number(10);
ie_pos_virgula_w	number(10);
nr_seq_anticorpo_w	varchar2(10);
ie_exite_w		number(10);
					  
begin


ds_lista_w := ds_lista_p;

while	(ds_lista_w is not null)  loop
	begin
	tam_lista_w		:= length(ds_lista_w);
	ie_pos_virgula_w		:= instr(ds_lista_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_anticorpo_w	:= to_number(substr(ds_lista_w,1,(ie_pos_virgula_w - 1)));
		ds_lista_w		:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	select 	count(*)
	into	ie_exite_w
	from	prescr_proc_result_antic
	where	nr_seq_prescr_proc_compl = nr_seq_exame_p
	and	nr_seq_anticorpo = nr_seq_anticorpo_w
	and	nr_seq_prescr_proc_peca = nr_seq_peca_p;
	
	if (ie_exite_w = 0) then
		insert 
		into	prescr_proc_result_antic
			(nr_sequencia,
			 dt_atualizacao,
			 nm_usuario,
			 dt_atualizacao_nrec,
			 nm_usuario_nrec,
			 nr_seq_anticorpo,
			 nr_seq_prescr_proc_compl,
			 ie_resultado,
			 ds_resultado,
			 nr_seq_prescr_proc_peca
			)
		values  (prescr_proc_result_antic_seq.nextval,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 nr_seq_anticorpo_w,
			 nr_seq_exame_p,
			 null,
			 null,
			 nr_seq_peca_p
			);
	end if;
	end;
	end loop;
commit;

end ins_lista_anticorpos_result;
/
