create or replace
procedure pls_alterar_inco_conf_sib
			(	nr_seq_inconsistencia_p	Number,
				nm_usuario_p		Varchar2) is 

		
begin

update	sib_inconsistencia_conf
set	ie_situacao	= decode(ie_situacao,'A','I','A'),
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_inconsistencia_p;

commit;

end pls_alterar_inco_conf_sib;
/
