create or replace
procedure consiste_cota_munic_agenda(	nr_seq_agenda_p		number,
					cd_agenda_p		number,
					dt_agenda_p		date,
					cd_pessoa_fisica_p	varchar2,
					cd_convenio_p		number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_seq_proc_interno_p	number,
					cd_medico_exec_p	varchar2,
					ie_novo_registro_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ie_proximo_mes_p	varchar2,
					nr_seq_proc_adic_p	number,
					ie_inserir_p		varchar2) is

ie_verifica_cota_w	varchar2(1);
				
begin

ie_verifica_cota_w	:= substr(verificar_cota_munic_agepac(cd_agenda_p, dt_agenda_p, cd_pessoa_fisica_p, cd_convenio_p, cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, null, cd_estabelecimento_p, ie_proximo_mes_p),1,1);

if	(ie_verifica_cota_w = 'S') or
	(ie_inserir_p = 'S') then
	inserir_cota_munic_agepac(nr_seq_agenda_p, cd_agenda_p, dt_agenda_p, cd_pessoa_fisica_p, cd_convenio_p, cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, cd_medico_exec_p, ie_novo_registro_p, nm_usuario_p, cd_estabelecimento_p,	ie_proximo_mes_p, nr_seq_proc_adic_p);
end if;

end consiste_cota_munic_agenda;
/
