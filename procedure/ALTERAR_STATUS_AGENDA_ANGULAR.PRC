CREATE OR REPLACE 
procedure Alterar_status_agenda_angular	(cd_agenda_p		number,
					dt_agenda_p		date,
					nr_seq_agenda_p	number,
					ie_status_p		varchar2,
					cd_motivo_p		varchar2,
					ds_motivo_p		varchar2,
					ie_agenda_dia_p	varchar2,
					nm_usuario_p		varchar2) is

dt_agenda_w		date;
nr_minuto_w		number(10,0);
ie_status_w		varchar2(3);
cd_pf_w			varchar2(10);
cd_agenda_w		number(10,0);
cd_funcao_ativa_w	number(5);

ds_erro_w		varchar2(255);
cd_estabelecimento_w	number(10);

qt_hor_cancel_w		number(10,0);
ie_status_desf_exec_w	varchar2(3);
ie_motivo_col_pac_w	varchar2(20);
nr_atendimento_w	number(10);
nr_seq_ageint_w		number(10);
ie_cancela_ageint_w	varchar2(1);
nr_seq_status_w		number(10);
nr_seq_agenda_int_w	number(10);
cd_tipo_agenda_ww	number(10);
nr_seq_agenda_ww	number(10);
ie_altera_status_confirmada_w parametro_agenda.ie_altera_status_confirmada%type;

begin

if	(nr_seq_agenda_p is null) then
	select	obter_tipo_agenda(cd_agenda_p)
	into	cd_tipo_agenda_ww
	from	dual;
	
	if	(cd_tipo_agenda_ww in (3,5)) then
		
		select	max(nr_sequencia)
		into	nr_seq_agenda_ww
		from	agenda_consulta
		where	cd_agenda = cd_agenda_p
		and		dt_agenda = dt_agenda_p;
		
	elsif	(cd_tipo_agenda_ww = 2) then
		
		select	max(nr_sequencia)
		into	nr_seq_agenda_ww
		from	agenda_paciente
		where	cd_agenda = cd_agenda_p
		and		hr_inicio = dt_agenda_p;
	
	end if;

end if;

if	(cd_agenda_p is not null) and
	((nr_seq_agenda_p is not null) or (nr_seq_agenda_ww is not null)) and
	(ie_status_p is not null) then
	Obter_Param_Usuario(820, 130, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_motivo_col_pac_w);
	select	max(hr_inicio),
		max(nr_minuto_duracao),
		nvl(max(ie_status_agenda),ie_status_p),
		max(cd_pessoa_fisica),
		max(cd_agenda)
	into	dt_agenda_w,
		nr_minuto_w,
		ie_status_w,
		cd_pf_w,
		cd_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	select 	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	agenda
	where	cd_agenda = cd_agenda_w;


	select 	nvl(max(ie_status_desf_exec),'O')
	into	ie_status_desf_exec_w
	from	parametro_agenda
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	select	max(Obter_Funcao_Ativa)
	into	cd_funcao_ativa_w
	from	dual;

	if	(ie_status_p = 'B') then

		if	(ie_status_w = 'B') then

			update	agenda_paciente
			set	ie_status_agenda	= 'L',
				dt_status		= null,
				nm_usuario_status	= null,
				ds_motivo_status	= null,
				nm_usuario_bloq	= null,
				dt_bloqueio		= null,
				nr_seq_motivo_bloq	= null,
				nm_paciente		= null,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
		else

			update	agenda_paciente
			set	ie_status_agenda	= 'B',
				dt_status		= sysdate,
				nm_usuario_status	= nm_usuario_p,
				ds_motivo_status	= ds_motivo_p,
				nm_usuario_bloq		= nm_usuario_p,
				dt_bloqueio		= sysdate,
				nr_seq_motivo_bloq	= cd_motivo_p,
				nm_usuario		= nm_usuario_p,
				nm_paciente		= substr(decode(ie_motivo_col_pac_w, 'S', wheb_mensagem_pck.get_texto(795187) || ' (' || ds_motivo_p || ')', wheb_mensagem_pck.get_texto(791944)),1,60)
			where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
		end if;

	elsif	(ie_status_p = 'C') then

		select	max(hr_inicio),
				max(nr_minuto_duracao),
				nvl(max(ie_status_agenda),ie_status_p),
				max(cd_pessoa_fisica)
		into	dt_agenda_w,
				nr_minuto_w,
				ie_status_w,
				cd_pf_w
		from	agenda_paciente
		where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
		
		if	(ie_status_w = 'C') then

			consistir_horario_agenda_exame(cd_agenda_p, to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss'), nr_minuto_w, 'S', ds_erro_w);

			if	(ds_erro_w is not null) then
				Wheb_mensagem_pck.exibir_mensagem_abort(208949, 'DS_ERRO_W=' || ds_erro_w);

			else

				update	agenda_paciente
				set	hr_inicio			= to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss'),
					ie_status_agenda		= 'N',
					nm_usuario_status		= null,
					dt_status			= null,
					ds_motivo_status		= null,
					nm_usuario_cancel		= null,
					dt_cancelamento		= null,
					cd_motivo_cancelamento	= null,
					nm_usuario			= nm_usuario_p
				where	nr_sequencia			= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

			end if;

		else

			select	nvl(max(to_number(to_char(hr_inicio,'ss'))),0)+1
			into	qt_hor_cancel_w
			from	agenda_paciente
			where	cd_agenda = cd_agenda_p
			and		to_date(to_char(hr_inicio,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
			and		ie_status_agenda = 'C';

			update	agenda_paciente
			set		hr_inicio				= hr_inicio + qt_hor_cancel_w / 86400,
					ie_status_agenda		= 'C',
					nm_usuario_status		= nm_usuario_p,
					dt_status				= sysdate,
					ds_motivo_status		= ds_motivo_p,
					nm_usuario_cancel		= nm_usuario_p,
					dt_cancelamento			= sysdate,
					cd_motivo_cancelamento	= cd_motivo_p,
					nm_usuario				= nm_usuario_p
			where	nr_sequencia			= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

			
		--INICIO - Deletar registro da tabela AGEINT_MARCACAO_USUARIO quando o agendamento for cancelado para liberar o hor�rio novamente na Agenda Integrada		
		select	max(nr_sequencia),
				max(nr_seq_agenda_int)
		into	nr_seq_ageint_w,
				nr_seq_agenda_int_w
		from	agenda_integrada_item
		where	nr_seq_agenda_exame	= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);		
		
		if	(nr_seq_ageint_w is not null) and
			(cd_funcao_ativa_w = 820)then --Deve deletar somente se o cancelamento estiver sendo feito pela Agenda de Exames, liberando o hor�rio novamente para marca��o
			begin

			ie_cancela_ageint_w := Ageint_Obter_Se_Cancel_Agenda(nr_seq_agenda_int_w);			
			if	(ie_cancela_ageint_w = 'S') then
				select	min(nr_sequencia) 
				into	nr_seq_status_w
				from 	agenda_integrada_status 
				where	ie_situacao = 'A' 
				and 	ie_Status_tasy = 'CA';

				update	agenda_integrada
				set		nr_seq_status		= nr_seq_status_w,
						dt_fim_Agendamento	= sysdate
				where	nr_sequencia		= nr_seq_agenda_int_w;
				
			end if;

			
			delete	ageint_marcacao_usuario
			where	nr_seq_ageint_item = nr_seq_ageint_w;			
			exception
			when others then
				ds_erro_w	:= '';
			
			end;			
			
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;		
		end if;
		--FIM - Deletar registro da tabela AGEINT_MARCACAO_USUARIO quando o agendamento for cancelado para liberar o hor�rio novamente na Agenda Integrada
		
		end if;

	elsif	(ie_status_p in ('F','I')) then

		select	nvl(max(to_number(to_char(hr_inicio,'ss'))),0)+1
		into	qt_hor_cancel_w
		from	agenda_paciente
		where	cd_agenda = cd_agenda_p
		and	to_date(to_char(hr_inicio,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
		and	ie_status_agenda = ie_status_p;

		update	agenda_paciente
		set	hr_inicio			= hr_inicio + qt_hor_cancel_w / 86400,
			ie_status_agenda		= ie_status_p,
			dt_status			= sysdate,
			ds_motivo_status		= ds_motivo_p,
			nr_seq_motivo_falta		= cd_motivo_p,
			nm_usuario_status		= nm_usuario_p,
			nm_usuario			= nm_usuario_p
		where	nr_sequencia			= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'RF') then

		select	max(hr_inicio),
			max(nr_minuto_duracao),
			nvl(max(ie_status_agenda),ie_status_p),
			max(cd_pessoa_fisica)
		into	dt_agenda_w,
			nr_minuto_w,
			ie_status_w,
			cd_pf_w
		from	agenda_paciente
		where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
			
		update	agenda_paciente
		set	hr_inicio			= to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss'),
			ie_status_agenda		= 'N',
			nm_usuario_status		= null,
			dt_status			= null,
			ds_motivo_status		= null,
			nr_seq_motivo_falta = null,
			nm_usuario			= nm_usuario_p
		where	nr_sequencia			= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'R') then

		update	agenda_paciente
		set	ie_status_agenda	= 'R',
			dt_status		= null,
			ds_motivo_status	= null,
			nm_usuario_status	= null,
			nm_usuario		= nm_usuario_p,
			nm_usuario_reserva	= nm_usuario_p,
			dt_reserva		= sysdate,
			ds_obs_reserva		= ds_motivo_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
	elsif	(ie_status_p = 'AC') then
		select 	nvl(max(ie_altera_status_confirmada),'N')
		into 	ie_altera_status_confirmada_w
		from 	parametro_agenda
		where	cd_estabelecimento = obter_estabelecimento_ativo;
		
		update	agenda_paciente
		set	dt_confirmacao				= sysdate,
			nm_usuario_confirm			= nm_usuario_p,
			nm_usuario					= nm_usuario_p,
			ds_confirmacao 				= ds_motivo_p,
			nr_seq_forma_confirmacao 	= nvl(cd_motivo_p,''),
			ie_status_agenda			= decode(ie_altera_status_confirmada_w,'S','CN',ie_status_agenda)
		where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'RC') then
		select 	nvl(max(ie_altera_status_confirmada),'N')
		into 	ie_altera_status_confirmada_w
		from 	parametro_agenda
		where	cd_estabelecimento = obter_estabelecimento_ativo;
		
		update	agenda_paciente
		set	dt_confirmacao				= null,
			nm_usuario_confirm			= null,
			nm_usuario					= nm_usuario_p,
			ds_confirmacao 				=  '',
			nr_seq_forma_confirmacao 	= null,
			ie_status_agenda			= decode(ie_altera_status_confirmada_w,'S','N',ie_status_agenda)
		where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'A') then

		update	agenda_paciente
		set	ie_status_agenda	= 'A',
			dt_chegada			= sysdate,
			nm_usuario			= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'RA') then

		update	agenda_paciente
		set	ie_status_agenda	= 'N',
			dt_chegada		= null,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'O') then

		update	agenda_paciente
		set	ie_status_agenda	= 'O',
			dt_atendimento	= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
		
		select	max(nr_atendimento)
		into	nr_atendimento_w
		from	agenda_paciente
		where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
		
		update	atendimento_paciente
		set	DT_ATEND_MEDICO = sysdate
		where	nr_atendimento = nr_atendimento_w;

	elsif	(ie_status_p = 'RO') then

		update	agenda_paciente
		set	ie_status_agenda	= 'A',
			dt_atendimento	= null,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'E') then

		update	agenda_paciente
		set	ie_status_agenda	= 'E',
			dt_executada		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'RE') then

		update	agenda_paciente
		set	ie_status_agenda	= nvl(ie_status_desf_exec_w,'O'),
			dt_executada		= null,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'REE') then

		update	agenda_paciente
		set	ie_status_agenda	= 'O',
			dt_em_exame		= null,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww)
		and	dt_atendido is null;


		update	agenda_paciente
		set	ie_status_agenda	= 'AD',
			dt_em_exame		= null,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww)
		and	dt_atendido is not null;

	elsif	(ie_status_p = 'RAD') then

		update	agenda_paciente
		set	ie_status_agenda	= 'O',
			dt_atendido		= null,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'EE') then

		update	agenda_paciente
		set	ie_status_agenda	= 'EE',
			dt_em_exame		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);

	elsif	(ie_status_p = 'AD') then

		update	agenda_paciente
		set	ie_status_agenda	= 'AD',
			dt_atendido		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
	elsif	(ie_status_p = 'L') then

		update	agenda_paciente
		set	ie_status_agenda	= 'L',
			nm_usuario		= nm_usuario_p,
			nm_usuario_reserva	= null,
			dt_reserva		= null,
			ds_obs_reserva		= null
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
	elsif	(ie_status_p = 'AE') then

		update	agenda_paciente
		set	ie_status_agenda	= 'AE',
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
	elsif	(ie_status_p = 'RAE') then

		update	agenda_paciente
		set	ie_status_agenda	= 'N',
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
	elsif	(ie_status_p = 'ET') then
		update	agenda_paciente
		set	ie_status_agenda		= 'ET',
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww); 		
	elsif	(ie_status_p = 'AR') then
		update	agenda_paciente
		set	ie_status_agenda		= 'AR',
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nvl(nr_seq_agenda_p, nr_seq_agenda_ww); 		
	end if;

	if	(nvl(ie_agenda_dia_p,'N') = 'S') and
		(ie_status_p in ('A','O','AC','AD')) then

		select	max(hr_inicio),
				max(cd_pessoa_fisica)
		into	dt_agenda_w,
				cd_pf_w
		from	agenda_paciente
		where	nr_sequencia = nvl(nr_seq_agenda_p, nr_seq_agenda_ww);
		
		alterar_status_agenda_dia(cd_agenda_p, nvl(nr_seq_agenda_p, nr_seq_agenda_ww), dt_agenda_w, ie_status_p, cd_pf_w, nm_usuario_p);

	end if;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Alterar_status_agenda_angular;
/
