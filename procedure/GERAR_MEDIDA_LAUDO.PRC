CREATE OR REPLACE
PROCEDURE Gerar_Medida_Laudo(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) is


nr_seq_w		number(10);
nr_seq_proc_w		number(10);
nr_prescricao_w		number(10);
nr_seq_prescr_w		number(10);
dt_laudo_w		date;

qt_anos_w		number(5);
qt_peso_prescr_w	number(5);
ie_sexo_w		varchar2(10);
nr_atendimento_w	number(10);

cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);

qt_valor_minimo_w	number(15,4);
qt_valor_maximo_w	number(15,4);

vl_padrao_w		varchar2(255);
ds_valor_medida_w	varchar2(255);
ie_tipo_valor_w	varchar2(5);
qt_peso_w		number(15,4);
qt_altura_cm_w	number(15,4);
qt_medida_w		number(15,4);
nr_Seq_interno_w	number(10);
nr_seq_apresent_w	number(10);
nr_seq_apresentacao_w	number(10);
cd_protocolo_w		number(15,0);
cd_especialidade_w	number(15);
cd_grupo_proc_w		number(15);
cd_area_procedimento_w	number(15);
ie_tipo_medida_w	varchar2(15);


CURSOR C01 IS
select	c.nr_sequencia,
	upper(c.vl_padrao),
	c.ie_tipo_valor,
	nvl(a.nr_seq_apresent,0), 
	c.nr_seq_apresentacao
from	medida_exame_laudo c,
	tipo_medida_laudo b,
	tipo_medida_laudo_proc a
where	a.nr_seq_tipo		= b.nr_sequencia
and 	b.nr_sequencia		= c.nr_seq_tipo_medida
and	(a.nr_seq_proc_interno = nr_seq_interno_w)
and	Obter_Se_mostra_medida(cd_protocolo_w, b.nr_sequencia) = 'S'
and	c.ie_situacao		= 'A' 
and nvl(b.ie_situacao,'A')		= 'A'


union
select	c.nr_sequencia,
	upper(c.vl_padrao),
	c.ie_tipo_valor,
	nvl(a.nr_seq_apresent,0), 
	c.nr_seq_apresentacao
from	medida_exame_laudo c,
	tipo_medida_laudo b,
	tipo_medida_laudo_proc a
where	a.nr_seq_tipo		= b.nr_sequencia
and 	b.nr_sequencia		= c.nr_seq_tipo_medida
and	nvl(a.cd_procedimento,cd_procedimento_w)	= cd_procedimento_w 
and	nvl(a.ie_origem_proced,ie_origem_proced_w)	= ie_origem_proced_w
and	cd_especialidade_w	= nvl(a.cd_especialidade,cd_especialidade_w)
and	cd_grupo_proc_w		= nvl(a.cd_grupo_proc,cd_grupo_proc_w)
and	cd_area_procedimento_w	= nvl(a.cd_area_procedimento,cd_area_procedimento_w)
and	a.nr_seq_proc_interno is null
and	Obter_Se_mostra_medida(cd_protocolo_w, b.nr_sequencia) = 'S'
and	c.ie_situacao		= 'A'
and nvl(b.ie_situacao,'A')		= 'A'
order by 4,5;

Cursor C02 IS
SELECT	QT_VALOR_MINIMO, 
	QT_VALOR_MAXIMO
FROM	MEDIDA_EXAME_REFER
WHERE	NR_SEQ_MEDIDA = nr_seq_w
and	(ie_sexo = ie_sexo_w or ie_sexo = 'A')
and	qt_anos_w between qt_idade_minima and qt_idade_maxima
and	qt_peso_prescr_w between nvl(qt_peso_minimo,qt_peso_prescr_w) and nvl(qt_peso_maximo,qt_peso_prescr_w)
and	(nvl(ie_tipo_medida,1) = nvl(nvl(ie_tipo_medida_w, ie_tipo_medida) ,1))
order	by ie_sexo;

BEGIN

select	nr_seq_proc,
	nr_prescricao,
	nr_seq_prescricao,
	dt_laudo,
	ie_tipo_medida,
	cd_protocolo
into	nr_seq_proc_w,
	nr_prescricao_w,
	nr_seq_prescr_w,
	dt_laudo_w,
	ie_tipo_medida_w,
	cd_protocolo_w
from	laudo_paciente
where	nr_sequencia = nr_sequencia_p;

select	max(to_number(obter_idade(b.dt_nascimento, dt_laudo_w, 'A'))),
	max(nvl(b.ie_sexo,'A'))
into	qt_anos_w,
	ie_sexo_w
from	pessoa_fisica b,
	prescr_medica a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	a.nr_prescricao 	= nr_prescricao_w;

select	nvl(obter_peso_prescr_sinal(nr_prescricao_w),0)
into	qt_peso_prescr_w
from	dual;

if	(nr_seq_proc_w is not null) then
	begin
	select	cd_procedimento,
		ie_origem_proced,
		nr_Seq_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_Seq_interno_w
	from	procedimento_paciente
	where	nr_sequencia = nr_seq_proc_w;
	exception
	when others then
		cd_procedimento_w	:= null;
		ie_origem_proced_w	:= null;
	end;
else
	begin
	select	cd_procedimento,
		ie_origem_proced,
		nr_Seq_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_Seq_interno_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_w
	and	nr_sequencia	= nr_seq_prescr_w;
	exception
	when others then
		cd_procedimento_w	:= null;
		ie_origem_proced_w	:= null;
	end;
end if;

select	max(cd_especialidade),
	max(cd_grupo_proc),
	max(cd_area_procedimento)
into	cd_especialidade_w,
	cd_grupo_proc_w,
	cd_area_procedimento_w
from	estrutura_procedimento_v
where	cd_procedimento = cd_procedimento_w
and	ie_origem_proced = ie_origem_proced_w;

OPEN C01;
LOOP
FETCH C01 into 
	nr_seq_w,
	vl_padrao_w,
	ie_tipo_valor_w,
	nr_seq_apresent_w,
	nr_seq_apresentacao_w;
EXIT WHEN C01%NOTFOUND;
		
	qt_valor_minimo_w	:= null;
	qt_valor_maximo_w	:= null;

	open c02;
	loop
	fetch c02 into	
		qt_valor_minimo_w,
		qt_valor_maximo_w;
	exit when c02%notfound;
	end loop;
	close c02;

	ds_valor_medida_w	:= '';
	qt_medida_w		:= null;
	if	(vl_padrao_w = '@PESO') then

		select	QT_PESO
		into	qt_peso_w
		from	prescr_medica
		where	nr_prescricao	= nr_prescricao_w;
		if	(ie_tipo_valor_w = 'N') then
			qt_medida_w		:= qt_peso_w;
		else
			ds_valor_medida_w	:= qt_peso_w;
		end if;

	elsif	(vl_padrao_w = '@ALTURA') then

		select	QT_ALTURA_CM
		into	qt_altura_cm_w
		from	prescr_medica
		where	nr_prescricao	= nr_prescricao_w;
		if	(ie_tipo_valor_w = 'N') then
			qt_medida_w		:= qt_altura_cm_w;
		else
			ds_valor_medida_w	:= qt_altura_cm_w;
		end if;
	else
		if	(ie_tipo_valor_w = 'N') then
			qt_medida_w		:= substr(vl_padrao_w,1,15);
		else
			ds_valor_medida_w	:= vl_padrao_w;
		end if;
	end if;

	insert into laudo_paciente_medida
			(nr_sequencia,
			nr_seq_laudo,
			nr_seq_medida,
			dt_atualizacao,
			nm_usuario,
			qt_minimo,
			qt_maximo,
			ds_valor_medida,
			qt_medida)
	values		(laudo_paciente_medida_seq.nextval,
			nr_sequencia_p,
			nr_seq_w,
			sysdate,
			nm_usuario_p,
			qt_valor_minimo_w,
			qt_valor_maximo_w,
			ds_valor_medida_w,
			qt_medida_w);

END LOOP;
close C01;

commit;

END Gerar_Medida_Laudo;
/
