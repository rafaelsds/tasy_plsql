create or replace
procedure gerar_volume_ciclo_soluc(
			nr_seq_atendimento_p 	number,
			nr_seq_solucao_p	number, 
			nm_usuario_p		Varchar2) is 

qt_volume_medic_w	number(15,0);
qt_etapas_w		number(15,0);
qt_volume_total_w	number(15,0);
ie_bomba_infusao_w	varchar2(15);
qt_dosagem_w		number(15,4);
qt_tempo_aplicacao_w	number(15,4);
ie_tipo_dosagem_w	varchar2(15);
			
begin

select  sum(nvl(obter_dose_convertida(cd_material,qt_dose_prescricao,cd_unid_med_prescr,obter_unid_med_usua('ml')),0))
into	qt_volume_medic_w	
from	paciente_atend_medic
where	nr_seq_atendimento = nr_seq_atendimento_p
and	nr_seq_solucao = nr_seq_solucao_p;

select 	max(nr_etapas),
	max(ie_bomba_infusao),
	max(ie_tipo_dosagem),
	max(qt_tempo_aplicacao)
into 	qt_etapas_w,
	ie_bomba_infusao_w,
	ie_tipo_dosagem_w,
	qt_tempo_aplicacao_w
from	paciente_atend_soluc
where	nr_seq_solucao = nr_seq_solucao_p
and	nr_seq_atendimento = nr_seq_atendimento_p;

qt_volume_total_w := qt_volume_medic_w * qt_etapas_w;

Calcular_Volume_Total_Solucao(upper(ie_tipo_dosagem_w), qt_tempo_aplicacao_w, qt_volume_total_w, null, null, null, 1, qt_dosagem_w, 'N');

update 	paciente_atend_soluc
set 	qt_solucao_total 	= qt_volume_total_w,
	qt_dosagem		= qt_dosagem_w
where	nr_seq_atendimento  	= nr_seq_atendimento_p
and	nr_seq_solucao   	= nr_seq_solucao_p;

if	(ie_bomba_infusao_w = 'B') then
	Calcular_Bomba_elast_atend(nr_seq_atendimento_p,
				nr_seq_solucao_p, 
				nm_usuario_p); 
end if;
	
commit;

end gerar_volume_ciclo_soluc;
/
