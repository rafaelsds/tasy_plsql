create or replace
procedure ajusta_OC_item_reg_compra is 

nr_sequencia_w		number(10);
nr_ordem_compra_w	number(10);

cursor c01 is
select	nr_sequencia,
	lic_obter_oc_reg_compra(nr_seq_licitacao, nr_seq_lic_item,nr_seq_reg_lic_compra) nr_ordem_compra
from	reg_lic_compra_item
where	lic_obter_oc_reg_compra(nr_seq_licitacao, nr_seq_lic_item,nr_seq_reg_lic_compra) is not null;
	
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nr_ordem_compra_w;
exit when C01%notfound;
	begin
	update	reg_lic_compra_item
	set	nr_ordem_compra = nr_ordem_compra_w
	where	nr_sequencia = nr_sequencia_w;
	end;
end loop;
close C01;



commit;

end ajusta_OC_item_reg_compra;
/