create or replace procedure icon_gerar_exames_agfa is 

arq_texto_w		utl_file.file_type;

nm_local_grav_w		varchar2(60):= '/mnt/agfa51/';  --Caminho onde ser� salvo o arquivo   **** SUBSTITUIR POR CAMINHO V�LIDO, HABILITADO NO ORACLE ****

ds_procedimento_w	varchar2(255);
cd_procedimento_w	number(10);
ie_sexo_w		varchar2(1);
ds_nascimento_w		varchar2(8);
nr_atendimento_w	number(10);
nm_pessoa_fisica_w	varchar2(100);
nr_prescricao_w		number(10);
nr_seq_w 		varchar2(20);
ie_open_mode_w		varchar2(1);



cursor c01 is
	select 	substr(obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced ), 1, 255) ds_procedimento ,
		a.cd_procedimento ,
		f.ie_sexo ,
		to_char(f.dt_nascimento, 'yyyymmdd') dt_nascimento,
		t.nr_atendimento,
		SUBSTR(OBTER_NOME_PF(f.CD_PESSOA_FISICA),0,100),
		a.nr_prescricao,
		a.nr_sequencia
	from	prescr_procedimento a,
		prescr_medica b,
		procedimento p,
		atendimento_paciente t,
		pessoa_fisica f
	where 	a.nr_prescricao   = b.nr_prescricao
	and	a.cd_procedimento = p.cd_procedimento
	and	t.nr_atendimento  = b.nr_atendimento	
	and	t.cd_pessoa_fisica  = f.cd_pessoa_fisica
	and 	a.dt_integracao IS NULL
	and 	p.ie_origem_proced = 1
	and 	p.cd_tipo_procedimento in (1,34)
	and	trunc(a.dt_atualizacao) = trunc(sysdate);

begin


begin
	
	--Verifica se a ultima vez que a job executou � o mesmo dia que o atual            A = Append ,  W = Write
	select 	decode(trunc(last_date), trunc(sysdate),'A','W')
	into	ie_open_mode_w
	from 	user_jobs
	where 	what = 'icon_gerar_exames_agfa;';

	--abre o arquivo
	arq_texto_w := utl_file.fopen(nm_local_grav_w,'AGFA.txt', ie_open_mode_w);

	open c01;
	loop
	fetch c01 into
		ds_procedimento_w,
		cd_procedimento_w,
		ie_sexo_w,
		ds_nascimento_w,
		nr_atendimento_w,
		nm_pessoa_fisica_w,
		nr_prescricao_w,
		nr_seq_w;
	exit when c01%notfound;
		
		--gera uma nova linha no arquivo
		utl_file.put_line(arq_texto_w, ','||
				  ds_procedimento_w||','||
				  cd_procedimento_w||','||
				  ie_sexo_w||','||
				  ds_nascimento_w||','||
				  nr_atendimento_w||','||
				  nm_pessoa_fisica_w||','||
				  ' ,V1');	
		
		
		update 	prescr_procedimento
		set 	dt_integracao = sysdate
		where 	nr_prescricao = nr_prescricao_w 
		and 	nr_sequencia  = nr_seq_w;
	
	
	end loop;
	close c01;
	
	--fecha e libera o arquivo
	utl_file.fclose(arq_texto_w);	
	
	commit;
	
exception
	when others then
		rollback;
end;


end icon_gerar_exames_agfa;
/