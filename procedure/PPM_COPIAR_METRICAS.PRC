create or replace
procedure PPM_COPIAR_METRICAS
			(nr_seq_metricas_p	varchar2,
			cd_pessoas_p		varchar2,
			nm_usuario_p		varchar2) is
	
pessoas_w		dbms_sql.varchar2_table;
metricas_w		dbms_sql.varchar2_table;
i			integer;
k			integer;
qt_w			number(10);
ppm_objetivo_w		ppm_objetivo%rowtype;
ppm_objetivo_meta_w	ppm_objetivo_meta%rowtype;
ppm_objetivo_metrica_w	ppm_objetivo_metrica%rowtype;
nr_seq_metrica_w	ppm_objetivo_metrica.nr_seq_metrica%type;
nr_seq_meta_w		ppm_objetivo_meta.nr_sequencia%type;
nr_seq_objetivo_w	ppm_objetivo.nr_sequencia%type;

cursor c01 is
select	last_day(dt_mes) dt_mes
from	mes_v
where	trunc(dt_mes,'year') = trunc(sysdate,'year')
and	trunc(dt_mes,'month') <> trunc(sysdate,'month');

c01_w	c01%rowtype;
	
begin

pessoas_w	:= obter_lista_string(cd_pessoas_p, ',');
metricas_w	:= obter_lista_string(nr_seq_metricas_p, ',');
k:= 0;
i:= 0;

for 	k in pessoas_w.first..pessoas_w.last loop
	begin
	
	for	i in metricas_w.first..metricas_w.last loop
		begin

		select	nr_seq_metrica
		into	nr_seq_metrica_w
		from	ppm_objetivo_metrica
		where	nr_sequencia	= metricas_w(i);
		
		select	count(*)
		into	qt_w
		from	ppm_objetivo_metrica a,
			ppm_objetivo_meta b,
			ppm_objetivo c
		where	a.nr_seq_meta		= b.nr_sequencia
		and	b.nr_seq_objetivo	= c.nr_sequencia
		and	c.cd_pessoa_fisica	= pessoas_w(k)
		and	a.nr_seq_metrica	= nr_seq_metrica_w
		and	sysdate between DT_INICIO_VIGENCIA and nvl(DT_FINAL_VIGENCIA,sysdate+1);
		
		if	(qt_w = 0) then
		
			select	c.*		
			into	ppm_objetivo_w
			from	ppm_objetivo_metrica a,
				ppm_objetivo_meta b,
				ppm_objetivo c
			where	a.nr_seq_meta		= b.nr_sequencia
			and	b.nr_seq_objetivo	= c.nr_sequencia
			and	a.nr_sequencia		= metricas_w(i);
			
			select	max(nr_sequencia)
			into	nr_seq_objetivo_w
			from	ppm_objetivo
			where	cd_pessoa_fisica	= pessoas_w(k)
			and	sysdate 		between DT_INICIO_VIGENCIA and nvl(DT_FINAL_VIGENCIA,sysdate+1)
			and	nr_seq_area		= ppm_objetivo_w.nr_seq_area;
			
			if	(nr_seq_objetivo_w is null) then
			
				select	ppm_objetivo_seq.nextval
				into	ppm_objetivo_w.nr_sequencia
				from	dual;
				
				ppm_objetivo_w.dt_atualizacao	:= sysdate;
				ppm_objetivo_w.nm_usuario	:= nm_usuario_p;
				ppm_objetivo_w.cd_pessoa_fisica	:= pessoas_w(k);
				ppm_objetivo_w.nr_seq_gerencia	:= null;
				
				insert into ppm_objetivo values ppm_objetivo_w;

				nr_seq_objetivo_w := ppm_objetivo_w.nr_sequencia;
			
			end if;			
			
			select	b.*	
			into	ppm_objetivo_meta_w
			from	ppm_objetivo_metrica a,
				ppm_objetivo_meta b,
				ppm_objetivo c
			where	a.nr_seq_meta		= b.nr_sequencia
			and	b.nr_seq_objetivo	= c.nr_sequencia
			and	a.nr_sequencia		= metricas_w(i);
			
			select	max(nr_sequencia)
			into	nr_seq_meta_w
			from	ppm_objetivo_meta a
			where	a.nr_seq_objetivo	= nr_seq_objetivo_w
			and	a.ie_tipo_meta		= ppm_objetivo_meta_w.ie_tipo_meta
			and	lower(a.DS_TITULO)	= lower(ppm_objetivo_meta_w.ds_titulo);
			
			if	(nr_seq_meta_w is null) then
			
				select	ppm_objetivo_meta_seq.nextval
				into	ppm_objetivo_meta_w.nr_sequencia
				from	dual;
			
				ppm_objetivo_meta_w.dt_atualizacao	:= sysdate;
				ppm_objetivo_meta_w.nm_usuario		:= nm_usuario_p;
				ppm_objetivo_meta_w.nr_seq_objetivo	:= nr_seq_objetivo_w;
				
				insert into ppm_objetivo_meta values ppm_objetivo_meta_w;
				nr_seq_meta_w	:= ppm_objetivo_meta_w.nr_sequencia;
			end if;
						
			select	a.*	
			into	ppm_objetivo_metrica_w
			from	ppm_objetivo_metrica a,
				ppm_objetivo_meta b,
				ppm_objetivo c
			where	a.nr_seq_meta		= b.nr_sequencia
			and	b.nr_seq_objetivo	= c.nr_sequencia
			and	a.nr_sequencia		= metricas_w(i);
		
			select	ppm_objetivo_metrica_seq.nextval
			into	ppm_objetivo_metrica_w.nr_sequencia
			from	dual;
			
			ppm_objetivo_metrica_w.dt_atualizacao	:= sysdate;
			ppm_objetivo_metrica_w.nm_usuario	:= nm_usuario_p;
			ppm_objetivo_metrica_w.nr_seq_meta	:= nr_seq_meta_w;
			
			insert into ppm_objetivo_metrica values ppm_objetivo_metrica_w;		
		
		end if;
		end;
	end loop;
	
	end;
end loop;

commit;

--meses anteriores
/*open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	ppm_calcular_metricas(nm_usuario_p,c01_w.dt_mes,null);

end loop;
close C01;*/

--mes atual
--ppm_calcular_metricas(nm_usuario_p,sysdate-1,null);

end PPM_COPIAR_METRICAS;
/