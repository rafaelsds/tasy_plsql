create or replace
PROCEDURE GV_Inserir_Proced_Conta(nr_atendimento_p  number,
			         qt_procedimento_p  varchar2,
				 nm_usuario_p varchar2,
				 cd_setor_atendimento_p number,
				 cd_estabelecimento_p	number
				 ) is

nr_seq_proc_pac_w  	number(10,0)  	:= 0;
nr_atendimento_w  	number(10,0)  	:=  0;
nr_seq_atepacu_w  	number(10,0)  	:=  0;
nr_seq_proc_interno_w 	number(10,0)  	:= 0;
cd_convenio_w   	number(10,0)  	:= 0;
cd_convenio_regra_w	number(10,0);
cd_categoria_w		number(10,0)  	:= 0;
cd_pessoa_fisica_w	number(10,0)  	:= 0;
dt_ent_unidade_w	date 		:= sysdate;
cd_local_estoque_w	number(10);
ds_erro_w		varchar2(255);
qt_registros_w		number(10,0);
cd_procedimento_w   	number(15,0)  	:= 0;
ie_origem_proced_w   	number(1,0)  	:= null;
cd_categoria_regra_w	number(10,0);

Cursor C01 is
	select	nr_seq_proc_interno,
		cd_convenio,
		cd_categoria
	from	regra_inserir_diaria_gv;

BEGIN

select 	nvl(count(*),'0')
into	qt_registros_w
from	regra_inserir_diaria_gv;

if 	((qt_registros_w > 0) and (nr_atendimento_p <> '0') and (nr_atendimento_p is not null)) then
	begin
		
	select 	max(obter_convenio_atendimento(a.nr_atendimento)),
		max(obter_dados_categ_conv(a.nr_atendimento,'CA')),
		max(a.cd_pessoa_fisica)
	into  	cd_convenio_w,
		cd_categoria_w,
		cd_pessoa_fisica_w
	from  	atendimento_paciente a
	where 	a.nr_atendimento = nr_atendimento_p;

	-- NR_SEQ_ATEPACU -- obtem data entrada unidade do atendimento quando setor for igual ao do usuario no momento da dialise
	select 	max(w.dt_entrada_unidade)
	into	dt_ent_unidade_w
	from   	atend_paciente_unidade w
	where  	w.nr_atendimento 		= nr_atendimento_p
	and    	w.cd_setor_atendimento 		= cd_setor_atendimento_p;

		BEGIN
		select	max(a.nr_seq_interno)
		into	nr_seq_atepacu_w
		from 	atend_paciente_unidade a
		where 	a.cd_setor_atendimento		= cd_setor_atendimento_p
		and	a.nr_atendimento 		= nr_atendimento_p
		and	trunc(a.dt_entrada_unidade) 	= trunc(dt_ent_unidade_w);
		EXCEPTION
		WHEN others then
			nr_seq_atepacu_w := 0;
		END;

	IF dt_ent_unidade_w is null then
		dt_ent_unidade_w := sysdate; --data entrada unidade, se tiver null eh pq nao teve passagem, atribui sysdate neste caso.
	END IF;

	--Se n�o possuir passagem naquele setor / atendimento, � preciso gerar passagem, se precisar gerar passagem  e n�o possuir NR_ATENDIMENTO, n�o ser� possivel gerar passagem,
	--neste caso a procedure vai ter q abortar

	IF ((nr_atendimento_p = '0') or (nr_atendimento_p is null)) then
		ds_erro_w := wheb_mensagem_pck.get_texto(191446)||chr(13);
		--Para inserir o procedimento automaticamente na conta � necess�rio existir o n�mero do atendimento
		Wheb_mensagem_pck.exibir_mensagem_abort(193594,'DS_ERRO=' || ds_erro_w);
	END IF;

	--se nao achar passagem setor, gera passagem.
	IF	(nvl(nr_seq_atepacu_w,0) = 0) then
		Gerar_Passagem_Setor_Atend(	nr_atendimento_p,
							cd_setor_atendimento_p,
							sysdate,--dt_ent_unidade_w,
							'S',
							nm_usuario_p);
	END IF;

	select	max(a.nr_seq_interno)
	into	nr_seq_atepacu_w
	from 	atend_paciente_unidade a
	where 	a.cd_setor_atendimento		= cd_setor_atendimento_p
	and	a.nr_atendimento 		= nr_atendimento_p
	and	trunc(a.dt_entrada_unidade) 	= trunc(dt_ent_unidade_w);

	select	max(cd_local_estoque)
	into	cd_local_estoque_w
	from	setor_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_p;

	OPEN C01;
	LOOP
	FETCH 	C01 into
		nr_seq_proc_interno_w,
		cd_convenio_regra_w,
		cd_categoria_regra_w;
	EXIT 	WHEN C01%notfound;
	
		if	(cd_convenio_regra_w <> cd_convenio_w) then
			cd_categoria_w	:= null;
		end if;
	
		obter_proc_tab_interno_conv(
					nr_seq_proc_interno_w,
					cd_estabelecimento_p,
					nvl(cd_convenio_regra_w,cd_convenio_w),
					nvl(cd_categoria_regra_w,cd_categoria_w),
					null,
					null,
					cd_procedimento_w,
					ie_origem_proced_w,
					null,
					sysdate,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);
	
		select 	procedimento_paciente_seq.NEXTVAL
		into  	nr_seq_proc_pac_w
		from  	dual;

		insert into procedimento_paciente
			( 	nr_sequencia,
			        nr_atendimento,
			        dt_entrada_unidade,
			        cd_procedimento,
			        dt_procedimento,
			        qt_procedimento,
			        dt_atualizacao,
			        nm_usuario,
			        cd_setor_atendimento,
			        ie_origem_proced,
			        nr_seq_atepacu,
			        nr_seq_proc_interno,
				cd_convenio,
				cd_categoria,
				cd_pessoa_fisica)
		values  (	nr_seq_proc_pac_w,
				nr_atendimento_p,
				dt_ent_unidade_w,
				cd_procedimento_w,
				sysdate,
				qt_procedimento_p,
				sysdate,
				nm_usuario_p,
				cd_setor_atendimento_p,
				ie_origem_proced_w,
				nr_seq_atepacu_w,
				nr_seq_proc_interno_w,
				nvl(cd_convenio_regra_w,cd_convenio_w),
				nvl(cd_categoria_regra_w,cd_categoria_w),
				cd_pessoa_fisica_w);

		consiste_exec_procedimento(nr_seq_proc_pac_w, ds_erro_w);
		atualiza_preco_procedimento(nr_seq_proc_pac_w, nvl(cd_convenio_regra_w,cd_convenio_w), nm_usuario_p);
		gerar_lancamento_automatico(nr_atendimento_p,cd_local_estoque_w,34,nm_usuario_p,nr_seq_proc_pac_w,null,null,null,null,null);
	END LOOP;
	CLOSE C01;

	COMMIT;

	end;
end if;

END GV_Inserir_Proced_Conta;
/
