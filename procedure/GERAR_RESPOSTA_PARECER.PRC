create or replace
procedure Gerar_resposta_parecer ( 	nr_seq_regulacao_p		number,
									nm_usuario_p			varchar2,
									nr_seq_interno_p 		out number) is
									
nr_seq_interno_ori_w	number(10);
nr_parecer_ori_w		number(10);
nr_seq_interno_w		number(10);
cd_profissional_w		varchar2(10);

begin

If ( nvl(nr_seq_regulacao_p,0) > 0) then

	Select 	nvl(max(nr_seq_resp_parecer_ori),0),
			nvl(max(nr_seq_parecer_ori),0)
	into	nr_seq_interno_ori_w,
			nr_parecer_ori_w
	from	regulacao_atend
	where	nr_sequencia = nr_seq_regulacao_p;
	
	if ( nr_seq_interno_ori_w > 0 ) then
	
		nr_seq_interno_w := nr_seq_interno_ori_w;
		
	elsif ( nr_parecer_ori_w > 0) then	
	
		Select 	parecer_medico_seq.nextval,
				Obter_Pf_Usuario(nm_usuario_p,'C')
		into	nr_seq_interno_w,
				cd_profissional_w
		from 	dual;
		
		insert into parecer_medico (	nr_seq_interno, 
										nr_parecer,
										nr_sequencia,
										cd_medico,
										dt_atualizacao,
										nm_usuario,
										ie_situacao,
										ie_status,
										nr_seq_regulacao )
							values (	nr_seq_interno_w,
										nr_parecer_ori_w,
										1,
										cd_profissional_w,
										sysdate,
										nm_usuario_p,
										'A',
										'F',
										nr_seq_regulacao_p);
										
		commit;
		
		update	regulacao_atend
		set		dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p,
				nr_seq_resp_parecer_ori = nr_seq_interno_w
		where	nr_sequencia = nr_seq_regulacao_p;
		
		commit;
		
	end if;

end if;

nr_seq_interno_p := nr_seq_interno_w;

end Gerar_resposta_parecer;
/