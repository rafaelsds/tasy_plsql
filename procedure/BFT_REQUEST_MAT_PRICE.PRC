create or replace
procedure bft_request_mat_price	(	ie_all_records_p	varchar2,
					nr_reference_p		number,
					cd_estabelecimento_p	number,
					cd_price_material_tab_p	number,
					nm_usuario_p		Varchar2) is 

ds_json_param_w		clob;	
ds_json_result_w	clob;	
json_w		philips_json;
					
begin


json_w := philips_json();
json_w.put('allRecords', nvl(ie_all_records_p,'S'));
json_w.put('referenceNumber', nvl(nr_reference_p,0));
json_w.put('establishmentId', nvl(cd_estabelecimento_p,0));
json_w.put('materialPriceTableCode', nvl(cd_price_material_tab_p,0));
dbms_lob.createtemporary(ds_json_param_w, true);
json_w.to_clob(ds_json_param_w);


select	bifrost.send_integration_content('request.material.price',ds_json_param_w,nm_usuario_p,'N')
into	ds_json_result_w
from 	dual;

commit;

end bft_request_mat_price;
/