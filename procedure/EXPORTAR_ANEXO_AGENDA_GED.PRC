create or replace procedure exportar_anexo_agenda_ged(
						nm_usuario_p		varchar2,
						cd_profissional_p	Varchar2,
						nr_seq_agenda_p		number) is

nr_sequencia_w		number(10);
nr_atendimento_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ds_arquivo_w		varchar2(255);
ds_titulo_w			varchar2(255);
cd_estabelecimento_w	ged_atendimento.cd_estabelecimento%type;

cursor c01 is 
select	ds_arquivo
from	anexo_agenda
where	nr_seq_agenda	= nr_seq_agenda_p;				    

begin

select 	max(nr_atendimento),
	max(cd_pessoa_fisica)
into	nr_atendimento_w,
	cd_pessoa_fisica_w
from	agenda_paciente
where	nr_sequencia 	=	nr_seq_agenda_p;

ds_titulo_w	:= wheb_mensagem_pck.get_texto(300734);
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento; 
open C01;
loop
fetch C01 into	
	ds_arquivo_w;
exit when C01%notfound;
	begin
	select 	ged_atendimento_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into ged_atendimento(
			nr_sequencia,
			ds_arquivo,
			ds_titulo,
			cd_profissional,
			dt_registro,
			dt_atualizacao,
			nm_usuario,
			cd_pessoa_fisica,
			nr_atendimento,
			cd_estabelecimento)
	values(	nr_sequencia_w,
			ds_arquivo_w,
			ds_titulo_w,
			cd_profissional_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_w,
			nr_atendimento_w,
			cd_estabelecimento_w);
	end;
end loop;
close C01;

end Exportar_anexo_agenda_ged;
/
