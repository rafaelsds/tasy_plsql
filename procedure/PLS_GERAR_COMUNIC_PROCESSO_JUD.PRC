create or replace
procedure pls_gerar_comunic_processo_jud
			(	nr_seq_processo_p	number,
				ds_usuario_p		varchar2,
				ds_perfil_p		varchar2,
				ds_titulo_p		varchar2,
				nm_usuario_p		Varchar2) is 

ds_usuario_w		varchar2(4000);
ds_perfil_w		varchar2(4000);
ds_historico_w		varchar2(4000);

begin
ds_usuario_w	:= substr(ds_usuario_p, 1, length(ds_usuario_p) - 2);
ds_perfil_w	:= substr(ds_perfil_p, 1, length(ds_perfil_p) - 2);

select	substr(max(a.ds_historico),1,4000)
into	ds_historico_w
from	processo_judicial_hist	a
where	a.nr_seq_processo	= nr_seq_processo_p
and	a.dt_historico		=	(select	max(x.dt_historico)
					from	processo_judicial_hist	x
					where	x.nr_seq_processo	= nr_seq_processo_p);
					
if	(ds_historico_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(255095);
end if;

insert into comunic_interna	
	(dt_comunicado,
	ds_titulo,
	ds_comunicado,
	nm_usuario,
	dt_atualizacao,
	ie_geral,
	nm_usuario_destino,
	cd_perfil,
	nr_sequencia,
	ie_gerencial,
	nr_seq_classif,
	ds_perfil_adicional,
	cd_setor_destino,
	cd_estab_destino,
	ds_setor_adicional, 
	dt_liberacao,
	ds_grupo,
	nm_usuario_oculto, 
	nr_atendimento,
	ds_grupo_perfil,
	ds_usuarios_ocultos,
	nr_seq_agenda)
values	(sysdate,
	ds_titulo_p,
	ds_historico_w,--'O protocolo ' || nr_seq_protocolo_p || ' est� liberado pelo Itamed.',
	nm_usuario_p,
	sysdate,
	'N',
	ds_usuario_w,
	null,
	comunic_interna_seq.nextval,
	'N',
	null,
	ds_perfil_w,
	null,
	null,
	null,
	sysdate,
	null,
	null,
	null,
	null,
	null,
	null);
	
commit;

end pls_gerar_comunic_processo_jud;
/