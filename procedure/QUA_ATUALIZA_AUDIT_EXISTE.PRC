create or replace
procedure qua_atualiza_audit_existe (
	nr_sequencia_p		number,
	ie_existe_p		varchar2,
	nm_usuario_p		Varchar2) is 

begin

if	(nr_sequencia_p is not null) then
	begin
	
	update	qua_audit_result_mat 
	set	ie_existe = ie_existe_p
	where	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end qua_atualiza_audit_existe ;
/
