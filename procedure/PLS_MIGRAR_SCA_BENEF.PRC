create or replace
procedure pls_migrar_sca_benef
			(	nr_seq_pessoa_proposta_p	Number, 
				nr_seq_segurado_novo_p		Number, 
				nm_usuario_p			Varchar2) is 

nr_seq_vinculo_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_vinculo_sca_novo_w	number(10);
nr_seq_tabela_w			number(10);
nr_seq_plano_sca_w		number(10);
dt_reajuste_sca_w		date;
ie_lancamento_mensalidade_w	varchar2(10);
dt_contrato_w			date;
ie_geracao_valores_w		varchar2(10);
dt_contratacao_w		date;

Cursor C01 is
	select	nr_sequencia,
		nr_seq_tabela,
		nr_seq_plano
	from	pls_sca_vinculo
	where	nr_seq_benef_proposta	= nr_seq_pessoa_proposta_p;
	
begin

select	nr_seq_contrato,
	dt_contratacao
into	nr_seq_contrato_w,
	dt_contratacao_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_novo_p;

select	ie_geracao_valores
into	ie_geracao_valores_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_w;

open C01;
loop
fetch C01 into	
	nr_seq_vinculo_w,
	nr_seq_tabela_w,
	nr_seq_plano_sca_w;
exit when C01%notfound;
	begin
	select	pls_sca_vinculo_seq.NextVal
	into	nr_seq_vinculo_sca_novo_w
	from	dual;
  
	select	dt_reajuste,
		ie_lancamento_mensalidade
	into	dt_reajuste_sca_w,
		ie_lancamento_mensalidade_w
	from	pls_plano
	where	nr_sequencia = nr_seq_plano_sca_w;

	insert into pls_sca_vinculo (	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, 
					nr_seq_segurado, nr_seq_tabela, ie_grau_dependencia, ie_dependente, dt_inicio_vigencia, 
					dt_fim_vigencia, qt_idade_limite, nr_seq_plano, ds_observacao, ie_embutido_produto, nr_seq_plano_origem, 
					nr_parcela_inicial, nr_parcela_final, nr_seq_vendedor_canal, nr_seq_vendedor_pf, 
					dt_reajuste, ie_geracao_valores, nr_seq_pagador, ds_consistencia, ie_lancamento_mensalidade, 
					dt_reativacao, dt_inclusao_benef)
				(select	nr_seq_vinculo_sca_novo_w, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
					nr_seq_segurado_novo_p, nr_seq_tabela_w, ie_grau_dependencia, ie_dependente, nvl(dt_inicio_vigencia,dt_contratacao_w), 
					dt_fim_vigencia, qt_idade_limite, nr_seq_plano, wheb_mensagem_pck.get_texto(1104295), 'N', nr_seq_plano_origem, 
					nr_parcela_inicial, nr_parcela_final, nr_seq_vendedor_canal, nr_seq_vendedor_pf, 
					dt_reajuste_sca_w, ie_geracao_valores_w, nr_seq_pagador, ds_consistencia, ie_lancamento_mensalidade_w, 
					dt_reativacao, dt_inclusao_benef
				from	pls_sca_vinculo
				where	nr_sequencia	= nr_seq_vinculo_w);
	
	pls_duplicar_tabela_preco_sca(nr_seq_contrato_w,nr_seq_vinculo_sca_novo_w,nr_seq_tabela_w,'N',nm_usuario_p);
	end;
end loop;
close C01;

end pls_migrar_sca_benef;
/
