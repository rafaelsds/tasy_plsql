create or replace
procedure	man_alt_dt_entrega_os_acordo	(nr_seq_os_p	number,
						dt_entrega_p	date) is

nr_seq_acordo_w	desenv_acordo_os.nr_sequencia%type;
cd_versao_w	version_merge.cd_versao%type;


cursor c01 is

	select	o.nr_sequencia
	from	desenv_acordo_os o,
		desenv_acordo a
	where	o.nr_seq_ordem_servico = nr_seq_os_p
	and	o.nr_seq_acordo = a.nr_sequencia;

begin

	select	cd_versao
	into	cd_versao_w
	from	version_merge
	where	dt_version_generated = dt_entrega_p
	order by dt_version_generated;

	open c01;
	loop
	fetch c01 into	nr_seq_acordo_w;
		exit when c01%notfound;

		update	desenv_acordo_os
		set	dt_entrega_prev = dt_entrega_p
		where	nr_seq_acordo_w = nr_sequencia;

	end loop;
	close c01;
	
	if(cd_versao_w is not null) then
		update	man_ordem_servico
		set	cd_versao_prevista = cd_versao_w
		where	nr_sequencia = nr_seq_os_p;
		
		insert into man_os_ctrl_versao(
			nr_sequencia,
			nr_seq_ordem_serv,
			dt_atualizacao,
			nm_usuario,
			ds_justificativa,
			cd_estabelecimento,
			cd_versao)
		values(	man_os_ctrl_versao_seq.nextval,
			nr_seq_os_p,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			obter_desc_expressao_idioma (877804, 'Ordem de servico adicionada no acordo', wheb_usuario_pck.get_nr_seq_idioma),
			wheb_usuario_pck.get_cd_estabelecimento,
			cd_versao_w);
	end if;

commit;

end  man_alt_dt_entrega_os_acordo;
/
