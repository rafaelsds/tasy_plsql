create or replace procedure fabio_teste(ds_texto IN VARCHAR2) is
    PRAGMA AUTONOMOUS_TRANSACTION;
begin
    insert into fabio_log(nr_sequencia, ds_mensagem, dt_atualizacao)   
    values(SEQ_FABIO_LOG.nextval,'ENTROU FABIO_TESTE: '||ds_texto, sysdate);
    commit;
end; 