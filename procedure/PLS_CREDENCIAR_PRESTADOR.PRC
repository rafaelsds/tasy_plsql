create or replace
procedure pls_credenciar_prestador
		(	nr_seq_prestador_p	in	Number,
			ie_tipo_prestador_p	in	Number,
			nm_usuario_p		in	Varchar2,
			dt_credenciamento_p	in	Date,
			ds_erro_p		out	Varchar2) is
			
/* IE_TIPO_P
	1 - PLS_PRESTADOR
	2 - PLS_PRESTADOR_MEDICO
*/

ds_erro_w		Varchar2(255)	:= '';
nr_seq_prestador_w	Number(10);
nr_seq_prest_medico_w	Number(10);
cd_pessoa_fisica_w	Varchar2(10);
nm_pessoa_fisica_w	Varchar2(60);
cd_estabelecimento_w	Number(4);
nr_seq_prestador_hist_w	number(10) := '';

begin

if	(ie_tipo_prestador_p	= 1) then
	/* Obter o profissional da PLS_PRESTADOR */
	select	cd_pessoa_fisica,
		substr(obter_nome_pf(cd_pessoa_fisica),1,60),
		nr_sequencia
	into	cd_pessoa_fisica_w,
		nm_pessoa_fisica_w,
		nr_seq_prestador_hist_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
elsif	(ie_tipo_prestador_p	= 2) then
	/* Obter o profissional da PLS_PRESTADOR_MEDICO */
	select	cd_medico,
		substr(obter_nome_pf(cd_medico),1,60),
		nr_seq_prestador
	into	cd_pessoa_fisica_w,
		nm_pessoa_fisica_w,
		nr_seq_prestador_hist_w
	from	pls_prestador_medico
	where	nr_sequencia	= nr_seq_prestador_p;
end if;
/*
alterra��o feita pelo usuario: cabelli
N� Ordem: 218456

select	nvl(max(nr_sequencia),0)
into	nr_seq_prestador_w
from	pls_prestador
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_vinculo		= 'C';

select	nvl(max(nr_seq_prestador),0)
into	nr_seq_prest_medico_w
from	pls_prestador_medico
where	cd_medico		= cd_pessoa_fisica_w
and	ie_tipo_vinculo		= 'C';

if	(nr_seq_prestador_w	> 0) then
	ds_erro_w	:= wheb_mensagem_pck.get_texto(280352, 'NM_PESSOA_FISICA_P=' || nm_pessoa_fisica_w ||
					';CD_PESSOA_FISICA_P=' || cd_pessoa_fisica_w ||
					';NR_SEQ_PRESTADOR_P=' || nr_seq_prestador_w);
elsif	(nr_seq_prest_medico_w	> 0) then
	ds_erro_w	:=  wheb_mensagem_pck.get_texto(280352, 'NM_PESSOA_FISICA_P=' || nm_pessoa_fisica_w ||
					';CD_PESSOA_FISICA_P=' || cd_pessoa_fisica_w ||
					';NR_SEQ_PRESTADOR_P=' || nr_seq_prest_medico_w);
else
*/
	if	(ie_tipo_prestador_p	= 1) then
		update	pls_prestador
		set	ie_tipo_vinculo	= 'C',
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_prestador_p
		and	cd_pessoa_fisica is not null;
		
		pls_altera_vinculo_prof(nr_seq_prestador_p,'C');
		
	elsif	(ie_tipo_prestador_p	= 2) then
		update	pls_prestador_medico
		set	ie_tipo_vinculo	= 'C',
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_prestador_p;
	end if;
	
	/* Obter o estabelecimento do prestador */
	select	nvl(max(cd_estabelecimento),1)
	into	cd_estabelecimento_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_hist_w;
	
	/* Gravar hist�rico da a��o */
	insert into pls_credenciamento_hist
		(nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_prestador,
		cd_pessoa_fisica,
		ie_tipo_historico,
		dt_historico)
	values(	pls_credenciamento_hist_seq.nextval,
		cd_estabelecimento_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_prestador_hist_w,
		cd_pessoa_fisica_w,
		'C',
		dt_credenciamento_p);
	
	commit;
--end if;

ds_erro_p	:= ds_erro_w;

end pls_credenciar_prestador;
/