create or replace
procedure man_estornar_inicio_caso_teste(
			nr_sequencia_p	number,
			nm_usuario_p	varchar2) is 

begin
if	(nr_sequencia_p is not null) then
	begin	
	update teste_caso_teste
		set dt_inicio = null,
		dt_atualizacao = sysdate, 
		nm_usuario = nm_usuario_p
		where nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;

commit;

end man_estornar_inicio_caso_teste;
/