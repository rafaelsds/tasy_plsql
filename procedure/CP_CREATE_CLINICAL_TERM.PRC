create or replace procedure cp_create_clinical_term (
	nr_seq_goal_p		cp_goal.nr_sequencia%type,
	nr_seq_care_plan_p	cp_clinical_term_assoc.nr_seq_care_plan%type,
	nr_seq_intervention_p	cp_intervention.nr_sequencia%type,
	cd_clinical_term_p	cp_clinical_term.cd_clinical_term%type,
	cd_clinical_term_type_p	cp_clinical_term.cd_clinical_term_type%type,
	nm_usuario_p		cp_clinical_term.nm_usuario%type
) is

nr_sequencia_w			cp_clinical_term.nr_sequencia%type;
si_type_of_term_w		cp_clinical_term.si_type_of_term%type;
nr_seq_goal_clin_term_w		cp_goal_clin_term.nr_sequencia%type;
nr_seq_interv_clin_term_w	cp_intervention_clin_term.nr_sequencia%type;
nr_seq_clin_term_assoc_w	cp_clinical_term_assoc.nr_sequencia%type;

begin

begin
	select	nr_sequencia
	into	nr_sequencia_w
	from	cp_clinical_term
	where	cd_clinical_term = cd_clinical_term_p
		and cd_clinical_term_type = cd_clinical_term_type_p;
exception
	when no_data_found then
		nr_sequencia_w	:= null;
end;

if (nr_sequencia_w is null) then
	select	cp_clinical_term_seq.nextval
	into	nr_sequencia_w
	from	dual;

	if (instr(cd_clinical_term_type_p, 'ICD-10') = 1) then
		si_type_of_term_w	:= 'CID10';
	elsif (instr(cd_clinical_term_type_p, 'ICD-9') = 1) then
		si_type_of_term_w	:= 'CID9';
	elsif (cd_clinical_term_type_p = 'NANDA') then
		si_type_of_term_w	:= 'NURSING';
	elsif (instr(cd_clinical_term_type_p, 'SNOMED') = 1) then
		si_type_of_term_w	:= 'SNOMED';
	elsif (cd_clinical_term_type_p = 'NIC') then
		si_type_of_term_w	:= 'INTERV';
	elsif (cd_clinical_term_type_p = 'NOC') then
		si_type_of_term_w	:= 'OUTCOME';
	end if;

	insert into cp_clinical_term (
		nr_sequencia,
		cd_clinical_term,
		cd_clinical_term_type,
		ie_origin,
		si_type_of_term,
		si_import_status,
		ie_situacao,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec
	) values (
		nr_sequencia_w,
		cd_clinical_term_p,
		cd_clinical_term_type_p,
		'E', -- From Elsevier
		si_type_of_term_w,
		'SP',
		'I',
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate
	);
end if;

-- Link with a goal if the code has been provided
if (nr_seq_goal_p is not null) then
	begin
		select	nr_sequencia
		into	nr_seq_goal_clin_term_w
		from	cp_goal_clin_term
		where	nr_seq_goal = nr_seq_goal_p
			and nr_seq_clinical_term = nr_sequencia_w;
	exception
		when no_data_found then
			nr_seq_goal_clin_term_w	:= null;
	end;

	if (nr_seq_goal_clin_term_w is null) then
		select	cp_goal_clin_term_seq.nextval
		into	nr_seq_goal_clin_term_w
		from	dual;

		insert into cp_goal_clin_term (
			nr_sequencia,
			nr_seq_goal,
			nr_seq_clinical_term,
			ie_origin,
			si_import_status,
			ie_situacao,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec
		) values (
			nr_seq_goal_clin_term_w,
			nr_seq_goal_p,
			nr_sequencia_w,
			'E', -- From Elsevier
			'SP',
			'I',
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate
		);
	end if;
end if;

if (nr_seq_intervention_p is not null) then
	begin
		select	nr_sequencia
		into	nr_seq_interv_clin_term_w
		from	cp_intervention_clin_term
		where	nr_seq_intervention = nr_seq_intervention_p
			and nr_seq_clinical_term = nr_sequencia_w;
	exception
		when no_data_found then
			nr_seq_interv_clin_term_w	:= null;
	end;

	if (nr_seq_interv_clin_term_w is null) then
		select	cp_intervention_clin_term_seq.nextval
		into	nr_seq_interv_clin_term_w
		from	dual;

		insert into cp_intervention_clin_term (
			nr_sequencia,
			nr_seq_intervention,
			nr_seq_clinical_term,
			ie_origin,
			si_import_status,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec
		) values (
			nr_seq_interv_clin_term_w,
			nr_seq_intervention_p,
			nr_sequencia_w,
			'E', -- From Elsevier
			'SP',
			'I',
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		);
	end if;
end if;

if (nr_seq_care_plan_p is not null) then
	begin
		select	nr_sequencia
		into	nr_seq_clin_term_assoc_w
		from	cp_clinical_term_assoc
		where	nr_seq_care_plan = nr_seq_care_plan_p
			and nr_seq_clin_term = nr_sequencia_w;
	exception
		when no_data_found then
			nr_seq_clin_term_assoc_w	:= null;
	end;

	if (nr_seq_clin_term_assoc_w is null) then
		select	cp_clinical_term_assoc_seq.nextval
		into	nr_seq_clin_term_assoc_w
		from	dual;

		insert into cp_clinical_term_assoc (
			nr_sequencia,
			nr_seq_care_plan,
			nr_seq_clin_term,
			ie_origin,
			si_import_status,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec
		) values (
			nr_seq_clin_term_assoc_w,
			nr_seq_care_plan_p,
			nr_sequencia_w,
			'E', -- From Elsevier
			'SP',
			'I',
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		);
	end if;
end if;

end cp_create_clinical_term;
/