CREATE OR REPLACE procedure qt_confirmar_transf_ageint(
			nr_seq_ageint_p	number,
			nm_usuario_p		Varchar2) is

nr_seq_atendimento_w	number(10);
nr_seq_paciente_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
dt_agenda_w		date;
nr_seq_local_w		number(10);
nr_duracao_w		number(10);
nr_seq_marcacao_w	number(10);
nr_seq_prof_w		number(10);
ie_tipo_agendamento_w	varchar2(15);
qt_gerado_w		number(10);
nr_seq_ageint_item_w	number(10);
nr_seq_item_marc_w	number(10);
nr_seq_mot_transf_w	number(10);

Cursor C01 is
	select	a.nr_sequencia,
		a.dt_agenda,
		a.nr_seq_local,
		a.nr_duracao,
		a.nr_seq_prof,
		a.ie_tipo_agendamento,
		c.cd_pessoa_fisica,
		b.nr_sequencia,
		a.nr_seq_ageint_item
	from	agenda_quimio_marcacao a,
		agenda_integrada_item b,
		agenda_integrada c
	where	c.nr_sequencia		= nr_seq_ageint_p
	and	b.nr_sequencia		= a.nr_seq_ageint_item
	and	b.nr_seq_agenda_int	= c.nr_sequencia
	and	a.ie_gerado		= 'N'
	and	a.ie_transferencia	= 'S';

begin

select	max(nr_sequencia)
into	nr_seq_mot_transf_w
from	qt_motivo_reagendamento;

open C01;
loop
fetch C01 into
	nr_seq_marcacao_w,
	dt_agenda_w,
	nr_seq_local_w,
	nr_duracao_w,
	nr_seq_prof_w,
	ie_tipo_agendamento_w,
	cd_pessoa_fisica_w,
	nr_seq_ageint_item_w,
	nr_seq_item_marc_w;
exit when C01%notfound;
	begin
	/* excluir marcacao anterior */
	delete	agenda_quimio_marcacao
	where	nr_seq_ageint_item	= nr_seq_ageint_item_w
	and	nr_seq_ageint_item	= nr_seq_item_marc_w
	and	ie_gerado		= 'S'
	and	ie_transferencia	= 'N';
	/* update novo horario */

	select	count(*)
	into	qt_gerado_w
	from	agenda_quimio
	where	nr_seq_ageint_item	= nr_seq_ageint_item_w;

	if	(qt_gerado_w	> 0) then
		update	agenda_quimio
		set	dt_agenda		= dt_agenda_w,
			nr_seq_local		= nr_seq_local_w,
			nr_seq_prof		= nr_seq_prof_w,
			nm_usuario		= nm_usuario_p,
			dt_Atualizacao		= sysdate,
			nr_seq_mot_reagendamento	= nr_seq_mot_transf_w
		where	nr_seq_ageint_item	= nr_seq_ageint_item_w;
	else
		insert into agenda_quimio
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			nr_seq_prof,
			nr_seq_local,
			nr_minuto_duracao,
			dt_agenda,
			ie_tipo_pend_agenda,
			ie_status_agenda,
			ie_tipo_agendamento,
			nr_seq_ageint_item,
			nr_seq_mot_reagendamento)
		values
			(agenda_quimio_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_w,
			nr_seq_prof_w,
			nr_seq_local_w,
			nr_duracao_w,
			dt_agenda_w,
			'Q',
			'N',
			ie_tipo_agendamento_w,
			nr_seq_ageint_item_w,
			nr_seq_mot_transf_w);		
	end if;
	
	update	agenda_integrada_item
	set	dt_prevista_quimio = nvl(dt_prev_transf_quimio,dt_prevista_quimio),
		dt_prev_transf_quimio = null
	where	nr_sequencia = nr_seq_ageint_item_w;

	/* alterar marcacao horario transferencia = 'N' */
	update	agenda_quimio_marcacao
	set	ie_gerado		= 'S',
		ie_transferencia	= 'N',
		nr_seq_prof		= nr_seq_prof_w,
		nr_seq_local		= nr_seq_local_w
	where	nr_sequencia		= nr_seq_marcacao_w;
	end;
end loop;
close C01;

commit;

end Qt_Confirmar_Transf_Ageint;
/