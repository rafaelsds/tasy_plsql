create or replace procedure tiss_ins_ret_denial_load_items(
						nr_seq_guia_p in convenio_retorno_item.nr_sequencia%type
						) is

begin

	tiss_ins_ret_denial_pck.load_items(nr_seq_guia_p);

end tiss_ins_ret_denial_load_items;
/
