create or replace
procedure gravar_log_retorno_convenio (	nr_seq_retorno_p		number,
					nm_usuario_p		varchar2,
					ie_tipo_log_p		varchar2,
					ds_log_p			varchar2) is

begin

insert into convenio_retorno_log
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,		
		nr_seq_retorno,
		ie_status_retorno,
		ds_log)
	values	(convenio_retorno_log_seq.nextval,
		sysdate,
		nm_usuario_p,		
		nr_seq_retorno_p,
		ie_tipo_log_p,
		ds_log_p);

commit;

end gravar_log_retorno_convenio;
/