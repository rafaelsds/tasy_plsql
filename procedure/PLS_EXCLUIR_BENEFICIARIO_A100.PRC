create or replace
procedure pls_excluir_beneficiario_a100
			(	nr_seq_benef_p		ptu_intercambio_benef.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type) is

qt_registros_w		number;
nr_seq_empresa_w	ptu_intercambio_empresa.nr_sequencia%type;

begin

select	max(nr_seq_empresa)
into	nr_seq_empresa_w
from	ptu_intercambio_benef
where	nr_sequencia	= nr_seq_benef_p;

delete from ptu_beneficiario_carencia where nr_seq_beneficiario = nr_seq_benef_p;
delete from ptu_beneficiario_compl where  nr_seq_beneficiario = nr_seq_benef_p;
delete from ptu_benef_plano_agregado where  nr_seq_beneficiario = nr_seq_benef_p;
delete from ptu_benef_preexistencia where nr_seq_beneficiario = nr_seq_benef_p;
delete from ptu_intercambio_benef where nr_sequencia = nr_seq_benef_p;

select	count(1)
into	qt_registros_w
from	ptu_intercambio_benef a
where	nr_seq_empresa	= nr_seq_empresa_w;

if	(qt_registros_w = 0) then
	delete from ptu_intercambio_plano
	where	nr_seq_empresa =  nr_seq_empresa_w;

	delete from ptu_intercambio_empresa where nr_sequencia = nr_seq_empresa_w;
end if;

commit;

end pls_excluir_beneficiario_a100;
/