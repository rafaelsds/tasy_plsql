create or replace
procedure pls_gerar_solic_rescisao_atend
			(	nr_seq_atendimento_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_solicitacao_w		pls_solicitacao_rescisao.nr_sequencia%type;
nr_contrato_w			pls_solicitacao_rescisao.nr_contrato%type;
nr_seq_contrato_w		pls_solicitacao_rescisao.nr_seq_contrato%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
ie_tipo_pessoa_w		pls_atendimento.ie_tipo_pessoa%type;
ie_gerar_w			varchar2(1);

begin

ie_gerar_w	:= 'N';

select	max(ie_tipo_pessoa)
into	ie_tipo_pessoa_w
from	pls_atendimento
where	nr_sequencia = nr_seq_atendimento_p;

if	(ie_tipo_pessoa_w in ('B','E','PG')) then
	if	(ie_tipo_pessoa_w = 'B') then
		select	max(c.nr_sequencia),
			max(c.nr_contrato),
			max(b.cd_pessoa_fisica)
		into	nr_seq_contrato_w,
			nr_contrato_w,
			cd_pessoa_fisica_w
		from	pls_atendimento a,
			pls_segurado b,
			pls_contrato c
		where	b.nr_sequencia	= a.nr_seq_segurado
		and	c.nr_sequencia	= b.nr_seq_contrato
		and	a.nr_sequencia	= nr_seq_atendimento_p;
	elsif	(ie_tipo_pessoa_w = 'E') then
		select	max(b.nr_sequencia),
			max(b.nr_contrato)
		into	nr_seq_contrato_w,
			nr_contrato_w
		from	pls_atendimento a,
			pls_contrato b
		where	b.nr_sequencia = a.nr_seq_contrato
		and	a.nr_sequencia = nr_seq_atendimento_p;
	elsif	(ie_tipo_pessoa_w = 'PG') then
		select	max(c.nr_sequencia),
			max(c.nr_contrato),
			max(b.cd_pessoa_fisica)
		into	nr_seq_contrato_w,
			nr_contrato_w,
			cd_pessoa_fisica_w
		from	pls_atendimento a,
			pls_contrato_pagador b,
			pls_contrato c
		where	b.nr_sequencia = a.nr_seq_pagador
		and	c.nr_sequencia = b.nr_seq_contrato
		and	a.nr_sequencia = nr_seq_atendimento_p;
	end if;
	
	ie_gerar_w := 'S';
end if;

if	(ie_gerar_w = 'S') then
	select	pls_solicitacao_rescisao_seq.nextval
	into	nr_seq_solicitacao_w
	from	dual;
	
	insert	into pls_solicitacao_rescisao
		(nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		dt_solicitacao,
		ie_status,
		ie_origem_solicitacao,
		nr_contrato,
		nr_seq_contrato,
		nr_seq_atendimento,
		ie_rescindir_contrato)
	values	(nr_seq_solicitacao_w,
		cd_estabelecimento_p,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		1,
		'C',
		nr_contrato_w,
		nr_seq_contrato_w,
		nr_seq_atendimento_p,
		'P');
	
	if	(cd_pessoa_fisica_w is not null) then
		pls_inserir_contato_solic_resc(nr_seq_solicitacao_w,cd_pessoa_fisica_w,'N',nm_usuario_p);
	end if;
end if;
commit;

end pls_gerar_solic_rescisao_atend;
/