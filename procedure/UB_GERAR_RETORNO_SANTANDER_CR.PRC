create or replace
procedure UB_GERAR_RETORNO_SANTANDER_CR(	nr_seq_cobr_escrit_p	number,
						nm_usuario_p		varchar2) is

nr_seq_reg_T_w		number(10);
nr_seq_reg_U_w		number(10);
nr_titulo_w		number(10);
vl_titulo_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_abatimento_w		number(15,2);
vl_liquido_w		number(15,2);
vl_outras_despesas_w	number(15,2);
dt_liquidacao_w		date;
ds_dt_liquidacao_w	varchar2(8);
ds_titulo_w		varchar2(15);
vl_cobranca_w		number(15,2);
vl_alterar_w		number(15,2);
cd_ocorrencia_w		varchar2(5);
nr_seq_ocorrencia_ret_w	number(10);
ds_nosso_numero_w	varchar2(12);
ie_digito_nosso_w	varchar2(1);

nr_ds_titulo_w		number(15);
nr_nosso_numero_w	varchar(20);
nr_seq_ocorr_motivo_w	number(10);
cd_motivo_w		varchar2(20);
vl_saldo_titulo_w	number(15,2);
ds_dt_credito_banco_w	varchar2(8);
dt_credito_banco_w	date;
cd_estab_ativo_w	number(4);
ie_permite_estab_w	varchar2(1);
cd_ocorrencia_ret_w	varchar2(2);
vl_juros_w			titulo_receber_cobr.vl_juros%type;
vl_multa_w			titulo_receber_cobr.vl_multa%type;
dt_vencimento_w		titulo_receber.dt_vencimento%type;
vl_saldo_multa_w	number(15,2);
dt_liquidacao_tit_w	titulo_receber.dt_liquidacao%type;

i number(10);


cursor c01 is
select	nr_sequencia,
	trim(substr(ds_string,55,15)) ds_titulo,
	lpad(trim(substr(ds_string,41,12)),12,'0') ds_nosso_numero,
	to_number(substr(ds_string,78,15))/100 vl_cobranca,
	substr(ds_string,209,10) cd_motivo,
	lpad(trim(substr(ds_string,53,1)),1,'0') ie_digito_nosso,
	substr(ds_string,16,2) cd_ocorrencia_ret -- Codigo da ocorrencia. Se for '02' Entrada confirmada, precisa atualizar o atributo IE_ENTRADA_CONFIRMADA no TITULO_RECEBER para 'C'.
from	w_retorno_banco
where	nr_seq_cobr_escrit		= nr_seq_cobr_escrit_p
and	substr(ds_string,8,1)		= '3'
and	substr(ds_string,14,1)	= 'T';

begin

delete	from cobranca_escrit_log
where	nr_seq_cobranca		= nr_seq_cobr_escrit_p;

select	max(trim(substr(ds_string,146,8)))
into	ds_dt_credito_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit		= nr_seq_cobr_escrit_p
and	substr(ds_string,8,1)		= '3'
and	substr(ds_string,14,1)	= 'U'
and	substr(ds_string,146,8) 	<> '00000000';

cd_estab_ativo_w := obter_estabelecimento_ativo;
obter_param_usuario(815,47,obter_perfil_ativo,nm_usuario_p,cd_estab_ativo_w,ie_permite_estab_w);

begin

	dt_credito_banco_w	:= to_date(ds_dt_credito_banco_w, 'dd/mm/yyyy');

exception
when others then

	dt_credito_banco_w	:= null;

end;

update	cobranca_escritural
set	dt_credito_bancario		= dt_credito_banco_w
where	nr_sequencia		= nr_seq_cobr_escrit_p;

open C01;
loop
fetch C01 into
	nr_seq_reg_T_w,
	ds_titulo_w,
	ds_nosso_numero_w,
	vl_cobranca_w,
	cd_motivo_w,
	ie_digito_nosso_w,
	cd_ocorrencia_ret_w;
exit when C01%notfound;
	begin

	vl_alterar_w		:= 0;
	vl_multa_w			:= 0;
	vl_juros_w			:= 0;	
	vl_saldo_multa_w	:= 0;

	nr_ds_titulo_w		:= somente_numero(ds_titulo_w);

	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo	= nr_ds_titulo_w
	and	((ie_permite_estab_w = 'S') or (cd_estabelecimento = cd_estab_ativo_w or cd_estab_financeiro = cd_estab_ativo_w));

	if	(nr_titulo_w	is null) then

		nr_nosso_numero_w	:= to_number(ds_nosso_numero_w);

		select	max(a.nr_titulo)
		into	nr_titulo_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_nosso_numero_w
		and	((ie_permite_estab_w = 'S') or (a.cd_estabelecimento = cd_estab_ativo_w or a.cd_estab_financeiro = cd_estab_ativo_w));

		if	(nr_titulo_w	is null) then

			select	max(a.nr_titulo)
			into	nr_titulo_w
			from	titulo_receber a
			where	ds_titulo_w like a.nr_titulo || '%'
			and	((ie_permite_estab_w = 'S') or (a.cd_estabelecimento = cd_estab_ativo_w or a.cd_estab_financeiro = cd_estab_ativo_w));

			if	(nr_titulo_w	is null) then

				select	max(a.nr_titulo)
				into	nr_titulo_w
				from	titulo_receber a
				where	lpad(a.nr_nosso_numero,12,'0')	= ds_nosso_numero_w || ie_digito_nosso_w
				and	nvl(a.nr_nosso_numero,'0')	<> '0'
				and	((ie_permite_estab_w = 'S') or (a.cd_estabelecimento = cd_estab_ativo_w or a.cd_estab_financeiro = cd_estab_ativo_w));

				if	(nr_titulo_w	is null) then

					nr_nosso_numero_w	:= somente_numero(ds_nosso_numero_w || ie_digito_nosso_w);

					select	max(a.nr_titulo)
					into	nr_titulo_w
					from	titulo_receber a
					where	a.nr_titulo	= nr_nosso_numero_w
					and	((ie_permite_estab_w = 'S') or (a.cd_estabelecimento = cd_estab_ativo_w or a.cd_estab_financeiro = cd_estab_ativo_w));

					if	(nr_titulo_w	is null) then

						select	max(a.nr_titulo)
						into	nr_titulo_w
						from	titulo_receber a
						where	somente_numero(a.nr_nosso_numero)	= nr_nosso_numero_w
						and	((ie_permite_estab_w = 'S') or (a.cd_estabelecimento = cd_estab_ativo_w or a.cd_estab_financeiro = cd_estab_ativo_w));

					end if;

				end if;

			end if;

		end if;

	end if;

	/* Se encontrou o t�tulo importa, sen�o grava no log */
	if	(nr_titulo_w is not null) then

		select	max(vl_titulo),
			max(vl_saldo_titulo),
			max(dt_vencimento),
			max(dt_liquidacao)
		into	vl_titulo_w,
			vl_saldo_titulo_w,
			dt_vencimento_w,
			dt_liquidacao_tit_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		nr_seq_reg_U_w := nr_seq_reg_T_w + 1;
		
		select	nvl(to_number(substr(ds_string,18,15))/100,0),
			nvl(to_number(substr(ds_string,33,15))/100,0),
			nvl(to_number(substr(ds_string,48,15))/100,0),
			nvl(to_number(substr(ds_string,93,15))/100,0),
			nvl(to_number(substr(ds_string,108,15))/100,0),
			decode(dt_credito_banco_w,null,substr(ds_string,146,8),substr(ds_string,138,8)),
			substr(ds_string,16,2)
		into	vl_acrescimo_w,
			vl_desconto_w,
			vl_abatimento_w,
			vl_liquido_w,
			vl_outras_despesas_w,
			ds_dt_liquidacao_w,
			cd_ocorrencia_w
		from	w_retorno_banco
		where	nr_sequencia	= nr_seq_reg_U_w;

		begin

			dt_liquidacao_w	:= to_date(ds_dt_liquidacao_w,'dd/mm/yyyy');

		exception
		when others then

			dt_liquidacao_w	:= sysdate;

		end;

		select 	max(a.nr_sequencia)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco	= 33
		and	a.cd_ocorrencia	= to_char(cd_ocorrencia_w);

		select	max(a.nr_sequencia)
		into	nr_seq_ocorr_motivo_w
		from	banco_ocorr_motivo_ret a
		where	instr(cd_motivo_w,a.cd_motivo)	> 0
		and	a.nr_seq_escrit_ret			= nr_seq_ocorrencia_ret_w;

		/*OS 1528137 Tratar Juros e multa que vem no campo Acr�scimos*/
		if (nvl(vl_saldo_titulo_w,0) <> nvl(vl_liquido_w,0)) and (vl_acrescimo_w <> 0) and (dt_liquidacao_tit_w is null) then /*Se o titulo ja estiver liquidado, caso depagamento em duplicidade, deve manter o valor como esta, e gerar a NC*/

			if (to_Date(dt_liquidacao_w,'dd/mm/yyyy') > to_date(dt_vencimento_w,'dd/mm/yyyy')) then

				vl_alterar_w	:= vl_liquido_w - vl_saldo_titulo_w;
				
				if	(vl_alterar_w > 0) then
				
					vl_saldo_multa_w	:= obter_juros_multa_titulo(nr_titulo_w,to_Date(dt_liquidacao_w,'dd/mm/RRRR'),'R','M');
					vl_juros_w 			:= nvl(vl_alterar_w,0) - nvl(vl_saldo_multa_w,0);
					vl_multa_w 			:= nvl(vl_alterar_w,0) - nvl(vl_juros_w,0);
					vl_acrescimo_w		:= nvl(vl_alterar_w,0) - (nvl(vl_juros_w,0)  + nvl(vl_multa_w,0));
						
				end if;
			
		    end if;
		
		end if;
		
		insert	into titulo_receber_cobr (	NR_SEQUENCIA,
							NR_TITULO,
							CD_BANCO,
							VL_COBRANCA,
							VL_DESCONTO,
							VL_ACRESCIMO,
							VL_DESPESA_BANCARIA,
							VL_LIQUIDACAO,
							DT_LIQUIDACAO,
							DT_ATUALIZACAO,
							NM_USUARIO,
							NR_SEQ_COBRANCA,
							nr_seq_ocorrencia_ret,
							nr_seq_ocorr_motivo,
							vl_saldo_inclusao,
							vl_juros,
							vl_multa)
					values	(	titulo_receber_cobr_seq.nextval,
							nr_titulo_w,
							033,
							vl_titulo_w,
							vl_desconto_w,
							vl_acrescimo_w,
							vl_outras_despesas_w,
							vl_liquido_w,
							dt_liquidacao_w,
							sysdate,
							nm_usuario_p,
							nr_seq_cobr_escrit_p,
							nr_seq_ocorrencia_ret_w,
							nr_seq_ocorr_motivo_w,
							vl_saldo_titulo_w,
							vl_juros_w,
							vl_multa_w);
		
		/*Segundo o layout, caso o C�digo de Movimento para o retorno seja 02 se trata de Entrada Confirmada. Nesses casos, atualizar o titulo_receber*/	
		if (cd_ocorrencia_ret_w = '02') then
			update	titulo_receber a
			set		a.ie_entrada_confirmada = 'C'
			where	a.nr_titulo = nr_titulo_w;
		end if;

	else

		insert	into cobranca_escrit_log
			(ds_log,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_nosso_numero,
			nr_seq_cobranca,
			nr_sequencia)
		values	('N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy',
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			ds_nosso_numero_w,
			nr_seq_cobr_escrit_p,
			cobranca_escrit_log_seq.nextval);

	end if;

	end;
end loop;
close C01;

commit;

end UB_GERAR_RETORNO_SANTANDER_CR;
/