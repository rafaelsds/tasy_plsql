create or replace procedure duv_gerar_segmento_bes(nr_seq_mensagem_p in duv_mensagem.nr_sequencia%type,
                                                   nm_usuario_p      in usuario.nm_usuario%type,
                                                   nr_seq_episodio_p in episodio_paciente.nr_sequencia%type)is
                                              
  cursor c01 is
   select null ie_admissao_reclamacao, 
          apq.dt_atualizacao dt_reclamacao, 
          substr(qp.ds_queixa,1,1000) ds_reclamacao , 
          ap.nr_seq_episodio,
          null ie_lado_machucado
   from  atendimento_paciente     ap,
         atend_paciente_queixa    apq,
         queixa_paciente          qp ,
         episodio_paciente        ep
   where  ap.nr_seq_episodio = ep.nr_sequencia
   and ap.nr_seq_episodio    = nr_seq_episodio_p
   and ap.nr_atendimento     = apq.nr_atendimento 
   and qp.nr_sequencia       = apq.nr_seq_queixa; 

  c01_w c01%rowtype;                                                   
                                                 
begin 
  c01_w := null;
  open c01;
  fetch c01 into c01_w;
  close c01;
    insert into duv_bes (nr_sequencia, 
                         dt_atualizacao, 
                         nm_usuario, 
                         dt_atualizacao_nrec, 
                         nm_usuario_nrec, 
                         nr_seq_mensagem, 
                         ie_admissao_reclamacao, 
                         dt_reclamacao, 
                         ds_reclamacao, 
                         ie_lado_machucado) values (duv_bes_seq.nextval,
                                                    sysdate,
                                                    nm_usuario_p,
                                                    sysdate,
                                                    nm_usuario_p,
                                                    nr_seq_mensagem_p,
                                                    c01_w.ie_admissao_reclamacao, 
                                                    c01_w.dt_reclamacao, 
                                                    c01_w.ds_reclamacao, 
                                                    c01_w.ie_lado_machucado);

end duv_gerar_segmento_bes;
/
