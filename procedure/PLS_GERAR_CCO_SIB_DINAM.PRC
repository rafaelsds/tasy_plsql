create or replace
procedure pls_gerar_cco_sib_dinam
			(	nr_seq_lote_sib_p		Number,
				ie_gravar_cartao_sist_ant_p	Varchar2,
				cd_estabelecimento_p		Number,
				nm_usuario_p			Varchar2) is

nr_cco_w			number(10);
ie_digito_cco_w			number(2);
nr_seq_segurado_w		number(10);
nr_seq_retorno_sib_w		number(10);
ie_tipo_reg_w			number(2);
nm_beneficiario_w		varchar2(70);
cd_cco_w			pls_segurado.cd_cco%type;

Cursor C01 is
	select	1 ie_tipo_reg,
		a.nr_sequencia nr_seq_retorno,
		a.nr_cco,
		a.ie_digito_cco,
		c.nr_sequencia nr_seq_segurado,
		nm_beneficiario
	from	pls_retorno_sib a,
		pls_segurado_carteira b,
		pls_segurado c,
		pls_lote_retorno_sib	d
	where	b.nr_seq_segurado	= c.nr_sequencia
	and	a.nr_seq_lote_sib	= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_sib_p
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_carteira_pesquisa	= b.cd_usuario_plano
	and	c.nr_cco is null
	and	ie_gravar_cartao_sist_ant_p	= 'N'
	UNION ALL
	select	3 ie_tipo_reg,
		a.nr_sequencia nr_seq_retorno,
		a.nr_cco,
		a.ie_digito_cco,
		c.nr_sequencia nr_seq_segurado,
		nm_beneficiario
	from	pls_retorno_sib a,
		pls_segurado_carteira b,
		pls_segurado c,
		pls_lote_retorno_sib	d
	where	b.nr_seq_segurado	= c.nr_sequencia
	and	a.nr_seq_lote_sib	= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_sib_p
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_carteira_pesquisa	= b.cd_usuario_plano
	and	c.nr_cco is not null
	and	ie_gravar_cartao_sist_ant_p	= 'N'
	UNION ALL
	select	1 ie_tipo_reg,
		a.nr_sequencia nr_seq_retorno,
		a.nr_cco,
		a.ie_digito_cco,
		c.nr_sequencia nr_seq_segurado,
		nm_beneficiario
	from	pls_retorno_sib a,
		pls_segurado_carteira b,
		pls_segurado c,
		pls_lote_retorno_sib	d
	where	b.nr_seq_segurado	= c.nr_sequencia
	and	a.nr_seq_lote_sib	= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_sib_p
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	c.cd_cartao_ident_ans_sist_ant	= a.cd_usuario_plano
	and	c.nr_cco is null
	and	ie_gravar_cartao_sist_ant_p	= 'S'
	UNION ALL
	select	3 ie_tipo_reg,
		a.nr_sequencia nr_seq_retorno,
		a.nr_cco,
		a.ie_digito_cco,
		c.nr_sequencia nr_seq_segurado,
		nm_beneficiario
	from	pls_retorno_sib a,
		pls_segurado_carteira b,
		pls_segurado c,
		pls_lote_retorno_sib	d
	where	b.nr_seq_segurado	= c.nr_sequencia
	and	a.nr_seq_lote_sib	= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_sib_p
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	c.cd_cartao_ident_ans_sist_ant	= a.cd_usuario_plano
	and	c.nr_cco is not null
	and	ie_gravar_cartao_sist_ant_p	= 'S';

TYPE 		fetch_array IS TABLE OF c01%ROWTYPE;
s_array 	fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_c01_w	Vetor;

begin

gravar_processo_longo('Comparando as carteiras dos beneficiários' ,'PLS_GERAR_CCO_SIB_DINAM',0);

open c01;
loop
FETCH C01 BULK COLLECT INTO s_array LIMIT 1000;
	Vetor_c01_w(i) := s_array;
	i := i + 1;
EXIT WHEN C01%NOTFOUND;
END LOOP;
CLOSE C01;

for i in 1..Vetor_c01_w.COUNT loop
	s_array := Vetor_c01_w(i);
	for z in 1..s_array.COUNT loop
	ie_tipo_reg_w		:= s_array(z).ie_tipo_reg;
	nr_seq_retorno_sib_w	:= s_array(z).nr_seq_retorno;
	nr_cco_w		:= s_array(z).nr_cco;
	ie_digito_cco_w		:= s_array(z).ie_digito_cco;
	nr_seq_segurado_w	:= s_array(z).nr_seq_segurado;
	nm_beneficiario_w	:= s_array(z).nm_beneficiario;
	
	begin
	gravar_processo_longo(nm_beneficiario_w,'PLS_GERAR_CCO_SIB_DINAM',-1);
	
	if	(ie_tipo_reg_w = 3) then
		update	pls_retorno_sib
		set	nr_seq_segurado_encontrado	= nr_seq_segurado_w,
			dt_atualizacao			= sysdate,
			nm_usuario			= nm_usuario_p
		where	nr_sequencia			= nr_seq_retorno_sib_w;
	elsif	(nvl(nr_seq_segurado_w,0) > 0) then
		cd_cco_w	:= lpad(nr_cco_w,10,0)||lpad(ie_digito_cco_w,2,0);
		update	pls_segurado
		set	nr_cco		= nr_cco_w,
			ie_digito_cco	= ie_digito_cco_w,
			cd_cco		= cd_cco_w,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_segurado_w;
		
		update	pls_retorno_sib
		set	nr_seq_segurado	= nr_seq_segurado_w,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_retorno_sib_w;
	end if;
	
	end;
	end loop;
end loop;

commit;

end pls_gerar_cco_sib_dinam;
/