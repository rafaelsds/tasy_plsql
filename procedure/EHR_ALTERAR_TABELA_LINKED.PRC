CREATE OR REPLACE PROCEDURE ehr_alterar_tabela_linked (
  nr_seq_template_p    IN EHR_TEMPLATE_CONTEUDO.NR_SEQ_TEMPLATE%TYPE
, nr_seq_linked_data_p IN EHR_TEMPLATE_CONTEUDO.NR_SEQ_LINKED_DATA%TYPE
, ie_ddl_operation_p   IN VARCHAR2 -- CREATE, ALTER, DROP
, nm_atributo_p        IN TABELA_ATRIBUTO_LINKED.NM_ATRIBUTO%TYPE DEFAULT NULL
, ie_componente_p      IN TABELA_ATRIBUTO_LINKED.IE_COMPONENTE%TYPE DEFAULT NULL
, ie_tipo_atributo_p   IN TABELA_ATRIBUTO_LINKED.IE_TIPO_ATRIBUTO%TYPE DEFAULT NULL
) 
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  
  LOG_PREFIX_C              CONSTANT VARCHAR2(15) DEFAULT 'EHR_LINKED_DATA';
  DATA_TABLE_PREFIX_C       CONSTANT VARCHAR2(10) DEFAULT 'EHR_LINKED';
  DATA_TABLE_INDEX_PREFIX_C CONSTANT VARCHAR2(6)  DEFAULT 'EHRLKD';
    
  DML_CREATE_OPERATION      CONSTANT VARCHAR2(6)  DEFAULT 'CREATE';
  DML_ALTER_OPERATION       CONSTANT VARCHAR2(5)  DEFAULT 'ALTER';
  DML_DROP_OPERATION        CONSTANT VARCHAR2(4)  DEFAULT 'DROP';
  IE_DDL_OPERATION_C        CONSTANT VARCHAR2(30) DEFAULT UPPER(NVL(ie_ddl_operation_p, ''));
  
  nm_table_w                USER_TABLES.TABLE_NAME%TYPE;
  nm_index_w                USER_OBJECTS.OBJECT_NAME%TYPE;
  ds_column_type_w          USER_TAB_COLUMNS.DATA_TYPE%TYPE;
  ds_script_w               VARCHAR2(32737);
  
  PROCEDURE execute_script (
    ds_script_p IN VARCHAR2
  ) 
  IS
    CURSOR c_scripts IS
      SELECT regexp_substr(ds_script_p, '[^;]+', '1', LEVEL) data
        FROM dual
     CONNECT BY regexp_substr(ds_script_p, '[^;]+', 1, LEVEL) IS NOT NULL;           
  BEGIN
    IF ds_script_p IS NOT NULL THEN 
      FOR r_script IN c_scripts LOOP
        IF r_script.data IS NOT NULL THEN
          exec_sql_dinamico(LOG_PREFIX_C, r_script.data);
        END IF;
      END LOOP;
    END IF;
  END execute_script;

  FUNCTION table_exists (
    ds_table_name_p IN USER_TABLES.TABLE_NAME%TYPE
  ) RETURN BOOLEAN 
  IS
    ie_table_exist_w NUMBER(1);
    CURSOR c_table_exists IS
      SELECT COUNT(1)
        FROM user_tables
       WHERE table_name = ds_table_name_p;
  BEGIN
    OPEN c_table_exists;
      FETCH c_table_exists INTO ie_table_exist_w;
    CLOSE c_table_exists;
    
    RETURN ie_table_exist_w > 0;
  END table_exists;

  FUNCTION get_column_type (
    ie_componente_p    IN TABELA_ATRIBUTO_LINKED.IE_COMPONENTE%TYPE
  , ie_tipo_atributo_p IN TABELA_ATRIBUTO_LINKED.IE_TIPO_ATRIBUTO%TYPE 
  ) RETURN VARCHAR2 
  IS
    ds_column_type_w USER_TAB_COLUMNS.DATA_TYPE%TYPE;
    CURSOR c_column_type IS
      SELECT DISTINCT 
             CASE 
               WHEN etd.ds_openehr = 'DV_BOOLEAN' THEN 'VARCHAR2(1)'
               WHEN etd.ds_openehr = 'DV_TEXT'  OR etd.ds_openehr = 'DV_CODED_TEXT' THEN 'VARCHAR2(4000)'
               WHEN etd.ds_openehr = 'DV_COUNT' OR etd.ds_openehr = 'DV_QUANTITY' THEN 'NUMBER(10,2)'
               WHEN etd.ds_openehr = 'DV_DATE'  OR etd.ds_openehr = 'DV_TIME' OR etd.ds_openehr = 'DV_DATE_TIME' THEN 'DATE'
			         WHEN etd.ds_openehr = 'DV_LONG' THEN 'LONG'
             ELSE NULL END ds_column_type
        FROM ehr_elemento  ele
           , ehr_tipo_dado etd
       WHERE ele.nr_seq_tipo_dado     = etd.nr_sequencia
         AND LOWER(ele.ie_componente) = LOWER(ie_componente_p)
         /* Linked Data Generic Elements for discover the attribute data type.
         *    1618: Edit Linked Data
         *    1620: LookUp Linked Data
         *    1621: CheckBox Linked Data
         *    1622: Localizador Linked Data
         *    1623: RadioGroup Linked Data
         *    1624: RichEdit Linked Data
         *    1625: Memo Linked Data
         *    1631: DateTimePicker Linked Data
         *    1636: AutoComplete Linked Data
         */
         AND ele.nr_sequencia         IN (1618, 1620, 1621, 1622, 1623, 1624, 1625, 1631, 1636);
  BEGIN
    OPEN c_column_type;
      FETCH c_column_type INTO ds_column_type_w;
    CLOSE c_column_type;
    
    IF ds_column_type_w IS NULL THEN
      IF ie_tipo_atributo_p = 'TIMESTAMP' OR ie_tipo_atributo_p = 'DATE' THEN
        ds_column_type_w := 'DATE';
      ELSIF ie_tipo_atributo_p = 'NUMBER' THEN
        ds_column_type_w := 'NUMBER(10,2)';
      ELSIF ie_tipo_atributo_p = 'LONG' THEN
        ds_column_type_w := 'LONG';
      ELSE
        ds_column_type_w := 'VARCHAR2(4000)';
      END IF;
    ELSIF LOWER(ie_componente_p) IN ('de', 'ed') AND ie_tipo_atributo_p = 'DATE' THEN
      ds_column_type_w := 'DATE';
    END IF;
    
    RETURN ds_column_type_w;    
  END get_column_type;
     
BEGIN
  
  IF nr_seq_template_p IS NULL OR nr_seq_linked_data_p IS NULL THEN
    raise_application_error(-20001, 'The arguments "nr_seq_template_p" and "nr_seq_linked_data_p" are required.');
  END IF;
  
  IF IE_DDL_OPERATION_C IS NULL OR IE_DDL_OPERATION_C NOT IN (DML_CREATE_OPERATION, DML_ALTER_OPERATION, DML_DROP_OPERATION) THEN
    raise_application_error(-20002, 'The argument "ie_ddl_operation_p" must be a valid DDL operation. The allowed values are: ' 
                                    || DML_CREATE_OPERATION ||', '|| DML_ALTER_OPERATION ||' or '|| DML_DROP_OPERATION ||'.');
  END IF;
  
  IF IE_DDL_OPERATION_C = DML_ALTER_OPERATION AND (nm_atributo_p IS NULL OR ie_componente_p IS NULL OR ie_tipo_atributo_p IS NULL) THEN
    raise_application_error(-20003, 'The arguments "nm_atributo_p", "ie_componente_p" and "ie_tipo_atributo_p" are required for a ' || DML_ALTER_OPERATION || ' DML operation.');
  END IF;
  
  nm_table_w := DATA_TABLE_PREFIX_C || '_' || TO_CHAR(nr_seq_template_p) || '_' || TO_CHAR(nr_seq_linked_data_p);
  
  IF IE_DDL_OPERATION_C = DML_CREATE_OPERATION AND NOT table_exists(nm_table_w) THEN
    nm_index_w := DATA_TABLE_INDEX_PREFIX_C || SUBSTR(nm_table_w, 11);
      
    ds_script_w := ' CREATE TABLE '  || nm_table_w || ' ('||
                   '   NR_SEQUENCIA            NUMBER(10)           NOT NULL ' ||
                   ' , NR_SEQ_TEMPLATE         NUMBER(10)           NOT NULL ' ||
                   ' , NR_SEQ_LINKED_DATA      NUMBER(10)           NOT NULL ' ||
                   ' , NR_SEQ_REG_TEMPLATE     NUMBER(10)           NOT NULL ' ||
                   ' , NM_USUARIO              VARCHAR2(15)         NOT NULL ' ||
                   ' , DT_ATUALIZACAO          DATE DEFAULT SYSDATE NOT NULL ' ||
                   ' , NM_USUARIO_NREC         VARCHAR2(15)         NOT NULL ' ||
                   ' , DT_ATUALIZACAO_NREC     DATE DEFAULT SYSDATE NOT NULL ' ||
                   ' , CONSTRAINT ' || nm_index_w ||'_PK'||  ' PRIMARY KEY (NR_SEQUENCIA) );'; 
                                      
    ds_script_w := ds_script_w || 'CREATE SEQUENCE '|| nm_table_w || '_SEQ;';
      
    ds_script_w := ds_script_w || 
                   ' CREATE INDEX ' || nm_index_w || '_I ON '|| nm_table_w || '(' ||
                   '    NR_SEQ_TEMPLATE, NR_SEQ_LINKED_DATA, NR_SEQ_REG_TEMPLATE' ||
                   ' ) TABLESPACE TASY_INDEX;';
  ELSIF IE_DDL_OPERATION_C = DML_DROP_OPERATION THEN 
    ds_script_w := ' DROP TABLE '  || nm_table_w || ';';
    ds_script_w := ds_script_w || 'DROP SEQUENCE '|| nm_table_w || '_SEQ;';
  ELSE
    ds_column_type_w := get_column_type(ie_componente_p, ie_tipo_atributo_p);
    ds_script_w := ' ALTER TABLE ' || nm_table_w || ' ADD ' || nm_atributo_p || ' ' ||ds_column_type_w|| ';';
  END IF;
  
  execute_script(ds_script_w);

END ehr_alterar_tabela_linked;
/
