create or replace
procedure lic_desfazer_rodada(	nr_seq_licitacao_p		number,
				nr_seq_lic_item_p		number,
				nr_seq_lance_p		number,
				nm_usuario_p		Varchar2) is 

nr_seq_fornec_w			number(10);
nr_seq_lance_w			number(10);
vl_item_w				number(15,4);
nr_seq_lance_anterior_w		number(10);
nr_seq_lic_item_fornec_w		number(10);
ie_qualificado_w			varchar2(1);
				
cursor c01 is
select	nr_seq_fornec
from	reg_lic_fornec_lance
where	nr_seq_licitacao	= nr_seq_licitacao_p
and	nr_seq_lic_item	= nr_seq_lic_item_p
and	nr_seq_lance	= nr_seq_lance_p;
				
begin

/*select para verificar o ultimo lance.*/
select	max(nr_seq_lance)
into	nr_seq_lance_w
from	reg_lic_fornec_lance
where	nr_seq_licitacao	= nr_seq_licitacao_p
and	nr_seq_lic_item	= nr_seq_lic_item_p;

if	(nr_seq_lance_w <> nr_seq_lance_p) then
	wheb_mensagem_pck.exibir_mensagem_abort(266182,'NR_SEQ_LANCE=' || nr_seq_lance_p);
	--'N�o � permitido desfazer a ' || nr_seq_lance_p || '� rodada porque existem rodadas posteriores a ela. ' ||
	--'S� � permitido desfazer a rodada se   ela for a �ltima.');
end if;

open C01;
loop
fetch C01 into	
	nr_seq_fornec_w;
exit when C01%notfound;
	begin
	
	nr_seq_lance_anterior_w := nr_seq_lance_p - 1;
	
	select	nvl(max(nr_seq_lic_item_fornec),0)
	into	nr_seq_lic_item_fornec_w
	from	reg_lic_fornec_lance
	where	nr_seq_lance		= nr_seq_lance_p
	and	nr_seq_lic_item		= nr_seq_lic_item_p
	and	nr_seq_licitacao		= nr_seq_licitacao_p
	and	nr_seq_fornec		= nr_seq_fornec_w;
	
	
	select	nvl(max(ie_qualificado),'S')
	into	ie_qualificado_w
	from	reg_lic_fornec_lance
	where	nr_seq_lance		= nr_seq_lance_p
	and	nr_seq_lic_item		= nr_seq_lic_item_p
	and	nr_seq_licitacao		= nr_seq_licitacao_p
	and	nr_seq_fornec		= nr_seq_fornec_w;
	
	if	(ie_qualificado_w = 'N') and
		(nr_seq_lic_item_fornec_w > 0) then
		lic_ativa_item_fornec(nr_seq_lic_item_fornec_w, nm_usuario_p);
	end if;
	
	if	(nr_seq_lance_anterior_w > 0) then
		/*select para obter o menor pre�o do lance anterior para atualizar o pre�o final na tabela REG_LIC_ITEM_FORNEC*/
		select	nvl(min(vl_item),0)
		into	vl_item_w
		from	reg_lic_fornec_lance
		where	nr_seq_licitacao	= nr_seq_licitacao_p
		and	nr_seq_lic_item	= nr_seq_lic_item_p
		and	nr_seq_lance	= nr_seq_lance_anterior_w;
		
		/*Para obter o pre�o que o fornecedor lan�ou na rodada anterior, que ser� utilizado para atualizar o pre�o final na tabela REG_LIC_ITEM_FORNEC*/
		select	nvl(min(vl_item),0)
		into	vl_item_w
		from	reg_lic_fornec_lance
		where	nr_seq_licitacao	= nr_seq_licitacao_p
		and	nr_seq_lic_item	= nr_seq_lic_item_p
		and	nr_seq_lance	= nr_seq_lance_anterior_w
		and	nr_seq_fornec	= nr_seq_fornec_w;
		
		/*Atualizar o pre�o final na tabela  REG_LIC_ITEM_FORNEC*/
		update	reg_lic_item_fornec
		set	vl_item		= vl_item_w
		where	nr_seq_licitacao	= nr_seq_licitacao_p
		and	nr_seq_lic_item	= nr_seq_lic_item_p
		and	nr_seq_fornec	= nr_seq_fornec_w;
	else
		/*select para obter o a proposta inicial quando n�o tem mais rodada anterior*/
		select	nvl(min(vl_original),0)
		into	vl_item_w
		from	reg_lic_item_fornec
		where	nr_seq_licitacao	= nr_seq_licitacao_p
		and	nr_seq_lic_item	= nr_seq_lic_item_p;
		
		/*Atualizar o pre�o final na tabela  REG_LIC_ITEM_FORNEC*/
		update	reg_lic_item_fornec
		set	vl_item		= 0
		where	nr_seq_licitacao	= nr_seq_licitacao_p
		and	nr_seq_lic_item	= nr_seq_lic_item_p
		and	nr_seq_fornec	= nr_seq_fornec_w;
	end if;
	
	lic_calcular_licitacao(nr_seq_licitacao_p, 'N', nm_usuario_p);
	
	/*Deleta todos os lances da rodada.*/
	delete from reg_lic_fornec_lance
	where	nr_seq_fornec		= nr_seq_fornec_w
	and	nr_seq_lic_item		= nr_seq_lic_item_p
	and	nr_seq_lance		= nr_seq_lance_p;
	
	if	(nr_seq_lance_anterior_w > 0) then
	
		update	reg_lic_fornec_lance
		set	dt_fechamento_rodada	= '',
			nm_usuario_fechamento	= ''
		where	nr_seq_licitacao		= nr_seq_licitacao_p
		and	nr_seq_lic_item		= nr_seq_lic_item_p
		and	nr_seq_lance		= nr_seq_lance_anterior_w;
	end if;			
	end;
end loop;
close C01;

insert into reg_lic_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_historico,
	ds_observacao,
	nr_seq_licitacao)
values(	reg_lic_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	'DR',
	WHEB_MENSAGEM_PCK.get_texto(312583, 'NR_SEQ_LANCE_P=' || nr_seq_lance_p || ';NR_SEQ_LIC_ITEM_P=' || nr_seq_lic_item_p), --'A rodada ' || nr_seq_lance_p || ' do item ' || nr_seq_lic_item_p || ' foi desfeita (atrav�s da op��o Desfazer rodada).'
	nr_seq_licitacao_p);

lic_calcular_licitacao(	nr_seq_licitacao_p, 'N', nm_usuario_p);

commit;

end lic_desfazer_rodada;
/
