create or replace
procedure pls_gerar_lote_camara_comp(	nr_seq_lote_fatura_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is
					
nr_fatura_w		number(10);
nr_seq_lote_camara_w	number(10);
dt_lote_w		date;
nr_seq_camara_w		number(10);
nr_seq_periodo_w	number(10);
tx_administrativa_w	number(7,4);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
vl_desc_benefic_custo_w	number(15,4);


Cursor c01 is
select	a.nr_fatura
from	ptu_fatura a
where	a.nr_seq_lote	= nr_seq_lote_fatura_p;					

begin

if	(nr_seq_lote_fatura_p is not null) then
	select	a.dt_geracao_lote,
		a.nr_seq_camara,
		a.nr_seq_periodo,
		a.dt_inicio,
		a.dt_fim
	into	dt_lote_w,
		nr_seq_camara_w,
		nr_seq_periodo_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w
	from	ptu_lote_fatura_envio a
	where	a.nr_sequencia	= nr_seq_lote_fatura_p;
	
	if	(nr_seq_camara_w is null) or
		(nr_seq_periodo_w is null) then
		-- N�o � poss�vel gerar c�mara, n�o foi informado a c�mara ou o lote.
		wheb_mensagem_pck.exibir_mensagem_abort(266796);
	end if;
	
	select	nvl(max(a.tx_administrativa),0),
		nvl(max(vl_desc_benefic_custo),0)
	into	tx_administrativa_w,
		vl_desc_benefic_custo_w
	from	pls_camara_compensacao a
	where	a.nr_sequencia	= nr_seq_camara_w;

	select	pls_lote_camara_comp_seq.nextval
	into	nr_seq_lote_camara_w
	from	dual;

	insert into pls_lote_camara_comp
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_lote,
		nr_seq_camara,
		nr_seq_periodo,
		dt_periodo_inicial,
		dt_periodo_final,
		ie_tipo_data_cr,
		ie_tipo_data_cp,
		cd_estabelecimento,
		dt_geracao,
		tx_administrativa,
		vl_desc_benefic_custo)
	values	(nr_seq_lote_camara_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		dt_lote_w,
		nr_seq_camara_w,
		nr_seq_periodo_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		'E',
		'V',
		cd_estabelecimento_p,
		sysdate,
		tx_administrativa_w,
		vl_desc_benefic_custo_w);

	open c01;
	loop
	fetch c01 into	
		nr_fatura_w;
	exit when c01%notfound;
		begin
		insert into pls_titulo_lote_camara
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_lote_camara,
			nr_titulo_receber,
			ie_tipo_inclusao,
			vl_baixado)
		values	(pls_titulo_lote_camara_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_lote_camara_w,
			nr_fatura_w,
			'A',
			0);
		end;
	end loop;
	close c01;
	
	update	ptu_lote_fatura_envio
	set	nr_seq_lote_camara	= nr_seq_lote_camara_w,
		dt_integracao_camara	= sysdate
	where	nr_sequencia		= nr_seq_lote_fatura_p;

	commit;
end if;

end pls_gerar_lote_camara_comp;
/