create or replace
procedure apap_extendido_exbir_grafico(	nr_seq_informacao_p	varchar2) is 

begin


update	w_apap_pac_informacao
set	ie_grafico = decode(ie_grafico,'S','N','S')
where	nr_sequencia	= nr_seq_informacao_p;

commit;

end apap_extendido_exbir_grafico;
/