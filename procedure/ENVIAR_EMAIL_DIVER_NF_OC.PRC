create or replace procedure enviar_email_diver_nf_oc(
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	Number,
			nr_sequencia_p		Number) is 

qt_existe_regra_w			number(10);
nr_ordem_compra_w		number(10);
nr_item_oci_w			number(10);
vl_unitario_oc_w			number(17,4);
qt_material_oc_w			number(17,4);
qt_item_nf_w			number(17,4);
dt_entrega_w			date;
dt_emissao_w			date;
nr_nota_fiscal_w			varchar2(255);
nr_item_nf_w			number(10);
cd_cnpj_editado_w			varchar2(20);
cd_cgc_fornecedor_w		varchar2(14);
ds_fantasia_w			varchar2(255);
ds_fornecedor_w			varchar2(255);
cd_material_w			number(6);
cd_material_ww			number(6);
ds_material_w			varchar2(255);
vl_unitario_nf_w			number(13,4);
ds_assunto_w			varchar2(80);
ds_mensagem_w			varchar2(4000);
ds_consistencia_w			varchar2(4000);
ie_usuario_w			varchar2(1);
nr_seq_regra_w			number(10);
ds_email_adicional_w		varchar2(2000);
cd_perfil_disparar_w		number(5);
ds_email_origem_w			Varchar2(255);
nm_usuario_origem_w		Varchar2(255);
cd_local_estoque_w		Number(4);
cd_centro_custo_w			Number(4);
cd_grupo_material_w		Number(3);
cd_subgrupo_material_w		Number(3);
cd_classe_material_w		Number(5);
ie_consignado_w			Varchar2(1);
ie_envia_email_w			Varchar2(1);
qt_existe_w			Number(10);
ie_enviar_w			Varchar2(1) := 'S';
nm_usuario_receb_w		Varchar2(15);
ds_email_w			varchar2(255);
ie_existe_w			varchar2(1);
ds_destinatarios_w			Varchar2(4000);
ie_momento_envio_w		varchar2(1);
cd_comprador_w			varchar2(10);
ds_consistencia_qtde_w		varchar2(1000);
ds_consistencia_valor_w		varchar2(1000);
ds_consistencia_prazo_w		varchar2(1000);
ds_email_remetente_w		varchar2(255);


Cursor C01 is
	select	ds_assunto,
		ds_mensagem_padrao,
		ie_usuario,
		nr_sequencia,
		ds_email_adicional,
		cd_perfil_disparar,
		nvl(ds_email_remetente,'X'),
		nvl(ie_momento_envio,'I')
	from	regra_envio_email_compra
	where	cd_estabelecimento = cd_estabelecimento_p
	and	ie_tipo_mensagem = 77
	and	ie_situacao = 'A';
	
Cursor C02 is
	select 	cd_local_estoque,
		cd_centro_custo,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		ie_consignado,
		ie_envia_email
	from	envio_email_compra_filtro
	where	nr_seq_regra = nr_seq_regra_w;
		
Cursor C03 is
	select	a.nr_nota_fiscal,
		b.nr_item_nf,
		substr(obter_cgc_cpf_editado(a.cd_cgc_emitente),1,20) cd_cnpj_editado,
		a.cd_cgc_emitente,
		substr(obter_dados_pf_pj(a.cd_pessoa_fisica, a.cd_cgc_emitente, 'F'),1,255) ds_fantasia,
		substr(obter_nome_pf_pj(a.cd_pessoa_fisica, a.cd_cgc_emitente),1,255) ds_fornecedor,
		b.cd_material,
		substr(obter_desc_material(b.cd_material),1,255) ds_material,		
		b.vl_unitario_item_nf,
		c.vl_unitario_material,
		c.nr_ordem_compra,
		c.nr_item_oci,
		b.qt_item_nf,
		trunc(b.dt_entrega_ordem,'dd'),
		trunc(a.dt_emissao,'dd')
	from	nota_fiscal a,
		nota_fiscal_item b,
		ordem_compra_item c
	where	a.nr_sequencia = b.nr_sequencia
	and	b.nr_ordem_compra = c.nr_ordem_compra
	and	b.nr_item_oci = c.nr_item_oci
	and	b.cd_material is not null
	and	b.nr_sequencia = nr_sequencia_p;

cursor C04 is
	select	nm_usuario_receb
	from	regra_envio_email_usu
	where	nr_seq_regra = nr_seq_regra_w
	and	nm_usuario_receb is not null;	
		
begin

select	count(*)
into	qt_existe_regra_w
from	regra_envio_email_compra
where	cd_estabelecimento = cd_estabelecimento_p
and	ie_tipo_mensagem = 77
and	ie_situacao = 'A';

if	(qt_existe_regra_w > 0) then

	open C03;
	loop
	fetch C03 into	
		nr_nota_fiscal_w,
		nr_item_nf_w,
		cd_cnpj_editado_w,
		cd_cgc_fornecedor_w,
		ds_fantasia_w,
		ds_fornecedor_w,
		cd_material_w,
		ds_material_w,
		vl_unitario_nf_w,
		vl_unitario_oc_w,
		nr_ordem_compra_w,
		nr_item_oci_w,
		qt_item_nf_w,
		dt_entrega_w,
		dt_emissao_w;
	exit when C03%notfound;
		begin
		
		
		ds_consistencia_qtde_w	:= '';
		ds_consistencia_valor_w	:= '';
		ds_consistencia_prazo_w	:= '';
		
		open C01;
		loop
		fetch C01 into	
			ds_assunto_w,
			ds_mensagem_w,
			ie_usuario_w,
			nr_seq_regra_w,
			ds_email_adicional_w,
			cd_perfil_disparar_w,
			ds_email_remetente_w,			
			ie_momento_envio_w;
		exit when C01%notfound;
			begin
			ds_destinatarios_w		:= '';
			ds_email_w		:= '';
			cd_comprador_w		:= '';
			
			
			select	cd_comprador
			into	cd_comprador_w
			from	ordem_compra
			where	nr_ordem_compra = nr_ordem_compra_w;
			
			
			if	(cd_perfil_disparar_w is null) or
				((cd_perfil_disparar_w is not null) and (cd_perfil_disparar_w = obter_perfil_ativo)) then
			
				if	(ie_usuario_w = 'U') or
					((nvl(cd_comprador_w,'0') <> '0') and (ie_usuario_w = 'O')) then --Usuario
					begin
					select	ds_email,
						nm_usuario
					into	ds_email_origem_w,
						nm_usuario_origem_w
					from	usuario
					where	nm_usuario = nm_usuario_p;
					end;
				elsif	(ie_usuario_w = 'C') then --Setor compras
					begin
					select	ds_email
					into	ds_email_origem_w
					from	parametro_compras
					where	cd_estabelecimento = cd_estabelecimento_p;
			
					select	nvl(ds_fantasia,ds_razao_social)
					into	nm_usuario_origem_w
					from	estabelecimento_v
					where	cd_estabelecimento = cd_estabelecimento_p;
					end;
				elsif	(ie_usuario_w = 'O') then --Comprador
					begin
					select	max(ds_email),
						max(nm_guerra)
					into	ds_email_origem_w,
						nm_usuario_origem_w
					from	comprador
					where	cd_pessoa_fisica = cd_comprador_w
					and	cd_estabelecimento = cd_estabelecimento_p;
					end;
				end if;

				if	(ds_email_remetente_w <> 'X') then
					ds_email_origem_w	:= ds_email_remetente_w;
				end if;
				
				open C02;
				loop
				fetch C02 into	
					cd_local_estoque_w,
					cd_centro_custo_w,
					cd_grupo_material_w,
					cd_subgrupo_material_w,
					cd_classe_material_w,
					cd_material_ww,
					ie_consignado_w,
					ie_envia_email_w;
				exit when C02%notfound;
					begin
					if	(ie_consignado_w = 'N') then
						ie_consignado_w := '0';
					elsif	(ie_consignado_w = 'S') then
						ie_consignado_w := '1';
					elsif	(ie_consignado_w = 'A') then
						ie_consignado_w	:= null;
					end if;
					
					select	count(*)
					into	qt_existe_w
					from	nota_fiscal_item a,
						estrutura_material_v e
					where	a.nr_sequencia = nr_sequencia_p
					and	a.nr_ordem_compra is not null
					and	((cd_local_estoque_w is null) or (a.cd_local_estoque = nvl(cd_local_estoque_w,a.cd_local_estoque)))
					and	((cd_centro_custo_w is null) or (a.cd_centro_custo = nvl(cd_centro_custo_w,a.cd_centro_custo)))
					and	a.cd_material = e.cd_material
					and	e.cd_material = nvl(cd_material_ww,e.cd_material)
					and	e.cd_grupo_material = nvl(cd_grupo_material_w,e.cd_grupo_material)
					and	e.cd_subgrupo_material = nvl(cd_subgrupo_material_w,e.cd_subgrupo_material)
					and	e.cd_classe_material = nvl(cd_classe_material_w,e.cd_classe_material)
					and	e.ie_consignado = nvl(ie_consignado_w,e.ie_consignado);
					
					if	(qt_existe_w > 0) and
						(ie_envia_email_w = 'S') then
						ie_enviar_w := 'S';
					else
						ie_enviar_w := 'N';
					end if;
					end;
				end loop;
				close C02;
				
				
				
				if	(ie_enviar_w = 'S') then	
					
					ds_destinatarios_w := ds_destinatarios_w || ds_email_adicional_w;					
					if	(ds_destinatarios_w is not null) and
						(substr(ds_destinatarios_w,length(ds_destinatarios_w),1) <> ';') then
						ds_destinatarios_w := ds_destinatarios_w || ';';
					end if;
					
					open C04;
					loop
					fetch C04 into	
						nm_usuario_receb_w;
					exit when C04%notfound;
						begin
						
						select	ds_email
						into	ds_email_w
						from	usuario
						where	nm_usuario = nm_usuario_receb_w;	
	
						if	(ds_email_w is not null) then
		
							select	nvl(max('S'),'N')
							into	ie_existe_w
							from	dual
							where	upper(ds_destinatarios_w) like upper('%' || ds_email_w || '%');

							if	(ie_existe_w = 'N') then
								ds_destinatarios_w	:= ds_destinatarios_w || ds_email_w || ';';
							end if;
						end if;
						end;				
					end loop;
					close C04;
					
					/*Prazo*/					
					if	(dt_entrega_w is not null) and
						(dt_emissao_w > dt_entrega_w) then					
						ds_consistencia_prazo_w := wheb_mensagem_pck.get_texto(300318,'DS_RETORNO1='||PKG_DATE_FORMATERS.TO_VARCHAR(dt_entrega_w, 'shortDate', cd_estabelecimento_p, nm_usuario_p)||';DS_RETORNO2='|| PKG_DATE_FORMATERS.TO_VARCHAR(dt_emissao_w, 'shortDate', cd_estabelecimento_p, nm_usuario_p));
					end if;
					
					
					select	sum(qt_prevista_entrega - nvl(qt_real_entrega,0))
					into	qt_material_oc_w
					from	ordem_compra_item_entrega
					where	nr_ordem_compra = nr_ordem_compra_w
					and	nr_item_oci = nr_item_oci_w
					and	trunc(dt_prevista_entrega,'dd') = trunc(dt_entrega_w,'dd')
					and	dt_cancelamento is null;	
					/*Quantidade*/
					if	(qt_material_oc_w <> qt_item_nf_w) then						
						ds_consistencia_qtde_w := wheb_mensagem_pck.get_texto(300324,'DS_RETORNO1='||campo_mascara_virgula_casas(qt_material_oc_w,4)||';DS_RETORNO2='||campo_mascara_virgula_casas(qt_item_nf_w,4));
					end if;
					
					/*Valor*/
					if	(vl_unitario_oc_w <> vl_unitario_nf_w) then
						ds_consistencia_valor_w	:= wheb_mensagem_pck.get_texto(300329,'DS_RETORNO1='||campo_mascara_virgula_casas(vl_unitario_oc_w,4)||';DS_RETORNO2='||campo_mascara_virgula_casas(vl_unitario_nf_w,4));
					end if;
					
							
					ds_consistencia_w	:= substr(ds_consistencia_prazo_w || ds_consistencia_qtde_w || ds_consistencia_valor_w,1,4000);
					
					
					
					
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_ordem',to_char(nr_ordem_compra_w)),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_consistencia',ds_consistencia_w),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_seq_nota',to_char(nr_sequencia_p)),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_nota_fiscal',nr_nota_fiscal_w),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_item_nf',to_char(nr_item_nf_w)),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@cnpj_editado',cd_cnpj_editado_w),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@cnpj',cd_cgc_fornecedor_w),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@fantasia_pj',ds_fantasia_w),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@razao_pj',ds_fornecedor_w),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@cd_material',to_char(cd_material_w)),1,80);
					ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_material',ds_material_w),1,80);					
					
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nr_ordem',to_char(nr_ordem_compra_w)),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ds_consistencia',ds_consistencia_w),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nr_seq_nota',to_char(nr_sequencia_p)),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nr_nota_fiscal',nr_nota_fiscal_w),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nr_item_nf',to_char(nr_item_nf_w)),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@cnpj_editado',cd_cnpj_editado_w),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@cnpj',cd_cgc_fornecedor_w),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@fantasia_pj',ds_fantasia_w),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@razao_pj',ds_fornecedor_w),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@cd_material',to_char(cd_material_w)),1,4000);
					ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ds_material',ds_material_w),1,4000);
					
					if	((ds_email_origem_w is not null) and
						(ds_destinatarios_w is not null) and
						(nm_usuario_origem_w is not null) and
						(ds_consistencia_w is not null)) then
						
						if	(ie_momento_envio_w = 'A') then
							sup_grava_envio_email(
								'NF',
								'77',
								nr_sequencia_p,
								null,
								null,
								ds_destinatarios_w,
								nm_usuario_origem_w,
								ds_email_origem_w,
								ds_assunto_w,
								ds_mensagem_w,
								cd_estabelecimento_p,
								nm_usuario_p);
						else
							begin
							enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_origem_w,ds_destinatarios_w,nm_usuario_origem_w,'M');
							exception when others then
								ds_assunto_w := '';
							end;
						end if;

					end if;
				end if;
			end if;
			end;
		end loop;
		close C01;
		
		end;
	end loop;
	close C03;
end if;

end enviar_email_diver_nf_oc;
/