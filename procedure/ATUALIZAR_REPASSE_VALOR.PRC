create or replace
procedure atualizar_repasse_valor(	nr_Seq_protocolo_p	number,
				nr_interno_conta_p		number,
				vl_baixa_p		number,
				nm_usuario_p		varchar2,
				nr_seq_baixa_cartao_p	number,
				vl_total_cartao_p		number,
				nr_seq_parcela_p	number default null,
				nr_seq_caixa_rec_p	number default null) is


nr_interno_conta_w	number(10);
nr_seq_proc_rep_novo_w	number(10);
nr_seq_mat_rep_novo_w	number(10);
nr_seq_repasse_w		number(10);
vl_titulo_w		number(15,2);
vl_repasse_w		number(15,2);
vl_total_repasse_w	number(15,2);
vl_desp_cartao_w		number(15,2) := 0;
vl_novo_repasse_w	number(15,2);
vl_baixa_w		number(15,2);
pr_conta_w		number(15,6);
vl_conta_w		number(15,2);
nr_seq_movto_cartao_w	number(10);
vl_cartao_w		number(15,2);
vl_baixado_cartao_w	number(15,2);
vl_saldo_cartao_w		number(15,2);
vl_ajuste_cartao_w		number(15,2);
vl_despesa_cartao_w	number(15,2);
ds_observacao_w		varchar2(4000)	:= null;
ie_desp_repasse_cartao_w	varchar2(1)	:= 'S';
cd_estabelecimento_w	number(4);
vl_total_cartao_w		number(15,2);
vl_total_contas_w		number(15,2);
vl_repasse_conta_w	number(15,4);
vl_repassar_w		number(15,4);
vl_original_repasse_w	number(15,4);
ds_consistencia_w	valor_dominio.ds_valor_dominio%type;
nr_sequencia_w		MOVTO_TRANS_FINANC.nr_sequencia%type;
nr_seq_forma_pagto_w	forma_pagto_regra.nr_sequencia%type;
vl_total_parcela_w	movto_cartao_cr.vl_transacao%type;
ie_despesa_w			varchar2(1) := 'N';

cursor c03 is
select 	nr_interno_conta,
	vl_conta,
	nvl(vl_repasse_conta,0)
from 	conta_paciente
where	((nr_interno_conta = nr_interno_conta_p) or
	(nr_seq_protocolo = nr_seq_protocolo_p and nr_seq_protocolo_p is not null));


cursor c01 is
select 	obter_valor_repasse_item(a.nr_seq_procedimento,'P') vl_total_repasse,
	a.vl_repasse,
	a.nr_sequencia
from	procedimento_repasse a,
	procedimento_paciente b
where 	nvl(a.ie_desc_caixa,'N')	= 'N'
and	a.nr_seq_procedimento	= b.nr_sequencia
and	a.ie_status in ('A','U')
and	b.nr_interno_conta  	= nr_interno_conta_w;

cursor c02 is
select	obter_valor_repasse_item(a.nr_seq_material,'M'),
	a.vl_repasse,
	a.nr_sequencia
from	material_repasse a,
	material_atend_paciente b
where 	nvl(a.ie_desc_caixa,'N')	= 'N'
and	a.nr_seq_material = b.nr_sequencia
and	a.ie_status in ('A','U')
and	b.nr_interno_conta  = nr_interno_conta_w;

begin

vl_despesa_cartao_w	:= 0;

if	(nr_seq_baixa_cartao_p is not null) then
	select	nr_seq_movto
	into	nr_seq_movto_cartao_w
	from	movto_cartao_cr_baixa
	where	nr_sequencia	= nr_seq_baixa_cartao_p;

	select	vl_transacao,
		cd_estabelecimento
	into	vl_cartao_w,
		cd_estabelecimento_w
	from	movto_cartao_cr
	where	nr_sequencia	= nr_seq_movto_cartao_w;
	
	obter_param_usuario(3020,5,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_despesa_w);
	
	/*Francisco - 23/10/2009 - OS 142835 - Deduzir despesa do valor repassado */
	select	nvl(max(ie_desp_repasse_cartao),'S')
	into	ie_desp_repasse_cartao_w
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_w;
	if	(ie_desp_repasse_cartao_w = 'N') then
		select	nvl(vl_despesa,0)
		into	vl_despesa_cartao_w
		from	movto_cartao_cr_baixa
		where	nr_sequencia	= nr_seq_baixa_cartao_p;
	end if;
	
	/*Francisco - 23/10/2009 - OS 142835 - Calcular ajustes nos casos de libera��o de repasse por parcela */

	select	nvl(sum(vl_baixa - nvl(vl_despesa,0)),0)
	into	vl_baixado_cartao_w
	from	movto_cartao_cr_baixa a
	where	a.nr_seq_movto	= nr_seq_movto_cartao_w
	and	a.nr_sequencia	<> nr_seq_baixa_cartao_p;

	vl_saldo_cartao_w	:= vl_cartao_w - vl_baixado_cartao_w;

	vl_ajuste_cartao_w	:= dividir(vl_cartao_w,vl_saldo_cartao_w);
else
	vl_ajuste_cartao_w	:= 1;

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	conta_paciente
	where	((nr_interno_conta = nr_interno_conta_p) or
		(nr_seq_protocolo = nr_seq_protocolo_p and nr_seq_protocolo_p is not null));
		
	/*Francisco - 23/10/2009 - OS 142835 - Deduzir despesa do valor repassado */
	select	nvl(max(ie_desp_repasse_cartao),'S')
	into	ie_desp_repasse_cartao_w
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(ie_desp_repasse_cartao_w = 'D') then
		begin
		select	obter_forma_pagto_regra_cartao(a.nr_sequencia),
			vl_transacao
		into	nr_seq_forma_pagto_w,
			vl_total_parcela_w
		from	movto_cartao_cr a
		where	a.nr_seq_caixa_rec	= nr_seq_caixa_rec_p;
		exception
		when others then
			nr_seq_forma_pagto_w := null;
			vl_total_parcela_w := 0;
		end;	
		
		begin
		select	tx_administracao
		into	vl_despesa_cartao_w
		from	forma_pagto_regra
		where	nr_sequencia = nr_seq_forma_pagto_w;
		exception
		when others then
			vl_despesa_cartao_w := 0;
		end;
		
		vl_despesa_cartao_w	:= vl_total_parcela_w * dividir_sem_round(vl_despesa_cartao_w,100);
		
	end if;	
end if;

select	nvl(max(ie_desp_repasse_cartao),'S')
into	ie_desp_repasse_cartao_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(sum(vl_conta),0)
into	vl_total_contas_w
from 	conta_paciente
where	((nr_interno_conta = nr_interno_conta_p) or
	(nr_seq_protocolo = nr_seq_protocolo_p and nr_seq_protocolo_p is not null));

open c03;
loop
fetch c03 into
	nr_interno_conta_w,
	vl_conta_w,
	vl_repasse_conta_w;
exit when c03%notfound;

	open c01;
	loop
	fetch c01 into
		vl_total_repasse_w,
		vl_repasse_w,
		nr_seq_repasse_w;
	exit when c01%notfound;

		vl_desp_cartao_w := vl_despesa_cartao_w;

		/* dsantos em 17/03/2010, OS 202895. Criado vl_original_repasse para gravar o valor do repasse antes das altera��es.
		este valor deve ser gravado no procedimento de repasse original, e em todos os desdobramentos gerados a partir dele.
		vl_original_repasse deve ser obtido atrav�s do select abaixo, nao no cursor. */
		
		select	vl_original_repasse
		into	vl_original_repasse_w
		from	procedimento_repasse
		where	nr_sequencia	= nr_seq_repasse_w;

		if	(vl_original_repasse_w is null) then
			vl_original_repasse_w	:= vl_repasse_w;
		end if;
		
		vl_repassar_w		:= (dividir_sem_round(vl_baixa_p,vl_conta_w) * vl_repasse_conta_w);		
		
		vl_novo_repasse_w	:= (dividir_sem_round(vl_repassar_w, vl_repasse_conta_w) * vl_original_repasse_w);
				
		if	((vl_repasse_w-vl_novo_repasse_w) < 0) then
			vl_novo_repasse_w	:= vl_novo_repasse_w - (vl_novo_repasse_w-vl_repasse_w);
		end if;
		
		if	(vl_desp_cartao_w <> 0) then
			vl_desp_cartao_w	:= dividir_sem_round(vl_desp_cartao_w, vl_baixa_p) * vl_repassar_w;
		end if;
		
		begin 
		
		select 	obter_descricao_dominio(708,x.IE_TIPO_CONSISTENCIA)
		into 	ds_consistencia_w
		from 	TIPO_RECEBIMENTO x, MOVTO_TRANS_FINANC y
		where 	x.cd_tipo_recebimento = y.cd_tipo_recebimento
		and 	y.nr_sequencia = (select  max(m.nr_sequencia)
				from 	procedimento_repasse r, 
						procedimento_paciente p, 
						conta_paciente c, 
						titulo_receber_liq l,
						titulo_receber t,
						MOVTO_TRANS_FINANC m,
						TIPO_RECEBIMENTO i
				where 	r.nr_seq_procedimento = p.nr_sequencia
				and   	p.nr_interno_conta =  c.nr_interno_conta
				and 	t.nr_titulo in (substr(obter_titulo_conta_protocolo(0, c.NR_INTERNO_CONTA),1,100))
				and 	l.nr_titulo = t.nr_titulo
				and		l.nr_sequencia = (select max(l.nr_sequencia)
						from procedimento_repasse r, 
						procedimento_paciente p, 
						conta_paciente c, 
						titulo_receber_liq l,
						titulo_receber t,
						MOVTO_TRANS_FINANC m,
						TIPO_RECEBIMENTO i
						where 	r.nr_seq_procedimento = p.nr_sequencia
						and   	p.nr_interno_conta =  c.nr_interno_conta
						and 	t.nr_titulo in (substr(obter_titulo_conta_protocolo(0, c.NR_INTERNO_CONTA),1,100))
						and 	l.nr_titulo = t.nr_titulo
						and 	l.NR_SEQ_MOVTO_TRANS_FIN = m.nr_sequencia
						and 	m.cd_tipo_recebimento = i.cd_tipo_recebimento
						and 	r.nr_sequencia = nr_seq_repasse_w)
				and 	l.VL_RECEBIDO > 0
				and 	l.NR_SEQ_MOVTO_TRANS_FIN = m.nr_sequencia
				and 	m.cd_tipo_recebimento = i.cd_tipo_recebimento
				and 	r.nr_sequencia = nr_seq_repasse_w);
		exception when others then 
			ds_consistencia_w := null;		
		end;
		
		update 	procedimento_repasse
		set	vl_repasse	= vl_novo_repasse_w,
			vl_liberado	= vl_novo_repasse_w,
			ie_status		= 'R',
			dt_liberacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			vl_original_repasse	= vl_original_repasse_w,
			ds_observacao	= decode(ds_consistencia_w, null, '',
							  wheb_mensagem_pck.get_texto(354279,'vl_repasse=' || vl_repasse || ';vl_novo_repasse_w=' || vl_novo_repasse_w || ';ds_consistencia_w=' || ds_consistencia_w ||';ds_observacao=' ||  ds_observacao)),
			vl_desp_cartao	= vl_desp_cartao_w,
			nr_seq_parcela	= nr_seq_parcela_p
		where	nr_sequencia	= nr_seq_repasse_w;		
		
		if	(ie_despesa_w = 'S') and
			(ie_desp_repasse_cartao_w = 'S') then
			vl_desp_cartao_w	:= 0;
		end if;
		
		if	((vl_repasse_w - (vl_novo_repasse_w + vl_desp_cartao_w)) > 0) then
			
			desdobrar_procmat_repasse(nr_seq_repasse_w,
						null,
						'A',
						(vl_repasse_w - (vl_novo_repasse_w + vl_desp_cartao_w)),
						nm_usuario_p,
						nr_seq_proc_rep_novo_w,
						nr_seq_mat_rep_novo_w);

			update	procedimento_repasse
			set	ds_observacao		= nvl(ds_observacao,ds_observacao_w),
				vl_original_repasse		= vl_original_repasse_w
			where	nr_sequencia		= nr_seq_proc_rep_novo_w;

		end if;
	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into
		VL_total_repasse_w,
		vl_repasse_w,
		nr_seq_repasse_w;
	exit when c02%notfound;

		vl_desp_cartao_w := vl_despesa_cartao_w;

		/* dsantos em 17/03/2010, OS 202895. Criado vl_original_repasse para gravar o valor do repasse antes das altera��es.
		este valor deve ser gravado no material de repasse original, e em todos os desdobramentos gerados a partir dele.
		vl_original_repasse deve ser obtido atrav�s do select abaixo, nao no cursor. */
		
		select	vl_original_repasse
		into	vl_original_repasse_w
		from	material_repasse
		where	nr_sequencia	= nr_seq_repasse_w;

		if	(vl_original_repasse_w is null) then
			vl_original_repasse_w	:= vl_repasse_w;
		end if;

		vl_repassar_w		:= (dividir_sem_round(vl_baixa_p,vl_conta_w) * vl_repasse_conta_w);                                                                                                   
		vl_novo_repasse_w	:= (dividir_sem_round(vl_repassar_w, vl_repasse_conta_w) * vl_original_repasse_w);

		if	(vl_desp_cartao_w <> 0) then

			vl_desp_cartao_w	:= dividir_sem_round(vl_desp_cartao_w, vl_baixa_p) * vl_repassar_w;

		end if;

		update	material_repasse
		set	vl_repasse		= vl_novo_repasse_w,
			vl_liberado		= vl_novo_repasse_w,
			ie_status		= 'R',
			dt_liberacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			vl_original_repasse	= vl_original_repasse_w,
			vl_desp_cartao		= vl_desp_cartao_w,
			nr_seq_parcela	 	= nr_seq_parcela_p
		where	nr_sequencia		= nr_seq_repasse_w;

		if	(ie_despesa_w = 'S') and
			(ie_desp_repasse_cartao_w = 'S') then
			vl_desp_cartao_w	:= 0;
		end if;
		
		if	((vl_repasse_w - (vl_novo_repasse_w + vl_desp_cartao_w)) > 0) then
			desdobrar_procmat_repasse(null,
						nr_seq_repasse_w,
						'A',
						(vl_repasse_w - (vl_novo_repasse_w - vl_desp_cartao_w)),
						nm_usuario_p,
						nr_seq_proc_rep_novo_w,
						nr_seq_mat_rep_novo_w);

			update	material_repasse
			set	ds_observacao		= nvl(ds_observacao,ds_observacao_w),
				vl_original_repasse	= vl_original_repasse_w
			where	nr_sequencia		= nr_seq_mat_rep_novo_w;

		end if;
	end loop;
	close c02;

end loop;
close c03;

end atualizar_repasse_valor;
/