create or replace
procedure pls_atualizar_cart_conf_dinam
		(	nr_seq_lote_sib_p		number,
			ie_tipo_verificacao_p		number,
			qt_retirar_incluir_dig_ini_p	varchar2,
			qt_retirar_incluir_dig_fim_p	varchar2,
			nm_usuario_p			Varchar2) is 
			
/*
ie_tipo_verificacao_p
1 - Retirar
2 - Incluir
*/			
			
nr_seq_retorno_sib_w	number(10);
cd_usuario_plano_w	varchar2(30);
cd_usuario_plano_ww	varchar2(30);
cd_carteira_pesquisa_w	varchar2(30);
nm_beneficario_w	varchar2(70);
			
Cursor C01 is
	select	a.nr_sequencia nr_seq_retorno_sib
	from	pls_retorno_sib		a,
		pls_lote_retorno_sib	b
	where	a.nr_seq_lote_sib	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_lote_sib_p
	order by nm_beneficiario;
	
TYPE 		fetch_array IS TABLE OF c01%ROWTYPE;
s_array 	fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_c01_w	Vetor;

begin

gravar_processo_longo('Atulizando cart�es dinamicamentes' ,'PLS_ATUALIZAR_CART_CONF_DINAM',0);

open c01;
loop
FETCH C01 BULK COLLECT INTO s_array LIMIT 1000;
	Vetor_c01_w(i) := s_array;
	i := i + 1;
EXIT WHEN C01%NOTFOUND;
END LOOP;
CLOSE C01;

for i in 1..Vetor_c01_w.COUNT loop
	s_array := Vetor_c01_w(i);
	for z in 1..s_array.COUNT loop
	nr_seq_retorno_sib_w	:= s_array(z).nr_seq_retorno_sib;
	
	begin
	
	select	cd_usuario_plano,
		cd_usuario_plano,
		nm_beneficiario 
	into	cd_usuario_plano_w,
		cd_usuario_plano_ww,
		nm_beneficario_w
	from	pls_retorno_sib
	where	nr_sequencia	= nr_seq_retorno_sib_w;
	
	gravar_processo_longo(nm_beneficario_w || ' - ' || cd_usuario_plano_w,'PLS_ATUALIZAR_CART_CONF_DINAM',-1);
	
	if	(ie_tipo_verificacao_p	= 1) then
		if	(qt_retirar_incluir_dig_ini_p <> '0') and
			(qt_retirar_incluir_dig_ini_p is not null) then
			cd_carteira_pesquisa_w	:= substr(cd_usuario_plano_w,qt_retirar_incluir_dig_ini_p+1,length(cd_usuario_plano_w));
			cd_usuario_plano_ww	:= cd_carteira_pesquisa_w;
		end if;	
		if	(qt_retirar_incluir_dig_fim_p <> '0') and
			(qt_retirar_incluir_dig_fim_p is not null) then
			cd_carteira_pesquisa_w	:= substr(cd_usuario_plano_ww,1,length(cd_usuario_plano_ww) - qt_retirar_incluir_dig_fim_p);
		end if;	
	elsif	(ie_tipo_verificacao_p	= 2) then
		if	(qt_retirar_incluir_dig_ini_p is not null) then
			cd_carteira_pesquisa_w	:= qt_retirar_incluir_dig_ini_p||cd_usuario_plano_w;
			cd_usuario_plano_ww	:= cd_carteira_pesquisa_w;
		end if;	
		if	(qt_retirar_incluir_dig_fim_p is not null) then
			cd_carteira_pesquisa_w	:= cd_usuario_plano_ww || qt_retirar_incluir_dig_fim_p;
		end if;	
	end if;
	
	update	pls_retorno_sib
	set	cd_carteira_pesquisa	= cd_carteira_pesquisa_w
	where	nr_sequencia		= nr_seq_retorno_sib_w;
	
	end;
	end loop;
end loop;	
	
commit;

end pls_atualizar_cart_conf_dinam;
/
