create or replace
procedure codificacao_iniciar_analise(
					nr_sequencia_p number,
					nm_usuario_p varchar) is

begin

if (nr_sequencia_p is not null) then

	update	codificacao_atendimento
	set		ie_status 			= 'AI',
			dt_inicio_analise 	= sysdate,
			dt_atualizacao		= sysdate,
			nm_usuario			= nm_usuario_p		
	where	nr_sequencia 		= nr_sequencia_p;

end if;

commit;

end codificacao_iniciar_analise;
/