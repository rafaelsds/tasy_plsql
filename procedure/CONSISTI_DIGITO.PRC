CREATE OR REPLACE
procedure Consisti_Digito(	ie_Rotina_p				Varchar2,
				cd_codigo_p				Varchar2, 
				ie_resultado_p 	out	Varchar2) is
				

ie_resultado_w			Varchar2(1);

BEGIN

if	(consiste_digito(ie_rotina_p, cd_codigo_p)) then
	ie_resultado_w		:= 'S';
else
	ie_resultado_w		:= 'N';
end if;

ie_resultado_p  :=ie_resultado_w;

END Consisti_Digito;
/

