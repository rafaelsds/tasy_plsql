create or replace
procedure gerar_w_extrato_cartao_cr (	nr_seq_extrato_p	number,
										ds_conteudo_p		varchar2,
										nm_usuario_p		Varchar2) is 

begin

insert into	w_extrato_cartao_cr (	nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_extrato,
									ds_conteudo )
						values  (   w_extrato_cartao_cr_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_extrato_p,
									substr(ds_conteudo_p,1,4000) );
											
commit;

end gerar_w_extrato_cartao_cr;
/
