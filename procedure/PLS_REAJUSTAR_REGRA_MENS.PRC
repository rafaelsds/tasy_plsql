create or replace
procedure pls_reajustar_regra_mens
			(	nr_seq_reajuste_p	Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

qt_regra_vl_min_w		number(10);
nr_seq_regra_mens_w		number(10);
vl_minimo_w			number(15,2);
tx_reajuste_w			number(7,4);
vl_minimo_novo_w		number(15,2);
nr_seq_contrato_w		number(10);
nr_seq_intercambio_w		number(10);

Cursor C01 is
	select	nr_sequencia,
		vl_minimo
	from	pls_regra_mens_contrato
	where	nr_seq_contrato	= nr_seq_contrato_w
	and	ie_tipo_regra	= 'VM'
	and	nvl(dt_fim_vigencia,sysdate) >= sysdate
	union all
	select	nr_sequencia,
		vl_minimo
	from	pls_regra_mens_contrato
	where	nr_seq_intercambio	= nr_seq_intercambio_w
	and	ie_tipo_regra	= 'VM'
	and	nvl(dt_fim_vigencia,sysdate) >= sysdate;

begin

select	max(nr_seq_contrato),
	max(nr_seq_intercambio)
into	nr_seq_contrato_w,
	nr_seq_intercambio_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;

select	count(*)
into	qt_regra_vl_min_w
from	pls_regra_mens_contrato
where	((nr_seq_contrato	= nr_seq_contrato_w) or (nr_seq_intercambio = nr_seq_intercambio_w))
and	ie_tipo_regra		= 'VM'
and	nvl(vl_minimo,0)	> 0;

if	(qt_regra_vl_min_w > 0) and
	(nvl(nr_seq_contrato_w,0) > 0) then
	select	tx_reajuste
	into	tx_reajuste_w
	from	pls_reajuste
	where	nr_sequencia	= nr_seq_reajuste_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_mens_w,
		vl_minimo_w;
	exit when C01%notfound;
		begin
		vl_minimo_novo_w	:= round(vl_minimo_w + (vl_minimo_w * tx_reajuste_w) / 100,2);
		
		insert into pls_regra_mens_contrato(	nr_sequencia, vl_minimo, cd_estabelecimento, dt_inicio_vigencia,
							dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
							dt_fim_vigencia, nr_seq_contrato, dt_limite_movimentacao,
							ie_tipo_data_limite, qt_interv_mes_mensalidade, ie_mensalidade_mes_rescisao, nr_seq_reajuste,
							ie_agrupar_valor, nr_seq_intercambio, vl_minimo_abat_fatura, pr_aplicacao_reajuste,
							nr_seq_regra_origem, ie_tipo_regra)
						(select	pls_regra_mens_contrato_seq.NextVal, vl_minimo_novo_w, cd_estabelecimento, dt_inicio_vigencia,
							sysdate, nm_usuario_p, sysdate, nm_usuario_p,
							dt_fim_vigencia, nr_seq_contrato, dt_limite_movimentacao,
							ie_tipo_data_limite, qt_interv_mes_mensalidade, ie_mensalidade_mes_rescisao, nr_seq_reajuste_p,
							ie_agrupar_valor, nr_seq_intercambio, vl_minimo_abat_fatura, tx_reajuste_w,
							nr_seq_regra_mens_w, ie_tipo_regra
						from	pls_regra_mens_contrato
						where	nr_sequencia	= nr_seq_regra_mens_w);
		
		update	pls_regra_mens_contrato
		set	dt_fim_vigencia	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_regra_mens_w;
		
		end;
	end loop;
	close C01;
end if;

commit;

end pls_reajustar_regra_mens;
/