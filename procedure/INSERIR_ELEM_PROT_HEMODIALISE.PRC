create or replace
procedure inserir_elem_prot_hemodialise (	nr_prescricao_p		number,
						nr_seq_solucao_p	number,
						nm_usuario_p		varchar2) is

qt_itens_w			number(10,0);
nr_seq_elemento_w			number(10,0);
cd_unidade_medida_w		varchar2(30);
qt_elemento_w			number(15,4);
nr_sequencia_w			number(10,0);
nr_seq_protocolo_w		number(10,0);
nr_seq_elem_mat_w		number(10,0);
qt_volume_w			number(15,4);
qt_volume_ww			number(15,4);
nr_erro_w				number(15,0);
cd_material_w			number(6);
qt_dose_w			number(15,3);
ie_via_aplicacao_w			varchar2(5);
ie_dose_peso_w			varchar2(5);
cd_intervalo_w			varchar2(7);
ie_dispensacao_w			varchar2(5);
qt_peso_w			number(6,3);

cd_estabelecimento_w		number(4);
dt_prescricao_w			date;
dt_primeiro_horario_w		date;
nr_horas_validade_w		number(5);
cd_setor_atendimento_w		number(5);


cd_unidade_consumo_w		varchar2(30);
ds_horarios_w			varchar2(2000);
ds_horarios_ww			varchar2(2000);
nr_agrupamento_w			number(7,1);
nr_intervalo_w			number(10);
qt_conversao_dose_w		number(15,4);
qt_unitaria_w			number(18,6);
qt_produto_w			number(15,3);
qt_total_dispensar_w		number(18,6);
ds_erro_w			varchar2(255);
cd_unidade_medida_ww		varchar2(255);
ds_observacao_w			varchar2(4000);
hr_prim_horario_w			varchar2(5);

nr_seq_material_w			number(6,0);
qt_solucao_w			number(15,4);

ie_regra_disp_w			varchar2(1); /* Rafael em 15/3/8 OS86206 */

cursor c01 is
	select	nr_seq_elemento,
		cd_unidade_medida,
		qt_elemento
	from	protocolo_npt_item
	where	nr_seq_protocolo = nr_seq_protocolo_w;

cursor c02 is
	select	cd_material,
		qt_volume,
		cd_unidade_medida,
		ds_observacao,
		ie_dose_peso,
		ie_dispensacao
	from	protocolo_npt_prod
	where	nr_seq_protocolo = nr_seq_protocolo_w;


begin

if	(nr_seq_solucao_p is not null) then
	begin
	delete	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_solucao	= nr_seq_solucao_p
	and	ie_agrupador		= 13;
	
	delete	hd_prescr_sol_elem
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p;
	
	commit;
	
	select	nvl(max(nr_seq_protocolo),0)
	into	nr_seq_protocolo_w
	from	prescr_solucao
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p;

	select	count(*)
	into	qt_itens_w
	from	hd_prescr_sol_elem
	where	nr_prescricao	= nr_prescricao_p
	and	nr_seq_solucao	= nr_seq_solucao_p;

	if	(qt_itens_w = 0) then
		begin

		open c01;
		loop
		fetch c01 into 
			nr_seq_elemento_w,
			cd_unidade_medida_w,
			qt_elemento_w;
		exit when c01%notfound;
	
			select	hd_prescr_sol_elem_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert	into hd_prescr_sol_elem (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_solucao,
				nr_seq_elemento,
				cd_unidade_medida,
				qt_elemento)
			values	(
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_seq_solucao_p,
				nr_seq_elemento_w,
				cd_unidade_medida_w,
				qt_elemento_w);
		end loop;
		close c01;
		
		end;
	end if;

	select	count(*)
	into	qt_itens_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_solucao	= nr_seq_solucao_p
	and	ie_agrupador		= 13;

	if	(qt_itens_w = 0) then
		begin
	
		/* gerar produto */
		select	nvl(max(nr_sequencia),0),
			nvl(max(nr_agrupamento),0)					
		into	nr_seq_material_w,
			nr_agrupamento_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p;

		cd_unidade_medida_w	:= obter_unid_med_usua('ml');

		open c02;
		loop
		fetch c02 into 	cd_material_w,
				qt_volume_w,
				cd_unidade_medida_ww,
				ds_observacao_w,
				ie_dose_peso_w,
				ie_dispensacao_w;
		exit when c02%notfound;
	
			if	(cd_unidade_medida_ww is not null) then
				cd_unidade_medida_w := cd_unidade_medida_ww;
			end if;
			
			select	nvl(qt_volume_w,nvl(max(qt_volume),0)),
				nvl(max(nr_etapas),1)
			into	qt_dose_w,
				nr_intervalo_w
			from	prescr_solucao
			where	nr_prescricao	= nr_prescricao_p
			and	nr_seq_solucao	= nr_seq_solucao_p;
			
			

			/* obter dados prescricao */
			select	cd_estabelecimento,
				dt_prescricao,
				dt_primeiro_horario,
				nr_horas_validade,
				cd_setor_atendimento,
				qt_peso
			into	cd_estabelecimento_w,
				dt_prescricao_w,
				dt_primeiro_horario_w,
				nr_horas_validade_w,
				cd_setor_atendimento_w,
				qt_peso_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_p;
			
			if	(ie_dose_peso_w = 'S') and
				(nvl(qt_peso_w,0) > 0) then
				qt_dose_w	:= qt_dose_w * qt_peso_w;
			end if;

			/* obter unidade consumo material */
			select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo
			into	cd_unidade_consumo_w
			from	material
			where	cd_material = cd_material_w;

			/* obter conversao dose */
			select	obter_conversao_unid_med(cd_material_w, cd_unidade_medida_w)
			into	qt_conversao_dose_w
			from	dual;

			/* obter quantidade consumo */
			select	obter_conversao_unid_med_cons(cd_material_w, cd_unidade_medida_w, qt_dose_w)
			into	qt_unitaria_w
			from	dual;

			/* obter quantidade dispensar */
			obter_quant_dispensar(cd_estabelecimento_w, cd_material_w, nr_prescricao_p, 0, cd_intervalo_w, ie_via_aplicacao_w, qt_unitaria_w, 0, 1, null, null, cd_unidade_medida_w, null, qt_produto_w, qt_total_dispensar_w, ie_regra_disp_w, ds_erro_w,'N','N');
			
			/* Verifica o campo IE_DISPENSACAO da tabela PROTOCOLO_NPT_PROD e aplica a regra do mesmo */
			if (ie_dispensacao_w = 'N') then
				ie_regra_disp_w := 'N';
			elsif (ie_dispensacao_w = 'T') then
				ie_regra_disp_w := 'S';
			end if;
			
			/* atualizar agrupamento */
			nr_agrupamento_w 	:= nr_agrupamento_w + 1;
			nr_seq_material_w	:= nr_seq_material_w + 1;
			
			/*obter se medicamento possui convers�o para ml*/
			select	nvl(obter_conversao_ml(cd_material_w,qt_dose_w,cd_unidade_medida_w),0)
			into	qt_volume_ww
			from	dual;

			/* inserir produto */
			insert into prescr_material(
				nr_prescricao,
				nr_sequencia,
				nr_sequencia_solucao,
				ie_origem_inf,
				cd_material,
				cd_unidade_medida,
				qt_dose,
				qt_unitaria,
				qt_material,
				qt_solucao,
				dt_atualizacao,
				nm_usuario,
				cd_intervalo,
				nr_agrupamento,
				cd_motivo_baixa,
				ie_utiliza_kit,
				cd_unidade_medida_dose,
				qt_conversao_dose,
				ie_urgencia,
				nr_ocorrencia,
				qt_total_dispensar,
				ie_medicacao_paciente,
				ie_agrupador,
				ie_suspenso,
				ie_se_necessario,
				ie_bomba_infusao,
				ie_aplic_bolus,
				ie_aplic_lenta,
				ie_acm,
				ie_cultura_cih,
				ie_antibiograma,
				ie_uso_antimicrobiano,
				ie_recons_diluente_fixo,
				ie_sem_aprazamento,
				ie_regra_disp,
				ds_observacao)
			values	(
				nr_prescricao_p,
				nr_seq_material_w,
				nr_seq_solucao_p,
				'N',
				cd_material_w,
				cd_unidade_consumo_w,
				qt_dose_w,
				qt_unitaria_w,
				qt_produto_w,
				qt_volume_ww,
				sysdate,
				nm_usuario_p,
				cd_intervalo_w,
				nr_agrupamento_w,
				0,
				'N',
				cd_unidade_medida_w,
				qt_conversao_dose_w,
				'N',
				nr_intervalo_w,
				qt_total_dispensar_w,
				'N',
				13,
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				ie_regra_disp_w,
				ds_observacao_w);
				
			Consistir_Prescr_Material(nr_prescricao_p, nr_seq_material_w, nm_usuario_p, obter_perfil_ativo, nr_erro_w); /*Almir em 09/10/2007 OS70754 */

		end loop;
		close c02;
		
		end;
	end if;
	end;
end if;

select	sum(qt_solucao)
into	qt_solucao_w
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
and	nr_sequencia_solucao	= nr_seq_solucao_p
and	ie_agrupador		= 13;

update	prescr_solucao
set	qt_solucao_total	= qt_solucao_w
where	nr_prescricao		= nr_prescricao_p
and	nr_seq_solucao		= nr_seq_solucao_p;

Calcular_Elem_Hemodialise(nr_prescricao_p, nr_seq_solucao_p, nm_usuario_p);

commit;

end inserir_elem_prot_hemodialise;
/
