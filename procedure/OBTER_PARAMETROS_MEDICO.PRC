create or replace
procedure obter_parametros_medico(
			cd_estabelecimento_p		number,
			qt_horas_passado_sv_p		out number,
			ie_lib_orientacao_alta_p 	out varchar2,
			ie_lib_evento_p			out varchar2,
			ie_libera_anestesia_p		out varchar2,
			ie_regra_atend_nasc_p		out varchar2,
			ie_lib_atestado_p		out varchar2,
			ie_lib_receita_p		out varchar2,
			ie_lib_fatur_med_p		out varchar2,
			ie_libera_partic_p		out varchar2,
			ie_libera_atraso_cirur_p	out varchar2,
			ie_liberar_consolidacao_pepo_p	out varchar2,
			ie_libera_evento_pepo_p		out varchar2,
			ie_libera_cavidade_p		out varchar2,
			ds_mascara_dnv_p		out varchar2,
			ds_mascara_dfm_p		out varchar2,
			ie_libera_demarcacao_p		out varchar2,
			ie_libera_hemoderivado_p	out varchar2,
			ie_libera_agente_p		out varchar2,
			ie_libera_medicamento_p 	out varchar2,
			ie_libera_material_p 		out varchar2,
			ie_libera_curativo_p		out varchar2,
			ie_libera_terapia_p			out varchar2,
			ds_diretorio_pacs_p			out varchar2,
			ie_libera_anexos_oft_p		out varchar2
			) is
			
qt_horas_passado_sv_w	number(15,5) := 0;
ie_lib_orientacao_alta_w	varchar(1) := 'N';
ie_lib_evento_w		varchar(1) := 'N';
ie_libera_anestesia_w	varchar(1) := 'N';
ie_regra_atend_nasc_w	varchar2(15);
ie_lib_atestado_w		varchar(1) := 'N';
ie_lib_receita_w		varchar(1) := 'N';
ie_lib_fatur_med_w		varchar(1) := 'N';
ie_libera_partic_w		varchar(1) := 'N';
ie_libera_atraso_cirur_w	varchar(1) := 'N';
ie_liberar_consolidacao_pepo_w	varchar(1) := 'N';
ie_libera_evento_pepo_w		varchar(1) := 'N';
ie_libera_cavidade_w		varchar(1) := 'N';
ds_mascara_dnv_w		varchar2(30);
ds_mascara_dfm_w		varchar2(30);
ie_libera_demarcacao_w		varchar2(1) := 'N';	
ie_libera_hemoderivado_w	varchar2(1) := 'N';
ie_libera_agente_w			varchar2(1) := 'N';
ie_libera_medicamento_w		varchar2(1) := 'N';
ie_libera_material_w		varchar2(1) := 'N'; 
ie_libera_curativo_w      	varchar2(1) := 'N';
ie_libera_terapia_w			varchar2(1) := 'N';
ie_libera_anexos_oft_w		varchar2(1) := 'N';
ds_diretorio_pacs_w			varchar2(256);

begin
if	(cd_estabelecimento_p is not null) then
	begin
	select	nvl(qt_horas_passado_sv,12),
		nvl(ie_lib_orientacao_alta, 'N'),
		nvl(ie_lib_evento, 'N'),
		nvl(ie_libera_anestesia, 'N'),
		nvl(ie_regra_atend_nasc, 'N'),
		nvl(ie_lib_atestado, 'N'),
		nvl(ie_lib_receita,'N'),		
		nvl(ie_lib_fatur_med,'N') , 
		nvl(ie_libera_partic,'N') ,
		nvl(ie_libera_atraso_cirur,'N'),		
		nvl(ie_liberar_consolidacao_pepo,'N'),
		nvl(ie_libera_evento_pepo,'N'),
		nvl(ie_libera_cavidade,'N'),		
		ds_mascara_dnv,
		ds_mascara_dfm,
		ie_libera_demarcacao,
		nvl(ie_libera_hemoder_pepo,'N'),
		nvl(ie_libera_agente_pepo,'N'),
		nvl(ie_libera_medic_pepo,'N'),
		nvl(ie_libera_material_pepo,'N'),
		nvl(ie_libera_curativo,'N'),
		nvl(ie_libera_terapia_pepo,'N'),
		ds_diretorio_pacs,
		nvl(ie_libera_anexos_oft,'N')
	into	qt_horas_passado_sv_w,
		ie_lib_orientacao_alta_w,
		ie_lib_evento_w,
		ie_libera_anestesia_w,
		ie_regra_atend_nasc_w,
		ie_lib_atestado_w,
		ie_lib_receita_w,
		ie_lib_fatur_med_w,
		ie_libera_partic_w,
		ie_libera_atraso_cirur_w,
		ie_liberar_consolidacao_pepo_w,
		ie_libera_evento_pepo_w,
		ie_libera_cavidade_w,
		ds_mascara_dnv_w,
		ds_mascara_dfm_w,
		ie_libera_demarcacao_w,
		ie_libera_hemoderivado_w,
		ie_libera_agente_w,
		ie_libera_medicamento_w,
		ie_libera_material_w,
		ie_libera_curativo_w,
		ie_libera_terapia_w,
		ds_diretorio_pacs_w,
		ie_libera_anexos_oft_w
	from	parametro_medico
	where	cd_estabelecimento = cd_estabelecimento_p;
	end;
end if;

qt_horas_passado_sv_p		:= qt_horas_passado_sv_w;
ie_lib_orientacao_alta_p	:= ie_lib_orientacao_alta_w;
ie_lib_evento_p			:= ie_lib_evento_w;
ie_libera_anestesia_p		:= ie_libera_anestesia_w;
ie_regra_atend_nasc_p		:= ie_regra_atend_nasc_w;
ie_lib_atestado_p		:= ie_lib_atestado_w;
ie_lib_receita_p		:= ie_lib_receita_w;
ie_lib_fatur_med_p		:= ie_lib_fatur_med_w;
ie_libera_partic_p		:= ie_libera_partic_w;
ie_libera_atraso_cirur_p	:= ie_libera_atraso_cirur_w;
ie_liberar_consolidacao_pepo_p	:= ie_liberar_consolidacao_pepo_w;
ie_libera_evento_pepo_p		:= ie_libera_evento_pepo_w;
ie_libera_cavidade_p		:= ie_libera_cavidade_w;
ds_mascara_dnv_p		:= ds_mascara_dnv_w;
ds_mascara_dfm_p		:= ds_mascara_dfm_w;
ie_libera_demarcacao_p		:= ie_libera_demarcacao_w;
ie_libera_hemoderivado_p   	:= ie_libera_hemoderivado_w;
ie_libera_agente_p   :=  ie_libera_agente_w;
ie_libera_medicamento_p := ie_libera_medicamento_w;
ie_libera_material_p := ie_libera_material_w;
ie_libera_curativo_p := ie_libera_curativo_w;
ie_libera_terapia_p  :=	ie_libera_terapia_w;
ds_diretorio_pacs_p := ds_diretorio_pacs_w;
ie_libera_anexos_oft_p := ie_libera_anexos_oft_w;
end obter_parametros_medico;
/
