create or replace
procedure HSL_GERAR_TIT_PAGAR_ADIANT_DEV
	(nr_adiantamento_p	in	number,
	 nr_sequencia_p		in	number,
	 nm_usuario_p		in	varchar2,
	 dt_vencimento_p	in 	date,
	 cd_centro_custo_p	in 	number) is

cd_estabelecimento_w	number(15,0);
dt_devolucao_w		date;
vl_devolucao_w		number(15,2);
cd_moeda_w		number(15,0);
cd_tipo_taxa_juro_w	number(15,0);
cd_tipo_taxa_multa_w	number(15,0);
cont_w			number(15,0);
cd_pessoa_fisica_w	varchar2(255);
cd_cgc_w		varchar2(255);
nr_seq_movto_tf_w	number(10);
ie_gerar_tributo_tit_w	varchar2(1);
nr_titulo_w		number(10);
nr_seq_trans_fin_baixa_w	number(10);
nr_seq_produto_w	number(10);
nr_seq_grupo_prod_w	number(10);
dt_vencimento_w		date;
nr_documento_w		number(20);

begin

select	count(*)
into	cont_w
from	titulo_pagar
where	nr_adiant_rec		= nr_adiantamento_p
and	nr_seq_adiant_dev	= nr_sequencia_p;

if	(cont_w > 0) then
	raise_application_error(-20011, 'J� existe t�tulo gerado para esta devolu��o!');
end if;

select	a.dt_devolucao,
	a.vl_devolucao,
	a.nr_seq_movto_tf,
	b.ie_gerar_tributo_tit,
	a.nr_seq_trans_fin_baixa
into	dt_devolucao_w,
	vl_devolucao_w,
	nr_seq_movto_tf_w,
	ie_gerar_tributo_tit_w,
	nr_seq_trans_fin_baixa_w
from	motivo_dev_adiant b,
	adiantamento_dev a
where	a.nr_seq_motivo_dev	= b.nr_sequencia(+)
and	a.nr_adiantamento	= nr_adiantamento_p
and	a.nr_sequencia		= nr_sequencia_p;

select	a.cd_estabelecimento,
	a.nr_seq_grupo_prod,
	substr(somente_numero(a.nr_documento),1,20)
into	cd_estabelecimento_w,
	nr_seq_grupo_prod_w,
	nr_documento_w
from	adiantamento a
where	a.nr_adiantamento	= nr_adiantamento_p;

select	max(b.nr_seq_produto)
into	nr_seq_produto_w
from	adiantamento_classif b,
	adiantamento a
where	a.nr_adiantamento	= b.nr_adiantamento
and	a.nr_adiantamento	= nr_adiantamento_p;

if	(nr_seq_movto_tf_w is not null) then

	select	max(a.cd_pessoa_fisica),
		max(a.cd_cgc)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	movto_trans_financ a
	where	a.nr_sequencia	= nr_seq_movto_tf_w;

end if;

if	(cd_pessoa_fisica_w is null) and (cd_cgc_w is null) then

	select	cd_pessoa_fisica,
		cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	adiantamento
	where	nr_adiantamento	= nr_adiantamento_p;

end if;

select	cd_moeda_padrao,
	cd_tipo_taxa_juro,
	cd_tipo_taxa_multa
into	cd_moeda_w,
	cd_tipo_taxa_juro_w,
	cd_tipo_taxa_multa_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

begin
select	obter_data_vencimento(dt_vencimento_p,4,1,'U','P')
into	dt_vencimento_w
from	dual;
exception
	when others then
	dt_vencimento_w := sysdate;
end;

select	titulo_pagar_seq.nextval
into	nr_titulo_w
from	dual;

insert into titulo_pagar
	(nr_titulo,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	vl_titulo,
	vl_saldo_titulo,
	vl_saldo_juros,
	vl_saldo_multa,
	cd_moeda,
	tx_juros,
	tx_multa,
	cd_tipo_taxa_juro,
	cd_tipo_taxa_multa,
	tx_desc_antecipacao,
	dt_limite_antecipacao,
	vl_dia_antecipacao,
	cd_tipo_taxa_antecipacao,
	ie_situacao,
	ie_origem_titulo,
	ie_tipo_titulo,
	nr_seq_nota_fiscal,
	cd_pessoa_fisica,
	cd_cgc,
	nr_adiant_rec,
	nr_seq_adiant_dev,
	dt_contabil,
	nr_lote_contabil,
	nr_seq_trans_fin_baixa,
	ds_observacao_titulo,
	nr_seq_grupo_prod,
	nr_documento,
	nr_nosso_numero)
select	nr_titulo_w,
	cd_estabelecimento_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	dt_vencimento_w,
	dt_vencimento_w,
	vl_devolucao_w,
	vl_devolucao_w,
	0,
	0,
	cd_moeda_w,
	0,
	0,
	cd_tipo_taxa_juro_w,
	cd_tipo_taxa_multa_w,
	0,
	null,
	0,
	null,
	'A',
	'2',
	'27',
	null,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	nr_adiantamento_p,
	nr_sequencia_p,
	sysdate,
	0,
	nr_seq_trans_fin_baixa_w,
	('Integra��o ISAT - Devolu��o vinculado ao adiantamento recebido nro ' || nr_adiantamento_p),
	nr_seq_grupo_prod_w,
	nr_adiantamento_p,
	nr_documento_w
from	dual;
ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_w, nm_usuario_p);

if	(nr_seq_produto_w is not null) then

	insert into titulo_pagar_classif
		(nr_titulo,
		nr_sequencia,
		vl_titulo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_produto,
		cd_centro_custo,
		nr_seq_conta_financ,
		cd_conta_contabil,
		nr_seq_trans_fin)
	values	(nr_titulo_w,
		1,
		vl_devolucao_w,
		sysdate,
		nm_usuario_p,
		nr_seq_produto_w,
		cd_centro_custo_p,
		529,
		'23267',
		1346);		
end if;

if	(ie_gerar_tributo_tit_w = 'S') then

	gerar_tributo_titulo(	nr_titulo_w,
				nm_usuario_p,
				'N',
				null,
				null,
				null,
				null,
				null,
				cd_estabelecimento_w,
				null);

end if;

commit;

end HSL_GERAR_TIT_PAGAR_ADIANT_DEV;
/