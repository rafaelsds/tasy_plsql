create or replace
procedure fis_gerar_lote_rvs(
			nm_usuario_p		varchar2,
			nr_sequencia_p		number,
			ie_somente_nota_p	varchar2) is

dt_referencia_w		date;
dt_inicio_lote_w	date;
dt_fim_lote_w		date;
ie_registro_w		varchar2(1);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_empresa_w		estabelecimento.cd_empresa%type;

begin

begin

select	'S',
	trunc(dt_referencia,'dd'),
	trunc(dt_inicio_lote,'dd'),
	fim_dia(dt_fim_lote),
	cd_estabelecimento,
	cd_empresa
into	ie_registro_w,
	dt_referencia_w,
	dt_inicio_lote_w,
	dt_fim_lote_w,
	cd_estabelecimento_w,
	cd_empresa_w	
from	fis_siscoserv_lote a
where	a.nr_sequencia = nr_sequencia_p;

exception
when others then
	ie_registro_w := 'N';
end;

if	(nvl(ie_registro_w,'N') = 'S') then
	begin

	fis_gerar_pessoas_rvs(nm_usuario_p, nr_sequencia_p, dt_inicio_lote_w, dt_fim_lote_w, ie_somente_nota_p, cd_empresa_w, cd_estabelecimento_w);
	
	fis_gerar_notas_rvs(nm_usuario_p,'1',dt_inicio_lote_w, dt_fim_lote_w, nr_sequencia_p);

	end;
end if;

commit;

end fis_gerar_lote_rvs;
/
