CREATE OR REPLACE procedure Ageint_gerar_cobertura_item(nr_seq_ageint_p		number,
				      nr_seq_ageint_item_p	varchar2,
				      nm_usuario_p		Varchar2,
              ie_html_p varchar2 default 'N') is 	      
			

cd_estab_w		Number(4,0);
cd_estabelecimento_w	Number(4,0);
cd_convenio_w		Number(5,0);
cd_categoria_w		Varchar2(10);
cd_plano_w		Varchar2(10);
ie_tipo_Atendimento_w	Number(3,0);
cd_usuario_convenio_w	Varchar2(30);
ie_Sexo_w		Varchar2(1);
qt_idade_w		Number(3);
dt_nascimento_w		Date;
cd_pessoa_fisica_w	Varchar2(10);
cd_conv_item_w		number(5,0);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
cd_procedimento_w	Number(15,0);
ie_origem_proced_w	Number(10,0);
nr_seq_proc_interno_w	number(10,0);
ie_bloq_glosa_part_w	varchar2(1);
ie_resp_autor_w		varchar2(10);

vl_aux_w		Number(15,4);
ds_aux_w		Varchar2(10);
nr_seq_regra_w		number(10);
ie_regra_w		Number(5,0);
ie_glosa_w		Varchar2(1);
ie_edicao_w            varchar2(1);
cd_edicao_ajuste_w     number(10);
cd_convenio_glosa_w	Number(10);
cd_categoria_glosa_w	Varchar2(10);
pr_glosa_w			number(15,4);
vl_glosa_w			number(15,4);
ds_erro_w		varchar2(255);
ie_pacote_w		varchar2(1);
qt_item_edicao_w   number(10);

ie_ignorar_regra_lib_proc_w	parametro_agenda_integrada.ie_ignorar_regra_lib_proc%type;
ie_autorizacao_w	    agenda_integrada_item.ie_autorizacao%type; 
nr_seq_ageint_item_w	agenda_integrada_item.nr_sequencia%type;
cd_convenio_ww			agenda_integrada.cd_convenio%type;
cd_categoria_ww			agenda_integrada.cd_categoria%type;
cd_plano_ww				agenda_integrada.cd_plano%type;
param437             	varchar2(1);
nr_seq_cobertura_w	agenda_integrada.nr_seq_cobertura%type;
cd_empresa_w		agenda_integrada.cd_empresa%type;
nr_seq_adic_item_w	ageint_exame_adic_item.nr_sequencia%type;

Cursor C01 is
	select	cd_estabelecimento
	from	estabelecimento
	where	ie_situacao = 'A' and nvl(param437,'N') <> 'S'
	union
	SELECT  cd_estabelecimento
	FROM    usuario_estabelecimento_v
	WHERE   nvl(param437,'N') = 'S'
	and     ie_situacao = 'A'
	AND     nm_usuario = nm_usuario_p;
	
Cursor C02 is 
	select	nr_sequencia,
			cd_procedimento,
			nr_seq_proc_interno,
			ie_origem_proced
	from	agenda_integrada_item
	where	((nr_seq_agenda_int = nr_seq_ageint_p
		and	nr_seq_ageint_item_p is null)
	or		nr_sequencia = nr_seq_ageint_item_p)
	and nr_seq_proc_interno is not null;
  
cursor C03 is
  select nr_seq_proc_interno,
        cd_procedimento,
        ie_origem_proced,
        nr_seq_item,
        nr_sequencia
  from ageint_exame_adic_item
  where ((nr_seq_item = nr_seq_ageint_item_p and nr_seq_ageint_p is null)
  or nr_seq_item in (select nr_sequencia from agenda_integrada_item where nr_seq_agenda_int = nr_seq_ageint_p) and nr_seq_ageint_item_p is null)
  and nr_seq_proc_interno is not null;
  
  procedure executar_cursor_c02 is
  
  begin
    open C01;
    loop
    fetch C01 into	
      cd_estab_w;
    exit when C01%notfound;
      begin
          
      ageint_consiste_plano_conv(
            null,
            cd_convenio_ww,
            cd_procedimento_w,
            ie_origem_proced_w,
            sysdate,
            1,
            nvl(ie_tipo_atendimento_w,0),
            cd_plano_ww,
            null,
            ds_erro_w,
            0,
            null,
            ie_regra_w,
            null,
            nr_seq_regra_w,
            nr_seq_proc_interno_w,
            cd_categoria_ww,
            cd_estab_w,
            null,
            ie_sexo_w,
            ie_glosa_w,
            cd_edicao_ajuste_w,
            nr_seq_cobertura_w,
            cd_convenio_glosa_w,
            cd_categoria_glosa_w,
            cd_pessoa_fisica_w,
            cd_empresa_w,
            pr_glosa_w,
            vl_glosa_w);
        
      ie_edicao_w	:= ageint_obter_se_proc_conv(cd_estab_w, cd_convenio_ww, cd_categoria_ww, sysdate, cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w, ie_tipo_atendimento_w);
      ie_pacote_w	:= obter_se_pacote_convenio(cd_procedimento_w, ie_origem_proced_w, cd_convenio_ww, cd_estab_w);
          
      if	(ie_edicao_w 				= 'N') and
        (nvl(cd_edicao_ajuste_w,0) 	= 0)   and
        (nvl(ie_glosa_w,'L') 		= 'L') and
        (ie_pacote_w				= 'N') then
         ie_glosa_w := 'T';
      end if;

      if	(ie_edicao_w 				= 'N') and
        (nvl(cd_edicao_ajuste_w,0) 	> 0)   and
        (nvl(ie_glosa_w,'L') 		= 'L') and
        (ie_pacote_w				= 'N') then

        select   count(*)
        into     qt_item_edicao_w
        from     preco_amb
        where    cd_edicao_amb = cd_edicao_ajuste_w
        and      cd_procedimento = cd_procedimento_w
        and      ie_origem_proced = ie_origem_proced_w;

        if	(qt_item_edicao_w = 0) then
            ie_glosa_w :=    'G';
        end if;
      end if;
    
      ie_autorizacao_w := 'L';
      if	((ie_regra_w in (1,2,5)) or ((ie_regra_w = 8) and (ie_bloq_glosa_part_w = 'N'))) then
          ie_autorizacao_w	:= 'B';
      elsif (ie_regra_w in (3,6,7)) then
        select 	nvl(max(ie_resp_autor),'H')
        into	ie_resp_autor_w
        from 	regra_convenio_plano
        where 	nr_sequencia = nr_seq_regra_w;
        if	(ie_resp_autor_w = 'H') then
           ie_autorizacao_w := 'PAH';
        elsif (ie_resp_autor_w = 'P') then
             ie_autorizacao_w	:= 'PAP';
        end if;
      end if;

      if	(ie_glosa_w in ('G','T','D','F')) then
         ie_autorizacao_w := 'B';
      end if;
    
      select	nvl(max(ie_ignorar_regra_lib_proc),'N')
      into	ie_ignorar_regra_lib_proc_w
      from	parametro_agenda_integrada
      where	nvl(cd_estabelecimento, cd_estab_w) = cd_estab_w;
    
      if	(ie_ignorar_regra_lib_proc_w = 'S') and
        (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'es_MX') then
        begin
          ie_regra_w		 := 4;
          ie_glosa_w		 := 'L';
          ie_autorizacao_w := 'L';
        end;
      end if;
            
      insert into w_ageint_cobertura_item(ie_glosa,
                ie_regra,              
                cd_estabelecimento,
                nr_seq_ageint_item,
                nm_usuario,
                ie_autorizacao,
                nr_seq_exame_adic_item)
              values	(
                ie_glosa_w,
                ie_regra_w,              
                cd_estab_w,
                nr_seq_ageint_item_w,
                nm_usuario_p,
                ie_autorizacao_w,
                nr_seq_adic_item_w);
      end;
    end loop;
    close C01;
  end executar_cursor_c02;
		      			
begin --Ageint_gerar_cobertura_item

delete	w_ageint_cobertura_item
where	nm_usuario			= nm_usuario_p;

obter_param_usuario(869, 437, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, param437);  
obter_param_usuario(869, 187, obter_perfil_ativo, nm_usuario_p, 0, ie_bloq_glosa_part_w);

select	cd_estabelecimento,
	cd_convenio,
	cd_categoria, 
	cd_plano,
	ie_tipo_Atendimento,
	cd_usuario_convenio,
	cd_pessoa_fisica,
	nr_seq_cobertura_w,
	cd_empresa_w
into	cd_estabelecimento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w, 
	ie_tipo_atendimento_w,
	cd_usuario_convenio_w,
	cd_pessoa_fisica_w,
	nr_seq_cobertura_w,
	cd_empresa_w
from	agenda_integrada
where	nr_sequencia = nr_seq_ageint_p;

select	max(ie_Sexo),
		max(dt_nascimento)
into	ie_Sexo_w,
		dt_nascimento_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

qt_idade_w	:= obter_idade(dt_nascimento_w, sysdate, 'A');

open C02;
loop
fetch C02 into	
	nr_seq_ageint_item_w,
	cd_procedimento_w,
	nr_seq_proc_interno_w,
	ie_origem_proced_w;
exit when C02%notfound;
	begin
	delete	w_ageint_cobertura_item
	where	nr_seq_ageint_item	= nr_seq_ageint_item_w;

	select	max(cd_convenio),
			max(cd_categoria),
			max(cd_plano)
	into	cd_conv_item_w,
			cd_categ_item_w,
			cd_plano_item_w
	from	agenda_integrada_conv_item
	where	nr_seq_agenda_item	= nr_seq_ageint_item_w;

	if	(cd_conv_item_w is not null) then
		cd_convenio_ww	:= cd_conv_item_w;
		cd_categoria_ww	:= cd_categ_item_w;
		cd_plano_ww		:= cd_plano_item_w;
	else
		cd_convenio_ww 	:= cd_convenio_w;
		cd_categoria_ww := cd_categoria_w;
		cd_plano_ww		:= cd_plano_w;
	end if;

	if	((cd_procedimento_w is null) or (ie_origem_proced_w is null)) then

		obter_proc_tab_interno_conv(	nr_seq_proc_interno_w,
						cd_estabelecimento_w,
						cd_convenio_ww,
						cd_categoria_ww,
						cd_plano_ww,
						null,
						cd_procedimento_w,
						ie_origem_proced_w,
						null,
						sysdate,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);

	end if;

  executar_cursor_c02();
	
	end;
end loop;
close C02;

if ie_html_p = 'S' then

  for c_03 in C03 loop

    select	max(cd_convenio),
        max(cd_categoria),
        max(cd_plano)
    into	cd_conv_item_w,
        cd_categ_item_w,
        cd_plano_item_w
    from	agenda_integrada_conv_item
    where	nr_seq_agenda_item	= c_03.nr_seq_item;

    if	(cd_conv_item_w is not null) then
      cd_convenio_ww	:= cd_conv_item_w;
      cd_categoria_ww	:= cd_categ_item_w;
      cd_plano_ww		:= cd_plano_item_w;
    else
      cd_convenio_ww 	:= cd_convenio_w;
      cd_categoria_ww := cd_categoria_w;
      cd_plano_ww		:= cd_plano_w;
    end if;

    if	(c_03.cd_procedimento is null or c_03.ie_origem_proced is null) then

      obter_proc_tab_interno_conv(c_03.nr_seq_proc_interno,
              cd_estabelecimento_w,
              cd_convenio_ww,
              cd_categoria_ww,
              cd_plano_ww,
              null,
              cd_procedimento_w,
              ie_origem_proced_w,
              null,
              sysdate,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null);
              
    else 
    
      cd_procedimento_w := c_03.cd_procedimento;
      ie_origem_proced_w := c_03.ie_origem_proced;
      nr_seq_proc_interno_w := c_03.nr_seq_proc_interno;

    end if;
    nr_seq_adic_item_w := c_03.nr_sequencia;
    nr_seq_ageint_item_w := c_03.nr_seq_item;
    
    executar_cursor_c02();

  end loop;
end if;

commit;

end Ageint_gerar_cobertura_item;
/
