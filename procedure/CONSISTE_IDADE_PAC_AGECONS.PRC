create or replace procedure Consiste_Idade_Pac_Agecons
		(cd_agenda_p		in	number,
		dt_agenda_p		in	date,
		qt_idade_p		in	number,
		cd_pessoa_fisica_p		in	varchar2,
		ds_erro_p			out	varchar2,
		nm_usuario_p		in	varchar2) is

ie_dia_semana_w				number(05);
qt_idade_min_w				number(10);
qt_idade_max_w				number(10);
qt_idade_w				number(10);
ds_erro_w				varchar2(255)	:= null;
dt_nascimento_w				date;
ie_consiste_idademin_zero_w			varchar2(1) 	:= 'N';
ie_cons_idade_pac_zerada_w		varchar2(1)	:= 'N';
nr_seq_turno_esp_w			number(10,0);
ie_cons_idade_pac_zero_ag_w 		agenda.ie_cons_idade_pac_zero_ag%type;
ie_consistir_idade_dt_agenda_w		agenda.ie_cons_idade_pac_zero_ag%type;

begin

obter_param_usuario(821, 105, Obter_Perfil_Ativo, nm_usuario_p, 0, ie_consiste_idademin_zero_w);
obter_param_usuario(821, 362, Obter_Perfil_Ativo, nm_usuario_p, 0, ie_cons_idade_pac_zerada_w);

select	max(dt_nascimento)
into	dt_nascimento_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

select	max(nvl(nr_seq_turno_esp,0))
into	nr_seq_turno_esp_w
from	agenda_consulta
where	cd_agenda = cd_agenda_p
and	dt_agenda = dt_agenda_p;

select	pkg_date_utils.get_WeekDay(dt_agenda_p)
into	ie_dia_semana_w
from	dual;

select 	nvl(max(ie_cons_idade_pac_zero_ag),'N'),
	nvl(max(ie_consistir_idade_dt_agenda),'N')
into 	ie_cons_idade_pac_zero_ag_w,
	ie_consistir_idade_dt_agenda_w
from	agenda 
where	cd_agenda = cd_agenda_p;

if 	(ie_consistir_idade_dt_agenda_w = 'S') then
	select	max(trunc(trunc(months_between(dt_agenda_p, dt_nascimento_w)) / 12))
	into	qt_idade_w
	from 	dual;
else	
	qt_idade_w := qt_idade_p;
end if;

select	max(qt_idade_min),
	max(qt_idade_max)
into	qt_idade_min_w,
	qt_idade_max_w
from	agenda_turno
where	cd_agenda	= cd_agenda_p
and		((ie_dia_semana	= ie_dia_semana_w) or ((ie_dia_semana = 9) and (pkg_date_utils.IS_BUSINESS_DAY(dt_agenda_p) = 1)))
and		to_date('01/01/1899 ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
		to_date('01/01/1899 ' || to_char(dt_agenda_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
and		to_date('01/01/1899 ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >= 
		to_date('01/01/1899 ' || to_char(dt_agenda_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
and		((dt_inicio_vigencia is null) or (trunc(dt_inicio_vigencia) <= trunc(dt_agenda_p)))
and		((dt_final_vigencia is null) or (trunc(dt_final_vigencia) >= trunc(dt_agenda_p)));		


if	(((qt_idade_min_w > 0) or ((ie_consiste_idademin_zero_w = 'S') and (qt_idade_min_w = 0))) or
	(qt_idade_max_w > 0)) and
	(((qt_idade_w > 0) or 
	((qt_idade_w = 0) and (ie_cons_idade_pac_zerada_w = 'S'))) or
	(ie_cons_idade_pac_zero_ag_w = 'S')) then
	begin
	
	if 	(qt_idade_min_w > qt_idade_w) or
		((qt_idade_w = 0) and (qt_idade_min_w = 0))THEN
		ds_erro_w	:= ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(277649,null);
	end if;		
	
	if (qt_idade_max_w is not null) and (nvl(qt_idade_min_w,0) <= qt_idade_max_w) and (qt_idade_max_w < qt_idade_w) then
		ds_erro_w	:= ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(277651,null);
	end if;		

	end;
end if;	

if	((qt_idade_min_w = 0) or (qt_idade_min_w is null)) and
	((qt_idade_max_w = 0) or (qt_idade_max_w is null)) and
	(ds_erro_w is null) then
	begin

	select	max(qt_idade_min),
			max(qt_idade_max)
	into	qt_idade_min_w,
			qt_idade_max_w
	from	agenda_turno
	where	cd_agenda	= cd_agenda_p
	and	ie_dia_semana	= 9
	and	to_date('01/01/1899 ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
		to_date('01/01/1899 ' || to_char(dt_agenda_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	and	to_date('01/01/1899 ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >= 
		to_date('01/01/1899 ' || to_char(dt_agenda_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	and		((dt_inicio_vigencia is null) or (trunc(dt_inicio_vigencia) <= trunc(dt_agenda_p)))
	and		((dt_final_vigencia is null) or (trunc(dt_final_vigencia) >= trunc(dt_agenda_p)));


	if	(((qt_idade_min_w > 0) or ((ie_consiste_idademin_zero_w = 'S') and (qt_idade_min_w = 0))) or
		(qt_idade_max_w > 0)) and
		(((qt_idade_w > 0) or
		((qt_idade_w = 0) and (ie_cons_idade_pac_zerada_w = 'S'))) or
		(ie_cons_idade_pac_zero_ag_w = 'S')) then
		begin

		if 	(qt_idade_min_w > qt_idade_w) or
			((qt_idade_w = 0) and (qt_idade_min_w = 0)) and
			(ds_erro_w is null) then
			ds_erro_w	:= ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(277649,null);
		end if;		
	
		if (qt_idade_max_w is not null) and (nvl(qt_idade_min_w,0) <= qt_idade_max_w) and (qt_idade_max_w < qt_idade_w) then
			ds_erro_w	:= ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(277651,null);
		end if;		
		
		end;
	end if;	

	end;
end if;

if	((nr_seq_turno_esp_w is not null) and (nr_seq_turno_esp_w > 0)) and
	(ds_erro_w is null) then
	
	select	max(qt_idade_min),
			max(qt_idade_max)
	into	qt_idade_min_w,
			qt_idade_max_w
	from	agenda_turno_esp
	where	nr_sequencia = nr_seq_turno_esp_w;
	
	if	(((qt_idade_min_w > 0) or ((ie_consiste_idademin_zero_w = 'S') and (qt_idade_min_w = 0))) or
		(qt_idade_max_w > 0)) and
		(((qt_idade_w > 0) or
		((qt_idade_w = 0) and (ie_cons_idade_pac_zerada_w = 'S'))) or
		(ie_cons_idade_pac_zero_ag_w = 'S')) then
		begin
		
		if 	(qt_idade_min_w > qt_idade_w) or
			((qt_idade_w = 0) and (qt_idade_min_w = 0)) and
			(ds_erro_w is null) then
			ds_erro_w	:= ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(277649,null);
		end if;		
	
		if (qt_idade_max_w is not null) and (nvl(qt_idade_min_w,0) <= qt_idade_max_w) and (qt_idade_max_w < qt_idade_w) then
			ds_erro_w	:= ds_erro_w || WHEB_MENSAGEM_PCK.get_texto(277651,null);
		end if;		

		end;
	end if;	
end if;

ds_erro_p	:= ds_erro_w;

end Consiste_Idade_Pac_Agecons;
/