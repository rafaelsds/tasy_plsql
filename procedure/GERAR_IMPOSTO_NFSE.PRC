create or replace
procedure gerar_imposto_nfse
			(	nr_seq_nota_p		number,
				nm_usuario_p		varchar2,
				nr_seq_sit_trib_p	number,
				cd_tributo_p		number) is

cd_tributo_w			number(03,0);
pr_imposto_w			number(07,4);
pr_desc_base_w			number(07,4);
cd_estabelecimento_w		number(04,0);
cd_cgc_emitente_w		varchar2(14);
cd_serie_nf_w			nota_fiscal.cd_serie_nf%type;
IE_TIPO_TRIBUTACAO_w		varchar2(255);
nr_nota_fiscal_w		varchar2(255);
nr_sequencia_nf_w		number(10);
nr_item_nf_w			number(05,0);
vl_total_nota_w			number(13,2);
vl_total_item_w			number(13,2);
vl_total_imposto_nota_w		number(13,2)	:= 0;
vl_total_imposto_item_w		number(13,2)	:= 0;
vl_minimo_tributo_w		number(15,2);
vl_reducao_base_w		number(15,2);
vl_minimo_base_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_desc_dependente_w		number(15,2);
vl_trib_anterior_w		number(15,2);
vl_total_base_w			number(15,2);
vl_reducao_w			number(15,2);
vl_mercadoria_w			number(15,2);
ie_soma_diminui_w		varchar2(01);
cd_convenio_w			Number(05,0);
nr_interno_conta_w		Number(10,0);
nr_seq_protocolo_w		Number(10,0);
dt_emissao_w			Date;
VL_TRIB_NAO_RETIDO_w		number(15,2);
VL_BASE_NAO_RETIDO_w		number(15,2);
VL_TRIB_ADIC_w			number(15,2);
VL_BASE_ADIC_w			number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
VL_TRIBUTO_W			number(15,2);
VL_MINIMO_BASE_CALCULO_W	number(15,2);
VL_TRIBUTO_A_RETER_W		number(15,2);
VL_BASE_A_RETER_W		number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_descontos_w			number(15,2);
cd_pessoa_fisica_w		varchar2(10);
ie_acumulativo_w		varchar2(10);
cd_cgc_w			varchar2(14);
nr_seq_sit_trib_w		number(10,0);
vl_mercadoria_liq_ww		number(15,2);
cd_condicao_pagamento_w		number(10,0);
ie_forma_pagamento_w		number(02,0);
vl_vencimento_w			number(15,2);
qt_venctos_w			number(10,0);
qt_existe_w			number(10,0);
nr_ccm_w			number(10)	:= null;
ie_vago_w			varchar2(255);
nr_seq_regra_w			number(10);
nr_seq_lote_protocolo_w		number(10);
nr_seq_eme_fatura_w		number(10);
ie_tipo_tributo_w		varchar2(15);
nr_seq_mensalidade_w		number(10);
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;

cd_darf_w			regra_calculo_imposto.cd_darf%type;

/* Impostos da nota fiscal */
cursor C01 is
	select	b.cd_tributo
	from	tributo	b
	where	b.ie_corpo_item	= 'C'
	and	b.ie_situacao	= 'A'
	and	b.ie_pf_pj in ('A',decode(cd_pessoa_fisica_w,null,'PJ','PF'))
	and	not exists 
		(select	1
		from	nota_fiscal_trib x
		where	x.nr_sequencia	= nr_seq_nota_p
		and	x.cd_tributo	= b.cd_tributo)
	and	(nr_ccm_w is null or nvl(b.ie_ccm,'S') = 'S')
	and	((nvl(ie_tipo_tributacao_w, 'X') <> '0') or (nvl(ie_super_simples, 'S') = 'S'))
	order 	by b.cd_tributo;

begin
select	cd_estabelecimento,
	vl_total_nota,
	cd_cgc_emitente,
	cd_serie_nf,
	nr_nota_fiscal,
	nr_sequencia_nf,
	dt_emissao,
	nvl(nr_interno_conta,0),
	nvl(nr_seq_protocolo,0),
	nvl(nr_seq_lote_prot,0),
	nvl(vl_mercadoria,0),
	cd_cgc,
	cd_pessoa_fisica,
	nvl(vl_descontos,0),
	cd_condicao_pagamento,
	nvl(nr_seq_eme_fatura,0),
	nr_seq_mensalidade
into	cd_estabelecimento_w,
	vl_total_nota_w,
	cd_cgc_emitente_w,
	cd_serie_nf_w,
	nr_nota_fiscal_w,
	nr_sequencia_nf_w,
	dt_emissao_w,
	nr_interno_conta_w,
	nr_seq_protocolo_w,
	nr_seq_lote_protocolo_w,
	vl_mercadoria_w,
	cd_cgc_w,
	cd_pessoa_fisica_w,
	vl_descontos_w,
	cd_condicao_pagamento_w,
	nr_seq_eme_fatura_w,
	nr_seq_mensalidade_w
from	nota_fiscal
where	nr_sequencia	= nr_seq_nota_p;

select	max(ie_tipo_tributacao)
into	ie_tipo_tributacao_w
from	pessoa_juridica
where	cd_cgc	= cd_cgc_w;

if	(nr_interno_conta_w > 0) then
	select	max(a.cd_convenio_parametro)
	into	cd_convenio_w
	from	atendimento_paciente	b,
		conta_paciente		a
	where	a.nr_interno_conta	= nr_interno_conta_w
	  and	a.nr_atendimento	= b.nr_atendimento;
elsif	(nr_seq_protocolo_w > 0) then
	select	max(cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
elsif	(nr_seq_lote_protocolo_w > 0) then
	select	max(cd_convenio)
	into	cd_convenio_w
	from	lote_protocolo
	where	nr_sequencia = nr_seq_lote_protocolo_w;
elsif	(nr_seq_eme_fatura_w > 0) then
	select	max(a.cd_convenio)
	into	cd_convenio_w
	from	eme_contrato a,
		eme_faturamento b
	where	a.nr_sequencia	= b.nr_seq_contrato
	and	b.nr_sequencia	= nr_seq_eme_fatura_w;
else
	select	max(cd_convenio)
	into	cd_convenio_w
	from	convenio_retorno	b,
		convenio_retorno_nf	a
	where	a.nr_seq_retorno	= b.nr_sequencia
	and	a.nr_seq_nota_fiscal	= nr_seq_nota_p;
end if;

if	(cd_convenio_w	is null) then
	select	max(a.cd_conv_integracao)
	into	cd_convenio_w
	from	nota_fiscal a
	where	a.nr_sequencia	= nr_seq_nota_p;
end if;

select	ie_forma_pagamento 		
into	ie_forma_pagamento_w
from	condicao_pagamento 
where	cd_condicao_pagamento	= cd_condicao_pagamento_w;

open C01;
loop
fetch C01 into
	cd_tributo_w;
exit when C01%notfound;
	begin
	select	max(ie_tipo_tributo)
	into	ie_tipo_tributo_w
	from	tributo
	where	cd_tributo	= cd_tributo_w;
	
	obter_dados_trib_tit_rec(	cd_tributo_w,
					cd_estabelecimento_w,
					cd_convenio_w,
					dt_emissao_w,
					'N',
					pr_imposto_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					ie_acumulativo_w,
					vl_teto_base_w,
					vl_desc_dependente_w,
					cd_pessoa_fisica_w,
					cd_cgc_w,
					ie_vago_w,
					null,
					0,
					nr_seq_regra_w,
					cd_darf_w);

	select	nvl(max(pr_desc_base),0)
	into	pr_desc_base_w
	from	regra_calculo_imposto
	where	nr_sequencia	= nr_seq_regra_w;

	if	(pr_imposto_w > 0) then
		vl_trib_nao_retido_w	:= 0;
		vl_base_nao_retido_w	:= 0;
		vl_trib_adic_w		:= 0;
		vl_base_adic_w		:= 0;
		vl_mercadoria_liq_ww	:= 0;

		select	nvl(sum(a.vl_trib_nao_retido), 0),
			nvl(sum(a.vl_base_nao_retido), 0),
			nvl(sum(a.vl_trib_adic), 0),
			nvl(sum(a.vl_base_adic), 0),
			nvl(sum(a.vl_tributo),0),
			nvl(sum(a.vl_base_calculo),0),
			nvl(sum(a.vl_reducao),0)
		into	vl_soma_trib_nao_retido_w,
			vl_soma_base_nao_retido_w,
			vl_soma_trib_adic_w,
			vl_soma_base_adic_w,
			vl_trib_anterior_w,
			vl_total_base_w,
			vl_reducao_w
		from	operacao_nota		o,
			nota_fiscal		b,
			nota_fiscal_trib	a
		where	a.nr_sequencia			= b.nr_sequencia
		and	trunc(b.dt_emissao, 'month')	= trunc(dt_emissao_w, 'month')
		and	a.cd_tributo			= cd_tributo_w
		and	b.cd_cgc_emitente		= cd_cgc_emitente_w
		and	nvl(b.ie_situacao,1)		= 1
		and	b.cd_operacao_nf		= o.cd_operacao_nf
		and	o.ie_operacao_fiscal		= 'S'
		and	(cd_cgc				= cd_cgc_w or 
			cd_pessoa_fisica		= cd_pessoa_fisica_w);
		
		if	(nr_seq_mensalidade_w is null) then
			vl_mercadoria_liq_ww	:= (vl_mercadoria_w - vl_descontos_w);	/*Anderson 12/09/2007 - OS66018 - Considerar o desconto ao gerar o valor do tributo*/
		else
			pls_obter_base_calc_mens(	nr_seq_mensalidade_w,
							null,
							cd_tributo_w,
							ie_tipo_tributo_w,
							vl_mercadoria_liq_ww);
		end if;

		vl_reducao_base_w		:= 0;
		if	(pr_desc_base_w <> 0) then
			vl_reducao_base_w	:= vl_mercadoria_liq_ww * (pr_desc_base_w / 100);
			vl_mercadoria_liq_ww	:= vl_mercadoria_liq_ww - vl_reducao_base_w;
		end if;

		obter_valores_tributo(	ie_acumulativo_w,
					pr_imposto_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					vl_soma_trib_nao_retido_w,
					vl_soma_trib_adic_w,
					vl_soma_base_nao_retido_w,
					vl_soma_base_adic_w,
					vl_mercadoria_liq_ww,
					vl_tributo_w,
					vl_trib_nao_retido_w,
					vl_trib_adic_w,
					vl_base_nao_retido_w,
					vl_base_adic_w,
					vl_teto_base_w,
					vl_trib_anterior_w,
					'N',
					vl_total_base_w,
					vl_reducao_w,
					vl_desc_dependente_w,
					0,
					0,
					null,
					null,
					0,
					sysdate,
					nr_seq_regra_irpf_w);

		insert	into nota_fiscal_trib
			(nr_sequencia,
			cd_tributo,
			vl_tributo,
			dt_atualizacao,
			nm_usuario,
			vl_base_calculo,
			tx_tributo,
			vl_reducao_base,
			vl_trib_nao_retido,
			vl_base_nao_retido,
			vl_trib_adic,           
			vl_base_adic,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_interno,
			vl_reducao,
			nr_seq_regra_trib)
		values	(nr_seq_nota_p,
			cd_tributo_w,
			vl_tributo_w,
			sysdate,
			nm_usuario_p,
			decode(nr_seq_mensalidade_w, null, vl_mercadoria_w - vl_descontos_w - vl_reducao_base_w,vl_mercadoria_liq_ww),
			pr_imposto_w,
			0,
			vl_trib_nao_retido_w,
			vl_base_nao_retido_w,
			vl_trib_adic_w,
			vl_base_adic_w,
			sysdate,
			nm_usuario_p,
			nota_fiscal_trib_seq.nextval,
			vl_reducao_base_w,
			nr_seq_regra_w);
		
		if	(ie_soma_diminui_w	= 'S') then
			vl_total_imposto_nota_w	:= vl_total_imposto_nota_w + vl_tributo_w;
		elsif	(ie_soma_diminui_w	= 'D') then 
			vl_total_imposto_nota_w	:= vl_total_imposto_nota_w - vl_tributo_w;
		end if;
	end if;
	end;
end loop;
close C01;

atualiza_total_nota_fiscal(	nr_seq_nota_p,
				nm_usuario_p);

select	count(1)
into	qt_venctos_w
from	nota_fiscal_venc
where	nr_sequencia	= nr_seq_nota_p;

if	(ie_forma_pagamento_w <> 10) then
	select	nvl(vl_total_nota,0)
	into	vl_total_nota_w
	from	nota_fiscal
	where	nr_sequencia	= nr_seq_nota_p;

	select	nvl(sum(vl_vencimento),0)
	into	vl_vencimento_w
	from	nota_fiscal_venc
	where	nr_sequencia	= nr_seq_nota_p;

	if	(nvl(vl_vencimento_w,0) <> nvl(vl_total_nota_w,0)) then
		begin
		if	(qt_venctos_w = 1) then
			update	nota_fiscal_venc
			set	vl_vencimento	= vl_vencimento + vl_total_nota_w - vl_vencimento_w
			where	nr_sequencia	= nr_seq_nota_p;
		elsif	(qt_venctos_w > 1) then
			update	nota_fiscal_venc
			set	vl_vencimento	= vl_vencimento + vl_total_nota_w - vl_vencimento_w
			where	nr_sequencia	= nr_seq_nota_p
			and	dt_vencimento	= dt_emissao_w;
		end if;
		end;
	end if;
end if;

end gerar_imposto_nfse;
/
