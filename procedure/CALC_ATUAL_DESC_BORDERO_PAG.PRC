create or replace
procedure CALC_ATUAL_DESC_BORDERO_PAG
			(nr_sequencia_p	number,
			nr_titulo_p	number,
			dt_prev_pagto_p	date,
			nm_usuario_p	varchar2,
			ie_tipo_p	varchar2)
			is
/* ie_tipo_p
A	Forma antiga WCP
N	Forma nova DBP da tabela bordero_tit_pagar
*/
vl_desconto_w	number(15,2) := 0;	
		
begin

Calcular_Desc_Ant_Pagto(nr_titulo_p, dt_prev_pagto_p, vl_desconto_w, nm_usuario_p);

if	(nvl(vl_desconto_w,0) <> 0) then

	if	(ie_tipo_p = 'A') then

		update	titulo_pagar
		set	vl_desconto_bordero 	= vl_desconto_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_titulo		= nr_titulo_p;
	
	elsif	(ie_tipo_p = 'N') then
	
		update	bordero_tit_pagar
		set	vl_desconto_bordero 	= vl_desconto_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_sequencia		= nr_sequencia_p;
	end if;

end if;

commit;

end CALC_ATUAL_DESC_BORDERO_PAG;
/