create or replace
procedure calc_juros_baixa_proporcional( 	nr_titulo_p	in	number,
						nr_seq_baixa_p	in	number,
						vl_juros_p	out	number) is 

ie_apropriar_juros_multa_w	titulo_receber_liq.ie_apropriar_juros_multa%type;
vl_recebido_w			titulo_receber_liq.vl_recebido%type;
vl_titulo_w			titulo_receber.vl_saldo_titulo%type;
vl_juros_calc_proporcional_w	number(15,2);
vl_juros_total_w		number(15,2);
vl_juros_cobrados_w		number(15,2);
vl_apropriado_juros_w		number(15,2);

begin
/*Somente calcular se tiver titulo e sequencia de baixa*/
if 	(nvl(nr_titulo_p,0) <> 0) and (nvl(nr_seq_baixa_p,0) <> 0) then

	select	nvl(max(a.ie_apropriar_juros_multa),'N'),
		nvl(max(a.vl_recebido),0),
		max(b.vl_titulo)
	into	ie_apropriar_juros_multa_w,
		vl_recebido_w,
		vl_titulo_w
	from	titulo_receber_liq a,
		titulo_receber b
	where	a.nr_titulo	= b.nr_titulo
	and	a.nr_titulo	= nr_titulo_p
	and	a.nr_sequencia	= nr_seq_baixa_p;

	/*Se estiver como Sim, significa que o usuario selecionou Sim na hora da baixa e deve calcular proporcional! Somente para baixas com valor recebido, se for desconto nao calcular*/
	if 	(nvl(ie_apropriar_juros_multa_w,'N') = 'S') and (vl_recebido_w <> 0) then
		
		select	nvl(sum(vl_juros),0)
		into	vl_juros_cobrados_w
		from	titulo_receber_liq
		where	nr_titulo = nr_titulo_p;

		vl_juros_total_w := obter_valor_juros_tit_integral(nr_titulo_p,nr_seq_baixa_p);
		
		select	nvl(sum(a.vl_item), 0)
		into	vl_apropriado_juros_w
		from	pls_segurado_mensalidade a
		where	a.nr_titulo		= nr_titulo_p
		and	a.ie_tipo_item		= 23
		and	a.ie_situacao		= 'A';
		
		/*Aqui faz  o calculo proporcional de juros referente ao valor baixado*/
		vl_juros_calc_proporcional_w	:= vl_recebido_w * (vl_juros_total_w/vl_titulo_w);
		
		if	((vl_apropriado_juros_w+vl_juros_cobrados_w+vl_juros_calc_proporcional_w) > vl_juros_total_w) then
			if	((vl_apropriado_juros_w+vl_juros_cobrados_w) < vl_juros_total_w) then
				vl_juros_calc_proporcional_w := vl_juros_total_w - (vl_apropriado_juros_w+vl_juros_cobrados_w);
			else
				vl_juros_calc_proporcional_w := 0;
			end if;
		elsif	((vl_apropriado_juros_w+vl_juros_cobrados_w) = vl_juros_total_w) then
			vl_juros_calc_proporcional_w := 0;
		end if;
	end if;
end if;

vl_juros_p	:= nvl(vl_juros_calc_proporcional_w,0);

end calc_juros_baixa_proporcional;
/
