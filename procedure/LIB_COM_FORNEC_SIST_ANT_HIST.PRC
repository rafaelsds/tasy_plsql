create or replace
procedure lib_com_fornec_sist_ant_hist(		nr_sequencia_p		number,
					nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	update	com_fornec_sist_ant_hist
	set	dt_liberacao = sysdate,
		nm_usuario_lib = nm_usuario_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
	commit;
	
	end;
end if;

end lib_com_fornec_sist_ant_hist;
/