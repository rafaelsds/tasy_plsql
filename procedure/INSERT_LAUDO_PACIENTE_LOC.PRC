CREATE OR REPLACE PROCEDURE INSERT_LAUDO_PACIENTE_LOC(NR_ATENDIMENTO_P       NUMBER,
							        NR_PRESCRICAO_P        NUMBER,
							        CD_SETOR_ATENDIMENTO_P NUMBER,
							        CD_PESSOA_ENTREGA_P    VARCHAR2,
							        IE_TIPO_LOCAL_P        VARCHAR2,
							        CD_SETOR_registro_P    NUMBER,
							        NR_SEQ_LAUDOS_P        VARCHAR2,
							        NM_USUARIO_P           VARCHAR2,
							        CD_PESSOA_FISICA_P     VARCHAR2,
							        CD_PESSOA_RECEBIDO_P   VARCHAR2,
							        DS_SENHA_P             VARCHAR2,
							        DS_OBSERVACAO_P        VARCHAR2,
							        NM_EXTERNO_P           VARCHAR2,
							        IE_GRAVAR_SETOR_P      VARCHAR2,
							        DT_RECEBIMENTO_P       DATE,
							        IE_COM_PROTOCOLO_P     VARCHAR2) IS

ds_possib_w              VARCHAR2(6000);
qt_controle_w            NUMBER(10);
qt_pos_separador_w NUMBER(10);
nr_seq_laudo_w        NUMBER(10);

nr_agrupamento_w             NUMBER(10);
ie_tipo_atendimento_w       NUMBER(3) := 0;
cd_estabelecimento_w       NUMBER(4);
ie_param_nove_w               VARCHAR2(1);
ie_data_retirada_w              VARCHAR(3);
ie_tipo_local_w                    VARCHAR2(3);
nr_seq_laudo_agrupado_w NUMBER(10);
cd_setor_atendimento_w    NUMBER(5);
dt_retirada_w                      DATE := NULL;
nr_seq_loc_w                      NUMBER(7);

CURSOR c01 IS
  SELECT nr_sequencia
  FROM   laudo_paciente
  WHERE  nr_agrupamento = nr_agrupamento_w
  AND    nr_sequencia <> nr_seq_laudo_w
  AND    nr_agrupamento_w > 0;

BEGIN
  ds_possib_w := nr_seq_laudos_p || ',';

  IF (INSTR(ds_possib_w,'(') > 0 )
  AND (INSTR(ds_possib_w,')') > 0 ) THEN
    ds_possib_w := SUBSTR(nr_seq_laudos_p,(INSTR(nr_seq_laudos_p,'(')+1),(INSTR(nr_seq_laudos_p,')')-2));
  END IF;

  qt_controle_w      := 0;
  qt_pos_separador_w := INSTR(ds_possib_w,',');

  IF (qt_pos_separador_w = 0) THEN
    qt_pos_separador_w := -1;
  END IF;


  WHILE  (qt_pos_separador_w >= 0)
  AND (qt_controle_w < 1000) LOOP
  BEGIN
    IF(qt_pos_separador_w = 0) THEN
      nr_seq_laudo_w     := TO_NUMBER(ds_possib_w);
      qt_pos_separador_w := -1;
    ELSE
      nr_seq_laudo_w := TO_NUMBER(SUBSTR(ds_possib_w,1,qt_pos_separador_w-1));
    END IF;


    IF (nr_atendimento_p <> 0) THEN
    BEGIN
      SELECT c.ie_tipo_atendimento,
             c.cd_estabelecimento
      INTO   ie_tipo_atendimento_w,
             cd_estabelecimento_w
      FROM   atendimento_paciente c
      WHERE  c.nr_atendimento = nr_atendimento_p;

      EXCEPTION
      WHEN no_data_found THEN
      BEGIN
        ie_tipo_atendimento_w := 0;
      END;
    END;
    END IF;

    IF (nr_prescricao_p <> 0) AND (ie_tipo_atendimento_w = 0) THEN
    BEGIN
      SELECT NVL(MAX(c.ie_tipo_atendimento),0),
             NVL(MAX(c.cd_estabelecimento),0)
      INTO   ie_tipo_atendimento_w,
             cd_estabelecimento_w
      FROM   prescr_medica b,
             atendimento_paciente c
      WHERE  c.nr_atendimento = b.nr_atendimento
      AND    b.nr_prescricao  = nr_prescricao_p;

      EXCEPTION
      WHEN no_data_found THEN
      BEGIN
        ie_tipo_atendimento_w := 0;
      END;
    END;
    END IF;

    SELECT  NVL(obter_valor_param_usuario(994, 9, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N')
    INTO  ie_param_nove_w
    FROM  dual;

    IF ie_param_nove_w = 'N' THEN
    SELECT MAX(b.ie_tipo_local)
    INTO   ie_tipo_local_w
    FROM   regra_loc_laudo b,
           prescr_medica a
    WHERE  a.cd_setor_entrega = b.cd_setor_entrega
    AND    (NVL(b.ie_tipo_atendimento, ie_tipo_atendimento_w) = ie_tipo_atendimento_w
              OR ie_tipo_atendimento_w  = 0)
    AND    a.nr_prescricao = nr_prescricao_p;
    END IF;

    SELECT NVL(obter_valor_param_usuario(994, 8, obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_w),'N')
    INTO   ie_data_retirada_w
    FROM   dual;

    IF (ie_tipo_local_w IS NULL) THEN
      ie_tipo_local_w := ie_tipo_local_p;
    END IF;

    IF (ie_tipo_local_p = '8')
      OR (ie_gravar_setor_p = 'S') THEN
      cd_setor_atendimento_w := cd_setor_atendimento_p;
    END IF;

    IF (ie_data_retirada_w = 'S') THEN
      dt_retirada_w := SYSDATE;
    ELSIF (ie_data_retirada_w = 'N')
      AND (dt_recebimento_p IS NOT NULL) THEN
      dt_retirada_w := dt_recebimento_p;
    END IF;

    SELECT NVL(MAX(nr_seq_loc), 0) + 1
    INTO   nr_seq_loc_w
    FROM   laudo_paciente_loc
    WHERE  nr_sequencia = nr_seq_laudo_w;

    INSERT INTO  laudo_paciente_loc
       (nr_sequencia,
        nr_seq_loc,
        ie_tipo_local,
        dt_atualizacao,
        nm_usuario,
        cd_setor_atendimento,
        dt_retirada,
        cd_pessoa_fISica,
        ds_observacao,
        cd_setor_registro,
        nm_pessoa_fISica,
        nm_usuario_nrec,
        cd_pessoa_recebido,
        ds_senha,
        dt_atualizacao_nrec,
        cd_pessoa_entrega,
        ie_com_protocolo)
    VALUES
       (nr_seq_laudo_w,
        nr_seq_loc_w,
        ie_tipo_local_w,
        SYSDATE,
        nm_usuario_p,
        DECODE(cd_setor_atendimento_w, 0, NULL, cd_setor_atendimento_w),
        dt_retirada_w,
        cd_pessoa_fISica_p,
        ds_observacao_p,
        DECODE(cd_setor_registro_p, 0, NULL, cd_setor_registro_p),
        SUBSTR(nm_externo_p,1,60),
        nm_usuario_p,
        cd_pessoa_recebido_p,
        ds_senha_p,
        SYSDATE,
        cd_pessoa_entrega_p,
        ie_com_protocolo_p);

    OPEN c01;
    LOOP
    FETCH c01 INTO
      nr_seq_laudo_agrupado_w;
    EXIT WHEN c01%notfound;
      BEGIN
        SELECT NVL(MAX(nr_seq_loc), 0) + 1
        INTO   nr_seq_loc_w
        FROM   laudo_paciente_loc
        WHERE  nr_sequencia = nr_seq_laudo_w;

    INSERT INTO laudo_paciente_loc
       (nr_sequencia,
        nr_seq_loc,
        ie_tipo_local,
        dt_atualizacao,
        nm_usuario,
        cd_setor_atendimento,
        dt_retirada,
        cd_pessoa_fISica,
        ds_observacao,
        cd_setor_registro,
        nm_pessoa_fISica,
        nm_usuario_nrec,
        cd_pessoa_recebido,
        ds_senha,
        dt_atualizacao_nrec,
        cd_pessoa_entrega,
        ie_com_protocolo)
    VALUES
	   (nr_seq_laudo_agrupado_w,
        nr_seq_loc_w,
        ie_tipo_local_w,
        SYSDATE,
        nm_usuario_p,
        DECODE(cd_setor_atendimento_w, 0, NULL, cd_setor_atendimento_w),
        dt_retirada_w,
        cd_pessoa_fISica_p,
        ds_observacao_p,
        DECODE(cd_setor_registro_p, 0, NULL, cd_setor_registro_p),
        SUBSTR(nm_externo_p,1,60),
        nm_usuario_p,
        cd_pessoa_recebido_p,
        ds_senha_p,
        SYSDATE,
        cd_pessoa_entrega_p,
        ie_com_protocolo_p);

      atualizar_status_exec_proced(nr_seq_laudo_agrupado_w,nm_usuario_p,'N');

    END;
    END loop;
    CLOSE c01;

    atualizar_status_exec_proced(nr_seq_laudo_w,nm_usuario_p,'N');

    IF(qt_pos_separador_w > 0 ) THEN
      ds_possib_w        := SUBSTR(ds_possib_w,qt_pos_separador_w+1,length(ds_possib_w));
      qt_pos_separador_w := INSTR(ds_possib_w,',');
    END IF;

    qt_controle_w := qt_controle_w + 1;
  END;
  END LOOP;

  COMMIT;

END INSERT_LAUDO_PACIENTE_LOC;
/
