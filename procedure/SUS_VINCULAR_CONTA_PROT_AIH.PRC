CREATE OR REPLACE
PROCEDURE SUS_VINCULAR_CONTA_PROT_AIH	
			(	nr_interno_conta_p	Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number)	is

ie_tipo_protocolo_w	Number(2);
nr_seq_protocolo_w	Number(10)	:= 0;
cd_convenio_w		Number(5);

BEGIN

select	cd_convenio_parametro
into	cd_convenio_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

select	nvl(a.ie_tipo_protocolo_aih,1)
into	ie_tipo_protocolo_w
from	parametro_faturamento a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

select	nvl(max(nr_seq_protocolo),0)
into	nr_seq_protocolo_w
from	protocolo_convenio
where	ie_tipo_protocolo	= ie_tipo_protocolo_w
and	cd_convenio		= cd_convenio_w
and	ie_status_protocolo	= 1;

if	(nr_seq_protocolo_w	> 0) then
	update	conta_paciente
	set	nr_seq_protocolo	= nr_seq_protocolo_w,
		nm_usuario		= nm_usuario_p
	where	nr_interno_conta	= nr_interno_conta_p
	and	ie_status_acerto	= 2;
end if;

commit;

END SUS_VINCULAR_CONTA_PROT_AIH;
/