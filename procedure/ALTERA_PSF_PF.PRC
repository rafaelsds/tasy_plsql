create or replace
procedure altera_psf_pf(	cd_pessoa_fisica_p	varchar2,
				dt_final_habit_p	date,
				nr_sequencia_p		number,
				nm_usuario_p		varchar2) is 

begin
if	(cd_pessoa_fisica_p is not null) and
	(dt_final_habit_p is not null) and
	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	psf_pessoa_fisica
	set	dt_final_habit = dt_final_habit_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	nr_sequencia <> nr_sequencia_p
	and      dt_final_habit is null;
	end;
end if;
commit;
end altera_psf_pf;
/