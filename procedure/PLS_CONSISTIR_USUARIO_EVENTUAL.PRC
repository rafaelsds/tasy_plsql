create or replace
procedure pls_consistir_usuario_eventual
			(	nr_seq_plano_p		Number,
				ie_tipo_inclusao_p	Varchar2,
				nr_seq_congenere_p	Number,
				nm_usuario_p		Varchar2) is 

/*
ie_tipo_inclusao_p
	'C' = Operadora cong�nere
	'T' = Interc�mbio
*/

ie_tipo_operacao_w	Varchar2(3);
qt_produto_congenere_w	Number(10);

begin

if	(nr_seq_plano_p is not null) then
	select	max(ie_tipo_operacao)
	into	ie_tipo_operacao_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_p;

	if	(ie_tipo_operacao_w	<> ie_tipo_inclusao_p) then
		if	(ie_tipo_inclusao_p = 'T') then
			wheb_mensagem_pck.exibir_mensagem_abort(262305);
			/* Mensagem: Deve ser selecionado um produto de interc�mbio! Favor verificar. */
		elsif	(ie_tipo_inclusao_p = 'C') then
			wheb_mensagem_pck.exibir_mensagem_abort(262306);
			/* Mesagem: Deve ser selecionado um produto para operadora cong�nere! Favor verificar. */
		end if;
	end if;

	if	(ie_tipo_inclusao_p = 'C') then
		select	count(*)
		into	qt_produto_congenere_w
		from	pls_plano
		where	nr_sequencia		= nr_seq_plano_p
		and	nr_seq_congenere	= nr_seq_congenere_p
		and	ie_tipo_operacao	= 'C';
		
		if	(qt_produto_congenere_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(262307);
			/* Mesagem: Produto n�o cadastrado para a operadora cong�nere selecionada! Favor verificar. */
		end if;
	end if;
else
	wheb_mensagem_pck.exibir_mensagem_abort(262308);
	/* Mesagem: Deve ser selecionado um produto! Favor verificar. */
end if;

end pls_consistir_usuario_eventual;
/