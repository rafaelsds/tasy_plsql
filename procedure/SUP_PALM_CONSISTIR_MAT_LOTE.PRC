create or replace 
procedure sup_palm_consistir_mat_lote(		nr_prescricao_p		Number,
					cd_material_p		Number,
					qt_material_p		Number,
					nr_seq_lote_fornec_p	Number,
					cd_estabelecimento_p	Number,
					cd_local_estoque_p	Number,
					dt_entrada_unidade_p	date,
					cd_tipo_baixa_p		Number,		
					nr_seq_lote_p		Number,	
					nm_usuario_p            varchar2,
					ds_erro_p	out 		varchar2) is 

nr_atendimento_w		Number(10,0);					
ie_conta_aberta_w		varchar2(1):= 'N';
ie_fim_conta_w		varchar2(1);
dt_fim_conta_w		date;
ie_local_valido_w		varchar2(1);
ie_baixa_estoque_pac_w	varchar2(1);
ie_estoque_disp_w		varchar2(1);
ie_atualiza_estoque_w	varchar2(1);
ie_saldo_estoque_w	varchar2(1);
nr_sequencia_w		number(10,0);
cd_setor_atendimento_w	number(5,0);
dt_prescricao_w		date;	
nr_seq_atepacu_w		number(10,0);
cd_unidade_medida_w	varchar2(30);
cd_cgc_fornec_w		varchar2(14);
ie_consignado_w		varchar2(1);
ds_erro_w		varchar2(255);

					
begin

ds_erro_w:= '';

select 	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

select 	max(ie_fim_conta), 
	max(dt_fim_conta)
into	ie_fim_conta_w,
	dt_fim_conta_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

select	substr(obter_dados_material(cd_material_p,'UME'),1,5)
into	cd_unidade_medida_w
from	dual;

select	max(cd_setor_atendimento),
	max(nr_seq_interno)
into	cd_setor_atendimento_w,
	nr_seq_atepacu_w
from 	atend_paciente_unidade
where 	nr_atendimento = nr_atendimento_w
and 	dt_entrada_unidade = dt_entrada_unidade_p;


select 	max(ie_atualiza_estoque)
into	ie_atualiza_estoque_W
from 	ap_lote
where 	nr_sequencia = nr_seq_lote_p;

if      (ie_atualiza_estoque_w is null) then
       	select	nvl(max(ie_atualiza_estoque),'S')
	into	ie_atualiza_estoque_w
	from	tipo_baixa_prescricao
	where	ie_prescricao_devolucao = 'P'
	and 	cd_tipo_baixa = cd_tipo_baixa_p
	and 	ie_situacao = 'A';

end if;


if	(ie_fim_conta_w <> 'F') and (dt_fim_conta_w is null) then
	ie_conta_aberta_w:= 'S';
else
	ds_erro_w:=  WHEB_MENSAGEM_PCK.get_texto(279983);
end if;

if	(ie_conta_aberta_w = 'S') and (ds_erro_w is null) then
	
	obter_local_valido(cd_estabelecimento_p, cd_local_estoque_p, cd_material_p, null, ie_local_valido_w);
	
	select	obter_se_baixa_estoque_pac(cd_setor_atendimento_w, cd_material_p,null,0) 
	into	ie_baixa_estoque_pac_w
	from 	dual;

	
	SELECT	NVL(ie_consignado,'0')
	INTO	ie_consignado_w
	FROM	material
	WHERE	cd_material = cd_material_p;
	IF	(ie_consignado_w <> '0') THEN
		BEGIN
		IF	(NVL(nr_seq_lote_fornec_p, 0) > 0) THEN
			SELECT	cd_cgc_fornec
			INTO	cd_cgc_fornec_w
			FROM	material_lote_fornec
			WHERE	nr_sequencia = nr_seq_lote_fornec_p;
		ELSE
			cd_cgc_fornec_w	:= obter_fornecedor_regra_consig(
						cd_estabelecimento_p,
						cd_material_p,
						'1');
		END IF;
		END;
	END IF;
	
	
	obter_disp_estoque(cd_material_p, cd_local_estoque_p, cd_estabelecimento_p, 0, qt_material_p, cd_cgc_fornec_w, ie_saldo_estoque_w);

		
	if	((ie_baixa_estoque_pac_w = 'N') or
	                (ie_saldo_estoque_w = 'S') 	or
	                (ie_atualiza_estoque_w = 'N')) 	then
		ie_estoque_disp_w:= 'S';
	else
		ie_estoque_disp_w:= 'N';
	end if;
	
	if	(ie_atualiza_estoque_w = 'S') then
		begin
		if  	not (ie_local_valido_w = 'S') then
			ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(279985);
		elsif	not (ie_estoque_disp_w = 'S') then
			ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(279986);
		end if;
		end;
	end if;  
	
end if;
	
ds_erro_p:= ds_erro_w;

end sup_palm_consistir_mat_lote;
/
