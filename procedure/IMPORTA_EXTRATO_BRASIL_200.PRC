CREATE OR REPLACE PROCEDURE
IMPORTA_EXTRATO_BRASIL_200(	nr_seq_extrato_p	number) is

nr_seq_conta_W		number(10);
vl_saldo_inicial_w	number(17,2);
vl_saldo_final_w	number(17,2);
dt_saldo_inicial_w	date;
dt_saldo_final_w	date;
nr_seq_extrato_w	number(10);
nr_seq_conta_nova_w	number(10);
nr_conta_ant_w		varchar2(13)	:= '-1';
ie_digito_ant_w		varchar2(1);
ie_deb_cred_saldo_ini_w	varchar2(1);
ie_deb_cred_saldo_fin_w	varchar2(1);
cd_banco_w		number(3);

/* Registro detalhe */
nr_documento_w		varchar2(15);
dt_lancamento_w		varchar2(8);
vl_lancamento_w		varchar2(18);
ie_deb_cred_w		varchar2(1);
nr_conta_w		varchar2(11);
digito_conta_w		varchar2(1);
ds_historico_w		varchar2(25);
nr_lote_w		varchar2(5);
ie_natureza_w		varchar2(3);
cd_historico_w		varchar2(4);
cd_categoria_lanc_w	varchar2(3);

/* Saldos inicial e final sem lan�amentos */
qt_lancamento_w		number(10);
nr_sequencia_w		number(10);
nr_seq_final_w		number(10);

--Detalhe - 03 Registro de lan�amento
cursor c01 is
Select	substr(ds_conteudo,136,15) nr_documento,
	substr(ds_conteudo,182,8) dt_lancto,
	substr(ds_conteudo,87,18) vl_lancto,
	substr(ds_conteudo,43,3) cd_categoria_lanc,
	substr(ds_conteudo,30,11) nr_conta,
	substr(ds_conteudo,41,1) digito_conta,
	substr(ds_conteudo,50,25) ds_historico,
	substr(ds_conteudo,111,5) nr_lote,
	substr(ds_conteudo,120,3) ie_natureza,
	substr(ds_conteudo,46,4) cd_historico
from	W_interf_concil
where	substr(ds_conteudo,1,1)	= '1'
and	substr(ds_conteudo,42,1) = '1'
and	nr_seq_conta		= nr_seq_conta_W;

--Detalhe - 02 Registro de saldo anterior
cursor	c02 is
Select	a.nr_sequencia,
	to_date(substr(ds_conteudo,81,6),'dd/mm/yy') dt_lancto,
	somente_numero(substr(ds_conteudo,87,18)) / 100 vl_lancto,
	substr(ds_conteudo,105,1) ie_deb_cred,
	substr(ds_conteudo,30,11) nr_conta,
	substr(ds_conteudo,41,1) digito_conta
from	W_interf_concil a
where	substr(ds_conteudo,1,1)	= '1'
and	substr(ds_conteudo,42,1) = '0'
and	a.nr_seq_conta		= nr_seq_conta_W;

begin

Select	max(b.nr_seq_conta),
	max(a.cd_banco)
into	nr_seq_conta_w,
	cd_banco_w
from	banco_estabelecimento a,
	banco_extrato b
where	b.nr_seq_conta	= a.nr_sequencia
and	b.nr_sequencia	= nr_seq_extrato_p;

open c01;
loop
fetch c01 into
	nr_documento_w,
	dt_lancamento_w,
	vl_lancamento_w,
	cd_categoria_lanc_w,
	nr_conta_w,
	digito_conta_w,
	ds_historico_w,
	nr_lote_w,
	ie_natureza_w,
	cd_historico_w;
exit when c01%notfound;

	if	(nr_conta_ant_w	= '-1') or
		(nr_conta_ant_w <> nr_conta_w) or
		(ie_digito_ant_w <> digito_conta_w) then

		nr_conta_ant_w	:= nr_conta_w;
		ie_digito_ant_w := digito_conta_w;

		select	to_date(substr(ds_conteudo,81,6),'dd/mm/yy') dt_lancto,
			somente_numero(substr(ds_conteudo,87,18)) / 100 vl_lancto,
			substr(ds_conteudo,105,1) ie_cred_deb
		into	dt_saldo_inicial_w,
			vl_saldo_inicial_w,
			ie_deb_cred_saldo_ini_w
		from	w_interf_concil a
		where	substr(ds_conteudo,41,1)	= digito_conta_w
		and	substr(ds_conteudo,30,11)	= nr_conta_w
		and	substr(ds_conteudo,1,1)		= '1'
		and	substr(ds_conteudo,42,1) 	= '0'
		and	a.nr_seq_conta			= nr_seq_conta_w;

		select	to_date(substr(a.ds_conteudo,81,6),'dd/mm/yy') dt_lancto,
			somente_numero(substr(a.ds_conteudo,87,18)) / 100 vl_lancto,
			substr(a.ds_conteudo,105,1) ie_cred_deb
		into	dt_saldo_final_w,
			vl_saldo_final_w,
			ie_deb_cred_saldo_fin_w
		from	w_interf_concil a
		where	substr(ds_conteudo,41,1)	= digito_conta_w
		and	substr(ds_conteudo,30,11)	= nr_conta_w
		and	substr(ds_conteudo,1,1)		= '1'
		and	substr(ds_conteudo,42,1) 	= '2'
		and	a.nr_seq_conta			= nr_seq_conta_w;

		select	max(a.nr_sequencia)
		into	nr_seq_conta_nova_w
		from	banco_estabelecimento a
		where	(a.nr_sequencia = nr_seq_conta_w or
			not exists
			(select	1
			from	banco_estabelecimento x
			where	x.ie_digito_conta	= a.ie_digito_conta
			and	x.cd_conta		= a.cd_conta
			and	x.nr_sequencia		= nr_seq_conta_w))
		and	somente_numero(a.cd_conta)||somente_numero(a.ie_digito_conta) = somente_numero(nr_conta_w)||somente_numero(nvl(trim(digito_conta_w),a.ie_digito_conta))
		and	cd_banco				= cd_banco_w;

		if (nr_seq_conta_nova_w is null) then
		
			select	max(a.nr_sequencia)
			into	nr_seq_conta_nova_w
			from	banco_estabelecimento a
			where	(a.nr_sequencia = nr_seq_conta_w or
				not exists
				(select	1
				from	banco_estabelecimento x
				where	x.ie_digito_conta	= a.ie_digito_conta
				and	x.cd_conta		= a.cd_conta
				and	x.nr_sequencia		= nr_seq_conta_w))
			and	somente_numero(a.ie_digito_conta)	= somente_numero(nvl(trim(digito_conta_w),a.ie_digito_conta))
			and	somente_numero(a.cd_conta)		= somente_numero(nr_conta_w)
			and	cd_banco				= cd_banco_w;
			
		end if;
		
		if	(nr_seq_conta_nova_w	is null) and
			(digito_conta_w = ' ') then

			select	max(a.nr_sequencia)
			into	nr_seq_conta_nova_w
			from	banco_estabelecimento a
			where	(a.nr_sequencia = nr_seq_conta_w or
				not exists
				(select	1
				from	banco_estabelecimento x
				where	x.ie_digito_conta	= a.ie_digito_conta
				and	x.cd_conta		= a.cd_conta
				and	x.nr_sequencia		= nr_seq_conta_w))
			and	somente_numero(a.ie_digito_conta)	= somente_numero(nvl(digito_conta_w,a.ie_digito_conta))
			and	somente_numero(a.cd_conta)		= somente_numero(nr_conta_w)
			and	cd_banco				= cd_banco_w;

		end if;

		if	(ie_deb_cred_saldo_ini_w	= 'D') then
			vl_saldo_inicial_w	:= nvl(vl_saldo_inicial_w,0) * -1;
		end if;

		if	(ie_deb_cred_saldo_fin_w	= 'D') then
			vl_saldo_final_w	:= nvl(vl_saldo_final_w,0) * -1;
		end if;

		if	(nr_seq_conta_w	= nr_seq_conta_nova_w) then

			nr_seq_extrato_w	:= nr_seq_extrato_p;
			nr_seq_conta_nova_w	:= nr_seq_conta_w;

			update	banco_extrato
			set	vl_saldo_inicial	= vl_saldo_inicial_w,
				vl_saldo_final		= vl_saldo_final_w,
				dt_inicio		= dt_saldo_inicial_w,
				dt_final		= dt_saldo_final_w,
				dt_atualizacao		= sysdate
			where	nr_sequencia		= nr_seq_extrato_w;

		elsif	(nr_seq_conta_nova_w	is not null) then

			select	banco_extrato_seq.nextval
			into	nr_seq_extrato_w
			from	dual;

			insert	into banco_extrato
				(dt_atualizacao,
				dt_atualizacao_nrec,
				dt_final,
				dt_importacao,
				dt_inicio,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_conta,
				nr_sequencia,
				vl_saldo_final,
				vl_saldo_inicial)
			values	(sysdate,
				sysdate,
				dt_saldo_final_w,
				sysdate,
				dt_saldo_inicial_w,
				'Tasy',
				'Tasy',
				nr_seq_conta_nova_w,
				nr_seq_extrato_w,
				vl_saldo_final_w,
				vl_saldo_inicial_w);

		end if;

	end if;

	if	(nr_seq_conta_nova_w	is not null) then
	
		if	(somente_numero(cd_categoria_lanc_w) > 200) then
		
			ie_deb_cred_w	:= 'C';
			
		else
		
			ie_deb_cred_w	:= 'D';
			
		end if;

		insert	into banco_extrato_lanc
			(nr_documento,
			dt_movimento,
			vl_lancamento,
			ie_deb_cred,
			nr_seq_extrato,
			nr_sequencia,
			nm_usuario,
			ie_conciliacao,
			dt_atualizacao,
			ds_historico,
			nr_lote,
			ie_natureza,
			cd_historico)
		values	(nr_documento_w,
			to_date(dt_lancamento_w,'dd/mm/yyyy'),
			somente_numero(vl_lancamento_w) / 100,
			ie_deb_cred_w,
			nr_seq_extrato_w,
			banco_extrato_lanc_seq.nextVal,
			'Tasy',
			'N',
			sysdate,
			ds_historico_w,
			nr_lote_w,
			ie_natureza_w,
			cd_historico_w);

	end if;

end loop;
close c01;

/* Importar os saldos inicial e final das contas que n�o possuem lan�amento */
open	c02;
loop
fetch	c02 into
	nr_sequencia_w,
	dt_saldo_inicial_w,
	vl_saldo_inicial_w,
	ie_deb_cred_saldo_ini_w,
	nr_conta_w,
	digito_conta_w;
exit	when c02%notfound;

	select	min(a.nr_sequencia)
	into	nr_seq_final_w
	from	w_interf_concil a
	where	a.nr_sequencia		> nr_sequencia_w
	and	substr(a.ds_conteudo,1,1)	= '1'
	and	substr(a.ds_conteudo,42,1)	= '2'
	and	a.nr_seq_conta		= nr_seq_conta_W;

	select	count(*)
	into	qt_lancamento_w
	from	w_interf_concil a
	where	a.nr_sequencia		between nr_sequencia_w and nr_seq_final_w
	and	substr(a.ds_conteudo,1,1)	= '1'
	and	substr(ds_conteudo,42,1)	= '1'
	and	a.nr_seq_conta		= nr_seq_conta_W;

	if	(qt_lancamento_w	= 0) then

		select	max(a.nr_sequencia)
		into	nr_seq_conta_nova_w
		from	banco_estabelecimento a
		where	(a.nr_sequencia = nr_seq_conta_w or
			not exists
			(select	1
			from	banco_estabelecimento x
			where	x.ie_digito_conta	= a.ie_digito_conta
			and	x.cd_conta		= a.cd_conta
			and	x.nr_sequencia		= nr_seq_conta_w))
		and	somente_numero(a.ie_digito_conta)	= somente_numero(nvl(trim(digito_conta_w),a.ie_digito_conta))
		and	somente_numero(a.cd_conta)		= somente_numero(nr_conta_w)
		and	cd_banco				= cd_banco_w;

		if	(nr_seq_conta_nova_w	is null) and
			(digito_conta_w		= ' ') then

			select	max(a.nr_sequencia)
			into	nr_seq_conta_nova_w
			from	banco_estabelecimento a
			where	(a.nr_sequencia = nr_seq_conta_w or
				not exists
				(select	1
				from	banco_estabelecimento x
				where	x.ie_digito_conta	= a.ie_digito_conta
				and	x.cd_conta		= a.cd_conta
				and	x.nr_sequencia		= nr_seq_conta_w))
			and	somente_numero(a.ie_digito_conta)	= somente_numero(nvl(digito_conta_w,a.ie_digito_conta))
			and	somente_numero(a.cd_conta)		= somente_numero(nr_conta_w)
			and	cd_banco				= cd_banco_w;

		end if;

		select	to_date(substr(a.ds_conteudo,81,6),'dd/mm/yy') dt_lancto,
			somente_numero(substr(a.ds_conteudo,87,18)) / 100 vl_lancto,
			substr(a.ds_conteudo,105,1) ie_deb_cred
		into	dt_saldo_final_w,
			vl_saldo_final_w,
			ie_deb_cred_saldo_fin_w
		from	w_interf_concil a
		where	a.nr_sequencia	= nr_seq_final_w;

		if	(ie_deb_cred_saldo_ini_w	= 'D') then
			vl_saldo_inicial_w	:= nvl(vl_saldo_inicial_w,0) * -1;
		end if;

		if	(ie_deb_cred_saldo_fin_w	= 'D') then
			vl_saldo_final_w	:= nvl(vl_saldo_final_w,0) * -1;
		end if;

		if	(nr_seq_conta_w	= nr_seq_conta_nova_w) then

			nr_seq_extrato_w	:= nr_seq_extrato_p;
			nr_seq_conta_nova_w	:= nr_seq_conta_w;

			update	banco_extrato
			set	vl_saldo_inicial	= vl_saldo_inicial_w,
				vl_saldo_final		= vl_saldo_final_w,
				dt_inicio		= dt_saldo_inicial_w,
				dt_final		= dt_saldo_final_w,
				dt_atualizacao		= sysdate
			where	nr_sequencia		= nr_seq_extrato_w;

		elsif	(nr_seq_conta_nova_w	is not null) and
			((nvl(vl_saldo_inicial_w,0) <> 0) or (nvl(vl_saldo_final_w,0) <> 0)) then

			select	banco_extrato_seq.nextval
			into	nr_seq_extrato_w
			from	dual;

			insert	into banco_extrato
				(dt_atualizacao,
				dt_atualizacao_nrec,
				dt_final,
				dt_importacao,
				dt_inicio,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_conta,
				nr_sequencia,
				vl_saldo_final,
				vl_saldo_inicial)
			values	(sysdate,
				sysdate,
				dt_saldo_final_w,
				sysdate,
				dt_saldo_inicial_w,
				'Tasy',
				'Tasy',
				nr_seq_conta_nova_w,
				nr_seq_extrato_w,
				vl_saldo_final_w,
				vl_saldo_inicial_w);

		end if;

	end if;

end	loop;
close	c02;

delete 	from w_interf_concil
where	nr_seq_conta	= nr_seq_conta_W;

commit;

end IMPORTA_EXTRATO_BRASIL_200;
/