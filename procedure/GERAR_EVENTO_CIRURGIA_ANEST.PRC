create or replace 
procedure gerar_evento_cirurgia_anest(	dt_evento_p	date,
					nr_cirurgia_p	number,
					nr_seq_evento_p	number,
					nm_usuario_p	varchar2,
					nr_seq_pepo_p	number) is

ie_finaliza_agentes_anest_w	varchar2(1);
ie_finaliza_terapia_w 		varchar2(1);
ie_finaliza_medicamentos_w	varchar2(1);
ie_finaliza_proced_adic_w	varchar(1);
nr_sequencia_w			number(10);
nr_sequencia_medic_w		number(10);
nr_sequencia_terap_w		number(10);
dt_final_adm_w			date;
ie_liberar_pepo_w		varchar2(1);
ie_calcula_tempos_w		varchar2(1);
dt_termino_w			date;
ie_liberar_adm_evento_w	varchar2(1);
ie_prescricao_anestesica_w	evento_cirurgia.ie_prescricao_anestesica%type;
nr_cirurgia_w			number(10);
ie_estrutura_pepo_w		varchar2(1);	
				
begin

obter_param_usuario(872, 158, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_estrutura_pepo_w);

select	nvl(max(ie_finaliza_terapia),'N'),
	nvl(max(ie_finaliza_agentes_anest),'N'),
	nvl(max(ie_finaliza_medicamentos),'N'),
	nvl(max(ie_libera_pepo),'N'),
	nvl(max(ie_calcula_tempos),'N'),
	nvl(max(ie_prescricao_anestesica),'N') ie_prescricao_anestesica,
	nvl(max(ie_finaliza_procedimento_adic),'N')
into	ie_finaliza_terapia_w,
	ie_finaliza_agentes_anest_w,
	ie_finaliza_medicamentos_w,
	ie_liberar_pepo_w,
	ie_calcula_tempos_w,
	ie_prescricao_anestesica_w,
	ie_finaliza_proced_adic_w
from	evento_cirurgia
where	nr_sequencia = nr_seq_evento_p;

select	nvl(max(ie_liberar_adm_evento),'N')
into	ie_liberar_adm_evento_w
from	parametros_pepo
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_finaliza_agentes_anest_w = 'S') then
		
	update	cirurgia_agente_anest_ocor
	set		dt_final_adm = dt_evento_p,
			dt_liberacao = decode(ie_liberar_adm_evento_w,'S',sysdate, dt_liberacao)
	where	nr_sequencia in (
		select	b.nr_sequencia
		from	cirurgia_agente_anestesico a,
			cirurgia_agente_anest_ocor b	
		where	a.nr_sequencia = b.nr_seq_cirur_agente
		and	(((a.nr_cirurgia = nr_cirurgia_p) and (nr_cirurgia_p is not null)) or ((a.nr_seq_pepo = nr_seq_pepo_p) and (nr_seq_pepo_p is not null)))
		and	a.ie_tipo = 1
		and	b.dt_final_adm is null
		and	nvl(a.ie_situacao,'A') = 'A'
		and	nvl(b.ie_situacao,'A') = 'A')
	and	nvl(ie_situacao,'A') = 'A'
	and	dt_liberacao is null;
	commit;
	
	if	(ie_liberar_adm_evento_w = 'S') then
		update	cirurgia_agente_anestesico
		set		dt_liberacao = sysdate
		where	(((nr_cirurgia = nr_cirurgia_p) and (nr_cirurgia_p is not null)) or ((nr_seq_pepo = nr_seq_pepo_p) and (nr_seq_pepo_p is not null)))
		and		dt_liberacao is null
		and		ie_tipo = 1;
		commit;
	end if;
	
end if;	

if	(ie_finaliza_terapia_w = 'S') then
		
	update	cirurgia_agente_anest_ocor
	set		dt_final_adm = dt_evento_p,
			dt_liberacao = decode(ie_liberar_adm_evento_w,'S',sysdate, dt_liberacao)
	where	nr_sequencia in (
		select	b.nr_sequencia
		from	cirurgia_agente_anestesico a,
			cirurgia_agente_anest_ocor b	
		where	a.nr_sequencia = b.nr_seq_cirur_agente
		and	(((a.nr_cirurgia = nr_cirurgia_p) and (nr_cirurgia_p is not null)) or ((a.nr_seq_pepo = nr_seq_pepo_p) and (nr_seq_pepo_p is not null)))
		and	a.ie_tipo = 2
		and	b.dt_final_adm is null
		and	nvl(a.ie_situacao,'A') = 'A'
		and	nvl(b.ie_situacao,'A') = 'A')
	and	nvl(ie_situacao,'A') = 'A'
	and	dt_liberacao is null;
	commit;
	
	if	(ie_liberar_adm_evento_w = 'S') then
		update	cirurgia_agente_anestesico
		set		dt_liberacao = sysdate
		where	(((nr_cirurgia = nr_cirurgia_p) and (nr_cirurgia_p is not null)) or ((nr_seq_pepo = nr_seq_pepo_p) and (nr_seq_pepo_p is not null)))
		and		dt_liberacao is null
		and		ie_tipo = 2;
		commit;
	end if;
end if;

if	(ie_finaliza_medicamentos_w = 'S') then
	
	update	cirurgia_agente_anest_ocor
	set		dt_final_adm = dt_evento_p,
			dt_liberacao = decode(ie_liberar_adm_evento_w,'S',sysdate, dt_liberacao)
	where	nr_sequencia in (
		select	b.nr_sequencia
		from	cirurgia_agente_anestesico a,
			cirurgia_agente_anest_ocor b	
		where	a.nr_sequencia = b.nr_seq_cirur_agente
		and	(((a.nr_cirurgia = nr_cirurgia_p) and (nr_cirurgia_p is not null)) or ((a.nr_seq_pepo = nr_seq_pepo_p) and (nr_seq_pepo_p is not null)))
		and	a.ie_tipo = 3
		and	b.dt_final_adm is null
		and	nvl(a.ie_situacao,'A') = 'A'
		and	nvl(b.ie_situacao,'A') = 'A')
	and	nvl(ie_situacao,'A') = 'A'
	and	dt_liberacao is null;
	commit;
	
	if	(ie_liberar_adm_evento_w = 'S') then
		update	cirurgia_agente_anestesico
		set		dt_liberacao = sysdate
		where	(((nr_cirurgia = nr_cirurgia_p) and (nr_cirurgia_p is not null)) or ((nr_seq_pepo = nr_seq_pepo_p) and (nr_seq_pepo_p is not null)))
		and		dt_liberacao is null
		and		ie_tipo = 3;
		commit;
	end if;
end if;

if	(nvl(nr_cirurgia_p,0) > 0) and (ie_liberar_pepo_w = 'S') then
	liberar_pepo(nr_cirurgia_p,'L',nm_usuario_p);
elsif	(nvl(nr_seq_pepo_p,0) > 0) and (ie_liberar_pepo_w = 'S') then
	liberar_pepo(0,'L',nm_usuario_p,nr_seq_pepo_p);
end if;

if	(ie_calcula_tempos_w = 'S') then
	gerar_tempo_cirurgico(nr_cirurgia_p,nm_usuario_p,nr_seq_pepo_p);
end if;

if 	(ie_finaliza_proced_adic_w = 'S') then
	select 	max(dt_termino) 
	into	dt_termino_w
	from	cirurgia 
	where 	nr_cirurgia = nr_cirurgia_p;
 
	update 	prescr_procedimento 
	set 	dt_fim = dt_termino_w
	where 	nr_prescricao = (select max(nr_prescricao) from cirurgia where nr_cirurgia = nr_cirurgia_p);
	commit;
end if;

if 	(ie_prescricao_anestesica_w = 'S') then 
	if (nr_cirurgia_p is not null) then
		gerar_prescricao_anestesica(nr_cirurgia_p, nm_usuario_p, 'S');
		
	elsif (nvl(ie_estrutura_pepo_w, 'N') = 'S') then
		SELECT MAX(nr_cirurgia)
		INTO nr_cirurgia_w
		FROM cirurgia
		WHERE nr_seq_pepo = nr_seq_pepo_p;
		
		gerar_prescricao_anestesica(nr_cirurgia_w, nm_usuario_p, 'S');
	end if;
end if;

end gerar_evento_cirurgia_anest;
/
