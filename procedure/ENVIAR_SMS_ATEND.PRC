create or replace
procedure enviar_sms_atend (	ds_remetente_p		varchar2,
				ds_destinatario_p	varchar2,
				ds_mensagem_p		varchar2,
				nr_seq_regra_envio_p	Number,
				nr_atendimento_p	Number,
				ie_envio_retorno_p	varchar2,
				nm_usuario_p		varchar2,
				ie_commit_p		varchar2) is
				
nr_sequencia_w	number(10);
id_sms_w		number(10);
nr_celular_w	varchar2(50);
nr_pos_ddd0_w			number(10) := 0;
ie_utilizar_ddi_w varchar(1);

begin

ie_utilizar_ddi_w := OBTER_VALOR_PARAM_USUARIO(0,214,0,obter_usuario_ativo,obter_estabelecimento_ativo);

nr_celular_w := trim(ds_destinatario_p);	

nr_celular_w := replace(nr_celular_w,'(','');
nr_celular_w := replace(nr_celular_w,')','');
nr_celular_w := replace(nr_celular_w,' ','');
nr_celular_w := replace(nr_celular_w,'-','');	

if NVL(ie_utilizar_ddi_w,'S') = 'S' Then
begin
	if	(substr(nr_celular_w,1,2) <> '55') then
		nr_celular_w :=  '55'||nr_celular_w;
	end if;

	nr_pos_ddd0_w	:= instr(nr_celular_w,'550');

	if	(nr_pos_ddd0_w > 0)then
		nr_celular_w	:= replace(nr_celular_w,'550','55');
	end if;

	if	(substr(nr_celular_w,1,3) = '550')then
		nr_celular_w := '55'||substr(nr_celular_w,4,50);	
end if;
end;
end if;
	
if	(ds_remetente_p is not null) and
	(nr_celular_w is not null) and
	(ds_mensagem_p is not null) and
	(nm_usuario_p is not null) then
	
	/* enviar sms */
	wheb_sms.enviar_sms(ds_remetente_p, nr_celular_w, ds_mensagem_p, nm_usuario_p,id_sms_w);

	/* gravar log */
	select	atend_regra_sms_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into atend_regra_sms (
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,	
		dt_atualizacao,
		nm_usuario,
		ds_mensagem,
		nr_seq_id_sms,
		nr_seq_regra_envio,
		nr_atendimento,
		ie_envio_retorno)
	values (
		nr_sequencia_w,
		sysdate, 
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_mensagem_p,
		id_sms_w,
		nr_seq_regra_envio_p,
		nr_atendimento_p,
		ie_envio_retorno_p);
		
end if;
if (ie_commit_p = 'S') then
	commit;
end if;

end enviar_sms_atend;
/