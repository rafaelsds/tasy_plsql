create or replace
procedure pls_gerar_hist_req_web
			(	nr_seq_requisicao_p	number,
				ie_tipo_historico_p		varchar2,
				ds_historico_p		varchar2,
				nm_usuario_p		varchar2) is 
	
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Gerar hist�rico de requisi��o , ao finalizar a auditoria  
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/	
	
begin

if	(nr_seq_requisicao_p is not null) then
	insert into pls_requisicao_historico
		(nr_sequencia, nr_seq_requisicao, ie_tipo_historico,
		ds_historico, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, dt_historico,
		ie_origem_historico)       
	values	(pls_requisicao_historico_seq.nextval, nr_seq_requisicao_p, ie_tipo_historico_p,
		ds_historico_p, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, sysdate,
		'A');
end if;

commit;

end pls_gerar_hist_req_web;
/
