 create or replace
procedure gerar_lib_bloqueio_telefone(	cd_estabelecimento_p	Number,
					nr_atendimento_p	Number,
					nm_usuario_p		varchar2,
					cd_setor_atendimento_p	Number,
					cd_unidade_basica_p	varchar2,
					cd_unidade_compl_p	varchar2,
					ie_codigo_bloq_lib_p	Number) as

/*Fixo para Libera��o do telefone
ie_codigo_bloq_lib_p
10 = Libera��o
12 = Bloqueio */

nr_seq_evento_w			Number(10);
nr_ramal_w			Number(5);
cd_setor_atendimento_w		Number(5);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
qt_existe_tel_w			Number(3);
ds_observacao_w			varchar2(255);
nm_paciente_w			varchar2(100);

begin
select	count(*)
into	qt_existe_tel_w
from	atend_lanc_auto
where	nr_seq_evento = 92
and	nr_atendimento = nr_atendimento_p;
if	(qt_existe_tel_w > 0) then
	begin
	cd_setor_atendimento_w		:= cd_setor_atendimento_p;
	cd_unidade_basica_w		:= cd_unidade_basica_p;
	cd_unidade_compl_w		:= cd_unidade_compl_p;

	if	(cd_setor_atendimento_p is null) or (cd_setor_atendimento_p = 0) then
		select	cd_setor_atendimento,
			cd_unidade_basica,
			cd_unidade_compl
		into	cd_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w
		from	atend_paciente_unidade
		where	nr_seq_interno	= obter_atepacu_paciente(nr_atendimento_p, 'A');
	end if;

	select	nvl(max(nr_ramal),0)
	into	nr_ramal_w
	from	unidade_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_w
	and	cd_unidade_basica	= cd_unidade_basica_w
	and	cd_unidade_compl	= cd_unidade_compl_w;

	if	(nr_ramal_w <> 0) then
		begin
		select	substr(obter_pessoa_atendimento(nr_atendimento_p, 'N'),1,100)
		into	nm_paciente_w
		from	dual;
		ds_observacao_w	:= wheb_mensagem_pck.get_texto(307853,	'NR_ATENDIMENTO_W=' || nr_atendimento_p || ';' ||
																'NM_PACIENTE_W=' || nm_paciente_w);
						-- Atendimento: #@NR_ATENDIMENTO_W#@ Paciente: #@NM_PACIENTE_W#@
		insert into tel_reg_evento(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_evento,
			nr_ramal,
			cd_evento,
			nr_seq_evento,
			nr_seq_acao,
			cd_setor_atendimento,
			cd_unidade_basica,
			cd_unidade_compl,
			ds_observacao,
			IE_PROCESSADO)
		values(	tel_reg_evento_seq.nextval,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_ramal_w,
			0,
			ie_codigo_bloq_lib_p,
			16, /*Fixo para Registro de Libera��o/ Bloqueios de telefone*/
			cd_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w,
			substr(ds_observacao_w,1,255),
			0);
		end;
	end if;
	end;
end if;

end gerar_lib_bloqueio_telefone;
/
