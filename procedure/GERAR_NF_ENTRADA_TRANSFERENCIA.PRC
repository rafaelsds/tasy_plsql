create or replace 
procedure gerar_nf_entrada_transferencia(
				nr_nota_fiscal_p		number,
				cd_serie_nf_p		varchar2,
				nr_ordem_compra_p	number,
				cd_operacao_nota_p	number,
				cd_natureza_operacao_p	number,
				cd_setor_digitacao_p	number,
				dt_emissao_p		date,
				nm_usuario_p		Varchar2,
				ie_consistidos		varchar2,
				ie_item_nf		varchar2,
				nr_seq_modelo_p		number,
				cd_estabelecimento_p	number,
				nr_sequencia_p		out	number,
				ds_erro_p			out	varchar2,
				ds_erro_item_p		out	varchar2,
				ds_erro_nota_p		out	varchar2) is

dt_liberacao_w			date;
dt_aprovacao_w			date;
nr_sequencia_w			number(10,0);
cd_cgc_emitente_w		varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
cd_condicao_pagamento_w		number(10,0);
ie_frete_w			varchar2(01);
cd_moeda_w			number(05,0);
vl_despesa_acessoria_nota_w	number(13,2)	:= 0;
vl_descontos_w			number(13,2)	:= 0;
vl_frete_w			number(13,2)	:= 0;
dt_atualizacao_w			date 		:= sysdate;
ie_tipo_ordem_w			varchar2(01);
cd_estab_transf_w			number(04,0);
cd_cgc_estabelecimento_w		varchar2(14);
nr_sequencia_nf_w			number(10,0);
nr_ordem_compra_w		number(10);
nr_item_oci_w			number(05,0);
nr_item_oci_ww			number(05,0);
cd_material_w			number(06,0);
cd_unidade_medida_compra_w	varchar2(30);
vl_unitario_item_nf_w		number(13,4)	:= 0;
pr_descontos_w			number(13,4);
cd_local_estoque_w		number(05,0);
ds_material_direto_w		varchar2(255);
cd_centro_custo_w			number(08,0);
cd_centro_conta_w			number(8);
cd_conta_contabil_w		varchar2(20)	:= null;
cd_conta_contabil_ww		varchar2(20)	:= null;
nr_seq_proj_rec_w			number(10,0);
pr_desc_financ_w			number(07,4);
nr_solic_compra_w			number(10,0);
dt_prevista_entrega_w		date;
nr_seq_unidade_adic_w		number(10,0);
nr_seq_criterio_rateio_w		number(10,0);
vl_desconto_oci_w			number(13,2);
nr_seq_ordem_serv_w		number(10,0);
nr_seq_proj_gpi_w			number(10,0);
nr_seq_etapa_gpi_w		number(10,0);
nr_seq_conta_gpi_w		number(10,0);
nr_contrato_w			number(10,0);
cd_local_direto_w			number(06,0);
cd_unidade_medida_estoque_w	varchar2(30);
qt_conv_compra_estoque_w		number(13,4);
cd_material_estoque_w		number(06,0)	:= null;
qt_prevista_entrega_w		number(13,4);
qt_conversao_w			number(13,4);
qt_item_estoque_w			number(13,4)	:= 0;
vl_total_item_nf_w			number(13,2)	:= 0;
vl_total_item_unit_nf_w			number(13,4)	:= 0;
nr_item_nf_w			number(05,0);
vl_desconto_w			number(13,2)	:= 0;
vl_liquido_w			number(13,2)	:= 0;
nr_seq_conta_financeira_w		number(10,0)	:= null;
ie_tipo_conta_w			number(05,0)	:= 2;
nr_seq_nota_transf_w		number(10,0);
cd_estab_oc_w			number(04,0);
ds_barra_w			varchar2(255);
ds_validade_w			varchar2(255);
nr_seq_lote_w			number(10);
nr_seq_reg_cb_w			number(10);
ds_lote_fornec_w			varchar2(20);
nr_seq_nf_saida_w			number(10);
ie_calcula_nf_w			varchar2(1);
dt_inicio_garantia_w		date;
dt_fim_garantia_w			date;
ie_tipo_local_w			varchar2(5);
ie_gera_lote_fornec_w		varchar2(1);
ie_grava_obs_lote_w		varchar2(1);
cd_operacao_estoque_w		number(3);
ie_consignado_operacao_w	varchar2(1) := '0';
cd_fornecedor_consig_w		varchar2(14);
ie_consistiu_lote_w			varchar2(1) := 'N';

ds_erro_w			varchar2(255);
ds_erro_item_w			varchar2(255) := '';
ds_erro_nota_w			varchar2(255) := '';

qt_itens_nota_w			number(10);
nr_seq_marca_w			material_lote_fornec.nr_seq_marca%type;
/*OS 1725476 - Incluido quando for emissao propria a partir de uma Ordem de Compra*/
ie_emissao_propria_w 		varchar2(1);
ie_tipo_nota_w 			number(1) := 0;
ie_tipo_nota_ww			nota_fiscal.ie_tipo_nota%type;
nr_nota_fiscal_w		serie_nota_fiscal.nr_ultima_nf%type;
/**********************************************************************************/

cursor c02 is
select	a.nr_item_oci,
	a.cd_material,
	a.cd_unidade_medida_compra,
	a.vl_unitario_material,
	nvl(a.pr_descontos,0),
	nvl(a.cd_local_estoque,cd_local_direto_w),
	substr(a.ds_material_direto,1,255),
	a.cd_centro_custo,
	a.cd_conta_contabil,
	a.nr_seq_proj_rec,
	nvl(a.pr_desc_financ,0),
	a.nr_solic_compra,
	b.dt_prevista_entrega,
	a.nr_seq_unidade_adic,
	a.nr_seq_criterio_rateio,
	nvl(a.vl_desconto,0),
	a.nr_seq_ordem_serv,
	a.nr_seq_proj_gpi,
	a.nr_seq_etapa_gpi,
	a.nr_seq_conta_gpi,
	a.nr_contrato,
	a.dt_inicio_garantia,
	a.dt_fim_garantia
from 	ordem_compra_item_entrega b,
	ordem_compra_item a
where	a.nr_ordem_compra 	= b.nr_ordem_compra
and	a.nr_item_oci 		= b.nr_item_oci
and	a.nr_ordem_compra	= nr_ordem_compra_p
and	nvl(a.qt_material,0) > nvl(a.qt_material_entregue,0)
and	a.dt_reprovacao is null
and	a.dt_aprovacao is not null
and	b.dt_cancelamento is null
group by a.nr_ordem_compra,
	a.nr_item_oci,
	a.cd_material,
	a.cd_unidade_medida_compra,
	a.vl_unitario_material,
	nvl(a.pr_descontos,0),
	nvl(a.cd_local_estoque,cd_local_direto_w),
	substr(a.ds_material_direto,1,255),
	a.cd_centro_custo,
	a.cd_conta_contabil,
	a.nr_seq_proj_rec,
	nvl(a.pr_desc_financ,0),
	a.nr_solic_compra,
	b.dt_prevista_entrega,
	a.nr_seq_unidade_adic,
	a.nr_seq_criterio_rateio,
	nvl(a.vl_desconto,0),
	a.nr_seq_ordem_serv,
	a.nr_seq_proj_gpi,
	a.nr_seq_etapa_gpi,
	a.nr_seq_conta_gpi,
	a.nr_contrato,
	a.dt_inicio_garantia,
	a.dt_fim_garantia
having	sum(b.qt_prevista_entrega) - max(obter_qt_oci_trans_nota(a.nr_ordem_compra, a.nr_item_oci,'E')) > 0
order by a.nr_item_oci;

Cursor c03 is
select	nvl(nr_sequencia,0)
from	ordem_compra_item_cb
where	nr_ordem_compra  = nr_ordem_compra_p
and	ie_atende_recebe = 'R';

Cursor c04 is
select	nr_ordem_compra,
	nr_item_oci,
	cd_material,
	qt_item_nf,
	cd_unidade_medida_compra,
	cd_lote_fabricacao,
	to_char(dt_validade,'dd/mm/yyyy') ds_validade,
	cd_barra_material,
	vl_unitario_item_nf,
	nvl(pr_desconto,0),
	vl_desconto,
	dt_entrega_ordem,
	cd_local_estoque,
	cd_fornecedor_consig,
	nr_seq_marca
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nf_saida_w;

begin
select	dt_liberacao,
	dt_aprovacao,
	cd_cgc_fornecedor,
	cd_pessoa_fisica,
	cd_condicao_pagamento,
	nvl(ie_frete,'F'),
	cd_moeda,
	nvl(vl_despesa_acessoria, 0),
	nvl(ie_tipo_ordem,'N'),
	cd_estab_transf,
	nvl(vl_frete,0) vl_frete,
	cd_estabelecimento
into	dt_liberacao_w,
	dt_aprovacao_w,
	cd_cgc_emitente_w,
	cd_pessoa_fisica_w,
	cd_condicao_pagamento_w,
	ie_frete_w,
	cd_moeda_w,
	vl_despesa_acessoria_nota_w,
	ie_tipo_ordem_w,
	cd_estab_transf_w,
	vl_frete_w,
	cd_estab_oc_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

if	(dt_liberacao_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(181730);
elsif	(dt_aprovacao_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(181731);
elsif	(ie_tipo_ordem_w <> 'T') then
	wheb_mensagem_pck.exibir_mensagem_abort(181732);
end if;

begin
select	min(cd_local_estoque)
into	cd_local_direto_w
from	local_estoque
where	ie_tipo_local = 8;
exception
	when others then
		cd_local_direto_w := 1;
end;

ie_calcula_nf_w		:= substr(nvl(obter_valor_param_usuario(146, 36, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);
ie_gera_lote_fornec_w	:= substr(nvl(obter_valor_param_usuario(40, 18, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);
ie_grava_obs_lote_w	:= substr(nvl(obter_valor_param_usuario(40, 144, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);

select	cd_cgc
into	cd_cgc_estabelecimento_w
from	estabelecimento
where	cd_estabelecimento = cd_estab_oc_w;

select	nota_fiscal_seq.nextval
into	nr_sequencia_w
from	dual;

select	(nvl(max(nr_sequencia_nf),0)+1)
into	nr_sequencia_nf_w
from	nota_fiscal
where	cd_estabelecimento		= cd_estab_oc_w
and	cd_cgc_emitente		= nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w)
and	nr_nota_fiscal		= nr_nota_fiscal_p
and	cd_serie_nf		= cd_serie_nf_p;

select	nvl(ie_recusa,'')
into	ie_emissao_propria_w
from	operacao_nota
where	cd_operacao_nf = cd_operacao_nota_p;

--Se a operacao da nota for de origem propria buscar o nr_sequencia_nf_w da ultima sequencia da serie da nota
--Definir tipo de nota 5 quando for emissao propria. Nos demais casos, ficara 0

nr_nota_fiscal_w := nr_nota_fiscal_p;
if(ie_emissao_propria_w = 'S') then
	SELECT
		(nvl(nr_ultima_nf, 0)+1)
	into nr_nota_fiscal_w
	from serie_nota_fiscal
	WHERE cd_serie_nf = cd_serie_nf_p
	AND cd_estabelecimento = cd_estabelecimento_p;
	ie_tipo_nota_w := 5;
end if;

select	nvl(obter_valor_desconto_ordem(nr_ordem_compra_p),0)
into	vl_descontos_w
from	dual;

if	(ie_tipo_ordem_w = 'T') and
	(cd_estab_transf_w is not null) then
	select	max(nr_sequencia)
	into	nr_seq_nota_transf_w
	from	nota_fiscal
	where	cd_estabelecimento		= cd_estab_transf_w
	and	nr_ordem_compra		= nr_ordem_compra_p;
end if;

select	nvl(cd_operacao_estoque,0)
into	cd_operacao_estoque_w
from	operacao_nota
where	cd_operacao_nf = cd_operacao_nota_p;

select	ie_consignado
into	ie_consignado_operacao_w
from	operacao_estoque
where	cd_operacao_estoque = cd_operacao_estoque_w;

if(ie_emissao_propria_w = 'S') then
ie_tipo_nota_ww := 'EP';
else

select decode(cd_pessoa_fisica_w,null,'EN','EF')
into ie_tipo_nota_ww
from dual;

end if;

insert into nota_fiscal (
	cd_estabelecimento,		cd_serie_nf,			cd_cgc_emitente,
	nr_nota_fiscal,			nr_sequencia_nf,			cd_operacao_nf,
	ie_acao_nf,			dt_entrada_saida,			dt_emissao,
	ie_emissao_nf,			ie_tipo_frete,			vl_mercadoria,
	vl_total_nota,			qt_peso_bruto,			qt_peso_liquido,
	dt_atualizacao,			nm_usuario,			cd_condicao_pagamento,
	dt_contabil,			cd_cgc,				cd_pessoa_fisica,
	vl_ipi,				vl_descontos,			vl_frete,
	vl_seguro,			nr_nota_referencia,			vl_despesa_acessoria,
	cd_serie_referencia,		cd_natureza_operacao,		dt_atualizacao_estoque,
	vl_desconto_rateio,			ie_situacao,			nr_ordem_compra,
	nr_lote_contabil,			nr_sequencia,			nr_sequencia_ref,
	cd_moeda,			vl_conv_moeda,			ie_entregue_bloqueto,
	cd_setor_digitacao,			ie_tipo_nota,			nr_seq_modelo)
values(	cd_estab_oc_w,	 		cd_serie_nf_p,			nvl(cd_cgc_emitente_w, cd_cgc_estabelecimento_w),
 	nr_nota_fiscal_w,			nr_sequencia_nf_w, 		cd_operacao_nota_p,
 	'1',				dt_atualizacao_w, 			nvl(dt_emissao_p, trunc(sysdate)),
 	'0',			 	ie_frete_w,		 	0,
 	0,			 	0,			 	0,
 	dt_atualizacao_w,			nm_usuario_p,		 	cd_condicao_pagamento_w,
 	null,			 	cd_cgc_emitente_w,	 	cd_pessoa_fisica_w,
 	0,			 	vl_descontos_w,			vl_frete_w,
 	0,			 	null,				nvl(vl_despesa_acessoria_nota_w, 0),
 	null,			 	cd_natureza_operacao_p,	 	null,
 	0,			 	'1',	 			nr_ordem_compra_p,
 	0,			 	nr_sequencia_w,			null,
	cd_moeda_w,			1,				'N',
	cd_setor_digitacao_p,		ie_tipo_nota_ww, 	nr_seq_modelo_p);


if	(ie_consistidos = 'S') or
	(ie_item_nf = 1) then
	begin
	open c03;
	loop
	fetch c03 into
		nr_seq_reg_cb_w;
	exit when C03%notfound;
		begin
		select	nvl(max(nr_item_oci),0),
			nvl(max(cd_material),0),
			nvl(max(qt_material),0),
			nvl(max(nr_seq_lote),0),
			nvl(max(cd_unidade_medida_compra),0)
		into	nr_item_oci_ww,
			cd_material_w,
			qt_prevista_entrega_w,
			nr_seq_lote_w,
			cd_unidade_medida_compra_w
		from	ordem_compra_item_cb
		where	nr_sequencia = nr_seq_reg_cb_w;

		if 	(nr_seq_lote_w > 0) then
			begin
			select	to_char(dt_validade,'dd/mm/yyyy'),
				decode(nvl(cd_barra_material,'X'), 'X', lpad(nr_sequencia || nr_digito_verif,11,0), cd_barra_material) ds_barra,
				ds_lote_fornec,
				nr_seq_marca
			into	ds_validade_w,
				ds_barra_w,
				ds_lote_fornec_w,
				nr_seq_marca_w
			from	material_lote_fornec
			where	nr_sequencia = nr_seq_lote_w;
			end;
		else
			nr_seq_lote_w	:= '';
			ds_barra_w	:= '';
			ds_validade_w	:= '';
			nr_seq_marca_w	:= null;
		end if;

		if	(nr_item_oci_ww > 0) then
			begin
			select	max(a.nr_item_oci),
				max(a.vl_unitario_material),
				max(nvl(a.pr_descontos,0)),
				max(nvl(a.cd_local_estoque,cd_local_direto_w)),
				max(substr(a.ds_material_direto,1,255)),
				max(a.cd_centro_custo),
				max(a.cd_conta_contabil),
				max(a.nr_seq_proj_rec),
				max(nvl(a.pr_desc_financ,0)),
				max(a.nr_solic_compra),
				max(b.dt_prevista_entrega),
				max(a.nr_seq_unidade_adic),
				max(a.nr_seq_criterio_rateio),
				max(nvl(a.vl_desconto,0)),
				max(a.nr_seq_ordem_serv),
				max(a.nr_seq_proj_gpi),
				max(a.nr_seq_etapa_gpi),
				max(a.nr_seq_conta_gpi),
				max(a.nr_contrato),
				max(a.dt_inicio_garantia),
				max(a.dt_fim_garantia)
			into	nr_item_oci_w,
				vl_unitario_item_nf_w,
				pr_descontos_w,
				cd_local_estoque_w,
				ds_material_direto_w,
				cd_centro_custo_w,
				cd_conta_contabil_w,
				nr_seq_proj_rec_w,
				pr_desc_financ_w,
				nr_solic_compra_w,
				dt_prevista_entrega_w,
				nr_seq_unidade_adic_w,
				nr_seq_criterio_rateio_w,
				vl_desconto_oci_w,
				nr_seq_ordem_serv_w,
				nr_seq_proj_gpi_w,
				nr_seq_etapa_gpi_w,
				nr_seq_conta_gpi_w,
				nr_contrato_w,
				dt_inicio_garantia_w,
				dt_fim_garantia_w
			from 	ordem_compra_item_entrega b,
				ordem_compra_item a
			where	a.nr_ordem_compra 	= b.nr_ordem_compra
			and	a.nr_item_oci		= nr_item_oci_ww
			and	a.nr_item_oci 		= b.nr_item_oci
			and	a.nr_ordem_compra		= nr_ordem_compra_p
			and	nvl(a.qt_material,0) > nvl(a.qt_material_entregue,0)
			and	(nvl(qt_prevista_entrega,0) - nvl(qt_real_entrega,0)) > 0
			and	a.dt_reprovacao is null
			and	a.dt_aprovacao is not null
			and	b.dt_cancelamento is null;

			if	(nvl(cd_material_w,0) > 0) then
				select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
						qt_conv_compra_estoque,
						cd_material_estoque
				into	cd_unidade_medida_estoque_w,
						qt_conv_compra_estoque_w,
						cd_material_estoque_w
				from 	material
				where	cd_material = cd_material_w;


				if	(cd_unidade_medida_compra_w = cd_unidade_medida_estoque_w) then
					qt_item_estoque_w	:= qt_prevista_entrega_w;
				else
					qt_item_estoque_w	:= obter_quantidade_convertida(cd_material_w,qt_prevista_entrega_w,cd_unidade_medida_compra_w,'UME','N');
				end if;

				if	(nvl(nr_seq_unidade_adic_w, 0) > 0) then
					select	qt_conversao
					into	qt_conversao_w
					from	unidade_medida_adic_compra
					where	nr_sequencia = nr_seq_unidade_adic_w;
					qt_item_estoque_w	:= qt_prevista_entrega_w * qt_conversao_w;
				end if;
				
				if	(nr_seq_nota_transf_w is not null) then
					begin
						select vl_nf 
						into    vl_unitario_item_nf_w
						from (  select	nvl(vl_unitario_item_nf,0) vl_nf
											from	nota_fiscal_item
											where	nr_ordem_compra	= nr_ordem_compra_p
											and	nr_item_oci	= nr_item_oci_w
											order by nr_sequencia desc ) 
						where rownum = 1;
						exception
						when others then
							vl_unitario_item_nf_w := 0;
					end;
				end if;

				vl_total_item_nf_w	:= (qt_prevista_entrega_w * vl_unitario_item_nf_w);

				--Variavel criada para que o calculo seja feito com 4 casas decimais, pois estava sendo gerado o valor unitario errado do material quanto exigia quebra por lote fornecedor
				vl_total_item_unit_nf_w		:= nvl((qt_prevista_entrega_w * vl_unitario_item_nf_w),0);

				vl_desconto_w		:= (vl_total_item_nf_w * pr_descontos_w) / 100 + nvl(vl_desconto_oci_w,0);
				vl_liquido_w		:= vl_total_item_nf_w - vl_desconto_w;
				vl_unitario_item_nf_w	:= dividir(vl_total_item_unit_nf_w,qt_prevista_entrega_w);

				if	(nm_usuario_p = '') then
					insert into log_tasy(
						dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(	sysdate,
						'',
						169,
						'cd_material_w = ' || cd_material_w || chr(10)||chr(13)
						|| 'nr_item_oci_ww = ' || nr_item_oci_ww || chr(10)||chr(13)
						|| 'nr_ordem_compra_p = ' || nr_ordem_compra_p || chr(10)||chr(13)
						|| 'qt_prevista_entrega_w = ' || qt_prevista_entrega_w || chr(10)||chr(13)
						|| 'vl_unitario_item_nf_w = ' || vl_unitario_item_nf_w || chr(10)||chr(13)
						|| 'vl_total_item_nf_w = ' || vl_total_item_nf_w || chr(10)||chr(13)
						|| 'pr_descontos_w = ' || pr_descontos_w || chr(10)||chr(13)
						|| 'vl_desconto_oci_w = ' || vl_desconto_oci_w || chr(10)||chr(13)
						|| 'vl_desconto_w = ' || vl_desconto_w || chr(10)||chr(13)
						|| 'vl_liquido_w = ' || vl_liquido_w);
				end if;

				select	(nvl(max(nr_item_nf),0)+1)
				into	nr_item_nf_w
				from	nota_fiscal_item
				where nr_sequencia = nr_sequencia_w;

				obter_conta_financeira(
					'S',
					cd_estab_oc_w,
					cd_material_w,
					null,
					null,
					null,
					null,
					cd_cgc_emitente_w,
					cd_centro_custo_w,
					nr_seq_conta_financeira_w,
					null,
					cd_operacao_nota_p,
					null,
					null,
					null,
					nr_seq_proj_rec_w,
					null,
					cd_pessoa_fisica_w,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);

				if	(nr_seq_conta_financeira_w = 0) then
					nr_seq_conta_financeira_w := null;
				end if;


				cd_centro_conta_w := cd_centro_custo_w;
				if	(nvl(cd_local_estoque_w,0) > 0) then
					select	ie_tipo_local
					into	ie_tipo_local_w
					from	local_estoque
					where	cd_local_estoque = cd_local_estoque_w;
				else
					ie_tipo_local_w := '0';
				end if;
				if	(cd_centro_custo_w is null) and
					(ie_tipo_local_w <> '8') then
					ie_tipo_conta_w	:= 2;
				else
					ie_tipo_conta_w	:= 3;
				end if;

				define_conta_material(
					cd_estab_oc_w,
					cd_material_w,
					ie_tipo_conta_w,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					cd_local_estoque_w,
					cd_operacao_estoque_w,
					trunc(dt_atualizacao_w),
					cd_conta_contabil_w,
					cd_centro_conta_w,
					null);

				if	(nvl(cd_conta_contabil_w, 'X') = 'X') or
					(nvl(cd_conta_contabil_w, 'X') <> nvl(cd_conta_contabil_ww, 'X')) then
					cd_conta_contabil_ww	:= cd_conta_contabil_w;
				end if;


				if	(nr_solic_compra_w is not null) and
					(nr_seq_ordem_serv_w is null) then
					select	nr_seq_ordem_serv
					into	nr_seq_ordem_serv_w
					from	solic_compra
					where	nr_solic_compra	= nr_solic_compra_w;
				end if;

				if	(ie_consignado_operacao_w > 0) and (nvl(nr_seq_lote_w,0) > 0) then
					select	cd_cgc_fornec
					into	cd_fornecedor_consig_w
					from	material_lote_fornec
					where	nr_sequencia = nr_seq_lote_w;
				end if;

				insert into nota_fiscal_item(
					cd_estabelecimento,
					cd_cgc_emitente,
					cd_serie_nf,
					nr_nota_fiscal,
					nr_sequencia_nf,
					nr_item_nf,
					cd_natureza_operacao,
					qt_item_nf,
					vl_unitario_item_nf,
					vl_total_item_nf,
					dt_atualizacao,
					nm_usuario,
					vl_frete,
					vl_desconto,
					vl_despesa_acessoria,
					cd_material,
					cd_local_estoque,
					ds_complemento,
					cd_unidade_medida_compra,
					qt_item_estoque,
					cd_unidade_medida_estoque,
					cd_conta_contabil,
					vl_desconto_rateio,
					vl_seguro,
					cd_centro_custo,
					cd_material_estoque,
					nr_ordem_compra,
					nr_sequencia,
					vl_liquido,
					pr_desconto,
					nr_item_oci,
					dt_entrega_ordem,
					nr_seq_conta_financ,
					nr_seq_proj_rec,
					pr_desc_financ,
					nr_seq_unidade_adic,
					nr_seq_proj_gpi,
					nr_seq_etapa_gpi,
					nr_seq_conta_gpi,
					nr_contrato,
					cd_lote_fabricacao,
					dt_validade,
					dt_inicio_garantia,
					dt_fim_garantia,
					nr_seq_lote_fornec,
					cd_fornecedor_consig,
					nr_seq_marca,
					cd_sequencia_parametro)
				values(	cd_estab_oc_w,
					nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w),
					cd_serie_nf_p,
					nr_nota_fiscal_p,
					nr_sequencia_nf_w,
					nr_item_nf_w,
					cd_natureza_operacao_p,
					qt_prevista_entrega_w,
					vl_unitario_item_nf_w,
					vl_total_item_nf_w,
					dt_atualizacao_w,
					nm_usuario_p,
					vl_frete_w,
					nvl(vl_desconto_w,0),
					0,
					cd_material_w,
					cd_local_estoque_w,
					ds_material_direto_w,
					cd_unidade_medida_compra_w,
					qt_item_estoque_w,
					cd_unidade_medida_estoque_w,
					cd_conta_contabil_w,
					0,
					0,
					cd_centro_custo_w,
					cd_material_estoque_w,
					nr_ordem_compra_p,
					nr_sequencia_w,
					vl_liquido_w,
					pr_descontos_w,
					nr_item_oci_w,
					dt_prevista_entrega_w,
					nr_seq_conta_financeira_w,
					nr_seq_proj_rec_w,
					pr_desc_financ_w,
					nr_seq_unidade_adic_w,
					nr_seq_proj_gpi_w,
					nr_seq_etapa_gpi_w,
					nr_seq_conta_gpi_w,
					nr_contrato_w,
					ds_lote_fornec_w,
					ds_validade_w,
					dt_inicio_garantia_w,
					dt_fim_garantia_w,
					nr_seq_lote_w,
					cd_fornecedor_consig_w,
					nr_seq_marca_w,
					philips_contabil_pck.get_parametro_conta_contabil);

					if 	(ds_barra_w is not null) then
						begin
						update  nota_fiscal_item
						set	cd_barra_material = ds_barra_w
						where   nr_item_nf = nr_item_nf_w
						and	nr_sequencia = nr_sequencia_w
						and	cd_material is not null
						and     ds_barras is null;
						end;
					end if;

				if	(nr_seq_criterio_rateio_w is not null) then
					ratear_item_nf(nr_sequencia_w, nr_item_nf_w, nr_seq_criterio_rateio_w, nm_usuario_p, trunc(dt_atualizacao_w));
				end if;
			end if;
		end;
		end if;
	end;
	end loop;
	close c03;
	end;
elsif	(ie_item_nf = 0) then
	begin
	open C02;
	loop
	fetch C02 into
		nr_item_oci_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		vl_unitario_item_nf_w,
		pr_descontos_w,
		cd_local_estoque_w,
		ds_material_direto_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		nr_seq_proj_rec_w,
		pr_desc_financ_w,
		nr_solic_compra_w,
		dt_prevista_entrega_w,
		nr_seq_unidade_adic_w,
		nr_seq_criterio_rateio_w,
		vl_desconto_oci_w,
		nr_seq_ordem_serv_w,
		nr_seq_proj_gpi_w,
		nr_seq_etapa_gpi_w,
		nr_seq_conta_gpi_w,
		nr_contrato_w,
		dt_inicio_garantia_w,
		dt_fim_garantia_w;
	exit when C02%notfound;
		begin
		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
			qt_conv_compra_estoque,
			cd_material_estoque
		into	cd_unidade_medida_estoque_w,
			qt_conv_compra_estoque_w,
			cd_material_estoque_w
		from 	material
		where	cd_material = cd_material_w;


		select	nvl(sum(a.qt_prevista_entrega) - max(obter_qt_oci_trans_nota(a.nr_ordem_compra, a.nr_item_oci,'E')),0)
		into	qt_prevista_entrega_w
		from	ordem_compra_item_entrega a,
			ordem_compra_item b
		where	a.nr_ordem_compra	= nr_ordem_compra_p
		and	a.nr_item_oci			= nr_item_oci_w
		and	b.nr_ordem_compra 		= a.nr_ordem_compra
		and	a.nr_item_oci			= b.nr_item_oci
		and	a.dt_prevista_entrega 	= dt_prevista_entrega_w
		and	a.dt_cancelamento is null;


		if	(cd_unidade_medida_compra_w = cd_unidade_medida_estoque_w) then
			qt_item_estoque_w	:= qt_prevista_entrega_w;
		else
			qt_item_estoque_w	:= obter_quantidade_convertida(cd_material_w,qt_prevista_entrega_w,cd_unidade_medida_compra_w,'UME','N');
		end if;

		if	(nvl(nr_seq_unidade_adic_w, 0) > 0) then
			select	qt_conversao
			into	qt_conversao_w
			from	unidade_medida_adic_compra
			where	nr_sequencia 	= nr_seq_unidade_adic_w;
			qt_item_estoque_w	:= qt_prevista_entrega_w * qt_conversao_w;
		end if;

		if	(nr_seq_nota_transf_w is not null) then
			begin
				select vl_nf 
				into    vl_unitario_item_nf_w
				from (  select	nvl(vl_unitario_item_nf,0) vl_nf
						from	nota_fiscal_item
						where	nr_ordem_compra	= nr_ordem_compra_p
						and	nr_item_oci	= nr_item_oci_w
						order by nr_sequencia desc ) 
				where rownum = 1;
				exception
				when others then
					vl_unitario_item_nf_w := 0;
			end;
		end if;


		vl_total_item_nf_w	:= (qt_prevista_entrega_w * vl_unitario_item_nf_w);
		vl_desconto_w		:= (vl_total_item_nf_w * pr_descontos_w) / 100 + nvl(vl_desconto_oci_w,0);
		vl_liquido_w		:= vl_total_item_nf_w - vl_desconto_w;
		vl_unitario_item_nf_w	:= dividir(vl_total_item_nf_w,qt_prevista_entrega_w);

		select	(nvl(max(nr_item_nf),0)+1)
		into	nr_item_nf_w
		from	nota_fiscal_item
		where nr_sequencia = nr_sequencia_w;

		obter_conta_financeira(
			'S',
			cd_estab_oc_w,
			cd_material_w,
			null,
			null,
			null,
			null,
			cd_cgc_emitente_w,
			cd_centro_custo_w,
			nr_seq_conta_financeira_w,
			null,
			cd_operacao_nota_p,
			null,
			null,
			null,
			nr_seq_proj_rec_w,
			null,
			cd_pessoa_fisica_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null);

		if	(nr_seq_conta_financeira_w = 0) then
			nr_seq_conta_financeira_w := null;
		end if;


		cd_centro_conta_w := cd_centro_custo_w;
		if	(nvl(cd_local_estoque_w,0) > 0) then
			select	ie_tipo_local
			into	ie_tipo_local_w
			from	local_estoque
			where	cd_local_estoque = cd_local_estoque_w;
		else
			ie_tipo_local_w := '0';
		end if;
		if	(cd_centro_custo_w is null) and
			(ie_tipo_local_w <> '8') then
			ie_tipo_conta_w	:= 2;
		else
			ie_tipo_conta_w	:= 3;
		end if;

		define_conta_material(
			cd_estab_oc_w,
			cd_material_w,
			ie_tipo_conta_w,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			cd_local_estoque_w,
			cd_operacao_estoque_w,
			trunc(dt_atualizacao_w),
			cd_conta_contabil_w,
			cd_centro_conta_w,
			null);

		if	(nvl(cd_conta_contabil_w, 'X') = 'X') or
			(nvl(cd_conta_contabil_w, 'X') <> nvl(cd_conta_contabil_ww, 'X')) then
			cd_conta_contabil_ww	:= cd_conta_contabil_w;
		end if;


		if	(nr_solic_compra_w is not null) and
			(nr_seq_ordem_serv_w is null) then
			select	nr_seq_ordem_serv
			into	nr_seq_ordem_serv_w
			from	solic_compra
			where	nr_solic_compra	= nr_solic_compra_w;
		end if;


		insert into nota_fiscal_item(
			cd_estabelecimento,
			cd_cgc_emitente,
			cd_serie_nf,
			nr_nota_fiscal,
			nr_sequencia_nf,
			nr_item_nf,
			cd_natureza_operacao,
			qt_item_nf,
			vl_unitario_item_nf,
			vl_total_item_nf,
			dt_atualizacao,
			nm_usuario,
			vl_frete,
			vl_desconto,
			vl_despesa_acessoria,
			cd_material,
			cd_local_estoque,
			ds_complemento,
			cd_unidade_medida_compra,
			qt_item_estoque,
			cd_unidade_medida_estoque,
			cd_conta_contabil,
			vl_desconto_rateio,
			vl_seguro,
			cd_centro_custo,
			cd_material_estoque,
			nr_ordem_compra,
			nr_sequencia,
			vl_liquido,
			pr_desconto,
			nr_item_oci,
			dt_entrega_ordem,
			nr_seq_conta_financ,
			nr_seq_proj_rec,
			pr_desc_financ,
			nr_seq_unidade_adic,
			nr_seq_proj_gpi,
			nr_seq_etapa_gpi,
			nr_seq_conta_gpi,
			nr_contrato,
			dt_inicio_garantia,
			dt_fim_garantia,
			cd_fornecedor_consig,
			cd_sequencia_parametro)
		values(	cd_estab_oc_w,
			nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w),
			cd_serie_nf_p,
			nr_nota_fiscal_p,
			nr_sequencia_nf_w,
			nr_item_nf_w,
			cd_natureza_operacao_p,
			qt_prevista_entrega_w,
			vl_unitario_item_nf_w,
			vl_total_item_nf_w,
			dt_atualizacao_w,
			nm_usuario_p,
			vl_frete_w,
			nvl(vl_desconto_w,0),
			0,
			cd_material_w,
			cd_local_estoque_w,
			ds_material_direto_w,
			cd_unidade_medida_compra_w,
			qt_item_estoque_w,
			cd_unidade_medida_estoque_w,
			cd_conta_contabil_w,
			0,
			0,
			cd_centro_custo_w,
			cd_material_estoque_w,
			nr_ordem_compra_p,
			nr_sequencia_w,
			vl_liquido_w,
			pr_descontos_w,
			nr_item_oci_w,
			dt_prevista_entrega_w,
			nr_seq_conta_financeira_w,
			nr_seq_proj_rec_w,
			pr_desc_financ_w,
			nr_seq_unidade_adic_w,
			nr_seq_proj_gpi_w,
			nr_seq_etapa_gpi_w,
			nr_seq_conta_gpi_w,
			nr_contrato_w,
			dt_inicio_garantia_w,
			dt_fim_garantia_w,
			cd_fornecedor_consig_w,
			philips_contabil_pck.get_parametro_conta_contabil);

		if	(nr_seq_criterio_rateio_w is not null) then
			ratear_item_nf(nr_sequencia_w, nr_item_nf_w, nr_seq_criterio_rateio_w, nm_usuario_p, trunc(dt_atualizacao_w));
		end if;
		end;
	end loop;
	close C02;
	end;
elsif	(ie_item_nf = 2) then
	begin
	SELECT 	NVL(MAX(nr_sequencia),0)
	into	nr_seq_nf_saida_w
	FROM	nota_fiscal
	WHERE	nr_nota_fiscal  	=	nr_nota_fiscal_p
	AND	cd_serie_nf		=	cd_serie_nf_p
	AND	trunc(dt_emissao,'dd')	=	trunc(dt_emissao_p,'dd')
	AND	nr_ordem_compra		=	nr_ordem_compra_p
	and	cd_estabelecimento	= 	cd_estab_transf_w
	AND	cd_cgc_emitente		= 	cd_cgc_emitente_w;


	if	(nr_seq_nf_saida_w > 0) then
		begin

		open C04;
		loop
		fetch C04 into
			nr_ordem_compra_w,
			nr_item_oci_ww,
			cd_material_w,
			qt_prevista_entrega_w,
			cd_unidade_medida_compra_w,
			ds_lote_fornec_w,
			ds_validade_w,
			ds_barra_w,
			vl_unitario_item_nf_w,
			pr_descontos_w,
			vl_desconto_oci_w,
			dt_prevista_entrega_w,
			cd_local_estoque_w,
			cd_fornecedor_consig_w,
			nr_seq_marca_w;
		exit when C04%notfound;
			begin
			select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
				qt_conv_compra_estoque,
				cd_material_estoque
			into	cd_unidade_medida_estoque_w,
				qt_conv_compra_estoque_w,
				cd_material_estoque_w
			from 	material
			where	cd_material = cd_material_w;


			if	(cd_unidade_medida_compra_w = cd_unidade_medida_estoque_w) then
				qt_item_estoque_w	:= qt_prevista_entrega_w;
			else
				qt_item_estoque_w	:= obter_quantidade_convertida(cd_material_w,qt_prevista_entrega_w,cd_unidade_medida_compra_w,'UME','N');
			end if;


			nr_seq_criterio_rateio_w := null;

			if	(nvl(nr_item_oci_ww,0) > 0) then
				begin
				begin
				select	cd_local_estoque,
					cd_centro_custo,
					cd_conta_contabil,
					nr_seq_criterio_rateio
				into	cd_local_estoque_w,
					cd_centro_custo_w,
					cd_conta_contabil_w,
					nr_seq_criterio_rateio_w
				from  	ordem_compra_item
				where 	nr_ordem_compra = nr_ordem_compra_w
				and	nr_item_oci = nr_item_oci_ww;
				exception
					when others then
						cd_local_estoque_w := null;
						cd_centro_custo_w := null;
						cd_conta_contabil_w := null;
						nr_seq_criterio_rateio_w := null;
				end;
				end;
			else
				select	cd_local_entrega,
					cd_centro_custo
				into	cd_local_estoque_w,
					cd_centro_custo_w
				from	ordem_compra
				where	nr_ordem_compra = nvl(nr_ordem_compra_w, nr_ordem_compra_p);
			end if;

			if	(cd_local_estoque_w is null) then
				select	cd_local_entrega
				into	cd_local_estoque_w
				from  	ordem_compra
				where 	nr_ordem_compra = nvl(nr_ordem_compra_w, nr_ordem_compra_p);
			end if;


			vl_total_item_nf_w	:= (qt_prevista_entrega_w * vl_unitario_item_nf_w);
			vl_desconto_w		:= (vl_total_item_nf_w * pr_descontos_w) / 100 + nvl(vl_desconto_oci_w,0);
			vl_liquido_w		:= vl_total_item_nf_w - vl_desconto_w;
			vl_unitario_item_nf_w	:= dividir(vl_total_item_nf_w,qt_prevista_entrega_w);

			select	(nvl(max(nr_item_nf),0)+1)
			into	nr_item_nf_w
			from	nota_fiscal_item
			where 	nr_sequencia = nr_sequencia_w;

			obter_conta_financeira(
				'S',
				cd_estab_oc_w,
				cd_material_w,
				null,
				null,
				null,
				null,
				cd_cgc_emitente_w,
				cd_centro_custo_w,
				nr_seq_conta_financeira_w,
				null,
				cd_operacao_nota_p,
				null,
				null,
				null,
				null,
				null,
				cd_pessoa_fisica_w,
				null,
				null,
				null,
				null,
				cd_local_estoque_w,
				null,
				null,
				null,
				null,
				null,
				null);

			if	(nr_seq_conta_financeira_w = 0) then
				nr_seq_conta_financeira_w := null;
			end if;


			cd_centro_conta_w := cd_centro_custo_w;
			if	(nvl(cd_local_estoque_w,0) > 0) then
				select	ie_tipo_local
				into	ie_tipo_local_w
				from	local_estoque
				where	cd_local_estoque = cd_local_estoque_w;
			else
				ie_tipo_local_w := '0';
			end if;
			if	(cd_centro_custo_w is null) and
				(ie_tipo_local_w <> '8') then
				ie_tipo_conta_w	:= 2;
			else
				ie_tipo_conta_w	:= 3;
			end if;

			define_conta_material(
				cd_estab_oc_w,
				cd_material_w,
				ie_tipo_conta_w,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				cd_local_estoque_w,
				cd_operacao_estoque_w,
				trunc(dt_atualizacao_w),
				cd_conta_contabil_w,
				cd_centro_conta_w,
				null);

			if	(nvl(cd_conta_contabil_w, 'X') = 'X') or
				(nvl(cd_conta_contabil_w, 'X') <> nvl(cd_conta_contabil_ww, 'X')) then
				cd_conta_contabil_ww	:= cd_conta_contabil_w;
			end if;


			if	(nr_solic_compra_w is not null) and
				(nr_seq_ordem_serv_w is null) then
				select	nr_seq_ordem_serv
				into	nr_seq_ordem_serv_w
				from	solic_compra
				where	nr_solic_compra	= nr_solic_compra_w;
			end if;


			insert into nota_fiscal_item(
				cd_estabelecimento,
				cd_cgc_emitente,
				cd_serie_nf,
				nr_nota_fiscal,
				nr_sequencia_nf,
				nr_item_nf,
				cd_natureza_operacao,
				qt_item_nf,
				vl_unitario_item_nf,
				vl_total_item_nf,
				dt_atualizacao,
				nm_usuario,
				vl_desconto,
				vl_despesa_acessoria,
				cd_material,
				cd_local_estoque,
				cd_unidade_medida_compra,
				qt_item_estoque,
				cd_unidade_medida_estoque,
				cd_conta_contabil,
				vl_desconto_rateio,
				vl_seguro,
				cd_centro_custo,
				cd_material_estoque,
				nr_ordem_compra,
				nr_sequencia,
				vl_liquido,
				pr_desconto,
				nr_item_oci,
				dt_entrega_ordem,
				nr_seq_conta_financ,
				cd_lote_fabricacao,
				dt_validade,
				vl_frete,
				cd_fornecedor_consig,
				nr_seq_marca,
				cd_sequencia_parametro)
			values(	cd_estab_oc_w,
				nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w),
				cd_serie_nf_p,
				nr_nota_fiscal_p,
				nr_sequencia_nf_w,
				nr_item_nf_w,
				cd_natureza_operacao_p,
				qt_prevista_entrega_w,
				vl_unitario_item_nf_w,
				vl_total_item_nf_w,
				dt_atualizacao_w,
				nm_usuario_p,
				nvl(vl_desconto_w,0),
				0,
				cd_material_w,
				cd_local_estoque_w,
				cd_unidade_medida_compra_w,
				qt_item_estoque_w,
				cd_unidade_medida_estoque_w,
				cd_conta_contabil_w,
				0,
				0,
				cd_centro_custo_w,
				cd_material_estoque_w,
				nr_ordem_compra_w,
				nr_sequencia_w,
				vl_liquido_w,
				pr_descontos_w,
				nr_item_oci_ww,
				dt_prevista_entrega_w,
				nr_seq_conta_financeira_w,
				ds_lote_fornec_w,
				ds_validade_w,
				0,
				cd_fornecedor_consig_w,
				nr_seq_marca_w,
				philips_contabil_pck.get_parametro_conta_contabil);

			if	(ds_barra_w is not null) then
				begin
				update  nota_fiscal_item
				set	cd_barra_material = ds_barra_w
				where   nr_item_nf = nr_item_nf_w
				and	nr_sequencia = nr_sequencia_w
				and	cd_material is not null
				and     ds_barras is null;
				end;
			end if;

			if	(nr_seq_criterio_rateio_w is not null) then
				ratear_item_nf(nr_sequencia_w, nr_item_nf_w, nr_seq_criterio_rateio_w, nm_usuario_p, trunc(dt_atualizacao_w));
			end if;
			end;
		end loop;
		close C04;
		end;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(181733);
	end if;
	end;
end if;

gerar_historico_nota_fiscal(nr_sequencia_w, nm_usuario_p, '7', WHEB_MENSAGEM_PCK.get_texto(279647) || nr_ordem_compra_p);

select 	count(*)
into	qt_itens_nota_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_w;

if	(qt_itens_nota_w = 0) then
	begin
	delete nota_fiscal where nr_sequencia = nr_sequencia_w;
	ds_erro_nota_p	:= WHEB_MENSAGEM_PCK.get_texto(279648);
	nr_sequencia_w	:= 0;
	end;
end if;

if	(qt_itens_nota_w > 0) then
	begin
	atualiza_total_nota_fiscal(nr_sequencia_w,nm_usuario_p);
	gerar_vencimento_nota_fiscal(nr_sequencia_w, nm_usuario_p);

	if	(ie_calcula_nf_w = 'S') then
		begin
		consistir_nota_fiscal(nr_sequencia_w,nm_usuario_p,ds_erro_item_w,ds_erro_nota_w);

		ds_erro_item_p := ds_erro_item_w;
		ds_erro_nota_p := ds_erro_nota_w;

		if 	(ds_erro_item_w is null) and
			(ds_erro_nota_w is null) then
			begin
			if	(ie_gera_lote_fornec_w = 'S') then
				begin
				consiste_gerar_lote_nf(	nr_sequencia_w, nm_usuario_p, ds_erro_nota_w);
				ds_erro_nota_w := Trim(ds_erro_nota_w);
				ie_consistiu_lote_w := 'S';
				if	(ds_erro_nota_w is null) then
					begin
					gerar_lote_fornec_nf(nr_sequencia_w, ie_grava_obs_lote_w, nm_usuario_p);
					Atualizar_Nota_Fiscal(nr_sequencia_w,'I',nm_usuario_p,ie_tipo_nota_w);
					end;
				else
					ds_erro_nota_p := ds_erro_nota_w;
				end if;
				end;
			else
				Atualizar_Nota_Fiscal(nr_sequencia_w,'I',nm_usuario_p,ie_tipo_nota_w);
			end if;
			end;
		end if;

		if 	((ds_erro_item_w is not null) or (ds_erro_nota_w is not null)) then
			begin
			delete nota_fiscal where nr_sequencia = nr_sequencia_w;

			if	(ie_consistiu_lote_w = 'N') then
				ds_erro_w := nr_sequencia_w;
			end if;

			nr_sequencia_w	:= 0;
			end;
		end if;
		end;
	end if;
	end;
end if;

if	(nr_sequencia_w > 0) then
	begin
	delete	ordem_compra_item_cb
	where	nr_ordem_compra = nr_ordem_compra_p
	and	ie_atende_recebe = 'R';

	if	(ds_erro_w is null) and
		(ds_erro_item_p is null) and
		(ds_erro_nota_p is null) then
		gerar_comunic_solic_transf(nr_ordem_compra_p,nr_sequencia_w,34,nm_usuario_p);
		gerar_email_solic_transf(nr_ordem_compra_p,nr_sequencia_w,44,nm_usuario_p);
	end if;
	end;
end if;

nr_sequencia_p 	:= nr_sequencia_w;
ds_erro_p  	:= ds_erro_w;

commit;
end gerar_nf_entrada_transferencia;
/
