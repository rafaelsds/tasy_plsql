create or replace
procedure define_taxa_porte(	cd_estabelecimento_p		number,
								cd_convenio_p       		number,
								cd_categoria_p       		varchar2,
								nr_porte_anestesico_p		number,
								cd_setor_atendimento_p		number,
								ie_tipo_atendimento_p		number,
								cd_taxa_cirurgia_p	out	number,
								ie_origem_proced_p	out	number,
								cd_tipo_acomodacao_p		number,
								nr_seq_proc_interno_p 	out	number,
								qt_procedimento_p	out	number) is

cd_setor_atendimento_w		number(5);
ie_tipo_atendimento_w		number(3);
cd_taxa_cirurgia_w		number(8);
ie_origem_proced_w		number(10);
ie_prioridade_w			number(2);
cd_taxa_cirurgia_w1		number(8);
ie_origem_proced_w1		number(10);
cd_tipo_acomodacao_w		number(4);
nr_seq_proc_interno_w		number(10);
qt_procedimento_w		number(10);
qt_procedimento_w1		number(10);

CURSOR C01 IS
	select	cd_setor_atendimento,
			ie_tipo_atendimento,
			cd_taxa_cirurgia,
			ie_origem_proced,
			1 ie_prioridade,
			cd_tipo_acomodacao,
			nr_seq_proc_interno,
			qt_procedimento
	from		convenio_taxa_cirurgia
	where		cd_estabelecimento	= cd_estabelecimento_p
	and		cd_convenio			= cd_convenio_p
	and		cd_categoria		= cd_categoria_p
	and		nr_porte_anestesico	= nr_porte_anestesico_p
	and		cd_setor_atendimento	= cd_setor_atendimento_p
	and		ie_tipo_atendimento	= ie_tipo_atendimento_p
	and		cd_tipo_acomodacao	is null
	union
	select	cd_setor_atendimento,
			ie_tipo_atendimento,
			cd_taxa_cirurgia,
			ie_origem_proced,
			2 ie_prioridade,
			cd_tipo_acomodacao,
			nr_seq_proc_interno,
			qt_procedimento
	from		convenio_taxa_cirurgia
	where		cd_estabelecimento	= cd_estabelecimento_p
	and		cd_convenio			= cd_convenio_p
	and		cd_categoria		= cd_categoria_p
	and		nr_porte_anestesico	= nr_porte_anestesico_p
	and		cd_setor_atendimento	= cd_setor_atendimento_p
	and		ie_tipo_atendimento	is null
	and		cd_tipo_acomodacao	is null
	union
	select	cd_setor_atendimento,
			ie_tipo_atendimento,
			cd_taxa_cirurgia,
			ie_origem_proced,
			3 ie_prioridade,
			cd_tipo_acomodacao,
			nr_seq_proc_interno,
			qt_procedimento
	from		convenio_taxa_cirurgia
	where		cd_estabelecimento	= cd_estabelecimento_p
	and		cd_convenio			= cd_convenio_p
	and		cd_categoria		= cd_categoria_p
	and		nr_porte_anestesico	= nr_porte_anestesico_p
	and		ie_tipo_atendimento	= ie_tipo_atendimento_p
	and		cd_setor_atendimento	is null
	and		cd_tipo_acomodacao	is null
	union
	select	cd_setor_atendimento,
			ie_tipo_atendimento,
			cd_taxa_cirurgia,
			ie_origem_proced,
			4 ie_prioridade,
			cd_tipo_acomodacao,
			nr_seq_proc_interno,
			qt_procedimento
	from		convenio_taxa_cirurgia
	where		cd_estabelecimento	= cd_estabelecimento_p
	and		cd_convenio			= cd_convenio_p
	and		cd_categoria		= cd_categoria_p
	and		nr_porte_anestesico	= nr_porte_anestesico_p
	and		ie_tipo_atendimento	is null
	and		cd_setor_atendimento	is null
	and		cd_tipo_acomodacao	is null
	union
	select	cd_setor_atendimento,
			ie_tipo_atendimento,
			cd_taxa_cirurgia,
			ie_origem_proced,
			5 ie_prioridade,
			cd_tipo_acomodacao,
			nr_seq_proc_interno,
			qt_procedimento
	from		convenio_taxa_cirurgia
	where		cd_estabelecimento	= cd_estabelecimento_p
	and		cd_convenio		= cd_convenio_p
	and		cd_categoria		= cd_categoria_p
	and		nr_porte_anestesico	= nr_porte_anestesico_p
	and		nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_p,0))	= nvl(cd_setor_atendimento_p,0)
	and		nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_p,0))		= nvl(ie_tipo_atendimento_p,0)
	and		nvl(cd_tipo_acomodacao, nvl(cd_tipo_acomodacao_p,0))		= nvl(cd_tipo_acomodacao_p,0)
	and		cd_tipo_acomodacao	is  not null
	order by 5;

BEGIN
cd_taxa_cirurgia_w1 := null;
ie_origem_proced_w1 := null;

	OPEN C01;
	LOOP
	FETCH C01 into
		cd_setor_atendimento_w,
		ie_tipo_atendimento_w,
		cd_taxa_cirurgia_w,
		ie_origem_proced_w,
		ie_prioridade_w,
		cd_tipo_acomodacao_w,
		nr_seq_proc_interno_w,
		qt_procedimento_w;
	exit when c01%notfound;
		begin
		if	cd_taxa_cirurgia_w1 is null then
			begin	
			cd_taxa_cirurgia_w1 := cd_taxa_cirurgia_w;
			ie_origem_proced_w1 := ie_origem_proced_w;
			qt_procedimento_w1  := qt_procedimento_w;
			
			if	(nvl(nr_seq_proc_interno_w,0) > 0) then
			obter_proc_tab_interno_conv(	nr_seq_proc_interno_w,
											cd_estabelecimento_p,
											cd_convenio_p,
											cd_categoria_p,
											null,
											cd_setor_atendimento_p,
											cd_taxa_cirurgia_w1,
											ie_origem_proced_w1,
											null,
											sysdate,
											null,
											null,
											null,
											null,
											null,
											null,
											null,
											null);
			nr_seq_proc_interno_p := nr_seq_proc_interno_w;
			end if;		

			end;
		end if;
		end;
	END LOOP;
	CLOSE C01;

cd_taxa_cirurgia_p := cd_taxa_cirurgia_w1;
ie_origem_proced_p := ie_origem_proced_w1;	
qt_procedimento_p  := qt_procedimento_w1;

END Define_Taxa_Porte;
/