create or replace
procedure DUV_CONSISTE_MENSAGEM
			(nr_seq_mensagem_p	number,
			ie_segmento_p		varchar2,
			nr_seq_segmento_p	number,
			nm_usuario_p		varchar2) is

nr_seq_versao_w			duv_mensagem.nr_seq_versao%type;
nr_seq_tipo_mensagem_w		duv_mensagem.nr_seq_tipo_mensagem%type;
duv_msg_inconsistencia_w	duv_msg_inconsistencia%rowtype;
cd_exp_campo_w			dic_expressao.cd_expressao%type;
nr_seq_grupo_w			tabela_visao_grupo.nr_sequencia%type;
nr_seq_subgrupo_w		tabela_visao_grupo.nr_sequencia%type;
vl_retorno_w			varchar2(255);
vl_resultado_w			varchar2(255);
ds_grupo_w			varchar2(255);
ds_subgrupo_w			varchar2(255);
ds_sql_w			varchar2(500);
ds_campo_w			varchar2(500);
ds_erro_w			varchar2(2000);
c001				integer;

cursor	c01 is
select	nr_sequencia,
	ie_tipo_segmento
from	duv_mensagem_segmento a
where	a.nr_seq_mensagem = nr_seq_mensagem_p
and	a.ie_tipo_segmento = nvl(lower(ie_segmento_p),a.ie_tipo_segmento)
and	a.ie_tipo_segmento not in ('swi');

c01_w	c01%rowtype;

cursor c02 is
select	a.nm_atributo,
	a.nm_tabela,
	a.ds_tag
from	duv_tipo_mens_seg_campo a,
	duv_tipo_mens_segmento b,
	duv_versao_mensagem c
where	a.nr_seq_msg_seg 	= b.nr_sequencia
and	b.nr_seq_versao_mens 	= c.nr_sequencia
and	c.nr_seq_tipo_mensagem 	= nr_seq_tipo_mensagem_w
and	c.nr_seq_versao		= nr_seq_versao_w
and	a.ie_obrigatorio	= 'S'
and	b.ie_tipo_segmento	= c01_w.ie_tipo_segmento;

c02_w	c02%rowtype;

begin

if	(nvl(nr_seq_segmento_p,0) > 0) then

	delete 	from duv_msg_inconsistencia
	where	nr_seq_mensagem 	= nr_seq_mensagem_p
	and	nr_seq_duv_segmento 	= nr_seq_segmento_p
	and	ie_sistema_validacao	= 'TASY';

else
	delete 	from duv_msg_inconsistencia
	where	nr_seq_mensagem 	= nr_seq_mensagem_p
	and	ie_sistema_validacao	= 'TASY';
end if;


select	nr_seq_versao,
	nr_seq_tipo_mensagem
into	nr_seq_versao_w,
	nr_seq_tipo_mensagem_w
from	duv_mensagem
where	nr_sequencia	= nr_seq_mensagem_p;

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	open C02;
	loop
	fetch C02 into
		c02_w;
	exit when C02%notfound;

		if	(nvl(nr_seq_segmento_p,0) > 0) then
			ds_sql_w := 	' select to_char('|| c02_w.nm_Atributo ||') from '|| c02_w.nm_tabela||
					' where nr_sequencia = '|| nr_seq_segmento_p||' and rownum = 1';
		else
			ds_sql_w := 	' select to_char('|| c02_w.nm_Atributo ||') from '|| c02_w.nm_tabela||
					' where NR_SEQ_MENSAGEM = '|| nr_seq_mensagem_p||' and rownum = 1';
		end if;

		c001	:= dbms_sql.open_cursor;
		dbms_sql.parse(c001,ds_sql_w,dbms_sql.native);
		dbms_sql.define_column(c001, 1, vl_retorno_w, 255);
		vl_resultado_w	:= dbms_sql.execute(c001);
		vl_resultado_w	:= dbms_sql.fetch_rows(c001);
		dbms_sql.column_value(c001,1,vl_retorno_w);
		dbms_sql.close_cursor(c001);

		if	(trim(vl_retorno_w) is null) then

			begin
				select	nvl(a.cd_exp_label,a.cd_exp_label_longo),
					a.nr_seq_visao_grupo,
					a.nr_seq_visao_subgrupo
				into	cd_exp_campo_w,
					nr_seq_grupo_w,
					nr_seq_subgrupo_w
				from	tabela_visao_atributo a,
					tabela_visao b,
					tabela_sistema c
				where	c.nm_tabela 	= b.nm_tabela
				and	b.nr_sequencia	= a.nr_sequencia
				and	c.nm_tabela	= c02_w.nm_tabela
				and	a.nm_atributo	= c02_w.nm_Atributo
				and	(a.cd_exp_label	is not null or a.cd_exp_label_longo is not null)
				and	rownum  = 1;
			exception
			when others then
				cd_exp_campo_w		:= null;
				nr_seq_grupo_w		:= null;
				nr_seq_subgrupo_w	:= null;
			end;


			if	(cd_exp_campo_w is not null) then

				ds_campo_w	:= substr(obter_desc_expressao(cd_exp_campo_w),1,200);

				if	(nr_seq_grupo_w is not null) then

					select	substr(max(obter_desc_expressao(CD_EXP_TITULO)),1,200)
					into	ds_grupo_w
					from	tabela_visao_grupo
					where	nr_sequencia	= nr_seq_grupo_w;

					select	substr(max(obter_desc_expressao(CD_EXP_TITULO)),1,200)
					into	ds_subgrupo_w
					from	tabela_visao_grupo
					where	nr_sequencia	= nr_seq_subgrupo_w;

					if	(ds_subgrupo_w is not null) then
						ds_campo_w := substr(ds_subgrupo_w ||' > '|| ds_campo_w,1,500);
					end if;
					if	(ds_grupo_w is not null) then
						ds_campo_w := substr(ds_grupo_w ||' > '|| ds_campo_w,1,500);
					end if;

				end if;

			else
				ds_campo_w := c02_w.nm_Atributo;
			end if;

			ds_erro_w := substr(wheb_mensagem_pck.GET_TEXTO(1031881,'ds_campo='||chr(39)||ds_campo_w||chr(39)),1,2000);

			select	duv_msg_inconsistencia_seq.nextval
			into	duv_msg_inconsistencia_w.nr_sequencia
			from	dual;
			duv_msg_inconsistencia_w.dt_atualizacao		:= sysdate;
			duv_msg_inconsistencia_w.nm_usuario		:= nm_usuario_p;
			duv_msg_inconsistencia_w.dt_atualizacao_nrec	:= sysdate;
			duv_msg_inconsistencia_w.nm_usuario_nrec	:= nm_usuario_p;
			duv_msg_inconsistencia_w.nr_seq_mensagem	:= nr_seq_mensagem_p;
			duv_msg_inconsistencia_w.nr_seq_msg_segmento	:= c01_w.nr_sequencia;
			duv_msg_inconsistencia_w.nr_seq_duv_segmento	:= nr_seq_segmento_p;
			duv_msg_inconsistencia_w.ds_campo		:= c02_w.nm_Atributo;
			duv_msg_inconsistencia_w.ds_erro		:= ds_erro_w;
			duv_msg_inconsistencia_w.ds_tag			:= c02_w.ds_tag;
			duv_msg_inconsistencia_w.ie_nivel		:= '1';
			duv_msg_inconsistencia_w.ds_identificador	:= obter_desc_expressao(629377);--Campo obrigatório
			duv_msg_inconsistencia_w.ie_sistema_validacao	:= 'TASY';

			insert into duv_msg_inconsistencia values duv_msg_inconsistencia_w;

		end if;

	end loop;
	close C02;

end loop;
close C01;

update	duv_mensagem
set	dt_consistencia = sysdate
where	nr_sequencia	= nr_seq_mensagem_p;

commit;

end DUV_CONSISTE_MENSAGEM;
/