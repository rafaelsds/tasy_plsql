create or replace
procedure OBTER_DADOS_AMAMENTACAO(nr_atendimento_p	in number,
									ie_amam_contato_pele_p	out varchar2,
									ie_amam_colostro_p	out varchar2,
									ie_amam_tempo_succao_p	out varchar2,
									ie_primeira_mamada_p	out varchar2,
									ie_momento_mamada_p	out varchar2) is 

begin

ie_amam_contato_pele_p	:= '';
ie_amam_colostro_p	:= '';
ie_amam_tempo_succao_p	:= '';
ie_primeira_mamada_p	:= '';
ie_momento_mamada_p	:= '';

SELECT	MAX(SUBSTR(obter_valor_dominio(2947,A.IE_AMAM_CONTATO_PELE),1,255)) ie_amam_contato_pele,
		MAX(SUBSTR(obter_valor_dominio(2948,A.IE_AMAM_COLOSTRO),1,255)) ie_amam_colostro,
		MAX(SUBSTR(obter_valor_dominio(2949,A.IE_AMAM_TEMPO_SUCCAO),1,255)) IE_AMAM_TEMPO_SUCCAO,
		MAX(SUBSTR(obter_valor_dominio(4184,A.IE_PRIMEIRA_MAMADA),1,255)) IE_PRIMEIRA_MAMADA,
		MAX(SUBSTR(obter_valor_dominio(5929,A.IE_MOMENTO_MAMADA),1,255)) IE_MOMENTO_MAMADA
INTO	ie_amam_contato_pele_p,
		ie_amam_colostro_p,
		ie_amam_tempo_succao_p,
		ie_primeira_mamada_p,
		ie_momento_mamada_p		
FROM	nascimento_amamentacao a
WHERE	a.nr_Sequencia = (SELECT 	MAX(b.nr_Sequencia)
			FROM	nascimento_amamentacao b
			WHERE	b.nr_Atendimento = nr_atendimento_p
			AND	NVL(a.IE_SITUACAO,'A') = 'A');

commit;

end OBTER_DADOS_AMAMENTACAO;
/