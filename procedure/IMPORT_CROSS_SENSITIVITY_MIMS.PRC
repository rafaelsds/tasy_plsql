create or replace 
procedure import_cross_sensitivity_mims (	nr_version_p in number, 
						nm_usuario_p in varchar2) 
						
is

cd_sense_count_w			NUMBER(10);
cd_cross_count_w			NUMBER(10);

 
		CURSOR	c_material IS 
		SELECT	cd_material, 
			nr_seq_ficha_tecnica 
		FROM	material 
		WHERE	nr_seq_ficha_tecnica IS NOT NULL; 



		CURSOR	c_gencode(cd_material_p	number) IS   
		select	c.cd_dcb
		from	medic_ficha_tecnica a 
		inner join dcb_medic_controlado c on c.nr_sequencia = a.nr_seq_dcb
		where	a.NR_SEQUENCIA = (select NR_SEQ_FICHA_TECNICA from material where cd_material = cd_material_p) and regexp_like(c.cd_dcb, '^[0-9]+$')
		union
		select c.cd_dcb
		from medic_ficha_tecnica a
		inner join dcb_medic_controlado c on c.nr_sequencia = a.nr_seq_dcb
		where a.NR_SEQ_SUPERIOR = (select NR_SEQ_FICHA_TECNICA from material where cd_material = cd_material_p) and regexp_like(c.cd_dcb, '^[0-9]+$');
    
		CURSOR	c_substance_gencode(gencode_P	number) IS  
		select	distinct	gs2.gencode
		from	substance_class sc1 
		inner	join	generic_substance gs1 on sc1.substance_class_id = gs1.substance_class_id 
		inner	join	sub_cross_sensitivity scc on sc1.substance_class_id = scc.substance_class_id1 
		inner	join	substance_class sc2 on scc.substance_class_id2 = sc2.substance_class_id 
		inner	join	generic_substance gs2 on sc2.substance_class_id = gs2.substance_class_id 
		where	gs1.gencode = gencode_P;
    
		cursor	c_gencode_sequence(gencode_P	number) is 
		select	b.nr_sequencia
		from	medic_ficha_tecnica b,
			dcb_medic_controlado c
		where	b.nr_seq_dcb = c.nr_sequencia
		and	c.cd_dcb = to_char(gencode_P); 
    
begin
  BEGIN
		select count(*) 
		into	cd_sense_count_w 
		from	sub_cross_sensitivity 
		where	version = nr_version_p;
		EXCEPTION WHEN NO_DATA_FOUND 
		THEN	cd_sense_count_w := 0;
   END;
  
	IF(cd_sense_count_w >0)	THEN
	
		FOR r_material IN c_material LOOP   
    
			FOR r_gencode IN c_gencode(r_material.CD_MATERIAL) LOOP

				FOR r_substance_gencode IN c_substance_gencode(r_gencode.cd_dcb) LOOP
				
					FOR r_gencode_sequence IN c_gencode_sequence(r_substance_gencode.gencode) LOOP
					BEGIN
					   select count(*) into cd_cross_count_w from medic_reacao_cruzada 
						where nr_seq_ficha_tecnica = r_gencode_sequence.nr_sequencia
						and cd_material = r_material.cd_material;
					  EXCEPTION 
					  WHEN	NO_DATA_FOUND 
					  THEN	cd_cross_count_w := 0;
					END;
					IF (cd_cross_count_w = 0) then
					insert into medic_reacao_cruzada 
								(nr_sequencia         , 
								dt_atualizacao       , 
								nm_usuario           , 
								dt_atualizacao_nrec  , 
								nm_usuario_nrec      , 
								nr_seq_ficha_tecnica , 
								cd_material)
					values	(medic_reacao_cruzada_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								r_gencode_sequence.nr_sequencia,
								r_material.cd_material   );
					END IF;
					END LOOP;
				    
             END LOOP;
        END LOOP;
      END LOOP;
   END IF;
   
  
end import_cross_sensitivity_mims;
/

