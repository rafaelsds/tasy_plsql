create or replace
procedure fis_gerar_arq_E110_icmsipi(	nr_seq_controle_p	number,
					nr_linha_p		in out number	) is 

-- VARIABLES
ds_linha_w  varchar2(8000);
ds_sep_w    varchar2(1) 	:= '|';
qt_cursor_w number(10)		:= 0;
nr_vetor_w  number(10)		:= 0;
nr_sequencia_w number(10) 	:= 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

-- FIS_EFD_ICMSIPI_ARQUIVO
nr_linha_w fis_efd_icmsipi_arquivo.nr_linha%type;

cursor c_reg_E110 is
select	*
from	fis_efd_icmsipi_E110
where	nr_seq_controle = nr_seq_controle_p;

/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_E110*/
type reg_c_reg_E110 is table of fis_efd_icmsipi_E110%RowType;
vetRegE110 reg_c_reg_E110;

/*Cria��o do array com o tipo sendo da tabela eespecificada - fis_efd_icmsipi_arquivo */
type registro is table of fis_efd_icmsipi_arquivo%rowtype index by pls_integer;
fis_registros_w registro;

begin

nm_usuario_w := Obter_Usuario_Ativo;
nr_linha_w   := nr_linha_p;

open c_reg_E110;
loop
fetch c_reg_E110 bulk collect into vetRegE110 limit 1000;
	for i in 1 .. vetRegE110.Count loop
		begin

		/*Incrementa a variavel para o array*/
		qt_cursor_w := qt_cursor_w + 1;	

		nr_linha_w := nr_linha_w + 1;
		ds_linha_w := substr(ds_sep_w || vetRegE110(i).cd_reg || 
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_tot_debitos ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_aj_debitos ), 1, 30) || 
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_tot_aj_debitos ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_estornos_cred ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_tot_creditos ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_aj_creditos ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_tot_aj_creditos ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_estornos_deb ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_sld_credor_ant ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_sld_apurado ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_tot_ded ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_icms_recolher ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_sld_credor_transportar ), 1, 30) ||
				     ds_sep_w || substr(sped_obter_campo_valor(vetRegE110(i).vl_deb_esp ), 1, 30) || ds_sep_w,
				     1,
				     8000);

		/*   Atualiza o sequencial de ordena��o de linhas do arquivo */
		Select	fis_efd_icmsipi_arquivo_seq.nextval
		into	nr_sequencia_w
		from	dual;  
				     
		fis_registros_w(qt_cursor_w).nr_sequencia		:= nr_sequencia_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao_nrec	:= sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario_nrec		:= nm_usuario_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao		:= sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario			:= nm_usuario_w;
		fis_registros_w(qt_cursor_w).nr_seq_controle		:= nr_seq_controle_p;
		fis_registros_w(qt_cursor_w).nr_linha			:= nr_linha_w;
		fis_registros_w(qt_cursor_w).ds_arquivo			:= substr(ds_linha_w,1,4000);
		fis_registros_w(qt_cursor_w).ds_arquivo_compl		:= substr(ds_linha_w,4001,4000);
		fis_registros_w(qt_cursor_w).cd_registro		:=  vetRegE110(i).cd_reg;

		if (nr_vetor_w >= 1000) then
			begin
			/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_E110 */
			forall i in fis_registros_w.first .. fis_registros_w.last
				insert into fis_efd_icmsipi_arquivo values fis_registros_w(i);

			nr_vetor_w := 0;
			fis_registros_w.delete;

			commit;

			end;
		end if;

		/*incrementa variavel para realizar o forall quando chegar no valor limite*/
		nr_vetor_w := nr_vetor_w + 1;

		end;
	end loop;
exit when c_reg_E110%notfound;
end loop;
close c_reg_E110;

if (fis_registros_w.count > 0) then
	begin
	/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_arquivo values fis_registros_w (i);

	fis_registros_w.delete;

	commit;

	end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

nr_linha_p := nr_linha_w;

end fis_gerar_arq_E110_icmsipi;
/