create or replace
procedure IMP_EXTRATO_003_JS_HTML(
		dt_movto_p	date,
		vl_extrato_p	number,
		nr_sequencia_p	number,
		ie_deb_cred_p	varchar2) is 
begin
update 	banco_extrato 
set 	dt_final = trunc(dt_movto_p, 'dd'), 
	vl_saldo_final = ((vl_extrato_p / 100) * decode(ie_deb_cred_p, 'D', -1, 1)) 
where 	nr_sequencia = nr_sequencia_p;
end IMP_EXTRATO_003_JS_HTML;
/
