create or replace 
procedure alterar_status_conta_bpa(
				nr_interno_conta_p			number,
				nm_usuario_p			varchar2) is
begin
	if (nr_interno_conta_p is not null) then
		update	conta_paciente 
		set	ie_status_acerto		= 1,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_interno_conta		= nr_interno_conta_p;
		commit;
	end if;
end alterar_status_conta_bpa;
/