create or replace procedure copy_to_cpoe_material(nr_sequencia_p	number,
                                                  nr_atendimento_p	number,
                                                  nm_usuario_p	varchar2,
                                                  nr_seq_proc_edit_p number) is
												  
cd_unidade_medida_w   prescr_proc_item_sug.cd_unidade_medida%type;
qt_dose_w             prescr_proc_item_sug.qt_dose%type;
cd_intervalo_w        prescr_proc_item_sug.cd_intervalo%type;
ie_via_aplicacao_w    prescr_proc_item_sug.ie_via_aplicacao%type;
cd_material_w         prescr_proc_item_sug.cd_material%type;
qt_register_w         number(5);

begin

if(nr_sequencia_p is not null)then

	select  cd_unidade_medida,
		  qt_dose,
		  cd_intervalo,
		  ie_via_aplicacao,
		  cd_material
	into    cd_unidade_medida_w,
		  qt_dose_w,
		  cd_intervalo_w,
		  ie_via_aplicacao_w,
		  cd_material_w
	from    prescr_proc_item_sug
	where   nr_sequencia = nr_sequencia_p;

	select  count(1)
	into    qt_register_w
	from    prescr_proc_item_sug
	where   nr_sequencia = nr_sequencia_p;
      
if (qt_register_w > 0) then

	insert into cpoe_material
      (nr_sequencia, 
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      cd_unidade_medida,
      qt_dose,
      cd_intervalo,
      ie_via_aplicacao,
      cd_material,
      nr_atendimento,
      nr_seq_proc_edit,
      nr_seq_sugest) 
    values
	  (cpoe_material_seq.nextval, 
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      cd_unidade_medida_w,
      qt_dose_w,
      cd_intervalo_w,
      ie_via_aplicacao_w,
      cd_material_w,
      nr_atendimento_p,
      nr_seq_proc_edit_p,
      nr_sequencia_p);

    commit;

  end if;

end if;

end copy_to_cpoe_material;
/
