create or replace
procedure tx_liberar_lista	(nr_seq_receptor_p	number,
				dt_liberacao_p		date,
				ie_tipo_prof_lib_p	varchar2,
				nm_usuario_p		varchar2) is

begin
if	(nvl(nr_seq_receptor_p,0) > 0) and
	(dt_liberacao_p is not null) and
	(ie_tipo_prof_lib_p is not null) then
	
	insert into Tx_liberacao_lista(	nr_sequencia,
					ie_tipo_prof_lib,
					dt_liberacao,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_receptor)
				values	(Tx_liberacao_lista_seq.nextval,
					ie_tipo_prof_lib_p,
					dt_liberacao_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_receptor_p);
end if;

end;
/