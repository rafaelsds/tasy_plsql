create or replace
procedure ALTERAR_ITEM_LOTE_AUDIT
			(nr_seq_hist_item_p	in	number,
			cd_motivo_glosa_p	in	number,
			cd_resposta_p		in	number,
			cd_setor_responsavel_p	in	number,
			ie_acao_glosa_p		in	varchar2,
			vl_glosa_p		in	number,
			ds_observacao_p		in	varchar2,
	 		nm_usuario_p		in	varchar2,
			ie_tipo_glosa_p		in	varchar2,
			ie_retorno_p		out	varchar2) is

/*	ie_retorno_p

S	Alterado
N	N�o alterado

*/

vl_glosa_w		number(15,2);
vl_amenor_w		number(15,2);
ie_acao_glosa_w		varchar2(1);
cd_motivo_glosa_w	number(5);
cd_resposta_w		number(10);
cd_setor_responsavel_w	number(5);
ds_observacao_w		varchar2(4000);
vl_glosa_informada_w	number(15,2);
cd_estabelecimento_w	number(4);
vl_saldo_w		number(15,2);
ie_tipo_glosa_w		varchar2(1);

ie_acao_final_w		varchar2(1);
ie_tipo_glosa_final_w	varchar2(1);
cd_motivo_final_w	number(5);
cd_resposta_final_w	number(10);
cd_setor_final_w	number(5);
ds_observacao_final_w	varchar2(4000);

ie_consistencia_w	varchar2(1)	:= 'S';
ie_glosa_maior_w	varchar2(1);


begin

select	max(a.vl_glosa),
	max(a.vl_amenor),
	max(a.ie_acao_glosa),
	max(a.cd_motivo_glosa),
	max(a.cd_resposta),
	max(a.cd_setor_responsavel),
	max(a.ds_observacao),
	max(a.vl_glosa_informada),
	max(d.cd_estabelecimento),
	max(a.vl_saldo),
	max(a.ie_tipo_glosa)
into	vl_glosa_w,
	vl_amenor_w,
	ie_acao_glosa_w,
	cd_motivo_glosa_w,
	cd_resposta_w,
	cd_setor_responsavel_w,
	ds_observacao_w,
	vl_glosa_informada_w,
	cd_estabelecimento_w,
	vl_saldo_w,
	ie_tipo_glosa_w
from	lote_auditoria d,
	lote_audit_hist c,
	lote_audit_hist_guia b,
	lote_audit_hist_item a
where	c.nr_seq_lote_audit	= d.nr_sequencia
and	b.nr_seq_lote_hist	= c.nr_sequencia
and	a.nr_seq_guia		= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_hist_item_p;

ie_acao_final_w	:= ie_acao_glosa_p;

if	/*(ie_acao_final_w	is not null) and
	((cd_motivo_glosa_p	is null) or
	(cd_setor_responsavel_p	is null) or
	((ds_observacao_p is null) and (cd_resposta_p is null)) or*/
	((ie_acao_final_w = 'P') and ((vl_glosa_w = 0) or (vl_amenor_w = 0))) then

	ie_acao_final_w		:= ie_acao_glosa_w;
	ie_consistencia_w	:= 'N';

	if	(cd_motivo_glosa_p	is null) then
		cd_motivo_final_w	:= cd_motivo_glosa_w;
	end if;

	if	(cd_resposta_p		is null) then
		cd_resposta_final_w	:= cd_resposta_w;
	end if;

	if	(cd_setor_responsavel_p	is null) then
		cd_setor_final_w	:= cd_setor_responsavel_w;
	end if;

	if	(ds_observacao_p	is null) then
		ds_observacao_final_w	:= ds_observacao_w;
	end if;
	
	if	(ie_tipo_glosa_p	is null) then
		ie_tipo_glosa_final_w	:= ie_tipo_glosa_w;
	end if;


else

	cd_motivo_final_w	:= cd_motivo_glosa_p;
	cd_resposta_final_w	:= cd_resposta_p;
	ie_tipo_glosa_final_w	:= ie_tipo_glosa_p;
	cd_setor_final_w	:= cd_setor_responsavel_p;
	ds_observacao_final_w	:= ds_observacao_p;

end if;

obter_param_usuario(69,32,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_glosa_maior_w);

if	(vl_glosa_p	is not null) and
	((nvl(ie_glosa_maior_w,'N') = 'S') or (vl_saldo_w >= vl_glosa_p)) then

	vl_glosa_informada_w	:= vl_glosa_p;

end if;

if	(ie_acao_final_w	= 'A') then

	vl_glosa_w		:= vl_glosa_informada_w;
	vl_amenor_w		:= 0;

elsif	(ie_acao_final_w	= 'R') then

	vl_glosa_w		:= 0;
	vl_amenor_w		:= vl_glosa_informada_w;

end if;

update	lote_audit_hist_item
set	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	vl_glosa_informada	= vl_glosa_informada_w,
	vl_glosa		= vl_glosa_w,
	vl_amenor		= vl_amenor_w,
	ie_acao_glosa		= ie_acao_final_w,
	cd_motivo_glosa		= cd_motivo_final_w,
	cd_resposta		= cd_resposta_final_w,
	ie_tipo_glosa		= ie_tipo_glosa_final_w,
	cd_setor_responsavel	= cd_setor_final_w,
	ds_observacao		= ds_observacao_final_w
where	nr_sequencia		= nr_seq_hist_item_p;

ie_retorno_p	:= ie_consistencia_w;

commit;

end ALTERAR_ITEM_LOTE_AUDIT;
/