create or replace procedure obter_caso_congelacao (	nr_prescricao_p		in	number,
					ds_mensagem_p		out	varchar,
					nr_seq_lab_p		out	varchar,
					nr_seq_amostra_princ_p	in	number) is
				
nr_seq_lab_w	varchar2(20);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(774280, null, wheb_usuario_pck.get_nr_seq_idioma);--Deseja vincular a amostra ao exame de congelação?				

					
begin
ds_mensagem_p 	:= '';
nr_seq_lab_p	:= '';

select 	max(c.nr_seq_lab)
into	nr_seq_lab_w
from   	prescr_proc_peca a,
	tipo_amostra b,
	prescr_procedimento c,
  prescr_medica d
where  	a.nr_seq_tipo 	= b.nr_sequencia
and	a.nr_prescricao = c.nr_prescricao
and	a.nr_seq_prescr	= c.NR_sequencia
and 	d.nr_prescricao = c.nr_prescricao
and	d.nr_atendimento = obter_atendimento_prescr(nr_prescricao_p)
and	a.nr_seq_amostra_princ	= nr_seq_amostra_princ_p
and	b.ie_tipo_exame = 5;

if	(nr_seq_lab_w is not null) then
	ds_mensagem_p 	:= expressao1_w;
	nr_seq_lab_p 	:= nr_seq_lab_w;
end if;	

end obter_caso_congelacao;
/