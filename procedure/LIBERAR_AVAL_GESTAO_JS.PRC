create or replace
procedure liberar_aval_gestao_js(	nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p is not null)then
	begin
	
	update 	avaliacao_gestao_vaga 
	set 	dt_liberacao = sysdate 
	where 	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end liberar_aval_gestao_js;
/