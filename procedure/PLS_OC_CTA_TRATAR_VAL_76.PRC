create or replace
procedure pls_oc_cta_tratar_val_76( dados_regra_p   pls_tipos_ocor_pck.dados_regra,
          nr_id_transacao_p pls_selecao_ocor_cta.nr_id_transacao%type,
          nm_usuario_p    usuario.nm_usuario%type) is


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar se o profissional executante est� vinculado ao prestador executor

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjkruk OS 734664
Altera��o: Cria��o da procedure
------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_tb_selecao_w        pls_tipos_ocor_pck.dados_table_selecao_ocor;
nr_seq_sel_anterior_w   pls_oc_cta_selecao_ocor_v.nr_sequencia%type;
ie_valido_anterior_w    varchar2(1);
nr_idx_w              pls_integer;

cursor C01 (  nr_seq_oc_cta_comb_p  dados_regra_p.nr_sequencia%type) is
  select  a.ie_valida ie_valida,
      nvl(a.ie_tipo_profissional,'PE') ie_tipo_profissional
  from  pls_oc_cta_val_exec_prest a
  where a.nr_seq_oc_cta_comb  = nr_seq_oc_cta_comb_p;

cursor C02 (  nr_id_transacao_pc  pls_oc_cta_selecao_ocor_v.nr_sequencia%type,
    ie_tipo_evento_pc pls_oc_cta_combinada.ie_evento%type) is
  select  x.nr_sequencia nr_seq_selecao,
    b.dt_inclusao,
    b.dt_exclusao,
    a.dt_atendimento,
    a.dt_atendimento_imp
  from  pls_oc_cta_selecao_ocor_v x,
    pls_conta_ocor_v    a,
    pls_prestador_medico    b
  where x.ie_valido     = 'S'
  and x.nr_id_transacao   = nr_id_transacao_pc
  and a.nr_sequencia      = x.nr_seq_conta
  and a.nr_seq_prestador_exec_imp_ref is not null
  and a.cd_medico_executor    is not null
  and b.nr_seq_prestador    = a.nr_seq_prestador_exec_imp_ref
  and b.cd_medico     = a.cd_medico_executor
  and b.ie_situacao     = 'A'
  and ie_tipo_evento_pc   = 'IMP'
  union all
  select  x.nr_sequencia nr_seq_selecao,
    b.dt_inclusao,
    b.dt_exclusao,
    a.dt_atendimento,
    a.dt_atendimento_imp
  from  pls_oc_cta_selecao_ocor_v x,
    pls_conta_ocor_v    a,
    pls_prestador_medico    b
  where x.ie_valido     = 'S'
  and x.nr_id_transacao   = nr_id_transacao_pc
  and a.nr_sequencia      = x.nr_seq_conta
  and a.nr_seq_prestador_exec   is not null
  and a.cd_medico_executor    is not null
  and b.nr_seq_prestador    = a.nr_seq_prestador_exec
  and b.cd_medico     = a.cd_medico_executor
  and b.ie_situacao     = 'A'
  and ie_tipo_evento_pc   <> 'IMP';
  
cursor C03 (  nr_id_transacao_pc  pls_oc_cta_selecao_ocor_v.nr_sequencia%type) is
with query_tmp as(
  select  x.nr_sequencia nr_seq_selecao,
    a.dt_atendimento,
    a.nr_seq_prestador_exec_imp_ref nr_seq_prest_exec,
    c.cd_medico_imp cd_medico,
    ( select  count(1) 
      from  pls_prestador_medico m
      where   m.cd_medico     = c.cd_medico_imp
      and m.nr_seq_prestador  = a.nr_seq_prestador_exec_imp_ref
      and m.ie_situacao     = 'A') qt_prof_valido
  from  pls_oc_cta_selecao_ocor_v x,
    pls_conta   a,
    pls_conta_proc  b,
    pls_proc_participante c
  where x.ie_valido     = 'S'
  and x.nr_id_transacao   = nr_id_transacao_pc
  and a.nr_sequencia      = x.nr_seq_conta
  and a.nr_sequencia      = b.nr_seq_conta
  and b.nr_sequencia      = c.nr_seq_conta_proc
  order by x.nr_sequencia
)
select  a.nr_seq_selecao,
    b.dt_inclusao,
    b.dt_exclusao,
    a.dt_atendimento,
    qt_prof_valido
from  query_tmp a,
    pls_prestador_medico  b
where   b.cd_medico(+)    = a.cd_medico
and   b.nr_seq_prestador(+) = a.nr_seq_prest_exec
and   b.ie_situacao(+)  = 'A';

cursor C04 (  nr_id_transacao_pc  pls_oc_cta_selecao_ocor_v.nr_sequencia%type) is
with query_tmp as(
  select  x.nr_sequencia nr_seq_selecao,
    a.dt_atendimento,
    a.nr_seq_prestador_exec,
    c.cd_medico,
    ( select  count(1) 
      from  pls_prestador_medico m
      where   m.cd_medico     = c.cd_medico
      and m.nr_seq_prestador  = a.nr_seq_prestador_exec
      and m.ie_situacao     = 'A') qt_prof_valido
  from  pls_oc_cta_selecao_ocor_v x,
    pls_conta   a,
    pls_conta_proc  b,
    pls_proc_participante c
  where x.ie_valido     = 'S'
  and x.nr_id_transacao   = nr_id_transacao_pc
  and a.nr_sequencia      = x.nr_seq_conta
  and a.nr_sequencia      = b.nr_seq_conta
  and b.nr_sequencia      = c.nr_seq_conta_proc
  order by x.nr_sequencia
)
select  a.nr_seq_selecao,
    b.dt_inclusao,
    b.dt_exclusao,
    a.dt_atendimento,
    qt_prof_valido
from  query_tmp a,
    pls_prestador_medico  b
where   b.cd_medico(+)    = a.cd_medico
and   b.nr_seq_prestador(+) = a.nr_seq_prestador_exec
and   b.ie_situacao(+)  = 'A';

Cursor C05( nr_id_transacao_pc  pls_oc_cta_selecao_ocor_v.nr_sequencia%type) is
  select  x.nr_sequencia nr_seq_selecao,
    b.dt_inclusao,
    b.dt_exclusao,
    a.dt_atendimento,
    ( select  count(1) 
      from  pls_prestador_medico m
      where   m.cd_medico     = a.cd_medico_executor
      and m.nr_seq_prestador  = a.nr_seq_prestador_exec 
      and m.ie_situacao     = 'A') qt_prof_valido
  from  pls_oc_cta_selecao_ocor_v x,
      pls_conta   a,
      pls_prestador_medico  b
  where x.ie_valido     = 'S'
  and x.nr_id_transacao   = nr_id_transacao_pc
  and a.nr_sequencia      = x.nr_seq_conta
  and b.nr_seq_prestador(+) = a.nr_seq_prestador_exec
  and b.cd_medico(+)      = a.cd_medico_executor
  and b.ie_situacao(+)    = 'A'
  and not exists(   select  1
            from  pls_conta_proc e,
                pls_proc_participante f
            where e.nr_seq_conta = a.nr_sequencia
            and   e.nr_sequencia = f.nr_seq_conta_proc);
        
Cursor C06( nr_id_transacao_pc  pls_oc_cta_selecao_ocor_v.nr_sequencia%type) is
  select  x.nr_sequencia nr_seq_selecao,
    b.dt_inclusao,
    b.dt_exclusao,
    a.dt_atendimento_imp dt_atendimento,
    ( select  count(1) 
      from  pls_prestador_medico m
      where   m.cd_medico     = a.cd_medico_executor_imp
      and   m.nr_seq_prestador  = a.nr_seq_prestador_exec_imp_ref 
      and   m.ie_situacao     = 'A') qt_prof_valido
  from  pls_oc_cta_selecao_ocor_v x,
      pls_conta   a,
      pls_prestador_medico  b
  where x.ie_valido     = 'S'
  and   x.nr_id_transacao   = nr_id_transacao_pc
  and   a.nr_sequencia      = x.nr_seq_conta
  and   b.nr_seq_prestador(+) = a.nr_seq_prestador_exec_imp_ref
  and   b.cd_medico(+)      = a.cd_medico_executor_imp
  and   b.ie_situacao(+)    = 'A'
  and   not exists(   select  1
              from  pls_conta_proc e,
                  pls_proc_participante f
              where e.nr_seq_conta = a.nr_sequencia
              and   e.nr_sequencia = f.nr_seq_conta_proc);

begin
if  (dados_regra_p.nr_sequencia is not null)  then
  for r_C01_w in C01(dados_regra_p.nr_sequencia) loop
    if  (r_C01_w.ie_valida = 'S') then
    begin
      pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
      nr_seq_sel_anterior_w   := 0;
      ie_valido_anterior_w  := 'N';
      nr_idx_w:=  0;
      -- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
      -- Considera todos como v�lidos, o que n�o trouxer no cursor vai permanecer como v�lido
      -- o que trouxer no cursor pode ser alterado para N dependendo das datas e inclus�o e exclus�o
      pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p, 'S');

        if  (r_C01_w.ie_tipo_profissional = 'PE') then
          for r_C02_w in  C02(nr_id_transacao_p, dados_regra_p.ie_evento) loop

            dados_tb_selecao_w.ie_valido(nr_idx_w):=  'N';
            if  (dados_regra_p.ie_evento = 'IMP') then
            if  (r_C02_w.dt_atendimento_imp < r_C02_w.dt_inclusao) then
              dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
            elsif (r_C02_w.dt_exclusao is not null and r_C02_w.dt_atendimento_imp > r_C02_w.dt_exclusao) then
              dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
            end if;
            else
            if  (r_C02_w.dt_atendimento < r_C02_w.dt_inclusao) then
              dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
            elsif (r_C02_w.dt_exclusao is not null and r_C02_w.dt_atendimento > r_C02_w.dt_exclusao) then
              dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
            end if;
            end if;
            
            dados_tb_selecao_w.nr_seq_selecao(nr_idx_w):= r_C02_w.nr_seq_selecao;
            dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';

            nr_idx_w := nr_idx_w + 1;

            if  (nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
            pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
                  'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
            nr_idx_w := 0;
            pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
            end if;
          end loop;
        elsif (r_C01_w.ie_tipo_profissional in ('PP','A')) then
        
          if ( dados_regra_p.ie_evento <> 'IMP' ) then
            for r_C03_w in  C03(nr_id_transacao_p) loop
                  
              if  (nr_seq_sel_anterior_w <> r_C03_w.nr_seq_selecao or ie_valido_anterior_w = 'N') then
              
                ie_valido_anterior_w            := 'N';                         
                dados_tb_selecao_w.ie_valido(nr_idx_w)    := 'N';
                dados_tb_selecao_w.nr_seq_selecao(nr_idx_w) := r_C03_w.nr_seq_selecao;
                dados_tb_selecao_w.ds_observacao(nr_idx_w)  := ''; 
                
                if  (r_C03_w.qt_prof_valido = 0) then
                  dados_tb_selecao_w.ie_valido(nr_idx_w)    :=  'S';
                  dados_tb_selecao_w.ds_observacao(nr_idx_w)  :=  'Profissional executante n�o est� vinculado ao prestador executor';
                  ie_valido_anterior_w    :=  'S';

                else
                  if  (r_C03_w.dt_atendimento < r_C03_w.dt_inclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';

                  elsif (r_C03_w.dt_exclusao is not null and r_C03_w.dt_atendimento > r_C03_w.dt_exclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';

                  end if;
                end if;           
                      
                nr_idx_w := nr_idx_w + 1;
              
                if  (nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
                  
                  pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
                  'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
                  
                  nr_idx_w := 0;
                  pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
                end if;
              end if;
              nr_seq_sel_anterior_w   := r_C03_w.nr_seq_selecao;
                        
            end loop;
          else
          
            for r_C04_w in  C04(nr_id_transacao_p) loop
                  
              if  (nr_seq_sel_anterior_w <> r_C04_w.nr_seq_selecao or ie_valido_anterior_w = 'N') then
              
                ie_valido_anterior_w    := 'N';           
                dados_tb_selecao_w.ie_valido(nr_idx_w)    := 'N';
                dados_tb_selecao_w.nr_seq_selecao(nr_idx_w) :=  r_C04_w.nr_seq_selecao;
                dados_tb_selecao_w.ds_observacao(nr_idx_w)  :=  '';
                
                if  (r_C04_w.qt_prof_valido = 0) then
                  dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                  dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                  ie_valido_anterior_w  :='S';

                else
                  if  (r_C04_w.dt_atendimento < r_C04_w.dt_inclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';

                  elsif (r_C04_w.dt_exclusao is not null and r_C04_w.dt_atendimento > r_C04_w.dt_exclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';

                  end if;
                end if;           
                      
                nr_idx_w := nr_idx_w + 1;
              
                if  (nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
                  pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
                  'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
                  nr_idx_w := 0;
                  pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
                end if;
              end if;
              nr_seq_sel_anterior_w   := r_C04_w.nr_seq_selecao;
                        
            end loop;
            
          end if;
        
        end if;
        
        if ( r_C01_w.ie_tipo_profissional = 'A') then
        
          if (dados_regra_p.ie_evento <> 'IMP') then 
        
            for r_C05_w in  C05(nr_id_transacao_p) loop
                
              if  (nr_seq_sel_anterior_w <> r_C05_w.nr_seq_selecao or ie_valido_anterior_w = 'N') then
              
                ie_valido_anterior_w    := 'N';
                dados_tb_selecao_w.ie_valido(nr_idx_w)    := 'N';
                dados_tb_selecao_w.nr_seq_selecao(nr_idx_w) :=  r_C05_w.nr_seq_selecao;
                dados_tb_selecao_w.ds_observacao(nr_idx_w)  :=  '';
                
                if  (r_C05_w.qt_prof_valido = 0) then
                  ie_valido_anterior_w  :='S';
                  dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                  dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                
                else
                  if  (r_C05_w.dt_atendimento < r_C05_w.dt_inclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                  elsif (r_C05_w.dt_exclusao is not null and r_C05_w.dt_atendimento > r_C05_w.dt_exclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                  end if;             
                end if;
                        
                nr_idx_w := nr_idx_w + 1;
              
                if  (nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
                  pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
                  'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
                  nr_idx_w := 0;
                  pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
                end if;

              end if;
              nr_seq_sel_anterior_w   := r_C05_w.nr_seq_selecao;
              
            end loop;
          else
          
            for r_C06_w in  C06(nr_id_transacao_p) loop
                
              if  (nr_seq_sel_anterior_w <> r_C06_w.nr_seq_selecao or ie_valido_anterior_w = 'N') then
              
                ie_valido_anterior_w    := 'N';
                dados_tb_selecao_w.ie_valido(nr_idx_w)    := 'N';
                dados_tb_selecao_w.nr_seq_selecao(nr_idx_w) :=  r_C06_w.nr_seq_selecao;
                dados_tb_selecao_w.ds_observacao(nr_idx_w)  :=  '';
                
                if  (r_C06_w.qt_prof_valido = 0) then
                  ie_valido_anterior_w  :='S';
                  dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                  dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                
                else
                  if  (r_C06_w.dt_atendimento < r_C06_w.dt_inclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                  elsif (r_C06_w.dt_exclusao is not null and r_C06_w.dt_atendimento > r_C06_w.dt_exclusao) then 
                    dados_tb_selecao_w.ie_valido(nr_idx_w):=  'S';
                    ie_valido_anterior_w  :='S';
                    dados_tb_selecao_w.ds_observacao(nr_idx_w):=  'Profissional executante n�o est� vinculado ao prestador executor';
                  end if;             
                end if;
                        
                nr_idx_w := nr_idx_w + 1;
              
                if  (nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
                  pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
                  'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
                  nr_idx_w := 0;
                  pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
                end if;

              end if;
              nr_seq_sel_anterior_w   := r_C06_w.nr_seq_selecao;
              
            end loop;
          
          end if;
        
        end if;
          
      if  (nr_idx_w > 0)  then
        pls_tipos_ocor_pck.gerencia_selecao_validacao(  dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
              'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
      end if;
      exception
      when others then
        pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, sqlerrm, nr_id_transacao_p, nm_usuario_p);
      end;
        pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
      end if;
  end loop;
end if;
end pls_oc_cta_tratar_val_76;
/