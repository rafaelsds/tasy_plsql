create or replace
procedure ptu_status_analise_finalizada
			(	nr_seq_conta_p		Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	varchar2) is 

qt_contas_analise_w		number(10);
nr_seq_fatura_w			number(10);
nr_seq_acao_w			number(10)	:= null;
nr_titulo_w			number(10);
nr_titulo_ndc_w			number(10);
dt_liquidacao_w			date;
ie_status_fatura_w		ptu_fatura.ie_status%type;

begin
select	max(nr_seq_fatura)
into	nr_seq_fatura_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_p;

if	(nr_seq_fatura_w is not null) then
	select	count(*)
	into	qt_contas_analise_w
	from	pls_analise_conta	a,
		pls_conta		b
	where	b.nr_seq_analise	= a.nr_sequencia
	and	b.nr_seq_fatura		= nr_seq_fatura_w
	and	a.ie_status		<> 'T';
	
	select	max(a.nr_titulo),
		max(a.nr_titulo_ndc),
		max(a.ie_status)
	into	nr_titulo_w,
		nr_titulo_ndc_w,
		ie_status_fatura_w
	from	ptu_fatura a
	where	a.nr_sequencia = nr_seq_fatura_w;

	if	(qt_contas_analise_w = 0) and
		(ie_status_fatura_w <> 'V') then
		ptu_atualizar_status_fatura(nr_seq_fatura_w, 'AF', null, nm_usuario_p);
	end if;
	
	if	(nr_seq_fatura_w is not null) then
		pls_obter_acao_intercambio(	'4', -- Finaliza��o da an�lise das contas
						'2', -- Gerar t�tulo a pagar
						nr_seq_fatura_w,
						null,
						null,
						null,
						sysdate,
						'A500',
						'N',
						nr_seq_acao_w);
		
		if	(nr_seq_acao_w is not null) then
			ptu_gerar_tit_pagar_fatura(nr_seq_fatura_w,nr_seq_acao_w,'G',cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		pls_obter_acao_intercambio(	'4', -- Finaliza��o da an�lise das contas
						'5', -- Liberar pagamento do t�tulo 
						nr_seq_fatura_w,
						null,
						null,
						null,
						sysdate,
						'A500',
						'N',
						nr_seq_acao_w);
		
		if	(nr_seq_acao_w is not null) then		
			if	(nr_titulo_w is not null) then
				pls_liberar_fatura_pagamento(nr_titulo_w,nm_usuario_p);
			end if;
			
			if	(nr_titulo_ndc_w is not null) then
				pls_liberar_fatura_pagamento(nr_titulo_ndc_w,nm_usuario_p);
			end if;
		end if;
		
		pls_obter_acao_intercambio(	'4', -- Finaliza��o da an�lise das contas
						'8', -- Mudar status do t�tulo a pagar
						nr_seq_fatura_w,
						null,
						null,
						null,
						sysdate,
						'A500',
						'N',
						nr_seq_acao_w);
		
		if	(nr_seq_acao_w is not null) then		
			if	(nr_titulo_w is not null) then
				pls_alterar_status_fatura_pag(nr_titulo_w,nm_usuario_p);
			end if;
			
			if	(nr_titulo_ndc_w is not null) then
				pls_alterar_status_fatura_pag(nr_titulo_ndc_w,nm_usuario_p);
			end if;
		end if;
		
		pls_obter_acao_intercambio(	'4', -- Finaliza��o da an�lise das contas
						'7', -- Baixar valor glosado no t�tulo
						nr_seq_fatura_w,
						null,
						null,
						null,
						sysdate,
						'A500',
						'N',
						nr_seq_acao_w);
		
		if	(nr_seq_acao_w is not null) then
			pls_baixar_glosas_conta_pag(nr_seq_conta_p,nr_seq_acao_w,nr_seq_fatura_w,nm_usuario_p,cd_estabelecimento_p);
		end if;
	end if;
	
	--aaschlote 21/10/2015 - OS 880346 Caso tenha todas as an�lises com atendimento encerrado e o t�tulo a pagar j� esteja liquidado, atualizar o status do protocolo
	if	(qt_contas_analise_w	= 0) and
		(nr_titulo_w is not null) then
			
		select	max(dt_liquidacao)
		into	dt_liquidacao_w
		from	titulo_pagar
		where	nr_titulo	= nr_titulo_w;
		
		if	(dt_liquidacao_w is not null) then
			pls_protocolo_atualizar_status(nr_titulo_w,nm_usuario_p);
		end if;
	end if;
end if;

end ptu_status_analise_finalizada;
/
