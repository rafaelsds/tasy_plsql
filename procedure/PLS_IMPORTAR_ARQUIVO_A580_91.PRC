create or replace
procedure pls_importar_arquivo_a580_91( ds_conteudo_p			varchar2,
					nr_seq_fat_geral_p		ptu_fatura_geral.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					nr_seq_regra_tit_fat_p		pls_regra_tit_fat_geral.nr_sequencia%type) is
					
cd_uni_des_w		ptu_fatura_geral.cd_uni_des%type;
cd_uni_ori_w		ptu_fatura_geral.cd_uni_ori%type;
dt_geracao_w		ptu_fatura_geral.dt_geracao%type;
nr_comp_w		ptu_fatura_geral.nr_comp%type;
dt_ven_doc_w		ptu_fatura_geral.dt_ven_doc%type;
dt_emi_doc_w		ptu_fatura_geral.dt_emi_doc%type;
vl_tot_doc_w		ptu_fatura_geral.vl_tot_doc%type;
nr_ver_tra_w		ptu_fatura_geral.nr_ver_tra%type;
vl_ir_w			ptu_fatura_geral.vl_ir%type;
nr_documento_w		ptu_fatura_geral.nr_documento%type;
doc_fiscal_w		ptu_fatura_geral.doc_fiscal%type;
tp_documento_w		ptu_fatura_geral.tp_documento%type;
nr_seq_fat_geral_w	ptu_fatura_geral.nr_sequencia%type;
dt_postagem_w		ptu_fatura_geral.dt_postagem%type;
nr_protocolo_w		ptu_fatura_geral.nr_protocolo%type;
qt_tit_imp_w		pls_integer;
cd_unimed_estab_w	pls_congenere.cd_cooperativa%type;
ds_hash_w		ptu_fatura_geral.ds_hash%type;
id_cobranca_w		ptu_fatura_geral.id_cobranca%type;
					
begin
if	(substr(ds_conteudo_p,9,3) = '581') then
	cd_uni_des_w	:= lpad(trim(substr(ds_conteudo_p,12,4)),4,'0');
	cd_uni_ori_w	:= lpad(trim(substr(ds_conteudo_p,16,4)),4,'0');	
	
	begin
	dt_geracao_w	:= to_date(substr(ds_conteudo_p,26,2)||substr(ds_conteudo_p,24,2)||substr(ds_conteudo_p,20,4),'dd/mm/yyyy');
	exception
	when others then
		dt_geracao_w := null;
	end;

	nr_comp_w	:= trim(substr(ds_conteudo_p,28,4));
	
	begin
	dt_ven_doc_w	:= to_date(substr(ds_conteudo_p,49,2)||substr(ds_conteudo_p,47,2)||substr(ds_conteudo_p,43,4),'dd/mm/yyyy');
	exception
	when others then
		dt_ven_doc_w := null;
	end;
	
	begin
	dt_emi_doc_w	:= to_date(substr(ds_conteudo_p,57,2)||substr(ds_conteudo_p,55,2)||substr(ds_conteudo_p,51,4),'dd/mm/yyyy');
	exception
	when others then
		dt_emi_doc_w := null;
	end;
	
	begin
	vl_tot_doc_w	:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,59,14));
	exception
	when others then
		vl_tot_doc_w := 0;
	end;
	
	nr_ver_tra_w	:= trim(substr(ds_conteudo_p,73,2));
	
	begin
	vl_ir_w		:= pls_format_valor_imp_ptu(substr(ds_conteudo_p,75,14));
	exception
	when others then
		vl_ir_w := 0;
	end;

	nr_documento_w	:= trim(substr(ds_conteudo_p,89,20));
	doc_fiscal_w	:= trim(substr(ds_conteudo_p,109,20));
	tp_documento_w	:= trim(substr(ds_conteudo_p,129,1));
	id_cobranca_w	:= trim(substr(ds_conteudo_p,130,1));
	
	cd_unimed_estab_w := pls_obter_unimed_estab(cd_estabelecimento_p);
	
	if	(lpad(cd_unimed_estab_w,'4','0') <> lpad(cd_uni_des_w,4,'0')) then
		wheb_mensagem_pck.exibir_mensagem_abort(182469);
	end if;
	
	select	count(1)
	into	qt_tit_imp_w
	from	ptu_fatura_geral
	where	nr_documento	= nr_documento_w
	and	cd_uni_ori	= cd_uni_ori_w
	and	dt_cancelamento is null;
	
	if	(qt_tit_imp_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(363435,'NR_DOCUMENTO=' || nr_documento_w);
	end if;

	insert into ptu_fatura_geral
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_titulo_receber,
		nr_titulo_pagar,
		cd_uni_des,
		cd_uni_ori,
		dt_geracao,
		nr_comp,
		dt_ven_doc,
		dt_emi_doc,
		vl_tot_doc,
		nr_ver_tra,
		vl_ir,
		nr_documento,
		doc_fiscal,
		tp_documento,
		dt_postagem,
		nr_protocolo,
		nr_seq_regra_tit_fat,
		id_cobranca)
	values	(ptu_fatura_geral_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		null,
		cd_uni_des_w,
		cd_uni_ori_w,
		dt_geracao_w,
		nr_comp_w,
		dt_ven_doc_w,
		dt_emi_doc_w,
		vl_tot_doc_w,
		nr_ver_tra_w,
		vl_ir_w,
		nr_documento_w,
		doc_fiscal_w,
		tp_documento_w,
		null,
		null,
		nr_seq_regra_tit_fat_p,
		id_cobranca_w);
		
elsif	(substr(ds_conteudo_p,9,3) = '998') then
	select	max(nr_sequencia)
	into	nr_seq_fat_geral_w
	from	ptu_fatura_geral
	where	nm_usuario		= nm_usuario_p
	and	nr_titulo_receber	is null
	and	nr_titulo_pagar		is null
	and	dt_postagem		is null;
	
	ds_hash_w := trim(substr(ds_conteudo_p,12,32));
	
	if	(nr_seq_fat_geral_w is not null) then
		update	ptu_fatura_geral
		set	ds_hash		= ds_hash_w
		where	nr_sequencia	= nr_seq_fat_geral_w;
	end if;		
		
elsif	(substr(ds_conteudo_p,9,3) = '999') then
	select	max(nr_sequencia)
	into	nr_seq_fat_geral_w
	from	ptu_fatura_geral
	where	nm_usuario		= nm_usuario_p
	and	nr_titulo_receber	is null
	and	nr_titulo_pagar		is null
	and	dt_postagem		is null;
	
	begin
	dt_postagem_w	:= to_date(substr(ds_conteudo_p,20,2)||substr(ds_conteudo_p,17,2)||substr(ds_conteudo_p,12,4)||substr(ds_conteudo_p,22,8),'dd/mm/yyyy hh24:mi:ss');	
	exception
	when others then
		dt_postagem_w := null;
	end;
	
	nr_protocolo_w := trim(substr(ds_conteudo_p,33,10));
	
	if	(nr_seq_fat_geral_w is not null) then
		update	ptu_fatura_geral
		set	dt_postagem	= dt_postagem_w,
			nr_protocolo	= nr_protocolo_w
		where	nr_sequencia	= nr_seq_fat_geral_w;
	end if;
end if;

end pls_importar_arquivo_a580_91;
/
