create or replace
procedure excluir_medic_kit(nr_prescricao_p		in prescr_medica.nr_prescricao%type,
							nr_sequencia_p		in prescr_material.nr_sequencia%type) is

ie_atualizado_w		boolean;

begin
	ie_atualizado_w := false;

	delete	prescr_material
	where	nr_sequencia <> nr_sequencia_p
	and		nr_prescricao = nr_prescricao_p
	and		nr_seq_kit = nr_sequencia_p
	--and	ie_agrupador = 1
	and		nr_sequencia_solucao is null
	and		nr_sequencia_proc is null
	and		nr_sequencia_diluicao is null
	and		nr_sequencia_dieta is null;

	ie_atualizado_w := sql%rowcount > 0;

	if (ie_atualizado_w) then
		update	prescr_medica
		set		ie_gerou_kit = null,
				dt_atualizacao = sysdate,
				ds_stack = substr(ds_stack || ' atualizado ie_gerou_kit',1,1800)
		where	nr_prescricao = nr_prescricao_p;
	end if;

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then
		commit;
	end if;

end excluir_medic_kit;
/