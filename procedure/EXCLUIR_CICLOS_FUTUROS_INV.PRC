create or replace
procedure excluir_ciclos_futuros_inv(
				nr_seq_regra_p		number) is

begin

delete
from	sup_inv_ciclico_ciclo
where	nr_seq_regra = nr_seq_regra_p
and	trunc(dt_inicio) > trunc(sysdate);

commit;

end excluir_ciclos_futuros_inv;
/