create or replace
procedure gerar_prescr_mat_compl(nr_prescricao_p		number,
								 nr_sequencia_p			number,
								 nm_usuario_p			varchar2,
								 ie_item_disp_gerado_p 	varchar,
								 ds_horarios_enf_p		varchar2 default null,
								 nr_seq_agente_anest_p	cirurgia_agente_anestesico.nr_sequencia%type default null) is 
nr_cont_w  numeric (2,0);
begin

if (nvl(nr_prescricao_p,0) > 0) and (nvl(nr_sequencia_p,0) > 0) then
	
  select  count(*)
  into    nr_cont_w
  from    prescr_material_compl
  where   nr_sequencia = nr_sequencia_p
  and     nr_prescricao = nr_prescricao_p;

  if (nr_cont_w = 0) then
    begin
    insert into prescr_material_compl(
      nr_sequencia,
      nr_prescricao,
      nm_usuario_nrec,
      dt_atualizacao_nrec,
      nm_usuario,
      dt_atualizacao,
      ie_item_disp_gerado,
      ds_horario_enf,
	  nr_seq_agente_anestesico)
    values(
      nr_sequencia_p,
      nr_prescricao_p,
      nvl(nm_usuario_p,wheb_usuario_pck.get_nm_usuario),
      sysdate,
      nvl(nm_usuario_p,wheb_usuario_pck.get_nm_usuario),
      sysdate,
      ie_item_disp_gerado_p,
      ds_horarios_enf_p,
	  nr_seq_agente_anest_p);
      
    exception when others then
		gravar_log_tasy(cd_log_p => 10007, 
						ds_log_p => substr('Exception gerar_prescr_mat_compl - Linha:'||$$plsql_line||pls_util_pck.enter_w
										|| ' nr_prescricao_p:'|| nr_prescricao_p||pls_util_pck.enter_w
										|| ' nm_usuario_p:'||nm_usuario_p||pls_util_pck.enter_w
										|| ' nr_sequencia_p:'|| nr_sequencia_p||pls_util_pck.enter_w
										|| ' ie_item_disp_gerado_p:'||ie_item_disp_gerado_p||pls_util_pck.enter_w
										|| ' ds_horarios_enf_p:'||ds_horarios_enf_p||pls_util_pck.enter_w
										|| ' Erro:'||substr(sqlerrm,1,100),1,2000),
						nm_usuario_p => nm_usuario_p) ;
    end;	
      
  end if;  	

end if;

if 	(nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then 
	commit; 
end if;

end gerar_prescr_mat_compl;
/
