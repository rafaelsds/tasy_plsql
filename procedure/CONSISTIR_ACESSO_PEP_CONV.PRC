create or replace
procedure consistir_acesso_pep_conv(	nr_atendimento_p	in number,
					cd_estabelecimento_p	in number,
					nm_usuario_p		in varchar2,
					ie_bloqueia_p		out varchar2,
					ds_consistencia_p	out varchar2) is 

ie_forma_consistir_w		varchar2(15);
cd_setor_pac_w			number(10,0);
cd_setor_usuario_w		number(10,0);
ie_setor_pac_lib_w		varchar2(1);
cd_convenio_w			number(10);
ie_registro_pep_w		varchar2(10);
					
begin

if	(nr_atendimento_p	> 0) then
	begin
	cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);
	exception
	when others then
		cd_convenio_w	:= 0;
	end;
	
	if	(cd_convenio_w	> 0) then
		select	nvl(max(IE_REGISTRO_PEP),'S')
		into	ie_registro_pep_w
		from	convenio_estabelecimento
		where	cd_convenio	= cd_convenio_w
		and	cd_estabelecimento = cd_estabelecimento_p;
		
		if	(ie_registro_pep_w	= 'N') then
			ds_consistencia_p	:= wheb_mensagem_pck.get_texto(306617, 'NM_CONVENIO=' || obter_nome_convenio(cd_convenio_w)); -- O conv�nio #@NM_CONVENIO#@ n�o permite a utiliza��o do PEP
			ie_bloqueia_p		:= 'S';
		end if;
	end if;
	
end if;


end consistir_acesso_pep_conv;
/
