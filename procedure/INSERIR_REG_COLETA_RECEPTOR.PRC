create or replace
procedure inserir_reg_coleta_receptor(	nr_sequencia_p		number,
                                        dt_coleta_receptor_p    Date,
					nm_usuario_coleta_p	Varchar2) is 

begin

update	san_reserva
set	dt_coleta_receptor = dt_coleta_receptor_p,
	nm_usuario_coleta = nm_usuario_coleta_p
where	nr_sequencia = nr_sequencia_p;

commit;

end inserir_reg_coleta_receptor;
/