create or replace
procedure bsc_gerar_comunic_ind(	dt_referencia_p	date,
				nm_usuario_p	varchar2) is 
				

cd_ano_w			number(4);
cd_dia_w				number(2);
cd_periodo_w			number(2);
ds_comunicado_w			long;
ds_comunicado_ww		long;
ds_origem_indicador_w		varchar2(80);	
ds_titulo_w			varchar2(255);
ds_titulo_ww			varchar2(4000);
nm_indicador_w			varchar2(255);
nm_pessoa_fisica_w		varchar2(60);
nm_usuario_resp_w			varchar2(15);
nm_usuario_destino_w		varchar2(2000);
nr_dia_liberacao_w			number(2);
nr_seq_indicador_w			number(10);
nr_seq_origem_w			number(10);
nr_seq_regra_w			number(10);
qt_dia_aviso_w			number(10);
qt_meta_w			number(15,2);
qt_limite_w			number(15,2);
qt_real_w				number(15,2);
ie_comunicacao_w			varchar2(1);
ie_email_w			varchar2(1);
ds_email_origem_w			varchar2(255);
ds_email_w			varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
ds_out_w				varchar2(50);
ds_comunicado_email_w		varchar2(4000);

cursor c01 is
select	a.nr_sequencia,
	nvl(a.ds_titulo,''),
	nvl(a.qt_dia_aviso,0),
	a.ds_comunicado,
	nvl(a.ie_comunicacao,''),
	nvl(a.ie_email,''),
	nvl(a.ds_email_origem,'')
from	bsc_regra_comunic_ind a
where	a.ie_situacao	= 'A';

cursor c02 is
select	a.nr_sequencia,
	a.nm_indicador,
	a.nr_dia_liberacao,
	a.nr_seq_origem
from	bsc_indicador a
where	a.ie_situacao	= 'A'
and	exists(	select	1
		from	bsc_ind_inf x
		where	x.nr_seq_indicador 	= a.nr_sequencia
		and	x.cd_ano		= cd_ano_w
		and	x.cd_periodo	= cd_periodo_w
		and	x.dt_liberacao is null)
and	exists(	select	1
		from	bsc_ind_resp y
		where	y.nr_seq_indicador	= a.nr_sequencia)
and	a.nr_dia_liberacao is not null;

cursor c03 is
select	a.nm_usuario
from	bsc_ind_resp b,
	usuario a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.nr_seq_indicador	= nr_seq_indicador_w
and	bsc_obter_se_resp_indic(nr_seq_indicador_w,dt_referencia_p,a.nm_usuario) = 'S'
and	a.ie_situacao = 'A'
and	nvl(b.ie_receb_comunic_inf,'S') = 'S'
order by 1;

begin
cd_dia_w	:= to_number(to_char(dt_referencia_p,'DD'));
cd_periodo_w	:= to_number(to_char(dt_referencia_p,'MM'));
cd_ano_w	:= to_number(to_char(dt_referencia_p,'YYYY'));

open c01;
loop
fetch c01 into	
	nr_seq_regra_w,
	ds_titulo_w,
	qt_dia_aviso_w,
	ds_comunicado_w,
	ie_comunicacao_w,
	ie_email_w,
	ds_email_origem_w;
exit when c01%notfound;
	begin
	open c02;	
	loop
	fetch c02 into	
		nr_seq_indicador_w,
		nm_indicador_w,
		nr_dia_liberacao_w,
		nr_seq_origem_w;
	exit when c02%notfound;
		begin		
		ds_comunicado_ww	:= ds_comunicado_w;
		select	max(ds_origem_indicador)
		into	ds_origem_indicador_w
		from	bsc_origem_indic
		where	nr_sequencia	= nr_seq_origem_w;

		select	nvl(max(qt_meta),0),
			nvl(max(qt_limite),0),
			nvl(max(qt_real),0)	
		into	qt_meta_w,
			qt_limite_w,
			qt_real_w
		from	bsc_ind_inf
		where	nr_seq_indicador	= nr_seq_indicador_w
		and	cd_ano		= cd_ano_w
		and	cd_periodo	= cd_periodo_w;	

		ds_titulo_ww		:= '';

		if	(cd_dia_w = nr_dia_liberacao_w) then
			ds_titulo_ww	:= substr(wheb_mensagem_pck.get_texto(799484) || nm_indicador_w || chr(13) || chr(10),1,4000);
		end if;
			
		if	(qt_dia_aviso_w > 0) and
			(dt_referencia_p >= (to_date(nr_dia_liberacao_w||'/'||to_char(dt_referencia_p,'mm/yyyy'),'dd/mm/yyyy') - qt_dia_aviso_w)) and
			(dt_referencia_p <= to_date(nr_dia_liberacao_w||'/'||to_char(dt_referencia_p,'mm/yyyy'),'dd/mm/yyyy')) then
			ds_titulo_ww	:= substr(' ' || wheb_mensagem_pck.get_texto(799485) || ' ' || qt_dia_aviso_w || wheb_mensagem_pck.get_texto(799486) || nm_indicador_w || chr(13) || chr(10),1,4000);		
		end if;
		
		
		/*if	(qt_dia_aviso_w > 0) and
			(cd_dia_w = (nr_dia_liberacao_w - qt_dia_aviso_w)) then
			ds_titulo_ww	:= substr(' Faltam: ('  || qt_dia_aviso_w || ') dia(s) para o termino do prazo para a liberacao das informacoes do indicador: ' || nm_indicador_w || chr(13) || chr(10),1,4000);
		end if;*/

		if	(nvl(ds_titulo_ww,'X') <> 'X') then
			begin
			nm_usuario_destino_w		:= '';
			open c03;
			loop
			fetch c03 into
				nm_usuario_resp_w;
			exit when c03%notfound;
				begin
				nm_usuario_destino_w	:= nm_usuario_destino_w || nm_usuario_resp_w || ',';
				end;
			end loop;
			close c03;

			if	(nvl(ds_comunicado_ww,'X') <> 'X') then
				begin
				nm_pessoa_fisica_w	:= substr(obter_pessoa_fisica_usuario(nm_usuario_resp_w, 'N'),1,60);
				ds_comunicado_ww	:= substr(replace_macro(ds_comunicado_ww,'@NM_RESP_INDICADOR',nm_pessoa_fisica_w),1,4000);
				ds_comunicado_ww	:= substr(replace_macro(ds_comunicado_ww,'@INDICADOR',nm_indicador_w),1,4000);
				ds_comunicado_ww	:= substr(replace_macro(ds_comunicado_ww,'@META',campo_mascara_virgula(qt_meta_w)),1,4000);
				ds_comunicado_ww	:= substr(replace_macro(ds_comunicado_ww,'@LIMITE',campo_mascara_virgula(qt_limite_w)),1,4000);
				ds_comunicado_ww	:= substr(replace_macro(ds_comunicado_ww,'@REAL',campo_mascara_virgula(qt_real_w)),1,4000);
				end;
			end if;

			if	(nvl(nm_usuario_destino_w,'X') <> 'X') and
				(nvl(ie_comunicacao_w,'S') = 'S') then
				begin
				gerar_comunic_padrao(	sysdate,
							ds_titulo_w,
							ds_comunicado_ww,
							nm_usuario_p,
							'N',
							nm_usuario_destino_w,
							'N',
							null,
							'',
							null,
							'',
							sysdate,
							'',
							'');
				end;
			end if;

			
			open C03;
			loop
			fetch C03 into	
				nm_usuario_resp_w;
			exit when C03%notfound;
				begin
				if	(nvl(nm_usuario_resp_w,'X') <> 'X') and
					(nvl(ie_email_w,'N') = 'S') then
					begin
					select	a.cd_pessoa_fisica
					into	cd_pessoa_fisica_w
					from	usuario a
					where	a.nm_usuario = nm_usuario_resp_w;

					ds_email_w := obter_dados_compl_pf_pj(null,cd_pessoa_fisica_w,'EMA');

					if	(nvl(ds_email_w,'X') <> 'X') then
						begin
						converte_rtf_string('select ds_comunicado from bsc_regra_comunic_ind where nr_sequencia = :nr',nr_seq_regra_w,nm_usuario_p,ds_out_w);

						select	ds_texto
						into	ds_comunicado_email_w
						from	tasy_conversao_rtf
						where	nr_sequencia = ds_out_w;
						
						if	(nvl(ds_comunicado_email_w,'X') <> 'X') then
							begin
							nm_pessoa_fisica_w	:= substr(obter_pessoa_fisica_usuario(nm_usuario_resp_w, 'N'),1,60);
							ds_comunicado_email_w	:= substr(replace_macro(ds_comunicado_email_w,'@NM_RESP_INDICADOR',nm_pessoa_fisica_w),1,4000);
							ds_comunicado_email_w	:= substr(replace_macro(ds_comunicado_email_w,'@INDICADOR',nm_indicador_w),1,4000);
							ds_comunicado_email_w	:= substr(replace_macro(ds_comunicado_email_w,'@META',campo_mascara_virgula(qt_meta_w)),1,4000);
							ds_comunicado_email_w	:= substr(replace_macro(ds_comunicado_email_w,'@LIMITE',campo_mascara_virgula(qt_limite_w)),1,4000);
							ds_comunicado_email_w	:= substr(replace_macro(ds_comunicado_email_w,'@REAL',campo_mascara_virgula(qt_real_w)),1,4000);
                            enviar_email(	ds_titulo_w,
									substr(ds_comunicado_email_w,1,4000),
									ds_email_origem_w,
									ds_email_w,
									nm_usuario_p,
									'A');
							end;
						end if;
						end;
					end if;
					end;
				end if;	
				end;
			end loop;
			close C03;
			end;
		end if;
		end;
	end loop;
	close c02;
	end;
end loop;
close c01;
commit;
end bsc_gerar_comunic_ind;
/
