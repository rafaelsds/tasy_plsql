create or replace
procedure GER_INSERIR_ADIANTAMENTO_PAGO(nr_adiantamento_p	number,
					nr_seq_cheque_p		number,
					nm_usuario_p		varchar2) is 

begin

insert into	adiantamento_pago_cheque
		(nr_sequencia,
		nr_adiantamento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_cheque)
values		(adiantamento_pago_cheque_seq.nextval,
		nr_adiantamento_p,
		sysdate,
		nm_usuario_p,
		nr_seq_cheque_p);

commit;

end GER_INSERIR_ADIANTAMENTO_PAGO;
/