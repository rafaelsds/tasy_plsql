create or replace
procedure GER_DESVINC_ADIANT_CHEQUE(	nr_seq_cheque_p		number,
					nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_cheque_p is not null) and
	(nm_usuario_p is not null) then
	begin
	delete from	adiantamento_pago_cheque
	where		nr_seq_cheque = nr_seq_cheque_p;
	end;
end if;	

commit;
end GER_DESVINC_ADIANT_CHEQUE;
/