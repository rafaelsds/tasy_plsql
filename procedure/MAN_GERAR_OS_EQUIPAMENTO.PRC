create or replace
procedure man_gerar_os_equipamento(
				nr_sequencia_p			number,
				nm_usuario_p			varchar2,
				ie_tipo_ordem_p			number,
				nr_seq_planej_p			varchar2,
				ie_gerar_campos_prev_p		varchar2,
				nr_nova_os_p		out	number,
				ds_dano_breve_p			varchar2 default '',
				ds_dano_p			varchar2 default '') is

nr_sequencia_w			number(10,0);
cd_pessoa_solic_w			varchar2(10);
ds_dano_w			varchar2(4000);
ds_dano_equip_w			varchar2(4000);
nr_seq_local_w			number(10,0);
ds_equipamento_w			varchar2(255);
ds_localizacao_w			varchar2(255);
cd_imobilizado_w			varchar2(20);
cd_imobilizado_ext_w		varchar2(20);
nr_grupo_planej_w			number(10,0);
nr_grupo_trab_w			number(10,0);
nr_grupo_planejamento_w		number(10,0);
nr_grupo_trabalho_w		number(10,0);
ie_tipo_ordem_w			number(10,0);
ie_prioridade_w			varchar2(1);
ie_parado_w			varchar2(1);
nr_seq_planej_w			number(10,0);
nr_seq_tipo_equip_w		number(10,0);
nr_seq_tipo_os_w			number(10,0);

dt_inicial_w			date;
ie_dia_nao_util_planej_w		varchar2(15);
ie_dia_util_planej_w			varchar2(15);
ie_dia_ultima_verif_w		varchar2(1) := 'E';
nr_seq_marca_w			number(10,0);
qt_dia_gerar_ordem_w		number(5,0);
qt_dia_inicio_desejado_w		number(5,0) := 0;
qt_dias_ordem_w			number(5,0) := 0;
cd_setor_atendimento_w		number(5,0);
qt_dia_w				number(4) := 0;
ds_planejamento_w			varchar2(80);
ds_contato_solic_w			varchar2(255);
nr_seq_estagio_w			number(10,0);
dt_conclusao_desejada_w		date;
dt_inicio_desejado_w		date;
qt_dia_previsto_w			number(5,0);

nm_usuario_exec_w		varchar2(15);
qt_existe_w			number(10,0);
ie_situacao_os_w			varchar2(5);
ie_status_w			varchar2(5);
nr_seq_origem_dano_w		number(10);
nr_seq_causa_dano_w		number(10);
nr_seq_tipo_solucao_w		number(10);
nr_seq_complex_w			number(10);
nr_seq_cs_w			number(10);
ie_classif_os_w			varchar2(1);

cursor c01 is
select	nm_usuario_exec
from	man_equip_usuario_exec
where	nr_seq_equipamento 				= nr_sequencia_p
and	nvl(nr_seq_grupo_trabalho,nvl(nr_grupo_trab_w,0)) 	= nvl(nr_grupo_trab_w,0)
and	nvl(nr_seq_grupo_planej,nvl(nr_grupo_planej_w,0)) 	= nvl(nr_grupo_planej_w,0);

begin

if	(nr_seq_planej_p <> '0') then
	nr_seq_planej_w := nr_seq_planej_p;
end if;

select	man_ordem_servico_seq.nextval
into	nr_sequencia_w
from	dual;

cd_pessoa_solic_w := substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10);

select	ds_equipamento,
	substr(obter_desc_man_localizacao(nr_seq_local),1,255) ds_localizacao,
	cd_imobilizado,
	cd_imobilizado_ext,
	nr_seq_local,
	nr_seq_planej,
	nr_seq_trab,
	nvl(ie_prioridade,'M'),
	nvl(ie_parado,'N'),
	ie_tipo_ordem,
	nr_seq_tipo_ordem,
	ie_classificacao_os
into	ds_equipamento_w,
	ds_localizacao_w,
	cd_imobilizado_w,
	cd_imobilizado_ext_w,
	nr_seq_local_w,
	nr_grupo_planej_w,
	nr_grupo_trab_w,
	ie_prioridade_w,
	ie_parado_w,
	ie_tipo_ordem_w,
	nr_seq_tipo_os_w,
	ie_classif_os_w
from	man_equipamento
where	nr_sequencia = nr_sequencia_p;

nr_grupo_planejamento_w	:= nr_grupo_planej_w;
nr_grupo_trabalho_w	:= nr_grupo_trab_w;

if	(nvl(nr_seq_planej_w,0) <> 0) and
	(nvl(ie_gerar_campos_prev_p,'N') = 'S') then
	begin
	select	b.qt_dia,
		a.nr_seq_tipo_equip,
		a.ie_dia_util,
		a.ie_dia_nao_util,
		a.ie_dia_ultima_verif,
		a.dt_inicial,
		a.nr_seq_marca,
		a.qt_dia_gerar_ordem,
		a.cd_setor_atendimento,
		a.ds_planejamento,
		a.ie_prioridade,
		a.ds_dano,
		a.cd_pessoa_solicitante,
		a.qt_dia_previsto,
		a.qt_dia_gerar_ordem,
		a.nr_seq_estagio,
		a.nr_seq_tipo_os,
		a.nr_seq_origem_dano,
		a.nr_seq_causa_dano,
		a.nr_seq_tipo_solucao,
		a.nr_seq_complex,
		a.nr_seq_cs,
		nvl(a.nr_seq_grupo_planej,nr_grupo_planejamento_w),
		nvl(a.nr_seq_grupo_trabalho,nr_grupo_trabalho_w)
	into	qt_dia_w,
		nr_seq_tipo_equip_w,
		ie_dia_util_planej_w,
		ie_dia_nao_util_planej_w,
		ie_dia_ultima_verif_w,
		dt_inicial_w,
		nr_seq_marca_w,
		qt_dia_gerar_ordem_w,
		cd_setor_atendimento_w,
		ds_planejamento_w,
		ie_prioridade_w,
		ds_dano_w,
		cd_pessoa_solic_w,
		qt_dia_previsto_w,
		qt_dia_gerar_ordem_w,
		nr_seq_estagio_w,
		nr_seq_tipo_os_w,
		nr_seq_origem_dano_w,
		nr_seq_causa_dano_w,
		nr_seq_tipo_solucao_w,
		nr_seq_complex_w,
		nr_seq_cs_w,
		nr_grupo_planejamento_w,
		nr_grupo_trabalho_w
	from	man_planej_prev a,
		man_freq_planej b
	where	a.nr_seq_frequencia = b.nr_sequencia
	and	nvl(a.ie_situacao,'A') = 'A'
	and	a.nr_sequencia	=  nr_seq_planej_w;

	if	(cd_pessoa_solic_w is not null) then
		begin
		select  max(substr(wheb_mensagem_pck.get_texto(305700) || ' ' || nvl(nr_ramal,substr(obter_compl_pf(cd_pessoa_fisica,2,'RAM'),1,20)) || ' - ' || wheb_mensagem_pck.get_texto(305701) || ' ' || substr(obter_nome_setor(obter_setor_usuario(nm_usuario)),1,80),1,255))
		into	ds_contato_solic_w
		from 	usuario
		where 	cd_pessoa_fisica = cd_pessoa_solic_w;
		end;
	end if;

	dt_inicio_desejado_w := sysdate + qt_dia_gerar_ordem_w;
	dt_conclusao_desejada_w := sysdate + qt_dia_previsto_w + qt_dia_gerar_ordem_w;

	if	(nr_seq_estagio_w is not null) then
		begin
		select	ie_situacao_os
		into	ie_situacao_os_w
		from	man_estagio_processo
		where	nr_sequencia = nr_seq_estagio_w;
		exception
		when others then
			ie_situacao_os_w := null;
		end;

		if	(ie_situacao_os_w is not null)and
			(ie_situacao_os_w <> '3')then
			ie_status_w := ie_situacao_os_w;
		end if;
	end if;

	end;
else
	begin	
	ds_dano_w := ds_dano_p;
	
	select	substr(max(ds_dano),1,4000)
	into	ds_dano_equip_w
	from	man_equipamento_dano
	where	nr_seq_equipamento = nr_sequencia_p;

	ds_dano_w := substr(ds_dano_w || chr(13) || chr(10) || ds_dano_equip_w,1,4000);
	end;
end if;

ie_tipo_ordem_w := nvl(ie_tipo_ordem_p,nvl(ie_tipo_ordem_w,1));

insert	into man_ordem_servico(
		nr_sequencia,
		cd_pessoa_solicitante,
		dt_ordem_servico,
		ie_prioridade,
		ie_parado,
		ds_dano_breve,
		dt_inicio_desejado,
		dt_conclusao_desejada,
		dt_inicio_previsto,
		dt_fim_previsto,
		dt_atualizacao,
		nm_usuario,
		ds_dano,
		ie_tipo_ordem,
		nr_seq_equipamento,
		nr_seq_localizacao,
		ie_status_ordem,
		nr_grupo_planej,
		nr_grupo_trabalho,
		ie_origem_os,
		nr_seq_planej,
		ds_contato_solicitante,
		nr_seq_estagio,
		nr_seq_tipo_ordem,
		nr_seq_origem_dano,
		nr_seq_causa_dano,
		nr_seq_tipo_solucao,
		nr_seq_complex,
		nr_seq_cs,
		ie_classificacao)
	values(	nr_sequencia_w,
		cd_pessoa_solic_w,
		sysdate,
		ie_prioridade_w,
		ie_parado_w,
		substr(nvl(nvl(ds_planejamento_w,ds_dano_breve_p),wheb_mensagem_pck.get_texto(305939)),1,80),
		nvl(dt_inicio_desejado_w,sysdate),
		nvl(dt_conclusao_desejada_w,null),
		decode(ie_status_w,2,dt_inicio_desejado_w,null),
		decode(ie_status_w,2,dt_conclusao_desejada_w,null),
		sysdate,
		nm_usuario_p,
		substr(nvl(ds_dano_w,wheb_mensagem_pck.get_texto(305939)),1,4000),
		ie_tipo_ordem_w,
		nr_sequencia_p,
		nr_seq_local_w,
		nvl(ie_status_w,'1'),		
		nr_grupo_planejamento_w,
		nr_grupo_trabalho_w,		
		'4',
		nr_seq_planej_w,
		ds_contato_solic_w,
		nr_seq_estagio_w,
		nr_seq_tipo_os_w,
		nr_seq_origem_dano_w,
		nr_seq_causa_dano_w,
		nr_seq_tipo_solucao_w,
		nr_seq_complex_w,
		nr_seq_cs_w,
		ie_classif_os_w);

open C01;
loop
fetch C01 into
	nm_usuario_exec_w;
exit when C01%notfound;
	begin
	select 	count(*)
	into	qt_existe_w
	from 	man_ordem_servico_exec
	where	nr_seq_ordem	= nr_sequencia_w
	and	nm_usuario_exec	= nm_usuario_exec_w;

	if	(qt_existe_w = 0) then
		insert into  man_ordem_servico_exec(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_exec)
		values(	man_ordem_servico_exec_seq.nextval,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nm_usuario_exec_w );
	end if;
	end;
end loop;
close C01;

commit;

nr_nova_os_p := nr_sequencia_w;

end man_gerar_os_equipamento;
/