create or replace
procedure ptu_imp_req_contagem_benef_v50
			(	ds_arquivo_p			Varchar2,
				cd_estabelecimento_p		Number,
				nm_usuario_p			Varchar2,
				nr_seq_pedido_cont_p	out	Number) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importa��o  contagem benef

Rotina utilizada nas transa��es ptu via scs homologadas com a unimed brasil.
quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
nr_seq_import_w			Number(15);
ds_conteudo_w			Varchar2(4000);
cd_transacao_w			Varchar2(255);
ie_tipo_cliente_w		Varchar2(255);
cd_unimed_requisitante_w	Number(15);
cd_unimed_destino_w		Number(15);
dt_ano_referencia_w		Varchar2(255);
dt_mes_referencia_w		Varchar2(255);
nr_seq_pedido_cont_w		Number(15);
nr_versao_w			varchar2(3);

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;

begin

open C01;
loop
fetch C01 into	
	nr_seq_import_w,
	ds_conteudo_w;
exit when C01%notfound;
	begin
	if	(substr(ds_conteudo_w,1,4)	<> 'FIM$') then
		if	(nr_seq_import_w	= 1) then
			cd_transacao_w			:= substr(ds_conteudo_w,1,5);
			ie_tipo_cliente_w		:= trim(substr(ds_conteudo_w,6,15));
			cd_unimed_requisitante_w	:= trim(substr(ds_conteudo_w,21,4));
			cd_unimed_destino_w		:= trim(substr(ds_conteudo_w,25,4));
			dt_ano_referencia_w		:= trim(substr(ds_conteudo_w,29,4));
			dt_mes_referencia_w		:= trim(substr(ds_conteudo_w,33,2));
			nr_versao_w			:= trim(substr(ds_conteudo_w,35,3));
			
			select	ptu_pedido_contagem_benef_seq.NextVal
			into	nr_seq_pedido_cont_w
			from	dual;
			
			if	(ie_tipo_cliente_w	= 'UNIMED') then
				ie_tipo_cliente_w	:= 'U';
			elsif	(ie_tipo_cliente_w	= 'PORTAL') then
				ie_tipo_cliente_w	:= 'P';
			elsif	(ie_tipo_cliente_w	= 'PRESTADOR') then
				ie_tipo_cliente_w	:= 'R';
			end if;
			
			insert	into ptu_pedido_contagem_benef
				(nr_sequencia, cd_transacao, cd_unimed_destino,
				 cd_unimed_requisitante, ds_ano_referencia, ds_mes_referencia,
				 ie_tipo_cliente, dt_atualizacao_nrec, dt_atualizacao,
				 nm_usuario, nm_usuario_nrec, dt_ano_referencia,
				 dt_mes_referencia, nr_versao)
			values	(nr_seq_pedido_cont_w, cd_transacao_w, cd_unimed_destino_w,
				 cd_unimed_requisitante_w, dt_ano_referencia_w, dt_mes_referencia_w,
				 ie_tipo_cliente_w, sysdate, sysdate,
				 nm_usuario_p, nm_usuario_p, sysdate, 
				 sysdate, nr_versao_w);
		end if;
	end if;
	end;
end loop;
close C01;

ptu_gest_env_res_cont_benef(nr_seq_pedido_cont_w, cd_estabelecimento_p, nm_usuario_p);
nr_seq_pedido_cont_p	:= nr_seq_pedido_cont_w;

commit;

end ptu_imp_req_contagem_benef_v50;
/