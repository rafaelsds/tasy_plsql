create or replace
procedure excluir_sinonimo_dicionario(
					nr_sequencia_p		number,
					nm_usuario_p		varchar2) is 

begin
delete	from	dicionario_sinonimo
where	((nr_seq_dicionario	= nr_sequencia_p)
or	(nr_seq_dicionario_sin	= nr_sequencia_p));

commit;

end excluir_sinonimo_dicionario;
/