create or replace
procedure  pls_verif_tolerancia_conta(  nr_seq_conta_proc_p	number,				
					nr_seq_conta_mat_p	number,
					ie_tipo_tabela_p	varchar2,
					nm_usuario_p		Varchar2) is 
vl_apresentado_menor_w		number(15,2);				
vl_apresentado_maior_w		number(15,2);
vl_tolerancia_maior_w		number(15,2);
vl_tolerancia_menor_w		number(15,2);
vl_apresentado_w		number(15,2);
vl_calculado_w			number(15,2);
vl_total_procedimento_w		number(15,2);
nr_motivo_glosa_maior_w		number(10);
nr_motivo_glosa_menor_w		number(10);
ie_ocorrencia_w			pls_controle_estab.ie_ocorrencia%type := 'N';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_tp_desp_conta_proc_w	pls_conta_proc.ie_tipo_despesa%type;
ie_tp_desp_conta_mat_w	pls_conta_mat.ie_tipo_despesa%type;
/*ie_tipo_tabela verifica em qual tabela ira fazer o update
p = procedimento
M = material*/

begin
ie_ocorrencia_w := pls_obter_se_controle_estab('GO');
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_tipo_tabela_p = 'P')	then
	select	nvl(vl_procedimento_imp,0),
			nvl(vl_total_procedimento,0),
			ie_tipo_despesa
	into	vl_apresentado_w,
			vl_calculado_w,
			ie_tp_desp_conta_proc_w
	from 	pls_conta_proc
	where 	nr_sequencia = nr_seq_conta_proc_p;

end if;

if	(ie_tipo_tabela_p = 'M')	then
	select	nvl(vl_material_imp,0),
			nvl(vl_material,0),
			ie_tipo_despesa
	into	vl_apresentado_w,
			vl_calculado_w,
			ie_tp_desp_conta_mat_w
	from 	pls_conta_mat
	where 	nr_sequencia = nr_seq_conta_mat_p;
end if;

select	max(nr_sequencia)
into 	nr_motivo_glosa_maior_w
from 	tiss_motivo_glosa
where 	cd_motivo_tiss = '1705';

if (nr_seq_conta_proc_p is not null) then
	if	(ie_ocorrencia_w = 'N') then
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_maior_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_proc is null) or (ie_tipo_despesa_proc = ie_tp_desp_conta_proc_w))
		and	nr_seq_motivo_glosa = nr_motivo_glosa_maior_w;
	else
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_maior_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_proc is null) or (ie_tipo_despesa_proc = ie_tp_desp_conta_proc_w))
		and	nr_seq_motivo_glosa = nr_motivo_glosa_maior_w
		and	cd_estabelecimento = cd_estabelecimento_w;
	end if;
else
	if	(ie_ocorrencia_w = 'N') then
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_maior_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_mat is null) or (ie_tipo_despesa_mat = ie_tp_desp_conta_mat_w))
		and		nr_seq_motivo_glosa = nr_motivo_glosa_maior_w;
	else
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_maior_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_mat is null) or (ie_tipo_despesa_mat = ie_tp_desp_conta_mat_w))
		and		nr_seq_motivo_glosa = nr_motivo_glosa_maior_w
		and		cd_estabelecimento = cd_estabelecimento_w;
	end if;
end if;

select	max(nr_sequencia)
into 	nr_motivo_glosa_menor_w
from 	tiss_motivo_glosa
where 	cd_motivo_tiss 	= '1706';

if (nr_seq_conta_proc_p is not null) then
	if	(ie_ocorrencia_w = 'N') then
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_menor_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_proc is null) or (ie_tipo_despesa_proc = ie_tp_desp_conta_proc_w))
		and	nr_seq_motivo_glosa = nr_motivo_glosa_menor_w;
	else
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_menor_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_proc is null) or (ie_tipo_despesa_proc = ie_tp_desp_conta_proc_w))
		and	nr_seq_motivo_glosa = nr_motivo_glosa_menor_w
		and	cd_estabelecimento = cd_estabelecimento_w;
	end if;
else
	if	(ie_ocorrencia_w = 'N') then
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_menor_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_mat is null) or (ie_tipo_despesa_mat = ie_tp_desp_conta_mat_w))
		and	nr_seq_motivo_glosa = nr_motivo_glosa_menor_w;
	else
		select	nvl(max(vl_tolerancia_glosa),0)
		into 	vl_tolerancia_menor_w
		from 	pls_regra_lanc_glosa
		where 	ie_situacao = 'A'
		and		((ie_tipo_despesa_mat is null) or (ie_tipo_despesa_mat = ie_tp_desp_conta_mat_w))
		and	nr_seq_motivo_glosa = nr_motivo_glosa_menor_w
		and	cd_estabelecimento = cd_estabelecimento_w;
	end if;
end if;
	
vl_apresentado_maior_w  := vl_apresentado_w - vl_tolerancia_maior_w;
vl_apresentado_menor_w	:= vl_apresentado_w + vl_tolerancia_menor_w;

if	(nvl(vl_apresentado_maior_w,0) > nvl(vl_calculado_w,0)) then
	goto final;	
elsif	(nvl(vl_apresentado_menor_w,0) < nvl(vl_calculado_w,0)) and (nvl(vl_calculado_w,0)>0)  and
	(nvl(vl_apresentado_w,0) > 0) then			
	goto final;
end if;

if	(nvl( vl_apresentado_w,0) > 0) then

	if (ie_tipo_tabela_p = 'P') then
	update	pls_conta_proc
	set 	vl_total_procedimento = vl_apresentado_w
	where 	nr_sequencia 	      = nr_seq_conta_proc_p;
	end if;

	if (ie_tipo_tabela_p = 'M') then
	update	pls_conta_mat
	set 	vl_material  = vl_apresentado_w
	where 	nr_sequencia = nr_seq_conta_mat_p;
	end if;
end if;

<<final>>
null;
/*
commit;N�o pode haver commit em procedure intermedi�ria
*/
end pls_verif_tolerancia_conta;
/
