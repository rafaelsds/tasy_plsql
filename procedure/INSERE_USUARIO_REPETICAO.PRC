create or replace
procedure insere_usuario_repeticao( nr_prescricao_p	number,
				     nr_seq_prescr_p	number,
				     nm_usuario_p	varchar2 ) is 
begin
insert	into PRESCR_PROC_EXAME_REPET(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_prescricao,
		nr_seq_prescr
	)
values
	(
		PRESCR_PROC_EXAME_REPET_seq.nextVal,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_prescricao_p,
		nr_seq_prescr_p
	);
commit;
end ;
/
