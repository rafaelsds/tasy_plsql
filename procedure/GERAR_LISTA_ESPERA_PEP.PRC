create or replace
procedure gerar_lista_espera_pep(
	nr_seq_pedido_p	number,
	nm_tabela_pedido_p	varchar2,
	nm_usuario_p	varchar2) is 

cd_agenda_w				number(10);
cd_especialidade_w		number(5);
cd_medico_w				varchar2(10);
cd_tipo_agenda_w		number(10);
ie_classif_agenda_w		varchar2(1);
cd_pessoa_fisica_w		varchar2(10);
cd_medico_dest_w		varchar2(10);
nr_seq_programa_w		number(10);
cd_espec_medico_w		number(5);
cd_espec_encaminhamento_w	number(5);
nr_seq_exame_w			number(10);
nr_seq_exame_lab_w		number(10);
cd_material_exame_w		varchar2(20);
ds_exame_sem_cad_w		varchar2(255);
cd_procedimento_w		number(15);
nr_proc_interno_w		number(10);
ie_origem_proced_w		number(10);
ie_urgente_w			varchar2(1);
qt_exame_w				number(5);
cd_ciap_w				varchar2(5);
cd_doenca_cid_w			varchar2(10);
ds_observacao_w			varchar2(4000);
nr_Atendimento_w		number(10);
cd_convenio_w			number(5);
ie_pendente_w			varchar2(1);	

Cursor C01 is
	select	a.nr_seq_exame,
		a.nr_seq_exame_lab,
		a.cd_material_exame,
		a.ds_exame_sem_cad,
		a.cd_procedimento,
		a.nr_proc_interno,
		a.ie_origem_proced,
		a.ie_urgente,
		a.qt_exame		
	from	pedido_exame_externo_item a
	where	a.nr_seq_pedido = nr_seq_pedido_p;

begin

if (nr_seq_pedido_p is not null) then
	if (nm_tabela_pedido_p = 'ATEND_ENCAMINHAMENTO') then
		select	max(a.cd_agenda),
			max(a.cd_pessoa_fisica),
			max(a.cd_medico_dest),
			max(a.cd_especialidade),
			max(obter_especialidade_medico(a.cd_medico_dest,null)),
			max(cd_ciap),
			max(cd_doenca),
			max(ds_observacao),
			max(nr_atendimento)
		into	cd_agenda_w,
			cd_pessoa_fisica_w,
			cd_medico_dest_w,
			cd_espec_encaminhamento_w,
			cd_espec_medico_w,
			cd_ciap_w,
			cd_doenca_cid_w,
			ds_observacao_w,
			nr_Atendimento_w
		from	atend_encaminhamento a
		where	a.nr_sequencia = nr_seq_pedido_p;
		
		
		select 	max(obter_convenio_atendimento(nr_Atendimento_w))
		into	cd_convenio_w
		from 	dual;

		select	max(a.cd_especialidade),
			max(a.cd_pessoa_fisica),
			max(a.cd_tipo_agenda),
			max(a.ie_classificacao)			
		into	cd_especialidade_w,
			cd_medico_w,
			cd_tipo_agenda_w,
			ie_classif_agenda_w
		from	agenda a
		where	a.cd_agenda = cd_agenda_w;

		insert into agenda_lista_espera (
			nr_sequencia,
			cd_ciap,
			cd_cid,
			cd_agenda,
			cd_especialidade,
			cd_medico,
			cd_tipo_agenda,
			ie_classif_agenda,
			nm_tabela_pedido,
			nr_seq_pedido,
			cd_pessoa_fisica,
			dt_agendamento,
			dt_atualizacao,
			dt_atualizacao_nrec,
			dt_desejada,
			ie_status_espera,
			nm_usuario_agenda,
			nm_usuario,
			nm_usuario_nrec,
			ds_observacao,
			cd_convenio)
		values (
			agenda_lista_espera_seq.nextval,
			cd_ciap_w,
			cd_doenca_cid_w,
			cd_agenda_w,
			nvl(cd_especialidade_w,nvl(cd_espec_encaminhamento_w,cd_espec_medico_w)),
			nvl(cd_medico_w,cd_medico_dest_w),
			cd_tipo_agenda_w,
			ie_classif_agenda_w,
			nm_tabela_pedido_p,
			nr_seq_pedido_p,
			cd_pessoa_fisica_w,
			sysdate,
			sysdate,
			sysdate,
			sysdate,
			'A',
			nm_usuario_p,
			nm_usuario_p,
			nm_usuario_p,
			substr(ds_observacao_w,1,2000),
			cd_convenio_w);
	elsif (nm_tabela_pedido_p = 'ATEND_PROGRAMA_SAUDE') then	
			
		gerar_programa_saude_pep(nr_seq_pedido_p);	
							
	elsif (nm_tabela_pedido_p = 'PEDIDO_EXAME_EXTERNO') then
		select	max(a.cd_pessoa_fisica),
				max(a.nr_atendimento),
			max(IE_PENDENTE)
		into	cd_pessoa_fisica_w,
				nr_Atendimento_w,
			ie_pendente_w
		from	pedido_exame_externo a
		where	a.nr_sequencia = nr_seq_pedido_p;

		if (nvl(IE_PENDENTE_W,'N') = 'S') then

			open C01;
			loop
			fetch C01 into	
				nr_seq_exame_w,
				nr_seq_exame_lab_w,
				cd_material_exame_w,
				ds_exame_sem_cad_w,
				cd_procedimento_w,
				nr_proc_interno_w,
				ie_origem_proced_w,
				ie_urgente_w,
				qt_exame_w;
			exit when C01%notfound;
				begin
				
				
				cd_convenio_w := obter_convenio_atendimento(nr_Atendimento_w);
				
				if (nr_seq_exame_w is not null) then
				
					
				
					insert into agenda_lista_espera (
						nr_sequencia,
						nr_seq_exame,
						ie_urgente,
						qt_procedimento,
						nm_tabela_pedido,
						nr_seq_pedido,
						cd_pessoa_fisica,
						dt_agendamento,
						dt_atualizacao,
						dt_atualizacao_nrec,
						dt_desejada,
						ie_status_espera,
						nm_usuario_agenda,
						nm_usuario,
						nm_usuario_nrec,
						cd_convenio)
					values (
						agenda_lista_espera_seq.nextval,
						nr_seq_exame_w,
						ie_urgente_w,
						qt_exame_w,
						nm_tabela_pedido_p,
						nr_seq_pedido_p,
						cd_pessoa_fisica_w,
						sysdate,
						sysdate,
						sysdate,
						sysdate,
						'A',
						nm_usuario_p,
						nm_usuario_p,
						nm_usuario_p,
						cd_convenio_w);
				end if;

				if (nr_seq_exame_lab_w is not null) then
					insert into agenda_lista_espera (
						nr_sequencia,
						nr_seq_exame_lab,
						cd_material_exame,
						ie_urgente,
						qt_procedimento,
						nm_tabela_pedido,
						nr_seq_pedido,
						cd_pessoa_fisica,
						dt_agendamento,
						dt_atualizacao,
						dt_atualizacao_nrec,
						dt_desejada,
						ie_status_espera,
						nm_usuario_agenda,
						nm_usuario,
						nm_usuario_nrec,
						cd_convenio)
					values (
						agenda_lista_espera_seq.nextval,
						nr_seq_exame_lab_w,
						cd_material_exame_w,
						ie_urgente_w,
						qt_exame_w,
						nm_tabela_pedido_p,
						nr_seq_pedido_p,
						cd_pessoa_fisica_w,
						sysdate,
						sysdate,
						sysdate,
						sysdate,
						'A',
						nm_usuario_p,
						nm_usuario_p,
						nm_usuario_p,
						cd_convenio_w);
				end if;

				if (ds_exame_sem_cad_w is not null) then
					insert into agenda_lista_espera (
						nr_sequencia,
						ds_exame_sem_cad,
						ie_urgente,
						qt_procedimento,
						nm_tabela_pedido,
						nr_seq_pedido,
						cd_pessoa_fisica,
						dt_agendamento,
						dt_atualizacao,
						dt_atualizacao_nrec,
						dt_desejada,
						ie_status_espera,
						nm_usuario_agenda,
						nm_usuario,
						nm_usuario_nrec,
						cd_convenio)
					values (
						agenda_lista_espera_seq.nextval,
						ds_exame_sem_cad_w,
						ie_urgente_w,
						qt_exame_w,
						nm_tabela_pedido_p,
						nr_seq_pedido_p,
						cd_pessoa_fisica_w,
						sysdate,
						sysdate,
						sysdate,
						sysdate,
						'A',
						nm_usuario_p,
						nm_usuario_p,
						nm_usuario_p,
						cd_convenio_w);
				end if;

				if (cd_procedimento_w is not null) then
					insert into agenda_lista_espera (
						nr_sequencia,
						cd_procedimento,
						ie_origem_proced,
						ie_urgente,
						qt_procedimento,
						nm_tabela_pedido,
						nr_seq_pedido,
						cd_pessoa_fisica,
						dt_agendamento,
						dt_atualizacao,
						dt_atualizacao_nrec,
						dt_desejada,
						ie_status_espera,
						nm_usuario_agenda,
						nm_usuario,
						nm_usuario_nrec,
						cd_convenio)
					values (
						agenda_lista_espera_seq.nextval,
						cd_procedimento_w,
						ie_origem_proced_w,
						ie_urgente_w,
						qt_exame_w,
						nm_tabela_pedido_p,
						nr_seq_pedido_p,
						cd_pessoa_fisica_w,
						sysdate,
						sysdate,
						sysdate,
						sysdate,
						'A',
						nm_usuario_p,
						nm_usuario_p,
						nm_usuario_p,
						cd_convenio_w);
				end if;

				if (nr_proc_interno_w is not null) then
					insert into agenda_lista_espera (
						nr_sequencia,
						nr_seq_proc_interno,
						ie_urgente,
						qt_procedimento,
						nm_tabela_pedido,
						nr_seq_pedido,
						cd_pessoa_fisica,
						dt_agendamento,
						dt_atualizacao,
						dt_atualizacao_nrec,
						dt_desejada,
						ie_status_espera,
						nm_usuario_agenda,
						nm_usuario,
						nm_usuario_nrec,
						cd_convenio)
					values (
						agenda_lista_espera_seq.nextval,
						nr_proc_interno_w,
						ie_urgente_w,
						qt_exame_w,
						nm_tabela_pedido_p,
						nr_seq_pedido_p,
						cd_pessoa_fisica_w,
						sysdate,
						sysdate,
						sysdate,
						sysdate,
						'A',
						nm_usuario_p,
						nm_usuario_p,
						nm_usuario_p,
						cd_convenio_w);
				end if;
				end;
			end loop;
			close C01;
		end if;	
		
	end if;
end if;

commit;

end gerar_lista_espera_pep;
/