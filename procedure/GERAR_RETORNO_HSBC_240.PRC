create or replace
procedure GERAR_RETORNO_HSBC_240(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p	varchar2) is 

nr_seq_reg_T_w			number(10);
nr_seq_reg_U_w			number(10);
nr_titulo_w			number(10);
vl_titulo_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
vl_outras_despesas_w		number(15,2);
dt_liquidacao_w			date;
ds_titulo_w			varchar2(255);
vl_cobranca_w			number(15,2);
vl_alterar_w			number(15,2);
cd_ocorrencia_w			number(10);
nr_seq_ocorrencia_ret_w		number(10);
vl_saldo_inclusao_w		number(15,2);
ds_log_w				varchar2(255);

cursor c01 is
	select	nr_sequencia,
		trim(substr(ds_string,38,16)),
		to_number(substr(ds_string,82,15))/100
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,8,1)	= '3'
	and	substr(ds_string,14,1)	= 'T'
	and	substr(ds_string,16,2)	<> '28';
begin

open C01;
loop
fetch C01 into	
	nr_seq_reg_T_w,
	ds_titulo_w,
	vl_cobranca_w;
exit when C01%notfound;
	begin

	vl_alterar_w	:= 0;

	/* Encontrar pelo t�tulo externo */
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo_externo = ds_titulo_w;

	/* Se n�o econtrou, procura pelo t�tulo no Tasy */
	if	(nr_titulo_w is null) then
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_titulo	= somente_numero(ds_titulo_w);
		
		if  (nr_titulo_w is null) then /* Quando n�o h� t�tulo, busca pelo nosso numero */
			select	max(nr_titulo)
			into  	nr_titulo_w
			from  	titulo_receber
			where  	nr_nosso_numero  = ds_titulo_w;
			
			if  (nr_titulo_w is null) then
				select	max(nr_titulo)
				into  	nr_titulo_w
				from  	titulo_receber
				where  	somente_numero(nr_nosso_numero)  = somente_numero(ds_titulo_w);			  
			end if;			
		end if;			
	end if;	


	/* Se encontrou o t�tulo importa, sen�o grava no log */

	if	(nr_titulo_w is not null) then

		select	max(vl_titulo),
			max(vl_saldo_titulo)
		into	vl_titulo_w,
			vl_saldo_inclusao_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		nr_seq_reg_U_w := nr_seq_reg_T_w + 1;

		select	nvl(to_number(substr(ds_string,18,15))/100,0),
			nvl(to_number(substr(ds_string,33,15))/100,0),
			nvl(to_number(substr(ds_string,48,15))/100,0),
			nvl(to_number(substr(ds_string,93,15))/100,0),
			nvl(to_number(substr(ds_string,108,15))/100,0),
			to_date(decode(substr(ds_string,146,8),'00000000',null,substr(ds_string,146,8)),'dd/mm/yyyy'),
			to_number(substr(ds_string,16,2))
		into	vl_acrescimo_w,
			vl_desconto_w,
			vl_abatimento_w,
			vl_liquido_w,
			vl_outras_despesas_w,
			dt_liquidacao_w,
			cd_ocorrencia_w
		from	w_retorno_banco
		where	nr_sequencia	= nr_seq_reg_U_w;

		select 	max(a.nr_sequencia)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco	= 399
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		/* Tratar acrescimos/descontos */
		if	(vl_titulo_w <> vl_liquido_w) then
			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;	
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;
		end if;

		insert	into titulo_receber_cobr (	NR_SEQUENCIA,
							NR_TITULO,
							CD_BANCO,
							VL_COBRANCA,
							VL_DESCONTO,
							VL_ACRESCIMO,
							VL_DESPESA_BANCARIA,
							VL_LIQUIDACAO,
							DT_LIQUIDACAO,
							DT_ATUALIZACAO,
							NM_USUARIO,
							NR_SEQ_COBRANCA,
							nr_seq_ocorrencia_ret,
							vl_saldo_inclusao)
					values	(	titulo_receber_cobr_seq.nextval,
							nr_titulo_w,
							399,
							vl_titulo_w,
							vl_desconto_w,
							vl_acrescimo_w,
							vl_outras_despesas_w,
							vl_liquido_w,
							dt_liquidacao_w,
							sysdate,
							nm_usuario_p,
							nr_seq_cobr_escrit_p,
							nr_seq_ocorrencia_ret_w,
							vl_saldo_inclusao_w);
	else
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(304455,'DS_TITULO_W=' || ds_titulo_w),1,255);
		fin_gerar_log_controle_banco(3,ds_log_w,nm_usuario_p,'N');

	end if;
	
	end;
end loop;
close C01;

commit;

end GERAR_RETORNO_HSBC_240;
/
