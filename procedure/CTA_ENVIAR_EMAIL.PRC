create or replace
procedure cta_enviar_email(	nr_atendimento_p		number,
				nr_interno_conta_p		number,
				nr_seq_pendencia_p		number,
				nr_seq_tipo_p			number,
				nr_seq_estagio_p		number,
				ie_evento_p			varchar2,
				ds_texto_p			varchar2,
				nm_usuario_p			varchar2) is 
				
ds_texto_email_w		varchar2(4000);
ds_assunto_email_w		varchar2(255):= '';
ds_email_destino_w		cta_regra_envio_email.ds_email_destino%type := null;
nm_usuario_envio_w		cta_regra_envio_email.nm_usuario_envio%type := null;
nr_sequencia_w			cta_regra_envio_email.nr_sequencia%type;

Cursor C01 is
	select	ds_email_destino,
		nm_usuario_envio
	from	cta_regra_envio_email
	where	ie_evento = ie_evento_p
	and	((nr_seq_tipo = nr_seq_tipo_p) or (nr_seq_tipo is null))
	and	((nr_seq_estagio = nr_seq_estagio_p) or (nr_seq_estagio is null))
	and	ie_situacao = 'A';

begin

ds_texto_email_w := Wheb_mensagem_pck.get_texto(413311) /* Paciente */ || ': ' || obter_pessoa_atendimento(nr_atendimento_p,'N') || chr(10) || chr(13);
ds_texto_email_w := ds_texto_email_w || Wheb_mensagem_pck.get_texto(270613) /* Atendimento */ || ': ' || nr_atendimento_p || chr(10) || chr(13);
ds_texto_email_w := ds_texto_email_w || Wheb_mensagem_pck.get_texto(270612) /* Conta */ || ': ' || nr_interno_conta_p || chr(10) || chr(13);
ds_texto_email_w := ds_texto_email_w || Wheb_mensagem_pck.get_texto(455722) /* Seq pend�ncia */ || ': ' || nr_seq_pendencia_p || chr(10) || chr(13);
ds_texto_email_w := ds_texto_email_w || Wheb_mensagem_pck.get_texto(418867) /* Tipo */ || ': ' || obter_desc_tipo_pendencia(nr_seq_tipo_p) || chr(10) || chr(13);
ds_texto_email_w := ds_texto_email_w || Wheb_mensagem_pck.get_texto(413313) /* Est�gio */ || ': ' || obter_desc_cta_estagio_pend(nr_seq_estagio_p) || chr(10) || chr(13);

if	(nvl(ds_texto_p,'X') <> 'X') then
	ds_texto_email_w := substr(ds_texto_email_w || Wheb_mensagem_pck.get_texto(455728) /* Complemento*/ || ': '  || ds_texto_p 	|| chr(10) || chr(13),1,4000);
end if;

if	(ie_evento_p = 'E') then
	ds_assunto_email_w := Wheb_mensagem_pck.get_texto(455729) /* Altera��o de Est�gio da Pend�ncia */;
elsif	(ie_evento_p = 'F') then
	ds_assunto_email_w := Wheb_mensagem_pck.get_texto(455730) /* Fechamento da Pend�ncia */;
elsif	(ie_evento_p = 'A') then
	ds_assunto_email_w := Wheb_mensagem_pck.get_texto(455731) /* Abertura de Pend�ncia */;
end if;

open C01;
loop
fetch 	C01 into	
	ds_email_destino_w,
	nm_usuario_envio_w;
exit when C01%notfound;
	begin
	if	(ds_email_destino_w is not null) then
	enviar_email(	ds_assunto_email_w,
			ds_texto_email_w,
			null,
			ds_email_destino_w,
			nvl(nm_usuario_envio_w,nm_usuario_p),
			null);	
	end if;
	end;
end loop;
close C01;

end cta_enviar_email;
/