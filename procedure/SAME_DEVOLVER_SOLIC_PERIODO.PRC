create or replace procedure Same_Devolver_Solic_Periodo (	nr_seq_prontuario_p	number,
					nr_seq_solicitacao_p	number,
					nm_usuario_p		varchar,
					ie_Acao_p			varchar) is

cd_estabelecimento_w		number(4,0);
nr_atendimento_w		number(10,0);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
qt_restante_w			number(5,0);
nr_seq_local_w			number(10,0);
nr_seq_caixa_w			number(10,0);
nr_seq_agrup_w			number(10,0);
cd_pessoa_solicitante_w		varchar2(10);
cd_setor_solicitante_w		number(5,0);
ie_status_dev_w			varchar2(1);
ie_status_dev_desf_w		varchar2(1);

ie_status_na_devolucao_w	varchar2(10);
ie_armazena_na_devolucao_w	varchar2(10);
begin

if (ie_Acao_p = 'D') then
	begin
	/* Atualiza o status da solicita��o */
	update	same_solic_pront
	set	dt_devolucao		= sysdate,
		ie_status		= 'D',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_solicitacao_p;

	/* Pega alguns dados do prontu�rio */
	select	cd_estabelecimento,
		nr_atendimento,
		dt_periodo_inicial,
		nvl(dt_periodo_final,sysdate),
		nr_seq_local,
		nr_seq_caixa,
		nr_seq_agrup
	into	cd_estabelecimento_w,
		nr_atendimento_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		nr_seq_local_w,
		nr_seq_caixa_w,
		nr_seq_agrup_w
	from	same_prontuario
	where	nr_sequencia		= nr_seq_prontuario_p;

	/* Pega alguns dados da solicita��o */
	select	cd_pessoa_solicitante,
		cd_setor_solicitante
	into	cd_pessoa_solicitante_w,
		cd_setor_solicitante_w
	from	same_solic_pront
	where	nr_sequencia		= nr_seq_solicitacao_p;

	/* Verifica se ainda tem alguma solicita��o por per�odo n�o entregue */
	select	count(*)
	into	qt_restante_w
	from	same_solic_pront a
	where	a.cd_estabelecimento	= cd_estabelecimento_w
	and	a.nr_atendimento       	= nr_atendimento_w
	and	a.dt_periodo_inicial   	is not null
	and	a.dt_periodo_final     	is not null
	and	a.dt_devolucao         	is null
	and	a.dt_periodo_inicial   	>= dt_periodo_inicial_w
	and	a.dt_periodo_final	<= dt_periodo_final_w;

	ie_status_na_devolucao_w	:= nvl(obter_valor_param_usuario(941,82,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w),'D');
	ie_armazena_na_devolucao_w	:= nvl(obter_valor_param_usuario(941,234,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w),'S');	
	
	/* Caso n�o tenha mais solicita��es em aberto, volta o status do prontu�rio para armazenado */
	if	(((ie_status_na_devolucao_w <> 'V') or (ie_armazena_na_devolucao_w <> 'N')) and (qt_restante_w = 0)) then
		update	same_prontuario
		set	ie_status    	= '1'
		where	nr_sequencia 	= nr_seq_prontuario_p;
	end if;

	/* Gera um hist�rico de devolu��o parcial */
	Gestao_Prontuario_Same(	nr_seq_prontuario_p,
				nm_usuario_p,
				cd_pessoa_solicitante_w,
				cd_setor_solicitante_w,
				nr_seq_local_w,
				nr_seq_caixa_w,
				nr_seq_agrup_w,
				'',
				21,
				null,
				null,
				null);
	end;
elsif (ie_Acao_p = 'E')	then
	begin
	select	nvl(max(obter_valor_param_usuario(941, 82, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento)), 'D')
	into	ie_status_dev_w
	from	dual;

	obter_param_usuario(941,196,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_status_dev_desf_w);
	
	
	if	(ie_status_dev_w = 'V') and
		(ie_status_dev_desf_w = 'S') then	
		same_desfazer_devolucao_lote(nr_seq_solicitacao_p,'N',nm_usuario_p);
	else
		update	same_solic_pront
		set	dt_devolucao		= null,
			ie_status		= 'E',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			dt_receb_devolucao 	= null,
			nm_usuario_receb_dev 	= null,
			dt_recebimento 		= null,
			nm_usuario_receb 	= null
		where	nr_sequencia		= nr_seq_solicitacao_p;	

    --458300 = Solicitacao: '||nr_seq_solicitacao_p||' alterado para Entregue.
		gravar_log_tasy(45878,WHEB_MENSAGEM_PCK.get_texto(458300, 'nr_seq_solicitacao='||(nr_seq_solicitacao_p)),nm_usuario_p);	
		
		select	cd_estabelecimento,
			nr_atendimento,
			dt_periodo_inicial,
			nvl(dt_periodo_final,sysdate),
			nr_seq_local,
			nr_seq_caixa,
			nr_seq_agrup
		into	cd_estabelecimento_w,
			nr_atendimento_w,
			dt_periodo_inicial_w,
			dt_periodo_final_w,
			nr_seq_local_w,
			nr_seq_caixa_w,
			nr_seq_agrup_w
		from	same_prontuario
		where	nr_sequencia		= nr_seq_prontuario_p;

		select	count(*)
		into	qt_restante_w
		from	same_solic_pront a
		where	a.cd_estabelecimento   	= cd_estabelecimento_w
		and	a.nr_atendimento       	= nr_atendimento_w
		and	a.dt_periodo_inicial   	is not null
		and	a.dt_periodo_final      is not null
		and	a.dt_devolucao         	is null
		and	a.dt_periodo_inicial   	>= dt_periodo_inicial_w
		and	a.dt_periodo_final	<= dt_periodo_final_w;

		if	(qt_restante_w > 0) then
			update	same_prontuario
			set	ie_status    	= '8'
			where	nr_sequencia 	= nr_seq_prontuario_p;
		end if;
	end if;
	
	
	end;
end if;

commit;

end Same_Devolver_Solic_Periodo;
/