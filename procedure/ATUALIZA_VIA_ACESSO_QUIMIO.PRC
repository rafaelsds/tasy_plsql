create or replace
procedure atualiza_via_acesso_quimio(
		nr_seq_via_acesso_p		number,
    nr_seq_interno_p      number,
		nr_seq_atendimento_p	number,
		ie_via_aplicacao_p		varchar2,
    atualizar_todos_p     varchar2) is 

begin

if (nr_seq_via_acesso_p is not null) and
    (nr_seq_interno_p is not null) and
    (nr_seq_atendimento_p is not null) and
    (ie_via_aplicacao_p is not null) and
    (atualizar_todos_p is not null) then

  update	paciente_atend_medic
  set	nr_seq_via_acesso	= nr_seq_via_acesso_p
  where	nr_seq_atendimento = nr_seq_atendimento_p
  and (atualizar_todos_p = 'S' or nr_seq_interno = nr_seq_interno_p)
  and	nr_seq_diluicao	is null
  and	ie_via_aplicacao	= ie_via_aplicacao_p;
  
  commit;

end if;

end atualiza_via_acesso_quimio;
/