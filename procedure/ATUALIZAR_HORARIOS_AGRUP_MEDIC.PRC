create or replace
procedure Atualizar_horarios_agrup_medic( nr_prescricao_p	number,
					nr_sequencia_p		number,
					nr_agrupamento_p	number,
					nr_agrup_ant_p		number,
					ie_farm_enf_p		varchar2,
					ie_volta_horario_composto_p varchar2,
					ds_horario_composto_p	varchar2,
					ds_horarios_p		OUT varchar2,
					cd_estabelecimento_p	number,
					ie_volta_horario_p	varchar2,
					nm_usuario_p		Varchar2) is 

ds_menor_hora_w		varchar2(255);
ds_horarios_w		varchar2(255);
ds_primeira_hora_w	varchar2(255);
nr_sequencia_w		number(15,0);
nr_seq_menor_horario_w	number(15,0);
nr_agrupamento_ant_w	number(07,1) := -1;
NR_AGRUPAMENTO_W	number(07,1);
ds_prim_horario_w	varchar2(5);
dt_primeiro_horario_w	date;
dt_prescricao_w		date;
qt_minutos_adic_w	number(10,0);
qt_hora_aplicacao_w	number(10,0);
cd_material_w		number(10,0);
nr_ocorrencia_w		number(15,4);
cd_intervalo_ww		varchar2(10);
ds_horarios2_w		varchar2(255);
ie_gerar_horarios_w	varchar2(1);
hr_prim_horario_w	varchar2(10);
hr_prim_horario_ant_w	varchar2(10);
qt_min_aplicacao_w	number(10,0);

cursor c01 is
select	a.ds_horarios,
	a.nr_sequencia
from	prescr_material a
where	a.nr_prescricao = nr_prescricao_p
and	nr_agrupamento	= nr_agrupamento_p
and	a.ds_horarios	is not null
and	obter_classif_material_proced(a.cd_material, null,null) <> 1
and	a.nr_sequencia_solucao	is null
and	a.nr_sequencia_proc	is null
and	a.ie_agrupador		= 1
and	a.ie_suspenso		<> 'S'
and	nvl(ie_administrar,'S')	= 'S'
and	a.nr_sequencia_diluicao	is null;

cursor c02 is
select	b.nr_sequencia,
	b.nr_agrupamento,
	b.qt_hora_aplicacao,
	b.qt_min_aplicacao,
	b.cd_intervalo,
	b.nr_ocorrencia,
	b.cd_material,
	a.dt_prescricao,
	b.hr_prim_horario
from	prescr_material b,
	prescr_medica a
where	a.nr_prescricao	= nr_prescricao_p
and	a.nr_prescricao	= b.nr_prescricao
--and	b.nr_agrupamento = nr_agrupamento_p
and	b.ie_agrupador	= 1
order by b.nr_agrupamento;

begin

obter_param_usuario(3130, 67, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, qt_minutos_adic_w);
obter_param_usuario(281, 423, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_horarios_w);

ds_menor_hora_w	:= '23:59';
open c01;
loop
fetch c01 into
	ds_horarios_w,
	nr_sequencia_w;
exit when c01%notfound;

	ds_primeira_hora_w	:= substr(ds_horarios_w,1,5);
	if	(instr(substr(ds_horarios_w,1,3),':') = 0) then
		ds_primeira_hora_w	:= substr(ds_horarios_w,1,2) || ':00';
	end if;
	
	if	(ds_primeira_hora_w	< ds_menor_hora_w) then
		ds_menor_hora_w		:= ds_primeira_hora_w;
		nr_seq_menor_horario_w	:= nr_sequencia_w;
	end if;	
end loop;
close c01;

select	dt_primeiro_horario,
	dt_prescricao
into	dt_primeiro_horario_w,
	dt_prescricao_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

if	(dt_primeiro_horario_w < sysdate) then
	dt_primeiro_horario_w	:= sysdate;
	dt_prescricao_w		:= sysdate;
end if;

if	(ie_farm_enf_p	= 'F') then

	if	(nvl(nr_seq_menor_horario_w,0) <> 0) and
		(ie_volta_horario_p	= 'N') then
		select	max(ds_horarios)
		into	ds_horarios_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_menor_horario_w;
		
		if	(nvl(ds_horario_composto_p,'X') <> 'X') then  --Voltar todos os itens ao hor�rio original.
			ds_horarios_w	:= ds_horario_composto_p;
		end if;
		
		update	prescr_material
		set	ds_horarios	= ds_horarios_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_prescricao	= nr_prescricao_p
		and	nr_agrupamento	= nr_agrupamento_p
		and	nr_sequencia	<> nr_seq_menor_horario_w
		and	obter_classif_material_proced(cd_material, null,null) <> 1
		and	nr_sequencia_solucao	is null
		and	nr_sequencia_proc	is null
		and	ie_agrupador		= 1
		and	ie_suspenso		<> 'S'
		and	nvl(ie_administrar,'S')	= 'S'
		and	nr_sequencia_diluicao	is null;

		ds_horarios_p	:= ds_horarios_w;
		
		commit;
	else	
	
		open c02;
		loop
		fetch c02 into	
			nr_sequencia_w,
			nr_agrupamento_w,
			qt_hora_aplicacao_w,
			qt_min_aplicacao_w,
			cd_intervalo_ww,
			nr_ocorrencia_w,
			cd_material_w,
			dt_prescricao_w,
			hr_prim_horario_w;
		exit when c02%notfound;
			begin
			
			if	(nr_agrupamento_w <> nr_agrupamento_ant_w) then
				begin
				select	to_date(to_char(dt_prescricao_w,'dd/mm/yyyy') || nvl(hr_prim_horario_ant_w, hr_prim_horario_w),'dd/mm/yyyy hh24:mi')
				into	dt_primeiro_horario_w
				from	dual;
				exception when others then
					dt_primeiro_horario_w	:= dt_prescricao_w + (qt_minutos_adic_w/1440);
				end;				
				dt_primeiro_horario_w	:= dt_primeiro_horario_w + dividir((nvl(qt_hora_aplicacao_w,0) * 60) + nvl(qt_min_aplicacao_w,0),1440);
				hr_prim_horario_ant_w	:= to_char(dt_primeiro_horario_w,'hh24:mi');
			end if;
			
			if	(nr_agrupamento_w <> nr_agrupamento_ant_w) then
				nr_ocorrencia_w := 0;
				calcular_horario_prescricao(NR_PRESCRICAO_p, cd_intervalo_ww,nvl(dt_primeiro_horario_w,dt_prescricao_w),dt_primeiro_horario_w, 24, cd_material_w, 
							0,0,nr_ocorrencia_w, ds_horarios_w,ds_horarios2_w, 'N', null);

				ds_horarios_w		:= ds_horarios_w || ds_horarios2_w;
				ds_prim_horario_w	:= to_char(dt_primeiro_horario_w,'hh24:mi');
				
				update	prescr_material
				set	ds_horarios	= ds_horarios_w,
					hr_prim_horario	= ds_prim_horario_w,
					nr_ocorrencia	= nr_ocorrencia_w
				where	nr_prescricao	= NR_PRESCRICAO_p
				and	nr_agrupamento	= nr_agrupamento_p
				and	nr_sequencia	= nr_sequencia_w;
				
				--dt_primeiro_horario_w	:= dt_primeiro_horario_w + dividir((nvl(qt_hora_aplicacao_w,0) * 60) + nvl(qt_min_aplicacao_w,0),1440);
				
			else
				update	prescr_material
				set	ds_horarios	= ds_horarios_w,
					hr_prim_horario	= ds_prim_horario_w,
					nr_ocorrencia	= nr_ocorrencia_w
				where	nr_prescricao	= nr_prescricao_p
				and	nr_agrupamento	= nr_agrupamento_p
				and	nr_sequencia	= nr_sequencia_w;
			end if;
			
			nr_agrupamento_ant_w	:= nr_agrupamento_w;
			
			end;
		end loop;
		close c02;
		
/*		select	max(ds_horarios)
		into	ds_horarios_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_menor_horario_w;
		
		update	prescr_material
		set	ds_horarios	= ds_horarios_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_prescricao	= nr_prescricao_p
		and	nr_agrupamento	= nr_agrupamento_p
		and	nr_sequencia	<> nr_seq_menor_horario_w
		and	obter_classif_material_proced(cd_material, null,null) <> 1
		and	nr_sequencia_solucao	is null
		and	nr_sequencia_proc	is null
		and	ie_agrupador		= 1
		and	ie_suspenso		<> 'S'
		and	nvl(ie_administrar,'S')	= 'S'
		and	nr_sequencia_diluicao	is null;
*/

	end if;

elsif (ie_farm_enf_p	= 'E') then

	if	(ie_volta_horario_composto_p	= 'S') then --Voltar todos os itens ao hor�rio original.
		nr_seq_menor_horario_w	:= nr_sequencia_p;
	end if;

	if	(nvl(nr_seq_menor_horario_w,0) <> 0) then
		select	max(ds_horarios)
		into	ds_horarios_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_menor_horario_w;
		
		if	(nvl(ds_horario_composto_p,'X') <> 'X') then  --Voltar todos os itens ao hor�rio original.
			ds_horarios_w	:= ds_horario_composto_p;
		end if;
		
		update	prescr_material
		set	ds_horarios	= ds_horarios_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_prescricao	= nr_prescricao_p
		and	(((ie_volta_horario_composto_p = 'S') and (nr_agrupamento = nr_agrup_ant_p)) or
			 (nr_sequencia		= nr_sequencia_p))
		and	nr_sequencia		<> nr_seq_menor_horario_w
		and	obter_classif_material_proced(cd_material, null,null) <> 1
		and	nr_sequencia_solucao	is null
		and	nr_sequencia_proc	is null
		and	ie_agrupador		= 1
		and	ie_suspenso		<> 'S'
		and	nvl(ie_administrar,'S')	= 'S'
		and	nr_sequencia_diluicao	is null;

		ds_horarios_p	:= ds_horarios_w;
		
		commit;
	end if;
end if;

end Atualizar_horarios_agrup_medic;
/
