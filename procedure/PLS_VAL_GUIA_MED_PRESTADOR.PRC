create or replace
procedure pls_val_guia_med_prestador(	ie_insere_p	out		varchar2,
					ie_opcao_p			varchar2,
					nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
					ie_instituicao_acred_p		varchar2,
					ie_nivel_acreditacao_p		varchar2,
					ie_odontologico_p		varchar2,
					ie_filtro_carater_int_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: VALIDAR CONDI��ES DE PRESTADOR
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

	UTILIZAR O IE_OPCAO_P
	"IA" - Valida��o de institui��o de acredita��o
	"NA" - Valida��o de n�vel de acredita��o
	"TO" - Valida��o de tipo odontol�gico
	"CI" - Valida��o de carater de interna��o (Atendimento)
	
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ie_instituicao_acred_ptu_w	pls_prestador.ie_instituicao_acred_ptu%type;
ie_nivel_acreditacao_ptu_w	pls_prestador.ie_nivel_acreditacao_ptu%type;
qt_registro_w			pls_integer := 0;

begin
if	(ie_opcao_p = 'IA') and
	(ie_instituicao_acred_p is not null) then
	ie_insere_p := 'S';
	
	select	max(ie_instituicao_acred_ptu)
	into	ie_instituicao_acred_ptu_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
	
	if	(ie_instituicao_acred_p <> ie_instituicao_acred_ptu_w) or
		(ie_instituicao_acred_ptu_w is null) then
		ie_insere_p := 'N';
	end if;

elsif	(ie_opcao_p = 'NA') and
	(ie_nivel_acreditacao_p is not null) then
	ie_insere_p := 'S';

	select	max(ie_nivel_acreditacao_ptu)
	into	ie_nivel_acreditacao_ptu_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_p;
	
	if	(ie_nivel_acreditacao_p <> ie_nivel_acreditacao_ptu_w) or
		(ie_nivel_acreditacao_ptu_w is null) then
		ie_insere_p := 'N';
	end if;
	
elsif	(ie_opcao_p = 'TO') and
	(ie_odontologico_p is not null) then
	ie_insere_p := 'S';
	
	-- Tipos adicionais , tipo prestadores, ie_odontologico_p = 'S' apenas  odontol�gicos, 'N' n�o odontol�gicos,  'T' todos tipos 
	if	(ie_odontologico_p <> 'T') then
		select	count(1)
		into	qt_registro_w
		from	pls_tipo_prestador y,
			pls_prestador x
		where	x.nr_seq_tipo_prestador = y.nr_sequencia(+)
		and	x.nr_sequencia		= nr_seq_prestador_p
		and	y.ie_odontologico	= ie_odontologico_p;
		
		if	(qt_registro_w = 0) then
			select	count(1)
			into	qt_registro_w
			from	pls_tipo_prestador y,
				pls_prestador_tipo x
			where	x.nr_seq_tipo		= y.nr_sequencia
			and	x.nr_seq_prestador	= nr_seq_prestador_p
			and	y.ie_odontologico	= ie_odontologico_p;
		end if;
		
		if	(qt_registro_w = 0) then
			ie_insere_p := 'N';
		end if;
	
	end if;	

elsif	(ie_opcao_p = 'CI') and
	(ie_filtro_carater_int_p is not null) then
	ie_insere_p := 'S';
	
	if	(ie_filtro_carater_int_p <> '0') then
		select	count(1)
		into	qt_registro_w
		from	pls_prestador_car_int	z
		where	z.nr_seq_prestador	= nr_seq_prestador_p
		and	z.ie_carater_internacao	= ie_filtro_carater_int_p;
		
		if	(qt_registro_w = 0) then
			ie_insere_p := 'N';
		end if;
	end if;
end if;

end pls_val_guia_med_prestador;
/