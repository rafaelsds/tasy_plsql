create or replace
procedure ica_importar_medico is 

cd_pessoa_fisica_w			varchar2(10);
cd_especialidade_w			number(5);
cd_nacionalidade_w			varchar2(8);
cd_municipio_ibge_w			varchar2(6);
ds_erro_w				varchar2(500);
nm_pessoa_fisica_sem_acento_w		varchar2(60);
nm_pessoa_pesquisa_w		varchar2(60);
nr_seq_categoria_w			number(10);
nr_seq_conselho_w			number(10);
qt_commit_w			number(5);
qt_registro_w			number(5);

cursor c01 is
select	substr(a.crm,1,20) crm,
	substr(a.nome,1,60) nome,
	a.situacao,
	a.municipio,
	upper(a.uf) uf,
	a.conselho,
	a.naciolidad,
	a.categoria,
	a.vinculo
from	w_ica_medico a;

vet01 C01%RowType;

cursor c02 is
select	a.especialid
from	w_ica_medico_especialidade a
where	a.crm	= vet01.crm;

vet02	C02%RowType;

begin
begin
qt_commit_w	:= 0;
wheb_usuario_pck.set_ie_executar_trigger('N');

	open C01;
	loop
	fetch C01 into	
		vet01;
	exit when C01%notfound;
		begin
				
		select	count(*)
		into	qt_registro_w
		from	medico
		where	nr_crm	= vet01.crm;
		
		if	(qt_registro_w = 0) then
		
			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_fisica_w
			from	dual;
			
			nm_pessoa_fisica_sem_acento_w	:= substr(elimina_acentuacao(vet01.nome),1,60);
			nm_pessoa_pesquisa_w	:= substr(padronizar_nome(vet01.nome),1,60);
			
			select	max(a.cd_nacionalidade)
			into	cd_nacionalidade_w
			from	nacionalidade a
			where	upper(vet01.naciolidad) = upper(a.ds_nacionalidade);
			
			select	max(a.nr_sequencia)
			into	nr_seq_conselho_w
			from	conselho_profissional a
			where	a.nr_sequencia	= vet01.conselho;
			
			begin
			insert into pessoa_fisica (	
				cd_pessoa_fisica,
				ie_tipo_pessoa,
				nm_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				nm_pessoa_fisica_sem_acento,
				nm_pessoa_pesquisa,
				cd_nacionalidade,
				nr_seq_conselho,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	cd_pessoa_fisica_w,
				2,
				vet01.nome,
				sysdate,
				'Importacao',
				nm_pessoa_fisica_sem_acento_w,
				nm_pessoa_pesquisa_w,
				cd_nacionalidade_w,
				nr_seq_conselho_w,
				sysdate,
				'Importacao');
			exception when others then
				
				ds_erro_w	:= substr('Pessoa Fisica '||sqlerrm(sqlcode),1,500);
				insert into log_tasy (	
					dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values(	sysdate,
					'Importacao',
					1209,
					ds_erro_w);
			end;	
					
			
			select	max(cd_municipio_ibge)
			into	cd_municipio_ibge_w
			from	sus_municipio
			where	upper(ds_municipio) = upper(vet01.municipio);
					
			begin
			insert into compl_pessoa_fisica (	
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_municipio,
				cd_municipio_ibge)
			values(	cd_pessoa_fisica_w,
				1,
				2,
				sysdate,
				'Importacao',
				sysdate,
				'Importacao',
				vet01.municipio,
				cd_municipio_ibge_w);
			exception when others then
				
				ds_erro_w	:= substr('Compl Pessoa fisica '||sqlerrm(sqlcode),1,500);
				insert into log_tasy (	
					dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values(	sysdate,
					'Importacao',
					1209,
					ds_erro_w);
			end;	
			
			select	max(a.nr_sequencia)
			into	nr_seq_categoria_w
			from	medico_categoria a
			where	upper(a.ds_categoria) = upper(vet01.categoria);
			
			begin
			insert into medico (	
				cd_pessoa_fisica,
				nr_crm,
				nm_guerra,
				ie_vinculo_medico,
				dt_atualizacao,
				nm_usuario,
				uf_crm,
				nr_seq_categoria,
				ie_situacao,
				ie_corpo_clinico,
				ie_corpo_assist,
				nm_usuario_nrec,
				dt_atualizacao_nrec)
			values(	cd_pessoa_fisica_w,
				vet01.crm,
				vet01.nome,
				14,
				sysdate,
				'Importacao',
				vet01.uf,
				nr_seq_categoria_w,
				vet01.situacao,
				'N',
				'N',
				'Importacao',
				sysdate);
			exception when others then
				
				ds_erro_w	:= substr('Medico '||sqlerrm(sqlcode),1,500);
				insert into log_tasy (	
					dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values(	sysdate,
					'Importacao',
					1209,
					ds_erro_w);
			end;
			
			if	(ds_erro_w is null) then
				open C02;
				loop
				fetch C02 into	
					vet02;
				exit when C02%notfound;
					begin
					
					select	nvl(max(a.cd_especialidade),0)
					into	cd_especialidade_w
					from	especialidade_medica a
					where	upper(a.ds_especialidade) = upper(vet02.especialid);
					
					if	(cd_especialidade_w <> 0) then
						begin
						insert into medico_especialidade (	
							cd_pessoa_fisica,
							cd_especialidade,
							dt_atualizacao,
							nm_usuario,
							nr_seq_prioridade)
						values(	cd_pessoa_fisica_w,
							cd_especialidade_w,
							sysdate,
							'Importacao',
							100);
						exception when others then
							
							ds_erro_w	:= substr('Medico especialidade '||sqlerrm(sqlcode),1,500);
							insert into log_tasy (	
								dt_atualizacao,
								nm_usuario,
								cd_log,
								ds_log)
							values(	sysdate,
								'Importacao',
								1209,
								ds_erro_w);
						end;	
					end if;
					
					end;
				end loop;
				close C02;
			end if;
		end if;
		if	(qt_commit_w >= 20) then
			
			qt_commit_w	:= 0;	
			commit;
		else
			qt_commit_w	:= qt_commit_w + 1;
		end if;
		end;
	end loop;
	close C01;
exception when others then
	wheb_usuario_pck.set_ie_executar_trigger('S');	
end;
wheb_usuario_pck.set_ie_executar_trigger('S');
commit;
end ica_importar_medico;
/