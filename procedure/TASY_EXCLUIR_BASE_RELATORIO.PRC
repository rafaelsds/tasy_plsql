CREATE OR REPLACE
PROCEDURE 	Tasy_Excluir_Base_Relatorio(ds_regra_p 	Varchar2)  is

ds_comando_w			Varchar2(2000);
vl_retorno_w			varchar2(255);

BEGIN

if	(trim(upper(ds_regra_p)) = 'TRUNCATE') then
	abortar_se_gerando_versao;
end if;

ds_comando_w	:= ds_regra_p || ' table w_banda_relat_variavel ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_relatorio_documentacao ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_usuario_relatorio ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_funcao_regra_relat ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_etiqueta ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_relatorio_funcao ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_relatorio_parametro ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p ||  ' table w_banda_relat_Campo ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p ||  ' table w_banda_relat_campo_longo ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_banda_relatorio ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);
ds_comando_w	:= ds_regra_p || ' table w_relatorio ';
Exec_sql_Dinamico('drop Relatorio', ds_comando_w);

END Tasy_Excluir_Base_Relatorio;
/
