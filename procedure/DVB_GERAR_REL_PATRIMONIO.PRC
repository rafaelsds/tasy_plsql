create or replace 
procedure dvb_gerar_rel_patrimonio (nr_seq_grupo_empresa_p number,
                                    dt_referencia_p date,
                                    vl_minimo_p number,
                                    nm_usuario_p varchar2 ) is

qt_meses_depreciados_w number(10);

cursor  c01 is
select  a.nr_sequencia,
        b.cd_empresa,
        obter_nome_empresa(b.cd_empresa) nm_razao_social,
        a.cd_estabelecimento,
        obter_nome_estabelecimento(a.cd_estabelecimento) nm_fantasia_estab,
        a.nr_seq_regra_conta,
        obter_desc_conta_contabil(f.cd_conta_contabil) ds_conta_contabil,
        a.tx_deprec,
        a.ds_bem,
        a.cd_bem,
        substr(obter_descricao_padrao_pk('PAT_TIPO_BEM','DS_EXTERNO','NR_SEQUENCIA',a.nr_seq_tipo),1,255) ds_externo,
        a.ie_situacao,
        a.dt_aquisicao,
        a.vl_original,
        null qt_bem,
        null ds_unidade,
        a.cd_cgc,
        a.nr_nota_fiscal,
        a.cd_centro_custo,
        c.vl_deprec_acum,
        a.qt_tempo_vida_util,
        null qt_meses_depreciados,
        null qt_vida_util_remanescente_br,
        null qt_vida_util_remanescente_usa,
        c.vl_deprec_mes,
        c.vl_deprec_mes_fis
from    pat_bem a,
        pat_valor_bem c,
        estabelecimento b,
        pat_conta_contabil f,
        grupo_empresa_v d
where   a.cd_estabelecimento = b.cd_estabelecimento
and     a.nr_seq_regra_conta = f.nr_sequencia
and     a.nr_sequencia = c.nr_seq_bem
and     d.cd_empresa = b.cd_empresa
and     d.nr_seq_grupo_empresa = nr_seq_grupo_empresa_p
and     c.dt_valor = dt_referencia_p
and 	((vl_minimo_p = 0) or a.vl_original >= vl_minimo_p);

c01_w  c01%rowtype;

begin

delete
from    w_dvb_rel_patrimonio
where   nm_usuario = nm_usuario_p;

open c01;
loop
fetch c01 into
  c01_w;
exit when c01%notfound;
  begin
  
        select  count(1)
        into    qt_meses_depreciados_w
        from    pat_valor_bem 
        where   nr_seq_bem = c01_w.nr_sequencia and dt_valor <= dt_referencia_p;
        
        insert into w_dvb_rel_patrimonio (
                cd_empresa, --1
                nm_razao_social, --2
                cd_estabelecimento, --3
                nm_fantasia_estab, --4
                nr_seq_regra_conta, --5
                ds_conta_contabil, --6
                tx_deprec, --7
                ds_bem, --8
                cd_bem, --9
                ds_externo, --10
                ie_situacao, --11
                dt_aquisicao, --12
                vl_original, --13
                qt_bem, --14
                ds_unidade, --15
                cd_cgc, --16
                nr_nota_fiscal, --17
                cd_centro_custo, --18
                vl_deprec_acum, --19
                qt_tempo_vida_util, --20
                qt_meses_depreciados, --21
                qt_vida_util_remanescente, --22
                qt_vida_util_remanescente_usa, --23
                vl_deprec_mes, --24
                vl_deprec_mes_fis, --25
                nm_usuario
        ) values (
                c01_w.cd_empresa, --1
                c01_w.nm_razao_social, --2
                c01_w.cd_estabelecimento, --3
                c01_w.nm_fantasia_estab, --4
                c01_w.nr_seq_regra_conta, --5
                c01_w.ds_conta_contabil, --6
                c01_w.tx_deprec, --7
                c01_w.ds_bem, --8
                c01_w.cd_bem, --9
                c01_w.ds_externo, --10
                c01_w.ie_situacao, --11
                c01_w.dt_aquisicao, --12
                c01_w.vl_original, --13
                c01_w.qt_bem, --14
                c01_w.ds_unidade, --15
                c01_w.cd_cgc, --16
                c01_w.nr_nota_fiscal, --17
                c01_w.cd_centro_custo, --18
                c01_w.vl_deprec_acum, --19
                c01_w.qt_tempo_vida_util, --20
                qt_meses_depreciados_w, --21
                c01_w.qt_tempo_vida_util - qt_meses_depreciados_w, --22
                null, --23
                c01_w.vl_deprec_mes, --24
                c01_w.vl_deprec_mes_fis, --25
                nm_usuario_p
        );
  end;
end loop;
close c01;

/*
create table "TASY"."W_DVB_REL_PATRIMONIO" (
        "CD_EMPRESA"                      number(4, 0),
        "NM_RAZAO_SOCIAL"                 varchar2(80 byte),
        "CD_ESTABELECIMENTO"              number(4, 0),
        "NM_FANTASIA_ESTAB"               varchar2(50 byte),
        "NR_SEQ_REGRA_CONTA"              number(10, 0),
        "DS_CONTA_CONTABIL"               varchar2(255 byte),
        "CD_CONTA_CONTABIL"               varchar2(20 byte),
        "TX_DEPREC"                       number(15, 4),
        "CD_BEM"                          varchar2(20 byte),
        "IE_SITUACAO"                     varchar2(1 byte),
        "DT_AQUISICAO"                    date,
        "VL_ORIGINAL"                     number(15, 2),
        "CD_CGC"                          varchar2(14 byte),
        "NR_NOTA_FISCAL"                  varchar2(255 byte),
        "CD_CENTRO_CUSTO"                 number(8, 0),
        "VL_DEPREC_ACUM"                  number(22, 4),
        "QT_TEMPO_VIDA_UTIL"              number(10, 0),
        "VL_DEPREC_MES"                   number(22, 4),
        "NM_USUARIO"                      varchar2(20 byte),
        "QT_BEM"                          number(10, 0),
        "DS_UNIDADE"                      varchar2(10 byte),
        "QT_MESES_DEPRECIADOS"            number(10, 0),
        "VL_DEPREC_MES_FIS"               number(22, 4),
        "QT_VIDA_UTIL_REMANESCENTE_USA"   number(10, 0),
        "QT_VIDA_UTIL_REMANESCENTE"       number(10, 0),
        "DS_EXTERNO"                      varchar2(255 byte),
        "DS_BEM"                          varchar2(255 byte)
)
*/
  
end dvb_gerar_rel_patrimonio;
/