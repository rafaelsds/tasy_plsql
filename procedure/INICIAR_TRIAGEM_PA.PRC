create or replace
procedure Iniciar_Triagem_PA   (  nr_seq_triagem_p  number,
                  nm_usuario_p    varchar2,
                  cd_estabelecimento_p    number) is 
        
ds_erro_w    varchar2(255);

begin

if (nr_seq_triagem_p is not null ) then

  update  triagem_pronto_atend
  set    dt_inicio_triagem  =  sysdate
  where  nr_sequencia    =  nr_seq_triagem_p;


  begin    
    gerar_log_triagem_pa( null,'I',nm_usuario_p, NULL, 'N', NULL, nr_seq_triagem_p);    
  exception
  when others then
    ds_erro_w  := substr(sqlerrm,1,255);
  end;

end if;

commit;

end Iniciar_Triagem_PA;
/
