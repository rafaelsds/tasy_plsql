create or replace
procedure OBTER_HTML_RTF_AGEINT(nr_seq_ageint_p	in	number,
								nm_usuario_p	in	varchar2,
								nr_seq_html_p	out number) is 

begin

delete 	W_AGENDA_ENVIO_EMAIL 
where 	nr_sequencia = nr_seq_ageint_p 
and 	dt_atualizacao < (	select 	max(dt_atualizacao) 
							from 	W_AGENDA_ENVIO_EMAIL 
							where 	nr_sequencia = nr_seq_ageint_p);
commit;

CONVERTE_RTF_HTML('Select ds_conteudo from W_AGENDA_ENVIO_EMAIL where nr_sequencia = :nr', nr_seq_ageint_p, nm_usuario_p, nr_seq_html_p);

commit;

end OBTER_HTML_RTF_AGEINT;
/
