create or replace
procedure inserir_contas_protocolo(nr_protocolo_p		varchar2,
			nr_seq_protocolo_p	number,
			nr_interno_conta_p	number,
			nm_usuario_p		Varchar2) is 

begin

update	conta_paciente 
set    	dt_atualizacao = sysdate ,
	nm_usuario =  nm_usuario_p,
	nr_protocolo = nr_protocolo_p,
	nr_seq_protocolo = nr_seq_protocolo_p
where  	nr_interno_conta = nr_interno_conta_p;

commit;

end inserir_contas_protocolo;
/