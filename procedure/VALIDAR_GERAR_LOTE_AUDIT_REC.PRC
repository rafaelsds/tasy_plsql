create or replace
procedure VALIDAR_GERAR_LOTE_AUDIT_REC
			(nr_seq_lote_rec_p	number,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number)
			is
			
VarPermiteContasSemDtAnalise	varchar2(10);
VarPermiteReceb			varchar2(10);
dt_item_receb_w			date;	
dt_item_indev_w			date;		
			
begin

obter_param_usuario(66,12,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,VarPermiteContasSemDtAnalise);
obter_param_usuario(66,5,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,VarPermiteReceb);

select	max(b.dt_historico)
into	dt_item_receb_w
from	conta_paciente d,
	hist_audit_conta_paciente c,
	conta_paciente_ret_hist b,
	conta_paciente_retorno a,
	lote_audit_recurso e
where	a.nr_sequencia		= b.nr_seq_conpaci_ret
and	d.nr_interno_conta	= a.nr_interno_conta
and	a.nr_interno_conta	= d.nr_interno_conta
and	b.nr_seq_hist_audit	= c.nr_sequencia
and	e.nr_sequencia		= nr_seq_lote_rec_p
and	a.cd_convenio		= e.cd_convenio
and	d.cd_estabelecimento 	= cd_estabelecimento_p
and	((b.nm_usuario_resp 	= e.nm_usuario_resp) or (e.nm_usuario_resp is null))
and	b.dt_historico 		between e.dt_inicial and e.dt_final
and	b.nr_seq_lote_recurso 	is null
and	c.ie_acao   = 1
and	(VarPermiteContasSemDtAnalise = 'S' or a.dt_analise is not null)
and	c.ie_acao 		= nvl(e.ie_acao_historico, c.ie_acao)
and	(exists	(select	1
		from	convenio_retorno_item z
		where	z.nr_sequencia    	= b.nr_seq_ret_item 
		and	z.nr_seq_retorno	= e.nr_seq_ret_filtro) 
	or nvl(e.nr_seq_ret_filtro,0) 	= 0);
	
select	max(b.dt_historico)
into	dt_item_indev_w
from	conta_paciente d,
	hist_audit_conta_paciente c,
	conta_paciente_ret_hist b,
	conta_paciente_retorno a,
	lote_audit_recurso e
where	a.nr_sequencia		= b.nr_seq_conpaci_ret
and	d.nr_interno_conta	= a.nr_interno_conta
and	a.nr_interno_conta	= d.nr_interno_conta
and	b.nr_seq_hist_audit	= c.nr_sequencia
and	e.nr_sequencia		= nr_seq_lote_rec_p
and	a.cd_convenio		= e.cd_convenio
and	d.cd_estabelecimento 	= cd_estabelecimento_p
and	((b.nm_usuario_resp 	= e.nm_usuario_resp) or (e.nm_usuario_resp is null))
and	b.dt_historico 		between e.dt_inicial and e.dt_final
and	b.nr_seq_lote_recurso 	is null
and	c.ie_acao   = 4
and	(VarPermiteContasSemDtAnalise = 'S' or a.dt_analise is not null)
and	c.ie_acao 		= nvl(e.ie_acao_historico, c.ie_acao)
and	(exists	(select	1
		from	convenio_retorno_item z
		where	z.nr_sequencia    	= b.nr_seq_ret_item 
		and	z.nr_seq_retorno	= e.nr_seq_ret_filtro) 
	or nvl(e.nr_seq_ret_filtro,0) 	= 0);
	
if	(dt_item_receb_w is not null and dt_item_indev_w is not null) and
	(VarPermiteReceb = 'N') and
	(dt_item_receb_w > dt_item_indev_w) then
	--Existem �tens de recebimento lan�ados ap�s a glosa indevida. 
	--N�o � poss�vel gerar o lote de recurso!
	wheb_mensagem_pck.exibir_mensagem_abort(209942);
end if;

end VALIDAR_GERAR_LOTE_AUDIT_REC;
/