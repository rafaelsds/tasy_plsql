create or replace
procedure gerar_auto_regra_seq_js(	nr_seq_gestao_p		number,
				nm_usuario_p		varchar2,
				cd_perfil_p		number,
				cd_estabelecimento_p	number,
				nr_sequencia_p		out number) is 

vl_parametro_w		varchar2(10) := '';
nr_sequencia_w		number(15,0) := 0;
					
begin

if	(nr_seq_gestao_p is not null)then
	begin
	
	gerar_autor_regra(null, null, null, null, null, null, 'GVE', nm_usuario_p, null, null, nr_seq_gestao_p, null, null, null,'','','');
	
	obter_param_usuario(1002, 51, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);
	
	if	(vl_parametro_w = 'S')then
		begin
		
		select 	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from 	autorizacao_convenio 
		where 	nr_seq_gestao = nr_seq_gestao_p;
		
		end;
	end if;
	
	end;
end if;

nr_sequencia_p	:= nr_sequencia_w;

commit;

end gerar_auto_regra_seq_js;
/
