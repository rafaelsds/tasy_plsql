create or replace
procedure tiss_gerar_reap(	nr_seq_reap_lote_p		in	number,
			nm_usuario_p		in	varchar2) is

nr_seq_conta_w			number(15);

cursor	c01 is
select	nvl(nr_sequencia,0)
from	tiss_reap_conta
where	nr_seq_lote		= nr_seq_reap_lote_p;

begin

open c01;
loop
fetch c01 into
	nr_seq_conta_w;
exit when c01%notfound;
	
	delete
	from	tiss_conta_proc
	where	nr_seq_reap_conta	= nr_seq_conta_w;
	
	delete
	from	tiss_conta_atend
	where	nr_seq_reap_conta	= nr_seq_conta_w;
	
	delete
	from	tiss_conta_guia
	where	nr_seq_reap_conta	= nr_seq_conta_w;
	
	delete
	from	tiss_conta_opm_exec
	where	nr_seq_reap_conta	= nr_seq_conta_w;
	
	delete
	from	tiss_conta_desp
	where	nr_seq_reap_conta	= nr_seq_conta_w;
	
	tiss_reap_atualizar_desp(nr_seq_conta_w, nm_usuario_p);	
	tiss_gerar_reap_atend(nr_seq_conta_w, nm_usuario_p);
	tiss_gerar_reap_proc(nr_seq_conta_w, nm_usuario_p);
	tiss_gerar_reap_guia(nr_seq_conta_w, nm_usuario_p);
	tiss_gerar_reap_opm(nr_seq_conta_w, nm_usuario_p);
	tiss_gerar_reap_desp(nr_seq_conta_w, nm_usuario_p);
	
end loop;
close c01;

commit;

end tiss_gerar_reap;
/