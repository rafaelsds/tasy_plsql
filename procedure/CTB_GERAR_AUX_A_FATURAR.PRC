create or replace
procedure ctb_gerar_aux_a_faturar(	nm_usuario_p		varchar2,
					cd_empresa_p		varchar2,
					cd_estabelecimento_p	varchar2,
					dt_inicial_p		date,
					dt_final_p		date,
					dt_liberacao_p		date,
					ie_gerar_arquivo_p	varchar2,
					nr_seq_regra_p		number) is 

nr_contador_w					number(10) := 0;
ie_tipo_documento_w				varchar2(2);
ds_erro_w					varchar2(255);
ds_local_w					varchar2(255);
arq_texto_w					utl_file.file_type;
nm_arquivo_w					varchar2(255);
ds_observacao_w					varchar2(255);
ds_observacao_item_w				varchar2(255);
dt_final_w					date;
ie_data_rec_faturamento_w			pls_parametro_contabil.ie_data_rec_faturamento%type;
ie_tipo_segurado_w				pls_segurado.ie_tipo_segurado%type;
ie_tipo_compartilhamento_w			pls_segurado_repasse.ie_tipo_compartilhamento%type;
ie_tipo_repasse_w				pls_segurado_repasse.ie_tipo_repasse%type;
dt_repasse_w					pls_segurado_repasse.dt_repasse%type;
dt_fim_repasse_w				pls_segurado_repasse.dt_fim_repasse%type;
dt_ref_repasse_w				pls_conta_proc.dt_procedimento%type;
						
ds_nome_livro_w					varchar2(255);
ds_periodo_w					varchar2(255);						
	
cursor c_linha is
	select	a.nr_documento													|| ';' ||
		a.ds_tipo_documento												|| ';' ||
		a.dt_vencimento													|| ';' ||
		a.dt_cancelamento												|| ';' ||
		a.dt_recebimento												|| ';' ||
		a.cd_beneficiario												|| ';' ||
		substr(obter_valor_dominio(6,pls_obter_se_benef_remido(a.nr_seq_segurado,a.dt_contabilizacao)),1,255)		|| ';' ||
		a.nm_usuario_evento												|| ';' ||
		a.ds_tipo_segurado												|| ';' ||
		a.nm_usuario_princ												|| ';' ||
		a.nm_prestador													|| ';' ||
		a.nr_cpf_cnpj													|| ';' ||
		a.ds_modalidade_contrat												|| ';' ||
		a.ds_tipo_regulamentacao											|| ';' ||
		a.ds_tipo_contratacao												|| ';' ||
		substr(pls_obter_se_corresp_assumida(a.ie_tipo_segurado,a.dt_contratacao),1,255)				|| ';' ||
		substr(pls_obter_se_corresp_assumida(a.ie_tipo_segurado,replace(a.nr_protocolo_ans,chr(039),'')),1,255)		|| ';' ||
		a.dt_inicio_cobertura												|| ';' ||
		a.dt_fim_cobertura												|| ';' ||
		a.ds_ato_cooperado												|| ';' ||
		substr(pls_obter_se_corresp_assumida(a.ie_tipo_segurado,a.nr_contrato),1,255)					|| ';' ||
		substr(pls_obter_se_corresp_assumida(a.ie_tipo_segurado,a.dt_contrato),1,255)					|| ';' ||
		a.vl_pagar													|| ';' ||
		a.cd_conta_contabil												|| ';' ||
		a.cd_conta_contrapartida											|| ';' ||
		a.dt_contabilizacao 												|| ';' ||
		substr(obter_valor_dominio(3384, a.ie_tipo_repasse), 1,255)							|| ';' ||
		substr(obter_valor_dominio(8980, a.ie_tipo_compartilhamento), 1,255)						|| ';' ||
		a.dt_inicio_compartilhamento											|| ';' ||
		a.dt_fim_compartilhamento											|| ';' ||
		a.ds_observacao 				ds_linha
	from	w_ctb_livro_aux_event_liq a
	where	nr_seq_reg_auxiliar = nr_seq_regra_p
	order by to_number(nr_documento);

						
Cursor c_conta is						
	select	v.cd_beneficiario,
		v.nm_beneficiario,
		v.ie_preco,
		v.ie_regulamentacao,
		v.ds_tipo_contratacao,
		v.ds_segmentacao,
		v.ie_ato_cooperado,
		v.vl_provisao,
		v.cd_conta_deb_provisao,
		v.cd_conta_cred_provisao,
		v.dt_mes_competencia,
		v.nr_seq_conta nr_documento,
		v.nr_contrato,
		v.nm_prestador,
		v.nr_cpf_cnpj,
		v.ie_tipo_contratacao ie_tipo_contratacao,
		v.nr_seq_segurado,
		v.nr_seq_prestador,
		v.nm_pagador,
		v.nr_seq_pagador,
		v.ie_tipo_segurado,
		v.nr_protocolo_ans,
		v.dt_contrato,
		v.dt_contratacao,
		v.dt_inicio_cobertura,
		v.dt_fim_cobertura,
		null dt_vencimento,
		null dt_recebimento,
		null dt_cancelamento,
		v.dt_atendimento_referencia dt_ref_repasse,
		v.nr_seq_congenere
	from	pls_conta_pos_dados_contab_v 	v
	where	v.ie_tipo_movto	in ('VLB','X')
	and	v.dt_mes_competencia <= dt_final_w
	and	nvl(v.nr_seq_lote_fat,0) = 0
	and	nvl(v.vl_provisao,0) <> 0
	and	((nvl(ie_tipo_segurado_w,'X') = nvl(v.ie_tipo_segurado,'X'))
	or	(nvl(ie_tipo_segurado_w,'X') = 'X'))	
	union all
	select	substr(pls_obter_dados_segurado(s.nr_sequencia,'CR'),1,30) cd_beneficiario,
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_beneficiario,
		n.ie_preco,
		n.ie_regulamentacao,
		substr(obter_valor_dominio(1666, n.ie_tipo_contratacao),1,255) ds_tipo_contratacao,
		substr(obter_valor_dominio(1665, n.ie_segmentacao),1,255) ds_segmentacao,
		(	select	b.ie_ato_cooperado
			from	pls_conta_proc b
			where	b.nr_seq_conta_proc = b.nr_sequencia
			and	b.nr_seq_conta = a.nr_sequencia
			union all
			select	b.ie_ato_cooperado
			from	pls_conta_mat b
			where	b.nr_seq_conta_mat = b.nr_sequencia
			and	b.nr_seq_conta = a.nr_sequencia) ie_ato_cooperado,
		c.vl_provisao vl_provisao,
		c.cd_conta_deb_provisao cd_conta_deb_provisao,
		c.cd_conta_cred_provisao cd_conta_cred_provisao,
		p.dt_mes_competencia dt_mes_competencia,
		a.nr_sequencia nr_documento,
		r.nr_contrato,
		substr(decode(p.ie_tipo_protocolo,'I',pls_obter_nome_congenere(p.nr_seq_congenere),pls_obter_dados_prestador(nvl(a.nr_seq_prestador,a.nr_seq_prestador_exec),'N')),1,255) nm_prestador,
		substr(decode(p.ie_tipo_protocolo,'I',pls_obter_cnpj_congenere(p.nr_seq_congenere),nvl(pls_obter_dados_prestador(nvl(a.nr_seq_prestador,a.nr_seq_prestador_exec),'CGC'),
		pls_obter_dados_prestador(nvl(a.nr_seq_prestador,a.nr_seq_prestador_exec),'CPF'))),1,14) nr_cpf_cnpj,
		n.ie_tipo_contratacao ie_tipo_contratacao,
		s.nr_sequencia nr_seq_segurado,
		nvl(a.nr_seq_prestador,a.nr_seq_prestador_exec) nr_seq_prestador,
		substr(pls_obter_dados_pagador(s.nr_seq_pagador,'N'),1,255) nm_pagador,
		s.nr_seq_pagador,
		a.ie_tipo_segurado,
		decode(n.ie_tipo_operacao, 'A', 'SCA', nvl(n.nr_protocolo_ans, n.cd_scpa)) nr_protocolo_ans,
		r.dt_contrato,
		s.dt_contratacao,
		s.dt_rescisao dt_inicio_cobertura,
		(s.dt_rescisao + 30) dt_fim_cobertura,
		null dt_vencimento,
		null dt_recebimento,
		null dt_cancelamento,
		null dt_ref_repasse,
		p.nr_seq_congenere
	from	pls_conta 			a,
		pls_conta_coparticipacao	b,
		pls_conta_copartic_contab	c,
		pls_plano			d,
		pls_protocolo_conta 		p,
		pls_segurado			s,
		pls_plano			n,
		pls_contrato			r
	where	a.nr_sequencia			= b.nr_seq_conta
	and	b.nr_sequencia			= c.nr_seq_conta_copartic
	and	a.nr_sequencia			= b.nr_seq_conta
	and	p.nr_sequencia			= a.nr_seq_protocolo
	and	d.nr_sequencia			= a.nr_seq_plano
	and	s.nr_sequencia		 	= a.nr_seq_segurado
	and	n.nr_sequencia		 	= a.nr_seq_plano
	and	r.nr_sequencia			= s.nr_seq_contrato
	and	p.ie_tipo_protocolo		in ('C','I')
	and	b.ie_status_coparticipacao	in ('D','S')
	and 	b.ie_status_mensalidade		<> 'C'
	and	b.nr_seq_mensalidade_seg	is null
	and	((nvl(ie_tipo_segurado_w,'X') 	= nvl(a.ie_tipo_segurado,'X'))
	or	(nvl(ie_tipo_segurado_w,'X') 	= 'X'))	
	and	1 = 2 
	and	a.ie_status			= 'F'
	and	c.dt_mes_competencia <= dt_final_w
	and	p.dt_mes_competencia <= dt_final_w
	and	nvl(c.vl_coparticipacao,0)		> 0
	order by nr_documento;

vet_conta c_conta%rowtype;

begin

wheb_usuario_pck.set_ie_executar_trigger('N');

ie_tipo_documento_w	:= 'CM';

dt_final_w := fim_mes(dt_final_p);

select	max(ie_tipo_segurado)
into	ie_tipo_segurado_w
from	ctb_livro_auxiliar
where	nr_sequencia = nr_seq_regra_p;

select	nvl(max(ie_data_rec_faturamento),'MC')
into	ie_data_rec_faturamento_w
from	pls_parametro_contabil
where	cd_estabelecimento	= cd_estabelecimento_p;

select	substr(obter_desc_expressao(922923),1,255)
into	ds_observacao_w
from	dual;

if	(ie_gerar_arquivo_p = 'N') then
	delete
	from	w_ctb_livro_aux_event_liq a
	where	nr_seq_reg_auxiliar = nr_seq_regra_p;

	commit;

	open c_conta;
	loop
	fetch c_conta into	
		vet_conta;
	exit when c_conta%notfound;
		begin
		nr_contador_w		:= nr_contador_w + 1;
	
		pls_obter_dados_repasse(	vet_conta.dt_ref_repasse,
						vet_conta.nr_seq_segurado,
						vet_conta.nr_seq_congenere,
						ie_tipo_repasse_w,
						ie_tipo_compartilhamento_w,
						dt_repasse_w,
						dt_fim_repasse_w);

		ds_observacao_item_w	:= null;
		
		if	(vet_conta.ie_tipo_segurado in ('C','I','T')) then
			ds_observacao_item_w	:= ds_observacao_w;
		end if;
		
		insert into w_ctb_livro_aux_event_liq (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario, 
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_segurado,
			nr_seq_prestador,
			cd_beneficiario,
			nm_usuario_evento,
			ie_preco,
			ds_modalidade_contrat,
			ie_regulamentacao,
			ds_tipo_regulamentacao,
			ds_tipo_contratacao,
			ds_segmentacao,
			ie_ato_cooperado,
			ds_ato_cooperado,
			vl_pagar,
			cd_conta_contrapartida,
			cd_conta_contabil,
			dt_contabilizacao,
			ie_tipo_livro,
			nr_documento,
			nm_prestador,
			nr_cpf_cnpj,
			nr_contrato,
			nr_seq_reg_auxiliar,
			nr_linha,
			ie_tipo_contratacao,
			ie_tipo_documento,
			ds_tipo_documento,
			nr_seq_pagador,
			nm_usuario_princ,
			ie_tipo_segurado,
			ds_tipo_segurado,
			nr_protocolo_ans,
			dt_contrato,
			dt_contratacao,
			dt_inicio_cobertura,
			dt_fim_cobertura,
			ie_tipo_repasse,
			ie_tipo_compartilhamento,
			dt_inicio_compartilhamento,
			dt_fim_compartilhamento,
			dt_referencia,
			ds_observacao)
		values(	w_ctb_livro_aux_event_liq_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,		
			vet_conta.nr_seq_segurado,
			vet_conta.nr_seq_prestador,
			vet_conta.cd_beneficiario,
			vet_conta.nm_beneficiario,
			vet_conta.ie_preco,
			obter_valor_dominio(1669, vet_conta.ie_preco),
			vet_conta.ie_regulamentacao,
			substr(obter_valor_dominio(2157, vet_conta.ie_regulamentacao),1,255),
			vet_conta.ds_tipo_contratacao,
			vet_conta.ds_segmentacao,
			vet_conta.ie_ato_cooperado,
			substr(obter_valor_dominio(3418,vet_conta.ie_ato_cooperado),1,255),
			vet_conta.vl_provisao,
			vet_conta.cd_conta_cred_provisao,
			vet_conta.cd_conta_deb_provisao,
			vet_conta.dt_mes_competencia,
			6,
			vet_conta.nr_documento,
			vet_conta.nm_prestador,
			vet_conta.nr_cpf_cnpj,
			vet_conta.nr_contrato,
			nr_seq_regra_p,
			nr_contador_w,
			vet_conta.ie_tipo_contratacao,
			ie_tipo_documento_w,
			obter_valor_dominio(8372, ie_tipo_documento_w),
			vet_conta.nr_seq_pagador,
			vet_conta.nm_pagador,
			vet_conta.ie_tipo_segurado,			
			substr(obter_valor_dominio(2406, vet_conta.ie_tipo_segurado),1,255),
			vet_conta.nr_protocolo_ans,
			vet_conta.dt_contrato,
			vet_conta.dt_contratacao,
			vet_conta.dt_inicio_cobertura,
			vet_conta.dt_fim_cobertura,
			ie_tipo_repasse_w,
			ie_tipo_compartilhamento_w,
			dt_repasse_w,
			dt_fim_repasse_w,
			dt_final_w,
			ds_observacao_item_w);
			
		if (mod(nr_contador_w, 1000) = 0) then
			commit;
		end if;
		
		end;
	end loop;
	close c_conta;

	update	ctb_livro_auxiliar
	set	qt_registros	= nr_contador_w
	where	nr_sequencia	= nr_seq_regra_p;

	commit;
	
end if;

if	(ie_gerar_arquivo_p = 'S') then
	begin
	nm_arquivo_w	:= 'RegAuxValoresFaturar' || to_char(sysdate,'ddmmyyyyhh24miss') || nm_usuario_p || '.csv';
	ds_nome_livro_w	:= 'Registro Auxiliar de Valores a Faturar'; 
	ds_periodo_w	:= wheb_mensagem_pck.get_texto(1108452,'DT_FINAL=' || dt_final_w || ';' || 'DT_LIBERACAO=' || dt_liberacao_p);
	
	obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

	arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W', 32000);

	utl_file.put_line(arq_texto_w, ds_nome_livro_w);
	utl_file.put_line(arq_texto_w, ds_periodo_w);
	utl_file.put_line(arq_texto_w, obter_desc_expressao(1028880));
	utl_file.fflush(arq_texto_w);
	for vetl in c_linha loop
		begin
		utl_file.put_line(arq_texto_w,vetl.ds_linha);
		utl_file.fflush(arq_texto_w);
		end;
	end loop;
	end;
end if;		

commit;

wheb_usuario_pck.set_ie_executar_trigger('S');				
						
end ctb_gerar_aux_a_faturar;
/
