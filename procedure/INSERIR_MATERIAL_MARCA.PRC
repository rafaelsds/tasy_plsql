create or replace
procedure inserir_material_marca(		nr_sequencia_p		number,
					cd_material_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

begin

insert into material_marca(
	cd_material,
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_situacao,
	cd_estabelecimento)
values(	cd_material_p,
	nr_sequencia_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	'A',
	cd_estabelecimento_p);

commit;

end inserir_material_marca;
/