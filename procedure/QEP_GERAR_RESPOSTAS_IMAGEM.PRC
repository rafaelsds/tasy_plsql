create or replace
procedure qep_gerar_respostas_imagem(	nr_seq_que_questao_p	number,
					nr_seq_imagem_banco_p	number,
					nm_usuario_p		Varchar2) is

qt_questao_imagem_w	number(10);
begin

select	count(1)
into	qt_questao_imagem_w
from	QUE_QUESTAO
where	nr_sequencia = nr_seq_que_questao_p
and	IE_TIPO_RESPOSTA = 'I'
and	NR_SEQ_IMAGEM_BANCO is not null
and	rownum = 1;

if	(qt_questao_imagem_w > 0) then
	-- Caso a imagem da quest�o for trocada,  e existam registros com �reas da imagem antiga, remove estes registros antes de adicionar as novas imagens.
	delete
	from	QUE_QUESTAO_OPCAO
	where	nr_seq_questao = nr_seq_que_questao_p
	and	(nr_seq_area_imagem is null
	or	nr_seq_area_imagem not in (	select	nr_sequencia
						from	area_imagem
						where	nr_seq_img = nr_seq_imagem_banco_p));

	if	(nr_seq_imagem_banco_p is not null) then
		insert into que_questao_opcao (
			nr_sequencia,
			nr_seq_questao,
			ds_opcao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_situacao,
			nr_seq_area_imagem)
		select	que_questao_opcao_seq.nextval,
			nr_seq_que_questao_p,
			trim(a.ds_titulo),
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'A',
			a.nr_sequencia
		from	area_imagem a
		where	a.nr_seq_img = nr_seq_imagem_banco_p
		and not exists (	select	1
					from	que_questao_opcao x
					where	x.nr_seq_area_imagem = a.nr_sequencia);
	end if;

	commit;
end if;

end qep_gerar_respostas_imagem;
/
