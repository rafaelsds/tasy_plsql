create or replace
procedure pls_inserir_preco_beneficiario
			(	nr_seq_lote_p	in	number,
				ie_commit_p	in	varchar2,
				nm_usuario_p	in	varchar2,
				cd_estabelecimento_p in	number) is

nr_seq_segurado_w		number(10);
nr_seq_reajuste_w		number(10);
qt_idade_w			number(10);
dt_mesano_referencia_w		date;
ie_tipo_contratacao_w		varchar2(3);
nr_seq_contrato_w		number(10);
cont_w				number(15);
dt_inicial_w			date;
dt_final_w			date;
nr_seq_preco_w			number(10);
nr_seq_tabela_w			number(10);
vl_preco_w			number(15,2);
nm_pessoa_fisica_w		varchar2(255);
nr_seq_segurado_preco_w		number(10);
qt_idade_ww			number(10);
qt_sca_w			number(10);
ie_preco_w			varchar2(2);
ie_aplicacao_reajuste_w		varchar2(2);
nr_seq_tabela_sca_w		number(10);
nr_seq_preco_sca_w		number(10);
ds_observacao_w			varchar2(255)	:= null;
nr_seq_sca_w			number(10);
nr_seq_segurado_sca_w		number(10);
nr_seq_vinculo_sca_w		number(10);
nr_seq_intercambio_w		number(10);
ie_tipo_beneficiario_w		varchar2(2);
ie_tipo_segurado_ww		varchar2(2);
nr_seq_contrato_seg_w		number(10);
nr_seq_produto_seg_w		number(10);
nr_seq_seg_preco_w		number(10);
ie_gerar_reajuste_w		varchar2(1);
dt_nascimento_w			date;
nr_seq_processo_w		number(10);
ie_situacao_preco_w		varchar2(1);
nr_seq_processo_reaj_w		number(10);
ie_reajustar_benef_cancelado_w	varchar2(1);
dt_rescisao_contrato_w		date;
nr_seq_reaj_preco_sca_w		number(10);
ie_grau_parentesco_w		varchar2(2);
qt_vidas_w			number(10);
ie_preco_vidas_contrato_w	varchar2(1);
ie_calculo_vidas_w		varchar2(2);
cd_matricula_familia_w		number(10);
nr_seq_titular_w		number(10);
nr_seq_contrato_ww		number(10);
nr_seq_reajuste_ww		number(10);
ie_situacao_reaj_contrato_w	varchar2(10);
ie_tipo_contratacao_benef_w	varchar2(10);
cont_seg_preco_w		number(10);
cont_seg_preco_orig_w		number(10);
nr_contrato_w			number(10);
nr_seq_lote_referencia_w	number(10);
nr_seq_reaj_deflator_w		pls_reajuste.nr_seq_lote_deflator%type;
ie_gerar_reajuste_deflator_w	varchar2(1) := 'S';
nr_seq_reajuste_fx_w		pls_segurado_preco.nr_sequencia%type;
dt_nascimento_ww		pessoa_fisica.dt_nascimento%type;
dt_aniversario_w		date;
vl_preco_inicial_w		pls_plano_preco.vl_preco_inicial%type;
vl_preco_atual_w		pls_plano_preco.vl_preco_atual%type;
vl_preco_nao_subsid_atual_w	pls_plano_preco.vl_preco_nao_subsid_atual%type;
vl_adaptacao_w			pls_plano_preco.vl_adaptacao%type;
vl_minimo_w			pls_plano_preco.vl_minimo%type;
tx_acrescimo_w			pls_plano_preco.tx_acrescimo%type;
tx_desconto_w			number(15,2);
nr_seq_regra_desconto_w		number(10);
vl_desconto_w			number(15,2);
vl_reajustado_w			pls_reajuste_preco.vl_reajustado%type;
vl_preco_atual_ww		pls_segurado_preco.vl_preco_atual%type;
nr_seq_preco_ww			pls_segurado_preco.nr_seq_preco%type;
nr_seq_segurado_preco_ww	pls_segurado_preco.nr_sequencia%type;
tx_deflator_w			pls_reajuste.tx_deflator%type;
nr_seq_tabela_ww		pls_segurado_preco.nr_seq_tabela%type;
qt_idade_reaj_fx_w		pls_segurado_preco.qt_idade%type;
ie_status_w			pls_reajuste.ie_status%type;
nr_seq_fx_segurado_w		number(10);
tx_reajuste_w			pls_reajuste.tx_reajuste%type;
vl_preco_atual_www		number(15,2);
nr_seq_tabela_www		number(10);
nr_seq_seg_preco_alt_tab_w	pls_segurado_preco.nr_sequencia%type;
vl_preco_nao_subsidiado_w	number(15,2);
vl_preco_nao_subsid_desc_ww	number(15,2);
qt_reaj_w			number(10);
qt_processo_w			number(10);
nr_seq_titular_ww		pls_segurado.nr_seq_titular%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

cursor c01 is
	select	b.nr_sequencia,
		a.dt_nascimento,
		b.nr_seq_tabela,
		c.ie_preco,
		b.ie_tipo_segurado,
		b.nr_seq_contrato,
		nvl(substr(pls_obter_garu_dependencia_seg(b.nr_sequencia,'C'),1,2),'X') ie_grau_parentesco
	from	pessoa_fisica	a,
		pls_segurado	b,
		pls_plano	c
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.nr_seq_plano		= c.nr_sequencia
	and	b.dt_liberacao		is not null
	and	b.dt_rescisao		is null
	and	ie_aplicacao_reajuste_w in ('G','C')
	and	((b.ie_tipo_segurado	= ie_tipo_beneficiario_w) or (ie_tipo_beneficiario_w is null))
	and	to_char(a.dt_nascimento, 'mm')	= to_char(dt_mesano_referencia_w, 'mm')
	and	dt_inicial_w	>= trunc(b.dt_contratacao,'Month')
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	--and	((ie_aplicacao_reajuste_w = 'G') or (((ie_aplicacao_reajuste_w = 'C') and (c.ie_tipo_operacao = 'B')) or ((ie_aplicacao_reajuste_w = 'S') and (c.ie_tipo_operacao = 'A'))))
	and	((c.ie_tipo_contratacao = ie_tipo_contratacao_benef_w) or (ie_tipo_contratacao_benef_w is null))
	and	ie_tipo_contratacao_w	= 'F';		-- Cursor dos reajustes de faixa etaria

cursor c02 is
	select  b.nr_sequencia,
		b.nr_seq_tabela,
		a.dt_nascimento,
		d.dt_rescisao_contrato
	from	pessoa_fisica	a,
		pls_segurado	b,
		pls_plano	c,
		pls_contrato	d
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	c.nr_sequencia		= b.nr_seq_plano
	and	d.nr_sequencia		= b.nr_seq_contrato
	and	b.dt_liberacao is not null
	and	c.ie_preco in ('1','4')
	and	c.ie_tipo_contratacao	= 'I'
	and	pls_obter_se_aniv_contrato(b.nr_sequencia, dt_inicial_w, dt_final_w, d.ie_reajuste) = 'S'
	and	ie_tipo_contratacao_w	= 'I'
	and	b.nr_sequencia not in  (	select	x.nr_seq_segurado
						from	pls_segurado_preco x,
							pls_lote_reaj_segurado y
						where	x.nr_seq_lote	= y.nr_sequencia
						and	trunc(dt_mesano_referencia,'month') = trunc(dt_mesano_referencia_w,'month')
						and	x.dt_liberacao is not null
						and	y.ie_tipo_contratacao = 'I');		-- Cursor dos reajustes para planos individual/familiar;

cursor c03 is
	select  b.nr_sequencia,
		b.nr_seq_tabela,
		to_number(obter_idade(a.dt_nascimento,add_months(fim_mes(dt_mesano_referencia_w),-1),'A')),
		substr(a.nm_pessoa_fisica,1,255),
		null,
		nvl(substr(pls_obter_garu_dependencia_seg(b.nr_sequencia,'C'),1,2),'X'),
		b.cd_matricula_familia,
		b.nr_seq_titular,
		a.dt_nascimento
	from	pessoa_fisica	a,
		pls_segurado	b,
		pls_plano	c
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	c.nr_sequencia		= b.nr_seq_plano
	and	((b.nr_seq_contrato	= nr_seq_contrato_w and c.ie_preco in ('1','4')) or (b.nr_seq_intercambio = nr_seq_intercambio_w and b.IE_TIPO_REPASSE = 'P'))
	and	b.dt_liberacao is not null
	and	c.ie_tipo_contratacao	<> 'I'
	and	ie_tipo_contratacao_w	= 'C'
	and	exists (	select	1
				from	pls_lote_reaj_segurado	x,
					pls_reajuste		y,
					pls_reajuste_tabela	z
				where	x.nr_sequencia		= nr_seq_lote_p
				and	x.nr_seq_reajuste	= y.nr_sequencia
				and	z.nr_seq_reajuste	= y.nr_sequencia
				and	z.nr_seq_tabela		= b.nr_seq_tabela
				union all
				select	1
				from	pls_lote_reaj_segurado	x,
					pls_reajuste		y,
					pls_reajuste_tabela	z
				where	x.nr_sequencia		= nr_seq_lote_p
				and	x.nr_seq_reajuste	= y.nr_seq_lote_referencia -- Quando nao e informado o contrato no lote de reajuste
				and	z.nr_seq_reajuste	= y.nr_sequencia
				and	z.nr_seq_tabela		= b.nr_seq_tabela);	-- Cursor dos reajustes para planos coletivos;

Cursor C04 is
	select	a.nr_seq_tabela,
		a.nr_seq_plano,
		c.nr_sequencia,
		a.nr_sequencia,
		b.dt_nascimento,
		nvl(substr(pls_obter_garu_dependencia_seg(c.nr_sequencia,'C'),1,2),'X') ie_grau_parentesco
	from	pls_sca_vinculo	a,
		pessoa_fisica	b,
		pls_segurado	c
	where	a.nr_seq_segurado	= c.nr_sequencia
	and	c.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	c.cd_estabelecimento	= cd_estabelecimento_p
	and	to_char(b.dt_nascimento, 'mm')	= to_char(dt_mesano_referencia_w, 'mm')
	and	c.dt_liberacao is not null
	and	c.dt_rescisao is null
	and	a.dt_liberacao is not null
	and	a.dt_fim_vigencia is null
	and	ie_tipo_contratacao_w	= 'F'
	and	ie_aplicacao_reajuste_w in ('G','S');

Cursor C05 is
	select	nr_sequencia,
		vl_preco_atual
	from	pls_plano_preco
	where	qt_idade_ww	>= qt_idade_inicial
	and	qt_idade_ww	<= qt_idade_final
	and	nr_seq_tabela	= nr_seq_tabela_w
	and	nvl(ie_grau_titularidade,ie_grau_parentesco_w)	= ie_grau_parentesco_w
	and	qt_vidas_w between nvl(qt_vidas_inicial,qt_vidas_w) and nvl(qt_vidas_final,qt_vidas_w)
	order	by	nvl(ie_grau_titularidade,' ');

begin

select	count(*)
into	cont_w
from	pls_segurado_preco
where	nr_seq_lote	= nr_seq_lote_p;

if	(cont_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort( 181602, null );
	/*Ja foram inseridos beneficiarios para este lote!
	Para gerar novamente exclua os ja existentes!' */
end if;

select	dt_mesano_referencia,
	ie_tipo_contratacao,
	nr_seq_contrato,
	trunc(dt_mesano_referencia,'month'),
	last_day(dt_mesano_referencia),
	ie_aplicacao_reajuste,
	nr_seq_intercambio,
	ie_tipo_beneficiario,
	nr_seq_reajuste,
	ie_tipo_contratacao_benef,
	nr_seq_lote_referencia
into	dt_mesano_referencia_w,
	ie_tipo_contratacao_w,
	nr_seq_contrato_w,
	dt_inicial_w,
	dt_final_w,
	ie_aplicacao_reajuste_w,
	nr_seq_intercambio_w,
	ie_tipo_beneficiario_w,
	nr_seq_reajuste_ww,
	ie_tipo_contratacao_benef_w,
	nr_seq_lote_referencia_w
from	pls_lote_reaj_segurado
where	nr_sequencia	= nr_seq_lote_p;

select	max(nr_contrato)
into	nr_contrato_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_w;

select	max(ie_reajustar_benef_cancelado)
into	ie_reajustar_benef_cancelado_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

ie_reajustar_benef_cancelado_w	:= nvl(ie_reajustar_benef_cancelado_w,'T');

select	max(ie_reajustar_benef_cancelado),
	max(tx_deflator),
	max(ie_status),
	max(tx_reajuste)
into	ie_situacao_reaj_contrato_w,
	tx_deflator_w,
	ie_status_w,
	tx_reajuste_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_ww;

if	(ie_status_w <> '2') then
	wheb_mensagem_pck.exibir_mensagem_abort(413189);
end if;

ie_situacao_reaj_contrato_w	:= nvl(ie_situacao_reaj_contrato_w,'T');

open c01;
loop
fetch c01 into
	nr_seq_segurado_w,
	dt_nascimento_w,
	nr_seq_tabela_w,
	ie_preco_w,
	ie_tipo_segurado_ww,
	nr_seq_contrato_ww,
	ie_grau_parentesco_w;
exit when c01%notfound;
	begin
	ie_gerar_reajuste_w	:= 'S';
	nr_seq_processo_w	:= null;
	nr_seq_processo_reaj_w	:= null;
	
	if	((nr_seq_tabela_w is null) and (ie_preco_w = '1') and (ie_tipo_segurado_ww in ('B','A'))) then /* Somente e obrigatorio existir tabela para produto com formacao de preco pre estabelecido */
		pls_gerar_log_erro_reaj(nr_seq_lote_p, nr_seq_segurado_w, wheb_mensagem_pck.get_texto(280499), -- 280499 = Nao existe tabela de preco informada para o beneficiario. Verifique.
						cd_estabelecimento_p, nm_usuario_p);
		ie_gerar_reajuste_w	:= 'N';
	end if;
	
	if	(nr_seq_tabela_w is not null) then
		qt_idade_w	:= trunc((months_between(fim_mes(dt_mesano_referencia_w),dt_nascimento_w) / 12));
		
		if	((qt_idade_w > 999) or (qt_idade_w < 0)) then
			pls_gerar_log_erro_reaj(nr_seq_lote_p, nr_seq_segurado_w, wheb_mensagem_pck.get_texto(280500, 'QT_IDADE_P=' || qt_idade_w), -- 280500 = O beneficiario possui #@QT_IDADE_P#@ anos. Verifique.
						cd_estabelecimento_p, nm_usuario_p);
			ie_gerar_reajuste_w	:= 'N';
		end if;
		
		begin
		select	nvl(ie_preco_vidas_contrato,'N'),
			nvl(ie_calculo_vidas,'A')
		into	ie_preco_vidas_contrato_w,
			ie_calculo_vidas_w
		from	pls_tabela_preco
		where	nr_sequencia	= nr_seq_tabela_w;
		exception
		when others then
			ie_preco_vidas_contrato_w	:= 'N';
			ie_calculo_vidas_w		:= 'A';
		end;
		
		if	(ie_preco_vidas_contrato_w = 'S') then
			if	(ie_calculo_vidas_w = 'A') then
				select	count(*)
				into	qt_vidas_w
				from	pls_segurado
				where	nr_seq_contrato = nr_seq_contrato_ww
				and	dt_liberacao is not null
				and	((dt_rescisao is null) or (dt_rescisao > dt_mesano_referencia_w));
			elsif	(ie_calculo_vidas_w = 'T') then
				select	count(*)
				into	qt_vidas_w
				from	pls_segurado
				where	nr_seq_contrato = nr_seq_contrato_ww
				and	nr_seq_titular is null
				and	dt_liberacao is not null
				and	((dt_rescisao is null) or (dt_rescisao > dt_mesano_referencia_w));
			elsif	(ie_calculo_vidas_w = 'D') then
				select	count(*)
				into	qt_vidas_w
				from	pls_segurado
				where	nr_seq_contrato = nr_seq_contrato_ww
				and	nr_seq_titular is not null
				and	dt_liberacao is not null
				and	((dt_rescisao is null) or (dt_rescisao > dt_mesano_referencia_w));
			elsif	(ie_calculo_vidas_w = 'TD') then
				select	count(*)
				into	qt_vidas_w
				from	pls_segurado a
				where	a.nr_seq_contrato = nr_seq_contrato_ww
				and	a.dt_liberacao is not null
				and	((a.dt_rescisao is null) or (a.dt_rescisao > dt_mesano_referencia_w))
				and	((nr_seq_titular is null) or ((nr_seq_titular is not null) and ((select	count(*)
													from	grau_parentesco x
													where	x.nr_sequencia = a.nr_seq_parentesco
													and	x.ie_tipo_parentesco = '1') > 0)));
			elsif	(ie_calculo_vidas_w = 'F') then
				qt_vidas_w	:= nvl(pls_obter_qt_familia_benef(nr_seq_segurado_w,nr_seq_titular_w,'L',dt_mesano_referencia_w),0);
			end if;
		else
			qt_vidas_w	:= 1;
		end if;
		
		qt_idade_ww	:= qt_idade_w;
		open C05;
		loop
		fetch C05 into
			nr_seq_preco_w,
			vl_preco_atual_www;
		exit when C05%notfound;
		end loop;
		close C05;
		
		if	(nvl(nr_seq_preco_w,0) = 0) then
			pls_gerar_log_erro_reaj(nr_seq_lote_p, nr_seq_segurado_w, wheb_mensagem_pck.get_texto(280501),
						cd_estabelecimento_p, nm_usuario_p);
			ie_gerar_reajuste_w	:= 'N';
		end if;
		
		if	(ie_gerar_reajuste_w = 'S') then
			select	nvl(nr_seq_titular, nr_sequencia),
				cd_pessoa_fisica
			into	nr_seq_titular_ww,
				cd_pessoa_fisica_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_w;
		
			select	max(a.nr_sequencia)
			into	nr_seq_processo_reaj_w
			from	pls_processo_judicial_reaj	a,
				processo_judicial_liminar	b
			where	a.nr_seq_processo		= b.nr_sequencia
			and	((b.nr_seq_segurado	= nr_seq_segurado_w) or (b.nr_seq_segurado is null))
			and	((b.nr_seq_contrato	= nr_seq_contrato_ww) or (b.nr_seq_contrato is null))
			and	b.ie_estagio			= 2 --Liberado
			and	a.ie_impedir_reaj_fx_etaria	= 'S'
			and	dt_mesano_referencia_w	>= nvl(b.dt_inicio_validade,dt_mesano_referencia_w) --data do processo
			and	dt_mesano_referencia_w	<= nvl(b.dt_fim_validade,dt_mesano_referencia_w)
			and	dt_mesano_referencia_w	>= nvl(a.dt_inicio_vigencia,dt_mesano_referencia_w) --data da regra
			and	dt_mesano_referencia_w	<= nvl(a.dt_fim_vigencia,dt_mesano_referencia_w);
			
			if	((cd_pessoa_fisica_w is not null) and (nr_seq_processo_reaj_w is null)) then
				select	max(a.nr_sequencia)
				into	nr_seq_processo_reaj_w
				from	pls_processo_judicial_reaj	a,
					processo_judicial_liminar	b
				where	a.nr_seq_processo		= b.nr_sequencia
				and	b.nr_seq_segurado in (	select	x.nr_sequencia
								from	pls_segurado x
								where	x.cd_pessoa_fisica = cd_pessoa_fisica_w)
				and	b.ie_estagio			= 2 --Liberado
				and	a.ie_impedir_reaj_fx_etaria	= 'S'
				and	b.ie_considera_codigo_pf = 'S'
				and	dt_mesano_referencia_w	>= nvl(b.dt_inicio_validade,dt_mesano_referencia_w) --data do processo
				and	dt_mesano_referencia_w	<= nvl(b.dt_fim_validade,dt_mesano_referencia_w)
				and	dt_mesano_referencia_w	>= nvl(a.dt_inicio_vigencia,dt_mesano_referencia_w) --data da regra
				and	dt_mesano_referencia_w	<= nvl(a.dt_fim_vigencia,dt_mesano_referencia_w);
			end if;
			
			if	((nr_seq_titular_ww is not null) and (nr_seq_processo_reaj_w is null)) then
				select	max(a.nr_sequencia)
				into	nr_seq_processo_reaj_w
				from	pls_processo_judicial_reaj 	a,
					processo_judicial_liminar 	b
				where	a.nr_seq_processo		= b.nr_sequencia
				and	(b.nr_seq_segurado in (	select	x.nr_sequencia
								from	pls_segurado x
								where	((x.nr_seq_titular = nr_seq_titular_ww) or (x.nr_sequencia = nr_seq_titular_ww))))
				and	b.ie_estagio			= 2 --Liberado
				and	a.ie_impedir_reaj_fx_etaria	= 'S'
				and	a.ie_considerar_dependente	= 'S'
				and	dt_mesano_referencia_w	>= nvl(b.dt_inicio_validade,dt_mesano_referencia_w) --data do processo
				and	dt_mesano_referencia_w	<= nvl(b.dt_fim_validade,dt_mesano_referencia_w)
				and	dt_mesano_referencia_w	>= nvl(a.dt_inicio_vigencia,dt_mesano_referencia_w) --data da regra
				and	dt_mesano_referencia_w	<= nvl(a.dt_fim_vigencia,dt_mesano_referencia_w);
			end if;
			
			ie_situacao_preco_w		:= 'A';

			if	(nvl(nr_seq_processo_reaj_w,0) <> 0) then
				ie_situacao_preco_w	:= 'I';
				
				select	max(nr_seq_processo)
				into	nr_seq_processo_w
				from	pls_processo_judicial_reaj
				where	nr_sequencia	= nr_seq_processo_reaj_w;
			end if;
			
			select	pls_segurado_preco_seq.nextval
			into	nr_seq_seg_preco_w
			from	dual;
			
			insert	into pls_segurado_preco
				(nr_sequencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, dt_reajuste,
				nr_seq_segurado, vl_preco_atual, qt_idade,
				cd_motivo_reajuste, vl_preco_ant, ds_observacao,
				nr_seq_reajuste, nr_seq_preco, nr_seq_lote,
				dt_liberacao, nm_usuario_liberacao, vl_desconto,
				vl_preco_nao_subsid_desc, nr_seq_tabela, nr_seq_processo,
				ie_situacao, nr_seq_processo_reaj)
			values	(nr_seq_seg_preco_w, sysdate, nm_usuario_p,
				sysdate, nm_usuario_p, trunc(dt_mesano_referencia_w,'dd'),
				nr_seq_segurado_w, 0, qt_idade_w,
				'E', 0, null,
				null, nr_seq_preco_w, nr_seq_lote_p,
				null, null, 0,
				0,nr_seq_tabela_w, nr_seq_processo_w,
				ie_situacao_preco_w, nr_seq_processo_reaj_w);
		end if;
	end if;
	end;
end loop;
close c01;

/* Reajuste por faixa etaria SCA vinculado ao beneficiario */
open C04;
loop
fetch C04 into
	nr_seq_tabela_sca_w,
	nr_seq_sca_w,
	nr_seq_segurado_sca_w,
	nr_seq_vinculo_sca_w,
	dt_nascimento_w,
	ie_grau_parentesco_w;
exit when C04%notfound;
	begin
	
	ie_gerar_reajuste_w	:= 'S';
	if	(nr_seq_tabela_sca_w is not null) then
		qt_idade_w	:= trunc((months_between(fim_mes(dt_mesano_referencia_w),dt_nascimento_w) / 12));
		
		if	((qt_idade_w > 999) or (qt_idade_w < 0)) then
			pls_gerar_log_erro_reaj(nr_seq_lote_p, nr_seq_segurado_w, wheb_mensagem_pck.get_texto(280500, 'QT_IDADE_P=' || qt_idade_w), -- 280500 = O beneficiario possui #@QT_IDADE_P#@ anos. Verifique.
						cd_estabelecimento_p, nm_usuario_p);
			ie_gerar_reajuste_w	:= 'N';
		end if;
		
		qt_vidas_w	:= 0;
		nr_seq_tabela_w	:= nr_seq_tabela_sca_w;
		qt_idade_ww	:= qt_idade_w;
		open C05;
		loop
		fetch C05 into
			nr_seq_preco_sca_w,
			vl_preco_atual_www;
		exit when C05%notfound;
		end loop;
		close C05;
		
		if	(nvl(nr_seq_preco_sca_w,0) = 0) then
			select	max(a.nr_contrato)
			into	nr_seq_contrato_seg_w
			from	pls_segurado	b,
				pls_contrato	a
			where	a.nr_sequencia 	= b.nr_seq_contrato
			and	b.nr_sequencia 	= nr_seq_segurado_sca_w;
			
			select	max(a.nr_sequencia)
			into	nr_seq_produto_seg_w
			from	pls_segurado	b,
				pls_plano	a
			where	a.nr_sequencia = b.nr_seq_plano
			and	b.nr_sequencia = nr_seq_segurado_sca_w;
			
			wheb_mensagem_pck.exibir_mensagem_abort( 181623, 'QT_IDADE=' || qt_idade_w || ';' || 'NR_SEQ_TABELA_SCA=' || to_char(nr_seq_tabela_sca_w) || ';' ||
									'NR_SEQ_SEGURADO_SCA=' || to_char(nr_seq_segurado_sca_w) || ';' || 'NR_SEQ_CONTRATO_SEG=' || to_char(nr_seq_contrato_seg_w) || ';' ||
									'NR_SEQ_PRODUTO_SEG=' ||to_char(nr_seq_produto_seg_w));
		end if;
		
		if	(ie_gerar_reajuste_w = 'S') then
			select	pls_segurado_preco_origem_seq.nextval
			into	nr_seq_reaj_preco_sca_w
			from	dual;
			
			insert	into pls_segurado_preco_origem
				(nr_sequencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, dt_reajuste,
				nr_seq_segurado, vl_preco_atual, qt_idade,
				cd_motivo_reajuste, ds_observacao, cd_estabelecimento,
				nr_seq_reajuste, nr_seq_preco, nr_seq_lote_reajuste,
				dt_liberacao, nm_usuario_liberacao, nr_seq_tabela,
				vl_preco_ant, nr_seq_plano, nr_seq_vinculo_sca)
			values	(nr_seq_reaj_preco_sca_w, sysdate, nm_usuario_p,
				sysdate, nm_usuario_p, trunc(dt_mesano_referencia_w,'dd'),
				nr_seq_segurado_sca_w, 0, qt_idade_w,
				'E', ds_observacao_w, cd_estabelecimento_p,
				null, nr_seq_preco_sca_w, nr_seq_lote_p,
				null, null, nr_seq_tabela_sca_w,
				0, nr_seq_sca_w, nr_seq_vinculo_sca_w);
		end if;
	end if;
	end;
end loop;
close C04;

open c02;
loop
fetch c02 into
	nr_seq_segurado_w,
	nr_seq_tabela_w,
	dt_nascimento_w,
	dt_rescisao_contrato_w;
exit when c02%notfound;
	begin
	if	((dt_rescisao_contrato_w is null) or (dt_rescisao_contrato_w > dt_mesano_referencia_w)) or
		((dt_rescisao_contrato_w is not null) and (ie_reajustar_benef_cancelado_w in ('T','I'))) then
		pls_preco_beneficiario_pck.atualizar_reajuste_benef(nr_seq_segurado_w, dt_mesano_referencia_w, nr_seq_reajuste_ww, nr_seq_lote_p, 'N', nm_usuario_p, cd_estabelecimento_p);
	end if;
	end;
end loop;
close c02;

open c03;
loop
fetch c03 into
	nr_seq_segurado_w,
	nr_seq_tabela_w,
	qt_idade_w,
	nm_pessoa_fisica_w,
	dt_rescisao_contrato_w,
	ie_grau_parentesco_w,
	cd_matricula_familia_w,
	nr_seq_titular_w,
	dt_nascimento_ww;
exit when c03%notfound;
	begin
	select	max(nr_sequencia)
	into	nr_seq_reaj_deflator_w
	from 	pls_reajuste
	where	nr_seq_lote_deflator = nr_seq_reajuste_ww;

	if	(nvl(nr_seq_reaj_deflator_w,0) <> 0) then
		select	count(1)
		into	qt_reaj_w
		from	pls_segurado_preco a,
			pls_reajuste_preco b,
			pls_reajuste	   c
		where	b.nr_sequencia 	= a.nr_seq_reajuste
		and	c.nr_sequencia 	= b.nr_seq_reajuste
		and	a.nr_seq_segurado = nr_seq_segurado_w
		and	c.nr_sequencia 	= nr_seq_reaj_deflator_w;

		if	(qt_reaj_w > 0) then
			ie_gerar_reajuste_deflator_w	:= 'S';
		else
			ie_gerar_reajuste_deflator_w	:= 'N';
		end if;
	end if;

	if	((ie_gerar_reajuste_deflator_w = 'S') and
		((dt_rescisao_contrato_w is null) or (dt_rescisao_contrato_w > dt_mesano_referencia_w)) or
		((dt_rescisao_contrato_w is not null) and (ie_reajustar_benef_cancelado_w in ('T','C')))) then
		
		/*Caso a situacao para o reajuste do contrato for diferente de ambos verificar como esta a situacao do contrato*/
		if	(ie_situacao_reaj_contrato_w <> 'T') then
			if	(nr_seq_contrato_w is not null) then
				if	(pls_obter_situac_contr_periodo(nr_seq_contrato_w,'C','M',dt_mesano_referencia_w) <> ie_situacao_reaj_contrato_w) then
					goto finalCursor03;
				end if;
			elsif	(nr_seq_intercambio_w is not null) then
				if	(pls_obter_situac_contr_periodo(nr_seq_intercambio_w,'I','M',dt_mesano_referencia_w) <> ie_situacao_reaj_contrato_w) then
					goto finalCursor03;
				end if;
			end if;
		end if;
		
		if	(length(qt_idade_w) > 3) then
			wheb_mensagem_pck.exibir_mensagem_abort( 181614, 'NR_SEQ_SEGURADO=' || nr_seq_segurado_w || ';' || 'NM_PESSOA_FISICA=' || nm_pessoa_fisica_w );
		end if;
		
		pls_preco_beneficiario_pck.atualizar_reajuste_benef(nr_seq_segurado_w, dt_mesano_referencia_w, nr_seq_reajuste_ww, nr_seq_lote_p, 'N', nm_usuario_p, cd_estabelecimento_p);
	end if;
	
	<<finalCursor03>>
	nr_seq_segurado_w	:= nr_seq_segurado_w;
	end;
end loop;
close c03;

if	(ie_tipo_contratacao_w <> 'F') then
	select	count(*)
	into	qt_sca_w
	from	pls_segurado_preco_origem
	where	nr_seq_lote_reajuste	= nr_seq_lote_p;
	
	if	(qt_sca_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 181605, null );
		/*Ja foram inseridos reajustes SCA beneficiarios para este lote!
		Para gerar novamente exclua os ja existentes!*/
	else
		pls_inserir_preco_benef_sca(nr_seq_lote_p,nm_usuario_p);
	end if;
end if;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

if	(nr_seq_reajuste_ww = nvl(nr_seq_lote_referencia_w,0)) then
	select	count(1)
	into	cont_seg_preco_w
	from	pls_segurado_preco
	where	nr_seq_lote = nr_seq_lote_p;
	
	select	count(1)
	into	cont_seg_preco_orig_w
	from	pls_segurado_preco_origem
	where	nr_seq_lote_reajuste = nr_seq_lote_p;
	
	if	(cont_seg_preco_w = 0 and cont_seg_preco_orig_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(232876, 'NR_CONTRATO='||nr_contrato_w );
	end if;
end if;

end pls_inserir_preco_beneficiario;
/