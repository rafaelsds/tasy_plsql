create or replace
procedure inactivate_advs_evts_result(nr_seq_registro_p number) as
begin

    update eventos_adversos_result
    set ie_situacao = 'I',
        nm_usuario = obter_usuario_ativo
    where nr_seq_registro = nr_seq_registro_p;
    
    commit;

end inactivate_advs_evts_result;
/
