create or replace
procedure pls_cria_job_ajus_estr_mat is 

jobno			number;
qt_reg_w		number;
ds_comando_job_w	varchar2(2000);	

begin

-- verifica se existem jobs ativas, se tiver alguma job parada j?? faz a limpeza da mesma
pls_obter_qt_job_ativa('PLS_AJUSTAR_ESTRUT_MAT_TM', qt_reg_w);

if	(qt_reg_w = 0) then
	begin
		ds_comando_job_w	:= 'PLS_AJUSTAR_ESTRUT_MAT_TM;';
		DBMS_JOB.SUBMIT(job => jobno,
				what => ds_comando_job_w,
				next_date => sysdate);
		commit;
	exception
	when others then
		null;
	end;
else
	wheb_mensagem_pck.exibir_mensagem_abort(1093644); -- O processo j� est� em execu��o.
end if;

end pls_cria_job_ajus_estr_mat;
/