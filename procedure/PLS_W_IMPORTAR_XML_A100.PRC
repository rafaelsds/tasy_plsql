create or replace
procedure pls_w_importar_xml_a100
			( 	ds_conteudo_p			varchar2,
				ie_tipo_registro_p		varchar2,
				ie_tipo_repasse_p		varchar2,
				ie_tipo_compartilhamento_p	varchar2,
				ie_tipo_movimento_p		varchar2,
				ie_operacao_p			varchar2,
				ie_proprio_recebido_p		varchar2,
				nm_usuario_p			varchar2) is

nr_transacao_w			ptu_intercambio_lote_receb.nr_transacao%type;
qt_lote_transacao_w		number(10);
ds_ptu_w			varchar2(255);		
ie_permite_imp_arq_duplicado_w	varchar2(10);

begin

if	(ie_operacao_p = 'I') then
	if	(ie_tipo_registro_p = '1') then
		select	obter_valor_param_usuario(1286,7,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento) -- Permitir importar o mesmo arquivo mais de uma vez
		into	ie_permite_imp_arq_duplicado_w
		from	dual;
	
		if	(ie_permite_imp_arq_duplicado_w = 'N') then
			if	(instr(ds_conteudo_p,'ptu:') > 0 ) then
				ds_ptu_w := 'ptu:';
			else
				ds_ptu_w := '';
			end if;
		
			nr_transacao_w		:= pls_extrair_dado_tag_xml(ds_conteudo_p,'<'||ds_ptu_w||'nr_transacao>');

			if	(nr_transacao_w is not null) then
				select	count(1)
				into	qt_lote_transacao_w
				from	ptu_intercambio_lote_receb
				where	nr_transacao = nr_transacao_w;
				
				if	(qt_lote_transacao_w > 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(1086622); -- Arquivo j� foi importado, n�o pode ser importado novamente conforme o par�metro[7]. Favor verificar.
				end if;
			end if;
		end if;
	end if;
	
	insert	into	w_importar_xml_a100
		(	dt_atualizacao, nm_usuario, ds_conteudo, ie_tipo_registro,
			ie_tipo_repasse, ie_tipo_compartilhamento, ie_tipo_movimento,ie_proprio_recebido)
		values (sysdate, nm_usuario_p, ds_conteudo_p, ie_tipo_registro_p,
			ie_tipo_repasse_p, ie_tipo_compartilhamento_p, ie_tipo_movimento_p,ie_proprio_recebido_p );
elsif	(ie_operacao_p = 'D') then
	delete from w_importar_xml_a100
	where nm_usuario = nm_usuario_p;
end if;

commit;

end pls_w_importar_xml_a100;
/
