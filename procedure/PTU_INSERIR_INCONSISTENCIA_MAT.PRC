create or replace
procedure ptu_inserir_inconsistencia_mat(	nr_seq_material_p	number,
						nr_seq_inconsistencia_p	number,
						nm_usuario_p		varchar2) is 

/*Finalidade: Verificar se deve realizar a consistencia no A400/450
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
*/

ds_inconsistencia_w	varchar2(255);
					
begin
select	substr(max(ds_inconsistencia),1,255)
into	ds_inconsistencia_w
from	ptu_inconsistencia_mat
where	nr_sequencia = nr_seq_inconsistencia_p;

if	(nr_seq_inconsistencia_p is not null) and
	(nr_seq_material_p is not null) then
	insert into pls_mat_unimed_inc
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ds_inconsistencia,
		ie_tipo_inconsistencia,
		nr_seq_material)
	values	(pls_mat_unimed_inc_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		ds_inconsistencia_w,
		'E',
		nr_seq_material_p);
end if;

end ptu_inserir_inconsistencia_mat;
/