create or replace
procedure pls_gravar_digital_benef
			(	cd_pessoa_fisica_p	varchar2,
				ds_biometria_p		varchar2,
				ie_dedo_p		number,
				nm_usuario_p		varchar2) is

qt_biometria_w		number(10);

begin

if	(cd_pessoa_fisica_p is not null) then
	select	count(1)
	into	qt_biometria_w
	from	pessoa_fisica_biometria
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_dedo			= ie_dedo_p;
	
	if	(qt_biometria_w	= 0) then
		insert into pessoa_fisica_biometria(	nr_sequencia, cd_pessoa_fisica, ds_biometria, cd_empresa_biometria, dt_atualizacao,
							nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_dedo)
						values(	pessoa_fisica_biometria_seq.NextVal, cd_pessoa_fisica_p, ds_biometria_p, 1, sysdate,
							nm_usuario_p, sysdate, nm_usuario_p, ie_dedo_p);
	elsif	(qt_biometria_w	> 0) then
		update	pessoa_fisica_biometria
		set	ds_biometria	= ds_biometria_p,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_dedo		= ie_dedo_p;
	end if;
	
	commit;
end if;

end pls_gravar_digital_benef;
/