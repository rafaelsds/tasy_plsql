create or replace
procedure sac_vincular_bo(	nr_seq_bo_p		number,
			nr_seq_bo_vinc_p		number,
			ie_opcao_p		varchar2,
			nm_usuario_p		Varchar2) is 

/* ie_opca_p
V - Vincular
D - Desvincular
*/

begin
if	(ie_opcao_p = 'V') then
	begin
	insert	into sac_boletim_vinculado (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_bo,
						nr_seq_bo_vinc)
					values(	sac_boletim_vinculado_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_bo_p,
						nr_seq_bo_vinc_p);
	end;
elsif	(ie_opcao_p = 'D') then
	begin
	delete
	from	sac_boletim_vinculado
	where	nr_seq_bo_vinc = nr_seq_bo_vinc_p;
	end;
end if;

commit;

end sac_vincular_bo;
/