create or replace procedure pie_reg_notation (nr_seq_pie_pract_reg_p	in pie_pract_registration.nr_sequencia%type,
                                              dt_notation_edit_p   	in pie_pract_reg_notation.dt_notation_edit%type,
                                              tp_notation_p        	in pie_pract_reg_notation.tp_notation%type,
                                              ds_notation_detail_p 	in pie_pract_reg_notation.ds_notation_detail%type)
is
  nr_sequencia_w 		pie_pract_reg_notation.nr_sequencia%type;
begin



	if (( dt_notation_edit_p is not null )
	and ( ds_notation_detail_p is not null )
	and ( nr_seq_pie_pract_reg_p is not null )) then

		select pie_pract_reg_notation_seq.nextval
		into   nr_sequencia_w
		from   dual;

		insert into pie_pract_reg_notation
			(nr_sequencia,
			dt_notation_edit,
			tp_notation,
			ds_notation_detail,
			nr_seq_pie_pract_reg)
		values	( nr_sequencia_w,
			dt_notation_edit_p,
			tp_notation_p,
			ds_notation_detail_p,
			nr_seq_pie_pract_reg_p );
	else
		wheb_mensagem_pck.exibir_mensagem_abort(1088701);
	end if;
end pie_reg_notation;
/