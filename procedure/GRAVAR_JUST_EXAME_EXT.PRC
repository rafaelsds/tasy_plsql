create or replace
procedure gravar_just_exame_ext(nr_seq_exame_p	number,
				ds_justificativa_p	varchar2,
				ds_alerta_p		varchar2,
				nm_usuario_p		Varchar2) is 

begin
if	(nvl(nr_seq_exame_p,0) > 0) then

	insert into pedido_exame_externo_just (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_sol_exame,
						ds_justificativa,
						ds_alerta)
					values(	pedido_exame_externo_just_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_exame_p, 
						ds_justificativa_p,
						ds_alerta_p);
	commit;

end if;

end gravar_just_exame_ext;
/