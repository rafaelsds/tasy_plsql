create or replace
procedure gerar_proc_executado (
			nr_seq_procedimento_p	in	procedimento_pac_executado.nr_seq_procedimento%type,
			nr_seq_proc_int_exec_p	in	procedimento_pac_executado.nr_seq_proc_int_exec%type,
			qt_procedimento_p		in	prescr_procedimento.qt_procedimento%type default null,
			ds_justificativa_exec_p	in	procedimento_paciente_dado.ds_justificativa_exec%type default null,
			nm_usuario_p			in	usuario.nm_usuario%type) is 

ie_situacao_w			constant varchar2(1 char) := 'A';

cd_procedimento_w		proc_interno.cd_procedimento%type;
ie_origem_proced_w		proc_interno.ie_origem_proced%type;

nr_prescricao_w			prescr_medica.nr_prescricao%type;

nr_seq_prescr_proc_w	prescr_procedimento.nr_sequencia%type;
ie_lado_w				prescr_procedimento.ie_lado%type;
qt_procedimento_w		prescr_procedimento.qt_procedimento%type;

nr_atendimento_w		atendimento_paciente.nr_atendimento%type;

cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;

nr_seq_propacmed_w		procedimento_pac_medico.nr_sequencia%type;
cd_departamento_w		procedimento_pac_medico.cd_departamento%type;

nr_seq_propaci_w		procedimento_paciente.nr_sequencia%type;
cd_setor_origem_w			prescr_medica.cd_setor_atendimento%type;
nr_seq_episodio_w       atendimento_paciente.nr_seq_episodio%type;

ie_gerar_proc_faturavel varchar2(10);
ie_gerar_passagem_setor_w	varchar2(10 char);	
ie_adm_adep_w		varchar2(1 char);								   

begin

obter_param_usuario(942, 331, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_gerar_passagem_setor_w);
obter_param_usuario(942, 78, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_adm_adep_w);

if (nvl(nr_seq_procedimento_p, 0) <> 0) then

	insert into PROCEDIMENTO_PAC_EXECUTADO (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_proc_int_exec,
		nr_seq_procedimento,
		dt_proc_exec
	) values (
		PROCEDIMENTO_PAC_EXECUTADO_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_proc_int_exec_p,
		nr_seq_procedimento_p,
		sysdate
	);
	
	select	max(cd_procedimento),
			max(ie_origem_proced)
	into	cd_procedimento_w,
			ie_origem_proced_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_int_exec_p;
	
	
	select	max(pm.nr_atendimento),
			max(pm.nr_prescricao),
			max(pp.nr_sequencia),
			max(pp.ie_lado),
			max(pp.qt_procedimento),
			max(pm.cd_setor_atendimento)				
	into	nr_atendimento_w,
			nr_prescricao_w,
			nr_seq_prescr_proc_w,
			ie_lado_w,
			qt_procedimento_w,
			cd_setor_origem_w	 
	from	prescr_procedimento pp,
			prescr_medica pm
	where	pm.nr_prescricao = pp.nr_prescricao
	and		pp.nr_seq_interno = nr_seq_procedimento_p;

	if (qt_procedimento_p is not null) then
		qt_procedimento_w := qt_procedimento_p;
	end if;
	
	select	Obter_Setor_exec_proc_interno(	nr_seq_proc_int_exec_p,
											cd_setor_origem_w,
											Obter_Tipo_Atendimento(nr_atendimento_w),
											Obter_Convenio_Atendimento(nr_atendimento_w),
											obter_categoria_convenio_atend(nr_atendimento_w, Obter_Convenio_Atendimento(nr_atendimento_w)))
	into	cd_setor_atendimento_w
	from	dual;
	
	if		(cd_setor_atendimento_w is not null) and
			(ie_gerar_passagem_setor_w = 'S' ) then
	
			gerar_passagem_setor_atend(nr_atendimento_w, cd_setor_atendimento_w, sysdate, 'S', nm_usuario_p);				
	
	else
	
			select 	cd_setor_origem_w
			into	cd_setor_atendimento_w
			from	dual;

	end if;
	
	
	select	obter_departamento_setor(cd_setor_atendimento_w),
	        obter_episodio_atendimento(nr_atendimento_w)
	into	cd_departamento_w,
	        nr_seq_episodio_w
	from	dual;
	
	select	PROCEDIMENTO_PAC_MEDICO_seq.nextval
	into	nr_seq_propacmed_w
	from	dual;
	
	ie_gerar_proc_faturavel := obter_valor_param_usuario(942, 456, Obter_Perfil_Ativo, nm_usuario_p,obter_estabelecimento_ativo);
    

    if(nvl(ie_gerar_proc_faturavel,'N') = 'S') then
        obter_proc_tab_interno(nr_seq_proc_int_exec_p , nr_prescricao_w,nr_atendimento_w,null, cd_procedimento_w, cd_setor_origem_w, null, null);
    end if;
	
	insert into PROCEDIMENTO_PAC_MEDICO (
		nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		ie_situacao, 
		nr_atendimento, 
		nr_seq_episodio,
		cd_setor_atendimento, 
		cd_procedimento, 
		dt_procedimento, 
		ie_lado,
		ie_origem_proced, 
		nr_seq_proc_interno,
		qt_procedimento,
		cd_departamento
	) values (
		nr_seq_propacmed_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_situacao_w,
		nr_atendimento_w,
		decode(nr_seq_episodio_w,0,null,nr_seq_episodio_w),
		cd_setor_atendimento_w,
		cd_procedimento_w,
		sysdate,
		ie_lado_w,
		ie_origem_proced_w,
		nr_seq_proc_int_exec_p,
		qt_procedimento_w,
		cd_departamento_w
	);
	
	LIBERAR_PROC_PAC_MEDIC(
		nr_seq_proc_pac_med_p => nr_seq_propacmed_w, 
		nm_usuario_p => nm_usuario_p,
		ds_justificativa_exec_p => ds_justificativa_exec_p
	);
	
	commit;
	
	select	max(nr_seq_propaci)
	into	nr_seq_propaci_w
	from	PROCEDIMENTO_PAC_MEDICO
	where	nr_sequencia = nr_seq_propacmed_w;
	
	if (nr_seq_propaci_w is not null) then
		update	procedimento_paciente
		set	nr_prescricao			= nr_prescricao_w,
			nr_sequencia_prescricao	= nr_seq_prescr_proc_w	
		where	nr_sequencia		= nr_seq_propaci_w;
	end if;
	
	update	prescr_procedimento
	set	dt_baixa		= sysdate,
		cd_motivo_baixa	= 1,
		dt_atualizacao	= sysdate,
		nm_usuario_baixa	= nm_usuario_p,
		ie_status_execucao	= '20'
	where 	nr_seq_interno	= nr_seq_procedimento_p;
	
	if  (nvl(ie_adm_adep_w,'N') = 'S') then
		ge_executar_adep(nr_prescricao_w, nr_seq_prescr_proc_w, nm_usuario_p);
    end if;

end if;

commit;

end gerar_proc_executado;
/
