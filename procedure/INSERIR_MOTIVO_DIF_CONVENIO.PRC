create or replace
procedure inserir_motivo_dif_convenio(
			nr_sequencia_p	number,
			nr_seq_motivo_dif_p	number,
			nr_atendimento_p	number) is 

nr_sequencia_w 	atend_paciente_unidade.nr_sequencia%type;

begin

if  (nvl(nr_atendimento_p,0) > 0) then
	
	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento 		= nr_atendimento_p
	and		obter_classif_setor_atend(nr_atendimento_p) = 3;

	update	atend_paciente_unidade
	set     nr_seq_motivo_dif = nr_seq_motivo_dif_p
	where	nr_sequencia = nr_sequencia_w
	and		nr_atendimento = nr_atendimento_p;
	
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end inserir_motivo_dif_convenio;
/