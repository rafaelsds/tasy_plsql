create or replace
procedure GERAR_W_REGRA_QTDE_PROC
		(nr_interno_conta_p	in	number,
		nm_usuario_p		in	varchar2) is

/*

1 - Procedimentos com quantidade abaixo da regra
2 - Procedimentos com quantidade acima da regra

*/


cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_atendimento_w		number(10,0);
nr_seq_regra_w			number(10,0);
cd_convenio_w			number(5,0);
ie_tipo_atendimento_w		number(3,0);
qt_permitida_w			number(15, 3);
ie_acao_excesso_w		varchar2(1);
qt_horas_intervalo_w		number(5,0);
ie_tipo_qtde_w			varchar2(1);
dt_final_periodo_w		date;
qt_executada_dia_w		number(15, 3);
qt_executada_periodo_w		number(15, 3);
qt_executada_atend_w		number(15, 3);
dt_procedimento_w		date;
cd_setor_atendimento_w		number(6);
ds_erro_w			varchar2(255);

cursor	c01 is
select	distinct cd_procedimento,
	ie_origem_proced
from	procedimento_paciente
where	nr_interno_conta	= nr_interno_conta_p;

cursor	c02 is
select 	a.nr_sequencia,
	a.qt_permitida,
	a.ie_acao_excesso,
	a.qt_horas_intervalo,
	a.ie_tipo_qtde
from 	convenio_regra_qtde_proc a
where	a.cd_procedimento				= cd_procedimento_w
and	a.ie_origem_proced				= ie_origem_proced_w
and	a.cd_convenio					= cd_convenio_w
and 	nvl(a.ie_tipo_atendimento, ie_tipo_atendimento_w) = ie_tipo_atendimento_w;


cursor	c03 is
select	distinct trunc(dt_procedimento, 'dd')
from	procedimento_paciente
where	cd_procedimento		= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w
and	nr_interno_conta	= nr_interno_conta_p;

cursor c04 is
select	dt_procedimento
from	procedimento_paciente
where	cd_procedimento		= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w
and	nr_interno_conta	= nr_interno_conta_p
order	by dt_procedimento;

begin

delete	from W_REGRA_QTDE_PROC
where	(nr_interno_conta	= nr_interno_conta_p
	or dt_atualizacao	< sysdate - 1);

select	a.nr_atendimento,
	a.cd_convenio_parametro,
	b.ie_tipo_atendimento
into	nr_atendimento_w,
	cd_convenio_w,
	ie_tipo_atendimento_w
from	atendimento_paciente b,
	conta_paciente a
where	a.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento;

open c01;
loop
fetch c01 into
	cd_procedimento_w,
	ie_origem_proced_w;
exit when c01%notfound;
	open c02;
	loop
	fetch c02 into
		nr_seq_regra_w,
		qt_permitida_w,
		ie_acao_excesso_w,
		qt_horas_intervalo_w,
		ie_tipo_qtde_w;
	exit when c02%notfound;
		if	(ie_tipo_qtde_w = 'D') then
			open c03;
			loop
			fetch c03 into
				dt_procedimento_w;
			exit when c03%notfound;

				select 	sum(qt_procedimento),
					max(cd_setor_atendimento)
				into	qt_executada_dia_w,
					cd_setor_atendimento_w
				from	procedimento_paciente
				where	nr_atendimento			= nr_atendimento_w
				and	cd_procedimento			= cd_procedimento_w
				and	ie_origem_proced		= ie_origem_proced_w
				and	trunc(dt_procedimento, 'dd')	= dt_procedimento_w
				and	cd_motivo_exc_conta			is null
				and	nr_interno_conta			is not null;

				if	(qt_executada_dia_w > qt_permitida_w) then
					
					ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(277817),1,255); --Ultrapassou quatidade m�xima do dia
					
					insert	into W_REGRA_QTDE_PROC
						(NR_INTERNO_CONTA,
						CD_PROCEDIMENTO,
						IE_ORIGEM_PROCED,
						QT_REGRA,
						IE_ACAO_EXCESSO,
						DS_ERRO,
						NR_SEQ_REGRA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						qt_conta,
						cd_setor_atendimento)
					values	(nr_interno_conta_p,
						cd_procedimento_w,
						ie_origem_proced_w,
						qt_permitida_w,
						ie_acao_excesso_w,
						ds_erro_w|| ' ' || PKG_DATE_FORMATERS.to_varchar(dt_procedimento_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),
						nr_seq_regra_w,
						sysdate,
						nm_usuario_p,
						qt_executada_dia_w,
						cd_setor_atendimento_w);
				elsif	(qt_executada_dia_w < qt_permitida_w) then
				
					ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(298662),1,255); --N�o atingiu a quantidade m�xima do dia
					
					insert	into W_REGRA_QTDE_PROC
						(NR_INTERNO_CONTA,
						CD_PROCEDIMENTO,
						IE_ORIGEM_PROCED,
						QT_REGRA,
						IE_ACAO_EXCESSO,
						DS_ERRO,
						NR_SEQ_REGRA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						qt_conta,
						cd_setor_atendimento)
					values	(nr_interno_conta_p,
						cd_procedimento_w,
						ie_origem_proced_w,
						qt_permitida_w,
						null,
						ds_erro_w|| ' ' || PKG_DATE_FORMATERS.to_varchar(dt_procedimento_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),
						nr_seq_regra_w,
						sysdate,
						nm_usuario_p,
						qt_executada_dia_w,
						cd_setor_atendimento_w);
				end if;

			end loop;
			close c03;
		end if;

		if	(ie_tipo_qtde_w = 'H') then
			open c04;
			loop
			fetch c04 into
				dt_procedimento_w;
			exit when c04%notfound;

				select	sum(qt_procedimento),
					max(dt_procedimento),
					max(cd_setor_atendimento)
				into	qt_executada_periodo_w,
					dt_final_periodo_w,
					cd_setor_atendimento_w
				from	procedimento_paciente
				where	cd_procedimento		= cd_procedimento_w
				and	ie_origem_proced	= ie_origem_proced_w
				and	nr_interno_conta	= nr_interno_conta_p
				and	cd_motivo_exc_conta	is null
				and	dt_procedimento_w		between	dt_procedimento_w and 
									dt_procedimento_w + ((1 / 24) * qt_horas_intervalo_w); 

				if	(qt_executada_periodo_w > qt_permitida_w) then
				
					ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(277826),1,255); --Ultrapassou quantidade m�xima no per�odo
					
					insert	into W_REGRA_QTDE_PROC
						(NR_INTERNO_CONTA,
						CD_PROCEDIMENTO,
						IE_ORIGEM_PROCED,
						QT_REGRA,
						IE_ACAO_EXCESSO,
						DS_ERRO,
						NR_SEQ_REGRA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						qt_conta,
						cd_setor_atendimento)
					values	(nr_interno_conta_p,
						cd_procedimento_w,
						ie_origem_proced_w,
						qt_permitida_w,
						ie_acao_excesso_w,
						ds_erro_w|| ' ' ||
						PKG_DATE_FORMATERS.to_varchar(dt_procedimento_w, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p) || ' '||OBTER_DESC_EXPRESSAO(347642)||' ' ||
						PKG_DATE_FORMATERS.to_varchar(dt_final_periodo_w, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),
						nr_seq_regra_w,
						sysdate,
						nm_usuario_p,
						qt_executada_periodo_w,
						cd_setor_atendimento_w);
				elsif	(qt_executada_periodo_w < qt_permitida_w) then
				
					ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(298664),1,255);--N�o atingiu a quantidade m�xima no per�odo de
					
					insert	into W_REGRA_QTDE_PROC
						(NR_INTERNO_CONTA,
						CD_PROCEDIMENTO,
						IE_ORIGEM_PROCED,
						QT_REGRA,
						IE_ACAO_EXCESSO,
						DS_ERRO,
						NR_SEQ_REGRA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						qt_conta,
						cd_setor_atendimento)
					values	(nr_interno_conta_p,
						cd_procedimento_w,
						ie_origem_proced_w,
						qt_permitida_w,
						null,
						ds_erro_w|| ' ' ||
						PKG_DATE_FORMATERS.to_varchar(dt_procedimento_w, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p) || ' '||OBTER_DESC_EXPRESSAO(347642)||' ' ||
						PKG_DATE_FORMATERS.to_varchar(dt_final_periodo_w, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p),
						nr_seq_regra_w,
						sysdate,
						nm_usuario_p,
						qt_executada_periodo_w,
						cd_setor_atendimento_w);
				end if;

			end loop;
			close c04;
		end if;

		if	(ie_tipo_qtde_w = 'A') then

			select 	sum(qt_procedimento),
				max(cd_setor_atendimento)
			into	qt_executada_atend_w,
				cd_setor_atendimento_w
			from	procedimento_paciente
			where	nr_atendimento		= nr_atendimento_w
			and	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w
			and	nr_interno_conta	is not null
			and	cd_motivo_exc_conta	is null;

			if	(qt_executada_atend_w > qt_permitida_w) then
				
				ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(277823),1,255);-- Ultrapassou quantidade m�xima do atendimento
				
				insert	into W_REGRA_QTDE_PROC
					(NR_INTERNO_CONTA,
					CD_PROCEDIMENTO,
					IE_ORIGEM_PROCED,
					QT_REGRA,
					IE_ACAO_EXCESSO,
					DS_ERRO,
					NR_SEQ_REGRA,
					DT_ATUALIZACAO,
					NM_USUARIO,
					qt_conta,
					cd_setor_atendimento)
				values	(nr_interno_conta_p,
					cd_procedimento_w, 
					ie_origem_proced_w,
					qt_permitida_w,
					ie_acao_excesso_w,
					ds_erro_w,
					nr_seq_regra_w,
					sysdate,
					nm_usuario_p,
					qt_executada_atend_w,
					cd_setor_atendimento_w);
			elsif	(qt_executada_atend_w < qt_permitida_w) then
			
				ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(298666),1,255);-- N�o atingiu a quantidade m�xima do atendimento
				
				insert	into W_REGRA_QTDE_PROC
					(NR_INTERNO_CONTA,
					CD_PROCEDIMENTO,
					IE_ORIGEM_PROCED,
					QT_REGRA,
					IE_ACAO_EXCESSO,
					DS_ERRO,
					NR_SEQ_REGRA,
					DT_ATUALIZACAO,
					NM_USUARIO,
					qt_conta,
					cd_setor_atendimento)
				values	(nr_interno_conta_p,
					cd_procedimento_w, 
					ie_origem_proced_w,
					qt_permitida_w,
					null,
					ds_erro_w,
					nr_seq_regra_w,
					sysdate,
					nm_usuario_p,
					qt_executada_atend_w,
					cd_setor_atendimento_w);
			end if;

		end if;

	end loop;
	close c02;
end loop;
close c01;

commit;

end;
/
