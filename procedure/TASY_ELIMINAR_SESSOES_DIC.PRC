create or replace
procedure tasy_eliminar_sessoes_dic is

sid_w		number(10,0);
serial_w	number(10,0);

Cursor C01 is
	select	sid,
		serial#
	from	GV$session
	where	upper(module) like '%CORSIS_FN';

begin

open C01;
loop
fetch C01 into	
	sid_w,
	serial_w;
exit when C01%notfound;
	begin
	--execute immediate ds_comando_w;	
	exec_sql_dinamico_bv('','begin TASY_KILL_SESSION('||sid_w||','||serial_w||'); end;','');
	end;
end loop;
close C01;

end tasy_eliminar_sessoes_dic;
/