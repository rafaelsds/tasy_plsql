create or replace
procedure gerar_evolucao_remocao (	nr_seq_remocao_p	number,
					ie_tipo_evolucao_p	varchar2,
					nr_atendimento_p	number,
					cd_profissional_p	varchar2,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2) is 

ds_patologia_w		varchar2(254);
cd_evolucao_w		number(10);
ie_ja_possui_evolucao_w	varchar2(1) := 'N';
begin

select	ds_patologia
into	ds_patologia_w
from	eme_regulacao
where	nr_sequencia = nr_seq_remocao_p;

select 	decode(count(*),0,'N','S')
into	ie_ja_possui_evolucao_w
from	evolucao_paciente
where	nr_seq_remocao = nr_seq_remocao_p;

select	evolucao_paciente_seq.nextval
into	cd_evolucao_w
from	dual;

if ( ie_ja_possui_evolucao_w = 'N') then
	insert into evolucao_paciente (	cd_evolucao,
					dt_evolucao,
					ie_tipo_evolucao,
					cd_pessoa_fisica,
					dt_atualizacao,
					nm_usuario,
					ds_evolucao,
					dt_liberacao,
					ie_situacao,
					cd_medico,
					nr_atendimento,
					nr_seq_remocao)
	values 				(cd_evolucao_w,
					sysdate,
					ie_tipo_evolucao_p,
					cd_pessoa_fisica_p,
					sysdate,
					nm_usuario_p,
					ds_patologia_w,
					sysdate,
					'A',
					cd_profissional_p,
					nr_atendimento_p,
					nr_seq_remocao_p);
else
update 	evolucao_paciente
set	ds_evolucao    = ds_patologia_w,
	dt_atualizacao = sysdate
where	nr_seq_remocao = nr_seq_remocao_p;
end if;
commit;

end gerar_evolucao_remocao;
/
