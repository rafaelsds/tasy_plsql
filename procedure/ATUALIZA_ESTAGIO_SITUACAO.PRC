create or replace
procedure atualiza_estagio_situacao(
			nr_sequencia_p number,
			nr_seq_estagio_p varchar,
			nm_usuario_p varchar2) is 

begin

if (nr_sequencia_p is not null) then
	begin
	update 	man_ordem_servico 
	set 	nr_seq_estagio = nr_seq_estagio_p, 
		dt_atualizacao = sysdate, 
		nm_usuario = nm_usuario_p
	where 	nr_sequencia = nr_sequencia_p;
	commit;
	end;	
end if;
end atualiza_estagio_situacao;
/