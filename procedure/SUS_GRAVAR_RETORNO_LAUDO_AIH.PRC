create or replace procedure sus_gravar_retorno_laudo_aih(cd_laudo_internacao_p	in VARCHAR2,
								cd_motivo_p	in VARCHAR2,
								ds_justificativa_p	in VARCHAR2,
								ds_motivo_p	in VARCHAR2,
								ds_situacao_p	in VARCHAR2,
								nm_paciente_p	in VARCHAR2,
								nm_usuario_p	in VARCHAR2,
								nr_seq_laudo_pac_p	in NUMBER,
								tp_situacao_p	in VARCHAR2) is

begin

  insert into sus_retorno_laudo_aih_ws (
    cd_laudo_internacao,
    cd_motivo,
    ds_justificativa,
    ds_motivo,
    ds_situacao,
    dt_atualizacao,
    nm_paciente,
    nm_usuario,
    nr_seq_laudo_pac,
    tp_situacao,
    nr_sequencia)
  values (cd_laudo_internacao_p,
    cd_motivo_p,
    ds_justificativa_p,
    ds_motivo_p,
    ds_situacao_p,
    sysdate,
    nm_paciente_p,
    nm_usuario_p,
    nr_seq_laudo_pac_p,
    tp_situacao_p,
    sus_retorno_laudo_aih_ws_seq.nextval);

end sus_gravar_retorno_laudo_aih;
/