/*A tabela abaixo foi criada somente na nossa base e na base do cliente

create table heal_gera_rel_saldo_estoque(
	CD_LOCAL_ESTOQUE                                   NUMBER(4),
	CD_MATERIAL_COMERCIAL                             NUMBER(6),
	QT_ESTOQUE                                         NUMBER(13,4));
*/

create or replace
procedure heal_relatorio_saldo_estoque(	cd_estabelecimento_p		number,
					dt_mesano_referencia_p		date) is 

cd_material_comercial_w			number(6);
cd_local_estoque_w			number(4);
qt_estoque_w				number(13,4);
					
cursor c01 is
select	a.cd_local_estoque,
	a.cd_material,
	a.qt_estoque
from	saldo_estoque a
where	a.cd_estabelecimento = cd_estabelecimento_p
and	a.dt_mesano_referencia = dt_mesano_referencia_p
and	obter_tipo_material(cd_material, 'C') = 2;

cursor c02 is
select	distinct
	b.cd_local_estoque,
	a.cd_material_generico,
	b.qt_estoque
from	material a,
	saldo_estoque b
where	a.cd_material = b.cd_material
and	a.cd_material_generico is not null
and	b.cd_estabelecimento = cd_estabelecimento_p
and	b.dt_mesano_referencia = dt_mesano_referencia_p
and	obter_tipo_material(a.cd_material_generico, 'C') = 2
and not exists (
	select	1
	from	heal_gera_rel_saldo_estoque x
	where	a.cd_material_generico = x.cd_material_comercial);
					
begin

delete from heal_gera_rel_saldo_estoque;

open C01;
loop
fetch C01 into	
	cd_local_estoque_w,
	cd_material_comercial_w,
	qt_estoque_w;
exit when C01%notfound;
	begin
	insert into heal_gera_rel_saldo_estoque(
		cd_local_estoque,
		cd_material_comercial,
		qt_estoque)
	values	(cd_local_estoque_w,
		cd_material_comercial_w,
		qt_estoque_w);
	end;
end loop;
close C01;


open C02;
loop
fetch C02 into	
	cd_local_estoque_w,
	cd_material_comercial_w,
	qt_estoque_w;
exit when C02%notfound;
	begin
	insert into heal_gera_rel_saldo_estoque(
		cd_local_estoque,
		cd_material_comercial,
		qt_estoque)
	values	(cd_local_estoque_w,
		cd_material_comercial_w,
		qt_estoque_w);
	end;
end loop;
close C02;

commit;

end heal_relatorio_saldo_estoque;
/