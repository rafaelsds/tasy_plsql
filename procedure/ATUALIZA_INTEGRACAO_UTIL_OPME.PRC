create or replace
procedure atualiza_integracao_util_opme(	ie_integracao_util_p	varchar2,
						nr_seq_agenda_p		number,
						cd_material_p		number,
						nm_usuario_p		varchar2) is 
begin
if 	(ie_integracao_util_p is not null) and
	(nr_seq_agenda_p is not null) and
	(cd_material_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update 	agenda_pac_opme 
	set 	ie_integracao_util = ie_integracao_util_p,
		dt_atualizacao 	= sysdate,
		nm_usuario 	= nm_usuario_p
	where 	nr_seq_agenda = nr_seq_agenda_p
	and 	cd_material = cd_material_p;
	end;
end if;
commit;

end atualiza_integracao_util_opme;
/