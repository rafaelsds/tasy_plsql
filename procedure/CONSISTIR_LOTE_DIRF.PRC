create or replace
procedure consistir_lote_dirf
			(	nr_sequencia_p	in	number,
				retorno_p	out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Consistir para que n�o seja permitido gerar o lote caso o m�s j� possui um lote gerado
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
cd_estabelecimento_w		dirf_lote_mensal.cd_estabelecimento%type;
qt_registros_w			number(10);
dt_mes_referencia_w		date;
dt_mes_referencia_fm_w		date;	

begin
select	a.dt_mes_referencia,
	a.cd_estabelecimento
into	dt_mes_referencia_w,
	cd_estabelecimento_w
from	dirf_lote_mensal	a
where	a.nr_sequencia	= nr_sequencia_p;

dt_mes_referencia_fm_w	:= fim_dia(fim_mes(PKG_DATE_UTILS.ADD_MONTH(dt_mes_referencia_w, -1, 0)));
dt_mes_referencia_w	:= trunc(dt_mes_referencia_w,'yyyy');

select	count(1)
into	qt_registros_w
from	dirf_lote_mensal	a
where	cd_estabelecimento	= cd_estabelecimento_w
and	dt_geracao is null
and	a.dt_mes_referencia between dt_mes_referencia_w and dt_mes_referencia_fm_w
and	rownum			= 1;

if	(qt_registros_w > 0) then
	retorno_p	:= 'F';
else
	retorno_p	:= 'V';
end if;

end consistir_lote_dirf;
/