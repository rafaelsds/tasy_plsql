create or replace
procedure cml_gravar_prospect_origem(
			nr_seq_origem_p		Number,
			nr_seq_prospect_p		Varchar2) as

begin

update	cml_prospect 
set 	nr_seq_origem 	= nr_seq_origem_p
where 	nr_sequencia 	= nr_seq_prospect_p;

commit;

end cml_gravar_prospect_origem;
/