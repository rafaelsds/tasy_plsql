create or replace
procedure atualiza_usuario_aval_clicp(	nr_sequencia_p		number,
										nm_usuario_p		varchar2) is
				
begin
	
	update med_avaliacao_paciente set nm_usuario = nm_usuario_p, dt_atualizacao = sysdate where nr_sequencia = nr_sequencia_p;

commit;

end atualiza_usuario_aval_clicp;
/
