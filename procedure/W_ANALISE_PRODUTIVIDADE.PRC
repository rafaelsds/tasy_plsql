create or replace
procedure w_analise_produtividade(
			dt_inicio_p		date,
			dt_fim_p		date,
			nr_seq_grupo_des_p	number,
			nr_seq_gerencia_p	number,
			nm_usuario_p		varchar2) is 

/*
create table w_produtividade_exec(
	nr_sequencia 	number(10),
	dt_atividade	date,
	nm_usuario_exec 	varchar2(15),
	nr_seq_ordem 	number(10),
	qt_minuto		number(10),
	qt_minuto_ret	number(10),
	nm_usuario	varchar2(15),
	ie_tipo		varchar2(15));
*/			

i			number(2);
nr_ult_dia_w		number(2);

dt_referencia_w		date;

dt_inicio_w		date;
dt_fim_w		date;

nm_usuario_exec_w	varchar2(15);
nr_seq_ordem_w		number(10);
qt_minuto_aux_w		number(10);
qt_minuto_w		number(10);
qt_minuto_ret_w		number(10);
dt_atividade_w		date;

ie_retorno_w		varchar2(1);
ie_desenv_w		varchar2(1);
nr_sequencia_w		number(10);
qt_reg_w			number(5);
nr_seq_estagio_w		number(10);
ie_tipo_w			varchar2(15);

cursor c01 is
select	a.nm_usuario_exec,
	a.nr_seq_ordem_serv,
	nvl(a.qt_minuto,0),
	a.dt_atividade
from	man_ordem_serv_ativ a,
	man_ordem_servico_v b
where	a.nr_seq_ordem_serv = b.nr_sequencia
and	(nr_seq_grupo_des_p is null or b.nr_seq_grupo_des = nr_seq_grupo_des_p)
and	(nr_seq_gerencia_p is null or b.nr_seq_gerencia_des = nr_seq_gerencia_p)
and	dt_atividade between dt_inicio_w and dt_fim_w;

begin
dt_referencia_w		:= trunc(dt_inicio_p,'dd');
nr_ult_dia_w		:= trunc(dt_fim_p,'dd') - trunc(dt_inicio_p,'dd');

if	(dt_fim_p < dt_inicio_p) then
	raise_application_error(-20011,'A data final n�o pode ser menor que a data inicial!');
end if;

delete	w_produtividade_exec
where	nm_usuario = nm_usuario_p;

nr_sequencia_w := 0;

for i in 0..(nr_ult_dia_w-1) loop
	begin
	dt_inicio_w 		:= dt_referencia_w + i;
	dt_fim_w		:= fim_dia(dt_inicio_w);
	
	open c01;
	loop
	fetch c01 into	
		nm_usuario_exec_w,
		nr_seq_ordem_w,
		qt_minuto_aux_w,
		dt_atividade_w;
	exit when c01%notfound;
		begin
		qt_minuto_w	:= 0;
		qt_minuto_ret_w	:= 0;
		
		select	nvl(max('S'),'N')
		into	ie_retorno_w
		from	man_ordem_serv_estagio a			
		where	a.nr_seq_estagio in (2,791)
		and	not exists(	select 1
				from	usuario_grupo_sup x
				where	x.nm_usuario_grupo = a.nm_usuario)
		and	a.dt_atualizacao < dt_atividade_w
		and	a.nr_seq_ordem = nr_seq_ordem_w;
		
		if	(ie_retorno_w = 'S') then
			qt_minuto_ret_w	:= qt_minuto_aux_w;
		else
			qt_minuto_w	:= qt_minuto_aux_w;
		end if;
		
		update	w_produtividade_exec
		set	qt_minuto		= qt_minuto + qt_minuto_w,
			qt_minuto_ret	= qt_minuto_ret + qt_minuto_ret_w
		where	nm_usuario	= nm_usuario_p
		and	dt_atividade	= dt_inicio_w
		and	nr_seq_ordem	= nr_seq_ordem_w;
		
		if	(sql%notfound) then
			begin
			select	nvl(max('S'),'N')
			into	ie_retorno_w
			from	man_ordem_serv_estagio a			
			where	a.nr_seq_estagio in (2,791)
			and	not exists(	select	1
					from	usuario_grupo_sup x
					where	x.nm_usuario_grupo = a.nm_usuario)
			and	a.dt_atualizacao < dt_inicio_w
			and	a.nr_seq_ordem = nr_seq_ordem_w;
			
			select	max(nr_seq_estagio)
			into	nr_seq_estagio_w
			from	man_ordem_serv_estagio a
			where	a.nr_sequencia = (	select	max(x.nr_sequencia)
						from	man_ordem_serv_estagio x
						where	x.nr_seq_ordem = a.nr_seq_ordem
						and	x.dt_atualizacao between dt_inicio_w and dt_fim_w)
			and	a.nr_seq_ordem = nr_seq_ordem_w;
			
			select	nvl(max(ie_desenv),'S')
			into	ie_desenv_w
			from	man_estagio_processo
			where	nr_sequencia = nr_seq_estagio_w;
			
			if	(ie_desenv_w = 'N') and (ie_retorno_w = 'S') then
				ie_tipo_w := 'CTR';
			elsif	(ie_desenv_w = 'N') and (ie_retorno_w = 'N') then
				ie_tipo_w := 'CTN';
			elsif	(ie_desenv_w = 'S') and (ie_retorno_w = 'N') then
				ie_tipo_w := 'PN';
			elsif	(ie_desenv_w = 'S') and (ie_retorno_w = 'S') then
				ie_tipo_w := 'PR';
			else	
				ie_tipo_w := 'P';
			end if;
			
			nr_sequencia_w	:= nr_sequencia_w + 1;
			
			insert into w_produtividade_exec(
				nr_sequencia,
				dt_atividade,
				nm_usuario_exec,
				nr_seq_ordem,
				qt_minuto,
				qt_minuto_ret,
				nm_usuario,
				ie_tipo)
			values (	nr_sequencia_w,
				dt_inicio_w,
				nm_usuario_exec_w,
				nr_seq_ordem_w,
				qt_minuto_w,
				qt_minuto_ret_w,
				nm_usuario_p,
				ie_tipo_w);
			end;
		end if;						
		end;
	end loop;
	close c01;
	end;
end loop;

commit;

end w_analise_produtividade;
/