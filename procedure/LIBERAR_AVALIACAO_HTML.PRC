create or replace
procedure liberar_avaliacao_html(
		nr_seq_avaliacao_p	number,
		nm_usuario_p		varchar2) is 

ie_possui_aval_w 	varchar2(2);

begin

select 	nvl(max('S'),'N') 
into 	ie_possui_aval_w
from 	med_avaliacao_result 
where 	nr_seq_avaliacao = nr_seq_avaliacao_p;

if (ie_possui_aval_w = 'N') then
	Wheb_mensagem_pck.exibir_mensagem_abort(881277); -- Conferir o preenchimento da avalia��o.
end if;

Liberar_avaliacao(nr_seq_avaliacao_p,nm_usuario_p);

end liberar_avaliacao_html;
/