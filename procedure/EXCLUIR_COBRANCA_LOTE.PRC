create or replace
procedure excluir_cobranca_lote(
				nr_sequencia_p		Varchar2,
				ie_tipo_p 		varchar2,
				nm_usuario_p		varchar2,
				ie_tipo_lote_p		varchar2) is 

nr_titulo_w 		titulo_receber_orgao_cobr.nr_titulo%type;
nr_seq_cheque_w		cheque_cr_orgao_cobr.nr_seq_cheque%type;
ds_historico_w		varchar2(255);
nr_seq_lote_tit_w	titulo_receber_orgao_cobr.nr_seq_lote%type;	
nr_seq_lote_cheque_w	cheque_cr_orgao_cobr.nr_seq_lote%type;
nr_seq_lote_outros_w	outros_orgao_cobr.nr_seq_lote%type;
vl_acobrar_w		cobranca.vl_acobrar%type;
dt_aprovacao_w		date;
		
begin

if	(ie_tipo_p = 'T') then

	select	nr_titulo,
		nvl(nr_seq_lote,nr_seq_lote_exc)
	into 	nr_titulo_w,
		nr_seq_lote_tit_w
	from	titulo_receber_orgao_cobr
	where	nr_sequencia = nr_sequencia_p;
	
	select 	dt_aprovacao
	into	dt_aprovacao_w
	from 	lote_orgao_cobranca
	where	nr_sequencia = nr_seq_lote_tit_w;
	
	if (dt_aprovacao_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(675005);
	end if;

	if	(ie_tipo_lote_p = 'I') then
		update	titulo_receber_orgao_cobr
		set	nr_seq_regra = null,
			nr_Seq_lote = null
		where	nr_sequencia = nr_sequencia_p; 
	elsif	(ie_tipo_lote_p = 'E') then
		update	titulo_receber_orgao_cobr
		set	nr_seq_regra = null,
			nr_Seq_lote_exc = null
		where	nr_sequencia = nr_sequencia_p; 
	end if;
	
	ds_historico_w := wheb_mensagem_pck.get_texto(337827, 'NM_USUARIO=' || nm_usuario_p || ';NR_TITULO=' || nr_titulo_w || ';DT_ATUALIZACAO=' || to_char(sysdate,'dd/MM/yyyy hh24:mi:ss'));
	gerar_historico_cobr_orgao(null,null,nr_seq_lote_tit_w,ds_historico_w,nm_usuario_p,'E');
	
elsif	(ie_tipo_p = 'C') then
	
	select	nr_seq_cheque,
		nvl(nr_seq_lote,nr_seq_lote_exc)
	into 	nr_seq_cheque_w,
		nr_seq_lote_cheque_w
	from	cheque_cr_orgao_cobr
	where	nr_sequencia = nr_sequencia_p;
	
	select 	dt_aprovacao
	into	dt_aprovacao_w
	from 	lote_orgao_cobranca
	where	nr_sequencia = nr_seq_lote_cheque_w;	
	
	if (dt_aprovacao_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(675005);
	end if;	
	
	if	(ie_tipo_lote_p = 'I') then
		update	cheque_cr_orgao_cobr
		set	nr_seq_regra = null,
			nr_Seq_lote = null
		where	nr_sequencia = nr_sequencia_p; 
	elsif	(ie_tipo_lote_p = 'E') then
		update	cheque_cr_orgao_cobr
		set	nr_seq_regra = null,
			nr_Seq_lote_exc = null
		where	nr_sequencia = nr_sequencia_p; 
	end if;

	ds_historico_w := wheb_mensagem_pck.get_texto(337826, 'NM_USUARIO=' || nm_usuario_p || ';NR_SEQ_CHEQUE=' || nr_seq_cheque_w ||  ';DT_ATUALIZACAO=' || to_char(sysdate,'dd/MM/yyyy hh24:mi:ss'));
	gerar_historico_cobr_orgao(null,null,nr_seq_lote_cheque_w,ds_historico_w,nm_usuario_p,'E');
	
elsif	(ie_tipo_p = 'O') then
	
	select	nvl(a.nr_seq_lote,a.nr_seq_lote_exc),
		b.vl_acobrar
	into	nr_seq_lote_outros_w,
		vl_acobrar_w
	from	outros_orgao_cobr a,
		cobranca b
	where	a.nr_seq_cobranca = b.nr_sequencia
	and 	a.nr_sequencia = nr_sequencia_p;
	
	select 	dt_aprovacao
	into	dt_aprovacao_w
	from 	lote_orgao_cobranca
	where	nr_sequencia = nr_seq_lote_outros_w;		
	
	if (dt_aprovacao_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(675005);
	end if;		
	
	if	(ie_tipo_lote_p = 'I') then
		update	outros_orgao_cobr
		set	nr_seq_regra = null,
			nr_Seq_lote = null
		where	nr_sequencia = nr_sequencia_p; 
	elsif	(ie_tipo_lote_p = 'E') then
		update	outros_orgao_cobr
		set	nr_seq_regra = null,
			nr_Seq_lote_exc = null
		where	nr_sequencia = nr_sequencia_p; 
	end if;

	ds_historico_w := wheb_mensagem_pck.get_texto(337828, 'NM_USUARIO=' || nm_usuario_p ||  ';VL_TOTAL=' || vl_acobrar_w || ';DT_ATUALIZACAO=' || to_char(sysdate,'dd/MM/yyyy hh24:mi:ss'));
	gerar_historico_cobr_orgao(null,null,nr_seq_lote_outros_w,ds_historico_w,nm_usuario_p,'E');
	
end if;

commit;

end excluir_cobranca_lote;
/
