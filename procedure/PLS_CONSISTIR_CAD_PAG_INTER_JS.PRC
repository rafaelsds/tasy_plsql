/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Consistir o cadastro do pagador no Java Swing (OPS - Contratos de Interc�mbio)
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_consistir_cad_pag_inter_js
			(	nr_seq_pagador_p		number,
				nr_seq_intercambio_p		number,
				ie_tipo_pagador_p		varchar2,
				ie_calc_primeira_mens_p		varchar2,
				ie_calculo_proporcional_p	varchar2,
				cd_pessoa_fisica_p		varchar2,
				cd_cgc_p			varchar2,
				ie_acao_executada_p		number,
				ie_erro_abort_p		out	varchar2,
				ds_erro_p		out	varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
				
nr_seq_pagador_aux_w		number(10);
cd_pf_cgc_aux_w			varchar2(20);
ie_consistir_enderec_w		varchar2(20);
ds_erro_w			varchar2(255);

begin

ie_erro_abort_p	:= 'S';

if	(ie_acao_executada_p in (1,2)) then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_pagador_aux_w
	from	pls_contrato_pagador
	where	ie_tipo_pagador	= 'P'
	and	nr_sequencia	<> nr_seq_pagador_p
	and	nr_seq_pagador_intercambio = nr_seq_intercambio_p;

	/*Deve-se informar um respons�vel pagador principal para este contrato. Verifique!*/
	if	(ie_tipo_pagador_p <> 'P') and
		(nr_seq_pagador_aux_w = 0) then
		ds_erro_w	:= substr(obter_texto_dic_objeto(26598,wheb_usuario_pck.GET_NR_SEQ_IDIOMA,''),1,255);
		goto final;
	end if;
	
	/*J� existe um respons�vel pagador principal para este contrato. Verifique!*/
	if	(ie_tipo_pagador_p = 'P') and
		(nr_seq_pagador_aux_w > 0) then
		ds_erro_w	:= substr(obter_texto_dic_objeto(26599,wheb_usuario_pck.GET_NR_SEQ_IDIOMA,''),1,255);
		goto final;
	end if;
	
	/*Deve ser informado a data de refer�ncia para o c�lculo proporcional. Verifique!*/
	if	(ie_calc_primeira_mens_p = 'P') and
		(ie_calculo_proporcional_p is null) then
		ds_erro_w	:= substr(obter_texto_dic_objeto(241335,wheb_usuario_pck.GET_NR_SEQ_IDIOMA,''),1,255);
		goto final;
	end if;
end if;

/*Informe um pagador v�lido!*/
if	(cd_pessoa_fisica_p is null) and
	(cd_cgc_p is null) then
	ds_erro_w	:= substr(obter_texto_dic_objeto(26600,wheb_usuario_pck.GET_NR_SEQ_IDIOMA,''),1,255);
	goto final;
end if;

/*Informe apenas um pagador!*/
if	(cd_pessoa_fisica_p is not null) and
	(cd_cgc_p is not null) then
	ds_erro_w	:= substr(obter_texto_dic_objeto(26601,wheb_usuario_pck.GET_NR_SEQ_IDIOMA,''),1,255);
	goto final;
end if;

begin
select	nvl(cd_pessoa_fisica,cd_cgc)
into	cd_pf_cgc_aux_w
from	pls_contrato_pagador
where	nr_sequencia		<> nr_seq_pagador_p
and	nr_seq_pagador_intercambio	= nr_seq_intercambio_p
and	(cd_pessoa_fisica	= cd_pessoa_fisica_p
or	cd_cgc			= cd_cgc_p);
exception
when others then
	cd_pf_cgc_aux_w	:= '';
end;

/*Aten��o! Pagador j� cadastrado*/
if	(cd_pf_cgc_aux_w is not null) then
	ds_erro_w	:= substr(obter_texto_dic_objeto(26651,wheb_usuario_pck.GET_NR_SEQ_IDIOMA,''),1,255);
	goto final;
end if;

<<final>>
ds_erro_p	:= ds_erro_w;

end pls_consistir_cad_pag_inter_js;
/
