create or replace
procedure grava_dados_item_nf(	
			nr_seq_nota_p			number,
			nr_seq_item_p			number,
			nr_seq_cnae_p			varchar2,
			nr_seq_classif_trib_p 		varchar2,
			ie_todos_p			varchar2) is

begin


if	(ie_todos_p = 'S') then
	begin
	update	nota_fiscal_item
	set	nr_seq_classif_trib	= nr_seq_classif_trib_p,
		nr_seq_cnae	        = nr_seq_cnae_p
	where	nr_sequencia		= nr_seq_nota_p
	and	nr_seq_classif_trib	is null
	and	nr_seq_cnae		is null;
	end;
else
	begin
	update	nota_fiscal_item
	set	nr_seq_classif_trib	= nr_seq_classif_trib_p,
		nr_seq_cnae	        = nr_seq_cnae_p
	where	nr_item_nf		= nr_seq_item_p
	and	nr_sequencia		= nr_seq_nota_p;
	end;
end if;	

commit;

end grava_dados_item_nf;
/