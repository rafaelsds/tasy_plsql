create or replace
procedure ptu_gerar_pls_procedimento
			(	nr_seq_conta_p			number,
				nr_seq_cobranca_p		number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Varchar2) is 

nm_prestador_w			varchar2(230);
cd_prestador_w			varchar2(20);
ie_tipo_participacao_w		varchar2(10);
ie_tipo_guia_w			varchar2(2);
ie_material_intercambio_w	varchar2(2);
vl_adic_procedimento_w		number(15,2);
vl_adic_co_w			number(15,2);
vl_adic_filme_w			number(15,2);
vl_participante_w		number(15,2);
vl_procedimento_w		number(12,2);
vl_custo_operacional_w		number(12,2);
cd_procedimento_w		number(10);
cd_material_imp_w		number(10);
cd_procedimento_imp_w		number(10);
qt_servico_w			number(10);
nr_sequencia_w			number(10);
nr_seq_material_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_nota_servico_w		number(10);
qt_internacao_w			number(10);
nr_seq_prestador_w		number(10);
nr_seq_grau_participacao_w	number(10);
nr_seq_proc_participante_w	number(10);
nr_seq_conta_proc_nota_w	number(10);
qt_medico_w			number(10);
nr_seq_nota_servico_ww		number(10);
qt_material_w			number(10);
nr_seq_conta_mat_w		number(10);
nr_seq_conta_proc_w		number(10);
tx_procedimento_w		number(9,2);
qt_procedimento_w		pls_conta_proc_v.qt_procedimento%type;
ie_via_acesso_w			number(2);
ds_via_acesso_w			varchar2(1);
ie_tipo_procedimento_w		number(2);
ie_tipo_despesa_w		number(1);
dt_procedimento_w		date;
tx_intercambio_imp_w		Number(7,4);
cd_servico_w			Number(10);
nr_cgc_cpf_w			varchar2(14);
nr_cgc_cpf_req_w		varchar2(14);
ie_tipo_pessoa_prestador_w	varchar2(1);
ie_proc_do_prestador_w		varchar2(1);
vl_filme_w			Number(15,2);
vl_proc_apresentado_ww		Number(15,2);
vl_procedimento_uni_w		Number(15,2);
dt_inicio_proc_w		date;
ds_hora_procedimento_w		varchar2(8);
cd_porte_anestesico_w		varchar2(1);
vl_total_proc_w			number(15,2);
ds_servico_w			varchar2(80);
ie_pacote_w			Varchar2(1);
ie_ato_cooperado_w		pls_conta_proc.ie_ato_cooperado%type;
ie_alto_custo_w			ptu_nota_servico.ie_alto_custo%type;
dt_fim_proc_w			date;
hr_final_w			varchar2(8);
cd_ref_material_fab_w		ptu_nota_servico.cd_ref_material_fab%type;
nr_seq_prest_inter_w		pls_proc_participante.nr_seq_prest_inter%type;
cd_cgc_prestador_w		varchar2(14);
nr_cpf_prestador_w		varchar2(11);
cd_munic_w			ptu_nota_servico.cd_munic%type;	
nr_seq_cong_prot_w		pls_protocolo_conta.nr_sequencia%type;
nr_seq_congenere_sup_w		pls_congenere.nr_seq_congenere%type;
nr_seq_guia_w			pls_conta_proc.nr_seq_guia%type;
nr_autorizacao_w		ptu_nota_servico.nr_autorizacao%type;
nr_seq_segurado_w		pls_conta.nr_seq_segurado%type;
nr_cbo_exec_w			ptu_nota_servico.nr_cbo_exec%type;
tp_rede_min_w			pls_conta_proc_regra.tp_rede_min%type;
tx_procedimento_regra_w		ptu_nota_servico.tx_procedimento%type;
ie_tx_procedimento_w		pls_parametros.ie_tx_procedimento%type;
cd_munic_cont_exec_w		ptu_nota_cobranca.cd_munic_cont_exec%type;
unidade_medida_w		ptu_nota_servico.unidade_medida%type;
cd_unidade_medida_w		tiss_unidade_medida.cd_unidade_medida%type;
nr_seq_medida_tiss_w		unidade_medida.nr_seq_medida_tiss%type;
tipo_rede_min_w			ptu_nota_cobranca.tipo_rede_min%type;
id_aviso_item_w			ptu_nota_servico.id_aviso_item%type;
ie_origem_conta_w		pls_conta.ie_origem_conta%type;
vl_apresentado_a500_w		pls_conta_proc.vl_procedimento_imp%type;
id_acres_urg_emer_w		ptu_nota_servico.id_acres_urg_emer%type;

/* Servi?os */
cursor C01 is
	select	ie_tipo_tabela,
		cd_procedimento,
		ie_via_acesso,	--decode(ie_via_acesso,'D',2,'M',1,'U',0),
		dt_procedimento,
		qt_procedimento,
		nvl(vl_adic_procedimento,0),
		nvl(vl_custo_operacional,0),
		ie_origem_proced,
		nvl(vl_procedimento,0),
		nvl(vl_filme,0),
		nvl(vl_adic_co,0),
		nvl(vl_adic_filme,0),
		nr_seq_material,
		cd_servico,
		nr_sequencia,
		nr_cgc_cpf,
		nr_cgc_cpf_req,
		ie_tipo_pessoa_prestador,
		ie_tipo_participacao,
		nvl(cd_cnes_prest, cd_prestador),
		nvl(nm_profissional_prestador, nm_prestador),
		ds_hora_procedimento,
		cd_porte_anestesico,
		ds_servico,
		ie_pacote,
		cd_ato,
		nvl(ie_alto_custo,'N'),
		hr_final,
		cd_ref_material_fab,
		cd_munic,
		nr_autorizacao,
		nr_cbo_exec,
		tipo_rede_min,
		tx_procedimento,
		unidade_medida,
		nvl(id_aviso_item, 'N') id_aviso_item,
		nvl(vl_procedimento,0) + nvl(vl_custo_operacional,0) + nvl(vl_filme,0) + nvl(vl_adic_procedimento,0) + nvl(vl_adic_co,0) + nvl(vl_adic_filme,0) vl_apresentado_a500,
		id_acres_urg_emer
	from	ptu_nota_servico
	where	nr_seq_nota_cobr		= nr_seq_cobranca_p
	and nvl(ie_pacote,'N') = 'N';--itens que vem marcados como pacote, devem 

begin

select 	nvl(max(ie_material_intercambio),'S'),
	nvl(max(ie_tx_procedimento), 'N')
into	ie_material_intercambio_w,
	ie_tx_procedimento_w
from 	pls_parametros
where 	cd_estabelecimento = cd_estabelecimento_p;	

select	max(nr_seq_congenere_prot),
	max(nr_seq_segurado),
	max(ie_origem_conta)
into	nr_seq_cong_prot_w,
	nr_seq_segurado_w,
	ie_origem_conta_w
from	pls_conta_v
where	nr_sequencia = nr_seq_conta_p;

select	cd_munic_cont_exec,
	tipo_rede_min
into	cd_munic_cont_exec_w,
	tipo_rede_min_w
from	ptu_nota_cobranca
where	nr_sequencia = nr_seq_cobranca_p;

open C01;
loop
fetch C01 into
	ie_tipo_despesa_w,--ie_tipo_procedimento_w,
	cd_procedimento_w,
	ie_via_acesso_w,
	dt_procedimento_w,
	qt_procedimento_w,
	vl_adic_procedimento_w,
	vl_custo_operacional_w,
	ie_origem_proced_w,
	vl_procedimento_w,
	vl_filme_w,
	vl_adic_co_w,
	vl_adic_filme_w,
	nr_seq_material_w,
	cd_servico_w,
	nr_seq_nota_servico_w,
	nr_cgc_cpf_w,
	nr_cgc_cpf_req_w,
	ie_tipo_pessoa_prestador_w,
	ie_tipo_participacao_w,
	cd_prestador_w,
	nm_prestador_w,
	ds_hora_procedimento_w,
	cd_porte_anestesico_w,
	ds_servico_w,
	ie_pacote_w,
	ie_ato_cooperado_w,
	ie_alto_custo_w,
	hr_final_w,
	cd_ref_material_fab_w,
	cd_munic_w,
	nr_autorizacao_w,
	nr_cbo_exec_w,
	tp_rede_min_w,
	tx_procedimento_regra_w,
	unidade_medida_w,
	id_aviso_item_w,
	vl_apresentado_a500_w,
	id_acres_urg_emer_w;
exit when C01%notfound;
	begin
	/* Efetua a soma dos valores - OS 416758 */
	vl_total_proc_w	:= nvl(vl_procedimento_w,0) + nvl(vl_filme_w,0) + nvl(vl_custo_operacional_w,0);
		
	tx_intercambio_imp_w	:= dividir_sem_round(((vl_adic_procedimento_w + vl_adic_co_w + vl_adic_filme_w) * 100),vl_total_proc_w);
	
	/*Robson da Silva OS - 376887*/
	
	vl_proc_apresentado_ww 	:= vl_procedimento_w + vl_adic_procedimento_w + vl_custo_operacional_w + vl_adic_co_w + vl_filme_w + vl_adic_filme_w;
	
	vl_procedimento_uni_w	:= dividir_sem_round(vl_proc_apresentado_ww, qt_procedimento_w);
	
	
	if	(ie_tx_procedimento_w = 'N') then
		tx_procedimento_regra_w := null;
	end if;
	
	if	(cd_munic_w is null) then
		cd_munic_w := cd_munic_cont_exec_w;
	end if;
	
	begin
	dt_inicio_proc_w := to_date(to_char(dt_procedimento_w,'dd/mm/yyyy')||' '||ds_hora_procedimento_w, 'dd/mm/yyyy hh24:mi:ss');
	exception
	when others then
		dt_inicio_proc_w	:= null;
	end;
	
	begin
	dt_fim_proc_w	:= to_date(to_char(dt_procedimento_w,'dd/mm/yyyy')||' '||hr_final_w, 'dd/mm/yyyy hh24:mi:ss');
	exception
	when others then
		dt_fim_proc_w		:= null;
	end;
	
	nr_seq_guia_w	:= null;

	if	(ie_tipo_despesa_w in (0,1,4)) or
		((nr_seq_material_w is null) and 
		(cd_procedimento_w is not null)) then
		
		-- caso n?o encontre vai tentar buscar do PTU pelo numero da conta
		select	max(e.nr_seq_guia)
		into	nr_seq_guia_w
		from	ptu_nota_servico 		c,
			ptu_resposta_autorizacao	d,
			ptu_nota_cobranca		b,
			pls_execucao_requisicao 	e,
			ptu_fatura 			a
		where	c.nr_sequencia			= nr_seq_nota_servico_w
		and	d.nr_seq_origem			= c.nr_autorizacao
		and	b.nr_sequencia			= c.nr_seq_nota_cobr
		and	e.nr_seq_requisicao		= d.nr_seq_requisicao
		and	a.cd_unimed_origem		= d.cd_unimed_executora
		and	a.nr_sequencia			= b.nr_seq_fatura;
		
		-- se tiver a informa??o da congenere ent?o busca a guia pela senha.
		if	(nr_seq_cong_prot_w is not null and nr_seq_guia_w is null) then
		
			-- as senha interna ou externa da autoriza??o devem ser iguais a da conta.
			select	max(x.nr_sequencia)
			into	nr_seq_guia_w
			from	(
				select	a.nr_sequencia
				from	pls_guia_plano a
				where	a.nr_seq_segurado = nr_seq_segurado_w
				and	a.nr_seq_uni_exec = nr_seq_cong_prot_w
				and	a.cd_senha = to_char(nr_autorizacao_w)
				and	a.ie_status in ('1', '2')
				union all
				select	a.nr_sequencia
				from	pls_guia_plano a
				where	a.nr_seq_segurado = nr_seq_segurado_w
				and	a.nr_seq_uni_exec = nr_seq_cong_prot_w
				and	a.cd_senha_externa = to_char(nr_autorizacao_w)
				and	a.ie_status in ('1', '2')
				) x;
			
			-- Tratamento inclu?do devido ao fato de a senha da autoriza??o, n?o ser igual ao n?mero da resposta ptu
			if	(nr_seq_guia_w is null) then
				select	max(guia.nr_sequencia)
				into	nr_seq_guia_w
				from	pls_guia_plano guia,
					ptu_resposta_autorizacao resposta
				where	guia.nr_seq_segurado = nr_seq_segurado_w
				and	guia.nr_seq_uni_exec = nr_seq_cong_prot_w
				and	guia.ie_status in ('1', '2')
				and	resposta.nr_seq_guia = guia.nr_sequencia
				and	resposta.nr_seq_origem = nr_autorizacao_w;

				-- Tratamento inclu?do devido ao fato que a operadora do A500 n?o ser a mesma da guia do SCS
				if	(nr_seq_guia_w is null) then
					
					select	max(resposta.nr_seq_guia)
					into	nr_seq_guia_w
					from	ptu_resposta_autorizacao resposta       
					where	resposta.nr_seq_origem = nr_autorizacao_w
					and	exists (select 1
							from   pls_guia_plano guia
							where  guia.nr_seq_segurado = nr_seq_segurado_w
							and    guia.ie_status in ('1', '2')
							and    guia.nr_sequencia = resposta.nr_seq_guia);
					
					-- Verifica se a operadora superior da guia ? a mesma do protocolo do A500
					if	(nr_seq_guia_w is not null) then
					
						select	max(a.nr_seq_congenere)
						into	nr_seq_congenere_sup_w
						from	pls_guia_plano b,
							pls_congenere a
						where	b.nr_sequencia = nr_seq_guia_w
						and	a.nr_sequencia = b.nr_seq_uni_exec;

						if	(nr_seq_congenere_sup_w <> nr_seq_cong_prot_w) then
							nr_seq_guia_w	:= null;
						end if;	
					end if;
				end if;
			end if;
		end if;
		
		select	decode(ie_via_acesso_w,2,'D',1,'M',0,'U')
		into	ds_via_acesso_w
		from	dual;
		
		if	(ds_via_acesso_w in ('D','M','U') ) then
			select 	obter_tx_proc_via_acesso(ds_via_acesso_w)
			into	tx_procedimento_w
			from 	dual;
		else
			tx_procedimento_w := 100;
		end if;
		
		select	count(1)
		into	qt_servico_w
		from	procedimento
		where	cd_procedimento = cd_procedimento_w;
		
		if	(ie_tipo_despesa_w not in (0,1,4)) then
			ie_tipo_despesa_w := null;
		end if;
		
		select	decode(ie_tipo_despesa_w,0,1,1,2,ie_tipo_despesa_w)
		into	ie_tipo_despesa_w
		from	dual;
		
		cd_procedimento_imp_w		:= cd_procedimento_w;
		
		if	(qt_servico_w = 0) then
			update	ptu_nota_servico
			set	cd_procedimento  = null,
				ie_origem_proced = null
			where	nr_sequencia	 = nr_seq_nota_servico_w;
		
			cd_procedimento_w	:= null;
			ie_origem_proced_w	:= null;
		end if;
		
		
		
		

		insert into pls_conta_proc
			(nr_sequencia, nm_usuario, dt_atualizacao,
			nm_usuario_nrec, dt_atualizacao_nrec, cd_procedimento,
			ie_via_acesso, dt_procedimento, tx_item,
			qt_procedimento, ie_status, 
			vl_procedimento_imp, ie_tipo_despesa, nr_seq_conta, 
			ie_situacao, vl_unitario_imp, qt_procedimento_imp, 
			ie_origem_proced, vl_adic_procedimento, vl_adic_co, 
			vl_adic_materiais, cd_procedimento_imp, tx_intercambio_imp,
			vl_taxa_co_imp, vl_taxa_servico_imp, vl_taxa_material_imp,
			vl_procedimento_ptu_imp, vl_co_ptu_imp, vl_material_ptu_imp,
			dt_inicio_proc,cd_porte_anestesico_imp,ds_procedimento_imp,
			ie_ato_cooperado, ie_pacote_ptu,qt_ptu_item, 
			vl_apresentado_xml, qt_procedimento_original, ie_alto_custo,
			dt_fim_proc,nr_seq_guia)
		values	(pls_conta_proc_seq.nextval, nm_usuario_p, sysdate,
			nm_usuario_p, sysdate, cd_procedimento_w,
			ds_via_acesso_w, dt_procedimento_w, tx_procedimento_w,
			qt_procedimento_w, 'U', 
			vl_proc_apresentado_ww, ie_tipo_despesa_w, nr_seq_conta_p, 
			'B', vl_procedimento_uni_w, qt_procedimento_w,
			ie_origem_proced_w, vl_adic_procedimento_w, vl_adic_co_w,
			vl_adic_filme_w, cd_servico_w, tx_intercambio_imp_w,
			vl_adic_co_w, vl_adic_procedimento_w, vl_adic_filme_w,
			vl_procedimento_w, vl_custo_operacional_w, vl_filme_w,
			dt_inicio_proc_w,cd_porte_anestesico_w,ds_servico_w,
			ie_ato_cooperado_w, ie_pacote_w,qt_procedimento_w,
			vl_proc_apresentado_ww, qt_procedimento_w, ie_alto_custo_w,
			dt_fim_proc_w,nr_seq_guia_w) returning nr_sequencia into nr_seq_conta_proc_w;
		
		update	ptu_nota_servico
		set	nr_seq_conta_proc = nr_seq_conta_proc_w
		where	nr_sequencia	  = nr_seq_nota_servico_w;
		
		insert into pls_conta_proc_regra (	nr_sequencia, dt_atualizacao, nm_usuario,
							tp_rede_min, tx_item, ie_a520,
							ie_acres_urg_emer) 
					values  (	nr_seq_conta_proc_w, sysdate, nm_usuario_p,
							nvl(tp_rede_min_w,tipo_rede_min_w), tx_procedimento_regra_w, id_aviso_item_w,
							id_acres_urg_emer_w);
		
		
		select	max(nr_sequencia)
		into	nr_seq_grau_participacao_w
		from	pls_grau_participacao
		where	cd_ptu		= ie_tipo_participacao_w
		and	ie_situacao = 'A';

		if	(nr_seq_grau_participacao_w is null) then
			select	max(nr_sequencia)
			into	nr_seq_grau_participacao_w
			from	pls_grau_participacao
			where	cd_ptu		= ie_tipo_participacao_w;
		end if;
		
		if	(nr_seq_conta_proc_w is not null) then
			
			if	(ie_tipo_pessoa_prestador_w = 'J') then
				cd_cgc_prestador_w		:= nr_cgc_cpf_w;
				nr_cpf_prestador_w		:= null;
			elsif	(ie_tipo_pessoa_prestador_w = 'F') then
				cd_cgc_prestador_w		:= null;
				nr_cpf_prestador_w		:= substr(nr_cgc_cpf_w,4,11);
			end if;
			
			pls_gerar_prest_intercambio(	nr_cpf_prestador_w,	cd_cgc_prestador_w,	nm_prestador_w,
							cd_prestador_w,		cd_munic_w,		nm_usuario_p,
							null,			nr_cbo_exec_w,		nr_seq_prest_inter_w,	
							nr_seq_prestador_w);
				
			insert	into pls_proc_participante
				(nr_sequencia,			dt_atualizacao,		nm_usuario,
				dt_atualizacao_nrec,		nm_usuario_nrec,	nr_seq_conta_proc,
				nr_seq_grau_partic,		vl_apresentado,		nm_medico_executor_imp,
				ie_status,			nr_seq_prest_inter)
			values	(pls_proc_participante_seq.nextval,	sysdate,		nm_usuario_p,
				sysdate,			nm_usuario_p,		nr_seq_conta_proc_w,
				nr_seq_grau_participacao_w,	vl_procedimento_w,	decode(nvl(nm_prestador_w,'X'),'X',null,cd_prestador_w || ' - ' || nm_prestador_w),
				'U',				nr_seq_prest_inter_w) returning nr_sequencia into nr_seq_proc_participante_w;
			
			update	ptu_nota_servico
			set	nr_seq_proc_partic	= nr_seq_proc_participante_w
			where	nr_sequencia		= nr_seq_nota_servico_ww;
		end if;
		
	elsif	(ie_tipo_despesa_w in (2,3,5,6)) then
		
		select	decode(ie_tipo_despesa_w,3,2,2,3,5,2,6,3)
		into	ie_tipo_despesa_w
		from	dual;
		
		select	max(nr_seq_medida_tiss)
		into	nr_seq_medida_tiss_w
		from	unidade_medida
		where	nr_unidade_ptu = unidade_medida_w;
		
		cd_unidade_medida_w := null;
		
		if	(nr_seq_medida_tiss_w is not null) then
			select	max(cd_unidade_medida)
			into	cd_unidade_medida_w
			from	tiss_unidade_medida
			where	nr_sequencia = nr_seq_medida_tiss_w;
		end if;
		
				
		
		insert into pls_conta_mat
			(nr_sequencia, nm_usuario, dt_atualizacao,
			nm_usuario_nrec, dt_atualizacao_nrec, nr_seq_material,
			dt_atendimento, tx_reducao_acrescimo, qt_material, 
			vl_material_imp, ie_status, nr_seq_conta,
			ie_situacao, vl_unitario_imp, qt_material_imp,
			ie_tipo_despesa, vl_taxa_material_imp, cd_material_imp,
			dt_inicio_atend,ds_material_imp, ie_ato_cooperado,
			tx_intercambio_imp, ie_pacote_ptu, ie_alto_custo,
			dt_fim_atend, vl_material_imp_xml, cd_ref_fabricante,
			cd_unidade_medida)
		values	(pls_conta_mat_seq.nextval, nm_usuario_p, sysdate,
			nm_usuario_p, sysdate, nr_seq_material_w, 
			dt_procedimento_w, tx_procedimento_w, qt_procedimento_w, 
			vl_proc_apresentado_ww, 'U', nr_seq_conta_p,
			'I', vl_procedimento_uni_w, qt_procedimento_w,
			ie_tipo_despesa_w, vl_adic_procedimento_w, cd_servico_w,
			dt_inicio_proc_w,ds_servico_w, ie_ato_cooperado_w,
			tx_intercambio_imp_w, ie_pacote_w, ie_alto_custo_w,
			dt_fim_proc_w, vl_proc_apresentado_ww, cd_ref_material_fab_w,
			cd_unidade_medida_w) returning nr_sequencia into nr_seq_conta_mat_w;
		
		insert into pls_conta_mat_regra (	nr_sequencia, dt_atualizacao, nm_usuario,
							tp_rede_min, ie_a520, ie_acres_urg_emer) 
					values  (	nr_seq_conta_mat_w, sysdate, nm_usuario_p,
							nvl(tp_rede_min_w,tipo_rede_min_w), id_aviso_item_w, id_acres_urg_emer_w);
							
		update	ptu_nota_servico
		set	nr_seq_conta_mat		= nr_seq_conta_mat_w
		where	nr_sequencia			= nr_seq_nota_servico_w;
		
	end if;

	end;
end loop;
close C01;

pls_cta_proc_mat_regra_pck.gera_seq_tiss_conta_proc(nr_seq_conta_p, nm_usuario_p);
pls_cta_proc_mat_regra_pck.gera_seq_tiss_conta_mat(nr_seq_conta_p, nm_usuario_p);


end ptu_gerar_pls_procedimento;
/
