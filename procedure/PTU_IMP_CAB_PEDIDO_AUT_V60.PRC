create or replace
procedure ptu_imp_cab_pedido_aut_v60( 	dt_atendimento_p		varchar2,
					ie_tipo_cliente_p		ptu_pedido_autorizacao.ie_tipo_cliente%type,
					cd_doenca_cid_p			ptu_pedido_autorizacao.cd_doenca_cid%type,
					ie_rede_min_p			ptu_pedido_autorizacao.ie_tipo_rede_min%type,
					ie_alto_custo_p			ptu_pedido_autorizacao.ie_alto_custo%type,
					cd_unimed_prestador_req_p	ptu_pedido_autorizacao.cd_unimed_prestador_req%type,
					nr_seq_prestador_req_p		ptu_pedido_autorizacao.nr_seq_prestador_req%type,
					cd_especialidade_p		ptu_pedido_autorizacao.cd_especialidade%type,
					ie_urg_emerg_p			ptu_pedido_autorizacao.ie_urg_emerg%type,
					cd_unimed_executora_p		ptu_pedido_autorizacao.cd_unimed_executora%type,
					cd_unimed_beneficiario_p	ptu_pedido_autorizacao.cd_unimed_beneficiario%type,
					cd_unimed_p			ptu_pedido_autorizacao.cd_unimed%type,
					nr_via_cartao_p			ptu_pedido_autorizacao.nr_via_cartao%type,
					nr_seq_execucao_p		ptu_pedido_autorizacao.nr_seq_execucao%type,
					cd_usuario_plano_p		ptu_pedido_autorizacao.cd_usuario_plano%type,
					ds_biometria_p			ptu_pedido_autorizacao.ds_biometria%type,
					cd_unimed_prestador_p		ptu_pedido_autorizacao.cd_unimed_prestador%type,
					cd_transacao_p			ptu_pedido_autorizacao.cd_transacao%type,
					nr_seq_prest_alto_custo_p	ptu_pedido_autorizacao.nr_seq_prest_alto_custo%type,
					nm_prestador_alto_custo_p	ptu_pedido_autorizacao.nm_prestador_alto_custo%type,
					cd_versao_p			ptu_pedido_autorizacao.nr_versao%type,
					ie_recem_nascido_p		ptu_pedido_autorizacao.ie_recem_nascido%type,
					ie_tipo_internacao_p		ptu_pedido_autorizacao.ie_tipo_internacao%type,
					ie_tipo_acidente_p		ptu_pedido_autorizacao.ie_tipo_acidente%type,
					nm_prof_solicitante_p		ptu_pedido_autorizacao.nm_prof_solicitante%type,
					nr_fone_prof_solic_p		ptu_pedido_autorizacao.nr_fone_prof_solic%type,	
					ds_email_prof_solic_p		ptu_pedido_autorizacao.ds_email_prof_solic%type,
					cd_unimed_atendimento_p		ptu_pedido_autorizacao.cd_unimed_atendimento%type,
					ie_anexo_p			ptu_pedido_autorizacao.ie_anexo%type,
					ie_sexo_p			ptu_pedido_autorizacao.ie_sexo%type,
					nr_idade_p			ptu_pedido_autorizacao.nr_idade%type,
					dt_sug_internacao_p		varchar2,
					ie_ordem_servico_p		ptu_pedido_autorizacao.ie_ordem_servico%type,
					nr_seq_ordem_p			ptu_pedido_autorizacao.nr_seq_ordem%type,
					cd_versao_tiss_p		ptu_pedido_autorizacao.nr_versao_tiss%type,
					ds_observacao_p			ptu_pedido_autorizacao.ds_observacao%type,
					ds_ind_clinica_p		ptu_pedido_autorizacao.ds_ind_clinica%type,
					ie_liminar_p			ptu_pedido_autorizacao.ie_liminar%type,
					cd_ibge_p			ptu_pedido_autorizacao.cd_municipio_ibge%type,
					nm_usuario_p			usuario.nm_usuario%type,
					nr_seq_pedido_aut_p  out 	ptu_pedido_autorizacao.nr_sequencia%type,
					ie_possui_registro_p out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a importa��o do arquivo de 00600 - Pedido de Autoriza��o do PTU 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_cliente_w	ptu_pedido_autorizacao.ie_tipo_cliente%type;
nr_seq_pedido_existe_w	ptu_pedido_autorizacao.nr_sequencia%type;
dt_atendimento_w	ptu_pedido_autorizacao.dt_atendimento%type;
dt_sug_internacao_w	ptu_pedido_autorizacao.dt_sug_internacao%type;

begin

if	(nvl(dt_atendimento_p,'X')	<> 'X') then
	dt_atendimento_w	:= to_date(dt_atendimento_p, 'dd/mm/rrrr');
end if;

if	(nvl(dt_sug_internacao_p,'X')	<> 'X') then
	dt_sug_internacao_w	:= to_date(dt_sug_internacao_p, 'dd/mm/rrrr');
end if;

--Select utilizado para verificar se j� existe um pedido de autoriza��o igual na base, se existir s� retorna a sequencia da resposta do pedido
--nr_ident_exec_w ptu:n�meroTransa��oPrestadora - N�mero da transa��o da Unimed Prestadora
--cd_unimed_exec_w ptu:codigoUnimedPrestadora - C�digo da Unimed Prestadora 
select	max(a.nr_sequencia)
into	nr_seq_pedido_existe_w
from	ptu_pedido_autorizacao a,
	ptu_controle_execucao b
where	b.nr_seq_pedido_aut	= a.nr_sequencia
and	a.nr_seq_execucao	= nr_seq_execucao_p 
and	a.cd_unimed_executora	= cd_unimed_executora_p;

ie_possui_registro_p := 'N';

--Se existir o pedido na base retorna a sequ�ncia
if	( nr_seq_pedido_existe_w is not null ) then
	nr_seq_pedido_aut_p 	:= nr_seq_pedido_existe_w;
	ie_possui_registro_p 	:= 'S';
else
	-- Valida��o � necess�rio converter os dados para  somente uma letra 
	ie_tipo_cliente_w := ptu_converter_tipo_cliente(ie_tipo_cliente_p);


	insert	into ptu_pedido_autorizacao
		(nr_sequencia, dt_atendimento, ie_tipo_cliente,
		cd_doenca_cid, ie_alto_custo, cd_unimed_prestador_req,
		nr_seq_prestador_req, cd_especialidade, ie_urg_emerg,
		cd_unimed_executora, cd_unimed_beneficiario, cd_unimed,
		nr_via_cartao, nr_seq_execucao, nm_usuario,
		dt_atualizacao, cd_usuario_plano, nr_seq_prest_alto_custo,
		cd_unimed_prestador, cd_transacao,
		nm_prestador_alto_custo, nr_versao, nm_usuario_nrec,
		dt_atualizacao_nrec, ie_recem_nascido, ie_tipo_internacao,
		ie_tipo_acidente, nm_prof_solicitante, nr_fone_prof_solic,
		ds_email_prof_solic, cd_unimed_atendimento, ie_anexo,
		ie_sexo, nr_idade, dt_sug_internacao,
		ie_ordem_servico, nr_seq_ordem, nr_versao_tiss,
		ds_observacao, ds_ind_clinica, ds_biometria,
		ie_liminar, cd_municipio_ibge, ie_tipo_rede_min)
	values	(ptu_pedido_autorizacao_seq.nextval, dt_atendimento_w, ie_tipo_cliente_w,
		cd_doenca_cid_p, ie_alto_custo_p, cd_unimed_prestador_req_p,
		nr_seq_prestador_req_p, cd_especialidade_p, ie_urg_emerg_p,
		cd_unimed_executora_p, cd_unimed_beneficiario_p, cd_unimed_p,
		nvl(nr_via_cartao_p,1), nr_seq_execucao_p, nm_usuario_p,
		sysdate, lpad(cd_usuario_plano_p,13,'0'), nr_seq_prest_alto_custo_p,
		cd_unimed_prestador_p, cd_transacao_p,
		nm_prestador_alto_custo_p, cd_versao_p, nm_usuario_p,
		sysdate, ie_recem_nascido_p, ie_tipo_internacao_p,
		ie_tipo_acidente_p, nm_prof_solicitante_p, nr_fone_prof_solic_p,
		substr(ds_email_prof_solic_p,1,60), cd_unimed_atendimento_p, ie_anexo_p,
		ie_sexo_p, nr_idade_p, dt_sug_internacao_w,
		ie_ordem_servico_p, nr_seq_ordem_p, cd_versao_tiss_p,
		ds_observacao_p, ds_ind_clinica_p, ds_biometria_p,
		ie_liminar_p, cd_ibge_p, ie_rede_min_p) returning nr_sequencia into nr_seq_pedido_aut_p;

	insert	into ptu_controle_execucao
		(nr_sequencia, dt_atualizacao, nm_usuario,
		nr_seq_pedido_compl, nr_seq_pedido_aut, nm_usuario_nrec,
		dt_atualizacao_nrec)
	values	(ptu_controle_execucao_seq.nextval, sysdate, nm_usuario_p,
		null, nr_seq_pedido_aut_p, nm_usuario_p,
		sysdate);
	
	commit;
end if;	

end ptu_imp_cab_pedido_aut_v60;
/
