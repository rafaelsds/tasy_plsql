create or replace
procedure   Gerar_taxa_mat_conta( 
				nr_prescricao_p		number,
				nr_seq_material_p	number,
				nm_usuario_p		Varchar2,
				ie_gerar_estornar_p	varchar2,
				ie_cig_ccg_p		varchar2) is 

cd_proc_adic_w			number(15,0);
ie_orig_proced_adic_w		number(10,0);
qt_proc_adic_w			number(15,4);
nr_seq_exame_w			number(10,0);
nr_seq_material_w		number(10,0);
ds_observacao_w			varchar2(255);
cd_setor_atendimento_w		number(5,0);
cd_setor_prescr_w		number(5,0);
nr_agrup_acum_w			number(10,0);
nr_seq_acum_w			number(10,0);
cd_estabelecimento_w		number(10,0);
nr_seq_proc_interno_w		number(10,0);
nr_atendimento_w		number(15,0);
ie_tipo_atendimento_w		number(15,0);
cont_w				number(15,0);
ie_agrupador_w			number(15,0);

dt_prescricao_w			date;
cd_material_w			number(6,0);
nr_dia_util_w			number(6,0);
ie_via_aplicacao_w		varchar2(5);
ie_considerar_dia_w		varchar2(5);
cd_material_exame_w		varchar2(20);
cd_intervalo_w			varchar2(7);
ds_horarios_w			varchar2(2000);
nr_ocorrencia_w			number(15,4);
cd_perfil_w			number(15,0);
cd_convenio_w			number(15,0);
ie_acm_w			varchar2(1);
ie_sn_w				varchar2(1);
ie_agora_w			varchar2(1);
ie_tipo_item_w			varchar2(1);

cursor c01 is
select	cd_proc_adic,
		ie_orig_proced_adic,
		qt_proc_adic,
		nr_seq_exame,
		nr_seq_material,
		ds_observacao,
		cd_setor_atendimento,
		nr_seq_proc_interno,
		ie_considerar_dia
from		regra_material_proced a
where	nvl(cd_material,cd_material_w)	= cd_material_w
and		((cd_setor_prescr is null) or 
		 (cd_setor_prescr = cd_setor_prescr_w))
and		((ie_tipo_atendimento is null) or 
		 (ie_tipo_atendimento = ie_tipo_atendimento_w))
and		((nr_dia_util is null) or (nr_dia_util >= nr_dia_util_w))
and		((cd_estab is null) or 
		 (cd_Estab = cd_estabelecimento_w))
and		((cd_convenio is null) or 
		 (cd_convenio = cd_convenio_w))
and		((cd_perfil is null) or 
		 (cd_perfil = cd_perfil_w))
and		(((nvl(ie_forma,'R')	= 'A') and (ie_cig_ccg_p = 'C')) or
		 ((nvl(ie_forma,'R')	= 'G') and (ie_cig_ccg_p = 'G')))
and		not exists (	select	1
					from		regra_material_proced_exc x
					where	x.nr_seq_regra	= a.nr_sequencia
					and		x.cd_material	= cd_material_w)
and 		not exists(	select	1
					from		regra_material_proced_exc x
					where	x.nr_seq_regra	= a.nr_sequencia
					and		nvl(x.ie_impede_lancamento,'N') = 'S'
					and		x.cd_material	in 	(select	y.cd_material 
												 from	prescr_material y
												 where	y.nr_prescricao = nr_prescricao_p))
and		nvl(ie_via_aplicacao,nvl(ie_via_aplicacao_w,'X')) = nvl(ie_via_aplicacao_w,'X')
and		((cd_perfil = cd_perfil_w) or (cd_perfil is null))
and		((cd_setor_atendimento = cd_setor_prescr_w) or (cd_setor_atendimento is null));

begin

select	count(*)
into		cont_w
from		regra_material_proced
where	nvl(ie_forma,'R') in('A','G');

if	(cont_w > 0) then

	select	max(dt_prescricao),
			max(cd_estabelecimento),
			max(cd_setor_atendimento),
			max(nr_atendimento),
			max(obter_convenio_atendimento(nr_atendimento))
	into		dt_prescricao_w,
			cd_estabelecimento_w,
			cd_setor_prescr_w,
			nr_atendimento_w,
			cd_convenio_w
	from		prescr_medica
	where	nr_prescricao	= nr_prescricao_p;
	
	cd_perfil_w	:= obter_perfil_ativo;
	
	select	max(ie_tipo_atendimento)
	into		ie_tipo_atendimento_w
	from		atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;

	select	nvl(max(cd_material),0),
			nvl(max(ie_via_aplicacao),'X'),
			nvl(max(cd_intervalo),'XPTO'),
			max(ds_horarios),
			nvl(max(nr_ocorrencia),0),
			nvl(max(ie_acm),'N'),
			nvl(max(ie_se_necessario),'N'),
			nvl(max(ie_urgencia),'N'),
			nvl(max(ie_agrupador),0),
			nvl(max(NR_DIA_UTIL),0)
	into		cd_material_w,
			ie_via_aplicacao_w,
			cd_intervalo_w,
			ds_horarios_w,
			nr_ocorrencia_w,
			ie_acm_w,
			ie_sn_w,
			ie_agora_w,
			ie_agrupador_w,
			nr_dia_util_w
	from		prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and		nr_sequencia	= nr_seq_material_p;
	
	ie_tipo_item_w	:= 'M';
	if	(ie_agrupador_w = 4) then
		ie_tipo_item_w	:= 'S';
	end if;

	open C01;
	loop
	fetch C01 into	
		cd_proc_adic_w,
		ie_orig_proced_adic_w,
		qt_proc_adic_w,
		nr_seq_exame_w,
		nr_seq_material_w,
		ds_observacao_w,
		cd_setor_atendimento_w,
		nr_seq_proc_interno_w,
		ie_considerar_dia_w;
	exit when C01%notfound;
	
		select	max(cd_material_exame)
		into		cd_material_exame_w
		from		material_exame_lab
		where	nr_sequencia	= nr_seq_material_w;
		
		if	(nr_seq_proc_interno_w is not null) then
			Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,nr_prescricao_p,nr_atendimento_w, 0, cd_proc_adic_w, ie_orig_proced_adic_w,null,null);
		end if;
		cont_w	:= 0;
		if	(ie_considerar_dia_w = 'S') and
			(ie_gerar_estornar_p = 'G') then
			select	count(*)
			into		cont_w
			from		procedimento_paciente a,
					conta_paciente b
			where	a.nr_interno_conta	= b.nr_interno_conta
			and		b.nr_atendimento	= nr_atendimento_w
			and		a.cd_procedimento	= cd_proc_adic_w
			--and		trunc(a.dt_conta,'dd')	= trunc(sysdate,'dd')
			and		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(a.dt_conta)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
			and		a.ie_origem_proced	= ie_orig_proced_adic_w;
		end if;
		
		if	(ie_gerar_estornar_p = 'E') then
			qt_proc_adic_w	:= qt_proc_adic_w * -1;
		end if;
		
		if	(cont_w = 0) then
			gerar_proc_pac_item_prescr(nr_prescricao_p, null,nr_atendimento_w,null,nr_seq_proc_interno_w, cd_proc_adic_w, ie_orig_proced_adic_w, qt_proc_adic_w, cd_setor_atendimento_w,1, SYSDATE, nm_usuario_p, NULL, nr_seq_exame_w, NULL,null);
		end if;
	end loop;
	close C01;

	commit;
	
end if;

end Gerar_taxa_mat_conta;
/
