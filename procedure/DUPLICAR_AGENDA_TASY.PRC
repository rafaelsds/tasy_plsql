create or replace
procedure duplicar_agenda_tasy(nr_seq_agenda_origem_p number, qt_dias_repet_p number,dt_fim_repeticao_p date) is 

/*
nr_seq_agenda_origem_p - Passar a sequencia da agenda que ir� originar a repeti��o
qt_dias_repet_p	  - Passar o intervalo em dias para repetir a agenda (Ex: A cada sete dias, passar valor 7
dt_fim_repeticao_p	 - Passar a data fim para repetir a agenda. 

*/

nr_w number(10);
dt_agenda_w date;

Cursor C01 is
	select *
	from	agenda_tasy
	where	nr_sequencia = nr_seq_agenda_origem_p;

agenda_tasy_w	c01%rowtype;

begin

open C01;
loop
fetch C01 into	
	agenda_tasy_w;
exit when C01%notfound;
	begin
	nr_w := 0;
	end;
end loop;
close C01;

dt_agenda_w := agenda_tasy_w.dt_agenda;

while(dt_agenda_w < dt_fim_repeticao_p) loop
	begin
	dt_agenda_w := dt_agenda_w + qt_dias_repet_p;
	insert into agenda_tasy(
		nr_sequencia,
		nm_usuario_agenda,
		dt_agenda,
		nr_minuto_duracao,
		nr_seq_tipo,
		dt_atualizacao,
		nm_usuario,
		ds_agenda,
		ds_local,
		ds_observacao,
		ie_status,
		dt_final,
		nr_seq_local,
		nr_seq_agenda_convite
	) values (
		agenda_tasy_seq.nextval,
		agenda_tasy_w.nm_usuario_agenda,
		dt_agenda_w,
		agenda_tasy_w.nr_minuto_duracao,
		agenda_tasy_w.nr_seq_tipo,
		agenda_tasy_w.dt_atualizacao,
		agenda_tasy_w.nm_usuario,
		agenda_tasy_w.ds_agenda,
		agenda_tasy_w.ds_local,
		agenda_tasy_w.ds_observacao,
		agenda_tasy_w.ie_status,
		agenda_tasy_w.dt_final,
		agenda_tasy_w.nr_seq_local,
		agenda_tasy_w.nr_seq_agenda_convite
	);
	
	end;
end loop;

commit;

end duplicar_agenda_tasy;
/