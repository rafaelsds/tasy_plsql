create or replace
procedure excluir_bloqueio_agendas(	dt_inicial_p		date,
				dt_final_p			date,
				hr_inicial_p		date,
				hr_final_p			date,
				ie_motivo_bloqueio_p 	varchar2)
				is
begin

delete	from agenda_bloqueio
where	TRUNC(dt_inicial)			=	TRUNC(dt_inicial_p)
and	TRUNC(dt_final)			=	TRUNC(dt_final_p)
and	((hr_inicio_bloqueio			=	hr_inicial_p) or (hr_inicio_bloqueio is null and hr_inicial_p is null))
and	((hr_final_bloqueio			=	hr_final_p) or (hr_final_bloqueio is null and hr_final_p is null))
and	ie_motivo_bloqueio			=	ie_motivo_bloqueio_p
and	obter_tipo_agenda(cd_agenda) 	= 1;

commit;

end excluir_bloqueio_agendas;
/