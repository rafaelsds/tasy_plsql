create or replace
procedure recalcular_adiantamento_conta	( nr_interno_conta_p	number,
					  nm_usuario_p		Varchar2) is 


vl_total_conta_w	number(15,2);
nr_adiantamento_w	conta_paciente_adiant.nr_adiantamento%type;
nr_sequencia_w		conta_paciente_adiant.nr_sequencia%type;
vl_vinculado_w		conta_paciente_adiant.vl_adiantamento%type;
vl_saldo_w		adiantamento.vl_saldo%type;
vl_a_vincular_w		conta_paciente_adiant.vl_adiantamento%type;

Cursor C01 is
	select	a.nr_adiantamento,
		a.nr_sequencia,
		nvl(a.vl_adiantamento,0),
		nvl(b.vl_saldo,0)
	from	conta_paciente_adiant a,
		adiantamento b
	where	a.nr_interno_conta 	= nr_interno_conta_p
	and	a.nr_adiantamento	= b.nr_adiantamento
	order by 	a.vl_adiantamento,
			a.nr_adiantamento asc;
begin

select	nvl(obter_valor_conta_imposto(nr_interno_conta_p,4),0)
into	vl_total_conta_w
from	dual;

open C01;
loop
fetch C01 into	
	nr_adiantamento_w,
	nr_sequencia_w,
	vl_vinculado_w, --valor ja vinculado a conta
	vl_saldo_w; --vl saldo do adto. Quando vincula nao interfere no saldo ainda
exit when C01%notfound;
	begin

	if (vl_total_conta_w > 0) then
	
		if (vl_saldo_w > vl_total_conta_w) then
			vl_a_vincular_w := vl_total_conta_w;
		else
			vl_a_vincular_w := vl_saldo_w;
		end if;
		
		update	conta_paciente_adiant
		set	vl_adiantamento 	= vl_a_vincular_w,
			dt_atualizacao		= sysdate
		where	nr_interno_conta 	= nr_interno_conta_p
		and	nr_adiantamento 	= nr_adiantamento_w
		and	nr_sequencia		= nr_sequencia_w;
	
		vl_total_conta_w := vl_total_conta_w - vl_a_vincular_w;
		
	elsif (vl_total_conta_w = 0) then --Se tiver adto vinculado que supere o valor da conta, precisa vincular 0 para ele, senao pode dar problema quando gerar o titulo e fazer a baixa, pois tentara lancar baixas no titulo com valor superior ao titulo
	
		update	conta_paciente_adiant
		set	vl_adiantamento 	= vl_total_conta_w,
			dt_atualizacao		= sysdate
		where	nr_interno_conta 	= nr_interno_conta_p
		and	nr_adiantamento 	= nr_adiantamento_w
		and	nr_sequencia		= nr_sequencia_w;
	
	end if;
	
	end;
end loop;
close C01;

--commit; Sem commit, o commit ocorrera na rotina recalcular_conta_paciente que chama esta.

end recalcular_adiantamento_conta;
/
