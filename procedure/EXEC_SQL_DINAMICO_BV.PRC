CREATE OR REPLACE
PROCEDURE Exec_sql_Dinamico_bv(	ds_identificacao_p	Varchar2,
				ds_comando_p		Varchar2,
				ds_parametros_p		varchar2) IS

/*
Executa o SQL passado como parametro (ds_comando_p) passando os parametros (ds_parametros_p) como bind_variable

Ex: exec_sql_dinamico_bv('','insert into XX (nr,ds) values (:nr:ds)','nr=1;ds=teste;');

*/
ds_module_w	varchar2(100);
nr_seq_log_w	number(15);
c001		Integer;		
retorno_w	Number(10);
ds_erro_w	Varchar2(10000);
dt_aux_w	date;

type lista is RECORD (
	nm VARCHAR2(100),
	vl long );
type myArray is table of lista index by binary_integer;

/*Contem os parametros do SQL*/
ar_parametros_w myArray;

ds_param_atual_w 	VARCHAR2(10000);
ds_parametros_w 	VARCHAR2(10000);
nr_pos_separador_w	NUMBER(15);
qt_parametros_w		NUMBER(15);
qt_contador_w		number(15);

ds_sep_bv_w		varchar2(100);
qt_tam_seq_w		number(10);

BEGIN
	begin

	ds_sep_bv_w := obter_separador_bv;
	if	(instr(ds_parametros_p,ds_sep_bv_w) = 0 ) then
		ds_sep_bv_w := ';';
	end if;

	qt_tam_seq_w := length(ds_sep_bv_w);

	ds_parametros_w    := ds_parametros_p;
	nr_pos_separador_w := instr(ds_parametros_w,ds_sep_bv_w);
	qt_parametros_w	   := 0;
	while	(nr_pos_separador_w > 0 ) loop
		begin
		qt_parametros_w := qt_parametros_w + 1;
		ds_param_atual_w  := substr(ds_parametros_w,1,nr_pos_separador_w-1);
		ds_parametros_w   := substr(ds_parametros_w,nr_pos_separador_w+qt_tam_seq_w,length(ds_parametros_w));
		nr_pos_separador_w := instr(ds_param_atual_w,'=');
		ar_parametros_w(qt_parametros_w).nm := upper(substr(ds_param_atual_w,1,nr_pos_separador_w-1));
		ar_parametros_w(qt_parametros_w).vl := substr(ds_param_atual_w,nr_pos_separador_w+1,length(ds_param_atual_w));
		nr_pos_separador_w := instr(ds_parametros_w,ds_sep_bv_w);
		if	(qt_parametros_w > 1000) then
			nr_pos_separador_w := 0;
		end if;
		end;
	end loop;

	nr_pos_separador_w := instr(ds_parametros_w,'=');
	if	( nr_pos_separador_w > 0 ) then
		qt_parametros_w := qt_parametros_w +1;
		ds_param_atual_w := ds_parametros_w;
		ar_parametros_w(qt_parametros_w).nm := upper(substr(ds_param_atual_w,1,nr_pos_separador_w-1));
		ar_parametros_w(qt_parametros_w).vl := substr(ds_param_atual_w,nr_pos_separador_w+1,length(ds_param_atual_w));		
	end if;

	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ds_comando_p, dbms_sql.Native);
	
	for contador_w in 1..ar_parametros_w.count loop
		if	(ar_parametros_w(contador_w).nm like 'DT_%') then
			dt_aux_w := to_date(ar_parametros_w(contador_w).vl,'dd/mm/yyyy hh24:mi:ss');
			DBMS_SQL.BIND_VARIABLE(C001, ar_parametros_w(contador_w).nm, dt_aux_w);
		else
			DBMS_SQL.BIND_VARIABLE(C001, ar_parametros_w(contador_w).nm, ar_parametros_w(contador_w).vl,32764);
		end if;
	end loop;

	Retorno_w := DBMS_SQL.execute(c001); 
	DBMS_SQL.CLOSE_CURSOR(C001);

	exception
	when others then
		begin
		ds_erro_w	:= SQLERRM(sqlcode);

		tratar_erro_banco(ds_erro_w);

		ds_module_w := wheb_usuario_pck.get_ds_form;

		if	(instr(ds_module_w,'CORSIS_FO_NR_SEQ=') >  0 ) then
			nr_seq_log_w := substr(ds_module_w,18,10);
			GRAVAR_LOG_ATUALIZACAO_ERRO(nr_seq_log_w,ds_comando_p,ds_erro_w,substr(ds_identificacao_p,1,15));
		end if;
		
		DBMS_SQL.CLOSE_CURSOR(C001);
		
		Wheb_mensagem_pck.exibir_mensagem_abort(261640,'RETORNO='||ds_identificacao_p || ';' ||
														'ERRO=' || ds_erro_w);
		
		end;
	end;
end Exec_sql_Dinamico_bv; 
/
