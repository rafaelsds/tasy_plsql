create or replace
procedure gerar_convenio_receb_nf(	nr_seq_nota_fiscal_p	number,
					nr_seq_receb_p		number,
					nm_usuario_p		Varchar2) is 
				 
vl_recebimento_w	number(15,2);
vl_total_vinculadas_w	number(15,2);
vl_total_nota_w		number(13,2);
qt_existe_w		number(10,0) := 0;
ie_vincular_maior_w	varchar2(10) := 'N';
	 
begin

obter_param_usuario(27, 260, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_vincular_maior_w);

begin
select	vl_recebimento
into	vl_recebimento_w
from	convenio_receb
where	nr_sequencia = nr_seq_receb_p;
exception
when others then
	vl_recebimento_w := 0;
end;

begin
select	sum(a.vl_total_nota)
into	vl_total_vinculadas_w
from	nota_fiscal a,
	convenio_receb_nf b
where	a.nr_sequencia = b.nr_seq_nota_fiscal
and	b.nr_seq_receb  = nr_seq_receb_p;
exception
when others then
	vl_total_vinculadas_w := 0;
end;

select	sum(vl_total_nota)
into	vl_total_nota_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nota_fiscal_p;

select	count(*)
into	qt_existe_w
from	convenio_receb_nf
where	nr_seq_nota_fiscal = nr_seq_nota_fiscal_p;


if	(((nvl(vl_total_vinculadas_w,0) + vl_total_nota_w) <= vl_recebimento_w) or (ie_vincular_maior_w = 'S')) and
	(nvl(qt_existe_w,0) = 0) then
	begin
	insert	into	convenio_receb_nf (
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_nota_fiscal,
		nr_seq_receb,
		nr_sequencia) 
	values (
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_nota_fiscal_p,
		nr_seq_receb_p,
		convenio_receb_nf_seq.nextval);
	end;
elsif	((nvl(vl_total_vinculadas_w,0) + vl_total_nota_w) > vl_recebimento_w) and (ie_vincular_maior_w = 'N') then
	Wheb_mensagem_pck.exibir_mensagem_abort(190511,'NR_SEQ_NOTA='||nr_seq_nota_fiscal_p);
end if;


commit;

end gerar_convenio_receb_nf;
/