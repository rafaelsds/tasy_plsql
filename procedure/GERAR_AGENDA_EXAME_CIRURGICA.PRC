create or replace
procedure gerar_agenda_exame_cirurgica (	nr_sequencia_exame_p		number,
											nr_sequencia_cirurgia_p	number,
											nm_usuario_p			varchar2) is


nr_atendimento_w			number(10);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(10);
nr_seq_proc_interno_w		number(10);
cd_pessoa_fisica_w			varchar2(10);
cd_medico_w					varchar2(10);
cd_setor_atendimento_w		number(5);
ie_lado_w					varchar2(1);
cd_medico_solicitante_w		varchar2(10);
cd_convenio_w				number(5,0);
dt_nascimento_w				date;
qt_idade_w					number(3,0);
cd_medico_resp_w			varchar2(10);
cd_categoria_w				varchar2(20);
cd_tipo_acomodacao_w		number(4,0);
cd_usuario_convenio_w		varchar2(30);
dt_validade_carteira_w		date;
ie_tipo_atendimento_w		number(10,0);
qt_idade_mes_w				number(2,0);
cd_plano_w					varchar2(10);
nr_sequencia_proc_w			number(10,0);
ie_status_w					varchar2(10);
qt_procedimento_w			number(8,3);


cursor c01 is
	select 		nr_seq_proc_interno, 
				cd_procedimento, 
				ie_origem_proced,
				ie_lado, 
				cd_medico, 
				qt_procedimento
	from		agenda_paciente_proc
	where		nr_sequencia = nr_sequencia_exame_p
	order by	nr_seq_agenda;
		
begin
	
if 	((nr_sequencia_exame_p is not null) and
	 (nr_sequencia_cirurgia_p is not null)) then
	
	select	cd_pessoa_fisica,
			cd_medico,
			cd_setor_atendimento,
			dt_nascimento_pac,
			qt_idade_paciente,	
			qt_idade_mes,
			cd_convenio,
			cd_categoria,
			cd_usuario_convenio,
			dt_validade_carteira,
			cd_tipo_acomodacao,
			ie_status_agenda,
			nr_atendimento,
			ie_tipo_atendimento,
			cd_plano,
			cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno
	into	cd_pessoa_fisica_w,
			cd_medico_w,
			cd_setor_atendimento_w,
			dt_nascimento_w,
			qt_idade_w,
			qt_idade_mes_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_usuario_convenio_w,
			dt_validade_carteira_w,
			cd_tipo_acomodacao_w,
			ie_status_w,
			nr_atendimento_w,
			ie_tipo_atendimento_w,
			cd_plano_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w
	from	agenda_paciente	
	where	nr_sequencia  = nr_sequencia_exame_p;		
			
			
	update	agenda_paciente
	set		cd_pessoa_fisica 		= cd_pessoa_fisica_w,
			cd_medico				= cd_medico_w,
			cd_setor_atendimento	= cd_setor_atendimento_w,
			nm_paciente				= obter_nome_pf(cd_pessoa_fisica_w),
			cd_medico_exec			= cd_medico_w,
			dt_nascimento_pac		= dt_nascimento_w,
			qt_idade_paciente		= qt_idade_w,
			qt_idade_mes			= qt_idade_mes_w,
			nr_telefone				= obter_fone_pac_agenda(cd_pessoa_fisica_w),
			cd_convenio				= cd_convenio_w,
			cd_categoria			= cd_categoria_w,
			cd_usuario_convenio		= cd_usuario_convenio_w,
			dt_validade_carteira	= dt_validade_carteira_w,
			cd_tipo_acomodacao		= cd_tipo_acomodacao_w,
			ie_status_agenda		= ie_status_w,
			nm_usuario_orig			= nm_usuario_p,
			dt_agendamento			= sysdate,
			dt_atualizacao			= sysdate,			
			nr_atendimento			= nr_atendimento_w,
			ie_tipo_atendimento		= ie_tipo_atendimento_w,
			cd_plano				= cd_plano_w,
			cd_procedimento			= cd_procedimento_w,
			ie_origem_proced		= ie_origem_proced_w,
			nr_seq_proc_interno		= nr_seq_proc_interno_w
	where	nr_sequencia			= nr_sequencia_cirurgia_p;
	

	open c01;
	loop
	fetch c01 into	
			nr_seq_proc_interno_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			ie_lado_w,
			cd_medico_solicitante_w,
			qt_procedimento_w;
	exit when c01%notfound;
		begin
		
			/* obter sequ�ncia */
			select	nvl(max(nr_seq_agenda),0) + 1
			into	nr_sequencia_proc_w
			from	agenda_paciente_proc
			where	nr_sequencia = nr_sequencia_cirurgia_p;
			
			insert into agenda_paciente_proc	(	nr_sequencia, 
													nr_seq_agenda, 
													nr_seq_proc_interno, 
													cd_procedimento, 
													ie_origem_proced, 
													ie_lado, 
													cd_medico, 
													dt_atualizacao,
													nm_usuario,
													qt_procedimento) 
			values 								(	nr_sequencia_cirurgia_p, 
													nr_sequencia_proc_w, 
													nr_seq_proc_interno_w,
													cd_procedimento_w,
													ie_origem_proced_w,
													ie_lado_w,
													cd_medico_solicitante_w, 
													sysdate, 
													nm_usuario_p,
													qt_procedimento_w);
		commit;
		end;
	end loop;
	close c01;

end if;

commit;

end gerar_agenda_exame_cirurgica;
/