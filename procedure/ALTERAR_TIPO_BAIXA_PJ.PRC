create or replace
procedure alterar_tipo_baixa_pj(	cd_tipo_baixa_p	number,
				cd_cgc_p	varchar2
) is
begin

update 	pessoa_juridica_estab 
set   	cd_tipo_baixa = cd_tipo_baixa_p
where 	cd_cgc = cd_cgc_p;

commit;
end alterar_tipo_baixa_pj;
/