create or replace
procedure gravar_log_status_laudo( nr_seq_laudo_p	number,
				   nr_seq_status_p	number,
				   nm_usuario_p		Varchar2) is 

qt_laudo_w		number(10);				   
begin

begin

if	(nr_seq_laudo_p is not null) and
	(nr_seq_status_p is not null) then
begin
	insert into laudo_paciente_status_log (nr_sequencia,
						nr_seq_laudo,
						nr_seq_etapa,
						dt_atualizacao_nrec,
						nm_usuario_nrec) values
						(laudo_paciente_status_log_seq.nextval,
						nr_seq_laudo_p,
						nr_seq_status_p,
						sysdate,
						nm_usuario_p);				

	end;
	end if;
	commit;

exception
when others then
	null;
end;


end gravar_log_status_laudo;
/
