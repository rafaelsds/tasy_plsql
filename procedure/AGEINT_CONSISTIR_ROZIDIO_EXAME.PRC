create or replace
procedure Ageint_consistir_rozidio_exame (	nr_seq_item_p		number,
						cd_medico_p		varchar2,
						dt_agenda_p		date,
						cd_agenda_p		number,
						qt_dias_rodizio_p	number,
						cd_estabelecimento_p	number,
						nm_usuario_p		Varchar2,
						ds_erro_p out varchar2	) is 

qt_prof_item_w		number(10,0);
qt_medico_w		number(10,0);
qt_medico_lib_w		number(10,0);
nm_medico_w		varchar2(255);
cd_tipo_agenda_w	Number(10);
ds_medico_w		varchar2(255) := '';
nr_seq_proc_interno_w	number(10);
cd_medico_lib_w		varchar2(10);
qt_livres_w			number(10);
	
--Consultas
Cursor C01 is
	select	distinct cd_pessoa_fisica,
			substr(obter_nome_medico(cd_pessoa_fisica,'N'),1,255)
	from	ageint_lib_usuario
	where	nm_usuario		= nm_usuario_p
	and		nr_seq_ageint_item 	= nr_seq_item_p
	and		cd_agenda		= cd_agenda_p
	and		cd_pessoa_fisica	<> cd_medico_p
	and		Ageint_Obter_Se_Turno_Med_Rod(cd_pessoa_fisica, dt_agenda_p, cd_agenda_p, nm_usuario_p, nr_seq_medico_regra, cd_estabelecimento_p) = 'S'
	order by 1;
	
--Exames	
Cursor C02 is
	select	distinct cd_pessoa_fisica,
			substr(obter_nome_medico(cd_pessoa_fisica,'N'),1,255)			
	from	ageint_lib_usuario
	where	nm_usuario		= nm_usuario_p
	and		nr_seq_ageint_item 	= nr_seq_item_p
	--and	cd_agenda	 	= cd_agenda_p
	and		cd_pessoa_fisica	<> cd_medico_p
	and		Ageint_Obter_Se_Turno_Med_Rod(cd_pessoa_fisica, dt_agenda_p, cd_agenda_p, nm_usuario_p, nr_seq_medico_regra, cd_estabelecimento_p) = 'S'
	order by 1;
	
	
begin

if (nvl(cd_agenda_p,0) > 0) then
	select	max(cd_tipo_agenda)
	into	cd_tipo_agenda_w
	from	agenda
	where	cd_agenda = cd_agenda_p;
end if;

select	count(*)
into	qt_prof_item_w
from	agenda_integrada_prof_item
where	nr_seq_agenda_item = nr_seq_item_p;

select	max(nr_seq_proc_interno)
into	nr_seq_proc_interno_w
from	agenda_integrada_item
where	nr_sequencia	= nr_seq_item_p;


if	(qt_prof_item_w = 0) then	
	if (nvl(cd_tipo_agenda_w,2) = 2) then		
		
		select	count(*) + 1
		into	qt_medico_w
		from	agenda_paciente a,
				agenda_integrada_item b,
				agenda_medico c,
				agenda	d
		where	b.nr_seq_agenda_exame	= a.nr_sequencia
		and		a.cd_medico_exec		= cd_medico_p
		and		a.cd_medico_exec		= c.cd_medico
		and		a.cd_agenda				= c.cd_agenda
		and		a.cd_agenda				= d.cd_agenda
		and		((a.nr_seq_proc_interno	= nr_seq_proc_interno_w) or (nr_seq_proc_interno_w is null))		
		--and	a.cd_agenda		= cd_agenda_p
		and		nvl(d.ie_rodizio_medico,'N') = 'S'
		and		a.dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
		and		a.nm_paciente is not null
		and		a.ie_status_agenda not in ('C','F','B','L');				
		
		open C02;
		loop
		fetch C02 into	
			cd_medico_lib_w,
			nm_medico_w;
		exit when C02%notfound;
			begin
						
			select	count(*)
			into	qt_medico_lib_w
			from	agenda_paciente a,
					agenda_integrada_item b,
					agenda_medico c,
					agenda d
			where	b.nr_seq_agenda_exame	= a.nr_sequencia
			and		a.cd_medico_exec	= cd_medico_lib_w
			and		a.cd_medico_exec	= c.cd_medico
			and		a.cd_agenda		= c.cd_agenda
			and		a.cd_agenda		= d.cd_agenda
			and		((a.nr_seq_proc_interno	= nr_seq_proc_interno_w) or (nr_seq_proc_interno_w is null))			
			--and	a.cd_agenda		= cd_agenda_p
			and		nvl(d.ie_rodizio_medico,'N') = 'S'
			and		a.dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
			and		a.nm_paciente is not null
			and		a.ie_status_agenda not in ('C','F','B','L');
					
			if	((qt_medico_w - qt_medico_lib_w) > qt_dias_rodizio_p)then
				ds_medico_w	:= substr(ds_medico_w || nm_medico_w ||', ',1,255); 
			end if;				
			
			end;
		end loop;
		close C02;
	elsif (nvl(cd_tipo_agenda_w,2) = 3) then
		
		select	count(*) + 1
		into	qt_medico_w
		from	agenda_consulta a,
			agenda_integrada_item b,
			agenda_medico c	
		where	b.nr_seq_agenda_cons	= a.nr_sequencia
		and	a.cd_medico_req		= cd_medico_p
		and	a.cd_medico_req		= c.cd_medico
		and	a.cd_agenda		= c.cd_agenda
		and	a.cd_agenda		= cd_agenda_p
		and	a.dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
		and	a.nm_paciente is not null
		and	a.ie_status_agenda not in ('C','F','B','L');

		open C01;
		loop
		fetch C01 into	
			cd_medico_lib_w,
			nm_medico_w;
		exit when C01%notfound;
			begin
			
			select	count(*)
			into	qt_medico_lib_w
			from	agenda_consulta a,
				agenda_integrada_item b,
				agenda_medico c	
			where	b.nr_seq_agenda_cons	= a.nr_sequencia
			and	a.cd_medico_req		= cd_medico_lib_w
			and	a.cd_medico_req		= c.cd_medico
			and	a.cd_agenda		= c.cd_agenda
			and	a.cd_agenda		= cd_agenda_p
			and	a.dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
			and	a.nm_paciente is not null
			and	a.ie_status_agenda not in ('C','F','B','L');
					
			if	((qt_medico_w - qt_medico_lib_w) > qt_dias_rodizio_p) then
				ds_medico_w	:= substr(ds_medico_w || nm_medico_w ||', ',1,255); 
			end if;		
			end;
		end loop;
		close C01;
	end if;
end if;

commit;

ds_erro_p := substr(ds_medico_w,1,length(ds_medico_w)-2);

end Ageint_consistir_rozidio_exame;
/