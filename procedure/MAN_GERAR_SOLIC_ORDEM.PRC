create or replace
procedure man_gerar_solic_ordem(	nr_sequencia_p			number,
				cd_estabelecimento_p		number,
				cd_local_estoque_p		number,
				nm_usuario_p			varchar2) as


cd_pessoa_solicitante_w		varchar2(10);
nr_seq_solic_w			Number(10);
nr_seq_ordem_w			Number(10);
nr_item_solic_w			number(10);
ds_observacao_w			solic_compra.ds_observacao%type;
cd_material_w			number(6);
ds_material_direto_w		varchar2(255);
cd_unidade_compra_w		varchar2(30);
qt_material_w			number(13,2);
ds_observacao_item_w		varchar2(255);
vl_material_w			number(17,4);

cursor c01 is
select	cd_material,
	qt_item,
	ds_item,
	substr(ds_observacao,1,255),
	vl_item
from	man_ord_serv_orc_item
where	nr_seq_orc_ordem = nr_sequencia_p
and	cd_material is not null
order by	nr_seq_apres;

begin

begin
select	max(cd_pessoa_fisica)
into	cd_pessoa_solicitante_w
from	usuario
where	nm_usuario = nm_usuario_p;
exception
	when no_data_found then
	/*(-2001,'Usu�rio ' || nm_usuario_p || 
				' sem a informa��o da pessoa fisica em seu cadastro');*/
	wheb_mensagem_pck.exibir_mensagem_abort(263129,'NM_USUARIO=' || nm_usuario_p);
end;

select	nr_seq_ordem,
	substr(ds_observacao,1,4000)
into	nr_seq_ordem_w,
	ds_observacao_w
from	man_ordem_servico_orc
where	nr_sequencia = nr_sequencia_p;

select	solic_compra_seq.nextval
into	nr_seq_solic_w
from	dual;

insert into solic_compra(
	nr_solic_compra,
	cd_estabelecimento,
	dt_solicitacao_compra,
	dt_atualizacao,
	nm_usuario,
	ie_situacao,
	cd_pessoa_solicitante,
	cd_local_estoque,
	cd_setor_atendimento,
	ie_aviso_chegada,
	nr_seq_ordem_serv,
	ds_observacao,
	ie_aviso_aprov_oc,
	ie_urgente,
	ie_tipo_solicitacao,
	ie_comodato,
	ie_semanal,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_tipo_servico)
values(nr_seq_solic_w,
	cd_estabelecimento_p,
	sysdate,
	sysdate,
	nm_usuario_p,
	'A',
	cd_pessoa_solicitante_w,
	cd_local_estoque_p,
	obter_setor_usuario(nm_usuario_p),
	'N',
	nr_seq_ordem_w,
	ds_observacao_w,
	'N','N',3,'N','N',
	nm_usuario_p,
	sysdate,
	nvl(substr(obter_valor_param_usuario(297, 101, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),1,2),'ES'));

open c01;
loop
fetch c01 into
	cd_material_w,
	qt_material_w,
	ds_material_direto_w,
	ds_observacao_item_w,
	vl_material_w;
exit when c01%notfound;
	begin
	select	nvl(max(nr_item_solic_compra),0) + 1
	into	nr_item_solic_w
	from	solic_compra_item
	where	nr_solic_compra = nr_seq_solic_w;

	select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,255) cd_unidade_medida_compra
	into	cd_unidade_compra_w
	from	material
	where	cd_material = cd_material_w;

	insert into solic_compra_item(
		nr_solic_compra,
		nr_item_solic_compra,
		cd_material,
		cd_unidade_medida_compra,
		qt_material,
		ds_material_direto,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		dt_solic_item,
		ie_geracao,
		vl_unit_previsto,
		ds_observacao,
		qt_conv_compra_est_orig,
		qt_saldo_disp_estoque)
	values(nr_seq_solic_w,
		nr_item_solic_w,
		cd_material_w,
		cd_unidade_compra_w,
		qt_material_w,
		ds_material_direto_w,
		sysdate,
		nm_usuario_p,
		'A',
		trunc(sysdate),
		'U',
		vl_material_w,
		ds_observacao_item_w,
		obter_dados_material(cd_material_w,'QCE'),
		obter_saldo_disp_estoque(cd_estabelecimento_p, cd_material_w, cd_local_estoque_p, trunc(sysdate,'mm')));
		
		gerar_solic_item_entrega(nr_seq_solic_w,nr_item_solic_w,nm_usuario_p);
		
	end;
end loop;
close c01;

commit;

end man_gerar_solic_ordem;
/
