create or replace
procedure ptu_consistir_lote_a300
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

Cursor C01 is
	select	a.nr_sequencia,
		a.cd_pessoa_fisica,
		a.nm_beneficiario,
		a.nm_mae_benef,
		a.dt_nascimento,
		a.ie_sexo,
		a.cd_cgc_cpf,
		a.nr_cartao_nac_sus,
		e.cd_cep,
		e.ds_endereco,
		e.nm_municipio,
		e.sg_uf,
		a.nr_rg,
		a.ds_orgao_emissor,
		a.sg_uf_rg,
		a.cd_pais,
		(select	max(x.dt_liberacao)
		from	pls_sca_vinculo x
		where	x.nr_sequencia = a.nr_seq_vinculo_sca) dt_liberacao
	from	ptu_mov_produto_benef	a,
		ptu_mov_produto_empresa	b,
		ptu_movimentacao_produto c,
		ptu_mov_produto_lote	d,
		ptu_movimento_benef_compl e
	where	b.nr_sequencia	= a.nr_seq_empresa
	and	c.nr_sequencia	= b.nr_seq_mov_produto
	and	d.nr_sequencia	= c.nr_seq_lote
	and	a.nr_sequencia	= e.nr_seq_beneficiario
	and	d.nr_sequencia	= nr_seq_lote_p;

begin

for r_c01_w in C01 loop
	begin
	delete	from	ptu_intercambio_consist
	where	nr_seq_mov_prod_benef	= r_c01_w.nr_sequencia
	and	nr_seq_inconsistencia_a300 is not null;
	
	if	(r_c01_w.nm_beneficiario is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 1, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.nm_mae_benef is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 2, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	((pls_consistir_letra_unica_pf(r_c01_w.nm_beneficiario) = 'S') or
		 (pls_consistir_letra_unica_pf(r_c01_w.nm_mae_benef) = 'S')) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 3, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.dt_nascimento is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 4, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.ie_sexo is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 5, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.cd_cgc_cpf is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 6, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.nr_cartao_nac_sus is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 7, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(length(r_c01_w.nr_cartao_nac_sus) <> 15) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 8, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	((r_c01_w.nr_rg is null and r_c01_w.ds_orgao_emissor is null and r_c01_w.sg_uf_rg is null and r_c01_w.cd_pais is null) or
		 (r_c01_w.nr_rg is not null and r_c01_w.ds_orgao_emissor is not null and r_c01_w.sg_uf_rg is not null and r_c01_w.cd_pais is not null)) then
		null;
	else
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 9, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.cd_cep is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 10, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.ds_endereco is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 11, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.nm_municipio is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 12, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.sg_uf is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 13, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	if	(r_c01_w.dt_liberacao is null) then
		pls_gravar_inc_benef_a300(r_c01_w.nr_sequencia, 14, cd_estabelecimento_p, nm_usuario_p);
	end if;
	end;
end loop;

commit;

end ptu_consistir_lote_a300;
/