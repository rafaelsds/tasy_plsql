create or replace
procedure cpoe_suspender_item_gpfc(	nr_Sequencia_p		number,
					ie_tipo_item_p		varchar2,
					nr_atendimento_p	number,
					nr_seq_motivo_susp_p	number,
					ds_motivo_susp_p	varchar2,
					nm_usuario_p		Varchar2) is 

ds_lista_w					varchar2(2000);
ds_lista_ww					varchar2(2000);
ie_tipo_item_w				varchar2(10);
lista_itens_ret_w			varchar2(2000);
lista_itens_ret2_w			varchar2(2000);
ds_lista_proc_susp_out_w	varchar2(4000);
					
begin

if	(ie_tipo_item_p	in ('D','J','SNE','LD','S')) then
	ie_tipo_item_w	:= 'N';
elsif	(ie_tipo_item_p in ('MAT','SOL')) then	
	ie_tipo_item_w	:= 'M';
elsif	(ie_tipo_item_p	= 'O') then	
	ie_tipo_item_w	:= 'G';
else
	ie_tipo_item_w	:= ie_tipo_item_p;
end if;	

ds_lista_w	:= 	'['||nr_sequencia_p||';'||ie_tipo_item_w||']';

cpoe_ajustar_itens_susp(ds_lista_w,sysdate,nm_usuario_p,lista_itens_ret_w,'P',lista_itens_ret2_w,ds_lista_ww);

ds_lista_w	:=	'['||nr_sequencia_p||';'||ie_tipo_item_w||';S]';

CPOE_Suspender_item_prescr(ds_lista_w,nr_Atendimento_p,nm_usuario_p,nr_seq_motivo_susp_p,ds_motivo_susp_p,'N',ds_lista_proc_susp_out_w);

end cpoe_suspender_item_gpfc;
/
