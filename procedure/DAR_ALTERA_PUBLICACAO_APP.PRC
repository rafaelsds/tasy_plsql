create or replace procedure dar_altera_publicacao_app(nr_sequencia_p in dar_app.nr_sequencia%type) is

   dt_publicacao_w dar_app.dt_publicacao%type;

begin

   -- Se ja publicado, passa a ser null a data de publicacao 
   select max(decode(a.dt_publicacao, null, sysdate, null))
     into dt_publicacao_w
     from dar_app a
    where a.nr_sequencia = nr_sequencia_p;

   update dar_app
      set dt_publicacao = dt_publicacao_w
    where nr_sequencia = nr_sequencia_p;

   commit;

end dar_altera_publicacao_app;
/
