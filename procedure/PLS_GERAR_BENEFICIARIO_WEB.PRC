create or replace
procedure pls_gerar_beneficiario_web
			(	cd_usuario_plano_p	Varchar2,
				cd_estabelecimento_p 	Number) is 

		
nr_seq_segurado_web_w	pls_segurado_web.nr_sequencia%type;
nr_seq_segurado_w		number(10);	
cd_usuario_plano_w		varchar2(255);	
ie_situacao_w			pls_segurado_web.ie_situacao%type;
cd_estabelecimento_w	pls_segurado.cd_estabelecimento%type; 
	
	
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Utilizado no acesso direto do benefici�rio  quando o benefici�rio  acessa diretamente pela
carteirinha no menu Canais. Caso o benefici�rio n�o possua login � criado automaticamente

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
	

begin

if	(cd_usuario_plano_p is not null) then

	select  elimina_caractere_especial(cd_usuario_plano_p)
	into    cd_usuario_plano_w
	from	dual;
		
	select	max(a.nr_sequencia),
			max(a.ie_situacao)
	into	nr_seq_segurado_web_w,
			ie_situacao_w
	from	pls_segurado_web a,
			pls_segurado b,
			pls_segurado_carteira c
	where	a.nr_seq_segurado = b.nr_sequencia
	and		b.nr_sequencia = c.nr_seq_segurado	
	and		c.cd_usuario_plano = cd_usuario_plano_p
	and    	pls_obter_dados_segurado(b.nr_sequencia, 'CS') = 'A';
	
	
	if	( nr_seq_segurado_web_w is null ) then
	
		select  nvl(max(b.nr_sequencia),0)
		into	nr_seq_segurado_w
		from	pls_segurado b,
				pls_segurado_carteira c
		where	b.nr_sequencia = c.nr_seq_segurado
		and		c.cd_usuario_plano = cd_usuario_plano_p
		and    	pls_obter_dados_segurado(b.nr_sequencia, 'CS') = 'A';
		
		
		if	(nr_seq_segurado_w is not null and nr_seq_segurado_w > 0 ) then
		
			if(cd_estabelecimento_p is null) then			
				select	pls_obter_dados_segurado(nr_seq_segurado_w, 'EST') cd_estabelecimento
				into	cd_estabelecimento_w
				from    dual;
			else  
				cd_estabelecimento_w := cd_estabelecimento_p;			
			end if;

			insert into pls_segurado_web
				(nr_sequencia, nr_seq_segurado, ds_senha,
				ie_situacao, ds_hash, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				ds_observacao, cd_estabelecimento, ie_origem_login)
			values	(pls_segurado_web_seq.nextval, nr_seq_segurado_w, null,
				'A', ' ', sysdate,
				 nr_seq_segurado_w, sysdate, nr_seq_segurado_w,
				 null, cd_estabelecimento_w, 'AR');
		end if;
	elsif	( nr_seq_segurado_web_w is not null and (ie_situacao_w = 'I' or ie_situacao_w = 'P')) then
	
		update	pls_segurado_web
		set		ie_situacao = 'A',
				ds_senha = null,
				ds_observacao = null,
				nr_seq_termo = null
		where 	nr_sequencia = nr_seq_segurado_web_w;		
	
	end if;
	
end if;
		
commit;

end pls_gerar_beneficiario_web;
/
