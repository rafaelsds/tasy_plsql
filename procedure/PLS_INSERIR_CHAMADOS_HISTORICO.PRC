create or replace procedure pls_inserir_chamados_historico (
    nr_seq_chamados_p         pls_chamados_historico.nr_seq_chamados%type,
    nr_seq_tipo_historico_p   pls_chamados_historico.nr_seq_tipo_historico%type,
    nm_usuario_p              pls_chamados_historico.nm_usuario%type,
    nm_usuario_nrec_p         pls_chamados_historico.nm_usuario_nrec%type,
    ds_detalhamento_hist_p    pls_chamados_historico.ds_detalhamento_hist%type
) is
begin
    insert into pls_chamados_historico (
        nr_sequencia,
        nr_seq_chamados,
        dt_atualizacao,
        nr_seq_tipo_historico,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        ds_detalhamento_hist
    ) values (
        pls_chamados_historico_seq.nextval,
        nr_seq_chamados_p,
        sysdate,
        nr_seq_tipo_historico_p,
        nm_usuario_p,
        sysdate,
        nm_usuario_nrec_p,
        ds_detalhamento_hist_p
    );

    commit;
end pls_inserir_chamados_historico;
/
