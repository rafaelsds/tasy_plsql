create or replace
procedure Consistir_espec_area_agecons(	cd_medico_p		varchar2,
					cd_especialidade_p	number,
					nr_seq_area_atuacao_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 
	
qt_especialidade_w	number(10,0);
qt_area_atuacao_w	number(10,0);
qt_med_cadastrado_w	number(10,0);
ie_cons_cad_med_w	varchar2(1);

begin

Obter_Param_Usuario(821, 227, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_cons_cad_med_w);

if	(ie_cons_cad_med_w = 'C') then
	select	count(*)
	into	qt_med_cadastrado_w
	from	medico a		
	where	a.cd_pessoa_fisica 	= cd_medico_p
	and	a.ie_situacao = 'A';
end if;

if	((qt_med_cadastrado_w	> 0) and
	(ie_cons_cad_med_w = 'C')) or
	(ie_cons_cad_med_w = 'S') then
	if	(cd_especialidade_p > 0) then
		select	count(*)
		into	qt_especialidade_w
		from	medico a,
			medico_especialidade b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.cd_pessoa_fisica 	= cd_medico_p
		and	b.cd_especialidade 	= cd_especialidade_p;
	end if;
		
	if	(nr_seq_area_atuacao_p > 0) then
		select	count(*)
		into	qt_area_atuacao_w
		from	medico a,
			medico_area_atuacao b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and		a.cd_pessoa_fisica 	= cd_medico_p
		and	b.nr_seq_area_atuacao	= nr_seq_area_atuacao_p;
	end if;	
end if;

if	(qt_especialidade_w = 0) and (qt_area_atuacao_w = 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262629);	
elsif	(qt_especialidade_w = 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262630);
elsif	(qt_area_atuacao_w = 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262631);
end if;

end Consistir_espec_area_agecons;
/
