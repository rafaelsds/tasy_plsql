create or replace
procedure pls_obter_guia_tiss(	nr_seq_conta_p		number,
				ie_tipo_guia_tiss_p out	varchar2) is
				
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Regra para verifcar qual o tipo da guia, que a conta deve ter quando importado o A500
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
				
qt_registro_w			number(10);
ie_tipo_guia_tiss_w		varchar2(2);
ie_tipo_guia_tiss_reg_w		varchar2(2);
nr_seq_regra_w			number(10);

ie_origem_proced_w		number(10);
cd_procedimento_w		number(15);
cd_area_procedimento_w		number(15);
cd_especialidade_w		number(15);
cd_grupo_proc_w			number(15);
ie_tipo_despesa_proc_tiss_w	varchar2(2);
ie_nota_hospitalar_w		varchar2(1) := 'N';
				
Cursor C01 is
	select	ie_tiss_tipo_guia,
		nr_sequencia
	from	pls_regra_guia_tiss
	order by ie_prioridade;
	
Cursor C02 is
	select	ie_origem_proced,
		cd_procedimento,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		ie_tipo_despesa_proc_tiss,
		ie_nota_hospitalar
	from	pls_regra_guia_tiss_proc
	where	nr_seq_regra = nr_seq_regra_w;
				
begin
ie_tipo_guia_tiss_p := null;

select	count(1)
into	qt_registro_w
from	pls_conta_proc
where	nr_seq_conta = nr_seq_conta_p
and	rownum = 1;

if	(qt_registro_w > 0) then
	open C01;
	loop
	fetch C01 into	
		ie_tipo_guia_tiss_w,
		nr_seq_regra_w;
	exit when C01%notfound;
		begin
		select	count(1)
		into	qt_registro_w
		from	pls_regra_guia_tiss_proc
		where	nr_seq_regra = nr_seq_regra_w
		and	rownum = 1;
		
		if	(qt_registro_w = 0) then		
			ie_tipo_guia_tiss_reg_w := ie_tipo_guia_tiss_w;
		end if;
		
		open C02;
		loop
		fetch C02 into	
			ie_origem_proced_w,
			cd_procedimento_w,
			cd_area_procedimento_w,
			cd_especialidade_w,
			cd_grupo_proc_w,
			ie_tipo_despesa_proc_tiss_w,
			ie_nota_hospitalar_w;
		exit when C02%notfound;
			begin			
			select	count(1)
			into	qt_registro_w
			from	estrutura_procedimento_v	b,
				pls_conta_proc			a
			where	a.cd_procedimento	= b.cd_procedimento
			and	a.ie_origem_proced	= b.ie_origem_proced
			and	a.nr_seq_conta		= nr_seq_conta_p
			and	(a.cd_procedimento 	= cd_procedimento_w or cd_procedimento_w is null)
			and	(a.ie_origem_proced 	= ie_origem_proced_w or ie_origem_proced_w is null)
			and	(b.cd_area_procedimento	= cd_area_procedimento_w or cd_area_procedimento_w is null)
			and	(b.cd_especialidade 	= cd_especialidade_w or cd_especialidade_w is null)
			and	(b.cd_grupo_proc 	= cd_grupo_proc_w or cd_grupo_proc_w is null)
			and	(b.ie_tipo_despesa_tiss	= ie_tipo_despesa_proc_tiss_w or ie_tipo_despesa_proc_tiss_w is null)
			and	rownum = 1;
			
			if	(nvl(ie_nota_hospitalar_w,'N') = 'S') then
				select	count(1)
				into	qt_registro_w
				from	pls_conta		x,
					ptu_nota_cobranca	w,
					ptu_nota_hospitalar	z
				where	w.nr_sequencia	= x.nr_seq_nota_cobranca
				and	w.nr_sequencia	= z.nr_seq_nota_cobr
				and	x.nr_sequencia	= nr_seq_conta_p
				and	rownum = 1;
			end if;
			
			if	(qt_registro_w > 0) then			
				ie_tipo_guia_tiss_p := ie_tipo_guia_tiss_w;
			end if;
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;
	
	if	(ie_tipo_guia_tiss_reg_w is not null) and
		(ie_tipo_guia_tiss_p is null) then		
		ie_tipo_guia_tiss_p := ie_tipo_guia_tiss_reg_w;
	end if;
end if;

end pls_obter_guia_tiss;
/