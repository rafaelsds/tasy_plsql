create or replace 
procedure conclusao_interna_checkup(	nr_sequencia_p		number,
					nm_usuario_p		varchar2) is
 
begin

if	(nr_sequencia_p is not null) then

	update 	checkup 
	set 	dt_fim_interno = sysdate,
		nm_usuario = nm_usuario_p
	where 	nr_sequencia = nr_sequencia_p;
	
end if;	

commit;	

end conclusao_interna_checkup;
/
