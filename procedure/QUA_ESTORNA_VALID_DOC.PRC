create or replace
procedure qua_estorna_valid_doc (
	nr_sequencia_p		number,
	ie_status_p		varchar2,
	nm_usuario_p		Varchar2) is 

begin

update	qua_documento
set	dt_validacao = null,
	ie_status    = ie_status_p
where	nr_sequencia = nr_sequencia_p;
commit;

update	qua_doc_validacao
set	dt_validacao = null,
dt_atualizacao = sysdate,
nm_usuario = nm_usuario_p
where	nr_seq_doc = nr_sequencia_p
and	(cd_pessoa_validacao = obter_pessoa_fisica_usuario(nm_usuario_p,'C') 
    or cd_cargo = obter_cargo_pf(nm_usuario_p,'C'));
commit;

end qua_estorna_valid_doc ;
/

