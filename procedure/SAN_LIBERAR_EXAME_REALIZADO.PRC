create or replace procedure san_liberar_exame_realizado(
		dt_liberacao_p		date,
		cd_pf_realizou_p	varchar2,
		nr_seq_exame_lote_p	number,
		nr_seq_exame_p		number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2) is

ie_alt_campo_realizado_por_w	varchar2(1);
ie_utiliza_nova_amostra_w	varchar2(1);

begin

/* Hemoterapia - Parametro [315] - Permite alterar o campo 'Realizado por' dos exames */
obter_param_usuario(450, 315, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_alt_campo_realizado_por_w);

/* Hemoterapia - Parametro [502] - Utiliza nova amostra */
obter_param_usuario(450, 502, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_utiliza_nova_amostra_w);

if	(ie_alt_campo_realizado_por_w <> 'S') then
	begin
	update	san_exame_realizado
	set	dt_liberacao	= dt_liberacao_p,
		cd_pf_realizou	= cd_pf_realizou_p,
		nm_usuario_lib = nm_usuario_p
	where	nr_seq_exame_lote	= nr_seq_exame_lote_p
	and	nr_seq_exame	= nr_seq_exame_p;
	end;
else
	begin
	update	san_exame_realizado
	set	dt_liberacao	= dt_liberacao_p,
		nm_usuario_lib = nm_usuario_p
	where	nr_seq_exame_lote	= nr_seq_exame_lote_p
	and	nr_seq_exame	= nr_seq_exame_p;
	end;
end if;

commit;

if(ie_utiliza_nova_amostra_w = 'S') then
	SAN_Gerar_Nova_Amostra(nr_seq_exame_lote_p, nr_seq_exame_p, nm_usuario_p);
end if;

end san_liberar_exame_realizado;
/
