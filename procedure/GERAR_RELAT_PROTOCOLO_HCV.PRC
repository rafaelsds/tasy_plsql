CREATE OR REPLACE
PROCEDURE Gerar_Relat_Protocolo_HCV	(dt_inicial_p			Date,
						dt_final_p			Date,
						cd_setor_atendimento_p	Number,
						ie_relatorio_p		Varchar2) IS

/*
Procedure criada para atender a OS33209.
Gera o relat�rio CATE 4180 HCV - Relat�rio Anual de Faturamento por Conv�nio (novo)
*/

/*vari�veis*/
ds_convenio_w		Varchar2(255);
nm_paciente_w		Varchar2(60);
dt_conta_w		Date;
vl_janeiro_w		Number(15,4)	:= 0;
vl_fevereiro_w	Number(15,4)	:= 0;
vl_marco_w		Number(15,4)	:= 0;
vl_abril_w		Number(15,4)	:= 0;
vl_maio_w		Number(15,4)	:= 0;
vl_junho_w		Number(15,4)	:= 0;
vl_julho_w		Number(15,4)	:= 0;
vl_agosto_w		Number(15,4)	:= 0;
vl_setembro_w		Number(15,4)	:= 0;
vl_outubro_w		Number(15,4)	:= 0;
vl_novembro_w		Number(15,4)	:= 0;
vl_dezembro_w		Number(15,4)	:= 0;
vl_total_conv_w	Number(15,4)	:= 0;
vl_percentual_w	Number(15,4)	:= 0;
vl_total_geral_w	Number(15,4)	:= 0;

/*comando sql relat�rio sint�tico*/
Cursor	C01 is
SELECT	ds_conv ds_convenio, 
	SUM(vl_jan) vl_janeiro,
	SUM(vl_fev) vl_fevereiro,
	SUM(vl_mar) vl_marco,
	SUM(vl_abr) vl_abril,
	SUM(vl_mai) vl_maio,
	SUM(vl_jun) vl_junho,
	SUM(vl_jul) vl_julho,
	SUM(vl_ago) vl_agosto,
	SUM(vl_set) vl_setembro,
	SUM(vl_out) vl_outubro,
	SUM(vl_nov) vl_novembro,
	SUM(vl_dez) vl_dezembro,
	SUM(vl_tot) vl_total_conv,
	sum(vl_perc) vl_percentual
FROM	(
	SELECT	SUBSTR(obter_nome_convenio(v.cd_convenio),1,100) ds_conv,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 1, NVL(v.vl_procedimento,0), 0)) vl_jan,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 2, NVL(v.vl_procedimento,0), 0)) vl_fev,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 3, NVL(v.vl_procedimento,0), 0)) vl_mar,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 4, NVL(v.vl_procedimento,0), 0)) vl_abr,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 5, NVL(v.vl_procedimento,0), 0)) vl_mai,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 6, NVL(v.vl_procedimento,0), 0)) vl_jun,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 7, NVL(v.vl_procedimento,0), 0)) vl_jul,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 8, NVL(v.vl_procedimento,0), 0)) vl_ago,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 9, NVL(v.vl_procedimento,0), 0)) vl_set,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 10, NVL(v.vl_procedimento,0), 0)) vl_out,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 11, NVL(v.vl_procedimento,0), 0)) vl_nov,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 12, NVL(v.vl_procedimento,0), 0)) vl_dez,
		SUM(NVL(v.vl_procedimento,0)) vl_tot,
		dividir((SUM(NVL(v.vl_procedimento,0)) * 100), vl_total_geral_w) vl_perc
	FROM	procedimento_paciente v,
		protocolo_convenio p,
		conta_paciente c
	WHERE	p.ie_status_protocolo 	= 2
	AND	p.nr_seq_protocolo		= c.nr_seq_protocolo
	AND	c.ie_cancelamento  		IS NULL
	AND	c.nr_interno_conta  		= v.nr_interno_conta
	AND	v.cd_convenio			IS NOT NULL
	AND	v.cd_motivo_exc_conta	IS NULL
	AND	c.dt_mesano_referencia 	BETWEEN dt_inicial_p and dt_final_p
	/*AND	c.dt_mesano_referencia 	BETWEEN	TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy') AND	
								PKG_DATE_UTILS.ADD_MONTH(TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy'),12,0)*/
	AND	((cd_setor_atendimento_p is null) OR (v.cd_setor_atendimento = cd_setor_atendimento_p))
	GROUP BY SUBSTR(obter_nome_convenio(v.cd_convenio),1,100)
	UNION
	SELECT	SUBSTR(obter_nome_convenio(v.cd_convenio),1,100) ds_conv,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 1, NVL(v.vl_material,0), 0)) vl_jan,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 2, NVL(v.vl_material,0), 0)) vl_fev,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 3, NVL(v.vl_material,0), 0)) vl_mar,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 4, NVL(v.vl_material,0), 0)) vl_abr,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 5, NVL(v.vl_material,0), 0)) vl_mai,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 6, NVL(v.vl_material,0), 0)) vl_jun,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 7, NVL(v.vl_material,0), 0)) vl_jul,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 8, NVL(v.vl_material,0), 0)) vl_ago,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 9, NVL(v.vl_material,0), 0)) vl_set,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 10, NVL(v.vl_material,0), 0)) vl_out,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 11, NVL(v.vl_material,0), 0)) vl_nov,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 12, NVL(v.vl_material,0), 0)) vl_dez,
		SUM(NVL(v.vl_material,0)) vl_tot,
		dividir((SUM(NVL(v.vl_material,0)) * 100), vl_total_geral_w) vl_perc
	FROM	material_atend_paciente v,
		protocolo_convenio p,
		conta_paciente c
	WHERE	p.ie_status_protocolo 	= 2
	AND	p.nr_seq_protocolo		= c.nr_seq_protocolo
	AND	c.ie_cancelamento  		IS NULL
	AND	c.nr_interno_conta  		= v.nr_interno_conta
	AND	v.cd_convenio  		IS NOT NULL
	AND	v.cd_motivo_exc_conta	IS NULL
	AND	c.dt_mesano_referencia 	BETWEEN dt_inicial_p and dt_final_p
	/*AND	c.dt_mesano_referencia 	BETWEEN	TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy') AND	
								PKG_DATE_UTILS.ADD_MONTH(TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy'),12,0)*/
	AND	((cd_setor_atendimento_p is null) OR (v.cd_setor_atendimento = cd_setor_atendimento_p))
	GROUP BY SUBSTR(obter_nome_convenio(v.cd_convenio),1,100)
	ORDER BY ds_conv
	)
group by	ds_conv
order by	ds_convenio;

/*comando sql relat�rio anal�tico*/
Cursor C02 is
SELECT	ds_conv ds_convenio,
	nm_pac nm_paciente,
	dt_cta dt_conta,
	SUM(vl_jan) vl_janeiro,
	SUM(vl_fev) vl_fevereiro,
	SUM(vl_mar) vl_marco,
	SUM(vl_abr) vl_abril,
	SUM(vl_mai) vl_maio,
	SUM(vl_jun) vl_junho,
	SUM(vl_jul) vl_julho,
	SUM(vl_ago) vl_agosto,
	SUM(vl_set) vl_setembro,
	SUM(vl_out) vl_outubro,
	SUM(vl_nov) vl_novembro,
	SUM(vl_dez) vl_dezembro,
	SUM(vl_tot) vl_total_conv,
	sum(vl_perc) vl_percentual
FROM	(
	SELECT	SUBSTR(obter_nome_convenio(v.cd_convenio),1,40) ds_conv,
		substr(obter_pessoa_atendimento(c.nr_atendimento,'N'),1,60) nm_pac,
		c.dt_mesano_referencia dt_cta,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 1, NVL(v.vl_procedimento,0), 0)) vl_jan,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 2, NVL(v.vl_procedimento,0), 0)) vl_fev,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 3, NVL(v.vl_procedimento,0), 0)) vl_mar,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 4, NVL(v.vl_procedimento,0), 0)) vl_abr,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 5, NVL(v.vl_procedimento,0), 0)) vl_mai,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 6, NVL(v.vl_procedimento,0), 0)) vl_jun,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 7, NVL(v.vl_procedimento,0), 0)) vl_jul,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 8, NVL(v.vl_procedimento,0), 0)) vl_ago,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 9, NVL(v.vl_procedimento,0), 0)) vl_set,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 10, NVL(v.vl_procedimento,0), 0)) vl_out,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 11, NVL(v.vl_procedimento,0), 0)) vl_nov,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 12, NVL(v.vl_procedimento,0), 0)) vl_dez,
		SUM(NVL(v.vl_procedimento,0)) vl_tot,
		dividir((SUM(NVL(v.vl_procedimento,0)) * 100), vl_total_geral_w) vl_perc
	FROM	procedimento_paciente v,
		protocolo_convenio p,
		conta_paciente c
	WHERE	p.ie_status_protocolo 	= 2
	AND	p.nr_seq_protocolo		= c.nr_seq_protocolo
	AND	c.ie_cancelamento  		IS NULL
	AND	c.nr_interno_conta  		= v.nr_interno_conta
	AND	v.cd_convenio			IS NOT NULL
	AND	v.cd_motivo_exc_conta	IS NULL
	AND	c.dt_mesano_referencia 	BETWEEN dt_inicial_p and dt_final_p
	/*AND	c.dt_mesano_referencia 	BETWEEN 	TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy') AND
								PKG_DATE_UTILS.ADD_MONTH(TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy'),12,0)*/
	AND	((cd_setor_atendimento_p is null) OR (v.cd_setor_atendimento = cd_setor_atendimento_p))
	GROUP BY SUBSTR(obter_nome_convenio(v.cd_convenio),1,40),
		 substr(obter_pessoa_atendimento(c.nr_atendimento,'N'),1,60),
		 c.dt_mesano_referencia
	UNION
	SELECT	SUBSTR(obter_nome_convenio(v.cd_convenio),1,40) ds_conv,
		substr(obter_pessoa_atendimento(c.nr_atendimento,'N'),1,60) nm_pac,
		c.dt_mesano_referencia dt_cta,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 1, NVL(v.vl_material,0), 0)) vl_jan,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 2, NVL(v.vl_material,0), 0)) vl_fev,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 3, NVL(v.vl_material,0), 0)) vl_mar,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 4, NVL(v.vl_material,0), 0)) vl_abr,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 5, NVL(v.vl_material,0), 0)) vl_mai,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 6, NVL(v.vl_material,0), 0)) vl_jun,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 7, NVL(v.vl_material,0), 0)) vl_jul,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 8, NVL(v.vl_material,0), 0)) vl_ago,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 9, NVL(v.vl_material,0), 0)) vl_set,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 10, NVL(v.vl_material,0), 0)) vl_out,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 11, NVL(v.vl_material,0), 0)) vl_nov,
		SUM(DECODE(pkg_date_utils.extract_field('MONTH',c.dt_mesano_referencia,0), 12, NVL(v.vl_material,0), 0)) vl_dez,
		SUM(NVL(v.vl_material,0)) vl_tot,
		dividir((SUM(NVL(v.vl_material,0)) * 100), vl_total_geral_w) vl_perc
	FROM	material_atend_paciente v,
		protocolo_convenio p,
		conta_paciente c
	WHERE	p.ie_status_protocolo 	= 2
	AND	p.nr_seq_protocolo		= c.nr_seq_protocolo
	AND	c.ie_cancelamento  		IS NULL
	AND	c.nr_interno_conta  		= v.nr_interno_conta
	AND	v.cd_convenio  		IS NOT NULL
	AND	v.cd_motivo_exc_conta	IS NULL
	AND	c.dt_mesano_referencia 	BETWEEN dt_inicial_p and dt_final_p
	/*AND	c.dt_mesano_referencia 	BETWEEN	TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy') AND 				
								PKG_DATE_UTILS.ADD_MONTH(TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy'),12,0)*/
	AND	((cd_setor_atendimento_p is null) OR (v.cd_setor_atendimento = cd_setor_atendimento_p))
	GROUP BY SUBSTR(obter_nome_convenio(v.cd_convenio),1,40),
		 substr(obter_pessoa_atendimento(c.nr_atendimento,'N'),1,60),
		 c.dt_mesano_referencia
	ORDER BY ds_conv
	)
group by	ds_conv, 
		nm_pac,
		dt_cta
order by	ds_convenio, 
		nm_paciente,
		dt_conta;

BEGIN
if	(dt_inicial_p is not null) and
	(dt_final_p is not null) then

	/*limpar tabela*/
	delete from w_relat_protocolo_hcv;
	commit;

	/*obter total geral para c�lculo do percentual*/
	select	sum(vl_tot) vl_total_geral
	into	vl_total_geral_w
	from	(
		select	SUM(NVL(v.vl_procedimento,0)) vl_tot
		FROM	procedimento_paciente v,
			protocolo_convenio p,
			conta_paciente c
		WHERE	p.ie_status_protocolo 	= 2
		AND	p.nr_seq_protocolo		= c.nr_seq_protocolo
		AND	c.ie_cancelamento  		IS NULL
		AND	c.nr_interno_conta  		= v.nr_interno_conta
		AND	v.cd_convenio			IS NOT NULL
		AND	v.cd_motivo_exc_conta	IS NULL
		AND	c.dt_mesano_referencia 	BETWEEN dt_inicial_p and dt_final_p
		/*AND	c.dt_mesano_referencia 	BETWEEN	TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy') AND			
									PKG_DATE_UTILS.ADD_MONTH(TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy'),12,0)*/
		AND	((cd_setor_atendimento_p is null) OR (v.cd_setor_atendimento = cd_setor_atendimento_p))
		union
		select	SUM(NVL(v.vl_material,0)) vl_tot
		FROM	material_atend_paciente v,
			protocolo_convenio p,
			conta_paciente c
		WHERE	p.ie_status_protocolo 	= 2
		AND	p.nr_seq_protocolo		= c.nr_seq_protocolo
		AND	c.ie_cancelamento  		IS NULL
		AND	c.nr_interno_conta  		= v.nr_interno_conta
		AND	v.cd_convenio			IS NOT NULL
		AND	v.cd_motivo_exc_conta	IS NULL
		AND	c.dt_mesano_referencia 	BETWEEN dt_inicial_p and dt_final_p
		/*AND	c.dt_mesano_referencia 	BETWEEN	TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy') AND 
									PKG_DATE_UTILS.ADD_MONTH(TO_DATE('01/01'||nr_ano_p, 'dd/mm/yyyy'),12,0)*/
		AND	((cd_setor_atendimento_p is null) OR (v.cd_setor_atendimento = cd_setor_atendimento_p))
		);

	/*relat�rio sint�tico*/
	if	(nvl(ie_relatorio_p,'S') = 'S') then

		Open C01;
		Loop
		Fetch C01 into	ds_convenio_w,
					vl_janeiro_w,
					vl_fevereiro_w,
					vl_marco_w,
					vl_abril_w,
					vl_maio_w,
					vl_junho_w,
					vl_julho_w,
					vl_agosto_w,
					vl_setembro_w,
					vl_outubro_w,
					vl_novembro_w,
					vl_dezembro_w,
					vl_total_conv_w,
					vl_percentual_w;
		Exit when C01%NotFound;

		/*inserir valores (relat�rio sint�tico) na tabela tempor�ria*/
		insert	into	w_relat_protocolo_hcv	(
								ds_convenio,
								vl_janeiro,
								vl_fevereiro,
								vl_marco,
								vl_abril,
								vl_maio,
								vl_junho,
								vl_julho,
								vl_agosto,
								vl_setembro,
								vl_outubro,
								vl_novembro,
								vl_dezembro,
								vl_total_convenio,
								vl_percentual
								)
							values	(
								ds_convenio_w,
								vl_janeiro_w,
								vl_fevereiro_w,
								vl_marco_w,
								vl_abril_w,
								vl_maio_w,
								vl_junho_w,
								vl_julho_w,
								vl_agosto_w,
								vl_setembro_w,
								vl_outubro_w,
								vl_novembro_w,
								vl_dezembro_w,
								vl_total_conv_w,
								vl_percentual_w
								);
		End loop;
		Close C01;

	/*relat�rio anal�tico*/
	elsif	(nvl(ie_relatorio_p,'S') = 'A') then

		Open C02;
		Loop
		Fetch C02 into	ds_convenio_w,
					nm_paciente_w,
					dt_conta_w,
					vl_janeiro_w,
					vl_fevereiro_w,
					vl_marco_w,
					vl_abril_w,
					vl_maio_w,
					vl_junho_w,
					vl_julho_w,
					vl_agosto_w,
					vl_setembro_w,
					vl_outubro_w,
					vl_novembro_w,
					vl_dezembro_w,
					vl_total_conv_w,
					vl_percentual_w;
		Exit when C02%NotFound;

		/*inserir valores (relat�rio anal�tico) na tabela tempor�ria*/
		insert	into	w_relat_protocolo_hcv	(
								ds_convenio,
								nm_paciente,
								dt_conta,
								vl_janeiro,
								vl_fevereiro,
								vl_marco,
								vl_abril,
								vl_maio,
								vl_junho,
								vl_julho,
								vl_agosto,
								vl_setembro,
								vl_outubro,
								vl_novembro,
								vl_dezembro,
								vl_total_convenio,
								vl_percentual
								)
							values	(
								ds_convenio_w,
								nm_paciente_w,
								dt_conta_w,
								vl_janeiro_w,
								vl_fevereiro_w,
								vl_marco_w,
								vl_abril_w,
								vl_maio_w,
								vl_junho_w,
								vl_julho_w,
								vl_agosto_w,
								vl_setembro_w,
								vl_outubro_w,
								vl_novembro_w,
								vl_dezembro_w,
								vl_total_conv_w,
								vl_percentual_w
								);
		End loop;
		Close C02;
	end if;
end if;
commit;
END	Gerar_Relat_Protocolo_HCV;
/