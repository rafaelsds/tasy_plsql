create or replace
procedure gerar_observacao_tecnica_md(	cd_setor_atendimento_p	number,
					dt_dieta_p		date,
					cd_refeicao_p		varchar2,
					ie_opcao_p		varchar2,
					ie_atualizar_obs_nula_p	varchar2,
					nm_usuario_p		varchar2) is

dt_dieta_w		date;
nr_sequencia_w		number(10);
ds_observacao_tec_w	varchar2(2000);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_superior_w	number(10);
ie_somente_dieta_princ_w	varchar2(1)	:= 'N';

cursor c01 is 
select	nr_sequencia,
	cd_pessoa_fisica,
	nr_seq_superior
from	mapa_dieta
where	cd_setor_atendimento	=	cd_setor_atendimento_p
and	dt_dieta		=	trunc(dt_dieta_p,'dd')
and	cd_refeicao		=	cd_refeicao_p
and	ie_destino_dieta	=	'A';

begin

Obter_Param_Usuario(1000,68,obter_perfil_ativo,nm_usuario_p,0,ie_somente_dieta_princ_w);

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_pessoa_fisica_w,
	nr_seq_superior_w;
	exit when c01%notfound;
	
	ds_observacao_tec_w := null;
	
	begin

	if	(ie_opcao_p <> 'IPU') and (nr_seq_superior_w is not null) then
		begin
		begin
		select	max(ds_observacao_tec)
		into	ds_observacao_tec_w	
		from	mapa_dieta
		where	cd_pessoa_fisica	= 	cd_pessoa_fisica_w
		and	ie_destino_dieta	= 	'A'
		and	ds_observacao_tec is not null
		and	cd_refeicao		=	cd_refeicao_p
		and	nr_seq_superior		is not null
		and	dt_dieta		=	(	select	trunc(max(a.dt_dieta),'dd')
								from	atendimento_paciente b,
									mapa_dieta a
								where	b.nr_atendimento	= a.nr_atendimento
								and	((b.dt_alta is null) or (ie_opcao_p in ('P','PU')))
								and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
								and	ie_destino_dieta	= 	'A'								
								and	((dt_dieta		< dt_dieta_p) or (ie_opcao_p not in ('U','PU')))
								and	((a.ds_observacao_tec is not null) or (ie_opcao_p in ('U','PU')))
								and	nr_seq_superior		is not null
								and	a.cd_refeicao		= cd_refeicao_p);	
		exception
		when others then
			ds_observacao_tec_w	:= null;
		end;	
		end;
		
	elsif	(ie_opcao_p = 'IPU') and (nr_seq_superior_w is not null) then
		begin
		begin
		
		select	max(ds_observacao_tec)
		into	ds_observacao_tec_w	
		from	mapa_dieta
		where	cd_pessoa_fisica	= 	cd_pessoa_fisica_w
		and	ie_destino_dieta	= 	'A'
		and	ds_observacao_tec is not null
		and	nr_seq_superior		is not null
		and	dt_dieta		=	(	select	trunc(max(a.dt_dieta),'dd')
								from	atendimento_paciente b,
									mapa_dieta a
								where	b.nr_atendimento	= a.nr_atendimento
								and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
								and	ie_destino_dieta	= 	'A'
								and	a.dt_dieta		<= dt_dieta_p
								and	nr_seq_superior		is not null
								and	a.ds_observacao_tec is not null);	
		exception
		when others then
			ds_observacao_tec_w	:= null;
		end;
		end;
	
	elsif	(ie_opcao_p <> 'IPU') and (nr_seq_superior_w is null) then
		begin
		begin
		select	max(ds_observacao_tec)
		into	ds_observacao_tec_w	
		from	mapa_dieta
		where	cd_pessoa_fisica	= 	cd_pessoa_fisica_w
		and	ie_destino_dieta	= 	'A'
		and	ds_observacao_tec is not null
		and	cd_refeicao		=	cd_refeicao_p
		and	nr_seq_superior		is null
		and	dt_dieta		=	(	select	trunc(max(a.dt_dieta),'dd')
								from	atendimento_paciente b,
									mapa_dieta a
								where	b.nr_atendimento	= a.nr_atendimento
								and	((b.dt_alta is null) or (ie_opcao_p in ('P','PU')))
								and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
								and	ie_destino_dieta	= 	'A'								
								and	((dt_dieta		< dt_dieta_p) or (ie_opcao_p not in ('U','PU')))
								and	((a.ds_observacao_tec is not null) or (ie_opcao_p in ('U','PU')))
								and	nr_seq_superior		is null
								and	a.cd_refeicao		= cd_refeicao_p);	
		exception
		when others then
			ds_observacao_tec_w	:= null;
		end;	
		end;
	
	elsif	(ie_opcao_p = 'IPU') and (nr_seq_superior_w is null) then
		begin
		begin
		
		select	max(ds_observacao_tec)
		into	ds_observacao_tec_w	
		from	mapa_dieta
		where	cd_pessoa_fisica	= 	cd_pessoa_fisica_w
		and	ie_destino_dieta	= 	'A'
		and	ds_observacao_tec is not null
		and	nr_seq_superior		is null
		and	dt_dieta		=	(	select	trunc(max(a.dt_dieta),'dd')
								from	atendimento_paciente b,
									mapa_dieta a
								where	b.nr_atendimento	= a.nr_atendimento
								and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
								and	ie_destino_dieta	= 	'A'
								and	a.dt_dieta		<= dt_dieta_p
								and	nr_seq_superior		is null
								and	a.ds_observacao_tec is not null);	
		exception
		when others then
			ds_observacao_tec_w	:= null;
		end;
		end;
	
	end if;
	
	if	((ie_somente_dieta_princ_w	= 'N') or
		((ie_somente_dieta_princ_w	= 'S') and
		(nr_seq_superior_w is null))) then
		update	mapa_dieta 
		set	ds_observacao_tec	= ds_observacao_tec_w	
		where	nr_sequencia		= nr_sequencia_w
		and	dt_liberacao is null
		and	((ie_atualizar_obs_nula_p = 'N') or ((ie_atualizar_obs_nula_p = 'S') and (ds_observacao_tec is null))); 
	end if;

	end;
end loop;
close c01;

commit;

end gerar_observacao_tecnica_md;
/
