create or replace
procedure	sup_informar_parecer_item_lc(
			nr_sequencia_p		number,
			ds_parecer_p		varchar2,
			nm_usuario_p		varchar2) is

begin

update	segmento_compras_rel_parec
set	ds_parecer = ds_parecer_p,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end sup_informar_parecer_item_lc;
/