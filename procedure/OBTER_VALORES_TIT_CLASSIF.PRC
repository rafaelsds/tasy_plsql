create or replace
procedure obter_valores_tit_classif(
					nr_titulo_p		number,
					vl_total_rateio_p 	out number,
					vl_dif_rateio_p		out number) is 

vl_total_rateio_w	number(15,2);
vl_dif_rateio_w		number(15,2);

begin

select  nvl(sum(vl_titulo),0)
into 	vl_total_rateio_w
from    titulo_pagar_classif
where   nr_titulo	= nr_titulo_p;
	
select  to_number(obter_dados_tit_pagar(nr_titulo, 'VT'))
into	vl_dif_rateio_w
from  	titulo_pagar
where   nr_titulo	= nr_titulo_p;

vl_total_rateio_p 	:= vl_total_rateio_w;
vl_dif_rateio_p   	:= vl_dif_rateio_w;

end obter_valores_tit_classif;
/