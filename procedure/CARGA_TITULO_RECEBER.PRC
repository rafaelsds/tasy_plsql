create or replace
procedure carga_titulo_receber (  nm_usuario_p		Varchar2,
								  ie_commit_p		Varchar2,
								  nr_lote_p			number) is 

												  
Cursor C01 is
	select	a.*
	from	w_import_tit_rec a
	where	a.nr_titulo_externo is not null /*tem que ter titulo_externo no arquivo importado*/
	and		not exists (select 1 /*n�o considerar titulos externos que ja foram importados no Tasy*/
						from	titulo_receber x
						where	x.nr_titulo_externo = a.nr_titulo_externo )
	order by a.nr_titulo_externo asc;								  
								  
c01_w				c01%rowtype;
titulo_receber_w	titulo_receber%rowtype;
qt_consistencia_w 	number(10) := 0;
nm_rotina_w			varchar2(50) := 'CARGA_TITULO_RECEBER';
ds_inconsistencia_w	varchar2(4000);
								  
begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin

	titulo_receber_w.cd_estabelecimento 	:= to_number(c01_w.cd_estabelecimento);
	titulo_receber_w.dt_atualizacao			:= nvl(to_date(c01_w.dt_atualizacao,'dd/mm/yyyy'),sysdate);
	titulo_receber_w.nm_usuario				:= c01_w.nm_usuario;
	titulo_receber_w.dt_emissao				:= c01_w.dt_emissao;			
	titulo_receber_w.dt_vencimento			:= c01_w.dt_vencimento;
	titulo_receber_w.dt_pagamento_previsto 	:= c01_w.dt_pagamento_previsto;
	titulo_receber_w.vl_titulo				:= to_number(c01_w.vl_titulo);
	titulo_receber_w.vl_saldo_titulo		:= to_number(c01_w.vl_saldo_titulo);
	titulo_receber_w.vl_saldo_juros			:= to_number(c01_w.vl_saldo_juros);
	titulo_receber_w.vl_saldo_multa			:= to_number(c01_w.vl_saldo_multa);
	titulo_receber_w.cd_moeda				:= to_number(c01_w.cd_moeda);
	titulo_receber_w.cd_portador			:= to_number(c01_w.cd_portador);
	titulo_receber_w.cd_tipo_portador		:= to_number(c01_w.cd_tipo_portador);
	titulo_receber_w.tx_juros				:= to_number(c01_w.tx_juros);
	titulo_receber_w.tx_multa				:= to_number(c01_w.tx_multa);
	titulo_receber_w.cd_tipo_taxa_juro		:= to_number(c01_w.cd_tipo_taxa_juro);
	titulo_receber_w.cd_tipo_taxa_multa		:= to_number(c01_w.cd_tipo_taxa_multa);
	titulo_receber_w.tx_desc_antecipacao	:= to_number(c01_w.tx_desc_antecipacao);
	titulo_receber_w.ie_situacao			:= substr(c01_w.ie_situacao,1,1);
	titulo_receber_w.ie_tipo_emissao_titulo := to_number(c01_w.ie_tipo_emissao_titulo);
	titulo_receber_w.ie_origem_titulo		:= substr(c01_w.ie_origem_titulo,1,10);
	titulo_receber_w.ie_tipo_titulo			:= substr(c01_w.ie_tipo_titulo,1,1);
	titulo_receber_w.ie_tipo_inclusao		:= substr(c01_w.ie_tipo_inclusao,1,1);
	titulo_receber_w.cd_pessoa_fisica		:= c01_w.cd_pessoa_fisica;
	titulo_receber_w.nr_interno_conta		:= c01_w.nr_interno_conta;
	titulo_receber_w.cd_cgc					:= c01_w.cd_cgc;
	titulo_receber_w.cd_serie				:= c01_w.cd_serie;
	titulo_receber_w.nr_documento			:= c01_w.nr_documento;
	titulo_receber_w.nr_sequencia_doc		:= c01_w.nr_sequencia_doc;
	titulo_receber_w.cd_banco				:= c01_w.cd_banco;
	titulo_receber_w.cd_agencia_bancaria	:= c01_w.cd_agencia_bancaria;
	titulo_receber_w.nr_bloqueto			:= c01_w.nr_bloqueto;
	titulo_receber_w.dt_liquidacao			:= c01_w.dt_liquidacao;
	titulo_receber_w.nr_lote_contabil		:= c01_w.nr_lote_contabil;
	titulo_receber_w.ds_observacao_titulo	:= c01_w.ds_observacao_titulo;
	titulo_receber_w.nr_atendimento			:= c01_w.nr_atendimento;
	titulo_receber_w.nr_seq_protocolo		:= c01_w.nr_seq_protocolo;
	titulo_receber_w.dt_contabil			:= c01_w.dt_contabil;
	titulo_receber_w.nr_seq_conta_banco		:= c01_w.nr_seq_conta_banco;
	titulo_receber_w.dt_emissao_bloqueto	:= c01_w.dt_emissao_bloqueto;
	titulo_receber_w.nr_seq_classe			:= c01_w.nr_seq_classe;
	titulo_receber_w.nr_guia				:= c01_w.nr_guia;
	titulo_receber_w.nr_nosso_numero		:= c01_w.nr_nosso_numero;
	titulo_receber_w.dt_ref_conta			:= c01_w.dt_ref_conta;
	titulo_receber_w.cd_convenio_conta		:= c01_w.cd_convenio_conta;
	titulo_receber_w.nr_seq_terc_conta		:= c01_w.nr_seq_terc_conta;
	titulo_receber_w.nr_seq_nf_saida		:= c01_w.nr_seq_nf_saida;
	titulo_receber_w.nr_processo_aih		:= c01_w.nr_processo_aih;
	titulo_receber_w.nr_seq_controle_pessoa	:= c01_w.nr_seq_controle_pessoa;
	titulo_receber_w.cd_tipo_recebimento	:= c01_w.cd_tipo_recebimento;
	titulo_receber_w.nr_seq_trans_fin_contab:= c01_w.nr_seq_trans_fin_contab;
	titulo_receber_w.nr_seq_contrato		:= c01_w.nr_seq_contrato;	
	titulo_receber_w.vl_desc_previsto		:= c01_w.vl_desc_previsto;
	titulo_receber_w.nr_seq_carteira_cobr	:= c01_w.nr_seq_carteira_cobr;
	titulo_receber_w.dt_integracao_externa	:= c01_w.dt_integracao_externa;
	titulo_receber_w.cd_estab_financeiro	:= c01_w.cd_estab_financeiro;
	titulo_receber_w.nr_titulo_externo		:= c01_w.nr_titulo_externo;
	titulo_receber_w.nr_seq_mensalidade		:= c01_w.nr_seq_mensalidade;
	titulo_receber_w.nr_seq_mens_segurado	:= c01_w.nr_seq_mens_segurado;	
	titulo_receber_w.ie_pls					:= c01_w.ie_pls;
	titulo_receber_w.nr_seq_trans_fin_baixa	:= c01_w.nr_seq_trans_fin_baixa;
	titulo_receber_w.nr_nota_fiscal			:= c01_w.nr_nota_fiscal;
	titulo_receber_w.nm_usuario_orig		:= c01_w.nm_usuario_orig;
	titulo_receber_w.dt_inclusao			:= to_date(substr(c01_w.dt_inclusao,1,10),'dd/mm/yyyy');	
	titulo_receber_w.ie_tipo_carteira		:= c01_w.ie_tipo_carteira;
	titulo_receber_w.nr_seq_empresa			:= c01_w.nr_seq_empresa;
	titulo_receber_w.nr_seq_proj_rec		:= c01_w.nr_seq_proj_rec;
	titulo_receber_w.nr_seq_lote_empresa	:= c01_w.nr_seq_lote_empresa;
	titulo_receber_w.nr_seq_negociacao_origem	:= c01_w.nr_seq_negociacao_origem;
	titulo_receber_w.nr_seq_pls_lote_camara	:= c01_w.nr_seq_pls_lote_camara;
	titulo_receber_w.nr_seq_ptu_fatura		:= c01_w.nr_seq_ptu_fatura;
	titulo_receber_w.dt_ultima_devolucao	:= c01_w.dt_ultima_devolucao;
	titulo_receber_w.nr_seq_lote_enc_contas	:= c01_w.nr_seq_lote_enc_contas;
	titulo_receber_w.ie_liq_inadimplencia	:= c01_w.ie_liq_inadimplencia;
	titulo_receber_w.ie_negociado			:= c01_w.ie_negociado;
	titulo_receber_w.nr_seq_guia			:= c01_w.nr_seq_guia;
	titulo_receber_w.nr_titulo_dest			:= c01_w.nr_titulo_dest;
	titulo_receber_w.nr_seq_pag_conta_evento	:= c01_w.nr_seq_pag_conta_evento;
	titulo_receber_w.vl_outras_despesas		:= c01_w.vl_outras_despesas;
	titulo_receber_w.ie_dig_nosso_numero	:= c01_w.ie_dig_nosso_numero;
	titulo_receber_w.nr_seq_segurado_mens	:= c01_w.nr_seq_segurado_mens;
	titulo_receber_w.nr_seq_eme_fatura		:= c01_w.nr_seq_eme_fatura;
	titulo_receber_w.nr_seq_pls_lote_contest	:= c01_w.nr_seq_pls_lote_contest;
	titulo_receber_w.nr_seq_pls_lote_disc	:= c01_w.nr_seq_pls_lote_disc;
	titulo_receber_w.nr_seq_grupo_prod		:= c01_w.nr_seq_grupo_prod;
	titulo_receber_w.nr_seq_cob_previa		:= c01_w.nr_seq_cob_previa;
	titulo_receber_w.nr_seq_pagador			:= c01_w.nr_seq_pagador;
	titulo_receber_w.vl_juros_boleto		:= c01_w.vl_juros_boleto;
	titulo_receber_w.vl_multa_boleto		:= c01_w.vl_multa_boleto;
	titulo_receber_w.nr_rps					:= c01_w.nr_rps;
	titulo_receber_w.nr_livro				:= c01_w.nr_livro;
	titulo_receber_w.nr_seq_agrupamento		:= c01_w.nr_seq_agrupamento;
	titulo_receber_w.nr_duplicata			:= c01_w.nr_duplicata;
	titulo_receber_w.nr_lote_contabil_curto_prazo	:= c01_w.nr_lote_contabil_curto_prazo;
	titulo_receber_w.nr_seq_tf_curto_prazo	:= c01_w.nr_seq_tf_curto_prazo;
	titulo_receber_w.nr_titulo_ext_numerico	:= c01_w.nr_titulo_ext_numerico;
	titulo_receber_w.dt_credito_bancario	:= c01_w.dt_credito_bancario;
	titulo_receber_w.ie_prorrogacao_portal	:= c01_w.ie_prorrogacao_portal;
	titulo_receber_w.nr_sistema_ant			:= c01_w.nr_sistema_ant;
	titulo_receber_w.vl_multa_fixo			:= c01_w.vl_multa_fixo;
	titulo_receber_w.nr_seq_lote_prot		:= c01_w.nr_seq_lote_prot;
	titulo_receber_w.nr_seq_nota_deb_conclusao	:= c01_w.nr_seq_nota_deb_conclusao;
	titulo_receber_w.ie_data_juros_multa	:= c01_w.ie_data_juros_multa;	
	titulo_receber_w.dt_envio_serasa		:= c01_w.dt_envio_serasa;
	titulo_receber_w.nr_seq_cliente			:= c01_w.nr_seq_cliente;
	titulo_receber_w.vl_abatimento			:= c01_w.vl_abatimento;
	titulo_receber_w.dt_ultima_alteracao	:= c01_w.dt_ultima_alteracao;
	titulo_receber_w.dt_limite_desconto		:= c01_w.dt_limite_desconto;
	titulo_receber_w.vl_titulo_estrang		:= c01_w.vl_titulo_estrang;
	titulo_receber_w.nr_seq_ptu_fatura_old	:= c01_w.nr_seq_ptu_fatura_old;
	titulo_receber_w.vl_cotacao				:= c01_w.vl_cotacao;
	titulo_receber_w.ie_integra_unimed		:= c01_w.ie_integra_unimed;
	titulo_receber_w.ie_entrada_confirmada	:= c01_w.ie_entrada_confirmada;
		
	if (titulo_receber_w.cd_pessoa_fisica is not null) and (titulo_receber_w.cd_cgc is not null) then
		ds_inconsistencia_w := 'O t�tulo externo de sequ�ncia '||titulo_receber_w.nr_titulo_externo||' possui pessoa f�sica e jur�dica informado. Deve possuir apenas pessoa f�sica ou apenas pessoa jur�dica';
		aacd_gravar_incons_carga_tit(ds_inconsistencia_w, titulo_receber_w.nr_titulo_externo, nm_usuario_p, nr_lote_p, nm_rotina_w);
		qt_consistencia_w	:= nvl(qt_consistencia_w,0) + 1;
	end if;
	ds_inconsistencia_w := null;
	

	if (qt_consistencia_w = 0) then
	
		begin
		select	titulo_seq.nextval
		into	titulo_receber_w.nr_titulo
		from	dual;
		
		insert into titulo_receber values titulo_receber_w;
		exception when others then
			ds_inconsistencia_w := 'N�o foi poss�vel incluir o t�tulo '||titulo_receber_w.nr_titulo_externo||'. Detalhes: '||sqlerrm;
			aacd_gravar_incons_carga_tit(ds_inconsistencia_w, titulo_receber_w.nr_titulo_externo, nm_usuario_p, nr_lote_p, nm_rotina_w);
		end;

	end if;
	
	end;
end loop;
close C01;

delete 	w_import_tit_rec;

if (ie_commit_p = 'S') then
	commit;
end if;

end carga_titulo_receber;
/