create or replace
procedure w_pls_relat_reaj_contratual
			(	nr_seq_grupo_p		number,
				dt_inicial_p		date,
				dt_final_p		date,
				qt_usuarios_inicial_p	number,
				qt_usuarios_final_p	number,
				dt_reajuste_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 
			
type t_pls_relat_relacionamento_row is record (
	nr_seq_grupo		pls_util_cta_pck.t_number_table,
	ds_grupo_contrato	pls_util_cta_pck.t_varchar2_table_4000,
	vl_custo_total		pls_util_cta_pck.t_number_table,
	vl_receita_total	pls_util_cta_pck.t_number_table,
	qt_beneficiarios	pls_util_cta_pck.t_number_table,
	tx_resultado		pls_util_cta_pck.t_number_table, 
	dt_referencia		pls_util_cta_pck.t_date_table, 
	ie_tipo_relatorio	pls_util_cta_pck.t_varchar2_table_5
     );   
			
nr_seq_grupo_w			number(10);
vl_custo_benef_w		number(15,2);
vl_total_custo_w		number(15,2);
tx_resultado_w			number(15,4);
vl_mensalidade_w		number(15,2);
vl_mensalidade_tot_w		number(15,2);
tx_administrativa_w		number(15,4);
qt_beneficiarios_w		pls_integer;
qt_registros_w			pls_integer	:= 0;
dt_final_w			date;
t_pls_relat_relacionamento_w	t_pls_relat_relacionamento_row;

Cursor C06 (	dt_inicial_pc 		date,
		dt_final_pc 		date) is
	select	trunc(dt_mes,'Month') dt_inicial
	from	mes_v
	where	dt_mes between dt_inicial_pc and dt_final_pc;

Cursor C01 (	nr_seq_grupo_pc 	pls_grupo_contrato.nr_sequencia%type,
		dt_reajuste_pc		varchar2) is
	select	a.nr_sequencia	nr_seq_grupo,
		a.ds_grupo
	from	pls_grupo_contrato	a
	where	((a.nr_sequencia	= nr_seq_grupo_pc and nr_seq_grupo_pc is not null) or (nr_seq_grupo_pc is null))
	and	(to_char(a.dt_reajuste, 'mm') = dt_reajuste_pc or dt_reajuste_pc = '  ')
	and	a.ie_situacao		= 'A';

Cursor C02 (	nr_seq_grupo_pc 	pls_grupo_contrato.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_segurado
	from	pls_segurado		b,
		pls_contrato		a,
		pls_contrato_grupo	c
	where	b.nr_seq_contrato	= a.nr_sequencia
	and	c.nr_seq_contrato	= a.nr_sequencia
	and	c.nr_seq_grupo		= nr_seq_grupo_pc
	and	b.dt_liberacao is not null;
			
begin

delete	w_pls_relat_relacionamento
where	ie_tipo_relatorio	= 'RC';

select	max(tx_administrativa)
into	tx_administrativa_w
from	pls_competencia
where	cd_estabelecimento = cd_estabelecimento_p
and	dt_mes_competencia = trunc(sysdate,'Month');

if	(tx_administrativa_w is null) or
	(tx_administrativa_w = 0) then
	tx_administrativa_w	:= 1;
end if;

qt_registros_w := 0;

for r_c06_w in C06(dt_inicial_p, dt_final_p) loop
	begin
	dt_final_w	:= last_day(r_c06_w.dt_inicial);
	
	for r_c01_w in C01(nr_seq_grupo_p, dt_reajuste_p) loop
		begin
		select	count(1) qt_beneficiarios
		into	qt_beneficiarios_w
		from	pls_segurado		a,
			pls_contrato		b,
			pls_contrato_grupo	d,
			pls_grupo_contrato	c
		where	a.nr_seq_contrato	= b.nr_sequencia
		and	d.nr_seq_contrato	= b.nr_sequencia
		and	d.nr_seq_grupo		= c.nr_sequencia
		and	c.nr_sequencia		= r_c01_w.nr_seq_grupo
		and	nvl(a.dt_rescisao, sysdate +1) > sysdate;
		--qt_beneficiarios_w	:= pls_obter_dados_grupo_contrato(r_c01_w.nr_seq_grupo,'QV');
		
		vl_mensalidade_tot_w	:= 0;
		vl_total_custo_w	:= 0;
		
		if	(((qt_beneficiarios_w between qt_usuarios_inicial_p and qt_usuarios_final_p) and
			(qt_usuarios_final_p is not null) and
			(qt_usuarios_inicial_p is not null)) or
			(qt_usuarios_inicial_p is null) and
			(qt_usuarios_final_p is null)) then
			for r_c02_w in C02(r_c01_w.nr_seq_grupo) loop
				begin				
				select	sum(nvl(a.vl_mensalidade,0))
				into	vl_mensalidade_w
				from	pls_mensalidade			b,
					pls_mensalidade_segurado	a
				where	a.nr_seq_mensalidade		= b.nr_sequencia
				and	a.nr_seq_segurado		= r_c02_w.nr_seq_segurado
				and	a.dt_mesano_referencia  between r_c06_w.dt_inicial and dt_final_w
				and	b.dt_cancelamento is null;
				
				select	sum(nvl(B.VL_TOTAL,0))
				into	vl_custo_benef_w
				from	pls_conta		b
				where	b.nr_seq_segurado	= r_c02_w.nr_seq_segurado
				and	b.ie_status		= 'F'
				and	b.dt_atendimento_referencia between r_c06_w.dt_inicial and dt_final_w;

				vl_mensalidade_tot_w		:= vl_mensalidade_tot_w + nvl(vl_mensalidade_w,0);
				vl_total_custo_w		:= vl_total_custo_w + nvl(vl_custo_benef_w,0);
				
				end;
			end loop;
			
			if	(vl_total_custo_w <> 0) then
				vl_total_custo_w	:= (vl_total_custo_w * (tx_administrativa_w/100)) + vl_total_custo_w;
			end if;
			
			tx_resultado_w	:= pls_obter_perc_result(vl_mensalidade_tot_w,vl_total_custo_w);
			
			/* Grava no record para realizar os inserts no final */
			t_pls_relat_relacionamento_w.nr_seq_grupo(qt_registros_w) 	:= r_c01_w.nr_seq_grupo;
			t_pls_relat_relacionamento_w.ds_grupo_contrato(qt_registros_w) 	:= r_c01_w.ds_grupo;
			t_pls_relat_relacionamento_w.vl_custo_total(qt_registros_w) 	:= vl_total_custo_w;
			t_pls_relat_relacionamento_w.vl_receita_total(qt_registros_w) 	:= vl_mensalidade_tot_w;
			t_pls_relat_relacionamento_w.qt_beneficiarios(qt_registros_w) 	:= qt_beneficiarios_w;
			t_pls_relat_relacionamento_w.tx_resultado(qt_registros_w) 	:= tx_resultado_w;	
			t_pls_relat_relacionamento_w.dt_referencia(qt_registros_w) 	:= r_c06_w.dt_inicial;
			t_pls_relat_relacionamento_w.ie_tipo_relatorio(qt_registros_w) 	:= 'RC';			
			
			qt_registros_w := qt_registros_w + 1;
		end if;				
		end;
	end loop;
	end;
end loop;

if	(t_pls_relat_relacionamento_w.nr_seq_grupo.count > 0) then
	forall i in t_pls_relat_relacionamento_w.nr_seq_grupo.first..t_pls_relat_relacionamento_w.nr_seq_grupo.last
		insert into w_pls_relat_relacionamento
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_grupo,
				ds_grupo_contrato,
				vl_custo_total,
				vl_receita_total,
				qt_beneficiarios,
				tx_resultado, 
				dt_referencia, 
				ie_tipo_relatorio)
		values	(	w_pls_relat_relacionamento_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				t_pls_relat_relacionamento_w.nr_seq_grupo(i),
				t_pls_relat_relacionamento_w.ds_grupo_contrato(i),
				t_pls_relat_relacionamento_w.vl_custo_total(i),
				t_pls_relat_relacionamento_w.vl_receita_total(i),
				t_pls_relat_relacionamento_w.qt_beneficiarios(i),
				t_pls_relat_relacionamento_w.tx_resultado(i), 
				t_pls_relat_relacionamento_w.dt_referencia(i), 
				t_pls_relat_relacionamento_w.ie_tipo_relatorio(i));
				
	commit;
end if;

end w_pls_relat_reaj_contratual;
/