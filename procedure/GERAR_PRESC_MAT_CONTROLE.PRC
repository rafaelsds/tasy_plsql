create or replace
procedure gerar_presc_mat_controle    ( nr_prescricao_p number,
					nr_seq_prescricao_p number,
					cd_pessoa_biometria_p 	varchar2,
					nm_usuario_p	varchar2,
					ie_opcao_p	varchar2) is

nr_sequencia_w           number(10,0);
qt_registros_controle_w	 number(10,0);
nm_usuario_w		 varchar2(15);

begin

if  (nvl(nr_prescricao_p,0) > 0) and
    (nvl(nr_seq_prescricao_p,0) > 0) then
   
	nm_usuario_w := substr(obter_usuario_pf(cd_pessoa_biometria_p),1,15);
 
	insert into prescr_material_controle   (nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_prescricao,
						nr_seq_prescricao,
						ie_acao,
						dt_atualizacao_nrec, 
						nm_usuario_nrec)
	values                                 (prescr_material_controle_seq.nextval,
						sysdate,
						nvl(nm_usuario_w,nm_usuario_p),
						nr_prescricao_p,
						nr_seq_prescricao_p,
						ie_opcao_p,
						sysdate, 
						nm_usuario_p);	
	commit;
end if;	


end gerar_presc_mat_controle;
/