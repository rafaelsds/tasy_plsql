Create or Replace
procedure atualizar_data_recebimento2(
			nm_usuario_receb_p		Varchar2,
			cd_item_barra_p Number,
			nr_sequencia_p	number) is 

begin
update protocolo_doc_item
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_receb_p,	
	nm_usuario_receb = nm_usuario_receb_p,
	dt_recebimento = sysdate
where	nr_seq_interno = cd_item_barra_p
and	nr_sequencia = nr_sequencia_p;	
commit;

end atualizar_data_recebimento2;
/
