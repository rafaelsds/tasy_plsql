create or replace
procedure qt_grava_motivo_alteracao_data(	nr_seq_paciente_p		number,
					nr_seq_motivo_alter_data_p	number,
					nr_seq_atendimento_p	number,
					nm_usuario_p		Varchar2) is 

begin

if 	(nr_seq_paciente_p is not null) and 
	(nr_seq_motivo_alter_data_p is not null) then 
	update 	paciente_atendimento
	set 	nr_seq_motivo_alter_data = nr_seq_motivo_alter_data_p
	where 	nr_seq_paciente = nr_seq_paciente_p
	and		nr_seq_atendimento = nr_seq_atendimento_p;
end if;

commit;

end qt_grava_motivo_alteracao_data;
/
