create or replace
procedure pls_liberar_repasse_seg
		(	nr_seq_seg_repasse_p	number,
			nm_usuario_p		varchar2,
			ie_commit_p		varchar2) is

dt_liberacao_w			date;
nr_seq_segurado_w		number(15);
ie_tipo_segurado_ant_w		varchar2(1);
ie_cartao_provisorio_w		varchar2(1);
nr_seq_carteira_w		number(10);
nr_seq_motivo_via_adic_w	number(10);
nr_seq_tabela_repasse_w		number(10);
ie_tipo_repasse_w		varchar2(1);
dt_repasse_w			date;
dt_liberacao_benef_w		date;
dt_contratacao_w		date;
dt_rescisao_w			date;
nr_seq_repasse_seg_w		number(10);
qt_repasse_aberto_w		number(10);
nr_seq_congenere_atend_w	number(10);
cd_estabelecimento_w		number(10);
cd_cgc_outorgante_w		varchar2(14);
cd_cgc_atendimento_w		varchar2(14);
qt_registros_w			number(10);
ds_historico_benef_w		pls_segurado_repasse.ds_historico_benef%type;
ie_tipo_compartilhamento_w	pls_segurado_repasse.ie_tipo_compartilhamento%type;

begin

select	a.dt_liberacao,
	b.nr_sequencia nr_seq_segurado,
	a.ie_cartao_provisorio,
	a.nr_seq_motivo_via_adic,
	a.nr_seq_tabela,
	a.ie_tipo_repasse,
	a.dt_repasse,
	b.dt_liberacao,
	b.dt_contratacao,
	b.dt_rescisao,
	a.nr_sequencia,
	a.nr_seq_congenere_atend,
	b.cd_estabelecimento,
	a.ds_historico_benef,
	b.ie_tipo_segurado,
	a.ie_tipo_compartilhamento
into	dt_liberacao_w,
	nr_seq_segurado_w,
	ie_cartao_provisorio_w,
	nr_seq_motivo_via_adic_w,
	nr_seq_tabela_repasse_w,
	ie_tipo_repasse_w,
	dt_repasse_w,
	dt_liberacao_benef_w,
	dt_contratacao_w,
	dt_rescisao_w,
	nr_seq_repasse_seg_w,
	nr_seq_congenere_atend_w,
	cd_estabelecimento_w,
	ds_historico_benef_w,
	ie_tipo_segurado_ant_w,
	ie_tipo_compartilhamento_w
from	pls_segurado_repasse	a,
	pls_segurado		b
where	a.nr_seq_segurado	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_seg_repasse_p;

if	(dt_repasse_w < dt_contratacao_w) and
	(ie_tipo_compartilhamento_w = 1) then
	update	pls_segurado_repasse
	set	dt_repasse	= dt_contratacao_w
	where	nr_sequencia	= nr_seq_repasse_seg_w;
	
	dt_repasse_w	:= dt_contratacao_w;
end if;

if	((dt_rescisao_w is not null) and (dt_rescisao_w < dt_repasse_w)) and
	(ie_tipo_compartilhamento_w = 1) then
	wheb_mensagem_pck.exibir_mensagem_abort(1067528, 'DT_RESCISAO='||dt_rescisao_w);
end if;

if	(ie_tipo_repasse_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(194983);
end if;

if	(dt_repasse_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(194984);
end if;

if	(dt_liberacao_benef_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(194985);
end if;

if	(nr_seq_congenere_atend_w is not null) then
	select	max(cd_cgc_outorgante)
	into	cd_cgc_outorgante_w
	from	pls_outorgante
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(cd_cgc_outorgante_w is not null) then
		select	max(cd_cgc)
		into	cd_cgc_atendimento_w
		from	pls_congenere
		where	nr_sequencia	= nr_seq_congenere_atend_w;
		
		if	(cd_cgc_atendimento_w = cd_cgc_outorgante_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(202794); --Operadora de atendimento n�o pode ser a operadora do plano de sa�de do benefici�rio!
		end if;
	end if;
end if;

select	count(*)
into	qt_repasse_aberto_w
from	pls_segurado_repasse	b,
	pls_segurado		a
where	a.nr_sequencia = b.nr_seq_segurado
and	a.nr_sequencia = nr_seq_segurado_w
and	b.nr_seq_congenere_atend = nr_seq_congenere_atend_w
and	b.dt_fim_repasse is null
and	b.dt_liberacao is not null
and	b.nr_sequencia <> nr_seq_seg_repasse_p;

if	(nvl(qt_repasse_aberto_w,0) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1040430); --J� existe um compartilhamento de risco vigente para a mesma operadora. Favor verificar.
end if;

if	(nr_seq_congenere_atend_w is not null) and
	(ie_tipo_compartilhamento_w = 1) then
	select	count(1)
	into	qt_registros_w
	from	pls_regra_bloqueio_repasse
	where	nr_seq_congenere	= nr_seq_congenere_atend_w
	and	trunc(dt_repasse_w,'dd') between trunc(dt_inicio_bloqueio,'dd') and trunc(nvl(dt_fim_bloqueio,dt_repasse_w),'dd')
	and	dt_liberacao is not null;
	
	if	(qt_registros_w	> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(223159); --Cooperativa de atendimento est� bloqueada para atendimento. Favor verificar.
	end if;
end if;

if	(dt_liberacao_w	is null) then
	update	pls_segurado_repasse
	set	dt_liberacao	= sysdate
	where	nr_sequencia	= nr_seq_seg_repasse_p;
	
	if	((trunc(dt_repasse_w,'dd') <= trunc(sysdate,'dd')) and (ie_tipo_compartilhamento_w = 1)) then
		pls_alterar_segurado_repasase(	nr_seq_segurado_w, nr_seq_tabela_repasse_w, nr_seq_repasse_seg_w, 
						dt_repasse_w, ie_tipo_segurado_ant_w, ds_historico_benef_w || ' - pls_liberar_repasse_seg', 
						nm_usuario_p);
	end if;
	
	--Gerar uma via adicional caso esteja para esteja marcado para gerar um cart�o provis�rio
	if	(nvl(ie_cartao_provisorio_w,'N') = 'S') then
		select	max(nr_sequencia)
		into	nr_seq_carteira_w
		from	pls_segurado_carteira
		where	nr_seq_segurado	= nr_seq_segurado_w;
		
		pls_gerar_via_carteira(nr_seq_carteira_w,nr_seq_motivo_via_adic_w,nm_usuario_p,1202,'M','N');
	end if;
end if;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_liberar_repasse_seg;
/