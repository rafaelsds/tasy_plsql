create or replace
procedure Gerar_prescr_Fanep(cd_estabelecimento_p	in	number,
				nr_seq_pepo_p		in	number,
				nm_usuario_p		in	varchar2,
				nr_prescricao_p		out	number) is

nr_prescricao_w		number(10,0);
cd_setor_atendimento_w	number(10,0);
dt_inicio_w		date;
cd_medico_anestesista_w	varchar2(10);
cd_pessoa_fisica_w	varchar2(10);
ie_tipo_pessoa_w	number(01,0);
ie_forma_adep_w		varchar2(10);
ie_adep_w		varchar2(10);
nr_atendimento_w	number(10);
cd_setor_atend_prescr_pepo_w number(10,0);

begin
select	nvl(max(nr_prescricao), 0)
into	nr_prescricao_w
from	pepo_cirurgia
where	nr_sequencia = nr_seq_pepo_p;

if	(nr_prescricao_w = 0) then
	begin
	select	dt_inicio_proced,
		cd_medico_anestesista,
		cd_pessoa_fisica,
		nr_atendimento,
		cd_setor_atend_prescr
	into	dt_inicio_w,
		cd_medico_anestesista_w,
		cd_pessoa_fisica_w,
		nr_atendimento_w,
		cd_setor_atend_prescr_pepo_w
	from	pepo_cirurgia
	where	nr_sequencia = nr_seq_pepo_p;

	/*
	select	nvl(max(b.ie_tipo_pessoa),'1')
	into	ie_tipo_pessoa_w
	from	pessoa_fisica b,
		usuario a
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nm_usuario	= nm_usuario_p;*/
	
	select 	nvl(max('1'),'3')
	into	ie_tipo_pessoa_w
	from	Medico b, 
		Usuario a
	where 	a.nm_usuario		= nm_usuario_p
	and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

	select 	obter_setor_usuario(nm_usuario_p)
	into	cd_setor_atendimento_w
	from	dual;
	
	select	prescr_medica_SEQ.NEXTVAL
	into	nr_prescricao_w
	from	dual;
	
	Obter_Param_Usuario(924,246,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_forma_adep_w);
		
	if	(ie_forma_adep_w = 'DS') then
		select	nvl(max(ie_adep),'N')
		into	ie_adep_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;
	elsif	(ie_forma_adep_w = 'NV') then
		ie_adep_w   := 'N';
	elsif	(ie_forma_adep_w = 'PV') then
		ie_adep_w := 'S';
	elsif	(ie_forma_adep_w = 'PNV') then
		ie_adep_w := 'N';
	else	
		ie_adep_w := 'S';
	end if;

	insert	into prescr_medica(
		dt_prescricao,
		cd_medico,
		cd_pessoa_fisica,
		cd_estabelecimento,
		nm_usuario_original,
		nr_horas_validade,
		nr_prescricao,
		ie_recem_nato,
		ie_origem_inf,
		dt_primeiro_horario,
		nm_usuario,
		dt_atualizacao,
		cd_prescritor,
		ie_adep,
		nr_atendimento,
		cd_setor_atendimento)
	values(	sysdate,
		cd_medico_anestesista_w,
		cd_pessoa_fisica_w,
		cd_estabelecimento_p,
		nm_usuario_p,
		24,
		nr_prescricao_w,
		'N', 
		ie_tipo_pessoa_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		obter_dados_usuario_opcao(nm_usuario_p, 'C'),
		ie_adep_w,
		nr_atendimento_w,
		cd_setor_atend_prescr_pepo_w);
	end;

end if;

commit;

nr_prescricao_p	:= nr_prescricao_w;

end Gerar_prescr_Fanep;
/