create or replace 
procedure pls_recalcular_preco_benef
			(	nr_seq_segurado_p	in	number,
				cd_motivo_recalculo_p	in	varchar2,
				ie_liberado_p		in	varchar2,
				dt_reajuste_p		in	date,
				ie_commit_p		in	varchar2,
				nm_usuario_p		in	varchar2,
				cd_estabelecimento_p	in	number) is

/*	cd_motivo_recalculo_p DOMINIO 1728 - PLS - Motivo reajuste da mensalidade */
qt_idade_w			number(10);
nr_seq_preco_w			number(10);
nr_seq_tabela_w			number(10);
vl_preco_atual_w		number(15,2);
vl_preco_ant_w			number(15,2);
nm_pessoa_fisica_w		varchar2(255);
nr_seq_segurado_preco_w		number(10);
nm_beneficiario_w		varchar2(255);
ie_preco_w			varchar2(2);
nr_seq_seg_preco_ant_w		number(10);
nr_seq_reajuste_w		number(10);
nr_seq_reajuste_ww		number(10);
dt_reajuste_w			date;
ie_geracao_valores_w		varchar2(1);
ie_titularidade_w		varchar2(1);
qt_idade_limite_reaj_w		number(3);
qt_anos_limite_reaj_w		number(3);
qt_idade_limite_reaj_contr_w	number(3);
qt_anos_limite_reaj_contr_w	number(3);
qt_idade_limite_reaj_param_w	number(3);
qt_anos_limite_reaj_param_w	number(3);
ds_observacao_w			varchar2(255);
qt_idade_operadora_w		number(10);
dt_inclusao_operadora_w		date;
vl_minimo_mensalidade_w		number(15,2);
qt_vidas_w			number(10)	:= 0;
vl_preco_nao_subsid_desc_w	number(15,2);
ie_grau_parentesco_w		varchar2(2);
ie_calculo_vidas_w		varchar2(2);
nr_seq_contrato_w		number(10);
dt_liberacao_w			date		:= null;
nm_usuario_liberacao_w		varchar2(15)	:= null;
nr_seq_preco_ant_w		number(10);
vl_preco_nao_subsid_desc_ant_w	number(15,2);
qt_idade_adesao_w		number(10);
dt_nascimento_w			date;
ie_regulamentacao_w		varchar2(2);
ie_preco_vidas_contrato_w	varchar2(1);
ie_data_ref_reaj_adaptado_w	varchar2(1);
dt_contratacao_w		date;
nr_seq_mtvo_alteracao_w		number(10);
dt_adaptacao_w			date;
dt_base_inclusao_w		date;
vl_adaptacao_w			number(15,2);
cd_motivo_recalculo_w		varchar2(2);
cd_matricula_familia_w		number(10);
nr_seq_titular_w		number(10);
ie_reaj_idade_limite_pre_w	varchar2(1);
nr_seq_tabela_ant_w		number(10);
vl_reajuste_w			pls_reajuste_preco.vl_reajustado%type;
tx_desconto_w			pls_preco_regra.tx_desconto%type;
nr_seq_regra_desconto_w		pls_regra_desconto.nr_sequencia%type;
vl_desconto_w			pls_segurado_preco.vl_desconto%type;
qt_idade_limite_reaj_benef_w	pls_segurado.qt_idade_limite_reaj%type;
qt_anos_limite_reaj_benef_w	pls_segurado.qt_anos_limite_reaj%type;
dt_reajuste_trunc_w		date;	
ie_reajusta_fx			varchar2(1);
qt_idade_ww			number(10);
nr_seq_reajuste_www		pls_reajuste_preco.nr_sequencia%type;
vl_preco_atual_ww		number(15,2);
nr_seq_reaj_fx_w		pls_segurado_preco.nr_sequencia%type;
dt_reajuste_ww			date;
ie_limite_fx_w			varchar2(1);
dt_reajuste_indc_w		date;
nr_seq_seg_preco_ind_w		pls_segurado_preco.nr_sequencia%type;
ds_observacao_ww		varchar2(255);
ie_gerou_indc_w			varchar2(1);
vl_preco_inicial_w		number(15,2);

cursor c02 is	-- select das faixas etarias
	select	nr_sequencia,
		nvl(vl_preco_atual,0),
		vl_preco_nao_subsid_atual,
		nvl(vl_minimo,0),
		nvl(vl_adaptacao,0),
		nvl(vl_preco_inicial,0)
	from	pls_plano_preco
	where	nvl(qt_idade_w,qt_idade_ww)	>= qt_idade_inicial
	and	nvl(qt_idade_w,qt_idade_ww)	<= qt_idade_final
	and	nr_seq_tabela	= nr_seq_tabela_w
	and	nvl(ie_grau_titularidade,ie_grau_parentesco_w)	= ie_grau_parentesco_w
	and	qt_vidas_w between nvl(qt_vidas_inicial,qt_vidas_w) and nvl(qt_vidas_final,qt_vidas_w)
	order	by	nvl(ie_grau_titularidade,' ');

begin

ds_observacao_w	:= '';
cd_motivo_recalculo_w	:= cd_motivo_recalculo_p;

select	nvl(ie_reaj_idade_limite_pre,'S'),
	qt_idade_limite,
	qt_tempo_limite,
	ie_data_ref_reaj_adaptado
into	ie_reaj_idade_limite_pre_w,
	qt_idade_limite_reaj_param_w,
	qt_anos_limite_reaj_param_w,
	ie_data_ref_reaj_adaptado_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_liberado_p = 'S') then
	dt_liberacao_w		:= sysdate;
	nm_usuario_liberacao_w	:= nm_usuario_p;
end if;

select	max(nr_sequencia)
into	nr_seq_seg_preco_ant_w
from	pls_segurado_preco
where	dt_liberacao is not null
and	nr_seq_segurado	= nr_seq_segurado_p
and	dt_reajuste = (	select	max(dt_reajuste)
			from	pls_segurado_preco
			where	dt_liberacao is not null
			and	nr_seq_segurado	= nr_seq_segurado_p
			and	dt_reajuste <= dt_reajuste_p);

if	(dt_reajuste_p is not null) then
	dt_reajuste_w	:= dt_reajuste_p;
else
	dt_reajuste_w	:= trunc(sysdate,'month');
end if;

ie_limite_fx_w		:= 'N';
ie_reajusta_fx		:= 'N';
ie_gerou_indc_w		:= 'N';
dt_reajuste_trunc_w	:= trunc(dt_reajuste_w,'month');

select	to_number(obter_idade(a.dt_nascimento,nvl(dt_reajuste_p, trunc(sysdate,'dd')),'A')),
	b.nr_seq_tabela,
	d.ie_preco,
	a.nm_pessoa_fisica,
	decode(b.nr_seq_titular,null,'T','D'),
	b.dt_inclusao_operadora,
	nvl(substr(pls_obter_garu_dependencia_seg(b.nr_sequencia,'C'),1,2),'X'),
	b.nr_seq_contrato,
	a.dt_nascimento,
	d.ie_regulamentacao,
	b.dt_contratacao,
	b.cd_matricula_familia,
	b.nr_seq_titular,
	b.qt_idade_limite_reaj,
	b.qt_anos_limite_reaj
into	qt_idade_w,
	nr_seq_tabela_w,
	ie_preco_w,
	nm_beneficiario_w,
	ie_titularidade_w,
	dt_inclusao_operadora_w,
	ie_grau_parentesco_w,
	nr_seq_contrato_w,
	dt_nascimento_w,
	ie_regulamentacao_w,
	dt_contratacao_w,
	cd_matricula_familia_w,
	nr_seq_titular_w,
	qt_idade_limite_reaj_benef_w,
	qt_anos_limite_reaj_benef_w
from	pessoa_fisica	a,
	pls_segurado	b,
	pls_plano	d
where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
and	b.nr_seq_plano		= d.nr_sequencia
--and	b.dt_liberacao 		is not null Comentado pois pode estar reativando o beneficiario
and	b.cd_estabelecimento	= cd_estabelecimento_p
and	b.nr_sequencia		= nr_seq_segurado_p;

ie_geracao_valores_w	:= 'B';

if	(dt_contratacao_w > dt_reajuste_w) then
	dt_reajuste_w := dt_contratacao_w;
end if;

if	(nr_seq_contrato_w is not null) then
	select	nvl(ie_geracao_valores,'B'),
		qt_idade_limite_reaj,
		qt_anos_limite_reaj
	into	ie_geracao_valores_w,
		qt_idade_limite_reaj_contr_w,
		qt_anos_limite_reaj_contr_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
end if;

if	(ie_geracao_valores_w <> 'B') then
	if	((ie_geracao_valores_w = 'T') and (ie_titularidade_w = 'D')) then
		wheb_mensagem_pck.exibir_mensagem_abort( 186265, null);
		/* Nao e possivel gerar valor para beneficiario dependente! Favor verifique no cadastro do contrato. */
	elsif	((ie_geracao_valores_w = 'D') and (ie_titularidade_w = 'T')) then
		wheb_mensagem_pck.exibir_mensagem_abort( 186267, null);
		/* Nao e possivel gerar valor para beneficiario titular! Favor verifique no cadastro do contrato. */
	end if;
end if;

if	((nr_seq_tabela_w is null) and (ie_preco_w = '1')) then /* Somente e obrigatorio existir tabela para produto com formacao de preco pre estabelecido */
	wheb_mensagem_pck.exibir_mensagem_abort( 186268, 'NR_SEQ_SEGURADO='||nr_seq_segurado_p||';'||'NM_SEGURADO='||nm_beneficiario_w);
	/* Nao existe tabela de preco informada para o beneficiario */
end if;

if	(nr_seq_tabela_w is not null) then
	if	(qt_idade_w > 999) then
		wheb_mensagem_pck.exibir_mensagem_abort( 186269, 'NR_SEQ_SEGURADO='||nr_seq_segurado_p||';'||'NM_SEGURADO='||nm_beneficiario_w||';'||'QT_IDADE='||qt_idade_w );
		/* O beneficiario 661 - Paulo Rosa possui 1200 anos. Verifique. */
	end if;
	
	/* Bsucar a data base para verificar a adesao do beneficiario quando for plano adaptado */
	if	(ie_regulamentacao_w in ('A','P')) then
		if	(ie_data_ref_reaj_adaptado_w = 'A') then
			dt_base_inclusao_w	:= dt_contratacao_w;
		elsif	(ie_data_ref_reaj_adaptado_w = 'D') then
			select	max(nr_sequencia)
			into	nr_seq_mtvo_alteracao_w
			from	pls_motivo_alteracao_plano
			where	cd_ans	= '12';
			
			select	max(dt_alteracao)
			into	dt_adaptacao_w
			from	pls_segurado_alt_plano
			where	nr_seq_segurado		= nr_seq_segurado_p
			and	ie_situacao 		= 'A'
			and	nr_seq_motivo_alt	= nr_seq_mtvo_alteracao_w;
			
			if	(dt_adaptacao_w is not null) then
				dt_base_inclusao_w	:= dt_adaptacao_w;
			else
				dt_base_inclusao_w	:= dt_inclusao_operadora_w;
			end if;
		else
			dt_base_inclusao_w	:= dt_inclusao_operadora_w;
		end if;
	else
		dt_base_inclusao_w	:= dt_inclusao_operadora_w;
	end if;	
		
	if	(nvl(nr_seq_seg_preco_ant_w,0) > 0) then
		select	vl_preco_atual,
			nr_seq_reajuste,
			vl_preco_nao_subsid_desc,
			nr_seq_preco,
			nr_seq_tabela
		into	vl_preco_ant_w,
			nr_seq_reajuste_w,
			vl_preco_nao_subsid_desc_ant_w,
			nr_seq_preco_ant_w,
			nr_seq_tabela_ant_w
		from	pls_segurado_preco
		where	nr_sequencia	= nr_seq_seg_preco_ant_w;
		
		if	(nr_seq_tabela_ant_w <> nr_seq_tabela_w) then
			nr_seq_reajuste_w	:= null;
		end if;
	end if;
	
	-- Consistencia de idade limite para reajuste por faixa etaria
	if	((ie_regulamentacao_w <> 'R') or (ie_regulamentacao_w = 'R' and ie_reaj_idade_limite_pre_w = 'S')) then
		qt_idade_limite_reaj_w	:= 0;
		qt_anos_limite_reaj_w	:= 0;
		
		if	(nvl(qt_idade_limite_reaj_benef_w,0) > 0) or
			(nvl(qt_anos_limite_reaj_benef_w,0) > 0) then
			qt_idade_limite_reaj_w	:= nvl(qt_idade_limite_reaj_benef_w,0);
			qt_anos_limite_reaj_w	:= nvl(qt_anos_limite_reaj_benef_w,0);
		elsif	(nvl(qt_idade_limite_reaj_contr_w,0) > 0) or
			(nvl(qt_anos_limite_reaj_contr_w,0) > 0) then
			qt_idade_limite_reaj_w	:= nvl(qt_idade_limite_reaj_contr_w,0);
			qt_anos_limite_reaj_w	:= nvl(qt_anos_limite_reaj_contr_w,0);
		elsif	(nvl(qt_idade_limite_reaj_param_w,0) > 0) or
			(nvl(qt_anos_limite_reaj_param_w,0) > 0) then
			qt_idade_limite_reaj_w	:= nvl(qt_idade_limite_reaj_param_w,0);
			qt_anos_limite_reaj_w	:= nvl(qt_anos_limite_reaj_param_w,0);
		else
			/* Se nenhum campo de idade e anos limite for preenchido o sistema sempre realiza o reajuste de mudanca de faixa etaria */
			qt_idade_limite_reaj_w	:= 999;
			qt_anos_limite_reaj_w	:= 999;
		end if;
		qt_idade_operadora_w	:= round(to_number(obter_idade(dt_base_inclusao_w, dt_reajuste_w, 'A')));
		
		if	(qt_idade_w >= qt_idade_limite_reaj_w) and
			(qt_idade_operadora_w >= qt_anos_limite_reaj_w) then
			qt_idade_adesao_w	:= to_number(obter_idade(dt_nascimento_w,dt_base_inclusao_w,'A'));
			
			if	(qt_idade_adesao_w > qt_idade_limite_reaj_w) then
				qt_idade_w	:= qt_idade_adesao_w + qt_anos_limite_reaj_w; /* Se na data de adesao, o beneficiario ja tinha 60 anos ou mais, deve gerar sempre com essa idade */
			else
				qt_idade_w	:= qt_idade_adesao_w + qt_anos_limite_reaj_w;
				
				if	(qt_idade_w < qt_idade_limite_reaj_w) then
					qt_idade_w	:= qt_idade_limite_reaj_w-1;
				end if;
				ds_observacao_w	:= wheb_mensagem_pck.get_texto(1109058);
			end if;
			
			ie_limite_fx_w	:= 'S';
		end if;
	end if;
	
	begin
	select	nvl(ie_preco_vidas_contrato,'N'),
		nvl(ie_calculo_vidas,'A')
	into	ie_preco_vidas_contrato_w,
		ie_calculo_vidas_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_w;
	exception
	when others then
		ie_calculo_vidas_w	:= 'A';
	end;
	
	if	(ie_preco_vidas_contrato_w = 'S') then
		if	(ie_calculo_vidas_w = 'A') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (trunc(dt_rescisao,'mm') > dt_reajuste_trunc_w));
		elsif	(ie_calculo_vidas_w = 'T') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (trunc(dt_rescisao,'mm') > dt_reajuste_trunc_w));
		elsif	(ie_calculo_vidas_w = 'D') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is not null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (trunc(dt_rescisao,'mm') > dt_reajuste_trunc_w));
		elsif	(ie_calculo_vidas_w = 'TD') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado a
			where	a.nr_seq_contrato = nr_seq_contrato_w
			and	a.dt_liberacao is not null
			and	((a.dt_rescisao is null) or (trunc(a.dt_rescisao,'mm') > dt_reajuste_trunc_w))
			and	((nr_seq_titular is null) or ((nr_seq_titular is not null) and ((select	count(*)
												from	grau_parentesco x
												where	x.nr_sequencia = a.nr_seq_parentesco
												and	x.ie_tipo_parentesco = '1') > 0)));
		elsif	(ie_calculo_vidas_w = 'F') then
			qt_vidas_w	:= nvl(pls_obter_qt_vida_benef(nr_seq_segurado_p,nr_seq_titular_w,dt_reajuste_trunc_w),0);
		end if;
	else
		qt_vidas_w	:= 1;
	end if;

	open c02;
	loop
	fetch c02 into
		nr_seq_preco_w,
		vl_preco_atual_w,
		vl_preco_nao_subsid_desc_w,
		vl_minimo_mensalidade_w,
		vl_adaptacao_w,
		vl_preco_inicial_w;
	exit when c02%notfound;
	end loop;
	close c02;	

	if	(cd_motivo_recalculo_w in ('B', 'R')) then
		vl_preco_atual_w := vl_preco_inicial_w;
	end if;
	
	/* Obter o reajuste vigente pela data do reajuste */
	select	max(a.nr_sequencia)
	into	nr_seq_reajuste_ww
	from	pls_reajuste_preco a
	where	a.nr_seq_preco	= nr_seq_preco_w
	and	trunc(a.dt_reajuste, 'month') <= trunc(dt_reajuste_w, 'month');	
	
	if 	(nr_seq_reajuste_ww is not null) then
		select	nvl(vl_reajustado,0)
		into	vl_preco_atual_w
		from	pls_reajuste_preco
		where	nr_sequencia	= nr_seq_reajuste_ww;
	end if;
	
	if	(nvl(nr_seq_reajuste_ww,0) > nvl(nr_seq_reajuste_w,0)) then
		nr_seq_reajuste_w	:= nr_seq_reajuste_ww;
	end if;
	
	if	(nvl(vl_preco_ant_w,0) <> nvl(vl_preco_atual_w,0)) or
		(nvl(vl_preco_nao_subsid_desc_w,0) <> nvl(vl_preco_nao_subsid_desc_ant_w,0)) or
		(nr_seq_preco_ant_w <> nr_seq_preco_w) or
		(cd_motivo_recalculo_w = 'AR') then

		/* Se for aniversario do beneficiario, deve gerar o preco como mudanca de faixa etaria */
		if	(to_char(dt_nascimento_w,'mm') = to_char(dt_reajuste_w,'mm') or 
			to_char(dt_nascimento_w,'mm') = to_char(add_months(dt_reajuste_w,1),'mm')) and
			(nr_seq_preco_ant_w <> nr_seq_preco_w) then
			ie_reajusta_fx		:= 'S';
			
			if	(cd_motivo_recalculo_w = 'Q') then
				ds_observacao_w		:= wheb_mensagem_pck.get_texto(1109052);
			else
				ds_observacao_w		:= wheb_mensagem_pck.get_texto(1109056);
			end if;
		end if;

		pls_obter_regra_desconto(nr_seq_segurado_p,1,cd_estabelecimento_p,tx_desconto_w,nr_seq_regra_desconto_w);
		
		vl_desconto_w := vl_preco_atual_w * (tx_desconto_w /100);
		
		insert	into pls_segurado_preco
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, dt_reajuste,
			nr_seq_segurado, vl_preco_atual, qt_idade,
			cd_motivo_reajuste, vl_preco_ant, ds_observacao,
			nr_seq_reajuste, nr_seq_preco, dt_liberacao,
			nm_usuario_liberacao, vl_desconto, vl_preco_nao_subsid_desc,
			nr_seq_tabela, vl_minimo_mensalidade, vl_adaptacao,
			ie_situacao)
		values	(pls_segurado_preco_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, trunc(dt_reajuste_w,'dd'),
			nr_seq_segurado_p, nvl(vl_preco_atual_w,0), qt_idade_w,
			cd_motivo_recalculo_w, nvl(vl_preco_ant_w,0), ds_observacao_w,
			nr_seq_reajuste_w, nr_seq_preco_w, dt_liberacao_w,
			nm_usuario_liberacao_w, vl_desconto_w, vl_preco_nao_subsid_desc_w,
			nr_seq_tabela_w, vl_minimo_mensalidade_w, vl_adaptacao_w,
			'A');
			
		select	max(nr_sequencia)
		into	nr_seq_reaj_fx_w
		from	pls_segurado_preco
		where	ie_situacao = 'A'
		and	cd_motivo_reajuste = 'E'
		and	dt_reajuste >= trunc(dt_reajuste_w,'mm')
		and	nr_seq_segurado = nr_seq_segurado_p;
			
		if	(nvl(nr_seq_reaj_fx_w,0) > 0) and (ie_reajusta_fx = 'S') then
			update	pls_segurado_preco
			set	ie_situacao = 'I',
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p,
				ds_observacao = 'Registro inativado pela procedure pls_recalcular_preco_benef.'
			where	nr_sequencia = nr_seq_reaj_fx_w;
		end if;
		
		select	max(nr_sequencia)
		into	nr_seq_seg_preco_ind_w
		from	pls_segurado_preco
		where	nr_seq_segurado = nr_seq_segurado_p
		and	ie_situacao 	= 'A'
		and	dt_liberacao is not null
		and	cd_motivo_reajuste = 'I'
		and	trunc(dt_reajuste_w,'mm') < trunc(dt_reajuste,'mm');
		
		if	((cd_motivo_recalculo_p in ('Q','R')) and
			(nr_seq_seg_preco_ind_w is not null)) then
			dt_reajuste_ww 	:= trunc(dt_reajuste_indc_w,'dd');

			qt_idade_w			:= null;
			nr_seq_preco_w 			:= null;
			vl_preco_nao_subsid_desc_w 	:= null;
			vl_minimo_mensalidade_w 	:= null;
			vl_adaptacao_w 			:= null;
			tx_desconto_w			:= null;
			vl_desconto_w			:= null;
			nr_seq_reajuste_www		:= null;

			select	max(dt_reajuste),
				max(qt_idade)
			into	dt_reajuste_indc_w,
				qt_idade_ww
			from	pls_segurado_preco	
			where	nr_sequencia = nr_seq_seg_preco_ind_w;
			
			open C02;
			loop
			fetch C02 into	
				nr_seq_preco_w,
				vl_preco_atual_ww,
				vl_preco_nao_subsid_desc_w,
				vl_minimo_mensalidade_w,
				vl_adaptacao_w,
				vl_preco_inicial_w;
			exit when C02%notfound;
			end loop;
			close C02;
			
			select	max(a.nr_sequencia)
			into	nr_seq_reajuste_www
			from	pls_reajuste_preco a
			where	a.nr_seq_preco	= nr_seq_preco_w
			and	trunc(a.dt_reajuste, 'month') <= trunc(dt_reajuste_indc_w, 'month');

			if	(nvl(nr_seq_reajuste_www,0) <> 0) then
				select	nvl(vl_reajustado,0)
				into	vl_preco_atual_ww
				from	pls_reajuste_preco
				where	nr_sequencia	= nr_seq_reajuste_www;
			end if;

			ds_observacao_ww	:= wheb_mensagem_pck.get_texto(1109048);
			
			if	(nr_seq_preco_w is not null) then
			
				pls_obter_regra_desconto(nr_seq_segurado_p,1,cd_estabelecimento_p,tx_desconto_w,nr_seq_regra_desconto_w);
			
				vl_desconto_w := vl_preco_atual_ww * (tx_desconto_w /100);
			
				insert	into pls_segurado_preco
					(nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, dt_reajuste,
					nr_seq_segurado, vl_preco_atual, qt_idade,
					cd_motivo_reajuste, vl_preco_ant, ds_observacao,
					nr_seq_reajuste, nr_seq_preco, dt_liberacao,
					nm_usuario_liberacao, vl_desconto, vl_preco_nao_subsid_desc,
					nr_seq_tabela, vl_minimo_mensalidade, vl_adaptacao,
					ie_situacao)
				values	(pls_segurado_preco_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, dt_reajuste_indc_w,
					nr_seq_segurado_p, nvl(vl_preco_atual_ww,0), qt_idade_ww,
					'I', nvl(vl_preco_atual_w,0), ds_observacao_ww,
					nr_seq_reajuste_www, nr_seq_preco_w, nvl(dt_liberacao_w,sysdate),
					nm_usuario_liberacao_w, vl_desconto_w, vl_preco_nao_subsid_desc_w,
					nr_seq_tabela_w, vl_minimo_mensalidade_w, vl_adaptacao_w,
					'A');
				
				ie_gerou_indc_w	:= 'S';
			end if;
		end if;

		
		if	(ie_reajusta_fx = 'S') and (ie_limite_fx_w = 'N') and (ie_gerou_indc_w = 'N') then
			if	(cd_motivo_recalculo_w = 'Q') then
				qt_idade_ww	:= to_number(obter_idade(dt_nascimento_w,nvl(last_day(add_months(dt_reajuste_p,1)),last_day(sysdate)),'A'));
				dt_reajuste_ww	:= to_char(trunc(dt_nascimento_w,'mm'),'dd/mm') || '/' || to_char(dt_reajuste_w,'yyyy');
			else
				qt_idade_ww	:= to_number(obter_idade(dt_nascimento_w,nvl(last_day(dt_reajuste_p),last_day(sysdate)),'A'));
				dt_reajuste_ww 	:= trunc(dt_reajuste_w,'dd');
			end if;
			
			if	(nvl(qt_idade_ww,0) > 0) and
				(qt_idade_w < qt_idade_ww) then
				
				--Limpar a variavel qt_idade_w para poder usar a qt_idade_ww no cursor C02 e buscar o preco da nova faixa etaria
				qt_idade_w			:= null;
				nr_seq_preco_w 			:= null;
				vl_preco_nao_subsid_desc_w 	:= null;
				vl_minimo_mensalidade_w 	:= null;
				vl_adaptacao_w 			:= null;
				tx_desconto_w			:= null;
				vl_desconto_w			:= null;
				nr_seq_reajuste_www		:= null;
				
				open C02;
				loop
				
				fetch C02 into
					nr_seq_preco_w,
					vl_preco_atual_ww,
					vl_preco_nao_subsid_desc_w,
					vl_minimo_mensalidade_w,
					vl_adaptacao_w,
					vl_preco_inicial_w;
				exit when C02%notfound;
				end loop;
				close C02;
				
				select	max(a.nr_sequencia)
				into	nr_seq_reajuste_www
				from	pls_reajuste_preco a
				where	a.nr_seq_preco	= nr_seq_preco_w
				and	trunc(a.dt_reajuste, 'month') <= trunc(dt_reajuste_w, 'month');

				if	(nvl(nr_seq_reajuste_www,0) <> 0) then
					select	nvl(vl_reajustado,0)
					into	vl_preco_atual_ww
					from	pls_reajuste_preco
					where	nr_sequencia	= nr_seq_reajuste_www;
				end if;
				
				if	(nr_seq_preco_w is not null) then					
					
					pls_obter_regra_desconto(nr_seq_segurado_p,1,cd_estabelecimento_p,tx_desconto_w,nr_seq_regra_desconto_w);

					vl_desconto_w := vl_preco_atual_ww * (tx_desconto_w /100);
					
					insert	into pls_segurado_preco
						(nr_sequencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, dt_reajuste,
						nr_seq_segurado, vl_preco_atual, qt_idade,
						cd_motivo_reajuste, vl_preco_ant, ds_observacao,
						nr_seq_reajuste, nr_seq_preco, dt_liberacao,
						nm_usuario_liberacao, vl_desconto, vl_preco_nao_subsid_desc,
						nr_seq_tabela, vl_minimo_mensalidade, vl_adaptacao,
						ie_situacao)
					values	(pls_segurado_preco_seq.nextval, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, dt_reajuste_ww,
						nr_seq_segurado_p, nvl(vl_preco_atual_ww,0), qt_idade_ww,
						'E', nvl(vl_preco_atual_w,0), ds_observacao_w,
						nr_seq_reajuste_www, nr_seq_preco_w, dt_liberacao_w,
						nm_usuario_liberacao_w, vl_desconto_w, vl_preco_nao_subsid_desc_w,
						nr_seq_tabela_w, vl_minimo_mensalidade_w, vl_adaptacao_w,
						'A');
				end if;
			end if;
		end if;
	end if;
end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_recalcular_preco_benef;
/
