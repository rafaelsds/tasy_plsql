create or replace
procedure alterar_protoc_conv_geracao(
			nr_seq_protocolo_p		number,
			nm_usuario_p		varchar2) is 

begin

if (nr_seq_protocolo_p is not null) then
update	protocolo_convenio 
set	nm_usuario_geracao  = nm_usuario_p, 
	dt_geracao          = sysdate,
	dt_atualizacao      = sysdate,
	nm_usuario          = nm_usuario_p
where nr_seq_protocolo = nr_seq_protocolo_p;
end if;

commit;

end alterar_protoc_conv_geracao;
/