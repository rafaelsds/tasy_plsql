create or replace 
procedure Obter_Se_Regra_Disp_Pepo(cd_material_p		in	number,
				ie_origem_gasto_p		in 	varchar,
				ie_possui_regra_p		out 	varchar,
				ie_permite_alterar_p 		out 	varchar,
				ds_origem_gasto_p		out 	varchar,
				ie_origem_padrao_p		out	varchar)
				is
														

cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
ie_permite_alterar_w	varchar2(1);
ie_possui_regra_w		varchar2(1) := 'N';
ds_origem_gasto_w		varchar2(255);
ie_origem_gasto_w		varchar2(255);

Cursor C01 is
	select 	ie_permite_alterar,
		obter_valor_dominio(1815,ie_origem_gasto),
		ie_origem_gasto
	from  	origem_gasto_material
	where	nvl(cd_grupo_material,cd_grupo_material_w) 		= cd_grupo_material_w
	and	nvl(cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
	and	nvl(cd_classe_material,cd_classe_material_w) 		= cd_classe_material_w
	and	nvl(cd_material,cd_material_p) 				= cd_material_p
	order by nvl(cd_grupo_material,9999999999),
		nvl(cd_subgrupo_material,9999999999),
		nvl(cd_classe_material,9999999999),
		nvl(cd_material,9999999999);

begin

select	Obter_estrutura_material(cd_material,'G'),
	Obter_estrutura_material(cd_material,'S'),
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w			
from	material
where	cd_material = cd_material_p;			

open C01;
loop
fetch C01 into	
	ie_permite_alterar_w,
	ds_origem_gasto_w,
	ie_origem_gasto_w;
exit when C01%notfound;
	begin
	ie_permite_alterar_w	:= ie_permite_alterar_w;
	ds_origem_gasto_w	:= ds_origem_gasto_w;	
	ie_possui_regra_w	:= 'S';
	end;
end loop;
close C01;			

if	(ie_origem_gasto_w <> ie_origem_gasto_p) then
	ie_possui_regra_p		:= ie_possui_regra_w;	
	ie_permite_alterar_p		:= ie_permite_alterar_w;	
	ds_origem_gasto_p		:= ds_origem_gasto_w;
	ie_origem_padrao_p		:= ie_origem_gasto_w;
end if;	
			
end	Obter_Se_Regra_Disp_Pepo;
/