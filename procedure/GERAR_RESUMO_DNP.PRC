create or replace
procedure gerar_resumo_dnp (cd_estabelecimento_p		number,
				nr_seq_consulta_p 		number,	
				cd_pessoa_fisica_p		varchar2,
				ie_mostra_todos_p		varchar2,
				nr_seq_item_p			number,
				nm_usuario_p			varchar2) as 


nm_profissional_w	varchar2(255);
ds_espaco_20_w		varchar(255) 	:=  lpad(' ',1,' ');
ds_enter_w		varchar2(30)	:=  chr(13) || chr(10);	
ds_observacao_w		varchar2(1000);
dt_registro_w		varchar2(30);
ds_dnp_w		varchar2(32000);
vl_oe_ard_dnp_w		number(5,2);
vl_od_ard_dnp_w		number(5,2);
vl_ard_dp_w		number(5,2);

cursor C01 is
select obter_nome_pf(cd_profissional) nm_profissional,
	   to_char(dt_exame,'dd/mm/yyyy hh24:mi:ss'),
	   vl_od_ard_dnp,
	   vl_oe_ard_dnp,
	   vl_ard_dp,
	   ds_observacao
from   oft_dnp
WHERE  	((nr_seq_consulta = nr_seq_consulta_p) OR ((ie_mostra_todos_p = 'S') AND  nr_seq_consulta IN (SELECT c.nr_sequencia 
                                                                        	 	  	      FROM    atendimento_paciente b, 
                                                                              				      oft_consulta c 
                                                                       				      WHERE   c.nr_atendimento = b.nr_atendimento 
                                                                        			      AND    b.cd_pessoa_fisica = cd_pessoa_fisica_p)))
and    nvl(ie_situacao,'A') = 'A'	   
order  by dt_exame desc;

begin

open C01;
loop
	fetch C01 into
		nm_profissional_w,
		dt_registro_w,
		vl_od_ard_dnp_w,
		vl_oe_ard_dnp_w,
		vl_ard_dp_w,
		ds_observacao_w;
	exit when C01%notfound;
	begin
	ds_dnp_w	:=	'';
	ds_dnp_w	:= 	ds_dnp_w || 'DATA: ' || dt_registro_w || ds_espaco_20_w ||
					'PROFISSIONAL: ' ||nm_profissional_w || ds_enter_w || ds_enter_w;
	ds_dnp_w	:= ds_dnp_w ||lpad('OD (mm): ',15,' ')|| to_char(vl_od_ard_dnp_w, 'fm999990.00') || ds_enter_w;
	ds_dnp_w	:= ds_dnp_w ||lpad('OE (mm): ',15,' ')|| to_char(vl_oe_ard_dnp_w, 'fm999990.00') || ds_enter_w;
	ds_dnp_w	:= ds_dnp_w ||lpad('DP (mm): ',15,' ')|| to_char(vl_ard_dp_w, 'fm999990.00') || ds_enter_w;
	ds_dnp_w 	:= ds_dnp_w || ds_enter_w || obter_desc_expressao(330933)/*'OBSERVAÇÃO: '*/ || ds_observacao_w ||ds_enter_w;
	
	gravar_registro_resumo_oft(ds_dnp_w,nr_seq_item_p,nm_usuario_p,to_date(dt_registro_w,'dd/mm/yyyy hh24:mi:ss'));
	
	end;
end loop;
close C01;	
	
commit;

end gerar_resumo_dnp;
/