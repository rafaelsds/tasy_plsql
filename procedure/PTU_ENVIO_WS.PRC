create or replace
procedure ptu_envio_ws
			(	nr_seq_guia_p			number,
				nr_seq_requisicao_p		number,
				ds_mensagem_p			varchar2,
				nr_seq_ordem_p			number,
				ie_tipo_transacao_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2,
				nr_seq_retorno_p	out	number) is 

nr_seq_retorno_w		number(10);				

begin
if	(ie_tipo_transacao_p = 1) then
	ptu_env_pck.ptu_env_00600_v70(nr_seq_guia_p,nr_seq_requisicao_p,null,cd_estabelecimento_p,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 2) then
	ptu_env_pck.ptu_env_00605_v70(nr_seq_guia_p,nr_seq_requisicao_p,cd_estabelecimento_p,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 3) then
	ptu_env_pck.ptu_env_00302_v70(nr_seq_requisicao_p,nr_seq_guia_p,ds_mensagem_p,null,cd_estabelecimento_p,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 4) then
	ptu_env_pck.ptu_env_00311_v70(nr_seq_guia_p,nr_seq_requisicao_p,ds_mensagem_p,null,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 5) then
	ptu_env_pck.ptu_env_00404_v70(nr_seq_guia_p,nr_seq_requisicao_p,null,ds_mensagem_p,cd_estabelecimento_p,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 6) then
	ptu_env_pck.ptu_env_00360_v70(nr_seq_guia_p,nr_seq_requisicao_p,null,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 7) then
	ptu_env_pck.ptu_env_00700_v70(nr_seq_guia_p,nr_seq_requisicao_p,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 8) then
	ptu_env_pck.ptu_env_00804_v70(nr_seq_guia_p,nr_seq_requisicao_p,nm_usuario_p,nr_seq_retorno_w);
elsif	(ie_tipo_transacao_p = 9) then
	ptu_env_pck.ptu_env_00806_v70(nr_seq_ordem_p,cd_estabelecimento_p,nm_usuario_p);
	
	nr_seq_retorno_w	:= nr_seq_ordem_p;
end if;

nr_seq_retorno_p	:= nr_seq_retorno_w;

end ptu_envio_ws;
/