create or replace
procedure HTML_VINCULAR_TRANSF_CANCEL
			(nr_seq_saldo_caixa_p	in	number,
			nr_seq_caixa_destino_p	in	number,
			nr_seq_movto_p		in	varchar2,
			nr_seq_trans_p		in	varchar2,
			nm_usuario_p		in	varchar2)
			is
			
			
begin

GERAR_TRANSFERENCIA_CAIXA(nr_seq_saldo_caixa_p,
			nr_seq_caixa_destino_p,
			nr_seq_movto_p,
			nr_seq_trans_p,
			nm_usuario_p,
			sysdate,
			wheb_mensagem_pck.get_texto(179124),
			null);			

end HTML_VINCULAR_TRANSF_CANCEL;
/