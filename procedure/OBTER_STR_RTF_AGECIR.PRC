create or replace
procedure OBTER_STR_RTF_AGECIR(nr_seq_agenda_p	number,
								nm_usuario_p	varchar2,
								nr_seq_retorno_p out number) is 

begin

converte_rtf_string('Select ds_conteudo from W_AGECIR_ENVIO_EMAIL where nr_sequencia = :nr', nr_seq_agenda_p, nm_usuario_p, nr_seq_retorno_p);

commit;

end OBTER_STR_RTF_AGECIR;
/
