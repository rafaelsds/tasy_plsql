create or replace
procedure HAOC_GERAR_CONV_RET_SULAMERICA
			(nr_seq_retorno_p	in number) is

nr_interno_conta_w	number(10);
nm_paciente_w		varchar2(255);
cd_item_w		number(15);
vl_pago_w		number(15,2);
vl_cobrado_w		number(15,2);
vl_glosa_w		number(15,2);
vl_pago_char_w		varchar2(255);
vl_cobrado_char_w	varchar2(255);
vl_glosa_char_w		varchar2(255);
cd_motivo_glosa_w	convenio_retorno_movto.cd_motivo%type;

cursor	c01 is
select	substr(obter_valor_campo_separador(ds_conteudo,22,';'),1,6) nr_interno_conta,
	substr(obter_valor_campo_separador(ds_conteudo,12,';'),1,255) nm_paciente,
	somente_numero(obter_valor_campo_separador(ds_conteudo,14,';')) cd_item,
	obter_valor_campo_separador(ds_conteudo,16,';') vl_pago,
	obter_valor_campo_separador(ds_conteudo,17,';') vl_cobrado,
	obter_valor_campo_separador(ds_conteudo,18,';') vl_glosa,
	obter_valor_campo_separador(ds_conteudo,19,';') cd_motivo_glosa
from	w_conv_ret_movto
where 	nr_seq_retorno		= nr_seq_retorno_p
and	upper(obter_valor_campo_separador(ds_conteudo,1,';')) <> 'CNPJSAS'
and 	obter_valor_campo_separador(ds_conteudo,14,';') <> 0;

begin

open C01;
loop
fetch C01 into	
	nr_interno_conta_w,
	nm_paciente_w,
	cd_item_w,
	vl_pago_char_w,
	vl_cobrado_char_w,
	vl_glosa_char_w,
	cd_motivo_glosa_w;
exit when C01%notfound;
	begin
	
	begin	
		vl_pago_w	:= to_number(vl_pago_char_w);	
		vl_cobrado_w	:= to_number(vl_cobrado_char_w);
		vl_glosa_w	:= to_number(vl_glosa_char_w);
	exception
	when others then		
		vl_pago_w	:= 0;
		vl_cobrado_w	:= 0;
		vl_glosa_w	:= 0;
	end;
	
	insert	into convenio_retorno_movto
		(nr_seq_retorno,
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nr_conta,
		cd_item,
		ds_complemento,
		vl_pago,
		vl_total_pago,
		vl_cobrado,
		vl_glosa,
		cd_motivo)
	values	(nr_seq_retorno_p,
		convenio_retorno_movto_seq.nextval,
		'Tasy_Imp',
		sysdate,
		nr_interno_conta_w,
		cd_item_w,
		nm_paciente_w,
		vl_pago_w,
		vl_pago_w,
		vl_cobrado_w,
		vl_glosa_w,
		cd_motivo_glosa_w);
	end;

end loop;
close C01;

delete 	from w_conv_ret_movto
where	nr_seq_retorno = nr_seq_retorno_p;

commit;

end;
/