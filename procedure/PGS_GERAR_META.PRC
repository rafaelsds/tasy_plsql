create or replace
procedure	pgs_gerar_meta(	nr_seq_lote_p		number,
				dt_inicial_p		date,
				dt_final_p		date,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number)is
		
nr_seq_meta_w		pgs_meta.nr_sequencia%type;
qt_carga_horaria_w	pgs_meta.qt_carga_horaria%type;
qt_ponto_w		pgs_meta.qt_ponto%type;
ie_medico_regra_w	pgs_meta.ie_medico_regra%type;
dt_vigencia_w		pgs_meta.dt_vigencia%type;
	
nr_seq_medico_meta_w	pgs_medico_meta.nr_sequencia%type;
cd_pessoa_fisica_w	pgs_medico_meta.cd_pessoa_fisica%type;
dt_inicio_vigencia_w	pgs_medico_meta.dt_inicio_vigencia%type;
dt_fim_vigencia_w	pgs_medico_meta.dt_fim_vigencia%type;

dt_inicial_w		pgs_lote_meta.dt_inicial%type;
dt_final_w		pgs_lote_meta.dt_final%type;
nr_seq_lote_meta_w	pgs_lote_meta.nr_sequencia%type;

nr_seq_criterio_meta_w	pgs_criterio_meta.nr_sequencia%type;
pr_valor_criterio_w	pgs_criterio_meta.pr_valor_criterio%type;
ie_origem_valor_crit_w	pgs_criterio_meta.ie_origem_valor_crit%type;
qt_ponto_criterio_w	pgs_criterio_meta.qt_ponto_criterio%type;
cd_cbo_criterio_w	pgs_criterio_meta.cd_cbo_criterio%type;	

cd_convenio_w		convenio.cd_convenio%type;

cd_procedimento_w	procedimento.cd_procedimento%type;
ie_origem_proced_w	procedimento.ie_origem_proced%type;

vl_procedimento_w	procedimento_paciente.vl_procedimento%type;
vl_medico_w		procedimento_paciente.vl_medico%type;
vl_materiais_w		procedimento_paciente.vl_materiais%type;

vl_participante_w	procedimento_participante.vl_participante%type;

nr_seq_grupo_w		sus_grupo.nr_sequencia%type;
nr_seq_subgrupo_w	sus_subgrupo.nr_sequencia%type;
nr_seq_forma_org_w	sus_forma_organizacao.nr_sequencia%type;
ie_complexidade_w	sus_procedimento.ie_complexidade%type;

vl_repasse_medico_w	pgs_dado_lote.vl_repasse_medico%type;
qt_ponto_medico_w	pgs_dado_lote.qt_ponto_medico%type;
ie_situacao_meta_w	pgs_dado_lote.ie_situacao_meta%type;
ie_gerou_dados_w	varchar2(1) := 'N';
nr_seq_dado_lote_w	number(10);
		
Cursor C01 is
select	nr_sequencia,
	qt_carga_horaria,
	qt_ponto,
	nvl(ie_medico_regra,'E'),
	trunc(dt_vigencia)
from	pgs_meta
where	dt_vigencia between dt_inicial_w and dt_final_w
and	cd_estabelecimento = cd_estabelecimento_p
and	ie_situacao = 'A';

Cursor C02 is
select	nr_sequencia,
	cd_pessoa_fisica,
	dt_inicio_vigencia,
	dt_fim_vigencia
from	pgs_medico_meta
where	nr_seq_meta = nr_seq_meta_w
and	dt_vigencia_w between nvl(dt_inicio_vigencia,dt_vigencia_w) and nvl(dt_fim_vigencia,dt_vigencia_w)
and	ie_situacao = 'A';

type 		fetch_array is table of c02%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c02_w			vetor;

Cursor C03 is
select	nr_sequencia
from	pgs_criterio_meta
where	(nvl(nr_seq_sus_grupo, nr_seq_grupo_w)			= nr_seq_grupo_w)
and	(nvl(nr_seq_sus_subgrupo, nr_seq_subgrupo_w)		= nr_seq_subgrupo_w)
and	(nvl(nr_seq_sus_forma_org, nr_seq_forma_org_w)		= nr_seq_forma_org_w)
and	(nvl(cd_procedimento, cd_procedimento_w)		= cd_procedimento_w)
and	(nvl(ie_origem_proced, ie_origem_proced_w)		= ie_origem_proced_w)
and	(nvl(cd_cbo_criterio,cd_cbo_criterio_w)			= cd_cbo_criterio_w)
and	nr_seq_meta						= nr_seq_meta_w
order by nvl(cd_procedimento,0),
	nvl(nr_seq_sus_forma_org,0),
	nvl(nr_seq_sus_subgrupo,0),
	nvl(nr_seq_sus_grupo, 0),
	cd_cbo_criterio,
	ie_complexidade;

cursor w_pgs_dado_lote is
select	cd_procedimento,
	ie_origem_proced,
	vl_procedimento,
	vl_medico,
	vl_materiais,
	vl_participante,
	nr_seq_meta,
	nr_seq_lote_meta,
	cd_pessoa_fisica,
	cd_cbo,
	nr_sequencia
from	w_pgs_dado_lote
where	nr_seq_lote_meta = nr_seq_lote_p;

cursor w_pgs_dado_lote_medico is
select	cd_pessoa_fisica,
	nr_seq_criterio_meta,
	nr_seq_meta,
	sum(vl_repasse_medico),
	sum(qt_ponto_medico)
from	w_pgs_dado_lote_medico
where	nr_seq_lote_meta = nr_seq_lote_p
group by cd_pessoa_fisica,
	nr_seq_criterio_meta,
	nr_seq_meta;

begin

delete from w_pgs_dado_lote
where nr_seq_lote_meta = nr_seq_lote_p;

delete from w_pgs_dado_lote_medico
where nr_seq_lote_meta = nr_seq_lote_p;

delete from pgs_dado_lote
where nr_seq_lote_meta = nr_seq_lote_p;

dt_inicial_w	:= trunc(dt_inicial_p);
dt_final_w	:= fim_dia(dt_final_p);
cd_convenio_w	:= nvl(obter_dados_param_faturamento(cd_estabelecimento_p,'CSUS'),0);

open C01;
loop
fetch C01 into
	nr_seq_meta_w,
	qt_carga_horaria_w,
	qt_ponto_w,
	ie_medico_regra_w,
	dt_vigencia_w;
exit when C01%notfound;
	begin
		
	open c02;
	loop
	fetch c02 bulk collect into s_array limit 1000;
		vetor_c02_w(i) := s_array;
		i := i + 1;
	exit when c02%notfound;
	end loop;
	close c02;
		
	for i in 1..vetor_c02_w.count loop
		begin
		s_array := vetor_c02_w(i);
		for z in 1..s_array.count loop
			begin
			
			nr_seq_medico_meta_w	:= s_array(z).nr_sequencia;
			cd_pessoa_fisica_w	:= s_array(z).cd_pessoa_fisica;
			
								
			if	(ie_medico_regra_w = 'E') then
				begin
				insert	into w_pgs_dado_lote(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_meta,
								nr_seq_medico_meta,
								--nr_seq_criterio_meta,
								nr_seq_lote_meta,
								nr_atendimento,
								nr_interno_conta,
								cd_procedimento,
								ie_origem_proced,
								vl_procedimento,
								vl_medico,
								vl_materiais,
								vl_participante,
								qt_procedimento,
								dt_procedimento,
								ie_medico_regra,
								cd_cbo,
								cd_pessoa_fisica)
							select	w_pgs_dado_lote_seq.nextval,
								sysdate,
								nm_usuario_p,
								nr_seq_meta_w,
								nr_seq_medico_meta_w,
								nr_seq_lote_p,
								a.nr_atendimento,
								a.nr_interno_conta,
								a.cd_procedimento,
								a.ie_origem_proced,
								a.vl_procedimento,
								a.vl_medico,
								a.vl_materiais,
								nvl((select sum(x.vl_participante)
								from	procedimento_participante x
								where	x.nr_sequencia = a.nr_sequencia),0) vl_participante,
								a.qt_procedimento,
								a.dt_procedimento,
								ie_medico_regra_w,
								a.cd_cbo,
								cd_pessoa_fisica_w
							from	procedimento_paciente a,									
								conta_paciente  b,
								protocolo_convenio d
							where	a.nr_interno_conta 	= b.nr_interno_conta
							and	b.nr_seq_protocolo 	= d.nr_seq_protocolo 
							and	b.cd_convenio_parametro = cd_convenio_w
							and	b.cd_estabelecimento	= cd_estabelecimento_p
							and	d.ie_status_protocolo	= 2
							and	trunc(a.dt_procedimento,'month') = dt_vigencia_w
							and	a.cd_motivo_exc_conta 	is null
							and	(nvl(a.cd_pessoa_fisica,a.cd_medico_executor)	= cd_pessoa_fisica_w);					
				end;
			else
				begin
				insert	into w_pgs_dado_lote(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_meta,
								nr_seq_medico_meta,
								--nr_seq_criterio_meta,
								nr_seq_lote_meta,
								nr_atendimento,
								nr_interno_conta,
								cd_procedimento,
								ie_origem_proced,
								vl_procedimento,
								vl_medico,
								vl_materiais,
								vl_participante,
								qt_procedimento,
								dt_procedimento,
								ie_medico_regra,
								cd_cbo,
								cd_pessoa_fisica)
							select	w_pgs_dado_lote_seq.nextval,
								sysdate,
								nm_usuario_p,
								nr_seq_meta_w,
								nr_seq_medico_meta_w,
								nr_seq_lote_p,
								a.nr_atendimento,
								a.nr_interno_conta,
								a.cd_procedimento,
								a.ie_origem_proced,
								a.vl_procedimento,
								a.vl_medico,
								a.vl_materiais,
								nvl((select sum(x.vl_participante)
								from	procedimento_participante x
								where	x.nr_sequencia = a.nr_sequencia),0) vl_participante,
								a.qt_procedimento,
								a.dt_procedimento,
								ie_medico_regra_w,
								a.cd_cbo,
								cd_pessoa_fisica_w
							from	procedimento_paciente a,									
								conta_paciente  b,
								atendimento_paciente c,
								protocolo_convenio d
							where	a.nr_interno_conta 	= b.nr_interno_conta
							and	b.nr_atendimento	= c.nr_atendimento
							and	b.nr_seq_protocolo 	= d.nr_seq_protocolo 
							and	b.cd_convenio_parametro = cd_convenio_w
							and	b.cd_estabelecimento	= cd_estabelecimento_p
							and	d.ie_status_protocolo	= 2
							and	trunc(a.dt_procedimento,'month') = dt_vigencia_w
							and	a.cd_motivo_exc_conta 	is null
							and	c.cd_medico_resp	= cd_pessoa_fisica_w;
				end;
			end if;
			
			end;
		end loop;
		end;
	end loop;
	
	end;
end loop;
close C01;


open w_pgs_dado_lote;
loop
fetch w_pgs_dado_lote into
	cd_procedimento_w,
	ie_origem_proced_w,
	vl_procedimento_w,	
	vl_medico_w,
	vl_materiais_w,
	vl_participante_w,
	nr_seq_meta_w,
	nr_seq_lote_meta_w,
	cd_pessoa_fisica_w,
	cd_cbo_criterio_w,
	nr_seq_dado_lote_w;
exit when w_pgs_dado_lote%notfound;
	begin
		
	begin
	select	nr_seq_grupo,
		nr_seq_subgrupo,
		nr_seq_forma_org,
		ie_complexidade
	into	nr_seq_grupo_w,
		nr_seq_subgrupo_w,
		nr_seq_forma_org_w,
		ie_complexidade_w
	from	sus_estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w;	
	exception
	when others then
		nr_seq_grupo_w		:= 0;
		nr_seq_subgrupo_w	:= 0;
		nr_seq_forma_org_w	:= 0;
		ie_complexidade_w	:= '';
	end;

	open	C03; --criterio 
	loop	
	fetch	C03 into
		nr_seq_criterio_meta_w;
	exit when C03%notfound;	
	end loop;
	close C03;

	if	(nvl(nr_seq_criterio_meta_w,0) > 0) then
		begin
		
		select 	ie_origem_valor_crit,
			pr_valor_criterio,
			qt_ponto_criterio
		into	ie_origem_valor_crit_w,
			pr_valor_criterio_w,
			qt_ponto_criterio_w
		from	pgs_criterio_meta
		where	nr_sequencia = nr_seq_criterio_meta_w;
		
		if	(ie_origem_valor_crit_w = 'P') then --procedimento
			vl_repasse_medico_w	:= vl_procedimento_w;
		elsif	(ie_origem_valor_crit_w = 'D') then --m�dico
			vl_repasse_medico_w	:= vl_medico_w;
		elsif	(ie_origem_valor_crit_w = 'M') then --mat/med
			vl_repasse_medico_w	:= vl_materiais_w;
		elsif	(ie_origem_valor_crit_w = 'T') then --participante
			vl_repasse_medico_w	:= vl_participante_w;
		end if;
		
		--calcular o percentual sobre o valor
		vl_repasse_medico_w := (vl_repasse_medico_w * nvl(pr_valor_criterio_w,100)) / 100;

		insert into w_pgs_dado_lote_medico(cd_pessoa_fisica,
						cd_procedimento,
						dt_atualizacao,
						ie_origem_proced,
						nm_usuario,
						nr_seq_dado_lote,
						nr_seq_meta,
						nr_seq_lote_meta,
						nr_seq_criterio_meta,
						nr_sequencia,
						vl_repasse_medico,
						qt_ponto_medico)
					values(	cd_pessoa_fisica_w,
						cd_procedimento_w,
						sysdate,
						ie_origem_proced_w,
						nm_usuario_p,
						nr_seq_dado_lote_w,
						nr_seq_meta_w,
						nr_seq_lote_meta_w,
						nr_seq_criterio_meta_w,
						w_pgs_dado_lote_medico_seq.nextval,
						vl_repasse_medico_w,
						qt_ponto_criterio_w);
						
		end;
	end if;	
	
	end;
end loop;
close w_pgs_dado_lote;	
	
open w_pgs_dado_lote_medico;
loop
fetch w_pgs_dado_lote_medico into
	cd_pessoa_fisica_w,
	nr_seq_criterio_meta_w,
	nr_seq_meta_w,
	vl_repasse_medico_w,
	qt_ponto_medico_w;
exit when w_pgs_dado_lote_medico%notfound;
	begin
				
	begin
	select	qt_ponto
	into	qt_ponto_w
	from	pgs_meta
	where	nr_sequencia = nr_seq_meta_w;	
	exception
	when others then
		qt_ponto_w := 0;
	end;
	
	ie_situacao_meta_w := 'N';
	if	(qt_ponto_medico_w >= qt_ponto_w) then
		ie_situacao_meta_w := 'A';
	end if;
				
	insert into pgs_dado_lote(	nr_sequencia,
					cd_pessoa_fisica,
					dt_atualizacao,
					nm_usuario,
					nr_seq_crit_meta,
					nr_seq_lote_meta,
					nr_seq_meta,					
					qt_ponto_medico,
					vl_repasse_medico,
					ie_situacao_meta)
			values	(	pgs_dado_lote_seq.nextval,
					cd_pessoa_fisica_w,
					sysdate,
					nm_usuario_p,
					nr_seq_criterio_meta_w,
					nr_seq_lote_p,
					nr_seq_meta_w,					
					qt_ponto_medico_w,
					vl_repasse_medico_w,
					ie_situacao_meta_w);
	ie_gerou_dados_w := 'S';
	end;
end loop;
close w_pgs_dado_lote_medico;

if	(ie_gerou_dados_w = 'S') then
	begin
	
	update	pgs_lote_meta
	set 	dt_geracao = sysdate,
		nm_usuario_geracao = nm_usuario_p,
	 	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_lote_p;
	
	end;
end if;

commit;

end pgs_gerar_meta;
/