create or replace
procedure ctb_liberar_cenario(	nr_seq_cenario_p	number,
					nm_usuario_p		varchar2) is

BEGIN

update	ctb_orc_cenario
set	dt_liberacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_cenario_p;

END ctb_liberar_cenario;
/