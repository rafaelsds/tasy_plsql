create or replace procedure confirm_enf_conf_item(
    nr_sequencia_p sumario_enf_conf_list.nr_sequencia%type,
	nm_aprovador_p sumario_enf_conf_list.nm_usuario%type,
    ie_nurse_sum_p varchar2 default null) is

nr_seq_conf_list_w sumario_enf_conf_list.nr_sequencia%type;
nr_seq_sum_enf_w sumario_enf_conf_list.nr_seq_sum_enf%type;

begin
  if ie_nurse_sum_p is not null then
    select nr_sequencia
    into nr_seq_conf_list_w
    from sumario_enf_conf_list
    where nr_seq_sum_enf = nr_sequencia_p;
  else
    nr_seq_conf_list_w := nr_sequencia_p;
  end if;

	update sumario_enf_conf_list
	set dt_aprovacao = sysdate,
		nm_aprovador = nm_aprovador_p
	where nr_sequencia = nr_seq_conf_list_w;

    update SUMARIO_ENFERMAGEM
	set DT_APPROVED = sysdate
	where nr_sequencia = nr_sequencia_p;

    select s.nr_seq_sum_enf
    into nr_seq_sum_enf_w
    from sumario_enf_conf_list s
    where s.nr_sequencia = nr_seq_conf_list_w;

    update SUMARIO_ENFERMAGEM
	set DT_APPROVED = sysdate
	where nr_sequencia = nr_seq_sum_enf_w;

  commit;
end confirm_enf_conf_item;
/