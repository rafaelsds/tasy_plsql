create or replace
procedure	cm_gerar_conj_retorno_atend(
			nr_seq_conjunto_p		number,
			nm_usuario_p		varchar2) is


nr_seq_conjunto_w		number(10);
nr_seq_embalagem_w		number(10);
cd_local_estoque_w		number(10);
cd_setor_atend_orig_w		number(5);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
nr_seq_cliente_w			number(10);
cd_setor_atendimento_w		number(5);
cd_pessoa_resp_w			varchar2(10);
ie_tipo_esterilizacao_w		varchar2(2);
vl_esterilizacao_w			number(15,2);
qt_ponto_w			number(15,4);
nr_seq_controle_w			number(10);
nr_sequencia_w			number(10);
cd_estabelecimento_w		number(4);
dt_validade_w			date;
cd_local_estoque_param_w	number(10);

begin

select	cd_estabelecimento,
	nr_seq_conjunto,
	nr_seq_embalagem,
	cd_local_estoque,
	cd_setor_atend_orig,
	cd_pessoa_fisica,
	cd_cgc,
	nr_seq_cliente,
	cd_setor_atendimento,
	cd_pessoa_resp,
	ie_tipo_esterilizacao,
	vl_esterilizacao,
	qt_ponto,
	dt_validade
into	cd_estabelecimento_w,
	nr_seq_conjunto_w,
	nr_seq_embalagem_w,
	cd_local_estoque_w,
	cd_setor_atend_orig_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	nr_seq_cliente_w,
	cd_setor_atendimento_w,
	cd_pessoa_resp_w,
	ie_tipo_esterilizacao_w,
	vl_esterilizacao_w,
	qt_ponto_w,
	dt_validade_w
from	cm_conjunto_cont
where	nr_sequencia = nr_seq_conjunto_p
and	nvl(ie_situacao,'A') = 'A';
	
obter_param_usuario(410,62,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,cd_estabelecimento_w, cd_local_estoque_param_w);
	
select 	nvl(max(nr_seq_controle),0) + 1
into	nr_seq_controle_w
from 	cm_conjunto_cont;

select	CM_CONJUNTO_CONT_seq.nextval
into	nr_sequencia_w
from	dual;

insert into cm_conjunto_cont(
	nr_sequencia,
	nr_seq_controle,
	nr_seq_conjunto,
	nr_seq_embalagem,
	cd_local_estoque,
	cd_setor_atend_orig,
	cd_pessoa_fisica,
	cd_cgc,
	nr_seq_cliente,
	cd_setor_atendimento,
	cd_pessoa_resp,
	ie_tipo_esterilizacao,
	vl_esterilizacao,
	qt_ponto,
	dt_origem,
	nm_usuario_origem,
	dt_receb_ester,
	nm_usuario_ester,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	ie_status_conjunto,
	ie_situacao,
	dt_validade) 
values	(nr_sequencia_w,
	nr_seq_controle_w,
	nr_seq_conjunto_w,
	nr_seq_embalagem_w,
	cd_local_estoque_w,
	cd_setor_atend_orig_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	nr_seq_cliente_w,
	cd_setor_atendimento_w,
	cd_pessoa_resp_w,
	ie_tipo_esterilizacao_w,
	vl_esterilizacao_w,
	qt_ponto_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_w,
	sysdate,
	nm_usuario_p,
	'1',
	'A',
	dt_validade_w);
	
update	cm_conjunto_cont
set		cd_local_estoque 	= nvl(cd_local_estoque_param_w, cd_local_estoque)
where	nr_sequencia		= nr_seq_conjunto_p;
	
cme_incluir_itens_controle(nr_sequencia_w,nm_usuario_p);

end cm_gerar_conj_retorno_atend;
/