create or replace
procedure san_excluir_prod_doacao( nr_seq_doacao_p		number) is 

begin
if	(nr_seq_doacao_p is not null) then
    delete 
    from san_producao_log_alteracao
    where nr_sequencia in(
      select a.nr_sequencia
      from san_producao_log_alteracao a,
           san_producao b
      where a.nr_seq_producao = b.nr_sequencia
      and     b.nr_seq_doacao = nr_seq_doacao_p);


	delete
	from	san_producao
	where	nr_seq_doacao = nr_seq_doacao_p;
end if;

commit;

end san_excluir_prod_doacao;
/
