create or replace
procedure desfazer_envio_dirf(	nr_seq_lote_p	number) as



begin

update	dirf_lote
set	dt_envio		= null
where	nr_sequencia	= nr_seq_lote_p;

commit;

end desfazer_envio_dirf;
/