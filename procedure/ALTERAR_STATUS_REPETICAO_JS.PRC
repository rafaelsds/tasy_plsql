create or replace
procedure alterar_status_repeticao_js(	nr_prescricao_p		number,	
					nr_seq_prescr_p		number,
					ie_confirmacao_p	varchar2,
					nm_usuario_p		varchar2) is 

ie_status_repet_w	lab_parametro.ie_status_repeticao%type;

begin

select 	nvl(max(a.ie_status_repeticao),0)
into	ie_status_repet_w
from	lab_parametro a, prescr_medica b
where   b.nr_prescricao = nr_prescricao_p
and 	a.cd_estabelecimento = b.cd_estabelecimento;


if	(ie_confirmacao_p = 'N') then

	update 	prescr_procedimento
	set    	ie_exame_bloqueado = 'R',
		nm_usuario = nm_usuario_p,
		ie_status_atend	= decode(ie_status_repet_w,0,ie_status_atend,ie_status_repet_w)
	where  	nr_sequencia = nr_seq_prescr_p
	and    	nr_prescricao = nr_prescricao_p
	and    ((ie_status_repet_w = 0) or (ie_status_atend < 35));
	commit;

	Insere_usuario_repeticao(nr_prescricao_p, nr_seq_prescr_p, nm_usuario_p);
	
	if(ie_status_repet_w > 0) then
		Lab_atualiza_exame_lab_rep(nr_prescricao_p, nr_seq_prescr_p, nm_usuario_p);
	end if;

else 
   
	update 	prescr_procedimento
	set    	ie_exame_bloqueado = 'N',
		nm_usuario = nm_usuario_p
	where  	nr_sequencia = nr_seq_prescr_p
	and    	nr_prescricao = nr_prescricao_p;
	commit;
end if;


end alterar_status_repeticao_js;
/
