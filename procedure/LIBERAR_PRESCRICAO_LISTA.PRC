create or replace
procedure liberar_prescricao_lista(
									ds_lista_prescricao_p	varchar2,
									ie_tipo_pessoa_p		varchar2,
									cd_perfil_p				number,
									nm_usuario_p			varchar2,
									ie_prescricao_p			varchar2, 
									cd_tipo_agenda_p		number,
									nr_seq_agenda_p			number,									
									ds_erro_p			out	varchar2
								   ) is 

							   
								   
lista_informacao_w		varchar2(800);
tam_lista_w				number(10,0);
ie_pos_virgula_w		number(3,0);
ds_erro_w				varchar2(800);
		
nr_prescricao_w			number(14,0);
nr_atendimento_w		number(10,0);
		
ds_tipo_agenda_w		varchar2(5);
cd_estabelecimento_w	number(4);

dt_prescricao_w			date;

nr_seq_agenda_w			number(10);
cd_tipo_agenda_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
		
			   
cursor c01 is
	select nr_sequencia,obter_tipo_agenda(cd_agenda)
	from	agenda_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and		trunc(dt_agenda) = trunc(dt_prescricao_w)
	and 	nr_atendimento = nr_atendimento_w
	and 	nr_sequencia <> nr_seq_agenda_p
	union
	select nr_seq_agenda_p, cd_tipo_agenda_p from dual
	order 	by	1;
	
		
begin

ds_erro_p := '';
lista_informacao_w	:= ds_lista_prescricao_p;

while	(lista_informacao_w is not null)  loop
	begin
	tam_lista_w		:= length(lista_informacao_w);
	ie_pos_virgula_w	:= instr(lista_informacao_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_prescricao_w	:= to_number(substr(lista_informacao_w,1,(ie_pos_virgula_w - 1)));
		lista_informacao_w	:= substr(lista_informacao_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	select 	nr_atendimento,
			dt_prescricao,
			cd_pessoa_fisica
	into	nr_atendimento_w,
			dt_prescricao_w,
			cd_pessoa_fisica_w
	from 	prescr_medica
	where 	nr_prescricao = nr_prescricao_w;
		
	Liberar_Prescricao(	nr_prescricao_w,
						nr_atendimento_w,
						ie_tipo_pessoa_p,
						cd_perfil_p,
						nm_usuario_p,
						ie_prescricao_p,
						ds_erro_w);
						
	if (ds_erro_w is not null) then
		ds_erro_p	:= ds_erro_p ||obter_desc_expressao(671332)/*'     prescr: '*/||nr_prescricao_w||' = '||ds_erro_w;						
	end if;	
	
	
	select 	nvl(max(cd_estabelecimento),0) 	
	into	cd_estabelecimento_w
	from   	prescr_medica                                                                       
	where  	nr_prescricao = nr_prescricao_w;
	
	
	open c01;
	loop
	fetch c01 into	
		nr_seq_agenda_w,
		cd_tipo_agenda_w;
	exit when c01%notfound;
		begin
		
		if  (cd_tipo_agenda_w = 1)  then 
			ds_tipo_agenda_w := 'CI';
		elsif	(cd_tipo_agenda_w = 2)  then 
			ds_tipo_agenda_w := 'E';
		elsif	(cd_tipo_agenda_w = 3)  then 
			ds_tipo_agenda_w := 'C';
		elsif	(cd_tipo_agenda_w = 5)  then 
			ds_tipo_agenda_w := 'S';		
		end if;		
		
			
		if (obter_se_existe_evento_agenda(cd_estabelecimento_w, 'LP' ,  ds_tipo_agenda_w) = 'S') then
					
			executar_evento_agenda('LP', ds_tipo_agenda_w, nr_seq_agenda_w, cd_estabelecimento_w, nm_usuario_p);
		end if;	
		
		end;
	end loop;
	close c01;
	
	
	end;
end loop;

commit;

end liberar_prescricao_lista;
/