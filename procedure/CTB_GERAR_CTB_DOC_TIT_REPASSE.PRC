create or replace procedure ctb_gerar_ctb_doc_tit_repasse (nr_titulo_repasse_p number,
                                         nm_usuario_p varchar2) is

titulo_repasse_w        titulo_pagar%rowtype;
vl_movimento_w          ctb_documento.vl_movimento%type;
dt_documento_w          ctb_documento.dt_atualizacao%type;
nr_seq_trans_financ_w   ctb_documento.nr_seq_trans_financ%type;
ctb_param_lote_tit_repasse_w ctb_param_lote_tit_repasse%rowtype;
nr_seq_tab_compl_w      ctb_documento.nr_seq_doc_compl%type;
nr_tit_pag_imp_w        titulo_pagar_imposto.nr_titulo%type;
ie_ctb_online_w         varchar2(1);

cursor  c01 is
select  a.nm_atributo
from    atributo_contab a
where   a.cd_tipo_lote_contab = 16
and     a.nm_atributo in ('VL_TITULO_REPASSE', 'VL_TRIBUTO_REPASSE', 'VL_LIBERADO');

c01_w   c01%rowtype;

begin

begin
select  *
into    titulo_repasse_w
from    titulo_pagar
where   nr_titulo = nr_titulo_repasse_p;
exception when others then
        titulo_repasse_w := null;
end;

if      (nvl(titulo_repasse_w.nr_titulo, 0) <> 0) then
        ie_ctb_online_w   := ctb_online_pck.get_modo_lote(16, titulo_repasse_w.cd_estabelecimento);
end if;

if      ((nvl(ie_ctb_online_w, 'N') = 'S') and nvl(titulo_repasse_w.nr_repasse_terceiro, 0) <> 0) then
        begin
        select  a.*
        into    ctb_param_lote_tit_repasse_w
        from    ctb_param_lote_tit_repasse a
        where   a.cd_empresa    = obter_empresa_estab(titulo_repasse_w.cd_estabelecimento)
        and     nvl(a.cd_estab_exclusivo, titulo_repasse_w.cd_estabelecimento)  = titulo_repasse_w.cd_estabelecimento;
        exception when others then
                ctb_param_lote_tit_repasse_w := null;
        end;

        nr_seq_trans_financ_w := nvl(titulo_repasse_w.nr_seq_trans_fin_contab, ctb_param_lote_tit_repasse_w.nr_seq_trans_repasse);

        open c01;
        loop
        fetch c01 into
                c01_w;
        exit when c01%notfound;
                begin

                dt_documento_w          := null;
                nr_seq_tab_compl_w      := null;
                vl_movimento_w          := 0;

                if      (c01_w.nm_atributo = 'VL_TITULO_REPASSE') and
                        (nvl(titulo_repasse_w.nr_seq_tributo,0) = 0) then
                        begin

                        if      (titulo_repasse_w.ie_situacao = 'D') then
                                vl_movimento_w := obter_valor_tit_pagar_desdob(titulo_repasse_w.nr_titulo);
                        else
                                vl_movimento_w := titulo_repasse_w.vl_titulo;
                        end if;

                        select
                                case ctb_param_lote_tit_repasse_w.ie_data_contab_tit_rep
                                when 'V'
                                    then titulo_repasse_w.dt_vencimento_atual
                                when 'C'
                                    then titulo_repasse_w.dt_contabil
                                when 'O'
                                    then titulo_repasse_w.dt_vencimento_original
                                else
                                    titulo_repasse_w.dt_emissao
                                end
                        into dt_documento_w
                        from dual;
                        end;
                elsif (c01_w.nm_atributo = 'VL_TRIBUTO_REPASSE' and nvl(titulo_repasse_w.nr_seq_tributo, 0) <> 0) then
                        begin

                        if      (titulo_repasse_w.ie_situacao = 'D') then
                                vl_movimento_w := obter_valor_tit_pagar_desdob(titulo_repasse_w.nr_titulo);
                        else
                                vl_movimento_w := titulo_repasse_w.vl_titulo;
                        end if;

                        nr_seq_tab_compl_w := titulo_repasse_w.nr_seq_tributo;

                        if      (nvl(ctb_param_lote_tit_repasse_w.ie_dt_tit_trib_orig, 'N') = 'N') then
                                begin
                                select
                                case ctb_param_lote_tit_repasse_w.ie_data_contab_tit_rep
                                    when 'V'
                                    then titulo_repasse_w.dt_vencimento_atual
                                    when 'C'
                                    then titulo_repasse_w.dt_contabil
                                    when 'O'
                                    then titulo_repasse_w.dt_vencimento_original
                                    else
                                    titulo_repasse_w.dt_emissao
                                end
                                into dt_documento_w
                                from dual;
                                end;
                        else
                                begin
                                begin
                                select  nr_titulo
                                into    nr_tit_pag_imp_w
                                from    titulo_pagar_imposto
                                where   nr_sequencia = titulo_repasse_w.nr_seq_tributo;
                                exception when others then
                                    nr_tit_pag_imp_w := 0;
                                end;

                                if      (nvl(nr_tit_pag_imp_w, 0) <> 0 ) then
                                        select
                                                case ctb_param_lote_tit_repasse_w.ie_dt_movto_contab_tit_rep
                                                when 'V'
                                                then to_date(obter_dados_tit_pagar(nr_tit_pag_imp_w,'VE'), 'dd/mm/yyyy')
                                                when 'C'
                                                then titulo_repasse_w.dt_contabil
                                                when 'O'
                                                then to_date(obter_dados_tit_pagar(nr_tit_pag_imp_w,'VO'), 'dd/mm/yyyy')
                                                else
                                                to_date(obter_dados_tit_pagar(nr_tit_pag_imp_w,'EM'), 'dd/mm/yyyy')
                                                end
                                        into dt_documento_w
                                        from dual;
                                end if;
                                end;
                        end if;
                        end;
                elsif   (c01_w.nm_atributo = 'VL_LIBERADO') and
                        (nvl(titulo_repasse_w.nr_seq_tributo, 0) = 0) and
                        (obter_valor_repasse(titulo_repasse_w.nr_repasse_terceiro,'L') <> 0) then
                        begin
                        vl_movimento_w := nvl(obter_valor_repasse_titulo(titulo_repasse_w.nr_titulo,'L'),0);

                        select  case ctb_param_lote_tit_repasse_w.ie_dt_movto_contab_tit_rep
                                when 'V'
                                    then titulo_repasse_w.dt_vencimento_atual
                                when 'C'
                                    then titulo_repasse_w.dt_contabil
                                when 'O'
                                    then titulo_repasse_w.dt_vencimento_original
                                else
                                    titulo_repasse_w.dt_emissao
                                end
                        into    dt_documento_w
                        from    dual;
                        end;
                end if;

                if      (nvl(vl_movimento_w, 0) <> 0) then
                        begin
                        ctb_concil_financeira_pck.ctb_gravar_documento( titulo_repasse_w.cd_estabelecimento,
                                                                        dt_documento_w,
                                                                        16,
                                                                        nr_seq_trans_financ_w,
                                                                        2,
                                                                        nr_titulo_repasse_p,
                                                                        nr_seq_tab_compl_w,
                                                                        null,
                                                                        vl_movimento_w,
                                                                        'TITULO_PAGAR',
                                                                        c01_w.nm_atributo,
                                                                        nm_usuario_p);
                        end;
                end if;
            end;
    end loop;
    close c01;
end if;

end ctb_gerar_ctb_doc_tit_repasse;
/