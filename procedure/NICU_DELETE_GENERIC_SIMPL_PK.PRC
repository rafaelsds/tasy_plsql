create or replace procedure nicu_delete_generic_simpl_pk(nm_table_p   in varchar2,
                                                         nr_pk_p      in number,
                                                         retorno_o    out varchar2,
                                                         nm_pk_p      in varchar2 default null) as 
  
  exist_table    number;
  erro_exception exception;                                           
begin
  
  begin
    select 1
      into exist_table
      from all_Tables 
     where upper(table_name) = upper(nm_table_p);
  exception
    when others then
      null;
  end;
  
  if (exist_table is null) then
      --Tabela #@NM_TABELA_ORIGEM#@ nao existe no sistema!
      retorno_o := wheb_mensagem_pck.get_texto(1181967, 'NM_TABELA_ORIGEM='||nm_table_p);
      raise erro_exception;        
  end if;
  
  if (nm_table_p is null) then
      --Tabela inexistente
      retorno_o := wheb_mensagem_pck.get_texto(1181965);
      raise erro_exception;    
  end if;
  
  if (nr_pk_p is null) then
      --Deve-se informar o campo #@CD_ITEM#@
      retorno_o := wheb_mensagem_pck.get_texto(1181970, 'CD_ITEM=NR_PK_P');
      raise erro_exception;    
  end if;
  
  begin
    execute immediate
      'DELETE '||nm_table_p||'
        WHERE '||nvl(nm_pk_p, 'NR_SEQUENCIA')||' = '||nr_pk_p;        
  exception
    when others then    
      --Ocorreu um erro ao deletar da tabela
      retorno_o := wheb_mensagem_pck.get_texto(1181973)||' Error: '||sqlerrm;
      raise erro_exception;
  end;
  
  commit;
exception
  when erro_exception then
    rollback;
  when others then
    rollback;
    --Ocorreu um erro ao deletar da tabela
    retorno_o := wheb_mensagem_pck.get_texto(1181973)||' Error: '||sqlerrm;
end nicu_delete_generic_simpl_pk;
/
