create or replace
procedure diops_cadastral_gerar_telfax
			(	nr_seq_operadora_p	number,
				nr_seq_periodo_p	number,
				nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar as informa��es de FAX
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	PLS_GERAR_DIOPS_CADASTRAL
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_cgc_w			varchar2(14);
cd_representante_w		varchar2(10);
cd_repres_rn117_w		varchar2(10);
cd_estabelecimento_w		number(4);

begin
/* Obter dados da operadora */
begin
select	cd_cgc_outorgante,
	cd_estabelecimento,
	cd_representante,
	cd_representante_rn117
into	cd_cgc_w,
	cd_estabelecimento_w,
	cd_representante_w,
	cd_repres_rn117_w
from	pls_outorgante
where	nr_sequencia	= nr_seq_operadora_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(243450, 'NR_SEQ_OPERADORA=' || nr_seq_operadora_p);
end;	

/* Gravar o n�mero do telefone da Operadora */
diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, '', cd_cgc_w, 'TO', cd_estabelecimento_w, nm_usuario_p);

/* Gravar o n�mero do fax da Operadora */
diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, '', cd_cgc_w, 'FO', cd_estabelecimento_w, nm_usuario_p);

/* Gravar o n�mero do telefone da Representante */
diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, cd_representante_w, '', 'TR', cd_estabelecimento_w, nm_usuario_p);

/* Gravar o n�mero do fax do Representante */
diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, cd_representante_w, '', 'FR', cd_estabelecimento_w, nm_usuario_p);

/* Gravar o n�mero do telefone do Representante RN117*/
diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, cd_repres_rn117_w, '', 'TRN', cd_estabelecimento_w, nm_usuario_p);

/* Gravar o n�mero do fax do Representante RN117 */
diops_cadastral_gravar_telfax(nr_seq_operadora_p, nr_seq_periodo_p, cd_repres_rn117_w, '', 'FRN', cd_estabelecimento_w, nm_usuario_p);

end diops_cadastral_gerar_telfax;
/