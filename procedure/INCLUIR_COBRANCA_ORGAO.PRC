create or replace
procedure incluir_cobranca_orgao(	nr_seq_regra_p		number,
					nr_seq_cobranca_p	number,
					nr_seq_orgao_p		number,
					dt_inclusao_p		date,
					ds_observacao_p		varchar2,
					nm_usuario_p		varchar2, 
					nr_seq_perda_p          number default null) is 

dt_inclusao_w			cobranca.dt_inclusao%type;
qt_dias_atraso_w		regra_lib_orgao_cobranca.qt_dias_atraso%type;   
qt_dias_vencimento_w		regra_lib_orgao_cobranca.qt_dias_vencimento%type;
ie_uso_plano_w			regra_lib_orgao_cobranca.ie_uso_plano%type;
ie_contrato_cancelado_w		regra_lib_orgao_cobranca.ie_contrato_cancelado%type;
ie_titulo_pls_w			regra_lib_orgao_cobranca.ie_titulo_pls%type;
ie_tipo_documento_w		regra_lib_orgao_cobranca.ie_tipo_documento%type;
dt_inicio_vigencia_w		regra_lib_orgao_cobranca.dt_inicio_vigencia%type;
dt_final_vigencia_w		regra_lib_orgao_cobranca.dt_final_vigencia%type;
nr_titulo_cobr_w		cobranca.nr_titulo%type;
cheque_cr_cobr_w		cobranca.nr_seq_cheque%type;
ds_mensagem_w			varchar2(255);
dt_pagamento_previsto_w		titulo_receber.dt_pagamento_previsto%type;
dt_vencimento_w			cheque_cr.dt_vencimento%type;
dt_cancelamento_w		pls_contrato.dt_cancelamento%type;
ie_origem_titulo_w		titulo_receber.ie_origem_titulo%type;
cd_pessoa_fisica_w		titulo_receber.cd_pessoa_fisica%type;
count_w				number(10);
ie_liberar_envio_orgao_w	orgao_cobranca.ie_liberar_envio_orgao%type;
cd_pessoa_fisica_titulo_w	titulo_receber.cd_pessoa_fisica%type;
cd_cgc_titulo_w			titulo_receber.cd_cgc%type;
cd_pessoa_fisica_cheque_w	cheque_cr.cd_pessoa_fisica%type;
cd_cgc_cheque_w			cheque_cr_orgao_cobr.cd_cgc%type;
cd_pessoa_fisica_contrato_w	titulo_receber.cd_pessoa_fisica%type;
vl_titulo_w			titulo_receber.vl_titulo%type;
vl_cheque_w			cheque_cr.vl_cheque%type;
nr_cheque_w			cheque_cr.nr_cheque%type;
qt_registro_exist_w		number(10);
vl_saldo_titulo_w			titulo_receber.vl_saldo_titulo%type;

begin
ds_mensagem_w := '';

select	max(a.qt_dias_atraso),                 
	max(a.qt_dias_vencimento),             
	max(a.ie_uso_plano),                   
	max(a.ie_contrato_cancelado),        
	max(a.ie_titulo_pls),                  
	max(a.ie_tipo_documento),             
	max(a.dt_inicio_vigencia),             
	max(a.dt_final_vigencia)		
into 	qt_dias_atraso_w,		
 	qt_dias_vencimento_w,		
 	ie_uso_plano_w,		
 	ie_contrato_cancelado_w,	
 	ie_titulo_pls_w,		
 	ie_tipo_documento_w,
 	dt_inicio_vigencia_w,	
 	dt_final_vigencia_w
from	regra_lib_orgao_cobranca a
where	a.nr_sequencia = nr_seq_regra_p;

if (nr_seq_cobranca_p is not null) then
	select	trunc(max(a.dt_inclusao),'dd') + qt_dias_atraso_w,
		max(a.nr_titulo),
		max(a.nr_seq_cheque)
	into	dt_inclusao_w,
		nr_titulo_cobr_w,
		cheque_cr_cobr_w
	from	cobranca a
	where	a.nr_sequencia = nr_seq_cobranca_p;
end if;


if (nr_seq_perda_p is not null) then

	select	max(b.nr_titulo),
		max(b.nr_seq_cheque)

	into   	nr_titulo_cobr_w,
		cheque_cr_cobr_w      
	from  	perda_contas_receber b

	where   nr_sequencia = nr_seq_perda_p;
end if;

if (nr_titulo_cobr_w is not null) then
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_contrato_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_cobr_w;
elsif (cheque_cr_cobr_w is not null) then
	select	max(b.cd_pessoa_fisica)
	into	cd_pessoa_fisica_contrato_w
	from	cheque_cr b
	where	b.nr_seq_cheque = cheque_cr_cobr_w;
end if;

if	(ie_tipo_documento_w = 'C') and 
	(nr_titulo_cobr_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(334769);
elsif	(ie_tipo_documento_w = 'T') and 
	(cheque_cr_cobr_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(334768);
end if;


if	(nr_seq_cobranca_p is not null)	then

	if	(trunc(dt_inclusao_w,'dd') > trunc(sysdate,'dd')) then
	
		ds_mensagem_w := wheb_mensagem_pck.get_texto(334719, 'NR_SEQUENCIA=' || nr_seq_cobranca_p || ';QT_DIAS_ATRASO=' || qt_dias_atraso_w);
		
		wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);
		
	end if;
	
end if;	






if	(nr_titulo_cobr_w is not null) then
	
	select	trunc(max(a.dt_pagamento_previsto),'dd') + qt_dias_vencimento_w
	into	dt_pagamento_previsto_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_cobr_w;
	
	if	(trunc(dt_pagamento_previsto_w,'dd') > trunc(sysdate,'dd')) then
	
		if (nr_seq_cobranca_p is not null) then
			ds_mensagem_w := wheb_mensagem_pck.get_texto(335056, 'NR_SEQUENCIA=' || nr_seq_cobranca_p || ';QT_DIAS_VENCIDO=' || qt_dias_vencimento_w);
		elsif (nr_seq_perda_p is not null) then
			ds_mensagem_w := wheb_mensagem_pck.get_texto(1031459, 'NR_SEQUENCIA=' || nr_seq_cobranca_p || ';QT_DIAS_VENCIDO=' || qt_dias_vencimento_w);
		end if;
		
		wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);
		
	end if;
	
elsif	(cheque_cr_cobr_w is not null)then
	
	select	trunc(max(a.dt_vencimento),'dd') + qt_dias_vencimento_w
	into	dt_vencimento_w
	from	cheque_cr a
	where	a.nr_seq_cheque = cheque_cr_cobr_w;
		
	if	(trunc(dt_vencimento_w,'dd') > trunc(sysdate,'dd')) then
	
		if (nr_seq_cobranca_p is not null) then
			ds_mensagem_w := wheb_mensagem_pck.get_texto(334752, 'NR_SEQUENCIA=' || nr_seq_cobranca_p || ';QT_DIAS_VENCIDO=' || qt_dias_vencimento_w);
		elsif (nr_seq_perda_p is not null) then
			ds_mensagem_w := wheb_mensagem_pck.get_texto(1031460, 'NR_SEQUENCIA=' || nr_seq_cobranca_p || ';QT_DIAS_VENCIDO=' || qt_dias_vencimento_w);
		end if;
		
			wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);
	end if;
end if;

	
if	((dt_inicio_vigencia_w is not null) 
	and (trunc(sysdate,'dd') < trunc(dt_inicio_vigencia_w,'dd'))) or
	((dt_final_vigencia_w is not null) 
	and (trunc(sysdate,'dd') > trunc(dt_final_vigencia_w,'dd'))) then
	wheb_mensagem_pck.exibir_mensagem_abort(334754);
end if;


if	(ie_contrato_cancelado_w = 'S') and  
	(cd_pessoa_fisica_contrato_w is not null) then


	select	max(a.dt_cancelamento)
	into	dt_cancelamento_w
	from	pls_contrato a
	where	a.cd_pf_estipulante = cd_pessoa_fisica_contrato_w;


	if	(dt_cancelamento_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(334770);
	end if;
end if;

if	(ie_titulo_pls_w = 'S') and
	(nr_titulo_cobr_w is not null)then
	
	select	max(a.ie_origem_titulo)
	into	ie_origem_titulo_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_cobr_w;
	
	if	(ie_origem_titulo_w <> 3) then		
		if (nr_seq_cobranca_p is not null) then
			ds_mensagem_w := wheb_mensagem_pck.get_texto(334779);
		elsif (nr_seq_perda_p is not null) then	
			ds_mensagem_w := wheb_mensagem_pck.get_texto(1031463);			
		end if;	

	wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);			
	end if;
end if;

if	(ie_uso_plano_w = 'S') and
	(nr_titulo_cobr_w is not null) then
	
	select	nvl(max(a.cd_pessoa_fisica),0)
	into 	cd_pessoa_fisica_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_cobr_w;
	
	if (cd_pessoa_fisica_w <> 0) then	
		select	count(*)
		into	count_w
		from	pls_conta a
		where	a.nr_seq_segurado = cd_pessoa_fisica_w;
	
		if	(count_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(334798);
		end if;	


	end if;
end if;



select	max(a.ie_liberar_envio_orgao)
into 	ie_liberar_envio_orgao_w
from	orgao_cobranca a 
where	a.nr_sequencia = nr_seq_orgao_p;

if (nr_seq_cobranca_p is not null) and
   (nr_seq_orgao_p is not null) and
   (dt_inclusao_p is not null) then







	insert into cobranca_orgao	(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_cobranca,
					nr_seq_orgao,
					dt_pedido_retirada,
					dt_retirada,
					ds_observacao,
					dt_solicitacao_envio,
					dt_inclusao)
				values	(cobranca_orgao_seq.nextval,
					sysdate,
					nm_usuario_p,
					nr_seq_cobranca_p,
					nr_seq_orgao_p,
					null,
					null,
					ds_observacao_p,
					dt_inclusao_p,
					decode(ie_liberar_envio_orgao_w,'N',dt_inclusao_p, null));
end if;				

if	(ie_liberar_envio_orgao_w = 'S') then

	if	(nr_titulo_cobr_w is not null) then
	

		select	max(a.cd_pessoa_fisica),
			max(a.cd_cgc)
		into	cd_pessoa_fisica_titulo_w,
			cd_cgc_titulo_w
		from	titulo_receber a
		where	a.nr_titulo = nr_titulo_cobr_w;

		select	max(vl_saldo_titulo)
		into	vl_saldo_titulo_w
		from	titulo_receber
		where	nr_titulo = nr_titulo_cobr_w;

		select	count(*)
		into	qt_registro_exist_w
		from	titulo_receber_orgao_cobr
		where	nr_titulo = nr_titulo_cobr_w
		and	nr_seq_orgao_cobr = nr_seq_orgao_p;
		
		if	(qt_registro_exist_w > 0)then
			wheb_mensagem_pck.exibir_mensagem_abort(335895);
		end if;
	

		insert into titulo_receber_orgao_cobr	(nr_sequencia,
							nr_titulo,
							dt_atualizacao,
							nm_usuario,
							cd_pessoa_fisica,
							cd_cgc,
							nr_seq_orgao_cobr,
							vl_titulo,
							nr_seq_cobranca,
							ie_selecionado,
							dt_liberacao_envio, 
							nr_seq_perda) 
					values		(titulo_receber_orgao_cobr_seq.nextval,
							nr_titulo_cobr_w,
							sysdate,
							nm_usuario_p,
							cd_pessoa_fisica_titulo_w,
							cd_cgc_titulo_w,
							nr_seq_orgao_p,
							vl_saldo_titulo_w,
							nr_seq_cobranca_p,
							'N',
							sysdate,
							nr_seq_perda_p);

		
	elsif	(cheque_cr_cobr_w is not null)then
		
		select	max(a.cd_pessoa_fisica),
			max(a.cd_cgc)	
		into	cd_pessoa_fisica_cheque_w,
			cd_cgc_cheque_w
		from	cheque_cr a
		where	a.nr_seq_cheque = cheque_cr_cobr_w;
		
		select	vl_cheque,
			nr_cheque
		into	vl_cheque_w,
			nr_cheque_w
		from	cheque_cr
		where	nr_seq_cheque = cheque_cr_cobr_w;
		
		select	count(*)
		into	qt_registro_exist_w
		from	cheque_cr_orgao_cobr
		where	nr_seq_cheque = cheque_cr_cobr_w
		and	nr_seq_orgao_cobr = nr_seq_orgao_p;
		
		if	(qt_registro_exist_w > 0)then
			wheb_mensagem_pck.exibir_mensagem_abort(335897);
		end if;
		
		insert into cheque_cr_orgao_cobr	(nr_sequencia,
							nr_seq_cheque,
							nr_cheque,
							dt_atualizacao,
							nm_usuario,
							cd_pessoa_fisica,
							cd_cgc,
							nr_seq_orgao_cobr,
							vl_cheque,
							nr_seq_cobranca,
							ie_selecionado,
							dt_liberacao_envio,
							nr_seq_perda) 
					values		(cheque_cr_orgao_cobr_seq.nextval,
							cheque_cr_cobr_w,
							nr_cheque_w,
							sysdate,
							nm_usuario_p,
							cd_pessoa_fisica_cheque_w,
							cd_cgc_cheque_w,
							nr_seq_orgao_p,
							vl_cheque_w,
							nr_seq_cobranca_p,
							'N',
							sysdate,
							nr_seq_perda_p);
							
	else
							
		insert into outros_orgao_cobr		(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_orgao_cobr,
							nr_seq_cobranca,
							ie_selecionado,
							dt_liberacao_envio) 
					values		(outros_orgao_cobr_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_orgao_p,
							nr_seq_cobranca_p,
							'N',
							sysdate);
							
	end if;				
end if;

commit;
end incluir_cobranca_orgao;
/