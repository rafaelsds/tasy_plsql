create or replace
procedure pls_marcar_notif_cobr_receb
			(	nr_seq_registro_cobr_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number) is

nm_contato_w			varchar2(255);
ie_proprio_devedor_w		varchar2(1);
nr_seq_notif_pagador_w		number(10);
nr_seq_vinculo_pag_w		number(10);
dt_registro_w			date;
nr_seq_lote_notif_w		registro_cobranca.nr_seq_lote_notif%type;

Cursor C01 is
	select	d.nr_sequencia
	from	pls_notificacao_lote e,
		pls_notificacao_pagador d,
		pls_notificacao_item c,
		cobranca b,
		registro_cobr_item a
	where	e.nr_sequencia		= d.nr_seq_lote
	and	d.nr_sequencia		= c.nr_seq_notific_pagador
	and	b.nr_titulo		= c.nr_titulo
	and	b.nr_sequencia		= a.nr_seq_cobranca
	and	d.dt_recebimento_notif is null
	and	(e.dt_limite_notif_tel is null or e.dt_limite_notif_tel > fim_dia(sysdate))
	and	a.nr_seq_registro	= nr_seq_registro_cobr_p
	and	((d.nr_seq_lote = nr_seq_lote_notif_w) or (nr_seq_lote_notif_w is null));

begin
if	(nr_seq_registro_cobr_p is not null) then
	select	a.nm_contato,
		a.nr_seq_vinculo_pag,
		nvl(a.ie_proprio_devedor,'N'),
		a.dt_registro,
		a.nr_seq_lote_notif
	into	nm_contato_w,
		nr_seq_vinculo_pag_w,
		ie_proprio_devedor_w,
		dt_registro_w,
		nr_seq_lote_notif_w
	from	registro_cobranca a
	where	a.nr_sequencia	= nr_seq_registro_cobr_p;
	
	if	((nm_contato_w is null) or
		(nr_seq_vinculo_pag_w is null)) and
		(ie_proprio_devedor_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(188967);
	end if;
	
	if	(ie_proprio_devedor_w = 'S') then
		select	obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc)
		into	nm_contato_w
		from	registro_cobranca a
		where	a.nr_sequencia	= nr_seq_registro_cobr_p;
		
		select	max(a.nr_sequencia)
		into	nr_seq_vinculo_pag_w
		from	pls_vinculo_pagador a
		where	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.ie_proprio_pagador	= 'S';
	end if;
	
	open C01;
	loop
	fetch C01 into
		nr_seq_notif_pagador_w;
	exit when C01%notfound;
		begin
		update	pls_notificacao_pagador
		set	dt_recebimento_notif	= dt_registro_w,
			nm_pessoa_notific	= nm_contato_w,
			nr_seq_vinculo_pagador	= nr_seq_vinculo_pag_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_status_ant   = ie_status,
			ie_status		= 'R',
			nr_seq_registro_cobr	= nr_seq_registro_cobr_p
		where	nr_sequencia		= nr_seq_notif_pagador_w;
			
		pls_registrar_receb_notif(nr_seq_notif_pagador_w, 
				nm_contato_w,
				dt_registro_w,
				'L',
				nr_seq_registro_cobr_p);
		end;
	end loop;
	close C01;
end if;

commit;

end pls_marcar_notif_cobr_receb;
/