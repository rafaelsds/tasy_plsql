create or replace procedure inativa_pj_data_validade is 

type cd_cgc_t 		is table of pessoa_juridica.cd_cgc%type;
type ds_razao_social_t 	is table of pessoa_juridica.ds_razao_social%type;

nm_usuario_w 		regra_envio_email_inat_pj.nm_usuario%type;
cd_perfil_w 		regra_envio_email_inat_pj.cd_perfil%type;
cd_pessoa_fisica_w 	regra_envio_email_inat_pj.cd_pessoa_fisica%type;
cd_estabelecimento_w 	regra_envio_email_inat_pj.cd_estabelecimento%type;
email_origem_w        usuario.ds_email%type;
email_destino_w 	varchar2(255);
ds_assunto_w		varchar2(50);
mensagem_w 		varchar2(4000);
cd_cgc_w		cd_cgc_t;
ds_razao_social_w	ds_razao_social_t;

cursor c1 is
select distinct cd_estabelecimento
from regra_envio_email_inat_pj;

cursor c2 (cd_perfil_p in usuario_perfil.cd_perfil%type) is
select u.ds_email
from   usuario u,
       usuario_perfil p
where  u.nm_usuario = p.nm_usuario
and    p.cd_perfil  = cd_perfil_p;

cursor c3 is
select	cd_cgc, ds_razao_social
from	pessoa_juridica
where	dt_validade_pj 	is not null
and	dt_validade_pj 	<= sysdate
and	ie_situacao 	<> 'I';

begin
	ds_assunto_w := wheb_mensagem_pck.get_texto(1148113);
	mensagem_w   := wheb_mensagem_pck.get_texto(1148110) || chr(10);

	open c3;
	loop
	fetch c3
	bulk collect into cd_cgc_w, ds_razao_social_w
	limit 500;
	exit when cd_cgc_w.count = 0;
		begin
		forall i in 1 .. cd_cgc_w.count save exceptions
			update  pessoa_juridica
			set     ie_situacao 	= 'I',
				dt_atualizacao 	= sysdate,
				nm_usuario 	= 'Tasy'
			where   cd_cgc 		= cd_cgc_w(i);
		exception
			when others then
				for i in 1 .. sql%bulk_exceptions.count
				loop
					dbms_output.put_line(
					    sql%bulk_exceptions(i).error_index
					    || ': '
					    || sql%bulk_exceptions(i).error_code
					);
				end loop;
		end;
		
		for i in 1 .. ds_razao_social_w.count
		loop
			mensagem_w := mensagem_w || chr(10) || '- ' || ds_razao_social_w(i);
		end loop;
	end loop;
	
	open c1;
	loop
	fetch c1 into cd_estabelecimento_w;
	exit when c1%notfound;
		begin
			select 	 u.nm_usuario,
				 r.cd_perfil,
				 r.cd_pessoa_fisica,
				 u.ds_email
			into   	 nm_usuario_w,
				 cd_perfil_w,
				 cd_pessoa_fisica_w,
				 email_origem_w
			from   	 regra_envio_email_inat_pj r,
				 usuario u
			where  	 r.ie_situacao 		= 'A'
			and    	 r.cd_estabelecimento 	= cd_estabelecimento_w
			and    	 r.nm_usuario 		= u.nm_usuario
			and 	 rownum 		= 1
			order by r.nr_sequencia desc;
       
			if (cd_pessoa_fisica_w is not null) then
				select max(ds_email)
				into   email_destino_w
				from   compl_pessoa_fisica
				where  cd_pessoa_fisica = cd_pessoa_fisica_w
				and    ie_tipo_complemento in (1, 2, 8, 9);
        
				enviar_email(
				  ds_assunto_w,
				  mensagem_w,
				  email_origem_w,
				  email_destino_w,
				  nm_usuario_w,
				  'M'
				);
        
				select nm_usuario_princ_ci
				into   nm_usuario_w
				from   pessoa_fisica
				where  cd_pessoa_fisica = cd_pessoa_fisica_w; 
        
				if (nm_usuario_w is null) then
					select max(nm_usuario)
					into   nm_usuario_w
					from   usuario
					where  cd_pessoa_fisica = cd_pessoa_fisica_w;
				end if;
        
				insert into comunic_interna(
				       dt_comunicado,	
				       ds_titulo,	
				       ds_comunicado,	
				       nm_usuario,
				       dt_atualizacao,	
				       ie_geral,	
				       nr_sequencia, 
				       ie_gerencial,
				       dt_liberacao,
				       nm_usuario_destino)
				values(sysdate,	
				       ds_assunto_w,
				       mensagem_w,	
				       nm_usuario_w,
				       sysdate,	
				       'N',	
				       comunic_interna_seq.nextval,
				       'N',	
				       sysdate,
				       nm_usuario_w);
			else if (cd_perfil_w is not null) then
				open c2(cd_perfil_w);
				loop
				fetch c2 into email_destino_w;
				exit when c2%notfound;
					begin
					    enviar_email(
					      ds_assunto_w,
					      mensagem_w,
					      email_origem_w,
					      email_destino_w,
					      nm_usuario_w,
					      'M'
					    );
					end;
				end loop;
				close c2;
			end if;
			end if;
		end;
	end loop;
	close c1;
  
	commit;
end inativa_pj_data_validade;
/
