create or replace
procedure ctb_alterar_registro_cem(	cd_registro_p		Varchar2,
					nr_seq_regra_p		number,
					ds_arquivo_p		Varchar2,
					nm_usuario_p		Varchar2) is 

begin

update 	ctb_regra_arq_mex_registro
set 	ds_arquivo = ds_arquivo_p, nm_usuario_geracao = nm_usuario_p, dt_geracao = sysdate
where 	cd_registro = cd_registro_p
and 	nr_seq_regra = nr_seq_regra_p;

commit;

end ctb_alterar_registro_cem;
/