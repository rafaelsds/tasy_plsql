create or replace 
procedure exportar_titulo_receber_ems_v3 ( nm_usuario_p			varchar,
										cd_estabelecimento_p	number,
										dt_inicial_p 			date,
										dt_final_p 				date,
										cd_cliente_p			number)is

ds_conteudo_w			varchar2(4000);	
ie_tipo_registro_w		varchar2(3);
ds_branco1_w			varchar2(1);
cd_cliente_w			number(9);	
cd_especie_doc_w		varchar2(3);
cd_serie_docto_w		varchar2(3);
cd_tit_acr_w			number(10);
cd_indic_econ_w			varchar2(8);
nr_parcela_w			varchar2(2);
dt_emissao_w			varchar2(8);	
dt_vencimento_w			varchar2(8);
ds_brancos8_w			varchar2(8);
vl_titulo_w				titulo_receber.vl_titulo%type;
vl_desc_previsto_w		titulo_receber.vl_desc_previsto%type;
tx_desc_antecipacao_w	titulo_receber.tx_desc_antecipacao%type;
ds_brancos2_w			varchar2(2);
tx_multa_w				titulo_receber.tx_multa%type;
cd_portador_w			varchar2(5);
cd_carteira_w			varchar2(3);
ds_endereco_cobr_w		varchar2(15);
ds_situacao_banco_w		varchar2(12);
ds_situacao_tit_w		varchar2(13);
nr_titulo_w				titulo_receber.nr_titulo%type;
vl_classificacao_w		varchar2(11);
cd_conta_contabil_w		varchar2(20);	
cd_pessoa_fisica_w		titulo_receber.cd_pessoa_fisica%type;
cd_cgc_w				titulo_receber.cd_cgc%type;		
cd_cliente_externo_w	varchar2(9);
cd_interno_w			varchar2(40);
ie_tipo_movto_w			varchar2(10);	
vl_recebido_w			titulo_receber_liq.vl_recebido%type;
nr_seq_caixa_rec_w		titulo_receber_liq.nr_seq_caixa_rec%type;
ie_ordem_w				number(10);
nr_seq_parcela_w		movto_cartao_cr_parcela.nr_sequencia%type;
nr_sequencia_w			number(10);
ds_sequencia_w			varchar2(10);
				
Cursor C01 is
	select	'200' ie_tipo_registro,
			' ' ds_branco1,
			lpad(nvl(decode(cd_cliente_p,0,999999999,cd_cliente_p),999999999),9,0) cd_cliente,
			'DSH' cd_especie_doc,
			'   ' cd_serie_docto,
			a.nr_titulo cd_tit_acr,
			'REAL    ' cd_indic_econ,
			--'01' nr_parcela,
			to_char(a.dt_emissao,'DDMMYYYY') dt_emissao,
			--to_char(a.dt_pagamento_previsto,'DDMMYYYY') dt_vencimento,
			lpad(' ',8,' ') ds_brancos8,
			a.vl_titulo,
			a.vl_desc_previsto,
			a.tx_desc_antecipacao,
			lpad(' ',2,' ') ds_brancos2,
			a.tx_multa,
			--'999  ' cd_portador, /*coloquei fixo pq nao especifca no layout*/
			'999'/*(select rpad(max(x.cd_carteira),3,' ') from banco_carteira x where x.nr_sequencia = a.nr_seq_carteira_cobr) */ cd_carteira,
			rpad('CLIENTE',15,' ') ds_endereco_cobr,
			rpad('LIBERADO',12,' ') ds_situacao_banco,
			rpad('NORMAL',13,' ') ds_situacao_tit,
			a.nr_titulo,
			a.cd_pessoa_fisica,
			a.cd_cgc,
			b.nr_seq_caixa_rec
	from	titulo_receber a,
			titulo_receber_liq b
	where	a.dt_emissao between trunc(dt_inicial_p) and fim_dia(dt_final_p)
	and		a.cd_estabelecimento = cd_estabelecimento_p
	and		a.nr_titulo 		= b.nr_titulo
	and		b.nr_seq_caixa_rec is not null;

	
Cursor C02 is
	select	lpad(replace(to_char(a.vl_classificacao, 'fm000000000.00'),'.',''),11,'0') vl_classificacao,
			--lpad(a.cd_conta_contabil,20,' ') cd_conta_contabil
			lpad(somente_numero((SELECT MAX(x.cd_classificacao) FROM conta_contabil x WHERE x.cd_conta_contabil = a.cd_conta_contabil)),20,' ') cd_conta_contabil
	from	titulo_receber_classif a,
			titulo_receber b
	where	a.nr_titulo = b.nr_titulo
	and		b.nr_titulo = nr_titulo_w;
	
Cursor C03 is
	/*select	'CARTAO' ie_tipo_movto,
			lpad(substr(to_char(d.nr_parcela),1,2),2,'0') nr_parcela,
			d.vl_parcela vl_recebido,
			decode( (select max(x.nr_seq_bandeira) from movto_cartao_cr x where x.nr_sequencia =  d.nr_seq_movto),1,'600  ',2,'610  ',3,'620  ','999  ') cd_portador
	from	titulo_receber_liq a,
			tipo_recebimento b,
			tit_rec_liq_cartao_cr c,
			movto_cartao_cr_parcela d
	where	a.cd_tipo_recebimento     = b.cd_tipo_recebimento
	and		a.nr_sequencia 		      = c.nr_seq_baixa
	and		a.nr_titulo	   		  	  = c.nr_titulo
	and		c.nr_seq_movto			  = d.nr_seq_movto
	and		a.nr_titulo    		  	  = nr_titulo_w
	and		b.ie_tipo_consistencia 	  = 2
	union all
	select	'CARTAO' ie_tipo_movto,
		   lpad(substr(to_char(e.nr_parcela),1,2),2,'0') nr_parcela,
		   e.vl_parcela vl_recebido,
		   decode( (select max(x.nr_seq_bandeira) from movto_cartao_cr x where x.nr_sequencia =  d.nr_sequencia),1,'600  ',2,'610  ',3,'620  ','999  ') cd_portador
	from   titulo_receber_liq a,
	 	   caixa_receb b,
		   tipo_recebimento c,
		   movto_cartao_cr d,
		   movto_cartao_cr_parcela e
	where  a.nr_seq_caixa_rec 	 = b.nr_sequencia
	and	   a.cd_tipo_recebimento = c.cd_tipo_recebimento
	and	   a.nr_titulo 			 = nr_titulo_w
	and	   b.nr_sequencia		 = d.nr_seq_caixa_rec
	and	   d.nr_sequencia		 = e.nr_seq_movto
	union all
	select	'ESPECIE' ie_tipo_movto,
			'01' nr_parcela,
			sum(a.vl_recebido) vl_recebido,
			'242  ' cd_portador
	from	titulo_receber_liq a,
			tipo_recebimento b
	where	a.cd_tipo_recebimento     = b.cd_tipo_recebimento
	and		a.nr_titulo    		  	  = nr_titulo_w
	and		b.ie_tipo_consistencia 	  = 14 
	group by '01'
	union all
	select	'CHEQUE' ie_tipo_movto,
			lpad(substr(to_char(rownum),1,2),2,'0') nr_parcela,
			a.vl_recebido vl_recebido,
			'901  ' cd_portador
	from	titulo_receber_liq a,
			tipo_recebimento b
	where	a.cd_tipo_recebimento     = b.cd_tipo_recebimento
	and		a.nr_titulo    		  	  = nr_titulo_w
	and		b.ie_tipo_consistencia 	  = 3; */
	select	'CARTAO' ie_tipo_movto,
		   lpad(substr(to_char(e.nr_parcela),1,2),2,'0') nr_parcela,
		   e.vl_parcela vl_recebido,
		   decode( (select max(x.nr_seq_bandeira) from movto_cartao_cr x where x.nr_sequencia =  d.nr_sequencia),1,'600  ',2,'610  ',3,'620  ','999  ') cd_portador,
		   3 ie_ordem,
		   e.nr_sequencia,
		   to_char(e.dt_parcela,'DDMMYYYY')
	from   titulo_receber_liq a,
		   caixa_receb b,
		   movto_cartao_cr d,
		   movto_cartao_cr_parcela e
	where  a.nr_seq_caixa_rec 	 = b.nr_sequencia
	and	   a.nr_titulo 			 = nr_titulo_w
	and	   b.nr_sequencia		 = d.nr_seq_caixa_rec
	and	   d.nr_sequencia		 = e.nr_seq_movto
	and	   b.nr_sequencia		 = nr_seq_caixa_rec_w
	union all
	select	'CHEQUE' ie_tipo_movto,
		   lpad(substr(to_char(rownum),1,2),2,'0') nr_parcela,
		   c.vl_cheque vl_recebido,
		   '901  ' cd_portador,
		   2 ie_ordem,
		   null,
		   to_char(c.dt_vencimento,'DDMMYYYY')
	from   titulo_receber_liq a,
		   caixa_receb b,
		   cheque_cr c
	where  a.nr_seq_caixa_rec 	 = b.nr_sequencia
	and	   a.nr_titulo 			 = nr_titulo_w
	and	   b.nr_sequencia		 = c.nr_seq_caixa_rec
	and	   b.nr_sequencia		 = nr_seq_caixa_rec_w
	union all
	select	'ESPECIE' ie_tipo_movto,
		   lpad(substr(to_char(rownum),1,2),2,'0') nr_parcela,
		   b.vl_especie vl_recebido,
		   '242  ' cd_portador,
		   1 ie_ordem,
		   null,
		   to_char(b.dt_recebimento,'DDMMYYYY')
	from   titulo_receber_liq a,
		   caixa_receb b
	where  a.nr_seq_caixa_rec 	 = b.nr_sequencia
	and	   a.nr_titulo 			 = nr_titulo_w
	and	   b.nr_sequencia		 = nr_seq_caixa_rec_w
	and	   nvl(b.vl_especie,0)	 > 0 	
    order by 5,6;
																			
begin

delete 
from	w_envio_banco
where	nm_usuario =  nm_usuario_p;

/*Essa tratativa aqui foi efetuada devido ao cliente necessitar de um sequencial para o sistema da totvs, que nao pode repetir, no lugar do IMPTASYCRE*/
nr_sequencia_w := trunc(sysdate) - to_date('01/01/2005','dd/mm/yyyy');
ds_sequencia_w := lpad(nvl(to_char(nr_sequencia_w),'0'),10,'0');

/*INICIO HEADER - registro 100*/
ds_conteudo_w :=	'100' || /*pos 1 a 3*/
					'   ' || /*pos 4 a 6*/
					ds_sequencia_w || /*'IMPTASYCRE' || /*pos 7 a 16*/
					to_char(sysdate, 'DDMMYYYY') || /*pos 17 a  24*/
					' ' || /*pos 25*/
					lpad(' ',13,' ') || /*pos 26 a 38*/
					lpad(' ',3,' ') || /*pos 39 a 41*/
					lpad(' ',10,' ') || /*pos 42 a 51*/
					lpad(' ',5,' ') || /*pos 52 a 56*/
					rpad('11',5,' '); /*pos 57 a 61*/

insert	into w_envio_banco	(cd_estabelecimento,
							 ds_conteudo,
							 dt_atualizacao,
							 dt_atualizacao_nrec,
							 nm_usuario,
							 nm_usuario_nrec,
							 nr_seq_apres,
							 nr_sequencia)	
					values  (cd_estabelecimento_p,
							 ds_conteudo_w,
							 sysdate,
							 sysdate,
							 nm_usuario_p,
							 nm_usuario_p,
							 1,
							 w_envio_banco_seq.nextval);
/*FIM HEADER*/

/*Cursorregistro 200*/
open C01;
loop
fetch C01 into	
	ie_tipo_registro_w,
	ds_branco1_w,
	cd_cliente_w,
	cd_especie_doc_w,
	cd_serie_docto_w,
	cd_tit_acr_w,
	cd_indic_econ_w,
	--nr_parcela_w,
	dt_emissao_w,
	--dt_vencimento_w,
	ds_brancos8_w,
	vl_titulo_w,
	vl_desc_previsto_w,
	tx_desc_antecipacao_w,
	ds_brancos2_w,
	tx_multa_w,
	--cd_portador_w,
	cd_carteira_w,
	ds_endereco_cobr_w,
	ds_situacao_banco_w,
	ds_situacao_tit_w,
	nr_titulo_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	nr_seq_caixa_rec_w;
exit when C01%notfound;
	begin
	
	/*Verificar Pessa fisica*/
	if (trim(cd_pessoa_fisica_w) is not null) then
		
		/*Tenta pegar o codigo do sistema anterior no cadastro de PF*/	
		select	lpad(substr(max(a.cd_sistema_ant),1,9),9,'0')
		into	cd_cliente_externo_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		if (cd_cliente_externo_w is null) then /*Se n�o achar pelo codigo do sistema anterior, procura no DE/PARA */
			cd_cliente_externo_w	:= substr(obter_conversao_interna(null,'PESSOA_FISICA','CD_PESSOA_FISICA',trim(cd_pessoa_fisica_w)),1,9);
		end if;
				
	end if;	
	
	/*Verificar Pessa  CGC*/
	if (trim(cd_cgc_w) is not null) then
		
		/*Tenta pegar o codigo do sistema anterior no cadastro de PF*/	
		select	lpad(substr(max(a.cd_sistema_ant),1,9),9,'0')
		into	cd_cliente_externo_w
		from	pessoa_juridica a
		where	a.cd_cgc = cd_cgc_w;
		
		if (cd_cliente_externo_w is null) then /*Se n�o achar pelo codigo do sistema anterior, procura no DE/PARA */
			cd_cliente_externo_w	:= substr(obter_conversao_interna(null,'PESSOA_JURIDICA','CD_CGC',trim(cd_cgc_w)),1,9);
		end if;
				
	end if;	
	
	open C03;
	loop
	fetch C03 into	
		ie_tipo_movto_w, -- Esse campo apenas para auxilio no cursor
		nr_parcela_w,
		vl_recebido_w,
		cd_portador_w,
		ie_ordem_w,
		nr_seq_parcela_w,
		dt_vencimento_w;
	exit when C03%notfound;
		begin
		
		/*Registro 200*/
		ds_conteudo_w := ie_tipo_registro_w || /*pos 1 a 3*/
						 ds_branco1_w || /*pos 4*/	
						 lpad(nvl(cd_cliente_externo_w,cd_cliente_w),9,0) || /*Cod cliente pos 5 a 13*/
						 cd_especie_doc_w || /*pos 14 a 16*/
						 cd_serie_docto_w || /*pos 17 a 19*/
						 lpad(substr(cd_tit_acr_w,1,10),10,' ') || /*pos 20 a 29*/
						 cd_indic_econ_w || /*pos 30 a 37*/
						 nr_parcela_w || /*pos 38 a 39*/
						 dt_emissao_w || /*pos 40 a 47*/
						 dt_vencimento_w || /*pos 48 a 55*/
						 ds_brancos8_w || /*pos 56 a 63*/
						 ds_brancos8_w || /*pos 64 a 71*/
						 '   ' || /*72 a 74*/
						 lpad(replace(to_char(vl_recebido_w, 'fm000000000.00'),'.',''),11,'0') || /*75 a 85*/
						 '   ' || /*86 a 88*/
						 lpad(' ',11,' ')  || /*pos 89 a 99*/
						 '      ' || /*pos 100 a 105*/
						 '  ' || /*pos 106 a 107*/
						 lpad(' ',4,' ') || /*pos 108 111*/
						 '   ' || /*pos 112 a 114*/
						 '   ' || /*pos 115 a 117*/
						 '   ' || /*pos 118 a 120*/
						 '           ' || /*pos 121 a 131*/
						 '        ' || /*pos 132 a 139*/
						 '        ' || /*pos 140 a 147*/
						 '      ' || /*pos 148 a 153*/
						 '    ' || /*pos 154 a 157*/
						 '    ' || /*pos 158 a 161*/ 
						 cd_portador_w || /*pos 162 a 166*/
						 '   ' || /*pos 167 a 169*/
						 rpad(nvl(cd_carteira_w,' '),3,' ') || /*pos 170 a 172*/
						 rpad(' ',118,' ') || /*118 brancos, 173 a 290*/
						 ds_endereco_cobr_w || /*pos 291 a 305 */
						 rpad(' ',21,' ') || /*21 brancos, 306 a 326*/
						 ds_situacao_banco_w || /*pos 327 a 338*/
						 ds_situacao_tit_w || /*pos 339 a 351*/
						 rpad(' ',105,' '); /*105 brancos, 352 a 456*/
			 
		insert	into w_envio_banco	(cd_estabelecimento,
									 ds_conteudo,
									 dt_atualizacao,
									 dt_atualizacao_nrec,
									 nm_usuario,
									 nm_usuario_nrec,
									 nr_seq_apres,
									 nr_sequencia)	
							values  (cd_estabelecimento_p,
									 ds_conteudo_w,
									 sysdate,
									 sysdate,
									 nm_usuario_p,
									 nm_usuario_p,
									 2,
									 w_envio_banco_seq.nextval);
		end;
	end loop;
	close C03;
	/*fim registro 200*/

	/*inico registro 300*/
	ds_conteudo_w := null;
	open C02;
	loop
	fetch C02 into	
		vl_classificacao_w,
		cd_conta_contabil_w;
	exit when C02%notfound;
		begin
		
		ds_conteudo_w :=	'300' || /*pos 1 a 3*/
							lpad(' ',20,' ') || /*pos 4 a 23*/
							lpad(' ',15,' ') || /*pos 24 a 38*/
							lpad(' ',8,' ') || /*pos 39 a 46*/
							lpad(' ',20,' ') || /*pos 47 a 66*/
							vl_classificacao_w || /*pos 67 a 77*/
							lpad(' ',3,' ') || /*pos 78 a 80*/
							'N' || /*pos 81*/
							'ANS2016 ' || /*pos 82 a 89*/
							cd_conta_contabil_w || /*90 a 109*/
							'PLA' || /*pos 110 a 112*/
							rpad('1.1.99',12,' ') || /*pos  113 a 124*/
							lpad(' ',8,' ') || /*pos 125 a 132*/
							lpad(' ',11,' ') || /*pos 133 a 143*/
							lpad(' ',8,' ') || /*pos 144 a 151*/
							lpad(' ',20,' '); /*pos 152 a 171*/
		
		insert	into w_envio_banco	(cd_estabelecimento,
									 ds_conteudo,
									 dt_atualizacao,
									 dt_atualizacao_nrec,
									 nm_usuario,
									 nm_usuario_nrec,
									 nr_seq_apres,
									 nr_sequencia)	
							values  (cd_estabelecimento_p,
									 ds_conteudo_w,
									 sysdate,
									 sysdate,
									 nm_usuario_p,
									 nm_usuario_p,
									 2,
									 w_envio_banco_seq.nextval);					
		
		end;
	end loop;
	close C02;
	/*Se o titulo nao tiver classificacao, o ds conteudo vai ser nulo, entao sera inserido uma linha apenas com o inicial 300 para esse segmento*/
	if (ds_conteudo_w is null) then 
		ds_conteudo_w :=	'300' || /*pos 1 a 3*/
							lpad(' ',20,' ') || /*pos 4 a 23*/
							lpad(' ',15,' ') || /*pos 24 a 38*/
							lpad(' ',8,' ') || /*pos 39 a 46*/
							lpad(' ',20,' ') || /*pos 47 a 66*/
							lpad(' ',11,' ') || /*pos 67 a 77*/
							lpad(' ',3,' ') || /*pos 78 a 80*/
							'N' || /*pos 81*/
							'ANS2016 ' || /*pos 82 a 89*/
							lpad(' ',20,' ') || /*90 a 109*/
							'PLA' || /*pos 110 a 112*/
							rpad('1.1.99',12,' ') || /*pos  113 a 124*/
							lpad('UNIMED  ',8,' ') || /*pos 125 a 132*/
							lpad(' ',11,' ') || /*pos 133 a 143*/
							lpad(' ',8,' ') || /*pos 144 a 151*/
							lpad(' ',20,' '); /*pos 152 a 171*/
							
		insert	into w_envio_banco	(cd_estabelecimento,
									 ds_conteudo,
									 dt_atualizacao,
									 dt_atualizacao_nrec,
									 nm_usuario,
									 nm_usuario_nrec,
									 nr_seq_apres,
									 nr_sequencia)	
							values  (cd_estabelecimento_p,
									 ds_conteudo_w,
									 sysdate,
									 sysdate,
									 nm_usuario_p,
									 nm_usuario_p,
									 2,
									 w_envio_banco_seq.nextval);					
	end if;
    /*fim registro 300*/	
	end;
end loop;
close C01;

/*Inicio registro 900*/
ds_conteudo_w	:= '900';

insert	into w_envio_banco	(cd_estabelecimento,
							 ds_conteudo,
							 dt_atualizacao,
							 dt_atualizacao_nrec,
							 nm_usuario,
							 nm_usuario_nrec,
							 nr_seq_apres,
							 nr_sequencia)	
					values  (cd_estabelecimento_p,
							 ds_conteudo_w,
							 sysdate,
							 sysdate,
							 nm_usuario_p,
							 nm_usuario_p,
							 3,
							 w_envio_banco_seq.nextval);
/*Fim registro 900*/
commit;

end exportar_titulo_receber_ems_v3; 
/