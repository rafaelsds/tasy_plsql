create or replace procedure pfcs_read_targets(
                        ie_classification_p	varchar2,
						nm_usuario_p		varchar2) is

	cd_establishment_w	pfcs_operational_level.cd_establishment%type;
	nr_seq_language_w	usuario.nr_seq_idioma%type;

        cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_indicator,
		a.ie_condition,
		a.qt_initial_target,
		a.qt_final_target
	from	pfcs_indicator_target a, pfcs_indicator b
	where	a.nr_seq_indicator = b.nr_sequencia
	and	b.ie_classification <> 'CO'
	and	b.ie_classification = nvl(ie_classification_p,b.ie_classification)
	union
	select	a.nr_sequencia,
		a.nr_seq_indicator,
		a.ie_condition,
		a.qt_initial_target,
		a.qt_final_target
	from	pfcs_indicator_target_def a, pfcs_indicator b
	where	a.nr_seq_indicator = b.nr_sequencia
	and	b.ie_classification <> 'CO'
	and	b.ie_classification = nvl(ie_classification_p,b.ie_classification)
	and	not exists (select 1 from pfcs_indicator_target b where a.nr_seq_indicator = b.nr_seq_indicator);

	cursor c02 is
	select	a.cd_establishment
	from	pfcs_operational_level a;

begin

	select	nvl(max(obter_nr_seq_idioma(a.nm_usuario)), obter_pais_sistema(null,null,null)) nr_seq_language
	into	nr_seq_language_w
	from	usuario a
	where	a.nm_usuario = nm_usuario_p;

	wheb_usuario_pck.set_nr_seq_idioma(nr_seq_language_w);
	philips_param_pck.set_nr_seq_idioma(nr_seq_language_w);
	for c01_w in c01 loop
	begin
		for c02_w in c02 loop
		begin
			cd_establishment_w := c02_w.cd_establishment;

      wheb_usuario_pck.set_cd_estabelecimento(cd_establishment_w);

			/* PFCS - Emergency Department */

			if (c01_w.nr_seq_indicator = 1) then
				pfcs_calculate_waiting_number(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 4) then

				pfcs_acceptable_times(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 7) then

				pfcs_calculate_patient_classif(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 8) then

				pfcs_detailed_accep_times(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 9) then

				pfcs_calculate_box_total(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 10) then

				pfcs_calculate_box_time_total(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 11) then

				pfcs_calculate_box_quantity(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 12) then

				pfcs_calculate_box_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 13) then

				pfcs_average_arrival_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 14) then

				pfcs_calculate_average_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 15) then

				pfcs_hospitalization_rate(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 16) then

				pfcs_patient_medication_rate(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 17) then

				pfcs_time_exceeded_specialty(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			/* PFCS - Bed Management */

			elsif (c01_w.nr_seq_indicator = 18) then

				pfcs_patient_solic_total(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 54) then

				pfcs_patient_solic_urgent(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 19) then

				pfcs_patient_classif_type(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 20) then

				pfcs_accomm_avg_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 21) then

				pfcs_calculate_occupancy(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 22) then

				pfcs_calculate_occup_type(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 23) then

				pfcs_inactive_beds(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 25) then

				pfcs_hospitalization_avg_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 26) then

				pfcs_patient_expec_discharge(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 27) then

				pfcs_calculate_wo_planned_dis(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 28) then

				pfcs_attend_discharge_total(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 29) then

				pfcs_delayed_sanitization(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 30) then

				pfcs_average_bed_release_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 31) then

				pfcs_calculate_readmission(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 32) then

				pfcs_solic_x_discharge(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 55) then

				pfcs_avg_bed_cleaning_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			/* PFCS - Surgical Center */

			elsif (c01_w.nr_seq_indicator = 62) then

				pfcs_not_scheduled_surgeries(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 66) then

				pfcs_surgeries_after_deadline(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 63) then

				pfcs_sched_surg_without_opme(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 64) then

				pfcs_hours_available(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 65) then

				pfcs_cost_of_scheduled_opme(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 67) then

				pfcs_sched_surg_of_the_day(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 68) then

				pfcs_available_hours_today(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 69) then

				pfcs_delayed_surgeries(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 70) then

				pfcs_average_setup_time(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 71) then

				pfcs_surgeries_done(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 72) then

				pfcs_calc_post_anesthetic(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 73) then

				pfcs_average_length_of_stay(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);

			elsif (c01_w.nr_seq_indicator = 74) then

				pfcs_discharge_post_anesthetic(
					nr_seq_indicator_p => c01_w.nr_seq_indicator,
					cd_estabelecimento_p => cd_establishment_w,
					nm_usuario_p => nm_usuario_p);
			end if;

		end;
		end loop;

	end;
	end loop;

end pfcs_read_targets;
/