create or replace
procedure ctb_duplicar_demonstrativo(		nr_sequencia_p		number,
					nr_seq_mes_ref_p		number,
					nm_usuario_p		Varchar2) is 
					
nr_sequencia_w		number(10);

begin

select	ctb_demonstrativo_seq.nextval
into	nr_sequencia_w
from	dual;

insert	into ctb_demonstrativo(
	nr_sequencia,
	nr_seq_mes_ref,
	nr_seq_tipo,
	dt_atualizacao,
	nm_usuario,
	cd_estabelecimento,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_unid_neg,
	ds_titulo)
(select	nr_sequencia_w,
	nr_seq_mes_ref_p,
	a.nr_seq_tipo,
	sysdate,
	nm_usuario_p,
	a.cd_estabelecimento,
	sysdate,
	nm_usuario_p,
	a.nr_seq_unid_neg,
	substr(wheb_mensagem_pck.get_texto(298049,null) || ' ' || ds_titulo,99)
from	ctb_demonstrativo a
where	a.nr_sequencia		= nr_sequencia_p);
	
	
insert	into ctb_demo_mes(
	nr_sequencia,
	nr_seq_demo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_mes_ref,
	nr_seq_coluna,
	ie_valor,
	nr_seq_col_1,
	nr_seq_col_2,
	ds_coluna,		
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_informacao,
	nr_seq_colunas)
(select	ctb_demo_mes_seq.nextval,
	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	nr_seq_mes_ref,
	b.nr_seq_coluna,
	b.ie_valor,
	b.nr_seq_col_1,
	b.nr_seq_col_2,
	b.ds_coluna,
	sysdate,
	nm_usuario_p,
	b.ie_informacao,
	b.nr_seq_colunas
from	ctb_demo_mes b
where	b.nr_seq_demo 		= nr_sequencia_p);
commit;


insert into ctb_demonstrativo_lib (	nr_sequencia, 
									nr_seq_demonstrativo, 
									dt_atualizacao, 
									nm_usuario, 
									dt_atualizacao_nrec, 
									nm_usuario_nrec, 
									cd_perfil, 
									cd_cargo, 
									nm_usuario_lib, 
									ie_permissao, 
									nr_seq_modelo)
							(select ctb_demonstrativo_lib_seq.nextval, 
									nr_seq_demonstrativo, 
									sysdate, 
									nm_usuario_p, 
									dt_atualizacao_nrec, 
									nm_usuario_nrec, 
									cd_perfil, 
									cd_cargo, 
									nm_usuario_lib, 
									ie_permissao, 
									nr_seq_modelo
							from	ctb_demonstrativo_lib
							where	nr_seq_demonstrativo = nr_sequencia_p);

commit;


end ctb_duplicar_demonstrativo;
/