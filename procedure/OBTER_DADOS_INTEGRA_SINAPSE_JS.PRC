create or replace
procedure obter_dados_integra_sinapse_js(
					nr_prescricao_p		number,
					nr_seq_prescricao_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2,
					cd_empresa_p	out	number,
					ds_pasta_exp_xml_p out 	varchar2,
					ds_nome_arquivo_p out 	varchar2,
					ie_integra_sinapse_p out varchar2) is 

ie_incluir_acesso_dicom_w	varchar2(1);	
nr_acesso_dicom_w		varchar2(20) := '';				
					
begin

select  nvl(max(nr_seq_empresa_integr),0) 
into	cd_empresa_p
from    empresa_integr_dados 
where  ((nr_seq_empresa_integr = 31) or 	(nr_seq_empresa_integr = 41) or (nr_seq_empresa_integr = 106) or (nr_seq_empresa_integr = 232))
and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

select  max(ds_caminho_saida)
into	ds_pasta_exp_xml_p
from    empresa_integr_dados a, 
        empresa_integracao b,
        sistema_integracao c
where   a.nr_seq_empresa_integr = b.nr_sequencia 
and	b.nr_sequencia = c.nr_seq_empresa
and	nvl(a.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
and      b.nr_sequencia = cd_empresa_p;

select	nvl(max(ie_incluir_acesso_dicom),'N') 
into	ie_incluir_acesso_dicom_w
from   	empresa_integr_dados
where  	nr_seq_empresa_integr = cd_empresa_p
and    	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

if	(ie_incluir_acesso_dicom_w = 'S') and
	(nvl(nr_seq_prescricao_p,0) > 0) then
	
	select 	nr_acesso_dicom
	into	nr_acesso_dicom_w	
        from    prescr_procedimento
        where	nr_prescricao = nr_prescricao_p
        and	nr_sequencia = nr_seq_prescricao_p;
	
	nr_acesso_dicom_w := nr_acesso_dicom_w || '_';

end if;

if	(nr_acesso_dicom_w <> '') then
	ds_nome_arquivo_p := obter_nm_arquivo_synapse(nr_prescricao_p);
	nr_acesso_dicom_w := '_' || nr_acesso_dicom_w || '.xml';
	ds_nome_arquivo_p := Substituir_Primeira_String(ds_nome_arquivo_p, '.xml', nr_acesso_dicom_w);
else 
	ds_nome_arquivo_p := obter_nm_arquivo_synapse(nr_prescricao_p);
	
end if;

ie_integra_sinapse_p := verifica_se_integra_sinapse(nr_prescricao_p);

commit;

end obter_dados_integra_sinapse_js;
/