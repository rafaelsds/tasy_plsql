create or replace  
procedure pls_alterar_proc_analise(	nr_sequencia_p			pls_conta_proc.nr_sequencia%type,
					nr_seq_item_ref_p		number,
					nr_seq_analise_p		pls_analise_conta.nr_sequencia%type,
					ie_tecnica_utilizada_p		pls_conta_proc.ie_tecnica_utilizada%type,
					ie_autogerado_p			pls_conta_proc.ie_autogerado%type,
					tx_item_p			pls_conta_proc.tx_item%type,
					tx_custo_operacional_p		pls_conta_proc.tx_custo_operacional%type,
					tx_material_p			pls_conta_proc.tx_material%type,
					tx_medico_p			pls_conta_proc.tx_medico%type,
					ie_cobranca_prevista_p		pls_conta_proc.ie_cobranca_prevista%type,
					ie_via_acesso_p			pls_conta_proc.ie_via_acesso%type,
					dt_procedimento_p		varchar2,
					dt_fim_proc_p			varchar2,
					dt_inicio_proc_p		varchar2,
					nr_seq_setor_atend_p		pls_conta_proc.nr_seq_setor_atend%type,
					nr_seq_grupo_p			number,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_criterio_horario_p		pls_conta_proc.ie_criterio_horario%type,
					vl_procedimento_imp_p		number,
					qt_procedimento_imp_p		number,
					cd_procedimento_p		procedimento.cd_procedimento%type,
					ie_origem_proced_p		procedimento.ie_origem_proced%type,
					ds_justificativa_p		pls_conta_proc.ds_justificativa%type,
					ds_especif_material_p		pls_conta_proc.ds_especif_material%type,
					cd_dente_p 				pls_conta_proc.cd_dente%type,
					cd_face_dente_p			pls_conta_proc.cd_face_dente%type,
					cd_regiao_boca_p		pls_conta_proc.cd_regiao_boca%type,
					nm_usuario_p			usuario.nm_usuario%type) is
 
ds_observacao_w			varchar2(4000);
ds_setor_atendimento_w		pls_setor_atendimento.ds_setor_atendimento%type;
ds_setor_atendimento_ww		pls_setor_atendimento.ds_setor_atendimento%type;
ie_tecnica_utilizada_w		pls_conta_proc.ie_tecnica_utilizada%type;
ie_autogerado_w			pls_conta_proc.ie_autogerado%type;
ie_cobranca_prevista_w		pls_conta_proc.ie_cobranca_prevista%type;
ie_via_acesso_w			pls_conta_proc.ie_via_acesso%type;
ie_preco_w			pls_plano.ie_preco%type;
ie_criterio_horario_w		pls_conta_proc.ie_criterio_horario%type;
ie_origem_conta_w		pls_conta.ie_origem_conta%type;
ie_valor_informado_w		pls_conta_proc.ie_valor_informado%type;
ie_vl_apresentado_sistema_w	pls_conta_proc.ie_vl_apresentado_sistema%type;
ie_atualiza_via_acesso_w	varchar2(1);
tx_item_via_acesso_w		pls_conta_proc.tx_item%type;
vl_procedimento_imp_w		pls_conta_proc.vl_procedimento_imp%type;
qt_procedimento_imp_w		pls_conta_proc.qt_procedimento_imp%type;
cd_procedimento_w		procedimento.cd_procedimento%type;
tx_item_w			pls_conta_proc.tx_item%type;
nr_seq_setor_atend_w		pls_conta_proc.nr_seq_setor_atend%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
nr_identificador_w		w_pls_resumo_conta.nr_identificador%type;
nr_seq_proc_ref_w		pls_conta_proc.nr_sequencia%type;
nr_seq_regra_via_acesso_w	pls_conta_proc.nr_seq_regra_via_acesso%type;
dt_procedimento_w		date;
dt_procedimento_ww		date;
dt_fim_proc_w			date;
dt_fim_proc_ww			date;
dt_inicio_proc_w		date;
dt_inicio_proc_ww		date;
ie_tx_manual_w			pls_conta_proc.ie_tx_manual%type;
tx_anestesista_w		pls_conta_proc.tx_anestesista%type;        
tx_auxiliares_w           	pls_conta_proc.tx_auxiliares%type;
tx_custo_operacional_w 		pls_conta_proc.tx_custo_operacional%type;
tx_material_w        		pls_conta_proc.tx_material%type;
tx_medico_w            		pls_conta_proc.tx_medico%type;
ie_via_acesso_manual_w		pls_conta_proc.ie_via_acesso_manual%type;
ie_taxa_manual_w		pls_conta_proc.ie_tx_manual%type;   
ie_cobranca_prevista_inf_w	pls_conta_proc.ie_cobranca_prevista_inf%type;
cd_dente_w 					pls_conta_proc.cd_dente%type;
cd_face_dente_w				pls_conta_proc.cd_face_dente%type;
cd_regiao_boca_w			pls_conta_proc.cd_regiao_boca%type;            

begin
if	(dt_procedimento_p = '  /  /    ') then
	dt_procedimento_w := null;
else
	dt_procedimento_w := to_date(dt_procedimento_p,'dd/mm/yyyy');
end if;

if	(dt_inicio_proc_p = '  :  :  ') or
	(dt_inicio_proc_p is null) then
	dt_inicio_proc_w := null;
else
	begin
	dt_inicio_proc_w := to_date(to_char(nvl(dt_procedimento_w,sysdate),'dd/mm/yyyy') || ' ' ||nvl(dt_inicio_proc_p, '00:00:01'),'dd/mm/yyyy hh24:mi:ss');
	exception
	when others then
		dt_inicio_proc_w := null;
	end;
end if;

if	(dt_fim_proc_p = '  :  :  ') or
	(dt_fim_proc_p is null) then
	dt_fim_proc_w := null;
else
	begin
	dt_fim_proc_w := to_date(to_char(nvl(dt_procedimento_w,sysdate),'dd/mm/yyyy') || ' ' ||nvl(dt_fim_proc_p, '00:00:01'),'dd/mm/yyyy hh24:mi:ss');
	exception
	when others then
		dt_fim_proc_w := null;
	end;
end if;

select	a.ie_tecnica_utilizada,
	a.ie_autogerado,
	a.tx_item,
	nvl(a.ie_cobranca_prevista,'N'),
	a.ie_via_acesso,
	a.dt_procedimento,
	a.dt_fim_proc,
	a.dt_inicio_proc,
	a.nr_seq_setor_atend,
	a.nr_seq_conta,
	a.cd_procedimento,
	a.ie_origem_proced,
	c.ie_preco,
	a.ie_criterio_horario,
	a.vl_procedimento_imp,
	a.qt_procedimento_imp,
	d.ie_origem_conta,
	a.ie_valor_informado,
	a.ie_vl_apresentado_sistema,
	a.nr_seq_regra_via_acesso,
	a.ie_tx_manual,
	a.tx_anestesista,
	a.tx_auxiliares,
	a.tx_custo_operacional,
	a.tx_material,
	a.tx_medico,
	a.nr_seq_proc_ref,
	a.cd_dente, 
	a.cd_face_dente, 
	a.cd_regiao_boca
into	ie_tecnica_utilizada_w,
	ie_autogerado_w,
	tx_item_w,
	ie_cobranca_prevista_w,
	ie_via_acesso_w,
	dt_procedimento_ww,
	dt_fim_proc_ww,
	dt_inicio_proc_ww,
	nr_seq_setor_atend_w,
	nr_seq_conta_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_preco_w,
	ie_criterio_horario_w,
	vl_procedimento_imp_w,
	qt_procedimento_imp_w,
	ie_origem_conta_w,
	ie_valor_informado_w,
	ie_vl_apresentado_sistema_w,
	nr_seq_regra_via_acesso_w,
	ie_tx_manual_w,
	tx_anestesista_w,
	tx_auxiliares_w,
	tx_custo_operacional_w,
	tx_material_w,
	tx_medico_w,
	nr_seq_proc_ref_w,
	cd_dente_w, 
	cd_face_dente_w, 
	cd_regiao_boca_w
from	pls_conta_proc	a,
	pls_plano	c,
	pls_segurado	b,
	pls_conta	d
where	d.nr_seq_segurado	= b.nr_sequencia(+)
and	b.nr_seq_plano		= c.nr_sequencia(+)
and	d.nr_sequencia		= a.nr_seq_conta
and	a.nr_sequencia 		= nr_sequencia_p;

if	(ie_preco_w in ('2','3')) and
	(nvl(ie_cobranca_prevista_p,'N') = 'S') then
	--(-20011,'Nao e permitido gerar cobranca prevista para beneficiario com produto Pos-estabelecido! #@#@');	
	wheb_mensagem_pck.exibir_mensagem_abort(191456);
end if;

if	(ie_origem_conta_w in ('A','E')) then
	if	(nvl(vl_procedimento_imp_w,0) <> nvl(vl_procedimento_imp_p,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(191454); /*Nao e permitico alterar o valor apresentado de arquivo importado*/
	end if;

	if	(nvl(qt_procedimento_imp_w,0) <> nvl(qt_procedimento_imp_p,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(191455); /*Nao e permitico alterar a quantidade apresentado de arquivo importado*/
	end if;
end if;

if	(nvl(vl_procedimento_imp_w,0) <> nvl(vl_procedimento_imp_p,0))then
	ie_valor_informado_w	:= 'S';
end if;

if	(nvl(vl_procedimento_imp_w,0) <> nvl(vl_procedimento_imp_p,0)) then
	ie_vl_apresentado_sistema_w	:= 'N';
end if;

if	(nvl(ie_via_acesso_w,'X') <> nvl(ie_via_acesso_p,'X')) then
	nr_seq_regra_via_acesso_w	:= null;
	ie_atualiza_via_acesso_w	:= obter_valor_param_usuario(1208, 28, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p);
	ie_via_acesso_manual_w		:= 'S';
	if	(nvl(ie_atualiza_via_acesso_w,'N') = 'S') then
		tx_item_via_acesso_w :=	obter_tx_proc_via_acesso(ie_via_acesso_p);
	end if;	
end if;

if	((tx_item_w <> tx_item_via_acesso_w) and (tx_item_via_acesso_w is not null)) then
	ie_tx_manual_w 		:= 'S';
	tx_anestesista_w	:= 100;
	tx_auxiliares_w		:= 100;
	tx_custo_operacional_w	:= 100;
	tx_material_w		:= 100;
	tx_medico_w		:= 100;

--Somente se nao tiver taxa de via de acesso
elsif	(((tx_item_w <> tx_item_p)    or
	  (tx_custo_operacional_p <>	tx_custo_operacional_w	) or
          (tx_material_p	  <>	tx_material_w	) or	
          (tx_medico_p		  <>	tx_medico_w	)) and 
	  (tx_item_via_acesso_w is null)) then
	
	ie_tx_manual_w 		:= 'S';
	tx_anestesista_w	:= 100;
	tx_auxiliares_w		:= 100;
	tx_custo_operacional_w	:= nvl(tx_custo_operacional_p,100);
	tx_material_w		:= nvl(tx_material_p, 100);
	tx_medico_w		:= nvl(tx_medico_p, 100);

--Para atualizar essas taxas quando alterar o tipo taxa item, (pls_taxa_item) , sendo 
--que ao tx_item e o mesmo que o anterior, porem nao e o mesmo
elsif	(tx_item_via_acesso_w is null) then

	tx_custo_operacional_w	:= nvl(tx_custo_operacional_p,100);
	tx_material_w		:= nvl(tx_material_p, 100);
	tx_medico_w		:= nvl(tx_medico_p, 100);

end if;

if	(nvl(ie_cobranca_prevista_p,'X') <> nvl(ie_cobranca_prevista_w,'X')) then	
	ie_cobranca_prevista_inf_w := 'S';
end if;

update	pls_conta_proc
set	ie_tecnica_utilizada			= ie_tecnica_utilizada_p,
	ie_autogerado				= ie_autogerado_p,
	tx_item					= nvl(tx_item_via_acesso_w,tx_item_p),
	ie_cobranca_prevista			= nvl(ie_cobranca_prevista_p,'N'),
	ie_cobranca_prevista_inf		= ie_cobranca_prevista_inf_w,
	ie_via_acesso				= ie_via_acesso_p,
	dt_procedimento				= dt_procedimento_w,
	dt_fim_proc				= dt_fim_proc_w,
	dt_inicio_proc				= dt_inicio_proc_w,
	ie_tx_manual				= ie_tx_manual_w,
	nr_seq_setor_atend			= nr_seq_setor_atend_p,
	ie_criterio_horario			= ie_criterio_horario_p,
	vl_procedimento_imp 			= vl_procedimento_imp_p,
	qt_procedimento_imp 			= qt_procedimento_imp_p,
	ie_valor_informado			= ie_valor_informado_w,
	nr_seq_regra_via_acesso 		= nr_seq_regra_via_acesso_w,
	ie_vl_apresentado_sistema		= ie_vl_apresentado_sistema_w,
	cd_procedimento				= cd_procedimento_p,
	ie_origem_proced			= ie_origem_proced_p,
	tx_anestesista				= tx_anestesista_w,
	tx_auxiliares				= tx_auxiliares_w,
	tx_custo_operacional			= tx_custo_operacional_w,
	tx_material				= tx_material_w,
	tx_medico				= tx_medico_w,
	ie_via_acesso_manual			= ie_via_acesso_manual_w,
	ds_justificativa			= ds_justificativa_p,
	ds_especif_material			= ds_especif_material_p,
	cd_dente 				= cd_dente_p, 
	cd_face_dente 			= cd_face_dente_p, 
	cd_regiao_boca 			= cd_regiao_boca_p
where	nr_sequencia 				= nr_sequencia_p;

--aaschlote 13/10/2015 OS 958416 - Tratamento das contas canceladas de honorario, feito para desagrupar na analise de contas
if	(nr_seq_proc_ref_w is not null) then
	update	pls_conta_proc
	set	dt_procedimento	= dt_procedimento_w,
		dt_fim_proc	= dt_fim_proc_w,
		dt_inicio_proc	= dt_inicio_proc_w
	where	nr_sequencia	= nr_seq_proc_ref_w
	and	ie_status	= 'D';
end if;

select	max(nr_identificador)
into	nr_identificador_w
from	w_pls_resumo_conta
where	nr_seq_item 		= nr_sequencia_p
and	ie_tipo_item		= 'P';

if	(nvl(vl_procedimento_imp_w,0) <> nvl(vl_procedimento_imp_p,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Valor apresentado:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||vl_procedimento_imp_w||' - Modificado: '||vl_procedimento_imp_p||chr(13)||chr(10);
end if;

if	(nvl(qt_procedimento_imp_w,0) <> nvl(qt_procedimento_imp_p,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Quantidade apresentada:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||qt_procedimento_imp_w||' - Modificado: '||qt_procedimento_imp_p||chr(13)||chr(10);
end if;
	
if	(nvl(ie_tecnica_utilizada_p,'X') <> nvl(ie_tecnica_utilizada_w,'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Tecnica utilizada:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||obter_valor_dominio(1834, ie_tecnica_utilizada_w)||' - Modificada: '||
				obter_valor_dominio(1834, ie_tecnica_utilizada_p)||chr(13)||chr(10);
end if;

if	(nvl(ie_criterio_horario_w,'X') <> nvl(ie_criterio_horario_p,'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Criterio de horario:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||ie_criterio_horario_w||' - Modificado: '||ie_criterio_horario_p||chr(13)||chr(10);
end if;

if	(nvl(ie_via_acesso_p,'X') <> nvl(ie_via_acesso_w,'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Via acesso: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||obter_valor_dominio(1268, ie_via_acesso_w)||' - Modificada: '||
				obter_valor_dominio(1268, ie_via_acesso_p)||chr(13)||chr(10);							
end if;

if	(nvl(tx_item_via_acesso_w,nvl(tx_item_p,0)) <> nvl(tx_item_w,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'% Taxa: '||chr(13)||chr(10)||	
				chr(9)||'Anterior: '||tx_item_w||' - Modificada: '||nvl(tx_item_via_acesso_w,nvl(tx_item_p,0))||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_setor_atend_p,0) <> nvl(nr_seq_setor_atend_w,0)) then
	select	max(ds_setor_atendimento)
	into	ds_setor_atendimento_w
	from 	pls_setor_atendimento
	where	nr_sequencia = nr_seq_setor_atend_p;
	
	select	max(ds_setor_atendimento)
	into	ds_setor_atendimento_ww
	from 	pls_setor_atendimento
	where	nr_sequencia = nr_seq_setor_atend_w;
	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Setor atend.: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||ds_setor_atendimento_ww||' - Modificado: '||ds_setor_atendimento_w||chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_procedimento_w),'X') <> nvl(to_char(dt_procedimento_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. procedimento: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_procedimento_ww, 'dd/mm/yyyy')||' - Modificada: '||
				to_char(dt_procedimento_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_fim_proc_w, 'hh24:mi:ss'),'X') <> nvl(to_char(dt_fim_proc_ww, 'hh24:mi:ss'),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Hora fim: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_fim_proc_ww, 'hh24:mi:ss')||' - Modificada: '||to_char(dt_fim_proc_w, 'hh24:mi:ss')||
				chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_inicio_proc_w, 'hh24:mi:ss'),'X') <> nvl(to_char(dt_inicio_proc_ww, 'hh24:mi:ss'),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Hora inicio: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_inicio_proc_ww, 'hh24:mi:ss')||' - Modificada: '||
				to_char(dt_inicio_proc_w, 'hh24:mi:ss')||chr(13)||chr(10);
end if;

if	(nvl(ie_cobranca_prevista_p,'X') <> nvl(ie_cobranca_prevista_w,'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Cobranca prevista: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||nvl(ie_cobranca_prevista_w,'N')||' - Modificada: '||ie_cobranca_prevista_p||chr(13)||chr(10);
end if;

if	(nvl(ie_autogerado_p,'X') <> nvl(ie_autogerado_w,'X')) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Autogerado: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||nvl(ie_autogerado_w,'N')||' - Modificado: '||nvl(ie_autogerado_p,'N')||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_setor_atend_w,0) <> nvl(nr_seq_setor_atend_p,0)) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Setor atendimento: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||pls_obter_dados_setor_atend(nr_seq_setor_atend_w,'DS')||' - Modificada: '||
				pls_obter_dados_setor_atend(nr_seq_setor_atend_p,'DS')||' - '||pls_obter_dados_setor_atend(nr_seq_setor_atend_p,'DS')||
				chr(13)||chr(10);
end if;

if	(nvl(cd_procedimento_w, 0) <> nvl(cd_procedimento_p, 0)) or
	(nvl(ie_origem_proced_w, 0) <> nvl(ie_origem_proced_p, 0)) then	
	ds_observacao_w :=	ds_observacao_w || chr(13) || chr(10) ||
				'Alterado codigo do item: ' || chr(13) || chr(10) ||
				'Codigo: ' || chr(13) || chr(10) ||
				chr(9) || 'Anterior: ' || cd_procedimento_w || ' - Modificado: ' || cd_procedimento_p || chr(13) || chr(10) ||
				'Origem: ' || chr(13) || chr(10) ||
				chr(9) || 'Anterior: ' || ie_origem_proced_w || ' - Modificado: '  || ie_origem_proced_p || chr(13) || chr(10) ||
				'Procedimento: ' ||chr(13) || chr(10) ||
				chr(9) || 'Anterior: ' || pls_obter_desc_procedimento(cd_procedimento_w, ie_origem_proced_w) || ' - Modificado: ' ||
				pls_obter_desc_procedimento(cd_procedimento_p, ie_origem_proced_p) || ' - ' || pls_obter_desc_procedimento(cd_procedimento_p, ie_origem_proced_p) ||
				chr(13) || chr(10);
end if;

if	(nvl(cd_dente_w,0) <> nvl(cd_dente_p,0)) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Codigo dente: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||cd_dente_w||' - Modificado: '|| cd_dente_p ||
				chr(13)||chr(10);
end if;

if	(nvl(cd_face_dente_w,0) <> nvl(cd_face_dente_p,0)) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Codigo face dente: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||cd_face_dente_w||' - Modificado: '|| cd_face_dente_p ||
				chr(13)||chr(10);
end if;

if	(nvl(cd_regiao_boca_w,0) <> nvl(cd_regiao_boca_p,0)) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Codigo regiao boca: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||cd_regiao_boca_w||' - Modificado: '|| cd_regiao_boca_p ||
				chr(13)||chr(10);
end if;


if	(nvl(ds_observacao_w,'X') <> 'X') then
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_p, 15,
				 nr_sequencia_p, 'P', null,
				 null, 'Modificado pelo auditor ' || obter_nome_usuario(nm_usuario_p) || '.' || chr(13) || chr(10) ||
				'Item ' || pls_obter_desc_procedimento(cd_procedimento_w, ie_origem_proced_w) || '.' || chr(13) || chr(10) ||
				 ds_observacao_w, nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);

else

	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_p, 15,
				 nr_sequencia_p, 'P', null,
				 null, 'Modificado pelo auditor ' || obter_nome_usuario(nm_usuario_p) || '.' || chr(13) || chr(10) ||
				'Item ' || pls_obter_desc_procedimento(cd_procedimento_w, ie_origem_proced_w) || '.' || chr(13) || chr(10) ||
				 'Modificar informacoes do item executada, porem, Nenhuma informacao foi alterada', 
				 nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);	

end if;

--pls_consistir_analise(nr_seq_analise_p, nr_seq_grupo_p, cd_estabelecimento_p, nm_usuario_p);

commit;

end pls_alterar_proc_analise;
/
