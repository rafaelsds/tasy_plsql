create or replace procedure
com_insert_func_impl	(
				nr_seq_mod_impl_p	in	number,
				cd_funcao_p		in	varchar2,
				nm_usuario_p		in	varchar2
			) is

tam_lista_w			number(10);
ie_pos_virgula_w		number(3)	:= 0;
pr_funcao_modulo_w		Number(17,2);

begin
	if	(cd_funcao_p is not null) then
		begin

			insert	into com_cliente_fun_impl(
				nr_sequencia,
				nr_seq_mod_impl,
				dt_atualizacao,
				nm_usuario,
				cd_funcao,
				dt_inicio_util)
			values( com_cliente_fun_impl_seq.nextval,
				nr_seq_mod_impl_p,
				sysdate,
				nm_usuario_p,
				cd_funcao_p,
				sysdate);

			select	nvl(sum(pr_funcao_modulo),0)
			into	pr_funcao_modulo_w
			from	funcao a,
				com_cliente_fun_impl b
			where	a.cd_funcao		= b.cd_funcao
			and	b.nr_seq_mod_impl	= nr_seq_mod_impl_p;

			update	com_cliente_mod_impl
			set	pr_utilizacao	= pr_funcao_modulo_w,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_mod_impl_p;

			commit;
		end;
	end if;
end com_insert_func_impl;
/
