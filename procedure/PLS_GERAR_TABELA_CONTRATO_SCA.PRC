create or replace
procedure pls_gerar_tabela_contrato_sca
			(	nr_seq_sca_regra_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

dt_contrato_w			date;
dt_inicio_vigencia_sca_w	date;
nr_seq_tabela_w			number(10);
qt_idade_inicial_w		number(5);
qt_idade_final_w		number(5);
vl_preco_atual_w		number(15,2);
tx_acrescimo_w			number(7,4);
qt_registros_w			number(10);
ie_tabela_contrato_w		varchar2(1);
nm_estipulante_w		varchar2(80);
ie_tipo_contratacao_w		varchar2(3);
vl_preco_nao_subsidiado_w	Number(15,2);
ie_preco_w			Varchar2(2);
nr_seq_contrato_tabela_w	Number(10);
nr_contrato_w			Number(10);
vl_minimo_w			Number(15,2);
ie_grau_titularidade_w		varchar2(2);
qt_vidas_inicial_w		number(10);
qt_vidas_final_w		number(10);
nr_seq_contrato_sca_w		number(10);
nr_seq_plano_sca_w		number(10);
nr_seq_tabela_sca_contrato_w	number(10);

vl_preco_nao_subsid_atual_w	number(15,2);

cursor c01 is
	select	qt_idade_inicial,
		qt_idade_final,
		vl_preco_atual,
		tx_acrescimo,
		vl_preco_nao_subsidiado,
		vl_minimo,
		ie_grau_titularidade,
		qt_vidas_inicial,
		qt_vidas_final,
		vl_preco_nao_subsid_atual
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_sca_contrato_w
	order by qt_idade_inicial;

begin

select	nr_seq_contrato,
	nr_seq_plano,
	nr_seq_tabela,
	dt_inicio_vigencia
into	nr_seq_contrato_sca_w,
	nr_seq_plano_sca_w,
	nr_seq_tabela_sca_contrato_w,
	dt_inicio_vigencia_sca_w
from	pls_sca_regra_contrato
where	nr_sequencia	= nr_seq_sca_regra_p;

select	nr_contrato,
	nvl(dt_contrato,sysdate),
	substr(obter_nome_pf_pj(cd_pf_estipulante,cd_cgc_estipulante),1,80)
into	nr_contrato_w,
	dt_contrato_w,
	nm_estipulante_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_sca_w;

select	nvl(max(nr_contrato),0)
into	nr_seq_contrato_tabela_w
from	pls_tabela_preco
where	nr_sequencia	= nr_seq_tabela_sca_contrato_w;

if	(nr_seq_plano_sca_w is not null) and
	(nr_seq_tabela_sca_contrato_w is not null) and
	(nr_seq_contrato_tabela_w = 0) then
	
	select	pls_tabela_preco_seq.nextval
	into	nr_seq_tabela_w
	from	dual;
	
	/*aaschlote 26/10/2010 - ao inserir um tabela de pre�o, n�o deve ser encerrada a vigencia no dia atual*/
	insert into pls_tabela_preco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nm_tabela,
		dt_inicio_vigencia,
		nr_seq_plano,
		dt_liberacao,
		ie_tabela_base,
		nr_contrato,
		nr_seq_tabela_origem,
		ie_preco_vidas_contrato,
		ie_calculo_vidas,
		nr_seq_faixa_etaria)
	select	nr_seq_tabela_w,
		sysdate,
		nm_usuario_p,
		'Tabela para o contrato ' || to_char(nr_contrato_w) || ' - ' || nm_estipulante_w,
		nvl(dt_inicio_vigencia_sca_w,dt_contrato_w),
		nr_seq_plano_sca_w,
		sysdate,
		'N',
		nr_seq_contrato_sca_w,
		nr_sequencia,
		ie_preco_vidas_contrato,
		ie_calculo_vidas,
		nr_seq_faixa_etaria
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_sca_contrato_w;
	
	open c01;
	loop
	fetch c01 into
		qt_idade_inicial_w,
		qt_idade_final_w,
		vl_preco_atual_w,
		tx_acrescimo_w,
		vl_preco_nao_subsidiado_w,
		vl_minimo_w,
		ie_grau_titularidade_w,
		qt_vidas_inicial_w,
		qt_vidas_final_w,
		vl_preco_nao_subsid_atual_w;
	exit when c01%notfound;			
		insert into pls_plano_preco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_tabela,
			qt_idade_inicial,
			qt_idade_final,
			vl_preco_inicial,
			vl_preco_atual,
			tx_acrescimo,
			vl_preco_nao_subsidiado,
			vl_minimo,
			ie_grau_titularidade,
			qt_vidas_inicial,
			qt_vidas_final,
			vl_preco_nao_subsid_atual)
		values	(pls_plano_preco_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_seq_tabela_w,
			qt_idade_inicial_w,
			qt_idade_final_w,
			vl_preco_atual_w,
			vl_preco_atual_w,
			tx_acrescimo_w,
			vl_preco_nao_subsidiado_w,
			vl_minimo_w,
			ie_grau_titularidade_w,
			qt_vidas_inicial_w,
			qt_vidas_final_w,
			vl_preco_nao_subsid_atual_w);
	end loop;
	close c01;
	
	update	pls_sca_regra_contrato
	set	nr_seq_tabela_origem = nr_seq_tabela_sca_contrato_w,
		nr_seq_tabela		 = nr_seq_tabela_w,
		nm_usuario		 = nm_usuario_p,
		dt_atualizacao		 = sysdate
	where	nr_sequencia		 = nr_seq_sca_regra_p;
end if;

commit;

end pls_gerar_tabela_contrato_sca;
/