create or replace 
procedure gerar_termino_sne(
	nm_usuario_p			varchar2,
	nr_atendimento_p		number,
	dt_alteracao_p			date,
	ie_consistir_dt_p		varchar2 default 'N',
	ie_horario_invalido_p	out varchar2,
  nr_prescr_p		prescr_medica.nr_prescricao%type default null,
  nr_seq_sol_p  prescr_material.nr_sequencia%type default null)
is

	nr_sequencia_w				prescr_material.nr_sequencia%type;
	nr_prescricao_w				prescr_material.nr_prescricao%type;
	dt_horario_w				prescr_mat_hor.dt_horario%type;
	nr_prescricao_finalizada_w	prescr_material.nr_prescricao%type;
	ds_mensagem_w				varchar2(255 char);
	ie_horario_invalido_w		varchar2(1 char);
	dt_evento_ref_w				date;
	dt_inicio_w					date;
	qt_volume_parcial_w			number;
	qt_volume_total_w			number;
	qt_vol_infundido_w			number;

cursor c01 is
	select	x.nr_sequencia,
			a.nr_prescricao,
			c.dt_horario,
			obter_volume_parcial_sol(x.nr_prescricao,x.nr_sequencia,2),
			obter_volume_total_sne(x.cd_intervalo,obter_conversao_ml(x.cd_material,x.qt_dose,x.cd_unidade_medida_dose),x.nr_ocorrencia)
	from	prescr_material x,
			prescr_medica a,
			prescr_mat_hor c
	where	x.nr_prescricao = a.nr_prescricao
	and		c.nr_prescricao = a.nr_prescricao
  and   ((nr_prescr_p is null or nr_seq_sol_p is null) or (a.nr_prescricao <> nr_prescr_p 
      or (a.nr_prescricao = nr_prescr_p  and x.nr_sequencia <> nr_seq_sol_p)))
	and		a.nr_atendimento = nr_atendimento_p
	and		a.dt_validade_prescr between dt_inicio_w and sysdate+2
	and		((obter_se_prescr_lib_adep(nvl(a.dt_liberacao_medico,x.dt_lib_material), nvl(a.dt_liberacao,x.dt_lib_material), nvl(a.dt_liberacao_farmacia,x.dt_lib_material), 'F', a.ie_lib_farm) = 'S') or
			((nvl(a.ie_prescr_nutricao, 'N') = 'S')))
	and		x.ie_agrupador = 8
	and		substr(obter_status_solucao_prescr(2,a.nr_prescricao,x.nr_sequencia),1,3) in ('I','R')
	and		x.dt_status between dt_inicio_w and sysdate
	and		nvl(a.ie_recem_nato,'N') = 'N'
	and		x.ie_status <> 'T'
	and		((x.dt_suspensao is null) and (nvl(x.ie_horario_susp,'N') <> 'S'))
	and		a.dt_inicio_prescr between dt_inicio_w and sysdate
	order by a.nr_prescricao,
		c.dt_horario desc;

begin
	nr_prescricao_finalizada_w := 0;
	ie_horario_invalido_w := 'N';
	ie_horario_invalido_p := 'N';

	dt_inicio_w	:= pkg_date_utils.start_of(sysdate-5, 'DAY');

	open C01;
	loop
	fetch C01 into
		nr_sequencia_w,
		nr_prescricao_w,
		dt_horario_w,
		qt_volume_parcial_w,
		qt_volume_total_w;
	exit when C01%notfound;
		begin
		if (ie_consistir_dt_p = 'S') then
			select	max(dt_atualizacao)
			into	dt_evento_ref_w
			from	prescr_solucao_evento
			where	nr_prescricao	= nr_prescricao_w
			and		nr_seq_material	= nr_sequencia_w
			and		ie_tipo_solucao	= 2
			and		ie_alteracao	in (1,3,35)
			and		ie_evento_valido	= 'S';

			if (dt_evento_ref_w is not null and dt_evento_ref_w >= dt_alteracao_p) then
				ie_horario_invalido_w := 'S';
				ie_horario_invalido_p := 'S';
			else 
				ie_horario_invalido_w := 'N';
			end if;
		end if;

		if(	((ie_consistir_dt_p = 'N' or ie_consistir_dt_p is null) 
			or (ie_consistir_dt_p = 'S' and ie_horario_invalido_w = 'N'))
			and nr_prescricao_finalizada_w <> nr_prescricao_w) then

			qt_vol_infundido_w := qt_volume_total_w - qt_volume_parcial_w;
			Gerar_alteracao_solucao_prescr(
				cd_estabelecimento_p	=> obter_estabelecimento_ativo,
				ds_observacao_p			=> null,
				dt_alteracao_p			=> (nvl(dt_alteracao_p,sysdate) - 1/86400),
				dt_horario_p			=> dt_horario_w,
				ie_tipo_solucao_p		=> 2,
				ie_alteracao_p			=> 4,
				ie_forma_infusao_p		=> null,
				ie_tipo_dosagem_p		=> 'mlh',
				ie_bomba_infusao_p		=> null,
				nm_usuario_p			=> nm_usuario_p,
				nr_atendimento_p		=> nr_atendimento_p,
				nr_prescricao_p			=> nr_prescricao_w,
				nr_seq_solucao_p		=> nr_sequencia_w,
				nr_seq_dispositivo_p	=> null,
				nr_seq_motivo_p			=> null,
				nr_etapa_evento_p		=> null,
				qt_vol_fase_p			=> qt_vol_infundido_w,
				qt_vel_infusao_p		=> 0,
				qt_vol_infundido_p		=> qt_vol_infundido_w,
				qt_vol_desprezado_p		=> 0,
				qt_vol_parcial_p		=> 0,
				ds_msg_p				=> ds_mensagem_w);

			nr_prescricao_finalizada_w := nr_prescricao_w;
		end if;
		end;
	end loop;
	close C01;

	commit;
end gerar_termino_sne;
/
