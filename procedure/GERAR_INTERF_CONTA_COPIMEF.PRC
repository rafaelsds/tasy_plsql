create or replace 
procedure gerar_interf_conta_copimef
		(cd_estabelecimento_p		number,
		cd_cgc_prestador_p		varchar2,
		dt_inicial_p			date,
		dt_final_p			date,
		nr_seq_protocolo_p		number,
		cd_convenio_p			number,
		nm_usuario_p			varchar2) is

nr_seq_protocolo_w			number(15)		:= 0;
nr_seq_envio_convenio_w			number(15)		:= 0;
dt_mesano_referencia_w			date;
dt_vencimento_w				date;
cd_convenio_w				number(15)		:= 0;
cd_cgc_hospital_w			varchar2(100);
nm_hospital_w				varchar2(100);
ds_convenio_w				varchar2(255);
cd_cgc_convenio_w			varchar2(100);
cd_interno_w				varchar2(100);
cd_regional_w				varchar2(100);
nr_sequencia_w				number(15)		:= 0;
nr_interno_conta_w			number(15)		:= 0;
nr_atendimento_w			number(15)		:= 0;
ie_tipo_protocolo_w			number(15)		:= 0;
ie_tipo_atendimento_w			number(15)		:= 0;
ie_excluir_hon_med_w			varchar2(100);
ie_excluir_hon_terc_w			varchar2(100);
QT_ITENS_CONTA_W			NUMBER(15)		:= 0;
nr_seq_registro_w			Number(15)		:= 0;
vl_material_w				Number(15,2)		:= 0;
nr_protocolo_w				Varchar2(40);
dt_inicio_protocolo_w			Date;
dt_fim_protocolo_w			Date;
nr_primeira_guia_w			Varchar2(100);
nr_ultima_guia_w			Varchar2(100);
qt_guia_w				number(15) := 0;
ie_agrup_item_interf_w			Varchar2(100);
w_tot_trailler_w			number(15) := 0;
qt_documento_w				Number(15,0);
vl_procedimento_w			Number(15,2);
nm_usuario_w				Varchar2(100);
nr_nota_inicial_w			Number(15);
msg_w					varchar2(255);

cursor c01 is
select	/*+ index(a conta_paciente a) */ distinct a.nr_seq_protocolo
from	protocolo_convenio c,
	procedimento_paciente b,
	conta_paciente a
where	a.dt_mesano_referencia is not null 
and	a.ie_status_acerto = 2
and	a.nr_seq_protocolo	= c.nr_seq_protocolo
and	c.ie_status_protocolo	= 2
and	a.dt_mesano_referencia		between dt_inicial_p and dt_final_p
and	a.cd_estabelecimento		= cd_estabelecimento_p
and	b.nr_interno_conta		= a.nr_interno_conta
and	((nvl(nr_seq_protocolo_p, 0)	= 0) or (a.nr_seq_protocolo = nr_seq_protocolo_p))
and	((nvl(cd_convenio_p, 0)	= 0) or (c.cd_convenio = cd_convenio_p))
and	b.cd_motivo_exc_conta		is null
and	b.cd_cgc_prestador		= cd_cgc_prestador_p;

CURSOR C02 IS
select	a.nr_interno_conta,
	a.nr_atendimento,
	b.ie_tipo_atendimento
from	conta_paciente a,
	atendimento_paciente b
where	a.nr_atendimento		= b.nr_atendimento
and 	a.nr_seq_protocolo	 	= nr_seq_protocolo_w
and	a.ie_cancelamento 		is null
order	by 1;


begin

open c01;
loop
fetch c01 into
	nr_seq_protocolo_w;
exit when c01%notfound;

	/* Limpar tabelas transit�rias */
	delete	from w_interf_conta_header
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	delete	from w_interf_conta_cab
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	delete	from w_interf_conta_autor
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	delete	from w_interf_conta_item
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	delete	from w_interf_conta_item_ipe
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	delete	from w_interf_conta_total
	where	nr_seq_protocolo	= nr_seq_protocolo_w;
	delete	from w_interf_conta_trailler
	where	nr_seq_protocolo	= nr_seq_protocolo_w;

	commit;

	delete from w_interf_conta_cab
	where nr_interno_conta in(	select nr_interno_conta 
					from conta_paciente
					where nr_seq_protocolo = nr_seq_protocolo_w);
	delete from w_interf_conta_autor
	where nr_interno_conta in(	select nr_interno_conta 
					from conta_paciente
					where nr_seq_protocolo = nr_seq_protocolo_w);
	delete from w_interf_conta_item
	where nr_interno_conta in(	select nr_interno_conta 
					from conta_paciente
					where nr_seq_protocolo = nr_seq_protocolo_w);
	delete from w_interf_conta_total
	where nr_interno_conta in(	select nr_interno_conta 
					from conta_paciente
					where nr_seq_protocolo = nr_seq_protocolo_w);
	delete from w_interf_conta_item_ipe
	where nr_interno_conta in(	select nr_interno_conta 
					from conta_paciente
					where nr_seq_protocolo = nr_seq_protocolo_w);
	commit;

end loop;
close c01;

open c01;
loop
fetch c01 into
	nr_seq_protocolo_w;
exit when c01%notfound;
	begin
	
	/* Buscar sequence */
	select	w_interf_conta_header_seq.nextval
	into	nr_sequencia_w
	from	dual;

	/* Dados do protocolo */
	select	a.nr_seq_protocolo,
	 	mod(a.nr_seq_envio_convenio, nvl(nr_multiplo_envio,0)),
	 	a.dt_mesano_referencia,
	 	a.dt_vencimento,
	 	a.cd_convenio,
	 	a.ie_tipo_protocolo,
	 	a.nr_protocolo,
	 	a.dt_periodo_inicial,
	 	a.dt_periodo_final,
	 	nvl(somente_numero(nr_seq_doc_convenio),0),
		nvl(a.nm_usuario_envio,a.nm_usuario)
	into	nr_seq_protocolo_w,
	 	nr_seq_envio_convenio_w,
	 	dt_mesano_referencia_w,
	 	dt_vencimento_w,
	 	cd_convenio_w,
	 	ie_tipo_protocolo_w,
	 	nr_protocolo_w,
	 	dt_inicio_protocolo_w,
	 	dt_fim_protocolo_w,
		nr_nota_inicial_w,
		nm_usuario_w
	from	convenio b,
		protocolo_convenio a
	where	a.cd_convenio		= b.cd_convenio
	and	a.nr_seq_protocolo	= nr_seq_protocolo_w;


	/* Dados do estabelecimento */
	select		a.cd_cgc,
			Elimina_Acentuacao(b.ds_razao_social)
	into		cd_cgc_hospital_w,
			nm_hospital_w
	from		estabelecimento a,
			pessoa_juridica b
	where		a.cd_estabelecimento		= cd_estabelecimento_p
	and		b.cd_cgc				= a.cd_cgc;

	/* Dados do convenio */ 
	select		Elimina_Acentuacao(ds_convenio),
			cd_cgc,
			Obter_Valor_Conv_Estab(cd_convenio, cd_estabelecimento_P, 'CD_INTERNO'),
			Obter_Valor_Conv_Estab(cd_convenio, cd_estabelecimento_P, 'CD_REGIONAL'),
			ie_agrup_item_interf
	into		ds_convenio_w,
			cd_cgc_convenio_w,
			cd_interno_w,
			cd_regional_w,
			ie_agrup_item_interf_w
	from		convenio
	where		cd_convenio				= cd_convenio_w;


	/* Chamada especial do conv�nio IPE */
	if	(ie_agrup_item_interf_w = 'E') then

		delete from w_interf_conta_item_ipe
		where nr_seq_protocolo	= nr_seq_protocolo_w;
		commit;

	end if;


	/* Gravar conta_header */ 
	msg_w		:= 'Insert no w_interf_conta_header';
	insert into w_interf_conta_header(
	 	nr_sequencia,
	 	cd_tipo_registro,
	 	nr_seq_registro,
	 	nr_seq_interface,
	 	cd_interno,
	 	cd_regional,
	 	nm_convenio,
	 	cd_cgc_convenio,
	 	nm_hospital,
	 	cd_cgc_hospital,
	 	nr_remessa,
	 	dt_remessa,
	 	dt_vencimento,
	 	nr_seq_protocolo,
	 	cd_convenio,
	 	ie_tipo_protocolo,
	 	nr_protocolo,
	 	dt_inicio_protocolo,
		dt_fim_protocolo)
	values(nr_sequencia_w,
		0,
		1,
		1,
		cd_interno_w,
		cd_regional_w,
		ds_convenio_w,
		cd_cgc_convenio_w,
		nm_hospital_w,
		cd_cgc_hospital_w,
		nr_seq_envio_convenio_w,
		dt_mesano_referencia_w,
		dt_vencimento_w,
		nr_seq_protocolo_w,
		cd_convenio_w,
		ie_tipo_protocolo_w,
		nr_protocolo_w,
		dt_inicio_protocolo_w,
		dt_fim_protocolo_w);


	commit;

	/* selecionar as contas do protocolo */
	open c02;
	loop
	fetch c02 into
		nr_interno_conta_w,
		nr_atendimento_w,
		ie_tipo_atendimento_w;
	exit when c02%notfound;
     		begin
		begin
		/* selecionar regra de exclusao de honor�rios */
		/* IE_EXCLUIR_HON_MED exclui regra honor�rio direto do m�dico 'M' */
		/* IE_EXCLUIR_HON_TERC exclui regra honor�rio terceiros 'COOP' */
		select	nvl(ie_excluir_hon_med,'N'),
			nvl(ie_excluir_hon_terc,'N')
		into	ie_excluir_hon_med_w,
			ie_excluir_hon_terc_w
		from	param_interface
		where	cd_convenio	= cd_convenio_w
		and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)	= ie_tipo_atendimento_w
		and	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p;
		exception
			when others then
			begin
		 	ie_excluir_hon_med_w	:= 'N';
			ie_excluir_hon_terc_w	:= 'N';
			end;
		end;
		
		Gerar_Interf_Conta_Cab(	nr_seq_protocolo_w,
						nr_seq_envio_convenio_w,
						cd_convenio_w,
						nr_atendimento_w,
						nr_interno_conta_w,
						cd_cgc_hospital_w,
						cd_cgc_convenio_w,
						cd_interno_w,
						nr_sequencia_w);
		nr_seq_registro_w		:= nr_seq_registro_w + 1;
		update w_interf_conta_cab
		set nr_seq_registro	= nr_seq_registro_w
		where nr_sequencia	= nr_sequencia_w;

		Gerar_Interf_Conta_Aut( nr_seq_protocolo_w,
						nr_seq_envio_convenio_w,
						cd_convenio_w,
						nr_atendimento_w,
						nr_interno_conta_w,
						cd_cgc_hospital_w,
						cd_cgc_convenio_w,
						cd_interno_w);

		Gerar_Interf_Conta_Item(nr_seq_protocolo_w,
						nr_seq_envio_convenio_w,
						cd_convenio_w,
						nr_atendimento_w,
						nr_interno_conta_w,
						cd_cgc_hospital_w,
						cd_cgc_convenio_w,
						cd_interno_w,
		 	 			ie_excluir_hon_med_w,
			 			ie_excluir_hon_terc_w);

		/* Chamada especial do conv�nio IPE - Internados */
		if	(ie_agrup_item_interf_w = 'E') and
			((nr_nota_inicial_w = 0) 	or
			(ie_tipo_protocolo_w = 1))	then
			Gerar_Interf_Conta_Item_IPE(nr_seq_protocolo_w,
						nr_interno_conta_w);
		end if;


		Gerar_Interf_Conta_Total(nr_seq_protocolo_w,
						nr_seq_envio_convenio_w,
						cd_convenio_w,
						nr_atendimento_w,
						nr_interno_conta_w,
						cd_cgc_hospital_w,
						cd_cgc_convenio_w,
						cd_interno_w,
		 	 			ie_excluir_hon_med_w,
			 			ie_excluir_hon_terc_w);


		/* Excluir contas sem item - excluidos pela regra honor�rios */ 
		begin
		qt_itens_conta_w	:= 0;
		Select	count(*)
		into		qt_itens_conta_w
		from		w_interf_conta_item
		where		nr_interno_conta	= nr_interno_conta_w;
		exception
				when others then
				qt_itens_conta_w := 0;
		end;
		if	(qt_itens_conta_w = 0) then
			begin
			delete from w_interf_conta_cab
			where nr_interno_conta	= nr_interno_conta_w;
			delete from w_interf_conta_autor
			where nr_interno_conta	= nr_interno_conta_w;
			delete from w_interf_conta_item
			where nr_interno_conta	= nr_interno_conta_w;
			delete from w_interf_conta_total
			where nr_interno_conta	= nr_interno_conta_w;
			end;
		end if;
		commit;
		END;
	END LOOP;
	close C02;
	commit;

	/* Chamada especial do conv�nio IPE - Exames */
	if	(ie_agrup_item_interf_w = 'E') and
		(nr_nota_inicial_w > 0) 	 and
		(ie_tipo_atendimento_w	= 7)	 then
		Gerar_Interf_Item_Exames_IPE(nr_seq_protocolo_w);
	end if;
	/* Chamada especial do conv�nio IPE - PA */
	if	(ie_agrup_item_interf_w = 'E') 	and
		(nr_nota_inicial_w > 0) 	 	and
		(ie_tipo_atendimento_w in(3,8))	then
		Gerar_Interf_Item_PA_IPE(nr_seq_protocolo_w);
	end if;
	commit;

	/* Tratar registro tipo TRAILLER */ 
	/* Buscar sequence */
	select w_interf_conta_trailler_seq.nextval
	into	 nr_sequencia_w
	from 	 dual;

	select 	nvl(sum(vl_material),0)
	into 	vl_material_w
	from 	material_atend_paciente b,
		conta_paciente a
	where 	a.nr_interno_conta	= b.nr_interno_conta
	and	a.nr_seq_protocolo	= nr_seq_protocolo_w
	and	DECODE(b.NR_SEQ_PROC_PACOTE,NULL,b.NR_SEQUENCIA,b.NR_SEQ_PROC_PACOTE) = b.NR_SEQUENCIA
	and	a.ie_cancelamento 	is null;

	begin
	select	nvl(a.cd_autorizacao,'0')
	into	nr_primeira_guia_w
	from	w_interf_conta_autor a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_w
	and	a.nr_interno_conta	= 
		(select min(x.nr_interno_conta) from w_interf_conta_autor x
			where x.nr_seq_protocolo = nr_seq_protocolo_w);
	exception
		when others then
		nr_primeira_guia_w := '0';
	end;

	begin
	select	nvl(a.cd_autorizacao,'0')
	into	nr_ultima_guia_w
	from	w_interf_conta_autor a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_w
	and	a.nr_interno_conta	= 
		(select max(x.nr_interno_conta) from w_interf_conta_autor x
		where	x.nr_seq_protocolo = nr_seq_protocolo_w);
	exception
		when others then
		nr_ultima_guia_w := '0';
	end;

	begin
	select	count(*)
	into	qt_guia_w
	from	w_interf_conta_autor a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_w;
	exception
		when others then
		qt_guia_w := 0;
	end;

	select nvl(sum(b.vl_procedimento),0)
	into	vl_procedimento_w
	from	 conta_paciente_honorario_v b
	where	 b.nr_seq_protocolo		= nr_seq_protocolo_w
	and	b.ie_cancelamento 		is null
	and	 DECODE(b.NR_SEQ_PROC_PACOTE,NULL,b.NR_SEQUENCIA,b.NR_SEQ_PROC_PACOTE) = b.NR_SEQUENCIA
	and	 ((ie_excluir_hon_med_w		= 'N') 		 or
		(b.ie_responsavel_credito 	<> 'M')		 or
		(b.ie_responsavel_credito	is null))
	and	 ((ie_excluir_hon_terc_w	= 'N') 		 or
		(b.ie_responsavel_credito 	in ('H','RM','M')) or
		(b.ie_responsavel_credito	is null));

	select count(*)
	into	qt_documento_w
	from	w_interf_conta_cab
	where	nr_seq_protocolo	= nr_seq_protocolo_w;

	begin
	msg_w		:= 'Insert no w_interf_conta_trailler';
	insert into w_interf_conta_trailler(
	 		nr_sequencia,
 			cd_tipo_registro,
 			nr_seq_registro,
	 		nr_seq_interface,
 			nr_remessa,
 			qt_total_conta,
	 		vl_total_conta,
 			nr_seq_protocolo,
 			cd_convenio,
	 		cd_cgc_hospital,
 			cd_cgc_convenio,
 			cd_interno,
	 		nr_primeira_guia,
 			nr_ultima_guia,
 			qt_guia)
	values(
			nr_sequencia_w,
			9,
			1,
			1,
			nr_seq_envio_convenio_w,
			qt_documento_w,
			vl_material_w + vl_procedimento_w,
 			nr_seq_protocolo_w,
 			cd_convenio_w,
	 		cd_cgc_hospital_w,
 			cd_cgc_convenio_w,
 			substr(cd_interno_w,1,14),
	 		nr_primeira_guia_w,
 			nr_ultima_guia_w,
 			qt_guia_w);
	commit;
	exception
		when others then
			/*rai-se_application_error(-20011, msg_w || ' - ' || sqlerrm || chr(13) ||
			 			'cd_convenio_w = ' || cd_convenio_w || chr(13) ||
	 					'cd_cgc_hospital_w = ' || cd_cgc_hospital_w || chr(13) ||
			 			'cd_cgc_convenio_w = ' || cd_cgc_convenio_w || chr(13) ||
 						'cd_interno_w = ' || cd_interno_w || chr(13) ||
				 		'nr_primeira_guia_w = ' || nr_primeira_guia_w || chr(13) ||
 						'nr_ultima_guia_w = ' || nr_ultima_guia_w || chr(13) ||
			 			'qt_guia_w = ' || qt_guia_w);*/
						null;
	end;

	/* Chamada especial do conv�nio IPE - Resumo das contas */
	if	(ie_agrup_item_interf_w = 'E') then
		Atualiza_Resumo_Conta_IPE(nr_seq_protocolo_w,nm_usuario_w);
	end if;

	exception
		when others then 
			--rai-se_application_error(-20011, sqlerrm);
			null;
	end;

end loop;
close c01;

commit;

end gerar_interf_conta_copimef;
/
