create or replace
procedure med_inserir_exames_padroes(	nr_seq_pedido_p	number,
					ds_resultado_p	varchar2,
					nm_usuario_p	varchar2) is

nr_seq_apresent_w	number(10,0);					
ds_lista_w		varchar2(2000);
ds_resultado_w		varchar2(2000);
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);

begin
if	(nr_seq_pedido_p is not null) and
	(ds_resultado_p is not null) then
	begin
	ds_lista_w	:= ds_resultado_p;	
	while	ds_lista_w is not null loop
		begin
		tam_lista_w		:= length(ds_lista_w);
		ie_pos_virgula_w		:= instr(ds_lista_w,',');
		
	
		if	(ie_pos_virgula_w <> 0) then
			
			ds_resultado_w	:= substr(ds_lista_w,1,(ie_pos_virgula_w - 1));
			ds_lista_w	:= substr(ds_lista_w,(ie_pos_virgula_w + 1), tam_lista_w);
		else
			ds_lista_w	:= null;
		end if;
		
		if	(trim(ds_resultado_w) is not null) and
			(trim(ds_resultado_w) <> ',') then
			begin
			select	nvl(d.nr_seq_apresent, 0)
			into	nr_seq_apresent_w
			from	med_grupo_exame   d,
				med_exame_padrao  c
			where	c.nr_seq_grupo = d.nr_sequencia
			and	c.nr_sequencia = ds_resultado_w;
	
			insert into Med_Ped_Exame_Cod(
				nr_sequencia,
				nr_seq_pedido,
				dt_atualizacao,
				nm_usuario,
				nr_seq_exame,
				qt_exame,
				nr_seq_apresent)
			values 	(Med_Ped_Exame_Cod_seq.nextval,
				nr_seq_pedido_p,
				sysdate,
				nm_usuario_p,
				ds_resultado_w,
				1,
				nr_seq_apresent_w);
			end;
		end if;
		end;
	end loop;
	end;	
end if;

commit;

end med_inserir_exames_padroes;
/