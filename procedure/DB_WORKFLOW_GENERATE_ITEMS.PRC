create or replace
procedure db_workflow_generate_items(
			nr_seq_workflow_p		number,
			nr_seq_workflow_template_p	number,
			nm_usuario_p			varchar2) is

nr_seq_item_w		db_workflow_item.nr_sequencia%type;
qt_reg_w		number(10);

cursor c01 is
	select	a.nr_seq_ordem,
		a.nr_sequencia,
		a.nr_seq_doc_template,
		a.ds_prefixo_id,
		a.ie_obrigatorio
	from	db_workflow_template_item a
	where	a.nr_seq_wf_template = nr_seq_workflow_template_p
	order by 1;

begin

select	count(1)
into	qt_reg_w
from	db_workflow_item a
where	nr_seq_workflow = nr_seq_workflow_p
and	ie_status <> 'P';

if	(qt_reg_w = 0) then
	begin
	delete from db_workflow_item
	where nr_seq_workflow = nr_seq_workflow_p;

	for c01_w in c01 loop
		begin
		select	db_workflow_item_seq.nextval
		into	nr_seq_item_w
		from	dual;

		insert into db_workflow_item (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_workflow,
				nr_seq_ordem,
				nr_seq_doc_template,
				ds_prefixo_id,
				ie_obrigatorio,
				dt_liberacao,
				nr_seq_documento,
				ie_status)
			values (nr_seq_item_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_workflow_p,
				c01_w.nr_seq_ordem,
				c01_w.nr_seq_doc_template,
				c01_w.ds_prefixo_id,
				c01_w.ie_obrigatorio,
				null,
				null,
				'P');

		insert into db_workflow_item_predec (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_wf_item,
				nr_seq_doc_template)
			select	db_workflow_item_predec_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_item_w,
				a.nr_seq_doc_template
			from	db_wf_templ_item_predec a
			where	a.nr_seq_item = c01_w.nr_sequencia;

		end;
	end loop;

	commit;
	end;
end if;

end db_workflow_generate_items;
/