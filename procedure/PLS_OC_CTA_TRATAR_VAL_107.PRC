create or replace	
procedure pls_oc_cta_tratar_val_107 (	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

ds_tipo_senha_w		varchar2(20);
ds_senha_dif_w		varchar2(20);
cd_senha_ref_w		pls_conta.cd_senha%type;
cd_senha_dif_w		pls_conta.cd_senha%type;
ds_observacao_w		pls_selecao_ocor_cta.ds_observacao%type;
cd_senha_valida_w	pls_conta.cd_senha%type;
nr_idx_w		pls_integer := 0; 
qt_regra_nsenha_w	pls_integer;
dados_tb_selecao_w	pls_tipos_ocor_pck.dados_table_selecao_ocor;
cd_senha_ref_guia_w	pls_guia_plano.cd_senha%type;
dt_valdiade_ref_w	pls_guia_plano.dt_validade_senha%type;
dt_base_vel_w		date;
nr_trans_origem_w	ptu_resposta_autorizacao.nr_seq_origem%type;

Cursor C01 (nr_seq_oc_cta_comb_pc	dados_regra_p.nr_sequencia%type) is
	select	nvl(ie_tipo_senha,'S') ie_tipo_senha,
		nvl(ie_senha_diferente,'N') ie_senha_diferente,
		nvl(qt_min_caracteres,0) qt_min_caracteres,
		ds_carac_valido,
		nr_seq_regra_nsenha,
		nvl(ie_senha_autorizada,'N') ie_senha_autorizada,
		nvl(ie_validade_expirada,'N') ie_validade_expirada,
		nvl(ie_data_base_val, 'R') ie_data_base_val,
		nvl(ie_comparar_origem,'N') ie_comparar_origem
	from	pls_oc_cta_val_nsenha
	where	nr_seq_oc_cta_comb = nr_seq_oc_cta_comb_pc;

Cursor C02 (nr_id_transacao_pc		pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is
	select	cta.cd_senha,
		cta.cd_senha_externa,
		cta.cd_senha_imp,
		sel.nr_sequencia nr_seq_selecao,
		(select	max(x.cd_senha)
		 from	pls_guia_plano x
		 where	x.nr_sequencia = cta.nr_seq_guia) cd_senha_guia,
		(select	max(x.cd_senha_externa)
		 from	pls_guia_plano x
		 where	x.nr_sequencia = cta.nr_seq_guia) cd_senha_ext_guia, 
		cta.nr_seq_guia,
		(select	max(x.dt_validade_senha)
		 from	pls_guia_plano x
		 where	x.nr_sequencia = cta.nr_seq_guia) dt_validade,
		(select	max(x.dt_valid_senha_ext)
		 from	pls_guia_plano x
		 where	x.nr_sequencia = cta.nr_seq_guia) dt_validade_ext,
		(select	trunc(max(x.dt_recebimento))
		 from	pls_protocolo_conta x
		 where	x.nr_sequencia = cta.nr_seq_protocolo) dt_recebimento,
		fim_dia(cta.dt_atendimento_referencia) dt_atendimento,
		(select max(nr_seq_origem)
		 from ptu_resposta_autorizacao ptu,
			pls_guia_plano x
		 where ptu.nr_seq_guia = x.nr_sequencia
		 and   x.nr_sequencia = cta.nr_seq_guia) nr_trans_origem,
		cta.nr_seq_segurado
	from	pls_selecao_ocor_cta sel,
		pls_conta cta
	where	cta.nr_sequencia	= sel.nr_seq_conta
	and	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S';
	
begin

-- Somente entra na validacao se tiver algo cadastrado na regra
if	(dados_regra_p.nr_sequencia is not null) then
	
	for r_C01_w in C01 (dados_regra_p.nr_sequencia) loop
		
		-- Somente abre o cursor das contas se tiver algum tipo de senha para validar
		if	(r_C01_w.ie_tipo_senha is not null) then
		
			pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
			pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
		
			for r_C02_w in C02 (nr_id_transacao_p) loop
				
				-- Limpa as variaveis
				ds_tipo_senha_w		:= null;
				ds_senha_dif_w 		:= null;
				cd_senha_ref_w 		:= null;
				cd_senha_dif_w 		:= null;
				ds_observacao_w 	:= null;
				cd_senha_valida_w	:= null;
				cd_senha_ref_guia_w	:= null;
				dt_valdiade_ref_w	:= null;
				qt_regra_nsenha_w	:= 0;
				
				--Verifica a data base conforme parametro ie_data_base_val
				dt_base_vel_w:= r_C02_w.dt_recebimento;
				if	(r_C01_w.ie_data_base_val = 'A') then
					begin
					dt_base_vel_w:= r_C02_w.dt_atendimento;
					end;
				elsif	(r_C01_w.ie_data_base_val = 'P') then
					begin
					select	min(dt_procedimento)
					into	dt_base_vel_w
					from	pls_conta_proc 	proc,
						pls_conta	cta
					where	proc.nr_seq_conta = cta.nr_sequencia
					and	cta.nr_seq_guia	= r_c02_w.nr_seq_guia;
					end;
				end if;
				
				-- Verifica qual sera o campo de senha referencia
				if	(r_C01_w.ie_tipo_senha = 'S') then
					
					ds_tipo_senha_w := 'Senha';
					cd_senha_ref_guia_w := r_C02_w.cd_senha_guia;
					dt_valdiade_ref_w := fim_dia(r_C02_w.dt_validade);
					
					if	(dados_regra_p.ie_evento = 'IMP') then
						cd_senha_ref_w := r_C02_w.cd_senha_imp;
					else
						cd_senha_ref_w := r_C02_w.cd_senha;
					end if;
				else
					ds_tipo_senha_w := 'Senha externa';
					cd_senha_ref_w := r_C02_w.cd_senha_externa;
					cd_senha_ref_guia_w := r_C02_w.cd_senha_ext_guia;
					dt_valdiade_ref_w := fim_dia(r_C02_w.dt_validade_ext);
				end if;
				
				-- Somente ira realizar as validacoes se tiver senha informada na conta
				if	(cd_senha_ref_w is not null) then
					
					-- Busca a senha a diferenciar, caso esteja assim parametrizado
					if	(r_C01_w.ie_senha_diferente <> 'N') then
						if	(r_C01_w.ie_senha_diferente = 'S') then
							
							ds_senha_dif_w := 'Senha';
							
							if	(dados_regra_p.ie_evento = 'IMP') then
								cd_senha_dif_w := r_C02_w.cd_senha_imp;
							else
								cd_senha_dif_w := r_C02_w.cd_senha;
							end if;
						else
							ds_senha_dif_w := 'Senha externa';
							cd_senha_dif_w := r_C02_w.cd_senha_externa;
						end if;
						
						-- Se estiver diferente, grava a observacao						
						if	(cd_senha_ref_w <> cd_senha_dif_w) then
							ds_observacao_w := ds_tipo_senha_w || ' diferente da senha ' || ds_senha_dif_w;
						end if;
					end if;
					
					-- Somente ira continuar verificando se a primeira validacao nao deve erar ocorrencia
					if	(ds_observacao_w is null) then	
					
						-- Caso a quantidade minina de caracteres esteja informada e o tamanho da senha seja menor
						-- que a quantidade minima, grava a observacao
						if	(r_C01_w.qt_min_caracteres > 0) and
							(length(cd_senha_ref_w) < r_C01_w.qt_min_caracteres) then
							ds_observacao_w := 	'A ' || ds_tipo_senha_w || ' nao possui quantidade minima de caracteres valida. ' || pls_util_pck.enter_w ||
										'Quantidade minima: ' || r_C01_w.qt_min_caracteres || '.' || pls_util_pck.enter_w ||
										'Quantidade de caracteres: ' || length(cd_senha_ref_w) || '.';
						end if;
						
						if	(ds_observacao_w is null) then							
							if (r_C01_w.ie_comparar_origem = 'S') then
							
								select max(b.nr_seq_origem)
								into nr_trans_origem_w
								from 	pls_guia_plano a,
									ptu_resposta_autorizacao b
								where a.nr_sequencia = b.nr_seq_guia
								and nr_seq_origem = cd_senha_ref_w
								and a.nr_seq_segurado = r_C02_w.nr_seq_segurado;
								
									if (nr_trans_origem_w is null) then
								
										if (cd_senha_ref_w <> r_C02_w.nr_trans_origem) then
										nr_trans_origem_w := r_C02_w.nr_trans_origem;
										ds_observacao_w := 	'A ' || ds_tipo_senha_w || ' na conta esta diferente da transacao de origem da requisicao. ' || pls_util_pck.enter_w ||
																ds_tipo_senha_w || ' da conta: ' || cd_senha_ref_w || '.' || pls_util_pck.enter_w ||
																 'Transacao de origem da autorizacao: ' || nr_trans_origem_w|| '.';
										end if;		
										
									end if;				
							end if;							
						end if;	
						
						-- Somente ira continuar verificando se nenhuma validacao gerou ocorrencia
						if	(ds_observacao_w is null) then
							
							-- Caso exista restricao por caracter valido
							if	(r_C01_w.ds_carac_valido is not null) then
								-- Retira os caracteres que nao sao validos
								cd_senha_valida_w := regexp_replace(cd_senha_ref_w, '[^'|| r_C01_w.ds_carac_valido ||']');
								
								-- Caso o resultado seja nulo, ou diferente do original, grava a observacao
								if	(cd_senha_valida_w is null) or
									(cd_senha_valida_w <> cd_senha_ref_w) then
									ds_observacao_w := 'A ' || ds_tipo_senha_w || ' possui caracteres invalidos.';
								end if;
							end if;

							-- Somente ira continuar verificando se nenhuma validacao gerou ocorrencia
							if	(ds_observacao_w is null) then
								
								-- Cso tenha informado regra de numero de senha
								if	(r_C01_w.nr_seq_regra_nsenha is not null) then
									
									select	count(1)
									into	qt_regra_nsenha_w
									from	pls_regra_nsenha b
									where	b.nr_sequencia	= r_c01_w.nr_seq_regra_nsenha
									and	b.ie_situacao 	= 'A'
									and	exists (select	1
											from	pls_itens_regra_nsenha a
											where	a.nr_seq_regra_nsenha = b.nr_sequencia
											and	a.cd_senha = cd_senha_ref_w);
									
									-- Caso exista uma regra pra senha, grava a observacao
									if	(qt_regra_nsenha_w > 0) then
										ds_observacao_w := ds_tipo_senha_w || ' nao e valida. Regra de numero de senha invalida: ' || r_c01_w.nr_seq_regra_nsenha;
									end if;
								end if;
								
								-- Somente ira continuar verificando se nenhuma validacao gerou ocorrencia
								if	(ds_observacao_w is null) then
									
									-- Caso a conta possua autorizacao e a senha esteja diferente da autorizacao, gera a ocorrencia
									if	(r_C02_w.nr_seq_guia is not null) and
										(cd_senha_ref_guia_w <> cd_senha_ref_w) and
										(r_C01_w.ie_senha_autorizada = 'S') then
										ds_observacao_w := 	'A ' || ds_tipo_senha_w || ' na conta esta diferente da ' || ds_tipo_senha_w || ' autorizada. ' || pls_util_pck.enter_w ||
													ds_tipo_senha_w || ' da conta: ' || cd_senha_ref_w || '.' || pls_util_pck.enter_w ||
													ds_tipo_senha_w || ' da autorizacao: ' || cd_senha_ref_guia_w || '.';
									end if;
									
									if	(ds_observacao_w is null) then
									
										if	(r_C02_w.nr_seq_guia is not null) and
											(dt_base_vel_w > dt_valdiade_ref_w) and
											(r_C01_w.ie_validade_expirada = 'S') then
											ds_observacao_w :=	'A ' || ds_tipo_senha_w || ' esta expirada. ' || pls_util_pck.enter_w ||
														'Data de envio: ' || dt_base_vel_w || '.' || pls_util_pck.enter_w ||
														'Data de validade: ' || dt_valdiade_ref_w || '.';
										end if;										
									end if;
								end if;
							end if;
						end if;
					end if;
					
					-- Caso, em algum momento, gerou alguma observacao, joga no array
					if	(ds_observacao_w is not null) then
						
						dados_tb_selecao_w.ie_valido(nr_idx_w)		:= 'S';
						dados_tb_selecao_w.nr_seq_selecao(nr_idx_w)	:= r_C02_w.nr_seq_selecao;
						dados_tb_selecao_w.ds_observacao(nr_idx_w)	:= ds_observacao_w;
						
						-- Se o numero de registros ultrapassou a quantidade por transacao, manda pro banco,
						-- Caso contrario, incrementa o contador
						if	(nr_idx_w >= pls_util_cta_pck.qt_registro_transacao_w) then
							pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, 
													pls_util_cta_pck.clob_table_vazia_w,
													'SEQ', 
													dados_tb_selecao_w.ds_observacao, 
													dados_tb_selecao_w.ie_valido, 
													nm_usuario_p);
							
							-- Zera as variaveis
							nr_idx_w := 0;
							pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
						else
							nr_idx_w := nr_idx_w + 1;
						end if;
					end if;
				end if;
			end loop;
			-- Caso tenha sobrado algo, manda pro banco
			if	(nr_idx_w > 0) then
				pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, 
										pls_util_cta_pck.clob_table_vazia_w,
										'SEQ', 
										dados_tb_selecao_w.ds_observacao, 
										dados_tb_selecao_w.ie_valido, 
										nm_usuario_p);
			end if;
			
			pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		end if;
	end loop;
end if;

end pls_oc_cta_tratar_val_107;
/