Create or Replace
Procedure Copia_materiais_exame(	nr_seq_exame_p		number,
					cd_exame_p		varchar2,
					nm_usuario_p		varchar2) is

nr_seq_exame_w		number(10);

begin


select	max(nr_seq_exame)
into	nr_seq_exame_w
from	exame_laboratorio
where	upper(cd_exame) = upper(cd_exame_p);

insert into exame_lab_material
	(nr_seq_exame,           
	nr_seq_material,        
	ie_prioridade,          
	dt_atualizacao,         
	nm_usuario,             
	ds_unidade_medida,      
	qt_decimais,            
	ds_formula,             
	cd_equipamento,         
	ie_formato_resultado,   
	qt_coleta,              
	nr_etiqueta,            
	ie_situacao,            
	nr_seq_unid_med,        
	qt_volume_referencia,   
	nr_seq_conservacao,     
	nr_seq_unid_med_volume)
select	nr_seq_exame_p,           
	nr_seq_material,        
	ie_prioridade,          
	sysdate,         
	nm_usuario_p,             
	ds_unidade_medida,      
	qt_decimais,            
	ds_formula,             
	cd_equipamento,         
	ie_formato_resultado,   
	qt_coleta,              
	nr_etiqueta,            
	ie_situacao,            
	nr_seq_unid_med,        
	qt_volume_referencia,   
	nr_seq_conservacao,     
	nr_seq_unid_med_volume
from	exame_lab_material a
where	nr_seq_exame	= nr_seq_exame_w
and	not exists (select 1 from exame_lab_material w 
		   where w.nr_seq_exame = nr_seq_exame_p 
		   and w.nr_seq_material = a.nr_seq_material);

commit;
end Copia_materiais_exame;
/