create or replace
procedure Atualiza_horario_nut_jejum(	nr_prescricao_p			number,
					ie_acao_p			varchar2,
					nm_usuario_p			Varchar2,
					cd_perfil_p			number) is 

hr_dose_especial_w		varchar2(5);
cd_intervalo_w			varchar2(7);
hr_prim_horario_w		varchar2(5);
hr_prim_hor_jejum_w		varchar2(5);
nr_atendimento_w		number(10);
nr_sequencia_w			number(6);
ds_horarios_w			varchar2(2000);
ds_horarios2_w			Varchar2(2000);
ie_agrupador_w			number(2);
dt_primeiro_horario_w		date;
nr_horas_validade_w		number(5);
dt_inicio_w			date;
cd_material_w			number(6);
qt_hora_intervalo_w		number(2);
qt_min_intervalo_w		number(5);
nr_ocorrencia_w			number(15,4);
ds_dose_diferenciada_w		varchar2(50);
ie_operacao_w			varchar2(1);
qt_operacao_w			intervalo_prescricao.qt_operacao%type;
ie_atualiza_hor_agora_w		varchar2(1);
cd_estabelecimento_w		number(4);
ie_urgencia_w			varchar2(1);

cursor c01 is
select	nvl(hr_dose_especial,'  :  '),
	cd_intervalo,
	nvl(hr_prim_horario,'  :  '),
	nr_sequencia,
	ie_agrupador,
	cd_material,
	qt_hora_intervalo,
	qt_min_intervalo,
	nr_ocorrencia,
	ds_dose_diferenciada,
	nvl(ie_urgencia, 'N')
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	ie_agrupador in (8,12,16,17);

cursor	c02 is
select	nvl(hr_dose_especial,'  :  '),
	nr_sequencia
from	prescr_dieta
where	nr_prescricao	= nr_prescricao_p;	

begin

select	max(nr_atendimento),
	max(cd_estabelecimento),
	nvl(max(nr_horas_validade),24)
into	nr_atendimento_w,
	cd_estabelecimento_w,
	nr_horas_validade_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

Obter_Param_Usuario(950,71,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_atualiza_hor_agora_w);

if	(ie_acao_p	= 'A') then

	select	max(to_char(a.dt_fim + 1/1440,'hh24:mi'))
	into	hr_prim_hor_jejum_w	
	from	rep_jejum a,
		prescr_medica b
	where	a.nr_prescricao 	= b.nr_prescricao
	and	b.dt_suspensao 		is null
	and	nvl(a.ie_suspenso,'N')	<> 'S'
	and	dt_fim			> sysdate
	and	b.nr_atendimento	= nr_atendimento_w;
	
	open C01;
	loop
	fetch C01 into	
		hr_dose_especial_w,
		cd_intervalo_w,
		hr_prim_horario_w,
		nr_sequencia_w,
		ie_agrupador_w,
		cd_material_w,
		qt_hora_intervalo_w,
		qt_min_intervalo_w,
		nr_ocorrencia_w,
		ds_dose_diferenciada_w,
		ie_urgencia_w;
	exit when C01%notfound;
		begin
		
		if	(hr_dose_especial_w		<> '  :  ') and
			(to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_dose_especial_w,'dd/mm/yyyy hh24:mi:ss') > to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_prim_hor_jejum_w,'dd/mm/yyyy hh24:mi:ss')) then
		
			update	prescr_material
			set	hr_dose_especial	= hr_prim_hor_jejum_w
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;
		
		end if;
		
		select	max(qt_operacao),
			max(ie_operacao)
		into	qt_operacao_w,
			ie_operacao_w
		from	intervalo_prescricao
		where	cd_intervalo	= cd_intervalo_w;

		if	(hr_prim_horario_w	<> '  :  ') and
			(((ie_agrupador_w		= 8) and
			  (qt_operacao_w		= 1) and
			  (ie_operacao_w		= 'X') and
			  (to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_prim_horario_w,'dd/mm/yyyy hh24:mi:ss')	> 
			   to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_prim_hor_jejum_w,'dd/mm/yyyy hh24:mi:ss'))) or
			 (ie_urgencia_w	= 'S')) then
		
			dt_primeiro_horario_w	:= to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_prim_hor_jejum_w,'dd/mm/yyyy hh24:mi:ss');
			
			select	max(to_date(to_char(dt_inicio_prescr,'dd/mm/yyyy')||' '||hr_prim_hor_jejum_w,'dd/mm/yyyy hh24:mi:ss'))
			into	dt_inicio_w
			from	prescr_medica
			where	nr_prescricao	= nr_prescricao_p;
		
			Calcular_Horario_Prescricao(	nr_prescricao_p,	cd_intervalo_w,		dt_primeiro_horario_w,	dt_inicio_w,		nr_horas_validade_w,
							cd_material_w,		qt_hora_intervalo_w,	qt_min_intervalo_w,	nr_ocorrencia_w,	ds_horarios_w,
							ds_horarios2_w,		'N', 			ds_dose_diferenciada_w);

			ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;		
		
			update	prescr_material
			set	hr_prim_horario		= hr_prim_hor_jejum_w,
				ds_horarios		= ds_horarios_w
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;
		
		end if;				
		
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into	
		hr_dose_especial_w,
		nr_sequencia_w;
	exit when C02%notfound;
		begin
		
		if	(hr_dose_especial_w		<> '  :  ') and
			(to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_dose_especial_w,'dd/mm/yyyy hh24:mi:ss') > to_date(to_char(sysdate,'dd/mm/yyyy')||' '||hr_prim_hor_jejum_w,'dd/mm/yyyy hh24:mi:ss')) then
		
			update	prescr_dieta
			set	hr_dose_especial	= hr_prim_hor_jejum_w
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;
		
		end if;		
		
		end;
	end loop;
	close C02;
	
elsif	(ie_atualiza_hor_agora_w	= 'S') then

	open C01;
	loop
	fetch C01 into	
		hr_dose_especial_w,
		cd_intervalo_w,
		hr_prim_horario_w,
		nr_sequencia_w,
		ie_agrupador_w,
		cd_material_w,
		qt_hora_intervalo_w,
		qt_min_intervalo_w,
		nr_ocorrencia_w,
		ds_dose_diferenciada_w,
		ie_urgencia_w;
	exit when C01%notfound;
		begin
		
		if	(hr_dose_especial_w		<> '  :  ') then		
		
			update	prescr_material
			set	hr_dose_especial	= to_char(sysdate,'hh24:mi')
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;
		
		end if;
		
		if	(hr_prim_horario_w	<> '  :  ') and
			(((ie_agrupador_w		= 8) and
			  (qt_operacao_w		= 1) and
			  (ie_operacao_w		= 'X')) or
			 (ie_urgencia_w	= 'S')) then
		
			Calcular_Horario_Prescricao(	nr_prescricao_p,	cd_intervalo_w,		sysdate,		sysdate,		nr_horas_validade_w,
							cd_material_w,		qt_hora_intervalo_w,	qt_min_intervalo_w,	nr_ocorrencia_w,	ds_horarios_w,
							ds_horarios2_w,		'N', 			ds_dose_diferenciada_w);

			ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;		
		
			update	prescr_material
			set	hr_prim_horario		= to_char(sysdate,'hh24:mi'),
				ds_horarios		= ds_horarios_w
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;
		
		end if;			
		
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into	
		hr_dose_especial_w,
		nr_sequencia_w;
	exit when C02%notfound;
		begin
		
		if	(hr_dose_especial_w		<> '  :  ') then
		
			update	prescr_dieta
			set	hr_dose_especial	= to_char(sysdate,'hh24:mi')
			where	nr_prescricao		= nr_prescricao_p
			and	nr_sequencia		= nr_sequencia_w;
		
		end if;		
		
		end;
	end loop;
	close C02;	
	
end if;

gerar_horarios_sem_lib(nr_prescricao_p, nm_usuario_p);

commit;

end Atualiza_horario_nut_jejum;
/