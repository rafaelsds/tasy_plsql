create or replace
procedure consiste_estornar_baixa_oc(	nr_ordem_compra_p		number,
					nr_item_oci_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is 

					
ie_permite_estornar_com_nf_w		funcao_param_usuario.vl_parametro%type;
qt_existe_w				number(10);
					
begin

select	max(obter_valor_param_usuario(917, 78, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p))
into	ie_permite_estornar_com_nf_w
from	dual;

if	(ie_permite_estornar_com_nf_w = 'N') then

	select	count(*)
	into	qt_existe_w
	from	nota_fiscal a,
		nota_fiscal_item b
	where	a.nr_sequencia = b.nr_sequencia
	and	b.nr_ordem_compra = nr_ordem_compra_p
	and	a.ie_situacao = '1'
	and	((nr_item_oci_p = 0) or
		((nr_item_oci_p > 0) and (b.nr_item_oci = nr_item_oci_p)));
	
	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(252188); /*Sem permiss�o para estornar a baixa, pois j� possui nota fiscal gerada que est� vinculada com essa ordem de compra. Verifique o par�metro [78].*/
	end if;
elsif	(ie_permite_estornar_com_nf_w = 'C') then

	select	count(*)
	into	qt_existe_w
	from	nota_fiscal a,
		nota_fiscal_item b
	where	a.nr_sequencia = b.nr_sequencia
	and	b.nr_ordem_compra = nr_ordem_compra_p
	and	a.dt_atualizacao_estoque is not null
	and	a.ie_situacao = '1'
	and	((nr_item_oci_p = 0) or
		((nr_item_oci_p > 0) and (b.nr_item_oci = nr_item_oci_p)));
	
	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(334861); /*Sem permiss�o para estornar a baixa, pois j� possui nota fiscal gerada e calculada que est� vinculada com essa ordem de compra. Verifique o par�metro [78].*/
	end if;
end if;

commit;

end consiste_estornar_baixa_oc;
/