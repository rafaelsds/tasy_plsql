create or replace
procedure ptu_gerar_hist_protocolo(	nr_seq_protocolo_p		number,
					nr_seq_hist_pa_p	out	number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a transa��o de solicita��o de hist�rico do protocolo do atendimento
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_protocolo_w			pls_protocolo_atendimento.nr_protocolo%type;

cd_operadora_origem_w		ptu_solicitacao_pa.cd_operadora_origem%type;
cd_operadora_destino_w		ptu_solicitacao_pa.cd_operadora_destino%type;
cd_operadora_w			ptu_solicitacao_pa.cd_operadora%type;
cd_usuario_plano_w		ptu_solicitacao_pa.cd_usuario_plano%type;
nr_seq_execucao_w		ptu_solicitacao_pa.nr_seq_execucao%type;

begin

select	nr_protocolo
into	nr_protocolo_w
from	pls_protocolo_atendimento
where	nr_sequencia	= nr_seq_protocolo_p;

select	cd_operadora_origem,
	cd_operadora_destino,
	cd_operadora,
	cd_usuario_plano,
	nr_seq_execucao
into	cd_operadora_origem_w,
	cd_operadora_destino_w,
	cd_operadora_w,
	cd_usuario_plano_w,
	nr_seq_execucao_w
from	ptu_solicitacao_pa
where	nr_seq_protocolo	= nr_seq_protocolo_p;

insert into ptu_consulta_historico_pa (
		nr_sequencia, cd_estabelecimento, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		cd_transacao, ie_tipo_cliente, cd_operadora_origem,
		cd_operadora_destino, nr_registro_ans, nr_seq_execucao,
		dt_geracao, cd_operadora, cd_usuario_plano,
		nr_protocolo, dt_solicitacao, dt_resposta,
		nr_versao, nr_seq_protocolo)
values (
		ptu_consulta_historico_pa_seq.nextval, cd_estabelecimento_p, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		'008', 'U', cd_operadora_origem_w,
		cd_operadora_destino_w, null, nr_seq_execucao_w,
		sysdate, cd_operadora_w, cd_usuario_plano_w,
		nr_protocolo_w, sysdate, null,
		'001', nr_seq_protocolo_p) returning nr_sequencia into nr_seq_hist_pa_p;

commit;

end ptu_gerar_hist_protocolo;
/