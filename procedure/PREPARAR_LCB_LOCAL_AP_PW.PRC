create or replace
procedure preparar_lcb_local_ap_pw(	nr_seq_horario_p		Number,
							nr_seq_protocolo_p		Number,
							cd_local_ap_p		out Varchar2,
							nm_usuario_p			Varchar2) is 

nr_sequencia_w			atendimento_glicemia.nr_sequencia%type;
nr_seq_ant_w			atendimento_glicemia.nr_sequencia%type;
nr_seq_glicemia_w		atendimento_glicemia.nr_seq_glicemia%type;	
ie_ultimo_local_w		atend_glicemia_evento.ie_local_aplicacao%type;	
ie_proximo_local_w		atend_glicemia_evento.ie_local_aplicacao%type;	
ie_prim_local_w			atend_glicemia_evento.ie_local_aplicacao%type;

cursor c01 is
select	nvl(a.ie_local_aplicacao,'')
from	prot_glic_local_insulina a
where	a.nr_seq_protocolo_glic = nr_seq_protocolo_p
order by   nr_seq_prioridade;
							
begin

open C01;
loop
fetch C01 into	
	ie_prim_local_w;
exit when C01%notfound;
	begin	
		Exit;	
	end;
end loop;
close C01;


cd_local_ap_p	:= '';

select	max(nr_sequencia),
		max(nr_seq_glicemia) 
into	nr_sequencia_w,
		nr_seq_glicemia_w
from	atendimento_glicemia
where	nr_seq_horario = nr_seq_horario_p;

if	(nr_sequencia_w is not null) then

	select	nvl(max(nr_sequencia),0) 
	into	nr_seq_ant_w
	from	atendimento_glicemia
	where 	nr_seq_glicemia = nr_seq_glicemia_w
	and		nr_sequencia 	< nr_sequencia_w;
	
	if	(nr_seq_ant_w > 0) then
	
		select	nvl(max(ie_local_aplicacao),'XPTO') 
		into	ie_ultimo_local_w
		from	atend_glicemia_evento
		where	nr_seq_glicemia = nr_seq_ant_w;
	
		if	(ie_ultimo_local_w <> 'XPTO') then
			
			select	nvl(max(a.ie_local_aplicacao),'') 
			into	ie_proximo_local_w
			from	prot_glic_local_insulina a
			where	a.nr_seq_protocolo_glic = nr_seq_protocolo_p
			and		nr_seq_prioridade > (Select max(b.nr_seq_prioridade)
			from 	prot_glic_local_insulina b
			where	b.nr_seq_protocolo_glic = nr_seq_protocolo_p
			and		b.ie_local_aplicacao = ie_ultimo_local_w)
			and 	rownum = 1;
			
			if	(ie_proximo_local_w <> '') then
				cd_local_ap_p	:=	ie_proximo_local_w; 
			else
				cd_local_ap_p	:=	ie_prim_local_w; 
			end if;
			
		end if;
	
	
	end if;

end if;

end preparar_lcb_local_ap_pw;
/