create or replace
procedure pls_confirmar_envio
			(	nr_seq_protocolo_p		number,
			 	cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

nm_prestador_w			varchar2(255);
ds_hash_w			varchar2(255);
nr_protocolo_prestador_w	varchar2(20);
ie_tipo_guia_w			varchar2(10);
ie_origem_protocolo_w		varchar2(10);
ie_fechado_w			varchar2(1);
vl_parametro_w			varchar2(1)	:= 'N';
vl_apresentado_w		number(15,2);
nr_seq_conta_w			number(10);
nr_seq_transacao_w		varchar2(12);
nr_seq_prestador_imp_w		number(10);
nr_seq_regra_periodo_w		number(10);
nr_seq_periodo_w		number(10);
nr_seq_lote_conta_w		number(10);
cd_perfil_comunic_w		number(5);
qt_registro_w			number(5)	:= 0;
dt_mes_competencia_w		date;
dt_protocolo_w			date;
dt_recebimento_w		date;
dt_aceite_w			date;
cd_cgc_prestador_imp_w		pls_protocolo_conta.cd_cgc_prestador_imp%type;
nr_seq_prestador_imp_aux_w	pls_protocolo_conta.nr_seq_prestador%type;

cursor C01 is
	select	nr_sequencia
	from	pls_conta
	where	nr_seq_protocolo	= nr_seq_protocolo_p;

begin
/* Obter dados do protocolo */
select	pls_obter_prestador_imp(cd_cgc_prestador_imp, nr_cpf_prestador_imp, nr_seq_prestador_imp, '', '', '', 'C', null, dt_protocolo) nr_seq_prestador,
	dt_mes_competencia,
	nr_protocolo_prestador,
	nvl(pls_obter_valor_protocolo(nr_sequencia,'TC'),0),
	nr_seq_transacao,
	ds_hash,
	ie_tipo_guia,
	ie_origem_protocolo,
	nr_seq_lote_conta,
	dt_protocolo,
	dt_recebimento,
	cd_cgc_prestador_imp
into	nr_seq_prestador_imp_w,
	dt_mes_competencia_w,
	nr_protocolo_prestador_w,
	vl_apresentado_w,
	nr_seq_transacao_w,
	ds_hash_w,
	ie_tipo_guia_w,
	ie_origem_protocolo_w,
	nr_seq_lote_conta_w,
	dt_protocolo_w,
	dt_recebimento_w,
	cd_cgc_prestador_imp_w
from	pls_protocolo_conta
where	nr_sequencia	= nr_seq_protocolo_p;

if	(cd_cgc_prestador_imp_w	is not null) then
	nr_seq_prestador_imp_aux_w	:= pls_obter_prestador_cgc(	cd_cgc_prestador_imp_w,null); 
	
	if	(nr_seq_prestador_imp_aux_w	is not null) then
		nr_seq_prestador_imp_w	:= nr_seq_prestador_imp_aux_w;
	end if;
end if;
begin
select	nvl(max(a.dt_aceite),sysdate)
into	dt_aceite_w
from	ptu_fatura	a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p;
exception
when others then
	dt_aceite_w	:= sysdate;
end;

nm_prestador_w	:= substr(pls_obter_dados_prestador(nr_seq_prestador_imp_w,'N'),1,255);

/*Leitura do param�tro [29] - Consistir duplicidade de protocolos importados atrav�s do TISS */
Obter_Param_Usuario(1208, 29, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);

if	(nvl(vl_parametro_w,'N')	= 'S') then
	select	count(1)
	into	qt_registro_w
	from	pls_protocolo_conta
	where	nr_protocolo_prestador	= nr_protocolo_prestador_w
	and	nr_seq_prestador	= nr_seq_prestador_imp_w
	and	ie_tipo_guia		= ie_tipo_guia_w
	and	nr_sequencia		<> nr_seq_protocolo_p
	and	ie_situacao not in ('I', 'A', 'RE');

	if	(qt_registro_w	> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(183003);
		--(-20011,'J� existe um protocolo desse prestador com o mesmo n�mero de lote integrado no sistema! #@#@');
	end if;
end if;
/* Felipe - 10/04/2008 - OS 87447 - Fiz o tratamento conforme solicita��o da OS e alterei o UPDATE do campo DT_MES_COMPETENCIA  de sysdate para a vari�vel do tratamento*/
dt_mes_competencia_w	:= pls_obter_dataref_prot_imp(	nr_seq_prestador_imp_w, ie_origem_protocolo_w, dt_mes_competencia_w, 
							dt_protocolo_w, dt_recebimento_w, dt_aceite_w, 
							ie_tipo_guia_w,nr_seq_protocolo_p, cd_estabelecimento_p);

select	pls_obter_se_mes_fechado(dt_mes_competencia_w,'T',cd_estabelecimento_p)
into	ie_fechado_w
from	dual;

if 	(ie_fechado_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(183006);
	--(-20011,'N�o � poss�vel realizar esta opera��o pois o m�s de compet�ncia ou a contabilidade do m�s est� fechada!');
end if;

pls_obter_periodo_pgto(nr_seq_prestador_imp_w, dt_mes_competencia_w, ie_tipo_guia_w,nr_seq_regra_periodo_w, nr_seq_periodo_w);

update	pls_protocolo_conta
set	ie_situacao		= 'T',
	dt_integracao		= sysdate,
	nm_usuario_integracao	= nm_usuario_p,
	dt_mes_competencia	= dt_mes_competencia_w,
	nr_seq_prestador	= nr_seq_prestador_imp_w,
	cd_condicao_pagamento	= pls_obter_dados_prestador(nr_seq_prestador_imp_w,'CP'),
	dt_base_venc		= sysdate,
	nr_seq_periodo_pgto	= nr_seq_periodo_w
where	nr_sequencia		= nr_seq_protocolo_p
and	cd_estabelecimento	= cd_estabelecimento_p;
 
open C01;
loop
fetch C01 into	
	nr_seq_conta_w;
exit when C01%notfound;
	begin
	pls_consistir_conta(nr_seq_conta_w, cd_estabelecimento_p, nm_usuario_p,'N',null,null,null,'S');
	
	pls_fechar_conta(nr_seq_conta_w, 'N', 'N', 'S', cd_estabelecimento_p, nm_usuario_p,null,null);
	end;
end loop;
close C01;

update	pls_lote_protocolo_conta
set	dt_geracao_analise	= Sysdate
where	nr_sequencia		= nr_seq_lote_conta_w;

pls_gerar_comunic_conta
	(1,	
	'Protocolo: ' || to_char(nr_seq_protocolo_p) || chr(13) ||
	'N� do protocolo no prestador: ' || nr_protocolo_prestador_w || chr(13) ||
	'Data integra��o: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
	'Usu�rio integra��o: ' || nm_usuario_p || chr(13) ||
	'Prestador: ' || to_char(nr_seq_prestador_imp_w) || ' - ' || nm_prestador_w || chr(13) ||
	'Valor apresentado: ' || to_char(vl_apresentado_w) || chr(13) ||
	'Transa��o TISS: ' || nr_seq_transacao_w || chr(13) ||
	'Hash: ' || ds_hash_w,
	nm_usuario_p,cd_estabelecimento_p);	

commit;

end pls_confirmar_envio;
/