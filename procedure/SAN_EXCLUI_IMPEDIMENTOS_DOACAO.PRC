create or replace
procedure san_exclui_impedimentos_doacao(
		nr_seq_doacao_p	number) is 

begin

delete	from san_doacao_impedimento a
where	a.nr_seq_impedimento in
	(select	b.nr_sequencia
	from	san_impedimento b
	where	b.ie_questionario	= 'S')
and	a.nr_seq_doacao = nr_seq_doacao_p;

commit;

end san_exclui_impedimentos_doacao;
/