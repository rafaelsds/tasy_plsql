CREATE OR REPLACE
PROCEDURE Tasy_Alterar_Parametro IS

cd_funcao_w				NUMBER(5);
nr_sequencia_w			NUMBER(5);
ds_parametro_w			VARCHAR2(255);
cd_dominio_w			NUMBER(5);
vl_parametro_padrao_w 	VARCHAR2(255);
ds_observacao_w			VARCHAR2(255);
nr_seq_apresentacao_w	NUMBER(6);
ie_nivel_liberacao_w	VARCHAR2(3);
ds_dependencia_w		VARCHAR2(255);
nr_seq_agrup_w			NUMBER(10);
ds_sql_w				VARCHAR2(2000);
nr_seq_localizador_w	NUMBER(10);

ds_comando_w			varchar2(2000);
c001					Integer;
retorno_w				Number(5);
BEGIN

ds_comando_w := ''||
		' select	a.cd_funcao,'||
		'			a.nr_sequencia,'||
		'			a.ds_parametro,'||
		'			a.cd_dominio,'||
		'			a.vl_parametro_padrao,'||
		'			a.ds_observacao,'||
		'			a.nr_seq_apresentacao,'||
		'			a.ie_nivel_liberacao,'||
		'			a.ds_dependencia,'||
		'			a.nr_seq_agrup,'||
		'			a.ds_sql,'||
		'			a.nr_seq_localizador'||
		' from		tasy_versao.funcao_parametro a'||
		' where		a.dt_atualizacao >= (select 	obter_data_geracao_versao'||
		'								from	dual) ';

		
C001 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C001, ds_comando_w, dbms_sql.Native);
DBMS_SQL.DEFINE_COLUMN(c001,1,cd_funcao_w);
DBMS_SQL.DEFINE_COLUMN(c001,2,nr_sequencia_w);
DBMS_SQL.DEFINE_COLUMN(c001,3,ds_parametro_w,255);
DBMS_SQL.DEFINE_COLUMN(c001,4,cd_dominio_w);
DBMS_SQL.DEFINE_COLUMN(c001,5,vl_parametro_padrao_w,255);
DBMS_SQL.DEFINE_COLUMN(c001,6,ds_observacao_w,255);
DBMS_SQL.DEFINE_COLUMN(c001,7,nr_seq_apresentacao_w);
DBMS_SQL.DEFINE_COLUMN(c001,8,ie_nivel_liberacao_w,3);
DBMS_SQL.DEFINE_COLUMN(c001,9,ds_dependencia_w,255);
DBMS_SQL.DEFINE_COLUMN(c001,10,nr_seq_agrup_w);
DBMS_SQL.DEFINE_COLUMN(c001,11,ds_sql_w,2000);
DBMS_SQL.DEFINE_COLUMN(c001,12,nr_seq_localizador_w);
retorno_w := DBMS_SQL.execute(c001);

while	( DBMS_SQL.FETCH_ROWS(C001) > 0 ) loop
	DBMS_SQL.COLUMN_VALUE(c001,1,cd_funcao_w);
	DBMS_SQL.COLUMN_VALUE(c001,2,nr_sequencia_w);
	DBMS_SQL.COLUMN_VALUE(c001,3,ds_parametro_w);
	DBMS_SQL.COLUMN_VALUE(c001,4,cd_dominio_w);
	DBMS_SQL.COLUMN_VALUE(c001,5,vl_parametro_padrao_w);
	DBMS_SQL.COLUMN_VALUE(c001,6,ds_observacao_w);
	DBMS_SQL.COLUMN_VALUE(c001,7,nr_seq_apresentacao_w);
	DBMS_SQL.COLUMN_VALUE(c001,8,ie_nivel_liberacao_w);
	DBMS_SQL.COLUMN_VALUE(c001,9,ds_dependencia_w);
	DBMS_SQL.COLUMN_VALUE(c001,10,nr_seq_agrup_w);
	DBMS_SQL.COLUMN_VALUE(c001,11,ds_sql_w);
	DBMS_SQL.COLUMN_VALUE(c001,12,nr_seq_localizador_w);
	
	update	funcao_parametro
	set		ds_parametro		= ds_parametro_w,
			cd_dominio			= cd_dominio_w,
			vl_parametro_padrao	= vl_parametro_padrao_w,
			ds_observacao		= ds_observacao_w,
			nr_seq_apresentacao = nr_seq_apresentacao_w,
			ie_nivel_liberacao	= ie_nivel_liberacao_w,
			ds_dependencia		= ds_dependencia_w,
			nr_seq_agrup		= nr_seq_agrup_w,
			ds_sql				= ds_sql_w,
			nr_seq_localizador	= nr_seq_localizador_w
	where	cd_funcao 			= cd_funcao_w
	and		nr_sequencia 		= nr_sequencia_w;
	
	commit;
	
END LOOP; 
DBMS_SQL.CLOSE_CURSOR(C001);

END Tasy_Alterar_Parametro;
/
