create or replace
procedure excluir_protocolo_convenio
                         (	nr_seq_protocolo_p		number,
                          	nm_usuario_p		varchar2) is
dt_atualizacao_w	date := sysdate;
ds_chave_w	varchar2(255);
nr_nota_fiscal_w	varchar2(255);
quebra_w		varchar2(10)	:= chr(13)||chr(10);

begin

update	conta_paciente
set	nr_protocolo   = '0',
	nr_seq_protocolo = null
where	nr_seq_protocolo = nr_seq_protocolo_p;
commit;

/*insert into log_exclusao
	(nm_tabela, dt_atualizacao, nm_usuario, ds_chave)
select 	'PROTOCOLO_CONVENIO ', sysdate, nm_usuario_p, 
	'Seq= ' || nr_seq_protocolo || ' Prot= ' || nr_protocolo || ' Conv= ' || cd_convenio
from 	protocolo_convenio
where 	nr_seq_protocolo = nr_seq_protocolo_p;*/

select 	'Seq= ' || nr_seq_protocolo || ' Prot= ' || nr_protocolo || ' Conv= ' || cd_convenio
into	ds_chave_w
from 	protocolo_convenio
where 	nr_seq_protocolo = nr_seq_protocolo_p;

select	nvl(max(nr_nota_fiscal),'0')
into	nr_nota_fiscal_w
from	 nota_fiscal
where	nr_seq_protocolo	= nr_seq_protocolo_p
and 	ie_situacao 		= 1; -- Somente as Notas Ativas (Fabr�cio em 16/10/2009 OS 172760)

if	(nvl(nr_nota_fiscal_w,'0') <> '0') then
	/*r.aise_application_error(-20011,'N�o � possivel excluir este protocolo pois existe a nota fiscal '||
					nr_nota_fiscal_w ||' vinculada ao mesmo.'||quebra_w||
					'� necess�rio a exclus�o da nota fiscal para poder efetuar este processo. #@#@');*/
	wheb_mensagem_pck.exibir_mensagem_abort(263318,'NR_NOTA_FISCAL_W='||NR_NOTA_FISCAL_W);
end if;

gravar_log_exclusao('PROTOCOLO_CONVENIO',nm_usuario_p,ds_chave_w,'N');

delete	from protocolo_convenio
where	nr_seq_protocolo   = nr_seq_protocolo_p;

commit;
end excluir_protocolo_convenio;
/