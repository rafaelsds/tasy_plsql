CREATE OR REPLACE 
procedure HAOC_Convenio_Categoria
		(cd_convenio_p		in 	number,
		cd_categoria_p		in 	varchar2,
		ds_categoria_p		in	varchar2,
		cd_tipo_acomodacao_p	in	number,
		ie_erro_p		out	number,
		ds_mensagem_p		out	varchar2) is
		

ds_erro_w		varchar2(2000);
cd_categoria_w		varchar2(10);
ds_categoria_w		varchar2(80);

begin

ie_erro_p	:= 0;
ds_mensagem_p	:= null;

if	(ie_erro_p	= 0) then
	begin

	ds_categoria_w		:= substr(ds_categoria_p,1,80);

	select	padronizar_nome(ds_categoria_w)
	into	ds_categoria_w
	from	dual;

	select	nvl(max(cd_categoria), '0')
	into	cd_categoria_w
	from	categoria_convenio
	where	cd_convenio		= cd_convenio_p
	and	cd_categoria		= cd_categoria_p;

	if	(cd_categoria_w	= '0') then
		begin

		insert	into categoria_convenio
			(CD_CONVENIO           , 
			CD_CATEGORIA           ,
			DS_CATEGORIA           ,
			IE_IDENT_ATEND         ,
			IE_EMPRESA             ,
			IE_SITUACAO            ,
			CD_TIPO_ACOMODACAO     ,
			DT_ATUALIZACAO         ,
			NM_USUARIO             ,
			IE_IDADE_DIETA         )
		values
			(CD_CONVENIO_p,
			cd_categoria_p,                       
			ds_categoria_w,
			Obter_Valor_Padrao('CATEGORIA_CONVENIO', 'IE_IDENT_ATEND'),          
			Obter_Valor_Padrao('CATEGORIA_CONVENIO', 'IE_EMPRESA'),          
			'A',          
			cd_tipo_acomodacao_p,
			sysdate,
			'INTEGRACAO',
			Obter_Valor_Padrao('CATEGORIA_CONVENIO', 'IE_IDADE_DIETA'));			

		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na inclus�o da categoria do conv�nio ' || ds_categoria_w || chr(10) || ds_erro_w,1,255);		

		end;
	else
		begin
	
		update	categoria_convenio
		set	ds_categoria		= ds_categoria_w,
			cd_tipo_acomodacao	= cd_tipo_acomodacao_p	
		where	cd_convenio		= cd_convenio_p
		and	cd_categoria		= cd_categoria_p;	

		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na altera��o da categoria do conv�nio ' || ds_categoria_w || chr(10) || ds_erro_w,1,255);			

		end;

	end if;
	end;
end if;

end HAOC_Convenio_Categoria;
/