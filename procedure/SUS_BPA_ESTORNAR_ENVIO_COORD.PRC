create or replace
procedure sus_bpa_estornar_envio_coord(	dt_inicial_p	date,
					dt_final_p		date,
					nm_usuario_p	varchar2) as

nr_interno_conta_w		number(10);

cursor c01 is
	select	a.nr_interno_conta
	from	w_susbpa_interf_coord d,
		sus_bpa_unif b,
		procedimento_paciente c,
		atendimento_paciente e,
		conta_paciente a
	where	a.nr_interno_conta 		= b.nr_interno_conta(+)
	and	a.nr_interno_conta 		= c.nr_interno_conta
	and	a.nr_interno_conta 		= d.nr_interno_conta
	and	a.cd_convenio_parametro 	= obter_dados_param_faturamento(a.cd_estabelecimento,'CSUS')
	and	sus_obter_tiporeg_proc(c.cd_procedimento, c.ie_origem_proced, 'C', 1) in (1,2)
	and	e.ie_tipo_atendimento 	<> 1
	and	a.ie_status_acerto 		= 2
	and	a.ie_cancelamento is null
	and	d.dt_envio_coord is not null
	and	a.dt_periodo_final between dt_inicial_p and dt_final_p
	and	a.nr_atendimento 		= e.nr_atendimento;
	
begin

open c01;
loop
fetch c01 into
	nr_interno_conta_w;
exit when c01%notfound;
	begin
	update	w_susbpa_interf_coord
	set	dt_envio_coord 	= null,
		nm_usuario_envio	= null,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_interno_conta	= nr_interno_conta_w;
	end;
end loop;
close c01;

commit;

end sus_bpa_estornar_envio_coord;
/