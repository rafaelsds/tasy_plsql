create or replace
procedure inserir_historico_faq(nr_seq_ordem_p		number,
				nr_seq_hist_p		number,
				ds_resposta_faq_p	varchar2,
				nm_usuario_p		Varchar2) is 

ds_relat_tecnico_w		varchar2(32000);
ds_hist_concat_w		varchar2(32000);
ds_faq_w			varchar2(32000);
nr_seq_faq_w			number(10);
nr_seq_hist_string		number(10);
nr_seq_faq_string		number(10);
	
Cursor C01 is
	select 	nr_sequencia
	from	com_faq
	where	nr_seq_ordem = nr_seq_ordem_p;	
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_faq_w;
exit when C01%notfound;
	begin
	select	ds_resposta 
	into	ds_faq_w
	from	com_faq  
	where	nr_sequencia = nr_seq_faq_w;
	
	begin

	/*converte_rtf_string('select	ds_relat_tecnico from	man_ordem_serv_tecnico where	nr_sequencia = :nr_seq_hist_p', nr_seq_hist_p, nm_usuario_p, nr_seq_hist_string);

		select	ds_texto
		into	ds_relat_tecnico_w
		from	tasy_conversao_rtf
		where	nr_sequencia = nr_seq_hist_string;

	ds_hist_concat_w := ds_faq_w || ds_relat_tecnico_w;*/
	
	ds_hist_concat_w := ds_faq_w || chr(13) || chr(10) || ds_resposta_faq_p;

	update 	com_faq
	set	ds_resposta = ds_hist_concat_w
	where	nr_sequencia = nr_seq_faq_w;
	
	exception
	when others then
		Wheb_mensagem_pck.exibir_mensagem_abort(291126);
	end;
end;
end loop;
close C01;

commit;

end inserir_historico_faq;
/
