CREATE OR REPLACE
PROCEDURE Cancelar_medic_onco(	nr_prescricao_p		Number,
				nr_seq_medic_p		Number,
				nm_usuario_p		Varchar) is


nr_seq_ordem_w		number(10);
dt_agenda_w		date;
cd_estabelecimento_w	number(4,0);
ie_permite_cancelar_w	varchar2(1);

Cursor C01 is
	select	b.nr_seq_ordem
	from	can_ordem_item_prescr b,
		prescr_material a
	where	a.nr_prescricao		= nr_prescricao_p
	and	nr_seq_atend_medic	= nr_seq_medic_p
	and	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_sequencia		= b.nr_seq_prescricao;

BEGIN

select	max(nvl(cd_estabelecimento,0))
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao	=	nr_prescricao_p;

Obter_Param_Usuario(281,282,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_permite_cancelar_w);

select	nvl(b.dt_real,b.dt_prevista)
into	dt_agenda_w
from	paciente_atendimento b,
	paciente_atend_medic a
where	a.nr_seq_interno	= nr_seq_medic_p
and	a.nr_seq_atendimento	= b.nr_seq_atendimento;

if	(dt_agenda_w < sysdate) and (ie_permite_cancelar_w = 'N') then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266289);
end if;

update	paciente_atend_medic
set	ie_cancelada		= 'S',
	dt_cancelamento		= sysdate,
	nm_usuario_cancel	= nm_usuario_p
where	nr_seq_interno		= nr_seq_medic_p;

update	prescr_material
set	ie_suspenso		= 'S',
	dt_suspensao		= sysdate,
	nm_usuario_susp		= nm_usuario_p
where	nr_prescricao		= nr_prescricao_p
and	nr_seq_atend_medic	= nr_seq_medic_p; 

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_ordem_w;
EXIT when C01%notfound;
	update	can_ordem_prod
	set	ie_cancelada	= 'S',
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_ordem_w;
END LOOP;
CLOSE C01;

commit; 

END Cancelar_medic_onco;
/