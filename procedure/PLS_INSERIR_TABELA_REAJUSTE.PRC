create or replace
procedure pls_inserir_tabela_reajuste
			(	nr_seq_reajuste_p	number,
				nr_seq_tabela_p		number,
				nm_usuario_p		varchar2) is

nr_seq_contrato_w		number(10,0);
nr_seq_contrato_ww		number(10,0);
nr_seq_tabela_preco_w		number(10,0);
nr_seq_plano_w			number(10,0);
cd_estabelecimento_w		number(10,0);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
dt_inicio_vigencia_w		date;
dt_contrato_w			date;
ie_tipo_reajuste_w		varchar2(1);
ie_tipo_inclusao_w		varchar2(1);
qt_aplicacao_w			number(10);
qt_aplicacao_ww			number(10);
cd_pf_estipulante_w		varchar2(10);
ie_aplicacao_reajuste_w		varchar2(2);
nr_seq_reaj_tabela_w		number(10);
nr_seq_lote_referencia_w	number(10);
dt_reajuste_w			date;
nr_seq_intercambio_w		number(10);
nr_seq_plano_ww			number(10);
dt_rescisao_contrato_w		date;
ie_reajustar_benef_cancelado_w	varchar2(1);
ie_situacao_reaj_contrato_w	varchar2(10);
qt_tabela_w			number(10);
tx_deflator_w			number(11,8);
tx_reajuste_lote_w		pls_reajuste.tx_reajuste%type;
tx_reajuste_liminar_w		pls_reajuste.tx_reajuste%type;
nr_seq_processo_w		processo_judicial_liminar.nr_sequencia%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_grupo_produto_w		pls_reajuste.nr_seq_grupo_produto%type;
ie_permite_reajuste_w		varchar2(1);
qt_regra_ativa_w		number(10);
qt_regra_grupo_prod_w		number(10);
nr_seq_titular_w		pls_segurado.nr_seq_titular%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

cursor c01 is
	select	b.nr_sequencia,
		b.nr_seq_plano,
		b.dt_inicio_vigencia,
		b.nr_segurado,
		nvl(to_number(pls_obter_dados_segurado(b.nr_segurado, 'NRTI')), b.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(b.nr_segurado, 'PF') cd_pessoa_fisica
	from	pls_tabela_preco b,
		pls_plano	a
	where	a.nr_sequencia		= b.nr_seq_plano
	--and	b.dt_inicio_vigencia	< trunc(dt_periodo_inicial_w, 'DD')
	and	a.cd_estabelecimento	= cd_estabelecimento_w
	and	a.ie_tipo_contratacao	= 'I'
	and	ie_tipo_reajuste_w	= 'I'
	and	b.dt_inicio_vigencia	< nvl(dt_reajuste_w,sysdate)
	and	((a.nr_sequencia = nr_seq_plano_ww) or (nr_seq_plano_ww is null))
	and	pls_obter_se_aniver_periodo(b.dt_inicio_vigencia,dt_periodo_inicial_w,dt_periodo_final_w)	= 'S'
	and	b.nr_sequencia not in (	select	x.nr_seq_tabela
					from	pls_reajuste_tabela x,
						pls_reajuste y
					where	x.nr_seq_reajuste = y.nr_sequencia
					and	(trunc(dt_periodo_inicial_w,'dd') between y.dt_periodo_inicial and fim_dia(y.dt_periodo_final)
					or	trunc(dt_periodo_final_w,'dd') between y.dt_periodo_inicial and fim_dia(y.dt_periodo_final)))
	order by b.nr_sequencia;		-- tabelas de preco individual

cursor c02 is
	select	b.nr_seq_plano,
		b.nr_sequencia,
		c.dt_contrato,
		b.dt_inicio_vigencia,
		c.dt_rescisao_contrato,
		b.nr_segurado,
		nvl(to_number(pls_obter_dados_segurado(b.nr_segurado, 'NRTI')), b.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(b.nr_segurado, 'PF') cd_pessoa_fisica
	from	pls_segurado		a,
		pls_tabela_preco	b,
		pls_contrato		c,
		pls_plano		d
	where	a.nr_seq_tabela		= b.nr_sequencia
	and	a.nr_seq_contrato	= c.nr_sequencia
	and	b.nr_seq_plano		= d.nr_sequencia
	and	c.nr_sequencia		= nr_seq_contrato_w
	and	c.dt_contrato		< nvl(dt_reajuste_w,sysdate)
	and	d.ie_tipo_contratacao	<> 'I'
	and	ie_tipo_reajuste_w	= 'C'
	and	((d.nr_sequencia = nr_seq_plano_ww) or (nr_seq_plano_ww is null))
	and	((c.ie_reajuste = 'C') or (to_char(nvl(a.dt_reajuste,a.dt_contratacao),'MM') = to_char(dt_reajuste_w,'MM')))
	union
	select	a.nr_seq_plano,
		a.nr_sequencia,
		b.dt_contrato,
		a.dt_inicio_vigencia,
		b.dt_rescisao_contrato,
		a.nr_segurado,
		null nr_seq_titular,
		null cd_pessoa_fisica
	from	pls_tabela_preco	a,
		pls_contrato		b,
		pls_plano		c,
		pls_grupo_contrato	d,
		pls_contrato_grupo	e
	where	a.nr_contrato		= b.nr_sequencia
	and	a.nr_seq_plano		= c.nr_sequencia
	and	e.nr_seq_contrato(+)	= b.nr_sequencia
	and	e.nr_seq_grupo		= d.nr_sequencia(+)
	and	b.nr_sequencia		= nr_seq_contrato_w
	and	b.dt_contrato		< nvl(dt_reajuste_w,sysdate)
	and	a.nr_segurado		is null
	and	c.ie_tipo_contratacao	<> 'I'
	and	ie_tipo_reajuste_w	= 'C'
	and	exists (select 1 from pls_contrato_plano x where x.nr_seq_contrato = b.nr_sequencia and x.nr_seq_plano = a.nr_seq_plano)
	and	((c.nr_sequencia = nr_seq_plano_ww) or (nr_seq_plano_ww is null))
	and	((b.ie_reajuste = 'C') 
	or	(((d.dt_reajuste is not null and e.ie_reajuste_grupo = 'S' and to_char(d.dt_reajuste,'mm') = to_char(dt_reajuste_w,'mm')) or	
		 ((d.dt_reajuste is null or e.ie_reajuste_grupo = 'N') and
			(((b.dt_reajuste is not null) and (to_char(b.dt_reajuste,'mm') = to_char(dt_reajuste_w,'mm'))) or	
			 ((b.dt_reajuste is null) and (to_char(add_months(b.dt_contrato,nvl(b.qt_intervalo,12)),'mm') = to_char(dt_reajuste_w,'mm'))))))))
	group by	a.nr_seq_plano,
			a.nr_sequencia,
			b.dt_contrato,
			a.dt_inicio_vigencia,
			b.dt_rescisao_contrato,
			a.nr_segurado
	union
	select	b.nr_seq_plano, -- Produtos do contrato de intercambio
		b.nr_sequencia,
		c.dt_inclusao,
		b.dt_inicio_vigencia,
		c.dt_exclusao,
		b.nr_segurado,
		nvl(to_number(pls_obter_dados_segurado(b.nr_segurado, 'NRTI')), b.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(b.nr_segurado, 'PF') cd_pessoa_fisica
	from	pls_segurado	a,
		pls_tabela_preco b,
		pls_intercambio	c,
		pls_plano	d
	where	a.nr_seq_tabela		= b.nr_sequencia
	and	a.nr_seq_intercambio	= c.nr_sequencia
	and	b.nr_seq_plano		= d.nr_sequencia
	and	c.nr_sequencia		= nr_seq_intercambio_w
	and	c.dt_inclusao		< nvl(dt_reajuste_w,sysdate)
	/*and	d.ie_tipo_contratacao	<> 'I'  Produto de intercambio nao tem tipo de contratacao */
	and	ie_tipo_reajuste_w	= 'C'
	and	((d.nr_sequencia = nr_seq_plano_ww) or (nr_seq_plano_ww is null))
	union
	select	a.nr_seq_plano,
		a.nr_sequencia,
		c.dt_inclusao,
		a.dt_inicio_vigencia,
		c.dt_exclusao,
		a.nr_segurado,
		nvl(to_number(pls_obter_dados_segurado(a.nr_segurado, 'NRTI')), a.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(a.nr_segurado, 'PF') cd_pessoa_fisica
	from	pls_tabela_preco	a,
		pls_intercambio_plano	b,
		pls_intercambio		c,
		pls_plano		d
	where	b.nr_seq_tabela		= a.nr_sequencia
	and	b.nr_seq_intercambio	= c.nr_sequencia
	and	a.nr_seq_plano		= d.nr_sequencia
	and	c.nr_sequencia		= nr_seq_intercambio_w
	and	c.dt_inclusao		< nvl(dt_reajuste_w,sysdate)
	/*and	d.ie_tipo_contratacao	<> 'I' Produto de intercambio nao tem tipo de contratacao */
	and	ie_tipo_reajuste_w	= 'C'
	and	((d.nr_sequencia = nr_seq_plano_ww) or (nr_seq_plano_ww is null))
	group by
		a.nr_seq_plano,
		a.nr_sequencia,
		c.dt_inclusao,
		a.dt_inicio_vigencia,
		c.dt_exclusao,
		a.nr_segurado
	union
	select	null,
		a.nr_sequencia,
		b.dt_contrato,
		a.dt_inicio_vigencia,
		b.dt_rescisao_contrato,
		a.nr_segurado,
		nvl(to_number(pls_obter_dados_segurado(a.nr_segurado, 'NRTI')), a.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(a.nr_segurado, 'PF') cd_pessoa_fisica
	from	pls_tabela_preco	a,
		pls_contrato		b
	where	a.nr_seq_contrato_repasse = b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_contrato_w
	and	b.dt_contrato		< nvl(dt_reajuste_w,sysdate)
	and	ie_tipo_reajuste_w	= 'C'
	and	((nr_seq_plano_ww is null) or (a.nr_seq_plano_repasse = nr_seq_plano_ww))
	group by
		a.nr_seq_plano,
		a.nr_sequencia,
		b.dt_contrato,
		a.dt_inicio_vigencia,
		b.dt_rescisao_contrato,
		a.nr_segurado
	union
	select	b.nr_seq_plano,
		b.nr_sequencia nr_seq_tabela,
		a.dt_contrato,
		b.dt_inicio_vigencia,
		a.dt_rescisao_contrato,
		c.nr_sequencia nr_seq_segurado,
		nvl(to_number(pls_obter_dados_segurado(c.nr_sequencia, 'NRTI')), c.nr_sequencia) nr_seq_titular,
		pls_obter_dados_segurado(c.nr_sequencia, 'PF') cd_pessoa_fisica
	from	pls_contrato a,
		pls_tabela_preco b,
		pls_segurado c,
		pls_plano d
	where	a.nr_sequencia	 	= c.nr_seq_contrato
	and	c.nr_sequencia	 	= b.nr_segurado
	and	a.nr_sequencia	 	= nr_seq_contrato_w
	and	d.nr_sequencia		= b.nr_seq_plano
	and	ie_tipo_reajuste_w	= 'C'
	and	d.ie_tipo_operacao	= 'B' --Produto
	and	a.dt_contrato		< nvl(dt_reajuste_w,sysdate)
	and  	((b.nr_contrato = a.nr_sequencia) or (b.nr_contrato is null))
	and	((d.nr_sequencia = nr_seq_plano_ww) or (nr_seq_plano_ww is null))
	and	((a.ie_reajuste = 'C') or (to_char(nvl(c.dt_reajuste, c.dt_contratacao),'MM') = to_char(dt_reajuste_w,'MM')))	
	order by 2;

Cursor C03 is
	select	b.nr_seq_tabela,
		b.nr_seq_plano,
		c.dt_inicio_vigencia,
		a.dt_rescisao,
		c.nr_segurado,
		nvl(to_number(pls_obter_dados_segurado(c.nr_segurado, 'NRTI')), c.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(c.nr_segurado, 'PF') cd_pessoa_fisica
	from	pls_segurado		a,
		pls_sca_vinculo		b,
		pls_tabela_preco 	c,
		pls_contrato		d
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	b.nr_seq_tabela		= c.nr_sequencia
	and	a.nr_seq_contrato	= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_contrato_w
	and	d.dt_contrato		< nvl(dt_reajuste_w,sysdate)
	and	((b.dt_reajuste is not null) and (trunc(b.dt_reajuste, 'month') = dt_reajuste_w))
	and	to_char(dt_reajuste_w,'YYYY') > to_char(b.dt_inicio_vigencia,'YYYY');
	
begin

if	(nvl(nr_seq_tabela_p,0) = 0) then
	ie_tipo_inclusao_w := 'C';
else
	ie_tipo_inclusao_w := 'I';
end if;

select	nr_seq_contrato,
	nr_seq_intercambio,
	nvl(dt_periodo_inicial,trunc(dt_reajuste,'month')),
	nvl(dt_periodo_final,last_day(dt_reajuste)),
	cd_estabelecimento,
	ie_tipo_reajuste,
	nr_seq_lote_referencia,
	trunc(dt_reajuste,'month'),
	nr_seq_plano,
	tx_deflator,
	nvl(ie_reajustar_benef_cancelado,'T'),
	nvl(tx_reajuste,0),
	nr_seq_grupo_produto,
	ie_aplicacao_reajuste
into	nr_seq_contrato_w,
	nr_seq_intercambio_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w,
	cd_estabelecimento_w,
	ie_tipo_reajuste_w,
	nr_seq_lote_referencia_w,
	dt_reajuste_w,
	nr_seq_plano_ww,
	tx_deflator_w,
	ie_situacao_reaj_contrato_w,
	tx_reajuste_lote_w,
	nr_seq_grupo_produto_w,
	ie_aplicacao_reajuste_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;

if	(ie_aplicacao_reajuste_w is null) then
	begin
	select	nvl(ie_aplicacao_reajuste,'G')
	into	ie_aplicacao_reajuste_w
	from	pls_reajuste
	where	nr_sequencia	= nr_seq_lote_referencia_w;
	exception
	when others then
		ie_aplicacao_reajuste_w	:= 'G';
	end;
end if;

select	count(*)
into	qt_aplicacao_w
from	pls_reajuste
where	(ie_aplicacao_reajuste	= 'C' or
	nvl(ie_aplicacao_reajuste,'G')	= 'G')
and	nr_sequencia	= nr_seq_reajuste_p;

if	(nvl(nr_seq_contrato_w,0) <> 0) then
	select	cd_pf_estipulante
	into	cd_pf_estipulante_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
elsif	(nvl(nr_seq_intercambio_w,0) <> 0) then
	select	cd_pessoa_fisica
	into	cd_pf_estipulante_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_w;
end if;

select	max(ie_reajustar_benef_cancelado)
into	ie_reajustar_benef_cancelado_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_w;

ie_reajustar_benef_cancelado_w	:= nvl(ie_reajustar_benef_cancelado_w,'T');

if	(ie_tipo_inclusao_w = 'C') and 
	(qt_aplicacao_w > 0) and
	(cd_pf_estipulante_w is null) then

	if	(ie_aplicacao_reajuste_w in ('G','C')) then
		open c01;
		loop
		fetch c01 into
			nr_seq_tabela_preco_w,
			nr_seq_plano_w,
			dt_inicio_vigencia_w,
			nr_seq_segurado_w,
			nr_seq_titular_w,
			cd_pessoa_fisica_w;
		exit when c01%notfound;
			begin
			select	count(1)
			into	qt_tabela_w
			from	pls_reajuste_tabela
			where	nr_seq_reajuste	= nr_seq_reajuste_p
			and	nr_seq_tabela	= nr_seq_tabela_preco_w;
			
			ie_permite_reajuste_w := 'S';

			if	(nr_seq_grupo_produto_w is not null) then
				select	count(1)
				into	qt_regra_ativa_w
				from	pls_grupo_produto
				where	nr_sequencia = nr_seq_grupo_produto_w
				and	ie_situacao = 'A';

				if	(qt_regra_ativa_w > 0) then
					select	count(1)
					into	qt_regra_grupo_prod_w
					from	pls_produto_grupo
					where	nr_seq_grupo = nr_seq_grupo_produto_w
					and	nr_seq_plano = nr_seq_plano_w;

					if	(qt_regra_grupo_prod_w = 0) then
						ie_permite_reajuste_w := 'N';
					end if;
				end if;
			end if;
			
			if	(qt_tabela_w = 0) and (ie_permite_reajuste_w = 'S') then
				pls_obter_processo_reaj(null, nr_seq_segurado_w, nr_seq_titular_w, cd_pessoa_fisica_w, dt_reajuste_w, nr_seq_processo_w, tx_reajuste_liminar_w);
				
				select	pls_reajuste_tabela_seq.nextval
				into	nr_seq_reaj_tabela_w
				from	dual;
				
				insert into pls_reajuste_tabela
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_reajuste,
					nr_seq_tabela,
					nr_seq_plano,
					dt_contrato,
					dt_inicio_vigencia,
					ie_origem_tabela,
					nr_seq_processo,
					tx_reajuste)
				values	(nr_seq_reaj_tabela_w,
					sysdate,
					nm_usuario_p,
					nr_seq_reajuste_p,
					nr_seq_tabela_preco_w,
					nr_seq_plano_w,
					null,
					dt_inicio_vigencia_w,
					'B',
					nr_seq_processo_w,
					nvl(tx_reajuste_liminar_w,tx_reajuste_lote_w));
			end if;
			end;
		end loop;
		close c01;

		open c02;
		loop
		fetch c02 into
			nr_seq_plano_w,
			nr_seq_tabela_preco_w,
			dt_contrato_w,
			dt_inicio_vigencia_w,
			dt_rescisao_contrato_w,
			nr_seq_segurado_w,
			nr_seq_titular_w,
			cd_pessoa_fisica_w;
		exit when c02%notfound;

			ie_permite_reajuste_w := 'S';

			if	(nr_seq_grupo_produto_w is not null) then
				select	count(1)
				into	qt_regra_ativa_w
				from	pls_grupo_produto
				where	nr_sequencia = nr_seq_grupo_produto_w
				and	ie_situacao = 'A';

				if	(qt_regra_ativa_w > 0) then
					select	count(1)
					into	qt_regra_grupo_prod_w
					from	pls_produto_grupo
					where	nr_seq_grupo = nr_seq_grupo_produto_w
					and	nr_seq_plano = nr_seq_plano_w;

					if	(qt_regra_grupo_prod_w = 0) then
						ie_permite_reajuste_w := 'N';
					end if;
				end if;
			end if;

			if	((ie_permite_reajuste_w = 'S') and
				((dt_rescisao_contrato_w is null) or (dt_rescisao_contrato_w > dt_reajuste_w)) or
				((dt_rescisao_contrato_w is not null) and (ie_reajustar_benef_cancelado_w in ('T','C')))) then
				
				/*Caso a situacao para o reajuste do contrato for diferente de ambos verificar como esta a situacao do contrato*/
				if	(ie_situacao_reaj_contrato_w <> 'T') then
					if	(nr_seq_contrato_w is not null) then
						if	(pls_obter_situac_contr_periodo(nr_seq_contrato_w,'C','M',dt_reajuste_w) <> ie_situacao_reaj_contrato_w) then
							goto finalCursor02;
						end if;
					elsif	(nr_seq_intercambio_w is not null) then
						if	(pls_obter_situac_contr_periodo(nr_seq_intercambio_w,'I','M',dt_reajuste_w) <> ie_situacao_reaj_contrato_w) then
							goto finalCursor02;
						end if;
					end if;
				end if;
				
				select	count(*)
				into	qt_tabela_w
				from	pls_reajuste_tabela
				where	nr_seq_reajuste	= nr_seq_reajuste_p
				and	nr_seq_tabela	= nr_seq_tabela_preco_w;
				
				if	(qt_tabela_w = 0) and
					(nvl(tx_deflator_w,0) = 0) then
					/* Conforme contato com Ewerton, o contrato nao pode ser reajuste masi de uma vez no mesmo mes, com excecao para poder aplicar o deflator */
					select	count(*)
					into	qt_tabela_w
					from	pls_reajuste_tabela	a,
						pls_reajuste		b
					where	a.nr_seq_reajuste	= b.nr_sequencia
					and	nr_seq_tabela		= nr_seq_tabela_preco_w
					and	trunc(b.dt_reajuste,'month')	= dt_reajuste_w
					and	b.nr_seq_lote_deflator is null
					and	nvl(b.tx_deflator,0) = 0;
				end if;
				
				if	(qt_tabela_w = 0) then
					pls_obter_processo_reaj(null, nr_seq_segurado_w, nr_seq_titular_w, cd_pessoa_fisica_w, dt_reajuste_w, nr_seq_processo_w, tx_reajuste_liminar_w);
					
					select	pls_reajuste_tabela_seq.nextval
					into	nr_seq_reaj_tabela_w
					from	dual;
					
					insert	into pls_reajuste_tabela
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_reajuste,
								nr_seq_tabela,
								nr_seq_plano,
								dt_contrato,
								dt_inicio_vigencia,
								ie_origem_tabela,
								nr_seq_processo,
								tx_reajuste)
						values	(	nr_seq_reaj_tabela_w,
								sysdate,
								nm_usuario_p,
								nr_seq_reajuste_p,
								nr_seq_tabela_preco_w,
								nr_seq_plano_w,
								dt_contrato_w,
								dt_inicio_vigencia_w,
								'B',
								nr_seq_processo_w,
								nvl(tx_reajuste_liminar_w,tx_reajuste_lote_w));
				end if;
			end if;
			
			<<finalCursor02>>
			nr_seq_plano_w	:= nr_seq_plano_w;
			
		end loop;
		close c02;
	end if;

	if	(ie_aplicacao_reajuste_w in ('G','S')) then
		open C03;
		loop
		fetch C03 into
			nr_seq_tabela_preco_w,
			nr_seq_plano_w,
			dt_inicio_vigencia_w,
			dt_rescisao_contrato_w,
			nr_seq_segurado_w,
			nr_seq_titular_w,
			cd_pessoa_fisica_w;
		exit when C03%notfound;
			begin

			ie_permite_reajuste_w := 'S';
			
			if	(nr_seq_grupo_produto_w is not null) then
				select	count(1)
				into	qt_regra_ativa_w
				from	pls_grupo_produto
				where	nr_sequencia = nr_seq_grupo_produto_w
				and	ie_situacao = 'A';

				if	(qt_regra_ativa_w > 0) then
					select	count(1)
					into	qt_regra_grupo_prod_w
					from	pls_produto_grupo
					where	nr_seq_grupo = nr_seq_grupo_produto_w
					and	nr_seq_plano = nr_seq_plano_w;

					if	(qt_regra_grupo_prod_w = 0) then
						ie_permite_reajuste_w := 'N';
					end if;
				end if;
			end if;

			if	((ie_permite_reajuste_w = 'S') and
				((dt_rescisao_contrato_w is null) or (dt_rescisao_contrato_w > dt_reajuste_w)) or
				((dt_rescisao_contrato_w is not null) and (ie_reajustar_benef_cancelado_w in ('T','C')))) then
				
				/*Caso a situacao para o reajuste do contrato for diferente de ambos verificar como esta a situacao do contrato*/
				if	(ie_situacao_reaj_contrato_w <> 'T') then
					if	(nr_seq_contrato_w is not null) then
						if	(pls_obter_situac_contr_periodo(nr_seq_contrato_w,'C','M',dt_reajuste_w) <> ie_situacao_reaj_contrato_w) then
							goto finalCursor03;
						end if;
					elsif	(nr_seq_intercambio_w is not null) then
						if	(pls_obter_situac_contr_periodo(nr_seq_intercambio_w,'I','M',dt_reajuste_w) <> ie_situacao_reaj_contrato_w) then
							goto finalCursor03;
						end if;
					end if;
				end if;
				
				select	count(*)
				into	qt_tabela_w
				from	pls_reajuste_tabela
				where	nr_seq_reajuste	= nr_seq_reajuste_p
				and	nr_seq_tabela	= nr_seq_tabela_preco_w;
				
				if	(qt_tabela_w = 0) and
					(nvl(tx_deflator_w,0) = 0) then
					/* Conforme contato com Ewerton, o contrato nao pode ser reajuste masi de uma vez no mesmo mes, com excecao para poder aplicar o deflator */
					select	count(*)
					into	qt_tabela_w
					from	pls_reajuste_tabela	a,
						pls_reajuste		b
					where	a.nr_seq_reajuste	= b.nr_sequencia
					and	nr_seq_tabela		= nr_seq_tabela_preco_w
					and	trunc(b.dt_reajuste,'month')	= dt_reajuste_w
					and	b.nr_seq_lote_deflator is null
					and	nvl(b.tx_deflator,0) = 0;
				end if;
				
				if	(qt_tabela_w = 0) then
					pls_obter_processo_reaj(null, nr_seq_segurado_w, nr_seq_titular_w, cd_pessoa_fisica_w, dt_reajuste_w, nr_seq_processo_w, tx_reajuste_liminar_w);
					
					select	pls_reajuste_tabela_seq.nextval
					into	nr_seq_reaj_tabela_w
					from	dual;
					
					insert into pls_reajuste_tabela
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_reajuste,
						nr_seq_tabela,
						nr_seq_plano,
						dt_contrato,
						dt_inicio_vigencia,
						ie_origem_tabela,
						nr_seq_processo,
						tx_reajuste)
					values	(nr_seq_reaj_tabela_w,
						sysdate,
						nm_usuario_p,
						nr_seq_reajuste_p,
						nr_seq_tabela_preco_w,
						nr_seq_plano_w,
						null,
						dt_inicio_vigencia_w,
						'B',
						nr_seq_processo_w,
						nvl(tx_reajuste_liminar_w,tx_reajuste_lote_w));
				end if;
			end if;
			
			<<finalCursor03>>
			nr_seq_plano_w	:= nr_seq_plano_w;
			
			end;
		end loop;
		close C03;
	end if;
elsif	(ie_tipo_inclusao_w = 'I') then
	select	nvl(max(b.nr_seq_plano),0),
		nvl(max(b.dt_inicio_vigencia),sysdate),
		b.nr_segurado,
		nvl(pls_obter_dados_segurado(b.nr_segurado, 'NRTI'), b.nr_segurado) nr_seq_titular,
		pls_obter_dados_segurado(b.nr_segurado, 'PF') cd_pessoa_fisica
	into	nr_seq_plano_w,
		dt_inicio_vigencia_w,
		nr_seq_segurado_w,
		nr_seq_titular_w,
		cd_pessoa_fisica_w
	from	pls_tabela_preco b,
		pls_plano	a
	where	a.nr_sequencia		= b.nr_seq_plano
	and	b.nr_sequencia		= nr_seq_tabela_p
	and	a.cd_estabelecimento	= cd_estabelecimento_w
	and	b.nr_sequencia not in (	select	x.nr_seq_tabela
					from	pls_reajuste_tabela x,
						pls_reajuste y
					where	x.nr_seq_reajuste = y.nr_sequencia
					and	(trunc(dt_periodo_inicial_w,'dd') between y.dt_periodo_inicial and fim_dia(y.dt_periodo_final)
					or	trunc(dt_periodo_final_w,'dd') between y.dt_periodo_inicial and fim_dia(y.dt_periodo_final)));
	
	ie_permite_reajuste_w := 'S';
	
	if	((nr_seq_grupo_produto_w is not null) and (nvl(nr_seq_plano_w,0) <> 0)) then
		select	count(1)
		into	qt_regra_ativa_w
		from	pls_grupo_produto
		where	nr_sequencia = nr_seq_grupo_produto_w
		and	ie_situacao = 'A';

		if	(qt_regra_ativa_w > 0) then
			select	count(1)
			into	qt_regra_grupo_prod_w
			from	pls_produto_grupo
			where	nr_seq_grupo = nr_seq_grupo_produto_w
			and	nr_seq_plano = nr_seq_plano_w;

			if	(qt_regra_grupo_prod_w = 0) then
				ie_permite_reajuste_w := 'N';
			end if;
		end if;
	end if;
	
	if	(nvl(nr_seq_plano_w,0) <> 0) and (ie_permite_reajuste_w = 'S') then
		pls_obter_processo_reaj(null, nr_seq_segurado_w, nr_seq_titular_w, cd_pessoa_fisica_w, dt_reajuste_w, nr_seq_processo_w, tx_reajuste_liminar_w);
		
		select	pls_reajuste_tabela_seq.nextval
		into	nr_seq_reaj_tabela_w
		from	dual;

		insert into pls_reajuste_tabela
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_reajuste,
				nr_seq_tabela,
				nr_seq_plano,
				dt_contrato,
				dt_inicio_vigencia,
				ie_origem_tabela,
				nr_seq_processo,
				tx_reajuste)
			values	(nr_seq_reaj_tabela_w,
				sysdate,
				nm_usuario_p,
				nr_seq_reajuste_p,
				nr_seq_tabela_p,
				nr_seq_plano_w,
				null,
				dt_inicio_vigencia_w,
				'B',
				nr_seq_processo_w,
				nvl(tx_reajuste_liminar_w,tx_reajuste_lote_w));
	end if;
end if;

select	max(a.nr_seq_contrato)
into	nr_seq_contrato_ww
from	pls_reajuste	a
where	a.nr_sequencia	= nr_seq_reajuste_p;

select	count(1)
into	qt_aplicacao_ww
from	pls_reajuste
where	(ie_aplicacao_reajuste	= 'S' or
	ie_aplicacao_reajuste	= 'G')
and	nr_sequencia	= nr_seq_reajuste_p;

if	(qt_aplicacao_ww > 0) then
	pls_inserir_tabelas_sca(nr_seq_reajuste_p,nr_seq_contrato_ww,nm_usuario_p);
end if;

commit;

end pls_inserir_tabela_reajuste;
/
