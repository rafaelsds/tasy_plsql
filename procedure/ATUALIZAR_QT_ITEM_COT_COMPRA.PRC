create or replace
procedure atualizar_qt_item_cot_compra(
				nr_sequencia_p	number,
				qt_material_p	varchar2,
				nm_usuario_p	varchar2) is

Cursor C01 is
	select 	a.cd_material,
		m.ds_material,
		a.qt_material, 
		b.cd_cgc_fornecedor cd_fornecedor,
		p.ds_razao_social ds_fornecedor,
		a.nr_cot_compra, 
		a.nr_item_cot_compra
	from 	cot_compra_forn_item a, 
		cot_compra_forn b,
		pessoa_juridica p,
		material m
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_seq_cot_forn = b.nr_sequencia
	and	b.cd_cgc_fornecedor = p.cd_cgc
	and	a.cd_material = m.cd_material
	union all
	select 	a.cd_material,
		m.ds_material,
		a.qt_material, 
		b.cd_pessoa_fisica cd_fornecedor,
		SUBSTR(OBTER_NOME_PF(p.cd_pessoa_fisica), 0, 255) ds_fornecedor,
		a.nr_cot_compra, 
		a.nr_item_cot_compra
	from 	cot_compra_forn_item a, 
		cot_compra_forn b,
		pessoa_fisica p,
		material m
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_seq_cot_forn = b.nr_sequencia
	and	b.cd_pessoa_fisica = p.cd_pessoa_fisica
	and	a.cd_material = m.cd_material;

c01_w				C01%rowtype;
historico_w			varchar2(4000);

begin
open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	update 	cot_compra_forn_item 
	set 	qt_material 	= qt_material_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where 	nr_sequencia 	= nr_sequencia_p;

	historico_w := Wheb_mensagem_pck.get_Texto(297658,
			'NR_ITEM_COT_COMPRA_W='||c01_w.nr_item_cot_compra||
			';CD_MATERIAL_W='||c01_w.cd_material||
			';DS_MATERIAL_W='||c01_w.ds_material||
			';DS_FORNECEDOR_W='||c01_w.ds_fornecedor||
			';QT_MATERIAL_W='||campo_mascara(c01_w.qt_material,4)||
			';QT_MAT_W='||campo_mascara(qt_material_p,4));
			
	
		/*Foi alterada a quantidade do item [#@NR_ITEM_COT_COMPRA_W#@] - #@CD_MATERIAL_W#@ (#@DS_MATERIAL_W#@), do fornecedor #@DS_FORNECEDOR_W#@.
		De: #@QT_MATERIAL_W
		Para: #@QT_MATERIAL_W#@*/
			

	insert into cot_compra_hist(
		nr_sequencia,
		nr_cot_compra,
		dt_atualizacao,
		nm_usuario,
		dt_historico,
		ds_titulo,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_origem,
		ie_tipo)
	values(
		cot_compra_hist_seq.nextval,
		c01_w.nr_cot_compra,
		sysdate,
		nm_usuario_p,
		sysdate,
		Wheb_mensagem_pck.get_Texto(297642),
		historico_w,
		sysdate,
		nm_usuario_p,
		'S','H');
		
	end;
end loop;
close C01;

commit;

end atualizar_qt_item_cot_compra;
/
