create or replace
procedure pls_desfazer_aguardo_anexo(
			nr_seq_guia_p		Number,
			nm_usuario_p		Varchar2) is 

ds_observacao_w		Varchar2(255);

begin

update	pls_auditoria
set	ie_status = 'A'
where	nr_seq_guia = nr_seq_guia_p;

ds_observacao_w	:= 'O usu�rio ' || nm_usuario_p || ' alterou o status da an�lise Aguardando Anexo Guia TISS para An�lise';
pls_guia_gravar_historico( nr_seq_guia_p, 2, ds_observacao_w, '', nm_usuario_p);

commit;

end pls_desfazer_aguardo_anexo;
/