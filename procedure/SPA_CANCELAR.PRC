create or replace
procedure spa_cancelar(	nr_seq_spa_p	Number,
			nr_seq_motivo_cancel_p	Number,
			nm_usuario_p	Varchar2) is 

ie_status_w	spa.ie_status%type;

begin
select 	ie_status
into	ie_status_w
from 	spa
where 	nr_sequencia = nr_seq_spa_p;

update	spa
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_cancelamento = sysdate,
	nm_usuario_cancel = nm_usuario_p,
	nr_seq_motivo_cancel = decode(nr_seq_motivo_cancel_p, 0, null, nr_seq_motivo_cancel_p),
	ie_status = 6
where	nr_sequencia = nr_seq_spa_p;

update	spa_aprovacao
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_cancelamento = sysdate
where	nr_seq_spa = nr_seq_spa_p;

gerar_spa_hist_email('C',nr_seq_spa_p,nm_usuario_p);

spa_gerar_historico(nr_seq_spa_p,nm_usuario_p,ie_status_w,6,3,'','N');

commit;

end spa_cancelar;
/
