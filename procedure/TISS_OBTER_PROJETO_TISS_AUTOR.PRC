create or replace
procedure TISS_OBTER_PROJETO_TISS_AUTOR
		(cd_estabelecimento_p	number,
		cd_convenio_p		number,
		nr_sequencia_autor_p	number,
		ie_tipo_autorizacao_p	varchar2,
		nr_seq_estagio_p	number,
		nr_seq_projeto_p	out number,
		ie_tipo_tiss_p		out varchar2) is
	
ie_interno_w		estagio_autorizacao.ie_interno%type;
ie_tipo_tiss_w		varchar2(30);
ie_guia_autor_w		varchar2(30);
nr_seq_projeto_w	number(10);
	
begin

select 	ie_interno
into	ie_interno_w
from 	estagio_autorizacao 
where 	nr_sequencia = nr_seq_estagio_p;

if	(ie_interno_w = '5') then
	ie_tipo_tiss_w	:= '18';
elsif	(ie_interno_w = '70') then
	ie_tipo_tiss_w	:= '19';
end if;	
		
select 	TISS_OBTER_XML_PROJETO(cd_convenio_p, cd_estabelecimento_p, ie_tipo_tiss_w, sysdate)
into	nr_seq_projeto_w
from 	dual;

if	(nvl(nr_seq_projeto_w,0) = 0) then

	select 	TISS_OBTER_TIPO_GUIA_AUTOR(nr_sequencia_autor_p)  
	into	ie_guia_autor_w
	from 	dual;
	
	if	(ie_guia_autor_w = '2') then
		ie_tipo_tiss_w := '3';
	elsif	(ie_guia_autor_w = '8') then
		ie_tipo_tiss_w := '2';
	elsif	(ie_guia_autor_w = '1') then
		ie_tipo_tiss_w := '1';
	else
		ie_tipo_tiss_w := ie_tipo_autorizacao_p;
	end if;
	
	select 	TISS_OBTER_XML_PROJETO(cd_convenio_p, cd_estabelecimento_p, ie_tipo_tiss_w,sysdate)
	into	nr_seq_projeto_w
	from 	dual;	

end if;

nr_seq_projeto_p 	:= nr_seq_projeto_w;
ie_tipo_tiss_p		:= ie_tipo_tiss_w;

end TISS_OBTER_PROJETO_TISS_AUTOR;
/