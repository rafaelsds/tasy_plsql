create or replace
procedure pls_excluir_conferencia_sib
			(	nr_seq_lote_p	number) is 

qt_registro_sib_vinculado	number(10);
qt_registro_sib_segurado	number(10);
qt_registro_sib_encontrado	number(10);
qt_registro_sib_envio		number(10);
qt_retorno_sib_w		number(10);
				
begin

select	count(*)
into	qt_registro_sib_vinculado
from	pls_retorno_sib
where	nr_seq_segurado_vinculado is not null
and	nr_seq_lote_sib		= nr_seq_lote_p;

select	count(*)
into	qt_registro_sib_segurado
from	pls_retorno_sib
where	nr_seq_segurado is not  null
and	nr_seq_lote_sib		= nr_seq_lote_p;

select	count(*)
into	qt_registro_sib_encontrado
from	pls_retorno_sib
where	nr_seq_segurado_encontrado is not null
and	nr_seq_lote_sib		= nr_seq_lote_p;

select	count(*)
into	qt_registro_sib_envio
from	pls_retorno_sib
where	ie_enviar_sib	<> 'N'
and	nr_seq_lote_sib		= nr_seq_lote_p;
	
select	count(*)
into	qt_retorno_sib_w
from	pls_lote_retorno_sib	a,
	pls_retorno_sib		b,
	pls_sib_tasy_ans	c
where	c.nr_seq_retorno_sib	= b.nr_sequencia
and	b.nr_seq_lote_sib	= a.nr_sequencia
and	a.nr_sequencia		= nr_seq_lote_p;
	
if	(qt_registro_sib_vinculado > 0) or
	(qt_registro_sib_segurado > 0) or
	(qt_registro_sib_encontrado > 0) or
	(qt_registro_sib_envio > 0) or
	(qt_retorno_sib_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(262234, 'NR_SEQ_LOTE=' || nr_seq_lote_p);
	/* Mensagem: N�o � poss�vel excluir o lote NR_SEQ_LOTE! */
else
	delete	pls_retorno_sib
	where	nr_seq_lote_sib	= nr_seq_lote_p;
	
	delete	w_sib_conferencia
	where	nr_seq_lote_conf = nr_seq_lote_p;
	
	delete	pls_lote_retorno_sib
	where	nr_sequencia	= nr_seq_lote_p;
end if;
		
commit;

end pls_excluir_conferencia_sib;
/