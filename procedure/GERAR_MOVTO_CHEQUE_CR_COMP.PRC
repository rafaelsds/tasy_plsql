create or replace
procedure gerar_movto_cheque_cr_comp(	nr_seq_cheque_p		number,
					nr_seq_conta_banco_p	number,
					nr_seq_trans_financ_p	number,
					nm_usuario_p		varchar2,
					dt_desf_comp_p date default null) is
					
vl_cheque_w		number(15,2);
vl_adiant_banco_w	number(15,2);
cd_estabelecimento_w	number(10);
nr_seq_movto_w		number(15);
dt_cheque_w		date;
dt_compensacao_w	date;
nr_seq_conta_banco_w	number(10);
nr_seq_trans_financ_w	number(10);
nr_seq_movto_trans_w	number(10);
ie_acao_w		varchar2(1);
dt_transacao_w		date;
ie_dt_transacao_w	varchar2(1);
/* Projeto Multimoeda - Vari�veis */
vl_cheque_estrang_w	number(15,2);
vl_complemento_w	number(15,2);
vl_cotacao_w		cotacao_moeda.vl_cotacao%type;
cd_moeda_cheque_w	number(5);
cd_moeda_banco_w	number(5);
cd_moeda_empresa_w	number(5);

begin
		
if	(nr_seq_cheque_p is not null) then

	select	nvl(max(vl_cheque),0),
		max(cd_estabelecimento),
		max(nvl(DT_COMPENSACAO, dt_contabil)),
		max(dt_compensacao),
		max(vl_cheque_estrang),  -- Projeto Multimoeda - Busca os dados do cheque em moeda estrangeira.
		max(vl_cotacao),
		max(cd_moeda)
	into	vl_cheque_w,
		cd_estabelecimento_w,
		dt_cheque_w,
		dt_compensacao_w,
		vl_cheque_estrang_w,
		vl_cotacao_w,
		cd_moeda_cheque_w
	from	cheque_cr
	where	nr_seq_cheque	= nr_seq_cheque_p;
	
	/* Projeto Multimoeda - Busca a moeda padr�o da empresa. */
	select	obter_moeda_padrao_empresa(cd_estabelecimento_w,'E')
	into	cd_moeda_empresa_w
	from dual;
	
	obter_param_usuario(810,73,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_dt_transacao_w);
	
	/* se n�o tiver data de compensa��o, est� desfazendo a compensa��o */
	if	(dt_compensacao_w	is null) then

		select	max(a.nr_sequencia)
		into	nr_seq_movto_trans_w
		from	transacao_financeira b,
			movto_trans_financ a
		where	b.ie_acao		in (0,25)
		and	a.nr_seq_trans_financ	= b.nr_sequencia
		and	a.nr_seq_cheque		= nr_seq_cheque_p;

		if	(nr_seq_movto_trans_w	is not null) then

			select	max(a.nr_seq_trans_financ),
				max(a.nr_seq_banco),
				max(a.dt_transacao)
			into	nr_seq_trans_financ_w,
				nr_seq_conta_banco_w,
				dt_transacao_w
			from	movto_trans_financ a
			where	a.nr_sequencia	= nr_seq_movto_trans_w;

		end if;
		
		if (dt_desf_comp_p is not null) then
			dt_cheque_w := dt_desf_comp_p;
		end if;

		if	(nvl(ie_dt_transacao_w,'S ') = 'N') then
			dt_cheque_w	:= dt_transacao_w;
		end if;
		vl_cheque_w	:= nvl(vl_cheque_w,0) * -1;
		ie_acao_w	:= 'E';
		/* Projeto Multimoeda - Verifica se o cheque � em moeda estrangeira, caso positivo inverte o valor para o estorno.*/
		if (nvl(vl_cheque_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			vl_cheque_estrang_w := vl_cheque_estrang_w * -1;
		end if;

	else

		nr_seq_trans_financ_w	:= nr_seq_trans_financ_p;
		nr_seq_conta_banco_w	:= nr_seq_conta_banco_p;
		ie_acao_w		:= 'I';

	end if;

	if 	(nvl(nr_seq_conta_banco_w, 0) <> 0) and
		(nr_seq_trans_financ_w is not null) then

		/* Projeto Multimoeda - Busca a moeda do banco para realizar a consist�ncia com a moeda do cheque.
				Quando a moeda do cheque for diferente da moeda do banco a compensa��o n�o pode ser realizada,
				pois poder� ocasionar diferen�as no banco, por existirem transa��es com moedas diferentes. A conta 
				tem uma moeda definida e as transa��es devem ocorrer sempre na mesma moeda. Caso n�o exista moeda
				para o cheque ou para o banco ser� considerada a moeda padr�o da empresa.
				Consist�ncia deve ser realizada apenas na inclus�o da compensa��o. */
		if (ie_acao_w = 'I') then
			select	obter_moeda_padrao_empresa(nr_seq_conta_banco_w,'B')
			into	cd_moeda_banco_w
			from 	dual;
			if (nvl(cd_moeda_cheque_w,cd_moeda_empresa_w) <> nvl(cd_moeda_banco_w,cd_moeda_empresa_w)) then
				/* Desfaz a compensa��o do cheque pois as moedas s�o diferentes.*/
				atualizar_dt_compe_cheque(null, -- Data da compensa��o
							nr_seq_cheque_p, 
							'D', -- Desfazer compensa��o
							nm_usuario_p);
				-- O moeda do cheque � diferente da moeda da conta banc�ria selecionada.
				wheb_mensagem_pck.exibir_mensagem_abort(346805);
			end if;
		end if;
		
		select	movto_trans_financ_seq.nextval
		into	nr_seq_movto_w
		from	dual;

		/* Projeto Multimoeda - Verifica se o cheque � em moeda estrangeira, caso positivo calcula o complemento, caso contr�rio limpa as vari�veis.*/
		if (nvl(vl_cheque_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			vl_complemento_w := vl_cheque_w - vl_cheque_estrang_w;
		else
			vl_cheque_estrang_w := null;
			vl_complemento_w := null;
			vl_cotacao_w := null;
			cd_moeda_cheque_w := null;
		end if;
		
		insert into movto_trans_financ
			(nr_sequencia,
			dt_transacao,
			nr_seq_trans_financ,
			vl_transacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_cheque,
			nr_seq_banco,
			nr_lote_contabil,
			ie_conciliacao,
			vl_transacao_estrang,
			vl_complemento,
			vl_cotacao,
			cd_moeda)
		values	(nr_seq_movto_w,
			dt_cheque_w,
			nr_seq_trans_financ_w,
			vl_cheque_w,
			sysdate,
			nm_usuario_p,
			nr_seq_cheque_p,
			nr_seq_conta_banco_w,
			0,
			'N',
			vl_cheque_estrang_w,
			vl_complemento_w,
			vl_cotacao_w,
			cd_moeda_cheque_w);

		atualizar_transacao_financeira(cd_estabelecimento_w, nr_seq_movto_w, nm_usuario_p, ie_acao_w);

	end if;
end if;

end gerar_movto_cheque_cr_comp;
/
