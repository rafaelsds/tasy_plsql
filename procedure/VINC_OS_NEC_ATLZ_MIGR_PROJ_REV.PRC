create or replace
procedure vinc_os_nec_atlz_migr_proj_rev (
		nr_seq_os_p	number,
		cd_funcao_p	number,
		qt_min_prev_p	number,
		nm_usuario_p	varchar2) is

nr_seq_ativ_funcao_w		number(10,0);
nr_seq_ativ_os_w		number(10,0);
nr_seq_apres_funcao_w		number(15,0);
nr_seq_apres_os_w		number(15,0);
nr_seq_vinc_proj_w		number(10,0);

nr_seq_projeto_w		number(10,0);
ie_tasymed_w			varchar2(1);

cd_pessoa_programador_w		varchar2(10);
nm_usuario_programador_w	varchar2(15);

ie_existe_revisor_w		varchar2(1) := 'N';

cursor c01 is
select	nvl(pp.cd_pessoa_fisica,'0')
from	proj_equipe_papel pp,
	proj_equipe pe
where	pp.nr_seq_equipe = pe.nr_sequencia
and	pp.nr_seq_funcao = 44
and	pp.ie_funcao_rec_migr = 'R'
and	pe.nr_seq_equipe_funcao = 11
and	nvl(pp.ie_situacao,'A') = 'A'
and	pe.nr_seq_proj = nr_seq_projeto_w
order by
	pp.nr_seq_apres desc;
		
begin
if	(nr_seq_os_p is not null) and
	(cd_funcao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ie_tasymed_w := obter_se_funcao_tasymed(cd_funcao_p);
	
	if	(ie_tasymed_w = 'N') then
		begin
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_projeto_w
		from	proj_projeto
		where	cd_funcao = cd_funcao_p
		and	nr_seq_classif = 14;
		end;
	else
		begin
		nr_seq_projeto_w := 1644;
		end;
	end if;
	
	if	(nr_seq_projeto_w > 0) then
		begin
		select	proj_ordem_servico_seq.nextval
		into	nr_seq_vinc_proj_w
		from	dual;
			
		insert into proj_ordem_servico (
			nr_sequencia,
			nr_seq_proj,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ordem,
			ie_tipo_ordem)
		values (
			nr_seq_vinc_proj_w,
			nr_seq_projeto_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_os_p,
			'R');
			
		/*begin
		ie_existe_revisor_w := 'N';
		open c01;
		loop
		fetch c01 into cd_pessoa_programador_w;
		exit when c01%notfound;
			begin
			ie_existe_revisor_w := 'S';
			if	(cd_pessoa_programador_w <> '0') then
				begin
				nm_usuario_programador_w := obter_usuario_pf(cd_pessoa_programador_w);			
				if	(nm_usuario_programador_w is not null) then
					begin
					insert into man_ordem_servico_exec (
						nr_sequencia,
						nr_seq_ordem,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_exec,
						qt_min_prev,
						dt_ult_visao,
						nr_seq_funcao,
						dt_recebimento,
						nr_seq_tipo_exec)
					values (
						man_ordem_servico_exec_seq.nextval,
						nr_seq_os_p,
						sysdate,
						'Tasy',
						nm_usuario_programador_w,
						null, --qt_min_prev_p,
						null,
						null,
						null,
						2);
					end;
				end if;
				end;
			end if;
			end;
		end loop;
		close c01;
		exception
		when others then
			cd_pessoa_programador_w := '';
			if	(ie_existe_revisor_w = 'N') then
				begin
				raise_application_error(-20011,'O projeto ' || to_char(nr_seq_projeto_w) || ' n�o possui um recurso definido para atendimento das ordens de revis�o!#@#@');
				end;
			end if;
		end;*/		
		end;
	end if;
	end;
end if;
commit;
end vinc_os_nec_atlz_migr_proj_rev;
/