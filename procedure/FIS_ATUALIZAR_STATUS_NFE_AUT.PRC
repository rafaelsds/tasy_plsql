create or replace
procedure fis_atualizar_status_nfe_aut(	nr_seq_lote_p			lote_nfe_transmissao.nr_sequencia%type,
										nr_seq_transmissao_p	nfe_transmissao.nr_sequencia%type,
										ie_tipo_p				number,
										ie_status_p				varchar2,
										nm_usuario_p			varchar2) is 
/*
ie_tipo_p
1 -  Status do lote de transmiss�o
2 - Status da tranmiss�o

*/
begin

if	(ie_tipo_p = 1) then

	update	lote_nfe_transmissao
	set		ie_status_lote = ie_status_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_lote_p;
	
elsif (ie_tipo_p = 2) then

	update	nfe_transmissao
	set		ie_status_transmissao = ie_status_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_transmissao_p;
	
end if;

commit;

end fis_atualizar_status_nfe_aut;
/	