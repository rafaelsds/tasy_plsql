create or replace
procedure pls_lote_comunicacao_inter_web(	nr_seq_usu_prest_web_p		number,
						nr_seq_prestador_p		number,
						cd_prestador_p			varchar2,
						cd_cgc_p			varchar2,
						cd_cpf_p			varchar2,
						cd_versao_p			varchar2,
						ds_arquivo_p			varchar2,
						nm_usuario_p			varchar2,
						ie_origem_lote_p		varchar2,
						nr_seq_lote_p		out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Criar o lote de comuinicação de internação
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  X] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_lote_w			Number(10);
nr_seq_prestador_w		Number(10);

begin

select	pls_lote_comunicacao_inter_seq.nextval
into	nr_seq_lote_w
from	dual;

nr_seq_prestador_w := nr_seq_prestador_p;

if	( cd_prestador_p is not null and nr_seq_prestador_p is null ) then
	select	max(nr_sequencia)
	into	nr_seq_prestador_w
	from	pls_prestador
	where	cd_prestador = cd_prestador_p
	and	ie_situacao = 'A';	
elsif	( cd_cgc_p is not null ) then
	select	max(nr_sequencia)
	into	nr_seq_prestador_w
	from	pls_prestador
	where	cd_cgc = cd_cgc_p
	and	ie_situacao = 'A';
elsif	( cd_cpf_p is not null ) then
	select	max(a.nr_sequencia)
	into	nr_seq_prestador_w
	from	pls_prestador a,
		pessoa_fisica b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.nr_cpf = cd_cpf_p
	and	a.ie_situacao = 'A';
end if;

insert into pls_lote_comunicacao_inter( dt_recebimento, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_prestador,
					cd_prestador, nr_seq_usu_prest_web, ds_arquivo,
					nr_sequencia, ie_origem_lote, ie_status,
					nr_cpf_prestador, cd_cgc_prestador, cd_versao_tiss)
				values( sysdate, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_prestador_w,
					cd_prestador_p, nr_seq_usu_prest_web_p, ds_arquivo_p,
					nr_seq_lote_w, ie_origem_lote_p, 'R',
					cd_cpf_p, cd_cgc_p, cd_versao_p);


nr_seq_lote_p := nr_seq_lote_w;

commit;

end pls_lote_comunicacao_inter_web;
/