create or replace
procedure ptu_conferir_doc_fisico_cobr
		(	nr_seq_nota_cobr_p	number,
			nm_usuario_p		varchar2) is 

nr_seq_fatura_w			number(10);
qt_notas_doc_fisico_w		number(10);
qt_notas_conf_w			number(10);
ie_doc_fisico_conf_w		varchar2(1);
ie_necessita_doc_fisico_w	varchar2(1);

begin
select	max(nr_seq_fatura)
into	nr_seq_fatura_w
from	ptu_nota_cobranca
where	nr_sequencia		= nr_seq_nota_cobr_p;

select	count(*)
into	qt_notas_doc_fisico_w
from	ptu_nota_cobranca
where	nr_seq_fatura		= nr_seq_fatura_w
and	ie_necessita_doc_fisico = 'S';

if	(qt_notas_doc_fisico_w > 0) then
	update	ptu_fatura
	set	ie_doc_fisica_conferida	= 'N',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_fatura_w;
end if;

select	max(ie_doc_fisico_conf)
into	ie_doc_fisico_conf_w
from	ptu_nota_cobranca
where	nr_sequencia		= nr_seq_nota_cobr_p;

if	(ie_doc_fisico_conf_w = 'S') then
	update	ptu_nota_cobranca
	set	ie_doc_fisico_conf	= 'N',
		dt_conferencia_doc	= null,
		nm_usuario_doc_fisico	= null
	where	nr_sequencia		= nr_seq_nota_cobr_p;
else
	update	ptu_nota_cobranca
	set	ie_doc_fisico_conf	= 'S',
		dt_conferencia_doc	= sysdate,
		nm_usuario_doc_fisico	= nm_usuario
	where	nr_sequencia		= nr_seq_nota_cobr_p;
end if;	

select	count(*)
into	qt_notas_conf_w
from	ptu_nota_cobranca
where	nr_seq_fatura		= nr_seq_fatura_w
and	ie_doc_fisico_conf	= 'S'
and	ie_necessita_doc_fisico = 'S';

if	(qt_notas_doc_fisico_w = qt_notas_conf_w) and
	(qt_notas_doc_fisico_w > 0) then
	
	update	ptu_fatura
	set	ie_doc_fisica_conferida	= 'S',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_fatura_w;	
end if;

commit;

end ptu_conferir_doc_fisico_cobr;
/