create or replace
procedure GER_CHEQUE_ATUALIZA_DT_IMP(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 
begin
if	(nm_usuario_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	update	cheque
	set	dt_impressao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;	
commit;

end GER_CHEQUE_ATUALIZA_DT_IMP;
/