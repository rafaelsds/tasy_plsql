create or replace
procedure gravar_log_email_nfe(nm_usuario_p	varchar2,
			 nr_seq_nota_fiscal_p	number,
			 ds_destino_p		varchar2,
			 ie_tipo_regra_p	varchar2) is 

nr_sequencia_w	number(10);
begin

select 	nfe_log_email_seq.nextval
into	nr_sequencia_w
from	dual;

insert into nfe_log_email(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota_fiscal,
		dt_envio,
		ds_destino,
		ie_tipo_regra)
	values (
		nfe_log_email_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_nota_fiscal_p,
		sysdate,
		substr(ds_destino_p,1,4000),
		ie_tipo_regra_p);

commit;

end gravar_log_email_nfe;
/
