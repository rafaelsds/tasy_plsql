CREATE OR REPLACE
PROCEDURE Duplicar_exame_padrao	(nr_seq_grupo_p		Number,
								 nm_usuario_p		Varchar2) IS
					
nr_seq_grupo_w			Number(10);
nr_seq_exame_w			Number(10);
nr_seq_exame_ori_w		Number(10);
cd_item_w				Varchar2(3);
ds_item_w				Varchar2(50);

Cursor C01 is
	select	*
	from	med_exame_padrao
	where	cd_medico is null
	and	nr_seq_grupo = nr_seq_grupo_p
	order by nr_sequencia;	
	
Cursor C02 is
	select	cd_item,
		ds_item
	from	med_item_result_exame
	where	nr_seq_exame = nr_seq_exame_ori_w
	order by cd_item;

Cw01	C01%RowType;

BEGIN

select	med_grupo_exame_seq.nextval
into	nr_seq_grupo_w
from	dual;

Insert into med_grupo_exame(
	nr_sequencia,
	ds_grupo_exame,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apresent,
	DT_ATUALIZACAO_NREC,
	NM_USUARIO_NREC)
select	nr_seq_grupo_w,
	ds_grupo_exame||obter_desc_expressao(303214)/*' (C�pia)'*/,
	cd_estabelecimento,
	sysdate,
	nm_usuario_p,
	nr_seq_apresent,
	sysdate,
	nm_usuario_p
from	med_grupo_exame
where	nr_sequencia	= nr_seq_grupo_p;

open C01;
loop
fetch   C01 into
		Cw01;
exit	when	c01%notfound;
	begin
	
	select	med_exame_padrao_seq.nextval
	into	nr_seq_exame_w
	from	dual;
	
	
	Insert into med_exame_padrao(
		nr_sequencia	    ,
		IE_PEDIDO   	    ,           
		IE_RESULTADO        ,   
		DT_ATUALIZACAO      ,   
		NM_USUARIO          ,   
		CD_PROCEDIMENTO     ,   
		IE_ORIGEM_PROCED    ,   
		DS_EXAME            ,   
		DS_GRUPO            ,   
		CD_ESTABELECIMENTO  ,   
		IE_FORMATO_RESULTADO,   
		QT_DECIMAIS         ,   
		IE_PADRAO           ,   
		CD_EXAME_SIST_ORIG  ,   
		NR_SEQ_APRESENT     ,   
		NR_SEQ_GRUPO        ,   
		VL_PADRAO_MINIMO    ,   
		VL_PADRAO_MAXIMO    ,   
		DS_UNIDADE          ,   
		DS_CALCULO          ,   
		DS_REST_CALCULO     ,   
		DT_ATUALIZACAO_NREC ,   
		NM_USUARIO_NREC     ,
		ie_lado   	    ,
		DS_REDUZIDA_EXAME,
		NR_PROC_INTERNO,
		IE_SITUACAO)
	Values(	nr_seq_exame_w,
		Cw01.IE_PEDIDO   	 ,           
		Cw01.IE_RESULTADO        ,   
		sysdate			 ,
		NM_USUARIO_p        	 ,   
		Cw01.CD_PROCEDIMENTO     ,   
		Cw01.IE_ORIGEM_PROCED    ,   
		Cw01.DS_EXAME            ,   
		Cw01.DS_GRUPO            ,   
		Cw01.CD_ESTABELECIMENTO  ,   
		Cw01.IE_FORMATO_RESULTADO,   
		Cw01.QT_DECIMAIS         ,   
		Cw01.IE_PADRAO           ,   
		Cw01.CD_EXAME_SIST_ORIG  ,   
		Cw01.NR_SEQ_APRESENT     ,   
		NR_SEQ_GRUPO_w      	 ,   
		Cw01.VL_PADRAO_MINIMO    ,   
		Cw01.VL_PADRAO_MAXIMO    ,   
		Cw01.DS_UNIDADE          ,   
		Cw01.DS_CALCULO          ,   
		Cw01.DS_REST_CALCULO     ,   
		sysdate 	      	 ,   
		nm_usuario_p	      	 ,
		Cw01.ie_lado		 ,
		Cw01.DS_REDUZIDA_EXAME,
		Cw01.NR_PROC_INTERNO,
		nvl(Cw01.IE_SITUACAO,'A'));

	commit;	
	
	nr_seq_exame_ori_w	:= Cw01.nr_sequencia;
	
	open C02;
	loop
	fetch C02 into	
		cd_item_w,
		ds_item_w;
	exit when C02%notfound;
		begin
		Insert into med_item_result_exame(
			nr_sequencia,
			nr_seq_exame,
			cd_item,
			ds_item,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec
			)
		Values	(med_item_result_exame_seq.nextval,
			nr_seq_exame_w,
			cd_item_w,
			ds_item_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

END Duplicar_exame_padrao;
/