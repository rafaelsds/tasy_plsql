create or replace
procedure Atualizar_Dif_Diaria_EUP (	nr_seq_interno_p	number,
				ie_calcular_dif_diaria_p	varchar2,
				nm_usuario_p	varchar2) is
					
begin

update	atend_paciente_unidade 
set	ie_calcular_dif_diaria	= ie_calcular_dif_diaria_p,
	nm_usuario		= nm_usuario_p
where	nr_seq_interno		= nr_seq_interno_p;

commit;

end Atualizar_Dif_Diaria_EUP;
/