create or replace
procedure GRG_MARCAR_GUIA_ITEM_PAGO
			(nr_seq_guia_p	number,
			  nm_usuario_p	varchar2,
        		  ie_opcao_p varchar2,
			  ie_pago_p	varchar2) is

begin

if( ie_opcao_p = 'T') then
    update	lote_audit_hist_item
    set	ie_pago_convenio	= ie_pago_p,
        nm_usuario		= nm_usuario_p,
        dt_atualizacao		= sysdate
    where	nr_seq_guia		= nr_seq_guia_p;

elsif(ie_opcao_p = 'R') then
    update	 lote_audit_hist_item
    set	ie_pago_convenio	= ie_pago_p,
        nm_usuario		= nm_usuario_p,
        dt_atualizacao		= sysdate
    where	nr_seq_guia		= nr_seq_guia_p
    and vl_amenor > 0;

end if;
commit;

end GRG_MARCAR_GUIA_ITEM_PAGO;
/
