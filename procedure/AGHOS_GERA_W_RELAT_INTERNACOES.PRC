create or replace 
procedure Aghos_gera_w_relat_internacoes is

nr_internacao_w	number(10);
ds_sep_bv_w	varchar2(50);
ds_comando_w	varchar2(2000);
ie_situacao_w	varchar2(2); 
nm_usuario_w	varchar2(15);
nr_count_w	number(10);

pragma autonomous_transaction;

begin
nm_usuario_w := nvl(obter_usuario_ativo,'Tasy');

select	count(table_name)
into	nr_count_w
from	user_tables
where	upper(table_name) = 'W_RELAT_AGHOS_LOG_INTERNACOES';

if (nr_count_w	= 0) then
	exec_sql_dinamico(nm_usuario_w,'create table W_RELAT_AGHOS_LOG_INTERNACOES(	nm_usuario		varchar2(15),
											nr_internacao		varchar2(255),
											ie_tipo_internacao	varchar2(255))'); 
else
	exec_sql_dinamico(nm_usuario_w,'delete from W_RELAT_AGHOS_LOG_INTERNACOES where nm_usuario = ' || chr(39) || nm_usuario_w || chr(39));
end if;

ds_sep_bv_w  := obter_separador_bv;


/* Verifica solicitações canceladas */
ds_comando_w :=	'declare '||
		'	cancel_sai gsh_i_pkg_integra_sai.cancel_sai@tasy_aghos; '||
		'	nr_sequencia_w	number(10); '||
		'begin '||
		'begin '||
		'	nr_sequencia_w := :nr_internacao; '||		
		' '||
		'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_cancel_sai@tasy_aghos(cancel_sai); '||
		' '||
		'	if	(cancel_sai.first is not null) then '||
		'		for i in cancel_sai.first..cancel_sai.last loop '||
		' '||
		' 		insert into w_relat_aghos_log_internacoes(nm_usuario, nr_internacao, ie_tipo_internacao) values '||
		'			      ('||nm_usuario_w||', substr(cancel_sai(i).idf_internacao,1,255), ''Solicitações canceladas'' ); '||
		' '||
		'				commit; '||
		' '||
		'		end loop; '||
		'	end if; '||
		' commit; '||		
		' DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||			
		'   exception '||
		'   when others then '||
		'      rollback; ' ||
		'      DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||				
		'   end; '||	
		' '||
		'end;';

exec_sql_dinamico_bv('AGHOS_RELAT', ds_comando_w, 'nr_internacao='|| nr_internacao_w);

/* Verifica solicitações rejeitadas */
ds_comando_w :=	'declare '||
		'	rejsol_sai gsh_i_pkg_integra_sai.rejsol_sai@tasy_aghos; '||
		'	nr_sequencia_w	number(10); '||
		'begin '||
		'begin '||
		'	nr_sequencia_w := :nr_internacao; '||
		' '||
		'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_rejsol_sai@tasy_aghos(rejsol_sai); '||
		' '||
		'	if	(rejsol_sai.first is not null) then '||
		'		for i in rejsol_sai.first..rejsol_sai.last loop '||
		' '||
		' 		insert into w_relat_aghos_log_internacoes(nm_usuario, nr_internacao, ie_tipo_internacao) values '||
		'			      ('||nm_usuario_w||', substr(rejsol_sai(i).idf_internacao,1,255), ''Solicitações rejeitadas'' ); '||
		' '||
		'				commit; '||
		' '||
		'		end loop; '||
		'	end if; '||
		' commit; '||		
		' DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||			
		'   exception '||
		'   when others then '||
		'      rollback; ' ||
		'      DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||				
		'   end; '||	
		' '||
		'end;';

exec_sql_dinamico_bv('AGHOS_RELAT', ds_comando_w, 'nr_internacao='|| nr_internacao_w);

/* Verifica solicitações autorizadas clinicamente */
ds_comando_w :=	'declare '||
		'	autcli_sai gsh_i_pkg_integra_sai.autcli_sai@tasy_aghos; '||
		'	ds_erro_ww	varchar2(500); '||
		'	nr_sequencia_w	number(10); '||
		'begin '||
		'begin '||
		'	nr_sequencia_w := :nr_internacao; '||
		' '||
		'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_autcli_sai@tasy_aghos(autcli_sai); '||
		' '||
		'	if	(autcli_sai.first is not null) then '||
		'		for i in autcli_sai.first..autcli_sai.last loop '||
		' '||
		' 		insert into w_relat_aghos_log_internacoes(nm_usuario, nr_internacao, ie_tipo_internacao) values '||
		'			      ('||nm_usuario_w||', substr(autcli_sai(i).idf_internacao,1,255), ''Solicitações autorizadas clinicamente'' ); '||
		' '||
		'				commit; '||
		' '||
		'		end loop; '||
		'	end if; '||
		' commit; '||		
		' DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||			
		'   exception '||
		'   when others then '||
		'      rollback; ' ||
		'      DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||				
		'   end; '||	
		' '||
		'end;';
exec_sql_dinamico_bv('AGHOS_RELAT', ds_comando_w, 'nr_internacao='|| nr_internacao_w);


/* Verifica solicitações autorizadas */
ds_comando_w :=	'declare '||
		'	autori_sai gsh_i_pkg_integra_sai.autori_sai@tasy_aghos; '||
		'	ds_erro_ww	varchar2(500); '||
		'	nr_sequencia_w	number(10); '||
		'begin '||
		'begin '||
		'	nr_sequencia_w := :nr_internacao; '||
		' '||
		'	gsh_i_pkg_integra_sai.gsh_i_prc_intern_autori_sai@tasy_aghos(autori_sai); '||
		' '||
		'	if	(autori_sai.first is not null) then '||
		'		for i in autori_sai.first..autori_sai.last loop '||
		' '||
		' 		insert into w_relat_aghos_log_internacoes(nm_usuario, nr_internacao, ie_tipo_internacao) values '||
		'			      ('||nm_usuario_w||', substr(autori_sai(i).idf_internacao,1,255), ''Solicitações autorizadas'' ); '||
		' '||
		'				commit; '||
		' '||
		'			end if; '||
		'		end loop; '||
		'	end if; '||
		' commit; '||		
		' DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||			
		'   exception '||
		'   when others then '||
		'      rollback; ' ||
		'      DBMS_SESSION.CLOSE_DATABASE_LINK(''tasy_aghos''); ' ||				
		'   end; '||	
		' '||
		'end;';

exec_sql_dinamico_bv('AGHOS_RELAT', ds_comando_w, 'nr_internacao='|| nr_internacao_w);
	
commit;
	
end Aghos_gera_w_relat_internacoes;
/