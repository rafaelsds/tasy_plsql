CREATE OR REPLACE 
PROCEDURE Atualizar_tempo_aplic_medic(	nr_prescricao_p		number,
						nr_sequencia_p		number) IS

qt_min_aplicacao_w				number(4);
cd_material_w					number(6);
cd_diluente_w					number(6);
ie_via_aplicacao_w				varchar2(5);
cd_estabelecimento_w				number(4);
qt_idade_w					Number(15,2);
qt_peso_w				number(10,3);

BEGIN

/* Matheus OS50978 20/03/07*/
select	a.cd_estabelecimento,
	obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'M'),
	nvl(a.qt_peso,0)
into	cd_estabelecimento_w,
	qt_idade_w,
	qt_peso_w
from	pessoa_fisica b,
	prescr_medica a
where	a.nr_prescricao		= nr_prescricao_p
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

qt_idade_w	:= dividir(qt_idade_w,12);

select	cd_material,
	ie_via_aplicacao
into	cd_material_w,
	ie_via_aplicacao_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nr_sequencia_p;

select	nvl(max(cd_material),0)
into	cd_diluente_w
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
and	nr_sequencia_diluicao	= nr_sequencia_p
and	ie_agrupador		= 3;

if	(ie_via_aplicacao_w is null) then
	select	nvl(max(qt_minuto_aplicacao),0)
	into	qt_min_aplicacao_w
	from	material_diluicao
	where	cd_material		= cd_material_w
	and	cd_estabelecimento	= cd_estabelecimento_w
	and	qt_idade_w between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
	and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
	and	cd_diluente		= cd_diluente_w
	and	ie_reconstituicao	= 'N';
else
	select	nvl(max(qt_minuto_aplicacao),0)
	into	qt_min_aplicacao_w
	from	material_diluicao
	where	cd_material		= cd_material_w
	and	cd_estabelecimento	= cd_estabelecimento_w
	and	qt_idade_w between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
	and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
	and	cd_diluente		= cd_diluente_w
	and	((ie_via_aplicacao	= ie_via_aplicacao_w) or
		 (ie_via_aplicacao is null))
	and	ie_reconstituicao	= 'N';
end if;

if	(qt_min_aplicacao_w > 0) then
	update	prescr_material
	set	qt_min_aplicacao	= qt_min_aplicacao_w,
		qt_hora_aplicacao	= ''
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		= nr_sequencia_p;
else
	update	prescr_material
	set	qt_min_aplicacao	= '',
		qt_hora_aplicacao	= ''
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		= nr_sequencia_p;
end if;

commit;

END Atualizar_tempo_aplic_medic;
/