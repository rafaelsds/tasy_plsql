create or replace
procedure fis_gerar_reg_0015_icmsipi(nr_seq_controle_p	number) is 

/*REGISTRO 0015: DADOS DO CONTRIBUINTE SUBSTITUTO OU RESPONS�VEL PELO ICMS DESTINO*/

-- VARIABLES

nr_seq_icmsipi_0015_w fis_efd_icmsipi_0015.nr_sequencia%type;

qt_cursor_w number(10) := 0;
nr_vetor_w  number(10) := 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

/*Cursor que retorna as informa��es para o registro 0015 restringindo pela sequencia da nota fiscal*/  
cursor c_reg_0015 is
select	'0015' cd_reg,
	substr(decode(upper(c.nr_inscricao_estadual),'ISENTA', '', 'ISENTO', '', elimina_caractere_especial(c.nr_inscricao_estadual)), 1, 14) nr_ie_st,
	substr(obter_dados_pf_pj(null, b.cd_cgc, 'UF'),1,2) sg_uf_st
from	fis_efd_icmsipi_controle a,
	estabelecimento b,
	pessoa_juridica c				
where	b.cd_cgc = c.cd_cgc
and	a.nr_sequencia = nr_seq_controle_p
and	b.cd_estabelecimento = a.cd_estabelecimento;
		
		
/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_0015*/
type reg_c_reg_0015 is table of c_reg_0015%RowType;
vetReg0015 reg_c_reg_0015;

/*Cria��o do array com o tipo sendo da tabela eespecificada - FIS_EFD_ICMSIPI_0015 */
type registro is table of fis_efd_icmsipi_0015%rowtype index by pls_integer;
fis_registros_w registro;

begin
/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_reg_0015;
loop
fetch c_reg_0015 bulk collect	into vetReg0015 limit 1000;
	for i in 1 .. vetReg0015.Count loop

	begin

	/*Incrementa a variavel para o array*/
	qt_cursor_w:=	qt_cursor_w + 1;

	/*Busca da sequencia da tabela especificada - fis_efd_icmsipi_0015 */
	select fis_efd_icmsipi_0015_seq.nextval  into nr_seq_icmsipi_0015_w  from dual;

	/*Inserindo valores no array para realiza��o do forall posteriormente*/      
	fis_registros_w(qt_cursor_w).nr_sequencia 		:= nr_seq_icmsipi_0015_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao 		:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario 		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao_nrec 	:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario_nrec 		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).cd_reg 			:= '0015';	
	fis_registros_w(qt_cursor_w).nr_ie_st  			:= vetReg0015(i).nr_ie_st;
	fis_registros_w(qt_cursor_w).sg_uf_st  			:= vetReg0015(i).sg_uf_st;
	fis_registros_w(qt_cursor_w).nr_seq_controle 		:= nr_seq_controle_p;

	if (nr_vetor_w >= 1000) then
	begin
	/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_0015 */
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_0015 values fis_registros_w (i);

		nr_vetor_w := 0;
		fis_registros_w.delete;

		commit;

	end;
	end if;

	/*incrementa variavel para realizar o forall quando chegar no valor limite*/
	nr_vetor_w := nr_vetor_w + 1;

	end;
	end loop;
exit when c_reg_0015%notfound;
end loop;
close c_reg_0015;

if (fis_registros_w.count > 0) then
begin
/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
forall i in fis_registros_w.first .. fis_registros_w.last
insert into fis_efd_icmsipi_0015 values fis_registros_w (i);

fis_registros_w.delete;

commit;

end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;


end fis_gerar_reg_0015_icmsipi;
/
