create or replace
procedure proj_update_log_ultimo_recalc (
	nr_seq_cronograma_p	proj_cronograma.nr_sequencia%type,
	nm_usuario_p		usuario.nm_usuario%type) is

/*
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Objective: N/A
	------------------------------------------------------------------------
	Direct call: 
	[   ] Dictionary objects
	[ X ] Tasy (Delphi/Java/HTML5)
	[   ] Portal
	[   ] Reports
	[   ] Others:
	 -----------------------------------------------------------------------
	Attention points: N/A
	------------------------------------------------------------------------
	References:
		Tables: N/A
		Objects: N/A
	------------------------------------------------------------------------
	Called in:
		Java: N/A
		HTML5: N/A
		Object: N/A
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

begin

if	(nr_seq_cronograma_p is not null) then
	begin
	update	proj_cron_log_alt
	set	ie_status = 'C',
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_seq_cronograma = nr_seq_cronograma_p
	and	ie_status = 'P';

	commit;
	end;
end if;


end proj_update_log_ultimo_recalc;
/
