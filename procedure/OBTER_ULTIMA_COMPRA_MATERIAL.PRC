CREATE OR REPLACE
PROCEDURE Obter_Ultima_Compra_Material( 
					cd_estabelecimento_p		Number,
					cd_material_p			number,
					cd_unidade_medida_p  		Varchar2,
					ie_unidade_medida_p  		Varchar2,
					dt_inicio_p			date,
					nr_sequencia_p 		out   	Number,
					vl_ultima_Compra_p	out		Number) IS 

cd_unid_medida_w			Varchar2(30);
nr_sequencia_w			Number(10,0);
vl_ultima_compra_w		Number(15,4);
cd_material_estoque_w		number(6);

cursor c01 is
select a.nr_sequencia,
	dividir(nvl(a.vl_liquido,0), nvl(qt_item_nf,1))
from	nota_fiscal b,
	nota_fiscal_item a
where	a.nr_sequencia		= b.nr_sequencia
and	b.ie_acao_nf		= '1'
and	b.ie_situacao		= '1'
and	b.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_material		= cd_material_p
and	a.cd_unidade_medida_compra 	= cd_unid_medida_w
and	b.dt_entrada_saida		>= dt_inicio_p
order by 1;

BEGIN

if	(cd_unidade_medida_p is not null) then
	cd_unid_medida_w	:= cd_unidade_medida_p;
else
	select	decode(ie_unidade_medida_p,
		'C', substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,30),
		'E', substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,30), 
		substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30))
	into	cd_unid_medida_w
	from	material
	where	cd_material = cd_material_p;
end if;

nr_sequencia_p	:= 0;
vl_ultima_compra_p	:= 0;
OPEN C01;
LOOP
FETCH C01 into
	nr_sequencia_w,
	vl_ultima_compra_w;
EXIT WHEN C01%NOTFOUND;
	nr_sequencia_p	:= nr_sequencia_w;
	vl_ultima_compra_p	:= vl_ultima_compra_w;
END LOOP;
CLOSE C01;

if	(vl_ultima_compra_p = 0) and
	(nr_sequencia_p = 0) then

	select	cd_material_estoque
	into	cd_material_estoque_w
	from	material
	where	cd_material = cd_material_p;
	
	select	nvl(max(vl_preco_ult_compra),0)
	into	vl_ultima_compra_p
	from	saldo_estoque
	where	cd_material = cd_material_estoque_w
	and	cd_estabelecimento = cd_estabelecimento_p
	and	dt_mesano_referencia = PKG_DATE_UTILS.start_of(sysdate, 'month', 0);


end if;

END Obter_Ultima_Compra_Material;
/