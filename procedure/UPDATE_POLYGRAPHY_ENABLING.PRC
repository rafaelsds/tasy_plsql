CREATE OR REPLACE PROCEDURE update_polygraphy_enabling (
    nr_polygraphy_record_id   IN   integra_poligrafo_detalhe.nr_sequencia%TYPE,
    nm_usuario_nrec_p         IN   poligrafo_sit_just.nm_usuario%TYPE
) IS
BEGIN
    UPDATE integra_poligrafo_detalhe
    SET
        ie_situacao = 'A',
        nm_usuario = nm_usuario_nrec_p,
        dt_atualizacao = sysdate
    WHERE
        nr_sequencia = nr_polygraphy_record_id;

    COMMIT;
END update_polygraphy_enabling;
/
