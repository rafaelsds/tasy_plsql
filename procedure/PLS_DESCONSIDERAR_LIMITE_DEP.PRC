create or replace 
procedure pls_desconsiderar_limite_dep(	nr_seq_segurado_p		number,
					ds_motivo_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is


nr_seq_contrato_w		number(10);
ds_historico_w		varchar(255);
nr_seq_seg_historico_w	number(10);

begin

select	nr_seq_contrato
into	nr_seq_contrato_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

ds_historico_w	:= 'Desconsiderado o beneficiário do limite de dependência';

update	pls_segurado
set	ie_controla_dt_dependencia	= 'N'
where	nr_sequencia	= nr_seq_segurado_p;

select	pls_segurado_historico_seq.nextval
into	nr_seq_seg_historico_w
from	dual;
		
pls_gerar_segurado_historico(	nr_seq_segurado_p, '40', sysdate, ds_historico_w,
				ds_motivo_p, null, null, null,
				null, sysdate, null, null,
				null, null, null, null,
				nm_usuario_p, 'N');		

commit;	

end pls_desconsiderar_limite_dep;
/