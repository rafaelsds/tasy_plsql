create or replace
procedure pls_update_validacao_biometria(	nr_seq_guia_p			number,
						ie_tipo_validacao_biometria_p   number) is 

begin

update 	pls_guia_plano
set 	ie_tipo_validacao_biometria = ie_tipo_validacao_biometria_p
where 	nr_sequencia = nr_seq_guia_p;

commit;

end pls_update_validacao_biometria;
/
