create or replace 
procedure verifica_dif_saldo_lote(	dt_mesano_referencia_p	date,
				ie_atualiza_p		number) is

cd_estabelecimento_w		number(04,0)	:= 0;
cd_local_estoque_w		number(04,0)	:= 0;
cd_material_estoque_w		number(06,0)	:= 0;
cd_operacao_estoque_w		number(03,0)	:= 0;
ie_entrada_saida_w			varchar2(1);
qt_estoque_w			number(15,4);
dt_mesano_referencia_w		date;
dt_mesano_anterior_w		date;
qt_entrada_w			number(15,4);
qt_saida_w			number(15,4);
qt_saldo_estoque_w		number(15,4);
qt_saldo_anterior_w			number(15,4);
qt_dif_w				number(15,4);
ds_retorno_w			varchar2(40);
nr_seq_lote_w			number(10,0);

cursor c00 is
select	distinct
	a.cd_estabelecimento,
	a.cd_material_estoque,
	a.cd_local_estoque,
	a.nr_seq_lote_fornec
from	material b,
	operacao_estoque o,
	movimento_estoque a
where	a.dt_mesano_referencia 	= dt_mesano_referencia_w
and	a.cd_material_estoque	= b.cd_material
and	a.cd_operacao_estoque	= o.cd_operacao_estoque
and	o.ie_atualiza_estoque	= 'S'
and	b.ie_consignado 		= 0
and	a.nr_seq_lote_fornec is not null
and	(substr(obter_se_material_estoque_lote(a.cd_estabelecimento, a.cd_material_estoque),1,1) = 'S')
union
select	distinct
	a.cd_estabelecimento,
	a.cd_material,
	a.cd_local_estoque,
	a.nr_seq_lote
from	material b,
	saldo_estoque_lote a
where	a.dt_mesano_referencia	= dt_mesano_referencia_w
and	a.cd_material		= b.cd_material
and	b.ie_consignado 		= 0
and	a.nr_seq_lote is not null
and	(substr(obter_se_material_estoque_lote(a.cd_estabelecimento, a.cd_material),1,1) = 'S');

cursor c01 is
select	decode(a.cd_acao,1,a.qt_estoque,a.qt_estoque * -1),
	a.cd_operacao_estoque,
	b.ie_entrada_saida
from	operacao_estoque b,
	movimento_estoque a
where	a.dt_mesano_referencia	= dt_mesano_referencia_w
and	a.cd_material_estoque	= cd_material_estoque_w
and	a.cd_local_estoque		= cd_local_estoque_w
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	a.nr_seq_lote_fornec	= nr_seq_lote_w
and	a.cd_operacao_estoque	= b.cd_operacao_estoque
and	a.dt_processo	is not null
and	b.ie_atualiza_estoque	= 'S'
and	b.ie_consignado	= '0'
and	(substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_estoque_w),1,1) = 'S');

begin
dt_mesano_referencia_w	:= pkg_date_utils.start_of(dt_mesano_referencia_p,'MONTH',0);
dt_mesano_anterior_w  	:= pkg_date_utils.add_month(dt_mesano_referencia_w, -1,0);
obter_valor_dinamico('Truncate table w_saldo_estoque',ds_retorno_w);

open c00;
loop
	begin
	fetch c00 into
		cd_estabelecimento_w,
		cd_material_estoque_w,
		cd_local_estoque_w,
		nr_seq_lote_w;
	exit when c00%notfound;
        	begin
		qt_entrada_w	:= 0;
		qt_saida_w		:= 0;
		begin
		select	qt_estoque
		into	qt_saldo_estoque_w
		from	saldo_estoque_lote
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_local_estoque	= cd_local_estoque_w
		and	cd_material	= cd_material_estoque_w
		and	dt_mesano_referencia = dt_mesano_referencia_w
		and	nr_seq_lote	= nr_seq_lote_w
		and	(substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_estoque_w),1,1) = 'S');
		exception
			when others	 then
				qt_saldo_estoque_w	:= 0;
		end;
		begin
		select	qt_estoque
		into	qt_saldo_anterior_w
		from	saldo_estoque_lote
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_local_estoque	= cd_local_estoque_w
		and	cd_material	= cd_material_estoque_w
		and	dt_mesano_referencia = dt_mesano_anterior_w
		and	nr_seq_lote	= nr_seq_lote_w
		and	(substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_estoque_w),1,1) = 'S');
		exception
			when others	 then
				qt_saldo_anterior_w	:= 0;
		end;

		open c01;
		loop
		fetch c01 into
			qt_estoque_w,
			cd_operacao_estoque_w,
			ie_entrada_saida_w;
		exit when c01%notfound;
       		begin
			if	(ie_entrada_saida_w = 'S') then
				qt_saida_w	:= qt_saida_w + qt_estoque_w;
			else
				qt_entrada_w	:= qt_entrada_w + qt_estoque_w;
			end if;
			end;
		end loop;
		close c01;
		qt_dif_w	:=  	qt_saldo_estoque_w -
					(qt_saldo_anterior_w + qt_entrada_w - qt_saida_w);
		if	(qt_dif_w <> 0) then
			begin
			insert into w_saldo_estoque(
				cd_estabelecimento,
				cd_local_estoque,
				cd_material,
				dt_mesano_referencia,
				qt_estoque_atual,
				qt_estoque_anterior,
				qt_entrada,
				qt_saida,
				qt_diferenca)
			values(
				cd_estabelecimento_w,
				cd_local_estoque_w,
				cd_material_estoque_w,
				dt_mesano_referencia_w,
				qt_saldo_estoque_w,
				qt_saldo_anterior_w,
				qt_entrada_w,
				qt_saida_w,
				qt_dif_w);

			if	(ie_atualiza_p = 1555) then
				update	saldo_estoque_lote
				set	qt_estoque = qt_estoque - qt_dif_w
				where	cd_estabelecimento		= cd_estabelecimento_w
           				and	cd_local_estoque 		= cd_local_estoque_w
	           			and	cd_material           		= cd_material_estoque_w
           				and	dt_mesano_referencia  	= dt_mesano_referencia_w
				and	nr_seq_lote		= nr_seq_lote_w;
			end if;
			end;
			end if;
		end;
	end;
end loop;
close c00;

commit;

end verifica_dif_saldo_lote;
/