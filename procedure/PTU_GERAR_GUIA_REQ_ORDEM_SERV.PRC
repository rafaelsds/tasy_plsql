create or replace
procedure ptu_gerar_guia_req_ordem_serv(	nr_seq_req_ord_serv_p		number,
						ie_tipo_guia_p			varchar2,
						nr_seq_motivo_inclusao_p	number,
						ds_indicacao_clinica_p		varchar2,
						ds_observacao_p			varchar2,
						ie_consulta_urgencia_p		varchar2,
						ie_opcao_p			varchar2,
						sg_conselho_p			varchar2,
						qt_dia_solicitado_p		number,
						cd_estabelecimento_p		number,
						nm_usuario_p			Varchar2,
						nr_seq_guia_p		out	number,
						nr_seq_requisicao_p	out	number) is

nr_seq_conselho_w		conselho_profissional.nr_sequencia%type;
cd_senha_w			pls_guia_plano.cd_senha_externa%type;
dt_validade_w			pls_guia_plano.dt_valid_senha_ext%type;

begin

select	max(nr_sequencia)
into	nr_seq_conselho_w
from	conselho_profissional
where	sg_conselho = sg_conselho_p;

begin
select	nr_seq_origem,
	dt_validade
into	cd_senha_w,
	dt_validade_w
from	ptu_autorizacao_ordem_serv
where	nr_sequencia = (select  max(nr_sequencia)
			from	ptu_autorizacao_ordem_serv
			where	nr_transacao_solicitante = (	select	max(nr_transacao_solicitante)
								from	ptu_requisicao_ordem_serv
								where	nr_sequencia = nr_seq_req_ord_serv_p));
exception
when others then
cd_senha_w := '';
dt_validade_w := null;
end;								

if	(pls_obter_param_atend_geral('FA') = '1') then --OPS - Autoriza��es
	insert	into pls_guia_plano
		(nr_sequencia, dt_atualizacao, nm_usuario,
		dt_solicitacao, ie_tipo_guia, ds_indicacao_clinica,
		ds_observacao, ie_carater_internacao, ie_status,
		ie_tipo_processo, cd_estabelecimento, cd_senha_externa,
		dt_valid_senha_ext, nr_seq_conselho, qt_dia_solicitado,
		qt_dia_autorizado)
	values	(pls_guia_plano_seq.nextval, sysdate, nm_usuario_p,
		sysdate, ie_tipo_guia_p, ds_indicacao_clinica_p,
		ds_observacao_p, decode(ie_consulta_urgencia_p, 'S', 'U', 'N', 'E', 'E'), '2',
		'I', cd_estabelecimento_p, cd_senha_w,
		dt_validade_w, nr_seq_conselho_w, qt_dia_solicitado_p,
		qt_dia_solicitado_p) returning nr_sequencia into nr_seq_guia_p;

	pls_guia_gravar_historico(nr_seq_guia_p,2,'Criada guia atrav�s da fun��o OPS - Ordem de Servi�o Interc�mbio','',nm_usuario_p);

	ptu_gerar_autoriz_ordem_serv(nr_seq_req_ord_serv_p, nr_seq_guia_p, null, ie_opcao_p, nm_usuario_p);

elsif	(pls_obter_param_atend_geral('FA') = '2') then --OPS - Requisi��es para Autoriza��o
	insert	into pls_requisicao
		(nr_sequencia, dt_atualizacao, nm_usuario,
		dt_requisicao, ie_tipo_guia, ds_indicacao_clinica,
		ds_observacao, ie_consulta_urgencia, cd_estabelecimento,
		cd_senha_externa, dt_valid_senha_ext, nr_seq_conselho,
		qt_dia_solicitado, qt_dia_autorizado, nr_seq_motivo_inclusao)
	values	(pls_requisicao_seq.nextval, sysdate, nm_usuario_p,
		sysdate, ie_tipo_guia_p, ds_indicacao_clinica_p,
		ds_observacao_p, ie_consulta_urgencia_p, cd_estabelecimento_p,
		cd_senha_w, dt_validade_w, nr_seq_conselho_w,
		qt_dia_solicitado_p, qt_dia_solicitado_p, nr_seq_motivo_inclusao_p) returning nr_sequencia into nr_seq_requisicao_p;
	
	pls_requisicao_gravar_hist(nr_seq_requisicao_p,'L','Criada requisi��o atrav�s da fun��o OPS - Ordem de Servi�o Interc�mbio', null, nm_usuario_p);

	ptu_gerar_autoriz_ordem_serv(nr_seq_req_ord_serv_p, null, nr_seq_requisicao_p, ie_opcao_p, nm_usuario_p);
end if;

end ptu_gerar_guia_req_ordem_serv;
/
