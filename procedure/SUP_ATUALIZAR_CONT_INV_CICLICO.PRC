create or replace
procedure sup_atualizar_cont_inv_ciclico(
			nr_seq_item_inventario_p	number,
			qt_contagem_p		number,
			ie_contagem_p	varchar2,
			nm_usuario_p		Varchar2,
			ds_cons_inv_p out varchar2,
			ds_cons_item_inv_p out varchar2) is 

ie_contagem_atual_w varchar2(15);
ds_contagem_atual_w varchar2(50);
ds_contagem_parametro_w varchar2(50);
ie_existe_w	varchar2(1);
ie_bloqueado_w varchar2(1);
ie_aprovado_w varchar2(1);
ds_cons_inv_w	varchar2(4000) := '';
ds_cons_item_inv_w varchar2(4000) := '';
ds_material_w	varchar2(255);
cd_material_w	number(6);
begin
select 	a.ie_contagem_atual,
	decode(decode(ie_contagem_p,
			1,b.qt_contagem,
			2,b.qt_recontagem,
			3,b.qt_seg_recontagem,
			4,b.qt_terc_recontagem),
		null,'N','S'),
	decode(a.dt_bloqueio,null,'N','S'),
	decode(a.dt_aprovacao,null,'N','S'),
	substr(obter_valor_dominio(1701,a.ie_contagem_atual),1,50),
	substr(obter_valor_dominio(1701,ie_contagem_p),1,50),
	substr(obter_desc_material(b.cd_material),1,255),
	b.cd_material
into	ie_contagem_atual_w,
	ie_existe_w,
	ie_bloqueado_w,
	ie_aprovado_w,
	ds_contagem_atual_w,
	ds_contagem_parametro_w,
	ds_material_w,
	cd_material_w
from	inventario a,
	inventario_material b
where	a.nr_sequencia = b.nr_seq_inventario
and	b.nr_sequencia = nr_seq_item_inventario_p;

if ((ie_existe_w = 'N') and 
	(ie_bloqueado_w = 'S') and
	(ie_aprovado_w = 'N') and
	(ie_contagem_atual_w = ie_contagem_p)) then
	begin
	if (ie_contagem_atual_w = 1) then
		update 	inventario_material
		set	qt_contagem 	= qt_contagem_p,
			nm_usuario_contagem = nm_usuario_p,
			dt_contagem	= sysdate,
			nm_usuario  	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where	nr_sequencia = nr_seq_item_inventario_p;
	elsif (ie_contagem_atual_w = 2) then
		update 	inventario_material
		set	qt_recontagem 	= qt_contagem_p,
			nm_usuario_recontagem = nm_usuario_recontagem,
			dt_recontagem	= sysdate,
			nm_usuario  	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where	nr_sequencia = nr_seq_item_inventario_p;
	elsif (ie_contagem_atual_w = 3) then
		update 	inventario_material
		set	qt_seg_recontagem = qt_contagem_p,
			nm_usuario_seg_recontagem = nm_usuario_seg_recontagem, 
			dt_seg_recontagem	= sysdate,
			nm_usuario  	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where	nr_sequencia = nr_seq_item_inventario_p;
	elsif (ie_contagem_atual_w = 4) then
		update 	inventario_material
		set	qt_terc_recontagem = qt_contagem_p,
			nm_usuario_terc_recontagem = nm_usuario_p,
			dt_terc_recontagem	= sysdate,
			nm_usuario  	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where	nr_sequencia = nr_seq_item_inventario_p;
	end if;
	end;
else
	begin
	if (ie_bloqueado_w = 'N') then
		--ds_cons_inv_w := 'Os itens deste invent�rio n�o est�o bloqueados para contagem!';
		ds_cons_inv_w := wheb_mensagem_pck.get_texto(314202);
	end if;
	if (ie_aprovado_w = 'S') then
		--ds_cons_inv_w := 'A contagem deste invent�rio j� est� encerrada!';
		ds_cons_inv_w := wheb_mensagem_pck.get_texto(314203);
	end if;
	if (ie_contagem_atual_w <> ie_contagem_p) then
		--ds_cons_inv_w := 'No momento o invent�rio est� na ' || ds_contagem_atual_w || ', verifique!';
		ds_cons_inv_w := wheb_mensagem_pck.get_texto(314204, 'DS_CONTAGEM_ATUAL=' || ds_contagem_atual_w);
	end if;
	if (ie_existe_w = 'S') then
		--ds_cons_item_inv_w := 'A contagem do material ['|| cd_material_w || ']('||ds_material_w ||') j� est� definida!' || chr(13);
		ds_cons_item_inv_w := wheb_mensagem_pck.get_texto(314205, 'CD_MATERIAL=' || cd_material_w || ';'|| 'DS_MATERIAL=' || ds_material_w) || chr(13);
	end if;
	end;
end if;
ds_cons_inv_p := ds_cons_inv_w;
ds_cons_item_inv_p := ds_cons_item_inv_w;
commit;
end sup_atualizar_cont_inv_ciclico;
/