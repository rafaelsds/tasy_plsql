create or replace
procedure com_copiar_previsao_implant (
		nr_seq_previsao_p in out	number,
		nm_usuario_p			varchar2) is
		
nr_seq_previsao_w	number(10,0);
		
begin
if	(nr_seq_previsao_p is not null) then
	begin
	select	com_prev_implant_seq.nextval
	into	nr_seq_previsao_w
	from	dual;
	
	insert into com_prev_implant (
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		dt_previsao,
		nm_usuario_previsao,
		dt_prev_inic_impl,
		nr_horas_prev_impl,
		nr_meses_prev_impl,
		dt_liberacao,
		nm_usuario_liberacao,
		dt_cancelamento,
		nm_usuario_cancelamento,
		nr_seq_cliente,
		ie_origem_prev_implant,
		nr_seq_proposta)
	select	nr_seq_previsao_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		dt_prev_inic_impl,
		nr_horas_prev_impl,
		nr_meses_prev_impl,
		null,
		null,
		null,
		null,
		nr_seq_cliente,
		ie_origem_prev_implant,
		nr_seq_proposta
	from	com_prev_implant
	where	nr_sequencia = nr_seq_previsao_p;
	
	insert into com_prev_implant_prof (
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		nr_seq_prev_impl,
		nr_mes_ref_inicial,
		nr_mes_ref_final,
		nr_seq_nivel_prof,
		qt_profissionais,
		nr_horas_prof_mes,
		ie_resp_implantacao)
	select	com_prev_implant_prof_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_previsao_w,
		nr_mes_ref_inicial,
		nr_mes_ref_final,
		nr_seq_nivel_prof,
		qt_profissionais,
		nr_horas_prof_mes,
		ie_resp_implantacao
	from	com_prev_implant_prof
	where	nr_seq_prev_impl = nr_seq_previsao_p;
	
	nr_seq_previsao_p := nr_seq_previsao_w;
	end;
end if;
commit;
end com_copiar_previsao_implant;
/