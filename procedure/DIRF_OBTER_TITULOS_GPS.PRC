create or replace
procedure dirf_obter_titulos_gps(	nr_sequencia_p		number,
				nr_seq_gps_p		number,
				cd_estabelecimento_p 	number,	
				cd_darf_p		varchar2) is
				
cd_tributo_w		number(3);
contador_w		number(10);
ie_tipo_titulo_w	varchar2(6);
nr_titulo_w		number(10);
qt_registros_w		number(10);
vl_imposto_w		number(15,2);
vl_imposto_aux_w	number(15,2);
vl_rendimento_w		number(15,2);
				
vl_nota_w					number(15,2);
vl_total_vencimento_w				number(15,2) := 0;
vl_soma_rendimento_w				number(15,2) := 0;
vl_vencimento_bruto_w				number(15,2) := 0;
ie_origem_titulo_w				varchar2(6);
vl_titulo_w					number(15,2) := 0;
nr_seq_nota_fiscal_w				nota_fiscal.nr_sequencia%type;
nr_repasse_terceiro_w				repasse_terceiro.nr_repasse_terceiro%type;
dt_base_titulo_w				date;
ie_tipo_data_p					number(10);

Cursor C02 is
	select	rownum nr_linha,
		nr_titulo_pagar,
		vl_vencimento,
		dt_vencimento
from 	(select	nr_titulo_pagar,
		vl_vencimento,
		dt_vencimento
	from	nota_fiscal_venc a
	where	a.nr_sequencia = nr_seq_nota_fiscal_w
	order by
		vl_vencimento,
		dt_vencimento);

c02_w		c02%rowtype;
				
cursor C04 is
	select	i.cd_tributo,
		p.nr_titulo,
		nvl(obter_vl_mercadoria_nf_titulo(p.nr_titulo),p.vl_titulo),
		nvl(sum(obter_dados_tit_pagar(to_number(Obter_Titulo_Imposto(i.nr_sequencia)),'V')),0)
	from	titulo_pagar_imposto i,
		titulo_pagar p
	where	p.nr_titulo = i.nr_titulo
	and	SUBSTR(nvl(i.cd_Darf,obter_codigo_darf(i.cd_tributo,p.cd_estabelecimento,p.cd_cgc,p.cd_pessoa_fisica)),1,10) = cd_darf_p
	and	i.nr_titulo in (	select	p.nr_titulo_original
				from	titulo_pagar p
				where	p.nr_titulo in (	select	t.nr_titulo
							from	gps g,
								gps_titulo t
							where	g.nr_sequencia = t.nr_seq_gps
							and	g.nr_sequencia = nr_seq_gps_p
							and	g.cd_estabelecimento = cd_estabelecimento_p
							and	g.cd_pagamento = cd_darf_p))
	group by i.cd_tributo, p.nr_titulo, nvl(obter_vl_mercadoria_nf_titulo(p.nr_titulo),p.vl_titulo);

begin

open C04;
loop
fetch C04 into	
	cd_tributo_w,
	nr_titulo_w,
	vl_rendimento_w,
	vl_imposto_W;
exit when C04%notfound;
	begin
		
		select	max(ie_tipo_data)
		into	ie_tipo_data_p
		from	dirf_regra_tributo 
		where	cd_tributo = cd_tributo_w
		and	cd_darf = cd_darf_p;
		
		
		begin
		select	ie_origem_titulo
		into	ie_origem_titulo_w
		from	titulo_pagar
		where	nr_titulo = nr_titulo_w;
		exception
		when others then
			ie_origem_titulo_w := -1;
		end;
		
		vl_rendimento_w := vl_titulo_w;
		
		if	(ie_origem_titulo_w = 1) then -- Nota Fiscal
			begin			
			begin
			-- Pegar o valor da nota bruto menos os descontos
			select	nvl(a.vl_mercadoria,0) - nvl(a.vl_descontos,0)
			into	vl_nota_w
			from	nota_fiscal a
			where	a.nr_sequencia = nr_seq_nota_fiscal_w;
			
			-- Soma dos vencimentos e quantidade
			select	count(*),
				sum(vl_vencimento)
			into	qt_registros_w,
				vl_total_vencimento_w
			from	nota_fiscal_venc a
			where	a.nr_sequencia = nr_seq_nota_fiscal_w;

			-- Se s� possui um vencimento ent�o o valor que deve ir � o valor da mercadoria menos os descontos
			if	(qt_registros_w = 1) then
				begin
				vl_rendimento_w := vl_nota_w;
				end;
			-- Se existe mais de um vencimento, ent�o deve ratear o valor de mercadoria menos os descontos, de acordo com o valor de vencimento, e no �ltimo vencimenteo arredontar para fechar o valor total da mercadoria
			elsif	(qt_registros_w > 1) then
				begin
				open C02;
				loop
				fetch C02 into	
					c02_w;
				exit when C02%notfound;
					begin
					vl_vencimento_bruto_w := (vl_nota_w * c02_w.vl_vencimento) / vl_total_vencimento_w;
					vl_soma_rendimento_w := vl_soma_rendimento_w + vl_vencimento_bruto_w;
					-- Se for o t�tulo do vencimento, ent�o entra pra pegar o valor 
					if	(c02_w.nr_titulo_pagar = nr_titulo_w) then
						begin
						-- Se for o �ltimo vencimento da parada, ent�o tem que arredondar o valor, para mais ou para menos, para adequar aos outros valores j� gerados
						if	(qt_registros_w = c02_w.nr_linha) then
							begin
							-- Aqui a conta '(vl_nota_w- vl_soma_rendimento_w)' deve retornar 1 , 2, -1 ou -2 centavos, do contr�rio tem alguma coisa errada
							vl_rendimento_w := vl_vencimento_bruto_w + (vl_nota_w - vl_soma_rendimento_w);
							end;
						else
							begin
							vl_rendimento_w := vl_vencimento_bruto_w;
							end;
						end if;
						-- Se encontrou o registro, ent�o cai fora do c01
						exit;
						end;
					end if;
					end; -- end do c01
				end loop;
				close C02;
				end;
			end if;
			
			exception
			when others then
				vl_rendimento_w := vl_titulo_w;
			end;
			end;
		elsif	(ie_origem_titulo_w = 3) then -- Repasse
			begin
			select	a.nr_repasse_terceiro
			into	nr_repasse_terceiro_w
			from	titulo_pagar a
			where	a.nr_titulo = nr_titulo_w;
			
			begin
			select	vl_vencimento
			into	vl_rendimento_w
			from	titulo_pagar a,
				repasse_terceiro_venc b
			where	a.nr_repasse_terceiro = b.nr_repasse_terceiro
			and	a.nr_titulo = b.nr_titulo
			and	a.nr_titulo = nr_titulo_w;			
			exception
			when others then
				vl_rendimento_w := vl_titulo_w;
			end;
			
			end;
		elsif	(ie_origem_titulo_w = 20) then -- OPS - Pagamento de produ��o m�dica
			begin
			begin
			vl_rendimento_w	:= pls_obter_valor_producao_med(null, nr_titulo_w);
			exception
			when others then
				vl_rendimento_w := vl_titulo_w;
			end;
			end;
		end if;
		
		if	(ie_tipo_data_p = 1) then -- Pega pela data de emiss�o
			select	max(p.dt_emissao)
			into	dt_base_titulo_w
			from	titulo_pagar	p
			where	nr_titulo	= nr_titulo_w;
		elsif	(ie_tipo_data_p = 4) then -- Pega pela data cont�bil
			select	max(n.dt_emissao)
			into	dt_base_titulo_w
			from	nota_fiscal n,
				titulo_pagar	p
			where	p.nr_seq_nota_fiscal = n.nr_sequencia
			and	p.nr_titulo	= nr_titulo_w;
		elsif	(ie_tipo_data_p = 2) then -- Pega pela data cont�bil
			select	max(p.dt_contabil)
			into	dt_base_titulo_w
			from	titulo_pagar	p
			where	nr_titulo	= nr_titulo_w;
		elsif	(ie_tipo_data_p = 3) then -- Pega pela data de liquida��o
			select	max(p.dt_liquidacao)
			into	dt_base_titulo_w
			from	titulo_pagar	p
			where	nr_titulo	= nr_titulo_w;
		elsif	(ie_tipo_data_p = 5) then -- Pega pela data de vencimento
			select	max(p.dt_vencimento_atual)
			into	dt_base_titulo_w
			from	titulo_pagar	p
			where	nr_titulo	= nr_titulo_w;
		/*elsif	(ie_tipo_data_p = 6) then -- Pega pela data de vencimento do tributo
			select	max(i.dt_imposto)
			into	dt_base_titulo_w
			from	titulo_pagar_imposto	i
			where	i.nr_titulo	= nr_titulo_w
			and	i.cd_tributo	= cd_tributo_p;
		*/
		end if;
	
	insert into dirf_titulo_pagar	(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_lote_dirf,
					nr_titulo,
					vl_rendimento,
					vl_imposto,
					cd_tributo,
					cd_darf,
					ie_origem,
					dt_base_titulo)
			values	(	dirf_titulo_pagar_seq.Nextval,
					sysdate,
					'Tasy',
					sysdate,
					'Tasy',
					nr_sequencia_p,
					nr_titulo_w,
					vl_rendimento_w,
					nvl(vl_imposto_w,0),
					cd_tributo_w,
					cd_darf_p,
					'S',
					dt_base_titulo_w);
	
	contador_w := contador_w + 1;
			
	if (mod(contador_w, 100) = 0) then
		commit;
	end if;
	
	end;
end loop;
close C04;

commit;

end dirf_obter_titulos_gps;
/
