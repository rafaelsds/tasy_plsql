create or replace procedure
insert_pixeon_laudo_pdf(nr_acesso_dicom_p varchar2, nr_seq_laudo_p number, 
                          ds_laudo_p long, cd_pessoa_fisica_p varchar2, 
                          nm_usuario_p varchar2, cd_procedimento_p varchar2, 
                          nr_prescricao_p varchar2, ie_valida_param char, cd_estabelecimento_p number) is
                          
ie_continua_w  char;
 
begin
  begin 
    ie_continua_w := 'S';
    
    if (ie_valida_param = 'S') then
      select  nvl(ie_pdf_pixeon,'N') ie_pdf_pixeon
      into ie_continua_w
      from   parametro_integracao_pacs
      where  cd_estabelecimento  = cd_estabelecimento_p;
    end if;
    
    if (ie_continua_w = 'S') then
      delete from laudo_paciente_pdf_serial
      where nr_seq_laudo = nr_seq_laudo_p;
      
      insert into laudo_paciente_pdf_serial
      values (laudo_paciente_pdf_serial_seq.nextval, sysdate, nm_usuario_p, sysdate, 
              nm_usuario_p, nr_acesso_dicom_p, nr_seq_laudo_p, ds_laudo_p, 
              cd_pessoa_fisica_p, cd_procedimento_p, null, null, null, nr_prescricao_p);
      commit;        
    end if;
  end;
end insert_pixeon_laudo_pdf;
/