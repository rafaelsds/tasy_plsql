create or replace
procedure pls_alterar_validade_carteira
		(	nr_seq_segurado_p	number,
			nr_seq_carteira_p	number,
			dt_validade_p		date,
			ie_tipo_alteracao_p	varchar2,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number) is

/*Utilizada nas funcoes OPS - Gestao de Contratos e OPS - Contratos de Intercambio*/

/* ie_tipo_alteracao_p
	0 - Somenente beneficiario
	1 - Beneficiario mais os depedentes
	2 - Todos beneficiarios do contrato
*/

dt_inicio_vigencia_w		date;
cd_usuario_plano_w		varchar2(30);
nr_seq_carteira_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
ds_trilha1_w			varchar2(255);
ds_trilha2_w			varchar2(255);
ds_trilha3_w			varchar2(255);
ds_trilha_qr_code_w		varchar2(255);
ie_inadimplencia_via_adic_w	varchar2(1);
dt_sysdate_trunc_dd_w		date;
cd_guncao_ativa_w		number(10)	:= wheb_usuario_pck.get_cd_funcao;
nr_seq_pagador_w		number(10);
qt_inadimplencia_w		number(10);
nm_beneficiario_w		varchar2(255);
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;

Cursor C01 is
	select	distinct nr_sequencia,
		ie_tipo_segurado
	from	pls_segurado
	where	((nvl(ie_tipo_alteracao_p,0) in (0,1) and nr_sequencia = nr_seq_segurado_p) or
		(ie_tipo_alteracao_p = 2 and nr_seq_contrato = nr_seq_contrato_w))
	and	(((dt_cancelamento is null or dt_cancelamento > sysdate) and (dt_rescisao is null or dt_rescisao > sysdate)) or (nvl(ie_tipo_alteracao_p,0) = 0))
	union
	select	distinct nr_sequencia,
		ie_tipo_segurado
	from	pls_segurado
	where	ie_tipo_alteracao_p = 1
	and 	nr_seq_titular = nr_seq_segurado_p
	and	(dt_cancelamento is null or dt_cancelamento > sysdate)
	and	(dt_rescisao is null or dt_rescisao > sysdate);

Cursor C02 is
	select	max(nr_sequencia)
	from	pls_segurado_carteira
	where	nr_seq_segurado = nr_seq_segurado_w;

begin

if	(nvl(nr_seq_carteira_p,0) <> 0) then
	nr_seq_carteira_w	:= nr_seq_carteira_p;
else
	begin
	select	nr_seq_contrato
	into	nr_seq_contrato_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
	exception
	when others then
		nr_seq_contrato_w := null;
	end;
	
	open C01;
	loop
	fetch C01 into
		nr_seq_segurado_w,
		ie_tipo_segurado_w;
	exit when C01%notfound;
		begin
		
		if	(cd_guncao_ativa_w = 1273) then
			select	obter_valor_param_usuario(1273,5,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p)
			into	ie_inadimplencia_via_adic_w
			from	dual;
		elsif	(cd_guncao_ativa_w = 1202) then
			select	max(a.ie_inadimplencia_via_adic)
			into	ie_inadimplencia_via_adic_w
			from	pls_contrato_pagador	a,
				pls_segurado		b
			where	b.nr_seq_pagador	= a.nr_sequencia
			and	b.nr_sequencia		= nr_seq_segurado_w;
		end if;
		
		if	(ie_inadimplencia_via_adic_w = 'S') then
			dt_sysdate_trunc_dd_w	:= trunc(sysdate,'dd');
			
			select	max(nr_seq_pagador)
			into	nr_seq_pagador_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_w;
			
			select	count(1)
			into	qt_inadimplencia_w
			from	pls_mensalidade a,
				pls_mensalidade_segurado b,
				pls_segurado c,
				pls_contrato d,
				pls_contrato_pagador e,
				titulo_receber f
			where	a.nr_sequencia = b.nr_seq_mensalidade
			and	c.nr_sequencia = b.nr_seq_segurado
			and	d.nr_sequencia = c.nr_seq_contrato
			and	e.nr_sequencia = a.nr_seq_pagador
			and	a.nr_sequencia = f.nr_seq_mensalidade(+)
			and	c.nr_seq_contrato = nr_seq_contrato_w
			and	e.nr_sequencia = nr_seq_pagador_w
			and	f.dt_pagamento_previsto < dt_sysdate_trunc_dd_w
			and	f.ie_situacao = '1';
			
			if	(qt_inadimplencia_w > 0) then
				select	b.nm_pessoa_fisica
				into	nm_beneficiario_w
				from	pessoa_fisica	b,
					pls_segurado	a
				where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
				and	a.nr_sequencia = nr_seq_segurado_w;
				
				wheb_mensagem_pck.exibir_mensagem_abort(277982, 'NM_BENEFICIARIO=' || nm_beneficiario_w);
			end if;
		end if;
		
		open C02;
		loop
		fetch C02 into
			nr_seq_carteira_w;
		exit when C02%notfound;
			begin
			
			insert into pls_segurado_cart_ant
				(	nr_sequencia, nm_usuario,
					dt_atualizacao, cd_usuario_ant, dt_validade,
					dt_inicio_vigencia, nr_seq_segurado,
					dt_alteracao, ds_observacao, ie_status_carteira,
					nm_usuario_solicitacao, dt_solicitacao, ds_trilha_qr_code,
					ds_trilha1, ds_trilha2, ds_trilha3,
					dt_desbloqueio, nm_usuario_desbloqueio, ie_tipo_desbloqueio)
				select	pls_segurado_cart_ant_seq.nextval, nm_usuario_p,
					sysdate, cd_usuario_plano, dt_validade_carteira,
					dt_inicio_vigencia, nr_seq_segurado,
					sysdate, ds_observacao, 'P',
					nm_usuario_solicitante, dt_solicitacao, ds_trilha_qr_code,
					ds_trilha1, ds_trilha2, ds_trilha3,
					dt_desbloqueio, nm_usuario_desbloqueio, ie_tipo_desbloqueio
				from	pls_segurado_carteira
				where	nr_sequencia	= nr_seq_carteira_w;
			
			update	pls_segurado_carteira
			set	dt_validade_carteira 	= dt_validade_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				nm_usuario_solicitante	= nm_usuario_p,
				dt_solicitacao		= sysdate,
				ds_observacao		= wheb_mensagem_pck.get_texto(1127391)
			where	nr_sequencia		= nr_seq_carteira_w;
			
			/*Alterar o estagio da carteira*/
			pls_alterar_estagios_cartao(nr_seq_carteira_w,sysdate,1,cd_estabelecimento_p,nm_usuario_p);
			
			/*As trilhas devem ser alteradas logo depois de alterar a via da carteira*/
			pls_obter_trilhas_cartao(nr_seq_segurado_w, ds_trilha1_w, ds_trilha2_w, ds_trilha3_w, ds_trilha_qr_code_w, nm_usuario_p);
			
			update	pls_segurado_carteira
			set	ds_trilha1		= ds_trilha1_w,
				ds_trilha2		= ds_trilha2_w,
				ds_trilha3		= ds_trilha3_w,
				ds_trilha_qr_code	= ds_trilha_qr_code_w
			where	nr_sequencia 		= nr_seq_carteira_w;
			
			end;
		end loop;
		close C02;
		
		-- Se for beneficiario de pericia medica, precisa reativar o mesmo, pois no encerramento da analise para a adesao e preenchida a data de rescisao
		if	(ie_tipo_segurado_w = 'P') then
			update	pls_segurado
			set	dt_rescisao		= dt_validade_p,
				dt_limite_utilizacao	= dt_validade_p
			where	nr_sequencia	= nr_seq_segurado_w
			and	dt_rescisao is not null;
		end if;
		end;
	end loop;
	close C01;
end if;

commit;

end pls_alterar_validade_carteira;
/