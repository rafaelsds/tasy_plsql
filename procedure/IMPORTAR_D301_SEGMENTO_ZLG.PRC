create or replace procedure importar_d301_segmento_zlg(	nr_seq_dataset_p	    number,
                                      				ds_conteudo_zlg_p	   varchar2,
                                       				nm_usuario_p	   varchar2) is 
 ds_conteudo_zlg_w    varchar2(4000);
begin
    
  if  (substr(ds_conteudo_zlg_p,1,3) = 'ZLG') then 
        
        ds_conteudo_zlg_w:=replace(ds_conteudo_zlg_p,'''',''); 
        
        insert into d301_segmento_zlg 
           (nr_sequencia, 
            dt_atualizacao, 
            nm_usuario, 
            dt_atualizacao_nrec, 
            nm_usuario_nrec, 
            vl_coparticipacao, 
            nr_seq_301_indic_pag, 
            nr_seq_dataset, 
            nr_seq_dataset_ret)  
       values(d301_segmento_zlg_seq.nextval,
              sysdate,
              nm_usuario_p,
              sysdate,
              nm_usuario_p,
              obter_valor_separador(ds_conteudo_zlg_w,2,'+'), 
              obter_seq_valor_301('C301_15_INDIC_PAGAMENTO','IE_INDICACAO',obter_valor_separador(ds_conteudo_zlg_w,3,'+')), 
              null,
              nr_seq_dataset_p);
     commit;   
  end if;
        

end importar_d301_segmento_zlg;
/