create or replace
procedure validate_administered_dose(	qt_dose_adm_p			number,
					nr_seq_mat_cpoe_p		number,
					nr_atendimento_p		number,
					cd_material_p			number,
					dt_horario_p			date,
					ds_msg_p		out 	varchar2) is

qt_dose_w 					number(18,6);
qt_dose_conv_w				number(18,6);
qt_dose_limite_dia_cpoe_w 	number(18,6);
cd_unid_med_limit_w			varchar2(30);

begin

if (nvl(qt_dose_adm_p, 0) > 0) then

	select	max(obter_conversao_unid_med_cons(cd_material,cd_unidade_medida,qt_dose_maxima)),
			max(cd_unidade_medida)
	into	qt_dose_limite_dia_cpoe_w,
			cd_unid_med_limit_w
	from	cpoe_material
	where	nr_sequencia = nr_seq_mat_cpoe_p;
	
	qt_dose_conv_w := obter_conversao_unid_med_cons(cd_material_p,cd_unid_med_limit_w,qt_dose_adm_p);

	qt_dose_w	:= qt_dose_conv_w + obter_dose_medic_periodo(nr_atendimento_p,cd_material_p,dt_horario_p-1,dt_horario_p);

	if	(qt_dose_limite_dia_cpoe_w > 0) and
		(qt_dose_limite_dia_cpoe_w < nvl(qt_dose_w,0)) then
		ds_msg_p	:= obter_desc_expressao(970215);
	end if;
	
end if;

end validate_administered_dose;
/
