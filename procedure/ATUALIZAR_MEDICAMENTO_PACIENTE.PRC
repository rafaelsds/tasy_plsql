create or replace
procedure atualizar_medicamento_paciente(nr_prescricao_p	number,
					 nr_sequencia_p		number) is

begin
/* Atualiza a PRESCR_MATERIAL o campo IE_MEDICACAO_PACIENTE para "S" quando o mesmo estiver com "N" */
update	prescr_material
set	ie_medicacao_paciente = 'S'
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia = nr_sequencia_p
and	nvl(ie_medicacao_paciente,'N') = 'N';
commit;
end atualizar_medicamento_paciente;
/