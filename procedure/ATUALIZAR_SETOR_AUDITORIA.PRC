create or replace
procedure atualizar_setor_auditoria(	nr_sequencia_p	Number,
				ie_tipo_item_p		Number,
				nr_seq_atepacu_p	Number,
				nr_seq_motivo_p		Number) is 

ie_altera_item_repasse_w	varchar2(1) := 'S';
nm_usuario_w			usuario.nm_usuario%type := wheb_usuario_pck.Get_Nm_usuario;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
begin


obter_param_usuario(1116,189,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_altera_item_repasse_w);


if	(ie_tipo_item_p = 1) then
	update	auditoria_propaci a
	set	a.nr_seq_atepacu_ajuste = nr_seq_atepacu_p,
		a.ie_tipo_auditoria = 'S',
		a.nr_seq_motivo = decode(nvl(nr_seq_motivo_p,0), 0, a.nr_seq_motivo, nr_seq_motivo_p),
		a.nm_usuario	= nm_usuario_w
	where	nr_sequencia = nr_sequencia_p
	and	not exists(	select	1
				from	procedimento_repasse x
				where	x.nr_seq_procedimento = a.nr_seq_propaci
				and	nvl(ie_altera_item_repasse_w,'S') = 'N');
	
else
	update	auditoria_matpaci a
	set	a.nr_seq_atepacu_ajuste = nr_seq_atepacu_p,
		a.ie_tipo_auditoria 	= 'S',
		a.nr_seq_motivo 	= decode(nvl(nr_seq_motivo_p,0), 0, a.nr_seq_motivo, nr_seq_motivo_p),
		a.nm_usuario		= nm_usuario_w
	where	a.nr_sequencia 		= nr_sequencia_p
	and	not exists(	select	1
				from	material_repasse x
				where	x.nr_seq_material = a.nr_seq_matpaci
				and	nvl(ie_altera_item_repasse_w,'S') = 'N');
		
end if;

commit;

end atualizar_setor_auditoria;
/
