create or replace 
procedure  atualizar_horario_participante(
				nr_seq_participante_p		mprev_participante.nr_sequencia%type,
				ds_horario_p		hdm_horario_preferencial.ds_horario%type,
				nm_usuario_p		varchar2) is
				
Begin
	
if (nr_seq_participante_p is not null) then

	UPDATE mprev_participante
	SET ds_horario_preferencial = ds_horario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where nr_sequencia = nr_seq_participante_p;
	
	commit;

end if;
	

end atualizar_horario_participante;
/
