create or replace
procedure registrar_observacao_saps_js(	ds_observacao_p		varchar2,
					nr_sequencia_p		number) is
					
begin

if 	(nr_sequencia_p is not null) then
	begin
		
	update 	pe_prescr_item_result 
	set 	ds_observacao 	= substr(ds_observacao_p,1,1000)
	where 	nr_sequencia 	= nr_sequencia_p;
		
	end;
end if;
	
end registrar_observacao_saps_js;
/
