create or replace
procedure lab_atualizar_etapa_amostra(nr_prescricao_p 	number,
					 ie_status_atend_p	number,
 					 nr_seq_amostra_p	number,
					 sql_item_p		varchar2,
					 nm_usuario_p		varchar2) is
nr_seq_proc_mat_item_w	number(10);
nr_seq_prescr_w		number(10);
qt_etapa_amostra_w	number(3);
qt_total_amostra_w	number(3);
Ie_data_amostra_w	varchar2(1);
cd_estabelecimento_w	number(4);
ie_atualiza_dt_coleta_dist_w	varchar(1);

cursor  c01 is
select 	nr_sequencia,
	nr_seq_prescr
from 	prescr_proc_mat_item
where	nr_prescricao = nr_prescricao_p
and	NR_SEQ_PRESCR_PROC_MAT = nr_seq_amostra_p
and	' ' || sql_item_p || ' ' like '% ' || nr_seq_prescr || ' %';

begin

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	prescR_medica
where	nr_prescricao = nr_prescricao_p;

Obter_Param_Usuario(722,123,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,Ie_data_amostra_w);
Obter_Param_Usuario(722,367,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_atualiza_dt_coleta_dist_w);

open c01;
loop
fetch c01 into
	nr_seq_proc_mat_item_w,
	nr_seq_prescr_w;
exit when c01%notfound;
	begin
	
	update 	prescr_proc_mat_item
	set 	ie_status = ie_status_atend_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where 	nr_sequencia = nr_seq_proc_mat_item_w;
	
	/*insert into log  tasy
		(DT_ATUALIZACAO,
		NM_USUARIO,
		CD_LOG,
		DS_LOG)
	values	(sysdate,
		nm_usuario_p,
		-7706,
		'lab_atualizar_etapa_amostra - nr_seq_proc_mat_item_w='||nr_seq_proc_mat_item_w||' ie_status_atend_p: '||ie_status_atend_p||' nr_prescricao_p: '||nr_prescricao_p);*/
		
	select  count(*)
	into	qt_total_amostra_w
	from	prescr_proc_mat_item
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_seq_prescr_w;

	select count(*)
	into	qt_etapa_amostra_w
	from	prescr_proc_mat_item
	where	nr_prescricao = nr_prescricao_p
	and	ie_status = ie_status_atend_p
	and	nr_seq_prescr = nr_seq_prescr_w;

	if (qt_total_amostra_w = qt_etapa_amostra_w) and
	   (qt_etapa_amostra_w > 0) then
	
		if ((ie_status_atend_p in (20,25) and ie_atualiza_dt_coleta_dist_w = 'S') or (ie_status_atend_p = 20 and ie_atualiza_dt_coleta_dist_w = 'N')) then
			update prescr_procedimento
			set ie_status_atend = ie_status_atend_p,
			    nm_usuario = nm_usuario_p,
			    dt_atualizacao = sysdate,
			    ie_amostra = 'S',
			     dt_coleta = decode(Ie_data_amostra_w,'S',Lab_obter_data_coleta_amostra(nr_prescricao,nr_sequencia),sysdate)
			where nr_prescricao = nr_prescricao_p
			and nr_sequencia = nr_seq_prescr_w
			and ie_status_atend < ie_status_atend_p;	
			

		elsif (ie_status_atend_p <> 35) then
			update prescr_procedimento
			set ie_status_atend = ie_status_atend_p,
			    nm_usuario = nm_usuario_p,
			    dt_atualizacao = sysdate
			where nr_prescricao = nr_prescricao_p
			and nr_sequencia = nr_seq_prescr_w
			and ie_status_atend < ie_status_atend_p;
		else
			update prescr_procedimento a
			set ie_status_atend = ie_status_atend_p,
			    nm_usuario = nm_usuario_p,
			    dt_atualizacao = sysdate
			where nr_prescricao = nr_prescricao_p
			and nr_sequencia = nr_seq_prescr_w
			and ie_status_atend < ie_status_atend_p
			and exists (select 1 from result_laboratorio x where x.nr_prescricao = a.nr_prescricao and x.nr_seq_prescricao = a.nr_sequencia);

		end if;

	end if;
	end;
end loop;
close c01;

commit;

end;
/
