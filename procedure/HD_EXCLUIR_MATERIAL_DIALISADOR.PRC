create or replace
procedure Hd_Excluir_Material_Dialisador (	nr_seq_dialisador_p	Number,
						nm_usuario_p		Varchar2) is 

begin

if (nr_seq_dialisador_p is not null) then
	delete	material_atend_paciente
	where	nr_seq_dialisador = nr_seq_dialisador_p;
end if;

commit;

end Hd_Excluir_Material_Dialisador;
/