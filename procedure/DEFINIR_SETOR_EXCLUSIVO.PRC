create or replace
procedure Definir_setor_exclusivo(	ie_estrutura_p		varchar2,
				cd_estrutura_p		number,
				cd_setor_exclusivo_p	number,
				cd_estabelecimento_p	number,	
				nm_usuario_p		Varchar2) is				 
				  
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_setor_exclusivo_w		number(5,0);
ds_setor_exclusivo_w		varchar2(60);
ds_setor_exclusivo_destino_w	varchar2(60);
ie_situacao_w			varchar2(1);
nr_sequencia_w			number(10);
ie_limpa_relac_setor_w		varchar2(1);

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced
	from	estrutura_procedimento_v
	where	(((ie_estrutura_p = 'A') and (cd_area_procedimento = cd_estrutura_p)) or 
		 ((ie_estrutura_p = 'E') and (cd_especialidade = cd_estrutura_p)) or
		 ((ie_estrutura_p = 'G') and (cd_grupo_proc = cd_estrutura_p)))
	order by cd_procedimento,
		 ie_origem_proced;
		 
Cursor	C02 is
	select 	cd_procedimento,
		ie_origem_proced
	from 	sus_estrutura_procedimento_v
	where 	(((ie_estrutura_p = 'P') and (nr_seq_grupo = cd_estrutura_p)) or 
		 ((ie_estrutura_p = 'S') and (nr_seq_subgrupo = cd_estrutura_p)) or
		 ((ie_estrutura_p = 'F') and (nr_seq_forma_org = cd_estrutura_p)))
	order by cd_procedimento,
		 ie_origem_proced;
				
begin

ie_limpa_relac_setor_w:= nvl(Obter_valor_param_usuario(25, 21, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'N');

if	(ie_estrutura_p in ('A','E','G')) then

	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C01%notfound;
		begin
		
		select 	max(cd_setor_exclusivo),
			max(substr(obter_nome_setor(cd_setor_exclusivo),1,60)),
			max(substr(obter_nome_setor(cd_setor_exclusivo_p),1,60)),
			nvl(max(ie_situacao),'I')
		into	cd_setor_exclusivo_w,
			ds_setor_exclusivo_w,
			ds_setor_exclusivo_destino_w,
			ie_situacao_w
		from 	procedimento
		where 	cd_procedimento  = cd_procedimento_w
		and 	ie_origem_proced = ie_origem_proced_w;
	
		if	(ie_situacao_w = 'A') then
		
			update	procedimento
			set 	cd_setor_exclusivo = cd_setor_exclusivo_p,
				nm_usuario	   = nm_usuario_p,
				dt_atualizacao     = sysdate
			where 	cd_procedimento	   = cd_procedimento_w
			and 	ie_origem_proced   = ie_origem_proced_w;
	
			insert	into procedimento_hist
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario, 
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_procedimento,
					ie_origem_proced,
					ds_titulo,
					ds_historico)
				values (procedimento_hist_seq.NextVal,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_procedimento_w,
					ie_origem_proced_w,
					upper(wheb_mensagem_pck.get_texto(802491)),
					wheb_mensagem_pck.get_texto(802493,'cd_setor_exclusivo_w=' || cd_setor_exclusivo_w || ';' ||
									   'ds_setor_exclusivo_w=' || ds_setor_exclusivo_w || ';' ||
									   'cd_setor_exclusivo_p=' || cd_setor_exclusivo_p || ';' ||
									   'ds_setor_exclusivo_destino_w=' || ds_setor_exclusivo_destino_w));
					
							
			if	(ie_limpa_relac_setor_w = 'S') and
				(cd_setor_exclusivo_p is null) then
				delete from procedimento_setor_atend
				where 	    cd_procedimento = cd_procedimento_w
				and 	    ie_origem_proced = ie_origem_proced_w;
			end if;
	
		end if;
	
		end;
	end loop;
	close C01;
	
elsif	(ie_estrutura_p in ('P','S','F')) then  -- Estrutura de Procedimentos SUS

	open C02;
	loop
	fetch C02 into	
		cd_procedimento_w,
		ie_origem_proced_w;
	exit when C02%notfound;
		begin
		
		select 	max(cd_setor_exclusivo),
			max(substr(obter_nome_setor(cd_setor_exclusivo),1,60)),
			max(substr(obter_nome_setor(cd_setor_exclusivo_p),1,60)),
			nvl(max(ie_situacao),'I')
		into	cd_setor_exclusivo_w,
			ds_setor_exclusivo_w,
			ds_setor_exclusivo_destino_w,
			ie_situacao_w
		from 	procedimento
		where 	cd_procedimento  = cd_procedimento_w
		and 	ie_origem_proced = ie_origem_proced_w;
	
		if	(ie_situacao_w = 'A') then
	
			update	procedimento
			set 	cd_setor_exclusivo = null,
				nm_usuario	   = nm_usuario_p,
				dt_atualizacao     = sysdate
			where 	cd_procedimento	   = cd_procedimento_w
			and 	ie_origem_proced   = ie_origem_proced_w;
			
			select	procedimento_setor_atend_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			insert	into procedimento_setor_atend
					(nr_sequencia,
					cd_estabelecimento,
					cd_procedimento,
					ie_origem_proced,
					cd_setor_atendimento,
					ie_prioridade,
					cd_setor_origem,
					ie_banco_sangue_rep,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values	(nr_sequencia_w,
					cd_estabelecimento_p,
					cd_procedimento_w,
					ie_origem_proced_w,					
					cd_setor_exclusivo_p,
					100,
					null,
					'N',
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p);
					
	
			insert	into procedimento_hist
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario, 
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_procedimento,
					ie_origem_proced,
					ds_titulo,
					ds_historico)
				values (	procedimento_hist_seq.NextVal,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_procedimento_w,
					ie_origem_proced_w,
					upper(wheb_mensagem_pck.get_texto(802491)),
					wheb_mensagem_pck.get_texto(802500,'cd_setor_exclusivo_w=' || cd_setor_exclusivo_w || ';' ||
									   'ds_setor_exclusivo_w=' || ds_setor_exclusivo_w || ';' ||
									   'cd_setor_exclusivo_p=' || cd_setor_exclusivo_p || ';' ||
									   'ds_setor_exclusivo_destino_w=' || ds_setor_exclusivo_destino_w));				
	
		end if;
	
		end;
	end loop;
	close C02;

end if;

commit;

end Definir_setor_exclusivo;
/