create or replace
procedure consiste_data_vigencia(
			dt_inicial_p		date,
			dt_final_p		date) is 

begin

if	(dt_inicial_p is not null) and
	(dt_final_p is not null) then
	
	if	(dt_inicial_p > dt_final_p) then
		wheb_mensagem_pck.exibir_mensagem_abort(122849);
	end if;
		
end if;

end consiste_data_vigencia;
/