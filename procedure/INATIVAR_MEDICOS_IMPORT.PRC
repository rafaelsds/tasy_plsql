create or replace
procedure inativar_medicos_import(
			ie_origem_import_p	Varchar2,			
			nm_usuario_p		Varchar2,
			nr_seq_conselho_p	number	default 0) is 


Cursor C01 is
	select	*
	from	cadastro_medico
	where	ie_origem_import = 'X'
	and	ie_situacao_reg_base = 'A'
	and	substr(ds_status,1,1) = 'I'
	order by 1;
	
c01_w				c01%rowtype;
qt_registros_atualizados_w	Number(10) := 0;
nr_seq_conselho_med_w		number(10);

begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin

	select	nvl(max(nr_seq_conselho),0)
	into	nr_seq_conselho_med_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = c01_w.cd_pessoa_fisica;

	if	(nvl(nr_seq_conselho_p,0) = 0) or
		(nr_seq_conselho_med_w = nvl(nr_seq_conselho_p,0)) then
		begin

		update	medico
		set	ie_situacao = 'I',
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	cd_pessoa_fisica = c01_w.cd_pessoa_fisica;
		
		select ie_situacao 
		into	c01_w.ds_status
		from medico where cd_pessoa_fisica = c01_w.cd_pessoa_fisica;
	
		qt_registros_atualizados_w := qt_registros_atualizados_w + 1;

		if	(qt_registros_atualizados_w = 100) then
			commit;
			qt_registros_atualizados_w := 0;
		end if;


		end;
	end if;

	end;
end loop;
close C01;

commit;

end inativar_medicos_import;
/