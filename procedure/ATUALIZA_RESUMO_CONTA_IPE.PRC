CREATE OR REPLACE
PROCEDURE Atualiza_Resumo_Conta_IPE(nr_seq_protocolo_p 	number,
						nm_usuario_p	 	varchar2) IS

nr_sequencia_w			number(10);
nr_seq_protocolo_w		number(10);
nr_interno_conta_w		number(10);
nr_seq_conta_convenio_w		number(15);
vl_total_item_w			number(15,2);
vl_original_w			number(15,2);



CURSOR C01 IS
select nr_seq_protocolo,
	 nr_interno_conta,
	 nvl(nr_seq_conta_convenio,0),
	 sum(vl_total_item),
	 sum(nvl(vl_original,0))
from	 w_interf_conta_item_ipe
where	 nr_seq_protocolo	= nr_seq_protocolo_p
group by 
	 nr_seq_protocolo,
	 nr_interno_conta,
	 nvl(nr_seq_conta_convenio,0)
order by 1,3,2;



BEGIN
/* Limpar tabela */
delete from ipe_conta_resumo
where nr_seq_protocolo	= nr_seq_protocolo_p;
commit;

OPEN C01;
LOOP
FETCH C01 	into
		nr_seq_protocolo_w,
		nr_interno_conta_w,
		nr_seq_conta_convenio_w,
		vl_total_item_w,
		vl_original_w;
EXIT WHEN 	C01%NOTFOUND;
     		BEGIN
		select ipe_conta_resumo_seq.nextval
		into	 nr_sequencia_w
		from	 dual;

		insert into ipe_conta_resumo(
			 nr_sequencia,
			 nr_seq_protocolo,
			 nr_interno_conta,
			 nr_seq_conta_convenio,
			 vl_total_conta,
			 dt_atualizacao,
			 nm_usuario,
			 vl_original)
		values (
			 nr_sequencia_w,
			 nr_seq_protocolo_w,
			 nr_interno_conta_w,
			 nr_seq_conta_convenio_w,
			 vl_total_item_w,
			 sysdate,
			 nm_usuario_p,
			 vl_original_w);
		END;
END LOOP;
close C01;
commit;
END Atualiza_Resumo_Conta_IPE;
/
