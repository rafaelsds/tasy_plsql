create or replace
procedure proj_atualizar_descricao_ativ(nr_sequencia_p	number,
					nm_usuario_p	Varchar2) is 

nr_sequencia_w		number(10);
nr_seq_cronograma_w	number(10);

Cursor C02 is
select	nr_sequencia
from	proj_cron_etapa
where	nr_seq_cronograma = nr_seq_cronograma_w
order by 1;

begin

select	max(nr_seq_cronograma)
into	nr_seq_cronograma_w
from	proj_cron_etapa
where	nr_sequencia = nr_sequencia_p
order by 1;

open C02;
loop
fetch C02 into	
	nr_sequencia_w;
exit when C02%notfound;
	begin
	proj_atualizar_desc_atividade(nr_sequencia_w);
	end;
end loop;
close C02;

commit;

end proj_atualizar_descricao_ativ;
/
