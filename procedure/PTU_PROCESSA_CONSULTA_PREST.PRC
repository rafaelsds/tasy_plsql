create or replace
procedure ptu_processa_consulta_prest(	nr_seq_consulta_p  			ptu_consulta_prestador.nr_sequencia%type,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_resp_consulta_prest_p	out	ptu_resp_consulta_prest.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a valida��o do arquivo 00418 - Consulta Prestado do PTU 

Rotina utilizada nas transa��es ptu via WebService.
Ir� ler a consulta de prestador que foi realizada e ir� gerar a resposta desta consulta.,
gravando as informa��es nas tabelas que ser�o consultadas na tela.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros:*
*WebService SCS
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nm_prestador_w			ptu_consulta_prestador.nm_prestador%type;
sg_cons_prof_w			ptu_consulta_prestador.sg_cons_profissional%type;
ie_tipo_cliente_w		ptu_consulta_prestador.ie_tipo_cliente%type;
uf_cons_prof_w			ptu_consulta_prestador.uf_cons_profissional%type;
cd_cgc_cpf_w			ptu_consulta_prestador.cd_cgc_cpf%type;
nr_cons_prof_w			ptu_consulta_prestador.nr_cons_profissional%type;
nr_seq_requisicao_w		ptu_consulta_prestador.nr_seq_requisicao%type;
nr_seq_execucao_w		ptu_consulta_prestador.nr_seq_execucao%type;
cd_unimed_executora_w		ptu_consulta_prestador.cd_unimed_executora%type;
cd_unimed_beneficiario_w	ptu_consulta_prestador.cd_unimed_beneficiario%type;
nr_versao_w			ptu_consulta_prestador.nr_versao%type;
ie_tipo_rede_min_w		ptu_resp_nomes_prest.ie_tipo_rede_min%type;
cd_especialidade_w		ptu_resp_nomes_prest.cd_especialidade%type;
ie_inseriu_w			varchar2(1)	:= 'N';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

Cursor C01 (	nm_prestador_pc		pessoa_fisica.nm_pessoa_fisica%type,
		cd_cgc_cpf_pc		varchar2,
		nr_cons_prof_pc		ptu_consulta_prestador.nr_cons_profissional%type,
		uf_cons_prof_pc		ptu_consulta_prestador.uf_cons_profissional%type,
		sg_cons_prof_pc		ptu_consulta_prestador.sg_cons_profissional%type )is
	
	select	a.cd_pessoa_fisica cd_prestador,
		a.nm_pessoa_fisica nm_pessoa_fisica,
		d.nr_sequencia nr_seq_prestador,
		decode(d.ie_tipo_rede_min_ptu, 1, 3, 3, 1, 2) ie_tipo_rede_min
	from	pessoa_fisica		a,
		medico			b,
		conselho_profissional	c,
		pls_prestador		d,
		pls_prestador_medico	e
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_seq_conselho	= c.nr_sequencia
	and	d.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	d.nr_sequencia		= e.nr_seq_prestador(+)
	and	(b.nr_crm		= nr_cons_prof_pc	or nr_cons_prof_pc 	is null)
	and	(b.uf_crm		= uf_cons_prof_pc	or uf_cons_prof_pc 	is null)
	and	(c.sg_conselho		= sg_cons_prof_pc	or sg_cons_prof_pc	is null)
	and	(a.nr_cpf		= to_char(lpad(cd_cgc_cpf_pc,11,'0')) or to_char(cd_cgc_cpf_pc) is null)
	and	b.ie_situacao		= 'A'
	and	d.ie_situacao		= 'A'
	and	(e.ie_situacao		= 'A' or e.ie_situacao	is null)
	and	trunc(sysdate,'dd') between trunc(nvl(e.dt_inclusao,sysdate),'dd') and  fim_dia(nvl(e.dt_exclusao,sysdate))
	and	upper(a.nm_pessoa_fisica_sem_acento) like nm_prestador_pc
	union
	select	cd_prestador cd_prestador,
		substr(obter_dados_pf_pj(null, cd_cgc,'N'),1,40) nm_pessoa_fisica,
		nr_sequencia nr_seq_prestador,
		decode(ie_tipo_rede_min_ptu, 1, 3, 3, 1, 2) ie_tipo_rede_min
	from	pls_prestador
	where	ie_situacao	= 'A'
	and	upper(obter_dados_pf_pj(null, cd_cgc,'N')) like nm_prestador_pc
	and	((cd_cgc = lpad(cd_cgc_cpf_pc,14,'0')) or (cd_cgc_cpf_pc is null))
	order by 1;	

begin

cd_estabelecimento_w := cd_estabelecimento_p;

--Quando as transa��es s�o geradas pelo WebService, n�o existe estabelecimento definido, ent�o � verificado o estabeleicmento do par�metro
if	( cd_estabelecimento_w is null	) then
	cd_estabelecimento_w := ptu_obter_estab_padrao;
end if;

select	ie_tipo_cliente,
	cd_unimed_executora,
	cd_unimed_beneficiario, 	
	upper(elimina_acentos(('%'||nm_prestador||'%'))),
	cd_cgc_cpf, 
	sg_cons_profissional, 
	nr_cons_profissional,
	uf_cons_profissional,
	nr_seq_execucao,
	nr_versao,
	ptu_resp_consulta_prest_seq.nextval
into	ie_tipo_cliente_w,
	cd_unimed_executora_w,
	cd_unimed_beneficiario_w, 
	nm_prestador_w, 
	cd_cgc_cpf_w, 
	sg_cons_prof_w, 
	nr_cons_prof_w,
	uf_cons_prof_w,
	nr_seq_execucao_w,
	nr_versao_w,
	nr_seq_resp_consulta_prest_p
from	ptu_consulta_prestador
where	nr_sequencia	= nr_seq_consulta_p;

insert	into ptu_resp_consulta_prest
	(nr_sequencia, cd_transacao, ie_tipo_cliente,
	cd_unimed_executora, nr_seq_execucao, cd_unimed_beneficiario,
	ie_confirmacao, nr_seq_origem, dt_atualizacao,
	nm_usuario, nr_seq_guia, nr_seq_requisicao,
	nm_usuario_nrec, dt_atualizacao_nrec, nr_versao)
values	(nr_seq_resp_consulta_prest_p, '00419', ie_tipo_cliente_w,
	cd_unimed_executora_w, nr_seq_execucao_w, cd_unimed_beneficiario_w,
	'S', nr_seq_resp_consulta_prest_p, sysdate,
	nm_usuario_p, nr_seq_resp_consulta_prest_p, nr_seq_requisicao_w,
	nm_usuario_p, sysdate, nr_versao_w);
begin
	for c01_w in C01(nm_prestador_w, cd_cgc_cpf_w, nr_cons_prof_w, uf_cons_prof_w, sg_cons_prof_w) loop
		begin		
		select	max(a.cd_ptu)
		into	cd_especialidade_w
		from	pls_prestador_med_espec	b,
			especialidade_medica	a
		where	b.nr_seq_prestador	= c01_w.nr_seq_prestador
		and	a.cd_especialidade	= b.cd_especialidade
		and	trunc(sysdate,'dd') between trunc(nvl(b.dt_inicio_vigencia, sysdate), 'dd') and  fim_dia(nvl(b.dt_fim_vigencia, sysdate));

		insert	into ptu_resp_nomes_prest
			(nr_sequencia, cd_unimed_prestador, ie_tipo_rede_min,
			nm_prestador, cd_prestador, nr_seq_resp_prest,
			dt_atualizacao, nm_usuario, cd_especialidade,
			nm_usuario_nrec, dt_atualizacao_nrec)
		values	(ptu_resp_nomes_prest_seq.nextval, cd_unimed_beneficiario_w, c01_w.ie_tipo_rede_min,
			substr(c01_w.nm_pessoa_fisica,1,40), c01_w.cd_prestador, nr_seq_resp_consulta_prest_p,
			sysdate, nm_usuario_p, cd_especialidade_w,
			nm_usuario_p, sysdate); 
		end;

		ie_inseriu_w	:= 'S';
	end loop;
exception
when others then
	ie_inseriu_w	:= 'N';
end;

if	(nvl(ie_inseriu_w, 'N') = 'N') then
	update	ptu_resp_consulta_prest
	set	ie_confirmacao	= 'N'
	where	nr_sequencia	= nr_seq_resp_consulta_prest_p;
	
	ptu_inserir_inconsistencia(	null, null, 5001,
					'', cd_estabelecimento_w, nr_seq_resp_consulta_prest_p, 
					'CP', '00419', null, 
					null, null, nm_usuario_p);
end if;

commit;

end ptu_processa_consulta_prest;
/