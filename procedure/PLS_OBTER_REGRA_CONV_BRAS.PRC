/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter a regra de convers�o de quantidade de bras�ndice
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
create or replace
procedure pls_obter_regra_conv_bras
				(	nr_seq_material_p		number,
					dt_referencia_p			date,
					cd_estabelecimento_p		number,
					qt_conversao_bras_p		number,
					qt_conversao_atual_p		number,
					qt_conversao_p		out	number) is
					
nr_seq_regra_mat_uni_bra_w		pls_regra_mat_uni_sc_bras.nr_sequencia%type;
qt_conversao_fixo_w			pls_regra_mat_uni_sc_bras.qt_conversao_fixo%type;
ie_aplicar_conv_fed_sc_w		pls_regra_mat_uni_sc_bras.ie_aplicar_conv_fed_sc%type;
qt_conversao_w				number(10) := null;
cd_material_ops_w			pls_material.cd_material_ops%type;
cd_unidade_medida_w			pls_material.cd_unidade_medida%type;

begin

select	max(cd_material_ops),
	max(cd_unidade_medida)
into	cd_material_ops_w,
	cd_unidade_medida_w
from	pls_material
where	nr_sequencia = nr_seq_material_p;

select	max(nr_sequencia)
into	nr_seq_regra_mat_uni_bra_w
from	pls_regra_mat_uni_sc_bras
where	((nr_seq_material	= nr_seq_material_p) or (nr_seq_material is null))
and	((cd_material_ops	= cd_material_ops_w) or (cd_material_ops is null) or (nvl(cd_material_ops, cd_material_ops_w) is null))
and	((cd_unidade_medida	= cd_unidade_medida_w) or (cd_unidade_medida is null))
and	dt_referencia_p between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_referencia_p);

if	(nr_seq_regra_mat_uni_bra_w is not null) then
	select	qt_conversao_fixo,
		ie_aplicar_conv_fed_sc
	into	qt_conversao_fixo_w,
		ie_aplicar_conv_fed_sc_w
	from	PLS_REGRA_MAT_UNI_SC_BRAS
	where	nr_sequencia	= nr_seq_regra_mat_uni_bra_w;
	
	/*Aplicar convers�o da Federa��o*/
	if	(ie_aplicar_conv_fed_sc_w = 'FED') then
		qt_conversao_w	:= qt_conversao_bras_p;
	/*Aplicar convers�o fixa*/
	elsif	(ie_aplicar_conv_fed_sc_w = 'FX') then
		qt_conversao_w	:= qt_conversao_fixo_w;
	/*N�o aplicar convers�o*/
	elsif	(ie_aplicar_conv_fed_sc_w = 'N') then
		qt_conversao_w	:= qt_conversao_atual_p;
	end if;
else
	qt_conversao_w	:= qt_conversao_atual_p;
end if;

qt_conversao_p	:= qt_conversao_w;

end pls_obter_regra_conv_bras;
/