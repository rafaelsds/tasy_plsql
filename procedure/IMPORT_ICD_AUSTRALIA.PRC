CREATE OR REPLACE PROCEDURE IMPORT_ICD_AUSTRALIA
IS
  -- tabela icd_chapter_aus
  cd_chapter_w icd_chapter_aus.cd_chapter%type;
  ds_chapter_w icd_chapter_aus.ds_chapter%type;
  cd_start_range_w icd_chapter_aus.cd_start_range%type;
  cd_end_range_w icd_chapter_aus.cd_end_range%type;
  -- tabela icd_codes_aus
  cd_icd_code_w icd_codes_aus.cd_icd_code%type;
  cd_icd_code_ww icd_codes_aus.cd_icd_code%type;
  cd_level_w icd_codes_aus.cd_level%type;
  cd_dagger_w icd_codes_aus.cd_dagger%type;
  cd_asterisk_w icd_codes_aus.cd_asterisk%type;
  cd_valid_w icd_codes_aus.cd_valid%type;
  cd_aust_code_w icd_codes_aus.cd_aust_code%type;
  ds_code_w icd_codes_aus.ds_code%type;
  ds_code_ww icd_codes_aus.ds_code%type;
  ds_ascii_short_w icd_codes_aus.ds_ascii_short%type;
  dt_effective_from_w icd_codes_aus.dt_effective_from%type;
  dt_inactive_on_w icd_codes_aus.dt_inactive_on%type;
  dt_reactivated_on_w icd_codes_aus.dt_reactivated_on%type;
  ie_sex_w icd_codes_aus.ie_sex%type;
  ie_stype_w icd_codes_aus.ie_stype%type;
  ie_age_l_w icd_codes_aus.ie_age_l%type;
  ie_age_h_w icd_codes_aus.ie_age_h%type;
  ie_atype_w icd_codes_aus.ie_atype%type;
  ie_rdiag_w icd_codes_aus.ie_rdiag%type;
  ie_morph_code_w icd_codes_aus.ie_morph_code%type;
  cd_concept_change_w icd_codes_aus.cd_concept_change%type;
  ie_unaccepted_pdx_w icd_codes_aus.ie_unaccepted_pdx%type;
  --tabela CIDO_MORFOLOGIA_AUS
  cd_code_w CIDO_MORFOLOGIA_AUS.cd_code%type;
  cd_aus_code_w CIDO_MORFOLOGIA_AUS.cd_aus_code%type;
  ds_code_www CIDO_MORFOLOGIA_AUS.ds_code%type;
  ds_ascii_code_w CIDO_MORFOLOGIA_AUS.ds_ascii_code%type;
  dt_effective_from_ww CIDO_MORFOLOGIA_AUS.dt_effective_from%type;
  dt_inactive_on_ww CIDO_MORFOLOGIA_AUS.dt_inactive_on%type;
  dt_reactivated_on_ww CIDO_MORFOLOGIA_AUS.dt_reactivated_on%type;
  -- verificacoes auxiliares
  qt_exist_chapter_w     NUMBER(10);
  qt_exist_category_w    NUMBER(10);
  qt_exist_cid_w         NUMBER(10);
  qt_existe_cid_versao_w NUMBER(10);
  qt_exist_morfologia_w  NUMBER(10);
  CURSOR c01
  IS
    SELECT cd_chapter,
      SUBSTR(ds_chapter, 1, 100) ,
      cd_start_range,
      cd_end_range
    FROM icd_chapter_aus;
  CURSOR c02
  IS
    SELECT cd_icd_code,
      SUBSTR(ds_code, 1, 240)
    FROM icd_codes_aus
    WHERE cd_level=3
    AND CD_ICD_CODE BETWEEN cd_start_range_w AND cd_end_range_w;
  CURSOR c03
  IS
    SELECT cd_icd_code,
      ds_code,
      ie_sex,
      dt_effective_from,
      dt_inactive_on
    FROM icd_codes_aus
    WHERE cd_level             IN (4,5)
    AND SUBSTR(CD_ICD_CODE,0,3) = cd_icd_code_w;
  CURSOR c04
  IS
    SELECT CD_CODE,
      CD_AUS_CODE,
      DS_CODE,
      DS_ASCII_CODE,
      DT_EFFECTIVE_FROM,
      DT_INACTIVE_ON,
      DT_REACTIVATED_ON
    FROM cido_morfologia_aus ;
BEGIN
  OPEN c01; -- ESPECIALIDADE
  LOOP
    FETCH c01 INTO cd_chapter_w, ds_chapter_w,cd_start_range_w,cd_end_range_w;
    EXIT
  WHEN c01%NOTFOUND;
    BEGIN
      SELECT COUNT(*)
      INTO qt_exist_chapter_w
      FROM cid_especialidade
      WHERE cd_especialidade_cid = cd_chapter_w;
      IF ( qt_exist_chapter_w    = 0 ) THEN
        BEGIN
          INSERT
          INTO cid_especialidade
            (
              cd_especialidade_cid,
              ds_especialidade_cid,
              dt_atualizacao,
              nm_usuario,
              dt_atualizacao_nrec,
              nm_usuario_nrec,
              cd_categ_inicial,
              cd_categ_final,
              ds_especialidade_compl,
              ie_situacao
            )
            VALUES
            (
              cd_chapter_w,    -- CD_ESPECIALIDADE_CID
              ds_chapter_w,    -- DS_ESPECIALIDADE_CID
              SYSDATE,         -- DT_ATUALIZACAO
              'TasyLoad',      -- NM_USUARIO
              SYSDATE,         -- DT_ATUALIZACAO_NREC
              'TasyLoad',      -- NM_USUARIO_NREC
              cd_start_range_w,-- CD_CATEG_INICIAL
              cd_end_range_w,  -- CD_CATEG_FINAL
              NULL,            -- DS_ESPECIALIDADE_COMPL
              'A'              -- IE_SITUACAO
            );
        END;
      ELSE
        BEGIN
          UPDATE cid_especialidade
          SET ds_especialidade_cid   = ds_chapter_w,
            dt_atualizacao           = SYSDATE,
            nm_usuario               = 'TasyLoad',
            ie_situacao              = 'A'
          WHERE cd_especialidade_cid = cd_chapter_w;
        END;
      END IF;
      OPEN c02; -- CATEGORIA
      LOOP
        FETCH c02 INTO cd_icd_code_w, ds_code_w;
        EXIT
      WHEN c02%NOTFOUND;
        BEGIN
          SELECT COUNT(*)
          INTO qt_exist_category_w
          FROM cid_categoria
          WHERE cd_categoria_cid   = cd_icd_code_w;
          IF ( qt_exist_category_w = 0 ) THEN
            BEGIN
              INSERT
              INTO cid_categoria
                (
                  cd_categoria_cid,
                  ds_categoria_cid,
                  dt_atualizacao,
                  nm_usuario,
                  cd_especialidade,
                  ds_categoria_compl,
                  ds_referencia,
                  ds_excluidos,
                  ie_situacao,
                  dt_atualizacao_nrec,
                  nm_usuario_nrec
                )
                VALUES
                (
                  cd_icd_code_w,-- CD_CATEGORIA_CID
                  ds_code_w,    -- DS_CATEGORIA_CID
                  SYSDATE,      -- DT_ATUALIZACAO
                  'TasyLoad',   -- NM_USUARIO
                  cd_chapter_w, -- CD_ESPECIALIDADE
                  NULL,         -- DS_CATEGORIA_COMPL
                  NULL,         -- DS_REFERENCIA
                  NULL,         -- DS_EXCLUIDOS
                  'A',          -- IE_SITUACAO
                  SYSDATE,      -- DT_ATUALIZACAO_NREC
                  'TasyLoad'    -- NM_USUARIO_NREC
                );
            END;
          ELSE
            BEGIN
              UPDATE cid_categoria
              SET ds_categoria_cid   = ds_code_w,
                dt_atualizacao       = SYSDATE,
                nm_usuario           = 'TasyLoad',
                cd_especialidade     = cd_chapter_w,
                ie_situacao          = 'A'
              WHERE cd_categoria_cid = cd_icd_code_w;
            END;
          END IF;
          OPEN c03; -- CID
          LOOP
            FETCH c03
            INTO cd_icd_code_ww,
              ds_code_ww,
              ie_sex_w,
              dt_effective_from_w,
              dt_inactive_on_w;
            EXIT
          WHEN c03%NOTFOUND;
            BEGIN
              SELECT COUNT(*)
              INTO qt_exist_cid_w
              FROM cid_doenca
              WHERE cd_doenca_cid = cd_icd_code_ww;
              DBMS_OUTPUT.PUT_LINE(cd_icd_code_ww || ' ' ||qt_exist_cid_w);
              IF ( qt_exist_cid_w = 0 ) THEN
                BEGIN
                  INSERT
                  INTO cid_doenca
                    (
                      cd_doenca_cid,
                      ds_doenca_cid,
                      cd_categoria_cid,
                      dt_atualizacao,
                      nm_usuario,
                      ie_sexo,
                      qt_campos_radio,
                      ie_estadio,
                      ie_repete_radio,
                      ie_cad_interno,
                      ie_situacao,
                      qt_idade_min,
                      qt_idade_max,
                      ie_exige_lado,
                      ds_descricao_original,
                      nr_seq_idioma,
                      cd_versao,
                      cd_doenca,
                      ds_informacao_adic,
                      ds_mensagem,
                      ie_dieta_oral,
                      ie_subcategoria,
                      ie_exibir_localizador,
                      qt_dias_prev_inter,
                      ie_obrigar_morfologia,
                      dt_atualizacao_nrec,
                      nm_usuario_nrec
                    )
                    VALUES
                    (
                      cd_icd_code_ww,                           -- CD_DOENCA_CID
                      ds_code_ww,                               -- DS_DOENCA_CID
                      cd_icd_code_w,                            -- CD_CATEGORIA_CID
                      SYSDATE,                                  -- DT_ATUALIZACAO
                      'TasyLoad',                               -- NM_USUARIO
                      DECODE(ie_sex_w, 'M', 'M', 'W', 'F', 'A'),-- IE_SEXO
                      NULL,                                     -- QT_CAMPOS_RADIO
                      NULL,                                     -- IE_ESTADIO
                      NULL,                                     -- IE_REPETE_RADIO
                      'N',                                      -- IE_CAD_INTERNO
                      'A',                                      -- IE_SITUACAO
                      NULL,                                     -- QT_IDADE_MIN
                      NULL,                                     -- QT_IDADE_MAX
                      NULL,                                     -- IE_EXIGE_LADO
                      ds_code_ww,                               -- DS_DESCRICAO_ORIGINAL
                      NULL,                                     -- NR_SEQ_IDIOMA
                      'ICD-10',                                 -- CD_VERSAO    ** ESTE CAMPO DEVE SER ADICIONADO NO ARQUIVO
                      cd_icd_code_ww,                           -- CD_DOENCA
                      NULL,                                     -- DS_INFORMACAO_ADIC
                      NULL,                                     -- DS_MENSAGEM
                      NULL,                                     -- IE_DIETA_ORAL
                      NULL,                                     -- IE_SUBCATEGORIA
                      NULL,                                     -- IE_EXIBIR_LOCALIZADOR
                      NULL,                                     -- QT_DIAS_PREV_INTER
                      NULL,                                     -- IE_OBRIGAR_MORFOLOGIA
                      SYSDATE,                                  -- DT_ATUALIZACAO_NREC
                      'TasyLoad'                                -- NM_USUARIO_NREC
                    );
                END;
              ELSE
                BEGIN
                  UPDATE cid_doenca
                  SET ds_doenca_cid   = ds_code_ww,
                    cd_categoria_cid  = cd_icd_code_ww,
                    dt_atualizacao    = SYSDATE,
                    nm_usuario        = 'TasyLoad',
                    ie_sexo           = DECODE(ie_sex_w, 'M', 'M', 'W', 'F', 'A'),
                    qt_idade_min      = NULL,
                    qt_idade_max      = NULL,
                    ie_situacao       = 'A'
                  WHERE cd_doenca_cid = cd_icd_code_ww;
                END;
              END IF;
              IF( cd_icd_code_ww IS NOT NULL ) THEN
                BEGIN
                  -- finaliza vigencia dos cids anteriores
                  UPDATE cid_doenca_versao
                  SET dt_vigencia_final    = sysdate
                  WHERE cd_doenca_cid      = cd_icd_code_ww
                  AND dt_vigencia_final   IS NULL
                  AND dt_vigencia_inicial IS NOT NULL;
                  SELECT COUNT(*)
                  INTO qt_existe_cid_versao_w
                  FROM cid_doenca_versao
                  WHERE cd_doenca_cid                          = cd_icd_code_ww
                  AND To_date(dt_vigencia_inicial, 'dd/mm/yy') = To_date(dt_effective_from_w, 'dd/mm/yy')
                  AND To_date(dt_vigencia_final, 'dd/mm/yy')   = To_date(dt_inactive_on_w, 'dd/mm/yy');
                  IF( qt_existe_cid_versao_w                   = 0 OR qt_existe_cid_versao_w IS NULL ) THEN
                    INSERT
                    INTO cid_doenca_versao
                      (
                        nr_sequencia,
                        dt_atualizacao,
                        nm_usuario,
                        dt_atualizacao_nrec,
                        nm_usuario_nrec,
                        cd_doenca_cid,
                        dt_versao,
                        dt_vigencia_inicial,
                        dt_vigencia_final,
                        cd_versao
                      )
                      VALUES
                      (
                        cid_doenca_versao_seq.NEXTVAL,                             -- nr_sequencia
                        SYSDATE,                                                   -- dt_atualizacao
                        'TasyLoad',                                                -- nm_usuario
                        SYSDATE,                                                   -- dt_atualizacao_nrec
                        'TasyLoad',                                                -- nm_usuario_nrec
                        cd_icd_code_ww,                                            -- cd_doenca_cid
                        TRUNC(To_date(dt_effective_from_w, 'dd/mm/yyyy'), 'yyyy'), -- dt_versao
                        To_date(dt_effective_from_w, 'dd/mm/yyyy'),                -- dt_vigencia_inicial
                        NVL(To_date(dt_inactive_on_w, 'dd/mm/yyyy'),NULL),         -- dt_vigencia_final
                        NULL
                      );
                  END IF;
                END;
              END IF;
              -- finaliza todas as vigencias dos cids anteriores
              UPDATE cid_doenca_versao
              SET dt_vigencia_final    = sysdate
              WHERE dt_vigencia_final IS NULL
              AND dt_vigencia_inicial IS NOT NULL
              AND dt_atualizacao       < sysdate
              AND nm_usuario LIKE 'TasyLoad';
            END;
          END LOOP;
          CLOSE c03; -- CID
        END;
      END LOOP;
      CLOSE c02; --- CATEGORIA
    END;
  END LOOP;
  CLOSE c01; -- ESPECIALIDADE
  BEGIN
    OPEN c04;
    LOOP
      FETCH c04
      INTO cd_code_w,
        cd_aus_code_w,
        ds_code_www,
        ds_ascii_code_w,
        dt_effective_from_ww,
        dt_inactive_on_ww,
        dt_reactivated_on_ww;
      EXIT
    WHEN c04%NOTFOUND;
      BEGIN
        SELECT COUNT(*)
        INTO qt_exist_morfologia_w
        FROM CIDO_MORFOLOGIA
        WHERE CD_MORFOLOGIA = cd_code_w;
        --DBMS_OUTPUT.PUT_LINE(cd_code_w||' '|| qt_exist_morfologia_w);
        IF ( qt_exist_morfologia_w = 0 ) THEN
          BEGIN
            INSERT
            INTO cido_morfologia
              (
                CD_MORFOLOGIA,
                DT_ATUALIZACAO,
                NM_USUARIO,
                DS_MORFOLOGIA,
                IE_SEXO,
                DT_ATUALIZACAO_NREC,
                NM_USUARIO_NREC,
                IE_SITUACAO
              )
              VALUES
              (
                cd_code_w,   -- CD_MORFOLOGIA
                SYSDATE,     -- DT_ATUALIZACAO
                'TasyLoad',  -- NM_USUARIO
                ds_code_www, --DS_MORFOLOGIA
                NULL,        -- DT_ATUALIZACAO
                SYSDATE,     -- NM_USUARIO
                'TasyLoad',  -- DT_ATUALIZACAO_NREC
                'A'
              );
          END;
        ELSE
          BEGIN
            UPDATE cido_morfologia
            SET DS_MORFOLOGIA   = ds_code_www,
              dt_atualizacao    = SYSDATE,
              nm_usuario        = 'TasyLoad',
              ie_situacao       = 'A'
            WHERE CD_MORFOLOGIA = cd_code_w;
          END;
        END IF;
      END;
    END LOOP;
  END;
  COMMIT;
END IMPORT_ICD_AUSTRALIA;
/