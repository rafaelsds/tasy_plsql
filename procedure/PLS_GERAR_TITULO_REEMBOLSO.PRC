create or replace
procedure pls_gerar_titulo_reembolso
			(	nr_seq_reembolso_p	number,
				dt_vencimento_p		date,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number) is 
				
/*

ESSA PROCEDURE N�O � MAIS UTILIZADA, VERIFICAR A PLS_GERAR_TITULO_PROT_REEMB

*/				


vl_reembolso_w			number(15,2);
cd_moeda_w			number(5);
pr_juros_padrao_w		number(7,4);
pr_multa_padrao_w		number(7,4);
cd_tipo_taxa_juros_w		number(10);
cd_tipo_taxa_multa_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			Varchar2(14);
nr_titulo_w			number(10);
dt_referencia_w			Date;
ie_mes_fechado_w		Varchar2(1);
ie_forma_pagamento_w		number(2);
ie_acao_venc_nao_util_w		Varchar2(1)	:= 'M';
cd_condicao_pagamento_w		Number(10);
vl_titulo_w			Number(15,2);
tx_fracao_parcela_w		Number(15,4)	:= 0;
tx_acrescimo_w			Number(15,4)	:= 0;
dt_vencimento_w			Date;
dt_base_venc_w			Date;
ie_tipo_resp_credito_w		Varchar2(2);
nr_seq_segurado_w		Number(10);
nr_seq_trans_fin_baixa_reemb_w	number(10);
cd_cgc_estipulante_w		Varchar2(14);
cd_pf_estipulante_w		Varchar2(10);
nr_contrato_principal_w		Number(10);
cd_conta_financ_w		Number(10);
nr_seq_classif_w		Number(10);
nr_seq_subestipulante_w		Number(10);
nr_seq_titular_w		Number(10);

Cursor C01 is
	select	tx_fracao_parcela,	/* A Prazo   */
		nvl(tx_acrescimo,0),
		Obter_data_vencimento(	dt_base_venc_w,
					qt_dias_parcela,
					cd_estabelecimento_p,
					ie_corrido_util,
					ie_acao_venc_nao_util_w)
	from	parcela
	where	cd_condicao_pagamento 	= cd_condicao_pagamento_w
	and	ie_forma_pagamento_w	<> 1
	UNION
	select	100, /* A Vista   */	
		0,	
		dt_base_venc_w
	from	dual
	where	ie_forma_pagamento_w	= 1;
	
begin

dt_base_venc_w	:= dt_vencimento_p;

select 	trunc(dt_referencia,'dd'),
	substr(pls_obter_valor_reembolso(nr_sequencia,'LQ'),1,100),
	substr(pls_obter_dados_segurado(nr_seq_segurado,'PF'),1,10),
	cd_condicao_pagamento,
	ie_tipo_resp_credito,
	nr_seq_segurado
into	dt_referencia_w,
	vl_reembolso_w,
	cd_pessoa_fisica_w,
	cd_condicao_pagamento_w,
	ie_tipo_resp_credito_w,
	nr_seq_segurado_w
from 	pls_reembolso
where	nr_sequencia = nr_seq_reembolso_p;

/* Obter dados do contrato */
begin
select	b.cd_cgc_estipulante,
	b.cd_pf_estipulante,
	nvl(b.nr_contrato_principal,0),
	a.nr_seq_subestipulante
into	cd_cgc_estipulante_w,
	cd_pf_estipulante_w,
	nr_contrato_principal_w,
	nr_seq_subestipulante_w
from	pls_contrato	b,
	pls_segurado	a
where	a.nr_sequencia		= nr_seq_segurado_w
and	a.nr_seq_contrato	= b.nr_sequencia;
exception
	when others then
	cd_cgc_estipulante_w	:= '';
	cd_pf_estipulante_w	:= '';
end;

if	(ie_tipo_resp_credito_w = 'P') then
	select	a.cd_pessoa_fisica,
		a.cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_contrato_pagador a,
		pls_segurado b
	where	a.nr_sequencia = b.nr_seq_pagador
	and	b.nr_sequencia = nr_seq_segurado_w;
elsif	(ie_tipo_resp_credito_w = 'E') then
	cd_pessoa_fisica_w	:= cd_pf_estipulante_w;
	cd_cgc_w		:= cd_cgc_estipulante_w;
elsif	(ie_tipo_resp_credito_w = 'CP') and 
	(nr_contrato_principal_w > 0) then
	select	cd_cgc_estipulante,
		cd_pf_estipulante
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_contrato
	where	nr_sequencia	= nr_contrato_principal_w;
elsif	(ie_tipo_resp_credito_w = 'SE') then
	begin
	select	cd_pessoa_fisica,
		cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_sub_estipulante
	where	nr_sequencia	= nr_seq_subestipulante_w;
	exception
		when others then
		cd_pessoa_fisica_w	:= '';
		cd_cgc_w		:= '';
	end;
elsif	(ie_tipo_resp_credito_w = 'T') then
	select	nvl(nr_seq_titular, 0)
	into	nr_seq_titular_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	if	(nr_seq_titular_w	<> 0) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	pls_segurado
		where	nr_seq_titular	= nr_seq_titular_w;
	elsif	(nr_seq_titular_w	= 0) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
	end if;
	cd_cgc_w	:= '';
end if;

select	pls_obter_se_mes_fechado(dt_referencia_w,'T', cd_estabelecimento_p)
into	ie_mes_fechado_w
from	dual;

if 	(ie_mes_fechado_w = 'S') then
	--R.aise_application_error(-20011,'N�o � poss�vel realizar esta opera��o pois o m�s de compet�ncia ou a contabilidade do m�s est� fechada!');
	wheb_mensagem_pck.exibir_mensagem_abort(267408);
end if;

select	cd_moeda_padrao
into	cd_moeda_w
from	parametros_contas_pagar
where	cd_estabelecimento = cd_estabelecimento_p;

select	nvl(max(ie_forma_pagamento),3),
	nvl(max(ie_acao_nao_util),'M')
into	ie_forma_pagamento_w,
	ie_acao_venc_nao_util_w
from	condicao_pagamento
where	cd_condicao_pagamento = cd_condicao_pagamento_w
and	ie_situacao           = 'A';

begin
select	cd_tipo_taxa_juro,
	pr_juro_padrao,
	cd_tipo_taxa_multa,
	pr_multa_padrao,
	nr_seq_trans_fin_baixa_reemb,
	cd_conta_financ_reembolso
into	cd_tipo_taxa_juros_w,
	pr_juros_padrao_w,
	cd_tipo_taxa_multa_w,
	pr_multa_padrao_w,
	nr_seq_trans_fin_baixa_reemb_w,
	cd_conta_financ_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;
exception
when others then
	--R.aise_application_error(-20011,'Problema na leitura dos par�metros na fun��o OPS - Gest�o de Operadoras. [' || cd_estabelecimento_p || '] #@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(267409);
end;

open c01;
loop
fetch c01 into
	tx_fracao_parcela_w,
	tx_acrescimo_w,
	dt_vencimento_w;
exit when c01%notfound;
	if (nvl(tx_fracao_parcela_w,0) > 0) then
		vl_titulo_w 	:= ((vl_reembolso_w * tx_fracao_parcela_w) / 100);
	end if;
		
	if (tx_acrescimo_w <> 0) then
		vl_titulo_w 	:= vl_titulo_w + ((vl_titulo_w * tx_acrescimo_w) / 100);
	end if;
		
	select	titulo_pagar_seq.nextval
	into	nr_titulo_w
	from	dual;
		
	insert into titulo_pagar (	NR_TITULO,
					CD_ESTABELECIMENTO,
					DT_ATUALIZACAO,
					NM_USUARIO,
					DT_EMISSAO,
					DT_VENCIMENTO_ORIGINAL,
					DT_VENCIMENTO_ATUAL,
					VL_TITULO,
					VL_SALDO_TITULO,
					VL_SALDO_JUROS,
					VL_SALDO_MULTA,
					CD_MOEDA,
					TX_JUROS,
					TX_MULTA,
					CD_TIPO_TAXA_JURO,
					CD_TIPO_TAXA_MULTA,
					IE_SITUACAO,
					IE_ORIGEM_TITULO,
					IE_TIPO_TITULO,
					CD_PESSOA_FISICA,
					CD_CGC,
					IE_PLS,
					NR_SEQ_REEMBOLSO,
					NR_SEQ_TRANS_FIN_BAIXA,
					ie_status)
				values(	nr_titulo_w,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					dt_vencimento_w,
					dt_vencimento_w,
					vl_titulo_w,
					vl_titulo_w,
					0,
					0,
					cd_moeda_w,
					pr_juros_padrao_w,
					pr_multa_padrao_w,
					cd_tipo_taxa_juros_w,
					cd_tipo_taxa_multa_w,
					'A',
					'6',
					'10',
					cd_pessoa_fisica_w,
					cd_cgc_w,
					'S',
					nr_seq_reembolso_p,
					nr_seq_trans_fin_baixa_reemb_w,
					'D');
				ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_w,nm_usuario_p);
	
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_classif_w
	from	titulo_pagar_classif
	where	nr_titulo	= nr_titulo_w;
	
	insert into titulo_pagar_classif(	dt_atualizacao,
						nm_usuario,
						nr_seq_conta_financ,
						nr_sequencia,
						nr_titulo,
						vl_acrescimo,
						vl_desconto,
						vl_original,
						vl_titulo)
					values(	sysdate,
						nm_usuario_p,
						cd_conta_financ_w,
						nr_seq_classif_w,
						nr_titulo_w,
						0,
						0,
						vl_titulo_w,
						vl_titulo_w);
					
	update	pls_reembolso
	set	nr_titulo = nr_titulo_w,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_reembolso_p;
end loop;
close c01;

commit;

end pls_gerar_titulo_reembolso;
/