create or replace
procedure inserir_acomp_paciente(	nr_atendimento_p	number,
					cd_acompanhante_p	number,
					nm_usuario_p		varchar2,
					cd_unidade_basica_p	varchar2,
					cd_unidade_compl_p	varchar2,
					cd_setor_atend_p	varchar2,
					nr_seq_interno_p    number) is
begin

if	(nr_atendimento_p is not null) 	and
	(cd_acompanhante_p is not null) and
	(cd_unidade_basica_p is not null) and
	(cd_unidade_compl_p is not null) and
	(cd_setor_atend_p is not null) and
	(nm_usuario_p is not null)	then
	begin	
	
	insert	into	atendimento_acompanhante
			(nr_atendimento,
			dt_acompanhante,
			nr_acompanhante,
			dt_atualizacao,
			nm_usuario,
			nm_acompanhante,
			cd_pessoa_fisica,
			ie_alojamento,
			nr_seq_interno)
		values (nr_atendimento_p,
			sysdate,
			1,
			sysdate,
			nm_usuario_p,
			obter_nome_pf(cd_acompanhante_p),
			cd_acompanhante_p,
			'S',
			nr_seq_interno_p);
			
	update	unidade_atendimento
	set	ie_status_unidade	= 'M',
		nr_atendimento_Acomp	= nr_atendimento_p,
		cd_paciente_reserva	= cd_acompanhante_p,
		nm_usuario_reserva	= nm_usuario_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	cd_unidade_basica	= cd_unidade_basica_p
	and	cd_unidade_compl	= cd_unidade_compl_p
	and	cd_setor_atendimento	= cd_setor_atend_p;
	
	commit;

	end;
end if;

end inserir_acomp_paciente;
/

