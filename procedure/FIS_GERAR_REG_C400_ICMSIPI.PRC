create or replace
procedure fis_gerar_reg_C400_icmsipi( nr_seq_controle_p	number) is 

/*REGISTRO C470: ITENS DO DOCUMENTO FISCAL EMITIDO POR ECF (C�DIGO 02 e2D).*/

-- VARIABLES
ie_gerou_dados_bloco_w	varchar2(1) := 'N';

nr_seq_icmsipi_C400_w	fis_efd_icmsipi_C400.nr_sequencia%type;

qt_cursor_w 		number(10) := 0;
nr_vetor_w  		number(10) := 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

/*Cursor que retorna as informa��es para o registro c405 restringindo pela sequencia da nota fiscal*/
cursor c_ecf is
	select	a.dt_inicio_apuracao,
		a.dt_fim_apuracao,
		a.cd_estabelecimento,
		c.cd_operacao_nf,
		c.cd_serie_nf,     
		c.ds_ecf_mod,
		c.nr_ecf_cx,       
		c.nr_ecf_fab,
		'2D' cd_mod
	from	fis_efd_icmsipi_controle 	a,
		fis_efd_icmsipi_lote		b,
		fis_efd_icmsipi_regra_c400	c
	where	a.nr_seq_lote	= b.nr_sequencia
	and 	b.nr_sequencia	= c.nr_seq_lote
	and 	a.nr_sequencia 	= nr_seq_controle_p;

/*Cria��o do array com o tipo sendo do cursor eespecificado - c_itens_cupom_fiscal*/
type reg_c_ecf is table of c_ecf%RowType;
vet_c_ecf reg_c_ecf;

/*Cria��o do array com o tipo sendo da tabela eespecificada - fis_efd_icmsipi_C400 */
type registro is table of fis_efd_icmsipi_C400%rowtype index by pls_integer;
fis_registros_w registro;

begin
/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_ecf;
loop
fetch c_ecf bulk collect into vet_c_ecf limit 1000;
	for i in 1 .. vet_c_ecf.Count loop
		begin

		/*Incrementa a variavel para o array*/
		qt_cursor_w:=	qt_cursor_w + 1;

		fis_gerar_reg_C460_icmsipi(	nr_seq_controle_p,
						vet_c_ecf(i).nr_ecf_cx,
						vet_c_ecf(i).dt_inicio_apuracao,
						vet_c_ecf(i).dt_fim_apuracao,
						vet_c_ecf(i).cd_estabelecimento,
						vet_c_ecf(i).cd_operacao_nf,
						vet_c_ecf(i).cd_serie_nf);
		
		fis_gerar_reg_C405_icmsipi(	nr_seq_controle_p,
						vet_c_ecf(i).nr_ecf_cx);
		
		fis_gerar_reg_C420_icmsipi(	nr_seq_controle_p, 
                                vet_c_ecf(i).nr_ecf_cx );

		fis_gerar_reg_C425_icmsipi(	nr_seq_controle_p, 
                                vet_c_ecf(i).nr_ecf_cx);
    
		if (ie_gerou_dados_bloco_w = 'N') then
			ie_gerou_dados_bloco_w := 'S';
		end if;
		
		/*B	usca da sequencia da tabela especificada - fis_efd_icmsipi_C400 */
		select	fis_efd_icmsipi_C400_seq.nextval
		into	nr_seq_icmsipi_C400_w
		from	dual;

		/*Inserindo valores no array para realiza��o do forall posteriormente*/
		fis_registros_w(qt_cursor_w).nr_sequencia         := nr_seq_icmsipi_C400_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao       := sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario           := nm_usuario_w;
		fis_registros_w(qt_cursor_w).dt_atualizacao_nrec  := sysdate;
		fis_registros_w(qt_cursor_w).nm_usuario_nrec      := nm_usuario_w;
		fis_registros_w(qt_cursor_w).cd_reg               := 'C400';
		fis_registros_w(qt_cursor_w).cd_mod		  := vet_c_ecf(i).cd_mod;
  		fis_registros_w(qt_cursor_w).ds_ecf_mod		  := vet_c_ecf(i).ds_ecf_mod;
		fis_registros_w(qt_cursor_w).nr_ecf_fab		  := vet_c_ecf(i).nr_ecf_fab;
		fis_registros_w(qt_cursor_w).nr_ecf_cx		  := vet_c_ecf(i).nr_ecf_cx;
		fis_registros_w(qt_cursor_w).nr_seq_controle	  := nr_seq_controle_p;

		end;
	end loop;
exit when c_ecf%notfound;
end loop;
close c_ecf;

if (fis_registros_w.count > 0) then
	begin
	/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_C400 values fis_registros_w (i);

	fis_registros_w.delete;

	commit;

	end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

/*Atualiza��o informa��o no controle de gera��o de registro para SIM*/
if (ie_gerou_dados_bloco_w = 'S') then
	update	fis_efd_icmsipi_controle
	set	ie_mov_C 	= 'S'
	where	nr_sequencia 	= nr_seq_controle_p;
end if;

end fis_gerar_reg_C400_icmsipi;
/