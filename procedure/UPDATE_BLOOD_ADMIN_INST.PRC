CREATE OR replace PROCEDURE update_blood_admin_inst (
    cd_cpoe_item_seq_p   NUMBER,
    ie_type_action_p       VARCHAR2) IS
BEGIN

IF ( cd_cpoe_item_seq_p IS NOT NULL ) THEN
    IF ( ie_type_action_p = 'U' ) THEN

        UPDATE blood_admin_instructions
        SET     cd_cpoe_item_seq = cd_cpoe_item_seq_p
        WHERE   cd_cpoe_item_seq_edit = cd_cpoe_item_seq_p
        AND     cd_cpoe_item_seq IS NULL;

    ELSIF ( ie_type_action_p = 'D' ) THEN
        DELETE  blood_admin_instructions
        WHERE   cd_cpoe_item_seq_edit = cd_cpoe_item_seq_p
        AND     cd_cpoe_item_seq IS NULL;

    ELSIF ( ie_type_action_p = 'C' ) THEN
        DELETE  blood_admin_instructions
        WHERE   cd_cpoe_item_seq_edit = cd_cpoe_item_seq_p;

    END IF;

    DELETE  blood_admin_instructions
    WHERE   dt_atualizacao_nrec < sysdate - 1
    AND     cd_cpoe_item_seq IS NULL;

    COMMIT;

END IF;

end update_blood_admin_inst;
/