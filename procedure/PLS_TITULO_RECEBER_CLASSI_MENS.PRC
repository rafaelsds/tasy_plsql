create or replace
procedure pls_titulo_receber_classi_mens(	nr_seq_mensalidade_p	pls_mensalidade.nr_sequencia%type,
						nr_titulo_p		titulo_receber.nr_titulo%type,
						vl_total_p		titulo_receber.vl_titulo%type,
						ie_commit_p		varchar2,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type) is 

nr_seq_classif_w	titulo_receber_classif.nr_sequencia%type;
cd_conta_financ_w	number(10);	
cd_conta_contabil_w	varchar2(20);
vl_classif_total_w	number(15,2);
	
Cursor C01 (	nr_seq_mensalidade_pc	pls_mensalidade.nr_sequencia%type) is
	select	a.cd_conta_deb,
		sum(a.vl_item) vl_item,
		c.nr_seq_contrato,
		a.ie_tipo_item,
		a.nr_seq_tipo_lanc,
		a.cd_conta_financ
	from	pls_mensalidade_seg_item	a,
		pls_mensalidade_segurado	b,
		pls_mensalidade			c
	where	a.nr_seq_mensalidade_seg	= b.nr_sequencia
	and	b.nr_seq_mensalidade		= c.nr_sequencia
	and	c.nr_sequencia			= nr_seq_mensalidade_pc
	and	a.ie_tipo_item not in ('6','7')
	group by	
		a.cd_conta_deb,
		c.nr_seq_contrato,
		a.ie_tipo_item,
		a.nr_seq_tipo_lanc,
		a.cd_conta_financ
	union all
	select	e.cd_conta_deb,
		sum(d.vl_item) vl_item,
		c.nr_seq_contrato,
		a.ie_tipo_item,
		a.nr_seq_tipo_lanc,
		a.cd_conta_financ
	from 	pls_mensalidade_seg_item   a,
		pls_mensalidade_segurado   b,
		pls_mensalidade            c,
		pls_mensalidade_item_conta d,
		pls_conta_pos_estabelecido e
	where 	a.nr_sequencia = d.nr_seq_item
	and 	a.nr_seq_mensalidade_seg = b.nr_sequencia
	and 	e.nr_sequencia = d.nr_seq_conta_pos_estab
	and 	b.nr_seq_mensalidade = c.nr_sequencia
	and 	c.nr_sequencia = nr_seq_mensalidade_pc
	and 	a.ie_tipo_item in ('6', '7')
	group by
	       e.cd_conta_deb,
	       c.nr_seq_contrato,
	       a.ie_tipo_item,
	       a.nr_seq_tipo_lanc,
	       a.cd_conta_financ;

begin

vl_classif_total_w	:= 0;

for r_c01_w in c01 (nr_seq_mensalidade_p) loop
	begin
	if	(r_c01_w.cd_conta_financ is null) then
		pls_obter_conta_financ_regra(	'M',			nr_seq_mensalidade_p,		cd_estabelecimento_p,
						r_c01_w.ie_tipo_item,	r_c01_w.nr_seq_tipo_lanc,	null,
						null,			null,				null,
						null,			null,				null,
						null,			null,				null,
						null,			null,				cd_conta_financ_w);
	else
		cd_conta_financ_w	:= r_c01_w.cd_conta_financ;
	end if;

	if	(r_c01_w.ie_tipo_item = '3') then
		select	max(c.cd_conta_deb)
		into	cd_conta_contabil_w
		from	pls_mensalidade_segurado a,
			pls_mensalidade_seg_item b,
			pls_conta_coparticipacao c
		where	a.nr_sequencia	= b.nr_seq_mensalidade_seg
		and	b.nr_seq_conta	= c.nr_seq_conta
		and	a.nr_seq_mensalidade = nr_seq_mensalidade_p
		and	b.ie_tipo_item	= r_c01_w.ie_tipo_item;
	else
		cd_conta_contabil_w	:= r_c01_w.cd_conta_deb;
	end if;

	select	max(a.nr_sequencia)
	into	nr_seq_classif_w
	from	titulo_receber_classif a
	where	a.nr_titulo = nr_titulo_p
	and	nvl(a.cd_conta_financ,0)	= nvl(cd_conta_financ_w,0)
	and	nvl(a.cd_conta_contabil,0)	= nvl(r_c01_w.cd_conta_deb,0);
	
	if	(nr_seq_classif_w is null) then
		select	nvl(max(a.nr_sequencia),0) + 1
		into	nr_seq_classif_w
		from	titulo_receber_classif a
		where	a.nr_titulo	= nr_titulo_p;

		insert into titulo_receber_classif
			(nr_titulo,
			nr_sequencia,
			vl_classificacao,
			dt_atualizacao,
			nm_usuario,
			cd_conta_financ,
			vl_desconto,
			vl_original,
			nr_seq_contrato,
			cd_conta_contabil)
		values	(nr_titulo_p,
			nr_seq_classif_w,
			r_c01_w.vl_item,
			sysdate,
			nm_usuario_p,
			cd_conta_financ_w,
			0,
			r_c01_w.vl_item,
			r_c01_w.nr_seq_contrato,
			cd_conta_contabil_w);
	else
		update	titulo_receber_classif
		set	vl_classificacao	= vl_classificacao + r_c01_w.vl_item,
			vl_original		= vl_original + r_c01_w.vl_item
		where	nr_titulo		= nr_titulo_p
		and	nr_sequencia		= nr_seq_classif_w;
	end if;

	vl_classif_total_w		:= r_c01_w.vl_item + vl_classif_total_w;	
	end;
end loop;

if	(vl_total_p <> vl_classif_total_w) then
	update	titulo_receber_classif
	set	vl_classificacao	= vl_classificacao + vl_total_p - vl_classif_total_w,
		vl_original		= vl_original + vl_total_p - vl_classif_total_w
	where	nr_titulo		= nr_titulo_p
	and	nr_sequencia		= nr_seq_classif_w;
end if;

if	(ie_commit_p = 'S') then
	commit;
end if;
	
end pls_titulo_receber_classi_mens;
/