create or replace 
procedure gerar_prontuario_pac(	cd_estabelecimento_p	number,
				cd_pessoa_fisica_p		varchar2,
				ie_commit_p		varchar2,
				nm_usuario_p		varchar2,
				nr_prontuario_p	out	number) is

nr_prontuario_w		number(10);
nr_sequencia_w		number(10);
ie_regra_pront_w		varchar2(15);
qt_registro_w		number(10);
nr_novo_prontuario_w	number(10);
ie_novo_pront_w		boolean;
qt_pront_w		number(10);
ie_reutiliza_pront_w	varchar2(01);
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
ie_pront_igual_cdpf_w	varchar2(1);
cd_pessoa_pront_w	pessoa_fisica.cd_pessoa_fisica%type;


BEGIN
cd_pessoa_fisica_w:= null;

select	nvl(max(vl_parametro),'BASE')
into	ie_regra_pront_w
from	funcao_parametro
where	cd_funcao	= 0
and	nr_sequencia	= 120;

if	(cd_pessoa_fisica_p is not null) then
	begin 
	Obter_Param_Usuario(5, 138, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_pront_igual_cdpf_w);
	
	if	(ie_pront_igual_cdpf_w = 'S') then
		
		select	max(cd_pessoa_fisica) 
		into	cd_pessoa_pront_w
		from	pessoa_fisica 
		where	nr_prontuario = cd_pessoa_fisica_p;
		

		if	(cd_pessoa_pront_w is not null) then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(131066, 'CD_PESSOA_FISICA=' || cd_pessoa_pront_w);
		end if;
		nr_prontuario_w	:= cd_pessoa_fisica_p;
		
		update pessoa_fisica
		set	   nr_prontuario = cd_pessoa_fisica_p,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	 cd_pessoa_fisica = cd_pessoa_fisica_p;
	else
		if (ie_regra_pront_w = 'BASE') then
		
			Obter_Param_Usuario(916, 597, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_reutiliza_pront_w);
			
			if (ie_reutiliza_pront_w = 'S') then
			
				nr_novo_prontuario_w := 0;
				ie_novo_pront_w := true;
				
				while	(ie_novo_pront_w) loop
					begin
					nr_novo_prontuario_w := nr_novo_prontuario_w + 1;
					
					select	count(*)
					into	qt_pront_w
					from	pessoa_fisica x
					where	x.nr_prontuario = nr_novo_prontuario_w;
					
					if (qt_pront_w = 0) then
						ie_novo_pront_w := false;
						nr_prontuario_w := nr_novo_prontuario_w;
					end if;

					end;
				end loop;
				
			else 				
				select	prontuario_seq.nextval
				into	nr_prontuario_w
				from	dual;
			end if;
			
      update pessoa_fisica
      set	   nr_prontuario  = nr_prontuario_w,
              nm_usuario	    = nm_usuario_p,
              dt_atualizacao = sysdate
      where	 cd_pessoa_fisica	= cd_pessoa_fisica_p;
			
		elsif	(ie_regra_pront_w = 'ESTAB') and
			(nvl(cd_estabelecimento_p,0) <> 0) then

			select	count(*)
			into	qt_registro_w
			from	regra_prontuario_estab
			where	cd_estabelecimento	= cd_estabelecimento_p;
			
			if	(qt_registro_w = 0) then
				WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(281294, 'CD_ESTABELECIMENTO=' || cd_estabelecimento_p);
			end if;
			
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	pessoa_fisica_pront_estab
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	cd_estabelecimento	= cd_estabelecimento_p;
			
			if	(nvl(nr_sequencia_w,0) = 0) then
				
				lock table regra_prontuario_estab in exclusive mode;
				
				select	nvl(max(nr_ult_prontuario),0) + 1
				into	nr_prontuario_w
				from	regra_prontuario_estab
				where	cd_estabelecimento	= cd_estabelecimento_p;
				
				select	max(cd_pessoa_fisica)
				into	cd_pessoa_fisica_w
				from	pessoa_fisica_pront_estab
				where	cd_estabelecimento	= cd_estabelecimento_p
				and	nr_prontuario		= nr_prontuario_w;
				
				if	(nvl(cd_pessoa_fisica_w,'X') <> 'X') then
					WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(281297, 'NR_PRONTUARIO=' || nr_prontuario_w || ';CD_PESSOA_FISICA=' || cd_pessoa_fisica_w);
				end if;
			
				update	regra_prontuario_estab
				set	nr_ult_prontuario	= nr_prontuario_w,
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	cd_estabelecimento	= cd_estabelecimento_p;
				
				insert into pessoa_fisica_pront_estab(
					nr_sequencia,
					cd_pessoa_fisica,
					cd_estabelecimento,
					nr_prontuario,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec)
				values(	pessoa_fisica_pront_estab_seq.nextval,
					cd_pessoa_fisica_p,
					cd_estabelecimento_p,
					nr_prontuario_w,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate);
			end if;
			
		end if;
	end if;
		
	if	(ie_commit_p = 'S') then
		commit;
	end if;
	end;
end if;

nr_prontuario_p	:= nr_prontuario_w;

END gerar_prontuario_pac;
/
