create or replace
procedure pls_alterar_qt_dias_abrang_ant
			(	nr_seq_carencia_p	number,
				qt_dias_abrang_ant_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

update	pls_carencia
set	qt_dias_fora_abrang_ant	= qt_dias_abrang_ant_p
where	nr_sequencia		= nr_seq_carencia_p;

commit;

end pls_alterar_qt_dias_abrang_ant;
/