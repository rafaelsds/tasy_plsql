create or replace
procedure pls_inserir_incons_lib_fat(	nr_seq_lib_fat_conta_p		pls_lib_fat_conta.nr_sequencia%type,
					nr_seq_inconsistencia_p		pls_inconsistencia_lib_fat.nr_sequencia%type,
					ds_complemento_p		pls_lib_fat_conta_incons.ds_complemento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_commit_p			varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

begin
insert into pls_lib_fat_conta_incons
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_lib_fat_conta,
	nr_seq_inconsistencia,
	ds_complemento)
values	(pls_lib_fat_conta_incons_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_lib_fat_conta_p,
	nr_seq_inconsistencia_p,
	ds_complemento_p);

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;

end pls_inserir_incons_lib_fat;
/