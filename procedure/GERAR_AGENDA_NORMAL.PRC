create or replace
procedure gerar_agenda_normal(	nr_seq_agenda_p		number,
				cd_cgc_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

ie_gerar_autor_cirurgia_w	varchar2(1)	:= 'N';
nr_seq_proc_interno_w		number(10);
ie_atualizar_status_agenda_w	varchar2(15)	:= 'S';
ie_status_agenda_atual_w	varchar2(15);
nr_cirurgia_w			number(10,0);
nr_seq_evento_w			number(10);											
nr_atendimento_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_agenda_w			number(10);
hr_inicio_w			date;
cd_medico_w			varchar2(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
qt_idade_w			number(10);
qt_opme_w			Number(10);
ie_atualizar_agenda_w		varchar2(1);
nr_seq_anterior_w		number(10);
cd_convenio_w			number(15);
ie_status_agenda_w		varchar2(3) := null;
ds_observacao_w			varchar2(255);

Cursor	C02	is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_evento_disp	= 'ANO'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	qt_opme_w > 0
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	(obter_regra_alerta_agenda(nr_sequencia,cd_agenda_w,ie_status_agenda_w) = 'S')
	and	nvl(a.ie_situacao,'A') = 'A';
	
Cursor C03 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_evento_disp	= 'ANSO'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	qt_opme_w = 0
	and	nvl(nr_seq_classif,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)
	and	(obter_regra_alerta_agenda(nr_sequencia,cd_agenda_w,ie_status_agenda_w) = 'S')
	and	nvl(a.ie_situacao,'A') = 'A';

begin

select	max(ie_status_agenda),
	max(nr_cirurgia),
	max(cd_convenio),
	substr(max(ds_observacao),1,255)
into	ie_status_agenda_atual_w,
	nr_cirurgia_w,
	cd_convenio_w,
	ds_observacao_w
from	agenda_paciente
where	nr_sequencia	=	nr_seq_agenda_p;

if	(nr_cirurgia_w > 0) and (ie_status_agenda_atual_w = 'C') then
	update	cirurgia
	set	ie_status_cirurgia	=	'1'
	where	nr_cirurgia		=	nr_cirurgia_w
	and	ie_status_cirurgia	=	'3';
end if;


/* Francisco - OS 92816 - 11/06/2008 */
ie_gerar_autor_cirurgia_w	:= obter_valor_param_usuario(871,85,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_atualizar_status_agenda_w	:= obter_valor_param_usuario(871,213,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_atualizar_agenda_w		:= obter_valor_param_usuario(871,472,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);

select	nvl(max(nr_seq_anterior),0)
into	nr_seq_anterior_w
from	agenda_paciente
where	nr_sequencia		=	nr_seq_agenda_p;

if 	(ie_atualizar_agenda_w = 'N') and 
	(nr_seq_anterior_w > 0) then
	nr_seq_anterior_w := 0;
elsif	(ie_atualizar_status_agenda_w = 'S') then
	ajustar_horario_agenda_cir(nr_seq_agenda_p,'N');
	update	agenda_paciente
	set	ie_status_agenda	=	'N'
	where	nr_sequencia		=	nr_seq_agenda_p;
elsif	(ie_atualizar_status_agenda_w = 'SE') then
	ajustar_horario_agenda_cir(nr_seq_agenda_p,'N');
	update	agenda_paciente
	set	ie_status_agenda	=	'N'
	where	nr_sequencia		=	nr_seq_agenda_p
	and	ie_status_agenda	<>	'E';
end if;

if	((ie_gerar_autor_cirurgia_w in ('S','A')) and (cd_convenio_w is not null)) then
	gerar_autor_cirurgia_opme(nr_seq_agenda_p, cd_cgc_p, cd_estabelecimento_p,nm_usuario_p,ie_gerar_autor_cirurgia_w);
end if;

/* Fim altera��o Francisco - OS 92816 - 11/06/2008 */

/* Francisco - OS 102081 - 07/08/2008 */
select	max(nr_seq_proc_interno)
into	nr_seq_proc_interno_w
from	agenda_paciente
where	nr_sequencia	= nr_seq_agenda_p;

gerar_autor_regra(null,null,null,null,null,null,'AP',nm_usuario_p,nr_seq_agenda_p,nr_seq_proc_interno_w,null,null,null,null,'','','');

select	count(*)
into	qt_opme_w
from	agenda_pac_opme
where	nr_Seq_agenda	= nr_seq_agenda_p
and	nvl(ie_origem_inf,'I') = 'I';

if	(qt_opme_w > 0) then
	select	nr_atendimento,
		cd_pessoa_fisica,
		cd_agenda,
		hr_inicio,
		cd_medico,
		cd_procedimento,
		ie_origem_proced,
		cd_convenio,
		ie_status_agenda
	into
		nr_atendimento_w,
		cd_pessoa_fisica_w,
		cd_agenda_w,
		hr_inicio_w,
		cd_medico_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_convenio_w,
		ie_status_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);

	open C02;
	loop
	fetch C02 into	
		nr_seq_evento_w;
	exit when C02%notfound;
		begin
		gerar_evento_agenda_trigger(	nr_seq_evento_w,
						nr_atendimento_w,
						cd_pessoa_fisica_w,
						null,
						nm_usuario_p,
						cd_agenda_w, 
						hr_inicio_w, 
						cd_medico_w, 
						cd_procedimento_w,
						ie_origem_proced_w, 
						null,
						null,
						null,
						null,
						cd_convenio_w,
						null,
						'N',
						nr_seq_agenda_p,
						null,
						null,
						null,
						null,
						ds_observacao_w);
		end;
	end loop;
	close C02;

else  
	select	nr_atendimento,
		cd_pessoa_fisica,
		cd_agenda,
		hr_inicio,
		cd_medico,
		cd_procedimento,
		ie_origem_proced,
		cd_convenio,
		ie_status_agenda
	into
		nr_atendimento_w,
		cd_pessoa_fisica_w,
		cd_agenda_w,
		hr_inicio_w,
		cd_medico_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_convenio_w,
		ie_status_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_evento_w;
	exit when C03%notfound;
		begin
		gerar_evento_agenda_trigger(	nr_seq_evento_w,
						nr_atendimento_w,
						cd_pessoa_fisica_w,
						null,
						nm_usuario_p,
						cd_agenda_w, 
						hr_inicio_w, 
						cd_medico_w, 
						cd_procedimento_w,
						ie_origem_proced_w, 
						null,
						null,
						null,
						null,
						cd_convenio_w,
						null,
						'N',
						nr_seq_agenda_p,
						null,
						null,
						null,
						null,
						ds_observacao_w);
		end;
	end loop;
	close C03;
end if;

commit;

end gerar_agenda_normal;
/