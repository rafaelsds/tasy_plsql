create or replace
procedure qua_atualiza_status_ap(
	nr_sequencia_p		number,
	ie_status_p		varchar2,
	nm_usuario_p		Varchar2) is 

begin

if	( nr_sequencia_p is not null ) then
	begin

update qua_analise_problema
set    ie_status = ie_status_p,
       dt_atualizacao = sysdate,
       nm_usuario = nm_usuario_p
where  nr_seq_rnc = nr_sequencia_p;
	
	end;
end if;

commit;

end qua_atualiza_status_ap;
/

