create or replace
procedure aacd_gravar_incons_carga_tit ( ds_inconsistencia_p 	varchar2,
										 nr_titulo_externo_p	varchar2,
										 nm_usuario_p			Varchar2,
										 nr_seq_lote_p			number,
										 nm_rotina_p			varchar2) is 

begin

insert into aacd_log_incons_carga_tit (	nr_titulo_externo,
										nm_usuario,
										dt_importacao,
										ds_inconsistencia,
										nr_seq_lote,
										ds_rotina )
							values    ( substr(nr_titulo_externo_p,1,255),
										substr(nm_usuario_p,1,15),
										sysdate,
										substr(ds_inconsistencia_p,1,3999),
										nr_seq_lote_p,
										substr(nm_rotina_p,1,50));

end aacd_gravar_incons_carga_tit;
/