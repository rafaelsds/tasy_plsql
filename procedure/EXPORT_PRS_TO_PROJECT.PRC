CREATE OR REPLACE PROCEDURE EXPORT_PRS_TO_PROJECT( NR_SEQ_REQUISITO_P NUMBER, NR_SEQ_PRS_P NUMBER, NM_USUARIO_P VARCHAR2) IS

  w_prs_ds_title    REG_PRODUCT_REQUIREMENT.ds_title%TYPE;
  

BEGIN

  BEGIN
   
    SELECT ds_title INTO w_prs_ds_title FROM REG_PRODUCT_REQUIREMENT WHERE nr_sequencia = NR_SEQ_PRS_P;

  EXCEPTION
    WHEN Others THEN
      NULL;
  END;

                                                                                    
  BEGIN                                                                                    

   INSERT INTO DES_REQUISITO_ITEM (nr_sequencia
                                    ,nr_seq_requisito
                                    ,dt_atualizacao
                                    ,nm_usuario
                                    ,dt_atualizacao_nrec
                                    ,nm_usuario_nrec
                                    ,nr_seq_apresentacao
                                    ,ds_item
                                    ,ds_titulo
                                    ,ie_status
                                    ,ie_tipo_requisito
                                    ,pr_realizacao
                                    ,nr_seq_classif
                                    ,ds_observacao
                                    ,qt_previsto
                                    ,nr_seq_motivo_pend
                                    ,nr_requisito
                                    ,nr_seq_modulo
                                    ,nr_seq_linha_base
                                    ,nr_seq_ordem_serv
                                    ,nr_seq_req_origem
                                    ,nr_seq_pr
                                    ,ie_caso_teste) VALUES (
                                    DES_REQUISITO_ITEM_SEQ.NEXTVAL, --nr_sequencia
                                    NR_SEQ_REQUISITO_P,--nr_seq_requisito
                                    SYSDATE,--dt_atualizacao
                                    NM_USUARIO_P,--nm_usuario
                                    SYSDATE,--dt_atualizacao_nrec
                                    NM_USUARIO_P,--nm_usuario_nrec
                                    100,--nr_seq_apresentacao
                                    w_prs_ds_title,--ds_item
                                    w_prs_ds_title,--ds_titulo
                                    'N',--ie_status
                                    'RF',--ie_tipo_requisito
                                    NULL,--pr_realizacao
                                    NULL,--nr_seq_classif
                                    NULL,--ds_observacao
                                    NULL,--qt_previsto
                                    NULL,--nr_seq_motivo_pend
                                    1,--nr_requisito
                                    NULL,--nr_seq_modulo
                                    NULL,--nr_seq_linha_base
                                    NULL,--nr_seq_ordem_serv
                                    NULL,--nr_seq_req_origem
                                    NR_SEQ_PRS_P,--nr_seq_pr
                                    NULL--ie_caso_teste
                                    );

  EXCEPTION
    WHEN Others THEN
      NULL;
  END;                                                                                                                            
END EXPORT_PRS_TO_PROJECT;
/