create or replace
procedure Excluir_Itens_Proc_Princ
		(nr_sequencia_p		number,
		cd_motivo_exc_conta_p	number,
		ds_compl_motivo_excon_p	varchar2,
		nm_usuario_p		varchar2) is
		

ie_proc_mat_w			varchar2(01);
nr_sequencia_w			number(10,0);
nr_interno_conta_w		number(10,0);
ie_status_acerto_w		varchar2(05);
qt_material_w			number(15,4);
cd_setor_atendimento_w		number(10,0);
cd_material_w			number(10,0);
ie_proc_mat_w2			varchar2(01);
nr_sequencia_w2			number(10,0);
nr_interno_conta_w2		number(10,0);


cursor	c01 is
	select	'P' ie_proc_mat,
		a.nr_sequencia,
		a.nr_interno_conta
	from	procedimento_paciente a
	where	a.nr_seq_proc_princ	= nr_sequencia_p
	union
	select	'M' ie_proc_mat,
		a.nr_sequencia,
		a.nr_interno_conta
	from	material_atend_paciente a
	where	a.nr_seq_proc_princ	= nr_sequencia_p;

cursor	c02 is
	select	'P' ie_proc_mat,
		a.nr_sequencia,
		a.nr_interno_conta
	from	procedimento_paciente a
	where	a.nr_seq_proc_princ	= nr_sequencia_w
	union
	select	'M' ie_proc_mat,
		a.nr_sequencia,
		a.nr_interno_conta
	from	material_atend_paciente a
	where	a.nr_seq_proc_princ	= nr_sequencia_w;

	

begin

open	c01;
loop
fetch	c01 into	
	ie_proc_mat_w,
	nr_sequencia_w,
	nr_interno_conta_w;
exit	when c01%notfound;
	begin

	select	NVL(max(ie_status_acerto),1)
	into	ie_status_acerto_w
	from	conta_paciente
	where	nr_interno_conta	= nr_interno_conta_w;
	
	if	(ie_status_acerto_w = 1) then
		begin

		if	(ie_proc_mat_w	= 'P') then
			begin

			open	c02;
			loop
			fetch	c02 into	
				ie_proc_mat_w2,
				nr_sequencia_w2,
				nr_interno_conta_w2;
			exit	when c02%notfound;
				begin

				if	(ie_proc_mat_w2	= 'P') then
					if	(nvl(cd_motivo_exc_conta_p,0) <> 0) then
						excluir_matproc_conta(nr_sequencia_w2, nr_interno_conta_w2, cd_motivo_exc_conta_p, ds_compl_motivo_excon_p, 'P', nm_usuario_p);
					else
						delete 	from procedimento_paciente
						where	nr_sequencia	= nr_sequencia_w2;
					end if;
				end if;

				if	(ie_proc_mat_w2	= 'M') then
					begin
					if	(nvl(cd_motivo_exc_conta_p,0) <> 0) then
						excluir_matproc_conta(nr_sequencia_w2, nr_interno_conta_w2, cd_motivo_exc_conta_p, ds_compl_motivo_excon_p, 'M', nm_usuario_p);
					else
						delete 	from material_atend_paciente
						where	nr_sequencia	= nr_sequencia_w2;
					end if;				
					end;
				end if;

				end;
			end loop;
			close c02;
			
			if	(nvl(cd_motivo_exc_conta_p,0) <> 0) then
				excluir_matproc_conta(nr_sequencia_w, nr_interno_conta_w, cd_motivo_exc_conta_p, ds_compl_motivo_excon_p, 'P', nm_usuario_p);
			else
				delete 	from procedimento_paciente
				where	nr_sequencia	= nr_sequencia_w;
			end if;

			end;
		end if;

		if	(ie_proc_mat_w	= 'M') then
			begin

			select	cd_material,
				cd_setor_atendimento
			into	cd_material_w,
				cd_setor_atendimento_w
			from	material_atend_paciente
			where	nr_sequencia	= nr_sequencia_w;

			
			select	count(*)
			into	qt_material_w
			from	material_atend_paciente
			where	cd_setor_atendimento	= cd_setor_atendimento_w
			and	cd_material		= cd_material_w
			and	nr_interno_conta	= nr_interno_conta_w
			and	qt_material		< 0;

			if	(qt_material_w = 0) then
				if	(nvl(cd_motivo_exc_conta_p,0) <> 0) then
					excluir_matproc_conta(nr_sequencia_w, nr_interno_conta_w, cd_motivo_exc_conta_p, ds_compl_motivo_excon_p, 'M', nm_usuario_p);
				else
					delete 	from material_atend_paciente
					where	nr_sequencia	= nr_sequencia_w;
				end if;
			else
				update	material_atend_paciente
				set	nr_seq_proc_princ	= null
				where	nr_sequencia	= nr_sequencia_w;
			end if;

			end;
		end if;

		end;
	end if;
	

	nr_sequencia_w	:= nr_sequencia_w;

	end;
end loop;
close	c01;

commit;

end Excluir_Itens_Proc_Princ;
/
	