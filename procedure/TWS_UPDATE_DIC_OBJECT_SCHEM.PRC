create or replace
procedure tws_update_dic_object_schem (	nr_seq_objeto_schematic_p	objeto_schematic.nr_sequencia%type,					
					nr_seq_dic_objeto_p		dic_objeto.nr_sequencia%type,		
					nm_usuario_p			usuario.nm_usuario%type) is 
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar a sequencia do DIC_OBJETO  na tabela do Schematic
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
Tasy HTML5
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


begin

if	( nr_seq_objeto_schematic_p is not null and nr_seq_dic_objeto_p is not null ) then

	update	objeto_schematic
	set	dt_atualizacao 		= sysdate,
		nm_usuario 		= nm_usuario_p,
		nr_seq_dic_objeto 	= nr_seq_dic_objeto_p
	where	nr_sequencia 		= nr_seq_objeto_schematic_p;
	
	commit;
end if;

end tws_update_dic_object_schem;
/