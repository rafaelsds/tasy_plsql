create or replace
procedure SUS_Consiste_Regra_Laudo(
			cd_estabelecimento_p		number,
			cd_procedimento_p		number,
			ie_origem_proced_p		number,
			ds_mensagem_p		out	varchar2) is 

nr_seq_forma_org_w		Number(10) := 0;
nr_seq_grupo_w			Number(10) := 0;
nr_seq_subgrupo_w		Number(10) := 0;
ie_tipo_financiamento_w		varchar2(4);
ie_complexidade_w		varchar2(2);
ds_mensagem_w			varchar2(255);
			
Cursor C01 is
	select	ds_mensagem
	from	sus_regra_laudo a
	where	a.cd_estabelecimento	 = cd_estabelecimento_p
	and	nvl(a.cd_procedimento,cd_procedimento_p) 		= cd_procedimento_p
	and	((cd_procedimento is null) or (nvl(a.ie_origem_proced,ie_origem_proced_p)= ie_origem_proced_p))
	and	nvl(a.nr_seq_grupo,nr_seq_grupo_w)			= nr_seq_grupo_w	
	and	nvl(a.nr_seq_subgrupo,nr_seq_subgrupo_w)		= nr_seq_subgrupo_w
	and	nvl(a.nr_seq_forma_org,nr_seq_forma_org_w)		= nr_seq_forma_org_w
	and	nvl(ie_complexidade,ie_complexidade_w)			= ie_complexidade_w
	and	nvl(ie_tipo_financiamento,ie_tipo_financiamento_w)	= ie_tipo_financiamento_w
	order by 	nvl(a.cd_procedimento,0),
			nvl(a.nr_seq_forma_org,0),
			nvl(a.nr_seq_subgrupo,0),
			nvl(a.nr_seq_grupo,0),
			nvl(ie_tipo_financiamento,'0'),
			nvl(ie_complexidade,'0');
begin

begin
select	nr_seq_forma_org,
	nr_seq_grupo,
	nr_seq_subgrupo,
	ie_complexidade,
	ie_tipo_financiamento
into	nr_seq_forma_org_w,
	nr_seq_grupo_w,
	nr_seq_subgrupo_w,
	ie_complexidade_w,
	ie_tipo_financiamento_w
from	sus_estrutura_procedimento_v a
where	a.cd_procedimento	= cd_procedimento_p
and	a.ie_origem_proced	= ie_origem_proced_p;
exception
	when others then
	null;
end;
open C01;
loop
fetch C01 into	
	ds_mensagem_w;
exit when C01%notfound;
end loop;
close C01;

ds_mensagem_p	:= ds_mensagem_w;


end SUS_Consiste_Regra_Laudo;
/