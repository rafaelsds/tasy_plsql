create or replace
procedure ctb_pls_contabilizar_comissao( nr_lote_contabil_p	number,
					nm_usuario_p		varchar2,
					ie_exclusao_p		varchar2,
					ds_retorno_p  in out	varchar2) is 

dt_referencia_w				date;
dt_ref_inicial_w			date;
dt_ref_final_w				date;
qt_lote_contabil_w			number(4);
cd_estabelecimento_w			number(10);
nr_seq_agrupamento_w			number(10);
nr_seq_w_movto_cont_w			number(10);
ie_debito_credito_w			varchar(1);
nm_agrupador_w				varchar2(255);
nm_vendedor_w				varchar2(255);
ds_compl_historico_w			varchar2(2000);
vl_retorno_w				varchar2(2000);
ds_conteudo_w				varchar2(4000);
nr_seq_regra_cc_w			pls_regra_centro_custo.nr_sequencia%type;
nr_lote_contabil_w			lote_contabil.nr_lote_contabil%type;
cd_tipo_lote_contabil_w			tipo_lote_contabil.cd_tipo_lote_contabil%type;
ie_centro_custo_w			conta_contabil.ie_centro_custo%type;
cd_centro_custo_w			centro_custo.cd_centro_custo%type;
cd_conta_contabil_w			conta_contabil.cd_conta_contabil%type;
ds_valor_parametro_w			lote_contabil_parametro.ds_valor_parametro%type;

				
Cursor c_comissao is
	select	d.cd_conta_debito,
		d.cd_conta_credito,
		d.cd_historico,
		d.nr_sequencia nr_seq_item,
		'PLS_COMISSAO_BENEF_ITEM' nm_tabela,
		'VL_COMISSAO' nm_atributo,
		75 nr_seq_info_ctb,
		d.vl_comissao vl_contabil,
		g.nr_sequencia nr_seq_vendedor,
		a.dt_referencia,
		c.nr_titulo,
		a.nr_sequencia nr_seq_lote,
		e.nr_sequencia nr_seq_segurado
	from	pls_lote_comissao		a,
		pls_comissao			b,
		pls_comissao_beneficiario	c,
		pls_comissao_benef_item		d,
		pls_segurado			e,
		pls_plano			f,
		pls_vendedor			g
	where	a.nr_sequencia	= b.nr_seq_lote
	and	g.nr_sequencia	= b.nr_seq_canal_venda
	and	b.nr_sequencia	= c.nr_seq_comissao
	and	c.nr_sequencia	= d.nr_seq_comissao_benef
	and	e.nr_sequencia	= c.nr_seq_segurado
	and	d.nr_seq_plano 	= f.nr_sequencia(+)
	and	d.nr_lote_contabil = nr_lote_contabil_p
	union all
	select	c.cd_conta_deb cd_conta_debito,
		c.cd_conta_cred cd_conta_credito,
		c.cd_historico,
		c.nr_sequencia nr_seq_item,
		'PLS_REPASSE_LANC' nm_tabela,
		'VL_LANCAMENTO' nm_atributo,
		75 nr_seq_info_ctb,
		c.vl_lancamento,
		d.nr_sequencia,
		a.dt_referencia,
		c.nr_titulo,
		a.nr_sequencia,
		null nr_seq_segurado
	from	pls_lote_comissao		a,
		pls_comissao			b,
		pls_repasse_lanc		c,
		pls_vendedor			d
	where	a.nr_sequencia	= b.nr_seq_lote
	and	d.nr_sequencia	= b.nr_seq_canal_venda
	and	b.nr_sequencia	= c.nr_seq_comissao
	and	c.nr_lote_contabil 	= nr_lote_contabil_p;
		
Cursor c_debito_credito is
	select	'D'
	from	dual
	union all
	select	'C'
	from	dual;
	
vet_comissao		c_comissao%rowtype;
	
begin

select	dt_referencia,
	cd_estabelecimento,
	nr_lote_contabil,
	cd_tipo_lote_contabil
into 	dt_referencia_w,
	cd_estabelecimento_w,
	nr_lote_contabil_w,
	cd_tipo_lote_contabil_w
from 	lote_contabil
where 	nr_lote_contabil 	= nr_lote_contabil_p;

dt_ref_inicial_w	:= trunc(dt_referencia_w, 'month');
dt_ref_final_w		:= fim_dia(fim_mes(dt_ref_inicial_w));
nr_seq_w_movto_cont_w	:= 0;

select	count(1)
into 	qt_lote_contabil_w
from 	lote_contabil
where 	nr_lote_contabil 	<> nr_lote_contabil_p
and	dt_referencia		= dt_referencia_w
and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
and	cd_tipo_lote_contabil	= cd_tipo_lote_contabil_w
and	rownum			= 1;

if	(qt_lote_contabil_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort( 190536, 'DT_REFERENCIA=' || to_char(dt_referencia_w,'mm/yyyy'));
	/* Shows the following message: There already is a accounting batch of sales comission for the referenc month. Pleasy verify */
end if;

/*select	max(ds_valor_parametro)
into	ds_valor_parametro_w
from	lote_contabil_parametro
where	nr_lote_contabil 	= nr_lote_contabil_p
and	nr_seq_parametro	= 1;

if	(ds_valor_parametro_w = 'A') then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_w_movto_cont_w
	from	movimento_contabil
	where	nr_lote_contabil = nr_lote_contabil_p;
end if;*/

if	(ie_exclusao_p = 'S') then
	wheb_usuario_pck.set_ie_lote_contabil('S');
	
	delete	from movimento_contabil
	where	nr_lote_contabil	= nr_lote_contabil_p;

	update	lote_contabil
	set	vl_credito 		= 0,
		vl_debito  		= 0
	where	nr_lote_contabil	= nr_lote_contabil_p;
	
	update	pls_comissao_benef_item
	set	nr_lote_contabil	= 0
	where	nr_lote_contabil 	= nr_lote_contabil_p;
	
	update	pls_repasse_lanc
	set	nr_lote_contabil	= 0
	where	nr_lote_contabil 	= nr_lote_contabil_p;
	
	wheb_usuario_pck.set_ie_lote_contabil('N');
else
	if	(ie_exclusao_p = 'N') then
		wheb_usuario_pck.set_ie_lote_contabil('S');
		
		ctb_gravar_log_lote(	nr_lote_contabil_p,
					1,
					wheb_mensagem_pck.get_texto(1097173),
					nm_usuario_p);

		select	max(ds_valor_parametro)
		into	ds_valor_parametro_w
		from	lote_contabil_parametro
		where	nr_lote_contabil 	= nr_lote_contabil_p
		and	nr_seq_parametro	= 1;
		
		if	(ds_valor_parametro_w <> 'A') then
			delete	w_movimento_contabil
			where	nr_lote_contabil	= nr_lote_contabil_p;
		end if;
		
		update	pls_comissao_benef_item		a
		set	a.nr_lote_contabil	= nr_lote_contabil_p
		where	exists	(select	1
				from	pls_lote_comissao		b,
					pls_comissao			c,
					pls_comissao_beneficiario	d
				where	d.nr_sequencia	= a.nr_seq_comissao_benef
				and	c.nr_sequencia	= d.nr_seq_comissao
				and	b.nr_sequencia	= c.nr_seq_lote
				and	b.dt_referencia between dt_ref_inicial_w and dt_ref_final_w
				and	c.dt_cancelamento is null);
				
		update	pls_repasse_lanc	a
		set	a.nr_lote_contabil	= nr_lote_contabil_p
		where	exists	(select	1
				from	pls_lote_comissao		b,
					pls_comissao			c
				where	c.nr_sequencia	= a.nr_seq_comissao
				and	b.nr_sequencia	= c.nr_seq_lote
				and	b.dt_referencia between dt_ref_inicial_w and dt_ref_final_w
				and	c.dt_cancelamento is null);
				
		wheb_usuario_pck.set_ie_lote_contabil('N');
		
		nm_agrupador_w	:= nvl(trim(obter_agrupador_contabil(cd_tipo_lote_contabil_w)),'NR_SEQ_REP_VEND');
		
		open c_comissao;
		loop
		fetch c_comissao into	
			vet_comissao;
		exit when c_comissao%notfound;
			begin
			
			open c_debito_credito;
			loop
			fetch c_debito_credito into	
				ie_debito_credito_w;
			exit when c_debito_credito%notfound;
				begin
				
			if	(ie_debito_credito_w = 'D') then
				cd_conta_contabil_w	:= vet_comissao.cd_conta_debito;
			elsif	(ie_debito_credito_w = 'C') then
				cd_conta_contabil_w	:= vet_comissao.cd_conta_credito;
			end if;
				
			if 	(cd_conta_contabil_w is null) then
				insert into w_pls_movimento_sem_conta 
					(nr_sequencia,
					cd_item, ds_item,
					ie_tipo_item,
					dt_atualizacao,
					nm_usuario,
					vl_item,
					dt_referencia,
					nr_lote_contabil,
					ie_proc_mat_item,
					ie_deb_cred,
					cd_tipo_lote_contabil)
				values	(w_pls_movimento_sem_conta_seq.nextval,
					vet_comissao.nr_seq_lote,
					obter_desc_expressao(957518) || to_char(vet_comissao.nr_seq_lote),
					'R',
					sysdate,
					nm_usuario_p,
					vet_comissao.vl_contabil,
					vet_comissao.dt_referencia,
					nr_lote_contabil_w,
					null,
					ie_debito_credito_w,
					cd_tipo_lote_contabil_w);
			else
				if 	(nm_agrupador_w = 'NR_SEQ_REP_MENS') then
					nr_seq_agrupamento_w	:=	vet_comissao.nr_seq_item;
				elsif 	(nm_agrupador_w = 'NR_SEQ_REP_VEND') then
					nr_seq_agrupamento_w	:=	vet_comissao.nr_seq_vendedor;
				else
					nr_seq_agrupamento_w	:=	vet_comissao.nr_seq_lote;
				end if;
				
				select	ie_centro_custo
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;
				
				if 	(ie_centro_custo_w = 'S') then
					pls_obter_centro_custo(	'D',
								null,
								cd_estabelecimento_w,
								'',
								'',
								'', 
								vet_comissao.nr_seq_segurado,
								'',
								cd_centro_custo_w, 
								nr_seq_regra_cc_w);
				end if;
				
				nm_vendedor_w := '';
				
				if	(nvl(vet_comissao.nr_seq_vendedor,0) <> 0) then
					select 	substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc),1,200)
					into 	nm_vendedor_w
					from 	pls_vendedor
					where 	nr_sequencia = vet_comissao.nr_seq_vendedor;
				end if;
								
				if (nvl(vet_comissao.cd_historico,0) > 0) then
					/* If needed to add/remove fields from the historical complement, it is also needed to change the procedure CTB_PLS_CONTA_DESP_REPASSE */
					ds_conteudo_w := substr(	to_char(null) || '#@' ||
									to_char(vet_comissao.nr_titulo) || '#@' ||
									to_char(nm_vendedor_w) || '#@' ||
									to_char(null),1,4000);
								
				
								
					begin
					ds_compl_historico_w := substr(obter_compl_historico(24,vet_comissao.cd_historico,ds_conteudo_w),1,255);
					exception
					when others then
						ds_compl_historico_w := null;
					end;
				end if;
				
				insert into w_movimento_contabil
					(nr_lote_contabil,
					nr_sequencia,
					cd_conta_contabil,
					ie_debito_credito,
					cd_historico,
					dt_movimento,
					vl_movimento,
					cd_estabelecimento,
					cd_centro_custo,
					nr_seq_tab_orig,
					nr_seq_info,
					nm_tabela,
					nm_atributo,
					nr_seq_agrupamento,
					ds_compl_historico)
				values	(nr_lote_contabil_p,
					nr_seq_w_movto_cont_w,
					cd_conta_contabil_w,
					ie_debito_credito_w,
					vet_comissao.cd_historico,
					vet_comissao.dt_referencia,
					vet_comissao.vl_contabil,
					cd_estabelecimento_w,
					cd_centro_custo_w,
					vet_comissao.nr_seq_item,
					vet_comissao.nr_seq_info_ctb,
					vet_comissao.nm_tabela,
					vet_comissao.nm_atributo,
					nr_seq_agrupamento_w,
					ds_compl_historico_w);
			end if;
				
				end;
			end loop;
			close c_debito_credito;
			
			end;
		end loop;
		close c_comissao;
		
		/*agrupa_movimento_contabil(	nr_lote_contabil_p,
						nm_usuario_p); */
	end if;
end if;

/*if   	(ds_retorno_p is null) then
	begin
	update	lote_contabil
	set	ie_situacao 		= 'A',
		dt_geracao_lote		= decode(ie_exclusao_p,'N',sysdate,'S',null)
	where	nr_lote_contabil 	= nr_lote_contabil_p;
	
	if	(ie_exclusao_p = 'S') then
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(298774);
	else
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(298775);
	end if;
	
	commit;
	end;
else
	rollback;
end if;*/

commit;

end ctb_pls_contabilizar_comissao;
/