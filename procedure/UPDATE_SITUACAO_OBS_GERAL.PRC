CREATE OR REPLACE PROCEDURE UPDATE_SITUACAO_OBS_GERAL(nr_sequencia_p NUMBER,
                                                      ie_um_reg_p VARCHAR2,
                                                      nm_usuario_p VARCHAR2) IS

nr_sequencia_w NUMBER(10);
ie_um_reg_w    VARCHAR2(1);
nm_usuario_w   VARCHAR2(15);

BEGIN
  nr_sequencia_w := nr_sequencia_p;
  ie_um_reg_w    := ie_um_reg_p;
  nm_usuario_w   := nm_usuario_p;

  IF (ie_um_reg_w = 'S') THEN
  BEGIN
    UPDATE prescr_proc_obs_gerais
    SET ie_situacao = 'A',
        dt_atualizacao = sysdate
    WHERE nr_sequencia = nr_sequencia_w;
  END;
  ELSE
  BEGIN
    UPDATE prescr_proc_obs_gerais
    SET ie_situacao = 'A',
        dt_atualizacao = sysdate
    WHERE nr_seq_interno = nr_sequencia_w
    AND   nm_usuario = nm_usuario_w
    AND   ie_situacao = 'I';
  END;
  END IF;

  COMMIT;

END UPDATE_SITUACAO_OBS_GERAL;
/