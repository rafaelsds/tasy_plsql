create or replace
procedure pls_consistir_titular_benef
		(	nr_seq_contrato_p	number,
			nm_usuario_p		varchar2) is

ie_titular_pf_w		varchar2(1);
ds_erro_w		varchar2(250);
qt_titular_w		number(10);
qt_contrato_pf_w	number(10);

begin

ie_titular_pf_w	:= nvl(obter_valor_param_usuario(1202, 48, Obter_Perfil_Ativo, nm_usuario_p, 0), 'N');

if	(ie_titular_pf_w = 'S') then
	select	count(1)
	into	qt_contrato_pf_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_p
	and	cd_cgc_estipulante is null;
	
	if	(qt_contrato_pf_w > 0) then
		select	count(1)
		into	qt_titular_w
		from	pls_segurado
		where	nr_seq_contrato	= nr_seq_contrato_p
		and	nr_seq_titular is null
		and	dt_rescisao is null;
		
		if	(qt_titular_w > 1) then
			wheb_mensagem_pck.exibir_mensagem_abort(203130);
		end if;
	end if;
end if;

end pls_consistir_titular_benef;
/