create or replace
procedure liberar_inspecao_recebimento(
				nr_sequencia_p			number,
				cd_estabelecimento_p		number,
				ie_ordem_p			varchar2,
				ie_obriga_temp_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_registro_p			number,
				ie_forma_registro_p			varchar2) is

nr_ordem_compra_w		ordem_compra.nr_ordem_compra%type;
nr_sequencia_w			inspecao_recebimento.nr_sequencia%type;
nr_seq_regitro_w			inspecao_registro.nr_sequencia%type;
nr_item_oci_w			ordem_compra_item.nr_item_oci%type;
qt_existe_w			number(10);
qt_inspecao_w			inspecao_recebimento.qt_inspecao%type;
qt_material_w			inspecao_recebimento_lote.qt_material%type;
cd_material_w			material.cd_material%type;
ds_material_w			material.ds_material%type;
cd_cnpj_w			pessoa_juridica.cd_cgc%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_pessoa_fis_insp_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_cgc_w			pessoa_juridica.cd_cgc%type;
ie_gera_lote_w			regra_nf_lote_fornec.ie_gerar_lote_inspecao%type;
nr_seq_tipo_nao_conf_w		inspecao_recebimento.nr_seq_tipo_nao_conf%type;
ds_justificativa_w			inspecao_recebimento.ds_justificativa%type;
ie_justificativa_w			inspecao_nao_conf.ie_justificativa%type;
ie_aspecto_w			inspecao_regra_material.ie_aspecto%type;
ie_controla_diverg_w		funcao_param_usuario.vl_parametro%type;
ie_consiste_qt_lote_w		funcao_param_usuario.vl_parametro%type;
ie_consiste_qt_maior_w		funcao_param_usuario.vl_parametro%type;
ie_atualiza_desc_lote_nf_w	funcao_param_usuario.vl_parametro%type;
qt_inspecionado_w			inspecao_recebimento.qt_inspecao%type;
qt_material_ordem_w		ordem_compra_item_entrega.qt_prevista_entrega%type;
qt_saldo_pendente_w		ordem_compra_item_entrega.qt_prevista_entrega%type;
ie_forma_registro_w			funcao_param_usuario.vl_parametro%type;
nr_seq_marca_w			ordem_compra_item.nr_seq_marca%type;
ie_consiste_status_marca_w		funcao_param_usuario.vl_parametro%type;
ie_consiste_status_marca_ww	funcao_param_usuario.vl_parametro%type;
ds_marca_w			marca.ds_marca%type;
ds_status_marca_w			valor_dominio.ds_valor_dominio%type;
ie_libera_w			varchar2(1);
cd_unid_medida_inspecao_w		unidade_medida.cd_unidade_medida%type;
cd_unid_medida_estoque_w		unidade_medida.cd_unidade_medida%type;
ie_consiste_marca_insp_w		funcao_param_usuario.vl_parametro%type;
ie_consiste_marca_insp_ww		funcao_param_usuario.vl_parametro%type;
ie_permite_item_filial_w		funcao_param_usuario.vl_parametro%type;
ie_consiste_temp_armaz_w		funcao_param_usuario.vl_parametro%type;
ie_consiste_temp_transp_w		funcao_param_usuario.vl_parametro%type;
ie_atualiza_cod_barra_nf_w      funcao_param_usuario.vl_parametro%type;
ie_temperatura_w			inspecao_recebimento.ie_temperatura%type;
qt_temperatura_max_w		number(5,0);
qt_temp_max_transp_w		number(5,0);
qt_temperatura_min_w		number(5,0);
qt_temp_min_transp_w		number(5,0);
dt_validade_w			inspecao_recebimento_lote.dt_validade%type;
qt_dias_minimo_validade_w		regra_val_minima_lote.qt_dias_validade_lote%type;
qt_dias_validade_w			number(10);
ie_origem_w			inspecao_registro.ie_origem%type;
cd_perfil_w			perfil.cd_perfil%type;
qt_temperatura_w		number(20,4);
nr_seq_regra_contrato_w			inspecao_recebimento.nr_seq_regra_contrato%type;
vl_unitario_item_w				inspecao_recebimento.vl_unitario_material%type;
vl_pagto_w						contrato_regra_nf.vl_pagto%type;
ie_preco_w						contrato_regra_nf.ie_preco%type;
qt_registros_w							number(10);
vl_total_bruto_w			number(20,4);
vl_total_contrato_w			contrato.vl_total_contrato%type;
ie_consiste_contrato_w			funcao_param_usuario.vl_parametro%type;
ie_avaliacao_registro_w			funcao_param_usuario.vl_parametro%type;
ds_mensagem_w					varchar2(255);
qt_existe_avaliacao_w			number(10);
qt_existe_contrato_w			number(10);




cursor	c01 is
select	nr_sequencia,
	cd_cgc,
	cd_pessoa_fisica,
	nr_ordem_compra,
	nr_item_oci,
	qt_inspecao,
	nvl(obter_dados_item_ordem(nr_ordem_compra, nr_item_oci, 'UMC'), obter_dados_material(cd_material, 'UMP')),
	obter_dados_material(cd_material, 'UME'),
	nr_seq_regra_contrato,
	nvl(vl_unitario_material,0)
from	inspecao_recebimento
where	nr_sequencia	= nr_sequencia_p
and	ie_ordem_p	= 'N'
and	ie_forma_registro_p in('N','X')
union all
select	nr_sequencia,
	cd_cgc,
	cd_pessoa_fisica,
	nr_ordem_compra,
	nr_item_oci,
	qt_inspecao,
	nvl(obter_dados_item_ordem(nr_ordem_compra, nr_item_oci, 'UMC'), obter_dados_material(cd_material, 'UMP')),
	obter_dados_material(cd_material, 'UME'),
	nr_seq_regra_contrato,
	nvl(vl_unitario_material,0)
from	inspecao_recebimento
where	nr_seq_registro	= nr_seq_registro_p
and	ie_ordem_p	= 'N'
and	ie_forma_registro_p = 'S'
union all
select	nr_sequencia,
	cd_cgc,
	cd_pessoa_fisica,
	nr_ordem_compra,
	nr_item_oci,
	qt_inspecao,
	nvl(obter_dados_item_ordem(nr_ordem_compra, nr_item_oci, 'UMC'), obter_dados_material(cd_material, 'UMP')),
	obter_dados_material(cd_material, 'UME'),
	nr_seq_regra_contrato,
	nvl(vl_unitario_material,0)
from	inspecao_recebimento
where	nr_ordem_compra = (
	select	nr_ordem_compra
	from	inspecao_recebimento
	where	nr_sequencia = nr_sequencia_p)
and	ie_ordem_p = 'S'
and	ie_forma_registro_p = 'N';

cursor c02 is
select	b.cd_material,
	b.nr_seq_marca,
	substr(obter_desc_marca(b.nr_seq_marca),1,255),
	substr(obter_desc_status_aval_marca(c.nr_seq_status_aval),1,255)
from	inspecao_recebimento a,
	ordem_compra_item b,
	material_marca c
where	a.nr_ordem_compra		= b.nr_ordem_compra
and	a.nr_item_oci		= b.nr_item_oci
and	c.cd_material		= b.cd_material
and	c.nr_sequencia		= b.nr_seq_marca
and	a.nr_sequencia		= nr_sequencia_w
and	c.nr_seq_status_aval	> 0
and	obter_tipo_status_aval_marca(c.nr_seq_status_aval) <> 'A';

cursor	c03 is
select	a.cd_material,
	b.nr_seq_marca,
	substr(obter_desc_marca(b.nr_seq_marca),1,255),
	substr(obter_desc_status_aval_marca(c.nr_seq_status_aval),1,255)
from	inspecao_recebimento a,
	inspecao_recebimento_lote b,
	material_marca c
where	a.nr_sequencia = b.nr_seq_inspecao
and	a.cd_material = c.cd_material
and	b.nr_seq_marca = c.nr_sequencia
and	a.nr_sequencia = nr_sequencia_w
and	c.nr_seq_status_aval	> 0
and	obter_tipo_status_aval_marca(c.nr_seq_status_aval) <> 'A';

cursor	c04 is
select	a.cd_material,
	b.dt_validade
from	inspecao_recebimento a,
	inspecao_recebimento_lote b
where	a.nr_sequencia = b.nr_seq_inspecao
and	a.nr_sequencia = nr_sequencia_w
and	ie_origem_w <> 'NF';

begin

cd_perfil_w	:= obter_perfil_ativo;

if	(ie_forma_registro_p in ('S','X')) then
	select	cd_cnpj,
		cd_pessoa_fisica
	into	cd_cnpj_w,
		cd_pessoa_fisica_w
	from	inspecao_registro
	where	nr_sequencia = nr_seq_registro_p;

	if	(cd_cnpj_w is null) and 
		(cd_pessoa_fisica_w is null) then
		--(-20011,'Deve ser informado uma pessoa fisica ou pessoa juridica.');
		wheb_mensagem_pck.exibir_mensagem_abort(127415);
	end if;
	if	(cd_cnpj_w is not null) and 
		(cd_pessoa_fisica_w is not null) then
		--(-20011,'Deve ser informado somente pessoa fisica ou pessoa juridica.');	
		wheb_mensagem_pck.exibir_mensagem_abort(115097);		
	end if;
end if;


select	nvl((max(obter_valor_param_usuario(270, 15, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 27, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 29, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 34, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 35, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 44, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'I'),
	nvl((max(obter_valor_param_usuario(270, 60, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 71, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 82, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 104, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 63, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
	nvl((max(obter_valor_param_usuario(270, 66, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N'),
    nvl((max(obter_valor_param_usuario(270, 113, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p))),'N')
into	ie_controla_diverg_w,
	ie_consiste_qt_lote_w,
	ie_consiste_qt_maior_w,
	ie_consiste_temp_transp_w,
	ie_consiste_temp_armaz_w,
	ie_forma_registro_w,
	ie_consiste_status_marca_w,
	ie_consiste_marca_insp_w,
	ie_permite_item_filial_w,
	ie_atualiza_desc_lote_nf_w,
	ie_consiste_contrato_w,
	ie_avaliacao_registro_w,
    ie_atualiza_cod_barra_nf_w
from	dual;

select	decode(ie_consiste_status_marca_w,'S','C','M'),
	decode(ie_consiste_marca_insp_w,'S','C','M')
into	ie_consiste_status_marca_ww,
	ie_consiste_marca_insp_ww
from	dual;

select	nvl(max(ie_origem),'X')
into	ie_origem_w
from	inspecao_registro
where	nr_sequencia = nr_seq_registro_p;

if	(ie_controla_diverg_w = 'S') and
	(nr_seq_registro_p > 0) then 

	select	count(*)
	into	qt_existe_w
	from	inspecao_tipo_divergencia
	where	ie_situacao	= 'A'
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_situacao = 'A';

	if	(qt_existe_w = 0) then
		/*(-20011,	'O sistema esta configurado para gerar as divergencias conforme a regra Tipos de divergencia da inspecao.' || chr(13) || chr(10) ||
		'Para gerar essas divergencias, e obrigatorio o cadastro destas regras.' || chr(13) || chr(10) ||
		'Este cadastro se encontra em Cadastros gerais - Suprimentos - Cadastros Compras - Tipos de divergencia da inspecao.');*/
		wheb_mensagem_pck.exibir_mensagem_abort(179834);
	end if;
	
	if	(ie_origem_w = 'NF') then
		gerar_divergencia_inspecao_nf(nr_seq_registro_p, nm_usuario_p);
	else
		gerar_divergencia_inspecao(nr_seq_registro_p, cd_estabelecimento_p, nm_usuario_p);
	end if;
	
	qt_existe_w := 0;

	if	(ie_forma_registro_p = 'S') then	
		select	count(*)
		into	qt_existe_w
		from	inspecao_divergencia
		where	nr_seq_registro	= nr_seq_registro_p
		and	ie_geracao	= 'S'
		and	ie_motivo_aceite	= 'F';
	elsif	(ie_forma_registro_p = 'X') then
		select	count(*)
		into	qt_existe_w
		from	inspecao_divergencia
		where	nr_seq_registro	= nr_seq_registro_p
		and	ie_geracao	= 'S'
		and	ie_motivo_aceite	= 'F'
		and	nr_seq_inspecao	= nr_sequencia_p;
	end if;

	if	(qt_existe_w > 0) then
		begin
			avisa_diverg_insp_recebimento(nr_seq_registro_p, cd_estabelecimento_p, nm_usuario_p);
		exception when others then
			null;
		end;

		/*(-20011,	'O sistema gerou algumas divergencias para essa inspecao. ' || chr(13) || chr(10) ||
			'Agora e necessario revisa-las e justifica-las antes de  efetuar a liberacao.' || chr(13) || chr(10) ||
			'Essa revisao se faz na pasta Divergencias.');*/
		wheb_mensagem_pck.exibir_mensagem_abort(179836);
	end if;	
end if;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_cgc_w,
	cd_pessoa_fis_insp_w,
	nr_ordem_compra_w,
	nr_item_oci_w,
	qt_inspecionado_w,
	cd_unid_medida_inspecao_w,
	cd_unid_medida_estoque_w,
	nr_seq_regra_contrato_w,
	vl_unitario_item_w;
exit when c01%notfound;
	begin
	ie_libera_w := 'S';
	
	delete	recebimento_insp_consist
	where	nr_seq_inspecao = nr_sequencia_w;
	
	if	(nr_seq_regra_contrato_w > 0) then

		select	nvl(vl_pagto,0),
			nvl(ie_preco,'V')
		into	vl_pagto_w,
			ie_preco_w
		from	contrato_regra_nf
		where	nr_sequencia = nr_seq_regra_contrato_w;
		
		if	(ie_preco_w = 'F') and
			(vl_pagto_w > 0) and
			(vl_unitario_item_w <> vl_pagto_w) then				
			wheb_mensagem_pck.exibir_mensagem_abort(712758,	'NR_SEQUENCIA_P='||nr_sequencia_w);	
			/*Inspecao #@NR_SEQUENCIA_P#@: o valor do item esta diferente do valor estipulado no contrato.*/
		else		
			select 	count(*)
			into	qt_registros_w
			from 	contrato_regra_nf
			where	nr_sequencia = nr_seq_regra_contrato_w
			and 	ie_preco = 'F'
			and 	nvl(vl_pagto,0) = 0
			and 	(((dt_inicio_vigencia is null) and (dt_fim_vigencia is null)) or (trunc(sysdate,'dd') between trunc(dt_inicio_vigencia,'dd') and trunc(dt_fim_vigencia,'dd')));
			
			if	(qt_registros_w > 0) then
				
				select	sum(vl_total_bruto)
				into	vl_total_bruto_w
				from	(
					select 	sum(b.qt_inspecao * b.vl_unitario_material) vl_total_bruto
					from   	inspecao_registro a,
						inspecao_recebimento b
					where  	a.nr_sequencia = b.nr_seq_registro
					and	a.dt_liberacao is not null
					and 	a.nr_sequencia <> nr_seq_registro_p
					and    	b.nr_seq_regra_contrato = nr_seq_regra_contrato_w
					union all
					select 	sum(b.qt_inspecao * b.vl_unitario_material) vl_total_bruto
					from   	inspecao_registro a,
						inspecao_recebimento b
					where  	a.nr_sequencia = b.nr_seq_registro
					and	a.nr_sequencia = nr_seq_registro_p
					and    	b.nr_seq_regra_contrato = nr_seq_regra_contrato_w);	

				select 	nvl(a.vl_total_contrato,0)
				into	vl_total_contrato_w
				from	contrato a,
					contrato_regra_nf b
				where 	a.nr_sequencia = b.nr_seq_contrato
				and	b.nr_sequencia = nr_seq_regra_contrato_w;
			
				if	(vl_total_bruto_w > vl_total_contrato_w) then
					wheb_mensagem_pck.exibir_mensagem_abort(713136,	'NR_SEQUENCIA_P='||nr_sequencia_w);
					/*Inspecao #@NR_SEQUENCIA_P#@: o valor desta inspecao vai superar o valor total do contrato. Verifique o valor total e as inspecoes vinculadas a ele.
					A soma das inspecoes nao pode superar o valor total.*/
				end if;
			end if;	
		end if;
	end if;
	
	if	--(nr_seq_registro_p > 0) and
		(nr_ordem_compra_w > 0) and
		(nr_item_oci_w > 0) and
		(ie_consiste_status_marca_w <> 'N') then
		
		open C02;
		loop
		fetch C02 into	
			cd_material_w,
			nr_seq_marca_w,
			ds_marca_w,
			ds_status_marca_w;
		exit when C02%notfound;
			begin
			
			if	(ie_consiste_status_marca_ww = 'C') then
				ie_libera_w := 'N';
			end if;
			
			grav_recebimento_insp_consist(
				nr_sequencia_w,
				WHEB_MENSAGEM_PCK.get_texto(303073,'CD_MATERIAL_W='|| CD_MATERIAL_W ||';DS_MARCA_W='|| DS_MARCA_W ||';DS_STATUS_MARCA_W='|| DS_STATUS_MARCA_W), /*A marca #@DS_MARCA_W#@ do material #@CD_MATERIAL_W#@ esta: #@DS_STATUS_MARCA_W#@.*/
				ie_consiste_status_marca_ww,
				'',
				nm_usuario_p);
			end;
		end loop;
		close C02;		
	end if;

	if	(ie_consiste_marca_insp_w <> 'N') then
		begin

		open c03;
		loop
		fetch c03 into
			cd_material_w,
			nr_seq_marca_w,
			ds_marca_w,
			ds_status_marca_w;
		exit when c03%notfound;
			begin

			if	(ie_consiste_marca_insp_ww = 'C') then
				ie_libera_w := 'N';
			end if;
			
			grav_recebimento_insp_consist(
				nr_sequencia_w,
				WHEB_MENSAGEM_PCK.get_texto(303073,'CD_MATERIAL_W='|| CD_MATERIAL_W ||';DS_MARCA_W='|| DS_MARCA_W ||';DS_STATUS_MARCA_W='|| DS_STATUS_MARCA_W), /*A marca #@DS_MARCA_W#@ do material #@CD_MATERIAL_W#@ esta: #@DS_STATUS_MARCA_W#@.*/
				ie_consiste_marca_insp_ww,
				'',
				nm_usuario_p);

			end;
		end loop;
		close c03;

		end;
	end if;	

	if	(nvl(qt_inspecionado_w,0) = 0) then
		select	sum(qt_material)
		into	qt_inspecionado_w
		from	inspecao_recebimento_lote
		where	nr_seq_inspecao = nr_sequencia_w;
	end if;

	select	cd_material,
		substr(obter_desc_material(cd_material),1,255) ds_material,
		nr_seq_tipo_nao_conf,
		ds_justificativa,
		ie_temperatura
	into	cd_material_w,
		ds_material_w,
		nr_seq_tipo_nao_conf_w,
		ds_justificativa_w,
		ie_temperatura_w
	from	inspecao_recebimento
	where	nr_sequencia = nr_sequencia_w;
	
	begin
		qt_temperatura_w:= to_number(regexp_replace(replace(ie_temperatura_w, '.', ','), '[^0-9|,|-]'));
	exception
	when others then
		qt_temperatura_w:=null;
	end;

	if	(ie_consiste_qt_maior_w = 'S') and
		(nr_ordem_compra_w is not null) and
		(nr_item_oci_w is not null) then
		
		select	sum(qt_prevista_entrega)
		into	qt_material_ordem_w
		from	ordem_compra_item_entrega
		where	nr_ordem_compra = nr_ordem_compra_w
		and	nr_item_oci = nr_item_oci_w
		and	dt_cancelamento is null;
		
		if	(qt_inspecionado_w > qt_material_ordem_w) then
			/*(-20011, 'A quantidade inspecionada do material ' || cd_material_w || ' superou a quantidade dele na ordem de compra.' ||
				'Verifique parametro [29].');*/					
			wheb_mensagem_pck.exibir_mensagem_abort(179844,'CD_MATERIAL_W='||cd_material_w);
		end if;
		
		select	sum(qt_prevista_entrega) - sum(nvl(qt_real_entrega,0))
		into	qt_saldo_pendente_w
		from	ordem_compra_item_entrega
		where	nr_ordem_compra = nr_ordem_compra_w
		and	nr_item_oci = nr_item_oci_w
		and	dt_cancelamento is null;
		
		if	(qt_inspecionado_w > qt_saldo_pendente_w) then
			/*A quantidade inspecionada do material #@CD_MATERIAL_W#@ superou o saldo restante de entrega.
			Verifique parametro [29].*/
			wheb_mensagem_pck.exibir_mensagem_abort(332532,'CD_MATERIAL_W='||cd_material_w);
		end if;
	end if;
	
	/*Consiste se a temperatura esta informada*/
	select	count(*)
	into	qt_existe_w
	from	inspecao_recebimento a
	where	a.nr_sequencia = nr_sequencia_w
	and	a.ie_temperatura is null
	and exists(
		select	1
		from	inspecao_regra_material x
		where	x.cd_material		= a.cd_material
		and	x.cd_estabelecimento	= cd_estabelecimento_p
		and	x.ie_temperatura		= 'S');

	if	(nvl(ie_obriga_temp_p,'N') = 'S') and
		(qt_existe_w > 0) then
		/*(-20011, 'Deve ser informado a temperatura do material ' || cd_material_w || chr(13) || chr(10) || 
			'Verifique parametro [2].');*/
					
		wheb_mensagem_pck.exibir_mensagem_abort(179850,'CD_MATERIAL_W='||cd_material_w);
	end if;

	/*Busca as temperaturas para realizar as consistencia*/
	begin
	select	x.qt_temperatura_max,
		x.qt_temp_max_transp,
		x.qt_temperatura_min,
		x.qt_temp_min_transp
	into	qt_temperatura_max_w,
		qt_temp_max_transp_w,
		qt_temperatura_min_w,
		qt_temp_min_transp_w
	from	inspecao_regra_material x
	where	x.cd_material		= cd_material_w
	and	x.cd_estabelecimento	= cd_estabelecimento_p
	and	x.ie_temperatura	= 'S';
	exception
		when others then
			qt_temperatura_max_w  	:= null;
			qt_temperatura_min_w	:= null;
			qt_temp_max_transp_w	:= null;
			qt_temp_min_transp_w	:= null;			
	end;

	/*Consiste se a temperatura esta maior que a Temp. Armazenamento*/
	if	(ie_consiste_temp_armaz_w <> 'N') and
		(qt_temperatura_max_w is not null) and
		(qt_temperatura_w is not null) and
		(qt_temperatura_w > qt_temperatura_max_w) then
		begin

		if	(ie_consiste_temp_armaz_w = 'S') then
			comunic_consist_temp_inspecao(nr_sequencia_w,cd_estabelecimento_p,nm_usuario_p);
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(303074, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta maior que a Temperatura Armazenamento*/
							'C',
							null,
							nm_usuario_p);
			ie_libera_w := 'N';

		else
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(303074, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta maior que a Temperatura Armazenamento*/
							'M',
							null,
							nm_usuario_p);
		end if;

		end;
	end if;

	/*Consiste se a temperatura esta maior que a Temp. Transporte*/
	if	(ie_consiste_temp_transp_w <> 'N') and
		(qt_temp_max_transp_w is not null) and
		(qt_temperatura_w is not null) and
		(qt_temperatura_w > qt_temp_max_transp_w) then
		begin

		if	(ie_consiste_temp_transp_w = 'S') then
			comunic_consist_temp_inspecao(nr_sequencia_w,cd_estabelecimento_p,nm_usuario_p);
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(303075, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta maior que a Temperatura Transporte*/
							'C',
							null,
							nm_usuario_p);
			ie_libera_w := 'N';
		else
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(303075, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta maior que a Temperatura Transporte*/
							'M',
							null,
							nm_usuario_p);
		end if;

		end;
	end if;

/*Consiste se a temperatura esta menor que a Temp. Armazenamento*/
	if	(ie_consiste_temp_armaz_w <> 'N') and
		(qt_temperatura_max_w is not null) and
		(qt_temperatura_w is not null) and
		(qt_temperatura_w < qt_temperatura_min_w) then
		begin

		if	(ie_consiste_temp_armaz_w = 'S') then
			comunic_consist_temp_inspecao(nr_sequencia_w,cd_estabelecimento_p,nm_usuario_p);
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(1187478, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta menor que a Temperatura Armazenamento*/
							'C',
							null,
							nm_usuario_p);
			ie_libera_w := 'N';

		else
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(1187478, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta menor que a Temperatura Armazenamento*/
							'M',
							null,
							nm_usuario_p);
		end if;

		end;
	end if;

	/*Consiste se a temperatura esta menor que a Temp. Transporte*/
	if	(ie_consiste_temp_transp_w <> 'N') and
		(qt_temp_max_transp_w is not null) and
		(qt_temperatura_w is not null) and
		(qt_temperatura_w < qt_temp_min_transp_w) then
		begin

		if	(ie_consiste_temp_transp_w = 'S') then
			comunic_consist_temp_inspecao(nr_sequencia_w,cd_estabelecimento_p,nm_usuario_p);
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(341248, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta menor que a Temperatura Transporte*/
							'C',
							null,
							nm_usuario_p);
			ie_libera_w := 'N';
		else
			grav_recebimento_insp_consist(	nr_sequencia_w,
							Wheb_mensagem_pck.get_Texto(341248, 'CD_MATERIAL_W='|| CD_MATERIAL_W), /*Temperatura do material #@CD_MATERIAL_W#@ esta menor que a Temperatura Transporte*/
							'M',
							null,
							nm_usuario_p);
		end if;

		end;
	end if;	
	
	
	if	(ie_controla_diverg_w = 'S') then
		/*Consiste se existe divergencia sem revisar*/
		select	count(*)
		into	qt_existe_w
		from	inspecao_divergencia
		where	nr_seq_inspecao		= nr_sequencia_w
		and	nvl(ie_motivo_aceite,'F')	= 'F';
		
		if	(qt_existe_w > 0) then
			/*(-20011, 'Existem divergencias do material ' || cd_material_w || ' que falta revisar.' || chr(13) || chr(10) || 
						'Verifique a pasta Divergencias');*/
			wheb_mensagem_pck.exibir_mensagem_abort(179855,'CD_MATERIAL_W='||cd_material_w);
		end if;
	end if;
	
	/*Consiste se a obriga informar aspecto*/	
	select	nvl(max(ie_aspecto), 'N')
	into	ie_aspecto_w
	from	inspecao_regra_material
	where	cd_material		= cd_material_w
	and	cd_estabelecimento		= cd_estabelecimento_p;
	
	select	count(*)
	into	qt_existe_w
	from	inspecao_recebimento a,
		inspecao_rec_aspecto_item b
	where	a.nr_sequencia	= b.nr_seq_inspecao
	and	a.nr_sequencia	= nr_sequencia_w;
	
	if	(ie_aspecto_w = 'S') and
		(qt_existe_w = 0) then
		/*(-20011, 'Deve ser informado pelo menos um aspecto para o material ' || cd_material_w || '.');*/
		wheb_mensagem_pck.exibir_mensagem_abort(179856,'CD_MATERIAL_W='||cd_material_w);
	end if;
	
	/*Consistencia para obrigar a informar justificativa quando a Nao conformidade exige justificativa*/
	if	(nr_seq_tipo_nao_conf_w is not null) then
	
		select	nvl(ie_justificativa,'N')
		into	ie_justificativa_w
		from	inspecao_nao_conf
		where	nr_sequencia	= nr_seq_tipo_nao_conf_w;
		
		if	(ie_justificativa_w = 'S') and
			(ds_justificativa_w is null) then
			/*(-20011, 'A Nao conformidade informada no material ' || cd_material_w || ' exige justificativa. Favor informa-la.');*/
			wheb_mensagem_pck.exibir_mensagem_abort(179857,'CD_MATERIAL_W='||cd_material_w);
		
		end if;	
	end if;

	/*Consistencia dos varios lotes para o mesmo item*/
	select	count(*)
	into	qt_existe_w
	from	inspecao_recebimento_lote
	where	nr_seq_inspecao = nr_sequencia_w;
	
	if	(qt_existe_w > 0) then

		select	obter_quantidade_convertida(cd_material_w, qt_inspecao, cd_unid_medida_inspecao_w, 'UME')
		into	qt_inspecao_w
		from	inspecao_recebimento
		where	nr_sequencia = nr_sequencia_w;
		
		qt_material_w := 0;
		
		select	sum(qt_material)
		into	qt_material_w
		from	inspecao_recebimento_lote
		where	nr_seq_inspecao = nr_sequencia_w;
		
		if	(ie_consiste_qt_lote_w = 'S') and
			(qt_material_w > qt_inspecao_w) then
			/*(-20011,'A quantidade do lote esta maior que a quantidade inspecionada. ' || chr(13) || chr(10) ||
							'Caso a Un. Med. Compra for diferente da Un. Med. Estoque, a quantidade do lote deve ser convertida para Un. Med. Estoque. '  || chr(13) || chr(10) ||
							'Verifique essa conversao no Cadastro do Material.' || cd_material_w);*/
			wheb_mensagem_pck.exibir_mensagem_abort(179861,'CD_MATERIAL_W='||cd_material_w);
			
		elsif	(ie_consiste_qt_lote_w = 'S') and
			(qt_material_w < qt_inspecao_w) then
			/*(-20011,'A quantidade do lote esta menor que a quantidade inspecionada. ' || chr(13) || chr(10) ||
							'Caso a Un. Med. Compra for diferente da Un. Med. Estoque, a quantidade do lote deve ser convertida para Un. Med. Estoque. '  || chr(13) || chr(10) ||
							'Verifique essa conversao no Cadastro do Material.' || cd_material_w);*/
			wheb_mensagem_pck.exibir_mensagem_abort(179863,'CD_MATERIAL_W='||cd_material_w);
		end if;
	else
		select	obter_regra_lote_inspecao(cd_estabelecimento_p,cd_material_w)
		into	ie_gera_lote_w
		from	dual;
		
		
		if	(ie_gera_lote_w = 'O') and
			(ie_forma_registro_w = 'I') then
			/*(-20011,	'E obrigatorio a informacao do lote fornecedor para este material.' || chr(13) || chr(10) || 
							cd_material_w || ' - ' || ds_material_w || chr(13));*/
			wheb_mensagem_pck.exibir_mensagem_abort(179869,'CD_MATERIAL_W='||cd_material_w||';'||'DS_MATERIAL_W='||ds_material_w);
			
		elsif	(ie_gera_lote_w = 'O') and
			(ie_forma_registro_w = 'A') then
			
			select	count(*)
			into	qt_existe_w
			from	inspecao_rec_contagem
			where	nr_seq_registro = nr_seq_registro_p
			and	cd_material = cd_material_w;
			
			if	(qt_existe_w = 0) then				
				wheb_mensagem_pck.exibir_mensagem_abort(179869,'CD_MATERIAL_W='||cd_material_w||';'||'DS_MATERIAL_W='||ds_material_w); /*(-20011,	'E obrigatorio a informacao do lote fornecedor para este material.' || chr(13) || chr(10) || cd_material_w || ' - ' || ds_material_w || chr(13));*/
			end if;
		end if;
	end if;
	
	if	(ie_forma_registro_p = 'S') or (ie_forma_registro_p = 'X') then
		
		if	(cd_cnpj_w is not null) and
			(cd_cgc_w is null) then
			/*(-20011, 'Deve ser informado um fornecedor na inspecao ' || nr_sequencia_w || ', conforme informado na pasta Registro.');*/
			wheb_mensagem_pck.exibir_mensagem_abort(179873,'NR_SEQUENCIA_W='||nr_sequencia_w);
		end if;
		
		if	(ie_permite_item_filial_w = 'S') then
			if	(cd_cnpj_w is not null) and
				(obter_se_pj_matriz_filial(cd_cnpj_w, cd_cgc_w) = 'N') then
				/*(-20011, 'O fornecedor da inspecao ' || nr_sequencia_w || ' e diferente do fornecedor informado na pasta Registro.');*/
				wheb_mensagem_pck.exibir_mensagem_abort(179875,'NR_SEQUENCIA_W='||nr_sequencia_w);
			end if;
		else
			if	(cd_cnpj_w is not null) and
				(cd_cnpj_w <> nvl(cd_cgc_w,'X')) then
				/*(-20011, 'O fornecedor da inspecao ' || nr_sequencia_w || ' e diferente do fornecedor informado na pasta Registro.');*/
				wheb_mensagem_pck.exibir_mensagem_abort(179875,'NR_SEQUENCIA_W='||nr_sequencia_w);
			end if;
		end if;

		if	(cd_pessoa_fisica_w is not null) and
			(cd_pessoa_fis_insp_w is null) then
			/*(-20011, 'Deve ser informado uma pessoa fisica na inspecao ' || nr_sequencia_w || ', conforme informado na pasta Registro.');*/
			wheb_mensagem_pck.exibir_mensagem_abort(179876,'NR_SEQUENCIA_W='||nr_sequencia_w);
		end if;
		
		if	(cd_pessoa_fisica_w is not null) and
			(cd_pessoa_fisica_w <> nvl(cd_pessoa_fis_insp_w,'X')) then
			/*(-20011, 'A pessoa fisica da inspecao ' || nr_sequencia_w || ' e diferente da pessoa fisica informada na pasta Registro.');*/
			wheb_mensagem_pck.exibir_mensagem_abort(179877,'NR_SEQUENCIA_W='||nr_sequencia_w);
		end if;
	end if;
	
	if (ie_atualiza_desc_lote_nf_w = 'S') then	
		atualizar_lote_nota_fiscal(nr_sequencia_w,nm_usuario_p,ie_atualiza_cod_barra_nf_w);
	end if;	

	
	if(ie_consiste_contrato_w = 'SC')then
		
		if(ie_avaliacao_registro_w  = 'S') then 
			select	count(*)
				into	qt_existe_avaliacao_w
				from 	avf_resultado
				where 	nr_seq_registro = nr_seq_registro_p
				and 	dt_aprovacao is not null;
		else
			select	count(*)
				into	qt_existe_avaliacao_w
				from 	avf_resultado
				where 	cd_cnpj = cd_cnpj_w 
				and		dt_aprovacao is not null 
				and 	nvl(ie_avaliado, 'F') = 'F';
		end if;
		
		select	count(*)
			into	qt_existe_contrato_w
			from	inspecao_recebimento a
			where	a.nr_sequencia = nr_sequencia_w
			and	    nr_seq_contrato is not null;
				
	
		if	(qt_existe_contrato_w > 0) and (qt_existe_avaliacao_w = 0) then
			ie_libera_w := 'N';
				ds_mensagem_w := substr(wheb_mensagem_pck.get_texto(222571),1,255);
				wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);
		end if; 
	
	end if;

	

	if	(ie_libera_w = 'S') then
		update	inspecao_recebimento
		set	dt_liberacao = sysdate,
			dt_atualizacao = sysdate,
			nm_usuario   = nm_usuario_p,
			nm_usuario_lib = nm_usuario_p
		where	nr_sequencia = nr_sequencia_w;
	end if;
	end;
end loop;
close c01;

select	count(*)
into	qt_existe_w
from	inspecao_recebimento
where	nr_seq_registro	= nr_seq_registro_p
and	dt_liberacao is null;

if	(qt_existe_w = 0) then

	update	inspecao_registro
	set	dt_liberacao	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		nm_usuario_lib	= nm_usuario_p
	where	nr_sequencia	= nr_seq_registro_p;
end if;

if	(nr_seq_registro_p > 0) then
	avisar_chegada_mat_inspecao(nr_seq_registro_p, cd_estabelecimento_p, nm_usuario_p);
end if;

open C04;
loop
fetch C04 into	
	cd_material_w,
	dt_validade_w;
exit when C04%notfound;
	begin
	if	(dt_validade_w is not null) then
	
		select	count(*)
		into	qt_existe_w
		from	regra_val_minima_lote
		where	ie_situacao = 'A'
		and	cd_estabelecimento = cd_estabelecimento_p;
		
		if	(qt_existe_w > 0) then
			
			select	obter_regra_valid_min_opcao(cd_material_w, cd_estabelecimento_p, nm_usuario_p, 'R')
			into	qt_dias_minimo_validade_w
			from	dual;
			
			select	obter_dias_entre_datas(trunc(sysdate,'dd'), trunc(dt_validade_w,'dd'))
			into	qt_dias_validade_w
			from	dual;
			
			if	(qt_dias_minimo_validade_w >= 0) and
				(qt_dias_validade_w < qt_dias_minimo_validade_w) then
				wheb_mensagem_pck.exibir_mensagem_abort(306557,'CD_MATERIAL_W='||cd_material_w);
				/*A data de validade do material #@CD_MATERIAL_W#@ nao pode ser maior que a quantidade de dias informado na Regra de validade minima lote que tem nos Cadastros gerais.*/							
			end if;		
		end if;	
	end if;		
	end;
end loop;
close C04;

select	count(*)
into	qt_existe_w
from	regra_envio_email_compra
where	ie_tipo_mensagem = 103
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_existe_w > 0) and
	(nr_seq_registro_p > 0) then

	gerar_email_ins_nao_conformes(nr_seq_registro_p, nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	regra_envio_email_compra
where	ie_tipo_mensagem = 104
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_existe_w > 0) and
	(nr_seq_registro_p > 0) then

	gerar_email_ins_devolucao(nr_seq_registro_p, nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	regra_envio_email_compra
where	ie_tipo_mensagem = 109
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_existe_w > 0) and
	(nr_seq_registro_p > 0) then

	enviar_email_diverg_inspecao(nr_seq_registro_p, nm_usuario_p);
end if;


commit;


end liberar_inspecao_recebimento;
/