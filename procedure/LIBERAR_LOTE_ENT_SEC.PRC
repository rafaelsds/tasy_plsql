
create or replace procedure liberar_lote_ent_sec(nr_seq_lote_sec_p	number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number) is


cd_pessoa_fisica_resp_w	varchar2(10);
cd_procedencia_w	number(5);
nr_atendimento_w	number(10);
nr_prescricao_w		number(14);
nr_seq_ficha_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_exame_w		number(10);
nr_seq_proc_interno_w	number(10);
ie_origem_proced_w	number(10);
cd_procedimento_w	number(15);
nr_seq_prescr_proc_w	number(6);
dt_entrada_w		date;
cd_convenio_w		atend_categoria_convenio.cd_convenio%type;
cd_categoria_w		atend_categoria_convenio.cd_categoria%type;
cd_plano_convenio_w	atend_categoria_convenio.cd_plano_convenio%type;
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
nr_seq_forma_laudo_w	lote_ent_inst_geracao.nr_seq_forma_laudo%type;
ie_tipo_convenio_w	lote_ent_inst_geracao.ie_tipo_convenio%type;
ie_tipo_atendimento_w	lote_ent_inst_geracao.ie_tipo_atendimento%type;													   
dt_coleta_w				date;
ie_status_coleta_w	number(2);
ie_amostra_inadequada_w	varchar(2);
contador_lote_w		number(5);
contador_ficha_w	number(5);
ds_erro_w			varchar2(255);
ie_medico_w			varchar2(2);
cd_material_exame_w	varchar2(20);
cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w	varchar2(10);
nr_seq_prescr_procc_w	number(6);
nr_seq_pacote_w		number(10);

nr_atend_ficha_w 	number(10);
nr_prescr_ficha_w	number(10);
ie_ficha_urg_w		varchar2(10);
cd_tipo_acomod_w	atend_categoria_convenio.cd_tipo_acomodacao%type;

cd_proced_pacote_w		number(15);
ie_origem_proced_pac_w	number(10);

/*Variaveis da procedure OBTER_SETOR_EXAME_LAB*/
cd_setor_atend_ww		varchar2(5);
cd_setor_coleta_w		varchar2(5);
cd_setor_entrega_w		varchar2(5);
qt_dia_entrega_w		number(10);
ie_emite_mapa_w			varchar2(1);
ds_hora_fixa_w			varchar2(10);
ie_data_resultado_w		varchar2(1);
qt_min_entrega_w		number(10);
ie_recoleta_w			varchar2(1);
ie_dia_semana_final_w		number(10);
qt_min_atraso_w		    	number(10);
ie_forma_atual_dt_result_w	exame_lab_regra_setor.ie_atul_data_result%type;

--variavel que veridica se ha fichas sem medicacao
ds_ficha_sem_medicacao_w	varchar2(255);
vl_parametro_p			varchar2(1);

--variavel que verifica ficha sem exame
ds_ficha_sem_exame_w		varchar2(255);


Cursor C01 is
	select	nr_sequencia,
		cd_pessoa_fisica,
		cd_medico_resp,
		nvl(to_date(to_char(nvl(DT_COLETA_FICHA_F,sysdate), 'dd/mm/yyyy')   ||    to_char(HR_COLETA_F, 'hh24:mi'),'dd/mm/yyyy hh24:mi'),sysdate),
		ie_susp_am_inad
	from	lote_ent_sec_ficha
	where	nr_seq_lote_sec = nr_seq_lote_sec_p;

Cursor C02 is
	select	nr_seq_exame,
		cd_material_exame
	from	lote_ent_sec_ficha_exam
	where	nr_seq_ficha = nr_seq_ficha_w
	and	ie_tipo_coleta is null;

Cursor C03 is
	select	a.nr_sequencia
	from	lote_ent_sec_ficha a
	where	a.ie_corticoide_f = 'S'
	and	a.nr_seq_lote_sec = nr_seq_lote_sec_p
	and 	not exists (select	1
			   from		paciente_medic_uso b
			   where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
			   and		trunc(nvl(dt_inicio,sysdate)) between trunc(sysdate-15) and trunc(sysdate));

Cursor C04 is
	select	a.nr_sequencia
	from	lote_ent_sec_ficha a,
		lote_ent_secretaria c
	where	a.nr_seq_lote_sec = c.nr_sequencia
	and	c.ie_tipo_ficha = 'C'
	and	c.nr_sequencia = nr_seq_lote_sec_p
	and	not exists (	select	1
				from	lote_ent_sec_ficha_exam b
				where	b.nr_seq_ficha = a.nr_sequencia);

begin

Obter_Param_Usuario(10060,68,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,vl_parametro_p);

select	max(ie_status_coleta)
into	ie_status_coleta_w
from	lab_parametro
where	cd_estabelecimento = cd_estabelecimento_p;

select	decode(count(*),0,'N','S')
into	ie_medico_w
from	medico a,
		usuario b
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and		b.nm_usuario		= nm_usuario_p
and		a.ie_situacao		= 'A';

SELECT	max(nr_seq_pacote)
INTO 	nr_seq_pacote_w
FROM	lote_ent_secretaria b
WHERE	b.nr_sequencia = nr_seq_lote_sec_p;

if	(nr_seq_lote_sec_p is not null) then

	if (vl_parametro_p = 'S') then
	--trata parametro 68
		for r_C03_w in C03 loop
			begin
			ds_ficha_sem_medicacao_w := ds_ficha_sem_medicacao_w||', '||r_C03_w.nr_sequencia;
			end;
		end loop;

		if (ds_ficha_sem_medicacao_w is not null or ds_ficha_sem_medicacao_w <> '') then
			ds_ficha_sem_medicacao_w := substr(ds_ficha_sem_medicacao_w,3,255);
			wheb_mensagem_pck.exibir_mensagem_abort(285082, 'DS_FICHAS='||ds_ficha_sem_medicacao_w);
		end if;
	end if;

	for r_C04_w in C04 loop
		begin
		ds_ficha_sem_exame_w := ds_ficha_sem_exame_w||', '||r_C04_w.nr_sequencia;
		end;
	end loop;

	if (ds_ficha_sem_exame_w is not null or ds_ficha_sem_exame_w <> '') then
		ds_ficha_sem_exame_w := substr(ds_ficha_sem_exame_w,3,255);
		wheb_mensagem_pck.exibir_mensagem_abort(285132, 'DS_FICHAS='||ds_ficha_sem_exame_w);
	end if;

	open C01;
	loop
	fetch C01 into
		nr_seq_ficha_w,
		cd_pessoa_fisica_w,
		cd_pessoa_fisica_resp_w,
		dt_coleta_w,
		ie_amostra_inadequada_w;
	exit when C01%notfound;
		begin

		dt_entrada_w	:= sysdate;

		--Verifica se possui ocorrencias para o lote / ficha
		SELECT	COUNT(*)
		INTO 	contador_lote_w
		FROM	lote_ent_ocorrencia a,
				lote_ent_secretaria b
		WHERE	b.nr_sequencia = a.nr_seq_lote_sec
		AND		b.nr_sequencia = nr_seq_lote_sec_p;

		SELECT 	COUNT(*)
		INTO	contador_ficha_w
		FROM	lote_ent_ocorrencia a,
				lote_ent_sec_ficha b
		WHERE	a.nr_seq_ficha = b.nr_sequencia
		AND		a.nr_seq_ficha = nr_seq_ficha_w;

		if ((contador_lote_w > 0) or (contador_ficha_w > 0)) then

			if (contador_lote_w > 0) then

				update	lote_ent_ocorrencia
				set		dt_baixa = sysdate,
						nm_usuario_baixa = nm_usuario_p
				where	nr_seq_lote_sec = nr_seq_lote_sec_p;

			end if;

			if (contador_ficha_w > 0) then

				update	lote_ent_ocorrencia
				set		dt_baixa = sysdate,
						nm_usuario_baixa = nm_usuario_p
				where	nr_seq_ficha = nr_seq_ficha_w;

			end if;

		end if;

		select	nvl(max(nr_atendimento),0),
				nvl(max(nr_prescricao),0),
				nvl(max(ie_ficha_urg),'N')
		into	nr_atend_ficha_w,
				nr_prescr_ficha_w,
				ie_ficha_urg_w
		from	lote_ent_sec_ficha
		where	nr_sequencia = nr_seq_ficha_w;

		--
		select	max(c.cd_convenio),
				max(c.cd_categoria),
				max(c.cd_plano_convenio),
				max(c.cd_setor_atendimento),
				max(cd_procedencia),
				max(c.cd_medico_resp),
				max(c.nr_seq_forma_laudo),
				max(c.cd_unidade_basica),
				max(c.cd_unidade_compl),
				max(c.cd_tipo_acomodacao),
				max(c.ie_tipo_convenio),
                		max(c.ie_tipo_atendimento)						  
		into	cd_convenio_w,
				cd_categoria_w,
				cd_plano_convenio_w,
				cd_setor_atendimento_w,
				cd_procedencia_w,
				cd_pessoa_fisica_resp_w,
				nr_seq_forma_laudo_w,
				cd_unidade_basica_w,
				cd_unidade_compl_w,
				cd_tipo_acomod_w,
				ie_tipo_convenio_w,
                		ie_tipo_atendimento_w
		from	lote_ent_secretaria a,
				lote_ent_inst_geracao c
		where	a.nr_seq_instituicao = c.nr_seq_instituicao
		and ((a.nr_seq_pacote = c.nr_seq_pacote and a.cd_pacote_ext = c.cd_pacote_ext) OR (a.cd_pacote_ext IS NULL AND a.nr_seq_pacote IS NULL))
																
		and		a.nr_sequencia = nr_seq_lote_sec_p;

		if ((nr_atend_ficha_w = 0) and (nr_prescr_ficha_w = 0)) then

			--Insere o atendimento
			select	atendimento_paciente_seq.nextVal
			into	nr_atendimento_w
			from	dual;

			insert into atendimento_paciente (
				nr_atendimento,
				ie_permite_visita,
				dt_entrada,
				ie_tipo_atendimento,
				cd_procedencia,
				cd_medico_resp,
				cd_pessoa_fisica,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_ficha_lote,
				ie_tipo_convenio
			) values (
				nr_atendimento_w,
				'S',
				dt_entrada_w,
				nvl(ie_tipo_atendimento_w, 7),
				cd_procedencia_w,
				cd_pessoa_fisica_resp_w,
				cd_pessoa_fisica_w,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				nr_seq_ficha_w,
				ie_tipo_convenio_w
			);

				insert into atend_categoria_convenio(
					nr_seq_interno,
					nr_atendimento,
					cd_convenio,
					cd_categoria,
					cd_plano_convenio,
					cd_tipo_acomodacao,
					dt_validade_carteira,
					dt_inicio_vigencia,
					ie_tipo_guia,
					nm_usuario,
					dt_atualizacao,
					nr_doc_convenio
				) values	(
					atend_categoria_convenio_seq.NextVal,
					nr_atendimento_w,
					cd_convenio_w,
					cd_categoria_w,
					cd_plano_convenio_w,
					cd_tipo_acomod_w,
					dt_entrada_w+365,
					dt_entrada_w,
					'L',
					nm_usuario_p,
					sysdate,
					nr_atendimento_w);

			--insere a unidade
			insert into atend_paciente_unidade (
					nr_atendimento,
					nr_sequencia,
					cd_setor_atendimento,
					cd_unidade_basica,
					cd_unidade_compl,
					dt_entrada_unidade,
					dt_saida_interno,
					nr_seq_interno,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					cd_tipo_acomodacao
			) values (
					nr_atendimento_w,
					1,
					cd_setor_atendimento_w,
					cd_unidade_basica_w,
					cd_unidade_compl_w,
					sysdate,
					sysdate,
					atend_paciente_unidade_seq.NextVal,
					sysdate,
					sysdate,
					'Tasy',
					'Tasy',
					2
			);

			--Insere a prescricao
			select	prescr_medica_seq.nextVal
			into	nr_prescricao_w
			from 	dual;

			insert into prescr_medica (
				nr_prescricao,
				cd_pessoa_fisica,
				dt_prescricao,
				dt_atualizacao,
				nm_usuario,
				nr_atendimento,
				nr_seq_ficha_lote,
				nr_seq_lote_entrada,
				cd_estabelecimento,
				cd_medico,
				cd_setor_entrega,
				nr_seq_forma_laudo
			) values (
				nr_prescricao_w,
				cd_pessoa_fisica_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nr_atendimento_w,
				nr_seq_ficha_w,
				nr_seq_lote_sec_p,
				cd_estabelecimento_p,
				cd_pessoa_fisica_resp_w,
				cd_setor_atendimento_w,
				nr_seq_forma_laudo_w
			);


			update	lote_ent_sec_ficha
			set		nr_prescricao = nr_prescricao_w, nr_atendimento = nr_atendimento_w
			where	nr_sequencia = nr_seq_ficha_w;
			-- Insere os procedimentos da prescricao

			open C02;
			loop
			fetch C02 into
				nr_seq_exame_w,
				cd_material_exame_w;
			exit when C02%notfound;
				begin
				select	max(nr_seq_proc_interno),
						max(ie_origem_proced),
						max(cd_procedimento)
				into	nr_seq_proc_interno_w,
						ie_origem_proced_w,
						cd_procedimento_w
				from	exame_laboratorio
				where	nr_seq_exame = nr_seq_exame_w;

				select	nvl(max(nr_sequencia), 0) + 1
				into	nr_seq_prescr_proc_w
				from	prescr_procedimento
				where	nr_prescricao	= nr_prescricao_w;

				/*SELECT 	MAX(b.cd_material_exame)
				into	cd_material_exame_w
				FROM	material_exame_lab b,
						exame_lab_material a
				WHERE 	a.nr_seq_material	= b.nr_sequencia
				AND 	a.nr_seq_exame		= nr_seq_exame_w
				AND 	a.ie_situacao		= 'A'
				ORDER BY a.ie_prioridade;
				*/
				if (cd_procedimento_w is not null) 	then
					insert into prescr_procedimento (
						nr_sequencia,
						cd_procedimento,
						qt_procedimento,
						nr_prescricao,
						dt_atualizacao,
						nm_usuario,
						ie_origem_inf,
						nr_seq_proc_interno,
						ie_origem_proced,
						nr_seq_interno,
						nr_seq_exame,
						dt_coleta,
						dt_prev_execucao,
						ie_status_atend,
						cd_setor_atendimento,
						cd_motivo_baixa,
						cd_material_exame,
						ie_suspenso,
						ie_urgencia
					) values (
						nr_seq_prescr_proc_w,
						cd_procedimento_w,
						1,
						nr_prescricao_w,
						sysdate,
						nm_usuario_p,
						'1',
						nr_seq_proc_interno_w,
						ie_origem_proced_w,
						prescr_procedimento_seq.nextVal,
						nr_seq_exame_w,
						nvl(dt_coleta_w,sysdate),
						Obter_data_prev_exec(sysdate, sysdate, cd_setor_atendimento_w, nr_prescricao_w, 'A'),
						decode(ie_amostra_inadequada_w,'S','23',ie_status_coleta_w),
						cd_setor_atendimento_w,
						0,
						cd_material_exame_w,
						'N',
						ie_ficha_urg_w
					);
				end if;

				obter_setor_exame_lab(	nr_prescricao_w, nr_seq_exame_w, cd_setor_atendimento_w, cd_material_exame_w,
					nvl(dt_coleta_w,sysdate), 'S', cd_setor_atend_ww, cd_setor_coleta_w, cd_setor_entrega_w,
					qt_dia_entrega_w, ie_emite_mapa_w, ds_hora_fixa_w, ie_data_resultado_w, qt_min_entrega_w,
					ie_recoleta_w, ie_ficha_urg_w, ie_dia_semana_final_w, ie_forma_atual_dt_result_w, qt_min_atraso_w);

				end;
			end loop;
			close C02;

			SELECT 	max(a.cd_proced_pacote),
					max(a.ie_origem_proced)
			into	cd_proced_pacote_w,
					ie_origem_proced_pac_w
			FROM	pacote a
			WHERE	a.nr_seq_pacote = nr_seq_pacote_w;

			select	nvl(max(nr_sequencia), 0) + 1
			into	nr_seq_prescr_procc_w
			from	prescr_procedimento
			where	nr_prescricao	= nr_prescricao_w;

			if (cd_proced_pacote_w is not null) 	then
				insert into prescr_procedimento (
					nr_sequencia,
					cd_procedimento,
					qt_procedimento,
					nr_prescricao,
					dt_atualizacao,
					nm_usuario,
					ie_origem_inf,
					ie_origem_proced,
					nr_seq_interno,
					cd_setor_atendimento,
					ie_suspenso,
					ie_status_atend,
					dt_prev_execucao
				) values (
					nr_seq_prescr_procc_w,
					cd_proced_pacote_w,
					1,
					nr_prescricao_w,
					sysdate,
					nm_usuario_p,
					'1',
					ie_origem_proced_pac_w,
					prescr_procedimento_seq.nextVal,
					cd_setor_atendimento_w,
					'N',
					decode(ie_amostra_inadequada_w,'S','23',ie_status_coleta_w),
					Obter_data_prev_exec(sysdate, sysdate, cd_setor_atendimento_w, nr_prescricao_w, 'A')
				);
			end if;

		else

			nr_prescricao_w := nr_prescr_ficha_w;
			nr_atendimento_w := nr_atend_ficha_w;

		end if;

		Liberar_Prescricao(nr_prescricao_w, nr_atendimento_w, ie_medico_w, Obter_Perfil_Ativo, nm_usuario_p, 'S',ds_erro_w);

		end;
	end loop;
	close C01;

	update	lote_ent_secretaria
	set		dt_liberacao = sysdate
	where	nr_sequencia = nr_seq_lote_sec_p;

end if;

commit;

end liberar_lote_ent_sec;
 /
