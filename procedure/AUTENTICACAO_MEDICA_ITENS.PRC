create or replace
procedure autenticacao_medica_itens(	nr_sequencia_p		number,
				cd_pessoa_fisica_p		varchar2,
				ie_opcao_p		number,
				nm_usuario_p		varchar2) is 
/*
1 - Materiais
2 - Procedimentos
*/
ds_biometria_w			varchar2(4000);
ds_biometria_ww			varchar2(4000);
cd_medico_autenticacao_w	varchar2(15);
dt_autenticacao_w		date;

begin

if	(cd_pessoa_fisica_p is not null) then

	select	max(ds_biometria)
	into	ds_biometria_w
	from	pessoa_fisica_biometria
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	if	(ie_opcao_p = 1) then
		
		select	max(ds_biometria),
			max(cd_medico_autenticacao),
			max(dt_autenticacao)
		into	ds_biometria_ww,
			cd_medico_autenticacao_w,
			dt_autenticacao_w
		from	material_atend_paciente
		where	nr_sequencia = nr_sequencia_p;
		
		if	(dt_autenticacao_w is not null) then
			
			insert into matpaci_autenticacao(
				nr_sequencia,
				nr_seq_matpaci,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_medico_autenticacao,
				dt_autenticacao,
				ds_biometria)
			values	(matpaci_autenticacao_seq.nextval,
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_medico_autenticacao_w,
				dt_autenticacao_w,
				ds_biometria_ww);
				
		end if;
		
		update	material_atend_paciente
		set	cd_medico_autenticacao = cd_pessoa_fisica_p,
			dt_autenticacao = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			ds_biometria = ds_biometria_w
		where	nr_sequencia = nr_sequencia_p;
		
	elsif	(ie_opcao_p = 2) then

		select	max(ds_biometria),
			max(cd_medico_autenticacao),
			max(dt_autenticacao)
		into	ds_biometria_ww,
			cd_medico_autenticacao_w,
			dt_autenticacao_w
		from	procedimento_paciente
		where	nr_sequencia = nr_sequencia_p;
		
		if	(dt_autenticacao_w is not null) then
			
			insert into propaci_autenticacao(
				nr_sequencia,
				nr_seq_propaci,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_medico_autenticacao,
				dt_autenticacao,
				ds_biometria)
			values	(propaci_autenticacao_seq.nextval,
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_medico_autenticacao_w,
				dt_autenticacao_w,
				ds_biometria_ww);
				
		end if;
		
		update	procedimento_paciente
		set	cd_medico_autenticacao = cd_pessoa_fisica_p,
			dt_autenticacao = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			ds_biometria = ds_biometria_w
		where	nr_sequencia = nr_sequencia_p;
		
	end if;

end if;

commit;

end autenticacao_medica_itens;
/