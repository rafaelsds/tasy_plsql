create or replace
procedure gerar_erro_pac_prot(		nr_seq_paciente_p		number,
					nr_seq_material_p		number,
					nr_regra_p	number,
					ds_compl_erro_p	varchar2,
					cd_perfil_p	number,
					nm_usuario_p	varchar2,
					nr_seq_erro_p 	out	number,
					ds_mensagem_p 	varchar2 default null) is

nr_seq_erro_w		number(10,0);
ie_regra_w		varchar2(15);
ds_regra_w		varchar2(255);
ds_msg_perfil_w		varchar2(4000);
ds_msg_wheb_w		varchar2(4000);
ds_inf_w		varchar2(2000);
ie_prioridade_w		number(3,0);
ds_cor_w					varchar2(15);
ie_agrupador_w				number(2,0);
ie_forma_consistencia_w		varchar2(3);
ie_justificar_consistencia_w	varchar2(3);


cursor c01 is
	select	ie_forma_consistencia,
			DS_MENSAGEM_CLIENTE,
			ie_justificar_consistencia
	from	regra_consiste_onc_par
	where	nr_seq_regra = nr_regra_p
	and	nvl(cd_perfil,cd_perfil_p) = cd_perfil_p
	order by nvl(cd_perfil,0);

begin

if	(nr_seq_paciente_p is not null) and
	(nm_usuario_p is not null) then

	/*select	ds_regra,
		ds_mensagem_usuario,
		ds_informacao_adic
	into	ds_regra_w,
		ds_msg_wheb_w,
		ds_inf_w
	from	regra_consiste_onc
	where	nr_sequencia = nr_regra_p;*/
	
	select	substr(obter_desc_expressao(cd_exp_regra,ds_regra),1,255),
			obter_desc_expressao(cd_exp_mensagem,ds_mensagem_usuario),
			obter_desc_expressao(cd_exp_informacao,ds_informacao_adic)
	into	ds_regra_w,
			ds_msg_wheb_w,
			ds_inf_w
	from	regra_consiste_onc
	where	nr_sequencia = nr_regra_p;	

	open c01;
	loop
	fetch c01 into 
			ie_forma_consistencia_w,
			ds_msg_perfil_w,
			ie_justificar_consistencia_w;
	exit when c01%notfound;
	end loop;

	select	paciente_atendimento_erro_seq.nextval
	into	nr_seq_erro_w
	from	dual;
	
	if	(nvl(ie_forma_consistencia_w,'S') = 'S') and
		(nvl(ie_justificar_consistencia_w,'N') = 'S') then
	
		ie_forma_consistencia_w := 'J';
	
	end if;	
	
	insert into paciente_atendimento_erro	(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_paciente,
						nr_seq_material,
						ds_inconsistencia,
						ds_erro,
						ie_libera,
						nr_seq_regra)
		values			(	nr_seq_erro_w,
						sysdate,
						nm_usuario_p,
						nr_seq_paciente_p,
						nr_seq_material_p,
						nvl(ds_mensagem_p, substr(ds_regra_w||'. '||ds_compl_erro_p,1,255)),
						nvl(ds_msg_perfil_w,ds_msg_wheb_w),
						nvl(ie_forma_consistencia_w,'S'),
						nr_regra_p);

end if;
nr_seq_erro_p	:= nr_seq_erro_w;

commit;

end gerar_erro_pac_prot;
/