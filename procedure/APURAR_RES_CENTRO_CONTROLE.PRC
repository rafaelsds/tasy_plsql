create or replace 
procedure apurar_res_centro_controle(cd_estabelecimento_p	number,
				cd_tabela_custo_p    		number,
				ie_atualizar_indic_p		varchar2,
				nm_usuario_p      		varchar2,
				nr_seq_tabela_p			number) is




begin

custos_pck.apurar_res_centro_controle(	cd_estabelecimento_p,
					cd_tabela_custo_p,
					ie_atualizar_indic_p,
					nm_usuario_p,
					nr_seq_tabela_p	
					);	

end apurar_res_centro_controle;
/