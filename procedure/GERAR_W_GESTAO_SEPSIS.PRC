create or replace
procedure Gerar_w_Gestao_Sepsis(dt_inicial_p		date,
				dt_final_p		date,
				nm_usuario_p		varchar2,
				cd_material_cefazolina_p varchar2) is 
					
				
nr_atendimento_w			number(10);
dt_entrada_w				date;
dt_alta_w				date;
dt_cirurgia_w				date;
cd_pessoa_fisica_w			varchar2(10);
cd_setor_Atendimento_w			number(10);
qt_tev_w				number(10);	
ie_inserir_w				boolean	:= False;
ie_passou_w				boolean	:= False;	
qt_primeira_dose_w			number(10); -- Verificar  se o paciente teve uma prescri��o realizada (prescr_mat_hor/prescr_material) com algum material que tenha no cadastro o campo ie_dias_util_medic <> 'N'
dt_primeira_dose_w			date; --  (Dt_horario da prescr_mat_hor)
dt_primeira_cultura_w			date;
dt_ponto_zero_w				date;
dt_obito_w				date;
dt_ultima_dose_w			date;
nr_seq_presc_hor_w			number(10);			
nr_prescricao_w				number(10); --  (nr_prescricao da prescr_material);
cd_material_w				number(6); -- (cd_material da prescr_material);
nr_seq_coleta_cultura_w			number(10);
nr_horas_w				number(10);
cd_setor_prescr_w			number(5);
qt_dias_w				number(10);
nr_seq_exame_cult_w		number(10);
ds_lista_exame_cult_w	varchar2(255);
cd_cefazolina_w			number(10);
qt_cefazolina_w			number(10);
dt_prescricao_w			date;
cd_prescritor_w			varchar2(10);
cd_procedimento_w		number(15);
qt_reg_w				number(10);

cd_material_ant_w			number(10); 
cd_classif_setor_w			varchar2(2);
ds_conteudo_w				varchar2(4000);
nm_arquivo_w				varchar2(255);
ds_local_w				varchar2(255) := null;
ds_erro_w				varchar2(255);

nr_sequencia_w				number(10);
ie_controle_reg_w			varchar2(10);

nr_atend_origem_pa_w		number(10);
dt_entrada_ant_w			date;
qt_dias_entrada_w			number(10);
				
Cursor C01 is
	select	a.cd_pessoa_fisica,
		a.nr_atendimento,
		b.cd_setor_atendimento,
		a.dt_entrada,
		a.dt_alta,
		a.nr_atend_origem_pa
	from	atend_paciente_unidade b,
		atendimento_paciente a
	where	a.nr_Atendimento	= b.nr_atendimento
	and	b.nr_seq_interno	= obter_atepacu_paciente(a.nr_atendimento, 'A')
	and	a.IE_TIPO_ATENDIMENTO in ('1') 												-- Lucimeri solicitoou pela OS 1296306
	and	a.dt_entrada between inicio_dia(dt_inicial_p) and fim_dia(dt_final_p)
	order by a.cd_pessoa_fisica, a.nr_atendimento;
	
Cursor C02 is
	Select  b.nr_sequencia,
		a.nr_prescricao,
		b.dt_horario,
		a.cd_material,
		cd_setor_atendimento
	from	prescr_material a,
		prescr_mat_hor b,
		material c,
		prescr_medica d
	where	d.nr_atendimento in (nr_atendimento_w,nr_atend_origem_pa_w)
	and	a.nr_prescricao = d.nr_prescricao
	and	a.nr_prescricao = b.nr_prescricao
	and	a.nr_sequencia = b.nr_seq_material
	and	b.cd_material = c.cd_material
	and	nvl(c.ie_dias_util_medic,'N') <> 'N'
	and	c.cd_medicamento is not null
	and	c.IE_CONTROLE_MEDICO in (2,4)        -- Lucimeri definiu que seria estas duas op��es OS 1296306            -- is not null
	order by b.dt_horario;
	--order by a.cd_material, b.dt_horario desc;
	
Cursor C03 is
	select	nr_seq_exame
	from	exames_gestao_sepsis
	where	IE_OPCAO = 'CU'; -- Apenas exames Hemoculturas

begin

delete  from w_gestao_sepsis
where	nm_usuario = nm_usuario_p;

open C03;
loop
fetch C03 into	
	nr_seq_exame_cult_w;
exit when C03%notfound;
	begin
	
	if	(ds_lista_exame_cult_w is null) then
		ds_lista_exame_cult_w := nr_seq_exame_cult_w;
	else
		ds_lista_exame_cult_w := ','||ds_lista_exame_cult_w;
	end if;
	
	end;
end loop;
close C03;

open C01;
loop
fetch C01 into
	cd_pessoa_fisica_w,
	nr_atendimento_w,
	cd_setor_Atendimento_w,
	dt_entrada_w,
	dt_alta_w,
	nr_atend_origem_pa_w;
exit when C01%notfound;
	begin

	select	count(*)
	into	qt_reg_w
	from	w_gestao_sepsis
	where	nr_atendimento = nr_atendimento_w
	and		nm_usuario = nm_usuario_p;
	
	
	select	max(dt_alta)
	into	dt_entrada_ant_w
	from	atendimento_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and		nr_atendimento < nr_atendimento_w;
	
	select	Obter_Dias_Entre_Datas_hora(dt_entrada_ant_w, dt_entrada_w)	
	into	qt_dias_entrada_w
	from	dual;
	
	if	(qt_reg_w = 0) and (qt_dias_entrada_w >= 30) then
	
		ie_passou_w	:= false;
		ie_inserir_w	:= false;

		cd_material_ant_w  	:= 999999999;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_presc_hor_w,
			nr_prescricao_w,
			dt_primeira_dose_w,
			cd_material_w,
			cd_setor_prescr_w;
		exit when C02%notfound;
			begin
			ie_controle_reg_w := '';
			
			if (cd_material_ant_w <> cd_material_w) then  -- N�o trazer duas vezes  o mesmo medicamento da mesma prescri��o
			   cd_material_ant_w := cd_material_w;
			   ie_inserir_w	:= false;
			   ie_passou_w	:= false;
			end if;
			
			Select 	min(a.nr_sequencia)
			into	nr_seq_coleta_cultura_w
			from    result_laboratorio a,
				prescr_procedimento c,
				prescr_medica b
			WHERE  	c.nr_prescricao     = a.nr_prescricao(+)
			AND  	c.nr_sequencia      = a.nr_seq_prescricao(+)
			AND  	c.nr_prescricao     = b.nr_prescricao
			AND  	b.nr_atendimento    in (NR_ATENDIMENTO_W,nr_atend_origem_pa_w)
			and		obter_se_contido(c.cd_procedimento,ds_lista_exame_cult_w) = 'S'
			and		nvl(c.dt_coleta,a.dt_coleta) between (DT_PRIMEIRA_DOSE_W - 3) and (DT_PRIMEIRA_DOSE_W + 2);
			
			if  (nr_seq_coleta_cultura_w is not null) and
				(ie_inserir_w <> true) then	--Item 1A	
				ie_inserir_w	:= true;
				
				Select 	max(nvl(b.dt_coleta,a.dt_coleta)),
						max(b.cd_procedimento)
				into	dt_primeira_cultura_w,
						cd_procedimento_w
				from	result_laboratorio a,
						prescr_procedimento b
				where	b.nr_prescricao     = a.nr_prescricao(+)
				AND  	b.nr_sequencia      = a.nr_seq_prescricao(+)
				and		a.nr_sequencia = nr_seq_coleta_cultura_w;
			
			else  --Item 1B 
				
				ie_controle_reg_w := 'SEMCOLETA'; --  Motivo: N�o possui coleta de cultura
				
				select	count(*)
				into	qt_cefazolina_w
				from	exames_gestao_sepsis
				where	cd_material = cd_material_w
				and		ie_opcao = 'CE'; -- Cefazolina
			
				if --(nvl(cd_material_cefazolina_p,0) <> cd_material_w) and -- Se o material n�o for cefazolina. C�digo de cadastro repassado pelo cliente de acordo com seu cadastro. 
					(qt_cefazolina_w = 0) and
					(ie_inserir_w <> true) then 
					
					if	(obter_classif_setor(cd_setor_prescr_w) <> 2 ) then  -- Se a classifica��o do setor for diferente de centro cirurgico.
						
						ie_controle_reg_w := '';
						
						select 	max(dt_horario)
						into	dt_ultima_dose_w
						from	prescr_material a,
								prescr_mat_hor b,
								material c,
								prescr_medica d
						where	a.nr_prescricao = d.nr_prescricao
						and		a.nr_prescricao = b.nr_prescricao
						and		a.nr_sequencia = b.nr_seq_material
						and		b.cd_material = c.cd_material
						and		a.cd_material = cd_material_w
						and		d.nr_atendimento in (nr_atendimento_w,nr_atend_origem_pa_w);

						
						select  Obter_Hora_Entre_datas(dt_primeira_dose_w, dt_ultima_dose_w)
						into	nr_horas_w
						from 	dual;

						select 	obter_data_obito(nr_atendimento_w) 
						into	dt_obito_w
						from 	dual;
						
						if	(dt_ultima_dose_w is not null) and
							(dt_primeira_dose_w <> dt_ultima_dose_w) then
							if 	(nvl(nr_horas_w,0) >= 96) then --Calcula se o paci�nte utilizou antibi�tico por mais de 96horas. 
								ie_inserir_w	:= true;
							else
								ie_controle_reg_w := 'MENOS96H'; -- Motivo: Utilizou menos de 96horas de ATB
							end if;
						end if;
						
						if	(dt_obito_w is not null) then
							if	((dt_obito_w >= dt_primeira_dose_w) and (nr_horas_w <= 96)) then -- Caso ele tenha entrado em obito nas primeiras 96 horas 
								ie_inserir_w	:= true;
							else
								ie_controle_reg_w := 'OBITO96H'; -- Motivo: �bito antes de 96 horas de uso do ATB
							end if;
						end if;
					else
						ie_controle_reg_w := 'SETORCC'; -- Motivo: Classifica��o do setor � Centro cirurgico
					end if;
				else
					ie_controle_reg_w := 'CEFAZOLINA'; -- Motivo: Possui Cefazolia
				end if;
				
			end if;
			
			dt_ponto_zero_w := dt_primeira_dose_w;		
			
			If (nvl(dt_primeira_cultura_w,sysdate) < dt_primeira_dose_w) then
				dt_ponto_zero_w := dt_primeira_cultura_w;
			end if;		
			
			
			--if	(ie_inserir_w) and
			--	(not ie_passou_w) then
				ie_passou_w := true;
				
				select	max(cd_classif_setor) 
				into 	cd_classif_setor_w
				from 	setor_atendimento
				where 	cd_setor_atendimento = cd_setor_Atendimento_w;
				
				select	w_gestao_assistencial_seq.nextval
				into	nr_sequencia_w
				from	dual;
				
				select	max(dt_prescricao),
						max(cd_prescritor)
				into	dt_prescricao_w,
						cd_prescritor_w
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_w;
				
				select	count(*)
				into	qt_reg_w
				from	w_gestao_sepsis
				where	nr_atendimento = nr_atendimento_w
				and		nm_usuario = nm_usuario_p;
				
				if	(qt_reg_w = 0) then
					
					insert into w_gestao_sepsis (	nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_atendimento,
									cd_setor_Atendimento,
									nr_prescricao,
									cd_material,
									dt_primeira_dose, 
									cd_classif_setor,
									dt_coleta,
									dt_prescricao,
									cd_prescritor,
									cd_procedimento,
									ie_controle)
							values	(nr_sequencia_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_atendimento_w,
									cd_setor_Atendimento_w,
									nr_prescricao_w,
									cd_material_w,
									dt_primeira_dose_w,
									cd_classif_setor_w,
									dt_primeira_cultura_w,
									dt_prescricao_w,
									cd_prescritor_w,
									cd_procedimento_w,
									ie_controle_reg_w);
					commit;				
				
					GQA_gerar_sepsis_sofa(nr_atendimento_w,nr_sequencia_w,dt_primeira_dose_w,nm_usuario_p);		
					
				end if;
				
				--GQA_gerar_sepsis_sofa(nr_atendimento_w,nr_sequencia_w,dt_primeira_dose_w,nm_usuario_p);
				
			--end if;
			
		end;
		end loop;
		close C02;
		
	end if;
	
	end;
end loop;
close C01;

commit;

end Gerar_w_Gestao_Sepsis;
/