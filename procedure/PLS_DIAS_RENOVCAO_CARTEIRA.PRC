create or replace
procedure pls_dias_renovcao_carteira
			(	nr_seq_contrato_p	number,
				cd_estabelecimento_p	number,
				ie_commit_p		varchar2,
				nm_usuario_p		varchar2) is

qt_dias_valid_cart_w		number(10);
				
begin

select	max(qt_dias_valid_cart)
into	qt_dias_valid_cart_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_p;

if	(qt_dias_valid_cart_w > 0) then
	insert into pls_carteira_renovacao
		(	nr_sequencia, cd_estabelecimento, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec , nm_usuario_nrec,
			qt_meses_renovacao , nr_seq_contrato 	)
	values	(	pls_carteira_renovacao_seq.nextval , cd_estabelecimento_p ,  sysdate ,
			nm_usuario_p , sysdate , nm_usuario_p,
			 qt_dias_valid_cart_w, nr_seq_contrato_p	);			 
end if;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_dias_renovcao_carteira;
/
