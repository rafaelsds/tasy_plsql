create or replace
procedure gravar_zpl_etiqueta( 	nr_seq_lab_etiqueta_p	number,
				zpl_p			varchar2,
				nm_usuario_p		varchar2) is 

begin

if	(nr_seq_lab_etiqueta_p is not null) then

	delete 
	from	lab_etiqueta_conting_zpl
	where	nr_seq_etiqueta_contingencia = nr_seq_lab_etiqueta_p;
	
	
	insert	into lab_etiqueta_conting_zpl
		(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_etiqueta_contingencia,
		ds_zpl
		)
	values
		(
		lab_etiqueta_conting_zpl_seq.NextVal,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_lab_etiqueta_p,
		zpl_p
		);
	


	update	lab_etiqueta_contingencia
	set	dt_atualizacao 	= sysdate,
		dt_envio 	= sysdate,
		nm_usuario 	= nm_usuario_p
	where	nr_sequencia =  nr_seq_lab_etiqueta_p;

	
	commit;

end if;

end gravar_zpl_etiqueta;
/