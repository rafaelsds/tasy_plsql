create or replace
procedure	des_encaminha_os_gestor_conta(nr_sequencia_p		number,
										  nm_usuario_p			varchar2) is

nm_usuario_gestor_w		varchar2(15);
qt_existe_w				number(10);
ds_historico_os_w		varchar2(4000);
ds_titulo_w				varchar2(255);
ds_comunic_w			varchar2(4000);
ds_email_w				varchar2(255);
ie_erro_mail_w			varchar2(1);
nr_seq_rtf_srtring_w	number(10);
nr_seq_cliente_w		number(10);
nr_seq_envio_w			number(10);

begin

select	nr_seq_cliente
into	nr_seq_cliente_w
from	man_ordem_servico
where	nr_sequencia = nr_sequencia_p;

begin
select	nvl(substr(obter_usuario_pf(cd_pessoa_fisica),1,15),'ytsantos')
into	nm_usuario_gestor_w
from	com_cliente_conta
where	nr_seq_cliente = nr_seq_cliente_w
and	sysdate between nvl(dt_inicial,sysdate) and nvl(dt_final,sysdate)
and	rownum	< 2;
exception
when others then
	nm_usuario_gestor_w := 'ytsantos';
end;

select	count(*)
into	qt_existe_w
from	man_ordem_servico_exec
where	nr_seq_ordem 	= nr_sequencia_p
and	nm_usuario_exec 	= nm_usuario_gestor_w;
	
if	(qt_existe_w = 0) then
	insert into man_ordem_servico_exec(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_exec,
			qt_min_prev,
			nr_seq_tipo_exec,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	man_ordem_servico_exec_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			nm_usuario_gestor_w,
			45,
			'3',
			sysdate,
			nm_usuario_p);
end if;

update	man_ordem_servico
set	nr_seq_estagio = 371,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

update	man_ordem_servico_exec
set	dt_fim_execucao 	= sysdate
where	nr_seq_ordem 		= nr_sequencia_p
and	nm_usuario_exec 	= nm_usuario_p
and	dt_fim_execucao is null;

begin
converte_rtf_string('select ds_relat_tecnico from man_ordem_serv_tecnico where nr_seq_ordem_serv = :nr_sequencia_p', nr_sequencia_p, nm_usuario_p, nr_seq_rtf_srtring_w);
select	ds_texto
into	ds_historico_os_w
from	tasy_conversao_rtf
where	nr_sequencia = nr_seq_rtf_srtring_w;
exception when others then
	ds_historico_os_w := 'Verificar ordem de servi�o ' || to_char(nr_sequencia_p) || '.';
end;

ds_titulo_w	:= 'OS ' || to_char(nr_sequencia_p) || ' encaminhada para p�s-venda';
ds_comunic_w	:= substr('A ordem de servi�o ' || to_char(nr_sequencia_p) || ' foi encaminhada para sua an�lise.' || chr(10) || chr(13) || chr(10) || chr(13) ||
			obter_desc_expressao(508099)/*'�ltimo hist�rico: '*/ || chr(10) || chr(13) || ds_historico_os_w,1,4000);

Gerar_Comunic_Padrao(
		sysdate,
		ds_titulo_w,
		ds_comunic_w,
		nm_usuario_p,
		'N',
		nm_usuario_gestor_w || ',',
		'N',
		5,
		null,
		null,
		null,
		sysdate,
		null,
		null);

Insert into man_ordem_serv_envio(
	nr_sequencia,
	nr_seq_ordem,
	dt_atualizacao,
	nm_usuario,
	dt_envio,
	ie_tipo_envio,
	ds_destino,
	ds_observacao)
values(	man_ordem_serv_envio_seq.nextval,
	nr_sequencia_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	'I',
	nm_usuario_gestor_w,
	ds_titulo_w);

select	nvl(substr(obter_dados_usuario_opcao(nm_usuario_gestor_w,'E'),1,255),'X')
into	ds_email_w
from	dual;

if	(ds_email_w <> 'X') then 

	begin
	ie_erro_mail_w	:= 'N';
	Enviar_Email(ds_titulo_w, ds_comunic_w, null, ds_email_w, nm_usuario_p,'M');
	exception when others then
	ie_erro_mail_w	:= 's';
	end;

	if	(ie_erro_mail_w = 'N') then

		Insert into man_ordem_serv_envio(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			dt_envio,
			ie_tipo_envio,
			ds_destino,
			ds_observacao)
		values(	man_ordem_serv_envio_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'E',
			ds_email_w,
			ds_titulo_w);
		
	end if;

end if;

commit;		

end des_encaminha_os_gestor_conta;
/