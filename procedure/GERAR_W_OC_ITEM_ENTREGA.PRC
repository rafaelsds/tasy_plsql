create or replace 
procedure GERAR_W_OC_ITEM_ENTREGA(	nr_ordem_compra_p	number,
					nr_item_oci_p		number,
					nm_ususario_p		varchar2,
					ie_exige_ocorrencia_p	varchar2) is



cursor c01 is
select	dt_prevista_entrega,
	(qt_prevista_entrega - nvl(qt_real_entrega,0)) qt_prevista_entrega,
	ds_observacao
from	ordem_compra_item_entrega
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p
and	qt_prevista_entrega > nvl(qt_real_entrega,0)
and	dt_cancelamento is null
order by dt_prevista_entrega;

dt_prevista_entrega_w	ordem_compra_item_entrega.dt_prevista_entrega%type;
qt_prevista_entrega_w	ordem_compra_item_entrega.qt_prevista_entrega%type;
ds_observacao_w		ordem_compra_item_entrega.ds_observacao%type;
qt_registros_w		number;

begin

if	(ie_exige_ocorrencia_p = 'S') then

	select  count(*)
	into	qt_registros_w
	from    ordem_compra_registro_ocor
	where   nr_ordem_compra = nr_ordem_compra_p;

	if	(qt_registros_w = 0) then
		-- Somente e permitido desdobrar as entregas do item se registrar alguma ocorrencia na ordem de compra.
                -- A ocorrencia e registrada atraves da opcao Gravar registro ocorrencia. Verifique o parametro [145].
		wheb_mensagem_pck.exibir_mensagem_abort(182505);
	end if;

end if;

delete from W_OC_ITEM_ENTREGA
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p
and	nm_usuario = nm_ususario_p;

open C01;
loop
fetch C01 into
	dt_prevista_entrega_w,
	qt_prevista_entrega_w,
	ds_observacao_w;
exit when C01%notfound;
	begin
    
     grava_log_tasy(882,'qt_prevista_entrega_w=' || qt_prevista_entrega_w || ', nr_ordem_compra_p=' || nr_ordem_compra_p || ', nr_item_oci_p=' || nr_item_oci_p,'TASY');	

	insert into W_OC_ITEM_ENTREGA
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_ordem_compra,
		nr_item_oci,
		qt_prevista_entrega,
		dt_prevista_entrega,
		ds_observacao)
	values	(w_oc_item_entrega_seq.nextval,
		sysdate,
		nm_ususario_p,
		nr_ordem_compra_p,
		nr_item_oci_p,
		qt_prevista_entrega_w,
		dt_prevista_entrega_w,
		ds_observacao_w);

	end;
end loop;
close C01;

commit;

end GERAR_W_OC_ITEM_ENTREGA;
/
