create or replace
procedure Consiste_SubClasse_Terap_ATC(	cd_subclasse_terap_p		varchar2,
					ds_subclasse_terap_p		varchar2,
					cd_classe_terap_p		varchar2,
					nm_usuario_p			varchar2) is

qt_registros_w		number(10);

begin

begin
select	count(*)
into	qt_registros_w
from	med_subclasse_terapeutica
where	cd_subclasse_terapeutica	= cd_subclasse_terap_p;
exception
	when others then
	qt_registros_w	:= 0;
end;

if	(qt_registros_w = 0) then
	insert into med_subclasse_terapeutica (
		cd_subclasse_terapeutica,
		dt_atualizacao,
		nm_usuario,
		ds_subclasse_terapeutica,
		cd_classe_terapeutica,
		dt_atualizacao_nrec,
		nm_usuario_nrec
	) values (
		cd_subclasse_terap_p,
		sysdate,
		nm_usuario_p,
		ds_subclasse_terap_p,
		cd_classe_terap_p,
		sysdate,
		nm_usuario_p
	);
else	
	update	med_subclasse_terapeutica
	set	ds_subclasse_terapeutica	= ds_subclasse_terap_p,
		nm_usuario			= nm_usuario_p,
		dt_atualizacao			= sysdate
	where	cd_subclasse_terapeutica	= cd_subclasse_terap_p;
end if;

commit;

end Consiste_SubClasse_Terap_ATC;
/	