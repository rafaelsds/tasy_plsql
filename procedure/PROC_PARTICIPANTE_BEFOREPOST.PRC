create or replace
procedure proc_participante_beforepost(	cd_funcao_p		Varchar2,
					dt_conta_p		Date,
					ie_origem_proced_p	Number,
					cd_procedimento_p	Number,
					cd_edicao_amb_p		Number,
					cd_categoria_p 		Number,	
					cd_convenio_p 		Number,					
					cd_estabelecimento_p	Number,					
					nm_usuario_p		Varchar2) is 
			
ie_anestesista_w	 Varchar2(1);
ds_porte_w		 Varchar2(5);
ie_consiste_anest_port_w Varchar2(1);

begin
--Utilizado na Gest�o de exames Java

select 	max(ie_consiste_anest_porte) 
into	ie_consiste_anest_port_w
from 	parametro_faturamento 
where  	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_consiste_anest_port_w = 'S') then

	select 	max(ie_anestesista)
	into	ie_anestesista_w 
	from 	funcao_medico 
	where  	cd_funcao = cd_funcao_p;

	ds_porte_w := obter_dados_preco_proc(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, cd_edicao_amb_p, cd_procedimento_p, ie_origem_proced_p, dt_conta_p, 'P');

	if 	(ie_anestesista_w = 'S') and
		(ds_porte_w = '0') then
		/*N�o � permitido o lan�amento de anestesistas para procedimentos sem porte anest�sico. Verifique os par�metros do faturamento.'||'#@#@');*/
		Wheb_mensagem_pck.exibir_mensagem_abort(261478);
	end if;

elsif 	(ie_consiste_anest_port_w = 'P') then
	
	select 	max(ie_anestesista)
	into	ie_anestesista_w 
	from 	funcao_medico 
	where  	cd_funcao = cd_funcao_p;

	ds_porte_w := obter_dados_preco_proc(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, cd_edicao_amb_p, cd_procedimento_p, ie_origem_proced_p, dt_conta_p, 'PN');

	if 	(ie_anestesista_w = 'S') and
		(ds_porte_w = '0') then
		/*N�o � permitido o lan�amento de anestesistas para procedimentos sem porte anest�sico. Verifique os par�metros do faturamento.'||'#@#@');*/
		Wheb_mensagem_pck.exibir_mensagem_abort(261478);
	end if;

end if;

end proc_participante_beforepost;
/