create or replace
procedure ctb_acumular_demo(	nr_seq_demo_p   	number,
				nm_usuario_p	varchar2) is 


nr_sequencia_w			number(10,0);
nr_seq_tipo_w			number(10,0);
nr_seq_mes_ref_w			number(10,0);
ds_origem_w			varchar2(4000);
vl_referencia_w			number(17,4);
nr_seq_col_w			number(10,0);
ie_origem_valor_w			varchar2(3);

cursor c01 is
select	distinct 
	nr_seq_mes_ref,
	nr_seq_col
from	ctb_demo_rubrica
where	nr_seq_demo	= nr_seq_demo_p;

cursor c02 is
select	a.nr_sequencia,
	ds_origem,
	b.ie_origem_valor
from	ctb_modelo_rubrica b,
	ctb_demo_rubrica a
where	a.nr_seq_demo	= nr_seq_demo_p
and	a.nr_seq_mes_ref	= nr_seq_mes_ref_w
and	a.nr_seq_col	= nr_seq_col_w
and	b.ie_origem_valor	in('SR','FR')
and	a.nr_seq_rubrica	= b.nr_sequencia
order by nr_seq_somat, nr_seq_apres;
	
begin

open	c01;
loop
fetch	c01 into 
	nr_seq_mes_ref_w,
	nr_seq_col_w;
exit	when c01%notfound;
	open	c02;
	loop
	fetch	c02 into 
		nr_sequencia_w,
		ds_origem_w,
		ie_origem_valor_w;
	exit	when c02%notfound;

		if	(ie_origem_valor_w = 'SR') then
	
			select	ctb_obter_valor_demo(
				nr_seq_demo_p,
				nr_seq_mes_ref_w,
				ds_origem_w,
				nr_seq_col_w)
			into	vl_referencia_w
			from	dual;
		elsif	(ie_origem_valor_w = 'FR') then
			select	ctb_obter_valor_function(
				nr_seq_demo_p,
				nr_seq_mes_ref_w,
				ds_origem_w,
				nr_seq_col_w)
			into	vl_referencia_w
			from	dual;
		end if;
		update	ctb_demo_rubrica
		set	vl_referencia	= vl_referencia_w
		where	nr_sequencia	= nr_sequencia_w
		and	nr_seq_col	= nr_seq_col_w;

	end loop;
	close c02;
end loop;
close c01;
commit;
ctb_calc_demo_var_per(nr_seq_demo_p, nm_usuario_p);
commit;

end ctb_acumular_demo;
/