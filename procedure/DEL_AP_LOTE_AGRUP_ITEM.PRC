create or replace
procedure del_ap_lote_agrup_item(
			nr_seq_lote_p number) is 

begin

	if (nr_seq_lote_p is not null) then
		begin

			delete 	ap_lote_agrup_item 
			where 	nr_seq_lote = nr_seq_lote_p;
		
			commit;
		end;	
	end if;
	
end del_ap_lote_agrup_item;
/