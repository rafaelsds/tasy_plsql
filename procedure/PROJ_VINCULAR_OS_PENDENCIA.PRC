create or replace
procedure proj_vincular_os_pendencia(		nr_seq_ordem_p		number,
					nr_seq_pendencia_p	number,
					ie_acao_p		varchar2,
					ie_atual_status_pend_p	Varchar2,
					nm_usuario_p		Varchar2) is 

begin

if (ie_acao_p = 'V') then
	insert into proj_ata_pendencia_os(
		nr_sequencia,
		nr_seq_pendencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_acordo)
	values(	proj_ata_pendencia_os_seq.nextval,
		nr_seq_pendencia_p,
		nr_seq_ordem_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'N');
	
	if	(nvl(ie_atual_status_pend_p, 'N') = 'S') then

		update	proj_ata_pendencia
		set	ie_status = 'O'
		where	nr_sequencia	= nr_seq_pendencia_p;
	
	end if;
	
elsif (ie_acao_p = 'D') then
	delete	proj_ata_pendencia_os
	where	nr_seq_ordem	= nr_seq_ordem_p
	and	nr_seq_pendencia	= nr_seq_pendencia_p;
end if;

commit;

end proj_vincular_os_pendencia;
/