create or replace
procedure gerar_mat_exclui_proc_pacote(	nr_seq_autorizacao_p	number,
					nr_seq_autor_conv_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

ie_pacote_w		varchar2(1);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_convenio_w		number(5);
nr_seq_pacote_w		number(10);
cd_setor_atendimento_w	number(5);
ie_tipo_atendimento_w	number(3);
cd_material_w		number(6);
cd_material_ww		number(6) := 0;
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(3);
nr_seq_familia_w		number(10);
qt_idade_paciente_w	number(3,0);
cd_pessoa_fisica_w	varchar2(10);
ie_sexo_w		varchar2(1);
nr_seq_agenda_w		number(10);

Cursor C01 is
	select	b.nr_seq_pacote
	from 	pacote b,
		pacote_material a
	where	a.nr_seq_pacote = b.nr_seq_pacote
	and	b.cd_proced_pacote = cd_procedimento_w
	and 	b.ie_origem_proced = ie_origem_proced_w
	and 	b.cd_convenio = cd_convenio_w
	and 	b.cd_estabelecimento = cd_estabelecimento_p
	and	((a.cd_setor_atendimento is null) or (cd_setor_atendimento_w = a.cd_setor_atendimento))
	and	((a.ie_tipo_atendimento is null) or (a.ie_tipo_atendimento = ie_tipo_atendimento_w))
	and	nvl(a.cd_grupo_material, cd_grupo_material_w) = cd_grupo_material_w
	and	nvl(a.cd_subgrupo_material, cd_subgrupo_material_w) = cd_subgrupo_material_w
	and	nvl(a.cd_classe_material, cd_classe_material_w) = cd_classe_material_w
	and	nvl(a.cd_material, cd_material_w) = cd_material_w
	and	nvl(a.nr_seq_familia, nr_seq_familia_w) = nr_seq_familia_w
	and	qt_idade_paciente_w between nvl(a.qt_idade_min, 0) and nvl(a.qt_idade_max, 999)
	and	nvl(a.ie_sexo, nvl(ie_sexo_w,'A')) = nvl(ie_sexo_w,'A')
	and	a.ie_inclui_exclui = 'E'
	and	b.ie_situacao = 'A';

Cursor C02 is
	select	distinct
		cd_material
	from	material_autor_cirurgia
	where	nr_seq_autorizacao = nr_seq_autorizacao_p;

begin

delete	from w_pacote_mat_autor_conv
where	nm_usuario = nm_usuario_p;

select	max(cd_procedimento_principal),
	max(ie_origem_proced),
	max(cd_convenio),
	max(obter_setor_atendimento(nr_atendimento)),
	max(nr_seq_agenda)
into	cd_procedimento_w,
	ie_origem_proced_w,
	cd_convenio_w,
	cd_setor_atendimento_w,
	nr_seq_agenda_w
from	autorizacao_convenio
where	nr_sequencia = nr_seq_autor_conv_p;

select	max(ie_tipo_atendimento),
	max(cd_pessoa_fisica)
into	ie_tipo_atendimento_w,
	cd_pessoa_fisica_w
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_w;

if	(cd_pessoa_fisica_w is null) then
	
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	autorizacao_cirurgia
	where	nr_sequencia = nr_seq_autorizacao_p;

end if;

select	max(obter_idade(dt_nascimento, sysdate, 'A')),
	nvl(max(ie_sexo),'A')
into	qt_idade_paciente_w,
	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

select	nvl(max(substr(obter_se_pacote_convenio(cd_procedimento_w, ie_origem_proced_w, cd_convenio_w, cd_estabelecimento_p),1,3)),'N')
into	ie_pacote_w
from	dual;

if	(ie_pacote_w = 'S') then

	open C02;
	loop
	fetch C02 into	
		cd_material_w;
	exit when C02%notfound;
		begin
		
		select	max(cd_grupo_material),
			max(cd_subgrupo_material),
			max(cd_classe_material),
			nvl(max(nr_seq_familia),0)
		into	cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w,
			nr_seq_familia_w
		from	estrutura_material_v
		where	cd_material = cd_material_w;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_pacote_w;
		exit when C01%notfound;
			begin
			
			if	(cd_material_w <> cd_material_ww) then
				
				cd_material_ww := cd_material_w;
				
				insert into w_pacote_mat_autor_conv(	
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_pacote,
					cd_material,
					ie_exclui_pacote)
				values(	w_pacote_mat_autor_conv_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_pacote_w,
					cd_material_ww,
					'S');
			end if;
			
			end;
		end loop;
		close C01;
		
		end;
	end loop;
	close C02;

end if;

commit;

end gerar_mat_exclui_proc_pacote;
/