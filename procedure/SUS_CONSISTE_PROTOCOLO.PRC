CREATE OR REPLACE
PROCEDURE Sus_Consiste_Protocolo
		(	nr_seq_protocolo_p	Number,
			ie_funcao_p		Varchar2,
			nm_usuario_p		Varchar2) is

/*
IE_FUNCAO_P

AIH
BPA
APAC
*/

ds_detalhe_w		Varchar2(255)	:= '';
cd_estabelecimento_w	Number(4);
nr_seq_apac_w		Number(10);
nr_apac_w		Number(13);
cd_procedimento_apac_w	Number(15);
ie_origem_proc_apac_w	Number(10);
cd_cid_principal_w	Varchar2(4);
cd_cid_secundario_w	Varchar2(4);
cd_cid_causa_assoc_w	Varchar2(4);
ie_apac_w		Varchar2(1)	:= 'S';
nr_apac_digito13_w	Number(1);
ds_erro_w		varchar2(255);
nr_seq_protocolo_w	Number(10);
dt_competencia_apac_w	Date;
dt_competencia_prot_w	Date;
nr_interno_conta_w	Number(10);
nr_atendimento_w	Number(10);

pr_cesariana_permitida_w	Number(7,4)	:= 0;
qt_cesariana_w			Number(10)	:= 0;
qt_total_parto_w		Number(10)	:= 0;
pr_cesariana_w			Number(7,4)	:= 0;
qt_parto_normal_w		Number(7,4)	:= 0;
pr_sobra_w			Number(7,4)	:= 0;

cursor C01 is
	select	nr_interno_conta,
		nr_atendimento,
		nvl(cd_estabelecimento,1)
	from	conta_paciente
	where	nr_seq_protocolo	= nr_seq_protocolo_p;

BEGIN
OPEN C01;
	LOOP
	FETCH C01 into
		nr_interno_conta_w,
		nr_atendimento_w,
		cd_estabelecimento_w;
	exit when C01%notfound;
		begin
		sus_consiste_conta(nr_interno_conta_w, nr_atendimento_w, nm_usuario_p);
		
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(299742, 'NR_SEQ_PROTOCOLO= ' || nr_seq_protocolo_p ||
								';NR_ATENDIMENTO=' || nr_atendimento_w ||
								';NR_INTERNO_CONTA=' || nr_interno_conta_w);
		
		if	(ie_funcao_p	= 'APAC') then
			/* Obter dados da APAC */
			begin
			select	nr_sequencia,
				nr_apac,
				cd_procedimento,
				ie_origem_proced,
				cd_cid_principal,
				cd_cid_secundario,
				cd_cid_causa_assoc,
				dt_competencia
			into	nr_seq_apac_w,
				nr_apac_w,
				cd_procedimento_apac_w,
				ie_origem_proc_apac_w,
				cd_cid_principal_w,
				cd_cid_secundario_w,
				cd_cid_causa_assoc_w,
				dt_competencia_apac_w
			from	sus_apac_unif
			where	nr_interno_conta	= nr_interno_conta_w
			and	nr_atendimento		= nr_atendimento_w;
			exception
				when others then
			/* 19 - Falta APAC para a conta, ou existe mais de uma APAC */
			if	(Sus_Obter_Inco_Ativa(19)) then
				sus_gravar_inconsistencia(nr_interno_conta_w, 19, '', cd_estabelecimento_w, nm_usuario_p);
			end if;
			end;	
					
			/*  Obter dados do protocolo */ 
			select max(nvl(b.dt_mesano_referencia,dt_competencia_apac_w))
			into	 dt_competencia_prot_w
			from	 protocolo_convenio b,
				 conta_paciente a
			where	 a.nr_seq_protocolo = b.nr_seq_protocolo
			and	 a.nr_interno_conta = nr_interno_conta_w;		
	
			/* 41 - Data de competencia da apac difere do protocolo */
			if	(Sus_Obter_Inco_Ativa(41)) and
				(dt_competencia_prot_w 	is not null) and
				(dt_competencia_apac_w	is not null) and
				(trunc(dt_competencia_apac_w,'month') <> trunc(dt_competencia_prot_w,'month')) then
				sus_gravar_inconsistencia(nr_interno_conta_w, 41, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end if;
		else if	(ie_funcao_p	= 'AIH') then
			begin
			Select	pr_cesariana_permitida
			into	pr_cesariana_permitida_w
			from	sus_parametros_aih
			where	cd_estabelecimento = cd_estabelecimento_w;
			exception
				when others then
					pr_cesariana_permitida_w := 100;
			end;

			begin
			Select 	sum(a.qt_procedimento)
			into 	qt_parto_normal_w
			from 	procedimento_paciente a,
				conta_paciente b
			where  	a.nr_interno_conta  	= b.nr_interno_conta
			and	b.nr_seq_protocolo	= nr_seq_protocolo_p
			and	a.cd_procedimento in(310010039, 310010047)
			and	a.cd_motivo_exc_conta is null;
			exception
				when others then
					qt_parto_normal_w := 0;
			end;			

			if	(pr_cesariana_permitida_w <> 100) and
				(pr_cesariana_permitida_w > 0) 	then
				begin
				Select	sum(a.qt_procedimento)
				into 	qt_cesariana_w
				from 	procedimento_paciente a,
					conta_paciente b
				where  	a.nr_interno_conta  	= b.nr_interno_conta
				and	b.nr_seq_protocolo	= nr_seq_protocolo_p
				and	(sus_validar_regra(33, a.cd_procedimento, a.ie_origem_proced,a.dt_procedimento) > 0)
				and	a.cd_motivo_exc_conta is null;
				exception
					when others then
						qt_cesariana_w := 0;
				end;
		
				qt_total_parto_w	:= (qt_cesariana_w + qt_parto_normal_w);
				if	(qt_total_parto_w = 0) then
					qt_total_parto_w := 1;
				end if;	
				pr_cesariana_w	:= ((qt_cesariana_w * 100) /	qt_total_parto_w);
				
				if	(Sus_Obter_Inco_Ativa(49)) and
					(pr_cesariana_w > pr_cesariana_permitida_w) then
					begin
					pr_sobra_w 	:= pr_cesariana_w - pr_cesariana_permitida_w;
					ds_detalhe_w 	:= ceil((qt_total_parto_w * pr_sobra_w)/100);
					sus_gravar_inconsistencia(nr_interno_conta_w, 49, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
					end;
				end if;
			end if;
		end if;
		end if;
		end;
	END LOOP;
CLOSE C01;

END Sus_Consiste_Protocolo;
/
