create or replace
procedure obter_dados_segurado_agenda(
		nr_seq_segurado_p		number,
		nm_usuario_p		varchar2,
		cd_estabelecimento_p	number,
		cd_convenio_p		out number,
		cd_categoria_p		out varchar2,
		cd_usuario_convenio_p	out varchar2,
		cd_pessoa_fisica_p 	out varchar2,
		nm_pessoa_fisica_p	out varchar2,
		ie_situacao_atend_p	out varchar2,
		ds_abort_p		out varchar2,
		dt_validade_carteira_p	out date,
		cd_plano_p		out varchar2,
		ie_tipo_atendimento_p number default null) is 

begin

if (nvl(nr_seq_segurado_p,0) > 0) then
	select	cd_pessoa_fisica,
			ie_situacao_atend
	into	cd_pessoa_fisica_p,
			ie_situacao_atend_p
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;

	select	nm_pessoa_fisica
	into	nm_pessoa_fisica_p
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	cd_usuario_convenio_p 	:= obter_dados_seg_entrada_unica(nr_seq_segurado_p, 'CI', ie_tipo_atendimento_p);
  
  --mesmo select feito na obter_dados_seg_entrada_unica(nr_seq_segurado_p, 'VC', ie_tipo_atendimento_p), alterado pois esse retorna um varchar em vez de data
  select	max(b.dt_validade_carteira)
	into	dt_validade_carteira_p
	from	pls_segurado_carteira b,
		pls_segurado a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_p;

	cd_convenio_p			:= obter_dados_seg_entrada_unica(nr_seq_segurado_p, 'CV', ie_tipo_atendimento_p); 
	cd_categoria_p			:= obter_dados_seg_entrada_unica(nr_seq_segurado_p, 'CA', ie_tipo_atendimento_p); 
	cd_plano_p				:= obter_dados_seg_entrada_unica(nr_seq_segurado_p, 'PLC', ie_tipo_atendimento_p);
end if;

ds_abort_p := obter_texto_tasy(143449, wheb_usuario_pck.get_nr_seq_idioma);

end obter_dados_segurado_agenda;
/
