create or replace
procedure  pls_imp_rec_glosa_prot_cta(	nr_seq_protocolo_p		number,
					cd_guia_prestador_p		varchar2,
					cd_ans_p			varchar2,
					cd_guia_p			varchar2,					
					nm_operadora_p			varchar2,
					cd_prestador_executor_p		varchar2, 
					nm_prestador_executor_p		varchar2,
					ie_recurso_glosa_p		varchar2,
					nr_protocolo_p 			varchar2,
					vl_total_recursado_p		number,
					dt_recurso_p			date,
					nr_lote_p			varchar2,
					nm_usuario_p			varchar2,
					cd_cpf_prestador_imp_p		pls_rec_glosa_prot_cta_imp.cd_cpf_prestador_imp%type,
					cd_cgc_prestador_imp_p		pls_rec_glosa_prot_cta_imp.cd_cgc_prestador_imp%type,
					nr_seq_protocolo_cpt_p   out    number) is 
					
														
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importar todas os protocolos do arquivo de Recurso de Glosa do padr�o TISS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_protocolo_cpt_imp_w		Number(10);
nr_seq_prestador_w			pls_rec_glosa_prot_cta_imp.nr_seq_prestador%type;
nr_seq_prestador_aux_w			pls_rec_glosa_prot_cta_imp.nr_seq_prestador%type;
begin

if	(cd_cgc_prestador_imp_p	is not null) then
	nr_seq_prestador_aux_w	:= pls_obter_prestador_cgc(	cd_cgc_prestador_imp_p,null); 
	
	if	(nr_seq_prestador_aux_w	is not null) then
		nr_seq_prestador_w	:= nr_seq_prestador_aux_w;
	end if;	
end if;

if	(nr_seq_prestador_w is null) then
	nr_seq_prestador_w	:= pls_obter_prestador_imp(	cd_cgc_prestador_imp_p, cd_cpf_prestador_imp_p, cd_prestador_executor_p, 
								null, null, null);
end if;

select 	pls_rec_glosa_prot_cta_imp_seq.nextval
into	nr_seq_protocolo_cpt_imp_w
from 	dual;

insert into pls_rec_glosa_prot_cta_imp( nr_sequencia, dt_recurso , dt_atualizacao,
					nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
					vl_total_recursado, cd_ans, cd_guia,                
					cd_guia_prestador, nm_operadora, ie_recurso_glosa,
					cd_prestador_executor, nm_prestador_executor, nr_lote,
					nr_protocolo, nr_seq_protocolo,nr_seq_prestador,
					cd_cpf_prestador_imp, cd_cgc_prestador_imp)
			values (	nr_seq_protocolo_cpt_imp_w, dt_recurso_p , sysdate,
					nm_usuario_p, sysdate, nm_usuario_p,
					vl_total_recursado_p, cd_ans_p, cd_guia_p,                
					cd_guia_prestador_p, nm_operadora_p, ie_recurso_glosa_p,
					cd_prestador_executor_p, nm_prestador_executor_p, nr_lote_p,
					nr_protocolo_p, nr_seq_protocolo_p, nr_seq_prestador_w,
					cd_cpf_prestador_imp_p, cd_cgc_prestador_imp_p);

/*----------------------------------------------------------------------------------------------------
		OS 874282 --- Edson (ekjunior)
    Criado esse trecho com UPDATE para alimentar o campo Prestador
    na tabela PLS_REC_GLOSA_PROTOCOLO com a informa��o vinda no XML.
    Tratamento realizado ap�s comentar trecho na 
    PROCEDURE - PLS_GERAR_REC_GLOSA_PROTOCOLO. Linha 20
----------------------------------------------------------------------------------------------------*/
update	pls_rec_glosa_protocolo
set	nr_seq_prestador 	= nr_seq_prestador_w,
	nr_lote_prestador	= nr_lote_p
where	nr_sequencia 		= nr_seq_protocolo_p
and	nr_seq_prestador is null;
/*-------------------------------------------------------------------------------------------------------*/

nr_seq_protocolo_cpt_p 	:= nr_seq_protocolo_cpt_imp_w;

end pls_imp_rec_glosa_prot_cta;
/
