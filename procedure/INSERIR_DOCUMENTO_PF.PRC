create or replace
procedure inserir_documento_pf(	cd_pessoa_fisica_p	varchar2,
				ie_tipo_doc_p		varchar2,
				ds_arquivo_p		varchar2,
				nm_usuario_p		Varchar2) is 

nr_sequencia_seq_w	number(10);
nr_sequencia_tipo_doc_w	number(10);
ds_documento_w		varchar2(100);
begin

/* ie_tipo_doc_p

CO - Certid�o �bito
ES - Escolaridade
PR - Procura��o
PC - Cart�o conv�nio
DF - Documento pessoal
PM - Pedido m�dico

*/

if	(ie_tipo_doc_p = 'CO') then

	select	max(nr_sequencia),
		max(ds_documento)
	into	nr_sequencia_tipo_doc_w,
		ds_documento_w
	from	tipo_documentacao
	where	IE_OBITO = 'S';

elsif	(ie_tipo_doc_p = 'ES') then

	select	max(nr_sequencia),
		max(ds_documento)
	into	nr_sequencia_tipo_doc_w,
		ds_documento_w
	from	tipo_documentacao
	where	IE_ESCOLARIDADE = 'S';
	
elsif	(ie_tipo_doc_p = 'PR') then

	select	max(nr_sequencia),
		max(ds_documento)
	into	nr_sequencia_tipo_doc_w,
		ds_documento_w
	from	tipo_documentacao
	where	IE_PROCURACAO = 'S';

elsif	(ie_tipo_doc_p = 'PC') then

	select	max(nr_sequencia),
		max(ds_documento)
	into	nr_sequencia_tipo_doc_w,
		ds_documento_w
	from	tipo_documentacao
	where	IE_CART_CONVENIO = 'S';

elsif	(ie_tipo_doc_p = 'DF') then

	select	max(nr_sequencia),
		max(ds_documento)
	into	nr_sequencia_tipo_doc_w,
		ds_documento_w
	from	tipo_documentacao
	where	IE_DOC_PESSOAL = 'S';
	
elsif	(ie_tipo_doc_p = 'PM') then

	select	max(nr_sequencia),
		max(ds_documento)
	into	nr_sequencia_tipo_doc_w,
		ds_documento_w
	from	tipo_documentacao
	where	IE_PEDIDO_MEDICO = 'S';
	
end if;

select	pessoa_documentacao_seq.nextval
into	nr_sequencia_seq_w
from	dual;

insert into PESSOA_DOCUMENTACAO (
	nr_sequencia, 
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_atualizacao,
	nm_usuario, 
	nr_seq_documento, 
	cd_pessoa_fisica, 
	ds_arquivo,
	ie_entregue,
	ds_titulo,
	ie_situacao)
values (nr_sequencia_seq_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_sequencia_tipo_doc_w,
	cd_pessoa_fisica_p,
	ds_arquivo_p,
	'N',
	ds_documento_w,
	'A');

commit;

end inserir_documento_pf;
/
