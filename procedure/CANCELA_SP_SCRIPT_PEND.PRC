create or replace
procedure CANCELA_SP_SCRIPT_PEND(cd_versao_p	varchar2,
				nr_service_pack_p 	varchar2,
				nm_usuario_p	Varchar2,
				cd_aplicacao_tasy_p varchar2 default null) is 

begin

update	SERVICE_PACK_SCRIPT_PEND
set	nr_service_pack = null
where	cd_version = cd_versao_p
and	nr_service_pack = nr_service_pack_p
and     nvl(cd_aplicacao_tasy,'Tasy') = check_application_sp@WHEBL02_ORCL(cd_aplicacao_tasy_p);
commit;

end CANCELA_SP_SCRIPT_PEND;
/
