create or replace
procedure int_inserir_dieta_oral(
		cd_dieta_p                number,
		cd_intervalo_p            varchar2,
		cd_motivo_baixa_p         number,
		cd_perfil_ativo_p         number,
		ds_horarios_p             varchar2,
		ds_justificativa_p        varchar2,
		ds_motivo_susp_p          varchar2,
		ds_observacao_p           varchar2,
		dt_atualizacao_p          date,
		dt_suspensao_p            date,
		hr_dose_especial_p        varchar2,
		hr_prim_horario_p         varchar2,
		ie_dose_espec_agora_p     varchar2,
		ie_suspenso_p             varchar2,
		ie_urgencia_p             varchar2,
		ie_via_aplicacao_p        varchar2,
		nm_usuario_p              varchar2,
		nm_usuario_susp_p         varchar2,
		nr_dia_util_p             number,
		nr_prescricao_p           number,
		nr_seq_interno_p          number,
		nr_seq_motivo_susp_p      number,
		qt_horas_jejum_p          number,
		qt_parametro_p            number,
		nr_sequencia_p			out number,
		ds_erro_p			out varchar2) is 

nr_sequencia_w		number(15);

begin

begin

select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	prescr_dieta
where	nr_prescricao	= nr_prescricao_p;

insert into prescr_dieta (
		cd_dieta                ,
		cd_intervalo            ,
		cd_motivo_baixa         ,
		cd_perfil_ativo         ,
		ds_horarios             ,
		ds_justificativa        ,
		ds_motivo_susp          ,
		ds_observacao           ,
		dt_atualizacao          ,
		dt_suspensao            ,
		hr_dose_especial        ,
		hr_prim_horario         ,
		ie_dose_espec_agora     ,
		ie_suspenso             ,
		ie_urgencia             ,
		ie_via_aplicacao        ,
		nm_usuario              ,
		nm_usuario_susp         ,
		nr_dia_util             ,
		nr_prescricao           ,
		nr_seq_interno          ,
		nr_seq_motivo_susp      ,
		qt_horas_jejum          ,
		qt_parametro,
		nr_sequencia)
		values (
		cd_dieta_p                ,
		cd_intervalo_p            ,
		cd_motivo_baixa_p         ,
		cd_perfil_ativo_p         ,
		ds_horarios_p             ,
		ds_justificativa_p        ,
		ds_motivo_susp_p          ,
		ds_observacao_p           ,
		dt_atualizacao_p          ,
		dt_suspensao_p            ,
		hr_dose_especial_p        ,
		hr_prim_horario_p         ,
		ie_dose_espec_agora_p     ,
		ie_suspenso_p             ,
		ie_urgencia_p             ,
		ie_via_aplicacao_p        ,
		nm_usuario_p              ,
		nm_usuario_susp_p         ,
		nr_dia_util_p             ,
		nr_prescricao_p           ,
		nr_seq_interno_p          ,
		nr_seq_motivo_susp_p      ,
		qt_horas_jejum_p          ,
		qt_parametro_p,
		nr_sequencia_w);

	commit;

nr_sequencia_p		:= 	nr_sequencia_w;

exception
when others then
	ds_erro_p	:= substr(SQLERRM,1,255);
end;

end int_inserir_dieta_oral;
/
