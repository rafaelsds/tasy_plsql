create or replace
procedure retirar_dt_integracao_exame(nr_prescricao_p	number,
									nm_usuario_p		Varchar2) is 

nr_seq_proc_mat_item_w	number(10);
nr_seq_prescr_w		number(10);

cursor c01 is
	select	a.nr_sequencia,
			a.nr_seq_prescr
	from	prescr_proc_mat_item a,
		prescr_procedimento b
	where	a.nr_seq_prescr  = b.nr_sequencia
	and	a.nr_prescricao = b.nr_prescricao
	and 	b.nr_seq_exame is not null
	and	a.dt_integracao is not null
	and	a.nr_prescricao = nr_prescricao_p;
	
begin

open c01;
loop
fetch c01 into
	nr_seq_proc_mat_item_w,
	nr_seq_prescr_w;
exit when c01%notfound;

	update 	prescr_proc_mat_item
	set	dt_integracao = null
	where	nr_sequencia = nr_seq_proc_mat_item_w;
		
	update	prescr_procedimento
	set		dt_integracao = null
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = nr_seq_prescr_w;
	
end loop;
close c01;
	
commit;

end retirar_dt_integracao_exame;
/