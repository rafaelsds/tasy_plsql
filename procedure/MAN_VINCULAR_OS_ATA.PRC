create or replace
procedure man_vincular_os_ata
			(	nr_seq_ordem_p		number,
				nr_seq_ata_p		number,
				nm_usuario_p		varchar2) is 
				

begin

update	ata_reuniao
set	nr_seq_ordem_serv	= nr_seq_ordem_p,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_ata_p;

commit;

end man_vincular_os_ata;
/