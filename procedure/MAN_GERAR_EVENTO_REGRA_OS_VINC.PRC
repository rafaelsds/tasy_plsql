create or replace
procedure man_gerar_evento_regra_os_vinc(
				ie_evento_p		varchar2,
				nr_sequencia_p		number,
				ds_valor_p		varchar2,
				nm_usuario_p		varchar2) is

/*	ie_evento_p
	1 - Alterar est�gio
	2 - Alterar tipo solu��o
	3 - Alterar tipo OS cliente
	4 - Alterar causa dano
	5 - Alterar a complexidade
	6 - Liberar um hist�rico
	7 - Gerar os executores previstos
	8 - Encerrar OS via webservice	*/
	
nr_seq_ordem_servico_w		number(10);
nm_usuario_exec_w		varchar2(15);
				
Cursor C01 is
	select	b.nr_sequencia
	from	man_ordem_servico b,
		man_ordem_serv_vinc a
	where	b.nr_sequencia = a.nr_seq_os_vinculada
	and	b.ie_status_ordem <> '3'
	and	b.dt_fim_real is null
	and	(ie_evento_p <> 8 or (ie_evento_p = 8 and b.nr_seq_wheb is null))
	and	a.nr_seq_ordem_servico = nr_sequencia_p;
	
Cursor C02 is
	select	nm_usuario_exec
	from	man_ordem_servico_exec
	where	nr_seq_ordem = nr_sequencia_p;

begin
wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('N');

if	(nvl(ie_evento_p,'0') <> '0') and
	(nvl(nr_sequencia_p,0) > 0) then
	begin
	if	(ie_evento_p = '1') then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_servico_w;
		exit when C01%notfound;
			begin
			update	man_ordem_servico
			set	nr_seq_estagio		= ds_valor_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_ordem_servico_w;
			end;
		end loop;
		close C01;
		end;
	elsif	(ie_evento_p = '2') then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_servico_w;
		exit when C01%notfound;
			begin
			update	man_ordem_servico
			set	nr_seq_tipo_solucao	= ds_valor_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_ordem_servico_w;
			end;
		end loop;
		close C01;
		end;
	elsif	(ie_evento_p = '3') then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_servico_w;
		exit when C01%notfound;
			begin
			update	man_ordem_servico
			set	nr_seq_tipo_ordem	= ds_valor_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_ordem_servico_w;
			end;
		end loop;
		close C01;
		end;
	elsif	(ie_evento_p = '4') then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_servico_w;
		exit when C01%notfound;
			begin
			update	man_ordem_servico
			set	nr_seq_causa_dano	= ds_valor_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_ordem_servico_w;
			end;
		end loop;
		close C01;
		end;
	elsif	(ie_evento_p = '5') then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_servico_w;
		exit when C01%notfound;
			begin
			update	man_ordem_servico
			set	nr_seq_complex		= ds_valor_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_seq_ordem_servico_w;
			end;
		end loop;
		close C01;
		end;
	elsif	(ie_evento_p = '6') then
		begin
		man_gerar_hist_os_vinculada(nr_sequencia_p, nm_usuario_p);		
		end;
	elsif	(ie_evento_p = '7') then
		begin		
		open C02;
		loop
		fetch C02 into	
			nm_usuario_exec_w;
		exit when C02%notfound;
			begin
			man_inserir_usuario_executor(ds_valor_p, nm_usuario_exec_w, nm_usuario_p, null, null);
			end;
		end loop;
		close C02;		
		end;
	elsif	(ie_evento_p = '8') then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_servico_w;
		exit when C01%notfound;
			begin
			update  man_ordem_servico
			set	ie_status_ordem		= 3,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				dt_inicio_real		= nvl(dt_inicio_real,dt_ordem_servico),
				dt_fim_real		= sysdate,
				nr_seq_estagio		= decode(ds_valor_p,0,nr_seq_estagio,ds_valor_p)
			where   nr_sequencia		= nr_seq_ordem_servico_w;
			end;
		end loop;
		close C01;
		end;
	end if;
	
	commit;
	end;
end if;
wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('S');

end man_gerar_evento_regra_os_vinc;
/