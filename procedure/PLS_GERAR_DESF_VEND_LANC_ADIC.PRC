create or replace
procedure pls_gerar_desf_vend_lanc_adic
		(	nr_seq_vendedor_p	number,
			nr_seq_repasse_p	number,
			dt_referencia_p		date,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number,
			ie_opcao_p		varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Adicionar ou apagar os lan�amentos adicionais dos repasses.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Um determinado repasse s� poder� ser desfeito se n�o houverem lan�amentos originados deste 
repasse j� cobrados em outro repasse ou comiss�o.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

/* IE_OPCAO_P:
	'G'	= Gerar repasse
	'D'	= Desfazer repasse
*/

/* Campos referente ao cursor 1 */
nr_titulo_w			titulo_receber.nr_titulo%type;
vl_titulo_w			titulo_receber.vl_titulo%type;

/* Campos referente ao cursor 2 */
vl_descontos_w			titulo_receber_liq.vl_descontos%type;

/* Campos referente ao cursor 3 e 4 */
vl_lancamento_antec_w		number(15,2);
ds_observacao_w			pls_vendedor_lanc.ds_observacao%type;

/* Campos referente ao cursor 5 */
vl_alteracao_w			alteracao_valor.vl_alteracao%type;

dt_referencia_w			date;
nr_seq_regra_vl_max_w		number;
vl_maximo_w			number(15,2);
ie_cobranca_futura_w		varchar2(1);
vl_repasse_w			number(15,2);
vl_lancamento_w			number(15,2);
nr_seq_repasse_lanc_w		number(10);
nr_seq_repasse_cobrado_w	number(10);
nr_seq_comissao_cobrada_w	number(10);
nr_seq_lote_cobrado_w		number(10);
vl_item_comissao_w		pls_repasse_mens.vl_item_comissao%type;
vl_repasse_tit_w		pls_repasse_mens.vl_repasse%type;
vl_descontos_aplic_w		titulo_receber_liq.vl_descontos%type;
vl_alteracao_aplic_w		alteracao_valor.vl_alteracao%type;
ie_situacao_w			titulo_receber.ie_situacao%type;
ie_repasse_sem_liq_tit_w	funcao_parametro.vl_parametro%type;

ie_gerar_desconto_tit_w		pls_parametros.ie_gerar_desconto_tit_repasse%type;
ie_tipo_desconto_tit_w		pls_parametros.ie_tipo_desconto_tit_repasse%type;
ie_valor_percent_desconto_w	pls_parametros.ie_valor_percent_desconto%type;

Cursor C01 is
	select	distinct c.nr_titulo,
			c.ie_situacao,
			c.vl_titulo
	from	pls_repasse_mens		a,
		pls_mensalidade_segurado	b,
		titulo_receber			c
	where	a.nr_seq_mens_seg	= b.nr_sequencia
	and	b.nr_seq_mensalidade	= c.nr_seq_mensalidade
	and	a.nr_seq_repasse	= nr_seq_repasse_p;

Cursor C02 is
	select	sum(vl_descontos)
	from	titulo_receber_liq	a
	where	a.nr_titulo		= nr_titulo_w;

Cursor C03 is
	select	vl_lancamento,
		ds_observacao
	from	pls_vendedor_lanc
	where	nr_seq_vendedor = nr_seq_vendedor_p
	and	nr_seq_repasse is null
	and	nr_seq_comissao is null
	and	ie_origem_lancamento <> 'M';

Cursor C04 is
	select	vl_lancamento,
		ds_observacao
	from	pls_vendedor_lanc
	where	nr_seq_vendedor = nr_seq_vendedor_p
	and	nr_seq_repasse is null
	and	nr_seq_comissao is null
	and	trunc(dt_lancamento,'month') = trunc(dt_referencia_p, 'month')
	and	ie_origem_lancamento = 'M';

Cursor C05 is
	select	sum(vl_alteracao)
	from	alteracao_valor		a
	where	a.nr_titulo		= nr_titulo_w
	and	a.ie_aumenta_diminui	= 'D'
	and	trunc(a.dt_alteracao,'month') <= trunc(dt_referencia_p, 'month');

begin

if	(ie_opcao_p = 'G') then
	
	ie_repasse_sem_liq_tit_w := obter_valor_param_usuario(1206, 16, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p);
	
	select	nvl(ie_gerar_desconto_tit_repasse,'N'),
		nvl(ie_tipo_desconto_tit_repasse,'A'),
		nvl(ie_valor_percent_desconto,'V')
	into	ie_gerar_desconto_tit_w,
		ie_tipo_desconto_tit_w,
		ie_valor_percent_desconto_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	select	sum(vl_repasse)
	into	vl_repasse_w
	from	pls_repasse_mens
	where	nr_seq_repasse = nr_seq_repasse_p;
	
	vl_repasse_w := nvl(vl_repasse_w,0);
	
	open C03;
	loop
	fetch C03 into
		vl_lancamento_antec_w,
		ds_observacao_w;
	exit when C03%notfound;
		begin
		if	(nvl(vl_lancamento_antec_w,0) > 0) then
			update	pls_vendedor_lanc
			set	nr_seq_repasse	= nr_seq_repasse_p,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p,
				dt_lancamento	= dt_referencia_p
			where	nr_seq_vendedor	= nr_seq_vendedor_p
			and	nr_seq_repasse is null
			and	nr_seq_comissao is null
			and	ie_origem_lancamento <> 'M';
			
			insert into pls_repasse_lanc
				       (	nr_sequencia, nr_seq_repasse, vl_lancamento, vl_lanc_aplicado, 
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_lancamento,
						nr_seq_regra, nr_seq_lancamento_vend, ds_observacao )
				values (	pls_repasse_lanc_seq.nextVal, nr_seq_repasse_p, vl_lancamento_antec_w, vl_lancamento_antec_w,
						sysdate, nm_usuario_p, sysdate, nm_usuario_p, '2', -- ie_tipo_lancamento = 2 = Lan�amento adicional referente ao valor de repasse excedente
						null, null, ds_observacao_w ); 
			
			vl_repasse_w := vl_repasse_w + vl_lancamento_antec_w;
		end if;
		end;
	end loop;
	close C03;
	
	open C04;
	loop
	fetch C04 into
		vl_lancamento_antec_w,
		ds_observacao_w;
	exit when C04%notfound;
		begin
		if	(nvl(vl_lancamento_antec_w,0) <> 0) then
			update	pls_vendedor_lanc
			set	nr_seq_repasse	= nr_seq_repasse_p,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_seq_vendedor	= nr_seq_vendedor_p
			and	nr_seq_repasse is null
			and	nr_seq_comissao is null
			and	trunc(dt_lancamento,'month') = trunc(dt_referencia_p, 'month')
			and	ie_origem_lancamento = 'M';
			
			insert into pls_repasse_lanc
				       (	nr_sequencia, nr_seq_repasse, vl_lancamento, vl_lanc_aplicado, 
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_lancamento,
						nr_seq_regra, nr_seq_lancamento_vend, ds_observacao )
				values (	pls_repasse_lanc_seq.nextVal, nr_seq_repasse_p, vl_lancamento_antec_w, vl_lancamento_antec_w,
						sysdate, nm_usuario_p, sysdate, nm_usuario_p, '4', -- ie_tipo_lancamento = 4 = Lan�amento Manual
						null, null, ds_observacao_w ); 
			
			vl_repasse_w := vl_repasse_w + vl_lancamento_antec_w;
		end if;
		end;
	end loop;
	close C04;
	
	if	(ie_gerar_desconto_tit_w = 'S') then
		open C01;
		loop
		fetch C01 into
			nr_titulo_w,
			ie_situacao_w,
			vl_titulo_w;
		exit when C01%notfound;
			begin
			
			if	(ie_repasse_sem_liq_tit_w = 'S' or ie_situacao_w = '2') then
				if	(ie_tipo_desconto_tit_w in ('A','B')) then
					open C02;
					loop
					fetch C02 into
						vl_descontos_w;
					exit when C02%notfound;
						begin
						if	(nvl(vl_descontos_w,0) > 0) then
							vl_descontos_aplic_w := vl_descontos_w;
							
							if	(ie_valor_percent_desconto_w = 'P') then
								select	sum(vl_item_comissao),
									sum(vl_repasse)
								into	vl_item_comissao_w,
									vl_repasse_tit_w
								from	pls_repasse_mens	a,
									titulo_receber		b
								where	nr_seq_repasse = nr_seq_repasse_p
								and	a.nr_seq_mensalidade = b.nr_seq_mensalidade
								and	b.nr_titulo = nr_titulo_w;
								
								vl_descontos_aplic_w := (vl_descontos_w * vl_repasse_tit_w) / vl_titulo_w;
							end if;
							
							insert into pls_repasse_lanc
								       (	nr_sequencia, nr_seq_repasse, vl_lancamento, vl_lanc_aplicado,
										dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_lancamento,
										nr_seq_regra, nr_seq_lancamento_vend, nr_titulo )
								values (	pls_repasse_lanc_seq.nextVal, nr_seq_repasse_p, (vl_descontos_w * -1), (vl_descontos_aplic_w * -1),
										sysdate, nm_usuario_p, sysdate, nm_usuario_p, '3', -- ie_tipo_lancamento = 3 = Desconto referente ao desconto concedido ao t�tulo a receber
										null, null, nr_titulo_w);
							
							vl_repasse_w := vl_repasse_w - vl_descontos_w;
						end if;
						end;
					end loop;
					close C02;
				end if;
				
				if	(ie_tipo_desconto_tit_w in ('A','V')) then
					open C05;
					loop
					fetch C05 into
						vl_alteracao_w;
					exit when C05%notfound;
						begin
						if	(nvl(vl_alteracao_w,0) > 0) then
							vl_alteracao_aplic_w := vl_alteracao_w;
							
							if	(ie_valor_percent_desconto_w = 'P') then
								select	sum(vl_item_comissao),
									sum(vl_repasse)
								into	vl_item_comissao_w,
									vl_repasse_tit_w
								from	pls_repasse_mens	a,
									titulo_receber		b
								where	nr_seq_repasse = nr_seq_repasse_p
								and	a.nr_seq_mensalidade = b.nr_seq_mensalidade
								and	b.nr_titulo = nr_titulo_w;
								
								vl_alteracao_aplic_w := (vl_alteracao_w * vl_repasse_tit_w) / vl_titulo_w;
							end if;
							
							insert into pls_repasse_lanc
								       (	nr_sequencia, nr_seq_repasse, vl_lancamento, vl_lanc_aplicado,
										dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_lancamento,
										nr_seq_regra, nr_seq_lancamento_vend, nr_titulo )
								values (	pls_repasse_lanc_seq.nextVal, nr_seq_repasse_p,(vl_alteracao_w * -1), (vl_alteracao_aplic_w * -1),
										sysdate, nm_usuario_p, sysdate, nm_usuario_p, '3', -- ie_tipo_lancamento = 3 = Desconto referente ao desconto concedido ao t�tulo a receber
										null, null, nr_titulo_w);
							
							vl_repasse_w := vl_repasse_w - vl_alteracao_w;
						end if;
						end;
					end loop;
					close C05;
				end if;
			end if;
			end;
		end loop;
		close C01;
	end if;
	
	dt_referencia_w	:= nvl(dt_referencia_p, sysdate);
	
	select	max(nr_sequencia)
	into	nr_seq_regra_vl_max_w
	from	pls_regra_vend_valor_max
	where	nr_seq_vendedor = nr_seq_vendedor_p
	and	dt_referencia_w between nvl(dt_inicio_vigencia, dt_referencia_w) and nvl(dt_fim_vigencia, dt_referencia_w);
	
	if	(nr_seq_regra_vl_max_w is not null) then
		select	vl_maximo,
			ie_cobranca_futura
		into	vl_maximo_w,
			ie_cobranca_futura_w
		from	pls_regra_vend_valor_max
		where	nr_sequencia = nr_seq_regra_vl_max_w;
		
		if	(vl_repasse_w > vl_maximo_w) then
			
			vl_lancamento_w	:= vl_maximo_w - vl_repasse_w;
			
			select	pls_repasse_lanc_seq.NextVal
			into	nr_seq_repasse_lanc_w
			from	dual;
			
			insert into pls_repasse_lanc
				       (	nr_sequencia, nr_seq_repasse, vl_lancamento, vl_lanc_aplicado,
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_lancamento,
						nr_seq_regra, nr_seq_lancamento_vend )
				values (	nr_seq_repasse_lanc_w, nr_seq_repasse_p, vl_lancamento_w, vl_lancamento_w,
						sysdate, nm_usuario_p, sysdate, nm_usuario_p, '1', -- ie_tipo_lancamento = 1 = Desconto para gera��o do valor m�ximo para o canal de venda
						nr_seq_regra_vl_max_w, null ); 
				
			if	(nvl(ie_cobranca_futura_w,'N') = 'S') then
				vl_lancamento_w	:= vl_lancamento_w * -1;
				insert into pls_vendedor_lanc
					       (	nr_sequencia, nr_seq_vendedor, nr_seq_repasse, 
							dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
							nr_seq_repasse_lanc, vl_lancamento, ie_origem_lancamento )
					values (	pls_vendedor_lanc_seq.NextVal, nr_seq_vendedor_p, null,
							sysdate, nm_usuario_p, sysdate, nm_usuario_p,
							nr_seq_repasse_lanc_w, vl_lancamento_w, 'R' ); 
			end if;
		
		end if;
	end if;
elsif	(ie_opcao_p = 'D') then
	select	max(b.nr_seq_repasse),
		max(b.nr_seq_comissao)
	into	nr_seq_repasse_cobrado_w,
		nr_seq_comissao_cobrada_w
	from	pls_repasse_lanc	a,
		pls_vendedor_lanc	b
	where	a.nr_sequencia		= b.nr_seq_repasse_lanc
	and	a.nr_seq_repasse	= nr_seq_repasse_p
	and	(b.nr_seq_repasse is not null or
		 b.nr_seq_comissao is not null);
	
	if	(nr_seq_repasse_cobrado_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(223993, 'NR_SEQ_REPASSE_COBRADO=' || nr_seq_repasse_cobrado_w || ';NR_SEQ_REPASSE=' || nr_seq_repasse_p);
		/* Mensagem: Lan�amentos adicionais originados deste repasse j� foram cobrados no repasse NR_SEQ_REPASSE_COBRADO! Para desfazer o repasse NR_SEQ_REPASSE � necess�rio primeiro desfazer o repasse NR_SEQ_REPASSE_COBRADO. */ 
	elsif	(nr_seq_comissao_cobrada_w is not null) then
		
		select	nr_seq_lote
		into	nr_seq_lote_cobrado_w
		from	pls_comissao
		where	nr_sequencia = nr_seq_comissao_cobrada_w;
		
		wheb_mensagem_pck.exibir_mensagem_abort(224114, 'NR_SEQ_COMISSAO_COBRADA=' || nr_seq_comissao_cobrada_w || ';NR_SEQ_REPASSE=' || nr_seq_repasse_p || ';NR_SEQ_LOTE_COBRADO=' || nr_seq_lote_cobrado_w);
		/* Mensagem: Lan�amentos adicionais originados deste repasse j� foram cobrados na comiss�o NR_SEQ_COMISSAO_COBRADA! Para desfazer o repasse NR_SEQ_REPASSE � necess�rio primeiro desfazer o lote de comiss�es NR_SEQ_LOTE_COBRADO. */ 
	else
		update	pls_vendedor_lanc
		set	nr_seq_repasse	= null,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_seq_repasse	= nr_seq_repasse_p;
		
		delete	from	pls_vendedor_lanc	a
		where	exists(	select	1
				from	pls_repasse_lanc	x
				where	x.nr_sequencia	 = a.nr_seq_repasse_lanc
				and	x.nr_seq_repasse = nr_seq_repasse_p);
		
		delete	from	pls_repasse_lanc
		where	nr_seq_repasse	= nr_seq_repasse_p;
	end if;
end if;

end pls_gerar_desf_vend_lanc_adic;
/