create or replace
procedure  alterar_motivo_falta_preco(
				cd_motivo_falta_preco_p		varchar2,
				ie_opcao_pasta_p			varchar2,
				nr_sequencia_p			number) is

begin


if(ie_opcao_pasta_p = '0') then
update	cot_compra_forn_item
set	cd_motivo_falta_preco = cd_motivo_falta_preco_p
where	nr_item_cot_compra = nr_sequencia_p;
end if;


if(ie_opcao_pasta_p = '1') then
update	cot_compra_forn_item
set	cd_motivo_falta_preco = cd_motivo_falta_preco_p
where	nr_sequencia = nr_sequencia_p;
end if;

commit;

end alterar_motivo_falta_preco;
/