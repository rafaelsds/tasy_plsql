create or replace
procedure GERAR_HIST_REP_INTEGRACAO
			(nm_usuario_p		in	varchar2,
			ds_erro_p		out	varchar2) is

qt_registro_w	number(10);
ds_erro_w	varchar2(4000);

dt_historico_w		date;
ds_historico_w		w_repasse_terceiro_hist.ds_historico%type;
nr_repasse_terceiro_w	w_repasse_terceiro_hist.nr_repasse_terceiro%type;

cursor	c01 is
select	a.dt_historico,
	a.ds_historico,
	a.nr_repasse_terceiro
from	w_repasse_terceiro_hist a
where	(a.nm_usuario = nm_usuario_p or nm_usuario_p is null);

begin

ds_erro_w	:= null;

open	c01;
loop
fetch	c01 into
	dt_historico_w,
	ds_historico_w,
	nr_repasse_terceiro_w;
exit	when c01%notfound;

	select	count(*)
	into	qt_registro_w
	from	repasse_terceiro a
	where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w;

	if	(nvl(qt_registro_w,0)	= 0) then

		if	(ds_erro_w	is null) or
			(length(ds_erro_w) < 3900) then

			if	(ds_erro_w is not null) then

				ds_erro_w	:= ds_erro_w || chr(13) || chr(10);

			end if;

			ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(281511) || nr_repasse_terceiro_w || WHEB_MENSAGEM_PCK.get_texto(281512);

		end if;

	else

		insert	into repasse_terceiro_hist
			(ds_historico,
			dt_atualizacao,
			dt_historico,
			nm_usuario,
			nr_repasse_terceiro,
			nr_sequencia)
		values	(ds_historico_w,
			sysdate,
			nvl(dt_historico_w,sysdate),
			nvl(nm_usuario_p,'Tasy'),
			nr_repasse_terceiro_w,
			repasse_terceiro_hist_seq.nextval);

	end if;

end	loop;
close	c01;

delete	from w_repasse_terceiro_hist
where	(nm_usuario = nm_usuario_p or nm_usuario_p is null);

ds_erro_p	:= ds_erro_w;

commit;

end GERAR_HIST_REP_INTEGRACAO;
/