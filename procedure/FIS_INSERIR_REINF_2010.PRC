create or replace
procedure fis_inserir_reinf_2010 (nr_sequencia_p    number,
                                  nr_seq_superior_p number,
		                  nm_usuario_p	    varchar2,
		                  nota_inserida_p   out varchar2) is


cd_tributo_w			fis_reinf_r2010.cd_tributo%TYPE;
cd_estabelecimento_ww	fis_reinf_r2010.cd_estabelecimento%TYPE;
cd_estabelecimento_w    nota_fiscal.cd_estabelecimento%type;
cd_serie_nf_w			nota_fiscal.cd_serie_nf%type;	
nr_nota_fiscal_w		nota_fiscal.nr_nota_fiscal%type;
cd_cnpj_w				nota_fiscal.cd_cgc_emitente%type;
cd_ind_obra_w			nota_fiscal.cd_ind_obra%type;
vl_bruto_nf_w			nota_fiscal.vl_mercadoria%type;
vl_base_trib_inss_w		nota_fiscal_trib.vl_base_calculo%type;
vl_retencao_inss_w		nota_fiscal_trib.vl_tributo%type;
vl_n_retencao_inss_w	nota_fiscal_trib.vl_tributo%TYPE;
vl_ret_subcontratado_w	nota_fiscal_trib.vl_ret_subcontratado%type;
cd_cno_w				nota_fiscal.cd_cno%type;
ds_erro_w				varchar2(1000);

begin	
nota_inserida_p := 'N';

select	max(a.cd_tributo) cd_tributo,
	max(cd_estabelecimento)
into	cd_tributo_w,
		cd_estabelecimento_ww
from	fis_reinf_r2010 a
where 	a.nr_sequencia = nr_seq_superior_p;

begin	
	select  max(a.cd_estabelecimento) cd_estab,  
			max(obter_dados_nota_fiscal(a.nr_sequencia,'1')) cd_serie_nota,
			max(substr(a.nr_nota_fiscal,1,80)) nr_nota_fiscal,
			max(a.cd_cgc_emitente) cd_cnpj,
			max(nvl(a.cd_ind_obra, 0)) cd_ind_obra,
			max(obter_dados_nota_fiscal(a.nr_sequencia,'46')) vl_bruto_nf,
			sum(b.vl_base_calculo) vl_base_trib_inss,
			sum(decode(obter_se_possui_processo(a.nr_sequencia,nr_sequencia_p,'2010'),'S',b.vl_tributo,0)) vl_n_retencao_inss,
			sum(b.vl_base_calculo * decode(obter_contribuinte_receita(a.cd_cgc_emitente),0, 0.11, 0.035)) vl_retencao_inss,
			sum(nvl(b.vl_ret_subcontratado,0)) vl_ret_subcontratado,
			max(a.cd_cno) cd_cno
    into	cd_estabelecimento_w,
			cd_serie_nf_w,
			nr_nota_fiscal_w,		
			cd_cnpj_w,
			cd_ind_obra_w,
			vl_bruto_nf_w,	        		
			vl_base_trib_inss_w,
			vl_n_retencao_inss_w,
			vl_retencao_inss_w,
			vl_ret_subcontratado_w,
			cd_cno_w
	from 	nota_fiscal a,
			nota_fiscal_trib b,
			tributo c
	where	a.nr_sequencia = b.nr_sequencia
	and		b.cd_tributo = c.cd_tributo
	and		a.ie_tipo_nota in ('EF', 'EN', 'EP') /*Dom�nio 1661*/
	and     a.dt_atualizacao_estoque is not null /*notas calculadas*/
	and     a.ie_situacao in (1)
	and		b.vl_tributo <> 0	
	and     a.cd_cgc_emitente is not null
	and  	c.cd_tributo = cd_tributo_w
	and		a.cd_estabelecimento = cd_estabelecimento_ww
	and     a.nr_sequencia = nr_sequencia_p
	group by a.nr_sequencia,
			obter_dados_nota_fiscal(a.nr_sequencia,'1')
	union
	select	max(a.cd_estabelecimento) cd_estab,  
			max(obter_dados_nota_fiscal(a.nr_sequencia,'1')) cd_serie_nota,
			max(substr(a.nr_nota_fiscal,1,80)) nr_nota_fiscal,
			max(a.cd_cgc_emitente) cd_cnpj,
			max(nvl(a.cd_ind_obra, 0)) cd_ind_obra,
			max(obter_dados_nota_fiscal(a.nr_sequencia,'46')) vl_bruto_nf,
			sum(b.vl_base_calculo) vl_base_trib_inss,
			sum(decode(obter_se_possui_processo(a.nr_sequencia,nr_sequencia_p,'2010'),'S',b.vl_tributo,0)) vl_n_retencao_inss,
			sum(b.vl_base_calculo * decode(obter_contribuinte_receita(a.cd_cgc_emitente),0, 0.11, 0.035)) vl_retencao_inss,
			sum(nvl(b.vl_ret_subcontratado,0)) vl_ret_subcontratado,
			max(a.cd_cno) cd_cno
	from 	nota_fiscal a,
			nota_fiscal_item_trib b,
			tributo c
	where	a.nr_sequencia = b.nr_sequencia
	and		b.cd_tributo = c.cd_tributo
	and		a.ie_tipo_nota in ('EF', 'EN', 'EP') /*Dom�nio 1661*/
	and     a.dt_atualizacao_estoque is not null /*notas calculadas*/
	and     a.ie_situacao in (1)
	and		b.vl_tributo <> 0	
	and     a.cd_cgc_emitente is not null
	and		c.cd_tributo = cd_tributo_w
	and		a.cd_estabelecimento = cd_estabelecimento_ww
	and     a.nr_sequencia = nr_sequencia_p
	group by a.nr_sequencia,
		 obter_dados_nota_fiscal(a.nr_sequencia,'1');
exception
		when others then 
			ds_erro_w	:= sqlerrm(sqlcode);
			/*Ocorreu algum erro ao inserir as informa��es;*/
			wheb_mensagem_pck.exibir_mensagem_abort(957236,'DS_ERRO_W='||ds_erro_w);
	end;		 

if  (cd_serie_nf_w is not null)	then
    nota_inserida_p := 'S';
	insert into fis_reinf_notas_r2010 (
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		vl_bruto_nf,
		vl_base_trib_inss,
		vl_retencao_inss,
		nr_documento,
		nr_seq_superior,
		cd_cnpj,
		nr_seq_nota,
		vl_n_retencao_inss,
		vl_ret_subcontratado,
		cd_ind_obra,
		cd_cno)
	values(	fis_reinf_notas_r2010_seq.nextval,
		cd_estabelecimento_w,
		sysdate,
		nm_usuario_p,
		vl_bruto_nf_w,
		vl_base_trib_inss_w,
		vl_retencao_inss_w,
		nr_nota_fiscal_w,
		nr_seq_superior_p, 
		cd_cnpj_w,
		nr_sequencia_p,
		vl_n_retencao_inss_w,
		vl_ret_subcontratado_w,
		cd_ind_obra_w,
		cd_cno_w); 		
end if;		
commit;				
end fis_inserir_reinf_2010;
/
