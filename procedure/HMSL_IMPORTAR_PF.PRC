create or replace
procedure hmsl_importar_pf is

/* Estrutura cliente */

nm_pessoa__w	varchar2(255);
dt_nascime_w	varchar2(255);
cd_naciona_w	varchar2(255);
ie_sexo_w	varchar2(255);
nr_cpf_w	varchar2(255);
nr_identid_w	varchar2(255);
ds_orgao_e_w	varchar2(255);
dt_emissao_w	varchar2(255);
nr_telefon_w	varchar2(255);
ie_estado__w	varchar2(255);
ie_funcion_w	varchar2(255);
nr_seq_cor_w	varchar2(255);
ie_grau_in_w	varchar2(255);
qt_altura__w	varchar2(255);
nr_titulo__w	varchar2(255);
nr_zona_w	varchar2(255);
nr_secao_w	varchar2(255);
nr_pis_pas_w	varchar2(255);
nr_cnh_w	varchar2(255);
nr_cert_mi_w	varchar2(255);
dt_admissa_w	varchar2(255);
cd_cargo_w	varchar2(255);
cd_sistema_w	varchar2(255);
nr_seq_con_w	varchar2(255);
ds_codigo__w	varchar2(255);
nr_ctps_w	varchar2(255);
nr_serie_c_w	varchar2(255);
ie_tipo_co_w	varchar2(255);
cd_cep_w	varchar2(255);
ds_enderec_w	varchar2(255);
nr_enderec_w	varchar2(255);
ds_bairro_w	varchar2(255);
ds_complem_w	varchar2(255);
cd_tipo_lo_w	varchar2(255);
ds_municip_w	varchar2(255);
sg_estado_w	varchar2(255);
nr_seq_pai_w	varchar2(255);
nr_ddd_tel_w	varchar2(255);
nr_telefo2_w	varchar2(255);
ds_email_w	varchar2(255);
nr_ddd_celular_w varchar2(3);

/* Internos */
cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(5);
ie_erro_compl_pf_w	varchar2(1)	:= 'N';
ie_erro_pf_w		varchar2(1)	:= 'N';
qt_registro_w		number(10);
trigger_name_w		varchar2(255);
sql_errm_w		varchar2(2000);
qt_inserido_w		number(15,0);

qt_teste_w		number(15,0);

Cursor c01 is
select	nm_pessoa_,
	dt_nascime,
	cd_naciona,
	ie_sexo,
	nr_cpf,
	nr_identid,
	ds_orgao_e,
	dt_emissao,
	nr_telefon,
	ie_estado_,
	ie_funcion,
	nr_seq_cor,
	ie_grau_in,
	qt_altura_,
	nr_titulo_,
	nr_zona,
	nr_secao,
	nr_pis_pas,
	nr_cnh,
	nr_cert_mi,
	dt_admissa,
	cd_cargo,
	cd_sistema,
	nr_seq_con,
	ds_codigo_,
	nr_ctps,
	nr_serie_c,
	ie_tipo_co,
	cd_cep,
	ds_enderec,
	nr_enderec,
	ds_bairro,
	ds_complem,
	cd_tipo_lo,
	ds_municip,
	sg_estado,
	nr_seq_pai,
	nr_ddd_tel,
	nr_telefo2,
	ds_email
from	hmsl_imp_pf
order by 1;

begin

wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('N');

begin

qt_inserido_w	:= 0;

open c01;
loop
fetch c01 into
	nm_pessoa__w,
	dt_nascime_w,
	cd_naciona_w,
	ie_sexo_w,
	nr_cpf_w,
	nr_identid_w,
	ds_orgao_e_w,
	dt_emissao_w,
	nr_telefon_w,
	ie_estado__w,
	ie_funcion_w,
	nr_seq_cor_w,
	ie_grau_in_w,
	qt_altura__w,
	nr_titulo__w,
	nr_zona_w,
	nr_secao_w,
	nr_pis_pas_w,
	nr_cnh_w,
	nr_cert_mi_w,
	dt_admissa_w,
	cd_cargo_w,
	cd_sistema_w,
	nr_seq_con_w,
	ds_codigo__w,
	nr_ctps_w,
	nr_serie_c_w,
	ie_tipo_co_w,
	cd_cep_w,
	ds_enderec_w,
	nr_enderec_w,
	ds_bairro_w,
	ds_complem_w,
	cd_tipo_lo_w,
	ds_municip_w,
	sg_estado_w,
	nr_seq_pai_w,
	nr_ddd_tel_w,
	nr_telefo2_w,
	ds_email_w;
exit when c01%notfound;

	ie_erro_pf_w		:= 'N';
	ie_erro_compl_pf_w	:= 'N';
	qt_inserido_w		:= qt_inserido_w + 1;

	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;


	if 	(nr_cpf_w is not null) then
		
		select	count(*) 
		into	qt_registro_w
		from	pessoa_fisica
		where	nr_cpf = nr_cpf_w;
	
		if	(qt_registro_w > 0) then
			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55715,
				'OS239156_PF',
				sysdate,
				'Erro ao inserir pessoa. ' 	|| chr(13) || chr(10) ||
				'Nome= ' || nm_pessoa__w 	|| chr(13) || chr(10) ||
				'Erro= J� existe uma pessoa f�sica cadastrada com o CPF ' || nr_cpf_w);
		
			ie_erro_pf_w	:= 'S';
		end if;
	end if;


	if	(ie_erro_pf_w = 'N') then	
		begin
		
		select	substr(nr_telefon_w,5,2),
			substr(nr_telefon_w,8,15)
		into	nr_ddd_celular_w,
			nr_telefon_w
		from dual;

		
		insert into pessoa_fisica
			(cd_pessoa_fisica,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_tipo_pessoa,
			nm_pessoa_fisica,
			dt_nascimento,
			cd_nacionalidade,
			ie_sexo,
			nr_cpf,
			nr_identidade,
			ds_orgao_emissor_ci,
			dt_emissao_ci,
			nr_telefone_celular,
			ie_estado_civil,
			ie_funcionario,
			nr_seq_cor_pele,
			ie_grau_instrucao,
			qt_altura_cm,
			nr_titulo_eleitor,
			nr_zona,
			nr_secao,
			nr_pis_pasep,
			nr_cnh,
			nr_cert_militar,
			dt_admissao_hosp,
			cd_cargo,
			cd_sistema_ant,
			nr_seq_conselho,
			ds_codigo_prof,
			nr_ctps,
			nr_serie_ctps,
			nr_ddd_celular)
		values	(cd_pessoa_fisica_w,
			'OS239156_PF',
			sysdate,
			'OS239156_PF',
			sysdate,
			1,
			substr(nm_pessoa__w,1,40),
			to_Date(dt_nascime_w,'dd-mm-yyyy'),
			substr(cd_naciona_w,1,8),
			substr(ie_sexo_w,1,1),
			substr(nr_cpf_w,1,11),
			substr(nr_identid_w,1,15),
			substr(ds_orgao_e_w,1,10),
			to_date(dt_emissao_w,'dd-mm-yyyy'),
			substr(nr_telefon_w,1,40),
			substr(ie_estado__w,1,2),
			substr(ie_funcion_w,1,1),
			substr(nr_seq_cor_w,1,10),
			substr(ie_grau_in_w,1,2),
			rpad(somente_numero(substr(qt_altura__w,1,4)),3,0),
			substr(nr_titulo__w,1,20),
			substr(nr_zona_w,1,5),
			substr(nr_secao_w,1,15),
			substr(nr_pis_pas_w,1,11),
			substr(nr_cnh_w,1,255),
			substr(nr_cert_mi_w,1,255),
			to_date(dt_admissa_w,'dd-mm-yyyy'),
			substr(cd_cargo_w,1,10),
			substr(cd_sistema_w,1,20),
			substr(nr_seq_con_w,1,10),
			substr(ds_codigo__w,1,15),
			substr(nr_ctps_w,1,7),
			substr(nr_serie_c_w,1,5),
			nr_ddd_celular_w);

		exception
			when others then
			sql_errm_w	:= sqlerrm;
			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55715,
				'OS239156_PF',
				sysdate,
				'Erro ao inserir pessoa. ' 	|| chr(13) || chr(10) ||
				'Nome= ' || nm_pessoa__w 	|| chr(13) || chr(10) ||
				'Erro= ' || sql_errm_w);

			ie_erro_pf_w	:= 'S';
		end;

		if	(ie_erro_pf_w = 'N') then

			/* Residencial - sempre insere */

			if	(ie_erro_compl_pf_w = 'N') then

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					ie_tipo_complemento,
					cd_cep,
					ds_endereco,
					nr_endereco,
					ds_bairro,
					ds_complemento,
					cd_tipo_logradouro,
					ds_municipio,
					sg_estado,
					nr_seq_pais,
					nr_ddd_telefone,
					nr_telefone,
					ds_email)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					'OS239156_PF',
					sysdate,
					'OS239156_PF',
					sysdate,
					substr(ie_tipo_co_w,1,2),
					substr(cd_cep_w,1,15),
					substr(ds_enderec_w,1,40),
					substr(nr_enderec_w,1,5),
					substr(ds_bairro_w,1,40),
					substr(ds_complem_w,1,40),
					substr(cd_tipo_lo_w,1,3),
					substr(ds_municip_w,1,40),
					substr(sg_estado_w,1,2),
					substr(nr_seq_pai_w,1,10),
					substr(nr_ddd_tel_w,1,3),
					substr(nr_telefo2_w,1,15),
					substr(ds_email_w,1,255));
			end if;
		end if;

		if	(qt_inserido_w	= 3000) then
			commit;
			qt_inserido_w	:= 0;
		end if;

	end if;

end loop;
close c01;

commit;

exception
	when others then
	wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('S');

	raise_application_error(-20011,'Erro: ' || sqlerrm || chr(13) ||  chr(10) ||
	' Pessoa = ' || nm_pessoa__w);	
	
	
end;
commit;
wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('S');

end hmsl_importar_pf;
/
