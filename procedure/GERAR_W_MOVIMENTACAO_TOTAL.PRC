create or replace
procedure gerar_w_movimentacao_total( cd_setor_destino_p number default 0,
				     cd_setor_origem_p	number default 0) is 

qt_total_w		varchar(255);
cc_uti_w		number(10);
uti_cc_w		number(10);
qt_total_cirurgias_w	number(10);
ds_total_cirurgias_w	varchar(255);
perc_cc_uti_w		varchar(255);
perc_uti_cc_w		varchar(255);
qt_uti_cc_w		varchar(255);
qt_cc_uti_w		varchar(255);
			     			     
begin

delete	relat_movimentacao_total;
commit;


select 	count(*) 
into	qt_total_w
from    w_relat_movimentacao_pac a
WHERE	((a.cd_setor_origem  = cd_setor_origem_p) or (nvl(cd_setor_origem_p,0) = 0))
and	((a.CD_SETOR_DESTINO = cd_setor_destino_p) or (nvl(cd_setor_destino_p,0) = 0));


select 	count(*) 
into	cc_uti_w
from   	w_relat_movimentacao_pac a
where  	obter_classif_setor(a.cd_setor_origem)  = 2
and    	obter_classif_setor(a.cd_setor_destino) = 4
and	((a.cd_setor_origem  = cd_setor_origem_p) or (nvl(cd_setor_origem_p,0) = 0))
and	((a.CD_SETOR_DESTINO = cd_setor_destino_p)  or (nvl(cd_setor_destino_p,0) = 0));

select 	count(*) 
into	uti_cc_w
from   	w_relat_movimentacao_pac a
where  	obter_classif_setor(a.cd_setor_origem)  = 4
and    	obter_classif_setor(a.cd_setor_destino) = 2
and	((a.cd_setor_origem  = cd_setor_origem_p) or (nvl(cd_setor_origem_p,0) = 0))
and	((a.CD_SETOR_DESTINO = cd_setor_destino_p)  or (nvl(cd_setor_destino_p,0) = 0));


select 	max(x.qt_total_cirurgia)
into	qt_total_cirurgias_w
from 	w_relat_movimentacao_pac x;

select	 (wheb_mensagem_pck.get_texto(802299) || ' -> ' || wheb_mensagem_pck.get_texto(802300) || ': ' || nvl(round(cc_uti_w * 100 / qt_total_cirurgias_w,2),0))
into	 perc_cc_uti_w
from	 dual;

select	(wheb_mensagem_pck.get_texto(802300) || ' -> ' || wheb_mensagem_pck.get_texto(802299) || ': ' || nvl(round(uti_cc_w * 100 / qt_total_cirurgias_w,2),0))
into	perc_uti_cc_w
from	dual;

select  (wheb_mensagem_pck.get_texto(802301) || ' ' || qt_total_w) 
into	qt_total_w
from 	dual;

select	wheb_mensagem_pck.get_texto(802302) || ' (' || wheb_mensagem_pck.get_texto(802299) || ' -> ' || wheb_mensagem_pck.get_texto(802300) || '): ' || cc_uti_w
into	qt_cc_uti_w
from 	dual;

select	wheb_mensagem_pck.get_texto(802302) || ' (' || wheb_mensagem_pck.get_texto(802300) || ' -> ' || wheb_mensagem_pck.get_texto(802299) || '): ' || uti_cc_w
into	qt_uti_cc_w
from	dual;

select 	wheb_mensagem_pck.get_texto(802303, 'QT_CIRURGIAS='||qt_total_cirurgias_w)
into	ds_total_cirurgias_w
from 	dual;

insert into  relat_movimentacao_total
	(ds_total_cirurgia,
	ds_perc_cc_uti,
	ds_perc_uti_cc,
	ds_total,
	ds_cc_uti,
	ds_uti_cc,
	nr_sequencia,
	dt_atualizacao,
	nm_usuario) 
	values (
	ds_total_cirurgias_w,
	perc_cc_uti_w,
	perc_uti_cc_w,
	qt_total_w,
	qt_cc_uti_w,
	qt_uti_cc_w,
	w_relat_movimentacao_pac_seq.nextval,
	sysdate,
	Obter_Usuario_Ativo);


commit;

end gerar_w_movimentacao_total;
/ 
