create or replace
procedure gerar_servicos_pac_js(
			nr_atendimento_p	number,
			ds_lista_servicos_p	varchar2,
			nm_usuario_p		Varchar2,
			lancar_conta_p		varchar2,
			cd_estabelecimento_p	number) is 

begin

if	(nvl(nr_atendimento_p,0) > 0) then

gerar_servicos_paciente_atend(nr_atendimento_p,ds_lista_servicos_p,nm_usuario_p,lancar_conta_p,cd_estabelecimento_p);

gerar_evento_servicopac(nr_atendimento_p,nm_usuario_p);

end if;

commit;

end gerar_servicos_pac_js;
/