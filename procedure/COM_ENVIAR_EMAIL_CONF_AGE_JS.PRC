create or replace
procedure com_enviar_email_conf_age_js(
					nm_usuario_p		Varchar2,
					nr_seq_regra_p		number,
					nr_seq_ordem_servico_p	number,
					nr_sequencia_solic_p	number) is 

ds_titulo_w		varchar2(255);	
ds_historico_w  	varchar2(4000);	
ds_email_origem_w	varchar2(255);	
ds_email_destino_w	varchar2(2000);
cd_pf_solic_w		varchar2(10);	
ds_email_solic_w	varchar2(255);	
ds_email_dest_w		varchar2(255);
					
Cursor C01 is
	select	substr(obter_dados_usuario_opcao(nm_usuario_exec,'E'),1,255)
	from	man_ordem_servico_exec
	where	nr_seq_ordem = nr_seq_ordem_servico_p
	and	substr(obter_dados_usuario_opcao(nm_usuario_exec,'E'),1,255) is not null;
					
begin

if	(nvl(nr_seq_regra_p,0) > 0) and
	(nvl(nr_seq_ordem_servico_p,0) > 0) then
	
	select	cd_pf_solic    
	into	cd_pf_solic_w
	from	com_solic_sd
	where	nr_sequencia = nr_sequencia_solic_p;
	
	select	substr(max(ds_email),1,255)
	into	ds_email_destino_w
	from	usuario
	where	cd_pessoa_fisica = cd_pf_solic_w;
	
	open C01;
	loop
	fetch C01 into	
		ds_email_dest_w;
	exit when C01%notfound;
		begin
		if	(ds_email_destino_w is not null) then
			ds_email_destino_w := ds_email_destino_w || ',' || ds_email_dest_w;
		else 
			ds_email_destino_w := ds_email_destino_w;
		end if;
		end;
	end loop;
	close C01;
	
	ds_email_origem_w := substr(obter_dados_usuario_opcao(nm_usuario_p,'E'),1,255);

	ds_titulo_w := substr(Wheb_mensagem_pck.get_texto(326614, 'NR_ORDEM='||nr_seq_ordem_servico_p),1,255);
	
	select	ds_historico
	into	ds_historico_w
	from	regra_comunic_conf_age
	where	nr_sequencia = nr_seq_regra_p;

	enviar_email(	ds_titulo_w,
			ds_historico_w,
			ds_email_origem_w,
			ds_email_destino_w,
			nm_usuario_p,
			'A');

end if;
			
commit;

end com_enviar_email_conf_age_js;
/