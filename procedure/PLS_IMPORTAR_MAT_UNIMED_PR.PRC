create or replace
procedure pls_importar_mat_unimed_pr(	cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2,
				ds_versao_tiss_p	varchar2) is 
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar os materiais/medicamentos da federa��o SC
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 

Utilizado apenas em:
	- Fun��o OPS - Cadastro de Materiais, pasta Materiais Unimed >> Federa��o SC >> Materiais
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

/* =================== CAMPOS ================== */
cd_material_w			number(10);
ie_digito_w			number(2);
ds_material_w			varchar2(255);
ds_nome_comercial_w		varchar2(255);
ds_fabricante_w			varchar2(255);
ie_unidade_utilizacao_w		varchar2(3);
cd_grupo_estoque_w		varchar2(5);
ie_moeda_w			varchar2(4);
ie_lista_w			varchar2(1);
vl_produto_w			number(15,2);
cd_simpro_w			number(8);
cd_brasindice_w			varchar2(30);
cd_tiss_w			varchar2(10);
ie_origem_w			varchar2(2);
dt_produto_preco_w		date;
ds_observacao_w			varchar2(500);
nr_seq_header_w			number(15);
ie_tipo_registro_w		varchar2(4);
ie_tipo_produto_w		varchar2(4);
nr_seq_trailer_w		number(15);
vl_soma_cd_mat_w		number(15)	:= 0;
vl_cd_mat_arquivo_w		number(15)	:= 0;
nr_seq_mat_w			number(15);
qt_material_cad_w		number(15);
ie_possui_preco_w		number(10);
ie_produto_w			varchar2(1);
cd_mat_digito_w			number(15);
cd_anvisa_w			varchar2(20);
dt_validade_anvisa_w		date;
qt_embalagem_w			number(10);
ie_unid_embalagem_w		varchar2(4);
qt_utilizacao_w			number(10);
qt_fracao_w			number(10);
ie_unid_fracao_w		varchar2(4);
ie_fracionar_w			varchar2(2);
ie_opme_w			varchar2(1);
cd_grupo_estoque_opme_w		number(5);
ds_nome_comercial_compl_w	varchar2(200);
cd_material_tuss_w		number(10);
ie_uso_restrito_w		varchar2(1);
qt_commit_w			pls_integer := 0;
vl_tnumm_w			number(15,2);
		
cursor c01 is
	select	ds_conteudo
	from	w_import_mat_unimed
	order by
		nr_sequencia;
	
begin
--Valida��o de arquivo selecionado para a Importa��o
begin
select	obter_se_somente_numero(substr(ds_conteudo,11,41))
into	ie_produto_w
from	w_import_mat_unimed
where	substr(ds_conteudo,7,1)	= 'D'
and	rownum = 1;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(282595);
end;

if	(ie_produto_w	= 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(224236);
else
	for r_C01_w in C01 loop	
		qt_commit_w := qt_commit_w + 1;
		
		--DETALHE
		if	(substr(r_C01_w.ds_conteudo,7,1) = 'D') then
			ie_tipo_registro_w	:= 'D';
			ie_tipo_produto_w	:= substr(r_C01_w.ds_conteudo,8,3);
			cd_material_w		:= substr(r_C01_w.ds_conteudo,11,7);
			ie_digito_w		:= substr(r_C01_w.ds_conteudo,18,1);
			ds_material_w		:= trim(substr(r_C01_w.ds_conteudo,19,200));
			ds_nome_comercial_w	:= trim(substr(r_C01_w.ds_conteudo,219,35));
			ds_fabricante_w		:= trim(substr(r_C01_w.ds_conteudo,254,30));	
			ie_unidade_utilizacao_w	:= trim(substr(r_C01_w.ds_conteudo,284,3));
			cd_grupo_estoque_w	:= substr(r_C01_w.ds_conteudo,287,5);
			cd_brasindice_w		:= trim(substr(r_C01_w.ds_conteudo,292,12));
			
			cd_simpro_w		:= ltrim(substr(r_C01_w.ds_conteudo,304,7));
			if	(cd_simpro_w = 0) then
				cd_simpro_w	:= null;	
			end if;
			
			begin
			vl_produto_w		:= to_number(substr(r_C01_w.ds_conteudo,311,10) || ',' || substr(r_C01_w.ds_conteudo,321,2));
			exception
			when others then
				vl_produto_w	:= 0;
			end;
			
			begin
			dt_produto_preco_w	:= to_char(to_date(substr(r_C01_w.ds_conteudo,323,8)),'dd/mm/yyyy');
			exception
				when others then
				dt_produto_preco_w	:= null;
			end;
			
			select	decode(substr(r_C01_w.ds_conteudo,331,3),000,1,1)
			into	ie_moeda_w
			from	dual;
			
			ie_origem_w		:= substr(r_C01_w.ds_conteudo,334,1);
			ds_observacao_w		:= trim(substr(r_C01_w.ds_conteudo,335,500));
			ie_lista_w		:= substr(r_C01_w.ds_conteudo,835,1);
			
			cd_tiss_w		:= lpad(trim(substr(r_C01_w.ds_conteudo,836,10)),10,'0');
			
			if	(cd_tiss_w = '0000000000') then
				cd_tiss_w	:= null;
			end if;	
			
			begin
			vl_tnumm_w		:= to_number(substr(r_C01_w.ds_conteudo,846,10) || ',' || substr(r_C01_w.ds_conteudo,856,2));
			exception
			when others then
				vl_tnumm_w	:= 0;
			end;
			
			cd_anvisa_w			:= trim(substr(r_C01_w.ds_conteudo,858,20));
			
			begin
			dt_validade_anvisa_w		:= to_date(trim(substr(r_C01_w.ds_conteudo,878,8)),'dd/mm/yyyy');
			exception
			when others then
				dt_validade_anvisa_w := null;
			end;			
			
			ie_uso_restrito_w		:= trim(substr(r_C01_w.ds_conteudo,886,1));
			ds_nome_comercial_compl_w	:= trim(substr(r_C01_w.ds_conteudo,887,200));
			cd_material_tuss_w		:= to_number(trim(substr(r_C01_w.ds_conteudo,1087,10)));
			
			vl_soma_cd_mat_w	:= vl_soma_cd_mat_w + cd_material_w;

			cd_mat_digito_w		:= cd_material_w || ie_digito_w;
			
			if	(cd_grupo_estoque_w is not null) then
				cd_grupo_estoque_opme_w := somente_numero(cd_grupo_estoque_w);
			
				select	nvl(max(ie_opme),'N')
				into	ie_opme_w
				from	pls_grupo_est_fed_sc
				where	cd_grupo_estoque	= cd_grupo_estoque_opme_w;
			end if;
			
			if	(ds_versao_tiss_p is not null) then
				select	count(1)
				into	qt_material_cad_w
				from	pls_mat_unimed_fed_sc
				where	cd_material	= cd_mat_digito_w
				and	(ds_versao_tiss	= ds_versao_tiss_p or ds_versao_tiss is null)
				and	rownum		= 1;			
			else
				--verifica se este material j� existe
				select	count(1)
				into	qt_material_cad_w
				from	pls_mat_unimed_fed_sc
				where	cd_material	= cd_mat_digito_w
				and	rownum		= 1;
			end if;
				
			--se existir, faz UPDATE, sen�o, faz INSERT
			if	(qt_material_cad_w = 0) then
					
				select	pls_mat_unimed_fed_sc_seq.nextval
				into	nr_seq_mat_w
				from	dual;
				
				insert into	pls_mat_unimed_fed_sc
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_estabelecimento,
					cd_material,
					ie_digito,
					ds_material,
					ds_nome_comercial,
					ds_fabricante,
					ie_unidade_utilizacao,
					cd_grupo_estoque,
					cd_brasindice,
					cd_simpro,
					ie_moeda,
					ie_origem,
					ds_observacao,
					ie_lista,
					cd_tiss,
					ie_tipo,
					ie_opme,
					cd_anvisa,
					dt_validade_anvisa,
					ie_uso_restrito,
					ds_nome_comercial_compl,
					cd_material_tuss,
					ds_versao_tiss)
				values	(nr_seq_mat_w,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_estabelecimento_p,
					cd_mat_digito_w,
					ie_digito_w,
					ds_material_w,
					ds_nome_comercial_w,
					ds_fabricante_w,
					ie_unidade_utilizacao_w,
					cd_grupo_estoque_w,
					cd_brasindice_w,
					cd_simpro_w,
					ie_moeda_w,
					ie_origem_w,
					ds_observacao_w,
					ie_lista_w,
					cd_tiss_w,
					ie_tipo_produto_w,
					ie_opme_w,
					cd_anvisa_w,
					dt_validade_anvisa_w,
					ie_uso_restrito_w,
					ds_nome_comercial_compl_w,
					cd_material_tuss_w,
					ds_versao_tiss_p);
					
			else	
				begin
				update	pls_mat_unimed_fed_sc
				set	nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					ds_material		= ds_material_w,
					ds_nome_comercial	= ds_nome_comercial_w,
					ds_fabricante		= ds_fabricante_w,
					ie_unidade_utilizacao	= ie_unidade_utilizacao_w,
					cd_grupo_estoque	= cd_grupo_estoque_w,
					cd_brasindice		= cd_brasindice_w,
					cd_simpro		= cd_simpro_w,
					ie_moeda		= ie_moeda_w,
					ie_origem		= ie_origem_w,
					ds_observacao		= ds_observacao_w,
					ie_lista		= ie_lista_w,
					cd_tiss			= cd_tiss_w,
					ie_tipo			= ie_tipo_produto_w,
					cd_anvisa		= cd_anvisa_w,
					dt_validade_anvisa	= dt_validade_anvisa_w,
					ie_unid_fracao		= ie_unid_fracao_w,
					ie_fracionar		= ie_fracionar_w,
					ie_opme			= ie_opme_w,
					ie_uso_restrito		= ie_uso_restrito_w,
					ds_nome_comercial_compl = ds_nome_comercial_compl_w,
					cd_material_tuss	= cd_material_tuss_w,
					ds_versao_tiss		= ds_versao_tiss_p
				where	cd_material		= cd_mat_digito_w;
				exception
				when others then
					wheb_mensagem_pck.exibir_mensagem_abort(328181,	'CD_MATERIAL=' || cd_material_w);
				end;
			
			end if;
			
						
		--TRAILER
		elsif	(substr(r_C01_w.ds_conteudo,7,1) = 'T') then
			null;
			--vl_cd_mat_arquivo_w	:= obter_somente_numero(substr(r_C01_w.ds_conteudo,8,14));
			
			-- Verifica se a somat�ria dos c�digos dos produtos � igual a informada no trailer do arquivo. Se for diferente, exibe a mensagem.	
			--if	(vl_cd_mat_arquivo_w <> vl_soma_cd_mat_w) then
			--	wheb_mensagem_pck.exibir_mensagem_abort(224237);
			--end if;
		end if;	
		
		if	(qt_commit_w = 500) then
			commit;
			qt_commit_w := 0;
		end if;	
	end loop;
end if;

commit;

end pls_importar_mat_unimed_pr;
/