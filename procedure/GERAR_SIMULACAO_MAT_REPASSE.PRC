CREATE OR REPLACE
PROCEDURE GERAR_SIMULACAO_MAT_REPASSE(	nr_sequencia_p		in Number,
					cd_estabelecimento_p	in Number,
					cd_medico_atend_p	in Varchar2,
					nm_usuario_p		in Varchar2,
					cd_edicao_amb_p		in Number,
					cd_convenio_p		in Number,
					ie_tipo_atendimento_p	in Number,
					cd_regra_p		out number,
					nr_seq_criterio_p	out number) IS

cd_tabela_preco_w		Number(04,0);
cd_convenio_w		number(05,0);
cd_categoria_w		Varchar2(255);
cd_pessoa_tratamento_w	Varchar2(255);
dt_conta_w		date;
cd_material_w		number(06,0);
qt_material_w		number(10,0);
cd_regra_w		number(5)	:= 0;
tx_repasse_w		number(15,4);
tx_repasse_terc_w	number(15,4);
vl_repasse_w		number(15,2);
vl_repassar_w		number(15,2);
vl_repasse_Terc_w	number(15,2);
vl_repasse_Terc_ww	number(15,2);
vl_material_w		number(15,2);
vl_tabela_original_w	number(15,2);
vl_tabela_w		Number(15,2);
nr_seq_proc_princ_w	Number(15,0);
nr_seq_regra_item_w	Number(15,0);
cd_medico_prescr_w	varchar2(10);
cd_medico_cir_w		varchar2(10);
cd_medico_w		varchar2(10);
cd_medico_rep_w		varchar2(10);
cd_medico_ww		varchar2(10);
nr_seq_terceiro_w	number(10,0);
nr_cirurgia_w		number(10,0);
cd_conta_contabil_w	varchar2(20);
nr_sequencia_w		number(10);
nr_seq_repasse_w	number(10);
nr_prescricao_w		Number(14,0);
ie_beneficiario_w	Varchar2(01);
ie_forma_calculo_w	mat_criterio_repasse.ie_forma_calculo%type;
nr_seq_proc_pacote_w	Number(10,0);
cd_tipo_acomodacao_w	Number(04,0);
cd_setor_atendimento_w	Number(05,0);
ie_forma_pagto_w		Varchar2(01);
dt_ult_vigencia_w		Date;
ie_origem_preco_w		Varchar2(01);
ie_pacote_w		Varchar2(01);
ie_status_w		Varchar2(01);
ie_perc_saldo_w		Varchar2(01);
nr_seq_trans_fin_w		Number(10,0);
ie_gravacao_medico_w	Varchar2(01);
ie_tipo_convenio_w		Number(15,0);
vl_custo_mat_repasse_w	Number(15,2);
cd_medico_atendimento_w	varchar2(10);
cd_medico_resp_w		varchar2(10);
cd_pessoa_indic_w		Varchar2(10);
cd_cgc_prestador_w	Varchar2(20);
ie_carater_inter_sus_w	varchar2(2);
cd_cgc_fornec_w		Varchar2(14);
cd_municipio_ibge_w	Varchar2(6);
ie_cancelamento_conta_w	Varchar2(1);
ie_atend_retorno_w		Varchar2(100);
cd_medico_prescritor_w	Varchar2(100);
NR_SEQ_TRANS_FIN_REP_MAIOR_w	number(10);
nr_seq_mat_crit_repasse_w	number(10,0);
cd_pessoa_fisica_w	varchar2(255);
ie_repasse_calc_w		varchar2(255);
nr_seq_ret_glosa_w		number(10);
cd_tab_preco_calc_w	number(10,0);
cd_plano_w		varchar2(100);
nr_atendimento_w		number(10,0);
cd_convenio_plano_w	number(05,0);
cont_w			number(10,0);
cd_categoria_plano_w	varchar2(255);
ie_perc_pacote_w		varchar2(100);
nr_interno_conta_rep_w	number(15);
ie_estornar_rep_desdob_w	varchar2(1);
cd_usuario_original_w	varchar2(255);
ie_repasse_audit_w	varchar2(1);
ie_responsavel_credito_w	varchar2(5);
nr_seq_bras_preco_w		number(10,0);
nr_seq_mat_bras_w		number(10,0);
nr_seq_conv_bras_w		number(10,0);
nr_seq_conv_simpro_w		number(10,0);
nr_seq_mat_simpro_w		number(10,0);
nr_seq_simpro_preco_w		number(10,0);
nr_seq_ajuste_mat_w		number(10,0);
nr_seq_classif_atend_w		atendimento_paciente.nr_seq_classificacao%type;

CURSOR C02 IS
select 	ie_beneficiario,
	tx_repasse,
	nr_seq_terceiro,
	cd_conta_contabil,
	nvl(ie_perc_saldo,'S'),
	nvl(ie_gravacao_medico,'S'),
	cd_pessoa_fisica,
	nr_seq_item
from	regra_repasse_terc_item 
where	cd_regra	     	= cd_regra_w
order	by nr_seq_item;

CURSOR	C03 IS
select	vl_preco_venda
from 	preco_material
where	cd_estabelecimento  	= cd_estabelecimento_p
and	cd_tab_preco_mat	= cd_tabela_preco_w
and 	cd_material         	= cd_material_w
and	ie_situacao	     	= 'A'
and 	dt_inicio_vigencia  	<= dt_conta_w
order by 
	dt_inicio_vigencia desc,
	vl_preco_venda desc;

CURSOR	C05 IS
select	nr_sequencia
from	Material_repasse a
where	nr_seq_material	= nr_sequencia_p
and	1 = 2
and	((nvl(ie_estornar_rep_desdob_w,'N') = 'N' and a.nr_seq_origem is null) or
	(nvl(ie_estornar_rep_desdob_w,'N') = 'S' and nvl(a.ie_estorno,'N') = 'N'))
and	not exists
	(select	1
	from 	material_Repasse x
	where 	(nvl(ie_estornar_rep_desdob_w,'N') = 'N' or nvl(x.ie_estorno,'N') = 'S')
	and	x.nr_seq_origem	= a.nr_sequencia);

begin

select	nvl(max(ie_repasse_item_audit),'S')
into	ie_repasse_audit_w
from	parametro_repasse
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(cd_material)
into	cd_material_w
from	material_atend_paciente
where	nr_sequencia	= nr_sequencia_p;

select	count(*)
into	cont_w
from	material
where	cd_material	= cd_material_w;

if	(cont_w > 1) then
	/* O material cd_material_w est� duplicado na base de dados!
	Existem cont_w materias com este c�digo cadastrado.
	N�o � poss�vel gerar o repasse! */
	wheb_mensagem_pck.exibir_mensagem_abort(191621,	'CD_MATERIAL_W=' || cd_material_w ||
							';CONT_W=' || cont_w);
end if;

/*delete from material_repasse a
where	nr_seq_material = nr_sequencia_p
and	nvl(nr_lote_contabil,0) = 0
and	nr_seq_item_retorno	is null
and	nr_seq_origem		is null
and	ie_status		in ('A','S')
and	not exists
	(select 1
	from repasse_terceiro b
	where nvl(a.nr_repasse_terceiro,0)	= b.nr_repasse_terceiro
	  and a.nr_repasse_terceiro is not null
	  and b.ie_status		= 'F')
  and	not exists
	(select 1
	from Material_Repasse c
	where c.nr_seq_origem	= a.nr_sequencia);*/

OPEN C05;
LOOP
FETCH C05 into
	nr_sequencia_w;
exit when c05%notfound;

	select	Material_repasse_seq.nextval
	into	nr_seq_repasse_w
	from	dual;

	/*insert into Material_repasse(
		nr_sequencia,
		nr_seq_Material,
		vl_repasse,
		dt_atualizacao,
		nm_usuario,
		nr_seq_terceiro,
		nr_lote_contabil,
		nr_repasse_terceiro,
		cd_conta_contabil,
		nr_seq_trans_fin,
		vl_liberado,
		nr_seq_item_retorno,
		ie_status,
		nr_seq_origem,
		cd_regra,
		cd_medico,
		NR_SEQ_TRANS_FIN_REP_MAIOR,
		ie_estorno,
		ie_repasse_calc,
		nr_seq_motivo_des)
	select	nr_seq_repasse_w,
		nr_seq_material,
		vl_repasse  * -1,
		sysdate,
		nm_usuario_p,
		nr_seq_terceiro,
		0,
		null,
		cd_conta_contabil,
		nr_seq_trans_fin,
		vl_liberado * -1,
		nr_seq_item_retorno,
		'E',
		nr_sequencia_w,
		cd_regra,
		cd_medico,
		NR_SEQ_TRANS_FIN_REP_MAIOR,
		'S',
		ie_repasse_calc_w,
		nr_seq_motivo_des
	from	material_repasse
	where	nr_sequencia	= nr_sequencia_w;

	select	max(a.nr_interno_conta)
	into	nr_interno_conta_rep_w
	from	material_atend_paciente a
	where	a.nr_Sequencia	= nr_sequencia_p;

	if	(nr_interno_conta_rep_w is not null) then
		gerar_procmat_repasse_nf(nr_interno_conta_rep_w, nm_usuario_p, 'S');
	end if;

	/* ahoffelder - OS 321335 - 10/06/2011 */
	/*update	material_repasse
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_estorno		= 'S'
	where	nr_sequencia		= nr_sequencia_w;*/

END LOOP;
CLOSE C05;
begin
select 	nvl(a.dt_conta, a.dt_atendimento),
	a.cd_material,
	a.nr_prescricao,
	a.nr_cirurgia,
	a.nr_seq_proc_pacote,
	a.vl_material,
	a.cd_setor_atendimento,
	decode(a.nr_seq_proc_pacote,null,'F','I'),
	c.cd_medico_atendimento,
	c.cd_medico_resp,
	c.cd_pessoa_indic,
	nvl(a.cd_cgc_prestador, 'X'),
	c.ie_carater_inter_sus,
	nvl(a.cd_cgc_fornecedor, 'X'),
	obter_compl_pf(c.cd_pessoa_fisica,1,'CDM'),
	ie_cancelamento,
	decode(c.nr_atend_original, null, 'N', 'S') ie_atend_retorno,
	a.cd_medico_prescritor,
	d.nr_seq_ret_glosa,
	e.cd_medico,
	nvl(a.vl_custo_mat_repasse,0),
	a.qt_material,
	a.nr_atendimento,
	a.cd_convenio,
	nvl(a.cd_categoria, d.cd_categoria_parametro),
	nvl(a.vl_tabela_original,0),
	obter_dados_usuario_opcao(a.nm_usuario_original, 'C') cd_usuario_original,
	a.ie_responsavel_credito
into	dt_conta_w,
	cd_material_w,
	nr_prescricao_w,
	nr_cirurgia_w,
	nr_seq_proc_pacote_w,
	vl_material_w,
	cd_setor_atendimento_w,
	ie_pacote_w,
	cd_medico_atendimento_w,
	cd_medico_resp_w,
	cd_pessoa_indic_w,
	cd_cgc_prestador_w,
	ie_carater_inter_sus_w,
	cd_cgc_fornec_w,
	cd_municipio_ibge_w,
	ie_cancelamento_conta_w,
	ie_atend_retorno_w,
	cd_medico_prescritor_w,
	nr_seq_ret_glosa_w,
	cd_pessoa_tratamento_w,
	vl_custo_mat_repasse_w,
	qt_material_w,
	nr_atendimento_w,
	cd_convenio_plano_w,
	cd_categoria_plano_w,
	vl_tabela_original_w,
	cd_usuario_original_w,
	ie_responsavel_credito_w
from	pessoa_fisica e,
	estrutura_material_v b,
	conta_paciente	d,
	atendimento_paciente c,
 	material_atend_paciente a
where 	a.nr_sequencia     	= nr_sequencia_p
and	a.nr_interno_conta	= d.nr_interno_conta
and	c.nr_atendimento	= a.nr_atendimento
--and	d.ie_cancelamento	is null			Edgar 12/02/2008, Tirei esta linha, OS 82591
and	a.cd_material		= b.cd_material
and	c.cd_pessoa_fisica	= e.cd_pessoa_fisica
and	((ie_repasse_audit_w = 'S') or (ie_repasse_audit_w = 'N' and a.ie_auditoria <> 'S'))
union
select 	nvl(a.dt_conta, a.dt_atendimento),
	a.cd_material,
	a.nr_prescricao,
	a.nr_cirurgia,
	a.nr_seq_proc_pacote,
	dividir_sem_round((a.vl_material * OBTER_VALOR_ITEM_AUDIT(a.nr_interno_conta,null,a.nr_sequencia,'Q')),a.qt_material) vl_material,
	a.cd_setor_atendimento,
	decode(a.nr_seq_proc_pacote,null,'F','I'),
	c.cd_medico_atendimento,
	c.cd_medico_resp,
	c.cd_pessoa_indic,
	nvl(a.cd_cgc_prestador, 'X'),
	c.ie_carater_inter_sus,
	nvl(a.cd_cgc_fornecedor, 'X'),
	obter_compl_pf(c.cd_pessoa_fisica,1,'CDM'),
	ie_cancelamento,
	decode(c.nr_atend_original, null, 'N', 'S') ie_atend_retorno,
	a.cd_medico_prescritor,
	d.nr_seq_ret_glosa,
	e.cd_medico,
	dividir_sem_round((nvl(a.vl_custo_mat_repasse,0) * OBTER_VALOR_ITEM_AUDIT(a.nr_interno_conta,null,a.nr_sequencia,'Q')),a.qt_material) vl_custo_mat_repasse,
	a.qt_material,
	a.nr_atendimento,
	a.cd_convenio,
	nvl(a.cd_categoria, d.cd_categoria_parametro),
	dividir_sem_round((nvl(a.vl_tabela_original,0) * OBTER_VALOR_ITEM_AUDIT(a.nr_interno_conta,null,a.nr_sequencia,'Q')),a.qt_material) vl_tabela_original,
	obter_dados_usuario_opcao(a.nm_usuario_original, 'C') cd_usuario_original,
	a.ie_responsavel_credito
from	pessoa_fisica e,
	estrutura_material_v b,
	conta_paciente	d,
	atendimento_paciente c,
 	material_atend_paciente a
where 	a.nr_sequencia     	= nr_sequencia_p
and	a.nr_interno_conta	= d.nr_interno_conta
and	c.nr_atendimento	= a.nr_atendimento
--and	d.ie_cancelamento	is null			Edgar 12/02/2008, Tirei esta linha, OS 82591
and	a.cd_material		= b.cd_material
and	c.cd_pessoa_fisica	= e.cd_pessoa_fisica
and	(ie_repasse_audit_w = 'N' and a.ie_auditoria = 'S' and a.nr_seq_orig_audit is null)
and	nvl(OBTER_VALOR_ITEM_AUDIT(a.nr_interno_conta,null,a.nr_sequencia,'Q'),0) > 0;
exception
when no_data_found then
	goto final;
end;

select	obter_dado_atend_cat_conv(nr_atendimento_w, dt_conta_w, cd_convenio_plano_w, cd_categoria_plano_w, 'P'),
	nr_seq_classificacao
into	cd_plano_w,
	nr_seq_classif_atend_w
from	atendimento_paciente
where	nr_atendimento		= nr_atendimento_w;

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio		= cd_convenio_p;

select	max(nr_seq_proc_princ)
into	nr_seq_proc_princ_w
from	material_atend_paciente
where	nr_sequencia	= nr_sequencia_p;

cd_regra_w		:= 0;
vl_repasse_w		:= 0;

/* Ricardo 07/10/2004 - Alterei a rotina abaixo, nvl no m�dico */

if	(nr_prescricao_w is not null) then
	select	nvl(max(cd_medico),'0')
	into	cd_medico_prescr_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_w;
end if;

if	(cd_medico_prescr_w = '0') then
	cd_medico_prescr_w	:= null;
end if;

cd_medico_prescr_w	:= nvl(cd_medico_prescritor_w, cd_medico_prescr_w);

if	(nr_cirurgia_w is not null) then
	select	cd_medico_cirurgiao
	into	cd_medico_Cir_w
	from	Cirurgia
	where	nr_cirurgia	= nr_cirurgia_w;
end if;

cd_medico_w	:= nvl(cd_medico_cir_w, cd_medico_prescr_w);
cd_medico_w	:= nvl(cd_medico_w, cd_medico_atend_p);
cd_medico_ww	:= cd_medico_w;

cd_tab_preco_calc_w	:= null;

--if	(nr_seq_criterio_p	is null) then

	/* ahoffelder - OS 414610 - 22/05/2012 - substitui o antigo cursor por esta procedure porque a mesma rotina precisa ser chamada em outro objeto */
	obter_regra_mat_repasse(	cd_convenio_p,
					cd_edicao_amb_p,
					cd_estabelecimento_p,
					cd_medico_atend_p,
					ie_tipo_atendimento_p,
					nm_usuario_p,
					nr_sequencia_p,
					cd_regra_w,
					nr_seq_mat_crit_repasse_w,
					ie_responsavel_credito_w);

--else

	cd_regra_p			:= cd_regra_w;
	nr_seq_criterio_p		:= nr_seq_mat_crit_repasse_w;

--end if;

select	max(b.ie_forma_calculo),
	max(b.tx_repasse),
	max(b.vl_repasse),
	max(b.cd_tabela_preco),
	max(b.cd_convenio_calc),
	max(b.cd_categoria_calc),
	nvl(max(a.ie_pagto),'T'),
	max(b.cd_tab_preco_calc),
	nvl(max(b.ie_repasse_calc), 'S'),
	nvl(max(b.ie_perc_pacote), 'N')
into	ie_forma_calculo_w,
	tx_repasse_w,
	vl_repasse_w,
	cd_tabela_preco_w,
	cd_convenio_w,
	cd_categoria_w,
	ie_forma_pagto_w,
	cd_tab_preco_calc_w,
	ie_repasse_calc_w,
	ie_perc_pacote_w
from 	mat_criterio_repasse b,
	regra_repasse_terceiro a
where	b.nr_sequencia	= nr_seq_mat_crit_repasse_w
and	a.cd_regra	= b.cd_regra
and	a.cd_regra	= cd_regra_w;

if	(ie_forma_calculo_w = 'T') then
	begin
	OPEN C03;
	LOOP
	FETCH C03 into vl_tabela_w;
	exit when c03%notfound;
		vl_tabela_w	:= vl_tabela_w;	
	END LOOP;
	CLOSE C03;
	if	(tx_repasse_w = 0) then
		vl_repasse_w	:= vl_tabela_w; 
	else
		vl_repasse_w	:= ((vl_tabela_w * tx_repasse_w) / 100); 
	end if;
	end;
elsif	(ie_forma_calculo_w in ('C', 'A')) then
	begin
    	define_preco_material(
		cd_estabelecimento_p, cd_convenio_w, cd_categoria_w, dt_conta_w,
		cd_material_w, cd_tipo_acomodacao_w, ie_tipo_atendimento_p,
		cd_setor_atendimento_w, null, 0, 0, cd_plano_w, null, null, null, null, nr_seq_classif_atend_w, null, null, vl_tabela_w,
		dt_ult_vigencia_w, cd_tabela_preco_w, ie_origem_preco_w, nr_seq_bras_preco_w, nr_seq_mat_bras_w, nr_seq_conv_bras_w,
		nr_seq_conv_simpro_w, nr_seq_mat_simpro_w, nr_seq_simpro_preco_w, nr_seq_ajuste_mat_w);         
		
	if	(tx_repasse_w = 0) then
		vl_repasse_w	:= vl_tabela_w; 
	else
		vl_repasse_w	:= ((vl_tabela_w * tx_repasse_w) / 100); 
	end if;
	if	(ie_forma_calculo_w = 'A') then
		vl_repasse_w	:= vl_repasse_w * qt_material_w;
	end if;
	end;
elsif	(ie_forma_calculo_w = 'D') then		-- Edgar 12/05/2009, OS 139895, calcular pre�o pela diferen�a entre tabelas
	begin
	select	max(nvl(a.vl_preco_venda,0))
	into	vl_tabela_w
	from	preco_material a
	where	a.cd_material 		= cd_material_w
	and	a.cd_tab_preco_mat	= cd_tab_preco_calc_w
	and	a.dt_inicio_vigencia	=	(select	max(x.dt_inicio_vigencia)
						from 	preco_material x
						where	x.cd_material		= a.cd_material
						and	x.vl_preco_venda	> 0
						and	x.cd_tab_preco_mat	= cd_tab_preco_calc_w);

	if	(tx_repasse_w = 0) then
		vl_repasse_w	:= vl_material_w - (vl_tabela_w * qt_material_w);
	else
		vl_repasse_w	:= (((vl_material_w - (vl_tabela_w * qt_material_w)) * tx_repasse_w) / 100); 
	end if;
	end;
elsif	(ie_forma_calculo_w = 'P') then
	begin	
	if	(ie_perc_pacote_w = 'S')  then -- afstringari 188736 15/01/2010
		vl_material_w	:= vl_tabela_original_w;
	end if;
	
	vl_repasse_w	:= ((vl_material_w * tx_repasse_w) / 100);
	end;
elsif	(ie_forma_calculo_w = 'L') then					--  Edgar 04/08/2009, OS 134368
	vl_repasse_w	:= (vl_material_w - (vl_custo_mat_repasse_w * qt_material_w));
elsif	(ie_forma_calculo_w = 'O') then					--  Edgar 07/01/2012, OS 400858
	vl_repasse_w	:= (vl_material_w - (vl_custo_mat_repasse_w * qt_material_w)) * dividir_sem_round(tx_repasse_w, 100);
elsif	(ie_forma_calculo_w = 'U') then
	vl_repasse_w	:= (vl_repasse_w * qt_material_w);
elsif	(ie_forma_calculo_w = 'R') then
	vl_repasse_w	:= (tx_repasse_w / 100) * obter_vl_mat_repasse_adic(nr_seq_mat_crit_repasse_w, nr_sequencia_p);
elsif	(ie_forma_calculo_w	= 'F') then
	select	nvl(max(a.vl_procedimento),0)
	into	vl_repasse_w
	from	procedimento_paciente a
	where	a.nr_sequencia	= nr_seq_proc_princ_w;

	vl_repasse_w	:= (nvl(vl_repasse_w,0) * nvl(tx_repasse_w,0)) / 100;
elsif	(ie_forma_calculo_w = 'W') then
	vl_repasse_w		:= (vl_material_w * tx_repasse_w / 100) + nvl(vl_repasse_w,0);
end if;

if	(cd_regra_w > 0) and
	(ie_cancelamento_conta_w is null or nr_seq_ret_glosa_w is not null) and  -- Bruna, 12/02/2008, n�o gerar repasse conta cancelada
	(vl_repasse_w <> 0) then
	begin

	/*update	material_atend_paciente
	set	nr_seq_mat_crit_repasse		= nr_seq_mat_crit_repasse_w,
		vl_repasse_calc			= null
	where	nr_sequencia			= nr_sequencia_p;*/


	vl_repassar_w		:= vl_repasse_w;
	select	nr_seq_trans_fin_prod,
		NR_SEQ_TRANS_FIN_REP_MAIOR
	into	nr_seq_trans_fin_w,
		NR_SEQ_TRANS_FIN_REP_MAIOR_w
	from	regra_repasse_terceiro
	where	cd_regra	= cd_regra_w;

	OPEN C02;
	LOOP
	FETCH C02 into
		ie_beneficiario_w,
		tx_repasse_terc_w,
		nr_seq_terceiro_w,
		cd_conta_contabil_w,
		ie_perc_saldo_w,
		ie_gravacao_medico_w,
		cd_pessoa_fisica_w,
		nr_seq_regra_item_w;
	exit when c02%notfound;
		begin
		if	(vl_repasse_terc_ww > 0) then
			vl_repasse_terc_w	:= vl_repasse_terc_ww;
		elsif	(ie_perc_saldo_w = 'S') then
			vl_repasse_terc_w	:= vl_repasse_w * tx_repasse_terc_w / 100;
		else
			vl_repasse_terc_w	:= vl_repassar_w * tx_repasse_terc_w / 100;
		end if;
		vl_repasse_w			:= vl_repasse_w - vl_repasse_terc_w;
		cd_medico_w		:= '';
		if	(ie_beneficiario_w = 'C') then
			cd_medico_w		:= cd_medico_cir_w;
		elsif	(ie_beneficiario_w = 'P') then
			cd_medico_w		:= cd_medico_prescr_w;
		elsif	(ie_beneficiario_w = 'E') then
			cd_medico_w		:= cd_medico_prescr_w;
		elsif	(ie_beneficiario_w = 'A') then
			cd_medico_w		:= CD_MEDICO_RESP_w;
		elsif	(ie_beneficiario_w = 'I') then
			cd_medico_w		:= CD_MEDICO_ATENDIMENTO_w;
		elsif	(ie_beneficiario_w = 'O') then
			cd_medico_w		:= cd_pessoa_indic_w;
		elsif	(ie_beneficiario_w = 'M') then
			cd_medico_w		:= cd_pessoa_tratamento_w;
		elsif	(ie_beneficiario_w = 'D') then
			select	nvl(max(cd_medico_executor), cd_medico_prescr_w)
			into	cd_medico_w
			from	procedimento_paciente
			where	nr_sequencia = nr_seq_proc_princ_w;
		elsif	(ie_beneficiario_w = '4') then
			select	max(cd_medico)
			into	cd_medico_w
			from	med_avaliacao_paciente
			where	nr_atendimento		= nr_atendimento_w;
		elsif	(ie_beneficiario_w = '9') then
			cd_medico_w			:= cd_usuario_original_w;
		end if;

		if	(ie_beneficiario_w not in('T','F')) and
			(nvl(cd_medico_w,0) > 0) then
			begin
			cd_medico_ww		:= cd_medico_w;
			select	nvl(min(nr_sequencia),0)
			into	nr_seq_terceiro_w
			from 	terceiro
			where	cd_pessoa_fisica	= cd_medico_w
			and	cd_estabelecimento	= cd_estabelecimento_p
			and	nvl(ie_utilizacao,'A') in ('A','R')
			and	dt_conta_w	between
				nvl(trunc(dt_inicio_vigencia,'dd'), dt_conta_w - 1) and nvl(fim_dia(dt_fim_vigencia), dt_conta_w + 1)
			and	ie_situacao		= 'A';
			if	(nr_seq_terceiro_w = 0) then
				select	min(a.nr_seq_terceiro)
				into	nr_seq_terceiro_w
				from	terceiro b,
					terceiro_pessoa_fisica a
				where	a.cd_pessoa_fisica	= cd_medico_w
				and	a.nr_seq_terceiro	= b.nr_sequencia
				and	b.cd_estabelecimento	= cd_estabelecimento_p
				and	b.ie_situacao		= 'A'
				and	nvl(b.ie_utilizacao,'A') in ('A','R')
				and	dt_conta_w	between
					nvl(trunc(b.dt_inicio_vigencia,'dd'), dt_conta_w - 1) and nvl(fim_dia(b.dt_fim_vigencia), dt_conta_w + 1)
				and	dt_conta_w	between
					nvl(trunc(a.dt_inicio_vigencia, 'dd'), dt_conta_w - 1) and nvl(fim_dia(a.dt_fim_vigencia), dt_conta_w + 1)
				and	OBTER_SE_TERC_PF_CONV(a.nr_seq_terceiro, a.cd_pessoa_fisica, cd_convenio_p) = 'S';
			end if;
			if	(nr_seq_terceiro_w = 0) then
				select	nvl(min(nr_seq_terceiro),0)
				into	nr_seq_terceiro_w
				from 	Parametro_faturamento
				where	cd_estabelecimento	= cd_estabelecimento_p;
			end if;
			end;
		end if;
		if 	(ie_beneficiario_w =  'F') then
			begin
			select	nvl(min(nr_sequencia),0)
			into	nr_seq_terceiro_w
			from 	terceiro
			where	cd_cgc = cd_cgc_fornec_w
			and	cd_estabelecimento	= cd_estabelecimento_p
			and	nvl(ie_utilizacao,'A') in ('A','R')
			and	dt_conta_w	between
				nvl(trunc(dt_inicio_vigencia,'dd'), dt_conta_w - 1) and nvl(fim_dia(dt_fim_vigencia), dt_conta_w + 1)
			and	ie_situacao		= 'A';

			if	(nr_seq_terceiro_w = 0) then
				select	nvl(min(nr_seq_terceiro),0)
				into	nr_seq_terceiro_w
				from 	Parametro_faturamento
				where	cd_estabelecimento	= cd_estabelecimento_p;
			end if;		
			end;

		end if;

		if	(ie_beneficiario_w = 'S') then

			select 	nvl(max(nr_sequencia),0)
			into	nr_seq_terceiro_w
			from 	terceiro
			where	cd_cgc = cd_cgc_prestador_w
			and	dt_conta_w	between
				nvl(trunc(dt_inicio_vigencia,'dd'), dt_conta_w - 1) and nvl(fim_dia(dt_fim_vigencia), dt_conta_w + 1);

			if (nr_seq_terceiro_w = 0) then
				select	nvl(max(nr_seq_terceiro),0)
				into	nr_seq_terceiro_w
				from 	Parametro_faturamento
				where	cd_estabelecimento	= cd_estabelecimento_p;
			end if;
		end if;

		if	(nvl(nr_seq_terceiro_w,0) > 0) then
			begin
			if	(ie_forma_pagto_w = 'T') then
				select	nvl(max(ie_forma_pagto),'R')
				into	ie_forma_pagto_w
				from	terceiro
				where	nr_sequencia	= nr_seq_terceiro_w;
			end if;
			if	(ie_forma_pagto_w = 'P') then
				ie_status_w	:= 'S';
			else
				ie_status_w	:= 'A';
			end if;
			/* Marcus 20/07/2004 Gravar o m�dico somente quando a regra exige */
			cd_medico_rep_w	:= null;
			if	(ie_gravacao_medico_w = 'S') then
				cd_medico_rep_w	:= cd_medico_ww;
			elsif	(ie_gravacao_medico_w = 'P') then
				cd_medico_rep_w	:= cd_medico_Prescr_w;
			elsif	(ie_gravacao_medico_w = 'C') then
				cd_medico_rep_w	:= cd_medico_Cir_w;
			elsif	(ie_gravacao_medico_w = 'E') then
				select	max(cd_medico_executor)
				into	cd_medico_rep_w
				from	procedimento_paciente
				where	nr_sequencia = nr_seq_proc_princ_w;
				cd_medico_rep_w	:= cd_medico_rep_w;
			elsif	(ie_gravacao_medico_w = 'F') then
				cd_medico_rep_w	:= cd_pessoa_fisica_w;
			end if;

			/*select	material_repasse_seq.nextval
			into	nr_seq_repasse_w
			from	dual;
			insert	into material_repasse
				(nr_sequencia, nr_seq_material,vl_repasse,
				dt_atualizacao,	nm_usuario, nr_seq_terceiro, 
				nr_lote_contabil, nr_repasse_terceiro,
				cd_conta_contabil, ie_status, vl_liberado,
				cd_regra, cd_medico, nr_seq_trans_fin,
				dt_contabil_titulo, dt_contabil, NR_SEQ_TRANS_FIN_REP_MAIOR, ie_estorno, ie_repasse_calc, nr_seq_regra_item, vl_original_repasse)
			values
				(nr_seq_repasse_w, nr_sequencia_p, vl_repasse_terc_w,
				sysdate,nm_usuario_p, nr_seq_terceiro_w, 
				0, null, cd_conta_contabil_w, ie_status_w, 
				decode(ie_status_w,'S', vl_repasse_terc_w, 0),
				cd_regra_w, cd_medico_rep_w, nr_seq_trans_fin_w,
				to_date('01/01/2999','dd/mm/yyyy'), to_date('01/01/2999','dd/mm/yyyy'), NR_SEQ_TRANS_FIN_REP_MAIOR_w, 'N', ie_repasse_calc_w, nr_seq_regra_item_w, vl_repasse_terc_w);*/

			/*update	material_atend_paciente
			set	vl_repasse_calc			= nvl(vl_repasse_calc,0) + vl_repasse_terc_w
			where	nr_sequencia			= nr_sequencia_p;*/

			end;
		end if;
		end;
	END LOOP;
	CLOSE C02;
	end;
end if;

<<final>>
if	(1 = 2) then
	commit;
end if;	

END GERAR_SIMULACAO_MAT_REPASSE;
/