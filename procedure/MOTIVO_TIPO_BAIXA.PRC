create or replace
procedure motivo_tipo_baixa( 	nr_seq_motivo_p		number,
			ds_justificativa_p		varchar2,
			nr_seq_prescr_mat_p	number,
			nr_prescricao_p		number,
			nm_usuario_p		Varchar2) is 

begin


insert into prescr_mat_just_baixa(  
	   	nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_prescricao, 
		nr_seq_prescr_mat, 
		nr_seq_motivo, 
		ds_justificativa)
values		(prescr_mat_just_baixa_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_prescricao_p,
		nr_seq_prescr_mat_p,
		nr_seq_motivo_p,
		ds_justificativa_p);
		
commit;

end motivo_tipo_baixa;
/