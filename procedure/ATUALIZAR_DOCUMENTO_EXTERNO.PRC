create or replace
procedure atualizar_documento_externo(	nr_documento_externo_p	varchar2,
					nr_solic_compra_p	number,
					nm_usuario_p		Varchar2) is 

begin
update	solic_compra
set 	nr_documento_externo = nr_documento_externo_p
where 	nr_solic_compra = nr_solic_compra_p;	
commit;

end atualizar_documento_externo;
/