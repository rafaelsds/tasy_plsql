create or replace
procedure gerar_controle_pessoa(	cd_tipo_controle_p		Number,
				nm_usuario_p		varchar2,
				cd_setor_atendimento_p	Number,
				ie_filtra_aniversario_p	varchar2,
				dt_aniversario_p		date,
				ie_somente_medico_p	varchar2,
				ie_especialidade_p		number,
				ie_somente_funcionario_p	varchar2,
				ie_filtra_data_passagem_p	varchar2,
				dt_inicio_passagem_p	varchar2,
				dt_final_passagem_p	varchar2,
				ie_corpo_clinico_p		varchar2,
				nr_seq_vinculo_p		number,
				nr_seq_categoria_p		number,
				ie_obito_P		varchar2) as

cd_pessoa_fisica_w		varchar2(10);
qt_registros_w			number(10);

cursor C01 is
select	p.cd_pessoa_fisica
from	pessoa_fisica p
where	(exists(	select	1
	  	from	atendimento_paciente b,
	  		atend_paciente_unidade a
	  	where	a .cd_setor_atendimento = cd_setor_atendimento_p
	  	and	a.nr_atendimento = b.nr_atendimento
	  	and	b.cd_pessoa_fisica = p.cd_pessoa_fisica
		and	((p.dt_obito is null and ie_obito_p = 'S') or (ie_obito_p = 'N'))
		and	((ie_filtra_data_passagem_p = 'S' 
				and trunc(a.dt_entrada_unidade) between dt_inicio_passagem_p and dt_final_passagem_p) 
				or (ie_filtra_data_passagem_p = 'N'))	
			) or cd_setor_atendimento_p = 0)
and	((substr(p.dt_nascimento,4,2) = substr(dt_aniversario_p,4,2)) or ie_filtra_aniversario_p = 'N')
and	(exists(	select	1
		from	medico m
		where	m.cd_pessoa_fisica = p.cd_pessoa_fisica
		and	(
			(to_number(Obter_Especialidade_medico(m.cd_pessoa_fisica,'E')) = ie_especialidade_p or ie_especialidade_p = 0) and 
			((m.nr_seq_categoria = nr_seq_categoria_p) or (nvl(nr_seq_categoria_p,0) = 0)) and
			((m.ie_corpo_clinico = ie_corpo_clinico_p) or (nvl(ie_corpo_clinico_p,'N') = 'N')) and
			((m.ie_vinculo_medico= nr_seq_vinculo_p) or (nvl(nr_seq_vinculo_p,0) = 0))
			)
			) or ie_somente_medico_p = 'N')
and	((ie_funcionario = 'S') or ie_somente_funcionario_p = 'N');

BEGIN

open C01;
loop
fetch C01 into
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
	select 	count(*) 
	into	qt_registros_w
	from 	controle_pessoa
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	cd_tipo_controle = cd_tipo_controle_p;
	
	if (nvl(qt_registros_w,0) = 0) then
	
		insert into controle_pessoa(	nr_sequencia,
					cd_tipo_controle,
					dt_atualizacao,
					nm_usuario,
					cd_pessoa_fisica,
					ie_situacao)
				values    (	controle_pessoa_seq.nextval,
					cd_tipo_controle_p,
					sysdate,
					nm_usuario_p,
					cd_pessoa_fisica_w,
					'A');
	
	else
	
		update 	controle_pessoa 
		set 	dt_atualizacao = sysdate, 
			nm_usuario = nm_usuario_p
		where	cd_tipo_controle = cd_tipo_controle_p
		and	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
	end if;
			
	end;
end loop;
close c01;

commit;
end gerar_controle_pessoa;
/
